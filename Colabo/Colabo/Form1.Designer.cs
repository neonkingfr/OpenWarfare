namespace Colabo
{
    partial class FormColabo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      Calendar.DrawTool drawTool2 = new Calendar.DrawTool();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormColabo));
      System.Windows.Forms.ToolStripButton NextUnreadButton;
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      this.dayView = new Calendar.DayView();
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.splitContainer3 = new System.Windows.Forms.SplitContainer();
      this.tagsNavigator = new System.Windows.Forms.WebBrowser();
      this.contextMenuTags = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.NOTTag = new System.Windows.Forms.ToolStripMenuItem();
      this.makeTagRead = new System.Windows.Forms.ToolStripMenuItem();
      this.renameTag = new System.Windows.Forms.ToolStripMenuItem();
      this.splitContainer4 = new System.Windows.Forms.SplitContainer();
      this.imState = new System.Windows.Forms.ComboBox();
      this.imUsers = new System.Windows.Forms.ListView();
      this.userColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.stateColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.lastSeenColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.locationColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
      this.presenceImages = new System.Windows.Forms.ImageList(this.components);
      this.imFilter = new System.Windows.Forms.ListBox();
      this.bookmarks = new System.Windows.Forms.TreeView();
      this.imageList1 = new System.Windows.Forms.ImageList(this.components);
      this.splitContainer2 = new System.Windows.Forms.SplitContainer();
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tabTasks = new System.Windows.Forms.TabPage();
      this.dataGridView1 = new Colabo.FormColabo.DataGridViewColabo();
      this.Picture = new Colabo.FormColabo.DataGridViewImagesColumn();
      this.Title = new Colabo.FormColabo.DataGridViewTitleColumn();
      this.Author = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.tabCalendar = new System.Windows.Forms.TabPage();
      this.monthCalendar = new System.Windows.Forms.MonthCalendar();
      this.webBrowser1 = new Colabo.ColaboWebBrowser();
      this.contextMenuPreview = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.copyPreviewMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.copyShortcutPreviewMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.replyPreviewMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.appendPreviewMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.contextMenuBookmarks = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.openBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.openBookmarkInNewWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
      this.editBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.deleteBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
      this.addBookmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.newFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.contextMenuArticle = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.replyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.appendNoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.markAsReadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.markAsUnreadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
      this.sVNLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.blameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
      this.copyColaboShortcutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
      this.setAsHomePageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.bookmarkThisArticleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStrip1 = new System.Windows.Forms.ToolStrip();
      this.homeButton = new System.Windows.Forms.ToolStripButton();
      this.navigateBackButton = new System.Windows.Forms.ToolStripButton();
      this.navigateForwardButton = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
      this.newArticleButton = new System.Windows.Forms.ToolStripButton();
      this.synchronizeButton = new System.Windows.Forms.ToolStripButton();
      this.findButton = new System.Windows.Forms.ToolStripButton();
      this.findAdvancedButton = new System.Windows.Forms.ToolStripButton();
      this.goToButton = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
      this.ganttChartButton = new System.Windows.Forms.ToolStripButton();
      this.ConfigureButton = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
      this.ruleSetComboBox = new System.Windows.Forms.ToolStripComboBox();
      this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
      this.dataGridViewTitleColumn1 = new Colabo.FormColabo.DataGridViewTitleColumn();
      this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
      this.newWindowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.newArticleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.newTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.openLocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
      this.synchronizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
      this.savePageAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.sendLinkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripSeparator();
      this.pageSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
      this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.navigateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem8 = new System.Windows.Forms.ToolStripSeparator();
      this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripSeparator();
      this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripSeparator();
      this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.findNextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripSeparator();
      this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.advancedSearchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.goToAddressBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.goToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.navigateBackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.navigateForwardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.homeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.goToLocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripSeparator();
      this.nextUnreadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.historyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.ruleSetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
      this.importFromNGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.importFromWikiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
      this.accountSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripSeparator();
      this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
      this.versionHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
      this.checkForUpdateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
      this.aboutColaboToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.designToolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.checkFulltextIndicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.SVNBenchmarkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.test1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.test2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.statusStrip1 = new System.Windows.Forms.StatusStrip();
      this.taskState = new System.Windows.Forms.ToolStripStatusLabel();
      this.taskProgress = new System.Windows.Forms.ToolStripProgressBar();
      this.queueState = new System.Windows.Forms.ToolStripStatusLabel();
      this.contextMenuIM = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.startChatMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.startPublicChatMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.setLocationNameMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
      NextUnreadButton = new System.Windows.Forms.ToolStripButton();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.splitContainer3.Panel1.SuspendLayout();
      this.splitContainer3.Panel2.SuspendLayout();
      this.splitContainer3.SuspendLayout();
      this.contextMenuTags.SuspendLayout();
      this.splitContainer4.Panel1.SuspendLayout();
      this.splitContainer4.Panel2.SuspendLayout();
      this.splitContainer4.SuspendLayout();
      this.splitContainer2.Panel1.SuspendLayout();
      this.splitContainer2.Panel2.SuspendLayout();
      this.splitContainer2.SuspendLayout();
      this.tabControl1.SuspendLayout();
      this.tabTasks.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      this.tabCalendar.SuspendLayout();
      this.contextMenuPreview.SuspendLayout();
      this.contextMenuBookmarks.SuspendLayout();
      this.contextMenuArticle.SuspendLayout();
      this.toolStrip1.SuspendLayout();
      this.menuStrip1.SuspendLayout();
      this.statusStrip1.SuspendLayout();
      this.contextMenuIM.SuspendLayout();
      this.SuspendLayout();
      // 
      // dayView
      // 
      drawTool2.DayView = this.dayView;
      this.dayView.ActiveTool = drawTool2;
      this.dayView.AllowInplaceEditing = false;
      this.dayView.AllowNew = false;
      resources.ApplyResources(this.dayView, "dayView");
      this.dayView.DaysToShow = 3;
      this.dayView.Name = "dayView";
      this.dayView.SelectedAppointment = null;
      this.dayView.SelectionEnd = new System.DateTime(((long)(0)));
      this.dayView.SelectionStart = new System.DateTime(((long)(0)));
      this.dayView.StartDate = new System.DateTime(((long)(0)));
      this.dayView.SelectionChanged += new System.EventHandler(this.dayView_SelectionChanged);
      this.dayView.ResolveAppointments += new Calendar.ResolveAppointmentsEventHandler(this.dayView_ResolveAppointments);
      this.dayView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dayView_MouseDoubleClick);
      this.dayView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dayView_MouseDown);
      // 
      // NextUnreadButton
      // 
      NextUnreadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(NextUnreadButton, "NextUnreadButton");
      NextUnreadButton.Name = "NextUnreadButton";
      NextUnreadButton.Click += new System.EventHandler(this.NextUnreadButton_Click);
      // 
      // splitContainer1
      // 
      resources.ApplyResources(this.splitContainer1, "splitContainer1");
      this.splitContainer1.BackColor = System.Drawing.SystemColors.Control;
      this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.splitContainer1.Name = "splitContainer1";
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.splitContainer3);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
      // 
      // splitContainer3
      // 
      resources.ApplyResources(this.splitContainer3, "splitContainer3");
      this.splitContainer3.Name = "splitContainer3";
      // 
      // splitContainer3.Panel1
      // 
      this.splitContainer3.Panel1.Controls.Add(this.tagsNavigator);
      // 
      // splitContainer3.Panel2
      // 
      this.splitContainer3.Panel2.Controls.Add(this.splitContainer4);
      // 
      // tagsNavigator
      // 
      this.tagsNavigator.AllowWebBrowserDrop = false;
      this.tagsNavigator.ContextMenuStrip = this.contextMenuTags;
      resources.ApplyResources(this.tagsNavigator, "tagsNavigator");
      this.tagsNavigator.IsWebBrowserContextMenuEnabled = false;
      this.tagsNavigator.Name = "tagsNavigator";
      this.tagsNavigator.ScriptErrorsSuppressed = true;
      this.tagsNavigator.WebBrowserShortcutsEnabled = false;
      this.tagsNavigator.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.tagsNavigator_Navigating);
      // 
      // contextMenuTags
      // 
      this.contextMenuTags.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NOTTag,
            this.makeTagRead,
            this.renameTag});
      this.contextMenuTags.Name = "contextMenuTags";
      this.contextMenuTags.ShowItemToolTips = false;
      resources.ApplyResources(this.contextMenuTags, "contextMenuTags");
      this.contextMenuTags.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuTags_Opening);
      // 
      // NOTTag
      // 
      this.NOTTag.Name = "NOTTag";
      resources.ApplyResources(this.NOTTag, "NOTTag");
      this.NOTTag.Click += new System.EventHandler(this.NOTTag_Click);
      // 
      // makeTagRead
      // 
      this.makeTagRead.Name = "makeTagRead";
      resources.ApplyResources(this.makeTagRead, "makeTagRead");
      this.makeTagRead.Click += new System.EventHandler(this.makeTagReadToolStripMenuItem_Click);
      // 
      // renameTag
      // 
      this.renameTag.Name = "renameTag";
      resources.ApplyResources(this.renameTag, "renameTag");
      this.renameTag.Click += new System.EventHandler(this.renameTag_Click);
      // 
      // splitContainer4
      // 
      resources.ApplyResources(this.splitContainer4, "splitContainer4");
      this.splitContainer4.Name = "splitContainer4";
      // 
      // splitContainer4.Panel1
      // 
      this.splitContainer4.Panel1.Controls.Add(this.imState);
      this.splitContainer4.Panel1.Controls.Add(this.imUsers);
      this.splitContainer4.Panel1.Controls.Add(this.imFilter);
      // 
      // splitContainer4.Panel2
      // 
      this.splitContainer4.Panel2.Controls.Add(this.bookmarks);
      // 
      // imState
      // 
      this.imState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.imState.FormattingEnabled = true;
      this.imState.Items.AddRange(new object[] {
            resources.GetString("imState.Items"),
            resources.GetString("imState.Items1"),
            resources.GetString("imState.Items2"),
            resources.GetString("imState.Items3"),
            resources.GetString("imState.Items4"),
            resources.GetString("imState.Items5")});
      resources.ApplyResources(this.imState, "imState");
      this.imState.Name = "imState";
      this.imState.SelectedIndexChanged += new System.EventHandler(this.imState_SelectedIndexChanged);
      // 
      // imUsers
      // 
      this.imUsers.AllowDrop = true;
      resources.ApplyResources(this.imUsers, "imUsers");
      this.imUsers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.userColumn,
            this.stateColumn,
            this.lastSeenColumn,
            this.locationColumn});
      this.imUsers.FullRowSelect = true;
      this.imUsers.Name = "imUsers";
      this.imUsers.StateImageList = this.presenceImages;
      this.imUsers.UseCompatibleStateImageBehavior = false;
      this.imUsers.View = System.Windows.Forms.View.Details;
      this.imUsers.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.imUsers_ItemDrag);
      this.imUsers.DragDrop += new System.Windows.Forms.DragEventHandler(this.imUsers_DragDrop);
      this.imUsers.DragEnter += new System.Windows.Forms.DragEventHandler(this.imUsers_DragEnter);
      // 
      // userColumn
      // 
      resources.ApplyResources(this.userColumn, "userColumn");
      // 
      // stateColumn
      // 
      resources.ApplyResources(this.stateColumn, "stateColumn");
      // 
      // lastSeenColumn
      // 
      resources.ApplyResources(this.lastSeenColumn, "lastSeenColumn");
      // 
      // locationColumn
      // 
      resources.ApplyResources(this.locationColumn, "locationColumn");
      // 
      // presenceImages
      // 
      this.presenceImages.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("presenceImages.ImageStream")));
      this.presenceImages.TransparentColor = System.Drawing.Color.Transparent;
      this.presenceImages.Images.SetKeyName(0, "Question_mark.ico");
      this.presenceImages.Images.SetKeyName(1, "user-offline.ico");
      this.presenceImages.Images.SetKeyName(2, "user-online.ico");
      this.presenceImages.Images.SetKeyName(3, "user-away.ico");
      this.presenceImages.Images.SetKeyName(4, "user-busy.ico");
      this.presenceImages.Images.SetKeyName(5, "user-busy.ico");
      this.presenceImages.Images.SetKeyName(6, "user-invisible.ico");
      // 
      // imFilter
      // 
      this.imFilter.FormattingEnabled = true;
      resources.ApplyResources(this.imFilter, "imFilter");
      this.imFilter.Name = "imFilter";
      // 
      // bookmarks
      // 
      this.bookmarks.AllowDrop = true;
      resources.ApplyResources(this.bookmarks, "bookmarks");
      this.bookmarks.ImageList = this.imageList1;
      this.bookmarks.Name = "bookmarks";
      this.bookmarks.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.bookmarks_ItemDrag);
      this.bookmarks.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.bookmarks_NodeMouseClick);
      this.bookmarks.DragDrop += new System.Windows.Forms.DragEventHandler(this.bookmarks_DragDrop);
      this.bookmarks.DragOver += new System.Windows.Forms.DragEventHandler(this.bookmarks_DragOver);
      // 
      // imageList1
      // 
      this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
      this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
      this.imageList1.Images.SetKeyName(0, "db.ico");
      this.imageList1.Images.SetKeyName(1, "Folder.ico");
      this.imageList1.Images.SetKeyName(2, "Messages.ico");
      this.imageList1.Images.SetKeyName(3, "SingleMessage.ico");
      this.imageList1.Images.SetKeyName(4, "Computer.ico");
      this.imageList1.Images.SetKeyName(5, "delete_16x.ico");
      this.imageList1.Images.SetKeyName(6, "error.ico");
      this.imageList1.Images.SetKeyName(7, "INFO.ICO");
      this.imageList1.Images.SetKeyName(8, "eventlogSuccessAudit.ico");
      this.imageList1.Images.SetKeyName(9, "eventlogFailureAudit.ico");
      this.imageList1.Images.SetKeyName(10, "Note.ico");
      this.imageList1.Images.SetKeyName(11, "performance.ico");
      this.imageList1.Images.SetKeyName(12, "repair.ico");
      this.imageList1.Images.SetKeyName(13, "servers.ico");
      this.imageList1.Images.SetKeyName(14, "warning.ico");
      // 
      // splitContainer2
      // 
      this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      resources.ApplyResources(this.splitContainer2, "splitContainer2");
      this.splitContainer2.Name = "splitContainer2";
      // 
      // splitContainer2.Panel1
      // 
      this.splitContainer2.Panel1.Controls.Add(this.tabControl1);
      // 
      // splitContainer2.Panel2
      // 
      this.splitContainer2.Panel2.Controls.Add(this.webBrowser1);
      // 
      // tabControl1
      // 
      this.tabControl1.Controls.Add(this.tabTasks);
      this.tabControl1.Controls.Add(this.tabCalendar);
      resources.ApplyResources(this.tabControl1, "tabControl1");
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
      // 
      // tabTasks
      // 
      this.tabTasks.Controls.Add(this.dataGridView1);
      resources.ApplyResources(this.tabTasks, "tabTasks");
      this.tabTasks.Name = "tabTasks";
      this.tabTasks.UseVisualStyleBackColor = true;
      // 
      // dataGridView1
      // 
      this.dataGridView1.AllowDrop = true;
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.AllowUserToOrderColumns = true;
      this.dataGridView1.AllowUserToResizeRows = false;
      this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
      this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Picture,
            this.Title,
            this.Author,
            this.Date});
      dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
      dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle4;
      resources.ApplyResources(this.dataGridView1, "dataGridView1");
      this.dataGridView1.MouseMoveEnabled = true;
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.ReadOnly = true;
      this.dataGridView1.RowHeadersVisible = false;
      this.dataGridView1.RowTemplate.Height = 17;
      this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dataGridView1.VirtualMode = true;
      this.dataGridView1.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.dataGridView1_CellContextMenuStripNeeded);
      this.dataGridView1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellDoubleClick);
      this.dataGridView1.CellToolTipTextNeeded += new System.Windows.Forms.DataGridViewCellToolTipTextNeededEventHandler(this.dataGridView1_CellToolTipTextNeeded);
      this.dataGridView1.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.dataGridView1_CellValueNeeded);
      this.dataGridView1.ColumnDisplayIndexChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dataGridView1_ColumnDisplayIndexChanged);
      this.dataGridView1.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1_ColumnHeaderMouseClick);
      this.dataGridView1.CurrentCellChanged += new System.EventHandler(this.dataGridView1_CurrentCellChanged);
      this.dataGridView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragDrop);
      this.dataGridView1.DragOver += new System.Windows.Forms.DragEventHandler(this.dataGridView1_DragOver);
      this.dataGridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseDown);
      this.dataGridView1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseMove);
      this.dataGridView1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dataGridView1_MouseUp);
      // 
      // Picture
      // 
      this.Picture.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      resources.ApplyResources(this.Picture, "Picture");
      this.Picture.Name = "Picture";
      this.Picture.ReadOnly = true;
      this.Picture.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      this.Picture.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
      // 
      // Title
      // 
      this.Title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.Title.FillWeight = 50F;
      resources.ApplyResources(this.Title, "Title");
      this.Title.Name = "Title";
      this.Title.ReadOnly = true;
      // 
      // Author
      // 
      this.Author.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.Author.FillWeight = 25F;
      resources.ApplyResources(this.Author, "Author");
      this.Author.Name = "Author";
      this.Author.ReadOnly = true;
      // 
      // Date
      // 
      this.Date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.Date.FillWeight = 25F;
      resources.ApplyResources(this.Date, "Date");
      this.Date.Name = "Date";
      this.Date.ReadOnly = true;
      // 
      // tabCalendar
      // 
      this.tabCalendar.Controls.Add(this.dayView);
      this.tabCalendar.Controls.Add(this.monthCalendar);
      resources.ApplyResources(this.tabCalendar, "tabCalendar");
      this.tabCalendar.Name = "tabCalendar";
      this.tabCalendar.UseVisualStyleBackColor = true;
      // 
      // monthCalendar
      // 
      resources.ApplyResources(this.monthCalendar, "monthCalendar");
      this.monthCalendar.Name = "monthCalendar";
      this.monthCalendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar_DateChanged);
      // 
      // webBrowser1
      // 
      this.webBrowser1.ContextMenuStrip = this.contextMenuPreview;
      resources.ApplyResources(this.webBrowser1, "webBrowser1");
      this.webBrowser1.httpErrorCode = 200;
      this.webBrowser1.IsWebBrowserContextMenuEnabled = false;
      this.webBrowser1.Name = "webBrowser1";
      this.webBrowser1.NavigateError += new Colabo.WebBrowserNavigateErrorEventHandler(this.WebBrowser_NavigateError);
      this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
      this.webBrowser1.Navigated += new System.Windows.Forms.WebBrowserNavigatedEventHandler(this.webBrowser1_Navigated);
      this.webBrowser1.Navigating += new System.Windows.Forms.WebBrowserNavigatingEventHandler(this.webBrowser1_Navigating);
      // 
      // contextMenuPreview
      // 
      this.contextMenuPreview.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyPreviewMenu,
            this.copyShortcutPreviewMenu,
            this.replyPreviewMenu,
            this.appendPreviewMenuItem});
      this.contextMenuPreview.Name = "contextMenuPreview";
      resources.ApplyResources(this.contextMenuPreview, "contextMenuPreview");
      this.contextMenuPreview.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuPreview_Opening);
      // 
      // copyPreviewMenu
      // 
      this.copyPreviewMenu.Name = "copyPreviewMenu";
      resources.ApplyResources(this.copyPreviewMenu, "copyPreviewMenu");
      this.copyPreviewMenu.Click += new System.EventHandler(this.copyPreviewMenu_Click);
      // 
      // copyShortcutPreviewMenu
      // 
      this.copyShortcutPreviewMenu.Name = "copyShortcutPreviewMenu";
      resources.ApplyResources(this.copyShortcutPreviewMenu, "copyShortcutPreviewMenu");
      this.copyShortcutPreviewMenu.Click += new System.EventHandler(this.copyShortcutPreviewMenu_Click);
      // 
      // replyPreviewMenu
      // 
      this.replyPreviewMenu.Name = "replyPreviewMenu";
      resources.ApplyResources(this.replyPreviewMenu, "replyPreviewMenu");
      this.replyPreviewMenu.Click += new System.EventHandler(this.replyPreviewMenu_Click);
      // 
      // appendPreviewMenuItem
      // 
      this.appendPreviewMenuItem.Name = "appendPreviewMenuItem";
      resources.ApplyResources(this.appendPreviewMenuItem, "appendPreviewMenuItem");
      this.appendPreviewMenuItem.Click += new System.EventHandler(this.appendNotePreviewMenuMenuItem_Click);
      // 
      // contextMenuBookmarks
      // 
      this.contextMenuBookmarks.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openBookmarkToolStripMenuItem,
            this.openBookmarkInNewWindowToolStripMenuItem,
            this.toolStripSeparator9,
            this.editBookmarkToolStripMenuItem,
            this.deleteBookmarkToolStripMenuItem,
            this.toolStripSeparator10,
            this.addBookmarkToolStripMenuItem,
            this.newFolderToolStripMenuItem});
      this.contextMenuBookmarks.Name = "contextMenuBookmarks";
      resources.ApplyResources(this.contextMenuBookmarks, "contextMenuBookmarks");
      // 
      // openBookmarkToolStripMenuItem
      // 
      this.openBookmarkToolStripMenuItem.Name = "openBookmarkToolStripMenuItem";
      resources.ApplyResources(this.openBookmarkToolStripMenuItem, "openBookmarkToolStripMenuItem");
      this.openBookmarkToolStripMenuItem.Click += new System.EventHandler(this.openBookmarkToolStripMenuItem_Click);
      // 
      // openBookmarkInNewWindowToolStripMenuItem
      // 
      this.openBookmarkInNewWindowToolStripMenuItem.Name = "openBookmarkInNewWindowToolStripMenuItem";
      resources.ApplyResources(this.openBookmarkInNewWindowToolStripMenuItem, "openBookmarkInNewWindowToolStripMenuItem");
      this.openBookmarkInNewWindowToolStripMenuItem.Click += new System.EventHandler(this.openBookmarkInNewWindowToolStripMenuItem_Click);
      // 
      // toolStripSeparator9
      // 
      this.toolStripSeparator9.Name = "toolStripSeparator9";
      resources.ApplyResources(this.toolStripSeparator9, "toolStripSeparator9");
      // 
      // editBookmarkToolStripMenuItem
      // 
      this.editBookmarkToolStripMenuItem.Name = "editBookmarkToolStripMenuItem";
      resources.ApplyResources(this.editBookmarkToolStripMenuItem, "editBookmarkToolStripMenuItem");
      this.editBookmarkToolStripMenuItem.Click += new System.EventHandler(this.editBookmarkToolStripMenuItem_Click);
      // 
      // deleteBookmarkToolStripMenuItem
      // 
      this.deleteBookmarkToolStripMenuItem.Name = "deleteBookmarkToolStripMenuItem";
      resources.ApplyResources(this.deleteBookmarkToolStripMenuItem, "deleteBookmarkToolStripMenuItem");
      this.deleteBookmarkToolStripMenuItem.Click += new System.EventHandler(this.deleteBookmarkToolStripMenuItem_Click);
      // 
      // toolStripSeparator10
      // 
      this.toolStripSeparator10.Name = "toolStripSeparator10";
      resources.ApplyResources(this.toolStripSeparator10, "toolStripSeparator10");
      // 
      // addBookmarkToolStripMenuItem
      // 
      this.addBookmarkToolStripMenuItem.Name = "addBookmarkToolStripMenuItem";
      resources.ApplyResources(this.addBookmarkToolStripMenuItem, "addBookmarkToolStripMenuItem");
      this.addBookmarkToolStripMenuItem.Click += new System.EventHandler(this.addBookmarkToolStripMenuItem_Click);
      // 
      // newFolderToolStripMenuItem
      // 
      this.newFolderToolStripMenuItem.Name = "newFolderToolStripMenuItem";
      resources.ApplyResources(this.newFolderToolStripMenuItem, "newFolderToolStripMenuItem");
      this.newFolderToolStripMenuItem.Click += new System.EventHandler(this.newFolderToolStripMenuItem_Click);
      // 
      // contextMenuArticle
      // 
      this.contextMenuArticle.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.replyToolStripMenuItem,
            this.editToolStripMenuItem,
            this.appendNoteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripSeparator1,
            this.markAsReadToolStripMenuItem,
            this.markAsUnreadToolStripMenuItem,
            this.toolStripSeparator3,
            this.sVNLogToolStripMenuItem,
            this.blameToolStripMenuItem,
            this.toolStripSeparator4,
            this.copyColaboShortcutToolStripMenuItem,
            this.toolStripSeparator8,
            this.setAsHomePageToolStripMenuItem,
            this.bookmarkThisArticleToolStripMenuItem});
      this.contextMenuArticle.Name = "contextMenuArticle";
      resources.ApplyResources(this.contextMenuArticle, "contextMenuArticle");
      // 
      // replyToolStripMenuItem
      // 
      this.replyToolStripMenuItem.Name = "replyToolStripMenuItem";
      resources.ApplyResources(this.replyToolStripMenuItem, "replyToolStripMenuItem");
      this.replyToolStripMenuItem.Click += new System.EventHandler(this.replyToolStripMenuItem_Click);
      // 
      // editToolStripMenuItem
      // 
      this.editToolStripMenuItem.Name = "editToolStripMenuItem";
      resources.ApplyResources(this.editToolStripMenuItem, "editToolStripMenuItem");
      this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
      // 
      // appendNoteToolStripMenuItem
      // 
      this.appendNoteToolStripMenuItem.Name = "appendNoteToolStripMenuItem";
      resources.ApplyResources(this.appendNoteToolStripMenuItem, "appendNoteToolStripMenuItem");
      this.appendNoteToolStripMenuItem.Click += new System.EventHandler(this.appendNoteToolStripMenuItem_Click);
      // 
      // deleteToolStripMenuItem
      // 
      this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
      resources.ApplyResources(this.deleteToolStripMenuItem, "deleteToolStripMenuItem");
      this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
      // 
      // markAsReadToolStripMenuItem
      // 
      this.markAsReadToolStripMenuItem.Name = "markAsReadToolStripMenuItem";
      resources.ApplyResources(this.markAsReadToolStripMenuItem, "markAsReadToolStripMenuItem");
      this.markAsReadToolStripMenuItem.Click += new System.EventHandler(this.markAsReadToolStripMenuItem_Click);
      // 
      // markAsUnreadToolStripMenuItem
      // 
      this.markAsUnreadToolStripMenuItem.Name = "markAsUnreadToolStripMenuItem";
      resources.ApplyResources(this.markAsUnreadToolStripMenuItem, "markAsUnreadToolStripMenuItem");
      this.markAsUnreadToolStripMenuItem.Click += new System.EventHandler(this.markAsUnreadToolStripMenuItem_Click);
      // 
      // toolStripSeparator3
      // 
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
      // 
      // sVNLogToolStripMenuItem
      // 
      this.sVNLogToolStripMenuItem.Name = "sVNLogToolStripMenuItem";
      resources.ApplyResources(this.sVNLogToolStripMenuItem, "sVNLogToolStripMenuItem");
      this.sVNLogToolStripMenuItem.Click += new System.EventHandler(this.SVNLogToolStripMenuItem_Click);
      // 
      // blameToolStripMenuItem
      // 
      this.blameToolStripMenuItem.Name = "blameToolStripMenuItem";
      resources.ApplyResources(this.blameToolStripMenuItem, "blameToolStripMenuItem");
      this.blameToolStripMenuItem.Click += new System.EventHandler(this.blameToolStripMenuItem_Click);
      // 
      // toolStripSeparator4
      // 
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
      // 
      // copyColaboShortcutToolStripMenuItem
      // 
      this.copyColaboShortcutToolStripMenuItem.Name = "copyColaboShortcutToolStripMenuItem";
      resources.ApplyResources(this.copyColaboShortcutToolStripMenuItem, "copyColaboShortcutToolStripMenuItem");
      this.copyColaboShortcutToolStripMenuItem.Tag = "list";
      this.copyColaboShortcutToolStripMenuItem.Click += new System.EventHandler(this.copyColaboShortcutToolStripMenuItem_Click);
      // 
      // toolStripSeparator8
      // 
      this.toolStripSeparator8.Name = "toolStripSeparator8";
      resources.ApplyResources(this.toolStripSeparator8, "toolStripSeparator8");
      // 
      // setAsHomePageToolStripMenuItem
      // 
      this.setAsHomePageToolStripMenuItem.Name = "setAsHomePageToolStripMenuItem";
      resources.ApplyResources(this.setAsHomePageToolStripMenuItem, "setAsHomePageToolStripMenuItem");
      this.setAsHomePageToolStripMenuItem.Click += new System.EventHandler(this.setAsHomePageToolStripMenuItem_Click);
      // 
      // bookmarkThisArticleToolStripMenuItem
      // 
      this.bookmarkThisArticleToolStripMenuItem.Name = "bookmarkThisArticleToolStripMenuItem";
      resources.ApplyResources(this.bookmarkThisArticleToolStripMenuItem, "bookmarkThisArticleToolStripMenuItem");
      this.bookmarkThisArticleToolStripMenuItem.Click += new System.EventHandler(this.bookmarkThisArticleToolStripMenuItem_Click);
      // 
      // toolStrip1
      // 
      this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.homeButton,
            this.navigateBackButton,
            this.navigateForwardButton,
            this.toolStripSeparator7,
            this.newArticleButton,
            this.synchronizeButton,
            this.findButton,
            this.findAdvancedButton,
            NextUnreadButton,
            this.goToButton,
            this.toolStripSeparator11,
            this.ganttChartButton,
            this.ConfigureButton,
            this.toolStripSeparator12,
            this.ruleSetComboBox});
      resources.ApplyResources(this.toolStrip1, "toolStrip1");
      this.toolStrip1.Name = "toolStrip1";
      // 
      // homeButton
      // 
      this.homeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(this.homeButton, "homeButton");
      this.homeButton.Name = "homeButton";
      this.homeButton.Click += new System.EventHandler(this.homeButton_Click);
      // 
      // navigateBackButton
      // 
      this.navigateBackButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(this.navigateBackButton, "navigateBackButton");
      this.navigateBackButton.Name = "navigateBackButton";
      this.navigateBackButton.Click += new System.EventHandler(this.navigateBackButton_Click);
      // 
      // navigateForwardButton
      // 
      this.navigateForwardButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(this.navigateForwardButton, "navigateForwardButton");
      this.navigateForwardButton.Name = "navigateForwardButton";
      this.navigateForwardButton.Click += new System.EventHandler(this.navigateForwardButton_Click);
      // 
      // toolStripSeparator7
      // 
      this.toolStripSeparator7.Name = "toolStripSeparator7";
      resources.ApplyResources(this.toolStripSeparator7, "toolStripSeparator7");
      // 
      // newArticleButton
      // 
      this.newArticleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(this.newArticleButton, "newArticleButton");
      this.newArticleButton.Name = "newArticleButton";
      this.newArticleButton.Click += new System.EventHandler(this.newArticleButton_Click);
      // 
      // synchronizeButton
      // 
      this.synchronizeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(this.synchronizeButton, "synchronizeButton");
      this.synchronizeButton.Name = "synchronizeButton";
      this.synchronizeButton.Click += new System.EventHandler(this.synchronizeButton_Click);
      // 
      // findButton
      // 
      this.findButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(this.findButton, "findButton");
      this.findButton.Name = "findButton";
      this.findButton.Click += new System.EventHandler(this.findButton_Click);
      // 
      // findAdvancedButton
      // 
      this.findAdvancedButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(this.findAdvancedButton, "findAdvancedButton");
      this.findAdvancedButton.Name = "findAdvancedButton";
      this.findAdvancedButton.Click += new System.EventHandler(this.findAdvancedButton_Click);
      // 
      // goToButton
      // 
      this.goToButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(this.goToButton, "goToButton");
      this.goToButton.Name = "goToButton";
      this.goToButton.Click += new System.EventHandler(this.goToButton_Click);
      // 
      // toolStripSeparator11
      // 
      this.toolStripSeparator11.Name = "toolStripSeparator11";
      resources.ApplyResources(this.toolStripSeparator11, "toolStripSeparator11");
      // 
      // ganttChartButton
      // 
      this.ganttChartButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(this.ganttChartButton, "ganttChartButton");
      this.ganttChartButton.Name = "ganttChartButton";
      this.ganttChartButton.Click += new System.EventHandler(this.ganttChartButton_Click);
      // 
      // ConfigureButton
      // 
      this.ConfigureButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      resources.ApplyResources(this.ConfigureButton, "ConfigureButton");
      this.ConfigureButton.Name = "ConfigureButton";
      this.ConfigureButton.Click += new System.EventHandler(this.configurationButton_click);
      // 
      // toolStripSeparator12
      // 
      this.toolStripSeparator12.Name = "toolStripSeparator12";
      resources.ApplyResources(this.toolStripSeparator12, "toolStripSeparator12");
      // 
      // ruleSetComboBox
      // 
      this.ruleSetComboBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
      this.ruleSetComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.ruleSetComboBox.Name = "ruleSetComboBox";
      resources.ApplyResources(this.ruleSetComboBox, "ruleSetComboBox");
      this.ruleSetComboBox.SelectedIndexChanged += new System.EventHandler(this.ruleSetComboBox_SelectedIndexChanged);
      // 
      // backgroundWorker
      // 
      this.backgroundWorker.WorkerReportsProgress = true;
      this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
      this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
      this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
      // 
      // dataGridViewTitleColumn1
      // 
      this.dataGridViewTitleColumn1.DataPropertyName = "Title";
      resources.ApplyResources(this.dataGridViewTitleColumn1, "dataGridViewTitleColumn1");
      this.dataGridViewTitleColumn1.Name = "dataGridViewTitleColumn1";
      this.dataGridViewTitleColumn1.ReadOnly = true;
      // 
      // dataGridViewTextBoxColumn1
      // 
      this.dataGridViewTextBoxColumn1.DataPropertyName = "URL";
      resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
      this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
      this.dataGridViewTextBoxColumn1.ReadOnly = true;
      // 
      // menuStrip1
      // 
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.navigateToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.goToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.designToolsToolStripMenuItem,
            this.debugToolStripMenuItem});
      resources.ApplyResources(this.menuStrip1, "menuStrip1");
      this.menuStrip1.Name = "menuStrip1";
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newWindowToolStripMenuItem,
            this.newArticleToolStripMenuItem,
            this.newTabToolStripMenuItem,
            this.openLocationToolStripMenuItem,
            this.closeToolStripMenuItem,
            this.toolStripSeparator5,
            this.synchronizeToolStripMenuItem,
            this.toolStripMenuItem2,
            this.savePageAsToolStripMenuItem,
            this.sendLinkToolStripMenuItem,
            this.toolStripMenuItem13,
            this.pageSetupToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.printToolStripMenuItem,
            this.toolStripSeparator6,
            this.exitToolStripMenuItem});
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
      // 
      // newWindowToolStripMenuItem
      // 
      this.newWindowToolStripMenuItem.Name = "newWindowToolStripMenuItem";
      resources.ApplyResources(this.newWindowToolStripMenuItem, "newWindowToolStripMenuItem");
      this.newWindowToolStripMenuItem.Click += new System.EventHandler(this.newWindowToolStripMenuItem_Click);
      // 
      // newArticleToolStripMenuItem
      // 
      resources.ApplyResources(this.newArticleToolStripMenuItem, "newArticleToolStripMenuItem");
      this.newArticleToolStripMenuItem.Name = "newArticleToolStripMenuItem";
      this.newArticleToolStripMenuItem.Click += new System.EventHandler(this.newArticleButton_Click);
      // 
      // newTabToolStripMenuItem
      // 
      this.newTabToolStripMenuItem.Name = "newTabToolStripMenuItem";
      resources.ApplyResources(this.newTabToolStripMenuItem, "newTabToolStripMenuItem");
      // 
      // openLocationToolStripMenuItem
      // 
      this.openLocationToolStripMenuItem.Name = "openLocationToolStripMenuItem";
      resources.ApplyResources(this.openLocationToolStripMenuItem, "openLocationToolStripMenuItem");
      // 
      // closeToolStripMenuItem
      // 
      this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
      resources.ApplyResources(this.closeToolStripMenuItem, "closeToolStripMenuItem");
      // 
      // toolStripSeparator5
      // 
      this.toolStripSeparator5.Name = "toolStripSeparator5";
      resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
      // 
      // synchronizeToolStripMenuItem
      // 
      this.synchronizeToolStripMenuItem.Name = "synchronizeToolStripMenuItem";
      resources.ApplyResources(this.synchronizeToolStripMenuItem, "synchronizeToolStripMenuItem");
      this.synchronizeToolStripMenuItem.Click += new System.EventHandler(this.synchronizeToolStripMenuItem_Click);
      // 
      // toolStripMenuItem2
      // 
      this.toolStripMenuItem2.Name = "toolStripMenuItem2";
      resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
      // 
      // savePageAsToolStripMenuItem
      // 
      this.savePageAsToolStripMenuItem.Name = "savePageAsToolStripMenuItem";
      resources.ApplyResources(this.savePageAsToolStripMenuItem, "savePageAsToolStripMenuItem");
      this.savePageAsToolStripMenuItem.Click += new System.EventHandler(this.savePageAsToolStripMenuItem_Click);
      // 
      // sendLinkToolStripMenuItem
      // 
      this.sendLinkToolStripMenuItem.Name = "sendLinkToolStripMenuItem";
      resources.ApplyResources(this.sendLinkToolStripMenuItem, "sendLinkToolStripMenuItem");
      this.sendLinkToolStripMenuItem.Click += new System.EventHandler(this.sendLinkToolStripMenuItem_Click);
      // 
      // toolStripMenuItem13
      // 
      this.toolStripMenuItem13.Name = "toolStripMenuItem13";
      resources.ApplyResources(this.toolStripMenuItem13, "toolStripMenuItem13");
      // 
      // pageSetupToolStripMenuItem
      // 
      this.pageSetupToolStripMenuItem.Name = "pageSetupToolStripMenuItem";
      resources.ApplyResources(this.pageSetupToolStripMenuItem, "pageSetupToolStripMenuItem");
      this.pageSetupToolStripMenuItem.Click += new System.EventHandler(this.pageSetupToolStripMenuItem_Click);
      // 
      // printPreviewToolStripMenuItem
      // 
      this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
      resources.ApplyResources(this.printPreviewToolStripMenuItem, "printPreviewToolStripMenuItem");
      this.printPreviewToolStripMenuItem.Click += new System.EventHandler(this.printPreviewToolStripMenuItem_Click);
      // 
      // printToolStripMenuItem
      // 
      this.printToolStripMenuItem.Name = "printToolStripMenuItem";
      resources.ApplyResources(this.printToolStripMenuItem, "printToolStripMenuItem");
      this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
      // 
      // toolStripSeparator6
      // 
      this.toolStripSeparator6.Name = "toolStripSeparator6";
      resources.ApplyResources(this.toolStripSeparator6, "toolStripSeparator6");
      // 
      // exitToolStripMenuItem
      // 
      this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
      resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
      this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
      // 
      // navigateToolStripMenuItem
      // 
      this.navigateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripMenuItem8,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem1,
            this.toolStripMenuItem9,
            this.selectAllToolStripMenuItem,
            this.toolStripMenuItem10,
            this.findToolStripMenuItem,
            this.findNextToolStripMenuItem,
            this.toolStripMenuItem11,
            this.searchToolStripMenuItem,
            this.advancedSearchToolStripMenuItem});
      this.navigateToolStripMenuItem.Name = "navigateToolStripMenuItem";
      resources.ApplyResources(this.navigateToolStripMenuItem, "navigateToolStripMenuItem");
      // 
      // undoToolStripMenuItem
      // 
      this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
      resources.ApplyResources(this.undoToolStripMenuItem, "undoToolStripMenuItem");
      // 
      // redoToolStripMenuItem
      // 
      this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
      resources.ApplyResources(this.redoToolStripMenuItem, "redoToolStripMenuItem");
      // 
      // toolStripMenuItem8
      // 
      this.toolStripMenuItem8.Name = "toolStripMenuItem8";
      resources.ApplyResources(this.toolStripMenuItem8, "toolStripMenuItem8");
      // 
      // cutToolStripMenuItem
      // 
      this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
      resources.ApplyResources(this.cutToolStripMenuItem, "cutToolStripMenuItem");
      this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
      // 
      // copyToolStripMenuItem
      // 
      this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
      resources.ApplyResources(this.copyToolStripMenuItem, "copyToolStripMenuItem");
      this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
      // 
      // pasteToolStripMenuItem
      // 
      this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
      resources.ApplyResources(this.pasteToolStripMenuItem, "pasteToolStripMenuItem");
      this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
      // 
      // deleteToolStripMenuItem1
      // 
      this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
      resources.ApplyResources(this.deleteToolStripMenuItem1, "deleteToolStripMenuItem1");
      this.deleteToolStripMenuItem1.Click += new System.EventHandler(this.deleteToolStripMenuItem1_Click);
      // 
      // toolStripMenuItem9
      // 
      this.toolStripMenuItem9.Name = "toolStripMenuItem9";
      resources.ApplyResources(this.toolStripMenuItem9, "toolStripMenuItem9");
      // 
      // selectAllToolStripMenuItem
      // 
      this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
      resources.ApplyResources(this.selectAllToolStripMenuItem, "selectAllToolStripMenuItem");
      this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
      // 
      // toolStripMenuItem10
      // 
      this.toolStripMenuItem10.Name = "toolStripMenuItem10";
      resources.ApplyResources(this.toolStripMenuItem10, "toolStripMenuItem10");
      // 
      // findToolStripMenuItem
      // 
      this.findToolStripMenuItem.Name = "findToolStripMenuItem";
      resources.ApplyResources(this.findToolStripMenuItem, "findToolStripMenuItem");
      this.findToolStripMenuItem.Click += new System.EventHandler(this.findToolStripMenuItem_Click);
      // 
      // findNextToolStripMenuItem
      // 
      this.findNextToolStripMenuItem.Name = "findNextToolStripMenuItem";
      resources.ApplyResources(this.findNextToolStripMenuItem, "findNextToolStripMenuItem");
      this.findNextToolStripMenuItem.Click += new System.EventHandler(this.findNextToolStripMenuItem_Click);
      // 
      // toolStripMenuItem11
      // 
      this.toolStripMenuItem11.Name = "toolStripMenuItem11";
      resources.ApplyResources(this.toolStripMenuItem11, "toolStripMenuItem11");
      // 
      // searchToolStripMenuItem
      // 
      resources.ApplyResources(this.searchToolStripMenuItem, "searchToolStripMenuItem");
      this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
      this.searchToolStripMenuItem.Click += new System.EventHandler(this.findButton_Click);
      // 
      // advancedSearchToolStripMenuItem
      // 
      resources.ApplyResources(this.advancedSearchToolStripMenuItem, "advancedSearchToolStripMenuItem");
      this.advancedSearchToolStripMenuItem.Name = "advancedSearchToolStripMenuItem";
      this.advancedSearchToolStripMenuItem.Click += new System.EventHandler(this.findAdvancedButton_Click);
      // 
      // viewToolStripMenuItem
      // 
      this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goToAddressBarToolStripMenuItem});
      this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
      resources.ApplyResources(this.viewToolStripMenuItem, "viewToolStripMenuItem");
      // 
      // goToAddressBarToolStripMenuItem
      // 
      this.goToAddressBarToolStripMenuItem.Name = "goToAddressBarToolStripMenuItem";
      resources.ApplyResources(this.goToAddressBarToolStripMenuItem, "goToAddressBarToolStripMenuItem");
      this.goToAddressBarToolStripMenuItem.Click += new System.EventHandler(this.goToAddressBarToolStripMenuItem_Click);
      // 
      // goToolStripMenuItem
      // 
      this.goToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.navigateBackToolStripMenuItem,
            this.navigateForwardToolStripMenuItem,
            this.homeToolStripMenuItem,
            this.goToLocationToolStripMenuItem,
            this.toolStripMenuItem7,
            this.nextUnreadToolStripMenuItem,
            this.historyToolStripMenuItem});
      this.goToolStripMenuItem.Name = "goToolStripMenuItem";
      resources.ApplyResources(this.goToolStripMenuItem, "goToolStripMenuItem");
      // 
      // navigateBackToolStripMenuItem
      // 
      resources.ApplyResources(this.navigateBackToolStripMenuItem, "navigateBackToolStripMenuItem");
      this.navigateBackToolStripMenuItem.Name = "navigateBackToolStripMenuItem";
      this.navigateBackToolStripMenuItem.Click += new System.EventHandler(this.navigateBackToolStripMenuItem_Click);
      // 
      // navigateForwardToolStripMenuItem
      // 
      resources.ApplyResources(this.navigateForwardToolStripMenuItem, "navigateForwardToolStripMenuItem");
      this.navigateForwardToolStripMenuItem.Name = "navigateForwardToolStripMenuItem";
      this.navigateForwardToolStripMenuItem.Click += new System.EventHandler(this.navigateForwardToolStripMenuItem_Click);
      // 
      // homeToolStripMenuItem
      // 
      this.homeToolStripMenuItem.Name = "homeToolStripMenuItem";
      resources.ApplyResources(this.homeToolStripMenuItem, "homeToolStripMenuItem");
      this.homeToolStripMenuItem.Click += new System.EventHandler(this.homeToolStripMenuItem_Click);
      // 
      // goToLocationToolStripMenuItem
      // 
      resources.ApplyResources(this.goToLocationToolStripMenuItem, "goToLocationToolStripMenuItem");
      this.goToLocationToolStripMenuItem.Name = "goToLocationToolStripMenuItem";
      this.goToLocationToolStripMenuItem.Click += new System.EventHandler(this.goToLocationToolStripMenuItem_Click);
      // 
      // toolStripMenuItem7
      // 
      this.toolStripMenuItem7.Name = "toolStripMenuItem7";
      resources.ApplyResources(this.toolStripMenuItem7, "toolStripMenuItem7");
      // 
      // nextUnreadToolStripMenuItem
      // 
      resources.ApplyResources(this.nextUnreadToolStripMenuItem, "nextUnreadToolStripMenuItem");
      this.nextUnreadToolStripMenuItem.Name = "nextUnreadToolStripMenuItem";
      this.nextUnreadToolStripMenuItem.Click += new System.EventHandler(this.nextUnreadToolStripMenuItem_Click);
      // 
      // historyToolStripMenuItem
      // 
      this.historyToolStripMenuItem.Name = "historyToolStripMenuItem";
      resources.ApplyResources(this.historyToolStripMenuItem, "historyToolStripMenuItem");
      // 
      // toolsToolStripMenuItem
      // 
      this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ruleSetsToolStripMenuItem,
            this.toolStripMenuItem5,
            this.importFromNGToolStripMenuItem,
            this.importFromWikiToolStripMenuItem,
            this.toolStripMenuItem6,
            this.accountSettingsToolStripMenuItem,
            this.configToolStripMenuItem,
            this.optionsToolStripMenuItem,
            this.toolStripMenuItem12});
      this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
      resources.ApplyResources(this.toolsToolStripMenuItem, "toolsToolStripMenuItem");
      // 
      // ruleSetsToolStripMenuItem
      // 
      this.ruleSetsToolStripMenuItem.Name = "ruleSetsToolStripMenuItem";
      resources.ApplyResources(this.ruleSetsToolStripMenuItem, "ruleSetsToolStripMenuItem");
      // 
      // toolStripMenuItem5
      // 
      this.toolStripMenuItem5.Name = "toolStripMenuItem5";
      resources.ApplyResources(this.toolStripMenuItem5, "toolStripMenuItem5");
      // 
      // importFromNGToolStripMenuItem
      // 
      this.importFromNGToolStripMenuItem.Name = "importFromNGToolStripMenuItem";
      resources.ApplyResources(this.importFromNGToolStripMenuItem, "importFromNGToolStripMenuItem");
      this.importFromNGToolStripMenuItem.Click += new System.EventHandler(this.importFromNGToolStripMenuItem_Click);
      // 
      // importFromWikiToolStripMenuItem
      // 
      this.importFromWikiToolStripMenuItem.Name = "importFromWikiToolStripMenuItem";
      resources.ApplyResources(this.importFromWikiToolStripMenuItem, "importFromWikiToolStripMenuItem");
      this.importFromWikiToolStripMenuItem.Click += new System.EventHandler(this.importFromWikiToolStripMenuItem_Click);
      // 
      // toolStripMenuItem6
      // 
      this.toolStripMenuItem6.Name = "toolStripMenuItem6";
      resources.ApplyResources(this.toolStripMenuItem6, "toolStripMenuItem6");
      // 
      // accountSettingsToolStripMenuItem
      // 
      this.accountSettingsToolStripMenuItem.Name = "accountSettingsToolStripMenuItem";
      resources.ApplyResources(this.accountSettingsToolStripMenuItem, "accountSettingsToolStripMenuItem");
      // 
      // configToolStripMenuItem
      // 
      this.configToolStripMenuItem.Name = "configToolStripMenuItem";
      resources.ApplyResources(this.configToolStripMenuItem, "configToolStripMenuItem");
      this.configToolStripMenuItem.Click += new System.EventHandler(this.configToolStripMenuItem_Click);
      // 
      // optionsToolStripMenuItem
      // 
      this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
      resources.ApplyResources(this.optionsToolStripMenuItem, "optionsToolStripMenuItem");
      this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
      // 
      // toolStripMenuItem12
      // 
      this.toolStripMenuItem12.Name = "toolStripMenuItem12";
      resources.ApplyResources(this.toolStripMenuItem12, "toolStripMenuItem12");
      // 
      // helpToolStripMenuItem
      // 
      this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.versionHistoryToolStripMenuItem,
            this.toolStripMenuItem4,
            this.checkForUpdateToolStripMenuItem,
            this.toolStripMenuItem3,
            this.aboutColaboToolStripMenuItem});
      this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
      resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
      // 
      // helpToolStripMenuItem1
      // 
      this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
      resources.ApplyResources(this.helpToolStripMenuItem1, "helpToolStripMenuItem1");
      // 
      // versionHistoryToolStripMenuItem
      // 
      this.versionHistoryToolStripMenuItem.Name = "versionHistoryToolStripMenuItem";
      resources.ApplyResources(this.versionHistoryToolStripMenuItem, "versionHistoryToolStripMenuItem");
      // 
      // toolStripMenuItem4
      // 
      this.toolStripMenuItem4.Name = "toolStripMenuItem4";
      resources.ApplyResources(this.toolStripMenuItem4, "toolStripMenuItem4");
      // 
      // checkForUpdateToolStripMenuItem
      // 
      this.checkForUpdateToolStripMenuItem.Name = "checkForUpdateToolStripMenuItem";
      resources.ApplyResources(this.checkForUpdateToolStripMenuItem, "checkForUpdateToolStripMenuItem");
      this.checkForUpdateToolStripMenuItem.Click += new System.EventHandler(this.checkForUpdateToolStripMenuItem_Click);
      // 
      // toolStripMenuItem3
      // 
      this.toolStripMenuItem3.Name = "toolStripMenuItem3";
      resources.ApplyResources(this.toolStripMenuItem3, "toolStripMenuItem3");
      // 
      // aboutColaboToolStripMenuItem
      // 
      this.aboutColaboToolStripMenuItem.Name = "aboutColaboToolStripMenuItem";
      resources.ApplyResources(this.aboutColaboToolStripMenuItem, "aboutColaboToolStripMenuItem");
      this.aboutColaboToolStripMenuItem.Click += new System.EventHandler(this.aboutColaboToolStripMenuItem_Click);
      // 
      // designToolsToolStripMenuItem
      // 
      this.designToolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkFulltextIndicesToolStripMenuItem,
            this.SVNBenchmarkToolStripMenuItem});
      this.designToolsToolStripMenuItem.Name = "designToolsToolStripMenuItem";
      resources.ApplyResources(this.designToolsToolStripMenuItem, "designToolsToolStripMenuItem");
      // 
      // checkFulltextIndicesToolStripMenuItem
      // 
      this.checkFulltextIndicesToolStripMenuItem.Name = "checkFulltextIndicesToolStripMenuItem";
      resources.ApplyResources(this.checkFulltextIndicesToolStripMenuItem, "checkFulltextIndicesToolStripMenuItem");
      this.checkFulltextIndicesToolStripMenuItem.Click += new System.EventHandler(this.checkFulltextIndicesToolStripMenuItem_Click);
      // 
      // SVNBenchmarkToolStripMenuItem
      // 
      this.SVNBenchmarkToolStripMenuItem.Name = "SVNBenchmarkToolStripMenuItem";
      resources.ApplyResources(this.SVNBenchmarkToolStripMenuItem, "SVNBenchmarkToolStripMenuItem");
      this.SVNBenchmarkToolStripMenuItem.Click += new System.EventHandler(this.SVNBenchmarkToolStripMenuItem_Click);
      // 
      // debugToolStripMenuItem
      // 
      this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.test1ToolStripMenuItem,
            this.test2ToolStripMenuItem});
      this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
      resources.ApplyResources(this.debugToolStripMenuItem, "debugToolStripMenuItem");
      // 
      // test1ToolStripMenuItem
      // 
      this.test1ToolStripMenuItem.Name = "test1ToolStripMenuItem";
      resources.ApplyResources(this.test1ToolStripMenuItem, "test1ToolStripMenuItem");
      this.test1ToolStripMenuItem.Click += new System.EventHandler(this.test1ToolStripMenuItem_Click);
      // 
      // test2ToolStripMenuItem
      // 
      this.test2ToolStripMenuItem.Name = "test2ToolStripMenuItem";
      resources.ApplyResources(this.test2ToolStripMenuItem, "test2ToolStripMenuItem");
      this.test2ToolStripMenuItem.Click += new System.EventHandler(this.test2ToolStripMenuItem_Click);
      // 
      // statusStrip1
      // 
      this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.taskState,
            this.taskProgress,
            this.queueState});
      resources.ApplyResources(this.statusStrip1, "statusStrip1");
      this.statusStrip1.Name = "statusStrip1";
      // 
      // taskState
      // 
      resources.ApplyResources(this.taskState, "taskState");
      this.taskState.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
      this.taskState.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
      this.taskState.Name = "taskState";
      // 
      // taskProgress
      // 
      this.taskProgress.Name = "taskProgress";
      resources.ApplyResources(this.taskProgress, "taskProgress");
      this.taskProgress.Value = 100;
      // 
      // queueState
      // 
      resources.ApplyResources(this.queueState, "queueState");
      this.queueState.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
      this.queueState.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
      this.queueState.Name = "queueState";
      // 
      // contextMenuIM
      // 
      this.contextMenuIM.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startChatMenuItem,
            this.startPublicChatMenuItem,
            this.setLocationNameMenuItem});
      this.contextMenuIM.Name = "contextMenuIM";
      resources.ApplyResources(this.contextMenuIM, "contextMenuIM");
      // 
      // startChatMenuItem
      // 
      this.startChatMenuItem.Name = "startChatMenuItem";
      resources.ApplyResources(this.startChatMenuItem, "startChatMenuItem");
      this.startChatMenuItem.Click += new System.EventHandler(this.startChatMenuItem_Click);
      // 
      // startPublicChatMenuItem
      // 
      this.startPublicChatMenuItem.Name = "startPublicChatMenuItem";
      resources.ApplyResources(this.startPublicChatMenuItem, "startPublicChatMenuItem");
      this.startPublicChatMenuItem.Click += new System.EventHandler(this.startPublicChatMenuItem_Click);
      // 
      // setLocationNameMenuItem
      // 
      this.setLocationNameMenuItem.Name = "setLocationNameMenuItem";
      resources.ApplyResources(this.setLocationNameMenuItem, "setLocationNameMenuItem");
      this.setLocationNameMenuItem.Click += new System.EventHandler(this.setLocationNameMenuItem_Click);
      // 
      // notifyIcon
      // 
      resources.ApplyResources(this.notifyIcon, "notifyIcon");
      this.notifyIcon.BalloonTipClicked += new System.EventHandler(this.notifyIcon_BalloonTipClicked);
      this.notifyIcon.Click += new System.EventHandler(this.notifyIcon_Click);
      this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
      this.notifyIcon.MouseMove += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseMove);
      // 
      // FormColabo
      // 
      resources.ApplyResources(this, "$this");
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.statusStrip1);
      this.Controls.Add(this.toolStrip1);
      this.Controls.Add(this.menuStrip1);
      this.Controls.Add(this.splitContainer1);
      this.KeyPreview = true;
      this.MainMenuStrip = this.menuStrip1;
      this.Name = "FormColabo";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormColabo_FormClosing);
      this.Load += new System.EventHandler(this.Form1_Load);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      this.splitContainer3.Panel1.ResumeLayout(false);
      this.splitContainer3.Panel2.ResumeLayout(false);
      this.splitContainer3.ResumeLayout(false);
      this.contextMenuTags.ResumeLayout(false);
      this.splitContainer4.Panel1.ResumeLayout(false);
      this.splitContainer4.Panel2.ResumeLayout(false);
      this.splitContainer4.ResumeLayout(false);
      this.splitContainer2.Panel1.ResumeLayout(false);
      this.splitContainer2.Panel2.ResumeLayout(false);
      this.splitContainer2.ResumeLayout(false);
      this.tabControl1.ResumeLayout(false);
      this.tabTasks.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      this.tabCalendar.ResumeLayout(false);
      this.contextMenuPreview.ResumeLayout(false);
      this.contextMenuBookmarks.ResumeLayout(false);
      this.contextMenuArticle.ResumeLayout(false);
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.statusStrip1.ResumeLayout(false);
      this.statusStrip1.PerformLayout();
      this.contextMenuIM.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
      private System.Windows.Forms.SplitContainer splitContainer2;
        private Colabo.ColaboWebBrowser webBrowser1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton newArticleButton;
        private System.Windows.Forms.ContextMenuStrip contextMenuArticle;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private FormColabo.DataGridViewTitleColumn dataGridViewTitleColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
      private System.Windows.Forms.WebBrowser tagsNavigator;
        private System.Windows.Forms.ToolStripMenuItem replyToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem markAsReadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem markAsUnreadToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton synchronizeButton;
      private System.Windows.Forms.ToolStripButton ConfigureButton;
      private FormColabo.DataGridViewColabo dataGridView1;
      private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
      private System.Windows.Forms.ContextMenuStrip contextMenuTags;
      private System.Windows.Forms.ToolStripMenuItem NOTTag;
      private System.Windows.Forms.ToolStripComboBox ruleSetComboBox;
      private System.ComponentModel.BackgroundWorker backgroundWorker;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
      private System.Windows.Forms.ToolStripMenuItem sVNLogToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem blameToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
      private System.Windows.Forms.ToolStripMenuItem copyColaboShortcutToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem appendNoteToolStripMenuItem;
      private System.Windows.Forms.ToolStripButton findButton;
      private System.Windows.Forms.ToolStripButton findAdvancedButton;
      private System.Windows.Forms.ToolStripButton goToButton;
      private System.Windows.Forms.MenuStrip menuStrip1;
      private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
      private System.Windows.Forms.ToolStripMenuItem newArticleToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem navigateToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem advancedSearchToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem designToolsToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
      private System.Windows.Forms.ToolStripMenuItem newWindowToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
      private System.Windows.Forms.StatusStrip statusStrip1;
      private System.Windows.Forms.ToolStripStatusLabel taskState;
      private System.Windows.Forms.ToolStripProgressBar taskProgress;
      private System.Windows.Forms.ToolStripStatusLabel queueState;
      private System.Windows.Forms.ToolStripMenuItem makeTagRead;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
      private System.Windows.Forms.ToolStripButton navigateBackButton;
      private System.Windows.Forms.ToolStripButton navigateForwardButton;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
      private System.Windows.Forms.ToolStripMenuItem checkFulltextIndicesToolStripMenuItem;
      private FormColabo.DataGridViewImagesColumn Picture;
      private FormColabo.DataGridViewTitleColumn Title;
      private System.Windows.Forms.DataGridViewTextBoxColumn Author;
      private System.Windows.Forms.DataGridViewTextBoxColumn Date;
      private System.Windows.Forms.ToolStripMenuItem renameTag;
      private System.Windows.Forms.ToolStripButton homeButton;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
      private System.Windows.Forms.ToolStripMenuItem setAsHomePageToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem SVNBenchmarkToolStripMenuItem;
      private System.Windows.Forms.SplitContainer splitContainer3;
      private System.Windows.Forms.TreeView bookmarks;
      private System.Windows.Forms.ContextMenuStrip contextMenuBookmarks;
      private System.Windows.Forms.ToolStripMenuItem openBookmarkToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem openBookmarkInNewWindowToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
      private System.Windows.Forms.ToolStripMenuItem editBookmarkToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem deleteBookmarkToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
      private System.Windows.Forms.ToolStripMenuItem addBookmarkToolStripMenuItem;
      public System.Windows.Forms.ImageList imageList1;
      private System.Windows.Forms.ToolStripMenuItem bookmarkThisArticleToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem newFolderToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
      private System.Windows.Forms.ToolStripButton ganttChartButton;
      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage tabTasks;
      private System.Windows.Forms.TabPage tabCalendar;
      private Calendar.DayView dayView;
      private System.Windows.Forms.MonthCalendar monthCalendar;
        private System.Windows.Forms.ToolStripMenuItem synchronizeToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem newTabToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem openLocationToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
      private System.Windows.Forms.ToolStripMenuItem savePageAsToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem sendLinkToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem checkForUpdateToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem ruleSetsToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem accountSettingsToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
      private System.Windows.Forms.ToolStripMenuItem versionHistoryToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
      private System.Windows.Forms.ToolStripMenuItem aboutColaboToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
      private System.Windows.Forms.ToolStripMenuItem importFromNGToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem importFromWikiToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
      private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem goToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem homeToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem navigateBackToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem navigateForwardToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem goToLocationToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem7;
      private System.Windows.Forms.ToolStripMenuItem nextUnreadToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem historyToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
      private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem findToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem findNextToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem8;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem9;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem10;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem11;
      private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem12;
      private System.Windows.Forms.ToolStripSeparator toolStripMenuItem13;
      private System.Windows.Forms.ToolStripMenuItem pageSetupToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem test1ToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem test2ToolStripMenuItem;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
      private System.Windows.Forms.ToolStripMenuItem goToAddressBarToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
      private System.Windows.Forms.SplitContainer splitContainer4;
      private System.Windows.Forms.ListBox imFilter;
      private System.Windows.Forms.ListView imUsers;
      private System.Windows.Forms.ColumnHeader userColumn;
      private System.Windows.Forms.ColumnHeader stateColumn;
      private System.Windows.Forms.ColumnHeader locationColumn;
      private System.Windows.Forms.ColumnHeader lastSeenColumn;
      private System.Windows.Forms.ComboBox imState;
      private System.Windows.Forms.ContextMenuStrip contextMenuIM;
      private System.Windows.Forms.ToolStripMenuItem setLocationNameMenuItem;
      private System.Windows.Forms.ToolStripMenuItem startChatMenuItem;
      private System.Windows.Forms.ToolStripMenuItem startPublicChatMenuItem;
      private System.Windows.Forms.ImageList presenceImages;
      private System.Windows.Forms.ContextMenuStrip contextMenuPreview;
      private System.Windows.Forms.ToolStripMenuItem copyPreviewMenu;
      private System.Windows.Forms.ToolStripMenuItem copyShortcutPreviewMenu;
      private System.Windows.Forms.ToolStripMenuItem replyPreviewMenu;
      private System.Windows.Forms.NotifyIcon notifyIcon;
      private System.Windows.Forms.ToolStripMenuItem appendPreviewMenuItem;
    }
}

