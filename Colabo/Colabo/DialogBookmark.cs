using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogBookmark : Form
  {
    FormColabo _owner;
    Bookmark _bookmark;

    public DialogBookmark()
    {
      _owner = null;
      InitializeComponent();
    }
    public DialogBookmark(FormColabo owner, Bookmark bookmark)
    {
      _owner = owner;
      _bookmark = bookmark;
      InitializeComponent();
    }
    private void DialogBookmark_Load(object sender, EventArgs e)
    {
      // valueName
      if (_bookmark != null) valueName.Text = _bookmark._name;

      // valueImage
      if (_owner != null)
      {
        int n = _owner.imageList1.Images.Count;
        for (int i = 0; i < n; i++) valueImage.Items.Add(_owner.imageList1.Images[i]);

        if (_bookmark != null) valueImage.SelectedIndex = _bookmark._image;
        else valueImage.SelectedIndex = 0;
      }

      // valueTags
      if (_bookmark != null) valueTags.Text = ArticleItem.WriteTags(_bookmark._tags);

      // valueArticle
      if (_bookmark != null && _bookmark._article._database != null) valueArticle.Text = _bookmark._database.GetArticleLink(_bookmark._article);
    }
    
    public Bookmark bookmark
    {
      get
      {
        if (_bookmark == null) _bookmark = new Bookmark(null);

        List<string> tags;
        SQLDatabase database;
        int id;

        Uri url = null;
        try
        {
          url = new Uri(valueArticle.Text);
        }
        catch (System.Exception)
        {
        }

        if (url != null && _owner.FindArticle(url, out tags, out database, out id))
        {
          _bookmark._article._database = database;
          _bookmark._article._id = id;

          if (database != _bookmark._database)
          {
            _bookmark._database = database;
            _bookmark._parent = -1; // parent from other database is no longer valid
          }
        }
        else _bookmark._article = ArticleId.None;

        _bookmark._name = valueName.Text;
        _bookmark._image = valueImage.SelectedIndex;
        ArticleItem.ReadTags(valueTags.Text, out _bookmark._tags);

        return _bookmark;
      }
    }

    private void valueImage_DrawItem(object sender, DrawItemEventArgs e)
    {
      e.DrawBackground();
      e.DrawFocusRectangle();

      if (e.Index >= 0)
      {
        Image image = (Image)valueImage.Items[e.Index];
        Point point = new Point((e.Bounds.Left + e.Bounds.Right - image.Width) / 2, (e.Bounds.Top + e.Bounds.Bottom - image.Height) / 2);
        e.Graphics.DrawImage(image, point);
      }
    }
  }
}