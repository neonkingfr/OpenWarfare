namespace Colabo
{
  partial class ImportWiki
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.Button startImport;
      this.username = new System.Windows.Forms.TextBox();
      this.password = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.wikiOutput = new System.Windows.Forms.TextBox();
      this.category = new System.Windows.Forms.TextBox();
      this.databasesList = new System.Windows.Forms.ListBox();
      this.label4 = new System.Windows.Forms.Label();
      this.importTypeCombo = new System.Windows.Forms.ComboBox();
      startImport = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // startImport
      // 
      startImport.Location = new System.Drawing.Point(12, 156);
      startImport.Name = "startImport";
      startImport.Size = new System.Drawing.Size(354, 23);
      startImport.TabIndex = 5;
      startImport.Text = "Start Import";
      startImport.UseVisualStyleBackColor = true;
      startImport.Click += new System.EventHandler(this.startImport_Click);
      // 
      // username
      // 
      this.username.Location = new System.Drawing.Point(80, 38);
      this.username.Name = "username";
      this.username.Size = new System.Drawing.Size(88, 20);
      this.username.TabIndex = 2;
      // 
      // password
      // 
      this.password.Location = new System.Drawing.Point(238, 38);
      this.password.Name = "password";
      this.password.PasswordChar = '*';
      this.password.Size = new System.Drawing.Size(128, 20);
      this.password.TabIndex = 3;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 41);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(60, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "User Name";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(179, 41);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(53, 13);
      this.label3.TabIndex = 7;
      this.label3.Text = "Password";
      // 
      // wikiOutput
      // 
      this.wikiOutput.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
      this.wikiOutput.Location = new System.Drawing.Point(15, 185);
      this.wikiOutput.Multiline = true;
      this.wikiOutput.Name = "wikiOutput";
      this.wikiOutput.ReadOnly = true;
      this.wikiOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.wikiOutput.Size = new System.Drawing.Size(351, 184);
      this.wikiOutput.TabIndex = 9;
      // 
      // category
      // 
      this.category.Location = new System.Drawing.Point(91, 12);
      this.category.Name = "category";
      this.category.Size = new System.Drawing.Size(275, 20);
      this.category.TabIndex = 1;
      // 
      // databasesList
      // 
      this.databasesList.FormattingEnabled = true;
      this.databasesList.Location = new System.Drawing.Point(12, 81);
      this.databasesList.Name = "databasesList";
      this.databasesList.Size = new System.Drawing.Size(354, 69);
      this.databasesList.TabIndex = 4;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(12, 64);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(89, 13);
      this.label4.TabIndex = 8;
      this.label4.Text = "Select Database:";
      // 
      // importTypeCombo
      // 
      this.importTypeCombo.BackColor = System.Drawing.Color.White;
      this.importTypeCombo.DisplayMember = "0";
      this.importTypeCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.importTypeCombo.ForeColor = System.Drawing.SystemColors.WindowText;
      this.importTypeCombo.FormattingEnabled = true;
      this.importTypeCombo.Items.AddRange(new object[] {
            "Category",
            "Article"});
      this.importTypeCombo.Location = new System.Drawing.Point(12, 11);
      this.importTypeCombo.Name = "importTypeCombo";
      this.importTypeCombo.Size = new System.Drawing.Size(73, 21);
      this.importTypeCombo.TabIndex = 0;
      this.importTypeCombo.SelectedIndex = 0;
      // 
      // ImportWiki
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(380, 381);
      this.Controls.Add(this.importTypeCombo);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.databasesList);
      this.Controls.Add(this.category);
      this.Controls.Add(this.wikiOutput);
      this.Controls.Add(startImport);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.password);
      this.Controls.Add(this.username);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "ImportWiki";
      this.Text = "ImportWiki";
      this.Load += new System.EventHandler(this.ImportWiki_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox username;
    private System.Windows.Forms.TextBox password;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox wikiOutput;
    private System.Windows.Forms.TextBox category;
    private System.Windows.Forms.ListBox databasesList;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.ComboBox importTypeCombo;
  }
}