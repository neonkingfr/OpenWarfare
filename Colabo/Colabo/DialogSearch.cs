using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Colabo
{
  public partial class DialogSearch : Colabo.DialogSearchResults
  {
    public DialogSearch(FormColabo owner) : base(owner)
    {
      InitializeComponent();
      _originalTitle = this.Text;
    }
    public DialogSearch(FormColabo owner, string text) : base(owner)
    {
      InitializeComponent();
      _originalTitle = this.Text;
      value.Text = text;
      StartSearch();
    }

    // create a query string from the dialog controls
    public override string GetQuery()
    {
      if (string.IsNullOrEmpty(value.Text)) return null;
      return string.Format("title:({0}) OR content:({0}) OR author:({0})", value.Text);
    }

    // create a user readable query string (used in the dialog title)
    public override string GetQueryReadable()
    {
      return value.Text;
    }

    // helper to disable some controls when search is in progress
    protected override void EnableSearch(bool enable)
    {
      value.Enabled = enable;
    }

    private void buttonSearch_Click(object sender, EventArgs e)
    {
      StartSearch();
    }
    private void buttonCancel_Click(object sender, EventArgs e)
    {
      Close();
    }
  }
}
