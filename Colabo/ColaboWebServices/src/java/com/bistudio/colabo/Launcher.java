/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.colabo;

import java.util.logging.Level;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Helper class to enforce creation of AppState singleton on program startup
 * (registered as a Listener)
 * @author Jirka
 */
public class Launcher implements ServletContextListener
{
    public Launcher()
    {
        AppState.getInstance();
    }

    public void contextInitialized(ServletContextEvent sce)
    {
        // ensure that the threads are running
        AppState.getInstance().Start();
    }
    public void contextDestroyed(ServletContextEvent sce)
    {
        // stop the threads
        AppState.getInstance().Stop();
    }
}
