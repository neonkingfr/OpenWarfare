/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.colabo;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONTokener;
import sun.misc.BASE64Decoder;

/**
 * REST Web Service
 *
 * @author Jirka
 */

@Path("DB")
public class Database
{
    @Context
    private UriInfo context;

    public Database()
    {
    }

    @Path("UserInfo/{user}")
    @GET
    @Produces("application/json")
    public Response getUserInfo(@HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            JSONObject result = new JSONObject();

            PreparedStatement statement = connection.prepareStatement("SELECT FullName, Picture, Color FROM Users WHERE (ID = ?)");
            statement.setString(1, user);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                result.put("FullName", resultSet.getObject(1));
                result.put("Picture", resultSet.getObject(2));
                result.put("Color", resultSet.getObject(3));
            }
            resultSet.close();
            statement.close();

            String aliases = "";
            statement = connection.prepareStatement("SELECT Alias FROM UserAliases WHERE (ID = ?)");
            statement.setString(1, user);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              String alias = resultSet.getString(1);
              if (alias != null && !alias.isEmpty())
              {
                if (!aliases.isEmpty()) aliases += ", ";
                aliases += alias;
              }
            }
            result.put("Aliases", aliases);
            resultSet.close();
            statement.close();

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("UserInfo/{user}")
    @PUT
    @Consumes("application/json")
    public Response putUserInfo(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            String fullName = root.getString("FullName");
            String picture = root.getString("Picture");
            int color = root.getInt("Color");
            String aliases = root.getString("Aliases");

            connection = AppState.getConnection();

            // insert a new record if not exist yet
            PreparedStatement statement = connection.prepareStatement("INSERT IGNORE INTO Users (ID) VALUES (?)");
            statement.setString(1, user);
            statement.executeUpdate();
            statement.close();

            // update the basic values
            statement = connection.prepareStatement("UPDATE Users SET FullName = ?, Picture = ?, Color = ? WHERE (ID = ?)");
            statement.setString(1, fullName);
            statement.setString(2, picture);
            statement.setInt(3, color);
            statement.setString(4, user);
            statement.executeUpdate();
            statement.close();

            // remove the current aliases
            statement = connection.prepareStatement("DELETE FROM UserAliases WHERE (ID = ?)");
            statement.setString(1, user);
            statement.executeUpdate();
            statement.close();

            // insert the new aliases
            String[] array = aliases.split(",");
            if (array.length > 0)
            {
              statement = connection.prepareStatement("INSERT IGNORE INTO UserAliases (Alias, ID) VALUES (?, ?)");
              statement.setString(2, user);
              for (String alias : array)
              {
                statement.setString(1, alias.trim());
                statement.addBatch();
              }
              statement.executeBatch();
              statement.close();
            }

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("User/{user}")
    @PUT
    @Consumes("application/json")
    public Response putUser(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            String fullName = root.getString("FullName");
            String picture = root.getString("Picture");
            int color = root.getInt("Color");

            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("UPDATE Users SET FullName = ?, Picture = ?, Color = ? WHERE (ID = ?)");
            statement.setString(1, fullName);
            statement.setString(2, picture);
            statement.setInt(3, color);
            statement.setString(4, user);
            statement.executeUpdate();
            statement.close();

            return Response.ok().build();
        }
        catch (Exception exception)
        {
          return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    static boolean BetterState(String s1, String s2)
    {
        // order states by importance
        String[] stateOrder = {"Offline","Away","Invisible","DND","Online","Busy"};
        int order1 = Arrays.asList(stateOrder).indexOf(s1);
        int order2 = Arrays.asList(stateOrder).indexOf(s1);
        return order1>order2;
    }

    public void setPresence(String name, String state, String guid) {
        Connection connection = null;
        try
        {
            connection = AppState.getConnection(false);
            String[] stateOffline = {"Offline","Away","Invisible"};
            boolean setTime = true;
            if (Arrays.asList(stateOffline).indexOf(state)>=0)
            {
              PreparedStatement statement = connection.prepareStatement("SELECT state, time FROM Presence WHERE (ID = ? AND stationGUID=?)");
              int st = 1;
              statement.setString(st++, name);
              statement.setString(st++, guid);

              ResultSet resultSet = statement.executeQuery();
              if (resultSet.next()) {
                  String oldState = resultSet.getString(1);
                  Timestamp time = resultSet.getTimestamp(2);
                  // we want to set time when going offline, or when previous data are not valid
                  setTime = oldState==null || time==null || Arrays.asList(stateOffline).indexOf(oldState)<0;
              }
              resultSet.close();
              statement.close();
            }
            if (!setTime)
            {
              {
                PreparedStatement statement = connection.prepareStatement("INSERT INTO Presence (stationGUID, Id, State) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE State = ?");
                int st = 1;
                statement.setString(st++, guid);
                statement.setString(st++, name);
                statement.setString(st++, state);

                statement.setString(st++, state);

                statement.executeUpdate();
                statement.close();
              }
            }
            else
            {
              PreparedStatement statement = connection.prepareStatement("INSERT INTO Presence (stationGUID, Id, State, Time) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE State = ?, Time = ?");
              Timestamp now = new Timestamp(System.currentTimeMillis());
              int st = 1;
              statement.setString(st++, guid);
              statement.setString(st++, name);
              statement.setString(st++, state);
              statement.setTimestamp(st++, now);

              statement.setString(st++, state);
              statement.setTimestamp(st++, now);

              statement.executeUpdate();
              statement.close();
            }
            connection.commit();
            AppState.WakeUpLongPoll();
        }
        catch (Exception exception)
        {
          AppState.logger().log(Level.WARNING, "Set presence failed", exception);
          rollback(connection);
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    public void setLocation(Connection connection, String name, String guid, String location) {
        try
        {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO Presence (stationGUID,Id,location) VALUES (?,?,?) ON DUPLICATE KEY UPDATE location=?");
            int st = 1;
            statement.setString(st++, guid);
            statement.setString(st++, name);
            statement.setString(st++, location);

            statement.setString(st++, location);
            
            statement.executeUpdate();
            statement.close();
        }
        catch (Exception exception)
        {
           AppState.logger().log(Level.WARNING, "Set presence failed", exception);
            
        }
    }

  private void AddConversationUsers(Connection connection, int id, ArrayList<String> userList, String user) throws SQLException {
{
        // create a list of participating users
        PreparedStatement statement = connection.prepareStatement(
                "INSERT IGNORE INTO ConversationUser (conversationId,userName,revRead,state,stateTime,stateRev) VALUES (?,?,0,'Invited',?,1)"
        );
        Timestamp now = new Timestamp(System.currentTimeMillis());
        int st = 1;
        statement.setInt(st++, id);
        int userSt = st++;
        statement.setTimestamp(st++, now);
        boolean userAdded = false;
        for (String chatUser:userList)
        {
            if (chatUser.equals(user)) userAdded = true;
            statement.setString(userSt, chatUser);
            statement.executeUpdate();
        }
        if (!userAdded)
        {
          statement.setString(userSt, user);
          statement.executeUpdate();
        }
        statement.close();
    }
  }

    private int ConversationRevision(Connection connection, int chatId) throws SQLException {
        int revision = 0;

        PreparedStatement statement = connection.prepareStatement("SELECT Revision FROM Conversation WHERE (id=?)");
        int st = 1;
        statement.setInt(st++, chatId);

        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            revision = resultSet.getInt(1);
        }
        resultSet.close();
        statement.close();
        return revision;
    }

  private ArrayList<String> ParseUserList(JSONArray users) throws JSONException {
    ArrayList<String> userList = new ArrayList<String>();
    for(int i = 0; i < users.length(); i++)
    {
        userList.add(users.getString(i));
    }
    return userList;
  }

    private void SetChatState(Connection connection, int revision, String state, Timestamp now, Integer chatId, String user) throws SQLException {
        // record which revision was read
        {
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE ConversationUser SET revRead=?,state=?,stateTime=?,stateRev=? WHERE (conversationId=? AND userName=?)");
            int st = 1;
            statement.setInt(st++, revision);
            statement.setString(st++, state);
            statement.setTimestamp(st++, now);
            statement.setInt(st++, revision);

            statement.setInt(st++, chatId);
            statement.setString(st++, user);
            statement.executeUpdate();
        }
    }

  private int chatRevision(Connection connection, Integer chatId) throws SQLException, JSONException {
    int revision = 0;
    {
        PreparedStatement statement = connection.prepareStatement(
                "SELECT revision FROM Conversation WHERE (id=?)");
        int st = 1;
        statement.setInt(st++, chatId);

        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            revision = resultSet.getInt(1);
        }
        resultSet.close();
        statement.close();
    }
    return revision;
  }

    private boolean checkChatAuthorization(Connection connection, int chatId, AuthorizationInfo authInfo) throws SQLException
    {
        boolean enabled = false;
        {
            PreparedStatement statement = connection.prepareStatement("SELECT Tags FROM Conversation WHERE Id=(?)");
            statement.setInt(1, chatId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                String tagsString = resultSet.getString(1);
                String[] tags = tagsString.split(",");
                enabled = true;
                for(String tag:tags)
                {
                    if (tag.equalsIgnoreCase("/private"))
                    {
                        enabled = false;
                    }
                }
            }
            resultSet.close();
            statement.close();
            // if not private, it can be accessed
            if (enabled) return true;
        }
        {
            PreparedStatement statement = connection.prepareStatement("SELECT ConversationId FROM ConversationUser WHERE ConversationId=(?) AND userName=(?)");
            statement.setInt(1, chatId);
            statement.setString(2, authInfo.name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                enabled = true;
            }
            resultSet.close();
            statement.close();
        }
        
        if (!connection.getAutoCommit()) connection.commit();
        return enabled;
    }

    private List<Integer> chatListForUser(Connection connection, AuthorizationInfo authInfo) throws SQLException
    {
        List<Integer> chats = new ArrayList<Integer>();
        PreparedStatement statement = connection.prepareStatement("SELECT ID,TAGS FROM Conversation WHERE NOT (Tags LIKE ?) OR (Tags LIKE ?)");
        statement.setString(1, "%/private,%");
        statement.setString(2, "%@" + authInfo.name + "%");
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next())
        {
          int id = resultSet.getInt(1);
          // check user access rights, query was conservative estimate only
          if (checkChatAuthorization(connection,id,authInfo))
          {
            chats.add(id);
          }
        }
        resultSet.close();
        return chats;
    }

    private int getLocationMaxId(Connection connection, String user) throws SQLException {
        // check what name is free
        int maxId = 0;
        {
            PreparedStatement statement = connection.prepareStatement("SELECT autoName FROM LocationNames WHERE (autoName LIKE ?)");
            statement.setString(1, user + "%");

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString(1);
                if (name.startsWith(user)) {
                    try {
                        int id = Integer.parseInt(name.substring(user.length()));
                        if (id >= maxId) {
                            maxId = id;
                        }

                    } catch (NumberFormatException e) {
                    }
                }
            }
            statement.close();
        }
        return maxId;
    }

    private JSONObject getUserList(Connection connection) throws SQLException, NamingException, JSONException {
        Statement statement = connection.createStatement();
        JSONObject result = new JSONObject();
        // users
        ResultSet resultSet = statement.executeQuery("SELECT ID, FullName, Picture, Color FROM Users");
        JSONArray array = new JSONArray();
        result.put("Users", array);
        while (resultSet.next())
        {
          String id = resultSet.getObject(1).toString();
          
          JSONObject userInfo = new JSONObject();
          array.put(userInfo);

          userInfo.put("ID", resultSet.getObject(1));
          userInfo.put("FullName", resultSet.getObject(2));
          userInfo.put("Picture", resultSet.getObject(3));
          userInfo.put("Color", resultSet.getObject(4));
          reportUserPresence(id, userInfo);
        }
        resultSet.close();
        // aliases
        resultSet = statement.executeQuery("SELECT Alias, ID FROM UserAliases");
        array = new JSONArray();
        result.put("Aliases", array);
        while (resultSet.next())
        {
          JSONObject userInfo = new JSONObject();
          array.put(userInfo);

          userInfo.put("Alias", resultSet.getObject(1));
          userInfo.put("ID", resultSet.getObject(2));
        }
        resultSet.close();
        statement.close();
        return result;
    }

  private JSONArray ConversationUserState(Connection connection, int chatId) throws SQLException, JSONException
  {
    JSONArray users = new JSONArray();

    PreparedStatement statement = connection.prepareStatement("SELECT userName,state,stateTime,stateRev FROM ConversationUser WHERE (conversationId=?)");
    int st = 1;
    statement.setInt(st++, chatId);

    ResultSet resultSet = statement.executeQuery();
    while (resultSet.next()) {
      JSONObject user = new JSONObject();
      int rs = 1;
      user.put("user",resultSet.getString(rs++));
      user.put("state",resultSet.getString(rs++));
      user.put("stateTime",resultSet.getTimestamp(rs++).toString());
      user.putOpt("stateRev",resultSet.getInt(rs++));
      users.put(user);
    }
    resultSet.close();
    statement.close();
    return users;
  }

  private void rollback(Connection connection) {
    try {
        if (connection != null) {
            connection.rollback();
        }
    } catch (SQLException ex) {
    }
  }

    private void sendChatReply(Connection connection, int chatId, String user, String reply) throws SQLException {
        int revision = 0;
        { // get current document content
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT Revision FROM Conversation WHERE (id=?)");
            int st = 1;
            statement.setInt(st++, chatId);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                revision = resultSet.getInt(1);
            }
            resultSet.close();
            statement.close();
        }

        int parts = 0;
        { // get current document parts
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT part FROM ConversationParts WHERE (id=?)");
            int st = 1;
            statement.setInt(st++, chatId);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                int part = resultSet.getInt(1);
                if (part>parts) parts = part;
            }
            resultSet.close();
            statement.close();
        }
        
        // create a new part
        if (revision<parts) revision = parts;
        Timestamp now = new Timestamp(System.currentTimeMillis());
        
        {
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO ConversationParts (id,part,user,text,time) VALUES (?,?,?,?,?)");


            int st = 1;
            statement.setInt(st++, chatId);
            statement.setInt(st++, revision+1);
            statement.setString(st++, user);
            statement.setString(st++, reply);
            statement.setTimestamp(st++, now);
            
            statement.executeUpdate();
            statement.close();
        }
        { // update the document revision
            PreparedStatement statement = connection.prepareStatement(
                    "UPDATE Conversation SET Revision=? WHERE (id=?)");

            int st = 1;
            statement.setInt(st++, revision + 1);
            statement.setInt(st++, chatId);

            statement.executeUpdate();
            statement.close();
        }
        SetChatState(connection, revision+1, "Ready", now, chatId, user);
    }

  private String traceLocation(Connection connection, String user, String guid, String ipAddr) throws SQLException {
    // check if location is already named
    try {
      String autoName = null;
      {
        PreparedStatement locStatement = connection.prepareStatement("SELECT autoName FROM LocationNames WHERE (ipAddress = ?)");
        locStatement.setString(1, ipAddr);
        ResultSet locResultSet = locStatement.executeQuery();
        if (locResultSet.next()) {
          autoName = locResultSet.getString(1);
        }
      }
      if (autoName == null) {
        int maxId = getLocationMaxId(connection, user);

        autoName = user + Integer.toString(maxId + 1);

        {
          PreparedStatement statement = connection.prepareStatement(
                  "INSERT INTO LocationNames (ipAddress, autoName) VALUES (?, ?) ON DUPLICATE KEY UPDATE autoName = ?");
          statement.setString(1, ipAddr);
          statement.setString(2, autoName);
          statement.setString(3, autoName);

          statement.executeUpdate();
          statement.close();
        }
      } else {
        // name already exists
        String common = "common";
        if (!autoName.startsWith(user) && !autoName.startsWith(common))
        {
            int maxId = getLocationMaxId(connection, common);

            autoName = common + Integer.toString(maxId+1);
            PreparedStatement statement = connection.prepareStatement("UPDATE LocationNames SET autoName = ? WHERE ipAddress=?");
            statement.setString(1, autoName);
            statement.setString(2, ipAddr);

            statement.executeUpdate();
            statement.close();
        }
          
      }
      setLocation(connection,user,guid,autoName);
      connection.commit();
      return autoName;
    } catch (SQLException ex) {
      rollback(connection);
      return "";
    }

  }

    public class PresenceInfo
    {
        public String state;
        public Timestamp time;
        public String location;
        public String ipAddress;
        public String build;
    };

    public PresenceInfo getUserPresence(String user) throws SQLException, NamingException
    {
        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("SELECT state, time, location,colaboBuild FROM Presence WHERE (ID = ?)");
            statement.setString(1, user);
            ResultSet resultSet = statement.executeQuery();
            String bestState = null; // consider offline unless proven otherwise
            String bestLocation = null;
            String bestBuild = null;
            
            Timestamp bestTime = null;
            long graceMs = TimeUnit.MINUTES.toMillis(15);
            long consideredNow = System.currentTimeMillis()-graceMs;
            while (resultSet.next())
            {
                String state = resultSet.getString(1);
                Timestamp time = resultSet.getTimestamp(2);
                String location = resultSet.getString(3);
                if (time==null) continue;
                if (time.getTime()>=consideredNow)
                {
                    if (bestState==null || BetterState(state, bestState) || bestTime.getTime()<consideredNow)
                    {
                        bestState = state;
                        bestTime = time;
                        bestLocation = location;
                        bestBuild = resultSet.getString(4);
                    }
                }
                else if (bestTime==null || time.getTime()>bestTime.getTime())
                {
                    // for past entries ignore state - we are interested in time and location only
                    bestTime = time;
                    // explicit offline is interesting (shutdown reported), otherwise assume N/A
                    bestState = state.equals("Offline") ? state : null;
                    bestLocation = location;
                    bestBuild = resultSet.getString(4);
                }
            }
            if (bestState==null)
            {
                bestState = bestTime==null ? "" : "N/A";
            }
            resultSet.close();
            statement.close();

            PresenceInfo result = new PresenceInfo();
            if (bestState!=null) result.state = bestState;
            result.location = bestLocation;
            result.ipAddress = bestLocation;
            result.build = bestBuild;
            
            {
                PreparedStatement locStatement = connection.prepareStatement("SELECT name FROM LocationNames WHERE (autoName = ?)");
                locStatement.setString(1, result.ipAddress);
                ResultSet locResultSet = locStatement.executeQuery();
                if (locResultSet.next())
                {
                    String name = locResultSet.getString(1);
                    if (name!=null) result.location = name;
                }
            }
            
            result.time = bestTime;
            
            return result;
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }
    
    private void reportUserPresence(String username, JSONObject userInfo) throws JSONException, SQLException, NamingException
    {
        PresenceInfo presence = getUserPresence(username);
        userInfo.putOpt("Presence",presence.state);
        userInfo.putOpt("Location",presence.location);
        userInfo.putOpt("IPAddress",presence.ipAddress);
        userInfo.putOpt("PresenceTime",presence.time);
        userInfo.putOpt("Build",presence.build);
    }
    
    JSONArray getUserChatToRead(Connection connection, String user) throws SQLException, JSONException
    {
        JSONArray chats = new JSONArray();
        
        {
            PreparedStatement statement = connection.prepareStatement("SELECT ConversationId,revRead FROM ConversationUser WHERE userName=?");
            statement.setString(1, user);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int chatId = resultSet.getInt(1);
                int revRead = resultSet.getInt(2);

                int revToRead = ConversationRevision(connection, chatId);
                JSONObject chatRev = new JSONObject();
                chatRev.put("id",chatId);
                if (revToRead>revRead)
                {
                  chatRev.put("revision",revToRead);
                }
                JSONArray users = ConversationUserState(connection, chatId);
                
                chatRev.put("users",users);
                chats.put(chatRev);
            }
            resultSet.close();
            statement.close();
        }
        return chats;
    }
    @Path("IMEvents/{user}")
    @GET
    @Produces("application/json")
    public Response getIMEvents(
            @Context HttpServletRequest requestContext, @HeaderParam("Authorization") String authorization,
            @PathParam("user") String user, @QueryParam("guid") String guid
    )
    {
        if (!authorize(authorization)) return notAuthorized();

        AuthorizationInfo authInfo = decodeAuthorization(authorization);
        
        Connection connection = null;
        try
        {
            connection = AppState.getConnection(false);
            
            String ipAddr = requestContext.getRemoteAddr();
            
            if (guid!=null)
            {
              ipAddr = traceLocation(connection,user,guid,ipAddr);
            }
            

            try {
                // very simple long poll - wait for a while first
                // TODO: break on a user state change
              Object obj = AppState.getInstance()._wakeUpLongPoll;
              synchronized (obj)
              {
                obj.wait(10000);
              }
            } catch (InterruptedException ex) {
            }
            
            JSONObject result = getUserList(connection);

            result.put("ipAddr", ipAddr);
            
            result.put("chatToRead",getUserChatToRead(connection, authInfo.name));
            
            connection.commit();

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            rollback(connection);
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }
    
    @Path("ReportIMState/{user}/{userState}/{guid:.*}")
    @PUT
    @Produces("application/json")
    public Response reportState(
            @HeaderParam("Authorization") String authorization,
            @PathParam("user") String user, @PathParam("userState") String userState, @PathParam("guid") String guid
    )
    {
        // get list of all users, together with their states
        if (!authorize(authorization)) return notAuthorized();
        // set state of user
        setPresence(user,userState,guid);
        return Response.ok().build();
    }

    @Path("ReportIMLocation/{user}/{guid}/{location:.*}")
    @PUT
    @Produces("application/json")
    public Response reportLocation(
            @Context HttpServletRequest requestContext,
            @HeaderParam("Authorization") String authorization,
            @PathParam("user") String user, @PathParam("guid") String guid, @PathParam("location") String location,
            @DefaultValue("") @QueryParam("build") String build
    )
    {
        // get list of all users, together with their states
        if (!authorize(authorization)) return notAuthorized();
        // set state of user

        Connection connection = null;
        try
        {
            connection = AppState.getConnection(false);

            PreparedStatement statement = connection.prepareStatement("INSERT INTO Presence (stationGUID,Id,colaboBuild) VALUES (?,?,?) ON DUPLICATE KEY UPDATE colaboBuild=?");
            int st = 1;
            statement.setString(st++, guid);
            statement.setString(st++, user);
            statement.setString(st++, build);

            statement.setString(st++, build);
            
            statement.executeUpdate();
            statement.close();
            
            String ipAddr = requestContext.getRemoteAddr();
            
            traceLocation(connection, user, guid, ipAddr);

            AppState.WakeUpLongPoll();
        }
        catch (Exception exception)
        {
           AppState.logger().log(Level.WARNING, "Set presence failed", exception);
            
        }
        finally
        {
            AppState.closeConnection(connection);
        }
        
        return Response.ok().build();
    }
    
    @Path("IMLocationName/{ipAddr}/{location:.*}")
    @PUT
    @Produces("application/json")
    public Response setLocationName(
            @HeaderParam("Authorization") String authorization,
            @PathParam("ipAddr") String ipAddr, @PathParam("location") String location
    )
    {
        // get list of all users, together with their states
        if (!authorize(authorization)) return notAuthorized();
        // set state of user

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("UPDATE LocationNames SET name=? WHERE autoName=?");
            statement.setString(1, location);
            statement.setString(2, ipAddr);

            statement.executeUpdate();
            statement.close();
            // TODO: notify any waiting long polls
        }
        catch (Exception exception)
        {
           AppState.logger().log(Level.WARNING, "Set presence failed", exception);
            
        }
        finally
        {
            AppState.closeConnection(connection);
        }
        return Response.ok().build();
    }
    
    @Path("LeaveChat/{chatId}")
    @PUT
    @Produces("application/json")
    public Response leaveChat(String content, @HeaderParam("Authorization") String authorization, @PathParam("chatId") Integer chatId)
    {
        if (!authorize(authorization)) return notAuthorized();
        
        AuthorizationInfo authInfo = decodeAuthorization(authorization);

        Connection connection = null;
        try
        {
            connection = AppState.getConnection(false);

            // only chat participant can remove the chat

            boolean enabled = checkChatAuthorization(connection, chatId, authInfo);
            
            if (!enabled) return notAuthorized();

            Timestamp now = new Timestamp(System.currentTimeMillis());
            
            int revision = chatRevision(connection, chatId);
            
            {
                PreparedStatement statement = connection.prepareStatement(
                        "UPDATE ConversationUser SET state='Close',stateTime=?,stateRev=? WHERE (conversationId=? AND userName=?)");

                int st = 1;
                statement.setTimestamp(st++, now);
                statement.setInt(st++, revision);
                statement.setInt(st++, chatId);
                statement.setString(st++, authInfo.name);

                statement.executeUpdate();

                statement.close();
                    
            }
            
            int activeUsers = 0;
            
            {
              PreparedStatement statement = connection.prepareStatement("SELECT COUNT(userName) FROM ConversationUser WHERE (conversationId=? AND state!='Close')");
              int st = 1;
              statement.setInt(st++, chatId);

              ResultSet resultSet = statement.executeQuery();
              if (resultSet.next()) {
                  activeUsers = resultSet.getInt(1);
              }
              resultSet.close();
              statement.close();
              
            }
            
            if (activeUsers<=0)
            {
              // remove rhe conversation
              PreparedStatement statement = connection.prepareStatement("DELETE FROM Conversation WHERE (id=?)");
              statement.setInt(1, chatId);
              statement.executeUpdate();
              statement.close();
            }

            // create the answer
            JSONObject result = new JSONObject();
            
            result.put("activeUsers",activeUsers);
            
            connection.commit();

            AppState.WakeUpLongPoll();
            
            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            rollback(connection);
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }
    
    @Path("ReportChatTyping/{chatId}")
    @PUT
    @Produces("application/json")
    public Response reportChatTyping(
            String content, @HeaderParam("Authorization") String authorization, @PathParam("chatId") Integer chatId,
            @DefaultValue("true") @QueryParam("typing") Boolean typing
    )
    {
        if (!authorize(authorization)) return notAuthorized();
        
        AuthorizationInfo authInfo = decodeAuthorization(authorization);

        Connection connection = null;
        try
        {
            connection = AppState.getConnection(false);

            boolean enabled = checkChatAuthorization(connection, chatId, authInfo);
            
            if (!enabled) return notAuthorized();

            Timestamp now = new Timestamp(System.currentTimeMillis());
            int revision = chatRevision(connection, chatId);
            
            {
                PreparedStatement statement = connection.prepareStatement(
                        "UPDATE ConversationUser SET state=?,stateTime=?,stateRev=? WHERE (conversationId=? AND userName=?)");

                int st = 1;
                statement.setString(st++, typing ? "Type" : "Ready");
                statement.setTimestamp(st++, now);
                statement.setInt(st++, revision);
                statement.setInt(st++, chatId);
                statement.setString(st++, authInfo.name);

                statement.executeUpdate();

                statement.close();
            }
            
            // create the answer
            JSONObject result = new JSONObject();
            
            connection.commit();

            AppState.WakeUpLongPoll();
            
            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            rollback(connection);
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }
    
    @Path("DeleteChat/{chatId}")
    @PUT
    @Produces("application/json")
    public Response deleteChat(String content, @HeaderParam("Authorization") String authorization, @PathParam("chatId") Integer chatId)
    {
        if (!authorize(authorization)) return notAuthorized();
        
        AuthorizationInfo authInfo = decodeAuthorization(authorization);

        Connection connection = null;
        try
        {
            connection = AppState.getConnection(false);

            // only chat participant can remove the chat

            boolean enabled = checkChatAuthorization(connection, chatId, authInfo);
            
            if (!enabled) return notAuthorized();

            {
              // remove rhe conversation
              PreparedStatement statement = connection.prepareStatement("DELETE FROM Conversation WHERE (id=?)");
              statement.setInt(1, chatId);
              statement.executeUpdate();
              statement.close();
            }

            // create the answer
            JSONObject result = new JSONObject();
            
            connection.commit();

            AppState.WakeUpLongPoll();
            
            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            rollback(connection);
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }
    
    @Path("SendChat/{chatId}")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response sendChat(String content, @HeaderParam("Authorization") String authorization, @PathParam("chatId") Integer chatId)
    {
        if (!authorize(authorization)) {
            return notAuthorized();
        }

        AuthorizationInfo authInfo = decodeAuthorization(authorization);

        Connection connection = null;
        try {
            connection = AppState.getConnection(false);

            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            JSONObject result = new JSONObject();

            JSONObject article = root.optJSONObject("Article");
            if (article == null) {
                boolean enabled = checkChatAuthorization(connection, chatId, authInfo);
                if (!enabled) {
                    return notAuthorized();
                }

                String reply = root.getString("Reply");
                sendChatReply(connection, chatId, authInfo.name, reply);
            } else {
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO Conversation (parent, title, url, author,date,tags,Revision) VALUES (?,?,?,?,?,?,?)",
                        Statement.RETURN_GENERATED_KEYS);

                Timestamp now = new Timestamp(System.currentTimeMillis());

                int st = 1;

                statement.setInt(st++, article.getInt("parent"));
                statement.setString(st++, article.getString("title"));
                statement.setString(st++, article.getString("url"));
                statement.setString(st++, article.getString("author"));
                statement.setTimestamp(st++, now);
                statement.setString(st++, article.getString("tags"));
                statement.setInt(st++, 0);

                statement.executeUpdate();

                ResultSet resultSet = statement.getGeneratedKeys();

                if (resultSet.next()) {
                    int id = resultSet.getInt(1);
                    chatId = id;
                }
                resultSet.close();

                result.put("id", chatId);
                statement.close();

                ArrayList<String> userList = ParseUserList(root.getJSONArray("users"));
                AddConversationUsers(connection, chatId, userList, authInfo.name);

                sendChatReply(connection, chatId, authInfo.name, article.getString("_"));
            }

            connection.commit();
            AppState.WakeUpLongPoll();
            // create the answer
            return Response.ok(result.toString()).build();
        } catch (Exception exception) {
            rollback(connection);
            return Response.serverError().entity(JSONError(exception)).build();
        } finally {
            AppState.closeConnection(connection);
        }
    }
    
  @Path("LoadChat/{chatId}")
  @GET
  @Produces("application/json")
  public Response loadChat(@HeaderParam("Authorization") String authorization,
          @PathParam("chatId") Integer chatId, @DefaultValue("true") @QueryParam("active") boolean active) {
    if (!authorize(authorization)) {
      return notAuthorized();
    }

    AuthorizationInfo authInfo = decodeAuthorization(authorization);

    Connection connection = null;
    try {
      connection = AppState.getConnection(false);

      boolean enabled = checkChatAuthorization(connection, chatId, authInfo);
      if (!enabled) {
        return notAuthorized();
      }

      JSONObject result = new JSONObject();
      Timestamp now = new Timestamp(System.currentTimeMillis());
            
      int revision = 0;
      {
        PreparedStatement statement = connection.prepareStatement(
                "SELECT revision FROM Conversation WHERE (id=?)");
        int st = 1;
        statement.setInt(st++, chatId);

        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
          revision = resultSet.getInt(1);
          result.put("_", ""); // TODO: remove, done for compatibility with old clients
        }
        resultSet.close();
        statement.close();
      }
      // append all parts
      JSONArray parts = new JSONArray();
      {
        PreparedStatement statement = connection.prepareStatement(
                "SELECT part,user,text,time FROM ConversationParts WHERE (id=?)");
        int st = 1;
        statement.setInt(st++, chatId);

        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
          JSONObject part = new JSONObject();
          int rt = 1;
          part.put("part",resultSet.getInt(rt++));
          part.put("user",resultSet.getString(rt++));
          part.put("_",resultSet.getString(rt++));
          part.put("time",resultSet.getTimestamp(rt++));
          parts.put(part);
        }
        if (parts.length()>0)
        {
          result.put("parts",parts);
        }
        resultSet.close();
        statement.close();
      }
      
      if (active) {
        String state = "Ready";
        {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT state FROM ConversationUser WHERE (conversationId=? AND userName=?)");
            int st = 1;
            statement.setInt(st++, chatId);
            statement.setString(st++, authInfo.name);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                state = resultSet.getString(1);
            }
            resultSet.close();
            statement.close();
        }
        // keep Type, otherwise set Ready
        if (!state.equalsIgnoreCase("Type")) state = "Ready";
        SetChatState(connection, revision, state, now, chatId, authInfo.name);

      }
      connection.commit();

      // create the answer
      return Response.ok(result.toString()).build();
    } catch (Exception exception) {
      rollback(connection);
      return Response.serverError().entity(JSONError(exception)).build();
    } finally {
      AppState.closeConnection(connection);
    }
  }
    
    @Path("ChatList")
    @GET
    @Produces("application/json")
    public Response chatList(@HeaderParam("Authorization") String authorization)
    {
        // list of all chats visible to the given user
        if (!authorize(authorization)) return notAuthorized();

        AuthorizationInfo authInfo = decodeAuthorization(authorization);
        
        Connection connection = null;
        try
        {
            connection = AppState.getConnection();
            JSONObject result = new JSONObject();
            List<Integer> chats = chatListForUser(connection, authInfo);

            JSONArray array = new JSONArray();
            result.put("Chats", array);
            for (Integer chat:chats)
            {
                array.put(chat);
            }
            
            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
        
    }
    
    @Path("ChatHeader/{chatId}")
    @GET
    @Produces("application/json")
    public Response chatHeader(@HeaderParam("Authorization") String authorization, @PathParam("chatId") Integer chatId)
    {
        if (!authorize(authorization)) return notAuthorized();
        
        AuthorizationInfo authInfo = decodeAuthorization(authorization);

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();
            
            boolean enabled = checkChatAuthorization(connection, chatId, authInfo);
            if (!enabled) return notAuthorized();
            
            JSONObject result = new JSONObject();
            

            {
                PreparedStatement statement = connection.prepareStatement(
                    "SELECT parent, title, url, author, date, tags FROM Conversation WHERE (id=?)"
                );
                int st = 1;
                statement.setInt(st++, chatId);

                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next())
                {
                    int rt = 1;
                    JSONObject article = new JSONObject();
                    article.put("parent",resultSet.getString(rt++));
                    article.put("title",resultSet.getString(rt++));
                    article.put("url",resultSet.getString(rt++));
                    article.put("author",resultSet.getString(rt++));
                    Timestamp date = resultSet.getTimestamp(rt++);
                    article.put("date",date.toString());
                    article.put("tags",resultSet.getString(rt++));
                    result.put("Article",article);
                }
                resultSet.close();
                statement.close();
            }
            
            {
                PreparedStatement statement = connection.prepareStatement("SELECT userName FROM ConversationUser WHERE ConversationId=?");
                statement.setInt(1, chatId);
                ResultSet resultSet = statement.executeQuery();
                
                JSONArray users = new JSONArray();
                while (resultSet.next())
                {
                    users.put(resultSet.getString(1));
                }
                resultSet.close();
                statement.close();
                result.put("users",users);
            }
            
            // create the answer
            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }
    
    
    @Path("Users")
    @GET
    @Produces("application/json")
    public Response getUsers(@Context HttpServletRequest requestContext, @HeaderParam("Authorization") String authorization)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();
            JSONObject result = getUserList(connection);

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.ok(JSONError(exception).toString()).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Users")
    @PUT
    @Consumes("application/json")
    public Response putUsers(String content, @HeaderParam("Authorization") String authorization)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);

            connection = AppState.getConnection();

            // users
            JSONArray users = root.getJSONArray("Users");
            if (users.length() > 0)
            {
                // delete removed users
                String mask = "?";
                for (int i=1; i<users.length(); i++) mask += ", ?";
                String command  = "DELETE FROM Users WHERE ID NOT IN(" + mask + ")";
                PreparedStatement statement = connection.prepareStatement(command);
                for (int i=0; i<users.length(); i++)
                {
                    JSONObject user = users.getJSONObject(i);
                    statement.setString(i + 1, user.getString("ID"));
                }
                statement.executeUpdate();
                statement.close();

                // add new users
                statement = connection.prepareStatement("INSERT IGNORE INTO Users (ID) VALUES (?)");
                for (int i=0; i<users.length(); i++)
                {
                    JSONObject user = users.getJSONObject(i);
                    statement.setString(1, user.getString("ID"));
                    statement.addBatch();
                }
                statement.executeBatch();
                statement.close();

                // update users
                statement = connection.prepareStatement("UPDATE Users SET AccessRights = ?, Password = ? WHERE (ID = ?)");
                for (int i=0; i<users.length(); i++)
                {
                    JSONObject user = users.getJSONObject(i);
                    statement.setString(1, user.getString("Access"));
                    statement.setString(2, user.getString("Password"));
                    statement.setString(3, user.getString("ID"));
                    statement.addBatch();
                }
                statement.executeBatch();
                statement.close();
            }

            // groups
            // delete groups
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Groups");
            statement.executeUpdate();
            statement.close();

            JSONArray groups = root.getJSONArray("Groups");
            if (groups.length() > 0)
            {
                // add groups
                statement = connection.prepareStatement("INSERT IGNORE INTO Groups (ID, FullName, AccessRights, Level, Homepage) VALUES (?, ?, ?, ?, ?)");
                for (int i=0; i<groups.length(); i++)
                {
                    JSONObject group = groups.getJSONObject(i);
                    statement.setString(1, group.getString("ID"));
                    statement.setString(2, group.getString("Name"));
                    statement.setString(3, group.getString("Access"));
                    statement.setInt(4, group.getInt("Level"));
                    statement.setInt(5, group.getInt("Home"));
                    statement.addBatch();
              }
                statement.executeBatch();
                statement.close();
            }

            // group users
            // delete group users
            statement = connection.prepareStatement("DELETE FROM GroupUsers");
            statement.executeUpdate();
            statement.close();

            JSONArray groupUsers = root.getJSONArray("GroupUsers");
            if (groupUsers.length() > 0)
            {
                statement = connection.prepareStatement("INSERT IGNORE INTO GroupUsers (GroupID, UserID) VALUES (?, ?)");
                for (int i=0; i<groupUsers.length(); i++)
                {
                    JSONObject groupUser = groupUsers.getJSONObject(i);
                    statement.setString(1, groupUser.getString("Group"));
                    statement.setString(2, groupUser.getString("User"));
                    statement.addBatch();
                }
                statement.executeBatch();
                statement.close();
            }

            return Response.ok().build();
        }
        catch (Exception exception)
        {
          return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("UserSettings/{user}")
    @GET
    @Produces("application/json")
    public Response getUserSettings(@HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            int homepage = 0;
            JSONArray groups = new JSONArray();
            JSONArray access = new JSONArray();

            // groups
            PreparedStatement statement = connection.prepareStatement("SELECT ID, AccessRights, HomePage FROM Groups INNER JOIN GroupUsers ON (ID = GroupID) WHERE (UserId = ?) ORDER BY Level");
            statement.setString(1, user);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              groups.put(resultSet.getObject(1));
              access.put(resultSet.getObject(2));

              int home = resultSet.getInt(3);
              if (home > 0) homepage = home;
            }
            resultSet.close();
            statement.close();

            // users
            statement = connection.prepareStatement("SELECT AccessRights, Homepage FROM Users WHERE (ID = ?)");
            statement.setString(1, user);

            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              access.put(resultSet.getObject(1));

              int home = resultSet.getInt(2);
              if (home > 0) homepage = home;
            }
            resultSet.close();
            statement.close();

            JSONObject result = new JSONObject();
            result.put("Groups", groups);
            result.put("Access", access);
            result.put("Home", homepage);

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
          return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Homepage/{user}")
    @GET
    @Produces("application/json")
    public Response getHomepage(@HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            JSONObject result = new JSONObject();

            PreparedStatement statement = connection.prepareStatement("SELECT Homepage FROM Users WHERE (ID = ?)");
            statement.setString(1, user);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
                result.put("Homepage", resultSet.getObject(1));
            resultSet.close();
            statement.close();

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Homepage/{user}")
    @PUT
    @Consumes("application/json")
    public Response putHomepage(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            int homepage = root.getInt("Homepage");

            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("INSERT IGNORE INTO Users (ID) VALUES (?)");
            statement.setString(1, user);
            statement.executeUpdate();
            statement.close();

            statement = connection.prepareStatement("UPDATE Users SET Homepage = ? WHERE (ID = ?)");
            statement.setInt(1, homepage);
            statement.setString(2, user);
            statement.executeUpdate();
            statement.close();

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("RuleSets/{user}")
    @GET
    @Produces("application/json")
    public Response getRuleSets(@HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            JSONArray result = new JSONArray();
            PreparedStatement statement = connection.prepareStatement("SELECT Name, RuleSetName, Expression, ParentName, ParentRuleSetName FROM RuleSets WHERE (Name = 'Default' OR Name = ?)");
            statement.setString(1, user);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              JSONObject ruleSet = new JSONObject();
              result.put(ruleSet);

              ruleSet.put("User", resultSet.getObject(1));
              ruleSet.put("Name", resultSet.getObject(2));
              ruleSet.put("Expression", resultSet.getObject(3));
              ruleSet.put("ParentUser", resultSet.getObject(4));
              ruleSet.put("ParentName", resultSet.getObject(5));
            }
            resultSet.close();
            statement.close();

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("RuleSets/{user}")
    @PUT
    @Consumes("application/json")
    public Response putRuleSets(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONArray array = new JSONArray(reader);

            connection = AppState.getConnection();

            // remove the old rule sets
            PreparedStatement statement = connection.prepareStatement("DELETE FROM RuleSets WHERE (Name = ?)");
            statement.setString(1, user);
            statement.executeUpdate();
            statement.close();

            // insert the new rule sets
            if (array.length() > 0)
            {
              statement = connection.prepareStatement("INSERT IGNORE INTO RuleSets (Name, RuleSetName, Expression, ParentName, ParentRuleSetName) VALUES (?, ?, ?, ?, ?)");
              statement.setString(1, user);
              for (int i=0; i<array.length(); i++)
              {
                  JSONObject obj = array.getJSONObject(i);
                  String name = obj.getString("Name");
                  String expression = obj.getString("Expression");
                  String parentUser = obj.getString("ParentUser");
                  String parentName = obj.getString("ParentName");

                  statement.setString(2, name);
                  statement.setString(3, expression);
                  statement.setString(4, parentUser);
                  statement.setString(5, parentName);
                  statement.addBatch();
              }
             statement.executeBatch();
             statement.close();
            }

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Bookmarks/{user}")
    @GET
    @Produces("application/json")
    public Response getBookmarks(@HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            JSONArray result = new JSONArray();

            PreparedStatement statement = connection.prepareStatement("SELECT ID, Title, Tags, Article, Image, Parent FROM Bookmarks WHERE (Name = ?)");
            statement.setString(1, user);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              JSONObject bookmark = new JSONObject();
              result.put(bookmark);
              bookmark.put("ID", resultSet.getObject(1));
              bookmark.put("Name", resultSet.getObject(2));
              bookmark.put("Tags", resultSet.getObject(3));
              bookmark.put("Article", resultSet.getObject(4));
              bookmark.put("Image", resultSet.getObject(5));
              bookmark.put("Parent", resultSet.getObject(6));
            }
            resultSet.close();
            statement.close();

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Bookmarks/{user}")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response postBookmarks(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            String name = root.getString("Name");
            String tags = root.getString("Tags");
            int article = root.getInt("Article");
            int image = root.getInt("Image");
            int parent = root.getInt("Parent");

            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("INSERT IGNORE INTO Bookmarks (Name, Title, Tags, Article, Image, Parent) VALUES (?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user);
            statement.setString(2, name);
            statement.setString(3, tags);
            String strArticle = article >= 0 ? Integer.toString(article) : null;
            statement.setString(4, strArticle);
            statement.setInt(5, image);
            String strParent = parent >= 0 ? Integer.toString(parent) : null;
            statement.setString(6, strParent);
            statement.executeUpdate();

            // retrieve the assigned id
            int id = -1;
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) id = resultSet.getInt(1);
            else throw new Exception("SQL Insert failed for postBookmarks");
            resultSet.close();

            statement.close();

            // create the answer
            JSONObject result = new JSONObject();
            result.put("ID", id);

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Bookmark/{id}")
    @PUT
    @Consumes("application/json")
    public Response putBookmark(String content, @HeaderParam("Authorization") String authorization, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            String name = root.getString("Name");
            String tags = root.getString("Tags");
            int article = root.getInt("Article");
            int image = root.getInt("Image");
            int parent = root.getInt("Parent");

            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("UPDATE Bookmarks SET Title = ?, Tags = ?, Article = ?, Image = ?, Parent = ? WHERE (ID = ?)");
            statement.setString(1, name);
            statement.setString(2, tags);
            String strArticle = article >= 0 ? Integer.toString(article) : null;
            statement.setString(3, strArticle);
            statement.setInt(4, image);
            String strParent = parent >= 0 ? Integer.toString(parent) : null;
            statement.setString(5, strParent);
            statement.setInt(6, id);
            statement.executeUpdate();

            statement.close();

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Bookmark/{id}")
    @DELETE
    @Consumes("application/json")
    public Response deleteBookmark(String content, @HeaderParam("Authorization") String authorization, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            int parent = root.getInt("Parent");

            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("DELETE FROM Bookmarks WHERE (ID = ?)");
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();

            // update the children to not point to this bookmark
            String strParent = parent >= 0 ? Integer.toString(parent) : null;
            statement = connection.prepareStatement("UPDATE Bookmarks SET Parent = ? WHERE (Parent = ?)");
            statement.setString(1, strParent);
            statement.setInt(2, id);
            statement.executeUpdate();
            statement.close();

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Scripts/{user}")
    @GET
    @Produces("application/json")
    public Response getScripts(@HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            JSONArray result = new JSONArray();
            PreparedStatement statement = connection.prepareStatement("SELECT UserName, `Usage`, Name, Image, Language, Script FROM Scripts WHERE (UserName = 'Default' OR UserName = ?)");
            statement.setString(1, user);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              JSONObject ruleSet = new JSONObject();
              result.put(ruleSet);

              ruleSet.put("User", resultSet.getObject(1));
              ruleSet.put("Usage", resultSet.getObject(2));
              ruleSet.put("Name", resultSet.getObject(3));
              ruleSet.put("Image", resultSet.getObject(4));
              ruleSet.put("Language", resultSet.getObject(5));
              ruleSet.put("Script", resultSet.getObject(6));
            }
            resultSet.close();
            statement.close();

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Scripts/{user}")
    @PUT
    @Consumes("application/json")
    public Response putScripts(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONArray array = new JSONArray(reader);

            connection = AppState.getConnection();

            // remove the old rule sets
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Scripts WHERE (UserName = ?)");
            statement.setString(1, user);
            statement.executeUpdate();
            statement.close();

            // insert the new rule sets
            if (array.length() > 0)
            {
              statement = connection.prepareStatement("INSERT IGNORE INTO Scripts (UserName, `Usage`, Name, Image, Language, Script) VALUES (?, ?, ?, ?, ?, ?)");
              statement.setString(1, user);
              for (int i=0; i<array.length(); i++)
              {
                  JSONObject obj = array.getJSONObject(i);
                  String usage = obj.getString("Usage");
                  String name = obj.getString("Name");
                  String image = obj.getString("Image");
                  String language = obj.getString("Language");
                  String script = obj.getString("Script");

                  statement.setString(2, usage);
                  statement.setString(3, name);
                  statement.setString(4, image);
                  statement.setString(5, language);
                  statement.setString(6, script);
                  statement.addBatch();
              }
             statement.executeBatch();
             statement.close();
            }

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Articles/{user}")
    @GET
    @Produces("application/json")
    public Response getArticles(@HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            // list of all articles
            JSONArray articles = new JSONArray();
            PreparedStatement statement = connection.prepareStatement("SELECT ID, Title, URL, Parent, Author, Date, Revision, MajorRevision FROM Articles");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              JSONObject article = new JSONObject();
              articles.put(article);
              article.put("ID", resultSet.getObject(1));
              article.put("Title", resultSet.getObject(2));
              article.put("URL", resultSet.getObject(3));
              article.put("Parent", resultSet.getObject(4));
              article.put("Author", resultSet.getObject(5));
              article.put("Date", resultSet.getObject(6));
              article.put("Revision", resultSet.getObject(7));
              article.put("MajorRevision", resultSet.getObject(8));
            }
            resultSet.close();
            statement.close();

            // tags
            JSONArray tags = new JSONArray();
            statement = connection.prepareStatement("SELECT ID, Tag FROM Tags");
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              JSONObject tag = new JSONObject();
              tags.put(tag);
              tag.put("ID", resultSet.getObject(1));
              tag.put("Tag", resultSet.getObject(2));
            }
            resultSet.close();
            statement.close();

            // private tags
            JSONArray privateTags = new JSONArray();
            statement = connection.prepareStatement("SELECT ID, Tag FROM PrivateTags WHERE (Name = ?)");
            statement.setString(1, user);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              JSONObject tag = new JSONObject();
              privateTags.put(tag);
              tag.put("ID", resultSet.getObject(1));
              tag.put("Tag", resultSet.getObject(2));
            }
            resultSet.close();
            statement.close();

            // read articles
            JSONArray read = new JSONArray();
            statement = connection.prepareStatement("SELECT ID, Revision FROM MarkedRead WHERE (Name = ?)");
            statement.setString(1, user);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              JSONObject item = new JSONObject();
              read.put(item);
              item.put("ID", resultSet.getObject(1));
              item.put("Revision", resultSet.getObject(2));
            }
            resultSet.close();
            statement.close();

            // collapsed subtrees
            JSONArray collapsed = new JSONArray();
            statement = connection.prepareStatement("SELECT ID FROM Collapsed WHERE (Name = ?)");
            statement.setString(1, user);
            resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              collapsed.put(resultSet.getObject(1));
            }
            resultSet.close();
            statement.close();

            JSONObject result = new JSONObject();
            result.put("Articles", articles);
            result.put("Tags", tags);
            result.put("PrivateTags", privateTags);
            result.put("Read", read);
            result.put("Collapsed", collapsed);

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Articles/{user}")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response postArticles(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            int id = root.getInt("ID");
            String title = root.getString("Title");
            String url = root.getString("URL");
            String author = root.getString("Author");
            java.sql.Timestamp date = parseDate(root.getString("Date"));
            int revision = root.getInt("Revision");
            int majorRevision = root.getInt("MajorRevision");
            int parent = root.getInt("Parent");
            String strParent = parent >= 0 ? Integer.toString(parent) : null;
            boolean changed = root.getBoolean("Changed");

            JSONArray tags = root.getJSONArray("Tags");
            JSONArray privateTags = root.getJSONArray("PrivateTags");

            connection = AppState.getConnection();

            PreparedStatement statement;
            ResultSet resultSet;

            if (id < 0)
            {
                // insert the article properties
                statement = connection.prepareStatement("INSERT INTO Articles (Title, URL, Author, Date, Revision, MajorRevision, Parent) VALUES (?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, title);
                statement.setString(2, url);
                statement.setString(3, author);
                statement.setTimestamp(4, date);
                statement.setInt(5, revision);
                statement.setInt(6, majorRevision);
                statement.setString(7, strParent);
                statement.executeUpdate();

                // retrieve the assigned id
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) id = resultSet.getInt(1);
                resultSet.close();

                statement.close();
            }
            else
            {
                // update the article properties
                statement = connection.prepareStatement("UPDATE Articles SET Title = ?, URL = ?, Author = ?, Date = ?, Revision = ?, MajorRevision = ?, Parent = ? WHERE (ID = ?)");
                statement.setString(1, title);
                statement.setString(2, url);
                statement.setString(3, author);
                statement.setTimestamp(4, date);
                statement.setInt(5, revision);
                statement.setInt(6, majorRevision);
                statement.setString(7, strParent);
                statement.setInt(8, id);
                statement.executeUpdate();
                statement.close();

                // remove the read flag for all users
                if (revision <= 0)
                {
                    statement = connection.prepareStatement("DELETE FROM MarkedRead WHERE (ID = ?)");
                    statement.setInt(1, id);
                    statement.executeUpdate();
                    statement.close();
                }

                // remove all old tags from Tags table
                statement = connection.prepareStatement("DELETE FROM Tags WHERE (ID = ?)");
                statement.setInt(1, id);
                statement.executeUpdate();
                statement.close();

                // remove all old private tags from PrivateTags table
                statement = connection.prepareStatement("DELETE FROM PrivateTags WHERE (ID = ? AND Name = ?)");
                statement.setInt(1, id);
                statement.setString(2, user);
                statement.executeUpdate();
                statement.close();
            }

            // insert tags
            if (tags != null && tags.length() > 0)
            {
                statement = connection.prepareStatement("INSERT IGNORE INTO Tags (ID, Tag) VALUES (?, ?)");
                statement.setInt(1, id);
                for (int i=0; i<tags.length(); i++)
                {
                    statement.setString(2, tags.getString(i));
                    statement.addBatch();
                }
                statement.executeBatch();
                statement.close();
            }
            if (privateTags != null && privateTags.length() > 0)
            {
                statement = connection.prepareStatement("INSERT IGNORE INTO PrivateTags (Name, ID, Tag) VALUES (?, ?, ?)");
                statement.setString(1, user);
                statement.setInt(2, id);
                for (int i=0; i<privateTags.length(); i++)
                {
                    statement.setString(3, privateTags.getString(i));
                    statement.addBatch();
                }
                statement.executeBatch();
                statement.close();
            }

            // mark article as read for the sender if it was marker read before
            if (!changed)
            {
                statement = connection.prepareStatement("REPLACE INTO MarkedRead (Name, ID, Revision) VALUES (?, ?, ?)");
                statement.setString(1, user);
                statement.setInt(2, id);
                statement.setInt(3, revision);
                statement.executeUpdate();
                statement.close();
            }

            // create the answer
            JSONObject result = new JSONObject();
            result.put("ID", id);

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Article/{id}")
    @DELETE
    @Consumes("application/json")
    public Response deleteArticle(String content, @HeaderParam("Authorization") String authorization, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            int parent = root.getInt("Parent");

            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("DELETE FROM Articles WHERE (ID = ?)");
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();

            // Delete records from helper tables

            statement = connection.prepareStatement("DELETE FROM Tags WHERE (ID = ?)");
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();

            statement = connection.prepareStatement("DELETE FROM PrivateTags WHERE (ID = ?)");
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();

            statement = connection.prepareStatement("DELETE FROM MarkedRead WHERE (ID = ?)");
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();

            statement = connection.prepareStatement("DELETE FROM Collapsed WHERE (ID = ?)");
            statement.setInt(1, id);
            statement.executeUpdate();
            statement.close();

            // Redirect children to the new parent
            String strParent = parent >= 0 ? Integer.toString(parent) : null;
            statement = connection.prepareStatement("UPDATE Articles SET Parent = ? WHERE (Parent = ?)");
            statement.setString(1, strParent);
            statement.setInt(2, id);
            statement.executeUpdate();
            statement.close();

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("ArticleRead/{user}/{id}")
    @GET
    @Produces("application/json")
    public Response getArticleRead(@HeaderParam("Authorization") String authorization, @PathParam("user") String user, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            JSONObject result = new JSONObject();

            PreparedStatement statement = connection.prepareStatement("SELECT Revision FROM MarkedRead WHERE (Name = ? AND ID = ?)");
            statement.setString(1, user);
            statement.setInt(2, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
                result.put("Revision", resultSet.getObject(1));
            resultSet.close();
            statement.close();

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("ArticleRead/{user}/{id}")
    @PUT
    @Consumes("application/json")
    public Response putArticleRead(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            int revision = root.getInt("Revision");

            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("REPLACE INTO MarkedRead (Name, ID, Revision) VALUES (?, ?, ?)");
            statement.setString(1, user);
            statement.setInt(2, id);
            statement.setInt(3, revision);
            statement.executeUpdate();
            statement.close();

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("ArticleRead/{user}/{id}")
    @DELETE
    public Response deleteArticleRead(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("DELETE FROM MarkedRead WHERE (Name = ? AND ID = ?)");
            statement.setString(1, user);
            statement.setInt(2, id);
            statement.executeUpdate();
            statement.close();

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("ArticleCollapsed/{user}/{id}")
    @PUT
    public Response putArticleCollapsed(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("INSERT IGNORE INTO Collapsed (Name, ID) VALUES (?, ?)");
            statement.setString(1, user);
            statement.setInt(2, id);
            statement.executeUpdate();
            statement.close();

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("ArticleCollapsed/{user}/{id}")
    @DELETE
    @Consumes("application/json")
    public Response deleteArticleCollapsed(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("DELETE FROM Collapsed WHERE (Name = ? AND ID = ?)");
            statement.setString(1, user);
            statement.setInt(2, id);
            statement.executeUpdate();
            statement.close();

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("ArticleContent/{id}/{revision}")
    @GET
    @Produces("application/json")
    public Response getArticleContent(@HeaderParam("Authorization") String authorization, @PathParam("id") int id, @PathParam("revision") int revision)
    {
        if (!authorize(authorization)) return notAuthorized();

        try
        {
            AppState app = AppState.getInstance();
            String content = app.FullTextArticleContent(id, revision);

            JSONObject result = new JSONObject();
            result.put("Content", content);

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
          return Response.serverError().entity(JSONError(exception)).build();
        }
    }

    @Path("Revision/{id}")
    @GET
    @Produces("application/json")
    public Response getRevision(@HeaderParam("Authorization") String authorization, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            JSONObject result = new JSONObject();

            PreparedStatement statement = connection.prepareStatement("SELECT Revision FROM Articles WHERE ID = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next())
                result.put("Revision", resultSet.getObject(1));
            resultSet.close();
            statement.close();

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Revision/{user}/{id}")
    @PUT
    @Consumes("application/json")
    public Response putRevision(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            int revision = root.getInt("Revision");
            int majorRevision = root.getInt("MajorRevision");
            java.sql.Timestamp date = parseDate(root.getString("Date"));
            boolean changed = root.getBoolean("Changed");

            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("UPDATE Articles SET Revision = ?, MajorRevision = ?, Date = ? WHERE (ID = ?)");
            statement.setInt(1, revision);
            statement.setInt(2, majorRevision);
            statement.setTimestamp(3, date);
            statement.setInt(4, id);
            statement.executeUpdate();
            statement.close();

            // mark article as read for the sender if it was marker read before
            if (!changed)
            {
                statement = connection.prepareStatement("REPLACE INTO MarkedRead (Name, ID, Revision) VALUES (?, ?, ?)");
                statement.setString(1, user);
                statement.setInt(2, id);
                statement.setInt(3, revision);
                statement.executeUpdate();
                statement.close();
            }

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("ArticleParent/{id}")
    @PUT
    @Consumes("application/json")
    public Response putArticleParent(String content, @HeaderParam("Authorization") String authorization, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            int parent = root.getInt("Parent");
            String strParent = parent >= 0 ? Integer.toString(parent) : null;

            connection = AppState.getConnection();

            PreparedStatement statement = connection.prepareStatement("UPDATE Articles SET Parent = ? WHERE (ID = ?)");
            statement.setString(1, strParent);
            statement.setInt(2, id);
            statement.executeUpdate();
            statement.close();

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Tag/{user}/{id}")
    @PUT
    @Consumes("application/json")
    public Response putTag(String content, @HeaderParam("Authorization") String authorization, @PathParam("user") String user, @PathParam("id") int id)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            String removeTag = root.optString("RemoveTag", null);
            String removePrivateTag = root.optString("RemovePrivateTag", null);
            String addTag = root.optString("AddTag", null);
            String addPrivateTag = root.optString("AddPrivateTag", null);

            connection = AppState.getConnection();

            if (removeTag != null)
            {
                PreparedStatement statement = connection.prepareStatement("DELETE FROM Tags WHERE (ID = ? AND Tag = ?)");
                statement.setInt(1, id);
                statement.setString(2, removeTag);
                statement.executeUpdate();
                statement.close();
            }
            if (removePrivateTag != null)
            {
                PreparedStatement statement = connection.prepareStatement("DELETE FROM PrivateTags WHERE (Name = ? AND ID = ? AND Tag = ?)");
                statement.setString(1, user);
                statement.setInt(2, id);
                statement.setString(3, removePrivateTag);
                statement.executeUpdate();
                statement.close();
            }
            if (addTag != null)
            {
                PreparedStatement statement = connection.prepareStatement("INSERT IGNORE INTO Tags (ID, Tag) VALUES (?, ?)");
                statement.setInt(1, id);
                statement.setString(2, addTag);
                statement.executeUpdate();
                statement.close();
            }
            if (addPrivateTag != null)
            {
                PreparedStatement statement = connection.prepareStatement("INSERT IGNORE INTO PrivateTags (Name, ID, Tag) VALUES (?, ?, ?)");
                statement.setString(1, user);
                statement.setInt(2, id);
                statement.setString(3, addPrivateTag);
                statement.executeUpdate();
                statement.close();
            }

            return Response.ok().build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("Benchmark")
    @GET
    @Produces("application/json")
    public Response getBenchmark(@HeaderParam("Authorization") String authorization)
    {
        if (!authorize(authorization)) return notAuthorized();

        Connection connection = null;
        try
        {
            connection = AppState.getConnection();

            JSONObject result = new JSONObject();

            int count = 0;
            long startMs = System.currentTimeMillis();
            {
              PreparedStatement statement = connection.prepareStatement("SELECT SQL_NO_CACHE COUNT(*) FROM Articles");
              ResultSet resultSet = statement.executeQuery();
              if (resultSet.next()) {
                count = resultSet.getInt(1);
              }
              resultSet.close();
              statement.close();
            }
            {
              int rand = (int)(Math.random() * count);
              PreparedStatement statement = connection.prepareStatement("SELECT SQL_NO_CACHE Revision FROM Articles LIMIT ?,1");
              statement.setInt(1, rand);
              ResultSet resultSet = statement.executeQuery();
              if (resultSet.next()) {
                result.put("Result", resultSet.getObject(1));
              }
              resultSet.close();
              statement.close();
            }
            long duration = System.currentTimeMillis()-startMs;
            result.put("QueryDuration",duration);

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
            return Response.serverError().entity(JSONError(exception)).build();
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }

    @Path("FullText/Search")
    @POST
    @Produces("application/json")
    public Response postFullTextSearch(String content, @HeaderParam("Authorization") String authorization)
    {
        if (!authorize(authorization)) return notAuthorized();

        try
        {
            JSONTokener reader = new JSONTokener(content);
            JSONObject root = new JSONObject(reader);
            String query = root.getString("Query");

            AppState app = AppState.getInstance();
            ArrayList<FullText.SearchResult> items = app.FullTextSearch(query);

            JSONArray result = new JSONArray();
            for (FullText.SearchResult item : items)
            {
              JSONObject obj = new JSONObject();
              result.put(obj);

              obj.put("Id", item._id);
              obj.put("Revision", item._revision);
              obj.put("Author", item._author);
              obj.put("Time", item._time);
              obj.put("Title", item._title);
              obj.put("Score", item._score);
            }

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
          return Response.serverError().entity(JSONError(exception)).build();
        }
    }

    @Path("FullText/CheckIndex")
    @GET
    @Produces("application/json")
    public Response getFullTextCheckIndex(@HeaderParam("Authorization") String authorization)
    {
        if (!authorize(authorization)) return notAuthorized();

        try
        {
            AppState app = AppState.getInstance();
            String message = app.FullTextCheckIndex();

            JSONObject result = new JSONObject();
            result.put("Result", message);

            return Response.ok(result.toString()).build();
        }
        catch (Exception exception)
        {
          return Response.serverError().entity(JSONError(exception)).build();
        }
    }

    // helper functions
    private java.sql.Timestamp parseDate(String strDate)
    {
        // update the string to make it parsable
        int index = strDate.indexOf('.');
        if (index < 0) return new java.sql.Timestamp(0);
        index++;
        StringBuilder builder = new StringBuilder(strDate.substring(0, index));
        // reduce the precision to miliseconds
        int count = 0;
        while (index < strDate.length())
        {
            char c = strDate.charAt(index);
            if (!Character.isDigit(c)) break;
            if (count < 3) builder.append(c);
            index++; count++;
        }
        // remove the semicolon from the time zone
        while (index < strDate.length())
        {
            char c = strDate.charAt(index);
            if (c != ':') builder.append(c);
            index++;
        }
        strDate = builder.toString();

        try
        {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            java.util.Date date = format.parse(strDate);
            return new java.sql.Timestamp(date.getTime());
        }
        catch (Exception exception)
        {
            return new java.sql.Timestamp(0);
        }
    }

    private String JSONError(Exception e)
    {
      try
      {
        return new JSONObject().put("error", e.toString()).toString();
      }
      catch (JSONException exception)
      {
        return "";
      }
    }

    private class AuthorizationInfo
    {
        String name;
        String password;
    };    

    private AuthorizationInfo decodeAuthorization(String authorization)
    {
        String authBasicPrefix = "Basic ";
        if (!authorization.startsWith(authBasicPrefix)) return null;
        String authEncoded = authorization.substring(authBasicPrefix.length());
        try
        {
            BASE64Decoder decoder = new BASE64Decoder();
            String auth = new String(decoder.decodeBuffer(authEncoded));

            int index = auth.indexOf(':');
            if (index < 0) return null;

            // check the user and password
            AuthorizationInfo authInfo = new AuthorizationInfo();
            authInfo.name = auth.substring(0, index);
            authInfo.password = auth.substring(index + 1);
            return authInfo;
        }
        catch (Exception exception)
        {
            return null;
        }
        
    }

    private boolean authorize(String authorization)
    {
        if (authorization == null) return false;
        AuthorizationInfo authInfo = decodeAuthorization(authorization);
        if (authInfo==null) return false;
        AppState app = AppState.getInstance();
        return app.IsValidUser(authInfo.name, authInfo.password);
    }

    private Response notAuthorized()
    {
        return Response.status(401).header("WWW-Authenticate",  "Basic realm=\"Secure Area\"").build();       
    }
}
