package com.bistudio.colabo;

import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.BindResult;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPSearchException;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import java.io.FileInputStream;
import java.net.URI;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.sql.DataSource;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jirka
 */

public class AppState
{
    private static long MS_PER_MINUTE = 60 * 1000;
    private AtomicReference<HashMap<String, String>> _users;
    private static final AppState _singleton = new AppState();
    private FullText _fullText;
    private Thread _updateThread = null;
    private Thread _indexThread = null;
    public final Object _wakeUpLongPoll = new Object();
    
    public static AppState getInstance()
    {
        return _singleton;
    }
    
    public static void WakeUpLongPoll()
    {
      Object obj = getInstance()._wakeUpLongPoll;
      synchronized (obj)
      {
        obj.notifyAll();
      }
    }

    public static Logger logger()
    {
       return Logger.getLogger(AppState.class.getName());
    }

    // thread used to regulary refresh the caches
    private class UpdateCacheThread extends Thread
    {
        private AppState _owner;

        public UpdateCacheThread(AppState owner)
        {
            _owner = owner;
        }
        @Override
        public void run()
        {
            while (!interrupted())
            {
                try
                {
                    sleep(5 * MS_PER_MINUTE);
                    _owner._users.set(_owner.LoadUsers());
                }
                catch (InterruptedException ex)
                {
                    // wake up and return
                    return;
                }
            }
        }
    }

    // thread used to regulary update the full text index
    private class UpdateFullTextIndexThread extends Thread
    {
        private AppState _owner;

        public UpdateFullTextIndexThread(AppState owner)
        {
            _owner = owner;
        }
        @Override
        public void run()
        {
            while (!interrupted())
            {
                try
                {
                    _owner._fullText.Update();
                    sleep(5 * MS_PER_MINUTE);
                }
                catch (InterruptedException ex)
                {
                    // wake up and return
                    return;
                }
            }
        }
    }

    private AppState()
    {
       // initialize logging
       try
       {
          String catalinaBase = System.getProperty("catalina.base");
          Handler fileHandler = new FileHandler(catalinaBase+"/logs/colabo.log");
          logger().addHandler(fileHandler);
          logger().setLevel(Level.INFO);
       }
       catch (Exception exception)
       {
          logger().log(Level.SEVERE, "Logger initialization failed", exception);
       }
       logger().info("Colabo server started");

       _users = new AtomicReference<HashMap<String, String>>(LoadUsers());
       String svnRoot = FindSVNRoot(); // search for the base for relative URLs in LDAP
       _fullText = new FullText(svnRoot);
       Start();
    }
    public synchronized void Start()
    {
      if (_updateThread == null)
      {
         _updateThread = new UpdateCacheThread(this);
         _updateThread.setDaemon(true);
         _updateThread.start();
      }
      if (_indexThread == null)
      {
         _indexThread = new UpdateFullTextIndexThread(this);
         _indexThread.setDaemon(true);
         _indexThread.start();
      }
    }
    public synchronized void Stop()
    {
        if (_updateThread != null)
        {
          _updateThread.interrupt();
          _updateThread = null;
        }
        if (_indexThread != null)
        {
          _indexThread.interrupt();
          _indexThread = null;
        }
    }

    public boolean IsValidUser(String name, String password)
    {
       String storedPassword = _users.get().get(name);
       if (storedPassword == null)
       {
           logger().log(Level.WARNING, "User {0} not found", name);
           return false;
       }

       String algorithm = null;
       if (storedPassword.startsWith("{SHA}"))
       {
         algorithm = "SHA-1";
         storedPassword = storedPassword.substring("{SHA}".length());
       }
       else if (storedPassword.startsWith("{MD5}"))
       {
         algorithm = "MD5";
         storedPassword = storedPassword.substring("{MD5}".length());
       }
       else if (storedPassword.startsWith("{CRYPT}"))
       {
         // own encryption implementation
         storedPassword = storedPassword.substring("{CRYPT}".length());
         String encodedPassword = org.eclipse.jetty.http.security.UnixCrypt.crypt(password, storedPassword);
         if (storedPassword.equals(encodedPassword)) return true;

         logger().log(Level.WARNING, "Unauthorized: {0} != {1}", new Object[] {encodedPassword, storedPassword});
         return false;
       }
       else if (storedPassword.startsWith("{SSHA}"))
       {
         storedPassword = storedPassword.substring("{SSHA}".length());
         algorithm = "SHA-1";

         try
         {
           BASE64Decoder decoder = new BASE64Decoder();
           byte[] hash_and_salt = decoder.decodeBuffer(storedPassword);
           if (hash_and_salt == null)
           {
             logger().log(Level.WARNING, "Password {0} not encoded correctly", storedPassword);
             return false;    // invalid base64 encoding...
           }
           if (hash_and_salt.length != 28)
           {
             logger().log(Level.WARNING, "Password {0} not encoded correctly", storedPassword);
             return false; // must be 20 bytes sha1 and 8 bytes salt
           }

           MessageDigest digest = MessageDigest.getInstance(algorithm);
           digest.update(password.getBytes("UTF-8")); // hash password
           digest.update(hash_and_salt, 20, 8);       // and salt
           byte[] passwordHash = digest.digest();
           for (int i=0; i<20; i++)
           {
             if (hash_and_salt[i] != passwordHash[i])
             {
                logger().log(Level.WARNING, "Unauthorized access for {0}, wrong SSHA password", name);
                return false;  // mismatched digest? Deny
             }
           }
           return true;    // digest matches, allow!
         }
         catch (Exception exception)
         {
           logger().log(Level.SEVERE, "Password check failed", exception);
           return false;
         }
       }

       if (algorithm == null)
       {
           logger().log(Level.WARNING, "Algorithm {0} not supported", storedPassword);
           return false;
       }

       // get password hash
       try
       {
           MessageDigest digest = MessageDigest.getInstance(algorithm);
           digest.update(password.getBytes("UTF-8"));
           byte[] passwordHash = digest.digest();

           BASE64Encoder encoder = new BASE64Encoder();
           String encodedPassword = encoder.encode(passwordHash);

           if (storedPassword.equals(encodedPassword.toString())) return true;

           logger().log(Level.WARNING, "Unauthorized: {0} != {1}", new Object[] {encodedPassword.toString(), storedPassword});
           return false;
        }
       catch (Exception exception)
       {
          logger().log(Level.SEVERE, "Password check failed", exception);
          return false;
       }
    }

    public ArrayList<FullText.SearchResult> FullTextSearch(String query)
    {
        return _fullText.Search(query);
    }

    public String FullTextCheckIndex()
    {
        return _fullText.CheckIndex();
    }

    public String FullTextArticleContent(int id, int revision)
    {
        return _fullText.ArticleContent(id, revision);
    }

    public static Connection getConnection(boolean autocommit) throws NamingException, SQLException
    {
        InitialContext ctx = new InitialContext();
        DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/Colabo");

        // http://tomcat.apache.org/tomcat-7.0-doc/jdbc-pool.html
        // The only state the pool itself inserts are defaultAutoCommit, defaultReadOnly, defaultTransactionIsolation, defaultCatalog if these are set.
        // These 4 properties are only set upon connection creation. Should these properties be modified during the usage of the connection,
        // the pool itself will not reset them.
        Connection connection = ds.getConnection();
        connection.setAutoCommit(autocommit);
        return connection;
        
    }
    public static Connection getConnection() throws NamingException, SQLException
    {
        return getConnection(true);
    }
    public static void closeConnection(Connection connection)
    {
        try
        {
            if (connection != null) connection.close();
        }
        catch (SQLException ex)
        {
            // cannot close, connection is probably no longer valid, ignore
        }
    }

    private String FindSVNRoot() 
    {
        try 
        {
            String svnRoot = null;
            InitialContext ctx = new InitialContext();
            String ldapDBDN = (String)ctx.lookup("java:comp/env/ldapDBDN");
            LDAPConnection ldapConnection = createLDAPConnection(ctx);
            if (ldapConnection != null)
            {
                SearchResultEntry db = ldapConnection.searchForEntry(ldapDBDN, SearchScope.BASE, "(objectClass=*)", "colaboSVNRoot");
                if (db != null)
                    svnRoot = db.getAttributeValue("colaboSVNRoot");
                ldapConnection.close(); 
            }
            return svnRoot;
        }
        catch (Throwable ex) 
        {
            Logger.getLogger(AppState.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }


    private HashMap<String, String> LoadUsers()
    {
        HashMap<String, String> users = new HashMap<String, String>();

        // check if LDAP is configured
        String ldapDBDN = null;
        LDAPConnection ldapConnection = null;
        try
        {
            InitialContext ctx = new InitialContext();
            ldapDBDN = (String)ctx.lookup("java:comp/env/ldapDBDN");
            ldapConnection = createLDAPConnection(ctx);
        }
        catch (NamingException exception)
        {
            // ldapConnection == null
        }

        if (ldapConnection != null)
        {
            try
            {
                // load directly from LDAP into users and the local structures (mirror to the database then)
                ArrayList<SimpleUserInfo> usersToSave = new ArrayList<SimpleUserInfo>();
                ArrayList<SimpleGroupInfo> groupsToSave = new ArrayList<SimpleGroupInfo>();
                ArrayList<SimpleGroupUserInfo> groupUsersToSave = new ArrayList<SimpleGroupUserInfo>();

                // load database users
                SearchResult result = ldapConnection.search("ou=users," + ldapDBDN, SearchScope.SUB, "(objectClass=colaboUser)", "cn", "colaboAccessRights", "colaboUserDN");
                for (int i = 0; i < result.getEntryCount(); i++)
                {
                    SearchResultEntry entry = result.getSearchEntries().get(i);
                    String cn = entry.getAttributeValue("cn");
                    if (cn == null) continue;
                    String dn = entry.getAttributeValue("colaboUserDN");
                    if (dn == null) continue;
                    String access = entry.getAttributeValue("colaboAccessRights");
                    // ask the dn for the password
                    SearchResultEntry user = ldapConnection.searchForEntry(dn, SearchScope.BASE, "(objectClass=*)", "userPassword");
                    if (user != null)
                    {
                        String password = user.getAttributeValue("userPassword");
                        if (password != null) users.put(cn, password);

                        SimpleUserInfo info = new SimpleUserInfo(cn);
                        if (access != null) info._access = access;
                        if (password != null) info._password = password;
                        usersToSave.add(info);
                    }
                }

                // load database groups
                LoadGroups(ldapConnection, ldapDBDN, users, usersToSave, groupsToSave, groupUsersToSave);
                ldapConnection.close();

                // mirror the internal structures to the SQL database
                // when no users are present, something is wrong maybe and do not save then
                if (!usersToSave.isEmpty())
                {
                    SaveUsers(usersToSave, groupsToSave, groupUsersToSave);
                }

                logger().log(Level.INFO, "{0} users loaded from LDAP", users.size());
            }
            catch (LDAPSearchException exception)
            {
              logger().log(Level.SEVERE, "Users loading failed", exception);
              return users;
            }
        }
        else
        {
            // load from the SQL mirror
            Connection connection = null;
            try
            {
                connection = getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT ID, Password FROM Users");

                while (resultSet.next())
                {
                    String id = resultSet.getString(1);
                    if (id != null)
                    {
                        String password = resultSet.getString(2);
                        if (password == null) password = "";
                        users.put(id, password);
                    }
                }

                resultSet.close();
                statement.close();

                logger().log(Level.INFO, "{0} users loaded from SQL", users.size());
            }
            catch (Exception exception)
            {
              logger().log(Level.SEVERE, "Users loading failed", exception);
              return users;
            }
            finally
            {
                closeConnection(connection);
            }
        }

        return users;
    }
    private void LoadGroups(LDAPConnection ldapConnection, String ldapDBDN,
      HashMap<String, String> users, ArrayList<SimpleUserInfo> usersToSave,
      ArrayList<SimpleGroupInfo> groupsToSave, ArrayList<SimpleGroupUserInfo> groupUsersToSave)
    {
        try
        {
            SearchResult result = ldapConnection.search("ou=groups," + ldapDBDN, SearchScope.SUB, "(objectClass=colaboGroup)", "cn", "description", "colaboAccessRights", "colaboGroupLevel", "colaboHomePage", "memberUid", "member");
            for (int i = 0; i < result.getEntryCount(); i++)
            {
                SearchResultEntry entry = result.getSearchEntries().get(i);
                String cn = entry.getAttributeValue("cn");
                if (cn == null) continue;
                SimpleGroupInfo group = new SimpleGroupInfo(cn);

                String value = entry.getAttributeValue("description");
                if (value != null) group._name = value;
                value = entry.getAttributeValue("colaboAccessRights");
                if (value != null) group._access = value;
                value = entry.getAttributeValue("colaboGroupLevel");
                if (value != null)
                {
                    try {group._level = Integer.parseInt(value);}
                    catch (NumberFormatException exception) {}
                }
                value = entry.getAttributeValue("colaboHomePage");
                if (value != null)
                {
                    try {group._home = Integer.parseInt(value);}
                    catch (NumberFormatException exception) {}
                }
                groupsToSave.add(group);

                Attribute attributes = entry.getAttribute("memberUid");
                if (attributes != null)
                {
                    String[] values = attributes.getValues();
                    for (int j = 0; j < values.length; j++)
                        groupUsersToSave.add(new SimpleGroupUserInfo(cn, values[j]));
                }

                attributes = entry.getAttribute("member");
                if (attributes != null)
                {
                    String[] values = attributes.getValues();
                    for (int j = 0; j < values.length; j++)
                        CollectMembers(ldapConnection, values[j], cn, users, usersToSave, groupUsersToSave);
                }
            }
        }
        catch (LDAPSearchException exception)
        {
          logger().log(Level.SEVERE, "Groups loading failed", exception);
        }
    }
    private void CollectMembers(LDAPConnection ldapConnection, String dn, String groupId,
      HashMap<String, String> users, ArrayList<SimpleUserInfo> usersToSave,
      ArrayList<SimpleGroupUserInfo> groupUsersToSave)
      throws LDAPSearchException
    {
        SearchResultEntry entry = ldapConnection.searchForEntry(
            dn, SearchScope.BASE, "(objectClass=*)", "uid", "userPassword", "member");
        if (entry != null)
        {
            String userId = entry.getAttributeValue("uid");
            if (userId != null)
            {
                String password = entry.getAttributeValue("userPassword");

                if (!users.containsKey(userId)) users.put(userId, password);
                if (!Contains(usersToSave, userId))
                {
                    SimpleUserInfo info = new SimpleUserInfo(userId);
                    if (password != null) info._password = password;
                    usersToSave.add(info);
                }
                if (!Contains(groupUsersToSave, groupId, userId))
                {
                    groupUsersToSave.add(new SimpleGroupUserInfo(groupId, userId));
                }
            }

            Attribute attributes = entry.getAttribute("member");
            if (attributes != null)
            {
                String[] values = attributes.getValues();
                for (int j = 0; j < values.length; j++)
                    CollectMembers(ldapConnection, values[j], groupId, users, usersToSave, groupUsersToSave);
            }
        }
    }
    private boolean Contains(ArrayList<SimpleUserInfo> users, String userId)
    {
        for (int i=0; i<users.size(); i++)
        {
            if (userId.equals(users.get(i)._id)) return true;
        }
        return false;
    }
    private boolean Contains(ArrayList<SimpleGroupUserInfo> users, String groupId, String userId)
    {
        for (int i=0; i<users.size(); i++)
        {
            if (groupId.equals(users.get(i)._group) && userId.equals(users.get(i)._user)) return true;
        }
        return false;
    }
    private void SaveUsers(ArrayList<SimpleUserInfo> users, ArrayList<SimpleGroupInfo> groups, ArrayList<SimpleGroupUserInfo> groupUsers)
    {
        Connection connection = null;

        try
        {
            // handle all changes in a single transaction
            connection = getConnection(false);

            // users
            if (users.size() > 0)
            {
                // delete removed users
                String mask = "?";
                for (int i=1; i<users.size(); i++) mask += ", ?";
                String command  = "DELETE FROM Users WHERE ID NOT IN(" + mask + ")";
                PreparedStatement statement = connection.prepareStatement(command);
                for (int i=0; i<users.size(); i++)
                {
                    statement.setString(i + 1, users.get(i)._id);
                }
                statement.executeUpdate();
                statement.close();

                // add new users
                statement = connection.prepareStatement("INSERT IGNORE INTO Users (ID) VALUES (?)");
                for (int i=0; i<users.size(); i++)
                {
                    statement.setString(1, users.get(i)._id);
                    statement.addBatch();
                }
                statement.executeBatch();
                statement.close();

                // update users
                statement = connection.prepareStatement("UPDATE Users SET AccessRights = ?, Password = ? WHERE (ID = ?)");
                for (int i=0; i<users.size(); i++)
                {
                    SimpleUserInfo user = users.get(i);
                    statement.setString(1, user._access);
                    statement.setString(2, user._password);
                    statement.setString(3, user._id);
                    statement.addBatch();
                }
                statement.executeBatch();
                statement.close();
            }

            // groups
            // delete groups
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Groups");
            statement.executeUpdate();
            statement.close();

            if (groups.size() > 0)
            {
                // add groups
                statement = connection.prepareStatement("INSERT IGNORE INTO Groups (ID, FullName, AccessRights, Level, Homepage) VALUES (?, ?, ?, ?, ?)");
                for (int i=0; i<groups.size(); i++)
                {
                    SimpleGroupInfo group = groups.get(i);
                    statement.setString(1, group._id);
                    statement.setString(2, group._name);
                    statement.setString(3, group._access);
                    statement.setInt(4, group._level);
                    statement.setInt(5, group._home);
                    statement.addBatch();
              }
                statement.executeBatch();
                statement.close();
            }

            // group users
            // delete group users
            statement = connection.prepareStatement("DELETE FROM GroupUsers");
            statement.executeUpdate();
            statement.close();

            if (groupUsers.size() > 0)
            {
                statement = connection.prepareStatement("INSERT IGNORE INTO GroupUsers (GroupID, UserID) VALUES (?, ?)");
                for (int i=0; i<groupUsers.size(); i++)
                {
                    SimpleGroupUserInfo groupUser = groupUsers.get(i);
                    statement.setString(1, groupUser._group);
                    statement.setString(2, groupUser._user);
                    statement.addBatch();
                }
                statement.executeBatch();
                statement.close();
            }

            connection.commit();
        }
        catch (Exception exception)
        {
            if (connection != null)
            {
                try
                {
                    connection.rollback();
                }
                catch (SQLException e)
                {
                }
            }
        }
        finally
        {
            closeConnection(connection);
        }
    }
    private class SimpleUserInfo
    {
      public String _id;
      public String _access;
      public String _password;
      public SimpleUserInfo(String id)
      {
        _id = id; _access = ""; _password = "";
      }
    };
    public class SimpleGroupInfo
    {
      public String _id;
      public String _name;
      public String _access;
      public int _level;
      public int _home;
      public SimpleGroupInfo(String id)
      {
        _id = id; _name = ""; _access = "";
        _level = 0; _home = 0;
      }
    };
    public class SimpleGroupUserInfo
    {
      public String _group;
      public String _user;
      public SimpleGroupUserInfo(String group, String user)
      {
        _group = group; _user = user;
      }
    };

    // helper to create a LDAP connection
    private LDAPConnection createLDAPConnection(InitialContext ctx)
    {
        try
        {
            String host = (String)ctx.lookup("java:comp/env/ldapHost");
            int port = (Integer)ctx.lookup("java:comp/env/ldapPort");
            boolean secure = (Boolean)ctx.lookup("java:comp/env/ldapSecure");
            String bindDN = (String)ctx.lookup("java:comp/env/ldapBindDN");
            String bindPassword = (String)ctx.lookup("java:comp/env/ldapBindPassword");

            LDAPConnectionOptions ops = new LDAPConnectionOptions();
            ops.setAutoReconnect(true);
            ops.setBindWithDNRequiresPassword(true);
            ops.setResponseTimeoutMillis(0);
            ops.setUseKeepAlive(true);
            ops.setUseTCPNoDelay(true);

            LDAPConnection c = null;
            if (secure)
            {
                String certificate = (String)ctx.lookup("java:comp/env/ldapCACertificate");

                KeyStore ks = KeyStore.getInstance("JKS");
                FileInputStream stream = new FileInputStream(certificate);
                try
                {
                    ks.load(stream, "changeit".toCharArray());
                }
                finally
                {
                    stream.close();
                }

                TrustManagerFactory tmf = TrustManagerFactory.getInstance("PKIX");
                tmf.init(ks);

                SSLContext sslcx = SSLContext.getInstance("TLS");
                sslcx.init(null, tmf.getTrustManagers(), new SecureRandom());

                SocketFactory sf = sslcx.getSocketFactory();

                c = new LDAPConnection(sf,ops);
            }
            else
            {
                c = new LDAPConnection(ops);
            }

            c.connect(host, port);
            BindResult result = c.bind(bindDN, bindPassword);
            return c;
        }
        catch (Exception exception)
        {
            // LDAP connection cannot be created now
            AppState.logger().log(Level.SEVERE, "LDAP connection failed", exception);
            return null;
        }
    }
}
