/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.colabo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.TermDocs;
import org.apache.lucene.index.TermEnum;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.tmatesoft.svn.core.ISVNLogEntryHandler;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.io.SVNRepository;
import org.tmatesoft.svn.core.io.SVNRepositoryFactory;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

/**
 *
 * @author Jirka
 */
public class FullText
{
    private class ArticleInfo
    {
        public int _id;
        public String _title;
        public String _url;
    }
    private class RevisionInfo
    {
        public long _revision;
        public String _author;
        public Date _time;
        // TODO: serialization
    }
    private class SVNCache implements ISVNLogEntryHandler
    {
        public long _lastRevision;
        public HashMap<String, ArrayList<RevisionInfo>> _revisions;

        public SVNCache()
        {
            _lastRevision = 0;
            _revisions = new HashMap<String, ArrayList<RevisionInfo>>();
        }
        private void Add(String url, RevisionInfo revision)
        {
            if (revision._revision > _lastRevision) _lastRevision = revision._revision;

            ArrayList<RevisionInfo> list = _revisions.get(url);
            if (list == null)
            {
                list = new ArrayList<RevisionInfo>();
                _revisions.put(url, list);
            }
            list.add(revision);
        }

        public void handleLogEntry(SVNLogEntry entry) throws SVNException
        {
            RevisionInfo info = new RevisionInfo();
            info._revision = entry.getRevision();
            info._author = entry.getAuthor();
            info._time = entry.getDate();
            for (Object obj : entry.getChangedPaths().keySet())
            {
                String path = (String)obj;
                Add(path, info);
            }
        }

        // TODO: serialization
    }

    // collection of articles in a SVN repository
    private class RepositoryInfo
    {
        public SVNRepository _repository;
        // list of articles contained in this directory
        public ArrayList<ArticleInfo> _articles;
        // list of all revisions of all files present in this SVN repository
        public SVNCache _svnCache;
        // location repository is constructed for
        public String _location;
        // base for relative paths in _svnCache
        public String _root;

        public RepositoryInfo(SVNRepository repository)
        {
            _repository = repository;
            _articles = new ArrayList<ArticleInfo>();
            _svnCache = new SVNCache();

            _location = repository.getLocation().toString();
            try
            {
                _root = repository.getRepositoryRoot(false).toString();
            }
            catch (SVNException exception)
            {
               AppState.logger().log(Level.SEVERE, "SVN repository root not found", exception);
            }
        }

        private void UpdateCache()
        {
            try
            {
long start = System.currentTimeMillis();
                _repository.log(new String[]{""}, _svnCache._lastRevision, -1L, true, true, _svnCache);
long diff = System.currentTimeMillis() - start;
AppState.logger().log(Level.INFO, "SVN Log received in {0} ms", diff);
            }
            catch (SVNException exception)
            {
                AppState.logger().log(Level.SEVERE, "SVN Log failed", exception);
            }
            catch (Exception exception)
            {
                AppState.logger().log(Level.SEVERE, "SVN Log failed", exception);
            }
        }
        private void IndexArticles(IndexWriter writer) throws IOException
        {
long start = System.currentTimeMillis();
int total = 0;
            IndexReader reader = writer.getReader();
            int rootLength = _root.length();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            for (ArticleInfo article : _articles)
            {
              // check if SVN info is present
              if (!article._url.startsWith(_root))
              {
                AppState.logger().log(Level.WARNING, "Article ''{0}'' not found in repository ''{1}''", new Object[]{article._url, _root});
                continue;
              }
              // find revisions in SVN
              String path = article._url.substring(rootLength);
              ArrayList<RevisionInfo> revisions = _svnCache._revisions.get(path);
              if (revisions == null) continue; // no revisions in SVN
              // find revisions in Index
              HashSet<Long> inIndex = new HashSet<Long>();
              String prefix = article._id + ":";
              TermEnum terms = reader.terms(new Term(_fieldName, prefix));
              while (terms.term() != null && terms.term().text().startsWith(prefix))
              {
                String rest = terms.term().text().substring(prefix.length());
                long revision = Long.parseLong(rest);
                inIndex.add(revision);
                terms.next();
              }
              terms.close();
              // index all revisions not indexed yet
              for (RevisionInfo revision : revisions)
              {
                if (!inIndex.contains(revision._revision))
                {
                  ByteArrayOutputStream stream = new ByteArrayOutputStream();
                  try
                  {
                    _repository.getFile(path, revision._revision, null, stream);

                    Document doc = new Document();
                    // unique document identifier
                    String name = article._id + ":" + revision._revision;
                    doc.add(new Field(_fieldName, name, Field.Store.YES, Field.Index.NOT_ANALYZED));
                    doc.add(new Field(_fieldAuthor, revision._author.toLowerCase(), Field.Store.YES, Field.Index.NOT_ANALYZED));
                    doc.add(new Field(_fieldTime, dateFormat.format(revision._time), Field.Store.YES, Field.Index.NOT_ANALYZED));
                    doc.add(new Field(_fieldTitle, article._title, Field.Store.YES, Field.Index.ANALYZED));
                    doc.add(new Field(_fieldContent, stream == null ? "" : stream.toString("UTF-8"), Field.Store.YES, Field.Index.ANALYZED));
                    writer.addDocument(doc);
total++;
                  }
                  catch (SVNException exception)
                  {
                      AppState.logger().log(Level.WARNING, "Cannot read revision: {0}", exception.getMessage());
                  }
                }
              }
            }
            reader.close();
long diff = System.currentTimeMillis() - start;
AppState.logger().log(Level.INFO, "{0} articles indexed in {1} ms", new Object[]{total, diff});

        }
    }

    public class SearchResult
    {
        public int _id;
        public int _revision;
        public String _author;
        public String _time;
        public String _title;
        // do not send the content of all revisions - client will ask for it when needed only
        // public String _content;
        public float _score;
    };
    private class SearchResultInternal
    {
        public int _doc;
        public int _id;
        public int _revision;
        public float _score;
    };

    private class SortByRevision implements Comparator
    {
        public int compare(Object o1, Object o2)
        {
            SearchResultInternal a = (SearchResultInternal)o1;
            SearchResultInternal b = (SearchResultInternal)o2;
            return b._revision - a._revision; // newer revisions first
        }
    };
    private class SortByScore implements Comparator
    {
        public int compare(Object o1, Object o2)
        {
            SearchResult a = (SearchResult)o1;
            SearchResult b = (SearchResult)o2;
            int diff = Float.compare(b._score, a._score); // higher score first
            if (diff != 0) return diff;
            diff = a._id - b._id; // lower id first
            if (diff != 0) return diff;
            return b._revision - a._revision; // newer revisions first
        }
    };

    private Directory _directory;
    private Analyzer _analyzer;

    private static String _fieldName = "name";
    private static String _fieldAuthor = "author";
    private static String _fieldTime = "time";
    private static String _fieldTitle = "title";
    private static String _fieldContent = "content";

    private String _svnRoot = null;
    
    public FullText(String svnRoot)
    {
        _analyzer = new StandardAnalyzer(Version.LUCENE_36);
        _directory = null;
        _svnRoot = svnRoot;

        String dir;
        try
        {
          InitialContext ctx = new InitialContext();
          dir = (String)ctx.lookup("java:comp/env/indexPath");
          _directory = FSDirectory.open(new File(dir));
        }
        catch (NamingException exception)
        {
          AppState.logger().log(Level.SEVERE, "Index path not configured", exception);
        }
        catch (IOException exception)
        {
          AppState.logger().log(Level.SEVERE, "Cannot open index", exception);
        }
    }

    ArrayList<SearchResult> Search(String query)
    {
        ArrayList<SearchResult> results = new ArrayList<SearchResult>();
        if (_directory != null)
        {
            try
            {
                QueryParser parser = new QueryParser(Version.LUCENE_36, "content", _analyzer);
                Query q = parser.parse(query);
                IndexSearcher searcher = new IndexSearcher(_directory);

                // collect the basic info about the query results (_doc, _score)
                final ArrayList<SearchResultInternal> resultsInternal = new ArrayList<SearchResultInternal>();
                searcher.search(q, new Collector()
                {
                    private int _docBase;
                    private Scorer _scorer;
                    @Override
                    public void setScorer(Scorer scorer) throws IOException 
                    {
                        _scorer = scorer;
                    }
                    @Override
                    public void collect(int doc) throws IOException 
                    {
                        float score = _scorer.score();
                        if (score > 0)
                        {
                          SearchResultInternal result = new SearchResultInternal();
                          result._doc = _docBase + doc;
                          result._id = -1;
                          result._revision = 0;
                          result._score = score;
                          resultsInternal.add(result); 
                        }
                    }
                    @Override
                    public void setNextReader(IndexReader reader, int docBase) throws IOException 
                    {
                        _docBase = docBase;
                    }
                    @Override
                    public boolean acceptsDocsOutOfOrder() 
                    {
                        return true;
                    }
                });

                // extend the results info where possible (_id, _revision)
                for (SearchResultInternal result : resultsInternal)
                {
                    Document doc = searcher.doc(result._doc);
                    String name = doc.get(_fieldName);
                    int index = name.indexOf(':');
                    if (index >= 0)
                    {
                        try
                        {
                            result._id = Integer.parseInt(name.substring(0, index));
                            result._revision = Integer.parseInt(name.substring(index + 1));
                        }
                        catch (Exception exception)
                        {
                            AppState.logger().log(Level.SEVERE, "Wrong document name: " + name, exception);
                        }
                    }
                }

                // optimize result - remove older revisions of articles with lower score
                // create Dictionary of article revisions
                HashMap<Integer, ArrayList<SearchResultInternal>> dictionary = new HashMap<Integer, ArrayList<SearchResultInternal>>();
                for (SearchResultInternal result : resultsInternal)
                {
                  ArrayList<SearchResultInternal> list = dictionary.get(result._id);
                  if (list == null)
                  {
                      list = new ArrayList<SearchResultInternal>();
                      dictionary.put(result._id, list);
                  }
                  list.add(result);
                }
                resultsInternal.clear();

                // optimize each list of revisions
                for (ArrayList<SearchResultInternal> list : dictionary.values())
                {
                    Collections.sort(list, new SortByRevision());
                    // remove older revisions
                    for (int i = 0; i < list.size(); i++)
                    {
                        float score = list.get(i)._score;
                        for (int j = i + 1; j < list.size();)
                        {
                            if (list.get(j)._score <= score) list.remove(j);
                            else j++;
                        }
                    }
                    // add revisions to results
                    for (SearchResultInternal src : list)
                    {
                        SearchResult dst = new SearchResult();
                        dst._id = src._id;
                        dst._revision = src._revision;
                        dst._score = src._score;
                        // fill rest of fields (none will be deleted from now)
                        Document doc = searcher.doc(src._doc);
                        dst._author = doc.get(_fieldAuthor);
                        dst._time = doc.get(_fieldTime);
                        dst._title = doc.get(_fieldTitle);
                        // dst._content = doc.get(_fieldContent);
                        results.add(dst);
                    }
                }
                dictionary.clear();
                searcher.close(); // no more needed

                // sort results by the score again
                Collections.sort(results, new SortByScore());
                return results;
            }
            catch (Exception exception)
            {
                AppState.logger().log(Level.SEVERE, "Cannot perform search", exception);
            }
        }
        return results;
    }

    public String CheckIndex()
    {
        if (_directory == null) return "Invalid full text directory";
        try
        {
            IndexReader reader = IndexReader.open(_directory);
            int total = reader.numDocs();
            int invalid = 0;
            for (int i = 0; i < total; i++)
            {
              Document doc = reader.document(i);
              String name = doc.get(_fieldName);
              if (name == null) invalid++;
            }
            if (invalid > 0) return total + " documents, " + invalid + " invalid";
            else return total + " documents";
        }
        catch (Exception exception)
        {
            return "Cannot read full text index";
        }
    }

    public String ArticleContent(int id, int revision)
    {
        String content = "";
        if (_directory != null)
        {
            try
            {
                IndexReader reader = IndexReader.open(_directory);
                String name = id + ":" + revision;
                TermDocs terms = reader.termDocs(new Term(_fieldName, name));
                if (terms != null && terms.next())
                {
                    int i = terms.doc();
                    Document doc = reader.document(i);
                    content = doc.get(_fieldContent);
                }
                terms.close();
                reader.close();
            }
            catch (Exception exception)
            {
                AppState.logger().log(Level.SEVERE, "Cannot access document", exception);
            }
        }
        return content;
    }

    public void Update()
    {
        if (_directory != null)
        {
long start = System.currentTimeMillis();
          ArrayList<RepositoryInfo> repositories = new ArrayList<RepositoryInfo>();

          // create object for each repository
          InitRepositories(repositories);
          // create a SVN cache for each repository
          for (RepositoryInfo repository : repositories)
              repository.UpdateCache();
          // distribute articles into repositories
          DistributeArticles(repositories);
          // index all what need to be indexed
          try
          {
            IndexWriter writer = new IndexWriter(_directory, _analyzer, IndexWriter.MaxFieldLength.UNLIMITED);
            for (RepositoryInfo repository : repositories)
              repository.IndexArticles(writer);
            writer.close();
          }
          catch (Exception exception)
          {
            AppState.logger().log(Level.SEVERE, "Cannot write to index", exception);
          }
long diff = System.currentTimeMillis() - start;
AppState.logger().log(Level.INFO, "Total full index built in {0} ms", diff);

/*        
            ArrayList<SearchResult> search = Search("client server command line");
            for(SearchResult res:search)
            {
                AppState.logger().log(Level.INFO, "{0} : {1}", new Object[]{res._title, res._score});
            }
*/        
        }
    }

    private void InitRepositories(ArrayList<RepositoryInfo> repositories)
    {
       // create list of SVN repositories application has access to
       try
       {
           DAVRepositoryFactory.setup();
           InitialContext ctx = new InitialContext();
           for (int i=1;;i++)
           {
               String path = (String)ctx.lookup("java:comp/env/svnURL." + i);
               String name = (String)ctx.lookup("java:comp/env/svnName." + i);
               String password = (String)ctx.lookup("java:comp/env/svnPassword." + i);
               if (path == null || name == null || password == null) break; // no more repositories

               SVNURL url = SVNURL.parseURIDecoded(path);
               ISVNAuthenticationManager auth = SVNWCUtil.createDefaultAuthenticationManager(name, password);
               SVNRepository repository = SVNRepositoryFactory.create(url);
               repository.setAuthenticationManager(auth);

               AppState.logger().log(Level.INFO, "Repository root: {0}", repository.getRepositoryRoot(true));

               repositories.add(new RepositoryInfo(repository));
           }
       }
       catch (NamingException exception)
       {
           // no more repositories
           return;
       }
       catch (SVNException exception)
       {
           AppState.logger().log(Level.SEVERE, "SVN repository unaccessible", exception);
       }
    }

    private void DistributeArticles(ArrayList<RepositoryInfo> repositories)
    {
        Connection connection = null;
        try
        {
            // distribute articles into repositories
            connection = AppState.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT ID, Title, URL FROM Articles");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
              ArticleInfo article = new ArticleInfo();
              article._id = resultSet.getInt(1);
              article._title = resultSet.getString(2);
              String url = resultSet.getString(3);
              if (_svnRoot != null) // resolve the absolute URL
              {
                  URI test = URI.create(url);
                  if (!test.isAbsolute()) url = _svnRoot + url;
              }
              article._url = url;
              // add to the correct repository
              RepositoryInfo owner = null;
              for (RepositoryInfo repository : repositories)
              {
                if (article._url.startsWith(repository._location))
                {
                    owner = repository;
                    break;
                }
              }
              if (owner != null) owner._articles.add(article);
              else
              {
                // warned too much
                // AppState.logger().log(Level.WARNING, "Article ''{0}'' will not be indexed (out of subscribed repositories).", article._url);
              }
            }
            resultSet.close();
            statement.close();
        }
        catch (Exception exception)
        {
           AppState.logger().log(Level.SEVERE, "SQL server unaccessible", exception);
        }
        finally
        {
            AppState.closeConnection(connection);
        }
    }
}
