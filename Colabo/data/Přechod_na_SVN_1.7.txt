﻿Chceme postupně přejít na SVN 1.7. Slibujeme si od toho zejména zrychlení některých operací v Colabu a SVN obecně (po dokončení celého procesu).

Pro klienty znamená přechod následující kroky:

1. Každému uživateli Colaba se při updatu (až bude verze zveřejněna) nainstaluje exe, které už používá klienta 1.7. Svá data by si Colabo mělo samo převést. Po updatu by mělo fungovat vše až na /SVN log/ a /Blame/ (které používají TortoiseSVN).

Kdo chce nové SVN nebo Colabo používat intenzivněji, měl by pokračovat kroky:

2. Ještě s Tortoise 1.6 se na všech adresářích s working copy provést operaci Clean up pro zajištění budoucího bezproblémového převodu. (v kontextovém menu adresáře s working copy provést ( TortoiseSVN / Clean up... ))

3. Instalace TortoiseSVN 1.7.0 z http://tortoisesvn.net/downloads.html

4. Kdo používá AnkhSVN, instaluje daily build (stable release prozatím není) AnkhSVN 2.3 z http://ankhsvn.open.collab.net/daily

5. Kdo používá command line klienta, najde jej na http://www.collab.net/downloads/subversion/.

6. Nakonec je třeba pomocí TortoiseSVN převést všechny working copy z 1.6 na 1.7 (podrobný popis v http://tortoisesvn.net/tsvn_1.7_releasenotes.html, v krátkosti by mělo stačit provést operaci ( SVN Upgrade working copy ), která se nabízí v kontextovém menu adresáře s working copy).
[jirka 13.10.2011 9:59:19]:
 Kdo chce Colabo místo updatu překládat ze zdrojových kódů, musí postup mírně modifikovat:
 - nejdříve provést Clean up a instalovat TortoiseSVN 1.7
 - převést zdrojové kódy Colaba do working copy 1.7
 - pak teprve build projektu uspěje
 
 (překlad používá utilitu SVNRevision.exe pro zjištění čísla revize zdrojáků, ta už je postavena na SharpSVN 1.7)
 
