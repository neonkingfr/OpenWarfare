﻿> > Protoze je konec mesice, uvedomil jsem si, ze by bylo dobre aby se pomoci Colaba dala nejak evidovat pracovni doba jednotlivych zamestnancu.
> 
> Kritické rozhodnutí pak je, dle čeho evidovat? Nějaký explicitní login/logout, nebo nějaká automatická heuristika (např. docela rozumné výsledky dává: když běží Colabo, pracuji, když ne, nepracuji?)

Pro zacatek bych klidne nechal uzivatele zadavat odchod a prichod rucne. 
> 
> Co pracovní přestávky (oběd)?

To stejne, klidne manualne.
> 
> Dále: v jaké podobě by se evidence děla? Nějaká speciální databáze? Bylo by to nějak provázané s kalendářem?

Me by se asi nejvice libilo, kdyz by byl dalsi list vedle stavajicich Task a Calendar. Kdyz by se u nekoho dochazka neevidovala (externiste), proste by byl list skryty (neaktivni).

> 
> Jak by bylo možné evidenci zpětně editovat a doplňovat další údaje (nemoci, dovolené)?

Uz pro tak caste chyby v dochazkach a vykazech urcite bude nutne mit moznost zaznam editovat zpetne, kdyz by se udelal export, 
tak by se exportovala jeste treba nekam i revize, at se pozna ze je to jina verze.
