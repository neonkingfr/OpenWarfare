﻿> Pokusil jsem se o zobrazení rozdílu po slovech (revize 61539).
> 
> Slovem rozumím souvislý úsek mezi whitespaces, slovo je platné jen pokud obsahuje písmeno nebo číslo. Rozdíly vždy začínají a končí platným slovem.
> 
> Vyzkoušejte prosím, zda se to takhle chová lépe.

Už mě to napadlo dříve, ale Rola mi to včera cestou připomněl a rozvinul. Možná by bylo vhodnější rozdíly nezjišťovat ve zdrojovém markdown textu, ale až ve výsledném HTML. Zde totiž máme lépe odlišený obsah od formy (elementy, CDATA sekce).

Také bychom se asi měli zamyslet nad tím, mezi kterými revizemi změny zjišťovat. Nyní se diference přímo počítají mezi poslední přečtenou a aktuální revizí (tj. mezilehlými revizemi se nezabýváme). Tím ale za každé situace nedosáhneme toho, abychom znali autora změny.

Zjišťovat změny postupně po revizích spíše připomíná Blame a to by asi bylo technicky i výkonostně o dost náročnější. Dávám k úvaze, zda o to stojíme natolik abychom delší prodlevu a větší pracnost řešení zkousli.