﻿> Může se stát, že o uživateli budeme mít konfliktní informace, a sice když uživatel bude současně komunikovat ze dvou různých klientů. Může se třeba stát, že někdo nechá doma zapnutý počítač s Colabem a v práci zapne druhý, nebo že souběžně bude do IM přispívat z tabletu přes XMPP klienta.

Dokážu reprodukovat jednoduchou situaci, kdy si uživatel spustí více instancí na stejném počítači. Instance mají stejný GUID (ten je přidělen trvale) a tak se mezi sebou perou.

Možná bych místo (nebo kromě) GUIDu klienta mohl používat číslo session - při každém spuštění Colaba by uživatel při přihlášení zahájil novou session. Číslo session by bylo jednoduché počítadlo v SQL tabulce.

Jiná možnost je, aby se session mezi sebou koordinovaly na straně klienta, nebo by to klient ani nemusel dovolit a při pokusu o spuštění nové instance stejného exe by jen aktivoval to předchozí (srov. též [Re: Colabo pro více projektů - multiple view](data/Re_Colabo_pro_více_projektů-multiple_view_1.txt?server=https%3A%2F%2Fdev.bistudio.com%2FColabo%2Fresources%2F&database=&id=556)).

Oproti současnému stavu by ukončené session bylo třeba nějak uklízet, jinak by tam zůstávaly viset pořád, protože stejné číslo session už nikdo nikdy nedostane. Asi by stačilo při přihlášení nové session smazat všechny ostatní session stejného uživatele, které jsou Offline nebo víc než několik hodin staré.