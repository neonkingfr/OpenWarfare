﻿Zkusil jsem změnu fontu na bezpatkový a ještě si hrál se stylem.

# Nadpis 1

## Nadpis 2

### Nadpis 3

Normálně<sup style="position:absolute">horní index</sup><sub>dolní index</sub> **Tučně** *Kurzívou* <span style="font-variant:small-caps">Kapitálkami</span> `Neproporcionálně`

-----
***maruk** (4.2.2010 14:40:11)*: Mně se to líbí. Ještě prosím změň font v okně tagů vlevo, tam stále straší Times.


-----
***rola** (4.2.2010 16:44:26)*: Hotovo v revizi 64142.
Velikost písma podle počtu článků se teď mění plynuleji: [*minimum 9px … medián 12px … maximum 28px*]. Taky jsem změnil písmo v seznamu článků.


-----
***rola** (4.2.2010 20:00:11)*: Velikost písma opravena, okamžitý hint v tag cloudu (revize 64169)