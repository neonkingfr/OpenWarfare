﻿> Ještě bych rád překontroloval, zda se při zavírání oken (nebo i celého programu) běžící tasky regulerně dokončí.

Nedokončily, ale násilně se přerušily. Dodělal jsem mechanismus, aby se právě běžící task okna vždy dokončil (okno se schová a převede do zvláštního seznamu, zruší se až když task oznámí své ukončení).