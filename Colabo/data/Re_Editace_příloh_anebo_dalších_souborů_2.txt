﻿> * drag and drop file to given place in text edit box
> * link se přidá, v případě obrázku jako obrázek, nejprve s destination ukazujícím na ten soubor, takže je to hned vidět
> * založí se task, který hodí soubor do SVN na místo, kde má být, nebo už je, ten článek
> * jakmile task skončí, původní link se přepíše tak, aby ukazoval do toho SVN
> * v případě, že soubor daného jména již existuje, nahradí se standardní cestou (používám již hotový *{DEFAULT FILENAME}*

Dohodli jsme se, že přidávání obrázků uděláme jinak:

* na místo kam byla příloha dropnutá se dá *ref*
* dialogNewArticle si zapamatuje, že mu byla dána příloha a příslušnou definici *refu* dodá (sharp)markdown parseru sám
    * prozatím tam bude dávat target na file:///hubbabubba.jpg
* časem bude možno v **UI** vidět seznam příloh, mazat je, ...
* až při commitu článku se nejprve commitnou přílohy a pak se na konec článku přidají definice pro ty *refy*, už ukazující do SVN

_____
**bebul** (17.3.2010 16:08:10)

Nový postup implementován, COMMIT: 67307
Ukázka výsledku např. [Attachments hokus pokus I](https://dev.bistudio.com/svn/sandbox2/data/Attachments_hokus_pokus_I_1.txt?server=dev.bistudio.com&database=sandbox2&id=14147)
