﻿Když je zobrazen kalendář a hodně se zmenší šířka aplikace, kalendář vyhodí exception a pak se místo něj zobrazuje už jen červeně přeškrtnutý obdélník.

************** Exception Text **************
System.ArgumentException: Rectangle '{X=108,Y=66,Width=0,Height=36}' cannot have a width or height equal to 0.
   at System.Drawing.Drawing2D.LinearGradientBrush..ctor(Rectangle rect, Color color1, Color color2, LinearGradientMode linearGradientMode)
   at Calendar.Office12Renderer.DrawAppointment(Graphics g, Rectangle rect, Appointment appointment, Boolean isSelected, Int32 gripWidth) in D:\WorkDirArma\Colabo\DayView\Office12Renderer.cs:line 208
   at Calendar.DayView.DrawAppointments(PaintEventArgs e, Rectangle rect, DateTime time) in D:\WorkDirArma\Colabo\DayView\DayView.cs:line 945
   at Calendar.DayView.DrawDay(PaintEventArgs e, Rectangle rect, DateTime time) in D:\WorkDirArma\Colabo\DayView\DayView.cs:line 878
   at Calendar.DayView.DrawDays(PaintEventArgs e, Rectangle rect) in D:\WorkDirArma\Colabo\DayView\DayView.cs:line 1040
   at Calendar.DayView.OnPaint(PaintEventArgs e) in D:\WorkDirArma\Colabo\DayView\DayView.cs:line 772
   at System.Windows.Forms.Control.PaintWithErrorHandling(PaintEventArgs e, Int16 layer, Boolean disposeEventArgs)
   at System.Windows.Forms.Control.WmPaint(Message& m)
   at System.Windows.Forms.Control.WndProc(Message& m)
   at System.Windows.Forms.Control.ControlNativeWindow.OnMessage(Message& m)
   at System.Windows.Forms.Control.ControlNativeWindow.WndProc(Message& m)
   at System.Windows.Forms.NativeWindow.Callback(IntPtr hWnd, Int32 msg, IntPtr wparam, IntPtr lparam)

