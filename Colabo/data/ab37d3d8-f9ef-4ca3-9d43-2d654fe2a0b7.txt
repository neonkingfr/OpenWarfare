﻿> Chtěl bych uzavřít chybu, ale zatím nevím jak. Potřebujeme trochu domyslet, jak zacházet s tagy a jak příspěvky filtrovat.

Protože implementace filtrování pomocí rule setů si vyžádá nějakou chvíli, udělal jsem rychlou možnost filtrování přes negaci tagu (revize 60779). Je možné jí zadat v Tag cloudu přes pravé tlačítko. 
Nějaké rozšíření tohoto postupu by se snad mohlo dát využít i pro rychlou tvorbu pravidel (uložit aktuální filtr jako podmínku pravidla). 
