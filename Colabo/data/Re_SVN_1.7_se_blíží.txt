﻿> * 1.7

Byla vydána [beta 2 od verze 1.7](http://mail-archives.apache.org/mod_mbox/subversion-dev/201107.mbox/%3CCAFwo6TZs9w-uRryRTynd9yvgXj4-pHOK2Ya7h=wY=v_HT2cqOA@mail.gmail.com%3E) - ovšem s tím, že beta1 nikdy vydána nebyla.

Dá se tedy čekat, že finální verze by někdy během podzimu měla být (oproti [původním očekáváním](data/Re_Další_optimalizace.txt?server=https%3A%2F%2Fdev.bistudio.com%2FColabo%2Fresources%2F&database=&id=1496) skluz cca 1 rok).
