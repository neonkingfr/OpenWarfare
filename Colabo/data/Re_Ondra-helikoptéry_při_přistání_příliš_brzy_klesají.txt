﻿> Soupis různých věcí, které mi připadají výhledově hodny pozornosti, ale zatím jsem pro ně nevytvořil vlastní vlákna:

http://forums.bistudio.com/showpost.php?p=1830014&postcount=12

Repro přiloženo.

Výsledky:

73346 - Fail / Fail
73340 - Fail / a teď OK? Jsem zmaten :( [test.utes.zip][Attachment1] 
73339 - Fail
73333 - OK
73266 - OK
73098 - OK

Ačkoliv je to tedy jistě chyba nepříjemná, oproti hlášení na fóru se nezdá, že by byla nová.

Příčina je v revizi 73339, konkrétně v:


`          float speedSize=fabs(ModelSpeed().Z());  
          avoidGround = floatMax(5.0f,speedSize * 0.35f);`


Místo dosavadního 

`       avoidGround=floatMax(30,targetAbove);`

Smyslem té opravy bylo dovolit vrtulníkům, které mají nastavené nižší flyInHeight, přiblížit se víc k zemi během, letu ve formaci. Omezil jsem opatření opravdu jen na ty, které mají nižší flyInHeight.

Porovnáním mrofilu klesání jsem našel ještě další probllém:

73098:
`  40 / 21
  26 / 16
  20 / 13
  15 / 9
  10 / 6`

77154:
`  40 / 18 
  26 / 9
  20 / 6
  15 / 4
  10 / 2`

Opravil jsem v 77157 (stejný výpočet v AutopilotAlign jako předtím v AutopilotNear):
`  40 / 20
  26 / 16
  20 / 13
  15 / 10
  10 / 6`

  


[Attachment1]: data/test.utes_3.zip