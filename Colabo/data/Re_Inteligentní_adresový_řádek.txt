﻿Comitnuta první verze adresního řádku (SVN 77478).
* Zobrazuje se adresa vybraného příspěvku.
* Lze napsat nebo vložit adresu příspěvku a po stisku enter se na něj přejde. Pokud neexistuje, adresní řádek zčervená.
* V adresním řádku lze editovat (díky odchytu klávesových zkratek Colabem to nebyla samozřejmost). Fungují i klávesové zkratky pro kopírování *Ctrl+C/V/X* a *Ctrl+Ins/Del* a *Shift+Ins*
* Když adresní řádek dostane focus a nebo se do něj klikne, celý text se označí.
* *AddressBar* je upravená komponenta *ToolStripTextBox*, která se umí roztáhnout do celé zbývající oblasti toolbaru.

Přijímám návrhy na další rozšiřování.

[ondra 20.1.2011 17:11:36]:
 Ještě nefunguje klávesa M (asi je odchycená nějak jinak - používáme ji jako klávesovou zkratku s proměnnou funkcí)

[ondra 20.1.2011 17:12:38]:
 Ať píšu, jak píšu (zkouším psát nesmysly), žádné červenání nevidím.

[jirka 21.1.2011 7:43:41]:
 > Ať píšu, jak píšu (zkouším psát nesmysly), žádné červenání nevidím.
 
 Až po stisku Enter. Kontrolovat adresu průběžně by asi stálo moc výkonu.

[maruk 21.1.2011 8:15:35]:
 Také N teď dělá problém.

[zdenek 21.1.2011 10:12:16]:
 SVN 77513. Opraveny problémy psaní znaků. Testuji všechna čísla a písmena bez modifikátorů i se shiftem. Přidáno okamžité testování existence url a zčervenání adresního řádku. Zatím to vypadá, že na výkon to nemá vliv.
