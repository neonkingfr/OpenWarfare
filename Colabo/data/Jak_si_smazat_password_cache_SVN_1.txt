﻿Hodilo se mi to pro ladění chování SVN pod jinými uživateli, poznamenávám kdyby tato informace ještě byla potřeba.

V adresáři *Application Data\Subversion\auth\svn.simple* jsou soubory, ve kterých je uložena SVN autentifikace - jeden soubor pro každé SVN repository (jméno uživatele a adresa repository je jako plain text). Stačí smazat a při příštím přístupu se SVN ptá na jméno a heslo.