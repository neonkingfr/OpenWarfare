﻿Správa uživatelů
----------------

V současnosti se k databázi každý přihlašuje svým účtem, přitom v průběhu vlastní práce s Colabem už se identita přihlášeného uživatele nepoužívá.

Do budoucna by asi měl systém fungovat jinak. Vlastní přístup k databázi by byl pod jedním standardním účtem (jak je to prý při práci s SQL databázemi obvyklé), nově by vznikla tabulka uživatelů (případně několik dalších pomocných tabulek), která by obsahovala:

- jméno
- heslo
- jméno pro zobrazení v UI
- kontaktní údaje (e-mail)
- další pomocné údaje (obrázek)
- aliasy (např. uživatelská jména v SVN)
- profily uživatelů

Profily uživatelů
-----------------

Musíme rozhodnout, jaké části profilu budou uloženy na klientu a jaké na serveru (v databázi). U něčeho je to jasné, někde musíme rozhodnout co bude pohodlnější. U údajů na serveru může existovat nějaký default, který se použije pro nové uživatele.

- údaje pro přihlášení k databázi (musí být na klientu)
- údaje pro přihlášení k SVN serverům (spíše na klientu)
- defaultní umístění nových příspěvků (může být na serveru)
- seznam přečtených příspěvků (může být na serveru) viz [Označování přečteného](features/oznacovaniPrecteneho.txt)
- seznam colapsovaných příspěvků (může být na serveru)
- rule sety pro filtrování příspěvků (může být na serveru)
- různá nastavení UI - šířky sloupců apod. (může být na serveru)
