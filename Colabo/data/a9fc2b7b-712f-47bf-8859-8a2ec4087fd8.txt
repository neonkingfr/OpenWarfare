﻿> Rád bych, kdyby i hodnoty zděděných tagů byly vidět u příspěvku, když ho prohlížím (a možná i edituji?)

Při prohlížení mi to připadá rozumné. Při editaci tak jak je teď (textově) by to ale myslím působilo zmatečně (dovedu si to ale představit u nějaké více vizuální editace tagů).

Bude to chtít trochu přepracovat práci s tagy - dosud se zděděné tagy vyhodnocovaly jen během filtrování, teď bych dědičnost spíše vyhodnotil už při načítání z databáze a uložil výsledky u příspěvku. Povede to i k přesnější statistice tagů v cloudu a bude se s tím lépe pracovat při filtrování pomocí výrazů.