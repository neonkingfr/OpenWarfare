﻿> > b) Nechat *GoTo Article* jak je, ale v *Navigate Back/Forward* by se rovněž ukládala celá *tag path*, která byla v okamžiku zobrazení článku zvolena.
> > 
> > * tohle by mi přišlo hodně sexy
> 
> Tohle bych si představoval 

Jakékoli změny v *"selected tags"* se nyní promítají do historie pro účely *back / forward* navigace.
Rev.65088


-----
***bebul** (17.2.2010 14:45:09)*: Nefungovalo to jak mělo. Hodně jsem si naletěl v ukládání seznamu aktuálních tagů pro tu kterou položku v historii (shallow copy a Equals).
Hopefully fixed in Rev.65118