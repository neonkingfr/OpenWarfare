﻿> 2. pokud práva přidáme, je situace ještě více matoucí - ve výsledném stavu je jedna working copy v ( uuid ) a další v ( uuid/dir ); když odesíláme článek z ( uuid/dir ), provede se commit na ( uuid ), ale vlastní článek se tak necommitne, ten je v jiné working copy

Tento problém je zvlášť protivný tím, že Colabo si myslí, že commit udělal, co měl (nevrátil chybu), ale článek ve skutečnosti odeslán nebyl.

[maruk 16.3.2011 9:21:54]:
 Tak jsem s tím také zápolil docela dlouho. 
