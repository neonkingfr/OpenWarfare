﻿1. JJ anebo Honza z Idey mi pošlou požadavek na CD klíče
2. Já zadám požadavek Jirkovi.
3. Jirka vygeneruje cd klíče
4. Jirka uloží cd klíče na místní síťový disk
5. Vezmu soubor s CD klíči, pošlu ho emailem do Gamespy k registraci
6. Soubor s cd klíči pošlu emailem do Idea Games

Rád bych celý postup zjednodušil do stavu, kdy JJ anebo Honza příp. jiná oprávněná osoba může vkládat požadavky přímo do Colaba a odtam také snadné klíče po vyřízení požadavku stáhne.

Souvísí s uživatelskými právy (v Colabu i SVN), formuláři atd. V ideálním případě bych z tohoto procesu chtěl vypadnout úplně, jsem v něm vlastně úplně neproduktivní. Kroky 2, 4, 5 a 6 bych ideálně vyřadil případně automatizoval (u 5. by chtělo domyslet verifikaci, zda u Gamespy registrace proběhla úspěšně).
[maruk 20.10.2010 12:28:24]:
 Prozatím jsem postup ustálil takto:
 
 1. požadavek posílám mailem pověřené osobě (A1, A2 Jirka, OA Hlaďas)
 2. mailem dostanu dávku klíče
 3. přeposílám do gamespy a dávám rovnou cc tomu, pro koho je cd klíč určen
