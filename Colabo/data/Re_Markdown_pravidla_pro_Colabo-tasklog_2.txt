﻿[27.5.] [19.5.] [13.5.] [12.5.] [11.5.] [10.5.] [7.5.] [6.5.]

[27.5.]: ~~ 27.5.2010 - analýza zápisu kódu
---
->[][index(obj)] Chceme napsat kód jako v pravém rámečku. Teď to je takhle:
n`
 ==== ``index(obj)`` --> ``int`` or ``nil``
 Returns the index of the first object in ``self`` which is ``==`` to ``obj``.
 This is an alias of ``find_index``.`

[index(obj)]:
 ==== `index(obj)` --> `int` or `nil`
 Returns the index of the first object in `self` which is `==` to `obj`.
 This is an alias of `find_index`.

Dobře vypadají `{svorky}` (s tím, že by se uvnitř počítaly otevření `{` a zavření `}`). Ale nejdou napsat na české klávesnici (maximálně jako |AltGr+B| a |AltGr+N|).
n`
==== {index(obj)} --> {int} or {nil}
Returns the index of the first object in {self} such that is {==} to {obj}.
This is an alias of {find_index}.`

Dobře vypadají i dvě mezery (mohly by se slučovat se začátky a místo druhé závěrečné mezery by mohlo být `.,:`). Trochu problém, že to není moc vidět.
n`
====  index(obj)  -->  int  or  nil
Returns the index of the first object in  self  such that is  ==  to  obj .
This is an alias of  find_index .`

[19.5.]: ~~ 19.5. - 21.5.2010
---
Spousta změn a přepnutí do %Comatu% (Markdown už je vypnutý, cha chá).
Správné uvozovky a apostrofy. Fungují jako zvýrazňovače, takže `""` se na konci řádku autozavře. //He joked "I told him 'That's what **she** said!'"
Dám test na podtržené nadpisy před ParserState.NORMAL, aby mi nekolidovalo s ligaturami `--` a `---`.

[13.5.]: ~~ 13.5.2010
---
Reference teď umí tooltip (stačí ho napsat za URI).
Chci-li těsně za návěští napsat URI, použiju místo prvního slova `~~`.

[12.5.]: ~~ 12.5.2010
---
Používám `->[]` a `<-[]` pro plovoucí objekty, zatím bez kolize.
Odkazy/vkládání objektů na začátku řádky už funguje (//special-case//).
Lokální odkazy jsou zelené.

*Reference* je na jednom řádku. Odkaz je jedno slovo – nesmí obsahovat mezery a musí mít uvnitř sebe tečku. Taky to celé musí být odstavec (nesmí to být třeba header).
N`
[Ref]: google.com` Pokud to reference nesplňuje, je to *návěští*.

*Poznámka* je sama na řádku a začíná blok:
N`
[Poznámka]:
 Já jsem poznámka.
 Pokračuju ještě jeden řádek.
Tohle už není poznámka.`

[11.5.]: ~~ 11.5.2010
---
Styly pro poznámky a spousta oprav.
Zápis `<[]` a `>[]` koliduje na začátku řádky, což je (najednou) jasné – `>` je citace. Budu muset použít něco jiného.
Zápis `[]:` dělá trochu problémy (tím, že funguje jen na začátku řádky).
Odkazy `[]` by neměly začínat uprostřed slova.
*Migrace:* bude třeba jenom převést `_____ notes` na `[]: notes`, zvýraznění (`**` na `*`, `*` na `/`). Zkusím ještě vymyslet kód předsazený mezerami, ale bude dělat kolize.

[10.5.]: ~~ 10.5.2010
---
~~<-[Poznámka vlevo][note] ->[Ta samá poznámka vpravo][note]
Odkazy, obrázky, reference a poznámky už fungují, ale někde můžou ještě pokulhávat. Zkusím je pár dní rozbíjet. Taky ještě nemají hotové styly.

Fungují i styly `<-[vlevo]` a `->[vpravo]`. Horizontální čára by měla umět zalomit, všechny dosavadní poznámky by se měly umět zobrazit před ní. Moc práce…

[note]:
 Tohle je poznámka na okraj.
 Hurá!
[7.5.]: ~~ 7.5.2010
---
Pracuji na odkazech a referencích, vezmu si je domů přes víkend.

[6.5.]: ~~ 6.5.2010
---
##### Návrhy
* *%Coma% na české klávesnici* (diskuze s Markem po Skypu). Použitelné symboly jsou `.,:;?!+-*/%=()"§_`:
  - Alternativní zápis `""kódu""` (N>1 uvozovek, dovnitř se jich smí napsat N-1. Výhoda je, že se uvnitř nemusí nic zdvojovat). Mezery na kraji kódu se ignorují, takže jde napsat `"" " ""`.
  - Alternativní odkazy a reference: `(odkaz)(ref)` a `(ref):`. Krátký zápis `(ref)` je zkratka pro `(ref)(ref)`, funguje jen pro reference.
* *Návrh tabulek v3*: CSV by mělo jít vložit přímo, takže v něm nesmí být markup :-/. Formátování bude v názvu reference, pár rozšíření uvnitř samotných CSV (nekolidujících se standartem).
##### Kód
* *Odkazy a reference.*
  Kvůli blokové struktuře se musí reference rozpoznávat v parseru a ukládat do vlastní struktury `Note` (= `List<ArticlePart>` + další data).
  Linky se v textu změní na `char[2] {LINK_CHAR, pořadí}` a taky půjdou do vlastní struktury. Nahradí se během postprocessu.
