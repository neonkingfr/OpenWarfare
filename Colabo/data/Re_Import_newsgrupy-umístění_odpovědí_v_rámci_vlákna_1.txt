﻿> import už *poměrně* funguje.

Odpovědi se konvertují špatně. Mnoho jich padá do rootu vlákna. 

Příklad: news:8v0bgo$1m0$1@localhost.localdomain

Srov. [Pamet v roce 2010](https://dev.bistudio.com/svn/sandbox/data/8v0bgo$1m0$1@localhost.localdomain.txt?server=dev.bistudio.com&database=sandbox&id=10170)

Je pravda, že to nevypadá úplně špatně, ale radši bych strukturu zachoval.


-----
***bebul** (29.1.2010 9:29:48)*: Myslím, že tahle chyba je trochu důvod pro implementaci další featury, a totiž REimportu již importované grupy. To zatím neumím, ale měl bych se naučit detekovat, že daný článek už byl importovaný. Dále bych chtěl nově vytvářet pro newsgrupy samostatné podadresáře, protože teď už je ten adresář data moc tučnej.

-----
***ondra** (29.1.2010 11:08:02)*: > a totiž REimportu již importované grupy.

To se mi moc nezdá. Proto máš sandbox, abys to tam všechno odladil. Až to bude dokonalé, tak provedeme import do ostré databáze.

-----
***bebul** (29.1.2010 11:15:30)*: No, ono se to totiž hodně strašně moc blbě ladí, když nemůžu dát opakovaně import bistudio.test. Obcházel jsem to tak, že jsem všechny články v bistudio.test opakovaně mazal, dokonce i pokusy, co si tam dělaj VBS.
Až to opravím, rád bych to otestoval zrovna na *bistudio.feature*, protože tam vím o *multiple parents in references*, ale nemám, jak jí znovu naimportovat.

-----
***ondra** (29.1.2010 11:18:59)*: Mysleli jsme to původně tak, že bys měl mít v ruce nějaký nástroj, kterým si kdykoliv snadno a rychle celý "sandbox" (SVN i SQL) zcela vyčistíš. Máš, nebo nemáš?

-----
***bebul** (29.1.2010 11:19:18)*: Na druhou stranu, pomocí telnetu dokážu nejspíš napsat článek do NG, který bude mít více references. Hned se o to pokusím.

-----
***bebul** (29.1.2010 11:22:48)*: 
> Mysleli jsme to původně tak, že bys měl mít v ruce nějaký nástroj, kterým si kdykoliv snadno a rychle celý "sandbox" (SVN i SQL) zcela vyčistíš. Máš, nebo nemáš?*

Nemám, ale trochu bych se toho bál. Na druhou stranu, úplně znovu založit svn repository a databázi by věc řešilo.
Jednak import trvá strašně dlouho, těch 8300 článků z engine.task-log už trval snad hodinu a commit už vůbec nemůžu dělat z Colabo. Dělám ho v tortoise a i tam je to záležitost třeba na půl hodiny.
Druhá věc je, že SVN repository by se muselo úplně zničit a znovu založit. Články v historii vadí, neb nelze stejný file přidat, protože si to stěžuje, že už tam je...


-----
***ondra** (29.1.2010 11:24:47)*:

> že SVN repository by se muselo úplně zničit a znovu založit

Ano, tak to bylo myšlené.

> 8300 článků z engine.task-log už trval snad hodinu

Pro běžné testování funčnosti by ti snad stačilo i něco menšího, ne?


-----
***bebul** (29.1.2010 11:26:22)*: 
> stačilo něco menšího, ne?

Ano a ne. Jedna věc je naučit se dělat import z NG. Druhá věc pak testovat, jak se Colabo chová při větší zátěži. To jsem myslel, že je jeden z hlavních cílů importu.
