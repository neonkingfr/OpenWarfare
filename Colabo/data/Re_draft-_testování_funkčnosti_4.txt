﻿> To mi připadá podivně a matoucí. Např. v thunderbirdu když edituji draft, tak ho tam dvakrát nevidím. Nedává to smysl, aby tam byl dvakrát.

Tohle řešení umožňuje, aby ses mohl při editaci draftu vrátit ke stavu před editací.

-----
***maruk** (11.2.2010 12:29:51)*: Chápu, možná to tedy nějak indikovat, ať to nemá dvakrát stejný název? Každopádně to není nic zásadního....
