﻿# Návrh kompletních pravidel pro tagy

Tagy mohou být **jednoduché**, **složité**, **privátní** nebo **virtuální**.

## Jednoduché tagy

Jednoduchý tag se vytvoří tak, že se napíše k nějakému článku. Má pouze jedno univerzální jméno (case-insensitive) a kopíruje se do seznamu tagů v odpovědi. Pokud už není nikde použit, přestane existovat.


## Složité tagy

Složité tagy jsou uložené v nějaké databázi. Adminové můžou upravovat jejich vlastnosti

* **kopírovaný tag** se píše do seznamu tagů odpovědi (stejně jako jednoduché tagy)
* **děděný tag** se chová, jako by měl `+` – platí pro celý podstom příspěvku, ve kterém je uveden.

a jména:

* jedno povinné **univerzální** jméno (`Thank you`)
* **lokalizovaná** jména, max. jedno pro každý jazyk (`en:Thank you, cs:Děkuji, cs-x-ascii:Dekuji, ru:Спасибо`)
* další možnosti zápisu (`Díky, Ď, Thanks`)

Uživatelům se vždy zobrazuje lokalizované jméno tagu (nezáleží na tom, jaký alias kdo napsal). Pokud pro daný jazyk chybí, použije se univerzální jméno.

Jazykové kódy u lokalizovaných jmen jsou podle [RFC 5646](http://tools.ietf.org/html/rfc5646). Pro češtinu bez diakritiky navrhuji `cs-x-ascii`.

*Srov. [Re: Multilingualita - zadání jazyka tagů a dokumentů](data/14f8de02-99ca-4b5b-89bd-def770885a95.txt)*

Jménům se pořádně věnuji níže.


## Privátní tagy

Začínají tečkou (`.abc`). Platí pro ně stejná pravidla jako pro jednoduché/složité tagy, ale každý si je spravuje sám (a nemusí na to být admin).


## Virtuální tagy, filtrování a vyhledávání

Virtuální tagy (psané `:abc`) jsou výrazy, které vytvářejí seznam id-ček článků. Nejde je přiřazovat ani definovat, ale některé se mohou zobrazovat mezi ostatními tagy (dobré by byly třeba `:to-read` a `:by-me`). Jména obsahují pouze malá písmena a pomlčky. Příklady:

* `:to-read` (nepřečtené články)
* `:by-me` (články napsané mnou)
* <code>:by-<i>username</i></code> (články napsané konkrétním uživatelem)
* `:has-attachment` (články s přílohou)
* `:chat` (články, co jsou historie chatu)
* `:active` (zrovna označený článek)
* <code>:<i>N</i></code> (článek, jehož id je N)
* <code>:<i>M-N</i></code> (články s id mezi M a N)
* <code>:"<i>text</i>"</code> (článek, jehož název je "text", případně s wildcardy ?*)
* `:child-of-Č` (články přímo odpovídající na článek Č (obecně: na některý ze seznamu článků Č))
* `:parent-of-Č` (článek, na který přímo odpovídá Č)
* `:root-of-Č` (kořen, pod kterým je Č)
* `:descendant-of-Č`
* `:ancestor-of-Č`
* `:before-Č` a `:after-Č` (články starší/novější než Č = s id nižším/vyšším než Č)
* <code>:older-than-<i>datum/čas</i></code> (články starší než konkrétní datum, nutné vymyslet formát)
* `tree-containing-Č` (zkratka pro `descendant-of-root-of-Č`)

Nynější barvicí pravidla by pak šly zapsat jako
    
    :active => COLOR:#ffffff BACKGROUND:#3399ff          
    :to-read => BOLD
    :tree-containing-active => BACKGROUND:#ffffd0

Operátory `&`, `|` a `!` budou pracovat na seznamech id-ček (průnik, sjednocení, doplněk).
Takže půjdou napsat věci jako `:tree-containing-("@rola" & newer-than-20090101 & !"done")` – diskuze obsahující neuzavřený článek pro uživatele `rola` novější než 2009.


## Jména tagů

Povolená jména tagů nesmí začínat na `+`, `-` nebo `#`, jinak mohou obsahovat libovolné Unicode znaky kromě řídících (`U+0000..U+001F` a `U+007F..U+009F`). Pro účely porovnávání jsou case-insensitive, ale ukládají a zobrazují se case-sensitive. Mezery na začátku a na konci se ignorují (jiné bílé znaky nepřipadají v úvahu). Kromě toho platí, že tagy začínající na

* `.` jsou privátní
* `:` jsou virtuální
* `@` jsou určené pro konkrétního uživatele, třeba `@rola` (konvence)

Znaky, které mají v daném kontextu specifický význam (čárky v seznamu tagů, `?*"` ve filtrování apod.) se musí **escapovat**. K tomu slouží zpětné lomítko `\`:

* <code>\x<i>NN</i></code> – Unicode znak U+00NN
* <code>\u<i>NNNN</i></code> – Unicode znak U+NNNN
* jinak se prostě použije znak za lomítkem, ale bez speciálního významu.

Dosavadní uvozovky se ruší – měly moc speciálních případů.


## Přiřazení tagů k článku

Před jméno tagu lze napsat

* `+` – tag se bude chovat jako děděný (platí pro celý podstrom)
* `-` – pro sebe a svůj podstrom vymaže děděný tag (platí `+` nebo `-` u nejbližšího předka)
* `.` – tag se bude chovat jako privátní (uvidí ho jen autor)
* `#` – tag se nebude kopírovat do odpovědí

Tagy se oddělují čárkami nebo odřádkováním. Escapovat se musí znaky `,` a `\`. Na druhou stranu, `+-.#` se escapovat nemusí, protože na začátku jména být nesmí (a tečka se u privátních tagů píše vždycky). Zhůvěřilosti (jako tabulátory) se vyhodnotí jako součást jména tagu, kde jsou zakázané (U+0009), takže vypíšou chybu. Mezery jsou běžné znaky, které se potichu ořežou.

Takže tagy `Run, Lola, run!`, `Escapování pomocí lomítka \`, `@rola` a `dlouhý tag s mezerami-a-spojovníky a dalšími?*podivnými$%^znaky少し日本語のテキスト` se můžou přiřadit k novému článku jako
    
    run\, LOLa\, run!, Escapování pomocí lomítka \\, @Rola, dlouhý tag s mezerami-a-spojovníky a dalšími?*podivnými$%^znaky少し日本語のテキスト

I když ten poslední tag by asi chtěl alias. ;-)


## Jména tagů ve virtuálních tazích (ve výrazech)

Jsou vždy uvnitř uvozovek a `?*` mají speciální význam, takže escapovat se musí `?`, `*`, `"` a `\`.
