﻿>  Osobně teď last-month používám jako default filtr.

Docela se mi to líbí, ale ještě bych to vylepšil. Často mi chybí kontext - vidím odpověd, ale nevidím, na co je. Většinou bych chtěl spolu s příspěvkem vždy zobrazit jeho rodiče (případně několik generací).

Implementačně vidím dvě různé možnosti:

- tuhle funkčnost propašovat přímo to virtuálních tagů `:last-month` apod.
- udělat to takhle vždy, bez ohledu na tagy, v rámci filtrování tagů (případně může být tohle "chytré" filtrování vypínatelné)

Jelikož máme vždy seznam všech příspěvků v paměti, technicky by to mělo jít snadno.
