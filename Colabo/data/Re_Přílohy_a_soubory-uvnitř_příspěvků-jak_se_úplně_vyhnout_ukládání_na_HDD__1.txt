﻿Pokud by přílohy byly součástí článku (uuencodovaný nebo jinak encodovaný uvnitř) pak je třeba dořešit, jak je zpřístupnit uživateli.

- obrázek nebo text aby byl rovnou vidět a šlo ho uložit
- jiné octet-stream soubory aby bylo možno uložit

Jedno řešení by mohlo být použít temporary soubory, což se nám nelíbí. Jirka dumal, jestli se nějak nenapíchnout na nějaké eventy toho WebBrowseru a zachytit, kdy si o data říká a ty mu pak podstrčit z paměti (tj. nemít nic uložené na HDD).

Další, co mě napadlo, je udělat pro to vlastní internet protocol, který by pak Colabo používalo a přímo Colabo ty dotazy odpovídalo. Googlování na toto téma ukázalo, že to je asi způsob, který se používá http://stackoverflow.com/questions/290035/how-do-i-get-a-c-webbrowser-control-to-show-jpeg-files-raw
Takový internet protokol by pak mohli využívat i odkazy přímo z markdownu, jestli jsem vůbec pochopil ondrovu námitku v [Re: Přílohy a soubory - uvnitř příspěvků, nebo samostatné soubory?](data/Re_Přílohy_a_soubory-uvnitř_příspěvků,_nebo_samostatné_soubory__1.txt)

(Rola ještě ukazoval čáry, jak přímo do html vložit data, nicméně tam je omezení 32kB na jejich velikost). Ukázka zde: [Re: Bébulovo pískoviště](data/Re_Bébulovo_pískoviště_3.txt)


-----
***ondra** (21.1.2010 15:58:21)*: > Takový internet protokol by pak mohli využívat i odkazy přímo z markdownu

Tenhle "protokol" je věc specifická pro Internet Explorer. Takový dokument sice bude formálně Markdownový, ale fakticky si ho mimo Colabo nikdo nepřečte. Použít https odkazy (do SVN) se mi jeví zdaleka nejlepší. Čemu vadí, že SVN umí dělat revize? Když nebudeš attachmenty editovat (a jak bys to ostatně dělal?), žádné revize nevzniknou. Naopak, pokud by někdy nějaká možnost editace attachmentů přibyla, tak to, že k ní budou revize, bude jen výhodné.

> Bude příloha uložena stranou příspěvku jako samostatný soubor? Navíc v SVN s celou škálou revizí? Přijde mi, že se to pak může celé dost zkomplikovat.

Takže bych chtěl slyšet, v čem konkrétně se to může zkomplikovat. Zatím mi to naopak připadá jako čisté, jednoduché a standardní řešení.


-----
***bebul** (22.1.2010 12:05:06)*: Ukládání obsahu příloh zakódovaně přímo do příspěvků taky přináší komplikace. 
Pokud budou přílohy samostatné soubory, je třeba vyřešit:

- kdy ukládat přílohu do SVN (aby to nebylo příliš brzy - co když článek nakonec neodešlu)
- jak volit její jméno, aby bylo jednoznačné
- kam ukládat odkazy na přílohy

Pokud budou přílohy bokem, nebudou to vlastně tak úplně přílohy článku, bude to prostě samostatně žijící artikl, na který se bude moci odkazovat kdodoli. Editací článků se může stát a asi i bude stávat, že začnou vznikat soubory sirotci, na které se už nikdo nebude odkazovat.
Hlavní věc, kterou bych rád vyřešil je, jak bude článek vědět o svých přílohách? Vygeneruje se pro ně nějaký markdown, který si bude moci případně autor ohnout podle svého gusta, nebo na to bude nějaká speciální sekce, kterou dokážeme o článku říct, že má těchto pět příloh? Prostě, jak se bude attachment lišit od jiných odkazů?

Jedna možnost je, aby existovala databázová tabulka Attachments, s dvojicema vždy id článku a url přílohy. Jiná, že by uvnitř článku byl nějaký oddělující tag, &lt;attachments&gt;...&lt;/attachments&gt;
Tak by bylo vždycky možno ohnout zobrazení a správu příloh podle nových požadavků. (Protože bychom uměli odpovědět na zásadní otázku: "What are attachments?"

-----
***ondra** (22.1.2010 12:38:51)*: > Vygeneruje se pro ně nějaký markdown, který si bude moci případně autor ohnout podle svého gusta,

Připojil bych to textově, jako markdown, něco jako

    -----
    [Attachment 1](http:/xxxxx)
    ![Image Attachment 2](http:/xxxxx)

Za zvážení by také mohlo stát použít reference (ne inline) syntax odkazů. Viz [1]


Definice odkazu:

    [id]: http://example.com/  "Optional Title Here"

Použití odkazu:

    This is [an example][id] reference-style link.


> Editací článků se může stát a asi i bude stávat, že začnou vznikat soubory sirotci, na které se už nikdo nebude odkazovat.

Přikládání příloh bych považoval za součást editace. Už v okamžiku, kdy přílohu začnu připojovat v editoru, by se tedy vytvořila v SVN (a v editoru bych i viděl k ní příslušný markdown, který se na ní odkazuje).

> že začnou vznikat soubory sirotci, na které se už nikdo nebude odkazovat.

Ano, to je pravda. To mi nepřipadá nijak hrozné. Stejná věc se zcela běžně děje ve Wiki i v běžně používaných bug tracking systémech.

[1]: http://daringfireball.net/projects/markdown/syntax#link "Markdown Link"


-----
***maruk** (3.2.2010 16:31:14)*: Pár pokusů, na kterých jsou vidět různé problémy a omezení:

[Test dokumentu jiného typu - rtf](Sandbox/Experiments/test.txt?server=dev.bistudio.com&database=colabo&id=635)

