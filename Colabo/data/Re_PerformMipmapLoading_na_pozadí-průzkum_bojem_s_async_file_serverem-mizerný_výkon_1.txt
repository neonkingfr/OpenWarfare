﻿> news:01cacce6$Blat.v2.6.2$11189ffa$bdc287ee1b8@192.168.2.207
> 
> Test na AB2 bohužel skončil naprostým fiaskem. Některé testy se nevykonaly vůbec (-noCB přestalo fungovat), ty co se provedly, běží výrazně hůř.

-noCB jsem opravil snadno. Bez CB textury na pozadí zavádět nebudu. Výrazně zhoršený výkon bude horší, ale i do toho se pouštím.

_____
**ondra** (28.3.2010 22:41:46)

Chtěl jsem to zase v distribuci zakázat, ale omylem jsem zakázal jen zavádění textur na pozadí, async. file server jsem nechal povolený. Fps je dál mizerné, takže je vidět, že hlavní problém je právě ten async. file server.