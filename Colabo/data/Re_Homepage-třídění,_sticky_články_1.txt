﻿Každopádně stávající chování homepage se mi neosvědčilo. Jedna věc mi na tom totiž vyloženě vadí:

že na homepage po spuštění i odskrolováno v seznamu příspěvků, tj. většinou po puštění colaba skroluji dolů na nové příspěvky.

Pro mě by dávalo smysl tuto stránku po spuštění zobrazit, ale bez ohledu na to skrolovat na nejnovější (resp. konec seznamu). Při kliknutí na home button mi naopak nevadí, že se na něj i skroluje...



_____
**rola** (25.3.2010 16:17:18)

Mně se osvědčilo načtení posledního zobrazeného článku.

_____
**maruk** (25.3.2010 16:19:48)

Je to tak. Tj. jinak:

* prosím homepage zobrazit jen při prvním spuštění Colaba (příp. když není znám poslední zobrazený článek) anebo při kliknutí na button home a nikoli při každém spuštění Colaba

_____
**jirka** (29.3.2010 8:52:36)

Prosím uvědomte si, že poslední zobrazený článek není totéž co nejnovější článek. Kterou z těch možností preferujete?

Jak to má fungovat při změně tagů? Jaký článek zvolit, pokud současný už není (díky filtrování) zobrazen?

_____
**rola** (29.3.2010 10:45:42)

Já bych byl pro poslední zobrazený.

_____
**maruk** (29.3.2010 12:38:50)

Poslední zobrazený je fajn, ano.