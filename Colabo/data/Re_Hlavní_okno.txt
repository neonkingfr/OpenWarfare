﻿V revizi 78290 přidáno automatické nastavení focus panelu na mouseover. Nyní už není potřeba do okna klikat, aby šlo scrolovat.
[jirka 17.2.2011 7:39:12]:
 Jedna věc se mi tam nelíbí. Když si otevřu okno pro odeslání nové zprávy nebo poznámky a myší se odsunu mimo něj (nad hlavní okno aplikace), nejen že ztratí focus, ale přesune se do pozadí. Bude možné to opravit?

[zdenek 17.2.2011 11:05:21]:
 Opraveno v revizi 78314
