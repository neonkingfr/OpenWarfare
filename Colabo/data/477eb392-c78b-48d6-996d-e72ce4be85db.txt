﻿> > Možná je problém spíš opačný. Odpověď nyní funguje tak, že se vytvoří working copy originálního příspěvku a načítá se z disku. Přímý přístup přes https by možná byl rychlejší (můžeme vyzkoušet).
> 
> Se vzdáleným přístupem to trvá možná 30 sekund. To rozhodně není disk, to musejí být síťové (SVN) operace.
> 
> > Jasně, tady bych ale tak trochu očekával, že příspěvek na který dávám reply tu vidím, tj. moc nechápu proč se vlastně zavádí znova
> 
> Naprosto souhlasím.

Pokusil jsem se citování původního příspěvku v odpovědi optimalizovat:

- pamatuji si obsah posledního načítaného článku (spolu s jeho URL pro kontrolu)
- pokud odpovídám na takový článek (což by nyní mělo být vždy), použiji k tvorbě citace uložený obsah
- jako fallback načtu obsah článku přes http request

Vyzkoušejte prosím, jestli to pomohlo. U sebe stále nějakou prodlevu pozoruji, ale během trasování vidím že je to už výhradně ve spojitosti s vytvářením nového příspěvku (i tady by ale myslím nějaký prostor pro optimalizaci byl).