﻿V současné době se mě při mazání příspěvku ptá Colabo 2x, poprvé, jestli ho chci smazat z databáze, podruhé, jestli ho chci smazat i z SVN. Ten druhý dotaz je navíc nepříjemný tím, že se jednak odehraje až za chvílí, a navíc
 má nějaký divný focus (může být schovaný za hlavní oknem, asi se vytváří v nějakém worker threadu a nemá hlavní okno jako rodiče).

Navrhoval bych se na obě dvě věci ptát najednou:

Remove the article from both the dabatase and SVN repository?

Odpovědi by byly:
- Yes
- Only from database
- Cancel

[maruk 10.11.2010 20:38:01]:
 Přijde mi to rozumné.
