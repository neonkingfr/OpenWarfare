﻿To už mi připadá tak trochu jako nesmysl, na to přeci stačí (a je nejvhodnější) prostě dikuse v Colabu :)

Já si naopak lineární diskusi představuji jako něco, co jde dělat jak offline tak online přes IM a myslím, že hierarchická struktura by to jen komplikovala.

V UI by to mělo nějak být svázené s článkem, ke kterému se diskuse vztahuje. V svn pak asi prostě samostatný soubor (nevím zatím, jak čistě a elegentně vytvořit vazbu mezi článkem a diskusí, v praxi by to asi mělo být nějak univerzální a chytré, např. diskuse se může týkat více článků atd.).

 
> Pak ještě jeden nápad, už asi hodně ulítlý, berte jen jako nápad: pokud diskuse nebude součástí článku, ale jen jeho jakousi "přílohou", nabízí se možnost, aby i sama tato diskuse mohla být hierarchická. Nevidím to jako nijak potřebné, i u současného Append note se mi zdá, že k udržení kontextu stačí citovat, na co reaguji, ale možné by to nejspíš bylo a třeba někdo najde i příklad, kdy by to bylo opravdu užitečné.
