﻿> b) tagy *&lt;nowiki&gt;xxx&lt;/nowiki&gt;*

Používáme to ve wiki dost často. Jediná možnost, jak to implementovat, která mě napadá je, že si nejprve vyparsuju z mediawiki textu všechny úseky, kde je **nowiki** použito, namísto toho si tam vložím nějaké *&lt;markNoWiki id="42"/&gt;* a na závěr to do výsledku zase přidám.



_____
**bebul** (12.3.2010 17:16:41)

Hotovo. Zkoušel jsem to na příkladě, kde byl problém velmi viditelný a teď už působí korektně, k vidění zde [Přehled o postupu prací na technologických úkolech (Technology Tasks Survey)](https://dev.bistudio.com/svn/sandbox2/data/wiki/d/d4/Přehled_o_postupu_prací_na_technologických_úkolech_%28Technology_Tasks_Survey%29.txt?server=dev.bistudio.com&database=sandbox2&id=14137)
