﻿> **ondra** (24.3.2010 9:30:52)
> Ha. No neříkal jsem to, že to budou chtít? ;)
> 
> Potíž je v tom, že ony to po odeslání už vlastně "přílohy" ani nejsou. Už to jsou odkazy, jako každé jiné. Pokud to budeme opravdu chtít, budeme to muset udělat nějak zvlášť.
>
> Jiná možnost by mohla být v nějakém okně udělat seznam všech "ref-style odkazů". Ty nám normálně, pokud je někdo nepřidá ručně, vznikají snad pouze z příloh.

Vzhledem k tomu, že současný stav ani neumožňoval přidat novou přílohu ke článku, který již přílohy měl a přidání navíc článek rozbilo (viz [Více příloh a editace příspěvku s přílohama](data/Re_Prvni_postrehy_1.txt?server=dev.bistudio.com&database=colabo&id=1084)), udělal jsem pokusné řešení v tomto stylu:

Po načtení textu článku najdu všechny definice *reference style linků* takové, že jsou tvaru `[AttachmentN]: url` a nakládám s nimi během editace jako s běžnými přílohami, které tam může uživatel DragAndDropnout s tím rozdílem, že už je znovu nikam neuploaduju, neb už jsou tam, kde být mají.

Tj. článek po zparsování textu umí zjistit, zda má nebo nemá attachments. Pokud chceme např. vidět **sponku** pro články, které mají attachments, navrhuju tuto informaci u každého článku držet a aktualizovat jí s každým "Send Article". Ano, pokud si někdo dá tu práci a přidá nový Attachment ručně, nebude o tom Colabo vědět, dokud článek někdo znovu nezparsuje a neodešle.

_____
**bebul** (26.3.2010 15:30:23)

Přidávat Přílohy k článkům s Přílohama jde od rev.68197