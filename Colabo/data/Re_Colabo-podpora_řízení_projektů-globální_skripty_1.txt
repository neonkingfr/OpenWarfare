﻿> Ověřil jsem, že základní technologie pro globální skripty ([MSScript](http://weblogs.asp.net/rosherove/articles/dotnetscripting.aspx)) je funkční a připravil jednoduchý test (viz konec funkce Form1_Load).

S MSScript jsou nějaké problémy na 64-bitových Windows. U Ondry nejde aplikace zkompilovat (právě kvůli problémům s komponentou Interop.MSScriptControl).

Měli bychom vyzkoušet, zda přeložená aplikace tam bude fungovat (včetně globálních skriptů) a případně najít jiný mechanismus, jak globální skripty spouštět (zatím jsem moc úspěšný nebyl).
[jirka 5.10.2010 8:41:29]:
 Zdá se, že existují ještě dvě možnosti jak skripty spouštět:
 a) pomocí [Microsoft.Vsa](http://msdn.microsoft.com/en-us/library/ms974577) - tady je problémem, že většina použitých konstrukcí je v dokumentaci označena jako [obsolete][http://msdn.microsoft.com/en-us/library/microsoft.visualbasic.vsa.vsaengine(v=VS.80).aspx]
 b) pomocí reflection a [CodeCompiler](http://odetocode.com/code/80.aspx) - tuhle možnost nyní zkusím blíže prozkoumat

[jirka 5.10.2010 11:45:52]:
 > pomocí reflection a CodeCompiler – tuhle možnost nyní zkusím blíže prozkoumat
 
 Po delším hledání a zkoumání se zdá, že to bude schůdná cesta. Bude potřeba lehce změnit syntaxi existujících globálních skriptů, což není až takový problém vzhledem k tomu že jediný jsem zatím napsal sám.
 
 Místo:
 `<script content>`
 
 se bude psát:
 `class Script
 {
   function Main(colabo)
   {
     <script content>
   }
 }`
 
 Jako bonus pak bude možné psát skripty v dalších (pouze instalovaných) jazycích:
 - C#
 - J#
 - C++

[jirka 6.10.2010 8:01:45]:
 V revizi 73807 se už MSScriptControl nepoužívá, prosím Ondru (až bude v práci) o vyzkoušení překladu a funkčnosti.

[ondra 6.10.2010 10:22:46]:
 Překládá a funguje.
