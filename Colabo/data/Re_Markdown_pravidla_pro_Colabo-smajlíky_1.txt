﻿Smajlíky
%%%
Smajlíky mají spoustu alternativních zápisů. Kolem názvů se píšou `:dvojtečky:`.

`A` :alien: :angel: :angry: :annoyed: :ashamed: :awesome:                                                   `alien angel angry annoyed ashamed awesome`
`B` :beer: :blush: :blushhappy: :bored:                                                                     `beer blush blushhappy bored`
`C` :cake: :cat: :cheerful: :chuckle: :coffee: :confused: :cool: :cry:                                      `cake cat cheerful chuckle coffee confused cool cry`
`D` :dead: :devil:                                                                                          `dead devil`
`E` :eww:                                                                                                   `eww`
`F` :flower: :frown:                                                                                        `flower frown`
`G` :grin:                                                                                                  `grin`
`H` :handshake: :happy: :heart: :horny:                                                                     `handshake happy heart horny`
`I` :inlove:                                                                                                `inlove`
`K` :kill: :kiss:                                                                                           `kill kiss`
`L` :laugh: :listen: :lol:                                                                                  `laugh listen lol`
`M` :meh: :mute:                                                                                            `meh mute`
`N` :nod: :note:                                                                                            `nod note`
`O` :omg:                                                                                                   `omg`
`P` :pizza: :proud: :punch: :puppyeyes:                                                                     `pizza proud punch puppyeyes`
`R` :robot: :rock: :rofl:                                                                                   `robot rock rofl`
`S` :screaming: :serious: :sleep: :smile: :smug: :sorrow:                                                      `scream serious sleep smile smug sorrow`
    :star: :sun: :surprised: :sweat: :sweatdrop:                                                            `star sun surprised sweat sweatdrop`
`T` :thinking: :thumbsdown: :thumbsup: :tongueout:                                                          `thinking thumbsdown thumbsup tongueout`
`U` :unsure:                                                                                                `unsure`
`W` :wasntme: :whisper: :wink: :wtf:                                                                        `wasntme whisper wink wtf`
`Y` :yawn: :yum:                                                                                            `yawn yum`
