﻿>Myslím, že by stačilo zajistit, aby stránky označené jako "portál" byly při jakémkoliv třídění vždy vytřízené jako první. Když pak uživatel klikne pouze na příslušný tag, zobrazí se ta první stránka, tj. portál.

To ještě postrádám, ostatně působí beztak dost divně když kliknutí na homepage mě hodí někam doprostřed mnoha článků.

Podobné chování mi ale chybí i u článků s "tagem - flagem" (např. important) a nejspíš i jinde.

Odhaduji, že bychom toto tedy chtěli řešit nějak obecněji přes ruleset?

_____
**jirka** (22.3.2010 10:25:54)

Návrh byl zde: [Re: Filtrování příspěvků](data/98765604-254d-48df-ac88-898db1ccf0eb.txt?server=dev.bistudio.com&database=colabo&id=176)

V rule setu by byla nová pravidla: `condition => SORTORDER:level`, přesunutí homepage nahoru by se dalo dosáhnout přes virtuální tag *:homepage*.

Ještě je třeba domyslet, kdy se tato pravidla mají uplatňovat:
- vždy nebo jen při třídění podle sloupce ikon
- jaká budou dodatečná kritéria při třídění podle sloupce ikon
