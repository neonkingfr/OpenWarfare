﻿> Např. v jednom framu se scope lDGC1 se vyvolal 128x, v celkové délce 156 ms.

Přidal jsem si další scopy. Vidím, že většina tohoto času je ve funkci Landscape::CheckRoadway. Ta se při 122 voláních lDGC1 (164 ms) a volala 2462x (147 ms).

Dle logů to přitom vypadá, že se jednalo o čtverce trávy, které dosud nikdy v cachi ani nebyly.

[ondra 19.7.2010 9:37:53]:
 Jednu věc nicméně nemohu potvrdit, a to že by se jednalo o problém, který je OA specifický. Ve stejné misi vidím totéž i v aktuální A2.

[ondra 20.7.2010 8:53:27]:
 Ukazuje se, že velká část objektů testovaných na roadwaye jsou kameny a kamenné zídky.

 Statistiky z jednoho framu:
 
 - silnice: 0,888 ms (v průměru 0,002 na jeden)
 - guardhouse: 0,392 ms (v průměru 0,005 na jeden)
 - kameny: 148.78 ms (v průměru 0,035 na jeden)
 
 Take je vidět, že testy vůči nim jsou výrazně pomalejší. Je to proto, že jejich roadwaje mají vyrazně víc faců (r2_rockwall - 1140 faců, r2_rock1 - 998 faců, proti tomu mil_barracks.p3d - 10 faců, asf3_10 -  100.p3d - 16 faců)
 
 Řekl bych, že ty ani clutter potlačovat nemusí, ale otázka je, podle čeho je můžeme poznat. Jiná možnost by mohla být testy pro složitější objekty optimalizovat třeba tím, že si facy v roadwaji v rámci objektu uspořádám do OffTree.
 
 

[ondra 20.7.2010 14:07:38]:
 S pomocí OffTree jsem se dostal na průměr 0,006 na jeden. Tím jsem ze 150 ms na cca 30 ms. To už sice není tragické, ale pořád je to poměrně dost.
 
 Pěkné ovšem je, že tahle optimalizace se může projevit i při běžné simulaci, tam, kde vozidla nebo vojáci s takovýmito složitými roadwayemi kolidují.
 
 (72189)

[ondra 13.10.2010 16:07:18]:
 Narazil jsem na velmi podobnou funkci Landscape::UnderRoadSurface a taky jsem ji podobně vyřešil. Čas v jednom volání klesl pro kameny ze 110 ms na 3, tedy 30 lepší. I tak nedovedu vyloučit, že se najdou další místa, která budou podobně degradovat, proto raději i news:i94d9p$ouu$1@new-server.localdomain
 
 Dokonce bych řekl, že Landscape::UnderRoadSurface  a Landscape::CheckRoadway jsou si natolik podobné, že by možná šly i sloučit do jedné, ale nevidím důvod se do toho hrnout.
