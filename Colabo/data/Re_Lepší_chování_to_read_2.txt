﻿> Nemělo by se zvýrazňování :to read řídit také přes rule set a ne natvrdo v programu, jak je tomu doposud?

Máme v revizi 66789 (v distribuci):

- označování příspěvků k přečtení v programu zrušeno
- zavedeny nové akce rule setů BOLD, ITALIC, STRIKEOUT a UNDERLINE
- pravidlo ":to read" => BOLD přidáno do default rule setů v Colabo databázi (pokud bude třeba nakopírovat nové rule sety do jiných databází, ozvěte se)
