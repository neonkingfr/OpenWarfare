﻿> Momentalne chci odebrat tagy
> 
> ***task-log***

Task log bych raději přejmenoval, případně zrevidoval použití. Zdá se mi docela rozumné označovat "vlastní pracovní poznámky", tj. různé nápady a TODO listy, které se v průběhu práce vynoří a které nezajímají nikoho jiného, než mě. Až bude [Colabo jako náhrada mailu](data/Re_Colabo_jako_náhrada_mailu_1.txt?server=dev.bistudio.com&database=colabo&id=885), tak bych takové věci mohl také dělat tak, že jako "adresáta" označím pouze sebe (nebo nikoho, to je asi totéž), do té doby to ovšem může sloužit k filtrování.

Místo ***task-log*** by se mi jako přiléhavější jevilo třeba ***/notes***.

_____
**maruk** (9.3.2010 17:03:58)


>Místo task-log by se mi jako přiléhavější jevilo třeba /notes.

Tak jo, kouzlo... a už to je. 



