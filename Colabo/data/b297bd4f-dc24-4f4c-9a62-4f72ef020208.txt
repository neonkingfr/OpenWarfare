﻿> Bylo by dobré, kdyby Ctrl+Q fungovalo i když je zafocusovaný preview okno, tj. prohlížím dlouhý článek, scroluju v něm a po dočtení dám Ctrl+Q. Nic se však nestane.

Je to hotové (revize 62040).

Technologicky bylo potřeba zachytit událost stisku klávesy ještě než se dostane controlům, to se podařilo zavedením message filtru. Je možné, že nyní naopak některé shortcuty zachytíme moc brzo (třeba Ctrl + C při označeném textu v preview), bude potřeba v kontextovém menu nějak označit působnost shortcutů. (proto zatím nezavírám)

-----
***jirka** (22.12.2009 14:30:26)*: Vyřešeno (revize 62066).

Při reakci na shortcut budeme chování rozlišovat podle hodnoty políčka Tag ve vlastnostech prvku kontextového menu. Zatím testujeme jen hodnotu <code>list</code>, která říká že shortcut se testuje jen pokud je focus na seznamu příspěvků.
