﻿Colabo Alpha
--
Dalším milníkem bude alfaverze, tedy příprava Colaba pro běžné pracovní nasazení v rámci studií na již rozběhnuté projekty.

Potřebné úkoly jsou označeny tagem */[/milestone](https://none?tags=/milestone)*/

[ondra (5.2.2010 11:00:10)]:
 Přimlouval bych se raději za označení milestone:alpha. Takhle mi to evokuje spíš stage:alpha. Vůbec bych se používání podobných "strukturovaných" tagů nevyhýbal, naopak jejich použití nám dá do ruky lepší možnosti, až budeme umět tagy všelijak parsovat - pro [Re: Zobrazení @tagů v seznamu článků za názvem](data/745c1076-7583-4721-a13c-eed267195424.txt?server=dev.bistudio.com&database=colabo&id=339) apod.

[maruk (5.2.2010 11:07:56)]:
 V tomto dost váhám. Asi hlavně kvůli absenci systému pro jejich vkládání ve stylu newsreaderu, navíc pak budou dost dlouhé v cloudu.

 Je pravda, že /stage/ a /milestone/ jsou v našem pracovním workflow dost důležité.

 Možná bych ale měl pro tento případ spíše volit *milestone* než *alpha*, analogicky moji přestavě o *priority* tagu. To dodatečné upřesnění pak chápu jako dodatečné upřesnění anebo změnu default hodnoty (která ale často stačí).

 Jiný příklad je *done*, líbí se mi o dost víc než *close:done*, ale nevím jak tam volitelně doplnit upřesnění (duplicate, postpone atd.)
