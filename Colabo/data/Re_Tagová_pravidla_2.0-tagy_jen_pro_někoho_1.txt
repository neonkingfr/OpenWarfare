﻿> Privátní tagy viděné ostatními my též přijdou zbytečné. Pokud to někdo opravdu chce, může to klidně založit jako veřejný tag ve styly ondra.co-mě-zajímá, ale nedovedu si beztak představit, k čemu by to kdo chtěl.

Pro úplnost, i když nadále neznám konkrétní použití, jak bychom analogicky k privátním tagům dělali "veřejné tagy s omezenou platností"? Dejme tomu článek, který je důležitý pro Jirku, Rolu a všechny designéry?

Něco jako:

Jirka.important, Rola.important, Designer.important

Otázky k rozhodnutí:
- chceme vůbec něco takového?
- má být "adresát tagu" součástí textu tagu, nebo se bude zadávat nějak jinak?

Jistě se takové věci dají implementovat i přes běžné rulesety s wildcardy, ale může to někdy být trochu těžkopádné. Třeba important:

    important | ondra.important | programmer.important => ICON:XXX

V této souvislosti mi navíc připadá neštastné řešení přes virtuální tagy - možnost dotazovat se na jméno (a třeba skupinu) v rule setu přes nějakou proměnnou by dávalo v tomto případě lepší možnosti (určitě jsme to diskutovali, ale nenašel jsem to)

_____
**jirka** (11.3.2010 9:27:56)

Něco podobného jsme už diskutovali, co to pojmout jako možnost nastavit článku cizí privátní tag? Tj. pokud při editaci článku zadáš rola.important, znamená to, že Rola bude mít u článku .important (a nemusíme zavádět nová pravidla).
Hodila by se pak možnost zkontrolovat u článku privátní tagy ostatních uživatelů (defaultně bych je neukazoval). Znamenalo by to také nutnost úpravy pravidel pro názvy tagů ('.' uvnitř tagu).

_____
**rola** (11.3.2010 10:51:56)

Taky by to mělo fungovat pro skupiny, designers./ignore

_____
**maruk** (22.3.2010 15:21:25)

Toto se mi hodně líbí. Jak pro jednotlivce, tak pro skupiny uživatelů (rovněž tak se mi líbí aby můj privátní tag ***.tag*** byl pak reprezentován prostě jako ***tag***, rule sety se tím pěkně zjednoduší).
