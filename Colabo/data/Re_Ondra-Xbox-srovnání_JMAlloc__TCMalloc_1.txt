﻿> > Očekávám, že tohle a ostatní věci nutné k přepojení alokátoru mi zabere zhurba celý dnešní den.

Oproti JMAllocu vidím, že program nevrací paměť OS (podobným problémem trpěl NedMalloc - viz . Něco takového jsem v poznámkách TCMallocu četl, ovšem mělo se to vztahovat jen na starší verze. Ve Windows portu to ovšem ještě asi není zahrnuto, v příslušné funkci je jen komentář:

`void TCMalloc_SystemRelease(void* start, size_t length) {
  // TODO(csilvers): should I be calling VirtualFree here?
}`
  
Náprava snad bude snadná. Pro úplnost výsledky z testovací scény (mise přiložena - [testMany.utes.zip][Attachment1]):

- po zavedení mise

` 406.609: MemAlloc - 151608456
 406.609: MemOvhead - 1291136
 406.610: MemCommit - 152407176
 406.610: MemReserved - 168995976

Virtual memory total 2047 MB (2147352576 B)
Virtual memory free 1624 MB (1703694336 B)
Physical memory free 422 MB (443039744 B)
Page file free 1863 MB (1953685504 B)
Process working set 175 MB (184307712 B)
Process page file used 192 MB (201887744 B)
Runtime reserved 161 MB (168995976 B)
Runtime committed 145 MB (152407176 B)
Longest free VM region: 829689856 B, busy 494137344 B, free 1653215232 B`
- po savu
` 814.443: MemAlloc - 153309712
 814.443: MemOvhead - 1334160
 814.443: MemCommit - 216871440
 814.444: MemReserved - 233435664

Virtual memory total 2047 MB (2147352576 B)
Virtual memory free 1553 MB (1628667904 B)
Physical memory free 294 MB (309293056 B)
Page file free 1570 MB (1647308800 B)
Process working set 239 MB (251359232 B)
Process page file used 254 MB (267292672 B)
Runtime reserved 222 MB (233435664 B)
Runtime committed 206 MB (216871440 B)
Longest free VM region: 758382592 B, busy 569159680 B, free 1578192896 B
`


[ondra 9.8.2010 15:19:26]:
 Náprava nebude tak snadná, jak jse myslel. `TCMalloc_SystemRelease` se pod Linuxem nepoužívá k úplném vrácení paměti systému, ale jen k decommitování. Snad to přeci jen dovedu zprovoznit, ale budu muset takovou paměť nevracet do "returned" seznamu (stav `ON_RETURNED_FREELIST`), ale úplně ji zahodit. To možná nebude až tak těžké, ale už to znamená změnu přímo uvnitř TCMalloc kódu, nejen ve "windows port" funkcích.
 

[ondra 11.8.2010 10:24:25]:
 Myslím, že jsem to snad jakž takž zapojil, nicméně mi s tím nyní alokátor občas padá. Jde mi z toho už hlava kolem, tak jen sepíšu repro a na chvíli to odložím.

 Repro mise přiložena (nejspíš se jedná o totéž, jako jsem už jednou přikládal, ale jist si nejsem)
 
 - spusit hru v debugu, s `-init="-init=playMission['','Testy\test.utes']"`
 - dát Win-S (save)
 - (Win-S možno několkrát opakovat)
 - dát Escape a z menu dát Suspend
 - dostanu assert v PageHeap::MergeIntoFreeList, kdy následující span získaný dle pagemap_ nesouhlasí
 



[ondra 12.8.2010 13:53:44]:
 Myslím, že to mám vše opravené, současně začínám i trochu rozumnět tomu, jak to vlastně uvnitř funguje.
 
 Hlavní problém byl v tom, že při zrušení spanu je nutno odznačit ukazatele vedoucí na něj ze zpětného indexu. Pod Linuxem nikdy k rušení spanu bez náhrady nedocházelo, takže je tento problém netrápil.
 
 Ještě aktualizované TCMalloc výsledky z kontrolního měření:

 - po zavedení mise

 ` 555.929: MemAlloc - 130768352
 555.929: MemOvhead - 1412224
 555.929: MemCommit - 145743872
 555.930: MemReserved - 163315712
Virtual memory total 2047 MB (2147352576 B)
Virtual memory free 1636 MB (1716379648 B)
Physical memory free 563 MB (590610432 B)
Page file free 1669 MB (1750401024 B)
Process working set 161 MB (168996864 B)
Process page file used 179 MB (188612608 B)
Runtime reserved 155 MB (163315712 B)
Runtime committed 138 MB (145743872 B)
Longest free VM region: 844861440 B
VM busy 481452032 B (reserved 174571520 B, committed 306880512 B, mapped 27725824 B), free 1665900544 B`

 - po savu
 ` 668.274: MemAlloc - 131672816
 668.275: MemOvhead - 1423232
 668.275: MemCommit - 148168704
 668.275: MemReserved - 164757504
Virtual memory total 2047 MB (2147352576 B)
Virtual memory free 1634 MB (1714413568 B)
Physical memory free 549 MB (576507904 B)
Page file free 1661 MB (1741881344 B)
Process working set 163 MB (171462656 B)
Process page file used 181 MB (190115840 B)
Runtime reserved 157 MB (164757504 B)
Runtime committed 141 MB (148168704 B)
Longest free VM region: 795607040 B
VM busy 483418112 B (reserved 175067136 B, committed 308350976 B, mapped 28250112 B), free 1663934464 B`
