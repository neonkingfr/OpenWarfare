﻿1) Vyhledávám "Jirkovo písko"
2) Vybírám jediný bez "Re: " ( [Jirkovo pískoviště](data/19cc3f57-c2c6-452f-9f4f-4cc6a4598f7c.txt?server=dev.bistudio.com&database=colabo&id=23) )
3) Všímám si, že posted by je Maruk a ukládám do paměti i datum
4) Dvojkliknu, skočí mi to na článek
5) Autorem je Jirka a datum je jiné. Článek nemá žádná podvlákna (ačkoli podle vyhledávání jich asi má spoustu a Maruk je asi poslední z nich).


- Neměl by ve vyhledávání být původní autor? Proč Maruk?
- Rád bych viděl celé vlákno včetně reakcí. Dělám něco špatně?


6) Tlačítko back mi odstraní vyhledaný článek ze seznamu a forwardem se na něj už nedostanu.


- Nebylo by lepší než článek přidávat do aktuálního seznamu článků skočit přímo na článek a klidně modifikovat aktuálně vybrané tagy, ale zároveň zařadit vyhledání do fronty back-forward?
