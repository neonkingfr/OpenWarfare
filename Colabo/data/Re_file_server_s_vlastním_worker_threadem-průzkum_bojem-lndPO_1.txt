﻿> Procházení seznamu mě unavovalo, tak jsem zkusil pro zpestření průzkum bojem.

Kontroluji na Chernarusi výkon, nejprve neformálně. Narazil jsem na frame, kdy se 133 ms strávilo v 325x lndPO. Přitom 32m se trávilo v 9898x memLo, tedy v práci s kritickou sekcí alokace paměti. Prohlížením záznamu je vidět, že tu a tam k čekání mezi thready opravdu došlo, zejména pak mezi hlavním threadem a  FileServerAsync (v tom async serveru dochází v souvislosi s předávání instrukcí a jejich úklidem k velkému počtu malých alokací, tady odstranění FastAllocu bolí hodně).

_____
**ondra** (15.3.2010 14:42:29)

Problém s alokacemi zhoršovala [hierarchie CS](news:gh3fv3$lcc$1@new-server.localdomain) - na PC, kde je zcela zbytečná, jsem ji odstranil.


_____
**ondra** (18.3.2010 14:50:32)

Je to funkční, ale ještě ne na uzavření - je tam v úklidu paměti nepěkný hack, kterým se zamyká file server.


_____
**ondra** (8.4.2010 19:40:38)

> je tam v úklidu paměti nepěkný hack, kterým se zamyká file server.

Hack vyčištěn.