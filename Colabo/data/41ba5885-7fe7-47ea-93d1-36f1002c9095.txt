﻿> ***[24.11.2009 15:00:27] Marek:*** URL dokumentu je sice fajn, ale je to asi neco trochu jineho otevrit neco primo v svn ci wiki anebo v colabu.

Pokud by se používaly HTTPS odkazy, mohlo by být nasledující chování:

V Colabu
--------

Pokud Colabo najde odpovídající dokument, přejde na něj tak, jak se v tuto chvíli přechází při kliknutí na `(colabo:id=222)`. Colabo mi dokument ukáže, jak umí, pokud ho snad chci otevřít externě a editovat, jistě na to časem v Colabu taky nějaká možnost bude.

Mimo Colabo
-----------

Mimo Colabo by https fungovalo normálně.

Výhody
======

- odkazy fungují i mimo Colabo.


Nevýhody
========

- nejednoznačné URL-Colabo příspěvku, připadá mi jako okrajový a fiktivní protipříklad.

- odkazy otevřené mimo Colabo se neotevřou v Colabu, ale ve webovém prohlížeči


