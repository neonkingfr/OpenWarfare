﻿Webové rozhraní je sice pro běžnou práci pomalé, ale má celkem velké výhody:

* běží na různých zařízeních (např. tablet)
* nevyžaduje (často složitou) instalaci

Netuším, zda by Colabo šlo takto udělat, ale přínosné by to bylo.


[jirka 24.5.2011 17:21:20]:
 Obávám se, že by šlo o dost rozsáhlý úkol. Některé jeho části (komunikace s databází přes web services, čtení ASCII verze aktuálních příspěvků) by ale mohly být dost snadné.

[maruk 27.5.2011 16:24:31]:
 Používat pro čtení a případně i nějakou editaci jen ascii by mi pro webového klienta přišlo zcela postačující. To je mj. síla mark up jazyka.
 
 Co si myslím je takový minimalistický zajímavý interface:
 
 * přihlášení k jedné colabo databázi
 * filtrování přes tag cloud
 * výpis příspěvků s nějakým základním využitím rulesetů (hlavně filtrování)

[jirka 23.6.2011 10:47:04]:
 Pokud zájem trvá, myslím že bych mohl začít s nějakými experimenty.
 - spíše než přihlášení ke colabo databázi bych volil přihlášení k LDAP serveru a bral všechna nastavení (včetně přihlášených databází) odtamtud
 - šlo by o java applet, hostovaný nejdříve na nějaké testovací adrese (http://www.bistudio.com/jirka/colabo/index.html), a po odladění přesunutý na colabo server (https://dev.bistudio.com)

[maruk 23.6.2011 13:13:59]:
 Rozhodně. Mě se např. colabo na veřejné wifi síti (nějaké nastavení windows kvůli bezpečnosti) nepřipojilo, prostě tiše selhalo. Mít nějaký byť omezený přistup ke colabu z webového rozhraní se mi jeví i nadále velmi zajímavé.

[jirka 27.6.2011 16:09:00]:
 Začal jsem se tomu věnovat. Zatím (cca 2 dny) se mi podařilo vyřešit:
 - komunikaci s LDAP serverem
 - komunikaci s Web Services
 - parsování JSON komunikace s Web Services
 
 Další věci už by měly být méně náročné (datové struktury, UI), předpokládám že během zítřka bych zveřejnil první pokus kde už by něco bylo vidět a pak bychom se dohodli, kam až bude účelné to rozvíjet.

[jirka 28.6.2011 12:00:21]:
 Zdrojové kódy appletu jsou v SVN: trunk/src/ColaboApplet
 
 Lokálně mi funguje minimální funkčnost (neuspořádaný seznam příspěvků a jejich textový obsah), ale nedaří se mi to zatím zprovoznit na webové stránce (http://www.bistudio.com/jirka/Colabo/) - zlobí inicializace LDAP knihovny (unboundid). 

[jirka 28.6.2011 14:12:47]:
 Konzultoval jsem problém s Hlaváčem a ukazuje se že bude hlubší než jsem očekával. Souvisí to se zabezpečením appletů - nedovolí přes sockety komunikovat se třetí stranou (tedy u nás s LDAP serverem), řešením by snad bylo nějaké podepisování kódu, ale to bych musel nastudovat.
 Po další rozmluvě docházím k názoru, že applet stejně asi není to, co bychom ideálně chtěli. Spíše bychom stáli o to, aby aplikační logika běžela na serveru a webové rozhraní bylo velmi lehké (pouze UI, ideálně pouze s využitím HTML, aby to běželo i na Androidu a spol.). Zkusím se podívat po nějaké technologii, která by to umožňovala (snad http://cs.wikipedia.org/wiki/JavaServer_Faces).
