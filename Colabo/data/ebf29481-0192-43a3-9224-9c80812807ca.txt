﻿> 1) poznám, kdo je fyzicky v kanceláři 

Pro Skype existuje jednoduchý ["geolokační" addon](https://extras.skype.com/1295/view) - každému uživateli můžeš zaregistrovat různé IP adresy a podle nich ti to ukazuje, kde zrovna je. 

-----
***ondra** (22.12.2009 14:50:26)*: Zkusil jsem to a funguje to nějak špatně. Pořád mi to ukazuje, že jsem v Praze. Čekal jsem, že si budu moct sám namapovat IP adresy na místa.

-----
***maruk** (22.12.2009 16:09:26)*: Zkusil jsem a na win7 x64 to nefunguje vůbec, prostě to hned spadne.

-----
***ondra** (22.12.2009 17:25:34)*: Navíc nejde odinstalovat. Idea je sice pro inspiraci zajímavá, ale provedení nestojí za nic. Rozhodně se mu vyhněte.