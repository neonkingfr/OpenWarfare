﻿> Souhlasím že je třeba mít možnost příspěvek v kalendáři explicitně umístit. Nejlepší by byla možnost volitelně zadávat začátek a konec (nebo jeden z údajů a v tom případě by se druhý jednoduše odvodil) události. Mohlo by se to dělat buď přes speciální tagy (se kterými bychom asi chtěli zacházet trochu jinak, například neukazovat v cloudu) nebo novými nezávislými vlastnostmi příspěvku.


Tohle mi připadá poměrně důležité. Ale nevím, v čem se to v principu liší třeba od priorit či přiřazení, kromě toho, že možných dat a časů by v cloud tagu opravdu rychle bylo moc (ale ono třeba 100 uživatelů, kteří v colabu mají přiřazený úkol, bude asi už také moc). V minulosti jsem si to nainvně představoval nějak tak, že tagy mohou mít nějaký parametr.


> > * v kalendáři nejde založit nový příspěvek
> 
> Příspevěk lze založit standardními způsoby - jako odpověď na zvolený příspěvek nebo z toolbaru. Pokud si představuješ ještě jinou možnost, pak ji prosím popiš.

Z nějakého důvodu mi přišlo intuitivní kliknout někam na prázdný řádek kalendáře a tam se pak pokoušet založit článek. Asi proto, že to tak funguje jinde (Lighting) kde double click zakládá new event (příp. mohu event nebo task založit přes pravou myš).

Určitě ale závisí na možnosti články do kalendáře vůbec zařadit...
[maruk 19.11.2010 15:44:03]:
 ( % ) tagy již umíme. Tj. asi chybí jen krok k tomu, moci příspěvku nastavit, aby se nějak speciálně v kalendáři zobrazoval (pro rozmezí času).

[jirka 5.1.2011 10:08:03]:
 Určitě. V podstatě chybí jen finální rozhodnutí, jaké tagy použít a jak je zadávat.
 
 Moc se mi z hlediska architektury nelíbí mít program navázaný na konkrétní tagy a konkrétní formát jejich hodnot, raději bych kdyby se čas příspěvku z tagů dostal nějakým skriptem nebo datovým popisem. Není mi zatím úplně jasné jak (ale evokuje mi to nějaké event handlery).
