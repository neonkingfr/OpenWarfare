﻿Je to celkem hezké.

Trochu bych chtěl default řazení upravit, ale něco asi dělám špatně, protože nejsem schopen dosáhnou kýženého výsledku, snad je vidět, o co se snažím (ve vlastním pokusném rule setu, který nic nedědí):

none(
:to-read => SORTORDER:20; 
.important => SORTORDER:15; 
/priority & :@me => SORTORDER:10; 
:@me & !/test => SORTORDER:5; 
* => SORTORDER:3;
/done => SORTORDER:2;
/fixed => SORTORDER:1;
/bug & /done => SORTORDER:1;
/closed | /ignored => SORTORDER:0;
/test => SORTORDER:0
)

Ale asi tam mám nějakou logickou botu. Např. moje pískoviště ***/test, @maruk*** se mi nedaří dostat na úplný konec seznamu, kde bych ho chtěl mít. 

_____
**jirka** (31.3.2010 12:16:44)

`* => SORTORDER:3;`

Tohle pravidlo zachytí všechno, cokoli pod tím se už neuplatní. Když dáš tenhle řádek na konec, mělo by to fungovat.
