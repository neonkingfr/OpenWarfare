﻿>  - SharpSVN (nutno aktualizovat knihovnu v Colabu, nějaká verze je na https://www.nuget.org/packages/SharpSvn.1.8-x64, nevím, nakolik už je stabilní)

Upgradoval jsem knihovnu použitou pro Colabo.

>  - AnkhSVN (v jeho rámci se vyvíjí SharpSVN, takže to bude nejspíš současně)
>  - TortoiseSVN (už je)

Tohle jsem si následně upgradoval na svém počítači také. Kdo si vezme nové Colabo, měl by taky své SVN klienty upgradovat, jinak nemusí být schopen správně pracovat s Colabo working copy. Pokud neuděláte ručně upgrade svých working copy v `%userprofile%\AppData\Local\Colabo`, dříve, než nové Colabo spustíte, tak vám je Colabo samo smaže a postupně doplní.

Až někdy někdo bude upgradovat klienta na Jenkinsovi, bude nutné upgradovat SharpSvn použité pro SVNRevision z [Číslování buildů](data/1bb98d57-3706-4039-9dca-d260cbc54197.txt?server=https%3A%2F%2Fdev.bistudio.com%2FColabo%2Fresources%2F&database=&id=157).

[ondra 1.7.2013 11:33:11]:
 Protože jsme zatím nebyli schopni upgradovat [SVN plugin pro Jenkinkse](https://wiki.jenkins-ci.org/display/JENKINS/Subversion+Plugin) (ani [SVNKit](http://svnkit.com/), na kterém je plugin postaven, zatím v 1.8 verzi není), upravil jsem SVNRevision tak, že v případě nepodporovaného formátu nevrací chybu, ale prázdný řetězec. V buildu se pak volá utilita dvakrát, jednou v 1.7 verzi a podruhé v 1.8 a výsledek se spojí. Build tedy funguje s working copy 1.7 i 1.8.
