﻿Plánujeme na toto téma setkání ve čtvrtek 16.9 v Mníšku.

* celkové plány vývoje souvisejícího s Arma enginem do roku 2012

* koordinace vývoje mezi týmy (sdílení technologií)

* domluvit se na pracovních postupech (branche, merge, tooly)

* prvotní rozbor plánu pro jednotlivé oblasti v enginu


Předpokládaná účast:
---


Lukáš Babíček - senior prg Brno
Filip Doksanský - lead prg. Praha-Black Element
Mark Dzulko - CTO, VBS
Vojta Hladík - senior prg. Mníšek
Jan Hloušek - lead prg. Centauri
Jiří Martinek - senior prg Mníšek
Daniel Musil - vedoucí projektu Brno
Vladimír Nejedlý - vedoucí programátor VBS
Mirek Papež - vedoucí projektu Centauri
Zdeněk Pavlík  - vedoucí programátor Brno
Marek Španěl - CEO / vedoucí projektu Mníšek 
Ondřej Španěl - vedoucí programátor Mníšek


Program:
---
11:00-12:00 Roadmap, celkové plány atd., všichni najednou

13:00-14:00 dohoda na pracovních postupech napříč týmy, sdílení práce

14:15-16:00 diskuse o jednotlivých technologiích (pravděpodobně již v menších skupinách podle oblasti zájmu)







