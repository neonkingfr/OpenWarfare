﻿Jak tak používám Colabo ve více oknech, začínám mít problém, že při přepínání mezi nimi v taskbaru nebo Alt-Tabem nevím, které je které.

Chtěl bych, aby v titulku okna byla kromě názvu aplikace a verze i databáze a aktuální cesta tagového filtru.


[jirka 27.1.2010 13:25:06]:
 Hotovo v revizi 63453.

[ondra 27.1.2010 13:43:39]:
 Skvělé, děkuji moc. Ještě mě napadá, jak tak koukám na to, jaký titulek má Visual Studio, a na to, jak to Windows v liště zkracují, že by bylo dobré to úplně obrátit - nejdřív tagy, pak databázi, to, že je to Colabo, až nakonec (to nakonec poznám i podle ikony)

 Totéž nakonec asi platí i pro titulky u Append note / Edit / New message

[ondra 27.1.2010 13:48:18]:
 >nejdřív tagy, pak databázi, to, že je to Colabo, až nakonec
 Hned jsem udělal.

 >Totéž nakonec asi platí i pro titulky u Append note / Edit / New message
 Udělám časem.

[ondra 27.1.2010 15:15:39]:
 A i to je hotovo.
