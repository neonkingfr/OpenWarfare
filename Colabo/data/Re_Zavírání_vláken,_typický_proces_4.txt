﻿Co takhle zavést si trochu jinou konvenci a tagy pro změnu stavu vlákna nějak jednoduše rozlišit od ostatních, mně by se asi nejvíc líbilo řešení ve stylu:

***/done 
/closed 
/fixed
/resolved
/ignored***

Připadá mi, že tyto věci jsou natolik specifické, že si něco podobného zaslouží a v současnosti mi připadá docela podviné že done či fixed je uvedeno dost nahodile mezi ostatními tagy. V tuto chvíli by změna znamenala upravit si ruleset plus [masově přejmenovat tagy](data/_Mass_rename_tagů_1.txt?server=dev.bistudio.com&database=colabo&id=657) (takovou funkci pokládám stejně za dost potřebnou). Jinak asi pro tyto stavové tagy zatím žádné specialní vychytávky nepotřebujeme, ale pokud bychom v budoucnu rozhodli jinak, budou dobře izolované od ostatních.


_____
**ondra** (2.3.2010 10:39:08)

Je to vpodstatě ekvivalent Close:Fixed, Close:Ignored atd. To, jestli tag začíná speciálním znakem, nebo speciální sekvencí, je zcela lhostejné. Můžeme vybrat cokoliv z toho, co nám bude připadat přehlednější.


_____
**jirka** (2.3.2010 10:45:21)

Jinou možností než pro každý typ tagu zavádět prefixy může být definovat typ tagu externě (ve zvláštní tabulce tagů). Pak by musela existovat snadná možnost zeptat se na typ tagu v rule setu.

_____
**rola** (2.3.2010 11:04:27)

Hodilo by se celkově vyčlenit tagy, které udávají stav úkolu, pomocí synonym. Dalo by se napsat všechno, v seznamu tagů by se zobrazily jen lomítkové verze. Lomítko je na to dobré, nebudou na to třeba větší změny.

**/priority1…3** = critical acute priority
**/stage1…6** = specification analysis prototype alpha beta gold
**/necessity1…3** = nice-to-have desired required
**/done** = done closed fixed resolved ignored expired
