﻿Článek [Klávesové zkratky](data/Klávesové_zkratky1.txt) u mě způsoboval exception. Měl jsem ho v databázi označen jako přečtený v určité revizi, ale změnilo se URL a revize tak byla neplatná.

Opraveno (commit 62506).
