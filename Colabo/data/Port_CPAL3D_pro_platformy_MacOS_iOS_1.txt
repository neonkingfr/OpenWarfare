﻿Narocnost prvni platformy: odhad 70 dni
Druha: 30 dni


Je v tom celkem 20 dni Cendy a 80 dni externiho prg (cca 80k mesic faktura) na zakladni port
Celkove odhadovane naklady: 400.000,- CZK zakladni port, 200.000,- finalizace


Znama uskali: PhysX, QA


Dostaneme:
 MacOS - Memento Mori 1
 MacOS - Memento Mori 2
 MacOS - Alternativa
 MacOS - Pat & Mat
 iPhone/iPAD - Pat & Mat
 iPhone/iPAD - Alternativa
 CPAL3D pro MacOS/iOS (+nakroceni k Android/PS3)
  snad MacOS - PoG (vetsi narocnost s prepisem meqonu, test na Apple HW) 
  snad iPhone/iPAD - PoG  
  snad iPhone/iPAD - Memento Mori 1&2 (k reseni - velky datovy objem)

Mozna distribuce MAC veci pres Steam

Mozne zafinancovani: dostat na to penize z nemcu, za MAC verze MM2/MM1

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head><title>Cpal3dRoadmap</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
<style>
body {
font-family: Arial, Helvetica, Sans-serif;
font-size: 11pt;
color: black;
}
h1 {
text-align: center;
background-color: rgb(126, 126, 245);
}

li { list-style-type: disc; }
li li { list-style-type: disc; }
li li li { list-style-type: disc; }
li li li li { list-style-type: disc; }

#page {
margin: 0 auto;
padding: 1% 1%;
}
</style></head>
<body>
<div class="page"> <h1><a name="Cpal3d_Roadmap"></a>  Cpal3d Roadmap </h1>
<p>
<div> <ul>
<li> <a href="#Introduction_Disclaimer"> Introduction &amp; Disclaimer</a>
</li> <li> <a href="#Strategy"> Strategy</a>
</li> <li> <a href="#Tasks"> Tasks</a> <ul>
<li> <a href="#Sound_System"> Sound System</a>
</li> <li> <a href="#Compiler_C"> Compiler / C++</a>
</li> <li> <a href="#MS_CRT_Reimplementation"> MS CRT Reimplementation</a>
</li> <li> <a href="#Direct3D_Compatible_Math_Classes"> Direct3D Compatible Math Classes</a>
</li> <li> <a href="#Input"> Input</a>
</li> <li> <a href="#IO"> IO</a>
</li> <li> <a href="#Video"> Video</a>
</li> <li> <a href="#Middle_Level_Renderer"> Middle-Level Renderer</a>
</li> <li> <a href="#Low_Level_Renderer"> Low-Level Renderer</a>
</li> <li> <a href="#Total_to_Alpha"> Total to Alpha</a>
</li></ul> 
</li> <li> <a href="#Left_Out"> Left Out</a>
</li></ul> 
</div>
<p>
Thursday 2010-Nov-04, 14:15 - 17:15
</p><p>
</p><h2><a name="Introduction_Disclaimer"></a> Introduction &amp; Disclaimer </h2>
<p>
Very rough estimates how to get from the current state of code
into an alpha state
(alpha = features do exist, although there are bugs everywhere,
no proper integration with the system,
not all corner cases are handled properly).
</p><p>
The estimates are based on incomplete information,
and by spending just a couple of minutes per task
figuring out the issues.
The estimates cover only stuff explicitely mentioned
(be sure to check the Left Out chapter below),
and mostly cover just the time required to reach the state of "first runnable".
</p><p>
</p><h2><a name="Strategy"></a> Strategy </h2>
<p>
The general approach to porting is basically
a function-call-level based emulation of APIs currently in use.
</p><p>
Solving out the perfect matching of existing semantics,
and any potential performance issues is left to the later phase of the project.
(It is very likely that the alpha-to-beta transition will last quite long,
and that the improvements during that period will be game-project specific.)
</p><p>
</p><h2><a name="Tasks"></a> Tasks </h2>
<p>
</p><h3><a name="Sound_System"></a> Sound System </h3>
<p>
Function level DSound emulator backed up by OpenAL.
</p><p>
Possibly tricky areas: </p><ul>
<li> streaming (DSound version uses a single repeating buffer,     filling in data by querying the current position)
</li> <li> EAX reverbs, although they can be left out in the initial port
</li></ul> 
<p>
Estimate: 5D
</p><p>
</p><h3><a name="Compiler_C"></a> Compiler / C++ </h3>
<p>
Making the code compile by gcc.
Partially in progress.
</p><p>
Estimate: 10D
</p><p>
</p><h3><a name="MS_CRT_Reimplementation"></a> MS CRT Reimplementation </h3>
<p>
All the non-standard or non-matching functions.
</p><p>
Estimate: 5D
</p><p>
</p><h3><a name="Direct3D_Compatible_Math_Classes"></a> Direct3D Compatible Math Classes </h3>
<p>
Vector, matrix, quaternion,
matching the D3DX implementation.
</p><p>
Estimate: 3D
</p><p>
</p><h3><a name="Input"></a> Input </h3>
<p>
Mouse and keyboard input layer,
reacting on OS events and generating engine actions.
Main window, message processing, character input.
</p><p>
Estimate: refactoring existing code - 1D, <br>
OSX port - 5D
</p><p>
</p><h3><a name="IO"></a> IO </h3>
<p>
<code>Rfile</code> / <code>File</code> code.
</p><p>
Estimate: 2D
</p><p>
</p><h3><a name="Video"></a> Video </h3>
<p>
Creation of Core Video backed player code,
with the same interface as the existing WMV player.
</p><p>
We ignore the codec issues for now.
</p><p>
Estimate: 5D
</p><p>
</p><h3><a name="Middle_Level_Renderer"></a> Middle-Level Renderer </h3>
<p>
Creation of classes to represent vertex buffer,
index buffer, and texture data.
Modification of existing render objects to use these new classes.
</p><p>
Estimate: 5D, <br>
OSX port of the new classes: 8D
</p><p>
</p><h3><a name="Low_Level_Renderer"></a> Low-Level Renderer </h3>
<p>
Basic OpenGL initialisation,
reimplementation of the render states in OpenGL,
and implementation of render target class.
(Vertex, index, texture buffers are included in the previous task.)
</p><p>
Estimates: basic GL setup - 5D, <br>
render states - 12D, <br>
render target - 5D <br>
</p><p>
</p><h3><a name="Total_to_Alpha"></a> Total to Alpha </h3>
<p>
Non-rendering (including video): 36D <br>
Rendering: 35D <br>
</p><p>
</p><h2><a name="Left_Out"></a> Left Out </h2>
<p> </p><ul>
<li> FX system - intentionally left out,     it will not be a part of the initial porting effort.
</li> <li> shader code conversion - intentionally left out,     it will not be a part of the initial porting effort.
</li></ul> 
<p> </p><ul>
<li> Introduction of <code>wchar16_t</code> - accidentally left out.
Changing all the resource formats to use the new 16 bit character type,
creation of glue code on the border between run-time <code>wchar_t</code> type     and data taken from resources.
</li></ul> 
<p> </p><ul>
<li> Any PhysX porting work - not enough information to generate any estimates.
</li></ul> 
<p>
</p></div>
</body></html>

