﻿Ruším svislítko `|` – místo něj je `][`. Je to kompatibilní a v textu to vypadá hezky :-).

Přesunul jsem změny do původního textu.
_____
**ondra** (11.3.2010 15:09:07)

`[text][url]`

tahle možnost se mi nelíbí, původní markdowné se mi zdá lepší:

`[text](url)`


_____
**rola** (11.3.2010 15:49:23)

Ve výsledku by mohly fungovat obě (kvůli kompatibilitě) – nikdy jako uživatel nevím, jesli mám napsat `[text](url)` nebo `(text)[url]`.

Teď prostě napíšu `[url]` nebo `[text][url]` a je to. Případně jen `url`.
Obrázky taky budou jednoduché: `[url]` nebo `[url tooltip]` nebo `[alt text][url]` nebo `[ref]`.


_____
**rola** (11.3.2010 15:58:00)

Připomínám, že původní Markdown podporuje zápis `[text][ref]` – jen jsem rozšířil druhé pole, aby mohlo obsahovat i `url` (pokud neexistuje reference s tímto názvem).

Zrácený zápis `[ref]` se v původním Markdownu píše jako `[ref][]`.

**Kompromis:** druhá závorka může být i kulatá ve všech případech.

_____
**rola** (11.3.2010 16:19:00)

Upraveno v [původním návrhu](data/Markdown_pravidla_pro_Colabo_1.txt?server=dev.bistudio.com&database=colabo&id=970).
