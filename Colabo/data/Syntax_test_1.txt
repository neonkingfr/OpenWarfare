﻿### Syntax test

link http://daringfireball.net/projects/markdown/syntax#precode
[pojmenovany link](http://daringfireball.net/projects/markdown/syntax#precode)




\*text s hvezdickami\*

"uvozovky"

*kurziva*

**tucne**

***tucna kurziva***


`
kod
`


* plne kolecko
- plny ctverec
+ prazdne kolecko
1. cislo 1
 * pod bod
 * pod bod
2. cislo 2



___
plna cara cerna

***
plna cara svetlejsi

===
plna cara seda

---
carkovana cara





Nadpis 1
***

Nadpis 2
===

Nadpis 3
---

# Nadpis 1
## Nadpis 2
### Nadpis 3
#### Nadpis 4
##### Nadpis 5
###### Nadpis 6


> citace 1
>> citace 2
>>> citace 3
>>>> citace 4
>>>>> citace 5


 
![Toto je obrázek](http://forums.bistudio.com/images/bis/bis_logo.png)