﻿> Náš stávající workflow trpí v Colabu jednou nectností:
> 
> Hotové úkoly do určité míry zmizí (při default rule setu) a snadno se pak ztratí dokumentace.

Ve skutečnosti bych řekl, že problém mizení není tak horký, jak vypadá. Pokud něco opravdu hledáš, stejně tohle hledáš fulltextem, a ten na nějaké zavírání stejně nehraje.


-----
***maruk** (9.2.2010 14:28:21)*: Pokud víš, co přesně hledáš, tak možná, ale rozhodně to je vůči wiki krok zpět.

Dokonce i vůči nr, tam jsme v announce měli úlohu otevřenou i jako alpha či beta navždy. Colabo svou otevřeností obávám se momentálně dost trpí. Samo o sobě pro architekturu je to výhoda, pro práci je to ale neštěstí a musíme s tím něco udělat.