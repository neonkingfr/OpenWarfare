﻿Test neuzavřených konstrukcí.

Ruby 1.9:

rb`
def shuffle(a)
  a.size.times { |i|                     # Pro 'i' od nuly do 'a.size'
    j = i + rand(a.size - i)             #   Nastav 'j' náhodně mezi 'i' a 'a.size - 1'
    a[i],a[j] = a[j],a[i] unless i==j    #   Pokud se 'i' a 'j' liší, prohoď 'a[i]' s 'a[j]'
  }
end

puts %w[a b c d e].shuffle               # Vypiš zamíchané pole ["a", "b", "c", "d", "e"]`

