﻿> Pro mnohé tagy v Colabu stačí, aby byl článek jen nějak barevně odlišen, ale zrovna pro tagy @xxx navrhuju všechny aktuální @tagy připojit za název článku. Nebo to mít alespoň jako konfigurovatelnou možnost.
> Samostatný sloupec se mi pro tento účel zdá méně vhodný.
>
> Co vy na to?

Mně by se líbil ten extra sloupec (klidně vypnutelný), ať jsou zarovnané doleva a ať se podle nich dají třídit články. V tom sloupci by se nemuselo psát `@`.
