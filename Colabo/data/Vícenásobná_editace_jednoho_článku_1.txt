﻿V současné době mohu otevřít ten samý článek pro editaci třeba stokrát.

Nenapadá mě pro to žádné smysluplné využití, naopak to snadno může vést k chybám.

Navrhuji to nepovolit a případý pokus o editaci článku, který již edituji, by mi měl dát do popředí to jeho již otevřené editační okno.

_____
**jirka** (16.3.2010 10:16:37)

Hotovo v revizi 67171.


_____
**ondra** (16.3.2010 11:21:54)

Někdy to dělám, když jsem z článku při editaci něco smazal, ale pak si uvědomím, že bych to tam chtěl zas zpátky. Nejsnažší způsob, jak se k tomu dostat, je otevřít si druhý editor. Chápu ale, že rizika špatného použití jsou příliš velká.

Pokud bude fungovat [diff](data/Re_Zvýraznění_posledních_změn,_lepší_práce_s_revizemi-diff_1.txt?server=dev.bistudio.com&database=colabo&id=994), tak to budu dělat přes něj.
