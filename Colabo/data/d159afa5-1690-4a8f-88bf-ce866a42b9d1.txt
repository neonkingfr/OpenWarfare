﻿> Prvním krokem by tedy bylo pro SVN dokumenty ukládat místo pouhého faktu zda je článek přečtený poslední přečtenou revizi [Re: Označování přečteného](data/0a38dcdc-9fdf-41ee-ad84-fce6b002722e.txt). 

To je hotové (revize 61129).

Teď by to chtělo rozhodnout, zda opravdu stojíme o (pravděpodobně pomalejší) SVN přístup ke článkům a případně rozmyslet co a jak tu dělat asynchronně.

Napadají mě další alternativy:

- články defaultně zobrazovat tak jak jsou, rozdíly ukázat na požádání
- pomalejší přístup zvolit jen u článků označených jako nepřečtené (to by fungovalo všude kde ke změně nedojde externě ale jen přes Colabo)
- vytvářet working copy článků preventivně na pozadí (jako úlohy s nižší prioritou)