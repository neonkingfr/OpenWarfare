﻿> - Když někdo vystřelí, kulka začne se `_simulationSkipped` převzatým od střelce (od AI tedy záporným). Jelikož se ho snaží udržet na nule, tak začne svou simulací tím, že nejprve chvíli počká.

Tohle je téměř ekvivaletní tomu, že se kulka vystřelí z pozice dané RenderVS, ale použití RenderVS je o něco lepší v tom, že to na sebe přesně časově navazuje, čímž se zjednodušuje třeba přeměna proxy na raketu.

Vůbec se kloním k názoru, že nejjednodušší bude Render VS používat velmi široce, a to i na většinu testování kolizí. Jako první zkusím Render VS používat při střelbě (a při míření, to je nutné, pokud chci, aby se mířilo správně).

Jedna otázka, která zbývá k dořešení, je ohledně odezvy na zjištěnou kolizi. Původní plán byl:

> - O je v budoucnosti: `E.ActTime()\<O.ActTime()` - zeptáme na `O.PastState(O.ActTime()-E.ActTime())`, kolizi testujeme vůči tomuto minulému stavu. Pokud zjistíme, že interakce mohla O ovlivnit, vrátíme jeho stav i čas zpět do posledního zapamatovaného stavu **Nastává typicky pro hráč vs. AI nebo střela vs. AI

Vracení do minulosti je ovšem trochu problém, protože vizuální stav není zdaleka postačující po simulaci (neobsahuje třeba úhlovou rychlost), takže vrácení VS není zcela korektní. Je možné, že to nebude vypadat nijak hrozně, nebo je možné, že se ukáže lepší se nevracet a jen na straně zareagovat, jako by daná událost nastala v okamžiku `O.ActTime()`.
[ondra 26.5.2011 16:43:05]:
 Pro střelbu už R používám, následovat bude míření, pak udržování pozice ve formaci.

[ondra 11.7.2011 15:24:21]:
 Přikládám [test case pro střelbu][Attachment1] . V režimu interpolace full je třeba střílet výrazně dopředu, jinak minete. Se zapnutými diagnostikami je "Future" pozice vidět jako zelená koule.
 
 

[Attachment1]: data/ArmA2/test.Desert_E.zip