﻿> Cílem je oddělit simulaci v ArmĚ od stavu, který je potřeba na vykreslování. Mezi těmito **vizuálními stavy** pak bude rendering interpolovat.

Zavedl jsem chráněný stav, který znamená to, že dokud se např. odkazuji na RenderVisualState, nemohu se od téhož objektu odkazovat na NewestVisualState, a naopak (myslím, že když se to stane, je to neobvyklé a divné).

- Očekávám, že budu v budoucnu muset zavést funkci, které dovolí udělat výjimku.
- Očekávám, že to budu muset předělat na TLS, aby testy probíhaly pro každý thread zvlášť, nebo testy dělat jen na hlavním threadu.
- chtěl bych postupně veškeré přístupy předělat na tento chráněný styl. To hlavně znamená, že v každé funkci, kde chci na VS přistupovat, bych tak měl dělat přes výslovně vytvořenou proměnnou, a ne se přímo odkazovat na membery VS získaného voláním NewestVisualState / RenderVisualState