﻿> - vyčlenit vizuální stav RTM animací a začít ho používat, pro začátek bez interpolace

Zapomněl jsem na animace mimo RTM (míření, rozhlížení...). Ty tam taky musím vyčlenit.

Dále:
- _cameraPositionWorld (spočtená pozice kamery)
- _aimingPositionWorld (např. pro autoaim - musíme mířit tam, kde vidíme)

[ondra 3.12.2010 15:25:27]:
 Většinu mám myslím hotovu. Vynořil se mi ale jeden otazník:
 
 V případě proměnných jako je _gunXRot se nabízí dva přístupy:
 
 1) interpolovat výsledné matice
 2) interpolovat zdrojová data, výsledné matice rekonstruovat
 
 1) se mi zdá o něco málo rychlejší, též to bylo jednodušší na implementaci, tak jsem to tak zatím udělal, s tím, že zdrojová data ve VS ani nejsou . 2) se mi ale zdá robustnější (není třeba přemýšlet, které matice jsou ortnormální),a pamatovalo by se k tomu méně dat

[ondra 6.4.2011 11:49:53]:
 Zdá se nakonec, že přístup 1) je chybný, protože předpokládá, že se středy otáčení hlavy apod. pohybují po přímkách. Nezbude tedy, než implementovat 2). Udělám to ale tak, že dotyčné matické také budu držet ve VS, nebudu je počítat pokaždé, když je někdo chce, ale spočítám je v rámci interpolace ze zdrojových hodnot.

[ondra 6.4.2011 14:09:41]:
 Nakonec jsem to udělal jednodušší, jen přepočítávám osy otáčení pro matice. jinak jsem stavy nechal, kde jsou.

[ondra 7.4.2011 14:27:26]:
 To nestačilo, ještě bylo nutno pro interpolaci matic použít Slerp místo Nlerpu. Pak už vše funguje bez artefaktů.
