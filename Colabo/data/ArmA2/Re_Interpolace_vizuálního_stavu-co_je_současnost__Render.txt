﻿>  Přikládám [test case pro střelbu][Attachment1] . V režimu interpolace full je třeba střílet výrazně dopředu, jinak minete. Se zapnutými diagnostikami je "Future" pozice vidět jako zelená koule.

Na test case si ujasňuji, v čem nemám moc jasno: mluvím o Render / Future, ale různé objekty mají Render / Future různě. Střela např. má render = future, ale vojáci ne. Současný přístup, který je založen na tom, že se před testováním kolizí vždy dotážu na věk VS pomocí `GetFutureVisualStateAge()` nebo `GetRenderVisualStateAge()`, výsledek není pro ostatní objekty použitelný. Abychom mohli uvažovat správně interakce mezi objekty, je nutno si ujasnit jejich vzájemné vztahy. Dosavadní přístup předpokládá, že vhodný vztažný bod je Future, nicméně daleko lepší vztažný bod je "Render" (tj. to, co hráč vidí). Věk je tedy v takových funkcích třeba udávat nikoliv vůči Future, ale vůči render.

Vztah mezi dvěma různými objekty pak můžeme počítat následovně:

Věk Future oproti Render je `_simulationSkipped-_timeOffset` (typicky záporný - Future je novější než render).

Když tedy chci testovat kolizi dvou objektů (this a with) tak, aby oba byli v render, 


[ondra 11.7.2011 17:04:56]:
 Tak tohle mám, byla to překvapivě malá změna a funguje překvapivě dobře. Ještě si to zkontroluji a víc otestuji a pak bych to snad mohl zveřejnit jako default. Pak bude zbývat vychytat případné problémy, budou-li jaké, a tím by měla být interpolace zcela uzavřena.
 

[ondra 14.7.2011 14:08:38]:
 Default už to je. Chci ještě dořešit nějaké věci kolem kolizí, to napíšu zvlášť.
 

[Attachment1]: data/ArmA2/test.Desert_E.zip