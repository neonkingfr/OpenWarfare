﻿Co se děje v Entity
===

V `EntityType` začíná animace.
* Dědí z `AnimatedType`, který jen říká, že má implementovat `CreateAnimationSource(AnimationType*, string)`.
  * To je funkce, která dostane **název kontroleru** a pokud ho pozná, registruje podle něj kontroler (Pokud ne, pošle ho do předka - tak to funguje od listů dál).
    To dělá (ve výsledku) předáním adresy jedné ze svých memberfunkcí do `CreateAnimationSource(AnimatedMemberFunc0*)`.
    Nakonec se z kontroleru vytvoří `AnimationSourceMemberFunc` - jedna z možností `AnimationSource`. (Těch je hodně! Třeba `~Damper`, `~Time`, `~Reload`, `~TurretHatch`...)

* Druhy animací:
  `AnimationBase`-->`Animation`-->
  * `~UV`
  * `~AnimatedTexture`
  * `~Rotation`, `~RotationAxisAligned<>`
  * `~Translation`, `~TranslationAxisAligned<>`
  * `~Generic` (= rotation+translation)
  * `~Hide`
  * ...

* `AnimationSource` má:
  - jméno: dá se číst pomocí `GetName`
  - `GetPhase`: chceme-li získat hodnotu kontroleru
  - `SetPhaseWanted`: volá se ze skriptu, pokud chceme nastavit hodnotu kontroleru (většinou to nejde :lol:)
  - `GetStateIndex`: pokud je kontroler v `_animationStates`, tohle je jeho index (jinak -1)
  - cpp`bool Simulate(Animated* obj, float deltaT)`: pokud kontroler není součástí nějakého nadobjektu (s vlastním `Simulate`), pak se tímhle ošvindluje vnitřní změna kontroleru
  - cpp`Matrix4 GetAnimMatrix(AnimationType* type, Animated* obj, int LOD)` - pokud kontroler nějak mění matici, pak se zjistí fáze `obj` pomocí `GetPhase` a pošle se i s aktuálním LODem do cpp`type->GetAnimMatrix`.
    Kontrolery, co mění matici, jsou:
    * `AnimationRotationType`, `AnimationRotationAxisAlignedType`
    * `AnimationTranslationType`, `AnimationTranslationAxisAlignedType`
    * `AnimationGenericType` (= rotation + translation)
    * `AnimationHideType` (schovávací kontroler??)
    `GetAnimMatrix` se používá v:
    * `AnimationSourceHolder::PrepareShapeDrawMatrices`
    * `Entity::AnimateBoneMatrix`

* `AnimationSourc`y se udržují v "poli" cpp`AnimationSourceHolder _animSources` uvnitř `Entity`.
  - Toto pole se zaplní voláními `AnimationSourceHolder::Load` z `AnimationHolder`ů (`AnimationType::GetSource()`). Ty se čtou z `LODShape._animations` (který se čte z configu).
  - `Shape` je uložený už v `Object`u jako `Ref<LODShapeWithShadow> _shape`. Obsahuje veškerou geometrii a spoustu dalších zvěrstev.

* `Entity` dědí z `Animated` (který sám o sobě nic neumí). To proto, aby se ve všech animačních funkcích objevoval `Animated` a ne `Entity`. Taky se tím řeší animované věci, co nepotřebují složitost `Entity` (třeba `Weapon`).

**Co se děje v `Entity::Animate`
* Animují se tam **textury** (`AnimationAnimatedTexture.AnimateTexture`). S tím asi nic nenadělám, bitmapy se přehazují diskrétně. (Pozor, to se liší od `AnimateUV`, které asi budou třeba).
* [swskin]:
 **Software skinning** (aplikace matic z `parentObject->PrepareShapeDrawMatrices`). Musím se podívat na `AnimationRT::ApplyMatrices`.
* Pokud je nastaven `EntityType::_damageInfo`, aplikuje se stupeň zničení (vypadá to, že je 0,1 nebo 2). Protože je diskrétní, nebude nutné nic interpolovat.
* Volá se `Object::Animate`

**Co se děje v `Object::Animate`
* Animuje se tam **destrukce** (stejným způsobem jako [software skinning][swskin]).
* Objekty, co mají být přilepené na zem, se na ni přilepí (všem vertexům se upraví souřadnice /y/ podle nějakých flagů).
