﻿> ...

*(diskuse o privátních příspěvcích nemůže být privátní, jinak si ji moc neužijem, příspěvek časem přesunem kam patří)*

Donalde, slezte z toho lustru, vidím vás!

*Ač private, vidím to*

_____
**jirka** (1.4.2010 14:37:54)

Už jí nevidí. Já teď ano, protože jsem admin.

_____
**budul** (1.4.2010 16:51:03)

jestli tedy nejsem take admin, tak to stale vidim (build 68390)

_____
**jirka** (2.4.2010 7:05:50)

Tady nezáleží na číslu buildu, ale na okamžiku spuštění Colaba. Při spuštění se načtou přístupová práva a ta se pak neobnovují.
Je to pro nás varování do budoucna, že se změny přístupových práv neprojeví okamžitě. Pokud to pokládáme za závažný problém, možná bychom měli dělat jejich update před každou synchronizací. Finálně by se ale stejně vyhodnocení přístupových práv mělo dělat na serveru.

