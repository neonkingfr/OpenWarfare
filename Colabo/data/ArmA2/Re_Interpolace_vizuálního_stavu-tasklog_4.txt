﻿Interpolace vizuálního stavu - //tasklog 2
%%%%%
[5.8.] [10.8.] [16.8.] [1.9.] [6.9.] [8.9.] [15.9.] [16.9.] [20.9.]
Pokračování v [tasklogu 3](data/ArmA2/Re_Interpolace_vizuálního_stavu-tasklog_5.txt?server=dev.bistudio.com&database=colabo&id=1376).

%%%%%

[20.9.]:
\20. září
---
* Hra běží! O_O
  Testuji, co se dá - podle rady od frenkeeho hlavně herní mise.
  Zatím jsem nenašel nic zlého :))), ale změn je na tisíce, takže se asi jenom málo snažím.

[16.9.]:
\16. září **(73363)
---
* Alokace je hotová. Všechny konstruktory objektů dostaly a`bool allocateVisualState`.
  Hierarchie (úrovně alokující vizuální stav jsou tučně):
 - **Object** (ObjectVisualState) --> **ObjectTyped** (EntityVisualState) --> Entity --> EntityAI --> EntityAIFull --> **Transport** (TransportVisualState) -->
  - **Parachute
  - PlaneOrHeli --> **Airplane, Helicopter
  - TankOrCar --> **Tank, Car, Motorcycle, Ship
 Věci, které jsou v hierarchii hlouběji (např. |HelicopterAuto| nebo |TankWithAI|), nemají vlastní vizuální stav a bublat nepotřebují.
* Zrušil jsem dočasné řešení, kde se alokovalo uvnitř a`XxxType::CreateObject()`.

[15.9.]:
\15. září **(73334)
---
* a`NewVehicle` a jeho odrůdy jsou hotové a t`const`-korektní. Končí v a`EntityType`.
* **Pracuji na** probublávání boolů mezi konstruktory.
 - Při kontrole |NewVehicle| jsem zjistil, kde všude se objekty vytvářejí, a není to hezké (přes typ, nějaké proxiny, ze sítě, při serializaci) – v konstruktoru samotných objektů to bude nejlepší.
 - a`allocateVisualState` bude tedy |true| /implicitně/.
* **TODO**: alokace, |AgeVisualStyles|, |CreateCurrentVisualState|, merge, vizuální stavy mimo základní strukturu (turret, damper, soldier...)

[8.9.]:
\8. září
---
- původně jsem alokoval vizuální stav v |XxxType::CreateObject()|, ale pak s tím musí počítat všechno, co jde vytvořit (určitě se to někde zapomene).
- hezký nápad, co nefunguje: volat z konstruktoru ||Object||u virtuální funkci |AllocateVisualState()|.
- teď to chci udělat tak, že přidám každému konstruktoru parametr `bool allocateVisualState`. Implicitně by byl |true| a do konstruktorů předků by se posílalo |true|, dokud by na nějaké úrovni hierarchie nedošlo k alokaci vizuálního stavu – další předkové by se už volali s false. (Tak se podle internetu obchází předchozí problém.)
  //Ondra po Skypu: Ano, to se mi zdá čisté a robustní, dělal bych to taky tak.

Až tohle bude, zařídím, aby se dokázal stav na začátku simulace zkopírovat do minulosti (funkce AgeVisualStyles()). Potom už bude stačit najít místa, kde se objekt připravuje na kreslení, a vytvořit v nich interpolovaný stav (z diskuze s Jirkou si myslím, že bude zatím nejlepší ho mít uložený v Objectu).

Iteruji k tomu, že do kresení půjde pouze typ a interpolovaný vizuální stav, ale snažím se to dělat "evolučně" – budu muset ještě pochopit, co se děje v Object::Draw.

[6.9.]:
\6. září
---
Analýza: **Jak a kde se budou vizuální stavy vytvářet?
- cpp`virtual ObjectVisualState* Xxx::AllocateVisualState()`.
- Při vytváření objektu se alokuje `futureVS`, a to až v konstruktoru `Object`u.
- Na začátku simulace se zavolá cpp`AgeVisualStyles()`, který zkopíruje obsah `futureVS` do `pastVS`. Při prvním volání se `pastVS` i alokuje.
- Interpolovaný stav se vytvoří pomocí cpp`CreateCurrentVisualState(float interpolationTime)`. Taky se bude alokovat.

[1.9.]:
\1. září **(73084)
---
* Změnil jsem své `&*` na `GetRef()`y.
+ **... 2.září:** Dál se prokousávám se vnitřkem cpp`NewNonAIVehicleQuiet()`, už je skoro hotová.
- Objekty se mohou kromě funkcí `NewVehicle()` alokovat i jinde (třeba |particlesource|, |animator|, |proxyinventory|).
+ Teď alokuji vizuální stav v cpp`XXXType::CreateObject()`. Není to lepší zkusit udělat rovnou v konstruktorech jednotlivých objektů?

[16.8.]:
\16. srpna **(72732)
---
Přetypování ve `FutureVisualState()` by mělo být nejsprávněji `dynamic_cast` z `Ref<ObjectVS>` na `Ref<XxxVS>`, ale volá se to každou chvíli, takže by to bylo pomalé. Konstrukce `static_cast` zase neprojde přes `Ref<>` - zatím jsem tam dal Céčkovské `&(XxxVS*)*`, než přijdu na něco hezčího.
+ Musí být vůbec `Ref<>`? Musí, aby se automaticky dealokoval.
- /Ondra přes Skype/ - Řešíme obvykle takto: cpp`static_cast<XXX *>(ref.GetRef())`

Alokace vizuálních stavů:
* Analýza hotová, postupně si odškrtávám typy.
* Přidal jsem virtuální funkci cpp`EntityType::CreateObject(bool fullCreate)`. Ten bool je kvůli `ManType` --> `Soldier`.

- **Pracuji na:** alokaci vizuálních stavů v `NewNonAIVehicleQuiet`.
  - Všech 18 typů objektů z cpp`NewVehicle(const EntityAIType* type, bool fullCreate)` je hotových. Ufff :sweat:

[10.8.]:
\10. srpna **(72622)
---
Už se mi povedlo přeložit ArmU. :)
* Hotové: `_speed`, `_modelSpeed`, `_acceleration`.
* Serializuje se jen to, co dřív - část obsahu `FutureVisualState`.
+ Nic nechce `Object::Type()`. Problém byl v tom, že `Transport::Type()` byl u mě cpp`protected`, takže ho hodně externích funkcí nevidělo.
+ Hybridní stav je hotov (trošku zlobilo, že `Turret`y nebyly přístupné z vizuálního stavu: nechal jsem zatím starou verzi).
- **Pracuji na:** alokaci vizuálních stavů v `NewVehicle`.

[5.8.]:
\5. srpna **(72482)
---
S Ondrou jsme se domluvili, že se zaměřím na dokončení vizuálního stavu základní struktury `Object - Entity - Transport - *`. Turrety, dampery a další věci (`Man`) budu řešit později.
* Listová struktura je pryč, všechno teď dědí z `ObjectVS`.
* `_speed`, `_modelSpeed` a `_acceleration` už jsou skoro přepsané.
+ Co by měl dělat `Type()` u Objectu? Něco ho vyžaduje...
+ Potřebuji dořešit serializaci.
+ Nějak vymyslet hybridní stav, kdy jsou některé kontrolery "staré" a některé interpolované.
- **Pracuji na:** přepsání věcí z EntityVisualState.
