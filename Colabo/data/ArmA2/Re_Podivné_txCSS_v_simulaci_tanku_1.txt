﻿> 3. nepoužívam zjednudoušenou simulaci, a tu jemnou optimalizovat natolik, aby to nepůsobilo citelné zpomalení 

Chtěl jsem prověřit, jak si jemná simulace stojí výkonnostně, narazil jsem na velmi podivnou věc. V rámci simulace tanku se zamyká kritická sekce správce textur (scope txCSS). Je to velmi nevhodný okamžik, protože zrovna v tu dobu často probíhá renderování nebo zavádění textur na pozadí. Vidím tak opakovaně v tomto zámku strávených přes 100 ms (nejspíš je to zdůrazněno tím, že mám nyní dost slabé GPU, ale je to stejně nesprávné v principu, žádný takový zámek by neměl nastávat).

Je to ještě podivnější: čas se tráví v txCSS, ale ne v txCSL. Není to tedy v zámku, ale jen se v době, kdy zámek držíme, dělá něco, co trvá dlouho.
[ondra 1.11.2010 13:20:12]:
 Vypadá to, že to souvisí s kreslením stop od tanků:
 
 K zamykání během simulace tanku dochází v:
 
 `  >	TextBankD3D9::StoreThreadId::StoreThreadId(bank={...})	C++
  	VertexBufferD3D9::~VertexBufferD3D9()	C++
  	QIStreamBufferMapped::OnUnused()	C++
  	Ref<Location>::Free()	C++
  	LODShape::ReleaseVBuffers()	C++
  	TrackStep::Change()	C++
  	TrackDraw::Update(pos={...}, force=false)	C++`
 Jedná se o změnu modelu stopy - než se model zafinalizuje, v každém framu se mění a původní verze se zahazuje. Nejspíš by to to zahazování chtělo dělat nějak asynchronně (nebo  to celé dělat dynamičtěji, třeba použít nějaký chytrý shader - to by mě sice bavilo, ale nemyslím, že to stojí za to úsilí). Dlouho trvá nejspíš nějaké D3D volání, a to z důvodu D3D kritické sekce.

[ondra 1.11.2010 14:20:03]:
 Opraveno v 74849, buffery stop ruším až na konci framu.

[Attachment1]: data/CO_1707.Takistan_1.zip