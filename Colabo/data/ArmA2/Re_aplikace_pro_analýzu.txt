﻿> ](news:jemc0e$aj0$1@new-server.localdomain) jsem navrhl možnost zavést custom alokátor, který by sledoval a analyzoval statistiky.

K alokátoru musí být ještě jako doplněk něco, co bude umět analyzovat výstupy. Vidím různé způsoby, jak k tomu přistoupit:

Statické:

- command line utilita s textovým výstupem (v C++)

Dynamické, umožňující filtrování nebo navigaci v GUI:
- C++ utilita s GUI postaveným dle Sleepy
- .NET utilita s GUI postaveným neznámo jak
- Java utilita (GUI na bázi Swing?)

V C++ bych asi měl něco nejrychleji, protože to umím a veškeré nástroje mám nainstalované. .NET by z hlediska rychlosti dosažených výsledků asi druhý (sice moc neumím, ale nástroje mám nainstalované a hlavní nástroj je Visual Studio, na které jsem zvyklý). Java je v tomhle ohledu nejhorší, ale na druhou stranu mi připadá, že by to mohl být dobrý experiment směrem, jakým se chceme v budoucnu vydat.

Pro začátek udělám jen command line utilitu v C++, která bude umět zhruba to, co uměla dosavadní Testing configurace: bude umět vypsat několik různých druhů statistik na základě současného snapshotu.
