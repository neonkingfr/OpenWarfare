﻿> Už tahle informace se mi zdá zajímavá - volání `Update` trvá poměrně dlouho i když se nic neupdatuje. Pravděpodobně by se před každým voláním `Update` vyplatilo zjistit, jestli už working copy není v pořádku (na to by mělo stačit volání `Info`).

Tohle nepůjde, škoda. Info pracuje jen s working copy (proto je srovnatelně rychlé na všech počítačích), HEAD revision takhle nezjistím. Pokud se pokusím `Info` volat tak, aby vrátilo informace o HEAD revision, trvá srovnatelně s `Update`.

Nějaká cesta jak zjistit HEAD revision rychle ale musí existovat - https request na adresář je i na remote strojích velmi rychlý, přitom číslo revize obsahuje (nabízí se tak hack jak číslo revize získat, ale raději bych našel standardnější postup).

P.S.: úloha je vlastně o trochu složitější, nestačí mi HEAD revision, potřebuji revizi poslední změny

_____
**jirka** (13.4.2010 11:09:32)

V rámci SharpSVN se mi nepodařilo najít prostředky, jak operace urychlit (aspoň v případě, kdy se nic nemusí dělat).

Poslední pokus jsem přidal do benchmarku (dělat update více prvků jedním voláním SharpSVN, v revizi 69019), ale z lokálních testů se mi zdá, že to nepomohlo. (prosím o jeden externí test pro potvrzení)

_____
**jirka** (13.4.2010 11:49:07)

Ondrův test potvrdil, že agregace `Update` operací nic nepřinese.

Možný další postup:

- zkrácení doby jednotlivých operací (obejití SharpSVN)
    - [C API](http://subversion.apache.org/docs/)
    - [WebDAV](http://svn.apache.org/repos/asf/subversion/trunk/notes/http-and-webdav/webdav-usage.html)
- zmenšení počtu operací
    - working copy nevytvářet 1:1 dle repository, ale s plošší strukturou
    - nedělat vždy `Update` operace od rootu working copy, ale do nadřazených adresářů jít jen v případě selhání updatu listové položky

_____
**maruk** (13.4.2010 11:49:54)

<pre>
---------------------------
Colabo
---------------------------
Timing:
  Working directory found in - 2,92E-05 s
  Directory 424887b8-adad-11dd-af15-83119a8b9cf6
    Uri found in - 0,0007203 s
    GetInfo - 0,0079666 s
    Info - 0,0008699 s
    Remote Info - 2,0706779 s
    Log - 1,1210926 s
    Update - 1,0522226 s
  Directory Colabo
    Uri found in - 0,0004384 s
    GetInfo - 0,0052269 s
    Info - 0,0041975 s
    Remote Info - 1,7733566 s
    Log - 1,1068696 s
    Update - 1,0120589 s
  Directory bugs
    Uri found in - 0,003803 s
    GetInfo - 0,000683 s
    Info - 0,0038692 s
    Remote Info - 2,0795382 s
    Log - 1,1247216 s
    Update - 1,0195348 s
  File buttonProNovyPrispevek.txt
    Uri found in - 0,0110704 s
    GetInfo - 0,0050576 s
    Info - 0,0046386 s
    Remote Info - 2,0825258 s
    Log - 1,2467697 s
    Update - 1,0244608 s
  Update of 5 items - 6,2816754 s

---------------------------
OK   
---------------------------

</pre>

_____
**maruk** (13.4.2010 11:51:20)


> zkrácení doby jednotlivých operací (obejití SharpSVN) 

Nebylo by rozumne prozkoumat Tortoise SVN, jak to vlastne dela?

Ten rozdil v rychlosti mi pripada opravdu dramaticky... 

_____
**jirka** (13.4.2010 13:16:53)

Jako první zkusím:

> nedělat vždy Update operace od rootu working copy, ale do nadřazených adresářů jít jen v případě selhání updatu listové položky 

pokud to nepomůže, inspirace v TortoiseSVN bude asi rozumné řešení (jen může být časově dost náročné).