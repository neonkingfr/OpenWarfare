﻿>   - neuvolněné vertex buffery při ukončení hry a resetu zařízení (někdo mimo Shape drží na buffer referenci)

Něco jsme vyřešilo, něco nám tam zůstává. Spouštím hru s parametrem `-cpuCount=1`, problém se mi neprojevuje po Resetu, ale až při ukončování hry. Postupně jsem umisťoval volání `CheckLRU()` na různá místa, zatím jsem došel k tomu, že se to rozbije během `AppWinDestroy`. Jirka řešil včera něco trochu podobného, došel k následujícímu (prozatím přikládám kompletní záznam Skype session, možná to časem nějak pročistím):
____
[27.5.2010 9:30:55] Jirka: v debugu CheckLRU selže už během destrukce World, při volání VRAMDiscardMetrics z ProgressRefresh

to by teorii o jiném threadu asi potvrzovalo
[27.5.2010 9:33:21] Jirka: může nějaké vertex buffery generovat:
Progress::DrawFrame nebo Landscape::Init?
[27.5.2010 9:35:19] Ondřej Španěl: Landscape::InitTo by asi mohlo. Obloha, mraky, slunce, měsíc, horizont ...
[27.5.2010 9:35:29] Ondřej Španěl: Možná i clutter
[27.5.2010 9:36:34] Jirka: možná je 
scene.GetLandscape()->Init(Pars >> "CfgWorlds" >> "DefaultWorld");
trochu moc silné, nedalo by se Landscape vyčistit nějak odlehčeněji?
[27.5.2010 9:37:06] Ondřej Španěl: Možná. Tohle bylo asi nejjednodušší.
[27.5.2010 9:39:04] Jirka: už proběhlo LoadWeather a to načítá blesky
a blesky asi budou modely, že?
[27.5.2010 9:39:34] Ondřej Španěl: Ano, jsou.
[27.5.2010 9:41:01] Jirka: stejně mi ale nedochází, co se tam může pokazit
proč by vytváření shapů v tomto okamžiku mělo něco rozbíjet
[27.5.2010 10:24:17] Jirka: zdá se, že se to rozbije v Landscape::FreeCaches() po _segCache->Clear();

ještě to opakovaně ověřím
[27.5.2010 10:24:48] Ondřej Španěl: Tam se určitě ruší shapy.
[27.5.2010 10:48:07] Jirka: teď jsem to zachytil v destruktoru LandSegment, v debugu paměť na kterou shape ukazuje obsahuje samé 0x7f817f81
[27.5.2010 10:49:00] Ondřej Španěl: Tj. Shape už je zahozený. LandSegment se na Shape odkazuje jak?
[27.5.2010 10:49:43] Jirka: to nevím, myslel jsem shape který je v LRU
[27.5.2010 10:52:56] Jirka: jinak LandSegment obsahuje _lod, v němž jsou Shape _table a Shape _roads

asi zkusím tyhle shapy destruovat explicitně a vypisovat do logu adresy (jestli se shodnou s adresou toho problematického shapu)
[27.5.2010 10:56:27] Ondřej Španěl: Jo, to zní rozumně.
[27.5.2010 11:02:41] Jirka: uvidíme co řekne logování
[27.5.2010 11:08:05] Jirka: je to on:

LandSegment destrukce:
 211.659: Lod 2: table 0x26499608, roads 0x26499790

LRU:
 -  shape 0x26499608 {_subSkeletonToSkeleton={...} _skeletonToSubSkeleton=[0x7f817f81] ...
[27.5.2010 11:08:33] Jirka: tj tenhle Shape zůstane v LRU i po své destrukci
[27.5.2010 11:08:45] Jirka: ale proč to dělá problémy až teď?
[27.5.2010 11:09:11] Ondřej Španěl: Třeba předtím už po destrukci Worldu na to LRU nikdo nikdy nesahal?
[27.5.2010 11:09:58] Ondřej Španěl: tj tenhle Shape zůstane v LRU i po své destrukciRozhodně musíme zjistit, jak tam může zůstat. Má svůj VertexBuffer?
[27.5.2010 11:10:05] Jirka: samo nám to dělalo v EngineDD9::ReleaseAllVertexBuffers() - to se předtím volalo také
[27.5.2010 11:11:35] Jirka: Dělá to hned první destrukce LandSegment, mělo by být snadné to protrasovat.
[27.5.2010 11:14:21] Jirka: jak by se měl z LRU dostat? přes EngineDD9::BufferReleased?
[27.5.2010 11:15:01] Ondřej Španěl: Jo.
[27.5.2010 11:35:37 | Upraveno 11:35:50] Jirka: zdá se mi, že při destrukci shapu se zavolalo ReleaseVBuffer, ale EngineDD9::BufferReleased k tomuto bufferu ne
[27.5.2010 11:36:46] Jirka: zkusím to raději ověřit ještě jednou, nejsem si jistý že jsem breakpointy nastavil včas (ale skoro jistě ano)
[27.5.2010 11:37:56] Jirka: aha, BufferReleased se volá až v destruktoru vertex bufferu, někdo na něj ještě mohl držet referenci mimo ten shape
[27.5.2010 11:38:11] Ondřej Španěl: Koukám, že ReleaseVBuffer nic nedělá (jen v Detach assertuje)

Aha. Už to tak vypadá.
[27.5.2010 11:38:24] Ondřej Španěl: _vbLRURequest
[27.5.2010 11:38:56] Ondřej Španěl: Nikdo jiný už to ani být nemůže.
[27.5.2010 11:38:57] Jirka: ještě ti delegáti - AddShapeOpBase
[27.5.2010 11:38:57] Jirka: O co se teď snažit? Upravit ReleaseVBuffer aby změnil ownera na NULL, nebo dohledat kdo drží tu referenci?
[27.5.2010 11:39:49] Ondřej Španěl: To je nepěkné. Pokud Shape dá pokyn k asynchronnímu zrušení VB a pak sám sebe zruší, máme tu problém.
[27.5.2010 11:41:38] Ondřej Španěl: Z _vbLRURequest můžeme odstranit dvěma způsoby:

a) nechat _vbLRURequest zpracovat (tedy volat ProcessLRURequests)
b) _vbLRURequest překopírovat do temporary listu, přitom se z něj vyjmout, pak překopírovat zpět
[27.5.2010 11:48:10] Ondřej Španěl: Jiná možnost by byla změnit, co se vlastně skladuje v _vbLRURequest (byly by tam jen nějaké LLinky na VertexBufferD3D9, nějaká speciální struktura)
[27.5.2010 11:49:45] Ondřej Španěl: a) by nejspíš Failovalo na Fail("Refreshed buffer should be already in LRU");
[27.5.2010 11:51:01] Ondřej Španěl:
> Upravit ReleaseVBuffer aby změnil ownera na NULL
To vypadá čistě.
[27.5.2010 11:51:19] Ondřej Španěl: Jen si musíme ověřit, že to pak všude umíme tak použít, ale to už se pozná rychle.
[27.5.2010 13:25:21] Jirka: to jsem udělal (+ ošetřil jsem ostatní volání GetOwner())

aplikace nyní při ukončování nepadá, ale zůstává tam viset nějaká paměť:


[ondra 29.5.2010 1:06:06]:
 Opakovaně se mi to stává při destrukci LODShape s názvem "ca\data\cl_feathers2.p3d", někdy se mi to ovšem stalo i u jiného LODShapu.
 
 Callstack:
 `
  	LODShape::DoDestruct()	C++
  	LODShape::~LODShape()	C++
  	LODShapeWithShadow::~LODShapeWithShadow()	C++
  	LODShapeWithShadow::scalar deleting destructor()	C++
  	RefCount::Destroy()	C++
  	ShapeBank::FreeOneItemLODShape()	C++
  	MHInstanceLODShape::FreeOneItem()	C++
  	MemoryFreeOnDemandHelper::Free(amount=4294967295)	C++
  	MemoryFreeOnDemandHelper::FreeAll()	C++
  	ShapeBank::ClearCache()	C++
 `
 Jakmile problém nastane, zdá se mi, že obsah _vBufferLRU je narušen tak, že položka v jednom seznamu je provázána se začátek nebo koncem jiného seznamu. Taková věc by se třeba mohla stát, kdyby se např. v nevhodnou chvíli pohnulo s FrameId, takže by nějaký VB byl zařazen k jinému stáří, než do kterého podle svého FrameId patří (s něčím podobným už jsem měl problém dřív u `FramedMemoryControlledList` seznamu textur při jejich zavádění na pozadí).
 
 Konkrétní mechanismus je mi ovšem nejasný, protože běžím s -cpuCount=1, takže thready na pozadí toho dělají naprosté minimum. Divné je mi i to, že je to poměrně spolehlivě reprodukovatelné.
 

[ondra 31.5.2010 8:21:29]:
 > kdyby se např. v nevhodnou chvíli pohnulo s FrameId, takže by nějaký VB byl zařazen k jinému stáří, než do kterého podle svého FrameId patří (s něčím podobným už jsem měl problém dřív u FramedMemoryControlledList seznamu textur při jejich zavádění na pozadí).
 
 Pro jistotu nyní frameId udřžuji lokálně v každém `FramedMemoryControlledList`. Při použití frameId z různých zdrojů je tedy třeba být opatrný, protože se někdy mohou mírně rozcházet, zejména tehdy, pokud by třeba některý seznam udržoval jiný thread  (odhaduji, že ne víc, než o jeden frame, ale úplně jist si nejsem).
 

[ondra 31.5.2010 10:15:31]:
Pomohlo to. Problém s EngineDD9 byl ten, že nebyl registrován jako FreeOnDemand, takže se rozešel stav v něm skladovaných položek s FrameId, protože nikdo nevolal EngineDD9.MemoryControlledFrame()

 Už ho zase jako FreeOnDemand registrujeme, díky zavedení lokálních FrameId jsme mohli zrušit zamykání CS file serveru v MemHeap::FreeOnDemandFrame.