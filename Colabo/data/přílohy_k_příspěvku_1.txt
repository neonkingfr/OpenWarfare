﻿1) Proč nejde přetáhnout do postu víc příloh najednou? Použije se jen první.

2) Mají nějaký význam ty dva čtverečky kolem "tagu" s přílohou? (
 ![sbalene\_vlakno.jpg][Attachment5] 
)

3) Když přidám přílohu, už ji nemůžu odstranit (před odesláním). Při pokusech jsem přidal jeden soubor třikrát. V článku jsem ho nechal pak jen jednou, vpravo nahoře v seznamu zůstal víckrát, ačkoli šlo o stejný soubor. Díval jsem se do dat a commitnul se 3x. Tím prosím o možnost odstranit přílohy, alespoň z neodeslaného článku.

4) Test: K tomuto příspěvku připojuji soubor, který v článku neodkazuji a před odesláním ho hned teď mažu z disku.


[dmusil 19.4.2010 16:25:36]:
 Ad 4) Chvíli (20s) se zastavil na Sending attachments. Pak odesílání článku dokončil. Článek v Colabu vypadá normálně, do SVN uložil přílohu nulové velikosti.

[Attachment1]: data/x_3.jpg