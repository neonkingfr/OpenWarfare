﻿Lineární diskuse určitě nejsou špatné. Dokonce je docela běžné, že se ve fórech dá libovolně přepínat mezi lineární a stromovou podobou té samé diskuse.

Obávám se, že momentálně trpí Colabo rozpolceností. V newsgrupě jsme se přeci jen snažili mít rozdělené např. diskuse a announce. V colabu je to tak nějak všechno smíchané, což nemusí být vždy ku prospěchu.

Každopádně, lineární diskuse (případně v budoucnu i vedené jako online konference přes IM) u článků mi nevadí, vadí mi jejich integrování s vlastním původním příspěvkem.


>přes Reply - a jestli budeš odpovídat ty, odpověz také tak, ať vidíme, jak to pak vypadá (nebo si můžeme najít nějaký příklad ze života v NR a třeba si ho sem i naimportovat).
> Kromě Append note jsem měl ještě jeden návrh, který si také myslím, že by nezávisle na tom mohlo být zajímavé vyzkoušet: [Re: Lineární diskuse - preview více zpráv](data/e9e7e287-cd5c-43cf-a670-727c4b6976f0.txt?server=dev.bistudio.com&database=colabo&id=313)
