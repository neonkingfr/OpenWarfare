﻿> - blendování se zahajuje a ukončuje centrálně, v místě, kde se prochází všechny SortObject za účelem pravidelné udržby

Mělo to své mouchy, takže jsem to znovu přepracovával. Ani teď se mi to nelíbí. 

Načatá práce na reversování blendů:

https://dev.bistudio.com/svn/pgm/branches/Poseidon/Arrowhead-BlendFix/

Hlavně se mi nelíbí, že ModifyObject pracuje se stavem ve chvíli, kdy už kreslení proběhlo, což vyžaduje pečlivě vážit, co dát do LoadedLevels a co do ModifyObject. Proč to tak je, to nevidím. Zkusím to sepsat.

#. `MergeInstances.Start` volá `LoadedLevels`. Pro zavedené lody se potom zjišťuje, zda jsou instancovatelné (`obj->CanBeInstanced`).
#. `ModifyObject` modifikuje `SortObject`
#. další objekty, které mohou být společně instancovány (ověřuje se pomocí `oiBeg->CanBeInstancedTogether`), se zpracují v MergeInstances.Add
#. jakmile se narazí na objekt, který už společně instancovat nejde, sebrané instance se nakreslí voláním `ObjDrawInstanced`, pro nový objekt se začíná znovu od začátku

Problémy a nejasnosti:

- `CanBeInstancedTogether` dostává na jedné straně informaci, která už prošla `LoadedLevels`/`ModifyObject`, ale na druhé straně data vstupují přímo ze správce scény. Může se tedy stát, že u nějakého objektu jsou domněle srcLOD/dstLOD stejné, ale ve skutečnosti už nejsou
- i bez instacování se mi na začátku blendování stává, že mi tam na chvíli problikne cílový LOD


[ondra 24.11.2010 15:06:40]:
 LoadedLevels/ModifyObject jsem sloučil do LoadedLevels.
 
 > mi tam na chvíli problikne cílový LOD
 
 Tohle už se mi tam neděje.
 
 (mergováno v 76047)
 
 Celé už se mi to líbí, jen následující výhrada trvá:
 
 > CanBeInstancedTogether dostává na jedné straně informaci, která už prošla LoadedLevels/ModifyObject, ale na druhé straně data vstupují přímo ze správce scény. Může se tedy stát, že u nějakého objektu jsou domněle srcLOD/dstLOD stejné, ale ve skutečnosti už nejsou
 
 Myslím, že by to mělo jít vyřešit geniálně jednoduše: CanBeInstancedTogether by mělo být možné provést už jen na základě vstupních informací. Pokud dodatečně zjistím, že to vůbec instancovat nejde, nebo že není zaveden příslušný lod, nakreslit to každé zvlášť vždycky ještě můžu.
 
 
 

[ondra 24.11.2010 16:42:06]:
 > CanBeInstancedTogether dostává na jedné straně informaci, která už prošla LoadedLevels/ModifyObject, ale na druhé straně data vstupují přímo ze správce scény. 
 
 Výhrada je pravdivá jen částečně. Co se hlavního lodu týče, zpracovává se přímo drawLOD / shadowLOD ze SortObject, který se v LoadedLevels nemění. Jediné, co se mohlo změnit, je srcLOD / dstLOD.
 
 Snažím se přijít na to, v jaké konrétně situaci by mohlo dojít ke zmatení. Nejspíš tehdy, když jsem se rozhodl instancovat, ale blendování přitom začíná nebo končí (obracení směru blendování nevadí, pořadí src/lod se při něm zachovává).
 
 Pokud ovšem začínám blendovat, tak je to proto, že drawLOD se od srcLOD i dstLOD liší. Příklad:
 
 src/dst/drawLOD
 
 A: 1/1/1
 B: 1/1/2 -> 1/2/2
 C: 1/2/2 -> 1/2/2
 D: 1/1/2 -> 1/2/2
 
 Není tedy možné, aby se B blendovalo spolu s A. Naopak se B může (v závislosti na to, v jakém pořadí A/B/C/D procházíme) blendovat s C nebo s D, ale to se mi zdá v pořádku, protože B i D mají stejné zadání, takže bude i stejný výsledek.
 
 Zatím tedy zůstávám na tom, že pokud se to jako vizuální problém nepotvrdí v praxi (projevilo se to probliknutím lodu u nějakého instancovaného stromu v okamžiku, kdy se něco začíná nebo končí blendovat), nechám to už být.
