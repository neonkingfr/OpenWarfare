﻿> Současné řešení používá streamwriter s kódováním utf8, které přidá ten prefix všude. Mám za to, že ty tři bajty utf8 prefixu nevadí ani u zdrojáků, nebo se pletu?

U C++ zdrojáků pro Visual Studio nevadí. U většiny našich konfigů a podobných věcí už snad taky nevadí. Umím si ale představit, že někdo bude používat i nějakou utilitu, která očekává obyčejné ASCII. Přidáná UTF-8 prefixu mu pak bude způsobovat podivné chyby, se kterými si možná nebude vědět rady, protože přidání UTF-8 nebude ani vidět jako změna v difu.
