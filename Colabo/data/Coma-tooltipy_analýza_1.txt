﻿Tooltip se bude psát do `"uvozovek"` **těsně** za to, k čemu patří:
- `akronym"tooltip"` --> <span title="tooltip">akronym</span>
- `[dlouhý akronym]"dlouhý tooltip"` --> <span title="dlouhý tooltip">dlouhý akronym</span>
- `[Seznam][www.seznam.cz]"Najdu tam, co neznám!"` --> <a title="Najdu tam, co neznám!" href="http://www.senzam.cz">Seznam</a>

Uvozovky se musí do konce řádku ukončit.
___
Tooltipy nemohou obsahovat HTML tagy.
HTML entity (jako třeba `&#34;` --> |&#34;|) jsou OK, odřádkování také.

V %Comatu% nepůjde v tooltipu odřádkovat (maximálně entitou `&#10;`).
___
Speciální znaky se v odkazech musí převést do procentového UTF-8 kódování (třeba znak eura € má kód `U+20AC`, což je v UTF-8 `0xE2 0x82 0xAC`, takže `%E2%82%AC`). Mezery jsou třeba `%20`.
___
Nápad:
- Odkazy s jednoslovným textem by taky mohly jít psát `takhle(URL)` nebo `takhle[URL]`, aby to bylo konzistentní s `tímhle"tooltip"`.
  //Ondra: Mělo by tak fungovat buď všechno, nebo nic.
- Vyzkouším, když to bude zmatené, zas to zruším. Obsah závorek samozřejmě musí být platný odkaz, jinak se vypíšou normálně (třeba ||f(x)|| nebo ||array[index-1]||) - tak to funguje už teď.
