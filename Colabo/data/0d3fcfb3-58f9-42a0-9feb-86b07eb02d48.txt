﻿> ## Tvůrce místo posledního editora
> 
> Možná bychom v Posted by vůbec neměli zobrazovat toho, kdo udělat poslední změnu, ale naopak toho, kdo článek založil?

Čím víc nad tím přemýšlím, tím víc se mi tohle líbí. Myslím, že to je minimálně dává mnohem lepší odhad toho, kdo napsal většinu článku, než současné autorství poslední změny. Pokud autorství poslední změny k něčemu potřebujeme, navrhoval bych na to zavést nový sloupeček.
