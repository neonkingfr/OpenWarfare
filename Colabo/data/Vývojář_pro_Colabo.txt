﻿Chci najít full time vývojáře, který se bude starat o vývoj i podporu uživatelů Colaba.

Prosím o pomoc přípravou a výběrem uchazeče.


Požadujeme:
==
* znalost českého a anglického jazyka
* znalost programování v .NET
* pracoviště Mníšek pod Brdy (podle mě by to bylo nejvhodnější)

Co dál?
[jirka 22.1.2011 9:50:59]:
 > znalost programování v .NET
 
 Asi bychom chtěli i přímo znalost C#.
 
 Dále by se hodila znalost Javy a základů psaní servletů (údržba a rozvoj Colabo serveru).
 
 Bylo by dobré, aby buď měl s uvedenými jazyky určitou praxi, nebo byl schopen rychlé adaptace.
