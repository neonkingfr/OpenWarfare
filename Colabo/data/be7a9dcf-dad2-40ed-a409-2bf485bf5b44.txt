﻿> V současné době se access-rights chápe jako dva tagy, access a -rights. To se mi zdá nesprávné, mělo by se to chápat jako jedno slovo. Speciální modifikátory (.,+,- aj.) bych speciálně chápal pouze na začátku, jinde bych je normálně akceptoval jako součást znaku.

Musíme konečně vyřešit syntax tagů – je hloupé ji pořád měnit. Udělám návrh.

> Rozhodně nepovažuji za přijatelné, aby tagy, které nejsou řádně oddělené, mi to v tichosti zpracovalo jako dva tagy. access-rights je tedy buď chyba, nebo jeden tag, ale rozhodně ne dva tagy.

To bylo přehlédnutí, už jsem to opravil (teď to zakřičí – musí se to uzavřít do uvozovek).
> > "...Now they have two problems."
> 
> Takže se zdá, že citát měl pravdu?

No jo ;-) Ale líp by to šlo udělat už jen tak, že by se udělala extra gramatika pro seznam tagů.
