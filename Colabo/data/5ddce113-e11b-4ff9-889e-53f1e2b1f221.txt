﻿Chtěl bych uzavřít chybu, ale zatím nevím jak. Potřebujeme trochu domyslet, jak zacházet s tagy a jak příspěvky filtrovat.

Filtrování příspěvků
--------------------
Nejspíše brzy nevystačíme jen s navigací pomocí cloudu tagů, ale budeme chtít nějaké pokročilejší filtrování (obdobu Filter a Highlight).

Mohlo by to fungovat tak, že se budou definovat určité pravidla. Pravidlem bude dvojice: výraz používající tagy, akce. Akce budou např. schovej příspěvek, obarvi titulek na určitou barvu apod.

Pravidla by se sdružovala do nějakých setů, sety ukládaly do profilu nebo samostatných souborů a aktuální set by se vybíral z UI.

Uzavírání podstromů
-------------------
Ještě mi není jasné, jak by fungovalo filtrování příspěvků z pohledu stromu odpovědí. Řešení v newsreaderu se nám asi nikomu nelíbí a chtěli bychom uzavírat jednotlivé podstromy příspěvků.

Jako nejpřirozenější (a tak to myslím funguje teď) se mi jeví, že jakmile je ve stromu uzel, který nesplňuje pravidla pro zobrazení, nezobrazí se celý podstrom jehož je kořenem. V této koncepci by ale uzavírání bylo trochu nepříjemné z pohledu UI - odpověděl bych na příspěvek a pak bych původnímu příspěvku změnil tagy.
