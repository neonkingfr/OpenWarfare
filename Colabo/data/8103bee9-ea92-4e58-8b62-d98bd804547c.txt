﻿> > Co v tomto kontextu znamená Mark as unread? Nemělo by nám to naopak ukázat změny od poslední přechozí revize?
> 
> Mark as unread interpretuji tak, jako bych článek ještě nikdy nečetl. Změnou je tedy celý článek a nemá cenu nic zvýrazňovat.

Aha. Popíšu tedy svojí motivaci, třeba tě něco napadne.

V současné době běkdy zmáčknu M proto, abych viděl, jak příspěvek doopravdy vypadá, protože mi se zvýrazněnými změnami moc nedává smysl - srov. [Re: Zvýraznění posledních změn - změny struktury](data/c63c5eae-1b66-4070-9322-c1f36470d346.txt) . Tím ale bohužel ztratím ty zvýrazněné změny, takže už znovu nezjistím, co se změnilo.

Co třeba:

- při Mark as Read si klient (jen v paměti, v rámi session) zapamatuje, jaká byla ta poslední revize, kterou jsem nečetl, pokud zas dám Mark as Unread, vrátí to do přechozího stavu (a ukáže tedy znovu stejné změny)
- nebo dát jinou možnost, jak změny dočasně schovat

Jestli ovšem zvýraznění změn bude fungovat rozumně, pak nejspíš vůbec není třeba to řešit.
