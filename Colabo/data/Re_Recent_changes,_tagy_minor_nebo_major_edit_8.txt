﻿Co by mělo být default major edit:

* nový příspěvek (přes new anebo reply)


Jinak se mi zdá chování dobré.


> > Kritérium 50 znaků nemusíme dodržet nijak přesně, ale aspoň nějaká heuristika, která by se mu blížila, by se mi zdála vhodná.

> -----
> ***maruk** (11.2.2010 12:27:57)*: Uživatel tam uvidí, zda je to minor edit nebo ne, možná stačí něčím jednoduchým začít a pak systém vyladit za provozu


-----
***ondra** (11.2.2010 14:41:05)*: Myslím, že u New / Reply by snad ani to zaškrtávátko nemělo být. To je asi Major vždycky, ne?

-----
***maruk** (11.2.2010 14:47:08)*: Klidně, svou logiku to má. Tj. asi by to tak bylo lepší.

-----
***jirka** (12.2.2010 8:45:02)*: Souhlasím, hotovo v 64840.
