﻿> Primárně bych to čekal přes "drag a drop" v seznamu příspěvků.

Jak přesně by to mělo fungovat? Jak bys přesunul příspěvek do rootu (aby neměl rodiče)?

-----
***maruk** (6.1.2010 12:40:31)*: * Uchopím příspěvěk, drzim, nesu ho, kdyz ho pustim na jinem prispevku, stane se jeho potomkem.

* Uchopím příspěvěk, drzim, nesu ho, upustim ho na sedive plose pod poslednim prispevkem, dostane se na root.

-----
***ondra** (6.1.2010 12:55:25)*: Typicky mám příspěvků tolik, že pod nimi žádná šedivá plocha není. Co tedy takhle: nesu ho a hodím ho někam do okna mimo názvy příspěvků:

tj.

- do šedivé plochy pod nimi
- do prostoru pro tagy
- na titulky od sloupců
- do prostoru toolbaru

Asi bych nedovolil hození do preview, nebo pokud ano, tak by mělo znamenat totéž, jako hození na titulek příspěvku, který je zrovna zobrazený.


-----
***jirka** (6.1.2010 13:25:07)*: 

> - do šedivé plochy pod nimi 
> - na titulky od sloupců 

Tohle by určitě šlo, nejsem si jistý s jinými controly (mimo DataGridView), zatím moc nevím jak přesně tu drag & drop funguje.
