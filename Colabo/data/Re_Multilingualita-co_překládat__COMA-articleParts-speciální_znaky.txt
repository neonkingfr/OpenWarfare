﻿>  To, co posíláme to Google Translate, by také tak mělo vypadat: obsah proložený něčím, co služba při překladu zachová nedotčené.

Zkoušel jsem různé varianty:
 
`Test: <t n="1"/>odkaz<t n="2"/><t n="3"/> v rámci řádku, <t n="4"/>Potom Bold<t n="5"/>.`
 
`Test: <t n="1"/> link <t n="2"/><t n="3"/> the line <t n="4"/> Then Bold <t n="5"/> .`
 
Jeden rozdíl oproti zdroji jsou mezery kolem tagů, ale ty by neměly vadit. Horší je, že "v rámci řádku" bylo přeloženo jako "the line" místo "within the line", lepší varianty, kterou jsme viděli při překladu COMA varianty.

Zkoušel jsem ještě různé jiné varianty


`Test: <t/>odkaz<t/><t/> v rámci řádku, <t/>Potom Bold<t/>.`
->`Test: <t/> link <t/><t/> the line <t/> Then Bold <t/> .`

`Test: *odkaz** v rámci řádku, *Potom Bold*.`
->`Test: * link ** within the line, * then * Bold.`
 

`Test: <>odkaz<><> v rámci řádku, <>Potom Bold<>.`
->`Test: &lt;&gt; link &lt;&gt; &lt;&gt; in the line &lt;&gt; Then Bold &lt;&gt;.`

`Test: §odkaz§§ v rámci řádku, §Potom Bold§.`
->`Test: § § § link in the row, then § § Bold.`

`Test: %odkaz%% v rámci řádku, %Potom Bold%.`
->`` (už na vstupu servletu, Možná by se vyřešilo předávání mimo URL)

`Test: &odkaz&& v rámci řádku, &Potom Bold&.`
->`Test: ` (už na vstupu servletu, Možná by se vyřešilo předávání mimo URL)

Nejlépe dopadlo nahrazení tagů hvězdičkami nebo `<>` sekvencemi. Pro obě varianty ještě dělám kontrolní překlad, kdy se bude dotyčný znak též vyskytovat v normálním textu:

`Test: *odkaz** v rámci řádku, následuje hvězdička: * *Potom Bold*.`
->`Test: * link ** within the line, followed by an asterisk: ** Then * Bold.`

`Test: <>odkaz<><> v rámci řádku, následuje template: <> <>Potom Bold<>.`
->`Test: &lt;&gt; link &lt;&gt; &lt;&gt; in the line, followed by template &lt;&gt; &lt;&gt; Then Bold &lt;&gt;.`

Text k překladu získám tak, že budu parsovat zdroj, formátovací informaci nahradím speciálními znaky a ke každému z nich si zapamatuji skutečný obsah. Pokud narazím na speciální znak, zapamatuji si, že reprezentuje sám sebe.

Oboje dopadlo podle mého dobře, stačí si toho znaku při zpracování textu všimnou a zapamatovat si, že reprezentuje sám sebe.

Docela by se mohlo hodit překládat HTML odstavec připravený k výstupu, nicméně nic takového neexistuje, articleParts se nejprve slepí a pak se v nich nahrazují Markery až v celku (viz `Coma.ComaParser.Postprocess` část `// Convert to string`). Pokud bych překládal ještě articleParts, musel bych identifikovat i markery a také je nahradit speciálními znaky.

Jiná možnost je vzít celé HTML, které mám na konci funkce `Coma.ComaParser`, rozdělit si ho na odstavce. To se mi také nezdá těžké, odstavce jsou jednak uzavřené v `<p> </p>` tagách, a dále oddělené `<div>` tagy:

` <p>Co vlastn&#283; chci:</p>
 <div style="font-size:0.5em">&nbsp;</div>`

Při překladu HTML se nabízí použít jako speciální znak `<>`, pak by stačilo vypustit (a zapamatovat) vnitřek každého tagu - příjemné je, je na `<>` nemohu narazit nikde uvnitř tagu ani mimo tag, protože tam by muselo být kódované jako `&lt;&gt;` - ale naopak nepříjemné je, že z překladače to vyleze ne jako `<>`, ale jako `&lt;&gt;`, takže bych to musel nějak konvertovat a dávat si pozor i na `&lt;&gt;` na vstupu. Zdá se mi tedy robustnější pracovat s hvězdičkami, které překladem prolezou, zdá se, bez problémů.

 >Pokud bych překládal ještě articleParts, musel bych identifikovat i markery a také je nahradit speciálními znaky.

Tohle jsem zkusil, vcelku to vypadá přijatelně, ale je tam jeden problém: některé markery zastupují odkazy, a to včetně jména. Pokud jména odkazů nepřeložím nějak zvlášť, budou v textu "trčet" v původním jazyce. Myslím ale, že překládat názvy odkazů až v Postprocess, kde se nahrazují, by snad nemělo být těžké.



[ondra 5.7.2012 21:45:41]:
 Rámcově mi to už funguje. Tu a tam narazím na rozbité odkazy, ale poměrně málo a doufám, že i ty případy, které vidím, budu umět opravit.
 
 Hlavním problém z hlediska nasazení vidím v tom, že to dost pomalé, a to i když to nejde přes Google server (když to přes něj jde, je to pomalé tuplem). Tím, že se ten text rozbíjí na kousíčky a ptám se na ně synchronně, běží to hodně pomalu (Pokud má dokument 30 částí, tak při pingu 20 ms dělá celkové čekání minimálně 600 ms). Asi budu muset zavést nějaké cachování ještě na straně Colaba, a též by stálo za úvahu překlad dělat nějak asynchronně nebo pro víc částí paralelně.
 
[ondra 11/07/2012 15:43:05]:
 > Zdá se mi tedy robustnější pracovat s hvězdičkami, které překladem prolezou, zdá se, bez problémů.
 
 Při delším testování se `<>` přeci jen zdá být lepší. Příklad (z [Re: Multilingualita - Google translate - placené API](data/Re_Multilingualita-Google_translate-placené_API.txt?server=https%3A%2F%2Fdev.bistudio.com%2FColabo%2Fresources%2F&database=&id=1724)):
 
 `Existuje *, které by tuto otázku vyřešilo, přístup na něj je přes https REST službu na *.`
 -> `* There, that should solve this problem, access to it is via https to REST service *.`
 
 `Existuje <>, které by tuto otázku vyřešilo, přístup na něj je přes https REST službu na <>.`
  -> `There is a &lt;&gt;, which should solve this problem, access to it is a REST service via https to &lt;&gt;.`
 
 > dávat si pozor i na `&lt;&gt;` na vstupu

 Tohle je nakonec menší problém, než se zdálo. Stačí `&lt;&gt;` na vstupu taky "vyhvězdičkovat".

[ondra 12.7.2012 11:09:14]:
 > Tohle je nakonec menší problém, než se zdálo. Stačí &lt;&gt; na vstupu taky „vyhvězdičkovat“.
 
 Začal jsem to implementovat a zjistil jsem, že to přeci jen je takový problém, jak se zdálo. Problém je v tom, že kolem překladu převádím mezi HTML a UTF-8 kódováním. Normální postup je:
 
 - vyhvězdičkovat tagy a jiné zvláštní sekvence
 - konverze z HTML do UTF8
 - vlastní překlad
 - konverze z UTF8 do HTML
 - nahradit hvězdičky původním obsahem
 
 Problém je v tom, že na rozdíl od * `<>` konverze z / do HTML nezachovává. Z Google překladače navíc `<>` dostávám v podobě `&lt;&gt;`.
 
 Asi tedy budu muset ještě přidat další krok, kdy místo `<>` použiji nějakou pracovní sekvenci (třeba právě *) a tu nahradím za `<>` jen během vlastního překladu. `<>` už vyhvězdičkované určitě je (spolu s HTML tagy), entity nezkonvertované do UTF8 hvězdičkuji také.
 
 Ještě mě napadá další obdobná situace pro * - co když je na vstupu &#42; tedy HTML reprezentace *?
 
 
