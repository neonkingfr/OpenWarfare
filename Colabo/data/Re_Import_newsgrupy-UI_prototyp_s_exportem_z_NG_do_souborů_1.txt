﻿Mám zprovozněný export z NG, zatím do souborů i s přílohama. Aby se mi lépe pracovalo, udělám COMMIT a v Colabo budete chvíli vidět položku v menu DesignTools s možností *Import from NG*. Po aktivaci se otevře NNTPReader dialog, ve kterém se můžete přihlásit k News serveru (je tam nastavena lokální IP adresa našeho news serveru, pro Ondru a outsidery, totiž outstandig..., ehhh, totiž uživatele Colabo mimo naší LAN je třeba vyplnit public adresu.

**Zatím prosím neklikejte na Save news!**

Pokud se chcete *pokochat*, tak práce s tímhle dialogem vypadá takto:

1. vyplnit adresu NNTP Serveru a stisknout *GO*
2. vyplnit username a heslo pro připojení k serveru
3. vybrat newsgrupu (nejlíp nějakou malou, třeba hned nahoře *bistudio.test*)
4. stisknout Get News (zjistí počet zpráv atd.)
5. a pomocí Next Article se dá postupně navigovat na články a zobrazovat si je
6. Save Body uloží článek do souboru i s jeho přílohama.

Nyní udělám to, že *Save News* nebude ukládat do souborů (to nýní dělá a důvod, proč to nemáte mačkat je, že to pro velké news grupy trvá hodně dlouho!), ale že bude vyrábět Colabo Articles a posílat je na server **SendArticle**.

-----
***bebul** (26.1.2010 11:26:32)*: Je to commitnutý, colabo rev, 63323