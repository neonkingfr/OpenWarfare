﻿> Rozlišoval bych následující "práva":
> 
> - co můžu číst
> - co musím číst (co se mi označuje jako "to read")
> - co můžu editovat
> - jaký druh obsahu smím vytvářet (kam můžu zapisovat)


Asi by nebylo od věci v tomto dát uživateli i trochu volnosti (a to tak, že se dobrovolně může některých oprávnění vzdát). Např. vím, že nějaký tag mě prostě vůbec nezajímá, anebo bych sice měl práva k němu psát, ale nechci a raději si to vypnu, protože vím, že k dané věci přispívat nechci.