﻿> Našel jsem certifikát z této doby (GfW) i baťák, který podepisoval cat soubor.
> Zkusím ověřit proces podepisování exe a pak se můžem domluvit na dalších krocích.

Tak jednoduché to nebylo, certifikát podepisující cat byl náš vlastní. Vedle byl ale i ten od VeriSign a v historii SVN se mi podařilo najít command line na podepisování včetně hesla.

Špatná zpráva je, že platnost certifikátu vypršela 4.2.2010 (zřejmě se vydává jen na rok).
_____
**ondra** (7.4.2010 9:27:46)

Hlaváč má nový (platný) certifikát. Viz news:hgtdjq$a0e$1@new-server.localdomain


_____
**jirka** (8.4.2010 11:49:09)

Už mám nový certifikát a ověřil jsem že jsme schopni ho používat.

Teď už stačí rozhodnout kdy a co všechno podepisovat.