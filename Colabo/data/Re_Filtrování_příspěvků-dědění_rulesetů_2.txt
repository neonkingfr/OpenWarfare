﻿> ***ondra** (1.2.2010 15:51:04)*: Přestalo mě to bavit, protože mi při editaci Combo boxů stávkovalo Visual Studiu (a to doslova - jak uvidělo Combo box, přestalo reagovat).

Brzy jsem zjistil, že dědění je u takhle komplikovaných rulesetů už pak obtížné nějak rozumně využít. Chtěl jsem si přidal pravidlo pro /todo příspěvky, ale není to tak snadné, jak jsem myslel. Snadno toho dosáhnu tak, že vezmu default rule set a pravidlo do něj vložím:

none(
:to-read => BOLD;
/ignore | /close => HIDE;
(/done | /fixed) & :to-read =>COLOR:#000; 
/done | /fixed =>COLOR:#777; 
/priority & :@me => COLOR:#a00;
/todo & :@me => COLOR:#04F;
:@me => COLOR:#00a;
/priority & @* =>COLOR:#b70;  
@* => COLOR:#070;
:draft => ICON:Messages; 
:changed => ICON:Note; 
.important => ICON:Warning; 
documentation => ICON:Info;
/priority & :@me & !/done & !/fixed => ICON:Error;
/bugs & !/done & !/fixed => ICON:Repair;
sandbox & !:@me => HIDE
)

Pokud bych chci téhož dosáhnout děděním, musím si dát pozor, abych nepřebil barvu u zavřených nebo přiřazených prioritních příspěvků. Jelikož ovšem jejich definice je taky součásí default rule setu, není to nakonec moc čisté, stejně musím musím kus obsahu rule setu napodobit:


none(
!(/done | /fixed) & !(/priority & :@me) & /todo & :@me => COLOR:#04F;
)

Jediná alternativa mě napadá s využitím priorit pravidel, s tím, že v default rulesetu by byla pravidla očíslovaná postupně, takže by se člověk mohl rozhodnout, kam to nové vloží.
