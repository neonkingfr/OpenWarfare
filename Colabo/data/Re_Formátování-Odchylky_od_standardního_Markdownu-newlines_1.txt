﻿> > > Standardní markdown jedno odřádkování ignoruje, my ho budeme chtít zachovat,
> 
> tohle zatím vůbec netuším, jak udělat. Díval jsem se trochu na implementaci markdownu, ale dost jsem se v tom ztratil (je to hromada regulárních výrazů). Zatím mi připadá, že to možná nebude až tak primitivní, jak se zdálo...

Nakonec jsem ani nemusel vznášet dotaz na stackoverflow, vygoogloval jsem totiž, že implementace Markdownu v C#, kterou používáme, už existuje ve verzi *MarkdownSharp*, která volitelně zachovávat newlines umí. Povoluje se to pomocí boolovských proměnných, takže by to teoreticky šlo zapínat/vypínat i v konfiguraci.

Navíc to umí např. automaticky detekovat linky a udělat je clickable, jako např. tenhle link: http://code.google.com/p/markdownsharp/source/checkout
