﻿> 2. Při spuštění hry s pbo souborem jako parametrem (dosaženém ve finální implementaci 
> např. kliknutím na pbo soubor) se vybere akce dle obsahu a umístění pbo.
>  - pokud addon je již na správném místě v My Documents a
>     - obsahuje právě jednu misi (buď je pbo výsledkem exportu z mission editoru, nebo má 
> v configu CfgMissions s jednou položkou), spustí se tato mise
>     - neobsahuje žádnou misi a obsahuje popis právě jedné entity (public položka v 
> CfgVehicles, případně CfgWeapons), provede se akce demonstrující použití této entity 
> (nejlépe otevření armory už s vybranou entitou, v případě že by to bylo příliš náročné 
> nějaké náhradní řešení (zkonstruování a spuštění uživatelské mise))
>  - v ostatních případech se ve vhodném okamžiku (po zobrazení hlavního menu) otevře nový 
> dialog, nabízející různé volby:
>     - instalovat addon do My Documents (případně do existujícího nebo nového mod 
> adresáře)
> > spustit nějakou z obsažených misí
> > vyzkoušet nějakou z obsažených entit




Tento podúkol je ve stavu prototypu. Hotové je:
- zavedení addonu, který je zadán jako parametr (plnou cestou)
- shromáždění všech akcí které je možné vykonat
- automatická akce při správném umístění addonu a přítomnosti právě jedné mise
- core UI výběru z akcí
- instalace addonu nebo spuštění mise po výběru akce

Zbývá dotáhnout:
- definitivní UI
- všechny řetězce do stringtable

Nutno domyslet a dořešit:
- testování vozidel nebo zbraní (preferoval bych armory, nutno probrat s Jorisem)
- ošetření možnosti podvádění (spuštění zamknutých misí, zamknutých vozidel v armory)
- instalace podpisů při instalaci addonů

Prosím o rozhodnutí, kdy je možné dát commit (s ohledem na plánované patche vs. možnost testování).
