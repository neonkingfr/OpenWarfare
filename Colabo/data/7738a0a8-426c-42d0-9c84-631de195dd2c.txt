﻿> Zrada (už jsem o ní věděl ale zapomněl jsem). V době kdy vytvářím nový článek ještě neznám jeho id (to se určí až při zápisu do databáze).

A další zrada. V době vytváření dialogu příspěvku neznám ani definitivní titulek (teprv ho budu editovat).

Prozatím (revize 61437) beru aspoň nejčastější cestu (vyzkoušet možno při vybraném tagu "editor" nebo "licence"). Rozumný název souboru budeme teprv muset domyslet (např. použít v cestě nějakou proměnnou a tu nahradit titulkem až při odesílání).