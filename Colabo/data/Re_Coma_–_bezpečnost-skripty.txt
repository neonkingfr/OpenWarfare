﻿> Obvykle se tento problém řeší tak, že pokud je možnost vkládat HTML, sada povolených HTML tagů je velmi omezená. 

Z bezpečnostního hlediska je rozhodně nešťastné, když kdokoliv může vkládat libovolné skripty. Pokud chceme jakékoliv zabezpečění proti uživatelům-škodičům, musíme skripty omezit. Omezovat skripty autorstvím dokumentů se přitom nezdá moc vhodné, protože to by vyžadovalo zvláštní kategorii přístupových práv (editace skriptů) a to mi připadá dost nepřehledné.

Jako vhodnější by se mi jevilo požadovat, aby skripty buď byly uloženy v nějaké zvláštní části databáze, do které nemají běžní uživatelé přístup, nebo abych vyžadovali, aby byly nějak podepsané (pak by je mohl editovat jen ten, kdo vlastní certifikát, který ostatní uživatelé přijímají).