﻿> Průzkum bojem assertoval na následujících místech, připisuji navržená řešení:

Ostatní věci jsem snad vyřešil, tohle mě ale zlobí:

> TextBankD3D9::UseMipmap
> 
> - UseMipmap se bude smět volat jen z Visualize threadu

UseMipmap se totiž používá nejen při samotném renderování, ale také pro kreslení 2D, např. v `CStatic.OnDraw`. Nějak to vystěhovat si v tuto chvíli neumím představit, a tak se asi přikloním k tomu, že se vlastnictví "správy textur" (`_textureThread`) bude nějak předávat, a budu tedy muset zajistit, že Visualize thread neběží současně s 2D kreslením (to by snad neměl být problém - 2D kreslení se dělá před zaváděním textur). Pokud někdy později bude Visualize thread dělat nejen zavádění textur, ale i kreslení, by se kreslení 2D muselo na ten thread přestěhovat celé.



_____
**ondra** (18.3.2010 14:49:21)

Vlastnictví threadu se už mění.