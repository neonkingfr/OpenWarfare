﻿> Možnost lokálně potlačit tagy je jistě fajn, tento problém to ale nijak neřeší.

Nevím, jak to myslíš - dává to prostředky ho vyřešit. Pokud vím, že je něco dokumentace a nemá to zmizet po uzavření vlákna, označím to #-done a je to. Leccos je nutno řešit konvencemi na straně workflow. Nijak se nebráním tomu, aby ke konvencím později přibyly i nástroje, které dodržování konvencí usnadní (nebo někdy i vynutí), ale v první řadě je třeba ujasnit, jak přesně má správné workflow z hlediska tagů vypadat. Tohle je jeden návrh řešení.

