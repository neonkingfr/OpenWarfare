﻿> Tagy se budou defaultně vztahovat pouze k samotnému příspěvku.

V současné době se tagy defaultně při reply dědí. Jen pro srovnání: Chování v NR (zvlášť přimyslíme-li si property pro podvlákna), odpovídá zhruba tomu, že každý tag se chová jako +TAG a při reply se tagy nezadávají.

Většinou se mi současné chování zdá vyhovující, nicméně u některých tagů se mi vhodné nezdá, ale zatím nevím, jak to rozlišit. Chování, které mám na mysli, je již zmíněné míchání task-logu, discuss a announce, které se rozlišuje jen tagy. Typicky to pak funguje tak, že chci dědit task-log tag, ale skoro nikdy nechci dědit announce tag (a  u discuss je to trochu sporné).

Napadají mě možnosti:

- někdo někde zadá, které tagy mají mít jaký default. Tto by znamenalo, že tagy někde centrální existují jako samostatná tabulka. Může mít i jiné použití.

- uživatel to bude muset nějak řídit při zadávání tagu. Např. tag, který bude začínat tečkou (.) bude znamenat, že funguje jako tag bez tečky, ale při reply se nemá kopírovat do nového příspěvu.

-----
***jirka** (12.1.2010 10:17:47)*: Zdá se mi, že současná logika dědění tagů (a #tagy) už dané požadavky řeší. Můžeme tento příspěvek uzavřít?
