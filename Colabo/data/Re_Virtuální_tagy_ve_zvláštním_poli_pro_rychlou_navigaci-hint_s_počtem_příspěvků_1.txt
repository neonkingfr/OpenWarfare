﻿> Než to domyslíme, potřeboval bych aby pod cloudem tagů zatím aspoň příbyl pod čárou nezávislý seznam virtuálních tagů a to nejlépe pod sebou (navozuje to jejich smysl změny rootu lépe, než za sebou), jinak se mi s těmi více databázemi velice špatně pracuje.

Když ukážu na virtuální tag v cloudu, ukáže mi to počet příspěvků, ale v dolním poli ne - to mi trochu chybí.

-----
***maruk** (15.2.2010 10:35:02)*: Mě by se v dolním poli dokonce zdálo šikovné vidět přímo počet za názvem, tedy např.

:to read (20)
:@me (2)

atd.