﻿> Mohlo by to fungovat tak, že se budou definovat určité pravidla. Pravidlem bude dvojice: výraz používající tagy, akce. Akce budou např. schovej příspěvek, obarvi titulek na určitou barvu apod.

> 

> Pravidla by se sdružovala do nějakých setů, sety ukládaly do profilu nebo samostatných souborů a aktuální set by se vybíral z UI.


Rule sety snad už fungují tak, jak jsme zamýšleli, prosím vyzkoušejte.

Zbylé nápady (i z tohoto vlákna):

- proměnné v rule setech
- možnost rule sety ovlivnit řazení (něco jako defaultní řazení v NR, pomocí nové akce SORTORDER)

-----
***jirka** (22.1.2010 10:23:21)*: > Zbylé nápady (i z tohoto vlákna):
> 
> - proměnné v rule setech 

řešíme přes virtuální tagy

> - možnost rule sety ovlivnit řazení (něco jako defaultní řazení v NR, pomocí nové akce SORTORDER) 

není už prioritní


_____
**rola** (3.3.2010 16:10:44)

Ještě nápad z diskuze o rulesetech:

Každé pravidlo má přiřazené číslo = prioritu, vyšší priorita přehlasuje nižší.

Odporují-li si dvě pravidla se stejnou prioritou, může se to vyřešit třemi způsoby:

* použije se pravidlo uvedené později (jako třeba `CSS` – tam jsou priority implicitní podle specifičnosti pravidla)
* použije se pravidlo uvedené dřív (tak to je v Colabu teď)
* chyba :-)
