﻿Zkusil jsem přidat Marka Dzulka jako nového uživatele Colaba, zkusím sepsat postup a problémy dokud to mám v paměti:

- přes LDAP Admin přidán nový uživatel do LDAP databáze
  - New / User pod ou=users
  - při editaci přidána objectClass : colaboProfile
  - přidání colaboDBDN bylo zbytečné, databáze se stejně musela přidat z Colaba
  - nepříjemná je potřeba centrálního zadání hesla (nemůže si ho snadno nastavit uživatel sám)

- v Colabu přes Database Management dialog přidán uživatel a zařazen do skupiny all (opravena chybka zavlečená při přepisování komunikace s LDAP)

- uživatel si v Colabu zkonfiguroval LDAP autentifikaci a přidal Colabo databázi, pro jistotu pak restartoval
- Colabo ale nyní není plně funkční:
  - nezobrazuje se obsah článků (nějaký problém s SVN přístupem)
  - selhává odesílání článků (pravděpodobně obdobná příčina)
  - nejsou prý dostupné žádné rule sety
  - při autoupdatu hlásí zamknutý soubor CalendarDayView.dll (update ale proběhne a verzi aktualizuje)
  - chybí nějaké logování, které by pomohlo určit přesnou příčinu problémů

- centrální správa práv k SVN (a zároveň ke Colabu) může vyřešit hlavní problém
- je potřeba zapracovat na nějakém rozumném reportování problémů (exceptions, selhání operací apod.), to teď zkouším aspoň pro situace kde vidíme problémy

[jirka 17.9.2010 15:59:55]:
 Problém byl nakonec triviální, zbytečně jsem hledal jinde než bylo třeba.
 
 Colabo server si při svém startu vytvoří tabulku uživatelů pro rychlé rozhodnutí jaké requesty přijímat. Při změně uživatelů v LDAP (a posléze v SQL databázi) se tato změna na serveru nepromítne. Řešením bude např. jednou za čas tuhle tabulku obnovit.

[jirka 21.9.2010 9:21:27]:
 Upravil jsem server tak, aby si seznam oprávněných uživatelů jednou za 5 minut obnovil. (revize 73463, update serveru)
 
 Příspěvek nechávám otevřený jako návod pro přidání uživatele.
