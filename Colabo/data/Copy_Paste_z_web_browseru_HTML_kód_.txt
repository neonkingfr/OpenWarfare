﻿Když zkopíruji data z webbroseru, uloží se do clipboardu nejen v textovém formátu, ale též jako HTML (pro tento účel se mi docela osvědčil [Clipboard Format Spy](http://www.delphidabbler.com/software/cfs/download?mid=2). Myslím, že by bylo zajímavé pokusit se o nějaký aspoň částečný import tohoto HTML. Co mě zajímá nejvíc je schopnost zkonvertovat hyperlinky v běžném textu, základní formátovací atributy by byly pěkný bonus.

Otázka je, co při importu provést s atributy, které nepodporujeme.

Stejně, jako u [Citace přes clibpboard](data/Citace_přes_clibpboard.txt?server=https%3A%2F%2Fdev.bistudio.com%2FColabo%2Fresources%2F&database=&id=1540), bych potom chtěl, aby Shift-Ctrl-V dělalo Paste obyčejného textu.