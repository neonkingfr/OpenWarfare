﻿> - perzistence údajů o working copy mezi spuštěními Colaba
>     - buď ukládáním obsahu cache do souboru (bude potřeba i pro offline režim)

Hotovo v revizi 69079.

Ukládá se po každé synchronizaci a při ukončení Colaba, načítá se při startu Colaba.

Tím by měly být omezeny případy delšího načítání na nové články (dlouhé *Updating...* by mělo být pouze jednou u každého článku).

Poslední co zbývá z tohoto seznamu je update článků na pozadí.