﻿# %Coma% – tabulky **(verze 1)

Tabulky se zapisují jako poznámky. Mají navíc popsaný formát řádek ve `{`složených závorkách`}` pomocí atributů jednotlivých buněk:

  - Linka pod každým řádkem: `U`nderline (síla podle počtu `U` 0–3)
  - Barva pozadí: `I`nvertované / `S`tínované / `A`lternující (každý druhý řádek stínován) / `W`hite–bílé (implicitně)
  - Zvýraznění: `H`eader–hlavička s tučným písmem / `N`one–žádné (implicitně)
  - Vertikální zarovnání: `T`op–nahoru / `M`iddle–doprostřed (implicitně) / `B`ottom–dolů.
  - Horizontální zarovnání: `L`eft–doleva (implicitně) / `C`enter–doprostřed / `R`ight–doprava / `J`ustify–do bloku.
  - Slučování sloupců: číslo (počet sloupců, které se spojí do jednoho: implicitně 1)
  - Mezery: zvětší mezeru vlevo/vpravo od buňky

  - Čárka: pokračuje další buňkou
  - Svislítka: pokračuje další buňkou, předtím nakreslí svislou linku (síla podle počtu svislítek 0–3). Svislítka můžou být i vlevo a vpravo.
  - Pomlčkami lze nakreslit jednorázovou vodorovnou linku mezi řádky (síla podle počtu pomlček 0–3). Linka definovaná úplně na začátku tabulky se zopakuje i na konci.

Je-li použité písmeno velké, atribut se nastaví pro celý zbytek řádku.  


N`
[Table 0]:
 { IHC || 3 ---}
 A–F,Čísla
 {| hcu || Ar,r,r ---}
 A,1,2,3
 B,4,5,6
 C,7,8,9
 D,10,11,12
 E,13,14,15
`

<<style>
table{border:0px solid #000;border-collapse:collapse}
table td{border:0px solid #000;padding:0 0.25em}
table td.i{background:#333;color:#fff}
table td.s{background:#ccc}
table tr.o td.a{background:#ccc}
table.b,table tr.tb td{border-top:1px solid #999}
table.bb,table tr.tbb td{border-top-width:1px}
table.bbb,table tr.tbbb td{border-top-width:2px}
table.b,table td.ub{border-bottom:1px solid #999}
table.bb,table td.ubb{border-bottom-width:1px}
table.bbb,table td.ubbb{border-bottom-width:2px}
table td.h{font-weight:bold}
table td.t{vertical-align:top}
table td.b{vertical-align:bottom}
table td.c{text-align:center}
table td.r{text-align:right}
table td.j{text-align:justify}
table td.l1{padding-left:0.5em}
table td.l2{padding-left:1em}
table td.l3{padding-left:1.5em}
table td.l4{padding-left:2em}
table td.r1{padding-right:0.5em}
table td.r2{padding-right:1em}
table td.r3{padding-right:1.5em}
table td.r4{padding-right:2em}
table td.lb{border-left:1px solid #999}
table td.rb{border-right:1px solid #999}
table td.lbb{border-left-width:1px}
table td.rbb{border-right-width:1px}
table td.lbbb{border-left-width:2px}
table td.rbbb{border-right-width:2px}
</style>

<table class="bbb">
<tr               ><td class="i h c l1 r1 rbb"><p class="coma">A–F  <td class="i h c l1 r1 ubbb" colspan="3"><p class="coma">Čísla
<tr class="tbbb o"><td class="h c ub l1 r1 lb rbb"><p class="coma">A  <td class="a r l1"><p class="coma">1  <td class="a r"><p class="coma">2  <td class="a r r1"><p class="coma">3
<tr               ><td class="h c ub l1 r1 lb rbb"><p class="coma">B  <td class="a r l1"><p class="coma">4  <td class="a r"><p class="coma">5  <td class="a r r1"><p class="coma">6
<tr class="o"     ><td class="h c ub l1 r1 lb rbb"><p class="coma">C  <td class="a r l1"><p class="coma">7  <td class="a r"><p class="coma">8  <td class="a r r1"><p class="coma">9
<tr               ><td class="h c ub l1 r1 lb rbb"><p class="coma">D  <td class="a r l1"><p class="coma">10 <td class="a r"><p class="coma">11 <td class="a r r1"><p class="coma">12
<tr class="o"     ><td class="h c ub l1 r1 lb rbb"><p class="coma">E  <td class="a r l1"><p class="coma">13 <td class="a r"><p class="coma">14 <td class="a r r1"><p class="coma">15
</table>
