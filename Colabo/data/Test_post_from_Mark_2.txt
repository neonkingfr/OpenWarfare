﻿The last seven days of Colabo posts
------------------

<
<script type="text/javascript">
  log = window.external.Log("https://dev.bistudio.com/svn/pgm/Colabo", 7);
  n = log.Count;
  if(n>0)
  {
    lastDate = log.Get(n-1).Date;
    document.write("<img src=http://chart.apis.google.com/chart?cht=lc&chd=t:");
    posts = 1;
    xAxis = lastDate.substring(0,5);
    for (i=n-1; i>=0; i--)
    {
      entry = log.Get(i);
      date = entry.Date;
      if (date != lastDate)
      {
        lastDate = date;
	xAxis=xAxis+ "|" + lastDate.substring(0,5);
        document.write(posts + ",");
        posts = 0;
      }
      else
      {
        posts = posts + 1;
      };  
    };
    document.write(posts + "&chs=400x150&chxt=x,y&chxl=0:|" + xAxis + ">");
  };
</script>
>

Project Management
===
<
<script type="text/javascript">
  function CheckPrefix(str, prefix)
  {
    var prefixLength = prefix.length;
    var toCheck = str.substring(0, prefixLength);
    if (toCheck == prefix) return str.substring(prefixLength);
    return null;
  }
  function GetMilestones(db)
  {
    var ids = db.Select("%milestone:*");
    var milestones = [];
    m = ids.Count();
    for (var j=0; j<m; j++)
    {
      var article = db.GetArticle(ids.Get(j));
      var tags = article.Tags();
      var o = tags.Count();
      for (var k=0; k<o; k++)
      {
        check = CheckPrefix(tags.Get(k), "%milestone:");
        if (check != null)
        {
          if(milestones.join("").indexOf(check) == -1) milestones.push(check);
        }
      }
    }
    return milestones;
  }
  function GetTime(article,tag)
  {
    var tags = article.Tags();
    var o = tags.Count();
    var t = 0;
    for (var k=0; k<o; k++)
    {
      check = CheckPrefix(tags.Get(k), tag);
      if (check != null) t = parseFloat(check);
    }
    //document.write("| time = "+ t);
    return t;
  }
  function SumTime(db, article)
  { 
    //document.write("<BR>"+ article.Title());
    var time = GetTime(article, "%time:"); 
    var children = article.Children();
    var n = children.Count();
    for (var i=0; i<children.Count(); i++)
    {
      var child = db.GetArticle(children.Get(i));
      time += SumTime(db,child);
    }
    return time;
  }
//
//main
//
 var n = window.external.NDatabases;
 for (i=0; i<n; i++)
 {
  var db = window.external.GetDatabase(i);
  var tasks = "";
  var allocations = "";
  //document.write("milestones: " + milestones.length + " ->  " +milestones.join(", ") +"<br>");
  //document.write("Items with Milestones: "+ids.Count()+"<br>");
  var milestones = GetMilestones(db);
  for(x in milestones)
  {
    var ids = db.Select("%milestone:"+milestones[x]);
    m = ids.Count();
    document.write("<br><br>Milestone " + milestones[x] + "<br>---------------------");
    for (var j=0; j<m; j++)
    {
      var id = ids.Get(j);
      var article = db.GetArticle(id);
      document.write("<li>" + article.Title() + "</li>");
      var duration = GetTime(article, "%duration:");
      var time = SumTime(db, article);
      document.write(" >> Duration:"+duration + "h Time spent:" +time +"h");			
    }
  }
 }
</script>
>
