﻿Nelíbí se mi, jak dopadnou poznámky, když je ocituji. Poznámka je zcela zjevně ocitovaná, tak bych čekal, že bude i odsazená jako ocitovaná. Krom toho si myslím, že by poznámky měly být rovnou tam, kde je napíšu. Tady je vidět, že poznámky jsou až za textem, který už není součástí citace. Možná by šlo to řešit nějakou implementací v tom stylu, že poznámky nedáme až na konec textu, ale na konec bloku, ve kterém zrovna jsme?
 
To vše je motivováno použitím v append note - možná nějaké poznámky, které by se v append note chovaly dobře, už máme, a jen je tedy nevhodně použit druh poznámek?

> pojmenovává blok %Comatového% textu. Píše se jako odsazený blok uvozený `[nadpisem poznámky]:` V nadpisu nefunguje %Coma% syntaxe a hned za ním musí začít nový řádek.
> 
> [Řrřola 1.1.2010 12:57]:
>  Tohle je poznámka.
>  Druhý řádek poznámky.
>  Třetí řádek poznámky.
> Poznámka skončila, tohle už je obyčejný odstavec.
> 
> [Řrřola 1.1.2010 12:57 (2)]:
>  To je za tuhle minutu už druhá. Měl bych trochu zvolnit.

_____
Tohle je můj vlastní text.
_____
[rola 26.5.2010 13:12:51]:
 Nad tímhle už jsem přemýšlel. Zní tohle dobře?
 
 // Poznámku vlepím přímo na místo, kde je uvedená. Pokud se však někam vlepuje, pak z defaultního místa zmizí.

[ondra 26.5.2010 13:35:06]:
 Jo, to zní velice dobře.
