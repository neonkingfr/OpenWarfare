﻿> Budeme chtít umět importovat obsah news serveru do colaba.

Několikrát jsme se bavili o možnosti mít z Colaba přístup do newsgroup přímo online, bez nutnosti importu. Zkusil jsem proto dnes nějaké testy s NNTP protokolem a celkově myslím, že by to bylo možné (a realizovatelné během několika málo dnů, pokud bude zájem trvat).

Zkoušel jsem nejprve postup, kterým Bébul newsgroupy importoval. Ten se ukazuje jako neúnosně pomalý (načtení hlaviček 100 článků trvalo téměř půl minuty).

Podíval jsem se pak do zdrojáků newsreaderu a našel jsem tam obrat, který celý proces řádově urychluje - podstatné informace z hlaviček všech článků z fp.bistudio.bugs (> 13000 článků) se načetly během několika sekund (cca 3 sekundy trvalo samotné načtení, zatím bez jakéhokoli zpracování). Princip spočívá v tom, že místo načítání kompletních hlaviček jednotlivých článků můžeme server (pokud má určité rozšíření) požádat o konkrétní informaci z hlavičky pro mnoho článků najednou. Bude možná potřeba zjistit, jestli tato metoda příliš nezatěžuje server.


[maruk 8.1.2011 14:20:29]:
 Zájem trvá.
 
 Dokonce to považuji za hodně důlěžité, abychom mohli na colabo už konečně přejít do stavu reálného nasazení.

[jirka 10.1.2011 15:38:49]:
 Mám hotovu první část - načítání hlaviček příspěvků.
 
 Zveřejněno v revizi 77143, vyzkoušet možno následovně:
 - v konfiguračním dialogu přidat databázi
 - vybrat Server Type: News Server
 - vyplnit Server: news.bistudio.com
 - vyplnit Database: název NG, např. bistudio.engine.bugs
 - vyplnit User, Password: přihlašovací údaje do NG
 - SVN Path: nevyplňovat
 
 Pokud narazíte na nějaké problémy, dejte mi prosím vědět (než se pustím do další dáze).

[ondra 10.1.2011 16:32:48]:
 První dojem neuvěřitelně pozitivní - zejména import propert je super.
 
 Dost otravné mi ale přijde, že bych pro každou jednotlivou NG měl mít konfigurovaný zvláštní server. Myslím, že by místo toho Colabo mělo automaticky přihlásit všechny NG, které na serveru vidí, filtrování bych pak nechal pouze na tagách, s tím, že z názvu NG by měly vzniknout tagy (každá část oddělená tečkou jako jeden tag).
 

[budul 10.1.2011 16:48:10]:
 Pri snaze si pripojit NG me to pise. 
 ---------------------------
 LDAP
 ---------------------------
 LDAP update failed: The attribute exists or the value has been assigned.
 ---------------------------
 OK   
 ---------------------------
 

[jirka 10.1.2011 18:32:13]:
 > Dost otravné mi ale přijde, že bych pro každou jednotlivou NG měl mít konfigurovaný zvláštní server.
 
 Také mě to napadlo, ale měl jsem strach z neúměrného nárůstu doby synchronizace a paměťové náročnosti:
 1. skupin je opravdu mnoho a i ty nepřihlášené se musí stahovat
 2. v rámci skupiny funguje jednoduchá identifikace indexem, pro více skupin by to znamenalo převodní tabulky
 
 Pokud ale bude zájem, můžeme to zkusit.

[jirka 11.1.2011 14:59:16]:
 V revizi 77209 je jednoduché zobrazování obsahu článků, zatím bez jakýchkoli transformací (převod textu do Coma, překlad odkazů, přílohy).
 Největší objem práce byla změna architektury tak, aby si databáze sama říkala jak se dostat na obsah článků (a jak případně obsah interpretovat). Máme to tím ale připravené na další aplikace (mail, wiki apod.)

[budul 12.1.2011 9:47:47]:
 Nevim jestli to nejak souvisi s chybou co jsem psal vyse, ale NG me nefunguje a Colabo client jako takovy, se u me stal silne nestabilni. Vcera asi tak 3x prestal reagovat.

[jirka 12.1.2011 10:43:01]:
 Nevidím tu žádnou souvislost mezi NG a LDAP. Pokud máš nějaké podrobnější info (záznam pádu, hlášení o exception v logu apod.), určitě ho pošli.

[jirka 12.1.2011 13:41:36]:
 Drobné úpravy (revize 77256):
 - tagy dle názvu newsgroupy
 - oprava formátování (zacházení se znakem \\)
 - odstranění příloh
 - oprava odesílání článku při přihlášené News databázi
