﻿Nevyhovuje mi, že minor edit teď změní čas příp. datum příspěvku. Podle mě by na něj neměl mít vliv.

_____
**jirka** (9.3.2010 11:24:59)

Vzhledem ke konzistenci s ["Mass rename" tagů](data/_Mass_rename_tagů_1.txt?server=dev.bistudio.com&database=colabo&id=657) to rovnou udělám.

_____
**jirka** (9.3.2010 15:15:02)

Hotovo v revizi 66385.