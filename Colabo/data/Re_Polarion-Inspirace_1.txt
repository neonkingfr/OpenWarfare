﻿Polarion se mi z těch všech systémů zatím líbi zdaleka nejvíc. Některé věci řeší od Colaba odlišně, ale i tak v něm vidím pěknou inspiraci pro některá řešení, co by pro Colabo byly velkým přínosem:

Asi nejzajímavější přehled těch hezkých rysů je tady:
http://www.polarion.com/products/trackwiki/features.php

- portály
- dobrá integrace s svn vlastního vývoje
- linkování mezi dokumenty ale i mezi dokumenty a zdrojáky
- snadné zakládání issues přímo z Wiki textu