﻿Mazání draftů se mi zdá, že sice funguje, ale probíhá divně. Nejprve se mě to zeptá, jestli ho chci odstranit z databáze. Když řeknu, že ano, ještě se mě zeptá, jestli ho chci odstranit i z SVN, s tím, že se mě ptá na cestu obsahující "{DEFAULT FILENAME}".

Repro:

- ulož si draft
- synchronizuj
- naviguj přes :draft
- vyber draft a zmáčkni klávesu Delete


[jirka 11.2.2010 14:01:54]:
 Pravda, u příspěvku který v SVN evidentně není (má záporné id) by se ptát neměl. Mazání draftu beru vždy jako součást mazání z databáze.

-----
Už se neptá (revize 64869).
