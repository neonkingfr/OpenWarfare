﻿Great rules:

    done | fixed =>COLOR:Silver;
    priority & :@me =>COLOR:Red;
    :@me =>COLOR:Blue;
    priority & @* =>COLOR:Orange;
    @* => COLOR:Green
