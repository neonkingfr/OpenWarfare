﻿> 4. Implementace chybějících featur
>  a) templates (obsolete apod.)

Tohle se ukázalo být pro import dost kritické. Spousta článků obsahuje nejrůznější, primitivní, ale i komplikované templaty, které často dost podstatně mění formátování článku, případně přidávají kategorie, do kterých článek patří.
Implemementace byla poměrně náročná, templaty se vyhodnocují rekurzivně (mohou  a často jsou templates uvnitř templatů, navíc jsou některé templaty fixní, např. *{{PAGENAME}}*. Výsledek zparsování templatů je často umístěn na více řádků, což díky naší newlines colabo konvenci vede k odlišným vizuálním výsledkům, než na jaké jsme zvyklí ve wiki. Ukázka výsledku konverze článku hojně využívajícího templaty: [Podepisování dat (Data Signatures)](https://dev.bistudio.com/svn/sandbox2/data/wiki/e/e1/Podepisování_dat_%28Data_Signatures%29.txt?server=dev.bistudio.com&database=sandbox2&id=14136)

Pozn.: každý použitý template stahuju z wiki jako samostatný článek a cacheuju si ho. Tj. templaty budou zdržovat hlavně ze začátku, než se načtou. Při importu více článků to snad už nebude zdržovat. (Obrázky zdržujou víc.)

Pozn.2: buďto jsem nepochopil, jak ve wiki templatech fungují *&lt;includeonly&gt;xxx&lt;/includeonly&gt;*, protože to máme minimálně v několika templatech, ale naše wiki jako by to ignorovala. V implementaci konverze wiki templatů do colaba tyto tagy ignoruju a výsledek vypadá obdobně, jako ve wiki (jedná se o např. o *ttask-xxx* templaty)


_____
**bebul** (11.3.2010 17:55:22)

Ještě mě v téhle souvislosti napadlo, že by některé články mohly mít volitelně vypnutelný zachovávání těch newlines? Články konvertované z wiki by se prostě nastavily jako nezachovávající newlines s tím, že by si to mohl třeba někdo, kdo by to chtěl v colabu hodně měnit zase změnit, že by to bylo součástí New/Edit Article Dialogu (zaškrtávátko).

_____
**bebul** (11.3.2010 17:59:29)

Vyzkoušel jsem to a vypadá to pěkně. Je to změna jednoho boolu, takže by to mohlo jít udělat velmi snadno.