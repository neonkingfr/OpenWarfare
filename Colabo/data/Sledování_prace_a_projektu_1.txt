﻿V Colabu chceme možnost inteligentně řídit:

* které příspěvky se vůbec z svn stahují a jak často
* typ zvýraznění příspěvků
* jaké jsou povolené operace / zobrazené UI na základě stavu příspěvku a oprávnění uživatele


Těmto řídícím logikám budeme říkat Trackers.

Srovnej:
[Lepší chování :to read, trackers](data/Lepší_chování_to_read,_trackers_1.txt?server=dev.bistudio.com&database=colabo&id=666)
[Shrnutí, používané koncepce](data/Shrnutí,_používané_koncepce_1.txt?server=dev.bistudio.com&database=colabo&id=658)
