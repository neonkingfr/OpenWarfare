﻿> Při hraní složitejších misí (na Černarusi, ale i na Xboxu) hra spadne někde uvnitř JM alokátoru.

Dostal jsem chybu v CreateEvent, další event nešlo vytvořit. Abych tomu předešel, snažím se eventy recyklovat.

[ondra 5.8.2010 10:03:29]:
 Recyklace eventů mi funguje.
 

[ondra 5.8.2010 22:29:21]:
 Vyřešil jsem problémy a racy, které jsem našel (např. i recyklátor eventů musí být thread safe).

[ondra 6.8.2010 9:38:40]:
 Maličko jsme to s Jirkou ještě zjednodušili a pak jsem ještě vyčlenil recyklaci, aby se v SetResult nemuselo volat celé GetResult.
 
