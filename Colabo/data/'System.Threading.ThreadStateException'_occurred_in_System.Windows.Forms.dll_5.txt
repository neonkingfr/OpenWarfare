﻿Při pokusu o Search došlo k exception:
<pre>
An unhandled exception of type 'System.Threading.ThreadStateException' occurred in System.Windows.Forms.dll

Additional information: ActiveX control '8856f961-340a-11d0-a96b-00c04fd705a2' cannot be instantiated because the current thread is not in a single-threaded apartment.

Colabo.DialogSearchResults.DialogSearchResults(Colabo.FormColabo owner = {Colabo.FormColabo}, System.Collections.Generic.List<Colabo.SearchResult> results, string queryReadable = "serverová aplikace") Line 22	C#
Colabo.TaskSearch.Finish() Line 3392 + 0x2d bytes	C#
Colabo.Program.backgroundWorker_RunWorkerCompleted(object sender = {System.ComponentModel.BackgroundWorker}, System.ComponentModel.RunWorkerCompletedEventArgs e = {System.ComponentModel.RunWorkerCompletedEventArgs}) Line 192	C#
System.ComponentModel.BackgroundWorker.OnRunWorkerCompleted(System.ComponentModel.RunWorkerCompletedEventArgs e) + 0x6d bytes	
</pre>

Zajímavé je, že BackgroundWorker.OnRunWorkerCompleted běželo v jiném threadu než Program.Main. Nějak to asi bude souviset s přestěhováním BackgroundWorker z formuláře do aplikace (dříve se mi to nedělo, včera opakovaně), ale zatím se v tom neorientuji.
