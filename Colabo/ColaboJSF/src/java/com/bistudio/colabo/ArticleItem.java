package com.bistudio.colabo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.antlr.runtime.Token;

public class ArticleItem
{
    // database fields
    public int _id = -1;
    public int _parent = -1;
    public String _title = "";
    public String _url = "";
    public String _author = "";
    public java.util.Date _date = new java.util.Date(0);
    public int _revision = 0;
    public int _majorRevision = 0;
    // tags attached to this article
    public ArrayList<String> _ownTags = null;
    // tags inherited from parents
    public ArrayList<String> _inheritedTags = null;
    // effective tags of this article
    public ArrayList<String> _effectiveTags = null;
    // list of children articles
    public ArrayList<Integer> _children = null;
    // article marked to read
    public boolean _toRead = true;
    // article marked as changed
    public boolean _changed = true;
    // article expanded in the tree view (children are visible)
    public boolean _expanded = true;

    public static class TagSpecialChars
    {
        // special tags begins with these chars
        public static char TagPrivate = '.';
        public static char TagVirtual = ':';
        public static char TagAttribute = '%';

        // these characters are not part of tags, are used to control tag assigning
        public static char PrefixLocal = '#';
        public static char PrefixRemove = '-';

        // conventions: '/', '@'
    }
    public enum AccessRights
    {
        Read,
        ToRead,
        Edit
    }

    public String getTitle() {return _title;}
    public String getAuthor() {return _author;}
    public java.util.Date getDate() {return _date;}

    // tags preparing
    public void addTag(String tag)
    {
        if (_ownTags == null) _ownTags = new ArrayList<String>();
        _ownTags.add(tag);
    }
    public ArrayList<String> inheritsTags()
    {
        // get tags inherited by the parent
        ArrayList<String> tags;
        if (_inheritedTags == null) tags = new ArrayList<String>();
        else tags = new ArrayList<String>(_inheritedTags);

        // apply own tags
        if (_ownTags != null)
        {
            for (int i = 0; i < _ownTags.size(); i++)
            {
                String tag = _ownTags.get(i);
                if (tag == null || tag.isEmpty()) continue;
                if (tag.charAt(0) == TagSpecialChars.PrefixLocal) continue;
                if (tag.charAt(0) == TagSpecialChars.PrefixRemove)
                {
                    tag = tag.substring(1); // remove the '-' modifier
                    if (tag.isEmpty()) continue; // avoid empty tag
                    tags.remove(tag);
                    continue;
                }
                if (tag.charAt(0) == TagSpecialChars.TagAttribute) continue;
                if (tag.length() >= 2 && 
                    tag.charAt(0) == TagSpecialChars.TagPrivate &&
                    tag.charAt(1) == TagSpecialChars.TagAttribute) continue;

                if (!tags.contains(tag)) tags.add(tag);
            }
        }
        return tags.isEmpty() ? null : tags;
    }
    public void updateEffectiveTags()
    {
        // get tags inherited by the parent
        ArrayList<String> tags;
        if (_inheritedTags == null) tags = new ArrayList<String>();
        else tags = new ArrayList<String>(_inheritedTags);

        // apply own tags
        if (_ownTags != null)
        {
            for (int i=0; i<_ownTags.size(); i++)
            {
                String tag = _ownTags.get(i);
                if (tag == null || tag.isEmpty()) continue;
                if (tag.charAt(0) == TagSpecialChars.PrefixLocal)
                {
                    tag = tag.substring(1);
                    if (tag.isEmpty()) continue;
                }
                if (tag.charAt(0) == TagSpecialChars.PrefixRemove)
                {
                    tag = tag.substring(1);
                    if (tag.isEmpty()) continue;
                    tags.remove(tag);
                }
                else
                {
                    if (!tags.contains(tag)) tags.add(tag);
                }
            }
        }
        _effectiveTags = tags.isEmpty() ? null : tags;
    }
    static ArrayList<String> readTags(String text)
    {
        ArrayList<String> tags = new ArrayList<String>();

        // Tokenize tags into whitespace/commas, normal tags, quoted tags and invalid entries, and put them into the tag list.
        // Matches munch subsequent whitespace.
        // Notes:
        //  - necessary escaping: "" in @"literal strings", \\ in regexes, \\ and \- in [regex character classes]
        //  - IgnorePatternWhitespace doesn't inherit into [regex character classes]
        //  - see ColaboRuleSet.g for grammar details
        Pattern pWhitespaces = Pattern.compile("[ \\t\\r\\n\\f,]+");
        Pattern pNormalTag = Pattern.compile("([+\\-:.]?[^ \\t\\r\\n\\f,\"'\\\\;<>=&|()+\\*?]+)");
        Pattern pEscapedTag = Pattern.compile("\"[+\\-:.]?((\\\\[btnfr\"'\\\\0]) | (\\\\x[0-9a-fA-F]{2}) | (\\\\u[0-9a-fA-F]{4}) | ([^\\\\\"*?]) )+\"");
        Pattern pInvalid = Pattern.compile("[^ \\t\\r\\n\\f,]+");
        Matcher mWhitespaces = pWhitespaces.matcher(text);
        Matcher mNormalTag = pNormalTag.matcher(text);
        Matcher mEscapedTag = pEscapedTag.matcher(text);
        Matcher mInvalid = pInvalid.matcher(text);

        // catch the significant part
        int index = 0;
        while (index < text.length())
        {
            if (mWhitespaces.find(index))
            {
                index = mWhitespaces.end();
            }
            else if (mNormalTag.find(index))
            {
                tags.add(mNormalTag.group().toLowerCase());
                index = mNormalTag.end();
            }
            else if (mEscapedTag.find(index))
            {
                tags.add(mEscapedTag.group().toLowerCase());
                index = mEscapedTag.end();
            }
            else if (mInvalid.find(index))
            {
                String message = "Syntax error in tag specification: '" + mInvalid.group() + "'";
                // show the error message
                index = mInvalid.end();
            }
        }

        return tags;
    }

    // tags matching
    public boolean isEnabled(ArrayList<Articles.TagCondition> tags, Database db)
    {
        for (Articles.TagCondition tag : tags)
        {
            if (tag._present)
            {
                if (!containsTag(tag._tag, db)) return false;
            }
            else
            {
                if (containsTag(tag._tag, db)) return false;
            }
        }
        return true;
    }
    public boolean isEnabled(ArrayList<Articles.TagCondition> tags, ArrayList<Token> ruleSet, Database db)
    {
      ArrayList<String> actions = getActions(ruleSet, db);
      if (actions.contains("hide")) return false; // article hidden by rule set
      return isEnabled(tags, db);
    }
    public EnumSet<AccessRights> getAccessRights(Database database)
    {
        EnumSet<AccessRights> rights = EnumSet.noneOf(AccessRights.class);

        final String allowPrefix = "allow:";
        final String denyPrefix = "deny:";
        final String readRight = "read";
        final String toReadRight = "to_read";
        final String editRight = "edit";

        ArrayList<String> actions = getActions(database.getAccessRights(), database);
        for (String action : actions)
        {
            if (action.startsWith(allowPrefix))
            {
                // some right allowed
                String right = action.substring(allowPrefix.length());
                if (right.equals(readRight)) rights.add(AccessRights.Read);
                else if (right.equals(toReadRight)) rights.add(AccessRights.ToRead);
                else if (right.equals(editRight)) rights.add(AccessRights.Edit);
            }
            else if (action.startsWith(denyPrefix))
            {
                // some right denied
                String right = action.substring(denyPrefix.length());
                if (right.equals(readRight)) rights.remove(AccessRights.Read);
                else if (right.equals(toReadRight)) rights.remove(AccessRights.ToRead);
                else if (right.equals(editRight)) rights.remove(AccessRights.Edit);
            }
        }

        return rights;
    }
    public ArrayList<String> getActions(ArrayList<Token> ruleSet, Database database)
    {
        ArrayList<String> actions = new ArrayList<String>();
        if (ruleSet == null) return actions;

        Stack<StackItem> stack = new Stack<StackItem>();
        for (Token token : ruleSet)
        {
            StackItem arg1;
            StackItem arg2;
            switch (token.getType())
            {
                case ColaboRuleSetParser.MASK:
                    stack.push(new StackItemMask(token.getText()));
                    break;
                case ColaboRuleSetParser.IDENTIFIER:
                    stack.push(new StackItemTag(token.getText()));
                    break;
                case ColaboRuleSetParser.OR:
                    stack.push(new StackItemBool(stack.pop().bool(this, database) || stack.pop().bool(this, database)));
                    break;
                case ColaboRuleSetParser.AND:
                    stack.push(new StackItemBool(stack.pop().bool(this, database) && stack.pop().bool(this, database)));
                    break;
                case ColaboRuleSetParser.NOT:
                    stack.push(new StackItemBool(!stack.pop().bool(this, database)));
                    break;
                case ColaboRuleSetParser.LT:
                    arg2 = stack.pop();
                    arg1 = stack.pop();
                    stack.push(new StackItemBool(arg1.compare(this, new CompareStackItems()
                    {
                        public boolean compare(String a, String b)
                        {
                            return a.compareTo(b) < 0;
                        }
                    }, arg2, database)));
                    break;
                case ColaboRuleSetParser.GT:
                    arg2 = stack.pop();
                    arg1 = stack.pop();
                    stack.push(new StackItemBool(arg1.compare(this, new CompareStackItems()
                    {
                        public boolean compare(String a, String b)
                        {
                            return a.compareTo(b) > 0;
                        }
                    }, arg2, database)));
                    break;
                case ColaboRuleSetParser.LE:
                    arg2 = stack.pop();
                    arg1 = stack.pop();
                    stack.push(new StackItemBool(arg1.compare(this, new CompareStackItems()
                    {
                        public boolean compare(String a, String b)
                        {
                            return a.compareTo(b) <= 0;
                        }
                    }, arg2, database)));
                    break;
                case ColaboRuleSetParser.GE:
                    arg2 = stack.pop();
                    arg1 = stack.pop();
                    stack.push(new StackItemBool(arg1.compare(this, new CompareStackItems()
                    {
                        public boolean compare(String a, String b)
                        {
                            return a.compareTo(b) >= 0;
                        }
                    }, arg2, database)));
                    break;
                case ColaboRuleSetParser.EQ:
                    arg2 = stack.pop();
                    arg1 = stack.pop();
                    stack.push(new StackItemBool(arg1.compare(this, new CompareStackItems()
                    {
                        public boolean compare(String a, String b)
                        {
                            return a.compareTo(b) == 0;
                        }
                    }, arg2, database)));
                    break;
                case ColaboRuleSetParser.NE:
                    arg2 = stack.pop();
                    arg1 = stack.pop();
                    stack.push(new StackItemBool(arg1.compare(this, new CompareStackItems()
                    {
                        public boolean compare(String a, String b)
                        {
                            return a.compareTo(b) != 0;
                        }
                    }, arg2, database)));
                    break;
                case ColaboRuleSetParser.IMPL:
                    arg2 = stack.pop();
                    arg1 = stack.pop();
                    if (arg1.bool(this, database))
                        actions.add(arg2.text());
                    break;
            }
        }
        return actions;
    }
    void collectSubtreeActions(ArrayList<String> actions, ArrayList<Token> ruleSet, Database database)
    {
        if (_children == null) return;
        for (int i=0; i<_children.size(); i++)
        {
            ArticleItem child = database.getArticle(_children.get(i));
            if (child != null)
            {
                ArrayList<String> childActions = child.getActions(ruleSet, database);
                if (!childActions.contains("hide"))
                {
                    // merge
                    for (String action : childActions)
                    {
                        if (!actions.contains(action)) actions.add(action);
                    }
                }
                // recurse
                child.collectSubtreeActions(actions, ruleSet, database);
            }
        }
    }

    // check if article contains given tag
    private boolean containsTag(final String tagName, final Database database)
    {
        if (checkEachTag(database, new CheckTagDelegate()
        {
            public boolean check(String tag)
            {
                return tag.equals(tagName);
            }
        })) return true;

        if (tagName.startsWith(Colabo._ancestorPrefix))
        {
            final String tag = tagName.substring(Colabo._ancestorPrefix.length());
            return forEachAncestor(database, new CheckArticleDelegate()
            {
                public boolean check(ArticleItem item)
                {
                    return item.containsTag(tag, database);
                }
            });
        }
        if (tagName.startsWith(Colabo._descendantPrefix))
        {
            final String tag = tagName.substring(Colabo._descendantPrefix.length());
            return forEachDescendant(database, new CheckArticleDelegate()
            {
                public boolean check(ArticleItem item)
                {
                    return item.containsTag(tag, database);
                }
            });
        }
        if (tagName.startsWith(Colabo._threadPrefix))
        {
            final String tag = tagName.substring(Colabo._threadPrefix.length());
            return getRoot(database).forEachDescendant(database, new CheckArticleDelegate()
            {
                public boolean check(ArticleItem item)
                {
                    return item.containsTag(tag, database);
                }
            });
        }
        return false;
    }
    // check if some tag is matching the mask
    private boolean matchTag(String mask, Database database)
    {
        // convert wildcard to regular expression
        Pattern p1 = Pattern.compile("\\*");
        Pattern p2 = Pattern.compile("\\?");

        String expression = p1.matcher(mask).replaceAll(".*");
        expression = p2.matcher(expression).replaceAll(".");

        final Pattern p = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        return checkEachTag(database, new CheckTagDelegate()
        {
            public boolean check(String tag)
            {
                return p.matcher(tag).matches();
            }
        });
    }
    // check if some tag satisfying the condition is matching the mask
    private boolean matchTag(String mask, final CompareStackItems cmp, final String with, Database database)
    {
        // convert wildcard to regular expression
        Pattern p1 = Pattern.compile("\\*");
        Pattern p2 = Pattern.compile("\\?");

        String expression = p1.matcher(mask).replaceAll(".*");
        expression = p2.matcher(expression).replaceAll(".");

        final Pattern p = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        return checkEachTag(database, new CheckTagDelegate()
        {
            public boolean check(String tag)
            {
                return cmp.compare(tag, with) && p.matcher(tag).matches();
            }
        });
    }
    // call the delegate for each virtual and real tag
    private boolean checkEachTag(Database database, CheckTagDelegate func)
    {
        // virtual tags
        if (_toRead && func.check(Colabo._tagToRead)) return true;
        if (_changed && func.check(Colabo._tagChanged)) return true;
        if (isAssignedToMe(database) && func.check(Colabo._tagToMe)) return true;
        if (_author.equalsIgnoreCase(database.getUser()) && func.check(Colabo._tagByMe)) return true;
        String byAuthor = Colabo._tagBy + _author.toLowerCase();
        if (func.check(byAuthor)) return true;
        String myDb = Colabo._tagDb + database.getName();
        if (func.check(myDb)) return true;
        // if (!string.IsNullOrEmpty(_draft) && func.check(Colabo._tagDraft)) return true;
        // if (database.homepage > 0 && _id == database.homepage && func.check(Colabo._tagHomepage)) return true;
        if (isToday() && func.check(Colabo._tagToday)) return true;
        if (isThisWeek() && func.check(Colabo._tagThisWeek)) return true;
        if (isThisMonth() && func.check(Colabo._tagThisMonth)) return true;
        if (isLastMonth() && func.check(Colabo._tagLastMonth)) return true;
        
        // real tags
        if (_effectiveTags != null)
        {
            for (String tag : _effectiveTags)
            {
                if (func.check(tag)) return true;
                if (tag.charAt(0) == TagSpecialChars.TagPrivate && func.check(tag.substring(1))) return true;
            }
        }
        return false;
    }

    public boolean isAssignedToMe(Database database)
    {
        if (_effectiveTags == null) return false;

        // assigned to me
        if (_effectiveTags.contains("@" + database.getUser().toLowerCase())) return true;
        // assigned to a group I belong to
        for (LDAPInfo.GroupInfo group : database.getGroups())
        {
            if (_effectiveTags.contains("@" + group._name.toLowerCase())) return true;
        }
        return false;
    }
    public boolean isToday()
    {
        Calendar today = Calendar.getInstance();
        Calendar date = Calendar.getInstance();
        date.setTime(_date);
        return date.get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
               date.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR);
    }
    public boolean isThisWeek()
    {
        Calendar today = Calendar.getInstance();
        Calendar date = Calendar.getInstance();
        date.setTime(_date);
        return date.get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
               date.get(Calendar.WEEK_OF_YEAR) == today.get(Calendar.WEEK_OF_YEAR);
    }
    public boolean isThisMonth()
    {
        Calendar today = Calendar.getInstance();
        Calendar date = Calendar.getInstance();
        date.setTime(_date);
        return date.get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
               date.get(Calendar.MONTH) == today.get(Calendar.MONTH);
    }
    public boolean isLastMonth()
    {
        Calendar lastMonth = Calendar.getInstance();
        lastMonth.add(Calendar.MONTH, -1);
        Calendar date = Calendar.getInstance();
        date.setTime(_date);
        return date.get(Calendar.YEAR) == lastMonth.get(Calendar.YEAR) &&
               date.get(Calendar.MONTH) == lastMonth.get(Calendar.MONTH);
    }

    // find the root of the item tree
    private ArticleItem getRoot(Database database)
    {
        ArticleItem item = this;
        while (item._parent > 0)
        {
            ArticleItem parent = database.getArticle(item._parent);
            if (parent == null) break;
            item = parent;
        }
        return item;
    }
    // evaluate the function for each ancestor, including itself
    private boolean forEachAncestor(Database database, CheckArticleDelegate func)
    {
        // check itself
        if (func.check(this)) return true;

        ArticleItem item = this;
        while (item._parent > 0)
        {
            ArticleItem parent = database.getArticle(item._parent);
            if (parent == null) break;
            if (func.check(parent)) return true;
            item = parent;
        }
        return false;
    }
    // evaluate the function for each descendant, including itself
    private boolean forEachDescendant(Database database, CheckArticleDelegate func)
    {
        // check itself
        if (func.check(this)) return true;
        
        if (_children != null)
        {
            for (int index : _children)
            {
                ArticleItem child = database.getArticle(index);
                if (child != null)
                {
                    // recursion
                    if (child.forEachDescendant(database, func)) return true;
                }
            }
        }
        return false;
    }

    private interface CheckArticleDelegate
    {
        public boolean check(ArticleItem item);
    }
    private interface CheckTagDelegate
    {
        public boolean check(String tag);
    }
    private interface CompareStackItems
    {
        public boolean compare(String a, String b);
    }
    private interface StackItem
    {
        public boolean bool(ArticleItem item, Database database);
        public String text();
        public boolean compare(ArticleItem item, CompareStackItems cmp, StackItem value, Database database);
    }
    private class StackItemBool implements StackItem
    {
        private boolean _value;
        public StackItemBool(boolean value) {_value = value;}
        public boolean bool(ArticleItem item, Database database) { return _value; }
        public String text() { throw new IllegalArgumentException(); }
        public boolean compare(ArticleItem item, CompareStackItems cmp, StackItem value, Database database) { throw new IllegalArgumentException(); }
    }
    private class StackItemTag implements StackItem
    {
        private String _value;
        public StackItemTag(String value)
        {
        // if tag enclosed in "", remove them and unescape it
        if (value.length() >= 2 && value.charAt(0) == '"' && value.charAt(value.length() - 1) == '"')
        {
            // TODO: unescape
            _value = value.substring(1, value.length() - 1).toLowerCase();
            // _value = Regex.Unescape(value.substring(1, value.length() - 1)).toLowerCase();
        }
        else
            _value = value.toLowerCase();
        }
        public boolean bool(ArticleItem item, Database database) { return item.containsTag(_value, database); }
        public String text() { return _value; }
        public boolean compare(ArticleItem item, CompareStackItems cmp, StackItem value, Database database)
        {
          return cmp.compare(text(), value.text());
        }
    }
    private class StackItemMask implements StackItem
    {
        private String _value;
        public StackItemMask(String value)
        {
            // if mask enclosed in "", remove them and unescape it
            if (value.length() >= 2 && value.charAt(0) == '"' && value.charAt(value.length() - 1) == '"')
            {
                // TODO: unescape
                _value = value.substring(1, value.length() - 1).toLowerCase();
                // _value = Regex.Unescape(value.substring(1, value.length() - 1)).toLowerCase();
            }
            else
                _value = value.toLowerCase();
        }
        public boolean bool(ArticleItem item, Database database) { return item.matchTag(_value, database); }
        public String text() { throw new IllegalArgumentException(); }
        public boolean compare(ArticleItem item, CompareStackItems cmp, StackItem value, Database database)
        {
            return item.matchTag(_value, cmp, value.text(), database);
        }
    }

    // helper function to decorate the private tag with a special character
    static public String toPrivateTag(String tag)
    {
        int pos = 0;
        // skip the prefix
        if (tag.length() > pos && tag.charAt(pos) == TagSpecialChars.PrefixLocal) pos++; // local scope
        if (tag.length() > pos && tag.charAt(pos) == TagSpecialChars.PrefixRemove) pos++; // tag removing
        // insert special character as the first character of the tag
        return tag.substring(0, pos) + "." + tag.substring(pos);
    }
    // helper function to recognize if tag is a private tag and remove the decoration
    static public String fromPrivateTag(String tag)
    {
        int pos = 0;
        // skip the prefix
        if (tag.length() > pos && tag.charAt(pos) == TagSpecialChars.PrefixLocal) pos++; // local scope
        if (tag.length() > pos && tag.charAt(pos) == TagSpecialChars.PrefixRemove) pos++; // tag removing
        // check the special character
        if (tag.length() > pos && tag.charAt(pos) == TagSpecialChars.TagPrivate)
        return tag.substring(0, pos) + tag.substring(pos + 1); // remove the special character
        // special character not present
        return null;
    }
}
