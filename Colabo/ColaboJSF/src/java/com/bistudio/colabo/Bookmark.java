/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bistudio.colabo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import javax.faces.context.FacesContext;

/**
 *
 * @author Jirka
 */
/// Bookmark properties
public class Bookmark
{
    // owner database
    public Database _database;
    // unique id of the bookmark in the database
    public int _id;
    // user friendly name
    public String _name;
    // image index
    public int _image;
    // list of tags to select
    public String[] _tags;
    // article to select
    public int _articleId;
    // parent bookmark in hierarchy
    public int _parent;

    // used for database items
    public Bookmark(Database database)
    {
        _database = database;
        _id = -1;
        _image = 0;
        _articleId = -1;
        _parent = -1;
    }
    // used for default items
    public Bookmark(String name, int image, String[] tags)
    {
        _database = null;
        _id = -1;
        _name = name;
        _image = image;
        if (tags != null) _tags = Arrays.copyOf(tags, tags.length);
        else _tags = null;
        _articleId = -1;
        _parent = -1;
    }

    public String getName() {return _name;}
    public ArrayList<Bookmark> getChildren()
    {
        ArrayList<Bookmark> children = new ArrayList<Bookmark>();
        if (_database != null)
        {
            ArrayList<Bookmark> list = _database.getBookmarks();
            for (Bookmark bookmark : list)
            {
                if (bookmark._parent == _id) children.add(bookmark);
            }
        }
        return children;
    }

    // list of default bookmarks
    public static Bookmark[] DefaultBookmarks =
    {
        // name, image index, tags
        new Bookmark("Unread", 2, new String[] {Colabo._tagToRead}),
        new Bookmark("My Tasks", 2, new String[] {Colabo._tagToMe}),
        new Bookmark("Sent", 2, new String[] {Colabo._tagByMe})
        // new Bookmark("Drafts", 2, new String[] {Colabo._tagDraft})
    };

    public void onSelected() throws IOException
    {
        FacesContext context = FacesContext.getCurrentInstance();
        Colabo app = (Colabo)context.getExternalContext().getSessionMap().get("colabo");
        Articles articles = app.getArticles();
        articles.processBookmark(this);
    }
}
