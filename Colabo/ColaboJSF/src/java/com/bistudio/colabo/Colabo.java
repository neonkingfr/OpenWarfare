/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bistudio.colabo;

import com.bistudio.colabo.Articles.TagCondition;
import com.bistudio.colabo.Articles.TagsStatsItem;
import com.unboundid.ldap.sdk.LDAPException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.antlr.runtime.RecognitionException;
import org.codehaus.jettison.json.JSONException;
import org.richfaces.event.DropEvent;

/**
 *
 * @author jirka
 */
@ManagedBean(name = "colabo")
@SessionScoped
public class Colabo implements Serializable
{
    // error message for error.xhtml page
    private String _error = null;
    public String getErrorMessage() {return _error;}

    // info read from the LDAP, access to it from JSF
    private LDAPInfo _ldapInfo = new LDAPInfo();
    public LDAPInfo getLdap() {return _ldapInfo;}

    private SSLSocketFactory _socketFactory = null;

    // size of panes
    private int _leftPaneWidth = 25;
    public int getLeftPaneWidth() {return _leftPaneWidth;}
    private int _leftTopPaneHeight = 60;
    public int getLeftTopPaneHeight() {return _leftTopPaneHeight;}
    private int _rightTopPaneHeight = 50;
    public int getRightTopPaneHeight() {return _rightTopPaneHeight;}

    // cache of all read articles
    private Articles _articles = new Articles();
    public Articles getArticles() {return _articles;}

    public class CurrentTagsDataSource extends DataModel<CurrentTagsDataSource.Item>
    {
        public class Item
        {
            private String _tag;
            private boolean _present;
            private int _index;

            public Item(String tag, boolean present, int index)
            {
                _tag = tag; _present = present; _index = index;
            }
            public String getText() {return _present ? _tag : "NOT " + _tag;}
            public void onSelected() throws IOException
            {
                _articles.splitOffTags(_index);
            }
        }

        private int _currentIndex = -1;

        @Override
        public boolean isRowAvailable()
        {
            return _currentIndex >= 0 && _currentIndex < getRowCount();
        }
        @Override
        public int getRowCount()
        {
            return _articles.getTags().size() + 1;
        }
        @Override
        public Item getRowData()
        {
            if (_currentIndex < 0) return null;
            if (_currentIndex == 0) return new Item("all", true, _currentIndex);
            TagCondition cond = _articles.getTags().get(_currentIndex - 1);
            return new Item(cond._tag, cond._present, _currentIndex);
        }
        @Override
        public int getRowIndex()
        {
           return _currentIndex;
        }
        @Override
        public void setRowIndex(int index)
        {
            _currentIndex = index;
        }
        @Override
        public Object getWrappedData()
        {
            return null;
        }
        @Override
        public void setWrappedData(Object data)
        {
        }
    }
    private CurrentTagsDataSource _currentTags = new CurrentTagsDataSource();
    public CurrentTagsDataSource getCurrentTags() {return _currentTags;}

    public class AllTagsDataSource extends DataModel<AllTagsDataSource.Item>
    {
        public class Item
        {
            private Entry<String, TagsStatsItem> _entry = null;

            public Item(Entry<String, TagsStatsItem> entry) {_entry = entry;}

            public String getText() {return _entry.getKey();}

            public int getSize()
            {
                final int avgSize = 88; // avgCount has this size (in percent)
                double doubleSizeCountRatio = Math.max(2.0, _articles.getTagsMaxCount()); // the biggest tag is twice as big as the tiniest tag, depends on log(article count)
                final int minSize = 63; // but enforce minimum size for the small tags

                // font size calculation: number of articles
                double logSize = Math.log(_entry.getValue()._total / _articles.getTagsAvgCount()) / Math.log(doubleSizeCountRatio);
                int size = (int)(avgSize * Math.pow(2, logSize));
                return Math.max(minSize, size);
            }

            public String getColor()
            {
                // if there are any unread articles, color = blue..red: number of unread articles
                int color = 0;
                int unread = _entry.getValue()._unread;
                if (unread > 0)
                {
                    color = Math.min(12, (int)(Math.log(unread + 4) * 4) - 5);
                    return "c" + color;
                }
                // if all articles were read, color = black..greenish: newest article age (levels: 4 days, 2 weeks, 2 months, 1 year)
                else
                {
                    Calendar newest = Calendar.getInstance();
                    newest.setTime(_entry.getValue()._newest);

                    Calendar check = Calendar.getInstance();
                    check.add(Calendar.YEAR, -1);
                    if (newest.before(check)) return "c23";

                    check = Calendar.getInstance();
                    check.add(Calendar.MONTH, -2);
                    if (newest.before(check)) return "c22";

                    check = Calendar.getInstance();
                    check.add(Calendar.DAY_OF_YEAR, -14);
                    if (newest.before(check)) return "c21";

                    check = Calendar.getInstance();
                    check.add(Calendar.DAY_OF_YEAR, -4);
                    if (newest.before(check)) return "c20";

                    return "c0";
                }
            }

            public void onSelected() throws IOException
            {
                String tag = _entry.getKey();
                _articles.addCurrentTag(tag);
            }
        }

        private int _currentIndex = -1;
        @Override
        public boolean isRowAvailable()
        {
            return _currentIndex >= 0 && _currentIndex < getRowCount();
        }
        @Override
        public int getRowCount()
        {
            return _articles.getAllTags() == null ? 0 : _articles.getAllTags().length;
        }
        @Override
        public Item getRowData()
        {
            if (_currentIndex < 0) return null;
            Object pair = _articles.getAllTags()[_currentIndex];
            return new Item((Entry<String, TagsStatsItem>)pair);
        }
        @Override
        public int getRowIndex()
        {
            return _currentIndex;
        }
        @Override
        public void setRowIndex(int index)
        {
            _currentIndex = index;
        }
        @Override
        public Object getWrappedData()
        {
            return null;
        }
        @Override
        public void setWrappedData(Object data)
        {
        }
    }
    private AllTagsDataSource _allTags = new AllTagsDataSource();
    public AllTagsDataSource getAllTags() {return _allTags;}

    @FacesConverter(value="FixedDateTimeConverter")
    public static class FixedDateTimeConverter extends javax.faces.convert.DateTimeConverter
    {
        public FixedDateTimeConverter()
        {
            super();
            setTimeZone(TimeZone.getDefault());
            setType("both");
            setDateStyle("medium");
            setTimeStyle("medium");
        }
    }

    // virtual tags names
    public static String _tagToRead = ":to-read";
    public static String _tagChanged = ":changed";
    public static String _tagToMe = ":@me";
    public static String _tagByMe = ":by-me";
    public static String _tagBy = ":by-";
    public static String _tagDb = ":db-";
    public static String _tagDraft = ":draft";
    public static String _tagHomepage = ":homepage";
    public static String _tagToday = ":today";
    public static String _tagThisWeek = ":this-week";
    public static String _tagThisMonth = ":this-month";
    public static String _tagLastMonth = ":last-month";
    public static String _ancestorPrefix = ":ancestor-has-";
    public static String _descendantPrefix = ":descendant-has-";
    public static String _threadPrefix = ":thread-has-";

    /** Root class, UI handling */
    public Colabo() throws IOException, GeneralSecurityException
    {
        CustomKeyManager[] manager = new CustomKeyManager[1];
        manager[0] = new CustomKeyManager();
        manager[0].add(getTrustManagers("trusted_ca.jks"));
        manager[0].add(getTrustManagers("trusted_of_ca.jks"));
/*
        TrustManager[] manager = getTrustManagers("trusted_ca.jks");
*/        
        SSLContext sslcx = SSLContext.getInstance("SSLv3");
        sslcx.init(null, manager, new SecureRandom());

        _socketFactory = sslcx.getSocketFactory();
        
        loadCookies();
    }
    private TrustManager[] getTrustManagers(String filename) throws IOException, GeneralSecurityException
    {
        // read the certificate to enable SSL connection
        InputStream stream = null;
        try
        {
            stream = Colabo.class.getResourceAsStream(filename);

            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(stream, "changeit".toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance("PKIX");
            tmf.init(ks);

            // SSLContext sslcx = SSLContext.getInstance("TLS");
            return tmf.getTrustManagers(); 
        }
        finally
        {
            if (stream != null)
            {
                try
                {
                    stream.close();
                }
                catch (IOException ex) {}
            }
        }
    }
    private final class CustomKeyManager implements X509TrustManager
    {
        ArrayList<X509TrustManager> _submanagers = new ArrayList<X509TrustManager>();

        public void add(TrustManager[] mans) 
        {
            for (TrustManager man : mans)
            {
                if (man instanceof X509TrustManager) _submanagers.add((X509TrustManager)man);
            }
        }
        
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException 
        {
            CertificateException last = null;
            for (X509TrustManager man : _submanagers)
            {
                try
                {
                    man.checkClientTrusted(arg0, arg1);
                    return;
                }
                catch (CertificateException ex)
                {
                    last = ex;
                }
            }
            // all managers failed to check
            throw last;
        }
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException 
        {
            CertificateException last = null;
            for (X509TrustManager man : _submanagers)
            {
                try
                {
                    man.checkServerTrusted(arg0, arg1);
                    Logger.getGlobal().info("Certificate " + CertName(arg0) + " is trusted");
                    return;
                }
                catch (CertificateException ex)
                {
                    last = ex;
                }
            }
            // all managers failed to check
            Logger.getGlobal().severe("Certificate " + CertName(arg0) + " is not trusted");
            
            throw last;
        }
        public X509Certificate[] getAcceptedIssuers() 
        {
            ArrayList<X509Certificate> list = new ArrayList<X509Certificate>();
            for (X509TrustManager man : _submanagers) Collections.addAll(list, man.getAcceptedIssuers());
            return list.toArray(new X509Certificate[0]);
        }
        private String CertName(X509Certificate[] cert) 
        {
            String result = "";
            for (X509Certificate c : cert)
            {
                if (!result.isEmpty()) result += " *** ";
                result += c.getSubjectDN();
            }
            return result;
        }
    }

    // login to LDAP based of data in form, read all data
    public String login() throws LDAPException, IOException, JSONException, RecognitionException
    {
        _error = _ldapInfo.login(_socketFactory);
        if (_error != null) return "fail";

        _articles.init(_ldapInfo, _socketFactory);
        _articles.update();

        return "success";
    }

    public static void showMessage(String message)
    {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(message));
    }

    public void splitterToLeft(DropEvent e)
    {
        if (_leftPaneWidth > 10)
        {
            _leftPaneWidth -= 5;
            saveCookie("UILeftPaneWidth", Integer.toString(_leftPaneWidth));
        }
    }
    public void splitterToRight(DropEvent e)
    {
        if (_leftPaneWidth < 90)
        {
            _leftPaneWidth += 5;
            saveCookie("UILeftPaneWidth", Integer.toString(_leftPaneWidth));
        }
    }
    public void leftSplitterUp(DropEvent e)
    {
        if (_leftTopPaneHeight > 10)
        {
            _leftTopPaneHeight -= 5;
            saveCookie("UILeftTopPaneHeight", Integer.toString(_leftTopPaneHeight));
        }
    }
    public void leftSplitterDown(DropEvent e)
    {
        if (_leftTopPaneHeight < 90)
        {
            _leftTopPaneHeight += 5;
            saveCookie("UILeftTopPaneHeight", Integer.toString(_leftTopPaneHeight));
        }
    }
    public void rightSplitterUp(DropEvent e)
    {
        if (_rightTopPaneHeight > 10)
        {
            _rightTopPaneHeight -= 5;
            saveCookie("UIRightTopPaneHeight", Integer.toString(_rightTopPaneHeight));
        }
    }
    public void rightSplitterDown(DropEvent e)
    {
        if (_rightTopPaneHeight < 90)
        {
            _rightTopPaneHeight += 5;
            saveCookie("UIRightTopPaneHeight", Integer.toString(_rightTopPaneHeight));
        }
    }

    private void loadCookies()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        Cookie cookies[] = request.getCookies();
        if (cookies != null) for (Cookie cookie : cookies)
        {
            String name = cookie.getName();
            if (name.equals("UILeftPaneWidth")) _leftPaneWidth = Integer.parseInt(cookie.getValue());
            else if (name.equals("UILeftTopPaneHeight")) _leftTopPaneHeight = Integer.parseInt(cookie.getValue());
            else if (name.equals("UIRightTopPaneHeight")) _rightTopPaneHeight = Integer.parseInt(cookie.getValue());
        }
    }
    private void saveCookie(String name, String value)
    {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(Integer.MAX_VALUE);
        response.addCookie(cookie);
    }
}
