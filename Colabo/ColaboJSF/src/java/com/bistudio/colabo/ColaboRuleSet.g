grammar ColaboRuleSet;

options
{
	language = Java;
}

tokens
{
	OR = '|';
	AND = '&';
	NOT = '!';
	LPAR = '(';
	RPAR = ')';
	GT = '>';
	LT = '<';
	GE = '>=';
	LE = '<=';
	EQ = '==';
	NE = '!=';
	IMPL = '=>';
	END = ';';
}

@header
{
package com.bistudio.colabo;
}

@members
{
public ArrayList<Token> _result;
}

// Parser rules
ruleSet
@init 
{
_result = new ArrayList<Token>();
}
	:       EOF | rule (END rule)*;
rule
	:	expression op = IMPL action {_result.add($op);};
expression
	:	term (op = OR term {_result.add($op);})*;
term
	:	factor (op = AND factor {_result.add($op);})*;
factor
	:	(mask_or_id | comparison | (LPAR expression RPAR)) |
		(op = NOT (mask_or_id | comparison | (LPAR expression RPAR))) {_result.add($op);};
comparison
	:	mask_or_id op = (GT | LT | GE | LE | EQ | NE) id {_result.add($op);};
action
	:	val = IDENTIFIER {_result.add($val);};
mask_or_id
	:	val = (MASK | IDENTIFIER) {_result.add($val);};
id	
	:	val = IDENTIFIER {_result.add($val);};
	
// Lexer rules	
WHITESPACE
	:	(' ' | '\t' | '\r' | '\n' | '\u000C')+ {$channel = HIDDEN;};

IDENTIFIER
	:	CHAR+ | ('"' (ESC_SEQ | ~('\\' | '"' | '*' | '?'))* '"');

MASK	
	:	(CHAR | '*' | '?')+ | ('"' (ESC_SEQ | ~('\\' | '"'))* '"');

// characters legal in identifiers
fragment CHAR:  ~(' ' | '\t' | '\r' | '\n' | '\u000C' | '\'' | '\\' | '\"' | ';' | '<' | '>' | '=' | '&' | '|' | '!' | '(' | ')' | '+' | '*' | '?' | ',')
    ;

// escape sequence is legal inside strings
fragment ESC_SEQ
    	:   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\'|'0')
    	|   UNICODE_ESC
    	|   HEXADECIMAL_ESC;

fragment HEXADECIMAL_ESC
    	:   '\\' 'x' HEX_DIGIT HEX_DIGIT;

fragment UNICODE_ESC
    	:   '\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT;

fragment HEX_DIGIT
	: ('0'..'9' | 'a'..'f' | 'A'..'F');
	