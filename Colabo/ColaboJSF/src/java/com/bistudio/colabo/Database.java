/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.colabo;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.antlr.runtime.*;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONTokener;

/**
 *
 * @author Jirka
 */
public class Database
{
    private Articles.DBConnection _connection = null;
    private LDAPInfo.DBInfo _dbInfo = null;
    private ArrayList<Token> _accessRights;

    private static DateFormat _inputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

    private HashMap<Integer, ArticleItem> _articles = new HashMap<Integer, ArticleItem>();

    public class RuleSet
    {
        public String _user;
        public String _name;
        public String _expression;
        // rule sets inheritance
        public String _parentUser;
        public String _parentName;

        public RuleSet()
        {
        }
        public RuleSet(RuleSet src)
        {
            _user = src._user;
            _name = src._name;
            _expression = src._expression;
            _parentUser = src._parentUser;
            _parentName = src._parentName;
        }
    }
    private ArrayList<RuleSet> _ruleSets = new ArrayList<RuleSet>();

    private ArrayList<Bookmark> _bookmarks = new ArrayList<Bookmark>();
    public ArrayList<Bookmark> getBookmarks() {return _bookmarks;}

    public Database(Articles.DBConnection connection, LDAPInfo.DBInfo dbInfo) throws RecognitionException, IOException, JSONException
    {
        _connection = connection;
        _dbInfo = dbInfo;

        String allAccessRights = "";
        for (int i=0; i<dbInfo._groups.size(); i++)
        {
            String rights = dbInfo._groups.get(i)._accessRights;
            if (rights != null)
            {
                if (allAccessRights.isEmpty()) allAccessRights = rights;
                else allAccessRights += ";" + rights;
            }
        }
        if (dbInfo._accessRights != null)
        {
            if (allAccessRights.isEmpty()) allAccessRights = dbInfo._accessRights;
            else allAccessRights += ";" + dbInfo._accessRights;
        }

        _accessRights = compileRuleSet(allAccessRights);

        loadRuleSets();
        loadBookmarks();
    }

    public interface ArticleFunc
    {
        public boolean process(ArticleItem article);
    }
    public boolean enumArticles(ArticleFunc func)
    {
        for (ArticleItem article : _articles.values())
        {
            if (func.process(article)) return true;
        }
        return false;
    }
    public ArticleItem getArticle(int id)
    {
        return _articles.get(id);
    }
    public String getContent(ArticleItem article) throws IOException
    {
        return _connection.response(toAbsolute(article._url), "GET", null);
    }
    public String getName()
    {
        return _dbInfo._name;
    }
    public String getUser()
    {
        return _connection.getUID();
    }
    public ArrayList<LDAPInfo.GroupInfo> getGroups()
    {
        return _dbInfo._groups;
    }
    public ArrayList<Token> getAccessRights() {return _accessRights;}
    public ArrayList<RuleSet> getRuleSets() {return _ruleSets;}

    public void update() throws IOException, JSONException
    {
       _articles.clear();

        String response = _connection.response(_dbInfo._server + "DB/Articles/" + _connection.getUID() + "/", "GET", null);
        if (response == null) return;

        JSONTokener reader = new JSONTokener(response);
        JSONObject root = new JSONObject(reader);
        JSONArray items = root.getJSONArray("Articles");
        for (int j=0; j<items.length(); j++)
        {
            JSONObject item = items.getJSONObject(j);

            ArticleItem article = new ArticleItem();
            article._id = item.optInt("ID", -1);
            article._title = item.optString("Title");
            article._url = item.optString("URL");
            article._parent = item.optInt("Parent", -1);
            article._author = item.optString("Author");
            String date = item.optString("Date");
            try
            {
                article._date = _inputDateFormat.parse(date);
            }
            catch (ParseException ex)
            {
                article._date = new java.util.Date(0);
            }
            article._revision = item.optInt("Revision", -1);
            article._majorRevision = item.optInt("MajorRevision", -1);
            _articles.put(article._id, article);
        }

        items = root.getJSONArray("Tags");
        for (int j=0; j<items.length(); j++)
        {
            JSONObject item = items.getJSONObject(j);

            int id = item.optInt("ID", -1);
            String tag = item.optString("Tag").trim();
            if (!tag.isEmpty())
            {
                ArticleItem article = _articles.get(id);
                if (article != null) article.addTag(tag.toLowerCase());
            }
        }

        items = root.getJSONArray("PrivateTags");
        for (int j=0; j<items.length(); j++)
        {
            JSONObject item = items.getJSONObject(j);

            int id = item.optInt("ID", -1);
            String tag = item.optString("Tag").trim();
            if (!tag.isEmpty())
            {
                ArticleItem article = _articles.get(id);
                if (article != null) article.addTag(ArticleItem.toPrivateTag(tag.toLowerCase()));
            }
        }

        items = root.getJSONArray("Read");
        for (int j=0; j<items.length(); j++)
        {
            JSONObject item = items.getJSONObject(j);

            int id = item.optInt("ID", -1);
            int revision = item.optInt("Revision", -1);
            ArticleItem article = _articles.get(id);
            if (article != null)
            {
                article._toRead = revision < article._majorRevision;
                article._changed = revision < article._revision;
            }
        }

        items = root.getJSONArray("Collapsed");
        for (int j=0; j<items.length(); j++)
        {
            int id = items.optInt(j, -1);
            ArticleItem article = _articles.get(id);
            if (article != null) article._expanded = false;
        }

        updateChildren();
    }

    private String toAbsolute(String url) 
    {
        if (_dbInfo._svnRoot == null || _dbInfo._svnRoot.isEmpty()) return url;
        try 
        {
            URL baseURL = new URL(_dbInfo._svnRoot);
            URL absoluteURL = new URL(baseURL, url);
            return absoluteURL.toString();
        }
        catch (MalformedURLException ex) 
        {
            return url;
        }
    }

    private void updateChildren()
    {
        // Build lists of children
        for (ArticleItem article : _articles.values())
        {
            ArticleItem parent = _articles.get(article._parent);
            if (parent != null)
            {
                if (parent._children == null) parent._children = new ArrayList<Integer>();
                parent._children.add(article._id);
            }
        }

        // Deduce inherited tags
        for (ArticleItem article : _articles.values())
        {
            if (article._parent < 0) addInheritedTags(article, null);
        }
    }
    private void addInheritedTags(ArticleItem article, ArrayList<String> tags)
    {
        // update list of inherited tags
        article._inheritedTags = tags;
        ArrayList<String> inheritedTags = article.inheritsTags();
        // apply inherited tags to the current article
        article.updateEffectiveTags();

        // recurse
        if (article._children != null)
        {
            for (int id : article._children)
            {
                ArticleItem child = _articles.get(id);
                if (child != null) addInheritedTags(child, inheritedTags);
            }
        }
    }

    private void loadBookmarks() throws IOException, JSONException
    {
        _bookmarks.clear();

        String response = _connection.response(_dbInfo._server + "DB/Bookmarks/" + _connection.getUID() + "/", "GET", null);
        if (response == null) return;

        JSONTokener reader = new JSONTokener(response);
        JSONArray items = new JSONArray(reader);
        for (int j=0; j<items.length(); j++)
        {
            JSONObject item = items.getJSONObject(j);

            Bookmark bookmark = new Bookmark(this);
            bookmark._id = item.optInt("ID", -1);
            bookmark._name = item.optString("Name");
            bookmark._tags = ArticleItem.readTags(item.optString("Tags")).toArray(new String[0]);
            bookmark._articleId = item.optInt("Article", -1);
            bookmark._image = item.optInt("Image", 0);
            bookmark._parent = item.optInt("Parent", -1);
            _bookmarks.add(bookmark);
        }
    }

    private void loadRuleSets() throws IOException, JSONException
    {
        _ruleSets.clear();

        String response = _connection.response(_dbInfo._server + "DB/RuleSets/" + _connection.getUID() + "/", "GET", null);
        if (response == null) return;

        JSONTokener reader = new JSONTokener(response);
        JSONArray items = new JSONArray(reader);
        for (int j=0; j<items.length(); j++)
        {
            JSONObject item = items.getJSONObject(j);

            RuleSet ruleSet = new RuleSet();
            ruleSet._user = item.optString("User");
            ruleSet._name = item.optString("Name");
            ruleSet._expression = item.optString("Expression");
            ruleSet._parentUser = item.optString("ParentUser");
            ruleSet._parentName = item.optString("ParentName");
            _ruleSets.add(ruleSet);
        }
    }
    public RuleSet findRuleSet(String user, String name)
    {
        for (RuleSet ruleSet : _ruleSets)
        {
            if (ruleSet._user.equalsIgnoreCase(user) &&
                ruleSet._name.equalsIgnoreCase(name)) return ruleSet;
        }
        return null;
    }

    public static ArrayList<Token> compileRuleSet(String ruleSet) throws RecognitionException
    {
        ColaboRuleSetLexer lexer = new ColaboRuleSetLexer(new ANTLRStringStream(ruleSet));
        ColaboRuleSetParser parser = new ColaboRuleSetParser(new CommonTokenStream(lexer));

        parser.ruleSet();

        if (parser.failed()) return null;
        return new ArrayList<Token>(parser._result);
    }
}
