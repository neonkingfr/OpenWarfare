// $ANTLR 3.2 Sep 23, 2009 12:02:23 W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g 2011-07-13 15:35:04

package com.bistudio.colabo;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class ColaboRuleSetParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "OR", "AND", "NOT", "LPAR", "RPAR", "GT", "LT", "GE", "LE", "EQ", "NE", "IMPL", "END", "IDENTIFIER", "MASK", "WHITESPACE", "CHAR", "ESC_SEQ", "UNICODE_ESC", "HEXADECIMAL_ESC", "HEX_DIGIT"
    };
    public static final int GE=11;
    public static final int LT=10;
    public static final int UNICODE_ESC=22;
    public static final int HEXADECIMAL_ESC=23;
    public static final int CHAR=20;
    public static final int HEX_DIGIT=24;
    public static final int WHITESPACE=19;
    public static final int NOT=6;
    public static final int AND=5;
    public static final int EOF=-1;
    public static final int ESC_SEQ=21;
    public static final int MASK=18;
    public static final int LPAR=7;
    public static final int IDENTIFIER=17;
    public static final int OR=4;
    public static final int GT=9;
    public static final int RPAR=8;
    public static final int EQ=13;
    public static final int IMPL=15;
    public static final int END=16;
    public static final int LE=12;
    public static final int NE=14;

    // delegates
    // delegators


        public ColaboRuleSetParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public ColaboRuleSetParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return ColaboRuleSetParser.tokenNames; }
    public String getGrammarFileName() { return "W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g"; }


    public ArrayList<Token> _result;



    // $ANTLR start "ruleSet"
    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:36:1: ruleSet : ( EOF | rule ( END rule )* );
    public final void ruleSet() throws RecognitionException {

        _result = new ArrayList<Token>();

        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:41:2: ( EOF | rule ( END rule )* )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==EOF) ) {
                alt2=1;
            }
            else if ( ((LA2_0>=NOT && LA2_0<=LPAR)||(LA2_0>=IDENTIFIER && LA2_0<=MASK)) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:41:10: EOF
                    {
                    match(input,EOF,FOLLOW_EOF_in_ruleSet158); 

                    }
                    break;
                case 2 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:41:16: rule ( END rule )*
                    {
                    pushFollow(FOLLOW_rule_in_ruleSet162);
                    rule();

                    state._fsp--;

                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:41:21: ( END rule )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==END) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:41:22: END rule
                    	    {
                    	    match(input,END,FOLLOW_END_in_ruleSet165); 
                    	    pushFollow(FOLLOW_rule_in_ruleSet167);
                    	    rule();

                    	    state._fsp--;


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "ruleSet"


    // $ANTLR start "rule"
    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:42:1: rule : expression op= IMPL action ;
    public final void rule() throws RecognitionException {
        Token op=null;

        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:43:2: ( expression op= IMPL action )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:43:4: expression op= IMPL action
            {
            pushFollow(FOLLOW_expression_in_rule177);
            expression();

            state._fsp--;

            op=(Token)match(input,IMPL,FOLLOW_IMPL_in_rule183); 
            pushFollow(FOLLOW_action_in_rule185);
            action();

            state._fsp--;

            _result.add(op);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "rule"


    // $ANTLR start "expression"
    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:44:1: expression : term (op= OR term )* ;
    public final void expression() throws RecognitionException {
        Token op=null;

        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:45:2: ( term (op= OR term )* )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:45:4: term (op= OR term )*
            {
            pushFollow(FOLLOW_term_in_expression195);
            term();

            state._fsp--;

            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:45:9: (op= OR term )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==OR) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:45:10: op= OR term
            	    {
            	    op=(Token)match(input,OR,FOLLOW_OR_in_expression202); 
            	    pushFollow(FOLLOW_term_in_expression204);
            	    term();

            	    state._fsp--;

            	    _result.add(op);

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "expression"


    // $ANTLR start "term"
    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:46:1: term : factor (op= AND factor )* ;
    public final void term() throws RecognitionException {
        Token op=null;

        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:47:2: ( factor (op= AND factor )* )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:47:4: factor (op= AND factor )*
            {
            pushFollow(FOLLOW_factor_in_term216);
            factor();

            state._fsp--;

            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:47:11: (op= AND factor )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==AND) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:47:12: op= AND factor
            	    {
            	    op=(Token)match(input,AND,FOLLOW_AND_in_term223); 
            	    pushFollow(FOLLOW_factor_in_term225);
            	    factor();

            	    state._fsp--;

            	    _result.add(op);

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "term"


    // $ANTLR start "factor"
    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:48:1: factor : ( ( mask_or_id | comparison | ( LPAR expression RPAR ) ) | (op= NOT ( mask_or_id | comparison | ( LPAR expression RPAR ) ) ) );
    public final void factor() throws RecognitionException {
        Token op=null;

        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:49:2: ( ( mask_or_id | comparison | ( LPAR expression RPAR ) ) | (op= NOT ( mask_or_id | comparison | ( LPAR expression RPAR ) ) ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==LPAR||(LA7_0>=IDENTIFIER && LA7_0<=MASK)) ) {
                alt7=1;
            }
            else if ( (LA7_0==NOT) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:49:4: ( mask_or_id | comparison | ( LPAR expression RPAR ) )
                    {
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:49:4: ( mask_or_id | comparison | ( LPAR expression RPAR ) )
                    int alt5=3;
                    int LA5_0 = input.LA(1);

                    if ( ((LA5_0>=IDENTIFIER && LA5_0<=MASK)) ) {
                        int LA5_1 = input.LA(2);

                        if ( ((LA5_1>=OR && LA5_1<=AND)||LA5_1==RPAR||LA5_1==IMPL) ) {
                            alt5=1;
                        }
                        else if ( ((LA5_1>=GT && LA5_1<=NE)) ) {
                            alt5=2;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 5, 1, input);

                            throw nvae;
                        }
                    }
                    else if ( (LA5_0==LPAR) ) {
                        alt5=3;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 5, 0, input);

                        throw nvae;
                    }
                    switch (alt5) {
                        case 1 :
                            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:49:5: mask_or_id
                            {
                            pushFollow(FOLLOW_mask_or_id_in_factor238);
                            mask_or_id();

                            state._fsp--;


                            }
                            break;
                        case 2 :
                            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:49:18: comparison
                            {
                            pushFollow(FOLLOW_comparison_in_factor242);
                            comparison();

                            state._fsp--;


                            }
                            break;
                        case 3 :
                            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:49:31: ( LPAR expression RPAR )
                            {
                            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:49:31: ( LPAR expression RPAR )
                            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:49:32: LPAR expression RPAR
                            {
                            match(input,LPAR,FOLLOW_LPAR_in_factor247); 
                            pushFollow(FOLLOW_expression_in_factor249);
                            expression();

                            state._fsp--;

                            match(input,RPAR,FOLLOW_RPAR_in_factor251); 

                            }


                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:50:3: (op= NOT ( mask_or_id | comparison | ( LPAR expression RPAR ) ) )
                    {
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:50:3: (op= NOT ( mask_or_id | comparison | ( LPAR expression RPAR ) ) )
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:50:4: op= NOT ( mask_or_id | comparison | ( LPAR expression RPAR ) )
                    {
                    op=(Token)match(input,NOT,FOLLOW_NOT_in_factor264); 
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:50:13: ( mask_or_id | comparison | ( LPAR expression RPAR ) )
                    int alt6=3;
                    int LA6_0 = input.LA(1);

                    if ( ((LA6_0>=IDENTIFIER && LA6_0<=MASK)) ) {
                        int LA6_1 = input.LA(2);

                        if ( ((LA6_1>=GT && LA6_1<=NE)) ) {
                            alt6=2;
                        }
                        else if ( ((LA6_1>=OR && LA6_1<=AND)||LA6_1==RPAR||LA6_1==IMPL) ) {
                            alt6=1;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 6, 1, input);

                            throw nvae;
                        }
                    }
                    else if ( (LA6_0==LPAR) ) {
                        alt6=3;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 6, 0, input);

                        throw nvae;
                    }
                    switch (alt6) {
                        case 1 :
                            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:50:14: mask_or_id
                            {
                            pushFollow(FOLLOW_mask_or_id_in_factor267);
                            mask_or_id();

                            state._fsp--;


                            }
                            break;
                        case 2 :
                            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:50:27: comparison
                            {
                            pushFollow(FOLLOW_comparison_in_factor271);
                            comparison();

                            state._fsp--;


                            }
                            break;
                        case 3 :
                            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:50:40: ( LPAR expression RPAR )
                            {
                            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:50:40: ( LPAR expression RPAR )
                            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:50:41: LPAR expression RPAR
                            {
                            match(input,LPAR,FOLLOW_LPAR_in_factor276); 
                            pushFollow(FOLLOW_expression_in_factor278);
                            expression();

                            state._fsp--;

                            match(input,RPAR,FOLLOW_RPAR_in_factor280); 

                            }


                            }
                            break;

                    }


                    }

                    _result.add(op);

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "factor"


    // $ANTLR start "comparison"
    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:51:1: comparison : mask_or_id op= ( GT | LT | GE | LE | EQ | NE ) id ;
    public final void comparison() throws RecognitionException {
        Token op=null;

        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:52:2: ( mask_or_id op= ( GT | LT | GE | LE | EQ | NE ) id )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:52:4: mask_or_id op= ( GT | LT | GE | LE | EQ | NE ) id
            {
            pushFollow(FOLLOW_mask_or_id_in_comparison293);
            mask_or_id();

            state._fsp--;

            op=(Token)input.LT(1);
            if ( (input.LA(1)>=GT && input.LA(1)<=NE) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }

            pushFollow(FOLLOW_id_in_comparison323);
            id();

            state._fsp--;

            _result.add(op);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "comparison"


    // $ANTLR start "action"
    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:53:1: action : val= IDENTIFIER ;
    public final void action() throws RecognitionException {
        Token val=null;

        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:54:2: (val= IDENTIFIER )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:54:4: val= IDENTIFIER
            {
            val=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_action337); 
            _result.add(val);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "action"


    // $ANTLR start "mask_or_id"
    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:55:1: mask_or_id : val= ( MASK | IDENTIFIER ) ;
    public final void mask_or_id() throws RecognitionException {
        Token val=null;

        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:56:2: (val= ( MASK | IDENTIFIER ) )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:56:4: val= ( MASK | IDENTIFIER )
            {
            val=(Token)input.LT(1);
            if ( (input.LA(1)>=IDENTIFIER && input.LA(1)<=MASK) ) {
                input.consume();
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }

            _result.add(val);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "mask_or_id"


    // $ANTLR start "id"
    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:57:1: id : val= IDENTIFIER ;
    public final void id() throws RecognitionException {
        Token val=null;

        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:58:2: (val= IDENTIFIER )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:58:4: val= IDENTIFIER
            {
            val=(Token)match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_id372); 
            _result.add(val);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "id"

    // Delegated rules


 

    public static final BitSet FOLLOW_EOF_in_ruleSet158 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule_in_ruleSet162 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_END_in_ruleSet165 = new BitSet(new long[]{0x00000000000600C0L});
    public static final BitSet FOLLOW_rule_in_ruleSet167 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_expression_in_rule177 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_IMPL_in_rule183 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_action_in_rule185 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_term_in_expression195 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_OR_in_expression202 = new BitSet(new long[]{0x00000000000600C0L});
    public static final BitSet FOLLOW_term_in_expression204 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_factor_in_term216 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_AND_in_term223 = new BitSet(new long[]{0x00000000000600C0L});
    public static final BitSet FOLLOW_factor_in_term225 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_mask_or_id_in_factor238 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_comparison_in_factor242 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAR_in_factor247 = new BitSet(new long[]{0x00000000000600C0L});
    public static final BitSet FOLLOW_expression_in_factor249 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_RPAR_in_factor251 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NOT_in_factor264 = new BitSet(new long[]{0x0000000000060080L});
    public static final BitSet FOLLOW_mask_or_id_in_factor267 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_comparison_in_factor271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAR_in_factor276 = new BitSet(new long[]{0x00000000000600C0L});
    public static final BitSet FOLLOW_expression_in_factor278 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_RPAR_in_factor280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_mask_or_id_in_comparison293 = new BitSet(new long[]{0x0000000000007E00L});
    public static final BitSet FOLLOW_set_in_comparison299 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_id_in_comparison323 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_action337 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_mask_or_id351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IDENTIFIER_in_id372 = new BitSet(new long[]{0x0000000000000002L});

}