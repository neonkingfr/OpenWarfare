/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bistudio.colabo;

import com.unboundid.ldap.sdk.BindResult;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPSearchException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import javax.faces.context.FacesContext;
import javax.net.SocketFactory;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jirka
 */
public class LDAPInfo 
{
    public String _host = "of.bistudio.com";
    public int _port = 636;
    public String _userDN = "uid=?,ou=users,o=of";
    public String _password = "";
    public boolean _remember = false;

    public String _uid = "";
    public String _name = "";
    public String _picture = "";
    public int _color = 0;

    public String getHost() {return _host;}
    public void setHost(String value) {_host = value;}
    public int getPort() {return _port;}
    public void setPort(int value) {_port = value;}
    public String getUserDN() {return _userDN;}
    public void setUserDN(String value) {_userDN = value;}
    public String getPassword() {return _password;}
    public void setPassword(String value) {_password = value;}
    public boolean isRemember() {return _remember;}
    public void setRemember(boolean value) {_remember = value;}

    public class GroupInfo
    {
        public String _name;
        public String _accessRights;
        public int _level;
        public int _homePage;
    };
    public class DBInfo
    {
        public String _name;
        public String _server;
        public String _svn;
        public String _svnRoot;
        public String _accessRights;
        public ArrayList<GroupInfo> _groups = new ArrayList<GroupInfo>();
    };
    public ArrayList<DBInfo> _dbInfo = new ArrayList<DBInfo>();

    public LDAPInfo()
    {
        loadCookies();
    }

    public String login(SocketFactory socketFactory) throws LDAPException
    {
        _dbInfo.clear();

        LDAPConnectionOptions ops = new LDAPConnectionOptions();
        ops.setAutoReconnect(true);
        ops.setBindWithDNRequiresPassword(true);
        ops.setResponseTimeoutMillis(0);
        ops.setUseKeepAlive(true);
        ops.setUseTCPNoDelay(true);

        LDAPConnection c = null;
        if (socketFactory != null && _port == 636)
            c = new LDAPConnection(socketFactory, ops);
        else
            c = new LDAPConnection(ops);

        c.connect(_host, _port);
        BindResult ok = c.bind(_userDN, _password);
        if (ok.getResultCode() != ResultCode.SUCCESS) return "LDAP Login<br/>Login failed";

        // access the user profile
        SearchResult result = c.search(_userDN, SearchScope.BASE, "(objectClass=*)",
                "uid", "displayName", "colaboUserPicture", "colaboUserColor", "colaboDBDN");
        if (result.getEntryCount() == 0) return "LDAP Login<br/>User DN not found";
        SearchResultEntry entry = result.getSearchEntries().get(0);

        String value = entry.getAttributeValue("uid");
        if (value == null || value.isEmpty()) return "LDAP Login<br/>User UID not found";
        _uid = value;

        // valid info, save it if requested
        if (_remember) saveCookies();

        value = entry.getAttributeValue("displayName");
        if (value != null && !value.isEmpty()) _name = value;

        value = entry.getAttributeValue("colaboUserPicture");
        if (value != null && !value.isEmpty()) _picture = value;

        value = entry.getAttributeValue("colaboUserColor");
        if (value != null && !value.isEmpty()) _color = Integer.parseInt(value);

        // list of signed databases
        String[] dbNames = entry.getAttributeValues("colaboDBDN");
        for (int i=0; i<dbNames.length; i++)
        {
            SearchResult dbResult = c.search(dbNames[i], SearchScope.BASE, "(objectClass=*)",
                    "cn", "colaboDBServerType", "colaboDBServer", "colaboSVNPath", "colaboSVNRoot");
            if (dbResult.getEntryCount() == 0) continue;
            SearchResultEntry dbEntry = dbResult.getSearchEntries().get(0);

            // read the database properties
            // load only Web Services based databases
            value = dbEntry.getAttributeValue("colaboDBServerType");
            if (!"STWebServices".equals(value)) continue;

            DBInfo dbInfo = new DBInfo();
            _dbInfo.add(dbInfo);

            value = dbEntry.getAttributeValue("cn");
            if (value != null && !value.isEmpty()) dbInfo._name = value;

            value = dbEntry.getAttributeValue("colaboDBServer");
            if (value != null && !value.isEmpty()) dbInfo._server = value;

            value = dbEntry.getAttributeValue("colaboSVNPath");
            if (value != null && !value.isEmpty()) dbInfo._svn = value;

            value = dbEntry.getAttributeValue("colaboSVNRoot");
            if (value != null && !value.isEmpty()) dbInfo._svnRoot = value;

            SearchResult userResult = c.search("ou=users," + dbNames[i], SearchScope.SUB,
                    "(&(objectClass=colaboUser)(colaboUserDN=" + _uid + "))",
                    "colaboAccessRights");
            if (userResult.getEntryCount() > 0)
            {
                SearchResultEntry userEntry = userResult.getSearchEntries().get(0);
                value = userEntry.getAttributeValue("colaboAccessRights");
                if (value != null && !value.isEmpty()) dbInfo._accessRights = value;
            }

            SearchResult groupResult = c.search("ou=groups," + dbNames[i], SearchScope.SUB,
                "(objectClass=colaboGroup)", "cn", "colaboAccessRights", "colaboGroupLevel", "colaboHomePage",
                "memberUid", "member");
            for (int j=0; j<groupResult.getEntryCount(); j++)
            {
                SearchResultEntry groupEntry = groupResult.getSearchEntries().get(j);

                // check if user is member of this group
                String[] list = groupEntry.getAttributeValues("memberUid");
                boolean isMember = list != null && Arrays.asList(list).contains(_uid);
                if (!isMember)
                {
                    list = groupEntry.getAttributeValues("member");
                    isMember = list != null && isMember(_uid, list, c);
                }
                if (!isMember) continue;

                // read the group attributes
                GroupInfo group = new GroupInfo();
                dbInfo._groups.add(group);

                value = groupEntry.getAttributeValue("cn");
                if (value != null && !value.isEmpty()) group._name = value;

                value = groupEntry.getAttributeValue("colaboAccessRights");
                if (value != null && !value.isEmpty()) group._accessRights = value;

                value = groupEntry.getAttributeValue("colaboGroupLevel");
                if (value != null && !value.isEmpty()) group._level = Integer.parseInt(value);

                value = groupEntry.getAttributeValue("colaboHomePage");
                if (value != null && !value.isEmpty()) group._homePage = Integer.parseInt(value);
            }
            Collections.sort(dbInfo._groups, new Comparator<GroupInfo>()
            {
                public int compare(GroupInfo group1, GroupInfo group2)
                {
                    int diff = group1._level - group2._level;
                    if (diff != 0) return diff;
                    return group1._name.compareTo(group2._name);
                }
            });
        }
        return null; // OK
    }
    private boolean isMember(String uid, String[] members, LDAPConnection c)
    {
        for (int i=0; i<members.length; i++)
        {
            if (isMember(uid, members[i], c)) return true;
        }
        return false;
    }
    private boolean isMember(String uid, String member, LDAPConnection c)
    {
        // access the entry with the given dn
        SearchResult result = null;
        try
        {
            result = c.search(member, SearchScope.BASE, "(objectClass=*)", "uid", "member");
        }
        catch (LDAPSearchException ex)
        {
            // continue with the processing of other permissions
            return false;
        }
        if (result.getEntryCount() == 0) return false;

        SearchResultEntry entry = result.getSearchEntries().get(0);
        // check if this is user entry with given uid
        String value = entry.getAttributeValue("uid");
        if (uid.equals(value)) {
            return true;
        }
        String[] list = entry.getAttributeValues("member");
        return list != null && isMember(uid, list, c);
    }

    private void loadCookies()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
        Cookie cookies[] = request.getCookies();
        if (cookies != null) for (Cookie cookie : cookies)
        {
            String name = cookie.getName();
            if (name.equals("LDAPHost")) _host = cookie.getValue();
            else if (name.equals("LDAPPort")) _port = Integer.parseInt(cookie.getValue());
            else if (name.equals("LDAPUser")) _userDN = cookie.getValue();
            else if (name.equals("LDAPPassword")) _password = cookie.getValue();
            else if (name.equals("LDAPRemember")) _remember = Boolean.parseBoolean(cookie.getValue());
        }
    }
    private void saveCookies()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
        addCookie(response, "LDAPHost", _host);
        addCookie(response, "LDAPPort", Integer.toString(_port));
        addCookie(response, "LDAPUser", _userDN);
        addCookie(response, "LDAPPassword", _password);
        addCookie(response, "LDAPRemember", Boolean.toString(_remember));
    }
    private void addCookie(HttpServletResponse response, String name, String value)
    {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(Integer.MAX_VALUE);
        response.addCookie(cookie);
    }
};
