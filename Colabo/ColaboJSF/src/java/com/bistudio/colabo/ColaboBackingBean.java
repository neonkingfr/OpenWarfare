/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.colabo;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.richfaces.component.UIExtendedDataTable;


/**
 *
 * @author Jirka
 */
@ManagedBean(name = "colabo_backing")
@RequestScoped
public class ColaboBackingBean implements Serializable
{
    private UIExtendedDataTable _table;
    public UIExtendedDataTable getTable() {return _table;}
    public void setTable(UIExtendedDataTable table) {_table = table;}
    
    public ColaboBackingBean()
    {
    }
}
