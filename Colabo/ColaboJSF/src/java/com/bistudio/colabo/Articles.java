/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bistudio.colabo;

import com.bistudio.colabo.Database.RuleSet;
import com.bistudio.colabo.LDAPInfo.DBInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import javax.faces.model.DataModel;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import org.antlr.runtime.*;
import org.codehaus.jettison.json.JSONException;
import sun.misc.BASE64Encoder;


        // Create index of all tags, compute geometric average and maximum of counts
/**
 *
 * @author jirka
 */
public class Articles extends DataModel<Articles.IndexItem>
{
    // unique overall identification of the article
    public class ArticleId
    {
        // database owner
        public Database _database;
        // ID of the article in the database
        public int _id;
    }
    /// index item - info about the article really visible in the articles list
    public class IndexItem
    {
        // database owner
        public Database _database;
        // article itself
        public ArticleItem _article;
        // how far is the article from the root
        public int _level;
        // parent id (null for no parent)
        public IndexItem _parent;
        // my date or the latest date of all my children recursively
        public java.util.Date _latest;
        // my sort order or the highest of all my children recursively
        public int _sortOrder;
        // visible item, the source for DataGridView (added into _index)
        public boolean _visible;
        // some children are visible
        public boolean _hasChildren;

        public IndexItem(Database database, ArticleItem article, int level, IndexItem parent)
        {
            _database = database;
            _article = article;
            _level = level;
            _parent = parent;
            _latest = new java.util.Date(0); // will be set later
            _sortOrder = 0; // will be set later
            _visible = false;
            _hasChildren = false;
        }

        public ArticleItem getArticle() {return _article;}
        public int getIndent() {return _level;}
        public String getStyle()
        {
            // collect the actions of the article and collapsed subtree
            ArrayList<String> actions = _article.getActions(_currentRuleSet, _database);
            ArrayList<String> subtreeActions = new ArrayList<String>();
            if (!_article._expanded) _article.collectSubtreeActions(subtreeActions, _currentRuleSet, _database); // collect actions from children

            // change font by the rule set
            String style = "";
            if (actions.contains("bold") || subtreeActions.contains("bold")) style += "font-weight:bold; ";
            else style += "font-weight:normal; ";
            if (actions.contains("italic") || subtreeActions.contains("italic")) style += "font-style:italic; ";
            else style += "font-style:normal; ";
            if (actions.contains("strikeout"))
            {
                if (actions.contains("underline") || subtreeActions.contains("underline")) style += "text-decoration:underline line-through; ";
                else style += "text-decoration:line-through; ";
            }
            else
            {
                if (actions.contains("underline") || subtreeActions.contains("underline")) style += "text-decoration:underline; ";
                else style += "text-decoration:none; ";
            }

            final String prefixColor = "color:";
            for (int i=0; i<actions.size(); i++)
            {
                String action = actions.get(i);
                if (action.startsWith(prefixColor)) return style + action;
            }
            return style + "color:black";
        }
        public ArrayList<String> getIcons()
        {
            ArrayList<String> icons = new ArrayList<String>();

            // collect the actions of the article and collapsed subtree
            ArrayList<String> actions = _article.getActions(_currentRuleSet, _database);
            ArrayList<String> subtreeActions = new ArrayList<String>();
            if (!_article._expanded) _article.collectSubtreeActions(subtreeActions, _currentRuleSet, _database); // collect actions from children

            final String prefixIcon = "icon:";
            for (int i=actions.size()-1; i>=0; i--)
            {
                String action = actions.get(i);
                if (action.startsWith(prefixIcon))
                {
                    String iconName = action.substring(prefixIcon.length()) + ".ico";
                    if (!icons.contains(iconName)) icons.add(iconName);
                }
            }
            for (int i=subtreeActions.size()-1; i>=0; i--)
            {
                String action = subtreeActions.get(i);
                if (action.startsWith(prefixIcon))
                {
                    String iconName = action.substring(prefixIcon.length()) + ".ico";
                    if (!icons.contains(iconName)) icons.add(iconName);
                }
            }

            return icons;
        }
        public boolean isLeaf() {return !_hasChildren;}
        public boolean isExpanded() {return _article._expanded;}

        public void onExpanded() throws IOException
        {
            if (_hasChildren)
            {
                _article._expanded = !_article._expanded;
                createIndexVisible();
            }
        }
    }

    /// index of the cache - all articles which can be shown
    private ArrayList<IndexItem> _index = new ArrayList<IndexItem>();
    /// index of the cache - all articles which are shown now (expanded)
    private ArrayList<IndexItem> _indexVisible = new ArrayList<IndexItem>();

    private Collection<Object> _selection = null;
    public Collection<Object> getSelected() {return _selection;}
    public void setSelected(Collection<Object> value) {_selection = value;}

    // content of the selected article
    private String _currentArticleContent = null;
    public String getCurrentArticleContent() {return _currentArticleContent;}

    private ArrayList<Database> _databases = new ArrayList<Database>();

    private ArrayList<Token> _currentRuleSet = null;

    private enum Column {Icon, Subject, Author, Date};
    private Column _sortBy = Column.Icon;
    private enum SortOrder {Ascending, Descending};
    private SortOrder _sortOrder = SortOrder.Descending;

    // DataModel implementation
    private int _currentRow = -1;
    @Override
    public boolean isRowAvailable() 
    {
        return _currentRow >= 0 && _currentRow < getRowCount();
    }
    @Override
    public int getRowCount()
    {
        return _indexVisible.size();
    }
    @Override
    public IndexItem getRowData()
    {
        if (_currentRow < 0) return null;
        return _indexVisible.get(_currentRow);
    }
    @Override
    public int getRowIndex() 
    {
        return _currentRow;
    }
    @Override
    public void setRowIndex(int rowIndex) 
    {
        _currentRow = rowIndex;
    }
    @Override
    public Object getWrappedData() 
    {
        return null;
    }
    @Override
    public void setWrappedData(Object data) 
    {
    }

    public class DBConnection
    {
        private SSLSocketFactory _socketFactory = null;
        private String _uid = null;
        private String _authHeader = null;

        public DBConnection(SSLSocketFactory socketFactory, String uid, String password)
        {
            _socketFactory = socketFactory;
            _uid = uid;

            String auth = uid + ":" + password;
            BASE64Encoder encoder = new BASE64Encoder();
            _authHeader = "Basic " + encoder.encode(auth.getBytes());
        }

        public String getUID() {return _uid;}

        /// Helper to request Web Services and return the response
        public String response(String addr, String method, String data) throws IOException
        {
            Socket socket = null;
            try
            {
                URL url = new URL(addr);
                int port = url.getPort();
                if (port < 0) port = url.getDefaultPort();
                socket = _socketFactory.createSocket(url.getHost(), port);
                ((SSLSocket)socket).setEnabledProtocols(new String[] {"SSLv3"});

                Writer out = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
                out.write(method + " " + url.toString() + " HTTP/1.0\r\n");
                out.write("Host: " + url.getHost() + ":" + port + "\r\n");
                out.write("Authorization: " + _authHeader + "\r\n");
                out.write("CONTENT-TYPE: application/json\r\n");
                out.write("Connection: close\r\n");
                out.write("\r\n");
                if (data != null) out.write(data);
                out.flush();

                BufferedReader in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream(), "UTF-8"));
                boolean body = false;
                String content = "";
                String line;
                while ((line = in.readLine()) != null)
                {
                    if (body) content += line + "\r\n";
                    if (line.isEmpty()) body = true; // header is closed by an empty line
                }

                return content;
            }
            finally
            {
                if (socket != null)
                {
                    try {socket.close();} catch (IOException ex) {}
                }
            }
        }
    }

    public Articles()
    {
    }

    public void init(LDAPInfo ldapInfo, SSLSocketFactory socketFactory) throws RecognitionException, IOException, JSONException
    {
        DBConnection connection = new DBConnection(socketFactory, ldapInfo._uid, ldapInfo._password);

        // create databases
        _databases.clear();
        for (int i=0; i<ldapInfo._dbInfo.size(); i++)
        {
            DBInfo info = ldapInfo._dbInfo.get(i);
            try
            {
                _databases.add(new Database(connection, info));
            }
            catch (Throwable ex)
            {
                ex.printStackTrace();
            }
        }

        selectRuleSet();
    }
    private void selectRuleSet() throws RecognitionException
    {
        // simplified rule set selection
        // TODO: improve
        for (Database db : _databases)
        {
            ArrayList<RuleSet> ruleSets = db.getRuleSets();
            if (!ruleSets.isEmpty())
            {
                RuleSet ruleSet = ruleSets.get(0);

                _currentRuleSet = new ArrayList<Token>();
                while (ruleSet != null)
                {
                    ArrayList<Token> list = Database.compileRuleSet(ruleSet._expression);
                    if (list != null) _currentRuleSet.addAll(list);

                    // find the parent rule set
                    ruleSet = db.findRuleSet(ruleSet._parentUser, ruleSet._parentName);
                }
                return;
            }
        }
    }

    public ArrayList<Bookmark> getRootBookmarks()
    {
        ArrayList<Bookmark> bookmarks = new ArrayList<Bookmark>();
        bookmarks.addAll(Arrays.asList(Bookmark.DefaultBookmarks));

        for (Database db : _databases)
        {
            String name = db.getName();
            String id = Colabo._tagDb + name;

            Bookmark bookmark = new Bookmark(db);
            bookmark._name = name;
            bookmark._image = 0;
            bookmark._tags = new String[] {id};
            bookmarks.add(bookmark);
        }

        return bookmarks;
    }
    public void processBookmark(Bookmark bookmark) throws IOException
    {
        // tags filtering
        if (bookmark._tags != null)
        {
            _tags.clear();
            for (String tag : bookmark._tags)
            {
                if (tag == null || tag.isEmpty()) continue;
                if (tag.charAt(0) == '!') _tags.add(new TagCondition(tag.substring(1), false));
                else _tags.add(new TagCondition(tag, true));
            }
            createIndex();
            updateTags();
            createIndexVisible();
        }

        if (bookmark._database != null && bookmark._articleId >= 0) goToArticle(bookmark._database, bookmark._articleId);
    }
    private void goToArticle(Database database, int articleId) throws IOException
    {
        ArticleItem article = database.getArticle(articleId);
        if (article == null) return;
        if (!article.getAccessRights(database).contains(ArticleItem.AccessRights.Read)) return;

        // check if article is already visible
        if (selectArticle(article)) return;

        // check if the article is in collapsed subtree
        if (article.isEnabled(_tags, _currentRuleSet, database))
        {
            // find the article in the list of all enabled articles
            for (int i=0; i<_index.size(); i++)
            {
                IndexItem item = _index.get(i);
                if (item._article == article)
                {
                    while (item._parent != null)
                    {
                        item = item._parent;
                        item._article._expanded = true;
                    }
                    createIndex();
                    updateTags();
                    createIndexVisible();
                    selectArticle(article);
                    return;
                }
            }
        }

        // article is not enabled, add temporarily article to the view
        IndexItem index = new IndexItem(database, article, 0, null);
        _indexVisible.add(index);
        selectArticle(article);
    }
    private boolean selectArticle(ArticleItem article) throws IOException
    {
        for (int i=0; i<_indexVisible.size(); i++)
        {
            IndexItem item = _indexVisible.get(i);
            if (item._article == article)
            {
                ArrayList<Object> selection = new ArrayList<Object>();
                selection.add(new Integer(i));
                setSelected(selection);
/*
                FacesContext context = FacesContext.getCurrentInstance();
                UIComponent component = context.getViewRoot().findComponent("articles");
*/
                selectionChanged();
                return true;
            }
        }
        return false;
    }

    public void update() throws IOException, JSONException
    {
        // update the cache
        for (Database db : _databases) db.update();
        // build the index
        createIndex();
        updateTags();
        createIndexVisible();
    }
    private void createIndex()
    {
        _index.clear();
        for (final Database db : _databases)
        {
            db.enumArticles(new Database.ArticleFunc()
            {
                public boolean process(ArticleItem article)
                {
                    if (article._parent < 0) addToIndex(db, article, 0, null);
                    return false; // continue
                }
                // recursive articles tree creation
                private IndexItem addToIndex(Database db, ArticleItem article, int level, IndexItem parent)
                {
                    IndexItem item = null;
                    final String prefixSortOrder = "sortorder:";
                    if (article.getAccessRights(db).contains(ArticleItem.AccessRights.Read))
                    {
                        // optimization - call GetActions once for both IsEnabled and sort order
                        ArrayList<String> actions = article.getActions(_currentRuleSet, db);
                        
                        if (!actions.contains("hide") && article.isEnabled(_tags, db))
                        {
                            item = new IndexItem(db, article, level, parent);
                            item._latest = article._date; // initialize the date of the latest article in the subtree

                            // find the first SORTORDER action
                            item._sortOrder = 0;
                            for (int i=0; i<actions.size(); i++)
                            {
                                String action = actions.get(i);
                                if (action.startsWith(prefixSortOrder))
                                {
                                    item._sortOrder = Integer.parseInt(action.substring(prefixSortOrder.length()));
                                    break;
                                }
                            }

                            _index.add(item);
                            // move the parent
                            level++;
                            parent = item;
                        }
                    }

                    // recursively add child articles
                    if (article._children != null)
                    {
                        for (int id : article._children)
                        {
                            ArticleItem childArticle = db.getArticle(id);
                            if (childArticle != null)
                            {
                                IndexItem childItem = addToIndex(db, childArticle, level, parent);
                                if (childItem != null && item != null)
                                {
                                    // update the subtree properties
                                    item._hasChildren = true;
                                    if (childItem._latest.after(item._latest)) item._latest = childItem._latest;
                                    if (childItem._sortOrder > item._sortOrder) item._sortOrder = childItem._sortOrder;
                                }
                            }
                        }
                    }

                    return item;
                }
            });
        }
        
        Class comparerType = null;
        switch (_sortBy)
        {
            case Subject:
                comparerType = TitleComparer.class; break;
            case Author:
                comparerType = AuthorComparer.class; break;
            case Date:
                comparerType = DateComparer.class; break;
            default:
                comparerType = CustomComparer.class; break;
        }
        boolean reveresed = _sortOrder == SortOrder.Descending;
        try
        {
            Comparator<IndexItem> comparator = (Comparator<IndexItem>)comparerType.getConstructor(boolean.class).newInstance(reveresed);
            Collections.sort(_index, new IndexComparer(comparator));
        }
        catch (Exception ex)
        {
            // handle internal error
        }
    }
    private void createIndexVisible() throws IOException
    {
        // find what article is currently selected
        IndexItem selected = null;
        if (_selection != null) for (Object key : _selection)
        {
            int index = ((Integer)key).intValue();
            IndexItem articleId = getArticleId(index);
            if (articleId != null)
            {
                selected = articleId;
                break;
            }
        }
        // create the index of visible articles
        _indexVisible.clear();
        for (int i=0; i<_index.size(); i++)
        {
            IndexItem index = _index.get(i);
            if (index._parent == null || (index._parent._visible && index._parent._article._expanded))
            {
                index._visible = true;
                _indexVisible.add(index);
            }
            else index._visible = false;
        }
        // try to keep previously selected article
        while (selected != null)
        {
            // find the selected article (or its predecessor) in the list
            int index = _indexVisible.indexOf(selected);
            if (index >= 0)
            {
                ArrayList<Object> selection = new ArrayList<Object>();
                selection.add(new Integer(index));
                setSelected(selection);
                break;
            }
            // move to the parent
            selected = selected._parent;
        }

        // the current article could change
        selectionChanged();
    }
    public String getSortedByCustom()
    {
        return getSortedBy(Column.Icon);
    }
    public String getSortedByTitle()
    {
        return getSortedBy(Column.Subject);
    }
    public String getSortedByAuthor()
    {
        return getSortedBy(Column.Author);
    }
    public String getSortedByDate()
    {
        return getSortedBy(Column.Date);
    }
    private String getSortedBy(Column column)
    {
        if (_sortBy != column) return "none";
        if (_sortOrder == SortOrder.Ascending) return "ascending";
        return "descending";
    }
    public void sortByCustom() throws IOException
    {
        sortBy(Column.Icon);
    }
    public void sortByTitle() throws IOException
    {
        sortBy(Column.Subject);
    }
    public void sortByAuthor() throws IOException
    {
        sortBy(Column.Author);
    }
    public void sortByDate() throws IOException
    {
        sortBy(Column.Date);
    }
    private void sortBy(Column column) throws IOException
    {
        if (_sortBy == column)
        {
            if (_sortOrder == SortOrder.Ascending) _sortOrder = SortOrder.Descending;
            else _sortOrder = SortOrder.Ascending;
        }
        else
        {
            _sortBy = column;
            _sortOrder = SortOrder.Ascending;
        }

        createIndex();
        createIndexVisible();
    }

    private static class TitleComparer implements Comparator<IndexItem>
    {
        boolean _reversed;
        public TitleComparer(boolean reversed)
        {
            _reversed = reversed;
        }
        public int compare(IndexItem item1, IndexItem item2)
        {
            int diff = item1._article._title.compareTo(item2._article._title);
            return _reversed ? -diff : diff;
        }
    }
    private static class AuthorComparer implements Comparator<IndexItem>
    {
        boolean _reversed;
        public AuthorComparer(boolean reversed)
        {
            _reversed = reversed;
        }
        public int compare(IndexItem item1, IndexItem item2)
        {
            int diff = item1._article._author.compareTo(item2._article._author);
            return _reversed ? -diff : diff;
        }
    }
    private static class DateComparer implements Comparator<IndexItem>
    {
        boolean _reversed;
        public DateComparer(boolean reversed)
        {
            _reversed = reversed;
        }
        public int compare(IndexItem item1, IndexItem item2)
        {
            if (item1._level > 0) // not root items should be always sorted the same way (new down)
                return (item1._latest.compareTo(item2._latest));
            else
            {
                int diff = item1._latest.compareTo(item2._latest);
                return _reversed ? -diff : diff;
            }
        }
    }
    private static class CustomComparer implements Comparator<IndexItem>
    {
        boolean _reversed;
        public CustomComparer(boolean reversed)
        {
            _reversed = reversed;
        }
        public int compare(IndexItem item1, IndexItem item2)
        {
            if (item1._level > 0) // not root items should be always sorted the same way (new down)
              return (item1._latest.compareTo(item2._latest));
            else
            {
              // sort threads primary by sort order, secondary by date
              int diff = item1._sortOrder - item2._sortOrder;
              if (diff != 0) return _reversed ? -diff : diff;
              diff = item1._latest.compareTo(item2._latest);
              return _reversed ? -diff : diff;
            }
        }
    }
    private static class IndexComparer implements Comparator<IndexItem>
    {
        private Comparator<IndexItem> _baseComparer;
        public IndexComparer(Comparator<IndexItem> baseComparer)
        {
            _baseComparer = baseComparer;
        }
        public int compare(IndexItem item1, IndexItem item2)
        {
            while (item1._parent != item2._parent)
            {
                if (item1._level > item2._level)
                {
                    if (item1._parent == item2) return 1; //'a' must be after 'b', parents always first, children should obey
                    item1 = item1._parent;
                }
                else if (item2._level > item1._level)
                {
                    if (item2._parent == item1) return -1;
                    item2 = item2._parent;
                }
                else //must be equal
                {
                    item1 = item1._parent;
                    item2 = item2._parent;
                }
            }
            int retVal = _baseComparer.compare(item1, item2);
            if (retVal != 0) return retVal;

            // compare by unique id
            // Note: using unique sorting is necessary! Without it, the Sort does not work (intransitive ordering)
            // Example:
            //    * 1
            //        2 Node A
            //    * 1
            //        3 Node B
            //        4 Node C
            // Comparing by numbers we have: NodeB<NodeC, but NodeB==NodeA and NodeC==NodeA implies NodeB==NodeC which is contradiction!

            retVal = item1._database.getName().compareTo(item2._database.getName());
            if (retVal != 0) return retVal;

            return item1._article._id - item2._article._id;
        }
    }

    public String loadArticle(IndexItem index) throws IOException
    {
        String content = index._database.getContent(index._article);
        // article formatting
        return content.replaceAll("\n", "<br/>");
    }
    public IndexItem getArticleId(int row)
    {
        return _indexVisible.get(row);
    }

    // Tags filter
    public class TagCondition
    {
        public String _tag;
        public boolean _present;

        public TagCondition(String tag, boolean present)
        {
            _tag = tag; _present = present;
        }
    }
    private ArrayList<TagCondition> _tags = new ArrayList<TagCondition>();
    public ArrayList<TagCondition> getTags() {return _tags;}

    public void addCurrentTag(String tag) throws IOException
    {
        TagCondition cond = new TagCondition(tag, true);
        if (!_tags.contains(cond))
        {
            _tags.add(cond);
            // update the index
            createIndex();
            updateTags();
            createIndexVisible();
        }
    }
    void splitOffTags(int index) throws IOException
    {
        _tags.subList(index, _tags.size()).clear();
        // update the index
        createIndex();
        updateTags();
        createIndexVisible();
    }

    // Tags cloud
    public static class TagsStatsItem
    {
        public int _total;
        public int _unread;
        public java.util.Date _newest;

        // Stats for nothing.
        public TagsStatsItem() { _total = 0; _unread = 0; _newest = new java.util.Date(0); }

        // Stats for a single article.
        public TagsStatsItem(ArticleItem item) { _total = 1; _unread = item._toRead ? 1 : 0; _newest = item._date; }

        // Combine two statistics.
        public void combineWith(TagsStatsItem item)
        {
            _total += item._total;
            _unread += item._unread;
            if (_newest.before(item._newest)) _newest = item._newest;
        }
    };
    private Object[] _allTags = null;
    public Object[] getAllTags() {return _allTags;}
    private double _avgTagsCount = 0;
    public double getTagsAvgCount() {return _avgTagsCount;}
    private int _maxTagsCount = 0;
    public int getTagsMaxCount() {return _maxTagsCount;}

    // Update the navigation through tags
    private void updateTags()
    {
        // Summarize the tags from articles not filtered out
        HashMap<String, TagsStatsItem> tagStats = new HashMap<String, TagsStatsItem>();

        // tagStats is summarized from the _indexEnabled list of articles
        // no need to check all articles in each databases cache.Values
        for (int i=0; i<_index.size(); i++)
        {
            IndexItem item = _index.get(i);
            ArticleItem article = item._article;
            Database database = item._database;

            // real tags
            if (article._effectiveTags != null)
            {
                for (String tag : item._article._effectiveTags)
                {
                    if (tag.length() >= 1 && tag.charAt(0) == ArticleItem.TagSpecialChars.TagAttribute) continue;
                    if (tag.length() >= 2 &&
                        tag.charAt(0) == ArticleItem.TagSpecialChars.TagPrivate &&
                        tag.charAt(1) == ArticleItem.TagSpecialChars.TagAttribute) continue;
                    updateStats(tagStats, tag, article);
                }
            }

            // virtual tags
            String dbTag = Colabo._tagDb + database.getName();
            updateStats(tagStats, dbTag, article);  // current database
            if (article._toRead) updateStats(tagStats, Colabo._tagToRead, article);
            if (article._changed) updateStats(tagStats, Colabo._tagChanged, article);
            if (article._author.equalsIgnoreCase(database.getUser())) updateStats(tagStats, Colabo._tagByMe, article);
            if (article.isAssignedToMe(database)) updateStats(tagStats, Colabo._tagToMe, article);
            // if (!string.IsNullOrEmpty(item._draft)) updateStats(tagStats, _tagDraft, item);
            if (article.isToday()) updateStats(tagStats, Colabo._tagToday, article);
            if (article.isThisWeek()) updateStats(tagStats, Colabo._tagThisWeek, article);
            if (article.isThisMonth()) updateStats(tagStats, Colabo._tagThisMonth, article);
            if (article.isLastMonth()) updateStats(tagStats, Colabo._tagLastMonth, article);
        }

        Set<Entry<String, TagsStatsItem>> entrySet = tagStats.entrySet();
        _allTags = entrySet.toArray();
        _avgTagsCount = 0;
        _maxTagsCount = 0;

        if (_allTags.length > 0)
        {
            double sumLogCount = 0;
            for (Object pair : _allTags)
            {
                Entry<String, TagsStatsItem> entry = (Entry<String, TagsStatsItem>)pair;
                int count = entry.getValue()._total;
                if (count > _maxTagsCount) _maxTagsCount = count;
                sumLogCount += Math.log(count);
            }
            _avgTagsCount = Math.exp(sumLogCount / _allTags.length);
        }
        Arrays.sort(_allTags, new Comparator<Object>()
        {
            public int compare(Object pair1, Object pair2)
            {
                Entry<String, TagsStatsItem> entry1 = (Entry<String, TagsStatsItem>)pair1;
                Entry<String, TagsStatsItem> entry2 = (Entry<String, TagsStatsItem>)pair2;
                return entry1.getKey().compareTo(entry2.getKey());
            }
        });
    }
    private void updateStats(HashMap<String, TagsStatsItem> map, String tag, ArticleItem article)
    {
        if (!_tags.contains(new TagCondition(tag, true)))  // hide tags in current selection
        {
            TagsStatsItem stats = new TagsStatsItem(article);
            if (!map.containsKey(tag)) map.put(tag, stats);
            else map.get(tag).combineWith(stats);
        }
    }

    public void selectionChanged() throws IOException
    {
        if (_selection == null)
        {
            _currentArticleContent = "No article selected";
        }
        else for (Object key : _selection)
        {
            int index = ((Integer)key).intValue();
            _currentArticleContent = loadArticle(getArticleId(index));
            return;
        }
    }

    public void onSelected() throws IOException
    {
        selectionChanged();
    }
};
