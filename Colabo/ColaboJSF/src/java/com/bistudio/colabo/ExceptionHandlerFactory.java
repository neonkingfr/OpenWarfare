/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.colabo;

import java.util.Iterator;
import javax.faces.application.NavigationHandler;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

/**
 *
 * @author Jirka
 */
public class ExceptionHandlerFactory extends javax.faces.context.ExceptionHandlerFactory
{
    private javax.faces.context.ExceptionHandlerFactory _parent;

    public ExceptionHandlerFactory(javax.faces.context.ExceptionHandlerFactory parent)
    {
        _parent = parent;
    }

    @Override
    public ExceptionHandler getExceptionHandler()
    {
        ExceptionHandler oldHandler = _parent.getExceptionHandler();
        return new CustomExceptionHandler(oldHandler);
    }

    private static class CustomExceptionHandler extends ExceptionHandlerWrapper
    {
        private ExceptionHandler _parent;

        public CustomExceptionHandler(ExceptionHandler parent)
        {
            _parent = parent;
        }

        @Override
        public ExceptionHandler getWrapped()
        {
            return _parent;
        }

        @Override
        public void handle()
        {
            for (Iterator<ExceptionQueuedEvent> i=getUnhandledExceptionQueuedEvents().iterator(); i.hasNext();)
            {
                ExceptionQueuedEvent e = i.next();
                ExceptionQueuedEventContext context = (ExceptionQueuedEventContext)e.getSource();
                Throwable ex = context.getException();
                if (ex instanceof ViewExpiredException)
                {
                    try
                    {
                        FacesContext fc = FacesContext.getCurrentInstance();
                        fc.getExternalContext().getFlash().put("expired", true);
                        NavigationHandler nh = fc.getApplication().getNavigationHandler();
                        nh.handleNavigation(fc, null, "/PageLogin?faces-redirect=true");
                        fc.renderResponse();
                    }
                    finally
                    {
                        i.remove();
                    }
                }
            }
            // let the parent handle other events
            getWrapped().handle();
        }
    }
}
