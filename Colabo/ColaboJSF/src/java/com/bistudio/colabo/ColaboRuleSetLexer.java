// $ANTLR 3.2 Sep 23, 2009 12:02:23 W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g 2011-07-13 15:35:04

package com.bistudio.colabo;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class ColaboRuleSetLexer extends Lexer {
    public static final int GE=11;
    public static final int LT=10;
    public static final int UNICODE_ESC=22;
    public static final int HEXADECIMAL_ESC=23;
    public static final int CHAR=20;
    public static final int HEX_DIGIT=24;
    public static final int WHITESPACE=19;
    public static final int NOT=6;
    public static final int AND=5;
    public static final int EOF=-1;
    public static final int ESC_SEQ=21;
    public static final int MASK=18;
    public static final int LPAR=7;
    public static final int IDENTIFIER=17;
    public static final int OR=4;
    public static final int GT=9;
    public static final int RPAR=8;
    public static final int IMPL=15;
    public static final int EQ=13;
    public static final int END=16;
    public static final int LE=12;
    public static final int NE=14;

    // delegates
    // delegators

    public ColaboRuleSetLexer() {;} 
    public ColaboRuleSetLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public ColaboRuleSetLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g"; }

    // $ANTLR start "OR"
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:7:4: ( '|' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:7:6: '|'
            {
            match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "AND"
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:8:5: ( '&' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:8:7: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "NOT"
    public final void mNOT() throws RecognitionException {
        try {
            int _type = NOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:9:5: ( '!' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:9:7: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "NOT"

    // $ANTLR start "LPAR"
    public final void mLPAR() throws RecognitionException {
        try {
            int _type = LPAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:10:6: ( '(' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:10:8: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LPAR"

    // $ANTLR start "RPAR"
    public final void mRPAR() throws RecognitionException {
        try {
            int _type = RPAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:11:6: ( ')' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:11:8: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RPAR"

    // $ANTLR start "GT"
    public final void mGT() throws RecognitionException {
        try {
            int _type = GT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:12:4: ( '>' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:12:6: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "LT"
    public final void mLT() throws RecognitionException {
        try {
            int _type = LT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:13:4: ( '<' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:13:6: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LT"

    // $ANTLR start "GE"
    public final void mGE() throws RecognitionException {
        try {
            int _type = GE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:14:4: ( '>=' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:14:6: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "GE"

    // $ANTLR start "LE"
    public final void mLE() throws RecognitionException {
        try {
            int _type = LE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:15:4: ( '<=' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:15:6: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LE"

    // $ANTLR start "EQ"
    public final void mEQ() throws RecognitionException {
        try {
            int _type = EQ;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:16:4: ( '==' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:16:6: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "EQ"

    // $ANTLR start "NE"
    public final void mNE() throws RecognitionException {
        try {
            int _type = NE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:17:4: ( '!=' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:17:6: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "NE"

    // $ANTLR start "IMPL"
    public final void mIMPL() throws RecognitionException {
        try {
            int _type = IMPL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:18:6: ( '=>' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:18:8: '=>'
            {
            match("=>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "IMPL"

    // $ANTLR start "END"
    public final void mEND() throws RecognitionException {
        try {
            int _type = END;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:19:5: ( ';' )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:19:7: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "END"

    // $ANTLR start "WHITESPACE"
    public final void mWHITESPACE() throws RecognitionException {
        try {
            int _type = WHITESPACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:62:2: ( ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' )+ )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:62:4: ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' )+
            {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:62:4: ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='\t' && LA1_0<='\n')||(LA1_0>='\f' && LA1_0<='\r')||LA1_0==' ') ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||(input.LA(1)>='\f' && input.LA(1)<='\r')||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);

            _channel = HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WHITESPACE"

    // $ANTLR start "IDENTIFIER"
    public final void mIDENTIFIER() throws RecognitionException {
        try {
            int _type = IDENTIFIER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:65:2: ( ( CHAR )+ | ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' | '*' | '?' ) )* '\"' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0>='\u0000' && LA4_0<='\b')||LA4_0=='\u000B'||(LA4_0>='\u000E' && LA4_0<='\u001F')||(LA4_0>='#' && LA4_0<='%')||(LA4_0>='-' && LA4_0<=':')||(LA4_0>='@' && LA4_0<='[')||(LA4_0>=']' && LA4_0<='{')||(LA4_0>='}' && LA4_0<='\uFFFF')) ) {
                alt4=1;
            }
            else if ( (LA4_0=='\"') ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:65:4: ( CHAR )+
                    {
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:65:4: ( CHAR )+
                    int cnt2=0;
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( ((LA2_0>='\u0000' && LA2_0<='\b')||LA2_0=='\u000B'||(LA2_0>='\u000E' && LA2_0<='\u001F')||(LA2_0>='#' && LA2_0<='%')||(LA2_0>='-' && LA2_0<=':')||(LA2_0>='@' && LA2_0<='[')||(LA2_0>=']' && LA2_0<='{')||(LA2_0>='}' && LA2_0<='\uFFFF')) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:65:4: CHAR
                    	    {
                    	    mCHAR(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt2 >= 1 ) break loop2;
                                EarlyExitException eee =
                                    new EarlyExitException(2, input);
                                throw eee;
                        }
                        cnt2++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:65:12: ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' | '*' | '?' ) )* '\"' )
                    {
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:65:12: ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' | '*' | '?' ) )* '\"' )
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:65:13: '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' | '*' | '?' ) )* '\"'
                    {
                    match('\"'); 
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:65:17: ( ESC_SEQ | ~ ( '\\\\' | '\"' | '*' | '?' ) )*
                    loop3:
                    do {
                        int alt3=3;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0=='\\') ) {
                            alt3=1;
                        }
                        else if ( ((LA3_0>='\u0000' && LA3_0<='!')||(LA3_0>='#' && LA3_0<=')')||(LA3_0>='+' && LA3_0<='>')||(LA3_0>='@' && LA3_0<='[')||(LA3_0>=']' && LA3_0<='\uFFFF')) ) {
                            alt3=2;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:65:18: ESC_SEQ
                    	    {
                    	    mESC_SEQ(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:65:28: ~ ( '\\\\' | '\"' | '*' | '?' )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<=')')||(input.LA(1)>='+' && input.LA(1)<='>')||(input.LA(1)>='@' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    match('\"'); 

                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "IDENTIFIER"

    // $ANTLR start "MASK"
    public final void mMASK() throws RecognitionException {
        try {
            int _type = MASK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:68:2: ( ( CHAR | '*' | '?' )+ | ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )* '\"' ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0>='\u0000' && LA7_0<='\b')||LA7_0=='\u000B'||(LA7_0>='\u000E' && LA7_0<='\u001F')||(LA7_0>='#' && LA7_0<='%')||LA7_0=='*'||(LA7_0>='-' && LA7_0<=':')||(LA7_0>='?' && LA7_0<='[')||(LA7_0>=']' && LA7_0<='{')||(LA7_0>='}' && LA7_0<='\uFFFF')) ) {
                alt7=1;
            }
            else if ( (LA7_0=='\"') ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:68:4: ( CHAR | '*' | '?' )+
                    {
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:68:4: ( CHAR | '*' | '?' )+
                    int cnt5=0;
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( ((LA5_0>='\u0000' && LA5_0<='\b')||LA5_0=='\u000B'||(LA5_0>='\u000E' && LA5_0<='\u001F')||(LA5_0>='#' && LA5_0<='%')||LA5_0=='*'||(LA5_0>='-' && LA5_0<=':')||(LA5_0>='?' && LA5_0<='[')||(LA5_0>=']' && LA5_0<='{')||(LA5_0>='}' && LA5_0<='\uFFFF')) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\b')||input.LA(1)=='\u000B'||(input.LA(1)>='\u000E' && input.LA(1)<='\u001F')||(input.LA(1)>='#' && input.LA(1)<='%')||input.LA(1)=='*'||(input.LA(1)>='-' && input.LA(1)<=':')||(input.LA(1)>='?' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='{')||(input.LA(1)>='}' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt5 >= 1 ) break loop5;
                                EarlyExitException eee =
                                    new EarlyExitException(5, input);
                                throw eee;
                        }
                        cnt5++;
                    } while (true);


                    }
                    break;
                case 2 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:68:26: ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )* '\"' )
                    {
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:68:26: ( '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )* '\"' )
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:68:27: '\"' ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )* '\"'
                    {
                    match('\"'); 
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:68:31: ( ESC_SEQ | ~ ( '\\\\' | '\"' ) )*
                    loop6:
                    do {
                        int alt6=3;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0=='\\') ) {
                            alt6=1;
                        }
                        else if ( ((LA6_0>='\u0000' && LA6_0<='!')||(LA6_0>='#' && LA6_0<='[')||(LA6_0>=']' && LA6_0<='\uFFFF')) ) {
                            alt6=2;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:68:32: ESC_SEQ
                    	    {
                    	    mESC_SEQ(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:68:42: ~ ( '\\\\' | '\"' )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    match('\"'); 

                    }


                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "MASK"

    // $ANTLR start "CHAR"
    public final void mCHAR() throws RecognitionException {
        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:71:14: (~ ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' | '\\'' | '\\\\' | '\\\"' | ';' | '<' | '>' | '=' | '&' | '|' | '!' | '(' | ')' | '+' | '*' | '?' | ',' ) )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:71:17: ~ ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' | '\\'' | '\\\\' | '\\\"' | ';' | '<' | '>' | '=' | '&' | '|' | '!' | '(' | ')' | '+' | '*' | '?' | ',' )
            {
            if ( (input.LA(1)>='\u0000' && input.LA(1)<='\b')||input.LA(1)=='\u000B'||(input.LA(1)>='\u000E' && input.LA(1)<='\u001F')||(input.LA(1)>='#' && input.LA(1)<='%')||(input.LA(1)>='-' && input.LA(1)<=':')||(input.LA(1)>='@' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='{')||(input.LA(1)>='}' && input.LA(1)<='\uFFFF') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "CHAR"

    // $ANTLR start "ESC_SEQ"
    public final void mESC_SEQ() throws RecognitionException {
        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:76:6: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' | '0' ) | UNICODE_ESC | HEXADECIMAL_ESC )
            int alt8=3;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='\\') ) {
                switch ( input.LA(2) ) {
                case '\"':
                case '\'':
                case '0':
                case '\\':
                case 'b':
                case 'f':
                case 'n':
                case 'r':
                case 't':
                    {
                    alt8=1;
                    }
                    break;
                case 'u':
                    {
                    alt8=2;
                    }
                    break;
                case 'x':
                    {
                    alt8=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae;
                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:76:10: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' | '0' )
                    {
                    match('\\'); 
                    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='0'||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||input.LA(1)=='t' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}


                    }
                    break;
                case 2 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:77:10: UNICODE_ESC
                    {
                    mUNICODE_ESC(); 

                    }
                    break;
                case 3 :
                    // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:78:10: HEXADECIMAL_ESC
                    {
                    mHEXADECIMAL_ESC(); 

                    }
                    break;

            }
        }
        finally {
        }
    }
    // $ANTLR end "ESC_SEQ"

    // $ANTLR start "HEXADECIMAL_ESC"
    public final void mHEXADECIMAL_ESC() throws RecognitionException {
        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:81:6: ( '\\\\' 'x' HEX_DIGIT HEX_DIGIT )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:81:10: '\\\\' 'x' HEX_DIGIT HEX_DIGIT
            {
            match('\\'); 
            match('x'); 
            mHEX_DIGIT(); 
            mHEX_DIGIT(); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "HEXADECIMAL_ESC"

    // $ANTLR start "UNICODE_ESC"
    public final void mUNICODE_ESC() throws RecognitionException {
        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:84:6: ( '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:84:10: '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
            {
            match('\\'); 
            match('u'); 
            mHEX_DIGIT(); 
            mHEX_DIGIT(); 
            mHEX_DIGIT(); 
            mHEX_DIGIT(); 

            }

        }
        finally {
        }
    }
    // $ANTLR end "UNICODE_ESC"

    // $ANTLR start "HEX_DIGIT"
    public final void mHEX_DIGIT() throws RecognitionException {
        try {
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:87:2: ( ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
            // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:87:4: ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='F')||(input.LA(1)>='a' && input.LA(1)<='f') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "HEX_DIGIT"

    public void mTokens() throws RecognitionException {
        // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:8: ( OR | AND | NOT | LPAR | RPAR | GT | LT | GE | LE | EQ | NE | IMPL | END | WHITESPACE | IDENTIFIER | MASK )
        int alt9=16;
        alt9 = dfa9.predict(input);
        switch (alt9) {
            case 1 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:10: OR
                {
                mOR(); 

                }
                break;
            case 2 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:13: AND
                {
                mAND(); 

                }
                break;
            case 3 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:17: NOT
                {
                mNOT(); 

                }
                break;
            case 4 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:21: LPAR
                {
                mLPAR(); 

                }
                break;
            case 5 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:26: RPAR
                {
                mRPAR(); 

                }
                break;
            case 6 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:31: GT
                {
                mGT(); 

                }
                break;
            case 7 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:34: LT
                {
                mLT(); 

                }
                break;
            case 8 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:37: GE
                {
                mGE(); 

                }
                break;
            case 9 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:40: LE
                {
                mLE(); 

                }
                break;
            case 10 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:43: EQ
                {
                mEQ(); 

                }
                break;
            case 11 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:46: NE
                {
                mNE(); 

                }
                break;
            case 12 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:49: IMPL
                {
                mIMPL(); 

                }
                break;
            case 13 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:54: END
                {
                mEND(); 

                }
                break;
            case 14 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:58: WHITESPACE
                {
                mWHITESPACE(); 

                }
                break;
            case 15 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:69: IDENTIFIER
                {
                mIDENTIFIER(); 

                }
                break;
            case 16 :
                // W:\\Colabo\\trunk\\src\\ColaboJSF\\src\\java\\com\\bistudio\\colabo\\ColaboRuleSet.g:1:80: MASK
                {
                mMASK(); 

                }
                break;

        }

    }


    protected DFA9 dfa9 = new DFA9(this);
    static final String DFA9_eotS =
        "\3\uffff\1\17\2\uffff\1\21\1\23\3\uffff\1\26\27\uffff";
    static final String DFA9_eofS =
        "\43\uffff";
    static final String DFA9_minS =
        "\1\0\2\uffff\1\75\2\uffff\3\75\2\uffff\2\0\12\uffff\1\42\1\0\1"+
        "\uffff\1\0\5\60\1\0\1\60\1\0";
    static final String DFA9_maxS =
        "\1\uffff\2\uffff\1\75\2\uffff\2\75\1\76\2\uffff\2\uffff\12\uffff"+
        "\1\170\1\uffff\1\uffff\1\uffff\5\146\1\uffff\1\146\1\uffff";
    static final String DFA9_acceptS =
        "\1\uffff\1\1\1\2\1\uffff\1\4\1\5\3\uffff\1\15\1\16\2\uffff\1\20"+
        "\1\13\1\3\1\10\1\6\1\11\1\7\1\12\1\14\1\17\2\uffff\1\17\11\uffff";
    static final String DFA9_specialS =
        "\1\4\12\uffff\1\0\1\6\13\uffff\1\5\1\uffff\1\2\5\uffff\1\1\1\uffff"+
        "\1\3}>";
    static final String[] DFA9_transitionS = {
            "\11\13\2\12\1\13\2\12\22\13\1\12\1\3\1\14\3\13\1\2\1\uffff"+
            "\1\4\1\5\1\15\2\uffff\16\13\1\11\1\7\1\10\1\6\1\15\34\13\1\uffff"+
            "\37\13\1\1\uff83\13",
            "",
            "",
            "\1\16",
            "",
            "",
            "\1\20",
            "\1\22",
            "\1\24\1\25",
            "",
            "",
            "\11\13\2\uffff\1\13\2\uffff\22\13\3\uffff\3\13\4\uffff\1\15"+
            "\2\uffff\16\13\4\uffff\1\15\34\13\1\uffff\37\13\1\uffff\uff83"+
            "\13",
            "\42\30\1\31\7\30\1\15\24\30\1\15\34\30\1\27\uffa3\30",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\32\4\uffff\1\32\10\uffff\1\32\53\uffff\1\32\5\uffff\1\32"+
            "\3\uffff\1\32\7\uffff\1\32\3\uffff\1\32\1\uffff\1\32\1\33\2"+
            "\uffff\1\34",
            "\42\30\1\31\7\30\1\15\24\30\1\15\34\30\1\27\uffa3\30",
            "",
            "\42\30\1\31\7\30\1\15\24\30\1\15\34\30\1\27\uffa3\30",
            "\12\35\7\uffff\6\35\32\uffff\6\35",
            "\12\36\7\uffff\6\36\32\uffff\6\36",
            "\12\37\7\uffff\6\37\32\uffff\6\37",
            "\12\40\7\uffff\6\40\32\uffff\6\40",
            "\12\41\7\uffff\6\41\32\uffff\6\41",
            "\42\30\1\31\7\30\1\15\24\30\1\15\34\30\1\27\uffa3\30",
            "\12\42\7\uffff\6\42\32\uffff\6\42",
            "\42\30\1\31\7\30\1\15\24\30\1\15\34\30\1\27\uffa3\30"
    };

    static final short[] DFA9_eot = DFA.unpackEncodedString(DFA9_eotS);
    static final short[] DFA9_eof = DFA.unpackEncodedString(DFA9_eofS);
    static final char[] DFA9_min = DFA.unpackEncodedStringToUnsignedChars(DFA9_minS);
    static final char[] DFA9_max = DFA.unpackEncodedStringToUnsignedChars(DFA9_maxS);
    static final short[] DFA9_accept = DFA.unpackEncodedString(DFA9_acceptS);
    static final short[] DFA9_special = DFA.unpackEncodedString(DFA9_specialS);
    static final short[][] DFA9_transition;

    static {
        int numStates = DFA9_transitionS.length;
        DFA9_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA9_transition[i] = DFA.unpackEncodedString(DFA9_transitionS[i]);
        }
    }

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = DFA9_eot;
            this.eof = DFA9_eof;
            this.min = DFA9_min;
            this.max = DFA9_max;
            this.accept = DFA9_accept;
            this.special = DFA9_special;
            this.transition = DFA9_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( OR | AND | NOT | LPAR | RPAR | GT | LT | GE | LE | EQ | NE | IMPL | END | WHITESPACE | IDENTIFIER | MASK );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA9_11 = input.LA(1);

                        s = -1;
                        if ( ((LA9_11>='\u0000' && LA9_11<='\b')||LA9_11=='\u000B'||(LA9_11>='\u000E' && LA9_11<='\u001F')||(LA9_11>='#' && LA9_11<='%')||(LA9_11>='-' && LA9_11<=':')||(LA9_11>='@' && LA9_11<='[')||(LA9_11>=']' && LA9_11<='{')||(LA9_11>='}' && LA9_11<='\uFFFF')) ) {s = 11;}

                        else if ( (LA9_11=='*'||LA9_11=='?') ) {s = 13;}

                        else s = 22;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA9_32 = input.LA(1);

                        s = -1;
                        if ( (LA9_32=='\"') ) {s = 25;}

                        else if ( (LA9_32=='\\') ) {s = 23;}

                        else if ( ((LA9_32>='\u0000' && LA9_32<='!')||(LA9_32>='#' && LA9_32<=')')||(LA9_32>='+' && LA9_32<='>')||(LA9_32>='@' && LA9_32<='[')||(LA9_32>=']' && LA9_32<='\uFFFF')) ) {s = 24;}

                        else if ( (LA9_32=='*'||LA9_32=='?') ) {s = 13;}

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA9_26 = input.LA(1);

                        s = -1;
                        if ( (LA9_26=='\"') ) {s = 25;}

                        else if ( (LA9_26=='\\') ) {s = 23;}

                        else if ( ((LA9_26>='\u0000' && LA9_26<='!')||(LA9_26>='#' && LA9_26<=')')||(LA9_26>='+' && LA9_26<='>')||(LA9_26>='@' && LA9_26<='[')||(LA9_26>=']' && LA9_26<='\uFFFF')) ) {s = 24;}

                        else if ( (LA9_26=='*'||LA9_26=='?') ) {s = 13;}

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA9_34 = input.LA(1);

                        s = -1;
                        if ( (LA9_34=='\"') ) {s = 25;}

                        else if ( (LA9_34=='\\') ) {s = 23;}

                        else if ( ((LA9_34>='\u0000' && LA9_34<='!')||(LA9_34>='#' && LA9_34<=')')||(LA9_34>='+' && LA9_34<='>')||(LA9_34>='@' && LA9_34<='[')||(LA9_34>=']' && LA9_34<='\uFFFF')) ) {s = 24;}

                        else if ( (LA9_34=='*'||LA9_34=='?') ) {s = 13;}

                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA9_0 = input.LA(1);

                        s = -1;
                        if ( (LA9_0=='|') ) {s = 1;}

                        else if ( (LA9_0=='&') ) {s = 2;}

                        else if ( (LA9_0=='!') ) {s = 3;}

                        else if ( (LA9_0=='(') ) {s = 4;}

                        else if ( (LA9_0==')') ) {s = 5;}

                        else if ( (LA9_0=='>') ) {s = 6;}

                        else if ( (LA9_0=='<') ) {s = 7;}

                        else if ( (LA9_0=='=') ) {s = 8;}

                        else if ( (LA9_0==';') ) {s = 9;}

                        else if ( ((LA9_0>='\t' && LA9_0<='\n')||(LA9_0>='\f' && LA9_0<='\r')||LA9_0==' ') ) {s = 10;}

                        else if ( ((LA9_0>='\u0000' && LA9_0<='\b')||LA9_0=='\u000B'||(LA9_0>='\u000E' && LA9_0<='\u001F')||(LA9_0>='#' && LA9_0<='%')||(LA9_0>='-' && LA9_0<=':')||(LA9_0>='@' && LA9_0<='[')||(LA9_0>=']' && LA9_0<='{')||(LA9_0>='}' && LA9_0<='\uFFFF')) ) {s = 11;}

                        else if ( (LA9_0=='\"') ) {s = 12;}

                        else if ( (LA9_0=='*'||LA9_0=='?') ) {s = 13;}

                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA9_24 = input.LA(1);

                        s = -1;
                        if ( (LA9_24=='\"') ) {s = 25;}

                        else if ( (LA9_24=='\\') ) {s = 23;}

                        else if ( ((LA9_24>='\u0000' && LA9_24<='!')||(LA9_24>='#' && LA9_24<=')')||(LA9_24>='+' && LA9_24<='>')||(LA9_24>='@' && LA9_24<='[')||(LA9_24>=']' && LA9_24<='\uFFFF')) ) {s = 24;}

                        else if ( (LA9_24=='*'||LA9_24=='?') ) {s = 13;}

                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA9_12 = input.LA(1);

                        s = -1;
                        if ( (LA9_12=='\\') ) {s = 23;}

                        else if ( ((LA9_12>='\u0000' && LA9_12<='!')||(LA9_12>='#' && LA9_12<=')')||(LA9_12>='+' && LA9_12<='>')||(LA9_12>='@' && LA9_12<='[')||(LA9_12>=']' && LA9_12<='\uFFFF')) ) {s = 24;}

                        else if ( (LA9_12=='\"') ) {s = 25;}

                        else if ( (LA9_12=='*'||LA9_12=='?') ) {s = 13;}

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 9, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}