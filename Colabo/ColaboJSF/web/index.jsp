<%-- 
    Document   : index
    Created on : 1.7.2011, 14:56:54
    Author     : Jirka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <title>Colabo</title>
    </head>
    <body>
        <f:view>
            <h1>Loading application...</h1>
            <h:form id="form">
                <h:commandButton style="visibility: hidden" id="redirect" action="redirect" immediate="true"/>
            </h:form>
            <script type="text/javascript">
                document.getElementById("form:redirect").click();
            </script>
        </f:view>
    </body>
</html>
