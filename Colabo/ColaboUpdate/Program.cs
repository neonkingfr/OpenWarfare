using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Diagnostics;

namespace ColaboUpdate
{
  class Program
  {
    static void Main(string[] args)
    {
      if (args.Length != 4) return;

      int processId;
      if (!int.TryParse(args[3], out processId)) return;

      Console.WriteLine("Updating Colabo...");

      Process process = null;
      try
      {
        process = Process.GetProcessById(processId);
        Console.WriteLine("Waiting for Colabo to exit");
      }
      catch (System.Exception e)
      {
      	
      }

      if (process != null) process.WaitForExit();
      Console.WriteLine("Colabo finished");

      string src = args[0].Trim();
      string exePath = args[1].Trim();
      string workingDir = args[2].Trim();
      string dst = Path.GetDirectoryName(exePath) + "\\";

      CopyDirectory(src, dst);

      // restart Colabo
      ProcessStartInfo startInfo = new ProcessStartInfo(exePath);
      startInfo.WorkingDirectory = workingDir;
      Process.Start(startInfo);
    }

    // recursive copy of the files
    private static void CopyDirectory(string src, string dst)
    {
      if (!Directory.Exists(dst)) Directory.CreateDirectory(dst);
      foreach (string file in Directory.GetFiles(src))
      {
        Console.WriteLine("File {0} ...", file);
        bool ok = false;
        while (!ok)
        {
          try
          {
            FileInfo info = new FileInfo(file);
            info.CopyTo(dst + info.Name, true);
            Console.WriteLine(" - done");
            ok = true;
          }
          catch (System.Exception e)
          {
            Console.WriteLine(" - copy failed ({0})", e.Message);
          }
        }
      }
      foreach (string dir in Directory.GetDirectories(src))
      {
        DirectoryInfo info = new DirectoryInfo(dir);
        if (info.Name != ".svn")
        {
          CopyDirectory(dir + "\\", dst + info.Name + "\\");
        }
      }
    }
  }
}
