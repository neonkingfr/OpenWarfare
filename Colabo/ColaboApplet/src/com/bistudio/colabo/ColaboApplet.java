/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ColaboApplet.java
 *
 * Created on 23.6.2011, 15:05:00
 */

package com.bistudio.colabo;

import com.unboundid.ldap.sdk.BindResult;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPSearchException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.swing.AbstractListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Jirka
 */
public class ColaboApplet extends java.applet.Applet
{
    public class LDAPInfo
    {
        public String _host = "ldap.bistudio.com";
        public int _port = 636;
        public String _userDN = "uid=*,ou=users,o=bohemiainteractive";
        public String _password = "";

        public String _uid = "";
        public String _name = "";
        public String _picture = "";
        public int _color = 0;
    };
    private LDAPInfo _ldap = new LDAPInfo();

    public class GroupInfo
    {
        public String _name;
        public String _accessRights;
        public int _level;
        public int _homePage;
    };
    public class DBInfo
    {
        public String _name;
        public String _server;
        public String _svn;
        public String _accessRights;
        public ArrayList<GroupInfo> _groups = new ArrayList<GroupInfo>();
    };
    private ArrayList<DBInfo> _dbInfo = new ArrayList<DBInfo>();

    private Articles _articles = new Articles();

    private SocketFactory _socketFactory = null;

    /** Initializes the applet ColaboApplet */
    @Override
    public void init()
    {
        // read the certificate to enable SSL connection
        InputStream stream = null;
        try
        {
            stream = ColaboApplet.class.getResourceAsStream("trusted_ca.jks");

            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(stream, "changeit".toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance("PKIX");
            tmf.init(ks);

            // SSLContext sslcx = SSLContext.getInstance("TLS");
            SSLContext sslcx = SSLContext.getInstance("SSLv3");
            sslcx.init(null, tmf.getTrustManagers(), new SecureRandom());

            _socketFactory = sslcx.getSocketFactory();
        }
        catch (Exception ex)
        {
            handleException("ColaboApplet.init", ex);
        }
        finally
        {
            if (stream != null)
            {
                try
                {
                    stream.close();
                }
                catch (IOException ex) {}
            }
        }

        try
        {
            java.awt.EventQueue.invokeAndWait(new Runnable()
            {
                public void run() {
                    initComponents();

                    com.unboundid.util.Debug.getLogger();
                    // com.unboundid.util.Debug.initialize();

                    // set default ldap settings
                    host.setText(_ldap._host);
                    port.setValue(_ldap._port);
                    userDN.setText(_ldap._userDN);
                    password.setText(_ldap._password);
                    // invoke modal login dialog
                    dialogLDAPLogin.setVisible(true);
                    // read ldap settings
                    _ldap._host = host.getText();
                    _ldap._port = ((Integer) port.getValue()).intValue();
                    _ldap._userDN = userDN.getText();
                    _ldap._password = new String(password.getPassword());
                    if (_socketFactory == null) {
                        _socketFactory = SSLSocketFactory.getDefault();
                    }
                    loadLDAPSettings();
                    _articles.init(_ldap, _dbInfo, _socketFactory);
                    loadArticles();
                }
            });
        } 
        catch (InvocationTargetException ex)
        {
            Throwable ex2 = ex.getTargetException();
            handleException("ColaboApplet.init", ex2 == null ? ex : ex2);
        }
        catch (InterruptedException ex)
        {
            handleException("ColaboApplet.init", ex);
        }
    }

    private boolean loadLDAPSettings()
    {
        LDAPConnectionOptions ops = new LDAPConnectionOptions();
        ops.setAutoReconnect(true);
        ops.setBindWithDNRequiresPassword(true);
        ops.setResponseTimeoutMillis(0);
        ops.setUseKeepAlive(true);
        ops.setUseTCPNoDelay(true);

        LDAPConnection c = null;
        if (_ldap._port == 636)
            c = new LDAPConnection(_socketFactory, ops);
        else
            c = new LDAPConnection(ops);
        try
        {
            c.connect(_ldap._host, _ldap._port);
            BindResult ok = c.bind(_ldap._userDN, _ldap._password);
            if (ok.getResultCode() != ResultCode.SUCCESS) return false;

            // access the user profile
            SearchResult result = c.search(_ldap._userDN, SearchScope.BASE, "(objectClass=*)",
                    "uid", "displayName", "colaboUserPicture", "colaboUserColor", "colaboDBDN");
            if (result.getEntryCount() == 0) return false;
            SearchResultEntry entry = result.getSearchEntries().get(0);

            String value = entry.getAttributeValue("uid");
            if (value == null || value.isEmpty()) return false;
            _ldap._uid = value;

            value = entry.getAttributeValue("displayName");
            if (value != null && !value.isEmpty()) _ldap._name = value;

            value = entry.getAttributeValue("colaboUserPicture");
            if (value != null && !value.isEmpty()) _ldap._picture = value;

            value = entry.getAttributeValue("colaboUserColor");
            if (value != null && !value.isEmpty()) _ldap._color = Integer.parseInt(value);

            // list of signed databases
            String[] dbNames = entry.getAttributeValues("colaboDBDN");
            for (int i=0; i<dbNames.length; i++)
            {
                SearchResult dbResult = c.search(dbNames[i], SearchScope.BASE, "(objectClass=*)",
                        "cn", "colaboDBServerType", "colaboDBServer", "colaboSVNPath");
                if (dbResult.getEntryCount() == 0) continue;
                SearchResultEntry dbEntry = dbResult.getSearchEntries().get(0);

                // read the database properties
                // load only Web Services based databases
                value = dbEntry.getAttributeValue("colaboDBServerType");
                if (!"STWebServices".equals(value)) continue;

                DBInfo dbInfo = new DBInfo();
                _dbInfo.add(dbInfo);

                value = dbEntry.getAttributeValue("cn");
                if (value != null && !value.isEmpty()) dbInfo._name = value;

                value = dbEntry.getAttributeValue("colaboDBServer");
                if (value != null && !value.isEmpty()) dbInfo._server = value;

                value = dbEntry.getAttributeValue("colaboSVNPath");
                if (value != null && !value.isEmpty()) dbInfo._svn = value;

                SearchResult userResult = c.search("ou=users," + dbNames[i], SearchScope.SUB,
                        "(&(objectClass=colaboUser)(colaboUserDN=" + _ldap._uid + "))",
                        "colaboAccessRights");
                if (userResult.getEntryCount() > 0)
                {
                    SearchResultEntry userEntry = userResult.getSearchEntries().get(0);
                    value = userEntry.getAttributeValue("colaboAccessRights");
                    if (value != null && !value.isEmpty()) dbInfo._accessRights = value;
                }

                SearchResult groupResult = c.search("ou=groups," + dbNames[i], SearchScope.SUB,
                    "(objectClass=colaboGroup)", "cn", "colaboAccessRights", "colaboGroupLevel", "colaboHomePage",
                    "memberUid", "member");
                for (int j=0; j<groupResult.getEntryCount(); j++)
                {
                    SearchResultEntry groupEntry = groupResult.getSearchEntries().get(j);

                    // check if user is member of this group
                    String[] list = groupEntry.getAttributeValues("memberUid");
                    boolean isMember = list != null && Arrays.asList(list).contains(_ldap._uid);
                    if (!isMember)
                    {
                        list = groupEntry.getAttributeValues("member");
                        isMember = list != null && isMember(_ldap._uid, list, c);
                    }
                    if (!isMember) continue;

                    // read the group attributes
                    GroupInfo group = new GroupInfo();
                    dbInfo._groups.add(group);

                    value = groupEntry.getAttributeValue("cn");
                    if (value != null && !value.isEmpty()) group._name = value;

                    value = groupEntry.getAttributeValue("colaboAccessRights");
                    if (value != null && !value.isEmpty()) group._accessRights = value;

                    value = groupEntry.getAttributeValue("colaboGroupLevel");
                    if (value != null && !value.isEmpty()) group._level = Integer.parseInt(value);

                    value = groupEntry.getAttributeValue("colaboHomePage");
                    if (value != null && !value.isEmpty()) group._homePage = Integer.parseInt(value);
                }
                Collections.sort(dbInfo._groups, new Comparator<GroupInfo>()
                {
                    public int compare(GroupInfo group1, GroupInfo group2)
                    {
                        int diff = group1._level - group2._level;
                        if (diff != 0) return diff;
                        return group1._name.compareTo(group2._name);
                    }
                });
            }
        }
        catch (LDAPException ex)
        {
            handleException("ColaboApplet.LoadLDAPSettings", ex);
            return false;
        }

        return true;
    }
    private boolean isMember(String uid, String[] members, LDAPConnection c)
    {
        for (int i=0; i<members.length; i++)
        {
            if (isMember(uid, members[i], c)) return true;
        }
        return false;
    }
    private boolean isMember(String uid, String member, LDAPConnection c)
    {
        try
        {
            // access the entry with the given dn
            SearchResult result = c.search(member, SearchScope.BASE, "(objectClass=*)", "uid", "member");
            if (result.getEntryCount() == 0) {
                return false;
            }
            SearchResultEntry entry = result.getSearchEntries().get(0);
            // check if this is user entry with given uid
            String value = entry.getAttributeValue("uid");
            if (uid.equals(value)) {
                return true;
            }
            String[] list = entry.getAttributeValues("member");
            return list != null && isMember(uid, list, c);
        }
        catch (LDAPSearchException ex)
        {
            handleException("ColaboApplet.IsMember", ex);
            return false;
        }
    }

    private void loadArticles()
    {
        _articles.update();

        articles.setModel(new AbstractListModel()
        {
            public int getSize()
            {
                return _articles._articles.size();
            }
            public Object getElementAt(int index)
            {
                return _articles._articles.get(index)._title;
            }
        });

        articles.addListSelectionListener(new ListSelectionListener()
        {
            public void valueChanged(ListSelectionEvent e)
            {
                if (!e.getValueIsAdjusting()) loadArticle(articles.getSelectedIndex());
            }
        });
    }

    private void loadArticle(int index)
    {
        String text = _articles.loadContent(index);
        content.setText(text);
    }

    public void handleException(String header, Throwable exception)
    {
        content.append(header + ": " + exception.toString() + "\r\n");
        StackTraceElement[] stack = exception.getStackTrace();
        for (int i=0; i<stack.length; i++)
            content.append(stack[i].toString() + "\r\n");
    }

    /** This method is called from within the init() method to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dialogLDAPLogin = new javax.swing.JDialog();
        host = new javax.swing.JTextField();
        userDN = new javax.swing.JTextField();
        buttonOK = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        password = new javax.swing.JPasswordField();
        port = new javax.swing.JSpinner();
        jSplitPane1 = new javax.swing.JSplitPane();
        jSplitPane2 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        tags = new javax.swing.JEditorPane();
        jScrollPane3 = new javax.swing.JScrollPane();
        bookmarks = new javax.swing.JTree();
        jSplitPane3 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        articles = new javax.swing.JList();
        jScrollPane4 = new javax.swing.JScrollPane();
        content = new javax.swing.JTextArea();

        dialogLDAPLogin.setTitle("LDAP Login");
        dialogLDAPLogin.setMinimumSize(new java.awt.Dimension(350, 180));
        dialogLDAPLogin.setModal(true);

        host.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        buttonOK.setText("OK");
        buttonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonOKActionPerformed(evt);
            }
        });

        jLabel1.setLabelFor(host);
        jLabel1.setText("Host:");

        jLabel2.setLabelFor(port);
        jLabel2.setText("Port:");

        jLabel3.setLabelFor(userDN);
        jLabel3.setText("User DN:");

        jLabel4.setLabelFor(password);
        jLabel4.setText("Password:");

        javax.swing.GroupLayout dialogLDAPLoginLayout = new javax.swing.GroupLayout(dialogLDAPLogin.getContentPane());
        dialogLDAPLogin.getContentPane().setLayout(dialogLDAPLoginLayout);
        dialogLDAPLoginLayout.setHorizontalGroup(
            dialogLDAPLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogLDAPLoginLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dialogLDAPLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dialogLDAPLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(dialogLDAPLoginLayout.createSequentialGroup()
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                            .addGap(65, 65, 65))
                        .addGroup(dialogLDAPLoginLayout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                        .addGroup(dialogLDAPLoginLayout.createSequentialGroup()
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                    .addGroup(dialogLDAPLoginLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(dialogLDAPLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonOK, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(userDN, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                    .addComponent(port, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dialogLDAPLoginLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(host, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))
                    .addComponent(password, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE))
                .addContainerGap())
        );
        dialogLDAPLoginLayout.setVerticalGroup(
            dialogLDAPLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dialogLDAPLoginLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dialogLDAPLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(host, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dialogLDAPLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(port, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dialogLDAPLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userDN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dialogLDAPLoginLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(buttonOK)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setLayout(new java.awt.CardLayout());

        jSplitPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jSplitPane1.setMinimumSize(new java.awt.Dimension(400, 300));
        jSplitPane1.setPreferredSize(new java.awt.Dimension(800, 600));

        jSplitPane2.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jScrollPane1.setViewportView(tags);

        jSplitPane2.setLeftComponent(jScrollPane1);

        jScrollPane3.setViewportView(bookmarks);

        jSplitPane2.setRightComponent(jScrollPane3);

        jSplitPane1.setLeftComponent(jSplitPane2);

        jSplitPane3.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        articles.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(articles);

        jSplitPane3.setLeftComponent(jScrollPane2);

        content.setColumns(20);
        content.setRows(5);
        jScrollPane4.setViewportView(content);

        jSplitPane3.setRightComponent(jScrollPane4);

        jSplitPane1.setRightComponent(jSplitPane3);

        add(jSplitPane1, "card2");
    }// </editor-fold>//GEN-END:initComponents

    private void buttonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonOKActionPerformed
        dialogLDAPLogin.setVisible(false);
    }//GEN-LAST:event_buttonOKActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList articles;
    private javax.swing.JTree bookmarks;
    private javax.swing.JButton buttonOK;
    private javax.swing.JTextArea content;
    private javax.swing.JDialog dialogLDAPLogin;
    private javax.swing.JTextField host;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JSplitPane jSplitPane3;
    private javax.swing.JPasswordField password;
    private javax.swing.JSpinner port;
    private javax.swing.JEditorPane tags;
    private javax.swing.JTextField userDN;
    // End of variables declaration//GEN-END:variables

}
