/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.colabo;

import com.bistudio.colabo.ColaboApplet.DBInfo;
import com.bistudio.colabo.ColaboApplet.LDAPInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONTokener;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Jirka
 */
class Articles
{
    public class ArticleItem
    {
        // database fields
        public int _id;
        public int _parent;
        public String _title;
        public String _url;
        public String _author;
        public java.util.Date _date;
        public int _revision;
        public int _majorRevision;
        // tags attached to this article
        public ArrayList<String> _ownTags;
        // tags inherited from parents
        public ArrayList<String> _inheritedTags;
        // effective tags of this article
        public ArrayList<String> _effectiveTags;
        // list of children articles
        public ArrayList<Integer> _children;
        // article marked to read
        public boolean _toRead;
        // article marked as changed
        public boolean _changed;
        // article expanded in the tree view (children are visible)
        public boolean _expanded;
    };
    public ArrayList<ArticleItem> _articles = new ArrayList<ArticleItem>();

    private LDAPInfo _ldapInfo = null;
    private ArrayList<DBInfo> _dbInfo = null;
    private SocketFactory _socketFactory = null;
    private String _authHeader = null;

    public void init(LDAPInfo ldap, ArrayList<DBInfo> dbInfo, SocketFactory socketFactory)
    {
        _ldapInfo = ldap;
        _dbInfo = dbInfo;
        _socketFactory = socketFactory;

        String auth = _ldapInfo._uid + ":" + _ldapInfo._password;
        BASE64Encoder encoder = new BASE64Encoder();
        _authHeader = "Basic " + encoder.encode(auth.getBytes());
    }

    public void update()
    {
        _articles.clear();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

        for (int i=0; i<_dbInfo.size(); i++)
        {
            DBInfo info = _dbInfo.get(i);
            String response = response(info._server + "DB/Articles/" + _ldapInfo._uid + "/", "GET", null);

            try
            {
                JSONTokener reader = new JSONTokener(response);
                JSONObject root = new JSONObject(reader);
                JSONArray items = root.getJSONArray("Articles");
                for (int j=0; j<items.length(); j++)
                {
                    JSONObject item = items.getJSONObject(j);

                    ArticleItem article = new ArticleItem();
                    article._id = item.optInt("ID", -1);
                    article._title = item.optString("Title");
                    article._url = item.optString("URL");
                    article._parent = item.optInt("Parent", -1);
                    article._author = item.optString("Author");
                    String date = item.optString("Date");
                    try
                    {
                        article._date = format.parse(date);
                    }
                    catch (ParseException ex)
                    {
                        article._date = new java.util.Date(0);
                    }
                    article._revision = item.optInt("Revision", -1);
                    article._majorRevision = item.optInt("MajorRevision", -1);
                    _articles.add(article);
                }


            }
            catch (JSONException ex)
            {
                Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


    String loadContent(int index)
    {
        ArticleItem item = _articles.get(index);
        return response(item._url, "GET", null);
    }


    /// Helper to request Web Services and return the response
    private String response(String addr, String method, String data)
    {
        Socket socket = null;
        try
        {
            URL url = new URL(addr);
            int port = url.getPort();
            if (port < 0) port = url.getDefaultPort();
            socket = _socketFactory.createSocket(url.getHost(), port);
            ((SSLSocket)socket).setEnabledProtocols(new String[] {"SSLv3"});

            Writer out = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
            out.write(method + " " + url.toString() + " HTTP/1.0\r\n");
            out.write("Host: " + url.getHost() + ":" + port + "\r\n");
            out.write("Authorization: " + _authHeader + "\r\n");
            out.write("CONTENT-TYPE: application/json\r\n");
            out.write("Connection: close\r\n");
            out.write("\r\n");
            if (data != null) out.write(data);
            out.flush();
            
            BufferedReader in = new BufferedReader(
                new InputStreamReader(socket.getInputStream(), "UTF-8"));
            boolean body = false;
            String content = "";
            String line;
            while ((line = in.readLine()) != null)
            {
                if (body) content += line + "\r\n";
                if (line.isEmpty()) body = true; // header is closed by an empty line
            }
            
            return content;
        }
        catch (Exception ex)
        {
            Logger.getLogger(Articles.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        finally
        {
            if (socket != null)
            {
                try {socket.close();} catch (IOException ex) {}
            }
        }
    }
}
