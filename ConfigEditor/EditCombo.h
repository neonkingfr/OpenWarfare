#pragma once
#include "EditDialog.h"

// CEditCombo dialog

class CEditCombo : public CEditDialog
{
	DECLARE_DYNAMIC(CEditCombo)

public:
	CEditCombo(CWnd* pParent = NULL);   // standard constructor
	virtual ~CEditCombo();
  CArray<CString> comboItems; 
  int value;
// Dialog Data
	enum { IDD = IDD_EDIT_COMBO };

  virtual BOOL OnInitDialog();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  CComboBox m_combo;
  CEdit m_editInfo;
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnCbnSelchangeEditComboCombo();
};
