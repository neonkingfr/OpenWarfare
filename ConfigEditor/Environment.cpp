#include "StdAfx.h"
#include "Environment.h"


////////////////////////////////////////////////////////////////////////////
CEnvironment::CEnvironment(void)
{
  m_fileName = "ConfigEditor.cfg"; // todo
  externalViewer="";
}
////////////////////////////////////////////////////////////////////////////
CEnvironment::~CEnvironment(void)
{
  rules=NULL;
  editable=NULL;
}
////////////////////////////////////////////////////////////////////////////
int CEnvironment::Load()
{
  m_rulesDirs.Init(1); 
  ParamFile confCE;
  if (confCE.Parse(m_fileName)!=LSOK) return ERROR_OPEN_CE_CFG_FILE;

  ParamEntryPtr cls_ConfigEditor = confCE.FindEntry("ConfigEditor");
  if (!cls_ConfigEditor->IsClass())  return ERROR_PARSE_CE_CFG_FILE;

  ParamEntryPtr pentry = cls_ConfigEditor->FindEntry("addons");
  if (!pentry) return ERROR_PARSE_CE_CFG_FILE;
  addonsFolder = *pentry;
  pentry = cls_ConfigEditor->FindEntry("coreConfig");
  if (!pentry) return ERROR_PARSE_CE_CFG_FILE;
  coreConfigFolder = *pentry;

  ParamEntryPtr temp = cls_ConfigEditor->FindEntry("viewer");
  if (temp.NotNull()) externalViewer=(CString)(RString)*temp;

  ParamEntryVal pRules = cls_ConfigEditor->FindEntry("rulesDir");
  if (pRules.IsTextValue()) 
    m_rulesDirs.Add((RStringB) pRules);
  else if (pRules.IsArray())
  {
    for(int i=0,end=pRules.GetSize();i<end;++i)
      if (pRules[i].IsTextValue())
        m_rulesDirs.Add((RStringB) pRules[i]);
  }
  else return ERROR_PARSE_CE_CFG_FILE;

  ParamFile confFile2nd;
  bool first=true;
//  confFile.Parse();
  // reading rules from directories from m_rulesDirs array
  for (int i=0,end=m_rulesDirs.Size();i<end;++i)
  {
    WIN32_FIND_DATA	findData;
    HANDLE h = FindFirstFile(CString(m_rulesDirs[i])+_T("\\*.cfg"), &findData);
    if (h != INVALID_HANDLE_VALUE)
    {
      do
      {
	      if ((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)// files
        {
          if (first)
          {
            if (rulesFile.Parse(m_rulesDirs[i]+"\\"+LPC2R(findData.cFileName))==LSOK)
              first=false;
          }
          else
          {
            if (confFile2nd.Parse(m_rulesDirs[i]+"\\"+LPC2R(findData.cFileName))==LSOK)
              rulesFile.Update(confFile2nd);
          }
        }
      }
      while (FindNextFile(h, &findData));
      if (h) FindClose(h);
    }
  }

  editable = rulesFile.FindEntry("CfgEditable");
  if (editable.IsNull())
  {
    LogF("CfgEditable not found in Rules file");
  }
  else
  {
    for(int i=0,end=editable->GetEntryCount();i<end;++i)
    {
      ParamEntryVal pEditableItem = editable->GetEntry(i);
      if (pEditableItem.IsClass())
      {
        CEditable * item = new CEditable();

        item->SetName(CString(pEditableItem.GetName()));
        ParamEntryVal name = pEditableItem.FindEntry("cfgname");
        if (name.IsTextValue())
          item->SetCfgName(CString(name.GetValue()));

        ParamEntryVal tem = pEditableItem.FindEntry("template");
        if (tem.IsTextValue())
          item->SetTemplateName(CString(tem.GetValue()));

        m_editable.Add(item);
      }
    }
  }
  rules = rulesFile.FindEntry("CfgClassRules");
  if (rules.IsNull()) return ERROR_PARSE_CFG_FILE;
  return NO_ERROR;
}
////////////////////////////////////////////////////////////////////////////
bool CEnvironment::Save()
{
  ParamFile confFile;
  if (confFile.Parse(m_fileName)!=LSOK) return false;


  ParamEntryPtr ceCfg = confFile.FindEntry("ConfigEditor");
  if (ceCfg.IsNull())
  {
    confFile.AddClass("ConfigEditor");
    ceCfg = confFile.FindEntry("ConfigEditor");
  }
  ParamEntryPtr temp  = ceCfg->FindEntry("viewer");
  if (temp.IsNull())
      ceCfg->Add("viewer",CS2R(externalViewer));
  else
      temp->SetValue(CS2R(externalViewer));

  confFile.Save(m_fileName+"2");
  return true;
}
////////////////////////////////////////////////////////////////////////////
///////////////////////////CEditedConfig////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
int CEditedConfig::LoadCFG(CString _fname)
{
  m_fileName=CS2R(_fname);
  if (m_fileName.GetLength()==0) return ERROR_CFG_FILE_NOT_EXIST;
 // m_confFile.Parse("p:\\bin\\config.cpp");
  //ParamFile openConfFile;
//  openConfFile.Parse(m_fileName);
  m_confFile.Clear();
  if (m_confFile.Parse(m_fileName)==LSOK)
    return NO_ERROR;

  return ERROR_PARSE_CFG_FILE;
}
//////////////////////////////////////////////////////////////////////////////////////
CString GetEnumValue(ParamEntryPtr rule,int ivalue)
{
  ParamEntryPtr enumModes= rule->FindEntry("modes");
  if (enumModes.NotNull())
  {
    for(int i=0,end=enumModes->GetSize();i<end;++i)
    {
      ParamEntryPtr mode=rule->FindEntry((RString)(*enumModes)[i]);
      if (mode.NotNull())
      {
        ParamEntryPtr mode_val=mode->FindEntry("value");
        if ((int)*mode_val==ivalue)
        {
          ParamEntryPtr mode_name=mode->FindEntry("name");
          if (mode_name.NotNull())
          return (CString)(RString) (*mode_name);
          return _T("");
        }
      }
    }
  };
  return _T("");
}
//////////////////////////////////////////////////////////////////////////////////////
CString GetEnumValue(IParamArrayValue* rule,int ivalue)
{
  for(int i=0,end=(*rule)[2].GetItemCount();i<end;++i)
    if ((int)(*rule)[2][i][0]==ivalue)
      return (CString)(RString) (*rule)[2][i][1];
  return _T("");
}
////////////////////////////////////////////////////////////////////////////
int CEditedConfig::LoadPBO(CString _fname)
{
  return NO_ERROR; //TODO
}
////////////////////////////////////////////////////////////////////////////
CString ReadValue(IParamArrayValue* _value,ParamEntryPtr _rule)
{
  if ((_rule)&&(GetType(_rule)==type_enum))
    return GetEnumValue(_rule,_value->GetInt());

  if(_value->IsTextValue())
    return (CString)_value->GetValue();      

  if(_value->IsIntValue()||_value->IsFloatValue())
    return (CString)_value->GetValue();    

  if(_value->IsArrayValue())
    return ReadArray(_value,_rule);    

  return _T("Parse error");    
}
////////////////////////////////////////////////////////////////////////////
CString ReadValue(ParamEntryPtr _value,ParamEntryPtr _rule)
{
  
  if (_rule)
  {
    if(GetType(_rule)==type_enum)
    return GetEnumValue(_rule,_value->GetInt());
  }
  if(_value->IsTextValue())
    return (CString)_value->GetValue();      

  if(_value->IsIntValue()||_value->IsFloatValue())
    return (CString)_value->GetValue();    

  if(_value->IsArray())
    return ReadArray(_value,_rule);    

  return _T("Parse error");    
}
////////////////////////////////////////////////////////////////////////////
CString ReadArray(ParamEntryPtr _value,ParamEntryPtr _rule)
{
  CString editText=_T("["); 
  bool more=false;
  for(int i=0,end=_value->GetSize();i<end;++i)
  {
    if (more)editText+=_T(",");
    else more=true;

    ParamEntryPtr ruleStruct = _rule->FindEntry("struct");
    if (ruleStruct.NotNull() && ruleStruct->GetSize()>i)
    {
      ParamEntryPtr valRule = _rule->FindEntry((RString)(*ruleStruct)[i]);
      if (valRule)
        editText+=ReadValue((IParamArrayValue*)&(*_value)[i],valRule);
    }
  }
  return editText+_T("]");
}
////////////////////////////////////////////////////////////////////////////
CString ReadArray(IParamArrayValue* _value,ParamEntryPtr _rule)
{
  CString editText=_T("["); 
  bool more=false;
  for(int i=0,end=_value->GetItemCount();i<end;++i)
  {  
    if (more)editText+=_T(",");
    else more=true;
    
    ParamEntryPtr ruleStruct = _rule->FindEntry("struct");
    if (ruleStruct.NotNull() && ruleStruct->GetSize()>i)
    {
      ParamEntryPtr valRule = _rule->FindEntry((RString)(*ruleStruct)[i]);
      if (valRule)
        editText+=ReadValue((IParamArrayValue*)&(*_value)[i],valRule);
    }
  }
  return editText+_T("]");
}
////////////////////////////////////////////////////////////////////////////
CString CEditedConfig::ReadClass(ParamEntry *_class,TCHAR* _nlChar /*= _T("\x0D\x0A")*/)
{
  static int indent=0;  
  CString editText;
  for (int i=0,end=_class->GetEntryCount();i<end;++i)
  {
    for(int j=0;j<indent;++j)
      editText+=_T("\t");
    if(_class->GetEntry(i).IsClass())
    {
      editText+=CString("Class ")+(CString)_class->GetEntry(i).GetName()+_T(" ");
      ConstParamEntryPtr parent= _class->GetEntry(i).GetPointer()->FindBase();
      if (parent)
        editText+=CString(" : ")+(CString)parent->GetName()+_nlChar;
      else 
        editText+=_nlChar;

      for(int j=0;j<indent;++j)
        editText+=_T("\t");
      editText+=CString("{")+_nlChar;
      ++indent;
      editText+=ReadClass(const_cast<ParamEntry *>(_class->GetEntry(i).GetPointer()));
      --indent;
      for(int j=0;j<indent;++j)
        editText+=_T("\t");
      editText+=CString("};")+_nlChar;
    }
    else 
    {
//      editText+=CString(_class->GetEntry(i).GetName())+_T(" = ")+ReadValue(&_class->GetEntry(i))+_T(";")+_nlChar;    
    }
  }  
  return editText;
}
////////////////////////////////////////////////////////////////////////////
int GetType(ParamEntryPtr _rule)
{
  if (_rule.NotNull())
  {
    ParamEntryPtr type = _rule->FindEntry("type");
   if (type.NotNull()) return *type;
  }
 return 0;
}












////////////////////////////////////////////////////////////////////////////
RString CS2R(CString &str, int codePage/*=CP_ACP*/)
{
  return LPC2R(str,codePage);
}
////////////////////////////////////////////////////////////////////////////
RString LPC2R(LPCTSTR str, int codePage/*=CP_ACP*/)
{
#ifdef UNICODE
  int size = WideCharToMultiByte(codePage, 0, str, -1, NULL, 0, NULL, NULL);
  RString buf;
  buf.CreateBuffer(size);
  WideCharToMultiByte(codePage, 0, str, -1, buf.MutableData(), size, NULL, NULL);
  return buf;
#else
  return RString(str);
#endif
}
////////////////////////////////////////////////////////////////////////////





