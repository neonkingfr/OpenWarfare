// EditArray.cpp : implementation file
//

#include "stdafx.h"
#include "ConfigEditor.h"
#include "EditArray.h"
#include "EditArrayOpenDialog.h"


// CEditArray dialog

IMPLEMENT_DYNAMIC(CEditArray, CDialog)

CEditArray::CEditArray(CWnd* pParent)
	:CEditDialog(CEditArray::IDD, pParent)
{

}

CEditArray::~CEditArray()
{
}

void CEditArray::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX,IDC_EDIT_ARRAY_EDIT_LIST,m_editArray);
  DDX_Control(pDX,IDC_EDIT_ARRAY_STATICTEXT,m_editInfo);
}


BEGIN_MESSAGE_MAP(CEditArray, CDialog)
  ON_NOTIFY(LVN_GETDISPINFO, IDC_EDIT_ARRAY_EDIT_LIST, &CEditArray::OnLvnGetdispinfoEditArrayEditList)
  ON_NOTIFY(NM_DBLCLK, IDC_EDIT_ARRAY_EDIT_LIST, &CEditArray::OnNMDblclkEditArrayEditList)
END_MESSAGE_MAP()


BOOL CEditArray::OnInitDialog()
{
	CDialog::OnInitDialog();
  SetWindowTextW(caption);
  m_editInfo.SetWindowTextW(textToShow);
  m_editArray.InsertColumn(0,_T("value"),LVCFMT_LEFT,200);
  ApplyItemChange();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

//////////////////////////////////////////////////////////////////////////////////////
void CEditArray::ApplyItemChange()
{
  m_paramsList.RemoveAll();
  m_editArray.DeleteAllItems();
  CString text;

  LVITEM item;
  item.mask = LVIF_TEXT | LVIF_PARAM | LVIF_STATE; 
  item.state = 0; 
  item.stateMask = 0; 
	item.iImage = 0;
	item.iSubItem = 0;
  item.pszText = LPSTR_TEXTCALLBACK; 

  if (arrayValue)
    for (int i=0,end=arrayValue->GetSize();i<end;++i)
      {
        item.iItem = i;
        CString name = _T("");//(CString)(RString)(*arrayRule)[2][i][1];
        CString value = ReadValue((IParamArrayValue*)&(*arrayValue)[i],arrayRule);

        TparamsList *list =new TparamsList(name,value,NULL);

        m_paramsList.Add(list);
        m_editArray.InsertItem(&item);
      }
}
//////////////////////////////////////////////////////////////////////////////////////
void CEditArray::OnLvnGetdispinfoEditArrayEditList(NMHDR *pNMHDR, LRESULT *pResult)
{
  NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
  *pResult = 0;

    switch (pDispInfo->item.iSubItem)
    {
    case 0:
        {
          TparamsList *list= m_paramsList[pDispInfo->item.iItem];
          if (list)
          {
            CString ts = list->value;
            size_t size = (ts.GetLength()+1)*sizeof(TCHAR);
            TCHAR *str = _tcscpy((TCHAR*)malloc(size),ts.GetBuffer());
            ts.ReleaseBuffer();
            pDispInfo->item.pszText =str; //TODO destructor /
          }
        }
      break;
    default:
      break;
    }
}
// CEditArray message handlers

void CEditArray::OnNMDblclkEditArrayEditList(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
  if ((pNMIA)&&(pNMIA->iItem>=0))
  {
    ParamFile cls;
    int index=pNMIA->iItem;
    ParamEntryPtr rule = arrayRule->FindEntry("value");

    const IParamArrayValue *selectedValue = &(*arrayValue)[pNMIA->iItem];
    if ((*arrayValue)[pNMIA->iItem].IsFloatValue())
      cls.Add("val",(float)(*arrayValue)[pNMIA->iItem]);
    else if ((*arrayValue)[pNMIA->iItem].IsIntValue())
      cls.Add("val",(int)(*arrayValue)[pNMIA->iItem]);
    else if ((*arrayValue)[pNMIA->iItem].IsTextValue())
      cls.Add("val",(RString)(*arrayValue)[pNMIA->iItem]);
    else if ((*arrayValue)[pNMIA->iItem].IsArrayValue())
    {
      ParamEntryPtr pentry = cls.AddArray("val");
      CopyParamArray(pentry,(IParamArrayValue*)&(*arrayValue)[pNMIA->iItem],arrayRule);
    }

    ParamEntryPtr param = cls.FindEntry("Val"); 
    if (OpenEditDialog(param,rule))
    {
    if (param->IsFloatValue())
      arrayValue->SetValue(pNMIA->iItem,(float)(*param));
    else if (param->IsIntValue())
      arrayValue->SetValue(pNMIA->iItem,param->GetInt());
    else if (param->IsTextValue())
      arrayValue->SetValue(pNMIA->iItem,(RString)*param);
    else if (param->IsArray())
      ;//TODO
    ApplyItemChange(); 
    }
  }
  *pResult = 0;
}
