// EditString.cpp : implementation file
//

#include "stdafx.h"
#include "ConfigEditor.h"
#include "EditString.h"


// CEditString dialog

IMPLEMENT_DYNAMIC(CEditString, CDialog)

CEditString::CEditString(CWnd* pParent /*=NULL*/)
:CEditDialog(CEditString::IDD, pParent)
{

}

CEditString::~CEditString()
{
}

void CEditString::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX,IDC_EDIT_STRING_EDIT_STRING,m_editString);
  DDX_Control(pDX,IDC_EDIT_STRING_STATICTEXT,m_editInfo);
}


BEGIN_MESSAGE_MAP(CEditString, CDialog)
END_MESSAGE_MAP()


BOOL CEditString::OnInitDialog()
{
	CDialog::OnInitDialog();
  m_editString.SetWindowTextW(value);
  m_editInfo.SetWindowTextW(textToShow);
  SetWindowTextW(caption);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CEditString::OnOK()
{
  m_editString.GetWindowTextW(value);
  CDialog::OnOK();
};
// CEditString message handlers
