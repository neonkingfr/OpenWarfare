#pragma once
#include "EditDialog.h"

// CEditString dialog

class CEditString : public CEditDialog
{
	DECLARE_DYNAMIC(CEditString)

public:
	CEditString(CWnd* pParent = NULL);   // standard constructor
	virtual ~CEditString();

 CString value;
// Dialog Data
	enum { IDD = IDD_EDIT_STRING };
  virtual BOOL OnInitDialog();
  virtual void OnOK();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()

  CEdit m_editString;
  CEdit m_editInfo;
};
