// EditArray.cpp : implementation file
//

#include "stdafx.h"
#include "ConfigEditor.h"
#include "EditStruct.h"
#include "EditArrayOpenDialog.h"


// CEditArray dialog

IMPLEMENT_DYNAMIC(CEditStruct, CDialog)

CEditStruct::CEditStruct(CWnd* pParent)
	:CEditDialog(CEditStruct::IDD, pParent)
{

}

CEditStruct::~CEditStruct()
{
}

void CEditStruct::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX,IDC_EDIT_STRUCT_EDIT_LIST,m_editArray);
  DDX_Control(pDX,IDC_EDIT_STRUCT_STATICTEXT,m_editInfo);
}


BEGIN_MESSAGE_MAP(CEditStruct, CDialog)
  ON_NOTIFY(LVN_GETDISPINFO, IDC_EDIT_STRUCT_EDIT_LIST, &CEditStruct::OnLvnGetdispinfoEditArrayEditList)
  ON_NOTIFY(NM_DBLCLK, IDC_EDIT_STRUCT_EDIT_LIST, &CEditStruct::OnNMDblclkEditArrayEditList)
END_MESSAGE_MAP()


BOOL CEditStruct::OnInitDialog()
{
	CDialog::OnInitDialog();
  SetWindowTextW(caption);
  m_editInfo.SetWindowTextW(textToShow);
  m_editArray.InsertColumn(0,_T("name"),LVCFMT_LEFT,86);
  m_editArray.InsertColumn(1,_T("value"),LVCFMT_LEFT,150);
  m_editArray.InsertColumn(2,_T("info"),LVCFMT_LEFT,700);
  ApplyItemChange();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

//////////////////////////////////////////////////////////////////////////////////////
void CEditStruct::ApplyItemChange()
{
  m_paramsList.RemoveAll();
  m_editArray.DeleteAllItems();
  CString text;

  LVITEM item;
  item.mask = LVIF_TEXT | LVIF_PARAM | LVIF_STATE; 
  item.state = 0; 
  item.stateMask = 0; 
	item.iImage = 0;
	item.iSubItem = 0;
  item.pszText = LPSTR_TEXTCALLBACK; 

  ParamEntryPtr pstruct=structRules->FindEntry("struct");
  if (pstruct.IsNull())
  {
    LogF("no class \"struct\" in \"%s\"",cc_cast(structRules->GetName()));  
    return;
  };
  if (structValue->GetSize()!=pstruct->GetSize()) 
  {
    LogF("different count of items in struct \"%s\" and rule for this struct",cc_cast(structValue->GetName()));  
    return;
  }

  if (structValue && pstruct.NotNull())
    for (int i=0,end=structValue->GetSize();i<end;++i)
      {
        ParamEntryPtr rule=  structRules->FindEntry((RString)(*pstruct)[i]);
        if (rule.IsNull())
        {
          LogF("no class \"%s\" in \"%s\"",cc_cast((RString)(*pstruct)[i]),cc_cast(rule->GetName()));  
          return;
        };
        ParamEntryPtr ruleName= rule->FindEntry("Name");
        ParamEntryPtr ruleInfo= rule->FindEntry("info");
        CString info= NULL;
        if (ruleInfo.NotNull())
          info=(CString)ruleInfo->GetValue();
        item.iItem = i;
        CString value,name =(CString)(RString)*ruleName;

        ParamEntryPtr ruleStruct = structRules->FindEntry("struct");
        if (ruleStruct.NotNull() && ruleStruct->GetSize()>i)
        {
          ParamEntryPtr valRule = structRules->FindEntry((RString)(*ruleStruct)[i]);
          if (valRule)
            value = ReadValue((IParamArrayValue*)&(*structValue)[i],valRule);
          else value = "mistake in struct syntax";
        }
        else value = "invalid struct";

    //    value = ReadValue((IParamArrayValue*)&(*structValue)[i],structRules);
        TparamsList *list =new TparamsList(name,value,NULL,info);
        m_paramsList.Add(list);
        m_editArray.InsertItem(&item);
      }
}
//////////////////////////////////////////////////////////////////////////////////////
void CEditStruct::OnLvnGetdispinfoEditArrayEditList(NMHDR *pNMHDR, LRESULT *pResult)
{
  NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
  *pResult = 0;

    switch (pDispInfo->item.iSubItem)
    {
    case 0:
        {
          TparamsList *list= m_paramsList[pDispInfo->item.iItem];
          if (list)
          {
            CString ts = list->name;
            size_t size = (ts.GetLength()+1)*sizeof(TCHAR);
            TCHAR *str = _tcscpy((TCHAR*)malloc(size),ts.GetBuffer());
            ts.ReleaseBuffer();
            pDispInfo->item.pszText =str; //TODO destructor 
          }
        }
      break;
    case 1:
        {
          TparamsList *list= m_paramsList[pDispInfo->item.iItem];
          if (list)
          {
            CString ts = list->value;
            size_t size = (ts.GetLength()+1)*sizeof(TCHAR);
            TCHAR *str = _tcscpy((TCHAR*)malloc(size),ts.GetBuffer());
            ts.ReleaseBuffer();
            pDispInfo->item.pszText =str; //TODO destructor /
          }
        }
        break;
    case 2:
        {
          TparamsList *list= m_paramsList[pDispInfo->item.iItem];
          if (list)
          {
            CString ts = list->info;
            size_t size = (ts.GetLength()+1)*sizeof(TCHAR);
            TCHAR *str = _tcscpy((TCHAR*)malloc(size),ts.GetBuffer());
            ts.ReleaseBuffer();
            pDispInfo->item.pszText =str; //TODO destructor /
          }
        }
      break;

    default:
      break;
    }
}
// CEditArray message handlers

void CEditStruct::OnNMDblclkEditArrayEditList(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
  if ((pNMIA)&&(pNMIA->iItem>=0))
  {
    ParamFile cls;
    int index=pNMIA->iItem;

    ParamEntryPtr pstruct=structRules->FindEntry("struct");
    if (pstruct.IsNull())
    {
      LogF("no class \"struct\" in \"%s\"",cc_cast(structRules->GetName()));  
      return;
    };
    ParamEntryPtr rule=structRules->FindEntry((RString)(*pstruct)[pNMIA->iItem]);
    if (pstruct.IsNull())
    {
      LogF("no class \"%s\" in \"%s\"",cc_cast((RString)(*pstruct)),cc_cast(structRules->GetName()));  
      return;
    };
    const IParamArrayValue *selectedValue = &(*structValue)[pNMIA->iItem];
    if ((*structValue)[pNMIA->iItem].IsFloatValue())
      cls.Add("val",(float)(*structValue)[pNMIA->iItem]);
    else if ((*structValue)[pNMIA->iItem].IsIntValue())
      cls.Add("val",(int)(*structValue)[pNMIA->iItem]);
    else if ((*structValue)[pNMIA->iItem].IsTextValue())
      cls.Add("val",(RString)(*structValue)[pNMIA->iItem]);
    else if ((*structValue)[pNMIA->iItem].IsArrayValue())
    {
      ParamEntryPtr pentry = cls.AddArray("val");
      CopyParamArray(pentry,(IParamArrayValue*)&(*structValue)[pNMIA->iItem],structRules);
      //TODO
    }

    ParamEntryPtr param = cls.FindEntry("Val"); 
    if (OpenEditDialog(param,rule))
    {
    if (param->IsFloatValue())
      structValue->SetValue(pNMIA->iItem,(float)(*param));
    else if (param->IsIntValue())
      structValue->SetValue(pNMIA->iItem,param->GetInt());
    else if (param->IsTextValue())
      structValue->SetValue(pNMIA->iItem,(RString)*param);
    else if (param->IsArray())
      ;//TODO
    ApplyItemChange(); 
    }
  }
  *pResult = 0;
}

