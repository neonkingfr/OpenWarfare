// ConfigEditorDlg.h : header file
//
#pragma once
#include "Environment.h"
#include <utility>
#include <vector>


typedef std::pair<ParamEntryPtr,ParamEntryPtr> TclassAndRule;

// CConfigEditorDlg dialog

class CConfigEditorDlg : public CDialog
{
// Construction
public:
	CConfigEditorDlg(CWnd* pParent = NULL);	// standard constructor
  virtual CConfigEditorDlg::~CConfigEditorDlg();
// Dialog Data
	enum { IDD = IDD_MAIN_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
protected:
	HICON m_hIcon;
  CEnvironment m_environment;
  CEditedConfig m_editedConfig;
  ParamEntryPtr m_editedgroup;
  ParamEntryPtr m_editedclass;
  ParamEntryPtr m_currentRulesList,m_baseRuleList;
  CListCtrl m_seznamParametru;
  CTreeCtrl m_treeClasses;
  CComboBox m_cmbAddons;
  CComboBox m_cmbGroup;
  CEdit m_lineEdit;
  std::vector<TclassAndRule *> classAndRule;

  CArray<TparamsList*> m_paramsList;
  // CButton m_btnEdit;

  void ApplyItemChange();
	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
  int CompateItemsProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
  void CheckClassForSubClasses(HTREEITEM root,ParamEntryPtr cls,CTreeCtrl *tree,ParamEntryPtr ruleList);
  void MoveItem(int dx,int dy,CWnd &item);
  void SizeItem(int dx,int dy,CWnd &item);
  afx_msg void OnFileOpenClick();
  afx_msg void OnCbnSelchangeComboGroup();
  afx_msg void OnCbnSelchangeComboAddon();
  virtual void OnOK();
  virtual void OnCancel();

  afx_msg void OnLvnGetdispinfoListProperties(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnViewSource();
  afx_msg void OnNMDblclkListProperties(NMHDR *pNMHDR, LRESULT *pResult);
//  afx_msg void OnNMClickListProperties(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnFileSave();
  afx_msg void OnTvnSelchangedTreeClasses(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
//  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnViewOptions();
  afx_msg void OnClose();
};
