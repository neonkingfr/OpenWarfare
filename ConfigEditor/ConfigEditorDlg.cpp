// ConfigEditorDlg.cpp : implementation file
//
#pragma once
#include "stdafx.h"
#include "ConfigEditor.h"
#include "ConfigEditorDlg.h"
#include "EditCombo.h"
#include "EditNumber.h"
#include "ViewSource.h"
#include "EditString.h"
#include "EditArray.h"
#include "EditArrayOpenDialog.h"
#include "OptionsDlg.h"

static AppFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

void CConfigEditorDlg::OnOK(){};
void CConfigEditorDlg::OnCancel()
{
  if ( AfxMessageBox(_T("Are you sure you want to abort the changes?"),MB_YESNO) == IDNO )
    return; 
  CDialog::OnCancel();
};

//////////////////////////////////////////////////////////////////////////////////////
CConfigEditorDlg::CConfigEditorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigEditorDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
//  m_currentValue=NULL;
}
//////////////////////////////////////////////////////////////////////////////////////
CConfigEditorDlg::~CConfigEditorDlg()
{
    while (!classAndRule.empty()) 
    {
      TclassAndRule *clsarule=classAndRule.back();
      classAndRule.pop_back();
      clsarule->first = NULL;
      clsarule->second = NULL;
    }
}
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX,IDC_LIST_PROPERTIES,m_seznamParametru);
  DDX_Control(pDX,IDC_COMBO_ADDON,m_cmbAddons);
  DDX_Control(pDX,IDC_COMBO_GROUP,m_cmbGroup);
  DDX_Control(pDX,IDC_EDIT_LINE,m_lineEdit);
  DDX_Control(pDX,IDC_TREE_CLASSES,m_treeClasses);
}
//////////////////////////////////////////////////////////////////////////////////////
BEGIN_MESSAGE_MAP(CConfigEditorDlg, CDialog)
	ON_WM_PAINT()
//	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
  ON_COMMAND(ID_FILE_OPEN32772, &CConfigEditorDlg::OnFileOpenClick)
  ON_CBN_SELCHANGE(IDC_COMBO_GROUP, &CConfigEditorDlg::OnCbnSelchangeComboGroup)
  ON_CBN_SELCHANGE(IDC_COMBO_ADDON, &CConfigEditorDlg::OnCbnSelchangeComboAddon)
  ON_NOTIFY(LVN_GETDISPINFO, IDC_LIST_PROPERTIES, &CConfigEditorDlg::OnLvnGetdispinfoListProperties)
  ON_COMMAND(ID_VIEW_SOURCE, &CConfigEditorDlg::OnViewSource)
  ON_NOTIFY(NM_DBLCLK, IDC_LIST_PROPERTIES, &CConfigEditorDlg::OnNMDblclkListProperties)
  ON_COMMAND(ID_FILE_SAVE32773, &CConfigEditorDlg::OnFileSave)
  ON_NOTIFY(TVN_SELCHANGED, IDC_TREE_CLASSES, &CConfigEditorDlg::OnTvnSelchangedTreeClasses)
  ON_WM_SIZING()
//  ON_WM_SIZE()
ON_WM_SIZE()
ON_COMMAND(ID_VIEW_OPTIONS, &CConfigEditorDlg::OnViewOptions)
ON_WM_CLOSE()
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////////////////////
typedef int ParseCFG_Test_F(int a);
static ParseCFG_Test_F *ParseCFG_Test;
typedef char * ParseCFG_Info_F();
static ParseCFG_Info_F *ParseCFG_Info;
//////////////////////////////////////////////////////////////////////////////////////
bool LoadDll()
{
    HINSTANCE module = LoadLibraryA("parseCFG_A1_109.dll");
    if (!module) return false;
    void *pa = GetProcAddress(module,"Test");
    if (pa)
    {
      ParseCFG_Info = (ParseCFG_Info_F *)pa;
    }
  return false;
}
// CConfigEditorDlg message handlers
//////////////////////////////////////////////////////////////////////////////////////
BOOL CConfigEditorDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

  LoadDll();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


  m_seznamParametru.InsertColumn(0,_T("name"),LVCFMT_LEFT,106);
  m_seznamParametru.InsertColumn(1,_T("value"),LVCFMT_LEFT,300);
  m_seznamParametru.InsertColumn(2,_T("info"),LVCFMT_LEFT,100);
#ifndef FOR_VBS
  m_seznamParametru.InsertColumn(3,_T("declared in"),LVCFMT_LEFT,100);
#endif

  if (m_environment.Load()!=NO_ERROR) return true;
  ParamEntryPtr editable =m_environment.GetEditable();
  if (!editable) return FALSE;
  for (int i=0,end=editable->GetEntryCount();i<end;++i)
  {
    ParamEntryPtr location = editable->GetEntry(i).FindEntry("location");
    if ((location)&&(location->GetValue().GetLength()==0))
      m_cmbGroup.AddString((CString)(RString)(editable->GetEntry(i).GetName()));
  }
	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::OnFileOpenClick()
{
  OPENFILENAME ofn; 
  TCHAR chFile[_MAX_PATH];
  _tcscpy_s(chFile,_MAX_PATH,_T("config.cpp"));
  ZeroMemory(&ofn,  sizeof(OPENFILENAME));
  ofn.lStructSize = sizeof(OPENFILENAME);
  ofn.lpstrFile = chFile;
  ofn.hwndOwner = 0;
  ofn.nMaxFile = sizeof(chFile);
  ofn.lpstrFilter = _T("ARMA Config (config.cpp,config.bin)\0config.cpp;config.bin\0");
  ofn.nFilterIndex = 1;
  ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
  if (GetOpenFileName(&ofn))
  {
    while (!classAndRule.empty()) 
    {
      TclassAndRule *clsarule=classAndRule.back();
      classAndRule.pop_back();
      clsarule->first = NULL;
      clsarule->second = NULL;
    }
    m_editedclass = NULL;
    m_editedgroup = NULL;
    m_editedConfig.LoadCFG(ofn.lpstrFile);
    OnCbnSelchangeComboGroup();
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::OnCbnSelchangeComboGroup()
{
//if (m_editedConfig.m_confFile.GetEntryCount()==0) return;
  if (m_cmbGroup.GetCurSel()==-1) return ;
  m_cmbAddons.ResetContent();
  m_seznamParametru.DeleteAllItems();
  m_treeClasses.DeleteAllItems();
  m_currentRulesList=NULL;
  m_baseRuleList=NULL;
  CString text;
  m_cmbGroup.GetLBText(m_cmbGroup.GetCurSel(),text);
  ParamEntryPtr editable=m_environment.GetEditable();
  if (text!="")
    for (int i=0,end=editable->GetEntryCount();i<end;++i)
      if (editable->GetEntry(i).GetName()==text)
      {
        m_editedgroup = m_editedConfig.m_confFile.FindEntry(CS2R(m_environment.m_editable[i]->GetCfgName()));
        m_currentRulesList = m_environment.GetRules()->FindEntry(CS2R(m_environment.m_editable[i]->GetTemplateName()));
        m_baseRuleList = m_currentRulesList;
        if (m_editedgroup)
          for (int i=0,end=m_editedgroup->GetEntryCount();i<end;++i)
          {
            ParamEntryPtr  scope = m_editedgroup->GetEntry(i).FindEntry("scope"); //TODO
            m_cmbAddons.AddString((CString)m_editedgroup->GetEntry(i).GetName());
          }
        return;
      }
}
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::CheckClassForSubClasses(HTREEITEM root,ParamEntryPtr cls,CTreeCtrl *tree,ParamEntryPtr ruleList)
{
  if (ruleList.IsNull()) return;
  for (int i=0,end=cls->GetEntryCount();i<end;++i)
  {
    ParamEntryPtr pentry=&cls->GetEntry(i);
    if (pentry->IsClass())
    {
      ParamEntryPtr ruleClasses = ruleList->FindEntry("classes");
      if (ruleClasses.IsNull()) continue;
      ParamEntryPtr allclasses = ruleList->FindEntry("allClasses");
      bool forAll = allclasses.NotNull() && allclasses->GetInt()!=0;

      RString name = pentry->GetName();
      ParamEntryPtr classRule;
      ParamEntryPtr currentrule;
      if (forAll)
      {
        currentrule = ruleClasses->FindEntry("all");
        if (currentrule.IsNull()) continue;
      }
      else
      {
        currentrule=ruleClasses->FindEntry(name);
        if (currentrule.IsNull()) continue;
      }
      classRule=m_environment.GetRules()->FindEntry(currentrule->GetValue());
      if (classRule.IsNull()) continue;    

      TclassAndRule *clsarule = new TclassAndRule(pentry,classRule);
      classAndRule.push_back(clsarule);

      HTREEITEM newroot = tree->InsertItem(TVIF_TEXT |TVIF_PARAM ,
      (CString)name, 0, 0, 0, 0,(LPARAM) clsarule, root, NULL);
      CheckClassForSubClasses(newroot,pentry,tree,classRule);    
    }
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::OnCbnSelchangeComboAddon()
{
  CString text;
  m_cmbAddons.GetLBText(m_cmbAddons.GetCurSel(),text);

  m_editedclass = m_editedgroup->FindEntry(CS2R(text));
  m_treeClasses.DeleteAllItems();

  while (!classAndRule.empty()) 
  {
    TclassAndRule *clsarule=classAndRule.back();
    classAndRule.pop_back();
    clsarule->first = NULL;
    clsarule->second = NULL;
  }

  std::pair<ParamEntryPtr,ParamEntryPtr> *clsarule = new std::pair<ParamEntryPtr,ParamEntryPtr>(m_editedclass,m_currentRulesList);
  classAndRule.push_back(clsarule);

  HTREEITEM root = m_treeClasses.InsertItem(TVIF_TEXT|TVIF_PARAM,text,0,0,0,0,(LPARAM)clsarule,TVI_ROOT, NULL);
  CheckClassForSubClasses(root,m_editedclass,&m_treeClasses,m_baseRuleList);
  m_treeClasses.Expand(root,TVE_EXPAND);

  ApplyItemChange();
}
//////////////////////////////////////////////////////////////////////////////////////
int CConfigEditorDlg::CompateItemsProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
  CString    strItem1 = (CString)m_seznamParametru.GetItemText(lParam1,0);
  CString    strItem2 = (CString)m_seznamParametru.GetItemText(lParam2,0);
  return _tcscmp(strItem1, strItem2);
}
//////////////////////////////////////////////////////////////////////////////////////
static int CALLBACK CmpItems(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
  CConfigEditorDlg *ce =(CConfigEditorDlg*)lParamSort;
  return ce->CompateItemsProc(lParam1,lParam2,lParamSort);
}
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::ApplyItemChange()
{
  m_paramsList.RemoveAll();
  m_seznamParametru.DeleteAllItems();

  ParamEntryPtr ruleValues;

  if (m_currentRulesList)
    ruleValues=m_currentRulesList->FindEntry("values");
  else
  {
    LogF("no rules for \"%s\"",cc_cast(m_editedclass->GetName()));
    return;  
  }
  if (ruleValues.IsNull())
  {
    LogF("no rule values for \"%s\"",cc_cast(m_editedclass->GetName()));
    return;
  }
  if (m_editedclass.IsNull())
  {
    LogF("no class selected");
    return;  
  }

  LVITEM item;
  item.mask = LVIF_TEXT | LVIF_PARAM |LVIF_STATE ; 
  item.state = 0; 
  item.stateMask = 0; 
	item.iImage = 0;
	item.iSubItem = 0;
  item.pszText = LPSTR_TEXTCALLBACK; 

  for (int i=0,c=0,end=ruleValues->GetEntryCount();i<end;++i)
  {
    ParamEntryPtr rule = &ruleValues->GetEntry(i);
    if (rule->IsClass())
    {
      RString ruleName =*rule->FindEntry("Name");
      ParamEntryPtr pentry = m_editedclass->FindEntry(rule->GetName());
      if (pentry.IsNull())
      {
        LogF("value \"%s\" not found in class \"%s\"",cc_cast(rule->GetName()),cc_cast(m_editedclass->GetName()));
      }
      else
      {
        item.lParam =i;
        CString value;
        item.iItem =i; 
        
        CString ruleinfo=_T("");
        ParamEntryPtr prInfo =rule->FindEntry("info");
        if (prInfo.NotNull())
          ruleinfo=(CString)(RString)*prInfo;
        CString clsName =(CString)(RString) pentry.GetClass()->GetName();
        value = ReadValue(pentry,rule);
#ifndef FOR_VBS
          TparamsList *list =new TparamsList((CString)ruleName,(CString)value,rule->GetName(),ruleinfo,clsName,i);
#else
          TparamsList *list =new TparamsList((CString)ruleName,(CString)value,rule->GetName(),ruleinfo,i);
#endif
        m_paramsList.Add(list);
        m_seznamParametru.InsertItem(&item);
      }
    }
  }
  m_seznamParametru.SortItems(CmpItems,(LPARAM)this);
}
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::OnLvnGetdispinfoListProperties(NMHDR *pNMHDR, LRESULT *pResult)
{
  NMLVDISPINFO *pDispInfo = reinterpret_cast<NMLVDISPINFO*>(pNMHDR);
  *pResult = 0;

  for(int i=0,end=m_paramsList.GetCount();i<end;++i)
    if (m_seznamParametru.GetItemData(pDispInfo->item.iItem)==m_paramsList[i]->index)
  switch (pDispInfo->item.iSubItem)
    {
    case 0:
        {
          TparamsList *list= m_paramsList[i];
          if (list)
          {
            CString ts = list->name;
            size_t size = (ts.GetLength()+1)*sizeof(TCHAR);
            TCHAR *str = _tcscpy((TCHAR*)malloc(size),ts.GetBuffer());
            ts.ReleaseBuffer();
            pDispInfo->item.pszText =str; //TODO destructor 
          }
        }
      break;
    case 1:
        {
          TparamsList *list= m_paramsList[i];
          if (list)
          {
            CString ts = list->value;
            size_t size = (ts.GetLength()+1)*sizeof(TCHAR);
            TCHAR *str = _tcscpy((TCHAR*)malloc(size),ts.GetBuffer());
            ts.ReleaseBuffer();
            pDispInfo->item.pszText =str; //TODO destructor /
          }
        }
      break;
    case 2:
        {
          TparamsList *list= m_paramsList[i];
          if (list)
          {
            CString ts = list->info;
            size_t size = (ts.GetLength()+1)*sizeof(TCHAR);
            TCHAR *str = _tcscpy((TCHAR*)malloc(size),ts.GetBuffer());
            ts.ReleaseBuffer();
            pDispInfo->item.pszText =str; //TODO destructor /
          }
        }
      break;
#ifndef FOR_VBS
    case 3:
        {
          TparamsList *list= m_paramsList[i];
          if (list)
          {
            CString ts = list->clsName;
            size_t size = (ts.GetLength()+1)*sizeof(TCHAR);
            TCHAR *str = _tcscpy((TCHAR*)malloc(size),ts.GetBuffer());
            ts.ReleaseBuffer();
            pDispInfo->item.pszText =str; //TODO destructor /
          }
        }
      break;
#endif
    default:
      break;
    }
}
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::OnViewSource()
{
  CViewSource dlg;
  QOStrStream strm;
  m_editedConfig.m_confFile.Save(strm,0);
  strm.put(0);
  dlg.text = (CString)strm.str();
  dlg.DoModal();
}
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::OnNMDblclkListProperties(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMITEMACTIVATE pNMIA = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
  if ((pNMIA)&&(pNMIA->iItem>=0))
  {
    int index = m_seznamParametru.GetItemData(pNMIA->iItem);

    for(int i=0,end=m_paramsList.GetCount();i<end;++i)
      if (index==m_paramsList[i]->index)
      {
        ParamEntryPtr currentValue = m_editedclass->FindEntry(m_paramsList[i]->ruleName);
        if (currentValue.IsNull()) continue; // TODO LOGF
        ParamEntryPtr pclass = m_currentRulesList->FindEntry("values");
        if (pclass.IsNull()) continue; // TODO LOGF

        // move entry to edited class if in present in base class
        if (currentValue->IsIntValue())
          m_editedclass->Add(currentValue->GetName(),currentValue->GetInt());
        else if (currentValue->IsFloatValue())
          m_editedclass->Add(currentValue->GetName(),(float)*currentValue);
        else if (currentValue->IsTextValue())
          m_editedclass->Add(currentValue->GetName(),currentValue->GetValue());
        else if (currentValue->IsArray())
        {
          // TODO
          // ParamEntryPtr parray = m_editedclass->AddArray(currentValue->GetName());
          // m_editedclass->Add(currentValue->GetName(),currentValue->GetValue());//TODO dont work for array
          // CopyParamArray(
        }
        //  m_editedclass->Add(currentValue->GetName(),currentValue->GetValue());//TODO dont work for array

        currentValue = m_editedclass->FindEntry(m_paramsList[i]->ruleName);

        if (!pclass) return;
        ParamEntryPtr rule= pclass->FindEntry(currentValue->GetName());
        if (rule&&(rule->IsClass()))
        {
          ParamEntryPtr ruleType=rule->FindEntry("type");
          if (ruleType.NotNull())
            if ((currentValue->IsIntValue())&&((int)(*ruleType)==type_float))
            {
              RString str= currentValue->GetName();
              float fval = (float) currentValue->GetInt();
              m_editedclass->Delete(str);
              m_editedclass->Add(str,fval);
              currentValue = m_editedclass->FindEntry(m_paramsList[i]->ruleName);
            }
          OpenEditDialog(currentValue,rule);
          ApplyItemChange();
        }
    }
  }
  *pResult = 0;
}
//////////////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::OnFileSave()
{
  OPENFILENAME ofn; 
  TCHAR chFile[_MAX_PATH];
  _tcscpy_s(chFile,_MAX_PATH,(CString)m_editedConfig.m_fileName);

  ZeroMemory(&ofn, sizeof(OPENFILENAME));
  ofn.lStructSize = sizeof(OPENFILENAME);
  ofn.lpstrFile = chFile;
  ofn.hwndOwner = 0;
  ofn.nMaxFile = sizeof(chFile);
  ofn.lpstrFilter = _T("ARMA Config (config.cpp,config.bin)\0config.cpp;config.bin\0");
  ofn.nFilterIndex = 1;
  ofn.Flags = OFN_PATHMUSTEXIST;
  if (GetSaveFileName(&ofn))
    m_editedConfig.m_confFile.Save(LPC2R(chFile));

}
///////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::OnTvnSelchangedTreeClasses(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMTREEVIEW pNMTreeView = reinterpret_cast<LPNMTREEVIEW>(pNMHDR);
  HTREEITEM item = m_treeClasses.GetSelectedItem();
  if (item)
  {
    DWORD_PTR i = m_treeClasses.GetItemData(item);
    std::pair<ParamEntryPtr,ParamEntryPtr> *clsarule =(std::pair<ParamEntryPtr,ParamEntryPtr>*)i;

    m_currentRulesList = clsarule->second;
    m_editedclass = clsarule->first;
    ApplyItemChange();
  }
  *pResult = 0;
}
///////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::MoveItem(int dx,int dy,CWnd &item)
{
    RECT btnRect;
    item.GetWindowRect(&btnRect);
    ScreenToClient(&btnRect);
    btnRect.left+=dx;
    btnRect.right+=dx;
    btnRect.top+=dy;
    btnRect.right+=dy;
    item.MoveWindow(&btnRect,1);
}
///////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::SizeItem(int dx,int dy,CWnd &item)
{
    RECT btnRect;
    item.GetWindowRect(&btnRect);
    ScreenToClient(&btnRect);
    btnRect.right+=dx;
    btnRect.bottom+=dy;
    item.MoveWindow(&btnRect,1);
}
///////////////////////////////////////////////////////////////////////////////
#define XMIN 350
#define YMIN 300
void CConfigEditorDlg::OnSizing(UINT fwSide, LPRECT pRect)
{
  CRect * rec = (CRect*)pRect;
  if ((fwSide==7)||(fwSide==6)||(fwSide==8))
  {
    if (rec->bottom-rec->top<YMIN) rec->bottom=rec->top+YMIN;
  }
  else
  {
    if (rec->bottom-rec->top<YMIN) rec->top=rec->bottom-YMIN;
  }
  if ((fwSide==5)||(fwSide==2)||(fwSide==8))
  {
    if (rec->right-rec->left<XMIN) rec->right=rec->left+XMIN;
  }
  else
  {
    if (rec->right-rec->left<XMIN) rec->left=rec->right-XMIN;
  }
}
///////////////////////////////////////////////////////////////////////////////
void CConfigEditorDlg::OnSize(UINT nType, int cx, int cy)
{
  if (nType==SIZE_RESTORED||nType==SIZE_MAXIMIZED)
  {
    static CRect oldrc(0,0,0,0);
    int dx = cx-(oldrc.right-oldrc.left);
    int dy = cy-(oldrc.bottom-oldrc.top);
    if (((dx!=0)||(dy!=0))&&oldrc.right>0)
      {
        SizeItem(0,dy,m_treeClasses);
        SizeItem(dx,dy,m_seznamParametru);
        m_treeClasses.RedrawWindow();
        m_seznamParametru.RedrawWindow();
      };
    GetClientRect(&oldrc);
  }
  CDialog::OnSize(nType, cx, cy);
}

void CConfigEditorDlg::OnViewOptions()
{
  COptionsDlg dlg;
  dlg.viewerPath= m_environment.externalViewer;
  if (dlg.DoModal()==IDOK)
    m_environment.externalViewer=dlg.viewerPath;
  // TODO: Add your command handler code here
}

void CConfigEditorDlg::OnClose()
{
  // TODO: Add your message handler code here and/or call default
  m_environment.Save();
  CDialog::OnClose();
}
