#pragma once


// CViewSource dialog

class CViewSource : public CDialog
{
	DECLARE_DYNAMIC(CViewSource)

public:
	CViewSource(CWnd* pParent = NULL);   // standard constructor
	virtual ~CViewSource();

// Dialog Data
	enum { IDD = IDD_VIEW_SOURCE_EDIT };
  CString text;
protected:
  virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  CEdit m_edit;

	DECLARE_MESSAGE_MAP()
};
