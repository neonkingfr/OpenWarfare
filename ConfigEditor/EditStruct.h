#pragma once

#include "EditDialog.h"
#include "EditCombo.h"
#include "EditNumber.h"
#include "ViewSource.h"
#include "EditString.h"
#include "Environment.h"

// CEditStruct dialog

class CEditStruct : public CEditDialog
{
	DECLARE_DYNAMIC(CEditStruct)

public:
	CEditStruct(CWnd* pParent = NULL);   // standard constructor
	virtual ~CEditStruct();
  ParamEntryPtr structValue;
  ParamEntryPtr structRules;
// Dialog Data
	enum {IDD = IDD_EDIT_STRUCT};
  virtual BOOL OnInitDialog();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()
  CListCtrl m_editArray;
  CEdit m_editInfo;
  void ApplyItemChange();
  CArray<TparamsList*> m_paramsList;
public:
  afx_msg void OnLvnGetdispinfoEditArrayEditList(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMDblclkEditArrayEditList(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnLvnGetdispinfoEditStructEditList(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnNMDblclkEditStructEditList(NMHDR *pNMHDR, LRESULT *pResult);
};
