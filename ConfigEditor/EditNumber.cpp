// EditNumber.cpp : implementation file
//

#include "stdafx.h"
#include "ConfigEditor.h"
#include "EditNumber.h"



// CEditNumber dialog

IMPLEMENT_DYNAMIC(CEditNumber, CDialog)

CEditNumber::CEditNumber(CWnd* pParent /*=NULL*/)
	: CEditDialog(CEditNumber::IDD, pParent)
{

}

CEditNumber::~CEditNumber()
{
}

void CEditNumber::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX,IDC_EDIT_NUMBER_SLIDER,m_slider);
  DDX_Control(pDX,IDC_EDIT_NUMBER_EDIT_NUMBER,m_editNumber);
  DDX_Control(pDX,IDC_EDIT_NUMBER_STATICTEXT,m_editInfo);
}


BEGIN_MESSAGE_MAP(CEditNumber, CEditDialog)
ON_WM_HSCROLL()
ON_EN_CHANGE(IDC_EDIT_NUMBER_EDIT_NUMBER, &CEditNumber::OnEnChangeEditNumberEditNumber)
END_MESSAGE_MAP()

BOOL CEditNumber::OnInitDialog()
{
	CDialog::OnInitDialog();

  SetWindowTextW(caption);
  m_editInfo.SetWindowTextW(textToShow);
  CString text=_T(""); 
  if (type==nint)
  {
    text.Format(_T("%i"),value.i);
    m_slider.SetRange(minValue.i,maxValue.i);
    m_slider.SetPos(value.i);
    m_slider.SetTicFreq((int)((maxValue.i-minValue.i)/10));
  }
  else
  {
    text.Format(_T("%f"),value.f);
    m_slider.SetRange(0,10000);
    m_slider.SetTicFreq(1000);
    m_slider.SetPos((int)((maxValue.f-minValue.f)/value.f*10000));
  }
    m_editNumber.SetWindowTextW(text);
	return TRUE;  
}

void CEditNumber::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
  if (((CWnd*)pScrollBar==&m_slider))
  {

    CString text=_T("");
    if (type==nint)
    {
      value.i=m_slider.GetPos();
      text.Format(_T("%i"),value.i);
    }
    else
    {
      value.f=(float)m_slider.GetPos()/10000*(maxValue.f-minValue.f);
      text.Format(_T("%f"),value.f);
    }
   m_editNumber.SetWindowTextW(text);
  }
  CEditDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CEditNumber::OnEnChangeEditNumberEditNumber()
{
 CString text;
 m_editNumber.GetWindowTextW(text);
 if (type==nint)
 {
   _stscanf(text,_T("%i"),&value.i);
   m_slider.SetPos(value.i);
 }
 else
 {
   _stscanf(text,_T("%f"),&value.f);
   m_slider.SetPos((int)(value.f/(maxValue.f-minValue.f)*10000));
 }
}
