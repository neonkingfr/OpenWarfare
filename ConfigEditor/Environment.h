#pragma once

#include "Editable.h"
#include <Es/Framework/appFrame.hpp>
#include <El\ParamFile\paramFile.hpp>
#include <Es\Containers\array.hpp>

#define ERROR_PARSE_CFG_FILE 4 
#define ERROR_CFG_FILE_NOT_EXIST 3 
#define ERROR_PARSE_CE_CFG_FILE 2 
#define ERROR_OPEN_CE_CFG_FILE 1 
#undef  NO_ERROR
#define NO_ERROR 0

CString ReadArray(IParamArrayValue * _value,ParamEntryPtr _rule);
CString ReadArray(ParamEntryPtr _value,ParamEntryPtr _rule);

CString ReadValue(ParamEntryPtr _value,ParamEntryPtr _rule);
CString ReadValue(IParamArrayValue* _value,ParamEntryPtr _rule);

int GetType(ParamEntryPtr _rule);

RString CS2R(CString &str, int codePage = CP_ACP);
RString LPC2R(LPCTSTR str, int codePage = CP_ACP);
//////////////////////////////////////////////////////////////////////////////////////
#define type_unknow 0
//#define type_bool 1 // used enum(true=1;false=0;);
#define type_string 2
#define type_model 3
#define type_array 4
#define type_texture 5
#define type_color 6
#define type_enum 7 
#define type_int 8
#define type_float 9
#define type_multienum 10 
#define type_struct 11
#define type_classesContainer 12 //similar to array, condition that element is existing class in config 
#define type_variablesContainer 13 // --//--
#define type_class 14
#define type_variable 15
////////////////////////////////////////////////////////////////////////////
struct TparamsList
{
  CString name;CString value;
  RString ruleName;
  CString info;
  int index;
#ifndef FOR_VBS
  CString clsName;
  TparamsList(CString &_name,CString &_value,RString _ruleName,CString _info = NULL,CString _clsName=NULL,int _index =0){value=_value;name=_name;ruleName=_ruleName;index=_index;info=_info;clsName=_clsName;};
#else
  TparamsList(CString &_name,CString &_value,RString _ruleName,CString _info = NULL,int _index =0){value=_value;name=_name;ruleName=_ruleName;index=_index;info=_info;};
#endif
}; 
////////////////////////////////////////////////////////////////////////////
class CEnvironment
{
public:
  CEnvironment(void);
  int Load();
  bool Save();
  ~CEnvironment(void);
  CArray<CEditable*> m_editable;
  CString externalViewer;

  CString GetLastFile(){return CString(lastFile);};
  void SetLastFile(LPCTSTR _str){lastFile=LPC2R(_str);};
  ParamEntryPtr GetEditable(){return editable;};
  ParamEntryPtr GetRules(){return rules;};
protected: 
  ParamEntryPtr rules,editable;
  ParamFile rulesFile;
  RString m_fileName;
  RString lastFile;
  RString addonsFolder;
  RString coreConfigFolder;
  AutoArray<RString> m_rulesDirs;
};
////////////////////////////////////////////////////////////////////////////
class CEditedConfig
{
public:
  int LoadCFG(CString _fname);
  int LoadPBO(CString _fname);
  int Find();
  ParamFile m_confFile;
  CString ReadClass(ParamEntry * _class,TCHAR * _nlChar = _T("\x0D\x0A"));
  CString ReadClass(){return ReadClass(&m_confFile);};
  RString m_fileName;
protected:
//  CString ReadArray(ParamEntryVal _value);
};
////////////////////////////////////////////////////////////////////////////
