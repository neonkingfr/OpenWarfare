#include "stdafx.h"

#include "ConfigEditor.h"
#include "EditDialog.h"
#include "EditCombo.h"
#include "EditNumber.h"
#include "EditString.h"
#include "EditArray.h"
#include "EditStruct.h"

#include "EditArrayOpenDialog.h"
//////////////////////////////////////////////////////////////////////////////////////
void CopyParamArray(ParamEntryPtr outValue,const IParamArrayValue* inValue,ParamEntryPtr  rule)
{
  ParamEntryPtr ruleType; 
  ParamEntryPtr temp = rule->FindEntry("type");
  bool typeIsArray= (temp==type_array);
  for (int i=0, end= inValue->GetItemCount();i<end;++i)
  {
    if (typeIsArray)
    {
      ruleType=rule->FindEntry("value");
    }
    else
    {
      ParamEntryPtr ruleclass = rule->FindEntry((RString)(*temp)[i]);
      ruleType = ruleclass->FindEntry("type");  
    }
    if (((*inValue)[i].IsFloatValue())||(ruleType==type_float))
    {
      outValue->AddValue((float) (*inValue)[i]);
    }
    else
    if ((*inValue)[i].IsIntValue())
    {
      outValue->AddValue((int) (*inValue)[i]);
    }
    else
    if ((*inValue)[i].IsTextValue())
    {
      outValue->AddValue((RString) (*inValue)[i]);
    }
    if ((*inValue)[i].IsArrayValue())
    {
      outValue->AddArrayValue();
      if (rule)
        CopyParamArray(const_cast<IParamArrayValue*>(&(*outValue)[i]),&(*inValue)[i],rule);
      else
        CopyParamArray(const_cast<IParamArrayValue*>(&(*outValue)[i]),&(*inValue)[i]);
    }
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void CopyParamArray(ParamEntryPtr outValue,const ParamEntryPtr inValue,ParamEntryPtr  rule)
{
  ParamEntryPtr ruleType; 
  ParamEntryPtr temp = rule->FindEntry("type");
  bool typeIsArray= (temp==type_array);
  for (int i=0, end= inValue->GetSize();i<end;++i)
  {
    if (typeIsArray)
    {
      ruleType=rule->FindEntry("value");
    }
    else
    {
      ParamEntryPtr ruleclass = rule->FindEntry((RString)(*temp)[i]);
      ruleType = ruleclass->FindEntry("type");  
    }
    if (((*inValue)[i].IsFloatValue())||(ruleType==type_float))
    {
      outValue->AddValue((float) (*inValue)[i]);
    }
    else
    if ((*inValue)[i].IsIntValue())
    {
      outValue->AddValue((int) (*inValue)[i]);
    }
    else
    if ((*inValue)[i].IsTextValue())
    {
      outValue->AddValue((RString) (*inValue)[i]);
    }
    if ((*inValue)[i].IsArrayValue())
    {
      outValue->AddArrayValue();
      if (rule)
        CopyParamArray(const_cast<IParamArrayValue*>(&(*outValue)[i]),&(*inValue)[i],rule);
      else
        CopyParamArray(const_cast<IParamArrayValue*>(&(*outValue)[i]),&(*inValue)[i]);
    }
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void CopyParamArray(IParamArrayValue* outValue,const IParamArrayValue* inValue,ParamEntryPtr rule)
{
  ParamEntryPtr ruleType; 
  ParamEntryPtr temp = rule->FindEntry("type");
  bool typeIsArray= (temp==type_array);
  for (int i=0, end= inValue->GetItemCount();i<end;++i)
  {
    if (typeIsArray)
    {
      ruleType=rule->FindEntry("value");
    }
    else
    {
      ParamEntryPtr ruleclass = rule->FindEntry((RString)(*temp)[i]);
      ruleType = ruleclass->FindEntry("type");  
    }

    if (((*inValue)[i].IsFloatValue())||(ruleType==type_float))
    {
      outValue->AddValue((float) (*inValue)[i]);
    }
    else if ((*inValue)[i].IsIntValue())
    {
      outValue->AddValue((int) (*inValue)[i]);
    }
    else if ((*inValue)[i].IsTextValue())
    {
      outValue->AddValue((RString) (*inValue)[i]);
    }
    if ((*inValue)[i].IsArrayValue())
    {
      outValue->AddArrayValue();
      if (rule)
        CopyParamArray(const_cast<IParamArrayValue*>(&(*outValue)[i]),&(*inValue)[i],rule);
      else 
        CopyParamArray(const_cast<IParamArrayValue*>(&(*outValue)[i]),&(*inValue)[i]);
    }
  }
}
//////////////////////////////////////////////////////////////////////////////////////
void CheckArrayforIntNumbers(ParamEntryPtr eValue, ParamEntryPtr rule)
{
  ParamEntryPtr temp = rule->FindEntry("struct");
  //TODO check if exist
  for (int i=0, end= eValue->GetSize();i<end;++i)
  {
    ParamEntryPtr ruleclass = rule->FindEntry((RString)(*temp)[i]);
    ParamEntryPtr ruleType = ruleclass->FindEntry("type");  
    //TODO check if exist
    if (((*eValue)[0].IsFloatValue())||(ruleType->GetInt() ==type_float))
    {
      eValue->AddValue((float) (*eValue)[0]);
      eValue->DeleteValue(0);
    }
    else
    if ((*eValue)[0].IsIntValue())
    {
      eValue->AddValue((int) (*eValue)[0]);
      eValue->DeleteValue(0);
    }
    else
    if ((*eValue)[0].IsTextValue())
    {
      eValue->AddValue((RString) (*eValue)[0]);
      eValue->DeleteValue(0);
    }
    else if ((*eValue)[0].IsArrayValue())
    {
      eValue->AddArrayValue();
      CopyParamArray(const_cast<IParamArrayValue*>(&(*eValue)[end]),&(*eValue)[0],ruleclass);
      eValue->DeleteValue(0);
    }
  };
}
//////////////////////////////////////////////////////////////////////////////////////
int OpenEditDialog(ParamEntryPtr eValue, ParamEntryPtr rule)
{
  if (eValue)
  {
    if (rule&&(rule->IsClass()))
    {
      ParamEntryPtr ruleInfo=rule->FindEntry("info");
      ParamEntryPtr ruleName=rule->FindEntry("name");
      if (ruleInfo.IsNull()||ruleName.IsNull()) return 0;
      switch (GetType(rule))
      {
        case type_enum: //ENUM
          {   
            CEditCombo dlg;
            if (ruleInfo.IsNull()||ruleName.IsNull()) return 0;

            ParamEntryPtr enumModes= rule->FindEntry("modes");
            if (enumModes.NotNull())
            {
              for(int i=0,end=enumModes->GetSize();i<end;++i)
              {
                ParamEntryPtr mode=rule->FindEntry((RString)(*enumModes)[i]);
                if (mode.NotNull())
                {
                  ParamEntryPtr mode_val=mode->FindEntry("value");
                  ParamEntryPtr mode_name=mode->FindEntry("name");
                  if (mode_name.NotNull())
                    dlg.comboItems.Add((CString)(RString)(*mode_name));
                  if ((int)*mode_val==(int)*eValue)
                    dlg.value =i;
                }
              }
            }
            dlg.textToShow=(CString)(RString)*ruleInfo;
            dlg.caption=_T("Chose value for ")+(CString)(RString)*ruleName;
            if (dlg.DoModal()==IDOK)
            {
              ParamEntryPtr mode=rule->FindEntry((RString)(*enumModes)[dlg.value]);
              if (mode.NotNull())
              {
                ParamEntryPtr mode_val=mode->FindEntry("value");
                if (mode_val.NotNull())
                  eValue->SetValue((int)*mode_val);
              }
            }
          }
          return 1;
        case type_int: //NUMBER INT
          {
            CEditNumber dlg;
            dlg.type=CEditNumber::nint;
            dlg.value.i=*eValue;
            ParamEntryPtr ruleMax=rule->FindEntry("max");
            ParamEntryPtr ruleMin=rule->FindEntry("min");
            if (ruleMax.IsNull()||ruleMin.IsNull()) return 0;
            dlg.maxValue.i=*ruleMax;
            dlg.minValue.i=*ruleMin;
            dlg.caption=_T("Edit integer number: ")+(CString)(RString)*ruleName;
            dlg.textToShow=(CString)(RString)*ruleInfo;
            if (dlg.DoModal()==IDOK)
            {
              eValue->SetValue(dlg.value.i);
            }
          }
          return 1;
        case type_float: //NUMBER FLOAT
          {
            CEditNumber dlg;
            dlg.type=CEditNumber::nfloat;
            dlg.value.f=*eValue;
            ParamEntryPtr ruleMax=rule->FindEntry("max");
            ParamEntryPtr ruleMin=rule->FindEntry("min");
            if (ruleMax.IsNull()||ruleMin.IsNull()) return 0;
            dlg.maxValue.f=*ruleMax;
            dlg.minValue.f=*ruleMin;
            dlg.caption=_T("Edit integer number: ")+(CString)(RString)*ruleName;
            dlg.textToShow=(CString)(RString)*ruleInfo;
            if (dlg.DoModal()==IDOK)
            {
              eValue->SetValue((float)dlg.value.f);
            }
          }
          return 1;
        case type_texture: //TEXTURE
        case type_model:   //MODEL
        case type_string:  //STRINGS
          {
            CEditString dlg;
            dlg.value=(CString)(RString)*eValue;
            dlg.caption=_T("Edit text value: ")+(CString)(RString)*ruleName;
            dlg.textToShow=(CString)(RString)*ruleInfo;
            if (dlg.DoModal()==IDOK)
            {
              eValue->SetValue(CS2R(dlg.value));
            }
          }
          return 1;
        case type_array:   //ARRAYS
          {
            CEditArray dlg;
            //ParamEntryPtr ruleValue=rule->FindEntry("value");
//            if (ruleValue.IsNull()) return 0;
//          CheckArrayforIntNumbers(eValue,&(*rule)[2]);
            dlg.arrayValue=eValue;// (ParamValueSpec*)m_currentValue->copy();
            dlg.arrayRule=rule;

            dlg.textToShow=(CString)(RString)(*ruleInfo);
            dlg.caption=_T("Edit array: ")+(CString)(RString)(*ruleName);
            if (dlg.DoModal()==IDOK)
            {
              //m_currentValue->SetValue(CS2R(dlg.value));
            }
          }
          return 1;
        case type_color:   //COLOR
        case type_struct:  //STRUCT
          {
            CEditStruct dlg;
            CheckArrayforIntNumbers(eValue,rule);
            dlg.structValue=eValue;// (ParamValueSpec*)m_currentValue->copy();
            dlg.structRules=rule;

            dlg.textToShow=(CString)(RString)(*ruleInfo);
            dlg.caption=_T("Edit array: ")+(CString)(RString)(*ruleName);
            if (dlg.DoModal()==IDOK)
            {
              //m_currentValue->SetValue(CS2R(dlg.value));
            }
          }
          return 1;
        default:
          return 0;

      }
    }
  }
  return 0;
}
//////////////////////////////////////////////////////////////////////////////////////