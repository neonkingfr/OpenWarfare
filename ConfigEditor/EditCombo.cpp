// EditCombo.cpp : implementation file
//

#include "stdafx.h"
#include "ConfigEditor.h"
#include "EditCombo.h"


// CEditCombo dialog

IMPLEMENT_DYNAMIC(CEditCombo, CDialog)

CEditCombo::CEditCombo(CWnd* pParent /*=NULL*/)
	: CEditDialog(CEditCombo::IDD, pParent)
{

}

CEditCombo::~CEditCombo()
{
}

void CEditCombo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX,IDC_EDIT_COMBO_COMBO,m_combo);
  DDX_Control(pDX,IDC_EDIT_COMBO_STATICTEXT,m_editInfo);
}

BEGIN_MESSAGE_MAP(CEditCombo, CDialog)
  ON_CBN_SELCHANGE(IDC_EDIT_COMBO_COMBO, &CEditCombo::OnCbnSelchangeEditComboCombo)
END_MESSAGE_MAP()

BOOL CEditCombo::OnInitDialog()
{
	CDialog::OnInitDialog();
  SetWindowTextW(caption);
  m_editInfo.SetWindowTextW(textToShow);
  for (int i =0,end=comboItems.GetCount();i<end;++i)
    m_combo.InsertString(i,comboItems[i]);
  m_combo.SetCurSel(value);
  //for(int i=0,end=m_combo.GetCount();i<end;++i)
 //   if (m_combo.getva) m_combo.SetCurSel(i);
	return TRUE;  // return TRUE  unless you set the focus to a control
}


// CEditCombo message handlers

void CEditCombo::OnCbnSelchangeEditComboCombo()
{
  value=m_combo.GetCurSel();
}
