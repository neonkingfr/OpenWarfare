//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ConfigEditor.rc
//
#define IDC_EDIT_ARRAY_BTN_DELETE       3
#define IDC_EDIT_ARRAY_BTN_ADD          4
#define IDC_EDIT_ARRAY_BTN_DOWN         5
#define IDC_EDIT_ARRAY_BTN_UP           6
#define IDM_ABOUTBOX                    0x0010
#define IDS_ABOUTBOX                    101
#define IDD_MY123_DIALOG                102
#define IDD_MAIN_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDR_MAIN_MENU                   129
#define IDR_POPUPMENU_MAINTEXT          130
#define IDD_EDIT_COMBO                  131
#define IDD_EDIT_NUMBER                 132
#define IDD_VIEW_SOURCE_EDIT            133
#define IDD_EDIT_ARRAY                  134
#define IDD_EDIT_STRING                 135
#define IDR_ACCELERATOR1                136
#define IDD_EDIT_STRUCT                 137
#define IDD_OPTIONS                     138
#define IDC_COMBO_GROUP                 1000
#define IDC_COMBO_ADDON                 1001
#define IDC_LIST1                       1002
#define IDC_LIST_PROPERTIES             1002
#define IDC_EDIT_ARRAY_EDIT_LIST        1002
#define IDC_EDIT1                       1003
#define IDC_MAIN_EDIT                   1003
#define IDC_EDIT_COMBO_STATICTEXT       1003
#define IDC_VIEW_SOURCE_EDIT            1003
#define IDC_OPT_EXT_VIEWER              1003
#define IDC_EDIT_LINE                   1014
#define IDC_BTN_EDIT                    1015
#define IDC_TREE_CLASSES                1017
#define IDC_EDIT_COMBO_COMBO            1018
#define IDC_EDIT_NUMBER_EDIT_NUMBER     1021
#define IDC_EDIT_NUMBER_STATICTEXT      1023
#define IDC_EDIT_NUMBER_SLIDER          1024
#define IDC_EDIT_ARRAY_STATICTEXT       1026
#define IDC_EDIT2                       1027
#define IDC_EDIT_STRING_EDIT_STRING     1027
#define IDC_EDIT_STRING_STATICTEXT      1028
#define IDC_EDIT_STRUCT_EDIT_LIST       1029
#define IDC_EDIT_STRUCT_STATICTEXT      1030
#define IDC_OPT_VIEW_CHECK              1031
#define IDC_BUTTON1                     1032
#define IDC_OPT_VIEW_BROWSE             1032
#define ID_FILE_NEW32771                32771
#define ID_FILE_OPEN32772               32772
#define ID_FILE_SAVE32773               32773
#define ID_EDIT                         32774
#define ID_VIEW_SOURCE                  32775
#define ID_VIEW_OPTIONS                 32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1033
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
