// OptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ConfigEditor.h"
#include "OptionsDlg.h"


// COptionsDlg dialog

IMPLEMENT_DYNAMIC(COptionsDlg, CDialog)

COptionsDlg::COptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDlg::IDD, pParent)
{

}

COptionsDlg::~COptionsDlg()
{
}

void COptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX,IDC_OPT_EXT_VIEWER,wExtViewer);
  DDX_Control(pDX,IDC_OPT_VIEW_CHECK,wViewerCheck);
  DDX_Control(pDX,IDC_OPT_VIEW_BROWSE,wViewerBrowse);
}


BEGIN_MESSAGE_MAP(COptionsDlg, CDialog)
  ON_BN_CLICKED(IDC_OPT_VIEW_CHECK, &COptionsDlg::OnBnClickedCheck1)
  ON_BN_CLICKED(IDC_OPT_VIEW_BROWSE, &COptionsDlg::OnBnClickedOptViewBrowse)
END_MESSAGE_MAP()

BOOL COptionsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
  wExtViewer.SetWindowTextW(viewerPath);
  bool ena =viewerPath!="";
  wExtViewer.EnableWindow(ena);
  wViewerBrowse.EnableWindow(ena);
  wViewerCheck.SetCheck(ena);
  return TRUE;
}

// COptionsDlg message handlers

void COptionsDlg::OnBnClickedCheck1()
{
  wExtViewer.EnableWindow(wViewerCheck.GetCheck());
  wViewerBrowse.EnableWindow(wViewerCheck.GetCheck());
  if (!wViewerCheck.GetCheck()) wExtViewer.SetWindowTextW(_T(""));
}

void COptionsDlg::OnBnClickedOptViewBrowse()
{
  OPENFILENAME ofn; 
  TCHAR chFile[_MAX_PATH];
  _tcscpy_s(chFile,_MAX_PATH,_T(""));
  ZeroMemory(&ofn,  sizeof(OPENFILENAME));
  ofn.lStructSize = sizeof(OPENFILENAME);
  ofn.lpstrFile = chFile;
  ofn.hwndOwner = 0;
  ofn.nMaxFile = sizeof(chFile);
  ofn.lpstrFilter = _T("Application \0*.exe\0");
  ofn.nFilterIndex = 1;
  ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
  if (GetOpenFileName(&ofn))
  {
    wExtViewer.SetWindowTextW(chFile);
  }
}
void COptionsDlg::OnOK()
{
  wExtViewer.GetWindowTextW(viewerPath);

  CDialog::OnOK();
}

