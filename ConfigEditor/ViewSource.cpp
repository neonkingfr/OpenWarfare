// ViewSource.cpp : implementation file
//

#include "stdafx.h"
#include "ConfigEditor.h"
#include "ViewSource.h"


// CViewSource dialog

IMPLEMENT_DYNAMIC(CViewSource, CDialog)

CViewSource::CViewSource(CWnd* pParent /*=NULL*/)
	: CDialog(CViewSource::IDD, pParent)
{

}

CViewSource::~CViewSource()
{
}

void CViewSource::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX,IDC_VIEW_SOURCE_EDIT,m_edit);
}


BEGIN_MESSAGE_MAP(CViewSource, CDialog)
END_MESSAGE_MAP()
//////////////////////////////////////////////////////////////////////////////////////
BOOL CViewSource::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
//	SetIcon(m_hIcon, TRUE);			// Set big icon
//	SetIcon(m_hIcon, FALSE);		// Set small icon

  m_edit.SetWindowTextW(text);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// CViewSource message handlers
