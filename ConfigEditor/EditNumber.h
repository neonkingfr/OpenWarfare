#pragma once
#include "EditDialog.h"

// CEditNumber dialog

class CEditNumber : public CEditDialog
{
	DECLARE_DYNAMIC(CEditNumber)

public:
  enum {nfloat,nint} type;
	CEditNumber(CWnd* pParent = NULL);   // standard constructor
	virtual ~CEditNumber();

  union {int i;float f;} value;
  union {int i;float f;} maxValue;
  union {int i;float f;} minValue;


// Dialog Data
	enum { IDD = IDD_EDIT_NUMBER };
  virtual BOOL OnInitDialog();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  CSliderCtrl m_slider;
  CEdit m_editNumber,m_editInfo;
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  afx_msg void OnEnChangeEditNumberEditNumber();
};
