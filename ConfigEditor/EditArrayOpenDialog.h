#pragma once

#include "Environment.h"

int OpenEditDialog(ParamEntryPtr eValue, ParamEntryPtr rule);
void CopyParamArray(ParamEntryPtr outValue,const IParamArrayValue* inValue, ParamEntryPtr rule=NULL);
void CopyParamArray(IParamArrayValue* outValue,const IParamArrayValue* inValue, ParamEntryPtr rule=NULL);
void CopyParamArray(ParamEntryPtr outValue,const ParamEntryPtr inValue,ParamEntryPtr rule=NULL);
void CheckSubArrayforIntNumbers(IParamArrayValue* outValue,const IParamArrayValue* inValue,const IParamArrayValue * rule);
