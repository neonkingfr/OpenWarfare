#ifndef _BREAKPOINT_H_
#define _BREAKPOINT_H_

class CBreakpoint
{
  // -1 means not set; 0-3 means we've set that hardware bp
  int m_index;

protected:
	inline void SetBits(unsigned long& dw, int lowBit, int bits, int newValue)
	{
		int mask = (1 << bits) - 1; // e.g. 1 becomes 0001, 2 becomes 0011, 3 becomes 0111
		dw = (dw & ~(mask << lowBit)) | (newValue << lowBit);
	}

public:
	// The enum values correspond to the values used by the Intel Pentium,
	// so don't change them!
	enum Condition 
  {
    Write = 1, 
    Read  = 3 /* or write! */
  };

	CBreakpoint() { m_index = -1; }
	~CBreakpoint() { Clear(); }

	void Set(void* address, int len /* 1, 2, or 4 */, Condition when);
	void Clear();	
};

#endif
