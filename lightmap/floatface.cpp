#include "sector.h"
#include "bsp.h"
#include "floatface.h"

void CFloatFacePart::Init(CPolygon Polygon, CFace *ParentFace, CFloatFacePart *Head) {
  // Init of CPolygon
  CPolygon::Init(Polygon.Vertex, Polygon.VertexCount, Polygon.Plane);
  // Copy ParentFace pointer
  this->ParentFace = ParentFace;
  // Add this FloatFacePart to the LinkList
  Next = Head->Next;
  Prev = Head;
  Head->Next->Prev = this;
  Head->Next = this;
}

void CFloatFacePart::Done() {
  Next->Prev = Prev;
  Prev->Next = Next;
}

void CFloatFace::Init(C3DPoint *Vertex, int VertexCount, CPlane Plane, CFRGB E, float ro) {
  CFace::Init(Vertex, VertexCount, Plane, E, ro);
}

void CFloatFace::InsertPart(CBSP *BSPNode, CPolygon Polygon) {
  CPolygon FrontPolygon, BackPolygon;
  int Result;
  Result = Polygon.Split(BSPNode->Plane, FrontPolygon, BackPolygon);
  if (Result & SP_FRONT) {
    switch (BSPNode->FrontType) {
      case BSP_TYPE_NODE:
        InsertPart(BSPNode->FrontNode, FrontPolygon);
        break;
      case BSP_TYPE_LEAF:
        _ASSERT(FFPCount < MAX_FFP);
        AFFP[FFPCount].Init(FrontPolygon, this, &BSPNode->FrontLeaf->FFPHead);
        FFPCount++;
        break;
    }
  }
  if (Result & SP_BACK) {
    switch (BSPNode->BackType) {
      case BSP_TYPE_NODE:
        InsertPart(BSPNode->BackNode, BackPolygon);
        break;
      case BSP_TYPE_LEAF:
        _ASSERT(FFPCount < MAX_FFP);
        AFFP[FFPCount].Init(BackPolygon, this, &BSPNode->BackLeaf->FFPHead);
        FFPCount++;
        break;
    }
  }
}

void CFloatFace::Insert(CBSP *BSP) {
  FFPCount = 0;
  InsertPart(BSP, *this);
/*
  for (int i = 0; i < Width * Height; i++) {
    Map[i].dB = E;
  }
  FocusSector->DistributeEnergy(this, 5000);
*/
}

void CFloatFace::Remove() {
/*
  for (int i = 0; i < Width * Height; i++) {
    Map[i].dB = E.Revert();
  }
  FocusSector->DistributeEnergy(this, 5000);
*/
  for (int i = 0; i < FFPCount; i++) {
    AFFP[i].Done();
  }
  FFPCount = 0;
}

void CFloatFace::Move(CBSP *BSP, C3DPoint Vector) {
  // Remove previous FFP
  Remove();
  // Move this face
  for (int i = 0; i < VertexCount; i++) {
    Vertex[i] = Vertex[i].Move(Vector);
  }
  for (i = 0; i < Width * Height; i++) {
    LMP[i].Sector = (CSector*)NULL2;
  }
  Plane.Init(Plane.NVector, Vertex[0]);
  P = P.Move(Vector);
  // Insert new FFP
  Insert(BSP);
}

void CFloatFace::RotateY(CBSP *BSP, float Angle) {
  // Remove previous FFP
  Remove();
  // Rotate this face
  C3DPoint Focus = GetFocus();
  for (int i = 0; i < VertexCount; i++) {
    Vertex[i] = Vertex[i].RotateY(Focus, Angle);
  }
  for (i = 0; i < Width * Height; i++) {
    LMP[i].Sector = (CSector*)NULL2;
  }
  Plane.NVector = Plane.NVector.RotateY(Angle);
  Plane.Init(Plane.NVector, Vertex[0]);
  P = P.RotateY(Focus, Angle);
  U = U.RotateY(Angle);
  V = V.RotateY(Angle);
  // Insert new FFP
  Insert(BSP);
}
