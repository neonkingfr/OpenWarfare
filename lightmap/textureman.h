#ifndef _textureman_h_
#define _textureman_h_

#include <d3dx8.h>
#include <assert.h>
#include "auxiliary.h"

#define MAKE_COLOR(R,G,B) ((WORD)(((R)>>3)<<11) | (WORD)(((G)>>2)<<5) | (WORD)((B)>>3))
// Returns the parent index of the specified index, -1 in case the index represents root.
#define PARENT(X) ((X==0)?(-1):(X-1)/4)
#define CHILD_1(X) ((X*4)+1)
#define CHILD_2(X) ((X*4)+2)
#define CHILD_3(X) ((X*4)+3)
#define CHILD_4(X) ((X*4)+4)

#define TEXTURE_SIZE 256
#define TEXTUREAREA_NUMBER 21845
#define TEXTURETREE_DEPTH 8

class CTextureArea {
public:
  //! Pointer to the first item in this area
  WORD *LMP;
  //! Coordinates of top left corner in texture space.
  int u, v;
  //! Size of the area. The biggest can be up to TEXTURESIZE. The smallest is 2.
  int Size;
  //! The biggest non-engaged size of sub area.
  int BNE;
  //! Owner of the area. When we release the area, we can set it to NULL.
  CTextureArea **Owner;
  //! Pointers to next and previous items in the queue.
  CTextureArea *Next, *Prev;
};

class CTextureMan {
private:
  //! Pointer to texture in videoram.
  LPDIRECT3DTEXTURE8 pTexture;
  //! Array of the nodes of the tree.
  CTextureArea TextureArea[TEXTUREAREA_NUMBER];
  //! Link list which represents a queue of items. Head.Next is the newest one and vice versa.
  CTextureArea Head;
  //! Locked rectangle
  D3DLOCKED_RECT LockedRect;
  int ItemsCount;
  /*!
    Initializes texture areas.
  */
  void InitTextureArea(int Item, int u, int v, int Size) {
    TextureArea[Item].u = u;
    TextureArea[Item].v = v;
    TextureArea[Item].Size = Size;
    TextureArea[Item].BNE = Size;
    TextureArea[Item].Owner = NULL;
    if (Size > 2) {
      int NewSize = Size/2;
      InitTextureArea(CHILD_1(Item), u, v, NewSize);
      InitTextureArea(CHILD_2(Item), u + NewSize, v, NewSize);
      InitTextureArea(CHILD_3(Item), u, v + NewSize, NewSize);
      InitTextureArea(CHILD_4(Item), u + NewSize, v + NewSize, NewSize);
    }
  }
  /*!
    Removes the oldest item from the queue and from the tree structure.
    \return Index of freed area. It can be bigger than the area of specified item.
  */
  int RemoveOldItem() {
    // Get Index of the item
    int Item = Head.Prev - TextureArea;
    assert(Item >= 0);
    assert(Item < TEXTUREAREA_NUMBER);
    // Inform the owner
    *TextureArea[Item].Owner = NULL;
    // Remove from the queue
    TextureArea[Item].Next->Prev = TextureArea[Item].Prev;
    TextureArea[Item].Prev->Next = TextureArea[Item].Next;
    // Decrement the number of items
    ItemsCount--;
    // Set the BNE to Size
    TextureArea[Item].BNE = TextureArea[Item].Size;
    // Cycle through parents and update their BNE
    while ((Item = PARENT(Item)) != -1) {
      if ((TextureArea[CHILD_1(Item)].BNE == TextureArea[CHILD_1(Item)].Size) &&
          (TextureArea[CHILD_2(Item)].BNE == TextureArea[CHILD_2(Item)].Size) &&
          (TextureArea[CHILD_3(Item)].BNE == TextureArea[CHILD_3(Item)].Size) &&
          (TextureArea[CHILD_4(Item)].BNE == TextureArea[CHILD_4(Item)].Size)) {
        TextureArea[Item].BNE = TextureArea[Item].Size;
      }
      else {
        return Item;
      }
    }
    return 0;
  }
  /*!
    Add specified item into the queue and tree structure.
    \param Item Item to add.
    \Owner Pointer to owner of this area.
  */
  void AddNewItem(int Item, CTextureArea **Owner) {
    // Assign the owner
    TextureArea[Item].Owner = Owner;
    // Insert into the queue
    TextureArea[Item].Next = Head.Next;
    TextureArea[Item].Prev = &Head;
    Head.Next->Prev = &TextureArea[Item];
    Head.Next = &TextureArea[Item];
    // Decrement the number of items
    ItemsCount--;
    // Set the BNE to zero
    TextureArea[Item].BNE = 0;
    // Cycle through parents and update their BNE
    while ((Item = PARENT(Item)) != -1) {
      int MaxBNE = 
        MAX(TextureArea[CHILD_1(Item)].BNE, 
        MAX(TextureArea[CHILD_2(Item)].BNE, 
        MAX(TextureArea[CHILD_3(Item)].BNE, TextureArea[CHILD_4(Item)].BNE)));
      if (TextureArea[Item].BNE > MaxBNE) {
        TextureArea[Item].BNE = MaxBNE;
      }
      else {
        return;
      }
    }
  }
public:
  //! Initializes the texture.
  int Init(LPDIRECT3DDEVICE8 pd3dDevice) {
    InitTextureArea(0, 0, 0, TEXTURE_SIZE);
    Head.Next = &Head;
    Head.Prev = &Head;
    ItemsCount = 0;
    return D3DXCreateTexture(pd3dDevice, TEXTURE_SIZE, TEXTURE_SIZE, 1, 0, D3DFMT_R5G6B5, D3DPOOL_MANAGED, &pTexture);
  }
  //! Enables writing into the texture.
  int Lock() {
    return pTexture->LockRect(0, &LockedRect, NULL, 0);
  }
  //! Disables writing. Posts changes into graphics HW.
  int Unlock() {
    return pTexture->UnlockRect(0);
  }
  /*! 
    Returns area of specified size.
    \param Area Pointer to specified area to get.
    \param Size Required size the area to be.
  */
  void GetArea(CTextureArea *&Area, int Size) {
    assert(Size <= TEXTURE_SIZE);
    // Update size
    Size = MAX(Size, 2);
    // Get index of area with enough space
    int Item;
    if (TextureArea[0].Size > Size) {
      Item = 0;
    }
    else {
      do {
        Item = RemoveOldItem();
      } while (TextureArea[Item].BNE < Size);
    }
    // Now we find the smallest possible area which is still bigger or eqal to Size
    while ((TextureArea[Item].Size/2) >= Size) {
      if (TextureArea[CHILD_1(Item)].BNE >= Size) {
        Item = CHILD_1(Item);
      }
      else if (TextureArea[CHILD_2(Item)].BNE >= Size) {
        Item = CHILD_2(Item);
      }
      else if (TextureArea[CHILD_3(Item)].BNE >= Size) {
        Item = CHILD_3(Item);
      }
      else if (TextureArea[CHILD_4(Item)].BNE >= Size) {
        Item = CHILD_4(Item);
      }
    }
    // Finally add the item
    Area = &TextureArea[Item];
    AddNewItem(Item, &Area);
  }
  /*!
    Returns pointer to memory according to u and v coordinate.
    This method suppose to be called after Lock().
    \param u U coordinate.
    \param v V coordinate.
    \return Pointer to memory and Pitch using D3DLOCKED_RECT structure.
  */
  D3DLOCKED_RECT GetRect(int u, int v) {
    D3DLOCKED_RECT LR;
    LR.Pitch = LockedRect.Pitch;
    LR.pBits = &((BYTE*)LockedRect.pBits)[v * LockedRect.Pitch + u * sizeof(WORD)];
    return LR;
  }
  /*!
    Returns pointer to memory according to specified TextureArea and some offset.
    This method suppose to be called after Lock().
    \param TextureArea TextureArea the returned memory is related to.
    \param off_u U offset.
    \param off_v V offset.
    \return Pointer to memory and Pitch using D3DLOCKED_RECT structure.
  */
  D3DLOCKED_RECT GetRect(CTextureArea *TextureArea, int off_u, int off_v) {
    return GetRect(TextureArea->u + off_u, TextureArea->v + off_v);
  }
  /*!
    Returns texture mapping coordinates according to specified TextureArea and values.
    \param TextureArea TextureArea the value is related to.
    \param Value Value in range (0, TEXTURE_SIZE)
    \return Point coordidnate in range (0, 1)
  */
  C2DPoint GetTexturePosition(CTextureArea *TextureArea, C2DPoint Value) {
    C2DPoint Result;
    Result.I = (TextureArea->u + (float)Value.I)/TEXTURE_SIZE;
    Result.J = (TextureArea->v + (float)Value.J)/TEXTURE_SIZE;
    return Result;
  }
  /*!
    Returns pointer to texture structure associated with TextureMan.
    \return Pointer to texture.
  */
  LPDIRECT3DTEXTURE8 GetTexture() {
    return pTexture;
  }
};

#endif