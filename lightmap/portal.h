#ifndef _portal_h_
#define _portal_h_

#include <string.h>
#include <class/array.hpp>
#include "polygon.h"

class CSector;

class CPortal : public CPolygon {
public:
  // Neighbour sector
  CSector *NeiSector;
  // Initialization of Portal
  void Init(C3DPoint *Vertex, int VertexCount, CPlane Plane, CSector *NeiSector);
};

TypeIsSimple(CPortal)

#endif
