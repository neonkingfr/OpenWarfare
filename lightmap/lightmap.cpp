//-----------------------------------------------------------------------------
// File: Matrices.cpp
//
// Desc: Now that we know how to create a device and render some 2D vertices,
//       this tutorial goes the next step and renders 3D geometry. To deal with
//       3D geometry we need to introduce the use of 4x4 matrices to transform
//       the geometry with translations, rotations, scaling, and setting up our
//       camera.
//
//       Geometry is defined in model space. We can move it (translation),
//       rotate it (rotation), or stretch it (scaling) using a world transform.
//       The geometry is then said to be in world space. Next, we need to
//       position the camera, or eye point, somewhere to look at the geometry.
//       Another transform, via the view matrix, is used, to position and
//       rotate our view. With the geometry then in view space, our last
//       transform is the projection transform, which "projects" the 3D scene
//       into our 2D viewport.
//
//       Note that in this tutorial, we are introducing the use of D3DX, which
//       is a set up helper utilities for D3D. In this case, we are using some
//       of D3DX's useful matrix initialization functions. To use D3DX, simply
//       include <d3dx8.h> and link with d3dx8.lib.
//
// Copyright (c) 2000 Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#include <d3dx8.h>
#include <dinput.h>
#include <mmsystem.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "auxiliary.h"
#include "scene.h"
#include "textureman.h"


//-----------------------------------------------------------------------------
// Global variables
//-----------------------------------------------------------------------------
LPDIRECT3D8             g_pD3D       = NULL; // Used to create the D3DDevice
LPDIRECT3DDEVICE8       g_pd3dDevice = NULL; // Our rendering device
LPDIRECT3DVERTEXBUFFER8 g_pVB        = NULL; // Buffer to hold vertices

// Keyboard
#define KEYDOWN(name, key) (name[key] & 0x80) 
LPDIRECTINPUT8          g_lpDI       = NULL;
LPDIRECTINPUTDEVICE8    g_lpDIDevice = NULL;
char g_buffer[256]; 

// Texture
LPDIRECT3DTEXTURE8      g_pTexture   = NULL; // Texture
CTextureMan             g_TextureMan;

// Scene
C3DLine g_Eye;
CScene g_Scene;

// CornerText
char g_CornerText[10][256];

// Flags
int g_Flag0 = 0;
// Counters
int g_LMPCount;
int g_PatchCount;
int g_RefineCount = 1000;
int g_CycleCount = 0;

void LogF(char const *, ...) {

};

//-----------------------------------------------------------------------------
// Name: InitD3D()
// Desc: Initializes Direct3D
//-----------------------------------------------------------------------------
HRESULT InitD3D( HWND hWnd )
{
    // Create DI object
    if FAILED(DirectInput8Create(GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&g_lpDI, NULL)) 
    { 
        return E_FAIL;
    }

    // Initialize DirectInput device
    if FAILED(g_lpDI->CreateDevice(GUID_SysKeyboard, &g_lpDIDevice, NULL)) { 
        return E_FAIL; 
    } 

    // Setting the Keyboard Data Format
    if FAILED(g_lpDIDevice->SetDataFormat(&c_dfDIKeyboard)) { 
        return E_FAIL; 
    } 

    // Set the cooperative level 
    if FAILED(g_lpDIDevice->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)) { 
        return E_FAIL; 
    }

    // Create the D3D object.
    if( NULL == ( g_pD3D = Direct3DCreate8( D3D_SDK_VERSION ) ) )
        return E_FAIL;

    // Get the current desktop display mode, so we can set up a back
    // buffer of the same format
    D3DDISPLAYMODE d3ddm;
    if( FAILED( g_pD3D->GetAdapterDisplayMode( D3DADAPTER_DEFAULT, &d3ddm ) ) )
        return E_FAIL;

    // Set up the structure used to create the D3DDevice
    D3DPRESENT_PARAMETERS d3dpp;
    ZeroMemory( &d3dpp, sizeof(d3dpp) );
    d3dpp.Windowed = TRUE;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3dpp.BackBufferFormat = d3ddm.Format;
    d3dpp.EnableAutoDepthStencil = TRUE;
    d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

    // Create the D3DDevice
    if( FAILED( g_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
                                      D3DCREATE_SOFTWARE_VERTEXPROCESSING,
                                      &d3dpp, &g_pd3dDevice ) ) )
    {
        return E_FAIL;
    }

    // Turn on Z-buffer
    g_pd3dDevice->SetRenderState( D3DRS_ZENABLE, TRUE );

    // Turn off culling, so we see the front and back of the triangle
    g_pd3dDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );

    // Turn off D3D lighting, since we are providing our own vertex colors
    g_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );

    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_SELECTARG1 );
    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );

    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_MINFILTER, D3DTEXF_LINEAR );
    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR );
    //g_pd3dDevice->SetTextureStageState( 0, D3DTSS_MINFILTER, D3DTEXF_NONE );
    //g_pd3dDevice->SetTextureStageState( 0, D3DTSS_MAGFILTER, D3DTEXF_NONE );

    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_ADDRESSU,  D3DTADDRESS_CLAMP );
    g_pd3dDevice->SetTextureStageState( 0, D3DTSS_ADDRESSV,  D3DTADDRESS_CLAMP );

    // Creating of a texture

/*
    if (FAILED(D3DXCreateTextureFromFile(g_pd3dDevice, "wall.bmp", &g_pTexture))) {
      return E_FAIL;
    }
*/

/*
    if (FAILED(g_pd3dDevice->CreateTexture(GLOBAL_TEXTURE_SIZE, GLOBAL_TEXTURE_SIZE, 1, 0, D3DFMT_R5G6B5, D3DPOOL_SYSTEMMEM, &g_pTexture))) {
      return E_FAIL;
    }

    UINT Width = 256, Height = 256, NumMipLevels = 1;
    D3DFORMAT Format;
    Format = D3DFMT_R8G8B8;
    if (FAILED(D3DXCheckTextureRequirements(g_pd3dDevice, &Width, &Height, &NumMipLevels, 0, &Format, D3DPOOL_SYSTEMMEM))) {
      return E_FAIL;
    }
*/


    if (FAILED(D3DXCreateTexture(g_pd3dDevice, GLOBAL_TEXTURE_SIZE, GLOBAL_TEXTURE_SIZE, 1, 0, D3DFMT_R5G6B5, D3DPOOL_MANAGED, &g_pTexture))) {
      return E_FAIL;
    }


    g_TextureMan.Init(g_pd3dDevice);

/*
    D3DLOCKED_RECT LockedRect;
    g_pTexture->LockRect(0, &LockedRect, NULL, 0);
    MAKE_R5G6B5(((WORD*)LockedRect.pBits)[0], 255, 0, 0);
    MAKE_R5G6B5(((WORD*)LockedRect.pBits)[1], 0, 255, 0);
    MAKE_R5G6B5(((WORD*)LockedRect.pBits)[2], 0, 0, 255);
    g_pTexture->UnlockRect(0);
*/
    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: InitGeometry()
// Desc: Creates the scene geometry
//-----------------------------------------------------------------------------
HRESULT InitGeometry()
{

    g_Eye.A.X = 0;
    g_Eye.A.Y = 0;
    g_Eye.A.Z = -80;

/*
    g_Eye.A.X = -355;
    g_Eye.A.Y = 3;
    g_Eye.A.Z = 367;
*/
    g_Eye.V.X = 0;
    g_Eye.V.Y = 0;
    g_Eye.V.Z = 1;

    //g_Scene.Init("S1");
    g_Scene.Init("SCENE04");
    //g_Scene.Init();

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: Cleanup()
// Desc: Releases all previously initialized objects
//-----------------------------------------------------------------------------
VOID Cleanup()
{
    if( g_pVB != NULL )
        g_pVB->Release();

    if( g_pd3dDevice != NULL )
        g_pd3dDevice->Release();

    if( g_pD3D != NULL )
        g_pD3D->Release();

    // Keyboard
    if( g_lpDIDevice != NULL )
        g_lpDIDevice->Release();

    if( g_lpDI != NULL )
        g_lpDI->Release();
}



//-----------------------------------------------------------------------------
// Name: SetupMatrices()
// Desc: Sets up the world, view, and projection transform matrices.
//-----------------------------------------------------------------------------
VOID SetupMatrices()
{
    // Read keyboard
    if (!FAILED(g_lpDIDevice->Acquire())) {
      if (!FAILED(g_lpDIDevice->GetDeviceState(sizeof(g_buffer),(LPVOID)&g_buffer))) {
        if (KEYDOWN(g_buffer, DIK_Q)) {
          g_Eye.A.Y += 5;
        }
        if (KEYDOWN(g_buffer, DIK_A)) {
          g_Eye.A.Y -= 5;
        }
        if (KEYDOWN(g_buffer, DIK_UPARROW)) {
          g_Eye.A = g_Eye.A.Move(g_Eye.V.Multiply(5));
        }
        if (KEYDOWN(g_buffer, DIK_DOWNARROW)) {
          g_Eye.A = g_Eye.A.Move(g_Eye.V.Multiply(5).Revert());
        }
        if (KEYDOWN(g_buffer, DIK_LEFTARROW)) {
          g_Eye.V = g_Eye.V.RotateY(PI/50);
        }
        if (KEYDOWN(g_buffer, DIK_RIGHTARROW)) {
          g_Eye.V = g_Eye.V.RotateY(-PI/50);
        }
        if (KEYDOWN(g_buffer, DIK_1)) {
          g_Flag0 = 1 - g_Flag0;
        }
        if (KEYDOWN(g_buffer, DIK_INSERT)) {
          g_Scene.AddFF(g_Eye.A);
        }
        if (KEYDOWN(g_buffer, DIK_DELETE)) {
          g_Scene.DeleteFF();
        }
        if (KEYDOWN(g_buffer, DIK_HOME)) {
          CFRGB Color(10.0f, 0.0f, 0.0f);
          g_Scene.ChangeREFF(Color);
        }
        if (KEYDOWN(g_buffer, DIK_END)) {
          CFRGB Color(-10.0f, 0.0f, 0.0f);
          g_Scene.ChangeREFF(Color);
        }
        if (KEYDOWN(g_buffer, DIK_NUMPAD4)) {
          g_Scene.MoveFF(C3DPoint(-20, 0, 0));
        }
        if (KEYDOWN(g_buffer, DIK_NUMPAD6)) {
          g_Scene.MoveFF(C3DPoint(20, 0, 0));
        }
        if (KEYDOWN(g_buffer, DIK_NUMPAD8)) {
          g_Scene.MoveFF(C3DPoint(0, 20, 0));
        }
        if (KEYDOWN(g_buffer, DIK_NUMPAD2)) {
          g_Scene.MoveFF(C3DPoint(0, -20, 0));
        }
        if (KEYDOWN(g_buffer, DIK_NUMPAD9)) {
          g_Scene.MoveFF(C3DPoint(0, 0, 20));
        }
        if (KEYDOWN(g_buffer, DIK_NUMPAD1)) {
          g_Scene.MoveFF(C3DPoint(0, 0, -20));
        }
        if (KEYDOWN(g_buffer, DIK_NUMPAD7)) {
          g_Scene.RotateYFF(PI/10.0);
        }
        if (KEYDOWN(g_buffer, DIK_NUMPAD3)) {
          g_Scene.RotateYFF(-PI/10.0);
        }
        if (KEYDOWN(g_buffer, DIK_PRIOR)) {
          g_RefineCount += 10;
        }
        if (KEYDOWN(g_buffer, DIK_NEXT)) {
          g_RefineCount -= 10;
        }
      }
    }

    // Set up our view matrix. A view matrix can be defined given an eye point,
    // a point to lookat, and a direction for which way is up. Here, we set the
    // eye five units back along the z-axis and up three units, look at the
    // origin, and define "up" to be in the y-direction.
    D3DXMATRIX matView;
    D3DXMatrixLookAtLH( &matView, &D3DXVECTOR3( g_Eye.A.X, g_Eye.A.Y, g_Eye.A.Z ),
                                  &D3DXVECTOR3( g_Eye.A.X + g_Eye.V.X, g_Eye.A.Y + g_Eye.V.Y, g_Eye.A.Z + g_Eye.V.Z ),
                                  &D3DXVECTOR3( 0.0f, 1.0f, 0.0f ) );
    g_pd3dDevice->SetTransform( D3DTS_VIEW, &matView );

    // For the projection matrix, we set up a perspective transform (which
    // transforms geometry from 3D view space to 2D viewport space, with
    // a perspective divide making objects smaller in the distance). To build
    // a perpsective transform, we need the field of view (1/4 pi is common),
    // the aspect ratio, and the near and far clipping planes (which define at
    // what distances geometry should be no longer be rendered).
    D3DXMATRIX matProj;
    D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, 1.0f, 1.0f, 10000.0f );
    g_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );
}



class CCustomVertex2 {
public:
  // 3D space coordinate of point
  C3DPoint P;
  // 2D texture coordinate
  C2DPoint T;
};

//-----------------------------------------------------------------------------
// Name: Render()
// Desc: Draws the scene
//-----------------------------------------------------------------------------
VOID Render()
{
    // Clear the backbuffer to some color
    g_pd3dDevice->Clear( 0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER , D3DCOLOR_XRGB(0,0,200), 1.0f, 0 );

    // Begin the scene
    g_pd3dDevice->BeginScene();

    // Setup the world, view, and projection matrices
    SetupMatrices();

    // Processing scene
    //g_Scene.Render(g_pd3dDevice, g_pTexture, g_Eye.A);
    g_Scene.Render2(g_pd3dDevice, g_pTexture, g_Eye, g_RefineCount);


    CCustomVertex2 CV2[4];
    CV2[0].P.X = -50.0f;
    CV2[0].P.Y = 50.0f;
    CV2[0].P.Z = 200.0f;
    CV2[0].T.I = 0.0f;
    CV2[0].T.J = 0.0f;

    CV2[1].P.X = 50.0f;
    CV2[1].P.Y = 50.0f;
    CV2[1].P.Z = 200.0f;
    CV2[1].T.I = 1.0f;
    CV2[1].T.J = 0.0f;

    CV2[2].P.X = 50.0f;
    CV2[2].P.Y = -50.0f;
    CV2[2].P.Z = 200.0f;
    CV2[2].T.I = 1.0f;
    CV2[2].T.J = 1.0f;

    CV2[3].P.X = -50.0f;
    CV2[3].P.Y = -50.0f;
    CV2[3].P.Z = 200.0f;
    CV2[3].T.I = 0.0f;
    CV2[3].T.J = 1.0f;

    g_pd3dDevice->SetVertexShader(D3DFVF_XYZ | D3DFVF_TEX1);
    g_pd3dDevice->SetTexture(0, g_TextureMan.GetTexture());
    g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, 2, CV2, sizeof(CCustomVertex2));



/*
    C3DPoint HaltonPoints[1024];
    CHalton Halton1(2);
    CHalton Halton2(3);
    C3DPoint P(1.0f, 0.0f, 0.0f);
    P = P.RotateY((float)g_CycleCount/100.0f).RotateZ((float)g_CycleCount/100.0f);
    for (int i = 0; i < 1024; i++) {
      HaltonPoints[i] = P.ChangeOrientation(2*PI*Halton2, (float)asin(1 - sqrt(Halton1)));
    }
    g_pd3dDevice->SetVertexShader(D3DFVF_XYZ);
    g_pd3dDevice->DrawPrimitiveUP(D3DPT_POINTLIST, 1024, HaltonPoints, sizeof(C3DPoint));
*/
    //g_pd3dDevice->DrawPrimitiveUP(D3DPT_POINTLIST, 1000, C3DPoint::DifuseRay, sizeof(C3DPoint));
    //g_pd3dDevice->DrawPrimitiveUP(D3DPT_POINTLIST, 32768, &C3DPoint::DifuseRay[32768], sizeof(C3DPoint));



    // End the scene
    g_pd3dDevice->EndScene();

    // Present the backbuffer contents to the display
    g_pd3dDevice->Present( NULL, NULL, NULL, NULL );

    g_CycleCount++;
}




//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------
LRESULT WINAPI MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
    switch( msg )
    {
        case WM_DESTROY:
            PostQuitMessage( 0 );
            return 0;
    }

    return DefWindowProc( hWnd, msg, wParam, lParam );
}




//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: The application's entry point
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR, INT )
{



  C3DPoint::InitDifuseRay();

/*
//------------------------------------
  C2DPoint Point[65536];
  C2DPoint BestPoint, NewPoint;
  float BestDistance, NewDistance;
  for (int i = 0; i < 65536; i++) {
    BestPoint.I = 0;
    BestPoint.J = 0;
    BestDistance = 0;
    for (int j = 0; j < 5; j++) {
      do {
        NewPoint.I = (float)((rand()*2.0)/RAND_MAX - 1.0);
        NewPoint.J = (float)((rand()*2.0)/RAND_MAX - 1.0);
      } while (NewPoint.Magnitude() >= 1);
      NewDistance = 1.0;
      for (int k = 0; k < i; k++) {
        if (Point[k].Distance(NewPoint) < NewDistance) {
          NewDistance = Point[k].Distance(NewPoint);
        }
      }
      if (NewDistance > BestDistance) {
        BestPoint = NewPoint;
        BestDistance = NewDistance;
      }
    }
    Point[i] = BestPoint;
    if ((i % 1000) == 0) {
      OutputDebugString("."); 
    }
  }


  C3DPoint *aaa = (C3DPoint*) malloc(sizeof(float)*3*65536);
  _ASSERT(aaa);
  for (i = 0; i < 65536; i++) {
    aaa[i].X = Point[i].I;
    aaa[i].Y = Point[i].J;
    aaa[i].Z = (float)sqrt(1 - aaa[i].X * aaa[i].X - aaa[i].Y * aaa[i].Y);
  }

  FILE *f;
  f = fopen("difuserays.bin", "a+b");

  fwrite(aaa, 1, sizeof(float)*3*65536, f);

  fclose(f);



//------------------------------------
*/

    int FPS = 0;
    time_t OldSec = 0;
    time_t NewSec = 0;
    int FrameCount = 0;



        int tmpDbgFlag;
        tmpDbgFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
        tmpDbgFlag |= _CRTDBG_DELAY_FREE_MEM_DF;
        tmpDbgFlag |= _CRTDBG_LEAK_CHECK_DF;
        tmpDbgFlag |= _CRTDBG_CHECK_CRT_DF;
        tmpDbgFlag |= _CRTDBG_CHECK_ALWAYS_DF;
        _CrtSetDbgFlag(tmpDbgFlag);








    // Register the window class
    WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L,
                      GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
                      "D3D Tutorial", NULL };
    RegisterClassEx( &wc );

    // Create the application's window
    HWND hWnd = CreateWindow( "D3D Tutorial", "D3D Tutorial 03: Matrices",
                              WS_OVERLAPPEDWINDOW, 100, 100, 512, 512,
                              GetDesktopWindow(), NULL, wc.hInstance, NULL );

    // Initialize Direct3D
    if( SUCCEEDED( InitD3D( hWnd ) ) )
    {
        // Create the scene geometry
        if( SUCCEEDED( InitGeometry() ) )
        {
            // Show the window
            ShowWindow( hWnd, SW_SHOWDEFAULT );
            UpdateWindow( hWnd );

            // Enter the message loop
            MSG msg;
            ZeroMemory( &msg, sizeof(msg) );
            while( msg.message!=WM_QUIT )
            {
                if( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
                {
                    TranslateMessage( &msg );
                    DispatchMessage( &msg );
                }
                else
                {
                    g_LMPCount = 0;
                    g_PatchCount = 0;
                    Render();
                    // FPS
                    FrameCount++;
                    time(&NewSec);
                    if (NewSec != OldSec) {
                      sprintf(g_CornerText[9], "FPS: %d", FrameCount);
                      FrameCount = 0;
                      OldSec = NewSec;
                    }
                    // PatchCount
                    sprintf(g_CornerText[7], "PCH: %d", g_PatchCount);
                    // LMPCount
                    sprintf(g_CornerText[8], "LMP: %d", g_LMPCount);


                    // CornerText
                    HDC dc = GetDC(hWnd);
                    for (int i = 0; i < 10; i++) {
                      TextOut(dc, 10, i*20 + 10, g_CornerText[i], lstrlen(g_CornerText[i]));
                    }
                }
            }
        }
    }

    // Clean up everything and exit the app
    Cleanup();
    UnregisterClass( "D3D Tutorial", wc.hInstance );
    return 0;
}



