class C3DPoint {
public:
  float X, Y, Z;
  /*!
    This method changes orientation of the vector according to angles specified by
    rx and ry. Consider the vector lies on X axis. First we rotate it along the
    Y axis by ry angle. Then we rotate it along X axis by to rx value. Of course
    it does work for an arbitrary vector.
    \param rx Angle of rotation along X axis.
    \param ry Angle of rotation along Y axis.
    \return Desired vector.
  */
  inline C3DPoint ChangeOrientation(float rx, float ry) {
    // Get precalculated values
    float SinRX = (float) sin(rx);
    float CosRX = (float) cos(rx);
    float SinRY = (float) sin(ry);
    float CosRY = (float) cos(ry);
    // Get the unrotated point according to rx and ry
    C3DPoint P;
    P.X = CosRY;
    P.Y = (float)(-SinRY * SinRX);
    P.Z = (float)(SinRY * CosRX);
    // In case this point lies on X axis then quit
    C3DPoint Result;
    if ((Y == 0.0f) && (Z == 0.0f)) {
      Result = P.Multiply(X);
      return Result;
    }
    // Get precalculated values
    float YZDist = (float)sqrt(Y*Y + Z*Z);
    // Rotate along Z and X at once
    Result.X = P.X * X - P.Y * YZDist;
    Result.Y = P.X * Y + (P.Y * Y * X - P.Z * Z) / YZDist;
    Result.Z = P.X * Z + (P.Y * Z * X + P.Z * Y) / YZDist;
    return Result;
  };
  /*!
    Returns diffuse ray according to independent random quantities a&b.
    Note that both a and b are from range (0, 1).
    \param a Independent random quantity.
    \param b Independent random quantity.
    \return Diffuse vector.
  */
  inline C3DPoint DiffuseRay(float a, float b) {
    return ChangeOrientation(2*PI*a, (float)asin(1-sqrt(b)));
  };
};

class CFRGB {
public:
  float R, G, B;
  INLINE CFRGB operator*(const float &Value) {
    return CFRGB(R*Value, G*Value, B*Value);
  };
  INLINE CFRGB operator*(const CFRGB &Value) {
    return CFRGB(R*Value.R, G*Value.G, B*Value.B);
  };
  INLINE void operator*=(const float &Value) {
    R *= Value;
    G *= Value;
    B *= Value;
  };
  INLINE void operator*=(const CFRGB &Value) {
    R *= Value.R;
    G *= Value.G;
    B *= Value.B;
  };
  INLINE CFRGB operator/(const float &Value) {
    return CFRGB(R/Value, G/Value, B/Value);
  };
  INLINE void operator/=(const float &Value) {
    R /= Value;
    G /= Value;
    B /= Value;
  };
};

class CFace {
  //! Normal vector of face size 1.
  C3DPoint Normal;
  /*!
    This value represents reflected color. Value (0, 0, 0) means there is not
    reflectance at all, whereas (255, 255, 255) means there is full reflectance.
    You should avoid to set the value to any of those extremes or values near them.
  */
  CFRGB BRDFColor;
  /*!
    Returns point which lies on the surface of the face according to a&b values.
    This method is suitable to get a random face point where a and b are
    independent random quantities from the range (0, 1). To make this method
    more easier we consider just the lightmap rectangle, a and b then correspond
    to u and v vectors.
    \param a Independent random quantity.
    \param b Independent random quantity.
    \return SurfacePoint.
  */
  C3DPoint SufacePoint(float a, float b);
}


/*!
  Point light source.
*/
#define LSATTR_PRIMARY 1
class CLightSource {
public:
  //! Attributes of the light source
  int Attr;
  //! Position of the light source.
  C3DPoint Position;
  //! Intensity of the light source in RGB values.
  CFRGB Intensity;
  //! Face the light source lies on.
  CFace *Face;
};

class CSceneManager {
public:

  /*!
    This method returns Light sources which may have some impact on visible part
    of the scene. Parameter MaxLightSources specifies the maximum number of
    returned light sources. The retrieved ones are the most important.
    \param MaxLightSources The maximum number of retrieved light sources.
    \param LightSources Array of light sources to return.
    \return Number of returned light sources.
  */
  int GetLightSources(int MaxLightSources, CFace *LightSources);

  /*!
    This routine sends a ray specified by Point, Vector and Face on which it
    starts, into the scene and Returns point of the first intersection (HitPoint) and
    hit face (HitFace). It returns 1 in case something was hit, 0 otherwise.
    \param Point Starting point of the ray.
    \param Vector Vector which specify the direction of the ray.
    \param Face Face the ray starts on. Actually Point lies on the Face.
    \param HitPoint Retrieved intersection of the ray.
    \param HitFace Face which was hit.
    \return 1 in case something was hit, 0 otherwise.
  */
  int TraceRay(C3DPoint Point, C3DPoint Vector, CFace *Face, C3DPoint &HitPoint, CFace *&HitFace);

};
