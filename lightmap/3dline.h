#ifndef _3dline_h_
#define _3dline_h_

#include "auxiliary.h"
#include "3dpoint.h"

class C3DLine {
 public:
  // Point on this line
  C3DPoint A;
  // Directional vector of this line
  C3DPoint V;
  // Constructor
  INLINE C3DLine() {};
  // Constructor
  INLINE C3DLine(C3DPoint A, C3DPoint B) {
    this->A.X = A.X;
    this->A.Y = A.Y;
    this->A.Z = A.Z;
    V.X = B.X - A.X;
    V.Y = B.Y - A.Y;
    V.Z = B.Z - A.Z;
  };
};

#endif