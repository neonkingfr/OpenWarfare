#ifndef _2DPOINT_H_
#define _2DPOINT_H_

#include <math.h>
#include "auxiliary.h"

class C2DPoint {
 public:
  // Coordinates
  float I, J;
  // Constructor
  INLINE C2DPoint() {};
  // Constructor
  INLINE C2DPoint(float I, float J) {
    this->I = I; this->J = J;
  };
  // Magnitude
  INLINE float Magnitude() {
    return (float)sqrt(I*I + J*J);
  };
  // Distance
  INLINE float Distance(C2DPoint P) {
    return (float)sqrt((P.I - I) * (P.I - I) + (P.J - J) * (P.J - J));
  };
  // Normalizing, returns magnitude of the original vector
  INLINE float Normalize() {
    // Getting the magnitude
    float M = Magnitude();
    // To reduce dividing, m will be the multiplier
    float m = 1/M;
    I*=m; J*=m;
    return M;
  };
  // Revert this vector
  INLINE C2DPoint Revert() {
    C2DPoint Result(-I, -J);
    return Result;
  };
  // Add a point
  INLINE C2DPoint Add(C2DPoint P) {
    C2DPoint Result(P.I + I, P.J + J);
    return Result;
  };
  // Focus: Average point between this and P
  INLINE C2DPoint GetFocus(C2DPoint P) {
    C2DPoint Result((P.I + this->I)/2, (P.J + this->J)/2);
    return Result;
  };
  // Scalar product (cos of angle between vectors (if they are size 1))
  INLINE float GetScalarProduct(C2DPoint P) {
    return P.I*I + P.J*J;
  };
  // Returns 1 if points are near to each other in euklidas space
  INLINE int Near(C2DPoint P) {
    if ((ABS(P.I - I) < DEFLECTION) && 
        (ABS(P.J - J) < DEFLECTION))
      return 1;
    else
      return 0;
  };
  // Multiple this vector by number N. No change is made to this vector.
  INLINE C2DPoint Multiple(float N) {
    C2DPoint Result;
    Result.I = I * N;
    Result.J = J * N;
    return Result;
  };
};

#endif
