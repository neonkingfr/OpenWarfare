#ifndef _2dline_h_
#define _2dline_h_

#include <assert.h>
#include "auxiliary.h"
#include "2dpoint.h"

//  +       B
//  |      /
//  |   V /
//  |    /\   x (right from line)
//  |   /   \
//  |  A      \
//  |           N (normal vector [V.J, -V.I])
//  0------------- +

class C2DLine {
 public:
  // Offset to line, along the normal vector.
  // Distance from (0,0) to the line
  float Dist;
  // Normal vector of this line, arbitraty size
  C2DPoint NVector;
  // Constructor
  INLINE C2DLine() {};
  // Constructor
  INLINE C2DLine(C2DPoint A, C2DPoint B) {
    Init(A, B);
  };
  // Constructor
  INLINE C2DLine(C2DPoint A, C2DPoint B, C2DPoint RightPoint) {
    Init(A, B, RightPoint);
  };
  // Initialization
  INLINE void Init(C2DPoint A, C2DPoint B) {
    NVector.I = B.J - A.J;
    NVector.J = A.I - B.I;
    NVector.Normalize();
    Dist = -NVector.I*A.I - NVector.J*A.J;
  };
  // Initialization, Right will be on right side of the line
  INLINE void Init(C2DPoint A, C2DPoint B, C2DPoint RightPoint) {
    Init(A, B);
    if (!InRight(RightPoint)) *this = Revert();
  }
  // Returns 1 if Point is right from this line, 0 otherwise
  // (in the direction of normal vector)
  INLINE int InRight(C2DPoint Point) {
    return (Point.I * NVector.I + Point.J * NVector.J + Dist > 0 ) ? 1: 0;
  };
  // Returned line will represent the same line, but the NVector will be reverted.
  INLINE C2DLine Revert() {
    C2DLine Line;
    Line.NVector = NVector.Revert();
    Line.Dist = -Dist;
    return Line;
  };
  // Returns distance of specified point
  INLINE float Distance(C2DPoint Point) {
    return Point.I * NVector.I + Point.J * NVector.J + Dist;
  }
  // Returns intersection of this and specified line.
  INLINE C2DPoint Intersection(C2DLine Line) {
    C2DPoint Result;
    float Denominator = Line.NVector.I * NVector.J - Line.NVector.J * NVector.I;
    assert(Denominator != 0.0f);
    Result.I = (Line.NVector.J * Dist - Line.Dist * NVector.J) / Denominator;
    Result.J = (Line.Dist * NVector.I - Line.NVector.I * Dist) / Denominator;
    return Result;
  }
};

#endif