#ifndef _face_h_
#define _face_h_

#include <d3d8.h>
#include <class/array.hpp>
#include "polygon.h"
#include "rgb.h"
#include "textureman.h"

//         U (Width)
//  P-------------->
//  | ..........>
//  | >.........>
//  | >..........
//  |
// V| (Height)
//  |
//  |
//  |
//  v

// Light Map Points Distance
#define LMP_DISTANCE 16
// Determines distance from face which can have some influence.
#define FACEAREA (LMP_DISTANCE*1.5f)

// Result status of GetLightPoint method
#define GLP_OUTSIDE   0
#define GLP_ALIGNED   1
#define GLP_NOCHANGE  2

class CFace;
class CLMP;

// The Patch
class CPatch {
public:
  // Face this patch belongs to
  CFace *Face;
  // Position of the top left corner of the patch in the lightmap
  CI2DPoint P;
  // Size of the patch - 1, 3 or 9
  int Size;
  // Sets B values of this patch
  void SetValue(CFRGB Value);
  // Splits Patch into 9 patches. Returns common size of new patches.
  int Split(CPatch *Patch);
  // Returns LMP of this patch
  CLMP *GetLMP();
};


class CSector;
//! LightMap Point
/*!
  This structure represents space coordinate of point in the light map.
*/
class CLMP {
public:
  //! Determines wheter this point belongs to the polygon or not.
  int IsVoid;
  //! The closest surface point.
  C2DPoint SurfacePoint;
  // Pointer to sector this point lies in.
  CSector *Sector;
};

class CFace : public CPolygon {
private:
  // Recursive function splits lightmap of this polygon into patches. Returns number of added patches.
  int GetBasicPatchesPart(CI2DPoint TL, CI2DPoint BR, CPatch *BasicPatch);
  /*!
    Alignes specified point according to specified abscissa, right point and distance.
    Only point which lies in the left side of the line is considered.
  */
  int GetLinePoint(C2DPoint A, C2DPoint B, C2DPoint &Point, C2DPoint RightPoint, float Distance);
  /*!
    Alignes specified point according to this face and distance. New point is the
    most closer point from polygon itself. 
    \param Point Point to align.
    \distance Considered distance for aligning. Further points are not to be aligned.
    \return 1 in case point was aligned, 0 otherwise.
  */
  int GetSurfacePoint(C2DPoint &Point, float Distance);
public:
  // Mapping point
  C3DPoint P;
  // Mapping vectors of size 1
  C3DPoint U, V;
  // Width and Height of this LightMap, Width*Height == #RGB
  int Width, Height;
  // Points
  CTextureArea *TextureArea;
  // Points which corresponds to light map points
  CLMP *LMP;
  // E - own energy of face
  CFRGB E;
  // ro - reflectance; 0 - no reflectance, 1 - full reflectance
  float ro;
  // Importancy of this face as a light source. This variable is only
  // actualized by function RedistributeEnergy()
  float Importancy;
  // Auxiliary variable for computing. Determines how many times this face have already been visited.
  int Visited;
  // Returns 2D point according to P, U and V. Point P has to lie on the surface
  C2DPoint MapSurfacePointTo2D(C3DPoint X);
  C3DPoint Map2DToSurfacePoint(C2DPoint X);
  // Initialization of Face
  void Init(C3DPoint *Vertex, int VertexCount, CPlane Plane, CFRGB E, float ro);
  // Deinitialization of Face
  void Done();
  // Drawing of this face
  void Draw(LPDIRECT3DDEVICE8 pd3dDevice, IDirect3DTexture8 *pTexture);
  // Splits lightmap of this polygon into patches. Returns number of added patches.
  int GetBasicPatches(CPatch *BasicPatch);
  // Returns a point on the surface
  C3DPoint SurfacePoint(float a, float b);
  // After calling this method you can be sure TextureArea is not NULL
  void FetchTextureArea();
};

TypeIsSimple(CFace)

#endif