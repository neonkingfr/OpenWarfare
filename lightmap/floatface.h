#ifndef _floatface_h_
#define _floatface_h_

#include "3dpoint.h"
#include "face.h"

// Maximum count of FloatFacePart in one FloatFace
#define MAX_FFP 32

class CFloatFacePart : public CPolygon {
public:
  // Pointer to Face this FFP belongs to
  CFace *ParentFace;
  // Pointers to next and previous FaceParts in sector
  CFloatFacePart *Next, *Prev;
  // Initialization of FloatFacePart and adding it to LinkList
  void Init(CPolygon Polygon, CFace *ParentFace, CFloatFacePart *Head);
  // Done - removing from LinkList. Note that we must not delete Map array.
  void Done();
};

class CBSP;
class CSector;

class CFloatFace : public CFace {
private:
  // Recursive function for inserting
  void InsertPart(CBSP *BSPNode, CPolygon Polygon);
public:
  // Array of FFP
  CFloatFacePart AFFP[MAX_FFP];
  // Number of FFP in AFFP array
  int FFPCount;
  // Initialization of FloatFace
  void Init(C3DPoint *Vertex, int VertexCount, CPlane Plane, CFRGB E, float ro);
  // Inserts its FFP into sectors
  void Insert(CBSP *BSP);
  // Removes its FFP from sectors
  void Remove();
  // Moves this FF according to Vector
  void Move(CBSP *BSP, C3DPoint Vector);
  // Rotates this FF according to Vector
  void RotateY(CBSP *BSP, float Angle);
  //void ChangeE(CBSP *BSP, float Angle);
};

#endif