#ifndef _scene_h_
#define _scene_h_

#include <d3d8.h>
#include <string.h>
#include <class/array.hpp>
#include "face.h"
#include "portal.h"
#include "sector.h"
#include "bsp.h"
#include "bspfile.h"
#include "floatface.h"
#include "lightsource.h"
#include "redixqueue.h"

// Maximum count of important lights in Eye neigbourhood
#define MAX_IMPORTANT_LIGHTS 100
// Number of available rays in frame
#define RAYS_COUNT 10000
// Number of rays for dynamic lights
#define DL_RAYS_COUNT 2000
// Max number of Float Faces
#define MAX_FF 32

TypeIsSimple(CFace*)
TypeIsSimple(CSector*)

class CScene {
private:
  friend CBspFile;
  // Scene arrays
  AutoArray<CFace>    AFace;
  AutoArray<CFace*>   ALFace;
  AutoArray<CPortal>  APortal;
  AutoArray<CSector*> AVisList;
  AutoArray<CSector>  ASector;
  AutoArray<CBSP>     ABSP;
  // Float faces
  CFloatFace FF[MAX_FF];
  int FFCount;
  // RedixQueue of Patches
  CRedixQueue<CPatch> RQP;
  // Returns light sources distributed by MC Keller's method
  GetMCLighSources(CFace *Face, int N, CLightSource *LS);
  // Add Light Source and remembers MAX_IMPORTANT_LIGHTS count of the most important light sources
  // Returns new number of items in LS array
  int AddLightSource(CFace **LS, int LSCount, CFace *NewLS);
  // Obtains a Sector from the Position
  CSector *GetSector(C3DPoint Position);
  // Retrieves list of light sources according to importancy in the neighbourhood
  // Returns number of light sources
  int GetLightSources(CSector *Sector, CFace **LS);
  int GetLightSources2(CSector *Sector, CLightSource *LS);
  // Retrieves list of primary patches
  // Returns number of those patches
  int GetPrimaryPatches(CSector *Sector, CPatch *PrimaryPatch);
  // Zero Visited flag in all sectors in the neighbourhood
  void ZeroVisited(CSector *Sector);
  // Drawing of sectors in the neighbourhood
  void Draw(CSector *Sector, LPDIRECT3DDEVICE8 pd3dDevice, IDirect3DTexture8 *pTexture, C3DPoint EyePosition);
  // Determines wheter point A which lies on AFace in ASector is ivsible from B on BFace in BSector or not
  int PointsAreVisible(C3DPoint A, CFace *AFace, CSector *ASector, C3DPoint B, CFace *BFace, CSector *BSector);
  // Sets an energy of the Patch from all light sources LS
  void SetPatchEnergy(CPatch *Patch, CLightSource *LS, int LSCount);
  // Refines one patch
  void RefinePatch(CLightSource *LS, int LSCount);
  // Trace a ray
  int TraceRay(C3DPoint Point, C3DPoint Vector, CFace *Face, CSector *Sector, C3DPoint &HitPoint, CFace *&HitFace, CSector *&HitSector);
public:
  // Initialization of scene
  void Init();
  // Initialization from file
  void Init(char *FileName);
  // Deinitialization of scene
  void Done();
  // Rendering of one frame
  void Render2(LPDIRECT3DDEVICE8 pd3dDevice, IDirect3DTexture8 *pTexture, C3DLine Eye, int RefineCount);
  // This method add a FloatFace to position Position
  void AddFF(C3DPoint Position);
  void MoveFF(C3DPoint Vector);
  void RotateYFF(float Angle);
  void ChangeREFF(CFRGB Color); // Relative E Float Face
  void DeleteFF();
};

#endif