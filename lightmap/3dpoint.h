#ifndef _3DPOINT_H_
#define _3DPOINT_H_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <crtdbg.h>
#include "auxiliary.h"
#include "2dpoint.h"

// Mapping plane (colinear with euklidas base planes)
#define MP_XY 0
#define MP_XZ 1
#define MP_YZ 2
//
#define DIFUSERAY_COUNT 65536

class C3DPoint {
 public:
  // Coordinates
  float X, Y, Z;
  // Static array of difuse rays
  static C3DPoint DifuseRay[DIFUSERAY_COUNT];
  static int CurrDifuseRay;
  // Constructor
  INLINE C3DPoint() {};
  // Constructor
  INLINE C3DPoint(float X, float Y, float Z) {
    this->X = X; this->Y = Y; this->Z = Z;
  };
  // Magnitude
  INLINE float Magnitude() {
    return (float)sqrt(X*X + Y*Y + Z*Z);
  };
  // Normalizing, returns magnitude of the original vector
  INLINE float Normalize() {
    // Getting the magnitude
    float M = Magnitude();
    // To reduce dividing, m will be the multiplier
    float m = 1/M;
    X*=m; Y*=m; Z*=m;
    return M;
  };
  // Revert this vector
  INLINE C3DPoint Revert() {
    C3DPoint Result(-X, -Y, -Z);
    return Result;
  };
  // Focus: Average point between this and P
  INLINE C3DPoint GetFocus(C3DPoint P) {
    C3DPoint Result((P.X + this->X)/2, (P.Y + this->Y)/2, (P.Z + this->Z)/2);
    return Result;
  };
  // Return vector: P - this
  INLINE C3DPoint GetVector(C3DPoint P) {
    C3DPoint Result(P.X - this->X, P.Y - this->Y, P.Z - this->Z);
    return Result;
  };
  // Scalar product (cos of angle between vectors (if they are size 1))
  INLINE float GetScalarProduct(C3DPoint P) {
    return P.X*X + P.Y*Y + P.Z*Z;
  };
  // Vector product (result will be orthogonal to both this vector and B vector)
  // (right hand rule)
  INLINE C3DPoint GetVectorProduct(C3DPoint B) {
    C3DPoint Result;
    Result.X = this->Y*B.Z - B.Y*this->Z;
    Result.Y = this->Z*B.X - B.Z*this->X;
    Result.Z = this->X*B.Y - B.X*this->Y;
    return Result;
  };
  // Returns 1 if points are near to each other in euklidas space
  INLINE int Near(C3DPoint P) {
    if ((ABS(P.X - X) < DEFLECTION) && 
        (ABS(P.Y - Y) < DEFLECTION) &&
        (ABS(P.Z - Z) < DEFLECTION))
      return 1;
    else
      return 0;
  };
  // Returns one of MP_XX indicating what plane is the best to map points to
  INLINE int GetMP() {
    if (ABS(X) > ABS(Y))
      if (ABS(X) > ABS(Z)) return MP_YZ;
      else return MP_XY;
    else
      if (ABS(Y) > ABS(Z)) return MP_XZ;
      else return MP_XY;
  };
  // Returns 2d point mapped on MappingPlane
  // FLY in future there will be faster improvement to create functions like Get2DPoint_MP_XY ...
  INLINE C2DPoint Get2DPoint(int MappingPlane) {
    C2DPoint Result;
    switch (MappingPlane) {
      case MP_XY:
        Result.I = X;
        Result.J = Y;
        break;
      case MP_XZ:
        Result.I = X;
        Result.J = Z;
        break;
      case MP_YZ:
        Result.I = Y;
        Result.J = Z;
        break;
    };
    return Result;
  };
  // Returns distance from Point according to one axis 
  INLINE float GetDistance(int MappingPlane, C3DPoint Point) {
    float Result;
    switch (MappingPlane) {
      case MP_XY:
        Result = ABS(Point.Z - Z);
        break;
      case MP_XZ:
        Result = ABS(Point.Y - Y);
        break;
      case MP_YZ:
        Result = ABS(Point.X - X);
        break;
    };
    return Result;
  };
  // Multiply this point as a vector (should be size 1 to Number represents the length). Returns the vector.
  INLINE C3DPoint Multiply(float Number) {
    C3DPoint Result(X*Number, Y*Number, Z*Number);
    return Result;
  };
  // Moves this point by vector V
  INLINE C3DPoint Move(C3DPoint V) {
    C3DPoint Result(X + V.X, Y + V.Y, Z + V.Z);
    return Result;
  };
  // Consider this point as a vector. This function returns an arbitraty vector
  // which is no more than 90 degree to this vector and should be from distribution
  // according to Gausian curve
  // This vector must be length 1
  INLINE C3DPoint GetDifuseRay() {
    C3DPoint T;
    T = DifuseRay[CurrDifuseRay];
//---
    //T.Z += 2.0;
    //T.Normalize();
//---
    if (CurrDifuseRay < DIFUSERAY_COUNT - 1)
      CurrDifuseRay++;
    else
      CurrDifuseRay = 0;

    // If this vector points in Z direction already, we need not (better to say must not) transform it
    if ((X == 0) && (Y == 0)) {
      T.Z *= Z;
      return T; 
    }
    // There is proper vector in T and now we will transform it according to this vector
    C3DPoint T1, T2;
    float Alfa, Beta;
    // Alfa, Beta
    Alfa = (float)acos(Y/((float)sqrt(X*X + Y*Y)));
    if (X < 0) Alfa = -Alfa;
    Beta = (float)acos(Z);
    // T1
    T1.X = T.X;
    T1.Y = T.Y * (float)cos(-Beta) - T.Z * (float)sin(-Beta);
    T1.Z = T.Y * (float)sin(-Beta) + T.Z * (float)cos(-Beta);
    // T2
    T2.X = T1.X * (float)cos(-Alfa) - T1.Y * (float)sin(-Alfa);
    T2.Y = T1.X * (float)sin(-Alfa) + T1.Y * (float)cos(-Alfa);
    T2.Z = T1.Z;

    // Returning of T2
    return T2;
  };
  // Resets the difuse ray
  INLINE static void ResetDifuseRay() {
    CurrDifuseRay = 0;
  };
  // Returns this vector rotated along Y axis by Angle
  INLINE C3DPoint RotateY(float Angle) {
    C3DPoint Result;
    Result.X = X * (float)cos(Angle) - Z * (float)sin(Angle);
    Result.Y = Y;
    Result.Z = X * (float)sin(Angle) + Z * (float)cos(Angle);
    return Result;
  };
  // Returns this vector rotated along point P and Y axis by Angle
  INLINE C3DPoint RotateY(C3DPoint P, float Angle) {
    C3DPoint Result;
    Result.X = (X-P.X) * (float)cos(Angle) - (Z-P.Z) * (float)sin(Angle) + P.X;
    Result.Y = Y;
    Result.Z = (X-P.X) * (float)sin(Angle) + (Z-P.Z) * (float)cos(Angle) + P.Z;
    return Result;
  };
  // Returns this vector rotated along Z axis by Angle
  INLINE C3DPoint RotateZ(float Angle) {
    C3DPoint Result;
    Result.X = X * (float)cos(Angle) - Y * (float)sin(Angle);
    Result.Y = X * (float)sin(Angle) + Y * (float)cos(Angle);
    Result.Z = Z;
    return Result;
  };
  // Initialization of array of DifuseRays
  static void InitDifuseRay() {
    FILE *f;
    f = fopen("difuserays.bin", "rb");
    _ASSERT(f);
    fread(DifuseRay, sizeof(C3DPoint)*DIFUSERAY_COUNT, 1, f);
    fclose(f);
  };
  C3DPoint ChangeOrientation(float rx, float ry) {
    // Get precalculated values
    float SinRX = (float) sin(rx);
    float CosRX = (float) cos(rx);
    float SinRY = (float) sin(ry);
    float CosRY = (float) cos(ry);
    // Get the unrotated point according to rx and ry
    C3DPoint P;
    P.X = CosRY;
    P.Y = (float)(-SinRY * SinRX);
    P.Z = (float)(SinRY * CosRX);
    // In case this point lies on X axis then quit
    C3DPoint Result;
    if ((Y == 0.0f) && (Z == 0.0f)) {
      Result = P.Multiply(X);
      return Result;
    }
    // Get precalculated values
    float YZDist = (float)sqrt(Y*Y + Z*Z);
    // Rotate along Z and X at once
    Result.X = P.X * X - P.Y * YZDist;
    Result.Y = P.X * Y + (P.Y * Y * X - P.Z * Z) / YZDist;
    Result.Z = P.X * Z + (P.Y * Z * X + P.Z * Y) / YZDist;
    return Result;

/* Easy but slow solution ---
    // Get the unrotated point according to rx and ry
    C3DPoint P;
    P.X = (float)cos(ry);
    P.Y = (float)(-sin(ry) * sin(rx));
    P.Z = (float)(sin(ry) * cos(rx));
    // Get alfa and beta according to current point
    float alfa, beta;
    alfa = (float)acos(X);
    if ((Y != 0) || (Z != 0)) 
      beta = (float)acos(Y/sqrt(Y*Y + Z*Z));
    else
      beta = 0.0f;
    // Rotate using alfa
    C3DPoint R;
    R.X = (float)(P.X * cos(alfa) - P.Y * sin(alfa));
    R.Y = (float)(P.X * sin(alfa) + P.Y * cos(alfa));
    R.Z = P.Z;
    // Rotate using beta
    C3DPoint Result;
    Result.X = R.X;
    Result.Y = (float)(R.Y * cos(beta) - R.Z * sin(beta));
    Result.Z = (float)(R.Y * sin(beta) + R.Z * cos(beta));
    // Return
    return Result;
*/
  };
  INLINE C3DPoint DiffuseRay(float a, float b) {
    return ChangeOrientation(2*PI*a, (float)asin(1-sqrt(b)));
  };

};

#endif
