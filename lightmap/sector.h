#ifndef _sector_h_
#define _sector_h_

#include <string.h>
#include <class/array.hpp>
#include "face.h"
#include "portal.h"
#include "floatface.h"

class CSector {
public:
  // List of faces
  CFace **LFace;
  // Number of faces
  int LFaceCount;
  // List of portals
  CPortal *Portal;
  // Number of portals
  int PortalCount;
  // Visibility List
  CSector **VisList;
  // Number of visible sectors
  int VisListCount;
  // Auxiliary variable for computing. Determines how many times this sector have already been visited.
  int Visited;
  // Empty head of FFP polygons
  CFloatFacePart FFPHead;
  // Initialization of Face
  void Init(CFace **LFace, int LFaceCount, CPortal *Portal, int PortalCount, CSector **VisList, int VisListCount);
  // Returns point which lies right in centre of the sector
  C3DPoint GetFocus();
};

TypeIsSimple(CSector)

#endif