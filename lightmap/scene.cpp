#include <memory.h>
#include "floatface.h"
#include "scene.h"
#include "halton.h"

extern char g_CornerText[10][256];
extern int g_Flag0;
extern int g_PatchCount;
extern CTextureMan g_TextureMan;


int g_Flag0State = -1;

#define AVG_RO 0.5f // Average reflectance in the scene

int CScene::GetMCLighSources(CFace *Face, int N, CLightSource *LS) {
  float w;                // Compensation factor
  float Start;
  int End;
  int Reflections = 0;    // Number of reflections for specified walk
  CFRGB L;                // Radiance
  C3DPoint y;             // Starting point of the ray
  C3DPoint omega;         // Direction of the ray
  CHalton HP2(2), HP3(3); // Halton's generators for primary points
  CHalton HR2(2), HR3(3); // Halton's generators for reflections directions
  int LSCount = 0;        // Number of light sources
  CSector *Sector, *NewSector;
  CFace *NewRFace;
  C3DPoint Newy;

  // Algorithm
  Start = (float)N;
  End = N;
  while (End > 0) {
    Start *= AVG_RO;
    for (int i = (int) Start; i < End; i++) {
      // Select starting point on light source
      y = Face->SurfacePoint(HP2, HP3);
      L = Face->E;
      w = (float)N;
      // Get sector

      Sector = Face->LMP[(Face->Width * Face->Height) / 2].Sector;
      if (Sector == NULL2) {
        Sector = GetSector(Face->GetFocus().Move(Face->Plane.NVector));
        Face->LMP[(Face->Width * Face->Height) / 2].Sector = Sector;
      }

      //Sector = GetSector(Face->GetFocus().Move(Face->Plane.NVector));
      if (!Sector) break;
      // Face for reflections
      CFace *RFace = Face;
      // Trace reflections
      for (int j = 0; j <= Reflections; j++) {
        // Save the light with (L/w) intensite
        (j == 0) ? LS[LSCount].Attr = LSATTR_PRIMARY : LS[LSCount].Attr = 0;
        LS[LSCount].Position = y;
        LS[LSCount].Intensity = L/w;
        LS[LSCount].Face = RFace;
        LS[LSCount].Sector = Sector;
        LSCount++;
        // Difuse scattering
        omega = RFace->Plane.NVector.DiffuseRay(HR2, HR3);
        // Trace ray from y into direction omega
        if (!TraceRay(y, omega, RFace, Sector, Newy, NewRFace, NewSector)) break;
        y = Newy;
        RFace = NewRFace;
        Sector = NewSector;
        // Attenuate and compensate
        L *= AVG_RO/*RFace->BRDFColor*/;
        w *= AVG_RO;
      }
    }
    Reflections++;
    End = (int) Start;
  }
  return LSCount;
}

int CScene::AddLightSource(CFace **LS, int LSCount, CFace *NewLS) {
  int i = 0; // Begin of block to move
  while ((i < LSCount) && (LS[i]->Importancy > NewLS->Importancy)) {
    i++;
  }
  if (i == MAX_IMPORTANT_LIGHTS) return MAX_IMPORTANT_LIGHTS;
  int j = LSCount - i; // Number of items to move
  if (LSCount == MAX_IMPORTANT_LIGHTS) j--;
  if (j > 0) memmove(&LS[i + 1], &LS[i], sizeof(CFace*) * j);
  LS[i] = NewLS;
  return i + j + 1;
}

int CScene::TraceRay(C3DPoint Point, C3DPoint Vector, CFace *Face, CSector *Sector, C3DPoint &HitPoint, CFace *&HitFace, CSector *&HitSector) {
  C3DPoint Intersection;
  int i;
  C3DLine Ray;
  Ray.A = Point;
  Ray.V = Vector;
  int MP = Ray.V.GetMP();

  repeat:

  // Test with all FloatFaces in sector
  CFloatFacePart *NearestFFP = NULL;
  float NearestFFDistance = FLT_MAX;
  C3DPoint NearestIntersection;
  float NewFFDistance;
  CFloatFacePart *FFP = Sector->FFPHead.Next;
  while (FFP != &Sector->FFPHead) {
    if (FFP->Hit(Ray, Intersection)) {
      if ((NewFFDistance = Ray.A.GetDistance(MP, Intersection)) < NearestFFDistance) {
        NearestFFP = FFP;
        NearestFFDistance = NewFFDistance;
        NearestIntersection = Intersection;
      }
    }
    FFP = FFP->Next;
  }
  if (NearestFFP != NULL) {
    HitPoint = NearestIntersection;
    HitFace = NearestFFP->ParentFace;
    HitSector = Sector;
    return 1;
  }

  // Test with all Portals in Sector
  for (i = 0; i < Sector->PortalCount; i++) {
    if (Sector->Portal[i].Hit(Ray, Intersection)) {
      Sector = Sector->Portal[i].NeiSector;
      goto repeat;
    }
  }

  // Test with all Faces in this sector
  // FLY there should also may be testing according to distance of the intersection
  for (i = 0; i < Sector->LFaceCount; i++) {
    CFace *Face = Sector->LFace[i];
    if (Face->Hit(Ray, Intersection)) {
      HitPoint = Intersection;
      HitFace = Face;
      HitSector = Sector;
      return 1;
    }
  }
  return 0;
}

int CScene::PointsAreVisible(C3DPoint A, CFace *AFace, CSector *ASector, C3DPoint B, CFace *BFace, CSector *BSector) {
  // Test Faces are suitably oriented
  if ((!AFace->Plane.InFront(B)) || (!BFace->Plane.InFront(A))) {
    return 0;
  }

  assert(AFace);
  assert(ASector);
  assert(BFace);
  assert(BSector);

  // Test portals
  C3DLine Ray(A, B);
  int PortalHit;
  CSector *Sector = ASector;
  C3DPoint Intersection;
  while (Sector != BSector) {
    // Test with all Portals in Sector
    PortalHit = 0;
    for (int i = 0; i < Sector->PortalCount; i++) {
      if (Sector->Portal[i].Hit(Ray, Intersection)) {
        Sector = Sector->Portal[i].NeiSector;
        PortalHit = 1;
        break;
      }
    }
    if (!PortalHit) return 0;
  }

  return 1;
}

void CScene::SetPatchEnergy(CPatch *Patch, CLightSource *LS, int LSCount) {
  // Get point which lies in the center of the patch
  float PatchCentre = (float)Patch->Size/2.0f;
  float CentreI = ((float)Patch->P.I + PatchCentre) * LMP_DISTANCE;
  float CentreJ = ((float)Patch->P.J + PatchCentre) * LMP_DISTANCE;
  CFace *Face = Patch->Face;
  // Get 3D coordinate of centre of the patch
  C3DPoint PatchPoint = Face->P.Move(Face->U.Multiply(CentreI).Move(Face->V.Multiply(CentreJ)));
  // Initialize sector
  C3DPoint Vector;
  CFRGB Value(0.0f, 0.0f, 0.0f);
  CLMP *pLMP = Patch->GetLMP();
  if (pLMP->IsVoid) return;
  // If Map[...].Sector is not initialized (NULL2), then do it
  if (pLMP->Sector == NULL2) {
    pLMP->Sector = GetSector(PatchPoint.Move(Face->Plane.NVector));
  }
  if (pLMP->Sector != NULL) {
    for (int i = 0; i < LSCount; i++) {
      Vector = LS[i].Position.GetVector(PatchPoint);
      float Magnitude = Vector.Normalize();
      // FLY
      if (PointsAreVisible(/*Face->Map2DToSurfacePoint(pLMP->SurfacePoint)*/PatchPoint, Face, pLMP->Sector, LS[i].Position, LS[i].Face, LS[i].Sector)) {
        Value = Value.Add(LS[i].Intensity.Multiply(Face->ro * LS[i].Face->Plane.NVector.GetScalarProduct(Vector) * (100.0f/Magnitude)));
      }
    }
  }
  // Increment
  g_PatchCount++;
  // Assign the value
  Patch->SetValue(Value);
}

void CScene::Init() {
  C3DPoint Point1;
  C3DPoint V[MAX_VERTICES];
  CPlane Plane1;
  CFRGB FRGB1;

  // Init
  AFace.Resize(5);
  ALFace.Resize(8);
  APortal.Resize(2);
  AVisList.Resize(4);
  ASector.Resize(2);
  ABSP.Resize(1);

  // Faces
  // 1. Sector
  V[0].X =  150; V[0].Y = -50; V[0].Z =  50;
  V[1].X =  150; V[1].Y = -50; V[1].Z = -50;
  V[2].X =  -50; V[2].Y = -50; V[2].Z = -50;
  V[3].X =  -50; V[3].Y = -50; V[3].Z =  50;
  Point1.X = 0; Point1.Y = 1; Point1.Z = 0;
  Plane1.Init(Point1, V[0]);
  FRGB1.R = 0; FRGB1.G = 0; FRGB1.B = 0;
  AFace[0].Init(V, 4, Plane1, FRGB1, 0.4f);

  V[0].X =  150; V[0].Y = -50; V[0].Z =  50;
  V[1].X =  -50; V[1].Y = -50; V[1].Z =  50;
  V[2].X =  -50; V[2].Y =  50; V[2].Z =  50;
  V[3].X =  150; V[3].Y =  50; V[3].Z =  50;
  Point1.X = 0; Point1.Y = 0; Point1.Z = -1;
  Plane1.Init(Point1, V[0]);
  FRGB1.R = 0; FRGB1.G = 0; FRGB1.B = 0;
  AFace[1].Init(V, 4, Plane1, FRGB1, 0.4f);

  V[0].X =  -50; V[0].Y =  50; V[0].Z =  50;
  V[1].X =  -50; V[1].Y =  50; V[1].Z = -50;
  V[2].X =  150; V[2].Y =  50; V[2].Z = -50;
  V[3].X =  150; V[3].Y =  50; V[3].Z =  50;
  Point1.X = 0; Point1.Y = -1; Point1.Z = 0;
  Plane1.Init(Point1, V[0]);
  FRGB1.R = 0; FRGB1.G = 500; FRGB1.B = 0;
  AFace[2].Init(V, 4, Plane1, FRGB1, 0.4f);

  V[0].X = -50; V[0].Y =  50; V[0].Z =  50;
  V[1].X = -50; V[1].Y = -50; V[1].Z =  50;
  V[2].X = -50; V[2].Y = -50; V[2].Z = -50;
  V[3].X = -50; V[3].Y =  50; V[3].Z = -50;
  Point1.X = 1; Point1.Y = 0; Point1.Z = 0;
  Plane1.Init(Point1, V[0]);
  FRGB1.R = 0; FRGB1.G = 0; FRGB1.B = 0;
  AFace[3].Init(V, 4, Plane1, FRGB1, 0.4f);

  // 2. Sector
  V[0].X = 150; V[0].Y =  50; V[0].Z =  50;
  V[1].X = 150; V[1].Y =  50; V[1].Z = -50;
  V[2].X = 150; V[2].Y = -50; V[2].Z = -50;
  V[3].X = 150; V[3].Y = -50; V[3].Z =  50;
  Point1.X = -1; Point1.Y = 0; Point1.Z = 0;
  Plane1.Init(Point1, V[0]);
  FRGB1.R = 0; FRGB1.G = 0; FRGB1.B = 0;
  AFace[4].Init(V, 4, Plane1, FRGB1, 0.4f);


  // LFaces
  ALFace[0] = &AFace[0];
  ALFace[1] = &AFace[1];
  ALFace[2] = &AFace[2];
  ALFace[3] = &AFace[3];
  ALFace[4] = &AFace[0];
  ALFace[5] = &AFace[1];
  ALFace[6] = &AFace[2];
  ALFace[7] = &AFace[4];

  // Portals

  // 1. Sector
  V[0].X = 50; V[0].Y =  50; V[0].Z =  50;
  V[1].X = 50; V[1].Y =  50; V[1].Z = -50;
  V[2].X = 50; V[2].Y = -50; V[2].Z = -50;
  V[3].X = 50; V[3].Y = -50; V[3].Z =  50;
  Point1.X = -1; Point1.Y = 0; Point1.Z = 0;
  Plane1.Init(Point1, V[0]);
  APortal[0].Init(V, 4, Plane1, &ASector[1]);

  // 2. Sector
  V[0].X =  50; V[0].Y =  50; V[0].Z =  50;
  V[1].X =  50; V[1].Y = -50; V[1].Z =  50;
  V[2].X =  50; V[2].Y = -50; V[2].Z = -50;
  V[3].X =  50; V[3].Y =  50; V[3].Z = -50;
  Point1.X = 1; Point1.Y = 0; Point1.Z = 0;
  Plane1.Init(Point1, V[0]);
  APortal[1].Init(V, 4, Plane1, &ASector[0]);

  // Sectors & VisLists
  AVisList[0] = &ASector[0];
  AVisList[1] = &ASector[1];
  AVisList[2] = &ASector[1];
  AVisList[3] = &ASector[0];

  ASector[0].Init(&ALFace[0], 4, &APortal[0], 1, &AVisList[0], 2);
  ASector[1].Init(&ALFace[4], 4, &APortal[1], 1, &AVisList[2], 2);

  // BSP nodes
  ABSP[0].Plane = APortal[0].Plane;
  ABSP[0].FrontType = BSP_TYPE_LEAF;
  ABSP[0].FrontLeaf = &ASector[0];
  ABSP[0].BackType = BSP_TYPE_LEAF;
  ABSP[0].BackLeaf = &ASector[1];

  FFCount = 0;
}

void CScene::Init(char *FileName) {
  CBspFile A(FileName, this);
  FFCount = 0;
}

void CScene::Done() {
  int i;
  // AFace finalization
  for (i = 0; i < AFace.Size(); i++) AFace[i].Done();
}

CSector *CScene::GetSector(C3DPoint Position) {
  // Find a node
  CBSP *Node = &ABSP[0];
  while (1) {
    // In Front
    if (Node->Plane.InFront(Position)) {
      switch (Node->FrontType) {
      case BSP_TYPE_NODE:
        Node = Node->FrontNode;
        break;
      case BSP_TYPE_LEAF:
        return Node->FrontLeaf;
      case BSP_TYPE_NULL:
        return NULL;
      }
    }
    // In Back
    else {
      switch (Node->BackType) {
      case BSP_TYPE_NODE:
        Node = Node->BackNode;
        break;
      case BSP_TYPE_LEAF:
        return Node->BackLeaf;
      case BSP_TYPE_NULL:
        return NULL;
      }
    }
  }
}

int CScene::GetLightSources(CSector *Sector, CFace **LS) {
  //--------------------------------------------------------
  int LSCount = 0;
  //--------------------------------------------------------
  int VisitedLevel = Sector->Visited;
  CSector *Sector1, *Sector2;
  int FVisitedLevel = -1;
  int FFVisitedLevel = -1;
  CFloatFacePart *FFP;
  for (int i = 0; i < Sector->VisListCount; i++) {
    Sector1 = Sector->VisList[i];
    for (int j = 0; j < Sector1->VisListCount; j++) {
      Sector2 = Sector1->VisList[j];
      if (Sector2->Visited <= VisitedLevel) {
        //--------------------------------------------------------
        for (int k = 0; k < Sector2->LFaceCount; k++) {
          if (FVisitedLevel == -1) {
            FVisitedLevel = Sector2->LFace[k]->Visited;
          }
          if (Sector2->LFace[k]->Visited <= FVisitedLevel) {
            if (Sector2->LFace[k]->Importancy > 0) {
              LSCount = AddLightSource(LS, LSCount, Sector2->LFace[k]);
            }
            Sector2->LFace[k]->Visited++;
          }
        }
        //--------------------------------------------------------
        FFP = Sector2->FFPHead.Next;
        while (FFP != &Sector2->FFPHead) {
          if (FFVisitedLevel == -1) {
            FFVisitedLevel = FFP->ParentFace->Visited;
          }
          if (FFP->ParentFace->Visited <= FFVisitedLevel) {
            //--------------------------------------------------------
            LSCount = AddLightSource(LS, LSCount, FFP->ParentFace);
            //--------------------------------------------------------
            FFP->ParentFace->Visited++;
          }
          FFP = FFP->Next;
        }
        Sector2->Visited++;
      }
    }
  }
  //--------------------------------------------------------
  return LSCount;
  //--------------------------------------------------------
}

int CScene::GetLightSources2(CSector *Sector, CLightSource *LS) {
  //--------------------------------------------------------
  int LSCount = 0;
  //--------------------------------------------------------
  int VisitedLevel = Sector->Visited;
  CSector *Sector1, *Sector2;
  int FVisitedLevel = -1;
  int FFVisitedLevel = -1;
  CFloatFacePart *FFP;
  for (int i = 0; i < Sector->VisListCount; i++) {
    Sector1 = Sector->VisList[i];
    for (int j = 0; j < Sector1->VisListCount; j++) {
      Sector2 = Sector1->VisList[j];
      if (Sector2->Visited <= VisitedLevel) {
        //--------------------------------------------------------
        for (int k = 0; k < Sector2->LFaceCount; k++) {
          if (FVisitedLevel == -1) {
            FVisitedLevel = Sector2->LFace[k]->Visited;
          }
          if (Sector2->LFace[k]->Visited <= FVisitedLevel) {
            if (Sector2->LFace[k]->E.GetIntensity() > 0) {
              LS[LSCount].Intensity = Sector2->LFace[k]->E;
              LS[LSCount].Position = Sector2->LFace[k]->GetFocus();
              LS[LSCount].Face = Sector2->LFace[k];
              LS[LSCount].Sector = Sector2;
              LSCount++;
            }
            Sector2->LFace[k]->Visited++;
          }
        }
        //--------------------------------------------------------
        FFP = Sector2->FFPHead.Next;
        while (FFP != &Sector2->FFPHead) {
          if (FFVisitedLevel == -1) {
            FFVisitedLevel = FFP->ParentFace->Visited;
          }
          if (FFP->ParentFace->Visited <= FFVisitedLevel) {
            //--------------------------------------------------------
            if (FFP->ParentFace->E.GetIntensity() > 0) {
              LS[LSCount].Intensity = FFP->ParentFace->E;
              LS[LSCount].Position = FFP->ParentFace->GetFocus();
              LS[LSCount].Face = FFP->ParentFace;
              LS[LSCount].Sector = Sector2;
              LSCount++;
            }
            //--------------------------------------------------------
            FFP->ParentFace->Visited++;
          }
          FFP = FFP->Next;
        }
        Sector2->Visited++;
      }
    }
  }
  //--------------------------------------------------------
  return LSCount;
  //--------------------------------------------------------
}

int CScene::GetPrimaryPatches(CSector *Sector, CPatch *PrimaryPatch) {
  //--------------------------------------------------------
  int PrimaryPatchCount = 0;
  //--------------------------------------------------------
  int VisitedLevel = Sector->Visited;
  CSector *Sector1, *Sector2;
  int FVisitedLevel = -1;
  int FFVisitedLevel = -1;
  CFloatFacePart *FFP;
  for (int i = 0; i < Sector->VisListCount; i++) {
    Sector1 = Sector->VisList[i];
    for (int j = 0; j < Sector1->VisListCount; j++) {
      Sector2 = Sector1->VisList[j];
      if (Sector2->Visited <= VisitedLevel) {
        //--------------------------------------------------------
        for (int k = 0; k < Sector2->LFaceCount; k++) {
          if (FVisitedLevel == -1) {
            FVisitedLevel = Sector2->LFace[k]->Visited;
          }
          if (Sector2->LFace[k]->Visited <= FVisitedLevel) {
            PrimaryPatchCount += Sector2->LFace[k]->GetBasicPatches(&PrimaryPatch[PrimaryPatchCount]);
            Sector2->LFace[k]->Visited++;
          }
        }
        //--------------------------------------------------------
        FFP = Sector2->FFPHead.Next;
        while (FFP != &Sector2->FFPHead) {
          if (FFVisitedLevel == -1) {
            FFVisitedLevel = FFP->ParentFace->Visited;
          }
          if (FFP->ParentFace->Visited <= FFVisitedLevel) {
            //--------------------------------------------------------
            PrimaryPatchCount += FFP->ParentFace->GetBasicPatches(&PrimaryPatch[PrimaryPatchCount]);
            //--------------------------------------------------------
            FFP->ParentFace->Visited++;
          }
          FFP = FFP->Next;
        }
        Sector2->Visited++;
      }
    }
  }
  //--------------------------------------------------------
  return PrimaryPatchCount;
  //--------------------------------------------------------
}

void CScene::ZeroVisited(CSector *Sector) {
  //--------------------------------------------------------
  CFloatFacePart *FFP;
  //--------------------------------------------------------
  CSector *Sector1, *Sector2;
  for (int i = 0; i < Sector->VisListCount; i++) {
    Sector1 = Sector->VisList[i];
    for (int j = 0; j < Sector1->VisListCount; j++) {
      Sector2 = Sector1->VisList[j];
      //--------------------------------------------------------
      for (int k = 0; k < Sector2->LFaceCount; k++) {
        Sector2->LFace[k]->Visited = 0;
      }
      Sector2->Visited = 0;
      FFP = Sector2->FFPHead.Next;
      while (FFP != &Sector2->FFPHead) {
        FFP->ParentFace->Visited = 0;
        FFP = FFP->Next;
      }
      //--------------------------------------------------------
    }
  }
}

void CScene::Draw(CSector *Sector, LPDIRECT3DDEVICE8 pd3dDevice, IDirect3DTexture8 *pTexture, C3DPoint EyePosition) {
/*
  Sector->Draw(pd3dDevice, pTexture, EyePosition);
  return;
*/

  CSector *Sector2;
  int FVisitedLevel = -1;
  int FFVisitedLevel = -1;
  CFloatFacePart *FFP;

  for (int i = 0; i < Sector->VisListCount; i++) {
    Sector2 = Sector->VisList[i];
    // Faces
    for (int j = 0; j < Sector2->LFaceCount; j++) {
      if (FVisitedLevel == -1) {
        FVisitedLevel = Sector2->LFace[j]->Visited;
      }
      if (Sector2->LFace[j]->Visited <= FVisitedLevel) {
        if (Sector2->LFace[j]->Plane.InFront(EyePosition)) {
          Sector2->LFace[j]->Draw(pd3dDevice, pTexture);
        }
        Sector2->LFace[j]->Visited++;
      }
    }
    // FloatFaces
    FFP = Sector2->FFPHead.Next;
    while (FFP != &Sector2->FFPHead) {
      if (FFVisitedLevel == -1) {
        FFVisitedLevel = FFP->ParentFace->Visited;
      }
      if (FFP->ParentFace->Visited <= FFVisitedLevel) {
        FFP->ParentFace->Draw(pd3dDevice, pTexture);
        FFP->ParentFace->Visited++;
      }
      FFP = FFP->Next;
    }
  }
}

void CScene::Render2(LPDIRECT3DDEVICE8 pd3dDevice, IDirect3DTexture8 *pTexture, C3DLine Eye, int RefineCount) {
  CPatch PrimaryPatch[10000];
  int PrimaryPatchCount;
  CLightSource LightSource[1000];
  int LightSourceCount;
  CFace *PrimaryLightSource[1000];
  int PrimaryLightSourceCount;
  CFRGB Color;

  // Find a sector where Eye lies
  CSector *EyeSector = GetSector(Eye.A);
  if (EyeSector == NULL) EyeSector = &ASector[0];
  // Get primary light sources
  PrimaryLightSourceCount = GetLightSources(EyeSector, PrimaryLightSource);
  // Get real light sources
  if (PrimaryLightSourceCount > 0) {
    LightSourceCount = GetMCLighSources(PrimaryLightSource[0], 10, LightSource);
  }
  else {
    LightSourceCount = 0;
  }
  // Get list of primary patches
  PrimaryPatchCount = GetPrimaryPatches(EyeSector, PrimaryPatch);
  // Set energy of primary patches and fill out a RedixQueue structure
  RQP.Clear();
  g_TextureMan.Lock();
  pd3dDevice->SetVertexShader(D3DFVF_XYZ | D3DFVF_TEX1);
  pd3dDevice->SetTexture(0, g_TextureMan.GetTexture());
  for (int i = 0; i < PrimaryPatchCount; i++) {
    SetPatchEnergy(&PrimaryPatch[i], LightSource, LightSourceCount);
    if (PrimaryPatch[i].Size > 1) {
      RQP.Put(PrimaryPatch[i], PrimaryPatch[i].Size);
    }
  }
  // Refinement
  for (i = 0; i < RefineCount; i++) RefinePatch(LightSource, LightSourceCount);
  g_TextureMan.Unlock();

/*
  RQP.Clear();
  CPatch Patch;
  while (RQP.Get(Patch)) {
    SetPatchEnergy(&Patch, LightSource, LightSourceCount);
  }
*/

  // Draw Scene
  Draw(EyeSector, pd3dDevice, pTexture, Eye.A);
  // Zero all visited Sectors and Faces
  ZeroVisited(EyeSector);
}

void CScene::RefinePatch(CLightSource *LS, int LSCount) {
  CPatch Patch;
  CPatch Patches[9];
  int i;
  int NewSize;
  if (RQP.Get(Patch)) {
    if (Patch.Size > 1) {
      NewSize = Patch.Split(Patches);
      for (i = 0; i < 4; i++) {
        SetPatchEnergy(&Patches[i], LS, LSCount);
      }
      for (i = 5; i < 9; i++) {
        SetPatchEnergy(&Patches[i], LS, LSCount);
      }
      if (NewSize > 1) {
        for (i = 0; i < 9; i++) {
          RQP.Put(Patches[i], NewSize);
        }
      }
    }
  }
}

void CScene::AddFF(C3DPoint Position) {
  C3DPoint Point1;
  C3DPoint V[MAX_VERTICES];
  CPlane Plane1;
  CFRGB FRGB1;
  if (FFCount == MAX_FF) return;
/*
  V[0].X =  120; V[0].Y =  0; V[0].Z =  120;
  V[1].X =  120; V[1].Y =  0; V[1].Z = -120;
  V[2].X = -120; V[2].Y =  0; V[2].Z = -120;
  V[3].X = -120; V[3].Y =  0; V[3].Z =  120;
  V[0] = V[0].Move(Position);
  V[1] = V[1].Move(Position);
  V[2] = V[2].Move(Position);
  V[3] = V[3].Move(Position);
  Point1.X = 0; Point1.Y = 1; Point1.Z = 0;
*/

  V[0].X =  0; V[0].Y =  10; V[0].Z = -10;
  V[1].X =  0; V[1].Y =  10; V[1].Z =  10;
  V[2].X =  0; V[2].Y = -10; V[2].Z =  10;
  V[3].X =  0; V[3].Y = -10; V[3].Z = -10;
  V[0] = V[0].Move(Position);
  V[1] = V[1].Move(Position);
  V[2] = V[2].Move(Position);
  V[3] = V[3].Move(Position);
  Point1.X = 1; Point1.Y = 0; Point1.Z = 0;
  Plane1.Init(Point1, V[0]);
  FRGB1.R = 300; FRGB1.G = 0; FRGB1.B = 0;
  FF[FFCount].Init(V, 4, Plane1, FRGB1, 0.6f);
  FF[FFCount].Insert(&ABSP[0]);

  //DistributeFaceEnergyB(&FF[FFCount], 5000);

  FFCount++;
}

void CScene::MoveFF(C3DPoint Vector) {
  //DistributeFaceEnergyNB(&FF[FFCount - 1], DL_RAYS_COUNT);
  FF[FFCount - 1].Move(&ABSP[0], Vector);
  //DistributeFaceEnergyB(&FF[FFCount - 1], DL_RAYS_COUNT);
}

void CScene::RotateYFF(float Angle) {
  //DistributeFaceEnergyNB(&FF[FFCount - 1], DL_RAYS_COUNT);
  FF[FFCount - 1].RotateY(&ABSP[0], Angle);
  //DistributeFaceEnergyB(&FF[FFCount - 1], DL_RAYS_COUNT);
}

void CScene::ChangeREFF(CFRGB Color) {
  FF[FFCount - 1].E = FF[FFCount - 1].E.Add(Color);
}

void CScene::DeleteFF() {
  if (FFCount <= 0) return;
  FFCount--;

  //DistributeFaceEnergyNB(&FF[FFCount], 5000);
  
  FF[FFCount].Remove();
}

