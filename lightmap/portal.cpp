#include "portal.h"
#include <crtdbg.h>

void CPortal::Init(C3DPoint *Vertex, int VertexCount, CPlane Plane, CSector *NeiSector) {
  _ASSERT(VertexCount != 0);
  // Call init of ancestor
  CPolygon::Init(Vertex, VertexCount, Plane);
  // NeiSector
  this->NeiSector = NeiSector;
}
