#include <malloc.h>
#include <crtdbg.h>
#include <class/debuglog.hpp>
#include <class/fltopts.hpp>

#include "plane.h"
#include "face.h"

extern int g_LMPCount;
extern CTextureMan g_TextureMan;

void CPatch::SetValue(CFRGB Value) {
  Face->FetchTextureArea();
  D3DLOCKED_RECT LockedRect = g_TextureMan.GetRect(Face->TextureArea, P.I, P.J);
  for (int i = 0; i < Size; i++) {
    for (int j = 0; j < Size; j++) {
      ((WORD*)LockedRect.pBits)[j] = MAKE_COLOR(MIN((int)Value.R, 255), MIN((int)Value.G, 255), MIN((int)Value.B, 255));
    }
    LockedRect.pBits = &((BYTE*)LockedRect.pBits)[LockedRect.Pitch];
  }
}

int CPatch::Split(CPatch *Patch) {
  int NewSize = Size/3;

  Patch[0].Face = Face;
  Patch[0].P.I = P.I + 0*NewSize;
  Patch[0].P.J = P.J + 0*NewSize;
  Patch[0].Size = NewSize;

  Patch[1].Face = Face;
  Patch[1].P.I = P.I + 1*NewSize;
  Patch[1].P.J = P.J + 0*NewSize;
  Patch[1].Size = NewSize;

  Patch[2].Face = Face;
  Patch[2].P.I = P.I + 2*NewSize;
  Patch[2].P.J = P.J + 0*NewSize;
  Patch[2].Size = NewSize;

  Patch[3].Face = Face;
  Patch[3].P.I = P.I + 0*NewSize;
  Patch[3].P.J = P.J + 1*NewSize;
  Patch[3].Size = NewSize;

  Patch[4].Face = Face;
  Patch[4].P.I = P.I + 1*NewSize;
  Patch[4].P.J = P.J + 1*NewSize;
  Patch[4].Size = NewSize;

  Patch[5].Face = Face;
  Patch[5].P.I = P.I + 2*NewSize;
  Patch[5].P.J = P.J + 1*NewSize;
  Patch[5].Size = NewSize;

  Patch[6].Face = Face;
  Patch[6].P.I = P.I + 0*NewSize;
  Patch[6].P.J = P.J + 2*NewSize;
  Patch[6].Size = NewSize;

  Patch[7].Face = Face;
  Patch[7].P.I = P.I + 1*NewSize;
  Patch[7].P.J = P.J + 2*NewSize;
  Patch[7].Size = NewSize;

  Patch[8].Face = Face;
  Patch[8].P.I = P.I + 2*NewSize;
  Patch[8].P.J = P.J + 2*NewSize;
  Patch[8].Size = NewSize;

  return NewSize;
}

CLMP *CPatch::GetLMP() {
  int HalfSize = Size/2;
  return &Face->LMP[(HalfSize + P.J) * Face->Width + (HalfSize + P.I)];
}

int CFace::GetLinePoint(C2DPoint A, C2DPoint B, C2DPoint &Point, C2DPoint RightPoint, float Distance) {
  C2DLine Line(A, B, RightPoint);
  Line = Line.Revert();
  // Test proper stripe
  float D = Line.Distance(Point);
  if (D < 0) return GLP_NOCHANGE;
  if (D > Distance) return GLP_OUTSIDE;
  // Test A bound
  C2DLine ALine(A, A.Add(Line.NVector), B);
  if (ALine.InRight(Point)) {
    // Test B bound
    C2DLine BLine(B, B.Add(Line.NVector), A);
    if (ALine.InRight(Point)) {
      C2DLine PLine(Point, Point.Add(Line.NVector));
      Point = Line.Intersection(PLine);
      return GLP_ALIGNED;
    }
  }
  // Test A quarter-circle
  if (A.Distance(Point) < Distance) {
    Point = A;
    return GLP_ALIGNED;
  }
  // Test B quarter-circle
  if (B.Distance(Point) < Distance) {
    Point = B;
    return GLP_ALIGNED;
  }
  // If we reached this point, the point lies outside
  return GLP_OUTSIDE;
}

int CFace::GetSurfacePoint(C2DPoint &Point, float Distance) {
  C2DPoint Focus = MapSurfacePointTo2D(GetFocus());
  // Get first line (between last point and first point)
  int Result;
  Result = GetLinePoint(MapSurfacePointTo2D(Vertex[VertexCount-1]), MapSurfacePointTo2D(Vertex[0]), Point, Focus, Distance);
  if (Result == GLP_OUTSIDE) return 0;
  if (Result == GLP_ALIGNED) return 1;
  // Test rest of the lines
  for (int i = 1; i < VertexCount; i++) {
    Result = GetLinePoint(MapSurfacePointTo2D(Vertex[i-1]), MapSurfacePointTo2D(Vertex[i]), Point, Focus, Distance);
    if (Result == GLP_OUTSIDE) return 0;
    if (Result == GLP_ALIGNED) return 1;
  }
  // If we reached this point, Point remains definitely unchanged
  return 1;
}

C2DPoint CFace::MapSurfacePointTo2D(C3DPoint X) {
  C2DPoint c;
  int MP = Plane.NVector.GetMP();
  C2DPoint p = P.Get2DPoint(MP);
  C2DPoint u = U.Get2DPoint(MP);
  C2DPoint v = V.Get2DPoint(MP);
  C2DPoint x = X.Get2DPoint(MP);
  // Statement
  c.I = (v.J*(x.I - p.I) + v.I*(p.J - x.J))/(v.J*u.I - v.I*u.J);
  if (v.I != 0)
    c.J = (x.I - p.I - c.I*u.I)/v.I;
  else
    c.J = (x.J - p.J) / v.J;
  // Returning a value
  return c;
}

C3DPoint CFace::Map2DToSurfacePoint(C2DPoint X) {
  return P.Move(U.Multiply(X.I * LMP_DISTANCE)).Move(V.Multiply(X.J * LMP_DISTANCE));
}

void CFace::Init(C3DPoint *Vertex, int VertexCount, CPlane Plane, CFRGB E, float ro) {
  int i, j;
  _ASSERT(VertexCount != 0);
  // Call init of ancestor
  CPolygon::Init(Vertex, VertexCount, Plane);
  // U vector
  U = Vertex[0].GetVector(Vertex[1]);
  U.Normalize();
  // V vector, is size 1 (in case U and Plane.NVector are both size 1)
  V = U.GetVectorProduct(Plane.NVector);
  // Check the width, height and mapping point
  float MinWD, MaxWD, MinHD, MaxHD; // Min Width Distance, ...
  MinWD = MaxWD = MinHD = MaxHD = 0;
  CPlane PlaneU(U, Vertex[0]);
  CPlane PlaneV(V, Vertex[0]);
  float Distance;
  for (i = 1; i < VertexCount; i++) {
    // U (Width)
    Distance = PlaneU.Distance(Vertex[i]);
    if (Distance < MinWD) MinWD = Distance;
    if (Distance > MaxWD) MaxWD = Distance;
    // V (Height)
    Distance = PlaneV.Distance(Vertex[i]);
    if (Distance < MinHD) MinHD = Distance;
    if (Distance > MaxHD) MaxHD = Distance;
  }
  P = Vertex[0].Move(U.Multiply(MinWD));
  P = P.Move(V.Multiply(MinHD));
  Width = (int)(MaxWD - MinWD + LMP_DISTANCE - 1)/LMP_DISTANCE + 1;
  Height = (int)(MaxHD - MinHD + LMP_DISTANCE - 1)/LMP_DISTANCE + 1;
  Width = MAX(2, Width);
  Height = MAX(2, Height);
  // Init LightMap Points
  LMP = (CLMP*) malloc(Width * Height * sizeof(CLMP));
  C2DPoint SP;
  for (j = 0; j < Height; j++) {
    for (i = 0; i < Width; i++) {
      SP.I = (float)i * LMP_DISTANCE;
      SP.J = (float)j * LMP_DISTANCE;
      if (GetSurfacePoint(SP, FACEAREA)) {
        LMP[j * Width + i].IsVoid = 0;
        LMP[j * Width + i].SurfacePoint = SP;
        LMP[j * Width + i].Sector = (CSector*)NULL2;
      }
      else {
        LMP[j * Width + i].IsVoid = 1;
        LMP[j * Width + i].Sector = NULL;
      }
    }
  }
  // Init TextureArea
  TextureArea = NULL;
  // E & ro
  this->E = E;
  this->ro = ro;
  // Visited
  Visited = 0;
}

void CFace::Done() {
  free(LMP);
}

class CCustomVertex {
public:
  // 3D space coordinate of point
  C3DPoint P;
  // 2D texture coordinate
  C2DPoint T;
};

void CFace::Draw(LPDIRECT3DDEVICE8 pd3dDevice, IDirect3DTexture8 *pTexture) {
  int i;
  CCustomVertex CV[MAX_VERTICES];
  // Fill all vertices with value
  for (i = 0; i < VertexCount; i++) {
    CV[i].P = Vertex[i];
    CV[i].T = g_TextureMan.GetTexturePosition(TextureArea, MapSurfacePointTo2D(Vertex[i]).Multiple(1.0/(LMP_DISTANCE)).Add(C2DPoint(0.5f, 0.5f)));

    //CV[i].T.I += 0.5/(GLOBAL_TEXTURE_SIZE);
    //CV[i].T.J += 0.5/(GLOBAL_TEXTURE_SIZE);

  }
  // Draw face
  //pd3dDevice->SetVertexShader(D3DFVF_XYZ | D3DFVF_TEX1);
  //pd3dDevice->SetTexture(0, g_TextureMan.GetTexture());
  pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN, VertexCount - 2, CV, sizeof(CCustomVertex));
}

int CFace::GetBasicPatchesPart(CI2DPoint TL, CI2DPoint BR, CPatch *BasicPatch) {
/*
  CPatch Patch;
  for (int i = 0; i <= BR.I; i++) {
    for (int j = 0; j <= BR.J; j++) {
      Patch.Face = this;
      Patch.P.I = i;
      Patch.P.J = j;
      Patch.Size = 1;
      pRQP->Put(Patch, RQ_LEVEL_COUNT - 1);
    }
  }
*/

  int BasicPatchCount = 0;
  int PatchWidth = BR.I - TL.I + 1;
  int PatchHeight = BR.J - TL.J + 1;
  int PatchSize = PATCH_SIZE(MIN(PatchWidth, PatchHeight));
  int WPatchesNum = PatchWidth/PatchSize;
  int HPatchesNum = PatchHeight/PatchSize;
  for (int i = 0; i < WPatchesNum; i++) {
    for (int j = 0; j < HPatchesNum; j++) {
      BasicPatch[BasicPatchCount].Face = this;
      BasicPatch[BasicPatchCount].P.I = TL.I + i * PatchSize;
      BasicPatch[BasicPatchCount].P.J = TL.J + j * PatchSize;
      BasicPatch[BasicPatchCount].Size = PatchSize;
      BasicPatchCount++;
    }
  }
  int DoneWidth = WPatchesNum * PatchSize;
  int DoneHeight = HPatchesNum * PatchSize;
  CI2DPoint TLNew, BRNew;
  // Portrait
  if (PatchWidth < PatchHeight) {
    // Right side
    if (DoneWidth < PatchWidth) {
      TLNew.I = TL.I + DoneWidth;
      TLNew.J = TL.J;
      BRNew.I = BR.I;
      BRNew.J = TL.J + DoneHeight - 1;
      BasicPatchCount += GetBasicPatchesPart(TLNew, BRNew, &BasicPatch[BasicPatchCount]);
    }
    // Bottom side
    if (DoneHeight < PatchHeight) {
      TLNew.I = TL.I;
      TLNew.J = TL.J + DoneHeight;
      BRNew.I = BR.I;
      BRNew.J = BR.J;
      BasicPatchCount += GetBasicPatchesPart(TLNew, BRNew, &BasicPatch[BasicPatchCount]);
    }
  }
  // Landscape
  else {
    // Right side
    if (DoneWidth < PatchWidth) {
      TLNew.I = TL.I + DoneWidth;
      TLNew.J = TL.J;
      BRNew.I = BR.I;
      BRNew.J = BR.J;
      BasicPatchCount += GetBasicPatchesPart(TLNew, BRNew, &BasicPatch[BasicPatchCount]);
    }
    // Bottom side
    if (DoneHeight < PatchHeight) {
      TLNew.I = TL.I;
      TLNew.J = TL.J + DoneHeight;
      BRNew.I = TL.I + DoneWidth - 1;
      BRNew.J = BR.J;
      BasicPatchCount += GetBasicPatchesPart(TLNew, BRNew, &BasicPatch[BasicPatchCount]);
    }
  }
  return BasicPatchCount;
}

int CFace::GetBasicPatches(CPatch *BasicPatch) {
  CI2DPoint TL, BR;
  TL.I = 0;
  TL.J = 0;
  BR.I = Width - 1;
  BR.J = Height - 1;
  return GetBasicPatchesPart(TL, BR, BasicPatch);
}

C3DPoint CFace::SurfacePoint(float a, float b) {
  return P.Move(U.Multiply(LMP_DISTANCE * Width * a)).Move(V.Multiply(LMP_DISTANCE * Height * b));
}

void CFace::FetchTextureArea() {
  if (TextureArea == NULL) {
    g_TextureMan.GetArea(TextureArea, MAX(Width, Height));
  }
}
