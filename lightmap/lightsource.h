#ifndef _lightsource_h_
#define _lightsource_h_

#include "3dpoint.h"
#include "rgb.h"

#define LSATTR_PRIMARY 1

class CLightSource {
public:
  C3DPoint Position;
  CFRGB Intensity;
  CFace *Face;
  CSector *Sector;
  int Attr;
};

#endif