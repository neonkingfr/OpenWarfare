#ifndef _auxiliary_h_
#define _auxiliary_h_

// Macros
#define ABS(V) (((V)<0)?-(V):(V))
#define MIN(A,B) (((A)<(B))?(A):(B))
#define MAX(A,B) (((A)<(B))?(B):(A))
#define MAKE_R5G6B5(W,R,G,B) {W=(WORD)(((R)>>3)<<11); W|=(WORD)(((G)>>2)<<5); W|=(WORD)((B)>>3);}
#define PATCH_SIZE(N) ((N)<3)?1:(((N)<9)?3:9)

// Constants
#define DEFLECTION 0.01f // Deflection which can appear during geom. math oper.
#define PI 3.141592654f
#define INLINE __forceinline
#define GLOBAL_TEXTURE_SIZE  256// Both width and height of the Global Texture
#define FLT_MAX 3.402823466e+38F
#define NULL2 ((void*)0xFFFFFFFF)

// 2D point with integer values
class CI2DPoint {
public:
  int I, J;
};

#endif