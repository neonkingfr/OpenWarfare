#ifndef _rgb_h_
#define _rgb_h_

#include "auxiliary.h"

// Flyman's RGB class
class CFRGB {
public:
  // RGB units of light
  float R, G, B;
  // Constructor
  INLINE CFRGB() {};
  // Constructor
  INLINE CFRGB(float R, float G, float B) {
    this->R = R; this->G = G; this->B = B;
  };
  // Add a C color to this color. Returns new color, this color remains unchanged.
  INLINE CFRGB Add(CFRGB C) {
    CFRGB Result(R + C.R, G + C.G, B + C.B);
    return Result;
  };
  // Sub a C color from this color. Returns new color, this color remains unchanged.
  INLINE CFRGB Sub(CFRGB C) {
    CFRGB Result(R - C.R, G - C.G, B - C.B);
    return Result;
  };
  // Multiply this color by N number. Returns new color, this color remains unchanged.
  INLINE CFRGB Multiply(float N) {
    CFRGB Result(R * N, G * N, B * N);
    return Result;
  };
  // Returns negative energy of this energy
  INLINE CFRGB Revert() {
    CFRGB Result(-R, -G, -B);
    return Result;
  };
  // Returns the intensity of this RGB
  INLINE float GetIntensity() {
    float Result = MAX(ABS(R), MAX(ABS(G), ABS(B)));
    return Result;
  };
  // Redefined operators
  INLINE CFRGB operator*(const float &Value) {
    return CFRGB(R*Value, G*Value, B*Value);
  };
  INLINE CFRGB operator*(const CFRGB &Value) {
    return CFRGB(R*Value.R, G*Value.G, B*Value.B);
  };
  INLINE void operator*=(const float &Value) {
    R *= Value;
    G *= Value;
    B *= Value;
  };
  INLINE void operator*=(const CFRGB &Value) {
    R *= Value.R;
    G *= Value.G;
    B *= Value.B;
  };
  INLINE CFRGB operator/(const float &Value) {
    return CFRGB(R/Value, G/Value, B/Value);
  };
  INLINE void operator/=(const float &Value) {
    R /= Value;
    G /= Value;
    B /= Value;
  };
};

#endif