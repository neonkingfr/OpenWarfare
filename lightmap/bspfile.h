#ifndef _bspfile_h_
#define _bspfile_h_

// Reading *.bsp file generated with qbsp utility version 29

// Defines
#define FILE_NAME_LENGTH       256
#define RECORD_LENGTH          256
#define MAX_EDGES              1024
#define MAX_PORTALS_IN_SECTOR  128
#define AVG_FACE_INCREASE_KOEF 2

// Basic data types
typedef float          scalar_t; // Scalar value,
typedef unsigned char  u_char;
typedef unsigned short u_short;
typedef unsigned long  u_long;

// ----------------------------------------------------------------------------------------------
// Basic BSP file structures
typedef struct                 // Vector or Position
{ scalar_t x;                  // horizontal
  scalar_t y;                  // horizontal
  scalar_t z;                  // vertical
} vec3_t;

typedef struct                 // Bounding Box, Float values
{ vec3_t   min;                // minimum values of X,Y,Z
  vec3_t   max;                // maximum values of X,Y,Z
} boundbox_t;

typedef struct                 // Bounding Box, Short values
{ short   min;                 // minimum values of X,Y,Z
  short   max;                 // maximum values of X,Y,Z
} bboxshort_t;

typedef struct                 // A Directory entry
{ long  offset;                // Offset to entry, in bytes, from start of file
  long  size;                  // Size of entry in file, in bytes
} dentry_t;

// BSP file structures
typedef struct                 // The BSP file header
{ long  version;               // Model version, must be 0x17 (23).
  dentry_t entities;           // List of Entities.
  dentry_t planes;             // Map Planes.
                               // numplanes = size/sizeof(plane_t)
  dentry_t miptex;             // Wall Textures.
  dentry_t vertices;           // Map Vertices.
                               // numvertices = size/sizeof(vertex_t)
  dentry_t visilist;           // Leaves Visibility lists.
  dentry_t nodes;              // BSP Nodes.
                               // numnodes = size/sizeof(node_t)
  dentry_t texinfo;            // Texture Info for faces.
                               // numtexinfo = size/sizeof(texinfo_t)
  dentry_t faces;              // Faces of each surface.
                               // numfaces = size/sizeof(face_t)
  dentry_t lightmaps;          // Wall Light Maps.
  dentry_t clipnodes;          // clip nodes, for Models.
                               // numclips = size/sizeof(clipnode_t)
  dentry_t leaves;             // BSP Leaves.
                               // numlaves = size/sizeof(leaf_t)
  dentry_t lface;              // List of Faces.
  dentry_t edges;              // Edges of faces.
                               // numedges = Size/sizeof(edge_t)
  dentry_t ledges;             // List of Edges.
  dentry_t models;             // List of Models.
                               // nummodels = Size/sizeof(model_t)
} dheader_t;

typedef struct
{ vec3_t normal;               // Vector orthogonal to plane (Nx,Ny,Nz)
                               // with Nx2+Ny2+Nz2 = 1
  scalar_t dist;               // Offset to plane, along the normal vector.
                               // Distance from (0,0,0) to the plane
  long    type;                // Type of plane, depending on normal vector.
} plane_t;

typedef struct{ 
  float X;                     // X,Y,Z coordinates of the vertex
  float Y;                     // usually some integer value
  float Z;                     // but coded in floating point
} vertex_t;

typedef struct
{ long    plane_id;            // The plane that splits the node
                               //           must be in [0,numplanes[
  u_short front;               // If bit15==0, index of Front child node
                               // If bit15==1, ~front = index of child leaf
  u_short back;                // If bit15==0, id of Back child node
                               // If bit15==1, ~back =  id of child leaf
  short   newfeature[6];       // Fill memory for new features
  u_short face_id;             // Index of first Polygons in the node
  u_short face_num;            // Number of faces in the node
} node_t;

typedef struct  
{ u_short plane_id;            // The plane in which the face lies
                               //           must be in [0,numplanes[ 
  u_short side;                // 0 if in front of the plane, 1 if behind the plane
  long ledge_id;               // first edge in the List of edges
                               //           must be in [0,numledges[
  u_short ledge_num;           // number of edges in the List of edges
  u_short texinfo_id;          // index of the Texture info the face is part of
                               //           must be in [0,numtexinfos[ 
  u_char typelight;            // type of lighting, for the face
  u_char baselight;            // from 0xFF (dark) to 0 (bright)
  u_char light[2];             // two additional light models  
  long lightmap;               // Pointer inside the general light map, or -1
                               // this define the start of the face light map
} face_t;

// leaf 0 is the generic CONTENTS_SOLID leaf, used for all solid areas
// all other leafs need visibility info
typedef struct{ 
  long type;                   // Special type of leaf
  long vislist;                // Beginning of visibility lists
                               //     must be -1 or in [0,numvislist[
  short   newfeature[6];       // Fill memory for new features
  u_short lface_id;            // First item of the list of faces
                               //     must be in [0,numlfaces[
  u_short lface_num;           // Number of faces in the leaf  
  u_char sndwater;             // level of the four ambient sounds:
  u_char sndsky;               //   0    is no sound
  u_char sndslime;             //   0xFF is maximum volume
  u_char sndlava;              //
} dleaf_t;

typedef struct
{ u_short vertex0;             // index of the start vertex
                               //  must be in [0,numvertices[
  u_short vertex1;             // index of the end vertex
                               //  must be in [0,numvertices[
} edge_t;

typedef struct
{ boundbox_t bound;            // The bounding box of the Model
  vec3_t origin;               // origin of model, usually (0,0,0)
  long node_id0;               // index of first BSP node
  long node_id1;               // index of the first Clip node
  long node_id2;               // index of the second Clip node
  long node_id3;               // usually zero
  long numleafs;               // number of BSP leaves
  long face_id;                // index of Faces
  long face_num;               // number of Faces
} model_t;

// ----------------------------------------------------------------------------------------------
// My own structures
class CScene;
// main class, which stores the bsp scene
class CBspFile {
 private:
  plane_t  *PlaneArray;
  //int PlaneArrayNum;
  C3DPoint *VertexArray;
  //int VertexArrayNum;
  node_t   *BspArray;
  face_t   *FaceArray;
  u_char   *VisArray;
  dleaf_t  *SectorArray;
  u_short  *LFaceArray;   // each u_short is the index of a Face
  edge_t   *EdgeArray;
  /*short*/int    *LEdgeArray;
  model_t  *ModelArray;
  // Loads portals from *.prt file
  void LoadPortals(char *FileName, CScene *Scene);
  // Separates faces along sector they lies in
  void SeparateFaces(CScene *Scene);
 public:
  CBspFile(char *FileName, CScene *Scene);
};

#endif