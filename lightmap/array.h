#ifndef _array_h_
#define _array_h_

#include <stdio.h>
#include <malloc.h>
#include <crtdbg.h>

template <class T> class CArray {
 public:
  // Data
  T *Items;
  // Number of items in the array
  int ItemsCount;
  // Constructor
  CArray() {
    Items = NULL;
    ItemsCount = 0;
  };
  // Destructor
  ~CArray() {
  }
  // Initializion
  void Init(int ItemsAllocated) {
    Items = (T*) malloc(sizeof(T)*ItemsAllocated);
    _ASSERT(Items != NULL);
    ItemsCount = 0;
  };
  // Destroying - can be called even without previous Init call
  void Done() {
    if (Items != NULL) free(Items);
  }
};

#endif