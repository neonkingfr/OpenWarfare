#ifndef _polygon_h_
#define _polygon_h_

#include "auxiliary.h"
#include "3dpoint.h"
#include "plane.h"
#include "2dline.h"

#define MAX_VERTICES 32

#define SP_FRONT 1
#define SP_BACK  2

class CPolygon {
private:
  void RemoveInvalidVertices();
public:
  // Vertices of this polygon and their count
  // Vertices are clockwise oriented (in case normal vector of Plane points to the eye)
  C3DPoint Vertex[MAX_VERTICES];
  int VertexCount;
  // Plane in which this polygon lies
  CPlane Plane;
  // Detects wheter Point lies inside this polygon
  // Point must lies on the plane of this polygon, otherwise it could produce
  // wrong result
  INLINE int IsInside(C3DPoint &Point) {
    // Get Mapping Plane
    int MP = Plane.NVector.GetMP();
    // Get 2DPoint according to MP
    C2DPoint Point2D = Point.Get2DPoint(MP);
    // Get first line (between last point and first point)
    C2DLine Line(Vertex[VertexCount-1].Get2DPoint(MP), Vertex[0].Get2DPoint(MP));
    // Get the position of Point to this Line
    int InRight = Line.InRight(Point2D);
    // All rest lines must have the same orientation
    for (int i = 1; i < VertexCount; i++) {
      // Get Line
      Line.Init(Vertex[i-1].Get2DPoint(MP), Vertex[i].Get2DPoint(MP));
      // Test with InRight
      if (Line.InRight(Point2D) != InRight) return 0;
    }
    return 1;
  };
  // Initialization of plane
  void Init(C3DPoint *Vertex, int VertexCount, CPlane Plane);
  // Returns 1 in case half line Line hits this polygon, 0 otherwise
  int Hit(C3DLine Line, C3DPoint &Intersection) {
    if (!Plane.Hit(Line, Intersection)) return 0;
    return IsInside(Intersection);
  };
  // Splites this polygon according to Plane and returns new polygons
  // which lies in front of the plane (FPolygon) and vice versa
  // Returns combination of SP_FRONT and SP_BACK
  int Split(CPlane Plane, CPolygon &FPolygon, CPolygon &BPolygon);
  // Returns a focus of this polygon
  C3DPoint GetFocus();
};

#endif