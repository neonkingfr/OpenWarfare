#include "sector.h"

void CSector::Init(CFace **LFace, int LFaceCount, CPortal *Portal, int PortalCount, CSector **VisList, int VisListCount) {
  this->LFace = LFace;
  this->LFaceCount = LFaceCount;
  this->Portal = Portal;
  this->PortalCount = PortalCount;
  this->VisList = VisList;
  this->VisListCount = VisListCount;
  Visited = 0;
  FFPHead.Next = &FFPHead;
  FFPHead.Prev = &FFPHead;
}

C3DPoint CSector::GetFocus() {
  C3DPoint Result(0, 0, 0);
  int PointCount = 0;
  for (int i = 0; i < LFaceCount; i++) {
    for (int j = 0; j < LFace[i]->VertexCount; j++) {
      Result = Result.Move(LFace[i]->Vertex[j]);
      PointCount++;
    }
  }
  return Result.Multiply((float)1.0/PointCount);
}
