#ifndef _redixqueue_h_
#define _redixqueue_h_

#include "auxiliary.h"
#include "assert.h"

#define RQ_LEVEL_COUNT  16      // 15 - the most important, 0 - the less important
#define RQ_MAX_ITEMS    20000

template <class T> class CRedixQueue {
private:
  T Items[RQ_LEVEL_COUNT][RQ_MAX_ITEMS];
  // First item in specified level.
  int First[RQ_LEVEL_COUNT];
  // Last item in specified level, -1 means there are no any items at that level.
  int Last[RQ_LEVEL_COUNT];
  // Number of level with the most important item, -1 menas that RQ is empty.
  int CurrLevel;
public:
  // Default constructor.
  INLINE CRedixQueue() {
    Clear();
  }
  // Clear the Queue.
  INLINE void Clear() {
    for (int i = 0; i < RQ_LEVEL_COUNT; i++) {
      First[i] = 0;
      Last[i] = -1;
    }
    CurrLevel = -1;
  }
  // Put an item at specified level.
  INLINE void Put(T Item, int Level) {
    // Check if there is enough space.
    if (Last[Level] >= (RQ_MAX_ITEMS - 1)) {
      assert(0);
      return;
    }
    // Update CurrLevel.
    CurrLevel = MAX(CurrLevel, Level);
    // Add the item eventually.
    Last[Level]++;
    Items[Level][Last[Level]] = Item;
  }
  // Get the most important item. Returns 1 if there is any to return.
  INLINE int Get(T &Item) {
    // If there is not any ...
    if (CurrLevel < 0) return 0;
    // Get the item.
    Item = Items[CurrLevel][First[CurrLevel]];
    First[CurrLevel]++;
    // Update CurrLevel.
    while ((CurrLevel >= 0) && (First[CurrLevel] > Last[CurrLevel])) CurrLevel--;
    // Returning.
    return 1;
  }
};

#endif