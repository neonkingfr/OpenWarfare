#ifndef _mclights_h_
#define _mclights_h_

#include "halton.h"


#define AVG_RO 0.5f // Average reflectance in the scene

/*!
  This class gets a list of light sources as an input and distributes energy in
  the scene. Returns list of distributed point light sources according to
  quasi-random walk described in Keller's "Instant Radiosity" document.
*/
class CMCLights {
public:
  /*!
    \param SM Pointer to Scene Manager.
    \param Face Face to start with.
    \param N number of lights on the surface of the Face.
    \param LS Pointer to Light sources array which method will fill out.
    \return Number of lights in LS array.
  */
  int GetMCLighSources(CSceneManager *SM, CFace *Face, int N, CLightSource *LS) {
    float w;                // Compensation factor
    float Start;
    int End;
    int Reflections = 0;    // Number of reflections for specified walk
    CFRGB L;                // Radiance
    C3DPoint y;             // Starting point of the ray
    C3DPoint omega;         // Direction of the ray
    CHalton HP2(2), HP3(3); // Halton's generators for primary points
    CHalton HR2(2), HR3(3); // Halton's generators for reflections directions
    int LSCount = 0;        // Number of light sources

    // Algorithm
    Start = End = N;
    while (End > 0) {
      Start *= AVG_RO;
      for (int i = (int) Start; i < End; i++) {
        // Select starting point on light source
        y = Face->SurfacePoint(HP2, HP3);
        L = Face->B / N;
        w = N;
        // Face for reflections
        CFace *RFace = Face;
        // Trace reflections
        for (int j = 0; j <= Reflections; j++) {
          // Save the light with (L/w) intensite
          (j == 0) ? LS[LSCount].Attr = LSATTR_PRIMARY : LS[LSCount].Attr = 0;
          LS[LSCount].Position = y;
          LS[LSCount].Intensity = L/w;
          LS[LSCount].Face = RFace;
          LSCount++;
          // Difuse scattering
          omega = RFace->Normal.DiffuseRay(HR2, HR3);
          // Trace ray from y into direction omega
          if (!SM->TraceRay(y, omega, RFace, y, RFace)) break;
          // Attenuate and compensate
          L *= RFace->BRDFColor;
          w *= AVG_RO;
        }
      }
      Reflections++;
      End = (int) Start;
    }
    return LSCount;
  }
};

#endif