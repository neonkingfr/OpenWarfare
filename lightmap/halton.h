#ifndef _halton_h_
#define _halton_h_

#include "assert.h"

/*!
  This class provides pseudorandom points generator according to Halton's formula.
  Further we will refer it as a Halton Sequence or HS. During initialization you
  must specify the base of the sequence. The numbers will be in range (0, 1).
  More information about HS you can find in the following document:
  A. Keller. The fast Calculation of Form Factors using Low Discrepancy Sequences. In Proc. Spring Conference
  on Computer Graphics (SCCG �96), pages 195-204, Bratislava, Slovakia, 1996. Comenius University Press.
*/
class CHalton {
private:
  //! Pseudorandom value itself.
  float value;
  //! Inverse base specified during initialization.
  float inv_base;
  //! This method generates next value of HS according to inv_base.
  inline void Next() {
    float r = 1.0f - value - 0.0000000001f;
    if (inv_base < r)
      value += inv_base;
    else {
      float h = inv_base, hh;
      do {
        hh = h;
        h *= inv_base;
      } while (h >= r);
      value += hh + h - 1.0f;
    }
  }
public:
  //! Default constructor. Note that without initialization is this class useless.
  /*!
    \sa Init
  */
  inline CHalton() {
  }
  //! Constructor. Class will be initialized with base parameter.
  /*!
    \param base Base of the HS.
    \sa Init
  */
  inline CHalton(int base) {
    Init(base);
  }
  //! Initialization of HS. You must specify the base of HS (for example 2).
  /*!
    \param base Base of the HS.
    The base must be equal or greater than 2.
  */
  inline void Init(int base) {
    assert(base >= 2);
    value = inv_base = 1.0f/base;
  }
  //! Using this operator you can directly receive Halton values.
  /*!
    Consider example: CHalton Halton(2); float a = Halton; float b = Halton;.
    In variable a will be the first point of HS whereas in b will be the second one.
    \return Next number from HS.
  */
  inline operator float() {
    float Result = value;
    Next();
    return Result;
  }
};

/*!
  This class provides pseudorandom 2D points generator using Halton generator.
  The points will be in square (0, 0), (1, 1).
  \sa CHalton
*/
class CHalton2D {
private:
  //! Halton sequence for the one dimension.
  CHalton H1;
  //! Halton sequence for the second dimension.
  CHalton H2;
public:
  //! Default constructor. Class will be initialized.
  /*!
    \sa Init
  */
  inline CHalton2D() {
    Init();
  }
  //! Initialization of both HS.
  /*!
    Note that it is important as the bases to chose arbitrary comprime numbers.
    Therefore we chose 2 and 3.
  */
  inline void Init() {
    H1.Init(2);
    H2.Init(3);
  }
  //! This method will fill parameters i and j with pseudorandom values.
  /*!
    \param i Returns I coordinate.
    \param j Returns J coordinate.
  */
  inline void Get(float &i, float &j) {
    i = H1;
    j = H2;
  }
};

#endif