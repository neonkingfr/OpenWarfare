#ifndef _bsp_h_
#define _bsp_h_

#include <string.h>
#include <class/array.hpp>
#include "plane.h"

#define BSP_TYPE_NODE 0
#define BSP_TYPE_LEAF 1
#define BSP_TYPE_NULL 2

class CSector;

class CBSP {
public:
  // Plane this node belongs to
  CPlane Plane;
  // Front node or sector
  int FrontType;
  union {
    CBSP *FrontNode;
	  CSector *FrontLeaf;
  };
  // Back node or sector
  short BackType;
  union {
    CBSP *BackNode;
	  CSector *BackLeaf;
  };
};

TypeIsSimple(CBSP)

#endif