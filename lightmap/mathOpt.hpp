#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MATHOPT_HPP
#define _MATHOPT_HPP

#define INV_EPS ( 1e-4 )
#define INVSQRT_EPS ( 1e-3 )

#include "mathDefs.hpp"
#include <math.h>

inline Coord HDegree( Coord angle )
{ // convert degree to radian
	return angle*coord(H_PI/180);
}

inline float fSign( float x )
{
	if( x>0 ) return +1.0;
	if( x<0 ) return -1.0;
	return 0;
}

inline int sign( float x )
{
	if( x>0 ) return +1;
	if( x<0 ) return -1;
	return 0;
}

#ifndef _KNI

__forceinline float Inv( float c ) {return 1/c;}

float InvSqrt( float c );

inline float Square( float x ) {return x*x;}

#else

// consider: NewtonRaphson Reciprocal 
//   [2 * rcpps(x) - (x * rcpps(x) * rcpps(x))]
__forceinline float Inv( float c ) {return 1/c;}
/*
inline float Inv( float f )
{
	__m128 ff=_mm_set_ss(f);
	__m128 ra0 = _mm_rcp_ss(ff);
	__m128 res=_mm_sub_ss(_mm_add_ss(ra0, ra0), _mm_mul_ss(_mm_mul_ps(ra0, ff), ra0));
	float ret;
	_mm_store_ss(&ret,res);
	return ret;
}
*/


//	NewtonRaphson Reciprocal Square Root 
//  	0.5 * rsqrtps * (3 - x * rsqrtps(x) * rsqrtps(x))
float InvSqrt( float f );

inline float Square( float x ) {return x*x;}

#endif

#endif
