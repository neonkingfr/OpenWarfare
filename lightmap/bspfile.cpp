#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include "scene.h"
#include "bspfile.h"

// Portals auxiliary structures

struct CSectorPortal {
public:
  CPortal Portal[MAX_PORTALS_IN_SECTOR];
  int PortalNum;
  INLINE CSectorPortal() {
    PortalNum = 0;
  }
};

void CBspFile::LoadPortals(char *FileName, CScene *Scene) {
  // Open file
  char FN[FILE_NAME_LENGTH];
  FILE *f;
  strcpy(FN, FileName);
  f = fopen(strcat(FN, ".prt"), "rt");
  char Record[RECORD_LENGTH];
  // Reading the *.prt.file
  // Skip first record
  fscanf(f, "%s", Record);
  // Number of sectors
  fscanf(f, "%s", Record);
  int SectorsNum = atoi(Record);
  CSectorPortal *Sector = new CSectorPortal[SectorsNum];
  // Number of portals
  fscanf(f, "%s", Record);
  int PortalsNum = atoi(Record);
  if (PortalsNum == 0) return ;
  // Allocate space for portals
  Scene->APortal.Resize(PortalsNum * 2);
  // For all Portals
  int i, j;
  for (i = 0; i < PortalsNum; i++) {
    // Read the number of edges
    fscanf(f, "%s", Record);
    int EdgesNum = atoi(Record);
    // Read Start and End Sector
    fscanf(f, "%s", Record);
    int SectorStart = atoi(Record);
    fscanf(f, "%s", Record);
    int SectorEnd = atoi(Record);
    // Read all edges
    C3DPoint Edge[MAX_EDGES];
    for (j = 0; j < EdgesNum; j++) {
      // Read X - without "(" - that's because + 1
      fscanf(f, "%s", Record);
      Edge[j].X = (float)atof(Record + 1);
      // Read Y
      fscanf(f, "%s", Record);
      Edge[j].Y = (float)atof(Record);
      // Read X
      fscanf(f, "%s", Record);
      Edge[j].Z = (float)atof(Record);
      // Skip ")"
      fscanf(f, "%s", Record);
    }
    // Write 2 portals
    CPlane Plane;
    Plane.Init(Edge[0], Edge[2], Edge[1]);
    if (!Plane.InFront(Scene->ASector[SectorStart].GetFocus())) {
      Plane.Init(Edge[0], Edge[1], Edge[2]);
    }
    Sector[SectorStart].Portal[Sector[SectorStart].PortalNum].Init
      (Edge, EdgesNum, Plane, &Scene->ASector[SectorEnd]);
    Sector[SectorStart].PortalNum++;

    Plane = Plane.RevertNVector();

    Sector[SectorEnd].Portal[Sector[SectorEnd].PortalNum].Init
      (Edge, EdgesNum, Plane, &Scene->ASector[SectorStart]);
    Sector[SectorEnd].PortalNum++;
  }

  // Copy portals sector by sector
  int CurrPortal = 0;
  for (i = 0; i < SectorsNum; i++) {
    Scene->ASector[i].Portal = &Scene->APortal[CurrPortal];
    // For all portals in the sector
    for (j = 0; j < Sector[i].PortalNum; j++) {
      // Copy Portal
      Scene->APortal[CurrPortal] = Sector[i].Portal[j];
      Scene->ASector[i].PortalCount++;
      CurrPortal++;
    }
  }

  // Free helper array
  delete []Sector;
  // Close file
  fclose(f);
}

void CBspFile::SeparateFaces(CScene *Scene) {
/*
  CSector *Sector;
  int Result;
  CPolygon FPolygon, BPolygon;
  CFace TempFace;
  for (int i = 0; i < Scene->ASector.Size(); i++) {
    Sector = &Scene->ASector[i];
    for (int j = 0; j < Sector->FaceCount; j++) {
      int k;

      for (k = 0; k < Sector->FaceCount; k++) {
        // Vezmu j - ty face a orezu ho podle k - teho
        Result = Sector->Face[j].Split(Sector->Face[k].Plane, FPolygon, BPolygon);
        if (Result & SP_FRONT) {
          TempFace = Sector->Face[j];
          Sector->Face[j].Done();
          Sector->Face[j].Init(FPolygon.Vertex, FPolygon.VertexCount, FPolygon.Plane, TempFace.E, TempFace.ro);
        }
      }

      for (k = 0; k < Sector->PortalCount; k++) {
        // Vezmu j - ty face a orezu ho podle k - teho portalu
        Result = Sector->Face[j].Split(Sector->Portal[k].Plane, FPolygon, BPolygon);
        if (Result & SP_FRONT) {
          TempFace = Sector->Face[j];
          Sector->Face[j].Done();
          Sector->Face[j].Init(FPolygon.Vertex, FPolygon.VertexCount, FPolygon.Plane, TempFace.E, TempFace.ro);
        }
      }


    };
  };
*/
}

CBspFile::CBspFile(char *FileName, CScene *Scene) {
  char FN[FILE_NAME_LENGTH];
  FILE *f;
  // ---------------------------------------------------------------------------
  // BSP file reading
  strcpy(FN, FileName);
  f = fopen(strcat(FN, ".bsp"), "rb");
  assert(f != NULL);
  int Result;
  // Header
  dheader_t DHeader;
  Result = fread(&DHeader, sizeof(dheader_t), 1, f);
  assert(Result > 0);
  // Planes
  PlaneArray = (plane_t*) malloc(DHeader.planes.size);
  fseek(f, DHeader.planes.offset, SEEK_SET); 
  Result = fread(PlaneArray, DHeader.planes.size, 1, f);
  assert(Result > 0);
  // Vertices
  VertexArray = (C3DPoint*) malloc(DHeader.vertices.size);
  fseek(f, DHeader.vertices.offset, SEEK_SET); 
  Result = fread(VertexArray, DHeader.vertices.size, 1, f);
  assert(Result > 0);
  // Bsp Nodes
  BspArray = (node_t*) malloc(DHeader.nodes.size);
  fseek(f, DHeader.nodes.offset, SEEK_SET); 
  Result = fread(BspArray, DHeader.nodes.size, 1, f);
  assert(Result > 0);
  // Faces
  FaceArray = (face_t*) malloc(DHeader.faces.size);
  fseek(f, DHeader.faces.offset, SEEK_SET); 
  Result = fread(FaceArray, DHeader.faces.size, 1, f);
  assert(Result > 0);
  // VisLists
  VisArray = (u_char*) malloc(DHeader.visilist.size);
  fseek(f, DHeader.visilist.offset, SEEK_SET); 
  Result = fread(VisArray, DHeader.visilist.size, 1, f);
  assert(Result > 0);
  // Sectors - Note that the first sector in SectorArray is allways empty
  SectorArray = (dleaf_t*) malloc(DHeader.leaves.size);
  fseek(f, DHeader.leaves.offset, SEEK_SET); 
  Result = fread(SectorArray, DHeader.leaves.size, 1, f);
  assert(Result > 0);
  // List of Faces
  LFaceArray = (u_short*) malloc(DHeader.lface.size);
  fseek(f, DHeader.lface.offset, SEEK_SET); 
  Result = fread(LFaceArray, DHeader.lface.size, 1, f);
  assert(Result > 0);
  // Edges
  EdgeArray = (edge_t*) malloc(DHeader.edges.size);
  fseek(f, DHeader.edges.offset, SEEK_SET); 
  Result = fread(EdgeArray, DHeader.edges.size, 1, f);
  assert(Result > 0);
  // List of Edges
  LEdgeArray = (int*) malloc(DHeader.ledges.size);
  fseek(f, DHeader.ledges.offset, SEEK_SET); 
  Result = fread(LEdgeArray, DHeader.ledges.size, 1, f);
  assert(Result > 0);
  // Models
  ModelArray = (model_t*) malloc(DHeader.models.size);
  fseek(f, DHeader.models.offset, SEEK_SET); 
  Result = fread(ModelArray, DHeader.models.size, 1, f);
  assert(Result > 0);
  // ---------------------------------------------------------------------------
  // Allocating Scene fields
  // FaceArray
  Scene->AFace.Resize(DHeader.faces.size/sizeof(face_t));
  // LFaceArray
  Scene->ALFace.Resize(DHeader.lface.size/sizeof(u_short));
  // SectorArray
  Scene->ASector.Resize(DHeader.leaves.size/sizeof(dleaf_t) - 1);
  // VisArray (upper bound of size of VisArray is LeafArrayNum*LeafArrayNum)
  Scene->AVisList.Resize((DHeader.leaves.size/sizeof(dleaf_t) - 1) * (DHeader.leaves.size/sizeof(dleaf_t) - 1));
  // BspArray
  Scene->ABSP.Resize(DHeader.nodes.size/sizeof(node_t));
  // ---------------------------------------------------------------------------
  // Loading Scene fields
  int i, j;
  
  // Faces
  for (i = 0; i < DHeader.faces.size/(int)sizeof(face_t); i++) {
    C3DPoint Vertex[MAX_VERTICES];
    C3DPoint Point;
    CPlane Plane;
    CFRGB E;
    for (j = 0; j < FaceArray[i].ledge_num; j++) {
      if (LEdgeArray[FaceArray[i].ledge_id + j] > 0)
        Vertex[j] = (C3DPoint) VertexArray[EdgeArray[LEdgeArray[FaceArray[i].ledge_id + j]].vertex0];
      else
        Vertex[j] = (C3DPoint) VertexArray[EdgeArray[-LEdgeArray[FaceArray[i].ledge_id + j]].vertex1];
    }
    Point.X = PlaneArray[FaceArray[i].plane_id].normal.x;
    Point.Y = PlaneArray[FaceArray[i].plane_id].normal.y;
    Point.Z = PlaneArray[FaceArray[i].plane_id].normal.z;
    if (FaceArray[i].side == 1) Point = Point.Revert();
    //Plane.Init(Point, PlaneArray[FaceArray[i].plane_id].dist);
    Plane.Init(Point, Vertex[0]);

    //E.R = (rand()*256)/RAND_MAX;
    //E.G = (rand()*256)/RAND_MAX;
    //E.B = (rand()*256)/RAND_MAX;


    E.R = 0; E.G = 0; E.B = 0;
/*
    if ((i%20) == 0) {
      E.R = ((float)rand()*2000)/RAND_MAX;
      E.G = ((float)rand()*2000)/RAND_MAX;
      E.B = ((float)rand()*2000)/RAND_MAX;

    }
    else {
      E.R = 0; E.G = 0; E.B = 0;
    }
*/

    Scene->AFace[i].Init(Vertex, FaceArray[i].ledge_num, Plane, E, 0.5);
  }

  // List of faces
  for (i = 0; i < DHeader.lface.size/(int)sizeof(u_short); i++) {
    Scene->ALFace[i] = &Scene->AFace[LFaceArray[i]];
  }

  // Sectors
  for (i = 1; i < DHeader.leaves.size/(int)sizeof(dleaf_t); i++) {
    Scene->ASector[i - 1].Init(&Scene->ALFace[SectorArray[i].lface_id], SectorArray[i].lface_num, NULL, 0, NULL, 0);
  }

  // BSP
  for (i = 0; i < DHeader.nodes.size/(int)sizeof(node_t); i++) {
    // Plane
    C3DPoint Point;
    Point.X = PlaneArray[BspArray[i].plane_id].normal.x;
    Point.Y = PlaneArray[BspArray[i].plane_id].normal.y;
    Point.Z = PlaneArray[BspArray[i].plane_id].normal.z;
    Scene->ABSP[i].Plane.Init(Point, VertexArray[EdgeArray[ABS(LEdgeArray[FaceArray[BspArray[i].face_id].ledge_id])].vertex0]);
    // Front
    if (BspArray[i].front == 65535) {
      Scene->ABSP[i].FrontType = BSP_TYPE_NULL;
    }
    else
    if (BspArray[i].front & 0x8000) {
      Scene->ABSP[i].FrontType = BSP_TYPE_LEAF;
      Scene->ABSP[i].FrontLeaf = &Scene->ASector[(u_char)~BspArray[i].front - 1];
    }
    else {
      Scene->ABSP[i].FrontType = BSP_TYPE_NODE;
      Scene->ABSP[i].FrontNode = &Scene->ABSP[BspArray[i].front];
    }
    // Back
    if (BspArray[i].back == 65535) {
      Scene->ABSP[i].BackType = BSP_TYPE_NULL;
    }
    else
    if (BspArray[i].back & 0x8000) {
      Scene->ABSP[i].BackType = BSP_TYPE_LEAF;
      Scene->ABSP[i].BackLeaf = &Scene->ASector[(u_char)~BspArray[i].back - 1];
    }
    else {
      Scene->ABSP[i].BackType = BSP_TYPE_NODE;
      Scene->ABSP[i].BackNode = &Scene->ABSP[BspArray[i].back];
    }
  }

  // VisList
  j = 0;
  for (i = 1; i < DHeader.leaves.size/(int)sizeof(dleaf_t); i++) {
    Scene->ASector[i - 1].VisList = &Scene->AVisList[j];
    int v = SectorArray[i].vislist;   //Leaf.vislist;
    for (int L = 1; L < DHeader.leaves.size/(int)sizeof(dleaf_t); v++) {
      if (VisArray[v] == 0) {         // value 0, leaves invisible
        L += 8 * VisArray[v + 1];     // skip some leaves
        v++;
      }
      else {                          // tag 8 leaves, if needed
        for (int bit = 1; bit <= 0x80; bit = bit * 2, L++) {
          if (VisArray[v] & bit) {
            // sector L is visible from sector i
            Scene->AVisList[j] = &Scene->ASector[L - 1];
            Scene->ASector[i - 1].VisListCount++;
            j++;
          }
        }
      }
    }
  }
/*
  CFRGB A(5000.0, 5000.0, 5000.0);
  Scene->ASector[0].LFace[2]->E = A;
  CFRGB B(0.0, 0.0, 5000.0);
  Scene->ASector[29].LFace[2]->E = B;
*/

  // Close file
  fclose(f);
  // Load portals from *.prt file
  LoadPortals(FileName, Scene);
  // Separate faces which were originaly shared among more sectors
  SeparateFaces(Scene);
}
