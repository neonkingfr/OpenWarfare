#ifndef _PLANE_H_
#define _PLANE_H_

#include "auxiliary.h"
#include "3dpoint.h"
#include "3dline.h"

// PointRelation result
#define PR_ON_PLANE 0
#define PR_IN_FRONT 1
#define PR_IN_BACK  2

class CPlane {
public:
  // Offset to plane, along the normal vector.
  // Distance from (0,0,0) to the plane
  float Dist;
  // Normal vector of this plane, size must be 1
  C3DPoint NVector;
  // Constructor
  INLINE CPlane() {};
  // Constructor
  INLINE CPlane(C3DPoint NVector, float Dist) {
    Init(NVector, Dist);
  };
  // Constructor
  INLINE CPlane(C3DPoint NVector, C3DPoint Point) {
    Init(NVector, Point);
  };
  // Initialization
  INLINE void Init(C3DPoint NVector, float Dist) {
    this->NVector = NVector;
    this->Dist = Dist;
  };
  // Initialization
  INLINE void Init(C3DPoint NVector, C3DPoint Point) {
    this->NVector = NVector;
    this->Dist = -NVector.X*Point.X - NVector.Y*Point.Y - NVector.Z*Point.Z;
  };
  // Initialization
  INLINE void Init(C3DPoint A, C3DPoint B, C3DPoint C) {
    C3DPoint U = A.GetVector(B);
    C3DPoint V = A.GetVector(C);
    C3DPoint N = U.GetVectorProduct(V);
    N.Normalize();
    Init(N, A);
  };
  // Gets the intersection of Line and this plane, returns 1 if success
  INLINE int GetIntersection(C3DLine Line, C3DPoint &Point) {
    // First get the divisor and if it's equal to 0 then return
    float Divisor = NVector.X*Line.V.X + NVector.Y*Line.V.Y + NVector.Z*Line.V.Z;
    if (Divisor == 0) return 0;
    // Then evaluate the position of intersection
    float t = (-NVector.X*Line.A.X - NVector.Y*Line.A.Y - NVector.Z*Line.A.Z - Dist)/Divisor;
    Point.X = Line.A.X + t*Line.V.X;
    Point.Y = Line.A.Y + t*Line.V.Y;
    Point.Z = Line.A.Z + t*Line.V.Z;
    return 1;
  };
  // Gets the intersection of Line represented by (P1 and P2) and this plane, returns 1 if success
  INLINE int GetIntersection(C3DPoint P1, C3DPoint P2, C3DPoint &Point) {
    C3DLine Line(P1, P2);
    return GetIntersection(Line, Point);
  };
  // Returns distance of given Point from this plane
  // (can be also of negativ value in case Point lies behind the plane)
  INLINE float Distance(C3DPoint Point) {
    return Point.X * NVector.X + Point.Y * NVector.Y + Point.Z * NVector.Z + Dist;
  };
  // Returns 1 if Point is in front of this plane, 0 otherwise
  INLINE int InFront(C3DPoint Point) {
    return (Distance(Point) > 0 ) ? 1: 0;
  };
  // Advanced hit with a half line. Line.A must be in front of the plane and must points towards the plane.
  // Line.V must be size 1. Returns 1 if hitted, 0 elsewhere.
  INLINE int Hit(C3DLine Line, C3DPoint &Point) {
    if (!InFront(Line.A)) return 0;
    if (Line.V.GetScalarProduct(NVector) >= 0) return 0;
    return GetIntersection(Line, Point);
  };
  // Returns 1 if Point is on this plane (with defined deflection), 0 elsewhere
  INLINE int OnPlane(C3DPoint Point) {
    return (fabs(Point.X * NVector.X + Point.Y * NVector.Y + Point.Z * NVector.Z + Dist) < DEFLECTION ) ? 1: 0;
  };
  INLINE int PointRelation(C3DPoint Point) {
    if (OnPlane(Point)) {
      return PR_ON_PLANE;
    }
    else {
      return (InFront(Point)) ? PR_IN_FRONT : PR_IN_BACK;
    }
  };
  // Revert NVector of this plane whereas plane remains the same
  INLINE CPlane RevertNVector() {
    CPlane Result(NVector.Revert(), -Dist);
    return Result;
  };
};

#endif