// precompiled header files - many files use something of Windows API
#include "ext_options.hpp"

#if _DEMO
	#include "demoConfig.h"
#elif _DED_SERVER
	#include "dedServerConfig.h"
#elif _DESIGN_SRELEASE
	#include "desReleaseConfig.h"
#elif _SUPER_RELEASE
	#include "sReleaseConfig.h"
#elif _MP_DEMO
	#include "mpDemoConfig.h"
#elif _USE_AFX
	#include "afxConfig.h"
#elif _VBS1
	#include "VBS1Config.h"
#else
	#include "normalConfig.h"
#endif

#ifdef _XBOX
	// some platform specific overrides, configuration independent
	#include "xboxConfig.h"
#endif

#include "stdIncludes.h"

// global support for array new/delete
/*
// init_seg user is default
#if _MSC_VER
	#pragma init_seg(user)
#endif
*/

void *ArrayNew( size_t size );
void *ArrayNew( size_t size, const char *file, int line );
void ArrayDelete( void *mem );

#if _MSC_VER
	// make for variable local
	#define CCALL __cdecl
#else
	#define CCALL 
#endif

#ifdef __GNUC__
	#define __cdecl
#endif

#if !_RELEASE && !defined MFC_NEW
	#include "checkMem.hpp"
#endif

#define WIN32_LEAN_AND_MEAN // no OLE or other complicated stuff
#define STRICT 1

// some basic types

#include <memType.h> // basic memory types

//#include <windows.h>
//#include <dinput.h>

#if _MSC_VER
	// make for variable local
	#define for if( false ) {} else for
#endif

// user general include
#include "libIncludes.hpp"

// global diagnostics

void CCALL GlobalShowMessage( int timeMs, const char *msg, ... );
void GlobalAlive();

#define I_AM_ALIVE() GlobalAlive()
