#include "polygon.h"

void CPolygon::RemoveInvalidVertices() {
  int OldIndex, NewIndex;
  for (OldIndex = 1, NewIndex = 1; OldIndex < VertexCount; OldIndex++) {
    if (!Vertex[OldIndex].Near(Vertex[OldIndex - 1])) {
      Vertex[NewIndex] = Vertex[OldIndex];
      NewIndex++;
    }
  }
  VertexCount = NewIndex;
}

void CPolygon::Init(C3DPoint *Vertex, int VertexCount, CPlane Plane) {
  for (int i = 0; i < VertexCount; i++) this->Vertex[i] = Vertex[i];
  this->VertexCount = VertexCount;
  this->Plane = Plane;
}

int CPolygon::Split(CPlane Plane, CPolygon &FPolygon, CPolygon &BPolygon) {
  int i;
  // If all vertices lies on the plane return it in SP_FRONT
  i = 0;
  while (i < VertexCount) {
    if (!Plane.OnPlane(Vertex[i])) break;
    i++;
  }
  if (i == VertexCount) {
    FPolygon = *this;
    return SP_FRONT;
  }
  // Split it elsewhere
  FPolygon.Plane = BPolygon.Plane = this->Plane;
  int LastPosition;
  C3DPoint Intersection;
  int Result;
  // Line between the last and first point
  if (Plane.InFront(Vertex[VertexCount - 1])) {
    if (Plane.InFront(Vertex[0])) {
      FPolygon.Vertex[0] = Vertex[0];
      FPolygon.VertexCount = 1;
      BPolygon.VertexCount = 0;
      LastPosition = SP_FRONT;
    }
    else {
      Plane.GetIntersection(Vertex[VertexCount - 1], Vertex[0], Intersection);
      FPolygon.Vertex[0] = Intersection;
      FPolygon.VertexCount = 1;
      BPolygon.Vertex[0] = Intersection;
      BPolygon.Vertex[1] = Vertex[0];
      BPolygon.VertexCount = 2;
      LastPosition = SP_BACK;
    }
  }
  else {
    if (Plane.InFront(Vertex[0])) {
      Plane.GetIntersection(Vertex[VertexCount - 1], Vertex[0], Intersection);
      FPolygon.Vertex[0] = Intersection;
      FPolygon.Vertex[1] = Vertex[0];
      FPolygon.VertexCount = 2;
      BPolygon.Vertex[0] = Intersection;
      BPolygon.VertexCount = 1;
      LastPosition = SP_FRONT;
    }
    else {
      FPolygon.VertexCount = 0;
      BPolygon.Vertex[0] = Vertex[0];
      BPolygon.VertexCount = 1;
      LastPosition = SP_BACK;
    }
  }
  // Rest of the lines
  for (i = 1; i < VertexCount; i++) {
    if (LastPosition == SP_FRONT) {
      if (Plane.InFront(Vertex[i])) {
        FPolygon.Vertex[FPolygon.VertexCount] = Vertex[i];
        FPolygon.VertexCount++;
        LastPosition = SP_FRONT;
      }
      else {
        Plane.GetIntersection(Vertex[i - 1], Vertex[i], Intersection);
        FPolygon.Vertex[FPolygon.VertexCount] = Intersection;
        FPolygon.VertexCount++;
        BPolygon.Vertex[BPolygon.VertexCount] = Intersection;
        BPolygon.VertexCount++;
        BPolygon.Vertex[BPolygon.VertexCount] = Vertex[i];
        BPolygon.VertexCount++;
        LastPosition = SP_BACK;
      }
    }
    else {
      if (Plane.InFront(Vertex[i])) {
        Plane.GetIntersection(Vertex[i - 1], Vertex[i], Intersection);
        FPolygon.Vertex[FPolygon.VertexCount] = Intersection;
        FPolygon.VertexCount++;
        FPolygon.Vertex[FPolygon.VertexCount] = Vertex[i];
        FPolygon.VertexCount++;
        BPolygon.Vertex[BPolygon.VertexCount] = Intersection;
        BPolygon.VertexCount++;
        LastPosition = SP_FRONT;
      }
      else {
        BPolygon.Vertex[BPolygon.VertexCount] = Vertex[i];
        BPolygon.VertexCount++;
        LastPosition = SP_BACK;
      }
    }
  }
  // Removing inv. vert.
  FPolygon.RemoveInvalidVertices();
  BPolygon.RemoveInvalidVertices();
  // Returning a value
  Result = 0;
  if (FPolygon.VertexCount > 2) Result |= SP_FRONT;
  if (BPolygon.VertexCount > 2) Result |= SP_BACK;
  return Result;
}

C3DPoint CPolygon::GetFocus() {
  C3DPoint Result(0, 0, 0);
  for (int i = 0; i < VertexCount; i++) {
    Result = Result.Move(Vertex[i]);
  }
  return Result.Multiply((float)1.0/VertexCount);
}