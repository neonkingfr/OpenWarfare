// specify extra options

#ifndef _DEBUG
	#pragma inline_depth(255)
	//#pragma inline_depth(128)
	#pragma inline_recursion(on)
#endif

#ifdef _MSC_VER
	#pragma warning(disable:4244;disable:4305)

	#pragma warning(3:4701 4702 4189)
#endif

