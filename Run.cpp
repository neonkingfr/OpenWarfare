#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")

#include <d3d9.h>
#include <iostream>
#include <windows.h>

#include "BicubicFilter.h"

#include "Framework/Form.h"
#include "Framework/Device.h"

int main()
{
	const SIZE wndSize = { 1280, 720 };

	Framework::Form *form = new Framework::Form(0, wndSize, "BicubicFiltering");

	Framework::Device *device = new Framework::Device((UINT16)wndSize.cx, (UINT16)wndSize.cy);

	if (device->CreateDevice(form->GetHandle()))
	{
		IDirect3DTexture9 *srcTexture = 0;

		if ( SUCCEEDED(D3DXCreateTextureFromFileEx(
			device->GetDevice(), 
			"Textures/orig-1024-576.png",
			D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT_NONPOW2, D3DX_DEFAULT, 0, D3DFMT_UNKNOWN, 
			D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &srcTexture)) )
		{
			// class perform bicubic filtering on srcTexture, srcTexture is resampled to window size,
			// where window size is define in variable wndSize
			BicubicFilter *bc = new BicubicFilter(srcTexture);

			if (bc->Initialize(device->GetDevice()))
			{
				form->Show();

				while(form->IsCreated())
				{
					form->DoEvents();

					// perform bilinear filetring
					bc->Render(device->GetDevice());
				}
			}
			else
			{
				std::cout << "Bicubic filter initialization failed" << std::endl; 
			}

			// release all allocated resources
			delete bc;

			srcTexture->Release();
			srcTexture = 0;
		}
		else
			std::cout << "Texture not found" << std::endl;
	}
	else
	{
		std::cout << "Device initialization failed" << std::endl;
	}

	delete form;

	delete device;

	return 0;
}
