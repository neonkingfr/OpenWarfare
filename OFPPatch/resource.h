//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OFPPatch.rc
//
#define IDS_CAPTION                     1
#define IDS_NOT_INSTALLED               2
#define IDS_BAD_VERSION                 3
#define IDS_ALREADY_PATCHED             4
#define IDS_MORE_RECENT                 5
#define IDS_ALREADY_PATCHED2            6
#define IDS_PATCHED_OK                  6
#define IDS_DISK_LOW                    7
#define IDS_UPGRADE_OK                  8
#define IDS_UPGRADE_FAILED              9
#define IDS_MISSING_FILE                10
#define IDS_VERIFY_CAPTION              11
#define IDS_ERROR_IN_FILE               12
#define IDS_ERROR_IN_FILE_NOCRC         30
#define IDS_NO_CRC_VER                  13
#define IDS_BAD_SOURCE                  14
#define IDS_NO_CRC_VER_FILE             15
#define IDS_UPDATE_CAPTION              16
#define IDS_UPDATE_CAPTION2             31
#define IDS_UPGRADE_CAPTION             17
#define IDS_UPGRADE_CAPTION2            32
#define IDS_CHECK_FILE                  18
#define IDS_ADDON_REQ_VERSION           19
#define IDS_REQ_VERSION                 19
#define IDS_WARN_ERROR_IN_FILE          20
#define IDS_ERROR_IN_COPY               21
#define IDS_BAD_CDKEY                   22
#define IDS_ERROR_LOG                   23
#define IDS_INFO_TEXT                   24
#define IDS_INTRO_MESSAGE               25
#define IDS_INTRO_QUESTION              26
#define IDS_INTRO_TEXT                  27
#define IDS_NO_ACCESS_RIGHTS            28
#define IDS_INTRO_WAIT_PLEASE           29
#define IDR_DATA1                       110
#define IDR_DATA2                       111
#define IDR_DATA3                       112
#define IDI_ICON1                       113
#define IDR_BINARY1                     117
#define IDD_SPLASH                      123
#define IDR_COPYFILE                    125
#define IDD_DISTRIBUTION                126
#define IDC_PROGRESS_AVI                1002
#define IDC_CONSOLE_EDIT                1003
#define IDC_CONSOLE_PROGRESS            1005
#define IDC_DISTRIBUTIONS               1007
#define IDC_CONSOLE_PROGRESS_BAR        1008
#define IDC_CONSOLE_PERCENTAGE          1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        127
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
