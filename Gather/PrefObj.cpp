/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/


#include "stdafx.h"
#include "math.h"
#include "mmsystem.h"
#include "Pref.h"
#include "PrefObj.h"

#include <dinput.h>
#include <d3d9.h>
#include <ddraw.h>
#include <Es/Framework/debugLog.hpp>
#include <El/Math/math3d.hpp>

#pragma comment(lib,"ddraw")
/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
*/

/////////////////////////////////////////////////////////////////////////////
// CPosPrefObject


CPosPrefObject::CPosPrefObject()
{
  _d3d << Direct3DCreate9(D3D_SDK_VERSION);
};

CPosPrefObject::~CPosPrefObject()
{
};



#define RegKey "Software\\Codemasters\\Operation Flashpoint"

/// perform a simple rounding
/**
use only top 4 bits of the information
*/

static size_t RoundMemory(size_t memSize)
{
  int log = 20;
  size_t topSize = memSize>>20;
  while (topSize>=0x10)
  {
    log++;
    topSize >>=1;
  }
  size_t mask = (1<<log)-1;
  if ((memSize&mask)>mask/2)
  {
    topSize++;
  }
  return topSize<<(log-20);
}

CString CPosPrefObject::DisplayVRAM(int adapter, CString &agp)
{
  // assume some minimum card abilities (16 MB VRAM)
  // ask directDraw to get information about video ram?
  // we do not know how adapters ID maps to ddraw GUIDS
  // we can handle only the simple case - default adapter is used
  // if users has more adapters, we assume has has plenty of VRAM
  ComRef<IDirectDraw7> ddraw;
  DirectDrawCreateEx(NULL,(void **)ddraw.Init(),IID_IDirectDraw7,NULL);
  if (ddraw)
  {
    size_t memSize,agpSize;
    {
      DDSCAPS2 caps;
      DWORD totalmem = 0,freemem = 0;
      //caps.dwCaps = DDSCAPS_LOCALVIDMEM|DDSCAPS_TEXTURE|DDSCAPS_VIDEOMEMORY;
      caps.dwCaps = DDSCAPS_LOCALVIDMEM|DDSCAPS_VIDEOMEMORY;
      caps.dwCaps2 = 0;
      caps.dwCaps3 = 0;
      caps.dwCaps4 = 0;
      ddraw->GetAvailableVidMem(&caps,&totalmem,&freemem);
      memSize = RoundMemory(totalmem);
    }
    {
      DDSCAPS2 caps;
      DWORD totalmem = 0,freemem = 0;
      caps.dwCaps = DDSCAPS_NONLOCALVIDMEM|DDSCAPS_VIDEOMEMORY;
      caps.dwCaps2 = 0;
      caps.dwCaps3 = 0;
      caps.dwCaps4 = 0;
      ddraw->GetAvailableVidMem(&caps,&totalmem,&freemem);
      agpSize = RoundMemory(totalmem);
    }

    agp.Format("%d MB",agpSize);

    CString memString;
    memString.Format("%d MB",memSize); // physical RAM size
    return memString;
    
  }
  agp = CString("0 MB");
  return CString("0 MB");
}

CString	CPosPrefObject::DetectMemory()
{
  MEMORYSTATUS mem;
  mem.dwLength=sizeof(mem);
  ::GlobalMemoryStatus(&mem);
  size_t memSize = RoundMemory(mem.dwTotalPhys);
  // it seems that memory is not reported correctly (like 31 MB instead of 32 MB)
  // perform a simple rounding
  // use only top 4 bits of the information

  CString memString;
  memString.Format("%d MB",memSize); // physical RAM size
  return memString;
}

#include <Es/Types/pointers.hpp>

#define DECLARE_PRIORITY	\
  HANDLE hProc = GetCurrentProcess();\
  HANDLE hThrd = GetCurrentThread();\
  DWORD dwPrtyCls = GetPriorityClass(hProc);\
  DWORD dwPrty = GetThreadPriority(hThrd)

#define BOOST_PRIORITY	\
  if (dwPrtyCls)\
  if (!SetPriorityClass(hProc, REALTIME_PRIORITY_CLASS  /*HIGH_PRIORITY_CLASS*/))\
  dwPrtyCls = 0;\
  if (dwPrty)\
  if (!SetThreadPriority(hThrd, THREAD_PRIORITY_HIGHEST))\
  dwPrty = 0

#define RESTORE_PRIORITY	\
  if (dwPrty)\
  SetThreadPriority(hThrd, dwPrty);\
  if (dwPrtyCls)\
  SetPriorityClass(hProc, dwPrtyCls)

int CPosPrefObject::Benchmark()
{
#if _DEBUG
  return 800;
#else

  AfxGetApp()->BeginWaitCursor();


#define NVec 100000

  DECLARE_PRIORITY;
  BOOST_PRIORITY;

  Temp<Vector3> src(NVec),dst(NVec);
  DWORD timeStart;
  //Vector3 src[NVec],dst[NVec];
  for( int b=0; b<20; b++ )
  {
    int i;
    if( b==2 ) timeStart=::GetTickCount();
    for( i=0; i<NVec; i++ )
    {
      src[i]=Vector3(0.2f,0.6f,-0.5f);
    }
    Matrix4 translation(MTranslation,Vector3(56.23f*b,253.2f,-569.3f));
    Matrix4 rotationY(MRotationY,1.25f*b);
    Matrix4 rotationZ(MRotationZ,-0.66f*b);
    for( i=0; i<NVec; i++ )
    {
      Matrix4 transform=rotationZ*rotationY*translation;
      dst[i].SetFastTransform(transform,src[i]);
    }
  }

  DWORD timeEnd=::GetTickCount();
  //double benchmark=3600000./(timeEnd-timeStart);
  //double benchmark=1350000./(timeEnd-timeStart);
  double benchmark=1500000./(timeEnd-timeStart);

  RESTORE_PRIORITY;

  AfxGetApp()->EndWaitCursor();
  return int(benchmark);
  #endif
};



#pragma warning( disable : 4035 )
#define CPUID	__asm __emit 0fh __asm __emit 0a2h
#define RDTCS	__asm __emit 0fh __asm __emit 031h

int CanCpuid()
{
  __asm
  {
    pushfd
    pop eax
    mov ecx,eax
    xor eax,200000h
    push eax
    popfd
    pushfd
    pop eax
    xor eax, ecx
  }
};

int CpuInfo( char *vendor )
{
  __asm
  {
    mov  esi, vendor
    push ebx
    push ecx
    push edx
    xor eax,eax
    cpuid
    mov [esi],ebx
    mov [esi+4],edx
    mov [esi+8],ecx
    pop edx
    pop ecx
    pop ebx
  }
};

int CpuExtInfo( int &sig, int *feat)
{
  __asm
  {
    mov  esi, sig
    mov  edi, feat
    push ebx
    push ecx
    push edx
    mov eax,1
    cpuid
    mov [esi],eax
    mov [edi],edx
    mov [edi+4],ecx
    pop edx
    pop ecx
    pop ebx
  }
};

static unsigned int CpuBrandMax()
{
  __asm
  {
    push ebx
    push ecx
    push edx
    mov eax,0x80000000
    cpuid
    pop edx
    pop ecx
    pop ebx
  }
}

static void CpuBrandString(int id, char *b)
{
  __asm
  {
    mov  esi, b
    push ebx
    push ecx
    push edx
    mov eax,id
    cpuid
    cmp eax,0x80000004
    mov [esi],eax
    mov [esi+4],ebx
    mov [esi+8],ecx
    mov [esi+12],edx
    pop edx
    pop ecx
    pop ebx
  }
}

int CpuRdtsc()
{
  int nVal;
  __asm
  {
    push ebx
    push ecx
    push edx
    xor eax,eax
    cpuid
    rdtsc
    mov esi,eax
    xor eax,eax
    mov nVal,esi
    cpuid
    pop edx
    pop ecx
    pop ebx
  }
  return nVal;
};

CString	CPosPrefObject::DetectCPU(CString &featureString)
{
  CString sResult;

  char cpuVendor[12];
  // first of all check if CPUID is available
  int canCpuid=CanCpuid();
  if( !canCpuid )
  {
    sResult = "486 ";
    return sResult;
  }

  int  cpuInfo=CpuInfo(cpuVendor);
  bool isIntel=false;
  bool isAMD=false;
  if( !strncmp(cpuVendor,"GenuineIntel",12) )
  {
    isIntel=true;
    sResult = "Intel ";
  }
  else if( !strncmp(cpuVendor,"AuthenticAMD",12) )
  {
    isAMD = true;
    sResult = "AMD ";
  }
  else if( !strncmp(cpuVendor,"CyrixInstead",12) )
  {
    sResult+="Cyrix ";
  }
  else
  {
    sResult+="Unknown vendor ";
  }

  int signature=0, features[2];
  float perfBoost=1.0f;
  CpuExtInfo(signature,features);
  if (features[0]&(1<<23)) featureString += "MMX ";
  if (features[0]&(1<<25)) featureString += "SSE ";
  if (features[0]&(1<<26)) featureString += "SSE2 ";
  if (features[1]&(1<<0)) featureString += "SSE3 ";
  if (features[0]&(1<<28)) featureString += "HT ";

  featureString.Trim(" ");

  if (isIntel || isAMD)
  {
    unsigned int brandStringLen = CpuBrandMax();
    if (brandStringLen>=0x80000002)
    {
      CString brandString;
      if (brandStringLen>=0x80000004) brandStringLen = 0x80000004;
      for (unsigned int bi=0x80000002; bi<=brandStringLen; bi++)
      {
        char brand[17];
        brand[16] = 0;
        CpuBrandString(bi,brand);
        brandString += brand;
      }
      brandString.Trim(" ");
      sResult = brandString;
      // check if brand string is specific enough to be used
      static const char generic[]="Genuine Intel";
      if (strncmp(sResult,generic,sizeof(generic-1)))
      {
        if (!sResult.IsEmpty()) return sResult;
      }
    }
  }

  if( isIntel )
  {
    int type=(signature>>4)&0xff;
    switch( type )
    {
    case 0x51:
    case 0x52:
    case 0x53:
      sResult="Intel Pentium ";
      break;
    case 0x54:
      sResult="Intel Pentium MMX ";
      perfBoost=1.12f;
      break;
    case 0x61:
      sResult="Intel Pentium Pro ";
      perfBoost=1.4f;
      break;
    case 0x63:
    case 0x65:
      sResult="Intel Pentium II ";
      perfBoost=1.6f;
      break;
    case 0x66:
      sResult="Intel Celeron ";
      perfBoost=1.5f;
      break;
    case 0x67:
    case 0x68:
    case 0x6b:
      sResult="Intel Pentium III ";
      perfBoost=1.6f;
      break;
    case 0x69:
    case 0x6d:
      sResult="Intel Pentium M ";
      perfBoost=1.6f;
      break;
    case 0x6a:
      sResult="Intel Pentium III Xeon ";
      perfBoost=1.6f;
      break;
    case 0xf0:
    case 0xf1:
    case 0xf2:
    case 0xf3:
      perfBoost=1.4f;
      sResult="Intel Pentium 4 ";
      break;
    default:
      break;
    }
  }
  else if (isAMD)
  {
    int type=(signature>>4)&0xff;
    switch (type)
    {
    case 0x50:
    case 0x51:
    case 0x52:
    case 0x53:
      sResult = "AMD-K5 ";
      break;
    case 0x56:
    case 0x57:
      sResult = "AMD-K6 ";
      break;
    case 0x58:
      sResult = "AMD-K6-2 ";
      break;
    case 0x59:
      sResult = "AMD-K6-III ";
      break;
    case 0x61:
    case 0x62:
    case 0x64:
      sResult = "AMD-Athlon ";
      break;
    case 0x63:
      sResult = "AMD-Duron ";
      break;
    default:
      sResult = "AMD ";
      break;
    }

  }

  return sResult;
}

CString	CPosPrefObject::DetectCPUFreq()
{

  // performance counter not available
  // set timer to maximal precision
  bool msTimer=(timeBeginPeriod(1)==TIMERR_NOERROR);
  int timeResol=1000;
  if( msTimer ) timeResol=1;
  else
  {
    // determine actual timer precision
    int begTime=timeGetTime();
    int time=begTime;
    while( time<begTime+100 )
    {
      int lastTime=time;
      time=timeGetTime();
      if( time>lastTime && time<lastTime+timeResol ) timeResol=time-lastTime;
    }
  }
  //WMessageBox::Messagef(this,WMsgBSOk,"Timer","%d ms",timeResol);
  // measure CPU speed
  float maxClock=0;
  for( int i=0; i<3; i++ )
  {
    // note: eTime-sTime >= actual time elapsed between RDTSC
    // therefore: clock <= actual CPU clock
    int sTime=timeGetTime();
    int sClock=CpuRdtsc();
    int eTime;
    do
    {
      eTime=timeGetTime();
    } while( eTime<sTime+500 );
    int eClock=CpuRdtsc();
    eTime=timeGetTime()+timeResol;
    float clock=(float)(double(eClock-sClock)*1e-3/(eTime-sTime));
    if( clock>maxClock ) maxClock=clock;
  }
  if( msTimer ) timeEndPeriod(1);

  char clockStr[256];
  if (maxClock>=1000)
  {
    sprintf(clockStr,"%.1f GHz",maxClock/1000);
  }
  else
  {
    sprintf(clockStr,"%.0f MHz",maxClock);
  }
  return clockStr;
}
