/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPrefErr.cpp : implementation file
//

#include "stdafx.h"
#include "Pref.h"
#include "PrefObj.h"
#include "io.h"
#include "mapi.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Text

#define MSG_SUBJECT			"Bohemia Interactive System Info Survey"

#define MSG_TO_ALIAS		"Bohemia Interactive Survey"
#define MSG_TO_ADRESS		"sysinfosurvey@bistudio.com"

static char *MSG_TEXT=
	"Please, do not edit the information provided below.\n"
	"Any system information which has been manually changed will be ignored.\n"
  "\n"
  "Note: this message is processed automatically.\n"
  "Do not write anything here, as no human will read it,\n"
  "and it could make automated analysis unreliable.\n"
  "\n"
;


/////////////////////////////////////////////////////////////////////////////
// DoSendErrorFile()

typedef ULONG FAR PASCAL MAPISaveMailFnc(
  LHANDLE lhSession, ULONG ulUIParam, 
  lpMapiMessage lpMessage, FLAGS flFlags, ULONG ulReserved
);

static BOOL DoSendMailFile(const char *text)
{
	BOOL bSuccess = FALSE;

	MapiRecipDesc MapiRecp;
	memset(&MapiRecp,0,sizeof(MapiRecp));
	MapiRecp.ulRecipClass = MAPI_TO;
	MapiRecp.lpszName   = MSG_TO_ALIAS;
	MapiRecp.lpszAddress = MSG_TO_ADRESS;

	MapiFileDesc MapiFile[3];
	memset(MapiFile,0,sizeof(MapiFile));

	int i=0;
  /*
  // file attachment
	MapiFile[i].nPosition = 0xFFFFFFFF;
	MapiFile[i].lpszPathName = pFile;
	i++;
  */

	MapiMessage  MapiMsg;
	memset(&MapiMsg,0,sizeof(MapiMsg));
	MapiMsg.lpszSubject  = MSG_SUBJECT;
  CString messageText = MSG_TEXT;
  messageText += text;
	MapiMsg.lpszNoteText = (char *)(const char *)messageText;
	MapiMsg.nRecipCount  = 1;
	MapiMsg.lpRecips		 = &MapiRecp;
	MapiMsg.nFileCount	 = i;
	MapiMsg.lpFiles			 = MapiFile;
	
	HINSTANCE hLib = LoadLibrary("mapi32.dll");
	if (hLib == NULL)
	{
		AfxMessageBox("MAPI interface not fond",MB_OK|MB_ICONSTOP);
		return bSuccess;
	};

	MAPISaveMailFnc* pMAPISendMail = (MAPISaveMailFnc*)GetProcAddress(hLib, "MAPISendMail"); 
	try
	{
		if (pMAPISendMail)
		{
			char oldDirectory[1024];
			::GetCurrentDirectory(sizeof(oldDirectory), oldDirectory);
			ULONG nRes = (*pMAPISendMail)(0,0,&MapiMsg,MAPI_DIALOG|MAPI_LOGON_UI|MAPI_NEW_SESSION,0);
			bSuccess = (nRes == SUCCESS_SUCCESS); 
			::SetCurrentDirectory(oldDirectory);
		}
		FreeLibrary(hLib); 
	}
	catch( ... )
	{
		AfxMessageBox("MAPI Error",MB_OK);
	}
	return bSuccess;
};

void DoSendErrorFile(const char *text)
{
	BOOL bSuccess = DoSendMailFile(text);
	// sma�u soubor
	if (bSuccess)
	{
	}
};

