/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPref.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Pref.h"
#include "PrefObj.h"
#include "PrefDlg.h"

#include <El/stringtable/stringtable.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosPrefApp

BEGIN_MESSAGE_MAP(CPosPrefApp, CWinApp)
	//{{AFX_MSG_MAP(CPosPrefApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPosPrefApp construction

CPosPrefApp::CPosPrefApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CPosPrefApp object

CPosPrefApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CPosPrefApp initialization

void DoSendErrorFile(const char *text);

BOOL CPosPrefApp::InitInstance()
{
	AfxEnableControlContainer();

  LANGID userLang = GetUserDefaultUILanguage();
  //LANGID sysLang = GetSystemDefaultUILanguage();

	switch (PRIMARYLANGID(userLang))
	{
	case LANG_FRENCH:
		GLanguage = "French";
		break;
	case LANG_SPANISH:
		GLanguage = "Spanish";
		break;
	case LANG_ITALIAN:
		GLanguage = "Italian";
		break;
	case LANG_GERMAN:
		GLanguage = "German";
		break;
	case LANG_CZECH:
		GLanguage = "Czech";
		break;
	case LANG_POLISH:
		GLanguage = "Polish";
		break;
	case LANG_RUSSIAN:
		GLanguage = "Russian";
		break;
	default:
		GLanguage = "English";
		break;
	}
	
  // load stringtable from current directory
	LoadStringtable("global", "stringtable.csv");

	_prefAppName = PrefLocalizeString (_T ("PREF_APPLICATION_NAME"));

	// error file

	CPosPrefDlg dlg;

  // set windows language as starting language setting (may be changed in LoadConfig)

	// preferences
	int nResponse = dlg.DoModal();
	
  switch (nResponse)
  {
    case IDC_MAIN_SENDMAIL:
      DoSendErrorFile(dlg._result);
      break;
    case IDC_MAIN_SENDWEB:
      break;
  }
	return TRUE;
}

BOOL CPosPrefApp::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
	// no help
	if (nID == ID_HELP)
		return FALSE;
	return CWinApp::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

/////////////////////////////////////////////////////////////////////////////
// RunGame

CString CPosPrefApp::PrefLocalizeString (LPCTSTR str)
{
	CString d;
	CString s = LocalizeString (str);
	int a = 0;
	int f = s.Find (_T ('\\'));
	int max = s.GetLength () - 1;
	while (f >= 0 && f < max)
	{
		switch (s[f + 1])
		{
		case _T ('\\'):
			d += s.Mid (a, f - a);
			d += _T ('\\');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		case _T ('t'):
			d += s.Mid (a, f - a);
			d += _T ('\t');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		case _T ('n'):
			d += s.Mid (a, f - a);
			d += _T ('\n');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		case _T ('r'):
			d += s.Mid (a, f - a);
			d += _T ('\r');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		case _T ('\"'):
			d += s.Mid (a, f - a);
			d += _T ('\"');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		case _T ('\''):
			d += s.Mid (a, f - a);
			d += _T ('\'');
			a = f + 2;
			f = s.Find (_T ('\\'), a);
			break;

		default:
			f = s.Find (_T ('\\'), f + 1);
			break;
		}
	}
	d += s.Mid (a);
	return d;
}

bool CPosPrefApp::ApplyApplicationNameAndStringTable (CString &text, bool applyApplName)
{
	bool r = false;
	int f = text.FindOneOf (_T ("%$"));
	int max = text.GetLength () - 1;
	if (f >= 0 && f < max)
	{
		do {
			switch (text[f])
			{
			case _T ('$'):
				{
					int a = f + 1;
					while ((a <= max) && 
						(text[a] >= _T ('A') && text[a] <= _T ('Z') ||
						text[a] >= _T ('0') && text[a] <= _T ('9') ||
						text[a] >= _T ('a') && text[a] <= _T ('z') ||
						text[a] == _T ('_')))
					{
						++a;
					}
					text = text.Left (f) + 
						PrefLocalizeString (text.Mid (f + 1, a - f - 1)) + 
						text.Mid (a);
					max = text.GetLength () - 1;
					f = PrefFindOneOf (text, _T ("%$"), f);
					r = true;
				}
				break;

			case _T ('%'):
				switch (text[f + 1])
				{
				case _T ('0'):
					if (applyApplName)
					{
						text = text.Left (f) + _prefAppName + text.Mid (f + 2);
						max = text.GetLength () - 1;
						f = PrefFindOneOf (text, _T ("%$"), f);
						r = true;
					}
					else
					{
						f = PrefFindOneOf (text, _T ("%$"), f + 1);
					}
					break;

				default:
					f = PrefFindOneOf (text, _T ("%$"), f + 1);
					break;
				}
				break;

			default:
				f = PrefFindOneOf (text, _T ("%$"), f);
				break;
			}
		} while (f >= 0 && f < max);
	}

	return r;
}

int CPosPrefApp::PrefFindOneOf (CString &where, LPCTSTR charSet, int start)
{
	int len = where.GetLength ();
	if (start < 0 || start >= len)
	{
		return -1;
	}

	int charSetLen = _tcslen (charSet);
	while (start < len)
	{
		TCHAR c = where[start];
		for (int i = 0; i < charSetLen; ++i)
		{
			if (c != charSet[i])
			{
				continue;
			}

			return start;
		}
		++start;
	}
	return -1;
}

void CPosPrefApp::ApplyApplicationNameAndStringTable (CWnd *item)
{
	CString text;
	item->GetWindowText (text);

	if (ApplyApplicationNameAndStringTable (text, true))
	{
		item->SetWindowText (text);
	}

	CWnd *subItem = item->GetTopWindow ();
	while (subItem != NULL)
	{
		ApplyApplicationNameAndStringTable (subItem);
		subItem = subItem->GetNextWindow (GW_HWNDNEXT);
	}
}
