/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/


/////////////////////////////////////////////////////////////////////////////
// CPosPrefObject - data storage


//#include "posRegistry.h"
#include <Es/Containers/array.hpp>
#include <Es/Strings/rstring.hpp>
#include <El/paramfile/paramfile.hpp>

struct Info3Dfx
{
  char	name[80];
  char	version[80];
  int		frameBufMB;
  int		totalTexMB;
  int		totalTMUse;
};

struct SResolution
{
  CString pText;
  int			nWidth;
  int			nHeight;
  int			nBPP;

  SResolution(){}
  SResolution(int w, int h, int bpp)
    :nWidth(w),nHeight(h),nBPP(bpp)
  {
    pText.Format("%5d x %4d x %2d",w,h,bpp);
  }
  bool operator == (const SResolution &w) const;
};
TypeIsGeneric(SResolution)

struct SAdapter
{
  UINT ordinal; // ordinal
  CString name; // user friendly name
};
TypeIsGeneric(SAdapter)

struct SControlDef
{
  const char*	pAction;		
  const char*	pPredefined;
  BOOL		bUserDef;
  LONG		nDefValue;
  LONG		nCurValue;
  const char* pCfgName;
};

#include <d3d9.h>

class CPosPrefObject : public CObject
{
  ComRef<IDirect3D9> _d3d;
public:
  CPosPrefObject();
  ~CPosPrefObject();

  // pr�ce s daty
  int Benchmark();
  CString DisplayVRAM(int adapter, CString &agp);

  CString	DetectCPU(CString &features);
  CString	DetectCPUFreq();
  CString	DetectMemory();

  // data
public:
};

/////////////////////////////////////////////////////////////////////////////

