//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Pref.rc
//
#define IDD_POSPREF_DIALOG              102
#define IDD_PROP_SOUND                  110
#define IDR_MAINFRAME                   128
#define IDD_POSPREF_CONTROLS            132
#define IDD_SEND_ERRFILE                134
#define IDC_MAIN_PLAY                   1000
#define IDI_SYSTEM                      1000
#define IDC_MAIN_SENDMAIL               1000
#define IDC_MAIN_AUTODETECT             1001
#define IDS_BTN_CANCEL                  1001
#define IDI_DISPLAY                     1001
#define IDS_BTN_OK                      1002
#define IDI_CHIP                        1002
#define IDC_MAIN_ADVANCED               1003
#define IDC_CPU                         1004
#define IDC_RAM                         1005
#define IDC_ESTIM_PERFORMANCE           1006
#define IDC_BENCHMARK_RESULT            1007
#define IDC_DISPLAY_DEVICE              1009
#define IDC_DISPLAY_INFO                1010
#define IDC_DISPLAY_ADAPTER             1010
#define IDC_RESOLUTION                  1011
#define IDC_CPU_TYPE                    1012
#define IDC_MAIN_CONTROLS               1015
#define IDC_CHECK_BACKGROUND            1020
#define IDC_SLIDER_MAXOBJECTS           1020
#define IDC_CHECK_REFLECTION            1021
#define IDC_LIST                        1034
#define IDC_CHANGE                      1035
#define IDC_DEFAULT                     1036
#define IDC_BUTTON1                     1036
#define IDC_AUDIO_SETUP                 1036
#define IDC_LANGUAGE                    1040
#define IDC_PRODUCT                     1042
#define IDC_SYSINFOTEXT                 1043
#define IDC_MAIN_SENDWEB                1044

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1045
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
