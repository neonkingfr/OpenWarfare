/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright © Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPrefDlg.cpp : implementation file
//

#include "stdafx.h"
#include "math.h"
#include "Pref.h"
#include "PrefObj.h"
#include "PrefDlg.h"
#include <mmsystem.h>
#include <winioctl.h>
#include <dsound.h>
#include <dinput.h>

#include <Wbemidl.h>

#include <El/paramFile/paramFile.hpp>

#define IEOL "\r\n"

#pragma comment(lib,"d3d9")
#pragma comment(lib,"dsound")
#pragma comment(lib,"dinput8")

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPosPrefDlg dialog

CPosPrefDlg::CPosPrefDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPosPrefDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPosPrefDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	_d3d << Direct3DCreate9(D3D_SDK_VERSION);

}

CPosPrefDlg::~CPosPrefDlg()
{
}

BOOL CPosPrefDlg::OnInitDialog()
{
	SetCursor(LoadCursor(NULL, IDC_WAIT));

	CDialog::OnInitDialog();
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// update zobrazení dat
	UpdateAll();

	((CPosPrefApp*) AfxGetApp ())->ApplyApplicationNameAndStringTable (this);
	return TRUE;  
}

void CPosPrefDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CPosPrefDlg)
  //}}AFX_DATA_MAP

  if (SAVEDATA)
  {
  }
  DDX_Control(pDX, IDC_SYSINFOTEXT, _infoText);
}

BEGIN_MESSAGE_MAP(CPosPrefDlg, CDialog)
	//{{AFX_MSG_MAP(CPosPrefDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_HSCROLL()
	//}}AFX_MSG_MAP
  ON_BN_CLICKED(IDC_MAIN_SENDMAIL, OnBnClickedMainSendmail)
  ON_BN_CLICKED(IDC_MAIN_SENDWEB, OnBnClickedMainSendweb)
END_MESSAGE_MAP()

void CPosPrefDlg::ReportStorage()
{
  char drives[4096];
  drives[0]=0;
  drives[1]=0;
  GetLogicalDriveStrings(sizeof(drives),drives);

  
  ULONGLONG totalDiskSpace = 0;
  int nCDRom = 0;
  int nDVDRom = 0;
  for (const char *drive = drives; *drive; drive += strlen(drive)+1)
  {
//    DWORD maxCompLength;
//    DWORD flags = 0;
//    GetVolumeInformation(
//      drive,NULL,0,NULL,
//      maxCompLength,&flags,
//      NULL,0
//    );
//    if (flags&)
    char devicePath[1024];
    // no trailing backslash here
    CString dosName = drive;
    dosName = dosName.TrimRight("\\");
    DWORD dos = QueryDosDevice(dosName,devicePath,sizeof(devicePath));

    // subst drives start with "\\??\\"
    const char substDrive[] = "\\??\\";
    if (dos>0 && !strncmp(devicePath,substDrive,sizeof(substDrive)-1))
    {
      // ignore subst drives
      continue;
    }
    UINT type = GetDriveType(drive);

    if (type==DRIVE_FIXED)
    {
      ULARGE_INTEGER total;
      total.QuadPart= 0;
      GetDiskFreeSpaceEx(drive,NULL,&total,NULL);
      totalDiskSpace += total.QuadPart;
    }
    else if (type==DRIVE_CDROM)
    {
      nCDRom++;

      CString deviceName = "\\\\.\\" + dosName;
      HANDLE device = CreateFile(
        deviceName,0,FILE_SHARE_READ|FILE_SHARE_WRITE,
        NULL,OPEN_EXISTING,0,NULL
      );
      if (device!=INVALID_HANDLE_VALUE)
      {
        //CHANGER_PRODUCT_DATA product;
        #if 0
        // somehow this does not work for me
        DISK_GEOMETRY geometries[100];
        DWORD returnedGeom = sizeof(geometries);
        BOOL okGeom = DeviceIoControl(
          device,IOCTL_STORAGE_GET_MEDIA_TYPES,
          NULL,0, // in buffer
          geometries,sizeof(geometries),&returnedGeom,NULL
        );
        if (okGeom)
        {
          int nGeometries = returnedGeom/sizeof(DISK_GEOMETRY);
          for (int i=0; i<nGeometries; i++)
          {
          }
        }
        else
        {
          HRESULT hr = GetLastError();
          hr = hr;
        }
        #endif
        // IOCTL_STORAGE_GET_MEDIA_TYPES_EX requires WinXP
        int numDevInfo = 20;
        size_t addDevInfo = sizeof(DEVICE_MEDIA_INFO)*numDevInfo;
        size_t typesSize = sizeof(GET_MEDIA_TYPES)+addDevInfo;
        //GET_MEDIA_TYPES types;
        GET_MEDIA_TYPES *types = (GET_MEDIA_TYPES *)new char[typesSize];
        DWORD returned;
        BOOL ok = DeviceIoControl(
          device,IOCTL_STORAGE_GET_MEDIA_TYPES_EX,
          NULL,0, // in buffer
          types,typesSize,&returned,NULL
        );
        if (ok)
        {
          if (types->DeviceType == FILE_DEVICE_DVD)
          {
            nDVDRom++;
          }
        }
        else
        {
          HRESULT hr = GetLastError();
          hr = hr;
        }
        delete[] (char *)types;
        CloseHandle(device);
      }


    }
    //_info << "  Drive " << drive << IEOL;
    
  }
  float spaceGB = float(totalDiskSpace/(1024*1024*1024));
  _info << Format("  Hard disk: %.0f GB",spaceGB) << IEOL;
  if (nCDRom>nDVDRom)
  {
    _info << Format("  CD Drive: %d",nCDRom-nDVDRom) << IEOL;
  }
  if (nDVDRom>0)
  {
    _info << Format("  DVD Drive: %d",nDVDRom) << IEOL;
  }

  // we could use WMI to get info about the storage
  // however WMI is not standard part of Win 95/98/ME
}

static BOOL CALLBACK DSAudioCallback(
  LPGUID  lpGuid,    
  LPCSTR  lpcstrDescription,  
  LPCSTR  lpcstrModule,   
  LPVOID  lpContext    
)
{
  if (lpGuid==NULL) return TRUE;
  CPosPrefDlg *ctx = (CPosPrefDlg *)lpContext;
  ctx->GetOutput() << "Audio" << IEOL;
  ctx->GetOutput() << "  " << lpcstrDescription << IEOL;

  ComRef<IDirectSound8> ds;
  HRESULT hr = CoCreateInstance(
    CLSID_DirectSound8, NULL, CLSCTX_INPROC, 
    IID_IDirectSound8, (void**)ds.Init()
  );
  if( FAILED(hr) || !ds )
  {
    return TRUE;
  }
  ds->Initialize(NULL);
  DSCAPS caps;
  caps.dwSize = sizeof(caps);
  ds->GetCaps(&caps);
  ctx->GetOutput() << Format("  3D buffers: %d",caps.dwFreeHw3DAllBuffers) << IEOL;
  return TRUE;
}



void CPosPrefDlg::ReportAudio()
{
  DirectSoundEnumerate(DSAudioCallback,this);

}

static BOOL CALLBACK EnumJoyCallback(LPCDIDEVICEINSTANCE lpddi, LPVOID context)
{
  CPosPrefDlg *ctx = (CPosPrefDlg *)context;
  ctx->GetOutput() << "  " << lpddi->tszProductName << IEOL;
  return TRUE;
}


void CPosPrefDlg::ReportInput()
{
  ComRef<IDirectInput8> dInput;
  HRESULT err=DirectInput8Create(
    AfxGetApp()->m_hInstance, DIRECTINPUT_VERSION,
    IID_IDirectInput8 ,(void **)dInput.Init(), NULL
  );
  if (!SUCCEEDED(err))
  {
    _info << "No IDirectInput8" << IEOL;
    return;
  }
  dInput->EnumDevices(
    DI8DEVCLASS_GAMECTRL, EnumJoyCallback, this, DIEDFL_ATTACHEDONLY
  );

}

  void CPosPrefDlg::ReportOS()
{
  OSVERSIONINFO version;
  version.dwOSVersionInfoSize = sizeof(version);
  GetVersionEx(&version);
  int major = version.dwMajorVersion;
  int minor = version.dwMinorVersion;
  if (major<4)
  {
    RString versionNum = Format("%d.%.02d",major,minor);
    _info << "  WinNT " << versionNum << IEOL;
  }
  else if (major==4)
  {
    if (minor==0) _info << "  Window 95" << IEOL;
    else if (minor<=10) _info << "  Windows 98" << IEOL;
    else if (minor<=90) _info << "  Windows ME" << IEOL;
  }
  else if (major==5 && minor<=2)
  {
    OSVERSIONINFOEX versionEx;
    versionEx.dwOSVersionInfoSize = sizeof(versionEx);
    GetVersionEx((OSVERSIONINFO *)&versionEx);

    if (minor==0) _info << "  Windows 2000";
    else if (minor<=1) _info << "  Windows XP";
    else if (minor<=2) _info << "  Windows Server 2003";
    const char *sp = versionEx.szCSDVersion;
    if (*sp)
    {
      _info << " " << sp;
    }
    _info << IEOL;
  }
  else
  {
    RString versionNum = Format("%d.%.02d",major,minor);
    RString buildNum = Format(" Build %d",version.dwBuildNumber);
    _info << "  Windows " << versionNum << buildNum << IEOL;
  }
}


void CPosPrefDlg::UpdateAll()
{
  CoInitialize(NULL);
  CString cpuFeatures;
  CString cpuName = m_Preferences.DetectCPU(cpuFeatures);
  CString cpuFreq = m_Preferences.DetectCPUFreq();
  CString memory = m_Preferences.DetectMemory();
  //int corePerCPU,logPerCPU;
  //m_Preferences.DetectNumberOfCPUs()

  SYSTEM_INFO sysInfo;
  GetSystemInfo(&sysInfo);

  _info << "CPU" << IEOL;
  _info << "  " << cpuName << IEOL;
  _info << "  Frequency: " << cpuFreq << IEOL;
  _info << Format("  Logical CPUs: %d",sysInfo.dwNumberOfProcessors) << IEOL;
  _info << "  Features: " << cpuFeatures << IEOL;
  _info << "  RAM: "<< memory << IEOL;


  CString benchmark;
  benchmark.Format("%d",m_Preferences.Benchmark());
  _info << "  Performance: " << benchmark << IEOL;

  _info << "------------------------" << IEOL;
  if (_d3d)
  {
	  // display device - select
	  ScanD3DAdapters();
  }
  else
  {
    _info << "DirectX 9.0c not installed" << IEOL;
  }

  _info << "------------------------" << IEOL;

  ReportAudio();

  _info << "------------------------" << IEOL;
  _info << "Storage" << IEOL;

  ReportStorage();

  _info << "------------------------" << IEOL;
  _info << "Game Controllers" << IEOL;

  ReportInput();

  _info << "------------------------" << IEOL;
  _info << "Operating System" << IEOL;

  ReportOS();

  CString text(_info.str(),_info.pcount());
  int hash = CalculateStringHashValue(text);

  int hashEncoded = hash&0x3f;
  int hashEncodedDots = (hashEncoded>>4) + 3;
  int hashEncodedLine = (hashEncoded&0xf) + 24;
  CString hashDots;
  CString hashLine;
  for (int i=0; i<hashEncodedDots; i++) hashDots += '.';
  for (int i=0; i<hashEncodedLine; i++) hashLine += '=';
  
  static const char header[] = 
    "SYSTEM INFO BEGINS ";
  static const char footer[] = 
    "SYSTEM INFO ENDS ";
  text = (
    CString(header) + hashDots + IEOL+
    hashLine + IEOL +
    text +
    hashLine + IEOL +
    CString(footer) + IEOL
  );
  _infoText.SetWindowText(text);
  _result = text;
  CoUninitialize();
};

/////////////////////////////////////////////////////////////////////////////
// CPosPrefDlg message handlers

void CPosPrefDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPosPrefDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


/////////////////////////////////////////////////////////////////////////////
// SLIDERS

void CPosPrefDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

bool SResolution::operator == (const SResolution &w) const
{
	return w.nHeight==nHeight && w.nWidth==nWidth && w.nBPP==nBPP;
}



void CPosPrefDlg::ScanD3DAdapter(UINT adapter)
{
  _info << "Display adapter" << IEOL;
	D3DADAPTER_IDENTIFIER9 id;
	_d3d->GetAdapterIdentifier(adapter,0,&id);
  _info << "  " << id.Description << IEOL;

  int maxArea = 0;
  int maxAreaW = 0;
  int maxAreaH = 0;
  static const D3DFORMAT formats[] = {D3DFMT_X8R8G8B8,D3DFMT_X1R5G5B5};
  for (int fi=0; fi<lenof(formats); fi++)
  {
    D3DFORMAT format = formats[fi];
    for (UINT mode = 0; mode<_d3d->GetAdapterModeCount(adapter,format); mode++)
	  {
		  D3DDISPLAYMODE dmode;
		  _d3d->EnumAdapterModes(adapter,format,mode,&dmode);
		  int bpp = 16;
		  switch (dmode.Format)
		  {
			  case D3DFMT_R8G8B8: bpp = 24; break;
			  case D3DFMT_A8R8G8B8:
			  case D3DFMT_X8R8G8B8: bpp = 32; break;
			  case D3DFMT_R3G3B2: bpp = 8; break;
		  }
      int area = dmode.Width * dmode.Height;
      if (area>maxArea)
      {
        maxArea = area;
        maxAreaW = dmode.Width;
        maxAreaH = dmode.Height;
      }
	  }
  }

	D3DCAPS9 caps;
	_d3d->GetDeviceCaps(adapter, D3DDEVTYPE_HAL, &caps);

  CString agp;
  _info << "  VRAM: " << m_Preferences.DisplayVRAM(adapter,agp) << IEOL;
  _info << "  VRAM (AGP): " << agp << IEOL;

  _info << "  HW T&L: " << ((caps.DevCaps&D3DDEVCAPS_HWTRANSFORMANDLIGHT) ? "Yes" : "No") << IEOL;

  int vsMajor = (caps.VertexShaderVersion>>8)&0xff;
  int vsMinor = caps.VertexShaderVersion&0xff;
  int psMajor = (caps.PixelShaderVersion>>8)&0xff;
  int psMinor = caps.PixelShaderVersion&0xff;
  _info << Format(
    "  Vertex/Pixel Shaders: %d.%d/%d.%d",vsMajor,vsMinor,psMajor,psMinor
  ) << IEOL;
  _info << "  Max. resolution: " << Format("%dx%d",maxAreaW,maxAreaH) << IEOL;
  _info
    << "  Max. texture size: "
    << Format("%dx%d",caps.MaxTextureHeight,caps.MaxTextureWidth) << IEOL;
}

void CPosPrefDlg::ScanD3DAdapters()
{
	for (UINT adapter = 0; adapter<_d3d->GetAdapterCount(); adapter++)
	{
    ScanD3DAdapter(adapter);
	}
}




void CPosPrefDlg::OnBnClickedMainSendmail()
{
  int ret = MessageBox(
    "Are you sure you want to send the system info?\nPlease, send it only once.",
    NULL,MB_YESNO
  );
  if (ret==IDYES)
  {
    EndDialog(IDC_MAIN_SENDMAIL);
  }
}

void CPosPrefDlg::OnBnClickedMainSendweb()
{

  MessageBox("Not working yet.");
  //EndDialog(IDC_MAIN_SENDWEB);
}
