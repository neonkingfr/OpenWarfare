/*********************************************************************/
/*																	 */
/*	Poseidon Preferences										     */
/*									Copyright � Mentar, 1999         */
/*																	 */
/*********************************************************************/

// PosPrefDlg.h : header file


/////////////////////////////////////////////////////////////////////////////
// CPosPrefDlg dialog

#include <d3d9.h>
#include <El/QStream/QStream.hpp>
#include "afxwin.h"

class CPosPrefDlg : public CDialog
{
  // Construction
public:
  CPosPrefDlg(CWnd* pParent = NULL);	// standard constructor
  ~CPosPrefDlg();	// standard constructor

  // Dialog Data
  CPosPrefObject		m_Preferences;
  ComRef<IDirect3D9> _d3d;

  AutoArray<SAdapter> m_Adapters;

  QOStrStream _info;


  //{{AFX_DATA(CPosPrefDlg)
  enum { IDD = IDD_POSPREF_DIALOG };
  //}}AFX_DATA

  void ReportInput();
  void ReportAudio();
  void ReportStorage();
  void ReportOS();
  void UpdateAll();

  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CPosPrefDlg)
protected:
  virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
  //}}AFX_VIRTUAL

  // Implementation
protected:
  void ScanD3DAdapter(UINT adapter);
  void ScanD3DAdapters();

  HICON m_hIcon;

  // Generated message map functions
  //{{AFX_MSG(CPosPrefDlg)
  virtual BOOL OnInitDialog();
  afx_msg void OnPaint();
  afx_msg HCURSOR OnQueryDragIcon();
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
public:
  CEdit _infoText;
  CString _result;
  afx_msg void OnBnClickedMainSendmail();
  afx_msg void OnBnClickedMainSendweb();

  QOStream &GetOutput() {return _info;}
};

