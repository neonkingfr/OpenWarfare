# Microsoft Developer Studio Project File - Name="texMerge" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=texMerge - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "texMerge.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "texMerge.mak" CFG="texMerge - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "texMerge - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "texMerge - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Poseidon/texMerge", TCCAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "texMerge - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /G6 /MT /W3 /GR /GX /Zi /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "MFC_NEW" /D _RELEASE=1 /D "_USE_AFX" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 winmm.lib ole32.lib dplay.lib dxguid.lib dinput.lib /nologo /subsystem:windows /debug /machine:I386 /out:"x:\Finalize\texMerge.exe" /force:multiple
# SUBTRACT LINK32 /pdb:none

!ELSEIF  "$(CFG)" == "texMerge - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /G6 /MTd /W3 /GR /GX /ZI /Od /D "MFC_NEW" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_USE_AFX" /FR /YX /FD /c
# SUBTRACT CPP /u
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 winmm.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /force:multiple
# SUBTRACT LINK32 /pdb:none /nodefaultlib

!ENDIF 

# Begin Target

# Name "texMerge - Win32 Release"
# Name "texMerge - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\dummyImport.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# End Source File
# Begin Source File

SOURCE=.\texMerge.cpp
# End Source File
# Begin Source File

SOURCE=.\texMerge.rc
# End Source File
# Begin Source File

SOURCE=.\texMergeDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\texMergeView.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\texMerge.h
# End Source File
# Begin Source File

SOURCE=.\texMergeDoc.h
# End Source File
# Begin Source File

SOURCE=.\texMergeView.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\texMerge.ico
# End Source File
# Begin Source File

SOURCE=.\res\texMerge.rc2
# End Source File
# Begin Source File

SOURCE=.\res\texMergeDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Group "El"

# PROP Default_Filter ""
# Begin Group "QStream"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\QStream\fileCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileMapping.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\qstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\qstreamUseBankDefault.cpp
# End Source File
# End Group
# Begin Group "ParamFile"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\ParamFile\classDbParamFile.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\classDbParamFile.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFile.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFileUseEvalDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFileUseLocalizeDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFileUsePreprocDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamFile\paramFileUseSumCalcDefault.cpp
# End Source File
# End Group
# Begin Group "ParamArchive"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\ParamArchive\paramArchive.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamArchive\paramArchive.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamArchive\paramArchiveDb.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamArchive\paramArchiveDb.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\ParamArchive\paramArchiveUseDbParamFile.cpp
# End Source File
# End Group
# Begin Group "Interfaces"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Interfaces\iClassDb.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\El\elementpch.hpp
# End Source File
# End Group
# Begin Group "Es"

# PROP Default_Filter ""
# Begin Group "Strings"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Strings\rString.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Strings\rString.hpp
# End Source File
# End Group
# Begin Group "Framework"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\useAppFrameDefault.cpp
# End Source File
# End Group
# Begin Group "Memory"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Memory\fastAlloc.cpp
# End Source File
# End Group
# Begin Group "Files"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Files\filenames.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Files\filenames.hpp
# End Source File
# End Group
# Begin Group "Types"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\lLinks.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\lLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\memtype.h
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\scopeLock.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\softLinks.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\softLinks.hpp
# End Source File
# End Group
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\texMerge.reg
# End Source File
# End Target
# End Project
