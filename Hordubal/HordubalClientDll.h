#ifndef _hordubalClientDll_h_
#define _hordubalClientDll_h_

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the HORDUBALCLIENTDLL_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// HORDUBALCLIENTDLL_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef HORDUBALCLIENTDLL_EXPORTS
#define HORDUBALCLIENTDLL_API __declspec(dllexport)
#else
#define HORDUBALCLIENTDLL_API __declspec(dllimport)
#endif

#include "winsock2.h"
#include <Hordubal/inc/message.h>

//! This class provides an interface to control the Hordubal server
class HORDUBALCLIENTDLL_API CHordubalClientDll {
private:
	SOCKET _socketSend;
	char _ip[256];
	bool _connected;
  bool Socket_TryToConnect(char *ip, SOCKET &socketSend);
  void Socket_HandleError(bool send);
  void Socket_SendMessage(SMessage &message);
public:
  //! Constructor with local host ip adress
	CHordubalClientDll();
  //! Constructor
	CHordubalClientDll(const char *ip);
  //! Destructor
  ~CHordubalClientDll();
  //! Determines wheter we are connected
  int IsConnected();
  //! Marks the beginning of the scene
  void BeginScene();
  //! Marks the end of the scene
  void EndScene();
  //! Deletes a model according to a specified id
  void DeleteModel(int id);
  //! Moves a model according to a specified id
  void MoveModel(int id, SMatrix4 origin);
  //! Creates a cube
  void CreateCube(int id, SMatrix4 origin, float size, unsigned int color);
  //! Creates a block
  void CreateBlock(int id, SMatrix4 origin, float sizeX, float sizeY, float sizeZ, unsigned int color);
  //! Creates a string
  void CreateString(int id, SMatrix4 origin, unsigned int color, const char *text);
};

#endif