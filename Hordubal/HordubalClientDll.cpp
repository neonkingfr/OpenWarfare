// HordubalClientDll.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "HordubalClientDll.h"

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}

bool CHordubalClientDll::Socket_TryToConnect(char *ip, SOCKET &socketSend) {
	if (socketSend == INVALID_SOCKET)	socketSend = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (socketSend == INVALID_SOCKET)	return false;

	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(ip);
	addr.sin_port = htons(2500);
	if (connect(socketSend, (sockaddr *)&addr, sizeof(addr)) != SOCKET_ERROR) {
		return true;
	}
	else {
		return false;
	}
}

void CHordubalClientDll::Socket_HandleError(bool send) {
	int error = WSAGetLastError();

	if (send)
		OutputDebugString("Sockets: Connection lost (cannot finish send, error %d)");
	else
		OutputDebugString("Sockets: Connection lost (cannot finish recv, error %d)");

	if (_socketSend != INVALID_SOCKET) {
		closesocket(_socketSend);
		_socketSend = INVALID_SOCKET;
	}
	_connected = false;
}

void CHordubalClientDll::Socket_SendMessage(SMessage &message) {
  // Test connection
  if (!_connected) {
		OutputDebugString("Sockets: Not connected.");
		return;
  }
  // Send message
	fd_set set;
	FD_ZERO(&set);
	FD_SET(_socketSend, &set);
	timeval timeout = {30L, 0L};
	int result = select(FD_SETSIZE, NULL, &set, NULL, &timeout);
	if (result == 0)
	{
		OutputDebugString("Sockets: Timed out during send, message lost.");
		return;
	}
	if (result == SOCKET_ERROR)
	{
		Socket_HandleError(true);
		return;
	}

	result = send(_socketSend, (const char *)&message, sizeof(message), 0);
	if (result == SOCKET_ERROR)
	{
		Socket_HandleError(true);
	}
}

CHordubalClientDll::CHordubalClientDll() { 
  // Sockets initialization
	WSADATA data;
	WSAStartup(0x0101, &data);
	_socketSend = INVALID_SOCKET;
	 strcpy(_ip, "127.0.0.1"); // local host
	_connected = strlen(_ip) > 0 ? Socket_TryToConnect(_ip, _socketSend) : false;
}

CHordubalClientDll::CHordubalClientDll(const char *ip) {
  // Sockets initialization
	WSADATA data;
	WSAStartup(0x0101, &data);
	_socketSend = INVALID_SOCKET;
	 strcpy(_ip, ip);
	_connected = strlen(_ip) > 0 ? Socket_TryToConnect(_ip, _socketSend) : false;
}

CHordubalClientDll::~CHordubalClientDll() {
	if (_socketSend != INVALID_SOCKET) closesocket(_socketSend);
	WSACleanup();
}

int CHordubalClientDll::IsConnected() {
  return _connected;
}

void CHordubalClientDll::BeginScene() {
  SMessage event;
  event._messageType = messageType_BeginScene;
  Socket_SendMessage(event);
}

void CHordubalClientDll::EndScene() {
  SMessage event;
  event._messageType = messageType_EndScene;
  Socket_SendMessage(event);
}

void CHordubalClientDll::DeleteModel(int id) {
  SMessage event;
  event._messageType = messageType_DeleteModel;
  event._deleteModel._id = id;
  Socket_SendMessage(event);
}

void CHordubalClientDll::MoveModel(int id, SMatrix4 origin) {
  SMessage event;
  event._messageType = messageType_MoveModel;
  event._moveModel._id = id;
  event._moveModel._origin = origin;
  Socket_SendMessage(event);
}

void CHordubalClientDll::CreateCube(int id, SMatrix4 origin, float size, unsigned int color) {
  SMessage event;
  event._messageType = messageType_CreateModel;
  event._createModel._id = id;
  event._createModel._origin = origin;
  event._createModel._modelType = modelType_Cube;
  event._createModel._cube._color = color;
  event._createModel._cube._size = size;
  Socket_SendMessage(event);
}

void CHordubalClientDll::CreateBlock(int id, SMatrix4 origin, float sizeX, float sizeY, float sizeZ, unsigned int color) {
  SMessage event;
  event._messageType = messageType_CreateModel;
  event._createModel._id = id;
  event._createModel._origin = origin;
  event._createModel._modelType = modelType_Block;
  event._createModel._block._color = color;
  event._createModel._block._sizeX = sizeX;
  event._createModel._block._sizeY = sizeY;
  event._createModel._block._sizeZ = sizeZ;
  Socket_SendMessage(event);
}

void CHordubalClientDll::CreateString(int id, SMatrix4 origin, unsigned int color, const char *text) {
  SMessage event;
  event._messageType = messageType_CreateModel;
  event._createModel._id = id;
  event._createModel._origin = origin;
  event._createModel._modelType = modelType_String;
  event._createModel._string._color = color;
  strcpy(event._createModel._string._text, text);
  Socket_SendMessage(event);
}
