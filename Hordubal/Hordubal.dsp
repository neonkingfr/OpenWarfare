# Microsoft Developer Studio Project File - Name="Hordubal" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Hordubal - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Hordubal.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Hordubal.mak" CFG="Hordubal - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Hordubal - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Hordubal - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Hordubal", RJMAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Hordubal - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /Zi /O2 /I "w:/C" /I "./inc" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D MFC_NEW=1 /YX /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 dsound.lib dinput8.lib dxerr8.lib d3dx8dt.lib d3d8.lib d3dxof.lib dxguid.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 dsound.lib dinput8.lib dxerr8.lib d3dx8dt.lib d3d8.lib d3dxof.lib dxguid.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /nologo /subsystem:windows /debug /machine:I386 /out:"bin/Hordubal.exe"
# SUBTRACT LINK32 /map

!ELSEIF  "$(CFG)" == "Hordubal - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "w:/C" /I "./inc" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D MFC_NEW=1 /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 dsound.lib dinput8.lib dxerr8.lib d3dx8dt.lib d3d8.lib d3dxof.lib dxguid.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 dsound.lib dinput8.lib dxerr8.lib d3dx8dt.lib d3d8.lib d3dxof.lib dxguid.lib winmm.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /nologo /subsystem:windows /debug /machine:I386 /out:"bin/HordubalD.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Hordubal - Win32 Release"
# Name "Hordubal - Win32 Debug"
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\brick.bmp
# End Source File
# Begin Source File

SOURCE=.\DirectX.ico
# End Source File
# End Group
# Begin Group "Es"

# PROP Default_Filter ""
# Begin Group "Algorithms"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Algorithms\bSearch.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Algorithms\crc32.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Algorithms\crc32.h
# End Source File
# Begin Source File

SOURCE=..\Es\Algorithms\qsort.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Algorithms\splineEqD.hpp
# End Source File
# End Group
# Begin Group "EssenceCommon"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Common\filenames.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\global.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\mathND.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\platform.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Common\win.h
# End Source File
# End Group
# Begin Group "Containers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Containers\array.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\array2D.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\bankArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\bankInitArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\bigArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\boolArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\cachelist.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\list.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\listBidir.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\maps.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\maps.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\quadtree.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\rStringArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\smallArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\sortedArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\staticArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\streamArray.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\typeGenTemplates.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\typeOpts.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Containers\typeTemplates.hpp
# End Source File
# End Group
# Begin Group "Framework"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Framework\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\logflags.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\netlog.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\netlog.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\optDefault.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\optEnable.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Framework\useAppFrameDefault.cpp
# End Source File
# End Group
# Begin Group "Memory"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Memory\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\defNew.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\defNormNew.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\fastAlloc.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Memory\normalNew.hpp
# End Source File
# End Group
# Begin Group "Strings"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Strings\bstring.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Strings\rString.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Strings\rString.hpp
# End Source File
# End Group
# Begin Group "Types"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\Es\Types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\lLinks.cpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\lLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\memtype.h
# End Source File
# Begin Source File

SOURCE=..\Es\Types\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\Types\scopeLock.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\Es\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\Es\platform.hpp
# End Source File
# End Group
# Begin Group "El"

# PROP Default_Filter ""
# Begin Group "ElementCommon"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Common\enumNames.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\globalAlive.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\isaac.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\perfLog.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\randomGen.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\randomGen.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\randomJames.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Common\randomJames.h
# End Source File
# End Group
# Begin Group "Evaluator"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Evaluator\evaluatorExpress.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Evaluator\evaluatorExpress.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Evaluator\evalUseLocalizeStringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Evaluator\express.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Evaluator\express.hpp
# End Source File
# End Group
# Begin Group "ParamFile"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\ParamFile\paramFile.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileCtx.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileCtx.hpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileUseEvalExpress.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileUseLocalizeStringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileUsePreprocC.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamFile\paramFileUseSumCalcDefault.cpp
# End Source File
# End Group
# Begin Group "Pch"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\PCH\afxConfig.h
# End Source File
# Begin Source File

SOURCE=..\El\PCH\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=..\El\PCH\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=..\El\PCH\normalConfig.h
# End Source File
# Begin Source File

SOURCE=..\El\PCH\sReleaseConfig.h
# End Source File
# Begin Source File

SOURCE=..\El\PCH\stdIncludes.h
# End Source File
# End Group
# Begin Group "Stringtable"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Stringtable\localizeStringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Stringtable\localizeStringtable.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Stringtable\stringtable.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Stringtable\stringtable.hpp
# End Source File
# End Group
# Begin Group "Xml"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\XML\xml.cpp
# End Source File
# Begin Source File

SOURCE=..\El\XML\xml.hpp
# End Source File
# End Group
# Begin Group "QStream"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\QStream\fileCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileMapping.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileOverlapped.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\fileOverlapped.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\qbstream.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\qstream.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\serializeBin.cpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=..\El\QStream\ssCompress.cpp
# End Source File
# End Group
# Begin Group "Math"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Math\math3d.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dK.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dK.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dP.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dP.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3DPK.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dT.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\math3dT.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\mathDefs.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\mathOpt.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\mathOpt.hpp
# End Source File
# Begin Source File

SOURCE=..\El\Math\matrix.asm
# End Source File
# Begin Source File

SOURCE=..\El\Math\matrixP3.cpp
# End Source File
# End Group
# Begin Group "Interfaces"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Interfaces\iPreproc.hpp
# End Source File
# End Group
# Begin Group "Modules"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\Modules\modules.cpp
# End Source File
# Begin Source File

SOURCE=..\El\Modules\modules.hpp
# End Source File
# End Group
# Begin Group "PreprocC"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\PreprocC\Preproc.cpp
# End Source File
# Begin Source File

SOURCE=..\El\PreprocC\Preproc.h
# End Source File
# Begin Source File

SOURCE=..\El\PreprocC\preprocC.cpp
# End Source File
# Begin Source File

SOURCE=..\El\PreprocC\preprocC.hpp
# End Source File
# End Group
# Begin Group "ParamArchive"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\El\ParamArchive\paramArchive.cpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamArchive\paramArchive.hpp
# End Source File
# Begin Source File

SOURCE=..\El\ParamArchive\paramArchiveUseDbDefault.cpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\El\elementpch.hpp
# End Source File
# End Group
# Begin Group "Shaders"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\Model.psh
# End Source File
# Begin Source File

SOURCE=.\Model.vsh
# End Source File
# End Group
# Begin Group "inc"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\inc\message.h
# End Source File
# End Group
# Begin Group "src"

# PROP Default_Filter ""
# Begin Group "radiosity"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\radiosity\auxiliary.cpp
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\auxiliary.h
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\faceBuilder.cpp
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\faceBuilder.h
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\halton.h
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\light.cpp
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\light.h
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\lightBlock.cpp
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\lightBlock.h
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\lightFace.cpp
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\lightFace.h
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\lightPatch.cpp
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\lightPatch.h
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\lightSource.cpp
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\lightSource.h
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\radiosity.cpp
# End Source File
# Begin Source File

SOURCE=.\src\radiosity\radiosity.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\src\d3dapp.cpp
# End Source File
# Begin Source File

SOURCE=.\src\d3dapp.h
# End Source File
# Begin Source File

SOURCE=.\src\d3dres.h
# End Source File
# Begin Source File

SOURCE=.\src\d3dutil.cpp
# End Source File
# Begin Source File

SOURCE=.\src\d3dutil.h
# End Source File
# Begin Source File

SOURCE=.\src\dxutil.cpp
# End Source File
# Begin Source File

SOURCE=.\src\dxutil.h
# End Source File
# Begin Source File

SOURCE=.\src\Hordubal.cpp
# End Source File
# Begin Source File

SOURCE=.\src\Hordubal.h
# End Source File
# Begin Source File

SOURCE=.\src\Hordubal.rc
# End Source File
# Begin Source File

SOURCE=.\src\model.cpp
# End Source File
# Begin Source File

SOURCE=.\src\model.h
# End Source File
# Begin Source File

SOURCE=.\src\model3D.cpp
# End Source File
# Begin Source File

SOURCE=.\src\model3D.h
# End Source File
# Begin Source File

SOURCE=.\src\modelArray.cpp
# End Source File
# Begin Source File

SOURCE=.\src\modelArray.h
# End Source File
# Begin Source File

SOURCE=.\src\modelBlock.cpp
# End Source File
# Begin Source File

SOURCE=.\src\modelBlock.h
# End Source File
# Begin Source File

SOURCE=.\src\modelCube.cpp
# End Source File
# Begin Source File

SOURCE=.\src\modelCube.h
# End Source File
# Begin Source File

SOURCE=.\src\modelString.cpp
# End Source File
# Begin Source File

SOURCE=.\src\modelString.h
# End Source File
# Begin Source File

SOURCE=.\src\primitivestream.cpp
# End Source File
# Begin Source File

SOURCE=.\src\primitivestream.h
# End Source File
# Begin Source File

SOURCE=.\src\resource.h
# End Source File
# End Group
# End Target
# End Project
