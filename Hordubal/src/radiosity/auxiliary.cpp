#include <El/elementpch.hpp>
#include "auxiliary.h"

int SphereIntersectsWithFrustum(Vector3Par spherePosition,
                                float sphereRadius,
                                Vector3Par frustumFocus,
                                Vector3Par frustumEndSpherePosition,
                                float frustumEndSphereRadius) {

  // 1-th Step - Test wheter the sphere lies somewhere between
  // the back and the front of the frustum

  // Calculate the distance between frustumFocus and frustumEndSpherePosition
  Vector3 frustumAxis = frustumEndSpherePosition - frustumFocus;
  float frustumAxis2 = frustumAxis.SquareSizeInline();
  if (frustumAxis2 == 0.0f) return 0;
  float invSize = InvSqrt(frustumAxis2);
  float frustumLength = frustumAxis2 * invSize;

  // Calculate the frustum axes vector of size 1
  frustumAxis *= invSize;

  Vector3 p = frustumFocus;
  Vector3 w = frustumAxis;
  Vector3 s = spherePosition;
  float divisor = frustumAxis2;

  // Calculate distance of the plane with normal w containing point s from point p
  float ww = -(
      w.X()*p.X()
    + w.Y()*p.Y()
    + w.Z()*p.Z()
    - s.X()*w.X()
    - s.Y()*w.Y()
    - s.Z()*w.Z()) / divisor;

  // If the sphere donesn't lie between the back and front of the frustum then return 0
  if ((ww <= -sphereRadius) || (ww > (frustumLength + frustumEndSphereRadius + sphereRadius))) return 0;

  // 2-th Step - Test wheter the sphere lies right or left out of the frustum

  // Calculate distance between the sphere position and frustum axis
  float axix_s_distance2 = ((p + w * ww) - s).SquareSizeInline();

  // Calculate distance between the frustum border + sphere radius
  float frustum_and_wrap = frustumEndSphereRadius * ww / frustumLength + sphereRadius;

  // If the sphere lies out of the frustum then return 0
  if (axix_s_distance2 > frustum_and_wrap * frustum_and_wrap) return 0;

  return 1;
}

int SphereIsInShadow(Vector3Par barrierPosition,
                     float barrierRadius,
                     Vector3Par lightPosition,
                     Vector3Par spherePosition,
                     float sphereRadius) {

  // 1-th Step - Test wheter the barrier lies somewhere between
  // the light and the sphere

  // Calculate the distance between light and sphere position
  Vector3 frustumAxis = spherePosition - lightPosition;
  float frustumAxis2 = frustumAxis.SquareSizeInline();
  if (frustumAxis2 == 0.0f) return SIIS_NONE;
  float invSize = InvSqrt(frustumAxis2);
  float frustumLength = frustumAxis2 * invSize;

  // Calculate the frustum axis vector of size 1
  frustumAxis *= invSize;

  Vector3 p = lightPosition;
  Vector3 w = frustumAxis;
  Vector3 s = barrierPosition;
  float divisor = 1.0f/*frustumAxis2*/;

  // Calculate distance of the plane with normal w containing point s from point p
  float ww = -(
      w.X()*p.X()
    + w.Y()*p.Y()
    + w.Z()*p.Z()
    - s.X()*w.X()
    - s.Y()*w.Y()
    - s.Z()*w.Z()) / divisor;

  // If the barrier donesn't lie between the light and sphere then return SIIS_NONE
  if ((ww <= -barrierRadius) || (ww > (frustumLength + sphereRadius + barrierRadius))) return SIIS_NONE;

  // 2-th Step - Test wheter the barrier lies right or left out of the frustum

  // Calculate distance between the barrier position and frustum axis
  float axix_s_distance = ((p + w * ww) - s).Size();

  // Calculate distance between the frustum border and frustum axis
  float frustum_border_distance = sphereRadius * ww / frustumLength;

  // Test the frustum with the barrier
  if (barrierRadius - axix_s_distance > frustum_border_distance) {
    return SIIS_FULL;
  }
  else if (axix_s_distance - barrierRadius > frustum_border_distance) {
    return SIIS_NONE;
  }
  else {
    return SIIS_HALF;
  }
}
