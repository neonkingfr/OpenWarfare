#include <El/elementpch.hpp>
#include "auxiliary.h"
#include "light.h"

void CLight::Reset(Vector3Par changeAreaPosition,
                   float changeAreaRadius) {
  
  // If light is inside change area
  if (_position.Distance(changeAreaPosition) < changeAreaRadius) {
    // Reset all blocks
    for (int i = 0; i < _blocks.Size(); i++) {
      _blocks[i]->Reset();
    }
  }
  else {
    // For each block test the change area with the frustum
    for (int i = 0; i < _blocks.Size(); i++) {
      CLightBlock *pBlock = _blocks[i];
      switch (SphereIsInShadow(changeAreaPosition,
                               changeAreaRadius,
                               _position,
                               pBlock->GetPosition(),
                               pBlock->GetRadius())) {
        case SIIS_FULL:
          pBlock->Reset();
          break;
        case SIIS_HALF:
          pBlock->Reset(changeAreaPosition, changeAreaRadius, _position);
          break;
      }
    }
  }
}

int CLight::GetMostImportantBlockIndex() {
  // Find the most important block
  int miBlockIndex = -1;
  float miValue = -1.0f;
  for (int i = 0; i < _blocks.Size(); i++) {
    float currImportancy = _blocks[i]->GetImportancy();
    if (currImportancy > miValue) {
      miBlockIndex = i;
      miValue = currImportancy;
    }
  }
  return miBlockIndex;
}

CLight::CLight(int id, Vector3Par position, Vector3Par color) {
  _id = id;
  _position = position;
  _color = color;
}

void CLight::AddBlock(int modelId,
                      int nodeId,
                      Matrix4Par origin,
                      Vector3Par dimension) {
  
  // Reset blocks possibly affected by the new shadow
  Reset(origin.Position(), dimension.Size() * 0.5f);

  // Add the block to blocks array
  _blocks.Add(new CLightBlock(modelId, nodeId, origin, dimension));
}

void CLight::RemoveBlock(int modelId, int nodeId) {
  for (int i = 0; i < _blocks.Size(); i++) {
    if (_blocks[i]->Matches(modelId, nodeId)) {
      Reset(_blocks[i]->GetPosition(), _blocks[i]->GetRadius());
      _blocks.Delete(i);
      break;
    }
  }
}

void CLight::MoveBlock(int modelId,
                       int nodeId,
                       Matrix4Par origin) {
  for (int i = 0; i < _blocks.Size(); i++) {
    if (_blocks[i]->Matches(modelId, nodeId)) {
      // Reset old position
      Reset(_blocks[i]->GetPosition(), _blocks[i]->GetRadius());
      // Remember dimension
      Vector3 dimension = _blocks[i]->GetDimension();
      // Delete it
      _blocks.Delete(i);
      // Reset new position
      Reset(origin.Position(), dimension.Size() * 0.5f);
      // Add the block to blocks array
      _blocks.Add(new CLightBlock(modelId, nodeId, origin, dimension));
      break;
    }
  }
}

void CLight::Refine(CModelArray *pScene) {
  
  // Find the most important block
  int miBlockIndex = GetMostImportantBlockIndex();

  // Split it
  if (miBlockIndex > -1) {
    _blocks[miBlockIndex]->Refine(pScene, _position);
  }
}

void CLight::GetGeometry(CPrimitiveStream &ps) {
  for (int i = 0; i < _blocks.Size(); i++) {
    _blocks[i]->GetGeometry(ps);
  }
}

int CLight::GetId() {
  return _id;
}

float CLight::GetImportancy() {
  int miBlockIndex = GetMostImportantBlockIndex();
  if (miBlockIndex > -1) {
    return _blocks[GetMostImportantBlockIndex()]->GetImportancy();
  }
  else {
    return 0.0f;
  }
}

void CLight::Resort(Vector3Par eyePosition) {
  for (int i = 0; i < _blocks.Size(); i++) {
    ///_blocks[i]->Resort(eyePosition);
  }
}

void CLight::AddModelFaceGeometry(CFaceBuilder &fb, int modelId, int faceNumber) {
  for (int i = 0; i < _blocks.Size(); i++) {
    if (_blocks[i]->Matches(modelId, 0)) {
      _blocks[i]->AddFaceGeometry(fb, faceNumber, _position, _color);
      break;
    }
  }
}

void CLight::GetModelFaceProperties(int modelId,
                                    int faceNumber,
                                    Vector3 &pos,
                                    Vector3 &u,
                                    Vector3 &v) {

  for (int i = 0; i < _blocks.Size(); i++) {
    if (_blocks[i]->Matches(modelId, 0)) {
      _blocks[i]->GetFaceProperties(faceNumber, pos, u, v);
      break;
    }
  }
}
