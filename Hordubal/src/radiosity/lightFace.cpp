#include <El/elementpch.hpp>
#include "../modelArray.h"
#include "auxiliary.h"
#include "faceBuilder.h"
#include "lightFace.h"

#define MIN_IMPORTANCY 0.4f

void CLightFace::GetPatchGeometry(CPrimitiveStream &ps, CLightPatch *pPatch) {
  Ref<CLightPatchQuad> children = pPatch->GetChildren();
  if (children.IsNull()) {
    pPatch->GetGeometry(ps, _pos, _u, _v);
  }
  else {
    GetPatchGeometry(ps, &children->_patches[0]);
    GetPatchGeometry(ps, &children->_patches[1]);
    GetPatchGeometry(ps, &children->_patches[2]);
    GetPatchGeometry(ps, &children->_patches[3]);
  }
}

void CLightFace::InsertPatchToQueue(CLightPatch *pPatch) {

  // Get the most important patch in queue
  CLightPatch *pCurrPatch = _patchList.Start();

  // Get importancy of the pPatch
  float patchImportancy = pPatch->GetImportancy();

  // Find first less important patch
  float CurrImportancy;
  while ((_patchList.NotEnd(pCurrPatch)) && 
         ((CurrImportancy = pCurrPatch->GetImportancy()) > patchImportancy) &&
         (CurrImportancy > MIN_IMPORTANCY)) {
    pCurrPatch = _patchList.Advance(pCurrPatch);
  }

  // Add the child patch before the current patch
  _patchList.InsertBefore(pCurrPatch, pPatch);

}

void CLightFace::ResetPatch(CLightPatch *pPatch,
                            Vector3Par changeAreaPosition,
                            float changeAreaRadius,
                            Vector3Par lightPosition) {
  
  Ref<CLightPatchQuad> children;

  switch (SphereIsInShadow(changeAreaPosition,
                           changeAreaRadius,
                           lightPosition,
                           pPatch->GetWorldPosition(_pos, _u, _v),
                           pPatch->GetRadius(_u.Size(), _v.Size()))) {
  case SIIS_FULL:
    pPatch->Reset();
    //Assert(pPatch != &_headPatch);
    if (pPatch->IsInList()) pPatch->Delete();
    InsertPatchToQueue(pPatch);
    break;
  case SIIS_HALF:
    children = pPatch->GetChildren();
    if (children.NotNull()) {
      for (int i = 0; i < 4; i++) {
        ResetPatch(&children->_patches[i], changeAreaPosition, changeAreaRadius, lightPosition);
      }
    }
    else {
      pPatch->Reset();
      //Assert(pPatch != &_headPatch);
      if (pPatch->IsInList()) pPatch->Delete();
      InsertPatchToQueue(pPatch);
    }
    break;
  }
}

void CLightFace::AddPatchToFaceBuilder(CFaceBuilder &fb,
                                       int fbIndex,
                                       CLightPatch *pPatch,
                                       Vector3Par lightPosition,
                                       Vector3Par lightColor) {

  Ref<CLightPatchQuad> children = pPatch->GetChildren();
  if (children.IsNull() || (!FB_HAS_CHILD(fbIndex))) {
    Vector3 c0, c1, c2, c3;
    pPatch->GetCorners(_pos, _u, _v, c0, c1, c2, c3);
    float dp0 = _normal.DotProduct((lightPosition - c0).Normalized());
    float dp1 = _normal.DotProduct((lightPosition - c1).Normalized());
    float dp2 = _normal.DotProduct((lightPosition - c2).Normalized());
    float dp3 = _normal.DotProduct((lightPosition - c3).Normalized());
    saturateMax(dp0, 0.0f);
    saturateMax(dp1, 0.0f);
    saturateMax(dp2, 0.0f);
    saturateMax(dp3, 0.0f);
    int es0, es1, es2, es3;
    pPatch->GetEdgeShadow(es0, es1, es2, es3);
    Vector3 color0 = lightColor * dp0 * ((float)(1 - es0));
    Vector3 color1 = lightColor * dp1 * ((float)(1 - es1));
    Vector3 color2 = lightColor * dp2 * ((float)(1 - es2));
    Vector3 color3 = lightColor * dp3 * ((float)(1 - es3));
    fb.AddColors(fbIndex, color0, color1, color2, color3);
  }
  else {
    int newFBIndex = fb.ForceSplit(fbIndex);
    AddPatchToFaceBuilder(fb, newFBIndex + 0, &children->_patches[0], lightPosition, lightColor);
    AddPatchToFaceBuilder(fb, newFBIndex + 1, &children->_patches[1], lightPosition, lightColor);
    AddPatchToFaceBuilder(fb, newFBIndex + 2, &children->_patches[2], lightPosition, lightColor);
    AddPatchToFaceBuilder(fb, newFBIndex + 3, &children->_patches[3], lightPosition, lightColor);
  }
}

void CLightFace::Init(Vector3Par pos, Vector3Par u, Vector3Par v, Vector3Par normal) {
  _pos = pos - u * 0.5f - v * 0.5f;
  _u = u;
  _v = v;
  _uSize = _u.Size();
  _vSize = _v.Size();
  _rootPatch.Init(0.5f, 0.5f, 1.0f, max(_uSize, _vSize));
  _normal = normal;
  _patchList.Insert(&_rootPatch);
}

void CLightFace::Done() {
  Delete();
}

void CLightFace::Refine(CModelArray *pScene,
                        Vector3Par lightPosition) {

  // Get the most important patch
  CLightPatch *pPatch = _patchList.Start();
  //Assert(pPatch != &_headPatch);

  Vector3 c0, c1, c2, c3;

  // If the patch is already set, then split it, else set it
  if (pPatch->IsSet()) {
    // Check the importancy
    //if (pPatch->GetImportancy(_pos, _u, _v, eyePosition) > MIN_IMPORTANCY) {
    //if (pPatch->GetImportancy(_pos, _u, _v, eyePosition) > (1.0f / 16.0f)) {
    if (pPatch->GetImportancy() > MIN_IMPORTANCY) {
      pPatch->GetCorners(_pos, _u, _v, c0, c1, c2, c3);
      if (pPatch->IsInList()) pPatch->Delete();

      // Get the w vectors
      Vector3 w0 = ((c0 + c3) * 0.5f - lightPosition) * 0.99f;
      Vector3 w1 = ((c0 + c1) * 0.5f - lightPosition) * 0.99f;
      Vector3 w2 = ((c1 + c2) * 0.5f - lightPosition) * 0.99f;
      Vector3 w3 = ((c2 + c3) * 0.5f - lightPosition) * 0.99f;
      Vector3 w4 = ((c0 + c2) * 0.5f - lightPosition) * 0.99f;

      // Split the patch into 4 children  
      Ref<CLightPatchQuad> _pChildern = pPatch->Split(
        pScene->IntersectsWithAbscissa(lightPosition, w0),
        pScene->IntersectsWithAbscissa(lightPosition, w1),
        pScene->IntersectsWithAbscissa(lightPosition, w2),
        pScene->IntersectsWithAbscissa(lightPosition, w3),
        pScene->IntersectsWithAbscissa(lightPosition, w4));

      // Add children to queue according to their importancy
      for (int j = 0; j < 4; j++) {
        InsertPatchToQueue(&_pChildern->_patches[j]);
      }
    }
  }
  else {
    pPatch->GetCorners(_pos, _u, _v, c0, c1, c2, c3);
    if (pPatch->IsInList()) pPatch->Delete();

    // Get the w vectors
    Vector3 w0 = (c0 - lightPosition) * 0.99f;
    Vector3 w1 = (c1 - lightPosition) * 0.99f;
    Vector3 w2 = (c2 - lightPosition) * 0.99f;
    Vector3 w3 = (c3 - lightPosition) * 0.99f;

    // Set shadow values
    pPatch->SetEdgeShadowValues(
      pScene->IntersectsWithAbscissa(lightPosition, w0),
      pScene->IntersectsWithAbscissa(lightPosition, w1),
      pScene->IntersectsWithAbscissa(lightPosition, w2),
      pScene->IntersectsWithAbscissa(lightPosition, w3));

    // Add the patch to queue
    InsertPatchToQueue(pPatch);
  }
}

void CLightFace::Reset() {
  _rootPatch.Reset();
  if (_rootPatch.IsInList()) _rootPatch.Delete();
  InsertPatchToQueue(&_rootPatch);
}

void CLightFace::Reset(Vector3Par changeAreaPosition,
                       float changeAreaRadius,
                       Vector3Par lightPosition) {
  ResetPatch(&_rootPatch, changeAreaPosition, changeAreaRadius, lightPosition);
}

float CLightFace::GetImportancy() {
  return _patchList.Start()->GetImportancy();
}

void CLightFace::GetGeometry(CPrimitiveStream &ps) {
  GetPatchGeometry(ps, &_rootPatch);
}

void CLightFace::AddToFaceBuilder(CFaceBuilder &fb,
                                  Vector3Par lightPosition,
                                  Vector3Par lightColor) {
  AddPatchToFaceBuilder(fb, 0, &_rootPatch, lightPosition, lightColor);
}

void CLightFace::GetProperties(Vector3 &pos, Vector3 &u, Vector3 &v) {
  pos = _pos;
  u = _u;
  v = _v;
}