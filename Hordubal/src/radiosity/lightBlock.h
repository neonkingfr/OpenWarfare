#ifndef _lightBlock_h_
#define _lightBlock_h_

#include <Es/Types/pointers.hpp>
#include "lightFace.h"

class CModelArray;
class CPrimitiveStream;

/*!
  id's, origin and dimension can be shared among the same blocks
  in different lights
*/
class CLightBlock : public RefCount {
private:
  //! Id of the model the block belongs to
  int _modelId;
  //! Id of the node in the model
  int _nodeId;
  //! Origin
  Matrix4 _origin;
  //! Dimensions
  Vector3 _dimension;
  //! One face for each side of the block (Order is X,-X,Y,-Y,Z,-Z)
  CLightFace _faces[6];
  //! List of 6 faces
  TListBidir<CLightFace> _faceList;
  //! Inserts the face to the queue according to its importancy
  void InsertFaceToQueue(CLightFace *pFace);
  //! Resorts faces in the queue according their importancy
  void ResortFacesInQueue();
public:
  //! Constructor
  CLightBlock(
    int modelId,
    int nodeId,
    Matrix4Par origin,
    Vector3Par dimension);
  ~CLightBlock();
  //! Returns position of the block
  Vector3 GetPosition();
  //! Returns radius of the block
  float GetRadius();
  //! Resets all faces*
  void Reset();
  //! Resets faces according to specified change area*
  void Reset(
    Vector3Par changeAreaPosition,
    float changeAreaRadius,
    Vector3Par lightPosition);
  //! Refines the solution*
  void Refine(
    CModelArray *pScene,
    Vector3Par lightPosition);
  //! Returns importancy of the block
  float GetImportancy();
  //! Fills out the geometry of the block
  void GetGeometry(CPrimitiveStream &ps);
  //! Determines wheter block matches specified ID's
  int Matches(int modelId, int nodeId);
  //! Returns dimension of the block
  Vector3 GetDimension();
  //! Add the geometry of the face
  void AddFaceGeometry(
    CFaceBuilder &fb,
    int faceNumber,
    Vector3Par lightPosition,
    Vector3Par lightColor);
  void GetFaceProperties(
    int faceNumber,
    Vector3 &pos,
    Vector3 &u,
    Vector3 &v);
};
template Ref<CLightBlock>;

#endif