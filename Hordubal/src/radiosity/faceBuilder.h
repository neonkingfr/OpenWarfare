#ifndef _faceBuilder_h_
#define _faceBuilder_h_

#include <El/Math/Math3d.hpp>

struct SFBPatch {
  float _posU;
  float _posV;
  float _sizeU;
  float _sizeV;
  Vector3 _edgeColors[4];
  int _isLeaf;
};

//! Number of nodes in a whole tree
#define FB_TREE_SIZE (65536+16384+4096+1024+256+64+16+4+1)
//! Indices smaller than HAS_CHILD_TRESHOLD can have a child
#define FB_HAS_CHILD_TRESHOLD (16384+4096+1024+256+64+16+4+1)
//! Determines wheter specified index can have a child
#define FB_HAS_CHILD(index) ((index<FB_HAS_CHILD_TRESHOLD)?1:0)

class CPrimitiveStream;

//! Stores light informations of one face for all involved lights
/*!
  Note that valid values are stored only in leafs.
*/
class CFaceBuilder : public RefCount {
private:
  //! Root of the tree
  SFBPatch _root[FB_TREE_SIZE];
  //! Recursive procedure initializes a subtree pos and size values
  void InitSubtree(
    int index,
    float posU,
    float posV,
    float sizeU,
    float sizeV);
  //! Recursive procedure returnes geometry of all patches
  void GetPatchGeometry(CPrimitiveStream &ps, int index, Vector3Par pos, Vector3Par u, Vector3Par v);
public:
  //! Constructor
  CFaceBuilder();
  //! Resets face builder to be prepared to gether information of new face
  void Reset();
  //! Splits specified patch into 4 new and sets their interpolated values
  int ForceSplit(int index);
  //! Add colors to the leafs of the subtree specified by index
  void AddColors(
    int index,
    Vector3Par c0,
    Vector3Par c1,
    Vector3Par c2,
    Vector3Par c3);
  //! Returns geometry of this face
  void GetGeometry(CPrimitiveStream &ps, Vector3Par pos, Vector3Par u, Vector3Par v);
};
template Ref<CFaceBuilder>;

#endif
