#ifndef _light_h_
#define _light_h_

#include <El/Math/math3d.hpp>
#include <Es/Containers/array.hpp>
#include "lightBlock.h"

class CModelArray;
class CPrimitiveStream;

class CLight : public RefCount {
private:
  //! Id of the light
  int _id;
  //! Position of the light in space
  Vector3 _position;
  //! Color of the light
  Vector3 _color;
  //! Blocks (models or models parts) enlighted by the light
  RefArray<CLightBlock> _blocks;
  //! Resets block affected by specified change area and light position
  void Reset(
    Vector3Par changeAreaPosition,
    float changeAreaRadius);
  //! Returns index of the most important block
  int GetMostImportantBlockIndex();
public:
  //! Constructor
  CLight(int id, Vector3Par position, Vector3Par color);
  //! Add a block
  void AddBlock(
    int modelId,
    int nodeId,
    Matrix4Par origin,
    Vector3Par dimension);
  //! Removes the specified block
  void RemoveBlock(int modelId, int nodeId);
  //! Moves the specified block to specified origin
  void MoveBlock(
    int modelId,
    int nodeId,
    Matrix4Par origin);
  //! Refines the solution
  void Refine(CModelArray *pScene);
  //! Fills out the geometry of the block
  void GetGeometry(CPrimitiveStream &ps);
  //! Returns id of the light
  int GetId();
  //! Returns importancy of the light
  float GetImportancy();
  //! Resorts patches
  void Resort(Vector3Par eyePosition);
  void AddModelFaceGeometry(CFaceBuilder &fb, int modelId, int faceNumber);
  void GetModelFaceProperties(
    int modelId,
    int faceNumber,
    Vector3 &pos,
    Vector3 &u,
    Vector3 &v);
};
template Ref<CLight>;


#endif