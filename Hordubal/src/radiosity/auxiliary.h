#ifndef _auxiliary_h_
#define _auxiliary_h_

#include <El/Math/math3d.hpp>

//! Determines wheter specified spehere collides with specified frustum
int SphereIntersectsWithFrustum(
      Vector3Par spherePosition,
      float sphereRadius,
      Vector3Par frustumFocus,
      Vector3Par frustumEndSpherePosition,
      float frustumEndSphereRadius);

#define SIIS_NONE 0
#define SIIS_FULL 1
#define SIIS_HALF 2
//! Determines wheter specified barrier could cast a shadow on specified sphere
/*!
  Note that current implementation is not accurate especially for
  degenerated frustums (light is too close to the sphere)
*/
int SphereIsInShadow(
      Vector3Par barrierPosition,
      float barrierRadius,
      Vector3Par lightPosition,
      Vector3Par spherePosition,
      float sphereRadius);

#endif