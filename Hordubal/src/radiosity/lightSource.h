#ifndef _lightSource_h_
#define _lightSource_h_

#include <El/Math/Math3d.hpp>
#include "halton.h"

class CLightSource {
public:
  virtual void GetNextLigthRay(Vector3 &position, Vector3 &direction);
};

class CLSPointHalfSpace : public CLightSource {
private:
public:
};

#endif