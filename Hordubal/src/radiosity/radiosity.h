#ifndef _radiosity_h_
#define _radiosity_h_

#include <El/Math/math3d.hpp>
#include "faceBuilder.h"
#include "light.h"


//! Model node structure
struct SModelNode : public RefCount {
  //! Id of the model node
  int _id;
  //! Origin of the node relative to its ancestor
  Matrix4 _origin;
  //! Dimensions of the block
  Vector3 _dimension; 
  //! Array of children
  RefArray<SModelNode> _children;
};
template Ref<SModelNode>;

//! Model structure
struct SModel : public RefCount {
  //! Id of the model
  int _id;
  //! Model node
  SModelNode _node;
};
template Ref<SModel>;

//! Light structure
struct SLight : public RefCount {
  //! Id of the light
  int _id;
  //! Position of the light
  Vector3 _position;
  //! RGB values of the color of light (all values are from interval <0, 1>)
  Vector3 _color;
};
template Ref<SLight>;

struct SBlock : public RefCount {
  //! Id of the model the block belongs to
  int _modelId;
  //! Id of the node in the model
  int _nodeId;
  //! Origin of the block
  Matrix4 _origin;
  //! Dimensions of the block
  Vector3 _dimension;
};
template Ref<SBlock>;

class CModelArray;
class CPrimitiveStream;

//! Interface to radiosity solution
class CRadiosity {
private:
  //! Origin of the eye - used for importancy counting purposes
  Matrix4 _eyeOrigin;
  //! Array of input models
  RefArray<SModel> _inputModels;
  //! Array of input lights
  RefArray<SLight> _inputLights;

  //! Blocks derived from models
  RefArray<SBlock> _radiosityBlocks;
  //! Lights derived from input lights including secondary lights
  RefArray<CLight> _radiosityLights;

  //! Builder of the faces
  Ref<CFaceBuilder> _faceBuilder;

  void ModelNodeAdd(int id, SModelNode *pModelNode);
  
  int GetMostImportantLightIndex();

  void GetModelFaceGeometry(int modelId, int faceNumber);
  void GetModelFaceProperties(
    int modelId,
    int faceNumber,
    Vector3 &pos,
    Vector3 &u,
    Vector3 &v);

public:
  //! Constructor
  CRadiosity();

  //! Sets new origin of the eye and resorts patches according to the new origin
  void SetOrigin(Matrix4Par eyeOrigin);
  void ModelAdd(SModel *pModel);
  void ModelRemove(int id);
  void ModelMove(int id, Matrix4Par origin);
  void LightAdd(SLight *pLight);
  //void LightMove();
  void LightRemove(int id);

  //! Performs specified number of steps of refinement
  /*!
    \param eyeOrigin Origin of the eye. We use it for importancy counting
    \param steps Number of steps we are able to do
  */
  void Refine(CModelArray *pScene);
  //! Fills out the geometry
  void GetGeometry(CPrimitiveStream &ps);


  //void GetDiffMap(int modelId);
  //void GetLights(int modelId);
};

#endif