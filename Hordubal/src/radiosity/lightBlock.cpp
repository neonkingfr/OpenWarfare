#include <El/elementpch.hpp>
#include "lightBlock.h"

void CLightBlock::InsertFaceToQueue(CLightFace *pFace) {
  // Get the most important face in queue
  CLightFace *pCurrFace = _faceList.Start();

  // Get importancy of the pFace
  float faceImportancy = pFace->GetImportancy();

  // Find first less important patch
  while ((_faceList.NotEnd(pCurrFace)) && 
         ((pCurrFace->GetImportancy()) > faceImportancy)) {
    pCurrFace = _faceList.Advance(pCurrFace);
  }

  // Add the child patch before the current patch
  _faceList.InsertBefore(pCurrFace, pFace);
}

void CLightBlock::ResortFacesInQueue() {
  int change;
  do {
    change = 0;
    CLightFace *pCurrFace = _faceList.Start();
    CLightFace *pNextFace;
    while (_faceList.NotEnd(pNextFace = _faceList.Advance(pCurrFace))) {
      if (pCurrFace->GetImportancy() < pNextFace->GetImportancy()) {
        pCurrFace->SwapWithNext();
        change = 1;
      }
      else {
        pCurrFace = pNextFace;
      }
    }
  } while (change);
}

CLightBlock::CLightBlock(int modelId,
                         int nodeId,
                         Matrix4Par origin,
                         Vector3Par dimension) {
  _modelId = modelId;
  _nodeId = nodeId;
  _origin = origin;
  _dimension = dimension;

  // Init faces
  float halfSizeX = dimension.X() * 0.5f;
  float halfSizeY = dimension.Y() * 0.5f;
  float halfSizeZ = dimension.Z() * 0.5f;

  // X
  _faces[0].Init(
    origin.Position() + origin.DirectionAside() * halfSizeX,
    origin.Direction() * dimension.Z(),
    origin.DirectionUp() * dimension.Y(),
    origin.DirectionAside());

  // -X
  _faces[1].Init(
    origin.Position() - origin.DirectionAside() * halfSizeX,
    -origin.Direction() * dimension.Z(),
    origin.DirectionUp() * dimension.Y(),
    -origin.DirectionAside());
  
  // Y
  _faces[2].Init(
    origin.Position() + origin.DirectionUp() * halfSizeY,
    origin.DirectionAside() * dimension.X(),
    origin.Direction() * dimension.Z(),
    origin.DirectionUp());

  // -Y
  _faces[3].Init(
    origin.Position() - origin.DirectionUp() * halfSizeY,
    -origin.DirectionAside() * dimension.X(),
    origin.Direction() * dimension.Z(),
    -origin.DirectionUp());

  // Z
  _faces[4].Init(
    origin.Position() + origin.Direction() * halfSizeZ,
    origin.DirectionUp() * dimension.Y(),
    origin.DirectionAside() * dimension.X(),
    origin.Direction());

  // -Z
  _faces[5].Init(
    origin.Position() - origin.Direction() * halfSizeZ,
    -origin.DirectionUp() * dimension.Y(),
    origin.DirectionAside() * dimension.X(),
    -origin.Direction());

  // Add faces to queue
  for (int i = 0; i < 6; i++) {
    _faceList.Insert(&_faces[i]);
  }
}

CLightBlock::~CLightBlock() {
  for (int i = 0; i < 6; i++) {
    _faces[i].Done();
  }
}

Vector3 CLightBlock::GetPosition() {
  return _origin.Position();
}

float CLightBlock::GetRadius() {
  return _dimension.Size() * 0.5f;
}

void CLightBlock::Reset() {
  for (int i = 0; i < 6; i++) {
    _faces[i].Reset();
  }
  ResortFacesInQueue();
}

void CLightBlock::Reset(Vector3Par changeAreaPosition,
                        float changeAreaRadius,
                        Vector3Par lightPosition) {
  for (int i = 0; i < 6; i++) {
    _faces[i].Reset(changeAreaPosition, changeAreaRadius, lightPosition);
  }
  ResortFacesInQueue();
}

void CLightBlock::Refine(CModelArray *pScene,
                         Vector3Par lightPosition) {

  // Get the most important face
  CLightFace *pFace = _faceList.Start();

  // Refine it
  Assert(_faceList.NotEnd(pFace));
  pFace->Refine(pScene, lightPosition);
  pFace->Delete();
  InsertFaceToQueue(pFace);
}

float CLightBlock::GetImportancy() {

  // Get the most important face
  CLightFace *pFace = _faceList.Start();

  // Get the importancy
  Assert(_faceList.NotEnd(pFace));
  return pFace->GetImportancy();
}

void CLightBlock::GetGeometry(CPrimitiveStream &ps) {
  for (int i = 0; i < 6; i++) {
    _faces[i].GetGeometry(ps);
  }
}

int CLightBlock::Matches(int modelId, int nodeId) {
  if ((_modelId == modelId) && (_nodeId == nodeId))
    return 1;
  else
    return 0;
}

Vector3 CLightBlock::GetDimension() {
  return _dimension;
}

void CLightBlock::AddFaceGeometry(CFaceBuilder &fb,
                                  int faceNumber,
                                  Vector3Par lightPosition,
                                  Vector3Par lightColor) {
  _faces[faceNumber].AddToFaceBuilder(fb, lightPosition, lightColor);
}

void CLightBlock::GetFaceProperties(int faceNumber,
                                    Vector3 &pos,
                                    Vector3 &u,
                                    Vector3 &v) {
  _faces[faceNumber].GetProperties(pos, u, v);
}