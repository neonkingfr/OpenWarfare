#ifndef _lightFace_h_
#define _lightFace_h_

#include <Es/Containers/listBidir.hpp>
#include <El/Math/Math3d.hpp>
#include "lightPatch.h"

class CModelArray;
class CPrimitiveStream;
class CFaceBuilder;

//! This class represents one face of the radiosity cube
class CLightFace : public TLinkBidir<> {
private:
  //! Root patch
  CLightPatch _rootPatch;
  //! Link list of patches
  TListBidir<CLightPatch> _patchList;
  //! Position of the reference point
  Vector3 _pos;
  //! U vector, length corresponds to the width
  Vector3 _u;
  //! V vector, length corresponds to the height
  Vector3 _v;
  //! Size of U vector
  float _uSize;
  //! Size of V vector
  float _vSize;
  //! Normal of the face
  Vector3 _normal;
  //! Recursive procedure builds a geometry
  void GetPatchGeometry(CPrimitiveStream &ps, CLightPatch *pPatch);
  //! Inserts the patch to the queue according to its importancy
  void InsertPatchToQueue(CLightPatch *pPatch);
  //! Recursive procedure resets a patch
  void ResetPatch(
    CLightPatch *pPatch,
    Vector3Par changeAreaPosition,
    float changeAreaRadius,
    Vector3Par lightPosition);
  //! Recursive procedure add a patch to the specified face builder
  void AddPatchToFaceBuilder(
    CFaceBuilder &fb,
    int fbIndex,
    CLightPatch *pPatch,
    Vector3Par lightPosition,
    Vector3Par lightColor);
public:
  //! Initialization
  void Init(Vector3Par pos, Vector3Par u, Vector3Par v, Vector3Par normal);
  void Done();
  //! Refines the solution
  void Refine(CModelArray *pScene, Vector3Par lightPosition);
  //! Resets all patches
  void Reset();
  //! Resets patches according to a specified change area and ligth
  void Reset(
    Vector3Par changeAreaPosition,
    float changeAreaRadius,
    Vector3Par lightPosition);
  //! Returns importancy of this face
  float GetImportancy();
  //! Returns geometry of the face
  void GetGeometry(CPrimitiveStream &ps);
  //! Add this face to face builder
  void AddToFaceBuilder(
    CFaceBuilder &fb,
    Vector3Par lightPosition,
    Vector3Par lightColor);
  void GetProperties(Vector3 &pos, Vector3 &u, Vector3 &v);
};

#endif