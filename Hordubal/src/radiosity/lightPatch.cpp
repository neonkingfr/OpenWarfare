#include <El/elementpch.hpp>
#include "../model.h"
#include "lightPatch.h"

void CLightPatch::Init(float posU,
                       float posV,
                       float size,
                       float maxAbsSize) {
  _posU = posU;
  _posV = posV;
  _size = size;
  _edgeShadow[0] = 0;
  _edgeShadow[1] = 0;
  _edgeShadow[2] = 0;
  _edgeShadow[3] = 0;
  _isSet = 0;
  _maxAbsSize = maxAbsSize;
}

void CLightPatch::Done() {
  if (_pChildern.NotNull()) {
    _pChildern->_patches[0].Done();
    _pChildern->_patches[1].Done();
    _pChildern->_patches[2].Done();
    _pChildern->_patches[3].Done();
  }
  if (IsInList()) Delete();
}

void CLightPatch::SetEdgeShadowValues(int es0,
                                      int es1,
                                      int es2,
                                      int es3) {
  _edgeShadow[0] = es0;
  _edgeShadow[1] = es1;
  _edgeShadow[2] = es2;
  _edgeShadow[3] = es3;
  _isSet = 1;
}

Ref<CLightPatchQuad> CLightPatch::Split(int ns0,
                                        int ns1,
                                        int ns2,
                                        int ns3,
                                        int ns4) {

  // Create new quad of childern
  _pChildern = new CLightPatchQuad();

  // Precalculate values
  float newSize = _size * 0.5f;
  float halfNewSize = newSize * 0.5f;
  float newMaxAbsSize = _maxAbsSize * 0.5f;

  // Set childern parameters

  // 0
  _pChildern->_patches[0].Init(
    _posU + halfNewSize, _posV + halfNewSize, newSize, newMaxAbsSize);
  _pChildern->_patches[0].SetEdgeShadowValues(
    _edgeShadow[0], ns1, ns4, ns0);

  // 1
  _pChildern->_patches[1].Init(
    _posU + halfNewSize, _posV - halfNewSize, newSize, newMaxAbsSize);
  _pChildern->_patches[1].SetEdgeShadowValues(
    ns1, _edgeShadow[1], ns2, ns4);

  // 2
  _pChildern->_patches[2].Init(
    _posU - halfNewSize, _posV - halfNewSize, newSize, newMaxAbsSize);
  _pChildern->_patches[2].SetEdgeShadowValues(
    ns4, ns2, _edgeShadow[2], ns3);

  // 3
  _pChildern->_patches[3].Init(
    _posU - halfNewSize, _posV + halfNewSize, newSize, newMaxAbsSize);
  _pChildern->_patches[3].SetEdgeShadowValues(
    ns0, ns4, ns3, _edgeShadow[3]);

  return _pChildern;
}

void CLightPatch::Reset() {
/*
  _edgeShadow[0] = 0;
  _edgeShadow[1] = 0;
  _edgeShadow[2] = 0;
  _edgeShadow[3] = 0;
*/
  _isSet = 0;
  if (_pChildern.NotNull()) {
    _pChildern->_patches[0].Done();
    _pChildern->_patches[1].Done();
    _pChildern->_patches[2].Done();
    _pChildern->_patches[3].Done();
    _pChildern.Free();
  }
}

Vector3 CLightPatch::GetWorldPosition(Vector3Par pos, Vector3Par u, Vector3Par v) {
  return pos + u * _posU + v * _posV;
}

float CLightPatch::GetRadius(float uCoef, float vCoef) {
  float u = _size * uCoef;
  float v = _size * vCoef;
  return sqrt(u*u + v*v) * 0.5f;
}

float CLightPatch::GetImportancy() {
  if (_isSet) {
    return _maxAbsSize;
  }
  else {
    return _maxAbsSize + 10.0f;
  }
}

void CLightPatch::GetGeometry(CPrimitiveStream &ps,
                              Vector3Par pos,
                              Vector3Par u,
                              Vector3Par v) {
  

  //Vector3 pos3d = pos + u * _posU * 0.5f + v * _posV * 0.5f;
  Vector3 pos3d = pos + u * _posU + v * _posV;
  Vector3 normal = v.CrossProduct(u).Normalized();

  SModelVertex mv;
  WORD VIndex;

  VIndex = ps.GetNewVertexIndex();



  

  //mv._position = pos3d + u * 0.5f * _sizeU * 0.5f + v * 0.5f * _sizeV * 0.5f;
  mv._position = pos3d + u * _size * 0.5f + v * _size * 0.5f;
  mv._normal = normal;
  if (_edgeShadow[0]) {
    mv._diffuse = D3DCOLOR_ARGB(255,0,0,0);
  }
  else {
    mv._diffuse = D3DCOLOR_ARGB(255,255,255,255);
  }
  mv._u0 = _posU + _size * 0.5f;
  mv._v0 = _posV + _size * 0.5f;
  ps.AddVertex(&mv);

  //mv._position = pos3d + u * 0.5f * _sizeU * 0.5f - v * 0.5f * _sizeV * 0.5f;
  mv._position = pos3d + u * _size * 0.5f - v * _size * 0.5f;
  mv._normal = normal;
  if (_edgeShadow[1]) {
    mv._diffuse = D3DCOLOR_ARGB(255,0,0,0);
  }
  else {
    mv._diffuse = D3DCOLOR_ARGB(255,255,255,255);
  }
  mv._u0 = _posU + _size * 0.5f;
  mv._v0 = _posV - _size * 0.5f;
  ps.AddVertex(&mv);

  //mv._position = pos3d - u * 0.5f * _sizeU * 0.5f - v * 0.5f * _sizeV * 0.5f;
  mv._position = pos3d - u * _size * 0.5f - v * _size * 0.5f;
  mv._normal = normal;
  if (_edgeShadow[2]) {
    mv._diffuse = D3DCOLOR_ARGB(255,0,0,0);
  }
  else {
    mv._diffuse = D3DCOLOR_ARGB(255,255,255,255);
  }
  mv._u0 = _posU - _size * 0.5f;
  mv._v0 = _posV - _size * 0.5f;
  ps.AddVertex(&mv);

  //mv._position = pos3d - u * 0.5f * _sizeU * 0.5f + v * 0.5f * _sizeV * 0.5f;
  mv._position = pos3d - u * _size * 0.5f + v * _size * 0.5f;
  mv._normal = normal;
  if (_edgeShadow[3]) {
    mv._diffuse = D3DCOLOR_ARGB(255,0,0,0);
  }
  else {
    mv._diffuse = D3DCOLOR_ARGB(255,255,255,255);
  }
  mv._u0 = _posU - _size * 0.5f;
  mv._v0 = _posV + _size * 0.5f;
  ps.AddVertex(&mv);

  ps.AddIndex(0, VIndex);
  ps.AddIndex(0, VIndex + 1);
  ps.AddIndex(0, VIndex + 2);
  ps.AddIndex(0, VIndex);
  ps.AddIndex(0, VIndex + 2);
  ps.AddIndex(0, VIndex + 3);
}

Ref<CLightPatchQuad> CLightPatch::GetChildren() {
  return _pChildern;
}

void CLightPatch::GetCorners(Vector3Par pos,
                             Vector3Par u,
                             Vector3Par v,
                             Vector3 &c0,
                             Vector3 &c1,
                             Vector3 &c2,
                             Vector3 &c3) {
  Vector3 pos3d = pos + u * _posU + v * _posV;
  float halfsize = _size * 0.5f;
  Vector3 uhalfsize = u * halfsize;
  Vector3 vhalfsize = v * halfsize;
  c0 = pos3d + uhalfsize + vhalfsize;
  c1 = pos3d + uhalfsize - vhalfsize;
  c2 = pos3d - uhalfsize - vhalfsize;
  c3 = pos3d - uhalfsize + vhalfsize;
}

int CLightPatch::IsSet() {
  return _isSet;
}

void CLightPatch::GetEdgeShadow(int &es0, int &es1, int &es2, int &es3) {
  es0 = _edgeShadow[0];
  es1 = _edgeShadow[1];
  es2 = _edgeShadow[2];
  es3 = _edgeShadow[3];
}

float CLightPatch::GetSize() {
  return _size;
}