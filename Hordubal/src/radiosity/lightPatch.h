#ifndef _lightPatch_h_
#define _lightPatch_h_

#include <Es/Types/pointers.hpp>
#include <Es/Containers/listBidir.hpp>
#include <El/Math/Math3d.hpp>

// Order of subpatches, edges
// -----
// |3|0|
// +-+-+
// |2|1|
// -----

// Order of new shadow values in the Split method
// --0--
// |X|X|
// 3-4-1
// |X|X|
// --2--

class CLightPatchQuad;
class CPrimitiveStream;

//! One patch
class CLightPatch : public TLinkBidir<> {
private:
  //! Children of this patch
  Ref<CLightPatchQuad> _pChildern;
  //! Position of the patch in the face. Number from interval (0, 1)
  float _posU;
  //! Position of the patch in the face. Number from interval (0, 1)
  float _posV;
  //! Size relative to face size. Number from interval (0, 1>
  float _size;
  //! Determines, wheter associated edge lies in a shadow
  int _edgeShadow[4];
  //! Determines wheter shadow values for this patch are set
  int _isSet;
  //! Maximum of the sizes in world coordinates
  float _maxAbsSize;
public:
  //! Initialization
  void Init(float posU, float posV, float size, float maxAbsSize);
  //! Deinitialization
  void Done();
  //! Sets shadow values of all 4 edges
  void SetEdgeShadowValues(
    int es0,
    int es1,
    int es2,
    int es3);
  //! Splits the node into 4 new children
  Ref<CLightPatchQuad> Split(
    int ns0,
    int ns1,
    int ns2,
    int ns3,
    int ns4);
  //! Resets this patch
  void Reset();
  //! Returns position of the patch in the world coordinates
  Vector3 GetWorldPosition(Vector3Par pos, Vector3Par u, Vector3Par v);
  //! Returns radius of the patch
  float GetRadius(float uCoef, float vCoef);
  //! Returns importancy of the patch
  float GetImportancy();
  //! Fills up the geometry of the patch
  void GetGeometry(
    CPrimitiveStream &ps,
    Vector3Par pos,
    Vector3Par u,
    Vector3Par v);
  //! Returns children of this patch
  Ref<CLightPatchQuad> GetChildren();
  //! Returns positions of 4 corners in world coordinates
  void GetCorners(
    Vector3Par pos,
    Vector3Par u,
    Vector3Par v,
    Vector3 &c0,
    Vector3 &c1,
    Vector3 &c2,
    Vector3 &c3);
  //! Determines wheter shadow values for this patch are set
  int IsSet();
  //! Returns edge shadow values
  void GetEdgeShadow(int &es0, int &es1, int &es2, int &es3);
  //! Returns size of the patch
  float GetSize();
};

//! Quad of patches
class CLightPatchQuad : public RefCount {
public:
  CLightPatch _patches[4];
};

#endif