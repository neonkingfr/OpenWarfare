#include <El/elementpch.hpp>
#include "../model.h"
#include "faceBuilder.h"

void CFaceBuilder::InitSubtree(int index,
                               float posU,
                               float posV,
                               float sizeU,
                               float sizeV){
  _root[index]._posU = posU;
  _root[index]._posV = posV;
  _root[index]._sizeU = sizeU;
  _root[index]._sizeV = sizeV;
  if (FB_HAS_CHILD(index)) {
    float newSizeU = sizeU * 0.5f;
    float newSizeV = sizeV * 0.5f;
    float halfNewSizeU = newSizeU * 0.5f;
    float halfNewSizeV = newSizeV * 0.5f;
    InitSubtree(index * 4 + 1, posU + halfNewSizeU, posV + halfNewSizeV, newSizeU, newSizeV);
    InitSubtree(index * 4 + 2, posU + halfNewSizeU, posV - halfNewSizeV, newSizeU, newSizeV);
    InitSubtree(index * 4 + 3, posU - halfNewSizeU, posV - halfNewSizeV, newSizeU, newSizeV);
    InitSubtree(index * 4 + 4, posU - halfNewSizeU, posV + halfNewSizeV, newSizeU, newSizeV);
  }
}

void CFaceBuilder::GetPatchGeometry(CPrimitiveStream &ps, int index, Vector3Par pos, Vector3Par u, Vector3Par v) {
  if (_root[index]._isLeaf) {

    Vector3 pos3d = pos + u * _root[index]._posU + v * _root[index]._posV;
    Vector3 normal = v.CrossProduct(u).Normalized();

    SModelVertex mv;
    WORD VIndex;

    VIndex = ps.GetNewVertexIndex();

    mv._position = pos3d + u * _root[index]._sizeU * 0.5f + v * _root[index]._sizeV * 0.5f;
    mv._normal = normal;
    mv._diffuse = D3DCOLOR_ARGB(255,
      (int)(_root[index]._edgeColors[0].X() * 255.0f),
      (int)(_root[index]._edgeColors[0].Y() * 255.0f),
      (int)(_root[index]._edgeColors[0].Z() * 255.0f));
    mv._u0 = _root[index]._posU + _root[index]._sizeU * 0.5f;
    mv._v0 = _root[index]._posV + _root[index]._sizeV * 0.5f;
    ps.AddVertex(&mv);

    mv._position = pos3d + u * _root[index]._sizeU * 0.5f - v * _root[index]._sizeV * 0.5f;
    mv._normal = normal;
    mv._diffuse = D3DCOLOR_ARGB(255,
      (int)(_root[index]._edgeColors[1].X() * 255.0f),
      (int)(_root[index]._edgeColors[1].Y() * 255.0f),
      (int)(_root[index]._edgeColors[1].Z() * 255.0f));
    mv._u0 = _root[index]._posU + _root[index]._sizeU * 0.5f;
    mv._v0 = _root[index]._posV - _root[index]._sizeV * 0.5f;
    ps.AddVertex(&mv);

    mv._position = pos3d - u * _root[index]._sizeU * 0.5f - v * _root[index]._sizeV * 0.5f;
    mv._normal = normal;
    mv._diffuse = D3DCOLOR_ARGB(255,
      (int)(_root[index]._edgeColors[2].X() * 255.0f),
      (int)(_root[index]._edgeColors[2].Y() * 255.0f),
      (int)(_root[index]._edgeColors[2].Z() * 255.0f));
    mv._u0 = _root[index]._posU - _root[index]._sizeU * 0.5f;
    mv._v0 = _root[index]._posV - _root[index]._sizeV * 0.5f;
    ps.AddVertex(&mv);

    mv._position = pos3d - u * _root[index]._sizeU * 0.5f + v * _root[index]._sizeV * 0.5f;
    mv._normal = normal;
    mv._diffuse = D3DCOLOR_ARGB(255,
      (int)(_root[index]._edgeColors[3].X() * 255.0f),
      (int)(_root[index]._edgeColors[3].Y() * 255.0f),
      (int)(_root[index]._edgeColors[3].Z() * 255.0f));
    mv._u0 = _root[index]._posU - _root[index]._sizeU * 0.5f;
    mv._v0 = _root[index]._posV + _root[index]._sizeV * 0.5f;
    ps.AddVertex(&mv);

    ps.AddIndex(0, VIndex);
    ps.AddIndex(0, VIndex + 1);
    ps.AddIndex(0, VIndex + 2);
    ps.AddIndex(0, VIndex);
    ps.AddIndex(0, VIndex + 2);
    ps.AddIndex(0, VIndex + 3);
  }
  else {
    Assert(FB_HAS_CHILD(index));
    int childindex = index * 4 + 1;
    GetPatchGeometry(ps, childindex + 0, pos, u, v);
    GetPatchGeometry(ps, childindex + 1, pos, u, v);
    GetPatchGeometry(ps, childindex + 2, pos, u, v);
    GetPatchGeometry(ps, childindex + 3, pos, u, v);
  }
}

CFaceBuilder::CFaceBuilder() {
  _root[0]._isLeaf = 1;
  _root[0]._edgeColors[0] =
    _root[0]._edgeColors[1] =
    _root[0]._edgeColors[2] =
    _root[0]._edgeColors[3] = Vector3(0.0f, 0.0f, 0.0f);
  InitSubtree(0, 0.5f, 0.5f, 1.0f, 1.0f);
}

void CFaceBuilder::Reset() {
  _root[0]._isLeaf = 1;
  _root[0]._edgeColors[0] =
    _root[0]._edgeColors[1] =
    _root[0]._edgeColors[2] =
    _root[0]._edgeColors[3] = Vector3(0.0f, 0.0f, 0.0f);
}

int CFaceBuilder::ForceSplit(int index) {
  if (FB_HAS_CHILD(index)) {
    int childindex = index * 4 + 1;
    if (_root[index]._isLeaf) {
   
      // Zero the leaf attribute
      _root[index]._isLeaf = 0;

      Vector3 c0 = (_root[index]._edgeColors[3] + _root[index]._edgeColors[0]) * 0.5f;
      Vector3 c1 = (_root[index]._edgeColors[0] + _root[index]._edgeColors[1]) * 0.5f;
      Vector3 c2 = (_root[index]._edgeColors[1] + _root[index]._edgeColors[2]) * 0.5f;
      Vector3 c3 = (_root[index]._edgeColors[2] + _root[index]._edgeColors[3]) * 0.5f;
      Vector3 c4 = (_root[index]._edgeColors[0] +
        _root[index]._edgeColors[1] +
        _root[index]._edgeColors[2] + 
        _root[index]._edgeColors[3]) * 0.25f;

      // 1-st child
      _root[childindex + 0]._edgeColors[0] = _root[index]._edgeColors[0];
      _root[childindex + 0]._edgeColors[1] = c1;
      _root[childindex + 0]._edgeColors[2] = c4;
      _root[childindex + 0]._edgeColors[3] = c0;
      _root[childindex + 0]._isLeaf = 1;

      // 2-st child
      _root[childindex + 1]._edgeColors[0] = c1;
      _root[childindex + 1]._edgeColors[1] = _root[index]._edgeColors[1];
      _root[childindex + 1]._edgeColors[2] = c2;
      _root[childindex + 1]._edgeColors[3] = c4;
      _root[childindex + 1]._isLeaf = 1;

      // 3-st child
      _root[childindex + 2]._edgeColors[0] = c4;
      _root[childindex + 2]._edgeColors[1] = c2;
      _root[childindex + 2]._edgeColors[2] = _root[index]._edgeColors[2];
      _root[childindex + 2]._edgeColors[3] = c3;
      _root[childindex + 2]._isLeaf = 1;

      // 4-st child
      _root[childindex + 3]._edgeColors[0] = c0;
      _root[childindex + 3]._edgeColors[1] = c4;
      _root[childindex + 3]._edgeColors[2] = c3;
      _root[childindex + 3]._edgeColors[3] = _root[index]._edgeColors[3];
      _root[childindex + 3]._isLeaf = 1;
    }
    return childindex;
  }
  else {
    return -1;
  }
}

void CFaceBuilder::AddColors(int index,
                             Vector3Par c0,
                             Vector3Par c1,
                             Vector3Par c2,
                             Vector3Par c3) {
  if (_root[index]._isLeaf) {
    _root[index]._edgeColors[0] += c0;
    _root[index]._edgeColors[1] += c1;
    _root[index]._edgeColors[2] += c2;
    _root[index]._edgeColors[3] += c3;
  }
  else {
    Assert(FB_HAS_CHILD(index));
    int childindex = index * 4 + 1;
    Vector3 nc0 = (c3 + c0) * 0.5f;
    Vector3 nc1 = (c0 + c1) * 0.5f;
    Vector3 nc2 = (c1 + c2) * 0.5f;
    Vector3 nc3 = (c2 + c3) * 0.5f;
    Vector3 nc4 = (c0 + c1 + c2 + c3) * 0.25f;
    AddColors(childindex + 0, c0, nc1, nc4, nc0);
    AddColors(childindex + 1, nc1, c1, nc2, nc4);
    AddColors(childindex + 2, nc4, nc2, c2, nc3);
    AddColors(childindex + 3, nc0, nc4, nc3, c3);
 }
}

void CFaceBuilder::GetGeometry(CPrimitiveStream &ps, Vector3Par pos, Vector3Par u, Vector3Par v) {
  GetPatchGeometry(ps, 0, pos, u, v);
}
