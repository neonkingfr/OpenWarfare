#include <El/elementpch.hpp>
#include "radiosity.h"

#define SPLIT_MODEL_TRESHOLD 1.0f

/*
TODO:
- Pamatovani si vyrenderovanych textur pro kazdy SBlock, renderovat pouze updates, prostudovat cube mapping
- Samotna radiosita: Vytvoreni sekundarnich zdroju (stromova struktura)
- Presnost na jednotku delky - ne podle rozmeru obdelniku
- Verify flag u patches
- Rozdelit patch i kdyz je verify, kdyz se na nem dlouho nic nedelo
*/

// --------------------------------------------------------------------------

void CRadiosity::ModelNodeAdd(int id, SModelNode *pModelNode) {

  // FLY add proper submodel to block array
  SBlock *pBlock = new SBlock;
  pBlock->_modelId = id;
  pBlock->_nodeId = pModelNode->_id;
  pBlock->_origin = pModelNode->_origin;
  pBlock->_dimension = pModelNode->_dimension;
  _radiosityBlocks.Add(pBlock);

  for (int i = 0; i < _radiosityLights.Size(); i++) {
    // FLY add proper submodel to all lights
    _radiosityLights[i]->AddBlock(pBlock->_modelId, pBlock->_nodeId, pBlock->_origin, pBlock->_dimension);
  }


/*
  if ((pModelNode->_children.Size() == 0) ||
      ((pModelNode->_dimension.Size() / 
        _eyeOrigin.Position().Distance(pModelNode->_origin.Position())) < SPLIT_MODEL_TRESHOLD)) {

    // FLY add proper submodel to block array
    SBlock *pBlock = new SBlock;
    pBlock->_id = id;
    pBlock->_origin = pModelNode->_origin;
    pBlock->_dimension = pModelNode->_dimension;
    _radiosityBlocks.Add(pBlock);

    for (int i = 0; i < _radiosityLights.Size(); i++) {
      // FLY add proper submodel to all lights
      _radiosityLights[i]->AddBlock(pBlock->_id, pBlock->_origin, pBlock->_dimension);
      // FLY Reset affected patches
      _radiosityLights[i]->Reset(pBlock->_origin.Position(), pBlock->_dimension.Size() * 0.5f);
    }
  }
  else {
    for (int i = 0; i < pModelNode->_children.Size(); i++) {
      ModelNodeAdd(id, pModelNode->_children[i]);
    }
  }
*/
}

int CRadiosity::GetMostImportantLightIndex() {
  int miLightIndex = -1;
  float miLightImportancy = -1.0f;
  for (int i = 0; i < _radiosityLights.Size(); i++) {
    if (_radiosityLights[i]->GetImportancy() > miLightImportancy) {
      miLightIndex = i;
      miLightImportancy = _radiosityLights[i]->GetImportancy();
    }
  }
  return miLightIndex;
}

void CRadiosity::GetModelFaceGeometry(int modelId, int faceNumber) {
  for (int i = 0; i < _radiosityLights.Size(); i++) {
    _radiosityLights[i]->AddModelFaceGeometry(*_faceBuilder, modelId, faceNumber);
  }
}

void CRadiosity::GetModelFaceProperties(int modelId,
                                        int faceNumber,
                                        Vector3 &pos,
                                        Vector3 &u,
                                        Vector3 &v) {
  if (_radiosityLights.Size() > 0) {
    _radiosityLights[0]->GetModelFaceProperties(modelId, faceNumber, pos, u, v);
  }
}


CRadiosity::CRadiosity() {
  _faceBuilder = new CFaceBuilder();
}

void CRadiosity::SetOrigin(Matrix4Par eyeOrigin) {
  _eyeOrigin = eyeOrigin;
  for (int i = 0; i < _radiosityLights.Size(); i++) {
    _radiosityLights[i]->Resort(eyeOrigin.Position());
  }
}

void CRadiosity::ModelAdd(SModel *pModel) {
  // FLY change some secondary lights
  // ...
  _inputModels.Add(pModel);
  ModelNodeAdd(pModel->_id, &pModel->_node);
}

void CRadiosity::ModelRemove(int id) {
  // FLY change some secondary lights
  // ...

  // Remove item from model array
  for (int i = 0; i < _inputModels.Size(); i++) {
    if (_inputModels[i]->_id == id) {
      _inputModels.Delete(i);
      break;
    }
  }

  // Remove all blocks related to the model from block array
  for (int i = 0; i < _radiosityBlocks.Size(); i++) {
    if (_radiosityBlocks[i]->_modelId == id) {
      _radiosityBlocks.Delete(i);
      break;
    }
  }

  // Remove all blocks related to the model from all lights
  for (int i = 0; i < _radiosityLights.Size(); i++) {
    _radiosityLights[i]->RemoveBlock(id, 0);
  }
}

void CRadiosity::ModelMove(int id, Matrix4Par origin) {
  for (int i = 0; i < _radiosityLights.Size(); i++) {
    _radiosityLights[i]->MoveBlock(id, 0, origin);
  }
}

void CRadiosity::LightAdd(SLight *pLight) {
  _inputLights.Add(pLight);
  // FLY add primary and secondary point light sources
  CLight *newLight = new CLight(pLight->_id, pLight->_position, pLight->_color);
  _radiosityLights.Add(newLight);
  
  // Register models in the light
  for (int i = 0; i < _radiosityBlocks.Size(); i++) {
    newLight->AddBlock(
      _radiosityBlocks[i]->_modelId,
      _radiosityBlocks[i]->_nodeId,
      _radiosityBlocks[i]->_origin,
      _radiosityBlocks[i]->_dimension) ;
  }
}

void CRadiosity::LightRemove(int id) {
  // FLY Remove specified light and all secondary light sources derived from this light
  for (int i = 0; i < _radiosityLights.Size(); i++) {
    if (_radiosityLights[i]->GetId() == id) {
      _radiosityLights.Delete(i);
      break;
    }
  }
}

void CRadiosity::Refine(CModelArray *pScene) {
  int miLigtIndex = GetMostImportantLightIndex();
  if (miLigtIndex > -1) {
    _radiosityLights[miLigtIndex]->Refine(pScene);
  }
}

void CRadiosity::GetGeometry(CPrimitiveStream &ps) {

  //_radiosityLights[0]->GetGeometry(ps);

/*
  for (int i = 0; i < _radiosityLights.Size(); i++) {
    _radiosityLights[i]->GetGeometry(ps);
  }
*/


  // For all models
  for (int i = 0; i < _radiosityBlocks.Size(); i++) {
    // For all their faces
    for (int j = 0; j < 6; j++) {
      _faceBuilder->Reset();
      GetModelFaceGeometry(_radiosityBlocks[i]->_modelId, j);
      Vector3 pos;
      Vector3 u;
      Vector3 v;
      GetModelFaceProperties(_radiosityBlocks[i]->_modelId, j, pos, u, v);
      _faceBuilder->GetGeometry(ps, pos, u, v);
    }
  }

}