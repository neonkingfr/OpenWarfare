//-----------------------------------------------------------------------------
// File: Hordubal.h
//
// Desc: Header file Hordubal sample app
//-----------------------------------------------------------------------------
#ifndef AFX_HORDUBAL_H__8864856B_ADD0_4A95_AEDE_6DB549ED6BBE__INCLUDED_
#define AFX_HORDUBAL_H__8864856B_ADD0_4A95_AEDE_6DB549ED6BBE__INCLUDED_

#include <winsock.h>
#include "modelArray.h"

//-----------------------------------------------------------------------------
// Defines, and constants
//-----------------------------------------------------------------------------
// TODO: change "DirectX AppWizard Apps" to your name or the company name
#define DXAPP_KEY        TEXT("Software\\DirectX AppWizard Apps\\Hordubal")

// Struct to store the current input state
struct UserInput
{
    BYTE diks[256];   // DirectInput keyboard state buffer 

    // TODO: change as needed
    BOOL bRotateUp;
    BOOL bRotateDown;
    BOOL bRotateLeft;
    BOOL bRotateRight;

    BOOL bMoveForward;
    BOOL bMoveBackward;

    BOOL bMoveUp;
    BOOL bMoveDown;
    
    BOOL bWireframe;
    BOOL bTextures;

};




//-----------------------------------------------------------------------------
// Name: class CMyD3DApplication
// Desc: Application class. The base class (CD3DApplication) provides the 
//       generic functionality needed in all Direct3D samples. CMyD3DApplication 
//       adds functionality specific to this sample program.
//-----------------------------------------------------------------------------
class CMyD3DApplication : public CD3DApplication
{
	  // Communication with another process
	  SOCKET _socketRecv;
	  SOCKET _socketServer;
    void Socket_HandleError(bool send);
    void Socket_SendRealMessage(const SMessage &message);
    bool Socket_ReceiveMessage(SMessage	&message);
    void Socket_DispatchModelMessage(SMessage	&message);


    CModelArray             m_ModelArray;
    DWORD                   m_hVSBranch;
    DWORD                   m_hPSBranch;
    Vector3                 m_EyePos;
    float                   m_EyeRotY;
    float                   m_EyeRotX;
    BOOL                    m_bWireframe;
    BOOL                    m_bTextures;

    BOOL                    m_bLoadingApp;          // TRUE, if the app is loading
    ID3DXFont*              m_pD3DXFont;            // D3DX font    

    LPDIRECTINPUT8          m_pDI;                  // DirectInput object
    LPDIRECTINPUTDEVICE8    m_pKeyboard;            // DirectInput keyboard device
    UserInput               m_UserInput;            // Struct for storing user input 

protected:
    HRESULT OneTimeSceneInit();
    HRESULT InitDeviceObjects();
    HRESULT RestoreDeviceObjects();
    HRESULT InvalidateDeviceObjects();
    HRESULT DeleteDeviceObjects();
    HRESULT Render();
    HRESULT FrameMove();
    HRESULT FinalCleanup();
    HRESULT ConfirmDevice( D3DCAPS8*, DWORD, D3DFORMAT );

    HRESULT RenderText();

    HRESULT InitInput( HWND hWnd );
    void    UpdateInput( UserInput* pUserInput );
    void    CleanupDirectInput();

    VOID    ReadSettings();
    VOID    WriteSettings();

public:
    LRESULT MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
    CMyD3DApplication();
    ~CMyD3DApplication();
};


#endif // !defined(AFX_HORDUBAL_H__8864856B_ADD0_4A95_AEDE_6DB549ED6BBE__INCLUDED_)
