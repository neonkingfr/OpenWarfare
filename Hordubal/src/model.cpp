#include <El/elementpch.hpp>
#include <d3dx8.h>
#include "model.h"

DWORD dwModelVertexDecl[] =
{
    D3DVSD_STREAM(0),
    D3DVSD_REG(D3DVSDE_POSITION,  D3DVSDT_FLOAT3),      // position
    D3DVSD_REG(D3DVSDE_NORMAL,    D3DVSDT_FLOAT3),      // normal
    D3DVSD_REG(D3DVSDE_DIFFUSE,   D3DVSDT_D3DCOLOR),    // diffuse color
    D3DVSD_REG(D3DVSDE_TEXCOORD0, D3DVSDT_FLOAT2),      // texture coordinate
    D3DVSD_END()
};

void ConvertMatrix(D3DMATRIX &mat, Matrix4Par src)
{
	mat._11 = src.DirectionAside()[0];
	mat._12 = src.DirectionAside()[1];
	mat._13 = src.DirectionAside()[2];

	mat._21 = src.DirectionUp()[0];
	mat._22 = src.DirectionUp()[1];
	mat._23 = src.DirectionUp()[2];

	mat._31 = src.Direction()[0];
	mat._32 = src.Direction()[1];
	mat._33 = src.Direction()[2];

	mat._41 = src.Position()[0];
	mat._42 = src.Position()[1];
	mat._43 = src.Position()[2];

	mat._14 = 0;
	mat._24 = 0;
	mat._34 = 0;
	mat._44 = 1;
}

CModel::CModel(EModelTypes modelType,
               int id,
               Matrix4Par origin) {
  _modelType = modelType;
  _id = id;
  _origin = origin;
}

void CModel::SetOrigin(Matrix4Par origin) {
  _origin = origin;
}

Matrix4 CModel::GetOrigin() {
  return _origin;
}
