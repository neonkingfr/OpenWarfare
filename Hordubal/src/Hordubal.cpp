//-----------------------------------------------------------------------------
// File: Hordubal.cpp
//
// Desc: DirectX window application created by the DirectX AppWizard
//-----------------------------------------------------------------------------
#define STRICT
#define DIRECTINPUT_VERSION 0x0800
#include <windows.h>
#include <basetsd.h>
#include <math.h>
#include <stdio.h>
#include <D3DX8.h>
#include <DXErr8.h>
#include <tchar.h>
#include <dinput.h>
#include "D3DApp.h"
#include "D3DUtil.h"
#include "DXUtil.h"
#include "resource.h"
#include "Hordubal.h"



//-----------------------------------------------------------------------------
// Global access to the app (needed for the global WndProc())
//-----------------------------------------------------------------------------
CMyD3DApplication* g_pApp  = NULL;
HINSTANCE          g_hInst = NULL;




//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: Entry point to the program. Initializes everything, and goes into a
//       message-processing loop. Idle time is used to render the scene.
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR, INT )
{
    CMyD3DApplication d3dApp;

    g_pApp  = &d3dApp;
    g_hInst = hInst;

    if( FAILED( d3dApp.Create( hInst ) ) )
        return 0;

    return d3dApp.Run();
}

void CMyD3DApplication::Socket_HandleError(bool send) {
	int error = WSAGetLastError();

	if (send)
		LogF("Sockets: Connection lost (cannot finish send)");
	else
		LogF("Sockets: Connection lost (cannot finish recv)");

	if (_socketRecv != INVALID_SOCKET) {
		closesocket(_socketRecv);
		_socketRecv = INVALID_SOCKET;
	}
}

void CMyD3DApplication::Socket_SendRealMessage(const SMessage &message) {
	if (_socketRecv == INVALID_SOCKET) return;

	fd_set set;
	FD_ZERO(&set);
	FD_SET(_socketRecv, &set);
	timeval timeout = {30L, 0L};
	int result = select(FD_SETSIZE, NULL, &set, NULL, &timeout);
	if (result == 0) {
		LogF("Sockets: Timed out during send, message lost.");
		return;
	}
	if (result == SOCKET_ERROR) {
		Socket_HandleError(true);
		return;
	}

	result = send(_socketRecv, (const char *)&message, sizeof(message), 0);
	if (result == SOCKET_ERROR) {
		Socket_HandleError(true);
	}
}

bool CMyD3DApplication::Socket_ReceiveMessage(SMessage	&message) {
	if (_socketRecv == INVALID_SOCKET) return false;

	fd_set set;
	FD_ZERO(&set);
	FD_SET(_socketRecv, &set);
	timeval timeout = {0L, 0L};

	int result = select(FD_SETSIZE, &set, NULL, NULL, &timeout);
	if (result == 0) return false; // no data
	if (result == SOCKET_ERROR)	{
		Socket_HandleError(false);
		return false;
	}

	int len = recv(_socketRecv, (char *)&message, sizeof(message), 0);
	if (len == 0) return false; // no data
	if (len == SOCKET_ERROR) {
		Socket_HandleError(false);
		return false;
	}
	while (len < sizeof(message)) {
		FD_ZERO(&set);
		FD_SET(_socketRecv, &set);
		timeval timeout = {30L, 0L};
		result = select(FD_SETSIZE, &set, NULL, NULL, &timeout);
		if (result == 0) {
			LogF("Sockets: Timed out during send, message lost.");
			return false;
		}
		if (result == SOCKET_ERROR)	{
			Socket_HandleError(false);
			return false;
		}

		int len2 = recv(_socketRecv, (char *)&message + len, sizeof(message) - len, 0);
		if (len2 == SOCKET_ERROR) {
			Socket_HandleError(false);
			return false;
		}
		len += len2;
	}
	return true;
}

void CMyD3DApplication::Socket_DispatchModelMessage(SMessage	&message) {
  switch (message._messageType) {
  case messageType_CreateModel:
    m_ModelArray.CreateModel(m_pd3dDevice, &message);
    break;
  case messageType_DeleteModel:
    m_ModelArray.DeleteModel(&message);
    break;
  case messageType_MoveModel:
    m_ModelArray.MoveModel(&message);
    break;
  }
}

//-----------------------------------------------------------------------------
// Name: CMyD3DApplication()
// Desc: Application constructor. Sets attributes for the app.
//-----------------------------------------------------------------------------
CMyD3DApplication::CMyD3DApplication()
{
    m_dwCreationWidth           = 500;
    m_dwCreationHeight          = 375;
    m_strWindowTitle            = TEXT( "Hordubal" );
    m_bUseDepthBuffer           = TRUE;

    m_pD3DXFont                 = NULL;
    m_bLoadingApp               = TRUE;
    m_pDI                       = NULL;
    m_pKeyboard                 = NULL;

    ZeroMemory( &m_UserInput, sizeof(m_UserInput) );

    // Eye params
    m_EyePos = Vector3(0, 0, -5);
    m_EyeRotY = 0.0f;
    m_EyeRotX = 0.0f;

    // Rendering status
    m_bWireframe = FALSE;
    m_bTextures = TRUE;

    // Read settings from registry
    ReadSettings();

    // Sockets
	  WSADATA data;
	  WSAStartup(0x0101, &data);

	  _socketRecv = INVALID_SOCKET;
	  _socketServer = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	  if (_socketServer != INVALID_SOCKET) {
		  sockaddr_in local;
		  local.sin_family = AF_INET;
		  local.sin_addr.s_addr = INADDR_ANY;
		  local.sin_port = htons(2500);
		  if (bind(_socketServer, (sockaddr *)&local, sizeof(local)) != SOCKET_ERROR) {
			  if (listen(_socketServer, 1) == SOCKET_ERROR) {
          LogF("Sockets: Failed to listen.");
			  }
		  }
		  else {
        LogF("Sockets: Failed to bind.");
			  closesocket(_socketServer);
			  _socketServer = INVALID_SOCKET;
		  }
	  }
	  else {
      LogF("Sockets: Failed to create.");
	  }

}

CMyD3DApplication::~CMyD3DApplication() {
	if (_socketRecv != INVALID_SOCKET) closesocket(_socketRecv);
	if (_socketServer != INVALID_SOCKET) closesocket(_socketServer);
	WSACleanup();
}



//-----------------------------------------------------------------------------
// Name: OneTimeSceneInit()
// Desc: Called during initial app startup, this function performs all the
//       permanent initialization.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::OneTimeSceneInit()
{
    // TODO: perform one time initialization

    // Drawing loading status message until app finishes loading
    SendMessage( m_hWnd, WM_PAINT, 0, 0 );

    // Initialize DirectInput
    InitInput( m_hWnd );

    m_bLoadingApp = FALSE;

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: ReadSettings()
// Desc: Read the app settings from the registry
//-----------------------------------------------------------------------------
VOID CMyD3DApplication::ReadSettings()
{
    HKEY hkey;
    if( ERROR_SUCCESS == RegCreateKeyEx( HKEY_CURRENT_USER, DXAPP_KEY, 
        0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey, NULL ) )
    {
        // TODO: change as needed

        // Read the stored window width/height.  This is just an example,
        // of how to use DXUtil_Read*() functions.
        DXUtil_ReadIntRegKey( hkey, TEXT("Width"), &m_dwCreationWidth, m_dwCreationWidth );
        DXUtil_ReadIntRegKey( hkey, TEXT("Height"), &m_dwCreationHeight, m_dwCreationHeight );

        RegCloseKey( hkey );
    }
}




//-----------------------------------------------------------------------------
// Name: WriteSettings()
// Desc: Write the app settings to the registry
//-----------------------------------------------------------------------------
VOID CMyD3DApplication::WriteSettings()
{
    HKEY hkey;
    DWORD dwType = REG_DWORD;
    DWORD dwLength = sizeof(DWORD);

    if( ERROR_SUCCESS == RegCreateKeyEx( HKEY_CURRENT_USER, DXAPP_KEY, 
        0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey, NULL ) )
    {
        // TODO: change as needed

        // Write the window width/height.  This is just an example,
        // of how to use DXUtil_Write*() functions.
        DXUtil_WriteIntRegKey( hkey, TEXT("Width"), m_rcWindowClient.right );
        DXUtil_WriteIntRegKey( hkey, TEXT("Height"), m_rcWindowClient.bottom );

        RegCloseKey( hkey );
    }
}





//-----------------------------------------------------------------------------
// Name: InitInput()
// Desc: Initialize DirectInput objects
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::InitInput( HWND hWnd )
{
    HRESULT hr;

    // Create a IDirectInput8*
    if( FAILED( hr = DirectInput8Create( GetModuleHandle(NULL), DIRECTINPUT_VERSION, 
                                         IID_IDirectInput8, (VOID**)&m_pDI, NULL ) ) )
        return DXTRACE_ERR_NOMSGBOX( "DirectInput8Create", hr );
    
    // Create a IDirectInputDevice8* for the keyboard
    if( FAILED( hr = m_pDI->CreateDevice( GUID_SysKeyboard, &m_pKeyboard, NULL ) ) )
        return DXTRACE_ERR_NOMSGBOX( "CreateDevice", hr );
    
    // Set the keyboard data format
    if( FAILED( hr = m_pKeyboard->SetDataFormat( &c_dfDIKeyboard ) ) )
        return DXTRACE_ERR_NOMSGBOX( "SetDataFormat", hr );
    
    // Set the cooperative level on the keyboard
    if( FAILED( hr = m_pKeyboard->SetCooperativeLevel( hWnd, 
                                            DISCL_NONEXCLUSIVE | 
                                            DISCL_FOREGROUND | 
                                            DISCL_NOWINKEY ) ) )
        return DXTRACE_ERR_NOMSGBOX( "SetCooperativeLevel", hr );

    // Acquire the keyboard
    m_pKeyboard->Acquire();

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: ConfirmDevice()
// Desc: Called during device initialization, this code checks the display device
//       for some minimum set of capabilities
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::ConfirmDevice( D3DCAPS8* pCaps, DWORD dwBehavior,
                                          D3DFORMAT Format )
{
    BOOL bCapsAcceptable;

    // TODO: Perform checks to see if these display caps are acceptable.
    bCapsAcceptable = TRUE;

    if( bCapsAcceptable )         
        return S_OK;
    else
        return E_FAIL;
}




//-----------------------------------------------------------------------------
// Name: InitDeviceObjects()
// Desc: Initialize scene objects.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::InitDeviceObjects()
{
  
    m_ModelArray.CreateDefaultScene(m_pd3dDevice);

/*
    // Create the model
    Matrix4 matWorld;
    matWorld.SetIdentity();
    matWorld.SetPosition(Vector3(0, 0, 10));
    SData data;
    data._modelType = modelType_Cube;
    data._dataCube._id = 0;
    data._dataCube._origin = matWorld;
    data._dataCube._color = D3DCOLOR_XRGB(255, 255, 255);
    data._dataCube._size = 1.0f;
    m_ModelArray.CreateModel(m_pd3dDevice, &data);
*/

    // Create shaders
    ID3DXBuffer *pCode;
    D3DXAssembleShaderFromFile("Model.vsh", 0, NULL, &pCode, NULL);
    m_pd3dDevice->CreateVertexShader(dwModelVertexDecl, (DWORD*)pCode->GetBufferPointer(), &m_hVSBranch, 0);
    pCode->Release();
    D3DXAssembleShaderFromFile("Model.psh", 0, NULL, &pCode, NULL );
    m_pd3dDevice->CreatePixelShader((DWORD*)pCode->GetBufferPointer(),&m_hPSBranch);
    pCode->Release();


    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: RestoreDeviceObjects()
// Desc: Restores scene objects.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::RestoreDeviceObjects()
{
    // TODO: setup render states
    HRESULT hr;

    // Setup a material
    D3DMATERIAL8 mtrl;
    D3DUtil_InitMaterial( mtrl, 1.0f, 0.0f, 0.0f );
    m_pd3dDevice->SetMaterial( &mtrl );

    // Set up the textures
    m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
    m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
    m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
    m_pd3dDevice->SetTextureStageState( 0, D3DTSS_MINFILTER, D3DTEXF_LINEAR );
    m_pd3dDevice->SetTextureStageState( 0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR );

    // Set miscellaneous render states
    m_pd3dDevice->SetRenderState( D3DRS_DITHERENABLE,   FALSE );
    m_pd3dDevice->SetRenderState( D3DRS_SPECULARENABLE, FALSE );
    m_pd3dDevice->SetRenderState( D3DRS_ZENABLE,        TRUE );
    m_pd3dDevice->SetRenderState( D3DRS_AMBIENT,        0x000F0F0F );

	  //m_pd3dDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

/*
    m_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
    m_pd3dDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
    m_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
    m_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
*/

     
    


    // Set the projection matrix
/*
    D3DXMATRIX matProj;
    FLOAT fAspect = ((FLOAT)m_d3dsdBackBuffer.Width) / m_d3dsdBackBuffer.Height;
    D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, fAspect, 1.0f, 100.0f );
    m_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );
*/

    // Create a D3D font using D3DX
    HFONT hFont = CreateFont( 20, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE,
                              ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
                              ANTIALIASED_QUALITY, FF_DONTCARE, "Arial" );      
    if( FAILED( hr = D3DXCreateFont( m_pd3dDevice, hFont, &m_pD3DXFont ) ) )
        return DXTRACE_ERR_NOMSGBOX( "D3DXCreateFont", hr );

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: FrameMove()
// Desc: Called once per frame, the call is the entry point for animating
//       the scene.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::FrameMove()
{

    // -----------------------------------------------------------
    // Socket processing start
    // -----------------------------------------------------------

		if (_socketRecv != INVALID_SOCKET) {
			// try to read message
      SMessage message;
      // Read messages in begin-end scope
      if (Socket_ReceiveMessage(message)) {
        // If there will be stream of items...
        if (message._messageType == messageType_BeginScene) {
          do {
            // Read until you get some message
            while (!Socket_ReceiveMessage(message)) {};
            // Dispatch messages
            Socket_DispatchModelMessage(message);
          } while (message._messageType != messageType_EndScene);
        }
        // Read messages as they come
        else {
          do {
            // Dispatch messages
            Socket_DispatchModelMessage(message);
          } while (Socket_ReceiveMessage(message));
        }
      }
		}
		else {
			// try to establish connection
			fd_set set;
			FD_ZERO(&set);
			FD_SET(_socketServer, &set);
			timeval timeout = {0L, 0L};
			int result = select(FD_SETSIZE, &set, NULL, NULL, &timeout);
			if (result == 1) _socketRecv = accept(_socketServer, NULL, NULL);
		}

    
    // -----------------------------------------------------------
    // Socket processing end
    // -----------------------------------------------------------


    // TODO: update world

    // Update user input state
    UpdateInput( &m_UserInput );

    // Update the world state according to user input
    D3DXMATRIX matWorld;
    D3DXMATRIX matRotY;
    D3DXMATRIX matRotX;

    if( m_UserInput.bRotateLeft && !m_UserInput.bRotateRight )
        m_EyeRotY += m_fElapsedTime;
    else if( m_UserInput.bRotateRight && !m_UserInput.bRotateLeft )
        m_EyeRotY -= m_fElapsedTime;

    if( m_UserInput.bRotateUp && !m_UserInput.bRotateDown )
      m_EyeRotX -= m_fElapsedTime;
    else if( m_UserInput.bRotateDown && !m_UserInput.bRotateUp )
      m_EyeRotX += m_fElapsedTime;


    Vector3 vecDir(-(float)sin(m_EyeRotY), 0.0f, (float)cos(m_EyeRotY));
    if( m_UserInput.bMoveForward && !m_UserInput.bMoveBackward )
      m_EyePos += vecDir * m_fElapsedTime * 5.0f;
    else if( m_UserInput.bMoveBackward && !m_UserInput.bMoveForward )
      m_EyePos -= vecDir * m_fElapsedTime * 5.0f;

    if( m_UserInput.bMoveUp && !m_UserInput.bMoveDown )
        m_EyePos[1] += m_fElapsedTime;
    else if( m_UserInput.bMoveDown && !m_UserInput.bMoveUp )
        m_EyePos[1] -= m_fElapsedTime;

    if (m_UserInput.bWireframe) m_bWireframe = !m_bWireframe;
    if (m_UserInput.bTextures) m_bTextures = !m_bTextures;

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: UpdateInput()
// Desc: Update the user input.  Called once per frame 
//-----------------------------------------------------------------------------
void CMyD3DApplication::UpdateInput( UserInput* pUserInput )
{
    HRESULT hr;

    // Get the input's device state, and put the state in dims
    ZeroMemory( &pUserInput->diks, sizeof(pUserInput->diks) );
    hr = m_pKeyboard->GetDeviceState( sizeof(pUserInput->diks), &pUserInput->diks );
    if( FAILED(hr) ) 
    {
        m_pKeyboard->Acquire();
        return; 
    }

    // TODO: Process user input as needed
    pUserInput->bRotateLeft   = ( (pUserInput->diks[DIK_LEFT] & 0x80)   == 0x80 );
    pUserInput->bRotateRight  = ( (pUserInput->diks[DIK_RIGHT] & 0x80)  == 0x80 );
    pUserInput->bRotateUp     = ( (pUserInput->diks[DIK_PGUP] & 0x80)   == 0x80 );
    pUserInput->bRotateDown   = ( (pUserInput->diks[DIK_PGDN] & 0x80)   == 0x80 );

    pUserInput->bMoveForward  = ( (pUserInput->diks[DIK_UP] & 0x80)     == 0x80 );
    pUserInput->bMoveBackward = ( (pUserInput->diks[DIK_DOWN] & 0x80)   == 0x80 );


    pUserInput->bMoveUp       = ( (pUserInput->diks[DIK_Q] & 0x80)      == 0x80 );
    pUserInput->bMoveDown     = ( (pUserInput->diks[DIK_A] & 0x80)      == 0x80 );

    pUserInput->bWireframe    = ( (pUserInput->diks[DIK_1] & 0x80)      == 0x80 );
    pUserInput->bTextures     = ( (pUserInput->diks[DIK_2] & 0x80)      == 0x80 );

}




//-----------------------------------------------------------------------------
// Name: Render()
// Desc: Called once per frame, the call is the entry point for 3d
//       rendering. This function sets up render states, clears the
//       viewport, and renders the scene.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::Render()
{
    // Set the wireframe
    if (m_bWireframe)
      m_pd3dDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
    else
      m_pd3dDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

    // Clear the viewport
    m_pd3dDevice->Clear( 0L, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,
                         0x000000ff, 1.0f, 0L );

    // Set shaders
    m_pd3dDevice->SetVertexShader(m_hVSBranch);
    m_pd3dDevice->SetPixelShader(m_hPSBranch);

    // Set shader constants
    D3DXMATRIX matProj;
    FLOAT fAspect = ((FLOAT)m_d3dsdBackBuffer.Width) / m_d3dsdBackBuffer.Height;
    D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, fAspect, 1.0f, 100.0f );
    m_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );
    D3DXMATRIX MatTemp;
    D3DXMatrixTranspose(&MatTemp, &matProj);
    m_pd3dDevice->SetVertexShaderConstant(4, &MatTemp, 4);

    float BranchC8[] = {0.0f, 0.0f, 1.0f, 0.0f};
    m_pd3dDevice->SetVertexShaderConstant(8, BranchC8, 1);
    float BranchC9[] = {0.5f, 1.0f, 0.0f, 0.0f};
    m_pd3dDevice->SetVertexShaderConstant(9, BranchC9, 1);
    float BranchpC0[] = {0.4f, 0.4f, 0.4f, 0.0f}; // Ambient light color
    m_pd3dDevice->SetPixelShaderConstant(0, BranchpC0, 1);

    // Set the View matrix
    Matrix4 matRotY;
    matRotY.SetRotationY(m_EyeRotY);
    Matrix4 matRotX;
    matRotX.SetRotationX(m_EyeRotX);
    Matrix4 matPos;
    matPos.SetIdentity();
    matPos.SetPosition(m_EyePos);
    Matrix4 mat;
    //mat = matPos * matRotY * matRotX;
    mat = (matPos * matRotY * matRotX).InverseGeneral();

    D3DXMATRIX matView;
    ConvertMatrix(matView, mat);

    // Begin the scene
    if( SUCCEEDED( m_pd3dDevice->BeginScene() ) )
    {
        // Drawing the models
/*
        m_ModelArray.Draw(
          m_pd3dDevice,
          matView,
          matProj,
          (float)m_d3dsdBackBuffer.Width,
          (float)m_d3dsdBackBuffer.Height);
*/


        m_ModelArray.DrawScene(
          m_pd3dDevice,
          matView,
          matProj,
          (float)m_d3dsdBackBuffer.Width,
          (float)m_d3dsdBackBuffer.Height,
          m_EyePos);


        // Render stats and help text  
        RenderText();

        // End the scene.
        m_pd3dDevice->EndScene();
    }

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: RenderText()
// Desc: Renders stats and help text to the scene.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::RenderText()
{
    D3DCOLOR fontColor        = D3DCOLOR_ARGB(255,255,255,0);
    D3DCOLOR fontWarningColor = D3DCOLOR_ARGB(255,0,255,255);
    TCHAR szMsg[MAX_PATH] = TEXT("");
    RECT rct;
    ZeroMemory( &rct, sizeof(rct) );       

    m_pD3DXFont->Begin();
    rct.left   = 2;
    rct.right  = m_d3dsdBackBuffer.Width - 20;

    // Output display stats
    INT nNextLine = 40; 

    lstrcpy( szMsg, m_strDeviceStats );
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );

    lstrcpy( szMsg, m_strFrameStats );
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );

    // Output statistics & help
    nNextLine = m_d3dsdBackBuffer.Height; 

    wsprintf( szMsg, TEXT("Arrow keys: Up=%d Down=%d Left=%d Right=%d"), 
              m_UserInput.bRotateUp, m_UserInput.bRotateDown, m_UserInput.bRotateLeft, m_UserInput.bRotateRight );
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );

    sprintf( szMsg, TEXT("Eye State: %0.3f, %0.3f, %0.3f"), 
                    m_EyePos.X(), m_EyePos.Y(), m_EyePos.Z());
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );

    lstrcpy( szMsg, TEXT("Use arrow keys to update input") );
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );

    lstrcpy( szMsg, TEXT("Press 'F2' to configure display") );
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );

    m_pD3DXFont->End();

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: Overrrides the main WndProc, so the sample can do custom message
//       handling (e.g. processing mouse, keyboard, or menu commands).
//-----------------------------------------------------------------------------
LRESULT CMyD3DApplication::MsgProc( HWND hWnd, UINT msg, WPARAM wParam,
                                    LPARAM lParam )
{
    switch( msg )
    {
        case WM_PAINT:
        {
            if( m_bLoadingApp )
            {
                // Draw on the window tell the user that the app is loading
                // TODO: change as needed
                HDC hDC = GetDC( hWnd );
                TCHAR strMsg[MAX_PATH];
                wsprintf( strMsg, TEXT("Loading... Please wait") );
                RECT rct;
                GetClientRect( hWnd, &rct );
                DrawText( hDC, strMsg, -1, &rct, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
                ReleaseDC( hWnd, hDC );
            }
            break;
        }

    }

    return CD3DApplication::MsgProc( hWnd, msg, wParam, lParam );
}




//-----------------------------------------------------------------------------
// Name: InvalidateDeviceObjects()
// Desc: Invalidates device objects.  
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::InvalidateDeviceObjects()
{
    // TODO: Cleanup any objects created in RestoreDeviceObjects()
    SAFE_RELEASE( m_pD3DXFont );

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: DeleteDeviceObjects()
// Desc: Called when the app is exiting, or the device is being changed,
//       this function deletes any device dependent objects.  
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::DeleteDeviceObjects()
{
    // TODO: Cleanup any objects created in InitDeviceObjects()

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: FinalCleanup()
// Desc: Called before the app exits, this function gives the app the chance
//       to cleanup after itself.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::FinalCleanup()
{
    // TODO: Perform any final cleanup needed
    // Cleanup DirectInput
    CleanupDirectInput();

    // Write the settings to the registry
    WriteSettings();

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: CleanupDirectInput()
// Desc: Cleanup DirectInput 
//-----------------------------------------------------------------------------
VOID CMyD3DApplication::CleanupDirectInput()
{
    // Cleanup DirectX input objects
    SAFE_RELEASE( m_pKeyboard );
    SAFE_RELEASE( m_pDI );

}




