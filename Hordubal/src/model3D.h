#ifndef _model3D_h_
#define _model3D_h_

#include "model.h"

class CModel3D : public CModel {
protected:
  //! Color of the object
  D3DCOLOR _color;
  //! Stream of primitives
  Ref<CPrimitiveStream> _ps;
public:
  //! Constructor
  CModel3D(
    EModelTypes modelType,
    int id,
    Matrix4Par origin,
    IDirect3DDevice8 *pD3DDevice,
    D3DCOLOR color);
  //! Draws the object
  virtual void Draw(
    IDirect3DDevice8 *pD3DDevice,
    D3DXMATRIX &matView,
    D3DXMATRIX &matProj,
    float backBufferWidth,
    float backBufferHeight);
};

#endif