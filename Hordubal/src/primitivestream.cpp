#include <limits.h>
#include <assert.h>
#include "primitivestream.h"

//extern int g_N0;

//--------------------------------------------------------------------------------------

CPrimitiveStage::CPrimitiveStage() {
  _pTextureS0 = NULL;
  _pTextureS1 = NULL;
  _MinVertexIndex = UINT_MAX;
  _MaxVertexIndex = 0;
}

void CPrimitiveStage::Clear() {
  _Indices.Resize(0);
}

void CPrimitiveStage::AddIndex(const WORD &Index) {
  _Indices.Add(Index);
  if (Index < _MinVertexIndex) _MinVertexIndex = Index;
  if (Index > _MaxVertexIndex) _MaxVertexIndex = Index;
}

int CPrimitiveStage::Size() {
  return _Indices.Size();
}

const WORD *CPrimitiveStage::Data() {
  return _Indices.Data();
}

UINT CPrimitiveStage::GetMinVertexIndex() {
  return _MinVertexIndex;
}

UINT CPrimitiveStage::GetNumVertices() {
  return _MaxVertexIndex - _MinVertexIndex + 1;
}

//--------------------------------------------------------------------------------------
CPrimitiveStream::CPrimitiveStream(IDirect3DDevice8 *pD3DDevice) {
  // Save device
  pD3DDevice->AddRef(); _pD3DDevice = pD3DDevice;
}

CPrimitiveStream::CPrimitiveStream(CPrimitiveStream &PrimitiveStream) {
  _pD3DDevice = PrimitiveStream._pD3DDevice; _pD3DDevice->AddRef();
  _pVB = PrimitiveStream._pVB;
  _pIB = PrimitiveStream._pIB;
  _VertexSize = PrimitiveStream._VertexSize;
  _IndexCount = PrimitiveStream._IndexCount;
}

void CPrimitiveStream::Init(int VertexSize, DWORD FVF) {
  // Save parametres
  _VertexSize = VertexSize;

  HRESULT hr;

  // Create vertex buffer
  hr = _pD3DDevice->CreateVertexBuffer(VB_SIZE * _VertexSize, D3DUSAGE_WRITEONLY, FVF, D3DPOOL_DEFAULT, _pVB.Init());
  assert(SUCCEEDED(hr));

  // Create index buffer
  hr = _pD3DDevice->CreateIndexBuffer(IB_SIZE * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_DEFAULT, _pIB.Init());
  assert(SUCCEEDED(hr));
  
  _IndexCount = 0;
}

void CPrimitiveStream::Clear() {
  for (int i = 0; i < PRIMITIVESTAGE_COUNT; i++) {
    _PS[i].Clear();
  }
  _IndexCount = 0;
  _Vertices.Resize(0);
}

WORD CPrimitiveStream::AddVertex(void *pData) {
  int OldSize = _Vertices.Size() / _VertexSize;
  if (OldSize < VB_SIZE) {
    for (int i = 0; i < _VertexSize; i++) {
      _Vertices.Add(((BYTE*)pData)[i]);
    }
  }
  return OldSize;
}

void CPrimitiveStream::AddIndex(int Stage, WORD Index) {
  if ((Stage < 0) || (Stage >= PRIMITIVESTAGE_COUNT)) {
    LogF("Stage index out of bounds.");
    return;
  }
  if (_IndexCount < IB_SIZE) {
    _PS[Stage].AddIndex(Index);
    _IndexCount++;
  }
}

WORD CPrimitiveStream::GetNewVertexIndex() {
  return _Vertices.Size() / _VertexSize;
}

void CPrimitiveStream::RegisterTextures(int Stage, IDirect3DTexture8 *pTextureS0, RStringB TS0Name, IDirect3DTexture8 *pTextureS1, RStringB TS1Name) {
  if ((Stage < 0) || (Stage >= PRIMITIVESTAGE_COUNT)) {
    LogF("Stage index out of bounds.");
    return;
  }
  if (pTextureS0 != NULL) {
    _PS[Stage]._pTextureS0 = pTextureS0;
    //_PS[Stage]._pTextureS0->AddRef();
    _PS[Stage]._TS0Name = TS0Name;
  }
  if (pTextureS1 != NULL) {
    _PS[Stage]._pTextureS1 = pTextureS1;
    //_PS[Stage]._pTextureS1->AddRef();
    _PS[Stage]._TS1Name = TS1Name;
  }
}

void CPrimitiveStream::Prepare() {
  HRESULT hr;
  BYTE *pData;

  // Vertex buffer
  assert(_Vertices.Size()/_VertexSize <= VB_SIZE);
  hr = _pVB->Lock(0, _Vertices.Size(), &pData, 0);
  assert(SUCCEEDED(hr));
  memcpy(pData, _Vertices.Data(), _Vertices.Size());
  _pVB->Unlock();

  // Index buffer
/*
  int Size = 0;
  for (int i = 0; i < PRIMITIVESTAGE_COUNT; i++) {
    Size += _PS[i].Size();
  }
  assert(Size <= IB_SIZE);
*/
  hr = _pIB->Lock(0, _IndexCount * sizeof(WORD), &pData, 0);
  assert(SUCCEEDED(hr));
  UINT LastIndex = 0;
  for (int i = 0; i < PRIMITIVESTAGE_COUNT; i++) {
    _StageIndexPos[i] = LastIndex;
    memcpy(&((WORD*)pData)[LastIndex], _PS[i].Data(), _PS[i].Size() * sizeof(WORD));
    LastIndex += _PS[i].Size();
  }
  _pIB->Unlock();
}

void CPrimitiveStream::Draw(BOOL SetTextures) {
  HRESULT hr;

  hr = _pD3DDevice->SetStreamSource(0, _pVB, _VertexSize);
  assert(SUCCEEDED(hr));

  hr = _pD3DDevice->SetIndices(_pIB, 0);
  assert(SUCCEEDED(hr));
  
  for (int i = 0; i < PRIMITIVESTAGE_COUNT; i++) {
    if (_PS[i].Size() > 0) {
      _pD3DDevice->SetTexture(0, NULL);
      _pD3DDevice->SetTexture(1, NULL);
      if (SetTextures) {
        _pD3DDevice->SetTexture(0, _PS[i]._pTextureS0);
        _pD3DDevice->SetTexture(1, _PS[i]._pTextureS1);
      }
      _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
      _pD3DDevice->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);
      _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
      _pD3DDevice->SetTextureStageState(1, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);
      hr = _pD3DDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, _PS[i].GetMinVertexIndex(), _PS[i].GetNumVertices(), _StageIndexPos[i], _PS[i].Size() / 3);
      assert(SUCCEEDED(hr));
      //g_N0 += _PS[i].Size() / 3;
    }
  }
}

AutoArray<BYTE> CPrimitiveStream::GetVertices() {
  return _Vertices;
}

CPrimitiveStage *CPrimitiveStream::GetPrimitiveStage(int i) {
  return &_PS[i];
}

int CPrimitiveStream::GetIndexCount() {
  return _IndexCount;
}
