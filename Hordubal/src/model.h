#ifndef _model_h_
#define _model_h_

#include <d3dx8.h>
#include <Es/Framework/debugLog.hpp>
#include <El/Math/math3d.hpp>
#include <Es/Types/pointers.hpp>
#include "primitiveStream.h"
#include "message.h"

//! Description of the structure of model vertex
#define D3DFVF_MODEL_VERTEX (D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_DIFFUSE|D3DFVF_TEX1|D3DFVF_TEXCOORDSIZE2(0))

//! Structure of the primitive vertex
struct SModelVertex
{
  // Vertex position
  Vector3 _position;
  // Vertex normal
  Vector3 _normal;
  // Diffuse color
  D3DCOLOR _diffuse;
  // 1st set, 2-D
  float _u0, _v0;
};

//! Declaration of the model vertex
DWORD dwModelVertexDecl[];

void ConvertMatrix(D3DMATRIX &mat, Matrix4Par src);

//! Ancestor of all kinds of models
class CModel : public RefCount {
protected:
  //! Type of the object
  EModelTypes _modelType;
  //! Id of the object - the instance of model
  int _id;
  //! Origin of the object
  Matrix4 _origin;
public:
  //! Constructor
  CModel(
    EModelTypes modelType,
    int id,
    Matrix4Par origin);
  //! Sets new origin of the object
  void SetOrigin(Matrix4Par origin);
  //! Returns origin of the object
  Matrix4 GetOrigin();
  //! Returns the id of the model
  int GetId() {return _id;};
  //! Draws the object
  virtual void Draw(
    IDirect3DDevice8 *pD3DDevice,
    D3DXMATRIX &matView,
    D3DXMATRIX &matProj,
    float backBufferWidth,
    float backBufferHeight) = 0;
  //! Determines wheter model intersects with the specified abscissa
  virtual int IntersectsWithAbscissa(Vector3Par p, Vector3Par v) {return 0;};
  virtual Vector3 GetDimension() {return Vector3(0.0f, 0.0f, 0.0f);};
};

#endif