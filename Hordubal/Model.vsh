vs.1.1

; This shader is suitable for rendering of a branch
;
; Input:
;
;  c0-c3  - World * View matrix (4x4)
;  c4-c7  - Projection matrix (4x4)
;  c8.xyz - Vector of light in world coordinates (we will not translate it by WV matrix).
;  c9     - Constant vector (0.5, 1.0, 0, 0)
;
;  v0     - Vertex position (x, y, z, 1) related to W*V*P matrix
;  v3     - Vertex normal (x, y, z, 1) related to W*V matrix
;  v5     - Diffuse color
;  v7.xy  - UV mapping coordinates
;
; Output:
;
;  oPos.xyz - Transformed position (related to V+P matrix)
;  oD0      - Diffuse color
;  oT0.xy   - Texture map coordinates

; Position transformation
m4x4 r0, v0, c0   ; W * V matrix transformation
m4x4 oPos, r0, c4 ; Projection matrix transformation

; Normal transformation
m3x3 r0, v3, c0   ; W * V matrix transformation

; Dot product with light
dp3 r0, r0, -c8

; Modulate against diffuse light color and output diffuse color
;mul oD0, r0, v5
mov oD0, v5

; Output texture coordinate
mov oT0.xy, v7