#ifndef _message_h_
#define _message_h_

#define STRING_SIZE 256

enum EMessageType {
  messageType_CreateModel = 0,
  messageType_DeleteModel,
  messageType_MoveModel,
  messageType_BeginScene,
  messageType_EndScene
};

enum EModelTypes {
  modelType_Cube = 0,
  modelType_Block,
  modelType_String,
  modelType_Sphere
};

struct SVector3 {
  float _x, _y, _z;
};

struct SMatrix4 {
  SVector3 _aside, _up, _direction, _position;
};

struct SMessage {
  // Type of the message
  EMessageType _messageType;
  union {
    
    // messageType_CreateModel
    struct {
      // ID of the model to create
      int _id;
      // Origin of the model
      SMatrix4 _origin;
      // Type of the model
      EModelTypes _modelType;
      union {

        // modelType_Cube
        struct {
          unsigned long _color;
          float _size;
        } _cube;

        // modelType_Block
        struct {
          unsigned long _color;
          float _sizeX;
          float _sizeY;
          float _sizeZ;
        } _block;

        // modelType_String
        struct {
          unsigned long _color;
          char _text[STRING_SIZE];
        } _string;

      };
    } _createModel;
    
    // messageType_DeleteModel
    struct {
      // ID of the model to delete
      int _id;      
    } _deleteModel;
    
    // messageType_MoveModel
    struct {
      // ID of the model to move
      int _id;
      // New origin of the model
      SMatrix4 _origin;
    } _moveModel;
  };
};

#endif