#ifndef _BI_STRING_H_
#define _BI_STRING_H_

#ifndef _CRT_SECURE_NO_WARNINGS
#define __UNDEF__CRT
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "BISDKCommon.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/// Implementation of string class
class BISDK_DLLEXTERN BI_String
{
private:
  /// String pointer
  char *_s;
  /// Safe destroy of the string
  void Destroy()
  {
    if (_s) delete _s;
  }
  void Copy(const BI_String &copy)
  {
    if (copy._s != NULL)
    {
      _s = new char[strlen(copy._s) + 1];
      strcpy(_s, copy._s);
    }
    else
    {
      _s = NULL;
    }
  }
public:
  /// Constructor
  BI_String()
  {
    _s = new char[1];
	_s[0] = '\0';
  }
  /// Constructor
  BI_String(const BI_String &copy)
  {
    Copy(copy);
  }
  /// Constructor
  BI_String(const char *s)
  {
    _s = new char[strlen(s) + 1];
    strcpy(_s, s);
  }
  /// Constructor
  BI_String(const char *s, int len)
  {
    int lenToCopy = BI_Min(len, (int)strlen(s));
    _s = new char[lenToCopy + 1];
    strncpy(_s, s, lenToCopy);
    _s[lenToCopy] = '\0';
  }
  /// Constructor
  BI_String(int n)
  {
    char s[256];
    sprintf(s, "%d", n);
    _s = new char[strlen(s) + 1];
    strcpy(_s, s);
  }
  /// Constructor
  BI_String(bool b)
  {
    char s[8];
    if (b) sprintf(s, "true"); else sprintf(s, "false");
    _s = new char[strlen(s) + 1];
    strcpy(_s, s);
  }
  /// Constructor
  BI_String(float n)
  {
    char s[256];
    sprintf(s, "%f", n);
    _s = new char[strlen(s) + 1];
    strcpy(_s, s);
  }
  /// Constructor
  BI_String(float n, int d)
  {
    char ss[10];
    sprintf(ss, "%%.%df", d);
    char s[256];
    sprintf(s, ss, n);
    _s = new char[strlen(s) + 1];
    strcpy(_s, s);
  }
  /// Destructor
  ~BI_String()
  {
    Destroy();
  }
  ///
  /** \name Operators */
  /** \{ */
  /// Plus operator
  BI_String operator+(const BI_String &src) const
  {
    BI_String s;
    s._s = new char[strlen(_s) + strlen(src._s) + 1];
    strcpy(s._s, _s);
    strcat(s._s, src._s);
    return s;
  }
  /// Assign operator
  void operator=(const BI_String &copy)
  {
    Destroy();
    Copy(copy);
  }
  ///Equality operator 
  bool operator==(const BI_String &src) const
  {
    if (_s == NULL)
    {
      if (src._s == NULL)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      if (src._s == NULL)
      {
        return false;
      }
      else
      {
        return strcmp(_s, src._s) == 0;
      }
    }
  }
  /// Non-equality operator
  bool operator!=(const BI_String &src) const
  {
    return !(*this == src);
  }
  /// Adding operator
  BI_String& operator+=(const BI_String &src)
  {    
	  char * oldStr = _s;
	  _s = new char[strlen(_s) + strlen(src._s) + 1];
	  strcpy(_s, oldStr);
	  strcat(_s, src._s);
	  delete oldStr;
	  return *this;
  }
  /** \} */
  /// Return the integer the string represents
  int GetInt() const
  {
    return atoi(_s);
  }
  /// Return the boolean the string represents
  bool GetBool() const
  {
    return _stricmp(_s, "false") != 0;
  }
  /// Return the float the string represents
  float GetFloat() const
  {
    return (float)atof(_s);
  }
  /// Return pointer to the string
  const char *c_str() const {return _s;}
  /// Return pointer to the string - dangerous, non-const version
  char *data() const {return _s;}
  /// Return length of the string
  int Length() const {return (_s == NULL) ? 0 : (int)strlen(_s);}
  /// Make the string lowercase
  void Lowercase() {_strlwr(_s);}
  /// Find substring in given string (case insensitive), fill out rest with the string behind substring
  bool SubstringI(const BI_String &s, BI_String *rest = NULL) const
  {
    int sLength = s.Length();
    if (_strnicmp(_s, s._s, sLength) == 0)
    {
      if (rest != NULL)
      {
        *rest = BI_String(&_s[sLength]);
      }
      return true;
    }
    return false;
  }
  /// Case insensitive comparison of strings
  bool EqualI(const BI_String &s) const
  {
    return _stricmp(_s, s._s) == 0;
  }
  /// Separate string into 2 along
  bool Split(const BI_String &separator, BI_String *before = NULL, BI_String *after = NULL) const
  {
    BI_String sLwr(*this); sLwr.Lowercase();
    BI_String fLwr(separator); fLwr.Lowercase();
    const char *find = strstr(sLwr._s, fLwr._s);
    if (find == NULL) return false;
    int findStart = (int)(find - sLwr._s);
    if (before != NULL)
    {
      *before = BI_String(_s, findStart);
    }
    if (after != NULL)
    {
      *after = BI_String(&_s[findStart + separator.Length()]);
    }
    return true;
  }
};
#ifdef __UNDEF__CRT
#undef _CRT_SECURE_NO_WARNINGS
#endif
#endif