
/***************************************************************************
         Copyright (c) Microsoft Corporation, All rights reserved.             
    This code sample is provided "AS IS" without warranty of any kind, 
    it is not recommended for use in a production environment.
***************************************************************************/

// pkgicmd.h : Menu commands defined for this Environment package

// do not use #pragma once - used by ctc compiler
#ifndef __SAMPICMD_H_
#define __SAMPICMD_H_

#define IsItemNodeCtx(idmx) ((idmx) == IDM_VS_CTXT_ITEMNODE || (idmx) == IDM_VS_CTXT_XPROJ_MULTIITEM) 

#endif	// __PKGICMD_H_
