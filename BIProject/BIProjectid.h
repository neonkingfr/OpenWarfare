
/***************************************************************************
         Copyright (c) Microsoft Corporation, All rights reserved.             
    This code sample is provided "AS IS" without warranty of any kind, 
    it is not recommended for use in a production environment.
***************************************************************************/

// Projid.h: definitions of GUIDs/IIDs/CLSIDs used in sample.pkg

#pragma once

#define CLSID_MyPackage CLSID_BIProjectPackage

// GUID for BIProject Project Type
// {69d45562-d090-4c2f-aa6c-9d0cc1a68d5b}
DEFINE_GUID(GUID_MyProjectType,
0x69d45562, 0xd090, 0x4c2f, 0xaa, 0x6c, 0x9d, 0x0c, 0xc1, 0xa6, 0x8d, 0x5b);

// GUID_NODETYPE_FileVw_Project -- 
// returned as VSHPROPID_TypeGuid for project node
// {7d0ba753-0070-4d0f-ba1a-9c4a1a407d34}
DEFINE_GUID(GUID_NODETYPE_FileVw_Project, 
0x7d0ba753, 0x0070, 0x4d0f, 0xba, 0x1a, 0x9c, 0x4a, 0x1a, 0x40, 0x7d, 0x34);

// GUID_NODETYPE_FileVw_File -- 
// to be used in image list for file nodes in project explorer
// {5bc11860-ec7e-4281-9cc2-677542710269}
DEFINE_GUID(GUID_NODETYPE_FileVw_File, 
0x5bc11860, 0xec7e, 0x4281, 0x9c, 0xc2, 0x67, 0x75, 0x42, 0x71, 0x02, 0x69);

// GUID_NODETYPE_FileVw_Folder -- 
// to be used in image list for folder nodes in project window (FileView)
// {5058dadb-a530-4ad3-ab8f-226aa6a98742}
DEFINE_GUID(GUID_NODETYPE_FileVw_Folder, 
0x5058dadb, 0xa530, 0x4ad3, 0xab, 0x8f, 0x22, 0x6a, 0xa6, 0xa9, 0x87, 0x42);
