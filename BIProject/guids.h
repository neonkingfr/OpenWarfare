
/***************************************************************************
         Copyright (c) Microsoft Corporation, All rights reserved.             
    This code sample is provided "AS IS" without warranty of any kind, 
    it is not recommended for use in a production environment.
***************************************************************************/

// guids.h: definitions of GUIDs/IIDs/CLSIDs

#pragma once

#include "oleipc.h"      // for ComponentUIManager
#include "BIProjectid.h"

// {2de568e8-2597-4165-bf7f-edeaeb09c0b5}
DEFINE_GUID(IID_CMyProjectCfg, 
0x2de568e8, 0x2597, 0x4165, 0xbf, 0x7f, 0xed, 0xea, 0xeb, 0x09, 0xc0, 0xb5);

// GUID for Enterprise Framework Project Type
// {88dbc4dd-1cc4-41c7-9cd6-339d589ba31a}
DEFINE_GUID(GUID_EFProjectType, 
0x88dbc4dd, 0x1cc4, 0x41c7, 0x9c, 0xd6, 0x33, 0x9d, 0x58, 0x9b, 0xa3, 0x1a);
