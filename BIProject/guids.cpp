
/***************************************************************************
         Copyright (c) Microsoft Corporation, All rights reserved.             
    This code sample is provided "AS IS" without warranty of any kind, 
    it is not recommended for use in a production environment.
***************************************************************************/

// guids.cpp : Declare and initialize GUID's

#include "stdafx.h"
#include "initguid.h"
#include "guids.h"

#include "BIProject.c"
#include "BIProjectExt.c"
