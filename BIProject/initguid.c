
/***************************************************************************
         Copyright (c) Microsoft Corporation, All rights reserved.             
    This code sample is provided "AS IS" without warranty of any kind, 
    it is not recommended for use in a production environment.
***************************************************************************/

//--------------------------------------------------------------------------
// initguid.c - initialises our CMDSETID_StandardCommandSet2K guid.
//---------------------------------------------------------------------------
#include <objbase.h>
#include <initguid.h>

// Command group guids etc.
#include <vsshlids.h>
#include <vsshell.h>

// End of initguid.c
