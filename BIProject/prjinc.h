
/***************************************************************************
         Copyright (c) Microsoft Corporation, All rights reserved.             
    This code sample is provided "AS IS" without warranty of any kind, 
    it is not recommended for use in a production environment.
***************************************************************************/

// PrjInc.h
//

#pragma once

#include "Guids.h"
#include "resource.h"
#include "BIProjectui\resource.h"
#include "BIProjectui\pkgicmd.h"
#include "macros.h"
#include "MiscUtil.h"

// automation
#include "Automation.h"
#include "AutomationEvents.h"
#include "AutomationObjects.h"
#include "AutomationCollections.h"
