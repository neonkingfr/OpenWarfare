//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by BIProject.rc
//
#define IDTLIB_PRJ                      1
#define IDS_PACKAGE_LOAD_KEY            1
#define IDTLIB_PRJEXT                   2
#define IDS_STRING2                     2
#define IDR_REGISTRYSCRIPT              101
#define IDR_CUSTOM                      102
#define IDR_LAUNCH                      103
#define IDI_FILEVW_PROJECT              206
#define IDI_FILEVW_FILE                 207
#define IDI_FILEVW_FOLDER               208
#define IDI_FILEVW_OPENFOLDER           209

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        211
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
