#ifndef __BI_VECTOR3__
#define __BI_VECTOR3__

#include "BISDKCommon.h"
template<class Real>
class __declspec( dllexport ) BI_TMatrix3;
template<class Real>
class __declspec( dllexport ) BI_TMatrix4;

#include <math.h>

/** \brief BI_Vector handling UV positions */
/** 2D BI_Vector describing texture position. User should not need any other methods than Set/Get UV values */
class BISDK_DLLEXTERN BI_UVVector
{
	/** \brief Coordinates of the position */
	float _coords[2]; 

	/* private enum determining access to part position, since we can forget that U-coordinate is at zero index */
	enum Access { _U, _V };

public:

	/** \brief Constructor UVVector */
	/** BI_Vector with defined coordinates x, y, z. If none are specified, null BI_Vector will be created */
	BI_UVVector( float x = 0.0f, float y = 0.0f )
	{
		_coords[_U] = x;
		_coords[_V] = y;
	}

	/** \brief Copy contructor */
	/** Copies all coordinates */
	BI_UVVector( const BI_UVVector& other )
	{
		memcpy( _coords, other._coords, sizeof( float ) * 2 );
	}

	/**\brief Gets U method */
	/** \return float U coordinate */
	float U() const 
  { 
    return (_coords[_U]); 
  }

	/**\brief Gets V method */
	/** \return float V coordinate */
	float V() const  
  { 
    return (_coords[_V]); 
  }

	/** \brief Sets new U coordinate */
	void SetU( float value ) 
  { 
    _coords[_U] = value; 
  }

	/** \brief Sets new V coordinate */
	void SetV( float value ) 
  { 
    _coords[_V] = value; 
  }

	/** \brief Sets vector coordinates */
	void Set( float u, float v )
	{
		SetU( u );
		SetV( v );
	}

	/** \brief Maximum coordinate from UVVector */
	float MaxCoord() const
  { 
    return (BI_Max( _coords[0], _coords[1] ));
  }
	
	/** \brief Minimum coordinate from UVVector */
	float MinCoord() const 
  { 
    return (BI_Min( _coords[_U], _coords[_V] )); 
  }

	/** \brief Checking intersection */
	/** \return true if there is intersection on texture coordinated
	 * \return false otherwise
	 */
	static bool Intersects( const BI_UVVector& a1, const BI_UVVector& a2, const BI_UVVector& b1, const BI_UVVector& b2 )
	{
		BI_UVVector dirA = a2 - a1;
		BI_UVVector dirB = b2 - b1;

		float d = dirB.U() * dirA.V() - dirA.U() * dirB.V();
		if ( fabs( d ) < 0.00000000001f ) 
      return (false);

		float difU = a1.U() - b1.U();
		float difV = a1.V() - b1.V();

    float dx = difU * dirA.V() - difV * dirA.U();
    float dy = - dirB.U() * difV + dirB.V() * difU; 
    float x = dx / d;
    float y = dy / d;
    return ((x > 0.0001f) && (x < 0.9999f) && (y > 0.001f) && (y < 0.9999f));
	}

	/** \name Operators */
	/** \{ */
	/** \brief  Assigment operator */
	/** operation of assigmens assignes new values, destroying old. There is no difference between copy 
	 * contructor and assigment operation except for return value */
	BI_UVVector& operator = ( const BI_UVVector& other )
	{
		memcpy( _coords, other._coords, sizeof( float ) * 2 );
		return (*this);
	}

	/** \brief Access operator */
	/** \returns float coordinate whose representing number is idx */
	float& operator [] ( int idx ) 
  { 
    return (_coords[idx]); 
  }

	/** \brief Access operator for constant access*/
	/** \returns float coordinate whose representing number is idx */
	float operator [] ( int idx ) const 
  { 
    return (_coords[idx]); 
  }

	/** \brief Equality operator */
	bool operator == ( const BI_UVVector& other )const
	{
		return ((fabs( U() - other.U() ) < 1e-4) && (fabs( V() - other.V() ) < 1e-4));
	}

	/** \brief Non-equality operator */
	bool operator != (const BI_UVVector& other )const
	{
		return (!(operator==(other)));
	}

	/** \brief Unary operator - */
	BI_UVVector operator - ()
	{
		BI_UVVector v( *this );
		v *= (-1);
		return (v);
	}

	/** \brief Plus operator */
	BI_UVVector& operator += ( const BI_UVVector& other )
	{
		_coords[_U] += other.U();
		_coords[_V] += other.V();
		return (*this);
	}

	/** \brief Plus operator */
	BI_UVVector operator + ( const BI_UVVector& other )const
	{
		BI_UVVector v( *this );
		return (v += other);
	}
	
	/** \brief Minus operator */
	BI_UVVector& operator -= ( const BI_UVVector& other )
	{
		_coords[_U] -= other.U();
		_coords[_V] -= other.V();
		return (*this);
	}

	/** \brief Minus operator */
	BI_UVVector operator - ( const BI_UVVector& other )const
	{
		BI_UVVector v( *this );
		v -= other;
		return (v);
	}

	/** \brief Multiplication operator */
	BI_UVVector& operator *= ( float f )
	{
		_coords[_U] *= f;
		_coords[_V] *= f;
		return (*this);
	}

	/** \brief Multiplication operator */
	BI_UVVector operator * ( float f )
	{
		BI_UVVector v( *this );
		return (v *= f);
	}

	/** \brief Division by number operator */
	BI_UVVector& operator /= ( float f )
	{
		float invf = 1 / f;
		return ((*this) *= invf);
	}
	/** \brief Division by number operator */
	BI_UVVector operator / ( float f )
	{
		BI_UVVector v( *this );
		return (v /= f);
	}
	/** \} */
};


/** \brief Class handling vector operation in 3D Euclidean space. */
/**	This class is base for normals and vertices. Vertices behave the same, 
 *  but have moreover special flags 
 */
template < typename Real >
class __declspec( dllexport ) BI_TVector3
{
protected:
	/** \brief Coordinates of vector in 3-dimensional space */
	Real _coords[3];

public:
	/** \brief Enum for determining which number correspond with position in vector */
	enum Access { _X, _Y, _Z };
	
	/** \brief Contructor*/
	/** Constructs BI_TVector3 with defined coordinates x, y, z
	 *  Default coordinates values are zeroes
	 */
	BI_TVector3( Real x = (Real)0, Real y = (Real)0, Real z = (Real)0 ) 
  {
    SetX( x );
    SetY( y );
    SetZ( z );
  }

	/** \brief Constructor */
	/** Creates a complete copy of source BI_TVector3 */
	BI_TVector3( const BI_TVector3< Real >& other ) 
  {	
    memcpy( _coords, other._coords, sizeof( Real ) * 3 );
  }

	/** \brief Constructor */
	/** Contructs BI_TVector3 from beginning point and ending point. 
	 *  point is there represented as another BI_TVector3 an no vetrex,
	 *  because in this case we don't need no additional information 
	 */
	BI_TVector3( const BI_TVector3< Real >& begin, const BI_TVector3< Real >& end )
	{
		_coords[0] = end.X() - begin.X();
		_coords[1] = end.Y() - begin.Y();
		_coords[2] = end.Z() - begin.Z();
	}

	/** \brief Access operator */
	/** acces via range, range is <0-2> */
	const Real& operator [] ( int idx ) const 
  { 
    return (_coords[idx]);
  }

	/** \brief Access via [] */
	/** \returns reference to float coordinate whose representing number is idx 
	 */
	Real& operator [] ( int idx ) 
  { 
    return (_coords[idx]); 
  }

	/** \brief Sets BI_TVector3 coordinate */
	/** The result as the same as operator=, but without the need to call constructor on vector
	 */
	void Set( Real x, Real y, Real z )
	{
		SetX( x ); 
    SetY( y ); 
    SetZ( z );
	}

	/** \brief Access to X coordinate*/
	Real X() const 
  { 
    return (_coords[_X]); 
  }

	/** \brief Access to Y coordinate */
	Real Y() const 
  { 
    return (_coords[_Y]); 
  }

	/** \brief Access to Z coordinate */
	Real Z() const 
  { 
    return (_coords[_Z]); 
  }

	/** \brief Sets new X coordinate */
	void SetX( Real value )
  { 
    _coords[_X] = value;
  }

  Real& SetX() 
  {
    return _coords[_X];
  }

	/** \brief Sets new Y coordinate */
	void SetY( Real value ) 
  { 
    _coords[_Y] = value; 
  }

  Real& SetY() 
  {
    return _coords[_Y];
  }

	/** \brief Sets new Z coordinate */
	void SetZ( Real value ) 
  { 
    _coords[_Z] = value; 
  }

  Real& SetZ() 
  {
    return _coords[_Z];
  }

	/** \brief Finds minimum value in BI_TVector3 */
	/** \return minimal value that BI_TVector3 contains 
	 * \param idx will contain index of element holding the value 
	 * \return minimum value */
	Real GetMinimum( int& idx ) const
	{
		idx = 0;
		Real res = _coords[0];
		for ( int i = 1; i < 3; ++i )
    {
			if ( _coords[i] < res )
			{
				res = _coords[i];
				idx = i;
			}
    }
    return (res);
	}

	/** \brief Finds maximum value in BI_TVector3 */
	/** \returns maximal value that BI_TVector3 contains 
	 * \param idx tells which item it was 
	 */
	Real GetMaximum( int& idx ) const
	{
		idx = 0;
		Real res = _coords[0];
		for ( int i = 1; i < 3; ++i )
    {
			if ( _coords[i] > res )
			{
				res = _coords[i];
				idx = i;
			}
    }
    return res;
	}

	/** \brief Equality operator*/
	/** BI_TVector3 are equal if all their values at each position are the same 
	 */
	bool operator == ( const BI_TVector3< Real >& other ) const 
	{ 
    return ((fabs( X() - other.X() ) < 1e-5) && (fabs( Y() - other.Y() ) < 1e-5) && (fabs( Z() - other.Z() ) < 1e-5));
	}
	
	/** \brief Inequality operator */
	/** BI_TVector3 are not equal if any their values on ony position differ 
	 */
	bool operator != ( const BI_TVector3< Real >& other ) const 
  { 
    return (!((*this)==other)); 
  }

	/** \brief BI_TVector3 operation plus.*/
	BI_TVector3< Real >& operator += ( const BI_TVector3< Real >& other )
	{
		_coords[0] += other._coords[0];
		_coords[1] += other._coords[1];
		_coords[2] += other._coords[2];
		return (*this);
	}

	/** \brief BI_TVector3 operator plus */
	BI_TVector3< Real > operator + ( const BI_TVector3< Real >& other ) const
	{
		BI_TVector3< Real > v( *this );
		v += other;
		return (v);
	}

	/** \brief BI_TVector3 operation minus */
	BI_TVector3< Real >& operator -= ( const BI_TVector3< Real >& other )
	{
		_coords[0] -= other._coords[0];
		_coords[1] -= other._coords[1];
		_coords[2] -= other._coords[2];
		return (*this);
	}

  /** \brief Unary operator - */
  BI_TVector3< Real > operator - () const
  {
    BI_TVector3< Real > v( *this );
    v *= (-1);
    return (v);
  }

	/** \brief BI_TVector3 operator minus */
	BI_TVector3< Real > operator - ( const BI_TVector3< Real >& other ) const
	{
		BI_TVector3< Real > v( *this );
		v -= other;
		return (v);
	}

	/** \brief Multiply each element by \param f */
	BI_TVector3< Real >& operator *= ( Real f )
	{
		_coords[0] *= f;
		_coords[1] *= f;
		_coords[2] *= f;
		return (*this);
	}

	/** \brief Multiply each element by \param f */
	BI_TVector3< Real > operator * ( Real f )const
	{
		BI_TVector3< Real > v( *this );
		v *= f;
		return (v);
	}

	/** \brief Divides each element by \param f */
	BI_TVector3< Real >& operator /= ( Real f )
	{
		Real invf = (Real)1 / f;
		return ((*this) *= invf);
	}

	/** \brief Multiply each element by \param f */
	BI_TVector3< Real > operator / ( Real f )
	{
		BI_TVector3< Real > v( *this );
		v /= f;
		return (v);
	}

	/** \brief Assign operator */
	BI_TVector3< Real >& operator = ( const BI_TVector3< Real >& other )
	{
		SetX( other.X() );
		SetY( other.Y() );
		SetZ( other.Z() );
		return (*this);
	}
	
	/** \brief Multiply BI_TVector3 by elements */
	BI_TVector3< Real > MultiplyByElements( const BI_TVector3< Real >& other ) const
	{
		BI_TVector3< Real > v( other.X() * X(), other.Y() * Y(), other.Z() * Z() );
		return (v);
	}

	/** \brief Computes BI_TVector3 dot product */
	Real DotProduct( const BI_TVector3< Real >& other ) const
	{
		return (X() * other.X() + Y() * other.Y() + Z() * other.Z());
	}

	/** \brief Computes BI_TVector3 dot product */
	BI_TVector3< Real > CrossProduct( const BI_TVector3< Real >& other ) const
	{
		Real ox = other.X();
		Real oy = other.Y();
		Real oz = other.Z();
		Real lx = X();
		Real ly = Y();
		Real lz = Z();

		Real x = ly * oz - lz * oy;
		Real y = lz * ox - lx * oz;
		Real z = lx * oy - ly * ox;
		return (BI_TVector3< Real >( x, y, z ));
	}
  void Swap( BI_TVector3 s)
  {
    Real coords[3];
    memcpy(coords, _coords, sizeof(Real)*3);
    memcpy(_coords, s._coords, sizeof(Real)*3);
    memcpy(s._coords, coords, sizeof(Real)*3);
  }

	/** \brief Computes BI_TVector3 normalization  */
	BI_TVector3< Real > Normalized() const
	{
		BI_TVector3< Real > v( *this );
		v.Normalize();
		return (v);
	}

	/** \brief Sets BI_TVector3 to size 1 */
	void Normalize()
	{
		Real sz = Size();
		if ( sz < 1.0000e-7 )
			sz = (Real)1;

		(*this) /= sz;
	}

	/** \brief Computes square size of BI_TVector3 */
	/** \return square size of BI_TVector3.
	 */
	Real SquareSize() const 
  { 
    return (X() * X() + Y() * Y() + Z() * Z()); 
  }

	/** \brief Computes actual size of vector */
	Real Size() const 
  { 
    return (sqrt( SquareSize() )); 
  }

	/** \brief Computes cosine angle between two BI_TVector3.*/
	/** @return cosine of the angle between the edges */
	Real CosAngle( const BI_TVector3< Real >& other ) const
	{
		Real denom2 = SquareSize() * other.SquareSize();
		return (DotProduct( other )/sqrt( denom2 ));
	}
	
	/** \brief Computes distance in Euclidean space  */
	Real Distance( const BI_TVector3< Real >& other ) const 
  { 
    return (sqrt( Distance2( other ) )); 
  }

	/** \brief Computes square distance in Euclidean space  */
	Real Distance2( const BI_TVector3< Real >& other ) const 
  { 
    return ((other - (*this)).SquareSize()); 
  }

  void SetMultiply( const BI_TMatrix3<Real> &a, const BI_TVector3<Real> &o )
  { // vector rotation - only 3x3 matrix is used, translation is ignored
    // u=M*v
    Real x = o[0], y = o[1], z = o[2];

    _coords[0] = a.Get(0, 0) * x + a.Get(0, 1) * y + a.Get(0, 2) * z;
    _coords[1] = a.Get(1, 0) * x + a.Get(1, 1) * y + a.Get(1, 2) * z;
    _coords[2] = a.Get(2, 0) * x + a.Get(2, 1) * y + a.Get(2, 2) * z;
  }

  void SetRotate( const BI_TMatrix4<Real> &a, const BI_TVector3<Real> &v )
  {
    SetMultiply(a.Orientation(), v);
  }


	/**\brief Conversion between different types of BI_TVector3 */
	/** Template method for converting user defined BI_TVector3 to our BI_TVector3. On user size there should be implemented method X(), Y(), Z()
	 * giving required value
	 */
	template < class BI_VectorClass >
  void Convert( const BI_VectorClass& s )
	{
		SetX( (Real)s.X() );
    SetY( (Real)s.Y() );
    SetZ( (Real)s.Z() );
	}

	/** \brief Conversion between different types of BI_TVector3  */
	/** Template method for converting our BI_TVector3 to user defined BI_TVector3. On user size there should be implemented operator[]
	 * giving value at position required and empty contructor 
	 * \param perm describes position of axis how it is Set on user side
	 */
	template < class VectorClass >
	VectorClass Convert( int* perm = NULL ) const
	{
		int permutation[] = {0,1,2};
		if ( perm != NULL )
			memcpy( perm, permutation, sizeof( int ) * 3 );

		VectorClass v;
		v[permutation[0]] = X();
		v[permutation[1]] = Y();
		v[permutation[2]] = Z();
		return (v);
	}
};

/* typedefs for float and double vectors */
typedef BI_TVector3< float >  BI_Vector3;
typedef BI_TVector3< double > BI_Vector3D;

/* for better handling, several trivial instances are created */

/* zero BI_UVVector */
#define BI_ZeroUVVector BI_UVVector( 0.0f, 0.0f )

/* zero BI_Vector3 */
#define BI_ZeroVector3 BI_Vector3( 0.0f, 0.0f, 0.0f )

/* zero BI_Vector3D */
#define BI_ZeroVector3D BI_Vector3D( 0.0, 0.0, 0.0 )

#endif