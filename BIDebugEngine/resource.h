//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by BIDebugEngine.rc
//
#define IDS_PROJNAME                    100
#define IDR_BIDEBUGENGINE               101
#define IDR_ENGINE                      102
#define IDR_PROGRAM                     103
#define IDR_PROGRAM1                    105
#define IDR_SCRIPT                      106
#define IDR_PENDINGBREAKPOINT           107
#define IDR_BOUNDBREAKPOINT             108
#define IDR_DEBUGCONTEXT                109
#define IDR_BREAKPOINTRESOLUTION        110
#define IDR_FRAME                       111
#define IDR_ENUMFRAMEINFO               112
#define IDR_PROPERTY                    113
#define IDR_EXPRESSIONCONTEXT           114
#define IDR_EXPRESSION                  115
#define IDR_DISASSEMBLYSTREAM           116

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           117
#endif
#endif
