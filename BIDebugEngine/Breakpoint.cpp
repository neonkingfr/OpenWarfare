// Breakpoint.cpp : Implementation of CPendingBreakpoint

#include "stdafx.h"
#include "Event.h"
#include "Context.h"
#include "Breakpoint.h"

typedef CComEnumWithCount<IEnumDebugBoundBreakpoints2, &IID_IEnumDebugBoundBreakpoints2, IDebugBoundBreakpoint2 *,  _CopyInterface<IDebugBoundBreakpoint2> > CEnumDebugBoundBreakpoints;

// CPendingBreakpoint

void CPendingBreakpoint::Initialize(IDebugBreakpointRequest2* pBPRequest, IDebugEventCallback2 *pCallback, IDebugEngine2 *pEngine)
{
  _callback = pCallback;
  _engine = pEngine;
  _request = pBPRequest;
}

void CPendingBreakpoint::SendBoundEvent(IDebugBoundBreakpoint2 *pBoundBP)
{
  // Create the bound enumerator.
  CComObject<CEnumDebugBoundBreakpoints>* pBoundEnum;
  CComObject<CEnumDebugBoundBreakpoints>::CreateInstance(&pBoundEnum);
  pBoundEnum->AddRef();

  IDebugBoundBreakpoint2* rgpBoundBP[] = { pBoundBP };
  pBoundEnum->Init(rgpBoundBP, &(rgpBoundBP[1]), NULL, AtlFlagCopy);

  CBreakpointBoundEvent* pBoundEvent = new CBreakpointBoundEvent(pBoundEnum, this);
  pBoundEvent->SendEvent(_callback, _engine, NULL, NULL);

  pBoundEnum->Release();
}

// IDebugPendingBreakpoint2

HRESULT CPendingBreakpoint::CanBind(IEnumDebugErrorBreakpoints2** ppErrorEnum)
{
  LogF("  - CPendingBreakpoint::CanBind");
  return E_NOTIMPL; 
}

HRESULT CPendingBreakpoint::Bind()
{
  BP_REQUEST_INFO info;
  _request->GetRequestInfo(BPREQI_ALLFIELDS, &info);

  if ((info.dwFields & BPREQI_BPLOCATION) && info.bpLocation.bpLocationType == BPLT_CODE_FILE_LINE)
  {
      // Get the file name of the document.
      info.bpLocation.bpLocation.bplocCodeFileLine.pDocPos->GetFileName(&_fileName);
      // Get the position in the document
      info.bpLocation.bpLocation.bplocCodeFileLine.pDocPos->GetRange(&_pos, NULL);

      // Create a context.
      CComObject<CDebugContext> *pContext;
      CComObject<CDebugContext>::CreateInstance(&pContext);
      pContext->AddRef();

      pContext->Initialize(_fileName, _pos);

      // Create a breakpoint resolution.
      CComObject<CBreakpointResolution>* pBPRes;
      CComObject<CBreakpointResolution>::CreateInstance(&pBPRes);
      pBPRes->AddRef();

      pBPRes->Initialize(pContext);

      // Create a bound breakpoint.
      CComObject<CBoundBreakpoint>* pBoundBP;
      CComObject<CBoundBreakpoint>::CreateInstance(&pBoundBP);
      pBoundBP->AddRef();

      pBoundBP->Initialize(this, pBPRes, _enabled);

      SendBoundEvent(pBoundBP);

      // Save bound breakpoint
      int AddBreakpoint(IDebugBoundBreakpoint2 *bp, const char *file, int line);
      USES_CONVERSION;
      AddBreakpoint(pBoundBP, OLE2CT(_fileName), _pos.dwLine);

      pBoundBP->Release();
      pBPRes->Release();
      pContext->Release();

      return S_OK;
    }
    return E_NOTIMPL;
}

HRESULT CPendingBreakpoint::GetState(PENDING_BP_STATE_INFO* pState)
{
  if (!pState) return E_INVALIDARG;
  pState->state = _enabled ? PBPS_ENABLED : PBPS_DISABLED;  
  pState->flags = PBPSF_NONE;
  return S_OK; 
}

HRESULT CPendingBreakpoint::GetBreakpointRequest(IDebugBreakpointRequest2** ppBPRequest)
{
  *ppBPRequest = _request;
  (*ppBPRequest)->AddRef();
  return S_OK;
}

HRESULT CPendingBreakpoint::Virtualize(BOOL fVirtualize)
{
  LogF("  - CPendingBreakpoint::Virtualize");
  return S_OK; 
}

HRESULT CPendingBreakpoint::Enable(BOOL fEnable)
{
  _enabled = fEnable == TRUE;
  return S_OK; 
}

HRESULT CPendingBreakpoint::SetCondition(BP_CONDITION bpCondition)
{
  LogF("  - CPendingBreakpoint::SetCondition");
  return E_NOTIMPL; 
}

HRESULT CPendingBreakpoint::SetPassCount(BP_PASSCOUNT bpPassCount)
{
  LogF("  - CPendingBreakpoint::SetPassCount");
  return E_NOTIMPL; 
}

HRESULT CPendingBreakpoint::EnumBoundBreakpoints(IEnumDebugBoundBreakpoints2** ppEnum)
{
  LogF("  - CPendingBreakpoint::EnumBoundBreakpoints");
  return E_NOTIMPL; 
}

HRESULT CPendingBreakpoint::EnumErrorBreakpoints(BP_ERROR_TYPE bpErrorType, IEnumDebugErrorBreakpoints2** ppEnum)
{
  LogF("  - CPendingBreakpoint::EnumErrorBreakpoints");
  return E_NOTIMPL; 
}

HRESULT CPendingBreakpoint::Delete(void)
{
  LogF("  - CPendingBreakpoint::Delete");
  return E_NOTIMPL; 
}


// CBoundBreakpoint

HRESULT CBoundBreakpoint::Initialize(IDebugPendingBreakpoint2 *pPending, IDebugBreakpointResolution2 *pBPRes, bool enabled)
{
  _pending = pPending;
  _resolution = pBPRes;
  _enabled = enabled;
  return S_OK;
}

// IDebugBoundBreakpoint2

HRESULT CBoundBreakpoint::GetPendingBreakpoint(IDebugPendingBreakpoint2** ppPendingBreakpoint)
{
  *ppPendingBreakpoint = _pending;
  (*ppPendingBreakpoint)->AddRef();
  return S_OK;
}

HRESULT CBoundBreakpoint::GetState(BP_STATE* pState)
{
  if (!pState) return E_INVALIDARG;
  *pState = _enabled ? BPS_ENABLED : BPS_DISABLED;  
  return S_OK; 
}

HRESULT CBoundBreakpoint::GetHitCount(DWORD* pdwHitCount)
{
  LogF("  - CBoundBreakpoint::GetHitCount");
  return E_NOTIMPL; 
}

HRESULT CBoundBreakpoint::GetBreakpointResolution(IDebugBreakpointResolution2** ppBPResolution)
{
  *ppBPResolution = _resolution;
  (*ppBPResolution)->AddRef();
  return S_OK;
}

HRESULT CBoundBreakpoint::Enable(BOOL fEnable)
{
  _enabled = fEnable == TRUE;

  // Save bound breakpoint
  void EnableBreakpoint(IDebugBoundBreakpoint2 *bp, bool enable);
  EnableBreakpoint(this, _enabled);
  return S_OK; 
}

HRESULT CBoundBreakpoint::SetHitCount(DWORD dwHitCount)
{
  LogF("  - CBoundBreakpoint::SetHitCount");
  return E_NOTIMPL; 
}

HRESULT CBoundBreakpoint::SetCondition(BP_CONDITION bpCondition)
{
  LogF("  - CBoundBreakpoint::SetCondition");
  return E_NOTIMPL; 
}

HRESULT CBoundBreakpoint::SetPassCount(BP_PASSCOUNT bpPassCount)
{
  LogF("  - CBoundBreakpoint::SetPassCount");
  return E_NOTIMPL; 
}

HRESULT CBoundBreakpoint::Delete(void)
{
  void RemoveBreakpoint(IDebugBoundBreakpoint2 *bp);
  RemoveBreakpoint(this);
  return S_OK; 
}
