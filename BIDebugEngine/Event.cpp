#include "StdAfx.h"
#include ".\Event.h"

typedef CComEnumWithCount<IEnumDebugBoundBreakpoints2, &IID_IEnumDebugBoundBreakpoints2, IDebugBoundBreakpoint2*, _CopyInterface<IDebugBoundBreakpoint2> > CEnumDebugBoundBreakpoints;

// generic event

Event::Event(REFIID iid, DWORD attrib)
{
  _iid = iid;
  _attrib = attrib;
  _refCount = 0;
}

Event::~Event()
{
}

HRESULT Event::SendEvent(IDebugEventCallback2 *pCallback, IDebugEngine2 *pEngine, IDebugProgram2 *pProgram, IDebugThread2 *pThread)
{
  AddRef();
  HRESULT hr = pCallback->Event(pEngine, NULL, pProgram, pThread, (IDebugEvent2 *)this, _iid, _attrib);

  if (SUCCEEDED(hr))
  {
    if (_attrib == EVENT_SYNCHRONOUS)
    {
      // suspend
      void WaitForSyncEvent();
      WaitForSyncEvent();
    }
  }
  Release();
  return hr;
}

// IUnknown methods
ULONG Event::AddRef()
{
  return ++_refCount;
}

ULONG Event::Release()
{
  _refCount--;
  if (_refCount == 0)
  {
    delete this;
    return 0;
  }
  return _refCount;
}

HRESULT Event::QueryInterface(REFIID iid, LPVOID *obj)
{
  if (iid == IID_IUnknown) *obj = (IUnknown*)this;
  else if (iid == IID_IDebugEvent2) *obj = (IDebugEvent2*)this;
  else return E_NOINTERFACE; // implemented by inherited classes

  AddRef();
  return S_OK;
}

// IDebugEvent2 methods
HRESULT Event::GetAttributes(DWORD* attrib) {
  *attrib = _attrib;
  return S_OK;
}

// special events

// IUnknown methods implementation macro
#define IMPLEMENT_EVENT_IUNKNOWN(T, I) \
  ULONG T::AddRef() {return Event::AddRef();} \
  ULONG T::Release() {return Event::Release();} \
  HRESULT T::QueryInterface(REFIID iid, LPVOID *obj) \
  { \
    if (IID_##I == iid) \
    { \
      *obj = (I *)(this); AddRef(); return S_OK; \
    } \
    return Event::QueryInterface(iid, obj); \
  }

CEngineCreateEvent::CEngineCreateEvent(IDebugEngine2 *pEngine)
: Event(IID_IDebugEngineCreateEvent2, EVENT_ASYNCHRONOUS)
{
  _engine = pEngine;
}

CEngineCreateEvent::~CEngineCreateEvent()
{
}

IMPLEMENT_EVENT_IUNKNOWN(CEngineCreateEvent, IDebugEngineCreateEvent2)

HRESULT CEngineCreateEvent::GetEngine(IDebugEngine2 **ppEngine)
{
  *ppEngine = _engine;
  (*ppEngine)->AddRef();
  return S_OK;
}

CProgramCreateEvent::CProgramCreateEvent()
: Event(IID_IDebugProgramCreateEvent2, EVENT_ASYNCHRONOUS)
{
}

CProgramCreateEvent::~CProgramCreateEvent()
{
}

IMPLEMENT_EVENT_IUNKNOWN(CProgramCreateEvent, IDebugProgramCreateEvent2)

CLoadCompleteEvent::CLoadCompleteEvent()
: Event(IID_IDebugLoadCompleteEvent2, EVENT_SYNC_STOP)
{
}

CLoadCompleteEvent::~CLoadCompleteEvent()
{
}

IMPLEMENT_EVENT_IUNKNOWN(CLoadCompleteEvent, IDebugLoadCompleteEvent2)

CEntryPointEvent::CEntryPointEvent()
: Event(IID_IDebugEntryPointEvent2, EVENT_SYNC_STOP)
{
}

CEntryPointEvent::~CEntryPointEvent()
{
}

IMPLEMENT_EVENT_IUNKNOWN(CEntryPointEvent, IDebugEntryPointEvent2)

CBreakpointBoundEvent::CBreakpointBoundEvent(IEnumDebugBoundBreakpoints2 *pEnum, IDebugPendingBreakpoint2 *pPending)
: Event(IID_IDebugBreakpointBoundEvent2, EVENT_ASYNCHRONOUS)
{
  _enum = pEnum;
  _pending = pPending;
}

CBreakpointBoundEvent::~CBreakpointBoundEvent()
{
}

IMPLEMENT_EVENT_IUNKNOWN(CBreakpointBoundEvent, IDebugBreakpointBoundEvent2)

// Get the pending breakpoint that was bound to the breakpoint
HRESULT CBreakpointBoundEvent::GetPendingBreakpoint(IDebugPendingBreakpoint2** ppPendingBP)
{
  *ppPendingBP = _pending;
  (*ppPendingBP)->AddRef();
  return S_OK;
}

// Get the list of breakpoints bound on this event
HRESULT CBreakpointBoundEvent::EnumBoundBreakpoints(IEnumDebugBoundBreakpoints2** ppEnum)
{
  *ppEnum = _enum;
  (*ppEnum)->AddRef();
  return S_OK;
}

CCodeBreakpointEvent::CCodeBreakpointEvent(IDebugBoundBreakpoint2 *pBP)
: Event(IID_IDebugBreakpointEvent2, EVENT_SYNC_STOP)
{
  _bp = pBP;
}

CCodeBreakpointEvent::~CCodeBreakpointEvent()
{
}

IMPLEMENT_EVENT_IUNKNOWN(CCodeBreakpointEvent, IDebugBreakpointEvent2);

HRESULT CCodeBreakpointEvent::EnumBreakpoints(IEnumDebugBoundBreakpoints2 **ppEnum)
{
  // Create the bound enumerator.
  CComObject<CEnumDebugBoundBreakpoints>* pBoundEnum;
  CComObject<CEnumDebugBoundBreakpoints>::CreateInstance(&pBoundEnum);

  IDebugBoundBreakpoint2* rgpBoundBP[] = {_bp};
  pBoundEnum->Init(rgpBoundBP, &(rgpBoundBP[1]), NULL, AtlFlagCopy);

  *ppEnum = pBoundEnum;
  (*ppEnum)->AddRef();

  return S_OK;
}

CDebugBreakEvent::CDebugBreakEvent()
: Event(IID_IDebugBreakEvent2, EVENT_ASYNC_STOP)
{
}

CDebugBreakEvent::~CDebugBreakEvent()
{
}

IMPLEMENT_EVENT_IUNKNOWN(CDebugBreakEvent, IDebugBreakEvent2);

COutputDebugStringEvent::COutputDebugStringEvent(const char *str)
: Event(IID_IDebugOutputStringEvent2, EVENT_ASYNCHRONOUS)
{
  USES_CONVERSION;
  _string = CT2OLE(str);
}

COutputDebugStringEvent::~COutputDebugStringEvent()
{
}

// IUnknown methods
IMPLEMENT_EVENT_IUNKNOWN(COutputDebugStringEvent, IDebugOutputStringEvent2)

// IDebugOutputStringEvent2 methods
HRESULT COutputDebugStringEvent::GetString(BSTR* pbstrString)
{
  *pbstrString = SysAllocString(_string);
  return S_OK; 
}
