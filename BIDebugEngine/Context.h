// Context.h : Declaration of the CDebugContext

#pragma once
#include "resource.h"       // main symbols

#include "BIDebugEngine.h"


// CDebugContext

class ATL_NO_VTABLE CDebugContext : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CDebugContext, &CLSID_DebugContext>,
  public IDebugCodeContext2,
  public IDebugDocumentContext2
{
protected:
  CComBSTR _fileName;
  TEXT_POSITION _pos;

public:
	CDebugContext()
	{
	}

  void Initialize(LPCWSTR pszFileName, TEXT_POSITION& pos);

DECLARE_REGISTRY_RESOURCEID(IDR_DEBUGCONTEXT)


BEGIN_COM_MAP(CDebugContext)
	COM_INTERFACE_ENTRY(IDebugCodeContext2)
  COM_INTERFACE_ENTRY(IDebugDocumentContext2)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  // IDebugCodeContext2 methods
  STDMETHOD(GetDocumentContext)(IDebugDocumentContext2 **ppSrcCxt);

  // IDebugMemoryContext2 methods
  STDMETHOD(GetName)(BSTR* pbstrName);
  STDMETHOD(GetInfo)(CONTEXT_INFO_FIELDS dwFields, CONTEXT_INFO* pInfo);
  STDMETHOD(Add)(UINT64 dwCount, IDebugMemoryContext2** ppMemCxt);
  STDMETHOD(Subtract)(UINT64 dwCount, IDebugMemoryContext2** ppMemCxt);
  STDMETHOD(Compare)(CONTEXT_COMPARE compare, IDebugMemoryContext2** rgpMemoryContextSet, DWORD dwMemoryContextSetLen, DWORD* pdwMemoryContext);
  STDMETHOD(Seek)(int ncount, IDebugDocumentContext2 **ppDocContext);

  // IDebugDocumentContext2 methods
  STDMETHOD(GetDocument)(IDebugDocument2 **ppDocument);
  STDMETHOD(GetName)(GETNAME_TYPE gnType, BSTR *pbstrFileName);
  STDMETHOD(EnumCodeContexts)(IEnumDebugCodeContexts2 **ppEnumCodeCxts);
  STDMETHOD(GetLanguageInfo)(BSTR* pbstrLanguage, GUID* pguidLanguage);
  STDMETHOD(GetStatementRange)(TEXT_POSITION* pBegPosition, TEXT_POSITION* pEndPosition);
  STDMETHOD(GetSourceRange)(TEXT_POSITION* pBegPosition, TEXT_POSITION* pEndPosition);
  STDMETHOD(Compare)(DOCCONTEXT_COMPARE compare, IDebugDocumentContext2** rgpDocContextSet, DWORD dwDocContextSetLen, DWORD* pdwDocContext);
};

OBJECT_ENTRY_AUTO(__uuidof(DebugContext), CDebugContext)


// CBreakpointResolution

class ATL_NO_VTABLE CBreakpointResolution : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CBreakpointResolution, &CLSID_BreakpointResolution>,
	public IDebugBreakpointResolution2
{
public:
  BP_RESOLUTION_INFO _info;

public:
	CBreakpointResolution();
  virtual ~CBreakpointResolution();

  void Initialize(IDebugCodeContext2 *pCodeContext);

DECLARE_REGISTRY_RESOURCEID(IDR_BREAKPOINTRESOLUTION)


BEGIN_COM_MAP(CBreakpointResolution)
	COM_INTERFACE_ENTRY(IDebugBreakpointResolution2)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  // IDebugBreakpointResolution2
  STDMETHOD(GetBreakpointType)(BP_TYPE* pBPType);
  STDMETHOD(GetResolutionInfo)(BPRESI_FIELDS dwFields, BP_RESOLUTION_INFO* pBPResolutionInfo);
};

OBJECT_ENTRY_AUTO(__uuidof(BreakpointResolution), CBreakpointResolution)
