// Breakpoint.h : Declaration of the CPendingBreakpoint

#pragma once
#include "resource.h"       // main symbols

#include "BIDebugEngine.h"

// CPendingBreakpoint

class ATL_NO_VTABLE CPendingBreakpoint : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CPendingBreakpoint, &CLSID_PendingBreakpoint>,
	public IDebugPendingBreakpoint2
{
protected:
  CComPtr<IDebugEventCallback2> _callback;
  CComPtr<IDebugEngine2> _engine;
  CComPtr<IDebugBreakpointRequest2> _request;

  CComBSTR _fileName;
  TEXT_POSITION _pos;

  bool _enabled;

public:
	CPendingBreakpoint()
	{
    _enabled = true;
	}

  void Initialize(IDebugBreakpointRequest2* pBPRequest, IDebugEventCallback2 *pCallback, IDebugEngine2 *pEngine);
  void SendBoundEvent(IDebugBoundBreakpoint2* pBoundBP);

DECLARE_REGISTRY_RESOURCEID(IDR_PENDINGBREAKPOINT)


BEGIN_COM_MAP(CPendingBreakpoint)
	COM_INTERFACE_ENTRY(IDebugPendingBreakpoint2)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  // IDebugPendingBreakpoint2
  STDMETHOD(CanBind)(IEnumDebugErrorBreakpoints2** ppErrorEnum);
  STDMETHOD(Bind)();
  STDMETHOD(GetState)(PENDING_BP_STATE_INFO* pState);
  STDMETHOD(GetBreakpointRequest)(IDebugBreakpointRequest2** ppBPRequest);
  STDMETHOD(Virtualize)(BOOL fVirtualize);
  STDMETHOD(Enable)(BOOL fEnable);
  STDMETHOD(SetCondition)(BP_CONDITION bpCondition);
  STDMETHOD(SetPassCount)(BP_PASSCOUNT bpPassCount);
  STDMETHOD(EnumBoundBreakpoints)(IEnumDebugBoundBreakpoints2** ppEnum);
  STDMETHOD(EnumErrorBreakpoints)(BP_ERROR_TYPE bpErrorType, IEnumDebugErrorBreakpoints2** ppEnum);
  STDMETHOD(Delete)(void);
};

OBJECT_ENTRY_AUTO(__uuidof(PendingBreakpoint), CPendingBreakpoint)


// CBoundBreakpoint

class ATL_NO_VTABLE CBoundBreakpoint : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CBoundBreakpoint, &CLSID_BoundBreakpoint>,
	public IDebugBoundBreakpoint2
{
protected:
  CComPtr<IDebugPendingBreakpoint2> _pending;
  CComPtr<IDebugBreakpointResolution2> _resolution;

  bool _enabled;

public:
	CBoundBreakpoint()
	{
    _enabled = true;
	}

  HRESULT Initialize(IDebugPendingBreakpoint2 *pPending, IDebugBreakpointResolution2 *pBPRes, bool enabled);

DECLARE_REGISTRY_RESOURCEID(IDR_BOUNDBREAKPOINT)


BEGIN_COM_MAP(CBoundBreakpoint)
	COM_INTERFACE_ENTRY(IDebugBoundBreakpoint2)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  // IDebugBoundBreakpoint2
  STDMETHOD(GetPendingBreakpoint)(IDebugPendingBreakpoint2** ppPendingBreakpoint);
  STDMETHOD(GetState)(BP_STATE* pState);
  STDMETHOD(GetHitCount)(DWORD* pdwHitCount);
  STDMETHOD(GetBreakpointResolution)(IDebugBreakpointResolution2** ppBPResolution);
  STDMETHOD(Enable)(BOOL fEnable);
  STDMETHOD(SetHitCount)(DWORD dwHitCount);
  STDMETHOD(SetCondition)(BP_CONDITION bpCondition);
  STDMETHOD(SetPassCount)(BP_PASSCOUNT bpPassCount);
  STDMETHOD(Delete)(void);
};

OBJECT_ENTRY_AUTO(__uuidof(BoundBreakpoint), CBoundBreakpoint)
