// Expression.h : Declaration of the CExpressionContext

#pragma once
#include "resource.h"       // main symbols

#include "BIDebugEngine.h"

class CopyDebugProperty
{
public:
  static HRESULT copy(DEBUG_PROPERTY_INFO *p1, DEBUG_PROPERTY_INFO *p2)
  {
    p1->dwFields = p2->dwFields;
    if (p2->bstrFullName) p1->bstrFullName = SysAllocString(p2->bstrFullName);
    if (p2->bstrName) p1->bstrName = SysAllocString(p2->bstrName);
    if (p2->bstrType) p1->bstrType = SysAllocString(p2->bstrType);
    if (p2->bstrValue) p1->bstrValue = SysAllocString(p2->bstrValue);
    if (p2->pProperty)
    {
      p1->pProperty = p2->pProperty;
      p1->pProperty->AddRef();
    }
    p1->dwAttrib = p2->dwAttrib;

    return S_OK;
  }
  static void init(DEBUG_PROPERTY_INFO *p)
  {
    memset(p, 0, sizeof(DEBUG_PROPERTY_INFO));
  }
  static void destroy(DEBUG_PROPERTY_INFO *p)
  {
    if (p->bstrFullName) SysFreeString(p->bstrFullName);
    if (p->bstrName) SysFreeString(p->bstrName);
    if (p->bstrType) SysFreeString(p->bstrType);
    if (p->bstrValue) SysFreeString(p->bstrValue);
    if (p->pProperty) p->pProperty->Release();
  }
};

TypeIsMovableZeroed(DEBUG_PROPERTY_INFO)
TypeIsSimple(const IDebugVariable *)
typedef CComEnumWithCount<IEnumDebugPropertyInfo2, &IID_IEnumDebugPropertyInfo2, DEBUG_PROPERTY_INFO,  CopyDebugProperty > CEnumDebugProperties;

// CExpressionContext

class ATL_NO_VTABLE CExpressionContext : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CExpressionContext, &CLSID_ExpressionContext>,
	public IDebugExpressionContext2
{
protected:
  IDebugScope *_scope;

public:
	CExpressionContext()
	{
	}

  void Init(IDebugScope *scope);

DECLARE_REGISTRY_RESOURCEID(IDR_EXPRESSIONCONTEXT)


BEGIN_COM_MAP(CExpressionContext)
	COM_INTERFACE_ENTRY(IDebugExpressionContext2)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  // IDebugExpressionContext2
  STDMETHOD(GetName)(BSTR *pbstrName);
  STDMETHOD(ParseText)(LPCOLESTR pszCode, PARSEFLAGS dwFlags, UINT nRadix, IDebugExpression2 **ppExpr,
    BSTR *pbstrError, UINT *pichError);
};

OBJECT_ENTRY_AUTO(__uuidof(ExpressionContext), CExpressionContext)


// CExpression

class ATL_NO_VTABLE CExpression : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CExpression, &CLSID_Expression>,
	public IDebugExpression2
{
protected:
  IDebugScope *_scope;
  CComBSTR _code;
  UINT _radix;

public:
	CExpression()
	{
	}

  void Init(IDebugScope *scope, LPCOLESTR pszCode, UINT nRadix);

DECLARE_REGISTRY_RESOURCEID(IDR_EXPRESSION)


BEGIN_COM_MAP(CExpression)
	COM_INTERFACE_ENTRY(IDebugExpression2)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  // IDebugExpression2
  STDMETHOD(EvaluateAsync)(EVALFLAGS dwFlags, IDebugEventCallback2 *pExprCallback);
  STDMETHOD(Abort)();
  STDMETHOD(EvaluateSync)(EVALFLAGS dwFlags, DWORD dwTimeout, IDebugEventCallback2 *pExprCallback, IDebugProperty2 **ppResult);
};

OBJECT_ENTRY_AUTO(__uuidof(Expression), CExpression)

// CDebugProperty

class ATL_NO_VTABLE CDebugProperty : 
  public CComObjectRootEx<CComSingleThreadModel>,
  public CComCoClass<CDebugProperty, &CLSID_Property>,
  public IDebugProperty2
{
protected:
  CComBSTR _name;
  IDebugValueRef _value;

public:
  CDebugProperty()
  {
  }

  void Init(const CComBSTR &name, IDebugValueRef value);

  DECLARE_REGISTRY_RESOURCEID(IDR_PROPERTY)


  BEGIN_COM_MAP(CDebugProperty)
    COM_INTERFACE_ENTRY(IDebugProperty2)
  END_COM_MAP()


  DECLARE_PROTECT_FINAL_CONSTRUCT()

  HRESULT FinalConstruct()
  {
    return S_OK;
  }

  void FinalRelease() 
  {
  }

public:
  // IDebugProperty2
  STDMETHOD(GetPropertyInfo)(DEBUGPROP_INFO_FLAGS dwFields, DWORD nRadix, DWORD dwTimeout,
    IDebugReference2** rgpArgs, DWORD dwArgCount, DEBUG_PROPERTY_INFO* pPropertyInfo);
  STDMETHOD(SetValueAsString)(LPCOLESTR pszValue, DWORD nRadix, DWORD dwTimeout);
  STDMETHOD(SetValueAsReference)(IDebugReference2 **rgpArgs, DWORD dwArgCount,
    IDebugReference2 *pValue, DWORD dwTimeout);
  STDMETHOD(EnumChildren)(DEBUGPROP_INFO_FLAGS dwFields, DWORD dwRadix, REFGUID guidFilter,
    DBG_ATTRIB_FLAGS dwAttribFilter, LPCOLESTR pszNameFilter, DWORD dwTimeout,
    IEnumDebugPropertyInfo2** ppEnum);
  STDMETHOD(GetParent)(IDebugProperty2** ppParent);
  STDMETHOD(GetDerivedMostProperty)(IDebugProperty2** ppDerivedMost);
  STDMETHOD(GetMemoryBytes)(IDebugMemoryBytes2** ppMemoryBytes);
  STDMETHOD(GetMemoryContext)(IDebugMemoryContext2** ppMemory);
  STDMETHOD(GetSize)(DWORD* pdwSize);
  STDMETHOD(GetReference)(IDebugReference2 **ppReference);
  STDMETHOD(GetExtendedInfo)(REFGUID guidExtendedInfo, VARIANT* pExtendedInfo);
};

OBJECT_ENTRY_AUTO(__uuidof(Property), CDebugProperty)
