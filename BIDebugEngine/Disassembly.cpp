// Disassembly.cpp : Implementation of CDisassemblyStream

#include "stdafx.h"
#include "Disassembly.h"

// CDisassemblyStream

// IDebugDisassemblyStream2 implementation

HRESULT CDisassemblyStream::Read(DWORD dwInstructions, DISASSEMBLY_STREAM_FIELDS dwFields,
             DWORD* pdwInstructionsRead, DisassemblyData* prgDisassembly)
{
  LogF("  - CDisassemblyStream::Read");
  LogF("    instructions 0x%x", dwInstructions);
  LogF("    fields 0x%x", dwFields);
  return E_NOTIMPL;


  dwFields &= DSF_ADDRESS | DSF_OPCODE;

  for (DWORD i=0; i<dwInstructions; i++)
  {
    DisassemblyData &data = prgDisassembly[i];
    data.dwFields = dwFields;
    if (dwFields & DSF_ADDRESS)
    {
      data.bstrAddress = SysAllocString(L"<Address>");
    }
    /*
    if (dwFields & DSF_ADDRESSOFFSET)
    {
      data.bstrAddressOffset = SysAllocString(L"<Address offset>");
    }
    if (dwFields & DSF_CODEBYTES)
    {
      data.bstrCodeBytes = SysAllocString(L"<Code bytes>");
    }
    */
    if (dwFields & DSF_OPCODE)
    {
      data.bstrOpcode = SysAllocString(L"<Opcode>");
    }
    /*
    if (dwFields & DSF_OPERANDS)
    {
      if (dwFields & DSF_OPERANDS_SYMBOLS)
      {
        data.bstrOperands = SysAllocString(L"<Operands with symbols>");
      }
      else
      {
        data.bstrOperands = SysAllocString(L"<Operands>");
      }
    }
    if (dwFields & DSF_SYMBOL)
    {
      data.bstrSymbol = SysAllocString(L"<Symbol>");
    }
    if (dwFields & DSF_CODELOCATIONID)
    {
      data.uCodeLocationId = i;
    }
    if (dwFields & DSF_POSITION)
    {
      data.posBeg.dwLine = 0;
      data.posBeg.dwColumn = 0;
      data.posEnd = data.posBeg;
    }
    if (dwFields & DSF_DOCUMENTURL)
    {
      data.bstrSymbol = SysAllocString(L"file://c:/actions.txt");
    }
    if (dwFields & DSF_BYTEOFFSET)
    {
      data.dwByteOffset = 0;
    }
    if (dwFields & DSF_FLAGS)
    {
      data.dwFlags = DF_DOCUMENTCHANGE | DF_HASSOURCE;
    }
*/
  }

  *pdwInstructionsRead = dwInstructions;

  return S_OK; 
}

HRESULT CDisassemblyStream::Seek(SEEK_START dwSeekStart, IDebugCodeContext2* pCodeContext,
             UINT64 uCodeLocationId, INT64 iInstructions)
{
  LogF("  - CDisassemblyStream::Seek");
  LogF("    start 0x%x", dwSeekStart);
  LogF("    context 0x%x", pCodeContext);
  LogF("    location id 0x%lx", uCodeLocationId);
  LogF("    instructions 0x%lx", iInstructions);
  return S_OK; 
}

HRESULT CDisassemblyStream::GetCodeLocationId(IDebugCodeContext2* pCodeContext, UINT64 *puCodeLocationId)
{
  LogF("  - CDisassemblyStream::GetCodeLocationId");
  return E_NOTIMPL; 
}

HRESULT CDisassemblyStream::GetCodeContext(UINT64 uCodeLocationId, IDebugCodeContext2** ppCodeContext)
{
  LogF("  - CDisassemblyStream::GetCodeContext");
  return E_NOTIMPL; 
}

HRESULT CDisassemblyStream::GetCurrentLocation(UINT64* puCodeLocationId)
{
  LogF("  - CDisassemblyStream::GetCurrentLocation");
  return E_NOTIMPL; 
}

HRESULT CDisassemblyStream::GetDocument(BSTR bstrDocumentUrl, IDebugDocument2** ppDocument)
{
  LogF("  - CDisassemblyStream::GetDocument");
  return E_NOTIMPL; 
}

HRESULT CDisassemblyStream::GetScope(DISASSEMBLY_STREAM_SCOPE* pdwScope)
{
  LogF("  - CDisassemblyStream::GetScope");
  return E_NOTIMPL; 
}

HRESULT CDisassemblyStream::GetSize(UINT64* pnSize)
{
  LogF("  - CDisassemblyStream::GetSize");
  *pnSize = 100;
  return S_OK; 
}

