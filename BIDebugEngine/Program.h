// Program.h : Declaration of the CProgram

#pragma once
#include "resource.h"       // main symbols
#include "windows.h"

#include "BIDebugEngine.h"

#include "Engine.h"
#include "./Event.h"

TypeIsMovable(CComPtr<IDebugThread2>)

struct BreakpointInfo
{
  CComPtr<IDebugBoundBreakpoint2> bp;
  RString file;
  int line;
};
TypeIsMovable(BreakpointInfo)

extern const GUID __declspec(selectany) 
IID_BIProgram = { 0x96fb5c89, 0xb5ae, 0x4ecf, { 0xb1, 0xbe, 0xd4, 0xb4, 0x58, 0xd, 0xd1, 0x38 } };

// CProgram

class ATL_NO_VTABLE CProgram : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CProgram, &CLSID_Program>,
  public IDebugProgramNode2,
	public IDebugProgram2
{
protected:
  CComPtr<IDebugEngine2> _engine;
  CComPtr<IDebugProgramPublisher2> _programPublisher;

  CComPtr<IDebugEventCallback2> _callback; // MDM callback
  GUID _progId; // MDM program ID

  AutoArray< CComPtr<IDebugThread2> > _threads;
  AutoArray<BreakpointInfo> _breakpoints;

public:
	CProgram();
  virtual ~CProgram();

  void Start(IDebugEngine2 *pEngine);

  void ScriptLoaded(IDebugScript *script, const char *name);
  void ScriptEntered(IDebugScript *script);
  void ScriptTerminated(IDebugScript *script);

  void FireBreakpoint(IDebugScript *script, DWORD bp);
  void Breaked(IDebugScript *script);

  int AddBreakpoint(IDebugBoundBreakpoint2 *bp, const char *file, int line);
  void RemoveBreakpoint(IDebugBoundBreakpoint2 *bp);
  void EnableBreakpoint(IDebugBoundBreakpoint2 *bp, bool enable);

  void OutputDebugString(const char *str);

DECLARE_REGISTRY_RESOURCEID(IDR_PROGRAM1)

BEGIN_COM_MAP(CProgram)
  COM_INTERFACE_ENTRY(IDebugProgramNode2)
	COM_INTERFACE_ENTRY(IDebugProgram2)
  COM_INTERFACE_ENTRY_IID(IID_BIProgram, CProgram)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  // IDebugProgramNode2
  STDMETHOD(GetProgramName)(BSTR* pbstrProgramName);
  STDMETHOD(GetHostName)(DWORD dwHostNameType, BSTR* pbstrHostName);
  STDMETHOD(GetHostPid)(AD_PROCESS_ID *pHostProcessId);
  STDMETHOD(GetHostMachineName_V7)(BSTR* pbstrHostMachineName);
  STDMETHOD(Attach_V7)(
    IDebugProgram2* pMDMProgram, IDebugEventCallback2* pCallback, 
    DWORD dwReason);
  STDMETHOD(GetEngineInfo)(BSTR* pbstrEngine, GUID* pguidEngine);
  STDMETHOD(DetachDebugger_V7)(void);

  // IDebugProgram2
  STDMETHOD(EnumThreads)(IEnumDebugThreads2** ppEnum);
  STDMETHOD(GetName)(BSTR* pbstrName);
  STDMETHOD(GetProcess)(IDebugProcess2** ppProcess);
  STDMETHOD(Terminate)(void);
  STDMETHOD(Attach)(IDebugEventCallback2* pCallback);
  STDMETHOD(Detach)(void);
  STDMETHOD(GetProgramId)(GUID* pguidProgramId);
  STDMETHOD(GetDebugProperty)(IDebugProperty2** ppProperty);
  STDMETHOD(Execute)();
  STDMETHOD(Continue)(IDebugThread2 *pThread);
  STDMETHOD(Step)(IDebugThread2 *pThread, STEPKIND sk, STEPUNIT step);
  STDMETHOD(CauseBreak)(void);
  //GetEngineInfo already defined for IDebugProgramNode2.
  STDMETHOD(EnumCodeContexts)(IDebugDocumentPosition2* pDocPos, IEnumDebugCodeContexts2** ppEnum);
  STDMETHOD(GetMemoryBytes)(IDebugMemoryBytes2** ppMemoryBytes);
  STDMETHOD(GetDisassemblyStream)(DISASSEMBLY_STREAM_SCOPE dwScope, IDebugCodeContext2* pCodeContext, IDebugDisassemblyStream2** ppDisassemblyStream);
  STDMETHOD(EnumModules)(IEnumDebugModules2** ppEnum);
  STDMETHOD(GetENCUpdate)(IDebugENCUpdate** ppUpdate);
  STDMETHOD(EnumCodePaths)(LPCOLESTR pszHint, IDebugCodeContext2* pStart, IDebugStackFrame2* pFrame, BOOL fSource, IEnumCodePaths2** ppEnum, IDebugCodeContext2** ppSafety);
  STDMETHOD(WriteDump)(DUMPTYPE dumptype, LPCOLESTR pszCrashDumpUrl);
  STDMETHOD(CanDetach)(void);

  // redirected from CEngine
  HRESULT CreatePendingBreakpoint(IDebugBreakpointRequest2 *pBPRequest, IDebugPendingBreakpoint2** ppPendingBP);

  void EngineAttach(IDebugProgram2* pMDMProgram, IDebugEventCallback2* pCallback, DWORD dwReason);


protected:
  int AddThread(IDebugThread2 *thr);

  IDebugThread2 *FindThread(IDebugScript *script);
  IDebugBoundBreakpoint2 *FindBreakpoint(DWORD id);
  
  // set all breakpoints to new launched script
  void SetBreakpoints(IDebugScript *script);
};

OBJECT_ENTRY_AUTO(__uuidof(Program), CProgram)
