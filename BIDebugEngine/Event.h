#pragma once

// generic event
class Event : public IDebugEvent2
{
protected:
  IID _iid;
  DWORD _attrib;
  ULONG _refCount;

public:
  Event(REFIID iid, DWORD attrib);
  virtual ~Event();

  HRESULT SendEvent(IDebugEventCallback2 *pCallback, 
    IDebugEngine2 *pEngine, IDebugProgram2 *pProgram,
    IDebugThread2 *pThread);

  // IUnknown methods
  STDMETHOD_(ULONG, AddRef)();
  STDMETHOD_(ULONG, Release)();
  STDMETHOD(QueryInterface)(REFIID iid, LPVOID *obj);

  // IDebugEvent2 methods
  STDMETHOD(GetAttributes)(DWORD* attrib);
};

// special events
#define DECLARE_EVENT_IUNKNOWN \
  STDMETHOD_(ULONG, AddRef)(); \
  STDMETHOD_(ULONG, Release)(); \
  STDMETHOD(QueryInterface)(REFIID iid, LPVOID *obj);

class CEngineCreateEvent : 
  public Event,
  public IDebugEngineCreateEvent2 
{
protected:
  CComPtr<IDebugEngine2> _engine;

public:
  CEngineCreateEvent(IDebugEngine2 *pEngine);
  virtual ~CEngineCreateEvent();

  // IUnknown methods
  DECLARE_EVENT_IUNKNOWN

  // IDebugEngineCreateEvent2 methods
  STDMETHOD(GetEngine)(IDebugEngine2 **ppEngine);
};

class CProgramCreateEvent : 
  public Event,
  public IDebugProgramCreateEvent2 
{
public:
  CProgramCreateEvent();
  virtual ~CProgramCreateEvent();

  // IUnknown methods
  DECLARE_EVENT_IUNKNOWN
};

class CLoadCompleteEvent : 
  public Event,
  public IDebugLoadCompleteEvent2 
{
public:
  CLoadCompleteEvent();
  virtual ~CLoadCompleteEvent();

  // IUnknown methods
  DECLARE_EVENT_IUNKNOWN
};

class CEntryPointEvent : 
  public Event,
  public IDebugEntryPointEvent2 
{
public:
  CEntryPointEvent();
  virtual ~CEntryPointEvent();

  // IUnknown methods
  DECLARE_EVENT_IUNKNOWN
};

class CBreakpointBoundEvent :
  public Event,
  public IDebugBreakpointBoundEvent2 
{
protected:
  CComPtr<IEnumDebugBoundBreakpoints2> _enum;
  CComPtr<IDebugPendingBreakpoint2> _pending;

public:
  CBreakpointBoundEvent(IEnumDebugBoundBreakpoints2 *pEnum, IDebugPendingBreakpoint2 *pPending);
  virtual ~CBreakpointBoundEvent();

  // IUnknown methods
  DECLARE_EVENT_IUNKNOWN

  //IDebugBreakpointBoundEvent2 methods
  STDMETHOD(GetPendingBreakpoint)(IDebugPendingBreakpoint2** ppPendingBP);
  STDMETHOD(EnumBoundBreakpoints)(IEnumDebugBoundBreakpoints2** ppEnum);
};

class CCodeBreakpointEvent :
  public Event,
  public IDebugBreakpointEvent2
{
protected:
  CComPtr<IDebugBoundBreakpoint2> _bp;

public:
  CCodeBreakpointEvent(IDebugBoundBreakpoint2 *pBP);
  virtual ~CCodeBreakpointEvent();

  // IUnknown methods
  DECLARE_EVENT_IUNKNOWN

  //IDebugBreakpointEvent2 methods
  STDMETHOD(EnumBreakpoints)(IEnumDebugBoundBreakpoints2 **ppEnum);
};

class CDebugBreakEvent :
  public Event,
  public IDebugBreakEvent2
{
public:
  CDebugBreakEvent();
  virtual ~CDebugBreakEvent();

  // IUnknown methods
  DECLARE_EVENT_IUNKNOWN
};

class COutputDebugStringEvent :
  public Event,
  public IDebugOutputStringEvent2
{
protected:
  CComBSTR _string;

public:
  COutputDebugStringEvent(const char *str);
  virtual ~COutputDebugStringEvent();

  // IUnknown methods
  DECLARE_EVENT_IUNKNOWN

  // IDebugOutputStringEvent2 methods
  STDMETHOD(GetString)(BSTR* pbstrString);
};