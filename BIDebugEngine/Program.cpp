// Program.cpp : Implementation of CProgram

#include "stdafx.h"
#include "Program.h"
#include "Thread.h"
#include "Breakpoint.h"
#include "Context.h"
#include "Disassembly.h"

#include "Es/Containers/staticArray.hpp"

// CProgram

CProgram::CProgram()
{
}

CProgram::~CProgram()
{
}

void CProgram::Start(IDebugEngine2 *pEngine)
{
  _engine = pEngine;

  HRESULT hr = _programPublisher.CoCreateInstance(CLSID_ProgramPublisher); 
  if (FAILED(hr)) 
  { 
    DebugLogF("Failed to create the program publisher: 0x%x.", hr);
    return; 
  } 

  hr = _programPublisher->PublishProgramNode(static_cast<IDebugProgramNode2*>(this)); 
  if (FAILED(hr)) 
  { 
    DebugLogF("Failed to publish the program node: 0x%x.", hr);
    _programPublisher.Release();
    return; 
  } 

  DebugLogF("Added program node.");

  /*
  CComPtr<IDebugMachine2> spMachine;
  HRESULT hr = CoCreateInstance(
    CLSID_MsMachineDebugManager, NULL, CLSCTX_SERVER, 
    IID_IDebugMachine2, (LPVOID *)&spMachine);
  if (FAILED(hr))
  {
    DebugLogF("Failed to create MDM: 0x%x", hr);
    return;
  }
  DebugLogF("CoCreated MDM");

  DWORD dwAuthnService ;
  DWORD dwAuthzService ;
  DWORD dwAuthnLevel ;
  DWORD dwImpLevel ;
  if(CoQueryProxyBlanket(spMachine, &dwAuthnService, &dwAuthzService, 
    NULL, &dwAuthnLevel, &dwImpLevel,NULL, NULL) == S_OK)
  {
    CoSetProxyBlanket(spMachine, dwAuthnService, dwAuthzService, 
      NULL, dwAuthnLevel, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE); 
  }

  CComPtr<IDebugPort2> spPort;
  hr = spMachine->GetPort(GUID_NULL, &spPort);
  if (FAILED(hr))
  {
    DebugLogF("Failed to Get Port: 0x%x", hr);
    return;
  }
  DebugLogF("Got Port");

  hr = spPort->QueryInterface(&_portNotify);
  if (FAILED(hr))
  {
    DebugLogF("Failed to get IDebugPortNotify2 interface: 0x%x.", hr);
    return;
  }
  DebugLogF("Got IDebugPortNotify2 interface.");

  hr =  _portNotify->AddProgramNode(this);
  if (FAILED(hr))
  {
    DebugLogF("Failed to add program node: 0x%x.", hr);
    return;
  }
  DebugLogF("Added program node.");
*/
}

void CProgram::ScriptLoaded(IDebugScript *script, const char *name)
{
  if (!script) return;

  if (!_callback)
  {
    DebugLogF("Script 0x%x (%s) loaded without debugger", script, name);
    script->AttachScript();  // do not wait for reaction
    return;
  }

  LogF("Script 0x%x (%s) loaded", script, name);

  // Create thread
  CComObject<CScript> *scr;
  CComObject<CScript>::CreateInstance(&scr);

  AddThread(scr); // AddRef is performed
  scr->Init(name, script);

  CLoadCompleteEvent *pDLCE = new CLoadCompleteEvent;
  pDLCE->SendEvent(_callback, _engine, this, scr); 
}

void CProgram::ScriptEntered(IDebugScript *script)
{
  if (!script) return;

  if (!_callback)
  {
    DebugLogF("Script 0x%x started without debugger", script);
     // do not wait for reaction
    script->EnterScript();
    script->RunScript(true);
    return;
  }

  IDebugThread2 *thread = FindThread(script);
  if (thread)
  {
    CEntryPointEvent *pDEPE = new CEntryPointEvent;
    pDEPE->SendEvent(_callback, _engine, this, thread);
    LogF("Script 0x%x started", script);
  }
  else
  {
    LogF("Script 0x%x not found (starting)", script);
  }
}

void CProgram::ScriptTerminated(IDebugScript *script)
{
  if (!script) return;

  for (int i=0; i<_threads.Size(); i++)
  {
    DWORD id;
    _threads[i]->GetThreadId(&id);
    if (id == (DWORD)script)
    {
      LogF("Script 0x%x finished", script);
      _threads.Delete(i);
      return;
    }
  }
  LogF("Script 0x%x not found (finishing)", script);
}

void CProgram::FireBreakpoint(IDebugScript *script, DWORD bp)
{
  if (!script) return;

  IDebugThread2 *thread = FindThread(script);
  IDebugBoundBreakpoint2 *breakpoint = FindBreakpoint(bp);
  if (thread && breakpoint)
  {
    // stopping event - stop all threads
    for (int i=0; i<_threads.Size(); i++)
    {
      DWORD id;
      _threads[i]->GetThreadId(&id);
      IDebugScript *script = (IDebugScript *)id;
      if (script) script->RunScript(false);
    }
    CCodeBreakpointEvent *event = new CCodeBreakpointEvent(breakpoint);
    event->SendEvent(_callback, _engine, this, thread);

    LogF("Script 0x%x - breakpoint activated", script);
  }
  else
  {
    LogF("Script 0x%x not found (breakpoint activation)", script);
  }
}

void CProgram::Breaked(IDebugScript *script)
{
  if (!script) return;

  IDebugThread2 *thread = FindThread(script);
  if (thread)
  {
    // stopping event - stop thread
    script->RunScript(false);

    CDebugBreakEvent *event = new CDebugBreakEvent;
    event->SendEvent(_callback, _engine, this, thread);

    LogF("Script 0x%x - break", script);
  }
  else
  {
    LogF("Script 0x%x not found (break)", script);
  }
}

int CProgram::AddThread(IDebugThread2 *thr)
{
  return _threads.Add(thr);
}

IDebugThread2 *CProgram::FindThread(IDebugScript *script)
{
  for (int i=0; i<_threads.Size(); i++)
  {
    DWORD id;
    _threads[i]->GetThreadId(&id);
    if (id == (DWORD)script) return _threads[i];
  }
  return NULL;
}

IDebugBoundBreakpoint2 *CProgram::FindBreakpoint(DWORD id)
{
  for (int i=0; i<_breakpoints.Size(); i++)
  {
    const BreakpointInfo &info = _breakpoints[i];
    IDebugBoundBreakpoint2 *bp = info.bp;
    if ((DWORD)bp == id) return bp;
  }
  return NULL;
}

void CProgram::SetBreakpoints(IDebugScript *script)
{
  if (!script) return;

  // set all breakpoints to new launched script
  for (int i=0; i<_breakpoints.Size(); i++)
  {
    const BreakpointInfo &info = _breakpoints[i];
    IDebugBoundBreakpoint2 *bp = info.bp;
    BP_STATE state;
    bp->GetState(&state);
    script->SetBreakpoint(info.file, info.line, (DWORD)bp, state == BPS_ENABLED);
  }
}

int CProgram::AddBreakpoint(IDebugBoundBreakpoint2 *bp, const char *file, int line)
{
  LogF("Breakpoint added to %s:%d (0x%x)", file, line, (DWORD)bp);
  // register breakpoint
  int index = _breakpoints.Add();
  _breakpoints[index].bp = bp;
  _breakpoints[index].file = file;
  _breakpoints[index].line = line;
 
  // set breakpoint to all running scripts
  BP_STATE state;
  bp->GetState(&state);
  for (int i=0; i<_threads.Size(); i++)
  {
    DWORD id;
    _threads[i]->GetThreadId(&id);
    IDebugScript *script = (IDebugScript *)id;
    if (script) script->SetBreakpoint(file, line, (DWORD)bp, state == BPS_ENABLED);
  }
  return index;
}

void CProgram::RemoveBreakpoint(IDebugBoundBreakpoint2 *bp)
{
  // remove breakpoint from all running scripts
  for (int i=0; i<_threads.Size(); i++)
  {
    DWORD id;
    _threads[i]->GetThreadId(&id);
    IDebugScript *script = (IDebugScript *)id;
    if (script) script->RemoveBreakpoint((DWORD)bp);
  }

  // unregister breakpoint
  for (int i=0; i<_breakpoints.Size(); i++)
  {
    if (_breakpoints[i].bp == bp)
    {
      _breakpoints.Delete(i);
      break;
    }
  }
}

void CProgram::EnableBreakpoint(IDebugBoundBreakpoint2 *bp, bool enable)
{
  // enable / disable breakpoint in all running scripts
  for (int i=0; i<_threads.Size(); i++)
  {
    DWORD id;
    _threads[i]->GetThreadId(&id);
    IDebugScript *script = (IDebugScript *)id;
    if (script) script->EnableBreakpoint((DWORD)bp, enable);
  }
}

void CProgram::OutputDebugString(const char *str)
{
  if (!_callback)
  {
    DebugLogF(str);
    return;
  }
  COutputDebugStringEvent *event = new COutputDebugStringEvent(str);
  event->SendEvent(_callback, _engine, this, NULL);
}

// IDebugProgramNode2 methods
HRESULT CProgram::GetProgramName(BSTR* pbstrProgramName)
{
  LogF("  - CProgram::GetProgramName");
  return E_NOTIMPL; 
}

HRESULT CProgram::GetHostName(DWORD dwHostNameType, BSTR* pbstrHostName)
{
  LogF("  - CProgram::GetHostName");
  return E_NOTIMPL; 
}

HRESULT CProgram::GetHostPid(AD_PROCESS_ID *pHostProcessId)
{
  pHostProcessId->ProcessIdType = AD_PROCESS_ID_SYSTEM;
  pHostProcessId->ProcessId.dwProcessId = GetCurrentProcessId();

  return S_OK;
}

HRESULT CProgram::GetHostMachineName_V7(BSTR* pbstrHostMachineName)
{
  LogF("  - CProgram::GetHostMachineName");
  return E_NOTIMPL; 
}

void CProgram::EngineAttach(IDebugProgram2* pMDMProgram, IDebugEventCallback2* pCallback, DWORD dwReason)
{
  LogF("  - CProgram::EngineAttach");

  _callback = pCallback;
  pMDMProgram->GetProgramId(&_progId);

  CEngineCreateEvent *pDECE = new CEngineCreateEvent(_engine);
  pDECE->SendEvent(pCallback, _engine, NULL, NULL);

  CProgramCreateEvent *pDPCE = new CProgramCreateEvent;
  pDPCE->SendEvent(pCallback, _engine, this, NULL);

  RptF("Starting BI debugger session");
}

HRESULT CProgram::Attach_V7(IDebugProgram2* pMDMProgram, IDebugEventCallback2* pCallback, DWORD dwReason)
{
  LogF("  - CProgram::Attach");
  return E_NOTIMPL; 
}

HRESULT CProgram::GetEngineInfo(BSTR* pbstrEngine, GUID* pguidEngine)
{
  if (pbstrEngine) *pbstrEngine = SysAllocString(L"BI Debug Engine");
  if (pguidEngine) *pguidEngine = __uuidof(Engine);

  return S_OK;
}

HRESULT CProgram::DetachDebugger_V7(void)
{
  LogF("  - CProgram::DetachDebugger");
  return E_NOTIMPL; 
}

// IDebugProgram2 methods

TypeIsSimple(IDebugThread2 *)

typedef CComEnumWithCount<IEnumDebugThreads2, &IID_IEnumDebugThreads2, IDebugThread2 *,  _CopyInterface<IDebugThread2> > CEnumDebugThreads;

HRESULT CProgram::EnumThreads(IEnumDebugThreads2** ppEnum)
{
  // Create the thread enumerator.
  CComObject<CEnumDebugThreads>* enumThreads;
  CComObject<CEnumDebugThreads>::CreateInstance(&enumThreads);

  int n = _threads.Size();
  AUTO_STATIC_ARRAY(IDebugThread2 *, threads, 16);
  threads.Resize(n);
  for (int i=0; i<n; i++) threads[i] = _threads[i];
  enumThreads->Init(&(threads[0]), &(threads[n]), NULL, AtlFlagCopy);

  *ppEnum = enumThreads;
  (*ppEnum)->AddRef();

  return S_OK;
}

HRESULT CProgram::GetName(BSTR* pbstrName)
{
  LogF("  - CProgram::GetName");
  return E_NOTIMPL; 
}

HRESULT CProgram::GetProcess(IDebugProcess2** ppProcess)
{
  LogF("  - CProgram::GetProcess");
  return E_NOTIMPL; 
}

HRESULT CProgram::Terminate(void)
{
  LogF("  - CProgram::Terminate");
  return E_NOTIMPL; 
}

HRESULT CProgram::Attach(IDebugEventCallback2* pCallback)
{
  LogF("  - CProgram::Attach");
  return E_NOTIMPL; 
}

HRESULT CProgram::Detach(void)
{
  LogF("  - CProgram::Detach");
  return E_NOTIMPL; 
}

HRESULT CProgram::GetProgramId(GUID* pguidProgramId)
{
  *pguidProgramId = _progId;
  return S_OK;
}

HRESULT CProgram::GetDebugProperty(IDebugProperty2** ppProperty)
{
  LogF("  - CProgram::GetDebugProperty");
  return E_NOTIMPL; 
}

HRESULT CProgram::Execute()
{
  LogF("  - CProgram::Execute");
  for (int i=0; i<_threads.Size(); i++)
  {
    DWORD id;
    _threads[i]->GetThreadId(&id);
    IDebugScript *script = (IDebugScript *)id;
    if (!script) continue;

    if (!script->IsAttached())
    {
      RptF("Error: CProgram::Execute called, script %d not attached.", id);
      script->AttachScript();
      SetBreakpoints(script);
    }
    if (!script->IsEntered())
    {
      RptF("Error: CProgram::Execute called, script %d not entered.", id);
      script->EnterScript();
    }
    script->RunScript(true);
  }
  return S_OK;
}

HRESULT CProgram::Continue(IDebugThread2 *pThread)
{
  LogF("  - CProgram::Continue");

  DWORD id;
  pThread->GetThreadId(&id);
  IDebugScript *script = (IDebugScript *)id;
  if (!script) return S_OK;

  if (!script->IsAttached())
  {
    script->AttachScript();
    SetBreakpoints(script);
  }
  else
  {
    if (!script->IsEntered())
    {
      script->EnterScript();
    }
    script->RunScript(true);
  }
  return S_OK;
}

HRESULT CProgram::Step(IDebugThread2 *pThread, STEPKIND sk, STEPUNIT step)
{
  LogF("  - CProgram::Step");

  DWORD id;
  pThread->GetThreadId(&id);
  IDebugScript *script = (IDebugScript *)id;
  if (!script) return S_OK;

  if (!script->IsAttached())
  {
    script->AttachScript();
    SetBreakpoints(script);
  }
  else
  {
    if (!script->IsEntered())
    {
      script->EnterScript();
    }
    StepKind kind;
    StepUnit unit;
    switch (sk)
    {
    case STEP_INTO:
      kind = SKInto; break;
    case STEP_OVER:
      kind = SKOver; break;
    case STEP_OUT:
      kind = SKOut; break;
    default:
      kind = SKInto; RptF("Step kind %d not supported", sk); break;
    }
    switch (step)
    {
    case STEP_STATEMENT:
      unit = SUStatement; break;
    case STEP_LINE:
      unit = SULine; break;
    case STEP_INSTRUCTION:
      unit = SUInstruction; break;
    default:
      unit = SUStatement; RptF("Step unit %d not supported", step); break;
    }
    script->Step(kind, unit);
  }
  return S_OK;
}

HRESULT CProgram::CauseBreak()
{
  LogF("  - CProgram::CauseBreak");

  for (int i=0; i<_threads.Size(); i++)
  {
    DWORD id;
    _threads[i]->GetThreadId(&id);
    IDebugScript *script = (IDebugScript *)id;
    if (script) script->RunScript(false);
  }

  CDebugBreakEvent *event = new CDebugBreakEvent;
  event->SendEvent(_callback, _engine, this, NULL);

  return S_OK;
}

typedef CComEnumWithCount<IEnumDebugCodeContexts2, &IID_IEnumDebugCodeContexts2, IDebugCodeContext2 *,  _CopyInterface<IDebugCodeContext2> > CEnumDebugCodeContext;

HRESULT CProgram::EnumCodeContexts(IDebugDocumentPosition2* pDocPos, IEnumDebugCodeContexts2** ppEnum)
{
  LogF("  - CProgram::EnumCodeContexts");

  // Create context
  CComObject<CDebugContext> *context;
  CComObject<CDebugContext>::CreateInstance(&context);
  context->AddRef();
  BSTR filename;
  pDocPos->GetFileName(&filename);
  TEXT_POSITION pos;
  pDocPos->GetRange(&pos, NULL);
  context->Initialize(filename, pos);

  // Create enumerator
  CComObject<CEnumDebugCodeContext>* enumerator;
  CComObject<CEnumDebugCodeContext>::CreateInstance(&enumerator);

  IDebugCodeContext2 *ctx = context;
  enumerator->Init(&ctx, &ctx, NULL, AtlFlagCopy);

  *ppEnum = enumerator;
  (*ppEnum)->AddRef();

  context->Release();

  return S_OK;
}

HRESULT CProgram::GetMemoryBytes(IDebugMemoryBytes2** ppMemoryBytes)
{
  LogF("  - CProgram::GetMemoryBytes");
  return E_NOTIMPL; 
}

HRESULT CProgram::GetDisassemblyStream(DISASSEMBLY_STREAM_SCOPE dwScope, IDebugCodeContext2* pCodeContext, IDebugDisassemblyStream2** ppDisassemblyStream)
{
  LogF("  - CProgram::GetDisassemblyStream");
  LogF("    stream scope 0x%x", dwScope);
  IDebugDocumentContext2 *docCtx = NULL;
  pCodeContext->GetDocumentContext(&docCtx);
  BSTR filename;
  docCtx->GetName(GN_FILENAME, &filename);
  TEXT_POSITION pos;
  docCtx->GetSourceRange(&pos, NULL);
  USES_CONVERSION;
  LogF("    context %s : %d", OLE2CT(filename), pos.dwLine + 1);
  
  CComObject<CDisassemblyStream> *stream;
  CComObject<CDisassemblyStream>::CreateInstance(&stream);
  stream->AddRef();

  *ppDisassemblyStream = stream;
  return S_OK; 
}

HRESULT CProgram::EnumModules(IEnumDebugModules2** ppEnum)
{
  LogF("  - CProgram::EnumModules");
  return E_NOTIMPL; 
}

HRESULT CProgram::GetENCUpdate(IDebugENCUpdate** ppUpdate)
{
  LogF("  - CProgram::GetENCUpdate");
  return E_NOTIMPL; 
}

class CopyCodePaths
{
public:
  static HRESULT copy(CODE_PATH *p1, CODE_PATH *p2)
  {
    if (p2->bstrName) p1->bstrName = SysAllocString(p2->bstrName);
    if (p2->pCode)
    {
      p1->pCode = p2->pCode;
      p1->pCode->AddRef();
    }
    return S_OK;
  }
  static void init(CODE_PATH *p)
  {
    memset(p, 0, sizeof(CODE_PATH));
  }
  static void destroy(CODE_PATH *p)
  {
    if (p->bstrName) SysFreeString(p->bstrName);
    if (p->pCode) p->pCode->Release();
  }
};

typedef CComEnumWithCount<IEnumCodePaths2, &IID_IEnumCodePaths2, CODE_PATH, CopyCodePaths> CEnumCodePaths;

HRESULT CProgram::EnumCodePaths(LPCOLESTR pszHint, IDebugCodeContext2* pStart, IDebugStackFrame2* pFrame, BOOL fSource, IEnumCodePaths2** ppEnum, IDebugCodeContext2** ppSafety)
{
  LogF("  - CProgram::EnumCodePaths");

  // decode filename
  IDebugDocumentContext2 *docCtx = NULL;
  pStart->GetDocumentContext(&docCtx);
  BSTR filename;
  docCtx->GetName(GN_FILENAME, &filename);

  // create code path
  CODE_PATH path;
  path.bstrName = SysAllocString(filename);
  path.pCode = pStart;
  path.pCode->AddRef();

  // Create enumerator
  CComObject<CEnumCodePaths>* enumerator;
  CComObject<CEnumCodePaths>::CreateInstance(&enumerator);

  enumerator->Init(&path, &path, NULL, AtlFlagCopy);

  *ppEnum = enumerator;
  (*ppEnum)->AddRef();

  *ppSafety = pStart;
  (*ppSafety)->AddRef();

  return S_OK;
}

HRESULT CProgram::WriteDump(DUMPTYPE dumptype, LPCOLESTR pszCrashDumpUrl)
{
  LogF("  - CProgram::WriteDump");
  return E_NOTIMPL; 
}

HRESULT CProgram::CanDetach(void)
{
  LogF("  - CProgram::CanDetach");
  return E_NOTIMPL; 
}

HRESULT CProgram::CreatePendingBreakpoint(IDebugBreakpointRequest2 *pBPRequest, IDebugPendingBreakpoint2** ppPendingBP)
{
  CComObject<CPendingBreakpoint> *pPending;
  CComObject<CPendingBreakpoint>::CreateInstance(&pPending);
  pPending->AddRef();
  pPending->Initialize(pBPRequest, _callback, _engine);
  pPending->QueryInterface(ppPendingBP);
  pPending->Release();
  return S_OK;
}
