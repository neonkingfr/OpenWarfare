// Context.cpp : Implementation of CDebugContext

#include "stdafx.h"
#include "Context.h"


// CDebugContext

void CDebugContext::Initialize(LPCWSTR pszFileName, TEXT_POSITION& pos)
{
  _fileName = pszFileName;
  memcpy(&_pos, &pos, sizeof(TEXT_POSITION));
}

// IDebugDocumentContext2 methods
HRESULT CDebugContext::GetDocument(IDebugDocument2 **ppDocument)
{ return E_NOTIMPL; }

HRESULT CDebugContext::GetName(GETNAME_TYPE gnType, BSTR *pbstrFileName)
{
  *pbstrFileName = SysAllocString(_fileName);
  return S_OK; 
}

HRESULT CDebugContext::EnumCodeContexts(IEnumDebugCodeContexts2 **ppEnumCodeCxts)
{ return E_NOTIMPL; }
HRESULT CDebugContext::GetLanguageInfo(BSTR* pbstrLanguage, GUID* pguidLanguage)
{ return E_NOTIMPL; }

HRESULT CDebugContext::GetStatementRange(TEXT_POSITION* pBegPosition, TEXT_POSITION* pEndPosition)
{
  return GetSourceRange(pBegPosition, pEndPosition); 
}

HRESULT CDebugContext::GetSourceRange(TEXT_POSITION* pBegPosition, TEXT_POSITION* pEndPosition)
{
  if (pBegPosition)
    memcpy(pBegPosition, &_pos, sizeof(TEXT_POSITION));
  if (pEndPosition)
    memcpy(pEndPosition, &_pos, sizeof(TEXT_POSITION));
  return S_OK; 
}

HRESULT CDebugContext::Compare(DOCCONTEXT_COMPARE compare, IDebugDocumentContext2** rgpDocContextSet, DWORD dwDocContextSetLen, DWORD* pdwDocContext)
{ return E_NOTIMPL; }
HRESULT CDebugContext::Seek(int ncount, IDebugDocumentContext2 **ppDocContext)
{ return E_NOTIMPL; }

// IDebugMemoryContext2 methods
HRESULT CDebugContext::GetName(BSTR* pbstrName)
{ return E_NOTIMPL; }
HRESULT CDebugContext::GetInfo(CONTEXT_INFO_FIELDS dwFields, CONTEXT_INFO* pInfo)
{ return E_NOTIMPL; }
HRESULT CDebugContext::Add(UINT64 dwCount, IDebugMemoryContext2** ppMemCxt)
{ return E_NOTIMPL; }
HRESULT CDebugContext::Subtract(UINT64 dwCount, IDebugMemoryContext2** ppMemCxt)
{ return E_NOTIMPL; }
HRESULT CDebugContext::Compare(CONTEXT_COMPARE compare, IDebugMemoryContext2** rgpMemoryContextSet, DWORD dwMemoryContextSetLen, DWORD* pdwMemoryContext)
{ return E_NOTIMPL; }

// IDebugCodeContext2 methods
HRESULT CDebugContext::GetDocumentContext(IDebugDocumentContext2 **ppSrcCxt)
{
  *ppSrcCxt = (IDebugDocumentContext2 *)this;
  (*ppSrcCxt)->AddRef();

  return S_OK; 
}


// CBreakpointResolution

CBreakpointResolution::CBreakpointResolution()
{
  memset(&_info, 0, sizeof(BP_RESOLUTION_INFO));

  _info.bpResLocation.bpType = BPT_CODE;
  _info.dwFields = BPRESI_BPRESLOCATION;
}

CBreakpointResolution::~CBreakpointResolution()
{
  _info.bpResLocation.bpResLocation.bpresCode.pCodeContext->Release();
}

void CBreakpointResolution::Initialize(IDebugCodeContext2 *pCodeContext)
{
  _info.bpResLocation.bpResLocation.bpresCode.pCodeContext = pCodeContext;
  _info.bpResLocation.bpResLocation.bpresCode.pCodeContext->AddRef();
}

// IDebugBreakpointResolution2

HRESULT CBreakpointResolution::GetBreakpointType(BP_TYPE* pBPType)
{
  return BPT_CODE;
}

HRESULT CBreakpointResolution::GetResolutionInfo(BPRESI_FIELDS dwFields, BP_RESOLUTION_INFO* pBPResolutionInfo)
{
  // Start with a raw copy.
  memcpy(pBPResolutionInfo, &_info, sizeof(BP_RESOLUTION_INFO));

  // Set the fields.
  pBPResolutionInfo->dwFields = dwFields & _info.dwFields;

  // Fill in the bp resolution destination.
  if (pBPResolutionInfo->dwFields & BPRESI_BPRESLOCATION)
  {
    pBPResolutionInfo->bpResLocation.bpResLocation.bpresCode.pCodeContext->AddRef();
    return S_OK;
  }

  return E_NOTIMPL; 
}
