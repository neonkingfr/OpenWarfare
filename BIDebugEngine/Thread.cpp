// Thread.cpp : Implementation of CScript

#include "stdafx.h"
#include "Thread.h"
#include "Frame.h"
#include "Context.h"

// CScript

CScript::~CScript()
{
  if (_name) SysFreeString(_name);
}

void CScript::Init(const char *name, IDebugScript *script)
{
  USES_CONVERSION;
  _name = SysAllocString(CT2OLE(name));
  _script = script;
}

//IDebugThread2 methods

HRESULT CScript::EnumFrameInfo(FRAMEINFO_FLAGS dwFieldSpec, UINT nRadix, IEnumDebugFrameInfo2 **ppEnum)
{
  // Create stack frames.
  AutoArray<FRAMEINFO> frames;
  
  int n = 0;
  if (_script)
  {
    dwFieldSpec &= FIF_LANGUAGE | FIF_FUNCNAME | FIF_STACKRANGE | FIF_FRAME | FIF_FUNCNAME_MODULE | FIF_FUNCNAME_LINES;
    CComBSTR lang = L"BI Scripting Language";

    IDebugScope *scope = NULL;
    _script->GetScriptContext(scope);

    while (scope)
    {
      frames.Add();
      FRAMEINFO &frame = frames[n++];
      CopyFrameInfo::init(&frame);
      frame.m_dwValidFields = dwFieldSpec;

      char file[256];
      int line;
      scope->GetDocumentPos(file, 256, line);

      CComBSTR sbstrDoc;
      USES_CONVERSION;
      sbstrDoc = CT2OLE(file);
      TEXT_POSITION posBeg;
      posBeg.dwLine = line;
      posBeg.dwColumn = 0;

      if (dwFieldSpec & FIF_FRAME)
      {
        CComObject<CFrame> *pFrame;
        CComObject<CFrame>::CreateInstance(&pFrame);
        pFrame->AddRef();
        frame.m_pFrame = (IDebugStackFrame2 *)pFrame;

        CComObject<CDebugContext> *pContext;
        CComObject<CDebugContext>::CreateInstance(&pContext);
        pContext->AddRef();
        pContext->Initialize(sbstrDoc, posBeg);

        pFrame->Init(pContext, scope);
        pContext->Release();
      }

      if (dwFieldSpec & FIF_LANGUAGE)
      {
        frame.m_bstrLanguage = SysAllocString(lang);
      }
      if (dwFieldSpec & FIF_FUNCNAME)
      {
        BString<512> name;
        strcpy(name, scope->GetName());
        if (dwFieldSpec & FIF_FUNCNAME_MODULE)
        {
          BString<512> temp;
          sprintf(temp, " File %s", file);
          strcat(name, temp);
        }
        if (dwFieldSpec & FIF_FUNCNAME_LINES)
        {
          BString<512> temp;
          sprintf(temp, " Line %d", line + 1);
          strcat(name, temp);
        }

        frame.m_bstrFuncName = SysAllocString(CT2OLE(name));
      }

      scope = scope->GetParentScope(); // next scope
    }
  }

  // Create and initialize enumerator.
  CComObject<CEnumFrameInfo> *pEnum;
  CComObject<CEnumFrameInfo>::CreateInstance(&pEnum);
  pEnum->AddRef();
  pEnum->Init(&(frames[0]), &(frames[n]), NULL, AtlFlagCopy);

  // Free frame resources.
  for (int i=0; i<n; i++)
  {
    FRAMEINFO &frame = frames[i];
    CopyFrameInfo::destroy(&frame);
  }
  frames.Clear();

  *ppEnum = pEnum;

  return S_OK;
}

HRESULT CScript::GetName(BSTR* pbstrName)
{
  *pbstrName = SysAllocString(_name);
  return S_OK; 
}

HRESULT CScript::SetThreadName(LPCOLESTR pszName)
{
  LogF("  - CScript::SetThreadName");
  return E_NOTIMPL; 
}

HRESULT CScript::GetProgram(IDebugProgram2** ppProgram)
{
  LogF("  - CScript::GetProgram");
  return E_NOTIMPL; 
}

HRESULT CScript::CanSetNextStatement(IDebugStackFrame2 *pStackFrame, IDebugCodeContext2 *pCodeContext)
{
  LogF("  - CScript::CanSetNextStatement");
  return E_NOTIMPL; 
}

HRESULT CScript::SetNextStatement(IDebugStackFrame2 *pStackFrame, IDebugCodeContext2 *pCodeContext)
{
  LogF("  - CScript::SetNextStatement");
  return E_NOTIMPL; 
}

HRESULT CScript::GetThreadId(DWORD* pdwThreadId)
{
  *pdwThreadId = (DWORD)_script.GetRef();
  return S_OK;
}

HRESULT CScript::Suspend(DWORD *pdwSuspendCount)
{
  LogF("  - CScript::Suspend");
  return E_NOTIMPL; 
}

HRESULT CScript::Resume(DWORD *pdwSuspendCount)
{
  LogF("  - CScript::Resume");
  return E_NOTIMPL; 
}

HRESULT CScript::GetThreadProperties(THREADPROPERTY_FIELDS dwFields, THREADPROPERTIES *ptp)
{
  // Check for valid argument.  
  if (!ptp) return E_INVALIDARG;

  // Create an array of buffers at ptp the size of the THREADPROPERTIES structure and set  
  // all of the buffers at ptp to 0.  
  memset(ptp, 0, sizeof(THREADPROPERTIES));  

  // Check if there is a valid THREADPROPERTY_FIELDS and the TPF_ID flag is set.  
  if (dwFields & TPF_ID)  
  {  
    // Check for successful assignment of the current thread ID to the dwThreadId of the  
    // passed THREADPROPERTIES.  
    if (GetThreadId(&(ptp->dwThreadId)) == S_OK)  
    {  
      // Set the TPF_ID flag in the THREADPROPERTY_FIELDS enumerator  
      // of the passed THREADPROPERTIES.  
      ptp->dwFields |= TPF_ID;  
    }  
  }  
  if (dwFields & TPF_NAME)  
  {  
    // Check for successful assignment of the current thread ID to the dwThreadId of the  
    // passed THREADPROPERTIES.  
    if (GetName(&(ptp->bstrName)) == S_OK)  
    {  
      // Set the TPF_ID flag in the THREADPROPERTY_FIELDS enumerator  
      // of the passed THREADPROPERTIES.  
      ptp->dwFields |= TPF_NAME;  
    }  
  }  

  return S_OK;  
}

HRESULT CScript::GetLogicalThread(IDebugStackFrame2 *pStackFrame, IDebugLogicalThread2 **ppLogicalThread)
{
  LogF("  - CScript::GetLogicalThread");
  return E_NOTIMPL; 
}
