// Engine.cpp : Implementation of CEngine

#include "stdafx.h"
#include "Engine.h"
#include "Program.h"

// CEngine

CEngine::CEngine()
{
  _syncEventSent = CreateEvent(NULL, FALSE, FALSE, NULL);
}

CEngine::~CEngine()
{
  if (_syncEventSent) CloseHandle(_syncEventSent);
}

void CEngine::Start(CComObject<CProgram> *pProgram)
{
  _program = pProgram;
}

void CEngine::WaitForSyncEvent()
{
  if (_syncEventSent) WaitForSingleObject(_syncEventSent, INFINITE);
}

//IDebugEngine2 methods
HRESULT CEngine::EnumPrograms(IEnumDebugPrograms2** ppEnum)
{
  LogF("  - CEngine::EnumPrograms");
  return E_NOTIMPL; 
}

HRESULT CEngine::Attach(IDebugProgram2** rgpPrograms, 
                        IDebugProgramNode2** rgpProgramNodes, DWORD celtPrograms,
                        IDebugEventCallback2* pCallback, ATTACH_REASON dwReason)
{
  LogF("  - CEngine::Attach");

  for (DWORD i=0; i<celtPrograms; i++) 
  { 
    CProgram *program = NULL; 
    if (rgpProgramNodes[i] != NULL && rgpProgramNodes[i]->QueryInterface(IID_BIProgram, (void**)&program) == S_OK) 
    { 
      program->EngineAttach(rgpPrograms[i], pCallback, dwReason); 
      program->Release(); 
    } 
  } 

  return S_OK; 
}

HRESULT CEngine::CreatePendingBreakpoint(IDebugBreakpointRequest2 *pBPRequest,  IDebugPendingBreakpoint2** ppPendingBP)
{
  // redirect to program
  return _program->CreatePendingBreakpoint(pBPRequest, ppPendingBP);
}

HRESULT CEngine::SetException(EXCEPTION_INFO* pException)
{
  LogF("  - CEngine::SetException");
  return E_NOTIMPL; 
}

HRESULT CEngine::RemoveSetException(EXCEPTION_INFO* pException)
{
  LogF("  - CEngine::RemoveSetException");
  return E_NOTIMPL; 
}

HRESULT CEngine::RemoveAllSetExceptions(REFGUID guidType)
{
  LogF("  - CEngine::RemoveAllSetExceptions");
  return E_NOTIMPL; 
}

HRESULT CEngine::GetEngineId(GUID *pguidEngine)
{
  LogF("  - CEngine::GetEngineId");
  return E_NOTIMPL; 
}

HRESULT CEngine::DestroyProgram(IDebugProgram2* pProgram)
{
  LogF("  - CEngine::DestroyProgram");
  return E_NOTIMPL; 
}

HRESULT CEngine::ContinueFromSynchronousEvent(IDebugEvent2* pEvent)
{
  // resume
  if (_syncEventSent) SetEvent(_syncEventSent);
  return S_OK; 
}

HRESULT CEngine::SetLocale(WORD wLangID)
{
  LogF("  - CEngine::SetLocale");
  return E_NOTIMPL; 
}

HRESULT CEngine::SetRegistryRoot(LPCOLESTR pszRegistryRoot)
{
  LogF("  - CEngine::SetRegistryRoot");
  return E_NOTIMPL; 
}

HRESULT CEngine::SetMetric(LPCOLESTR pszMetric, VARIANT varValue)
{
  LogF("  - CEngine::SetMetric");
  return E_NOTIMPL; 
}

HRESULT CEngine::CauseBreak()
{
  // redirect to program
  return _program->CauseBreak();
}
