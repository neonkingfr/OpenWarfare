// Thread.h : Declaration of the CScript

#pragma once
#include "resource.h"       // main symbols

#include "BIDebugEngine.h"

// CScript

class ATL_NO_VTABLE CScript : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CScript, &CLSID_Script>,
	public IDebugThread2
{
protected:
  CComBSTR _name;
  IDebugScriptRef _script;

public:
	CScript()
	{
	}

  virtual ~CScript();

  void Init(const char *name, IDebugScript *script);

DECLARE_REGISTRY_RESOURCEID(IDR_SCRIPT)


BEGIN_COM_MAP(CScript)
	COM_INTERFACE_ENTRY(IDebugThread2)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  // IDebugThread2
  STDMETHOD(EnumFrameInfo)(FRAMEINFO_FLAGS dwFieldSpec, UINT nRadix, IEnumDebugFrameInfo2 **ppEnum);
  STDMETHOD(GetName)(BSTR* pbstrName);
  STDMETHOD(SetThreadName)(LPCOLESTR pszName);
  STDMETHOD(GetProgram)(IDebugProgram2** ppProgram);
  STDMETHOD(CanSetNextStatement)(IDebugStackFrame2 *pStackFrame, IDebugCodeContext2 *pCodeContext);
  STDMETHOD(SetNextStatement) (IDebugStackFrame2 *pStackFrame, IDebugCodeContext2 *pCodeContext);
  STDMETHOD(GetThreadId)(DWORD* pdwThreadId);
  STDMETHOD(Suspend)(DWORD *pdwSuspendCount);
  STDMETHOD(Resume)(DWORD *pdwSuspendCount);
  STDMETHOD(GetThreadProperties)(THREADPROPERTY_FIELDS dwFields, THREADPROPERTIES *ptp);
  STDMETHOD(GetLogicalThread)(IDebugStackFrame2 *pStackFrame, IDebugLogicalThread2 **ppLogicalThread);
};

OBJECT_ENTRY_AUTO(__uuidof(Script), CScript)
