// Disassembly.h : Declaration of the CDisassemblyStream

#pragma once
#include "resource.h"       // main symbols

#include "BIDebugEngine.h"


// CDisassemblyStream

class ATL_NO_VTABLE CDisassemblyStream : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CDisassemblyStream, &CLSID_DisassemblyStream>,
	public IDebugDisassemblyStream2
{
public:
	CDisassemblyStream()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_DISASSEMBLYSTREAM)


BEGIN_COM_MAP(CDisassemblyStream)
	COM_INTERFACE_ENTRY(IDebugDisassemblyStream2)
END_COM_MAP()

  // IDebugDisassemblyStream2 implementation
  STDMETHOD(Read)(DWORD dwInstructions, DISASSEMBLY_STREAM_FIELDS dwFields,
    DWORD* pdwInstructionsRead, DisassemblyData* prgDisassembly);
  STDMETHOD(Seek)(SEEK_START dwSeekStart, IDebugCodeContext2* pCodeContext,
      UINT64 uCodeLocationId, INT64 iInstructions);
  STDMETHOD(GetCodeLocationId)(IDebugCodeContext2* pCodeContext, UINT64 *puCodeLocationId);
  STDMETHOD(GetCodeContext)(UINT64 uCodeLocationId, IDebugCodeContext2** ppCodeContext);
  STDMETHOD(GetCurrentLocation)(UINT64* puCodeLocationId);
  STDMETHOD(GetDocument)(BSTR bstrDocumentUrl, IDebugDocument2** ppDocument);
  STDMETHOD(GetScope)(DISASSEMBLY_STREAM_SCOPE* pdwScope);
  STDMETHOD(GetSize)(UINT64* pnSize);

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:

};

OBJECT_ENTRY_AUTO(__uuidof(DisassemblyStream), CDisassemblyStream)
