// Expression.cpp : Implementation of CExpressionContext

#include "stdafx.h"
#include "Expression.h"

#include "Es/Containers/staticArray.hpp"
#include "Es/Strings/bstring.hpp"

// CExpressionContext

void CExpressionContext::Init(IDebugScope *scope)
{
  _scope = scope;
}

// IDebugExpressionContext2 implementation

HRESULT CExpressionContext::GetName(BSTR *pbstrName)
{
  LogF("  - CExpressionContext::GetName");
  return E_NOTIMPL; 
}

HRESULT CExpressionContext::ParseText(LPCOLESTR pszCode, PARSEFLAGS dwFlags, UINT nRadix, IDebugExpression2 **ppExpr,
  BSTR *pbstrError, UINT *pichError)
{
  CComObject<CExpression>* expr;
  CComObject<CExpression>::CreateInstance(&expr);
  expr->AddRef();
  // TODO: Check expression, set pbstrError, pichError
  expr->Init(_scope, pszCode, nRadix);
  *ppExpr = expr;
  return S_OK; 
}


// CExpression

void CExpression::Init(IDebugScope *scope, LPCOLESTR pszCode, UINT nRadix)
{
  _scope = scope;
  _code = pszCode;
  _radix = nRadix;
}

// IDebugExpression2

HRESULT CExpression::EvaluateAsync(EVALFLAGS dwFlags, IDebugEventCallback2 *pExprCallback)
{
  LogF("  - CExpression::EvaluateAsync");
  return E_NOTIMPL; 
}

HRESULT CExpression::Abort()
{
  LogF("  - CExpression::Abort");
  return E_NOTIMPL; 
}

HRESULT CExpression::EvaluateSync(EVALFLAGS dwFlags, DWORD dwTimeout, IDebugEventCallback2 *pExprCallback, IDebugProperty2 **ppResult)
{
  USES_CONVERSION;
  IDebugValueRef value = _scope->EvaluateExpression(OLE2CT(_code), _radix);
  CComObject<CDebugProperty> *result;
  CComObject<CDebugProperty>::CreateInstance(&result);
  result->AddRef();
  result->Init(_code, value);
  *ppResult = result;
  return S_OK; 
}

// CDebugProperty

void CDebugProperty::Init(const CComBSTR &name, IDebugValueRef value)
{
  _name = name;
  _value = value;
}

// IDebugProperty2 implementation

HRESULT CDebugProperty::GetPropertyInfo(DEBUGPROP_INFO_FLAGS dwFields, DWORD nRadix, DWORD dwTimeout,
                                        IDebugReference2** rgpArgs, DWORD dwArgCount, DEBUG_PROPERTY_INFO* pPropertyInfo)
{
  memset(pPropertyInfo, 0, sizeof(DEBUG_PROPERTY_INFO));

  USES_CONVERSION;
  if (dwFields & DEBUGPROP_INFO_FULLNAME)
  {
    pPropertyInfo->bstrFullName = SysAllocString(_name);
    pPropertyInfo->dwFields |= DEBUGPROP_INFO_FULLNAME;
  }
  if (dwFields & DEBUGPROP_INFO_NAME)
  {
    pPropertyInfo->bstrName = SysAllocString(_name);
    pPropertyInfo->dwFields |= DEBUGPROP_INFO_NAME;
  }
  if (dwFields & DEBUGPROP_INFO_TYPE)
  {
    if (_value)
    {
      char buffer[512];
      _value->GetValueType(buffer, 512);
      pPropertyInfo->bstrType = SysAllocString(CT2OLE(buffer));
    }
    else
    {
      pPropertyInfo->bstrType = SysAllocString(L"Anything");
    }
    pPropertyInfo->dwFields |= DEBUGPROP_INFO_TYPE;
  }
  if (dwFields & DEBUGPROP_INFO_VALUE)
  {
    if (_value)
    {
      char buffer[512];
      _value->GetValueValue(nRadix, buffer, 512);
      pPropertyInfo->bstrValue = SysAllocString(CT2OLE(buffer));
    }
    else
    {
      pPropertyInfo->bstrValue = SysAllocString(L"<no value>");
    }
    pPropertyInfo->dwFields |= DEBUGPROP_INFO_VALUE;
  }
  if (dwFields & DEBUGPROP_INFO_ATTRIB)
  {
    bool isExpandable = _value ? _value->IsExpandable() : false;
    pPropertyInfo->dwAttrib = isExpandable ? DBG_ATTRIB_OBJ_IS_EXPANDABLE : DBG_ATTRIB_NONE;
    pPropertyInfo->dwFields |= DEBUGPROP_INFO_ATTRIB;
  }
  if (dwFields & DEBUGPROP_INFO_PROP)
  {
    pPropertyInfo->pProperty = this;
    pPropertyInfo->pProperty->AddRef();
    pPropertyInfo->dwFields |= DEBUGPROP_INFO_PROP;
  }

  return S_OK;
}

HRESULT CDebugProperty::SetValueAsString(LPCOLESTR pszValue, DWORD nRadix, DWORD dwTimeout)
{
  LogF("  - CDebugProperty::SetValueAsString");
  return E_NOTIMPL;
}

HRESULT CDebugProperty::SetValueAsReference(IDebugReference2 **rgpArgs, DWORD dwArgCount,
                                            IDebugReference2 *pValue, DWORD dwTimeout)
{
  LogF("  - CDebugProperty::SetValueAsReference");
  return E_NOTIMPL;
}

HRESULT CDebugProperty::EnumChildren(DEBUGPROP_INFO_FLAGS dwFields, DWORD dwRadix, REFGUID guidFilter,
                                     DBG_ATTRIB_FLAGS dwAttribFilter, LPCOLESTR pszNameFilter, DWORD dwTimeout,
                                     IEnumDebugPropertyInfo2** ppEnum)
{
  if (IsEqualGUID(guidFilter, guidFilterArgs))
  {
    LogF("  - CDebugProperty::EnumChildren - arguments enumeration is not implemented");
    return E_NOTIMPL; 
  }
  else if (IsEqualGUID(guidFilter, guidFilterRegisters))
  {
    LogF("  - CDebugProperty::EnumChildren - registers enumeration is not implemented");
    return E_NOTIMPL; 
  }
  else if (IsEqualGUID(guidFilter, guidFilterThis))
  {
    LogF("  - CDebugProperty::EnumChildren - this enumeration is not implemented");
    return E_NOTIMPL; 
  }

  // enumerate local variables
  if (_value)
  {
    int n = _value->NItems();

    // Create the properties enumerator.
    CComObject<CEnumDebugProperties>* enumProperties;
    CComObject<CEnumDebugProperties>::CreateInstance(&enumProperties);

    USES_CONVERSION;

    AUTO_STATIC_ARRAY(DEBUG_PROPERTY_INFO, properties, 16);
    properties.Resize(n);
    for (int i=0; i<n; i++)
    {
      IDebugValueRef value = _value->GetItem(i);

      // TODO: use dwAttribFilter
      BString<16> buffer;
      sprintf(buffer, "[%d]", i);
      CComBSTR name = CT2OLE(buffer);

      DEBUG_PROPERTY_INFO &info = properties[i];
      CComObject<CDebugProperty> *prop;
      CComObject<CDebugProperty>::CreateInstance(&prop);
      prop->AddRef();
      prop->Init(name, value);
      prop->GetPropertyInfo(dwFields, dwRadix, dwTimeout, NULL, 0, &info);
      prop->Release();
    }

    enumProperties->Init(&(properties[0]), &(properties[n]), NULL, AtlFlagCopy);

    *ppEnum = enumProperties;
    (*ppEnum)->AddRef();

    // Free resources.
    for (int i=0; i<n; i++)
    {
      DEBUG_PROPERTY_INFO &info = properties[i];
      if (info.bstrFullName) SysFreeString(info.bstrFullName);
      if (info.bstrName) SysFreeString(info.bstrName);
      if (info.bstrType) SysFreeString(info.bstrType);
      if (info.bstrValue) SysFreeString(info.bstrValue);
      if (info.pProperty) info.pProperty->Release();
    }
  }

  return S_OK;
}

HRESULT CDebugProperty::GetParent(IDebugProperty2** ppParent)
{
  LogF("  - CDebugProperty::GetParent");
  return E_NOTIMPL;
}

HRESULT CDebugProperty::GetDerivedMostProperty(IDebugProperty2** ppDerivedMost)
{
  LogF("  - CDebugProperty::GetDerivedMostProperty");
  return E_NOTIMPL;
}

HRESULT CDebugProperty::GetMemoryBytes(IDebugMemoryBytes2** ppMemoryBytes)
{
  LogF("  - CDebugProperty::GetMemoryBytes");
  return E_NOTIMPL;
}

HRESULT CDebugProperty::GetMemoryContext(IDebugMemoryContext2** ppMemory)
{
  LogF("  - CDebugProperty::GetMemoryContext");
  return E_NOTIMPL;
}

HRESULT CDebugProperty::GetSize(DWORD* pdwSize)
{
  LogF("  - CDebugProperty::GetSize");
  return E_NOTIMPL;
}

HRESULT CDebugProperty::GetReference(IDebugReference2 **ppReference)
{
  LogF("  - CDebugProperty::GetReference");
  return E_NOTIMPL;
}

HRESULT CDebugProperty::GetExtendedInfo(REFGUID guidExtendedInfo, VARIANT* pExtendedInfo)
{
  LogF("  - CDebugProperty::GetExtendedInfo");
  return E_NOTIMPL;
}
