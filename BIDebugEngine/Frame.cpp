// Frame.cpp : Implementation of CFrame

#include "stdafx.h"
#include "Frame.h"
#include "Context.h"
#include "Expression.h"

#include "Es/Containers/staticArray.hpp"

// CFrame

void CFrame::Init(IDebugDocumentContext2* pDocContext, IDebugScope *scope)
{   
  _context = pDocContext;
  _scope = scope;
}

// IDebugStackFrame2

HRESULT CFrame::GetCodeContext(IDebugCodeContext2 **ppCodeCxt)
{
  LogF("  - CFrame::GetCodeContext");
  
  CComObject<CDebugContext> *ctx;
  CComObject<CDebugContext>::CreateInstance(&ctx);
  ctx->AddRef();

  BSTR filename;
  _context->GetName(GN_FILENAME, &filename);
  TEXT_POSITION pos;
  _context->GetSourceRange(&pos, NULL);
  ctx->Initialize(filename, pos);

  *ppCodeCxt = ctx;
  return S_OK; 
}

HRESULT CFrame::GetName(BSTR* pbstrName)
{
  LogF("  - CFrame::GetName");
  return E_NOTIMPL; 
}

HRESULT CFrame::GetInfo(FRAMEINFO_FLAGS dwFieldSpec, UINT nRadix, FRAMEINFO *pFrameInfo)
{
  LogF("  - CFrame::GetInfo");
  return E_NOTIMPL; 
}

HRESULT CFrame::GetPhysicalStackRange(UINT64* paddrMin, UINT64* paddrMax)
{
  LogF("  - CFrame::GetPhysicalStackRange");
  return E_NOTIMPL; 
}

HRESULT CFrame::GetExpressionContext(IDebugExpressionContext2 **ppExprCxt)
{
  CComObject<CExpressionContext>* ctx;
  CComObject<CExpressionContext>::CreateInstance(&ctx);
  ctx->AddRef();
  ctx->Init(_scope);
  *ppExprCxt = ctx;
  return S_OK; 
}

HRESULT CFrame::GetLanguageInfo(BSTR* pbstrLanguage, GUID* pguidLanguage)
{
  LogF("  - CFrame::GetLanguageInfo");
  return E_NOTIMPL; 
}

HRESULT CFrame::GetDebugProperty(IDebugProperty2 **ppDebugProp)
{
  *ppDebugProp = this;
  (*ppDebugProp)->AddRef();
  return S_OK; 
}

HRESULT CFrame::EnumProperties(DEBUGPROP_INFO_FLAGS dwFields, UINT nRadix, REFGUID guidFilter, DWORD dwTimeout, ULONG *pcelt, IEnumDebugPropertyInfo2 **ppepi)
{
  if (IsEqualGUID(guidFilter, guidFilterArgs))
  {
    LogF("  - CFrame::EnumProperties - arguments enumeration is not implemented");
    return E_NOTIMPL; 
  }
  else if (IsEqualGUID(guidFilter, guidFilterRegisters))
  {
    LogF("  - CFrame::EnumProperties - registers enumeration is not implemented");
    return E_NOTIMPL; 
  }
  else if (IsEqualGUID(guidFilter, guidFilterThis))
  {
    LogF("  - CFrame::EnumProperties - this enumeration is not implemented");
    return E_NOTIMPL; 
  }

  // enumerate local variables
  AutoArray<const IDebugVariable *> vars;
  if (_scope)
  {
    int n = _scope->NVariables();
    vars.Realloc(n);
    vars.Resize(n);
    n = _scope->GetVariables(vars.Data(), n);

    // Create the thread enumerator.
    CComObject<CEnumDebugProperties>* enumProperties;
    CComObject<CEnumDebugProperties>::CreateInstance(&enumProperties);

    USES_CONVERSION;

    AUTO_STATIC_ARRAY(DEBUG_PROPERTY_INFO, properties, 16);
    properties.Resize(n);
    for (int i=0; i<n; i++)
    {
      char buffer[512];
      vars[i]->GetVarName(buffer, 512);
      CComBSTR name = CT2OLE(buffer);
      IDebugValueRef value = vars[i]->GetVarValue();

      DEBUG_PROPERTY_INFO &info = properties[i];
      CComObject<CDebugProperty> *prop;
      CComObject<CDebugProperty>::CreateInstance(&prop);
      prop->AddRef();
      prop->Init(name, value);
      prop->GetPropertyInfo(dwFields, nRadix, dwTimeout, NULL, 0, &info);
      prop->Release();
    }
    enumProperties->Init(&(properties[0]), &(properties[n]), NULL, AtlFlagCopy);

    *ppepi = enumProperties;
    (*ppepi)->AddRef();
    
    *pcelt = n;

    // Free resources.
    for (int i=0; i<n; i++)
    {
      DEBUG_PROPERTY_INFO &info = properties[i];
      if (info.bstrFullName) SysFreeString(info.bstrFullName);
      if (info.bstrName) SysFreeString(info.bstrName);
      if (info.bstrType) SysFreeString(info.bstrType);
      if (info.bstrValue) SysFreeString(info.bstrValue);
      if (info.pProperty) info.pProperty->Release();
    }
  }

  return S_OK;
}

HRESULT CFrame::GetThread(IDebugThread2** ppThread)
{
  LogF("  - CFrame::GetThread");
  return E_NOTIMPL; 
}

HRESULT CFrame::GetDocumentContext(IDebugDocumentContext2 **ppCxt)
{ 
  *ppCxt = _context;
  (*ppCxt)->AddRef();

  return S_OK; 
}

// IDebugProperty2 implementation

HRESULT CFrame::GetPropertyInfo(DEBUGPROP_INFO_FLAGS dwFields, DWORD nRadix, DWORD dwTimeout,
                                IDebugReference2** rgpArgs, DWORD dwArgCount, DEBUG_PROPERTY_INFO* pPropertyInfo)
{
  LogF("  - CFrame::GetPropertyInfo");
  return E_NOTIMPL;
}

HRESULT CFrame::SetValueAsString(LPCOLESTR pszValue, DWORD nRadix, DWORD dwTimeout)
{
  LogF("  - CFrame::SetValueAsString");
  return E_NOTIMPL;
}

HRESULT CFrame::SetValueAsReference(IDebugReference2 **rgpArgs, DWORD dwArgCount,
                                    IDebugReference2 *pValue, DWORD dwTimeout)
{
  LogF("  - CFrame::SetValueAsReference");
  return E_NOTIMPL;
}

HRESULT CFrame::EnumChildren(DEBUGPROP_INFO_FLAGS dwFields, DWORD dwRadix, REFGUID guidFilter,
                             DBG_ATTRIB_FLAGS dwAttribFilter, LPCOLESTR pszNameFilter, DWORD dwTimeout,
                             IEnumDebugPropertyInfo2** ppEnum)
{
  if (IsEqualGUID(guidFilter, guidFilterArgs))
  {
    LogF("  - CFrame::EnumChildren - arguments enumeration is not implemented");
    return E_NOTIMPL; 
  }
  else if (IsEqualGUID(guidFilter, guidFilterRegisters))
  {
    LogF("  - CFrame::EnumChildren - registers enumeration is not implemented");
    return E_NOTIMPL; 
  }
  else if (IsEqualGUID(guidFilter, guidFilterThis))
  {
    LogF("  - CFrame::EnumChildren - this enumeration is not implemented");
    return E_NOTIMPL; 
  }

  // enumerate local variables
  AutoArray<const IDebugVariable *> vars;
  if (_scope)
  {
    int n = _scope->NVariables();
    vars.Realloc(n);
    vars.Resize(n);
    n = _scope->GetVariables(vars.Data(), n);

    // Create the thread enumerator.
    CComObject<CEnumDebugProperties>* enumProperties;
    CComObject<CEnumDebugProperties>::CreateInstance(&enumProperties);

    USES_CONVERSION;

    AUTO_STATIC_ARRAY(DEBUG_PROPERTY_INFO, properties, 16);
    for (int i=0; i<n; i++)
    {
      // TODO: use dwAttribFilter

      // pszNameFilter
      char buffer[512];
      vars[i]->GetVarName(buffer, 512);
      CComBSTR name = CT2OLE(buffer);
      if (pszNameFilter && name != pszNameFilter) continue;

      IDebugValueRef value = vars[i]->GetVarValue();

      int index = properties.Add();
      DEBUG_PROPERTY_INFO &info = properties[index];
      CComObject<CDebugProperty> *prop;
      CComObject<CDebugProperty>::CreateInstance(&prop);
      prop->AddRef();
      prop->Init(name, value);
      prop->GetPropertyInfo(dwFields, dwRadix, dwTimeout, NULL, 0, &info);
      prop->Release();
    }
    n = properties.Size();

    enumProperties->Init(&(properties[0]), &(properties[n]), NULL, AtlFlagCopy);

    *ppEnum = enumProperties;
    (*ppEnum)->AddRef();

    // Free resources.
    for (int i=0; i<n; i++)
    {
      DEBUG_PROPERTY_INFO &info = properties[i];
      if (info.bstrFullName) SysFreeString(info.bstrFullName);
      if (info.bstrName) SysFreeString(info.bstrName);
      if (info.bstrType) SysFreeString(info.bstrType);
      if (info.bstrValue) SysFreeString(info.bstrValue);
      if (info.pProperty) info.pProperty->Release();
    }
  }

  return S_OK;
}

HRESULT CFrame::GetParent(IDebugProperty2** ppParent)
{
  LogF("  - CFrame::GetParent");
  return E_NOTIMPL;
}

HRESULT CFrame::GetDerivedMostProperty(IDebugProperty2** ppDerivedMost)
{
  LogF("  - CFrame::GetDerivedMostProperty");
  return E_NOTIMPL;
}

HRESULT CFrame::GetMemoryBytes(IDebugMemoryBytes2** ppMemoryBytes)
{
  LogF("  - CFrame::GetMemoryBytes");
  return E_NOTIMPL;
}

HRESULT CFrame::GetMemoryContext(IDebugMemoryContext2** ppMemory)
{
  LogF("  - CFrame::GetMemoryContext");
  return E_NOTIMPL;
}

HRESULT CFrame::GetSize(DWORD* pdwSize)
{
  LogF("  - CFrame::GetSize");
  return E_NOTIMPL;
}

HRESULT CFrame::GetReference(IDebugReference2 **ppReference)
{
  LogF("  - CFrame::GetReference");
  return E_NOTIMPL;
}

HRESULT CFrame::GetExtendedInfo(REFGUID guidExtendedInfo, VARIANT* pExtendedInfo)
{
  LogF("  - CFrame::GetExtendedInfo");
  return E_NOTIMPL;
}

// CEnumFrameInfo

/*

// Copy source FRAMEINFO to member FRAMEINFO.
void CEnumFrameInfo::Init(FRAMEINFO *pFrameInfo)
{
  CopyEnumElement(&_info, pFrameInfo);
}

void CEnumFrameInfo::FinalRelease()
{
  SysFreeString(_info.m_bstrLanguage);
  SysFreeString(_info.m_bstrFuncName);
  (_info.m_pFrame)->Release();
}

// IEnumDebugFrameInfo2

HRESULT CEnumFrameInfo::Skip(ULONG celt)
{ return E_NOTIMPL; }
HRESULT CEnumFrameInfo::Clone(IEnumDebugFrameInfo2** ppEnum)
{ return E_NOTIMPL; }

HRESULT CEnumFrameInfo::Reset()
{ return S_OK; }

HRESULT CEnumFrameInfo::GetCount(ULONG* pcelt)
{ 
  *pcelt = 1;
  return S_OK; 
}

// Return one frame.
HRESULT CEnumFrameInfo::Next(ULONG celt, FRAMEINFO *rgelt, ULONG* pceltFetched)
{ 
  if (celt == 0 || celt > 1)
    return E_NOTIMPL;

  CopyEnumElement(&rgelt[0], &_info);
  *pceltFetched = 1;

  return S_OK; 
}


// Copy source FRAMEINFO to destination FRAMEINFO.
void CEnumFrameInfo::CopyEnumElement(FRAMEINFO* pDest, FRAMEINFO* pSrc)
{
  // Start with rough copy.
  memcpy(pDest, pSrc, sizeof(FRAMEINFO));

  pDest->m_bstrFuncName = SysAllocString(pSrc->m_bstrFuncName);
  pDest->m_bstrLanguage = SysAllocString(pSrc->m_bstrLanguage);

  pDest->m_pFrame = pSrc->m_pFrame;
  if (pDest->m_pFrame)
    pDest->m_pFrame->AddRef();
}

*/