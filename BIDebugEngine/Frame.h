// Frame.h : Declaration of the CFrame

#pragma once
#include "resource.h"       // main symbols

#include "BIDebugEngine.h"


// CFrame

class ATL_NO_VTABLE CFrame : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CFrame, &CLSID_Frame>,
	public IDebugStackFrame2,
  public IDebugProperty2 // root of properties
{
protected:
  CComPtr<IDebugDocumentContext2> _context;
  IDebugScope *_scope;

public:
	CFrame()
	{
    _scope = NULL;
	}

  void Init(IDebugDocumentContext2* pDocContext, IDebugScope *scope);

DECLARE_REGISTRY_RESOURCEID(IDR_FRAME)


BEGIN_COM_MAP(CFrame)
	COM_INTERFACE_ENTRY(IDebugStackFrame2)
  COM_INTERFACE_ENTRY(IDebugProperty2)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  // IDebugStackFrame2
  STDMETHOD(GetCodeContext)(IDebugCodeContext2 **ppCodeCxt);
  STDMETHOD(GetDocumentContext)(IDebugDocumentContext2 **ppCxt);
  STDMETHOD(GetName)(BSTR* pbstrName);
  STDMETHOD(GetInfo)(FRAMEINFO_FLAGS dwFieldSpec, UINT nRadix, FRAMEINFO *pFrameInfo);
  STDMETHOD(GetPhysicalStackRange)(UINT64* paddrMin, UINT64* paddrMax);
  STDMETHOD(GetExpressionContext)(IDebugExpressionContext2 **ppExprCxt);
  STDMETHOD(GetLanguageInfo)(BSTR* pbstrLanguage, GUID* pguidLanguage);
  STDMETHOD(GetDebugProperty)(IDebugProperty2 **ppDebugProp);
  STDMETHOD(EnumProperties)(DEBUGPROP_INFO_FLAGS dwFields, UINT nRadix, REFGUID guidFilter, DWORD dwTimeout, ULONG *pcelt, IEnumDebugPropertyInfo2 **ppepi);
  STDMETHOD(GetThread)(IDebugThread2** ppThread);

  // IDebugProperty2
  STDMETHOD(GetPropertyInfo)(DEBUGPROP_INFO_FLAGS dwFields, DWORD nRadix, DWORD dwTimeout,
    IDebugReference2** rgpArgs, DWORD dwArgCount, DEBUG_PROPERTY_INFO* pPropertyInfo);
  STDMETHOD(SetValueAsString)(LPCOLESTR pszValue, DWORD nRadix, DWORD dwTimeout);
  STDMETHOD(SetValueAsReference)(IDebugReference2 **rgpArgs, DWORD dwArgCount,
    IDebugReference2 *pValue, DWORD dwTimeout);
  STDMETHOD(EnumChildren)(DEBUGPROP_INFO_FLAGS dwFields, DWORD dwRadix, REFGUID guidFilter,
    DBG_ATTRIB_FLAGS dwAttribFilter, LPCOLESTR pszNameFilter, DWORD dwTimeout,
    IEnumDebugPropertyInfo2** ppEnum);
  STDMETHOD(GetParent)(IDebugProperty2** ppParent);
  STDMETHOD(GetDerivedMostProperty)(IDebugProperty2** ppDerivedMost);
  STDMETHOD(GetMemoryBytes)(IDebugMemoryBytes2** ppMemoryBytes);
  STDMETHOD(GetMemoryContext)(IDebugMemoryContext2** ppMemory);
  STDMETHOD(GetSize)(DWORD* pdwSize);
  STDMETHOD(GetReference)(IDebugReference2 **ppReference);
  STDMETHOD(GetExtendedInfo)(REFGUID guidExtendedInfo, VARIANT* pExtendedInfo);
};

OBJECT_ENTRY_AUTO(__uuidof(Frame), CFrame)

class CopyFrameInfo
{
public:
  static HRESULT copy(FRAMEINFO *p1, FRAMEINFO *p2)
  {
    p1->m_dwValidFields = p2->m_dwValidFields;
    p1->m_bstrFuncName = SysAllocString(p2->m_bstrFuncName);
    p1->m_bstrReturnType = SysAllocString(p2->m_bstrReturnType);
    p1->m_bstrArgs = SysAllocString(p2->m_bstrArgs);
    p1->m_bstrLanguage = SysAllocString(p2->m_bstrLanguage);
    p1->m_bstrModule = SysAllocString(p2->m_bstrModule);
    p1->m_addrMin = p2->m_addrMin;
    p1->m_addrMax = p2->m_addrMax;
    p1->m_pFrame = p2->m_pFrame;
    if (p1->m_pFrame)
      p1->m_pFrame->AddRef();
    p1->m_pModule = p2->m_pModule;
    if (p1->m_pModule)
      p1->m_pModule->AddRef();
    p1->m_fHasDebugInfo = p2->m_fHasDebugInfo;
    p1->m_fStaleCode = p2->m_fStaleCode;
    return S_OK;
  }
  static void init(FRAMEINFO *p)
  {
    memset(p, 0, sizeof(FRAMEINFO));
  }
  static void destroy(FRAMEINFO *p)
  {
    if (p->m_bstrFuncName) SysFreeString(p->m_bstrFuncName);
    if (p->m_bstrReturnType) SysFreeString(p->m_bstrReturnType);
    if (p->m_bstrArgs) SysFreeString(p->m_bstrArgs);
    if (p->m_bstrLanguage) SysFreeString(p->m_bstrLanguage);
    if (p->m_bstrModule) SysFreeString(p->m_bstrModule);
    if (p->m_pFrame) p->m_pFrame->Release();
    if (p->m_pModule) p->m_pModule->Release();
  }
};

// CEnumFrameInfo

TypeIsMovableZeroed(FRAMEINFO)
typedef CComEnumWithCount<IEnumDebugFrameInfo2, &IID_IEnumDebugFrameInfo2, FRAMEINFO,  CopyFrameInfo > CEnumFrameInfo;

/*

class ATL_NO_VTABLE CEnumFrameInfo : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CEnumFrameInfo, &CLSID_EnumFrameInfo>,
	public IEnumDebugFrameInfo2
{
protected:
  FRAMEINFO _info;

public:
	CEnumFrameInfo()
	{
	}

  void Init(FRAMEINFO *pFrameInfo);

DECLARE_REGISTRY_RESOURCEID(IDR_ENUMFRAMEINFO)


BEGIN_COM_MAP(CEnumFrameInfo)
	COM_INTERFACE_ENTRY(IEnumDebugFrameInfo2)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease(); 

public:
  // IEnumDebugFrameInfo2
  STDMETHOD(Next)(ULONG celt, FRAMEINFO *rgelt, ULONG* pceltFetched);
  STDMETHOD(Skip)(ULONG celt);
  STDMETHOD(Reset)();
  STDMETHOD(Clone)(IEnumDebugFrameInfo2** ppEnum);
  STDMETHOD(GetCount)(ULONG* pcelt);

private:
  // implementation
  void CopyEnumElement(FRAMEINFO* pDest, FRAMEINFO* pSrc);
};

OBJECT_ENTRY_AUTO(__uuidof(EnumFrameInfo), CEnumFrameInfo)

*/