// Engine.h : Declaration of the CEngine

#pragma once
#include "resource.h"       // main symbols

#include "BIDebugEngine.h"

class CProgram;      // forward reference

// CEngine

class ATL_NO_VTABLE CEngine : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CEngine, &CLSID_Engine>,
	public IDebugEngine2
{
protected:
  HANDLE _syncEventSent;
  CComObject<CProgram> *_program; // Do not use CComPtr - avoid cyclic dependence

public:
	CEngine();
  virtual ~CEngine();

  void Start(CComObject<CProgram> *pProgram);
  void WaitForSyncEvent();

DECLARE_REGISTRY_RESOURCEID(IDR_ENGINE)

BEGIN_COM_MAP(CEngine)
	COM_INTERFACE_ENTRY(IDebugEngine2)
END_COM_MAP()


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}

public:
  //IDebugEngine2
  STDMETHOD(EnumPrograms)(IEnumDebugPrograms2** ppEnum);
  STDMETHOD(Attach)(IDebugProgram2** rgpPrograms, 
    IDebugProgramNode2** rgpProgramNodes, DWORD celtPrograms,
    IDebugEventCallback2* pCallback, ATTACH_REASON dwReason);
  STDMETHOD(CreatePendingBreakpoint)(
    IDebugBreakpointRequest2 *pBPRequest,
    IDebugPendingBreakpoint2** ppPendingBP);
  STDMETHOD(SetException)(EXCEPTION_INFO* pException);
  STDMETHOD(RemoveSetException)(EXCEPTION_INFO* pException);
  STDMETHOD(RemoveAllSetExceptions)(REFGUID guidType);
  STDMETHOD(GetEngineId)(GUID *pguidEngine);
  STDMETHOD(DestroyProgram)(IDebugProgram2* pProgram);
  STDMETHOD(ContinueFromSynchronousEvent)(IDebugEvent2* pEvent);
  STDMETHOD(SetLocale)(WORD wLangID);
  STDMETHOD(SetRegistryRoot)(LPCOLESTR pszRegistryRoot);
  STDMETHOD(SetMetric)(LPCOLESTR pszMetric, VARIANT varValue);
  STDMETHOD(CauseBreak)();
};

OBJECT_ENTRY_AUTO(__uuidof(Engine), CEngine)
