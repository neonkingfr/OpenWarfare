// BIDebugEngine.cpp : Implementation of DLL Exports.

#include "stdafx.h"
#include "resource.h"
#include "BIDebugEngine.h"

#include <Es/Strings/bstring.hpp>
#include <El/BIDebugEngine/DebugEngineInterface.hpp>

#include "Engine.h"
#include "Program.h"
#include "Breakpoint.h"
#include "Context.h"
#include "Frame.h"

class CBIDebugEngineModule : public CAtlDllModuleT< CBIDebugEngineModule >, public IToDll
{
protected:
  IToApp *_toApp;

  CComObject<CEngine> *_engine;
  CComObject<CProgram> *_program;

  bool _started;

public:
	DECLARE_LIBID(LIBID_BIDebugEngineLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_BIDEBUGENGINE, "{26E523D6-6771-498C-A3CD-721C824FC53D}")

  CBIDebugEngineModule();

  // connect to application
  IToDll *Connect(IToApp *toApp);

  // implementation of IToDll interface
  virtual void Startup();
  virtual void Shutdown();

  virtual void ScriptLoaded(IDebugScript *script, const char *name);
  virtual void ScriptEntered(IDebugScript *script);
  virtual void ScriptTerminated(IDebugScript *script);

  virtual void FireBreakpoint(IDebugScript *script, unsigned int bp);
  virtual void Breaked(IDebugScript *script);

  virtual void DebugEngineLog(const char *str);

  // access to ItoApp interface

  void DebugLogF(const char *str);

  // implementation

  void WaitForSyncEvent();

  int AddBreakpoint(IDebugBoundBreakpoint2 *bp, const char *file, int line);
  void RemoveBreakpoint(IDebugBoundBreakpoint2 *bp);
  void EnableBreakpoint(IDebugBoundBreakpoint2 *bp, bool enable);
};

CBIDebugEngineModule::CBIDebugEngineModule()
{
  _toApp = NULL;
  _engine = NULL;

  _started = false;
}

// connect to application
IToDll *CBIDebugEngineModule::Connect(IToApp *toApp)
{
  _toApp = toApp;
  return this;
}

// implementation of IToDll interface
void CBIDebugEngineModule::Startup()
{
  LogF("Debug engine Dll Startup:");

  _started = true;

  CoInitialize(NULL);

  CComObject<CEngine>::CreateInstance(&_engine);
  _engine->AddRef();

  CComObject<CProgram>::CreateInstance(&_program);
  _program->AddRef();

  _engine->Start(_program);
  _program->Start(_engine);
}

void CBIDebugEngineModule::Shutdown()
{
  if (!_started) return;

  _program->Release();
  _engine->Release();

  LogF("Debug engine Dll Shutdown:");

  CoUninitialize();
}

void CBIDebugEngineModule::ScriptLoaded(IDebugScript *script, const char *name)
{
  if (_program) _program->ScriptLoaded(script, name);
}

void CBIDebugEngineModule::ScriptEntered(IDebugScript *script)
{
  if (_program) _program->ScriptEntered(script);
}

void CBIDebugEngineModule::ScriptTerminated(IDebugScript *script)
{
  if (_program) _program->ScriptTerminated(script);
}

void CBIDebugEngineModule::FireBreakpoint(IDebugScript *script, unsigned int bp)
{
  if (_program) _program->FireBreakpoint(script, bp);
}

void CBIDebugEngineModule::Breaked(IDebugScript *script)
{
  if (_program) _program->Breaked(script);
}

void CBIDebugEngineModule::WaitForSyncEvent()
{
  if (_engine) _engine->WaitForSyncEvent();
}

int CBIDebugEngineModule::AddBreakpoint(IDebugBoundBreakpoint2 *bp, const char *file, int line)
{
  if (_program) return _program->AddBreakpoint(bp, file, line);
  return -1;
}

void CBIDebugEngineModule::RemoveBreakpoint(IDebugBoundBreakpoint2 *bp)
{
  if (_program) _program->RemoveBreakpoint(bp);
}

void CBIDebugEngineModule::EnableBreakpoint(IDebugBoundBreakpoint2 *bp, bool enable)
{
  if (_program) _program->EnableBreakpoint(bp, enable);
}

void CBIDebugEngineModule::DebugEngineLog(const char *str)
{
  if (_program) _program->OutputDebugString(str);

//  MessageBox(NULL, str, "BI Debug Engine", MB_OK);
}

// access to ItoApp interface

void CBIDebugEngineModule::DebugLogF(const char *str)
{
  if (_toApp) _toApp->DebugLogF(str);

//  MessageBox(NULL, str, "BI Debug Engine", MB_OK);
}

CBIDebugEngineModule _AtlModule;

void DebugLogF(const char *format, ...)
{
  va_list arglist;
  va_start(arglist, format);
  BString<512> buffer;
  vsprintf(buffer, format, arglist);
  _AtlModule.DebugLogF(buffer);
  va_end(arglist);
}

void LogF(const char *format, ...)
{
#if 1 // enabled only for debugging
  va_list arglist;
  va_start(arglist, format);
  BString<512> buffer;
  vsprintf(buffer, format, arglist);
  strcat(buffer, "\n");
  _AtlModule.DebugEngineLog(buffer);
  va_end(arglist);
#endif
}

void RptF(const char *format, ...)
{
  va_list arglist;
  va_start(arglist, format);
  BString<512> buffer;
  vsprintf(buffer, format, arglist);
  strcat(buffer, "\n");
  _AtlModule.DebugEngineLog(buffer);
  va_end(arglist);
}

void ErrF(const char *format, ...)
{
  va_list arglist;
  va_start(arglist, format);
  BString<512> buffer;
  vsprintf(buffer, format, arglist);
  strcat(buffer, "\n");
  _AtlModule.DebugEngineLog(buffer);
  va_end(arglist);
}

// DLL Entry Point
extern "C" BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	hInstance;
    return _AtlModule.DllMain(dwReason, lpReserved); 
}


// Used to determine whether the DLL can be unloaded by OLE
STDAPI DllCanUnloadNow(void)
{
    return _AtlModule.DllCanUnloadNow();
}


// Returns a class factory to create an object of the requested type
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _AtlModule.DllGetClassObject(rclsid, riid, ppv);
}

static const WCHAR registrationRoot[] = L"Software\\Microsoft\\VisualStudio\\8.0"; 

// DllRegisterServer - Adds entries to the system registry
STDAPI DllRegisterServer(void)
{
  SetMetric(metrictypeEngine, __uuidof(Engine), metricName, L"BI File", false, registrationRoot);
  SetMetric(metrictypeEngine, __uuidof(Engine), metricCLSID, CLSID_Engine, false, registrationRoot);
  SetMetric(metrictypeEngine, __uuidof(Engine), metricProgramProvider, CLSID_MsProgramProvider, false, registrationRoot);

  // registers object, typelib and all interfaces in typelib
  HRESULT hr = _AtlModule.DllRegisterServer();
  return hr;
}

// DllUnregisterServer - Removes entries from the system registry
STDAPI DllUnregisterServer(void)
{
  RemoveMetric(metrictypeEngine, __uuidof(Engine), metricName, registrationRoot);
  RemoveMetric(metrictypeEngine, __uuidof(Engine), metricCLSID, registrationRoot);
  RemoveMetric(metrictypeEngine, __uuidof(Engine), metricProgramProvider, registrationRoot);

  HRESULT hr = _AtlModule.DllUnregisterServer();
	return hr;
}

// Connect - exchange interfaces with application
extern "C" IToDll *Connect(IToApp *toApp)
{
  return _AtlModule.Connect(toApp);
}

void WaitForSyncEvent()
{
  _AtlModule.WaitForSyncEvent();
}

int AddBreakpoint(IDebugBoundBreakpoint2 *bp, const char *file, int line)
{
  return _AtlModule.AddBreakpoint(bp, file, line);
}

void RemoveBreakpoint(IDebugBoundBreakpoint2 *bp)
{
  _AtlModule.RemoveBreakpoint(bp);
}

void EnableBreakpoint(IDebugBoundBreakpoint2 *bp, bool enable)
{
  _AtlModule.EnableBreakpoint(bp, enable);
}
