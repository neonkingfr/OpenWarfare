#include "StdAfx.h"
#include "Console.h"

CConsole::CConsole(void)
{
  consoleOut=consoleIn=0;
}
///////////////////////////////////////////////////////////////////////////////
void CConsole::Init()
{
  if ((consoleOut==0)||(consoleIn==0))
  {
   if (AttachConsole(-1)==0)
     AllocConsole();
   consoleOut=GetStdHandle(STD_OUTPUT_HANDLE);
   consoleIn =GetStdHandle(STD_INPUT_HANDLE);
  }
}
///////////////////////////////////////////////////////////////////////////////
CConsole::~CConsole(void)
{
  FreeConsole();
}
///////////////////////////////////////////////////////////////////////////////
int CConsole::operator << (CString _text)
{
  DWORD count = operator << (_text.GetBuffer());
  _text.ReleaseBuffer();
  return count;
}
///////////////////////////////////////////////////////////////////////////////
int CConsole::operator >> (CString &_text) 
{
  TCHAR text[_MAX_PATH]; 
  DWORD count;
  ReadConsole(consoleIn,(void*)text,_MAX_PATH,&count,NULL);
  _text=CString(text);
  return count;
}
///////////////////////////////////////////////////////////////////////////////
int CConsole::operator << (TCHAR * _text) 
{
  DWORD count;
  WriteConsole(consoleOut,(void*)_text,(DWORD)_tcslen(_text),&count,NULL);
  return (int)count;
}
///////////////////////////////////////////////////////////////////////////////
int CConsole::operator >> (TCHAR * _text)
{
  DWORD count;
  ReadConsole(consoleIn,(void*)_text,_MAX_PATH,&count,NULL);
  return count;
}