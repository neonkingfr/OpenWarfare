// Packer.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "console.h"
#include "PackerAlgs.h"
#include "WinRegistry.h" 

// CPackerApp:
// See Packer.cpp for the implementation of this class
//

class CPackerApp : public CWinApp
{
public:
	CPackerApp();
  CPackerAlgs * m_packer;
// Overrides
	public:
	virtual BOOL InitInstance();
private:
  void CheckRegisters();

// Implementation

	DECLARE_MESSAGE_MAP()
};


extern CPackerApp theApp;