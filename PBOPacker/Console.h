#pragma once

class CConsole
{
public:
  CConsole(void);
  ~CConsole(void);
  int operator << (CString _text);
  int operator >> (CString &_text);
  int operator << (TCHAR * _text);
  int operator >> (TCHAR * _text); 
  void Init();
protected:
  HANDLE consoleIn,consoleOut;
};
