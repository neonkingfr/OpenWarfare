// Packer.cpp : Defines the class behaviors for the application.
//
#include "stdafx.h"
#include "Main.h"
#include "MainDlg.h"
#include "shlwapi.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define AUTODETECT_REG _T("auto")

//global
 CConsole console;

// CPackerApp

BEGIN_MESSAGE_MAP(CPackerApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()

// CPackerApp construction


CPackerApp::CPackerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CPackerApp object

  CPackerApp theApp;

// CPackerApp initialization
///////////////////////////////////////////////////////////////////////////////
ReturnPackingStat UpdatePakingStatusConsole(CString _text,bool _done,bool *_terminate)
{
  console << _text+_T("\n");
  return 0;
}
///////////////////////////////////////////////////////////////////////////////
CString ReadArg(const int i)
{
#ifdef UNICODE
    return CString(__wargv[i]);
#else
    return CString(__argv[i]);
#endif
}
///////////////////////////////////////////////////////////////////////////////
void CPackerApp::CheckRegisters()
{
#ifdef PERSONAL_EDITIONAL 
#else
#endif
  CWinRegistry winReg;

  CString armaPath = winReg.GetValue(ARMAREGPATH);
  if (_tcsicmp(armaPath,AUTODETECT_REG)==0) 
    armaPath=REG_ARMA;

  CString armaAddons=winReg.GetArmaPath(armaPath);

  TCHAR dir[_MAX_PATH];
  GetModuleFileName(0,dir,_MAX_PATH);

  TCHAR *path=dir;
  CString str;
  winReg.GetValueSTR(DEST_NAME,str);

    ExpandEnvironmentStrings (winReg.GetValue(SOURCE_NAME),path, _MAX_PATH);
  if (_tcsicmp(path,AUTODETECT_REG)==0)
	  m_packer->project.source=armaAddons;
  else
	  m_packer->project.source=path;

  if (armaAddons!=_T("")) armaAddons+=_T("\\addons");

  ExpandEnvironmentStrings (str,path, _MAX_PATH);
  if (_tcsicmp(path,AUTODETECT_REG)==0)
	  m_packer->project.dest=armaAddons;
  else
	  m_packer->project.dest=path;



  ExpandEnvironmentStrings (winReg.GetValue(TEMP_NAME),path, _MAX_PATH);
  if (_tcsicmp(path,AUTODETECT_REG)==0)
	  ExpandEnvironmentStrings(_T("%tmp%\\ARMAaddons"),path,_MAX_PATH);
  m_packer->temp.projectTemp=path;
 
  ExpandEnvironmentStrings (winReg.GetValue(PROJECT_PATH),path, _MAX_PATH);
  m_packer->project.projectPath = path;

CWinRegistry binarizeWinReg(HKEY_LOCAL_MACHINE,_T("SOFTWARE\\BIStudio\\Binarize"));
  m_packer->binarize.dir = binarizeWinReg.GetValue(_T("path"));
  m_packer->binarize.appName = binarizeWinReg.GetValue(_T("exe"));
CWinRegistry cfgConvertWinReg(HKEY_LOCAL_MACHINE,_T("SOFTWARE\\BIStudio\\CfgConvert"));
  m_packer->cfgConvert.dir = cfgConvertWinReg.GetValue(_T("path"));
  m_packer->cfgConvert.appName = cfgConvertWinReg.GetValue(_T("exe"));

CWinRegistry filebankWinReg(HKEY_LOCAL_MACHINE,_T("SOFTWARE\\BIStudio\\FileBank"));
  m_packer->filebank.dir = filebankWinReg.GetValue(_T("path"));
  m_packer->filebank.appName = filebankWinReg.GetValue(_T("exe"));
CWinRegistry dsSignFileWinReg(HKEY_LOCAL_MACHINE,_T("SOFTWARE\\BIStudio\\dsSignFile"));
  m_packer->dsSignFile.dir = dsSignFileWinReg.GetValue(_T("path"));
  m_packer->dsSignFile.appName = dsSignFileWinReg.GetValue(_T("exe"));

  m_packer->clearTemp=winReg.GetValue(CLEAR_TEMP).CompareNoCase(_T("0"))!=0;
  m_packer->autoPrefix=winReg.GetValue(USE_ADDON_PREFIX).CompareNoCase(_T("0"))!=0;
  m_packer->createSignature=winReg.GetValue(USE_SIGNATURES).CompareNoCase(_T("0"))!=0;
  m_packer->createBinarize=winReg.GetValue(FORCE_BINARIZE).CompareNoCase(_T("0"))!=0;
  m_packer->usingProject=winReg.GetValue(USE_PROJECT).CompareNoCase(_T("0"))!=0;
  m_packer->debugMode=winReg.GetValue(CREATE_LOG).CompareNoCase(_T("0"))!=0;
  m_packer->addonPrefix=winReg.GetValue(ADDON_PREFIX);
  m_packer->signatureFile=winReg.GetValue(SIGNATURE_FILE);

}
///////////////////////////////////////////////////////////////////////////////
CString ReadWhiteListFromFile(CString _fn)
{
  CString result=_T("");
  FILE *f;
  if (_tfopen_s(&f,_fn,_T("rt"))==0)
  {
    while (!feof(f))
    {
      TCHAR buf[512];
      _fgetts(buf,512,f);
      result+=CString(buf);
    }
    fclose(f);
    for (TCHAR *tch=result.GetBuffer();*tch!=_T('\0');++tch)
    {
      if (*tch==_T('\n')) *tch=_T(';');
    }
    result.ReleaseBuffer();
  }
  return result;  
}
///////////////////////////////////////////////////////////////////////////////
BOOL CPackerApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);
  
	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

  m_packer= new CPackerAlgs();

  CWinRegistry winReg;
  CheckRegisters();
  CString whiteListString = winReg.GetValue(WHITE_LIST);
  m_packer->ReadWhiteList(whiteListString);

  bool windoved = false;
  int  count = 0;
  bool packOnStartInWindow = false;
  bool projectInArgs = false,destInArgs=false;
  bool showHelp = false;
  bool doBinarize = false;
  bool autoPrefix = true;
  bool clearTemp = false;
  CString *includeFileName=NULL;

  for(int i=1;i<__argc;i++) // parse for all arguments
  {
    CString arg=ReadArg(i);
    if (arg[0]=='-')
    {
      if (arg.CompareNoCase(_T("-project"))==0)
      {
        i++;
        if (i<__argc)
        {
          m_packer->project.projectPath = ReadArg(i);
          m_packer->usingProject=true;
          projectInArgs=true;
        }
      }
      if (arg.CompareNoCase(_T("-prefix"))==0)
      {
        i++;
        if (i<__argc)
        {
          m_packer->addonPrefix = ReadArg(i);
          autoPrefix=false;
          m_packer->autoPrefix=autoPrefix;
        }
      }
      if (arg.CompareNoCase(_T("-include"))==0)
      {
        i++;
        if (i<__argc)
        {
          includeFileName= new CString();
          *includeFileName=ReadArg(i);
        }
      }
      else if (arg.CompareNoCase(_T("-temp"))==0)
      {
        i++;
        if (i<__argc) m_packer->temp.projectTemp = ReadArg(i);
      }
      else if ((arg.CompareNoCase(_T("-sign"))==0)||(arg.CompareNoCase(_T("-signature"))==0))
      {
        i++;
        if (i<__argc) {
          m_packer->signatureFile = ReadArg(i);
          if (PathFileExists(m_packer->signatureFile))m_packer->createSignature = true;
        }
      }
      else if (arg.CompareNoCase(_T("-window"))==0)
      {
        windoved=true;
        packOnStartInWindow=true;
      }
      else if (arg.CompareNoCase(_T("-clear"))==0)
      {
        clearTemp=true;
        m_packer->clearTemp=true;
      }
      else if (arg.CompareNoCase(_T("-debug"))==0)
        m_packer->debugMode=true;
      else if (arg.CompareNoCase(_T("-help"))==0)
        showHelp=true;
      else if (arg.CompareNoCase(_T("-binarize"))==0)
      {
        doBinarize=true;
        m_packer->createBinarize=doBinarize;
      }
      else if (arg.CompareNoCase(_T("-pack"))==0)
      {
        doBinarize=false;
        m_packer->createBinarize=doBinarize;
      }
    }
    else
    {
      if (count==0) 
        m_packer->project.source=arg;
      else 
      {
        m_packer->project.dest=arg;
        destInArgs=true; 
      }
      count++; 
    }
  } // end read args

  if (includeFileName!=NULL)
  {
    whiteListString = ReadWhiteListFromFile(*includeFileName);
    m_packer->ReadWhiteList(whiteListString);
  }

  if (showHelp)
  {
    console.Init();
    console << _T("\nBI's BinPBO PE\nSyntax BinPBO.exe [source_path [destination_path] [-WINDOW]] [-PACK|-BINARIZE [-CLEAR][-TEMP temp_path] ] [-DEBUG] [-PREFIX prefix_path] [-HELP] [-PROJECT project_path] [-SIGN privatekey_file] [-INCLUDE file_name]\n\n\
source_path: path to folder to make PBO addon.\n\
destination_path: path to folder to save the final PBO.\n\
If destination_path is not present then one level up from source_path is used to store the PBO file.\n\
Without any parameters, BinPBO will start in window mode.\n\n\
BinPBO.exe with source_path parameter executes a program in console mode; you can disable it by using parameter -WINDOW.\n\
Parameters:\n\
-HELP: show informations about program and parameters.\n\
-PACK: only stores the folder to a PBO.\n\
-BINARIZE: uses a temp folder and stores there compressed files, and then packs them.\n\
-CLEAR: before binarizing, the subfolder for the current project is emptied.\n\
-TEMP: path for the folder which stores binarized files, default destination is temp in Windows, if no value is present then the last folder is used, if you use the same folder, the next time you binarize the same addon it will take less time.\n\
-DEBUG: shows output from the binarizing and packing process.\n\
-PREFIX: relative path to files used in addon, if not present then this value is calculated automatically.\n\
-PROJECT: path to folder where project starts from, if you are packing only the path of a project.\n\
-SIGN: *.biprivatekey file with signature, addons created with a signature can be added to secure servers.\n\
-INCLUDE: create a new list of file-masks to direct copy to PBO from record in file, separator is \";\" ");

  }
  else if (!windoved &&count>0)
  {
    console.Init();
    console << _T("Start...\n");
    if (!destInArgs)  m_packer->project.dest = RemoveLastFolder(m_packer->project.source);
    if (!projectInArgs) m_packer->project.projectPath =m_packer->project.dest;

    m_packer->usingProject= projectInArgs;
    m_packer->createBinarize=doBinarize;
    m_packer->autoPrefix=autoPrefix;
    m_packer->clearTemp=clearTemp;

    int result;
    if ((result=m_packer->ConvertAndBinarize((ReturnPackingStat)UpdatePakingStatusConsole))==0)
      result=m_packer->PackFolder(false,(ReturnPackingStat)UpdatePakingStatusConsole);

    console << m_packer->GetErrorText((ErrorResults)result);
  }
  else
  {
	  CPackerDlg dlg;
	  m_pMainWnd = &dlg;
    dlg.packOnStart=packOnStartInWindow;
    dlg.whiteListString=whiteListString;
    dlg.packer= m_packer;

	  INT_PTR nResponse = dlg.DoModal();
	/*  if (nResponse == IDOK)
	  { }
	  else if (nResponse == IDCANCEL)
	  {}*/
    winReg.SetValueSTR(DEST_NAME,m_packer->project.dest);
    winReg.SetValueSTR(SOURCE_NAME,m_packer->project.source);
    winReg.SetValueSTR(TEMP_NAME,m_packer->temp.projectTemp);
    if (m_packer->usingProject) winReg.SetValueSTR(PROJECT_PATH,m_packer->project.projectPath);
    winReg.SetValueSTR(WHITE_LIST,dlg.whiteListString);
    winReg.SetValueSTR(ADDON_PREFIX,m_packer->addonPrefix);
    winReg.SetValueSTR(SIGNATURE_FILE,m_packer->signatureFile);

    winReg.SetValueSTR(USE_SIGNATURES,m_packer->createSignature?_T("1"):_T("0"));
    winReg.SetValueSTR(FORCE_BINARIZE,m_packer->createBinarize?_T("1"):_T("0"));
    winReg.SetValueSTR(CLEAR_TEMP,m_packer->clearTemp?_T("1"):_T("0")); 
    winReg.SetValueSTR(USE_ADDON_PREFIX,m_packer->autoPrefix?_T("1"):_T("0"));
    winReg.SetValueSTR(USE_PROJECT,m_packer->usingProject?_T("1"):_T("0"));
    winReg.SetValueSTR(CREATE_LOG,m_packer->debugMode?_T("1"):_T("0"));
  }
	// Since the dialog has been closed, return FALSE so that we exit the
	// application, rather than start the application's message pump.
	return FALSE;
}

