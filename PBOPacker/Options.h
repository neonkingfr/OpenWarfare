#pragma once


// COptions dialog

class COptions : public CDialog
{
	DECLARE_DYNAMIC(COptions)

public:
	COptions(CWnd* pParent = NULL);   // standard constructor
	virtual ~COptions();
  bool projectEnable,autoPrefix;
  CString tempPath,projectPath,sourcePath,whiteList,addonPrefix;
  CString signatureFile;
  bool createLog;

// Dialog Data
	enum {IDD = IDD_OPTIONS };



protected:
  HICON m_hIcon;

  CEdit	m_EditTempDir,m_EditProjectDir,m_EditWhiteList,m_EditPrefix,m_EditSign;
  CButton m_chboxProject,m_btnProject,m_btnTemp,m_chboxUsePrefix,m_btnOK,m_btnSign,m_chboxCreateLog;
  CButton m_stGroup;
  bool initialized;

  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
  virtual BOOL OnInitDialog();
  void MoveItem(int d,CWnd &item);
  void SizeItem(int d,CWnd &item);
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnBnClickedChangeTemp();
  afx_msg void OnBnClickedChangeProject();
  afx_msg void OnBnClickedCheckProject();
  afx_msg void OnEnChangeEditProject();
  afx_msg void OnEnChangeEditTemp();
//  afx_msg void OnClose();
  afx_msg void OnBnClickedOk();
  afx_msg void OnBnClickedCheckCopyAll2();
  afx_msg void OnBnClickedCheckAutomatically();
  afx_msg void OnEnChangeEditPrefix();
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
  afx_msg void OnBnClickedChangeSign();
  afx_msg void OnEnChangeEditSignPath();
  afx_msg void OnBnClickedCheckLog();
};
