// PackerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Main.h"
#include "MainDlg.h"
#include "Options.h"
#include <process.h>
#include "ShowAbout.h"
#include "shlwapi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CPackerDlg dialog

static UINT BASED_CODE indicators[] =
{
    ID_STATUS_TEXT
};

#define TIMER_COPY_ID 100

CString g_stattext;
bool g_packingRunning,g_terminatePacking;
CRITICAL_SECTION g_CsStatText;

void CPackerDlg::OnOK()
{};
///////////////////////////////////////////////////////////////////////////////
CPackerDlg::CPackerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPackerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
  InitializeCriticalSection (&g_CsStatText);
  g_packingRunning=false;
  g_terminatePacking=false;
  _packThread=NULL;
}
///////////////////////////////////////////////////////////////////////////////
CPackerDlg::~CPackerDlg()
{
  EnterCriticalSection(&g_CsStatText);
  g_terminatePacking=true;
  LeaveCriticalSection(&g_CsStatText);

  if( _packThread )
  {
    ::WaitForSingleObject(_packThread,INFINITE);
    ::CloseHandle(_packThread);
    _packThread=NULL;
  }
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_EDIT_SOURCE, m_cEditSourceDir);
  DDX_Control(pDX, IDC_EDIT_DEST, m_cEditDestDir);
  DDX_Control(pDX, IDC_BTN_PACK, m_btnPack);
  DDX_Control(pDX, IDC_CHECK_BINARIZE, m_chboxBinarize);
  DDX_Control(pDX, IDC_CHECK_CLEAR_ALL, m_chboxClearTemp);
  DDX_Control(pDX, IDC_ABOUT_BTN, m_btnAbout);
  DDX_Control(pDX, IDC_BTN_OPTIONS, m_btnOptions);
  DDX_Control(pDX, IDC_CHANGE_DEST, m_btnChange2);
  DDX_Control(pDX, IDC_CHANGE_SOURCE, m_btnChange1);
  DDX_Control(pDX, IDC_CHECK_CREATE_SIGN,m_chboxCreateSign);
  CDialog::DoDataExchange(pDX);
}
///////////////////////////////////////////////////////////////////////////////
BEGIN_MESSAGE_MAP(CPackerDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_CHANGE_SOURCE, &CPackerDlg::OnBnClickedChangeSource)
  ON_BN_CLICKED(IDC_CHANGE_DEST, &CPackerDlg::OnBnClickedChangeDest)
  ON_BN_CLICKED(IDC_BTN_PACK, &CPackerDlg::OnPackBnClicked)
  ON_EN_CHANGE(IDC_EDIT_DEST, &CPackerDlg::OnEnChangeEditDest)
  ON_EN_CHANGE(IDC_EDIT_SOURCE, &CPackerDlg::OnEnChangeEditSource)
  ON_WM_TIMER()
  ON_BN_CLICKED(IDC_BUTTON1, &CPackerDlg::OnBnClickedButton1)
//  ON_WM_MOVING()
  ON_BN_CLICKED(IDC_BUTTON2, &CPackerDlg::OnBnClickedButton2)
  ON_BN_CLICKED(IDC_CHECK_CLEAR_ALL, &CPackerDlg::OnBnClickedCheckClearAll)
  ON_BN_CLICKED(IDC_CHECK_BINARIZE, &CPackerDlg::OnBnClickedCheckBinarize)
  ON_WM_SIZING()
  ON_WM_SIZE()
  ON_BN_CLICKED(IDC_CHECK_CREATE_SIGN, &CPackerDlg::OnBnClickedCheckCreateSign)
END_MESSAGE_MAP()
// CPackerDlg message handlers
///////////////////////////////////////////////////////////////////////////////
BOOL CPackerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	// when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
  m_cEditSourceDir.SetWindowText(packer->project.source);
  m_cEditDestDir.SetWindowText(packer->project.dest);
  m_chboxBinarize.SetCheck(packer->createBinarize);
  m_chboxClearTemp.SetCheck(packer->clearTemp);
  m_chboxClearTemp.EnableWindow(packer->createBinarize);

  m_statusBar.Create(this);
  m_statusBar.SetIndicators(indicators,1);
  CRect rect;
  GetClientRect(&rect);
  m_statusBar.SetPaneText(0,_T("Ready"));
  m_statusBar.SetPaneInfo(0,ID_STATUS_TEXT, SBPS_STRETCH,0); 

  bool exist=PathFileExists(packer->signatureFile)!=0;
  packer->createSignature&=exist;
  m_chboxCreateSign.SetCheck(packer->createSignature);
  m_chboxCreateSign.EnableWindow(exist);

  RepositionBars(AFX_IDW_CONTROLBAR_FIRST,AFX_IDW_CONTROLBAR_LAST,ID_STATUS_TEXT);
  if (packOnStart) OnPackBnClicked();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
///////////////////////////////////////////////////////////////////////////////
HCURSOR CPackerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
///////////////////////////////////////////////////////////////////////////////
int WINAPI PosBrowseCallbackProc(HWND hwnd,UINT uMsg,LPARAM lParam,LPARAM lpData)
{
	if (uMsg == BFFM_INITIALIZED)
	{
    ::SendMessage(hwnd,BFFM_SETSELECTION,TRUE,(LPARAM)(LPCTSTR)*((CString*)lpData));
		::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)((LPCTSTR)*(CString*)lpData));
	} else
	if (uMsg == BFFM_SELCHANGED)
	{
/*		TCHAR buff[_MAX_PATH];
		if (SHGetPathFromIDList((LPITEMIDLIST)lParam,buff))
		{
			CString sPath = buff;
			((CWizardPageDir*)lpData)->m_pSetup->AddInstallationSubdirectory(sPath);
			::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)((LPCTSTR)sPath));
		}*/
	}
	return 0;
};
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnBnClickedChangeSource()
{
  m_cEditSourceDir.GetWindowText(packer->project.source);

	LPITEMIDLIST pItemIDList = NULL;
	BROWSEINFO BrowseInfo; 
	memset(&BrowseInfo,0,sizeof(BrowseInfo));

	BrowseInfo.hwndOwner = m_hWnd;
  BrowseInfo.ulFlags	 = BIF_STATUSTEXT;
  BrowseInfo.lpfn		   = (BFFCALLBACK)(PosBrowseCallbackProc);
  BrowseInfo.lParam		 = (LPARAM)&(packer->project.source);

	pItemIDList = SHBrowseForFolder(&BrowseInfo);
	if (pItemIDList != NULL)
	{
		TCHAR buff[_MAX_PATH];
		if (SHGetPathFromIDList(pItemIDList, buff))
		{
        packer->project.source = buff;
		};
	}
  m_cEditSourceDir.SetWindowText(packer->project.source);
  UpdateData(FALSE);
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnBnClickedChangeDest()
{
  m_cEditDestDir.GetWindowText(packer->project.dest);

	LPITEMIDLIST pItemIDList = NULL;
	BROWSEINFO BrowseInfo; 
	memset(&BrowseInfo,0,sizeof(BrowseInfo));

	BrowseInfo.hwndOwner = m_hWnd;
  BrowseInfo.ulFlags	 = BIF_NEWDIALOGSTYLE;
  BrowseInfo.lpfn		   = (BFFCALLBACK)(PosBrowseCallbackProc);
  BrowseInfo.lParam		 = (LPARAM)&(packer->project.dest);

	pItemIDList = SHBrowseForFolder(&BrowseInfo);
	if (pItemIDList != NULL)
	{
		TCHAR buff[_MAX_PATH];
		if (SHGetPathFromIDList(pItemIDList, buff))
		{
        packer->project.dest = buff;
		};
	}
  m_cEditDestDir.SetWindowText(packer->project.dest);
  UpdateData(FALSE);
}
///////////////////////////////////////////////////////////////////////////////
ReturnPackingStat UpdatePakingStatus(CString _text,bool _done,bool *_terminate)
{
//m_bar.SetPaneText(0,_text);
  EnterCriticalSection(&g_CsStatText);
  g_stattext=_text;
  g_packingRunning=!_done;
  if (_terminate) *_terminate=g_terminatePacking;
  LeaveCriticalSection(&g_CsStatText);
  return 0;
}
///////////////////////////////////////////////////////////////////////////////
unsigned int __stdcall ThreadRunPackFunc(LPVOID pParam) // thread
{
  CPackerAlgs *packer=(CPackerAlgs*)pParam;
  packer->project.source.MakeLower();
  packer->project.projectPath.MakeLower();
  int result;
  if ((result=packer->ConvertAndBinarize((ReturnPackingStat)UpdatePakingStatus))==0)
    result=packer->PackFolder(false,(ReturnPackingStat)UpdatePakingStatus);
  UpdatePakingStatus(_T("Ready"),true,NULL);
  return result;
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnPackBnClicked()
{
  EnterCriticalSection(&g_CsStatText);
  bool packingRunning=g_packingRunning;
  LeaveCriticalSection(&g_CsStatText);
  if (packingRunning)
  {
    EnterCriticalSection(&g_CsStatText);
    g_terminatePacking=true;
    LeaveCriticalSection(&g_CsStatText);
    m_statusBar.SetPaneText(0,_T("Wait - terminating"));
  }
  else
  {
    bool canContinue=true;
    if (packer->project.source.Find(' ')!=-1)
      canContinue=(::MessageBox(0,_T("Path to source folder contain a space charecter\n\nContinue?"),_T("warning"),MB_OKCANCEL)==1);

    if (canContinue)
    {
      g_terminatePacking=false;
      unsigned ThreadID;
      _packThread=(HANDLE)_beginthreadex(NULL,0,ThreadRunPackFunc,(void *)packer,0,&ThreadID);
      if(_packThread)
      {
        EnterCriticalSection(&g_CsStatText);
        g_packingRunning=true;
        LeaveCriticalSection(&g_CsStatText);
        m_btnPack.SetWindowTextW(_T("Cancel"));
        m_nTimer = SetTimer(TIMER_COPY_ID,50,NULL);
      } 
      else ShowErrorMSG(-1);
    }
  }
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnEnChangeEditDest()
{
  m_cEditDestDir.GetWindowText(packer->project.dest);
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnEnChangeEditSource()
{
  m_cEditSourceDir.GetWindowText(packer->project.source);
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
  if (nIDEvent=TIMER_COPY_ID)
  {
      static int count;
      count=(count+1)%40;
      CString text;
      EnterCriticalSection(&g_CsStatText);
      text=g_stattext;
      bool packingRunning=g_packingRunning;
      LeaveCriticalSection(&g_CsStatText);
      for (int i=0;i<count;i+=10)
        text+=_T(".");
    if (!packingRunning)
    {
      KillTimer(m_nTimer);
      if( _packThread )
      {
        DWORD exitCode;
        GetExitCodeThread(_packThread,&exitCode);
        while (ShowErrorMSG(exitCode)!=MB_OK) 
        {
         if (packer->PackFolder(true)==EM_OK) break;
        }
	      ::CloseHandle(_packThread);
	      _packThread=NULL;
      }
      m_btnPack.SetWindowTextW(_T("Pack"));
      m_statusBar.SetPaneText(0,_T("Ready"));
    }
    else if (!g_terminatePacking) m_statusBar.SetPaneText(0,text);
  }
  CDialog::OnTimer(nIDEvent);
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnBnClickedButton1()
{
  COptions dlgOp;
  dlgOp.tempPath=packer->temp.projectTemp;
  dlgOp.projectPath=packer->project.projectPath;
  dlgOp.sourcePath=packer->project.source;
  dlgOp.projectEnable=packer->usingProject;
  dlgOp.whiteList=whiteListString;
  dlgOp.autoPrefix=packer->autoPrefix;
  dlgOp.addonPrefix=packer->addonPrefix;
  dlgOp.signatureFile=packer->signatureFile;
  dlgOp.createLog=packer->debugMode;

  INT_PTR i;
  if ((i=dlgOp.DoModal())==IDOK)
  {
    packer->temp.projectTemp=dlgOp.tempPath;
    packer->project.projectPath=dlgOp.projectPath;
    packer->usingProject=dlgOp.projectEnable;
    whiteListString=dlgOp.whiteList;
    packer->ReadWhiteList(dlgOp.whiteList);
    packer->autoPrefix=dlgOp.autoPrefix;
    packer->addonPrefix=dlgOp.addonPrefix;
    packer->signatureFile=dlgOp.signatureFile;
    bool exist=PathFileExists(packer->signatureFile)!=0;
    packer->createSignature&=exist;
    m_chboxCreateSign.SetCheck( packer->createSignature);
    m_chboxCreateSign.EnableWindow(exist);
    packer->debugMode=dlgOp.createLog;
  }
}
///////////////////////////////////////////////////////////////////////////////
int CPackerDlg::ShowErrorMSG(DWORD _erVal) const
{
  ErrorResults erVal =ErrorResults(_erVal);
  CString erText =packer->GetErrorText(erVal);
  if (erText==_T("")) return false;
  if ((erVal==EM_Unable_delete_PBO)||(erVal==EM_Unable_Copy_PBO))
  {
    int result =(::MessageBox(0,erText+_T("\ntry again?"),_T("Packing Error"),MB_OKCANCEL|MB_ICONSTOP));
    return result==1;
  }
  ::MessageBox(0,erText,_T("Packing Error"),MB_OK|MB_ICONERROR);
  return false;
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnBnClickedButton2()
{
  CShowAbout dlg;
  dlg.DoModal();
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnBnClickedCheckClearAll()
{
  packer->clearTemp=m_chboxClearTemp.GetCheck()!=0;
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnBnClickedCheckBinarize()
{
  packer->createBinarize=m_chboxBinarize.GetCheck()!=0;
  m_chboxClearTemp.EnableWindow(packer->createBinarize);
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnSizing(UINT fwSide, LPRECT pRect)
{
  static int minx;
  CRect oldwindowRect;
  GetWindowRect(&oldwindowRect);
  if (minx==0) minx=pRect->right-pRect->left;
  if (((fwSide==1)||(fwSide==7)||(fwSide==4))&&(pRect->right-pRect->left<minx)) 
  {
    pRect->left=pRect->right-minx;
    pRect->right=pRect->left+minx;
  }
  if (((fwSide==2)||(fwSide==8)||(fwSide==5))&&(pRect->right-pRect->left<minx)) 
  {
    pRect->right=pRect->left+minx;
    pRect->left=pRect->right-minx;
  }
  
  pRect->top=oldwindowRect.top;
  pRect->bottom=oldwindowRect.bottom;
  CDialog::OnSizing(fwSide, pRect);
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::MoveItem(int d,CWnd &item)
{
    RECT btnRect;
    item.GetWindowRect(&btnRect);
    ScreenToClient(&btnRect);
    btnRect.left+=d;
    btnRect.right+=d;
    item.MoveWindow(&btnRect,1);
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::SizeItem(int d,CWnd &item)
{
    RECT btnRect;
    item.GetWindowRect(&btnRect);
    ScreenToClient(&btnRect);
    btnRect.right+=d;
    item.MoveWindow(&btnRect,1);
}
///////////////////////////////////////////////////////////////////////////////
void CPackerDlg::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType,cx,cy);
  if (nType!=SIZE_RESTORED )return;
  static int ocx;
  static int mincx;
  RECT oldwindowRect;
  GetWindowRect(&oldwindowRect);
  int dx=cx-ocx;
  if ((ocx!=0)&&(dx!=0))
  {
    MoveItem(dx,m_btnPack);
    MoveItem(dx,m_btnChange1);
    MoveItem(dx,m_btnChange2);
    MoveItem(dx,m_btnOptions);
    MoveItem(dx,m_chboxBinarize);
    SizeItem(dx,m_statusBar);
    SizeItem(dx,m_cEditDestDir);
    SizeItem(dx,m_cEditSourceDir);

    m_btnPack.RedrawWindow(); 
    m_btnChange1.RedrawWindow(); 
    m_btnChange2.RedrawWindow(); 
    m_btnOptions.RedrawWindow(); 
    m_statusBar.RedrawWindow(); 
    m_cEditDestDir.RedrawWindow(); 
    m_cEditSourceDir.RedrawWindow(); 
    m_chboxBinarize.RedrawWindow(); 
  }
  ocx=cx;
}
///////////////////////////////////////////////////////////////////////////////

void CPackerDlg::OnBnClickedCheckCreateSign()
{
  packer->createSignature= m_chboxCreateSign.GetCheck()!=0;
}
