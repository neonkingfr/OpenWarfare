#pragma once

#include "resource.h"
// CShowAbout dialog

class CShowAbout : public CDialog
{
	DECLARE_DYNAMIC(CShowAbout)

public:
	CShowAbout(CWnd* pParent = NULL);   // standard constructor
  CEdit m_editLicence;
	virtual ~CShowAbout();

// Dialog Data
#ifdef PERSONAL_EDITIONAL
	enum {IDD = IDD_ABOUT_DIALOG_PERSONAL};
#else
	enum {IDD = IDD_ABOUT_DIALOG };
#endif


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnEnChangeEditLicence();
  afx_msg void OnEnUpdateEditLicence();
};
