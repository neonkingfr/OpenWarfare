// Options.cpp : implementation file
//

#include "stdafx.h"
#include "Main.h"
#include "Options.h"

// COptions dialog

IMPLEMENT_DYNAMIC(COptions, CDialog)

COptions::COptions(CWnd* pParent /*=NULL*/)
	: CDialog(COptions::IDD, pParent)
{
  m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
  initialized=false;
}
///////////////////////////////////////////////////////////////////////////////
COptions::~COptions()
{
}
///////////////////////////////////////////////////////////////////////////////
void COptions::DoDataExchange(CDataExchange* pDX)
{
  DDX_Control(pDX, IDC_EDIT_PROJECT, m_EditProjectDir);
  DDX_Control(pDX, IDC_EDIT_TEMP, m_EditTempDir);
  DDX_Control(pDX, IDC_CHECK_PROJECT, m_chboxProject);
  DDX_Control(pDX, IDC_CHANGE_PROJECT, m_btnProject);
  DDX_Control(pDX, IDC_EDIT_WHITE_LIST ,m_EditWhiteList);
  DDX_Control(pDX, IDC_EDIT_SIGN_PATH, m_EditSign);
  DDX_Control(pDX, IDC_CHANGE_SIGN, m_btnSign);
  DDX_Control(pDX, IDC_CHECK_AUTOMATICALLY, m_chboxUsePrefix);
  DDX_Control(pDX, IDC_EDIT_PREFIX, m_EditPrefix);
  DDX_Control(pDX, IDOK,m_btnOK);
  DDX_Control(pDX, IDC_CHANGE_TEMP,m_btnTemp);
  DDX_Control(pDX, IDC_GROUP_BOX,m_stGroup);
  DDX_Control(pDX, IDC_CHECK_LOG,m_chboxCreateLog);
	CDialog::DoDataExchange(pDX);
}
///////////////////////////////////////////////////////////////////////////////
BOOL COptions::OnInitDialog()
{
	CDialog::OnInitDialog();

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

  m_EditTempDir.SetWindowText(tempPath);
  m_EditProjectDir.SetWindowText(projectPath);

  m_EditProjectDir.EnableWindow(projectEnable);
  m_btnProject.EnableWindow(projectEnable);
  m_chboxProject.SetCheck(!projectEnable);

  if (!projectEnable)
    m_EditProjectDir.SetWindowTextW(sourcePath);
  else
    m_EditProjectDir.SetWindowTextW(projectPath);

  m_EditWhiteList.SetWindowText(whiteList);

  m_EditPrefix.SetWindowTextW(addonPrefix);
  m_chboxUsePrefix.SetCheck(autoPrefix);
  m_chboxCreateLog.SetCheck(createLog);
  m_EditPrefix.EnableWindow(!autoPrefix);
  m_EditSign.SetWindowTextW(signatureFile);
  initialized=true;

	return TRUE;
}
///////////////////////////////////////////////////////////////////////////////
BEGIN_MESSAGE_MAP(COptions, CDialog)
  ON_BN_CLICKED(IDC_CHANGE_TEMP, &COptions::OnBnClickedChangeTemp)
  ON_BN_CLICKED(IDC_CHANGE_PROJECT, &COptions::OnBnClickedChangeProject)
  ON_BN_CLICKED(IDC_CHECK_PROJECT, &COptions::OnBnClickedCheckProject)
  ON_EN_CHANGE(IDC_EDIT_PROJECT, &COptions::OnEnChangeEditProject)
  ON_EN_CHANGE(IDC_EDIT_TEMP, &COptions::OnEnChangeEditTemp)
//  ON_WM_CLOSE()
  ON_BN_CLICKED(IDOK, &COptions::OnBnClickedOk)
  ON_BN_CLICKED(IDC_CHECK_AUTOMATICALLY, &COptions::OnBnClickedCheckAutomatically)
  ON_EN_CHANGE(IDC_EDIT_PREFIX, &COptions::OnEnChangeEditPrefix)
  ON_WM_SIZE()
  ON_WM_SIZING()
  ON_BN_CLICKED(IDC_CHANGE_SIGN, &COptions::OnBnClickedChangeSign)
  ON_EN_CHANGE(IDC_EDIT_SIGN_PATH, &COptions::OnEnChangeEditSignPath)
  ON_BN_CLICKED(IDC_CHECK_LOG, &COptions::OnBnClickedCheckLog)
END_MESSAGE_MAP()

///////////////////////////////////////////////////////////////////////////////
// COptions message handlers
int WINAPI PosBrowseCallbackProcOP(HWND hwnd,UINT uMsg,LPARAM lParam,LPARAM lpData)
{
	if (uMsg == BFFM_INITIALIZED)
	{
    ::SendMessage(hwnd,BFFM_SETSELECTION,TRUE,(LPARAM)(LPCTSTR)*((CString*)lpData));
		::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)((LPCTSTR)*(CString*)lpData));
	} else
	if (uMsg == BFFM_SELCHANGED)
	{
/*		TCHAR buff[_MAX_PATH];
		if (SHGetPathFromIDList((LPITEMIDLIST)lParam,buff))
		{
			CString sPath = buff;
			((CWizardPageDir*)lpData)->m_pSetup->AddInstallationSubdirectory(sPath);
			::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)((LPCTSTR)sPath));
		}*/
	}
	return 0;
};
///////////////////////////////////////////////////////////////////////////////
void COptions::OnBnClickedChangeTemp()
{
  m_EditTempDir.GetWindowText(tempPath);

	LPITEMIDLIST pItemIDList = NULL;
	BROWSEINFO BrowseInfo; 
	memset(&BrowseInfo,0,sizeof(BrowseInfo));

	BrowseInfo.hwndOwner = m_hWnd;
  BrowseInfo.ulFlags	 = BIF_NEWDIALOGSTYLE;
  BrowseInfo.lpfn		   = (BFFCALLBACK)(PosBrowseCallbackProcOP);
  BrowseInfo.lParam		 = (LPARAM)&tempPath;

	pItemIDList = SHBrowseForFolder(&BrowseInfo);
	if (pItemIDList != NULL)
	{
		TCHAR buff[_MAX_PATH];
		if (SHGetPathFromIDList(pItemIDList, buff))
		{
        tempPath = buff;
		};
	}
  m_EditTempDir.SetWindowText(tempPath);
  UpdateData(FALSE);
}
///////////////////////////////////////////////////////////////////////////////
void COptions::OnBnClickedChangeProject()
{
  m_EditProjectDir.GetWindowText(projectPath);

	LPITEMIDLIST pItemIDList = NULL;
	BROWSEINFO BrowseInfo; 
	memset(&BrowseInfo,0,sizeof(BrowseInfo));

	BrowseInfo.hwndOwner = m_hWnd;
  BrowseInfo.ulFlags	 = BIF_STATUSTEXT;
  BrowseInfo.lpfn		   = (BFFCALLBACK)(PosBrowseCallbackProcOP);
  BrowseInfo.lParam		 = (LPARAM)&projectPath;

	pItemIDList = SHBrowseForFolder(&BrowseInfo);
	if (pItemIDList != NULL)
	{
		TCHAR buff[_MAX_PATH];
		if (SHGetPathFromIDList(pItemIDList, buff))
		{
        projectPath = buff;
		};
	}
  m_EditProjectDir.SetWindowText(projectPath);
  UpdateData(FALSE);
}
///////////////////////////////////////////////////////////////////////////////
void COptions::OnBnClickedCheckProject()
{
  projectEnable =!m_chboxProject.GetCheck();
  m_EditProjectDir.EnableWindow(projectEnable);
  m_btnProject.EnableWindow(projectEnable);
 /* if (!projectEnable)
  {
    m_cEditProjectDir.SetWindowTextW(sourcePath);
  }
  else
  {
    m_cEditProjectDir.SetWindowTextW(projectPath);
  }*/
}
///////////////////////////////////////////////////////////////////////////////
void COptions::OnEnChangeEditProject()
{
  m_EditProjectDir.GetWindowTextW(projectPath);
}
///////////////////////////////////////////////////////////////////////////////
void COptions::OnEnChangeEditTemp()
{
  m_EditTempDir.GetWindowTextW(tempPath);
}
//void COptions::OnClose()
//{
//  TODO: Add your message handler code here and/or call default
//  m_cWhiteList.GetWindowTextW(whiteList);
//  CDialog::OnClose();
//}
///////////////////////////////////////////////////////////////////////////////
void COptions::OnBnClickedOk()
{
  m_EditWhiteList.GetWindowText(whiteList);
  OnOK();
}
///////////////////////////////////////////////////////////////////////////////
void COptions::OnBnClickedCheckAutomatically()
{
  autoPrefix= m_chboxUsePrefix.GetCheck()!=0;
  m_EditPrefix.EnableWindow(!autoPrefix);
}
///////////////////////////////////////////////////////////////////////////////

void COptions::OnEnChangeEditPrefix()
{
  m_EditPrefix.GetWindowTextW(addonPrefix);
}

///////////////////////////////////////////////////////////////////////////////
void COptions::OnSizing(UINT fwSide, LPRECT pRect)
{
  static int minx;
  CRect oldwindowRect;
  GetWindowRect(&oldwindowRect);
  if (minx==0) minx=pRect->right-pRect->left;
  if (((fwSide==1)||(fwSide==7)||(fwSide==4))&&(pRect->right-pRect->left<minx)) 
  {
    pRect->left=pRect->right-minx;
    pRect->right=pRect->left+minx;
  }
  if (((fwSide==2)||(fwSide==8)||(fwSide==5))&&(pRect->right-pRect->left<minx)) 
  {
    pRect->right=pRect->left+minx;
    pRect->left=pRect->right-minx;
  }
  
  pRect->top=oldwindowRect.top;
  pRect->bottom=oldwindowRect.bottom;
  CDialog::OnSizing(fwSide, pRect);
}
///////////////////////////////////////////////////////////////////////////////
void COptions::MoveItem(int d,CWnd &item)
{
    RECT btnRect;
    item.GetWindowRect(&btnRect);
    ScreenToClient(&btnRect);
    btnRect.left+=d;
    btnRect.right+=d;
    item.MoveWindow(&btnRect,1);
}
///////////////////////////////////////////////////////////////////////////////
void COptions::SizeItem(int d,CWnd &item)
{
    RECT btnRect;
    item.GetWindowRect(&btnRect);
    ScreenToClient(&btnRect);
    btnRect.right+=d;
    item.MoveWindow(&btnRect,1);
}
///////////////////////////////////////////////////////////////////////////////
void COptions::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType,cx,cy);
  static int ocx;
  if (initialized) 
  {
    RECT oldwindowRect;
    GetWindowRect(&oldwindowRect);
    int dx=cx-ocx;
    if ((ocx!=0)&&(dx!=0))
    {
      MoveItem(dx,m_btnOK);
      MoveItem(dx,m_btnTemp);
      MoveItem(dx,m_btnProject);
      MoveItem(dx,m_btnSign);
      SizeItem(dx,m_EditProjectDir);
      SizeItem(dx,m_EditTempDir);
      SizeItem(dx,m_EditWhiteList);
      SizeItem(dx,m_stGroup);
      SizeItem(dx,m_EditPrefix);
      SizeItem(dx,m_EditSign);

      m_btnOK.RedrawWindow(); 
      m_btnTemp.RedrawWindow();
      m_btnProject.RedrawWindow(); 
      m_EditProjectDir.RedrawWindow(); 
      m_EditTempDir.RedrawWindow();
      m_EditWhiteList.RedrawWindow(); 
      m_stGroup.RedrawWindow(); 
      m_EditPrefix.RedrawWindow(); 
      m_EditSign.RedrawWindow(); 
      m_btnSign.RedrawWindow(); 
    }
  }
  ocx=cx;
}
///////////////////////////////////////////////////////////////////////////////

void COptions::OnBnClickedChangeSign()
{
  OPENFILENAME ofn; 
  TCHAR chFile[_MAX_PATH];
  _tcscpy_s(chFile,_MAX_PATH,signatureFile.GetBuffer());
  signatureFile.ReleaseBuffer();
  ZeroMemory(&ofn, sizeof(OPENFILENAME));
  ofn.lStructSize = sizeof(OPENFILENAME);
  ofn.lpstrFile = chFile;
  ofn.hwndOwner = 0;
  ofn.nMaxFile = sizeof(chFile);
  ofn.lpstrFilter = 
          _T("BiPrivateKey file (*.biprivatekey)\0*.biprivatekey\0");
  ofn.nFilterIndex = 1;
  ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
  if (GetOpenFileName(&ofn))
  {
     signatureFile= chFile;
     m_EditSign.SetWindowTextW(signatureFile);
  }
}

void COptions::OnEnChangeEditSignPath()
{
  m_EditSign.GetWindowTextW(signatureFile);
}

void COptions::OnBnClickedCheckLog()
{
  createLog= m_chboxCreateLog.GetCheck()!=0;
}
