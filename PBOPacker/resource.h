//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Packer.rc
//
#define IDOK                            1
#define ID_STATUS_TEXT                  101
#define IDD_MAIN                        102
#define IDD_MAIN_PERSONAL               103
#define IDR_MAINFRAME                   128
#define IDD_OPTIONS                     131
#define IDD_ABOUT_DIALOG                143
#define IDD_ABOUT_DIALOG_PERSONAL       144
#define IDR_LICENCE1                    146
#define IDR_LICENCE                     146
#define IDR_LICENCE2                    149
#define IDR_LICENCE_PUBLIC              149
#define IDC_EDIT_SOURCE                 1000
#define IDC_EDIT_DEST                   1001
#define IDC_CHANGE_SOURCE               1002
#define IDC_CHANGE_DEST                 1003
#define IDC_BTN_PACK                    1004
#define IDC_STATUS                      1005
#define IDC_BUTTON1                     1006
#define IDC_BTN_OPTIONS                 1006
#define IDC_CHANGE_TEMP                 1009
#define IDC_CHANGE_SIGN                 1010
#define IDC_EDIT_TEMP                   1012
#define IDC_EDIT_PROJECT                1013
#define IDC_CHANGE_PROJECT              1014
#define IDC_EDIT_SIGN_PATH              1015
#define IDC_CHECK_PROJECT               1016
#define IDC_LIST1                       1017
#define IDC_EDIT_WHITE_LIST             1018
#define IDC_BUTTON2                     1020
#define IDC_ABOUT_BTN                   1020
#define IDC_EDIT_PREFIX                 1020
#define IDC_CHECK_COPY_ALL              1021
#define IDC_CHECK_AUTOMATICALLY         1021
#define IDC_CHECK_BINARIZE              1022
#define IDC_EDIT1                       1026
#define IDC_EDIT_LICENCE                1026
#define IDC_CHECK_CLEAR_ALL             1028
#define IDC_GROUP_BOX                   1029
#define IDC_CHECK_CREATE_SIGN           1030
#define IDC_RICHEDIT21                  1031
#define IDC_RICHEDIT22                  1032
#define IDC_CHECK                       1033
#define IDC_CHECK_LOG                   1033
#define IDC_RICHTEXT                    1310

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        150
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1034
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
