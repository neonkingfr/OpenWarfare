#pragma once

void MakeLongDir(CString &_dir);
void MakeShortDir(CString &_dir);
CString ReturnLongDir(CString _dir);
CString ReturnShortDir(CString _dir);
CString RemoveLastFolder(CString _path,CString *_folder =NULL);

typedef struct {CString dir; CString appName;} TappInfo;
typedef int (*ReturnPackingStat)(CString _text,bool _done,bool *_terminate);

enum ErrorResults {EM_OK=0,EM_Terminated_By_User,EM_Temp_Not_Exist,EM_Source_Not_Exist,EM_Copy_File_Error,EM_Binarize_Not_Found,EM_CfgConvert_Not_Found,EM_FileBank_Not_Found,EM_Unable_delete_PBO,EM_Project_No_Subpath_Source,EM_Unable_Copy_PBO,EM_DSSignFile_Not_Found};

typedef struct 
{
  CString name; CString source; CString dest;CString projectPath;
} TAddonProject;

typedef struct 
{
  CString addonTemp;CString projectTemp;
  CString Full(){return ReturnLongDir(projectTemp)+ReturnLongDir(addonTemp);};
} TTemp;

class CPackerAlgs
{
public:
  CPackerAlgs(void);
  ~CPackerAlgs(void);
    //values
  TappInfo binarize,cfgConvert,filebank,dsSignFile; // path and filename to required applications; 
  TTemp temp;
  TAddonProject project;
  bool usingProject;
  bool debugMode;
  bool createBinarize;
  bool clearTemp;
  bool autoPrefix;
  bool createSignature;
  CString signatureFile;
  CString addonPrefix;
  
  CArray<CString> whiteList; // files extensions to include to package; 

  //functions
public:
  int ConvertAndBinarize(ReturnPackingStat statFce=NULL);
protected:
  int SynchFolders(CString _tempPath,CString _sourcePath,ReturnPackingStat statFce=NULL);
  int SynchFolders(ReturnPackingStat statFce,CString _projectpath);
  int ConvertCfgsInFolders(CString _tempPath,CString _sourcePath,ReturnPackingStat statFce);
  int CopyPBO(CString readFrom,ReturnPackingStat statFce=NULL);
  bool ClearTemp();
public:
  int PackFolder(bool copyOnly, ReturnPackingStat statFce =NULL);

  int DeleteTemp();
  int CreatePath(CString path)const;
  bool RunApp(CString fileName, CString params,CString dir=NULL) const;
  void ReadWhiteList(CString _data);
  CString CPackerAlgs::GetErrorText(ErrorResults _erVal) ;
};
