// PackerDlg.h : header file
//

#pragma once

#include "PackerAlgs.h"

//global
// CPackerDlg dialog
class CPackerDlg : public CDialog
{
// Construction
public:
	CPackerDlg(CWnd* pParent = NULL);	// standard constructor
  ~CPackerDlg();

  CPackerAlgs *packer;
  CString whiteListString;
  bool packOnStart;

 // ReturnPackingStat UpdateStatusbar(CString _text);

// Dialog Data
#ifdef PERSONAL_EDITIONAL
	enum {IDD = IDD_MAIN_PERSONAL};
#else
	enum {IDD = IDD_MAIN };
#endif

protected:
	CEdit	m_cEditSourceDir,m_cEditDestDir;
  CButton m_btnPack,m_btnChange1,m_btnChange2,m_btnOptions,m_btnAbout;
  CStatusBar m_statusBar;
  CButton m_chboxBinarize,m_chboxClearTemp,m_chboxCreateSign;


  bool m_stopPacking;
  CString m_packingProggres;

  UINT_PTR m_nTimer;
  HANDLE _packThread;
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
  int ShowErrorMSG(DWORD erVal) const;
  void MoveItem(int d,CWnd &item);
  void SizeItem(int d,CWnd &item);
  
public:
// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
  virtual void OnOK();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
  afx_msg void OnTimer(UINT nIDEvent); 
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedChangeSource();
  afx_msg void OnBnClickedChangeDest();
  afx_msg void OnPackBnClicked();
  afx_msg void OnEnChangeEditDest();
  afx_msg void OnEnChangeEditSource();
  afx_msg void OnBnClickedButton1();
//  afx_msg void OnMoving(UINT fwSide, LPRECT pRect);
  afx_msg void OnStnClickedLogo();
  afx_msg void OnBnClickedButton2();
  afx_msg void OnBnClickedCheckCopyAll();
  afx_msg void OnBnClickedCheckClearAll();
  afx_msg void OnBnClickedCheckBinarize();
  afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnBnClickedCheckCreateSign();
};
