#pragma once
#define REG_DEST _T("SOFTWARE\\Bohemia Interactive\\PBOPacker")
#define REG_ARMA _T("SOFTWARE\\Bohemia Interactive Studio\\ArmA 2 OA")

#define SOURCE_NAME _T("SourcePATH")
#define TEMP_NAME _T("TempPATH")
#define DEST_NAME _T("DestPATH")
#define ARMAREGPATH _T("ARMAREGPATH")
#define PROJECT_PATH _T("Project path")
#define USE_PROJECT _T("Using project")
#define WHITE_LIST _T("White list") 
#define BINARIZE_DIR _T("Binarize directory") 
#define BINARIZE_APPNAME _T("Binarize appname") 
#define CFGCONVERT_DIR _T("CfgConvert directory") 
#define CFGCONVERT_APPNAME _T("CfgConvert appname") 
#define FILEBANK_DIR _T("Filebank directory") 
#define FILEBANK_APPNAME _T("Filebank appname")
#define DSSINGFILE_DIR _T("dsSignFile directory") 
#define DSSINGFILE_APPNAME _T("dsSignFile appname") 
#define FORCE_BINARIZE _T("Force Binarize") 
#define CLEAR_TEMP _T("Clear temp") 
#define ADDON_PREFIX _T("Addon prefix")
#define USE_ADDON_PREFIX _T("Use addon prefix")
#define USE_SIGNATURES _T("Use signatures") 
#define SIGNATURE_FILE _T("Signature file")
#define CREATE_LOG _T("Create log")

class CWinRegistry
{
public:
  CWinRegistry(HKEY _root=HKEY_CURRENT_USER,CString _path=REG_DEST);
  ~CWinRegistry(void);
//functions

  bool SetValueSTR(CString _valuename,CString _value);
  bool GetValueSTR(CString _valuename,CString &_value);
  CString GetValue(CString _valuename);
  
  CString GetArmaPath(CString _path) ;
//variables
  HKEY m_key;

};
