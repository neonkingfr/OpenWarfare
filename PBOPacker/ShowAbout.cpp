// ShowAbout.cpp : implementation file
//

#include "stdafx.h"
#include "ShowAbout.h"


// CShowAbout dialog

IMPLEMENT_DYNAMIC(CShowAbout, CDialog)

CShowAbout::CShowAbout(CWnd* pParent /*=NULL*/)
	: CDialog(CShowAbout::IDD, pParent)
{

}

CShowAbout::~CShowAbout()
{
}

void CShowAbout::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_EDIT_LICENCE, m_editLicence);
  
}

BOOL CShowAbout::OnInitDialog()
{
  CDialog::OnInitDialog();
  HMODULE mod=AfxGetInstanceHandle();
#ifdef PERSONAL_EDITIONAL
  HRSRC rsrc=FindResource(mod,MAKEINTRESOURCE(IDR_LICENCE_PUBLIC),_T("licence"));
#else
  HRSRC rsrc=FindResource(mod,MAKEINTRESOURCE(IDR_LICENCE),_T("licence"));
#endif
  HGLOBAL glob=LoadResource(mod,rsrc);
  char *p=(char *)LockResource(glob);
  CString cstr=CString(p);
 // DWORD sz=SizeofResource(mod,rsrc);
  
  m_editLicence.SetWindowTextW(cstr);
  HWND handle=m_editLicence.GetSafeHwnd();
 // ::SendMessage(handle, EM_SETBKGNDCOLOR, 0, RGB(0, 0, 255));
 return true;
}


BEGIN_MESSAGE_MAP(CShowAbout, CDialog)
  ON_EN_CHANGE(IDC_EDIT_LICENCE, &CShowAbout::OnEnChangeEditLicence)
  ON_EN_UPDATE(IDC_EDIT_LICENCE, &CShowAbout::OnEnUpdateEditLicence)
END_MESSAGE_MAP()


// CShowAbout message handlers

void CShowAbout::OnEnChangeEditLicence()
{
  // TODO:  If this is a RICHEDIT control, the control will not
  // send this notification unless you override the CDialog::OnInitDialog()
  // function and call CRichEditCtrl().SetEventMask()
  // with the ENM_CHANGE flag ORed into the mask.

  // TODO:  Add your control notification handler code here
}

void CShowAbout::OnEnUpdateEditLicence()
{
  // TODO:  If this is a RICHEDIT control, the control will not
  // send this notification unless you override the CDialog::OnInitDialog()
  // function to send the EM_SETEVENTMASK message to the control
  // with the ENM_UPDATE flag ORed into the lParam mask.

  // TODO:  Add your control notification handler code here
}
