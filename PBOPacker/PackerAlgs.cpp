#include "StdAfx.h"
#include "shlwapi.h"
#include "PackerAlgs.h"
#include <atlrx.h>
#include <sys/stat.h>

///////////////////////////////////////////////////////////////////////////////
CPackerAlgs::CPackerAlgs(void)
{
  debugMode=false;
}
///////////////////////////////////////////////////////////////////////////////
CPackerAlgs::~CPackerAlgs(void)
{
}
///////////////////////////////////////////////////////////////////////////////
CString ProjectName(CString _path)
{
  CString path = ReturnShortDir(_path);
  if (path.GetLength()<1) return _T("");
  int slashpos=path.GetLength()-1;
  while (path[slashpos]!=_T('\\')&&(slashpos>0)) --slashpos;
  return path.Right(path.GetLength()-1-slashpos);
}
///////////////////////////////////////////////////////////////////////////////
// check if file destination file older that source file and replace him.
bool XCopyFile(CString _src,CString _dest)
{
  struct _stat infoSrc,infoDest;
  if (_tstat(_src,&infoSrc)==0)
    if (_tstat(_dest,&infoDest)==0)
      if (infoSrc.st_mtime<=infoDest.st_mtime) return false;
  CopyFile(_src,_dest,false);
  return true;
}
///////////////////////////////////////////////////////////////////////////////
// check all sub-folders in project and removwe them if empty
bool RemoveEmptyFolders(CString _path)
{
  WIN32_FIND_DATA	findData;
  HANDLE h = FindFirstFile(_path+_T("\\*.*"), &findData);
  bool empty=true;
  if (h != INVALID_HANDLE_VALUE)
  {
    do
    {
	    if ((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)// files
	    {
        if ((_tcsicmp(findData.cFileName,_T("."))!=0)&&(_tcsicmp(findData.cFileName,_T(".."))!=0))
        {
          if (RemoveEmptyFolders(_path+_T("\\")+findData.cFileName))
          {
            _tchdir(_path);
            _trmdir(findData.cFileName);
            _tchdir(_T("\\"));
          }
          else empty=false;
        }
	    }
      else empty=false;
    }
    while (FindNextFile(h, &findData));
    if (h) FindClose(h);
  }
  return empty;
}
///////////////////////////////////////////////////////////////////////////////
void ClearFolder(CString _path)
{
  WIN32_FIND_DATA	findData;
  HANDLE h = FindFirstFile(_path+_T("\\*.*"), &findData);
  bool empty=true;
  if (h != INVALID_HANDLE_VALUE)
  {
    do
    {
	    if ((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)
	    {
        if ((_tcsicmp(findData.cFileName,_T("."))!=0)&&(_tcsicmp(findData.cFileName,_T(".."))!=0))
        {
          ClearFolder(_path+_T("\\")+findData.cFileName);
          _trmdir(_path+_T("\\")+findData.cFileName);
        }
	    }
      else 
      {
        _tchmod(_path+_T("\\")+findData.cFileName,S_IREAD | _S_IWRITE);
        _tremove(_path+_T("\\")+findData.cFileName);
      }
    }
    while (FindNextFile(h, &findData));
    if (h) FindClose(h);
  }
}
///////////////////////////////////////////////////////////////////////////////
// run a synchronizing source and a temp folder
int CPackerAlgs::ConvertAndBinarize(ReturnPackingStat statFce)
{
  if (!PathFileExists(project.source)) return EM_Source_Not_Exist;
  project.name=ProjectName(project.source);
  CString _projectpath;
  if (usingProject)
  {
    if ((project.source.Find(project.projectPath)!=0)||(!PathFileExists(project.projectPath)))
      return EM_Project_No_Subpath_Source;
    _projectpath=ReturnShortDir(project.projectPath);
  }
  else 
    _projectpath=ReturnShortDir(project.source);

  CString projectPactLevelDown= RemoveLastFolder(_projectpath);
  if (projectPactLevelDown==_T("")) return EM_Project_No_Subpath_Source;
  temp.addonTemp=project.source.Mid(projectPactLevelDown.GetLength()+1);

  if (debugMode) _tremove(ReturnLongDir(project.dest)+project.name+_T(".log"));

  if (!createBinarize) return EM_OK; // do not continue if user dont want binarize;
  if (clearTemp&&createBinarize) ClearFolder(temp.Full());

  int result = ConvertCfgsInFolders(temp.Full(),ReturnLongDir(project.source),statFce);
  if (!result)
  {
    bool terminate=false;
    if (statFce) statFce(CString("Binarizing P3Ds: ")+ project.source,false,&terminate);
    if (terminate) return EM_Terminated_By_User;

    CString params=_T(" -targetBonesInterval=56 -addon=\"") //-maxProcesses=20 
      +ReturnShortDir(_projectpath)+_T("\" -exclude=\"")+ReturnLongDir(project.source)+_T("exclude.lst\" -textures=\"")+ReturnShortDir(temp.projectTemp)
      +_T("\" -binpath=\"")+ReturnShortDir(binarize.dir)+_T("\" \"")
      +ReturnShortDir(project.source)+_T("\" \"")+ReturnShortDir(temp.Full())+_T("\" ");

  if (!RunApp(ReturnLongDir(binarize.dir)+ binarize.appName,params,ReturnLongDir(projectPactLevelDown)))
    return EM_Binarize_Not_Found;
  result = SynchFolders(temp.Full(),ReturnLongDir(project.source),statFce);

  if (statFce) statFce(_T("Cleaning "),false,&terminate);
  if (terminate) return EM_Terminated_By_User;
  RemoveEmptyFolders(ReturnShortDir(temp.Full()));
  }
  return result;
}
///////////////////////////////////////////////////////////////////////////////
// Copy files from whitelist or all if copyAll=true 
int CPackerAlgs::SynchFolders(CString _tempPath,CString _sourcePath,ReturnPackingStat statFce)
{
  bool terminate=false;
  if (CreatePath(_tempPath)==0)
  {
    if (_tchdir(_sourcePath)==0)
    { 
      CString sPath = _sourcePath+ CString("*.*");
	    WIN32_FIND_DATA	findData;
	    HANDLE h = FindFirstFile(sPath, &findData);

      int i;
      CAtlRegExp<> rePath;
      CAtlREMatchContext<> reMc;
      INT_PTR count = whiteList.GetCount();

	    if (h != INVALID_HANDLE_VALUE)
	    {
		    do
		    {
			    if ((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)// files
			    {
            TCHAR ext[_MAX_EXT]=_T("");
            _tsplitpath_s(findData.cFileName,NULL,0,NULL,0,NULL,0,ext,_MAX_EXT);
            for (i=0; i<count;i++) 
            {
              rePath.Parse(whiteList[i],false);
              if (rePath.Match(findData.cFileName,&reMc))
                if (XCopyFile(_sourcePath+findData.cFileName,_tempPath+findData.cFileName))
                {
                  if (statFce) statFce(CString("Copy:")+ _sourcePath+findData.cFileName,false,&terminate);
                  if (terminate)  
                  {
                    if (h) FindClose(h);
                    _tchdir(_T("\\"));
                    return EM_Terminated_By_User;                
                  }
                }
            }
			    }
			    else if (lstrcmp(findData.cFileName,_T("."))&&lstrcmp(findData.cFileName,_T(".."))) //directory
			    {
            int result;
            if ((result=SynchFolders(_tempPath+findData.cFileName+_T("\\"),_sourcePath+findData.cFileName+_T("\\"),statFce))!=0) return result;
			    }
		    }
		    while (FindNextFile(h, &findData));
        if (h) FindClose(h);
	    }
    }
    else 
      return(EM_Source_Not_Exist);
    _tchdir(_T("\\"));
  }
  else return(EM_Temp_Not_Exist);
  return EM_OK;
}
///////////////////////////////////////////////////////////////////////////////
// create folder tree in temp folder, convert all config.cpps, binarize models and textures and copy all files that are succes one of condition whitelist 
// this function is spread out to 2 overloaded functions 
// this check subfolder.
int CPackerAlgs::ConvertCfgsInFolders(CString _tempPath,CString _sourcePath,ReturnPackingStat statFce)
{
  bool terminate=false;
  if (CreatePath(_tempPath)==0)
  {
    if (_tchdir(_sourcePath)==0)
    { 
      if (PathFileExists(_sourcePath+_T("config.cpp")))
      {
        if (statFce) statFce(_T("Converting:")+ _sourcePath+_T("config.cpp"),false,&terminate);
        if (terminate) return EM_Terminated_By_User;
        CString params=_T(" -bin -dst \"")+_tempPath +_T("config.bin\" \"")+_sourcePath+_T("config.cpp\"");
        if (!RunApp(cfgConvert.appName,params,cfgConvert.dir))
        {
          _tchdir(_T("\\"));
          return EM_CfgConvert_Not_Found;
        }
      }
      CString sPath = _sourcePath+ CString("*.*");
	    WIN32_FIND_DATA	findData;
	    HANDLE h = FindFirstFile(sPath, &findData);

	    if (h != INVALID_HANDLE_VALUE)
	    {
		    do
		    {
			    if (((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0)&&(lstrcmp(findData.cFileName,_T("."))&&lstrcmp(findData.cFileName,_T(".."))))
			    {
            int result;
            if ((result=ConvertCfgsInFolders(_tempPath+findData.cFileName+_T("\\"),_sourcePath+findData.cFileName+_T("\\"),statFce))!=0)
            {
              if (h) FindClose(h);
              return result;
            }       
			    }
		    }
		    while (FindNextFile(h, &findData));
        if (h) FindClose(h);
	    }
    }
    else return(EM_Source_Not_Exist);
    _tchdir(_T("\\"));
  }
  else return(EM_Temp_Not_Exist);
  return EM_OK;
}
///////////////////////////////////////////////////////////////////////////////
// not used yet
int CPackerAlgs::DeleteTemp()
{
 return 0; 
}
///////////////////////////////////////////////////////////////////////////////
// create folder list, each of folders to lead to end of folder list.
int CPackerAlgs::CreatePath(CString path) const
{
  TCHAR *end = path.GetBuffer();
  while (end = _tcschr(end, '\\'))
  {
    *end = 0;
    if ((_tmkdir(path)) ==ENOENT) return ENOENT;
    *end = '\\';
    end++;
  }
  path.ReleaseBuffer();
  return 0;
}
///////////////////////////////////////////////////////////////////////////////
// create a new process and wait for end.
bool CPackerAlgs::RunApp(CString fileName, CString params,CString dir) const
{
  CString wholeFileName;
  if (PathFileExists(fileName))
  {
    wholeFileName=fileName;
  }
  else if (PathFileExists(ReturnLongDir(dir)+fileName))
  {
    wholeFileName=ReturnLongDir(dir)+fileName;
  }
  else return false; 
  STARTUPINFO stInfo;
  memset (&stInfo, 0, sizeof (stInfo));
  stInfo.cb = sizeof(stInfo);
  PROCESS_INFORMATION prInfo;
  memset (&prInfo, 0, sizeof (prInfo));

  SECURITY_ATTRIBUTES secAtt;
  secAtt.nLength= sizeof(secAtt);
  secAtt.bInheritHandle=true;
  secAtt.lpSecurityDescriptor=NULL;
  if (debugMode) 
  {
    stInfo.hStdOutput = CreateFile(ReturnLongDir(project.dest)+project.name+_T(".log"),GENERIC_WRITE,FILE_SHARE_READ|FILE_SHARE_WRITE,&secAtt,OPEN_ALWAYS,0,0);
    stInfo.hStdError =stInfo.hStdOutput;
    stInfo.dwFlags = STARTF_USESTDHANDLES;
    SetFilePointer(stInfo.hStdOutput,0,NULL,FILE_END);
  }
  int procesFlags=CREATE_DEFAULT_ERROR_MODE | NORMAL_PRIORITY_CLASS |CREATE_NO_WINDOW;

  if (::CreateProcess (wholeFileName,params.GetBuffer(),&secAtt, &secAtt,
    true, procesFlags,
    NULL, dir, &stInfo, &prInfo))
  {
    //wait until it finishes its work
    ::WaitForSingleObject(prInfo.hProcess, INFINITE);

    if (prInfo.hThread)  CloseHandle(prInfo.hThread);
    if (prInfo.hProcess) CloseHandle(prInfo.hProcess);
    if (stInfo.hStdOutput) CloseHandle(stInfo.hStdOutput);
    params.ReleaseBuffer();
    return true;
  } 
  if (stInfo.hStdOutput) CloseHandle(stInfo.hStdOutput);
  params.ReleaseBuffer();
  return false;
}
///////////////////////////////////////////////////////////////////////////////
// pack all data from temp folder and save a PBO in destination folder
int CPackerAlgs::PackFolder(bool copyOnly,ReturnPackingStat statFce)
{ 
  bool terminate=false;
  CString readFrom;
  if (createBinarize)
  {
    readFrom=ReturnShortDir(temp.Full());
    if (!PathFileExists(readFrom)) return EM_Temp_Not_Exist;
  }
  else 
  {
    readFrom=ReturnShortDir(project.source);
    if (!PathFileExists(readFrom)) return EM_Source_Not_Exist;
  }
  if (!copyOnly)
  {
    if (statFce) statFce(_T("Creating texture headers: "+readFrom),false,&terminate);
    CString params=_T(" -texheaders -silent -exclude=\"")+ReturnLongDir(filebank.dir)+_T("exclude.lst\" \"") + readFrom+_T("\" \"")+readFrom+_T("\"");

    if (!RunApp(ReturnLongDir(binarize.dir)+binarize.appName,params,filebank.dir))
      return EM_Binarize_Not_Found;

    CString prefix; 
    if (autoPrefix) prefix=  temp.addonTemp;
    else prefix=addonPrefix;
    params=_T(" -exclude \"")+ReturnLongDir(filebank.dir)+_T("exclude.lst\" -property prefix=\"")+ prefix +_T("\" \"")+readFrom+_T("\" \"")+readFrom+_T("\"");
    if (statFce) statFce(_T("Packing ")+project.name+_T(".pbo"),false,&terminate);
    if (terminate) return EM_Terminated_By_User;
    _tremove(ReturnShortDir(readFrom)+_T(".pbo"));
    if (!RunApp(filebank.appName,params,filebank.dir))
      return EM_FileBank_Not_Found;

    if (statFce) statFce(_T("Packing ")+project.name+_T(".pbo"),false,&terminate);
    if (terminate) return EM_Terminated_By_User;
  }
  int result;
  if ((result=CopyPBO(readFrom,statFce))!=EM_OK) return result;
  return EM_OK;
}
///////////////////////////////////////////////////////////////////////////////
// copy PBO from temp folder to dest folder
int CPackerAlgs::CopyPBO(CString readFrom,ReturnPackingStat statFce)
{
  bool terminate=false;
  CString fileName=project.name+_T(".pbo");

  if (statFce) statFce(_T("Copy ")+project.name+_T(".pbo ")+project.dest,false,&terminate);
  if (terminate) return EM_Terminated_By_User;

  if (readFrom.CompareNoCase(ReturnLongDir(project.dest)+project.name)!=0)
  {
    _tremove(ReturnLongDir(project.dest)+fileName);
    if (errno==EACCES) return EM_Unable_delete_PBO;
    if (_trename(readFrom+_T(".pbo"),ReturnLongDir(project.dest)+fileName)==-1) return EM_Unable_Copy_PBO;
  }
  if (createSignature)
    {
      if (statFce) statFce(_T("Creating Bi signature"),false,&terminate);
      if (terminate) return EM_Terminated_By_User;
      CString params=_T(" \"")+signatureFile+_T("\" \"")+ReturnLongDir(project.dest)+project.name+_T(".pbo\"");
      if (!RunApp(dsSignFile.appName,params,dsSignFile.dir))
        return  EM_DSSignFile_Not_Found;
    }
  if (statFce) statFce(_T("Ready"),false,NULL);
  return EM_OK;
}
///////////////////////////////////////////////////////////////////////////////
// remove all from end of string to char "\" include it;
CString RemoveLastFolder(CString _path,CString *_folder)
{
  CString path =ReturnShortDir(_path);
  TCHAR *dir = path.GetBuffer();
  int i;
  for (i=(int)_tcslen(dir)-1;dir[i]!=_T('\\');--i);
  if (_folder)
  {
    *_folder= path.Mid(i+1);
  }
  return path.Left(i);
}
///////////////////////////////////////////////////////////////////////////////
// parse a string of ([*|?|somechars]) to regular expresion and save to string array
void CPackerAlgs::ReadWhiteList(CString _data)
{
  if (_data==_T("")) return;
  TCHAR * pos= _data.GetBuffer(),*first = pos;
  for(;;pos++)
  {
    if ((*pos==_T(';'))||(*pos==_T('\0'))||(*pos==_T('\n')))
    {
      bool last=(*pos==_T('\0'));
      *pos=_T('\0');
      CString item = CString(first);
      item.Replace(_T("."),_T("\\."));
      item.Replace(_T("*"),_T(".*"));
      item.Replace(_T('?'),_T('.'));
      whiteList.Add(item);
      if (last) break;
      first =++pos;
    }
   if (*pos==_T('\0')) break; 
  }
  _data.ReleaseBuffer();
}
///////////////////////////////////////////////////////////////////////////////
void MakeLongDir(CString &_dir)
{
 if ((_dir.GetLength()>0)&&(_dir[_dir.GetLength()-1]!='\\')) _dir+='\\';
}
///////////////////////////////////////////////////////////////////////////////
void MakeShortDir(CString &_dir)
{
 if ((_dir.GetLength()>0)&&(_dir[_dir.GetLength()-1]=='\\')) _dir.Left(_dir.GetLength()-1);
}
///////////////////////////////////////////////////////////////////////////////
CString ReturnLongDir(CString _dir)
{
 if ((_dir.
   GetLength()>0)&&(_dir[_dir.GetLength()-1]!='\\')) 
   return _dir+='\\';
 return _dir;
}
///////////////////////////////////////////////////////////////////////////////
CString ReturnShortDir(CString _dir)
{
 if ((_dir.GetLength()>0)&&(_dir[_dir.GetLength()-1]=='\\')) 
   return _dir.Left(_dir.GetLength()-1);
 return _dir;
}
///////////////////////////////////////////////////////////////////////////////
CString CPackerAlgs::GetErrorText(ErrorResults _erVal) 
{
  if      (_erVal==EM_OK)                   return _T("");
  else if (_erVal==EM_Terminated_By_User)   return _T("");
  else if (_erVal==-1)                      return _T("Unable to open thread - internal error");
  else if (_erVal==EM_Temp_Not_Exist)       return _T("Temporary directory not found!");
  else if (_erVal==EM_Source_Not_Exist)     return _T("Source directory not found!");
  else if (_erVal==EM_Copy_File_Error)      return _T("Cannot Copy Files!");
  else if (_erVal==EM_Binarize_Not_Found)   return _T("Required application Binarize not found in ")+binarize.dir;
  else if (_erVal==EM_CfgConvert_Not_Found) return _T("Required application CfgConvert not found in ")+cfgConvert.dir;
  else if (_erVal==EM_FileBank_Not_Found)   return _T("Required application FileBank not found in ")+filebank.dir;
  else if (_erVal==EM_DSSignFile_Not_Found) return _T("Required application DSSignFile not found in ")+dsSignFile.dir;
  else if (_erVal==EM_Unable_delete_PBO)    return _T("Unable to replace ")+project.name+_T(".PBO in ")+project.dest;
  else if (_erVal==EM_Unable_Copy_PBO)      return _T("Unable to copy ")+project.name+_T(".PBO to ")+project.dest;
  else if (_erVal==EM_Project_No_Subpath_Source) return _T("Project is not subfolder in source path");
  else                                      return _T("");
}
///////////////////////////////////////////////////////////////////////////////


