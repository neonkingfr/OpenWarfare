#include "StdAfx.h"
#include "WinRegistry.h"
///////////////////////////////////////////////////////////////////////////////
CWinRegistry::CWinRegistry(HKEY _root,CString _path)
{
  if (RegCreateKeyEx(_root,_path,0,0,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,0,&m_key,0)!= ERROR_SUCCESS) 
   m_key=0;
}
///////////////////////////////////////////////////////////////////////////////
CWinRegistry::~CWinRegistry(void)
{
  if (m_key) RegCloseKey (m_key);
}
///////////////////////////////////////////////////////////////////////////////
bool CWinRegistry::SetValueSTR(CString _valuename,CString _value) 
{
 TCHAR lpData[_MAX_PATH];
 lstrcpy(lpData,_value.GetBuffer());
 _value.ReleaseBuffer();
  return ((m_key)&&(RegSetValueEx(m_key, _valuename,0, REG_SZ ,(BYTE*) lpData,_MAX_PATH) == ERROR_SUCCESS)!=0);
}
///////////////////////////////////////////////////////////////////////////////
bool CWinRegistry::GetValueSTR(CString _valuename,CString &_value) 
{
  TCHAR lpData[_MAX_PATH+1];
  DWORD dataSize = _MAX_PATH;
  if(m_key&&(RegQueryValueEx(m_key,_valuename,0,0,(BYTE*) lpData,&dataSize)) == ERROR_SUCCESS)
    {
      _value= CString(lpData);
      return 0;
    }
  _value= CString(_T(""));
  return 1;
}

///////////////////////////////////////////////////////////////////////////////
CString CWinRegistry::GetValue(CString _valuename) 
{
  CString value;
  GetValueSTR(_valuename,value);
  return value;
}
///////////////////////////////////////////////////////////////////////////////
CString CWinRegistry::GetArmaPath(CString _path) 
{
  CWinRegistry LmReg(HKEY_LOCAL_MACHINE,_path);
  CString result;
  LmReg.GetValueSTR(_T("MAIN"),result);
  return result;
}
///////////////////////////////////////////////////////////////////////////////
