#include "bintree.h"

//every type stored in binTree should have:
//1. typedef KeyType ... needed when using default KeyTypeSpec in BinTreeKeyTraits
//2. if used with BinTreeKeyTraits, ValueBinTreeKeyTraits, RefBinTreeKeyTraits
//   then also operator < and ==
//        and method: KeyType GetKey()
struct TrialType
{
  typedef int KeyType;
  int a;
  float b;
  KeyType GetKey() {return a;};

  bool operator<(const TrialType &x) const {return a<x.a; }
  //bool operator>(const TrialType &x) const {return a>x.a; }
  bool operator==(const TrialType &x) const {return a==x.a; }
};

struct TrialTypeRef : public RefCount
{
  typedef int KeyType;
  int a;
  float b;
  KeyType GetKey() {return a;};

  bool operator<(const TrialTypeRef &x) const {return a<x.a; }
  //bool operator>(const TrialTypeRef &x) const {return a>x.a; }
  bool operator==(const TrialTypeRef &x) const {return a==x.a; }
};

Ref<TrialTypeRef> trialFce()
{
  typedef BinTree<Ref<TrialTypeRef>, RefBinTreeKeyTraits<Ref<TrialTypeRef> > > MyTree;
  //typedef BinTree<Ref<TrialTypeRef>, BinTreeKeyTraits<Ref<TrialTypeRef>,int> > MyTree;
  MyTree refTree;
  Ref<TrialTypeRef> x = new TrialTypeRef; x->a=10; x->b=10; refTree.Insert(x);
  Ref<TrialTypeRef> y = new TrialTypeRef; y->a=12; y->b=12; refTree.Insert(y);
  Ref<TrialTypeRef> z = new TrialTypeRef; z->a=13; z->b=13; refTree.Insert(z);
  Ref<TrialTypeRef> q = new TrialTypeRef; q->a=11; q->b=11; refTree.Insert(q);
  Ref<TrialTypeRef> w = new TrialTypeRef; w->a=5; w->b=5; refTree.Insert(w);
  MyTree::Iterator refIterator(refTree, BTMInOrder);
  Ref<TrialTypeRef> refItem;
  printf("Ordered list:\n");
  while (refIterator.Next(refItem)) printf("item = %d, %f\n", refItem->a, refItem->b);

  int max = (*refTree.GetMax())->a;
  int min = (*refTree.GetMin())->a;
  int less = (*refTree.GetFirstLess(10))->a;
  int greater = (*refTree.GetFirstGreater(10))->a;
  int ge = (*refTree.GetFirstGreaterOrEqual(10))->a;
  int le = (*refTree.GetFirstLessOrEqual(10))->a;
  printf("10 is: >%d, <%d, <=%d, >=%d\nmax=%d, min=%d\n", less, greater, ge, le, max, min);

  int k=5;
  const Ref<TrialTypeRef> *item = refTree.FindKey(k);
  if (item)
    printf("item with key=%d has been found\n", k);
  else 
    printf("item with key=%d has NOT been found\n", k);

  return x;
}

int main()
{
  typedef BinTree<TrialType, ValueBinTreeKeyTraits<TrialType> > ValueTree;
  ValueTree tree;
  TrialType d; d.a = 10; d.b = 10.0f;
  tree.Insert(d);
  TrialType c; c.a = 12; c.b = 12.0f;
  tree.Insert(c);
  TrialType b; b.a = 13; b.b = 13.0f;
  tree.Insert(b);
  TrialType a; a.a = 11; a.b = 11.0f;
  tree.Insert(a);
  a.a = 5; a.b = 5.0f;
  tree.Insert(a);
  //ok
  tree.Delete(d);
  //ok
  ValueTree::Iterator iterator(tree, BTMInOrder);
  TrialType item;
  printf("Ordered list:\n");
  while (iterator.Next(item)) printf("item = %d, %f\n", item.a, item.b);

  Ref<TrialTypeRef> x = trialFce(); //ok: testing memory leaks

  return 0;
}
