#pragma once

#define _WIN32_WINDOWS 0x0500

#include <windows.h>

namespace Framework
{
    class Form
    {
    public:
        typedef struct tagXWindow 
        {
            char* className;        // Class name
            HINSTANCE hInst;        // Handle to the current instance of the application
            HWND hWnd;              // Handle to window, handle that uniquely identifies the window
            SIZE size;              // Window size
            RECT rect;              // Client rectangle
            bool isCreated;

        public: 
            tagXWindow(void) 
            { 
                isCreated = false;
            };
        };

        typedef struct Events
        {
        private:
            enum { MAX_KEYS = 256 };
        public:
            bool LeftMouse;
            bool RightMouse;
            bool MouseMove;
            char MouseWheel;
            short X;
            short Y;
            bool Keys[MAX_KEYS];

        public:
            Events(void) 
            {  
                LeftMouse = RightMouse = false;
                MouseWheel = 0;
                memset(Keys, 0, sizeof(Keys));
            };
        } *PEvents;

    protected:
        tagXWindow* _pWndParams;
        Events* _pEvents;

    public:
        Form(HINSTANCE hInst, const SIZE &size, const char* className = "Form_class");
        ~Form(void);

        // Do system commands
        int DoEvents(void);
        // Form created
        bool IsCreated(void) const { return _pWndParams->isCreated; }
        // Show window
        BOOL Show(int nCmdShow = SW_SHOWDEFAULT);    
        // Get client rectangle
        RECT GetClientRectangle(void) { return _pWndParams->rect; }
        // Events message handler
        PEvents GetEvents(void) { return _pEvents; }
        // Hwnd
        HWND GetHandle(void) const { return _pWndParams->hWnd; }
        // Window width
        UINT GetWidth(void) const { return _pWndParams->size.cx; }
        // Window height
        UINT GetHeight(void) const { return _pWndParams->size.cy; }

    protected:
        static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
        void Create(HWND hWndParent);

    private:
        void OnWindowSizeChange(void);
        void OnWindowClose(void);
        void OnKeyDown(WPARAM wParam, LPARAM lParam);
        void OnKeyUp(WPARAM wParam, LPARAM lParam);
        void OnMouseDown(UINT &uMsg, LPARAM lParam);
        void OnMouseUp(UINT &uMsg, LPARAM lParam);
        void OnMouseMove(LPARAM lParam);
        void OnMouseWheel(WPARAM wParam);
    };
}