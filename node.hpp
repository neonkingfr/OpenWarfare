#ifdef _MSC_VER
#  pragma once
#endif

/*
    node.hpp

    Abstract hierarchy node
    @since 28.10.2001

    13.11.2001, PE

    Copyright (C) 2001 by BIStudio (www.bistudio.com)
*/

#ifndef _NODE_H
#define _NODE_H

/**
    DAG node used both in type- and in scene- hierarchies.
*/
class Node : public RefCount {

protected:

    Ref<Node> ancestor;                     /// hierarchy ancestor (attribute inheritance, DAG traversal)

    Ref<Node> type;                         /// associated type node

public:

    Node ();

    virtual ~Node ();

    /**
        @name   Attributes
        General access to Node's attributes.
    */
    //@{

    /**
        Locks the attribute value for R/O access.
            Invokes watch-dog pre-synchronization.
        @param  Id attribute Id
        @return NULL in case of invalid access (invalid Id)
        @see    lockValueRW()
        @see    unlockValue()
    */
    template <class Type>
    virtual const Type* lockValueRO ( int Id );

    /**
        Locks one item of the attribute value array for R/O access.
            Invokes watch-dog pre-synchronization.
        @param  Id attribute Id
        @param  i zero-based array index
        @return NULL in case of invalid access (invalid Id, scalar attribute, index out of range)
        @see    lockValueRW(int)
        @see    unlockValue(int)
    */
    template <class Type>
    virtual const Type* lockValueRO ( int Id, int i );

    /**
        Unlocks the attribute value.
            Invokes watch-dog post-synchronization.
        @param  Id attribute Id
    */
    virtual void unlockValue ( int Id );

    /**
        Unlocks one item of the attribute value array.
            Invokes watch-dog post-synchronization.
        @param  Id attribute Id.
        @param  i zero-based array index.
    */
    virtual void unlockValue ( int Id, int i );

    /**
        Retrieves the actual attribute value.
            Invokes watch-dog synchronizations.
        @param  Id attribute Id.
        @param  dest Destination pointer (able to hold attribute data).
    */
    template <class Type>
    void get ( int Id, Type *dest );

    /**
        Changes the actual attribute value.
            Invokes watch-dog synchronizations.
        @param  Id attribute Id.
        @param  src Source pointer (contains attribute data).
    */
    template <class Type>
    void put ( int Id, Type *src );

    //@}

    /**
        @name   Children
        Access to Node's children.
    */
    //@{

    //@}

protected:

    // implementation: local hash-map [Id -> AttributeValue]

    };

#endif
