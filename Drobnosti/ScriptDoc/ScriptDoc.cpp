// CfgConvert.cpp : Defines the entry point for the console application.
//

#include <El/elementpch.hpp>
#include <El/ParamFile/paramFile.hpp>
#include <El/ParamXML/paramXML.hpp>
#include <Es/Files/fileContainer.hpp>

#include <stdio.h>

struct ParseFolderContext
{
  ParamFile &_doc;

  ParseFolderContext(ParamFile &doc):_doc(doc){}
};

static bool ParseFileDoc(const FileItem &item, ParseFolderContext &ctx)
{
  // preprocessing could make parsing more reliable
  // we are looking for a function definitions in a form like:
  /*
   GameFunction( GameScalar, "random", Random, GameScalar ),
   GameOperator( GameBool, "and", logicAnd, BoolAnd, GameBool, GameBool ),
   GameNular( GameVoid, "nil", VoidNil ),
  */
  /* A very simple way to create a documentation is to scan
  current evaluator tables and write everything into a ParamFile.
  However this way text docs need to be present runtime, otherwise
  they cannot be known during documentation.
  */

  return false;
}

static void ParseFolder(ParamFile &doc, const char *arg)
{
  ParseFolderContext ctx(doc);
  ForEachFileR(arg,ParseFileDoc,ctx);

}

int main(int argc, char* argv[])
{
	argv++;
	argc--;
  ParamFile doc;

	const char *dst = NULL; 
	bool nextDst = false;

	if (argc < 1) goto Usage;
	while (argc>0)
	{
		const char *arg = *argv++;
		argc--;
		if (*arg == '-' || *arg == '/')
		{
			arg++; // skip dash
			if (!stricmp(arg,"dst")) nextDst = true;
			goto Usage;
		}
		else if (nextDst)
		{
			dst = arg;
			nextDst = false;
		}
		else
		{
      ParseFolder(doc,arg);
		}
	}
  if (dst)
  {
    SaveXML(doc,dst);
  }
  else
  {
    SaveXML(doc,stdout);
  }
	return 0;

Usage:
	printf("Usage\n");
	printf("   scriptDoc <directory> {<directory>}\n");
	printf("\n");
	printf("GameFunction declaration are searched\n");
	return 1;		
}
