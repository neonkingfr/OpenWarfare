//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDS_CAPTION                     1
#define IDS_NOT_INSTALLED               2
#define IDS_CAPTION_EXT                 3
#define IDS_MISSING_FILE                4
#define IDS_ERROR_IN_FILE               5
#define IDS_DONE_OK                     6
#define IDS_DONE                        7
#define IDS_NO_INPUT                    8
#define IDD_DIALOG1                     101
#define IDD_CONSOLE                     101
#define IDI_ICON1                       102
#define IDC_CONSOLE_EDIT                1000
#define IDC_CONSOLE_PROGRESS            1001

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
