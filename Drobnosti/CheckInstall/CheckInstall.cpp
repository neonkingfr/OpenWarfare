#include "stdafx.h"
#include "io.h"
#include <class/array.hpp>
#include "resource.h"
#include "fileMap.hpp"
#include <class/filenames.hpp>
#include "consoleWindow.hpp"


#define RES_TYPE MAKEINTRESOURCE(RT_RCDATA)

#pragma comment(lib,"version")

class CRCCalculator
{
	unsigned long _table[256];

	void InitTable();

	public:
	CRCCalculator();
	// sequential interface
	unsigned int CRC(const unsigned char *data, int size);
};

CRCCalculator::CRCCalculator()
{
	InitTable();
}

unsigned int CRCCalculator::CRC(const unsigned char *data, int size)
{
	unsigned int crc = ~0; // preload shift register, per CRC-32 spec
	while (--size>=0)
	{
		crc = (crc<<8) ^ _table[(crc>>24) ^ *data++];
	}
	return ~crc;
}

void CRCCalculator::InitTable()
{
	const int CRCPoly=0x04c11db7; // AUTODIN II, Ethernet, & FDDI
	for( int i=0; i<256; i++ )
	{
		unsigned long c;
		int j;
		for (c = i << 24, j = 8; j > 0; --j)
		{
			c = c & 0x80000000 ? (c << 1) ^ CRCPoly : (c << 1);
		}
		_table[i] = c;
	}
}



void ErrorMessage(HINSTANCE hInstance, const char *buffer, int icon = MB_ICONERROR)
{
	char caption[256];
	LoadString(hInstance, IDS_CAPTION, caption, sizeof(caption));
	LogF("%s",buffer);
	MessageBox(NULL, buffer, caption, MB_OK | icon);
}

static bool FileExist(const char *name)
{
	// check normal file existence
	HANDLE check=::CreateFile
	(
		name,GENERIC_READ,FILE_SHARE_READ,
		NULL,OPEN_EXISTING,0,NULL
	);
	if( check==INVALID_HANDLE_VALUE ) return false;
	::CloseHandle(check);
	return true;
}

static void GetAppVersion(char *app, char *version)
{
	DWORD handle;
	DWORD bufferSize = GetFileVersionInfoSize(app, &handle);
	void *data = NULL;
	unsigned int dataSize = 0;
	char *buffer = new char[bufferSize];
	BOOL ok = GetFileVersionInfo(app, handle, bufferSize, buffer);
	if (ok)
	{
		VerQueryValue(buffer, TEXT("\\StringFileInfo\\040904b0\\FileVersion"), &data, &dataSize);
		strncpy(version, (char *)data, dataSize);
	}
	version[dataSize] = 0;
	delete [] buffer;
}


#define AppDisplayName "Operation Flashpoint"
#define AppName "OperationFlashpoint.exe"
#define ConfigApp "Software\\Codemasters\\" AppDisplayName

// get OFP path from registry
#if _DEBUG

static BOOL CALLBACK EnumResNames(HMODULE hModule, LPCTSTR lpType, LPTSTR lpName, LONG lParam)
{ 
   // Write the resource name to a resource information file. 
   // The name may be a string or an unsigned decimal 
   // integer, so test before printing. 

  if ((ULONG)lpName & 0xFFFF0000) 
  { 
      LogF("\tName: %s", lpName);
  } 
  else 
  { 
      LogF("\tName: %u", (USHORT)lpName); 
  } 

  return TRUE; 
} 

static BOOL CALLBACK EnumResTypes( HMODULE hModule, LPTSTR lpType, LONG lParam )
{
	if ((ULONG)lpType & 0xFFFF0000) 
	{ 
		LogF("Type: %s", lpType); 
	} 
	else 
	{ 
		LogF("Type: %u", (USHORT)lpType); 
	}


  EnumResourceNames(hModule, lpType, EnumResNames, lParam); 
 
  return TRUE; 
}

#endif

static const char *OpenResourceText(const char *txt)
{
	char upName[256];
	strcpy(upName,txt);
	strupr(upName);
	HRSRC hRsc = FindResource(NULL, upName, RES_TYPE);
	if (!hRsc)
	{
		return NULL;
	}

	HGLOBAL hMem = LoadResource(NULL, hRsc);
	void *ptr = LockResource(hMem);
	int size = SizeofResource(NULL, hRsc);
	if (size<=0) return NULL;
	char *cptr = new char[size+1];
	memcpy(cptr,ptr,size);
	cptr[size] = 0;
	return cptr;
}

int WINAPI WinMain
(
  HINSTANCE hInstance,  // handle to current instance
  HINSTANCE hPrevInstance,  // handle to previous instance
  LPSTR lpCmdLine,      // pointer to command line
  int nCmdShow          // show state of window
)
{
	HMODULE hmod = GetModuleHandle(NULL);
	
	#if _DEBUG
	EnumResourceTypes(hmod,EnumResTypes,NULL);
	#endif

	SRef<ConsoleWindow> console = new ConsoleWindow;
	console->Create(hInstance);

	console->SetTitle(IDS_CAPTION);

	HKEY key;
	char dir[256];
	DWORD dirSize = sizeof(dir);
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, ConfigApp, 0, KEY_READ, &key) != ERROR_SUCCESS)
	{
		char buffer[1024];
		LoadString(hInstance, IDS_NOT_INSTALLED, buffer, sizeof(buffer));
		ErrorMessage(hInstance, buffer);
		return 0;
	}
	if (RegQueryValueEx(key, "MAIN", NULL, NULL, (BYTE *)dir, &dirSize) !=  ERROR_SUCCESS)
	{
		char buffer[1024];
		LoadString(hInstance, IDS_NOT_INSTALLED, buffer, sizeof(buffer));
		ErrorMessage(hInstance, buffer);
		RegCloseKey(key);
		return 0;
	}
	RegCloseKey(key);
	if (dir[strlen(dir) - 1] != '\\') strcat(dir, "\\");

	// check exe version

	char app[256];
	strcpy(app, dir);
	strcat(app, AppName);
	if (!FileExist(app))
	{
		char buffer[1024];
		LoadString(hInstance, IDS_NOT_INSTALLED, buffer, sizeof(buffer));
		ErrorMessage(hInstance, buffer);
		return 1;
	}
	char version[256];
	GetAppVersion(app, version);
	if (!*version)
	{
		char buffer[1024];
		LoadString(hInstance, IDS_NOT_INSTALLED, buffer, sizeof(buffer));
		ErrorMessage(hInstance, buffer);
		return 1;
	}

	int verMajor = 0, verMinor = 0;
	{
		char *end;
		verMajor = strtoul(version,&end,10);
		if (*end!='.')
		{
			ErrorMessage(hInstance, "Internal error: unrecognized exe version format");
			return 1;
		}
		verMinor = strtoul(end+1,&end,10);
		Log("Version original: %d.%02d",verMajor,verMinor);
	}

	console->SetTitle(IDS_CAPTION_EXT,AppDisplayName,verMajor,verMinor);
	char inputPath[256];
	sprintf(inputPath,"crc\\%d.%02d\\crc.crc",verMajor,verMinor);

	const char *f = OpenResourceText(inputPath);
	if (f)
	{
		console->EnableOK(false);
		bool noError = true;
		bool printDone = true;
		CRCCalculator cc;
		for(;;)
		{
			char line[1024];
			char *eol = strchr(f,'\n');
			if (!eol) break;
			strncpy(line,f,eol-f);
			line[eol-f] = 0;
			f = eol+1;
			if (*line && line[strlen(line)-1]=='\r') line[strlen(line)-1]=0;

			char *sp = strchr(line,' ');
			if (!sp) continue;
			*sp++=0;
			char *end;
			AutoArray<unsigned int> infoCrc;
			infoCrc.Add(strtoul(sp,&end,16));
			while (isspace(*end))
			{
				while (isspace(*end)) end++;
				if (isxdigit(*end))
				{
					infoCrc.Add(strtoul(end,&end,16));
				}
			}
			//info.name = line;

			const char *sname = line;
			bool addon = (*sname=='+');
			if (addon) sname++;
	
			char fullname[1024];
			strcpy(fullname,dir);
			strcat(fullname,sname);
			FileBufferMapped file;
			file.Open(fullname);
			console->SetProgress("Checking file %s",sname);
			console->ProcessMessages();
			if (file.GetError() )
			{
				if (!addon)
				{
					console->PrintF(IDS_MISSING_FILE,sname);
					noError = false;
				}
			}
			else
			{
				if (infoCrc.Size()>0)
				{
					unsigned int crc = cc.CRC((unsigned char *)file.GetData(),file.GetSize());
					bool match = false;
					for (int i=0; i<infoCrc.Size(); i++)
					{
						if (crc==infoCrc[i]) match = true;
					}
					if (!match)
					{
						console->PrintF(IDS_ERROR_IN_FILE,sname,infoCrc[0],crc);
						noError = false;
					}
				}
			}
			console->ProcessMessages();
			if (console->Closed())
			{
				printDone = false;
				//console->PrintF(IDS_ABORT"User aborted verification.\r\n");
				break;
			}
		}
		console->SetProgress("");
		if (printDone)
		{
			if (noError)
			{
				console->PrintF(IDS_DONE_OK);
			}
			else
			{
				console->PrintF(IDS_DONE);
			}
		}
		console->EnableOK(true);
		//fclose(f);
	}
	else
	{
		console->PrintF(IDS_NO_INPUT,inputPath);
	}


	console->WaitForClosing();

	return 0;
}

void LogF( const char *format, ... )
{
	char buf[512];
		
	va_list arglist;
	va_start( arglist, format );
	vsprintf(buf,format,arglist);
	va_end( arglist );

	strcat(buf,"\n");
	OutputDebugString(buf);
}
