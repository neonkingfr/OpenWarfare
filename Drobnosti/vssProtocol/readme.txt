Command line:
   C:\Program Files\Internet Explorer\IEXPLORE.EXE
Arguments:
   vcs://Pgm/
   
Install:
  debugOn.reg  ... change registry so that vcs: protocol 
			handler will be W:\C\Drobnosti\vssProtocol\Debug\vcsProtocol.dll
  debugOff.reg ... reset vcs: protocol handler to installed release
			(vcsProtocol should be installed from U:\Tools\vcsProtocol\vcsProtocol.inf)
