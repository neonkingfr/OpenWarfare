#include <Es/essencepch.hpp>
#include "dirFormat.hpp"
#include <Es/Algorithms/qsort.hpp>
#include <Es/Strings/bString.hpp>

static const char DirTitHeader[]=
  "<HTML>\n"
  "<HEAD>\n"
  "<META NAME=\"GENERATOR\" Content=\"vcs Protocol Handler\">\n"
  "<META HTTP-EQUIV=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n"
  "<TITLE>"
;

static const char DirHeader[]=
  "</TITLE>\n"
  "</HEAD>\n"
  "<BODY>\n"
  "<PRE>\n"
;

static const char ParentHeader[]=
  "[parent] <a href=\"..\">..</a>\n"
;

static const char DirFooter[]=
  "</PRE>\n"
  "</BODY>"
  "</HTML>"
;

static const char ItemDirFormat[]=  "[dir]    <A HREF=\"%s/\">%s</A>\n";
static const char ItemFileFormat[]= "         <A HREF=\"%s\">%s</A>\n";


static int CmpVSSItemEntry(const VSSItemEntry *e1, const VSSItemEntry *e2)
{
  int diff = e2->_isFolder-e1->_isFolder;
  if (diff) return diff;
  return strcmpi(e1->_name,e2->_name);
}

RString VSSDirText::GetText(bool isRoot, RString file)
{
  QSort(_items.Data(),_items.Size(),CmpVSSItemEntry);

  RString ret = RString(DirTitHeader) + file + RString(DirHeader);

  if (!isRoot)
  {
    ret = ret + ParentHeader;
  }

  for (int i=0; i<_items.Size(); i++)
  {
    const VSSItemEntry &entry = _items[i];
    BString<256> formattedName;                
    const char *name = entry._name;
    if (entry._isFolder)
    {
      sprintf(formattedName,ItemDirFormat,(const char *)name,(const char *)name);
    }
    else
    {
      sprintf(formattedName,ItemFileFormat,(const char *)name,(const char *)name);
    }

    ret = ret + formattedName.cstr();

  }

  ret = ret + DirFooter;
  return ret;
}
