#ifdef _MSC_VER
#pragma once
#endif

#ifndef _DIR_FORMAT_HPP
#define _DIR_FORMAT_HPP

#include <Es/Strings/rString.hpp>
#include <Es/Containers/array.hpp>

struct VSSItemEntry
{
  RString _name;
  bool _isFolder;

  VSSItemEntry(RString name, bool isFolder)
  :_name(name),_isFolder(isFolder)
  {
  }
};

TypeIsMovableZeroed(VSSItemEntry)


/// class responsible for directory browsing
class VSSDirText
{
  AutoArray<VSSItemEntry> _items;

  public:
  void Add(VSSItemEntry &item)
  {
    _items.Add(item);
  }
  RString GetText(bool isRoot, RString file);
};

#endif
