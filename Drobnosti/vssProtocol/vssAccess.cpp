/**
@file
responsible for VSS file retrieval using SS automation
*/

#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <olectl.h>
#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <Es/Types/pointers.hpp>
#include <Es/Strings/rString.hpp>
#include <Es/Common/filenames.hpp>
#include <Es/Containers/array.hpp>
#include <El/QStream/QStream.hpp>
#include <comdef.h>
#include <atlbase.h>
#include <WinInet.h>
#include "ssauto.h"

#include "dirFormat.hpp"

static RString ReadBSTR(BSTR str)
{
  size_t reslen = wcstombs(NULL,str,INT_MAX);
  RString res(NULL,reslen);
  char *dst = res.MutableData();
  wcstombs(dst,str,reslen+1);
  return res;
}

static RString PathSlashToBackslash(RString path)
{
  char *dta = path.MutableData();
  while (*dta)
  {
    if (*dta=='/') *dta='\\';
    dta++;
  }
  return path;
}

static void UnCanonicalizePath(RString &path)
{
  //InternetCanonicalizeUrlW(pwzUrl,pwzResult,pcchResult,ICU_ESCAPE|ICU_DECODE);
  URL_COMPONENTS urlComp = {0};
  urlComp.dwStructSize = sizeof(URL_COMPONENTSW);
  const int bufflen = 1024;
  urlComp.dwUrlPathLength=bufflen;
  urlComp.lpszUrlPath=new char[bufflen];
  RString longPath = RString("x://x/") + path;
  if (!InternetCrackUrl(longPath.Data(), longPath.GetLength(), ICU_ESCAPE,  &urlComp)) {
    HRESULT hr = GetLastError();
    Log("InternetCrackUrl not successful: %x", hr);
  }
  path = RString(urlComp.lpszUrlPath+1);  //skip slash
  delete urlComp.lpszUrlPath;
}

static RString OpenVSSFile
(
  IVSSDatabase *pVSSDBObject, RString dbaseIni, RString file, bool &isFile
)
{
  dbaseIni = dbaseIni;
  RString fileTgt = PathSlashToBackslash(file);
  char dbaseIniPath[MAX_PATH];
  strcpy(dbaseIniPath,dbaseIni);
  TerminateBy(dbaseIniPath,'\\');
  strcat(dbaseIniPath, "srcsafe.ini");
  CComBSTR bsVSSIniFile(dbaseIniPath);

  // Open the sourcesafe database under the current logged on username and (cached) password
  // by entering empty strings.
  HRESULT hr;
  if(SUCCEEDED(hr=pVSSDBObject->Open(bsVSSIniFile, L"", L"")))
  {
    CComPtr<IVSSItem> pIVSSItem;
    UnCanonicalizePath(file);
    CComBSTR bsVSSFile(file); // bsVSSFile("$/Courses/Cursus evaluatie.doc"); //successful trial, subst " " for "%20"
    if(SUCCEEDED(hr=pVSSDBObject->get_VSSItem(bsVSSFile, 0, &pIVSSItem)))
    {
      int nItemType = -1;
      // Test if the VSS item is a file and not a project.
      VARIANT_BOOL vbDeleted;
      if (SUCCEEDED(hr = pIVSSItem->get_Deleted(&vbDeleted)))
      {
        if (vbDeleted == VARIANT_FALSE)
        {
          if (SUCCEEDED(hr = pIVSSItem->get_Type(&nItemType)))
          {
            if (nItemType==VSSITEM_PROJECT)
            {
              VSSDirText dirText;
              // return project content
              VARIANT_BOOL vbFalse = VARIANT_FALSE;
              CComPtr<IVSSItems> items;
              HRESULT hr = pIVSSItem->get_Items(vbFalse,&items);
              if (FAILED(hr)) return "";
              AutoArray<VSSItemEntry> array;
              LONG count;
              hr = items->get_Count(&count);
              if (FAILED(hr)) return "";
              int i;
              for (i=0; i<count; i++)
              {
                CComVariant ivar = i+1;
                CComPtr<IVSSItem> item;
                hr = items->get_Item(ivar,&item);
                if (FAILED(hr)) continue;
                CComBSTR str;
                hr = item->get_Name(&str);
                if (FAILED(hr)) continue;
                int itype;
                hr = item->get_Type(&itype);
                if (FAILED(hr)) continue;

                dirText.Add(VSSItemEntry(ReadBSTR(str),itype==VSSITEM_PROJECT));
              }

              bool isRoot = !strcmpi(file,"$/");
              RString ret = dirText.GetText(isRoot,file);

              isFile = false;
              return ret;

            }
            else if (nItemType == VSSITEM_FILE)
            {
              char tempPath[MAX_PATH] = {0};
              if (GetTempPath(_MAX_PATH, tempPath) > 0)
              {
                char tempFile[MAX_PATH] = {0};
                GetTempFileName(tempPath,"vss",0,tempFile);
                //const char *tgtName = GetFilenameExt(fileTgt);
                //StrCat(tempFile, tgtName);
                CComBSTR tempFileBS(tempFile);
                
                if (SUCCEEDED(hr=pIVSSItem->Get(&tempFileBS, VSSFLAG_REPREPLACE))) // VSSFLAG_USERROYES | VSSFLAG_REPREPLACE)))
                {
                  // fileTgt is downloaded in the temp - you can read it and delete it
                  isFile = true;
                  return tempFile;
                }
              }
            }
          }
        }
      }
    }

  }
  return RString();
}

static CComPtr<IVSSDatabase> OpenVSSDBase()
{
  HRESULT hr;
  CLSID clsid;
  if(SUCCEEDED(hr=CLSIDFromProgID(L"SourceSafe", &clsid)))
  {
    CComPtr<IVSSDatabase> VSSDBObject;
    if (SUCCEEDED(hr=CoCreateInstance(clsid, NULL, CLSCTX_ALL, IID_IVSSDatabase, (void **)&VSSDBObject)))
    {
      return VSSDBObject;
    }
  }
  return NULL;
}


RString GetVSSFile(RString dbase, RString relPath, bool &isFile)
{
  CComPtr<IVSSDatabase> db = OpenVSSDBase();
  if (!db)
  {
    return RString();
  }

  RString tempFile = OpenVSSFile(db,dbase,relPath,isFile);
  return tempFile;
}
