/**
@file
responsible for VSS file retrieval using SCC interface
*/

#include <Es/essencepch.hpp>
#include <Es/Common/win.h>
#include <Es/Files/filenames.hpp>
#include "dirFormat.hpp"
#include <El/Scc/Scc.hpp>
#include <comdef.h>
#include <atlbase.h>

static RString PathSlashToBackslash(RString path)
{
  char *dta = path.MutableData();
  while (*dta)
  {
    if (*dta=='/') *dta='\\';
    dta++;
  }
  return path;
}

static MsSccFunctions GMsSccFunctions;

RString GetSccFile(RString dbase, RString relPath, bool &isFile)
{
  char tempPath[512];
  char userName[512];
  GetTempPath(sizeof(tempPath),tempPath);
  DWORD userNameSize = sizeof(userName);
  GetUserName(userName,&userNameSize);

  // note: tempPath should never terminate with backslash
  // if it does, SCC functions will not work
  int tempLen = strlen(tempPath);
  if (tempLen>0 && tempPath[tempLen-1]=='\\')
  {
    tempPath[tempLen-1] = 0;
  }

  RString localPath = RString(tempPath);
  SCCRTN ret = GMsSccFunctions.Init(dbase,"$/",localPath,NULL,userName);
  //SCCRTN ret = GMsSccFunctions.Init(dbase,"$/",NULL,NULL,userName);
  if (IS_SCC_ERROR(ret))
  {
    return "";
  }
  char ssLocalPath[1024];
  strcpy(ssLocalPath,GMsSccFunctions.GetLocalPath());
  TerminateBy(ssLocalPath,'\\');

  const char *relSSPath = cc_cast(relPath)+2;
  // convert SS path to local path
  RString localRelPath = ssLocalPath + PathSlashToBackslash(relSSPath);

  int lenSSPath = strlen(relSSPath);
  bool isDir = lenSSPath>0 && relSSPath[lenSSPath-1]=='/';


  if (isDir)
  {
    // based on URL it should be a directory
    ret = GMsSccFunctions.GetLatestVersionDir(localRelPath,true);
    // debugging - return a message string
    RString dirList = Format("Directory %s\n",cc_cast(relPath));
    GMsSccFunctions.Done();
    isFile = false;
    return dirList;
  }
  else
  {
    // try getting it as a file
    ret = GMsSccFunctions.GetLatestVersion(localRelPath);
    // return the path to the file
    GMsSccFunctions.Done();
    return localRelPath;
  }
}
