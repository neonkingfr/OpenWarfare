#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <olectl.h>
#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <Es/Types/pointers.hpp>
#include <Es/Strings/rString.hpp>
#include <Es/Strings/bString.hpp>
//#include <Es/Common/filenames.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Algorithms/qsort.hpp>
#include <El/QStream/QStream.hpp>
#include <El/Enum/enumNames.hpp>
#include <comdef.h>
#include <atlbase.h>
#include <WinInet.h>
#include <msxml/msxml2.h>
#include "ssauto.h"

#pragma comment(lib,"WinInet.lib")
#pragma comment(lib,"msxml2.lib")

//#define CALL_REPORT

//DEFINE_GUID(VssProtocolGUID, 0xfc03991f, 0x4cd5, 0x4623, 0xbe, 0x6e, 0x14, 0xc3, 0xea, 0xd5, 0xcb, 0x56);

static const GUID LIBID_SourceSafeTypeLib = 
{ 0x783CD4E0L,0x9D54,0x11CF, { 0xB8, 0xEE, 0x00, 0x60, 0x8C, 0xC9, 0xA7, 0x1F } };
//{0xfc03991f, 0x4cd5, 0x4623, { 0xbe, 0x6e, 0x14, 0xc3, 0xea, 0xd5, 0xcb, 0x56 } };

static const GUID IID_IVSSDatabase = 
{ 0x2A0DE0E2L,0x2E9F,0x11D0,{0x92,0x36,0x00,0xAA,0x00,0xA1,0xEB,0x95} };

static const GUID VssProtocolGUID = 
{ 0xfc03991f, 0x4cd5, 0x4623, { 0xbe, 0x6e, 0x14, 0xc3, 0xea, 0xd5, 0xcb, 0x56 } };

#define VssProtocolGUIDText "{FC03991F-4CD5-4623-BE6E-14C3EAD5CB56}"

/*
GUID: {FC03991F-4CD5-4623-BE6E-14C3EAD5CB56}

// {FC03991F-4CD5-4623-BE6E-14C3EAD5CB56}

// {FC03991F-4CD5-4623-BE6E-14C3EAD5CB56}
static const GUID <<name>> = 
{ 0xfc03991f, 0x4cd5, 0x4623, { 0xbe, 0x6e, 0x14, 0xc3, 0xea, 0xd5, 0xcb, 0x56 } };

// {FC03991F-4CD5-4623-BE6E-14C3EAD5CB56}
IMPLEMENT_OLECREATE(<<class>>, <<external_name>>, 
0xfc03991f, 0x4cd5, 0x4623, 0xbe, 0x6e, 0x14, 0xc3, 0xea, 0xd5, 0xcb, 0x56);
*/

/*

Implement the IInternetProtocol interface.
Implement the IInternetProtocolRoot interface.
Implement the IClassFactory interface.

Optional. Implement the IInternetProtocolInfo interface. Support for the HTTP protocol is provided by the transaction handler.
If IInternetProtocolInfo is implemented, provide support for PARSE_SECURITY_URL and PARSE_SECURITY_DOMAIN so the URL security zone manager can handle the security properly.
Write the code for your protocol handler.
Provide support for BINDF_NO_UI and BINDF_SILENTOPERATION.
Add a subkey for your protocol handler in the registry under HKEY_CLASSES_ROOT\PROTOCOLS\Handler.
Create a string value, CLSID, under the subkey and set the string to the CLSID of your protocol handler.
*/

#define COM_QUERY(res, InterfaceName) \
  if \
  ( \
    IsEqualGUID(id,IID_##InterfaceName) \
  ) \
  { \
    res = (InterfaceName *)this; \
    AddRef(); \
    return S_OK; \
  }

#define ENUM_VALUE_EX(type,typeprefix,name,mtype) \
  typeprefix##name,

#define ENUM_NAME_EX(type,typeprefix,name,mtype) \
  EnumName(typeprefix##name,#name),

#define ENUM_MEMBER_EX(type,typeprefix,name,mtype) \
  mtype _##typeprefix##name;

//! use enum-definition macro to declare enum names and define enum values
#define DECLARE_ENUM_EX(type,prefix,ENUM_DEF) \
    enum type \
  { \
    ENUM_DEF(type,prefix,ENUM_VALUE_EX) \
    N##type \
  }; 
/*
  template <> \
  const EnumName *GetEnumNames(type dummy); \

//! use enum-definition macro to define enum names
#define DEFINE_ENUM_EX(type,prefix,ENUM_DEF) \
  static const EnumName type##Names[]= \
  { \
    ENUM_DEF(type,prefix,ENUM_NAME_EX) \
    EnumName() \
  }; \
  template <> \
  const EnumName *GetEnumNames(type dummy) {return type##Names;}\
  \
  template <> \
  type GetEnumCount(type value) {return N##type;}
*/
//! use enum-definition macro to define members based on enum
#define DEFINE_ENUM_MEMBERS(type,prefix,ENUM_DEF) ENUM_DEF(type,prefix,ENUM_MEMBER_EX)

#define PARAM_ENUM(type,prefix,XX) \
  XX(type, prefix, Wordwrap, bool) \
  XX(type, prefix, Runshell, bool) \
  XX(type, prefix, Mimetype, RString)

static RString Get_RStringFromIni(const char *section, const char *key, const char *iniFile)
{
  const int len=2048;
  char retval[len];
  GetPrivateProfileString(section, key, "", retval, len, iniFile);
  return retval;
}

static int Get_intFromIni(const char *section, const char *key, const char *iniFile)
{
  return GetPrivateProfileInt(section,key,0,iniFile);
}
static bool Get_boolFromIni(const char *section, const char *key, const char *iniFile)
{
  return GetPrivateProfileInt(section,key,0,iniFile)!=0;
}

DECLARE_ENUM_EX(ParamType,Par,PARAM_ENUM)
/*
DEFINE_ENUM_EX(ParamType,Par,PARAM_ENUM)
*/
/// vss protocol implementation of IInternetProtocol
class VSSInternetProtocol: public RefCount, public IInternetProtocol
{
  int _handle;
  AutoArray<RString> _tempFile;
  RString _iniFile;

  CComPtr<IInternetProtocolSink> _sink;
  int _size;
  LPCWSTR _mimeType;
  int _readFrom;
  QIStrStream _str;
  Buffer<char> _strBuf;

  DEFINE_ENUM_MEMBERS(ParamType,Par,PARAM_ENUM)

  private:
  void Close();
  RString GetVSSFile(RString realPath, bool &isFile);
  RString GetIniEntry(RString section,RString name) const;

  //! get path to database based on its symbolic (domain) name
  RString GetDBasePath(RString name) const;

  /// check SCC database names
  RString GetSccDBasePath(RString name) const;
  

  //! get stylesheet path based on stylesheet spec
  RString GetStylesheet(RString name) const;

  //! converts text file to HTML (wordwrap)
  void TextFileToHTML(RString &filename, RString &path, RString &suggestedMimeType);

  //! get parameters by file extension (from ini file)
  void GetIniFileParams(char *fileExt);

  //! process specific files (by theirs extension) using ShellExecute
  void ExamineAndProcessByExtension(RString filename, char *path, RString &suggestedMimeType);

  //! ReportProgress and ReportData
  HRESULT Report(DWORD grfPI);

  public:
  VSSInternetProtocol();
  ~VSSInternetProtocol();

  // IUnknown
  HRESULT STDMETHODCALLTYPE QueryInterface(REFIID id,void **object);
  ULONG STDMETHODCALLTYPE AddRef() {return RefCount::AddRef();}
  ULONG STDMETHODCALLTYPE Release() {return RefCount::Release();}

  // IInternetProtocolRoot
  virtual HRESULT STDMETHODCALLTYPE Start( 
  /* [in] */ LPCWSTR szUrl,
  /* [in] */ IInternetProtocolSink __RPC_FAR *pOIProtSink,
  /* [in] */ IInternetBindInfo __RPC_FAR *pOIBindInfo,
  /* [in] */ DWORD grfPI,
  /* [in] */ DWORD dwReserved);

  virtual HRESULT STDMETHODCALLTYPE Continue( 
  /* [in] */ PROTOCOLDATA __RPC_FAR *pProtocolData);

  virtual HRESULT STDMETHODCALLTYPE Abort( 
  /* [in] */ HRESULT hrReason,
  /* [in] */ DWORD dwOptions);

  virtual HRESULT STDMETHODCALLTYPE Terminate( 
  /* [in] */ DWORD dwOptions);

  virtual HRESULT STDMETHODCALLTYPE Suspend();

  virtual HRESULT STDMETHODCALLTYPE Resume();

  // IInternetProtocol 
  virtual HRESULT STDMETHODCALLTYPE Read( 
  /* [length_is][size_is][out][in] */ void __RPC_FAR *pv,
  /* [in] */ ULONG cb,
  /* [out] */ ULONG __RPC_FAR *pcbRead);

  virtual HRESULT STDMETHODCALLTYPE Seek( 
  /* [in] */ LARGE_INTEGER dlibMove,
  /* [in] */ DWORD dwOrigin,
  /* [out] */ ULARGE_INTEGER __RPC_FAR *plibNewPosition);

  virtual HRESULT STDMETHODCALLTYPE LockRequest( 
  /* [in] */ DWORD dwOptions);

  virtual HRESULT STDMETHODCALLTYPE UnlockRequest();
};

HRESULT STDMETHODCALLTYPE VSSInternetProtocol::QueryInterface(REFIID id,void **object)
{
  COM_QUERY(*object,IInternetProtocol);
  COM_QUERY(*object,IInternetProtocolRoot);
  COM_QUERY(*object,IUnknown);
  return E_NOINTERFACE;
}

//HKCR,CLSID\%PROTGUID%\Config

static RString GetIniFile()
{
  long  lErr = 0;
  HKEY  hKey;
  if ((lErr = RegOpenKeyEx(HKEY_CLASSES_ROOT,
         _T("CLSID\\" VssProtocolGUIDText "\\Config"),
         0,
         KEY_READ,
         &hKey)) == ERROR_SUCCESS)
  {
    DWORD dwType = REG_SZ;
    char  val[512] = {0};
    DWORD dwSize = 511;

    if ((lErr = RegQueryValueEx(hKey,
          _T("IniFile"),
          NULL,
          &dwType,
          (LPBYTE)val,
          &dwSize)) == ERROR_SUCCESS && dwType == REG_SZ)
    {
      return val;
    }
  }
  return "";
}

VSSInternetProtocol::VSSInternetProtocol()
{
  _handle = -1;

  _iniFile = GetIniFile();

  // scan ini file for database mapping?
}

RString VSSInternetProtocol::GetIniEntry(RString section,RString name) const
{
  char ret[512];
  *ret = 0;
  GetPrivateProfileString(section,name,"",ret,sizeof(ret),_iniFile);
  return ret;
}

RString VSSInternetProtocol::GetDBasePath(RString name) const
{
  return GetIniEntry("databases",name);
}

RString VSSInternetProtocol::GetSccDBasePath(RString name) const
{
  return GetIniEntry("scc",name);
}

RString VSSInternetProtocol::GetStylesheet(RString name) const
{
  return GetIniEntry("stylesheets",name);
}

void VSSInternetProtocol::Close()
{
  Log("vssProtocol: Close handle %d, size= %d", _handle, _tempFile.Size());
  if (_handle>=0)
  {
    close(_handle);
    _handle = -1;
  }
  if (_tempFile.Size()>0)
  {
    for (int i=0; i<_tempFile.Size(); i++)
    {
      int retv = chmod(_tempFile[i],_S_IREAD | _S_IWRITE);
      int retval = unlink(_tempFile[i]);
      int k=2;
    }
    _tempFile.Clear();
  }
  _sink.Release();
}

VSSInternetProtocol::~VSSInternetProtocol()
{
  Close();
}

static void GetFileContent(Buffer<char> &buffer, RString realFile)
{
  buffer.Delete();
  int handle = open(realFile,O_RDONLY|O_BINARY);
  if (handle<0) return;
  size_t size = filelength(handle);
  buffer.Resize(size);
  read(handle,buffer.Data(),size);
  close(handle);
}

static CComPtr<IXMLDOMDocument2> LoadXML(const char *filename, HRESULT &hr)
{
  // more difficult handling - xml parsing required
  CComPtr<IXMLDOMDocument2> xmldoc;
  xmldoc.CoCreateInstance(CLSID_DOMDocument,NULL,CLSCTX_INPROC_SERVER); // Check the return value, hr...
  if (xmldoc==NULL)
  {
    hr = E_NOINTERFACE;
    return xmldoc;
  }


  VARIANT_BOOL ok;
  VARIANT_BOOL vbfalse;
  vbfalse = FALSE;
  xmldoc->put_async(vbfalse);
  xmldoc->put_validateOnParse(vbfalse);

  Buffer<char> xmlContent;
  GetFileContent(xmlContent,filename);

  HGLOBAL mem = GlobalAlloc(GMEM_FIXED,xmlContent.Size());
  IStream *stream;
  CreateStreamOnHGlobal(mem,TRUE,&stream);
  memcpy(mem,xmlContent.Data(),xmlContent.Size());

  _variant_t var;
  var = stream;
  hr = xmldoc->load(var,&ok);

  stream->Release();
  stream = NULL;

  if (hr!=S_OK)
  {
    if (hr>S_OK)
    {
      hr = E_FAIL;
    }
    return NULL;
  }
  return xmldoc;
}

void VSSInternetProtocol::TextFileToHTML(RString &filename, RString &path, RString &suggestedMimeType)
{
  if (_ParWordwrap)
  {
    suggestedMimeType=RString("text/html");
    char tempPath[MAX_PATH] = {0};
    if (GetTempPath(_MAX_PATH, tempPath) > 0)
    {
      FILE *file = fopen(filename,"rt");
      if (file) {
        char tempFile[MAX_PATH] = {0};
        GetTempFileName(tempPath,"vss",0,tempFile);
        FILE *outFile = fopen(tempFile,"w+t");
        if (outFile)
        {
          char line[2048];
          fputs("<tt>\n", outFile);
          while (fgets(line,2048,file))
          {
            char *ptr, *start=line;
            while (*start==' ' || *start=='\t') 
            {
              switch (*start) 
              {
                case ' ' : fputs("&nbsp;", outFile); break;
                case '\t': fputs("&nbsp;&nbsp;", outFile); break;
              }
              start++;
            }
            while (( ptr = strpbrk( start, "<&> \t" ))) 
            {
              char c = *ptr;
              *ptr = 0;
              fputs(start, outFile);
              switch (c) 
              {
                case '<' : fputs("&lt;", outFile); break;
                case '>' : fputs("&gt;", outFile); break;
                case '&' : fputs("&amp;", outFile); break;
                case ' ' : 
                  if (ptr[1]==' ') fputs("&nbsp;", outFile);
                  else             fputs(" ", outFile); 
                  break;
                case '\t': 
                  if (ptr[1]=='\t') fputs("&nbsp;&nbsp;", outFile);
                  else fputs("\t", outFile);
                  break;
              }
              start = ptr+1;
            }
            fputs(start, outFile);
            fputs("<br>\n", outFile);
          }
          fputs("</tt>\n", outFile);
          fclose(outFile);
        }
        fclose(file);
        close(_handle); //opened before call
        chmod(filename, _S_IREAD | _S_IWRITE);
        if (DeleteFile(filename)==FALSE)
        {
          HRESULT hr = GetLastError();
          Log("vssProtocol:  DeleteFile failed %x",hr);
        }
        if (rename(tempFile,filename)) 
        {
          Log("vssProtocol: %s cannot be deleted", filename);
          _handle = open(tempFile, O_RDONLY|O_BINARY);
        }
        else
          _handle = open(filename, O_RDONLY|O_BINARY);
      }
    }
  }
}

void ThreadExecuteAndDelete(LPVOID param)
{
  //intptr_t retval = spawnlp(_P_NOWAIT,"C:\\bis\\tex\\TexView.exe", "TexView.exe", newFile, NULL);
  //CThreadInfo *par = (CThreadInfo*)param;
  //intptr_t retval = spawnlp(_P_WAIT,par->_path, par->_cmd, par->_fileName, NULL);
  char *newFile = (char*)param;
  //HINSTANCE hi = ShellExecute(NULL, "open", newFile, NULL, NULL, SW_NORMAL);
  SHELLEXECUTEINFO execInfo;
  execInfo.cbSize=sizeof(SHELLEXECUTEINFO);
  execInfo.fMask=SEE_MASK_NOCLOSEPROCESS;
  execInfo.hwnd=NULL;
  execInfo.lpVerb="open";
  execInfo.lpFile=newFile;
  execInfo.lpParameters=NULL;
  execInfo.lpDirectory=NULL;
  execInfo.nShow=SW_NORMAL;
/*
  execInfo.hInstApp=NULL;
  execInfo.lpIDList=NULL; //ignored
  execInfo.lpClass=NULL;  //ignored
  execInfo.hkeyClass=NULL; //ignored
  execInfo.dwHotKey=NULL; //ignored
  execInfo.hIcon=NULL; //ignored
*/
  BOOL retval = ShellExecuteEx(&execInfo);
  if (retval)
  {
    Sleep(5000);
    WaitForSingleObject(execInfo.hProcess, INFINITE);
    //intptr_t retval = _cwait(NULL, (intptr_t)(execInfo.hProcess), NULL);
    CloseHandle(execInfo.hProcess); //closed yet
  }
  chmod(newFile, _S_IREAD | _S_IWRITE);
  if (DeleteFile(newFile)==FALSE)
  {
    HRESULT hr = GetLastError();
    Log("vssProtocol:  DeleteFile failed %x",hr);
  }
  //unlink(newFile);
  delete newFile;
  //_endthread();
}

static const char separators[] = ";";
void VSSInternetProtocol::GetIniFileParams(char *fileExt)
{

  char fileTypesTokens[4096];
  DWORD retLen = GetPrivateProfileString("filetypes",NULL,"",fileTypesTokens,4096,_iniFile);

  const char *fileType = "default";
  for (const char *nextFileType=fileTypesTokens; nextFileType<fileTypesTokens+retLen; nextFileType+=strlen(nextFileType)+1)
  {
    char fileExtensionsTokens[4096];
    GetPrivateProfileString("filetypes", nextFileType, "", fileExtensionsTokens, 4096, _iniFile);
    char *extensionToken = strtok(fileExtensionsTokens, separators);
    while (extensionToken!=NULL)
    {
      if (!stricmp(fileExt+1, extensionToken))
      {
        //key nextFileType should be found in ini file as session and processed
        fileType = nextFileType;
        goto Break;
      }
      extensionToken = strtok(NULL,separators);
    }
  }
  Break:
  //fileType for fileExt was found, or default will be used instead

 #define ENUM_GET_MEMBER_EX(type,typeprefix,name,mtype) \
    _##typeprefix##name = Get_##mtype##FromIni(fileType,#name,_iniFile);

  PARAM_ENUM(ParamType,Par,ENUM_GET_MEMBER_EX)

  
//  DWORD paramLen = GetPrivateProfileString(fileType,NULL,"", parameters,4096,_iniFile);
//  for (const char *nextParam=parameters; nextParam<parameters+paramLen; nextParam+=strlen(nextParam)+1)
//  {
//    int key = GetEnumValue<ParamType>(nextParam);
//    if (key>=0)
//    {
//      int value=GetPrivateProfileInt(fileType,nextParam,0,_iniFile);
//      _paramValues[key]=(GetPrivateProfileInt(fileType,nextParam,0,_iniFile)!=0);
//    }
//  }
}

void VSSInternetProtocol::ExamineAndProcessByExtension(RString filename, char *path, RString &suggestedMimeType)
{
  char *fileExt = GetFileExt(GetFilenameExt(path));
  //GetIniFileParams(fileExt);
  if (_ParRunshell) {
    //it's a file ext
    close(_handle);
    BString<1024> newFile;
    strcpy(newFile, filename);
    strcat(newFile, fileExt);
    rename(filename, newFile);
    FILE *file=fopen(filename, "w+t");
    static const char fakeFile[] = "<html><body><b>VSSProtocol information:</b> This file cannot be opened in Internet Explorer!</body></html>\n";
    fputs(fakeFile, file);
    suggestedMimeType = RString("text/html");
    fclose(file);
    _handle = open(filename,O_RDONLY|O_BINARY);
    char *newFileName=new char[strlen(newFile)+1];
    strcpy(newFileName, newFile);
    _beginthread(ThreadExecuteAndDelete, 0, (LPVOID)newFileName);
  }
}

HRESULT VSSInternetProtocol::Report(DWORD grfPI)
{
  PROTOCOLDATA protocolData;
  if (grfPI & PI_FORCE_ASYNC)
  {
    _sink->ReportProgress(BINDSTATUS_VERIFIEDMIMETYPEAVAILABLE, _mimeType);      
    _sink->ReportData(BSCF_LASTDATANOTIFICATION | BSCF_DATAFULLYAVAILABLE,0,_size);
    return S_OK;
  }
  else
  {
    protocolData.cbData=0;
    protocolData.dwState=1;
    protocolData.grfFlags=PI_FORCE_ASYNC;
    protocolData.cbData=0;
    if (_sink) _sink->Switch(&protocolData);
    else return E_INVALIDARG;
    return E_PENDING;
  }
}

RString GetVSSFile(RString dBase, RString relPath, bool &isFile);
RString GetSccFile(RString dBase, RString relPath, bool &isFile);

RString VSSInternetProtocol::GetVSSFile(RString realPath, bool &isFile)
{
  // parse domain/path
  LogF("Getting file: %s", realPath.Data());
  const char *path = strchr(realPath,'/');
  if (!path) return "";
  RString domain = realPath.Substring(0,path-realPath);
  RString relPath = RString("$")+path;

  RString tempFile;
  RString dBase = GetDBasePath(domain);
  if (dBase.GetLength()==0)
  {
    RString sccName = GetSccDBasePath(domain);
    if (sccName.GetLength()==0)
    {
      return sccName;
    }
    tempFile = ::GetSccFile(sccName,relPath,isFile);
  }
  else
  {
    tempFile = ::GetVSSFile(dBase,relPath,isFile);
  }
  if (tempFile.GetLength()>0)
  {
    _tempFile.Add(tempFile);
  }
  return tempFile;
}


// IInternetProtocolRoot
HRESULT STDMETHODCALLTYPE VSSInternetProtocol::Start( 
/* [in] */ LPCWSTR url,
/* [in] */ IInternetProtocolSink __RPC_FAR *sink,
/* [in] */ IInternetBindInfo __RPC_FAR *bindInfo,
/* [in] */ DWORD grfPI,
/* [in] */ DWORD dwReserved)
{
  Log("vssProtocol: Start");
  BINDINFO bind;
  DWORD bindf;
  bind.cbSize = sizeof(bind);
  HRESULT hr = bindInfo->GetBindInfo(&bindf,&bind);
  if (FAILED(hr)) return hr;

  if (bind.dwBindVerb!=BINDVERB_GET)
  {
    return INET_E_INVALID_REQUEST;  
  }

  #if 0
    BINDINFO_OPTIONS_BINDTOOBJECT
    bind.dwOptions
  #endif

  char path[512];
  wcstombs(path,url,sizeof(path)-1);
  RString realPath = path;
  static const char vssPrefix[] = "vcs://";
  if (!strnicmp(path,vssPrefix,strlen(vssPrefix)))
  {
    realPath = path+strlen(vssPrefix);
    //"$/";
  }

  FindArrayKey<RString> args;
  // check if there is any optional argument
  const char *arg = strchr(realPath,'?');
  bool xdoc = false;
  if (arg)
  {
    args.Add(arg+1);
    realPath = realPath.Substring(0,arg-realPath);
    // TODO: parse multiple arguments
    xdoc = true;
  }
  _sink = sink;
  _readFrom = 0;
  //_bind = bindInfo;

  if (!xdoc)
  {
    bool isFile = true;
    RString filename = GetVSSFile(realPath,isFile);
    if (!isFile)
    {
      int len = wcslen(url);
      if (len && url[len-1]!='/') //redirect!
      {
        LPWSTR newurl=new WCHAR[wcslen(url)+2];
        wcscat(wcscpy(newurl,url),L"/");
        sink->ReportProgress(BINDSTATUS_REDIRECTING, newurl);
        delete newurl;
      }
      int reslen = filename.GetLength();
      /* //inserting http header does not help (should be done by BINDSTATUS_MIMETYPEAVAILABLE)      
          RString httpHeader = Format(
            "HTTP/1.1 200 OK\n"
            "Content-Length: %d\n"
            "Connection: close\n"
            "Content-Type: text/html\n\n", reslen
          );
          RString fileWithHTTPHeader = httpHeader + filename;
          reslen = fileWithHTTPHeader.GetLength();
      */
      _strBuf.Resize(reslen);
      memcpy(_strBuf.Data(),filename,reslen);
      //memcpy(_strBuf.Data(), fileWithHTTPHeader, reslen);
      _str.init(_strBuf.Data(),_strBuf.Size());
      int size = reslen;
      //int size = reslen|0xf; //this operation magically worked due to incorrect ussage of first parameter in sink->ReportData(size,0,size); 
      
      #ifdef CALL_REPORT
        _mimeType = L"text/html";
        _size = size;
        Report(grfPI);
      #else
        //notify protocol sink the content mime type
        LPCWSTR wcMimeType = L"text/html";
        sink->ReportProgress(BINDSTATUS_VERIFIEDMIMETYPEAVAILABLE, wcMimeType);     
        sink->ReportData(BSCF_LASTDATANOTIFICATION | BSCF_DATAFULLYAVAILABLE,0,size);
      #endif
    }
    else
    {
      if (filename.GetLength()<=0)
      {
        return INET_E_CANNOT_CONNECT;
      }
      _handle = open(filename,O_RDONLY|O_BINARY);

      if (_handle<0)
      {
        return INET_E_RESOURCE_NOT_FOUND;
      }
      size_t size = filelength(_handle);

      Log("vssProtocol: open handle %d, size = %d", _handle, size);
      //Mime type recognition trial
      /*
        //trial to feed FindMimeFromData with parameter LPVOID pBuffer
        //this doesn't worked (html files processed as ContentType text/plain)
        const DWORD bufSize = 1024;
        char buff[bufSize];
        int tmpHandle = open(filename,O_RDONLY|O_BINARY);
        read(tmpHandle, buff, bufSize);
        close(tmpHandle);
        //trial to persuade FindMimeFromData, that vcs://.../file.html has content type text/html
        RString urlWithHttp = RString("http://")+realPath;
        wchar_t httpedUrl[1024]; 
        mbstowcs(httpedUrl, urlWithHttp.Data(), 1024);
      */
      /* 
        HRESULT FindMimeFromData
        (          
        LPBC pBC, LPCWSTR pwzUrl, LPVOID pBuffer, DWORD cbSize, LPCWSTR pwzMimeProposed,
        DWORD dwMimeFlags, LPWSTR *ppwzMimeOut, DWORD dwReserved
        )
      */
      const char *fileExt = GetFileExt(GetFilenameExt(path));
      GetIniFileParams(unconst_cast(fileExt));
      RString suggestedMimeType;
      if (_ParMimetype.GetLength()>0)
      {
        suggestedMimeType = RString(_ParMimetype);
      }
      else
      {
        LPWSTR wfoundMime;
        const DWORD bufSize = 1024;
        char buff[bufSize];
        int tmpHandle = open(filename,O_RDONLY|O_BINARY);
        read(tmpHandle, buff, bufSize);
        close(tmpHandle);
        HRESULT hr = FindMimeFromData
        (
          NULL, url/*httpedUrl*/, buff, bufSize/*bufSize*/, NULL, 0, &wfoundMime, 0
        );
        if (FAILED(hr)) {
          suggestedMimeType = "application/octet-stream";
        } else {
          int mimeLen = wcstombs(NULL, wfoundMime, INT_MAX);
          char *foundMime = new char[mimeLen+1];
          wcstombs(foundMime, wfoundMime, mimeLen+1);
          suggestedMimeType=RString(foundMime);
          delete foundMime;
        }
      }
      Log("vssProtocol: ContentType: %s", suggestedMimeType.Data());
      //add line breaks for content type text/plain
      if (!strcmpi(suggestedMimeType, "text/plain")) 
      {
        TextFileToHTML(filename, realPath, suggestedMimeType);
      }
      //notify protocol sink the content mime type
      //BINDSTATUS_MIMETYPEAVAILABLE ... MSIE will take suggestedMimeType only as recommendation
      //BINDSTATUS_VERIFIEDMIMETYPEAVAILABLE ... MSIE will take it as final MIME type
      #ifdef CALL_REPORT
        _mimeType = suggestedMimeType;
        _size = size;
        Report(grfPI);
      #else
        ExamineAndProcessByExtension(filename, path, suggestedMimeType);
        //sink->ReportProgress(BINDSTATUS_CONTENTDISPOSITIONATTACH, L"attachment;filename=\"anything.html\""); //doesn't help
        int mimeLen = mbstowcs(NULL, suggestedMimeType, INT_MAX);
        LPWSTR wsuggestedMimeType = new wchar_t[mimeLen+1];
        mbstowcs(wsuggestedMimeType, suggestedMimeType, mimeLen+1);
        sink->ReportProgress(BINDSTATUS_VERIFIEDMIMETYPEAVAILABLE, wsuggestedMimeType);
        delete wsuggestedMimeType;
        sink->ReportData(BSCF_LASTDATANOTIFICATION | BSCF_DATAFULLYAVAILABLE,0,size);
      #endif
    }
  }
  else
  {
    // more difficult handling - xml parsing required

    HRESULT hr = S_OK;
    CComPtr<IXMLDOMDocument2> xmldoc,xsldoc;

    bool isFile = true;
    RString filename = GetVSSFile(realPath+".xml",isFile);
    if (filename.GetLength()<=0)
    {
      return INET_E_CANNOT_CONNECT;
    }
    if (!isFile)
    {
      return INET_E_RESOURCE_NOT_FOUND;
    }

    xmldoc = LoadXML(filename,hr);
    if (hr!=S_OK) return hr;


    // get stylesheet URL
    RString stylesheetURL = GetStylesheet(args[0]);
    // check if our protocol is used top refence stylesheet
    RString stylesheetPath;
    if (!strnicmp(stylesheetURL,vssPrefix,strlen(vssPrefix)))
    {
      stylesheetPath = stylesheetURL+strlen(vssPrefix);
    }

    RString stylesheet = GetVSSFile(stylesheetPath,isFile);
    //RString stylesheet = RString("w:\\c\\doc\\")+RString(shortname)+".xsl";
    if (stylesheet.GetLength()<=0)
    {
      return INET_E_CANNOT_CONNECT;
    }
    if (!isFile)
    {
      return INET_E_RESOURCE_NOT_FOUND;
    }
    xsldoc = LoadXML(stylesheet,hr);
    if (hr!=S_OK) return hr;

    CComBSTR xmlResult;
    hr = xmldoc->transformNode(xsldoc,&xmlResult);
    // return content now
    size_t reslen = wcstombs(NULL,xmlResult,INT_MAX);
    _strBuf.Resize(reslen);
    wcstombs(_strBuf.Data(),xmlResult,reslen);
    _str.init(_strBuf.Data(),_strBuf.Size());
    //_total = reslen;
    int size = reslen;
    //int size = reslen|0xf; //this operation magically worked due to incorrect ussage of first parameter in sink->ReportData(size,0,size); 
    #ifdef CALL_REPORT
        _mimeType = L"text/html";
        _size = size;
        Report(grfPI);
    #else
        //notify protocol sink the content mime type
        LPCWSTR wcMimeType = L"text/html";
        sink->ReportProgress(BINDSTATUS_VERIFIEDMIMETYPEAVAILABLE, wcMimeType);      
        sink->ReportData(BSCF_LASTDATANOTIFICATION | BSCF_DATAFULLYAVAILABLE,0,size);
    #endif
  }

  return S_OK;
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocol::Continue( 
/* [in] */ PROTOCOLDATA __RPC_FAR *pProtocolData)
{
  Log("vssProtocol: Continue");
  //Fail("VSSInternetProtocol::Continue");
  if ( pProtocolData->dwState == 1 ) return Report(PI_FORCE_ASYNC);
  return E_FAIL;
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocol::Abort( 
/* [in] */ HRESULT hrReason,
/* [in] */ DWORD dwOptions)
{
  Log("vssProtocol: Abort");
  Close();
  return S_OK;
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocol::Terminate( 
/* [in] */ DWORD dwOptions)
{
  Log("vssProtocol: Terminate");
  Close();
  return S_OK;
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocol::Suspend()
{
  Log("vssProtocol: Suspend");
  Fail("VSSInternetProtocol::Suspend");
  return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocol::Resume()
{
  Log("vssProtocol: Resume");
  Fail("VSSInternetProtocol::Resume");
  return E_NOTIMPL;
}

// IInternetProtocol 
HRESULT STDMETHODCALLTYPE VSSInternetProtocol::Read( 
/* [length_is][size_is][out][in] */ void __RPC_FAR *pv,
/* [in] */ ULONG cb,
/* [out] */ ULONG __RPC_FAR *pcbRead)
{
  Log("vssProtocol: Read");

  // if some stream is preloaded, return its content
  if (_str.rest()>0)
  {
    int rd = _str.rest();
    if ((int)cb<rd) rd = cb;
    _str.read(pv,rd);
    _readFrom += rd;
    if (pcbRead) *pcbRead = rd;
    if (rd!=(int)cb)
    {
      if (_sink) _sink->ReportResult(S_OK,S_OK,L"");
      return S_FALSE;
    }
    return S_OK; //it's still not over
  }
  // read from stream
  if (_handle<0) return S_FALSE;

  size_t rd = read(_handle,pv,cb);
  _readFrom += rd;
  Log("vssProtocol: readed so far: %d B, readed last: %d B", _readFrom, rd);
  if (pcbRead) *pcbRead = rd;
  if (rd!=cb)
  {
    if (_sink) _sink->ReportResult(S_OK,S_OK,L"");
    //if (_sink) _sink->ReportResult(S_OK,0,NULL);
    return S_FALSE;
  }
  return S_OK; //it's still not over
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocol::Seek( 
/* [in] */ LARGE_INTEGER dlibMove,
/* [in] */ DWORD dwOrigin,
/* [out] */ ULARGE_INTEGER __RPC_FAR *plibNewPosition)
{
  Log("vssProtocol: Seek");
  Fail("VSSInternetProtocol::Seek");
  return E_NOTIMPL;
}


HRESULT STDMETHODCALLTYPE VSSInternetProtocol::LockRequest( 
/* [in] */ DWORD dwOptions)
{
  Log("vssProtocol: Lock Request");
  return S_OK;
  //return INET_E_CANNOT_LOCK_REQUEST;
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocol::UnlockRequest()
{
  Log("vssProtocol: Unlockock Request");
  return S_OK;
  //return E_NOTIMPL;
}

class VSSInternetProtocolInfo: public RefCount,public IInternetProtocolInfo
{
  public:

  // IUnknown
  HRESULT STDMETHODCALLTYPE QueryInterface(REFIID id,void **object);
  ULONG STDMETHODCALLTYPE AddRef() {return RefCount::AddRef();}
  ULONG STDMETHODCALLTYPE Release()
  {
    int ret = RefCount::Release();
    return ret;
  }
  // IInternetProtocolInfo
  virtual HRESULT STDMETHODCALLTYPE VSSInternetProtocolInfo::ParseUrl( 
  /* [in] */ LPCWSTR pwzUrl,
  /* [in] */ PARSEACTION ParseAction,
  /* [in] */ DWORD dwParseFlags,
  /* [out] */ LPWSTR pwzResult,
  /* [in] */ DWORD cchResult,
  /* [out] */ DWORD __RPC_FAR *pcchResult,
  /* [in] */ DWORD dwReserved);

  virtual HRESULT STDMETHODCALLTYPE CombineUrl( 
  /* [in] */ LPCWSTR pwzBaseUrl,
  /* [in] */ LPCWSTR pwzRelativeUrl,
  /* [in] */ DWORD dwCombineFlags,
  /* [out] */ LPWSTR pwzResult,
  /* [in] */ DWORD cchResult,
  /* [out] */ DWORD __RPC_FAR *pcchResult,
  /* [in] */ DWORD dwReserved);

  virtual HRESULT STDMETHODCALLTYPE CompareUrl( 
  /* [in] */ LPCWSTR pwzUrl1,
  /* [in] */ LPCWSTR pwzUrl2,
  /* [in] */ DWORD dwCompareFlags);

  virtual HRESULT STDMETHODCALLTYPE QueryInfo( 
  /* [in] */ LPCWSTR pwzUrl,
  /* [in] */ QUERYOPTION OueryOption,
  /* [in] */ DWORD dwQueryFlags,
  /* [size_is][out][in] */ LPVOID pBuffer,
  /* [in] */ DWORD cbBuffer,
  /* [out][in] */ DWORD __RPC_FAR *pcbBuf,
  /* [in] */ DWORD dwReserved);
};

HRESULT STDMETHODCALLTYPE VSSInternetProtocolInfo::ParseUrl( 
/* [in] */ LPCWSTR pwzUrl,
/* [in] */ PARSEACTION ParseAction,
/* [in] */ DWORD dwParseFlags,
/* [out] */ LPWSTR pwzResult,
/* [in] */ DWORD cchResult,
/* [out] */ DWORD __RPC_FAR *pcchResult,
/* [in] */ DWORD dwReserved)
{
  Log("vssProtocol: ParseUrl");
  Log("vssProtocol: Parse action: %d", ParseAction);
  char logUrl[256];  wcstombs(logUrl, pwzUrl, 256);
  Log("vssProtocol: Parse url=%s", logUrl);

  *pcchResult = cchResult;
  wcscpy(pwzResult,pwzUrl);
  switch (ParseAction)
  {
  case PARSE_CANONICALIZE: 
    {
      InternetCanonicalizeUrlW(pwzUrl,pwzResult,pcchResult,dwParseFlags);
    }
    break;
  case PARSE_FRIENDLY:
  case PARSE_SECURITY_DOMAIN:
  case PARSE_SECURITY_URL:
    //*pcchResult = 0;
    //*pwzResult = 0;
    //return E_NOTIMPL;
    //return INET_E_DEFAULT_ACTION;
    return S_OK;
  default:
    *pcchResult = 0;
    *pwzResult = 0;
    //return INET_E_DEFAULT_ACTION;
    return E_NOTIMPL;
    
   
/*    
PARSE_CANONICALIZE    : stTemp:= PROTOCOL_SCHEME+':'+FURIHandler.FFullURI;
PARSE_FRIENDLY        : stTemp:= 'Hier klicken...';
PARSE_SECURITY_URL    : stTemp:= PROTOCOL_SCHEME+':'+FURIHandler.FFullURI;
PARSE_ENCODE          : stTemp:= PROTOCOL_SCHEME+':'+FURIHandler.FFullURI;

PARSE_ROOTDOCUMENT    : stTemp:= FURIHandler.FOptValue;
PARSE_DOCUMENT        : stTemp:= FURIHandler.FOptValue;
PARSE_MIME            : stTemp:= FURIHandler.FMimeType;

PARSE_ANCHOR          : stTemp:= '';
PARSE_DECODE          : stTemp:= '';
PARSE_PATH_FROM_URL   : stTemp:= '';
PARSE_URL_FROM_PATH   : stTemp:= '';
PARSE_SERVER          : stTemp:= '';
PARSE_SCHEMA          : stTemp:= '';
PARSE_SITE            : stTemp:= '';
PARSE_DOMAIN          : stTemp:= '';
PARSE_LOCATION        : stTemp:= '';
PARSE_SECURITY_DOMAIN : stTemp:= '';
PARSE_ESCAPE          : stTemp:= '';
PARSE_UNESCAPE        : stTemp:= '';
*/
  }
  return S_OK;
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocolInfo::CombineUrl( 
/* [in] */ LPCWSTR pwzBaseUrl,
/* [in] */ LPCWSTR pwzRelativeUrl,
/* [in] */ DWORD dwCombineFlags,
/* [out] */ LPWSTR pwzResult,
/* [in] */ DWORD cchResult,
/* [out] */ DWORD __RPC_FAR *pcchResult,
/* [in] */ DWORD dwReserved)
{
  Log("vssProtocol: CombineUrl");

  *pcchResult = cchResult;
  BOOL ok =InternetCombineUrlW
  (
    pwzBaseUrl,pwzRelativeUrl,pwzResult,pcchResult,dwCombineFlags
  );
  return ok ? S_OK : S_FALSE;
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocolInfo::CompareUrl( 
/* [in] */ LPCWSTR pwzUrl1,
/* [in] */ LPCWSTR pwzUrl2,
/* [in] */ DWORD dwCompareFlags)
{
  Log("vssProtocol: CompareUrl");
  Fail("VSSInternetProtocolInfo::CompareUrl");
  return E_NOTIMPL;
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocolInfo::QueryInfo( 
/* [in] */ LPCWSTR pwzUrl,
/* [in] */ QUERYOPTION OueryOption,
/* [in] */ DWORD dwQueryFlags,
/* [size_is][out][in] */ LPVOID pBuffer,
/* [in] */ DWORD cbBuffer,
/* [out][in] */ DWORD __RPC_FAR *pcbBuf,
/* [in] */ DWORD dwReserved)
{
  //return E_NOTIMPL;
  Log("vssProtocol: QueryInfo %d,%d",OueryOption,dwQueryFlags);
  char logUrl[256];  wcstombs(logUrl, pwzUrl, 256);
  Log("vssProtocol: QueryInfo url=%s", logUrl);

  *pcbBuf = cbBuffer;
  memset(pBuffer,0,cbBuffer);
  switch (OueryOption)
  {

    case QUERY_CAN_NAVIGATE:
    //Check if the protocol can navigate
      *(BOOL *)pBuffer = TRUE;
      break;
    case QUERY_USES_NETWORK:
      *(BOOL *)pBuffer = TRUE;
      break;
    case QUERY_IS_CACHED_OR_MAPPED:
    //Check if this resource is stored in the cache or if it is on a mapped drive (in a cache container). 
    case QUERY_IS_CACHED:
      *(BOOL *)pBuffer = FALSE;
      break;
    case QUERY_USES_CACHE:
    //Check if the specified protocol uses the Internet cache. 
      *(BOOL *)pBuffer = FALSE;
      break;
      
    /*

    case QUERY_IS_INSTALLEDENTRY:
    //Check if this resource is installed locally on a CD-ROM. 


    case QUERY_USES_CACHE:
    //Check if the specified protocol uses the Internet cache. 

    case 0xd:
      *(BOOL *)pBuffer = TRUE;
      break;
    //case QUERY_IS_SECURE:
    //Check if the protocol is encrypted. 

    //case QUERY_IS_SAFE:
    //Check if the protocol only serves trusted content. 
      break;
      */
    //case QUERY_IS_SECURE???:
    case 0xd:
      *(BOOL *)pBuffer = FALSE;
      break;
    default:
      return INET_E_DEFAULT_ACTION;
  }
  return S_OK;
}

HRESULT STDMETHODCALLTYPE VSSInternetProtocolInfo::QueryInterface(REFIID id,void **object)
{
  COM_QUERY(*object,IInternetProtocolInfo);
  COM_QUERY(*object,IUnknown);
  return E_NOINTERFACE;
}

//////////////////////////////////////////////////////////////////////////

class VSSClassFactory: public RefCount, public IClassFactory
{
  //CComPtr<IXMLDOMDocument2> _domdoc;
  
  public:
  VSSClassFactory();
  ~VSSClassFactory();

  // IUnknown
  HRESULT STDMETHODCALLTYPE QueryInterface(REFIID id,void **object);
  ULONG STDMETHODCALLTYPE AddRef() {return RefCount::AddRef();}
  ULONG STDMETHODCALLTYPE Release() {return RefCount::Release();}

  // IClassFactory

  HRESULT STDMETHODCALLTYPE CreateInstance
  (
    /* [unique][in] */ IUnknown __RPC_FAR *pUnkOuter,
    /* [in] */ REFIID riid,
    /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject
  );
  HRESULT STDMETHODCALLTYPE LockServer(/* [in] */ BOOL fLock);
};


VSSClassFactory::VSSClassFactory()
{
//hr = CoInitialize(NULL); // Check the return value, hr...
  //_domdoc.CoCreateInstance(CLSID_DOMDocument,NULL,CLSCTX_INPROC_SERVER); // Check the return value, hr... _domdoc
}
VSSClassFactory::~VSSClassFactory()
{
  //CoInitialize(NULL); // Check the return value, hr...
}

HRESULT STDMETHODCALLTYPE VSSClassFactory::QueryInterface(REFIID id,void **object)
{
  COM_QUERY(*object,IClassFactory);
  COM_QUERY(*object,IUnknown);
  return E_NOINTERFACE;
}

HRESULT STDMETHODCALLTYPE VSSClassFactory::CreateInstance
(
  /* [unique][in] */ IUnknown __RPC_FAR *pUnkOuter,
  /* [in] */ REFIID riid,
  /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject
)
{
  if
  (
    IsEqualGUID(riid,IID_IUnknown)
  )
  {
    VSSInternetProtocol *cls = new VSSInternetProtocol;
    cls->AddRef();
    *ppvObject = (IUnknown *)cls;
    return S_OK;
  }
  if
  (
    IsEqualGUID(riid,IID_IInternetProtocol)
  )
  {
    VSSInternetProtocol *cls = new VSSInternetProtocol;
    cls->AddRef();
    *ppvObject = (IInternetProtocol *)cls;
    return S_OK;
  }

  /**/
  if
  (
    IsEqualGUID(riid,IID_IInternetProtocolInfo)
  )
  {
    VSSInternetProtocolInfo *cls = new VSSInternetProtocolInfo;
    cls->AddRef();
    *ppvObject = (IInternetProtocolInfo *)cls;
    return S_OK;
  }
  /**/

  return E_NOINTERFACE;
}

HRESULT STDMETHODCALLTYPE VSSClassFactory::LockServer(/* [in] */ BOOL fLock)
{
  return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
// Special entry points required for inproc servers


STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
  if (!IsEqualGUID(rclsid,VssProtocolGUID))
  {
    return CLASS_E_CLASSNOTAVAILABLE;
  }
  if (!IsEqualGUID(riid,IID_IClassFactory))
  {
    return CLASS_E_CLASSNOTAVAILABLE;
  }
  VSSClassFactory *cls = new VSSClassFactory;
  cls->AddRef();
  *ppv = (IClassFactory *)cls;
  return S_OK;
}

STDAPI DllCanUnloadNow(void)
{
  //return AfxDllCanUnloadNow();
  return S_OK;
}

// by exporting DllRegisterServer, you can use regsvr.exe
STDAPI DllRegisterServer(void)
{
  //COleObjectFactory::UpdateRegistryAll();
  return SELFREG_E_CLASS;
  //return S_OK;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
  return TRUE;
}
