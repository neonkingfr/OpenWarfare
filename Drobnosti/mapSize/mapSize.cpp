#include <Es/essencePch.hpp>
#include <Es/Framework/consoleBase.h>
#include <Es/Strings/rString.hpp>
#include <El/Debugging/mapFile.hpp>
#include <El/Statistics/statistics.hpp>
#include <stdio.h>

class MapFileAnalyser: public MapFile
{
  public:
  void ReportSizes(FILE *out);
};

class FilePrint
{
  FILE *_file;
  int _limit;
public:
  FilePrint(FILE *file, int limit)
  {
    _file = file;
    _limit = limit;
  }
  void operator() (const char *name, int value) const
  {
    if (value<_limit) return;
    fprintf(_file,"%32s: %8d\n",name,value);
  }
};

void MapFileAnalyser::ReportSizes(FILE *out)
{
  StatisticsByName moduleSize;
  //StatisticsByName libSize;
  int totalSize = 0;
  for (int i=1; i<GetSymbolCount()-1; i++)
  {
    const MapInfo &sym = GetSymbol(i);
    const MapInfo &next = GetSymbol(i+1);
    int size = next.physAddress-sym.physAddress;
    if (size>12*1024)
    {
      printf("%54s: %8x size %8d\n",sym.ReadableName(),sym.physAddress,size);
    }
    const char *libEnd = strchr(sym.module,':');
    if (libEnd)
    {
      RString libName(sym.module,libEnd-sym.module);
      libName = libName + Format(" %x",sym.section);
      moduleSize.Count(libName,size);
      //libSize.Count(libName,size);
    }
    else
    {
      RString libName(sym.module);
      libName = libName + Format(" %x",sym.section);
      moduleSize.Count(libName,size);
    }
    totalSize += size;
  }
  printf("Total size %8d\n",totalSize);

  FilePrint print(out,4*1024);
  moduleSize.Report(print);
}

int consoleMain( int argc, const char *argv[] )
{
  if (argc<1)
  {
    return -1;
  }
  argc--,argv++;
  if (argc!=1)
  {
    return -1;
  }
  const char *mapName = *argv;
  MapFileAnalyser map;
  map.ParseMapFile(mapName);
  if (map.Empty())
  {
    fprintf(stderr,"Cannot parse mapfile %s\n",mapName);
    return 1;
  }
  // calculate size of each map file symbol
  map.ReportSizes(stdout);
  #if _DEBUG
    return 1;
  #else
    return 0;
  #endif
}