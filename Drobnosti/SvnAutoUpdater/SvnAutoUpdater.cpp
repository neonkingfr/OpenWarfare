#include "StdAfx.h"
#include <shellapi.h>
#include <string>
#include <io.h>
#include "svnAutoUpdater.h"
#include "SystemTraySDK.h"
#include "pipeManager.h"
#include "stdStringFormat.h"
#include "resource.h"
#include "Es/Files/commandLine.hpp"

HINSTANCE TheInstance = 0;

#include <Psapi.h>
#include <tlhelp32.h>
#pragma comment(lib,"Psapi.lib")
BOOL GetParentPID( PROCESSENTRY32& procentry )
{
  HANDLE         hSnapShot ;
  BOOL           cont ;

  // Get a handle to a Toolhelp snapshot of the systems
  // processes.
  hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0 );
  if( hSnapShot == INVALID_HANDLE_VALUE )
  {
    return FALSE ;
  }

  // Get the first process' information.
  memset((LPVOID)&procentry,0,sizeof(PROCESSENTRY32));
  procentry.dwSize = sizeof(PROCESSENTRY32) ;
  cont = Process32First( hSnapShot, &procentry ) ;
  DWORD pid = 0;
  // While there are processes, keep looping.
  DWORD  crtpid= GetCurrentProcessId();
  while( cont )
  {
    if(crtpid == procentry.th32ProcessID)
      pid =  procentry.th32ParentProcessID;

    procentry.dwSize = sizeof(PROCESSENTRY32) ;
    cont = !pid && Process32Next( hSnapShot, &procentry );

  }//while ends

  return pid?TRUE:FALSE ;
}

HANDLE GetParentHandle()
{
  PROCESSENTRY32 procentry;
  if ( GetParentPID(procentry) && procentry.th32ParentProcessID )
  {
    return OpenProcess(SYNCHRONIZE | PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, procentry.th32ParentProcessID ) ;
  }
  return INVALID_HANDLE_VALUE;
}

#ifndef _CONSOLE
template <class T>
inline T GetWinLong (HWND hwnd, int which = GWL_USERDATA)
{
    return reinterpret_cast<T> (::GetWindowLong (hwnd, which));
}

template <class T>
inline void SetWinLong (HWND hwnd, T value, int which = GWL_USERDATA)
{
    ::SetWindowLong (hwnd, which, reinterpret_cast<long> (value));
}

struct LoginDialogData
{
  char *username, *password;
  LoginDialogData(char *user, char *pass) : username(user), password(pass) {}
};

void GetString(HWND hWnd, char* buf, int len)
{
  SendMessage (hWnd, WM_GETTEXT, (WPARAM) len, (LPARAM) buf);
}

INT_PTR CALLBACK LoginDialogProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	LoginDialogData* ctrl = GetWinLong<LoginDialogData *> (hwnd);
  switch (message)
  {
  case WM_INITDIALOG:
      {
          ctrl = reinterpret_cast<LoginDialogData *> (lParam);
 		      SetWinLong<LoginDialogData *>(hwnd, ctrl);
      }
      return TRUE;

  case WM_COMMAND:
    {
      int ctrlID = LOWORD(wParam);
      int notifyCode = HIWORD(wParam);
      switch (ctrlID)
      {
      case IDC_LOGIN_USERNAME:
          if (notifyCode==EN_CHANGE)
          {
            GetString (GetDlgItem(hwnd, IDC_LOGIN_USERNAME), ctrl->username, 256);
          }
          return true;
      case IDC_LOGIN_PASSWORD:
          if (notifyCode==EN_CHANGE)
          {
            GetString (GetDlgItem(hwnd, IDC_LOGIN_PASSWORD), ctrl->password, 256);
          }
          return true;
      case IDOK:
          EndDialog(hwnd, TRUE);
          return true;
      case IDCANCEL:
          EndDialog(hwnd, FALSE);
          return true;
      }
    }

  case WM_DESTROY:
  	SetWinLong<LoginDialogData *> (hwnd, 0);
	  break;
  }
  return FALSE;
}
#endif

//@{ Global option values taken from commandLine
struct Options
{
  int gfdIn;
  int gfdOut;
  string userName;
  string password;
  Options() : gfdIn(-1), gfdOut(-1) {}
  ~Options() {}
};
Options GOptions;
//@}

char auth_username[512];
char auth_password[512];
Controller::Controller (HWND hwnd)
    :	_editLog		 (hwnd,	IDC_LOG),
      _clear		 (hwnd,	IDC_CLEAR, FALSE),
      _status		 (hwnd,	IDC_STATUS),
      _minSysTrayCheck	 (hwnd,	IDC_MINIMIZE_SYSTRAY),
      _svnAcc(GOptions.userName.c_str(), GOptions.password.c_str()),
      _svnAccOK(true), _svnErrorShown(false),
      _svnAccRestarted(false),
      _pipeManager(GOptions.gfdIn, GOptions.gfdOut),
      _restartAfterShutDown(false),
      _shutDown(false),
      _firstSvnCheckDone(false),
      _hSvnThread(INVALID_HANDLE_VALUE),
      _hwnd(hwnd),
      _minimized(false)
{
  // Attach icon to main dialog
  HICON hIcon = LoadIcon (TheInstance, MAKEINTRESOURCE (DLG_ICON));
  SendMessage (hwnd, WM_SETICON, WPARAM (TRUE), LPARAM (hIcon));
  hIcon = LoadIcon (TheInstance, MAKEINTRESOURCE (DLG_ICON_S));
  SendMessage (hwnd, WM_SETICON, WPARAM (FALSE), LPARAM (hIcon));

  // Create the tray icon
  TrayIcon.Create(TheInstance,
                  hwnd,                // Parent window
                  WM_ICON_NOTIFY,      // Icon notify message to use
                  _T("SvnAutoUpdater"),  // tooltip
                  hIcon,
                  IDR_SYSTRAY_POPUP_MENU);

  // Set timer to check for new update periodically
  ::SetTimer(hwnd, ID_CHECK_UPDATE_TIMER, 500, NULL);

  // Other initializations...
  _editLog.SetFocus();
  _minSysTrayCheck.Check();

  // need handle of process to finish after it ends
  _parentProc = GetParentHandle();
}

Controller::~Controller()
{
  if (_parentProc != INVALID_HANDLE_VALUE)
    CloseHandle(_parentProc);
}

void Controller::Command (HWND hwnd, int controlID, int command)
{
    switch (controlID)
    {
        case IDC_LOG:
            if (_editLog.IsChanged(command))
            {
                if (_editLog.GetLength())
                    _clear.Enable();
                else
                    _clear.Disable();
            }
            break;
        case IDC_CLEAR:
            _editLog.SetString("");
            _clear.Disable();
            _editLog.SetFocus();
            break;
        case IDC_MINIMIZE_SYSTRAY:
            break;
        case ID_POPUP_RESTORE:
          TrayIcon.MaximiseFromTray(hwnd);
          ::ShowWindow(_hwnd, SW_SHOW);
          _minimized = false;
           break;
        case ID_POPUP_QUIT:
				   DestroyWindow(hwnd);
           break;
	/*
        case IDM_ABOUT:
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;
  */
    }
}

bool Controller::Minimize(HWND hwnd)
{
  if (_minSysTrayCheck.IsChecked())
  {
    TrayIcon.MinimiseToTray(hwnd);
    TrayIcon.ShowBalloon("The SvnAutoUpdater\nwas minimized to SysTray");
    _minimized = true;
    return true;
  }
  return false;
}

void Controller::ShowError()
{
  if (!_pipeManager.Ready()) 
  {
    char *msg = "Cannot establish connection to the application!";
    _editLog.AddLine(msg);
    _status.SetString(msg);
  }
}

DWORD WINAPI ThreadSvnUpdateProc(LPVOID lpParameter)
{
  Controller *control = reinterpret_cast<Controller *>(lpParameter);
  if (control)
  {
    int rev;
    control->_svnAccOK = control->_svnAcc.Update(control->_updateDir.c_str(), rev);
  }
  return 0;
}

DWORD WINAPI ThreadSvnCheckoutProc(LPVOID lpParameter)
{
  Controller *control = reinterpret_cast<Controller *>(lpParameter);
  if (control)
  {
    control->_svnAccOK = control->_svnAcc.Checkout(control->_svnUrl.c_str(), control->_updateDir.c_str());
  }
  return 0;
}

void Controller::CheckForUpdate()
{
  const string CmdUpToDate("up-to-date");
  const string CmdCheckout("checkouting");
  const string CmdOutdated("updating");

  _svnAccOK = (_svnAcc.GetLastError()==NULL);

  if ( _hSvnThread != INVALID_HANDLE_VALUE )
  {
    if (WaitForSingleObject(_hSvnThread, 0)==WAIT_OBJECT_0 )
    {
      if (_svnAccOK)
      {
        _shutDown = true;
        _hSvnThread = INVALID_HANDLE_VALUE;
      }
      else
      {
        _editLog.AddLine("Connection to SVN has failed. Unable to check for updates!");
        _hSvnThread = INVALID_HANDLE_VALUE;
      }
    }
  }
  else if ( !_svnUrl.empty() && !_updateDir.empty() && !_shutDown && !_restartAfterShutDown && _svnAccOK)
  {
    if ( !_svnAcc.IsVersioned(_updateDir.c_str()) )
    { // checkout it
      if (_svnAccRestarted || GOptions.userName.length()>0)
      { // IsVersioned does not test connection to svn, restart it with auth info
        _pipeManager.Send(CmdCheckout);
        _editLog.AddLine("Application in not under svn yet, checkout in progress...");
        _status.SetString("Checkout in progress, downloading...");
        _hSvnThread = CreateThread(NULL, 0, ThreadSvnCheckoutProc, this, 0, NULL);
      }
      else _svnAccOK = false; 
    }
    else 
    {
      bool updateAvailable = false;
      if ( _svnAcc.UpdateAvailable(_updateDir.c_str(), updateAvailable) )
      { 
        if (updateAvailable)
        { // update it
          _pipeManager.Send(CmdOutdated);
          _editLog.AddLine("Application is outdated, svn update in progress...");
          _status.SetString("Update in progress, downloading...");
          _hSvnThread = CreateThread(NULL, 0, ThreadSvnUpdateProc, this, 0, NULL);
        }
        else if (!_firstSvnCheckDone)
        {
          _firstSvnCheckDone = true;
          _pipeManager.Send(CmdUpToDate);
          _editLog.AddLine("Application is up-to-date");
          _status.SetString("Application is up-to-date");
        }
      }
      else
      { // something went wrong
        _svnAccOK = false;
      }
    }
  }
  else if ( !_svnAccOK )
  {
    if (!_svnAccRestarted)
    {
      _status.SetString("Access to SVN repository without Username/Password has failed! ");
      ::KillTimer(_hwnd, ID_CHECK_UPDATE_TIMER);  //stop timer
      LoginDialogData loginData(auth_username, auth_password);
      ::DialogBoxParam(TheInstance, MAKEINTRESOURCE(IDD_LOGIN), _hwnd, LoginDialogProc, (LPARAM) &loginData);
      ::SetTimer(_hwnd, ID_CHECK_UPDATE_TIMER, 500, NULL);
      _svnAccRestarted = true;
      _svnAcc.Init(auth_username, auth_password);
      _svnAccOK = true; //try again
    }
    else
    {
      _status.SetString("Access to SVN repository has failed!");
      if (!_svnErrorShown)
      {
        _pipeManager.Send(PipeManager::CmdFailed);
        _svnErrorShown = true;
        _editLog.AddLine(_svnAcc.GetLastError());
        TrayIcon.MaximiseFromTray(_hwnd);
        _minimized = false;
      }
    }
  }
}

//check the commandline
/// check if an option with arguments matches
static bool IsOptionWArgs(const char *beg, const char *opt)
{
  // when this is used, the last character of opt should be '=', like in -x=800
  //Assert(opt[0]!=0 && opt[strlen(opt)-1]=='=');
  return !strnicmp(beg,opt,strlen(opt));
}

static void SingleArgument( const char *beg, const char *end )
{
  if( end==beg ) return;
  if( *beg=='-' || *beg=='/' )
  {
    beg++;
    // option
    static const char fdInStr[]="in=";
    static const char fdOutStr[]="out=";
    static const char userNameStr[]="login=";
    static const char passwdStr[]="password=";

    if (IsOptionWArgs(beg, fdInStr))
    {
      const char *val = beg + strlen(fdInStr); 
      GOptions.gfdIn = atoi(val);
    }
    else if (IsOptionWArgs(beg, fdOutStr))
    {
      const char *val = beg + strlen(fdOutStr); 
      GOptions.gfdOut = atoi(val);
    }
    else if (IsOptionWArgs(beg, userNameStr))
    {
      const char *val = beg + strlen(userNameStr); 
      GOptions.userName = string(val, end-val);
    }
    else if (IsOptionWArgs(beg, passwdStr))
    {
      const char *val = beg + strlen(passwdStr); 
      GOptions.password= string(val, end-val);
    }
  }
}

BOOL CALLBACK DialogProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    static Controller* control = 0;
    switch (message)
    {
    case WM_INITDIALOG:
        try
        {
            control = new Controller (hwnd);
        }
        catch (WinException e)
        {
            MessageBox (0, e.GetMessage (), "Exception", MB_ICONEXCLAMATION | MB_OK);
        }
        catch (...)
        {
            MessageBox (0, "Unknown", "Exception", MB_ICONEXCLAMATION | MB_OK);
            return -1;
        }
        // Minimize, when everything is OK, but keep the window open otherwise
        if ( control->Ready() )
          control->Minimize(hwnd);
        else 
          control->ShowError();
        return TRUE;
    case WM_COMMAND:
        control->Command(hwnd, LOWORD(wParam), HIWORD (wParam));
        return TRUE;
    case WM_ICON_NOTIFY:
        return control->TrayIcon.OnTrayNotification(wParam, lParam);
    case WM_SYSCOMMAND:
      {
        switch (wParam)
        {
        case SC_MINIMIZE:
          if (!control->Minimize(hwnd))
            return DefWindowProc(hwnd, message, wParam, lParam);
          return TRUE;
        default:
          return DefWindowProc(hwnd, message, wParam, lParam);
        }
      }
      break;
    case WM_TIMER:
      if (wParam==ID_CHECK_UPDATE_TIMER)
      {
        DWORD curTime = ::GetTickCount();
        static DWORD nextUpdateTime = curTime;
        if ( curTime > nextUpdateTime )
        {
          nextUpdateTime = curTime + 5*1000; //5sec
          control->CheckForUpdate();
        }

        // Receive possible new commands from pipe and process them
        if ( control->Ready() )
          control->UpdatePipeManager();

        //check the parent application is still running
        if ( control->CheckShutDown() )
        { // shut down myself too
          PostQuitMessage(0);
        }

/* NOTE: when svnAutoUpdater tell parent application to shutDown, the pipe will not be read on the other side 
         and the _write would block at last (FREEZE)
        // test pipe
        static int count = 1;
        string countStr = Format("svnAutoUpdater timer = %d", count++);
        _write(GOptions.gfdOut, countStr.c_str(), countStr.length());
*/
      }
      break;
    case WM_DESTROY:
        PostQuitMessage(0);
        return TRUE;
    case WM_CLOSE:
        delete control;
        DestroyWindow (hwnd);
        return TRUE;
    }
    return FALSE;
}

int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, char * cmdLine, int cmdShow)
{
    TheInstance = hInst;
    HWND hDialog = 0;

    CommandLine cLine(cmdLine);
    ScanCommandLine(cLine,SingleArgument);

    //Sleep(10000);

    hDialog = CreateDialog (hInst, MAKEINTRESOURCE (DLG_MAIN), 0, (DLGPROC)DialogProc);
    if (!hDialog)
    {
        char buf [100];
        wsprintf (buf, "Error x%x", GetLastError ());
        MessageBox (0, buf, "CreateDialog", MB_ICONEXCLAMATION | MB_OK);
        return 1;
    }
/*
#ifdef _DEBUG
    string params = Format("in=%d, out=%d", GOptions.gfdIn, GOptions.gfdOut);
    ::MessageBox(hDialog, params.c_str(), "Parameters info", MB_OK);
#endif
*/
    MSG  msg;
    int status;
    while ((status = ::GetMessage (&msg, 0, 0, 0)) != 0)
    {
        if (status == -1)
            return -1;
        if (!IsDialogMessage (hDialog, &msg))
        {
            TranslateMessage ( &msg );
            DispatchMessage ( &msg );
        }
    }

    return msg.wParam;
}

#define CHECK_PARAM(CmdName, CmdVar) \
  { \
    int len = CmdName.length(); \
    if ( !strnicmp(command.c_str(), CmdName.c_str(), len) ) \
    { \
      CmdVar = command.substr(len, command.length()-len); \
      _pipeManager.Send(CmdThx + CmdName); \
      _editLog.AddLine((string("Received: ")+string(command)).c_str()); \
      continue; \
    } \
  } 

// sets TestVar true when command is CmdName 
#define IS_PARAM(CmdName, TestVar) \
  { \
    int len = CmdName.length(); \
    if ( !strnicmp(command.c_str(), CmdName.c_str(), len) ) \
    { \
      _editLog.AddLine((string("Received: ")+string(command)).c_str()); \
      TestVar = true; \
    } \
  } 

void Controller::UpdatePipeManager()
{
  const string CmdThx("thanks for ");
  const string CmdCommandline("commandline: ");
  const string CmdUpdateDir("updateDir: ");
  const string CmdSvnUrl("url: ");
  const string CmdShutDown("shutDown");
  const string CmdCurDir("workingDir: ");
  const string CmdCopyDir("copyDir: ");

  // receive new data
  _pipeManager.Update();
  // process all commands
  for (string command=_pipeManager.Receive(); !command.empty(); command=_pipeManager.Receive())
  {
    CHECK_PARAM(CmdCommandline, _commandLine)
    CHECK_PARAM(CmdUpdateDir, _updateDir)
    CHECK_PARAM(CmdCurDir, _workingDir)
    CHECK_PARAM(CmdCopyDir, _copyDir)
    CHECK_PARAM(CmdSvnUrl, _svnUrl)
    bool shutDown = false;
    IS_PARAM(CmdShutDown, shutDown)
    if (shutDown)
    {
      PostQuitMessage(0);
    }
  }
  // test special conditions
  if (_shutDown)
  {
    _pipeManager.Send(CmdShutDown);
    _editLog.AddLine("Asking application to shut down");
    _restartAfterShutDown = true;
    _shutDown = false;
  }
}

bool CopyDirectoryStructure(const char *dst, const char *src, vector<string> &skipDirList)
{
#ifdef _WIN32
  string buffer = Format("%s\\*.*", src);

  _finddata_t info;
  long h = _findfirst(buffer.c_str(), &info);
  if (h != -1)
  {
    do
    {
      if ((info.attrib & _A_SUBDIR) != 0)
      {
        // FIX - do not delete only current and parent directory
        bool skipIt = false;
        for (auto it = skipDirList.begin(); it!=skipDirList.end(); it++)
        {
          if ( !strcmp(info.name, (*it).c_str()) )
          {
            skipIt=true;
            break;
          }
        }
        if (skipIt) continue;
        if (strcmp(info.name, ".") != 0 && strcmp(info.name, "..") != 0)
        {
          string srcNew = Format("%s\\%s", src, info.name);
          string dstNew = Format("%s\\%s", dst, info.name);
          if (!::CreateDirectory(dstNew.c_str(), NULL) && GetLastError()!=ERROR_ALREADY_EXISTS)
          { //some error?
            return false;
          }
          if (!CopyDirectoryStructure(dstNew.c_str(), srcNew.c_str(), skipDirList))
          { //some error?
            return false;
          }
        }
      }
      else
      {
        string srcNew = Format("%s\\%s", src, info.name);
        string dstNew = Format("%s\\%s", dst, info.name);
        if (!::CopyFile(srcNew.c_str(), dstNew.c_str(), false /*overwrite existing*/))
        {
          return false;
        }
      }
    }
    while (_findnext(h, &info)==0);
    _findclose(h);
  }
  return true;
#else
  not done, yet, this line purpose is to cause compiler error
  LocalPath(srcname,src);
  LocalPath(dstname,dst);                 // dstname must exist
  DIR *dir = OpenDir(srcname);
  if ( !dir ) return;
  struct dirent *entry;
  int srclen = strlen(srcname);
  srcname[srclen++] = '/';
  int dstlen = strlen(dstname);
  dstname[dstlen++] = '/';
  while ( (entry = readdir(dir)) ) {      // process one src-directory item..
    strcpy(srcname+srclen,entry->d_name);
    strcpy(dstname+dstlen,entry->d_name);
    struct stat st;
    if ( !stat(srcname,&st) )           // valid item
      if ( S_ISDIR(st.st_mode) ) {    // sub-directory
        if ( entry->d_name[0] != '.' ||
          entry->d_name[1] &&
          (entry->d_name[1] != '.' || entry->d_name[2]) ) {
            mkdir(dstname,NEW_DIRECTORY_MODE);
            CopyDirectoryStructure(dstname,srcname);
        }
      }
      else                            // file
        fileCopy(srcname,dstname);
  }
  closedir(dir);
#endif
}

bool Controller::CheckShutDown()
{
  if (_parentProc!=INVALID_HANDLE_VALUE)
  {
    DWORD waitRV = WaitForSingleObject(_parentProc, 0);
    if (waitRV==WAIT_OBJECT_0) 
    { //parent application is down
      if (_restartAfterShutDown)
      { // copy new application version taken from svn over the currently shutdowned one
        vector<string> skipDirList;
        skipDirList.push_back(".svn");
        if ( !_copyDir.empty() && !CopyDirectoryStructure(_copyDir.c_str(), _updateDir.c_str(), skipDirList) )
        { // update failed
          char *errMsg = "Could not copy the new version over old one!";
          _status.SetString(errMsg);
          _editLog.AddLine(errMsg);
          ::KillTimer(_hwnd, ID_CHECK_UPDATE_TIMER);  //stop timer
          TrayIcon.MaximiseFromTray(_hwnd);
          return false;
        }
        // and restart
        CloseHandle(_parentProc); _parentProc = INVALID_HANDLE_VALUE;
        STARTUPINFO startupInfo; 
        PROCESS_INFORMATION processInfo; 
        memset(&startupInfo, 0, sizeof(startupInfo)); 
        memset(&processInfo, 0, sizeof(processInfo)); 
        startupInfo.cb = sizeof(startupInfo);
        char *cmdLine = new char[_commandLine.length()+1];
        strcpy(cmdLine, _commandLine.c_str());
        CreateProcess(NULL, cmdLine , NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS, NULL, _workingDir.c_str(), &startupInfo, &processInfo);
      }
      return true;
    }
    else if (waitRV!=WAIT_TIMEOUT)
    {
      _status.SetString("Error while checking the application process!");
      _pipeManager.Send(PipeManager::CmdFailed);
      TrayIcon.MaximiseFromTray(_hwnd);
    }
  }
  return false;
}

bool Controller::Ready() const 
{
  return ( _pipeManager.Ready() && (_parentProc!=INVALID_HANDLE_VALUE) );
}
