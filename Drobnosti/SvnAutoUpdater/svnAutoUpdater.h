#ifndef _MAIN_H_INCLUDED
#define _MAIN_H_INCLUDED

#include <windows.h>
#include "controls.h"
#include "pipeManager.h"
#include "SystemTraySDK.h"
#include "svnAccess.hpp"

class WinException
{
public:
    WinException (char* msg)
    : _err (GetLastError()), _msg(msg)
    {}
    DWORD GetError() const { return _err; }
    char* GetMessage () const { return _msg; }
private:
    DWORD _err;
    char* _msg;
};

class Controller
{
public:
    CSystemTray TrayIcon;
    SvnAccess _svnAcc;
    //! contains return value from SvnAccess functions (false means the SVN is unavailable for some reason)
    bool      _svnAccOK;
    bool      _svnErrorShown;
    bool      _svnAccRestarted;  //true when SvnAcc was restarted with username and password

    Controller(HWND hwnd);
    ~Controller ();
    void Command (HWND hwnd, int controlID, int command);
    bool Minimize(HWND hwnd);
    void CheckForUpdate();
    void UpdatePipeManager();
    bool CheckShutDown();
    //! test whether pipe and parent process are well initialized
    bool Ready() const;
    void ShowError();

public:
    //{@ parameters read from parent application using pipe
    string      _commandLine;  //commandline to use to restart parent application
    string      _workingDir;   //working dir to use to restart parent application
    string      _copyDir;      //directory to copy directory structure to
    string      _updateDir;    //svn working directrory to check
    string      _svnUrl;       //svn to use when checkout is needed (no working directory exists yet)
    //@}

private:
    HWND        _hwnd;         //window handle
    HANDLE      _hSvnThread;   //handle to thread doing svn checkout or update

    Edit        _editLog;
    Button      _clear;
    Edit        _status;
    CheckBox    _minSysTrayCheck;
    PipeManager _pipeManager;
    bool        _restartAfterShutDown;
    HANDLE      _parentProc;
    bool        _shutDown;
    bool        _firstSvnCheckDone;
    //! it tracks the IconTray minimise, maximise...
    bool        _minimized;
};

#endif // _MAIN_H_INCLUDED
