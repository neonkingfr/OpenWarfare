#ifndef _RC_H_INCLUDED
#define _RC_H_INCLUDED

#include <windows.h>

#define DLG_MAIN               200

#define DLG_ICON		           300
#define DLG_ICON_S	           301

#define IDC_STATUS             1000
#define IDC_MINIMIZE_SYSTRAY   1001
#define IDC_LOG                1002
#define IDC_CLEAR              1003
#define IDR_SYSTRAY_POPUP_MENU 1004
#define IDC_STATIC               -1

#define WM_ICON_NOTIFY         (WM_APP+10)
#define ID_CHECK_UPDATE_TIMER  1005
#endif // _RC_H_INCLUDED
