#ifndef _STD_STRING_FORMAT
#define _STD_STRING_FORMAT

#include <string>

std::string Format(const char *fmt, ...); 

#endif
