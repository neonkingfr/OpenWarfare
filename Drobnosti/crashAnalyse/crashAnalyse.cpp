#include <Es/essencepch.hpp>

#include <stdio.h>
#include <io.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>

#include <Es/Common/win.h>
#include <Es/Framework/consoleBase.h>
#include <Es/Files/filenames.hpp>
#include <Es/Framework/debugLog.hpp>
#include <Es/Strings/rString.hpp>
#include <El/CRC/crc.hpp>
#include <El/Debugging/mapFile.hpp>
#include <El/Debugging/xboxContext.h>
#include <El/Debugging/xboxFileFormats.h>

#define CODE_OFFSET 0x400000
// when loading Xbox XBE something is loaded from the address 0x10000
// is seems part of if (0xbc0) is .xbe header
#define CODE_OFFSET_XBOX 0x10b80

// experimental: -0x003ef480


//-0x3ef440

//  0x006fd985-0x002fd545 0x00400440

#define USE_CREATE_PROCESS 0

class ProcessImage
{
  #if USE_CREATE_PROCESS
  PROCESS_INFORMATION _pi;
  #endif
  
  Temp<unsigned char> memCopy;

  public:
  // offset converting logical to physical
  long physicalOffset;
  
  // public access to section boundaries - logical address
  size_t codeStart;
  size_t codeSize;
  size_t dataStart;
  size_t dataSize;

  size_t totalStart;
  size_t totalSize;

  public:
  ProcessImage();
  ~ProcessImage();

  const unsigned char *Memory() const {return memCopy.Data();}
  size_t Size() const {return memCopy.Size();}

  size_t GetCodeStartAddress() const {return codeStart;}
  size_t GetCodeEndAddress() const {return codeStart+codeSize;}

  size_t GetCodeStart() const {return codeStart;}
  size_t GetCodeSize() const {return codeSize;}

  size_t GetDataStartAddress() const {return dataStart;}
  size_t GetDateEndAddress() const {return dataStart+dataSize;}
  long PhysicalOffset() const {return physicalOffset;}

  bool IsBadReadPtr(void *addr, size_t size)
  {
    return
    (
      //(size_t)addr<totalStart+physicalOffset ||
      //(size_t)addr+size>totalStart+physicalOffset+totalSize
      (size_t)addr<(size_t)physicalOffset ||
      (size_t)addr+size>(size_t)physicalOffset+totalSize
    );
  }
  bool Loaded() const {return memCopy.Size()>0;}
  int Load(const char *name, int explicitOffset);
  void Unload();
};


#define _tprintf printf
#define _T(x) x

void IntelPCPrint( MapFile &map, int pc, bool doEol=true)
{
  int nameValue=0;
  const char *name=map.MapNameFromPhysical(pc,&nameValue);


  if( nameValue!=0 )
  {
    int nameOffset=pc-nameValue;
    _tprintf( _T("%8X %8X %8X + %s"),
              pc, nameValue, nameOffset, name );
  }
  else
  {
    _tprintf( _T("%08X"), pc);
  }
  if (doEol)
  {
    _tprintf( _T("\n") );
  }
}

//! abstract context handling base class
class IContextHolder: public RefCount
{
  public:
  virtual DWORD Eip() const = NULL;
  virtual DWORD Esp() const = NULL;
  virtual void PrintRegisters(FILE *f) = NULL;
  //virtual int GetPhysicalOffset() const = NULL;
};

//! Xbox context handling implementation
class ContextHolderI86: public IContextHolder
{
  CONTEXT _ctx;
  
  public:
  ContextHolderI86(const CONTEXT &ctx)
  {
    _ctx = ctx;
  }
  virtual DWORD Eip() const {return _ctx.Eip;}
  virtual DWORD Esp() const {return _ctx.Esp;}
  //virtual int GetPhysicalOffset() const {return 0;}
  virtual void PrintRegisters(FILE *f);
};

void ContextHolderI86::PrintRegisters(FILE *f)
{
  fprintf(f,"EAX:%08X EBX:%08X\nECX:%08X EDX:%08X\nESI:%08X EDI:%08X\n",
        _ctx.Eax, _ctx.Ebx, _ctx.Ecx, _ctx.Edx, _ctx.Esi, _ctx.Edi );

  fprintf(f, "CS:EIP:%04X:%08X\n", _ctx.SegCs, _ctx.Eip );
  fprintf(f, "SS:ESP:%04X:%08X  EBP:%08X\n",
            _ctx.SegSs, _ctx.Esp, _ctx.Ebp );
  fprintf(f, "DS:%04X  ES:%04X  FS:%04X  GS:%04X\n",
            _ctx.SegDs, _ctx.SegEs, _ctx.SegFs, _ctx.SegGs );
  fprintf(f, "Flags:%08X\n", _ctx.EFlags );
}

//! Xbox context handling implementation
class ContextHolderXbox: public IContextHolder
{
  CONTEXT_XBOX _ctx;
  
  public:
  ContextHolderXbox(const CONTEXT_XBOX &ctx)
  {
    _ctx = ctx;
  }
  virtual DWORD Eip() const {return _ctx.Eip;}
  virtual DWORD Esp() const {return _ctx.Esp;}
  // some better method of determining Xbox start address would be helpfull
  //virtual int GetPhysicalOffset() const {return -0x3ef440;}
  
  //virtual int GetPhysicalOffset() const {return CODE_OFFSET_XBOX-CODE_OFFSET;}
  
  virtual void PrintRegisters(FILE *f);
};

void ContextHolderXbox::PrintRegisters(FILE *f)
{
  fprintf(f,"EAX:%08X EBX:%08X\nECX:%08X EDX:%08X\nESI:%08X EDI:%08X\n",
        _ctx.Eax, _ctx.Ebx, _ctx.Ecx, _ctx.Edx, _ctx.Esi, _ctx.Edi );

  fprintf(f, "CS:EIP:%04X:%08X\n", _ctx.SegCs, _ctx.Eip );
  fprintf(f, "SS:ESP:%04X:%08X  EBP:%08X\n",
            _ctx.SegSs, _ctx.Esp, _ctx.Ebp );
  //fprintf(f, "DS:%04X  ES:%04X  FS:%04X  GS:%04X\n",
  //          _ctx.SegDs, _ctx.SegEs, _ctx.SegFs, _ctx.SegGs );
  fprintf(f, "Flags:%08X\n", _ctx.EFlags );
}

void PrintContext(ProcessImage &process, IContextHolder *ctx)
{
  if (process.Loaded())
  {
    char code[1024];

    *code = 0;
    BYTE *ip = (BYTE *)(ctx->Eip()-process.PhysicalOffset()+process.Memory());
    DWORD codeBeg = process.GetCodeStart()+process.PhysicalOffset();
    DWORD codeEnd = process.GetCodeSize()+codeBeg;
    if (ctx->Eip()<codeBeg || ctx->Eip()>codeEnd)
    {
      _tprintf( _T("Fault address outside main module.\n") );
    }
    else
    {
      for (int i=0; i<16; i++)
      {
        sprintf(code+strlen(code)," %02X",((unsigned char *)ip-16)[i]);
      }
      _tprintf( _T("Prev. code bytes:%s\n"), code );

      *code = 0;
      for (int i=0; i<16; i++)
      {
        sprintf(code+strlen(code)," %02X",((unsigned char *)ip)[i]);
      }
      _tprintf( _T("Fault code bytes:%s\n"), code );
    }
  }

  // Show the registers
  _tprintf( _T("\nRegisters:\n") );

  ctx->PrintRegisters(stdout);
}

class AnalyseCalls
{
  public:
  enum {MaxCalls = 16*1024};

  int _pcValue[MaxCalls]; // pc logical address
  int _fStart[MaxCalls]; // pc function start logical address
  int _called[MaxCalls]; // called logical address
  int _calls; // actual number of stored calls

  public:
  void DeleteCall(int i);
  void OptimizeCalls(int functionStart);
  void PrintCalls(MapFile &map, int pc, bool optimized);
};

void AnalyseCalls::DeleteCall(int i)
{
  for (int j=i+1; j<_calls; j++)
  {
    _pcValue[j-1] = _pcValue[j]; // pc logical address
    _fStart[j-1] = _fStart[j]; // pc logical address
    _called[j-1] = _called[j]; // pc logical address
  }
  _calls--;
}



void AnalyseCalls::OptimizeCalls(int functionStart)
{
  // functionStart should be handled as _fStart[-1]
  {
    int i=-1;
    // check if we know where was this place called from
    int calledFrom = -1;
    for (int j=0; j<_calls; j++)
    {
      // in case of recursion we cannot proceed
      if (_fStart[j]==functionStart) break;
      // check if this is the call-site
      if (_called[j]==functionStart) {calledFrom=j;break;}
    }
    if (calledFrom>=0)
    {
      // remove anything between i dan calledFrom
      for (int j=0; j<calledFrom; )
      {
        DeleteCall(j);
        calledFrom--; // calledFrom index is moved
      }
    }

  }
  // remove functions that are sure to be skipped
  for (int i=0; i<_calls; i++)
  {
    // check if we know where was this place called from
    int calledFrom = -1;
    for (int j=i+1; j<_calls; j++)
    {
      // in case of recursion we cannot proceed
      if (_fStart[j]==_fStart[i]) break;
      // check if this is the call-site
      if (_called[j]==_fStart[i]) {calledFrom=j;break;}
    }
    if (calledFrom<0) continue; // we do not know the call site
    // remove anything between i dan calledFrom
    for (int j=i+1; j<calledFrom; )
    {
      DeleteCall(j);
      calledFrom--; // calledFrom index is moved
    }

  }
  // remove calls that are impossible
  // note: first call (index _calls-1) is always possible
  for (int i=0; i<_calls-1; i++)
  {
    if (_called[i]==0) continue; // indirect call - cannot remove
    bool callPossible = functionStart==_called[i];
    for (int j=0; j<i; j++)
    {
      if (_fStart[j]==_called[i]) {callPossible = true;break;}
    }
    if (callPossible) continue;
    DeleteCall(i);
    i--;
  }

  // TODO: remove impossible indirect calls
}


void AnalyseCalls::PrintCalls(MapFile &map, int pc, bool optimized)
{
  IntelPCPrint(map,pc);

  //int minPc=map.MinLogicalAddress();
  //int maxPc=map.MaxLogicalAddress();
  
  int functionStart=0;
  map.MapNameFromPhysical(pc,&functionStart);

  int lastFunctionStart = functionStart;
  for (int i=0; i<_calls; i++)
  {
    int pc = _pcValue[i];
    int called = _called[i];

    int nameValue=0;
    const char *name=map.MapNameFromPhysical(pc,&nameValue);
    int nameOffset = pc-nameValue;

  
    

    //IntelPCPrint(map,pc, false);

    // print call information

    if (!called)
    {
      _tprintf( _T("%8X %8X %8X + %-30s"),
                pc, nameValue, nameOffset, name );
      _tprintf( _T("     (Indirect)\n") );
    }

    else if (lastFunctionStart!=called)
    {
      _tprintf( _T("%8X %8X %8X + %-30s"),
                pc, nameValue, nameOffset, name );
      #if _DEBUG
      int nameValue=0;
      const char *name=map.MapNameFromPhysical(called,&nameValue);
      int nameOffset = called-nameValue;
      _tprintf( _T("     -> %8X : %s+%X\n"), called, name, nameOffset );
      #else
      _tprintf( _T("     -> %8X\n"), called );
      #endif
    }
    else
    {
      _tprintf( _T("%8X %8X %8X + %s"),
                pc, nameValue, nameOffset, name );
      _tprintf( _T("\n") );
    }
    lastFunctionStart = nameValue;

    
  }
}


void IntelStackWalk
(
  MapFile &map, ProcessImage &process,
  DWORD eip, DWORD *pStackTop, DWORD *pStackBot
)
{
  if( pStackBot<pStackTop ) pStackBot=pStackTop+4*1024; // in DWORDS

  if (!process.Loaded())
  {
    _tprintf( _T("\nWarning: Process not loaded, callstack optimization not possible\n") );
    _tprintf( _T("\n----------------------------------------------------------------\n") );
  }

  _tprintf( _T("\nCall stack:\n") );
  _tprintf( _T("\nStack size %d DWORDs\n"), pStackBot-pStackTop );

  _tprintf( _T("Address  Logical            Function\n") );

  DWORD pc = eip;
  
  if( map.Empty() ) return;

  size_t minPc=map.MinPhysicalAddress();
  size_t maxPc=map.MaxPhysicalAddress();
  
  //int offset=map.MinPhysicalAddress()-map.MinLogicalAddress();
  
  if (process.Loaded())
  {
    
    AnalyseCalls calls;
    // two pass processing
    calls._calls = 0;

    int functionStart=0;
    map.MapNameFromPhysical(pc,&functionStart);

    PDWORD stack;
    for( stack=pStackTop; stack<pStackBot; stack++ )
    {
      //if( IsBadReadPtr(stack,4) ) break;
      size_t abspc = *stack;

      //size_t logpc=abspc-offset;
      if( abspc<minPc || abspc>=maxPc ) continue;

      // check start of the function
      int functionStart=0;

      map.MapNameFromPhysical(abspc,&functionStart);

      // Can two DWORDs be read from the supposed frame address?          
      //if ( IsBadCodePtr((int (__stdcall*)())abspc) ) continue;

      // try to verify if we could be called from this address
      // check for known call instructions

      DWORD calledAddress = -1; // -1 -> no call

      // read some bytes from the abspc

      // read process memory

      BYTE *pcCode = (BYTE *)abspc;

      if( !process.IsBadReadPtr(pcCode-6,6) )
      {
        // convert from process address space to mapped address space
        BYTE *pcCodeRd = (BYTE *)((size_t)pcCode-process.PhysicalOffset()+process.Memory());
        // note: cs is not the same as ds
        // this may result in some conversions necessary?
        // advance PC validation
        if (pcCodeRd[-5]==0xe8)
        {
          // simple relative call instruction
          LONG relativeOffset = *(LONG *)(pcCodeRd-4);
          DWORD addr = abspc+relativeOffset;
          calledAddress = addr /*-0x54CB*/;
        }
        else
        {
          // check 2..6 B call instruction
          for (int b=-2; b>=-6; b--)
          {
            // check R/M byte
            // bits 5,4,3 of R/M should be 010
            if (pcCodeRd[b]!=0xff) continue;
            if ( ((pcCodeRd[b+1]>>3)&7)!=2) continue;
            calledAddress = 0;
            break;
          }
        }
      }

      if (calledAddress!=-1)
      {
        // some call detected

        if (calls._calls<calls.MaxCalls)
        {
          // store call information
          calls._pcValue[calls._calls] = abspc; // abspc logical address
          calls._fStart[calls._calls] = functionStart; // abspc function start logical address
          calls._called[calls._calls] = calledAddress; // called logical address

          calls._calls++;
        }
      }

    }

    calls.PrintCalls(map,pc,false);

    _tprintf( _T("\n------- Begin:: Optimized callstack:\n") );
    calls.OptimizeCalls(functionStart);
    calls.PrintCalls(map,pc,true);
    _tprintf( _T("\n------- End  :: Optimized callstack:\n") );
  }
  else
  {
    IntelPCPrint(map,pc);
    // dump call stack (old way)
    PDWORD stack;
    for( stack=pStackTop; stack<pStackBot; stack++ )
    {
      // Can two DWORDs be read from the supposed frame address?          
      //if( IsBadReadPtr(stack,4) ) break;
      size_t abspc = *stack;

      //size_t logpc=abspc; //-offset;
      if( abspc<minPc || abspc>=maxPc ) continue;

      // check start of the function
      //if ( IsBadCodePtr((int (__stdcall*)())abspc) ) continue;

      // log the value
      IntelPCPrint(map,abspc, true);

    }
  }
}


const char *SkipStart(const char *text, const char *start)
{
  int len = strlen(start);
  if (strnicmp(text,start,len)) return NULL;
  return text+len;
}



int ProcessImage::Load(const char *name, int explicitOffset)
{
  #if USE_CREATE_PROCESS
  // map process file into memory
  // use CreateProcess to load process
  STARTUPINFO startupInfo;
  memset(&startupInfo,0,sizeof(startupInfo));
  startupInfo.cb = sizeof(startupInfo);
  if
  (
    !CreateProcess
    (
      name,NULL,NULL,NULL,false,
      CREATE_SUSPENDED,
      NULL,NULL,&startupInfo,&_pi
    )
  )
  {
    fprintf(stderr,"Cannot create process %s\n",name);
    return 1;
  }

  const int headerSize = 0x1000;
  char header[headerSize];

  ::ReadProcessMemory(_pi.hProcess,(void *)CODE_OFFSET,&header,headerSize,NULL);

  PIMAGE_DOS_HEADER dosHdr = (PIMAGE_DOS_HEADER )header;
  PIMAGE_NT_HEADERS ntHdr = (PIMAGE_NT_HEADERS )(header + dosHdr->e_lfanew);

  // check all sections
  PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION( ntHdr );

  physicalOffset = CODE_OFFSET;
  
  // section 1 is code section
  codeStart = pSection[0].VirtualAddress;
  codeSize = pSection[0].SizeOfRawData;
  if (ntHdr->FileHeader.NumberOfSections>1)
  {
    // protect also section 2 (data section)
    dataStart = pSection[1].VirtualAddress;
    dataSize = pSection[1].SizeOfRawData;
  }

  totalStart = codeStart;
  totalSize = codeSize;
  /*
  if (dataStart==codeStart+codeSize)
  {
    totalSize += dataSize;
  }
  */

  // copy whole process address space (code and data section)
  memCopy.Realloc(totalSize+headerSize);
  ::ReadProcessMemory
  (
    _pi.hProcess,(void *)CODE_OFFSET,memCopy,totalSize+headerSize,NULL
  );
  Unload();
  #else
  // load exe file, assume no relocation is necessary
  int handle = open(name,_O_RDONLY|_O_BINARY);
  if (handle>=0)
  {
    // read process header
    // it may be .xbe or .exe

    char magic[4];
    
    read(handle,magic,sizeof(magic));
    lseek(handle,-(long)sizeof(magic),SEEK_CUR);
    

    if (magic[0]=='M' && magic[1]=='Z')
    {
      const int headerSize = 0x1000;
      char header[headerSize];

      read(handle,header,headerSize);
      lseek(handle,-headerSize,SEEK_CUR);
      
      PIMAGE_DOS_HEADER dosHdr = (PIMAGE_DOS_HEADER )header;
      PIMAGE_NT_HEADERS ntHdr = (PIMAGE_NT_HEADERS )(header + dosHdr->e_lfanew);
      if (ntHdr->Signature==IMAGE_NT_SIGNATURE)
      {
        // check all sections
        PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION( ntHdr );

        physicalOffset = CODE_OFFSET;
        
        // section 1 is code section
        codeStart = pSection[0].VirtualAddress;
        codeSize = pSection[0].SizeOfRawData;
        if (ntHdr->FileHeader.NumberOfSections>1)
        {
          // protect also section 2 (data section)
          dataStart = pSection[1].VirtualAddress;
          dataSize = pSection[1].SizeOfRawData;
        }

        totalStart = codeStart;
        totalSize = codeSize;

        // copy whole process address space (code and data section)
        memCopy.Realloc(totalSize+headerSize);
        read(handle,memCopy,totalSize+headerSize);
      }
    }
    else if (!strncmp(magic,"XBEH",4))
    {
      // XBE executable - load
      xbe::header header;
      
      read(handle,&header,sizeof(header));
      Assert(header.m_magic==XBE_HEADER_MAGIC);
      lseek(handle,-(long)sizeof(header),SEEK_CUR);

      int headerSize = header.m_sizeof_headers;
      int imageSize = header.m_sizeof_image;
      
      // section 1 is code section
      codeStart = header.m_base;
      codeSize = imageSize;
      
      totalStart = codeStart;
      totalSize = codeSize;

      // copy whole process address space (code and data section)
      memCopy.Realloc(totalSize+headerSize);
      
      read(handle,memCopy,totalSize+headerSize);

      int sectionsOffset = header.m_section_headers_addr-header.m_base;
      xbe::section_header *sections = (xbe::section_header *)(memCopy.Data()+sectionsOffset);
      int nSections = header.m_sections;
      // access sections
      if (header.m_sections>1)
      {
        // protect also section 2 (data section)
        dataStart = sections[1].m_virtual_addr;
        dataSize = sections[1].m_virtual_size;
      }
      if (header.m_sections>0)
      {
        codeStart = sections[0].m_virtual_addr;
        codeSize = sections[0].m_virtual_size;
      }
      // we have both code and data section ready
      
      /*
      if (explicitOffset) 
      {
        physicalOffset = explicitOffset;
      }
      else
      */
      {
        physicalOffset = header.m_base;
      }
      
      codeStart -= physicalOffset;
      dataStart -= physicalOffset;
      
    }

    close(handle);
  }
  #endif

  return 0;
}

void ProcessImage::Unload()
{
  // we read what we need - terminate process and close handle
  #if USE_CREATE_PROCESS
  if (_pi.hProcess)
  {
    ::TerminateProcess(_pi.hProcess,1);
    // wait until process really terminates
    ::WaitForSingleObject(_pi.hProcess,60000);
    ::CloseHandle(_pi.hProcess);
    ::CloseHandle(_pi.hThread);
  }
  _pi.hProcess = NULL;
  _pi.hThread = NULL;
  #endif
}

ProcessImage::ProcessImage()
{
}

ProcessImage::~ProcessImage()
{
}

bool FindFileExact(char *mapTemp,const char *archiveWild)
{
  _finddata_t data;
  long h = _findfirst(archiveWild,&data);
  if (h!=-1)
  {
    strcpy(mapTemp,archiveWild);
    strcpy(GetFilenameExt(mapTemp),data.name);
    _findclose(h);
    return true;
  }
  return false;
}

class MapFileCached: public MapFile
{
  RString _filename;

  public:
  void Load(const char *name);
};

void MapFileCached::Load(const char *name)
{
  if (name && !strcmpi(name,_filename)) return;
  Clear();
  ParseMapFile(name);
  _filename = name;
}

static MapFileCached GMapFile;

bool PrintPCVersion
(
  const char *verString, const char *mapExt, const char *archive,
  DWORD eip, bool quiet
)
{
  char archiveWild[1024];
  char mapTemp[1024];
  sprintf(archiveWild,"%s\\%s\\*.map",archive,verString);
  static MapFileCached map;
  if (FindFileExact(mapTemp,archiveWild))
  {
    map.Load(mapTemp);
  }
  else if (!quiet)
  {
    fprintf
    (
      stderr,"Error: no map file found for version %s - %s\n",
      verString,mapExt
    );
  }
  if (!map.Empty())
  {
    if (quiet)
    {
      printf("%s? ",verString);
    }
    else
    {
      printf("%s - %s: ",verString,mapExt);
    }
    IntelPCPrint(map,eip, true);
    return true;
  }
  return false;
}

static int ProcessTextInput(FILE *crash, const char *archive)
{
  // check for line "Version"
  char line[1024];
  bool ok = false;
  int major = 1;
  int minor = 20;
  bool explicitVer = false; // version known, or guessed?
  bool inFrozen = false;
  for(;;)
  {
    char *ret = fgets(line,sizeof(line),crash);
    if (!ret) break;
    if (*line && line[strlen(line)-1]=='\n') line[strlen(line)-1]=0;
    if (!*line) continue;
    char *scan = line;
    const char *version = "Version ";
    const char *fault = "Fault address: ";
    const char *frozenBeg = "===FROZEN====>>>>>>BEG";
    const char *frozenEnd = "===FROZEN====>>>>>>END";
    const char *errorMessage = "ErrorMessage:";

    if (!strncmp(frozenBeg,line,strlen(frozenBeg)))
    {
      inFrozen = true;
    }
    else if (!strncmp(frozenEnd,line,strlen(frozenEnd)))
    {
      inFrozen = false;
    }
    else if (!strncmp(errorMessage,line,strlen(errorMessage)))
    {
      _tprintf( "%s\n", line );
    }
    else if (!strncmp(version,line,strlen(version)))
    {
      scan += strlen(version);
      major = strtoul(scan,&scan,10);
      minor = 999;
      if (*scan=='.')
      {
        scan++;
        minor = strtoul(scan,&scan,10);
        explicitVer = true;
      }
    }
    else if (!strncmp(fault,line,strlen(fault)))
    {
      scan += strlen(fault);
      while (isspace(*scan)) scan++;
      DWORD eip = strtoul(scan,&scan,16);
      while (isspace(*scan)) scan++;
      int section = strtoul(scan,&scan,16);
      if (*scan==':') scan++;
      int logaddr = strtoul(scan,&scan,16);
      while (isspace(*scan)) scan++;
      if (inFrozen) _tprintf( "Frozen:\n" ),inFrozen = false;
      if (section==1 && (unsigned long)logaddr==eip-0x401000)
      {
        ok = true;
        const char *exename = GetFilenameExt(scan);
        const char *ext = GetFileExt(exename);
        if (strcmpi(ext,".exe"))
        {
          // non-exe, print module name
          _tprintf( "%s\n", line );
        }
        else
        {
          // we have real EIP
          // we have version number
          // now we can dump it
          // check if we know archive extension

          const char *archiveSpec = archive;
          BString<1024> buf;

          RString exeLowName = exename;
          exeLowName.Lower();

          char mapExt[16];
          *mapExt = 0;
          if (strstr(exeLowName,"server"))
          {
            strcpy(mapExt,"DS");
          }

          // append mapExt - if not there already
          size_t lenMapExt = strlen(mapExt);
          if (strlen(archive)>lenMapExt && strcmpi(archive+strlen(archive)-lenMapExt,mapExt))
          {
            strcpy(buf,archive);
            strcat(buf,mapExt);
            archiveSpec = buf;
          }

          if (explicitVer)
          {
            char verString[128];
            sprintf(verString,"%d.%02d",major,minor);
            PrintPCVersion(verString,mapExt,archiveSpec,eip,false);
          }
          else
          {
            // if no explicit version, can be from
            // any version before version logging was introduced
            static const char *verNames[] =
            {
              "1.00", "1.10","1.20", // regular versions
              "1.26","1.27", // beta versions
              NULL
            };
            // try several versions:
            for (const char **ver=verNames; *ver; ver++)
            {
              PrintPCVersion(*ver,"",archiveSpec,eip,true);
            }
          }
        }
      }
      else
      {
        ok = true;
        // non-exe, print module name
        if (section)
        {
          _tprintf( "%X %X:%X %s\n", eip, section, logaddr, scan );
        }
        else
        {
          _tprintf( "%s\n", line );
        }
      }
      
    }


  }
  if (!ok)
  {
    fprintf(stderr,"Error: Bad input format\n");
    return 1;
  }
  return 0;
}

static void FlushCRCDiff(unsigned int beg, unsigned int end, MapFile &map)
{
  printf("CRC %6x: (%6x) ",beg,end-beg);

  const char *nameBeg = map.MapNameFromPhysical(beg);
  if (nameBeg) printf("%s",nameBeg);
  printf(" to ");
  const char *nameEnd = map.MapNameFromPhysical(end);
  if (nameEnd) printf("%s\n",nameEnd);
}

int consoleMain(int argc, const char *argv[])
{
  // scan source paths, generate list of \patch sections
  // send output to stdout
  const char *mapfile = NULL;
  const char *exefile = NULL;
  const char *archive = NULL;
  const char *path = NULL;

  while (argc>1)
  {
    if (*argv[1]=='-')
    {
      const char *option = argv[1]+1;
      const char *oarg = NULL;
      if ((oarg = SkipStart(option,"map="))!=NULL)
      {
        mapfile = oarg;
        #if _DEBUG
        fprintf(stderr,"Map %s\n",oarg);
        #endif
      }
      else if ((oarg = SkipStart(option,"exe="))!=NULL)
      {
        exefile = oarg;
        #if _DEBUG
        fprintf(stderr,"Exe %s\n",oarg);
        #endif
      }
      else if ((oarg = SkipStart(option,"archive="))!=NULL)
      {
        archive = oarg;
        #if _DEBUG
        fprintf(stderr,"Exe %s\n",oarg);
        #endif
      }
      /*
      else if (!strcmpi(option,"noheaders"))
      {
        pars.skipDateHeader = true; 
        #if _DEBUG
        fprintf(stderr,"No headers\n");
        #endif
      }
      */
      argv++;
      argc--;
    }
    else break;
  }
  if (argc!=2)
  {
    printf
    (
      "Usage: crashanalyse {-option} crashfile_path\n"
      "Options:\n"
      "    -map=<path>      map file to use\n"
      "    -exe=<path>      exe file used to optimize callstack\n"
      "    -archive=<path>  map/exe archive directory\n"
      "\n"
      "Archives:\n"
      "Archive directory should contain subdirectories named by the build (revision) number\n"
      "in each version subdirectory should be one exe and one map file\n"
      "\n"
    );
    return 1;
  }

  path = argv[1];
  
  #ifndef _DEBUG
  // redirect stdout into the file
  int stdHandle = fileno(stdout);
  if (stdHandle<2)
  {
    // add .rpt to the input file name
    char outName[512];
    strcpy(outName,path);
    strcat(outName,".birpt");
    freopen(outName,"w",stdout);
  }
  #endif
  
  FILE *crash = fopen(path,"rb");
  if (!crash)
  {
    fprintf(stderr,"Error: Cannot read %s\n",path);
    return 1;
  }
  // get 
  static const char magic7[]="STK7"; // since OFP 2.03
  static const char magic6[]="STK6"; // since OFP 2.03
  static const char magic5[]="STK5"; // since OFP 2.03
  static const char magic4[]="STK4"; // since OFP 1.28
  static const char magic3[]="STK3"; // since OFP 1.12
  static const char magic2[]="STK2"; // since OFP 1.04
  const int pagesize = 4*1024;
  int contextVer = 0;
  char magicRd[sizeof(magic2)];
  fread(magicRd,sizeof(magicRd),1,crash);
  if (!strcmp(magicRd,magic2)) contextVer = 2;
  else if (!strcmp(magicRd,magic3)) contextVer = 3;
  else if (!strcmp(magicRd,magic4)) contextVer = 4;
  else if (!strcmp(magicRd,magic5)) contextVer = 5;
  else if (!strcmp(magicRd,magic6)) contextVer = 6;
  else if (!strcmp(magicRd,magic7))
  {
    int rd = fread(&contextVer,sizeof(contextVer),1,crash);
    if (rd!=1 || contextVer<100 || contextVer>10000) contextVer = 0;
  }

  if (contextVer==0)
  {
    if (archive)
    {
      // it might be text file input
      // special processing required
      fclose(crash);
      crash = fopen(path,"r");
      if (crash) 
      {
        int ret = ProcessTextInput(crash,archive);
        fclose(crash);
        #if _DEBUG
          return 1;
        #else
          return ret;
        #endif
      }
    }
    fprintf(stderr,"Error: Cannot read %s - unknown file format\n",path);
    return 1;
  }

  if (contextVer>103)
  {
    fprintf(stderr,"Error: More recent analysis app needed to analyze %d version crashdump\n",contextVer);
  }
  // mapExt is used to distinguish exe configurations (Czech, Dedicated server)
  char mapExt[3]; // archive extension (2 letters)
  mapExt[0] = 0;
  // context.bin was introduced in 102,
  // 103 is last version with STK1 format
  Temp<unsigned int> crcData;
  int pStackBot;
  Ref<IContextHolder> context;
  int ver = 0;
  int build = 0;
  int exeSize = 0;
  int codeSize = 0;

  // analysed module should contain FunctionWithKnownAddress, decorated as
  // 
  
  int knownFunctionAddress = 0;
  char shortName[512];
  char sourceName[512];
  char guessName[512];
  strcpy(shortName,"");
  strcpy(sourceName,"");
  if (contextVer==2)
  {
    // get context
    CONTEXT ctx;
    fread(&ctx,sizeof(ctx),1,crash);
    fread(&ver,sizeof(ver),1,crash);
    context = new ContextHolderI86(ctx);
  }
  else if (contextVer>=3)
  {
    fread(&ver,sizeof(ver),1,crash);
    if (contextVer>=103)
    {
      fread(&build,sizeof(build),1,crash);
    }
    if (contextVer>=6)
    {
      int contextSize;
      fread(&contextSize,sizeof(contextSize),1,crash);
      if (contextSize==sizeof(CONTEXT))
      {
        CONTEXT ctx;
        fread(&ctx,sizeof(ctx),1,crash);
        context = new ContextHolderI86(ctx);
      }
      else if (contextSize==sizeof(CONTEXT_XBOX))
      {
        CONTEXT_XBOX ctx;
        fread(&ctx,sizeof(ctx),1,crash);
        context = new ContextHolderXbox(ctx);
      }
      else
      {
        // unknown context - cannot read
        fseek(crash,contextSize,SEEK_CUR);
      }
    }
    else
    {
      CONTEXT ctx;
      fread(&ctx,sizeof(ctx),1,crash);
      context = new ContextHolderI86(ctx);
    }
    // extended info about exe file
    if (contextVer>=5)
    {
      int len;
      fread(&len,sizeof(len),1,crash);
      if (len<0 || len>=sizeof(shortName))
      {
        fprintf(stderr,"Read format error.\n");
        return 1;
      }
      fread(shortName,len,1,crash);
      printf("Exe file %s\n",shortName);
      if (!exefile)
      {
        // check if such file exists
        // if not, base mapile on report name
        struct _stat st;
        if (_stat(shortName,&st)>=0)
        {
          exefile = shortName;
        }
        else // try also exe based on map file name
        {
          strcpy(guessName,path);
          strcpy(GetFilenameExt(guessName),shortName);
          if (_stat(guessName,&st)>=0)
          {
            exefile = guessName;
          }
          
        }
      }
      if (contextVer>=102)
      {
        fread(&len,sizeof(len),1,crash);
        if (len<0 || len>=sizeof(sourceName))
        {
          fprintf(stderr,"Read format error.\n");
          return 1;
        }
        fread(sourceName,len,1,crash);
        printf("Source %s\n",sourceName);
      }
    }
    fread(&exeSize,sizeof(exeSize),1,crash);
    if (contextVer>=4)
    {
      fread(mapExt,2,1,crash);
      mapExt[2]=0;
    }
    printf("Exe file size: %d B\n",exeSize);
    void *codeStart = NULL;
    if (contextVer>=101)
    {
      fread(&codeStart,sizeof(codeStart),1,crash);
      printf("Code start %x\n",codeStart);
    }
    fread(&codeSize,sizeof(codeSize),1,crash);
    printf("Code size %d B\n",codeSize);
    if (contextVer>=101)
    {
      fread(&knownFunctionAddress,sizeof(knownFunctionAddress),1,crash);
      printf("Known function address %x\n",knownFunctionAddress);
    }

    int crcCount = codeSize/pagesize;
    crcData.Realloc(crcCount);
    for (int i=0; i<crcCount; i++)
    {
      unsigned int crcResult;
      fread(&crcResult,sizeof(crcResult),1,crash);
      crcData[i] = crcResult;
    }
  }
  if (!context)
  {
    fprintf(stderr,"Unknown context type.\n");
    return 1;
  }
  int eip = context->Eip();
  int pStackTop = context->Esp();

  int major = ver/100;
  int minor = ver%100;
  printf("Context from version %d.%02d.%d - %s\n",major,minor,build,mapExt);
  fread(&pStackBot,sizeof(pStackBot),1,crash);
  int stackLen = pStackBot - pStackTop;
  int pStackLenCheck = stackLen;
  if (contextVer>=100)
  {
    fread(&pStackLenCheck,sizeof(pStackLenCheck),1,crash);
  }
  if (ferror(crash) || feof(crash))
  {
    fprintf(stderr,"Error while reading header.\n");
    return 1;
  }

  if (stackLen<0 || stackLen>16*1024*1024)
  {
    fprintf(stderr,"Error: Callstack too big\n");
    return 1;
  }
  Temp<DWORD> stack(stackLen/4);
  int rd = fread(stack,stackLen,1,crash);
  if (ferror(crash) || rd!=1)
  {
    fprintf(stderr,"Error while reading callstack.\n");
    stack.Realloc(0);
    //return 1;
  }
  fclose(crash);


  char mapTemp[1024];
  char exeTemp[1024];

  if (archive)
  {
    char archiveWild[1024];
    sprintf(archiveWild,"%s%s\\%d\\*.map",archive,mapExt,build);

    if (FindFileExact(mapTemp,archiveWild))
    {
      mapfile = mapTemp;
    }
    else
    {
      fprintf
      (
        stderr,"Error: no map file found for version %d.%02d.%d\n",
        major,minor,build
      );
      return 1;
    }

    strcpy(GetFilenameExt(archiveWild),"*.exe");
    if (FindFileExact(exeTemp,archiveWild))
    {
      exefile = exeTemp;
    }
  }

  if (!mapfile)
  {
    // deduce mapfile name from the exe name
    if (exefile)
    {
      strcpy(mapTemp,exefile);
    }
    else
    {
      strcpy(mapTemp,path);     
    }
    strcpy(GetFileExt(mapTemp),".map");
    mapfile = mapTemp;
  }
  if (!exefile)
  {
    strcpy(exeTemp,path);
    strcpy(GetFileExt(exeTemp),".xbe");
    exefile = exeTemp;
  }
  MapFile map;
  map.ParseMapFile(mapfile);
  if (map.Empty())
  {
    fprintf(stderr,"Error: Mapfile %s is empty\n",mapfile);
    return 1;
  }
  
  int explicitOffset = 0;
  if (knownFunctionAddress)
  {
    // check function physical address
    int mapKnownFunctionAddress = map.PhysicalAddress("_FunctionWithKnownAddress");
    if (mapKnownFunctionAddress)
    {
      explicitOffset = knownFunctionAddress-mapKnownFunctionAddress;
      if (explicitOffset<0) printf("Map offset -%x\n",-explicitOffset);
      else if (explicitOffset>0) printf("Map offset +%x\n",+explicitOffset);
      map.OffsetPhysicalAddress(explicitOffset);

      //int cOffset = context->GetPhysicalOffset();
      //if (cOffset<0) printf("Map def. offset -%x\n",-cOffset);
      //else printf("Map def. offset +%x\n",+cOffset);
    }
  }

  ProcessImage process;
  if (exefile)
  {
    process.Load(exefile,explicitOffset);
    //{
    //  return 1;
    //}
  }


  // check if mapfile is from corresponding version

  static const char verIdString[] = "VersionMapID";
  int mapIdAddr = map.PhysicalAddressBySubstring(verIdString);
  bool verified = false;
  if (mapIdAddr>0)
  {
    const char *rawName = map.MapRawName(mapIdAddr,-1,&MapInfo::physAddress);

    // find important part of version info
    const char *name = strstr(rawName,verIdString);
    if (name)
    {
      name += strlen(verIdString);
      // scan information from name
      int majorId = *name++-'0';
      int minorId = -1;
      if (name[0]=='?' && name[1]=='4')
      {
        minorId = strtoul(name+2,NULL,10);
      }
      if (major==majorId && minor==minorId) verified = true;
      else printf("Map file from version %d.%02d\n",majorId,minorId);
    }
    //sprintf(mapId,"??_C@_0BB@LEAL@VersionMapID%d?4%02d?$AA@",major,minor);
  }
  if (!verified)
  {
    fprintf(stderr,"Warning: map file is from different version\n");
  }

  if (crcData.Size()>0)
  {
    //int offset = CODE_OFFSET+0x1000;

    if (process.Loaded() && codeSize!=process.GetCodeSize())
    {
      printf
      (
        "Code size different: old %d B, new %d B\n",
        codeSize,process.GetCodeSize()
      );
    }

  
    unsigned int begCRCDiff = 0;
    unsigned int endCRCDiff = 0;
    bool crcDiffOpen = false;

    if (process.Loaded()) for (int i=0; i<crcData.Size(); i++)
    {
      size_t pagestart = pagesize*i+process.GetCodeStartAddress();
      if (process.GetCodeEndAddress()>=pagestart+pagesize)
      {
        CRCCalculator crc;
        unsigned int crcResult = crc.CRC(process.Memory()+pagestart,pagesize);
        unsigned int crcResultExpected = crcData[i];
        if (crcResult!=crcResultExpected)
        {
          if (!crcDiffOpen)
          {
            begCRCDiff = pagestart;
            endCRCDiff = pagestart+pagesize;
            crcDiffOpen = true;
          }
          else if (endCRCDiff==pagestart)
          {
            endCRCDiff = pagestart+pagesize;
          }
          else
          {
            FlushCRCDiff(begCRCDiff+process.PhysicalOffset(),endCRCDiff+process.PhysicalOffset(),map);
            begCRCDiff = pagestart;
            endCRCDiff = pagestart+pagesize;
          }
          //FlushCRCDiff(pagestart,pagestart,map);
        }
      }
    } // for(i)
    if (crcDiffOpen)
    {
      FlushCRCDiff(begCRCDiff+process.PhysicalOffset(),endCRCDiff+process.PhysicalOffset(),map);
    }
  }

  
  if (contextVer>=2)
  {
    PrintContext(process,context);
  }
  IntelStackWalk
  (
    map,process,eip,stack.Data(),stack.Data()+stack.Size()
  );

  // if (!noPause) wasError = true;

  stack.Free();

  return 0;
}
