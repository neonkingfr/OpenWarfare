#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MAPFILE_HPP
#define _MAPFILE_HPP

#include <string.h>
#include <Es/Containers/typeopts.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Strings/rstring.hpp>

struct MapInfo
{
	RString name;
	int section;
	int physAddress; // symbol value
	int logAddress; // logical address
};

typedef int MapInfo::*MapAddressId;

TypeIsMovableZeroed(MapInfo);

class MapFile
{
	char _name[256];
	AutoArray<MapInfo> _map;

	int _firstSection2;

	public:
	void ParseMapFile(const char *sourceName);
	const char *GetName() const {return _name;}
	const char *MapRawName( int address, int section, MapAddressId id, int *lower=NULL );
	const char *MapName( int address, MapAddressId id, int *lower=NULL );
	const char *MapNameFromPhysical( int fAddress, int *lower=NULL )
	{
		return MapName(fAddress,&MapInfo::physAddress,lower);
	}
	const char *MapNameFromLogical( int lAddress, int *lower=NULL )
	{
		return MapName(lAddress,&MapInfo::logAddress,lower);
	}

	int AddressBySubstring( const char *name, MapAddressId id ) const ;
	int Address( const char *name, MapAddressId id ) const ;
	int MinAddress( MapAddressId id ) const ;
	int MaxAddress( MapAddressId id ) const ;

	int PhysicalAddressBySubstring( const char *name ) const {return AddressBySubstring(name,&MapInfo::physAddress);}
	int LogicalAddressBySubstring( const char *name ) const {return AddressBySubstring(name,&MapInfo::logAddress);}

	int PhysicalAddress( const char *name ) const {return Address(name,&MapInfo::physAddress);}
	int LogicalAddress( const char *name ) const {return Address(name,&MapInfo::logAddress);}

	int MinPhysicalAddress() const {return MinAddress(&MapInfo::physAddress);}
	int MaxPhysicalAddress() const {return MaxAddress(&MapInfo::physAddress);}
	int MinLogicalAddress() const {return MinAddress(&MapInfo::logAddress);}
	int MaxLogicalAddress() const {return MaxAddress(&MapInfo::logAddress);}

	bool Empty() const {return _map.Size()<=0;}
};

#endif
