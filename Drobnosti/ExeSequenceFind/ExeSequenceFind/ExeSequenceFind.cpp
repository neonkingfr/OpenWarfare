/*
ExeSequenceFind.cpp
- finds bytes (given in source file by offset:size) from code segment of running exe_source in code segment of running exe_find here



Cracked exe:
- is completely different than our .exe
- bad .exe header
- loader fills only 0x00400000 - 0x00401000
- between 0x00400000 - 0x00401000 is code that calls unpacking routines that unpack data starting @ 0x00401000
- unpacked codesegment after 0x00401000 is quite similar to our .exe code segment



Purpose of this utility: hack changes some parts of code segment of cracked exe, question: what functions in engine correspond to these parts? This utility finds similar part of code in code segment of our exe. This part can be then converted to function+offset_in_function using our mapfile.
*/


#include <stdio.h>
#include <io.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <windows.h>

#include <iostream>

#pragma warning(disable:4996)

#include <Es/essencepch.hpp>

#include <Es/Types/Pointers.hpp>
#include <Es/Strings/rString.hpp>
#include <El/Debugging/xboxContext.h>
#include <El/Debugging/xboxFileFormats.h>
#include <El/QStream/qStream.hpp>



#define DO_SLEEP 
#define SLEEP_TIME 1000

#define CODE_OFFSET 0x400000
#define USE_CREATE_PROCESS 1

#define OUR_CODE_SIZE 0x0050b000


//how much of neighbor bytes on each side together with given bytes are forming search pattern
#define HOW_MUCH_NEIGHBOR_BYTES 25

int interestingOffset = 0x001000;



#include <tchar.h>

QOFStream out;


class ProcessImage
{
  #if USE_CREATE_PROCESS
  PROCESS_INFORMATION _pi;
  #endif
  
  Temp<unsigned char> memCopy;
  

  
  public:
  // offset converting logical to physical
  long physicalOffset;
  
  // public access to section boundaries - logical address
  size_t codeStart;
  size_t codeSize;
  size_t dataStart;
  size_t dataSize;

  size_t totalStart;
  size_t totalSize;

  public:
  ProcessImage();
  ~ProcessImage();

  const unsigned char *Memory() const {return memCopy.Data();}
  size_t Size() const {return memCopy.Size();}

  size_t GetCodeStartAddress() const {return codeStart;}
  size_t GetCodeEndAddress() const {return codeStart+codeSize;}

  size_t GetCodeStart() const {return codeStart;}
  size_t GetCodeSize() const {return codeSize;}

  size_t GetDataStartAddress() const {return dataStart;}
  size_t GetDateEndAddress() const {return dataStart+dataSize;}
  long PhysicalOffset() const {return physicalOffset;}

  bool IsBadReadPtr(void *addr, size_t size)
  {
    return
    (
      //(size_t)addr<totalStart+physicalOffset ||
      //(size_t)addr+size>totalStart+physicalOffset+totalSize
      (size_t)addr<(size_t)physicalOffset ||
      (size_t)addr+size>(size_t)physicalOffset+totalSize
    );
  }
  bool Loaded() const {return memCopy.Size()>0;}
  int Load(const char *name, int explicitOffset);
  void StoreMem(const char *name);
  void Unload();

  int MemSize();
  void *MemBufferStart();
};

int ProcessImage::MemSize()
{
  return memCopy.Size();
}
void *ProcessImage::MemBufferStart()
{
  return memCopy.Data();
}

void ProcessImage::StoreMem(const char *name)
{//Dumping memory (copy of code segment of running exe) to name+".dump" file
  int i=0;
  int size=memCopy.Size();
  //printf("Velikost pameti: %d pamet: %s", memCopy.Size(),memCopy.Data()); 
  void *mem=memCopy.Data();

  RString openthisname;
  openthisname = RString(name) + ".dump";
    
  QOFStream out;
  out.open(cc_cast(openthisname));
  out.write(mem,size);
  out.close();
}



int ProcessImage::Load(const char *name, int explicitOffset)
{
  #if USE_CREATE_PROCESS
  // map process file into memory
  // use CreateProcess to load process
  STARTUPINFO startupInfo;
  memset(&startupInfo,0,sizeof(startupInfo));
  startupInfo.cb = sizeof(startupInfo);

  RString commandlinemy;
  commandlinemy = RString(name) + " -window -nosplash";
  LPSTR commandline = (LPTSTR) cc_cast(commandlinemy);

  if
  (
    !CreateProcess
    (
      name,commandline,NULL,NULL,false,
      CREATE_NEW_CONSOLE,
      NULL,NULL,&startupInfo,&_pi
    )
  )
  {
    fprintf(stderr,"Cannot create process %s\n",name);
    return 1;
  }

  //let cracked exe to upack itself into memory...
#ifdef DO_SLEEP
  Sleep(SLEEP_TIME);
#endif

  const int headerSize = 0x1000;
  char header[headerSize];

  ::ReadProcessMemory(_pi.hProcess,(void *)CODE_OFFSET,&header,headerSize,NULL);

  PIMAGE_DOS_HEADER dosHdr = (PIMAGE_DOS_HEADER )header;
  PIMAGE_NT_HEADERS ntHdr = (PIMAGE_NT_HEADERS )(header + dosHdr->e_lfanew);

  // check all sections
  PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION( ntHdr );

  physicalOffset = CODE_OFFSET;
  
  // section 1 is code section
  codeStart = pSection[0].VirtualAddress;
  codeSize = pSection[0].SizeOfRawData;
  if (ntHdr->FileHeader.NumberOfSections>1)
  {
    // protect also section 2 (data section)
    dataStart = pSection[1].VirtualAddress;
    dataSize = pSection[1].SizeOfRawData;
  }

  totalStart = codeStart;
  totalSize = codeSize;
  /*
  if (dataStart==codeStart+codeSize)
  {
    totalSize += dataSize;
  }
  */
//0x0012fd14



  // copy whole process address space (code and data section)

  if (totalSize == 0) totalSize = OUR_CODE_SIZE;
  
  SIZE_T readBytes;
  SIZE_T *rBp= &readBytes;

  memCopy.Realloc(totalSize+headerSize);
  ::ReadProcessMemory
  (
    //_pi.hProcess,(void *)CODE_OFFSET,memCopy,totalSize+headerSize,NULL
    _pi.hProcess,(void *)CODE_OFFSET,memCopy,totalSize+headerSize,rBp
  );
  Unload();

  StoreMem(name);

  #else
  // load exe file, assume no relocation is necessary
  int handle = open(name,_O_RDONLY|_O_BINARY);
  if (handle>=0)
  {
    // read process header
    // it may be .xbe or .exe

    char magic[4];
    
    read(handle,magic,sizeof(magic));
    lseek(handle,-(long)sizeof(magic),SEEK_CUR);
    

    if (magic[0]=='M' && magic[1]=='Z')
    {
      const int headerSize = 0x1000;
      char header[headerSize];

      read(handle,header,headerSize);
      lseek(handle,-headerSize,SEEK_CUR);
      
      PIMAGE_DOS_HEADER dosHdr = (PIMAGE_DOS_HEADER )header;
      PIMAGE_NT_HEADERS ntHdr = (PIMAGE_NT_HEADERS )(header + dosHdr->e_lfanew);
      if (ntHdr->Signature==IMAGE_NT_SIGNATURE)
      {
        // check all sections
        PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION( ntHdr );

        physicalOffset = CODE_OFFSET;
        
        // section 1 is code section
        codeStart = pSection[0].VirtualAddress;
        codeSize = pSection[0].SizeOfRawData;
        if (ntHdr->FileHeader.NumberOfSections>1)
        {
          // protect also section 2 (data section)
          dataStart = pSection[1].VirtualAddress;
          dataSize = pSection[1].SizeOfRawData;
        }

        totalStart = codeStart;
        totalSize = codeSize;

        // copy whole process address space (code and data section)
        memCopy.Realloc(totalSize+headerSize);
        read(handle,memCopy,totalSize+headerSize);
      }
    }
    else if (!strncmp(magic,"XBEH",4))
    {
      // XBE executable - load
      xbe::header header;
      
      read(handle,&header,sizeof(header));
      Assert(header.m_magic==XBE_HEADER_MAGIC);
      lseek(handle,-(long)sizeof(header),SEEK_CUR);

      int headerSize = header.m_sizeof_headers;
      int imageSize = header.m_sizeof_image;
      
      // section 1 is code section
      codeStart = header.m_base;
      codeSize = imageSize;
      
      totalStart = codeStart;
      totalSize = codeSize;

      // copy whole process address space (code and data section)
      memCopy.Realloc(totalSize+headerSize);
      
      read(handle,memCopy,totalSize+headerSize);

      int sectionsOffset = header.m_section_headers_addr-header.m_base;
      xbe::section_header *sections = (xbe::section_header *)(memCopy.Data()+sectionsOffset);
      int nSections = header.m_sections;
      // access sections
      if (header.m_sections>1)
      {
        // protect also section 2 (data section)
        dataStart = sections[1].m_virtual_addr;
        dataSize = sections[1].m_virtual_size;
      }
      if (header.m_sections>0)
      {
        codeStart = sections[0].m_virtual_addr;
        codeSize = sections[0].m_virtual_size;
      }
      // we have both code and data section ready
      
      /*
      if (explicitOffset) 
      {
        physicalOffset = explicitOffset;
      }
      else
      */
      {
        physicalOffset = header.m_base;
      }
      
      codeStart -= physicalOffset;
      dataStart -= physicalOffset;
      
    }

    close(handle);
  }
  #endif

  return 0;
}

void ProcessImage::Unload()
{
  // we read what we need - terminate process and close handle
  #if USE_CREATE_PROCESS
  if (_pi.hProcess)
  {
    ::TerminateProcess(_pi.hProcess,1);
    // wait until process really terminates
    ::WaitForSingleObject(_pi.hProcess,60000);
    ::CloseHandle(_pi.hProcess);
    ::CloseHandle(_pi.hThread);
  }
  _pi.hProcess = NULL;
  _pi.hThread = NULL;
  #endif
}




void compare(void)
{
   char first[]  = "123456789";
   char second[] = "123456789";
   int int_arr1[] = {1,2,3,4};
   int int_arr2[] = {1,2,3,4};
   int result;

   printf( "Compare '%.19s' to '%.19s':\n", first, second );
   result = memcmp( first, second, 19 );
   if( result < 0 )
      printf( "First is less than second.\n" );
   else if( result == 0 )
      printf( "First is equal to second.\n" );
   else
      printf( "First is greater than second.\n" );

   printf( "Compare '%d,%d' to '%d,%d':\n", int_arr1[0], int_arr1[1], int_arr2[0], int_arr2[1]);
   result = memcmp( int_arr1, int_arr2, sizeof(int) * 2 );
   if( result < 0 )
      printf( "int_arr1 is less than int_arr2.\n" );
   else if( result == 0 )
      printf( "int_arr1 is equal to int_arr2.\n" );
   else 
      printf( "int_arr1 is greater than int_arr2.\n" );
}


ProcessImage::ProcessImage()
{
}

ProcessImage::~ProcessImage()
{
}

void printfMem(void *offset, int size)
{
  BYTE *mem = (BYTE *) offset;
  int i;

  printf ("pattern: ");
  for (i=0;i<size;i++)
  {
    printf("%c",(char) mem[i]);
  }
}

void SequenceFindAndStore(int offset_wanted, int size_wanted, ProcessImage processsource, ProcessImage processfindhere)
{//search for pattern given by offsed and size, use stored copies of code segments
  void *pattern;

  int found = 0;
  int neighbor_bytes = HOW_MUCH_NEIGHBOR_BYTES;

  while ((!found) && (neighbor_bytes >=0))
  {//search for pattern, decrease pattern size if not found
    int offset = offset_wanted - neighbor_bytes;
    if (offset <0) offset = 0;

    int memfindheresize = processfindhere.MemSize();
    int howmuchbytesleft = memfindheresize - offset;
    
    int patternSize = size_wanted + 2 * neighbor_bytes;
    if (howmuchbytesleft<patternSize) 
      patternSize = howmuchbytesleft;   

    pattern = (void *) malloc(patternSize);
    if (pattern==NULL) {fprintf(stderr,"Error: No memory."); exit(1);};

    int size = patternSize;
    
    memcpy_s(pattern,patternSize,(void *) (offset+(int) processsource.MemBufferStart()),patternSize);
    pattern=pattern;
    
    BYTE *lastsearch = (BYTE *) ((int) processfindhere.MemBufferStart() + processfindhere.MemSize() - patternSize);


    int start = (int) processfindhere.MemBufferStart();



    BYTE *searchOffset = (BYTE *) processfindhere.MemBufferStart();

    while (searchOffset <= lastsearch)
    {//go thru whole processfindhere memory copy and try to find pattern
      if (!memcmp(searchOffset, pattern, patternSize))
      {
        printf("\nOffset: 0x%08x size: %d+%d+%d ",offset+CODE_OFFSET+neighbor_bytes,neighbor_bytes,size_wanted,neighbor_bytes);
        //printfMem(pattern,patternSize);
        

        char output[1000];
        sprintf(output,"  0x%08x:0x%08x\n",CODE_OFFSET+neighbor_bytes+(int) (searchOffset - (BYTE *) processfindhere.MemBufferStart() ),size_wanted);
        out.write(output,strlen(output));
        
        printf(" found at (offset:size): ");
        printf(output);  
        found = 1;
        

      }

      searchOffset = searchOffset + 1;    
    }

  //decrease bound size for potential next try
  neighbor_bytes--;
  printf("|");

  }

   

  
}





int _tmain(int argc, _TCHAR* argv[])
{

  if (argc != 5)
  {
    
    printf("\nUsage: Exesequencefind.exe source destination exe_source exe_findhere\n");
    printf("\n");
    printf("\nFunction: Executes exe_source and exe_findhere - executes .exe for one second, \n\tcopies code segment to buffer. Reads source containing offset:size, \n\tfinds corresponding bytes in exe_source code memory, takes these \n\tbytes with some bytes before and after (bounds), finds this pattern \n\tin exe_findhere code segment memory copy, stores offset:size into \n\tdestination file. If pattern not matched, it is cut on the sides by \n\tone byte and search is repeated.");
    return 1;
  }

  const char *src = argv[1];
  const char *dst = argv[2];
  const char *exefilesource = argv[3];
  const char *exefilefindhere = argv[4];

  int explicitOffset = 0;

  QIFStream in;
  in.open(src);  
  out.open(dst);
      

  ProcessImage processsource,processfindhere;
  if (exefilesource)
  {
    processsource.Load(exefilesource,0);
  //printf("Size: %d",process.Size());
  }

  if (exefilefindhere)
  {
    processfindhere.Load(exefilefindhere,0);
  //printf("Size: %d",process.Size());
  }

  while (!in.eof())
  {
    char buffer[256];
    if (!in.readLine(buffer, 256)) continue;
    int offset, size;
    int read = sscanf(buffer, "  0x%x:0x%x", &offset, &size);
    if (read == 2)
    {
      SequenceFindAndStore(offset - CODE_OFFSET,size,processsource,processfindhere);
    }
  }




  //if (processsource.Loaded()) 
  //{process=process;};
  


/*
  void *buffer;
  void *buffer2;

  void *pattern;
  
  int handle = open(exefilesource,_O_RDONLY|_O_BINARY);  
  int handle2 = open(exefilefindhere,_O_RDONLY|_O_BINARY);
  if ((handle>=0) && (handle2<0))
  {    
   int size =  _filelength(handle);
   int size2 =  _filelength(handle2);

   buffer = (void *) malloc(size);
   buffer2 = (void *) malloc(size2);

    read(handle,buffer,size);
    read(handle2,buffer2,size2);


  //int where = 0x1000;

  //pattern = buffer + 0x1000;




    //lseek(handle,-(long)sizeof(magic),SEEK_CUR);

   //printf ("Filesizes: %d and %d", size, size2);
    printf("%s",buffer);
  }
  else
  {
    fprintf(stderr,"Error: Cannot open input .exe files. ");
  }

*/








  int i=0;


  return 0;
}


