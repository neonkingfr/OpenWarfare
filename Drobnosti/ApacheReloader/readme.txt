This is a linux daemon that allows triggering of reloads of Apache config from within
Apache or other processes that do not have root privileges.
It is written completely in bash.
All components (daemon, starup stcripts) are contained within the script.
It can be integrated in system startup using chkconfig.

INSTALLATION
------------

Copy the script in a convenient location and run it as root.
It will create a default configuration file for you to edit
in the same directory as the script.
You should set the configuration file permissions to allow root access only.

apache_reloader  - man executable
apache_reloader.cfg - default configuration file
apache_reloader.pid - default PID file, contains process ID of running daemon
apache_reloader.log - logfile of the daemon
apache_reloader.pipe - named pipe used to communicate with daemon.

CONFIGURATION
-------------

PIDFILE=/root/apache_reloader/apache_reloader.pid  - name of PID file
PIPE=/root/apache_reloader/apache_reloader.pipe    - name of PIPE
LOGFILE=/root/apache_reloader/apache_reloader.log  - name of logfile
COMMAND_DIE=f238570eb4817b2ac6ee		   - command key to exit the daemon nicely
COMMAND_RELOAD=3bdfe362a5b33c8878c		   - command key to initiate apache config reload


COMMAND_DIE and COMMAND_RELOAD are randomly generated strings for security reasons
to avoid someone controlling the daemon without permission.

You should create the control named pipe using mkfifo command and set permissions on it to allow
apache to write to the pipe. It will be created automatically if it does not exist but
you should check the permissions on it.


RUNNING
-------

Daemon should be started at startup under root by invoking:

# apache_reloader start


To shut down:

# apache_reloader stop

To find out about the config and whether the daemon is running:

# apache_reloader status

In case of emergency you can try to kill the daemon forcibly:

# apache_reloader kill

To test the reload of apache config, you can try:

# apache_reloader invoke


RELOADING APACHE CONFIG FROM PHP
--------------------------------

When daemon is running, reloading apache config is triggered by sending a text line
containing the string specified in COMMAND_RELOAD in the configuration file to the named pipe.
Example:

function reload_apache_config() {
    global $daemon_socket, $daemon_key;
    if (file_exists($daemon_socket)) {
        $pipe = fopen($daemon_socket, 'w'); /* or die('Failed to open socket to reload daemon'); */
        fwrite($pipe,$daemon_key);
        fclose($pipe);
    }
}
