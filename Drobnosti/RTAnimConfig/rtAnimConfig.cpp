#include <El/elementPch.hpp>
#include <El/QStream/QStream.hpp>
#include <El/QStream/QStdStream.hpp>
#include <El/ParamFile/paramFile.hpp>
#include <El/Math/math3D.hpp>
#include <El/Math/mathStore.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Framework/consoleBase.h>

#define RTMMagicLen 8
static const char RTMMagic100[RTMMagicLen+1]="RTM_0100";
static const char RTMMagic101[RTMMagicLen+1]="RTM_0101";

struct AnimPhase: public AutoArray<Matrix4>
{
  RString name;
  float time;
};

TypeIsMovableZeroed(AnimPhase)

/// save transformation as matrix (4x3 floats)
static void SaveMatrix(ParamClassPtr cls, Matrix4Par trans)
{
  {
    ParamEntryPtr array = cls->AddArray("aside");
    array->AddValue(trans.DirectionAside()[0]);
    array->AddValue(trans.DirectionAside()[1]);
    array->AddValue(trans.DirectionAside()[2]);
  }

  {
    ParamEntryPtr array = cls->AddArray("up");
    array->AddValue(trans.DirectionUp()[0]);
    array->AddValue(trans.DirectionUp()[1]);
    array->AddValue(trans.DirectionUp()[2]);
  }

  {
    ParamEntryPtr array = cls->AddArray("dir");
    array->AddValue(trans.Direction()[0]);
    array->AddValue(trans.Direction()[1]);
    array->AddValue(trans.Direction()[2]);
  }
  
  {
    ParamEntryPtr array = cls->AddArray("pos");
    array->AddValue(trans.Position()[0]);
    array->AddValue(trans.Position()[1]);
    array->AddValue(trans.Position()[2]);
  }
}

/// save transformation as quaternion + position
static void SaveQuaternion(ParamClassPtr cls, Matrix4Par trans)
{
  // convert orientation to quaternion
  Quaternion<float> q;
  q.FromMatrixRotationScale(trans.Orientation());

  {
    ParamEntryPtr array = cls->AddArray("orient");
    array->AddValue(q.X());
    array->AddValue(q.Y());
    array->AddValue(q.Z());
    array->AddValue(q.W());
  }

  {
    ParamEntryPtr array = cls->AddArray("pos");
    array->AddValue(trans.Position()[0]);
    array->AddValue(trans.Position()[1]);
    array->AddValue(trans.Position()[2]);
  }
  
}

static void SaveAxisAngleCenter(ParamClassPtr cls, Matrix4Par trans)
{
  // convert orientation to quaternion
  Quaternion<float> q;
  q.FromMatrixRotationScale(trans.Orientation());
  // transform a matrix into the space defined by rotation axis

  // no axis may be defined
  Vector3 axis = VZero;
  q.Normalize();
  float cosAngle = q.W();
  //float angle = acos( cosAngle ) * 2;
  float sinAngle2 = 1.0 - cosAngle * cosAngle;
  if (sinAngle2>1e-3)
  {
    float invSinAngle = InvSqrt( sinAngle2 );
    axis[0] = q.X() * invSinAngle;
    axis[1] = q.Y() * invSinAngle;
    axis[2] = q.Z() * invSinAngle;
    Assert(fabs(axis.SquareSize()-1)<1e-3);
    // we know axis orientation
    // create a vector perpendicular to it
    // both result are in the perpendicular plane
    // one of them is non-zero
    Vector3 v1 = axis.CrossProduct(VUp);
    Vector3 v2 = axis.CrossProduct(VForward);
    Vector3 up = v1 + v2;
    Assert(up.SquareSize()>Square(0.1));
    // axis space - direction along the axis
    Matrix4 axisSpace;
    axisSpace.SetDirectionAndUp(axis,up);
    axisSpace.SetPosition(VZero);
    Matrix4 toAxisSpace = axisSpace.InverseRotation();
    // transformation transformed to axis space
    //Matrix4 tPlanar = axisSpace * trans * toAxisSpace;
    Matrix4 tPlanar = toAxisSpace * trans * axisSpace;
    // we do not care what is happening in z-direction (along the axis)
    // solve 2D problem - find rotation axis
    // set of linear equations
    // transAxisSpace(2x2)*c + transAxisSpace.pos = c
    // t00*cx + t01*cy + t03 = cx
    // t10*cx + t11*cy + t13 = cy
    // ||
    // (t00-1)*cx + t01*cy = -t03
    // t10*cx + (t11-1)*cy = -t13
    // ||
    // ( t00-1  t01   ) | -t03
    // ( t10    t11-1 ) | -t13
    float t00 = tPlanar(0,0), t10 = tPlanar(1,0);
    float t01 = tPlanar(0,1), t11 = tPlanar(1,1);
    float t03 = tPlanar.Position()[0], t13 = tPlanar.Position()[1];
    float det = (t00-1)*(t11-1)-t10*t01;
    float detX = -t03*(t11-1)+t01*t13;
    float detY = -t13*(t00-1)+t10*t03;
    // we know it is rotation
    // there must exist solution, so det is non-zero
    Assert(fabs(det)>1e-8);
    float centerX = detX/det;
    float centerY = detY/det;
    // any point on the axis will do
    Vector3 center = axisSpace * Vector3(centerX,centerY,0);

    // verify centerX,centerY is a real 2D center
    Vector3 checkXY = tPlanar * Vector3(centerX,centerY,0);
    
    // check how much there is a translation along the axis
    float offset = tPlanar.Position().Z();

#if _DEBUG
    Assert (fabs(checkXY.Z()-offset)<1e-3);
    Vector3 center1 = axisSpace * Vector3(centerX,centerY,1);
    Vector3 center2 = axisSpace * Vector3(centerX,centerY,2);
    // result verification
    // check axis is aligned with Z-axis
    Vector3 checkAxis = toAxisSpace * axis;
    Matrix4 rot(MIdentity);
    rot.SetOrientation(trans.Orientation());
    // verify should be the same as trans
    Matrix4 verify = Matrix4(MTranslation,center) * rot * Matrix4(MTranslation,-center);
    Assert(verify.Distance2(trans)<1e-3);
    // no point on the axis should move out of the axis when transformed
    Vector3 centerCheck = trans * center;
    Vector3 centerCheck1 = trans * center1;
    Vector3 centerCheck2 = trans * center2;
    Assert(fabs(centerCheck.Distance(center)-offset)<1e-3);
    Assert(fabs(centerCheck1.Distance(center1)-offset)<1e-3);
    Assert(fabs(centerCheck2.Distance(center2)-offset)<1e-3);
#endif
    // store axis/angle representation
    {
      ParamEntryPtr array = cls->AddArray("axisDir");
      array->AddValue(axis.X());
      array->AddValue(axis.Y());
      array->AddValue(axis.Z());
    }

    {
      ParamEntryPtr array = cls->AddArray("axisPos");
      array->AddValue(center.X());
      array->AddValue(center.Y());
      array->AddValue(center.Z());
    }
    float angle = acos( cosAngle ) * 2;
    cls->Add("angle",angle/(H_PI/180));
    cls->Add("axisOffset",offset);

  }
  else
  {
    // no axis - translation only
    Vector3 axis = trans.Position().Normalized();
    float offset = trans.Position().Size();
    {
      ParamEntryPtr array = cls->AddArray("axisDir");
      array->AddValue(axis[0]);
      array->AddValue(axis[1]);
      array->AddValue(axis[2]);
    }
    {
      ParamEntryPtr array = cls->AddArray("axisPos");
      array->AddValue(0.0f);
      array->AddValue(0.0f);
      array->AddValue(0.0f);
    }
    cls->Add("axisOffset",offset);
    cls->Add("angle",0.0f);
  }

}

struct AnimationDesc
{
  FindArray<RString> _selections;
  AutoArray<AnimPhase> _phases;
  RString sourceName;

  bool LoadHeader(QIStream &in, int &nSel, int &nAnim);
  
  public:
  /// append an animation from a file
  void ScanSelections(const char *src);
  void Append(const char *src);
  void Output(QOStream &stream);
  void ListBones(QOStream &stream);
  
};

bool AnimationDesc::LoadHeader(QIStream &in, int &nSel, int &nAnim)
{
  // process the header

  char magic[RTMMagicLen+1];
  in.read(magic,RTMMagicLen);
  magic[RTMMagicLen]=0;
  bool streaming = true;
  Vector3 step;
  if( !strcmp(magic,RTMMagic100) )
  {
    float stepZ=0;
    in.read((char *)&stepZ,sizeof(stepZ));
    step=Vector3(0,0,stepZ);
    streaming = false;
  }
  else if( !strcmp(magic,RTMMagic101) )
  {
    float stepX=0, stepY=0, stepZ=0;
    in.read((char *)&stepX,sizeof(stepX));
    in.read((char *)&stepY,sizeof(stepY));
    in.read((char *)&stepZ,sizeof(stepZ));
    step=Vector3(stepX,stepY,stepZ);
  }
  else
  {
    return false;
  }
  in.read(&nAnim,sizeof(nAnim));
  in.read(&nSel,sizeof(nSel));
  return true;
}

void AnimationDesc::ScanSelections(const char *src)
{
  // open RTM file
  QIFStream in;
  in.open(src);

  // process the header
  int nSel=0, nAnim=0;
  if (!LoadHeader(in,nSel,nAnim))
  {
    ErrorMessage("Bad animation file format in file '%s'.",src);
    return;
  }
  // scan all selection and update weights as necessary
  for( int i=0; i<nSel; i++ )
  {
    char name[32];
    in.read(name,sizeof(name));
    strlwr(name);
    _selections.AddUnique(name);
  }
}

void AnimationDesc::Append(const char *src)
{
  // open RTM file
  QIFStream in;
  in.open(src);
  
  int nSel=0, nAnim=0;
  if (!LoadHeader(in,nSel,nAnim))
  {
    return;
  }

  AutoArray<int> mapSel;
  mapSel.Resize(nSel);
  
  for( int i=0; i<nSel; i++ )
  {
    char name[32];
    in.read(name,sizeof(name));
    strlwr(name);
    mapSel[i] = _selections.AddUnique(name);
  }
  
  char shortName[256];
  GetFilename(shortName,src);
  shortName[0] = toupper(shortName[0]);
  RString shortNameStr = shortName;
  
  if (sourceName.GetLength()==0)
  {
    sourceName = shortNameStr;
  }
  
  float timeOffset = _phases.Size()>0 ? _phases[_phases.Size()-1].time : 0;
  for (int p=0; p<nAnim; p++)
  {
    float time = 0;
    in.read((char *)&time,sizeof(time));
    // for each phase load all selection names and bone positions
    AnimPhase &phase = _phases.Append();
    phase.time = time + timeOffset;
    phase.name = shortNameStr;
    phase.Resize(_selections.Size());
    for( int i=0; i<_selections.Size(); i++ )
    {
      phase[i] = MIdentity;
    }
    for( int i=0; i<nSel; i++ )
    {
      char name[32];
      Matrix4P transformP;
      in.read(name,sizeof(name));
      // get matrix index from corresponding skeleton

      strlwr(name);
      int selIndex = _selections.Find(name);
      Assert(!strcmp(name,_selections[i]))
      
      in.read((char *)&transformP,sizeof(transformP));
      Matrix4 transform = ConvertToM(transformP);
      if (selIndex<0) continue;
      // analyse non-reversed - it will be reversed during binarization
      if( false )
      {
        const static Matrix4 swapMatrix(MScale,-1,1,-1);
        transform=swapMatrix*transform*swapMatrix;
      }
      phase[i]=transform;
    }
  }
}  

void AnimationDesc::Output(QOStream &stream)
{ 
  // create a ParamFile structure from the animation
  ParamFile out;

  int nAnim = _phases.Size();
  for (int s=0; s<_selections.Size(); s++)
  {
#if 0
    // simple debugging output - output matrices in absolute form
    for (int i=0; i<nAnim; i++)
    {
      Matrix4 trans = _phases[i][s];
      float prevTime = i>0 ? _phases[i-1].time : 0.0f;
      BString<64> name;
      sprintf(name,"Anim%d",i);
      BString<256> animName;

      BString<64> selName;
      sprintf(selName,"%s%d",cc_cast(_phases[i].name),s);
      
      sprintf(animName,"%s_%d",cc_cast(selName),i);
      ParamClassPtr animDesc = out.AddClass(cc_cast(animName));
      animDesc->Add("type","direct");
      animDesc->Add("source",sourceName);
      animDesc->Add("selection",_selections[s]);
      animDesc->Add("minValue",prevTime);
      animDesc->Add("maxValue",_phases[i].time);
      //SaveMatrix(animDesc,relTrans);
      SaveAxisAngleCenter(animDesc,trans);
    }
#else

    Matrix4 lastMatrix = MIdentity;
    // process the loop
    for (int i=0; i<nAnim; i++)
    {
      
      Matrix4 trans = _phases[i][s];
      // if matrix is close to a previous one, there is no need to save it
      float distance = trans.Distance2(lastMatrix);
      if (fabs(distance)<1e-8)
      {
        // skip the matrix, do not extend the time
        continue;
      }
      // calculate relative transform - difference between key frames
      Matrix4 relTrans = trans*lastMatrix.InverseGeneral();
      float prevTime = i>0 ? _phases[i-1].time : 0.0f;
      float relTime = _phases[i].time-prevTime;
      // check how many difference matrices in the chain are the same
      // both space and time needs to be the same
      int countSame = 0;
      Matrix4 predict = trans*relTrans;
      float predictTime = _phases[i].time+relTime;
      for(int ii=i+1;ii<nAnim; ii++)
      {
        Matrix4Val pii = _phases[ii][s];
        float dist2 = predict.Distance2(pii);
        if (dist2>1e-4)
        {
          break;
        }
        if (fabs(predictTime-_phases[ii].time)>0.01f) break;
        predict = relTrans*predict;
        predictTime = predictTime+relTime;
        countSame++;
      }
      if (countSame>0)
      {
        // we want to skip some matrices, but keep time interval
        i += countSame;
        relTrans = _phases[i][s]*lastMatrix.InverseGeneral();
      }
      lastMatrix = _phases[i][s];

      BString<64> name;
      sprintf(name,"Anim%d",i);
      BString<256> animName;

      BString<64> selName;
      sprintf(selName,"%s%d",cc_cast(_phases[i].name),s);
      
      sprintf(animName,"%s_%d",cc_cast(selName),i);
      ParamClassPtr animDesc = out.AddClass(cc_cast(animName));
      animDesc->Add("type","direct");
      animDesc->Add("source",sourceName);
      animDesc->Add("selection",_selections[s]);
      animDesc->Add("minValue",prevTime);
      animDesc->Add("maxValue",_phases[i].time);
      //SaveMatrix(animDesc,relTrans);
      SaveAxisAngleCenter(animDesc,relTrans);
    }
#endif
  }

  out.Save(stream,0,"\n");
}

void AnimationDesc::ListBones(QOStream &stream)
{ 
  // create a ParamFile structure from the animation
  ParamFile out;

  //ParamEntryPtr array = out.AddArray("skeletonBones");
  for (int s=0; s<_selections.Size(); s++)
  {
    RString key = _selections[s];
    BString<1024> animName;
    sprintf(animName,"\"%s\",\"\",\n",cc_cast(key));
    stream << animName;
    
  }

  out.Save(stream,0,"\n");
}

int consoleMain(int argc, const char *argv[])
{
  if (argc<2)
  {
    printf(
      "Usage:\n\n"
      "  RTAnimConfig [options ] <file1> [<file2> ...]\n\n"
      "Multiple animations can be concatenated.\n"
      "Last frame of file1 is assumed to define time 0 for file2\n"
      "Output is directed to stdout.\n\n"
      "Options:\n\n"
      "--bones Generate bone list only\n\n"
      "Example:\n\n"
      "  RTAnimConfig anim1.rtm anim2.rtm >anim.hpp\n"
      "  RTAnimConfig --bones anim1.rtm >bones.hpp\n"
    );
    return 1;
  }
  argv++,argc--;
  enum RTMCommand
  {
    RTMConfig,
    RTMBones
  } command = RTMConfig;
  while (**argv=='-')
  {
    if (!strcmpi(*argv,"--bones"))
    {
      command = RTMBones;
    }
    argv++,argc--;
  }
  AnimationDesc anims;
  for (int i=0; i<argc; i++)
  {
    anims.ScanSelections(argv[i]);
  }
  
  QStdOutStream streamOut;
  switch (command)
  {
    default:
      for (int i=0; i<argc; i++)
      {
        anims.Append(argv[i]);
      }
      anims.Output(streamOut);
      break;
    case RTMBones:
      anims.ListBones(streamOut);
      break;
  }
  #if _DEBUG
    wasError = true;
  #endif
  return 0;
}
