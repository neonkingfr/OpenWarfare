// charset=UTF-8
// ------------------------------------------------
// Knihovna JavaScriptovych funkci pro Docházku
// (C) 2003 Jan Hlavatý, Bohemia Interactive Studio
// ------------------------------------------------


//--------------------------------------------------------
// Zkontroluje případně doopraví čas v textfieldu r.
// Vrací true při chybě ve formátu, false pokud je vše OK.
//--------------------------------------------------------
function kontrolaFormatu(r) {
    var s = r.value;

    // prázdno nevadí...
    if (s.length == 0) return false;
    // speciality:
    // t - aktuální čas
    if (s == 't') {
        var d = new Date();
        var min = d.getMinutes();
        r.value = ""+d.getHours()+":"+(min<10?"0":"")+min;
        return false;
    }
    var pred = 0;
    var za = 0;
    var chyba = false;
    var dvojtecka = false;
    for (var i=0; i<s.length; i++) {
        var z = s.charAt(i);
        if (z == ':') {
            if (dvojtecka) {
                chyba = true;
            } else {
                dvojtecka = true;
            }
        } else {
            if ((z >= '0') && (z<='9')) {
                if (dvojtecka) {
                    za++;
                } else {
                    pred++;
                }
            } else {
                chyba=true;
            }
        }
    }
    if (chyba) return true;

    var hodin, minut;
    if ((pred==4)&&!dvojtecka) {
        // military time
        hodin = parseInt(s.substring(0,2),10);
        minut = parseInt(s.substring(2,4),10);
    } else if (!dvojtecka) {
        // pouze hodiny?
        hodin = parseInt(s,10);
        minut = 0;
    } else {
        // hodiny:minuty
        hodin = parseInt(s.substring(0,pred),10);
        minut = parseInt(s.substring(pred+1),10);
    }
    // zkontrolovat platné rozsahy
    if ((hodin < 0)||(hodin>24)) return true;
    if ((minut < 0)||(minut>59)) return true;
    if ((hodin == 24)&&(minut != 0)) return true;

    // poopravit formát...
    r.value = ""+hodin+":"+(minut<10?"0":"")+minut;
    return false;
}


// -------------------------------------------
// Převod času v hh:mm stringu na počet minut
// -------------------------------------------

function timeToMinutes(s) {
    var split = s.indexOf(':');
    if (split == -1) return NaN;
    var hours = parseInt(s.substring(0,split),10);
    var minutes = parseInt(s.substring(split+1),10);
    if (hours<0) {
        return 60*hours-minutes;
    } else {
        return 60*hours+minutes;
    }
}

// ----------------------------------------------
// Převod počtu minunt na string ve formátu hh:mm
// ----------------------------------------------
function minutesToTime(i) {
    if (i < 0) {
        var hours = Math.floor((-i)/60);
        var minutes = (-i)%60;
        return "-"+hours+":"+(minutes<10?"0":"")+minutes;
    } else {
        var hours = Math.floor(i/60);
        var minutes = i%60;
        return ""+hours+":"+(minutes<10?"0":"")+minutes;
    }
}


/*         
// Handler validace pole s časem.
function valCas(cislo_dne, cislo_intervalu, pocet_intervalu) {
  var ja = document.forms["form"].elements["d"+cislo_dne+"i"+cislo_intervalu+"c"];
  ja.style.borderColor='#FF00FF';
  var err = isCasValid(cislo_dne, cislo_intervalu, pocet_intervalu);
  if (err == null) {
    // formát je v pořádku
    var time_value = timeValue(cislo_dne,cislo_intervalu);
    if (isValidValue(time_value)) {
      // vyplněná hodnota OK
      ja.style.borderColor="#408040";
    } else {
      // nevyplněné hodnota OK
      ja.style.borderColor="#E0E0E0";
    }
    window.status = "Zadání OK";
  } else {                        
    // chybná hodnota
    ja.style.borderColor='#FF4040';
    window.status = err;
  }
}

// validační kontrola pole času.
// Pole je platné pokud platí všechny následující podmínky:
// Formát je v pořádku.
// Je-li vyplněné:
//    Alespoň jeden z okolních typů intervalů není prázdný.
//    Hodnota je nižší než všechny vyplněné hodnoty vpravo od ní.
//    Hodnota je vyšší než všechny vyplněné hodnoty vlevo od ní.
// Není-li vyplněné:
//    Oba okolní typy intervalů jsou prázdné.
// parametry: číslo dne, číslo intervalu, počet intervalů.
// vrací: null pokud je vše OK, jinak String s chybovou hláškou
function isCasValid(cislo_dne, cislo_intervalu, pocet_intervalu) {
  var ja = document.forms["form"].elements["d"+cislo_dne+"i"+cislo_intervalu+"c"];
  // formát je v pořádku.
  if (kontrolaFormatu(ja)) {
    return "Chybný formát času";
  }
  var val = timeValue(cislo_dne,cislo_intervalu);
  if (isValidValue(val)) {
    // vyplněné
    //    Alespoň jeden z okolních typů intervalů není prázdný.
    
    var il = isValidValue(intervalType(cislo_dne,cislo_intervalu-1));
    var ip = isValidValue(intervalType(cislo_dne,cislo_intervalu));
    if (!(il || ip)) {
      return "Není zadán typ intervalu který tento čas ohraničuje";
    }
    // Hodnota je nižší než všechny vyplněné hodnoty vpravo od ní.
    for (i=cislo_intervalu+1;i<pocet_intervalu; i++) {
      var right = timeValue(cislo_dne,i);
      if (isValidValue(right)) {
        if (right > val) {
          break;
        } else {
          return "Čas musí byt nižsí než čas vpravo od něj";
        }
      }
    }
    // Hodnota je vyšší než všechny vyplněné hodnoty vlevo od ní.
    for (i=cislo_intervalu-1; i>=0; i--) {
      var left = timeValue(cislo_dne,i);
      if (isValidValue(left)) {
        if (left < val) {
          break;
        } else {
          return "Čas musí být vyšší než čas vlevo od něj";
        }
      }
    }
  } else {
    // nevyplněné
    var jep = isValidValue(intervalType(cislo_dne,cislo_intervalu));
    var jel = isValidValue(intervalType(cislo_dne,cislo_intervalu-1));
    if (jel && jep) {
       return "Musí být zadán čas přechodu";
    } else if (jep) {
       return "Musí být zadán čas začátku";
    } else if (jel) {
      return "Musí být zadán čas ukončení";
    }
  }           
  // všechno OK
  return null;
}

 
// handler validace selekce typu intervalu. Prováděna validací okolních časů                                                             
function valTyp(cislo_dne, cislo_intervalu, pocet_intervalu) {
  valCas(cislo_dne, cislo_intervalu, pocet_intervalu);
  valCas(cislo_dne, cislo_intervalu+1, pocet_intervalu);
}                                                              
*/
                                                              
// Vrací hodnotu času políčka v minutách
// parametry: číslo dne, číslo políčka
// vrací: počet minut od 0:00, NaN pokud je chybný formát, null pokud není vyplněno
function timeValue(den, cislo) {
  var fieldname = "d"+den+"i"+cislo+"c";
  if (document.forms["form"].elements[fieldname]) {
    var timestring = document.forms["form"].elements[fieldname].value;
    // pokud je nevyplněný, vrátit null
    if (timestring == "") return null;
    return timeToMinutes(timestring);
  } else {
    // neexistuje tento field
    return NaN;
  }
}
/*  
// Vrací vybraný typ intervalu.
function intervalType(den, cislo) {
  var fieldname = "d"+den+"i"+cislo+"t";
  if (document.forms["form"].elements[fieldname]) {
    var val = document.forms["form"].elements[fieldname].value;
    if (val == "") return null;
    return val;
  } else {
    return undefined;
  }
}
  */
      
// vrací true pokud je t normální hodnota (a ne NaN, undefined nebo null)      
function isValidValue(t) {
  if ((typeof t == 'number') && isNaN(t)) return false;
  return t != null;
}

/*
function valPage(dnu,intervalu) {
  var den,i;
  for (den = 1; den <= dnu; den++) {
    for (i=0; i<intervalu; i++) {
      valCas(den,i,intervalu);
    }
  }
}
*/




function handlePageEditLoad() {
  var den = 0;
  while (document.forms["form"].elements["d"+den+"t"]) {
    handleDayChange(den);
    den++;
  }
} 
                                       
                                       
// validační kontrola polí času v řádku
// Pole je platné pokud platí všechny následující podmínky:
// Formát je v pořádku.
// Je-li vyplněné:
//    Alespoň jeden z okolních typů intervalů není prázdný.
//    Hodnota je nižší než všechny vyplněné hodnoty vpravo od ní.
//    Hodnota je vyšší než všechny vyplněné hodnoty vlevo od ní.
// Není-li vyplněné:
//    Oba okolní typy intervalů jsou prázdné.

function handleDayChange(day) {
  var pocet_intervalu = document.forms["form"].elements["intervalu"].value;
  var vlevo = "";
  var minimum_time = NaN;
  
  for (i=0; i<=pocet_intervalu; i++) {
    var cas_ok = true;
    var vpravo = "";
    if (i < pocet_intervalu) {
      vpravo = document.forms["form"].elements["d"+day+"i"+i+"t"].value;
    }
    
    var vl = vlevo != "";
    var vp = vpravo != "";
    
    //alert('vlevo='+vl+' vpravo='+vp);
    
    var cas_chybny = kontrolaFormatu(document.forms["form"].elements["d"+day+"i"+i+"c"]);
    
    if (cas_chybny) {
      cas_ok = false;
    } else {               
        var time_value = timeValue(day,i);
        if (isValidValue(minimum_time)) {
          if (isValidValue(time_value)) {
            if (time_value <= minimum_time) {
              cas_ok = false;
            }
          }
        }
        if (isValidValue(time_value)) {
          minimum_time = time_value;
          
          // vyplněný čas...
          if (!(vl || vp)) {
            cas_ok = false;
          }
          
        } else {
          // nevyplněný čas...
          if (vl || vp) {
            cas_ok = false;
          }
        }
    }
    if (cas_ok) {
      document.forms["form"].elements["d"+day+"i"+i+"c"].style.borderColor = '#E0E0E0';
    } else {
      document.forms["form"].elements["d"+day+"i"+i+"c"].style.borderColor = '#FF0000';
    }
    
    vlevo = vpravo;
  }
  
}

