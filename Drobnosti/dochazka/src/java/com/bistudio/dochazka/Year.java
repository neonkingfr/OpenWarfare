/*
 * Year.java
 *
 * Created on 5. červen 2003, 17:28
 */

package com.bistudio.dochazka;

import java.util.HashMap;

/**
 * Class for holding computed characteristics of given year.
 * @author  hlavac
 */
public class Year {
    
   
    int year_number;
    Day easter_sunday = null;
    Day easter_monday = null;
    Day first_day = null;
    Day last_day = null;
    
    Day first_week_start = null;
    Day last_week_end = null;
    
    int number_of_weeks = 0;
    
    // den nabytí platnosti zákona č. 245/2000 Sb.
    static final Day ZAKON_245_2000 = Day.getDayByDMY(Day.DAY29, Day.CERVEN, 2000);
    static final Day FUTURE = null;
    static final Day PAST = null;
    
    private java.util.HashMap holidays = null;
    
    private static Year _cache = null;
    
    /** Creates a new instance of Year */
    private Year(int year_number) {
        this.year_number = year_number;
    }

    public static Year getYear(int year_number) {
        Year y = _cache;
        if ((y!=null)&&(y.year_number == year_number)) return y;
        y = new Year(year_number);
        _cache = y;
        return y;
    }
    
    public static Year getCurrentYear() {
        return Day.getToday().getYear();
    }
    
    public int getYear() {
        return this.year_number;
    }
    
    public Year previous() {
        return Year.getYear(year_number-1);
    }
    
    public Year next() {
        return Year.getYear(year_number+1);
    }
    
    public Year relativeYear(int offset) {
        return Year.getYear(year_number+offset);
    }
    
    public Day getEasterSunday() {
        if (easter_sunday == null) {
            easter_sunday = Day.getDayByDayNumber(Year.easterSunday(year_number));
        }
        return easter_sunday;
    }
    
    public Day getEasterMonday() {
        if (easter_monday == null) {
            easter_monday = getEasterSunday().nextDay();
        }
        return easter_monday;
    }
    
    public Day getFirstDay() {
        if (first_day == null) {
            first_day = Day.getDayByDMY(Day.DAY1, Day.LEDEN, year_number);
        }
        return first_day;
    }

    public Day getLastDay() {
        if (last_day == null) {
            last_day = Day.getDayByDMY(Day.DAY31, Day.PROSINEC, year_number);
        }
        return last_day;
    }

    
    /* číslo dne velikonoc pro daný rok */
    static int easterSunday(int year) {
        // Zlaté číslo
        int g = (year % 19) + 1;
        // Solární korekce
        int s = ((year - 1600)/100)-((year-1600)/400);
        // Lunární korekce
        int l = (((year-1400)/100)*8)/25;
        
        // p = paškální úplněk jako počet dní po 21. březnu
        int p = ( 303 - (11*g) + s - l ) % 30;
        if ((p == 29)||((p == 28)&&(g>11))) {
            p--;
        }
        
        int moon_day = Day.dayNumber(year, 2, 20) + p;  // číslo dne paškálního úplňku.
        int moon_dow = moon_day % 7;
        if (moon_dow == 6) { // neděle - velikonoce až příští neděli!
            return moon_day + 7;
        }
        // velikonoční neděle je první neděle po úplňku.
        return moon_day + (6-moon_dow);
    }
    
    
    public int hashCode() {
        return this.year_number;
    }
    
    public boolean equals(Object o) {
        if (o == null) return false;
        try {
            Year other = (Year)o;
            return this.year_number == other.year_number;
        } catch (ClassCastException e) {
            return false;
        }
    }
    
    public String toString() {
        return Integer.toString(this.year_number);
    }
    
    public static void main(String[] args) {
        Year tj = Year.getYear(2003);
        System.out.println("Fist week start: "+tj.getFirstWeekStart());
        System.out.println("Last week end: "+tj.getLastWeekEnd());
        System.out.println("Počet týdnů: "+tj.getNumberOfWeeks());
    }
    
    public HashMap getHoliaysMap() {
        if (holidays == null) {
            _buildHolidays();
        }
        return holidays;
    }
    
    private void _buildHolidays() {
        java.util.HashMap h = new java.util.HashMap();
        
        // svátky platné před 29. červnem 2000 nejsou zadány!!
        
        // svátky dle zákona 245/2000, platné od 29. června 2000:
            // státní svátky
            _addHoliday(h, Day.getDayByDMY(Day.DAY1,  Day.LEDEN, year_number), "Den obnovy samostatného českého státu", ZAKON_245_2000, FUTURE);
            _addHoliday(h, Day.getDayByDMY(Day.DAY8,  Day.KVETEN, year_number), "Den osvobození", ZAKON_245_2000, FUTURE);
            _addHoliday(h, Day.getDayByDMY(Day.DAY5,  Day.CERVENEC, year_number), "Den slovanských věrozvěstů Cyrila a Metoděje", ZAKON_245_2000, FUTURE);
            _addHoliday(h, Day.getDayByDMY(Day.DAY6,  Day.CERVENEC, year_number), "Den upálení mistra Jana Husa", ZAKON_245_2000, FUTURE);
            _addHoliday(h, Day.getDayByDMY(Day.DAY28, Day.ZARI, year_number), "Den české státnosti", ZAKON_245_2000, FUTURE);
            _addHoliday(h, Day.getDayByDMY(Day.DAY28, Day.RIJEN, year_number), "Den vzniku samostatného československého státu", ZAKON_245_2000, FUTURE);
            _addHoliday(h, Day.getDayByDMY(Day.DAY17, Day.LISTOPAD, year_number), "Den boje za svobodu a demokracii", ZAKON_245_2000, FUTURE);
            // ostatní svátky
            _addHoliday(h, Day.getDayByDMY(Day.DAY1, Day.LEDEN, year_number), "Nový rok", ZAKON_245_2000, FUTURE);
            _addHoliday(h, getEasterMonday(), "Velikonoční pondělí", ZAKON_245_2000, FUTURE);
            _addHoliday(h, Day.getDayByDMY(Day.DAY1, Day.KVETEN, year_number), "Svátek práce", ZAKON_245_2000, FUTURE);
            _addHoliday(h, Day.getDayByDMY(Day.DAY24, Day.PROSINEC, year_number), "Štědrý den", ZAKON_245_2000, FUTURE);
            _addHoliday(h, Day.getDayByDMY(Day.DAY25, Day.PROSINEC, year_number), "1. svátek vánoční", ZAKON_245_2000, FUTURE);
            _addHoliday(h, Day.getDayByDMY(Day.DAY26, Day.PROSINEC, year_number), "2. svátek vánoční", ZAKON_245_2000, FUTURE);
        holidays = h;
    }
    
    private void _addHoliday(java.util.HashMap h, Day day, String name, Day valid_from, Day valid_to) {
        String[] existing = (String[])h.get(day);
        if (existing==null) {
            String[] newone = new String[1];
            newone[0] = name;
            h.put(day,newone);
        } else {
            String[] newone = new String[existing.length+1];
            System.arraycopy(existing, 0, newone, 0, existing.length);
            newone[existing.length] = name;
            h.put(day,newone);
        }
    }
    
    
    public Day getFirstWeekStart() {
        if (first_week_start == null) {
            Day firstday = getFirstDay();
            int fd_dow = firstday.getDayOfWeek();
            if (fd_dow > 3) fd_dow -= 7;
            if (fd_dow != 0) {
                first_week_start = firstday.relativeDay(-fd_dow);
            } else {
                first_week_start = firstday;
            }
        }
        return first_week_start;
    }
    
    public Day getLastWeekEnd() {
        if (last_week_end == null) {
            Day lastday = getLastDay();
            int ld_dow = lastday.getDayOfWeek();
            if (ld_dow < 3) ld_dow += 7;
            if (ld_dow != 0) {
                last_week_end = lastday.relativeDay(6-ld_dow);
            } else {
                last_week_end = lastday;
            }
        }
        return last_week_end;
    }
    
    public int getNumberOfWeeks() {
        if (number_of_weeks == 0) {
            number_of_weeks = (getFirstWeekStart().diff(getLastWeekEnd())+1)/7;
        }
        return number_of_weeks;
    }
    
    
}
