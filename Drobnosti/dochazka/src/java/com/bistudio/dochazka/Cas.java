/*
 * Cas.java
 *
 * Created on 9. září 2003, 11:38
 */

package com.bistudio.dochazka;

import java.io.Serializable;

/**
 * Třída reprezentující čas s rozlišením minuty.
 * @author  hlavac
 */
public class Cas implements Serializable, Comparable {
    
    private int minuty = 0; // počet minut od začátku dne
    
    /** Creates a new instance of Cas */
    public Cas() {
    }
    
    public Cas(Cas copy) {
        if (copy != null) {
            this.minuty = copy.minuty;
        } else {
            this.minuty = 0;
        }
    }
    
    public Cas(int minut) {
        setTimeMinutes(minut);
    }
    
    public Cas(String s) {
        setTime(s);
    }
    
    public void setTimeMinutes(int minutes) {
        minuty = minutes;
    }
    
    public int getTimeMinutes() {
        return minuty;
    }

    /** Set time in the hours:minutes string format */
    public void setTime(String s) {
        try {
            int split = s.indexOf(':');
            if (split == -1) {
                // only hours
                int h = Integer.parseInt(s);
                minuty = h*60;
            } else {
                // hours:minutes
                int h = Integer.parseInt(s.substring(0,split));
                int m = Integer.parseInt(s.substring(split+1));
                if (s.charAt(0)=='-' || h<0 ) {
                    minuty = 60*h-m;
                } else {
                    minuty = 60*h+m;
                }
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Bad time format: "+s);
        }
    }

    public String toString() {
        StringBuffer sb;
        if (minuty >= 0) { 
            sb = new StringBuffer(6);
            int hours = minuty/60;
            int mins  = Math.abs(minuty) % 60;
            sb.append(hours);
            sb.append(':');
            if (mins < 10) {
                sb.append('0');
            }
            sb.append(mins);
        } else {
            sb = new StringBuffer(7);
            sb.append('-');
            int hours = -minuty/60;
            int mins  = -minuty % 60;
            sb.append(hours);
            sb.append(':');
            if (mins < 10) {
                sb.append('0');
            }
            sb.append(mins);
        }
        return sb.toString();
    }

    public boolean equals(Object o) {
        if (o == null) return false;
        try {
            Cas other = (Cas)o;
            return this.minuty==other.minuty;
        } catch (ClassCastException e) {
            return false;
        }
    }
    
    public int compareTo(Object o) {
        Cas other = (Cas)o;
        return this.minuty - other.minuty;
    }
    
    public int diff(Cas other) {
        if (other != null) {
            return other.minuty - this.minuty;
        } else {
            return 0;
        }
    }

    public void add(Cas c) {
        if (c != null) {
            this.minuty += c.minuty;
        }
    }
    
    public void sub(Cas c) {
        if (c != null) {
            this.minuty -= c.minuty;
        }
    }
    
    public static Cas add(Cas c1, Cas c2) {
        Cas c = new Cas(c1);
        c.add(c2);
        return c;
    }
    
    public static Cas sub(Cas c1, Cas c2) {
        Cas c = new Cas(c1);
        c.sub(c2);
        return c;
    }
    
    public static void main(String[] args) {
        Cas c = new Cas("17:50");
        Cas c2 = new Cas("23:35");
        System.out.println(c.diff(c2));
        System.out.println(new Cas("0").diff(new Cas("24")));
        Cas c3 = new Cas("-15:30");
        c3.add(c);
        System.out.println(c3);
        
        System.out.println(new Cas("-0:07").zaokrouhli(15));
        System.out.println(new Cas("-1:08").zaokrouhli(15));
        System.out.println(new Cas("0:07").zaokrouhli(15));
        System.out.println(new Cas("1:08").zaokrouhli(15));
    }
    
    public static Cas instance(String str) {
        if ((str != null)&&(str.length()>0)) {
            return new Cas(str);
        } else {
            return null;
        }
    }
    
    /** Zaokrouhlí na nejbližší násobek počtu minut */
    public Cas zaokrouhli(int na_nasobky_minut) {
        boolean minus = (minuty < 0);
        int work = minuty;
        if (minus) {
            work = -work;
        }
        int rest = work % na_nasobky_minut;
        if (rest != 0) {
            work -= rest;
            if (rest > (na_nasobky_minut / 2)) {
                work += na_nasobky_minut;
            }
        }
        if (minus) {
            minuty = -work;
        } else {
            minuty = work;
        }
        return this;
    }
    
    public static Cas min(Cas c1, Cas c2) {
        if (c1.minuty > c2.minuty) {
            return c2;
        } else {
            return c1;
        }
    }

    public static Cas max(Cas c1, Cas c2) {
        if (c1.minuty > c2.minuty) {
            return c1;
        } else {
            return c2;
        }
    }
}
