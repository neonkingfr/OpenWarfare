/*
 * UhrnMesice.java
 *
 * Created on 6. říjen 2003, 14:56
 */

package com.bistudio.dochazka;

/**
 *
 * @author  hlavac
 */
public class UhrnMesice {
    
    boolean platny = true;
    public Cas dovolena    = new Cas(); // délka dovolené
    public Cas uvazek      = new Cas(); // délka úvazku
    public Cas odpracovano = new Cas(); // odpracováno ve všední dny
    public Cas odpracovano_vikend = new Cas(); // odpracováno o víkendech
    public Cas nemoc       = new Cas(); // nemoc
    public Cas placene_volno = new Cas(); // hodiny placeneho volna
    public Cas neplacene_volno = new Cas(); // hodiny neplaceneho volna
    public Cas bilance     = new Cas(); // dovolená + odpracováno + placene volno + nemoc - úvazek + bonus
    public Cas bonus    = new Cas(); // délka bonusu
    public int puldnu_prace = 0;
    public int puldnu_prace_vikendy = 0;
    public int puldnu_dovolene = 0;
    public int puldnu_volna = 0;
    public int puldnu_nemoci = 0;
    public int puldnu_placeneho_volna = 0;
    public int pocet_svatku = 0;
    public int pocet_vikendu = 0;
    public int puldnu_neplaceneho_volna = 0;
    public int puldnu_bonusu = 0;
    public SeznamTerminu terminy_dovolene = new SeznamTerminu(1,31);
    public SeznamTerminu terminy_nemoci = new SeznamTerminu(1,31);
    public SeznamTerminu terminy_neplacene_volno = new SeznamTerminu(1,31);
    public SeznamTerminu terminy_bonus = new SeznamTerminu(1,31);

    public void pridejTerminDovolene(int den) {
        terminy_dovolene.yes(den);
    }
    public void pridejTerminNemoci(int den) {
        terminy_nemoci.yes(den);
    }
    public void pridejTerminNeplaceneVolno(int den) {
        terminy_neplacene_volno.yes(den);
    }
    public void pridejTerminBonus(int den) {
        terminy_bonus.yes(den);
    }
    
    public void pridejPraci(Cas delka) {
        if (delka == null) {
            neplatny();
        } else {
            this.odpracovano.add(delka);
            this.bilance.add(delka);
        }
    }

    public void pridejPraciVikend(Cas delka) {
        if (delka == null) {
            neplatny();
        } else {
            this.odpracovano_vikend.add(delka);
            this.bilance.add(delka);
        }
    }

    public void pridejDovolenou(Cas delka) {
        if (delka == null) {
            neplatny();
        } else {
            this.dovolena.add(delka);
            this.bilance.add(delka);
        }
    }
    
    public void pridejBonus(Cas delka) {
        if (delka == null) {
            neplatny();
        } else {
            this.bonus.add(delka);
            this.bilance.add(delka);
        }
    }

    public void pridejPuldnyPrace(int puldnu) {
        puldnu_prace += puldnu;
    }

    public void pridejPuldnyPraceVikend(int puldnu) {
        puldnu_prace_vikendy += puldnu;
    }
    
    public void pridejPuldnyDovolene(int puldnu) {
        puldnu_dovolene += puldnu;
    }
    
    public void pridejPuldnyBonus(int puldnu) {
        puldnu_bonusu += puldnu;
    }

    public void pridejPuldnyNemoci(int puldnu) {
        puldnu_nemoci += puldnu;
    }
    
    public void pridejPuldnyVolna(int puldnu) {
        puldnu_volna += puldnu;
    }

    public void pridejPuldnyPlacenehoVolna(int puldnu) {
        puldnu_placeneho_volna += puldnu;
    }

    public void pridejPuldnyNeplacenehoVolna(int puldnu) {
        puldnu_neplaceneho_volna += puldnu;
    }

    public void pridejSvatek() {
        pocet_svatku += 1;
    }
    public void pridejVikend() {
        pocet_vikendu += 1;
    }
    
    public void pridejUvazek(Cas delka) {
        if (delka == null) {
            neplatny();
        } else {
            this.uvazek.add(delka);
            this.bilance.sub(delka);
        }
    }

    public void pridejNemoc(Cas delka) {
        if (delka == null) {
            neplatny();
        } else {
            this.nemoc.add(delka);
            this.bilance.add(delka);
        }
    }
    
    public void pridejPlaceneVolno(Cas delka) {
        if (delka == null) {
            neplatny();
        } else {
            this.placene_volno.add(delka);
            this.bilance.add(delka);
        }
    }

    public void pridejNeplaceneVolno(Cas delka) {
        if (delka == null) {
            neplatny();
        } else {
            this.neplacene_volno.add(delka);
            this.bilance.add(delka);
        }
    }

    public void neplatny() {
        this.platny = false;
        //Exception e = new Exception("Kuk");
        //e.printStackTrace(System.out);
    }
    
    public boolean isValid() {
        return this.platny;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("={ dovolena=");
        sb.append(dovolena);
        sb.append(", bonus=");
        sb.append(bonus);
        sb.append(", uvazek=");
        sb.append(uvazek);
        sb.append(", odpracovano=");
        sb.append(odpracovano);
        sb.append(", odpracovano_vikend=");
        sb.append(odpracovano_vikend);
        sb.append(", placene_volno=");
        sb.append(placene_volno);
        sb.append(", neplacene_volno=");
        sb.append(neplacene_volno);
        sb.append(", nemoc=");
        sb.append(nemoc);
        sb.append(", bilance=");
        sb.append(bilance);
        sb.append(", puldnu_prace=");
        sb.append(puldnu_prace);
        sb.append(", puldnu_prace_vikendy=");
        sb.append(puldnu_prace_vikendy);
        sb.append(", puldnu_dovolene=");
        sb.append(puldnu_dovolene);
        sb.append(", puldnu_bonusu=");
        sb.append(puldnu_bonusu);
        sb.append(", puldnu_nemoci=");
        sb.append(puldnu_nemoci);
        sb.append(", puldnu_volna=");
        sb.append(puldnu_volna);
        sb.append(", puldnu_neplaceneho_volna=");
        sb.append(puldnu_neplaceneho_volna);
        sb.append(", pocet_svatku=");
        sb.append(pocet_svatku);
        sb.append(", pocet_vikendu=");
        sb.append(pocet_vikendu);
        sb.append(" }");
        return sb.toString();
    }
    
}
