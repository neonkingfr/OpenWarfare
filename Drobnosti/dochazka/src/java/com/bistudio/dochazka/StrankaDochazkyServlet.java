/*
 * StrankaDochazky.java
 *
 * Created on 1. červenec 2003, 14:46
 */

package com.bistudio.dochazka;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author  hlavac
 * @version
 */
public class StrankaDochazkyServlet extends ServletBase {

    File userdir = null;
    java.io.File smbpasswd = null;
    DatabaseDochazka database = null;
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            String userdir_path = config.getInitParameter("userdir");
            String smbpasswd_file = config.getInitParameter("smbpasswd");
            userdir = new File(userdir_path);
            smbpasswd = new File(smbpasswd_file);
            if (!userdir.isDirectory()) throw new UnavailableException("userdir error");
            database = new DatabaseDochazka(userdir, smbpasswd);
        } catch (IOException e) {
            throw new ServletException(e);
        }
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            Parameters param = new Parameters(request,response);
            if (param.akce == null) {
                akceDefault(param);
            } else {
                if ("EditaceDochazky".equals(param.akce)) {
                    akceEditaceStranky(param);
                } else if ("Tisk".equals(param.akce)) {
                    akceTisk(param);
                } else if ("EditaceUzivatele".equals(param.akce)) {
                    akceEditaceUzivatele(param);
                } else {
                    log("WARNING: Ilegalni akce "+param.akce+" login="+param.user.getLogin());
                    akceKill(param);
                }
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        Parameters param = new Parameters(request,response);
        if ("UlozeniDochazky".equals(param.akce)) {
            akceUlozeniStranky(param);
        } else if ("UlozeniUzivatele".equals(param.akce)) {
            akceUlozeniUzivatele(param);
        } else {
            response.sendError(response.SC_METHOD_NOT_ALLOWED);
        }
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Zobrazuje stránku docházky a umožnuje editaci";
    }
    
    
    private class Parameters {
        int mesic;
        int rok;
        boolean spatny_mesic = false;
        
        int dalsi_mesic,dalsi_rok,predchozi_mesic,predchozi_rok;
        
        String mesic_value;
        String rok_value;
        Day dnes;
        UserRecordV1 user;
        String akce;
        String login;
        
        Day[] dny;
        String akce_url;
        String akce_url_tento;
        String akce_url_dalsi;
        String akce_url_predchozi;
        
        HttpServletRequest request;
        HttpServletResponse response;
        
        int pracovnich_dni;
        int vikendovych_dni;
        int svatky_dni;
        
        
        public Parameters(HttpServletRequest request, HttpServletResponse response) throws ServletException {
            try {
                this.request = request;
                this.response = response;
                request.setCharacterEncoding("UTF-8");
                String username = (String)request.getAttribute("smbpasswd.username");
                user = database.loadUser(username);
                dnes =  Day.getToday();
                mesic_value = request.getParameter("mesic");
                rok_value = request.getParameter("rok");
                if (rok_value != null) {
                    rok = Integer.parseInt(rok_value);
                }
                if (mesic_value != null) {
                    mesic = Integer.parseInt(mesic_value);
                }
                akce = request.getParameter("akce");
                if ((mesic_value == null)||(mesic_value == null)||(rok<2002)||(mesic<1)||(mesic>12)) {
                    rok = dnes.getYearNumber();
                    mesic = dnes.getMonth()+1;
                    spatny_mesic = true;
                }
                
                if (mesic == 12) {
                    dalsi_mesic = 1;
                    dalsi_rok = rok+1;
                } else {
                    dalsi_mesic = mesic+1;
                    dalsi_rok = rok;
                }
                
                if (mesic == 1) {
                    predchozi_mesic = 12;
                    predchozi_rok = rok-1;
                } else {
                    predchozi_mesic = mesic-1;
                    predchozi_rok = rok;
                }

                akce_url = request.getContextPath()+request.getServletPath();
                akce_url_tento = request.getServletPath()+"?rok="+rok+"&mesic="+mesic;
                akce_url_dalsi = request.getServletPath()+"?rok="+dalsi_rok+"&mesic="+dalsi_mesic;
                akce_url_predchozi = request.getServletPath()+"?rok="+predchozi_rok+"&mesic="+predchozi_mesic;
                
                dny = Day.getWholeMonth(mesic-1,rok);
                
                pracovnich_dni = 0;
                svatky_dni = 0;
                vikendovych_dni = 0;
                for (int i=0; i<dny.length; i++) {
                    if (dny[i].isWorkDay()) {
                        pracovnich_dni++;
                    } else if (dny[i].getDayOfWeek() < 5) {
                        svatky_dni++;
                    } else {
                        vikendovych_dni++;
                    }
                }

                login = request.getParameter("login");
                if (login == null) {
                    login = user.getLogin();
                } else {
                    login = login.toLowerCase();
                }
                
            /*} catch (ServletException e) {
                throw e;*/
            } catch (Exception e) {
                throw new ServletException("Exception: "+e.toString(),e);
            }
        }
        
    }

    /*
    void akcePresmerovani(Parameters param) throws ServletException {
        try {
            param.response.sendRedirect(param.akce_url);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
*/    
    void akceEditaceUzivatele(Parameters param) throws ServletException {
        try {
            
            // ověřit práva na editaci nastavení uživatele...
            if (!(param.user.getLogin().equals(param.login))) {
                // pokud login <> aktivní login, ověřit práva...
                if (!(
                    param.user.hasRole("dochazka.admin") ||
                    param.user.hasRole("dochazka.edituser.*") ||
                    param.user.hasRole("dochazka.edituser."+param.login)
                    )) {
                    // nemá právo.
                    log("WARNING: Pokus o editaci uživatele "+param.login+" uživatelem "+param.user.getLogin());
                    akceKill(param);
                    return;
                }
            }
            
            UserRecordV1 editovany_uzivatel = database.loadUser(param.login);
            if (editovany_uzivatel == null) {
                log("WARNING: Pokus o editaci neexistujícího uživatele "+param.login+" uživatelem "+param.user.getLogin());
                akceKill(param);
                return;
            }
            
            TempByteHolder outdata = new TempByteHolder(8192);
            OutputStream out = outdata.getOutputStream();
            PrintWriter o = new PrintWriter(new OutputStreamWriter(out,"UTF-8"));

            o.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            o.println("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><link rel='stylesheet' type='text/css' href='style.css'>");
            o.println("<title>Docházka - editace uživatele "+html(param.login)+"</title>");
            o.println("</head><body>");
            o.println("<h1>Editace vlastností uživatele "+html(editovany_uzivatel.getLogin())+"</h1>");
            o.println("<form method='POST' action='"+html(param.akce_url)+"'>");
            o.println("<input type=hidden name='akce' value='UlozeniUzivatele'>");
            o.println("<input type=hidden name='login' value='"+html(param.login)+"'>");
            o.println("<table class='vlastnosti'>");
            o.println("<tr><td class='titulek'>Jméno:</td><td><input name='jmeno' type='text' size='60' maxlenghth='200' value='"+html(editovany_uzivatel.getRealName())+"'></td></tr>");
            o.println("<tr><td colspan='2' class='cudliky'><input type='submit' value='Uložit změny'></td></tr>");
            o.println("</table>");
            o.println("</form>");
            o.println("</body></html>");
            o.close();
            param.response.setContentType("text/html; charset=UTF-8");
            OutputStream response_out = param.response.getOutputStream();
            outdata.writeTo(response_out);
            response_out.close();
            outdata.clear();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
    
    void akceUlozeniUzivatele(Parameters param) throws ServletException {
        try {
            UserRecordV1 editovany_uzivatel = database.loadUser(param.login);
            if (editovany_uzivatel == null) {
                log("WARNING: Pokus o editaci neexistujícího uživatele "+param.login+" uživatelem "+param.user.getLogin());
                akceKill(param);
                return;
            }
            if (!(param.user.getLogin().equals(param.login))) {
                // jiný uživatel, ověřit
                if (!(
                    param.user.hasRole("dochazka.admin") ||
                    param.user.hasRole("dochazka.edituser.*") ||
                    param.user.hasRole("dochazka.edituser."+param.login)
                    )) {
                    // nemá právo.
                    log("WARNING: Pokus o editaci uživatele "+param.login+" uživatelem "+param.user.getLogin());
                    akceKill(param);
                    return;
                }
            }
            
            String jmeno = param.request.getParameter("jmeno");
            // tady kontrolovat!
            editovany_uzivatel.setRealName(jmeno);
            database.saveUser(editovany_uzivatel);
            param.response.sendRedirect(param.akce_url);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    
    
    void akceTisk(Parameters param) throws ServletException {
        try {
            UserRecordV1 editovany_uzivatel = database.loadUser(param.login);
            if (editovany_uzivatel == null) {
                 log("WARNING: Pokus uzivatele "+param.user.getLogin()+" o tisk neexistujiciho uzivatele "+param.login);
                 akceKill(param);
                 return;
            }
            if (!(editovany_uzivatel.getLogin().equals(param.user.getLogin()))) {
                // pokud editujeme jiného než přihlášeného uživatele,
                // musí na to mít právo.
                if ( (!param.user.hasRole("dochazka.admin"))&&(!param.user.hasRole("dochazka.tisk.*"))&&(!param.user.hasRole("dochazka.tisk."+editovany_uzivatel.getLogin() ))) {
                     log("WARNING: Pokus uzivatele "+param.user.getLogin()+" o tisk uzivatele "+param.login);
                     akceKill(param);
                     return;
                }
            }

            StrankaDochazky stranka = new StrankaDochazky();

            String filename = dochazkaXMLFilename(userdir.toString(), param.login, param.rok, param.mesic);
            FileInputStream str = null;
            try {
                str = new FileInputStream(filename);
                try {
                    stranka.loadFromXML(str);
                } finally {
                    str.close();
                }
            } catch (FileNotFoundException e) {
                // pokud neexistuje dokument není co tisknout
                stranka = null;
            } 
            

            TempByteHolder outdata = new TempByteHolder(8192);
            OutputStream out = outdata.getOutputStream();
            PrintWriter o = new PrintWriter(new OutputStreamWriter(out,"UTF-8"));

            if (stranka == null) {
                // není co tisknout...
                o.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
                o.println("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><link rel='stylesheet' type='text/css' href='style.css'>");
                o.println("<title>Docházka - "+html(editovany_uzivatel.getRealName())+"</title>");
                o.println("</head><body>");
                o.println("<h1>Není co tisknout</h1>");
                o.println("<p>Data docházky za tento měsíc ještě nebyla uložena.</p>");
                o.println("</body></html>");
            } else {
                // zkontrolovat validitu stránky pro tisk, vypočítat hodnoty...
                UhrnMesice uhrn = stranka.getSummary();
                if (uhrn.isValid()) {

                    o.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
                    o.println("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><link rel='stylesheet' type='text/css' href='style.css'>");
                    o.println("<title>Docházka - "+html(editovany_uzivatel.getRealName())+"</title>");
                    o.println("</head><body class='tisk'>");


    /*                if (!uhrn.isValid()) {
                        o.println("<div style='color: red;'>Data docházky nejsou ještě kompletní nebo obsahují chyby.</div>");
                    }
    */                
                    o.println("<h1>Docházka - "+stranka.getYear()+"/"+stranka.getMonth()+" "+html(editovany_uzivatel.getRealName())+"</h1>");
                    // přehled...
                    o.println("<table class='tisk-tabulka'>");

                    o.println("<tr class='titulek'><th colspan=2>Den</th><th>Typ</th><th>Odpracováno</th><th>Celkem</th>");

                    StrankaDochazky.Radek[] dny = stranka.getDays();
                    for (int den=0; den < dny.length; den++) {
                        Day d = dny[den].getDay();
                        if (d.isWorkDay()) {
                            o.println("<tr class='pracovni'>");
                        } else {
                            o.println("<tr class='svatek'>");
                        }
                        o.println("<td class='den'>"+d.getDayOfWeekShortName()+"</td><td class='cislo-dne'>"+(den+1)+".</td>");

                        o.println("<td class='typ'>"+html(dny[den].getDayType().getDescription())+"</td>");

                        o.println("<td class='seznam'>");

                        boolean carka = false;
                        for (int i=0; i<dny[den].getNumberOfIntervals(); i++) {
                            Interval in = dny[den].getInterval(i);
                            String typ = in.getType().getDescription();
                            if (typ.length() > 0) {
                                if (carka) {
                                    o.print(", ");
                                } else {
                                    carka = true;
                                }
                                o.print(html(typ));
                                o.print(" ");
                                o.print(in.getStart());
                                o.print(" - ");
                                o.print(in.getEnd());
                            }
                        }
                        o.print("</td><td class='celkem'>");
                        o.print(dny[den].getWorkDuration());
                        o.print("</td>");

                        /*
                        {
                            UhrnMesice u = new UhrnMesice();
                            dny[den].zapocti(u);
                            o.print("<td>"+u+"</td>");
                        }*/
                        o.println("</tr>");
                    }
                    o.println("</table>");

                    o.println("<table class='tisk-suma'>");

                    Cas celkem_prace = Cas.add(uhrn.odpracovano,uhrn.odpracovano_vikend).zaokrouhli(15);
                    Cas celkem_prace_vikend = new Cas(uhrn.odpracovano.getTimeMinutes()+(3*uhrn.odpracovano_vikend.getTimeMinutes()/2)).zaokrouhli(15);
                    Cas celkem_volna = new Cas((uhrn.puldnu_volna+uhrn.puldnu_placeneho_volna)*8*30);
                    Cas celkem_neplaceneho_volna = new Cas(uhrn.neplacene_volno);
                    Cas celkem_bonusu = new Cas(uhrn.puldnu_bonusu*8*30);
                    Cas kontrolni_soucet_casu1 = new Cas(celkem_prace);
                    kontrolni_soucet_casu1.add(uhrn.dovolena);
                    kontrolni_soucet_casu1.add(uhrn.nemoc);
                    kontrolni_soucet_casu1.add(celkem_volna);
                    kontrolni_soucet_casu1.add(celkem_neplaceneho_volna);
                    kontrolni_soucet_casu1.add(celkem_bonusu);
                    Cas kontrolni_soucet_casu2 = new Cas(celkem_prace_vikend);
                    kontrolni_soucet_casu2.add(uhrn.dovolena);
                    kontrolni_soucet_casu2.add(uhrn.nemoc);
                    kontrolni_soucet_casu2.add(celkem_volna);
                    kontrolni_soucet_casu2.add(celkem_neplaceneho_volna);
                    kontrolni_soucet_casu2.add(celkem_bonusu);

                    o.println("<tr><td class='titulek'>Celkem práce dní:</td><td class='dny'>"+(uhrn.puldnu_prace*0.5f)+"</td><td class='titulek'>Celkem odpracováno:</td><td class='hodiny'>"+celkem_prace+"</td><td class='hodiny'>"+celkem_prace_vikend+"</td></tr>");
                    o.println("<tr><td class='titulek'>Celkem dovolená dní:</td><td class='dny'>"+(uhrn.puldnu_dovolene*0.5f)+"</td><td class='titulek'>Celkem dovolená hodin:</td><td class='hodiny'>"+uhrn.dovolena+"</td><td class='hodiny'>"+uhrn.dovolena+"</td></tr>");
                    o.println("<tr><td class='titulek'>Celkem nemoc dní:</td><td class='dny'>"+(uhrn.puldnu_nemoci*0.5f)+"</td><td class='titulek'>Celkem nemoc hodin:</td><td class='hodiny'>"+uhrn.nemoc+"</td><td class='hodiny'>"+uhrn.nemoc+"</td></tr>");
                    o.println("<tr><td class='titulek'>Celkem náhradní a placené volno dní:</td><td class='dny'>"+((uhrn.puldnu_volna+uhrn.puldnu_placeneho_volna)*0.5f)+"</td><td class='titulek'>Celkem volno hodin:</td><td class='hodiny'>"+celkem_volna+"</td><td class='hodiny'>"+celkem_volna+"</td></tr>");
                    o.println("<tr><td class='titulek'>Celkem neplacené volno dní:</td><td class='dny'>"+(uhrn.puldnu_neplaceneho_volna*0.5f)+"</td><td class='titulek'>Celkem neplacené volno hodin:</td><td class='hodiny'>"+celkem_neplaceneho_volna+"</td><td class='hodiny'>"+celkem_neplaceneho_volna+"</td></tr>");
                    o.println("<tr><td class='titulek'>Celkem bonus dní:</td><td class='dny'>"+(uhrn.puldnu_bonusu*0.5f)+"</td><td class='titulek'>Celkem bonus hodin:</td><td class='hodiny'>"+celkem_bonusu+"</td><td class='hodiny'>"+celkem_bonusu+"</td></tr>");
                    o.println("<tr><td class='titulek'>Kontrolní součet:</td><td class='dny'>"+((uhrn.puldnu_prace+uhrn.puldnu_dovolene+uhrn.puldnu_bonusu+uhrn.puldnu_nemoci+uhrn.puldnu_volna+uhrn.puldnu_placeneho_volna+uhrn.puldnu_neplaceneho_volna)*0.5f)+"</td><td class='titulek'></td><td class='hodiny'>"+kontrolni_soucet_casu1+"</td><td class='hodiny'>"+kontrolni_soucet_casu2+"</td></tr>");
                    o.println("<tr><td class='titulek'>Svátků:</td><td class='dny'>"+uhrn.pocet_svatku+"</td><td class='titulek'></td></tr>");
                    o.println("</table>");

                    o.println("<hr>");
                    // cedulka
                    o.println("<div style='page-break-before: always'>");
                    o.println("<div style='margin-top: 3cm; margin-bottom: 1cm; text-align: center; font-size: 18pt; font-weight: bold;'>Evidence odpracované doby</div>");
                    o.println("<table class='tisk-cedulka'>");
                    o.println("<tr>");
                    o.println("<th>Měsíc</th><th>Jméno</th><th>Odpracované hodiny</th><th>Odpracované dny</th><th>hodiny-důvod</th><th>Termíny</th>");
                    o.println("</tr>");

                    Cas uvazek_na_den = new Cas("8:00");
                    Cas cas_prace_celkem = Cas.add(Cas.add(uhrn.odpracovano,uhrn.odpracovano_vikend),uhrn.placene_volno).zaokrouhli(15);
                    Cas planovany_cas = new Cas(uvazek_na_den.getTimeMinutes()*(uhrn.puldnu_prace + uhrn.puldnu_volna)/2);
                    Cas maximalni_prescasy = new Cas("12:30");
                    Cas cas_prace_max = Cas.add(planovany_cas,maximalni_prescasy);
                    Cas placeny_cas = Cas.min(cas_prace_celkem, cas_prace_max);
                    Cas placeny_cas_bez_prescasu = Cas.min(cas_prace_celkem, planovany_cas);
                    Cas placene_prescasy = Cas.sub(placeny_cas,placeny_cas_bez_prescasu);
                    int planovanych_puldni = uhrn.puldnu_prace; //+uhrn.puldnu_volna+uhrn.puldnu_placeneho_volna;

                    o.println("<tr><td rowspan=3>"+stranka.getMonth()+"/"+stranka.getYear()+"</td><td>"+html(stranka.getName())+"</td><td class='hodiny'>"+placeny_cas+"</td><td class='dny'>"+(planovanych_puldni*0.5f)+"</td><td class='titulek'>Odpracováno</td><td>xxxxxxxxx</td>");
                    o.println("<tr><td class='titulek'>Hodiny bez přesčasů:</td><td class='hodiny'>"+placeny_cas_bez_prescasu+"</td><td class='dny'>"+(uhrn.puldnu_dovolene*0.5f)+"</td><td class='titulek'>Dovolená</td><td class='terminy'>"+uhrn.terminy_dovolene+"</td></tr>");
                    o.println("<tr><td class='titulek'>Přesčasové hodiny:</td><td class='hodiny'>"+placene_prescasy+"</td><td class='dny'>"+(uhrn.puldnu_nemoci*0.5f)+"</td><td class='titulek'>Nemoc</td><td class='terminy'>"+uhrn.terminy_nemoci+"</td></tr>");
                    o.println("<tr><td></td><td></td><td></td><td class='dny'>"+(uhrn.puldnu_placeneho_volna*0.5f)+"</td><td class='titulek'>Placené volno</td><td></td>");
                    o.println("<tr><td></td><td></td><td></td><td class='dny'>"+(uhrn.puldnu_neplaceneho_volna*0.5f)+"</td><td class='titulek'>Neplacené volno</td><td></td>");
                    o.println("<tr><td></td><td></td><td></td><td class='dny'>"+(uhrn.puldnu_bonusu*0.5f)+"</td><td class='titulek'>Bonus</td><td>"+uhrn.terminy_bonus+"</td>");
                    o.println("</table>");
                    o.println("<table style='width: 100%; border: none'>");
                    o.println("<tr><td style='width: 30%; text-align: center'>Jméno a podpis vedoucího</td><td style='width: 40%;'></td><td style='width: 30%; text-align: center;'>Jméno a podpis pracovníka</td></tr>");
                    o.println("<tr><td style='width: 30%; text-align: center'></td><td style='width: 40%;'></td><td style='width: 30%; text-align: center; font-weight: bold; font-size: 12pt;'>"+html(stranka.getName())+"</td></tr>");
                    o.println("<tr><td style='width: 30%; text-align: center; border-bottom: 1px dotted black;'>&nbsp;<br>&nbsp;<br>&nbsp;<br>&nbsp;</td><td style='width: 40%;'></td><td style='width: 30%; text-align: center; border-bottom: 1px dotted black;'>&nbsp;</td></tr>");
                    o.println("</table>");
                    o.println("</div>");
                    o.println("</body></html>");
                } else {
                    // data jsou nekompletní...
                    o.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
                    o.println("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><link rel='stylesheet' type='text/css' href='style.css'>");
                    o.println("<title>Docházka - "+html(param.user.getRealName())+"</title>");
                    o.println("</head><body>");
                    o.println("<h1>Není co tisknout</h1>");
                    o.println("<p>Data docházky za tento měsíc nejsou ještě kompletní nebo obsahují chyby.</p>");
                    o.println("</body></html>");
                }
            }
            o.close();
            param.response.setContentType("text/html; charset=UTF-8");
            param.response.setHeader("Cache-Control", "no-cache");
            OutputStream response_out = param.response.getOutputStream();
            outdata.writeTo(response_out);
            response_out.close();
            outdata.clear();
        } catch (IOException e) {
            throw new ServletException(e);
        }
    }
    
    
    // Homepage uživatele. Odkazy na věci které může dělat.
    void akceDefault(Parameters param) throws ServletException {
        try {
            TempByteHolder outdata = new TempByteHolder(8192);
            OutputStream out = outdata.getOutputStream();
            PrintWriter o = new PrintWriter(new OutputStreamWriter(out,"UTF-8"));

            UserRecordV1 editovany_uzivatel = database.loadUser(param.login);
            if (!(param.user.getLogin().equals(editovany_uzivatel.getLogin()))) {
                // stránka někoho jiného...
                log("DEBUG: editace cizí stránky");
                if (!(
                    param.user.hasRole("dochazka.admin")||
                    param.user.hasRole("dochazka.home.*")||
                    param.user.hasRole("dochazka.home."+editovany_uzivatel.getLogin())
                    )) {
                        // nemá právo. Narušení bezpečnostních opatření!
                        log("WARNING: Neoprávněný pokus "+param.user.getLogin()+" o přístup k homepage "+editovany_uzivatel.getLogin()+" "+param.rok+"/"+param.mesic);
                        akceKill(param);
                        return;
                }
            } else {
                log("DEBUG: editace vlastní stránky povolena");
            }
            
            
            o.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            o.println("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><link rel='stylesheet' type='text/css' href='style.css'>");
            o.println("<title>Docházka - "+editovany_uzivatel.getRealName()+"</title>");
            //o.println("<script language='JavaScript' type='text/javascript' src='dochazka.js'></script>");
            o.println("</head><body>");
            o.println("<div class='top-bar'>");
            o.print("Přihlášený uživatel: <span class='login'>");
            o.print(html(param.user.getLogin()));
            o.print("</span> <span class='name'>(");
            o.print(html(param.user.getRealName()));
            o.println(")</span>");
            if (!(param.user.getLogin().equals(editovany_uzivatel.getLogin()))) {
                o.print("Zobrazený uživatel: <span class='login'>");
                o.print(html(editovany_uzivatel.getLogin()));
                o.print("</span> <span class='name'>(");
                o.print(html(editovany_uzivatel.getRealName()));
                o.println(")</span>");
            }
            o.println("</div>");
            
            o.print("Dnes je ");
            o.print(param.dnes.toString());
            
            o.print("<div class='nabidka'>");
            
            int tento_rok = param.dnes.getYearNumber();
            int tento_mesic = param.dnes.getMonth()+1;
            int minuly_mesic, minuly_rok;
            if (tento_mesic == 1) {
                minuly_mesic = 12;
                minuly_rok = tento_rok-1;
            } else {
                minuly_mesic = tento_mesic-1;
                minuly_rok = tento_rok;
            }
            
            o.println("<a href='"+html(param.akce_url+"?akce=EditaceUzivatele&login="+editovany_uzivatel.getLogin())+"'>Editace uživatelských nastavení</a><br>");
            
            o.print("Editace docházky: <a href='"+html(param.akce_url+"?akce=EditaceDochazky&rok="+tento_rok+"&mesic="+tento_mesic+"&login="+editovany_uzivatel.getLogin())+"'>Tento měsíc</a>, ");
            o.print("<a href='"+html(param.akce_url+"?akce=EditaceDochazky&rok="+minuly_rok+"&mesic="+minuly_mesic+"&login="+editovany_uzivatel.getLogin())+"'>Minulý měsíc</a>");
            o.print("<br>Tisk: <a href='"+html(param.akce_url+"?akce=Tisk&rok="+tento_rok+"&mesic="+tento_mesic+"&login="+editovany_uzivatel.getLogin())+"'>Tento měsíc</a>, ");
            o.print("<a href='"+html(param.akce_url+"?akce=Tisk&rok="+minuly_rok+"&mesic="+minuly_mesic+"&login="+editovany_uzivatel.getLogin())+"'>Minulý měsíc</a>");
            o.println("</div>");
            o.println("</body></html>");
            o.close();
            param.response.setContentType("text/html; charset=UTF-8");
            OutputStream response_out = param.response.getOutputStream();
            outdata.writeTo(response_out);
            response_out.close();
            outdata.clear();
        } catch (IOException e) {
            throw new ServletException(e);
        }
    }
    
    
    void akceEditaceStranky(Parameters param) throws ServletException {
        try {
            
            UserRecordV1 editovany_uzivatel = database.loadUser(param.login);
            if (editovany_uzivatel == null) {
                 log("WARNING: Pokus uzivatele "+param.user.getLogin()+" o editaci neexistujiciho uzivatele "+param.login);
                 akceKill(param);
                 return;
            }
            if (!(editovany_uzivatel.getLogin().equals(param.user.getLogin()))) {
                // pokud editujeme jiného než přihlášeného uživatele,
                // musí na to mít právo.
                if ( (!param.user.hasRole("dochazka.admin"))&&(!param.user.hasRole("dochazka.edit.*"))&&(!param.user.hasRole("dochazka.edit."+editovany_uzivatel.getLogin() ))) {
                     log("WARNING: Pokus uzivatele "+param.user.getLogin()+" o editaci uzivatele "+param.login);
                     akceKill(param);
                     return;
                }
            }
            
            StrankaDochazky stranka = new StrankaDochazky();
            
            String filename = dochazkaXMLFilename(userdir.toString(), param.login, param.rok, param.mesic);
            FileInputStream str = null;
            try {
                str = new FileInputStream(filename);
                try {
                    stranka.loadFromXML(str);
                } finally {
                    str.close();
                }
            } catch (FileNotFoundException e) {
                // pokud neexistuje dokument, založit nový
                log("Zakládám nový dokument docházky: "+param.user.getLogin()+" pro "+param.login+" "+param.rok+"/"+param.mesic);
                stranka.loadEmpty(param.login, editovany_uzivatel.getRealName(), param.rok, param.mesic-1, 8);
            } 

            
            TempByteHolder outdata = new TempByteHolder(8192);
            OutputStream out = outdata.getOutputStream();
            PrintWriter o = new PrintWriter(new OutputStreamWriter(out,"UTF-8"));
        
            o.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            o.println("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><link rel='stylesheet' type='text/css' href='style.css'>");
            o.println("<title>Editace docházky - "+html(param.login)+" "+param.rok+"/"+param.mesic+"</title>");
            o.println("<script language='JavaScript' type='text/javascript' src='dochazka.js'></script>");
            o.println("</head><body onload='handlePageEditLoad()'>");
            
            o.println("<form id='form' name='form' method='POST' action='"+param.akce_url+"'>");
            o.println("<input type='hidden' name='akce' value='UlozeniDochazky'>");
            o.println("<input type='hidden' name='login' value='"+html(param.login)+"'>");
            o.println("<input type='hidden' name='rok' value='"+param.rok+"'>");
            o.println("<input type='hidden' name='mesic' value='"+param.mesic+"'>");

            stranka.saveToForm(o);
            
            o.println("<input type='submit' name='submit' value='Uložit změny'>");
            o.println("<input type='submit' value='Zrušit změny'>");
            o.println("</form>");
            o.println("</body></html>");
            o.close();
            param.response.setContentType("text/html; charset=UTF-8");
            OutputStream response_out = param.response.getOutputStream();
            outdata.writeTo(response_out);
            response_out.close();
            outdata.clear();
        } catch (IOException e) {
            throw new ServletException(e);
        }
    }

    void akceUlozeniStranky(Parameters param) throws ServletException {
        try {

            // opravdu jde o uložení?
            if (param.request.getParameter("submit")==null) {
                param.response.sendRedirect(param.akce_url);
                return;
            }
            
            // zpracovat data....
            
            StrankaDochazky sd = new StrankaDochazky();
            sd.loadFromForm(param.request);
            
            // ověřit oprávnění k uložení změn pro tento login...
            if (!(param.user.getLogin().equals(sd.getLogin()))) {
                // stránka někoho jiného...
                log("DEBUG: editace cizí stránky");
                if (!(
                    param.user.hasRole("dochazka.admin")||
                    param.user.hasRole("dochazka.edit.*")||
                    param.user.hasRole("dochazka.edit."+sd.getLogin())
                    )) {
                        // nemá právo. Narušení bezpečnostních opatření!
                        log("WARNING: Neoprávněný pokus "+param.user.getLogin()+" o uložení dat "+sd.getLogin()+" "+param.rok+"/"+param.mesic);
                        akceKill(param);
                        return;
                }
            } else {
                log("DEBUG: editace vlastní stránky povolena");
            }
            
            // Zápis dat na disk
            String filename = dochazkaXMLFilename(userdir.toString(), sd.getLogin(), sd.getYear(), sd.getMonth());
            FileOutputStream fos;
            try {
                 fos = new FileOutputStream(filename);
                 try {
                     sd.saveToXML(fos);
                 } finally {
                     if (fos != null) {
                        fos.close();
                     }
                 }
            } catch (FileNotFoundException e) {
                throw new ServletException("Unable to save data",e);
            }
            
            TempByteHolder outdata = new TempByteHolder(8192);
            OutputStream out = outdata.getOutputStream();
            PrintWriter o = new PrintWriter(new OutputStreamWriter(out,"UTF-8"));
        
            o.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            o.println("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><link rel='stylesheet' type='text/css' href='style.css'>");
            o.println("<title>Změny uloženy</title>");
            o.println("<script language='JavaScript' type='text/javascript' src='dochazka.js'></script>");
            o.println("</head><body>");
            
            o.println("Změny uloženy. Zpět na <a href='"+param.akce_url+"?akce=EditaceDochazky&rok="+param.rok+"&mesic="+param.mesic);
            if (!param.login.equals(param.user.getLogin())) {
                o.println("&login="+html(param.login));
            }
            o.println("'>editaci</a>, ");
            o.println("<a href='"+param.akce_url+"?akce=Tisk&rok="+param.rok+"&mesic="+param.mesic);
            if (!param.login.equals(param.user.getLogin())) {
                o.println("&login="+html(param.login));
            }
            o.println("'>tisk</a> nebo na <a href='"+param.akce_url+"'>hlavní stránku</a>.");
            
            // tady možná přidat informaci o platnosti uložené stránky?
            
            o.println("</body></html>");
            o.close();
            param.response.setContentType("text/html; charset=UTF-8");
            OutputStream response_out = param.response.getOutputStream();
            outdata.writeTo(response_out);
            response_out.close();
            outdata.clear();
            
        } catch (IOException e) {
            throw new ServletException(e);
        }
    }
    
    
    void akceKill(Parameters param) throws ServletException {
        try {
            TempByteHolder outdata = new TempByteHolder(8192);
            OutputStream out = outdata.getOutputStream();
            PrintWriter o = new PrintWriter(new OutputStreamWriter(out,"UTF-8"));
        
            o.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            o.println("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><link rel='stylesheet' type='text/css' href='style.css'>");
            o.println("<title>Přístup odepřen</title>");
            o.println("<script language='JavaScript' type='text/javascript' src='dochazka.js'></script>");
            o.println("</head><body>");
            
            o.println("Milý narušiteli bezpečnostních opatření, zůstaňte laskavě kde jste, popravčí komando je už na cestě ;)");
            
            o.println("</body></html>");
            o.close();
            param.response.setContentType("text/html; charset=UTF-8");
            OutputStream response_out = param.response.getOutputStream();
            outdata.writeTo(response_out);
            response_out.close();
            outdata.clear();
        } catch (IOException e) {
            throw new ServletException(e);
        }
    }
    
    /*
    void akceDefault(Parameters param) throws ServletException {
        try {
            int pocet_intervalu = 8;
            TempByteHolder outdata = new TempByteHolder(8192);
            OutputStream out = outdata.getOutputStream();
            PrintWriter o = new PrintWriter(new OutputStreamWriter(out,"UTF-8"));

            o.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
            o.println("<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><link rel='stylesheet' type='text/css' href='style.css'>");
            o.println("<title>Docházka</title>");
            o.println("<script language='JavaScript' type='text/javascript' src='dochazka.js'></script>");
            o.println("</head><body onload='valPage("+param.dny.length+","+pocet_intervalu+")'>");

            o.println("<form name='form' method='POST' action='"+param.akce_url+"&akce=form_submit'>");
            
            o.println("<div class='dnes'>Dnes je "+param.dnes.getDayOfWeekName()+", "+(param.dnes.getDay()+1)+". "+param.dnes.getMonthName2()+" "+param.dnes.getYearNumber()+"</div>");
            o.println("<div class='username'><a href='"+param.akce_url+"&akce=editace_uzivatele'>"+html(param.user.getLogin())+"</a></div>");
            o.println("<div class='realname'>"+html(param.user.getRealName())+"</div>");

            o.println("<a onclick='runDebug(); return true'>Klik</a>");

            // nadpis měsíce
            o.println("<div class='nadpis_mesic'>");
            
            o.println("<a href='"+param.akce_url_predchozi+"'><img src='img/sipl.png' width='50' height='35' alt='Předchozí měsíc' border='0'></a>");
            
            o.println(html(param.dny[0].getMonthName()));
            o.println(" ");
            o.println(param.dny[0].getYearNumber());

            o.println("<a href='"+param.akce_url_dalsi+"'><img src='img/sipr.png' width='50' height='35' alt='Následující měsíc' border='0'></a>");
            o.println("</div>");

            o.println("<p>Pracovních dní: "+param.pracovnich_dni+" Víkendových dní: "+param.vikendovych_dni+" Svátků: "+param.svatky_dni+"</p>");
            // celý měsíc...

            
            o.println("<table class='dochazka'>");
            // hlavička docházky
            o.println("<tr><th>den</th><th>debug info</th></tr>");
            
            // řádky docházky
            for (int i=0; i<param.dny.length; i++) {
                int cislo_dne = i+1;
                
                o.print("<tr class='radek-dochazky'>");
                // den
                o.print("<td class='");
                if (param.dny[i].isWorkDay()) {
                    // pracovní den
                    o.print("pracovni");
                } else {
                    // svátek
                    o.print("svatek");
                }
                o.print("'>");
                o.print(cislo_dne);
                o.print(". ");
                o.print("</td>");
            
                //o.print("<td>"+param.dny[i].toString()+"</td>");
                
                
                // výběr globálního typu dne, d?t
                o.print("<td><select name='d"+cislo_dne+"t'>");
                o.print("<option value='1' selected='selected'>1: přítomen</option>");
                o.print("<option value='D'>D: dovolená</option>");
                o.print("<option value='d'>d: 1/2 dov.</option>");
                o.print("<option value='N'>N: nemoc</option>");
                o.print("<option value='n'>n: náhr. volno</option>");
                o.print("<option value='V'>V: nepl. volno</option>");
                o.print("<option value='-'>-: neplatí</option>");
                o.println("</select></td>");
                
                
                for (int interval=0; interval<pocet_intervalu; interval++) {
                    o.print("<td><input class='cas' type='text' maxlength='5' name='d"+cislo_dne+"i"+interval+"c' value='' onfocus='valCas("+cislo_dne+","+interval+","+pocet_intervalu+"); select()' onchange='valCas("+cislo_dne+","+interval+","+pocet_intervalu+");'></td>");
                    // typ intervalu
                    if (interval != (pocet_intervalu-1)) {
                        String sel = "";
                        if ((interval == 0)||(interval==2)) {
                            sel = "P";
                        }
                        
                        // prázdno
                        o.print("<td><select name='d"+cislo_dne+"i"+interval+"t' onchange='valTyp("+cislo_dne+","+interval+","+pocet_intervalu+")'>");
                        o.print("<option value=''");
                        if ("".equals(sel)) {
                            o.print(" selected='selected'");
                        }
                        o.print("></option>");
                        
                        // práce
                        o.print("<option value='P' style='background-color: #D0FFD0'");
                        if ("P".equals(sel)) {
                            o.print(" selected='selected'");
                        }
                        o.print(">P: práce</option>");
                        
                        // lékař
                        o.print("<option value='L' style='background-color: #FFD0D0'");
                        if ("L".equals(sel)) {
                            o.print(" selected='selected'");
                        }
                        o.print(">L: lékař</option>");
                        
                        // externí práce
                        o.print("<option value='X' style='background-color: #D0FFFF'");
                        if ("X".equals(sel)) {
                            o.print(" selected='selected'");
                        }
                        o.print(">X: ext. práce</option>");
                        o.println("</select></td>");
                    }
                }
                
                o.println("</tr>");
            }
            o.println("</table>");

            o.println("<input type='submit' name='debug' value='DEBUG'>");
            o.println("</form>");
            
            o.println("</body></html>");


            o.close();
            param.response.setContentType("text/html; charset=UTF-8");
            OutputStream response_out = param.response.getOutputStream();
            outdata.writeTo(response_out);
            response_out.close();
            outdata.clear();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
    */
    
    void dumpParameters(Parameters param) {
        log("------- parameter dump -------");
        Enumeration e = param.request.getParameterNames();

        SortedSet s = new TreeSet();
        
        while (e.hasMoreElements()) {
            s.add(e.nextElement());
        }
        
        Iterator i = s.iterator();
        while (i.hasNext()) {
            String name = (String)i.next();
            String[] values = param.request.getParameterValues(name);
            if (values != null) {
                log("\""+name+"\" = \""+values[0]+"\"");
                for (int j=1; j<values.length; j++) {
                    log(" ... \""+values[j]+"\"");
                }
            } else {
                log("\""+name+"\" = null");
            }
        }
        log("------- end of parameter dump -------");
    }
    
    String dochazkaXMLFilename(String directory, String login, int rok, int mesic) {
        StringBuffer sb = new StringBuffer(50);
        sb.append(directory);
        sb.append(File.separatorChar);
        sb.append("dochazka_");
        sb.append(login);
        sb.append('_');
        sb.append(rok);
        if (mesic < 10) {
            sb.append('0');
        }
        sb.append(mesic);
        sb.append(".xml");
        return sb.toString();
    }
    
/*    String padZeros(int i, int places) {
        if (i<0) throw new IllegalArgumentException("Negative number");
        StringBuffer sb = new StringBuffer(places);
        sb.append(i);
        int zbyva = places-sb.length();
        if (zbyva > 0) {
            sb.insert(0, "0000000000".substring(0,zbyva));
        }
        return sb.toString();
    }
 */
}
