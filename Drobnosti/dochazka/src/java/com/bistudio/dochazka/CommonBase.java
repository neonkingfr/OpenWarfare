/*
 * CommonBase.java
 *
 * Created on 1. září 2003, 17:16
 */

package com.bistudio.dochazka;

/**
 *
 * @author  hlavac
 */
public abstract class CommonBase {
    
    public static final String HTML_DOCTYPE = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">";
    
    /** Creates a new instance of CommonBase */
    private CommonBase() {
    }
    
    public static String htmlSafe(String source) {
        if (source == null) return null;
        StringBuffer sb = new StringBuffer(source.length()+20);
        for (int i=0; i<source.length(); i++) {
            char c = source.charAt(i);
            switch (c) {
                case 160:
                    sb.append("&nbsp;");
                case '<':
                    sb.append("&lt;");
                    break;
                case '>':
                    sb.append("&gt;");
                    break;
                case '&':
                    sb.append("&amp;");
                    break;
                case '\'':
                    sb.append("&#39;");
                    break;
                case '\"':
                    sb.append("&#34;");
                    break;
                default:
                    sb.append(c);
            }
        }
        return sb.toString();
    }
    
    public static String xmlSafe(String source) {
        if (source == null) return null;
        StringBuffer sb = new StringBuffer(source.length()+20);
        for (int i=0; i<source.length(); i++) {
            char c = source.charAt(i);
            switch (c) {
                case '<':
                    sb.append("&lt;");
                    break;
                case '>':
                    sb.append("&gt;");
                    break;
                case '&':
                    sb.append("&amp;");
                    break;
                case '\'':
                    sb.append("&apos;");
                    break;
                case '\"':
                    sb.append("&quot;");
                    break;
                default:
                    sb.append(c);
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(htmlSafe("Příšero! <ahoj> & vole! \"Uhu'ju\""));
    }
}
