/*
 * SmbpasswdFile.java
 *
 * Created on 27. srpen 2003, 17:08
 */

package com.bistudio.dochazka;

import java.io.*;
import java.security.MessageDigest;

/**
 *
 * @author  hlavac
 */
public class SmbpasswdFile {

    File smbpasswd = null;
    
    private static final String hex_numbers = "0123456789ABCDEF";
    /** Creates a new instance of SmbpasswdFile */
    public SmbpasswdFile(File file) throws IOException {
        smbpasswd = file;
        if (!file.canRead()) {
            throw new IOException("Smbpasswd file not found: "+file.getPath());
        }
    }
    
    /** NT password hash is hex string representation of MD5 of password in UTF-16-LE */
    protected static String createNTPasswordHash(String password) throws java.security.NoSuchAlgorithmException {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream(password.length()*2);
            OutputStreamWriter osw = new OutputStreamWriter(bos,"UnicodeLittleUnmarked");
            osw.write(password);
            osw.flush();
            osw.close();
            osw = null;
            byte[] unicode_bytes = bos.toByteArray();
            //MessageDigest md5 = MessageDigest.getInstance("MD4");
            MD4Digest md4 = new MD4Digest();
            md4.update(unicode_bytes, 0, unicode_bytes.length);
            byte[] digest = new byte[md4.getDigestSize()];
            md4.doFinal(digest, 0);
            return hex(digest);
        } catch (UnsupportedEncodingException e) {
        } catch (IOException e) {
        }
        return null;
    }
    
    protected static String hex(byte[] bytes) {
        if (bytes == null) return null;
        StringBuilder sb = new StringBuilder(bytes.length*2);
        for (int i=0; i<bytes.length; i++) {
            int b = bytes[i]&0xFF;
            sb.append(hex_numbers.charAt((b>>4)&0x0f));
            sb.append(hex_numbers.charAt(b&0x0f));
        }
        return sb.toString();
    }
    
    public static void main(String[] args) {
        try {

            SmbpasswdFile f = new SmbpasswdFile(new File("smbpasswd"));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Ověřit existenci uživatelského jména a případně ověřit heslo.
     * @param username login name
     * @param password heslo nebo null pro ověření pouze username
     * @return true pokud je username (a případně heslo) v pořádku, false pokud není.
     */
    public boolean checkPassword(String username, String password) throws IOException, java.security.NoSuchAlgorithmException {
        boolean result = false;
        int retries = 10;
        FileInputStream fis = null;
        while (fis == null) {
            try {
                fis = new FileInputStream(smbpasswd);
            } catch (IOException e) {
                retries--;
                if (retries>0) {
                    try {
                        Thread.currentThread().sleep(1000);
                    } catch (InterruptedException f) {
                    }
                } else {
                    throw e;
                }
            }
        }
        try {
        
            String looking_for = username.toLowerCase();

            InputStreamReader isr = new InputStreamReader(new BufferedInputStream(fis),"US-ASCII");
            BufferedReader b = new BufferedReader(isr, 8192);

            String line = b.readLine();
            while (line != null) {
                //System.out.println(line);
                String uname = field(line,0).toLowerCase();
                if (uname.equals(looking_for)) {
                    if (password == null) {
                        result = true;
                    } else {
                        String nt_hash = field(line,3);
                        if (nt_hash.equals(createNTPasswordHash(password))) {
                            result = true;
                        }
                    }
                    line = null;
                } else {
                    line = b.readLine();
                }
            }
            b.close();
        } finally {
            fis.close();
            fis = null;
        }
        return result;
    }
    
    protected static String field(String data, int index) {
        int start = 0;
        int end = data.length();
        
        if (index != 0) {
             start = nthIndexOf(data, ':', index-1)+1;
        }
        end = data.indexOf(':',start+1);
        if (end == -1) end = data.length();
        return data.substring(start,end);
    }
    
    protected static int nthIndexOf(String source, char delim, int index) {
        int found = 0;
        for (int i=0; (i<=index) && (found != -1); i++) {
            found = source.indexOf(delim,found+1);
        }
        return found;
    }
}
