/*
 * TestServlet.java
 *
 * Created on 17. září 2003, 14:33
 */

package com.bistudio.dochazka;

import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
 * @author  hlavac
 * @version
 */
public class TestServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>TestServlet</title>");
        out.println("</head>");
        out.println("<body><table>");
        out.println("<tr><th>Request URI:</th><td>"+html(request.getRequestURI())+"</td></tr>");
        out.println("<tr><th>Context path:</th><td>"+html(request.getContextPath())+"</td></tr>");
        out.println("<tr><th>Servlet path:</th><td>"+html(request.getServletPath())+"</td></tr>");
        out.println("<tr><th>Path info:</th><td>"+html(request.getPathInfo())+"</td></tr>");
        out.println("<tr><th>Query string:</th><td>"+html(request.getQueryString())+"</td></tr>");
        out.println("</table></body>");
        out.println("</html>");
        out.close();
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
    private String html(String source) {
        return CommonBase.htmlSafe(source);
    }
}
