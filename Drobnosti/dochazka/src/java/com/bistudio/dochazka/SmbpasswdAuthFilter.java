/*
 * SmbpasswdAuthFilter.java
 *
 * Created on 1. září 2003, 16:15
 */

package com.bistudio.dochazka;

import java.io.File;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author  hlavac
 * @version
 */

public class SmbpasswdAuthFilter extends FilterBase {

    SmbpasswdFile smbpasswd = null;
    String realm_name = null;
    String attribute_name = null;
    
    @Override
    public void destroy() {
    }
    
    @Override
    public void doFilter(javax.servlet.ServletRequest servletRequest, javax.servlet.ServletResponse servletResponse, javax.servlet.FilterChain filterChain) throws java.io.IOException, javax.servlet.ServletException {
        try {
            // check for presence of basic auth
            BasicAuthenticationInfo auth = new BasicAuthenticationInfo(servletRequest);
            if ((auth.getUsername() == null)||(auth.getPassword() == null)||(!smbpasswd.checkPassword(auth.getUsername(),auth.getPassword()))) {
                // request authentication.
                if (servletResponse instanceof javax.servlet.http.HttpServletResponse) {
                    javax.servlet.http.HttpServletResponse response = (javax.servlet.http.HttpServletResponse) servletResponse;
                    response.setHeader("WWW-Authenticate", "Basic realm=\""+realm_name+"\"");
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
                } else {
                    throw new ServletException("Unknown reponse type, unable to request authentication");
                }
            } else {
                // normal processing. Set attributes
                servletRequest.setAttribute(attribute_name, auth.getUsername().toLowerCase());
                filterChain.doFilter(servletRequest,servletResponse);
            }
        } catch (java.security.NoSuchAlgorithmException e) {
            throw new UnavailableException("No MD4 algorithm provider installed, cannot function");
        }
    }
    
    @Override
    public void init(javax.servlet.FilterConfig filterConfig) throws javax.servlet.ServletException {
        super.init(filterConfig);
        
        try {
            smbpasswd = new SmbpasswdFile( new File(getRequiredInitParameterString("smbpasswd")) );
        } catch (Exception e) {
            throw new UnavailableException("Error configuring smbpasswd file");
        }
        
        realm_name = getRequiredInitParameterString("realm_name");
        attribute_name = getRequiredInitParameterString("attribute_name");
    }
    
}
