/*
 * FilterBase.java
 *
 * Created on 1. září 2003, 17:03
 */

package com.bistudio.dochazka;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;

/**
 *
 * @author  hlavac
 */
public abstract class FilterBase implements javax.servlet.Filter {
    
    protected javax.servlet.FilterConfig config;
    
    /** Creates a new instance of FilterBase */
    protected FilterBase() {
    }
    
    public void destroy() {
    };
    
    public void doFilter(javax.servlet.ServletRequest servletRequest, javax.servlet.ServletResponse servletResponse, javax.servlet.FilterChain filterChain) throws java.io.IOException, javax.servlet.ServletException {
        filterChain.doFilter(servletRequest, servletResponse);
    }
    
    public void init(javax.servlet.FilterConfig filterConfig) throws javax.servlet.ServletException {
        config = filterConfig;
    }
    
    protected String getRequiredInitParameterString(String paramname) throws ServletException {
        String result = config.getInitParameter(paramname);
        if (result == null) throw new UnavailableException("Missing init parameter: "+paramname);
        return result;
    }
    
}
