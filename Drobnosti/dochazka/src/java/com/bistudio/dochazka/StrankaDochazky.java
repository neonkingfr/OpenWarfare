/*
 * StrankaDochazky.java
 *
 * Created on 25. září 2003, 16:09
 */

package com.bistudio.dochazka;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import javax.servlet.ServletException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 *
 * @author  hlavac
 */
public class StrankaDochazky implements Serializable {

    int rok;
    int mesic;
    String login = null;
    String name = null;
    int pocet_intervalu = 8;
    UhrnMesice uhrn = null;
    Radek[] radky = null;

    
    // řádek představující jeden den v měsíci.
    public static class Radek {
        Day            den;
        TypDne         typ_dne;
        TypIntervalu[] typy;
        Cas[]          casy; // časů je vždy o jeden více než intervalů!
        
        public Radek(Day den, int pocet_intervalu) {
            this.den = den;
            typy = new TypIntervalu[pocet_intervalu];
            casy = new Cas[pocet_intervalu+1];
            TypDne pracovni_den = TypDne.getDayType("1");
            // předvyplnit typ dne
            if (den.isWorkDay()) {
                typ_dne = pracovni_den;
            } else {
                if (den.getDayOfWeek() < Day.SOBOTA) {
                    // svátek
                    typ_dne = TypDne.getDayType("S");
                } else {
                    // víkend
                    typ_dne = TypDne.getDayType("s");
                }
            }

            // předvyplnit typy intervalů 
            TypIntervalu prazdno = TypIntervalu.getIntervalType("");
            TypIntervalu prace = TypIntervalu.getIntervalType("P");
            for (int i=0; i<pocet_intervalu; i++) {
                if (((i==0)||(i==2))&&(typ_dne==pracovni_den)) {
                    typy[i] = prace;
                } else {
                    typy[i] = prazdno;
                }
            }
        }
        
        public TypDne getDayType() {
            return typ_dne;
        }
        
        public Interval getInterval(int index) {
            return new Interval( casy[index], casy[index+1], typy[index] );
        }
        
        public int getNumberOfIntervals() {
            return typy.length;
        }
        
        public void setNumberOfIntervals(int numb) {
            if (numb < 1) throw new IllegalArgumentException();
            if (typy.length != numb) {
                TypIntervalu[] nove_typy = new TypIntervalu[numb];
                Cas[] nove_casy = new Cas[numb+1];
                if (typy.length < numb) {
                    // prodloužení
                    System.arraycopy(typy, 0, nove_typy, 0, typy.length);
                    System.arraycopy(casy, 0, nove_casy, 0, casy.length);
                    TypIntervalu nic = TypIntervalu.getIntervalType("");
                    for (int i=typy.length; i<numb; i++) {
                        nove_typy[i] = nic;
                        casy[i] = null;
                    }
                    casy[typy.length] = null;
                } else {
                    // zkrácení
                    System.arraycopy(typy,0, nove_typy,0, nove_typy.length);
                    System.arraycopy(casy, 0, nove_casy, 0, nove_casy.length);
                }
                typy = nove_typy;
                casy = nove_casy;
            }
        }
        
        public Cas getWorkDuration() {
            Cas total = new Cas("0:00");
            for (int i=0; i<typy.length; i++) {
                Interval ivl = getInterval(i);
                total.add(ivl.getWorkDuration());
            }
            return total;
        }
        
        public void zapocti(UhrnMesice um) {
            typ_dne.zapocti(this, um);
        }
        
        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder(20);
            sb.append(den);
            sb.append(" typ dne: ");
            sb.append(typ_dne);
            sb.append(" intervaly: ");
            for (int i=0; i<typy.length; i++) {
                sb.append(casy[i]);
                sb.append(" [");
                sb.append(typy[i]);
                sb.append("] ");
            }
            sb.append(casy[typy.length]);
            return sb.toString();
        }
        
        public Day getDay() {
            return this.den;
        }
    }

    /** Vytvoří prázdnou stránku docházky */
    public void loadEmpty(String login, String jmeno, int rok, int mesic, int pocet_intervalu) {
        this.login = login;
        this.name = jmeno;
        this.rok = rok;
        this.mesic = mesic;
        this.pocet_intervalu = pocet_intervalu;
        Day[] dny = Day.getWholeMonth(mesic,rok);
        radky = new Radek[dny.length];
        for (int i=0; i<dny.length; i++) {
            radky[i] = new Radek(dny[i], pocet_intervalu);
        }
    }
    
    /** Načte dokument stránky docházky z formuláře přijatého servletem. */
    public void loadFromForm(javax.servlet.http.HttpServletRequest request) throws javax.servlet.ServletException {
        String login = request.getParameter("login");
        String mesic_s = request.getParameter("mesic");
        String rok_s = request.getParameter("rok");
        String intervalu_s = request.getParameter("intervalu");
        String name = request.getParameter("jmeno");
        int mesic,rok,intervalu;
        try {
            mesic = Integer.parseInt(mesic_s);
            rok = Integer.parseInt(rok_s);
            intervalu = Integer.parseInt(intervalu_s);
        } catch (NumberFormatException e) {
            throw new ServletException("Bad number format",e);
        }
        
        this.mesic = mesic;
        this.rok = rok;
        this.pocet_intervalu = intervalu;
        this.login = login;
        this.name = name;
        
        Day[] dny = Day.getWholeMonth(mesic-1,rok);
        radky = new Radek[dny.length];
        for (int i=0; i<dny.length; i++) {
            radky[i] = new Radek(dny[i], pocet_intervalu);
            radky[i].typ_dne = TypDne.getDayType(request.getParameter("d"+i+"t"));
            for (int j=0; j<pocet_intervalu; j++) {
                radky[i].typy[j] = TypIntervalu.getIntervalType(request.getParameter("d"+i+"i"+j+"t"));
                radky[i].casy[j] = Cas.instance(request.getParameter("d"+i+"i"+j+"c"));
            }
            radky[i].casy[pocet_intervalu] = Cas.instance(request.getParameter("d"+i+"i"+pocet_intervalu+"c"));
        }
    }
    
    
    private class XMLLoader extends DefaultHandler {

        int akt_den;
        int akt_interval;
        Day[] dny = null;
        StringBuffer text = null;

        public XMLLoader() {
            akt_den = 0;
            akt_interval = 0;
        }
        
        public void startDocument() throws SAXException {
        }

        public void startElement(String uri,
                                 String localName,
                                 String qName,
                                 Attributes attributes)
                          throws SAXException {
           if (qName.equals("stranka-dochazky")) {
//               System.out.println("stránka");
               login = attributes.getValue("login");
               mesic = Integer.parseInt(attributes.getValue("mesic"));
               rok = Integer.parseInt(attributes.getValue("rok"));
               name = attributes.getValue("jmeno");
               pocet_intervalu = Integer.parseInt(attributes.getValue("intervalu"));
               dny = Day.getWholeMonth(mesic-1, rok);
               radky = new Radek[dny.length];
               akt_den = 0;
           } else if (qName.equals("den")) {
//               System.out.println("den");
               radky[akt_den] = new Radek(dny[akt_den], pocet_intervalu);
               radky[akt_den].typ_dne = TypDne.getDayType(attributes.getValue("typ"));
               akt_interval = 0;
           } else if (qName.equals("cas")) {
//               System.out.println("čas");
               text = new StringBuffer(5);
           } else if (qName.equals("typ")) {
//               System.out.println("typ");
               text = new StringBuffer();
           }
        }
        
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (text != null) {
                text.append(ch,start,length);
            }
        }
        
        public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
            if (qName.equals("stranka-dochazky")) {

            } else if (qName.equals("den")) {
                akt_den++;
            } else if (qName.equals("cas")) {
                if (text.length() > 0) {
                    radky[akt_den].casy[akt_interval] = new Cas(text.toString());
                } else {
                    radky[akt_den].casy[akt_interval] = null;
                }
            } else if (qName.equals("typ")) {
                radky[akt_den].typy[akt_interval] = TypIntervalu.getIntervalType(text.toString());
                akt_interval++;
            }
        }
    }
    
    public void loadFromXML(java.io.InputStream data) throws IOException {
        try {
            SAXParserFactory f = SAXParserFactory.newInstance();
            f.setValidating(false);
            SAXParser p = f.newSAXParser();
            p.parse(data, new XMLLoader());
            
        } catch (ParserConfigurationException e) {
            IOException x = new IOException("XML parser configuration error",e);
            throw x;
        } catch (SAXException e) {
            IOException x2 = new IOException("SAX Exception",e);
            throw x2;
        }
    }
    
    
    /** Vytvoří HTML formulář obsahující aktuální data. */
    public void saveToForm(java.io.PrintWriter out) throws java.io.IOException {
        Day dnes = Day.getToday();
        
        out.println("\n<!-- Stránka docházky - hlavička -->\n");
        out.println(" <input type='hidden' name='login' value='"+html(this.login)+"'>");
        out.println(" <input type='hidden' name='mesic' value='"+this.mesic+"'>");
        out.println(" <input type='hidden' name='rok' value='"+this.rok+"'>");
        out.println(" <input type='hidden' name='intervalu' value='"+this.pocet_intervalu+"'>");
        out.println(" <input type='hidden' name='jmeno' value='"+html(this.name)+"'>");
        out.println("\n <!-- Stránka docházky - řádky -->\n");
        
        out.println("<table class='radky'>");
        
        for (int i=0; i<radky.length; i++) {
            Day den = radky[i].den;
            
            // komentář s datem
            out.println("   <!-- "+den.toString()+" -->");

            // <tr> s klasifikací typu dne pro stylesheet
            if (den.isWorkDay()) {
                if (den.equals(dnes)) {
                    out.println("   <tr class='pracovni-dnes'>");
                } else {
                    out.println("   <tr class='pracovni'>");
                }
            } else {
                if (den.equals(dnes)) {
                    out.println("   <tr class='svatek-dnes'>");
                } else {
                    out.println("   <tr class='svatek'>");
                }
            }

            // číslo dne
            out.println("     <td class='den'>"+(den.getDay()+1)+".</td>");
            
            // typ dne
            out.print("     <td class='typ-dne'>");
            out.print(radky[i].typ_dne.toForm("d"+i+"t",i));
            out.println("</td>");
            
            // intervaly:
            for (int x = 0; x < pocet_intervalu; x++) {
                
                // čas vlevo od intervalu
                out.print("      <td class='cas'>");
                out.print(casToForm("d"+i+"i"+x+"c",i,radky[i].casy[x]));
                out.println("</td>");
                
                // typ intervalu
                out.print("      <td class='typ-intervalu'>");
                out.print(radky[i].typy[x].toForm("d"+i+"i"+x+"t",i));
                out.println("</td>");
            }
            // poslední čas
            out.print("      <td class='cas'>");
            out.print(casToForm("d"+i+"i"+pocet_intervalu+"c",i,radky[i].casy[pocet_intervalu]));
            out.println("</td>");
            
            out.println("   </tr>");
        }
        
        out.println("</table>");
    }


    public void saveToXML(java.io.OutputStream out) throws java.io.IOException {
        java.io.PrintWriter pw = new java.io.PrintWriter(new java.io.OutputStreamWriter(out,"UTF-8"));
        pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        pw.println("<stranka-dochazky login=\""+xml(login)+"\" rok=\""+rok+"\" mesic=\""+mesic+"\" intervalu=\""+pocet_intervalu+"\" jmeno=\""+xml(name)+"\">\n");
        for (int den=0; den<radky.length; den++) {
            pw.println("  <!-- "+xml(radky[den].getDay().toString())+" -->");
            pw.println("  <den typ=\""+xml(radky[den].typ_dne.getCode())+"\">");
            for (int i=0; i<pocet_intervalu; i++) {
                pw.print("    <cas>");
                Cas c = radky[den].casy[i];
                if (c != null) {
                    pw.print(xml(c.toString()));
                }
                pw.println("</cas>");
                pw.println("    <typ>"+xml(radky[den].typy[i].getCode())+"</typ>");
            }
            pw.print("    <cas>");
            Cas c = radky[den].casy[pocet_intervalu];
            if (c != null) {
                pw.print(xml(c.toString()));
            }
            pw.println("</cas>");
            
            pw.println("  </den>\n");
        }
        pw.println("</stranka-dochazky>");
        pw.flush();
    }
    
    public String saveToXMLString() {
        try {
            java.io.ByteArrayOutputStream b = new java.io.ByteArrayOutputStream();
            saveToXML(b);
            b.flush();
            return new String(b.toByteArray(),"UTF-8");
        } catch (Exception e) {
            return null;
        }
    }
    
    public static String xml(String text) {
        return CommonBase.xmlSafe(text);
    }
    
    public static String html(String text) {
        return CommonBase.htmlSafe(text);
    }

    private static String casToForm(String fieldname, int day, Cas cas) {
        StringBuffer sb = new StringBuffer(20);
        sb.append("<input name='");
        sb.append(html(fieldname));
        sb.append("' type='text' maxlength='5' value='");
        if (cas!=null) {
            sb.append(cas.toString());
        }
        sb.append("' onchange='handleDayChange("+day+")'>");
        return sb.toString();
    }
    
    public String toString() {
        StringBuffer sb =  new StringBuffer(500);
        sb.append(super.toString());
        sb.append(" = {\n");
        sb.append("  login=");
        sb.append(this.login);
        sb.append("\n  name=");
        sb.append(this.name);
        sb.append("\n  rok=");
        sb.append(this.rok);
        sb.append("\n  mesic=");
        sb.append(this.mesic);
        sb.append("\n  ----------");
        for (int i=0; i<radky.length; i++) {
            sb.append("\n  ["+i+"] ");
            sb.append(radky[i]);
        }
        sb.append("\n}");
        return sb.toString();
    }
    
    
    public int getNumberOfIntervals() {
        return this.pocet_intervalu;
    }
    
    
    public void setNumberOfIntervals(int pocet) {
        if (pocet < 1) throw new IllegalArgumentException("Number of intervals must be > 0");
        this.pocet_intervalu = pocet;
        for (int i=0; i<radky.length; i++) {
            radky[i].setNumberOfIntervals(pocet);
        }
    }
    
    public String getLogin() {
        return this.login;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getYear() {
        return this.rok;
    }
    
    public int getMonth() {
        return this.mesic;
    }
    
    
    public UhrnMesice getSummary() {
        UhrnMesice um = new UhrnMesice();
        for (int den = 0; den < radky.length; den++) {
            radky[den].zapocti(um);
        }
        return um;
    }
    
    public StrankaDochazky.Radek[] getDays() {
        return radky;
    }
    
    
    
    
    public static void main(String[] args) {
        try {
            StrankaDochazky s = new StrankaDochazky();
            
            FileInputStream fis = new FileInputStream("C:\\projects\\dochazkawebapp\\userdata\\dochazka_hlavac_200310.xml");
            try {
            
            s.loadFromXML(fis);
            System.out.println(s);
            
            } finally {
                fis.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
