/*
 * WorkersMonth.java
 *
 * Created on 17. červen 2003, 17:13
 */

package com.bistudio.dochazka;

/**
 *
 * @author  hlavac
 */
public class WorkersMonth {
    
    Day[] days = null;
    int pracovnich_dni       = 0;
    int svatku_dni           = 0;
    int dni_pracovniho_klidu = 0;
    
    /** Creates a new instance of WorkersMonth */
    private WorkersMonth(int month, int year) {
        days = Day.getWholeMonth(month, year);
        pracovnich_dni       = 0;
        svatku_dni           = 0;
        dni_pracovniho_klidu = 0;
        for (int i=0; i<days.length; i++) {
            if (days[i].getDayOfWeek()>4) {
                // sobota, neděle
                dni_pracovniho_klidu++;
            } else if (days[i].getHolidays().length > 0) {
                // svátek
                svatku_dni++;
            } else {
                pracovnich_dni++;
            }
        }
    }
    
    public static WorkersMonth getByMonthYear(int month, int year) {
        return new WorkersMonth(month, year);
    }
    
    
    public int getNumberOfWorkDays() {
        return pracovnich_dni;
    }
    
    public int getNumberOfHolidays() {
        return svatku_dni;
    }
    
    public int getNumberOfWeekendDays() {
        return dni_pracovniho_klidu;
    }
    
    public int getNumberOfDays() {
        return days.length;
    }
    
    
    public static void main(String[] args) {
        Day dnes = Day.getToday();
        WorkersMonth wm = WorkersMonth.getByMonthYear(dnes.getMonth(),  dnes.getYearNumber());
        System.out.println("Tento pracovní měsíc má:");
        System.out.println("dní: "+wm.getNumberOfDays());
        System.out.println("pracovních dní: "+wm.getNumberOfWorkDays());
        System.out.println("státních svátků: "+wm.getNumberOfHolidays());
        System.out.println("sobot a nedělí: "+wm.getNumberOfWeekendDays());
    }
}
