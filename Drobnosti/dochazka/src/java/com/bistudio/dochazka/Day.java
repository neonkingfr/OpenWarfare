/*
 * Day.java
 *
 * Created on 5. červen 2003, 17:33
 */


package com.bistudio.dochazka;

import java.util.HashMap;

/**
 *
 * @author  hlavac
 */
public class Day implements Comparable {
    
    public static final int PONDELI  = 0;
    public static final int UTERY    = 1;
    public static final int STREDA   = 2;
    public static final int CTVRTEK  = 3;
    public static final int PATEK    = 4;
    public static final int SOBOTA   = 5;
    public static final int NEDELE   = 6;
    
    public static final int LEDEN    = 0;
    public static final int UNOR     = 1;
    public static final int BREZEN   = 2;
    public static final int DUBEN    = 3;
    public static final int KVETEN   = 4;
    public static final int CERVEN   = 5;
    public static final int CERVENEC = 6;
    public static final int SRPEN    = 7;
    public static final int ZARI     = 8;
    public static final int RIJEN    = 9;
    public static final int LISTOPAD = 10;
    public static final int PROSINEC = 11;
    
    public static final int DAY1 = 0;
    public static final int DAY2 = 1;
    public static final int DAY3 = 2;
    public static final int DAY4 = 3;
    public static final int DAY5 = 4;
    public static final int DAY6 = 5;
    public static final int DAY7 = 6;
    public static final int DAY8 = 7;
    public static final int DAY9 = 8;
    public static final int DAY10 = 9;
    public static final int DAY11 = 10;
    public static final int DAY12 = 11;
    public static final int DAY13 = 12;
    public static final int DAY14 = 13;
    public static final int DAY15 = 14;
    public static final int DAY16 = 15;
    public static final int DAY17 = 16;
    public static final int DAY18 = 17;
    public static final int DAY19 = 18;
    public static final int DAY20 = 19;
    public static final int DAY21 = 20;
    public static final int DAY22 = 21;
    public static final int DAY23 = 22;
    public static final int DAY24 = 23;
    public static final int DAY25 = 24;
    public static final int DAY26 = 25;
    public static final int DAY27 = 26;
    public static final int DAY28 = 27;
    public static final int DAY29 = 28;
    public static final int DAY30 = 29;
    public static final int DAY31 = 30;
    
    static final String day_names_css[] = { "Po ","Út ","St ","Čt ","Pá ","So ","Ne "};
    static final String day_names_cs[] = { "pondělí","úterý","středa","čtvrtek","pátek","sobota","neděle"};
    static final String month_names_cs[] = {"leden","únor","březen","duben","květen","červen","červenec","srpen","září","říjen","listopad","prosinec"};
    static final String month_names_cs2[] = {"ledna","února","března","dubna","května","června","července","srpna","září","října","listopadu","prosince"};
    static final String nth[] = {"1.","2.","3.","4.","5.","6.","7.","8.","9.","10.",
                                "11.","12.","13.","14.","15.","16.","17.","18.","19.",
                                "20.","21.","22.","23.","24.","25.","26.","27.",
                                "28.","29.","30.","31."};
                                
    static final String[] NOTHING = {};
                                
    boolean have_day_number = false;
    int day_number = 0;
    int dow = 0;
    
    boolean have_day_month_year = false;
    int day = 0;
    int month = 0;
    Year year = null;

    public static int daysInMonth(int year, int month) {
        if (month == 1) { // únor
            return ((year % 4) == 0)&&((year % 100 !=0)||(year % 400 == 0)) ? 29 : 28;
        } else {
            return days_in_month[month];
        }
    }
    
    public static Day[] getWholeMonth(int month, int year) {
        Day[] days = new Day[Day.daysInMonth(year,month)];
        days[0] = Day.getDayByDMY(0, month, year);
        for (int i=1; i<days.length; i++) {
            days[i] = days[0].relativeDay(i);
        }
        return days;
    }
    
    public static Day getDayByDayNumber(int day_number) {
        return new Day(day_number);
    }
    
    public static Day getDayByDate(java.util.Date date) {
        return Day.getDayByDayNumber(Day.dayNumber(date));
    }
    
    public static String getMonthName(int month) {
        return month_names_cs[month];
    }
    public static String getMonthName2(int month) {
        return month_names_cs2[month];
    }
    
    public String getMonthName() {
        return getMonthName(getMonth());
    }
    public String getMonthName2() {
        return getMonthName2(getMonth());
    }
    
    public static String getDayOfWeekName(int dow) {
        return day_names_cs[dow];
    }

    public static String getDayOfWeekShortName(int dow) {
        return day_names_css[dow];
    }
    
    public String getDayOfWeekName() {
        return getDayOfWeekName(getDayOfWeek());
    }
    
    public String getDayOfWeekShortName() {
        return getDayOfWeekShortName(getDayOfWeek());
    }
    
    public static Day getDayByDMY(int day, int month, int year) {
        return new Day(year,month,day);
    }
    
    public static Day getToday() {
        return Day.getDayByDate(new java.util.Date());
    }
    
    public boolean isWorkDay() {
        return !((getDayOfWeek()>4)||(getHolidays().length!=0));
    }
    
    
    /** Creates a new instance of Day */
    private Day(int day_number) {
        this.day_number = day_number;
        this.dow = day_number % 7;
        this.have_day_number = true;
    }
    
    private Day(int year, int month, int day) {
        this.day = day;
        this.month = month;
        this.year = Year.getYear(year);
        this.have_day_month_year = true;
    }
    
    public int getDay() {
        if (!have_day_month_year) {
            _calculateDMY();
            have_day_month_year = true;
        }
        return this.day;
    }
    
    public int getMonth() {
        if (!have_day_month_year) {
            _calculateDMY();
            have_day_month_year = true;
        }
        return this.month;
    }
    
    public int getYearNumber() {
        if (!have_day_month_year) {
            _calculateDMY();
            have_day_month_year = true;
        }
        return this.year.getYear();
    }
    
    public Year getYear() {
        if (!have_day_month_year) {
            _calculateDMY();
            have_day_month_year = true;
        }
        return this.year;
    }
    
    public int getDayNumber() {
        if (!have_day_number) {
            _calculateDN();
            have_day_number = true;
        }
        return this.day_number;
    }
    
    public int getDayOfWeek() {
        if (!have_day_number) {
            _calculateDN();
            have_day_number = true;
        }
        return this.dow;
    }
    
    
    public Day nextDay() {
        return getDayByDayNumber(getDayNumber()+1);
    }

    public Day previousDay() {
        return getDayByDayNumber(getDayNumber()-1);
    }
    
    public Day relativeDay(int offset_days) {
        return getDayByDayNumber(getDayNumber()+offset_days);
    }

    static final int[] days_in_month = {31,28,31,30,31,30,31,31,30,31,30,31}; 

    
    private void _calculateDN() {
        int dn = Day.dayNumber(this.year.getYear(), this.month, this.day);
        this.day_number = dn;
        this.dow = dn % 7;
    }
    
    private void _calculateDMY() {
        int x = ((this.day_number+366)/ 146097);
        int rok = 400*x;
        int zbytek = (this.day_number+366) % 146097;
            
        // zbytek = počet dní čtyřsetletí
        // 25 přestupných čtyřletí
        x = zbytek/1461;
        if (x>25) x = 25;
        if (x > 0) {
            rok += 4*x;
            zbytek -= x*1461;
            // 1 nepřestupné čtyřletí
            if (zbytek >= 1460) {
                rok += 4;
                zbytek -= 1460;
                // 24 přestupných čtyřletí
                x = zbytek/1461; if (x>24) x = 24;
                if (x>0) {
                    rok += 4*x; zbytek -= x*1461;
                    // 1 nepřestupné čtyřletí
                    if (zbytek >= 1460) {
                        rok += 4;
                        zbytek -= 1460;
                        // 24 přestupných čtyřletí
                        x = zbytek/1461; if (x>24) x = 24;
                        if (x>0) {
                            rok += 4*x; zbytek -= x*1461;
                            // 1 nepřestupné čtyřletí
                            if (zbytek >= 1460) {
                                rok += 4;
                                zbytek -= 1460;
                                // 24 přestupných čtyřletí
                                x = zbytek/1461; if (x>24) x = 24;
                                if (x>0) {
                                    rok += 4*x; zbytek -= x*1461;
                                }
                            }
                        }
                    }
                }
            }
        }
        // zbytek = čtyřletí. rok je první rok čtyřletí...
        int prvnirok = (rok % 4 == 0)&&((rok % 100 != 0)||(rok % 400 == 0)) ? 366: 365;
        if (zbytek >= prvnirok) {
            rok += 1;
            zbytek -= prvnirok;
            int zbylych_roku = zbytek/365;
            rok += zbylych_roku;
            zbytek -= 365*zbylych_roku;
        }
        // zbytek = jeden rok
        int unor = (rok % 4 == 0)&&((rok % 100 != 0)||(rok % 400 == 0)) ? 29: 28;
        int m = 0;
        if (zbytek >= 31) { // leden
            m++;
            zbytek -= 31;
            if (zbytek >= unor) { // únor
                m++;
                zbytek -= unor;
                if (zbytek >= 31) { // březen
                    m++;
                    zbytek -= 31;
                    if (zbytek >= 30) { // duben
                        m++;
                        zbytek -= 30;
                        if (zbytek >= 31) { // květen
                            m++;
                            zbytek -= 31;
                            if (zbytek >= 30) { // červen
                                m++;
                                zbytek -= 30;
                                if (zbytek >= 31) { // červenec
                                    m++;
                                    zbytek -= 31;
                                    if (zbytek >= 31) { // srpen
                                        m++;
                                        zbytek -= 31;
                                        if (zbytek >= 30) { // září
                                            m++;
                                            zbytek -= 30;
                                            if (zbytek >= 31) { // říjen
                                                m++;
                                                zbytek -= 31;
                                                if (zbytek >= 30) { // listopad
                                                    m++;
                                                    zbytek -= 30;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        // zbytek = den
        this.day = zbytek;
        this.month = m;
        this.year = Year.getYear(rok);
    }
    
    public static int dayNumber(int year, int month, int day) {
        if (year < 0) throw new IllegalArgumentException("Year must be > 0");
        boolean is_leap_year = ((year % 4) == 0) && (((year % 100) != 0 )||((year % 400) == 0));

        int february_days = (is_leap_year ? 29 : 28);

        if ((month < 0)||(month>11)) throw new IllegalArgumentException("Month must be 0..11");

        if (month != 1) { // all except february
            if ((day < 0)||(day >= days_in_month[month])) throw new IllegalArgumentException("Day must be 0.."+(days_in_month[month]-1));
        } else { // february
            if ((day < 0) || (day >= february_days)) throw new IllegalArgumentException("Day must be 0.."+(february_days-1));
        }
        
        int days = (year*365)+((year+399)/4)-((year+399)/100)+((year+399)/400)-462;
        
        switch (month) {
            
            case LEDEN:     break;
            
            case UNOR:      days += 31;
                            break;
                            
            case BREZEN:    days += (february_days + 31);  
                            break;

            case DUBEN:     days += (31 + february_days + 31);  
                            break;

            case KVETEN:    days += (30 + 31 + february_days + 31);  
                            break;

            case CERVEN:    days += (31 + 30 + 31 + february_days + 31);  
                            break;

            case CERVENEC:  days += (30 + 31 + 30 + 31 + february_days + 31);  
                            break;

            case SRPEN:     days += (31 + 30 + 31 + 30 + 31 + february_days + 31);  
                            break;

            case ZARI:      days += (31 + 31 + 30 + 31 + 30 + 31 + february_days + 31);  
                            break;

            case RIJEN:     days += (30 + 31 + 31 + 30 + 31 + 30 + 31 + february_days + 31);  
                            break;

            case LISTOPAD:  days += (31 + 30 + 31 + 31 + 30 + 31 + 30 + 31 + february_days + 31);  
                            break;

            case PROSINEC:  days += (30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31 + february_days + 31);  
                            break;
        }
        days += day;
        return days;
    }
    
    public static int dayNumber(java.util.Date date) {
        return (int)(date.getTime()/1000/60/60/24)+719162;
    }

    public static java.util.Date getDateFromDayNumber(int day_number) {
        return new java.util.Date((long)(day_number-719162)*1000*60*60*24);
    }

    public int hashCode() {
        return this.getDayNumber();
    }
   
    public boolean equals(Object o) {
        if (o == null) return false;
        try {
            Day other = (Day)o;
            if (this.have_day_number && other.have_day_number) {
                // compare by day number...
                return this.day_number == other.day_number;
            } else if (this.have_day_month_year && other.have_day_month_year) {
                // compare by DMY
                return (this.day == other.day) && (this.month == other.month) && (this.year.equals(other.year));
            } else {
                return this.getDayNumber() == other.getDayNumber();
            }
        } catch (ClassCastException e) {
            return false;
        }
    }
    
    public int compareTo(Object o) {
        Day other = (Day)o;
        return this.getDayNumber()-other.getDayNumber();
    }
    
    
    /** Returns how many days from this day the parameter day is.
     * @param day
     * @returns The number of days from this day. Returns negative offsets for day before this day.
     */
    public int diff(Day other) {
        return other.getDayNumber()-this.getDayNumber();
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer(13);
        sb.append(day_names_cs[getDayOfWeek()]);
        sb.append(' ');
        sb.append(nth[getDay()]);
        sb.append(nth[getMonth()]);
        sb.append(year.getYear());
        return sb.toString();
    }
    
    public int getWeekNumber() {
        Year thisyear = getYear();
        Day first_week_start = thisyear.getFirstWeekStart();
        Day last_week_end = thisyear.getLastWeekEnd();
        int days_since_first_week_start = first_week_start.diff(this);
        if (days_since_first_week_start < 0) {
            // last week of previous year
            return thisyear.previous().getNumberOfWeeks();
        }
        int days_since_last_week_end = last_week_end.diff(this);
        if (days_since_last_week_end > 0) return 1;
        return (days_since_first_week_start/7)+1;
    }
    
    public String[] getCalendarNames() {
        return jmeniny[getMonth()][getDay()];
    }
    
    public String[] getHolidays() {
        String[] holidays = (String[])getYear().getHoliaysMap().get(this);
        if (holidays == null) holidays = NOTHING;
        return holidays;
    }
    
    
    public static void main(String[] args) {
        Year this_year = Year.getCurrentYear();
        Day first_day = this_year.getFirstDay();
        Day last_day = this_year.getLastDay();
        
        System.out.println("ROK "+this_year.getYear()+" má "+this_year.getNumberOfWeeks()+" týdnů\n");
        
        for (Day den = first_day; den.compareTo(last_day)<=0; den=den.nextDay()) {
            if (den.getDay() == 0) {
                System.out.println(month_names_cs[den.getMonth()].toUpperCase()+"\n");
            }
            String[] jmena = den.getCalendarNames();
            String[] svatky = den.getHolidays();
            int dow = den.getDayOfWeek();
            if ((dow == Day.PONDELI)||(den.equals(den.getYear().getFirstDay()))) {
                System.out.print("["+den.getWeekNumber()+"]  ");
            } else {
                System.out.print("     ");
            }
            if ((dow >= Day.SOBOTA)||(svatky.length > 0)) {
                System.out.print("* ");
            } else {
                System.out.print("  ");
            }
            System.out.print(den);
            if (jmena.length > 0) {
                System.out.print(" JMENINY: ");
                for (int xi=0; xi<jmena.length;xi++) {
                    if (xi>0) System.out.print(", ");
                    System.out.print(jmena[xi]);
                }
            }
            if (svatky.length > 0) {
                System.out.print(" SVÁTKY: ");
                for (int xj=0; xj<svatky.length;xj++) {
                    if (xj>0) System.out.print(", ");
                    System.out.print(svatky[xj]);
                }
            }
            System.out.println();
        }
    }        
    
    static final String[][][] jmeniny = {
        // leden
        {
            /*  1. */ { },
            /*  2. */ { "Karina" },
            /*  3. */ { "Radmila" },
            /*  4. */ { "Diana" },
            /*  5. */ { "Dalimil" },
            /*  6. */ { "Tři králové" },
            /*  7. */ { "Vilma" },
            /*  8. */ { "Čestmír" },
            /*  9. */ { "Vladan" },
            /* 10. */ { "Břetislav" },
            /* 11. */ { "Bohdana" },
            /* 12. */ { "Pravoslav" },
            /* 13. */ { "Edita" },
            /* 14. */ { "Radovan" },
            /* 15. */ { "Alice" },
            /* 16. */ { "Ctirad" },
            /* 17. */ { "Drahoslav" },
            /* 18. */ { "Vladislav" },
            /* 19. */ { "Doubravka" },
            /* 20. */ { "Ilona" },
            /* 21. */ { "Běla" },
            /* 22. */ { "Slavomír" },
            /* 23. */ { "Zdeněk" },
            /* 24. */ { "Milena" },
            /* 25. */ { "Miloš" },
            /* 26. */ { "Zora" },
            /* 27. */ { "Ingrid" },
            /* 28. */ { "Otýlie" },
            /* 29. */ { "Zdislava" },
            /* 30. */ { "Robin" },
            /* 31. */ { "Marika" }
        },
        // únor
        {
            /*  1. */ { "Hynek" },
            /*  2. */ { "Nela" },
            /*  3. */ { "Blažej" },
            /*  4. */ { "Jarmila" },
            /*  5. */ { "Dobromila" },
            /*  6. */ { "Vanda" },
            /*  7. */ { "Veronika" },
            /*  8. */ { "Milada" },
            /*  9. */ { "Apolena" },
            /* 10. */ { "Mojmír" },
            /* 11. */ { "Božena" },
            /* 12. */ { "Slavěna" },
            /* 13. */ { "Věnceslav" },
            /* 14. */ { "Valentýn" },
            /* 15. */ { "Jiřina" },
            /* 16. */ { "Ljuba" },
            /* 17. */ { "Miloslava" },
            /* 18. */ { "Gizela" },
            /* 19. */ { "Patrik" },
            /* 20. */ { "Oldřich" },
            /* 21. */ { "Lenka" },
            /* 22. */ { "Petr" },
            /* 23. */ { "Svatopluk" },
            /* 24. */ { "Matěj" },
            /* 25. */ { "Liliana" },
            /* 26. */ { "Dorota" },
            /* 27. */ { "Alexandr" },
            /* 28. */ { "Lumír" },
            /* 29. */ { "Horymír" }
        },
        // březen
        {
            /*  1. */ { "Bedřich" },
            /*  2. */ { "Anežka" },
            /*  3. */ { "Kamil" },
            /*  4. */ { "Stela" },
            /*  5. */ { "Kazimír" },
            /*  6. */ { "Miroslav" },
            /*  7. */ { "Tomáš" },
            /*  8. */ { "Gabriela" },
            /*  9. */ { "Františka" },
            /* 10. */ { "Viktorie" },
            /* 11. */ { "Anděla" },
            /* 12. */ { "Řehoř" },
            /* 13. */ { "Růžena" },
            /* 14. */ { "Rút", "Matylda"},
            /* 15. */ { "Ida" },
            /* 16. */ { "Elena", "Herbert" },
            /* 17. */ { "Vlastimil" },
            /* 18. */ { "Eduard" },
            /* 19. */ { "Josef" },
            /* 20. */ { "Světlana" },
            /* 21. */ { "Radek" },
            /* 22. */ { "Leona" },
            /* 23. */ { "Ivona" },
            /* 24. */ { "Gabriel" },
            /* 25. */ { "Marián" },
            /* 26. */ { "Emanuel" },
            /* 27. */ { "Dita" },
            /* 28. */ { "Soňa" },
            /* 29. */ { "Taťána" },
            /* 30. */ { "Arnošt" },
            /* 31. */ { "Kvído" }
      },
        // duben
        {
            /*  1. */ { "Hugo" },
            /*  2. */ { "Erika" },
            /*  3. */ { "Richard" },
            /*  4. */ { "Ivana" },
            /*  5. */ { "Miroslava" },
            /*  6. */ { "Vendula" },
            /*  7. */ { "Heřman", "Hermína" },
            /*  8. */ { "Ema" },
            /*  9. */ { "Dušan" },
            /* 10. */ { "Darja" },
            /* 11. */ { "Izabela" },
            /* 12. */ { "Julius" },
            /* 13. */ { "Aleš" },
            /* 14. */ { "Vincenc" },
            /* 15. */ { "Anastázie" },
            /* 16. */ { "Irena" },
            /* 17. */ { "Rudolf" },
            /* 18. */ { "Valérie" },
            /* 19. */ { "Rostislav" },
            /* 20. */ { "Marcela" },
            /* 21. */ { "Alexandra" },
            /* 22. */ { "Evžénie" },
            /* 23. */ { "Vojtěch" },
            /* 24. */ { "Jiří" },
            /* 25. */ { "Marek" },
            /* 26. */ { "Oto" },
            /* 27. */ { "Jaroslav" },
            /* 28. */ { "Vlastislav" },
            /* 29. */ { "Robert" },
            /* 30. */ { "Blahoslav"}
        },
        // květen
        {
            /*  1. */ { },
            /*  2. */ { "Zikmund" },
            /*  3. */ { "Alexej" },
            /*  4. */ { "Květoslav" },
            /*  5. */ { "Klaudie" },
            /*  6. */ { "Radoslav" },
            /*  7. */ { "Stanislav" },
            /*  8. */ { },
            /*  9. */ { "Ctibor" },
            /* 10. */ { "Blažena" },
            /* 11. */ { "Svatava" },
            /* 12. */ { "Pankrác" },
            /* 13. */ { "Servác" },
            /* 14. */ { "Bonifác" },
            /* 15. */ { "Žofie" },
            /* 16. */ { "Přemysl"},
            /* 17. */ { "Aneta" },
            /* 18. */ { "Nataša" },
            /* 19. */ { "Ivo" },
            /* 20. */ { "Zbyšek" },
            /* 21. */ { "Monika" },
            /* 22. */ { "Emil" },
            /* 23. */ { "Vladimír" },
            /* 24. */ { "Jana" },
            /* 25. */ { "Viola" },
            /* 26. */ { "Filip" },
            /* 27. */ { "Valdemar" },
            /* 28. */ { "Vilém" },
            /* 29. */ { "Maxmilián", "Maxim" },
            /* 30. */ { "Ferdinand" },
            /* 31. */ { "Kamila" },
        },
        // červen
        {
            /*  1. */ { "Laura" },
            /*  2.?*/ { "Jarmil" },
            /*  3. */ { "Tamara" },
            /*  4. */ { "Dalibor" },
            /*  5. */ { "Dobroslav" },
            /*  6. */ { "Norbert" },
            /*  7. */ { "Iveta", "Slavoj" },
            /*  8. */ { "Medard" },
            /*  9. */ { "Stanislava" },
            /* 10. */ { "Gita" },
            /* 11. */ { "Bruno" },
            /* 12. */ { "Antonie" },
            /* 13. */ { "Antonín" },
            /* 14. */ { "Roland" },
            /* 15. */ { "Vít" },
            /* 16. */ { "Zbyněk" },
            /* 17. */ { "Adolf" },
            /* 18. */ { "Milan" },
            /* 19. */ { "Leoš" },
            /* 20. */ { "Květa" },
            /* 21. */ { "Alois" },
            /* 22. */ { "Pavla" },
            /* 23. */ { "Zdeňka" },
            /* 24. */ { "Jan" },
            /* 25. */ { "Ivan" },
            /* 26. */ { "Adriana" },
            /* 27. */ { "Ladislav" },
            /* 28. */ { "Lubomír" },
            /* 29. */ { "Petr", "Pavel" },
            /* 30. */ { "Šárka" },
        },
        // červenec
        {
            /*  1. */ { "Jaroslava" },
            /*  2. */ { "Patricie" },
            /*  3. */ { "Radomír" },
            /*  4. */ { "Prokop" },
            /*  5. */ { },
            /*  6. */ { },
            /*  7. */ { "Bohuslava" },
            /*  8. */ { "Nora" },
            /*  9. */ { "Drahoslava" },
            /* 10. */ { "Libuše", "Amálie" },
            /* 11. */ { "Olga" },
            /* 12. */ { "Bořek" },
            /* 13. */ { "Markéta" },
            /* 14. */ { "Karolína" },
            /* 15. */ { "Jindřich" },
            /* 16. */ { "Luboš" },
            /* 17. */ { "Martina" },
            /* 18. */ { "Drahomíra" },
            /* 19. */ { "Čeněk" },
            /* 20. */ { "Ilja" },
            /* 21. */ { "Vítězslav" },
            /* 22. */ { "Magdaléna" },
            /* 23. */ { "Libor" },
            /* 24. */ { "Kristýna" },
            /* 25. */ { "Jakub" },
            /* 26. */ { "Anna" },
            /* 27. */ { "Věroslav" },
            /* 28. */ { "Viktor" },
            /* 29. */ { "Marta" },
            /* 30. */ { "Bořivoj" },
            /* 31. */ { "Ignác" }
        },
        // srpen
        {
            /*  1. */ { "Oskar" },
            /*  2. */ { "Gustav" },
            /*  3. */ { "Miluše" },
            /*  4. */ { "Dominik" },
            /*  5. */ { "Kristián" },
            /*  6. */ { "Oldřiška" },
            /*  7. */ { "Lada" },
            /*  8. */ { "Soběslav" },
            /*  9. */ { "Roman" },
            /* 10. */ { "Vavřinec" },
            /* 11. */ { "Zuzana" },
            /* 12. */ { "Klára" },
            /* 13. */ { "Alena" },
            /* 14. */ { "Alan" },
            /* 15. */ { "Hana" },
            /* 16. */ { "Jáchym" },
            /* 17. */ { "Petra" },
            /* 18. */ { "Helena" },
            /* 19. */ { "Ludvík" },
            /* 20. */ { "Bernard" },
            /* 21. */ { "Johana" },
            /* 22. */ { "Bohuslav" },
            /* 23. */ { "Sandra" },
            /* 24. */ { "Bartoloměj" },
            /* 25. */ { "Radim" },
            /* 26. */ { "Luděk" },
            /* 27. */ { "Otakar" },
            /* 28. */ { "Augustýn" },
            /* 29. */ { "Evelína" },
            /* 30. */ { "Vladěna" },
            /* 31. */ { "Pavlína" },
        },
        // září
        {
            /*  1. */ { "Linda", "Samuel" },
            /*  2. */ { "Adéla" },
            /*  3. */ { "Bronislav" },
            /*  4. */ { "Jindřiška" },
            /*  5. */ { "Boris" },
            /*  6. */ { "Boleslav" },
            /*  7.?*/ { "Regina" },
            /*  8. */ { "Mariana" },
            /*  9. */ { "Daniela" },
            /* 10. */ { "Irma" },
            /* 11. */ { "Denisa" },
            /* 12. */ { "Marie" },
            /* 13. */ { "Lubor" },
            /* 14. */ { "Radka" },
            /* 15. */ { "Jolana" },
            /* 16. */ { "Ludmila" },
            /* 17. */ { "Naděžda" },
            /* 18. */ { "Kryštof" },
            /* 19. */ { "Zita" },
            /* 20. */ { "Oleg" },
            /* 21. */ { "Matouš" },
            /* 22. */ { "Darina" },
            /* 23. */ { "Berta" },
            /* 24. */ { "Jaromír" },
            /* 25. */ { "Zlata" },
            /* 26. */ { "Andrea" },
            /* 27. */ { "Jonáš" },
            /* 28. */ { "Václav" },
            /* 29. */ { "Michal" },
            /* 30. */ { "Jeroným" }
        },
        // říjen
        {
            /*  1. */ { "Igor" },
            /*  2. */ { "Olivie", "Oliver" },
            /*  3. */ { "Bohumil" },
            /*  4. */ { "František" },
            /*  5. */ { "Eliška" },
            /*  6. */ { "Hanuš" },
            /*  7. */ { "Justýna" },
            /*  8. */ { "Věra" },
            /*  9. */ { "Štefán", "Sára" },
            /* 10. */ { "Marina" },
            /* 11. */ { "Andrej" },
            /* 12. */ { "Marcel" },
            /* 13.?*/ { "Renata" },
            /* 14. */ { "Agáta" },
            /* 15. */ { "Tereza" },
            /* 16. */ { "Havel" },
            /* 17. */ { "Hedvika" },
            /* 18. */ { "Lukáš" },
            /* 19. */ { "Michaela" },
            /* 20. */ { "Vendelín" },
            /* 21. */ { "Brigita" },
            /* 22. */ { "Sabina" },
            /* 23. */ { "Teodor" },
            /* 24. */ { "Nina" },
            /* 25. */ { "Beáta" },
            /* 26. */ { "Erik" },
            /* 27. */ { "Šarlota", "Zoe" },
            /* 28. */ { },
            /* 29. */ { "Silvie" },
            /* 30. */ { "Tadeáš" },
            /* 31. */ { "Štěpánka" }
        },
        // listopad
        {
            /*  1. */ { "Felix" },
            /*  2. */ { },
            /*  3. */ { "Hubert" },
            /*  4. */ { "Karel" },
            /*  5. */ { "Miriam" },
            /*  6. */ { "Liběna" },
            /*  7. */ { "Saskie" },
            /*  8. */ { "Bohumír" },
            /*  9. */ { "Bohdan" },
            /* 10. */ { "Evžen" },
            /* 11. */ { "Martin" },
            /* 12. */ { "Benedikt" },
            /* 13. */ { "Tibor" },
            /* 14.?*/ { "Sáva" },
            /* 15. */ { "Leopold" },
            /* 16. */ { "Otmar" },
            /* 17. */ { "Mahulena" },
            /* 18. */ { "Romana" },
            /* 19. */ { "Alžběta" },
            /* 20. */ { "Nikola" },
            /* 21. */ { "Albert" },
            /* 22. */ { "Cecílie" },
            /* 23. */ { "Klement" },
            /* 24. */ { "Emílie" },
            /* 25. */ { "Kateřina" },
            /* 26. */ { "Artur" },
            /* 27. */ { "Xenie" },
            /* 28. */ { "René" },
            /* 29. */ { "Zina" },
            /* 30. */ { "Ondřej" }
        },
        // prosinec
        {
            /*  1. */ { "Iva" },
            /*  2. */ { "Blanka" },
            /*  3. */ { "Svatoslav" },
            /*  4. */ { "Barbora" },
            /*  5. */ { "Jitka" },
            /*  6. */ { "Mikuláš" },
            /*  7. */ { "Ambrož", "Benjamín" },
            /*  8. */ { "Květoslava" },
            /*  9. */ { "Vratislav" },
            /* 10. */ { "Julie" },
            /* 11. */ { "Dana" },
            /* 12. */ { "Simona" },
            /* 13. */ { "Lucie" },
            /* 14. */ { "Lýdie" },
            /* 15. */ { "Radana", "Radan" },
            /* 16. */ { "Albína" }, 
            /* 17. */ { "Daniel" }, 
            /* 18. */ { "Miloslav" },
            /* 19. */ { "Ester" },
            /* 20. */ { "Dagmar" },
            /* 21. */ { "Natálie" },
            /* 22. */ { "Šimon" },
            /* 23. */ { "Vlasta" },
            /* 24. */ { "Adam", "Eva" },
            /* 25. */ { },
            /* 26. */ { },
            /* 27. */ { "Žaneta" },
            /* 28. */ { "Bohumila" },
            /* 29. */ { "Judita" },
            /* 30. */ { "David" },
            /* 31. */ { "Silvestr" }
        }
    };     
    
}
