/*
 * SeznamObdobi.java
 *
 * Created on 31. říjen 2003, 18:51
 */

package com.bistudio.dochazka;

/**
 *
 * @author  hlavac
 */
public class SeznamTerminu {
    
    int low = 0;
    int high = 0;
    boolean[] je = null;
    
    public SeznamTerminu(int low, int high) {
        this.low = low;
        this.high = high;
        je = new boolean[high-low+1];
    }
    
    public void yes(int i) {
        je[i-low] = true;
    }
    public void no(int i) {
        je[i-low] = false;
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        boolean prvni = true;
        for (int i=0; i<je.length; i++) {
            int start = i;
            while (i<je.length&&je[i]) {
                i++;
            }
            int count = i-start;
            if (count>0) {
                if (prvni) {
                    prvni = false;
                } else {
                    sb.append(", ");
                }
                if (count == 1) {
                    sb.append(start+low);
                    sb.append('.');
                } else {
                    sb.append(start+low);
                    sb.append(". - ");
                    sb.append(start+count-1+low);
                    sb.append('.');
                }
            }
        }
        return sb.toString();
    }
    
    
    public static void main(String[] args) {
        SeznamTerminu s = new SeznamTerminu(1,31);
        s.yes(1);
        s.yes(5);
        s.yes(6);
        s.yes(10);
        s.yes(11);
        s.yes(12);
        s.yes(13);
        s.yes(31);
        System.out.println(s);
    }
}
