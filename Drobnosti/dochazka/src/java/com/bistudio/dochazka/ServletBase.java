/*
 * ServletBase.java
 *
 * Created on 1. září 2003, 16:55
 */

package com.bistudio.dochazka;

import javax.servlet.*;
import javax.servlet.http.*;
/**
 *
 * @author  hlavac
 */
public class ServletBase extends HttpServlet {
    
    /** Creates a new instance of ServletBase */
    public ServletBase() {
    }
    
    public static String html(String source) {
        return CommonBase.htmlSafe(source);
    }
}
