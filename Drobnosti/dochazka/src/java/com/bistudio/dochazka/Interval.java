/*
 * Interval.java
 *
 * Created on 9. září 2003, 13:10
 */

package com.bistudio.dochazka;

import java.io.Serializable;
/**
 *
 * @author  hlavac
 */
public class Interval implements Serializable {

    private static Cas zero_time = new Cas("0:00");
    
    private Cas start;
    private Cas end;
    private TypIntervalu type;
    
    /** Creates a new instance of Interval */
    public Interval() {
    }
    
    public Interval(Cas start, Cas end, TypIntervalu type) {
        this.start = start;
        this.end = end;
        this.type = type;
    }
    
    public Interval(String start, String end, TypIntervalu type) {
        this(new Cas(start),  new Cas(end), type);
    }
    
    public void setStart(Cas c) {
        this.start = c;
    }
    
    public Cas getStart() {
        return this.start;
    }
    
    public void setEnd(Cas c) {
        this.end = c;
    }
    
    public Cas getEnd() {
        return this.end;
    }
    
    public int getMinutes() {
        return this.start.diff(this.end);
    }
    
    public Cas getDuration() {
        return Cas.sub(this.end,this.start);
    }
    
    public Cas getWorkDuration() {
        if (type.countsAsWork()) {
            return getDuration();
        } else {
            return zero_time;
        }
    }
    
    public TypIntervalu getType() {
        return this.type;
    }
    
    
    public String toString() {
        StringBuffer sb = new StringBuffer(11);
        sb.append(type.getDescription());
        sb.append(" ");
        if (this.start != null) {
            sb.append(this.start);
        } else {
            sb.append('?');
        }
        sb.append('-');
        if (this.end != null) {
            sb.append(this.end);
        } else {
            sb.append('?');
        }
        return sb.toString();
    }
    
}
