/*
 * BasicAuthenticationInfo.java
 *
 * Created on 29. srpen 2003, 15:56
 */

package com.bistudio.dochazka;

/**
 *
 * @author  hlavac
 */
public class BasicAuthenticationInfo {
    

    public static void main(String[] args) {
    }
    
    String username;
    String password;
    /** Creates a new instance of BasicAuthenticationInfo */
    public BasicAuthenticationInfo(String username, String password) {
        this.username = username;
        this.password = password;
    }
    /** Creates a new instance of BasicAuthenticationInfo */
    public BasicAuthenticationInfo(javax.servlet.ServletRequest req) {
        if (req instanceof javax.servlet.http.HttpServletRequest) {
            javax.servlet.http.HttpServletRequest request = (javax.servlet.http.HttpServletRequest)req;
            String auth_header = request.getHeader("Authorization");
            if ((auth_header != null) && (auth_header.startsWith("Basic "))) {
                String encoded = new String(Base64.decode(auth_header.substring(6).getBytes()));
                int split = encoded.indexOf(':');
                this.username = encoded.substring(0,split);
                this.password = encoded.substring(split+1);
            } else {
                this.username = null;
                this.password = null;
            }
        } else {
            this.username = null;
            this.password = null;
        }
    }
    
    public String getUsername() {
        return username;
    }
    
    public String getPassword() {
        return password;
    }
}
