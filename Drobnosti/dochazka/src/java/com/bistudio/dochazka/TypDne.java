/*
 * TypDne.java
 *
 * Created on 6. říjen 2003, 15:37
 */

package com.bistudio.dochazka;

/**
 *
 * @author  hlavac
 */
public abstract class TypDne {

    // class variables
    private static final Cas OSM_HODIN = new Cas("8:00");
    private static final Cas CTYRI_HODINY = new Cas("4:00");
    private static final TypDne[] typy = new TypDne[12];
    static {
        typy[0] = new TypDnePrazdny();
        typy[1] = new TypDnePritomen();
        typy[2] = new TypDneDovolena();
        typy[3] = new TypDneDovolenaPul();
        typy[4] = new TypDneNemoc();
        typy[5] = new TypDneNahradniVolno();
        typy[6] = new TypDnePlaceneVolno();
        typy[7] = new TypDneNeplati();
        typy[8] = new TypDneSvatek();
        typy[9] = new TypDneVikend();
        typy[10] = new TypDneNeplaceneVolno();
        typy[11] = new TypDneBonus();
    }

    // instance variables
    private String kod = null;
    private String popis = null;
    
    
    /** Creates a new instance of TypDne */
    public TypDne(String kod, String popis) {
        this.kod = kod;
        this.popis = popis;
    }
    
    public abstract void zapocti(StrankaDochazky.Radek radek, UhrnMesice sem);

    public String getCode() {
        return this.kod;
    }
    
    public String getDescription() {
        return this.popis;
    }
    
    public static TypDne[] getDayTypes() {
        return typy;
    }
    
    public static TypDne getDayType(String code) {
        for (int i=0; i<typy.length; i++) {
            if (typy[i].kod.equals(code)) {
                return typy[i];
            }
        }
        throw new IllegalArgumentException("Bad day type code");
    }
    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.kod);
        sb.append(" (");
        sb.append(this.popis);
        sb.append(")");
        return sb.toString();
    }
    
    public String toForm(String selectname, int day) {
        StringBuilder sb = new StringBuilder();
        sb.append("<select name='");
        sb.append(html(selectname));
        sb.append("' onchange='handleDayChange(");
        sb.append(day);
        sb.append(")'>");
        for (int i=0; i<typy.length; i++) {
            sb.append("<option value='");
            sb.append(html(typy[i].kod));
            sb.append("'");
            if (this == typy[i]) {
                sb.append(" selected='selected'");
            }
            sb.append('>');
            sb.append(html(typy[i].popis));
            sb.append("</option>");
        }
        sb.append("</select>");
        return sb.toString();
    }
    
    private static String html(String text) {
        return CommonBase.htmlSafe(text);
    }

    // ------------- typy dnů --------------
    
    public static class TypDnePrazdny extends TypDne {
        public TypDnePrazdny() {
            super("","");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
            sem.neplatny();
        }
    }
    
    public static class TypDnePritomen extends TypDne {
        public TypDnePritomen() {
            super("1","přítomen");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
//            sem.pridejUvazek(OSM_HODIN);
            sem.pridejPuldnyPrace(2);
            sem.pridejPraci(radek.getWorkDuration());
        }
    }
    
    public static class TypDneDovolena extends TypDne {
        public TypDneDovolena() {
            super("D","dovolená");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
//            sem.pridejUvazek(OSM_HODIN);
            sem.pridejDovolenou(OSM_HODIN);
            sem.pridejPuldnyDovolene(2);
            sem.pridejPraci(radek.getWorkDuration());
            sem.pridejTerminDovolene(radek.getDay().getDay()+1);
        }
    }

    public static class TypDneDovolenaPul extends TypDne {
        public TypDneDovolenaPul() {
            super("d","1/2den dovol.");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
            sem.pridejDovolenou(CTYRI_HODINY);
//            sem.pridejUvazek(OSM_HODIN);
            sem.pridejPuldnyPrace(1);
            sem.pridejPuldnyDovolene(1);
            sem.pridejPraci(radek.getWorkDuration());
            sem.pridejTerminDovolene(radek.getDay().getDay()+1);
        }
    }
    
    public static class TypDneNemoc extends TypDne {
        public TypDneNemoc() {
            super("N","nemocen");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
            sem.pridejNemoc(OSM_HODIN);
//            sem.pridejUvazek(OSM_HODIN);
            sem.pridejPuldnyNemoci(2);
            sem.pridejTerminNemoci(radek.getDay().getDay()+1);
        }
    }

    public static class TypDneNahradniVolno extends TypDne {
        public TypDneNahradniVolno() {
            super("v","náhradní volno");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
            sem.pridejUvazek(OSM_HODIN);
            sem.pridejPraci(OSM_HODIN);
            sem.pridejPuldnyPrace(2);
            //sem.pridejPlaceneVolno(OSM_HODIN);
            //sem.pridejPuldnyPlacenehoVolna(2);
        }
    }

    public static class TypDnePlaceneVolno extends TypDne {
        public TypDnePlaceneVolno() {
            super("V","placené volno");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
            sem.pridejUvazek(OSM_HODIN);
            //sem.pridejPraci(OSM_HODIN);
            //sem.pridejPuldnyPrace(2);
            sem.pridejPlaceneVolno(OSM_HODIN);
            sem.pridejPuldnyPlacenehoVolna(2);
        }
    }
    
    public static class TypDneNeplati extends TypDne {
        public TypDneNeplati() {
            super("-","neplatí");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
        }
    }

        public static class TypDneNeplaceneVolno extends TypDne {
        public TypDneNeplaceneVolno() {
            super("n","neplacené volno");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
            sem.pridejNeplaceneVolno(OSM_HODIN);
            sem.pridejPuldnyNeplacenehoVolna(2);
            sem.pridejTerminNeplaceneVolno(radek.getDay().getDay()+1);
        }
    }

    public static class TypDneSvatek extends TypDne {
        public TypDneSvatek() {
            super("S","svátek");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
            sem.pridejSvatek();
            sem.pridejPraciVikend(radek.getWorkDuration());
        }
    }

    public static class TypDneVikend extends TypDne {
        public TypDneVikend() {
            super("s","víkend");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
            sem.pridejPraciVikend(radek.getWorkDuration());
        }
    }

    public static class TypDneBonus extends TypDne {
        public TypDneBonus() {
            super("B","bonus");
        }
        public void zapocti(StrankaDochazky.Radek radek, com.bistudio.dochazka.UhrnMesice sem) {
            //sem.pridejUvazek(OSM_HODIN);
            //sem.pridejPraci(OSM_HODIN);
            //sem.pridejPuldnyPrace(2);
            //sem.pridejPlaceneVolno(OSM_HODIN);
            //sem.pridejPuldnyPlacenehoVolna(2);
            sem.pridejBonus(OSM_HODIN);
            sem.pridejPuldnyBonus(2);
            sem.pridejTerminBonus(radek.getDay().getDay()+1);
        }
    }

}
