/*
 * TypIntervalu.java
 *
 * Created on 25. září 2003, 16:45
 */

package com.bistudio.dochazka;

import java.util.*;

/**
 *
 * @author  hlavac
 */
public abstract class TypIntervalu {

    private String kod = null;
    private String popis = null;
    private static final TypIntervalu[] typy;
    static {
        typy = new  TypIntervalu[4];
        typy[0] = new IntervalPrazdny();
        typy[1] = new IntervalPrace();
        typy[2] = new IntervalLekar();
        typy[3] = new IntervalExterniPrace();
    }
    
    
    protected TypIntervalu(String kod, String popis) {
        this.kod = kod;
        this.popis = popis;
    }
    
    public String getCode() {
        return this.kod;
    }
    
    public String getDescription() {
        return this.popis;
    }
    
    public static TypIntervalu[] getIntervalTypes() {
        return typy;
    }
    
    public static TypIntervalu getIntervalType(String kod) {
        for (int i=0; i<typy.length; i++) {
            if (typy[i].kod.equals(kod)) return typy[i];
        }
        throw new IllegalArgumentException("Unknown interval code: "+kod);
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(kod);
        sb.append(" (");
        sb.append(popis);
        sb.append(")");
        return sb.toString();
    }

    public String toForm(String selectname, int day) {
        StringBuffer sb = new StringBuffer();
        sb.append("<select name='");
        sb.append(html(selectname));
        sb.append("' onchange='handleDayChange("+day+")'>");
        for (int i=0; i<typy.length; i++) {
            sb.append("<option value='");
            sb.append(html(typy[i].kod));
            sb.append("'");
            if (this == typy[i]) {
                sb.append(" selected='selected'");
            }
            sb.append(">");
            sb.append(html(typy[i].popis));
            sb.append("</option>");
        }
        sb.append("</select>");
        return sb.toString();
    }
    
    protected static String html(String text) {
        return CommonBase.htmlSafe(text);
    }
    
    
    
    // defaultní implementace určená k dědění - přidá interval k práci
    public void zapocti(com.bistudio.dochazka.Cas zacatek, com.bistudio.dochazka.Cas konec, com.bistudio.dochazka.UhrnMesice sem) {
        // musí být platný začátek i konec, a začátek < konec
        if ((zacatek == null)||(konec==null)||(zacatek.compareTo(konec)>=0)) {
            // neplatný interval.
            sem.neplatny();
        } else {
            // platný interval. Připočíst jeho délku k délce práce.
            Cas delka = Cas.sub(konec,zacatek);
            sem.pridejPraci( delka );
        }
    }
    
    
    public boolean countsAsWork() {
        return true;
    }
    
    /*-------- typy intervalů ----- */
    public static class IntervalPrazdny extends TypIntervalu {
        public IntervalPrazdny() {
            super("","");
        }

        public void zapocti(com.bistudio.dochazka.Cas zacatek, com.bistudio.dochazka.Cas konec, com.bistudio.dochazka.UhrnMesice sem) {
        }
        
        public boolean countsAsWork() {
            return false;
        }
    }
    
    public static class IntervalPrace extends TypIntervalu {
        public IntervalPrace() {
            super("P","práce");
        }
    }

    public static class IntervalLekar extends TypIntervalu {
        public IntervalLekar() {
            super("L","lékař");
        }
    }
    
    public static class IntervalExterniPrace extends TypIntervalu {
        public IntervalExterniPrace() {
            super("X","externí práce");
        }
    }
}
