/*
 * Database.java
 *
 * Created on 1. září 2003, 17:31
 */

package com.bistudio.dochazka;

import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;

/**
 *
 * @author  hlavac
 */
public class DatabaseDochazka {
    
    File root = null;
    SmbpasswdFile smbpasswd = null;
    
    public DatabaseDochazka(java.io.File root, java.io.File smbpasswd_file) throws IOException {
        this.root = root.getCanonicalFile();
        if (!this.root.isDirectory()) throw new IOException("Root directory not accessible");
        if (!smbpasswd_file.isFile()) throw new IOException("Smbpasswd file not accessible");
        this.smbpasswd = new SmbpasswdFile(smbpasswd_file);
    }
    
    public UserRecordV1 loadUser(String login) throws IOException {
        UserRecordV1 result = null;
        File filename = new File(root,"user_"+login+".xml");
        if (filename.isFile()) {
            FileInputStream fis = new FileInputStream(filename);
            try {
                XMLDecoder decoder = new XMLDecoder(fis);
                try {
                    result = (UserRecordV1)decoder.readObject();
                } finally {
                    decoder.close();
                }
            } finally {
                fis.close();
            }
        } else {
            // neexistuje soubor s daty o uživateli. Je třeba ověřit platnost uživ. jména!
            try {
                if (smbpasswd.checkPassword(login, null)) {
                    result = new UserRecordV1(login,"<nový uživatel>");
                    if (login.equals("hlavac")) {
                        result.grantRole("dochazka.admin");
                    }
                    saveUser(result);
                }
            } catch (java.security.NoSuchAlgorithmException e) { }
        }
        return result;
    }
    
    public void saveUser(UserRecordV1 u) throws IOException {
        String login = u.getLogin();
        if ((login == null)||(login.length()==0)) throw new IllegalArgumentException("bad login");
        File filename = new File(root,"user_"+login+".xml");
        FileOutputStream fos = new FileOutputStream(filename);
        try {
            XMLEncoder encoder = new XMLEncoder(fos);
            encoder.writeObject(u);
            encoder.flush();
            encoder.close();
        } finally {
            fos.flush();
            fos.close();
        }
    }
}
