/*
 * UserRecordV1.java
 *
 * Created on 3. září 2003, 11:32
 */

package com.bistudio.dochazka;

import java.util.StringTokenizer;
import java.beans.XMLEncoder;
import java.beans.XMLDecoder;


/**
 *
 * @author  hlavac
 */
public class UserRecordV1 implements java.io.Serializable {
    
    private String login = "";
    private String real_name = "";
    private java.util.Properties properties = new java.util.Properties();
    /** Creates a new instance of UserRecordV1 */
    
    public UserRecordV1() {
        login = "";
        real_name = "";
        properties = new java.util.Properties();
    }
    
    public UserRecordV1(String login, String name) {
        this.login = login.toLowerCase();
        this.real_name = name;
        properties = new java.util.Properties();
    }
    
    public void setLogin(String login) {
        this.login = login.toLowerCase();
    }
    
    public String getLogin() {
        return login;
    }
    
    public String getRealName() {
        return real_name;
    }
    
    public void setRealName(String name) {
        real_name = name;
    }

    public java.util.Properties getProperties() {
        return properties;
    }
    
    public void setProperties(java.util.Properties prop) {
        if (prop == null) throw new IllegalArgumentException();
        properties = prop;
    }
    
    public synchronized boolean hasRole(String role) {
        String roles = " "+properties.getProperty("roles","")+" ";
        String lookfor = " "+role+" ";
        return roles.indexOf(lookfor) != -1;
    }
    
    public synchronized void grantRole(String role) {
        String roles = properties.getProperty("roles","");
        String bound = " "+roles+" ";
        String lookfor = " "+role+" ";
        if (bound.indexOf(lookfor) == -1) {
            if (roles.length()==0) {
                properties.setProperty("roles", role);
            } else {
                properties.setProperty("roles", roles+" "+role);
            }
        }
    }
    
    public synchronized void revokeRole(String role) {
        String roles = properties.getProperty("roles","");
        String bound = " "+roles+" ";
        String lookfor = " "+role+" ";
        int pos = bound.indexOf(lookfor);
        if (pos != -1) {
            if (pos == 0) {
                properties.setProperty("roles", roles.substring(pos+role.length()+1));
            } else {
                properties.setProperty("roles", roles.substring(0,pos)+roles.substring(pos+role.length()+1));
            }
        }
    }
    
    
}
