#include <stdio.h>
//#include <windows.h>
#include <Es/Common/win.h>
#include <direct.h>
#include <io.h>
#include <fcntl.h>

#include <Es/Framework/consoleBase.h>
//#include <class/consoleBase.cpp>
#include <Es/Containers/array.hpp>
#include <Es/Strings/rString.hpp>
#include <Es/Files/filenames.hpp>
//#include "fileMap.hpp"
//#include <El/QStream/fileMapping.hpp>
#include <El/QStream/qGzStream.hpp>

#define LZMA_API_STATIC
#include <El/QStream/qLzmaStream.hpp>

class CRCCalculator
{
  unsigned long _table[256];
  unsigned long _crc;

  void InitTable();

public:
  //! Constructor
  CRCCalculator();
  //! Calculate CRC of memory block
  unsigned long CRC( const void *data, int len );
  //! Calculate CRC of memory block
  /*!
  \name Incremental interface
  \patch_internal 1.11 Date 8/1/2001 by Ondra
  - New: Incremental CRC interface.
  */
  //@{
  //! initialize
  void Reset();
  //! add memory block
  void Add(const void *data, int len);
  //! add signle character
  void Add(char c);
  //! get result of all Add operations since Reset()
  unsigned long GetResult() const {return ~_crc;}
  //}@

};

CRCCalculator::CRCCalculator()
{
  InitTable();
}

void CRCCalculator::Reset()
{
  _crc = ~0;       // preload shift register, per CRC-32 spec
}

void CRCCalculator::Add(const void *data, int len)
{
  const unsigned char *p = (const unsigned char *)data;
  unsigned long crc = _crc;
  for( ; len>0; p++,len-- )
  {
    crc = (crc<<8) ^ _table[(crc>>24) ^ *p];
  }
  _crc=crc;
}

void CRCCalculator::Add(char c)
{
  _crc = (_crc<<8) ^ _table[(_crc>>24) ^ c];
}

unsigned long CRCCalculator::CRC( const void *data, int len )
{
  const unsigned char *p = (const unsigned char *)data;
  unsigned long crc = ~0;       // preload shift register, per CRC-32 spec
  for( ; len>0; p++,len-- )
  {
    crc = (crc<<8) ^ _table[(crc>>24) ^ *p];
  }
  return ~crc; // transmit complement, per CRC-32 spec
}

void CRCCalculator::InitTable()
{
  const int CRCPoly=0x04c11db7; // AUTODIN II, Ethernet, & FDDI
  for( int i=0; i<256; i++ )
  {
    unsigned long c;
    int j;
    for (c = i << 24, j = 8; j > 0; --j)
    {
      c = c & 0x80000000 ? (c << 1) ^ CRCPoly : (c << 1);
    }
    _table[i] = c;
  }
}

CRCCalculator cc;

static unsigned int GetFileCRC(const char *fullname)
{
  QIFStream *inFile = new QIFStream; inFile->open(fullname);
  QIStream *in = inFile;
  const char *ext = GetFileExt(fullname);
  if (ext && stricmp(ext, ".gz")==0)
  { //we should use GZ stream
    QIStream *inFile = in;
    in = new QIGzStream(inFile);
  }
  else if (ext && stricmp(ext, ".xz")==0)
  { //we should use XZ stream
    QIStream *inFile = in;
    in = new QILzmaStream(inFile);
  }
  if (in)
  {
    cc.Reset();
    int   nBytes;
    const int COPY_BUFF_SIZE = 65536;
    TCHAR buff[COPY_BUFF_SIZE];
    bool  copyFinished = false;
    for(;;)
    {
      nBytes = in->read(buff,COPY_BUFF_SIZE);
      if (nBytes>0) cc.Add(buff,nBytes);
      else break;
    };
    delete in;
  }
  else return 0;
  return cc.GetResult();
}

static void CRCCheckFile(const char *fullname)
{
  printf("%s %08x\n",fullname,GetFileCRC(fullname));
}

static void LoadInput()
{
	FILE *f = stdin;
	for(;;)
	{
		char line[1024];
		char *ok = fgets(line,sizeof(line),f);
		if (!ok) break;
		if (*line && line[strlen(line)-1]=='\n') line[strlen(line)-1]=0;
		char *sp = strchr(line,' ');
		if (!sp) continue;
		*sp++=0;
		unsigned int infoCrc = strtoul(sp,NULL,16);

		//FileBufferMapped file(line);
		//unsigned int crc = cc.CRC((unsigned char *)file.GetData(),file.GetSize());
    unsigned int crc = GetFileCRC(line);
		if (crc!=infoCrc)
		{
			printf("%s %08x!=%08x\n",line,infoCrc,crc);
			wasError = true;
		}
	}
}

static int CRCCheck( const char *path)
{
	char pathwild[1024];
	strcpy(pathwild,path);
	TerminateBy(pathwild,'\\');
	strcat(pathwild,"*.*");
	_finddata_t data;
	long h = _findfirst(pathwild,&data);
	if (h!=-1)
	{
		do
		{
			if (!strcmp(data.name,".") || !strcmp(data.name,".."))
			{
				continue;
			}
			char fullname[1024];
			strcpy(fullname,path);
			TerminateBy(fullname,'\\');
			strcat(fullname,data.name);
			if (data.attrib&_A_SUBDIR)
			{
				CRCCheck(fullname);
			}
			else
			{
				CRCCheckFile(fullname);
			}
		} while( _findnext(h,&data)==0);
		_findclose(h);
	}
	return 0;
}

static int CRCCheck(const char *datPath,FILE *dat)
{
	// process dat file
	for(;;)
	{
		char line[1024];
		char *ok = fgets(line,sizeof(line),dat);
		if (!ok) break;
		if (*line && line[strlen(line)-1]=='\n') line[strlen(line)-1]=0;
		// check if it is file or folder name
		char fullName[1024];
		strcpy(fullName,datPath);
		//TerminateBy(fullName,'\\');
		const char *beg = strchr(line,':');
		if (!beg) continue;
		beg++;
		const char *end = strchr(beg,':');
		if (!end) continue;
		char name[1024];
		strncpy(name,beg,end-beg);
		name[end-beg] = 0;
		strcpy(GetFilenameExt(fullName),name);

		if (toupper(*line)=='D')
		{
			fprintf(stderr,"Processing folder %s\n",line);
			CRCCheck(fullName);
		}
		else if (toupper(*line)=='F')
		{
			fprintf(stderr,"Processing file %s\n",line);
			CRCCheckFile(fullName);
		}
	}
	return 0;

}

int consoleMain(int argc, const char *argv[])
{
	// scan all folders submitted as argumetns and print CRC to stdout
	if (argc<2)
	{
		printf
		(
			"Usage: CRCCheck {pattern}\n"
			"   or: CRCCheck -check\n"
			"\n"
			"\tCRCCheck -check:    Check CRC, use stdin as CRC and filename input.\n"
			"\tCRCCheck {pattern}: Pattern may be folder name or filename of dat file suitable for SQSetup\n"
			"\t\tOutput is sent to stdout.\n"
		);
		return 1;
	}
	while (argc>1)
	{
		const char *arg = argv[1];
		if (*arg=='-')
		{
			arg++;
			if (!strcmpi(arg,"check"))
			{
				LoadInput();
				return 0;
			}
		}
		else
		{
			FILE *f = fopen(arg,"r");
			if (f)
			{
				int ok = CRCCheck(arg,f);
				fclose(f);
			}
			else
			{
				int ok = CRCCheck(arg);
				if (ok)
				{
					return 1;
				}
			}
		}
		argc--;
		argv++;
	}
	return 0;
}
