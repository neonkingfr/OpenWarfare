// MapOrder.cpp : Defines the entry point for the console application.
//

#include <Es/essencepch.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include <Es/Files/filenames.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Strings/RString.hpp>
#include <Es/Algorithms/QSort.hpp>
#include <Es/Framework/consoleBase.h>

struct MapSymbol
{
	int order;
	int section;
	int addr;
	RString name;
	RString module;

	MapSymbol()
	{
		// default order: end of list
		order = 10000;
	}
	bool operator == ( const MapSymbol &w) const
	{
		return w.name==name;
	}
};

TypeIsMovable(MapSymbol)

static int CmpSym( const MapSymbol *s1, const MapSymbol *s2)
{
	int d = s1->order-s2->order;
	if (d) return d;
	d = s1->section-s2->section;
	if (d) return d;
	d = s1->addr-s2->addr;
	if (d) return d;
	d = strcmp(s1->module,s2->module);
	if (d) return d;
	return strcmp(s1->name,s2->name);
}

static int CmpSymAddr( const MapSymbol *s1, const MapSymbol *s2)
{
	int d = s1->section-s2->section;
	if (d) return d;
	d = s1->addr-s2->addr;
	return d;
}

int consoleMain(int argc, const char* argv[])
{
	if (argc!=2)
	{
		printf
		(
			"Usage: MapOrder source.map\n"
			"  Result is generated to source.prf\n"
			"  Additional ordering info may be specified in source.ord\n"
			"\n"
			".ord directives:\n\n"
			"  #rem [Text]             remark\n"
			"  #order <number>[+]      specify order, toggle autoincrement on\n"
			"  #exact                  input is exact match\n"
			"  #substr                 input is substring match\n"
			"  #function               input is function name\n"
			"  #module                 input is module name\n"
			"  #reportsections         dump section boundary symbols .sec file\n"
			"\n"
			"On the beginning of .ord file following lines are assumed:\n"
			"  #order 0+\n"
			"  #exact\n"
			"  #function\n"
		);
		return 1;
	}
	// check map file name
	const char *src = argv[1];
	char tgt[512];
	char ordName[512];
	strcpy(tgt,src);
	strcpy(ordName,src);
	// simulate output from working set tuner
	strcpy(GetFileExt(tgt),".prf");
	strcpy(GetFileExt(ordName),".ord");
	// parse map file


	FILE *map = fopen(src,"r");
	FILE *prf = fopen(tgt,"w");
	FILE *ord = fopen(ordName,"r");
	if (!map || !prf) return 1;

	FindArray<MapSymbol> syms;

	for(;;)
	{ // search for first nonempty line
		char line[1024];
		char *ret = fgets(line,sizeof(line),map);
		if (!ret) break;
		// remove terminating EOL
		if (*line && line[strlen(line)-1]=='\n') line[strlen(line)-1]=0;
		if (strstr(line,"Static symbols")) break;
		if( !strchr(line,':') ) continue;
		if( line[0]==0 ) continue;
		// skip some typical lines
		if (strstr(line,"Preferred load address")) continue;
		if (strstr(line,"entry point at")) continue;
		// scan line for: <rel addr> <name> <abs addr> <......>
		const char *c=line;
		while( isspace(*c) ) c++;
		int logSection=strtoul(c,(char **)&c,16);
		if (logSection==0 || logSection>16) continue;
		if (logSection!=1) continue;
		while( *c==':' || isspace(*c) ) c++;
		int logAddress=strtoul(c,(char **)&c,16);
		while( isspace(*c) ) c++;
		char name[1024];
		char *d=name;
		while( !isspace(*c) && *c ) *d++=*c++; // get name
		*d=0;
		while( isspace(*c) ) c++;
		int physAddress=strtoul(c,(char **)&c,16);
		if( *name==0 ) continue; // no name
		if (isdigit(*name)) continue; // no valid name
		// we can ignore rest of the line
		// store name/address information
		c++; // function/module information follows
		// skip symbol kind
		// c should be ' ' or 'f' now
		if (*c!=' ' && *c!='f')
		{
			printf("Strange symbol type\n");
			printf("  in '%s'",line);
		}
		c++;
		// c should be ' ' now
		if (*c!=' ')
		{
		  printf("Space expected\n");
			printf("  in '%s'",line);
		}
		c++;
		// c should be ' ' or 'i' now
		if (*c!=' ' && *c!='i')
		{
			printf("Strange inline type\n");
			printf("  in '%s'",line);
		}
		c++;
		// c should be ' ' now
		if (*c!=' ')
		{
		  printf("Space expected\n");
			printf("  in '%s'",line);
		}
		c++;
		// c should be ' ' now
		

		MapSymbol &info=syms.Append();
		if (name[0]=='_') info.name = name+1;
		else info.name = name;
		info.module = c;
		info.section = logSection;
		info.addr = logAddress;
	}

	bool report = false;
	if (ord)
	{
		printf("Ordering file found.\n");
		// parse ordering file info
		int order = 0;
		bool autoOrder = true;
		bool substringSearch = false;
		bool moduleSearch = false;
		for(;;)
		{ // search for first nonempty line
			char line[1024];
			char *ret = fgets(line,sizeof(line),ord);
			if (!ret) break;

			if (*line && line[strlen(line)-1]=='\n') line[strlen(line)-1]=0;
			if (!*line) continue;
			if (*line=='#')
			{
				// directive follows
				char *space = strchr(line,' ');
				if (!space) space = line+strlen(line); // no argument
				int lenDir = space-line;
				if (!strncmp(line,"#rem",lenDir))
				{
					// remark
				}
				else if (!strncmp(line,"#exact",lenDir))
				{
					substringSearch = false;
				}
				else if (!strncmp(line,"#substr",lenDir))
				{
					substringSearch = true;
				}
				else if (!strncmp(line,"#function",lenDir))
				{
					moduleSearch = false;
				}
				else if (!strncmp(line,"#module",lenDir))
				{
					moduleSearch = true;
				}
				else if (!strncmp(line,"#reportsections",lenDir))
				{
					report = true;
				}
				else if (!strncmp(line,"#order",lenDir))
				{
					// order specified
					if (!*space)
					{
						fprintf(stderr,"No argument in '%s'\n",line);
						continue; // no argument
					}
					space++;
					order = strtol(space,&space,10);
					autoOrder = (*space=='+');
				}
				else
				{
					fprintf(stderr,"Unknown directive '%s'",line);
				}
				continue; // unknown directiove
			}
			// use substring search or exact match
			if (substringSearch)
			{
				// process substring search
				for (int index=0; index<syms.Size(); index++)
				{
					const char *s = moduleSearch ? syms[index].module : syms[index].name;
					if (strstr(s,line))
					{
						syms[index].order = order;
					}
				}
			}
			else
			{
				// process exact function match
				if (!moduleSearch)
				{
					MapSymbol key;
					key.name = line;
					int index = syms.Find(key);
					if (index>=0)
					{
						syms[index].order = order;
					}
				}
				else
				{
					for (int index=0; index<syms.Size(); index++)
					{
						const char *s = moduleSearch ? syms[index].module : syms[index].name;
						if (strcmp(s,line))
						{
							syms[index].order = order;
							break;
						}
					}
				}
			}
			if (autoOrder) order++;
		}
		fclose(ord);
	}
	fclose(map);
	QSort(syms.Data(),syms.Size(),CmpSymAddr);
	
	// if the are multiple function binary identical, they can be merge into one
	// in such case we want to place them base on their first ordering
	for (int i=0; i<syms.Size(); i++)
	{
		MapSymbol &sym0 = syms[i];
		int j;
		int minOrder = sym0.order;
		for (j=i+1; j<syms.Size(); j++)
		{
		  const MapSymbol &symJ = syms[j];
		  if (symJ.section!=sym0.section) break;
		  if (symJ.addr!=sym0.addr) break;
		  if (symJ.order<minOrder) minOrder = symJ.order;
		}
		for (int k=i; k<j; k++)
		{
		  syms[k].order = minOrder;
		}
		i = j-1;
	}


	QSort(syms.Data(),syms.Size(),CmpSym);
	// dump symbols
	for (int i=0; i<syms.Size(); i++)
	{
		const MapSymbol &sym = syms[i];
		fprintf(prf,"%s\n",(const char *)sym.name);
	}
	fclose(prf);
	if (report)
	{
		char secName[512];
		strcpy(secName,src);
		strcpy(GetFileExt(secName),".sec");
		FILE *sec = fopen(secName,"w");
		if (!sec)
		{
			fprintf(stderr,"Cannot create file %s",sec);
			return 1;
		}
		int order = -1;
		for (int i=0; i<syms.Size(); i++)
		{
			const MapSymbol &sym = syms[i];
			if (sym.order>order)
			{
				order = sym.order;
				fprintf(sec,"%d %s\n",sym.order,(const char *)sym.name);
			}
		}
		fclose(sec);
	}

	return 0;
}

