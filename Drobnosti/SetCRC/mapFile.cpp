// Map File parsing

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "mapFile.hpp"

#include <Es/Algorithms/QSort.hpp>
#include <Es/Files/filenames.hpp>
#if REM
// typical map file looks like:
//...some other stuff...
//...followed by:
  Address         Publics by Value              Rva+Base     Lib:Object

 0001:00000000       ??_H@YGXPAXIHP6EX0@Z@Z     00401000 f i engGlide.obj
 0001:00000030       ??_I@YGXPAXIHP6EX0@Z@Z     00401030 f i engGlide.obj
 0001:00000070       ??0EngineGlide@@QAE@PAX0HHH_N@Z 00401070 f   engGlide.obj
#endif

static int CmpMaps( const MapInfo *f0, const MapInfo *f1 )
{
	return f0->physAddress-f1->physAddress;
}

static void GetLine( char *line, int size, FILE *f)
{
	char * l = line;
	char c = fgetc(f);
	while (c!=EOF && c!='\n')
	{
		if (size>1)
		{
			if (c!='\r')
			{
				*l++ = c;
				size--;
			}
		}
		c = fgetc(f);
	}
	if (size>0) *l=0;
}

void MapFile::ParseMapFile(const char *exeName)
{
	if( _map.Size()>0 ) return; // map file already parsed
	//QIFStream in;
	char sourceName[256];
	// use program name as given on command line
	strcpy(sourceName,exeName);
	strcpy(GetFileExt(sourceName),".map");

	strcpy(_name,sourceName);

	FILE *in = fopen(sourceName,"r");
	if (!in) return;
	char line[4096];
	for(;;)
	{

		char *ret = fgets(line,sizeof(line),in);
		if (!ret) break;
		// remove terminating EOL
		if (*line && line[strlen(line)-1]=='\n') line[strlen(line)-1]=0;
		if( feof(in) || ferror(in) ) return;
		if
		(
			!strcmpi(
				line,
				"  Address         Publics by Value              Rva+Base     Lib:Object"
			) ||
			!strcmpi(
				line,
				"  Address         Publics by Value              Rva+Base      Lib:Object"
			) ||
			!strcmpi(
				line,
        "  Address         Publics by Value              Rva+Base       Lib:Object"
			)
		)
		{
		  break;
		}
	}
	// 
	for(;;)
	{ // search for first nonempty line
		char *ret = fgets(line,sizeof(line),in);
		if (!ret) break;
		if( feof(in) || ferror(in) ) break;
		// remove terminating EOL
		if (*line && line[strlen(line)-1]=='\n') line[strlen(line)-1]=0;
		if( !strchr(line,':') ) continue;
		if( line[0]==0 ) continue;
		// scan line for: <rel addr> <name> <abs addr> <......>
		const char *c=line;
		while( isspace(*c) ) c++;
		int logSection=strtoul(c,(char **)&c,16);
		if (logSection<1 || logSection>8) continue;
		
		while( *c==':' || isspace(*c) ) c++;
		int logAddress=strtoul(c,(char **)&c,16);
		while( isspace(*c) ) c++;
		const char *begin = c;
		while( !isspace(*c) && *c ) c++; // get name
		RString name(begin, c - begin);
		while( isspace(*c) ) c++;
		int physAddress=strtoul(c,(char **)&c,16);
		if (name.GetLength() == 0) continue; // no name
		// we can ignore rest of the line
		// store name/address information
		MapInfo info;
		info.name = name;
		info.physAddress=physAddress;
		info.logAddress=logAddress;
		_map.Add(info);
	}
	fclose(in);
	QSort(_map.Data(),_map.Size(),CmpMaps);
}

const char *MapFile::MapDecorName( int address, MapAddressId id, int *lower )
{
	for( int i=0; i<_map.Size(); i++ )
	{
		//if( _map[i].*id>=address )
		if( _map[i].*id<=address && ( i>=_map.Size()-1 || _map[i+1].*id>address ) )
		{
			if( lower ) *lower=_map[i].*id;
			const char *name=_map[i].name;
			// extract class and function name
			// starting character is calling convention
			return name;
		}
	}
	return "???";
}

const char *MapFile::MapName( int address, MapAddressId id, int *lower )
{
	for( int i=0; i<_map.Size(); i++ )
	{
		//if( _map[i].*id>=address )
		if( _map[i].*id<=address && ( i>=_map.Size()-1 || _map[i+1].*id>address ) )
		{
			if( lower ) *lower=_map[i].*id;
			const char *name=_map[i].name;
			// extract class and function name
			// starting character is calling convention
			if( *name=='?' ) // C++ decorated name
			{
				const char *eName;
				static char resName[256];
				static char resClass[256];
				name++;
				if( *name=='?' )
				{
					name++;
					#if 0
					if( *name=='_' )
					{
						name++;
						strcpy(resName,"operator ");
						char *opName=strchr(resName,0);
						*opName++=*name++;
						*opName=0;
					}
					else
					#endif
					switch( *name )
					{
						case '0': strcpy(resName,"constructor");name++;break;
						case '1': strcpy(resName,"destructor");name++;break;
						case '2': strcpy(resName,"operator new");name++;break;
						case '3': strcpy(resName,"operator delete");name++;break;
						case '4': strcpy(resName,"operator =");name++;break;
						//case '4': strcpy(resName,"operator ->");name++;break;
						case 'R': strcpy(resName,"operator ()");name++;break;
						default:
							strcpy(resName,"###");
							char *opName=strchr(resName,0);
							*opName++=*name++;
							*opName=0;
						break;
					}
				}
				else
				{
					eName=strchr(name,'@');
					if( !eName ) return name;
					strcpy(resName,name);
					resName[eName-name]=0;
					name=eName+1;
				}
				eName=strchr(name,'@');
				if( !eName ) strcpy(resClass,"");
				else
				{
					strcpy(resClass,name);
					resClass[eName-name]=0;
				}
				if( *resClass )
				{
					strcat(resClass,"::");
					strcat(resClass,resName);
					return resClass;
				}
				else return resName;
			}
			else
			{
				return name+1;
			}
		}
	}
	return "???";
}

int MapFile::MinAddress( MapAddressId id ) const
{
	if( _map.Size()<=0 ) return 0;
	return _map[0].*id;
}

int MapFile::MaxAddress( MapAddressId id ) const 
{
	if( _map.Size()<=0 ) return 0x7fffffff;
	return _map[_map.Size()-1].*id;
}

int MapFile::Address( const char *name, MapAddressId id ) const 
{
	for( int i=0; i<_map.Size(); i++ )
	{
		if( !strcmp(_map[i].name,name) ) return _map[i].*id;
	}
	return 0;
}

