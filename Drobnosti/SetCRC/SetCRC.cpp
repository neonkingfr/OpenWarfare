#include <stdio.h>
#include <Es/Common/win.h>

#include <Es/Framework/consoleBase.h>
#include <Es/Framework/consoleBase.cpp>
#include <Es/Containers/array.hpp>
#include <Es/Files/filenames.hpp>

// to use SetCRC following preparations has to be made:
// application must contain two points:
// 'FSUM' at DWORD boundary used to adjust checksum
// 'FCRC' containing CRC of code segment

#define SUM_BASE_MAGIC 'SBAS'
#define SUM_SIZE_MAGIC 'SSIZ'

#define CRC_BASE_MAGIC 'CBAS'
#define CRC_SIZE_MAGIC 'CSIZ'

#define SUM_ADJUST_MAGIC 'FSUM'
#define CRC_VALUE_MAGIC  'FCRC'

#define CRC_VALUE_MAGIC  'FCRC'

#define ALLOCATOR_MAGIC  'FALC'

#define FADE_ADR_MAGIC 'FADR'

#define CODE_OFFSET 0x400000

class CRCCalculator
{
	unsigned long _table[256];
	unsigned long _crc;

	void InitTable();

	public:
	CRCCalculator();
	// sequential interface
	void Reset();
	void AddByte(unsigned char data);
	void AddBlock(unsigned char *data, int size);
	unsigned long CRC() const {return ~_crc;}
};

CRCCalculator::CRCCalculator()
{
	InitTable();
	Reset();
}

void CRCCalculator::Reset()
{
	_crc = ~0;       // preload shift register, per CRC-32 spec
}

void CRCCalculator::AddBlock(unsigned char *data, int size)
{
	while (--size>=0)
	{
		_crc = (_crc<<8) ^ _table[(_crc>>24) ^ *data++];
	}
}

void CRCCalculator::AddByte(unsigned char c)
{
	_crc = (_crc<<8) ^ _table[(_crc>>24) ^ c];
}

void CRCCalculator::InitTable()
{
	const int CRCPoly=0x04c11db7; // AUTODIN II, Ethernet, & FDDI
	for( int i=0; i<256; i++ )
	{
		unsigned long c;
		int j;
		for (c = i << 24, j = 8; j > 0; --j)
		{
			c = c & 0x80000000 ? (c << 1) ^ CRCPoly : (c << 1);
		}
		_table[i] = c;
	}
}

class ProcessPatch
{
	PROCESS_INFORMATION _pi;
	
	Temp<unsigned char> memCopy;
	FILE *_file;

  int loadOffset;

	public:
	// public access to section boundaries
	int codeStart;
	int codeSize;
	int dataStart;
	int dataSize;

	int totalStart;
	int totalSize;

  
	public:
	ProcessPatch();
	~ProcessPatch();

	void ReadMemory(const void *address, void *buf, int size) const;
  bool PatchAligned(void *address, const void *buf, int size, int alignment);
	void Patch(void *address, const void *buf, int size);
	void PatchDword(void *address, DWORD val);

	void *FindBlock(const void *address, int size, int start=0) const;
	void *FindDword(DWORD dword) const;
	bool TestByte(unsigned char value, int start) const;
	int CountByte(unsigned char value, int start) const;

	int Load(const char *name);
	void Unload();
};

ProcessPatch::ProcessPatch()
{
	_file = NULL;
  loadOffset = 0;
}

ProcessPatch::~ProcessPatch()
{
	if (_file)
	{
		fclose(_file);
		_file = NULL;
	}
}

int ProcessPatch::Load(const char *name)
{
	STARTUPINFO startupInfo;
	memset(&startupInfo,0,sizeof(startupInfo));
	startupInfo.cb = sizeof(startupInfo);
	if
	(
		!CreateProcess
		(
			name,NULL,NULL,NULL,false,
			CREATE_SUSPENDED,
			NULL,NULL,&startupInfo,&_pi
		)
	)
	{
		fprintf(stderr,"Cannot create process %s\n",name);
		return 1;
	}

	const int headerSize = 0x1000;
	char header[headerSize];

  loadOffset = 0;

	if (!::ReadProcessMemory(_pi.hProcess,(void *)CODE_OFFSET,&header,headerSize,NULL))
  {
    fprintf(stderr,"ReadProcessMemory failed\n",name);
    return 1;
  }

  PIMAGE_DOS_HEADER dosHdr = (PIMAGE_DOS_HEADER )header;
  PIMAGE_NT_HEADERS ntHdr = (PIMAGE_NT_HEADERS )(header + dosHdr->e_lfanew);

  loadOffset = ntHdr->OptionalHeader.SizeOfHeaders - 0x1000;

  printf("Load offset assumed %x\n",-loadOffset);

  if (dosHdr->e_magic!=IMAGE_DOS_SIGNATURE)
  {
    fprintf(stderr,"Signature mismatchd\n",name);
    return 1;
  }

	// check all sections
  PIMAGE_SECTION_HEADER pSection = IMAGE_FIRST_SECTION( ntHdr );

	// section 1 is code section
	codeStart = pSection[0].VirtualAddress;
	codeSize = pSection[0].SizeOfRawData;
	if (ntHdr->FileHeader.NumberOfSections>1)
	{
		// protect also section 2 (data section)
		dataStart = pSection[1].VirtualAddress;
		dataSize = pSection[1].SizeOfRawData;
	}

	totalStart = codeStart;
	totalSize = codeSize;
	/*
	if (dataStart==codeStart+codeSize)
	{
		totalSize += dataSize;
	}
	*/

	// copy whole process address space (code and data section)
	memCopy.Realloc(totalSize+headerSize);
	::ReadProcessMemory
	(
		_pi.hProcess,(void *)CODE_OFFSET,memCopy,totalSize+headerSize,NULL
	);
	Unload();

	_file = fopen(name,"rb+");
	if (!_file)
	{
    Sleep(2000);
	  _file = fopen(name,"rb+");
	  if (!_file)
    {
		  fprintf(stderr,"Cannot open file %s\n",name); //EACCES
		  return 1;
    }
	}

	return 0;
}

void ProcessPatch::Unload()
{
	// we read what we need - terminate process and close handle
	if (_pi.hProcess)
	{
		::TerminateProcess(_pi.hProcess,1);
		// wait until process really terminates
		::WaitForSingleObject(_pi.hProcess,60000);
		::CloseHandle(_pi.hProcess);
		::CloseHandle(_pi.hThread);
	}
	_pi.hProcess = NULL;
	_pi.hThread = NULL;
}

void ProcessPatch::ReadMemory(const void *address, void *buf, int size) const
{
	int offset = (int)address-CODE_OFFSET;
	memcpy(buf,memCopy+offset,size);
	//::ReadProcessMemory(_pi.hProcess,address,buf,size,NULL);
}

bool ProcessPatch::PatchAligned(void *address, const void *buf, int size, int alignment)
{
	int offset = (int)address-CODE_OFFSET;

	const unsigned char *origBuf = memCopy.Data()+offset;

  char old[256];
	fseek(_file,offset+alignment,SEEK_SET);
  fread(old,size,1,_file);
  if (!memcmp(old,origBuf,size))
  {
	  // update memory copy
	  memcpy(memCopy+offset,buf,size);
	  // update _file
	  fseek(_file,offset+alignment,SEEK_SET);

	  fwrite(buf,size,1,_file);
    return true;
  }
  return false;
}

void ProcessPatch::Patch(void *address, const void *buf, int size)
{
  if (PatchAligned(address,buf,size,loadOffset))
  {
    return;
  }

  // executables produced by VS 2010 are no longer aligned to 4KB, as /OPT:WIN98 is no longer supported
  for (int alignment=0; alignment<0x1000; alignment+=0x200)
  {
    if (PatchAligned(address,buf,size,-alignment))
    {
      printf("Load offset adjusted to %x",alignment);
      loadOffset = -alignment;
      return;
    }    
  }

  fprintf(stderr,"Original content not matching at %p (%p)",address,(intptr_t)address-CODE_OFFSET);
}

void ProcessPatch::PatchDword(void *address, DWORD val)
{
	Patch(address,&val,sizeof(val));
}

void *ProcessPatch::FindBlock(const void *address, int size, int start) const
{	
	const char *find = (const char *)address;
	const unsigned char *next = memCopy.Data()+start;
	int nextSize = memCopy.Size()-start;
	while (nextSize>0)
	{
		const void *test = memchr(next,*find,nextSize);
		if (!test) return NULL; // no more possibilities found
		// try if we match here
		if (!memcmp(test,find,size))
		{
			return (void *)((int)test-(int)memCopy.Data()+CODE_OFFSET);
		}
		int skip = (unsigned char *)test-next+1;
		nextSize -= skip;
		next += skip;
	}
	return NULL;
}

inline bool ProcessPatch::TestByte(unsigned char value, int start) const
{	
	const unsigned char *next = memCopy.Data()+start;
	int nextSize = memCopy.Size()-start;
	if (nextSize<=0) return false;
	return *next==value;
}

inline int ProcessPatch::CountByte(unsigned char value, int start) const
{	
	const unsigned char *next = memCopy.Data()+start;
	int nextSize = memCopy.Size()-start;
	int count = 0;
	while (nextSize>0)
	{
		if (*next!=value) return count;
		count++;
		next++;
		nextSize--;
	}
	return count;
}


void *ProcessPatch::FindDword(DWORD dword) const
{
	return FindBlock(&dword,sizeof(dword));
}

/*
#define MASK_LOW(x) ((1<<(x))-1)
#define ROL_U(a,b) \
	( ((a)<<(b)) | ( (((a)>>(32-(b))))&MASK_LOW((b)) ) )
#define ROR_U(a,b) \
	( ((((a)>>(b))&MASK_LOW(32-(b))) | ((a)<<(32-(b)))) )
*/

inline unsigned ROL( unsigned x, unsigned y)
{
	int retval;
	__asm
	{
		mov eax,x
		mov ecx,y
		rol eax,cl
		mov retval,eax
	}
	//unsigned ret = ROL_U(x,((y)&0x1f));
	return retval;
}
inline unsigned ROR( unsigned x, unsigned y)
{
	int retval;
	__asm
	{
		mov eax,x
		mov ecx,y
		ror eax,cl
		mov retval,eax
	}
	//unsigned ret = ROR_U(x,((y)&0x1f));
	return retval;
}

#include "mapFile.hpp"

//! simulate checksum calculation (see lib/integrity.hpp)

int CalculateChecksum(const ProcessPatch &patch, int codeStart, int codeSize)
{
	int esi = 0;
	int ecx = codeSize/4;
	unsigned int *edx = (unsigned int *)codeStart;
	while (ecx>0)
	{
		DWORD eax;
		patch.ReadMemory(edx,&eax,sizeof(eax));
		// simulate ROL
		int add = ROL(eax,ecx);
		/*
		if (eax==SUM_ADJUST_MAGIC)
		{
			printf("Magic %x->%x\n",eax,add);
		}
		*/
		esi += add;

		ecx--;
		edx++;
	}
	return esi;
}

char *fgetline(char *line, int size, FILE *f)
{
	*line = 0;
	char *ret = fgets(line,size,f);
	if (!ret) return ret;
	if (*line && line[strlen(line)-1]=='\n') line[strlen(line)-1]=0;
	return line;
}

int CalculateCRC(const ProcessPatch &patch, int codeStart, int codeSize)
{
	// calculate CRC of code section
	int size = codeSize;
	unsigned char *code = (unsigned char *)codeStart;
	const int pageSize = 0x1000;
	unsigned char page[pageSize];
	CRCCalculator crc;
	while (size>=pageSize)
	{
		patch.ReadMemory(code,page,pageSize);
		crc.AddBlock(page,pageSize);
		size -= pageSize;
		code += pageSize;
	}
	if (size>0)
	{
		patch.ReadMemory(code,page,size);
		crc.AddBlock(page,size);
	}
	return crc.CRC();
}

bool Overlap( int beg1, int len1, int beg2, int len2)
{
	int end1 = beg1+len1;
	int end2 = beg2+len2;
	if (end2<beg1 && beg2<beg1) return false;
	if (end1<beg2 && end1<beg2) return false;
	return true;
}

static void RandomNop(char *buf, int size)
{
	static const unsigned char data2[][2]=
	{
		{0xeB,0x00},
		{0x8B,0xC0},
	};
	static const unsigned char data3[][3]=
	{
		{0x8D,0x40,0x00},
	};
	const int ndata2 = sizeof(data2)/sizeof(*data2);
	const int ndata3 = sizeof(data3)/sizeof(*data3);
	if (size<2) return;
	if (size&1)
	{
		int i = rand()%ndata3;
		memcpy(buf,data3[i],3);
		size -= 3;
		buf += 3;
	}
	while (size>=2)
	{
		// select random data2
		int i = rand()%ndata2;
		memcpy(buf,data2[i],i);
		size -= 2;
		buf += 2;
	}
}


int consoleMain(int argc, const char *argv[])
{
	if (argc!=2)
	{
		return 1;
	}
	int ret = 0;
	const char *src = argv[1];

	#if _DEBUG
	__asm
	{
		jmp Skip
		mov eax,eax
		lea eax,[eax]
		_emit 0x8d
		_emit 0x40
		_emit 0
		_emit 0xe9
		_emit 0
		_emit 0
		_emit 0
		_emit 0

		jmp Skip
		Skip:
	}
	#endif
	// process handle is pointer to executable header
	printf("Loading %s\n",src);
	ProcessPatch patch;
	if (patch.Load(src))
	{
		fprintf(stderr,"Cannot load process\n");
		return 1;
	}

	MapFile map;
	map.ParseMapFile(src);
	if (map.Empty())
	{
		fprintf(stderr,"Cannot load mapfile\n");
		return 1;
	}

	printf("Code section start %x, size %x\n",patch.codeStart,patch.codeSize);
	printf("Data section start %x, size %x\n",patch.dataStart,patch.dataSize);
	//printf("Total        start %x, size %x\n",patch.totalStart,patch.totalSize);
	/*
	if (patch.dataStart!=patch.codeStart+patch.codeSize)
	{
		printf("Data and code sections are not contiguous\n");
		printf("Data section will not be signed.\n");
	}
	*/


	int crcSize = patch.codeSize;
	int crcBase = patch.codeStart+CODE_OFFSET;

	int sumSize = patch.codeSize;
	int sumBase = patch.codeStart+CODE_OFFSET;

	char secName[1024];
	strcpy(secName,src);
	strcpy(GetFileExt(secName),".sec");
	FILE *sec = fopen(secName,"r");
	if (!sec)
	{
		fprintf(stderr,"Cannot open %s\n",secName);
		return 1;
	}
	else
	{
		// read section boundaries from .sec _file
		// first is end of non-protected functions
		int endCRC = patch.codeSize+patch.codeStart+CODE_OFFSET;
		int startSecSum = 0;
		int startSecCRC = 0;
		for(;;)
		{
			char line[1024];
			char *ret = fgetline(line,sizeof(line),sec);
			if (!ret) break;
			char *endNum;
			int order = strtoul(line,&endNum,10);
			while (*endNum==' ') endNum++;
			int address = map.PhysicalAddress(endNum);
			if (!address)
			{
				address = map.PhysicalAddress(RString("_")+RString(endNum));
			}
			if (order==20)  startSecSum = address;
			else if (order==30)  startSecCRC = address;
			else if (order>30)  endCRC = address;
		}
		fclose(sec);
		if (startSecSum && startSecCRC)
		{
			crcBase = startSecCRC;
			crcSize = endCRC-startSecCRC;
			sumBase = startSecSum;
			sumSize = startSecCRC-startSecSum;
			int alignSumBase = sumBase-(sumBase&~3);
			sumBase -= alignSumBase;
			sumSize += alignSumBase;
			int alignSumSize = (4-sumSize)&3;
			sumSize += alignSumSize;

			printf("Checksum  from %x (%x), size %x\n",sumBase,sumBase-CODE_OFFSET,sumSize);
			printf("CRC       from %x (%x), size %x\n",crcBase,crcBase-CODE_OFFSET,crcSize);
			if (crcSize<=0)
			{
				fprintf(stderr,"Invalid CRC section size.\n");
				return 1;
			}
			if (sumSize<=0)
			{
				fprintf(stderr,"Invalid checksum section size.\n");
				return 1;
			}
		}
		else
		{
			fprintf(stderr,"Invalid section boundaries.\n");
		}
	}

	srand(GetTickCount());
	#define DWORDB(x) (x)&0xff,((x)>>8)&0xff,((x)>>16)&0xff,((x)>>24)&0xff

	const char *allocator = "?_allocatorTrackLLinks@TrackLLinks@@0VFastCAlloc@@A";
	int allocatorAddr = map.PhysicalAddress(allocator);
	
	// replace allocator placeholder with actual code
	/*
	004099FD B8 43 4C 41 46       mov         eax,46414C43h
	00409A02 05 43 4C 41 46       add         eax,46414C43h
	00409A07 50                   push        eax
	*/
	static const unsigned char allocCodeSrc[]=
	{
		0xB8, DWORDB(ALLOCATOR_MAGIC),
		0x05, DWORDB(ALLOCATOR_MAGIC),
		0x50
	};
	for(;;)
	{
		void *allocSection = patch.FindBlock(allocCodeSrc,sizeof(allocCodeSrc));
		if (!allocSection) break;
		if (allocatorAddr==0)
		{
			fprintf(stderr,"_allocatorTrackLLinks not found\n");
			ret = 1;
			break;
		}
		float factor = rand()%80*0.01f+0.1f;
		int addr1 = allocatorAddr*factor;
		int addr2 = allocatorAddr-addr1;
		unsigned char allocCodeTgt[]=
		{
			0xB8, DWORDB(addr1),
			0x05, DWORDB(addr2),
			0x50
		};
		patch.Patch(allocSection,allocCodeTgt,sizeof(allocCodeTgt));
		printf("Patching Alloc section (%x)\n",(int)allocSection-CODE_OFFSET);
	}

	const char *doFadeVar = "?DoFade@@3_NA";
	int doFadeVarAddr = map.PhysicalAddress(doFadeVar);
	if (!doFadeVarAddr)
	{
		fprintf(stderr,"bool DoFade not found\n");
		return 1;
	}

	
	// replace DoFade address load code more difficult to track
	/*
	004153ED 8D 05 4A DA 6F 00    lea         eax,[DoFade (006fda4a)]
	004153F3 2D 52 44 41 46       sub         eax,46414452h
	004153F8 05 52 44 41 46       add         eax,46414452h
	004153FD 80 38 00             cmp         byte ptr [eax],0
	00415400 0F 95 45 E7          setne       byte ptr [isValid]
	*/
	static const unsigned char fadeCodeSrc[]=
	{
		0x8D, 0x05, DWORDB(doFadeVarAddr), 
		0x2D, DWORDB(FADE_ADR_MAGIC),
		0x05, DWORDB(FADE_ADR_MAGIC),
		0x80, 0x38, 0x00             
	};
	for(;;)
	{
		void *code = patch.FindBlock(fadeCodeSrc,sizeof(fadeCodeSrc));
		if (!code) break;
		float factor1 = rand()%80*0.01f+0.1f;
		float factor2 = rand()%80*0.01f+0.1f;
		int addr1 = doFadeVarAddr*factor1;
		int addr2 = doFadeVarAddr*factor2;
		// doFadeVarAddr = addr1 - addr2 + addr3
		// doFadeVarAddr + addr2 - addr1 = addr3
		int addr3 = doFadeVarAddr-addr1+addr2;
		unsigned char codeTgt[]=
		{
			0x8D, 0x05, DWORDB(addr1), 
			0x2D, DWORDB(addr2),
			0x05, DWORDB(addr3),
			0x80, 0x38, 0x00             
		};
		patch.Patch(code,codeTgt,sizeof(codeTgt));
		printf("Patching IF_FADE() (%x)\n",(int)code-CODE_OFFSET);
	}



	// simulate checksum calculation (see lib/integrity.hpp)
	/*
	CheckFade looks like this:
	0045AB42 68 00 80 25 00       push        CRC_SIZE_MAGIC
	0045AB47 68 00 10 40 00       push        CRC_BASE_MAGIC
	0045AB4C 8D 8D 00 FC FF FF    lea         ecx,[crc]
	0045AB52 E8 99 5D FF FF       call        CRCCalculator::CRC (004508f0)
	*/
	static const unsigned char crcCodeSrc[]=
	{
		0x68, DWORDB(CRC_SIZE_MAGIC),
		0x68, DWORDB(CRC_BASE_MAGIC),
	};
	unsigned char crcCodeTgt[]=
	{
		0x68, DWORDB(crcSize),
		0x68, DWORDB(crcBase),
	};
	for(;;)
	{
		void *crcSection = patch.FindBlock(crcCodeSrc,sizeof(crcCodeSrc));
		if (!crcSection) break;
		patch.Patch(crcSection,crcCodeTgt,sizeof(crcCodeTgt));
		printf("Patching CRC section (%x)\n",(int)crcSection-CODE_OFFSET);
	}

	/*
	CheckGenuine looks like this
	0040CAA3 BA 53 41 42 53       mov         edx,53424153h
	0040CAA8 B9 5A 49 53 53       mov         ecx,5353495Ah
	0040CAAD 81 C2 53 41 42 53    add         edx,53424153h
	0040CAB3 81 C1 5A 49 53 53    add         ecx,5353495Ah
	*/
	static const unsigned char sumCodeSrc[]=
	{
		0xBA, DWORDB(SUM_BASE_MAGIC),
		0xB9, DWORDB(SUM_SIZE_MAGIC),
		0x81, 0xc2, DWORDB(SUM_BASE_MAGIC),
		0x81, 0xc1, DWORDB(SUM_SIZE_MAGIC),
	};

	for(;;)
	{
		void *sumSection = patch.FindBlock(sumCodeSrc,sizeof(sumCodeSrc));
		if (!sumSection) break;
		float factorSize = rand()%80*0.01+0.1;
		float factorBase = rand()%80*0.01+0.1;
		int size1 = sumSize/4*factorSize;
		int size2 = sumSize/4-size1;
		int base1 = sumBase*factorBase;
		int base2 = sumBase-base1;
		unsigned char sumCodeTgt[]=
		{
			0xBA, DWORDB(base1),
			0xB9, DWORDB(size1),
			0x81, 0xc2, DWORDB(base2),
			0x81, 0xc1, DWORDB(size2),
		};
		patch.Patch(sumSection,sumCodeTgt,sizeof(sumCodeTgt));
		printf("Patching SUM section (%x)\n",(int)sumSection-CODE_OFFSET);
	}

	// verify if there is any indirect call inside of protected region

	static const unsigned char indirectCall[]={0xFF,0x15};

  
  for(int base=sumBase;;)
  {
	  void *icall = patch.FindBlock(indirectCall,sizeof(indirectCall),base-CODE_OFFSET);
	  if (icall && (int)icall<sumBase+sumSize)
	  {
		  printf
		  (
			  "Warning: Checksum: indirect call at offset %x (%s)\n",
			  (int)icall-CODE_OFFSET,map.MapNameFromPhysical((int)icall)
		  );
		  base = (int)icall+1;
	  }
	  else break;
  }
  

  for(int base=crcBase;;)
  {
	  void *icall = patch.FindBlock(indirectCall,sizeof(indirectCall),base-CODE_OFFSET);
	  if (icall && (int)icall<crcBase+crcSize)
	  {
		  printf
		  (
			  "Warning: CRC: indirect call at offset %x (%s)\n",
			  (int)icall-CODE_OFFSET,map.MapNameFromPhysical((int)icall)
		  );
		  base = (int)icall+1;
	  }
	  else break;
  }

	// change any nop/int 3 instructions in checksum or CRC regions
	unsigned char nopCode = 0x90;
	unsigned char brkCode = 0xcc;
	int offset = 0;
	for(;;)
	{
		void *nopAddr = patch.FindBlock(&nopCode,sizeof(nopCode),offset);
		void *brkAddr = patch.FindBlock(&brkCode,sizeof(brkCode),offset);
		// select first of them
		void *minAddr = nopAddr;
		int val = nopCode;
		if (nopAddr>brkAddr && brkAddr)
		{
			val = brkCode;
			minAddr = brkAddr;
		}
		if (!minAddr) break;
		offset = (int)minAddr-CODE_OFFSET;
		int count = patch.CountByte(val,offset);
		// check if part of string is inside of checksum or CRC region
		if
		(
			count>1 &&
			(
				Overlap((int)minAddr,count,sumBase,sumSize)
				|| Overlap((int)minAddr,count,crcBase,crcSize)
			)
		)
		{
			// test preceding byte
			bool isRet = false;
			if (offset>=1)
			{
				if (patch.TestByte(0xc3,offset-1))
				{
					isRet = true;
				}
			}
			if (offset>=3)
			{
				if (patch.TestByte(0xc2,offset-3))
				{
					isRet = true;
				}
			}
			if (offset>=5)
			{
				if (patch.TestByte(0xe9,offset-5))
				{
					isRet = true;
				}
			}
			if (offset>=2)
			{
				if (patch.TestByte(0xeb,offset-2))
				{
					isRet = true;
				}
			}
			bool isAlign = ((offset+count)&15)==0;
			if (!isRet || !isAlign)
			{
				if (isRet) printf("Info: %d x %02x ignored at (%x->%x), not a padding\n",count,val,(int)minAddr-CODE_OFFSET,minAddr);
			}
			else
			{
				#if _DEBUG
					printf("Padding: %02x (%d) (%x->%x)\n",val,count,(int)minAddr-CODE_OFFSET,minAddr);
				#endif

				Temp<char> nopRandom(count);
				RandomNop(nopRandom.Data(),nopRandom.Size());
				patch.Patch(minAddr,nopRandom.Data(),nopRandom.Size());
			}
		}
		offset+=count;


	}


	// seek for signatures
	int crcResult = CalculateCRC(patch,crcBase,crcSize);
	for(;;)
	{
		void *fcrcAddr = patch.FindDword(CRC_VALUE_MAGIC);
		if (!fcrcAddr) 
		{
			break;
		}
		printf("Patching CRC value (%x)\n",(int)fcrcAddr-CODE_OFFSET);
		patch.PatchDword(fcrcAddr,crcResult);
	}

	int sumResult = CalculateChecksum(patch,sumBase,sumSize);
	// there should be only one sum address
	void *fsumAddr = patch.FindDword(SUM_ADJUST_MAGIC);
	if (!fsumAddr) 
	{
		if (sumResult==0)
		{
			void *crcFind = patch.FindDword(crcResult);
			if (crcFind)
			{
				printf("File is already signed, CRC signature OK.\n");
				return 0;
			}
			else
			{
				printf("File seems to be already signed,\n");
				printf("but CRC signature does not match.\n");
			}
		}
		else
		{
			printf("No signature points found.\n");
		}
		return 1;
	}
	else
	{
		printf("Patching checksum value (%x)\n",(int)fsumAddr-CODE_OFFSET);

		// adapt sum result

		int fsumOffset = (int)fsumAddr-sumBase;
		int rotCount = ((sumSize-fsumOffset)/4)&0x1f;
		if (fsumOffset&3)
		{
			printf("Checksum adjustment not DWORD aligned (%d).\n",fsumOffset&3);
			ret = 1;
		}

		//0 = sumResult - ROL(SUM_ADJUST_MAGIC,rotCount) + ROL(aSum,rotCount);
		//0 = -sumResult + ROL(SUM_ADJUST_MAGIC,rotCount) - ROL(aSum,rotCount);
		//ROL(aSum,rc) = sumResult - ROL(SUM_ADJUST_MAGIC,rotCount);
		// aSum = ROR(sumResult - ROL(SUM_ADJUST_MAGIC,rotCount),rotCount)
		DWORD aSum = ROR(ROL(SUM_ADJUST_MAGIC,rotCount)-sumResult,rotCount);

		DWORD cSum = sumResult - ROL(SUM_ADJUST_MAGIC,rotCount) + ROL(aSum,rotCount);

		if ((int)fsumAddr<sumBase || (int)fsumAddr>=sumBase+sumSize-4)
		{
			printf("Checksum adjustment out of checksum range.\n");
			ret = 1;
		}
		patch.PatchDword(fsumAddr,aSum);

		int sumPResult = CalculateChecksum(patch,sumBase,sumSize);
		if (sumPResult)
		{
			printf("Checksum result failed: %x\n",sumPResult);
			ret = 1;
		}
		int crcPResult = CalculateCRC(patch,crcBase,crcSize);
		if (crcResult!=crcPResult)
		{
			printf("crcResult result failed: %x!=%x\n",crcResult,crcPResult);
			ret = 1;
		}
	}

	patch.Unload();

	#if 1 //_DEBUG
		wasError = true;
	#endif
	return ret;
}

/*
void ErrF(const char *format, ... )
{
}
void LogF(const char *format, ... )
{
}
*/