#include <Es/essencepch.hpp>
#include <Es/Common/win.h>
#include <Es/Strings/rString.hpp>
#include <Es/Containers/array.hpp>

TypeIsSimple(HANDLE)

int WINAPI WinMain( HINSTANCE hInstance, 
 // handle to current instance 
  HINSTANCE hPrevInstance, 
 // handle to previous instance 
  LPSTR lpCmdLine, 
 // pointer to command line 
  int nCmdShow 
 // show state of window 
 
)
{
  if (*lpCmdLine==0)
  {
    // fail: no command line given
    MessageBox(NULL,"Usage: run {wait command1} {wait command2} no-wait command1","Run",MB_OK|MB_ICONINFORMATION);
    return 1;
  }
  // {wait command} {wait command} {wait command} {wait command} no-wait command"
  // list of wait commands

  AutoArray<RString> waitCommands;

  // handles we will be waiting for
  AutoArray<HANDLE> handles;

  for (;;)
  {
    if (*lpCmdLine==0) break;

    if (*lpCmdLine=='{')
    {
      // check content of {}
      LPSTR endOfCommand = strchr(lpCmdLine+1,'}');
      if (!endOfCommand)
      {
        MessageBox(NULL,"No closing '}' found","Run",MB_OK|MB_ICONERROR);
        return 1;
      }

      waitCommands.Add(RString(lpCmdLine+1,endOfCommand-(lpCmdLine+1)));
      lpCmdLine = endOfCommand+1;
    }
    else if (*lpCmdLine==' ') lpCmdLine++;
    else break;
  }

  // used shared startup info for all processes run
  STARTUPINFO startupInfo;
  startupInfo.cb=sizeof(startupInfo); 
  startupInfo.lpReserved=0;
  startupInfo.lpDesktop=NULL;
  startupInfo.lpTitle=NULL;
  startupInfo.dwFlags=0;
  startupInfo. cbReserved2=0; 
  startupInfo.lpReserved2=NULL; 

  // unless proven otherwise, assume success
  bool success=true;

  // run commands we are waiting for
  // 
  for (int i=0; i<waitCommands.Size(); i++)
  {
    // current directory for wait commands in unchanged
    PROCESS_INFORMATION pi;
    BOOL ok = CreateProcess(
        NULL,waitCommands[i].MutableData(),
        NULL,NULL, // security
        TRUE, // inheritance
        0, // creation flags 
        NULL, // env
        NULL, // pointer to current directory name 
        &startupInfo,&pi
    );
    if (ok==FALSE) success = false;
    else
    {
      CloseHandle(pi.hThread);
      handles.Add(pi.hProcess);
    }
  }

  // wait for them

  WaitForMultipleObjects(handles.Size(),handles.Data(),TRUE,INFINITE);
  // run command which we are not waiting for

  // check process return values
  // on any non-zero assume failure
  for (int i=0; i<handles.Size(); i++)
  {
    DWORD exitCode;
    if (!GetExitCodeProcess(handles[i],&exitCode)) success = false;
    else if (exitCode!=0) success = false;
    CloseHandle(handles[i]);
  }
  handles.Clear();

  PROCESS_INFORMATION processInformation;

  /*
  // do not touch current directory
	char buff[_MAX_PATH+1];
	DWORD nLen = GetModuleFileName(hInstance,buff,_MAX_PATH);
	while((buff[nLen-1]!='\\')&&(buff[nLen-1]!='/')) nLen--;
	buff[nLen-1]=0;
	SetCurrentDirectory(buff);
  */

	/*
	char name[512];
	strcpy(name,"bin\\");
	strcat(name,lpCmdLine);
	*/

  
  BOOL ok = CreateProcess(
      NULL,lpCmdLine,
      NULL,NULL, // security
      FALSE, // inheritance
      0, // creation flags 
      NULL, // env
      NULL, // pointer to current directory name 
      &startupInfo,&processInformation 
  );
  if (ok==FALSE) success = false;
  return success ? 0 : 1; 	
}
 
