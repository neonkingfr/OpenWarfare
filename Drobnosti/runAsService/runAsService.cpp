// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (C) 1993-1997  Microsoft Corporation.  All Rights Reserved.
//
//  MODULE:   simple.c
//
//  PURPOSE:  Implements the body of the service.
//            The default behavior is to open a
//            named pipe, \\.\pipe\simple, and read
//            from it.  It the modifies the data and
//            writes it back to the pipe.
//
//  FUNCTIONS:
//            ServiceStart(DWORD dwArgc, LPTSTR *lpszArgv);
//            ServiceStop( );
//
//  COMMENTS: The functions implemented in simple.c are
//            prototyped in service.h
//              
//
//  AUTHOR: Craig Link - Microsoft Developer Support
//


#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <process.h>
#include <direct.h>
#include <tchar.h>
#include "service.h"


//
//  FUNCTION: ServiceStart
//
//  PURPOSE: Actual code of the service
//           that does the work.
//
//  PARAMETERS:
//    dwArgc   - number of command line arguments
//    lpszArgv - array of command line arguments
//
//  RETURN VALUE:
//    none
//
//  COMMENTS:
//

PROCESS_INFORMATION ServiceProcessInformation;

static void TrimEOL(char *line)
{
	if (strlen(line)>0 && line[strlen(line)-1]=='\n')
	{
		line[strlen(line)-1]=0;
	}
}

#define ConfigApp "Software\\Codemasters\\Operation Flashpoint"

void GetOFPDir(char *buf, int bufSize)
{
	HKEY key;
	*buf = 0;
	if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, ConfigApp, 0, KEY_READ, &key) != ERROR_SUCCESS) return;
	DWORD bufSizeRd = bufSize;
	if (::RegQueryValueEx(key, "Main", NULL, NULL, (BYTE *)buf, &bufSizeRd) !=  ERROR_SUCCESS) return;
	::RegCloseKey(key);
}

HANDLE stopEvent;

VOID ServiceStart (DWORD argc, LPTSTR *argv)
{
	DWORD err = ERROR_INVALID_FUNCTION;
	{
	// first argument is application name
	// second argument is working directory for the application
	// third and more arguments are passed to the application

    ///////////////////////////////////////////////////
    //
    // Service initialization
    //

    // report the status to the service control manager.
    //
    if (!ReportStatusToSCMgr(
        SERVICE_START_PENDING, // service state
        NO_ERROR,              // exit code
        10000))                 // wait hint
        return;


	char dirLine[1024];
	char cmdLine[1024];
	bool cfgFile = false;
	if (argc<3)
	{
		// load command line arguments from argument file
		// get directory from OFP registry
		char argsName[1024];
		GetOFPDir(argsName,sizeof(argsName));
		strcat(argsName,"\\runAsService.cfg");

		FILE *args = fopen(argsName,"r");
		if (!args) goto Error;
		bool ok = true;
		if (!fgets(dirLine,sizeof(dirLine),args)) ok = false;
		if (!fgets(cmdLine,sizeof(cmdLine),args)) ok = false;
		TrimEOL(dirLine);
		TrimEOL(cmdLine);
		fclose(args);
		if (!ok) goto Error;
		cfgFile = true;
	}

	// ignore service exe command
  STARTUPINFO startupInfo;
  startupInfo.cb=sizeof(startupInfo); 
  startupInfo.lpReserved=0;
  startupInfo.lpDesktop=NULL;
  startupInfo.lpTitle=NULL;
  startupInfo.dwFlags=0;
  startupInfo. cbReserved2=0; 
  startupInfo.lpReserved2=NULL; 

	if (!cfgFile)
	{
		// create command line from command line arguments
		argv++,argc--;
		// ignore -debug switch
		if (!strcmpi("-debug",argv[0]))
		{
			argv++,argc--;
		}
		if (argc<2) goto Error;
		sprintf(cmdLine,"\"%s\"",argv[0]);
		argv++,argc--;
		strcpy(dirLine,argv[0]);
		argv++,argc--;
		while (argc>0)
		{
			sprintf(cmdLine+strlen(cmdLine)," \"%s\"",argv[0]);
			argv++,argc--;
		}
	}

	stopEvent = CreateEvent(NULL,TRUE,FALSE,NULL);

	Restart:
	BOOL success = CreateProcess
	(
			NULL,cmdLine,
			NULL,NULL, // security
			FALSE, // inheritance
			0, // creation flags 
			NULL, // env
			dirLine, // pointer to current directory name 
			&startupInfo,&ServiceProcessInformation
	); 

	if (!success)
	{
		//err = GetLastError()&0xffff;
	  AddToMessageLog(TEXT("Cannot create process"),true,GetLastError());
		goto Error;
	}

    // report the status to the service control manager.
    //
    if (!ReportStatusToSCMgr(
        SERVICE_RUNNING,       // service state
        NO_ERROR,              // exit code
        0))                    // wait hint
        goto Error;

    //
    // End of initialization
    //
    ////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////
    //
    // Service is now running, perform work until shutdown
    //

     // the only work is waiting for a process to terminate
		WaitForSingleObject(ServiceProcessInformation.hProcess,INFINITE);

		// check process return code
		// TODO: if process terminated as result of ServiceStop,
		// we should not never restart it
		if (WaitForSingleObject(stopEvent,0)!=WAIT_OBJECT_0)
		{
			// check process termination value?
			DWORD exit = 0;
			GetExitCodeProcess(ServiceProcessInformation.hProcess,&exit);
			if (exit!=0 && exit!=STILL_ACTIVE)
			{
				char buf[1024];
				sprintf(buf,"Restarted after process error %x",exit);
				AddToMessageLog(TEXT(buf),true,0);
			}
			else
			{
				AddToMessageLog("Restarted after process shutdown",false,0);
			}
			goto Restart;
		}


	// cleanup;
	CloseHandle(ServiceProcessInformation.hProcess);
	CloseHandle(ServiceProcessInformation.hThread);


	return;
	}
	Error:
	
   ReportStatusToSCMgr
	 (
        SERVICE_START_PENDING, // service state
        err,              // exit code
				10000
   );

}


//
//  FUNCTION: ServiceStop
//
//  PURPOSE: Stops the service
//
//  PARAMETERS:
//    none
//
//  RETURN VALUE:
//    none
//
//  COMMENTS:
//    If a ServiceStop procedure is going to
//    take longer than 3 seconds to execute,
//    it should spawn a thread to execute the
//    stop code, and return.  Otherwise, the
//    ServiceControlManager will believe that
//    the service has stopped responding.
//

void DoTerminateThread(void *context)
{
	// check if process is able to terminate on request
	PostThreadMessage(ServiceProcessInformation.dwThreadId,WM_APP+1,0,0);
	DWORD wait = WaitForSingleObject(ServiceProcessInformation.hProcess,30000);
	if (wait!=WAIT_OBJECT_0)
	{
		AddToMessageLog(TEXT("Process did not terminate - force termination"),true,0);
		// process did not terminate - force termination
		TerminateProcess(ServiceProcessInformation.hProcess,1);
	}
}

VOID ServiceStop()
{
	SetEvent(stopEvent);
	// start process termination thread
	_beginthread(DoTerminateThread,0,NULL);

	// 

}
