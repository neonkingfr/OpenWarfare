// OFPPatch.cpp : Defines the entry point for the application.
//

#include <windows.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <direct.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <class/filenames.hpp>
#include <class/debugLog.hpp>
#include <class/array.hpp>

#define USE_RESOURCES	1

//#define RES_TYPE "BINARY"
#define RES_TYPE MAKEINTRESOURCE(RT_RCDATA)

void ErrorMessage(HINSTANCE hInstance, const char *buffer, int icon = MB_ICONERROR)
{
	char caption[256];
	MessageBox(NULL, buffer, caption, MB_OK | icon);
}


void DisplayErrorMessage()
{
	LPVOID lpMsgBuf;
	FormatMessage( 
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM | 
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
			(LPTSTR) &lpMsgBuf,
			0,
			NULL 
	);
	// Display the string.
	MessageBox( NULL, (LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION );
	#if _DEBUG
		OutputDebugString((LPCTSTR)lpMsgBuf);
	#endif
	// Free the buffer.
	LocalFree( lpMsgBuf );
}

void *FindAndLoadResource(HMODULE hmod, int &size, const char *source)
{
	char upName[512];
	strcpy(upName,source);
	strupr(upName);
	HRSRC hRsc = FindResource(hmod, upName, RES_TYPE);
	if (!hRsc)
	{
		return NULL;
	}
	HGLOBAL hMem = LoadResource(hmod, hRsc);
	size = SizeofResource(hmod, hRsc);
	return LockResource(hMem);
}


DWORD ResourceSize(const char *source, const void *ctx)
{
	char upName[256];
	strcpy(upName,source);
	strupr(upName);
	HRSRC hRsc = FindResource(NULL, upName, RES_TYPE);
	if (!hRsc)
	{
		DisplayErrorMessage();
		return false;
	}
	return SizeofResource(NULL, hRsc);
}


//! create path to given directory
void CreatePath(const char *path)
{
	// string will be changed temporary
	char name[256];
	strcpy(name, path);
	char *end = name;
	while (end = strchr(end, '\\'))
	{
		*end = 0;
		mkdir(name);
		*end = '\\';
		end++;
	}
}


bool ResourceToFile(HMODULE hmod, const char *source)
{
	int size;
	void *ptr = FindAndLoadResource(hmod,size,source);
	if (!ptr)
	{
		DisplayErrorMessage();
		return false;
	}

	const char *destination = source;
	CreatePath(destination);
	chmod(destination,S_IREAD | S_IWRITE);
	unlink(destination);
	int f = open
	(
		destination,O_CREAT|O_TRUNC|O_WRONLY|O_BINARY,S_IREAD | S_IWRITE
	);
	if (f<0) return false;

	bool ok = true;
	while (size>0 && ok)
	{
		int step = size;
		if (step>1024*1024) step = 1024*1024;

		int wr = write(f, ptr, step);
		if (wr!=step) ok = false;
		if (!ok)
		{
			#if 1 //_DEBUG
				char buffer[256];
				sprintf(buffer, "errno = %d (0x%x), %s", errno, errno, destination);
				MessageBox( NULL, buffer, "Error in write", MB_OK | MB_ICONINFORMATION);
			#endif
		}
		else
		{
			size -= step;
			ptr = (char *)ptr + step;
		}
	}

	if (close(f)<0)
	{
		ok = false;
		#if _DEBUG
			char buffer[256];
			sprintf(buffer, "errno = %d (0x%x)", errno, errno);
			MessageBox( NULL, buffer, "Error in close", MB_OK | MB_ICONINFORMATION);
		#endif
	}
	return ok;
}


struct EnumApplyContext 
{
	int verMajor,verMinor; // version number (source)
	int verMajorPatch,verMinorPatch; // version number (target)
	char version[512]; // version path
	char dir[512]; // destination path
	char errorFile[512]; // when error, say where

	bool found;
	bool error;
	bool nospace;
};
    


static BOOL CALLBACK EnumResNames(HMODULE hModule, LPCTSTR lpType, LPTSTR lpName, LONG lParam)
{ 
   // Write the resource name to a resource information file. 
   // The name may be a string or an unsigned decimal 
   // integer, so test before printing. 

  if ((ULONG)lpName & 0xFFFF0000) 
  { 
      LogF("\tName: %s", lpName);
  } 
  else 
  { 
      LogF("\tName: %u", (USHORT)lpName); 
  } 

	ResourceToFile(hModule,lpName);

  return TRUE; 
} 



int consoleMain(int argc, const char *argv[])
{
	if (argc!=2)
	{
		return 1;
	}
	HINSTANCE hinst = LoadLibraryEx(argv[1],NULL,LOAD_LIBRARY_AS_DATAFILE);

	if (!hinst)
	{
		return 1;
	}
	HMODULE hmod = hinst;

	LogF("Loaded %s",argv[1]);

	EnumResourceNames(hmod,RES_TYPE,EnumResNames,NULL);

	FreeLibrary(hinst);

	return 0;
}

