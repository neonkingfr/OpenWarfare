#ifndef WIKI_TASKS_H
#define WIKI_TASKS_H

enum Products
{
  POFP2,
  PArmA,
  PVBS2,
  ArmA2,
  NProducts
};
const char *productsStr[];

enum EPriority
{
  P_NONE, P_NICE_TO_HAVE, P_DESIRED, P_REQUIRED, NPriorities
};
const char *prioritiesStr[];

enum Stages {
  S_ANALYSIS,
  S_PROTOTYPE,
  S_ALPHA,
  S_BETA,
  NStages
};
const char *stagesStr[];

//global functions
int GetPriorityIx(const char *priority);
int GetStageIx(const char *stage);
int GetProductIx(const char *product);

#endif
