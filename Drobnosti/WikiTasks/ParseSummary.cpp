#include <El/elementpch.hpp>

#include <Es/Common/win.h>
#include <Es/Strings/rString.hpp>

#include <El/XML/inet.hpp>
#include <El/XML/xml.hpp>
#include <El/QStream/qStream.hpp>
#include <El/ParamFile/paramFile.hpp>

#include "WikiTasks.h"

#ifdef PARSE_SUMMARY

///StackArray implementation of basic Stack data structure using AutoArray
template <class Type>
class StackArray : public AutoArray<Type>
{
public:
  ///standard push operation
  void Push(Type item) {Add(item);}
  ///standard pop operation
  Type Pop()
  {
    int n = Size() - 1;
    Type item = Set(n);
    Delete(n);
    return item;
  }
  ///get the top stack value for possible update
  Type &Top() 
  {
    return Set(Size() - 1);
  }
  ///true when Stack is empty
  bool IsEmpty()
  {
    return Size()==0;
  }
};

class UserInfo
{
public:
  typedef RString KeyType;
  RString name;
  float timeLeft[NStages];
  
  UserInfo() 
  {
    for(int i=0; i<NStages; i++) timeLeft[i]=0.0f;
  }
  UserInfo(RString name1) 
  {
    name = name1;
    for(int i=0; i<NStages; i++) timeLeft[i]=0.0f;
  }
  bool operator==(const UserInfo &user) const {return (name==user.name); }
};
TypeIsMovable(UserInfo)

enum TasksElements
{
  TE_TASKS, TE_TASK, 
  TE_NAME, 
  TE_PRIORITIES, TE_PRIORITY, 
  TE_ASSIGNES, TE_ASSIGN, 
  TE_ANALYSIS, TE_PROTOTYPE, TE_ALPHA, TE_BETA,
  TE_DEPENDENCIES, TE_DEPENDENCY
};

class ExtractSummary : public SAXParser
{
private:
  UserInfo *_curUser;
protected:
  FindArrayKey<UserInfo> _users;
  int _insideElement;
  RString _currValue;
  int _currElement;

public:
  ExtractSummary() {_curUser = NULL; _insideElement=0; }
  FindArrayKey<UserInfo> &GetUsers() {return _users;}

  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "Assign") == 0) {
      _curUser = NULL;
      XMLAttribute *puser = attributes.Find("user");
      if (puser) {
        int ix = _users.FindKey(puser->value);
        if (ix!=-1)
        {
          _curUser = &_users[ix];
        }
        else { //add new user
          int ixNew = _users.Add(UserInfo(puser->value));
          _curUser = &_users[ixNew];
        }
      }
    }
    else
    {
      int i=0;
      for (; i<NStages; i++)
      {
        if (stricmp(stagesStr[i], name)==0)
        {
          _insideElement++;
          _currValue="";
          _currElement=TE_ANALYSIS+i;
          break;
        }
      }
    }
  }
  virtual void OnEndElement(RString name)
  {
    for (int i=0; i<NStages; i++)
    {
      if (stricmp(stagesStr[i], name)==0)
      {
        _insideElement--;
        _curUser->timeLeft[_currElement-TE_ANALYSIS]+=(float)atof(_currValue.Data());
      }
    }
  }
  virtual void OnCharacters(RString chars)
  {
    if (_insideElement > 0) _currValue = _currValue + chars;
  }
};

int main(int argc, char* argv[])
{
  QIFStream in;

  if (argc==2) in.open(argv[1]);
  else {
    printf("Usage: parseSummary.exe wikiToolFile.xml\n\n");
    return 1;
  }

  ExtractSummary parser;
  parser.Parse(in);
  FindArrayKey<UserInfo> users = parser.GetUsers();
  for (int i=0; i<users.Size(); i++)
  {
    float beta=0.0f;
    float alpha=0.0f;
    alpha = users[i].timeLeft[0]+users[i].timeLeft[1]+users[i].timeLeft[2];
    beta = alpha + users[i].timeLeft[3];
    printf("%s ... %.1f (alpha=%.1f)\n", cc_cast(users[i].name), beta, alpha); 
  }
}

#endif
