@echo off
set /P user=Username:
set /P pass=Password:
WikiTasks.exe Technology-ArmA %user% %pass% ttask ArmA
if ERRORLEVEL 13 pause There were errors while processing ArmA
msxsl arma_users.xml -pi -o ArmA_users.html
msxsl arma_users.xml extract_to_wiki.xsl -o ArmA_to_wiki.txt
WikiTasks.exe "Technology-ArmA_2" %user% %pass% ttask "ArmA 2"
if ERRORLEVEL 13 pause There were errors while processing ArmA 2
msxsl "ArmA 2_users.xml" -pi -o "ArmA2_users.html"
msxsl "ArmA 2_users.xml" extract_to_wiki.xsl -o ArmA2_to_wiki.txt
WikiTasks.exe Technology-VBS2 %user% %pass% ttask VBS2
if ERRORLEVEL 13 pause There were errors while processing VBS2
msxsl VBS2_users.xml -pi -o VBS2_users.html
msxsl VBS2_users.xml extract_to_wiki.xsl -o VBS2_to_wiki.txt
WikiTasks.exe Technology %user% %pass%
if ERRORLEVEL 13 pause There were errors while processing Technology
