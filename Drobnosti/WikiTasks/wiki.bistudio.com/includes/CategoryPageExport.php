<?php
if( !defined( 'MEDIAWIKI' ) )
	die();

class CategoryPage extends Article {
	function view() {
		if ( NS_CATEGORY == $this->mTitle->getNamespace() ) {
			$this->closeShowCategory();
		}
	}
	function printXML() {
		global $wgContLang,$wgUser, $wgCategoryMagicGallery;
		if ( NS_CATEGORY == $this->mTitle->getNamespace() ) {
			$articles = array();

			$dbr =& wfGetDB( DB_SLAVE );
			$pageCondition = '1';
			$res = $dbr->select(
				array( 'page', 'categorylinks' ),
				array( 'page_title', 'page_namespace', 'page_len', 'cl_sortkey' ),
				array( $pageCondition,
					'cl_from          =  page_id',
					'cl_to'           => $this->mTitle->getDBKey()),
				$fname,
				array( 'ORDER BY' => 'cl_sortkey') );
			
			$sk =& $wgUser->getSkin();
			$r = "<br style=\"clear:both;\"/>\n";
			while( $x = $dbr->fetchObject ( $res ) ) {
				
				$title = Title::makeTitle( $x->page_namespace, $x->page_title );
				
				if( $title->getNamespace() == NS_CATEGORY ) {
				} elseif( $wgCategoryMagicGallery && $title->getNamespace() == NS_IMAGE ) {
				} else {
					// Page in this category
					array_push( $articles, htmlspecialchars($title->getText()) ) ;
				}
			}
			$dbr->freeResult( $res );

			# Showing articles in this category

			$ti = htmlspecialchars( $this->mTitle->getText() );
			print "<CategoryExport category=\"{$ti}\">\n";
			//print "\t<Category>{$ti}</Category>\n";
			$this->printShortListXML( $articles );
			print "</CategoryExport>\n";
		}
	}

	# generate a list of subcategories and pages for a category
	# depending on wfMsg("usenewcategorypage") it either calls the new
	# or the old code. The new code will not work properly for some
	# languages due to sorting issues, so they might want to turn it
	# off.

	function closeShowCategory() {
		global $wgOut, $wgRequest;
		$wgOut->addHTML( $this->doCategoryMagic() );
	}

	/**
	 * Format the category data list.
	 *
	 * @return string HTML output
	 * @access private
	 */
	function doCategoryMagic( ) {
		global $wgContLang,$wgUser, $wgCategoryMagicGallery;
		$fname = 'CategoryPage::doCategoryMagic';
		wfProfileIn( $fname );

		$articles = array();

		$dbr =& wfGetDB( DB_SLAVE );
		$pageCondition = '1';
		$res = $dbr->select(
			array( 'page', 'categorylinks' ),
			array( 'page_title', 'page_namespace', 'page_len', 'cl_sortkey' ),
			array( $pageCondition,
			       'cl_from          =  page_id',
			       'cl_to'           => $this->mTitle->getDBKey()),
			$fname,
			array( 'ORDER BY' => 'cl_sortkey') );
		
		$sk =& $wgUser->getSkin();
		$r = "<br style=\"clear:both;\"/>\n";
		while( $x = $dbr->fetchObject ( $res ) ) {
			
			$title = Title::makeTitle( $x->page_namespace, $x->page_title );
			
			if( $title->getNamespace() == NS_CATEGORY ) {
			} elseif( $wgCategoryMagicGallery && $title->getNamespace() == NS_IMAGE ) {
			} else {
				// Page in this category
				array_push( $articles, $sk->makeSizeLinkObj( $x->page_len, $title, $wgContLang->convert( $title->getPrefixedText() ) ) ) ;
			}
		}
		$dbr->freeResult( $res );

		# Showing articles in this category
		$ti = htmlspecialchars( $this->mTitle->getText() );
		$r .= '<h2>' . wfMsg( 'category_header', $ti ) . "</h2>\n";
		$r .= $this->shortList( $articles );

		wfProfileOut( $fname );
		return $r;
	}

	/**
	 * Format a list of articles in a bullet list.
	 * @param array $articles
	 * @return string
	 * @access private
	 */
	function shortList( $articles ) {
		$r = '<ul><li>'.$articles[0].'</li>';
		for ($index = 1; $index < count($articles); $index++ )
		{
			$r .= "<li>{$articles[$index]}</li>";
		}
		$r .= '</ul>';
		return $r;
	}
	function printShortListXML( $articles ) {
		for ($index = 0; $index < count($articles); $index++ )
		{
			print "\t<Article>" . $articles[$index] . "</Article>\n";
		}
	}

}

?>
