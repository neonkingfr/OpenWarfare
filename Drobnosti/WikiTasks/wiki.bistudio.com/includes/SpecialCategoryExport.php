<?php
/**
 *
 * @package MediaWiki
 * @subpackage SpecialPage
 */

/** */
require_once( 'Revision.php' );

require_once( 'CategoryPageExport.php' );

/**
 *
 */
function wfSpecialCategoryExport( $page = '' ) {
	global $wgOut, $wgLang, $wgRequest, $wgContLanguageCode;

	# Pre-check the 'current version only' box in the UI
	$curonly = true;
	
	if( $page != '' ) {
		$parts = split(":", $page, 2);
		$namespace = $parts[0];
		if ($namespace === 'Category')
		{
/*
			//html export
			$category = $parts[1];
			$title = Title::makeTitle( NS_CATEGORY, $category);
			$categoryPage = new CategoryPage($title);
			$categoryPage->view();
*/
			//xml export
			$wgOut->disable();
  		    header( "Content-type: application/xml; charset=utf-8" );
			print "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n\n";
			$category = $parts[1];
			$title = Title::makeTitle( NS_CATEGORY, $category);
			$categoryPage = new CategoryPage($title);
			$categoryPage->printXML();
		}
		else
		{
			$wgOut->addHTML("Specified namespace \"$namespace\" is not category!");			
		}
		return;
	}
	$wgOut->addHTML("No output, as Category has not been specified!");
}
?>
