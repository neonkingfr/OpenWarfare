Nyni je treba pouzit plugin.
Informace lze najit v readme.txt v adresari plugin!

NASLEDUJICI INFORMACE JSOU PROTO OD 11.7.2007 OBSOLENTNI.







WikiTasks
=========
Zde je prehled zmen ve wiki, ktere je treba udelat pri instalaci nove wiki tak, 
aby fungoval tool WikiTasks, ktery z wiki cte.
Je treba upravit/vytvorit nasledujici soubory:

Nove vytvorene soubory: 
 - tyto soubory staci pouze zkopirovat
 includes/CategoryPageExport.php
 includes/SpecialCategoryExport.php 
Zmenene soubory:
 - zmeny jsou oznaceny komentarem obsahujicim "BISTUDIO extension"
 - lze ocekavat, ze s novou wiki budou tyto soubory trochu jine, takze je treba zmeny 
   pridat rucne (v kazdem souboru se jedna o dve radky, z nichz jedna je zmimeny komentar)
 includes/SpecialPage.php 
 languages/Messages.php ... od wiki 1.6.6
 zastarale: languages/Language.php ... pole $wgAllMessagesEn je premisteno do Messages.php

Testovani funkcnosti, kterou wikiTasks vyzaduje:
https://wiki.bistudio.com/index.php/Special:CategoryExport/Category:OFP2

(V lokalni wiki to bylo: http://testwiki.localdomain/index.php/Special:CategoryExport/Category:OFP2)
