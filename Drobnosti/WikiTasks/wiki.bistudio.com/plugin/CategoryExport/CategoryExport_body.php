<?php

class CategoryExport extends SpecialPage
{
        function CategoryExport() {
                SpecialPage::SpecialPage("CategoryExport");
                self::loadMessages();
        }

        function execute( $par ) {
            global $wgRequest, $wgOut;
#Rest of the URL should be category name
	    if ($par == '') {
# Provide a list of categories to export?
		$this->SetHeaders();
		$wgOut->addWikiText(wfMsg('selectcategory'));
		    $dbr = wfGetDB( DB_SLAVE );
		    $res = $dbr->select(
			array( 'categorylinks' ),
			array( 'cl_to','count(*) as count','cl_sortkey' ),
			array(),
        		__METHOD__,
			array('GROUP BY' => 'cl_to' )
			);
		    while ($row = $dbr->fetchObject($res)) {
			$category = Title::makeTitle(NS_CATEGORY, $row->cl_to);
			$wgOut->addWikiText("[[Special:CategoryExport/".$category->getFullText()."|".$category->getText()."]]");
		    }

		    $dbr->freeResult($res);


		$wgOut->addWikiText('[[Special:CategoryExport/Category:Technology-ArmA 2|Technology-ArmA 2]]');
	    } else {
		$title = Title::newFromURL($par);
		if ($title->getNamespace() !=  NS_CATEGORY) {
# Error: expecting category link...
		    $this->SetHeaders();
		    $wgOut->showErrorPage('error','badarticleerror');
		} else {
# ok export category...
#		    $this->SetHeaders();
		    $category_name = $title->getText();
		    $category_dbkey = $title->getDBKey();

		    $wgOut->disable();
		    $res = $wgRequest->response();
		    $res->header("HTTP/1.1 200 OK");
		    $res->header("Cache-Control: max-age=0");
		    $res->header("Content-Type: text/xml; charset=utf-8");
		    
		    print '<?xml version="1.0" encoding="utf-8"?>';

		    print wfOpenElement("CategoryExport", array('category' => $category_name /*, 'db_key' => $category_dbkey*/ ));

		    $dbr = wfGetDB( DB_SLAVE );
		    $res = $dbr->select(
			array( 'page', 'categorylinks' ),
			array( 'page_title', 'page_namespace', 'cl_sortkey' ),
			array( 'cl_from = page_id', 'cl_to = '.$dbr->addQuotes($category_dbkey) ),
        		__METHOD__,
			array('ORDER BY' => 'cl_sortkey')			
			);
		    while ($row = $dbr->fetchObject($res)) {
			$article = Title::makeTitle($row->page_namespace, $row->page_title);
			$article_ns = $article->getNamespace();
			if (($article_ns != NS_CATEGORY) && ($article_ns != NS_IMAGE)) {
			    print wfElement('Article',array("partialurl"=>$article->getPartialURL(),"prefixedurl"=>$article->getPrefixedURL(),"namespace"=>$article->getNamespace(),"dbkey"=>$article->getDBKey()),$article->getText());
			}
		    }

		    $dbr->freeResult($res);

		    print wfCloseElement("CategoryExport");

		}
	    }
        }

        function loadMessages() {
                static $messagesLoaded = false;
                global $wgMessageCache;
                if ( $messagesLoaded ) return;
                $messagesLoaded = true;

                require( dirname( __FILE__ ) . '/CategoryExport_i18n.php' );
                foreach ( $allMessages as $lang => $langMessages ) {
                        $wgMessageCache->addMessages( $langMessages, $lang );
                }
        }
}
?>
