<?php
# Not a valid entry point, skip unless MEDIAWIKI is defined
if (!defined('MEDIAWIKI')) {
        echo <<<EOT
To install my extension, put the following line in LocalSettings.php:
require_once( "\$IP/extensions/CategoryExport/CategoryExport.php" );

EOT;
        exit( 1 );
}

$wgAutoloadClasses['CategoryExport'] = dirname(__FILE__) . '/CategoryExport_body.php';
$wgSpecialPages['CategoryExport'] = 'CategoryExport';
$wgHooks['LoadAllMessages'][] = 'CategoryExport::loadMessages';
?>
