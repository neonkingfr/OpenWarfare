ServerSide WikiTasks Extension
==============================

WikiTasks pou��v� na stran� wiki serveru extenzi, kter� je implementov�na pluginem.
Str�nka, kterou WikiTasks vyt�uje je na: 
  https://wiki.bistudio.com/index.php/Special:CategoryExport
WikiTasks pak nakonec je�t� p�id�vaj� kategorii, o kterou maj� z�jem. 
Nap��klad:
  https://wiki.bistudio.com/index.php/Special:CategoryExport/Category:ArmA_2

Organizace zdroj�k�
===================
Zdroj�ky pluginu je t�eba ulo�it do: (ko�en cesty m��e m�nit podle toho, kde wiki na serveru s�dl�)
  /data/web/wiki.bistudio.com/content_ssl/extensions/CategoryExport

Plugin tvo�� t�i soubory, kter� lze naj�t ve VSS v 
 * CategoryExport_body.php ... vlastn� PHP pro generov�n� wiki page
 * CategoryExport_i18n.php ... internacionalizace (lokalizace)
 * CategoryExport.php ... instalace hooku

Vlastn� extenze se p�id� do local settings:
  /data/web/wiki.bistudio.com/content_ssl/LocalSettings.php
Na konci je "Hlav��ova extenze":

# Hlav��ova extenze pro export kategori�:
require_once( "$IP/extensions/CategoryExport/CategoryExport.php" );
