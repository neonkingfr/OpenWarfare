<?xml version="1.0" encoding="ISO-8859-2"?>
<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="Task/Name">
</xsl:template>

<xsl:template match="TimeLeft">
	<xsl:apply-templates select="child::*"/>
</xsl:template>

<xsl:template match="Analysis">
	<xsl:text>(</xsl:text><xsl:value-of select="."/><xsl:text>, </xsl:text>
</xsl:template>
<xsl:template match="Prototype">
	<xsl:value-of select="."/><xsl:text>, </xsl:text>
</xsl:template>
<xsl:template match="Alpha">
	<xsl:value-of select="."/><xsl:text>, </xsl:text>
</xsl:template>
<xsl:template match="Beta">
	<xsl:value-of select="."/><xsl:text>)</xsl:text>
</xsl:template>
<xsl:template match="Tasks">
	<xsl:text>&nbsp;&nbsp; </xsl:text>
</xsl:template>

<xsl:template name="ListTasks">
	<ul>
	<xsl:for-each select="Tasks/Task">
		<xsl:sort select="Name"/>
		<li><a><xsl:attribute name="href"><xsl:text>https://wiki.bistudio.com/index.php/</xsl:text><xsl:value-of select="Name"/></xsl:attribute><xsl:value-of select="Name"/></a>&nbsp;<i><xsl:apply-templates select="child::*"/></i></li>
	</xsl:for-each>
	</ul>
</xsl:template>

<xsl:template name="ShowTasks">
	&nbsp;&nbsp;
	<a>
		<xsl:attribute name="href">#</xsl:attribute>
		<xsl:attribute name="OnClick">
			<xsl:text>Toggle(</xsl:text>
			<xsl:value-of select="../@name"/><xsl:value-of select="name()"/>
			<xsl:text>)</xsl:text>
		</xsl:attribute>
		<xsl:text>Show Tasks</xsl:text>
	</a>
	<div style="display:none">
		<xsl:attribute name="id">
			<xsl:value-of select="../@name"/><xsl:value-of select="name()"/>
		</xsl:attribute>
		<xsl:choose>
			<xsl:when test="name()='Total'">
				<ul>
				<li><b>All tasks:</b></li>
				<xsl:call-template name="ListTasks"/>
				</ul>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="ListTasks"/>
			</xsl:otherwise>
		</xsl:choose> 
	</div>
</xsl:template>

<xsl:template name="ShowInfo">
	<xsl:apply-templates select="child::*"/>
	<xsl:call-template name="ShowTasks"/>
</xsl:template>

<xsl:template match="Total">
	<xsl:call-template name="ShowInfo"/>
</xsl:template>

<xsl:template name="ShowDetails">
	<xsl:if test="count(Tasks/Task) &gt; 0">
	<ul>
 		<li><b><xsl:value-of select="name()"/>:&nbsp;</b>
			<xsl:call-template name="ShowInfo"/>
		</li>
	</ul>
	</xsl:if>
</xsl:template>

<xsl:template match="Required">
	<xsl:call-template name="ShowDetails"/>
</xsl:template>

<xsl:template match="Desired">
	<xsl:call-template name="ShowDetails"/>
</xsl:template>

<xsl:template match="NiceToHave">
	<xsl:call-template name="ShowDetails"/>
</xsl:template>

<xsl:template match="/">
  <html>
	<head>
		<SCRIPT LANGUAGE="javascript">
		<xsl:text>&lt;!--
	function Toggle (obj)
	{
	if (obj.style.display == "") obj.style.display = "none";
					else obj.style.display= "";
	return false;
	}
		--&gt;</xsl:text>
		</SCRIPT>
	</head>
  <body>
  <h2><xsl:value-of select="UserTasks/@category"/> Tasks List by Users</h2>
    <ul>
    <xsl:for-each select="UserTasks/User">
    <xsl:sort select="@name"/>
		<li><b><xsl:value-of select="@name"/>:&nbsp;</b>
		<xsl:apply-templates select="Total"/>
		</li>
		<xsl:apply-templates select="Required"/>
		<xsl:apply-templates select="Desired"/>
		<xsl:apply-templates select="NiceToHave"/>
    </xsl:for-each>
    </ul>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>
