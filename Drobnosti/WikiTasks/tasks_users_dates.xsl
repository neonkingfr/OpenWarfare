<?xml version="1.0" encoding="ISO-8859-2"?>
<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="UserTask/Name">
</xsl:template>
<xsl:template match="Order">
</xsl:template>
<xsl:template match="Tasks">
</xsl:template>
<xsl:template match="Stage">
</xsl:template>
<xsl:template match="Priority">
</xsl:template>
<xsl:template match="Order">
</xsl:template>

<xsl:template match="TimeLeft">
	<xsl:apply-templates select="child::*"/>
</xsl:template>

<xsl:template match="Analysis">
</xsl:template>
<xsl:template match="Prototype">
	<xsl:if test="name(..)='UserTotals' or name(..)='Dates'">
	<TD>&nbsp;<xsl:value-of select="."/>&nbsp;</TD>
	</xsl:if>
</xsl:template>
<xsl:template match="Alpha">
	<xsl:if test="name(..)='UserTotals' or name(..)='Dates'">
	<TD>&nbsp;<xsl:value-of select="."/>&nbsp;</TD>
	</xsl:if>
</xsl:template>
<xsl:template match="Beta">
	<xsl:if test="name(..)='UserTotals' or name(..)='Dates'">
	<TD>&nbsp;<xsl:value-of select="."/>&nbsp;</TD>
	</xsl:if>
</xsl:template>
<xsl:template match="Date">
	<td><xsl:value-of select="Day"/>.<xsl:value-of select="Month"/>.<xsl:value-of select="Year"/></td>
</xsl:template>

<xsl:template match="UserTasks">
	<xsl:text>&nbsp;&nbsp;</xsl:text>
</xsl:template>
<xsl:template match="Total">
	<tr>
	<th><xsl:value-of select="name()"/></th>
	<xsl:apply-templates select="child::*"/>
	<xsl:call-template name="ShowTasks"/>
	</tr>
	<tr><td colspan="6"><div style="display:none">
		<xsl:attribute name="id">
			<xsl:value-of select="../@name"/><xsl:value-of select="name()"/>
		</xsl:attribute>
		<table border="0">
			<tr align="left"><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td><b>Task name</b></td><th>Stage</th><th>Date</th><th>Priority</th><th>Order</th></tr>
			<xsl:call-template name="ListTasksTotal"/>
		</table>
	</div></td></tr>
</xsl:template>

<xsl:template name="ListTasks">
	<xsl:for-each select="UserTasks/UserTask">
		<tr>
		<td></td>
		<td><a><xsl:attribute name="href"><xsl:text>https://wiki.bistudio.com/index.php/</xsl:text><xsl:value-of select="Name"/></xsl:attribute><xsl:value-of select="Name"/></a></td>
		<xsl:apply-templates select="child::*"/>
		</tr>
	</xsl:for-each>
</xsl:template>

<xsl:template name="ListTasksTotal">
	<xsl:for-each select="UserTasks/UserTask">
		<tr>
		<td></td>
		<td><a><xsl:attribute name="href"><xsl:text>https://wiki.bistudio.com/index.php/</xsl:text><xsl:value-of select="Name"/></xsl:attribute><xsl:value-of select="Name"/></a></td>
		<td><i><xsl:value-of select="Stage"/></i>&nbsp;</td>
		<xsl:apply-templates select="Date"/>
		<td><i><xsl:value-of select="Priority"/></i>&nbsp;</td>
		<td><i><xsl:value-of select="Order"/></i>&nbsp;</td>
		</tr>
	</xsl:for-each>
</xsl:template>

<xsl:template name="ShowTasks">
	<td>
	<a>
		<xsl:attribute name="href">#fake<xsl:value-of select="../@name"/><xsl:value-of select="name()"/></xsl:attribute>
		<xsl:attribute name="OnClick">
			<xsl:text>Toggle(</xsl:text>
			<xsl:value-of select="../@name"/><xsl:value-of select="name()"/>
			<xsl:text>)</xsl:text>
		</xsl:attribute>
		<xsl:text>Show Tasks</xsl:text>
	</a>
	</td>
</xsl:template>

<xsl:template name="ShowInfo">
	<tr>
	<th><xsl:value-of select="name()"/></th>
	<xsl:apply-templates select="child::*"/>
	<xsl:call-template name="ShowTasks"/>
	</tr>
	<tr><td colspan="6"><div style="display:none">
		<xsl:attribute name="id">
			<xsl:value-of select="../@name"/><xsl:value-of select="name()"/>
		</xsl:attribute>
		<table border="0">
			<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td><b>Task name</b></td><th>Prototype</th><th>Alpha</th><th>Beta</th></tr>
			<xsl:call-template name="ListTasks"/>
		</table>
	</div></td></tr>
</xsl:template>

<xsl:template match="UserTotal">
	<xsl:call-template name="ShowInfo"/>
</xsl:template>

<xsl:template name="ShowDetails">
	<xsl:if test="count(UserTasks/UserTask) &gt; 0">
		<xsl:call-template name="ShowInfo"/>
	</xsl:if>
</xsl:template>

<xsl:template match="Required">
	<xsl:call-template name="ShowDetails"/>
</xsl:template>

<xsl:template match="Desired">
	<xsl:call-template name="ShowDetails"/>
</xsl:template>

<xsl:template match="NiceToHave">
	<xsl:call-template name="ShowDetails"/>
</xsl:template>

<xsl:template match="/">
  <html>
	<head>
		<SCRIPT LANGUAGE="javascript">
		<xsl:text>&lt;!--
	function Toggle (obj)
	{
	if (obj.style.display == "") obj.style.display = "none";
					else obj.style.display= "";
	return false;
	}
		--&gt;</xsl:text>
		</SCRIPT>
	</head>
  <body>
  <div align="left"><h1><xsl:value-of select="UserTasks/@category"/> Tasks List by Users</h1></div>
    <xsl:for-each select="UserTasks/User">
    <xsl:sort select="@name"/>
		<table border="1" cellspacing="0pt" cellpadding="1pt">
		<CAPTION><h2><xsl:value-of select="@name"/></h2></CAPTION>
		<TR><TH>&nbsp;</TH><TH>Prototype</TH><TH>Alpha</TH><TH>Beta</TH><TH>&nbsp;</TH></TR>
		<xsl:apply-templates select="Total"/>
		<xsl:apply-templates select="Required"/>
		<xsl:apply-templates select="Desired"/>
		<xsl:apply-templates select="NiceToHave"/>
		</table>
    </xsl:for-each>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>
