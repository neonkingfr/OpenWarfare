<?xml version="1.0" encoding="ISO-8859-2"?>
<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" indent="no"/>


<xsl:template match="/">
<xsl:for-each select="Tasks/Task/Name">
[[<xsl:value-of select="."/>]]<xsl:text>&#xA;</xsl:text>
</xsl:for-each>
</xsl:template>

</xsl:stylesheet>

