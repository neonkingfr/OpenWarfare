#include <El/elementpch.hpp>
#include <Es/Common/win.h>
#include <Es/Strings/rString.hpp>

#include <El/XML/inet.hpp>
#include <El/XML/xml.hpp>
#include <El/QStream/qStream.hpp>
#include <El/ParamFile/paramFile.hpp>

#include "WikiTasks.h"

QOStream &operator<<(QOStream &out, const float f)
{
  char buff[64];
  sprintf(buff, "%.1f", f);
  return (out << buff);
}
QOStream &operator<<(QOStream &out, const int i)
{
  char buff[64];
  sprintf(buff, "%d", i);
  return (out << buff);
}

enum TasksElements
{
  TE_TASKS, TE_TASK, 
  TE_NAME, 
  TE_PRIORITIES, TE_PRIORITY, 
  TE_ASSIGNES, TE_ASSIGN, 
  TE_ANALYSIS, TE_PROTOTYPE, TE_ALPHA, TE_BETA,
  TE_DEPENDENCIES, TE_DEPENDENCY
};

#ifdef WIKI_TOOLS_XML
//! free Win32 allocated memory when variable is destructed
class AutoFree
{
  //! handle to Win32 memory obtained by GlobalAlloc call
  void *_mem;

private:
  //! no copy constructor
  AutoFree(const AutoFree &src);

public:
  //! attach Win32 memory handle
  /*!
  \param mem handle to Win32 memory obtained by GlobalAlloc call
  */
  void operator = (void *mem)
  {
    if (mem==_mem) return;
    Free();
    _mem = mem;
  }
  //! copy - reassing Win32 handle to new object
  /*! source will be NULL after assignement */
  void operator = (AutoFree &src)
  {
    if (src._mem == _mem) return;
    Free();
    _mem = src._mem;
    src._mem = NULL;
  }
  //! empty constructor
  AutoFree(){_mem = NULL;}
  //! construct from Win32 memory handle
  AutoFree(void *mem){_mem = mem;}
  //! convert to pointer
  operator void * () {return _mem;}
  //! destruct - free pointer
  ~AutoFree(){Free();}
#ifdef _WIN32
  //! free pointer explicitelly
  void Free(){if (_mem) GlobalFree(_mem),_mem = NULL;}
#else
  //! free pointer explicitelly
  void Free(){if (_mem) free(_mem),_mem = NULL;}
#endif
};

class ExtractText : public SAXParser
{
protected:
  RString _text;
  int _textElement;

public:
  ExtractText() {_textElement = 0;}
  RString GetText() const {return _text;}

  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "text") == 0) _textElement++;
  }
  virtual void OnEndElement(RString name)
  {
    if (stricmp(name, "text") == 0) _textElement--;
  }
  virtual void OnCharacters(RString chars)
  {
    if (_textElement > 0) _text = _text + chars;
  }
};

RString ConvertCharset(RString in)
{
  const int bufferSize = 8192;
  WCHAR buffer[bufferSize];
  MultiByteToWideChar(CP_UTF8, 0, in, -1, buffer, bufferSize);
  char out[bufferSize];
  WideCharToMultiByte(CP_ACP, 0, buffer, -1, out, bufferSize, NULL, NULL);
  return out;
}

template<int size>
void strCat(TempString<size> &out, const char *str)
{
  while(*str)
  {
    out.Add(*str);
    str++;
  }
}

RString ConvertSpecialChars(RString in)
{
  TempString<1024> out;
  for (int i=0, siz=in.GetLength(); i<siz; i++)
  {
    switch (in[i])
    {
    case '&': strCat(out,"&amp;"); break;
    case '<': strCat(out,"&lt;"); break;
    case '>': strCat(out,"&gt;"); break;
    case '"': strCat(out,"&quot;"); break;
    case '\'': strCat(out,"&apos;"); break;
    default: out.Add(in[i]);
    }
  }
  out.Add(0);
  return out;
}

class ExtractArticles : public SAXParser
{
private:
  RString _currentArticle;
protected:
  RString _category;
  AutoArray<RString> _articleNames;  
  int _articleElement;

public:
  ExtractArticles() {_articleElement = 0; }
  AutoArray<RString> &GetArticles() {return _articleNames;}
  RString GetCategory() const {return _category; }

  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "CategoryExport") == 0) {
      XMLAttribute *pcatg = attributes.Find("category");
      if (pcatg) _category = pcatg->value;
    }
    else if (stricmp(name, "article") == 0) {
      _currentArticle=RString("");
      _articleElement++;
    }
  }
  virtual void OnEndElement(RString name)
  {
    if (stricmp(name, "article") == 0) {
      _articleElement--;
      _articleNames.Add(_currentArticle);
    }
  }
  virtual void OnCharacters(RString chars)
  {
    if (_articleElement > 0) _currentArticle = _currentArticle + chars;
  }
};

struct Assign
{
  RString user;
  float timeLeft[NStages];
  Assign()
  {
    for (int i=0; i<NStages; i++) timeLeft[i] = 0.0f;
  }
  Assign(const char *parameters);
};
TypeIsMovable(Assign)

Assign::Assign(const char *p)
{
  for (int i=0; i<NStages; i++) timeLeft[i]=0.0f;
  const char *f = strchr(p, '|');
  if (f)
  {
    user = RString (p, f - p);
    for (int i=0; i<NStages; i++)
    {
      p = f+1;
      f = strchr(p, '|');
      if (f) 
      {
        RString num = RString (p, f - p);
        timeLeft[i] = (float)atof(cc_cast(num));
      } 
      else 
      {
        RString num = RString (p);
        timeLeft[i] = (float)atof(cc_cast(num));
        break; //all other are zero as default
      }
    }
  }
}

struct UserDefault
{
  RString name;
  float daysInWeek;
  UserDefault() : daysInWeek(0.0f) {}
  UserDefault(const char *name1) : daysInWeek(0.0f), name(name1) {}
};
TypeIsMovable(UserDefault)

class WikiTasksDefaults : public SAXParser
{
private:
  RString _curValue;
  int _prIx, _stIx;
  int _usIx;
protected:
  int _defaultElement;
public:
  int orders[NPriorities][NStages];
  AutoArray<UserDefault> users;
  WikiTasksDefaults()
  {
    for (int i=0; i<NPriorities; i++)
      for (int j=0; j<NStages; j++) orders[i][j]=0;
    QIFStream in;
    in.open("wikiTasksDefaults.xml");
    //initialize parser
    _defaultElement = 0; _prIx=_stIx=-1;
    Parse(in);
  }

public:
  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "order") == 0) {
      XMLAttribute *ppr = attributes.Find("priority");
      if (ppr) {
        XMLAttribute *pst = attributes.Find("stage");
        if (pst) {
          _prIx=GetPriorityIx(ppr->value);
          _stIx=GetStageIx(pst->value);
        }
      }
      _defaultElement++;
    }
    else if (stricmp(name, "user") == 0) {
      XMLAttribute *pname = attributes.Find("name");
      if (pname) {
        _usIx=-1;
        for (int i=0; i<users.Size(); i++)
          if (stricmp(cc_cast(users[i].name),pname->value)==0) {_usIx=i; break;}
          if (_usIx==-1) _usIx = users.Add(UserDefault(pname->value));
          _defaultElement++;
      }
    }
  }
  virtual void OnEndElement(RString name)
  {
    if (stricmp(name, "order") == 0) {
      _defaultElement--;
      if (_prIx!=-1 && _stIx!=-1) {
        orders[_prIx][_stIx]=atoi(cc_cast(_curValue));
      }
    }
    else if (stricmp(name, "user") == 0) {
      _defaultElement--;
      users[_usIx].daysInWeek=(float)atof(_curValue);
    }
    _curValue="";
  }
  virtual void OnCharacters(RString chars)
  {
    if (_defaultElement > 0) _curValue = _curValue + chars;
  }
};

struct Page
{
  struct Order {
    int orders[NStages];
    Order() 
    { 
      for(int i=0; i<NStages; i++) orders[i]=-1; //if not specified in wiki page, should be set to default value
    }
    void Update(const char *f);
  };
  RString name;
  int priority[NProducts];
  AutoArray<RString> dependencies;
  AutoArray<RString> doAfter;
  AutoArray<Assign> assignes[NProducts];
  Order order;
  int level;
  int stage[NProducts];
  
  Page()
  {
    for (int i=0; i<NProducts; i++) 
    {
      priority[i] = 0;
      stage[i] = -1;
    }
    level=-1;
  }
  Page(RString n)
  {
    name = n;
    level=-1;
    for (int i=0; i<NProducts; i++) 
    {
      priority[i] = 0;
      stage[i] = -1;
    }
  }
  void ConvertCharset()
  {
    name = ::ConvertCharset(name);
    for (int i=0; i<dependencies.Size(); i++) dependencies[i]=::ConvertCharset(dependencies[i]);
    for (int i=0; i<doAfter.Size(); i++) doAfter[i]=::ConvertCharset(doAfter[i]);
  }
  void ConvertSpecialChars()
  {
    name = ::ConvertSpecialChars(name);
    for (int i=0; i<dependencies.Size(); i++) dependencies[i]=::ConvertSpecialChars(dependencies[i]);
    for (int i=0; i<doAfter.Size(); i++) doAfter[i]=::ConvertSpecialChars(doAfter[i]);
  }
  void UpdateOrder(WikiTasksDefaults &defaults, int categoryIx)
  {
    for (int i=0; i<NStages; i++)
    {
      if (order.orders[i]==-1) order.orders[i] = defaults.orders[priority[categoryIx]][i];
      order.orders[i]+=level*100;
    }
  }
  //updates assignes timeleft using stage (zero all times for stages done)
  void UpdateAssignesUsingStage();
};
TypeIsMovable(Page)

void Page::Order::Update(const char *p)
{
  for (int i=0; i<NStages; i++) orders[i]=-1;
  const char *f = NULL;
  for (int i=0; i<NStages; i++)
  {
    if (i==NStages-1) {
      RString num(p);
      orders[i] = atoi(cc_cast(num));
    }
    else {
      f = strchr(p, '|');
      if (f) {
        RString num = RString (p, f - p);
        orders[i] = atoi(cc_cast(num));
      } 
      else break; //all other are zero as default
      p = f+1;
    }
  }
}

class Pages: public AutoArray<Page>
{
public:
  Pages(AutoArray<RString> &articleNames);
  void ConvertCharset()
  {
    for (int i=0; i<Size(); i++) operator[](i).ConvertCharset();
  }
  void ConvertSpecialChars()
  {
    for (int i=0; i<Size(); i++) operator[](i).ConvertSpecialChars();
  }
  //compute level of each task in POSET of doAfter graph
  int ComputeDoAfterPosetLevels();
  void UpdateOrders(WikiTasksDefaults &defaults, int productIx);
};

Pages::Pages(AutoArray<RString> &articleNames) : AutoArray<Page>()
{
  for (int i=0; i<articleNames.Size(); i++)
  {
    Add(Page(articleNames[i]));
  }
}
void Pages::UpdateOrders(WikiTasksDefaults &defaults, int productIx)
{
  for (int i=0; i<Size(); i++)
  {
    operator[](i).UpdateOrder(defaults, productIx);
  }
}

struct TaskIndex
{
  AutoArray<int> doAfter; //indexes of Pages, that this task declares as doAfter
  int level; //computed level. -1 for t.b.d.
  TaskIndex() {level=-1;}
};
TypeIsMovable(TaskIndex);

int Pages::ComputeDoAfterPosetLevels()
{
  AutoArray<TaskIndex> tindex;
  int siz=Size();
  tindex.Realloc(siz);
  tindex.Resize(siz);
  for (int i=0; i<siz; i++)
  {
    const Page &page = operator[](i);
    if (page.doAfter.Size()!=0)
    { //determine tindex[i].doAfter index
      for (int di=0, disiz=page.doAfter.Size(); di<disiz; di++)
      {
        for (int j=0; j<siz; j++)
        {
          if (stricmp(page.doAfter[di], operator[](j).name)==0) 
          {
            tindex[i].doAfter.Add(j); break; //maybe some Assert when not found?
          }
        }
      }
    }
    if (page.dependencies.Size()!=0)
    { //determine tindex[i].doAfter index
      for (int di=0, disiz=page.dependencies.Size(); di<disiz; di++)
      {
        for (int j=0; j<siz; j++)
        {
          if (stricmp(page.dependencies[di], operator[](j).name)==0) 
          {
            tindex[i].doAfter.Add(j); break; //maybe some Assert when not found?
          }
        }
      }
    }
  }
  int passNo=0;
  bool done;
  do {
    done = true;
    for (int i=0; i<siz; i++)
    {
      if (tindex[i].level==-1)
      { //try to determine
        int lev=0;
        for (int j=0, disiz=tindex[i].doAfter.Size(); j<disiz; j++)
        {
          if (tindex[tindex[i].doAfter[j]].level==-1) {lev=-1; break;} //cannot determine
          saturateMax(lev,tindex[tindex[i].doAfter[j]].level+1);
        }
        if (lev!=-1) operator[](i).level=tindex[i].level=lev;
        else done=false;
      }
    }
    if ( (passNo++)>siz ) {
      //the underlying graph of doAfter is not POSET!
      printf("Error: unable to sort using doAfter wiki template. Reasoning cycles follows:\n");
      int mark=-2;
      for (int i=0; i<siz; i++) {
        if (tindex[i].level==-1) { //print cycle
          printf("Cycle:\n");
          int next=i;
          do {
            printf("  %s\n",cc_cast(operator[](next).name));
            tindex[next].level=mark; //this task is marked as already printed
            for (int j=0, disiz=tindex[next].doAfter.Size(); j<disiz; j++)
            {
              int newNext = tindex[next].doAfter[j];
              if (tindex[newNext].level==mark) {
                printf("  %s\n",cc_cast(operator[](newNext).name));
                next=-1; //cycle printed
                break;
              }
              else if (tindex[newNext].level<0) { next=newNext; break; }
            }
          } while (next>=0);
        }
        mark--;
      }
      for (int i=0; i<siz; i++) if (operator[](i).level<0) operator[](i).level=0;
      return 13; //retval to possible testing using ERRORLEVEL
    }
  } while (!done);
  return 0;
}

//example: if stage is alfa, then all times for alfa, prototype and analysis should be zeroed
void Page::UpdateAssignesUsingStage()
{
  for (int pr=0; pr<NProducts; pr++)
  {
    for (int i=0, len = assignes[pr].Size(); i<len; i++)
    {
      for (int k=0; k<=stage[pr]; k++) assignes[pr][i].timeLeft[k]=0;
    }
  }
}

void ProcessPage(Page &page, RString text, const char *templatePrefix)
{
  const char *beg = "{{";
  const char *end = "}}";

  const char *ptr = text;
  RString templateName;
  while (ptr = strstr(ptr, beg))
  {
    ptr += strlen(beg);
    const char *p2 = strstr(ptr, end);
    if (!p2) break;
    {
      RString record(ptr, p2 - ptr);
      const char *p = record;
      const char *f = strchr(p, '|');
      if (f)
      {
        RString name(p, f - p);
        templateName=RString(templatePrefix)+RString("-priority");
        if (stricmp(name, cc_cast(templateName)) == 0)
        {
          p = f + 1;
          f = strchr(p, '|');
          if (f)
          {
            RString product(p, f - p);
            RString priority(f + 1);
            int pri = 0;
            if (stricmp(priority, "NiceToHave") == 0) pri = 1;
            else if (stricmp(priority, "Desired") == 0) pri = 2;
            else if (stricmp(priority, "Required") == 0) pri = 3;
            int pro = -1;
            for (int cati=0; cati<NProducts; cati++)
            {
              if (stricmp(product, productsStr[cati])==0) {pro=cati; break; }
            }
            if (pro >= 0 && pri > 0) page.priority[pro] = pri;
          }
        }
        else {
          do {
            templateName=RString(templatePrefix)+RString("-dependency");
            if (stricmp(name, cc_cast(templateName)) == 0)
            {
              page.dependencies.Add(f+1); break;
            }
            templateName=RString(templatePrefix)+RString("-assign");
            if (stricmp(name, cc_cast(templateName)) == 0)
            {
              for (int pr=0; pr<NProducts; pr++)
              {
                page.assignes[pr].Add(Assign(f+1));
              }
              break;
            }
            templateName=RString(templatePrefix)+RString("-order");
            if (stricmp(name, cc_cast(templateName)) == 0)
            {
              page.order.Update(f+1); break;
            }
            templateName=RString(templatePrefix)+RString("-doAfter");
            if (stricmp(name, cc_cast(templateName)) == 0)
            {
              page.doAfter.Add(f+1); break;
            }
            templateName=RString(templatePrefix)+RString("-stage");
            if (stricmp(name, cc_cast(templateName)) == 0)
            {
              for (int i=0; i<NStages; i++)
              {
                if (stricmp(stagesStr[i],f+1)==0) 
                {
                  for (int pr=0; pr<NProducts; pr++)
                  {
                    page.stage[pr]=i;
                  }
                  break;
                }
              }
              break;
            }
            templateName=RString(templatePrefix)+RString("-desc");
            if (stricmp(name, cc_cast(templateName)) == 0)
            {
              // ttask-desc|ArmA 2|Desired|Task|Ondra|2|2|4|4
              p = f + 1; f = strchr(p, '|');
              if (f)
              {
                // product
                RString product(p, f - p);
                // priority
                p = f + 1; f = strchr(p, '|');
                if (!f) break;
                RString priority(p, f - p);
                int pri = 0;
                if (stricmp(priority, "NiceToHave") == 0) pri = 1;
                else if (stricmp(priority, "Desired") == 0) pri = 2;
                else if (stricmp(priority, "Required") == 0) pri = 3;
                int pro = -1;
                for (int cati=0; cati<NProducts; cati++)
                {
                  if (stricmp(product, productsStr[cati])==0) { pro=cati; break; }
                }
                if (pro >= 0 && pri > 0) page.priority[pro] = pri;
                // Stage
                p = f + 1; f = strchr(p, '|');
                if (!f) break;
                RString stage(p, f - p);
                if (pro >= 0)
                {
                  for (int i=0; i<NStages; i++)
                  {
                    if (stricmp(stagesStr[i],stage)==0) 
                    {
                      page.stage[pro]=i;
                      break;
                    }
                  }
                }
                // assign
                if (pro >= 0) page.assignes[pro].Add(Assign(f+1));
              }
              break;
            }
          } while (false);
        }
      }
    }
    ptr = p2 + strlen(end);
  }
}

void ExportXML(Pages &pages, RString categoryName, int productIx)
{
  RString outXmlFileName = categoryName + ".xml";
  QOFStream out(cc_cast(outXmlFileName));
  out << "<?xml version=\"1.0\" encoding=\"windows-1250\"?>\r\n"
    "<?xml-stylesheet href=\"tasks.xsl\" type=\"text/xsl\"?>\r\n\r\n";

  out << "<Tasks category=\"" << categoryName << "\">\r\n";
  for (int i=0; i<pages.Size(); i++)
  {
    Page &page = pages[i];
    bool insertThisPage=false;
    for (int j=0; j<NStages; j++) 
    {
      if (page.order.orders[j]) {insertThisPage=true; break; }
    }
    if (!insertThisPage) continue;
    if (!page.priority[productIx]) continue;
    out << "\t<Task>\r\n";
    //name
    out << "\t\t<Name>" << page.name << "</Name>\r\n";
/*
    //priorities
    out << "\t\t<Priorities>\r\n";
    for (int j=0; j<NProducts; j++) {
      if (page.priority[j]) out << "\t\t\t<Priority product=\"" << productsStr[j] << "\">" << prioritiesStr[page.priority[j]] << "</Priority>\r\n";
    }
    out << "\t\t</Priorities>\r\n";
*/
    //priority
    out << "\t\t<Priority>" << prioritiesStr[page.priority[productIx]] << "</Priority>\r\n";
    //order
    out << "\t\t<Orders>\r\n";
    for (int j=0; j<NStages; j++) out << "\t\t\t<Order stage=\"" << stagesStr[j] << "\">" << page.order.orders[j] << "</Order>\r\n";
    out << "\t\t</Orders>\r\n";
    //timeleft
    out << "\t\t<TimeLeft>\r\n";
    for (int j=0; j<NStages; j++) 
    {
      float tmleft=0;
      for (int k=0; k<page.assignes[productIx].Size(); k++) tmleft += page.assignes[productIx][k].timeLeft[j];
      out << "\t\t\t<Time stage=\"" << stagesStr[j] << "\">" << tmleft << "</Time>\r\n";
    }
    out << "\t\t</TimeLeft>\r\n";
    //assignes
    out << "\t\t<Assignes>\r\n";
    for (int j=0; j<page.assignes[productIx].Size(); j++) {
      out << "\t\t\t<Assign user=\"" << page.assignes[productIx][j].user << "\">\r\n";
      for (int k=0; k<NStages; k++)
      {
        out << "\t\t\t\t<" << stagesStr[k] << ">" << page.assignes[productIx][j].timeLeft[k] << "</" << stagesStr[k] << ">\r\n";
      }
      out << "\t\t\t</Assign>\r\n";
    }
    out << "\t\t</Assignes>\r\n";
    //dependencies
    out << "\t\t<Dependencies>\r\n";
    for (int j=0; j<page.dependencies.Size(); j++) {
      out << "\t\t\t<Dependency>" << page.dependencies[j] << "</Dependency>\r\n";
    }
    out << "\t\t</Dependencies>\r\n";
    out << "\t</Task>\r\n";
  }
  out << "</Tasks>\r\n";
}

struct TaskDate
{
  int year,month,day;
  TaskDate() {year=month=day=0;}
  TaskDate(int y, int d, int m) { year=y; day=d; month=m; }
};

QOStream &operator<<(QOStream &out, const TaskDate &date)
{
  char buff[64];
  sprintf(buff, "%d.%d.%d", date.day, date.month, date.year);
  return (out << buff);
}

class Task
{
public:
  RString name;
  int *priority; //pointer to priorities of some page
  float *timeLeft; //pointer to timeLeft of some page
  int *orders; //pointer to order.orders of some page
  bool alreadyExportedInXMLUserTasks; 
  TaskDate dates[NStages]; //latest date for each stage of this task

  Task() {alreadyExportedInXMLUserTasks=false;}
  Task(Page &page, Assign &user)
  {
    name = page.name;
    priority = page.priority;
    timeLeft=user.timeLeft;
    orders = page.order.orders;
    alreadyExportedInXMLUserTasks=false;
  }
};
TypeIsMovable(Task)

class UserTask
{
public:
  RString name;
  int priority; 
  float timeLeft; 
  int stage;
  int order;
  TaskDate date;
  int taskIx; //index into UserInfo::tasks
  
  UserTask() {}
  UserTask(Page &page, Assign &user, int productIx, int stageIx, int tskIx)
  {
    name = page.name;
    priority = page.priority[productIx];
    timeLeft=user.timeLeft[stageIx];
    order = page.order.orders[stageIx];
    stage=stageIx;
    taskIx=tskIx;
  }
  int CompareTo(const UserTask &task, const AutoArray<Task> &tasks) const
  {
    int order=RepairedOrder(tasks);
    int taskOrder=task.RepairedOrder(tasks);
    if (order-taskOrder) return (order-taskOrder);
    else {
      int retval = strcmp(cc_cast(name),cc_cast(task.name));
      if (retval) return retval;
      else {
        return (stage-task.stage);
      }
    }
  }
  int RepairedOrder(const AutoArray<Task> &tasks) const
  {
    const Task &task=tasks[taskIx];
    int i=stage;
    for (;i>0; i--)
    {
      if (task.timeLeft[i]!=0.0f) break;
    }
    return task.orders[i];
  }
};
TypeIsMovable(UserTask)

static int CompareUserTasks(const UserTask *c1, const UserTask *c2, const AutoArray<Task> &tasks)
{
  return c1->CompareTo(*c2, tasks);
}

class UserInfo
{
public:
  typedef RString KeyType;
  RString name;
  float timeLeft[NPriorities][NStages];
  AutoArray<Task> tasks;
  AutoArray<UserTask> userTasks;
  TaskDate totalDates[NPriorities+1][NStages];  //dates maxima

  UserInfo() 
  {
    for(int j=0; j<NPriorities; j++)
      for(int i=0; i<NStages; i++) timeLeft[j][i]=0.0f;
  }
  UserInfo(RString name1)
  {
    //    UserInfo(); //this doesn't work
    for(int j=0; j<NPriorities; j++)
      for(int i=0; i<NStages; i++) timeLeft[j][i]=0.0f;
    name = name1;
  }
  void Update(Page &page, Assign &user, int productIx)
  {
    int taskIx = tasks.Add(Task(page, user));
    int priorityIx = page.priority[productIx];
    float sum=0.0f;
    for (int i=0; i<NStages; i++) {
      sum += user.timeLeft[i];
      timeLeft[priorityIx][i] += sum;
    }
    for (int i=0; i<NStages; i++) userTasks.Add(UserTask(page, user, productIx, i, taskIx));
  }
  bool operator==(const UserInfo &user) const {return (name==user.name); }
  void ComputeDates(WikiTasksDefaults &defaults);
};
TypeIsMovable(UserInfo)

#include <Es/Algorithms/qsort.hpp>
#include <time.h>
void UserInfo::ComputeDates(WikiTasksDefaults &defaults)
{
  QSort(userTasks.Data(),userTasks.Size(),tasks, CompareUserTasks);
  time_t ltime; time( &ltime );
  //get defaults for user
  float daysInWeek = 4.0f;
  for (int i=0; i<defaults.users.Size(); i++)
  {
    if (stricmp(cc_cast(defaults.users[i].name),cc_cast(name))==0) daysInWeek=defaults.users[i].daysInWeek;
  }
  for (int i=0; i<userTasks.Size(); i++)
  {
    UserTask &task=userTasks[i];
    ltime += task.timeLeft*(60*60*24)*(7.0f/daysInWeek);
    struct tm *newtime = localtime( &ltime ); /* Convert to local time. */
    task.date.year=newtime->tm_year+1900;
    task.date.month=newtime->tm_mon+1;
    task.date.day=newtime->tm_mday;
    tasks[task.taskIx].dates[task.stage]=task.date;
    totalDates[task.priority][task.stage]=task.date;  //maximum
    totalDates[NPriorities][task.stage]=task.date;  //maximum for all priorities
  }
}

void ExportXMLUserTasks(QOStream &out, UserInfo &user, int priority, int stage)
{
  //date totals
  out << "\t\t<UserTotals>\r\n";
  out << "\t\t\t<Prototype>" << user.totalDates[priority][S_PROTOTYPE] << "</Prototype>\r\n";
  out << "\t\t\t<Alpha>" << user.totalDates[priority][S_ALPHA] << "</Alpha>\r\n";
  out << "\t\t\t<Beta>" << user.totalDates[priority][S_BETA] << "</Beta>\r\n";
  out << "\t\t</UserTotals>\r\n";
  out << "\t\t<UserTasks>\r\n";
  //task details
  for (int i=0; i<user.userTasks.Size(); i++)
  {
    UserTask &task = user.userTasks[i];
    if (task.priority==priority || priority==NPriorities)
    {
      Task &gtask=user.tasks[task.taskIx];
      if (stage!=NStages)
      { //check, whether this task is already listed
        if (gtask.alreadyExportedInXMLUserTasks) continue;
        float timeLeft=0;
        for (int stg=0; stg<NStages; stg++) timeLeft+=gtask.timeLeft[stg];
        if (timeLeft<=0.001f) continue;
      }
      if (task.timeLeft==0.0f && stage==NStages) continue;
      
      // check if next task is the continuation of this one
      if (i<user.userTasks.Size()-1)
      {
        const UserTask &nxTask = user.userTasks[i+1];
        if (0==strcmp(nxTask.name,task.name) && nxTask.timeLeft>0)
        {
          continue;
        }
      }
      if (stage!=NStages) gtask.alreadyExportedInXMLUserTasks=true;
      
      out << "\t\t\t<UserTask>\r\n";
      out << "\t\t\t\t<Name>" << task.name << "</Name>\r\n";
      //timeLeft
      out << "\t\t\t\t<TimeLeft>" << task.timeLeft << "</TimeLeft>\r\n";
      //order
      out << "\t\t\t\t<Order>" << task.order << "</Order>\r\n";
      //stage
      out << "\t\t\t\t<Stage>" << stagesStr[task.stage]<< "</Stage>\r\n";
      //priority
      out << "\t\t\t\t<Priority>" << prioritiesStr[task.priority]<< "</Priority>\r\n";
      //date
      if (priority==NPriorities) { //full date info
        out << "\t\t\t\t<Date>\r\n";
        out << "\t\t\t\t\t<Day>" << task.date.day << "</Day>\r\n";
        out << "\t\t\t\t\t<Month>" << task.date.month << "</Month>\r\n";
        out << "\t\t\t\t\t<Year>" << task.date.year << "</Year>\r\n";
        out << "\t\t\t\t</Date>\r\n";
      }
      else //short date info, but for all stages
      {
        out << "\t\t\t\t<Dates>\r\n";
        Task &gtask=user.tasks[task.taskIx];
        out << "\t\t\t\t\t<Prototype>" << gtask.dates[S_PROTOTYPE] << "</Prototype>\r\n";
        out << "\t\t\t\t\t<Alpha>" << gtask.dates[S_ALPHA] << "</Alpha>\r\n";
        out << "\t\t\t\t\t<Beta>" << gtask.dates[S_BETA] << "</Beta>\r\n";
        out << "\t\t\t\t</Dates>\r\n";
      }
      out << "\t\t\t</UserTask>\r\n";
    }
  }
  out << "\t\t</UserTasks>\r\n";
}

void ExportXMLTasks(QOStream &out, UserInfo &user, int priority, int categoryIx)
{
  out << "\t\t<Tasks>\r\n";
  for (int i=0; i<user.tasks.Size(); i++)
  {
    Task &task = user.tasks[i];
    if (task.priority[categoryIx]==priority || priority==NPriorities)
    {
      out << "\t\t\t<Task>\r\n";
      out << "\t\t\t\t<Name>" << task.name << "</Name>\r\n";
      //timeLeft
      out << "\t\t\t\t<TimeLeft>\r\n";
      out << "\t\t\t\t\t<Analysis>" << task.timeLeft[0] << "</Analysis>\r\n";
      out << "\t\t\t\t\t<Prototype>" << task.timeLeft[1] << "</Prototype>\r\n";
      out << "\t\t\t\t\t<Alpha>" << task.timeLeft[2] << "</Alpha>\r\n";
      out << "\t\t\t\t\t<Beta>" << task.timeLeft[3] << "</Beta>\r\n";
      out << "\t\t\t\t</TimeLeft>\r\n";
/*
      //order
      out << "\t\t\t\t<Orders>\r\n";
      for (int j=0; j<NStages; j++) out << "\t\t\t\t\t<Order stage=\"" << stagesStr[j] << "\">" << task.orders[j] << "</Order>\r\n";
      out << "\t\t\t\t</Orders>\r\n";
*/
      out << "\t\t\t</Task>\r\n";
    }
  }
  out << "\t\t</Tasks>\r\n";
}

void ExportXMLUsers(Pages &pages, int productIx, WikiTasksDefaults &defaults)
{
  //create UserInfo
  FindArrayKey<UserInfo> _users;
  UserInfo *_curUser=NULL;
  for (int i=0; i<pages.Size(); i++)
  {
    Page &page = pages[i];
    for (int userix=0; userix<page.assignes[productIx].Size(); userix++)
    {
      Assign &user = page.assignes[productIx][userix];
      int ix = _users.FindKey(user.user);
      if (ix!=-1)
      {
        _curUser = &_users[ix];
      }
      else { //add new user
        int ixNew = _users.Add(UserInfo(user.user));
        _curUser = &_users[ixNew];
      }
      _curUser->Update(page,user,productIx);
    }
  }

  RString outXmlFileName = RString(productsStr[productIx]) + "_users.xml";
  QOFStream out(cc_cast(outXmlFileName));
  out << "<?xml version=\"1.0\" encoding=\"windows-1250\"?>\r\n"
    "<?xml-stylesheet href=\"tasks_users_dates.xsl\" type=\"text/xsl\"?>\r\n\r\n";

  out << "<UserTasks category=\"" << productsStr[productIx] << "\">\r\n";
  float analysis, prototype, alpha, beta;
  for (int i=0; i<_users.Size(); i++)
  {
    UserInfo &user = _users[i];
    user.ComputeDates(defaults);
    out << "\t<User name=\"" << user.name<< "\">\r\n";
    //total
    out << "\t\t<Total>\r\n";
    analysis=0.0f; for (int j=0; j<NPriorities; j++) analysis += user.timeLeft[j][TE_ANALYSIS-TE_ANALYSIS];
    out << "\t\t\t<Analysis>" << analysis << "</Analysis>\r\n";
    prototype=0.0f; for (int j=0; j<NPriorities; j++) prototype += user.timeLeft[j][TE_PROTOTYPE-TE_ANALYSIS];
    out << "\t\t\t<Prototype>" << prototype << "</Prototype>\r\n";
    alpha=0.0f; for (int j=0; j<NPriorities; j++) alpha += user.timeLeft[j][TE_ALPHA-TE_ANALYSIS];
    out << "\t\t\t<Alpha>" << alpha << "</Alpha>\r\n";
    beta=0.0f; for (int j=0; j<NPriorities; j++) beta += user.timeLeft[j][TE_BETA-TE_ANALYSIS];
    out << "\t\t\t<Beta>" << beta << "</Beta>\r\n";
    ExportXMLTasks(out, user, NPriorities, productIx);
    ExportXMLUserTasks(out, user, NPriorities, NStages);
    out << "\t\t</Total>\r\n";
    //required
    out << "\t\t<Required>\r\n";
    analysis = user.timeLeft[P_REQUIRED][TE_ANALYSIS-TE_ANALYSIS];
    out << "\t\t\t<Analysis>" << analysis << "</Analysis>\r\n";
    prototype = user.timeLeft[P_REQUIRED][TE_PROTOTYPE-TE_ANALYSIS];
    out << "\t\t\t<Prototype>" << prototype << "</Prototype>\r\n";
    alpha = user.timeLeft[P_REQUIRED][TE_ALPHA-TE_ANALYSIS];
    out << "\t\t\t<Alpha>" << alpha << "</Alpha>\r\n";
    beta = user.timeLeft[P_REQUIRED][TE_BETA-TE_ANALYSIS];
    out << "\t\t\t<Beta>" << beta << "</Beta>\r\n";
    ExportXMLTasks(out, user, P_REQUIRED, productIx);
    ExportXMLUserTasks(out, user, P_REQUIRED, S_ALPHA);
    out << "\t\t</Required>\r\n";
    //desired
    out << "\t\t<Desired>\r\n";
    analysis = user.timeLeft[P_DESIRED][TE_ANALYSIS-TE_ANALYSIS];
    out << "\t\t\t<Analysis>" << analysis << "</Analysis>\r\n";
    prototype = user.timeLeft[P_DESIRED][TE_PROTOTYPE-TE_ANALYSIS];
    out << "\t\t\t<Prototype>" << prototype << "</Prototype>\r\n";
    alpha = user.timeLeft[P_DESIRED][TE_ALPHA-TE_ANALYSIS];
    out << "\t\t\t<Alpha>" << alpha << "</Alpha>\r\n";
    beta = user.timeLeft[P_DESIRED][TE_BETA-TE_ANALYSIS];
    out << "\t\t\t<Beta>" << beta << "</Beta>\r\n";
    ExportXMLTasks(out, user, P_DESIRED, productIx);
    ExportXMLUserTasks(out, user, P_DESIRED, S_ALPHA);
    out << "\t\t</Desired>\r\n";
    //niceToHave
    out << "\t\t<NiceToHave>\r\n";
    analysis = user.timeLeft[P_NICE_TO_HAVE][TE_ANALYSIS-TE_ANALYSIS];
    out << "\t\t\t<Analysis>" << analysis << "</Analysis>\r\n";
    prototype = user.timeLeft[P_NICE_TO_HAVE][TE_PROTOTYPE-TE_ANALYSIS];
    out << "\t\t\t<Prototype>" << prototype << "</Prototype>\r\n";
    alpha = user.timeLeft[P_NICE_TO_HAVE][TE_ALPHA-TE_ANALYSIS];
    out << "\t\t\t<Alpha>" << alpha << "</Alpha>\r\n";
    beta = user.timeLeft[P_NICE_TO_HAVE][TE_BETA-TE_ANALYSIS];
    out << "\t\t\t<Beta>" << beta << "</Beta>\r\n";
    ExportXMLTasks(out, user, P_NICE_TO_HAVE, productIx);
    ExportXMLUserTasks(out, user, P_NICE_TO_HAVE, S_ALPHA);
    out << "\t\t</NiceToHave>\r\n";
    out << "\t</User>\r\n";
  }
  out << "</UserTasks>\r\n";
}

BYTE toHex(BYTE n)
{
  if (n<10) return (n+'0');
  if (n<16) return (n+'A'-10);
  return '0';
}

/**
@param text string in UTF8
*/
RString UrlEncode(RString text)
{
  RString sOut;
  const int nLen = text.GetLength()+1;
  LPBYTE pOutTmp = NULL;
  LPBYTE pOutBuf = NULL;
  LPBYTE pInTmp = NULL;
  LPBYTE pInBuf =(LPBYTE)text.Data();
  //alloc out buffer
  pOutBuf = (LPBYTE)sOut.CreateBuffer(nLen  * 3 - 2);
  if(pOutBuf)
  {
    pInTmp	= pInBuf;
    pOutTmp = pOutBuf;
    // do encoding
    while (*pInTmp)
    {
      if (*pInTmp>=0x80) {
        *pOutTmp++ = '%';
        *pOutTmp++ = toHex(*pInTmp>>4);
        *pOutTmp++ = toHex(*pInTmp%16);
      } 
      else if(isalnum(*pInTmp)) {
        *pOutTmp++ = *pInTmp;
      }
      else if(isspace(*pInTmp))*pOutTmp++ = '_';
      else
      {
        *pOutTmp++ = '%';
        *pOutTmp++ = toHex(*pInTmp>>4);
        *pOutTmp++ = toHex(*pInTmp%16);
      }
      pInTmp++;
    }
    *pOutTmp = '\0';
  }
  return sOut;
}

//global funcions
//returns -1 for not found
int GetPriorityIx(const char *priority)
{
  for (int i=0; i<NPriorities; i++)
    if (stricmp(prioritiesStr[i],priority)==0) return i;
  return -1;
}
//returns -1 for not found
int GetStageIx(const char *stage)
{
  for (int i=0; i<NStages; i++)
    if (stricmp(stagesStr[i],stage)==0) return i;
  return -1;
}
//returns -1 for not found
int GetProductIx(const char *product)
{
  for (int i=0; i<NStages; i++)
    if (stricmp(productsStr[i],product)==0) return i;
  return -1;
}

//Force LogF to write into stderr (in release configuration)
#if !_DEBUG
#include "Es/Framework/appFrame.hpp"
class ConsoleAppFrameFunctions : public AppFrameFunctions
{
public:
#if _ENABLE_REPORT
  virtual void LogF(const char *format, va_list argptr);
#endif
};

#if _ENABLE_REPORT
void ConsoleAppFrameFunctions::LogF(const char *format, va_list argptr)
{
  BString<512> buf;
  vsprintf(buf,format,argptr);
  strcat(buf,"\n");
  fputs(buf,stderr);
}
#endif

static ConsoleAppFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;
#endif

int main(int argc, char* argv[])
{
  RString path = "/index.php/Special:Export/"; //https://wiki.bistudio.com/index.php/Special:Export/
  RString pathCategory = "/index.php/Special:CategoryExport/Category:"; //https://wiki.bistudio.com/index.php/Special:CategoryExport/Category:
  RString categoryName;

  char *templatePrefix;
  const char *productName;
  if (argc>=4) {
    categoryName = argv[1];
    if (argc>=5) templatePrefix=argv[4];
    else templatePrefix="ttask";
    if (argc==6) productName=argv[5];
    else productName=cc_cast(categoryName);
  }
  else {
    printf("Usage: wikiTasks.exe categoryName username password [templatePrefix]\n\n");
    return 1;
  }
  InternetConnectInfo inetInfo;
  inetInfo.userName = argv[2];
  inetInfo.password = argv[3];
  inetInfo.port = 443;
  inetInfo.serverName = "wiki.bistudio.com";

  size_t size = 0;
  char *data = DownloadFile(pathCategory + categoryName, size, NULL, &inetInfo);
  if (!data) {
    printf("Error: Unable to download file: %s\n", cc_cast(pathCategory + categoryName));
    return -1;
  }
  printf("Downloaded file: %s (size = %d)\n", cc_cast(pathCategory + categoryName), strlen(data));
  printf("Content: ((\n%s\n))\n", data);
  AutoFree buffer(data);

  QIStrStream in(data, size);

  ExtractArticles parser;
  parser.Parse(in);
  AutoArray<RString> articles = parser.GetArticles();
  RString category = parser.GetCategory();
  //  for (int i=0; i<articles.Size(); i++)
  //  {
  //    OutputDebugString(cc_cast(articles[i])); OutputDebugString("\n");
  //  }
  Pages pages(articles);

  // read content of pages
  const char *avoid = "Category:";
  for (int i=pages.Size()-1; i>=0; i--)
  {
    if (strnicmp(pages[i].name, avoid, strlen(avoid)) == 0)
    {
      pages.Delete(i);
      continue;
    }

    size_t size = 0;
    printf("(Done: %d%%) %s\n", (100*(pages.Size()-i))/pages.Size(), cc_cast(pages[i].name));
    RString nameUrlEncoded = UrlEncode(pages[i].name);
//    char *data = DownloadFile(path + pages[i].name, size, NULL); //much slower
    char *data = DownloadFile(path + nameUrlEncoded/*pages[i].name*/, size, NULL, &inetInfo);
    if (!data) {
      printf("Error: Unable to download file: %s\n", cc_cast(path + nameUrlEncoded));
      printf("Hint: Possibly wrong UrlEncoded file name\n");
      return -1;
    }
    AutoFree buffer(data);

    QIStrStream in(data, size);
#if _DEBUG
    int doIt=-1; //simulaceZvirat == 16 for ArmA
    if (i==doIt)
    {
      QOFStream outf("simulaceZvirat.xml");
      RString debugData(data,size);
      outf << debugData;
    }
#endif

    ExtractText parser;
    parser.Parse(in);
    RString text = parser.GetText();

    ProcessPage(pages[i], text, templatePrefix);
    pages[i].UpdateAssignesUsingStage();
  }
  pages.ConvertCharset();
  pages.ConvertSpecialChars();
  int retval = pages.ComputeDoAfterPosetLevels();
  WikiTasksDefaults defaults; //constructor parses wikiTasksDefaults.xml
  int productIx=GetProductIx(productName);
  if (productIx!=-1) pages.UpdateOrders(defaults, productIx);
  //pages.ComputeDates();

  if (productIx!=-1) ExportXML(pages, categoryName, productIx);
  if (productIx!=-1) ExportXMLUsers(pages, productIx, defaults);

	return retval;
}

#endif
//end of #ifdef WIKI_TOOLS_XML
