#include <El/elementpch.hpp>

#include <Es/Common/win.h>
#include <Es/Strings/rString.hpp>

#include <El/XML/inet.hpp>
#include <El/XML/xml.hpp>
#include <El/QStream/qStream.hpp>
#include <El/ParamFile/paramFile.hpp>

#include "WikiTasks.h"

#ifdef UNASSIGNED_TASKS

class ExtractUnAssigned : public SAXParser
{
private:
  RString _curTask;
protected:
  int _insideElement;

public:
  ExtractUnAssigned() { _insideElement=0; }

  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "Assign") == 0) {
      _curTask = ""; //this is not unassigned task
    } else if (stricmp(name, "Name")==0)
    {
      _curTask="";
      _insideElement++;
    }
  }
  virtual void OnEndElement(RString name)
  {
    if (stricmp(name, "Name")==0)
    {
      _insideElement--;
    } else if (stricmp(name, "Task")==0)
    {
      if (_curTask!=RString("")) printf("%s\n", cc_cast(_curTask));
    }
  }
  virtual void OnCharacters(RString chars)
  {
    if (_insideElement > 0) _curTask = _curTask + chars;
  }
};

int main(int argc, char* argv[])
{
  QIFStream in;

  if (argc==2) in.open(argv[1]);
  else {
    printf("Usage: unassignedTasks.exe wikiToolFile.xml\n\n");
    return 1;
  }

  ExtractUnAssigned parser;
  parser.Parse(in);
}

#endif
