<?xml version="1.0" encoding="ISO-8859-2"?>
<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#160;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2><xsl:value-of select="Tasks/@category"/> Tasks List</h2>
    <ol>
    <xsl:for-each select="Tasks/Task">
    <xsl:sort select="Name"/>
    <li><a><xsl:attribute name="href"><xsl:text>https://wiki.bistudio.com/index.php/</xsl:text><xsl:value-of select="Name"/></xsl:attribute><b><xsl:value-of select="Name"/></b></a>
      <ul>
      <xsl:if test="count(Priorities/Priority) &gt; 0">
        <li><xsl:for-each select="Priorities/Priority">
          <b><xsl:value-of select="@product"/>(</b><xsl:value-of select="."/>)<xsl:text> </xsl:text>
        </xsl:for-each></li>
      </xsl:if>
      <xsl:if test="count(Assignes/Assign) &gt; 0">
        <li><xsl:for-each select="Assignes/Assign">
          <b><xsl:value-of select="@user"/>:</b>
          <xsl:text> </xsl:text><xsl:value-of select="Analysis"/> 
          <xsl:text> </xsl:text><xsl:value-of select="Prototype"/> 
          <xsl:text> </xsl:text><xsl:value-of select="Alpha"/> 
          <xsl:text> </xsl:text><xsl:value-of select="Beta"/> 
          &nbsp;&nbsp;&nbsp;
        </xsl:for-each></li>
      </xsl:if>
      <xsl:if test="count(Dependencies/Dependency) &gt; 0">
        <li><b>Dependencies:</b>
  			<ol>
			<xsl:for-each select="Dependencies/Dependency">
			<li><xsl:value-of select="."/></li>
			</xsl:for-each>
			</ol>
        </li>
      </xsl:if>
      </ul>
    </li>
    </xsl:for-each>
    </ol>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>
