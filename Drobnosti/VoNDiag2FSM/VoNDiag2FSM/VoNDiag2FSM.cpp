// VoNDiag2FSM.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

#pragma warning( disable : 4996 4267 )

#include <El/elementpch.hpp>
#include <El/ParamFile/paramFile.hpp>
#include <Es/Files/filenames.hpp>

int FindPhraseLine(const char* phrase, AutoArray<RString> &lines, int from=0)
{
  int phrLen = strlen(phrase);
  for (int i=from; i<lines.Size(); i++)
  {
    if (strncmp(phrase, lines[i], phrLen)==0)
    {
      return i;
    }
  }
  return -1;
};

struct Peer
{
  RString id;
  AutoArray<RString> edges;
  RString log;  //should contain all lines from the peer's report
  Peer() { id="0"; }
};
TypeIsMovable(Peer)

struct Id2Nick 
{
  struct Item
  {
    RString id;
    RString nick;
    Item() {};
    ClassIsMovable(Item)
  };

  AutoArray<Item> map;

  void Add(RString id, RString nick)
  {
    int ix = map.Add();
    map[ix].id = id;
    map[ix].nick = nick;
  }

  RString GetNick(RString id)
  {
    for(int i=0; i<map.Size(); i++)
    {
      if (map[i].id==id) return map[i].nick;
    }
    return RString("");
  }
};

void ConvertMatrixToParamFile(/*IN*/AutoArray<Peer> &matrix, /*OUT*/ParamFile &f, Id2Nick &id2Nick, int diagNum, bool invert)
{
  f.Add("version", 1);
  const int ItemLenX = 80; //in horizontal direction
  float radius = matrix.Size()*ItemLenX/3.14f;
  // Graph Items
  ParamClassPtr graphItems = f.AddClass("GraphItems");
  if (graphItems)
  {
    for (int i=0; i<matrix.Size(); i++)
    {
      RString itemName = Format("Item%d", i);
      ParamClassPtr item = graphItems->AddClass(itemName);
      if (item)
      { //next item
        if (matrix[i].id=="#2") //server
        {
          item->Add("Flags", 4346);  //make this item selected and show InitCode in data window
          item->Add("BgColor", 255); 
        }
        else
        {
          item->Add("Flags", 250);
          item->Add("BgColor", 16777215);
        }
        item->Add("BasicText", "");
        float phi = i*2*3.1415f/matrix.Size();
        float posX = radius*sin(phi);
        float posY = radius*cos(phi);
        float Left=posX-ItemLenX/2;
        float Top=posY-ItemLenX/4;
        float Right=posX+ItemLenX/2;
        float Bottom=posY+ItemLenX/4;
        item->Add("Left", Left);
        item->Add("Top", Top);
        item->Add("Right", Right);
        item->Add("Bottom", Bottom);
        ParamClassPtr itemInfo = item->AddClass("ItemInfo");
        RString text = Format("%s", cc_cast(matrix[i].id));
        RString nick = id2Nick.GetNick(matrix[i].id);
        if (!nick.IsEmpty())
        {
          text = text + "\n" + nick;
        }
        itemInfo->Add("Text", text);
        if (matrix[i].id=="#2") //server
        {
          itemInfo->Add("ItemType",0);
          itemInfo->Add("Shape",6);
        }
        else
          itemInfo->Add("ItemType",2);
        ParamClassPtr itemInfo2 = itemInfo->AddClass("ItemInfo");
        itemInfo2->Add("Id", i);
        if (!matrix[i].log.IsEmpty())
        {
          itemInfo2->Add("InitCode", matrix[i].log);
        }
      }
    }
  }
  // Links
  // Graph Items
  ParamClassPtr graphLinks = f.AddClass("GraphLinks");
  if (graphLinks)
  {
    int linkno = 0;
    // find the serverix
    int serverix = 0;
    for (int i=0; i< matrix.Size(); i++)
    {
      if (matrix[i].id=="#2") { serverix=i; break; }
    }
    for (int i=0; i<matrix.Size(); i++)
    {
      AutoArray<bool> edges;
      edges.Realloc(matrix.Size());
      edges.Resize(matrix.Size());
      for (int j=0; j<edges.Size(); j++) edges[j]=false; //no edge yet
      for (int j=0; j<matrix[i].edges.Size(); j++)
      {
        RString id=matrix[i].edges[j];
        int k=0;
        for (; k<matrix.Size(); k++)
        {
          if (matrix[k].id==id) 
          {
            edges[k]=true;
            break;
          }
        }
      }
      for (int j=0; j<edges.Size(); j++)
      {
        if (invert ^ edges[j]) //xor
        {
          RString linkName = Format("Link%d", linkno++);
          ParamClassPtr link = graphLinks->AddClass(linkName);
          if (link)
          {
            link->Add("From", i);
            link->Add("To", j);
            if (j==serverix || i==serverix) //edge with server
              link->Add("Color",4227327);
            else
              link->Add("Color",6316128);
            link->Add("Flags",2);
            ParamClassPtr extra = link->AddClass("Extra");
            extra->Add("ArrowSize",0);
          }
        }
      }
    }
  }
  // Append Globals class
  ParamClassPtr globals = f.AddClass("Globals");
  if (globals)
  {
    //globals->Add("Grid",float(25.000000));
    //globals->Add("GridShow",1);
    globals->Add("PageColor",1);
    globals->Add("PageColorVal",16383954); //light cyan as page background color is really COOL :)
    globals->Add("NextGroupID",1);
    globals->Add("NextID", matrix.Size());
    RString fsmName = Format("VoN Topology Diagnostic (num. %d)", diagNum+1);
    globals->Add("FSMName",fsmName);
    ParamClassPtr dl = globals->AddClass("DefaultLink");
    if (dl) dl->Add("ArrowSize",0);
    globals->Add("DefaultLinkColor",33023);
    globals->Add("DefaultLinkUseCustom",1);
    float squareSize = radius + ItemLenX/2;
    globals->Add("PZoomLeft",-squareSize);
    globals->Add("PZoomRight",squareSize);
    globals->Add("PZoomBottom",squareSize);
    globals->Add("PZoomTop",-squareSize);
    globals->Add("Clxs",squareSize);
    globals->Add("Clys",squareSize);
    globals->Add("Aspect",1);
  };
  // Append Window class at the end
  ParamClassPtr window = f.AddClass("Window");
  if (window)
  {
    window->Add("Flags",0);
    window->Add("MaxPosX",-1);
    window->Add("MaxPosY",-1);
    window->Add("MinPosX",-1);
    window->Add("MinPosY",-1);
    window->Add("Left",0);
    window->Add("Right",800);
    window->Add("Top",0);
    window->Add("Bottom",650);
    window->Add("ShowCmd",1);
    window->Add("SplitPos",500);
  }
}

RString GetWord(const char *str)
{
  const char *ptr = str;
  while (*ptr && ( isalnum(*ptr) || (*ptr=='#') ) ) ptr++;
  return RString(str, ptr-str);
}

void ProcessLogFile(RString filename)
{
  // read lines from input file into array
  AutoArray<AutoArray<RString> > lineArray;
  QIFStream in;
  in.open(filename);
  Id2Nick id2nick;
  if (!in.fail())
  {
    char buf[4096];
    bool storeLines=false;
    int diagNum=0;
    while (in.readLine(buf, 4096))
    {
      if (strcmp(buf, "*** VoN Topology Diagnostics ((***")==0) 
      {
        diagNum = lineArray.Add();
        storeLines=true; //read begin
      }
      else if (strcmp(buf, "*** VoN Topology Diagnostics ))***")==0) 
      {
        storeLines=false;
      }
      else if (storeLines)
      {
        lineArray[diagNum].Add(buf);
      }
      else // possible id2nick line
      { //lines can look like this:
        //16:53:57 Player ManiK connected (id=609543).
        const char *connectedStr = "connected (id=";
        char *nickEnd = strstr(buf, connectedStr);
        if (nickEnd)
        {
          const char *playerStr = "Player ";
          char *nick=strstr(buf, playerStr);
          if (nick)
          {
            char *idStr = nickEnd+strlen(connectedStr);
            //int id = atoi(idStr);
            RString id = GetWord(idStr);
            nickEnd--;
            *nickEnd = 0; //end the string;
            nick+=strlen(playerStr);
            id2nick.Add(id, nick);
          }
        }
      }
    }
  }
  else printf("Cannot open input file!\n");
  in.close();
  // Create one FSM Graph for each diagnostic inside log file
  for (int diagNum=0; diagNum<lineArray.Size(); diagNum++)
  {
    printf("File %s: read %d lines\n", cc_cast(filename), lineArray[diagNum].Size());

    //read Matrix
    AutoArray<Peer> matrix;
    matrix.Realloc(50);
    RString phrase("Matrix:");
    int start = FindPhraseLine(phrase, lineArray[diagNum]);
    if (start>=0)
    {
      for (int i=start+1; i<lineArray[diagNum].Size(); i++)
      {
        if (strncmp(lineArray[diagNum][i],"  ",2)==0)
        {
          // read id of peer first
          const char *line = lineArray[diagNum][i];
          const char *colon = strchr(line+2,':');
          if (!colon) break; //wrong line
          RString id = GetWord(line+2);
          int ix = matrix.Add();
          matrix[ix].id = id;
          // read the edges
          colon++; // the char after ':'
          if (*colon==' ') colon++;  //the start of next id
          const char *nextId = colon;
          while (*nextId!=0 && *nextId!='\r' && *nextId!='\n')
          {
            RString edge = GetWord(nextId);
            matrix[ix].edges.Add(edge);
            const char* comma = strchr(nextId,',');
            if (!comma) break;
            nextId = comma+1;
          }
        }
        else break; //this line is not matrix line (end detected)
      }
    }
    else
    {
      printf("No Matrix: line found!\n");
      continue;
    }

    // read =player=: lines
    RString playerPh("=Player=:");
    for (
      int plIx=FindPhraseLine(playerPh, lineArray[diagNum]); 
      plIx>=0; 
    plIx=FindPhraseLine(playerPh, lineArray[diagNum], plIx+1)
      )
    {
      const char *player = lineArray[diagNum][plIx];
      player+=playerPh.GetLength();
      if (*player==' ') player++;
      RString id = GetWord(player);
      for (int i=0; i<matrix.Size(); i++)
      {
        if (matrix[i].id==id)
        { //write all lines belonging to this player to his log
          int nextIx=FindPhraseLine(playerPh, lineArray[diagNum], plIx+1);
          if (nextIx<0) nextIx=lineArray[diagNum].Size();
          QOStrStream log;
          for (int k=plIx+1; k<nextIx; k++)
          {
            log << lineArray[diagNum][k] << "\n";
          }
          matrix[i].log = RString(log.str(), log.pcount());
          break;
        }
      }
    }

    // matrix now contains all data needed to create the Diagnostics Graph for FSM Editor
    // save it in *.bifsm format using paramfile
    for (int invert=0; invert<2; invert++)
    {
      ParamFile f;
      ConvertMatrixToParamFile(matrix, f, id2nick, diagNum, invert>0);
      // write ParamFile to file
      RString outFileName = filename;
      *GetFileExt(outFileName.MutableData()) = 0; //clear the extension
      if (diagNum>0)
      {
        RString num = Format("%d", diagNum);
        outFileName = outFileName + num;
      }
      if (invert>0) outFileName = outFileName + "inv";
      outFileName = outFileName + ".bifsm";
      FILE *outfile = fopen(outFileName, "w+b");
      if (outfile)
      {
        QOStrStream out;
        f.Save(out, 0, "\r\n");
        fwrite(out.str(), 1, out.pcount(), outfile);
        fclose(outfile);
      }
    }
  }
}

int main(int argc, char* argv[])
{
  if (argc!=2)
  {
    printf("Usage: vonDiag2Fsm.exe vonLogFileName\n"
           "  It generates the vonLogFileName.bifsm file\n");
    return 1;
  }
  ProcessLogFile(argv[1]);
  return 0;
}
