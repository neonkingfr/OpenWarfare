// patch.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <float.h>
#include <direct.h>

#include <class/fileNames.hpp>

static HANDLE hConsoleOutput,hConsoleInput;

static bool wasError=true;

static void ExitCon()
{
	if( wasError )
	{
		DWORD ret;
		static const char text[]="\n-----------------------\nPress ENTER to continue";
		char buf[80];
		WriteConsole(hConsoleOutput,text,strlen(text),&ret,NULL);
		ReadConsole(hConsoleInput,buf,sizeof(buf),&ret,NULL);
	}
}

#include <io.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <class/debugLog.hpp>
#include <class/fltopts.hpp>

#include "fileMap.hpp"

#if _DEBUG
#define OUT_TEXT 1
#else
#define OUT_TEXT 0
#endif

class IAction
{
	public:
	virtual bool Perform
	(
		const char *tgtPath, const char *srcPath,const char *dataPath,
		const char *name
	) = NULL;
	virtual void CleanUp() = NULL;
	virtual ~IAction(){}
};


class CreatePatch: public IAction
{
	FileBufferMapped fileNew;
	FileBufferMapped fileOld;

	//int _oldPos,_newPos; // position of identical part
	//int _identical,_oldSkip,_newSkip; // current change state

	int _samePos,_sameSize; // identical prepared to be saved
	int _diffPos,_diffSize; // different prepared to be saved

	FILE *fileDst;
	FILE *fileTxt;

	public:
	CreatePatch();
	bool Perform
	(
		const char *tgtPath, const char *srcPath,const char *dataPath,
		const char *name
	);
	void CleanUp();

	private:
	void SaveFromOld( int oldPos, int oldSize );
	void SaveFromNew( int newPos, int newSize );

	//void SaveIdentical( int identical, int newPos, int oldPos );
	//void SaveChange( int skipOld, int skipNew ); // buffered save

	void Flush();
	void ShowProgress( const char *name, int left );

	private:
	DWORD lastProgressTime;
	int lastProgressNum;
};

class CreateDelete: public IAction
{
	FileBufferMapped fileNew,fileOld;
	FILE *fileDst;

	public:
	CreateDelete();
	bool Perform
	(
		const char *tgtPath, const char *srcPath,const char *dataPath,
		const char *name
	);
	void CleanUp();
};

class ApplyPatch: public IAction
{
	FileBufferMapped fileOld,fileDiff;

	public:
	ApplyPatch();
	bool Perform
	(
		const char *tgtPath, const char *srcPath,const char *dataPath,
		const char *name
	);
	void CleanUp();
};

enum // kind of operation
{
	DiffAdd,
	DiffChange,
	DiffRemove
};

CreateDelete::CreateDelete()
{
}

bool CreateDelete::Perform
(
	const char *tgtPath, const char *srcPath,const char *dataPath,
	const char *name
)
{
	// delete: tgt = new, src = old, data = patch
	char tgtFullPath[1024];
	char srcFullPath[1024];
	char dtaFullPath[1024];
	strcpy(tgtFullPath,tgtPath);
	strcpy(srcFullPath,srcPath);
	strcpy(dtaFullPath,dataPath);
	strcat(tgtFullPath,name);
	strcat(srcFullPath,name);
	strcat(dtaFullPath,name);
	strcat(dtaFullPath,".upd");

	fileNew.Open(tgtFullPath);
	fileOld.Open(srcFullPath);

	if (fileOld.GetError())
	{
		fileDst = fopen(dtaFullPath,"wb");
		if (!fileDst)
		{
			printf("Cannot write %s\n",dtaFullPath);
			return false;
		}
		// new file does not exist - it was deleted
		char op = DiffRemove;
		fwrite(&op,sizeof(op),1,fileDst);
		fclose(fileDst);
		fileDst = NULL;
		return true;
	}

	CleanUp();

	return true;
}

void CreateDelete::CleanUp()
{
	fileNew.Close();
	fileOld.Close();
}

CreatePatch::CreatePatch()
{
	fileDst = NULL;
	fileTxt = NULL;
}

void CreatePatch::CleanUp()
{
	fileNew.Close();
	fileOld.Close();

	if (fileDst)
	{
		fclose(fileDst);
		fileDst = NULL;
	}
	#if OUT_TEXT
	if (fileTxt)
	{
		fclose(fileTxt);
		fileTxt = NULL;
	}
	#endif
}


void CreatePatch::ShowProgress( const char *name, int left )
{
	if (left>0)
	{
		if (lastProgressNum-left<256*1024)
		{
			DWORD time = GetTickCount();
			if (time-lastProgressTime<1000) return; // nothing to show
		}
	}
	lastProgressTime = GetTickCount();
	lastProgressNum = left;
	printf("%20s %10d KB\r",name,left/1024);
}

void CreatePatch::Flush()
{
	if (_sameSize<=0 && _diffSize<=0)
	{
		// nothing to flush
		return;
	}
	//int _diffPos,_diffSize; // different prepared to be saved
	// save identical data

	char *newData = fileNew.GetData();
	char *oldData = fileOld.GetData();

	int newLen = fileNew.GetSize();
	int oldLen = fileOld.GetSize();

	// save identical information
	#if OUT_TEXT
		fprintf(fileTxt,"Old data %d size %d\n",_samePos,_sameSize);
	#endif
	fwrite(&_sameSize,sizeof(_sameSize),1,fileDst);
	if (_sameSize>0)
	{
		fwrite(&_samePos,sizeof(_samePos),1,fileDst);
	}
	// save information about change

	#if OUT_TEXT
		fprintf
		(
			fileTxt,"New data %d (LA 0x%06x, VA 0x%06x) size %d\n",
			_diffPos,0x400000+_diffPos,_diffPos-0x1000,_diffSize
		);
	#endif
	// save new from newRead to bestN
	fwrite(&_diffSize,sizeof(_diffSize),1,fileDst);
	if (_diffSize>0)
	{
		fwrite(&_diffPos,sizeof(_diffPos),1,fileDst);
		fwrite(newData+_diffPos,_diffSize,1,fileDst);
	}

	_samePos = 0;
	_sameSize = 0;
	_diffPos = 0;
	_diffSize = 0;
}

void CreatePatch::SaveFromOld( int oldPos, int oldSize )
{
	if (oldSize<=0) return;
	// check if "same" section is closed
	if (_diffSize>0)
	{
		// check if different data before us are really different
		int maxCmp = _diffSize;
		saturateMin(maxCmp,oldPos);

		const char *oldDiffEnd = fileOld.GetData()+oldPos;
		const char *newDiffEnd = fileNew.GetData()+_diffPos+_diffSize;
		// try to attach as much as possible to our identical section

		while (--maxCmp>=0)
		{
			--oldDiffEnd;
			--newDiffEnd;
			if (*oldDiffEnd!=*newDiffEnd) break;
			// move byte from "diff" section to "same" section
			_diffSize--;
			oldPos--;
			oldSize++;
		}

		if (_diffSize>0)
		{
			Flush();
		}
	}
	// check if we may append to identical information
	if (_samePos+_sameSize==oldPos)
	{
		_sameSize += oldSize;
	}
	else
	{
		Flush();
		_samePos = oldPos;
		_sameSize = oldSize;
	}

}

void CreatePatch::SaveFromNew( int newPos, int newSize )
{
	if (newSize<=0) return;
	// check if may be appended to "diff"
	if (_diffPos+_diffSize==newPos)
	{
		_diffSize += newSize;
	}
	else
	{
		// may be appended to "same" section
		// wrtiting to diff section makes "same" section closed
		if (_diffSize>0)
		{
			Flush();
		}
		_diffPos = newPos;
		_diffSize = newSize;
	}
}

#pragma optimize("gt",on)

static int CountSame(const char *a, const char *b, int maxLen)
{
	int oldLen = maxLen;
	const char *oldA = a;
	while (--maxLen>=0)
	{
		if (*a!=*b)
		{
			break;
		}
		a++,b++;
	}
	return a-oldA;
}

static const char *findrchr( const char *beg, const char *end, char c )
{
	while( end>beg)
	{
		if (*--end==c) return end;
	}
	return NULL;
}

static const char *findchr( const char *beg, const char *end, char c )
{
	while( beg<end)
	{
		if (*beg==c) return beg;
		beg++;
	}
	return NULL;
}

#if _DEBUG
#pragma optimize("gt",off)
#endif

bool CreatePatch::Perform
(
	const char *tgtPath, const char *srcPath,const char *dataPath,
	const char *name
)
{
	// create: tgt = old, src = new, data = patch
	char tgtFullPath[1024];
	char srcFullPath[1024];
	char dtaFullPath[1024];
	#if OUT_TEXT
	char txtFullPath[1024];
	strcpy(txtFullPath,dataPath);
	strcat(txtFullPath,name);
	strcat(txtFullPath,".log");
	#endif
	strcpy(tgtFullPath,tgtPath);
	strcpy(srcFullPath,srcPath);
	strcpy(dtaFullPath,dataPath);
	strcat(tgtFullPath,name);
	strcat(srcFullPath,name);
	strcat(dtaFullPath,name);
	strcat(dtaFullPath,".upd");

	fileNew.Open(srcFullPath);
	fileOld.Open(tgtFullPath);

	lastProgressNum = fileNew.GetSize()*2; // init
	lastProgressTime = GetTickCount();

	_samePos = 0;
	_sameSize = 0;
	_diffPos = 0;
	_diffSize = 0;
	// compare srcNew and srcOld files
	// store differences to dst file
	fileDst = fopen(dtaFullPath,"wb");
	if (!fileDst)
	{
		printf("Cannot write %s\n",dtaFullPath);
		return false;
	}
	#if OUT_TEXT
	fileTxt = fopen(txtFullPath,"w");
	if (!fileTxt)
	{
		printf("Cannot write log file\n");
		return false;
	}
	#endif
	setvbuf(fileDst,NULL,_IOFBF,64*1024);

	if (fileNew.GetError())
	{
		// new file should always exist
		Fail("Error");
		return true;
	}
	else
	{
		// file added or changed
		char op = DiffChange;
		if (fileOld.GetError())
		{
			op = DiffAdd;
		}
		fwrite(&op,sizeof(op),1,fileDst);

		// calculate file checksums
		int checkSumNew=0;

		int newLen = fileNew.GetSize();
		int oldLen = fileOld.GetSize();
		int newRead = 0;
		int oldRead = 0;

		// write information about old file
		#if OUT_TEXT
		fprintf(fileTxt,"New '%s' - %d B\n",srcFullPath,newLen);
		fprintf(fileTxt,"Old '%s' - %d B\n",tgtFullPath,oldLen);
		#endif

		//const char *newShortName = GetFilenameExt(newName);
		//const char *oldShortName = GetFilenameExt(oldName);

		//fwrite(newShortName,strlen(newShortName)+1,1,fileDst);
		fwrite(&newLen,sizeof(newLen),1,fileDst);
		//fwrite(oldShortName,strlen(oldShortName)+1,1,fileDst);
		fwrite(&oldLen,sizeof(oldLen),1,fileDst);

		// write information about new file

		// check identical parts

		const char *newData = fileNew.GetData();
		const char *oldData = fileOld.GetData();
		const char *oldEnd = oldData+oldLen;

		float skipSize = 1;
		//int skipSize = 1;

		const int minIdenticalRequired = 8;
		const int minIdenticalWanted = 16;

		while(newRead<newLen)
		{
			ShowProgress(name,newLen-newRead);
			{
				// count how much is rest in both files
				int restNew = newLen-newRead;
				int restOld = oldLen-oldRead;

				int rest = restNew;
				saturateMin(rest,restOld);

				int identical = CountSame(newData+newRead,oldData+oldRead,rest);

				// compare if byte is equal in both files
				if (identical>minIdenticalRequired || identical==restNew)
				{
					for (int i=0; i<identical; i++)
					{
						checkSumNew += newData[newRead+i];
					}
					// skip identical part
					newRead+=identical;
					oldRead+=identical;
					skipSize = 1;
					// write info about identical part
					SaveFromOld(oldRead-identical,identical);
					continue;
				}
			}

			// now comes the hard part - try to get both files in sync
			// actually: we want to find data we have in fileNew somewhere in fileOld
			// we have some expectations

			// we maintain oldRead cursor that shows where reading ended in oldFile
			// (1) data will probably lay someplace near
			// (2) we will check all other positions in oldFile is this is not true
			// (3) if we will not find anything, we will skip some data

			int bestLen = 0;
			int bestO = 0;
			// (1)
			const int searchOld = 16*1024;
			const int goodMatch = 1024;

			// search depth depending on skipped data
			float satSkipSize = floatMin(skipSize*256,(float)oldLen);
			int depth = toLargeInt(satSkipSize+0.5f);
			
			// find next occurence of the searched character
			int actC = newData[newRead];

			int advanceMax = depth;
			int advanceMin = -depth;
			saturateMin(advanceMax,oldLen-oldRead);
			saturateMax(advanceMin,-oldRead);
			const char *pos = oldData+oldRead;
			const char *advanceEnd = pos+advanceMax;
			const char *advanceBeg = pos+advanceMin;

			for(;;)
			{
				pos = findchr(pos,advanceEnd,actC);
				if (!pos) break; // no more matches

				int o = pos-oldData;
				// calculate how much is rest in both files
				int restOld = oldLen-o;
				int restNew = newLen-newRead;
				int rest = restOld;
				saturateMin(rest,restNew);

				saturateMin(rest,goodMatch);
				int r = CountSame(newData+newRead,pos,rest);

				if (r>=minIdenticalWanted && r>=bestLen)
				{
					bestLen = r;
					bestO = o;
					if (r>=goodMatch) break;
				}
				pos++; // skip this occurence
			}

			// no good match when advancing - try to go back
			// if the match found is near, it may be much shorter - big probability of sync
			if (bestLen<=goodMatch)
			{
				pos = oldData+oldRead;

				for(;;)
				{
					pos = findrchr(advanceBeg,pos,actC);
					if (!pos) break; // no more matches

					int o = pos-oldData;

					// calculate how much is rest in both files
					int restOld = oldLen-o;
					int restNew = newLen-newRead;
					int rest = restOld;
					saturateMin(rest,restNew);

					saturateMin(rest,goodMatch);
					int r = CountSame(newData+newRead,pos,rest);

					if (r>=minIdenticalWanted && r>=bestLen)
					{
						bestLen = r;
						bestO = o;
						if (r>=goodMatch) break;
					}
					pos--; // skip this occurence
				}
			}

			if (bestLen<goodMatch)
			{
				// no match
			}

			if (bestLen>0)
			{
				// we have found some old data
				// default loop handling will take care of them
				oldRead = bestO;
				continue;
			}
			else
			{
				// we have to save difference
				float skipSizeSat = floatMin(skipSize,1024*16);
				int diffSize = toLargeInt(skipSizeSat+0.5f);
				saturateMin(diffSize,newLen-newRead);
				SaveFromNew(newRead,diffSize);
				// skip diffSize bytes
				for (int i=0; i<diffSize; i++)
				{
					checkSumNew += newData[newRead+i];
				}
				newRead += diffSize;
				skipSize *= 1.1f;
			}

			// continue comparing
		}
		// check if there is something rest in old or new
		//
		// save change information:
		// oldLen-oldRead characters replaced by newLen-newRead
		int cnLen = newLen-newRead;
		int coLen = oldLen-oldRead;
		if (cnLen>0)
		{
			SaveFromNew(newRead,cnLen);

			// calculate cs of skipped part
			for (int n=newRead; n<newLen; n++)
			{
				checkSumNew += newData[n];
			}
		}

		ShowProgress("",0);

		Flush();

		// save summary information
		#if OUT_TEXT
		fprintf(fileTxt,"New checksum %d\n",checkSumNew);
		#endif
		fwrite(&checkSumNew,sizeof(checkSumNew),1,fileDst);

	}
	CleanUp();

	return true;
}

ApplyPatch::ApplyPatch()
{
}

template <class Type>
inline Type GetDiffT( char *&pos, char *end )
{
	if (pos+sizeof(Type)>end) return Type(0); // return error
	Type *ret = (Type *)pos;
	pos += sizeof(Type);
	return *ret;
}

static void CCALL ToLogF(const char* format, ...)
{
	char buffer[1024];
	
	va_list argptr;      
	va_start(argptr, format);
	wvsprintf(buffer, format, argptr);
	va_end(argptr);

	FILE *logFile = fopen("patch.log", "a+");
	if (!logFile)
	{
		printf("Cannot write to log file\n");
		return;
	}
	fwrite(buffer, sizeof(char), strlen(buffer), logFile);
	fclose(logFile);
}

bool ApplyPatch::Perform
(
	const char *tgtPath, const char *srcPath,const char *dataPath,
	const char *name
)
{
	const char *ext = GetFileExt(name);
	if (strcmpi(ext,".upd")) return true; // ignore unknown file types

	// apply: tgt = new, src = patch, data = old
	// remove .upd from name
	char dtaFullPath[1024];
	char tgtFullPath[1024];
	char srcFullPath[1024];
	strcpy(dtaFullPath,dataPath);
	strcpy(tgtFullPath,tgtPath);
	strcpy(srcFullPath,srcPath);
	strcat(dtaFullPath,name);
	strcat(tgtFullPath,name);
	strcat(srcFullPath,name);

	 // remove .upd extension from tgt and dta
	strcpy(GetFileExt(tgtFullPath),"");
	strcpy(GetFileExt(dtaFullPath),"");


	fileOld.Open(dtaFullPath);
	fileDiff.Open(srcFullPath);

	char *diffPos = fileDiff.GetData();
	char *diffEnd = diffPos+fileDiff.GetSize();

	if (fileDiff.GetError())
	{
		printf("Cannot read %s\n",srcFullPath);
		ToLogF("Cannot read %s\n", srcFullPath);
		return false;
	}
	char op = GetDiffT<char>(diffPos,diffEnd);
	switch (op)
	{
		case DiffRemove:
			// target file should be removed
			remove(tgtFullPath);
		return true;
		case DiffChange:
			// old file should exist
			if (fileOld.GetError())
			{
				printf("Warning: Cannot read %s\n",dtaFullPath);
				ToLogF("Cannot read %s\n", dtaFullPath);
			}
		case DiffAdd:
		break;
		default:
			// unknown operation - ignore
		return false;
	}
	// read old file
	// read changes
	// read old & new sizes
	int oldLen,newLen;

	#define getDiff() ( diffPos<diffEnd ? *diffPos++ : 0 )

	newLen = GetDiffT<int>(diffPos,diffEnd);
	oldLen = GetDiffT<int>(diffPos,diffEnd);

	if (oldLen!=fileOld.GetSize())
	{
		printf("Incompatible patch version.\n");
		ToLogF("File %s: bad size: %d (old=%d, new=%d)\n", dtaFullPath, fileOld.GetSize(), oldLen, newLen);
		return false;
	}

	FILE *dstFile = fopen(tgtFullPath,"wb");
	if (!dstFile)
	{
		printf("Cannot write %s\n",tgtFullPath);
		ToLogF("Cannot write %s\n", tgtFullPath);
		return false;
	}
	setvbuf(dstFile,NULL,_IOFBF,64*1024);

	// read change description
	int done =0;
	int newCheckSum = 0;

	const int maxWrite = 64*1024;

	while (done<newLen)
	{
		if (diffEnd<=diffPos) return false; // error

		int oldSize = GetDiffT<int>(diffPos,diffEnd);
		if (oldSize>0)
		{
			int oldPos = GetDiffT<int>(diffPos,diffEnd);
			// copy identical data from old file
			while (oldSize > maxWrite)
			{
				fwrite(fileOld.GetData()+oldPos,maxWrite,1,dstFile);
				// calculate checksum
				for (int i=0; i<maxWrite; i++)
				{
					newCheckSum += fileOld.GetData()[oldPos+i];
				}
				done += maxWrite;
				oldPos += maxWrite;
				oldSize -= maxWrite;
			}
			fwrite(fileOld.GetData()+oldPos,oldSize,1,dstFile);
			// calculate checksum
			for (int i=0; i<oldSize; i++)
			{
				newCheckSum += fileOld.GetData()[oldPos+i];
			}
			done += oldSize;
		}
		int newSize = GetDiffT<int>(diffPos,diffEnd);
		if (newSize>0)
		{
			int newPos = GetDiffT<int>(diffPos,diffEnd);
			
			while (newSize > maxWrite)
			{
				fwrite(diffPos,maxWrite,1,dstFile);
				// calculate checksum
				for (int i=0; i<maxWrite; i++)
				{
					newCheckSum += diffPos[i];
				}
				done += maxWrite;
				diffPos += maxWrite;
				newSize -= maxWrite;
			}
			fwrite(diffPos,newSize,1,dstFile);
			// calculate checksum
			for (int i=0; i<newSize; i++)
			{
				newCheckSum += diffPos[i];
			}
			diffPos += newSize;
			done += newSize;
		}
	}

	fclose(dstFile);
	dstFile = NULL;
	// read checksums from file
	int newCheckSumF = GetDiffT<int>(diffPos,diffEnd);

	if (newCheckSum != newCheckSumF)
	{
		printf("Incompatible patch version.\n");
		ToLogF("File %s: bad checksum: %d (expected=%d)\n", tgtFullPath, newCheckSum, newCheckSumF);
		remove(tgtFullPath);
		return false;
	}

	if (done != newLen)
	{
		printf("Incompatible patch version.\n");
		ToLogF("File %s: bad size: %d (expected=%d)\n", tgtFullPath, done, newLen);
		remove(tgtFullPath);
		return false;
	}

	fileDiff.Close();
	fileOld.Close();
	return true;
}

void ApplyPatch::CleanUp()
{
	fileDiff.Close();
	fileOld.Close();
}


static int ApplyRecursive
(
	IAction *action,
	const char *wild,
	const char *tgtdir, const char *srcdir, const char *datadir
)
{
	char dataBasePath[1024];
	char srcBasePath[1024];
	char tgtBasePath[1024];

	char srcWild[1024];

	strcpy(tgtBasePath,tgtdir);
	strcpy(srcBasePath,srcdir);
	strcpy(dataBasePath,datadir);

	TerminateBy(tgtBasePath,'\\');
	TerminateBy(srcBasePath,'\\');
	TerminateBy(dataBasePath,'\\');

	strcpy(srcWild,srcBasePath);
	strcat(srcWild,wild);

	int result = 0;

	// data all files that reside in "src"
	// scan srcDirectory
	// TODO: recursive search?
	_finddata_t fd;
  long hFile = _findfirst(srcWild,&fd);
	if (hFile!=-1)
	{

		do
		{
			if (fd.attrib&_A_SUBDIR)
			{
				// do recursion
				// check for fake directories
				if (!strcmp(fd.name,".")) continue;
				if (!strcmp(fd.name,"..")) continue;

				char dataFullPath[1024];
				char srcFullPath[1024];
				char tgtFullPath[1024];
				strcpy(tgtFullPath,tgtBasePath);
				strcpy(srcFullPath,srcBasePath);
				strcpy(dataFullPath,dataBasePath);
				strcat(tgtFullPath,fd.name);
				strcat(srcFullPath,fd.name);
				strcat(dataFullPath,fd.name);
				// real subdir
				// create target subdir
				_mkdir(dataFullPath);
				_mkdir(tgtFullPath);
				ApplyRecursive(action,wild,tgtFullPath,srcFullPath,dataFullPath);
				continue; // ignore subdirs
			}

			if (!action->Perform(tgtBasePath,srcBasePath,dataBasePath,fd.name))
			{
				result = 1;
			}
			action->CleanUp();
		} while( _findnext(hFile,&fd)==0 );
		_findclose(hFile);
	}
	return result;
}

int main(int argc, char* argv[])
{
	_control87(RC_NEAR|PC_24|MCW_EM,MCW_EM|MCW_RC|MCW_PC);

	CONSOLE_SCREEN_BUFFER_INFO csbi;
	BOOL bLaunched;
	
	// Lets try a trick to determine if we were 'launched' as a seperate
	// screen, or just running from the command line.

	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsoleOutput, &csbi);
	bLaunched = ((csbi.dwCursorPosition.X==0) && (csbi.dwCursorPosition.Y==0));
	if ((csbi.dwSize.X<=0) || (csbi.dwSize.Y <= 0)) bLaunched = FALSE;

	if( bLaunched ) atexit(ExitCon);

	if (argc==6)
	{
		enum Command {ComCreate,ComApply,ComNone} command = ComNone;
		if (!strcmpi(argv[1],"-create")) command = ComCreate;
		else if (!strcmpi(argv[1],"-apply")) command = ComApply;
		else goto Usage;

		ToLogF("\n**************************************************\n");
		ToLogF("New session: patch %s %s %s %s %s\n\n", argv[1], argv[2], argv[3], argv[4], argv[5]);

		// patch all files that reside in "old"
		// scan oldDirectory
		// TODO: recursive search?
		const char *wild = argv[2];
		const char *newdir = argv[3];
		const char *olddir = argv[4];
		const char *patchdir = argv[5];
		if (command==ComCreate)
		{
			// create patch from new and old file
			CreatePatch patch;
			// create: tgt = old, src = new, data = patch
			int ret = ApplyRecursive(&patch,wild,olddir,newdir,patchdir);
			if (ret)
			{
				return ret;
			}
			CreateDelete deleteA;
			// delete: tgt = new, src = old, data = patch
			ret = ApplyRecursive(&deleteA,wild,newdir,olddir,patchdir);
			if (ret)
			{
				return ret;
			}

		} // if (create)
		else if (command==ComApply)
		{
			// create patch from new and old file
			ApplyPatch patch;
			// apply: tgt = new, src = patch, data = old
			int ret = ApplyRecursive(&patch,wild,newdir,patchdir,olddir);
			if (ret)
			{
				return ret;
			}
		}
		else goto Usage;
		wasError = false;
		return 0;
	}
	Usage:
	printf("Usage:\n",argv[0]);
	printf("\n");
	//printf("\tPatch -create <in:newfile> <in:oldfile> <out:patchfile>\n");
	//printf("\tPatch -apply <out:newfile> <in:oldfile> <in:patchfile>\n");
	printf("\tPatch -create <filename or pattern> <in:newdir>  <in:olddir> <out:patchdir>\n");
	printf("\tPatch -apply  <filename> <out:newdir> <in:olddir> <in:patchdir>\n");
	return 1;
}

void CCALL ErrF( const char *format, ... )
{
}

void CCALL LogF( const char *format, ... )
{
	
}
