// ParamContext.cpp : Defines the entry point for the console application.
//

#include <class/consoleBase.h>
#include <class/appFrame.hpp>
#include <class/filenames.hpp>
#include <malloc.h>
#include <direct.h>
#include <direct.h>
#include <io.h>
#include <paramfile/paramfile.hpp>
#include <paramfile/paramFileCtx.hpp>
#include <qstream/qbstream.hpp>

ParamFile Pars;


void ReportFindEntry(const ParamEntryWithContext &e, const char *name)
{
	const ParamEntry *find = e.FindEntry(name);
	if (find)
	{
		printf
		(
			"%s found (%s), Owner: %s\n",
			name,(const char *)find->GetContext(),(const char *)find->GetOwner()
		);
		return;
	}
	printf("%s not found\n",name);
}

void ReportEnumeration(const ParamEntryWithContext &e)
{
	printf("Enumerating %s\n",(const char *)e.GetName());
	int n = e.GetEntryCount();
	for (int i=0; i<n; i++)
	{
		const ParamEntry &entry = e.GetEntry(i);
		printf
		(
			"  %s (owner %s)\n",
			(const char *)entry.GetName(),(const char *)entry.GetOwner()
		);
	}
}

int consoleMain(int argc, const char* argv[])
{
	if (argc<2)
	{
		char dir[512];
		getcwd(dir,sizeof(dir));
		chdir("bin");
		if (!Pars.ParseBinOrTxt("config.cpp"))
		{
			Pars.ParseBinOrTxt("config.bin");
		}

		chdir(dir);
	}
	else
	{
		Pars.ParseBinOrTxt(argv[1]);
	}
	if (Pars.GetEntryCount()<=0)
	{
		fprintf(stderr,"Input file empty");
		return 1;
	}
	// now load all addon config files
	// scan .pbo in Addons directory
	_finddata_t fd;

	char dir[512];
	getcwd(dir,sizeof(dir));
	chdir("Addons");

	long h = _findfirst("*.pbo",&fd);
	if (h!=-1)
	{
		do
		{
			if (fd.attrib&_A_SUBDIR)
			{
				// ignore subdirs
				continue;
			}
			QFBank bank;
			char bankname[256];
			GetFilename(bankname,fd.name);
			bank.open(bankname);
			// check config file
			ParamFile addonConfig;
			printf("Addon %s\n",fd.name);
			/*
			if (bank.FileExists("config.cpp"))
			{
				QIFStreamB f;
				f.open(bank,"config.cpp");
				addonConfig.Parse(f);
			}
			else
			*/
			if (bank.FileExists("config.bin"))
			{
				addonConfig.ParseBin(bank,"config.bin");
			}
			if (addonConfig.GetEntryCount()<=0)
			{
				printf("Addon %s - config empty\n",fd.name);
				continue;
			}
			addonConfig.SetOwner(bankname);
			Pars.Update(addonConfig,&Pars);
			Pars.SetFile(&Pars);
		} while (_findnext(h,&fd)==0);
		_findclose(h);
	}

	chdir(dir);
	ParamOwnerList addons;
	addons.Add("deer");
	addons.Add("flags");
	addons.Add("xms");

	ParamOwnerList addons2;
	addons2.Add("o");

	const ParamEntry *find = NULL;
	ParamEntryWithContext parsExt(Pars,addons);
	ParamEntryWithContext parsExt2(Pars,addons2);

	// check different access points

	ReportFindEntry(parsExt>>"CfgVehicles","SoldierWSaboteurXMS");
	ReportFindEntry(parsExt>>"CfgVehicles","Deer");
	ReportFindEntry(parsExt>>"CfgVehicles","DeerWomen");
	ReportFindEntry(parsExt>>"CfgVehicles","NoSuchEntry");
	ReportFindEntry(parsExt>>"CfgVehicles","BicycleTest");
	ReportFindEntry(parsExt>>"CfgVehicles","Jawa");

	ReportEnumeration(parsExt>>"CfgMusic");

	ParamEntryWithContext entry1 = parsExt>>"CfgVehicles">>"SoldierWSaboteurXMS";
	ParamEntryWithContext entry2 = parsExt2>>"CfgVehicles">>"Jawa";
	if (entry1.GetEntry().CheckVisible(addons))
	{
		printf
		(
			"%s (owner %s) visible by addons\n",
			(const char *)entry1.GetName(),(const char *)entry1.GetOwner()
		);
	}
	if (entry1.GetEntry().CheckVisible(addons2))
	{
		printf
		(
			"%s (owner %s) visible by addons2\n",
			(const char *)entry1.GetName(),(const char *)entry1.GetOwner()
		);
	}
	if (entry2.GetEntry().CheckVisible(addons))
	{
		printf
		(
			"%s (owner %s) visible by addons\n",
			(const char *)entry2.GetName(),(const char *)entry2.GetOwner()
		);
	}
	if (entry2.GetEntry().CheckVisible(addons2))
	{
		printf
		(
			"%s (owner %s) visible by addons2\n",
			(const char *)entry2.GetName(),(const char *)entry2.GetOwner()
		);
	}

	return 1;
}


void *operator new(size_t size,char const *file,int line)
{
	return malloc(size);
}

void *operator new(size_t size)
{
	return malloc(size);
}

void operator delete(void *mem)
{
	free(mem);
}
