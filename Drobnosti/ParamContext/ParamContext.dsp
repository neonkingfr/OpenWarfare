# Microsoft Developer Studio Project File - Name="ParamContext" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 60000
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=ParamContext - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ParamContext.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ParamContext.mak" CFG="ParamContext - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ParamContext - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "ParamContext - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ParamContext - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /W3 /GR /GX- /Zi /O2 /I "\c\element" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "_RELEASE" /FD /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /version:0.88 /subsystem:console /debug /machine:I386 /force

!ELSEIF  "$(CFG)" == "ParamContext - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /GZ  /c
# ADD CPP /nologo /W3 /Gm /GR /GX- /ZI /Od /I "\c\element" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FD /GZ  /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /force /pdbtype:sept

!ENDIF 

# Begin Target

# Name "ParamContext - Win32 Release"
# Name "ParamContext - Win32 Debug"
# Begin Group "Element"

# PROP Default_Filter ""
# Begin Group "ParamFile"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Element\ParamFile\paramFile.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\ParamFile\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\ParamFile\paramFileCtx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\ParamFile\paramFileCtx.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\ParamFile\paramFilePreproc.cpp
# End Source File
# End Group
# Begin Group "QStream"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Element\QStream\fileCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileMapping.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileOverlapped.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileOverlapped.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\qbstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\qstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\serializeBin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\ssCompress.cpp
# End Source File
# End Group
# Begin Group "Preprocessor"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Element\Preprocessor\Preproc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\Preprocessor\Preproc.h
# End Source File
# End Group
# End Group
# Begin Group "Class"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\suma\class\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\class\consoleBase.cpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\class\fastAlloc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\class\rString.cpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ParamContext.cpp
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
