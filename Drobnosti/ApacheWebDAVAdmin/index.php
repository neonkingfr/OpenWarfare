<?php
# Administrace (C) 2011 Jan Hlavaty, hlavac@bistudio.com
#
/* --- CONFIG --- */
$apache_config = '/data/web/ext.bistudio.com/cfg/generated.cfg';
$htpasswd_file = '/data/web/ext.bistudio.com/cfg/users.cfg';
$group_file = '/data/web/ext.bistudio.com/cfg/groups.cfg';
$daemon_socket = '/data/web/_default_/cfg/reloader.pipe';
$daemon_key = '3bdfe362a5b33c8878c';
$lock_file = '/data/web/ext.bistudio.com/cfg/lock.lck';
$repository = '/data/web/ext.bistudio.com/content/users';
$repository_url = 'https://ext.bistudio.com/users/';
$repository_location = '/users';

/*----------------*/

$global_lock = false;
# Enforce a global lock on this script file to avoid inconsistencies.
acquireGlobalLock();

try {
    $config = new Configuration($htpasswd_file,$group_file,$apache_config,$repository_location,$repository);
} catch (Exception $e) {
    releaseGlobalLock();
    die("Failed to load config");
}


$action="";
if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

switch ($action) {
    case 'createuser':
        processCreateUserForm();
        break;
    case 'edituser':
        processEditUserForm();
        break;
    default:
        processDefault();
}

releaseGlobalLock();
/*-----------------------------------------------------------------------------*/

function acquireGlobalLock() {
   global $global_lock;
   global $lock_file;
   
    $global_lock = fopen($lock_file,'x+b');
    if (!$global_lock) {
        $timeout = 1000;
        while (!$global_lock) {
            $timeout--;
            if ($timeout<=0) die("Lock timeout");
            usleep(10000);  /* wait 10ms */
            $global_lock = fopen($lock_file,'x+b');
        }
    }
}

function releaseGlobalLock() {
   global $global_lock;
   global $lock_file;
   unlink($lock_file);
   fclose($global_lock);
}

function reload_apache_config() {
    global $daemon_socket, $daemon_key;
    if (file_exists($daemon_socket)) {
        $pipe = fopen($daemon_socket, 'w') or die('Failed to open socket to reload daemon');
        fwrite($pipe,$daemon_key);
        fclose($pipe);
    }
}


function redirect($args = array()) {
        //header("HTTP/1.1 302 See other");
        header('Location: '.makeurl($args));
}

function htmlheader($title) {
    return "<!DOCTYPE html public \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n<html>\n<head>\n<title>".htmlspecialchars($title)."</title>\n<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\">\n<body>";
}

function htmlfooter() {
    return "</body></html>";
}

function htmlbox($content) {
    return("<div class=\"box\">".$content."</div>");
}

function htmllink($url,$content) {
    return("<a href=\"".htmlspecialchars($url)."\">".$content."</a>");
}
function htmllinknew($url,$content) {
    return("<a href=\"".htmlspecialchars($url)."\" target=\"_blank\">".$content."</a>");
}

function cleanUsername($username) {
    if ($username == "") throw new Exception ("Username can not be empty");
    if ($username == "admin") throw new Exception ("Username admin is reserved");
    if ($username == "access") throw new Exception ("Username access is reserved");
    if (preg_match('/^[A-Za-z]+[A-Za-z0-9._-]*$/', $username) != 1) throw new Exception ("Username contains forbidden characters!");
    return $username;
}


function makeurl($args = Array() ) {
    $port = $_SERVER["SERVER_PORT"];
    $is_ssl = false;
    if ($_SERVER["HTTPS"]=="on") {
	$scheme = "https://";
	if ($port == 443) {
	    $port = "";
	} else {
	    $port = ":".$port;
	}
    } else {
        $scheme = "http://";
	if ($port == 80) {
	    $port = "";
	} else {
	    $port = ":".$port;
	}
    }
    $scriptname = $_SERVER["SCRIPT_NAME"];
    $scriptnamelen = strlen($scriptname);
    $suffix = substr($scriptname,$scriptnamelen-10,10);
    if ($suffix == "/index.php") {
        $scriptname = substr($scriptname,0,$scriptnamelen-9);
    }
    $url = $scheme.$_SERVER["SERVER_NAME"].$port.$scriptname;
    $split_char='?';
    foreach ($args as $key => $value) {
	$url = $url.$split_char.urlencode($key)."=".urlencode($value);
	$split_char = '&';
    }
    return $url;
}



function processCreateUserForm() {
    global $config;
    
    if ($_SERVER['REQUEST_METHOD']=='POST') {
        # process submitted create user form.
        if (isset($_POST['cancel'])) {
            # cancel, redirect to main
            redirect();
        } else {
            try {
                
                $username = $_POST['username'];
                $password = $_POST['password'];
                $is_enabled = isset($_POST['enabled']);
                $is_access = isset($_POST['access']);
                $is_admin = isset($_POST['admin']);
                
                $config->createNewUser($username,$password);
                $config->setPermissions($username, $is_enabled, $is_access, $is_admin);
                $config->save();
                        
                redirect();
                
            } catch (Exception $e) {
                handleException($e);
            }
        }
    } else {
        # show create user form.
        print htmlheader("Create new user");
        print pageHeader();
        print "<h1>Create new user</h1>\n";
        print '<form action="'.makeurl(array("action"=>"createuser")).'" method="POST">';
        print '<table class="form">';
        print '<tr><th>Username:</th><td><input type="text" name="username" value="" size="40"></td></tr>';
        print '<tr><th>Password:</th><td><input type="text" name="password" value="" size="40"></td></tr>';
        print '<tr><th>Enabled:</th><td><input type="checkbox" name="enabled" checked="checked"></td></tr>';
        print '<tr><th>Access all repositories:</th><td><input type="checkbox" name="access"></td></tr>';
        print '<tr><th>Administrator:</th><td><input type="checkbox" name="admin"></td></tr>';
        
        print '<tr><td colspan=2 style="text-align: center;"><input type="submit" name="create" value="Create new user">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="cancel" value="Cancel"></td></tr></table>';
        print '</form>';
        
        print htmlfooter();
    }
}

function processEditUserForm() {
    global $config;
    
    if ($_SERVER['REQUEST_METHOD']=='POST') {
        if (isset($_POST['cancel'])) {
            # cancel, redirect to main
            redirect();
        } elseif (isset($_POST['delete'])) {
            # delte user
            $username = $_GET['username'];
            print htmlheader("Delete user");
            print pageHeader();
            
            print '<form method="POST" action="';
            print makeurl(array('action'=>'edituser','username'=>$username));
            print '"><table class="form"><td colspan="2">Are you sure you want to delete user ';
            print $username;
            print '?</td></tr><tr><td><input type="submit" name="reallydelete" value="Yes delete"></td><td style="text-align: right;"><input type="submit" name="cancel" value="Cancel"></td></tr></table>';
            print htmlfooter();
        } elseif (isset($_POST['reallydelete'])) {
            try {
                $username = $_GET['username'];
                $config->deleteUser($username);
                $config->save();
                redirect();
            } catch (Exception $e) {
                handleException($e);
            }
        } elseif (isset($_POST['update'])) {
            try {
                
                $username = $_GET['username'];
                $password = $_POST['password'];
                $is_enabled = isset($_POST['enabled']);
                $is_access = isset($_POST['access']);
                $is_admin = isset($_POST['admin']);
                
                if ($password != "") {
                    $config->setPassword($username,$password);
                }
                
                $config->setPermissions($username, $is_enabled, $is_access, $is_admin);
                $config->save();
                        
                redirect();
                
            } catch (Exception $e) {
                handleException($e);
            }
        }
    } else {
        # show create user form.
        try {
            $username = $_GET['username'];
            $is_enabled = $config->isEnabled($username);
            $is_access = $config->isAccess($username);
            $is_admin = $config->isAdmin($username);
            
            
            
            print htmlheader('Editing user '.$username);

            print pageHeader();

            print '<h1>Editing user "';
            print htmlspecialchars($username);
            print '"</h1>';

            print '<form method="POST" action="';
            print makeurl(array('action'=>'edituser','username'=>$username));
            print '">';

            print '<table class="form"><tr><th>New password:</th><td><input name="password" value="" size="16"></td></tr>';
            print '<tr><th>Enabled:</th><td><input type="checkbox" name="enabled"';
            if ($is_enabled) {
                print ' checked="checked"';
            }
            print "></td></tr>\n";
            print '<tr><th>Access:</th><td><input type="checkbox" name="access"';
            if ($is_access) {
                print ' checked="checked"';
            }
            print "></td></tr>\n";
            print '<tr><th>Admin:</th><td><input type="checkbox" name="admin"';
            if ($is_admin) {
                print ' checked="checked"';
            }
            print "></td></tr>\n";
            print '<tr><td colspan=2><input type="submit" name="update" value="Update">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="delete" value="Delete">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="cancel" value="Cancel">';
            print "</table>\n";
            print htmlfooter();
        } catch (Exception $e) {
            handleException($e);
        }
    }
}

function handleException($e) {
    print htmlheader("Error");
    print pageHeader();
    print htmlbox(htmlspecialchars($e->getMessage()));
    print "<div>".homeButton("Continue")."</div>";
    print htmlfooter();
}

function homeButton($text) {
    return '<form action="'.makeurl().'" method="GET"><input type="submit" value="'.htmlspecialchars($text).'"></form>';
}

function processDefault() {
        global $config;
        global $repository_url;
        
        header("Cache-Control: no-cache");
	print htmlheader("WebDAV administration");
        print pageHeader();
        
        print "<table><tr><th>Username:</th><th>Repository URL:</th><th>Enabled</th><th>Access</th><th>Admin</th></tr>";
        foreach ($config->getUsernames() as $username) {
            $repourl = $repository_url.$username.'/';
            print "<tr><td>".htmllink(makeurl(array('action'=>'edituser','username'=>$username)),htmlspecialchars($username))."</td><td>".htmllinknew($repourl,$repourl)."</td>";
            print "<td>";
            if ($config->isEnabled($username)) {
                print "*";
            } else {
                print "&nbsp;";
            }
            print "</td><td>";
            if ($config->isAccess($username)) {
                print "*";
            } else {
                print "&nbsp;";
            }
            print "</td><td>";
            if ($config->isAdmin($username)) {
                print "*";
            } else {
                print "&nbsp;";
            }
            print "</td></tr>";
        }
        print "</table>";
        
        print htmllink(makeurl(array('action'=>'createuser')),"Create new user");
        
	print htmlfooter();
}


function pageHeader() {
    return '<div style="text-align: center;"><img src="/logo.png" width=350 height=131 alt="Bohemia Interactive"><br><h1>WebDAV repository administration</h1></div>';
}


class Configuration {
    var $passwd;
    var $groups;
    var $config = "";
    var $location = "";
    var $path = "";
    var $changed = false;
    
    
    public function __construct($passwdfile,$groupfile,$apache_config,$repository_location,$repository_path) {
        $this->config = $apache_config;
        $this->location = $repository_location;
        $this->path = $repository_path;
        $this->passwd = new PasswdFile($passwdfile);
        $this->groups = new GroupsFile($groupfile);
    }
    
    public function save() {
        $this->passwd->save();
        $this->groups->save();
        
        
        $fh = fopen($this->config,"w");
        if (!$fh) throw new Exception ("Unable to write Apache config $this->config");
        foreach ( $this->passwd->getUsernames() as $username) {
            fwrite($fh,"<Location $this->location/$username>\n");
            fwrite($fh,"    Satisfy all\n");
            fwrite($fh,"    Require group admin access $username\n");
            fwrite($fh,"</Location>\n");
        }
        fclose($fh);
        reload_apache_config();
    }



    public function createNewUser($username, $password) {
        $username = cleanUsername($username);
        
        if ($this->passwd->userExists($username)) throw new Exception("User $username already exists");

        # create repository for the user
        $repopath = $this->path.'/'.$username;
        if (!file_exists($repopath)) {
            mkdir($repopath);
        }
        # create user htpasswd entry
        $this->passwd->addUser($username, $password);
        # create users group
        $this->groups->addGroup($username);
    }
    
    public function deleteUser($username) {
        if (!$this->passwd->userExists($username)) throw new Exception("User $username does not exist");
        $this->groups->removeUser($username);
        $this->groups->removeGroup($username);
        $this->passwd->removeUser($username);
        # TODO: should delete users repository as well
    }
    
    public function setPermissions($username, $is_enabled, $is_access, $is_admin) {
        if (!$this->passwd->userExists($username)) throw new Exception("User $username does not exist");
        
        if ($this->groups->isMember($username, $username)) {
            if (!$is_enabled) {
                $this->groups->removeMember($username, $username);
            }
        } else {
            if ($is_enabled) {
                $this->groups->addMember($username, $username);
            }
        }

        if ($this->groups->isMember($username, 'access')) {
            if (!$is_access) {
                $this->groups->removeMember($username, 'access');
            }
        } else {
            if ($is_access) {
                $this->groups->addMember($username, 'access');
            }
        }
            
        if ($this->groups->isMember($username, 'admin')) {
            if (!$is_admin) {
                $this->groups->removeMember($username, 'admin');
            }
        } else {
            if ($is_admin) {
                $this->groups->addMember($username, 'admin');
            }
        }
    }

    public function setPassword($username,$password) {
        $this->passwd->setPasswordFor($username, $password);
    }
    
    public function isEnabled($username) {
        return $this->groups->isMember($username, $username);
    }
    
    public function isAccess($username) {
        return $this->groups->isMember($username, 'access');
    }
    
    public function isAdmin($username) {
        return $this->groups->isMember($username, 'admin');
    }

    public function getUsernames() {
        $usernames = $this->passwd->getUsernames();
        sort($usernames);
        return $usernames;
    }
}


class PasswdFile
{
    var $changed = false;
    var $passwdfile = "";
    var $passwdtable = array();
    
    public function __construct($filename) {
        $this->passwdfile = $filename;
        $fh = fopen($filename,"r");
        if (!$fh) throw new Exception("Unable to load passwdfile");
        while (!feof($fh)) {
            $line = trim(fgets($fh));
            if ( (strlen($line)>0) && (strpos($line,':')>0)) {
                $linebits = explode(':',$line,2);
                $username = $linebits[0];
                $passwordhash = $linebits[1];
                $this->passwdtable[$username]=$passwordhash;
            }
        }
        fclose($fh);
    }
    
    public function save() {
        if ($this->changed) {
            $fh = fopen($this->passwdfile.".new","w");
            if (!$fh) throw new Exception("Unable to save passwdfile");
            foreach ($this->passwdtable as $username => $passwordhash) {
                fwrite($fh, $username.':'.$passwordhash."\n");
            }
            fclose($fh);

            unlink($this->passwdfile.".old");
            if (!rename($this->passwdfile,$this->passwdfile.".old")) throw new Exception("Unable to rename ".$this->passwdfile." to ".$this->passwdfile.".old");
            if (!rename($this->passwdfile.".new",$this->passwdfile)) throw new Exception ("Unable to rename ".$this->passwdfile.".new"." to ".$this->passwdfile);
            if (!unlink($this->passwdfile.".old")) throw new Exception ("Unable to delete ".$this->passwdfile.".old");
            $this->changed = false;
        }
    }
    
    public function getUsernames() {
        $ret = array();
        foreach (array_keys($this->passwdtable) as $username) {
            $ret[] = $username;
        }
        return $ret;
    }
    
    
    public function userExists($username) {
        return array_key_exists($username, $this->passwdtable);
    }
    
    public function addUser($newusername,$newpassword) {
        if ($this->userExists($newusername)) throw new Exception("User $newusername already exists");
        $this->passwdtable[$newusername] = crypt($newpassword);
        $this->changed = true;
    }
    
    public function removeUser($username) {
        if ($this->userExists($username)) {
            unset($this->passwdtable[$username]);
        } else throw new Exception ("User $username does not exitst thus cannot be deleted");
        $this->changed = true;
    }
    
    public function setPasswordFor($username, $password) {
        if ($this->userExists($username)) {
            $this->passwdtable[$username] = crypt($password);
        }
        $this->changed = true;
    }
    
}

class GroupsFile
{
    //var $loaded = false;
    var $changed = false;
    var $groupfile = "";
    var $grouptable = array();
    
    function __construct($filename) {
        $this->groupfile = $filename;
        $this->grouptable = array();
        $this->changed = false;
        
        $fh = fopen($filename,"r");
        if (!$fh) throw new Exception("Unable to load groupfile $filename");
        while (!feof($fh)) {
            $line = trim(fgets($fh));
            if ( (strlen($line)>0) && (strpos($line,':')>0)) {
                $linebits = explode(':',$line,2);
                $groupname = $linebits[0];
                $this->grouptable[$groupname] = array();
                $members = explode(' ',$linebits[1]);
                foreach ($members as $membername) {
                    $trimmed = trim($membername);
                    if ($trimmed != "") {
                        $this->grouptable[$groupname][$membername] = $membername;
                    }
                }
            }
        }
        fclose($fh);
    }
    
    public function save() {
        if ($this->changed) {
            $fh = fopen($this->groupfile.".new","w");
            if (!$fh) throw new Exception("Unable to save groupfile $this->groupfile");
            foreach ($this->grouptable as $groupname => $members) {
                fwrite($fh, $groupname.':');
                foreach ($members as $membername) {
                    fwrite($fh,' '.$membername);
                }
                fwrite($fh,"\n");
            }
            fclose($fh);

            unlink($this->groupfile.".old");
            if (!rename($this->groupfile,$this->groupfile.".old")) throw new Exception("Unable to rename $this->groupfile to $this->groupfile.old");
            if (!rename($this->groupfile.".new",$this->groupfile)) throw new Exception ("Unable to rename $this->groupfile.new to $this->groupfile");
            if (!unlink($this->groupfile.".old")) throw new Exception ("Unable to delete $this->groupfile.old");
        }
        $this->changed = false;
    }
    
    public function getGroups() {
        return array_keys($this->grouptable);
    }
    
    public function groupExists($groupname) {
        return array_key_exists($groupname,  $this->grouptable);
    }
    
    public function addGroup($groupname) {
        if ($this->groupExists($groupname)) throw new Exception ("Group $groupname already exists!");
        $this->grouptable[$groupname] = array();
    }
    
    public function removeGroup($groupname) {
        if (!$this->groupExists($groupname)) throw new Exception ("Group $groupname does not exist!");
        unset($this->grouptable[$groupname]);
        unset($this->grouptable[$groupname]);
    }
    
    public function getGroupMembers($groupname) {
        if (!$this->groupExists($groupname)) throw new Exception ("Group $groupname does not exist!");
        return array_keys($this->grouptable[$groupname]);
    }

    public function isMember($username, $groupname) {
        if (!$this->groupExists($groupname)) throw new Exception ("Group $groupname does not exist!");
        return array_key_exists($username, $this->grouptable[$groupname]);
    }

    public function getUsersGroups($username) {
        $groups = array();
        foreach ($this->getGroups() as $groupname) {
            if ($this->isMember($username, $groupname)) {
                $groups[] = $groupname;
            }
        }
        return $groups;
    }
    
    
    public function addMember($username, $groupname) {
        if ($this->isMember($username, $groupname)) throw new Exception ("User $username is already member of $groupname");
        $this->grouptable[$groupname][$username] = $username;
        $this->changed = true;
    }
    
    public function removeMember($username, $groupname) {
        if (!$this->isMember($username, $groupname)) throw new Exception ("User $username is not member of $groupname");
        unset($this->grouptable[$groupname][$username]);
        $this->changed = true;
    }
    
    public function removeUser($username) {
        foreach ($this->getUsersGroups($username) as $group) {
            $this->removeMember($username, $group);
        }
    }
    
}





?>
