#include <Es/essencepch.hpp>
#include <stdio.h>
#include <io.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include <Es/Framework/consoleBase.h>
#include <Es/Containers/array.hpp>
#include <Es/Algorithms/qsort.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Framework/debugLog.hpp>
#include <Es/Strings/rstring.hpp>

struct ChangeInfo
{
	RString file;
	RString version;
	RString date;
	RString by;
	RString text;

	void Reset()
	{
		file = RString();
		version = RString();
		date = RString();
		by = RString();
		text = RString();
	}
};

static int SortChange(const ChangeInfo *c1, const ChangeInfo *c2)
{
	int d;
	d = strcmp(c1->version,c2->version);
	if (d) return d;
	d = strcmp(c1->date,c2->date);
	if (d) return d;
	d = strcmp(c1->file,c2->file);
	if (d) return d;
	d = strcmp(c1->by,c2->by);
	if (d) return d;
	d = strcmp(c1->text,c2->text);
	return d;
}

inline int SortChangeRev(const ChangeInfo *c1, const ChangeInfo *c2)
{
	return SortChange(c2,c1);
}

TypeIsMovableZeroed(ChangeInfo)

static const char *Extensions[]=
{
	".h",".c",".hpp",".cpp",
	NULL
};

static bool StringInList(const char **list, const char *t)
{
	while (*list)
	{
		if (!strcmpi(t,*list)) return true;
		list++;
	}
	return false;
}

const char *SkipStart(const char *text, const char *start)
{
	int len = strlen(start);
	if (strnicmp(text,start,len)) return NULL;
	return text+len;
}

struct PatchPars
{
	// in
	const char *command;
	bool skipDateHeader;
	bool printFilenames;
  /// what version from
  RString from;
	// out
	AutoArray<ChangeInfo> list;
};


void PatchList(const char *filename, PatchPars &pars)
{
	char command[256];
	char outbuf[256];
	*outbuf = 0;


	FILE *f = fopen(filename,"r");
	if (!f) return;
	Log("Input file %s",filename);
	// process input
	bool inCppComment = false;
	bool cCommentStar = false;
	bool inCComment = false;
	bool slash = false;
	bool inDocCommand = false;
	bool startComment = false;
	bool specCommand = false;
	bool startOutput = false;

	// set to true when some leading characters have been written to output
	bool outbufShifted = false;

	ChangeInfo change; // actually processed change

	for(;;)
	{
		int c = fgetc(f);
		if (c==EOF) break;
		if (inCComment || inCppComment)
		{
			if (inDocCommand)
			{
				if (specCommand)
				{
					if (isalpha(c) || c=='_')
					{
						int len = strlen(command);
						if (len<sizeof(command)-1)
						{
							command[len] = c;
							command[len+1] = 0;
						}
					}
					else
					{
						specCommand = false;
						startOutput = !strcmpi(command,pars.command);
					}
				}
				else if (c=='\\')
				{
					specCommand = true;
					*command = 0;
					if (startOutput && strlen(outbuf)>1)
					{
						change.text = change.text + RString(outbuf,strlen(outbuf)-1) + RString("\n");
            if (pars.from.GetLength()==0 || strcmp(change.version,pars.from)>0)
            {
					    pars.list.Add(change);
            }
						change.Reset();
						*outbuf = 0;
						outbufShifted = false;
					}
					startOutput = false;
				}
				if (inDocCommand && startOutput)
				{
					int outlen = strlen(outbuf);
					if (outlen>sizeof(outbuf)-2)
					{
						int wlen = outlen-2;
						change.text = change.text + RString(outbuf,wlen);
						char tempbuf[sizeof(outbuf)];
						strcpy(tempbuf,outbuf+wlen);
						strcpy(outbuf,tempbuf);
						outlen -= wlen;
						outbufShifted = true;
					}
					outbuf[outlen] = c;
					outbuf[outlen+1] = 0;

					// check for \patch pattern and 
					if (!outbufShifted)
					{
						// scan version
						const char *beg = outbuf;
						char *end;
						while (isspace(*beg)) beg++;
						int major = strtoul(beg,&end,10);
            int minor = -1; // negative minor means a build number is used
						if (*end=='.')
            {
							minor = strtoul(end+1,&end,10);
            }
						beg = end;
            // newer comments are based on build number
						{
							const char *startBy,*startDate;

							while (isspace(*beg)) beg++;
							const char *ignore = SkipStart(beg,"Date");
							if (!ignore) goto NoSkip;
							beg = ignore;
							while (isspace(*beg)) beg++;
							// skip Date
							startDate = beg;
							while (isdigit(*beg) || *beg=='/') beg++;
							RString dateString(startDate,beg-startDate);
							while (isspace(*beg)) beg++;
							ignore = SkipStart(beg,"by");
							if (!ignore) goto NoSkip;
							beg = ignore;
							while (isspace(*beg)) beg++;
							// skip name
							if (*beg==0 || !isalpha(*beg)) goto NoSkip;
							startBy = beg;
							while (isalpha(*beg)) beg++;
							RString byString(startBy,beg-startBy);
							if (*beg=='.') beg++;
							if (*beg==':') beg++;
							if (!isspace(*beg)) goto NoSkip;
							while (isspace(*beg)) beg++;

							// skip upto beg from outbuf
							*outbuf = 0;
							outbufShifted = true;

              if (minor>=0)
              {
							  change.version = Format("%d.%02d ",major,minor);
              }
              else
              {
							  change.version = Format("%d ",major);
              }
							//strcat(tempbuf,beg);
							change.file = filename;
							change.by = byString;
							change.date = dateString;
							change.text = beg;

						}
						NoSkip:;
						
					}
				}
			}
			if (startComment)
			{
				if (c=='!')
				{
					inDocCommand = true;
					specCommand = false;
					startOutput = false;
				}
			}
			startComment = false;
		}
		if (c=='\n')
		{
			if (inCppComment)
			{
				inCppComment = false;
				startComment = false;
				inDocCommand = false;
				if (inDocCommand && startOutput)
				{
					change.text = change.text + RString(outbuf);
          if (pars.from.GetLength()==0 || strcmp(change.version,pars.from)>0)
          {
					  pars.list.Add(change);
          }
					change.Reset();
					*outbuf = 0;
					outbufShifted = false;
				}
			}
		}
		if (inCppComment)
		{
		}
		else if (inCComment)
		{
			if (cCommentStar)
			{
				if (c=='/')
				{
					inCComment = false;
					startComment = false;
					if (inDocCommand && startOutput)
					{
						inDocCommand = false;
						change.text = change.text + RString(outbuf,strlen(outbuf)-2) + RString("\n");
            if (pars.from.GetLength()==0 || strcmp(change.version,pars.from)>0)
            {
					    pars.list.Add(change);
            }
						change.Reset();
						*outbuf = 0;
						outbufShifted = false;
					}
				}
			}
			cCommentStar = ( c=='*');
		}
		else if (slash)
		{
			if (c=='*') inCComment = true, startComment = true;
			else if (c=='/') inCppComment = true, startComment = true;
			slash = false;
		}
		else if (c=='/')
		{
			slash = true;
		}
		// process // comments
	}
	fclose(f);
}

void PatchList(const char *path, const char **extensions, PatchPars &pars)
{
  bool once = true;
	char wild[1024];
	strcpy(wild,path);
	TerminateBy(wild,'\\');
	strcat(wild,"*");

	_finddata_t data;
	long h = _findfirst(wild,&data);
	if (h!=-1)
	{
		do
		{
			char subpath[1024];
			strcpy(subpath,path);
			TerminateBy(subpath,'\\');
			strcat(subpath,data.name);
			if (data.attrib&_A_SUBDIR)
			{
				if (!strcmp(data.name,".") || !strcmp(data.name,".."))
				{
					continue;
				}
				PatchList(subpath,extensions,pars);
			}
			else
			{
				// check for extension match
				const char *ext = GetFileExt(data.name);
				if (!StringInList(extensions,ext))
				{
					continue;
				}
        if (once)
        {
        	fprintf(stderr,"Scanning %s\n",path);
          once = false;
        }
				PatchList(subpath,pars);
			}
		} while (_findnext(h,&data)==0);
		_findclose(h);
	}
}


int consoleMain(int argc, const char *argv[])
{
	// scan source paths, generate list of \patch sections
	// send output to stdout
	PatchPars pars;
	pars.command = "patch";
	pars.skipDateHeader = false;	
	pars.printFilenames = false;
	bool sort = false;
	bool sortRev = false;
	while (argc>1)
	{
		if (*argv[1]=='-')
		{
			const char *option = argv[1]+1;
			const char *oarg = NULL;
			if ((oarg = SkipStart(option,"command="))!=NULL)
			{
				pars.command = oarg;
				#if _DEBUG
				fprintf(stderr,"Command %s\n",oarg);
				#endif
			}
			if ((oarg = SkipStart(option,"from="))!=NULL)
			{
				pars.from = oarg;
				#if _DEBUG
				fprintf(stderr,"From %s\n",oarg);
				#endif
			}
			else if (!strcmpi(option,"noheaders"))
			{
				pars.skipDateHeader = true;	
				#if _DEBUG
				fprintf(stderr,"No headers\n");
				#endif
			}
			else if (!strcmpi(option,"filenames"))
			{
				pars.printFilenames = true;	
				#if _DEBUG
				fprintf(stderr,"Print filenames\n");
				#endif
			}
			else if (!strcmpi(option,"sort")) sort = true;	
			else if (!strcmpi(option,"sortrev")) sortRev = true;	
			argv++;
			argc--;
		}
		else break;
	}
	if (argc<2)
	{
		printf
		(
			"Usage: patchlist {-option} source_path\n"
			"Options:\n"
			"    -command=<text>      specifies command to search for\n"
			"    -ext=<ext1>,<ext2>   specifies extensions to scan\n"
			"    -noheaders           skip Date and By part of headers\n"
			"    -files               print filename before each change (works only with noheaders)\n"
			"    -sort                sort by version and date\n"
			"    -sortRev             sort by version and date (descending)\n"
			"    Default extensions scanned: h,c,hpp,cpp\n"
			"    Default command is patch"
			"    Assumed header format (for header skipping) is\n"
			"        <num>.<num> Date <num>/<num>/<num> by <name>[.]\n"
			"\n"
			"patchlist scans for DOxygen command \"patch\" and generates list from them\n"
			"Output is send to standard output\n"
			"\n"
			"Examples:\n"
			"patchlist w:\\c\\project >patch.txt\n"
			"patchlist w:\\c\\company >>patch.txt\n"
		);

	}
	while (argc>=2)
	{
		const char *path = argv[1];


		PatchList(path,Extensions,pars);
		argv++;
		argc--;
	}

	// sort and print all changes
	if (sort)
	{
		QSort(pars.list.Data(),pars.list.Size(),SortChange);
	}
	else if (sortRev)
	{
		QSort(pars.list.Data(),pars.list.Size(),SortChangeRev);
	}
	for (int i=0; i<pars.list.Size(); i++)
	{
		const ChangeInfo &info = pars.list[i];
		if (pars.printFilenames)
		{
			printf("%s ",GetFilenameExt(info.file));
		}
		printf((const char *)info.version);
		if (!pars.skipDateHeader)
		{
			printf(" Date %s by %s",(const char *)info.date,(const char *)info.by);
		}
		printf(" %s",(const char *)info.text);
	}

	#if _DEBUG
		return 1;
	#else
		return 0;
	#endif
}
