// CfgConvert.cpp : Defines the entry point for the console application.
//

#include <El/elementpch.hpp>
#include <El/ParamFile/paramFile.hpp>
#include <El/ParamXML/paramXML.hpp>

#include <string.h>
#include <stdio.h>
#include <time.h>

static bool GSilent = false;
#include "Es/Framework/appFrame.hpp"
class CfgConvertSimpleFrameFunctions: public AppFrameFunctions
{
public:
#if _ENABLE_REPORT
  virtual void ErrF(const char *format, va_list argptr)
  {
    if (GSilent) return; 
    BString<512> buf;
    vsprintf(buf,format,argptr);
    strcat(buf,"\n");
#ifdef _WIN32
#ifdef UNICODE
    WCHAR wBuf[512];
    MultiByteToWideChar(CP_ACP, 0, buf, -1, wBuf, lenof(wBuf));
    OutputDebugString(wBuf);
#else
    OutputDebugString(buf);
#endif // !UNICODE
    fputs(buf,stderr);
#else
    fputs(buf,stderr);
#endif
  };

  virtual void ErrorMessage(ErrorMessageLevel level, const char *format, va_list argptr)
  {
#if _ENABLE_REPORT
    ErrF(format,argptr);
#endif
  }

  virtual void ErrorMessage(const char *format, va_list argptr)
  {
#if _ENABLE_REPORT
    ErrF(format,argptr);
#endif
  }
#endif
};

#if _MSC_VER && !defined INIT_SEG_COMPILER
#pragma warning(disable:4074)
#pragma init_seg(compiler)
#define INIT_SEG_COMPILER
#endif

static CfgConvertSimpleFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;




static bool LoadParamFile(ParamFile &f, const char *name)
{
  // autodetect format based on extension or content?
	if (f.ParseBin(name))
  {
    return true;
  }
  if (ParseXML(f,name))
  {
    return true;
  }
  return f.Parse(name)==LSOK;
}

//void InitParamFilePreprocess();

int main(int argc, char* argv[])
{
	// initialization of Element library
	//InitParamFilePreprocess();
	//GGameState.Init();

	argv++;
	argc--;
  enum OutMode {Txt,Bin,Xml,Test} outMode = Txt;
	const char *dst = NULL; 
	bool nextDst = false;
	if (argc < 1) goto Usage;
	int ret = 0;
	while (argc>0)
	{
		const char *arg = *argv++;
		argc--;
		if (*arg == '-' || *arg == '/')
		{
			arg++; // skip dash
			if (!stricmp(arg,"bin")) outMode = Bin;
			else if (!stricmp(arg,"txt")) outMode = Txt;
			else if (!stricmp(arg,"xml")) outMode = Xml;
			else if (!stricmp(arg,"test")) outMode = Test;
			else if (!stricmp(arg,"q")) GSilent = true;
			else if (!stricmp(arg,"dst")) nextDst = true;
			else goto Usage;
		}
		else if (nextDst)
		{
			dst = arg;
			nextDst = false;
		}
		else
    {
			ParamFile f;
      // read file in any format
			if (!LoadParamFile(f,arg))
			{
				fprintf(stderr,"Error reading binary file '%s'\n",arg);
				ret = 1;
				continue;
			}
      // save it in format requested by the command line
      if (outMode==Bin)
			{
				f.SaveBin(dst ? dst : arg);
				dst = NULL;
			}
			else if (outMode==Txt)
			{
				f.Save(dst ? dst : arg);
				dst = NULL;
			}
			else if (outMode==Xml)
      {
				SaveXML(f,dst ? dst : arg);
				dst = NULL;
      }
      else if (outMode==Test)
      {
        // do nothing, report errors on load only 
      }
    }

	}
	return ret;

Usage:
	printf("Usage\n");
	printf("   cfgConvert [-bin | -txt | -xml | -q | -test ] {[-dst <destination>] <source>}\n");
	return 1;		
}

/*
#include <Es/Memory/normalNew.hpp>

void *operator new ( size_t size, const char *file, int line )
{
	return malloc(size);
}
*/
