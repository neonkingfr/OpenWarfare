  /*
mod_authz_svn_ldap.cpp
*/

//#define AP_HAVE_DESIGNATED_INITIALIZER

#include "httpd.h"
#include "http_core.h"
#include "http_config.h"
#include "http_protocol.h"
#include "http_request.h"
#include "http_log.h"

#include "apr_strings.h"

#include "util_ldap.h"

#define USE_MOD_DAV_SVN 0

#if USE_MOD_DAV_SVN
// svn_uri_canonicalize
#include "private_uri.h"
// dav_svn_split_uri
#include "mod_dav_svn.h"
// svn_path_join
#include "svn_path.h"
#endif

static const char *CanonicalizePath(const char *dir, apr_pool_t *pool)
{
#if USE_MOD_DAV_SVN
  return dir ? svn_uri_canonicalize(dir, pool) : NULL;
#else
  return dir ? apr_pstrdup(pool, dir) : NULL;
#endif
}

static int SplitURL(request_rec *r, apr_pool_t *pool, const char *uri, const char *basePath, const char **repos_path)
{
#if USE_MOD_DAV_SVN
  const char *cleaned_uri;   // not used
  int trailing_slash;        // not used
  const char *repos_name;    // not used
  const char *relative_path; // not used
  dav_error *dav_err = dav_svn_split_uri(r, uri, basePath,
    &cleaned_uri, &trailing_slash, &repos_name, &relative_path, repos_path);
  if (dav_err)
  {
    ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r,
      "%s  [%d, #%d]", dav_err->desc, dav_err->status, dav_err->error_id);
    /* Ensure that we never allow access by dav_err->status */
    return (dav_err->status != OK && dav_err->status != DECLINED) ? dav_err->status : HTTP_INTERNAL_SERVER_ERROR;
  }
  if (repos_path) repos_path = svn_path_join("/", repos_path, pool);
  return OK;
#else
  if (uri == NULL)
  {
    *repos_path = NULL;
    return OK;
  }
  if (basePath == NULL)
  {
    *repos_path = uri;
    return OK;
  }

  int prefixLen = strlen(basePath);
  if (strncmp(uri, basePath, prefixLen) != 0)
  {
    // this path not belong to us
    *repos_path = NULL;
    return OK;
  }

  if (strlen(uri) <= prefixLen)
  {
    *repos_path = "/";
    return OK;
  }
  else if (uri[prefixLen] == '/')
  {
    *repos_path = apr_pstrdup(pool, uri + prefixLen);
    return OK;
  }
  else
  {
    *repos_path = apr_pstrcat(pool, "/", uri + prefixLen, NULL);
    return OK;
  }
#endif
};

#if !APR_HAS_LDAP
#error mod_authz_svn_ldap requires APR-util to have LDAP support built in. To fix add --with-ldap to ./configure.
#endif

// Common C++ object registered to pool for cleanup
class ObjectInPool
{
protected:
  // never create
  ObjectInPool() {}

public:
  // always destroy the correct class
  virtual ~ObjectInPool()
  {
  }

protected:
  // pool registration
  static apr_status_t DeleteInstance(void *ptr)
  {
    delete (ObjectInPool *)ptr;
    return OK;
  }
  static void *RegisterInstance(apr_pool_t *pool, ObjectInPool *instance)
  {
    apr_pool_cleanup_register(pool, instance, &DeleteInstance, apr_pool_cleanup_null);
    return instance;
  }
};

class LocalPool
{
private:
  apr_pool_t *_pool;

public:
  LocalPool()
  {
    if (apr_pool_create(&_pool, NULL) != APR_SUCCESS) _pool = NULL;
  }
  ~LocalPool()
  {
    if (_pool) apr_pool_destroy(_pool);
  }
  operator apr_pool_t *() {return _pool;}
};

// Factory macros
#define DECLARE_CMD(type, name, func, description) \
  const char *func(cmd_parms *cmd, const char *arg);

#define REDIRECT_CMD(type, name, func, description) \
  static const char *func##Handler(cmd_parms *cmd, void *cfg, const char *arg) \
{ \
  return static_cast<type *>(cfg)->func(cmd, arg); \
}

#define REGISTER_CMD(type, name, func, description) \
  AP_INIT_TAKE1(#name, (cmd_func)&type::func##Handler, NULL, OR_AUTHCFG, description),



// Optional functions from mod_ldap
static APR_OPTIONAL_FN_TYPE(uldap_connection_open) *util_ldap_connection_open;
static APR_OPTIONAL_FN_TYPE(uldap_connection_close) *util_ldap_connection_close;
static APR_OPTIONAL_FN_TYPE(uldap_connection_unbind) *util_ldap_connection_unbind;
static APR_OPTIONAL_FN_TYPE(uldap_connection_find) *util_ldap_connection_find;
static APR_OPTIONAL_FN_TYPE(uldap_cache_comparedn) *util_ldap_cache_comparedn;
static APR_OPTIONAL_FN_TYPE(uldap_cache_compare) *util_ldap_cache_compare;
static APR_OPTIONAL_FN_TYPE(uldap_cache_checkuserid) *util_ldap_cache_checkuserid;
static APR_OPTIONAL_FN_TYPE(uldap_cache_getuserdn) *util_ldap_cache_getuserdn;
static APR_OPTIONAL_FN_TYPE(uldap_ssl_supported) *util_ldap_ssl_supported;

// List of commands
#define COMMANDS(type, XX) \
  XX(type, AuthSvnLDAPURL, ParseURL, "URL to define LDAP connection of the form ldap://host[:port]/basedn[?attrib[?scope[?filter]]] or ldaps://host[:port]/basedn[?attrib[?scope[?filter]]].") \
  XX(type, AuthSvnLDAPGroup, ParseGroup, "LDAP query to find groups of the form ldap://host[:port]/basedn[?attrib[?scope[?filter]]].") \
  XX(type, AuthSvnLDAPMode, SetMode, "LDAP connection mode. Can be one of the values NONE, SSL, or TLS/STARTTLS") \
  XX(type, AuthSvnLDAPBindDN, SetBindDN, "DN to use to bind to LDAP server. If not provided, will do an anonymous bind.") \
  XX(type, AuthSvnLDAPBindPassword, SetBindPassword, "Password to use to bind to LDAP server. If not provided, will do an anonymous bind.") \
  XX(type, AuthSvnLDAPDereferenceAliases, SetDeref, "Determines how aliases are handled during a search. Can be one of the values \"never\", \"searching\", \"finding\", or \"always\". Defaults to always.") \

// Access definition table
class AccessTable : public ObjectInPool
{
private:
  enum AccessType
  {
    AccessNone,
    AccessRead,
    AccessWrite
  };
  struct AccessDesc
  {
    const char *_path;
    AccessType _access;
  };

  apr_hash_t *_groups;
  apr_hash_t *_permissions;

  // always create instance using CreateInstance
  AccessTable()
  {
  }

public:
  static void *CreateInstance(apr_pool_t *pool)
  {
    return RegisterInstance(pool, new AccessTable());
  }

  int LoadGroups(apr_pool_t *pool, util_ldap_connection_t *ldc, const char *baseDN, const char *attribute, int scope, const char *extFilter, request_rec *r);
  int LoadPermissions(apr_pool_t *pool, util_ldap_connection_t *ldc, const char *tableDN, request_rec *r);
  int CheckAccess(const char *path, const char *userDN, bool write, bool recursive, request_rec *r);

  void Diag(apr_pool_t *pool, request_rec *r);

private:
  void SetPermission(const char *userDN, const char *path, AccessType access, apr_pool_t *pool, request_rec *r);
  AccessType GetPermission(const char *userDN, const char *path, bool recursive, request_rec *r);
};

void AccessTable::Diag(apr_pool_t *pool, request_rec *r)
{
  for (apr_hash_index_t *i=apr_hash_first(pool,_groups); i!=NULL; i=apr_hash_next(i))
  {
    const char *userDN;
    apr_array_header_t *array;
    apr_hash_this(i, (const void **)&userDN, NULL, (void **)&array);

    int n = array->nelts;
    const char **data = (const char **)array->elts;
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
      "AccessTable: user %s is member of %d groups:", userDN, n);
    for (int j=0; j<n; j++)
    {
      ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
        " - %s", data[j]);
    }
  }
  for (apr_hash_index_t *i=apr_hash_first(NULL,_permissions); i; i=apr_hash_next(i))
  {
    const char *userDN;
    apr_array_header_t *array;
    apr_hash_this(i, (const void **)&userDN, NULL, (void **)&array);

    int n = array->nelts;
    AccessDesc *data = (AccessDesc *)array->elts;
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
      "AccessTable: permissions for %s (%d directories):", userDN, n);
    for (int j=0; j<n; j++)
    {
      AccessDesc &desc = data[j];
      ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
        " - %s level %d", desc._path, desc._access);
    }
  }
}

int AccessTable::LoadGroups(apr_pool_t *pool, util_ldap_connection_t *ldc, const char *baseDN, const char *attribute, int scope, const char *extFilter, request_rec *r)
{
  _groups = apr_hash_make(pool);

  int result = LDAP_SUCCESS;
  char *attributes[] = {(char *)attribute, NULL};
  char filter[MAX_STRING_LEN];
  apr_snprintf(filter, MAX_STRING_LEN, "(%s)", extFilter);

  // try 10x when server is down
  for (int i=0; i<10; i++)
  {
    result = util_ldap_connection_open(r, ldc);
    if (result != LDAP_SUCCESS) return result;

    // try to search the list of groups
    LDAPMessage *msg;
    result = ldap_search_ext_s(ldc->ldap, baseDN, scope,
                               filter, attributes, 0,
                               NULL, NULL, NULL, APR_LDAP_SIZELIMIT, &msg);
    if (AP_LDAP_IS_SERVER_DOWN(result))
    {
      // server is down, restart the connection
      util_ldap_connection_unbind(ldc);
      continue;
    }
    if (result != LDAP_SUCCESS) 
    {
      // search failed
      break;
    }
    // process the results now
    for (LDAPMessage *entry=ldap_first_entry(ldc->ldap,msg); entry!=NULL; entry=ldap_next_entry(ldc->ldap,entry))
    {
      char *groupDN = ldap_get_dn(ldc->ldap, entry);
      char **values = ldap_get_values(ldc->ldap, entry, attribute);
      if (values)
      {
        for (char **userDN=values; *userDN; userDN++)
        {
/*
          ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
            "AccessTable: %s is member of %s",
            *userDN, groupDN);
*/
          // hash userDN, groupDN
          apr_array_header_t *array = (apr_array_header_t *)apr_hash_get(_groups, *userDN, APR_HASH_KEY_STRING);
          if (array == NULL)
          {
            // the first group for this user
            array = apr_array_make(pool, 8, sizeof(char *));
            apr_hash_set(_groups, apr_pstrdup(pool, *userDN), APR_HASH_KEY_STRING, array);
          }
          char **item = (char **)apr_array_push(array);
          *item = apr_pstrdup(pool, groupDN);
        }
        ldap_value_free(values);
      }
      ldap_memfree(groupDN);
    }
    ldap_msgfree(msg);

    break; // done now
  }
  return result;
}

void AccessTable::SetPermission(const char *userDN, const char *path, AccessType access, apr_pool_t *pool, request_rec *r)
{
  // find / create array for the user
  apr_array_header_t *array = (apr_array_header_t *)apr_hash_get(_permissions, userDN, APR_HASH_KEY_STRING);
  if (array == NULL)
  {
    // the first group for this user
    array = apr_array_make(pool, 8, sizeof(AccessDesc));
    apr_hash_set(_permissions, apr_pstrdup(pool, userDN), APR_HASH_KEY_STRING, array);
  }

  // check if entry for the path exists already
  int n = array->nelts;
  AccessDesc *data = (AccessDesc *)array->elts;

  int index = n; // where to insert the new item
  for (int i=0; i<n; i++)
  {
    AccessDesc &desc = data[i];
    int diff = strcmp(path, desc._path);
    if (diff == 0)
    {
      // found, do not change (current value has higher priority)
      return;
    }
    if (diff > 0)
    {
      index = i;
      break;
    }
  }

  // insert the value to array[index]
  void *ret = apr_array_push(array);
  data = (AccessDesc *)array->elts; // data could be re-allocated
  for (int i=n; i>index; i--) data[i] = data[i - 1];
  AccessDesc &desc = data[index];
  desc._path = path; // already allocated in the pool (need not duplicate)
  desc._access = access;
}

int AccessTable::LoadPermissions(apr_pool_t *pool, util_ldap_connection_t *ldc, const char *tableDN, request_rec *r)
{
  _permissions = apr_hash_make(pool);

  // try 10x when server is down
  int result = LDAP_SUCCESS;
  static const char *attributes[] = {"path", "accessWrite", "accessRead", "accessNone", NULL};
  for (int i=0; i<10; i++)
  {
    result = util_ldap_connection_open(r, ldc);
    if (result != LDAP_SUCCESS) return result;

    // try to search the permissions
    LDAPMessage *msg;
    result = ldap_search_ext_s(ldc->ldap, tableDN, LDAP_SCOPE_SUBTREE,
                               "(objectclass=*)", (char **)attributes, 0,
                               NULL, NULL, NULL, APR_LDAP_SIZELIMIT, &msg);
    if (AP_LDAP_IS_SERVER_DOWN(result))
    {
      // server is down, restart the connection
      util_ldap_connection_unbind(ldc);
      continue;
    }
    if (result != LDAP_SUCCESS) 
    {
      // search failed
      break;
    }
    // process the results now
    for (LDAPMessage *entry=ldap_first_entry(ldc->ldap,msg); entry!=NULL; entry=ldap_next_entry(ldc->ldap,entry))
    {
      char *path = NULL;
      char **values = ldap_get_values(ldc->ldap, entry, "path");
      if (values)
      {
        if (*values != NULL) path = apr_pstrdup(pool, *values);
        ldap_value_free(values);
      }
      if (path == NULL) continue; // path required

      values = ldap_get_values(ldc->ldap, entry, "accessWrite");
      if (values)
      {
        for (char **userDN=values; *userDN; userDN++)
        {
/*
          ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
            "AccessTable: Write access to %s for %s",
            path, *userDN);
*/
          // hash userDN, path, "write"
          SetPermission(*userDN, path, AccessWrite, pool, r);
        }
        ldap_value_free(values);
      }

      values = ldap_get_values(ldc->ldap, entry, "accessRead");
      if (values)
      {
        for (char **userDN=values; *userDN; userDN++)
        {
/*
          ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
            "AccessTable: Read access to %s for %s",
            path, *userDN);
*/
          // hash userDN, path, "read"
          SetPermission(*userDN, path, AccessRead, pool, r);
        }
        ldap_value_free(values);
      }

      values = ldap_get_values(ldc->ldap, entry, "accessNone");
      if (values)
      {
        for (char **userDN=values; *userDN; userDN++)
        {
/*
          ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
            "AccessTable: No access to %s for %s",
            path, *userDN);
*/
          // hash userDN, path, "none"
          SetPermission(*userDN, path, AccessNone, pool, r);
        }
        ldap_value_free(values);
      }
    }
    ldap_msgfree(msg);

    break; // done now
  }
  return result;
}

AccessTable::AccessType AccessTable::GetPermission(const char *userDN, const char *path, bool recursive, request_rec *r)
{
  apr_array_header_t *array = (apr_array_header_t *)apr_hash_get(_permissions, userDN, APR_HASH_KEY_STRING);
  if (array == NULL) return AccessNone;

  int n = array->nelts;
  AccessDesc *data = (AccessDesc *)array->elts;
  if (path == NULL)
  {
    // check if anything is accessible
    AccessType bestAccess = AccessNone;
    for (int i=0; i<n; i++)
    {
      AccessDesc &desc = data[i];
      if (desc._access > bestAccess)
      {
        bestAccess = desc._access;
        if (bestAccess == AccessWrite) break; // will not be better
      }
    }
    return bestAccess;
  }
  else
  {
    AccessType worseAccess = AccessWrite;
    // array is sorted, find the first parent
    for (int i=0; i<n; i++)
    {
      AccessDesc &desc = data[i];
      if (strncmp(path, desc._path, strlen(desc._path)) == 0)
      {
        if (recursive && worseAccess < desc._access) return worseAccess;
        return desc._access;
      }
      if (recursive && desc._access < worseAccess && strncmp(path, desc._path, strlen(path)) == 0)
      {
        // restricted access for the subdirectory
        worseAccess = desc._access;
      }
    }
    return AccessNone; // when no access granted
  }
}

int AccessTable::CheckAccess(const char *path, const char *user, bool write, bool recursive, request_rec *r)
{
  ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
    "AccessTable: Checking access %s for %s %s%s", path, user, write ? "W" : "", recursive ? "R" : "");

  // check if granted for the user directly
  AccessType access = GetPermission(user, path, recursive, r);
  if (access == AccessWrite)
  {
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
      "AccessTable: Access granted for %s", user);
    return OK;
  }
  if (access == AccessRead && !write)
  {
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
      "AccessTable: Access granted for %s", user);
    return OK;
  }

  apr_array_header_t *array = (apr_array_header_t *)apr_hash_get(_groups, user, APR_HASH_KEY_STRING);
  if (array != NULL)
  {
    int n = array->nelts;
    const char **data = (const char **)array->elts;
    for (int i=0; i<n; i++)
    {
      const char *group = data[i];
      access = GetPermission(group, path, recursive, r);
      if (access == AccessWrite)
      {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
          "AccessTable: Access granted for %s", group);
        return OK;
      }
      if (access == AccessRead && !write)
      {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
          "AccessTable: Access granted for %s", group);
        return OK;
      }
    }
  }

  ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
    "AccessTable: Access NOT granted");
  return DECLINED;
}

// Module object declaration
class AuthzSvnLDAP : public ObjectInPool
{
private:
  const char *_url;       /* String representation of the URL */
  const char *_host;      /* Name of the LDAP server */
  int _port;              /* Port of the LDAP server */
  const char *_basedn;    /* Base DN for user DN search */
  const char *_attribute; /* Attribute to DN search for */
  int _scope;             /* Scope of the DN search */
  const char *_filter;    /* Filter to further limit the DN search  */
  const char *_groupBaseDN;    /* Base DN for group search */
  const char *_groupAttribute; /* Attribute to group search for */
  int _groupScope;             /* Scope of the group search */
  const char *_groupFilter;    /* Filter to further limit the group search  */
  const char *_binddn;    /* DN to bind to server (can be NULL) */
  const char *_bindpw;    /* Password to bind to server (can be NULL) */
  deref_options _deref;   /* how to handle alias dereferencing */
  int _secure;            /* True if SSL connections are requested */

  const char *_basePath;  // root of the repository

  // always create instance using CreateInstance
  AuthzSvnLDAP(const char *root)
  {
    _url = NULL;
    _host = NULL;
    _port = 0;
    _binddn = NULL;
    _bindpw = NULL;
    _deref = always;
    _secure = -1;   /* Initialize to unset */

    // default values for the group search
    _groupBaseDN = NULL;
    _groupAttribute = "member";
    _groupScope = LDAP_SCOPE_SUBTREE;
    _groupFilter = "objectclass=groupOfNames";

    _basePath = root;
  }

  // declaration of hooks and commands
  int AuthChecker(request_rec *r);
  COMMANDS(AuthzSvnLDAP, DECLARE_CMD)

public:
  // module instanciation
  static void *CreateInstance(apr_pool_t *pool, char *dir)
  {
    const char *root = CanonicalizePath(dir, pool);
    return RegisterInstance(pool, new AuthzSvnLDAP(root));
  }

  // hooks registration
  static int AuthCheckerHandler(request_rec *r) { return GetInstance(r)->AuthChecker(r); }
  static void ImportULDAPOptFn()
  {
    util_ldap_connection_open   = APR_RETRIEVE_OPTIONAL_FN(uldap_connection_open);
    util_ldap_connection_close  = APR_RETRIEVE_OPTIONAL_FN(uldap_connection_close);
    util_ldap_connection_unbind = APR_RETRIEVE_OPTIONAL_FN(uldap_connection_unbind);
    util_ldap_connection_find   = APR_RETRIEVE_OPTIONAL_FN(uldap_connection_find);
    util_ldap_cache_comparedn   = APR_RETRIEVE_OPTIONAL_FN(uldap_cache_comparedn);
    util_ldap_cache_compare     = APR_RETRIEVE_OPTIONAL_FN(uldap_cache_compare);
    util_ldap_cache_checkuserid = APR_RETRIEVE_OPTIONAL_FN(uldap_cache_checkuserid);
    util_ldap_cache_getuserdn   = APR_RETRIEVE_OPTIONAL_FN(uldap_cache_getuserdn);
    util_ldap_ssl_supported     = APR_RETRIEVE_OPTIONAL_FN(uldap_ssl_supported);
  }
  static int PostConfig(apr_pool_t *p, apr_pool_t *plog, apr_pool_t *ptemp, server_rec *s)
  {
    /* make sure that mod_ldap (util_ldap) is loaded */
    if (ap_find_linked_module("util_ldap.c") == NULL)
    {
      ap_log_error(APLOG_MARK, APLOG_ERR, 0, s,
        "Module mod_ldap missing. Mod_ldap (aka. util_ldap) must be loaded in order for mod_auth_ldap to function properly");
      return HTTP_INTERNAL_SERVER_ERROR;
    }

    return OK;
  }

  static void RegisterHooks(apr_pool_t *pool)
  {
    ap_hook_post_config(PostConfig, NULL, NULL, APR_HOOK_MIDDLE);
    static const char *const these_after_me[] = {"mod_authz_user.c", NULL};           /* list of modules that we want to be called after us */
    ap_hook_auth_checker(AuthCheckerHandler, NULL, these_after_me, APR_HOOK_MIDDLE);	/* register authorization handler */
    ap_hook_optional_fn_retrieve(ImportULDAPOptFn, NULL, NULL, APR_HOOK_MIDDLE);
  }

  // redirection of commands
  COMMANDS(AuthzSvnLDAP, REDIRECT_CMD)
    static const command_rec commands[];

private:
  // helper to find class instance in the request
  static AuthzSvnLDAP *GetInstance(request_rec *r);
  static apr_status_t CleanupConnectionClose(void *param)
  {
    util_ldap_connection_t *ldc = (util_ldap_connection_t *)param;
    util_ldap_connection_close(ldc);
    return APR_SUCCESS;
  }

  // implementation
  int AuthByTable(request_rec *r, const char *tableDN);
  const char *GetUserDN(apr_pool_t *pool, util_ldap_connection_t *ldc, request_rec *r);
};

// Commands implementation

const char *AuthzSvnLDAP::ParseURL(cmd_parms *cmd, const char *arg)
{
  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, cmd->server,
    "ParseURL: %s", arg);

  _url = arg;

  // parse URL
  apr_ldap_url_desc_t *url;
  apr_ldap_err_t *result;
  int rc = apr_ldap_url_parse(cmd->pool, arg, &url, &result);
  if (rc != APR_SUCCESS) return result->reason;

  /* Set all the values, or at least some sane defaults */
  _host = url->lud_host ? apr_pstrdup(cmd->pool, url->lud_host) : "localhost";

  if (strncasecmp(arg, "ldaps", 5) == 0)
  {
    _secure = APR_LDAP_SSL;
    _port = url->lud_port ? url->lud_port : LDAPS_PORT;
    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, cmd->server, "LDAP: auth_ldap using SSL connections");
  }
  else
  {
    _port = url->lud_port? url->lud_port : LDAP_PORT;
    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, cmd->server, "LDAP: auth_ldap not using SSL connections");
  }

  _basedn = url->lud_dn ? apr_pstrdup(cmd->pool, url->lud_dn) : "";
  if (_groupBaseDN == NULL) _groupBaseDN = _basedn; // by default, use base DN for all searches

  if (url->lud_attrs && url->lud_attrs[0])
    _attribute = apr_pstrdup(cmd->pool, url->lud_attrs[0]);
  else
    _attribute = "uid";

  _scope = url->lud_scope == LDAP_SCOPE_ONELEVEL ? LDAP_SCOPE_ONELEVEL : LDAP_SCOPE_SUBTREE;

  if (url->lud_filter)
  {
    if (url->lud_filter[0] == '(') 
    {
      /*
      * Get rid of the surrounding parens; later on when generating the
      * filter, they'll be put back.
      */
      _filter = apr_pstrdup(cmd->pool, url->lud_filter + 1);
      ((char *)_filter)[strlen(_filter) - 1] = '\0';
    }
    else _filter = apr_pstrdup(cmd->pool, url->lud_filter);
  }
  else _filter = "objectclass=*";

  return NULL;
}

const char *AuthzSvnLDAP::ParseGroup(cmd_parms *cmd, const char *arg)
{
  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, cmd->server,
    "ParseGroup: %s", arg);

  // parse URL
  apr_ldap_url_desc_t *url;
  apr_ldap_err_t *result;
  int rc = apr_ldap_url_parse(cmd->pool, arg, &url, &result);
  if (rc != APR_SUCCESS) return result->reason;

  /* Set all the values, or at least some sane defaults */
  _groupBaseDN = url->lud_dn ? apr_pstrdup(cmd->pool, url->lud_dn) : "";

  if (url->lud_attrs && url->lud_attrs[0])
    _groupAttribute = apr_pstrdup(cmd->pool, url->lud_attrs[0]);
  else
    _groupAttribute = "member";

  _groupScope = url->lud_scope == LDAP_SCOPE_ONELEVEL ? LDAP_SCOPE_ONELEVEL : LDAP_SCOPE_SUBTREE;

  if (url->lud_filter)
  {
    if (url->lud_filter[0] == '(') 
    {
      /*
      * Get rid of the surrounding parens; later on when generating the
      * filter, they'll be put back.
      */
      _groupFilter = apr_pstrdup(cmd->pool, url->lud_filter + 1);
      ((char *)_groupFilter)[strlen(_filter) - 1] = '\0';
    }
    else _groupFilter = apr_pstrdup(cmd->pool, url->lud_filter);
  }
  else _groupFilter = "objectclass=groupOfNames";

  return NULL;
}

const char *AuthzSvnLDAP::SetMode(cmd_parms *cmd, const char *arg)
{
  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, cmd->server,
    "SetMode: %s", arg);

  if (_secure != -1) return NULL; // already set by ParseURL

  if (0 == strcasecmp("NONE", arg))
    _secure = APR_LDAP_NONE;
  else if (0 == strcasecmp("SSL", arg))
    _secure = APR_LDAP_SSL;
  else if (0 == strcasecmp("TLS", arg) || 0 == strcasecmp("STARTTLS", arg))
    _secure = APR_LDAP_STARTTLS;
  else
    return "Invalid LDAP connection mode setting: must be one of NONE, SSL, or TLS/STARTTLS";

  return NULL;
}

const char *AuthzSvnLDAP::SetDeref(cmd_parms *cmd, const char *arg)
{
  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, cmd->server,
    "SetDeref: %s", arg);

  if (strcmp(arg, "never") == 0 || strcasecmp(arg, "off") == 0)
    _deref = never;
  else if (strcmp(arg, "searching") == 0)
    _deref = searching;
  else if (strcmp(arg, "finding") == 0)
    _deref = finding;
  else if (strcmp(arg, "always") == 0 || strcasecmp(arg, "on") == 0)
    _deref = always;
  else
    return "Unrecognized value for AuthSvnLDAPAliasDereference directive";
  return NULL;
}

const char *AuthzSvnLDAP::SetBindDN(cmd_parms *cmd, const char *arg)
{
  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, cmd->server,
    "SetBindDN: %s", arg);

  _binddn = apr_pstrdup(cmd->pool, arg);
  return NULL;
}

const char *AuthzSvnLDAP::SetBindPassword(cmd_parms *cmd, const char *arg)
{
  ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, cmd->server,
    "SetBindPassword: %s", arg);

  _bindpw = apr_pstrdup(cmd->pool, arg);
  return NULL;
}

int AuthzSvnLDAP::AuthByTable(request_rec *r, const char *tableDN)
{
  // basic URL check
  const char *canonicalized_uri = CanonicalizePath(r->uri, r->pool);
  if (strcmp(canonicalized_uri, _basePath) == 0)
  {
    /* Do no access control when conf->base_path(as configured in <Location>)
    * and given uri are same. The reason for such relaxation of access
    * control is "This module is meant to control access inside the
    * repository path, in this case inside PATH is empty and hence
    * dav_svn_split_uri fails saying no repository name present".
    * One may ask it will allow access to '/' inside the repository if
    * repository is served via SVNPath instead of SVNParentPath.
    * It does not, The other methods(PROPFIND, MKACTIVITY) for
    * accomplishing the operation takes care of making a request to
    * proper URL */
    return OK;
  }

  LocalPool pool;
  if (!(apr_pool_t *)pool) return HTTP_INTERNAL_SERVER_ERROR; // cannot create local pool

  const char *repos_path;    // relative path in the repository
  int dav_err = SplitURL(r, pool, r->uri, _basePath, &repos_path);
  if (dav_err != OK) return dav_err;

  // Ignore the URI passed to MERGE, like mod_dav_svn does.
  if (r->method_number == M_MERGE) repos_path = NULL;

  // decide what to check based on DAV method
  bool writeAccess = false;
  bool recursive = false;
  bool checkDestination = false;
  switch (r->method_number)
  {
  case M_COPY:
    recursive = true;
    checkDestination = true;
    break;
  case M_OPTIONS:
  case M_GET:
  case M_PROPFIND:
  case M_REPORT:
    break;
  case M_MOVE:
    writeAccess = true;
    recursive = true;
    checkDestination = true;
    break;
  case M_DELETE:
    writeAccess = true;
    recursive = true;
    break;
  case M_MKCOL:
  case M_PUT:
  case M_PROPPATCH:
  case M_CHECKOUT:
  case M_MERGE:
  case M_MKACTIVITY:
  case M_LOCK:
  case M_UNLOCK:
    writeAccess = true;
    break;
  default:
    // Require most strict access for unknown methods
    writeAccess = true;
    recursive = true;
    break;
  }

  // read access with no URL is always granted
  if (!repos_path && !writeAccess) return OK;

  util_ldap_connection_t *ldc = util_ldap_connection_find(r, _host, _port, _binddn, _bindpw, _deref, _secure);
  apr_pool_cleanup_register(pool, ldc, CleanupConnectionClose, apr_pool_cleanup_null);
  const char *userDN = GetUserDN(pool, ldc, r);
  if (!userDN) return DECLINED; // unknown user

  // load the decision table from LDAP
  // TODO: caching
  AccessTable *table = (AccessTable *)AccessTable::CreateInstance(pool);

  if (table->LoadGroups(pool, ldc, _groupBaseDN, _groupAttribute, _groupScope, _groupFilter, r) != LDAP_SUCCESS) return HTTP_INTERNAL_SERVER_ERROR; // cannot connect the LDAP server
  if (table->LoadPermissions(pool, ldc, tableDN, r) != LDAP_SUCCESS) return HTTP_INTERNAL_SERVER_ERROR; // cannot connect the LDAP server
  // table->Diag(pool, r);

  int result = table->CheckAccess(repos_path, userDN, writeAccess, recursive, r);
  if (result != OK) return result;
  if (!checkDestination) return result;

  // find and preprocess destination URI
  const char *dest_uri = apr_table_get(r->headers_in, "Destination");
  // Decline MOVE or COPY when there is no Destination URI, this will cause failure.
  if (!dest_uri) return DECLINED;
  apr_uri_t parsed_dest_uri;
  apr_uri_parse(r->pool, dest_uri, &parsed_dest_uri);
  ap_unescape_url(parsed_dest_uri.path);
  dest_uri = parsed_dest_uri.path;

  if (strncmp(dest_uri, _basePath, strlen(_basePath)))
  {
    /* If it is not the same location, then we don't allow it.
    * XXX: Instead we could compare repository uuids, but that
    * XXX: seems a bit over the top.
    */
    return HTTP_BAD_REQUEST;
  }

  // re-use the dummy parameters from the previous call
  // TODO: What is dest_uri relative to?
  const char *dest_repos_path;
  dav_err = SplitURL(r, pool, dest_uri, _basePath, &dest_repos_path);
  if (dav_err != OK) return dav_err;

  return table->CheckAccess(dest_repos_path, userDN, true, true, r);
}

const char *AuthzSvnLDAP::GetUserDN(apr_pool_t *pool, util_ldap_connection_t *ldc, request_rec *r)
{
  // Build the filter (TODO: escape some characters in the user name)
  char filter[MAX_STRING_LEN];
  apr_snprintf(filter, MAX_STRING_LEN, "(&(%s)(%s=%s))", _filter, _attribute, r->user);

  /* Search for the user DN */
  const char *dn = NULL;
  const char **vals = NULL;
  char *attributes[] = {(char *)_attribute, NULL};
  int result = util_ldap_cache_getuserdn(r, ldc, _url, _basedn, _scope, attributes, filter, &dn, &vals);
  if (result != LDAP_SUCCESS)
  {
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
      "GetUserDN: User DN not found, %s", ldc->reason);
    return NULL;
  }
  return apr_pstrdup(pool, dn);
}

// Hooks implementation
int AuthzSvnLDAP::AuthChecker(request_rec *r)
{
  ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r,
    "AuthCheck: protocol: %s hostname: %s method: %s uri: %s user: %s",
    r->protocol, r->hostname, r->method, r->uri, r->user);

  if (!_host)
  {
    // not configured to launch for this request
    return DECLINED;
  }
  if (!r->user || strlen(r->user) == 0)
  {
    // anonymous access impossible through this module
    return DECLINED;
  }

  const apr_array_header_t *requires = ap_requires(r);
  if (requires)
  {
    const require_line *reqlines = (require_line *)requires->elts;
    for (int i=0; i<requires->nelts; i++)
    {
      const require_line &require = reqlines[i];
      // skip if Limit does not apply for this method
      if (!(require.method_mask & (AP_METHOD_BIT << r->method_number))) continue;

      const char *t = require.requirement;
      const char *w = ap_getword_white(r->pool, &t);
      if (strcmp(w, "ldap-table") == 0) 
      {
        // this is what we want to handle
        int result = AuthByTable(r, t);
        // when access was declined, request is not authorized (do not pass it further)
        if (result == DECLINED)
        {
          ap_note_auth_failure(r);
          return HTTP_UNAUTHORIZED;
        }
        return result;
      }
    }
  }

  return DECLINED;
}

// Module registration

const command_rec AuthzSvnLDAP::commands[] = 
{
  COMMANDS(AuthzSvnLDAP, REGISTER_CMD)
  {NULL}
};

module AP_MODULE_DECLARE_DATA mod_authz_svn_ldap_module = 
{
  STANDARD20_MODULE_STUFF,
  &AuthzSvnLDAP::CreateInstance,       /* create per-dir    config structures */
  NULL,                                /* merge  per-dir    config structures */
  NULL,                                /* create per-server config structures */
  NULL,                                /* merge  per-server config structures */
  AuthzSvnLDAP::commands,              /* table of config file commands       */
  &AuthzSvnLDAP::RegisterHooks         /* register hooks                      */
};

AuthzSvnLDAP *AuthzSvnLDAP::GetInstance(request_rec *r)
{
  return static_cast<AuthzSvnLDAP *>(ap_get_module_config(r->per_dir_config, &mod_authz_svn_ldap_module));
}
