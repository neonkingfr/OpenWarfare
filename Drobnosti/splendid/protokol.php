<?php
require 'config.php';
$sql = InitDatabase($db_host,$db_username,$db_password,$db_database);


class Parser {
    
    private $handle = NULL;
    private $callback = NULL;
    
    private $have_next = FALSE;
    private $next_char = 'NEDEFINOVANO';
    
    private $current_protocol = '';
    private $current_sentence = '';
    private $current_variant = 0;
    
    function __construct($fh, $cb) {
        $this->handle = $fh;
        $this->callback = $cb;
        $this->have_next = FALSE;
    }
    
    private function get() {
        if ($this->have_next) {
            $this->have_next = FALSE;
            return $this->next_char;
        } else {
            $ch = fgetc($this->handle);
            if ($ch === FALSE) {
                throw new Exception('Unexpected end of data');
            }
            return $ch;
        }
    }
    
    private function putback($ch) {
        if ($this->have_next) throw new Exception('No space to put back character '.$ch);
        $this->next_char = $ch;
        $this->have_next = TRUE;
    }
    
    
    private function nextCharMustBe($mustbe) {
        $ch = $this->get();
        if ($ch != $mustbe) throw new Exception('Expected "'.$mustbe.'" here, got "'.$ch.'"');
    }
    
    private function parseArray($parse_element_callback) {
        $this->nextCharMustBe('[');

        if (($ch = $this->get()) != ']') {    // special case for empty array []
            $this->putback($ch);
            
            $this->$parse_element_callback(); // first element
            
            while (($ch = $this->get()) != ']') {
                $this->putback($ch);
                $this->nextCharMustBe(',');
                $this->$parse_element_callback();
            }
        }
    }
    
    private function parseTuple($parse_key_callback,$parse_value_callback) {
        $this->nextCharMustBe('[');
        $this->$parse_key_callback();
        $this->nextCharMustBe(',');
        $this->$parse_value_callback();
        $this->nextCharMustBe(']');
    }
    
    private function parseString() {
        $this->nextCharMustBe('"');
        $str = '';
        while (($ch = $this->get()) != '"') {
            $str = $str.$ch;
        }
        return $str;
    }
    
    private function parseInteger() {
        $sign = 1;
        $value = 0;
        $ch = $this->get();
        if ($ch == '-') {
            $sign = -1;
            $ch = $this->get();
        }
        while (($ch >='0')&&($ch<='9')) {
            $value = 10*$value + ($ch-'0');
            $ch = $this->get();
        }
        $this->putback($ch);
        
        $result = $sign*$value;
        return $result;
    }
    
    //  [protocol, [sentence, [variants] ]], protocol = string, sentence = string, variants = array of integers, index = variant & value = variant
    public function parse() { // callback args: $protocol $sentence $variant $count
        $this->parseArray('parseProtocol');
    }
    
    private function parseProtocol() {
        $this->parseTuple('parseProtocolName','parseProtocolContent');
    }
    
    private function parseProtocolName() {
        $this->current_protocol = $this->parseString();
    }
    
    private function parseProtocolContent() {
        $this->parseArray('parseSentence');
    }
    
    private function parseSentence() {
        $this->parseTuple('parseSentenceName','parseSentenceContent');
    }
    
    private function parseSentenceName() {
        $this->current_sentence = $this->parseString();
    }
    
    private function parseSentenceContent() {
        $this->current_variant = 0;
        $this->parseArray('parseSentenceVariant');
    }
    
    private function parseSentenceVariant() {
        $count = $this->parseInteger();
        $cb = $this->callback;
        $cb($this->current_protocol, $this->current_sentence, $this->current_variant, $count);
        $this->current_variant++;
    }
}



/*----------- main -------------------------------*/



if ($_SERVER['REQUEST_METHOD']=='POST') {
    if ($_SERVER["CONTENT_TYPE"]=='text/plain') {
        $error = NULL;
        try {
            // parse data
            $data = fopen('php://input','r+');
            if (!is_resource($data)) throw new Exception("Cannot read request body");

            $parser = new Parser($data, 'processRecord');
            $parser->parse();
            $sql->commit();
            
            
        } catch (Exception $e) {
            $error = $e->getMessage()."\ntrace:\n";
            foreach ($e->getTrace() as $t) {
                $error=$error.sprintf("%-50s    @ %s\n", $t['function'].'('.implode(',',$t['args']).')', $t['file'].' #'.$t['line']);
            }
        }

        // sezrat zbytek pokud je parse error
        if ($data != FALSE) {
            while (!feof($data)) {
                $char = fgetc($data);
            }
            fclose($data);
        }

        
        if ($error === NULL) {
            header('HTTP/1.1 200 OK');
            header('Content-Type: text/plain; charset=UTF-8');
            print "OK";
        } else {
            header('HTTP/1.1 400 Bad Request');
            header('Content-Type: text/plain; charset=UTF-8');
            print "ERROR: $error\n";
        }        
        
    } else {
        PageError('POST, ale divny Content type: '.$_SERVER['CONTENT_TYPE']);
    }
    
} else {
    header('HTTP/1.1 200 OK');
    header('Content-Type: text/html; charset=UTF-8');
    HTMLHeader('Protocol statistics');
    ?>
    <body>
        <h1>Protocol Statistics</h1>
        
        <div>
            <table>
                <tr><th>Protocol</th><th>Sentence</th><th>Variant</th><th>Use count</th></tr>
<?php
    if (!$sql->query('START TRANSACTION WITH CONSISTENT SNAPSHOT')) {
        die ('Failed to start transaction');
    }
    $result = $sql->query('SELECT protocol,sentence,variant,count FROM stats_protocol ORDER BY protocol ASC,count DESC', MYSQLI_STORE_RESULT);
    
    while ($row = $result->fetch_array()) {
?>
                <tr>
                    <td><?php Text($row[0]); ?></td>
                    <td><?php Text($row[1]); ?></td>
                    <td><?php Text($row[2]); ?></td>
                    <td><?php Text($row[3]); ?></td>
                </tr>
<?php                
    }
    
    $result->close();
    $sql->commit();
    
?>
            </table>
        </div>
        
    </body>
</html>
<?php
}



/*
function PageWoundsSummary() {
    header('HTTP/1.1 200 OK');
    header('Content-Type: text/html; charset=UTF-8');
    HTMLHeader('Wounds summary');
    list($zaznamu, $hlava1,$hlava2,$hlava3,$hlava4,$hlava5, $telo1,$telo2,$telo3,$telo4,$telo5, $ruce1,$ruce2,$ruce3,$ruce4,$ruce5, $nohy1,$nohy2,$nohy3,$nohy4,$nohy5, $smrt) = GetWoundsSummary(); // zaznamu, hlava[5], telo[5], ruce[5], nohy[5], smrt
?>
    <body>
        <h1>Wounds summary</h1>
        <p>
            <table class="wounds-summary">
                <tr><th></th><th>0-25%</th><th>25-50%</th><th>50-75%</th><th>75-100%</th><th>&gt;100%</th></tr>
                <tr><th>Head:</th><td><?php print $hlava1 ?></td><td><?php print $hlava2 ?></td><td><?php print $hlava3 ?></td><td><?php print $hlava4 ?></td><td><?php print $hlava5 ?></td></tr>
                <tr><th>Body:</th><td><?php print $telo1 ?></td><td><?php print $telo2 ?></td><td><?php print $telo3 ?></td><td><?php print $telo4 ?></td><td><?php print $telo5 ?></td></tr>
                <tr><th>Hands:</th><td><?php print $ruce1 ?></td><td><?php print $ruce2 ?></td><td><?php print $ruce3 ?></td><td><?php print $ruce4 ?></td><td><?php print $ruce5 ?></td></tr>
                <tr><th>Legs:</th><td><?php print $nohy1 ?></td><td><?php print $nohy2 ?></td><td><?php print $nohy3 ?></td><td><?php print $nohy4 ?></td><td><?php print $nohy5 ?></td></tr>
                <tr><th>Deaths:</th><td colspan="5"><?php print $smrt ?></td></tr>
                <tr><th>Records:</th><td colspan="5"><?php print $zaznamu ?></td></tr>
            </table>
        </p>
    </body>
</html>
<?php
}
*/

function Text($text) {
    print htmlentities($text);
}

/*
function PageUpdateWounds() {
    $mise = $_REQUEST['mise'];
    $hrac = $_REQUEST['hrac'];
    $data = $_REQUEST['data'];
    $pole = split(',', $data);
    $hlava1 = intval($pole[0]);
    $hlava2 = intval($pole[1]);
    $hlava3 = intval($pole[2]);
    $hlava4 = intval($pole[3]);
    $hlava5 = intval($pole[4]);
    $telo1 = intval($pole[5]);
    $telo2 = intval($pole[6]);
    $telo3 = intval($pole[7]);
    $telo4 = intval($pole[8]);
    $telo5 = intval($pole[9]);
    $ruce1 = intval($pole[10]);
    $ruce2 = intval($pole[11]);
    $ruce3 = intval($pole[12]);
    $ruce4 = intval($pole[13]);
    $ruce5 = intval($pole[14]);
    $nohy1 = intval($pole[15]);
    $nohy2 = intval($pole[16]);
    $nohy3 = intval($pole[17]);
    $nohy4 = intval($pole[18]);
    $nohy5 = intval($pole[19]);
    $smrt = intval($pole[20]);
    UpdateWoundsStatistics($mise, $hrac, $hlava1,$hlava2,$hlava3,$hlava4,$hlava5, $telo1,$telo2,$telo3,$telo4,$telo5, $ruce1,$ruce2,$ruce3,$ruce4,$ruce5, $nohy1,$nohy2,$nohy3,$nohy4,$nohy5, $smrt);
    header('HTTP/1.1 200 OK');
    header('Content-Type: text/plain; charset=UTF-8');
    print 'OK';
}
*/



/*
function PageWoundsPerMission() {
    
    header('HTTP/1.1 200 OK');
    header('Content-Type: text/html; charset=UTF-8');
    HTMLHeader('Wounds per mission');
    
    $rows = GetWoundsPerMission();
    
    
    ?>
    <body>
        <h1>Wounds per mission</h1>
        <p>
        <table class="wounds-per-mission">
            <tr>
                <th rowspan="2" style="text-align: center;">Mission</th>
                <th rowspan="2" style="text-align: center;">Records</th>
                <th colspan="5" style="text-align: center;">Head</th>
                <th colspan="5" style="text-align: center;">Body</th>
                <th colspan="5" style="text-align: center;">Hands</th>
                <th colspan="5" style="text-align: center;">Legs</th>
                <th rowspan="2" style="text-align: center;">Deaths</th>
            </tr>
            <tr><th>0-25%</th><th>25-50%</th><th>50-75%</th><th>75-100%</th><th>&gt;100%</th><th>0-25%</th><th>25-50%</th><th>50-75%</th><th>75-100%</th><th>&gt;100%</th><th>0-25%</th><th>25-50%</th><th>50-75%</th><th>75-100%</th><th>&gt;100%</th><th>0-25%</th><th>25-50%</th><th>50-75%</th><th>75-100%</th><th>&gt;100%</th></tr>
            <?php
                foreach ($rows as $row) {
                    // mise, zaznamu, hlava, telo, ruce, nohy, smrt
                    ?>
            <tr>
                <td><?php Text($row[0]) ?></td>
                <td><?php Text($row[1]) ?></td>
                <td><?php Text($row[2]) ?></td>
                <td><?php Text($row[3]) ?></td>
                <td><?php Text($row[4]) ?></td>
                <td><?php Text($row[5]) ?></td>
                <td><?php Text($row[6]) ?></td>
                <td><?php Text($row[7]) ?></td>
                <td><?php Text($row[8]) ?></td>
                <td><?php Text($row[9]) ?></td>
                <td><?php Text($row[10]) ?></td>
                <td><?php Text($row[11]) ?></td>
                <td><?php Text($row[12]) ?></td>
                <td><?php Text($row[13]) ?></td>
                <td><?php Text($row[14]) ?></td>
                <td><?php Text($row[15]) ?></td>
                <td><?php Text($row[16]) ?></td>
                <td><?php Text($row[17]) ?></td>
                <td><?php Text($row[18]) ?></td>
                <td><?php Text($row[19]) ?></td>
                <td><?php Text($row[20]) ?></td>
                <td><?php Text($row[21]) ?></td>
                <td><?php Text($row[22]) ?></td>
            </tr><?php
                
                }
            ?>
        </table>
        </p>
    </body>
</html>
<?php
}

*/


function PageError($proc) {
    header('HTTP/1.1 404 Nelibi');
    header('Content-Type: text/plain; charset=UTF-8');
    print "Tohle se mi nelibi! ".$proc;
}

function MakeLink($html,$url) {
    return "<a href=\"".htmlspecialchars($url)."\">".$html.'</a>';
}

function HTMLLink($html,$url) {
    print MakeLink($html,$url);
}

function ActionLinkURL($action,$params =  Array() ) {
    $l = $_SERVER['PHP_SELF'].'?action='.urlencode($action);
    foreach ($params as $key => $value) {
        $l = $l.'&'.urlencode($key).'='.urlencode($value);
    }
    return $l;
}

function HTMLHeader($title) {
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
    <head>
        <title><?php print htmlspecialchars($title); ?></title>
        <style type="text/css">
            body {
                background-color: aliceblue;
            }            
            
            h1 {
                text-align: center;
            }
            table {
                border-collapse: collapse;
                border: 1px solid black;
                background-color: white;
                margin: auto;
            }
            
            table td {
                text-align: right;
                padding: 2px 5px;
                border: 1px solid #E0E0E0;
            }
            
            table th {
                text-align: right;
                padding: 2px 5px;
                background-color: #FFFFE0;
                border: 1px solid #E0E0E0;
            }
            
        </style>
    </head>
<?php
}

function InitDatabase($host,$username,$password,$database) {
    
    $sql = new mysqli($host,$username,$password,$database);
    if ($sql->connect_errno) {
        die('Failed to connect to database');
    }
    
    $sql->autocommit(FALSE);    /* pouzivam transakce! */
    
    if (!$sql->set_charset('utf8')) {
        die('Failed to set charset');
    }
    
    $sql->query('SET TRANSACTION ISOLATION LEVEL SERIALIZABLE');
    if ($sql->errno) {
        die('Failed to enable transactions');
    }
    
    //$sql->commit();
    return $sql;
}



function processRecord($protokol, $veta, $varianta, $pocet) {
    global $sql;
    if ($pocet > 0) {
    // print 'Protokol: '.$protokol.' Veta: '.$veta.' Varianta: '.$varianta.' Pocet: '.$pocet."\n";
        $s = $sql->prepare('UPDATE stats_protocol SET count=count+? WHERE protocol=? AND sentence=? AND variant=?');
        $s->bind_param('issi', $pocet, $protokol, $veta, $varianta);

        $success = $s->execute();
        $errno = $s->errno;
        $error = $s->error;
        $rows = $s->affected_rows;
        $s->close(); $s = NULL;
        if (!$success) {
            $sql->rollback();
            die("Failed update: $errno, $error");
        }
        if ($rows != 1) {
            $s = $sql->prepare('INSERT INTO stats_protocol (protocol,sentence,variant,count) VALUES (?,?,?,?)');
            $s->bind_param('ssii', $protokol, $veta, $varianta, $pocet);
            $success = $s->execute();
            $s->close(); $s = NULL;
            if (!$success) {
                $sql->rollback();
                die('Failed to insert');
            }
        }
    }
}

/*
function UpdateWoundsStatistics($mise,$hrac,$hlava1,$hlava2,$hlava3,$hlava4,$hlava5,$telo1,$telo2,$telo3,$telo4,$telo5,$ruce1,$ruce2,$ruce3,$ruce4,$ruce5,$nohy1,$nohy2,$nohy3,$nohy4,$nohy5,$smrt) {
    global $sql;
    $s = $sql->prepare('UPDATE stats_zraneni SET hlava1=hlava1+?, hlava2=hlava2+?, hlava3=hlava3+?, hlava4=hlava4+?, hlava5=hlava5+?, telo1=telo1+?, telo2=telo2+?, telo3=telo3+?, telo4=telo4+?, telo5=telo5+?, ruce1 = ruce1+?, ruce2 = ruce2+?, ruce3 = ruce3+?, ruce4 = ruce4+?, ruce5 = ruce5+?, nohy1=nohy1+?, nohy2=nohy2+?, nohy3=nohy3+?, nohy4=nohy4+?, nohy5=nohy5+?, smrt=smrt+? WHERE mise=? AND hrac=?');
    $s->bind_param('iiiiiiiiiiiiiiiiiiiiiss', $hlava1,$hlava2,$hlava3,$hlava4,$hlava5, $telo1,$telo2,$telo3,$telo4,$telo5, $ruce1,$ruce2,$ruce3,$ruce4,$ruce5, $nohy1,$nohy2,$nohy3,$nohy4,$nohy5, $smrt, $mise, $hrac);
    $success = $s->execute();
    $errno = $s->errno;
    $error = $s->error;
    $rows = $s->affected_rows;
    $s->close(); $s = NULL;
    if (!$success) {
        $sql->rollback();
        die("Failed update: $errno, $error");
    }
    
    if ($rows != 1) {
        // record for this mission+player does not exist, insert it
        $s = $sql->prepare('INSERT INTO stats_zraneni (mise,hrac,hlava1,hlava2,hlava3,hlava4,hlava5,telo1,telo2,telo3,telo4,telo5,ruce1,ruce2,ruce3,ruce4,ruce5,nohy1,nohy2,nohy3,nohy4,nohy5,smrt) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $s->bind_param('ssiiiiiiiiiiiiiiiiiiiii', $mise, $hrac, $hlava1,$hlava2,$hlava3,$hlava4,$hlava5,$telo1,$telo2,$telo3,$telo4,$telo5,$ruce1,$ruce2,$ruce3,$ruce4,$ruce5,$nohy1,$nohy2,$nohy3,$nohy4,$nohy5, $smrt);
        $success = $s->execute();
        $s->close(); $s = NULL;
        if (!$success) {
            $sql->rollback();
            die('Failed to insert');
        }
    }
    $sql->commit();
}
*/

/*
function GetWoundsSummary() {
    global $sql;
    if (!$sql->query('START TRANSACTION WITH CONSISTENT SNAPSHOT')) {
        die ('Failed to start transaction');
    }
    $result = $sql->query('SELECT COUNT(*) as zaznamu, '
            .'SUM(hlava1) as hlava1, SUM(hlava2) as hlava2, SUM(hlava3) as hlava3, SUM(hlava4) as hlava4, SUM(hlava5) as hlava5, '
            .'SUM(telo1) as telo1, SUM(telo2) as telo2, SUM(telo3) as telo3, SUM(telo4) as telo4, SUM(telo5) as telo5, '
            .'SUM(ruce1) as ruce1, SUM(ruce2) as ruce2, SUM(ruce3) as ruce3, SUM(ruce4) as ruce4, SUM(ruce5) as ruce5, '
            .'SUM(nohy1) as nohy1, SUM(nohy2) as nohy2, SUM(nohy3) as nohy3, SUM(nohy4) as nohy4, SUM(nohy5) as nohy5, '
            .'SUM(smrt) as smrt FROM stats_zraneni WHERE mise <> \'test\'', MYSQLI_STORE_RESULT);
    $row = $result->fetch_row();
    $result->close();
    $sql->commit();
    return $row;
}

function GetWoundsPerMission() {
    global $sql;
    if (!$sql->query('START TRANSACTION WITH CONSISTENT SNAPSHOT')) {
        die ('Failed to start transaction');
    }
    $result = $sql->query('SELECT mise, COUNT(*) as zaznamu, '
            .'SUM(hlava1) as hlava1, SUM(hlava2) as hlava2, SUM(hlava3) as hlava3, SUM(hlava4) as hlava4, SUM(hlava5) as hlava5, '
            .'SUM(telo1) as telo1, SUM(telo2) as telo2, SUM(telo3) as telo3, SUM(telo4) as telo4, SUM(telo5) as telo5, '
            .'SUM(ruce1) as ruce1, SUM(ruce2) as ruce2, SUM(ruce3) as ruce3, SUM(ruce4) as ruce4, SUM(ruce5) as ruce5, '
            .'SUM(nohy1) as nohy1, SUM(nohy2) as nohy2, SUM(nohy3) as nohy3, SUM(nohy4) as nohy4, SUM(nohy5) as nohy5, '
            .'SUM(smrt) as smrt FROM stats_zraneni GROUP BY mise ORDER BY mise ASC', MYSQLI_STORE_RESULT);
    
    $rows = array();
    
    while ($row = $result->fetch_array()) {
        $rows[] = $row;
    }
    
    $result->close();
    $sql->commit();
    return $rows;
}
*/
?>

    
