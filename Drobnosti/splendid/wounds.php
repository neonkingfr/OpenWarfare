<?php
require 'config.php';
$sql = InitDatabase($db_host,$db_username,$db_password,$db_database);

if ($_SERVER['REQUEST_METHOD']=='POST') {
    if (array_key_exists('action',$_POST)) {
        switch ($_POST['action']) {

            case 'update-wounds':
                PageUpdateWounds();
                break;

            default:
            PageError('POST, neznama action "'.$action.'"');
        }
    } else {
        PageError('POST, ale nemam action. Content type:'.$_SERVER['CONTENT_TYPE']);
    }
    
} else {
    if (array_key_exists('action',$_GET)) {
        switch ($_GET['action']) {

            case 'wounds-summary':
                PageWoundsSummary();
                break;

            case 'wounds-per-mission':
                PageWoundsPerMission();
                break;

            case 'update-wounds':
                PageError('Spatna metoda: '.$_SERVER['REQUEST_METHOD']);
                break;

            default:
            PageDefault();
        }
    } else {
        PageDefault();
    }
}
function PageWoundsSummary() {
    header('HTTP/1.1 200 OK');
    header('Content-Type: text/html; charset=UTF-8');
    HTMLHeader('Wounds summary');
    list($zaznamu, $hlava1,$hlava2,$hlava3,$hlava4,$hlava5, $telo1,$telo2,$telo3,$telo4,$telo5, $ruce1,$ruce2,$ruce3,$ruce4,$ruce5, $nohy1,$nohy2,$nohy3,$nohy4,$nohy5, $smrt) = GetWoundsSummary(); // zaznamu, hlava[5], telo[5], ruce[5], nohy[5], smrt
?>
    <body>
        <h1>Wounds summary</h1>
        <p>
            <table class="wounds-summary">
                <tr><th></th><th>0-25%</th><th>25-50%</th><th>50-75%</th><th>75-100%</th><th>&gt;100%</th></tr>
                <tr><th>Head:</th><td><?php print $hlava1 ?></td><td><?php print $hlava2 ?></td><td><?php print $hlava3 ?></td><td><?php print $hlava4 ?></td><td><?php print $hlava5 ?></td></tr>
                <tr><th>Body:</th><td><?php print $telo1 ?></td><td><?php print $telo2 ?></td><td><?php print $telo3 ?></td><td><?php print $telo4 ?></td><td><?php print $telo5 ?></td></tr>
                <tr><th>Hands:</th><td><?php print $ruce1 ?></td><td><?php print $ruce2 ?></td><td><?php print $ruce3 ?></td><td><?php print $ruce4 ?></td><td><?php print $ruce5 ?></td></tr>
                <tr><th>Legs:</th><td><?php print $nohy1 ?></td><td><?php print $nohy2 ?></td><td><?php print $nohy3 ?></td><td><?php print $nohy4 ?></td><td><?php print $nohy5 ?></td></tr>
                <tr><th>Deaths:</th><td colspan="5"><?php print $smrt ?></td></tr>
                <tr><th>Records:</th><td colspan="5"><?php print $zaznamu ?></td></tr>
            </table>
        <div style="text-align: center; font-size: 8pt; color: #808080;">Records for mission 'test' do not count here.</div>
        </p>
    </body>
</html>
<?php
}


function Text($text) {
    print htmlentities($text);
}


function PageUpdateWounds() {
    $mise = $_REQUEST['mise'];
    $hrac = $_REQUEST['hrac'];
    $data = $_REQUEST['data'];
    $pole = split(',', $data);
    $hlava1 = intval($pole[0]);
    $hlava2 = intval($pole[1]);
    $hlava3 = intval($pole[2]);
    $hlava4 = intval($pole[3]);
    $hlava5 = intval($pole[4]);
    $telo1 = intval($pole[5]);
    $telo2 = intval($pole[6]);
    $telo3 = intval($pole[7]);
    $telo4 = intval($pole[8]);
    $telo5 = intval($pole[9]);
    $ruce1 = intval($pole[10]);
    $ruce2 = intval($pole[11]);
    $ruce3 = intval($pole[12]);
    $ruce4 = intval($pole[13]);
    $ruce5 = intval($pole[14]);
    $nohy1 = intval($pole[15]);
    $nohy2 = intval($pole[16]);
    $nohy3 = intval($pole[17]);
    $nohy4 = intval($pole[18]);
    $nohy5 = intval($pole[19]);
    $smrt = intval($pole[20]);
    UpdateWoundsStatistics($mise, $hrac, $hlava1,$hlava2,$hlava3,$hlava4,$hlava5, $telo1,$telo2,$telo3,$telo4,$telo5, $ruce1,$ruce2,$ruce3,$ruce4,$ruce5, $nohy1,$nohy2,$nohy3,$nohy4,$nohy5, $smrt);
    header('HTTP/1.1 200 OK');
    header('Content-Type: text/plain; charset=UTF-8');
    print 'OK';
}


function PageWoundsPerMission() {
    
    header('HTTP/1.1 200 OK');
    header('Content-Type: text/html; charset=UTF-8');
    HTMLHeader('Wounds per mission');
    
    $rows = GetWoundsPerMission();
    
    
    ?>
    <body>
        <h1>Wounds per mission</h1>
        <p>
        <table class="wounds-per-mission">
            <tr>
                <th rowspan="2" class="c1">Mission</th>
                <th rowspan="2" class="c1">Records</th>
                <th colspan="5" class="c1">Head</th>
                <th colspan="5" class="c1">Body</th>
                <th colspan="5" class="c1">Hands</th>
                <th colspan="5" class="c1">Legs</th>
                <th rowspan="2" class="c1">Deaths</th>
            </tr>
            <tr><th>0-25%</th><th>25-50%</th><th>50-75%</th><th>75-100%</th><th>&gt;100%</th><th>0-25%</th><th>25-50%</th><th>50-75%</th><th>75-100%</th><th>&gt;100%</th><th>0-25%</th><th>25-50%</th><th>50-75%</th><th>75-100%</th><th>&gt;100%</th><th>0-25%</th><th>25-50%</th><th>50-75%</th><th>75-100%</th><th>&gt;100%</th></tr>
            <?php
                foreach ($rows as $row) {
                    // mise, zaznamu, hlava, telo, ruce, nohy, smrt
                    ?>
            <tr>
                <td><?php Text($row[0]) ?></td>
                <td class="c2"><?php Text($row[1]) ?></td>
                <td class="c2"><?php Text($row[2]) ?></td>
                <td><?php Text($row[3]) ?></td>
                <td><?php Text($row[4]) ?></td>
                <td><?php Text($row[5]) ?></td>
                <td><?php Text($row[6]) ?></td>
                <td class="c2"><?php Text($row[7]) ?></td>
                <td><?php Text($row[8]) ?></td>
                <td><?php Text($row[9]) ?></td>
                <td><?php Text($row[10]) ?></td>
                <td><?php Text($row[11]) ?></td>
                <td class="c2"><?php Text($row[12]) ?></td>
                <td><?php Text($row[13]) ?></td>
                <td><?php Text($row[14]) ?></td>
                <td><?php Text($row[15]) ?></td>
                <td><?php Text($row[16]) ?></td>
                <td class="c2"><?php Text($row[17]) ?></td>
                <td><?php Text($row[18]) ?></td>
                <td><?php Text($row[19]) ?></td>
                <td><?php Text($row[20]) ?></td>
                <td><?php Text($row[21]) ?></td>
                <td class="c2"><?php Text($row[22]) ?></td>
            </tr><?php
                
                }
            ?>
        </table>
        </p>
    </body>
</html>
<?php
}


function PageDefault() {
    header('HTTP/1.1 200 OK');
    header('Content-Type: text/html; charset=UTF-8');
    HTMLHeader('Statistics');
    ?>
    <body>
        <h1>Statistics </h1>
        <h2>Wounds</h2>
        <p>
            <ul>
                <li><?php HTMLLink('Totals', ActionLinkURL('wounds-summary') ) ?></li>
                <li><?php HTMLLink('Per mission', ActionLinkURL('wounds-per-mission') ) ?></li>
            </ul>
        </p>
    </body>
</html>
<?php
}

function PageError($proc) {
    header('HTTP/1.1 404 Nelibi');
    header('Content-Type: text/plain; charset=UTF-8');
    print "Tohle se mi nelibi! ".$proc;
}

function MakeLink($html,$url) {
    return "<a href=\"".htmlspecialchars($url)."\">".$html.'</a>';
}

function HTMLLink($html,$url) {
    print MakeLink($html,$url);
}

function ActionLinkURL($action,$params =  Array() ) {
    $l = $_SERVER['PHP_SELF'].'?action='.urlencode($action);
    foreach ($params as $key => $value) {
        $l = $l.'&'.urlencode($key).'='.urlencode($value);
    }
    return $l;
}

function HTMLHeader($title) {
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
    <head>
        <title><?php print htmlspecialchars($title); ?></title>
        <style type="text/css">
            body {
                background-color: aliceblue;
            }            
            
            h1 {
                text-align: center;
            }
            table {
                border-collapse: collapse;
                border: 2px solid #E0E0E0;
                background-color: white;
                margin: auto;
            }
            
            table td {
                text-align: right;
                padding: 2px 5px;
                border: 1px solid #E0E0E0;
            }
            
            table th {
                text-align: right;
                padding: 2px 5px;
                background-color: #FFFFE0;
                border: 1px solid #E0E0E0;
            }
            
            table th.c1 {
                text-align: center;
                border-right: 2px solid #E0E0E0;
                border-left: 2px solid #E0E0E0;
            }
            
            table td.c2 {
                border-left: 2px solid #E0E0E0;
            }
            
        </style>
    </head>
<?php
}

function InitDatabase($host,$username,$password,$database) {
    
    $sql = new mysqli($host,$username,$password,$database);
    if ($sql->connect_errno) {
        die('Failed to connect to database');
    }
    
    $sql->autocommit(FALSE);    /* pouzivam transakce! */
    
    if (!$sql->set_charset('utf8')) {
        die('Failed to set charset');
    }
    
    $sql->query('SET TRANSACTION ISOLATION LEVEL SERIALIZABLE');
    if ($sql->errno) {
        die('Failed to enable transactions');
    }
    
    //$sql->commit();
    return $sql;
}


// SELECT mise, COUNT(*) as hracu, SUM(hlava) as hlava, SUM(telo) as telo, SUM(ruce) as ruce, SUM(nohy) as nohy, SUM(smrt) as smrt FROM stats_zraneni GROUP BY mise;
// SELECT hrac, COUNT(*) as misi, SUM(hlava) as hlava, SUM(telo) as telo, SUM(ruce) as ruce, SUM(nohy) as nohy, SUM(smrt) as smrt FROM stats_zraneni GROUP BY hrac;
// SELECT COUNT(*) as zaznamu, SUM(hlava) as hlava, SUM(telo) as telo, SUM(ruce) as ruce, SUM(nohy) as nohy, SUM(smrt) as smrt FROM stats_zraneni;


function UpdateWoundsStatistics($mise,$hrac,$hlava1,$hlava2,$hlava3,$hlava4,$hlava5,$telo1,$telo2,$telo3,$telo4,$telo5,$ruce1,$ruce2,$ruce3,$ruce4,$ruce5,$nohy1,$nohy2,$nohy3,$nohy4,$nohy5,$smrt) {
    global $sql;
    $s = $sql->prepare('UPDATE stats_zraneni SET hlava1=hlava1+?, hlava2=hlava2+?, hlava3=hlava3+?, hlava4=hlava4+?, hlava5=hlava5+?, telo1=telo1+?, telo2=telo2+?, telo3=telo3+?, telo4=telo4+?, telo5=telo5+?, ruce1 = ruce1+?, ruce2 = ruce2+?, ruce3 = ruce3+?, ruce4 = ruce4+?, ruce5 = ruce5+?, nohy1=nohy1+?, nohy2=nohy2+?, nohy3=nohy3+?, nohy4=nohy4+?, nohy5=nohy5+?, smrt=smrt+? WHERE mise=? AND hrac=?');
    $s->bind_param('iiiiiiiiiiiiiiiiiiiiiss', $hlava1,$hlava2,$hlava3,$hlava4,$hlava5, $telo1,$telo2,$telo3,$telo4,$telo5, $ruce1,$ruce2,$ruce3,$ruce4,$ruce5, $nohy1,$nohy2,$nohy3,$nohy4,$nohy5, $smrt, $mise, $hrac);
    $success = $s->execute();
    $errno = $s->errno;
    $error = $s->error;
    $rows = $s->affected_rows;
    $s->close(); $s = NULL;
    if (!$success) {
        $sql->rollback();
        die("Failed update: $errno, $error");
    }
    
    if ($rows != 1) {
        /* record for this mission+player does not exist, insert it */
        $s = $sql->prepare('INSERT INTO stats_zraneni (mise,hrac,hlava1,hlava2,hlava3,hlava4,hlava5,telo1,telo2,telo3,telo4,telo5,ruce1,ruce2,ruce3,ruce4,ruce5,nohy1,nohy2,nohy3,nohy4,nohy5,smrt) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $s->bind_param('ssiiiiiiiiiiiiiiiiiiiii', $mise, $hrac, $hlava1,$hlava2,$hlava3,$hlava4,$hlava5,$telo1,$telo2,$telo3,$telo4,$telo5,$ruce1,$ruce2,$ruce3,$ruce4,$ruce5,$nohy1,$nohy2,$nohy3,$nohy4,$nohy5, $smrt);
        $success = $s->execute();
        $s->close(); $s = NULL;
        if (!$success) {
            $sql->rollback();
            die('Failed to insert');
        }
    }
    $sql->commit();
}

function GetWoundsSummary() {
    global $sql;
    if (!$sql->query('START TRANSACTION WITH CONSISTENT SNAPSHOT')) {
        die ('Failed to start transaction');
    }
    $result = $sql->query('SELECT COUNT(*) as zaznamu, '
            .'SUM(hlava1) as hlava1, SUM(hlava2) as hlava2, SUM(hlava3) as hlava3, SUM(hlava4) as hlava4, SUM(hlava5) as hlava5, '
            .'SUM(telo1) as telo1, SUM(telo2) as telo2, SUM(telo3) as telo3, SUM(telo4) as telo4, SUM(telo5) as telo5, '
            .'SUM(ruce1) as ruce1, SUM(ruce2) as ruce2, SUM(ruce3) as ruce3, SUM(ruce4) as ruce4, SUM(ruce5) as ruce5, '
            .'SUM(nohy1) as nohy1, SUM(nohy2) as nohy2, SUM(nohy3) as nohy3, SUM(nohy4) as nohy4, SUM(nohy5) as nohy5, '
            .'SUM(smrt) as smrt FROM stats_zraneni WHERE mise NOT LIKE \'test%\'', MYSQLI_STORE_RESULT);
    $row = $result->fetch_row();
    $result->close();
    $sql->commit();
    return $row;
}

function GetWoundsPerMission() {
    global $sql;
    if (!$sql->query('START TRANSACTION WITH CONSISTENT SNAPSHOT')) {
        die ('Failed to start transaction');
    }
    $result = $sql->query('SELECT mise, COUNT(*) as zaznamu, '
            .'SUM(hlava1) as hlava1, SUM(hlava2) as hlava2, SUM(hlava3) as hlava3, SUM(hlava4) as hlava4, SUM(hlava5) as hlava5, '
            .'SUM(telo1) as telo1, SUM(telo2) as telo2, SUM(telo3) as telo3, SUM(telo4) as telo4, SUM(telo5) as telo5, '
            .'SUM(ruce1) as ruce1, SUM(ruce2) as ruce2, SUM(ruce3) as ruce3, SUM(ruce4) as ruce4, SUM(ruce5) as ruce5, '
            .'SUM(nohy1) as nohy1, SUM(nohy2) as nohy2, SUM(nohy3) as nohy3, SUM(nohy4) as nohy4, SUM(nohy5) as nohy5, '
            .'SUM(smrt) as smrt FROM stats_zraneni GROUP BY mise ORDER BY mise ASC', MYSQLI_STORE_RESULT);
    
    $rows = array();
    
    while ($row = $result->fetch_array()) {
        $rows[] = $row;
    }
    
    $result->close();
    $sql->commit();
    return $rows;
}

?>

    
