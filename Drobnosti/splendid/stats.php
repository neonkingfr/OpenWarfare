<?php
require 'config.php';
$sql = InitDatabase($db_host,$db_username,$db_password,$db_database);

$error = NULL;
$content_type = 'text/plain; charset=UTF-8';
$data = FALSE;
ob_start();
try {
    if ($_SERVER['REQUEST_METHOD']=='POST') {
        if ($_SERVER["CONTENT_TYPE"]=='text/plain') {
            $data = fopen('php://input','r+');
            if (!is_resource($data)) throw new Exception("Cannot read request body");
            // parse array from input
            $pbs = new PutbackStream($data);
            $pbs->expect('[');
            $statname = $pbs->getString();
            $pbs->expect(',');
            $hrac = $pbs->getString();
            $pbs->expect(',');
            $mise = $pbs->getString();
            $pbs->expect(',');
            $keyvals = $pbs->ParseKeyValueArray();
            $pbs->expect(']');
            
            StartUpdateTransaction();
            foreach ($keyvals as $name => $val) {
                InsertStatRecord($statname, $hrac, $mise, $name, $val);
            }
            CommitTransaction();
            print"OK\n";
            
            
        } else {
            throw new Exception('POST, ale divny Content type: '.$_SERVER['CONTENT_TYPE']);
        }
    } else {
        
        
        
        StartReadOnlyTransaction();
        $missions = GetMissions();
        $players = GetPlayers();
        $stattypes = GetStatTypes();
        
        CommitTransaction();

        $action = '';
        if (array_key_exists('action', $_GET)) {
            $action = $_GET['action'];
        }

        $player = '';
        if (array_key_exists('player', $_GET)) {
            $player = $_GET['player'];
        }
        
        $mission = '';
        if (array_key_exists('mission', $_GET)) {
            $mission = $_GET['mission'];
        }

        $stattype = '';
        if (array_key_exists('stat', $_GET)) {
            $stattype = $_GET['stat'];
        }
        
        
        if ($action == '') {
            $content_type = 'text/html; charset=UTF-8';
            HTMLHeader('Universal statistics');
?><body>
 <form id="frm" action="<?php print $_SERVER['PHP_SELF']; ?>" method="GET">
     <div style="margin:auto; text-align: center; margin-bottom: 10px;">
         Statistic:&nbsp;<?php print MakeOptions('stat', $stattypes, $stattype); ?>
         Mission:&nbsp;<?php print MakeOptions('mission', $missions, $mission); ?>
         Player:&nbsp;<?php print MakeOptions('player', $players, $player); ?>
         <input type="submit" value="Refresh">
     </div>
 </form>
<?php
if ($stattype != '') {
    
}
?>
 </body>
<?php    
            HTMLFooter();
            ?>
            
<?php            
        }
        
    }
} catch (Exception $e) {
    $error = $e->getMessage()."\ntrace:\n";
    foreach ($e->getTrace() as $t) {
        $error=$error.sprintf("%-50s    @ %s\n", $t['function'].'('.implode(',',$t['args']).')', $t['file'].' #'.$t['line']);
    }
}

// sezrat zbytek pokud je parse error
if ($data != FALSE) {
    while (!feof($data)) {
        fgetc($data);
    }
    fclose($data);
}
if ($error === NULL) {
    header('HTTP/1.1 200 OK');
    header('Content-Type: '.$content_type);
    $sql->commit();
    ob_end_flush();
} else {
    ob_end_clean();
    $sql->rollback();
    header('HTTP/1.1 400 Bad Request');
    header('Content-Type: text/plain; charset=UTF-8');
    print "ERROR: $error\n";
} 




/*-------------- DATABASE FUNCTIONS -------------------*/

function InitDatabase($host,$username,$password,$database) {
    
    $sql = new mysqli($host,$username,$password,$database);
    if ($sql->connect_errno) {
        die('Failed to connect to database');
    }
    
    $sql->autocommit(FALSE);    /* pouzivam transakce! */
    
    if (!$sql->set_charset('utf8')) {
        die('Failed to set charset');
    }
    
    $sql->query('SET TRANSACTION ISOLATION LEVEL SERIALIZABLE');
    if ($sql->errno) {
        die('Failed to enable transactions');
    }
    
    //$sql->commit();
    return $sql;
}

function StartUpdateTransaction() {
    global $sql;
    if (!$sql->query('START TRANSACTION')) {
        throw new Exception('Unable to start update transaction');
    }
}

function StartReadOnlyTransaction() {
    global $sql;
    if (!$sql->query('START TRANSACTION WITH CONSISTENT SNAPSHOT')) {
        throw new Exception('Unable to start read only transaction');
    }
}

function CommitTransaction() {
    global $sql;
    $sql->commit();
}

function RollbackTransaction() {
    global $sql;
    $sql->rollback();
}

function InsertStatRecord($statname,$hrac,$mise,$name,$val) {
    global $sql;
    $s = $sql->prepare('INSERT INTO stats (stat,hrac,mise,name,value) VALUES (?,?,?,?,?)');
    $s->bind_param('ssssi', $statname,$hrac,$mise,$name,$val);
    $success = $s->execute();
    $error = $s->error;
    $s->close(); $s = NULL;
    if (!$success) throw new Exception('Failed to insert: '.$error);
}


function GetMissions() {
    global $sql;
    $mise = array();
    $result = $sql->query('SELECT DISTINCT mise FROM stats ORDER BY mise', MYSQLI_USE_RESULT);
    if ($result) {
        while ($row = $result->fetch_row()) {
            $mise[] = $row[0];
        }
        $result->close();
    }
    return $mise;
}

function GetPlayers() {
    global $sql;
    $hraci = array();
    $result = $sql->query('SELECT DISTINCT hrac FROM stats ORDER BY hrac', MYSQLI_USE_RESULT);
    if ($result) {
        while ($row = $result->fetch_row()) {
            $hraci[] = $row[0];
        }
        $result->close();
    }
    return $hraci;
}

function GetStatTypes() {
    global $sql;
    $typy = array();
    $result = $sql->query('SELECT DISTINCT stat FROM stats ORDER BY stat', MYSQLI_USE_RESULT);
    if ($result) {
        while ($row = $result->fetch_row()) {
            $typy[] = $row[0];
        }
        $result->close();
    }
    return $typy;
}

function GetStats($stattype,$hrac,$mise) {
    global $sql;
    $typy = array();
    $result = $sql->query('SELECT DISTINCT stat FROM stats ORDER BY stat', MYSQLI_USE_RESULT);
    if ($result) {
        while ($row = $result->fetch_row()) {
            $typy[] = $row[0];
        }
        $result->close();
    }
    return $typy;
}

/* ------ HTML formatting functions ----- */

function Text($text) {
    print htmlentities($text);
}

function HTMLHeader($title) {
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title><?php print htmlspecialchars($title); ?></title>
        <style type="text/css">
            body {
                background-color: darkkhaki;
                font-face: sans-serif;
            }            
            
            h1 {
                text-align: center;
            }
            table {
                border-collapse: collapse;
                border: 1px solid black;
                background-color: white;
                margin: auto;
            }
            
            
            
        </style>
    </head>
<?php
}

function HTMLFooter() {
    print '</html>';
}

function MakeLink($html,$url) {
    return "<a href=\"".htmlspecialchars($url)."\">".$html.'</a>';
}

function HTMLLink($html,$url) {
    print MakeLink($html,$url);
}

function ActionLinkURL($action,$params =  Array() ) {
    $l = $_SERVER['PHP_SELF'].'?action='.urlencode($action);
    foreach ($params as $key => $value) {
        $l = $l.'&'.urlencode($key).'='.urlencode($value);
    }
    return $l;
}

function MakeOptions($id,$options,$selected) {
    $out = '<select name="'.$id.'">'."\n".'<option value=""';
    if ($selected=='') {
        $out=$out.' selected="selected"';
    }
    $out = $out.'>';
    foreach ($options as $option) {
        $out=$out."\n".'<option value="'.urlencode($option).'"';
        if ($selected == $option) {
            $out = $out.' selected="selected"';
        }
        $out=$out.'>'.htmlentities($option).'</option>';
    }
    $out=$out."\n</select>";
    return $out;
}

/*---- Parsing functions -----*/
class PutbackStream {
    private $fh;
    private $lastchar = 'INVALID';
    private $have_lastchar = FALSE;
    
    function __construct($fh) {
        $this->fh = $fh;
        $this->lastchar = fgetc($fh);
        $this->have_lastchar = TRUE;
    }
    
    function next() {
        if ($this->have_lastchar) {
            $this->have_lastchar = FALSE;
            return $this->lastchar;
        } else {
            $this->lastchar = fgetc($this->fh);
            return $this->lastchar;
        }
    }
    
    function back() {
        if ($this->have_lastchar) {
            throw new Exception('Putback called more than once');
        } else {
            $this->have_lastchar = TRUE;
        }
    }
    
    function expect($char) {
        if ($this->next() != $char) throw new Exception('Expected '.$char);
    }
    
    function getNumber() {
        $sign = 1;
        $value = 0;
        $valid = FALSE;
        if ($this->next()==='-') {
            $sign = -1;
        } else {
            $this->back();
        }
        $digit = $this->next();
        while (($digit >= '0')&&($digit<='9')) {
            $value = 10*$value + ($digit-'0');
            $valid = TRUE;
            $digit = $this->next();
        }
        $this->back();
        if (!$valid) throw new Exception('Expected integer');
        return $sign*$value;
    }
    
    function getString() {
        $str = '';
        $end = FALSE;
        $this->expect('"');
        
        while (!$end) {
            $ch = $this->next();
            if ($ch === FALSE) throw new Exception('Unexpected end of string');
            if ($ch === '"') {
                if ($this->next()==='"') {
                    $str .= '"';
                } else {
                    $this->back();
                    $end = TRUE;
                }
            } else {
                $str .= $ch;
            }
        }
        
        return $str;
    }
    
    function ParseArrayOfNumbers() {
        $this->expect('[');
        $numbers = array();

        $next = $this->next();
        if ($next!=']') {
            $this->back();
            $numbers[] = $this->getNumber();
            while ($this->next() != ']') {
                $this->back();
                $this->expect(',');
                $numbers[] = $this->getNumber();
            }
        }
        return $numbers;
    }
    
    function ParseKeyValueArray() {
        $this->expect('[');
        $keyval = array();
        $next = $this->next();
        if ($next!=']') {
            $this->back();
            $name = $this->getString();
            $this->expect(',');
            $value = $this->getNumber();
            $keyval[$name] = $value;
            while ($this->next() != ']') {
                $this->back();
                $this->expect(',');
                $name = $this->getString();
                $this->expect(',');
                $value = $this->getNumber();
                $keyval[$name] = $value;
            }
        }
        return $keyval;
    }
}


?>
