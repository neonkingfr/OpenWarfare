<?php
require 'config.php';
$sql = InitDatabase($db_host,$db_username,$db_password,$db_database);

/*                background-color:#eaeaea;
                border: 4px solid;
                border-top-color:#ffffff;
                border-left-color:#f8f8f8;
                border-right-color:#d8d8d8;
                border-bottom-color:#c8c8c8;
*/
$color_map = array(
    array(  0,    '#ffffff','#f8f8f8','#eaeaea','#d8d8d8','#c8c8c8','#000000' ),
    array(  1,    '#FFFEEB','#F1F1C8','#E7E7AF','#D5D59C','#C6C68B','#000000' ),
    array(  10,    '#ffff80','#f8f86e','#eaea5a','#d8d866','#c8c832','#000000' ),
    array(  50,    '#FBD855','#E9C43C','#DDAB1F','#CB9A14','#B8870A','#000000' ),
    array(  100,   '#ff4040','#f83e3e','#ea3a3a','#d83636','#c83232','#000000' )
);


/*----- main -----*/
$error = NULL;
$content_type = 'text/plain; charset=UTF-8';
$data = FALSE;
ob_start();
try {
    if ($_SERVER['REQUEST_METHOD']=='POST') {
        if ($_SERVER["CONTENT_TYPE"]=='text/plain') {
            $data = fopen('php://input','r+');
            if (!is_resource($data)) throw new Exception("Cannot read request body");
            // parse array from input
            $pbs = new PutbackStream($data);
            $pbs->expect('[');
            $hrac = $pbs->getString();
            $pbs->expect(',');
            $mise = $pbs->getString();
            $pbs->expect(',');
            $keys = $pbs->ParseArrayOfNumbers();
            $pbs->expect(']');
            
            StartUpdateTransaction();
            foreach ($keys as $key => $count) {
                //print 'Key: '.$key.' count: '.$count."\n";
                $dik = $key+1;
                UpdateDikCount($mise,$hrac,$dik,$count);
            }
            CommitTransaction();
            print"OK\n";
            
            
        } else {
            throw new Exception('POST, ale divny Content type: '.$_SERVER['CONTENT_TYPE']);
        }
    } else {
        
        $mission = '';
        if (array_key_exists('mission', $_GET)) {
            $mission = $_GET['mission'];
        }
        
        $player = '';
        if (array_key_exists('player', $_GET)) {
            $player = $_GET['player'];
        }
        
        $modifier = '';
        if (array_key_exists('modifier', $_GET)) {
            $modifier = $_GET['modifier'];
        }
        
        
        StartReadOnlyTransaction();
        $missions = GetMissions();
        $players = GetPlayers();
        $numbers = GetKeyNumbers($mission,$player,$modifier);
        
        CommitTransaction();
        
        $content_type = 'text/html; charset=UTF-8';
        HTMLHeader('Key usage statistics');
?>
<body>
    <h1>Key usage statistics</h1>
    
    
    <form id="frm" action="<?php print $_SERVER['PHP_SELF']; ?>" method="GET">
        <div style="margin:auto; text-align: center; margin-bottom: 10px;">
        Mission:&nbsp;<select name="mission">
            <option value=""<?php if ($mission==='') print ' selected=selected'?>>*</option>
            <?php
                foreach ($missions as $m) {?>
            <option value="<?php print urlencode($m);?>"<?php if($m===$mission) print 'selected=selected';?>><?php Text($m); ?></option>
            <?php
                }
            ?>
        </select>
        Player:&nbsp;<select name="player">
            <option value="">*</option>
            <?php
                foreach ($players as $p) {?>
            <option value="<?php print urlencode($p);?>"<?php if($p===$player) print 'selected=selected';?>><?php Text($p); ?></option>
            <?php
                }
            ?>
        </select>
        Modifier:&nbsp;<select name="modifier">
            <option value=""<?php if ($modifier==='') print ' selected=selected';?>>none</option>
            <option value="ctrl"<?php if ($modifier==='ctrl') print ' selected=selected';?>>Ctrl</option>
            <option value="shift"<?php if ($modifier==='shift') print ' selected=selected';?>>Shift</option>
        </select>
        <input type="submit" value="Refresh">
        </div>
    <table id="keyboard">
        <tr>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
            <th style="width:8px;"></th>
        </tr>
        <tr>
            <?php MakeKey(0x01,'Esc',4); ?>
            <th colspan="4"></th>
            <?php MakeKey(0x3B,'F1',4); ?>
            <?php MakeKey(0x3C,'F2',4); ?>
            <?php MakeKey(0x3D,'F3',4); ?>
            <?php MakeKey(0x3E,'F4',4); ?>
            <th colspan="2"></th>
            <?php MakeKey(0x3F,'F5',4); ?>
            <?php MakeKey(0x40,'F6',4); ?>
            <?php MakeKey(0x41,'F7',4); ?>
            <?php MakeKey(0x42,'F8',4); ?>
            <th colspan="2"></th>
            <?php MakeKey(0x43,'F9',4); ?>
            <?php MakeKey(0x44,'F10',4); ?>
            <?php MakeKey(0x57,'F11',4); ?>
            <?php MakeKey(0x58,'F12',4); ?>
            <th colspan="2"></th>
            <?php MakeKey(0xB7,'PrtScr',4); ?>
            <?php MakeKey(0x46,'Scroll',4); ?>
            <?php MakeKey(0xC5,'Pause',4); ?>
        </tr>
        <tr>
            <th style="height: 15px;"></th>
        </tr>
        <tr>
            <?php MakeKey(0x29,'`',4); ?>
            <?php MakeKey(0x02,'1',4); ?>
            <?php MakeKey(0x03,'2',4); ?>
            <?php MakeKey(0x04,'3',4); ?>
            <?php MakeKey(0x05,'4',4); ?>
            <?php MakeKey(0x06,'5',4); ?>
            <?php MakeKey(0x07,'6',4); ?>
            <?php MakeKey(0x08,'7',4); ?>
            <?php MakeKey(0x09,'8',4); ?>
            <?php MakeKey(0x0A,'9',4); ?>
            <?php MakeKey(0x0B,'0',4); ?>
            <?php MakeKey(0x0C,'-',4); ?>
            <?php MakeKey(0x0D,'=',4); ?>
            <?php MakeKey(0x0E,'◀-',8); ?>
            <th colspan="2"></th>
            <?php MakeKey(0xD2,'Insert',4); ?>
            <?php MakeKey(0xC7,'Home',4); ?>
            <?php MakeKey(0xC9,'PgUp',4); ?>
            <th colspan="2"></th>
            <?php MakeKey(0x45,'Num',4); ?>
            <?php MakeKey(0xB5,'/',4); ?>
            <?php MakeKey(0x37,'*',4); ?>
            <?php MakeKey(0x4A,'-',4); ?>
            
        </tr>
        <tr>
            <?php MakeKey(0x0F,'Tab',6); ?>
            <?php MakeKey(0x10,'Q',4); ?>
            <?php MakeKey(0x11,'W',4); ?>
            <?php MakeKey(0x12,'E',4); ?>
            <?php MakeKey(0x13,'R',4); ?>
            <?php MakeKey(0x14,'T',4); ?>
            <?php MakeKey(0x15,'Y',4); ?>
            <?php MakeKey(0x16,'U',4); ?>
            <?php MakeKey(0x17,'I',4); ?>
            <?php MakeKey(0x18,'O',4); ?>
            <?php MakeKey(0x19,'P',4); ?>
            <?php MakeKey(0x1A,'[',4); ?>
            <?php MakeKey(0x1B,']',4); ?>
            <?php MakeKey(0x2B,'\\',6); ?>
            <th colspan="2"></th>
            <?php MakeKey(0xD3,'Delete',4); ?>
            <?php MakeKey(0xCF,'End',4); ?>
            <?php MakeKey(0xD1,'PgDn',4); ?>
            <th colspan="2"></th>
            <?php MakeKey(0x47,'7',4); ?>
            <?php MakeKey(0x48,'8',4); ?>
            <?php MakeKey(0x49,'9',4); ?>
            <?php MakeKey(0x4E,'+',4,2); ?>
        </tr>
        <tr>
            <?php MakeKey(0x3A,'Caps Lock',7); ?>
            <?php MakeKey(0x1E,'A',4); ?>
            <?php MakeKey(0x1F,'S',4); ?>
            <?php MakeKey(0x20,'D',4); ?>
            <?php MakeKey(0x21,'F',4); ?>
            <?php MakeKey(0x22,'G',4); ?>
            <?php MakeKey(0x23,'H',4); ?>
            <?php MakeKey(0x24,'J',4); ?>
            <?php MakeKey(0x25,'K',4); ?>
            <?php MakeKey(0x26,'L',4); ?>
            <?php MakeKey(0x27,';',4); ?>
            <?php MakeKey(0x28,'\'',4); ?>
            <?php MakeKey(0x1C,'Enter',9); ?>
            <th colspan="16"></th>
            <?php MakeKey(0x4B,'4',4); ?>
            <?php MakeKey(0x4C,'5',4); ?>
            <?php MakeKey(0x4D,'6',4); ?>
        </tr>
        <tr>
            <?php MakeKey(0x2A,'△Shift',9); ?>
            <?php MakeKey(0x2C,'Z',4); ?>
            <?php MakeKey(0x2D,'X',4); ?>
            <?php MakeKey(0x2E,'C',4); ?>
            <?php MakeKey(0x2F,'V',4); ?>
            <?php MakeKey(0x30,'B',4); ?>
            <?php MakeKey(0x31,'N',4); ?>
            <?php MakeKey(0x32,'M',4); ?>
            <?php MakeKey(0x33,',',4); ?>
            <?php MakeKey(0x34,'.',4); ?>
            <?php MakeKey(0x35,'/',4); ?>
            <?php MakeKey(0x36,'△Shift',11); ?>
            <th colspan="6"></th>
            <?php MakeKey(0xC8,'▲',4); ?>
            <th colspan="6"></th>
            <?php MakeKey(0x4F,'1',4); ?>
            <?php MakeKey(0x50,'2',4); ?>
            <?php MakeKey(0x51,'3',4); ?>
            <?php MakeKey(0x9C,'Enter',4,2); ?>
        </tr>
        <tr>
            <?php MakeKey(0x1D,'Ctrl',6); ?>
            <?php MakeKey(0xDB,'Win',5); ?>
            <?php MakeKey(0x38,'Alt',5); ?>
            <?php MakeKey(0x39,'Space',23); ?>
            <?php MakeKey(0xB8,'Alt',5); ?>
            <?php MakeKey(0xDC,'Win',5); ?>
            <?php MakeKey(0xDD,'Menu',5); ?>
            <?php MakeKey(0x9D,'Ctrl',6); ?>
            <th colspan="2"></th>
            <?php MakeKey(0x3B,'◄',4); ?>
            <?php MakeKey(0xD0,'▼',4); ?>
            <?php MakeKey(0xCD,'►',4); ?>
            <th colspan="2"></th>
            <?php MakeKey(0x52,'0',8); ?>
            <?php MakeKey(0x53,'.',4); ?>
            
        </tr>
        <tr>
            <th style="height: 2px;"></th>
        </tr>
        
    </table>

        
        
    </form>        
        
        
        
</body>
<?php
        HTMLFooter();
    }
} catch (Exception $e) {
    $error = $e->getMessage()."\ntrace:\n";
    foreach ($e->getTrace() as $t) {
        $error=$error.sprintf("%-50s    @ %s\n", $t['function'].'('.implode(',',$t['args']).')', $t['file'].' #'.$t['line']);
    }
}

// sezrat zbytek pokud je parse error
if ($data != FALSE) {
    while (!feof($data)) {
        fgetc($data);
    }
    fclose($data);
}
if ($error === NULL) {
    header('HTTP/1.1 200 OK');
    header('Content-Type: '.$content_type);
    $sql->commit();
    ob_end_flush();
} else {
    ob_end_clean();
    $sql->rollback();
    header('HTTP/1.1 400 Bad Request');
    header('Content-Type: text/plain; charset=UTF-8');
    print "ERROR: $error\n";
} 



function MakeLink($html,$url) {
    return "<a href=\"".htmlspecialchars($url)."\">".$html.'</a>';
}

function HTMLLink($html,$url) {
    print MakeLink($html,$url);
}

function ActionLinkURL($action,$params =  Array() ) {
    $l = $_SERVER['PHP_SELF'].'?action='.urlencode($action);
    foreach ($params as $key => $value) {
        $l = $l.'&'.urlencode($key).'='.urlencode($value);
    }
    return $l;
}



function Text($text) {
    print htmlentities($text);
}

function HTMLHeader($title) {
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title><?php print htmlspecialchars($title); ?></title>
        <style type="text/css">
            body {
                background-color: darkkhaki;
                font-face: sans-serif;
            }            
            
            h1 {
                text-align: center;
            }
            table {
                border-collapse: collapse;
                border: 1px solid black;
                background-color: white;
                margin: auto;
            }
            
            
            table#keyboard {
                border: solid #707070;
                border-radius: 7px;
                background: #404040;
                border-collapse: separate;
                border-right-color:#383838;
                border-bottom-color:#202020;
                border-top-width: 3px;
                border-left-width: 3px;

                border-right-width: 3px;
                border-bottom-width: 3px;
                border-top-color: #505050;
                border-left-color: #484848;
                padding: 5px;
                margin: auto;
            }

            table#keyboard tr td {
                text-align:left;
                vertical-align:top;
                background-color:#eaeaea;
                border: 4px solid;
                border-top-color:#ffffff;
                border-left-color:#f8f8f8;
                border-right-color:#d8d8d8;
                border-bottom-color:#c8c8c8;
                border-radius: 5px;
                width: 33px;
                height: 33px;
                
            }

            table#keyboard tr th {
            }
            
            table#keyboard  div.l {
                /*color: #666;*/
                font-size: 10px;
                padding-left: 3px;
            }
            table#keyboard  div.c {
                /*color: #222;*/
                text-align: right;
                padding-top: 5px;
                padding-bottom: 5px;
                padding-right: 3px;
                font-size: 10px;
            }
            
        </style>
    </head>
<?php
}

function HTMLFooter() {
    print '</html>';
}




class PutbackStream {
    private $fh;
    private $lastchar = 'INVALID';
    private $have_lastchar = FALSE;
    
    function __construct($fh) {
        $this->fh = $fh;
        $this->lastchar = fgetc($fh);
        $this->have_lastchar = TRUE;
    }
    
    function next() {
        if ($this->have_lastchar) {
            $this->have_lastchar = FALSE;
            return $this->lastchar;
        } else {
            $this->lastchar = fgetc($this->fh);
            return $this->lastchar;
        }
    }
    
    function back() {
        if ($this->have_lastchar) {
            throw new Exception('Putback called more than once');
        } else {
            $this->have_lastchar = TRUE;
        }
    }
    
    function expect($char) {
        if ($this->next() != $char) throw new Exception('Expected '.$char);
    }
    
    function getNumber() {
        $sign = 1;
        $value = 0;
        $valid = FALSE;
        if ($this->next()==='-') {
            $sign = -1;
        } else {
            $this->back();
        }
        $digit = $this->next();
        while (($digit >= '0')&&($digit<='9')) {
            $value = 10*$value + ($digit-'0');
            $valid = TRUE;
            $digit = $this->next();
        }
        $this->back();
        if (!$valid) throw new Exception('Expected integer');
        return $sign*$value;
    }
    
    function getString() {
        $str = '';
        $end = FALSE;
        $this->expect('"');
        
        while (!$end) {
            $ch = $this->next();
            if ($ch === FALSE) throw new Exception('Unexpected end of string');
            if ($ch === '"') {
                if ($this->next()==='"') {
                    $str .= '"';
                } else {
                    $this->back();
                    $end = TRUE;
                }
            } else {
                $str .= $ch;
            }
        }
        
        return $str;
    }
    
    function ParseArrayOfNumbers() {
        $this->expect('[');
        $numbers = array();

        $next = $this->next();
        if ($next!=']') {
            $this->back();
            $numbers[] = $this->getNumber();
            while ($this->next() != ']') {
                $this->back();
                $this->expect(',');
                $numbers[] = $this->getNumber();
            }
        }
        return $numbers;
    }
    
}




/*-------------- DATABASE FUNCTIONS -------------------*/

function InitDatabase($host,$username,$password,$database) {
    
    $sql = new mysqli($host,$username,$password,$database);
    if ($sql->connect_errno) {
        die('Failed to connect to database');
    }
    
    $sql->autocommit(FALSE);    /* pouzivam transakce! */
    
    if (!$sql->set_charset('utf8')) {
        die('Failed to set charset');
    }
    
    $sql->query('SET TRANSACTION ISOLATION LEVEL SERIALIZABLE');
    if ($sql->errno) {
        die('Failed to enable transactions');
    }
    
    //$sql->commit();
    return $sql;
}

function colorMapEnry($count) {
    global $color_map;
    $e = $color_map[0];
    foreach ($color_map as &$cme) {
        if ($cme[0] > $count) {
            return $e;
        } else {
            $e = $cme;
        }
    }
    return $e;
}

function keyColors($count) {
    $c = colorMapEnry($count);
    return 'background-color: '.$c[3].'; color: '.$c[6].'; border-top-color:'.$c[1].'; border-left-color:'.$c[2].'; border-right-color:'.$c[4].'; border-bottom-color:'.$c[5].';';
}

function MakeKey($dik, $label, $colspan = 1, $rowspan = 1) {
    global $numbers;
    $count = 0;
    if (array_key_exists($dik,$numbers)) {
        $count = $numbers[$dik];
    }
    
    print '<td';
    if ($colspan != 1) {
        print ' colspan='.$colspan;
    }
    if ($rowspan != 1) {
        print ' rowspan='.$rowspan;
    }
    print ' style="';
    print keyColors($count);
    print '">';
    print '<div class="l">'.$label.'</div>';
    print '<div class="c">'.$count.'</div>';
    print '</td>';
}


function StartUpdateTransaction() {
    global $sql;
    if (!$sql->query('START TRANSACTION')) {
        throw new Exception('Unable to start update transaction');
    }
}

function StartReadOnlyTransaction() {
    global $sql;
    if (!$sql->query('START TRANSACTION WITH CONSISTENT SNAPSHOT')) {
        throw new Exception('Unable to start read only transaction');
    }
}

function CommitTransaction() {
    global $sql;
    $sql->commit();
}

function RollbackTransaction() {
    global $sql;
    $sql->rollback();
}

function GetMissions() {
    global $sql;
    $mise = array();
    $result = $sql->query('SELECT DISTINCT mise FROM stats_keys', MYSQLI_USE_RESULT);
    if ($result) {
        while ($row = $result->fetch_row()) {
            $mise[] = $row[0];
        }
        $result->close();
    }
    return $mise;
}

function GetPlayers() {
    global $sql;
    $hraci = array();
    $result = $sql->query('SELECT DISTINCT hrac FROM stats_keys', MYSQLI_USE_RESULT);
    if ($result) {
        while ($row = $result->fetch_row()) {
            $hraci[] = $row[0];
        }
        $result->close();
    }
    return $hraci;
}

function UpdateDikCount($mise,$hrac,$dik,$count) {
    global $sql;
    if ($count != 0) {
        $s = $sql->prepare('UPDATE stats_keys SET pocet = pocet+? WHERE mise=? AND hrac=? AND dik=?');
        $s->bind_param('issi', $count, $mise, $hrac, $dik);
        $success = $s->execute();
        $errno = $s->errno;
        $error = $s->error;
        $rows = $s->affected_rows;
        $s->close(); $s = NULL;
        if (!$success) throw new Exception('Failed to update: '.$error);
        if ($rows === 0) {
            $s = $sql->prepare('INSERT INTO stats_keys (mise,hrac,dik,pocet) VALUES (?,?,?,?)');
            $s->bind_param('ssii', $mise, $hrac, $dik, $count);
            $success = $s->execute();
            $errno = $s->errno;
            $error = $s->error;
            $rows = $s->affected_rows;
            $s->close(); $s = NULL;
            if (!$success) throw new Exception('Failed to insert: '.$error);
        }
    }
}

function GetKeyNumbers($mission,$player,$modifier) {
    global $sql;
    $map = array();
    $s = NULL;
    switch ($modifier) {
        case '':
            if ($mission==='') {
                if ($player==='') {
                    if (!$s = $sql->prepare('SELECT dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik<256 AND mise NOT LIKE \'test%\' GROUP BY dik')) throw new Exception('Failed to prepare');
                    
                } else {
                    if (!$s = $sql->prepare('SELECT dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik<256 AND hrac=? GROUP BY dik')) throw new Exception('Failed to prepare');
                    if (!$s->bind_param('s',$player)) throw new Exception('Failed to bind param');
                }
            } else {
                if ($player==='') {
                    if (!$s = $sql->prepare('SELECT dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik<256 AND mise=? GROUP BY dik')) throw new Exception('Failed to prepare');
                    if (!$s->bind_param('s',$mission)) throw new Exception('Failed to bind param');
                } else {
                    if (!$s = $sql->prepare('SELECT dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik<256 AND mise=? AND hrac=? GROUP BY dik')) throw new Exception('Failed to prepare');
                    if (!$s->bind_param('ss',$mission,$player)) throw new Exception('Failed to bind param');
                }
            }
            break;
        case 'ctrl':
            if ($mission==='') {
                if ($player==='') {
                    if (!$s = $sql->prepare('SELECT dik-256 AS dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik>=256 AND dik<512 AND mise NOT LIKE \'test%\' GROUP BY dik')) throw new Exception('Failed to prepare');
                } else {
                    if (!$s = $sql->prepare('SELECT dik-256 AS dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik>=256 AND dik<512 AND hrac=? GROUP BY dik')) throw new Exception('Failed to prepare');
                    if (!$s->bind_param('s',$player)) throw new Exception('Failed to bind param');
                }
            } else {
                if ($player==='') {
                    if (!$s = $sql->prepare('SELECT dik-256 AS dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik>=256 AND dik<512 AND mise=? GROUP BY dik')) throw new Exception('Failed to prepare');
                    if (!$s->bind_param('s',$mission)) throw new Exception('Failed to bind param');
                } else {
                    if (!$s = $sql->prepare('SELECT dik-256 AS dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik>=256 AND dik<512 AND mise=? AND hrac=? GROUP BY dik')) throw new Exception('Failed to prepare');
                    if (!$s->bind_param('ss',$mission,$player)) throw new Exception('Failed to bind param');
                }
            }
            break;
        case 'shift':
            if ($mission==='') {
                if ($player==='') {
                    if (!$s = $sql->prepare('SELECT dik-512 AS dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik>=512 AND dik<768 AND mise NOT LIKE \'test%\' GROUP BY dik')) throw new Exception('Failed to prepare');
                } else {
                    if (!$s = $sql->prepare('SELECT dik-512 AS dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik>=512 AND dik<768 AND hrac=? GROUP BY dik')) throw new Exception('Failed to prepare');
                    if (!$s->bind_param('s',$player)) throw new Exception('Failed to bind param');
                }
            } else {
                if ($player==='') {
                    if (!$s = $sql->prepare('SELECT dik-512 AS dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik>=512 AND dik<768 AND mise=? GROUP BY dik')) throw new Exception('Failed to prepare');
                    if (!$s->bind_param('s',$mission)) throw new Exception('Failed to bind param');
                } else {
                    if (!$s = $sql->prepare('SELECT dik-512 AS dik, SUM(pocet) AS pocet FROM stats_keys WHERE dik>=512 AND dik<768 AND mise=? AND hrac=? GROUP BY dik')) throw new Exception('Failed to prepare');
                    if (!$s->bind_param('ss',$mission,$player)) throw new Exception('Failed to bind param');
                }
            }
            break;
        default:
            throw new Exception("Unknown modifier");
    }
    if (!$s->execute()) throw new Exception('Failed to execute'); 
    if (!$s->bind_result($dik,$pocet)) throw new Exception('Failed to bind result');
    while ($s->fetch()) {
        $map[$dik]=$pocet;
    }
    $s->close();
    return $map;
}

?>
