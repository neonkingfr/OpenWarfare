/* konvertor obrazku */
/* SUMA 2/1995 */

#include <windows.h>
#include <shlobj.h>
#include <string.h>
#include <stdlib.h>
#include <direct.h>
#include <dos.h>
#include <io.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys\stat.h>
#include <Es/Files/filenames.hpp>

static BOOL wasError=TRUE;

static unsigned int FileTime( const char *path )
{
	struct _stat buffer;
	if (_stat(path,&buffer)<0) return 0;
	unsigned int ret=buffer.st_mtime;
	return ret&~1; // ignore last bit - DOS vs. UNIX compatibility
}
static void CreateDir( const char *dest )
{
	// whole chain must be created
	char temp[MAX_PATH];
	strcpy(temp,dest);
	char *p;
	for( p=temp; *p; p++ )
	{
		if( *p=='\\' )
		{
			*p=0;
			CreateDirectory(temp,NULL);
			*p='\\';
		}
	}
}

static bool needLF;

static void Report( const char *text, const char *name, bool doLF=false )
{
	const int repSize=45;
	char buf[repSize+1];
	strncpy(buf,name,repSize);
	buf[repSize]=0;
	printf("%30s %-*s",text,repSize,buf);
	if( doLF ) printf("\n"),needLF=false;
	else printf("\r"),needLF=true;
}
static void EmptyLine()
{
	Report("","");
}

static bool Safe=false;
static bool StopOnError=false;

static int ConvertDir
(
	const char *dRoot, const char *dest, const char *src, const char *mask,
	bool subdirs
)
{
	char srcWild[MAX_PATH];
  // process files
	strcpy(srcWild,src);
	TerminateBy(srcWild,'\\');
	strcat(srcWild,mask);
	_finddata_t find;
	long hFile=_findfirst(srcWild,&find);
	if( hFile>=0 )
	{
		Report("Destination",src);
		BOOL destCreated=FALSE;
		do
		{
      if (find.attrib & _A_SUBDIR) continue;

			char sPath[MAX_PATH];
			char dPath[MAX_PATH];
/*
			if( !strcmp(find.name,".") ) continue;
			if( !strcmp(find.name,"..") ) continue;
*/

			// actual file - corresponding source pathname
			strcpy(sPath,src);
			TerminateBy(sPath,'\\');
			strcat(sPath,find.name);
			
			// actual file - corresponding destination pathname
			strcpy(dPath,dest);
			TerminateBy(dPath,'\\');
			strcat(dPath,find.name);
			
			// check target time
			if (FileTime(dPath)<FileTime(sPath))
			{
				// copy file from source to destination
				if( !destCreated )
				{
					Report("Writing to",dest,true);
					CreateDir(dest);
					destCreated=TRUE;
				}
				// add file description into the FolderFiles
				//Report("Copy",dPath);
				if( !Safe )
				{
					SetFileAttributes(dPath,FILE_ATTRIBUTE_NORMAL);
				}
				if( !CopyFile(sPath,dPath,FALSE) )
				{
					EmptyLine();
					printf("Error: Cannot copy file '%s' to '%s'\n",sPath,dPath);
          if (StopOnError)
          {
					  return -1;
          }
				}	
			}
			else
			{
				//Report("Skip",sPath);
			}
		}
		while( _findnext(hFile,&find)==0 );
		_findclose(hFile);
	}
  if (subdirs)
  {
    // process directories
    strcpy(srcWild,src);
    TerminateBy(srcWild,'\\');
    strcat(srcWild, "*.*");
    hFile = _findfirst(srcWild,&find);
    if( hFile>=0 )
    {
      do
      {
        if ((find.attrib & _A_SUBDIR) == 0) continue;

        if( !strcmp(find.name,".") ) continue;
        if( !strcmp(find.name,"..") ) continue;

        char sPath[MAX_PATH];
        char dPath[MAX_PATH];

        // actual file - corresponding source pathname
        strcpy(sPath,src);
        TerminateBy(sPath,'\\');
        strcat(sPath,find.name);

        // actual file - corresponding destination pathname
        strcpy(dPath,dest);
        TerminateBy(dPath,'\\');
        strcat(dPath,find.name);

        TerminateBy(dPath,'\\');
        TerminateBy(sPath,'\\');
        if( ConvertDir(dRoot,dPath,sPath,mask,subdirs)<0 ) return -1;
      }
      while( _findnext(hFile,&find)==0 );
      _findclose(hFile);
    }
  }

  return 0;
}

static HANDLE hConsoleOutput,hConsoleInput;

static void ExitCon()
{
	if( wasError )
	{
		DWORD ret;
		static const char text[]="\n-----------------------\nPress ENTER to continue";
		char buf[80];
		WriteConsole(hConsoleOutput,text,strlen(text),&ret,NULL);
		ReadConsole(hConsoleInput,buf,sizeof(buf),&ret,NULL);
	}
}

int main( int argc, const char *argv[] )
{
	char S[MAX_PATH];
	char D[MAX_PATH];

	CONSOLE_SCREEN_BUFFER_INFO csbi;
	BOOL bLaunched;
	
	// Lets try a trick to determine if we were 'launched' as a seperate
	// screen, or just running from the command line.
	// We want to do this so that when we exit, we can prompt the user
	// before we shut down if we were 'launched'. Otherwise, any data on
	// the output window will be lost.
	// We will do this by simply getting the current cursor position. It
	// 'should' always be (0,0) on a launch, and something else if we were
	// executed as a command from a console window. The only time I can see
	// this as not working, is if the user executed a CLS and appended
	// our program with the '&' character, as in:
	//   C:\> CLS & ConGUI
	// This will also result in a (0,0) cursor pos, but in this case, the
	// user might also be wanting us to 'prompt' before closeing.
	// We also need to handle the case of:
	//   C:\> ConGUI > output.dat

	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsoleOutput, &csbi);
	bLaunched = ((csbi.dwCursorPosition.X==0) && (csbi.dwCursorPosition.Y==0));
	if ((csbi.dwSize.X<=0) || (csbi.dwSize.Y <= 0)) bLaunched = FALSE;

	if( bLaunched ) atexit(ExitCon);

	bool subdirs=true;
	
	if( argc<3 )
	{
		Usage:
		printf
		(
			"Usage: sync [options] dest source\n"
			"\n"
			"Options:\n"
			"    -f - no folder recusions\n"
			"    -r - do not overwrite\n"
			"    -s - stop on first error\n"
		);
	}
	else
	{
		argv++,argc--;
		while( **argv=='-' )
		{
			if( !strcmpi(*argv,"-f") ) subdirs=false;
			else if( !strcmpi(*argv,"-r") ) Safe=true;
			else if( !strcmpi(*argv,"-s") ) StopOnError=true;
			argv++,argc--;
		}
		if( argc<2 ) goto Usage;
		// D is source directory
		strcpy(D,*argv++),argc--;
		TerminateBy(D,'\\');

		strcpy(S,*argv++),argc--;
		TerminateBy(S,'\\');

		const char *mask = "*";
		if (argc>0)
		{		
			mask = *argv++,argc--;
		}

		//ScanDir(D,D,mask);

		if( ConvertDir(D,D,S,mask,subdirs)<0 ) return -1;

		EmptyLine();
		printf("Directories synchronized.\n");
		
		wasError=0;
	}
	#if _DEBUG
		wasError=1;
	#endif
	if( !wasError ) return 0;
	return -1;
}


