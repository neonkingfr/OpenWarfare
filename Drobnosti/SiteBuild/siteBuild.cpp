#include <Es/Framework/consoleBase.h>
#include <Es/Common/win.h>
#include <stdio.h>

static inline unsigned int CalculateStringHashValue(const char *key, int hashValue=0)
{
	while (*key)
	{
		hashValue = hashValue*33 + (unsigned char)(*key++);
  }
	return hashValue;
}

int consoleMain( int argc, const char *argv[] )
{
  char username[512];
  char compname[512];
  DWORD usernameSize = sizeof(username);
  DWORD compnameSize = sizeof(compname);
  GetUserName(username,&usernameSize);
  GetComputerName(compname,&compnameSize);

  int usernameHash = CalculateStringHashValue(username);
  int compnameHash = CalculateStringHashValue(compname);

  char usernameHashStr[64];
  char compnameHashStr[64];
  ultoa(usernameHash,usernameHashStr,31);
  ultoa(compnameHash,compnameHashStr,31);

  printf("#define COMPILE_SITE \"%s\"\n",compname);
  printf("#define COMPILE_USER \"%s\"\n",username);
  printf("#define COMPILE_SITE_HASH \"%s\"\n",compnameHashStr);
  printf("#define COMPILE_USER_HASH \"%s\"\n",usernameHashStr);
#if _DEBUG
  wasError = true;
#endif

  return 0;
}
