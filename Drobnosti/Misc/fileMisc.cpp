#include <string.h>
#include "fileMisc.hpp"

char *NajdiNazev( const char *W )
{
  char *Nam=strrchr(W,'\\');
  if( Nam ) return Nam+1;
  if( W[0]!=0 && W[1]==':' ) return (char *)W+2;
  return (char *)W;
}
char *NajdiExt( const char *N )
{
  char *Nam=strrchr(N,'.');
  if( Nam ) return Nam+1;
  return (char *)N+strlen(N);
}
char *NajdiPExt( const char *N )
{
  /* NajdiExt pusobilo najasnosti */
  /* je lepsi vracet tecku */
  char *Nam=strrchr(N,'.');
  if( Nam ) return Nam;
  return (char *)N+strlen(N);
}

char *estr( const char *S )
{
  return (char *)S+strlen(S);
}

void EndChar( char *S, int c )
{
  char *e=estr(S);
  if( e==S || e[-1]!=c ) *e++=c,*e=0;
}

