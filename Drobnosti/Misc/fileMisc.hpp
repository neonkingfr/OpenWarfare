#ifndef _FILEMISC_HPP
#define _FILEMISC_HPP

char *NajdiNazev( const char *W );
char *NajdiExt( const char *N );
char *NajdiPExt( const char *N );
char *estr( const char *S );
void EndChar( char *S, int c );

#endif
