// AddrToFunctionName.cpp : Defines the entry point for the console application.
//

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.                   
#define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif						

#include <stdio.h>
#include <tchar.h>

#include <Es/essencepch.hpp>
#include <El/Debugging/mapFile.hpp>
#include <El/QStream/qStream.hpp>

int _tmain(int argc, _TCHAR* argv[])
{
  if (argc != 4)
  {
    printf("Usage: AddrToFunctionName source destination map_file\n");
    return 1;
  }

  const char *src = argv[1];
  const char *dst = argv[2];
  const char *mapfile = argv[3];

  MapFile map;
  map.ParseMapFile(mapfile);
  if (map.Empty())
  {
    fprintf(stderr, "Error: Mapfile %s is empty\n", mapfile);
    return 1;
  }

  QIFStream in;
  in.open(src);
  QOFStream out;
  out.open(dst);

  while (!in.eof())
  {
    char buffer[256];
    if (!in.readLine(buffer, 256)) continue;
    
    int offset, size;
    int read = sscanf(buffer, "  0x%x:0x%x", &offset, &size);
    if (read != 2) continue;
    
    const MapInfo *info = map.FindByPhysical(offset);
    if (!info) continue;

    out << Format("%s+0x%x:0x%x\r\n", cc_cast(info->name), offset - info->physAddress, size);
  }

  in.close();
  out.close();

  return 0;
}

