//--------------------------------------------------------------------------------------
// File: BasicHLSL11.cpp
//
// This sample shows a simple example of the Microsoft Direct3D's High-Level 
// Shader Language (HLSL) using the Effect interface. 
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
#include "DXUT.h"
#include "DXUTcamera.h"
#include "DXUTgui.h"
#include "DXUTsettingsDlg.h"
#include "SDKmisc.h"
#include "SDKMesh.h"
#include "resource.h"
#include <string>

//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------
CDXUTDialogResourceManager  g_DialogResourceManager; // manager for shared resources of dialogs
CModelViewerCamera          g_Camera;               // A model viewing camera
CDXUTDirectionWidget        g_LightControl;
CD3DSettingsDlg             g_D3DSettingsDlg;       // Device settings dialog
CDXUTDialog                 g_HUD;                  // manages the 3D   
CDXUTDialog                 g_SampleUI;             // dialog for sample specific controls
D3DXMATRIXA16               g_mCenterMesh;
float                       g_fLightScale;
int                         g_nNumActiveLights;
int                         g_nActiveLight;
bool                        g_bShowHelp = false;    // If true, it renders the UI control text

// Direct3D9 resources
CDXUTTextHelper*            g_pTxtHelper = NULL;

CDXUTSDKMesh                g_Mesh11;

ID3D11SamplerState*         g_pSamLinear = NULL;

//--------------------------------------------------------------------------------------
// Find and compile the specified shader
//--------------------------------------------------------------------------------------
HRESULT CompileShaderFromFile( WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut )
{
  HRESULT hr = S_OK;

  // find the file
  WCHAR str[MAX_PATH];
  V_RETURN( DXUTFindDXSDKMediaFileCch( str, MAX_PATH, szFileName ) );

  DWORD dwShaderFlags = D3D10_SHADER_ENABLE_STRICTNESS;
#if defined( DEBUG ) || defined( _DEBUG )
  // Set the D3D10_SHADER_DEBUG flag to embed debug information in the shaders.
  // Setting this flag improves the shader debugging experience, but still allows 
  // the shaders to be optimized and to run exactly the way they will run in 
  // the release configuration of this program.
  dwShaderFlags |= D3D10_SHADER_DEBUG;
#endif

  ID3DBlob* pErrorBlob;
  hr = D3DX11CompileFromFile( str, NULL, NULL, szEntryPoint, szShaderModel, 
    dwShaderFlags, 0, NULL, ppBlobOut, &pErrorBlob, NULL );
  if( FAILED(hr) )
  {
    if( pErrorBlob != NULL )
      OutputDebugStringA( (char*)pErrorBlob->GetBufferPointer() );
    SAFE_RELEASE( pErrorBlob );
    return hr;
  }
  SAFE_RELEASE( pErrorBlob );

  return S_OK;
}

class InstancingTechnique
{
public:
  enum ETechnique
  {
    ETNonInstanced, //< no instancing
    ETInstancedNormalCB, //< standard instancing, using cbuffer for per instance data
    ETInstancedNormalTB, //< standard instancing, using tbuffer for per instance data
    ETInstancedNormalStream, //< standard instancing, using additional stream for per instance data
    ETInstancedMultiCB, //< instancing with multiple duplicated objects in VB/IB buffers, using cbuffer for per instance data
    ETInstancedMultiTB, //< instancing with multiple duplicated objects in VB/IB buffers, using tbuffer for per instance data
    ETCount
  };
  /// per instance data
  struct PerInstanceData
  {
    D3DXMATRIX matWorld;
  };


  InstancingTechnique();
  ~InstancingTechnique();

  HRESULT Create(ETechnique technique, ID3D11Device* device,int numTrianglesPerObj, int maxInstances, float radius);
  HRESULT ReCreate(ID3D11Device* device,int numTrianglesPerObj, int maxInstances);
  void Destroy(bool destroyShaders = true);
  void Render(ID3D11DeviceContext* devContext, int numObjects);
  ETechnique GetTechnique() const {return _technique;}
    
private:
  /// constant buffer
  struct ConstantBufferPerFrame
  {
    D3DXMATRIX matViewProj;
  };

  /// creates vertex and index buffers
  HRESULT CreateVBIB(ID3D11Device* device, int numTriangles, int numInstances, float radius);
  /// creates shaders and input layouts
  HRESULT CreateShadersAndLayouts(ID3D11Device* device);
  HRESULT CreatePerInstanceStuff(ID3D11Device* device);
  HRESULT CreateConstantBuffers(ID3D11Device* device);

  void SetupPerInstanceData(ID3D11DeviceContext* devContext, int numDrawn, int instancesToDraw);
  void RenderInstances(ID3D11DeviceContext* devContext, int instancesToDraw);
  void CreateObjWorldMatrix(D3DXMATRIX &result, int objID);

  ETechnique _technique;

  ID3D11InputLayout*  _vertexLayout;
  ID3D11Buffer* _vertexBuffer;
  ID3D11Buffer* _indexBuffer;

  ID3D11Buffer* _perInstanceDataVB;
  ID3D11Buffer* _perInstanceDataTB;
  ID3D11ShaderResourceView* _perInstanceDataTBResView;  
  ID3D11Buffer* _perInstanceDataCB;
  ID3D11PixelShader* _pixelShader;
  ID3D11VertexShader* _vertexShader;

  ID3D11Buffer* _constantBufferPerFrame;
  ID3D11Buffer* _constantBufferPerObject;

  int _numTrianglesPerObj;
  int _maxInstances;

  int _vbStride;
  int _ibStride;

  float _radius;

  int _objsPerRow;
  float _xStart;
  float _xSize;
  float _yStart;
  float _ySize;

  /// current position in per instance VB
  int _curPosPerInstanceDataVB;
};

static InstancingTechnique g_instancingTechniques[InstancingTechnique::ETCount];
static InstancingTechnique::ETechnique g_currentTechnique = InstancingTechnique::ETNonInstanced;

/// 4096 * 16 = size of constant buffer
static const int MaxPossibleInstances = (4096 * 16) / (sizeof(InstancingTechnique::PerInstanceData));
static const int MaxPossibleInstancesInStream = MaxPossibleInstances * 16;

static int g_numObjects = 10000;
static int g_trianglesPerObject = 100;
static int g_maxInstances = 1;

InstancingTechnique::InstancingTechnique()
:_technique(ETCount)
, _vertexLayout (0)
, _vertexBuffer (0)
, _indexBuffer (0)
, _perInstanceDataVB (0)
, _perInstanceDataTBResView (0)
, _perInstanceDataTB (0)
, _perInstanceDataCB (0)
, _pixelShader (0)
, _vertexShader (0)
, _constantBufferPerFrame(0)
, _constantBufferPerObject(0)
, _numTrianglesPerObj(0)
, _maxInstances(0)
, _vbStride(0)
, _ibStride(0)
, _radius(0)
, _objsPerRow(0)
, _xStart(0)
, _xSize(0)
, _yStart(0)
, _ySize(0)
, _curPosPerInstanceDataVB(0)
{
}

InstancingTechnique::~InstancingTechnique()
{
  Destroy();
}

void InstancingTechnique::Destroy(bool destroyShaders)
{
  if (destroyShaders)
  {
    SAFE_RELEASE( _vertexLayout );
    SAFE_RELEASE( _pixelShader );
    SAFE_RELEASE( _vertexShader );
  }
  SAFE_RELEASE( _vertexBuffer );
  SAFE_RELEASE( _indexBuffer );
  SAFE_RELEASE( _perInstanceDataVB );
  SAFE_RELEASE( _perInstanceDataTBResView );
  SAFE_RELEASE( _perInstanceDataTB );
  SAFE_RELEASE( _perInstanceDataCB );
  SAFE_RELEASE( _constantBufferPerFrame );
  SAFE_RELEASE( _constantBufferPerObject );
}

HRESULT InstancingTechnique::CreateVBIB(ID3D11Device* device, int numTriangles, int numInstances, float radius)
{
  if (!device)
    return E_FAIL;

  if (0 == numTriangles)
    return E_FAIL;


  struct VertexBasic
  {
    float posX;
    float posY;
    float posZ;
    float normalX;
    float normalY;
    float normalZ;
    float u;
    float v;
  };

  struct VertexIndices: VertexBasic
  {
    unsigned int instanceID;
  };

  struct Index
  {
    unsigned int index;
  };

  float angleDelta = (2 * D3DX_PI) / numTriangles;

  void *vbData = 0;
  void *ibData = 0;
  UINT byteWidthVB = 0;
  UINT byteStrideVB = 0;
  UINT byteWidthIB = 0;
  UINT byteStrideIB = 0;

  switch (_technique)
  {
  case ETNonInstanced:
  case ETInstancedNormalCB:
  case ETInstancedNormalTB:
  case ETInstancedNormalStream:
    vbData = new VertexBasic[numTriangles * 3];
    ibData = new Index[numTriangles * 3];
    byteWidthVB = sizeof(VertexBasic) * numTriangles * 3;
    byteStrideVB = sizeof(VertexBasic);
    byteWidthIB = sizeof(Index) * numTriangles * 3;
    byteStrideIB = sizeof(Index);

    for (int triangle=0; triangle < numTriangles; ++triangle)
    {
      for (int vertex = 0; vertex < 3; ++vertex)
      {
        // index into array
        int index = triangle*3 + vertex;
        VertexBasic& v = ((VertexBasic *)vbData)[index];
        Index& i = ((Index *)ibData)[index];

        i.index = index;

        v.normalX = 0;
        v.normalY = 0;
        v.normalZ = 1;
        v.posZ = 0;
        v.u = 0;
        v.v = 0;

        switch (vertex)
        {
        case 0:
          v.posX = 0;
          v.posY = 0;
          break;
        case 1:
          v.posX = radius * cos((triangle + 1)* angleDelta);
          v.posY = radius * sin((triangle + 1) * angleDelta);;
          break;
        case 2:
          v.posX = radius *cos(triangle * angleDelta);
          v.posY = radius *sin(triangle * angleDelta);
          break;
        }
      }
    }

    break;
  case ETInstancedMultiCB:
  case ETInstancedMultiTB:
    vbData = new VertexIndices[numTriangles * numInstances * 3];
    ibData = new Index[numTriangles * numInstances * 3];   
    byteWidthVB = sizeof(VertexIndices) * numInstances * numTriangles * 3;
    byteStrideVB = sizeof(VertexIndices);
    byteWidthIB = sizeof(Index) * numInstances * numTriangles * 3;
    byteStrideIB = sizeof(Index);

    for (int instance = 0; instance < numInstances; ++instance)
    {
      for (int triangle=0; triangle < numTriangles; ++triangle)
      {
        for (int vertex = 0; vertex < 3; ++vertex)
        {
          // index into array
          int index = instance * numTriangles * 3 + triangle*3 + vertex;
          VertexIndices& v = ((VertexIndices *)vbData)[index];
          Index& i = ((Index *)ibData)[index];

          i.index = index;

          v.instanceID = instance;
          v.normalX = 0;
          v.normalY = 0;
          v.normalZ = 1;
          v.posZ = 0;
          v.u = 0;
          v.v = 0;

          switch (vertex)
          {
          case 0:
            v.posX = 0;
            v.posY = 0;
            break;
          case 1:
            v.posX = radius * cos((triangle + 1)* angleDelta);
            v.posY = radius * sin((triangle + 1) * angleDelta);;
            break;
          case 2:
            v.posX = radius *cos(triangle * angleDelta);
            v.posY = radius *sin(triangle * angleDelta);
            break;
          }
        }
      }
    }

    break;
  }

  _vbStride = byteStrideVB;
  _ibStride = byteStrideIB;

  if (!vbData || !ibData)
    return E_FAIL;

  // create VB
  D3D11_BUFFER_DESC vbDesc;
  vbDesc.ByteWidth = byteWidthVB;
  vbDesc.Usage = D3D11_USAGE_IMMUTABLE;
  vbDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  vbDesc.CPUAccessFlags = 0;
  vbDesc.MiscFlags = 0;
  vbDesc.StructureByteStride = byteStrideVB;

  D3D11_SUBRESOURCE_DATA vbInitData;
  vbInitData.pSysMem = vbData;
  vbInitData.SysMemPitch = 0;
  vbInitData.SysMemSlicePitch = 0;

  HRESULT hr = S_OK;
  V_RETURN(device->CreateBuffer(&vbDesc, &vbInitData, &_vertexBuffer));

  // create IB
  D3D11_BUFFER_DESC ibDesc;
  ibDesc.ByteWidth = byteWidthIB;
  ibDesc.Usage = D3D11_USAGE_IMMUTABLE;
  ibDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
  ibDesc.CPUAccessFlags = 0;
  ibDesc.MiscFlags = 0;
  ibDesc.StructureByteStride = byteStrideIB;

  D3D11_SUBRESOURCE_DATA ibInitData;
  ibInitData.pSysMem = ibData;
  ibInitData.SysMemPitch = 0;
  ibInitData.SysMemSlicePitch = 0;

  V_RETURN(device->CreateBuffer(&ibDesc, &ibInitData, &_indexBuffer));

  SAFE_DELETE_ARRAY(vbData);
  SAFE_DELETE_ARRAY(ibData);

  return hr;
}

HRESULT InstancingTechnique::CreateShadersAndLayouts(ID3D11Device* device)
{
  if (!device)
    return E_FAIL;

  std::string vsVersion;
  std::string psVersion;  
  switch( DXUTGetD3D11DeviceFeatureLevel() )
  {
  case D3D_FEATURE_LEVEL_11_0:
    {
      vsVersion = "vs_5_0";
      psVersion = "ps_5_0";
      break;
    }
  case D3D_FEATURE_LEVEL_10_1:
    {
      vsVersion = "vs_4_1";
      psVersion = "ps_4_1";
      break;
    }
  case D3D_FEATURE_LEVEL_10_0:
    {
      vsVersion = "vs_4_0";
      psVersion = "ps_4_0";
      break;
    }
  case D3D_FEATURE_LEVEL_9_3:
    {
      vsVersion = "vs_4_0_level_9_3";
      psVersion = "ps_4_0_level_9_3";
      break;
    }
  case D3D_FEATURE_LEVEL_9_2: // Shader model 2 fits feature level 9_1
  case D3D_FEATURE_LEVEL_9_1:
    {
      vsVersion = "vs_4_0_level_9_1";
      psVersion = "ps_4_0_level_9_1";
      break;
    }
  }

  std::string vsShaderName;
  std::string psShaderName = "PSMain";

  switch (_technique)
  {
  case ETNonInstanced:
    vsShaderName = "VSNonInstanced";
    break;
  case ETInstancedNormalCB:
    vsShaderName = "VSInstancedNormalCB";
    break;
  case ETInstancedNormalTB:
    vsShaderName = "VSInstancedNormalTB";
    break;
  case ETInstancedNormalStream:
    vsShaderName = "VSInstancedNormalStream";
    break;
  case ETInstancedMultiCB:
    vsShaderName = "VSInstancedMultiCB";
    break;
  case ETInstancedMultiTB:
    vsShaderName = "VSInstancedMultiTB";
    break;
  }

  HRESULT hr;
  // Create the shaders
  ID3DBlob* pVertexShaderBuffer = NULL;
  ID3DBlob* pPixelShaderBuffer = NULL;
  V_RETURN( CompileShaderFromFile( L"BasicHLSL11_VS.hlsl", vsShaderName.c_str(), vsVersion.c_str(), &pVertexShaderBuffer ) );
  V_RETURN( CompileShaderFromFile( L"BasicHLSL11_PS.hlsl", psShaderName.c_str(), psVersion.c_str(), &pPixelShaderBuffer ) );

  V_RETURN( device->CreateVertexShader( pVertexShaderBuffer->GetBufferPointer(),
    pVertexShaderBuffer->GetBufferSize(), NULL, &_vertexShader ) );
  V_RETURN( device->CreatePixelShader( pPixelShaderBuffer->GetBufferPointer(),
    pPixelShaderBuffer->GetBufferSize(), NULL, &_pixelShader ) );

  // Create our vertex input layout
  switch (_technique)
  {
  case ETNonInstanced:
  case ETInstancedNormalCB:
  case ETInstancedNormalTB:
    {
      const D3D11_INPUT_ELEMENT_DESC layout[] =
      {
        { "POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL",    0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT,    0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
      };

      V_RETURN( device->CreateInputLayout( layout, ARRAYSIZE( layout ), pVertexShaderBuffer->GetBufferPointer(),
        pVertexShaderBuffer->GetBufferSize(), &_vertexLayout ) );
    }
    break;
  case ETInstancedNormalStream:
    {
      const D3D11_INPUT_ELEMENT_DESC layout[] =
      {
        { "POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "NORMAL",    0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT,    0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "mTransform", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 0, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
        { "mTransform", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 16, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
        { "mTransform", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 32, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
        { "mTransform", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, 48, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
      };

      V_RETURN( device->CreateInputLayout( layout, ARRAYSIZE( layout ), pVertexShaderBuffer->GetBufferPointer(),
        pVertexShaderBuffer->GetBufferSize(), &_vertexLayout ) );
    }
    break;
  case ETInstancedMultiCB:
  case ETInstancedMultiTB:
    {
      {
        const D3D11_INPUT_ELEMENT_DESC layout[] =
        {
          { "POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0 },
          { "NORMAL",    0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
          { "TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT,    0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
          { "BLENDINDICES",  0, DXGI_FORMAT_R32_UINT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        };
        V_RETURN( device->CreateInputLayout( layout, ARRAYSIZE( layout ), pVertexShaderBuffer->GetBufferPointer(),
          pVertexShaderBuffer->GetBufferSize(), &_vertexLayout ) );
      }
    }
    break;
  }

  SAFE_RELEASE( pVertexShaderBuffer );
  SAFE_RELEASE( pPixelShaderBuffer );

  return hr;
}

HRESULT InstancingTechnique::CreatePerInstanceStuff(ID3D11Device* device)
{
  if (!device)
    return E_FAIL;

  int numInstances = _maxInstances;

  HRESULT hr = S_OK;

  switch (_technique)
  {
  case ETNonInstanced:
  case ETInstancedNormalCB:
  case ETInstancedMultiCB:
    {
      D3D11_BUFFER_DESC desc;
      desc.ByteWidth = sizeof(PerInstanceData) * numInstances;
      desc.Usage = D3D11_USAGE_DYNAMIC;
      desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
      desc.MiscFlags = 0;
      desc.StructureByteStride = sizeof(PerInstanceData);
      desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
      V_RETURN( device->CreateBuffer( &desc, NULL, &_perInstanceDataCB ) );
    }
    break;
  case ETInstancedNormalTB:
  case ETInstancedMultiTB:
    {
      D3D11_BUFFER_DESC desc;
      desc.ByteWidth = sizeof(PerInstanceData) * numInstances;
      desc.Usage = D3D11_USAGE_DYNAMIC;
      desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
      desc.MiscFlags = 0;
      desc.StructureByteStride = sizeof(PerInstanceData);
      desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
      V_RETURN( device->CreateBuffer( &desc, NULL, &_perInstanceDataTB ) );


      // create resource view
      D3D11_SHADER_RESOURCE_VIEW_DESC rvDesc;
      ZeroMemory( &rvDesc, sizeof( rvDesc ) );
      rvDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
      rvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
      rvDesc.Buffer.ElementOffset = 0;
      rvDesc.Buffer.ElementWidth = numInstances * 4;
      V_RETURN( device->CreateShaderResourceView( _perInstanceDataTB, &rvDesc, &_perInstanceDataTBResView ) );
    }
    break;
  case ETInstancedNormalStream:
    {
      D3D11_BUFFER_DESC desc;
      desc.ByteWidth = sizeof(PerInstanceData) * MaxPossibleInstancesInStream;
      desc.Usage = D3D11_USAGE_DYNAMIC;
      desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
      desc.MiscFlags = 0;
      desc.StructureByteStride = sizeof(PerInstanceData);
      desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
      V_RETURN( device->CreateBuffer( &desc, NULL, &_perInstanceDataVB ) );
    }
    break;
  }

  return hr;
}

HRESULT InstancingTechnique::CreateConstantBuffers(ID3D11Device* device)
{
  if (!device)
    return E_FAIL;

  // Setup constant buffers
  {
    D3D11_BUFFER_DESC desc;
    desc.ByteWidth = sizeof( ConstantBufferPerFrame );
    desc.Usage = D3D11_USAGE_DYNAMIC;
    desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    desc.MiscFlags = 0;
    desc.StructureByteStride = sizeof( ConstantBufferPerFrame );

    HRESULT hr;
    V_RETURN( device->CreateBuffer( &desc, NULL, &_constantBufferPerFrame ) );
  }

  {
    D3D11_BUFFER_DESC desc;
    desc.ByteWidth = sizeof( PerInstanceData);
    desc.Usage = D3D11_USAGE_DYNAMIC;
    desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    desc.MiscFlags = 0;
    desc.StructureByteStride = sizeof( PerInstanceData );

    HRESULT hr;
    V_RETURN( device->CreateBuffer( &desc, NULL, &_constantBufferPerObject ) );
  }

  return S_OK;
}

HRESULT InstancingTechnique::Create(InstancingTechnique::ETechnique technique, ID3D11Device* device, int numTrianglesPerObj, int maxInstances, float radius)
{
  _technique = technique;
  _radius = radius;
  _maxInstances = maxInstances;
  if (_maxInstances < 1)
    _maxInstances = 1;

  if (_maxInstances > MaxPossibleInstances)
    _maxInstances = MaxPossibleInstances;

  if (_technique == ETNonInstanced)
    _maxInstances = 1;

  _numTrianglesPerObj = numTrianglesPerObj;
  if (_numTrianglesPerObj < 3)
    _numTrianglesPerObj = 3;

  if (!device)
    return E_FAIL;

  HRESULT hr;
  V_RETURN(CreateShadersAndLayouts(device));
  V_RETURN(CreateVBIB(device, _numTrianglesPerObj, _maxInstances, _radius));
  V_RETURN(CreatePerInstanceStuff(device));
  V_RETURN(CreateConstantBuffers(device));

  return S_OK;
}

HRESULT InstancingTechnique::ReCreate(ID3D11Device* device,int numTrianglesPerObj, int maxInstances)
{
  _maxInstances = maxInstances;
  if (_maxInstances < 1)
    _maxInstances = 1;

  if (_maxInstances > MaxPossibleInstances)
    _maxInstances = MaxPossibleInstances;

  if (_technique == ETNonInstanced)
    _maxInstances = 1;

  _numTrianglesPerObj = numTrianglesPerObj;
  if (_numTrianglesPerObj < 3)
    _numTrianglesPerObj = 3;

  if (!device)
    return E_FAIL;

  Destroy(false);

  HRESULT hr;
  V_RETURN(CreateVBIB(device, _numTrianglesPerObj, _maxInstances, _radius));
  V_RETURN(CreatePerInstanceStuff(device));
  V_RETURN(CreateConstantBuffers(device));
  return S_OK;
}


void InstancingTechnique::Render(ID3D11DeviceContext* devContext, int numObjects)
{
  if (!devContext)
    return;

  if (numObjects <= 0 )
    return;

  // Get the projection & view matrix from the camera class
  D3DXMATRIX mProj = *g_Camera.GetProjMatrix();
  D3DXMATRIX mView = *g_Camera.GetViewMatrix();

  // setup constant buffers
  if (!_constantBufferPerFrame)
    return;

  D3D11_MAPPED_SUBRESOURCE mappedResource;
  HRESULT hr;
  V( devContext->Map( _constantBufferPerFrame, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource ) );
  if (!mappedResource.pData)
    return;

  ConstantBufferPerFrame* perFrame = (ConstantBufferPerFrame *)mappedResource.pData;
  D3DXMATRIX vpMatrix  = mView * mProj;
  D3DXMatrixTranspose(&(perFrame->matViewProj), &vpMatrix);
  devContext->Unmap( _constantBufferPerFrame, 0 );

  ID3D11Buffer* constBuffers[3] = {_constantBufferPerFrame, _constantBufferPerObject, _perInstanceDataCB};

  devContext->VSSetConstantBuffers( 0, 3, constBuffers );

  if (!_vertexBuffer)
    return;
  UINT strides[2] = {_vbStride, sizeof(PerInstanceData)};
  UINT offsets[2] = {0, 0};
  ID3D11Buffer* vbs[2] = {_vertexBuffer, _perInstanceDataVB};
  devContext->IASetVertexBuffers( 0, 2, vbs, strides, offsets );

  if (!_indexBuffer)
    return;
  devContext->IASetIndexBuffer(_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

  if (!_vertexShader)
    return;
  devContext->VSSetShader( _vertexShader, NULL, 0 );
  if (!_pixelShader)
    return;
  devContext->PSSetShader( _pixelShader, NULL, 0 );

  devContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
  if (!_vertexLayout)
    return;
  devContext->IASetInputLayout(_vertexLayout);

  devContext->VSSetShaderResources(0,1,&_perInstanceDataTBResView);

  if (_maxInstances <= 0)
    return;

  _objsPerRow = ceil(sqrtf(numObjects));
  _xStart = -200;
  _yStart = -200;
  _xSize = (fabs(_xStart) * 2) / _objsPerRow;
  _ySize = (fabs(_yStart) * 2) / _objsPerRow;

  int numToDraw = numObjects;
  int numDrawn = 0;
  while (numToDraw > 0)
  {
    int instancesToDraw = _maxInstances;
    if (instancesToDraw > numToDraw)
      instancesToDraw = numToDraw;

    SetupPerInstanceData(devContext, numDrawn, instancesToDraw);
    RenderInstances(devContext, instancesToDraw);
    numToDraw -= _maxInstances;
    numDrawn += _maxInstances;
  }
}

void InstancingTechnique::CreateObjWorldMatrix(D3DXMATRIX &result, int objID)
{
  static D3DXMATRIX tmp;
  float x = _xStart + (objID % _objsPerRow) * _xSize;
  float y = _yStart + (objID / _objsPerRow) * _ySize;
  D3DXMatrixTranslation(&tmp, x, y, 0);
  D3DXMatrixTranspose(&result, &tmp);
}

void InstancingTechnique::SetupPerInstanceData(ID3D11DeviceContext* devContext, int numDrawn, int instancesToDraw)
{
  if (!devContext)
    return;

  D3D11_MAPPED_SUBRESOURCE mappedResource;
  HRESULT hr;

  int baseIndex = 0;

  switch (_technique)
  {
  case ETNonInstanced:
    if (instancesToDraw != 1)
      return;
    if (!_constantBufferPerObject)
      return;
    V( devContext->Map( _constantBufferPerObject, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource ) );
    break;
  case ETInstancedNormalCB:
  case ETInstancedMultiCB:
    {
      if (!_perInstanceDataCB)
        return;
      V( devContext->Map( _perInstanceDataCB, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource ) );
    }
    break;
  case ETInstancedNormalTB:
  case ETInstancedMultiTB:
    {
      if (!_perInstanceDataTB)
        return;
      V( devContext->Map( _perInstanceDataTB, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource ) );
    }
    break;
  case ETInstancedNormalStream:
    {
      D3D11_MAP mapType;
      if (MaxPossibleInstancesInStream - _curPosPerInstanceDataVB >= instancesToDraw)
      {
        mapType = D3D11_MAP_WRITE_NO_OVERWRITE;
      }
      else
      {
        mapType = D3D11_MAP_WRITE_DISCARD;
        _curPosPerInstanceDataVB = 0;
      }

      baseIndex = _curPosPerInstanceDataVB;    

      if (!_perInstanceDataVB)
        return;
      V( devContext->Map( _perInstanceDataVB, 0, mapType, 0, &mappedResource ) );
    }
    break;
  }

  if (!mappedResource.pData)
    return;

  PerInstanceData* perInstance = (PerInstanceData *)mappedResource.pData;
  for (int i=0; i<instancesToDraw; ++i)
  {
    CreateObjWorldMatrix(perInstance[baseIndex + i].matWorld, numDrawn + i);
  }

  switch (_technique)
  {
  case ETNonInstanced:
    {
      devContext->Unmap( _constantBufferPerObject, 0 );
    }
    break;
  case ETInstancedNormalCB:
  case ETInstancedMultiCB:
    {
      devContext->Unmap( _perInstanceDataCB, 0 );
    }
    break;
  case ETInstancedNormalTB:
  case ETInstancedMultiTB:
    {
      devContext->Unmap( _perInstanceDataTB, 0 );
    }
    break;
  case ETInstancedNormalStream:
    {
      devContext->Unmap( _perInstanceDataVB, 0 );
    }
    break;
  }
}

void InstancingTechnique::RenderInstances(ID3D11DeviceContext* devContext, int instancesToDraw)
{
  if (!devContext)
    return;

  switch (_technique)
  {
  case ETNonInstanced:
    devContext->DrawIndexed(_numTrianglesPerObj * 3, 0, 0);
    break;
  case ETInstancedNormalCB:
  case ETInstancedNormalTB:
    devContext->DrawIndexedInstanced(_numTrianglesPerObj * 3, instancesToDraw, 0, 0, 0);
    break;
  case ETInstancedNormalStream:
    devContext->DrawIndexedInstanced(_numTrianglesPerObj * 3, instancesToDraw, 0, 0, _curPosPerInstanceDataVB);
    _curPosPerInstanceDataVB += instancesToDraw;
    break;
  case ETInstancedMultiCB:
  case ETInstancedMultiTB:
    devContext->DrawIndexed(instancesToDraw * _numTrianglesPerObj * 3, 0, 0);
    break;
  }
}


//--------------------------------------------------------------------------------------
// UI control IDs
//--------------------------------------------------------------------------------------
#define IDC_TOGGLEFULLSCREEN    1
#define IDC_TOGGLEREF           3
#define IDC_CHANGEDEVICE        4
#define IDC_INSTANCING_TECHNIQUE_TEXT 5
#define IDC_INSTANCING_TECHNIQUE_LIST 6
#define IDC_NUMOBJECTS_TEXT 7
#define IDC_NUMOBJECTS_EDIT 8
#define IDC_NUMTRIANGLES_TEXT 9
#define IDC_NUMTRIANGLES_EDIT 10
#define IDC_MAXINSTANCES_TEXT 11
#define IDC_MAXINSTANCES_EDIT 12

//--------------------------------------------------------------------------------------
// Forward declarations 
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext );
void CALLBACK OnFrameMove( double fTime, float fElapsedTime, void* pUserContext );
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, bool* pbNoFurtherProcessing,
                          void* pUserContext );
void CALLBACK OnKeyboard( UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext );
void CALLBACK OnGUIEvent( UINT nEvent, int nControlID, CDXUTControl* pControl, void* pUserContext );

extern bool CALLBACK IsD3D9DeviceAcceptable( D3DCAPS9* pCaps, D3DFORMAT AdapterFormat, D3DFORMAT BackBufferFormat,
                                             bool bWindowed, void* pUserContext );
extern HRESULT CALLBACK OnD3D9CreateDevice( IDirect3DDevice9* pd3dDevice,
                                            const D3DSURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext );
extern HRESULT CALLBACK OnD3D9ResetDevice( IDirect3DDevice9* pd3dDevice, const D3DSURFACE_DESC* pBackBufferSurfaceDesc,
                                           void* pUserContext );
extern void CALLBACK OnD3D9FrameRender( IDirect3DDevice9* pd3dDevice, double fTime, float fElapsedTime,
                                        void* pUserContext );
extern void CALLBACK OnD3D9LostDevice( void* pUserContext );
extern void CALLBACK OnD3D9DestroyDevice( void* pUserContext );

bool CALLBACK IsD3D11DeviceAcceptable(const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
                                       DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext );
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc,
                                      void* pUserContext );
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
                                          const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext );
void CALLBACK OnD3D11ReleasingSwapChain( void* pUserContext );
void CALLBACK OnD3D11DestroyDevice( void* pUserContext );
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, double fTime,
                                  float fElapsedTime, void* pUserContext );

void InitApp();
void RenderText();


//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
    // Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
    _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

    // DXUT will create and use the best device (either D3D9 or D3D11) 
    // that is available on the system depending on which D3D callbacks are set below

    // Set DXUT callbacks
    DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );
    DXUTSetCallbackMsgProc( MsgProc );
    DXUTSetCallbackKeyboard( OnKeyboard );
    DXUTSetCallbackFrameMove( OnFrameMove );


    DXUTSetCallbackD3D9DeviceAcceptable( IsD3D9DeviceAcceptable );
    DXUTSetCallbackD3D9DeviceCreated( OnD3D9CreateDevice );
    DXUTSetCallbackD3D9DeviceReset( OnD3D9ResetDevice );
    DXUTSetCallbackD3D9FrameRender( OnD3D9FrameRender );
    DXUTSetCallbackD3D9DeviceLost( OnD3D9LostDevice );
    DXUTSetCallbackD3D9DeviceDestroyed( OnD3D9DestroyDevice );


    DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
    DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
    DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
    DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
    DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
    DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );

    InitApp();
    DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
    DXUTSetCursorSettings( true, true ); // Show the cursor and clip it when in full screen
    DXUTCreateWindow( L"BasicHLSL11" );
    DXUTCreateDevice (D3D_FEATURE_LEVEL_9_2, true, 800, 600 );
    //DXUTCreateDevice(true, 640, 480);
    DXUTMainLoop(); // Enter into the DXUT render loop

    return DXUTGetExitCode();
}


//--------------------------------------------------------------------------------------
// Initialize the app 
//--------------------------------------------------------------------------------------
void InitApp()
{
    D3DXVECTOR3 vLightDir( -1, 1, -1 );
    D3DXVec3Normalize( &vLightDir, &vLightDir );
    g_LightControl.SetLightDirection( vLightDir );

    // Initialize dialogs
    g_D3DSettingsDlg.Init( &g_DialogResourceManager );
    g_HUD.Init( &g_DialogResourceManager );
    g_SampleUI.Init( &g_DialogResourceManager );

    g_HUD.SetCallback( OnGUIEvent ); int iY = 10;
    g_HUD.AddButton( IDC_TOGGLEFULLSCREEN, L"Toggle full screen", 0, iY, 170, 23 );
    g_HUD.AddButton( IDC_TOGGLEREF, L"Toggle REF (F3)", 0, iY += 26, 170, 23, VK_F3 );
    g_HUD.AddButton( IDC_CHANGEDEVICE, L"Change device (F2)", 0, iY += 26, 170, 23, VK_F2 );


    g_HUD.AddStatic( IDC_INSTANCING_TECHNIQUE_TEXT, L"Instancing Technique", 0, iY += 24, 105, 25 );
    CDXUTComboBox* pComboBox = NULL;
    g_HUD.AddComboBox( IDC_INSTANCING_TECHNIQUE_LIST, 0, iY += 24, 140, 24, 'I', false, &pComboBox );
    if( pComboBox )
    {
      pComboBox->SetDropHeight( 100 );
      pComboBox->AddItem(L"NonInstanced", (void*)&g_instancingTechniques[InstancingTechnique::ETNonInstanced]);
      pComboBox->AddItem(L"NormalCB", (void*)&g_instancingTechniques[InstancingTechnique::ETInstancedNormalCB]);
      pComboBox->AddItem(L"NormalTB", (void*)&g_instancingTechniques[InstancingTechnique::ETInstancedNormalTB]);
      pComboBox->AddItem(L"NormalStream", (void*)&g_instancingTechniques[InstancingTechnique::ETInstancedNormalStream]);
      pComboBox->AddItem(L"MultiCB", (void*)&g_instancingTechniques[InstancingTechnique::ETInstancedMultiCB]);
      pComboBox->AddItem(L"MultiTB", (void*)&g_instancingTechniques[InstancingTechnique::ETInstancedMultiTB]);
    }

    g_HUD.AddStatic( IDC_NUMOBJECTS_TEXT, L"Number of objects:", 0, iY += 24, 105, 25 );
    CDXUTEditBox* pEditBox = NULL;   
    WCHAR text[256];
    wsprintf(text, L"%d", g_numObjects);
    g_HUD.AddEditBox( IDC_NUMOBJECTS_EDIT, text, 0, iY += 30, 140, 30, false, &pEditBox );

    g_SampleUI.SetCallback( OnGUIEvent ); iY = 10;
}


//--------------------------------------------------------------------------------------
// Called right before creating a D3D9 or D3D11 device, allowing the app to modify the device settings as needed
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext )
{
    // Uncomment this to get debug information from D3D11
    //pDeviceSettings->d3d11.CreateFlags |= D3D11_CREATE_DEVICE_DEBUG;

    // For the first device created if its a REF device, optionally display a warning dialog box
    static bool s_bFirstTime = true;
    if( s_bFirstTime )
    {
        s_bFirstTime = false;
        if( ( DXUT_D3D11_DEVICE == pDeviceSettings->ver &&
              pDeviceSettings->d3d11.DriverType == D3D_DRIVER_TYPE_REFERENCE ) )
        {
            DXUTDisplaySwitchingToREFWarning( pDeviceSettings->ver );
        }
    }

    return true;
}


//--------------------------------------------------------------------------------------
// Handle updates to the scene.  This is called regardless of which D3D API is used
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove( double fTime, float fElapsedTime, void* pUserContext )
{
    // Update the camera's position based on user input 
    g_Camera.FrameMove( fElapsedTime );
}


//--------------------------------------------------------------------------------------
// Render the help and statistics text
//--------------------------------------------------------------------------------------
void RenderText()
{
    UINT nBackBufferHeight = ( DXUTIsAppRenderingWithD3D9() ) ? DXUTGetD3D9BackBufferSurfaceDesc()->Height :
            DXUTGetDXGIBackBufferSurfaceDesc()->Height;

    g_pTxtHelper->Begin();
    g_pTxtHelper->SetInsertionPos( 2, 0 );
    g_pTxtHelper->SetForegroundColor( D3DXCOLOR( 1.0f, 1.0f, 0.0f, 1.0f ) );
    g_pTxtHelper->DrawTextLine( DXUTGetFrameStats( DXUTIsVsyncEnabled() ) );
    g_pTxtHelper->DrawTextLine( DXUTGetDeviceStats() );

    // Draw help
    if( g_bShowHelp )
    {
        g_pTxtHelper->SetInsertionPos( 2, nBackBufferHeight - 20 * 6 );
        g_pTxtHelper->SetForegroundColor( D3DXCOLOR( 1.0f, 0.75f, 0.0f, 1.0f ) );
        g_pTxtHelper->DrawTextLine( L"Controls:" );

        g_pTxtHelper->SetInsertionPos( 20, nBackBufferHeight - 20 * 5 );
        g_pTxtHelper->DrawTextLine( L"Rotate model: Left mouse button\n"
                                    L"Rotate light: Right mouse button\n"
                                    L"Rotate camera: Middle mouse button\n"
                                    L"Zoom camera: Mouse wheel scroll\n" );

        g_pTxtHelper->SetInsertionPos( 550, nBackBufferHeight - 20 * 5 );
        g_pTxtHelper->DrawTextLine( L"Hide help: F1\n"
                                    L"Quit: ESC\n" );
    }
    else
    {
        g_pTxtHelper->SetForegroundColor( D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f ) );
        g_pTxtHelper->DrawTextLine( L"Press F1 for help" );
    }

    g_pTxtHelper->End();
}


//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, bool* pbNoFurtherProcessing,
                          void* pUserContext )
{
    // Pass messages to dialog resource manager calls so GUI state is updated correctly
    *pbNoFurtherProcessing = g_DialogResourceManager.MsgProc( hWnd, uMsg, wParam, lParam );
    if( *pbNoFurtherProcessing )
        return 0;

    // Pass messages to settings dialog if its active
    if( g_D3DSettingsDlg.IsActive() )
    {
        g_D3DSettingsDlg.MsgProc( hWnd, uMsg, wParam, lParam );
        return 0;
    }

    // Give the dialogs a chance to handle the message first
    *pbNoFurtherProcessing = g_HUD.MsgProc( hWnd, uMsg, wParam, lParam );
    if( *pbNoFurtherProcessing )
        return 0;
    *pbNoFurtherProcessing = g_SampleUI.MsgProc( hWnd, uMsg, wParam, lParam );
    if( *pbNoFurtherProcessing )
        return 0;

    g_LightControl.HandleMessages( hWnd, uMsg, wParam, lParam );

    // Pass all remaining windows messages to camera so it can respond to user input
    g_Camera.HandleMessages( hWnd, uMsg, wParam, lParam );

    return 0;
}


//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard( UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext )
{
    if( bKeyDown )
    {
        switch( nChar )
        {
            case VK_F1:
                g_bShowHelp = !g_bShowHelp; break;
        }
    }
}


//--------------------------------------------------------------------------------------
// Handles the GUI events
//--------------------------------------------------------------------------------------
void CALLBACK OnGUIEvent( UINT nEvent, int nControlID, CDXUTControl* pControl, void* pUserContext )
{
    switch( nControlID )
    {
        case IDC_TOGGLEFULLSCREEN:
            DXUTToggleFullScreen(); break;
        case IDC_TOGGLEREF:
            DXUTToggleREF(); break;
        case IDC_CHANGEDEVICE:
            g_D3DSettingsDlg.SetActive( !g_D3DSettingsDlg.IsActive() ); break;

        case IDC_INSTANCING_TECHNIQUE_LIST:
          {
            CDXUTComboBox* pComboBox = ( CDXUTComboBox* )pControl;
            
            InstancingTechnique* technique = (InstancingTechnique *)pComboBox->GetSelectedData();
            if (technique)
            {
              g_currentTechnique = technique->GetTechnique();
            }
            break;
          }
        case IDC_NUMOBJECTS_EDIT:
          {
            CDXUTEditBox* pEditBox = ( CDXUTEditBox* )pControl;
            int val = _wtoi(pEditBox->GetText());           
            if (val > 0)
              g_numObjects = val;
            break;
          }


    }

}


//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable( const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
                                       DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext )
{
    return true;
}


//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependant on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc,
                                      void* pUserContext )
{
    HRESULT hr;

    ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();
    V_RETURN( g_DialogResourceManager.OnD3D11CreateDevice( pd3dDevice, pd3dImmediateContext ) );
    V_RETURN( g_D3DSettingsDlg.OnD3D11CreateDevice( pd3dDevice ) );
    g_pTxtHelper = new CDXUTTextHelper( pd3dDevice, pd3dImmediateContext, &g_DialogResourceManager, 15 );

    D3DXVECTOR3 vCenter( 0.25767413f, -28.503521f, 111.00689f );
    FLOAT fObjectRadius = 378.15607f;

    D3DXMatrixTranslation( &g_mCenterMesh, -vCenter.x, -vCenter.y, -vCenter.z );
    D3DXMATRIXA16 m;
    D3DXMatrixRotationY( &m, D3DX_PI );
    g_mCenterMesh *= m;
    D3DXMatrixRotationX( &m, D3DX_PI / 2.0f );
    g_mCenterMesh *= m;

    // Setup the camera's view parameters
    D3DXVECTOR3 vecEye( 0.0f, 0.0f, -100.0f );
    D3DXVECTOR3 vecAt ( 0.0f, 0.0f, -0.0f );
    g_Camera.SetViewParams( &vecEye, &vecAt );
    g_Camera.SetRadius( fObjectRadius * 3.0f, fObjectRadius * 0.5f, fObjectRadius * 10.0f );

    for (int i=0; i< InstancingTechnique::ETCount; ++i)
    {
      g_instancingTechniques[i].Create((InstancingTechnique::ETechnique)i, pd3dDevice, g_trianglesPerObject, g_maxInstances, 20.0f);
    }

    return S_OK;
}


//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
                                          const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
    HRESULT hr;

    V_RETURN( g_DialogResourceManager.OnD3D11ResizedSwapChain( pd3dDevice, pBackBufferSurfaceDesc ) );
    V_RETURN( g_D3DSettingsDlg.OnD3D11ResizedSwapChain( pd3dDevice, pBackBufferSurfaceDesc ) );

    // Setup the camera's projection parameters
    float fAspectRatio = pBackBufferSurfaceDesc->Width / ( FLOAT )pBackBufferSurfaceDesc->Height;
    g_Camera.SetProjParams( D3DX_PI / 4, fAspectRatio, 2.0f, 4000.0f );
    g_Camera.SetWindow( pBackBufferSurfaceDesc->Width, pBackBufferSurfaceDesc->Height );
    g_Camera.SetButtonMasks( MOUSE_MIDDLE_BUTTON, MOUSE_WHEEL, MOUSE_LEFT_BUTTON );

    g_HUD.SetLocation( pBackBufferSurfaceDesc->Width - 170, 0 );
    g_HUD.SetSize( 170, 170 );
    g_SampleUI.SetLocation( pBackBufferSurfaceDesc->Width - 170, pBackBufferSurfaceDesc->Height - 300 );
    g_SampleUI.SetSize( 170, 300 );

    return S_OK;
}


//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, double fTime,
                                  float fElapsedTime, void* pUserContext )
{
    HRESULT hr;

    // If the settings dialog is being shown, then render it instead of rendering the app's scene
    if( g_D3DSettingsDlg.IsActive() )
    {
        g_D3DSettingsDlg.OnRender( fElapsedTime );
        return;
    }

    // Clear the render target and depth stencil
    float ClearColor[4] = { 0.0f, 0.25f, 0.25f, 0.55f };
    ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
    pd3dImmediateContext->ClearRenderTargetView( pRTV, ClearColor );
    ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
    pd3dImmediateContext->ClearDepthStencilView( pDSV, D3D11_CLEAR_DEPTH, 1.0, 0 );

    g_instancingTechniques[g_currentTechnique].Render(pd3dImmediateContext, g_numObjects);

    DXUT_BeginPerfEvent( DXUT_PERFEVENTCOLOR, L"HUD / Stats" );
    g_HUD.OnRender( fElapsedTime );
    g_SampleUI.OnRender( fElapsedTime );
    RenderText();
    DXUT_EndPerfEvent();
}


//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain( void* pUserContext )
{
    g_DialogResourceManager.OnD3D11ReleasingSwapChain();
}


//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice( void* pUserContext )
{
    g_DialogResourceManager.OnD3D11DestroyDevice();
    g_D3DSettingsDlg.OnD3D11DestroyDevice();
    //CDXUTDirectionWidget::StaticOnD3D11DestroyDevice();
    DXUTGetGlobalResourceCache().OnDestroyDevice();
    SAFE_DELETE( g_pTxtHelper );

    g_Mesh11.Destroy();
                
    SAFE_RELEASE( g_pSamLinear );

    for (int i=0; i< InstancingTechnique::ETCount; ++i)
    {
      g_instancingTechniques[i].Destroy();
    }
}



