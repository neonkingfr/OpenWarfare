//--------------------------------------------------------------------------------------
// File: BasicHLSL11_VS.hlsl
//
// The vertex shader file for the BasicHLSL11 sample.  
// 
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Globals
//--------------------------------------------------------------------------------------
cbuffer cbPerFrame : register( b0 )
{
	matrix matViewProj: packoffset( c0 );
};

cbuffer cbPerObject : register( b1 )
{
	matrix matWorldPerObj: packoffset( c0 );
};

// 4096 / 4 (4x4 matrices)
#define MAX_INSTANCES 1024

cbuffer cbPerInstanceData : register( b2 )
{
	matrix cbMatWorldPerInst[MAX_INSTANCES]: packoffset( c0 );
};

tbuffer tbPerInstanceData : register(t0)
{
	matrix tbMatWorldPerInst[MAX_INSTANCES];
}

//--------------------------------------------------------------------------------------
// Input / Output structures
//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
	float4 vPosition	: SV_POSITION;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VSNonInstanced(
  in float3 vPosition	: POSITION,
	in float3 vNormal		: NORMAL,
	in float2 vTexcoord	: TEXCOORD0
 )
{
	VS_OUTPUT Output;

	Output.vPosition = mul( float4(vPosition, 1), matWorldPerObj );
	Output.vPosition = mul( Output.vPosition, matViewProj);
	return Output;
}


VS_OUTPUT VSInstancedNormalCB(
  in float3 vPosition	: POSITION,
	in float3 vNormal		: NORMAL,
	in float2 vTexcoord	: TEXCOORD0,
  in uint instanceID : SV_InstanceID
 )
{
	VS_OUTPUT Output;

	Output.vPosition = mul( float4(vPosition, 1), cbMatWorldPerInst[instanceID] );
	Output.vPosition = mul( Output.vPosition, matViewProj);
	return Output;
}

VS_OUTPUT VSInstancedNormalTB(
  in float3 vPosition	: POSITION,
	in float3 vNormal		: NORMAL,
	in float2 vTexcoord	: TEXCOORD0,
  in uint instanceID : SV_InstanceID
 )
{
	VS_OUTPUT Output;

	Output.vPosition = mul( float4(vPosition, 1), tbMatWorldPerInst[instanceID] );
	Output.vPosition = mul( Output.vPosition, matViewProj);
	return Output;
}

VS_OUTPUT VSInstancedNormalStream(
  in float3 vPosition	: POSITION,
	in float3 vNormal		: NORMAL,
	in float2 vTexcoord	: TEXCOORD0,
  in float4x4 mTransform : mTransform
 )
{
	VS_OUTPUT Output;

	Output.vPosition = mul( float4(vPosition, 1), mTransform );
	Output.vPosition = mul( Output.vPosition, matViewProj);
	return Output;
}

VS_OUTPUT VSInstancedMultiCB(
  in float3 vPosition	: POSITION,
	in float3 vNormal		: NORMAL,
	in float2 vTexcoord	: TEXCOORD0,
  in uint instanceID : BLENDINDICES
 )
{
	VS_OUTPUT Output;

	Output.vPosition = mul( float4(vPosition, 1), cbMatWorldPerInst[instanceID] );
	Output.vPosition = mul( Output.vPosition, matViewProj);
	return Output;
}

VS_OUTPUT VSInstancedMultiTB(
  in float3 vPosition	: POSITION,
	in float3 vNormal		: NORMAL,
	in float2 vTexcoord	: TEXCOORD0,
  in uint instanceID : BLENDINDICES
 )
{
	VS_OUTPUT Output;

	Output.vPosition = mul( float4(vPosition, 1), tbMatWorldPerInst[instanceID] );
	Output.vPosition = mul( Output.vPosition, matViewProj);
	return Output;
}
