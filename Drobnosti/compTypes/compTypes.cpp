#include <stdio.h>
#include <Es/essencePch.hpp>
#include <Es/Framework/debugLog.hpp>
#include <Es/Common/fltOpts.hpp>
#include <El/Math/math3D.hpp>
#include <El/Math/mathStore.hpp>


int main(int argc, const char *argv[])
{
  Float16bFixed<8> f1(0);
  Float16b<10> f2(-2.5e-4f);
  Float16b<10> f3(-1.3e-8f);
  Float16b<10> f4(1e8f);
  printf("f1 %.3f\n",float(f1));
  printf("f2 %.3f\n",float(f2));
  printf("f3 %.3f\n",float(f3));
  printf("f4 %.3f\n",float(f4));
  printf("Press enter...\n");
  getchar();

  // test quaternion maths
  Matrix3 rot = Matrix3(MRotationX,0.7f)*Matrix3(MRotationY,0.2f)*Matrix3(MRotationZ,0.9f);
  Quaternion<float> q1(rot);
  Quaternion< Float16bFixed<14> > q2(rot);
  Quaternion< Float16b<10> > q3(rot);
  Matrix3 rot1 = q1;
  Matrix3 rot2 = q2;
  Matrix3 rot3 = q3;
  //Quaternion< Float16b<10> > q3(rot);

  return 0;
}