# Microsoft Developer Studio Project File - Name="compTypes" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 60000
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=compTypes - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "compTypes.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "compTypes.mak" CFG="compTypes - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "compTypes - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "compTypes - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Drobnosti/compTypes", ABSAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "compTypes - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "MFC_NEW" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "compTypes - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "MFC_NEW" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "compTypes - Win32 Release"
# Name "compTypes - Win32 Debug"
# Begin Group "Es"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\array.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Strings\bstring.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\list.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\listBidir.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\memtype.h
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\normalNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\platform.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeOpts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\useAppFrameDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\useMallocMemFunctions.cpp
# End Source File
# End Group
# Begin Group "El"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\elementpch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\math3d.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\math3dP.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\math3dP.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\mathDefs.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\mathOpt.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\mathOpt.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Math\mathStore.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\compTypes.cpp
# End Source File
# End Target
# End Project
