using System;
using System.Collections.Generic;
using System.Text;

using SharpSvn;

namespace SVNRevision
{
  class Program
  {
    static int Main(string[] args)
    {
      if (args.Length != 1) return -1;

      try
      {
        SvnClient client = new SvnClient();
        SharpSvn.UI.SvnUI.Bind(client, (System.Windows.Forms.IWin32Window)null);
        SvnTarget target = SvnTarget.FromString(args[0]);
        SvnInfoEventArgs info;
        if (!client.GetInfo(target, out info)) return -1;
        long revision = info.Revision;
        Console.Write(revision.ToString());
        return 0;
      }
      catch (SvnWorkingCopyException ex)
      {
        if (ex.SvnErrorCode == SvnErrorCode.SVN_ERR_WC_UNSUPPORTED_FORMAT)
        {
          return 0;
        }
        Console.Error.Write("SVNRevision '" + args[0] + "': exception " + ex.ToString());
        return -1;
      }
      catch (Exception ex)
      {
        Console.Error.Write("SVNRevision '" + args[0] + "': exception " + ex.ToString());
        return -1;
      }
    }
  }
}
