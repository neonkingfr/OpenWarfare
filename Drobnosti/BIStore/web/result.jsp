<%--
    Document   : result
    Created on : 7.12.2010, 8:35:24
    Author     : Jirka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css?v=4" media="screen, projection"  />
        <title>Product Key</title>
        <style type="text/css">
            input{
            border: 1px;
            color: #fff;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
        <div id="header">
         <div id="btn-wrap">
                            <div class="visite shadow"><a href="http://www.arma2.com" title="official Arma 2 website" target="_parrent">Visit Arma 2 official website</a></div>

        <div class="button" id="updates"><a href="updates.html" class="shadow" title="Download updates Arma 2: Free" target="_parrent">DOWNLOAD UPDATES</a></div>
        <div class="button" id="download"><a href="http://www.arma2.com/free#mirrors" class="shadow" title="Download Arma 2: Free" target="_parrent">DOWNLOAD ARMA 2: FREE</a></div>

        </div>
            <a href="http://free.arma2.com/activation/"><img src="images/arma2free_logo.png" alt="arma 2 free"/></a>
            <h1 class="shadow">Your product key:</h1>
        </div>
            <div id="content">
        <div class="fullWid">
        <input type="text" name="key" value="<%= request.getAttribute("key") %>" readonly size="80"/>
        </div>
        
        <div class="copyKey shadow"><img alt=""  src="images/arrow_l.png"/>Please, copy the key to the clipboard (Ctrl + 'C').<img alt=""  src="images/arrow_r.png" /></div>
        <div id="ads">
        <h3>Google Ads</h3>
		<script type="text/javascript"><!--
google_ad_client = "ca-pub-4164884610799232";
/* Arma free */
google_ad_slot = "6669402883";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
		</div>
        
        <script type="text/javascript">
            var key = document.getElementsByName('key')[0];
            key.focus();
            key.select();
        </script>
        <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2014915-16']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
            </div>
        </div>
    </body>
</html>