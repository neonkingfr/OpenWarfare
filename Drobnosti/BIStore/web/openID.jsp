<%--
    Document   : google
    Created on : 7.12.2010, 8:01:50
    Author     : Jirka
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css?v=4" media="screen, projection"  />
        <title>Sign in using your OpenID</title>
        <style type="text/css">
        .openID
        {
            background: url(images/input_bg.png) repeat-x top left;
            height: 20px;
            width: 200px;
            border: 1px;
            padding: 0 0 0 2px;
            margin: 0;
            color:#fff;
            vertical-align: middle;
        }

        .openID:focus{
        -moz-box-shadow: 0px 0px 5px #fff;
        -webkit-box-shadow: 0px 0px 5px #fff;
        box-shadow: 0px 0px 5px #fff;    
        }

        form{
            background: black;
            height: 50px;
            margin-left: -70px;
            padding-top: 20px;
        }

        .getId {
            margin-left: -70px;
        }
        
        </style>
    </head>
    <body>
        <div id="wrapper">
             <div id="header">
           <div id="btn-wrap">
                            <div class="visite shadow"><a href="http://www.arma2.com" title="official Arma 2 website" target="_parrent">Visit Arma 2 official website</a></div>

        <div class="button" id="updates"><a href="updates.html" class="shadow" title="Download updates Arma 2: Free" target="_parrent">DOWNLOAD UPDATES</a></div>
        <div class="button" id="download"><a href="http://www.arma2.com/free#mirrors" class="shadow" title="Download Arma 2: Free" target="_parrent">DOWNLOAD ARMA 2: FREE</a></div>

        </div>
             <a href="http://free.arma2.com/activation/"><img src="images/arma2free_logo.png" alt="arma 2 free"/></a>
            <h1 class="shadow">Sign in using your OpenID:</h1>
        </div>
         <div id="content">
             <form name="OpenID Login Form" action="OpenID" method="POST" class="shadow">
            <input type="text" name="OpenID" class="openID shadow" size="80%" value="type your OpenID here" />
            <input type="submit" size="20%" value="OK" class="okBtn shadow"/>
        </form>
             <div class="getId shadow"><img alt=""  src="images/arrow_l.png"/><a href="http://openid.net/get-an-openid/" target="_blank">Get an OpenID</a><img alt=""  src="images/arrow_r.png" /></div>
         </div>
         </div>
         <div id="ads">
         <h3>Google Ads</h3>
		<script type="text/javascript"><!--
google_ad_client = "ca-pub-4164884610799232";
/* Arma free */
google_ad_slot = "6669402883";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
		</div>
        <div style="width: 900px; margin: 200px auto 0 auto">
            <a href="http://www.bistudio.com/index.php/english/company/profile" title="Bohemia interactive website" target="_parrent"><img src="images/Bohemia Interactive.png" alt="Bohemia Interactive" /></a> <br />
            Copyright © 2011 Bohemia Interactive.<br /> All Rights Reserved.
    ARMA 2™ and Bohemia Interactive™ are trademarks of Bohemia Interactive.
    <a href="privacy.html">Privacy statement</a>
</div>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2014915-16']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    </body>
</html>
