<%-- 
    Document   : index
    Created on : 24.11.2010, 11:59:37
    Author     : BI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css?v=4" media="screen, projection"  />
        <script type="text/javascript"  src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
        <title>Sign in to activate Arma 2 Free!</title>
        <style>
            h1{
            background: none;
            font-size: 12px;
            padding: 0;
            margin: 5px;
            width: 99%;
         }
        </style>
    </head>
    <body>
        <div id="wrapper">
        <div id="header">
            <div id="btn-wrap">
                            <div class="visite shadow"><a href="http://www.arma2.com" title="official Arma 2 website" target="_parrent">Visit Arma 2 official website</a></div>

        <div class="button" id="updates"><a href="updates.html" class="shadow" title="Download updates Arma 2: Free" target="_parrent">DOWNLOAD UPDATES</a></div>
        <div class="button" id="download"><a href="http://www.arma2.com/free#mirrors" class="shadow" title="Download Arma 2: Free" target="_parrent">DOWNLOAD ARMA 2: FREE</a></div>

        </div>            <a href="http://free.arma2.com/activation/"><img src="images/arma2free_logo.png" alt="arma 2 free"/></a>
            
        </div>
            <div id="content">
                <div id="sign" class="shadow"><h1><img alt=""  src="images/arrow.gif" style="vertical-align: middle;"/> Sign in using your account to activate Arma 2 Free! No credit card or sensitive personal data required to play!</h1>
                <%
        String error = "";
        Object obj = request.getAttribute("error");
        if (obj != null) error = (String)obj;

        obj = request.getParameter("nonce");
        if (obj != null)
        {
            String nonce = (String)obj;
            session.setAttribute("nonce", nonce);
        }
%>
<p><%= error %></p>
                </div>
        <div id="register">
            <img src="images/sign.png" alt="" />
        <form name="OpenID Login Form" action="openID.jsp">
            <button name = "submit_button" class="btn openID shadow" id = "submit_button" >
            </button>
        </form>
        <form name="Facebook Login Form" action="Facebook">
            <button name = "submit_button" class="btn facebook shadow" id = "submit_button">
            </button>
        </form>
        <form name="Windows Live ID Login Form" action="WindowsLiveID">
                <button name = "submit_button" class="btn liveID shadow" id = "submit_button">
            </button>
        </form>
        <form name="Google Login Form" action="Google">
            <button name = "submit_button" class="btn google shadow" id = "submit_button">
            </button>
        </form>
        <form name="Steam Login Form" action="Steam">
            <button name = "submit_button" class="btn steam shadow" id = "submit_button">
            </button>
        </form>
                    <iframe src="http://www.bistudio.com/banner/banners.php?g=armafree" frameborder="0" scrolling="no" width="100px" height="200px" allowtransparency="true" style="padding-top: 10px "></iframe>
        </div>
                <div id="desc" class="shadow">
                    <p>
                        <a href="http://www.arma2.com/free" title="Arma 2 Free">Arma 2: Free</a> (A2F) redefines the free-to-play battlefield with its truly unrivalled scale and gameplay possibilities. <a href="http://www.arma2.com/free" title="Arma 2 Free">A2F</a> serves up almost everything offered by the original Arma 2 - the '13th best PC game of all time', according to PC Gamer <a href="http://www.pcgamer.com/2011/02/16/the-100-best-pc-games-of-all-time/9/" title="http://www.pcgamer.com/2011/02/16/the-100-best-pc-games-of-all-time/9/" >[1]</a> - minus the campaign, HD graphics and support for user-made addons and mods.</p>
                    <p> Create your own custom-built scenarios or deploy a massive selection of missions and game-modes made by others. No micro-transactions, no hidden costs, just the same epic terrain and huge variety of equipment! This is <a href="http://www.arma2.com/free" title="Arma 2 Free">Arma 2 Free</a> - virtual war without the training wheels.</p>
                    <p style="border-bottom: black dotted 1px; width: 820px; padding-bottom: 15px;"><a href="http://www.arma2.com/free" title="Arma 2 Free">Arma 2: Free</a> to download, free to play, free to share, free to host, free to create... free to play redefined!
                    </p>

<table cellpadding="4" cellspacing="0" border="0" style="border-bottom: #000 solid 4px;">
<thead>
    <tr>
        <th>Features</th><th><b><a href="http://www.arma2.com/game-features/arma-2-co-game-features_en.html
                                   " title="Arma 2: Combined Operations">Arma 2: Combined Operations</a></b> </th><th> <b><a href="http://www.bistudio.com/index.php/english/games/arma-2-series/arma-2
                                                                     " title="Arma 2">Arma 2</a></b> </th><th> <b><a href="http://www.arma2.com/free" title="Arma 2 Free">Arma 2: Free</a></b>
</th></tr>
</thead>
<tfoot><tr> <td colspan="4"> * available at extra cost </td> </tr> </tfoot>
<tbody>
<tr>
    <td> <strong>Mil-Sim</strong> </td><td class="center"> <img src="images/ok.png" alt=""> </td><td class="center"> <img src="images/ok.png" alt=""> </td><td class="center"> <img src="images/ok.png" alt="">
</td></tr>
<tr>
<td> <strong>Multiplayer</strong> </td><td class="center"><img src="images/ok.png" alt=""> </td><td class="center"> <img src="images/ok.png" alt=""></td><td class="center"> <img src="images/ok.png" alt="">
</td></tr>
<tr>
<td> <strong>Mission Editing</strong> </td><td class="center"> <img src="images/ok.png" alt=""> </td><td class="center"> <img src="images/ok.png" alt=""></td><td class="center"> <img src="images/ok.png" alt="">
</td></tr>
<tr>
<td> <strong>High Definition Graphics</strong> </td><td class="center"> <img src="images/ok.png" alt=""> </td><td class="center"> <img src="images/ok.png" alt=""> </td><td class="center"> <img src="images/no.png" alt="">
</td></tr>
<tr>
<td> <strong>User Mods</strong> </td><td class="center"> <img src="images/ok.png" alt=""> </td><td class="center"> <img src="images/ok.png" alt=""></td><td class="center"> <img src="images/no.png" alt="">
</td></tr>
<tr>
<td> <strong>Story Driven Campaigns</strong> </td><td class="center"> <img src="images/ok.png" alt=""></td><td class="center"> <img src="images/ok.png" alt=""> </td><td class="center"> <img src="images/no.png" alt="">
</td></tr>
<tr>
<td> <strong>Operation Arrowhead</strong> </td><td class="center"> <img src="images/ok.png" alt=""></td><td class="center"> <img src="images/no.png" alt=""> </td><td class="center"> <img src="images/no.png" alt="">
</td></tr>
<tr>
<td> <strong>BAF and PMC Addons*</strong> </td><td class="center"> <img src="images/ok.png" alt=""> </td><td class="center"> <img src="images/no.png" alt=""> </td><td class="center"> <img src="images/no.png" alt="">
</td></tr>
</tbody></table>
                    <h4>Also available:</h4>
                    <p class="pRight"><a href="http://www.sprocketidea.com/joomla/index.php?page=shop.product_details&amp;amp;flypage=flypage-honza.tpl&amp;amp;product_id=37&amp;amp;category_id=2&amp;amp;option=com_virtuemart&amp;amp;Itemid=2" title="BUY Arma 2" target="_blank"><img height="85" width="70" src="http://www.arma2.com/images/arma2.png" alt="buyA2oa-baf-dvdbox"><br /><span class="buy">BUY Arma 2</span></a></p>
                    <p class="pRight"><a href="http://www.sprocketidea.com/joomla/index.php?page=shop.product_details&amp;flypage=flypage-honza.tpl&amp;product_id=32&amp;category_id=2&amp;option=com_virtuemart&amp;Itemid=2" title="BUY Arma 2: Operation Arrowhead" target="_blank"><img alt=""  height="85" width="70" src="http://www.arma2.com/images/arma2oa.png"><br /><span class="buy">BUY Arma 2: <acronym title="Operation Arrowhead">OA</acronym> </span></a></p>
                    <p class="pRight"><a href="http://www.sprocketidea.com/joomla/index.php?page=shop.product_details&amp;flypage=flypage-honza.tpl&amp;product_id=33&amp;category_id=2&amp;option=com_virtuemart&amp;Itemid=2&amp;vmcchk=1&amp;Itemid=2" title="BUY Arma 2: Combined Operations" target="_blank"><img height="85" width="70" src="http://www.arma2.com/images/arma2co.png" alt="buyA2co-dvdbox"><br /><span class="buy">BUY Arma 2: <acronym title="Combined Operations">CO</acronym> </span></a></p>
                    <p class="pRight"><a href="http://www.sprocketidea.com/joomla/index.php?page=shop.product_details&amp;amp;flypage=flypage-honza.tpl&amp;amp;product_id=37&amp;amp;category_id=2&amp;amp;option=com_virtuemart&amp;amp;Itemid=2" title="BUY Arma 2: British Armed Forces" target="_blank"><img height="85" width="70" src="http://www.arma2.com/images/arma2baf.png" alt="buyA2oa-baf-dvdbox"><br /><span class="buy">BUY Arma 2: <acronym title="British Armed Forces">BAF</acronym></span></a></p>
                    <p class="pRight"><a href="http://www.sprocketidea.com/joomla/index.php?page=shop.product_details&flypage=sprocket.tpl&product_id=39&category_id=2&option=com_virtuemart&Itemid=2&vmcchk=1&Itemid=2" title="BUY Arma 2: Private Military Company" target="_blank"><img height="85" width="70" src="http://www.arma2.com/images/arma2pmc.png" alt="buyA2oa-baf-dvdbox"><br /><span class="buy">BUY Arma 2: <acronym title="Private Military Company">PMC</acronym></span></a></p>

<p class="pRight"><a href="http://www.sprocketidea.com/joomla/index.php?http://www.sprocketidea.com/joomla/index.php?page=shop.product_details&flypage=sprocket.tpl&product_id=41&category_id=2&option=com_virtuemart&Itemid=57" title="BUY Arma X" target="_blank"><img height="85" width="70" src="http://www.arma2.com/images/armax.png" alt="buyA2oa-baf-dvdbox"><br /><span class="buy">BUY Arma X</span></a></p>          
              </div>
            </div>            
        </div>
        <div id="ads">
        <h3>Google Ads</h3>
		<script type="text/javascript"><!--
google_ad_client = "ca-pub-4164884610799232";
/* Arma free */
google_ad_slot = "6669402883";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
		</div>
        <div class="footer">
            <a href="http://www.bistudio.com/index.php/english/company/profile" title="Bohemia interactive website" target="_parrent"><img src="images/Bohemia Interactive.png" alt="Bohemia Interactive" /></a> <br />
            Copyright © 2011 Bohemia Interactive.<br /> All Rights Reserved.
    Arma 2™ and Bohemia Interactive™ are trademarks of Bohemia Interactive.
    <a href="privacy.html" title="Privacy statement">Privacy statement</a>
</div>
        <script type="text/javascript">
            $(".btn").mouseover(function() {
                $(this).css("background-position","-100px");
                }).mouseout(function(){
                 $(this).css("background-position","0px");
             });
        </script>
        <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-2014915-16']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    </body>
</html>