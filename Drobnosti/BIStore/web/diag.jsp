<%-- 
    Document   : diag
    Created on : 21.12.2010, 15:32:33
    Author     : Jirka
--%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Diagnostics</title>
    </head>
    <body>
        <h1>Diagnostics</h1>
        <%
            try {
                Class.forName("com.mysql.jdbc.Driver");
                out.println("Mam class");
            } catch (ClassNotFoundException e) {
                out.println("Nemam class");
            }
        %>
        <br>
        Connection: <%

            try {
                Connection c = DriverManager.getConnection("jdbc:mysql://217.16.178.76:3306/BIStore?autoReconnect=true","bistore","0mUvDIFX");
                out.println("Mam connection");
                c.close();
            } catch (SQLException e) {
                out.println("Exception: "+e.toString());
            }
        %>
        <br>
    </body>
</html>
