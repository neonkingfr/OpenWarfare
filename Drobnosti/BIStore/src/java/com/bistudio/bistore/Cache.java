/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.bistore;

import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Jirka
 */
public class Cache
{
    private boolean _unitTest = false;
    // timeout in seconds
    private int _timeout = _unitTest ? 2 : 5 * 60;

    private class CacheItem
    {
        public String _nonce;
        public String _key;
        public Calendar _validUntil;
    };
    private ConcurrentLinkedQueue<CacheItem> _queue;

    private Cache()
    {
        _queue = new ConcurrentLinkedQueue<CacheItem>();
        if (_unitTest) UnitTest();
    }
    private static class CacheHolder
    {
        private static final Cache INSTANCE = new Cache();
    }
    private static Cache getInstance()
    {
        return CacheHolder.INSTANCE;
    }
    private void Add(String nonce, String key)
    {
        Maintain();

        CacheItem item = new CacheItem();
        item._nonce = nonce;
        item._key = key;
        item._validUntil = Calendar.getInstance();
        item._validUntil.add(Calendar.SECOND, _timeout);

        _queue.add(item);

        if (_unitTest) logger().log(Level.INFO, "+ {0}", nonce);
    }
    private String Get(String nonce)
    {
        for (CacheItem item : _queue)
        {
            if (item._nonce.equals(nonce)) return item._key;
        }
        return null;
    }
    private void Maintain()
    {
        // remove old items
        Calendar now = Calendar.getInstance();
        for (CacheItem item : _queue)
        {
            if (item._validUntil.before(now))
            {
                _queue.remove(item);
                if (_unitTest) logger().log(Level.INFO, "- {0}", item._nonce);
            }
        }
        if (_unitTest) logger().log(Level.INFO, "= {0}", _queue.size());
    }

    private void UnitTest()
    {
        Random generator = new Random();
        for (int i=0; true; i++)
        {
            Add(Integer.toString(i), Integer.toString(i));
            int delay = (int)(1000 * _timeout * (0.2 * generator.nextDouble()));
            try
            {
                Thread.sleep(delay);
            }
            catch (InterruptedException ex)
            {
            }
        }
    }
    private Logger logger()
    {
        return Logger.getLogger("BIStore");
    }

    public static void AddKey(HttpServletRequest request, String key)
    {
        // get the nonce from the session object
        HttpSession session = request.getSession(false);
        if (session == null) return;
        String nonce = (String)session.getAttribute("nonce");
        if (nonce != null) getInstance().Add(nonce, key);
    }
    public static String GetKey(String nonce)
    {
        return getInstance().Get(nonce);
    }
 }
