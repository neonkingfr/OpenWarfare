/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.bistore;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.openid4java.OpenIDException;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.InMemoryConsumerAssociationStore;
import org.openid4java.consumer.InMemoryNonceVerifier;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.AuthSuccess;
import org.openid4java.message.MessageException;
import org.openid4java.message.ParameterList;
import org.openid4java.message.ax.AxMessage;
import org.openid4java.message.ax.FetchRequest;
import org.openid4java.message.ax.FetchResponse;

/**
 *
 * @author Jirka
 */
public class GoogleServlet extends HttpServlet
{
    private ServletContext _context;
    private ConsumerManager _manager;

    @Override
    public void init(ServletConfig config)
            throws ServletException
    {
        super.init(config);
        _context = config.getServletContext();

        _manager = new ConsumerManager();
        _manager.setAssociations(new InMemoryConsumerAssociationStore());
        _manager.setNonceVerifier(new InMemoryNonceVerifier(5000));
    }
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException 
    {
        logger().info("Google servlet - request received");
        if ("true".equals(request.getParameter("logged")))
            processLogged(request, response);
        else
            processAuth(request, response);
    }

    private void processAuth(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        try
        {
            // configure the return_to URL where your application will receive
            // the authentication responses from the OpenID provider
            String returnToUrl = request.getRequestURL().toString() + "?logged=true";

            // perform discovery on the user-supplied identifier
            List discoveries = _manager.discover("https://www.google.com/accounts/o8/id");

            // attempt to associate with the OpenID provider
            // and retrieve one service endpoint for authentication
            DiscoveryInformation discovered = _manager.associate(discoveries);

            // store the discovery information in the user's session
            request.getSession().setAttribute("openid-disc", discovered);

            // obtain a AuthRequest message to be sent to the OpenID provider
            AuthRequest authReq = _manager.authenticate(discovered, returnToUrl);

            // Attribute Exchange
            FetchRequest fetch = FetchRequest.createFetchRequest();

            boolean required = true;
            fetch.addAttribute("email", "http://axschema.org/contact/email", required);
            fetch.addAttribute("fullname", "http://axschema.org/namePerson", required);
            fetch.addAttribute("country", "http://axschema.org/contact/country/home", required);
            fetch.addAttribute("first", "http://axschema.org/namePerson/first", required);
            fetch.addAttribute("middle", "http://axschema.org/namePerson/middle", required);
            fetch.addAttribute("last", "http://axschema.org/namePerson/last", required);
            authReq.addExtension(fetch);

            logger().log(Level.INFO, "... redirecting to {0}", authReq.getDestinationUrl(true));

            if (!discovered.isVersion2())
            {
                // Option 1: GET HTTP-redirect to the OpenID Provider endpoint
                // The only method supported in OpenID 1.x
                // redirect-URL usually limited ~2048 bytes
                response.sendRedirect(authReq.getDestinationUrl(true));
            }
            else
            {
                // Option 2: HTML FORM Redirection (Allows payloads >2048 bytes)
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/formredirection.jsp");
                // request.setAttribute("parameterMap", request.getParameterMap());
                request.setAttribute("message", authReq);
                dispatcher.forward(request, response);
            }
        }
        catch (OpenIDException e)
        {
            OpenIDServlet.onError(request, response, getServletContext(), "Registration failed");
        }
    }

    private void processLogged(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {
        VerificationResult verification = verifyResponse(request);
        Identifier identifier = verification.getVerifiedId();
	if (identifier == null)
        {
            OpenIDServlet.onError(request, response, getServletContext(), "Not authentified by the OpenID provider.");
	}
        else
        {
            logger().info("... logged in");

            // create an info for registration
            Database.UserInfo userInfo = new Database.UserInfo();
            userInfo._id = identifier.getIdentifier();

            AuthSuccess authSuccess = (AuthSuccess)verification.getAuthResponse();
            if (authSuccess.hasExtension(AxMessage.OPENID_NS_AX))
            {
                try
                {
                    FetchResponse fetchResp = (FetchResponse) authSuccess.getExtension(AxMessage.OPENID_NS_AX);
                    List values = fetchResp.getAttributeValues("email");
                    if (values.size() > 0) userInfo._email = (String)values.get(0);
                    values = fetchResp.getAttributeValues("fullname");
                    if (values.size() > 0) userInfo._fullName = (String)values.get(0);
                    values = fetchResp.getAttributeValues("country");
                    if (values.size() > 0) userInfo._country = (String)values.get(0);

                    if (userInfo._fullName == null || userInfo._fullName.isEmpty())
                    {
                        String first = null, middle = null, last = null;
                        values = fetchResp.getAttributeValues("first");
                        if (values.size() > 0) first = (String)values.get(0);
                        values = fetchResp.getAttributeValues("middle");
                        if (values.size() > 0) middle = (String)values.get(0);
                        values = fetchResp.getAttributeValues("last");
                        if (values.size() > 0) last = (String)values.get(0);
                        
                        userInfo._fullName = first;
                        if (middle != null && !middle.isEmpty())
                        {
                            if (userInfo._fullName == null || userInfo._fullName.isEmpty()) userInfo._fullName = middle;
                            else userInfo._fullName += " " + middle;
                        }
                        if (last != null && !last.isEmpty())
                        {
                            if (userInfo._fullName == null || userInfo._fullName.isEmpty()) userInfo._fullName = last;
                            else userInfo._fullName += " " + last;
                        }
                    }
                }
                catch (MessageException ex)
                {
                }
            }

            // ask the key
            String key = Database.registerUser(userInfo);
            if (key == null)
                OpenIDServlet.onError(request, response, getServletContext(), "Registration failed");
            else if (key.isEmpty())
                OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: no more keys available");
            else
            {
                Cache.AddKey(request, key);
                OpenIDServlet.showKey(request, response, getServletContext(), key);
            }
        }
    }

    // --- processing the authentication response ---
    private VerificationResult verifyResponse(HttpServletRequest request)
    {
        try
        {
            // extract the parameters from the authentication response
            // (which comes in as a HTTP request from the OpenID provider)
            ParameterList response = new ParameterList(request.getParameterMap());

            // retrieve the previously stored discovery information
            DiscoveryInformation discovered =
                (DiscoveryInformation) request.getSession().getAttribute("openid-disc");

            // extract the receiving URL from the HTTP request
            StringBuffer receivingURL = request.getRequestURL();
            String queryString = request.getQueryString();
            if (queryString != null && queryString.length() > 0)
                receivingURL.append("?").append(request.getQueryString());

            // verify the response; ConsumerManager needs to be the same
            // (static) instance used to place the authentication request
            VerificationResult verification = _manager.verify(receivingURL.toString(), response, discovered);

            // examine the verification result and extract the verified
            // identifier
            return verification;
        }
        catch (OpenIDException e)
        {
            // present error to the user
            return null;
        }
    }

    private Logger logger()
    {
        return Logger.getLogger("BIStore");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Google login handler";
    }// </editor-fold>

}
