/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.bistore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONTokener;
import org.openid4java.OpenIDException;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.InMemoryConsumerAssociationStore;
import org.openid4java.consumer.InMemoryNonceVerifier;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.ParameterList;

/**
 *
 * @author Jirka
 */
public class SteamServlet extends HttpServlet
{
    private ServletContext _context;
    private ConsumerManager _manager;

    private String _appID = "2F5C07CAF9A71DA9636137B74A0DC22A";

    @Override
    public void init(ServletConfig config)
            throws ServletException
    {
        super.init(config);
        _context = config.getServletContext();

        _manager = new ConsumerManager();
        _manager.setAssociations(new InMemoryConsumerAssociationStore());
        _manager.setNonceVerifier(new InMemoryNonceVerifier(5000));
    }



    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        logger().info("Steam servlet - request received");
        if ("true".equals(request.getParameter("logged")))
            processLogged(request, response);
        else
            processAuth(request, response);
    } 

    private void processAuth(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {
        try
        {
            // configure the return_to URL where your application will receive
            // the authentication responses from the OpenID provider
            String returnToUrl = request.getRequestURL().toString() + "?logged=true";

            // perform discovery on the user-supplied identifier
            List discoveries = _manager.discover("http://steamcommunity.com/openid");

            // attempt to associate with the OpenID provider
            // and retrieve one service endpoint for authentication
            DiscoveryInformation discovered = _manager.associate(discoveries);

            // store the discovery information in the user's session
            request.getSession().setAttribute("openid-disc", discovered);

            // obtain a AuthRequest message to be sent to the OpenID provider
            AuthRequest authReq = _manager.authenticate(discovered, returnToUrl);

            logger().log(Level.INFO, "... redirecting to {0}", authReq.getDestinationUrl(true));

            if (!discovered.isVersion2())
            {
                // Option 1: GET HTTP-redirect to the OpenID Provider endpoint
                // The only method supported in OpenID 1.x
                // redirect-URL usually limited ~2048 bytes
                response.sendRedirect(authReq.getDestinationUrl(true));
            }
            else
            {
                // Option 2: HTML FORM Redirection (Allows payloads >2048 bytes)
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/formredirection.jsp");
                // request.setAttribute("parameterMap", request.getParameterMap());
                request.setAttribute("message", authReq);
                dispatcher.forward(request, response);
            }
        }
        catch (OpenIDException e)
        {
            OpenIDServlet.onError(request, response, getServletContext(), "Registration failed");
        }
    }

    private void processLogged(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException
    {
        VerificationResult verification = verifyResponse(request);
        Identifier identifier = verification.getVerifiedId();
	if (identifier == null)
        {
            OpenIDServlet.onError(request, response, getServletContext(), "Not authentified by the OpenID provider.");
	}
        else
        {
            logger().info("... logged in");

            // create an info for registration
            Database.UserInfo userInfo = new Database.UserInfo();
            userInfo._id = identifier.getIdentifier();

            // try to obtain info using Steam Web API
            int index = userInfo._id.lastIndexOf('/');
            if (index >= 0)
            {
                String steamID = userInfo._id.substring(index + 1);
                String url = String.format(
                  "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0001/?key=%s&steamids=%s",
                  _appID, steamID);
                String content = processQuery(url);
                if (content != null && !content.isEmpty())
                {
                    JSONTokener reader = new JSONTokener(content);
                    try
                    {
                        JSONObject root = new JSONObject(reader);
                        if (root != null)
                        {
                            JSONObject item = root.optJSONObject("response");
                            if (item != null)
                            {
                                JSONObject players = item.optJSONObject("players");
                                if (players != null)
                                {
                                    JSONArray playerList = players.getJSONArray("player");
                                    if (playerList != null && playerList.length() > 0)
                                    {
                                        JSONObject player = playerList.optJSONObject(0);
                                        if (player != null)
                                        {
                                            String name = player.optString("realname");
                                            if (name != null && !name.isEmpty()) userInfo._fullName = name;
                                            else
                                            {
                                                name = player.optString("personaname");
                                                if (name != null && !name.isEmpty()) userInfo._fullName = name;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // userInfo._fullName = root.optString("name");
                    }
                    catch (JSONException exception)
                    {
                        // ignore error, authentification was successful
                    }
                }
            }

            // ask the key
            String key = Database.registerUser(userInfo);
            if (key == null)
                OpenIDServlet.onError(request, response, getServletContext(), "Registration failed");
            else if (key.isEmpty())
                OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: no more keys available");
            else
            {
                Cache.AddKey(request, key);
                OpenIDServlet.showKey(request, response, getServletContext(), key);
            }
        }
    }

    // --- processing the authentication response ---
    private VerificationResult verifyResponse(HttpServletRequest request)
    {
        try
        {
            // extract the parameters from the authentication response
            // (which comes in as a HTTP request from the OpenID provider)
            ParameterList response = new ParameterList(request.getParameterMap());

            // retrieve the previously stored discovery information
            DiscoveryInformation discovered =
                (DiscoveryInformation) request.getSession().getAttribute("openid-disc");

            // extract the receiving URL from the HTTP request
            StringBuffer receivingURL = request.getRequestURL();
            String queryString = request.getQueryString();
            if (queryString != null && queryString.length() > 0)
                receivingURL.append("?").append(request.getQueryString());

            // verify the response; ConsumerManager needs to be the same
            // (static) instance used to place the authentication request
            VerificationResult verification = _manager.verify(receivingURL.toString(), response, discovered);

            // examine the verification result and extract the verified
            // identifier
            return verification;
        }
        catch (OpenIDException e)
        {
            // present error to the user
            return null;
        }
    }

    private String processQuery(String strURL)
    {
        try
        {
            // send a GET request
            URL url = new URL(strURL);
            URLConnection connection = url.openConnection();
            // get the response
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) builder.append(line);
            return builder.toString();
        }
        catch (Exception exception)
        {
            // should not happen
            return null;
        }
    }

    private Logger logger()
    {
        return Logger.getLogger("BIStore");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Steam login handler";
    }// </editor-fold>

}
