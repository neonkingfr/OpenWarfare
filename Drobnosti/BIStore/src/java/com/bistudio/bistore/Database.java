/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.bistore;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Jirka
 */
public class Database
{
    static public class UserInfo
    {
        String _id;
        String _fullName;
        String _email;
        String _country;

        public UserInfo() {}
    };
    static public class StatsInfo
    {
        public int _keys = 0;
        public int _users = 0;
    }

    public static StatsInfo getStats()
    {
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;

        try
        {
            connection = getConnection();
            if (connection == null) return null;

            StatsInfo stats = new StatsInfo();
            statement = connection.createStatement();

            // get the number of rows from the CDKeys table
            resultSet = statement.executeQuery("SELECT COUNT(*) FROM CDKeys");
            resultSet.next();
            stats._keys = resultSet.getInt(1);
            resultSet.close();

            // get the number of rows from the Users table
            resultSet = statement.executeQuery("SELECT COUNT(*) FROM Users");
            resultSet.next();
            stats._users = resultSet.getInt(1);
            return stats;
        }
        catch (Exception exception)
        {
            logger().severe("Exception: " + exception.toString());
            return null;
        }
        finally
        {
            if (resultSet != null) try { resultSet.close(); } catch (SQLException ex) {}
            if (statement != null)  try { statement.close(); } catch (SQLException ex) {}
            if (connection != null)  try { connection.close(); } catch (SQLException ex) {}
        }
    }

    public static String registerUser(UserInfo user)
    {
        if (user._id == null) return null;
        if (user._fullName == null) user._fullName = "";
        if (user._email == null) user._email = "";
        if (user._country == null) user._country = "";
        
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;

        try
        {
            connection = getConnection();
            if (connection == null) return null;

            statement = connection.prepareStatement("SELECT Users.Index, FullName, EMail, Country FROM Users WHERE (ID = ?)");
            statement.setString(1, user._id);

            // check if user is already registered
            int index = -1;
            String oldName = null, oldMail = null, oldCountry = null;
            resultSet = statement.executeQuery();
            if (resultSet.next())
            {
                index = resultSet.getInt(1);
                oldName = resultSet.getString(2);
                oldMail = resultSet.getString(3);
                oldCountry = resultSet.getString(4);
            }
            resultSet.close(); resultSet = null;
            statement.close(); statement = null;
            
            if (index < 0)
            {
                // not found, register now
                statement = connection.prepareStatement("INSERT INTO Users (ID, FullName, EMail, Country) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, user._id);
                statement.setString(2, user._fullName);
                statement.setString(3, user._email);
                statement.setString(4, user._country);
                statement.executeUpdate();
                // retrieve the assigned id
                resultSet = statement.getGeneratedKeys();
                if (resultSet.next()) index = resultSet.getInt(1);

                resultSet.close(); resultSet = null;
                statement.close(); statement = null;
            }
            else
            {
                // if possible, update the user info in the database
                statement = connection.prepareStatement("UPDATE Users SET FullName = ?, EMail = ?, Country = ? WHERE (Users.Index = ?)");
                statement.setString(1, GetValueToUpdate(user._fullName, oldName));
                statement.setString(2, GetValueToUpdate(user._email, oldMail));
                statement.setString(3, GetValueToUpdate(user._country, oldCountry));
                statement.setInt(4, index);
                statement.executeUpdate();
                statement.close(); statement = null;
            }

            String key = null;
            if (index >= 0)
            {
                key = "";
                statement = connection.prepareStatement("SELECT CDKey FROM CDKeys WHERE (CDKeys.Index = ?)");
                statement.setInt(1, index);
                resultSet = statement.executeQuery();
                if (resultSet.next())
                    key = resultSet.getString(1);
            }

            return key;
        }
        catch (Exception exception)
        {
            logger().severe("Exception: " + exception.toString());
            return null;
        }
        finally
        {
            if (resultSet != null) try { resultSet.close(); } catch (SQLException ex) {}
            if (statement != null)  try { statement.close(); } catch (SQLException ex) {}
            if (connection != null)  try { connection.close(); } catch (SQLException ex) {}
        }
    }

    private static String GetValueToUpdate(String newValue, String oldValue)
    {
        if (newValue != null && !newValue.isEmpty()) return newValue;
        if (oldValue != null && !oldValue.isEmpty()) return oldValue;
        return "";
    }

    private static Connection getConnection()
    {
        DataSource ds;
        try
        {
            InitialContext ctx = new InitialContext();
            ds = (DataSource)ctx.lookup("java:comp/env/jdbc/BIStore");
        }
        catch (NamingException exception)
        {
            logger().log(Level.SEVERE, "Data source not found", exception);
            return null;
        }
        // several tries to create and check the connection
        int CONNECTION_TRY_COUNT = 3;
        for (int i=0; i<CONNECTION_TRY_COUNT; i++)
        {
            Statement statement = null;
            Connection connection = null;
            
            try
            {
                connection = ds.getConnection();
                // check if connection is valid (connection.isValid is not working)
                statement = connection.createStatement();
                statement.executeUpdate("SET NAMES 'utf8'");
                statement.close();
                // statement.execute("SELECT 1");
                return connection;
            }
            catch (Exception exception)
            {
                if (statement != null)  try { statement.close(); statement = null; } catch (SQLException ex) {}
                if (connection != null)  try { connection.close(); connection = null; } catch (SQLException ex) {}
                String msg = String.format("Connection not valid (PASS {0})", i + 1);
                logger().log(Level.WARNING, msg, exception);
            }
        }

        return null;
    }

    private static Logger logger()
    {
        return Logger.getLogger("BIStore");
    }
}


