/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.bistore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONTokener;

/**
 *
 * @author Jirka
 */
public class WindowsLiveIDServlet extends HttpServlet
{
    // private String _appID = "000000004403FB4A";
    // private String _appSecret = "PVkTh8Z7iIb8ON5FkiNOx6PObjAPccqA";

    private String _appID = "0000000048043C39";
    private String _appSecret = "HJiHW3gLcHfeoMhMexxLDY6VOjFRSzEO";

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String error = request.getParameter("wrap_error_reason");
        if (error == null) error = request.getParameter("error_code");
        if (error != null)
        {
            // invalid name, return to the login page
            logger().info("... error " + error + " - redirecting to original page");
            OpenIDServlet.onError(request, response, getServletContext(), error);
            return;
        }

        String code = request.getParameter("wrap_verification_code");
        if (code != null)
        {
            receiveInfo(code, request, response);
            return;
        }

        authorize(request, response);
    } 

    private void receiveInfo(String code, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        // ask for access token
        String url = "https://consent.live.com/AccessToken.aspx";
        String query = String.format(
            "wrap_client_id=%s&wrap_client_secret=%s&wrap_callback=%s&wrap_verification_code=%s",
            URLEncoder.encode(_appID, "UTF-8"), URLEncoder.encode(_appSecret, "UTF-8"),
            URLEncoder.encode(request.getRequestURL().toString(), "UTF-8"), URLEncoder.encode(code, "UTF-8"));
        logger().info("... asking for access token at " + url);
        String accessToken = getAccessToken(url, query);
        if (accessToken == null || accessToken.isEmpty())
        {
            OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: your account was not authorized");
            return;
        }
        // find the token in response
        String token = null;
        for (String param : accessToken.split("&"))
        {
            String[] pair = param.split("=");
            if (pair.length != 2) continue;
            if ("wrap_access_token".equals(pair[0]))
            {
                token = pair[1];
                break;
            }
        }
        if (token == null || token.isEmpty())
        {
            OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: your account was not authorized");
            return;
        }

        try
        {
            // retrieve the basic info
            String content = processQuery("https://apis.live.net/", token);

            // retrieve the profile
            {
                JSONTokener reader = new JSONTokener(content);
                JSONObject root = new JSONObject(reader);
                JSONObject signedIn = root.getJSONObject("SignedInUser");
                url = signedIn.optString("Uri");
                content = processQuery(url, token);
            }
            
            // extract info from profile
            Database.UserInfo userInfo = new Database.UserInfo();
            {
                JSONTokener reader = new JSONTokener(content);
                JSONObject root = new JSONObject(reader);
                JSONArray entries = root.getJSONArray("Entries");
                if (entries.length() == 0)
                {
                    OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: internal error");
                    return;
                }

                JSONObject entry = entries.getJSONObject(0);
                userInfo._id = entry.optString("Id");
                JSONArray emails = entry.getJSONArray("Emails");
                if (emails.length() > 0)
                {
                   JSONObject email = emails.getJSONObject(0);
                   userInfo._email = email.optString("Address");
                }
            }

            // ask the key
            String key = Database.registerUser(userInfo);
            if (key == null)
                OpenIDServlet.onError(request, response, getServletContext(), "Registration failed");
            else if (key.isEmpty())
                OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: no more keys available");
            else
            {
                Cache.AddKey(request, key);
                OpenIDServlet.showKey(request, response, getServletContext(), key);
            }
        }
        catch (JSONException exception)
        {
            OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: internal error");
        }
    }

    private void authorize(HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        String returnToUrl = request.getRequestURL().toString();
        // String returnToUrl = "http://charlie.bistudio.com:8084/BIStore/WindowsLiveID";
        String url = "https://consent.live.com/Connect.aspx?wrap_client_id=" + _appID +
                "&wrap_callback=" + returnToUrl +
                "&wrap_scope=WL_Profiles.View";
        logger().info("... redirecting to " + url);
        response.sendRedirect(url);
    }

    private String getAccessToken(String strURL, String data)
    {
        try
        {
            // send a POST request
            URL url = new URL(strURL);
            URLConnection connection = url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(data);
            writer.flush();
            writer.close();

            // get the response
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) builder.append(line);
            return builder.toString();
        }
        catch (Exception exception)
        {
            // should not happen
            return null;
        }
    }

    private String processQuery(String strURL, String accessToken)
    {
        try
        {
            // send a GET request
            URL url = new URL(strURL);
            URLConnection connection = url.openConnection();
            connection.setUseCaches(false);
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", "WRAP access_token=" + accessToken);

            // get the response
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) builder.append(line);
            return builder.toString();
        }
        catch (Exception exception)
        {
            // should not happen
            return null;
        }
    }

    private Logger logger()
    {
        return Logger.getLogger("BIStore");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        logger().info("Windows Live ID servlet - GET request received");
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        logger().info("Windows Live ID servlet - POST request received");
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Windows Live ID login handler";
    }// </editor-fold>

}
