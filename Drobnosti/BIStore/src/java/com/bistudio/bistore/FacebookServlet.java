/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bistudio.bistore;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.json.JSONTokener;

/**
 *
 * @author Jirka
 */
public class FacebookServlet extends HttpServlet
{
    private String _appID = "126163514110966";
    private String _appSecret = "fbfddb383757ccc7b75aa00e3fb6aedd";

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        // handle errors
        String error = request.getParameter("error_description");
        if (error == null) error = request.getParameter("error_reason");
        if (error == null) error = request.getParameter("error");
        if (error != null)
        {
            OpenIDServlet.onError(request, response, getServletContext(), error);
            return;
        }

        String code = request.getParameter("code");
        if (code != null)
        {
            receiveInfo(code, request, response);
            return;
        }

        authorize(request, response);
    }

    private void receiveInfo(String code, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        // ask for access token
        String url = String.format(
            "https://graph.facebook.com/oauth/access_token?client_id=%s&redirect_uri=%s&client_secret=%s&code=%s",
            _appID, request.getRequestURL().toString(), _appSecret, code);
        logger().info("... asking for access token at " + url);
        String accessToken = processQuery(url);
        if (accessToken == null || accessToken.isEmpty())
        {
            OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: your account was not authorized");
            return;
        }

        url = "https://graph.facebook.com/me?" + accessToken;
        logger().info("... asking for info at " + url);
        String info = processQuery(url);
        if (info == null || info.isEmpty())
        {
            OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: your account was not authorized");
            return;
        }

        JSONTokener reader = new JSONTokener(info);
        try
        {
            JSONObject root = new JSONObject(reader);

            // create an info for registration
            Database.UserInfo userInfo = new Database.UserInfo();
            userInfo._id = root.optString("link");
            if (userInfo._id == null || userInfo._id.isEmpty())
            {
                OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: unique identifier not found");
                return;
            }
            userInfo._fullName = root.optString("name");
            userInfo._email = "";
            userInfo._country = root.optString("locale");

            // ask the key
            String key = Database.registerUser(userInfo);
            if (key == null)
                OpenIDServlet.onError(request, response, getServletContext(), "Registration failed");
            else if (key.isEmpty())
                OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: no more keys available");
            else
            {
                Cache.AddKey(request, key);
                OpenIDServlet.showKey(request, response, getServletContext(), key);
            }
        }
        catch (JSONException exception)
        {
            OpenIDServlet.onError(request, response, getServletContext(), "Registration failed: internal error");
        }
    }

    private void authorize(HttpServletRequest request, HttpServletResponse response)
            throws IOException
    {
        String returnToUrl = request.getRequestURL().toString();
        String url = String.format(
            "https://graph.facebook.com/oauth/authorize?client_id=%s&redirect_uri=%s&type=web_server",
            _appID, returnToUrl);
        logger().info("... redirecting to " + url);
        response.sendRedirect(url);
    }

    private String processQuery(String strURL)
    {
        try
        {
            // send a GET request
            URL url = new URL(strURL);
            URLConnection connection = url.openConnection();
            // get the response
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) builder.append(line);
            return builder.toString();
        }
        catch (Exception exception)
        {
            // should not happen
            return null;
        }
    }

    private Logger logger()
    {
        return Logger.getLogger("BIStore");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException 
    {
       logger().info("Facebook servlet - GET request received");
       processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        logger().info("Facebook servlet - POST request received");
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo()
    {
        return "Facebook login handler";
    }// </editor-fold>

}
