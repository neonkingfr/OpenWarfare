/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bistudio.ComTrans;

import com.unboundid.ldap.sdk.BindResult;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPConnectionOptions;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchScope;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;
import javax.faces.application.Application;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;
import javax.faces.model.DataModel;
import javax.net.ssl.SSLSocketFactory;

/**
 *
 * @author Jirka
 */
@ManagedBean(name = "sessionBean")
@SessionScoped
public class SessionBean 
{
    // Login properties
    private String name;
    public String getName() {return name;}
    public void setName(String value) {name = value;}

    private String password;
    public String getPassword() {return password;}
    public void setPassword(String value) {password = value;}

    private String errorMsg;
    public String getErrorMsg() {return errorMsg;}
    public void setErrorMsg(String value) {errorMsg = value;}
    
    public String login()
    {
        LDAPConnectionOptions ops = new LDAPConnectionOptions();
        ops.setAutoReconnect(true);
        ops.setBindWithDNRequiresPassword(true);
        ops.setResponseTimeoutMillis(0);
        ops.setUseKeepAlive(true);
        ops.setUseTCPNoDelay(true);

        ApplicationBean app = getApplicationBean();
        SSLSocketFactory socketFactory = app.getSocketFactory();

        String host = app.getLdapHost();
        int port = app.getLdapPort();
        String userMask = app.getLdapUserMask();
        String groupDN = app.getLdapGroupDN();
        String userDN = String.format(userMask, name);

        try
        {
            LDAPConnection c = new LDAPConnection(socketFactory, ops);
            c.connect(host, port);
            BindResult ok = c.bind(userDN, password);
            if (ok.getResultCode() != ResultCode.SUCCESS)
            {
                errorMsg = "Login failed, wrong name or password";
                return "fail";
            }
            
            SearchResult isInGroup = c.search(groupDN, SearchScope.SUB,
                    "(member=" + userDN + ")", "member");

            if (isInGroup.getEntryCount() == 0)
            {
                errorMsg = "Login failed, no access rights";
                return "fail";
            }
            
            return "ok";
        }
        catch (LDAPException ex)
        {
            errorMsg = ex.getMessage();
            return "fail";
        }
    }

    // Main form properties

    public class Language
    {
        private int id;
        public int getId() {return id;}
        public void setId(int value) {id = value;}

        private String name;
        public String getName() {return name;}
        public void setName(String value) {name = value;}
        
        public Language(int id, String name)
        {
            this.id = id; this.name = name;
        }
    }
    private ArrayList<Language> languages = new ArrayList<Language>();
    public ArrayList<Language> getLanguages() {return languages;}
    public void setLanguages(ArrayList<Language> value) {languages = value;}
    private void loadLanguages()
    {
        languages.clear();
        
        ApplicationBean app = getApplicationBean();
        Connection connection = null;
        try
        {
            connection = app.openConnection();

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT LanguageID, Name FROM Languages");
            while (resultSet.next())
            {
                languages.add(new Language(resultSet.getInt(1), resultSet.getString(2)));
            }
            resultSet.close();
            statement.close();
        }
        catch (Exception exception)
        {
            Logger.getGlobal().severe(exception.toString());
        }
        finally
        {
            app.closeConnection(connection);
        }
    }

    // Left panel

    private Integer translateToCurrent = null;
    public Integer getTranslateToCurrent() {return translateToCurrent;}
    public void setTranslateToCurrent(Integer value) {translateToCurrent = value;}
    public void onTranslateToSelected()
    {
        // if selection did not change, update only translation (different language)
        if (!loadStrings(null))
            updateTranslation();
    }
    
    private Boolean showTranslated;
    public Boolean getShowTranslated() {return showTranslated;}
    public final void setShowTranslated(Boolean value) {showTranslated = value;}
    public void onShowTranslatedChanged()
    {
        loadStrings(null);
    }

    public class Project
    {
        private int id;
        public int getId() {return id;}
        public void setId(int value) {id = value;}

        private String name;
        public String getName() {return name;}
        public void setName(String value) {name = value;}
        
        public Project(int id, String name)
        {
            this.id = id; this.name = name;
        }
    }
    private ArrayList<Project> projects = new ArrayList<Project>();
    public ArrayList<Project> getProjects() {return projects;}
    public void setProjects(ArrayList<Project> value) {projects = value;}
    private Integer projectsCurrent = null;
    public Integer getProjectsCurrent() {return projectsCurrent;}
    public void setProjectsCurrent(Integer value) {projectsCurrent = value;}
    public void onProjectSelected()
    {
        loadStrings(null);
    }
    private final void loadProjects()
    {
        projects.clear();
        
        ApplicationBean app = getApplicationBean();
        Connection connection = null;
        try
        {
            connection = app.openConnection();

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT ProjectID, Name FROM Projects");
            while (resultSet.next())
            {
                projects.add(new Project(resultSet.getInt(1), resultSet.getString(2)));
            }
            resultSet.close();
            statement.close();
        }
        catch (Exception exception)
        {
            Logger.getGlobal().severe(exception.toString());
        }
        finally
        {
            app.closeConnection(connection);
        }
    }
    
    public class Strings extends DataModel<Strings.Item>
    {
        public class Item
        {
            private int id;
            public int getId() {return id;}
            public void setId(int value) {id = value;}
            
            private String text;
            public String getText() {return text;}
            public void setText(String value) {text = value;}

            public Item(int id, String text)
            {
                this.id = id;
                this.text = text;
            }
        }
        private ArrayList<Item> items = new ArrayList<Item>();

        private void clear()
        {
            items.clear();
        }
        private void add(int id, String text)
        {
            items.add(new Item(id, text));
        }
        private int findIndex(Integer id) 
        {
            for (int i=0; i<items.size(); i++)
            {
                if (items.get(i).id == id) return i;
            }
            return -1;
        }

        // DataModel implementation
        private int _currentRow = -1;
        @Override
        public boolean isRowAvailable() 
        {
            return _currentRow >= 0 && _currentRow < items.size();
        }
        @Override
        public int getRowCount() 
        {
            return items.size();
        }
        @Override
        public Item getRowData() 
        {
            if (_currentRow < 0) return null;
            return items.get(_currentRow);
        }
        @Override
        public int getRowIndex() 
        {
            return _currentRow;
        }
        @Override
        public void setRowIndex(int rowIndex) 
        {
            _currentRow = rowIndex;
        }
        @Override
        public Object getWrappedData() 
        {
            return null;
        }
        @Override
        public void setWrappedData(Object data) 
        {
        }
    }
    private Strings strings = new Strings();
    public Strings getStrings() {return strings;}

    private Collection<Object> _selection = null;
    public Collection<Object> getStringsCurrent() {return _selection;}
    public final void setStringsCurrent(Collection<Object> value) {_selection = value;}
    public Integer getSelectedString()
    {
        if (_selection == null) return null;
        
        for (Object obj : _selection)
        {
            Integer index = (Integer)obj;
            Strings.Item item = strings.items.get(index);
            return item.id;
        }
        return null;
    }

    private final boolean loadStrings(Integer select)
    {
        Integer selectedBefore = getSelectedString();
        if (select == null) select = selectedBefore;
        
        // read from database
        strings.clear();
        
        if (projectsCurrent != null)
        {
            ApplicationBean app = getApplicationBean();
            Connection connection = null;
            try
            {
                connection = app.openConnection();

                PreparedStatement statement = null;
                if (showTranslated != null && showTranslated)
                {
                    statement = connection.prepareStatement("SELECT StringID, Name FROM Strings WHERE (ProjectID = ?)");
                    statement.setInt(1, projectsCurrent);
                }
                else
                {
                    statement = connection.prepareStatement("SELECT S.StringID, S.Name FROM (SELECT * FROM Strings WHERE ProjectID = ?) AS S LEFT JOIN Translations AS T ON T.StringID = S.StringID AND T.LanguageID = ? WHERE T.StringID IS NULL");
                    statement.setInt(1, projectsCurrent);
                    if (translateToCurrent != null) statement.setInt(2, translateToCurrent);
                    else statement.setInt(2, 0);
                }

                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next())
                {
                    strings.add(resultSet.getInt(1), resultSet.getString(2));
                }
                resultSet.close();
                statement.close();
            }
            catch (Exception exception)
            {
                Logger.getGlobal().severe(exception.toString());
            }
            finally
            {
                app.closeConnection(connection);
            }
        }

        if (strings.items.isEmpty())
        {
            setStringsCurrent(null);
            if (selectedBefore != null)
            {
                stringChanged(null);
                return true;
            }
            return false;
        }
        
        // if the wanted selection not present, select the first item
        int index = 0;
        if (select != null)
        {
            index = strings.findIndex(select);
            if (index < 0) index = 0;
        }
        Strings.Item item = strings.items.get(index);
        select = item.id;
        
        ArrayList<Object> selection = new ArrayList<Object>();
        selection.add(index);
        setStringsCurrent(selection);
        
        if (!select.equals(selectedBefore))
        {
            stringChanged(item);
            return true;
        }
        return false;
    }

    public void onStringSelected()
    {
        if (_selection.isEmpty())
        {
            stringChanged(null);
            return;  
        }
        for (Object index : _selection)
        {
            Integer i = (Integer)index;
            Strings.Item item = strings.items.get(i);
            stringChanged(item);
        }
    }

    private String stringToAdd;
    public String getStringToAdd() {return stringToAdd;}
    public void setStringToAdd(String value) {stringToAdd = value;}

    public void onAddString()
    {
        if (stringToAdd == null || stringToAdd.isEmpty()) return;
        if (projectsCurrent == null) return;
        
        // INSERT new string into database
        ApplicationBean app = getApplicationBean();
        Connection connection = null;
        Integer id = null;
        try
        {
            connection = app.openConnection();

            PreparedStatement statement = connection.prepareStatement("INSERT IGNORE INTO Strings (ProjectID, Name) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, projectsCurrent);
            statement.setString(2, stringToAdd);

            statement.executeUpdate();

            // retrieve the assigned id
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) id = resultSet.getInt(1);
            resultSet.close();

            statement.close();
        }
        catch (Exception exception)
        {
            Logger.getGlobal().severe(exception.toString());
        }
        finally
        {
            app.closeConnection(connection);
        }
        
        if (id != null) loadStrings(id);
    }
    
    // Right panel

    private static String str_no_selection = "No string selected";
    
    private final void stringChanged(Strings.Item item) 
    {
        string = item;
        updateSource();
        updateTranslation();
        updateTags();
    }

    private Strings.Item string = null;
    public String getString() 
    {
        if (string == null) return str_no_selection;
        return string.text;
    }

    private Integer translateFromCurrent = null;
    public Integer getTranslateFromCurrent() {return translateFromCurrent;}
    public void setTranslateFromCurrent(Integer value) {translateFromCurrent = value;}
    public void onTranslateFromSelected()
    {
        updateSource();
    }

    private String source;
    public String getSource() {return source;}
    public void setSource(String value) {source = value;}
    public void updateSource()
    {
        if (string == null || translateFromCurrent == null)
        {
            source = "";
            return;
        }
        
        source = findLastText(string.id, translateFromCurrent);
        if (source == null) source = "";
    }

    private String translation;
    public String getTranslation() {return translation;}
    public void setTranslation(String value) {translation = value;}
    public void updateTranslation()
    {
        if (string == null || translateToCurrent == null)
        {
            translation = "";
            return;
        }
        
        translation = findLastText(string.id, translateToCurrent);
        if (translation == null) translation = "";
    }
    public void onSendTranslation()
    {
        if (string == null || translateToCurrent == null) return;
        if (translation == null) return;
        
        // INSERT new translation into database
        ApplicationBean app = getApplicationBean();
        Connection connection = null;
        try
        {
            connection = app.openConnection();

            PreparedStatement statement = connection.prepareStatement("INSERT IGNORE INTO Translations (StringID, LanguageID, Time, Sender, Text) VALUES (?, ?, NOW(), ?, ?)");
            statement.setInt(1, string.id);
            statement.setInt(2, translateToCurrent);
            statement.setString(3, name);
            statement.setString(4, translation);

            statement.executeUpdate();

            statement.close();
        }
        catch (Exception exception)
        {
            Logger.getGlobal().severe(exception.toString());
        }
        finally
        {
            app.closeConnection(connection);
        }
        
        stringChanged(string);
    }
        
    private String tags;
    public String getTags() {return tags;}
    public void setTags(String value) {tags = value;}
    private void updateTags()
    {
        tags = "";

        if (string == null) return;
        
        ApplicationBean app = getApplicationBean();
        Connection connection = null;
        try
        {
            connection = app.openConnection();

            PreparedStatement statement = connection.prepareStatement("SELECT Tag FROM Tags WHERE StringID = ?");
            statement.setInt(1, string.id);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                String tag = resultSet.getString(1).trim();
                if (!tag.isEmpty())
                {
                    if (!tags.isEmpty()) tags += ", ";
                    tags += tag;
                }
            }
            resultSet.close();
            statement.close();
            
        }
        catch (Exception exception)
        {
            Logger.getGlobal().severe(exception.toString());
        }
        finally
        {
            app.closeConnection(connection);
        }
    }
    public void onUpdateTags()
    {
        if (string == null) return;
        if (tags == null) return;
        String[] values = tags.split(",");
        
        // replace tags in database
        ApplicationBean app = getApplicationBean();
        Connection connection = null;
        try
        {
            connection = app.openConnection();

            // DELETE all old tags
            PreparedStatement statement = connection.prepareStatement("DELETE FROM Tags WHERE (StringID = ?)");
            statement.setInt(1, string.id);
            statement.executeUpdate();
            statement.close();
        
            // INSERT tags
            statement = connection.prepareStatement("INSERT IGNORE INTO Tags (StringID, Tag) VALUES (?, ?)");
            statement.setInt(1, string.id);
            for (int i=0; i<values.length; i++)
            {
                String tag = values[i].trim().toLowerCase();
                if (!tag.isEmpty())
                {
                    statement.setString(2, tag);
                    statement.addBatch();
                }
            }
            statement.executeBatch();
            statement.close();
        }
        catch (Exception exception)
        {
            Logger.getGlobal().severe(exception.toString());
        }
        finally
        {
            app.closeConnection(connection);
        }
        
        updateTags();
    }
    
    private String findLastText(Integer sel, Integer lang) 
    {
        ApplicationBean app = getApplicationBean();
        Connection connection = null;
        try
        {
            connection = app.openConnection();

            PreparedStatement statement = connection.prepareStatement("SELECT Time, Text FROM Translations WHERE (StringID = ? AND LanguageID = ?)");
            statement.setInt(1, sel);
            statement.setInt(2, lang);

            String text = null;
            Timestamp latest = null;
            
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                Timestamp time = resultSet.getTimestamp(1);
                if (latest == null || time.after(latest))
                {
                    latest = time;
                    text = resultSet.getString(2);
                }
            }
            resultSet.close();
            statement.close();
            
            return text;
        }
        catch (Exception exception)
        {
            Logger.getGlobal().severe(exception.toString());
            return null;
        }
        finally
        {
            app.closeConnection(connection);
        }
    }

    /** Creates a new instance of SessionBean */
    public SessionBean() 
    {
        loadLanguages();
        if (!languages.isEmpty())
        {
            translateToCurrent = languages.get(0).id;
            translateFromCurrent = languages.get(0).id;
        }
        
        setShowTranslated(true);
        
        loadProjects();
        if (!projects.isEmpty())
        {
            projectsCurrent = projects.get(0).id;
        }
        
        loadStrings(null);
    }

    private ApplicationBean getApplicationBean() 
    {
        FacesContext context = FacesContext.getCurrentInstance();   
        Application app = context.getApplication();   
        ValueBinding binding = app.createValueBinding("#{applicationBean}");   
        Object value = binding.getValue(context);   
        return (ApplicationBean)value;   
    }
}
