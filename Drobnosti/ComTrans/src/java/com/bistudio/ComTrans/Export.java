/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bistudio.ComTrans;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 *
 * @author Jirka
 */
public class Export extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        String parameter = request.getParameter("ProjectID");
        if (parameter == null) return;
        
        int projectID = Integer.parseInt(parameter);

        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        Connection connection = null;
        try
        {
            connection = openConnection();

            // Read the list of languages
            ArrayList<Integer> languages = new ArrayList<Integer>();
            int maxLangID = 0;
            String langList = "Language";
            
            {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT LanguageID, Name FROM Languages");
                while (resultSet.next())
                {
                    langList += "," + resultSet.getString(2);
                    int id = resultSet.getInt(1);
                    languages.add(id);
                    if (id > maxLangID) maxLangID = id;
                }
                resultSet.close();
                statement.close();
            }

            // Create the list of strings
            HashMap<Integer, StringInfo> strings = new HashMap<Integer, StringInfo>();
            {
                PreparedStatement statement = connection.prepareStatement(
                        "SELECT StringID, Name FROM Strings WHERE ProjectID = ? ORDER BY StringID");
                statement.setInt(1, projectID);

                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next())
                {
                    int id = resultSet.getInt(1);
                    String name = resultSet.getString(2);
                    strings.put(id, new StringInfo(name, maxLangID + 1));
                }
                resultSet.close();
                statement.close();
            }
            
            // Collect all related translations
            {
                PreparedStatement statement = connection.prepareStatement(
                        "SELECT Strings.StringID, LanguageID, Time, Text FROM Strings, Translations WHERE Strings.StringID = Translations.StringID AND ProjectID = ? ORDER BY Strings.StringID");
                statement.setInt(1, projectID);

                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next())
                {
                    int id = resultSet.getInt(1);
                    StringInfo s = strings.get(id);
                    if (s == null) continue;
                    
                    int language = resultSet.getInt(2);
                    Timestamp time = resultSet.getTimestamp(3);
                    
                    TranslationInfo item = s.translations[language];
                    if (item == null)
                    {
                        item = new TranslationInfo();
                        s.translations[language] = item;
                    }
                    
                    if (item.latest == null || time.after(item.latest))
                    {
                        item.latest = time;
                        item.text = resultSet.getString(4);
                    }
                }
                resultSet.close();
                statement.close();
            }

            // Create the output
            
            // The first line - list of languages
            out.println(langList);
            
            // The translations
            for (StringInfo s : strings.values())
            {
                String line = s.name;
                for (int j=0; j<languages.size(); j++)
                {
                    TranslationInfo item = s.translations[languages.get(j)];
                    String text = "";
                    if (item != null)
                    {
                        text = item.text;
                        // encode string if needed
                        if (text.indexOf(',') >= 0 || text.endsWith("\\"))
                        {
                            text = "\"" + text.replaceAll("\"", "\"\"") + "\"";
                        }
                    }
                    line += "," + text;
                }
                out.println(line);
            }
        }
        catch (Exception exception)
        {
            Logger.getGlobal().severe(exception.toString());
        }
        finally
        {
            out.close();
            closeConnection(connection);
        }
    }
    private class TranslationInfo
    {
        private Timestamp latest = null;
        private String text = null;
    }
    private class StringInfo
    {
        private String name;
        private TranslationInfo[] translations;

        private StringInfo(String name, int size)
        {
            this.name = name;
            translations = new TranslationInfo[size];
        }
    }
    
    private Connection openConnection() throws SQLException, NamingException
    {
        InitialContext ctx = new InitialContext();
        DataSource dataSource = (DataSource)ctx.lookup("java:comp/env/jdbc/ComTrans");
        return dataSource.getConnection();
    }
    private void closeConnection(Connection connection)
    {
        try
        {
            if (connection != null) connection.close();
        }
        catch (SQLException ex)
        {
            // cannot close, connection is probably no longer valid, ignore
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() 
    {
        return "Export strings related to product to stringtable";
    }// </editor-fold>
}
