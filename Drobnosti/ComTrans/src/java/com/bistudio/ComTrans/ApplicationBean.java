/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bistudio.ComTrans;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.sql.DataSource;

/**
 *
 * @author Jirka
 */
@ManagedBean(name = "applicationBean")
@ApplicationScoped
public class ApplicationBean 
{
    private SSLSocketFactory _socketFactory = null;
    public SSLSocketFactory getSocketFactory() {return _socketFactory;}
    
    private String ldapHost;
    public String getLdapHost() {return ldapHost;}
    private int ldapPort;
    public int getLdapPort() {return ldapPort;}
    private String ldapUserMask;
    public String getLdapUserMask() {return ldapUserMask;}
    private String ldapGroupDN;
    public String getLdapGroupDN() {return ldapGroupDN;}

    private DataSource _dataSource = null;
    public Connection openConnection() throws SQLException
    {
        return _dataSource.getConnection();
    }
    public void closeConnection(Connection connection)
    {
        try
        {
            if (connection != null) connection.close();
        }
        catch (SQLException ex)
        {
            // cannot close, connection is probably no longer valid, ignore
        }
    }

    /** Creates a new instance of ApplicationBean */
    public ApplicationBean() throws IOException, GeneralSecurityException, NamingException
    {
        // load properties from the context.xml
        InitialContext ctx = new InitialContext();
        ldapHost = (String)ctx.lookup("java:comp/env/ldapHost");
        ldapPort = (Integer)ctx.lookup("java:comp/env/ldapPort");
        ldapUserMask = (String)ctx.lookup("java:comp/env/ldapUserMask");
        ldapGroupDN = (String)ctx.lookup("java:comp/env/ldapGroupDN");

        _dataSource = (DataSource)ctx.lookup("java:comp/env/jdbc/ComTrans");

        // initialize the socket factory to trust our web
        CustomKeyManager[] manager = new CustomKeyManager[1];
        manager[0] = new CustomKeyManager();
        manager[0].add(getTrustManagers("trusted_of_ca.jks"));

        SSLContext sslcx = SSLContext.getInstance("SSLv3");
        sslcx.init(null, manager, new SecureRandom());

        _socketFactory = sslcx.getSocketFactory();
    }

    private TrustManager[] getTrustManagers(String filename) throws IOException, GeneralSecurityException
    {
        // read the certificate to enable SSL connection
        InputStream stream = null;
        try
        {
            stream = ApplicationBean.class.getResourceAsStream(filename);

            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(stream, "changeit".toCharArray());

            TrustManagerFactory tmf = TrustManagerFactory.getInstance("PKIX");
            tmf.init(ks);

            // SSLContext sslcx = SSLContext.getInstance("TLS");
            return tmf.getTrustManagers(); 
        }
        finally
        {
            if (stream != null)
            {
                try
                {
                    stream.close();
                }
                catch (IOException ex) {}
            }
        }
    }
    private final class CustomKeyManager implements X509TrustManager
    {
        ArrayList<X509TrustManager> _submanagers = new ArrayList<X509TrustManager>();

        public void add(TrustManager[] mans) 
        {
            for (TrustManager man : mans)
            {
                if (man instanceof X509TrustManager) _submanagers.add((X509TrustManager)man);
            }
        }
        
        public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException 
        {
            CertificateException last = null;
            for (X509TrustManager man : _submanagers)
            {
                try
                {
                    man.checkClientTrusted(arg0, arg1);
                    return;
                }
                catch (CertificateException ex)
                {
                    last = ex;
                }
            }
            // all managers failed to check
            throw last;
        }
        public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException 
        {
            CertificateException last = null;
            for (X509TrustManager man : _submanagers)
            {
                try
                {
                    man.checkServerTrusted(arg0, arg1);
                    Logger.getGlobal().info("Certificate " + CertName(arg0) + " is trusted");
                    return;
                }
                catch (CertificateException ex)
                {
                    last = ex;
                }
            }
            // all managers failed to check
            Logger.getGlobal().severe("Certificate " + CertName(arg0) + " is not trusted");
            
            throw last;
        }
        public X509Certificate[] getAcceptedIssuers() 
        {
            ArrayList<X509Certificate> list = new ArrayList<X509Certificate>();
            for (X509TrustManager man : _submanagers) Collections.addAll(list, man.getAcceptedIssuers());
            return list.toArray(new X509Certificate[0]);
        }
        private String CertName(X509Certificate[] cert) 
        {
            String result = "";
            for (X509Certificate c : cert)
            {
                if (!result.isEmpty()) result += " *** ";
                result += c.getSubjectDN();
            }
            return result;
        }
    }
}
