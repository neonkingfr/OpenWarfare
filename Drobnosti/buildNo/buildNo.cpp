#include <stdio.h>
#include <limits.h>
#include <string.h>

int main(int argc, const char *argv[])
{
  const char *pattern="#define BUILD_NO %d";
  const char *filename = "buildNo.h";
  
  bool incrementVersion = true;
  while (argc>1 && *argv[1]=='-' )
  {
    if (_strcmpi(argv[1], "-n") == 0) incrementVersion = false;
    argc--;
    argv++;
  }
  if (argc>1)
  {
    filename = argv[1];
  }
  
  FILE *file;
  if (incrementVersion)
  {
    file = fopen(filename,"r+");
  }
  else
  {
    file = fopen(filename,"r");
  }

  if (!file)
  {
    // failed - does not exist or locked
    return -1;
  }
  
  int ret = -1;
  // file should contain something like
  // #define BUILD_NO 1452
  int version = 0;
  if (fscanf(file,pattern,&version)==1)
  {
    // result is always longer than original version
    // unless there are negative numbers or overflow
    if (version>=0 && version<INT_MAX)
    {
      // return original version number
      ret = version;

      if (incrementVersion)
      {
        version++;
        fseek(file,0,SEEK_SET);
        fprintf(file,pattern,version);
        fprintf(file,"\n");
      }
      
      // return version number
    }
  }
  
  
  if (fclose(file)<0)
  {
    ret = -1;
  }
  
  return ret;
}

