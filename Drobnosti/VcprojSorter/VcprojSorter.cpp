// VcprojSorter2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <string>

#include <conio.h>

#include "tinyxml/tinyxml.h"

class VcprojSorter
{
public:
  VcprojSorter(TiXmlDocument& inDoc, TiXmlDocument& sortDoc)
    : m_inDoc(inDoc), m_sortDoc(sortDoc) {}

  bool Sort();
private:
  void ProcessNode(TiXmlNode& inNode);
  void ProcessConfigurations(TiXmlNode& inElement);
  void ProcessFiles(TiXmlNode& inElement);
  void ProcessFile(TiXmlNode& inNode, TiXmlNode& sortNode, TiXmlNode& outNode);
  void ProcessFilter(TiXmlNode& inNode, TiXmlNode& sortNode, TiXmlNode& outNode);

  TiXmlElement* FindElement(TiXmlNode& startNode, const std::string& name);
  TiXmlElement* FindElementByAttrib(TiXmlNode& startNode, const std::string& attribName, const std::string& attribValue);

  bool m_error;

  /// input document
  TiXmlDocument& m_inDoc;
  /// document that is used to sort inDoc
  TiXmlDocument& m_sortDoc;
};

typedef std::vector<TiXmlNode* > TNodeList;

TiXmlElement* VcprojSorter::FindElement(TiXmlNode& startNode, const std::string& name)
{
  if (startNode.Type() == TiXmlNode::TINYXML_ELEMENT)
  {
    TiXmlElement* element = startNode.ToElement();
    if (element && element->ValueStr() == name)
      return element;
  }

  for (TiXmlNode *child = startNode.FirstChild(); child != 0; child = child->NextSibling())
  {
    TiXmlElement* element = FindElement(*child, name);
    if (element)
      return element;
  }  

  return 0;
}

TiXmlElement* VcprojSorter::FindElementByAttrib(TiXmlNode& startNode, const std::string& attribName, const std::string& attribValue)
{
  if (startNode.Type() == TiXmlNode::TINYXML_ELEMENT)
  {
    TiXmlElement* element = startNode.ToElement();
    if (element)
    {
      const std::string* res = element->Attribute(attribName);
      if (res && attribValue == *res)
        return element;
    }
  }

  for (TiXmlNode *child = startNode.FirstChild(); child != 0; child = child->NextSibling())
  {
    TiXmlElement* element = FindElementByAttrib(*child, attribName, attribValue);
    if (element)
      return element;
  }  

  return 0;
}

bool VcprojSorter::Sort()
{
  m_error = false;
  ProcessNode(m_inDoc);

  return !m_error;
}

void VcprojSorter::ProcessNode(TiXmlNode& inNode)
{
  bool processed = false;
  if (inNode.Type() == TiXmlNode::TINYXML_ELEMENT)
  {
    TiXmlElement* element = inNode.ToElement();
    if (element)
    {
      if (element->ValueStr() == "Configurations")
      {
        ProcessConfigurations(*element);
        processed = true;
      }
      if (element->ValueStr() == "Files")
      {
        ProcessFiles(*element);
        processed = true;
      }
    }
  }

  if (!processed)
  {
    for (TiXmlNode *child = inNode.FirstChild(); child != 0; child = child->NextSibling())
    {
      ProcessNode(*child);
    }
  }
}


void VcprojSorter::ProcessConfigurations(TiXmlNode& inElement)
{
  TiXmlElement* sortConfigs = FindElement(m_sortDoc, "Configurations");
  assert(sortConfigs);
  if (!sortConfigs)
  {
    m_error = true;
    return;
  }

  TiXmlElement newConfigs(*sortConfigs);
  newConfigs.Clear();

  std::set<TiXmlNode*> added;
  for (TiXmlNode *child = sortConfigs->FirstChild(); child != 0; child = child->NextSibling())
  {
    assert(child->Type() == TiXmlNode::TINYXML_ELEMENT);
    if (child->Type() == TiXmlNode::TINYXML_ELEMENT)
    {
      TiXmlElement* element = child->ToElement();
      assert(element);
      if (!element)
      {
        m_error = true;
        continue;
      }

      const std::string* configName = element->Attribute(std::string("Name"));
      assert(configName);
      if (!configName)
      {
        m_error = true;
        continue;
      }
      
      TiXmlElement* foundEl = FindElementByAttrib(inElement, "Name", *configName);
      if (!foundEl)
        continue; //not found
      
      added.insert(foundEl);
      newConfigs.InsertEndChild(*foundEl);
    }
    else
    {
      m_error = true;
    }
  }

  // add rest
  for (TiXmlNode *child = inElement.FirstChild(); child != 0; child = child->NextSibling())
  {
    if (added.find(child) == added.end())
    {
      newConfigs.InsertEndChild(*child);
    }
  }

  // replace
  inElement.Clear();
  for (TiXmlNode *child = newConfigs.FirstChild(); child != 0; child = child->NextSibling())
  {
    inElement.InsertEndChild(*child);
  }
}

void VcprojSorter::ProcessFile(TiXmlNode& inNode, TiXmlNode& sortNode, TiXmlNode& outNode)
{
  outNode.Clear();
  std::set<TiXmlNode*> added;
  for (TiXmlNode *child = sortNode.FirstChild(); child != 0; child = child->NextSibling())
  {
    assert(child->Type() == TiXmlNode::TINYXML_ELEMENT);
    if (child->Type() == TiXmlNode::TINYXML_ELEMENT)
    {
      TiXmlElement* element = child->ToElement();
      assert(element);
      if (!element)
      {
        m_error = true;
        continue;
      }

      const std::string* name = element->Attribute(std::string("Name"));
      assert(name);
      if (!name)
      {
        m_error = true;
        continue;
      }

      TiXmlElement* foundEl = FindElementByAttrib(inNode, "Name", *name);
      if (!foundEl)
        continue; //not found

      added.insert(foundEl);
      outNode.InsertEndChild(*foundEl);
    }
    else
    {
      m_error = true;
    }
  }    

  // add rest
  for (TiXmlNode *child = inNode.FirstChild(); child != 0; child = child->NextSibling())
  {
    if (added.find(child) == added.end())
    {
      outNode.InsertEndChild(*child);
    }
  }
}

void VcprojSorter::ProcessFilter(TiXmlNode& inNode, TiXmlNode& sortNode, TiXmlNode& outNode)
{
  for (TiXmlNode *child = inNode.FirstChild(); child != 0; child = child->NextSibling())
  {
    TiXmlNode* outChild = outNode.InsertEndChild(*child);
    assert(outChild);
    if (!outChild)
    {
      m_error = true;
      continue;
    }

    assert(child->Type() == TiXmlNode::TINYXML_ELEMENT);
    if (child->Type() == TiXmlNode::TINYXML_ELEMENT)
    {
      TiXmlElement* element = child->ToElement();
      assert(element);
      if (!element)
      {
        m_error = true;
        continue;
      }

      if (element->ValueStr() == "Filter")
      {
        // another filter..

        const std::string* name = element->Attribute(std::string("Name"));
        assert(name);
        if (!name)
        {
          m_error = true;
          continue;
        }

        TiXmlElement* foundEl = FindElementByAttrib(sortNode, "Name", *name);
        if (!foundEl)
          continue; //not found

        outChild->Clear();
        ProcessFilter(*child, *foundEl , *outChild);
        continue;
      }

      const std::string* name = element->Attribute(std::string("RelativePath"));
      assert(name);
      if (!name)
      {
        m_error = true;
        continue;
      }

      TiXmlElement* foundEl = FindElementByAttrib(sortNode, "RelativePath", *name);
      if (foundEl)
      {
        outChild->Clear();
        ProcessFile(*child, *foundEl, *outChild);
      }
    }
    else
    {
      m_error = true;
    }
  }
}

void VcprojSorter::ProcessFiles(TiXmlNode& inElement)
{
  TiXmlElement* sortFiles = FindElement(m_sortDoc, "Files");
  assert(sortFiles);
  if (!sortFiles)
  {
    m_error = true;
    return;
  }

  TiXmlElement newFiles(*sortFiles);
  newFiles.Clear();

  // process filters
  std::set<TiXmlNode*> added;
  for (TiXmlNode *child = sortFiles->FirstChild(); child != 0; child = child->NextSibling())
  {
    assert(child->Type() == TiXmlNode::TINYXML_ELEMENT);
    if (child->Type() == TiXmlNode::TINYXML_ELEMENT)
    {
      TiXmlElement* element = child->ToElement();
      assert(element);
      if (!element)
      {
        m_error = true;
        continue;
      }

      if (element->ValueStr() == "Filter")
      {
        const std::string* name = element->Attribute(std::string("Name"));
        assert(name);
        if (!name)
        {
          m_error = true;
          continue;
        }

        TiXmlElement* foundEl = FindElementByAttrib(inElement, "Name", *name);
        if (!foundEl)
          continue; //not found

        added.insert(foundEl);
        TiXmlNode* newChild = newFiles.InsertEndChild(*foundEl);
        newChild->Clear();
        assert(newChild);
        if (!newChild)
        {
          m_error = true;
          continue;
        }
        ProcessFilter(*foundEl, *child, *newChild);
      }
    }
    else
    {
      m_error = true;
    }
  }

  // add rest 
  for (TiXmlNode *child = inElement.FirstChild(); child != 0; child = child->NextSibling())
  {
    if (added.find(child) == added.end())
    {
      newFiles.InsertEndChild(*child);
    }
  }

  // process files
  for (TiXmlNode *child = inElement.FirstChild(); child != 0; child = child->NextSibling())
  {
    if (child->ValueStr() != "File")
      continue;

    assert(child->Type() == TiXmlNode::TINYXML_ELEMENT);
    if (child->Type() == TiXmlNode::TINYXML_ELEMENT)
    {
      TiXmlElement* element = child->ToElement();
      assert(element);
      if (!element)
      {
        m_error = true;
        continue;
      }

      const std::string* name = element->Attribute(std::string("RelativePath"));
      assert(name);
      if (!name)
      {
        m_error = true;
        continue;
      }

      TiXmlElement* foundSort = FindElementByAttrib(*sortFiles, "RelativePath", *name);
      if (foundSort)
      {
        TiXmlElement* foundNew = FindElementByAttrib(newFiles, "RelativePath", *name);
        assert(foundNew);
        if (!foundNew)
        {
          m_error = true;
          continue;
        }

        ProcessFile(*child, *foundSort, *foundNew);
      }
    }
    else
    {
      m_error = true;
    }
  }

  // replace
  inElement.Clear();
  for (TiXmlNode *child = newFiles.FirstChild(); child != 0; child = child->NextSibling())
  {
    inElement.InsertEndChild(*child);
  }
}

int _tmain(int argc, _TCHAR* argv[])
{
  if (argc != 4)
  {
    std::cout << "Usage:" << std::endl;
    std::cout << "VcprojSorter2.exe fileIn fileSrt fileOut" << std::endl;
    std::cout << "fileIn .. input vcproj (vcproj that we want to sort)" << std::endl;
    std::cout << "fileSrt.. vcproj that is used to sort fileIn" << std::endl;
    std::cout << "fileOut.. output file (sorted fileIn)" << std::endl;
  }
  else
  {
    TiXmlDocument docIn;
    if (!docIn.LoadFile(argv[1]))
    {
      std::cout << "Reading of input file failed." << std::endl;
      _getch();
      return -1;
    }

    TiXmlDocument docSort;
    if (!docSort.LoadFile(argv[2]))
    {
      std::cout << "Reading of sort file failed." << std::endl;
      _getch();
      return -1;
    }

    VcprojSorter sorter(docIn, docSort);
    if (!sorter.Sort())
    {
      std::cout << "Sorting failed." << std::endl;
      _getch();
      return -1;
    }

    if (!docIn.SaveFile(argv[3]))
    {
      std::cout << "Writing of output file failed." << std::endl;
      _getch();
      return -1;
    }

    std::cout << "Done." << std::endl;
  }

  _getch();
  return 0;
}

