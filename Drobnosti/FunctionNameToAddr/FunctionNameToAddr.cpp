// FunctionNameToAddr.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <tchar.h>

#include <Es/essencepch.hpp>
#include <El/Debugging/mapFile.hpp>
#include <El/QStream/qStream.hpp>
#include <El/CRC/crc.hpp>

struct ExeCRCHeader
{
  int magic; // fixed mark
  int blocks; // number of stored ExeCRCBlock structures
};

struct ExeCRCBlock
{
  int offset, size, crc;
};

// allow access to the protected members
class MapFileEx : public MapFile
{
public:
  int GetSymbolCount() const {return MapFile::GetSymbolCount();}
  const MapInfo &GetSymbol(int i) const {return MapFile::GetSymbol(i);}
};

int _tmain(int argc, _TCHAR* argv[])
{
  if (argc != 5)
  {
    printf("Usage: FunctionNameToAddr source destination exe_file map_file\n");
    return 1;
  }

  const char *src = argv[1];
  const char *dst = argv[2];
  const char *exefile = argv[3];
  const char *mapfile = argv[4];

  MapFileEx map;
  map.ParseMapFile(mapfile);
  if (map.Empty())
  {
    fprintf(stderr, "Error: Mapfile %s is empty\n", mapfile);
    return 1;
  }

  QIFStream in;
  in.open(src);
  QOFStream out;
  out.open(dst);
  QIFStream exe;
  exe.open(exefile);

  ExeCRCHeader header;
  header.magic = 'Chk\0';
  header.blocks = 0;

  while (!in.eof())
  {
    char buffer[256];
    if (!in.readLine(buffer, 256)) continue;
    if (buffer[0] == 0) continue; // ignore empty lines

    // block size
    int size = 0;
    char *ptr = strrchr(buffer, ':');
    if (ptr)
    {
      sscanf(ptr + 1, "0x%x", &size);
      *ptr = 0;
    }

    // offset in the function
    int offsetInFunc = 0;
    ptr = strrchr(buffer, '+');
    if (ptr)
    {
      sscanf(ptr + 1, "0x%x", &offsetInFunc);
      *ptr = 0;
    }

    // function name left in buffer
    int index = -1;
    for (int i=0; i<map.GetSymbolCount(); i++)
    {
      const MapInfo &info = map.GetSymbol(i);
      if (strcmp(info.name, buffer) == 0)
      {
        index = i;
        break;
      }
    }
    if (index < 0) continue;

    const MapInfo &current = map.GetSymbol(index);

    if (size == 0)
    {
      if (index < map.GetSymbolCount() - 1)
      {
        const MapInfo &next = map.GetSymbol(index + 1);
        size = next.physAddress - current.physAddress;
      }
      else continue;
    }

    ExeCRCBlock block;
    block.offset = current.physAddress + offsetInFunc;
    block.size = size;

    exe.seekg(block.offset - 0x00400000, QIOS::beg);
    Buffer<char> readBuf;
    readBuf.Init(size);
    exe.read(readBuf.Data(), size);

    // CRC calculation
    CRCCalculator crc;
    crc.Reset();
    crc.Add((void *)readBuf.Data(), size);
    block.crc = crc.GetResult();

    // write the block to the output
    out.write(&block, sizeof(block));
    header.blocks++;
  }

  // write the header on the end
  out.write(&header, sizeof(header));

  in.close();
  out.close();

	return 0;
}

