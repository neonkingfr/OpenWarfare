// MakeIdentities.cpp : Defines the entry point for the console application.
//

#include <elementpch.hpp>
#include <paramfile/paramfile.hpp>
#include <common/randomGen.hpp>
#include <common/win.h>

#include <ctype.h>

ParamFile Pars;

struct NameInfo
{
	RString name;
	RString nameCls;
};
TypeIsMovableZeroed(NameInfo)

void ReadNamesTxt(const char *in, AutoArray<NameInfo> &names)
{
	QIFStream fileIn;
	fileIn.open(in);

	char name[256], nameCls[256];
	int i = 0, iCls = 0;

	while (true)
	{
		int c = fileIn.get();
		if (fileIn.eof() || fileIn.fail() || c == '\r' || c == '\n')
		{
			if (iCls > 0)
			{
				name[i] = 0;
				nameCls[iCls] = 0;

				int index = names.Add();
				names[index].name = name;
				names[index].nameCls = nameCls;
			}
			i = 0; iCls = 0;
		}
		else
		{
			name[i++] = c;
			if (!isspace(c)) nameCls[iCls++] = c;
		}
		if (fileIn.eof() || fileIn.fail()) break;
	}
}

void ReadNamesParamFile(const char *in, AutoArray<NameInfo> &names)
{
	ParamFile fileIn;
	fileIn.Parse(in);

	const ParamEntry &cls = fileIn >> "Soldiers";
	for (int i=0; i<cls.GetEntryCount(); i++)
	{
		const ParamEntry &entry = cls.GetEntry(i);
		int index = names.Add();
		names[index].nameCls = entry.GetName();
		names[index].name = entry >> "name";
	}
}

void PermuteNames(AutoArray<NameInfo> &names, AutoArray<NameInfo> &namesNew)
{
	for (int i=0; i<names.Size(); i++)
	{
		int iNew = toIntFloor(namesNew.Size() * GRandGen.RandomValue());
		namesNew.Insert(iNew, names[i]);
	}
}

void CreateIdentities(const char *out, AutoArray<RString> &faces, AutoArray<NameInfo> &names)
{
	const ParamEntry &glasses = Pars >> "CfgGlasses";
	const ParamEntry &speakers = Pars >> "CfgVoice" >> "voices";
	int nFaces = faces.Size();
	int nVoices = speakers.GetSize();

	ParamFile fileOut;
	for (int i=0; i<names.Size(); i++)
	{
		ParamEntry *entry = fileOut.AddClass(names[i].nameCls);
		entry->Add("name", names[i].name);

		int iFace = toIntFloor(nFaces * GRandGen.RandomValue());
		entry->Add("face", faces[iFace]);

		int iSpeaker = toIntFloor(nVoices * GRandGen.RandomValue());
		entry->Add("speaker",(RStringB)speakers[iSpeaker]);

		float pitch = GRandGen.PlusMinus(1,0.1);
		entry->Add("pitch",pitch);

		float g = GRandGen.RandomValue();
		int ig;
		if (g < 0.89) ig = 0;
		else if (g < 0.96) ig = 1;
		else ig = 2;
		entry->Add("glasses", glasses.GetEntry(ig).GetName());
	}

	fileOut.Save(out);
}

void Translate(const char *in, const char *out, AutoArray<RString> &faces)
{
	// read list of names
	AutoArray<NameInfo> names;
	ReadNamesTxt(in, names);

	// random permutation
	AutoArray<NameInfo> namesNew;
	PermuteNames(names, namesNew);

	// create output
	CreateIdentities(out, faces, namesNew);
}

void Translate2(const char *in, const char *out, AutoArray<RString> &faces)
{
	// read list of names
	AutoArray<NameInfo> names;
	ReadNamesParamFile(in, names);

	// create output
	CreateIdentities(out, faces, names);
}

int main(int argc, char* argv[])
{
	char oldDirectory[512];

	GetCurrentDirectory(512, oldDirectory);
	SetCurrentDirectory("bin");
	Pars.Parse("config.cpp");
	SetCurrentDirectory(oldDirectory);

	ParamFile addon;
	GetCurrentDirectory(512, oldDirectory);
	SetCurrentDirectory("o");
	addon.Parse("config.cpp");
	SetCurrentDirectory(oldDirectory);

	Pars.Update(addon, &Pars);
	Pars.SetFile(&Pars);

	const ParamEntry &cfgFaces = Pars >> "CfgFaces";

	// resistance soldiers
	/*
	AutoArray<RString> faces;
	for (int i=0; i<cfgFaces.GetEntryCount(); i++)
	{
		const ParamEntry &cls = cfgFaces.GetEntry(i);
		if (stricmp(cls.GetName(), "custom") == 0) continue;
		bool woman = false;
		if (cls.FindEntry("woman")) woman = cls >> "woman";
		if (!woman) faces.Add(cls.GetName());
	}
	Translate("muzska.dat", "muzska.cfg", faces);

	faces.Clear();
	for (int i=0; i<cfgFaces.GetEntryCount(); i++)
	{
		const ParamEntry &cls = cfgFaces.GetEntry(i);
		if (stricmp(cls.GetName(), "custom") == 0) continue;
		bool woman = false;
		if (cls.FindEntry("woman")) woman = cls >> "woman";
		if (woman) faces.Add(cls.GetName());
	}
	Translate("zenska.dat", "zenska.cfg", faces);
	*/

	AutoArray<RString> faces;
	for (int i=0; i<cfgFaces.GetEntryCount(); i++)
	{
		const ParamEntry &cls = cfgFaces.GetEntry(i);
		if (stricmp(cls.GetName(), "custom") == 0) continue;
		if (stricmp(cls.GetName(), "default") == 0) continue;
		bool woman = false;
		if (cls.FindEntry("woman")) woman = cls >> "woman";
		if (!woman) faces.Add(cls.GetName());
	}
	Translate2("czechnames.txt", "czechnames.hpp", faces);
	return 0;
}

