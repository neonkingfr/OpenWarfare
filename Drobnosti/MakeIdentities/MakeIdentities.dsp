# Microsoft Developer Studio Project File - Name="MakeIdentities" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=MakeIdentities - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "MakeIdentities.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "MakeIdentities.mak" CFG="MakeIdentities - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "MakeIdentities - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "MakeIdentities - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Drobnosti/MakeIdentities", MPJAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /W3 /GR /GX- /Zi /O2 /I "w:\c\element" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /D "_RELEASE" /YX"elementpch.hpp" /FD /c
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /out:"O:\StatusQuo\MakeIdentities.exe"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /GZ  /c
# ADD CPP /nologo /W3 /Gm /GR /GX- /ZI /Od /I "w:\c\element" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /GZ  /c
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib  kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "MakeIdentities - Win32 Release"
# Name "MakeIdentities - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\MakeIdentities.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Group "Element"

# PROP Default_Filter ""
# Begin Group "Common"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Element\Common\perfLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\Common\randomGen.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\Common\randomGen.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\Common\win.h
# End Source File
# End Group
# Begin Group "PCH"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Element\PCH\afxConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\Element\PCH\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\PCH\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\PCH\normalConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\Element\PCH\sReleaseConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\Element\PCH\stdIncludes.h
# End Source File
# End Group
# Begin Group "QStream"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Element\QStream\fileCompress.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# ADD CPP /D "_RELEASE" /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileMapping.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# ADD CPP /D "_RELEASE" /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileOverlapped.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# ADD CPP /D "_RELEASE" /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\fileOverlapped.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\qbstream.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# ADD CPP /D "_RELEASE" /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\qstream.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# ADD CPP /D "_RELEASE" /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\serializeBin.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# ADD CPP /D "_RELEASE" /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\QStream\ssCompress.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# ADD CPP /D "_RELEASE" /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# End Group
# Begin Group "Preprocessor"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Element\Preprocessor\Preproc.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# ADD CPP /D "_RELEASE" /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Element\Preprocessor\Preproc.h
# End Source File
# End Group
# Begin Group "ParamFile"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Element\ParamFile\paramFile.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# ADD CPP /D "_RELEASE" /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\Element\ParamFile\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Element\ParamFile\paramFilePreproc.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# ADD CPP /D "_RELEASE" /YX"elementpch.hpp"

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# End Group
# Begin Source File

SOURCE=..\..\Element\elementpch.hpp
# End Source File
# End Group
# Begin Group "Esence"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\suma\Class\appFrame.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\array.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\bstring.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\cachelist.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\consoleBase.h
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\fastAlloc.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\list.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\lzcompr.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\lzcompr.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\memtype.h
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\normalNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\rString.cpp

!IF  "$(CFG)" == "MakeIdentities - Win32 Release"

# SUBTRACT CPP /YX /Yc /Yu

!ELSEIF  "$(CFG)" == "MakeIdentities - Win32 Debug"

# ADD CPP /YX"elementpch.hpp"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\rString.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\smallArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\..\suma\Class\typeOpts.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
