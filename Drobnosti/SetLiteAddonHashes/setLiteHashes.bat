Setlocal EnableDelayedExpansion

FOR %%i in (*.pbo) do (
  b:\bankrev\bankrev.exe -f "unpack" "%%~dpnxi"
)

@for /D %%i in (unpack\*.*) do @(
  @set Prefix=
  @for /F "delims== tokens=1,2*" %%A in ('type unpack\%%~ni.txt ^| find /I "prefix" ^| find /I /V "hprotect" ^| find /I /V "hash"') do @set Prefix=!Prefix! -property %%A="%%B"
  @set Prefix=!Prefix:~0,-2!"
  @for /F "delims== tokens=1,2*" %%A in ('type unpack\%%~ni.txt ^| find /I /V "prefix" ^| find /I /V "hprotect" ^| find /I /V "hash"') do @set Prefix=!Prefix! -property %%A="%%B"
  @for /F "delims== tokens=1,2*" %%A in ('type hashes\%%~ni.txt ^| find /I "hash"') do @set Prefix=!Prefix! -property %%A="%%B"
  b:\filebank\FileBank.exe -dst newPBOs !Prefix! %%i
  @ECHO %%~ni.PBO
)
