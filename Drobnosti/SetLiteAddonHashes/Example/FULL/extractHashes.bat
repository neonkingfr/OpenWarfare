Setlocal EnableDelayedExpansion

mkdir hashes

FOR %%i in (*.pbo) do (
  @set PBO_HASH=hash
  @for /F %%d in ('b:\bankrev\bankrev.exe -hash "%%~dpnxi"') do @set PBO_HASH=!PBO_HASH!=%%d
  @echo !PBO_HASH!>>"hashes"\"%%~ni".txt
  echo !PBO_HASH!
)
