Popis probl�mu: st�vaj�c� LITE BAF/PMC addony neobsahuj� propertu hash=xxx, co� zp�sobuje nap�. news:jgavof$p5p$2@new-server.localdomain

�e�en�: jedn� se o poloautomacitk� �e�en�, snad nebude t�eba to d�lat �asto
Postup:

1. get decrypted FULL BAF addons into some directory
  * note, you can use engine with special commandline news:jbqmel$ruq$1@new-server.localdomain
2. copy extractHashes.bat into this directory
  * note, there should be no other subdirectories inside, only *.pbo files
3. run extractHashes.bat
  * check the subdirectory hashes were created with bunch of *.txt files with hash=xxx content
4. create some new directory containing all LITE BAF addons 
  * their count should match the count of *.txt in folder hashes (or count of full BAF addons)
5. copy/move there the hashes folder
6. copy the setLiteHashes.bat into this new folder containing LITE addons
7. run setLiteHashes.bat
  * check the unpack, hashes nad newPBOs folders are present at the end
  * inside newPBOs the new LITE addons should be present
  * open some pbos to check there is hash=xxx property inside and all other properties present as well

In the Example folder the FULL and LITE folders are prepared to run the extractHashes.bat or setLiteHashes.bat.
