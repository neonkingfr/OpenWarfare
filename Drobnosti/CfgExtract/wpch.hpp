// C libraries
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stddef.h>

#include <memType.h> // basic memory types

#include <class\array.hpp>
#include <class\pointers.hpp>
#include <class\removeLinks.hpp>
