#include "wpch.hpp"
#include <class/consoleBase.h>
#include <class/consoleBase.cpp>
#include "\c\poseidon\lib\paramFile.hpp"

// custom preprocessor provided

static RString OneTab = "\t";

static void PrintDocs(const ParamEntry &entry, RString indentS=RString())
{
	// print any entry that has "comment" member present
	// when printing, print also "scope" member (if present)
	int i;
	for (i=0; i<entry.GetEntryCount(); i++)
	{
		const ParamEntry &e = entry.GetEntry(i);
		if (!e.IsClass()) continue;
		if (e.FindEntryNoInheritance("__doc_comment"))
		{
			RString indentS1 = indentS+OneTab;
			const char *i0 = (const char *)indentS;
			const char *i1 = (const char *)indentS1;
			const RStringB &dc = e>>"__doc_comment";
			if (dc[0]=='>')
			{
				// starting new XML value list
				const char *classname = (const char *)dc+1;
				while (isspace(*classname)) classname++;

				printf("%s<class>\n",i0);

				printf("%s<name>%s</name>\n",i1,(const char *)e.GetName());
				printf("%s<description>%s</description>\n",i1,(const char *)classname);
				PrintDocs(e,indentS1);

				printf("%s</class>\n",i0);
			}
			else
			{
				Log("%s%s",(const char *)indentS, (const char *)e.GetName());
				printf("%s<value>\n",i0);
				printf("%s<name>%s</name>\n",i1,(const char *)e.GetName());
				printf("%s<description>%s</description>\n",i1,(const char *)dc);
				if (e.FindEntry("scope"))
				{
					const ParamEntry &pe = e>>"scope";
					int scopeValue = pe;
					RString pes = pe;
					if (!strcmp(pes,"public")) scopeValue = 2;
					else if (!strcmp(pes,"protected")) scopeValue = 1;
					const char *scope = "unknown";
					switch (scopeValue)
					{
						case 0: scope = "abstract";break;
						case 1: scope = "hidden";break;
						case 2: scope = "normal";break;
					}
					printf("%s<scope>%s</scope>\n",i1,scope);
				}
				const ParamClass *pc = dynamic_cast<const ParamClass *>(&e);
				if (pc)
				{
					const char *basename = pc->GetBaseName();
					if (basename && *basename)
					{
						printf("%s<base>%s</base>\n",i1,basename);
					}
				}
				printf("%s</value>\n",i0);
			}
		}
		else
		{
			PrintDocs(e,indentS+OneTab);
		}
	}
}

int consoleMain(int argc, const char *argv[])
{
	if (argc<2)
	{
		return 1;
	}
	int ret = 0;
	printf("<classlist>\n\n");
	while (argc>=2)
	{
		const char *src = argv[1];
		ParamFile in;
		in.Parse(src);
		PrintDocs(in);
		argc--,argv++;
	}
	printf("\n</classlist>\n");
	// print result to stdout
	// scan all entries
	return 0;
	// extract documented classes from config file

}

#include <class\rString.cpp>
#include <class\fastAlloc.cpp>

void LstF(char const *format,...)
{
}

void ErrorMessage(char const *format,...)
{
}

void WarningMessage(char const *format,...)
{
}

#include <class/normalNew.hpp>

void *operator new ( size_t size, const char *file, int line )
{
	return malloc(size);
}

#include <windows.h>
HWND hwndApp;