// convert different tabsizes
// (C) 1997, SUMA

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <stdlib.h>
//#include <fstream.h>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

/*
	Reads file, recalculates indent accordingly to new TabSize
	File is scaned in first pass and TabSize is autodetected
*/

static int CountSpaces( const char *line )
{
	int count=0;
	while( *line==' ' ) count++,line++;
	return count;
}

int main( int argc, const char *argv[] )
{
	string inputName;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	
	// Lets try a trick to determine if we were 'launched' as a seperate
	// screen, or just running from the command line.
	HANDLE hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	HANDLE hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsoleOutput, &csbi);
	BOOL bLaunched = ((csbi.dwCursorPosition.X==0) && (csbi.dwCursorPosition.Y==0));
	if ((csbi.dwSize.X<=0) || (csbi.dwSize.Y <= 0)) bLaunched = FALSE;

	int oldTab=0;
	int newTab=0;
	
	while( --argc>0 )
	{
		const char *arg=*++argv;
		if( *arg=='-' )
		{
			// some option
			if( arg[1]=='o' ) oldTab=atoi(arg+2);
			else if( arg[1]=='n' ) newTab=atoi(arg+2);
			else goto wrong_usage;
		}
		else
		{
			if( inputName.length()>0 ) goto wrong_usage;
			inputName=arg;
		}
	}
	if( inputName.length()<=0 )
	{
		wrong_usage:
		cout << "Usage:\n";
		cout << "tabsize [-oXXX] [-nXXX] filename\n\n";
		cout << "Options:\n\n";
		cout << "-oXXX is old tab size, (if missing, determined from the file contents).\n";
		cout << "-nXXX is new tab size (default - use hard tabs).\n";
		goto Error;
	}

	{
		// calculate distibution of indents
		{
			int indent=oldTab;
			#define MAX_INDENT 80
			BOOL autodetect=( indent==0 );
			int indentDistribution[MAX_INDENT];
			memset(indentDistribution,0,sizeof(indentDistribution));
			ifstream f(inputName.c_str());
			while( !f.fail() && !f.eof() )
			{
				char line[512];
				f.getline(line,sizeof(line));
				if( strlen(line)>=sizeof(line)-2 )
				{
					cout << "Too long line encountered.\n";
					goto Error;
				}
				int spaceCount=CountSpaces(line);
				if( spaceCount<MAX_INDENT )
				{
					indentDistribution[spaceCount]++;
				}
			}
			if( !f.eof() ) goto Error;
	
			if( autodetect )
			{
				// analyse the distribution table
		
				// we search for maximal indent so that 90% of distribution table (without position 0)
				// is on the indent*i for any i
		
				int i;
				int sum=0;
				for( i=2; i<MAX_INDENT; i++ ) sum+=indentDistribution[i];
		
				// try all possible indents
				for( i=2; i<MAX_INDENT; i++ )
				{
					int cover=0;
					for( int j=i; j<MAX_INDENT; j+=i )
					{
						cover+=indentDistribution[j];
					}
					//if( cover>0.9*sum ) indent=i;
					if( cover>0.7*sum ) indent=i;
				}
		
				if( indent==0 || indent>=MAX_INDENT-1 )
				{
					cout << "No soft tabs encountered - no conversion.\n";
					goto Error;
				}
				oldTab=indent;
				cout << "TabSize detected " << oldTab << '\n';
			}
			else
			{
				// no autodetect, size given by user
				cout << "TabSize is " << oldTab << '\n';
			}
		}
		
		// convert to temporary file
		BOOL done=FALSE;
		char tempName[MAX_PATH+1];
		
		GetTempFileName(".","tab",0,tempName);
		
		{
			ifstream f(inputName.c_str());
			ofstream o(tempName);
			while( !f.fail() && !f.eof() && !o.fail() )
			{
				char line[512];
				f.getline(line,sizeof(line));
				int spaceCount=CountSpaces(line);
				int tabCount=spaceCount/oldTab;
				if( newTab>0 )
				{
					for( int c=0; c<tabCount*newTab; c++ ) o << ' ';
				}
				else
				{
					for( int c=0; c<tabCount; c++ ) o << '\t';
				}
				const char *ol=line+spaceCount;
				o.write(ol,strlen(ol));
				o << '\n';
			}
			if( f.eof() && !o.fail() ) done=TRUE;
		}
		if( done )
		{
			DeleteFile(inputName.c_str());
			MoveFile(tempName,inputName.c_str());
			return 0;
		}
		else
		{
			DeleteFile(tempName);
		}
	}
	
	Error: // return to console or desktop    
	if( bLaunched )
	{
		DWORD ret;
		static const char text[]="\n-----------------------\nPress ENTER to continue";
		char buf[80];
		WriteConsole(hConsoleOutput,text,strlen(text),&ret,NULL);
		ReadConsole(hConsoleInput,buf,sizeof(buf),&ret,NULL);
	}
	return -1;
}


