#include <Es/essencePch.hpp>
#include <stdio.h>
#include <Es/Containers/array.hpp>
#include <Es/Containers/Array2D.hpp>

void SetDitherMatrix(Array2D<int> &res, int dim, int x, int z, int min, int max)
{
  // 2x2 dithering pattern on (x,z..x+dim,z+dim>
  // fill: (x,z..x+dim/2,z+dim/2>
  if (dim==1)
  {
    res(z,x) = min;
  }
  else
  {
    int d2 = dim/2;
    int s2 = (max-min)/(d2*d2);
    int s = (max-min)/d2;
    SetDitherMatrix(res,d2,x,z,min+s2*2,min+(max-min)*3/4+s2*2);
    SetDitherMatrix(res,d2,x+d2,z,min,min+(max-min)*3/4);
    SetDitherMatrix(res,d2,x,z+d2,min+s2,min+(max-min)*3/4+s2);
    SetDitherMatrix(res,d2,x+d2,z+d2,min+s2*3,min+(max-min)*3/4+s2*3);
  }
}

void CreateDitherMatrix(Array2D<int> &res, int dim, int min=0, int max=256)
{
  res.Dim(dim,dim);
  for (int x=0; x<dim; x++) for (int z=0; z<dim; z++)
  {
    res(x,z) = 0;
  }
  //int step=2;
  for (int step=2; step<=dim; step*=2)
  {
    for (int x=0; x<dim; x+=step) for (int z=0; z<dim; z+=step)
    {
      for (int xx=x; xx<x+step/2; xx++)
      for (int zz=z; zz<z+step/2; zz++)
      {
        res(xx,zz) += min;
      }
      for (int xx=x+step/2; xx<x+step; xx++)
      for (int zz=z; zz<z+step/2; zz++)
      {
        res(xx,zz) += max-(max-min)*2/4;
      }
      for (int xx=x; xx<x+step/2; xx++)
      for (int zz=z+step/2; zz<z+step; zz++)
      {
        res(xx,zz) += max-(max-min)*1/4;
      }
      for (int xx=x+step/2; xx<x+step; xx++)
      for (int zz=z+step/2; zz<z+step; zz++)
      {
        res(xx,zz) += max-(max-min)*3/4;
      }
    }
    int nMin = 0;
    int nMax = (max-min)/4;
    min = nMin;
    max = nMax;
  }
  //SetDitherMatrix(res,dim,0,0,min,max);

}

void CreateMipmap(Array2D<int> &res, const Array2D<int> &src)
{
  res.Dim(src.GetXRange()/2,src.GetYRange()/2);
  for (int z=0; z<res.GetXRange(); z++)
  for (int x=0; x<res.GetXRange(); x++)
  {
    int sum = 0;
    for (int xx=0; xx<2; xx++)
    for (int zz=0; zz<2; zz++)
    {
      sum += src(x*2+xx,z*2+zz);
    }
    res(x,z) = sum/4;
  }
}

void LogDitherMatrix(const Array2D<int> &res)
{
  LogF("---");
  for (int y=0; y<res.GetYRange(); y++)
  {
    char line[1024];
    strcpy(line,"");
    for (int x=0; x<res.GetXRange(); x++)
    {
      char item[256];
      sprintf(item,"%3d",res(y,x));
      if (*line) strcat(line," ");
      strcat(line,item);
    }
    LogF("(%s)",line);
  }
}

void LogDitherMatrix(const Array2D<int> &res, int edge)
{
  LogF("-- %d",edge);
  for (int y=0; y<res.GetYRange(); y++)
  {
    char line[1024];
    strcpy(line,"");
    for (int x=0; x<res.GetXRange(); x++)
    {
      char item[256];
      sprintf(item,"%d",res(y,x)<edge);
      if (*line) strcat(line," ");
      strcat(line,item);
    }
    LogF("(%s)",line);
  }
}

int main(int argc, const char *argv[])
{
  Array2D<int> m2x2;
  Array2D<int> m4x4;
  Array2D<int> m8x8;
  Array2D<int> m16x16;
  CreateDitherMatrix(m2x2,2,0,4);
  LogDitherMatrix(m2x2);
  CreateDitherMatrix(m4x4,4,0,16);
  LogDitherMatrix(m4x4);

  CreateDitherMatrix(m8x8,8,192,256);
  LogDitherMatrix(m8x8);
  CreateMipmap(m4x4,m8x8);
  LogDitherMatrix(m4x4);
  CreateMipmap(m2x2,m4x4);
  LogDitherMatrix(m2x2);
  /*
  LogDitherMatrix(m8x8,3);
  LogDitherMatrix(m8x8,16);
  LogDitherMatrix(m8x8,32);
  LogDitherMatrix(m8x8,48);
  */
  return 0;
}