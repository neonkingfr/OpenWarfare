// BIStoreClient.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "BIStoreClient.h"

// { INCLUDES
#include <stdio.h>
#include <ShellAPI.h>
#include <WinCrypt.h>
#include "Winhttp.h"

#pragma comment(lib, "winhttp")
// } INCLUDES

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
HWND				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

// { HELPER FUNCTIONS
BOOL GenerateRandomBytes(BYTE *buffer, int count) 
{
  BOOL ok = false;

  HCRYPTPROV hProvider = 0;
  if (::CryptAcquireContext(&hProvider, 0, 0, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT | CRYPT_SILENT))
  {
    ok = ::CryptGenRandom(hProvider, count, buffer);
    ::CryptReleaseContext(hProvider, 0);
  }
  return ok;
}

inline TCHAR ToChar(int digit)
{
  if (digit < 10) return _T('0') + digit;
  return _T('a') + digit - 10;
}

void OnError()
{
  DWORD err = GetLastError();
  printf("Error: 0x%x", err);
}

static char _result[100] = "";

// } HELPER FUNCTIONS

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_BISTORECLIENT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
  HWND wnd = InitInstance (hInstance, nCmdShow);
	if (!wnd) return FALSE;

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_BISTORECLIENT));

  // { OPEN THE BROWSER
  const bool hostSecure = false; // https protocol
  const TCHAR *hostName = _T("free.arma2.com");
  const int hostPort = INTERNET_DEFAULT_HTTP_PORT;
  const TCHAR *hostRoot = _T("BIStore");
  
  // create a nonce
  BYTE random[32];
  TCHAR nonce[2 * sizeof(random) + 1];
  BOOL nonceOK = GenerateRandomBytes(random, sizeof(random));
  if (nonceOK)
  {
    TCHAR *ptr = nonce;
    for (int i=0; i<sizeof(random); i++)
    {
      *ptr = ToChar((random[i] >> 4) & 0x0f); ptr++;
      *ptr = ToChar(random[i] & 0x0f); ptr++;
    }
    *ptr = 0;
  }

  // open the browser
  TCHAR cmd[1024];
  size_t cmdSize = sizeof(cmd) / sizeof(TCHAR);
  _stprintf_s(cmd, cmdSize, _T("url.dll,FileProtocolHandler %s://%s:%d/%s/index.jsp"),
    hostSecure ? _T("https") : _T("http"), hostName, hostPort, hostRoot);
  if (nonceOK) // otherwise user need to copy Product Key manually
  {
    _tcscat_s(cmd, cmdSize, _T("?nonce="));
    _tcscat_s(cmd, cmdSize, nonce);
  }
  ShellExecute(NULL, _T("open"), _T("rundll32.exe"), cmd, NULL, SW_SHOWNORMAL);
  // } OPEN THE BROWSER

  // { CONNECT TO THE SERVER
  HINTERNET session = NULL, connect = NULL, request = NULL;
  if (nonceOK)
  {
    TCHAR query[1024];
    size_t querySize = sizeof(query) / sizeof(TCHAR);
    _stprintf_s(query, querySize, _T("/%s/Result?nonce=%s"), hostRoot, nonce);

    session = WinHttpOpen(NULL, 
      WINHTTP_ACCESS_TYPE_NO_PROXY, WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS,
      0);
    if (!session) OnError();
    else
    {
      connect = WinHttpConnect(session, hostName, hostPort, 0);
      if (!connect) OnError();
      else
      {
        request = WinHttpOpenRequest(connect, _T("GET"), query,
          NULL, WINHTTP_NO_REFERER, WINHTTP_DEFAULT_ACCEPT_TYPES, hostSecure ? WINHTTP_FLAG_SECURE : 0); 
        if (!request) OnError();
      }
    }
  }
  // } CONNECT TO THE SERVER

  SetTimer(wnd, 1, 2000, NULL);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
    // { CHECK THE SERVER
    if (msg.message == WM_TIMER && *_result == 0) // result not known yet
    {
      if (request)
      {
        if (!WinHttpSendRequest(request, 
          WINHTTP_NO_ADDITIONAL_HEADERS, 0,
          WINHTTP_NO_REQUEST_DATA, 0, 
          0, 0)) OnError();
        else
        {
          if (!WinHttpReceiveResponse(request, NULL)) OnError();
          else
          {
            DWORD maxSize = sizeof(_result);
            DWORD size;
            if (WinHttpQueryDataAvailable(request, &size))
            {
              if (size > 0 && size < maxSize)
              {
                if (!WinHttpReadData(request, _result, size, &size))
                {
                  OnError();
                  _result[0] = 0;
                }
                else
                {
                  _result[size] = 0;
                  InvalidateRect(wnd, NULL, TRUE);
                }
              }
            }
          }
        }
      }
    }
    // } CHECK THE SERVER

		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

  // { DISCONNECT FROM THE SERVER
  if (request) WinHttpCloseHandle(request);
  if (connect) WinHttpCloseHandle(connect);
  if (session) WinHttpCloseHandle(session);
  // } DISCONNECT FROM THE SERVER

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_BISTORECLIENT));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_BISTORECLIENT);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
HWND InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd) return NULL;

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return hWnd;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
    if (*_result)
    {
      RECT rect;
      GetClientRect(hWnd, &rect);
      DrawTextA(hdc, _result, -1, &rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
    }
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
