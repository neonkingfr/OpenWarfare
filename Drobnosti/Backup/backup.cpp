/* konvertor obrazku */
/* SUMA 2/1995 */

#include <windows.h>
#include <shlobj.h>
#include <string.h>
#include <stdlib.h>
#include <direct.h>
#include <vector>
#include <fstream>
#include <dos.h>
#include <io.h>
#include <fcntl.h>
#include <sys\stat.h>
#include "..\misc\filemisc.hpp"

using namespace std;

static BOOL wasError=TRUE;

static const char *ExtList[]=
{
	".obj",".lib",
	".exe",".dll",
	".pch",".ilk",
	".idb",".pdb",".plg",".pbt",".pbi", // MSVC temporary files
	".ncb",".plg", // more MSVC remporaries
	".lst",".err",".mak", // compiler output
	".sym",".map",".log", // generated files
	//".pac", // pac considered precious
	".tbs",".mdb", // VTune temporary
	".mk1",".lk1",".mk",".lb1", // Watcom temporary files
	"._xe",".res",
	NULL,
};

#define ListName "files.bck"

class FileInfo
{
	public:
	char *name;
	unsigned int time;
	int backupNum; // in which backup it was included
	int Compare( const FileInfo &t ) const
	{
		return strcmpi(name,t.name);
	}
	bool operator == ( const FileInfo &t ) const
	{
		return Compare(t)==0;
	}
	bool operator != ( const FileInfo &t ) const
	{
		return Compare(t)!=0;
	}
	bool operator < ( const FileInfo &t ) const
	{
		return Compare(t)<0;
	}
	FileInfo( int num=0 )
	{
		name=NULL;
		time=0,backupNum=0;
	}
	FileInfo operator ! () const {return *this;}
};

//typedef vector<FileInfo> FileList;
typedef basic_string<FileInfo> FileList;

static FileList DestFiles;
static FileList FolderFiles;
static bool IsNew=false;

static int ExcludeFiles( const char *ext )
{
	const char **exts;
	for( exts=ExtList; *exts; exts++ )
	{
		if( !strcmpi(ext,*exts) ) return 1;
	}
	return 0;
}

static unsigned int FileTime( const char *path )
{
	struct _stat buffer;
	_stat(path,&buffer);
	return buffer.st_mtime;
}
static void CreateDir( const char *dest )
{
	// whole chain must be created
	char temp[MAX_PATH];
	strcpy(temp,dest);
	char *p;
	for( p=temp; *p; p++ )
	{
		if( *p=='\\' )
		{
			*p=0;
			CreateDirectory(temp,NULL);
			*p='\\';
		}
	}
}

static bool needLF;

static void Report( const char *text, const char *name, bool doLF=false )
{
	const int repSize=45;
	char buf[repSize+1];
	strncpy(buf,name,repSize);
	buf[repSize]=0;
	printf("%30s %-*s",text,repSize,buf);
	if( doLF ) printf("\n"),needLF=false;
	else printf("\r"),needLF=true;
}
static void EmptyLine()
{
	Report("","");
}

static int ConvertDir( int num, const char *dRoot, const char *dest, const char *src )
{
	char srcWild[MAX_PATH];
	strcpy(srcWild,src);
	EndChar(srcWild,'\\');
	strcat(srcWild,"*");
	_finddata_t find;
	long hFile=_findfirst(srcWild,&find);
	if( hFile>=0 )
	{
		Report("Scanning",src);
		BOOL destCreated=FALSE;
		do
		{
			char sPath[MAX_PATH];
			char dPath[MAX_PATH];
			if( !strcmp(find.name,".") ) continue;
			if( !strcmp(find.name,"..") ) continue;

			// actual file - corresponding source pathname
			strcpy(sPath,src);
			EndChar(sPath,'\\');
			strcat(sPath,find.name);
			
			// actual file - corresponding destination pathname
			strcpy(dPath,dest);
			EndChar(dPath,'\\');
			strcat(dPath,find.name);
			
			if( find.attrib&_A_SUBDIR )
			{
				EndChar(dPath,'\\');
				EndChar(sPath,'\\');
				if( ConvertDir(num,dRoot,dPath,sPath)<0 ) return -1;
				continue;
			}
			
			if( ExcludeFiles(NajdiPExt(find.name)) ) continue;

			FileInfo key;
			key.name=dPath+strlen(dRoot);
			int index=DestFiles.find(key);
			if( index<0 || DestFiles[index].time<FileTime(sPath) )
			{
				// copy file from source to destination
				if( !destCreated )
				{
					Report("Writing to",dest,true);
					CreateDir(dest);
					destCreated=TRUE;
				}
				// add file description into the FolderFiles
				FileInfo add;
				add.name=strdup(dPath+strlen(dRoot));
				add.time=FileTime(sPath);
				add.backupNum=num;
				FolderFiles.append(1,add);
				IsNew=true;
				if( !CopyFile( sPath,dPath,TRUE) )
				{
					EmptyLine();
					printf("Error: Cannot copy file '%s' to '%s'\n",sPath,dPath);
					return -1;
				}	
			}
			else
			{
				FolderFiles.append(1,DestFiles[index]);
			}
		}
		while( _findnext(hFile,&find)==0 );
		_findclose(hFile);
	}
	return 0;
}

static int RestoreDir( const char *dRoot, const char *src )
{
	Report("Restore",src,true);

	// scan all backup files
	int i,n=DestFiles.size();
	for( i=0; i<n; i++ )
	{
		// DestFiles[i].name is something like "c\poseidon\poly.hpp"
		// we must add drive specification from src
		// if it matches src specification, restore it
		FileInfo &file=DestFiles[i];
		if( src[1]==':' && src[2]=='\\' )
		{
			if( strnicmp(src+3,file.name,strlen(src+3)) ) continue;
		}
		char dst[MAX_PATH];
		*dst=0;
		if( src[1]==':' ) dst[0]=src[0],strcpy(dst+1,":\\");
		strcat(dst,file.name);
		int time=FileTime(dst); // hard-disk file information
		if( time!=file.time )
		{
			// necessary to restore
			char src[MAX_PATH];
			sprintf(src,"%s%d\\%s",dRoot,file.backupNum,file.name);
			
			Report("Write",src);
			/**/
			CreateDir(dst);
			if( !CopyFile(src,dst,TRUE) ) // caution - may overwrite
			{
				Report("Overwrite",dst,true);
				if( !CopyFile(src,dst,FALSE) ) // caution - may overwrite
				{
					printf("Error: Cannot copy file '%s' to '%s'\n",src,dst);
					return -1;
				}
			}
			/**/
		}
	}
	return 0;
}

static int ScanDir( int num, const char *root, const char *dest )
{
	_finddata_t find;
	char destWild[MAX_PATH];
	strcpy(destWild,dest);
	EndChar(destWild,'\\');
	strcat(destWild,"*");
	long hFile=_findfirst(destWild,&find);
	if( hFile<0 ) return 1;
	
	Report("Scanning",dest);
	
	do
	{
		char dPath[MAX_PATH];
		if( !strcmp(find.name,".") ) continue;
		if( !strcmp(find.name,"..") ) continue;
		
		// actual file - destination pathname
		strcpy(dPath,dest);
		EndChar(dPath,'\\');
		strcat(dPath,find.name);
		
		if( find.attrib&_A_SUBDIR )
		{
			EndChar(dPath,'\\');
			if( ScanDir(num,root,dPath)<0 ) return -1;
			continue;
		}
		
		if( ExcludeFiles(NajdiPExt(find.name)) ) continue;

		// store relative names only
		
		FileInfo info;
		info.name=strdup(dPath+strlen(root));
		if( !info.name ) return -1;
		info.time=FileTime(dPath);
		info.backupNum=num;
		
		int index=DestFiles.find(info);
		if( index>=0 )
		{
			FileInfo &prev=DestFiles[index];
			// filename same, time changed
			if( info.time>prev.time ) prev.time=info.time,prev.backupNum=info.backupNum;
		}
		else DestFiles.append(1,info);
	}
	while( _findnext(hFile,&find)==0 );
	_findclose(hFile);
	return 0;
}

static int ReadNum( istream &f )
{
	int len=0;
	f.read((char *)&len,sizeof(len));
	return len;
}
static void WriteNum( ostream &f, int num )
{
	f.write((char *)&num,sizeof(num));
}

static char *ReadStr( istream &f )
{
	int len=ReadNum(f);
	if( len<=0 ) return NULL;
	char *buf=new char[len+1];
	if( buf )
	{
		f.read(buf,len);
		buf[len]=0;
		return buf;
	}
	return NULL;
}


static void WriteStr( ostream &f, const char *str )
{
	int len=strlen(str);
	WriteNum(f,len);
	f.write(str,len);
}

static void SaveFileList( const char *path,  FileList &list )
{
	Report("Saving file list",path,true);
	// save file list on backup root
	char name[MAX_PATH];
	strcpy(name,path);
	EndChar(name,'\\');
	strcat(name,ListName);
	ofstream f;
	f.open(name,ios::binary|ios::out);
	int i,n=list.size();
	for( i=0; i<n; i++ )
	{
		WriteStr(f,list[i].name);
		WriteNum(f,list[i].backupNum);
		WriteNum(f,list[i].time);
	}
}

static bool CheckFileInfo( const char *buf )
{
	ifstream f;
	f.open(buf,ios::binary|ios::in);
	if( f.fail() ) return false;
	return true; // file exists - you can try to read it
}

static int LoadFileInfo( const char *buf )
{
	ifstream f;
	f.open(buf,ios::binary|ios::in);
	if( f.fail() ) return -1;
	Report("Loading file list",buf,true);
	for(;;)
	{
		FileInfo add;
		add.name=ReadStr(f);
		if( !add.name ) break;
		add.backupNum=ReadNum(f);
		add.time=ReadNum(f);
		if( f.fail() ) return -1;
		int index=DestFiles.find(add);
		if( index>=0 )
		{
			FileInfo &prev=DestFiles[index];
			// filename same, time changed
			if( add.time>prev.time )
			{
				prev.time=add.time;
				prev.backupNum=add.backupNum;
			}
		}
		else DestFiles.append(1,add);
	}
	return 0;
}

static int ScanDir( const char *dest )
{
	char root[MAX_PATH];
	strcpy(root,dest);
	EndChar(root,'\\');
	int i;
	bool someList=false;
	// search for last existing backup
	for( i=1; i<=99999999; i++ )
	{
		char buf[MAX_PATH];
		sprintf(buf,"%s%d\\" ListName,root,i);
		if( !CheckFileInfo(buf) ) break;
	}
	int last=i-1;

	if( last>0 )
	{
		char buf[MAX_PATH];
		sprintf(buf,"%s%d\\" ListName,root,last);
		if( LoadFileInfo(buf)>=0 ) someList=true;
	}
	if( someList ) return i;
	// no file list - we have to scan all previous directories and merge them
	for( i=1; i<=99999999; i++ )
	{
		char buf[MAX_PATH];
		sprintf(buf,"%s%d\\",root,i);
		Report("Scanning backup",buf);
		int ret=ScanDir(i,buf,buf);
		if( ret<0 ) return ret;
		// save backup information
		if( ret>0 ) break;
		SaveFileList(buf,DestFiles);
	}
	return i;
}



static HANDLE hConsoleOutput,hConsoleInput;

static void ExitCon()
{
	if( wasError )
	{
		DWORD ret;
		static const char text[]="\n-----------------------\nPress ENTER to continue";
		char buf[80];
		WriteConsole(hConsoleOutput,text,strlen(text),&ret,NULL);
		ReadConsole(hConsoleInput,buf,sizeof(buf),&ret,NULL);
	}
}

static int Restore=-1;

int main( int argc, const char *argv[] )
{
	char S[MAX_PATH];
	char D[MAX_PATH];

	CONSOLE_SCREEN_BUFFER_INFO csbi;
	BOOL bLaunched;
	
	// Lets try a trick to determine if we were 'launched' as a seperate
	// screen, or just running from the command line.
	// We want to do this so that when we exit, we can prompt the user
	// before we shut down if we were 'launched'. Otherwise, any data on
	// the output window will be lost.
	// We will do this by simply getting the current cursor position. It
	// 'should' always be (0,0) on a launch, and something else if we were
	// executed as a command from a console window. The only time I can see
	// this as not working, is if the user executed a CLS and appended
	// our program with the '&' character, as in:
	//   C:\> CLS & ConGUI
	// This will also result in a (0,0) cursor pos, but in this case, the
	// user might also be wanting us to 'prompt' before closeing.
	// We also need to handle the case of:
	//   C:\> ConGUI > output.dat

	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsoleOutput, &csbi);
	bLaunched = ((csbi.dwCursorPosition.X==0) && (csbi.dwCursorPosition.Y==0));
	if ((csbi.dwSize.X<=0) || (csbi.dwSize.Y <= 0)) bLaunched = FALSE;

	if( bLaunched ) atexit(ExitCon);
	
	if( argc<3 )
	{
		Usage:
		printf("Usage: backup [-restore <number>] <dest_path> <source_path> [<source_path>]\n");
	}
	else
	{
		argv++,argc--;
		if ( !strcmpi(*argv,"-restore") )
		{
			argv++,argc--;
			Restore=atoi(*argv);
			if( Restore<=0 ) goto Usage;
			argv++,argc--;
		}
		if( argc<2 ) goto Usage;
		strcpy(D,*argv++),argc--;
		EndChar(D,'\\');
		
		int numSave;
		if( Restore>=0 )
		{
			ScanDir(D); // if DestFiles are not created, create them
			numSave=Restore;
			DestFiles.resize(0);
			char list[MAX_PATH];
			sprintf(list,"%s%d\\" ListName,D,numSave);
			LoadFileInfo(list);
		}
		else numSave=ScanDir(D);
		if( numSave>0 )
		{
			char rootPath[MAX_PATH];
			sprintf(rootPath,"%s%d\\",D,numSave);
			while( argc>0 )
			{
				strcpy(S,*argv++),argc--;
				EndChar(S,'\\');

				if( Restore>=0 )
				{
					if( RestoreDir(D,S)<0 ) return -1;
				}
				else
				{		
					char destPath[MAX_PATH];
					strcpy(destPath,rootPath);
				
					// use full pathnames if possible
					if( S[1]==':' && S[2]=='\\' ) strcat(destPath,S+3);
					if( ConvertDir(numSave,rootPath,destPath,S)<0 ) return -1;
					EmptyLine();
				}
			}
			if( IsNew && Restore<0 ) SaveFileList(rootPath,FolderFiles);
			wasError=FALSE;
		}
	}
	#if _DEBUG
		wasError=1;
	#endif
	if( !wasError ) return 0;
	return -1;
}


