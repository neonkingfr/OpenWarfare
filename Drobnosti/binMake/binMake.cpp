#include <El/elementpch.hpp>
#include <Es/Framework/consoleBase.h>
#include <Es/Strings/rString.hpp>
#include <Es/Strings/bString.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Containers/array.hpp>
#include <sys/stat.h>
#include <direct.h>
#include <io.h>
#include <process.h>

//! rule describing how to make one file type from another
struct MakeRule
{
  RString _srcExt,_tgtExt;
  AutoArray<RString> _commands;
};

TypeIsMovable(MakeRule)

static AutoArray<MakeRule> Rules;

static bool BuildAll = false;
#if _DEBUG
static int Debug = 1000;
#else
static int Debug = 0;
#endif
static bool KeepGoing = false;


static bool GetLine(char *line, int lineSize, FILE *file, int &lineNum)
{
  *line = 0;
  char *ok = fgets(line,lineSize,file);
  line[lineSize-1]=0;
  if (line[0])
  {
    char *eLine = line+strlen(line);
    if (eLine[-1]=='\n')
    {
      eLine[-1]=0;
      lineNum++;
    }
  }
  return ok!=NULL;
}

/* rule example:
.p3d.bi:  
        x:\Finalize\Update\binarize.exe -o $@ $<
        
        
*/
/* fule format description:
{<sp>} is space or tab
.<source_extension>.<target_extension>:{<sp>}<eol>
<sp><command><eol>
<sp><command><eol>
{<sp>}<eol>
*/


static bool LoadRules(const char *from)
{
  bool rulesOk = true;
  FILE *file = fopen(from,"r");
  if (!file)
  {
    fprintf(stderr,"Cannot open rule file %s\n",from);
    return false;
  }
  
  // rule e
  int lineNum = 1;
  for(;;)
  {
    char line[1024];
    bool ok = GetLine(line,sizeof(line),file,lineNum);
    if (line[0]=='#') continue; // comment
    // line should not start with a space unless empty
    const char *act = line;
    if (isspace(*act))
    {
      while (isspace(*act)) act++;
      if (*act)
      {
        fprintf(
          stderr,"Line %d: Rule start expected instead of %s\n",
          lineNum,line
        );
        rulesOk = false;
      }
      break;
    }
    else if (*act=='.')
    {
      MakeRule rule;
      // scan extensions
      const char *srcExtStart = act;
      act++;
      while (*act!='.' && *act!=0 && *act!=':') act++;
      if (*act!='.')
      {
        fprintf(
          stderr,"Line %d: '%c' expected in '%s'\n",
          lineNum,'.',line
        );
        rulesOk = false;
        break;
      }
      rule._srcExt = RString(srcExtStart,act-srcExtStart);
      const char *tgtExtStart = act;
      act++;
      while (*act!='.' && *act!=0 && *act!=':') act++;
      if (*act!=':')
      {
        fprintf(
          stderr,"Line %d: '%c' expected in '%s'\n",
          lineNum,':',line
        );
        rulesOk = false;
        break;
      }
      rule._tgtExt = RString(tgtExtStart,act-tgtExtStart);
      // scan commands
      for(;;)
      {
        bool ok = GetLine(line,sizeof(line),file,lineNum);
        // ignore comments
        if (*line=='#') continue;
        if (*line==0) break;
        if (!isspace(*line))
        {
          fprintf(
            stderr,"Line %d: Space expected in '%s'\n",
            lineNum,line
          );
          rulesOk = false;
          break;
        }
        const char *cmdStart = line;
        while (isspace(*cmdStart)) cmdStart++;
        rule._commands.Add(cmdStart);
        if (!ok) break;
      }
      if (!rulesOk) break;
      Rules.Add(rule);
    }
    else if (*line)
    {
      fprintf(
        stderr,"Line %d: Rule expected instead of '%s'\n",
        lineNum,line
      );
      rulesOk = false;
      break;
    }
    
    if (!ok) break;
    
  }
  
  fclose(file);
  return rulesOk;
}

/*
following variables are recognized inside of <command>:
$< or $(<) full source path
$@ or $(@) full target path
$$ or $($) character $
*/

static RString SubstVariables(RString rule, const char *source, const char *target)
{
  // variables start with $
  RString ret;
  const char *act = rule;
  while (*act)
  {
    const char *nextVar = strchr(act,'$');
    if (nextVar)
    {
      ret = ret + RString(act,nextVar-act);
      char varName = nextVar[1];
      if (varName=='(')
      {
        varName = nextVar[2];
        if (varName==0 || nextVar[3]!=')')
        {
          fprintf(stderr,"Variable syntax error in %s\n",(const char *)rule);
          break;
        }
        nextVar += 4;
      }
      else
      {
        nextVar += 2;
      }
      switch (varName)
      {
        case '<':
          ret = ret + RString(source);
          break;
        case '@':
          ret = ret + RString(target);
          break;
        case '$':
          ret = ret + RString("$");
          break;
        default:
          fprintf(stderr,"Unknown variable %c in %s\n",varName,(const char *)rule);
          break;
      }
      act = nextVar;
    }
    else
    {
      ret = ret + RString(act);
      break;
    }
  }
  return ret;
}

static int CopyFile(const char *src, const char *dst)
{
  int ret = CopyFile(src,dst,FALSE);
  if (ret ==0)
  {
    char buf[1024]; buf[0] = '\0';
    DWORD err = GetLastError();
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, err, 0, buf, sizeof(buf), NULL);
    fprintf(stderr,"Copy of file %s -> %s fail with: %s",src,dst,buf);    
    return err;
  }
  return 0;
}

static int CopyFile(const char *args)
{
  // parse src/dst info
  // we expect both src and dst enclosed in "
  if (*args!='"')
  {
    fprintf(stderr,"'%s': Opening '\"' expected\n",args);
    return -1;
  }
  const char *srcEnd = strchr(args+1,'"');
  if (!srcEnd)
  {
    fprintf(stderr,"'%s': Closing '\"' expected\n",args);
    return -1;
  }
  const char *dstBeg = srcEnd+1;
  while (isspace(*dstBeg)) dstBeg++;
  if (*dstBeg!='"')
  {
    fprintf(stderr,"'%s': Opening '\"' expected\n",args);
    return -1;
  }
  const char *dstEnd = strchr(dstBeg+1,'"');
  if (!dstEnd)
  {
    fprintf(stderr,"'%s': Closing '\"' expected\n",args);
    return -1;
  }
  RString src(args+1,srcEnd-args-1);
  RString dst(dstBeg+1,dstEnd-dstBeg-1);
  return CopyFile(src,dst);
}

static int ApplyRule(const MakeRule &rule, const char *source, const char *target)
{
  int ret=0;
  for (int i=0; i<rule._commands.Size(); i++)
  {
    RString command = rule._commands[i];
    RString substCommand = SubstVariables(command,source,target);
    if (Debug>=3)
    {
      fprintf(stderr,"\tRule: %s\n",(const char *)command);
      fprintf(stderr,"\tExec: %s\n",(const char *)substCommand);
    }
    
    // parse command name
    RString exe;
    const char *args = substCommand;
    if (substCommand[0]=='"')
    {
      const char *endExe = strchr(args+1,'"');
      if (!endExe)
      {
        fprintf(stderr,"Error: Missing '\"' in %s\n",(const char *)substCommand);
        return 1;
      }
      exe = RString(args+1,endExe-substCommand-1);
      args = endExe+1;
    }
    else
    {
      const char *endExe = strchr(substCommand,' ');
      if (!endExe)
      {
        exe = substCommand;
        args = substCommand+strlen(substCommand);
      }
      else
      {
        exe = RString(substCommand,endExe-substCommand);
        args = endExe;
      }
    }
    while (isspace(*args)) args++;
    int err = 0;
    if (!stricmp(exe,"!copy"))
    {
      err = CopyFile(args);
    }
    else
    {
      // note: we assume _spawn or called process will separate arguments from args
      // this is true in Win32, but might not be true on other platforms
      err = _spawnlp(_P_WAIT,exe,substCommand,NULL,NULL);
    }
    if (err)
    {
      fprintf(stderr,"%s returned error %d\n",(const char *)exe,err);
      if (!KeepGoing) ret = err;
      break;
    }
  }
  return ret;
}


static int Make(const char *source, const char *target, const char *pattern, const char *targetExt = NULL)
{
  struct stat sStat;
  struct stat tStat;
  bool sourceIsDir = false;
  bool targetIsDir = false;
  unsigned sTime = 0;
  unsigned tTime = 0;
  if (stat(source,&sStat)==0)
  {
    sourceIsDir = (sStat.st_mode&_S_IFDIR)!=0;
    sTime = sStat.st_mtime;
  }
  if (stat(target,&tStat)==0)
  {
    targetIsDir = (tStat.st_mode&_S_IFDIR)!=0;
    tTime = tStat.st_mtime;
  }
  if (sTime==0 && !sourceIsDir)
  {
    // nothing to build - no source
    if (Debug>=3)
    {
      fprintf(stderr,"Source %s for %s not found\n",source,target);
    }
    return 0;
  }
  
  if (targetIsDir && sourceIsDir)
  {
    // if source and target are folders , update all files in target from source if necessary
    if (Debug)
    {
      fprintf(stderr,"Making dir %s to %s\n",source, target);
    }
    // scan target folder
    BString<512> wild;
    strcpy(wild,source);
    strcat(wild,"\\");
    if (!pattern)
    {
      strcat(wild,"*.*");
    }
    else
    {
      strcat(wild,pattern);
    }
    _finddata_t find;
    intptr_t h = _findfirst(wild,&find);
    int ret = 0;
    if (h>=0)
    {
      do
      {
        if (strcmp(find.name,".")==0 || strcmp(find.name,"..")==0)
        {
          continue;
        }
      	BString<512> tPath,sPath;
      	strcpy(tPath,target),strcpy(sPath,source);
      	strcat(tPath,"\\"),strcat(sPath,"\\");
      	strcat(tPath,find.name),strcat(sPath,find.name);
        if (targetExt)
          strcpy(GetFileExt(GetFilenameExt(tPath)),targetExt);

      	int err = Make(sPath,tPath,pattern);
      	if (err)
      	{
      	  ret=err;
      	  break;
      	}
      } while (_findnext(h,&find)==0);
      _findclose(h);
    }
    return ret;
    
  }
  else if (targetIsDir || sourceIsDir)
  {
    fprintf(
      stderr,"Error: %s %s a directory, %s %s\n",
      source,sourceIsDir ? "is" : "is not",
      target,targetIsDir ? "is" : "is not"
    );
    return -1;
  }
  
    
  // check if source is newer than target
  if (!BuildAll && tTime>=sTime)
  {
    if (Debug>=2)
    {
      fprintf(stderr,"%s is up to date (target %s)\n",source, target);
    }
    return 0; // nothing to make
  }
  // find corresponding rule
  const char *tExt = GetFileExt(GetFilenameExt(target));
  const char *sExt = GetFileExt(GetFilenameExt(source));
  // find corresponding rule
  for (int i=0; i<Rules.Size(); i++)
  {
    const MakeRule &rule = Rules[i];
    if (!strcmpi(rule._srcExt,sExt) && !strcmpi(rule._tgtExt,tExt))
    {
      if (Debug)
      {
        fprintf(stderr,"Making %s to %s\n",source, target);
      }
      return ApplyRule(rule,source,target);
    }
  }

  // no rule found
  // if source and target filename is the same, skip the file
  if (!stricmp(source,target))
  {
    return 0;
  }
  /*
  // if source and target extension is the same, perform simple copy
  if (!stricmp(GetFileExt(GetFilenameExt(source)),GetFileExt(GetFilenameExt(target))))
  {
    FILE 
    CopyFile();
  }
  */
  fprintf(stderr,"Do not know how to make %s from %s\n",target, source);
  return 1;
}

int consoleMain( int argc, const char *argv[] )
{
  const char *currDir = NULL;
  // check options
  argv++,argc--;
  while (argc>0)
  {
    const char *arg = *argv;
    if (*arg=='-')
    {
      arg++;
      argv++,argc--;
      if (!strcmp(arg,"B") || !strcmp(arg,"-always-make"))
      {
        BuildAll = true;
      }
      else if (!strcmp(arg,"d") || !strcmp(arg,"-debug"))
      {
        Debug = 1000;
      }
      else if (!strcmp(arg,"k") || !strcmp(arg,"-keep-going"))
      {
        KeepGoing = true;
      }
      else if (!strcmp(arg,"C") )
      {
        if (argc<1) goto Usage;
        currDir = *argv;
        argv++,argc--;
      }
      else
      {
        fprintf(stderr,"Unknown option %s",arg);
      }
    }
    else
    {
      break;
    }
  }
  // on no errors other than no arguments we want to wait for the keyboard
  if (argc>=2)
  {
    ignoreError = true;
  }
  if (argc<2 || argc>4)
  {
    Usage:
    fprintf(
      stderr,
      "Usage:\n"
      "        binMake [options] source_file target_file\n"
      "     or binMake [options] source_dir target_dir [wildcard [target_extension] ]\n"
      "\n"
      "Options\n"
      "\t-B or --always-make   Consider all targets out-of-date\n"
      "\t-d or --debug         Print debugging information in addition to normal processing\n"
      "\t-k or --keep-going    Continue as much as possible after an error\n"
      "\t-C                    Change to directory dir before running the rules\n"
    );
    if (argc>0)
    {
      fprintf(stderr,"\nArguments used:\n");
      while (argc>0)
      {
        fprintf(stderr,"  '%s'\n",*argv);
        argv++,argc--;
      }
      
    }
    return 1;
  }
  if (!LoadRules("binMakeRules.txt"))
  {
    fprintf(
      stderr,"Error in rules\n"
    );
    return 1;
  }
  char *source = strdup(*argv);
  if (source[strlen(source)-1]=='\\') source[strlen(source)-1] = '\0'; // filename for stat() from sys/stat.h cannot end by slash
  argv++,argc--;
  char *target = strdup(*argv);
  if (target[strlen(target)-1]=='\\') target[strlen(target)-1] = '\0'; // filename for stat() from sys/stat.h cannot end by slash
  argv++,argc--;
  const char *wild = NULL;
  if (argc>0)
  {
    wild = *argv;
    argv++,argc--;
  }
  const char *trgExt = NULL;
  if (argc>0)
  {
    trgExt = *argv;
    argv++,argc--;
  }
  if (currDir)
  {
    chdir(currDir);
  }
  // we expect: source name, target name
  // or source name, target name, wild mask and target extension for directory convertion 
  int ret = Make(source,target,wild,trgExt);
  return ret;
}
