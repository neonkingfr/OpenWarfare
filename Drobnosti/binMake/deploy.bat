@echo off
copy /y "Release\binMake.exe" "b:\BinMake\"
if errorlevel 1 pause

copy /y "Release\binMake.map" "b:\BinMake\"
if errorlevel 1 pause

copy /y "binMakeRules.txt" "b:\BinMake\"
if errorlevel 1 pause

copy /y "binMake.inf" "b:\BinMake\"
if errorlevel 1 pause

copy /y "Release\binMake.exe" "U:\Tools\BinMake\"
if errorlevel 1 pause

copy /y "Release\binMake.map" "U:\Tools\BinMake\"
if errorlevel 1 pause

copy /y "binMakeRules.txt" "U:\Tools\BinMake\"
if errorlevel 1 pause

copy /y "binMake.inf" "U:\Tools\BinMake\"
if errorlevel 1 pause
