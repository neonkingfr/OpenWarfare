#include <stdio.h>
#include <windows.h>
#include <Es/Containers/array.hpp>
#include <Es/Strings/rString.hpp>

class WorkerThread;

class IOverlapped: public RefCount
{
  public:
  virtual HANDLE GetHandle() = NULL;
  /// return false when failed
  virtual bool Perform() = NULL;
  virtual ~IOverlapped(){}
  virtual bool IsDone() = NULL;
  virtual void OnDone(WorkerThread *worker) {}
};

class OverlappedOpen: public IOverlapped
{
  RString _name;
  HANDLE _handle;
  public:
  OverlappedOpen(RString name)
  :_name(name)
  {
    _handle=NULL;
  }
  ~OverlappedOpen();
  HANDLE GetHandle()
  {
    Assert(_handle!=NULL);
    return _handle;
  }
  bool Perform();
  bool IsDone();
};

OverlappedOpen::~OverlappedOpen()
{
}

bool OverlappedOpen::Perform()
{
  _handle = ::CreateFile(
    _name,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,
    FILE_FLAG_RANDOM_ACCESS,NULL
  );
  return _handle!=INVALID_HANDLE_VALUE;
}

bool OverlappedOpen::IsDone()
{
  return _handle!=NULL;
}

class OverlappedRead: public IOverlapped
{
  Ref<IOverlapped> _source;
  size_t _offset;
  size_t _size;
  size_t _read;
  void *_buffer;
  bool _done;

  public:
  OverlappedRead(
    IOverlapped *source, size_t offset, size_t size, void *buffer
  )
  :_source(source),_offset(offset),_size(size),_buffer(buffer),
  _read(0),_done(false)
  {
  }
    
  public:
  bool Perform();
  HANDLE GetHandle();
  bool IsDone();
};


bool OverlappedRead::Perform()
{
  HANDLE handle = _source->GetHandle();
  DWORD read = 0;
  ::SetFilePointer(handle,_offset,NULL,FILE_BEGIN);
  BOOL ok = ReadFile(handle,_buffer,_size,&read,NULL);
  _read = read;
  _done = true;
  return ok!=FALSE;
}

HANDLE OverlappedRead::GetHandle()
{
  Assert(_done);
  return _source->GetHandle();
}

bool OverlappedRead::IsDone()
{
  return _done;
}

class OverlappedClose: public IOverlapped
{
  Ref<IOverlapped> _source;
  bool _done;

  public:
  OverlappedClose(IOverlapped *source)
  :_source(source),_done(false)
  {
  }
    
  public:
  bool Perform();
  HANDLE GetHandle() {return NULL;}
  bool IsDone() {return _done;}
};

bool OverlappedClose::Perform()
{
  Assert(!_done);
  HANDLE handle = _source->GetHandle();
  BOOL ok = CloseHandle(handle);
  _done = true;
  return ok!=FALSE;
}

class WorkerThread
{
  HANDLE _thread;
  /// item can never be desrtroyed unless already processed
  RefArray<IOverlapped> _queue;
  /// worker thread cannot manipulate the queue - it only increases processed count
  int _queueProcessed;
  CRITICAL_SECTION _queueLock;
  /// count submitted request
  HANDLE _submitSemaphore;
  HANDLE _deleteSemaphore;
  HANDLE _terminateEvent;

  public:
  WorkerThread();
  ~WorkerThread();
  void Prepare();
  void CleanUp();

  void DoWork();
  static DWORD WINAPI DoWorkCallback(void *context);

  IOverlapped *Submit(IOverlapped *req);
  void Wait(Ref<IOverlapped> req);

  IOverlapped *Open(const char *name)
  {
    return Submit(new OverlappedOpen(name));
  }
  IOverlapped *Read(
    IOverlapped *handle, size_t offset, size_t size, void *buffer
  )
  {
    return Submit(new OverlappedRead(handle,offset,size,buffer));
  }
  IOverlapped *Close(IOverlapped *handle)
  {
    return Submit(new OverlappedClose(handle));
  }

  void Maintain();
  void Update();
};

WorkerThread::WorkerThread()
{
  _thread = NULL;
  _queueProcessed = 0;
  _submitSemaphore = NULL;
  _deleteSemaphore = NULL;
  _terminateEvent = NULL;
}
WorkerThread::~WorkerThread()
{
  CleanUp();
}

void WorkerThread::Prepare()
{
  ::InitializeCriticalSection(&_queueLock);
  _submitSemaphore = ::CreateSemaphore(NULL,0,INT_MAX,NULL);
  _terminateEvent = ::CreateEvent(NULL,FALSE,FALSE,NULL);
  _queueProcessed = 0;
  DWORD threadId;
  _thread = ::CreateThread(NULL,16*1024,DoWorkCallback,this,0,&threadId);
}

void WorkerThread::CleanUp()
{
  if (_thread)
  {
    ::SetEvent(_terminateEvent);
    ::WaitForSingleObject(_thread,INFINITE);
    Maintain();
    ::DeleteCriticalSection(&_queueLock);
    ::CloseHandle(_thread),_thread = NULL;
    ::CloseHandle(_terminateEvent),_terminateEvent = NULL;
    ::CloseHandle(_submitSemaphore),_submitSemaphore = NULL;
  }
}

void WorkerThread::Wait(Ref<IOverlapped> req)
{
  while (!req->IsDone())
  {
    // ... we can do anything here
    Sleep(0);
    // we can update here - req keeps reference count
    Update();
  }
}

void WorkerThread::Maintain()
{
  // remove all request that are already done
  ::EnterCriticalSection(&_queueLock);
  if (_queueProcessed>0)
  {
    for (int i=0; i<_queueProcessed; i++)
    {
      _queue[i]->OnDone(this);
    }
    _queue.Delete(0,_queueProcessed);
    _queueProcessed = 0;
  }
  ::LeaveCriticalSection(&_queueLock);
}

void WorkerThread::Update()
{
  Maintain();
}

IOverlapped *WorkerThread::Submit(IOverlapped *req)
{
  ::EnterCriticalSection(&_queueLock);
  Maintain();
  _queue.Add(req);
  ::LeaveCriticalSection(&_queueLock);
  ::ReleaseSemaphore(_submitSemaphore,1,NULL);
  return req;
}

DWORD WINAPI WorkerThread::DoWorkCallback(void *context)
{
  WorkerThread *ctx = (WorkerThread *)context;
  ctx->DoWork();
  return 0;
}

void WorkerThread::DoWork()
{
  while(true)
  {
    HANDLE objs[]= {_submitSemaphore,_terminateEvent};
    DWORD ret = WaitForMultipleObjects(2,objs,FALSE,INFINITE);
    switch (ret)
    {
      case WAIT_OBJECT_0:
        {
          // we have some item to be processed
          EnterCriticalSection(&_queueLock);
          // get the item from the queue
          IOverlapped *item = _queue[_queueProcessed];
          LeaveCriticalSection(&_queueLock);
          // perfom may take very long
          item->Perform();
          EnterCriticalSection(&_queueLock);
          _queueProcessed++;
          LeaveCriticalSection(&_queueLock);
        }
        break;
      case WAIT_OBJECT_0+1:
        return;
    }
  }
}


int main(int argc, const char *argv[])
{
  WorkerThread worker;
  worker.Prepare();

  char buffer[100];

  IOverlapped *file = worker.Open("fileThread.cpp");
  IOverlapped *read = worker.Read(file,0,sizeof(buffer),buffer);
  IOverlapped *closeFile = worker.Close(read);

  worker.Wait(closeFile);
  worker.CleanUp();
  return 0;
}
