// CfgConvert.cpp : Defines the entry point for the console application.
//

#include <El/elementpch.hpp>
#include <El/ParamFile/paramFile.hpp>
#include <El/ParamXML/paramXML.hpp>

#include <string.h>
#include <stdio.h>
#include <time.h>


static bool LoadParamFile(ParamFile &f, const char *name)
{
  // autodetect format based on extension or content?
  if (f.ParseBin(name))
  {    
    return true;
  }
  if (ParseXML(f,name))
  {    
    return true;
  }
  if (f.Parse(name)==LSOK)
  {    
    return true;
  }
  return false;
}

struct ChangeFileNames
{
protected:
  const char * _from;
  const char * _to;
  int _fromLenght;
  int _toLenght;

  mutable char _tmp[1024];

public:

  ChangeFileNames(const char * from, const char * to): _from(from), _fromLenght(strlen(from)), 
    _to(to), _toLenght(strlen(to)) {};

  ChangeFileNames(const ChangeFileNames& src) {*this = src;};

  bool operator()(ParamEntryVal& entry) const
  {
    if (entry.IsClass())
    {
      entry.GetClassInterface()->ForEachEntry<ChangeFileNames>(*this);      
      return false; 
    }

    if (!entry.IsTextValue()) return false;
    const char * text = (const char *) (RStringB) entry; 
    int textLenght = strlen(text);
    if (textLenght < _fromLenght)
      return false; 

    char * tmp = _tmp;
    if (textLenght - _fromLenght + _toLenght + 1 > 1024)
    {
      tmp = new char[textLenght - _fromLenght + _toLenght + 1 ];
    }

    
    strcpy(tmp,text);
    strlwr(tmp); // we have to copy it 

    for(int i = 0; i < _fromLenght; i++)
    {
      if (tmp[textLenght - _fromLenght + i] != _from[i])
      {   
        if (tmp != _tmp)
          delete tmp;
        return false;
      }
    }  
   
    char * txEnd = tmp + (textLenght - _fromLenght);

    strcpy(txEnd,_to);
    (entry).GetModPointer()->SetValue(RStringB(_tmp));

    if (tmp != _tmp)
      delete tmp;
    return false;
  }
};

static void ReplacePath(ParamFile &f, const char *from, const char *to)
{
  f.GetClassInterface()->ForEachEntry<ChangeFileNames>(ChangeFileNames(from, to));
}

//void InitParamFilePreprocess();

int main(int argc, char* argv[])
{
  // initialization of Element library
  //InitParamFilePreprocess();
  //GGameState.Init();

  argv++;
  argc--;
  enum OutMode {Txt,Bin,Xml} outMode = Txt;
  const char *dst = NULL; 
  bool nextDst = false;
  if (argc < 1) goto Usage;
  int ret = 0;
  while (argc>0)
  {
    const char *arg = *argv++;
    argc--;
    if (*arg == '-' || *arg == '/')
    {
      arg++; // skip dash
      if (!stricmp(arg,"bin")) outMode = Bin;
      else if (!stricmp(arg,"txt")) outMode = Txt;
      else if (!stricmp(arg,"xml")) outMode = Xml;
      else if (!stricmp(arg,"dst")) nextDst = true;
      else goto Usage;
    }
    else if (nextDst)
    {
      dst = arg;
      nextDst = false;
    }
    else
    {
      ParamFile f;
      // read file in any format
      if (!LoadParamFile(f,arg))
      {
        fprintf(stderr,"Error reading binary file '%s'\n",arg);
        ret = 1;
        continue;
      }
      // perform file name replacement
      ReplacePath(f,".tga",".paa");
      ReplacePath(f,".png",".paa");
      // save it in format requested by the command line
 
      if (outMode==Bin)
      {
        bool result=f.SaveBin(dst ? dst : arg);
        if (result)printf("%s->%s\n",arg,dst ? dst : arg);
        else 
        {
          fprintf(stderr,"Error (saving file) %s -> %s\n",arg,dst ? dst : arg);
          ret = 1;
        }
        dst = NULL;
      }
      else if (outMode==Txt)
      {
        bool result=f.Save(dst ? dst : arg)==LSOK;
        if (result)printf("%s->%s\n",arg,dst ? dst : arg);
        else 
        {
          fprintf(stderr,"Error (saving file) %s -> %s\n",arg,dst ? dst : arg);
          ret = 1;
        }
        dst = NULL;
      }
      else if (outMode==Xml)
      {
        bool result=SaveXML(f,dst ? dst : arg);
        if (result)printf("%s->%s\n",arg,dst ? dst : arg);
        else 
        {
          fprintf(stderr,"Error (saving file) %s -> %s\n",arg,dst ? dst : arg);
          ret = 1;
        }
        dst = NULL;
      }
    }

  }
  return ret;

Usage:
  printf("Usage\n");
  printf("   cfgConvertFileChng [-bin | -txt | -xml] {[-dst <destination>] <source>}\n");
  return 1;		
}

/*
#include <Es/Memory/normalNew.hpp>

void *operator new ( size_t size, const char *file, int line )
{
return malloc(size);
}
*/
