// CopyKeyStore.cpp : Defines the entry point for the console application.
//

#pragma warning(disable: 4996)

#include "stdafx.h"

#include <Es/essencepch.hpp>
#include <Es/Common/win.h>
#include <Es/Strings/rString.hpp>

int _tmain(int argc, _TCHAR* argv[])
{
  if (argc != 2)
  {
    printf("Usage: \n");
    printf("\tCopyKeyStore source \n");
    return 1;
  }
  RWString src = argv[1];

  RWString path = _T("SOFTWARE\\JavaSoft\\Java Runtime Environment");
  HKEY key;
  if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, path, 0, KEY_READ, &key) != ERROR_SUCCESS)
  {
    printf("Cannot open registry entry '%s'", (const wchar_t *)(path));
    return 2;
  }

  BYTE buf[1024];
  DWORD bufSize = sizeof(buf);
  bool ok = ::RegQueryValueEx(key, _T("CurrentVersion"), NULL, NULL, buf, &bufSize) == ERROR_SUCCESS;
  ::RegCloseKey(key);
  if (!ok)
  {
    printf("Cannot read registry entry '%s\\CurrentVersion'\n", (const wchar_t *)(path));
    return 3;
  }

  RWString version = (wchar_t *)(buf);
  path = path + RWString(_T("\\")) + version;

  if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, path, 0, KEY_READ, &key) != ERROR_SUCCESS)
  {
    printf("Cannot open registry entry '%s'\n", (const wchar_t *)(path));
    return 4;
  }

  bufSize = sizeof(buf);
  ok = ::RegQueryValueEx(key, _T("JavaHome"), NULL, NULL, buf, &bufSize) == ERROR_SUCCESS;
  ::RegCloseKey(key);
  if (!ok)
  {
    printf("Cannot read registry entry '%s\\JavaHome'\n", (const wchar_t *)(path));
    return 5;
  }

  RWString dstDir = (const wchar_t *)(buf);
  RWString dst = dstDir + RWString(_T("\\lib\\security\\user_v2.ks"));

  if (!::CopyFile(src, dst, FALSE))
  {
    printf("Cannot copy file '%s' to '%s'\n", (const wchar_t *)(src), (const wchar_t *)(dst));
    return 6;
  }

	return 0;
}

