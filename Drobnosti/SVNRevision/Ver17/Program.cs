using System;
using System.Collections.Generic;
using System.Text;

using SharpSvn;

namespace SVNRevision
{
  class Program
  {
    static void Main(string[] args)
    {
      if (args.Length != 1) return;

      SvnClient client = new SvnClient();
      SharpSvn.UI.SvnUI.Bind(client, (System.Windows.Forms.IWin32Window)null);
      SvnTarget target = SvnTarget.FromString(args[0]);
      SvnInfoEventArgs info;
      if (!client.GetInfo(target, out info)) return;
      long revision = info.Revision;
      Console.Write(revision.ToString());
    }
  }
}
