// MakefileGen.cpp : Defines the entry point for the console application.
//

#include <El/elementpch.hpp>

#include <Es/Common/win.h>
#include <Es/Strings/rString.hpp>
#include <Es/Containers/forEach.hpp>

#include <El/XML/inet.hpp>
#include <El/XML/xml.hpp>
#include <El/QStream/qStream.hpp>
#include <El/ParamFile/paramFile.hpp>

//! free Win32 allocated memory when variable is destructed
class AutoFree
{
  //! handle to Win32 memory obtained by GlobalAlloc call
  void *_mem;

private:
  //! no copy constructor
  AutoFree(const AutoFree &src);

public:
  //! attach Win32 memory handle
  /*!
  \param mem handle to Win32 memory obtained by GlobalAlloc call
  */
  void operator = (void *mem)
  {
    if (mem==_mem) return;
    Free();
    _mem = mem;
  }
  //! copy - reassing Win32 handle to new object
  /*! source will be NULL after assignement */
  void operator = (AutoFree &src)
  {
    if (src._mem == _mem) return;
    Free();
    _mem = src._mem;
    src._mem = NULL;
  }
  //! empty constructor
  AutoFree(){_mem = NULL;}
  //! construct from Win32 memory handle
  AutoFree(void *mem){_mem = mem;}
  //! convert to pointer
  operator void * () {return _mem;}
  //! destruct - free pointer
  ~AutoFree(){Free();}
#ifdef _WIN32
  //! free pointer explicitelly
  void Free(){if (_mem) GlobalFree(_mem),_mem = NULL;}
#else
  //! free pointer explicitelly
  void Free(){if (_mem) free(_mem),_mem = NULL;}
#endif
};

class ExtractFiles: public SAXParser
{
private:
  AutoArray<RString> fileList;
  RString _curFileName; //last filename got from tag File attribute RelativePath
  const char *configuration;
public:
  ExtractFiles(const char *filename, const char *config)
  {
    QIFStream in;
    in.open(filename);
    if (in.fail()) {return;}
    configuration=config;
    //printf("Parsing project file: %s, configuration: %s\n", filename, configuration);
    //initialize parser
    Parse(in);
  }

public:
  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "file") == 0) {
      XMLAttribute *ppr = attributes.Find("RelativePath");
      if (ppr) {
         _curFileName = ppr->value;
//         LogF("<%s RelativePath=\"%s\">",cc_cast(name), cc_cast(_curFileName));
//         if (stricmp(cc_cast(_curFileName),".\\d3d9\\Shaders\\psPostProcessStencilShadowsPre.psa")==0)
//           _asm nop;
      }
      else {
        _curFileName = "";
        LogF("Warning: No RelativePath attribute of some File tag!");
      }
    }
    else if (stricmp(name, "fileconfiguration") == 0) {
      XMLAttribute *name = attributes.Find("Name");
//      if (name) LogF("<FileConfiguration Name=\"%s\">", cc_cast(name->value));
      if (name && stricmp(name->value,configuration)==0)
      {
        XMLAttribute *excluded = attributes.Find("ExcludedFromBuild");
        if (excluded) {
          if (stricmp(excluded->value,"true")==0) _curFileName="";
        }
      }
    }

  }
  virtual void OnEndElement(RString name)
  {
    if (stricmp(name, "file") == 0) {
      if (_curFileName!=RString("")) fileList.Add(_curFileName);
    }
  }
  virtual void OnCharacters(RString chars)
  {
  }
  AutoArray<RString> GetFiles() {return fileList;}
};

#include <El/Pathname/Pathname.h>
bool PrintFileName(RString filename)
{
  Pathname path(filename);
  if (stricmp(path.GetExtension(),".cpp")==0 || stricmp(path.GetExtension(),".c")==0)
    printf("%s\n", path.GetFilename());
  return false;
}


#define MAKEFILE_RESISTANCE_SERVER 1
#if MAKEFILE_RESISTANCE_SERVER
const char PathPrefix[] = ".\\..\\ResistanceServer\\";
#else
const char PathPrefix[] = ".\\Release\\";
#endif

class PrintDependenceLine
{
public:
  PrintDependenceLine() {};
  bool operator()(RString filename) const
  {
    Pathname path(filename);
    if (stricmp(path.GetExtension(),".cpp")==0 || stricmp(path.GetExtension(),".c")==0)
    {
      RString line = path.GetTitle(); 
      line = RString(PathPrefix) + line+".obj: ";
      line = line + cc_cast(filename);
      printf("%s\n\t$(CPP) $(CPPFLAGS) $**\n", cc_cast(line));
    }
    return false;
  }
};

#if MAKEFILE_RESISTANCE_SERVER

const char OutputExe[] = "o:\\FPSuperRelease\\OFPR_Server.exe";
const char CPPFlags[] = "CPPFLAGS= /Og /Ob1 /Oi /Os /Oy /G6 "
   "/I \"W:\\c\" /I \"W:\\c\\PhysicalLibs\\RigidBody\\ODE\" /I \"W:\\c\\extern\" "
   "/D \"_RELEASE=1\" /D \"NDEBUG\" /D \"WIN32\" /D \"_WINDOWS\" /D \"_SUPER_RELEASE=1\" "
   "/D \"_RESISTANCE_SERVER=1\" /GF /FD /EHsc /ML /GS /Gy /GR /Fp\".\\../ResistanceServer/lib.pch\" "
   "/Fo\".\\../ResistanceServer/\" /Fd\".\\../ResistanceServer/vc70.pdb\" /W3 /c /Zi";
const char LFlags[] = "LFLAGS= /INCREMENTAL:NO /NOLOGO /NODEFAULTLIB:\"libcmt.lib\" /DELAYLOAD:\"mpiwin32.dll\" "
   "/DEBUG /PDB:\".\\../ResistanceServer/lib.pdb\" /MAP:\"O:\\FPSuperRelease\\OFPR_Server.map\" "
   "/SUBSYSTEM:WINDOWS /STACK:8388608,1048576 /OPT:REF /MACHINE:I386 /LIBPATH:w:\\c\\extern";
const char Libs[] = "LIBS= Delayimp.lib ddraw.lib vorbisfile.lib winmm.lib dsound.lib dxguid.lib dinput.lib version.lib rsa.lib largeint.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib DelayImp.lib";

#else

const char OutputExe[] = "Release\\MakefileGen.exe";
const char CPPFlags[] = "CPPFLAGS= /O2 /I \"w:\\c\" /D \"WIN32\" /D \"NDEBUG\" /D "
"\"_CONSOLE\" /D \"_RELEASE\" /D \"MFC_NEW\" /D \"_MBCS\" /FD /ML /GS /GR /Fo\"Release/\" /W3 /nologo /c /Zi /TP";
const char LFlags[] = "LFLAGS= /INCREMENTAL:NO /NOLOGO "
"/DEBUG /SUBSYSTEM:CONSOLE /OPT:REF /OPT:ICF /MACHINE:X86 /LIBPATH:w:\\c\\extern";
const char Libs[] = "LIBS= kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib";

#endif

//deletes all specified chars from text
void DeleteChars(RString &txt, char c)
{
  char *text=txt.MutableData();
  int ix=0;
  for (int i=0, size=txt.GetLength(); i<size; i++)
  {
    if (text[i]==c) continue;
    text[ix++]=text[i];
  }
  text[ix]=0;
}

RString ReadFile(const char *filname, bool deleteChars=false)
{
  const char *filename = filname;
  QIFStream in; in.open(filename);
  if (in.fail()) return RString("");
  int size = QFBankQueryFunctions::GetFileSize(filename);
  RString output; output.CreateBuffer(size+1);
  in.read(output.MutableData(), size); output.MutableData()[size]=0;
  if (deleteChars) DeleteChars(output, 0x0d);
  return output;
}

struct MakefileInfo
{
  char *path;           //starting from current directory eg. "Es\lib\Makefile"
  char *pathPrefix;     //starting from Poseidon\lib eg. "..\..\Es\" 
  char *excludePrefix;  //starting from Poseidon\lib eg. ".\GameSpy\" 
  char *insertPrefix;   //starting from XXX\lib, one should sometimes add a prefix ../
  //MakefileInfo(char *pth, char *pthPref, char *exclPref) : path(pth), pathPrefix(pthPref), excludePrefix(exclPref) {}
};

#define FILE_SERVER_TEST 0
#define I_AM_ARMA2 1
#if I_AM_ARMA2
#if FILE_SERVER_TEST
const int MakefilesCount = 3;
static MakefileInfo Makefiles[MakefilesCount] = {
  {"Es\\lib\\Makefile","W:\\C\\Poseidon\\Es\\","","../"},
  {"El\\lib\\Makefile","W:\\C\\Poseidon\\El\\","","../"},
  {"Poseidon\\lib\\Makefile",".\\",".\\GameSpy\\",""}
};
static MakefileInfo OrphanFiles = {"", "..\\", "", ""};
#else
const int MakefilesCount = 3;
static MakefileInfo Makefiles[MakefilesCount] = {
  {"Es\\lib\\Makefile","..\\Es\\","","../"},
  {"El\\lib\\Makefile","..\\El\\","","../"},
  {"Poseidon\\lib\\Makefile",".\\","",""} //we want GameSpy to be the part of the project, not the standalone library
  //{"Poseidon\\lib\\Makefile",".\\",".\\GameSpy\\",""}
};
static MakefileInfo OrphanFiles = {"", "..\\", "", ""};
#endif
#else //arma1
#if FILE_SERVER_TEST
const int MakefilesCount = 3;
static MakefileInfo Makefiles[MakefilesCount] = {
  {"Es\\lib\\Makefile","W:\\C\\Es\\","","../"},
  {"El\\lib\\Makefile","W:\\C\\El\\","","../"},
  {"Poseidon\\lib\\Makefile",".\\",".\\GameSpy\\",""}
};
static MakefileInfo OrphanFiles = {"", "..\\", "", ""};

#else
const int MakefilesCount = 3;
static MakefileInfo Makefiles[MakefilesCount] = {
  {"Es\\lib\\Makefile","..\\..\\Es\\","","../"},
  {"El\\lib\\Makefile","..\\..\\El\\","","../"},
  {"Poseidon\\lib\\Makefile",".\\","",""} //we want GameSpy to be the part of the project, not the standalone library
  //{"Poseidon\\lib\\Makefile",".\\",".\\GameSpy\\",""}
};
static MakefileInfo OrphanFiles = {"", "..\\", "", ""};
#endif
#endif

//case insensitive FindArray traits for MakeDirectory class
struct MakeDirectoryKeyTraits
{
  typedef const RString &KeyType;
  /// check if two keys are equal
  static bool IsEqual(KeyType a, KeyType b) {return (stricmp(a,b)==0);}
  /// get a key from an item
  static KeyType GetKey(const RString &a) {return a;}
};

class MakeDirectory : public FindArrayKey<RString, MakeDirectoryKeyTraits>
{
public:
  int Add(RString dir)
  {
    int ix=FindKey(dir);
    if (ix!=-1) {
      if (dir!=operator[](ix)) 
        LogF("Warning: case sensitive difference in directory names %s, %s",cc_cast(dir), cc_cast(operator[](ix)));
      return ix;
    }
    return FindArrayKey<RString,MakeDirectoryKeyTraits>::Add(dir);
  }
};

void ReplaceChar(RString &str, char from, char to)
{
  char *text = str.MutableData();
  for (int i=0, siz=str.GetLength(); i<siz; i++)
  {
    if (text[i]==from) text[i]=to;
  }
}

//Get all directories from fileList, matching Makefiles[ix].pathPrefix
RString GetDirectories(AutoArray<RString> &fileList, int ix)
{
  MakeDirectory dirlist;
  for (int i=0; i<fileList.Size(); i++)
  {
    const char *file = cc_cast(fileList[i]);
    Pathname path(file);
    if (strnicmp(file, Makefiles[ix].pathPrefix, strlen(Makefiles[ix].pathPrefix))==0)
    { //this adept matches pathPrefix
      if (Makefiles[ix].excludePrefix[0]!=0 && strnicmp(file, Makefiles[ix].excludePrefix, strlen(Makefiles[ix].excludePrefix))==0) continue;
      //check extension (should be *.c or *.cpp)
      if (stricmp(path.GetExtension(), ".cpp")==0 || stricmp(path.GetExtension(), ".c")==0)
      {
        RString filename(path.GetFilename());
        RString filepath(file);
        int from = strlen(Makefiles[ix].pathPrefix), to = filepath.GetLength()-filename.GetLength()-1;
        if (from<to) {
          RString relativeDir = Makefiles[ix].insertPrefix + filepath.Substring(from,to);
          ReplaceChar(relativeDir, '\\', '/');
          dirlist.Add(relativeDir);
        }
      }
    }
    else
    { // it can be possibly file in some directory NOT LISTED in makefiles pathPrefixes
      if (ix == MakefilesCount-1) //last Makefile should contain also all other files not matching pathPrefixes
      {
        // it should not match any of the previous pathPrefixes
        bool skipIt = false;
        for (int k=0; k<ix; k++)
        {
          if (strnicmp(file, Makefiles[k].pathPrefix, strlen(Makefiles[k].pathPrefix))==0) { skipIt=true; break; }
        }
        // it should match OrphanFiles pathPrefix
        if (skipIt || strnicmp(file, OrphanFiles.pathPrefix, strlen(OrphanFiles.pathPrefix))!=0 ) continue; // process next file
        //check extension (should be *.c or *.cpp)
        if (stricmp(path.GetExtension(), ".cpp")==0 || stricmp(path.GetExtension(), ".c")==0)
        {
          RString filename(path.GetFilename());
          RString filepath(file);
          int from = 0;
          int to = filepath.GetLength()-filename.GetLength()-1;
          if (from<to) {
            RString relativeDir = OrphanFiles.insertPrefix + filepath.Substring(from,to);
            ReplaceChar(relativeDir, '\\', '/');
            dirlist.Add(relativeDir);
          }
        }
      }
    }
  }
  RString output;
  if (dirlist.Size()) output=dirlist[0];
  for (int i=1; i<dirlist.Size(); i++) output = output + ":" + dirlist[i];
  //output = output + "\n";
  return output;
}

//Get all files from fileList, matching Makefiles[ix], having extension specified
RString GetFiles(AutoArray<RString> &fileList, const char *ext, int ix)
{
  RString output;
  const int LineSizeThreshold = 60;
  int len=0;  
  for (int i=0; i<fileList.Size(); i++)
  {
    const char *file = cc_cast(fileList[i]);
    Pathname path(file);
    if (strnicmp(file, Makefiles[ix].pathPrefix, strlen(Makefiles[ix].pathPrefix))==0)
    { //this adept matches pathPrefix
      if (Makefiles[ix].excludePrefix[0]!=0 && strnicmp(file, Makefiles[ix].excludePrefix, strlen(Makefiles[ix].excludePrefix))==0) continue;
      //check extension
      if (stricmp(path.GetExtension(), ext)==0)
      {
        if (len>LineSizeThreshold) 
        {
          len=0;
          output = output + " \\\n";
        }
        const char *fileName = path.GetFilename();
        len += strlen(fileName);
        output = output + fileName + " ";
      }
    }
    else
    { // it can be possibly file in some directory NOT LISTED in makefiles pathPrefixes
      if (ix == MakefilesCount-1) //last Makefile should contain also all other files not matching pathPrefixes
      {
        // it should not match any of the previous pathPrefixes
        bool skipIt = false;
        for (int k=0; k<ix; k++)
        {
          if (strnicmp(file, Makefiles[k].pathPrefix, strlen(Makefiles[k].pathPrefix))==0) { skipIt=true; break; }
        }
        // it should match OrphanFiles pathPrefix
        if (skipIt || strnicmp(file, OrphanFiles.pathPrefix, strlen(OrphanFiles.pathPrefix))!=0 ) continue; // process next file
        //check extension matching paramter ext
        if ( stricmp(path.GetExtension(), ext)==0 )
        {
          if (len>LineSizeThreshold) 
          {
            len=0;
            output = output + " \\\n";
          }
          const char *fileName = path.GetFilename();
          len += strlen(fileName);
          output = output + fileName + " ";
        }
      }
    }
  }
  output = output + "\n";
  return output;
}

void GenerateMakeFiles(AutoArray<RString> &fileList)
{
  for (int i=0; i<MakefilesCount; i++)
  {
    RString path=Makefiles[i].path; 
    RString pathProt = path + ".prot";  //read prototype
    RString makefile = ReadFile(cc_cast(pathProt),true);
    if (makefile==RString(""))
    {
      LogF("File not found: %s", cc_cast(pathProt));
      continue;
    }
    FILE *out = fopen(cc_cast(path), "wt");
    RString directories = GetDirectories(fileList, i);
    RString cppSources = GetFiles(fileList, ".cpp", i);
    RString cSources = GetFiles(fileList, ".c", i);
    if (out!=NULL)
    {
      fprintf(
          out, cc_cast(makefile),
          cc_cast(directories),
          cc_cast(cppSources),
          cc_cast(cSources)
      );
      fclose(out);
    }
    else{
      LogF("Cannot open file for writing: %s", cc_cast(path));
      continue;
    }
  }
}

int main(int argc, char* argv[])
{
  if (argc!=3)
  {
    printf("Usage: MakefileGen.exe project.vcproj \"configurationname\"\n");
    return 1;
  }
  ExtractFiles parser(argv[1], argv[2]);
  AutoArray<RString> fileList = parser.GetFiles();
  //Macros
  printf("%s\n%s /OUT:\"%s\"\n%s\n\n", CPPFlags, LFlags, OutputExe, Libs);
  //Objs
  printf("Objs=");
  bool notFirst=false;
  for (int i=0; i<fileList.Size(); i++)
  {
    Pathname path(fileList[i]);
    if (stricmp(path.GetExtension(),".cpp")==0 || stricmp(path.GetExtension(),".c")==0)
    {
      if (notFirst) printf(" \\\n");
      notFirst=true;
      RString obj = path.GetTitle(); 
      obj = RString(PathPrefix) + obj + ".obj";
      printf("%s", cc_cast(obj));
    }
  }
  printf("\n\n");
  //Executable rule
#if MAKEFILE_RESISTANCE_SERVER
  printf("%s: $(Objs)\n\tlink $(LFLAGS) $(Objs) %sresource.res $(LIBS)\n\n", OutputExe, PathPrefix);
#else
  printf("%s: $(Objs)\n\tlink $(LFLAGS) $(Objs) $(LIBS)\n\n", OutputExe);
#endif
  //Inferred dependants
  printf(".cpp.obj:\n\t$(CPP) $(CPPFLAGS) $<\n\n");
  //Dependencies
  PrintDependenceLine prn;
  fileList.ForEachF(prn);
  
  //Generate makefiles
  GenerateMakeFiles(fileList);
  return 0;
}
