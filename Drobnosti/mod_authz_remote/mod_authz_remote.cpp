/*
    mod_authz_remote.c
*/

//#define AP_HAVE_DESIGNATED_INITIALIZER

#include "httpd.h"
#include "http_core.h"
#include "http_config.h"
#include "http_protocol.h"
#include "http_request.h"
#include "http_log.h"
#include "ap_config.h"
#include "apr_network_io.h"
#include "apr_tables.h"

#define DEFAULT_AUTH_SERVER_PORT 11044

#define DECISION_FAILURE 0
#define DECISION_GRANTED 1
#define DECISION_DENIED 2

/*
typedef struct server_request_ {
    unsigned char protocol_version;
    unsigned char username_len;
    unsigned char access_type_len;
    unsigned char permission_resource_len;
    unsigned char path1_len;
    unsigned char path2_len;
    unsigned char data[0];
} server_request;
*/

/* forward declarations */
static int authz_handler(request_rec *r);


typedef struct {
    const char *server_spec;
    apr_sockaddr_t *server;
    int pathlen;
    char *path;		/* path for this configuration */
} my_directory_config;


typedef void (*error_callback_t)(void *cx, char *message);

static void request_error_callback(void *cx, char * message) {
    request_rec *r = (request_rec *)cx;
    ap_log_rerror(APLOG_MARK,APLOG_DEBUG,0,r,"%s",message);
}


static void *create_my_directory_config(apr_pool_t *pool, char *dir) {	/* dir is dummy, need to set path from command */

    my_directory_config *new_directory_config = static_cast<my_directory_config *>(apr_pcalloc(pool,sizeof(my_directory_config)));

    new_directory_config->server = NULL;
    new_directory_config->path = NULL;

    return new_directory_config;
}


static char *error_message(apr_pool_t *pool, apr_status_t code) {
    char *buffer = static_cast<char*>(apr_pcalloc(pool,256));
    if (buffer) {
        apr_strerror(code,buffer,256);
    }
    return buffer;
}


static void register_my_hooks(apr_pool_t *pool) {

    static const char *const these_after_me[] = {"mod_authz_user.c", NULL}; /* list of modules that we want to be called after us */

    ap_hook_auth_checker(authz_handler, NULL, these_after_me, APR_HOOK_MIDDLE);	/* register authorization handler */
}


static const char* cmd_DynamicAuthorizationServer( cmd_parms *cmdp, void *cfg, const char *value) {
    my_directory_config *dconf = (my_directory_config *)cfg;

    dconf->pathlen = strlen(cmdp->path);
    dconf->path = cmdp->path;
    dconf->server = NULL;
    dconf->server_spec = value;
    /* Parse hostname and port number */
    {
        char *hostname = NULL;
        char *scope_id = NULL;
        apr_port_t port = DEFAULT_AUTH_SERVER_PORT;

        if (apr_parse_addr_port(&hostname,&scope_id,&port,value,cmdp->pool) != APR_SUCCESS) {
            return "Failed to parse address";
        }
    
        if (apr_sockaddr_info_get(&dconf->server, hostname, APR_UNSPEC, port, APR_IPV4_ADDR_OK, cmdp->pool) != APR_SUCCESS) {
          return "Failed to create sockaddr";
        }
    }
    return NULL;
}




/* Dispatch list for API hooks */

static const command_rec my_commands[] = {
    AP_INIT_TAKE1("DynamicAuthorizationServer",(cmd_func)&cmd_DynamicAuthorizationServer, NULL, OR_AUTHCFG, "Dynamic access control server hostname:port"),
    { NULL }
};



/*extern "C" __attribute__((__visibility__("default")))*/ module AP_MODULE_DECLARE_DATA mod_authz_remote_module = {
    STANDARD20_MODULE_STUFF,
    create_my_directory_config,       /* create per-dir    config structures */
    NULL,                             /* merge  per-dir    config structures */
    NULL,                             /* create per-server config structures */
    NULL,                             /* merge  per-server config structures */
    my_commands,                      /* table of config file commands       */
    register_my_hooks                 /* register hooks                      */
};


/* return APR_APR_SUCCESS on success, any other on failure */
static int receive_full_buffer_or_die(apr_socket_t *socket, char *buffer, apr_size_t len) {
	apr_size_t done;
	apr_size_t received;
	apr_status_t rc;
	done = 0;
	while (done < len) {
		received = len - done;
		rc = apr_socket_recv(socket, &buffer[done], &received);
		if (rc != APR_SUCCESS) {
			return rc;
		}
		done += received;
	}
	return APR_SUCCESS;
}

static int ask_server_if_permitted(apr_sockaddr_t *server, const char *username, const char *access_type, const char *permission_resource, const char *path1, const char *path2, error_callback_t err, void *err_cx) {
    int decision = DECISION_FAILURE;
    apr_pool_t *pool;
    int username_len = 0;
    int access_type_len = 0;
    int permission_resource_len = 0;
    int path1_len = 0;
    int path2_len = 0;
    int buffer_size;
    char *buffer;
    apr_socket_t *socket;

    if (username) {
        username_len = strlen(username);
    }
    
    if (access_type) {
        access_type_len = strlen(access_type);
    }
    
    if (permission_resource) {
        permission_resource_len = strlen(permission_resource);
    }

    if (path1) {
        path1_len = strlen(path1);
    }
    
    if (path2) {
        path2_len = strlen(path2);
    }
    
    if ( (username_len <= 255) && (access_type_len <= 255) && (permission_resource_len <= 255) && (path1_len <= 255) && (path2_len <= 255)) {
    
        buffer_size = 6 + username_len + access_type_len + permission_resource_len + path1_len + path2_len;
    
        if (apr_pool_create(&pool,NULL)==APR_SUCCESS) {
            /* build request to send */
            if (buffer = static_cast<char *>(apr_palloc(pool,buffer_size))) {
                buffer[0] = 1; /* request format version 1 */
                buffer[1] = username_len;
                buffer[2] = access_type_len;
                buffer[3] = permission_resource_len;
                buffer[4] = path1_len;
                buffer[5] = path2_len;
                if (username_len) {
                    memcpy(&buffer[6], username, username_len);
                }
                if (access_type_len) {
                    memcpy(&buffer[6+username_len], access_type, access_type_len);
                }
                if (permission_resource_len) {
                    memcpy(&buffer[6+username_len+access_type_len], permission_resource, permission_resource_len);
                }
                if (path1_len) {
                    memcpy(&buffer[6+username_len+access_type_len+permission_resource_len], path1, path1_len);
                }
                if (path2_len) {
                    memcpy(&buffer[6+username_len+access_type_len+permission_resource_len+path1_len], path2, path2_len);
                }
                
                if (apr_socket_create(&socket, server->family, SOCK_STREAM, APR_PROTO_TCP, pool)==APR_SUCCESS) {
                    if (apr_socket_opt_set(socket,APR_SO_REUSEADDR,1)==APR_SUCCESS) {
                        if (apr_socket_connect(socket,server)==APR_SUCCESS) {
                            if (1/*apr_socket_timeout_set(socket,0)==APR_SUCCESS*/) {
                                /*err(err_cx, "Connected");*/
                                apr_size_t tosend = buffer_size;
                                if ((apr_socket_send(socket,buffer,&tosend)==APR_SUCCESS) && (tosend == buffer_size)) {
                                    /*err(err_cx, "Sent OK");*/

                                    buffer[0] = 0;
                                    if (receive_full_buffer_or_die(socket,buffer,1)==APR_SUCCESS) {
                                        /*err(err_cx, "Received OK");*/

                                        switch (buffer[0]) {
                                            case 1:
                                                decision = DECISION_DENIED;
                                                break;
                                            case 2:
                                                decision = DECISION_GRANTED;
                                        }
                                    }
                                }
                            }
                            apr_socket_close(socket);
                        }
                    }
                }
            }
        }
    }
    apr_pool_destroy(pool);
    return decision;
}


/*
 * Authentication handler:
 * - irrelevant: return DECLINE;
 * - access granted: return OK;
 * - access denied: ap_note_auth_failure(r); return HTTP_UNAUTHORIZED;
 */
 

static int authz_handler(request_rec *r) {
    /* Get configuration record */
    my_directory_config *dconf = static_cast<my_directory_config *>(ap_get_module_config(r->per_dir_config, &mod_authz_remote_module));

    const char *user;
    const char *access_type;
    const char *path1;
    int path1_len = 0;
    const char *path2;
    int decision;
    int x;
    
    const apr_array_header_t *requires = ap_requires(r); 
    
    user = r->user;

    path1 = r->uri;     /* TODO: canonicalize? */
    if (path1) {
        path1_len = strlen(path1);
    }
    if (strncmp(dconf->path,r->uri,dconf->pathlen)==0) {
        path1 += dconf->pathlen;
    } else {
        return DECLINED;    /* not our path?! wtf */
    }

    
    path2 = apr_table_get(r->headers_in,"Destination");


    ap_log_rerror(APLOG_MARK,APLOG_DEBUG,0,r,"protocol: %s hostname: %s method: %s uri: %s user: %s path: %s server: %s authpath: %s",r->protocol,r->hostname,r->method,r->uri,r->user,dconf->path,dconf->server_spec,path1);

    if (requires) {
        for (x=0; x<requires->nelts; x++) {
            const char *rest;
            const char *requires_keyword;
            const char *permission_resource;
            
            require_line *reqline = (require_line *)requires->elts;

            /* skip if Limit does not apply for this method */
            if (!(reqline[x].method_mask & (AP_METHOD_BIT << (r->method_number)))) {
                continue;
            }

            rest = reqline[x].requirement;
            requires_keyword = ap_getword_white(r->pool,&rest);
            
            if (requires_keyword && (strcmp(requires_keyword,"dynamic")==0)) {
                access_type = ap_getword_white(r->pool,&rest);
                permission_resource = rest;
                
                decision = ask_server_if_permitted(dconf->server, user, access_type, permission_resource, path1, path2, request_error_callback, r);
               
                switch (decision) {

                    case DECISION_FAILURE:
                    
                        ap_log_rerror(APLOG_MARK,APLOG_DEBUG,0,r,"Failed to get decision from athorization server: user=%s access_type=%s permission_resource=%s path1=%s path2=%s",user,access_type,permission_resource,path1,path2);
                        return HTTP_INTERNAL_SERVER_ERROR;
                        
                    case DECISION_DENIED:
                    
                        ap_log_rerror(APLOG_MARK,APLOG_DEBUG,0,r,"Authorization server denied access: user=%s access_type=%s permission_resource=%s path1=%s path2=%s",user,access_type,permission_resource,path1,path2);
                        ap_note_auth_failure(r);
                        return HTTP_UNAUTHORIZED;
                    
                    case DECISION_GRANTED:
                        
                        return OK;
                }
            }
        }
    }

    return DECLINED;
}

