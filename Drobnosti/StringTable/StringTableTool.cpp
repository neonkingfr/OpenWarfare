#include <stdio.h>
#include <string.h>
#include "el/qstream/qstdstream.hpp"
#include "el/qstream/qbstream.hpp"
#include "es/files/fileContainer.hpp"
#include "es/containers/array.hpp"

#pragma optimize( "2", on )
bool ReadColumn(QIStream &f, RString &column)
{
  int c = f.get();
  if (c < 0) { column=""; return false; };
  // skip leading spaces:
  while (c != 0x0D && c != 0x0A && isspace(c))
  {
    c = f.get();
    if (c < 0) { column=""; return false; };
  }
  char buffer[4096];
  int i = 0;
  if (c == '\"')
  {
    if (i < sizeof(buffer) - 1) buffer[i++] = c;
    c = f.get();
    if (c < 0) { column=""; return false; };
    while (true)
    {
      if (c == '\"')
      {
        if (i < sizeof(buffer) - 1) buffer[i++] = c;
        c = f.get();
        if (c < 0)
        {
          buffer[i] = 0;
          column=buffer; 
          return true;
        }
        if (c != '\"')
        {
          while (c != 0x0D && c != 0x0A && c != ',')		
          {
            c = f.get();
            if (c < 0)
            {
              buffer[i] = 0;
              column=buffer; 
              return true;
            }
          }
          if (c == 0x0D || c == 0x0A) 
          {
            f.unget();
          }
          if (i == 0) { column = ""; return true; }
          buffer[i] = 0;
          column=buffer;
          return true;
        }
      }
      if (i < sizeof(buffer) - 1) buffer[i++] = c;
      c = f.get();
      if (c < 0)
      {
        Assert(i > 0);
        buffer[i] = 0;
        column=buffer;
        return true;
      }
    }
    // Unaccessible
  }
  else
  {
    while (c != 0x0D && c != 0x0A && c != ',')		
    {
      if (i < sizeof(buffer) - 1) buffer[i++] = c;
      c = f.get();
      if (c < 0)
      {
        buffer[i] = 0;
        column = buffer;
        return true;
      }
    }
    if (c == 0x0D || c == 0x0A) {
      f.unget();
      if (!i) return false;
      buffer[i]=0;
      column=buffer; 
      return true;
    }
    if (i == 0) {
      column = "";
      return true;
    }
    buffer[i] = 0;
    column=buffer;
    return true;
  }
}

static RString Dequote(RString str)
{
  int siz = str.GetLength();
  if (str[0]=='\"' && str[siz-1]=='\"' && siz>2) return str.Substring(1,siz-1);
  return str;
}

static RString DeepDequote(RString str)
{
  const unsigned char *buf=(unsigned char *)str.Data();
  int lastQuote=-2;
  int i=0;
  int siz=str.GetLength();
  while(i<siz) {
    if (buf[i]=='\"') lastQuote=i;
    else if (!isspace(buf[i])) break;
    i++;
  }
  if (lastQuote==siz-1){
    return RString("\"\"");
  }
  i=siz-1;
  int firstQuote=siz;
  while(i>0) {
    if (buf[i]=='\"') firstQuote=i;
    else if (!isspace(buf[i])) break;
    i--;
  }
  if (lastQuote==-2) return str;
  if (firstQuote<=lastQuote) return RString("\"\"");
  RString retVal=str.Substring(lastQuote,firstQuote+1);
  return retVal;
}

static RString GetFileContents(const char *fileName)
{
  FILE *file;
  RString output;
  if ((file = fopen(fileName, "rb"))!=NULL)
  {
    int len = _filelength(file->_file);
    char *buffer = new char[len+1];
    int bufsize = fread(buffer, 1, len, file);
    output = RString(buffer, bufsize);
    fclose(file);
    delete buffer;
  }
  return output;
}

struct TargetFile
{
  FileItem file; //file where the change take place
  AutoArray<RString> columns; //target columns
  int lineNum; //number of line to change
  void Update();
};
TypeIsMovable(TargetFile);

void TargetFile::Update()
{
  RString filePath = file.path+file.filename;
  printf("   ... %s\n",filePath.Data());
  RString fileData = GetFileContents(filePath);
  QOFStream out(file.path+file.filename);
  //find matching strCode at the begining of line
  QIStrStream in(fileData.Data(),fileData.GetLength());
  char buffer[4048];
  buffer[0]=0;
  int lNum=0;
  while (!in.eof())
  {
    if (lNum) out << "\r\n";
    in.readLine(buffer);
    if (lNum==lineNum)
    {
      for (int i=0, siz=columns.Size(); i<siz; i++)
      {
        RString outstr = columns[i];
        out << columns[i];
        if (i!=siz-1) out << ",";
      }
    }
    else out << buffer;
    lNum++;
  }
  out.close();
}

/*
void TargetFile::UpdateWithOmitting()
{
  RString filePath = file.path+file.filename;
  RString fileData = GetFileContents(filePath);
  QIStrStream inStream(fileData.Data(),fileData.GetLength());
  QOFStream out(filePath);
  printf("   ... %s\n",filePath.Data());
  int lNum=0;
  int omitSiz=omitColumns.Size();
  while (!inStream.eof())
  {
    if (lNum==lineNum)
    {
      for (int i=0, siz=columns.Size(); i<siz; i++)
      {
        RString outstr = columns[i];
        out << columns[i];
        if (i!=siz-1) out << ",";
      }
      continue;
    }
    RString column;
    int colIx=0;
    while (ReadColumn(inStream,column)) 
    {
      bool printCol=true;
      for (int i=0; i<omitSiz; i++)
      {
        if (omitColumns[i]==colIx) {printCol=false; break;}
      }
      if (printCol) 
      {
        if (colIx) out << ",";
        out << column;
      }
      colIx++;
    }
    out << "\r\n";
    inStream.nextLine();    
    lNum++;
  }
  out.close();
}
*/

struct SourceLine
{
  RString strCode;
  RString english;
  AutoArray<RString> columns;
  AutoArray<TargetFile> targets;
  AutoArray<TargetFile> narrowTargets;
  bool matched;
    
  SourceLine() {}
  bool GetNextLine(QIStream &in);
  bool GetNextLineRaw(QIStream &in); //no dequoting
  void ChangeTargets();
};
TypeIsMovable(SourceLine);

static AutoArray<int> omitColumns;
static int ColumnIx=-1;
static RString ColumnString;
bool SourceLine::GetNextLine(QIStream &in)
{
  columns.Clear(); targets.Clear(); narrowTargets.Clear(); 
  matched=false;
  RString column;
  int omitSiz=omitColumns.Size();
  while (!in.eof())
  {
    if (ReadColumn(in, column) && column.GetLength())
    {
      strCode=Dequote(column);  //should be dequoted, as export from oupenOffice adds quotes everywhere
      int colIx=0;
      while (ReadColumn(in, column))
      {
        bool printCol=true;
        if (!colIx) {
          if (column.GetLength()) english=DeepDequote(column);
          else english=RString("");
        }
        if (!column.GetLength()) columns.Add(RString(""));
        else
        {
          for (int i=0; i<omitSiz; i++)
          {
            if (omitColumns[i]==colIx+1) { printCol=false; break; }
          }
          if (printCol) columns.Add(DeepDequote(column));
          else columns.Add(RString(""));
        }
        colIx++;
      }
      in.nextLine();
      return true;
    }
    in.nextLine();
  }
  return false;
}
bool SourceLine::GetNextLineRaw(QIStream &in)
{
  columns.Clear(); targets.Clear(); narrowTargets.Clear(); 
  RString column;
  while (!in.eof())
  {
    if (ReadColumn(in, column) && column.GetLength())
    {
      strCode=column;
      int colIx=0;
      while (ReadColumn(in, column))
      {
        if (!column.GetLength()) columns.Add(RString(""));
        else
        {
          columns.Add(column);
        }
        colIx++;
      }
      in.nextLine();
      return true;
    }
    in.nextLine();
  }
  return false;
}

void SourceLine::ChangeTargets()
{
  int narSize = narrowTargets.Size();
  int size = targets.Size();
  if ((narSize+size)==0)
  {
    //log no match
    if (!matched) {
      FILE *log;
      if ((log=fopen("strTableTool.log","at"))!=NULL)
      {
        fputs(strCode.Data(),log);
        for(int i=0, siz=columns.Size(); i<siz; i++)
        {
          fputs(",",log);
          fputs(columns[i].Data(),log);
        }
        fputs("\n",log);
        fclose(log);
      }
    }
    return;
  }
  if ((narSize+size)==1)
  {
    for (int i=0, siz=targets.Size(); i<siz; i++) targets[i].Update();
  }
  for (int i=0, siz=narrowTargets.Size(); i<siz; i++) narrowTargets[i].Update();
}
#pragma optimize( "2", off )

static bool ProcessStringTableFile(const FileItem &file, SourceLine &sLine)
{
  QIFStream inStream;
  RString filePath = file.path + file.filename;
  inStream.open(filePath);
  int lineNum=0;
  while (!inStream.eof())
  {
    AutoArray<RString> line;
    RString column;
    if (ReadColumn(inStream, column))
    {
      if (strcmp(sLine.strCode.Data(),column.Data())==0)
      {
        line.Add(column);
        int i=0;
        int sLineSiz=sLine.columns.Size();
        bool changed=false;
        RString _english;
        while (ReadColumn(inStream,column)) 
        {
          if (i<sLineSiz && strcmp(Dequote(column).Data(),Dequote(sLine.columns[i]).Data())!=0) 
            changed=true;
          if (!i) _english=column;
          if (i<sLineSiz && sLine.columns[i].GetLength()) line.Add(sLine.columns[i]);
          else line.Add(column); //empty lines intact
          i++;
        }
        if (line.Size()>1 && changed)
        {
          TargetFile target;
          target.columns = line;
          target.file=file;
          target.lineNum=lineNum;
          if (strcmp(Dequote(_english).Data(), Dequote(sLine.english).Data())==0)
          { //narrow test also successful
            sLine.narrowTargets.Add(target);
          }
          else sLine.targets.Add(target);
        }
        else if (!changed) sLine.matched=true;
        return false;
      }
    }
    inStream.nextLine();
    lineNum++;
  }
  return false;
}

void ChangeColumnInFile(SourceLine &sLine, AutoArray<SourceLine> &destination)
{
  int siz=destination.Size();
  for (int lineNum=0; lineNum<siz; lineNum++)
  {
    AutoArray<RString> &columns=destination[lineNum].columns;
    if (!columns.Size()) continue;
    RString column;
    RString strcode1=Dequote(sLine.strCode.Data());
    RString strcode2=Dequote(columns[0].Data());
    if (strcmp(strcode1,strcode2)==0)
    {
      if (columns.Size()>ColumnIx) 
        //stricmp(Dequote(DeepDequote(columns[ColumnIx])).Data(),ColumnString.Data())==0
      {
        column = Dequote(DeepDequote(columns[ColumnIx])); column.Upper();
        if (strstr(column.Data(),ColumnString.Data())!=NULL)
          columns[ColumnIx]=sLine.columns[ColumnIx-1];
      }
      break;
    }
  }
}

int main(int argc, char **argv)
{
  int paramIx=1;
  const char *sourceFileName=NULL;
  const char *destinationFolder=NULL;
  for (; paramIx<argc; paramIx++)
  {
    if (argv[paramIx][0]=='-')
    {
      if (isdigit(argv[paramIx][1]))
      {
        int colIx=atoi(argv[paramIx]+1);
        omitColumns.Add(colIx);
      }
      if (stricmp(argv[paramIx],"-col")==0)
      {
        paramIx++;
        if (paramIx>=argc || !isdigit(argv[paramIx][0])) { printf("Error: there should be column number after -col\n"); return 2;}
        ColumnIx=atoi(argv[paramIx]);
        paramIx++;
        if (paramIx>=argc) { printf("Error: there should be the string after -col num\n"); return 2;}
        ColumnString=Dequote(RString(argv[paramIx])); ColumnString.Upper();
      }
      else {
        printf("Error: unknown parameter: %s\n", argv[paramIx]);
        return 1;
      }
    }
    else if (!sourceFileName) sourceFileName = argv[paramIx];
    else if (!destinationFolder) destinationFolder = argv[paramIx];
    else
    {
      printf("Error: both <source file> and <destination folder> have been specified yet!\n");
      return 1;
    }
  }
  if (!sourceFileName)
  {
    printf(
      "Usage: stringTableTool [-1 ...] <sourceFile.csv> <destination folder>\n"
      "       stringTableTool -col n string <source file> <destination file>\n"
      "   -digit ... column, that should not be copied to the destination\n"
      "   -col ..... 'n' is number of column, which should rewrite destination\n"
      "               file only if destination column contain string specified\n"
      "   example: stringTableTool -1 -2 \"some terrific file.csv\" o:\\statusQuo\\\n"
      "            stringTableTool -col 3 ITALIAN italian.csv source.csv\n\n"
      );
    return 1;
  }
  if (!destinationFolder)
  {
    printf("Error: <destination folder> has not been specified!\n");
    return 1;
  }
  //make the logFile empty
  FILE *log;
  if ((log=fopen("strTableTool.log","wt"))!=NULL)
  {
    fclose(log);
  }

  if (ColumnIx<0)
  {
    SourceLine ctx;
    QIFStream sourceFile;
    if (QFBankQueryFunctions::FileExist(sourceFileName))
    {
      sourceFile.open(sourceFileName);
      SourceLine line;
      int lineNum=0;
      while (line.GetNextLine(sourceFile))
      {
        if (strcmp(Dequote(line.strCode).Data(),"LANGUAGE")==0) continue; //omit language line
        if (strcmp(Dequote(line.strCode).Data(),"COMMENT")==0) continue;
        //get target files to change
        printf("line number: %d   string code: %s\n", lineNum++, line.strCode.Data());
        ForMaskedFileR(destinationFolder, "*.csv", ProcessStringTableFile, line);
        line.ChangeTargets();
      }
    }
  }
  else
  {
    if (!QFBankQueryFunctions::FileExist(sourceFileName) || !QFBankQueryFunctions::FileExist(destinationFolder))
    {
      printf("Error: source file name or destination file name doesn't exist!\n");
      return 3;
    }
    AutoArray<SourceLine> destination;
    RString fileData = GetFileContents(destinationFolder);
    QIStrStream inStream(fileData, fileData.GetLength());
    while (!inStream.eof())
    {
      SourceLine line;
      RString column;
      while (ReadColumn(inStream,column)) 
      {
        line.columns.Add(column);
      }
      destination.Add(line);
      inStream.nextLine();
    }

    QIFStream sourceFile;
    sourceFile.open(sourceFileName);
    SourceLine line;
    int lineNum=0;
    while (line.GetNextLineRaw(sourceFile))
    {
      if (strcmp(Dequote(line.strCode).Data(),"LANGUAGE")==0) continue; //omit language line
      if (strcmp(Dequote(line.strCode).Data(),"COMMENT")==0) continue;
      //get target files to change
      printf("line number: %d   string code: %s\n", lineNum++, line.strCode.Data());
      ChangeColumnInFile(line, destination);
    }

    QOFStream out(destinationFolder);
    for (int i=0, siz=destination.Size(); i<siz; i++)
    {
      if (destination[i].columns.Size())
      {
        out << destination[i].columns[0];
        for (int j=1, sizlin=destination[i].columns.Size(); j<sizlin; j++)
        {
          out << ",";
          out << destination[i].columns[j];
        }
      }
      out << "\r\n";
    }
    out.close();
  }

  return 0;
}
