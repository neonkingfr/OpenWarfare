// WikiToDiagram.cpp : Defines the entry point for the console application.
//

#include <El/elementpch.hpp>

#include <Es/Common/win.h>
#include <Es/Strings/rString.hpp>

#include <El/XML/inet.hpp>
#include <El/XML/xml.hpp>
#include <El/QStream/qStream.hpp>
#include <El/ParamFile/paramFile.hpp>

//! free Win32 allocated memory when variable is destructed
class AutoFree
{
  //! handle to Win32 memory obtained by GlobalAlloc call
  void *_mem;

private:
  //! no copy constructor
  AutoFree(const AutoFree &src);

public:
  //! attach Win32 memory handle
  /*!
  \param mem handle to Win32 memory obtained by GlobalAlloc call
  */
  void operator = (void *mem)
  {
    if (mem==_mem) return;
    Free();
    _mem = mem;
  }
  //! copy - reassing Win32 handle to new object
  /*! source will be NULL after assignement */
  void operator = (AutoFree &src)
  {
    if (src._mem == _mem) return;
    Free();
    _mem = src._mem;
    src._mem = NULL;
  }
  //! empty constructor
  AutoFree(){_mem = NULL;}
  //! construct from Win32 memory handle
  AutoFree(void *mem){_mem = mem;}
  //! convert to pointer
  operator void * () {return _mem;}
  //! destruct - free pointer
  ~AutoFree(){Free();}
#ifdef _WIN32
  //! free pointer explicitelly
  void Free(){if (_mem) GlobalFree(_mem),_mem = NULL;}
#else
  //! free pointer explicitelly
  void Free(){if (_mem) free(_mem),_mem = NULL;}
#endif
};

class ExtractText : public SAXParser
{
protected:
  RString _text;
  int _textElement;

public:
  ExtractText() {_textElement = 0;}
  RString GetText() const {return _text;}

  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "text") == 0) _textElement++;
  }
  virtual void OnEndElement(RString name)
  {
    if (stricmp(name, "text") == 0) _textElement--;
  }
  virtual void OnCharacters(RString chars)
  {
    if (_textElement > 0) _text = _text + chars;
  }
};

enum Products
{
  POFP2,
  PArmA,
  PVBS2,
  NProducts
};

struct Page
{
  RString name;
  int priority[NProducts];
  AutoArray<RString> dependencies;
  int index;

  Page()
  {
    for (int i=0; i<NProducts; i++) priority[i] = 0;
    index = -1;
  }
  Page(RString n)
  {
    name = n;
    for (int i=0; i<NProducts; i++) priority[i] = 0;
    index = -1;
  }
};
TypeIsMovable(Page)

class Pages: public AutoArray<Page>
{
public:
  int FindPage(RString name);
};

int Pages::FindPage(RString name)
{
  for (int i=0; i<Size(); i++)
  {
    if (stricmp(Get(i).name, name) == 0) return Get(i).index;
  }
  return -1;
}

void ListPages(Pages &pages, RString text)
{
  const char *beg = "[[";
  const char *end = "]]";

  const char *ptr = text;
  while (ptr = strstr(ptr, beg))
  {
    ptr += strlen(beg);
    const char *p2 = strstr(ptr, end);
    if (!p2) break;
    pages.Add(Page(RString(ptr, p2 - ptr)));
    ptr = p2 + strlen(end);
  }
}

void ProcessPage(Page &page, RString text)
{
  const char *beg = "{{";
  const char *end = "}}";

  const char *ptr = text;
  while (ptr = strstr(ptr, beg))
  {
    ptr += strlen(beg);
    const char *p2 = strstr(ptr, end);
    if (!p2) break;
    {
      RString record(ptr, p2 - ptr);
      const char *p = record;
      const char *f = strchr(p, '|');
      if (f)
      {
        RString name(p, f - p);
        if (stricmp(name, "ttask-priority") == 0)
        {
          p = f + 1;
          f = strchr(p, '|');
          if (f)
          {
            RString product(p, f - p);
            RString priority(f + 1);
            int pri = 0;
            if (stricmp(priority, "NiceToHave") == 0) pri = 1;
            else if (stricmp(priority, "Desired") == 0) pri = 2;
            else if (stricmp(priority, "Required") == 0) pri = 3;
            int pro = -1;
            if (stricmp(product, "OFP2") == 0) pro = POFP2;
            else if (stricmp(product, "ArmA") == 0) pro = PArmA;
            else if (stricmp(product, "VBS2") == 0) pro = PVBS2;
            if (pro >= 0 && pri > 0) page.priority[pro] = pri;
          }
        }
        else if (stricmp(name, "ttask-dependency") == 0)
        {
          page.dependencies.Add(f + 1); 
        }
      }
    }
    ptr = p2 + strlen(end);
  }
}

RString FormatTitle(RString in)
{
  const int bufferSize = 2048;
  WCHAR buffer[bufferSize];
  MultiByteToWideChar(CP_UTF8, 0, in, -1, buffer, bufferSize);
  char out[bufferSize];
  WideCharToMultiByte(CP_ACP, 0, buffer, -1, out, bufferSize, NULL, NULL);
  const char *translation = strchr(out, '(');
  if (!translation) return out;
  const char *p = translation;
  while (p - 1 > out && isspace(*(p - 1))) p--;
  return RString(out, p - out) + RString("\\r\\n") + RString(translation);
}

static int BgColors[] = 
{
  0x000000,
  0xffffff,
  0x80ffff,
  0x8080ff
};

void ExportDiagram(Pages &pages, Products product, RString filename)
{
  ParamFile out;
  ParamClassPtr items = out.AddClass("GraphItems");
  int count = 0;
  float y = 0;
  for (int i=0; i<pages.Size(); i++)
  {
    Page &page = pages[i];
    int priority = page.priority[product];
    if (priority == 0)
    {
      page.index = -1;
      continue;
    }

    page.index = count;

    RString name = "Item";
    if (count > 0) name = Format("Item_%d", count + 1);
    ParamClassPtr item = items->AddClass(name);

    item->Add("text", FormatTitle(page.name));
    
    item->Add("left", -100);
    item->Add("top", y);
    item->Add("right", 100);
    item->Add("bottom", y + 45);

    item->Add("fontFace", "Arial");
    item->Add("fontHeight", 10);

    item->Add("bgColor", BgColors[priority]);

    y += 50;
    count++;
  }

  ParamClassPtr links = out.AddClass("GraphLinks");
  count = 0;
  for (int i=0; i<pages.Size(); i++)
  {
    Page &page = pages[i];
    if (page.index < 0) continue;
    for (int j=0; j<page.dependencies.Size(); j++)
    {
      int index = pages.FindPage(page.dependencies[j]);
      if (index < 0) index = page.index;

      RString name = "Link";
      if (count > 0) name = Format("Link_%d", count + 1);
      ParamClassPtr link = links->AddClass(name);

      link->Add("from", index);
      link->Add("to", page.index);
      link->Add("color", 0x0);

      count++;
    }
  }

  out.Save(filename);
}

int main(int argc, char* argv[])
{
  RString path = "https://Biki2005:BumBula1968@wiki.bistudio.com/index.php/Special:Export/";
  RString root = "N�vrh technologi� (Technology Design)";


  Pages pages;
  // read list of pages
  {
    size_t size = 0;
    char *data = DownloadFile(path + root, size, NULL);
    if (!data) return -1;
    AutoFree buffer(data);

    QIStrStream in(data, size);

    ExtractText parser;
    parser.Parse(in);
    RString text = parser.GetText();
    ListPages(pages, text);
    
  }

  // read content of pages
  const char *avoid = "Category:";
  for (int i=pages.Size()-1; i>=0; i--)
  {
    if (strnicmp(pages[i].name, avoid, strlen(avoid)) == 0)
    {
      pages.Delete(i);
      continue;
    }

    size_t size = 0;
    char *data = DownloadFile(path + pages[i].name, size, NULL);
    if (!data) return -1;
    AutoFree buffer(data);

    QIStrStream in(data, size);

    ExtractText parser;
    parser.Parse(in);
    RString text = parser.GetText();

    ProcessPage(pages[i], text);
  }

  ExportDiagram(pages, POFP2, "OFP2.bidgm");
  ExportDiagram(pages, PArmA, "ArmA.bidgm");
  ExportDiagram(pages, PVBS2, "VBS2.bidgm");

	return 0;
}

