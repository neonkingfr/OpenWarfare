#ifndef WIKI_FILTER_EXT_H
#define WIKI_FILTER_EXT_H

#include "Es/essencepch.hpp"
#include <El/XML/inet.hpp>
#include <El/XML/xml.hpp>
#include <El/QStream/qStream.hpp>
#include "Es/Strings/rString.hpp"

RString ConvertCharset(RString in);

class Article
{
public:
  RString text;
  RString title;
  RString author;
  RString date;
  RString url;
  RString originalUrl;
  RString path;
  RString categories;


  Article() {}
  Article(RString txt, RString tit, RString auth, RString dat) : text(txt), title(tit), author(auth), date(dat) {}
  void ConvertCharset()
  {
    title = ::ConvertCharset(title);
    author = ::ConvertCharset(author);
    date = ::ConvertCharset(date);
    text = ::ConvertCharset(text);
    //categories = ::ConvertCharset(categories);
  }
};
TypeIsMovable(Article)

class ExtractArticle : public SAXParser
{
protected:
  RString _text;
  RString _author;
  RString _title;
  RString _date;

  int _textElement;
  int _authorElement;
  int _titleElement;
  int _dateElement;

  bool _invalid;
public:
  ExtractArticle() 
  {
    _trim = false; //we need to preserver the \n characters and spaces
    _textElement = _authorElement = _titleElement = _dateElement = 0;
    _invalid = false;
  }
  Article GetArticle() { return Article(_text, _title, _author, _date); }

  bool Invalid() const { return _invalid; }

  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "text") == 0) _textElement++;
    else if (stricmp(name, "title") == 0) _titleElement++;
    else if (stricmp(name, "username") == 0) _authorElement++;
    else if (stricmp(name, "timestamp") == 0) _dateElement++;
    else if (stricmp(name, "html") == 0) _invalid=true; //it is probably "401 Authorization Required"
  }
  virtual void OnEndElement(RString name)
  {
    if (stricmp(name, "text") == 0) _textElement--;
    else if (stricmp(name, "title") == 0) _titleElement--;
    else if (stricmp(name, "username") == 0) _authorElement--;
    else if (stricmp(name, "timestamp") == 0) _dateElement--;
  }
  virtual void OnCharacters(RString chars)
  {
    if (_textElement > 0) _text = _text + chars;
    else if (_authorElement > 0) _author = _author + chars;
    else if (_titleElement > 0) _title = _title + chars;
    else if (_dateElement > 0) _date = _date + chars;
  }
};

class WikiFilesList;
struct ImageInfo
{
  static  int imgNo, imgCount; //helper variables used for progress percents show
  RString title;
  RString url;       //url of image inside wiki (with some hash prefix such as .../22/8/imageName.jpg
  RString path;      //path to save the image inside working copy
  RString fileName;  //filename (i.e. title) with spaces converted to underscores and hashing prefix (such as "c/c8/")
  ImageInfo() {}
  ImageInfo(RString tit, WikiFilesList *parent);

  const char *GetKey() const {return title;}
};
TypeIsMovableZeroed(ImageInfo)

struct TemplateInfo
{
  RString name;
  RString content; 
  TemplateInfo() {}
  TemplateInfo(RString tname, RString tcontent) : name(tname), content(tcontent) {}

  const char *GetKey() const {return name;}
};
TypeIsMovableZeroed(TemplateInfo)

typedef MapStringToClass<ImageInfo, AutoArray<ImageInfo> > ImageInfoTable;
typedef MapStringToClass<TemplateInfo, AutoArray<TemplateInfo> > TemplatesInfoTable;

class WikiFilesList
{
private:
  ImageInfoTable _imageCache;
  RString        _workingCopyPath;
  RString        _wikiUrlRoot;
  RString        _urlRoot;
  InternetConnectInfo *_inetInfo;

public:
  WikiFilesList() { _inetInfo=NULL; };
  // url and path to root space for images to be downloaded
  void SetPaths(RString wc, RString url, RString wikiUrl) { _workingCopyPath=wc; _urlRoot=url; _wikiUrlRoot=wikiUrl; };
  // inet info needed to connect
  void SetInetInfo(InternetConnectInfo &inetInfo) { _inetInfo = &inetInfo; }
  RString GetWikiURLRoot() { return _wikiUrlRoot; }
  RString GetPath() { return _workingCopyPath; }
  RString GetImagesURLRoot() { return _urlRoot+RString("images/"); }
  RString GetImagesPath() { return _workingCopyPath+RString("images\\"); }
  // Add string identifying the image into the list of files which are to be later downloaded from wiki
  const char *AddImage(char *imgTitle);
  // Download Images
  void DownloadImages();
  // Export list of images in XML format
  void Export2Xml(QOStream &out);

  //@{ Templates support
  // templates content already downloaded from wiki
  TemplatesInfoTable _templatesCache;
  // download template from wiki server
  char *DownloadTemplate(RString templateName);
  // get template content from cache or download it from wiki if the template does not exists (should return char buffer with more space available at the end)
  char *GetTemplate(char *templateName);
  //@} Templates support

  /// static methods to expose basic fileName conversion functionality
  // replace each occurrence of fromChar inside name to toChar
  static RString ReplaceChar(RString name, char fromChar, char toChar);
  // replace ' ' with '_'
  static RString ConvertToFileName(RString name);
  // compute hash subdirectories for given char* key (such as "8/8b/")
  static char *GetHashingSubdir(const char *key, bool useBackslash=true);
  //! create path to given directory / file
  static void CreatePath(const char *path); //
};
extern WikiFilesList GWikiFilesList;

class CategoryList
{
  char  catStr[8192];
  char *catTail;
  int   catCount;

public:
  CategoryList() { catTail = catStr; catCount = 0; catStr[0]=0; /*empty string initialization*/ }
  void AddCategory(char *cat);
  char *GetCategories() { return catStr; }
};

#endif
