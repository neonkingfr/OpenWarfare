#include <El/elementpch.hpp>
#include <Es/Strings/rString.hpp>
#include "wikiFilterExt.h"
#include "WikiFilter/WF_Main.h"

extern char *Parse(char *bufRaw,int *oBytes, CategoryList *categoryList);
extern BOOL OpenIniFile();

char *testFile = "{{ttask-header}}\r\n"
"'''1st Brigade Combat Team'''<br />\r\n"
"'''<nowiki>{{PAGENAME}}</nowiki>'''<br />\r\n"
"dalsi ukazka <nowiki><nejaky text><b>nebold</b></nowiki><br />\r\n"
"'''<nowiki>{{{bohemka}}}</nowiki>'''<br />\r\n";

RString ParseWiki(RString wikitext, CategoryList *categoryList)
{
  static bool initialized = false;
  if (!initialized)
  { //initialize only once
    OpenIniFile();
    initialized = true;
    static bool doIt = false;
    if (doIt)
      wikitext = RString(testFile);
  }
  char *inbuf = GmC(wikitext.GetLength()+1);
  strcpy(inbuf,wikitext);
  int oBytes;
  char *out = Parse(inbuf,&oBytes, categoryList);
  RString retVal(out);
  Fm(out);
  return retVal;
}

// Container to store information about images to download from wiki later
WikiFilesList GWikiFilesList;
// URL root to store wiki images
RString   GImageURLRoot;

//returns url style image name with hashing prefix, such as b/b0/imageName.jpg
static RString GAddImageRetVal;
const char *WikiFilesList::AddImage(char *imgTitle)
{
  RString img = imgTitle;
  //use only substring of imgTitle to the possible '|' char
  char * pipePtr = strchr(imgTitle,'|');
  if (pipePtr)
    img = img.Substring(0, pipePtr-imgTitle);
  // img now contains the image to store
  ImageInfo imgInfo(img, this);
  _imageCache.Add(imgInfo);
  // convert backslash to slash for return value
  GAddImageRetVal = ReplaceChar(imgInfo.fileName,'\\','/');
  return GAddImageRetVal;
}

RString WikiFilesList::ReplaceChar(RString name, char fromChar, char toChar)
{
  RString converted = name;
  char *text = converted.MutableData();
  for (int i=0, len=name.GetLength(); i<len; i++)
  {
    if (name[i]==fromChar) text[i]=toChar;
  }
  return converted;
}

RString WikiFilesList::ConvertToFileName(RString name)
{
  return ReplaceChar(name, ' ', '_');
}

ImageInfo::ImageInfo(RString tit, WikiFilesList *parent) : title(tit) 
{
  //set fileName to title with spaces converted to underscores
  fileName = WikiFilesList::ConvertToFileName(title);
  char *hashSubdir = WikiFilesList::GetHashingSubdir(fileName);
  fileName = RString(hashSubdir)+fileName;
  path = parent->GetImagesPath() + fileName;
}

#include <El/XML/inet.hpp>
#include <El/XML/xml.hpp>
#include <El/QStream/qStream.hpp>

class ExtractImage : public SAXParser
{
protected:
  RString _url;
public:
  ExtractImage() {}
  RString GetUrl() { return _url; }

  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (name=="Image")
    {
      XMLAttribute *urlAttr = attributes.Find("url");
      if (urlAttr)
      {
        _url = urlAttr->value;
      }
    }
  }
  virtual void OnEndElement(RString name) {}
  virtual void OnCharacters(RString chars) {}
};

int ImageInfo::imgNo = 0;
int ImageInfo::imgCount = 0;
void DownloadImageFunc(ImageInfo &info, ImageInfoTable *table, void *ctx)
{
  printf("Downloading image '%s'...\n", cc_cast(info.title));
  InternetConnectInfo *inetInfo = (InternetConnectInfo *)ctx;
  size_t size = 0;
  RString exportImageURL = RString("/index.php/Special:ImageExport/") + info.title;
  char *data = DownloadFile(exportImageURL , size, NULL, inetInfo);
  ImageInfo::imgNo++;
  printf("  image info, downloaded %d bytes\n", size);
  // Parse out the url of image
  QIStrStream in(data, size);
  ExtractImage parser;
  parser.Parse(in);
  GlobalFree(data),data = NULL;
  RString fullURL = parser.GetUrl();
  // we need to get only relative url (everything after the server url) with leading '/'
  const char *serverStart = strstr(fullURL, inetInfo->serverName);
  if (serverStart!=NULL)
  {
    const char *serverEnd = serverStart+strlen(inetInfo->serverName);
    info.url = serverEnd; //substring starting at server end
    // Download the image from url
    size = 0;
    data = DownloadFile(info.url, size, NULL, inetInfo);
    printf("  image date, downloaded %d bytes. Progress (%d/%d) %.1f%%\n", size, ImageInfo::imgNo, ImageInfo::imgCount, 100*float(ImageInfo::imgNo)/ImageInfo::imgCount);
    // save the file to disk
    RString win1250FileName = ConvertCharset(info.path);
    WikiFilesList::CreatePath(win1250FileName);
    FILE *f = fopen(win1250FileName, "wb");
    if (f)
    {
      fwrite(data, sizeof(char), size, f);
      fclose(f);
    }
    GlobalFree(data),data = NULL;
  }
  else printf("Error: Cannot download the file %s\n", cc_cast(info.title));
}

void WikiFilesList::DownloadImages()
{
  ImageInfo::imgCount = _imageCache.NItems();
  _imageCache.ForEach(DownloadImageFunc, _inetInfo);
}

void CategoryList::AddCategory(char *cat)  
{
  if (catCount) *catTail++=',';
  // Stop at the possible '|' char, example: "Technology-OFP2|RequiredPodepisovani dat (Data Signatures)"
  char *t = cat;
  To_C(t, '|');
  char buf[256];
  //copy only chars before possible '|'
  strncpy(buf, cat, t-cat);  buf[t-cat]=0;
  //change spaces to commas
  for (unsigned int i=0; i<strlen(buf); i++) if (isspace(buf[i])) buf[i]='-';
  //convert longer commas to one, such as "---" to "-"
  int wr=1; //preserve first char in any case
  for (unsigned int i=1; i<strlen(buf); i++)
  {
    if (buf[i]=='-' && buf[i-1]=='-') continue;
    buf[wr++]=buf[i];
  }
  buf[wr]=0; //terminate
  t = buf;
  S_CPY(catTail, t);
  catCount++;
}

void ExportXmlImageFunc(ImageInfo &info, ImageInfoTable *table, void *ctx)
{
  QOStream &out = *((QOFStream *)ctx);
  out << "\t<Image path=\"" << info.path << "\" />\r\n";
}

void WikiFilesList::Export2Xml(QOStream &out)
{
  out << "<WikiImages>\r\n";
  _imageCache.ForEach(ExportXmlImageFunc, &out);
  out << "</WikiImages>\r\n";
}

static char hashingSubdirBuf[16];
char *WikiFilesList::GetHashingSubdir(const char *key, bool useBackslash)
{
  unsigned int hashVal = CalculateStringHashValue(key);
  unsigned int lastByte = hashVal & 0xff;
  RString hex = Format("%02x", lastByte);
  if (useBackslash)
    sprintf(hashingSubdirBuf, "%c\\%s\\", hex[0], cc_cast(hex));
  else
    sprintf(hashingSubdirBuf, "%c/%s/", hex[0], cc_cast(hex));
  return hashingSubdirBuf;
}

//! create path to given directory / file
void WikiFilesList::CreatePath(const char *path)
{
  // string will be changed temporary
  char *end = unconst_cast(path);
  if (end[0] != 0 && end[1] == ':' && end[2] == '\\') end += 3;
  while (end = strchr(end, '\\'))
  {
    *end = 0;
#ifdef _WIN32
    ::CreateDirectoryA(path, NULL);
#else
    ::CreateDirectory(path, NULL);
#endif
    *end = '\\';
    end++;
  }
}

//Download template
static char GTemplateRetVal[16384];
char *WikiFilesList::DownloadTemplate(RString templateName)
{
  printf("Downloading template '%s'...\n", cc_cast(templateName));
  size_t size = 0;
  RString exportTemplateURL = RString("/index.php/Special:Export/Template:") + templateName;
  char *data = DownloadFile(exportTemplateURL , size, NULL, _inetInfo);
  printf("  template info, downloaded %d bytes\n", size);
  // Parse out the url of image
  QIStrStream in(data, size);
  ExtractArticle parser;
  parser.Parse(in, true); //preserveSpaces
  // add article to array at last
  if ( !parser.Invalid() )
  {
    extern void TempRidStuffBistudioWiki(char *);
    extern void TempBlock(char *);
    extern void PipeReplaceLink(char *);
    extern void PipeReplace(char *);
    Article templateArticle = parser.GetArticle();
    strcpy(GTemplateRetVal, templateArticle.text);
    TempRidStuffBistudioWiki(GTemplateRetVal);
    TempBlock(GTemplateRetVal);
    //PipeReplaceLink(GTemplateRetVal);
    //PipeReplace(GTemplateRetVal);
    TemplateInfo tpInfo(templateName, GTemplateRetVal);
    _templatesCache.Add(tpInfo);
    return GTemplateRetVal;
  }
  return NULL;
}

//returns template content or NULL
char *WikiFilesList::GetTemplate(char *templateName)
{
  extern void FormatTitle(char *s);
  FormatTitle(templateName);
  const TemplateInfo &tempInfo = _templatesCache.Get(RString(templateName));
  if (_templatesCache.IsNull(tempInfo)) 
  { // try to download the template content from wiki server
    return DownloadTemplate(templateName);
  }
  else
  {
    strcpy(GTemplateRetVal, cc_cast(tempInfo.content));
    return GTemplateRetVal;
  }
  //return NULL;
}
