#include "WF_Main.h"

inline char *scAin(char *dest,char *src) //returns NULL end of *dest
{	
  while(*src) *dest++=*src++;
  *dest=0;
  return dest; 
}

//parse one code block
char *ltstr = "&lt;";
char *gtstr = "&gt;";
char *ParseCodeBlockOne(char *s,char *sNew,char **pS)
{	
  char *preStart = "<pre class=\"wiki\">";
  char *preEnd = "</pre>\r\n";
  S_CPY(sNew, preStart); //pre prefix
  while(1)
  {	
    char *tmp;
    while(*s&&*s!=LB) 
    {
      switch(*s)
      {
      case '<': tmp=ltstr; S_CPY(sNew, tmp); break;
      case '>': tmp=gtstr; S_CPY(sNew, tmp); break;
      default: 
        *sNew++=*s;
        break;
      }
      s++; 
    }
    *sNew=0;
    *sNew++ = *s++; //next line
    if (*s!=SPACE) //end of code block
    {
      S_CPY(sNew,preEnd); //pre sufix
      break;
    }
  }
  *pS=s;  
  return sNew; 
}

int ParseCodeBlocks(char *s,char *sNew)
{	
  char *sOld=s,*sNewOrg=sNew;	
  int b=0;

  while(*s)
  {	
    if(*s!=LB) { s++; continue; }  //list char must follow LB
    s++; //skip LB (at the start of the new line)
    if (*s==SPACE)
    {
      char *s1 = s;
      while (*s1==SPACE) s1++;
      if (*s1!=LB1 && *s1!=LB2) //there is something other than spaces on this space leading line
      {
        memcpy(sNew,sOld,s-sOld); sNew+=(s-sOld);
        sNew=ParseCodeBlockOne(s,sNew,&s); *sNew++=LB;
        sOld=s;	b=1;
      }
      else continue; //no code block start
    }
  }
  if(b) 
  {	
    S_CPY(sNew,sOld); 
    Z8(sNew); 
    return 1; 
  }
  return 0; 
}

#include <El/elementpch.hpp>
#include <Es/Strings/rString.hpp>

void EscapeChars(char *out, char *s)
{
  char *tmp;
  while(*s) 
  {
    switch(*s)
    {
    case '<': tmp=ltstr; S_CPY(out, tmp); break;
    case '>': tmp=gtstr; S_CPY(out, tmp); break;
    default: 
      *out++=*s;
      break;
    }
    s++; 
  }
  *out = 0; //zero terminate
}

AutoArray<RString> segments;
int ParseNoWiki(char *s,char *sNew, bool parseOut)
{	
  int offS[512],offE[512];
  if (parseOut)
  { // change all blocks inside <nowiki>...<nowiki/> into <NoWikiSeg ix=\"%d\"/> and remember its content
    int k;
    segments.Resize(0);

    int from = 0;
    if((k=WMarkDeepStr(s,offS,offE,"<nowiki>","</nowiki>",100)))
    {	
      for (int i=0; i<k; i++)
      {	
        char *nowikiSeg = s+offS[i]+8;
        int bytes=offE[i]-offS[i]-8; 
        if ( bytes<0 ) bytes=0;
        RString nowikiStr(nowikiSeg, bytes);
        int ix = segments.Add(nowikiStr);
        memmove(sNew, s+from, offS[i]-from); 
        sNew += offS[i]-from; 
        from = offE[i]+9;
        sprintf(sNew, "<NoWikiSeg ix=\"%d\"/>", ix);
        To_0(sNew);
      }
      strcpy(sNew, s+from); 
      To_0(sNew);
    }
    Z4(sNew);

    return (k ? 1 : 0); 
  }
  else
  {
    char buf[32768];
    int count = 0;
    const char *nowikistr = "<NoWikiSeg ix=\"";
    static int nowikistrlen = strlen(nowikistr);
    for ( char *next=strstr(s, nowikistr); next; next=strstr(next+1, nowikistr) )
    {
      offS[count++] = next-s;
    }
    char *next = s;
    for (int i=0; i<count; i++)
    {
      // get the index of the segment
      int ix = atoi(s+offS[i]+nowikistrlen);
      // copy chunk of text before the NoWikiSeg mark
      S_CPYN(sNew, next, (s+offS[i])-next);
      // find the continuation after the NoWikiSeg mark
      next = s+offS[i]+nowikistrlen;
      while (*next && *next!='>') next++;
      if (*next) next++;
      // write the stored nowiki segment content to output
      char *seg = segments[ix].MutableData();
      EscapeChars(buf, seg);
      seg = buf; S_CPY(sNew, seg);
    }
    S_CPY(sNew, next);
    return 1;
  }
}
