#include "WF_Main.h"

#define IS_PIPE(c) (((c)=='|'||(c)=='!')? 1:0)

extern BOOL bTest;

static STR_T strTable; //for checking prop

char szCellOpen[2][12]={"\r\n<td ","\r\n<th "};
char szCellEnd[2][12]={"</td>\r\n","</th>\r\n"};
char szCell[2][12]={"\r\n<td>","\r\n<th>"};

char szAttr[]=	//conveniently add more table props anywhere
	"align\0bgcolor\0border\0cellspacing\0class\0"
	"col\0colgroup\0color\0colspan\0dir\0"
	"height\0id\0lang\0name\0rowspan\0"		
	"size\0style\0title\0valign\0width\0"
	"background\0cellpadding\0halign\0"
	
	"\0\0";	
/*
{	"align",		"bgcolor",		"border",		"cellspacing",	"class",
	"col",			"colgroup",		"color",		"colspan",		"dir",
	"height",		"id",			"lang",			"name",			"rowspan",		
	"size",			"style",		"title",		"valign",		"width",
	"background",	"cellpadding",	"halign"
};*/

void TableExit()  // a dozen ints
{	Fm(strTable.off);Fm(strTable.index); strTable.cnt=0; }

//returns ending | or !, or 0 if no more text
char *TableToCellEnd(char *s,char **cpyTo)
{	char c;

	while((c=*s))			  
	{	if(c==LB) 		 // "\n|" or "\n!", spaces between allowed
		{	*cpyTo=s; P_SPACE(s);
			if((c=*s)&&IS_PIPE(c)) //"\n\n|||||" allowed
			{	P_C(s,c); return s-1; }
			
			if(*s=='<')
			{ 	if(s[1]=='t'&&(s[3]==SPACE||s[3]=='>')) //<tr or <td or <th 
				return s;
			}
			continue;
		}
		if(IS_PIPE(c)&&c==s[1])	// || or !!, more repeats allowed
		{	*cpyTo=s; P_C(s,c); 		 
			return (s-1);	  // returns last | or !
			continue;
		}
		s++;
	}
	*cpyTo=s; //something wrong
	return s;
}
extern int StrSearchN(STR_T *str,char *key);	

//|-classBatonRougeLA.gifBatonRougeLAseal.gif="hiddenStructure"
//so cannot simply check '=' for prop
int IsProp(char *s)	
{	char low[20],*t;	

	P_SPACE(s);
	t=low; S_LOWN(t,s,18); 
	return (StrSearchN(&strTable,low)+1);
}

char *TableCpyProp(char *s,char *sNew,char **pS,char *label)
{	char *start,*org=sNew,*cpyTo;

	if(label) sNew=scA(sNew,label);
	P_SPACE(s); 
				 // mixed wiki table and html table
	S_CPYTO3(sNew,s,'!','|','<');  // " |- class="a" <td>bb</td> "
	W_RE(org,sNew);	*sNew++='>';
	
	if(*s!='|'&&*s!='!')
	{	start=s; 
		s=TableToCellEnd(s,&cpyTo);
		S_CPYPOS(sNew,start,cpyTo);
	}
	*pS=s;
	return sNew;
}

char *TableCpyData(char *s,char *sNew,char **pS,char *label)
{	char *start=s,*cpyTo;

	if(label) sNew=scA(sNew,label);
	//P_SPACE(s); start=s; //do NOT pass spaces, in case it's an empty cell
	s=TableToCellEnd(s,&cpyTo);
	S_CPYPOS(sNew,start,cpyTo);
	*pS=s; return sNew;
}

char *TableTable(char *s,char *sNew,char **pS)
{	
	if(*s=='|'||*s=='!') sNew=scA(sNew,"\r\n<table>");
	else sNew=TableCpyProp(s,sNew,&s,"\r\n<table ");
	*pS=s; 
	return sNew;
}
char *TableCaption(char *s,char *sNew,char **pS)
{	char *sNewOrg=sNew;

	P_SPACE1(s,'+'); // allow "|+  ++++" 
	if(*s=='|'||*s=='!')  //empty
	{	*pS=s; return sNew;

	}
	if(!IsProp(s)) sNew=scA(sNew,"\r\n<caption>");
	else {  sNew=TableCpyProp(s,sNew,&s,"\r\n<caption "); s++; }
	
	sNew=TableCpyData(s,sNew,&s,0);	
	
	if((sNewOrg=StrStrI(sNewOrg,"<big>"))) //some tables miss ending tag
	{	sNew=scA(sNew,"</big>"); sNewOrg+=5; 

		if((sNewOrg=StrStrI(sNewOrg,"<big>")))
			sNew=scA(sNew,"</big>");  
	}
	sNew=scA(sNew,"</caption>\r\n");
	*pS=s; return sNew;
}


char *TableRow(char *s,char *sNew,char **pS)
{	int bTh; char c; int bTr=0;
	
	if(s[1]=='-') //row start
	{	s++; P_C(s,'-');  //allow "|-------"
		
		if(!IsProp(s)) 
		{	sNew=scA(sNew,"\r\n<tr>"); 
			P_SPACE(s);
			c=*s;
			if(IS_PIPE(c)) { P_C(s,c); s--; } // "|-\n||||" allow repeats
		}
		else sNew=TableCpyProp(s,sNew,&s,"\r\n<tr ");
	}

	while(*s)
	{		//if cell data is '!' or '|', has to separate with space
		if(*s=='!')	{ bTh=1; ; P_C(s,'!'); } 
		else if(*s=='|') { bTh=0; P_C(s,'|');  }
		else
		{	bTh=0;
			if(*s=='<') //html
			{	if(!strnicmp(s,"<tr",3)) 
				{	sNew=scA(sNew,"</tr>\r\n"); bTr=1;	}
				sNew=TableCpyData(s,sNew,&s,0);
				if(bTr) break;
				continue;
			}
		}
		
		if(*s=='-'&&*(s-1)=='|')	//empty row
		{	s--; break; }
		if(*s=='+'&&*(s-1)=='|')	 //caption
		{	sNew=TableCaption(s,sNew,&s); continue; }

		if(!IsProp(s)) sNew=scA(sNew,szCell[bTh]);
		else 		
		{	sNew=TableCpyProp(s,sNew,&s,szCellOpen[bTh]);
			s++;
		}
		sNew=TableCpyData(s,sNew,&s,0);
		sNew=scA(sNew,szCellEnd[bTh]);

		if(!*s||s[1]=='-') break; //it's the next row
	}
	if(!bTr)
		sNew=scA(sNew,"</tr>\r\n");	//close row tag
	*pS=s; 
	return sNew;
}

char *TableDoOne(char *s,char *sNew)
{	char *t;
	
	P_SPACE(s); if(!*s) return sNew;

	sNew=TableTable(s,sNew,&s);
	
	if(!*s) {  return scA(sNew,"</table>\r\n");	}  //no content
	
	t=s; P_SPACE2(t,'|','-'); //pass empty
	
	if(*t=='+')	 //caption
	{	s=t; sNew=TableCaption(s,sNew,&s); }

	if(s[1]!='-') 	sNew=scA(sNew,"\r\n<tr>"); //missing first row tag
	
	while(*s) sNew=TableRow(s,sNew,&s);	 //parse a row

	return scA(sNew,"</table>\r\n");
}

extern int WMarkDeepStr2(char *buf,int *offS,int *offE,char *cS,char *cE,int max,int bLB);
extern int *offSPre,*offEPre;
extern int iNS;

void TableRidBrace(char *s)
{	while(*s)
	{	if(*s=='{'&&s[1]=='{') { while(*s=='{') *s++=SPACE; continue; }
		if(*s=='}'&&s[1]=='}') { while(*s=='}') *s++=SPACE; continue; }
		s++;
	}
}

int ParseTable(char *bufOld,char *bufNew)
{	char *s,*sNew=0,*tt;
	int k,i,bytes,cLoop=6; //up tp 6 nested tables: {| {| {| ... |}  |} |}
	char *b1,*b2,*t;

	if(!(s=strstr(bufOld,"{|"))) return 0; //no temp
	
	b1=bufOld; b2=bufNew; 
	
	if(!strTable.cnt) StrInit(&strTable,szAttr,1); 
	while(cLoop--) //avoid infinite recursion 
	{	
		if(!(k=WMarkDeepStr2(b1,offSPre,offEPre,"{|","|}",200,1))) break;
		
		s=b1; sNew=b2; 
		for(i=0;i<k;i++)
		{	
			bytes=offSPre[i]-(s-b1);
			memcpy(sNew,s,bytes); 
			sNew+=bytes; s+=bytes;
			
			t=b1+offEPre[i]; //terminate this temp
			*t=0;  W_RE(b1,t);

			tt=sNew;
			sNew=TableDoOne(s+2,sNew); TableRidBrace(tt);
			
			s=b1+offEPre[i]+2; 
		}
		S_CPY(sNew,s);
		BufShift(&b1,&b2);
							
	}
	if(sNew) Z4(sNew);
	if(bTest)
		WWriteTest("ParseTable",b1);

	if(b1!=bufNew) return 0; //so Parse() wont shift buffer
	return 1; 	
}

void TableRidHidden(char *buf) //soooooo much nuisance stuff in wikitext
{	int k,i; char *s;

	if(!(k=WMarkDeepStr(buf,offSPre,offEPre,"class","hiddenStructure",MAX_OFF)))
		return;
	for(i=0;i<k;i++)	
	{	s=buf+offSPre[i]+5;	P_SPACE(s);
		if(*s==C_EQ)
		{	s=buf+offEPre[i]+15;
			if(*s==C_QQ) continue;
			J_C(s,C_QQ); 
		}
		else 
		{	s=buf+offEPre[i]+15;
			J_C(s,C_QQ);
		}			   //for this funny stuff, we use a funny method!
					   //simply set all hidden stuff as SPACE!
					   //then when parsing table, these spaces are all ignored
		memset(buf+offSPre[i],SPACE,s-(buf+offSPre[i]));	
	}
}




			  