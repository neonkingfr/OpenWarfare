#ifndef MYMAC_H
#define MYMAC_H


#define C_0	   48  //'0'
#define C_9	   57  //'9'
#define C_A		65 //'A'
#define C_Z		90
#define C_a		97
#define C_z		122
#define C_L		60 //'<'
#define C_R		62 //'>'
#define DF	92 // Dash file	'\'
#define DW	47 //dash web '/'
#define LB1	13 //'\r'
#define LB2	10 //'\n'
#define DOT	46 //'.'
#define SPACE	32
#define TAB		9
#define SEMI	59 //semi-column ';'
#define C_QQ	34 //double quote '"'
#define C_Q		39 //single quote'''
#define C_STAR 42  //'*'
#define C_EQ	61 //'='
#define C_DOT2	58 //':'

#define LB	LB2

#define MB 1048576
#define KB 1024
#define MP MAX_PATH

#define ISFILE 1 //the name is a file name
#define ISDIR 2  //the name is a dir name


#define C_F	 70
#define C_f	102
#define IS_HEX(c) ((((c)>=C_0&&(c)<=C_9)||((c)>=C_A&&(c)<=C_F)||((c)>=C_a&&(c)<=C_f))? 1:0)

///////// all dlg macros assume hparent as dlg handle

//Write LineBreak
#define WLB(fp) fwriteWin("\r\n",2,fp)

#define SetDir(dir) SetCurrentDirectory(dir)

#define Z4(s) do{ *s++=0; *s++=0;*s++=0;*s=0;}while(0)
#define Z8(s) do{ Z4(s); Z4(s); }while(0)

//copy upto pos	(not including pos)
#define S_CPYPOS(dest,src,pos) \
    do{ while(src<pos&&*src) *dest++=*src++; *dest=0; }while(0)

// strcpy,convenient for copying many consecutive strs
#define S_CPY(dest,src)  do{ while(*src) *dest++=*src++; *dest=0; }while(0) 
#define S_CPYN(dest,src,cnt) \
  do{ int yYy=cnt; while(*src&&yYy--) *dest++=*src++; *dest=0; }while(0)
  
#define S_CPYTO2(dest,src,c1,c2) \
    do{ while(*src&&*src!=c1&&*src!=c2) *dest++=*src++; *dest=0; }while(0)

#define S_CPYTO3(dest,src,c1,c2,c3) \
    do{ while(*src&&*src!=c1&&*src!=c2&&*src!=c3) *dest++=*src++; *dest=0; }while(0)

#define S_CPYTO4(dest,src,c1,c2,c3,c4) \
    do{ while(*src&&*src!=c1&&*src!=c2&&*src!=c3&&*src!=c4) *dest++=*src++; *dest=0; }while(0)
 
#define S_CPYTO(dest,src,c) \
    do{ while(*src&&*src!=c) *dest++=*src++; *dest=0; }while(0)

#define S_CPYTON(dest,src,c,cnt) \
    do{ int zZz=cnt; while(*src&&*src!=c&&zZz--) *dest++=*src++; *dest=0; }while(0)

#define S_CPYTO2N(dest,src,c1,c2,cnt) \
    do{ int zZz=cnt; while(*src&&*src!=c1&&*src!=c2&&zZz--) *dest++=*src++; *dest=0; }while(0)

//is bad char for file names
#define IS_BAD(c) \
(((c)==DW||(c)==DF||(c)==C_QQ||(c)=='?'||(c)=='*'||(c)=='|'||(c)=='<'||(c)=='<')? 1:0)


//is c a digit,only for positive number
#define IS_DIG(c) (((c)>=C_0&&(c)<=C_9)? 1:0)
#define IS_UP(c)  (((c)>=C_A&&(c)<=C_Z)? 1:0)
#define IS_LOW(c)  (((c)>=C_a&&(c)<=C_z)? 1:0)
//is space or linebreak
#define IS_SPACE(c) (((c)==SPACE||(c)==LB)? 1:0) 

//is letter
#define IS_A(c) ((((c)>=C_A&&(c)<=C_Z)||((c)>=C_a&&(c)<=C_z))? 1:0)

///////////////////////////////////////////// allocate memory
#define GmC(x) (char *)HeapAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,x)
#define GmCx(x) (char *)HeapAlloc(GetProcessHeap(),0,x)

#define GmIx(cnt) \
   (int *)HeapAlloc(GetProcessHeap(),0,(cnt)*sizeof(int))
#define GmI(cnt) \
   (int *)HeapAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,(cnt)*sizeof(int))

#define GmZ(cnt,sTruct) \
  (sTruct *)HeapAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,(cnt)*sizeof(sTruct))
#define GmZx(cnt,sTruct) \
  (sTruct *)HeapAlloc(GetProcessHeap(),0,(cnt)*sizeof(sTruct))

//reget mem is normally shrink, to enlarge, better use a new buf
#define ReGmB(cnt,buf) \
   (BYTE *)HeapReAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,buf,cnt)

#define ReGmI(cnt,buf) \
   (int *)HeapReAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,buf,(cnt)*sizeof(int))

#define ReGmC(cnt,buf) \
   (char *)HeapReAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,buf,(cnt))

#define ReGmZ(cnt,sTruct,buf) \
  (sTruct *)HeapReAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,buf,(cnt)*sizeof(sTruct))

#define Fm(x) do{ if(x) { HeapFree(GetProcessHeap(),0,x); x=0; } }while(0)

#define SizeMem(x) HeapSize(GetProcessHeap(),0,x)
///////////////////////////////////////////// allocate memory


//goto next str in many strs  if(*s)  ListView_GetItemCount
#define N_S(s)  while(*s++)

//goto ending-NULL
#define To_0(s) do{ if(*s) { while(*s) s++; } }while(0)

//to_last char: if s[0]==0, then dothing
#define To_L(s) do{ if(*s) { while(*s) s++; s--; } }while(0)  


#define To_NON_DIG(s) while(*s&&(*s>=C_0)&&(*s<=C_9)) s++
#define To_DIG(s) while(*s&&(*s<C_0)||(*s>C_9)) s++


#define To_C(s,c)  while(*s&&*s!=c)	s++
#define To_C2(s,c1,c2)  while(*s&&*s!=c1&&*s!=c2)	s++
#define To_C3(s,c1,c2,c3)  while(*s&&*s!=c1&&*s!=c2&&*s!=c3)	s++
#define To_C4(s,c1,c2,c3,c4)  while(*s&&*s!=c1&&*s!=c2&&*s!=c3&&*s!=c4)	s++

#define To_CN3(s,c1,c2,c3,cnt) \
  do{ int nNn=cnt; while(nNn--&&*s&&*s!=c1&&*s!=c2&&*s!=c3)	s++;}while(0)

#define To_CN4(s,c1,c2,c3,c4,cnt) \
  do{ int nNn=cnt; while(nNn--&&*s&&*s!=c1&&*s!=c2&&*s!=c3&&*s!=c4)	s++;}while(0)
#define To_CN2(s,c1,c2,cnt) do{ int mMm=cnt; while(*s&&*s!=c1&&*s!=c2&&mMm--) s++;}while(0) 
#define To_CN(s,c,cnt) do{ int nNn=cnt; while(*s&&*s!=c&&nNn--)	s++; }while(0)

#define BACK_CN(start,s,c,cnt) do{ int oOo=cnt; while(s>start&&*s!=c&&oOo--) s--;}while(0) 
#define BACK_CN2(start,s,c1,c2,cnt) \
	do{ int bBb=cnt; while(s>start&&*s!=c1&&*s!=c2&&bBb--) s--;}while(0) 
#define BACK_CN3(start,s,c1,c2,c3,cnt) \
	do{ int cCc=cnt; while(s>start&&*s!=c1&&*s!=c2&&*s!=c3&&cCc--) s--;}while(0) 




#define To_C5(s,c1,c2,c3,c4,c5)  while(*s&&*s!=c1&&*s!=c2&&*s!=c3&&*s!=c4&&*s!=c5)	s++
#define BACK_C(start,s,c) while(s>start&&*s!=c) s-- 
#define BACK_C2(start,s,c1,c2) while(s>start&&*s!=c1&&*s!=c2) s-- 
#define BACK_C3(start,s,c1,c2,c3) while(s>start&&*s!=c1&&*s!=c2&&*s!=c3) s-- 
#define To_CB_(start,s,c) { s=start; To_0(s); BACK_C(start,s,c); }
#define To_CB(start,s,c) do{ s=start; To_0(s); BACK_C(start,s,c); }while(0)

#define To_FN(start,s) do{s=start;To_0(s);BACK_C(start,s,DF);if(*s==DF) s++; }while(0)
#define To_WN(start,s) do{s=start;To_0(s);BACK_C(start,s,DW);if(*s==DW) s++; }while(0)
//ex: if ending with /, further backward
#define To_WN_EX(start,s) do{s=start;To_L(s);if(*s==DW) s--; BACK_C(start,s,DW);if(*s==DW) s++; }while(0)

//max: max chars to backup, usually 8
//ensure do not back pass path
//after calling, first check if max not changed (last char is DOT)
//or check if s[1]==NULL 
#define To_EXT(start,s,max) \
	do{ int aAa=max; s=start; To_L(s); if(*s!=DOT) BACK_CN3(start,s,DOT,DF,DW,aAa);}while(0)

#define P_C(s,c) while(*s==c)	s++
#define P_C2(s,c1,c2) while((*s==c1||*s==c2))	s++
#define P_C3(s,c1,c2,c3) while((*s==c1||*s==c2||*s==c3))	s++
#define P_C4(s,c1,c2,c3,c4) while((*s==c1||*s==c2||*s==c3||*s==c4))	s++

#define P_CB(start,s,c) do{ while(s>start&&*s==c) s--; s++; }while(0)
#define P_CB2(start,s,c1,c2) do{ while(s>start&&(*s==c1||*s==c2)) s--; s++; }while(0)

#define J_C(s,c) do{To_C(s,c); P_C(s,c);  }while(0)


//#define BACK_UPTO(start,s,c,uptoC) while(s>start&&*s!=c) s-- 
//#define P_SPACE(s)  while(*s&&(*s==SPACE||*s==TAB||*s==LB1||*s==LB2)) s++
#define P_SPACE(s)  while(*s==SPACE||*s==LB) s++

//pass_space_backward, stops at first non-space, non-linkbreak char, or at NULL 
#define P_SPACEB(start,s)  while(s>start&&(*s==SPACE||*s==LB)) s--
#define P_SPACE_BACK(s)  while((*s==SPACE||*s==LB)) s--

#define C_UP(c)	if(c>=C_a&&c<=C_z) c-=32
#define C_LOW(c) if(c>=C_A&&c<=C_Z) c+=32 

#define S_CMP(s1,s2) do{ while(*s1&&*s2&&*s1==*s2) { s1++; s2++; } }while(0)

//turn all strings in s2 to lower case
#define S_LOW2(s2) do{\
	while(1)\
	{	if(*s2)\
		{	if(*s2>=C_A && *s2<=C_Z)  *s2=*s2+32; \
			s2++; \
		} \
		else { s2++; if(!*s2) break; }\
	} 				}while(0)

//string_lower: change src directly 
#define S_LOW1(src) \
  do{ while(*src) { if(*src>=C_A&&*src<=C_Z) *src=*src+32; src++; }  }while(0)

#define S_LOWN1(src,cnt) \
  do{ int cCc=cnt; while(*src&&cCc--) { if(*src>=C_A&&*src<=C_Z) *src=*src+32; src++; }  }while(0)

#define S_LOWTO1(src,c) \
  do{ while(*src&&*src!=c) { if(*src>=C_A&&*src<=C_Z) *src=*src+32; src++; }  }while(0)

//turn to lower case upto 'c' (NOT including 'c')
#define S_LOWTO(dest,src,c) \
  do{ while(*src&&*src!=c) \
      { if(*src>=C_A&&*src<=C_Z) *dest=*src+32;else *dest=*src; src++; dest++; } *dest=0; \
  }while(0)

#define S_LOWTO2(dest,src,c,c2) \
  do{ while(*src&&*src!=c&&*src!=c2) \
      { if(*src>=C_A&&*src<=C_Z) *dest=*src+32;else *dest=*src; src++; dest++; } *dest=0; \
  }while(0)

#define S_LOWTO2N(dest,src,c,c2,cnt) \
  do{ int zzslzz=cnt; while(*src&&*src!=c&&*src!=c2&&zzslzz--) \
      { if(*src>=C_A&&*src<=C_Z) *dest=*src+32;else *dest=*src; src++; dest++; } *dest=0; \
  }while(0)


#define S_LOWTON(dest,src,c,cnt) \
  do{ int zCntz=cnt; while(*src&&*src!=c&&zCntz--) \
      { if(*src>=C_A&&*src<=C_Z) *dest=*src+32;else *dest=*src; src++; dest++; } *dest=0; \
  }while(0)

#define S_LOWN(dest,src,cnt) \
  do{ int lLl=cnt; *dest=0; \
      while(*src&&lLl--) \
      { if(*src>=C_A&&*src<=C_Z) *dest=*src+32; else *dest=*src; dest++; src++; }\
	  *dest=0; \
	}while(0)

#define S_LOW(dest,src) \
  do{ *dest=0; \
      while(*src) \
      { if(*src>=C_A&&*src<=C_Z) *dest=*src+32; else *dest=*src; dest++; src++; }\
	  *dest=0; \
	}while(0)
// string_compare

#define S_UP(dest,src) \
  do{ *dest=0; \
      while(*src) \
      { if(*src>=C_a&&*src<=C_z) *dest=*src-32; else *dest=*src; dest++; src++; }\
	  *dest=0; \
	}while(0)


#define zm(pos,cnt) memset(pos,0,cnt)

#endif