#include "WF_Main.h"

//find text by title
//data-base functions
//because we are multi-language and multi-project
//have to use multi-dimensional arrays

char *szBufStrWiki;	//used for qsort
//extern char szDomainLong[MAX_DOMAIN][20];

extern BOOL bWriteLog;
extern BOOL bTest;
extern int iNS,iDomain,iLang; 
extern WL_T *glt;
extern BOOL bShowImg;
extern char *ExtReplace(char *fname,char *extNew);
extern HANDLE WOpenDumpFile(TCHAR *fname);
extern char *UtfUp1(char *inOut);
int iVersion=23;

typedef struct 
{int offTitle,midPoscnt; }	    
W_TEMP_LOAD_T;

static int catBytePos[2]; //category bytes and pos, use gloabal so no need to pass around
						  //this not often used info

			 //dst is lower case already
inline int InstrcmpBothCase1(unsigned char * dst,unsigned char * src)
{   int l,f;
	do 
    {   //if ( ((f = (*(dst++))) >= 'A') && (f <= 'Z') ) f -= 'A' - 'a';
        f=(*(dst++));
		if ( ((l = (*(src++))) >= 'A') &&  (l <= 'Z') )	l -= 'A' - 'a';
    } while ( f && (f== l) );
    return (f - l);
}

int WFindCompWord1(const void *key,const void *v)
{	return strcmp((char *)key,szBufStrWiki+((W_POS_T *)v)->posWord1); }

W_POS_T *WikiFindWord1(char *key,char *buf,W_POS_T *pt,int max)
{	szBufStrWiki=buf; 
	return (W_POS_T *)bsearch(key,pt,max,sizeof(W_POS_T),WFindCompWord1);
}

W_POS_T *WikiSearchWord1(char *key,char *buf,W_POS_T *pt,int max)
{	int i,k=0; char c=*key,*key1;

	C_UP(c);
	for(i=0;i<max;i++)	//try to match first letter
	{	if(c==buf[pt[i].posWord1]) { k=i; break; }  }
	
	for(i=k;i<max;i++)	//search from here
	{	if(c!=buf[pt[i].posWord1]) break; //no more first letter match
		if(StrStrIA(buf+pt[i].posWord1,key)) return pt+i; 
	}

	key1=key+1;
	if(!*key1) return 0;
	 
	c=*key1;	//try second letter
	C_UP(c);
	for(i=0;i<max;i++)	//try to match first letter
	{	if(c==buf[pt[i].posWord1]) { k=i; break; }  }
	
	for(i=k;i<max;i++)	//search from here
	{	if(c!=buf[pt[i].posWord1]) break; //no more first letter match
		if(StrStrIA(buf+pt[i].posWord1,key1)) return pt+i; 
	}
	return 0; 
}

int WFindCompIt(const void *key,const void *v)
{	return strcmp((char *)key,(szBufStrWiki+(*(int *)v))); }
				
int WFindCompItBothCase(const void *key,const void *v)
{	return stricmp((char *)key,(szBufStrWiki+(*(int *)v)));	}
				
int *WikiFindIt(char *key,char *buf,int *off,int max,BOOL bBothCase)
{	szBufStrWiki=buf; 
	if(!bBothCase)
		return (int *)bsearch(key,off,max,sizeof(int),WFindCompIt);	
	else
		return (int *)bsearch(key,off,max,sizeof(int),WFindCompItBothCase);	
}

#define F_CAT	3
#define F_STR	4

//use strict error checking
char *WReadIndexFile(int ns,int which,int start,int byte)
{	HANDLE fp;
	char *buf=0;
	int pos=0;
	
	if(!glt||!(fp=glt[iLang].fpIndex[iDomain])) return 0; 

	if(which==F_STR) pos=glt[iLang].fPos[iDomain].ns[ns];  //title strs
	else if(which==F_CAT) pos=glt[iLang].fPos[iDomain].cat;	//category list
	
	if(!LibSeek(fp,pos+start)) return 0; 
	buf=GmCx(byte+20);
	if(!freadWin(buf,byte,fp)) 
	{	 Fm(buf);  
		return 0; 
	}
	zm(buf+byte,16);
	return buf; 
}

//discard ns str and returns ns index
extern char *GetToText(char *s);



BYTE *WMarkWordEnd(BYTE *str,BYTE *orgC)	
{	BYTE c;	 int i=1,len;
	
	if(glt[iLang].c1stWord[iDomain]) len=glt[iLang].c1stWord[iDomain];
	else len=254;	
	
	for(i=1;i<len;i++)
	{	
		c=str[i];
		if(!c) break;	
		
		if(c<SPACE||c>127||IS_A(c)||IS_DIG(c)) continue;
		break;
	} 	 	//for single letter word, *orgC=0, so later restoring, 0 will be appended
	if(i==len) i--;	 
	str+=i;
	*orgC=*str; *str=0;
	return str;
}
typedef struct	//older index file, (version 2.2)
{	int art;     //article info block
	int cat;	 //category list block
	int ns[16];	 //title info for each nameSpace
	int end;   //ending tag == 0x12345678, a genuine index file!!
}FPOSOLD_T; // start position of blocks in index file

HANDLE OpenIndexFile(FPOS_T *pos)
{	HANDLE fp; int i=0;  
	char *t; 
	//FPOSOLD_T posOld;

	if(!glt||!glt[iLang].dumpName[iDomain]) return 0; //no dump file
	
	//strcpy(fn,glt[iLang].dumpName[iDomain]); //index is dumpName + "_index.art"
	t=glt[iLang].dumpName[iDomain];
	ExtReplace(t,"_index.art");
	if(!(fp=fopenWin(t,"r")))
	{	strcpy(strstr(t,"_index.art"),".xml");
		return 0; 
	}
	
	strcpy(strstr(t,"_index.art"),".xml");

	SetFilePointer(fp,-4,0,FILE_END);
	freadWin(&i,4,fp); 
	if(i==iVersion) glt[iLang].indexNew[iDomain]=1;
	else if(i==0x12345678) { glt[iLang].indexNew[iDomain]=0;}
	else  //wrong index file
	{	CloseHandle(fp);
		return 0;
	}
	LogInt("Version",i);
	zm(pos,sizeof(FPOS_T));

	if(i==iVersion) i=sizeof(FPOS_T);
	else i=sizeof(FPOSOLD_T);
	 
	SetFilePointer(fp,-i,0,FILE_END);
	freadWin(pos,i,fp);
	LibSeek(fp,0); 
	return fp;
}

int FindGetVersion(int lang,int site)
{	int i; char temp[MP]; 
	HANDLE fp;
	
	if(!glt||!glt[lang].dumpName[site]) return 0; 
	
	if(glt[lang].fpIndex[site]) 
	{	if(glt[lang].indexNew[site]) return iVersion;
		else return 22;
	}
	strcpy(temp,glt[lang].dumpName[site]);
	ExtReplace(temp,"_index.art");
	if(!(fp=fopenWin(temp,"r"))) return 0; 
	SetFilePointer(fp,-4,0,FILE_END);
	freadWin(&i,4,fp); 	
	CloseHandle(fp);
	return i;	
}

//index file: bytes of 1stWords + 1stWord strs + bytes of 1stWord offsets + offsets +
// bytes of full titles + cnt of full titles + full titles
// all strings NULL-ending
BOOL HttpLoadIndexOne(int ns)
{	int i,max;  
	HANDLE  fp;
	W_TEMP_LOAD_T *off; 
	W_POS_T *pt;
	char *s,*buf;

	if(ns<0||ns>=glt[iLang].nsStr[iDomain].cnt) ns=0;

	if(!glt[iLang].fpIndex[iDomain])
	{	glt[iLang].fpIndex[iDomain]=OpenIndexFile(glt[iLang].fPos+iDomain);
		if(!glt[iLang].fpIndex[iDomain]) return 0; 
	}
	if(!ns)
		Log2Eq("Loading index for","Article");
	else
		Log2Eq("Loading index for",GetNSStr(ns));

	if(glt[iLang].fPos[iDomain].ns[ns]==0)
	{	Log("This nameSpace has no title");
		return 0; 
	}

	fp=glt[iLang].fpIndex[iDomain];	
	LibSeek(fp,glt[iLang].fPos[iDomain].ns[ns]);

	freadWin(&i,sizeof(int),fp); //1st int is bytes of 1stWords	
	LogInt("index-word bytes",i);

	if(i<1) return 0; 

	buf=GmCx(i+10); zm(buf+freadWin(buf,i,fp),8); //read 1stWords

	freadWin(&i,sizeof(int),fp);  //bytes of offsets, each offset entry is 2-int-long
	max=i/sizeof(W_TEMP_LOAD_T);  //int 1 is offset to full title group starting	
	LogInt("Total index words:",max-1);	 //with this 1stWord, int 2 is bytes of the group

	off=GmZx(max,W_TEMP_LOAD_T);  freadWin(off,i,fp); //read offsets
	
	freadWin(&i,sizeof(int),fp);  //bytes of all titles in this nameSpace
	LogInt("Total bytes of titles in this nameSpace:",i);
	
	freadWin(&i,sizeof(int),fp); 
	if(ns==0) glt[iLang].cntTitle[iDomain]=i;
	LogInt("Total titles (including redirect) in this nameSpace:",i); //cnt of titles

	glt[iLang].szWord1[iDomain][ns]=buf; 

	pt=GmZx(max,W_POS_T); s=buf;	
	for(i=0;i<max;i++)
	{	pt[i].offTitle=off[i].offTitle;
		pt[i].midPoscnt=off[i].midPoscnt;
		pt[i].posWord1=s-buf; //remember 1stWord pos for later bsearch()
		N_S(s);
	}
	glt[iLang].maxWord1[iDomain][ns]=max-1;
	glt[iLang].ptG[iDomain][ns]=pt;

	Fm(off); 
	return 1;  
}

//extern void ToUp(char *str);

//match title with 1stWord array, and if match load all titles of its group
//nameSpace directs to different 1stWord arrays	  
char *FindTitles(char *title,int ns,BOOL bSearch,int *oCnt)
{	W_POS_T *pt,*ptOrg;  
	BYTE *endWord1,endC; 	 
	int max; char *word1;

	if(oCnt) *oCnt=0;
	
	if(!glt) { Log("no glt!!!!!!!!!"); return 0; }

	if(ns<0||ns>=glt[iLang].nsStr[iDomain].cnt) ns=0;

	if(!glt[iLang].ptG[iDomain][ns]&&!HttpLoadIndexOne(ns))   return 0;
	
	ptOrg=glt[iLang].ptG[iDomain][ns];
	max=glt[iLang].maxWord1[iDomain][ns];
	word1=glt[iLang].szWord1[iDomain][ns];

	if(!*title)	//random page
	{	pt=ptOrg+WRand(max);
		if(oCnt) *oCnt=pt->midPoscnt;
		return WReadIndexFile(ns,F_STR,pt->offTitle,pt[1].offTitle-pt->offTitle);
	}

	//if(bSearch&&iDomain!=1)  UtfUp1(title);
	if(iDomain!=1)  UtfUp1(title);

	endWord1=WMarkWordEnd((BYTE *)title,&endC);
	Log2("Index-Word of title",title);
	
	pt=WikiFindWord1(title,word1,ptOrg,max);

	if(!pt&&bSearch) pt=WikiSearchWord1(title,word1,ptOrg,max);
	
	*endWord1=endC; //recover title
	
	if(!pt) {  Log("No match for 1st word"); return 0; }
	   
	LogInt("Bytes of all titles in this index-word group:",pt[1].offTitle-pt->offTitle);

	endWord1=(BYTE *) WReadIndexFile(ns,F_STR,pt->offTitle,pt[1].offTitle-pt->offTitle);
	
	if(endWord1&&oCnt) *oCnt=pt->midPoscnt; //pt actually points to somewhere in global ptG[nameSpace]
	return (char *)endWord1; 
}

extern char *WReadDumpEx(HANDLE fpD,LONGLONG pos,int bytes,BOOL bInfo,BOOL bNewIndex); 
#define TEXT1	0x74786574	//text
#define REVI	0x69766572	   //revi  (revision>)
/*
char *CheckRaw(char *s)
{	DWORD *dw; char *start=s;

	while(1)
	{	
		To_C(s,'<'); 
		if(!*s) return start;
		s++;	
		dw=(DWORD *)s; 	   //test both </text> and </revision> in case missing former
		if(*dw==TEXT1&&start==s) { J_C(s,'>'); start=s; continue; }
		if(*dw==TEXT1||*dw==REVI) break;
	}
	s-=2; Z4(s);
} */

//posText can either be at header: <title ...
//or at text start
char *FindGetText(LONGLONG posText,int bytesText)
{	char *buf,*s;
	HANDLE fp; 
	char *fn;

	if(!glt||!(fn=glt[iLang].dumpName[iDomain])) return 0; 
	
	if(!glt[iLang].fpDump[iDomain]&&!(glt[iLang].fpDump[iDomain]=WOpenDumpFile(fn)))
	{	Log2Eq("Failed to open dump file",fn); 
		return 0;	 
	}

	fp=glt[iLang].fpDump[iDomain];
	if(!LibSeekEx(fp,posText)) return 0;
	 
	if(bytesText<1)	//empty text
	{	buf=GmC(KB); buf[0]=SPACE; return buf; }
	else if(bytesText>(10*MB)) return 0; //such a large text can only be error

	buf=GmC(bytesText+KB);
	if(!freadWin(buf,bytesText,fp)) { Fm(buf); return 0; }
	if(strncmp(buf,"<title",6)) return buf; //text
						  //header, skip it
	if(!(s=strstr(buf,"<text"))) { Fm(buf); return 0; }
	J_C(s,'>');
	s=scA(buf,s);
	if(!*buf)
	{	s=buf; *s++=SPACE;  }
	zm(s,KB);
	buf=ReGmC(s-buf+KB,buf);
	return buf;
}

char *StrStrBack(char *sOrg,char *s,char *str)
{	char c=*str,C=c;	
	int len=strlen(str);

	C_LOW(c); C_UP(C);

	while(s>sOrg)
	{	BACK_C2(sOrg,s,c,C);
		if(s<=sOrg)	return 0; 
		if(!strnicmp(str,s,len)) return s;
		s--;
	}
	return 0; 
}


char *FindGetTextInfo(LONGLONG posText)
{	char *buf,*s; 	
	LONGLONG pos; 
	int k,size=4*KB; //
	HANDLE fp; 
	char *fn;

	if(!glt||!(fn=glt[iLang].dumpName[iDomain])) return 0; 
	
	if(!glt[iLang].fpDump[iDomain]&&!(glt[iLang].fpDump[iDomain]=WOpenDumpFile(fn)))
	{	Log2Eq("Failed to open dump file",fn); 
		return 0;	 
	}
	fp=glt[iLang].fpDump[iDomain];
	if(!LibSeekEx(fp,posText)) return 0;

	buf=GmC(size);
	if(!freadWin(buf,size-20,fp)) { Fm(buf); return 0; }
	
	if(!strncmp(buf,"<title",6))  //header
	{	if(!(s=strstr(buf,"<text"))) { Fm(buf); return 0; }
		J_C(s,'>');
		Z8(s);
		buf=ReGmC(s-buf,buf);
		return buf;
	}
	
	pos=posText-(LONGLONG)(size-20);
	if(pos<0) pos=0;

	if(!LibSeekEx(fp,pos)) return 0;
	k=(int)(posText-pos);
	
	if(!freadWin(buf,k,fp)) { Fm(buf); return 0; }
	s=StrStrBack(buf,buf+size-20,"<text");
	if(!s)	{ Fm(buf); return 0; }
	J_C(s,'>');
	*s--=0;

	s=StrStrBack(buf,s,"<title");
	if(!s)	{ Fm(buf); return 0; }
	
	if(s!=buf)  s=scA(buf,s); //for sure!
	else s=buf+strlen(buf);
	buf=ReGmC(s+8-buf,buf);	
	return buf;
}



 //note: *oBytesText can be 0 if text is empty. it's not error
 //templates can have empty text, meaning that template should be ignored 
BOOL FindPagePosFromID(int id,LONGLONG *oPosText,int *oBytesText)
{	W_ART_T art;	
	HANDLE fp; 
	
	if(oPosText)  *oPosText=0;
	if(oBytesText)  *oBytesText=0;

	if(!glt||!(fp=glt[iLang].fpIndex[iDomain])) return 0; 
	
	zm(&art,sizeof(art));
	LibSeek(fp,glt[iLang].fPos[iDomain].art+id*sizeof(W_ART_T));
	if(!freadWin(&art,sizeof(W_ART_T),fp)) return 0; 
	if(oPosText) *oPosText=art.posArticle;
	if(oBytesText) *oBytesText=art.bytesArticle;
	return 1; 
}

extern void WWriteTitles(char *buf,int *off,int max); 

int *WikiFindManual(char *title,char *buf,int *off,int max)
{	char *s=title;	int i;	

	S_LOW1(s); 
	for(i=0;i<max;i++)
	{	if(!InstrcmpBothCase1((unsigned char *)title,(unsigned char *)(buf+off[i]))) 
		{	strcpy(title,buf+off[i]); //restore case 
			return off+i; 
		} 
	}
	return 0; 
}
//returns page ID, -1 if not found
int FindPageID(char *title,int ns)
{	char *buf=0,*s=0; 
	int max,*off=0,*find=0,i,add; 

	if(!(buf=FindTitles(title,ns,0,&max)))  return -1; 
	
	LogInt("Total titles in this title group",max);	
	LogInt("Bytes of the group",SizeMem(buf));

	s=buf;	off=GmIx(max); 
	
	if(ns==14) //category, first 2 ints are info about article titles in this category
	{	for(i=0;i<max;i++)	
		{	s+=8; //string starts after 2 ints
			off[i]=s-buf;
			N_S(s); //pass string
			s+=(sizeof(int)); //pass ID
		}
	}
	else //use first int (at entry start): total bytes of this entry
	{	add=0;
		for(i=0;i<max;i++)	
		{	s+=add;	//next record start
			add=(*(int *)s); //first int of record is bytes of this record
			off[i]=s-buf+8;	//string starts after 2 ints
		}
	}

	if(!*title)	 //random page no title name
	{
		find=off+WRand(max);
		strcpy(title,buf+find[0]);
	}
	else 
	{	
		find=WikiFindIt(title,buf,off,max,0);
		if(!find) 
		{	Log2("Trying one-by-one string compare for title",title);
			find=WikiFindManual(title,buf,off,max); 
		}
	}
			
	if(find) 
	{	s=buf+off[find-off]; 
		if(ns==14)
		{	catBytePos[0]=(*(int*)(s-8));	 //first int is bytes of titles in this category, second int is pos in
			if(catBytePos[0]) catBytePos[1]=(*(int*)(s-sizeof(int)));
		}
		N_S(s); i=(*(int*)s); //article ID
	}
	else i=-1;
	
	Fm(off); Fm(buf);   
	return i; 
}

#define IS_DDM(s,c) ( (*(s)==c&&*(s+1)==c)? 1:0)
#define IS_DDDM(s,c) ( (*(s)==c&&*(s+1)==c&&*(s+2)==c)? 1:0)
#define IS_GALLERY(s) ( ((s[1]=='g'||s[1]=='G')&&!strnicmp(s,"<gallery>",9))? 1:0 )

extern char *PreParse(char *buf);
extern char *GetSiteStrCur();

void LogTitle(char *title,int ns)
{
	if(!bWriteLog) return ;
	Log2("\r\n****** Got article title: *******",title);
	Log2Eq("Site",GetSiteStrCur());
	Log2Eq("Language",glt[iLang].name);
		
	if(!ns) Log2Eq("nameSpace","article");
	else Log2Eq("nameSpace",GetNSStr(ns));
}

//if redirect, *s is new title name
BOOL FindIsRedirect(char *s)
{	char *sOrg=s,*t=s;
	
	P_SPACE(s); 
	if(!*s||*s!='#'||strnicmp(s,"#redirect",9)) return 0; 
	s+=9; To_CN(s,'[',10); 
	if(*s!='[') return 0;
	P_SPACE1(s,'['); 	
	S_CPYTON(t,s,']',260);
	W_RE(sOrg,t);
	return 1;  
}

extern void FormatTitle(char *s);
char *FindPageInter(char *title,int ns,LONGLONG *oPos)
{	int i,bytes; 
	LONGLONG pos;

	FormatTitle(title);	
	LogTitle(title,ns);

	if((i=FindPageID(title,ns))<0) return 0; 
	if(!FindPagePosFromID(i,&pos,&bytes)) return 0;
	if(oPos) *oPos=pos; 
	return FindGetText(pos,bytes); 
}

//must handle redirect
//redirect to another ns is OK, buf fail if to another site or lang
//if bInfo, returned buffer is info + text. buffer starts as "<title..."
extern int StripNS(char *temp);  
char *FindPage(char *title,int *ns,BOOL bInfo,BOOL bPreParse)
{	int redirectCnt=10;
	char *buf=0,*bufInfo=0;
	LONGLONG pos=0; 
	
	while(redirectCnt--) //avoid infinite loop: A --> B and B --> A
	{	buf=FindPageInter(title,*ns,&pos);
		if(!buf) return 0;
		if(!FindIsRedirect(buf)) break;
		strcpy(title,buf);
		Fm(buf);
		*ns=StripNS(title);
	}
	if(!buf) 	
	{	Log("FindPage() failed: redirect over 10 times");  return 0;  }

	if(bInfo&&(bufInfo=FindGetTextInfo(pos)))
	{	bufInfo=ReGmC(SizeMem(bufInfo)+SizeMem(buf),bufInfo);
		strcat(bufInfo,buf);
		Fm(buf);
		buf=bufInfo;
	}
	if(bPreParse)  buf=PreParse(buf);
	return  buf;
}

extern char szTitle[];
extern char szHrefWiki[];
extern int UtfGetCharLen(char in);
extern BOOL IsLinkImg(char *t);
													  
char *FindCategory(int *oBytes) //show a list a articles in this category
{	char *bufNew,*s,*bufStr,*sNew; char name[8],temp[8];
	int i,k,half,cntTop=0; 

	if(oBytes)	*oBytes=0;
	
	if(!catBytePos[0]) return 0;

	if(!(bufStr=WReadIndexFile(0,F_CAT,catBytePos[1],catBytePos[0])))
	{	catBytePos[0]=0;  //be safe
		catBytePos[1]=0; 
		return 0;  
	} 

	s=bufStr; N_S(s);  //first str is category name	 
	k=*(int*)s;	 s+=sizeof(int);  // after name, cnt of article titles
	half=k/2; 


	bufNew=GmCx(catBytePos[0]*8+20*KB); sNew=bufNew;  //guess buf size
	sNew=scA(sNew,"<p><a name=\"CatTop\"></a><h2>Articles in this category</h2><p>\n"
		"<table style=\"width: 80%;\"><tr valign=\"top\">\n");
	
	zm(temp,sizeof(temp)); zm(name,sizeof(name));
	
	for(i=0;i<k;i++)
	{	if(i==0) sNew=scA(sNew,"<td><ul>");
		else if(i==half) 
			sNew=scA(sNew,
			"\n</ul></td>\n<td style=\"align:right;\"><ul>\n"); //start a new column
		
		if(*s>0)
		{	name[0]=*s; name[1]=0; }
		else strcpyN(name,s,UtfGetCharLen(*s));
		
		if(strcmp(temp,name)) 
		{	if(*s>0) { temp[0]=name[0]; temp[1]=0; }
			else strcpy(temp,name);

			sNew=Gcpy3(sNew,"<h4>",name,"</h4>");
			if(i>=half&&cntTop>10)	
			{	sNew=scA(sNew,
					"<span style=\"float: right; margin: 4px 4px 4px 4px\">"
					"<a href=\"#CatTop\">Back to top</a></span>");
				cntTop=0;
			}
		}
		sNew=Gcpy2(sNew,"\n<li>","<a href=\"/wiki/");
		sNew=PrefixLinkCurNoNs(s,sNew);	
		if(IsLinkImg(s)) sNew=scA(sNew,".igp");
		sNew=Gcpy3(sNew,"\">",s,"</a></li>");	
		N_S(s);	
		if(i>=half) cntTop++;
	}
	sNew=scA(sNew,"</ul></td></tr></table><p>\n");
	
	if(oBytes) *oBytes=(sNew-bufNew); //not including NULL
									  //for piecing together by memcpy()
	Fm(bufStr);	
	catBytePos[0]=0; catBytePos[1]=0;

	Z4(sNew);  
	bufNew=ReGmC(sNew-bufNew,bufNew);
	return bufNew; 
}





