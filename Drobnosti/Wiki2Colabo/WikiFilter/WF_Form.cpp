#include "WF_Main.h"

char *szCss,*szBullet,*szBg,*szHeadOrg;
char *szNSPos;
int sizeHeadOrg,sizeCss,sizeBullet,sizeBg;
char *th12[2],*td12[2];

extern int iMaxLang,iLang,iDomain;
extern char szTitle[];

extern HMODULE GetMe();
extern char *GetLangName(int i);

inline char *scAin(char *dest,char *src) //returns NULL end of *dest
{
	while(*src) *dest++=*src++;
	*dest=0;
	return dest; 
}


char *FormGetStyle(int i,int *oBytes,BOOL *bFreeAfterUse)
{	*bFreeAfterUse=0;
	*oBytes=0;
	if(i==6)
	{	if(!szCss) return 0; 
		*oBytes=sizeCss;
		return szCss;
	}
	if(i==4)
	{	if(!szBg) return 0; 
		*oBytes=sizeBg;
		return szBg;
	}
	if(i==5)
	{	if(!szBullet) return 0; 
		*oBytes=sizeBullet;
		return szBullet;
	}
	return 0; 
}

extern char *GetLangStr(int lang);

char *WLoadFormAddLang(char *s)
{	int i;	char temp[8];

	for(i=0;i<iMaxLang;i++)
	{	s=Gcpy3(s,"\r\n<option value='",ItoS(temp,i),"'>");
		s=scLess1(s,GetLangStr(i));
		s=scAin(s,"</option>");
	}
	return s;
}
extern int GetNSCnt();

extern BOOL bWriteLog;

char *WLoadFormBin(int id,char *fn,int *bytes,int extra)
{	HRSRC h; HGLOBAL g;	 char *buf=0,*bufNew=0;
	int k=0;
	HMODULE hdll=GetMe();
	
	if(bytes) *bytes=0;
	h=FindResource(hdll,MAKEINTRESOURCE(id),RT_RCDATA);
	k=SizeofResource(hdll,h);
	g=LoadResource(hdll,h);	
	buf=(char *)LockResource(g);
	if(!buf||!k) return 0; 
	
	bufNew=GmC(k+10+extra);
	memcpy(bufNew,buf,k);
	FreeResource(g);
	if(fn&&*fn) LibWriteFile(fn,bufNew,k);	 
	if(bytes) *bytes=k;
	return bufNew;
}

extern char *UtfUp1(char *inOut);

char *FormAddNS(char *s)
{	int i,k; char temp[8],*tp;
	k=GetNSCnt();

	for(i=0;i<k;i++)
	{	s=Gcpy3(s,"\r\n<option value='",ItoS(temp,i),"'>");
		if(!i) s=scAin(s,"Article");
		else
		{ 	tp=s;
			s=scLess1(s,GetNSStr(i));
			UtfUp1(tp);
		}
		s=scAin(s,"</option>");
	}
	return s;
}
void FreeForm()
{	Fm(szHeadOrg);	Fm(szCss); Fm(szBg); Fm(szBullet); 
	sizeBg=sizeBullet=sizeCss=sizeHeadOrg=0;
} 

void FormRidComment(int bHeaderOnly)
{	char *t;
	
	if(szHeadOrg) StrRidBetween(szHeadOrg,"<!--","-->");
	if(bHeaderOnly) return ;

	if(szCss)
	{	t=StrRidBetween(szCss,"/*","*/");
		sizeCss=t-szCss;
		szCss=ReGmC(sizeCss+4,szCss);
	}

}

char *StrInFromEnd(char *insertStr,char *insertAt,int insertStrLen)
{	int k; char *end;

	if(insertStrLen<1) k=strlen(insertStr);
	else k=insertStrLen;
	end=insertStr+k;

	memmove(end,insertStr,k); //move *insertStr to buf end
	memmove(insertAt+k,insertAt,insertStr-insertAt); //move *insertAt down
	memmove(insertAt,end,k);  //insert 
	return end;
}

void WLoadForm() //if form files exit, load them, else use our default 
{
/*
  char *s,*sOld,*t;
	char fn[MP];
	int k;
	
	FreeForm();	//in case we are re-loading	  RT_RCDATA 
	
	if(!(szBg=LibReadFile(WAddPath("headbg.jpg",fn),&sizeBg)))
		szBg=WLoadFormBin(IDR_BG,fn,&sizeBg,0);
	
	if(!(szBullet=LibReadFile(WAddPath("bullet.gif",fn),&sizeBullet)))
		szBullet=WLoadFormBin(IDR_BULLET,fn,&sizeBullet,0);

	if(!(szCss=LibReadFile(WAddPath("WikiFilter_main23.css",fn),&sizeCss)))
		szCss=WLoadFormBin(IDR_CSS,fn,&sizeCss,0);
									 // 2*KB 2*KB	sizeHead  sizeHead
	  //add KB to insert language names
	if(!(szHeadOrg=LibReadFileExtra(WAddPath("WikiFilter_header23.txt",fn),&k,KB)))
		 szHeadOrg=WLoadFormBin(IDR_HEADER,fn,&k,KB);
	

	if(!szHeadOrg) return ;	 //not recoverable
	FormRidComment(0);
								  //if none use our own
	if(!strstr(szHeadOrg,"langStrPos")||!strstr(szHeadOrg,"nsStrPos"))
	{	Fm(szHeadOrg); 
		szHeadOrg=WLoadFormBin(IDR_HEADER,fn,&k,KB);
		 FormRidComment(1);
	}
	s=strstr(szHeadOrg,"langStrPos");
	if(s)
	{	BACK_C(szHeadOrg,s,'<'); sOld=s;
		To_0(s);  t=s;
		s=WLoadFormAddLang(s);	
		StrInFromEnd(t,sOld,s-t);
	}
	else { s=szHeadOrg; To_0(s); }
		
	szHeadOrg=ReGmC(s+4-szHeadOrg,szHeadOrg); //shrink to actual size
		
	sizeHeadOrg=s-szHeadOrg;

	if((sOld=strstr(szHeadOrg,"nsStrPos")))	//name space insertion point
	{	BACK_C(szHeadOrg,sOld,'<');
		szNSPos=sOld;
	}
	else szNSPos=0;
*/
	
}
static char szHeadScript[]="document.SearchForm."; 
extern char *WGetMainPage();
extern int bShowRaw;

char *ConvertQuote(char *in,char *sNew)
{	unsigned char *s=(unsigned char *)in;
	while(*s)
	{	if(*s==C_Q) {	sNew=scA(sNew,"&rsquo"); s++; continue; }
		*sNew++=*s++;
	}
	*sNew=0;
	return sNew;
}

char *MakeRawName(char *title,char *out)
{	char tp[400];

	PrefixLinkCur(title,tp);
	return ConvertQuote(tp,out);
}
extern char *GetWebStr();

char *SpaceToUnder2(char *t,char *s)	//returns ending NULL
{	while(*s)
	{	if(*s==SPACE||*s==TAB||*s=='_') { P_C3(s,SPACE,TAB,'_'); *t++='_'; }
		else *t++=*s++;
	}
	*t=0; return t; 
}

extern SBOX_T *sb;

//*title is NULL if showing raw page
char *FormGetHeader(int bRaw,int *oBytes)	
{	char *s,*head,temp[8],*t;    

	if(!szHeadOrg) return 0; //no header!!! should not happen though

	head=GmC(sizeHeadOrg+4*KB+MAX_SPACE_USE*MAX_NS_LEN);
	if(!szNSPos)	//no ns pos, just ignore it
	{	memcpy(head,szHeadOrg,sizeHeadOrg);				
		s=head+sizeHeadOrg;
	}
	else
	{	memcpy(head,szHeadOrg,szNSPos-szHeadOrg); //cpy stuff before
		s=head+(szNSPos-szHeadOrg);
		s=FormAddNS(s); //add ns strs
		memcpy(s,szNSPos,sizeHeadOrg-(szNSPos-szHeadOrg)); //cpy stuff after
		s+=(sizeHeadOrg-(szNSPos-szHeadOrg));
	}
	
	if(iDomain)
	{	s=Gcpy4(s,"\r\n<SCRIPT>\r\n","document.SiteForm.TheSite.selectedIndex=",
				ItoS(temp,iDomain),";\r\n</SCRIPT>\r\n");	
	}
	if(iLang)
	{	s=Gcpy4(s,"\r\n<SCRIPT>\r\n","document.LangForm.TheLang.selectedIndex=",
				ItoS(temp,iLang),";\r\n</SCRIPT>\r\n");	
	}

	if(sb) //copy old values only if searched before,
	{	s=scAin(s,"\r\n<SCRIPT>\r\n");
		if(sb->keyOrg[0])
			s=Gcpy4(s,szHeadScript,"TheKey.value=\"",sb->keyOrg,"\";\r\n");
		if(sb->ns)
			s=Gcpy4(s,szHeadScript,"In.selectedIndex=",ItoS(temp,sb->ns),";\r\n");
		if(sb->max)
			s=Gcpy4(s,szHeadScript,"Max.selectedIndex=",ItoS(temp,sb->max),";\r\n");
		if(sb->bShowBestMatch)
			s=Gcpy4(s,szHeadScript,"Best.checked=",ItoS(temp,sb->bShowBestMatch),";\r\n");
		s=scAin(s,"</SCRIPT>\r\n");
	}

	s=scAin(s,"<div style=\"margin: 2px 10px 10px\">");
		
		///////// Main Page	link
	s=scAin(s,"<span style=\"float: right;\"><a href=\"/wiki/"); 	
	s=PrefixLinkCurNoNs(WGetMainPage(),s); 
	s=scAin(s,"\">Main Page</a></span>");
		
		///////// Random page
	s=scAin(s,"<span style=\"float: right;\"><a href=\"/wiki/RandomPage.ran"); 
	s=scAin(s,"\">Random page</a>&nbsp;&nbsp;&nbsp;</span>");
			
		////////// if not test page, show online link	
	if(szTitle[0])				
	{	s=scAin(s,"<span style=\"float: right;\"><a href=\"");		 
		s=scAin(s,GetWebStr()); BACK_C(head,s,'/'); s++; t=s;
		s=scAin(s,GetNSStrCur()); UtfUp1(t); 
		s=SpaceToUnder2(s,szTitle);
		s=scAin(s,"\" target=\"OnlineVersion\">Wiki site</a>&nbsp;&nbsp;&nbsp</span>");
	}
	if(szTitle[0]&&bShowRaw&&!bRaw)
	{	s=scAin(s,"<span><a href=\"/wiki/");
		s=MakeRawName(szTitle,s);
		s=scAin(s,".raw\" target=\"WikiText\"\">Show wiki text</a></span>\r\n");
	}
	s=scAin(s,"</div>");
	
	if(oBytes) *oBytes=s-head;	//will be freed soon, no bother to adjust size
	Z4(s); 
	return head;
}











