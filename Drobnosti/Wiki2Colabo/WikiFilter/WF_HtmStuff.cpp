#include "WF_Main.h"
#define MAX_COMMAND 9
#define COM_CLEAR_TEMP	0
#define COM_FREE		1
#define COM_RELOAD_FORM		2
#define COM_LOG_ON		3
#define COM_LOG_OFF		4
#define COM_IMG_ON		5
#define COM_IMG_OFF		6
#define COM_TEMP_ON		7
#define COM_TEMP_OFF	8
#define COM_CHECK_ON		9
#define COM_CHECK_OFF		10


/*
<SCRIPT>
document.SearchForm.TheKey.value=;  //write user input
document.SearchForm.In.selectedIndex=0;	 //selected nameSpace index
document.SearchForm.Max.selectedIndex=0; //max show title index
document.SearchForm.Best.checked=0;	  //show best match article?
</SCRIPT>

<SCRIPT>
document.SearchForm.TheSite.selectedIndex=0;	 //selected nameSpace index
</SCRIPT>
*/
BOOL bTest=0; BOOL bWriteLog=0;
extern int bShowImg,bShowRaw,bShowTemplate;
							  //show any buf, will free it. bufLen must be exact strlen
							  //if NULL included, NULL will be copied to html text!

char szCommandNote[MAX_COMMAND][64]=
{	"free all cached Templates",
	"free all allocated memory, close all opened files ( = Restart)",
	"reload form (header, css, and background image, etc.)",
	
	"enable writing log file", "disable writing log file",
	"show image", 
	"hide image",
	"show template","hide template",
//	"check some html tags errors"
};

static char szCommand[MAX_COMMAND][20]=
{	"clearTemp",
	"free",		  
	"form",
	"logOn",		"logOff",
	"imageOn",		"imageOff",		
	"tempOn",		"tempOff",
//	"checkOn",		"checkOff"
};

char szOptionCom[]=
			"<p>To pass a command, type it in the "
			"<b>Search</b> text box, like \"<code>&lt;form</code>\"" 
			" (to view a random page leave it empty) and press \"<b>Go</b>\" or \"Enter\" key."
			"<p>Note \"&lt;\" is required so that it's not confused with a search word."
			" Also, anything after the necessary"
			" part of a command string is igored. So \"<code>&lt;form</code>\""
			"and \"<code>&lt;format</code>\" will both cause "
			"form files to be reloaded.";

char szOptionNote[]=
			"<p>The above options will be saved to the ini file, "
		   "so that the next time WikiFilter starts, they remain the same."
		   "<p>Log file is normally a useless waste. "
		   "If you do not have image files and "
		   "do not want to see empty boxes in a page either, turn image off. "
		   "Templates are frequently used in wiki pages and contain abundent "
		   "information not available in the text. So turn it off only if you "
		   "often see them wrongly parsed. And in that case, dropping a note "
		   "at the <a href=\"http://wikifilter.sourceforge.net\">project site</a> "
		   "is appreciated.";


extern void WikiTemplateFree();
extern char *InitGetLangInfo(char *s);
extern char *GetPageFixed(int *oBytes,char *strFixed,BOOL bFree);

char *HtmShowCommand(int *oBytes)
{	char *buf,*s; DWORD i;  

	buf=GmC(10*KB); s=buf;	//  or <b>&lt;*</b>
	s=scA(s,"<html><body><p><h2>Defined commands (case-insensitive)</h2><ol>");
	
	s=scA(s,"<li>    empty text brings up a random page "
			"in the current name space, selected language and site. "
			"Literally you are searching for any page.</li>"
	);
	
	s=scA(s,"<li><code><b>&lt;?</b> or <b></code>&lt;</b>    show this help page.</li>"
	);
	
	for(i=0;i<MAX_COMMAND;i++)
		s=Gcpy5(s,"<li><code><b>&lt;",szCommand[i],"</b></code>    ",szCommandNote[i],"</li>");
	
	s=scA(s,"</ol><p>");
 	s=scA(s,szOptionCom);

	s=scA(s,"<h4>Current status</h4><ul>");
	

	if(bWriteLog) s=scA(s,"<li>Log file: <b>ON</b></li>");
	else  s=scA(s,"<li>Log file: <b>OFF</b></li>");
	
	if(bShowImg) s=scA(s,"<li>Show image: <b>ON</b></li>");
	else  s=scA(s,"<li>Show image: <b>OFF</b></li>");

	if(bShowTemplate) s=scA(s,"<li>Show template: <b>ON</b></li>");
	else  s=scA(s,"<li>Show template: <b>OFF</b></li>");

	s=scA(s,"</ul>"); s=scA(s,szOptionNote);


	s=InitGetLangInfo(s);
	s=scA(s,"<p></body></html>");
	return GetPageFixed(oBytes,buf,1);
}

extern void WikiExit();
//free everything
char *HtmDoCommandFree(int *oBytes)
{	char *s,*buf=GmC(400);
	
	WikiExit(); 
	s=buf;
	s=Gcpy3(buf,"<html lang=\"en\"><body>",szCommandNote[COM_FREE],
	"<br>Done<p>Sorry for this poor output. "
	"(WikiFilter has let go all resources)</body><html>");
	*oBytes=s-buf+1;
	return buf;
}

extern void WLoadForm();

char *HtmDoCommand(char *comStr,int *oBytes)
{	int i; char temp[200];  

	for(i=0;i<MAX_COMMAND;i++)
	{	
		if(!strnicmp(comStr,szCommand[i],strlen(szCommand[i])))
		{	
			if(i==COM_CLEAR_TEMP) WikiTemplateFree();
			else if(i==COM_RELOAD_FORM) WLoadForm();
			else if(i==COM_LOG_ON) bWriteLog=1;
			else if(i==COM_LOG_OFF) bWriteLog=0;
			else if(i==COM_IMG_ON) bShowImg=1;
			else if(i==COM_IMG_OFF) bShowImg=0;
			else if(i==COM_TEMP_OFF) bShowTemplate=0;
			else if(i==COM_TEMP_ON) bShowTemplate=1;
			else if(i==COM_FREE) 
			{	return HtmDoCommandFree(oBytes);	  
				
			}
			strcpy(temp,szCommandNote[i]); strcat(temp,"<p>Done.");
			return GetPageFixed(oBytes,temp,0);
		}
	} 
	return HtmShowCommand(oBytes); 
}

////margin style: top,right,bottom,left, (diagonal opposite of window position)
