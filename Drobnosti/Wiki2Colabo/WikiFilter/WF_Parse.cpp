#include "WF_Main.h"
extern BOOL bTest,bShowImg,bShowTemplate;

extern int ParseList(char *s,char *sNew);
extern int ParseBold(char *s,char *sNew);
extern int ParseCaption(char *s,char *sNew);
extern int ParseLink(char *s,char *sNew,int bDoImg,CategoryList *categoryList);
extern int ParseTable(char *s,char *sNew);
extern int ParseTemplate(char *s,char *sNew,int bDoTemp);
extern void ParseStuff(char *s,char *sNew,int *oBytes); 
extern void TableRidHidden(char *buf);
extern int ParseCodeBlocks(char *s,char *sNew);

extern void TableClean(char *s);

//used by PreParse(),ParseLink(),ParseTable() TableRidHidden()
int *offSPre,*offEPre; 

BOOL ParseTest(char *bufRaw,int *oBytes)
{	char *s=bufRaw;

	if(!s) return 0; 
	P_SPACE(s);
	if(!*s) { *oBytes=strlen(s); return 0; }
	return 1; 
}
  //parse all wiki stuff
char *Parse(char *bufRaw,int *oBytes, CategoryList *categoryList)
{	char *buf1,*buf2;  
	
	if(oBytes) *oBytes=0;
	if(!ParseTest(bufRaw,oBytes)) return bufRaw;
	
	buf1=GmC(SizeMem(bufRaw)*2+400*KB);	//allocate 2 bufs and shift them
	buf2=GmC(SizeMem(buf1));			//between 2 parsing steps:
										//step1's output = step2's input
	if(!offSPre) 
	{	offSPre=GmI(MAX_OFF*2); 
		offEPre=offSPre+MAX_OFF;
	} 
//	bTest=1;
	
	buf1[0]=LB1; buf1[1]=LB2; 

	strcpy(buf1+2,bufRaw);
	Fm(bufRaw);

  extern int ParseNoWiki(char *s,char *sNew, bool parseOut);
  if (ParseNoWiki(buf1, buf2, true)) BufShift(&buf1,&buf2); 

	if(ParseTemplate(buf1,buf2,bShowTemplate)) BufShift(&buf1,&buf2); 
	if(ParseBold(buf1,buf2)) BufShift(&buf1,&buf2);

	TableRidHidden(buf1);

	if(ParseLink(buf1,buf2,bShowImg,categoryList))	BufShift(&buf1,&buf2);

	if(ParseTable(buf1,buf2)) BufShift(&buf1,&buf2);
	
	ParseStuff(buf1,buf2,oBytes);	BufShift(&buf1,&buf2);

  if (ParseCodeBlocks(buf1,buf2)) BufShift(&buf1,&buf2);

  if(ParseList(buf1,buf2)) BufShift(&buf1,&buf2); // ParseCodeBlocks << ParseList (must be called before) (as nested lists creates indenting, ie. wiki code blocks)

  if(ParseCaption(buf1,buf2))	BufShift(&buf1,&buf2); // ParseList << ParseCaption (as caption becomes "## xxx" which is wiki Link

  if (ParseNoWiki(buf1, buf2, false)) BufShift(&buf1,&buf2); //get the nowiki stuff back

  Fm(offSPre);
	Fm(buf2);
	return buf1;
}
