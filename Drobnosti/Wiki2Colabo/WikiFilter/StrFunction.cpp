//this code is completely independent
//manages the struct type STR_T
//major function: uses fast bsearch() to search an array of strings
//mainly used for getting name space index and table props

#include <windows.h>
#include <stdlib.h>


#define GmC(x) (char *)HeapAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,x)
#define GmI(cnt) \
   (int *)HeapAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,(cnt)*sizeof(int))
#define ReGmC(cnt,buf) \
   (char *)HeapReAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,buf,(cnt))
#define ReGmI(cnt,buf) \
   (int *)HeapReAlloc(GetProcessHeap(),HEAP_ZERO_MEMORY,buf,(cnt)*sizeof(int))
#define Fm(x) do{ if(x) { HeapFree(GetProcessHeap(),0,x); x=0; } }while(0)
#define zm(pos,cnt) memset(pos,0,cnt)
#define N_S(s)  while(*s++)
#define S_CPY(dest,src)  do{ while(*src) *dest++=*src++; *dest=0; }while(0) 


typedef struct
{	char *buf;
	int *off,*index;  //note: index is internal use only if sorted
	int cnt,maxLen;
}STR_T;

static STR_T *theStr;
static int theKeyLen;
					//each use separate buf so 
					//when reallocating, no need to do complicated memmove()
void StrGetMem(STR_T *str,int bytes,int max)
{	int i;
	str->buf=GmC(bytes+4);
	str->off=GmI(max);
	str->index=GmI(max);
	for(i=0;i<max;i++)	str->index[i]=i;
}

void StrGetMemNoBuf(STR_T *str,int max)
{	int i;
	str->off=GmI(max);
	str->index=GmI(max);
	for(i=0;i<max;i++)	str->index[i]=i;
}

void StrReGetMem(STR_T *str,int bytes,int max)
{	str->buf=ReGmC(bytes,str->buf); 
	str->off=ReGmI(max,str->off); 	
	str->index=ReGmI(max,str->index);  
}

void StrFree(STR_T *str)
{	Fm(str->buf); Fm(str->off); Fm(str->index); str->cnt=0;  }

void StrFreeNoBuf(STR_T *str)
{	 Fm(str->off); Fm(str->index); str->cnt=0;  }

int StrGetSize(STR_T *str) // a\0b\0 ==> returns 4
{	char *s;
	s=str->buf+str->off[str->cnt-1];
	while(*s++); 
	return (s-str->buf);
}

void StrGetMaxLen(STR_T *str)
{	int i,j,k=str->cnt-1,max=0;

	if(k<0)	
	{	str->maxLen=strlen(str->buf); return ; }
		
	for(i=0;i<k;i++)
	{	j=str->off[i+1]-str->off[i]-1;
		if(max<j) max=j;
	}
	j=strlen(str->buf+str->off[i]);	
	if(max<j)	max=j;
	str->maxLen=max;
}

int StrCmpFunction(const void *i1,const void *i2)
{	return strcmp(theStr->buf+theStr->off[(*((int*)i1))],
				theStr->buf+theStr->off[(*((int*)i2))]); }
		

void StrSort(STR_T *str)
{	if(str->cnt<2||!str->buf) return ;
	theStr=str;
	qsort(str->index,str->cnt,sizeof(int),StrCmpFunction);
}

int StrSearchFunction(const void *key,const void *index)
{	return strcmp((char *)key,theStr->buf+theStr->off[*((int*)index)]);	}

int StrSearchFunctionI(const void *key,const void *index)
{	return stricmp((char *)key,theStr->buf+theStr->off[*((int*)index)]);	}

int StrSearchFunctionN(const void *key,const void *index)
{	char *t=theStr->buf+theStr->off[*((int*)index)];
	return strncmp((char *)key,t,strlen(t));	
}

//returns index of match or -1 if no match
int StrSearch(STR_T *str,char *key)	
{	int *i;
	if(!str->buf) return -1; 
	theStr=str;
	i=(int *)bsearch(key,str->index,str->cnt,sizeof(int),StrSearchFunction);
	if(!i) return -1;
	return str->index[(i-str->index)]; // *i is pos within str->index

}

//returns index of match or -1 if no match
//case-Insensitive search
int StrSearchI(STR_T *str,char *key)	
{	int *i;

	if(!str->buf) return -1; 
	theStr=str;
	i=(int *)bsearch(key,str->index,str->cnt,sizeof(int),StrSearchFunctionI);
	if(!i) return -1;
	return str->index[(i-str->index)];
}
	   //*str has standard strs, but *key may contain extra stuff
	   //so cmp according to len of strs in *str
int StrSearchN(STR_T *str,char *key)	
{	int *i;

	if(!str->buf) return -1; 
	theStr=str;	
	i=(int *)bsearch(key,str->index,str->cnt,sizeof(int),StrSearchFunctionN);
	if(!i) return -1;
	return str->index[(i-str->index)];
}

extern char *UtfLow(char *in,char *out); //*in *out can be same buf

//note: a few utf chars shrink after changing case,
//so do NOT use N_S(s). always use str->off[]
void StrLowUtf(STR_T *str)
{	int i,k; 
	k=str->cnt;
	for(i=0;i<k;i++) 
		UtfLow(str->buf+str->off[i],str->buf+str->off[i]); 
}
	//*buf must end with >=2 NULLs
int StrConvert(STR_T *str,char *buf)
{	char *s=buf,*sNew; int k=0,i;

	zm(str,sizeof(STR_T));
	 
	while(*s) { N_S(s);	k++; }
	if(!k) return 0; 

	StrGetMem(str,s+4-buf,k);  sNew=str->buf; s=buf;
	for(i=0;i<k;i++)
	{	str->off[i]=sNew-str->buf;
		S_CPY(sNew,s); sNew++; s++;		
	}
	str->cnt=k;		
	return k;
}

int StrGetOff(STR_T *str)
{	char *s=str->buf; int k=0;

	while(*s) 
	{	str->off[k]=s-str->buf; 
		N_S(s);	k++; 
	}
	str->cnt=k;
	return k;
}

int StringCnt(char *s)
{	int k=0;

	while(*s) { N_S(s); k++; }
	return k;
}
	   //form a *str using *buf
int StrInit(STR_T *str,char *buf,int bSort)
{	int i;

	str->buf=buf;
	str->cnt=StringCnt(buf);
	str->off=GmI(str->cnt);
	str->index=GmI(str->cnt);

	StrGetOff(str);
	StrGetMaxLen(str);
	for(i=0;i<str->cnt;i++) str->index[i]=i;
	if(bSort) StrSort(str);
	return str->cnt;
}




