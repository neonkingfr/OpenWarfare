//convert case for utf chars
//rarely used, no interest because i feel no need

#include "WF_Main.h"

typedef struct
{	char from[8]; //from[] is lower case for utfToUp[], upper for utfToLow[]
	char to[8];	//convert to this 
}LOWUP_T;

extern LOWUP_T utfToUp[745],utfToLow[735]; //table borrowed from mediawiki

int UtfGetCharLen(char in)
{	unsigned char c=(unsigned char)in;
	
	if(c<127) return 1; 
	if(c<=0xd6&&c>=0xc2) return 2;
	if(c<=0xef&&c>=0xe1) return 3;
	if(c==0xf0)  return 4;	
	return	1;
}

int UftSearchFunction(const void *key,const void *elem)
{	return strcmp((char*)key,((LOWUP_T *)elem)->from);	}
	
inline char *UtfInter(char *in,char *out,char **pIn,LOWUP_T *up,int cntUp)
{	char temp[8],*tp=temp;	LOWUP_T *t;
	unsigned char c=(unsigned char)*in;

	if(c<=0xd6&&c>=0xc2)  S_CPYN(tp,in,2); //2-char long
	else if(c<=0xef&&c>=0xe1) S_CPYN(tp,in,3); //3-char long
	else if(c==0xf0) S_CPYN(tp,in,4);	 //4-char long, rare
	else { 	*out++=*in++;  *pIn=in; return out;  } //wrong char

	t=(LOWUP_T *)bsearch(temp,up,cntUp,sizeof(LOWUP_T),UftSearchFunction);
	if(t)  tp=t->to; 	 //found upper case
	else  tp=temp; 	//not found, should be upper case already
	while(*tp) { *out++=*tp++; }
	
	*pIn=in; 
	return out;
}

//*in *out can be same buf
//returns end of *out
char *UtfUp(char *in,char *out) 
{	while(*in)
	{	if(*in>0) { *out=*in++; C_UP(*out); out++; }
		else out=UtfInter(in,out,&in,utfToUp,745);
	}
	*out=0;
	return out; 
}

char *UtfLow(char *in,char *out) //*in *out can be same buf
{	while(*in)
	{	if(*in>0) { *out=*in++; C_LOW(*out); out++; }
		else out=UtfInter(in,out,&in,utfToLow,735);
	}
	*out=0;
	return out; 
}

char *UtfLowN(char *in,char *out,int cnt) //*in *out can be same buf
{	while(*in&&cnt--)
	{	if(*in>0) { *out=*in++; C_LOW(*out); out++; }
		else out=UtfInter(in,out,&in,utfToLow,735);
	}
	*out=0;
	return out; 
}

//returns end of *inOut, plus 1 if ansi, plus 2 or 3 or 4 if other
//so the difference between input buf and returned buf is 
//char len of char1
//if first char shrinks after case change, re-cpy 
char *UtfUp1(char *inOut)
{	char *in=inOut;

	if(*inOut>0) { C_UP(*inOut); return (inOut+1);  }
	inOut=UtfInter(inOut,inOut,&in,utfToUp,745);
	if(inOut==in) return inOut;
	strcpy(inOut,in); //shrink: char1 is 3-bytes, but after changing, 1-bytes  
	return inOut;	  //must cpy all chars after pos 3 to pos 2
}  

char *UtfLow1(char *inOut)
{	char *in=inOut;

	if(*inOut>0) {  C_LOW(*inOut); return (inOut+1); }
	inOut=UtfInter(inOut,inOut,&inOut,utfToLow,735);
	if(inOut==in) return inOut;
	strcpy(inOut,in);
	return inOut;
} 
