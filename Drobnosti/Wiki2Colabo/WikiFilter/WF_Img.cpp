#include "WF_Main.h"

static char imgTextSpace[1024];
typedef struct
{	int px;
	char *text;
	BOOL thumb;
	int dir;
}IMG_T;

static char szDirThumb[5][20]=
{	"thumb tnone\">","thumb tleft\">","thumb tright\">",	"thumb\">","thumb tnone\">"};

static char szDirFloat[5][20]=
{	"floatnone\">","floatleft\">","floatright\">","float\">","floatnone\">"};
 
extern BOOL bTest;

inline char *scAin(char *dest,char *src) //returns NULL end of *dest
{
	while(*src) *dest++=*src++;
	*dest=0;
	return dest; 
}

inline BOOL ImgIsThumb(char *s)
{	if(!strnicmp(s,"thumb",5)||!strnicmp(s,"framed",6)||
		!strnicmp(s,"frame",5)||!strnicmp(s,"enframed",8))
		 return 1;
	return 0; 	
}

inline int ImgGetDirection(char *s)
{	if(!strnicmp(s,"left",4)) return 1; 
	if(!strnicmp(s,"right",5)) return 2;
	if(!strnicmp(s,"center",6)||!strnicmp(s,"centre",6)) return 2;
	if(!strnicmp(s,"none",4)) return 4;

	return 0; 
}

inline int ImgGetPx(char *s)
{	char *start=s;

	if(!IS_DIG(*s)) return 0; 
	while(IS_DIG(*s)) s++;
	if(*s=='x'||(*s=='p'&&s[1]=='x')) return WStrToInt(start); 
	return 0;
}

//[[Image:Wikibooks-logo.png|35px|*]]
BOOL ImgGetProp(char *s,IMG_T *it)
{	int b=0;
	zm(it,sizeof(IMG_T));
	P_SPACE(s);
	while(*s)
	{	To_C(s,'|'); if(!*s) break;
		P_SPACE2(s,'|','*'); if(!*s) break;
		
		if(!it->thumb) //is "thumb" or "thumbnail"?
		{	if((it->thumb=ImgIsThumb(s))) continue;;	}
		
		if(!it->dir) //is "left" etc?
		{	if((it->dir=ImgGetDirection(s))) continue; }	

		if(!it->px)	 //is size?
		{	if((it->px=ImgGetPx(s))) continue;	}

		//not prop, it must be text. Use the last text specified
    char *text = imgTextSpace;
    S_CPYTO(text,s,'|');
    it->text = imgTextSpace;
	}
	if(it->dir||it->px||it->text||it->thumb) return 1; 
	return 0; 
}

//must rid various stuff
char *ImgFormatTitle(char *s,char *t)
{	char c;
	
	while((c=*s))
	{	if(c==C_QQ||c=='|') {	s++; continue; }
		if(c==SPACE||c=='_') { P_C2(s,SPACE,'_'); *t++=SPACE; continue; }
		if(c=='<')	 {	s++; J_C(s,'>');  continue;	 }
		*t++=*s++;
	}								
	*t=0; return t;
}

char *ParseImgOne(char *s,char *sNew)
{	IMG_T it; int divC=0;  char *sOld,*sNewOrg=sNew;	
	int showText;
	
  if (strnicmp(s,"image:", 6)==0)
    s += 6; //skip image: prefix

	P_SPACE2(s,':','[');
	
	sOld=s;	 //return sNew;
	
	if(!ImgGetProp(s,&it)) 
	{	
    char *imageNameConverted = unconst_cast(cc_cast(GWikiFilesList.AddImage(s))); //BIS extension: we need to download the image file from the wiki
    sNew=Gcpy6(sNew,"<img src=\"",GWikiFilesList.GetImagesURLRoot().MutableData(),imageNameConverted,"\" alt=\"",s,"\">");
		return sNew;
	}
			//thumb caption		 //text contains links
	if((it.thumb&&it.text)||(it.text&&strstr(it.text,"<a href="))) showText=1;
	else showText=0;	 
	
	if(it.thumb||it.dir||showText)  //thumb
	{	sNew=scAin(sNew,"\r\n<div class=\"");	  
		if(it.thumb) { if(!it.dir) it.dir=2; sNew=scAin(sNew,szDirThumb[it.dir]);	}
		else if(it.dir) sNew=scAin(sNew,szDirFloat[it.dir]);
		else  sNew=scAin(sNew,szDirFloat[2]);
		
		if(it.px)
		{	wsprintf(sNew,"\r\n<div style=\"width: %dpx;\">",it.px+2); 
			To_0(sNew);
		}
		else sNew=scAin(sNew,"\r\n<div>");
		divC=2;
	}
  //<a href="images/abc.png">
  const char *imageNameConverted = GWikiFilesList.AddImage(s); //BIS extension: we need to download the image file from the wiki. Trailing '|' will be removed inside.
  const char *imageNameConvertedOld = imageNameConverted;
	sNew = Gcpy2(sNew,"\r\n<a href=\"", GWikiFilesList.GetImagesURLRoot().MutableData());
	S_CPYTO(sNew,imageNameConverted,'|');   W_RE(sNewOrg,sNew);
	sNew=scAin(sNew,"\">");	
	
	//<img src="images/abc.png"
  imageNameConverted = imageNameConvertedOld;
  sNew=Gcpy2(sNew,"<img src=\"", GWikiFilesList.GetImagesURLRoot().MutableData());
	S_CPY(sNew, imageNameConverted); To_C(s, '|'); W_RE(sNewOrg,sNew);
	*sNew++=C_QQ; 
  //<img src="images/abc.png" width="180"
	if(it.px) { wsprintf(sNew," width=\"%d\"",it.px); To_0(sNew); }
	
	if(it.text)	 //<img src="/wiki/w:en:image:abc.png" width="180" title="abc"
	{	sNew=scAin(sNew," alt=\""); 
		sNew=ImgFormatTitle(it.text,sNew);
		*sNew++=C_QQ;
	}
	*sNew++='>';  //close src, <img src="/wiki/w:en:image:abc.png" width="180" title="abc">
	
	sNew=scAin(sNew,"</a>"); //close link
	
	if(showText)
	{	
		sNew=scAin(sNew,"\r\n<div class=\"thumbcaption\">");
		sNew=scAin(sNew,it.text);
		sNew=scAin(sNew,"</div>");
	}
	if(divC==1) sNew=scAin(sNew,"</div>\r\n");
	else if(divC==2) sNew=scAin(sNew,"</div></div>\r\n");
	return sNew; 
}
extern BOOL HasExt(char *fname,char *extStrs);
extern BOOL IsLinkImg(char *s);


//<gallery>
//[[Image:{{{1}}}|135px|left|thumb|{{{2}}}]]
//just turn gallery into img links
int ParseGallery(char *s,char *sNew)
{	char *sOld=s,*t,*tp;	 
	
	while(1)
	{	if(!(s=strstr(s,"<gallery>"))) break;
		memcpy(sNew,sOld,s-sOld); sNew+=(s-sOld);
		J_C(s,'>');	P_SPACE(s);
		
		t=strstr(s,"</gallery>");	//terminate
		if(t) { *t++=0;	J_C(t,'>'); }
		else {  t=s; To_0(t); }
		
		while(*s)
		{	if(!IsLinkImg(s))
			{	
				S_CPYTO(sNew,s,LB); 
				sNew=scAin(sNew,"<br>"); //*sNew++=LB; *sNew++=LB;	
				while(IS_SPACE(*s)) *sNew++=*s++;
				continue;
			}
			tp=s; To_C3(tp,'|',LB,'<');

			*sNew++='['; *sNew++='['; //start tag
			
			S_CPYPOS(sNew,s,tp);
			sNew=scAin(sNew,"|200px|thumb"); //add prop
			
			S_CPYTO(sNew,s,LB); //has text?
			
			*sNew++=']'; *sNew++=']';  //end tag
			while(IS_SPACE(*s)) *sNew++=*s++;
		}	
		sOld=s=t;
	}
	S_CPY(sNew,sOld);

	Z8(sNew);
	return 1; 
}








