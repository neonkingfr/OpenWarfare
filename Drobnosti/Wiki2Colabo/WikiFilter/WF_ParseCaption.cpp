//caption and toc

#include "WF_Main.h"

extern int bTest;

static char szCap1[7][32]={"","<br style=\"clear:left\"><h1>",
"<br style=\"clear:left\"><h2>",
"<br style=\"clear:left\"><h3>","<br style=\"clear:left\"><h4>",
"<br style=\"clear:left\"><h5>",""};
static char szCap2[7][10]={"","</h1>","</h2>","</h3>","</h4>","</h5>",""};


int CharCnt(char *s,char c)
{	int k=0;
	while(*s==c) { s++; k++; }
	return k;
}

extern int IsWeb(char *dir);
extern BOOL IsLinkImg(char *s);

char *CapCpyLink(char *sNew,char *s,char **pS)
{	char *t;  char *sNewOld=sNew;

	P_SPACE1(s,'['); t=s;
	To_C(s,']'); 
	
	if(IsWeb(t)) 
	{	BACK_C(t,s,SPACE); //web link text?
		if(*s==SPACE) s++;
		else  //a web link in caption and NO alt text! ignore
		{	J_C(s,']'); *pS=s; 
			return sNew;
		}
	}
	else
	{	if(IsLinkImg(t)) //how odd to have img link as caption!
		{	J_C(s,']'); P_SPACE(s); *pS=s; 
			return sNew;
		}
		
		BACK_C(t,s,'|');  
		if(*s=='|') s++; 
	}
	S_CPYTO(sNew,s,']'); W_RE(sNewOld,sNew);
	P_C(s,']');	*pS=s;
	return sNew; 
}

char *CapCpy(char *sNew,char *s,char *end)
{	char *t=sNew,*org=sNew,cEnd; 

	cEnd=*end; *end=0;
	while(*s)
	{	if(*s=='<') { J_C(s,'>'); P_SPACE(s); continue; }
		if(*s=='[') { sNew=CapCpyLink(sNew,s,&s); continue; }
		if(*s==C_Q&&s[1]==C_Q) { P_C(s,C_Q); continue; }
		*sNew++=*s++;
	}
	*end=cEnd;
	*sNew=0;
	W_RE(t,sNew);
	if((sNew-org)>40)
	{	sNew=org+20; 
		To_CN(sNew,SPACE,30); *sNew++=SPACE;
		*sNew++='.'; *sNew++='.'; *sNew++='.';
		*sNew=0;
	}
	return sNew;	
}

//==Bloopers[http://www.moviemistakes.com/film1114]==\n

//===[[Holy Trinity]]===
//caption starts <a name="What_is_MediaWiki.3F">
char *ParseCaptionOne(char *s,char *sNew,char **pS,char *sNewOrg,int *off,int *total)
{	int k=0; char *t;

	while(*s==C_EQ) { s++; k++; }
	P_SPACE(s);
	if(k>5) k=5;
	t=s;
	To_C(t,C_EQ);
	if(CharCnt(t,C_EQ)!=k)
	{	To_C(t,LB);  //goto line break
		BACK_C(s,t,C_EQ); 
		while(*t==C_EQ) t--; t++;	//back to caption text end
	}
#if CONVERT_TO_MARKDOWN
  while (k--) *sNew++='#'; //markdown header
  *sNew++=' ';
  S_CPYPOS(sNew,s,t);	W_RE(sNewOrg,sNew); //cpy caption text
#else
  if(k==2||k==3)  //prefix an in-page link
	{	sNew=scA(sNew,"<a name=\"");  
		if(k==2&&off)	*off=sNew-sNewOrg;
		sNew=CapCpy(sNew,s,t);
		sNew=scA(sNew,"\"></a>");	
	}
	sNew=scA(sNew,szCap1[k]);	//start tag
	S_CPYPOS(sNew,s,t);	W_RE(sNewOrg,sNew); //cpy caption text
	sNew=scA(sNew,szCap2[k]);	//end tag
#endif

	*sNew++=LB; 

	P_C(s,C_EQ); //pass ending '='s
	*pS=s;
	return sNew;
}
// bad tags like this one: both are starting tags
//<h3>General information<h3>	
char *CheckTagH(char *s)
{	char tp[8],*t,*tt; 
	tp[0]='<'; tp[1]='/'; tp[2]='h'; tp[4]='>';	tp[5]=0;
	while(*s)
	{	if(*s!='<') { s++; continue; }
		if(s[1]!='h'||!IS_DIG(s[2])) { s++; continue; }
		
		tp[3]=s[2];	s+=4;
		t=s; To_C(s,LB);  *s=0;
		
		if(strstr(t,tp)) { *s++=LB; continue; }	 //ok
		
				//2 open tags, insert '/', hopefully it wont overwrite text
		if((tt=strstr(t,tp+2)))	
		{	tt-=1; memcpy(tt,tp,5); if(s>(tt+5)) *s=LB; s++; continue; }
		
		t-=3; *t='z'; //one open tag only, just hide it, because
		*s++=LB;	  //we have no idea where it should end
	}
	return s;
}	

int ParseCaption(char *s,char *sNew)
{	char *sOld=s,*sOrg=s,*sNewOrg=sNew,c,*t,*tp; int b=0,bToc;
	int off[102],cnt=0,i,total=0; 

	
	if((t=strstr(s,"==")))
	{	*t=0;
		if(!(strstr(s,"__NOTOC__"))) bToc=1;
		else bToc=0;
		*t='=';
	}  
	zm(off,sizeof(off));
	while((c=*s))
	{	//if(c=='<') { J_C(s,'>'); continue; }
		if(c!=LB) { s++; continue; }  
		P_SPACE(s);	
		if(*s!=C_EQ) {	 continue; }
		
		t=s; P_C(t,C_EQ);   //pass all '='s
		tp=t;
		To_C2(tp,C_EQ,LB);
		if(*tp!=C_EQ)
		{ 
			continue;	//no ending tag
		}
		memcpy(sNew,sOld,s-sOld); sNew+=(s-sOld);
		if(bToc&&cnt<99)
		{	sNew=ParseCaptionOne(s,sNew,&s,sNewOrg,off+cnt,&total);
			if(off[cnt]) cnt++;
		}
		else sNew=ParseCaptionOne(s,sNew,&s,sNewOrg,0,&total);
		
		sOld=s;	 b=1;
	}
	

	if(!b)
	{ 	CheckTagH(s);
		return 0;
	}
	S_CPY(sNew,sOld);

	CheckTagH(sNewOrg);	   
	if(cnt>4) //show toc only if not too few or too many
	{	
		sOld=sNew; 
		/*	sunflower
		sNew=scA(sNew,"<div style=\"float:right;margin: 6px 6px 6px 6px\">"
		"<img src=\"/wiki/home/wiki-indexed.png\"></div>");
		  */
		sNew=scA(sNew,
		"<p><table class=\"toc\" style=\"margin: 10px 16px 10px 6px; float:left;\">");		
		sNew=scA(sNew,
		"<tr><th colspan=\"4\"><div id=\"toctitle\">"
		"Contents</div></th></tr><tr cellpadding=\"4\"><td><ul>");
		for(i=0;i<cnt;i++)
		{	
			sNew=scA(sNew,"\r\n<li class=\"toclevel-1\"><a href=\"#");
			t=sNewOrg+off[i];
			S_CPYTO(sNew,t,'<');  
			sNew=scA(sNew,"<span class=\"tocnumber\">");
			ItoS(sNew,i+1); To_0(sNew);
			sNew=scA(sNew," </span><span class=\"toctext\">");
			
			t=sNewOrg+off[i];
			tp=sNew;
			S_CPYTO(sNew,t,C_QQ);  //if(bToc<(sNew-tp)) bToc=(sNew-tp);
			sNew=scA(sNew,"</span></a></li>"); 	  
		}

		if(total>cnt)
		{	sNew=scA(sNew,"\r\n<li class=\"toclevel-1\">");
			sNew=scA(sNew,"<span class=\"tocnumber\">");
			sNew=scA(sNew,"&nbsp;&nbsp;&nbsp;</span> <span class=\"toctext\">");
			sNew=scA(sNew,"...</span></a></li>"); 	  
		}

		sNew=scA(sNew,"</ul></td></tr></table>");	
		i=sNew-sOld;  cnt=sOld-sNewOrg;	 
		memcpy(sNew,sOld,i); //move toc to end	</div>
		memmove(sNewOrg+i,sNewOrg,cnt);	//move text to end
		memcpy(sNewOrg,sNew,i);	  //move toc to beginning
		sNew=sNewOrg+i+cnt; *sNew=0;
	}
		 
	if(bTest) 
		WWriteTest("ParseCaption",sNewOrg);
	Z4(sNew); 
	return 1; 
}




