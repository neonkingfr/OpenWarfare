#include "WF_Main.h"

#define MAX_TEMPLATE	1000
extern BOOL bTest;
extern int iDomain,iNS;


typedef struct
{	char *buf; 
	char name[100];
	BYTE indexLang;
	BYTE dm;
}
TEMP_T;

char szMonthL[12][20]=
{ "January",
 "February",
 "March",
 "April",
 "May",
 "June",
 "July",
 "August",
 "September",
 "October",
 "November",
 "December",
};
char szMonthS[12][8]=
{ "Jan",
 "Feb",
 "Mar",
 "Apr",
 "May",
 "Jun",
 "Jul",
 "Aug",
 "Sep",
 "Oct",
 "Nov",
 "Dec",
}; 	

extern char szTitle[];
extern int iDomain,iNS,iLang;
extern WL_T *glt; 

static TEMP_T *gtt;
static int cntTT;
int iMaxTemplate=300; //set in ini file

char *scLess1(char *sNew,char *s)
{	char *t=sNew;
	S_CPY(sNew,s); if(t<sNew) sNew--;  
	*sNew=0; return sNew;
}

char *TempGetFixedNS(char *fn,char *sNew)
{	int i; char *t;
	if(*fn=='-')
	{	if(fn[1]=='2')	sNew=scA(sNew,"Media");
		if(fn[1]=='1')	sNew=scA(sNew,"Special");
		return sNew; 
	}
	if(IS_DIG(*fn))
	{	sNew=scLess1(sNew,GetNSStr(WStrToInt(fn)));	}
	else
	{	for(i=1;i<MAX_SPACE_USE;i++)
		{	t=GetNSStr(i);
			if(!strnicmp(fn,t,strlen(t)-1)) 
			{	return scLess1(sNew,t); } 
		}
	}
	return sNew;
}

char *TempGetDateStart(char *dumpName)
{	char *s=dumpName,*start;
	
	if(!s||!*s)  return 0; 
	To_FN(dumpName,s);
	To_DIG(s); if(!*s) return 0;  start=s;
	To_NON_DIG(s); if((s-start)!=8) return 0; 
	return start;
}
void GetYear(char *oBuf)
{	SYSTEMTIME time;
	zm(&time,sizeof(time));
	GetLocalTime(&time); 	  
	wsprintf(oBuf,"%d",time.wYear);
}
char szDatStub[]="? (stub by <b><i>Wikifilter</i></b>)";

 //date time must be exactly 8-char: 4-char year, 2-char month, 2-char day
 //anything wrong returns default
 //i: 0(year), 1(month as number) 2(month as short) 3(month as long) 
 //4(date as number)

char *TempDate(char *dumpName,int i)
{	char *start; int k=0;
	static char myYear[8]; //must update for each call because differnt 
	static char myDate[4];	   //dumpfiles have different date time
	
	if(!(start=TempGetDateStart(dumpName)))
	{	if(i==0) {	 GetYear(myYear);  return myYear; }
		if(i==1) {	return "01";	}  //default month Jan
		if(i==2) {  return szMonthS[0]; }
		if(i==3) {  return szMonthL[0]; }
		return "01";  //default date
	}

	if(i==0)  //year
	{	CopyMemory(myYear,start,4); myYear[4]=0; 
		return myYear;
	}
	start+=4;
	if(i<4)	 //month
	{	myDate[0]=start[0]; myDate[1]=start[1]; myDate[2]=0;
		if(i==1)  {  return myDate; } // month as number

		k=WStrToInt(myDate); if(k<1||k>12) k=1; //wrong
		k--;
		if(i==2) {  return szMonthS[k]; }
		return szMonthL[k]; 
	}
			//date
	start+=2; 
	myDate[0]=start[0]; myDate[1]=start[1]; myDate[2]=0;
	return myDate;
}
//{{fullurl:{{FULLPAGENAME}}|action=edit}} 
// these stuff so useless and boring, no bother
char *TempGetLocalUrl(char *fn,char *out) 
{	char *s=out;

	s=scA(s,"/wiki/");
	To_C(fn,':');
	if(*fn==':') fn++;
	S_CPYN(s,fn,260);
	return out;
}
char *TempGetFullUrl(char *fn,char *out)
{	char *s=out;

	s=scA(s,"http://localhost/wiki/");
	To_C(fn,':');
	if(*fn==':') fn++;
	S_CPYN(s,fn,260);
	return out;
}

char *TempGetFixed(char *fn)//,int ns)
{	static char szSpace[400]; 
	char *dumpName=glt[iLang].dumpName[iDomain]; //will give default if it's NULL
	char c;

	P_SPACE(fn); c=*fn;
	if(c=='C')
	{	if(!strncmp(fn,"CURRENTYEAR",10)) return TempDate(dumpName,0);
											  //long month
		if(!strncmp(fn,"CURRENTMONTHNAME",16)||!strncmp(fn,"CURRENTMONTHNAMEGEN",18)) 
			return TempDate(dumpName,3);
												 //short month
		if(!strncmp(fn,"CURRENTMONTHABBREV",16)) return TempDate(dumpName,2);
											   //month number
		if(!strncmp(fn,"CURRENTMONTH",11)) return TempDate(dumpName,1);

		if(!strncmp(fn,"CURRENTDAYNAME",12)) 
		{	return szDatStub; } //default dayname

		if(!strncmp(fn,"CURRENTDOW",10)) 
		{	 return szDatStub; }	 //default day of week 

		if(!strncmp(fn,"CURRENTDAY",9))	return TempDate(dumpName,4);
		 
		if(!strncmp(fn,"CURRENTWEEK",10))  
		{  return szDatStub; }	//default

		if(!strncmp(fn,"CURRENTTIME",10))  
		{  return szDatStub; }
		return 0; 
	}
	if(c=='l'||c=='L')
	{	if(!strnicmp(fn,"localurl",7)) 
		{	return TempGetLocalUrl(fn,szSpace);
		}
		return 0; 
	}
	if(c=='f'||c=='F') //full and local can be treated as same
	{	if(!strnicmp(fn,"fullurl",7)) //
		{	return TempGetFullUrl(fn,szSpace);

		}
		
		if(!strnicmp(fn,"fullpagename",12))
		{	strcpy(szSpace,GetNSStrCur());
			strcat(szSpace,szTitle);
			return szSpace;
		}
		return 0; 
	}

	if(c=='R')
	{	if(!strncmp(fn,"REVISIONID",10))
		{	return "<i>See it by clicking \"Show wiki text\" --<b>WikiFilter</b></i>"; }
		return 0; 
	}

	if(c=='S'||c=='s')
	{	if(!strnicmp(fn,"sitename",8)) 
		{	strcpy(szSpace,GetSiteStrCur());
			szSpace[1]=0;	
			return szSpace;	
		}
		if(!strnicmp(fn,"server",6)) {  return "http://localhost"; } //root dir: 
		if(!strnicmp(fn,"subpagename",11)) return szTitle;
		return 0; 
	}
	if(c=='P')
	{	if(!strncmp(fn,"PAGENAME",8)) { return szTitle; }
		return 0; 
	}
	if(c=='N'||c=='n')
	{	if(!strnicmp(fn,"ns:",3)) 
		{	szSpace[0]=0;
			TempGetFixedNS(fn+3,szSpace);
			return szSpace; 
		}
		
		if(!strncmp(fn,"NAMESPACE",8)) 
		{	scLess1(szSpace,GetNSStrCur()); 
			return szSpace;
		}
		
		if(!strncmp(fn,"NUMBEROFARTICLES",12))  
		{	if(glt[iLang].cntTitle[iDomain]) 
			{	wsprintf(szSpace,"%d (including redirects. --<i><b>Wikifilter</b></i>)",
						glt[iLang].cntTitle[iDomain]);
				return szSpace;
			}
			else return szDatStub;;
			
		}
		return 0; 
	}
	return 0; 
}
void TempCacheInit()
{	if(iMaxTemplate<200) iMaxTemplate=200;
	else if(iMaxTemplate>MAX_TEMPLATE) iMaxTemplate=MAX_TEMPLATE;
	gtt=GmZ(iMaxTemplate+1,TEMP_T);
	cntTT=0;
}

int TempGetInfo(int *oCnt)
{	int i,k=0; 
	
	*oCnt=cntTT;

	for(i=0;i<cntTT;i++) k+=SizeMem(gtt[i].buf);
	return k;
}

//add temp to cache
BOOL TempAdd(char *fn,char *bufNew)
{	char *t;

	if(cntTT>=iMaxTemplate) { LogInt("Cache full:",cntTT); return 0; }
	if(!gtt) TempCacheInit();
	
	t=gtt[cntTT].name; S_LOWN(t,fn,98);
	
	gtt[cntTT].buf=bufNew;    
	gtt[cntTT].indexLang=(BYTE)iLang;
	gtt[cntTT].dm=iDomain;
	LogInt("/////////Template cached Index",cntTT);
	cntTT++; 
	return 1; 
}

char *TempGetOld(char *fn)
{	int i; 	
	char low[100],*t=low;

	if(!gtt||!cntTT) return 0; //no cache yet

	S_LOWN(t,fn,98);

	for(i=0;i<cntTT;i++) //be sure the temp is in this lang and site
	{	if(gtt[i].indexLang!=(BYTE)iLang||gtt[i].dm!=(BYTE)iDomain) continue;
		if(!strcmp(low,gtt[i].name)) 
		{	
			return gtt[i].buf;
		}
	}
	return 0; 
}

extern int HttpStripNS(char *temp); //temp like: :Category:abc 

extern void HttpFormatTitle(char *s);
extern char *WikiFindPage(char *title,int ns,BOOL bRaw,int *oBytes);

BOOL StrRidStr(char *buf,char *str)
{	char *s=buf,*sNew=0; int len=strlen(str),bRid=0;

	while(1)
	{	if(!(s=StrStrI(s,str))) break;
		sNew=s; s+=len;	
		memmove(sNew,s,strlen(s)+1); //ending NULL is never lost
		s=sNew;	 bRid++;
	}
	return bRid;
}


extern int WMarkDeepStr(char *buf,int *offS,int *offE,char *cS,char *cE,int max);

void TempRidStuff(char *buf) //more nuisance stuff will come soon!
{	char *sNew,*s;	 		 //and funnier they will get
	int k=0,i,bytes; 		 //I really cant see why they just not use comment <!-- -->
	int offS[100],offE[100];	

	sNew=s=buf;			// anything outside is junk
	if((k=WMarkDeepStr(buf,offS,offE,"<includeonly>","</includeonly>",100)))
	{	for(i=0;i<k;i++)	
		{	s=buf+offS[i]+13;
			bytes=offE[i]-offS[i]-13; if(bytes<0) bytes=0;
			memmove(sNew,s,bytes); 
			sNew+=bytes; 
		}
		Z4(sNew);
	}

	sNew=s=buf;		  //same as above, but note the word order
	if((k=WMarkDeepStr(buf,offS,offE,"<onlyinclude>","</onlyinclude>",100)))
	{	
		for(i=0;i<k;i++)	
		{	s=buf+offS[i]+13;
			bytes=offE[i]-offS[i]-13; if(bytes<0) bytes=0;
			memmove(sNew,s,bytes); 
			sNew+=bytes; 
		}	 
		Z4(sNew);
	}
		 
		//how about <includeonly> A <noinclude> B </noinclude> C </includeonly>?
		//well, no problem: we get "AC"
		//but  <noinclude> A <includeonly> B </includeonly>	C </noinclude>
		//we get nothing
	sNew=s=buf;						  //anything WITHIN is junk.
	if((k=WMarkDeepStr(buf,offS,offE,"<noinclude>","</noinclude>",100)))
	{	for(i=0;i<k;i++)	
		{	bytes=offS[i]-(s-buf);
			memmove(sNew,s,bytes); 
			sNew+=bytes; s=buf+offE[i]+12;
		}
		S_CPY(sNew,s); Z4(sNew);
	}
} 

void DeleteSubstrings(char *buf, char *substring)
{
  static int subIx[512];
  int count=0;
  char *subs, *next=buf;
  int len = strlen(substring);
  int bufLen = strlen(buf);
  while ( (subs = strstr(next, substring)) !=NULL )
  {
    subIx[count++] = subs-buf;
    next = subs+len;
  }
  if (count) next = buf+subIx[0];
  else next = buf+bufLen; //end of buf
  for (int i=0; i<count; i++)
  {
    char *chunk = buf + subIx[i] + len;
    int chunkLen = i<count-1 ? subIx[i+1]-subIx[i] : bufLen-subIx[i];
    memcpy(next, chunk, chunkLen);
    next += chunkLen;
  }
  *next = 0; //terminate
}

void TempRidStuffBistudioWiki(char *buf) //more nuisance stuff will come soon!
{	
  DeleteSubstrings(buf, "<includeonly>");
  DeleteSubstrings(buf, "</includeonly>");
  DeleteSubstrings(buf, "<onlyinclude>");
  DeleteSubstrings(buf, "</onlyinclude>");
  DeleteSubstrings(buf, "<noinclude>");
  DeleteSubstrings(buf, "</noinclude>");
}

char *WPassBetween(char *s,char cStart,char cEnd)
{	int k=0; char c;

	while((c=*s))
	{	if(c==cStart) { k++;  }
		else if(c==cEnd) {  k--;  }
		s++;
		if(!k) break;
	}
	return s;
}

char *PipeReplaceLinkOne(char *s)
{	int k=0; char c;

	while((c=*s))
	{	if(c=='[') { k++;  }
		else if(c==']') {  k--;  }
		else if(c=='|') *s=PIPE_TEMP;
		else if(c=='{') 
		{	if(s[1]=='{') {  s=WPassBetween(s,'{','}'); continue; } }
		s++;
		if(!k) break;
	}
	return s;	
}
void PipeReplaceLink(char *s)
{	while(*s)
	{	if(*s=='[')	 { s=PipeReplaceLinkOne(s); continue; }
		s++;
	}
}

void PipeReplace(char *s)
{	char c;

	while((c=*s))
	{	if(c=='{')
		{	if(s[1]=='{') {  s=WPassBetween(s,'{','}'); continue; }
			s++; continue;
		}
		
		if(c=='|')	 
		{ 	*s++=PIPE_TEMP; continue; }	
		s++;
	}
}

//insert a LB if first char of template is table or list stuff
void TempBlock(char *s)	 
{	int k=0; char *sOrg=s;
	
	while(*s==SPACE) s++;

	if(*s=='*'||*s==':'||*s=='#'||*s==';') k=1; //list
	
	if(!k)
	{	if((*s=='|'||*s=='!')&&s[1]!=s[0]) k=1; } //table, ignore || or !!
	
	if(!k)
	{	if(*s=='{'&&s[1]=='|') k=1; } //table start
	 
	if(!k)
	{	if(*s=='{'&&s[1]=='{'&&s[2]!='{') k=1; } //table start
	   
	if(k)  //insert LB
	{	k=strlen(s)+1; memmove(s+1,s,k); *s=LB;
		s+=k; *s++=0; *s=0;
	}
}

//template name *tn can be modified
// when getting temp as article, find the temp page and return it

extern void FormatTitle(char *s);
extern int StripNS(char *s);
extern char *FindPage(char *title,int *ns,BOOL bInfo,BOOL bPreParse);
extern char *CheckTag(char *s,char *sT,char *eT,BOOL bRid);

  //do NOT modify or free returned *buf, unless *bFree is set to 1
// important note: *tn is assumed to be a large buf
// so we use it to store a temporary temp name   
char *GetTemplateInter(char *tn,char *tnBack,BOOL *bFree)
{	char *buf,*s;//[300];
	int ns;	

	*bFree=0; //bFreeTemplate=0; 
	
	Log2Eq("\r\n/////////// Got template name",tn);

	if(!*tn) return 0; //template parse error
	
	if(!strnicmp(tn,"gallery",7)) return 0; //this is not working

	if(*tn=='/') { ns=iNS; tn++; }
	else
	{	ns=StripNS(tn); //temp like: Category:abc 
		if(ns<=0) ns=10; //default: template
	}
	FormatTitle(tn);
	
	strcpy(tnBack,tn); 
	
	if((s=TempGetOld(tn))) return s; // Is it in cache?
		 
						//this func will change *tn if redirect
	buf=FindPage(tn,&ns,0,1); //find it, only within current domain	
	if(!buf) 
	{	Log2Eq("!!! No template for",tn); return 0;  }
									 // tn
	TempRidStuff(buf); //not in template nameSpace, rid some tags
	TempBlock(buf);
	PipeReplaceLink(buf);	//replace all pipes in links
	PipeReplace(buf);  //replace pipes that are not within braces
	
	buf=ReGmC(strlen(buf)+8,buf);
	//Log4("###",tn,buf,"####"); //log this new template  
	
	if(!TempAdd(tnBack,buf)) *bFree=1;	
	return buf;
}

void WikiTemplateFree()
{	int i;
	if(!gtt) return;
	for(i=0;i<cntTT;i++) Fm(gtt[i].buf); 
	Fm(gtt); 
	cntTT=0;
}



		







