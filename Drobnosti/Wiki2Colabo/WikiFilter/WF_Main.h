#ifndef WF_MAIN_H
#define WF_MAIN_H

#define CONVERT_TO_MARKDOWN 1

#include "../wikiFilterExt.h"
#include "MyMacNew.h"

#include <windows.h>
#include <httpfilt.h>
#include <shellapi.h>
#include <shlwapi.h>
//#include "resource.h"

#define MAX_LANG	20  //max languages we can "host"
#define MAX_DOMAIN	9	//max projects
#define MAX_SITE	9	//max projects

#define MAX_SPACE 40	//max name spaces, max ns in index file
						//used only when loading index file
						//because index file W_POS_T struct is this large
#define MAX_SPACE_USE	20 //currentlt 20 is enough, use this
						


#define MAX_NS_LEN	80 //max len of ns str
#define MAX_OFF	1000



#define ISDIR	2
#define ISFILE	1

#define W_NO	1  //<nowiki>
#define W_PRE	2  //<pre>
#define W_MATH	3
#define W_CODE	4  //<code>
#define W_BLOCK	4  //<blockquote>

#define PIPE_TEMP	6  //used to hide | in templates

#define J_C2(s,c,c2) do{To_C2(s,c,c2); P_C2(s,c,c2);  }while(0)

char *strcpyN(char *d,char *s,int cnt);
char *strcpyTo(char *d,char *s,char c);

typedef struct
{	char *buf;
	int *off,*index; //to access as unsorted, do not use index
	int cnt,maxLen;
}STR_T;


typedef struct
{	int offTitle; //start pos in main .str file of this 1st word group
	DWORD midPoscnt; //cnt of titles in .str file for this group
	int posWord1;  //start pos of this 1st word group in all groups
}W_POS_T;

typedef struct
{	int art;     //article info block
	int cat;	 //category list block
	int ns[MAX_SPACE];	 //title info for each nameSpace
	int end;   //ending tag == 0x12345678, a genuine index file!!
}FPOS_T; // start position of blocks in index file

typedef struct
{	
	char name[8]; //language name, like en, fr, zh, etc.
	
	///////////////// these names are all pointers, must allocate or free
	char *dumpName[MAX_DOMAIN]; //each language has at most 5 domains: 
								//wikipedia, wiktionary, wikiquote, ...
	char *web[MAX_DOMAIN];	//a cpy of base name: 
	//<base>http://commons.wikimedia.org/wiki/Main_Page</base>

								   //others are normally in local language
	char *titleOld[MAX_DOMAIN];	//last visited page, so we return to it
								//when we jump over languages or domains
	
	//////////////////// these are for index words							
	char *szWord1[MAX_DOMAIN][MAX_SPACE_USE]; //index word buffer, for English, 2MB
	W_POS_T *ptG[MAX_DOMAIN][MAX_SPACE_USE];  //info about index word
	int maxWord1[MAX_DOMAIN][MAX_SPACE_USE];  //cnt of index words
	
	HANDLE fpDump[MAX_DOMAIN]; //dump file handle
	HANDLE fpIndex[MAX_DOMAIN];	//index file handle
	

	int cntTitle[MAX_DOMAIN]; // for wiki variable number of articles only
	int c1stWord[MAX_DOMAIN]; //how many bytes of index word, 0 means whole word
							  //like English. fixed len is for Eastern languages
	STR_T nsStr[MAX_DOMAIN];
	BOOL indexNew[MAX_DOMAIN];
	
	BOOL bEn; //english?, if so, saves some work
	FPOS_T fPos[MAX_DOMAIN]; //various blocks' pos in index file 

}WL_T; //info for each language 			

typedef struct
{	int bytesArticle; //article len in bytes 
	LONGLONG posArticle; //article position in dump file
}W_ART_T; //article info

typedef struct
{	char keyOrg[300]; //user input key
	int ns;	  //nameSpace index
	int max;  //max match show index
	BOOL bShowBestMatch; //show best match article along with search result checkBox
}SBOX_T; //search info type

typedef struct
{	char *site,*lang,*ns;
	char *bare;	//page name start pos stripped of all prefixes
	int xsite,xlang,xns; //index number

}FULL_LINK_T;

		 //for the odd case start==s, ignore
#define W_RE(start,s) \
do{ \
	if(s>start) \
	{	while(s>start&&!*s) s--; P_SPACEB(start,s); *(++s)=0;}\
  }\
  while(0)

#define P_SPACE1(s,c)  while(*s==SPACE||*s==LB||*s==c) s++
#define P_SPACE2(s,c1,c2)  while(*s==SPACE||*s==LB||*s==c1||*s==c2) s++
#define P_SPACE3(s,c1,c2,c3)  \
	while(*s==SPACE1||*s==LB||*s==c1||*s==c2||*s==c3) s++

#define S_CPYPOSN(dest,src,pos,cnt) \
    do{ int cNNTT=cnt; while(src<pos&&*src&&cNNTT--) *dest++=*src++; *dest=0; }while(0)

#define RID_END(s) while(*s==SPACE) *s--=0

char *WPassBetween(char *s,char cStart,char cEnd);

char *scA(char *dest,char *src); //returns NULL end of *dest
char *scLess1(char *sNew,char *s);

char *Gcpy7(char *s,char *s1,char *s2,char *s3,char *s4,char *s5,char *s6,char *s7);
char *Gcpy4(char *s,char *s1,char *s2,char *s3,char *s4) ;
char *Gcpy3(char *s,char *s1,char *s2,char *s3);
char *Gcpy2(char *s,char *s1,char *s2) ;
char *Gcpy5(char *s,char *s1,char *s2,char *s3,char *s4,char *s5) ;
char *Gcpy6(char *s,char *s1,char *s2,char *s3,char *s4,char *s5,char *s6) ;

void Log3(char *s1,char *s2,char *s3);
void Log2(char *s1,char *s2);
void Log(char *s1);
void Log4(char *s1,char *s2,char *s3,char *s4);
void LogInt(char *label,int i);
void Log2Eq(char *s1,char *s2);
void LogN(char *s,int cnt);

char *ItoS(char *buf,int value);

void BufShift(char **b1,char **b2);


char *PrefixLinkCur(char *link,char *out);
char *PrefixLinkCurNoNs(char *link,char *out);
char *PrefixLink(char *link,char *out,int lang,int site,int ns);
int HttpStripNS(char *temp);
char *GetNSStr(int ns);
char *GetNSStrCur();
char *GetSiteStrCur();
char *GetLangStrCur();

void WWriteTest(char *fn,char *buf);
int RidSpace(char *in);

int GetFT(TCHAR *fname);
BOOL LibSeek(HANDLE fp,int pos);
BOOL LibSeekEx(HANDLE fp,LONGLONG pos);
 
HANDLE fopenTemp(char *name);  //if name has path, use it, if not, add temp path
HANDLE fopenWin(TCHAR *name, TCHAR *how);  

char *LibReadFile(TCHAR *fname,int *oSize);
int freadWin(void *buffer,int cBytes,HANDLE hfile); //returns bytes read or -1 on error
int LibReadFileBuffer(TCHAR *fname,void *yourBuf,int size4Less);
char *LibReadFileBytes(TCHAR *fname,int start,int *size);

int fwriteWin(void *buffer,int cBytes,HANDLE hfile); //returns bytes written or -1 on error
BOOL LibWriteFile(TCHAR *fname,void *buffer,int size);
int LibWriteFileOn(char *fname,void *buffer,int size); //append
char *LibReadFileExtra(TCHAR *fname,int *oSize,int extraBytes);

//generate a random number <= max. if max<=0, any int
DWORD WRand(int max);

int WStrToInt(char *buf);	
char *WPassNoWiki(char *s);

char *WReadMyFile(char *fnNoPath,int *oBytes);
HANDLE WOpenMyFile(char *fnNoPath);
char *WAddPath(char *fnNoPath,char *oFull);
char *GetMainPage(char *web);



//returns bare pageName
//pass *oLang, *oSite, *oNs as NULL if no need
char *GetLinkInfo(char *link,int *oLang,int *oSite,int *oNs);
int IsEn();

int StrSearch(STR_T *str,char *key)	;
void StrFree(STR_T *str);
void StrReGetMem(STR_T *str,int bytes,int max) ;
void StrGetMem(STR_T *str,int bytes,int max);
int StrGetSize(STR_T *str); // a\0b\0 ==> returns 4
void StrGetMaxLen(STR_T *str);
void StrSort(STR_T *str);
void StrLowUtf(STR_T *str);
int StringCnt(char *s);
int StrInit(STR_T *str,char *buf,int bSort);

//case-insensitive
//rid chars between *str1 and *str2. also rid *str1 and *str2 themselves
//note: cannot rid nested strings
//returns ending NULL of *buf 
char *StrRidBetweenI(char *buf,char *str1,char *str2);
//case-sensitive, faster for ridding comments "<!--" "-->"
char *StrRidBetween(char *buf,char *str1,char *str2);


int WMarkDeep3(char *buf,int *offS,int *offE,char cS,char cE,int max);
int WMarkDeep2Risky(char *buf,int *offS,int *offE,char cS,char cE,int max) ;
int WMarkDeepStr(char *buf,int *offS,int *offE,char *cS,char *cE,int max);
int WMarkDeep(char *buf,int *offS,int *offE,char cS,char cE,int max);
int WMarkDeep2(char *buf,int *offS,int *offE,char cS,char cE,int max);
int WMarkDeepStr2(char *buf,int *offS,int *offE,char *cS,char *cE,int max,int bLB);



#endif