#include "WF_Main.h"

#define MAX_RID	5
#define MAX_SKEY	20

typedef struct
{	char word[MAX_SKEY][100];
	int bBad[MAX_SKEY];
	char key[KB];
	char low[300];
	int cWord;
	char showTitle[300];
	char titleWord[MAX_SKEY][100]; //words of the title to be matched
	int matched[MAX_SKEY]; //this title word has been matched
}W_SEARCH_T;

char szRid[MAX_RID][8]={"of","in","the","to","a"};

typedef struct
{	int offTitle;
	int hit;
}WS_FOUND_T;

extern int iNS;

inline char *scAin(char *dest,char *src) //returns NULL end of *dest
{
	while(*src) *dest++=*src++;
	*dest=0;
	return dest; 
}


BOOL WIsBadKey(char *key)
{	int i;
	for(i=0;i<MAX_RID;i++) { if(!stricmp(key,szRid[i]))	return 1; }
	return 0; 
}

					  //sort most hit to least hit
int WSortValue(const void *v1,const void *v2)
{	return (((WS_FOUND_T *)v2)->hit-((WS_FOUND_T *)v1)->hit);	}

extern char *UtfLow(char *in,char *out); //*in *out can be same buf

char *WMakeHrefInter(char *sHtm,char *title)
{	
	sHtm=scA(sHtm,"<a href=\"/wiki/");
	sHtm=PrefixLinkCur(title,sHtm);
	return Gcpy3(sHtm,"\">",title,"</a>");
}


extern char *WikiGetPageEx(char *title,int *oBytes,BOOL bRaw,
					char *extra,int extraBytes,BOOL bFind);

extern char szTitle[]; //global unique current title name, simplify function calls

extern BOOL bSaveTitle;

int WGetKeyWords(char *s,W_SEARCH_T *st)
{	int i; char *t;

	for(i=0;i<MAX_SKEY;i++)
	{	P_SPACE1(s,'+');
		if(!*s) break;

		t=st->word[i]; S_CPYTON(t,s,'+',98);
		st->bBad[i]=WIsBadKey(st->word[i]);
	}	
	st->cWord=i;
	return i;
}


int WGetKeyWordsTitle(char *str,W_SEARCH_T *st)
{	int k=0,i; char *t;	
	unsigned char *s=(unsigned char *)str,c;

	for(i=0;i<MAX_SKEY;i++)
	{	while((c=*s)&&c>=SPACE&&c<128&&!IS_A(c)&&!IS_DIG(c)) s++;
		if(!*s) break;

		t=st->titleWord[k]; S_CPYTON(t,s,SPACE,98);

		if(t==st->titleWord[k]) break;	//no more to cpy

		t--; while((c=*t)&&c>=SPACE&&c<128&&!IS_A(c)&&!IS_DIG(c)) t--; *(++t)=0;
		if(!WIsBadKey(st->titleWord[k])) k++;
	}	
	return k;
}

int WGetGoodWord(W_SEARCH_T *st,int start) //get first good word
{	int i;

	for(i=start;i<st->cWord;i++)
	{	if(!st->bBad[i]) return i; }	
	return -1;
}
									 // ,int *indexNext
int WFormKey(W_SEARCH_T *st,int *index) //returns 0 if no more
{	int i,start; char *sNew;

	sNew=st->key;

	if(!*index) //first search, use whole sentence
	{	for(i=0;i<st->cWord;i++)
		{	if(i) *sNew++=SPACE; //separate with space
			sNew=scA(sNew,st->word[i]);
		}			
		//**index=index+1;
		return 1;
	}

	if(*index>=st->cWord) return 0; //no more

	start=WGetGoodWord(st,*index); //find a good word as start
	if(start<0) return 0; //no more good

	sNew=scA(sNew,st->word[start]);
	
	for(i=0;i<st->cWord;i++)
	{	if(i==start||st->bBad[i]) continue; // ignore
		*sNew++=SPACE; 
		sNew=scA(sNew,st->word[i]);
	}
	*index=start;
	return 1;
}


inline int WMatchHit(char *keyLow,char *strLow,int keyLen,int strLen,int max,int scale)
{	
	if(keyLow[0]!=strLow[0]) return 0; //no point if first char differs
 
	if(keyLen==strLen)
	{	if(!strcmp(keyLow,strLow)) return max; }
	
	else if(keyLen<strLen)
	{	if(!strncmp(keyLow,strLow,keyLen)) //all key sentence letters match
		{	return ((keyLen*scale)/strLen);  }
	}
	else //all title letters match
	{	if((keyLen-strLen)<4&&!strncmp(keyLow,strLow,strLen))
		{	return ((strLen*scale)/(keyLen*2)); }
	}  
	return 0;
}


int WMatchTitleOneWord(W_SEARCH_T *st,char *keyWord,int keyLen,int cntTitleWords,int round)
{	int i,hit=0; 
	
	for(i=0;i<cntTitleWords;i++)
	{	if(st->matched[i]) continue;
		if(!round)
			hit=WMatchHit(keyWord,st->titleWord[i],keyLen,strlen(st->titleWord[i]),20,8);
		else
			hit=WMatchHit(keyWord,st->titleWord[i],keyLen,strlen(st->titleWord[i]),10,4);
		if(hit) {  st->matched[i]=1; break; }
	}
	return hit;
}

int WMatchTitleOneWordVague(W_SEARCH_T *st,char *keyWord,int cntTitleWords,int value)
{	int i; 
	
	for(i=0;i<cntTitleWords;i++)
	{	if(st->matched[i]) continue;
		if(keyWord[0]==st->titleWord[i][0]&&strstr(keyWord,st->titleWord[i]))
		{	 
			st->matched[i]=1; 
			return value; 
		}
	}
	return 0;
}

int WMatchTitle(W_SEARCH_T *st,int keyLen,char *keyLow,char *strLow,int round)
{	int strLen,cnt,i,hit;

	strLen=strlen(strLow);
	
	if(!round)
	{	if((hit=WMatchHit(keyLow,strLow,keyLen,strLen,MB,KB))>100) 
			return hit;
	}
	else
	{	if((hit=WMatchHit(keyLow,strLow,keyLen,strLen,MB-100,KB-100))>100) 
			return hit;
	}
	
	if(!(cnt=WGetKeyWordsTitle(strLow,st))) { return 0;  }//not likely
	
	zm(st->matched,sizeof(st->matched));
	for(i=0;i<st->cWord;i++)
	{	if(st->bBad[i]) continue;
		hit+=WMatchTitleOneWord(st,st->word[i],keyLen,cnt,round);
	}

	if(hit>20) return hit; //two words exact match

	zm(st->matched,sizeof(st->matched));
	for(i=0;i<st->cWord;i++)
	{	if(st->bBad[i]) continue;
		if(!round) hit+=WMatchTitleOneWordVague(st,st->word[i],cnt,12);
		else hit+=WMatchTitleOneWordVague(st,st->word[i],cnt,8);
	}
	return hit;
}
extern BOOL IsEn();

extern char *FindTitles(char *title,int ns,BOOL bSearch,int *oCnt);
WS_FOUND_T *SearchOne(W_SEARCH_T *st,int index,int *cntFound,char **bufTitle) 
{	char *s,*t,*buf,*start; 
	int i,max,keyLen,iFound=0; 	  
	WS_FOUND_T *ft; 					  
	int bEn=IsEn();
	

	*cntFound=0; //st->iShow=0; 	
	*bufTitle=0;
	
	if(!(buf=FindTitles(st->key,iNS,1,&max))) return 0; 
	
	if(bEn) strlwr(st->key);
	else UtfLow(st->key,st->key);

	keyLen=strlen(st->key);

	ft=GmZ(max,WS_FOUND_T);	//create record for each title	 
	
	s=buf; 
	for(i=0;i<max;i++)
	{	s+=8;	start=s; //pass first 2 ints
		
		t=st->low;
		
		if(bEn) {  S_LOWN(t,s,298); }
		else 
		{ 	S_CPYN(t,s,298); 
			UtfLow(st->low,st->low);
			
		}
		if(*s) To_0(s);
		s+=(sizeof(int)+1);	//pass last int
		ft[i].hit=WMatchTitle(st,keyLen,st->key,st->low,index);
		ft[i].offTitle=start-buf;
		if(ft[i].hit) iFound++; 
	}
	if(!iFound) { Fm(buf); Fm(ft); return 0; } //not possible, at least first word matched
	
	qsort(ft,max,sizeof(WS_FOUND_T),WSortValue); //sort from most hit to no hit
	
	*cntFound=iFound;  
	*bufTitle=buf;
	return ft;
}

char *SearchResult(WS_FOUND_T *ft,char *s,char *bufTitle,char *keyWord,int cntFound,int max)
{	int i,k;
	
	if(cntFound<max) k=cntFound; 
	else k=max;
							
	s=Gcpy3(s,"\r\n<p><table><tr bgcolor=\"#CCCCFF\"><th>Search result for \"",
			keyWord," ...\"");
	wsprintf(s," (found: %d, showing: %d)</th></tr>\r\n",cntFound,k);  To_0(s);
	if(!cntFound) { s=scAin(s,"</th></tr></table>"); return s; }

	s=scAin(s,"\r\n<tr style=\"font-size: 90%;\"><td>");
	for(i=0;i<k;i++)
	{	if(i)  s=scAin(s,"&nbsp; | &nbsp;");
		s=WMakeHrefInter(s,bufTitle+ft[i].offTitle); 
	}
	s=scAin(s,"\r\n</td></tr></table>");
	return s;
}

//*key is "word1+word2+...", maxShow is max results to show	 
char *SearchPage(char *key,int maxShow,int *oBytes)
{	W_SEARCH_T *st;	 WS_FOUND_T *ft;
	char *buf,*s,*bufTitle;
	int i=0,cntFound,total=0,hitMost=0;

	if(oBytes) *oBytes=0;
	st=GmZ(1,W_SEARCH_T); 
	if(!WGetKeyWords(key,st))  { Fm(st); return 0; }
	
	buf=GmCx(maxShow*400*st->cWord+KB); s=buf;
		
	while(WFormKey(st,&i))
	{	ft=SearchOne(st,i,&cntFound,&bufTitle);	 
		s=SearchResult(ft,s,bufTitle,st->word[i],cntFound,maxShow);
		if(ft)
		{	total+=cntFound;
			if(hitMost<ft[0].hit)
			{	hitMost=ft[0].hit;	  //note: *ft was sorted, so ft[0].offTitle
									 // may not be at buf beginning
				strcpyN(st->showTitle,bufTitle+ft[0].offTitle,260);
			}
			Fm(ft);	Fm(bufTitle);
		}
		i++;
	}
	
	if(oBytes) *oBytes=s-buf;
	
	Z4(s);	 
	buf=ReGmC(s-buf,buf);
	strcpy(key,st->showTitle); 
	Fm(st);
	return buf;
}


