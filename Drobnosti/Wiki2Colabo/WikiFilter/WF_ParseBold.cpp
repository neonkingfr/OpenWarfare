//bold italics

#include "WF_Main.h"			 
extern int bTest;

typedef struct
{	int kAll[100]; //in fact, well-formatted tags can only have 2
	int cur;	  //reason: same tag cannot be nested: <b><i><b>...</b></i></b>
	int old;
}W_BI_T; //bold italic	// here, <b> nests <i> and <b>, which is wrong

char szBI1[7][12]={"","","<i>","<b>","","<i><b>","<i><b>"};  	
char szBI2[7][12]={"","","</i>","</b>","","</i></b>","</i></b>"};


inline char *scAin(char *dest,char *src) //returns NULL end of *dest
{
	while(*src) *dest++=*src++;
	*dest=0;
	return dest; 
}

//this function is not very safe: only backward checking,
//better do forward checking too, ie, if there are only 2
//quotes, then no need worry about mismatch
char *BoldConvert(char *s,char *sNew,char **pS,W_BI_T *ft)
{	int k=0,max=5; char c; 

	c=*(s-1);

	while(*s==C_Q&&max--) {	s++; k++; }	// handles max 5 quotes
	*pS=s;								//more consecutive ones will 
										//cause next call
	if(k==4) { *sNew++=C_Q; k--; }
	
	if(ft->kAll[ft->cur]) //tag open	 
	{						 
		if(k==ft->kAll[ft->cur])	//exact matching previous
		{	sNew=scAin(sNew,szBI2[ft->kAll[ft->cur]]); 
			ft->kAll[ft->cur]=0;
			if(ft->cur>0) ft->cur--;
			return sNew;
		}

		////////////// the following all for no match only
		//////////// like first 2 second 3, first 3 second 5
		
						//3 quotes with s like: ''abc'''s 
		if(ft->kAll[ft->cur]==(k-1)&&*s=='s'&&(IS_A(c)||IS_DIG(c))) 
		{	sNew=scAin(sNew,szBI2[ft->kAll[ft->cur]]); 
			*sNew++=C_Q;
			ft->kAll[ft->cur]=0;
			if(ft->cur>0) ft->cur--;
			return sNew;
		}
		 
		 //5 quotes,
		 //sum-up of previous 2: ''abc''' mn''''' == (''abc ('''mn''') '')
		if(ft->cur>0&&k==(ft->kAll[ft->cur]+ft->kAll[ft->cur-1]))
		{	sNew=scAin(sNew,szBI2[k]); 
			ft->kAll[ft->cur]=ft->kAll[ft->cur-1]=0;
			ft->cur--;
			return sNew;
		}
		 //5 quotes,
		 //not sum-up, must split: ''abc''''' mn''' == (''abc'')('''mn''')
		if(k==5)
		{	sNew=scAin(sNew,szBI2[ft->kAll[ft->cur]]); //close prev
			ft->kAll[ft->cur]=k-ft->kAll[ft->cur];	//replace with splited New part
			sNew=scAin(sNew,szBI1[ft->kAll[ft->cur]]); //open new
										  //simply replace, so no need to move index
			return sNew;
		}

		//2 or 3 quotes, partly close previous 5 quotes
		// like '''''abc'' def''' == '''(''abc'') def'''
		if(ft->kAll[ft->cur]==5)
		{	sNew=scAin(sNew,szBI2[k]);
			ft->kAll[ft->cur]-=k;
			return sNew;
		}

			   //not matching, add to stack
			   //like ''abc'''def'''''
		sNew=scAin(sNew,szBI1[k]);
		ft->cur++; ft->kAll[ft->cur]=k;
		return sNew;
	}
	else //new
	{	
		sNew=scAin(sNew,szBI1[k]);
		ft->kAll[ft->cur]=k;	
	}
	return sNew;
}

char *CloseBi(char *sNew,W_BI_T *ft) //close all at LB	if missing
{	int i;

	for(i=ft->cur;i>=0;i--)
	{	if(ft->kAll[i]) sNew=scAin(sNew,szBI2[ft->kAll[i]]); }
	return sNew;	
}

char *ParseBoldOne(char *s,char *sNew,char **pS)
{	W_BI_T ft; 

	zm(&ft,sizeof(ft));
	while(*s)
	{	if(*s==C_Q&&s[1]==C_Q)
		{	sNew=BoldConvert(s,sNew,&s,&ft);
			continue;
		}
		if(*s==LB) break;
		*sNew++=*s++;	
	}
	*pS=s;
	return CloseBi(sNew,&ft); 
}

int ParseBold(char *s,char *sNew)
{	char *sOld=s,*sNewOrg=sNew;	
	int b=0;

	while(*s)
	{	if(*s==C_Q&&s[1]==C_Q)
		{	memcpy(sNew,sOld,s-sOld); sNew+=(s-sOld);
			sNew=ParseBoldOne(s,sNew,&s);
			sOld=s;	 b=1;
			continue;
		}
		s++;
	}
	if(!b) return 0; 

	S_CPY(sNew,sOld); 
	Z4(sNew); 
	if(bTest) 
		WWriteTest("ParseBold",sNewOrg);
	return 1; 
}




