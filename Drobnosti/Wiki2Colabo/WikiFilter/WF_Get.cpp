#include "WF_Main.h"

//a link between server request and data-base functions
//mainly process address
//and decide what to do with different requests

extern BOOL bTest;
char *szLocalDir,*szImgDir;
char szTitle[400];

int iLang,iDomain,iMaxLang,iNS;
WL_T *glt;

SBOX_T *sb;
BOOL bShowRaw=1,bShowImg=1,bShowTemplate=1; //in ini file
HANDLE fpLog;

extern void RidHex(char *in,char *out);
char *GetImg(int *oBytes);

char szImgHead[]="<img src=\"/wiki/Image:";
char szLinkHead[]="<a href=\"/wiki/Image:";

extern char *Parse(char *bufRaw,int *oBytes, CategoryList *categoryList);
extern char *FindPage(char *title,int *ns,BOOL bInfo,BOOL bPreParse);
extern char *FindCategory(int *oBytes); //show a list a articles in this category

extern int GetEnglishIndex() ;

char *WGetMainPage()
{	char *web,*t;
	
	web=glt[iLang].web[iDomain];
	if(!web) return 0;
	To_WN_EX(web,t);
	return t; 
}

int GetKeyOrg(char *key,char *keyOrg)
{	char *s=key; int cnt=0;
	
	*keyOrg=0;
	P_C2(s,SPACE,'+');
	if(*s=='&') return 0; 

	To_C(s,'&'); *s--=0; while(*s=='+') s--; *(++s)=0; //mark end
	s=key;
	while(1)
	{	P_C(s,'+'); if(!*s) break; //no more key
		S_CPYTO(keyOrg,s,'+'); *keyOrg++=SPACE;	cnt++;
	}
	*(--keyOrg)=0;
	return cnt;
}

extern char *FormGetHeader(int bRaw,int *oBytes);	
extern char *ParseAsPre(char *buf,int *oTextBytes);
extern char *ParseXMLCharBack(char *buf); //'<' and '>' only

char *GetPageText(BOOL bRaw,int *textBytes)
{	char *buf=0,*bufCat=0;  int catBytes,bPreParse;
	
	*textBytes=0;
	
	if(!strnicmp(szTitle,"testpage",8)) //read test file
	{	if(!(buf=WReadMyFile(szTitle,textBytes)))
		{	Log2Eq("Failed to read test page file",szTitle); return 0;	}
		buf=ParseXMLCharBack(buf); //'<' and '>' only
		return Parse(buf,textBytes,NULL);
	}
	
	if(bRaw||iNS==10) bPreParse=0;
	else bPreParse=1;

	if(!(buf=FindPage(szTitle,&iNS,bRaw,bPreParse))) return 0; 
	
	if(bRaw||iNS==10) return ParseAsPre(buf,textBytes);
	
	buf=Parse(buf,textBytes,NULL);	 
	if(iNS!=14||!(bufCat=FindCategory(&catBytes))) return buf;
					
	buf=ReGmC(*textBytes+catBytes+10,buf);
	memcpy(buf+(*textBytes),bufCat,catBytes);
	*textBytes+=catBytes;
	return buf; 
}
char szMissing[]=
	"Sorry, no page to show.<p>Please check if the "
	"data-base has been installed for this language and site.<br>"
	"This can also be caused by a broken or non-existent link in a wiki page.<p>";
				
char szMissingTest[]=
	"Sorry,the local test page file could not be found<br>";

char *GetPageFixed(int *oBytes,char *strFixed,BOOL bFree)
{	char *bufHead=0,*bufPage,*s;
	int bytesHead=0;
	
	szTitle[0]=0; //no "Show wiki text", no "Wiki Site"
	bufHead=FormGetHeader(0,&bytesHead);

	*oBytes=bytesHead+strlen(strFixed)+100;
	bufPage=GmC(*oBytes); 
	s=bufPage;
	
	if(bufHead)	 
	{	memcpy(s,bufHead,bytesHead); Fm(bufHead); s+=bytesHead;  }
	
	if(strFixed)
	{	s=scA(s,strFixed); 
		if(bFree) Fm(strFixed);
	}
	
	s=scA(s,"</div><p></body></html>");
	*oBytes=s+1-bufPage; //include a NULL
	Z4(s); 	   
	return bufPage; 						
}

char *GetPageNormal(BOOL bRaw,int *oBytes)
{	int bytesRand=0,textBytes=0,imgBytes=0,bytesHead=0;
	char *bufText=0,*bufPage=0,*bufHead=0,*s;

	if(!szTitle[0])	bytesRand=400;
	
	if(iNS==6&&!bRaw) imgBytes=2*KB;
	
	if(!(bufText=GetPageText(bRaw,&textBytes))) 
	{	if(!strnicmp(szTitle,"testpage",8))
			return GetPageFixed(oBytes,szMissingTest,0);
		else
			return GetPageFixed(oBytes,szMissing,0);
	}
	
	if(!strnicmp(szTitle,"testpage",8))	szTitle[0]=0; //clear this

	bufHead=FormGetHeader(bRaw,&bytesHead);
	
	*oBytes=bytesHead+imgBytes+bytesRand+textBytes+KB;
	
	bufPage=GmC(*oBytes); 
	s=bufPage;

	if(bufHead)	 //no header???
	{	memcpy(s,bufHead,bytesHead); Fm(bufHead); s+=bytesHead;  }

	if(szTitle[0]) 
		s=Gcpy3(s,"<h1 align=\"center\">",szTitle,"</h1><p>\n");

	if(bytesRand)
	{	s=scA(s,"<div><i>This random page came from <a href=\"/wiki/");
		s=PrefixLinkCur(szTitle,s); 
		s=scA(s,"\">here</a>.</i></div><p>");
	}
	
	if(imgBytes) s=Gcpy6(s,szLinkHead,szTitle,"\">",szImgHead,szTitle,"\"></a>");
	
	if(bufText)
	{	memcpy(s,bufText,textBytes); Fm(bufText); s+=textBytes; }

	s=scA(s,"</div><a name=\"ArticleEnd\"></a><p></body></html>");
	*oBytes=s+1-bufPage; //include a NULL
	Z4(s); 	   
	return bufPage; 						
}

extern char *SearchPage(char *key,int maxShow,int *oBytes);

char *GetPageSearch(int *oBytes,BOOL *bSaveTitle)
{  	int bytesS=0,bytesHead=0,textBytes=0; 
	char *bufS=0,*bufPage=0,*bufText=0,*bufHead,*s;

	*bSaveTitle=0;

	bufS=SearchPage(szTitle,(sb->max+1)*30,&bytesS);
	
	if(szTitle[0]&&sb->bShowBestMatch) bufText=GetPageText(0,&textBytes);
	
	bufHead=FormGetHeader(0,&bytesHead);

	*oBytes=bytesHead+textBytes+bytesS+KB;
	bufPage=GmC(*oBytes); 
	s=bufPage;


	
	if(bufHead)	 
	{	memcpy(s,bufHead,bytesHead); Fm(bufHead); s+=bytesHead;  }
	
	if(bufS)
	{	memcpy(s,bufS,bytesS); Fm(bufS); s+=bytesS; }
	
	if(szTitle[0]) 
		s=Gcpy3(s,"<h1 align=\"center\">",szTitle,"</h1><p>\n");

	if(bufText)
	{	*bSaveTitle=1;
		memcpy(s,bufText,textBytes); Fm(bufText); s+=textBytes; 
	}
	
	s=scA(s,"</div><a name=\"ArticleEnd\"></a><p></body></html>");
	*oBytes=s+1-bufPage; //include a NULL
	Z4(s); 	   
	return bufPage; 						
}

extern char *HtmShowCommand(int *oBytes);
extern char *HtmDoCommand(char *comStr,int *oBytes);

char *GetSearch(char *qestion,int *oBytes,int *bSaveTitle)
{	char *s;  
	
	*bSaveTitle=0; //do not save search except for random page
	
	Log(qestion);  
	if(!sb) sb=GmZ(1,SBOX_T);

	if(!(s=StrStrIA(qestion,"In="))) return 0; //get ns	RAND_MAX
	J_C(s,C_EQ);
	sb->ns=WStrToInt(s);							
	if(sb->ns<0||sb->ns>=glt[iLang].nsStr[iDomain].cnt) sb->ns=0; 
	iNS=sb->ns;

	if(!(s=StrStrIA(qestion,"Max="))) return 0; //max search results to show
	J_C(s,C_EQ);
	sb->max=WStrToInt(s);
	
	if((s=StrStrIA(qestion,"Best=on"))) sb->bShowBestMatch=1;	//show best match
	else sb->bShowBestMatch=0;

	if(!(s=StrStrIA(qestion,"TheKey="))) return 0;	//search key
	J_C(s,C_EQ);
	
	if(!GetKeyOrg(s,sb->keyOrg)) //no key, random page
	{	szTitle[0]=0; 
		*bSaveTitle=1; //save random page	
		return GetPageNormal(0,oBytes);
	}

	if(*s=='<') //a command,like "TheKey=<abc...
	{	P_C(s,'<');	
		if(!*s||*s=='?') return HtmShowCommand(oBytes);
		
		if(!strnicmp(s,"testpage",8)) 
		{	scA(szTitle,s);
			s=szTitle; To_0(s); BACK_CN(szTitle,s,DOT,6);
			if(*s!=DOT) strcat(szTitle,".txt");	 //if no file extension, assume .txt
			return GetPageNormal(0,oBytes);
		}
		return HtmDoCommand(s,oBytes);
	}
	scA(szTitle,s);
	return GetPageSearch(oBytes,bSaveTitle);
}

// get a normal wiki page, set global iLang, iDomain, iNS
char *GetNormal(BOOL bRaw,int *oBytes) 
{	char *t;

	t=GetLinkInfo(szTitle,&iLang,&iDomain,&iNS);
	if(t!=szTitle) scA(szTitle,t);
							 //like this: "w:en:"
	if(!szTitle[0]||(szTitle[0]==':'&&!szTitle[1])) 
	{	t=WGetMainPage();
		if(t) strcpy(szTitle,t);
		else return GetPageFixed(oBytes,szMissing,0); 
	}
	return GetPageNormal(bRaw,oBytes);
}

 //when setting site, to reduce "sorry no page to show", 
 //auto fall back to english if fail
char *SetDm(int dm,int *oBytes)
{	WL_T *lt; 
	
	lt=glt+iLang;
	if(!lt->titleOld[dm]) 
	{	lt=glt+GetEnglishIndex(); //last resort is always english!	
		if(!lt->titleOld[dm])  
			return GetPageFixed(oBytes,szMissing,0); //no english either, fail
	}
	strcpy(szTitle,lt->titleOld[dm]);
	return GetNormal(0,oBytes);
}  
//when setting lang, if current site has no such lang,
//fall back to first site (wikipedia)
char *SetLang(int lang,int *oBytes)
{	WL_T *lt; int dm=iDomain;

	lt=glt+lang;
	if(!lt->titleOld[dm]) //fall back to site 0
	{	dm=0;
		if(!lt->titleOld[dm])  //none either, fail
			return GetPageFixed(oBytes,szMissing,0); 
	}
	strcpy(szTitle,lt->titleOld[dm]); 
	return GetNormal(0,oBytes);
}
	 
extern BOOL IsLinkImg(char *s);
char *GetContentInter(int *oBytes,BOOL *bSaveTitle)
{	char *s,*t;
	
	*oBytes=0; 
	t=szTitle;

	To_CN(t,'?',12);  //has a '?'
	
	if(*t=='?')
	{	if(!strnicmp(szTitle,"SetSite?",8)) //site change, load previously visited page
		{	s=szTitle+8; 				   // of this site
			J_C(s,C_EQ);  
			return 	SetDm(WStrToInt(s),oBytes);
		}
	
		if(!strnicmp(szTitle,"SetLang?",8)) //lang change
		{	s=szTitle+8; 
			J_C(s,C_EQ);  
			return SetLang(WStrToInt(s),oBytes);
		}
		if(!strnicmp(szTitle,"SearchIt?",8))	//search
			return GetSearch(szTitle+9,oBytes,bSaveTitle);  
	}
	s=szTitle; To_0(s);

	BACK_C(szTitle,s,DOT); //has extension?
	
	if(*s!=DOT)	return GetNormal(0,oBytes); //a normal wiki page
	
	if(!stricmp(s,".igp")) //<a href="/wiki/w:en:Image:abc.png.img">
	{	*s=0;  
		*bSaveTitle=0; //don't save this
		return GetNormal(0,oBytes);
	}

	if(!stricmp(s,".raw"))	
	{	*s=0;  
		*bSaveTitle=0; //don't save this
		return GetNormal(1,oBytes); //raw text
	}				

	if(!stricmp(s,".ran")) //<a href="/wiki/randompage.ran">
	{	szTitle[0]=0; 
		return GetPageNormal(0,oBytes);
	}
	if(IsLinkImg(szTitle))
	{	t=szTitle; To_C(t,':'); P_SPACE1(t,':');
		scA(szTitle,t);
		*bSaveTitle=0;	
		return GetImg(oBytes);
	}
	return GetNormal(0,oBytes);
}

char *GetHome(int *oBytes)
{	return WReadMyFile(szTitle,oBytes);	  }

char *GetLocal(int *oBytes)
{	char fn[MP],*s;
	
	s=scA(fn,szLocalDir);
	scA(s,szTitle);
	return LibReadFile(fn,oBytes);
}
// *szTitle is "/wiki/w:en:Main page", after call, it is "w:en:Main page"
int HttpIsWiki()
{	char *s,c;
			   // szTitle is "/wiki/abc"
	s=szTitle+5; 
	if(!*s) // "/wiki", change to main page  
	{	//*s++=DW; 
		strcpy(szTitle,WGetMainPage()); 
		return 1;  
	}
	if(*s!=DW) return 0; // non-wiki, like "/wikiStuff"
	s++;
	if(!*s) 
	{	strcpy(szTitle,WGetMainPage());	 // "/wiki/", change to "Main Page"
		return 1; 
	}
	RidHex(s,szTitle);	 
	
	c=szTitle[0]; // "/wiki/headbg.jpg"	etc. cached style files
	if((c=='m'||c=='M')&&!stricmp(szTitle,"main.css")) return 6;
	if((c=='h'||c=='H')&&!stricmp(szTitle,"headbg.jpg")) return 4;
	if((c=='b'||c=='B')&&!stricmp(szTitle,"bullet.gif")) return 5;
		 
						  // "/wiki/local/abc": a local file, in the dir *szLocalDir
	if((c=='l'||c=='L')&&!strnicmp(szTitle,"local/",6))	 
	{	scA(szTitle,szTitle+6);
		return 2;
	}
					  //style files can also be put in "home": WikiFilter's dir
	if((c=='h'||c=='H')&&!strnicmp(szTitle,"home/",5))	 
	{	scA(szTitle,szTitle+5);
		c=szTitle[0];
		if((c=='m'||c=='M')&&!stricmp(szTitle,"main.css")) return 6;
		if((c=='h'||c=='H')&&!stricmp(szTitle,"headbg.jpg")) return 4;
		if((c=='b'||c=='B')&&!stricmp(szTitle,"bullet.gif")) return 5;
		return 3;
	}
	return 1;
}

extern char *FormGetStyle(int i,int *oBytes,BOOL *bFreeAfterUse);
//save current title so when jumping back to this domain or language, 
//we bring up the previously visited page
// saved title is full: "w:en:abc"
void SaveOld()  
{	if(szTitle[0]) PrefixLinkCur(szTitle,glt[iLang].titleOld[iDomain]); }	

// bFreeAfterUse is set to 0 only for style files
// page content will be freed
char *GetContent(int *oBytes,BOOL *bFreeAfterUse)
{	char *buf=0; int i;	BOOL bSaveT=1;

	Log2Eq("\r\n======= url address",szTitle);
	
	*bFreeAfterUse=1; 

	if(!(i=HttpIsWiki())) return 0; //not wiki page call

	if(i==2)
	{	if(szLocalDir)  
			return GetLocal(oBytes);
		return 0; 								 
	}
	if(i==3) return GetHome(oBytes);
	if(i>3) return FormGetStyle(i,oBytes,bFreeAfterUse);

	buf=GetContentInter(oBytes,&bSaveT);

	if(buf&&bSaveT) SaveOld(); //save this title name so we can come back
	return buf; 
}

extern char *SpaceToUnder(char *s);	//returns ending NULL


char *GetImg(int *oSize)
{	char fn[MP],*s,*buf=0,*tp,*name=szTitle; 
	int k=2;
	
	if(!szImgDir) return 0;    
	

	SpaceToUnder(name);	
	
	s=scA(fn,szImgDir);	
	scA(s,name);
	if((buf=LibReadFile(fn,oSize))) return buf;

	s=scA(fn,szImgDir);	

while(k--) // for files like "200px-abc.png", try it and also "abc.png"
{	if(!IS_A(*name)&&!IS_DIG(*name)) *s++='0'; //non-ansi goes to 0_img
	else *s++=*name;
	s=scA(s,"_Img\\");
	
	if(IS_A(name[1])) *s++=name[1];
	else *s++=C_0; //non-ansi and digits go to 0_img
	*s++=DF;
	scA(s,name); 

	buf=LibReadFile(fn,oSize); //read img file from disk
	if(buf) break;
	
	  //Chess_kdd44.png ==> Chess_kdd40.png 
	if(!strnicmp(name,"chess_",6)&&(tp=strchr(name,'4'))&&IS_DIG(tp[1])&&tp[2]==DOT)
	{	tp[1]='0';		
		tp=strrchr(fn,DF)+1;
		//s=scA(fn,szImgDir);	
		scA(tp,name);
		return LibReadFile(fn,oSize); //read img file from disk
	}

	
	s=scA(fn,szImgDir);	scA(s,name); //try root dir
	buf=LibReadFile(fn,oSize);
	if(buf||!IS_DIG(*name)) break;
	
	while(IS_DIG(*name)) name++;  //200px-abc.png ==> abc.png 
	P_C2(name,'_','-');				   
	if(strnicmp(name,"px",2)) break; 
	name+=2; 
	P_C2(name,'_','-');
	s=scA(fn,szImgDir);
}	
	return buf;
}








