//final step

#include "WF_Main.h"

extern BOOL bTest;
extern int CharCnt(char *s,char c);

#define HIDE_START	11 // 10 is linebreak

static char szHide[40];
static char szASC[132];
static int iHideMax;

static char noStuff1[5][16]=				   //"<blockquote>"
{	"",		"<nowiki>","<pre>","<math>"	,"<code>"		 };

static char noStuff2[5][16]=					//	</blockquote>
{	"",		"</nowiki>",	"</pre>",	"</math>",	"</code>" };

static char noStuff1C[5][20]=					 // blockquote
{	"","&lt;nowiki&gt;","&lt;pre&gt;","&lt;math&gt;","&lt;code&gt;"
};

static char noStuff2C[5][20]=								// blockquote
{	"","&lt;/nowiki&gt;","&lt;/pre&gt;","&lt;/math&gt;","&lt;code/&gt;"
};

void InitHideTable() //these chars must be hidden in a nowiki or pre tag
{	char *s,*start;

	start=s=szHide;
	*s++='!';  
	*s++='#';
	*s++=C_Q;
	*s++='*';
	*s++='+';
	*s++='-';
	*s++=':';
	*s++=';';
	*s++='=';
	*s++='[';
	*s++=']';
	*s++='_'; 
	*s++='{';
	*s++='|';
	*s++='}'; //28++
				//insert more here, no need for order

	iHideMax=s-start;
}
char GetHideCharIndex(char c)	//returns index of hide char
{	char i;		   //check every guy so szHide[] 
							   //does not need to be in order
	for(i=0;i<iHideMax;i++)
	{	if(c==szHide[i]) return i+HIDE_START; }
	return 0; 
}

void InitHide()	//compile an ASC table, remap hide chars 
{	char i,k; 
	
	InitHideTable(); //get table and iHideMax
	for(i=1;i<30;i++) szASC[i]=i;

	for(i=30;i<127;i++)
	{	k=GetHideCharIndex(i);
		if(k) szASC[i]=k; //this char must be hidden
		else szASC[i]=i;
	}
	iHideMax+=HIDE_START; //easier on recovering
}

char *WHide(char *s,char *sNew)	//hide wiki markup chars
{	char *old=sNew;
	
	if(!iHideMax)	InitHide();
	
	while(*s)
	{	if(*s>0) *sNew++=szASC[*s]; //remap this char to our own ASC table
		else *sNew++=*s;
		s++;
	}
	return sNew;
}

char *WReCover(char *s,char *sNew) //recover wiki markup chars hidden
{	if(!iHideMax)	InitHide();	//should not happen
	while(*s)				//remap back
	{	if(*s>=HIDE_START&&*s<iHideMax)	*sNew++=szHide[*s-HIDE_START]; 
		else *sNew++=*s;
		s++;
	}
	return sNew;
}

inline char *scAin(char *dest,char *src) //returns NULL end of *dest
{
	while(*src) *dest++=*src++;
	*dest=0;
	return dest; 
}

	  //rid tag if missing end
char *CheckTag(char *s,char *sT,char *eT,BOOL bRid)
{	char *start=0,cnt=0,i; 
	int sLen=strlen(sT),eLen=strlen(eT);
	
	if(!bRid)
	{	while(*s)
		{	if(*s!='<') { s++; continue; }
			if(!strnicmp(s,sT,sLen)) 
			{	cnt++; 
				s+=sLen;
				continue;
			}

			if(!strnicmp(s,eT,eLen)) 
			{	cnt--; s+=eLen;
				continue;
			}
			s++;
		}
		if(cnt>0)
		{	for(i=0;i<cnt;i++) s=scA(s,eT);	}
		return s;
	}

	while(*s)
	{	if(*s!='<') { s++; continue; }
		if(!strnicmp(s,sT,sLen)) 
		{	if(start) start[1]='z'; //sNew=scAin(sNew,eT);
			start=s;
			s+=sLen;
			continue;
		}

		if(!strnicmp(s,eT,eLen)) 
		{	s+=eLen;
			start=0; 
			continue;
		}
		s++;
	}
	if(start) //still open!	just close it at LB
		start[1]='z';
	return s;

}

	
void ParseBack(char *s,char *sNew,char c)
{	char *t; char *bufNew=sNew;

	while(*s)
	{	if(*s!=c)  { *sNew++=*s++; continue; }
		
		if(c!=1)  sNew=scAin(sNew,noStuff1[c]);
		s++;
		t=s;
		To_C(t,c); //find ending tag
		if(*t) *t++=0;
		if(c<4)
			sNew=WReCover(s,sNew); 
		else S_CPY(sNew,s);
		if(c!=1)  sNew=scAin(sNew,noStuff2[c]);
		s=t; 	
	}
	*sNew++=0; *sNew=0;
}

extern char szTitle[];
void ParseStuff(char *bufOld,char *bufNew,int *oTextBytes)
{	char *t,d,*s=bufOld,*sNew;	
	int k;  
	int bHas[5];
	char i;	int bStuff=0;
	
	zm(bHas,sizeof(bHas));
	
	while(*s) //check if has tag
	{	for(i=1;i<5;i++)
		{	if(*s==i) 
			{	bHas[i]=1; bStuff=1; break; }
		}
		s++;
	}
	s=bufOld; sNew=bufNew;
	for(i=1;i<5;i++)  //recover if has tag
	{	if(bHas[i])	 
		{	ParseBack(s,sNew,i); 
			t=s; s=sNew; sNew=t;
		} 
	}
	
	zm(bHas,sizeof(bHas));

	if(s==bufNew) strcpy(bufOld,bufNew);
	s=bufOld; sNew=bufNew;

	while((d=*s))
	{	
		if(d==LB) 
		{	k=0; 	//cpy LB, insert <p> if 2 or more LBs
			while(*s==LB) { s++; k++; }	 
#if CONVERT_TO_MARKDOWN
      *sNew++=LB;
      if (k>1) *sNew++=LB; //two newlines for new paragraph
#else
      if(k>1) sNew=scAin(sNew,"\n<p>");
			else *sNew++=LB;
#endif
			continue;
		} 
#if CONVERT_TO_MARKDOWN
    if(d=='-')
    {	
    	while(*s=='-') *sNew++=*s++;
      continue;
    }
#else
    if(d=='-')
		{	if(CharCnt(s,'-')==4)
			{	sNew=scA(sNew,"<hr>"); P_C(s,'-'); }	
			else
			{	while(*s=='-') *sNew++=*s++; } 
			continue;
		}
#endif
		if(d=='_'&&s[1]==d&&IS_UP(s[2])) 
		{ P_C(s,'_'); J_C(s,'_'); continue; } //this one skips for instance frequent __TOC__ (it is possibly feasible to implement it)
		*sNew++=*s++;
	}
	*sNew=0;
	//sNew=CheckTagH(bufNew);						
	sNew=CheckTag(bufNew,"<small>","</small>",1);						
	
	if(bTest)
		WWriteTest("ParseStuff",bufNew);	

	*oTextBytes=sNew-bufNew;
}
extern int iNS;

void RidLB(char *s)	 
{	char *sNew=s; char *buf=s;

	while(*s)
	{	if(*s==LB1&&s[1]==LB2) //\r\n ==> \n
		{	*sNew++=LB; *sNew++=SPACE; s+=2; continue; }

		if(*s==LB1||*s==LB2) //turn to the same one
		{	*sNew++=LB; s++; continue; }
		
		if(*s==TAB) {	*sNew++=SPACE; s++; continue; }
		*sNew++=*s++;
	}
	Z4(sNew);
}

extern int WMarkDeepStr(char *buf,int *offS,int *offE,char *cS,char *cE,int max);

char *ParseXMLCharBack(char *buf) //'<' and '>' only
{	char *bufNew,*sNew,*s=buf;

	bufNew=GmC(SizeMem(s)*2+40*KB);  sNew=bufNew;
	while(*s)
	{	if(*s=='<')
		{	sNew=scA(sNew,"&lt;"); s++; continue; }
		
		if(*s=='>')
		{	sNew=scA(sNew,"&gt;"); s++; continue; }
		*sNew++=*s++;
	}
	Fm(buf);
	bufNew=ReGmC(sNew+10-bufNew,bufNew);
	return bufNew;
}

char *ParseXMLCharOther(char *s)
{	char *sNew=s,c;

	while(*s)			 // ||c==W_BLOCK
	{	if((c=*s)==W_PRE) 
		{	*sNew++=*s++; while(*s&&*s!=c) *sNew++=*s++;
			if(*s==c) *sNew++=*s++;
			continue;
		}
		if(*s!='&') {  *sNew++=*s++;  continue; }
		
		if(!strncmp(s,"&quot;",6))
		{	*sNew++=C_QQ; s+=6; continue; }
		
		if(!strncmp(s,"&amp;",5))  
		{	*sNew++='&'; s+=5;  continue; }  		
		  
		if(!strnicmp(s,"&lt;",4)) // '<'
		{	*sNew++='<'; s+=4; continue; }	//normal stuff
		
		if(!strnicmp(s,"&gt;",4))
		{	*sNew++='>'; s+=4; continue; }
		*sNew++=*s++; //dont know what it is
	}
	*sNew++=0;	*sNew=0;
	return sNew;
}

char *ParseXMLChar(char *s,int *offSG,int *offEG) //also hide wiki markup chars in nowiki
{	int k,i,j,bytes;	char *sNew=0;
	char *b1=s,*t;
	int len[5];

	StrRidBetweenI(s,"&lt;!--","--&gt;");
	for(i=1;i<5;i++) len[i]=strlen(noStuff2C[i]);

	for(j=1;j<5;j++) //has to loop for each: nested like <pre><nowiki>...</nowiki></pre>
	{	
		k=WMarkDeepStr(b1,offSG,offEG,noStuff1C[j],noStuff2C[j],MAX_OFF);
		if(!k) continue;
		
		s=sNew=b1;  
		for(i=0;i<k;i++)
		{	bytes=offSG[i]-(s-b1);
			memmove(sNew,s,bytes); 
			sNew+=bytes; s+=bytes;

			*sNew++=(char)j;
				
			t=b1+offSG[i]-1;
			if(t>=b1&&*t==LB) *sNew++=LB; //LB preceding nowiki, must append

			s+=5; J_C(s,';'); //jump over
			b1[offEG[i]]=0;
			if(j<4)		
				sNew=WHide(s,sNew);	//hide markup
			else S_CPY(sNew,s);
			*sNew++=(char)j; //ending tag
			
			s=b1+offEG[i]+5; J_C(s,';'); 
		}
		S_CPY(sNew,s);
	}			 //must first parse special tags: some chars must not be parsed
	
	return ParseXMLCharOther(b1);	  //when in pre tag
}

extern int *offSPre,*offEPre;

char *PreParse(char *buf)
{	if(!offSPre)  //a global array
	{	offSPre=GmI(MAX_OFF*2); 
		offEPre=offSPre+MAX_OFF;
	}
	RidLB(buf);
	ParseXMLChar(buf,offSPre,offEPre);
	return buf;	
}

#define S_CPYTO5N(dest,src,c1,c2,c3,c4,c5,cnt) \
    do{ int cNNt=cnt; \
	while(*src&&*src!=c1&&*src!=c2&&*src!=c3&&*src!=c4&&*src!=c5&&cNNt--) \
	*dest++=*src++; *dest=0; \
	}while(0)


//parse wiki text as <pre> .... </pre>
//for output as raw wiki text or raw template
char *ParseAsPre(char *buf,int *oTextBytes)
{	char *s=buf,*bufNew,*sNew,*start=buf;

	if(!strnicmp(s,"<title",6))	//has page info
	{	if((s=strstr(s,"<text"))) 
		{	*s++=0;  
			J_C(s,'>');
			start=s; //start of text follows info
		} 
	}

	s=buf;
	bufNew=GmC(SizeMem(buf)*2+KB); 
	sNew=bufNew;
	
	if(start!=buf) //info
	{	sNew=scA(sNew,
	"<p><pre style=\"font: x-small sans-serif;font-size: 95%;width: 80%;\">"
		"<b>Header start</b>\n");
		while(1)
		{	To_C(s,'<'); if(!*s) break; 
			s++;  
			if(*s=='/') { J_C(s,'>'); continue; } //ending tag, ignore
		
			C_UP(*s); sNew=scA(sNew,"<b>");
			S_CPYTO(sNew,s,'>'); sNew=scA(sNew,"</b>");
			*sNew++=':'; *sNew++=SPACE; s++; //label
		
			while(1)
			{	S_CPYTON(sNew,s,'<',30);   
				if(!*s||*s=='<') break;
				S_CPYTO2N(sNew,s,SPACE,'<',30);
				*sNew++=LB;
			}
			if(s[1]=='/') J_C(s,'>');
			S_CPYTO(sNew,s,'<');
		}
		W_RE(bufNew,sNew); *sNew++=LB;

		sNew=scA(sNew,"<b>Header end</b></pre><p>"); 
		s++; J_C(s,'>'); //to text start
	}

	sNew=scA(sNew,"<p><pre style=\"font: x-small sans-serif;font-size: 95%;\">");
	start=sNew; 
	
	while(1)
	{	S_CPYTON(sNew,s,LB,50);   
		if(!*s) break;
		if(*s!=LB) 
		{	S_CPYTO5N(sNew,s,SPACE,LB,'{','[','&',50);
			if(*s!=LB)
			{		
				*sNew++=LB;
			}
			else *sNew++=*s++;
		}
		else *sNew++=*s++;
	}
	
	if(sNew==start){ *sNew++=SPACE; } 		
	sNew=scA(sNew,"</pre>");
	Fm(buf);
	if(oTextBytes) *oTextBytes=sNew-bufNew;
	bufNew=ReGmC(sNew+4-bufNew,bufNew);
	return bufNew;
}


	






