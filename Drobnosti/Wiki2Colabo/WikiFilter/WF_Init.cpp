#include "WF_Main.h"

char szVersion[]="2.3";


extern HANDLE fpLog;
extern char *szImgDir,*szLocalDir;
extern BOOL bWriteLog,bShowRaw;
extern int iMaxTemplate;

extern int iMaxLang;
extern STR_T nsStrE;

extern WL_T *glt;
extern BOOL bShowImg,bShowTemplate;
extern BOOL RegReadInt(char *fn,char *fnSub,char *label,int *oValue);
extern BOOL RegReadStr(char *fn,char *fnSub,char *label,char *oStr);
extern void RidHex(char *in,char *out);

#define P_SPACEA(s) while(*s==SPACE||*s==LB1||*s==LB2||*s==TAB) s++

char *WReadMyFile(char *fnNoPath,int *oBytes)
{	char fn[MP];
	return LibReadFile(WAddPath(fnNoPath,fn),oBytes);
}

HANDLE WOpenMyFile(char *fnNoPath)
{	char fn[MP];
	return fopenWin(WAddPath(fnNoPath,fn),"r");
}

void WFreeIndex()
{	int i,j,k;
	WL_T *lt;

	if(!glt) return;
	for(k=0;k<iMaxLang;k++)	 
	{	lt=glt+k;
		for(j=0;j<MAX_DOMAIN;j++)  
		{	for(i=0;i<MAX_SPACE_USE;i++)
			{	Fm(lt->ptG[j][i]);	
				Fm(lt->szWord1[j][i]); 
				lt->maxWord1[j][i]=0;
			}
			Fm(lt->web[j]);
			StrFree(lt->nsStr+j);
		}				 		
	}
}

void WCloseDumpFile() //close all dump and index file handles
{	int i,j;
	
	if(!glt) return ;
	for(j=0;j<iMaxLang;j++)	
	{	for(i=0;i<MAX_DOMAIN;i++)
		{	if(glt[j].fpDump[i])
			{	CloseHandle(glt[j].fpDump[i]); 
				glt[j].fpDump[i]=0; 
			}
			if(glt[j].fpIndex[i])
			{	CloseHandle(glt[j].fpIndex[i]); 
				glt[j].fpIndex[i]=0; 
			}
		}
	}
}
void WFreeLang()
{	if(!glt) return;

	WCloseDumpFile();
	WFreeIndex();
	iMaxLang=0;
	Fm(glt);
}

void AddChar(char *fn,char c)
{	char *s=fn;
	if(!*s) return; 
	To_L(s);
	while(s>fn&&*s==c) s--; 
	s++; *s++=c;  *s=0;
}

char *RidEndChar(char *fn,char c)
{	char *s=fn;
	if(!*s) return fn; 
	To_L(s);
	while(s>fn&&*s==c) s--;
	*(++s)=0; 
	return fn;
}				  

extern BOOL HasExt(char *fname,char *comString);

//<base>http://commons.wikimedia.org/wiki/Main_Page</base>
//<base>http://fr.wikisource.org/wiki/Accueil</base>
//<sitename>Wikisource</sitename>
int WGetDumpHead(char *s,char *oLang,char *oSite,
					char *oMainPage,char *oGenerator,char *oUrl)
{	char *t,*start;

	if(oLang)
	{	*oLang=0;
		t=strstr(s,"<base>");
		if(t) 
		{	J_C(t,'/'); 
			if(t[2]!=DOT) strcpy(oLang,"en");
			else S_CPYTON(oLang,t,DOT,2);
		}
		else return 0; 

	}
	if(oUrl)
	{	*oUrl=0;
		t=strstr(s,"<base>");
		if(t) 
		{	J_C(t,'>');	P_SPACE(t);
			S_CPYTON(oUrl,t,'<',300);
		}
		else return 0; 

	}
	if(oSite)
	{	*oSite=0;
		t=strstr(s,"<sitename>");
		if(t) 
		{	J_C(t,'>'); P_SPACE(t);
			S_CPYTON(oSite,t,'<',100);
		}
		else return 0; 
	}
	if(oMainPage)
	{	*oMainPage=0;

		t=strstr(s,"<base>");
		if(t) 
		{	J_C(t,'>'); P_SPACE(t); 
			start=t;  To_C(t,'<');
			BACK_C(start,t,'/');	t++;
			S_CPYTON(oMainPage,t,'<',198);
		}
		else return 0; 
	}
	if(oGenerator)
	{	*oGenerator=0;

		t=strstr(s,"<generator>");
		if(t) 
		{	J_C(t,'>'); 
			S_CPYTON(oGenerator,t,'<',98);
		}
		else return 0; 
	}
	return 1;
}

int DumpReadNSStr(char *s,char *out,int *off,int max)
{	char *t;  int k=0;

	if(!(s=strstr(s,"<namespaces>"))||!(t=strstr(s,"</namespaces>"))) return 0; 
	*t++=0;	
	t=out;
	while(1)
	{	if(!(s=strstr(s,"key="))) break;
		J_C(s,C_EQ); P_C3(s,SPACE,C_QQ,C_EQ); //goto key value
		
		if(*s=='-')	continue; //ignore
		if(*s=='0') 
		{   if(off) off[k]=t-out;
			k++; 
			*t++=SPACE; *t++=':'; *t++=0; t++;
			continue; 
		} 
		if(!IS_DIG(*s)) break; //wrong format
		
		J_C(s,'>'); P_SPACE(s);
		
		if(off) off[k]=t-out;

		S_CPYTO(t,s,'<'); W_RE(out,t); *t++=':'; t++;
		k++;
		if(k>=max) break;
	}
	*t=0;
	return k;
}
int DumpGetNSStr(STR_T *str,char *buf)
{	
	StrGetMem(str,MAX_NS_LEN*MAX_SPACE_USE,MAX_SPACE_USE);
	
	if(!(str->cnt=DumpReadNSStr(buf,str->buf,str->off,MAX_SPACE_USE)))
	{	StrFree(str); return 0; } //impossible unless wrong file
	
	StrReGetMem(str,StrGetSize(str)+4,str->cnt);	 
	StrGetMaxLen(str); 	
	if(str->maxLen<nsStrE.maxLen) str->maxLen=nsStrE.maxLen;
	StrSort(str);
	return 1;
}

extern int TempGetInfo(int *oCnt);
extern char *GetSiteStrShort(int site);
extern char *GetSiteStrLong(int site);
extern int FindGetVersion(int lang,int site);
extern int iVersion;
char *InitGetLangInfo(char *s)
{	int j,i,bEn; char *t;  int v;

	s=scA(s,"<p><h2>General information</h2><p>");
	s=Gcpy3(s,
		"<a href=\"http://wikifilter.sourceforge.net\">WikiFilter (WikiIndex)</a> version: ",
		szVersion,
		"<p><b>Important note</b>: This version is <b>NOT</b> fully compatible "
		"with earlier versions. "
		"So index files compiled by earlier versions of <i>WikiIndex</i> "
		"may not work properly. Thus, it is highly recommended that you recompile "
		"any file listed below that has a <font color=\"red\">red</font> mark."
		"<h4>Database files</h4>");

	s=scA(s,"<ul>");
	for(i=0;i<iMaxLang;i++)
	{	s=scA(s,"<li><b>Language: ");
		*s=glt[i].name[0]; C_UP(*s); s++; *s++=glt[i].name[1];
		s=scA(s,"</b>");
		s=scA(s,"<ol>");
		if(glt[i].name[0]=='e'&&glt[i].name[1]=='n') bEn=1;
		else bEn=0;
		for(j=0;j<MAX_DOMAIN;j++)
		{	if((j==5||j==7)&&!bEn) continue; //these 2 english only
			s=scA(s,"<li>");	
			t=s; s=scLess1(s,GetSiteStrLong(j)); C_UP(*t);
			s=scA(s,"<ul><li>File: ");	
			
			if(glt[i].dumpName[j]) s=Gcpy2(s,glt[i].dumpName[j],"</li>");
			else s=Gcpy2(s,"Not installed","</li>");
			
			s=scA(s,"<li>Index version: ");
			if(glt[i].dumpName[j])	
			{	v=FindGetVersion(i,j);
				if(!v) s=scA(s,"N/A");
				else if(v==iVersion)	s=scA(s,szVersion);
				else s=scA(s,"<font color=\"red\">2.2 or earlier</font> "
				"(recompiling highly recommended)"); 
			}
			else s=scA(s,"N/A");
			s=scA(s,"</li></ul>");
		} 
		s=scA(s,"</ol></li>");
	}
	s=scA(s,"</ul><h4>Templates</h4><ul>");
	i=TempGetInfo(&j);
	wsprintf(s,"<li>Cached: %d</li><li>Memory use: %d bytes</li></ul>",j,i);	To_0(s);
	return s;
}

extern char *UtfLow(char *in,char *out); //*in *out can be same buf

char *GetMainPage(char *web)
{	char *t;
	To_WN_EX(web,t); return t;
}

BOOL WGetLangInfo(WL_T *lt,int dm,char *fnDump)
{	int i=4000;	 
	char *buf;
	char mainPage[304];

	if(!(buf=LibReadFileBytes(fnDump,0,&i))) return 0;
	
	if(!WGetDumpHead(buf,0,0,0,0,mainPage))	
	{	Fm(buf);
		return 0; 
	}
	if(!DumpGetNSStr(lt->nsStr+dm,buf))
	{	Fm(buf); 
		return 0; 
	}				   
	StrLowUtf(lt->nsStr+dm);

	i=strlen(mainPage)+4;
	lt->web[dm]=GmC(i+300+strlen(fnDump)+40); 
	//strcpy(lt->web[dm],mainPage);
	RidHex(mainPage,lt->web[dm]);
	
	lt->titleOld[dm]=lt->web[dm]+i;
	lt->dumpName[dm]=lt->titleOld[dm]+300;

	Gcpy3(lt->titleOld[dm],GetSiteStrShort(dm),lt->name,GetMainPage(lt->web[dm]));
	strcpy(lt->dumpName[dm],fnDump);

	Fm(buf);
	return 1; 
}
 

int WGetLangStr(char *str)
{	int i;
	char *s=str,*t,c;
	char *buf;

	if(!(i=strlen(s))) return 0; //empty! 
	S_LOW1(s); //turn to lower-case
	
	buf=GmC(i+10); 	t=buf;
	s=str;
	i=0; 
	//strict checking, even this: "  ;  en ;  ; ;; fr;de ; " will become: "en fr de"
	while(*s)  
	{	P_C2(s,SPACE,';'); //all spaces and ';'s are ignored
		if(!*s) break;	//no more
		
		c=s[1];
		if(!c||c==SPACE||c==';') //ignore 1-char lang name
		{	s++;
			continue;
		}
		*t++=*s++; //cpy only 2 chars
		*t++=*s++;
		To_C2(s,SPACE,';');	
		s++;
		*t++=0; 
		i++; //increment lang cnt			
	}
	Z4(t);
	memcpy(str,buf,t-buf);
	Fm(buf);
	return i;
}
extern BOOL HasExt(char *fname,char *extStrs);

BOOL WGetLangOne(WL_T *lt,char *name)
{	char temp[100],temp1[MP],*s; 

	int j,cntDm=0;

	for(j=0;j<MAX_DOMAIN;j++) //get dump name for each domain
	{	if(j==7) strcpy(temp,"Wikimedia Commons");
		else scLess1(temp,GetSiteStrLong(j));	 
		s=temp; To_0(s);
			
		          ///////////// ensure mainPage name is NOT NULL, even missing
				  ///////////// to save excessive NULL checking
		
		strcpy(s,"_File");	//"wikipedia_File" is dump fname (lt->nameMainPage[j]=
		RegReadStr("WikiFilter",name,temp,temp1);
		
		strcpy(lt->name,name); 
		strcat(lt->name,":");	//"en" ==> "en:", just for later convenience

/*
		if(!temp1[0]||!HasExt(temp1,"xml\0\0")||!WGetLangInfo(lt,j,temp1))
		{	 continue; }
			
		Log2Eq("Found dump file ",temp1);
*/
		strcpy(s,"_1stWordLen");	//"wikipedia_1stWordLen" 
		RegReadInt("WikiFilter",name,temp,&(lt->c1stWord[j]));
    lt->c1stWord[j] = 0;
/*
		if(lt->c1stWord[j]<0) lt->c1stWord[j]=0;
*/

		cntDm++;
	}
	if(cntDm>0)
	{	
    if(!strncmp(name,"en",2)) 
    {
      lt->bEn=1;
      lt->nsStr[0] = nsStrE;
    }
		else lt->bEn=0;
    return 1;
	}
	lt->name[0]=0;
	return 0; 
}

extern int StrInit(STR_T *str,char *buf,int bSort);
extern char szNameSpaceE[];
extern void StrFreeNoBuf(STR_T *str);

WL_T *WGetLangCnt(int *total) //read registry info
{	int i,k,cntLang=0;	
	WL_T *lt; 
	char temp[400],*s;

	*total=0;

	WFreeLang(); //strange, but free it anyway
	zm(temp,sizeof(temp));
	
/*
	RegReadStr("WikiFilter",0,"LangName",temp); 
	Log2Eq("LangName",temp);

	if(!(k=WGetLangStr(temp))) return 0; // no language installed 
	
	if(k>MAX_LANG) k=MAX_LANG; //limit 
*/
  strcpy(temp, "en");
  k = 1;
	
	lt=GmZ(k,WL_T);  //assume k langs installed
	
	StrInit(&nsStrE,szNameSpaceE,1);
	
	s=temp;
	for(i=0;i<k;i++)
	{	cntLang+=WGetLangOne(lt+cntLang,s);
		N_S(s);
	}
	if(!cntLang) { Fm(lt); StrFreeNoBuf(&nsStrE); return 0; }

	*total=cntLang;
	return lt;
}
extern void WLoadForm();  
extern char *WLoadFormBin(int id,char *fn,int *bytes,int extra);
#define P_SPACEA1(s,c)  while(*s&&(*s==SPACE||*s==TAB||*s==LB1||*s==LB2||*s==c)) s++

void SaveIniFile()
{	char *buf,*s; char fn[MP]; int k=0;

	if(!(buf=WReadMyFile("WikiFilter23.ini",&k))) return ;
	
	if((s=StrStrI(buf,"wf_show_img")))
	{	s+=strlen("wf_show_img"); P_SPACEA1(s,C_EQ);
		if(bShowImg) *s='1';
		else *s='0';
	}
	if((s=StrStrI(buf,"wf_show_raw")))
	{	s+=strlen("wf_show_raw"); P_SPACEA1(s,C_EQ);
		if(bShowRaw) *s='1';
		else *s='0';
	}
	if((s=StrStrI(buf,"wf_show_template")))
	{	s+=strlen("wf_show_template"); P_SPACEA1(s,C_EQ);
		if(bShowTemplate) *s='1';
		else *s='0';
	}
	if((s=StrStrI(buf,"wf_write_logfile")))
	{	s+=strlen("wf_write_logfile"); P_SPACEA1(s,C_EQ);
		if(bWriteLog) *s='1';
		else *s='0';
	}
	LibWriteFile(WAddPath("WikiFilter23.ini",fn),buf,k);
	Fm(buf);	
}
		 //rid all comment lines from ini file
		 //returns *s
char *IniRidComment(char *s)
{	char *sNew=s,*sOrg=s;  
	
	if(!s) return 0; 

	while(*s)
	{	P_SPACEA(s);
		if(*s=='/'&&s[1]=='/') 
		{	To_C2(s,LB1,LB2); continue; }

		while(*s&&*s!=LB1&&*s!=LB2)	*sNew++=*s++; //cpy non-comment line
		
		*sNew=0; W_RE(sOrg,sNew); // rid ending spaces if any
		sNew++; s++;
	}
	Z4(sNew);
	return sOrg;
}

int IniReadStr(char *s,char *str,char *out)
{	int len=strlen(str);
	*out=0;
	while(*s)
	{	if(!strnicmp(s,str,len))
		{	s+=len; P_SPACEA1(s,C_EQ);
			strcpy(out,s); return 1; 	
		}
		N_S(s);
	}	
	return 0; 
}					
//set *oI only if got an number
//or *oI is not changed (preserve it' default value)
int IniReadInt(char *s,char *str,int *oI)
{	int len=strlen(str);
	while(*s)
	{	if(!strnicmp(s,str,len))
		{	s+=len; P_SPACEA1(s,C_EQ);
			if(IS_DIG(*s)) 
			{	*oI=WStrToInt(s);  return 1; }
			return 0; 	
		}
		N_S(s);
	}	
	return 0; 
}

//because we are in another process, we do not modify current dir
BOOL OpenIniFile()
{	
char *buf;//,*s;  //int k=0;
	char temp[MP];	
	
	if(!fpLog)
		fpLog=fopenTemp(WAddPath("WikiFilter_Log.txt",temp));	 
	
	if(!(glt=WGetLangCnt(&iMaxLang)))
	{	Log("Fatal error: cannot find any .xml dump file from system registry"
		"(Check Registry key \"WikiFilter\" to see" 
		"if dump files are in the correct directory)");
		CloseHandle(fpLog);
		return -1; //no ini file, cannot run! 
	}
	
	if((buf=IniRidComment(WReadMyFile("WikiFilter23.ini",0)))) //load some options
	{
		IniReadStr(buf,"WF_Img_Dir",temp); 
		if(temp[0]&&GetFT(RidEndChar(temp,DF))==ISDIR)	
		{	
			szImgDir=GmC(strlen(temp)+4); 
			strcpy(szImgDir,temp); 
			strcat(szImgDir,"\\");
		}
		
		IniReadStr(buf,"WF_LOCAL_FILE_DIR",temp); 
		if(temp[0]&&GetFT(RidEndChar(temp,DF))==ISDIR)	
		{	szLocalDir=GmC(strlen(temp)+4); 
			strcpy(szLocalDir,temp); 
			strcat(szImgDir,"\\");
		}
		
		IniReadInt(buf,"WF_Max_Template_cache",&iMaxTemplate);
		IniReadInt(buf,"WF_Show_Raw",&bShowRaw);
		IniReadInt(buf,"WF_Show_Img",&bShowImg);
		IniReadInt(buf,"WF_Show_Template",&bShowTemplate);

		IniReadInt(buf,"WF_Write_LogFile",&bWriteLog);

		Fm(buf);
	}
	else
	{	
// 		WAddPath("WikiFilter23.ini",temp);
// 		if((buf=WLoadFormBin(IDR_INI,temp,0,0)))
// 		Fm(buf);
	}
//	WLoadForm(); 

  return 1; 
}
extern void WikiTemplateFree();
extern void FreeForm();
extern void TableExit();  // a dozen ints
extern void ExitNSE();
extern int initDone;			    
extern SBOX_T *sb;

void WikiExit()
{	Fm(szImgDir); Fm(szLocalDir);
	Fm(sb); FreeForm(); //Fm(szTitle); //ensure it is freed
	if(fpLog)	{ CloseHandle(fpLog); fpLog=0; }
	WFreeLang();
	WikiTemplateFree();
	TableExit();
	ExitNSE();

	SaveIniFile();
	initDone=0;
}




