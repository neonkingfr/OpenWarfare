
#include "WF_Main.h"

#define MAX_LIST_CHAR	40

typedef struct
{	int typeOld[MAX_LIST_CHAR];
	int typeNew[MAX_LIST_CHAR];
	int typeOldCnt;
	int endType;
	int bNoMore;
}W_LIST_FLAG;

#if CONVERT_TO_MARKDOWN
char szListStart[6][8]={"","*","1.","* ","* ",""};
char szListEnd[6][8]={"","","","","",""};
char szListData[6][8]={"","","","","",""};
#else
char szListStart[6][8]={"","<ul>","<ol>","<dl>","<dl>",""};
char szListEnd[6][8]={"","</ul>","</ol>","</dl>","</dl>",""};
char szListData[6][8]={"","</li>","</li>","</dd>","</dt>",""};
#endif

extern int bTest;
inline char *scAin(char *dest,char *src) //returns NULL end of *dest
{	while(*src) *dest++=*src++;
	*dest=0;
	return dest; 
}

inline int WGetListType(char c)
{	int ret=0;
	if(c=='*') ret=1;  //unordered list	 
	else if(c=='#')  ret=2; //ordered list 
	else if(c==':')  ret=3;  //indent
	else if(c==';')  ret=4;  //dont know exactly what it is
	return ret; 
}

// we check up to 40 list chars if there are so many,like:
//#####:::::*:#*******;;*;;###***:

//keep track of each type when forward going or backing out
char *PrefixListTag(char *s,char *sNew,char **pS,W_LIST_FLAG *ft)
{	int i,cntOld=0,cntNew=0;
	int start=0;

	for(i=0;i<MAX_LIST_CHAR;i++)
	{	
		ft->typeNew[i]=WGetListType(s[0]); 
		if(!ft->typeNew[i]) break; //stop checking if none list char
		P_C2(s,SPACE,TAB); //allow spaces between two list chars
		cntNew++; s++;				   
	}							   
					   //no new and no old, do nothing
	if(!cntNew /*&&!ft->typeOldCnt*/) 
	{	ft->bNoMore=1;
		return sNew;
	}
	
	cntOld=ft->typeOldCnt;

	for(i=0;i<cntOld;i++)  //get 1st different tag
	{	if(ft->typeOld[i]!=ft->typeNew[i])	break;
		else if (i<cntNew-1) start++;
	}

#if CONVERT_TO_MARKDOWN
  for (int j=0; j<start; j++) *sNew++ = '\t'; //indent
  if (start<cntNew)
    sNew=scAin(sNew,szListStart[ft->typeNew[start]]);
  else if (start>0)
    sNew=scAin(sNew,szListStart[ft->typeNew[start-1]]);
  if(cntNew)
  {	
    ft->endType = ft->typeNew[cntNew-1];
    for(i=0; i<cntNew; i++) ft->typeOld[i]=ft->typeNew[i]; //save new info
  }
  else memset(ft->typeOld,0,sizeof(ft->typeOld));
  ft->typeOldCnt=cntNew; 
#else
  for(i=(cntOld-1);i>=start;i--)	//close old	(if any) backward
		sNew=scAin(sNew,szListEnd[ft->typeOld[i]]);	 
	
	if(i<(cntOld-1)&&start>=cntNew) 
		*sNew++=LB;	//line after list, insert a LB

	for(i=start;i<cntNew;i++)  //open new (if any) forward
		sNew=scAin(sNew,szListStart[ft->typeNew[i]]);

	if(cntNew)
	{	if(ft->typeNew[cntNew-1]<3) sNew=scAin(sNew,"<li>"); //prefix item tag 
		else //if(ft->typeNew[cntNew-1]==3) 
			sNew=scAin(sNew,"<dd>");
		//else sNew=scAin(sNew,"<dt>");
	
		ft->endType=ft->typeNew[cntNew-1];
		for(i=0;i<cntNew;i++) ft->typeOld[i]=ft->typeNew[i]; //save new info
	}
	else memset(ft->typeOld,0,sizeof(ft->typeOld));
	ft->typeOldCnt=cntNew; 
#endif

	*pS=s; 
	return sNew;
}
			  //parse one block
char *ParseListOne(char *s,char *sNew,char **pS)
{	W_LIST_FLAG ft;
									   
	zm(&ft,sizeof(ft));
#if CONVERT_TO_MARKDOWN
  *sNew++ = LB; //list block must start with empty line
#endif
  while(1)
	{	sNew=PrefixListTag(s,sNew,&s,&ft); //prefix, like "<li>"	
		if(ft.bNoMore) break;
		S_CPYTO(sNew,s,LB);	//list text
		
		if(ft.endType) 
		{	sNew=scAin(sNew,szListData[ft.endType]); //suffix,like "</li>"
			ft.endType=0;
		}
		while(IS_SPACE(*s))	*sNew++=*s++; //go to next line
	}
#if CONVERT_TO_MARKDOWN
  *sNew++ = LB; //list block must end with empty line
#endif
	*pS=s;  
	return sNew; 
}

int ParseList(char *s,char *sNew)
{	char *sOld=s,*sNewOrg=sNew;	
	int b=0;

	while(*s)
	{	if(*s!=LB) { s++; continue; }  //list char must follow LB
		//P_SPACE(s); //do not skip spaces! The list char must immediately follow LB!
    s++; //skip LB
    if(!WGetListType(*s)) {P_SPACE(s); continue; }	//no list char

		memcpy(sNew,sOld,s-sOld); sNew+=(s-sOld);
		sNew=ParseListOne(s,sNew,&s); *sNew++=LB;
		sOld=s;	b=1;
	}
	if(b) 
	{	S_CPY(sNew,sOld); 
		if(bTest) 
			WWriteTest("ParseList",sNewOrg);
		Z8(sNew); 
		return 1; 
	}
	return 0; 
}

