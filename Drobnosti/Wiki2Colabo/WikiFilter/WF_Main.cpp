#include "WF_Main.h"

//server function calls									   

HMODULE hme;
int initDone;
extern char szTitle[400];
extern  int OpenIniFile();
extern char *GetContent(int *oBytes,BOOL *bFreeAfterUse);
extern void WikiExit();
							 
char *WAddPath(char *fnNoPath,char *oFull)
{	char *s;

	oFull[0]=0;
	GetModuleFileName(hme,oFull,MP-1);
	if(!oFull[0]) return 0; 

	To_FN(oFull,s); strcpy(s,fnNoPath);
	return oFull;
}

HMODULE	GetMe()
{	return hme;	 }

BOOL APIENTRY DllMain( HANDLE hModule,DWORD  ul_reason_for_call,LPVOID lpReserved) 	    
{			 
    switch (ul_reason_for_call)
	{	 
		case DLL_PROCESS_ATTACH: hme=(HMODULE)hModule; 
			break;
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}  	 

///////////////////////////// filter functions
  
BOOL HttpSendDataFilter(HTTP_FILTER_CONTEXT *pfc,void *pvoid)
{	HTTP_FILTER_PREPROC_HEADERS *head;	
	int bytes=396,bFreeAfterUse; 
	char *buf;
	
	head=(HTTP_FILTER_PREPROC_HEADERS *)pvoid;
	if(!(head->GetHeader(pfc,"url",szTitle,(DWORD *)(&bytes)))) return 0;
	
	if(strnicmp(szTitle,"/wiki",5)) return 0; // non-wiki

	if(!initDone) initDone=OpenIniFile();
	if(initDone<0) return 0; //failed find any dump file 
	if(!(buf=GetContent(&bytes,&bFreeAfterUse))) return 0; 
	pfc->WriteClient(pfc,buf,(DWORD *)&bytes,0);	 
	
	if(bFreeAfterUse)  Fm(buf);
													   
	return 1; 
}

BOOL WINAPI __stdcall GetFilterVersion(HTTP_FILTER_VERSION *pVer)
{	
	pVer->dwFlags = (SF_NOTIFY_NONSECURE_PORT | SF_NOTIFY_PREPROC_HEADERS| 	
					 SF_NOTIFY_ORDER_DEFAULT);	//SF_NOTIFY_SEND_RESPONSE|
					 
	pVer->dwFilterVersion = HTTP_FILTER_REVISION;
	lstrcpy(pVer->lpszFilterDesc, TEXT("wf"));
	return 1;
}
	
DWORD WINAPI __stdcall HttpFilterProc(HTTP_FILTER_CONTEXT *pfc, DWORD note, VOID *pvData)
{	
	if((note&SF_NOTIFY_PREPROC_HEADERS))
	{	if(HttpSendDataFilter(pfc,pvData))  return SF_STATUS_REQ_FINISHED; }
	return SF_STATUS_REQ_NEXT_NOTIFICATION;
}
BOOL WINAPI TerminateFilter(DWORD dwFlags)	//not really needed because
{	WikiExit();	return 1; }			   //we'll be unloaded from memory completely
			 
