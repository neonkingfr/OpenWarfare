#include "WF_Main.h"

//various helper functions

extern HANDLE fpLog;
extern BOOL bWriteLog;

char *strcpyN(char *d,char *s,int cnt)
{ S_CPYN(d,s,cnt); return d; }
char *strcpyTo(char *d,char *s,char c)
{	S_CPYTO(d,s,c); return d; }


void LogN(char *s,int cnt)
{	if(!bWriteLog||!s||!*s) return ;
	WLB(fpLog);	fwriteWin(s,cnt,fpLog);
}

void Log3(char *s1,char *s2,char *s3)
{	if(!bWriteLog) return ;
	if(s1&&*s1) { WLB(fpLog);fwriteWin(s1,strlen(s1),fpLog);}
	if(s2&&*s2) { WLB(fpLog);fwriteWin(s2,strlen(s2),fpLog); }
	if(s3&&*s3) { WLB(fpLog);fwriteWin(s3,strlen(s3),fpLog); }
}
void Log2(char *s1,char *s2)
{	if(!bWriteLog) return ;
	if(s1&&*s1) { WLB(fpLog); fwriteWin(s1,strlen(s1),fpLog); }
	if(s2&&*s2) { WLB(fpLog);fwriteWin(s2,strlen(s2),fpLog);}
}
void Log(char *s1)
{	if(!bWriteLog||!s1||!*s1) return ;
	WLB(fpLog);	fwriteWin(s1,strlen(s1),fpLog);
}
void Log4(char *s1,char *s2,char *s3,char *s4)
{	if(!bWriteLog) return ;
	WLB(fpLog);	if(s1&&*s1) fwriteWin(s1,strlen(s1),fpLog);
	WLB(fpLog);	if(s2&&*s2) fwriteWin(s2,strlen(s2),fpLog);
	WLB(fpLog);	if(s3&&*s3) fwriteWin(s3,strlen(s3),fpLog);
	WLB(fpLog);	if(s4&&*s4) fwriteWin(s4,strlen(s4),fpLog);
}
void Log2Eq(char *s1,char *s2)
{	if(!bWriteLog) return ;
	if(s1&&*s1) { WLB(fpLog);fwriteWin(s1,strlen(s1),fpLog); }
	if(s2&&*s2){ fwriteWin("=",1,fpLog);	 fwriteWin(s2,strlen(s2),fpLog); }
}
void LogInt(char *label,int i)
{	char tp[40]; 
	if(!bWriteLog) return ;
	WLB(fpLog);	
	if(label&&*label) 
	{	fwriteWin(label,strlen(label),fpLog);  }
	wsprintf(tp," = %d",i);
	fwriteWin(tp,strlen(tp),fpLog);
}

//turn int value to str buf. same as wsprintf(%d)
char *ItoS(char *buf,int value)
{  char *s=buf,temp[40],*st=temp; 
   unsigned int u; 

   if(value<0)
   {  *s++=TEXT('-');	u=-value;  //always remember ending-NULL!
   	  if(value>-10) { *s++=(u+C_0); *s=0; return buf; }
   }
   else	
   {  if(value<10) { *s++=(value+C_0); *s=0; return buf; } //this is fast!
      u=value;
   }
   while(u) { *st++=(u%10)+C_0; u/=10; }  //st is reverse order
   while(st>temp) { *s++=*(--st); }	 //now copy backward
   *s=0;
   return buf;
}


//simple string to int conversion, used in place for atoi() 
//no overflow or sign check, to speed things up
//used to convert wiki nameSpace index (0 to 15)
int WStrToInt(char *buf)	
{	int v=0;

	P_C2(buf,'0',SPACE); //skip leading 0s
   
	while((IS_DIG(*buf))) //loop like this: 520 ==> v=5; v=52; v=520
	{	v=v*10+(int)(*buf-C_0); 
		buf++; 
	}	
	return v;
}
int WHexStrToInt(char *buf) //turn hex string to int	
{	int v=0,i;

	while(*buf==C_0) buf++; //skip leading 0s
	if(*buf=='x'||*buf=='X') {  buf++; } //0x prefix
	if(!IS_HEX(*buf)) return 0;	//no digit

	while(1)
	{	if(IS_DIG(*buf)) i=(*buf-C_0);
		else if(*buf>='A'&&*buf<='F') i=*buf-'A'+10;
		else if(*buf>='a'&&*buf<='f') i=*buf-'a'+10;
		else break;
		v=v*16+i; buf++;
	}
	return v;
}

inline char HttpHexStrToChar(char *buf) //turn hex string to char	
{	unsigned int v=0,i,k=2; 
	unsigned char *s=(unsigned char *)buf;

	if((!IS_HEX(s[0]))||(!IS_HEX(s[1]))) return 0; 
	while(k--)
	{	if(IS_DIG(*s)) i=(*s-C_0);
		else if(*s>='A'&&*s<='F') i=*s-'A'+10;
		else if(*s>='a'&&*s<='f') i=*s-'a'+10;
		else break;
		v=v*16+i; s++;
	}
	return ((char)v);
}
   //turn %hh to char
void RidHex(char *in,char *out)
{	char c;

	while(*in)
	{	if(*in=='%'&&(c=HttpHexStrToChar(in+1)))  { *out++=c; in+=3; continue; }
		if(*in=='&'&&!strncmp(in,"&rsquo",6)) {	*out++=C_Q;	J_C(in,'o'); continue; }
		*out++=*in++;
	}
	*out=0; out[1]=0;	
}

//case-insensitive
//rid chars between *str1 and *str2. also rid *str1 and *str2 themselves
//note: cannot rid nested strings
//returns ending NULL of *buf 
char *StrRidBetweenI(char *buf,char *str1,char *str2)
{	int len1=strlen(str1), len2=strlen(str2);
	char *s=buf,*sNew=s,c1[2],c2[2];
	
	c1[0]=c1[1]=*str1; C_LOW(c1[0]); C_UP(c1[1]); 
	c2[0]=c2[1]=*str2; C_LOW(c2[0]); C_UP(c2[1]);
	while(*s)
	{	if((*s!=c1[0]&&*s!=c1[1])||strnicmp(s,str1,len1)) //test start str
		{	*sNew++=*s++; continue; }
	 	
		s+=len1; 
		
		while(*s) //locate end str
		{	if((*s!=c2[0]&&*s!=c2[1])||strnicmp(s,str2,len2)) //test end str
			{	s++; continue; }  // skip until 
			s+=len2;
			break;
		}
	}
	s=sNew; Z4(s);
	return sNew;
}

//case-sensitive, faster for ridding comments "<!--" "-->"
char *StrRidBetween(char *buf,char *str1,char *str2)
{	int len1=strlen(str1), len2=strlen(str2);
	char *s=buf,*sNew=s,c1,c2;
	
	c1=*str1;  
	c2=*str2; 
	while(*s)
	{	if((*s!=c1)||strnicmp(s,str1,len1)) //test start str
		{	*sNew++=*s++; continue; }
	 	
		s+=len1; 
		
		while(*s) //locate end str
		{	if((*s!=c2)||strnicmp(s,str2,len2)) //test end str
			{	s++; continue; }  // skip until 
			s+=len2;
			break;
		}
	}
	s=sNew; Z4(s);
	return sNew;
}

HANDLE fopenTemp(char *name)  //if name has path, use it, if not, add temp path
{  HANDLE handle; //char *s;
   char nameT[MP];
  
   nameT[0]=0;
   if(!name) name=nameT;
   if(!*name) { GetTempPath(MP,name); lstrcat(name,TEXT("\\tempFile.tmp"));	}
   handle=CreateFile(name,GENERIC_WRITE|GENERIC_READ,FILE_SHARE_READ,NULL,
                     CREATE_ALWAYS,FILE_ATTRIBUTE_TEMPORARY,NULL); 
   if(handle==INVALID_HANDLE_VALUE) return 0;
   return handle;
}


HANDLE fopenWin(TCHAR *name, TCHAR *how)  
{  HANDLE handle;
   DWORD mode;
   DWORD method;  int furtherOp=0;

   if(!name||!*name||!how||!*how) return NULL;
   
   if(*how==TEXT('r'))
   {  method=OPEN_EXISTING;	mode=GENERIC_READ; if(how[1]==TEXT('w')) mode|=GENERIC_WRITE; }
   else if(*how==TEXT('w'))
   {  method=CREATE_ALWAYS; mode=GENERIC_WRITE; 
	  
	  if(how[1]==TEXT('r'))	mode|=GENERIC_READ;
	  else if(how[1]==TEXT('w')) {  furtherOp=1; method=OPEN_EXISTING; }
   }
   else return NULL;  //unknown FILE_SHARE_READ	//do not share
   handle=CreateFile(name,mode,FILE_SHARE_READ,NULL,method,0,NULL); 
   if(handle==INVALID_HANDLE_VALUE) return NULL;
   if(furtherOp==1) SetFilePointer(handle,0,0,FILE_END); //prepare for further writing
   return handle;
}

//to read whole file, set cBytes as size of *buffer
int freadWin(void *buffer,int cBytes,HANDLE hfile) //returns bytes read or -1 on error
{  DWORD read=0; BOOL ok;
   //if(cBytes<=0) { cBytes=GetFileSize(fp,0); }
   ok=ReadFile(hfile,buffer,cBytes,&read,NULL);
   if(ok) return read; //bytes acturally read
   return 0;
}

int fwriteWin(void *buffer,int cBytes,HANDLE hfile) //returns bytes written or -1 on error
{  DWORD written=0; BOOL ok;
   if(cBytes<1||!buffer) return 0; //bug: if cBytes<0, as DWORD, it is > GB!

   ok=WriteFile(hfile,buffer,cBytes,&written,NULL);
   if(ok) return written; //bytes acturally written
   return 0;
}


BOOL LibWriteFile(TCHAR *fname,void *buffer,int size)
{  HANDLE fp;  int b;

   if(!size||!buffer) return 0;
   fp=fopenWin(fname,TEXT("w")); if(!fp) return 0;
   b=fwriteWin(buffer,size,fp);	CloseHandle(fp); return b;  
}
int LibReadFileBuffer(TCHAR *fname,void *yourBuf,int size4Less)
{  HANDLE fp=0; int bytes=0; char *s;

   if(!size4Less||!yourBuf||!fname||!*fname) return 0;
   fp=fopenWin(fname,TEXT("r")); 
   if(!fp) return 0;
   bytes=freadWin(yourBuf,size4Less,fp);	
   CloseHandle(fp); 
   s=(char *)yourBuf; s+=bytes;
   Z4(s);
   //zm(s+bytes,4);
   return bytes;
}


// read at most 4*MB bytes from *fname
//*oSize can be NULL if you do not want returned buffer bytes
char *LibReadFile(TCHAR *fname,int *oSize)
{  HANDLE fp=0; DWORD dw; int bytes=0; char *buf,*s; 

   if(oSize) *oSize=0; 
   if(!(fp=fopenWin(fname,TEXT("r")))) return 0;
   dw=GetFileSize(fp,0); 
   if(dw==0xFFFFFFFF) { CloseHandle(fp); return 0; } //no size info
   if(dw>(20*MB)) dw=20*MB;
   buf=GmCx(dw+16); 
   bytes=freadWin(buf,dw,fp);	CloseHandle(fp); 
   if(bytes<=0) { Fm(buf); return 0; }
   s=buf+bytes; Z8(s);
   if(oSize) *oSize=bytes;
   return buf;
}
char *LibReadFileExtra(TCHAR *fname,int *oSize,int extraBytes)
{  HANDLE fp=0; DWORD dw; int bytes=0; char *buf,*s; 

   if(oSize) *oSize=0; 
   if(!(fp=fopenWin(fname,TEXT("r")))) return 0;
   dw=GetFileSize(fp,0); 
   if(dw==0xFFFFFFFF) { CloseHandle(fp); return 0; } //no size info
   if(dw>(4*MB)) dw=4*MB;  dw+=extraBytes;
   buf=GmCx(dw+16); 
   bytes=freadWin(buf,dw-extraBytes,fp);	CloseHandle(fp); 
   if(bytes<=0) { Fm(buf); return 0; }
   s=buf+bytes; Z8(s);
   if(oSize) *oSize=bytes;
   return buf;
}

char *LibReadFileBytesInter(char *fname,LONGLONG start,int *size,char *yourBuf)
{  HANDLE fp;  int bytes=0; char *buffer; 
	LARGE_INTEGER g;

	zm(&g,sizeof(g));

   if(!size||!*size) return 0;
   if(!(fp=fopenWin(fname,TEXT("r")))) return 0;
   
   if(start) LibSeekEx(fp,start);
   
   GetFileSizeEx(fp,&g);
   bytes=*size;	 //if(bytes>(4*MB)) bytes=4*MB;
   
   if(bytes>g.QuadPart) bytes=(int)g.QuadPart;
   if(!yourBuf)	buffer=GmCx(bytes+64);
   else buffer=yourBuf;

   bytes=freadWin(buffer,bytes,fp);	CloseHandle(fp); 
   if(bytes<=0) { *size=0; if(!yourBuf) Fm(buffer); return 0;  }
   *size=bytes;
   zm(buffer+bytes,4);
   return buffer;
}
char *LibReadMax(char *fname,int max)
{	return LibReadFileBytesInter(fname,(LONGLONG)0,&max,0); }

char *LibReadFileBytes(char *fname,int start,int *size)
{	return LibReadFileBytesInter(fname,(LONGLONG)start,size,0); }

char *LibReadFileBuf(char *fname,int start,int *size,char *yourBuf)
{	return LibReadFileBytesInter(fname,(LONGLONG)start,size,yourBuf); }

//simple string to int conversion, used in place for atoi() 
//no overflow or sign check, to speed things up
//used to convert wiki nameSpace index (0 to 15)
inline int WStrToInt(unsigned char *buf)	
{	int v=0;

	while(*buf==C_0) buf++; //skip leading 0s
	while((IS_DIG(*buf))) //loop like this: 520 ==> v=5; v=52; v=520
	{	v=v*10+(int)(*buf-C_0); 
		buf++; 
	}	
	return v;
}
BOOL LibSeekEx(HANDLE fp,LONGLONG pos)
{	LARGE_INTEGER g;
	ZeroMemory(&g,sizeof(g)); g.QuadPart=pos;
	return SetFilePointerEx(fp,g,0,FILE_BEGIN);
}
BOOL LibSeek(HANDLE fp,int pos)
{	return SetFilePointer(fp,pos,0,FILE_BEGIN);
}
char *WstrChar3(char *s,char d)
{	int k; char *start,c;
	while((c=*s))	
	{	//if(c==W_NO||c==W_PRE) { P_NO_EX(s,c); continue; }
		if(c==d)//&&s[1]==c&&s[2]==c) return s; 
		{	k=0; start=s;
			while(*s==c) { s++; k++; }
			if(!(k%3)) return start;
			continue;
		}
		s++;						
	}
	return 0; 
}

char *SpaceToUnder(char *s)	//returns ending NULL
{	char *t=s;
	while(*s)
	{	if(*s==SPACE||*s==TAB||*s=='_') { P_C3(s,SPACE,TAB,'_'); *t++='_'; }
		else *t++=*s++;
	}
	*t=0; return t; 
}
char *UnderToSpace(char *s)	//returns ending NULL
{	char *t=s;
	while(*s)
	{	if(*s==SPACE||*s==TAB||*s=='_') { P_C3(s,SPACE,TAB,'_'); *t++=SPACE; }
		else *t++=*s++;
	}
	*t=0;	return t;
}
void FormatTitle(char *s)
{	char *t=s,c,*ret=s;
	
	if(!s||!*s) return;
	P_C3(s,SPACE,'_',TAB);

	while((c=*s))
	{	if(c==SPACE||c==TAB||c=='_') { P_C3(s,SPACE,TAB,'_'); *t++=SPACE; }
		else if(*s==DF) s++;
		else *t++=*s++;
	}
	*t--=0; if(*t==SPACE) *t=0;

	if((t=strstr(ret,"Special:Search/")))
	{	strcpy(t,t+strlen("Special:Search/"));
	
	}	
}
//cmp *str to many strs, all strs can be either case
//returns 0 if found, 1 if not
int strcmpManyEx(char *str,char *strManyAnyCase)
{
	while(*strManyAnyCase)
	{	if(!stricmp(str,strManyAnyCase)) return 0; 
		N_S(strManyAnyCase);
	}
	return 1; 
}

void BufShift(char **b1,char **b2)
{	char *t;
	t=*b1; *b1=*b2; *b2=t;
}									    


BOOL HasExt(char *fname,char *extStrs)
{	char *s;

	if(!fname||!*fname||!extStrs||!*extStrs) return 0; 
	To_EXT(fname,s,8);
	if(*s==DOT&&!strcmpManyEx(++s,extStrs)) return 1; 
	return 0; 
}

int GetFT(TCHAR *fname)
{ int i;
  i=GetFileAttributes(fname); if(i==-1) return 0;
  if((i&FILE_ATTRIBUTE_DIRECTORY)) return ISDIR; else return ISFILE;
}
char *scA(char *dest,char *src) //returns NULL end of *dest
{
	while(*src) *dest++=*src++;
	*dest=0;
	return dest; 
}
int LibWriteFileOn(char *fname,void *buffer,int size) //append
{  HANDLE fp;  int b;

   if(!size||!buffer) return 0;
   fp=fopenWin(fname,TEXT("ww")); 
   if(!fp) 	 //first write
   {	fp=fopenWin(fname,TEXT("w"));	
		if(!fp)  return 0;
	}
   b=fwriteWin(buffer,size,fp);	CloseHandle(fp); return b;  
}

//if fnSub==NULL, opens root key
//if bCreate==1, open existing key or create it if none
// bCreate==0, open existing, if none, fails
HKEY RegOpen(char *fnRoot,char *fnSub,BOOL bCreate) 
{	HKEY hk=0;										
	char tp[MP],*s=tp;
	DWORD dw;


	s=scA(s,"software\\"); 
	s=scA(s,fnRoot);
	
	if(fnSub&&*fnSub) 
	{	*s++=DF;
		scA(s,fnSub);
	}
	if(bCreate)
		RegCreateKeyEx(HKEY_LOCAL_MACHINE,tp,0,0,0,KEY_READ|KEY_WRITE,0,&hk,&dw);	
	else
		RegOpenKeyEx(HKEY_LOCAL_MACHINE,tp,0,KEY_READ|KEY_WRITE,&hk);
	return hk;
}

BOOL RegReadInt(char *fn,char *fnSub,char *label,int *oValue)
{	HKEY hk=0; int b; DWORD len=sizeof(int);
	
	*oValue=0;
	if(!(hk=RegOpen(fn,fnSub,0))) return 0; 
	b=RegQueryValueEx(hk,label,0,0,(BYTE*)oValue,&len);
	RegCloseKey(hk);
	if(b==ERROR_SUCCESS) return 1; 
	return 0; 
}

BOOL RegReadStr(char *fn,char *fnSub,char *label,char *oStr)
{	HKEY hk=0; int b; DWORD len=MP-1;
	
	*oStr=0;
	if(!(hk=RegOpen(fn,fnSub,0))) return 0; 
	b=RegQueryValueEx(hk,label,0,0,(BYTE*)oStr,&len);
	RegCloseKey(hk);
	if(b==ERROR_SUCCESS) return 1; 
	return 0; 
}

//only use with max>0
DWORD WRand(int max) 
{	DWORD dw;
	dw=GetTickCount() * 214013L + 2531011L;
	if(max>0) return (dw%max);
	else return rand();
}

void WWriteTest(char *fn,char *buf)	//write test file
{	char t[MP]; 
	static int cnt;
	if(!buf) return ;							
	WAddPath(fn,t);
	wsprintf(t+strlen(t),"_%d.txt",++cnt);
	
	LibWriteFile(t,buf,strlen(buf));
}

char *RidExt(char *fname)
{  char *s;
   To_EXT(fname,s,8); 
   if(*s==DOT) *s=0;
   return s;
}

char *ExtReplace(char *fname,char *extNew)
{  	char *s;
	s=RidExt(fname); 
   strcpy(s,extNew);
   return fname;
}

 char *Gcpy2(char *s,char *s1,char *s2)
{
	S_CPY(s,s1); S_CPY(s,s2);
	return s; 
}
 char *Gcpy3(char *s,char *s1,char *s2,char *s3)
{
	S_CPY(s,s1); S_CPY(s,s2); S_CPY(s,s3);
	return s; 
}
 char *Gcpy4(char *s,char *s1,char *s2,char *s3,char *s4)
{	if(s1)
	S_CPY(s,s1); S_CPY(s,s2); S_CPY(s,s3);	S_CPY(s,s4);
	return s; 
}				

char *Gcpy5(char *s,char *s1,char *s2,char *s3,char *s4,char *s5)
{	s=Gcpy4(s,s1,s2,s3,s4); S_CPY(s,s5);
	return s; 
}				

char *Gcpy6(char *s,char *s1,char *s2,char *s3,char *s4,char *s5,char *s6)
{	s=Gcpy5(s,s1,s2,s3,s4,s5); S_CPY(s,s6);
	return s; 
}				

char *Gcpy7(char *s,char *s1,char *s2,char *s3,char *s4,
	char *s5,char *s6,char *s7)
{	s=Gcpy6(s,s1,s2,s3,s4,s5,s6); S_CPY(s,s7);
	return s; 
}				
//rid both starting and ending spaces, tabs, linebreaks
int RidSpace(char *in)
{  char *start;  
   
   if(!in||!*in) return 0;
   start=in;  P_SPACE(start); 
   if(!*start) { *in=0; return 0; } //all are spaces
   if(start!=in) memmove(in,start,strlen(start)+1);	//rid starting spaces
   start=in; To_L(start);
   while(IS_SPACE(*start)) start--;
   *(++start)=0;
   return (start-in);
}

char *WReadDump(HANDLE fpD,LONGLONG pos,int bytes) //read dump text
{	char *buf;

	if(!fpD) return 0; 
	if(!LibSeekEx(fpD,pos)) return 0; 
	buf=GmCx(bytes+100);// buf[0]=SPACE;
	ZeroMemory(buf+freadWin(buf,bytes,fpD),96);
	return buf; 
}
HANDLE WOpenDumpFile(TCHAR *fname)
{	HANDLE fp;
	fp=CreateFile(fname,GENERIC_READ,FILE_SHARE_READ,0,
						OPEN_EXISTING,FILE_FLAG_RANDOM_ACCESS,0);
	if(fp==INVALID_HANDLE_VALUE) return 0; 
	return fp;
}
void WWriteTitles(char *buf,int *off,int max)
{	int i;
	LogInt("========= Existing titles",max);
	if(max>20) max=20;
	LogInt("Showing",max);
	for(i=0;i<max;i++)	 Log(buf+off[i]);
	Log("============");
}



//all these mark functions can be done in one function
//by using strstr() or strchr() to locate the start and end tags. 
//but that may be a bit slower

// for {{ ... }},  [[ ... ]] so on
int WMarkDeep2(char *buf,int *offS,int *offE,char cS,char cE,int max)
{	int cnt=0,bStarted=0; 
	char *s=buf;

	while(*s)
	{	if(*s==cS&&s[1]==cS)
		{	offS[cnt]=s-buf; 
			s+=2;  
			bStarted=1;
			continue;
		}
		if(*s==cE&&s[1]==cE)
		{	if(!bStarted)  {  s+=2; continue; }
			bStarted=0;

			offE[cnt]=s-buf; 
			s+=2; cnt++; 
			if(cnt>=max) break;
			continue;
		}
		s++;
	}
	return cnt;
}
	   //for tables, {|
int WMarkDeepStr2(char *buf,int *offS,int *offE,char *cS,char *cE,int max,int bLB)
{	int cnt=0,bStarted=0; 
	char *s=buf;
	
	if(bLB)
	{	while(*s)  //when marking tables, {| must follow LB
		{	if(*s!=LB) { s++; continue; }	
			P_SPACE(s);	
		
			if(*s==cS[0]&&s[1]==cS[1])
			{	offS[cnt]=s-buf; 
				s+=2;  
				bStarted=1;
				continue;
			}

			if(*s==cE[0]&&s[1]==cE[1])
			{	if(!bStarted)  {  s+=2; continue; }
				bStarted=0;

				offE[cnt]=s-buf; 
				s+=2; cnt++; 
				if(cnt>=max) break;
				continue;
			}
			s++;
		}
		return cnt;
	}

	while(*s)
	{	if(*s==cS[0]&&s[1]==cS[1])
		{	offS[cnt]=s-buf; 
			s+=2;  
			bStarted=1;
			continue;
		}
		if(*s==cE[0]&&s[1]==cE[1])
		{	if(!bStarted)  {  s+=2; continue; }
			bStarted=0;

			offE[cnt]=s-buf; 
			s+=2; cnt++; 
			if(cnt>=max) break;
			continue;
		}
		s++;
	}
	return cnt;	
}
		 //arbitrary strs, like tags <includeonly> </includeonly>
int WMarkDeepStr(char *buf,int *offS,int *offE,char *cS,char *cE,int max)
{	int cnt=0,bStarted=0,lenS=strlen(cS),lenE=strlen(cE); 
	char *s=buf;

	while(*s)
	{	
		if(*s==cS[0]&&!strncmp(s,cS,lenS))
		{	offS[cnt]=s-buf; 
			s+=lenS;  
			bStarted=1;
			continue;
		}
		if(*s==cE[0]&&!strncmp(s,cE,lenE))
		{	if(!bStarted)  {  s+=lenE; continue; }
			bStarted=0;

			offE[cnt]=s-buf; 
			s+=lenE; cnt++; 
			if(cnt>=max) break;
			continue;
		}
		s++;
	}
	return cnt;
}
		//1 single char, like [ ... ]
int WMarkDeep(char *buf,int *offS,int *offE,char cS,char cE,int max)
{	int cnt=0,bStarted=0; 
	char *s=buf;

	while(*s)
	{	if(*s==cS&&s[1]!=cS)
		{	offS[cnt]=s-buf; 
			s+=2;  
			bStarted=1;
			continue;
		}

		if(*s==cE&&s[1]!=cE)
		{	if(!bStarted)  {  s+=2; continue; }
			bStarted=0;

			offE[cnt]=s-buf; 
			s+=2; cnt++; 
			if(cnt>=max) break;
			continue;
		}

		s++;
	}
	return cnt;
}


/*	this risky function is to allow missing tag, like this one in {{Infobox language}}
{{	   // 2 braces
qif
 |test=
 |then=''unknown''
 |else=
 
 {{	  // 2 braces
 
 qif
  |test=
  |then=[[constructed language]]
  |else=[[Indo-European languages.Indo-European]]
 
 }	 // a single brace !!!

<br />&nbsp;[[Germanic languages<br />&nbsp;&nbsp;
[[West Germanic languages<br />&nbsp;&nbsp;&nbsp;[[Anglo-Frisian languages<br />
&nbsp;&nbsp;&nbsp;&nbsp;[[Anglic languages<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'''English'''

}}}	//  triple braces !!!

*/
int WMarkDeep2Risky(char *buf,int *offS,int *offE,char cS,char cE,int max)
{	int cnt=0,bStarted=0;  int kS=0;
	char *s=buf;
	
	while(*s)
	{	if(*s==cS&&s[1]==cS)
		{	offS[cnt]=s-buf; 
			s+=2;  	 kS++;
			bStarted=1;	
			continue;
		}

		if(*s==cE&&s[1]==cE)
		{	if(!bStarted)  {  s+=2; continue; }
			bStarted=0;

			offE[cnt]=s-buf; 
			s+=2; cnt++;  
			if(cnt>=max) break;
			continue;
		}
		if(*s==cE&&bStarted) // single ending '}' and we got starting tag  
		{	bStarted=0;

			offE[cnt]=s-buf; 
			s+=1; cnt++;  
			if(cnt>=max) break;
			continue;
		}
		s++;
	}
	return cnt;
}

inline int MarkCnt(char *s,char cE)
{	int k=0;
	while(*s==cE) { s++; k++; }
	return k;

}
// {{{{{aaa}}bb}}}	==>{{{  {{ aaa }}bb }}}
// {{{{{aaa}}}bb}}  ==> {{ {{{aaa}}} bb}}
// {{{ aa {{ bb }}}}} ==> {{{ aa {{bb}} }}}
// Zh-stp: {{zh-s|{{{s}}}}}; {{zh-t|{{{t}}}}}; {{zh-p|{{{p}}}}}
int WMarkDeep3(char *buf,int *offS,int *offE,char cS,char cE,int max)
{	int cnt=0,bStarted=0,k,j;  
	char *s=buf,*t,*tp;

	while(*s)
	{	if(*s==cS&&s[1]==cS&&s[2]==cS)
		{	
			offS[cnt]=s-buf; 
			k=(MarkCnt(s,cS))%3;
			if(k)
			{	t=s+3; P_C(t,cS); To_C(t,cE);
				if(MarkCnt(t,cE)>=3)   // {{ {{{aaa}}} bb }} ==> pass first "{{"
				{	s+=k; offS[cnt]+=k; }
			}
			s+=3;  
			bStarted=1;
			continue;
		}

		if(*s==cE&&s[1]==cE&&s[2]==cE)
		{	if(!bStarted)  {  s+=3; continue; }
			bStarted=0;

			offE[cnt]=s-buf; 
			 
			k=(MarkCnt(s,cE))%3;
			if(k)  
			{	t=s;  tp=buf+offS[cnt]+3;	P_C(tp,cS);
				BACK_C(tp,t,cS);
				if(*t==cS)	   //{{{ aa {{bb}} }}} ==> pass first "}}" 
				{	j=0; while(*t==cS) { t--; j++; }
					if(j<3) 
					{	s+=k; offE[cnt]+=k; }
				}
			}
			s+=3; 
			cnt++;
			if(cnt>=max) break;
			continue;
		}
		s++;
	}
	return cnt;
}



