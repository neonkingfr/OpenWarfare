#include "WF_Main.h"

//parse wiki and external links
//because we have different languages and different sites
//to ensure that "abc" is for English pedia, not French quote
//have to prefix with site and language tags,
//complication is: many interwiki and inter-language links
//already contains1 or 2 tags. there are also hackish wiki shorthands.
//link parsing is quite messy 

char szHref[]="<a href=\"";
char szHrefWiki[]="<a href=\"/wiki/";
int linkCount;
extern int iDomain,iLang,iNS;
extern int bTest;
extern WL_T *glt;
extern char *WCpyDmShort(char *sNew);
STR_T nsStrE; //english ns

inline char *sc4in(char *sNew,char *s1,char *s2,char *s3,char *s4)
{	*sNew=0;
	if(s1) S_CPY(sNew,s1);	 
	if(s2) S_CPY(sNew,s2); 
	if(s3) S_CPY(sNew,s3); 
	if(s4) S_CPY(sNew,s4); 
	return sNew;
}
inline char *sc2in(char *sNew,char *s1,char *s2)
{	*sNew=0;
	if(s1) S_CPY(sNew,s1);	 
	if(s2) S_CPY(sNew,s2); 
	return sNew;
}
inline char *sc3in(char *sNew,char *s1,char *s2,char *s3)
{	*sNew=0;
	if(s1) S_CPY(sNew,s1);	 
	if(s2) S_CPY(sNew,s2); 
	if(s3) S_CPY(sNew,s3); 
	return sNew;
}
extern void StrFreeNoBuf(STR_T *str);

inline char *scAin(char *dest,char *src) //returns NULL end of *dest
{
	while(*src) *dest++=*src++;
	*dest=0;
	return dest; 
}

char szSiteLong[MAX_SITE][32]=
{	"wikipedia:","wiktionary:",
	"wikiquote:","wikibooks:",
	"wikinews:",					 //"Wikimedia Commons:"
					
	"meta:","wikisource:","commons:","wikispecies:"
};	//5: meta is english only, 7: commons is english only 

char szSiteShort[MAX_SITE][8]
=	 //short for dict is "wikt:"
{	"w:","d:","q:","b:","n:", 
	"m:", "s:","c:","e:"
};


char szNameSpaceE[]= //english ns
	" :\0talk:\0"	 //1
	"user:\0user talk:\0" //2
	"wikipedia:\0wikipedia talk:\0"	//5	
	"image:\0image talk:\0"	   //7
	"mediawiki:\0mediawiki talk:\0"	//9	
	"template:\0template talk:\0"  //11
	"help:\0help talk:\0"	  //13
	"category:\0category talk:\0" //15
	"portal:\0portal talk:\0\0"; //17

static char szLangPrefix[8];

void ExitNSE()
{	StrFreeNoBuf(&nsStrE); 
	nsStrE.buf=0;
}

extern int iMaxLang;
BOOL IsEn()
{	return glt[iLang].bEn; }

int GetEnglishIndex()
{	int i;

	for(i=0;i<iMaxLang;i++)
	{	if(glt[i].bEn) return i; }
	return 0;  //default
}

char *GetSiteStrLong(int site)
{	if(site<0||site>=MAX_SITE) site=0;
	return szSiteLong[site]; 
}

char *GetSiteStrShort(int site)
{	if(site<0||site>=MAX_SITE) site=0;
	return szSiteShort[site]; 
}

char *GetLangStr(int lang)
{	return glt[lang].name;	 }

static char szEmpty[2];

char *GetWebStr()
{	return glt[iLang].web[iDomain];	}

char *GetNSStr(int ns)
{	STR_T *str=glt[iLang].nsStr+iDomain;

	if(ns<=0||ns>=str->cnt) return szEmpty;
	return str->buf+str->off[ns]; 	
}
int GetNSCnt()
{	return glt[iLang].nsStr[iDomain].cnt; }

char *GetLangStrCur()
{	return glt[iLang].name;	 }

char *GetSiteStrCur()
{	return szSiteShort[iDomain]; }

char *GetNSStrCur()
{	return GetNSStr(iNS);
}

char *PrefixLinkCur(char *link,char *out)
{	if(!link||!out) return 0; 
	return sc4in(out,szSiteShort[iDomain],glt[iLang].name,GetNSStrCur(),link);
}

char *PrefixLinkCurNoNs(char *link,char *out)
{	return sc3in(out,szSiteShort[iDomain],glt[iLang].name,link);	}

char *PrefixLink(char *link,char *out,int lang,int site,int ns)
{	return sc4in(out,szSiteShort[site],glt[lang].name,GetNSStr(ns),link);	}


extern int iMaxLang;

int GetLangIndex(char *s)
{	int i;

	for(i=0;i<iMaxLang;i++)
	{	if(!strnicmp(s,glt[i].name,3)) return i; }
	return -1;
}

int GetSiteIndex(char *s)
{	char low[20],*t=s,*tp; int i;

	To_CN(t,':',14);
	if(*t!=':') return -1; 
	t++;
	tp=low; S_LOWN(tp,s,t-s); 
	for(i=0;i<MAX_SITE;i++)
	{	if(!strcmp(low,szSiteLong[i])) return i;  }
	if(!strcmp(low,"wikt:")) return 1;
	return -1;
}
extern char *UtfLow(char *in,char *out);

int GetNSIndex(char *s)
{	char low[MAX_NS_LEN],*t,c; int i;
	
	i=glt[iLang].nsStr[iDomain].maxLen+4;
	P_SPACE1(s,':'); t=s;
	To_CN(t,':',i);
	if(*t!=':') return -1;	 
	t++; c=*t; *t=0; UtfLow(s,low);	*t=c;
	
	i=StrSearch(glt[iLang].nsStr+iDomain,low);	
	
	if(i<0&&!IsEn()) i=StrSearch(&nsStrE,low);
	return i;
}
	   //get ns index directly from glt
	   //when loading a new title, if it's lang or site different,
	   //it's ns str may be different, so 
	   //cannot use current ns str to match it
int GetNSIndexRaw(char *s,int lang,int site)
{	int i;
	int langOrg=iLang,siteOrg=iDomain;

	if(lang>=0) iLang=lang;
	if(site>=0) iDomain=site;

	i=GetNSIndex(s);
	iLang=langOrg; iDomain=siteOrg;
	return i;
}

int StripNS(char *temp)
{	char *s=temp; int ns;
	
	P_SPACE1(s,':');
	ns=GetNSIndex(s);
	if(ns<=0) return 0; 

	To_C(s,':'); P_SPACE1(s,':');
	scA(temp,s);
	return ns;
}

extern char *GetLangName(int i);

void GetFullLinkDef(char *linkBare,FULL_LINK_T *ft)
{	if(!ft->site) ft->site=GetSiteStrCur();
	if(!ft->lang) ft->lang=GetLangStrCur();
	if(!ft->ns) ft->ns=szEmpty;//=GetNSStr(0);
	if(!ft->bare&&linkBare) ft->bare=linkBare;
}

 //loop call until returns 0
 //zero *ft before loop calling
BOOL GetFullLinkStr(char *link,FULL_LINK_T *ft,char **pLink)
{	char *t=link,*s; int i;

	P_SPACE1(link,':'); 
	*pLink=link;
	To_C(t,':'); 
	if(!*t) return 0; 
	t++;
	i=t-link;
	if(i==3)  //2-char, assume language prefix
	{	if(ft->lang) return 0; //lang already specified, so no more prefixes
		s=szLangPrefix; S_CPYPOS(s,link,t);
		ft->lang=szLangPrefix;
		P_SPACE1(t,':'); *pLink=t; 
		return 1; 
	}
	else if(i==2) //1-char, assume short site name
	{	if(ft->site) return 0; 	//already got it, so this is no long prefix
		for(i=0;i<MAX_SITE;i++)				
		{	if(*link==szSiteShort[i][0]) 
			{	ft->site=szSiteShort[i]; break; }
		}
		if(!ft->site) return 0;	//not site name, no more prefix
		P_SPACE1(t,':');  *pLink=t; 
		return 1; 
	}

	if(!ft->ns)	//test ns
	{	i=GetNSIndex(link);	
		if(i>=0) 
		{	ft->ns=GetNSStr(i); 
			ft->xns=i;
			P_SPACE1(t,':');  *pLink=t;
			return 1; 
		}
	}	

	if(!ft->site) //test site long name
	{	i=GetSiteIndex(link);	
		if(i>=0) 
		{	ft->site=szSiteShort[i]; 
			P_SPACE1(t,':');  *pLink=t;
			return 1; 
		}
	}	
	return 0; 
}
//extern int iDomain,iLang;

//this function is called when loading a title
BOOL GetFullLinkIndex(char *link,FULL_LINK_T *ft,char **pLink)
{	char *t=link; int i;

	P_SPACE1(link,':'); 
	*pLink=link;
	To_C(t,':'); 
	if(!*t) return 0; 
	t++;
	i=t-link;
	if(i==3)  //2-char, assume language prefix
	{	if(ft->xlang>=0) return 0; //lang already specified, so no more prefixes
		ft->xlang=GetLangIndex(link);
		P_SPACE1(t,':'); *pLink=t; 
		return 1; 
	}
	else if(i==2) //1-char, assume short site name
	{	if(ft->xsite>=0) return 0; 	//already got it, so this is no long prefix
		for(i=0;i<MAX_SITE;i++)				
		{	if(*link==szSiteShort[i][0]) 
			{	ft->xsite=i; break; }
		}
		if(ft->xsite<0) return 0;	//not site name, no more prefix
		P_SPACE1(t,':');  *pLink=t; 
		return 1; 
	}
	if(ft->xns<0)	//test ns first, so	[[Wikisource:Fiction|Fiction]]
	{				//will lead to wikisource's project ns
			 //get ns index directly from glt, in case this title's lang or site
			 //is different from current 
		ft->xns=GetNSIndexRaw(link,ft->xlang,ft->xsite);
		if(ft->xns>=0) 
		{	P_SPACE1(t,':');  *pLink=t;
			return 1; 
		}
	}	
	if(ft->xsite<0) //test site long name
	{	ft->xsite=GetSiteIndex(link);	
		if(ft->xsite>=0) 
		{	P_SPACE1(t,':');  *pLink=t;
			return 1; 
		}
	}	
	return 0; 
}

//returns bare pageName
//pass *oLang, *oSite, *oNs as NULL if no need
char *GetLinkInfo(char *link,int *oLang,int *oSite,int *oNs)
{	FULL_LINK_T ftt, *ft=&ftt;	//int lang
	
	ft->xlang=-1; ft->xns=-1; ft->xsite=-1;

	while(GetFullLinkIndex(link,ft,&link));
	
	if(ft->xlang>=0&&oLang) *oLang=ft->xlang;
	if(oNs) 
	{  if(ft->xns>=0) *oNs=ft->xns;
		else *oNs=0;
	}	 
	if(ft->xsite>=0&&oSite)
	{	*oSite=ft->xsite;
		if(*oSite==5||*oSite==7) //meta or commons, change to english
		{	if(*oLang) *oLang=GetEnglishIndex(); }
	} 
	return link;
}

void GetFullLink(char *link,FULL_LINK_T *ft)
{	zm(ft,sizeof(FULL_LINK_T));
	while(GetFullLinkStr(link,ft,&link)); 
	GetFullLinkDef(link,ft); 
}

extern char *WToEnd(char *s,char cStart,char cEnd);

//*in *out can be same buf
//returns end of *out
extern char *UtfUp(char *in,char *out); 
BOOL IsNSCategory(char *ns)
{	if(!ns) return 0; 
	if(!strcmp(ns,GetNSStr(14))) return 1; 
	if(!strcmp(ns,"category:")) return 1; 
	return 0; 
}

char *CombineLink(FULL_LINK_T *ft,char *sNew,char *tail,int bCat)
{	char *t; int bCur;
	
	if(ft->bare[0]=='#') bCur=1;
	else  bCur=0;
	
	t=ft->bare;	
	To_C(t,'|'); 

	if(*t) 
	{	*t=0; W_RE(ft->bare,t);	//for category links, ignore |
		N_S(t); P_SPACE1(t,'*'); 
	}
	else W_RE(ft->bare,t); 

	if(!bCur) //wiki link
	{	sNew=scAin(sNew,"<a href=\"");
		sNew=sc3in(sNew, GWikiFilesList.GetWikiURLRoot().MutableData(), ft->ns, ft->bare);
	}
	else { sNew=sc2in(sNew,"<a href=\"",ft->bare); }  //in page link

	*sNew++=C_QQ; *sNew++='>';

	if(!*t)	t=ft->bare; //use link as text if no alt text
	else		//[[:Category:abc|a]] ==> "a"
				//[[Category:abc|a]] ==> "abc"
	{	if(bCat) t=ft->bare;
		
	}
	 sNew=sc3in(sNew,t,tail,"</a>");
	return sNew;
}

char *GetTail(char *s,char *out)
{	int k=38;

	while(k--&&(*s==C_Q||IS_A(*s)||IS_DIG(*s))) *out++=*s++;
	*out=0;
	return s;
}

char *ParseLinkWiki(char *s,char *sNew,char *tail,int *bCat, CategoryList *categoryList)
{	FULL_LINK_T ft;	int bEsc;

	zm(&ft,sizeof(ft)); ft.xns=-1; //use this

	P_SPACE1(s,'[');//,':'); 
	if(!*s) return sNew;
	
	if(*s==':')	// : escape
	{	bEsc=1;	P_SPACE1(s,':'); }
	else bEsc=0;

	if(*s=='/')
	{	P_SPACE1(s,'/');
		ft.ns=GetNSStrCur();
		ft.bare=s; 
		GetFullLinkDef(0,&ft);
	}
	else if(*s=='#')  ft.bare=s; //in page or current dir
	else  GetFullLink(s,&ft);

	if(!bEsc&&ft.xns==14)
  {
    *bCat=1;
    categoryList->AddCategory(ft.bare);
  }
	else *bCat=0;
	return CombineLink(&ft,sNew,tail,*bCat);
}

BOOL IsWeb(char *dir) 
{  	if(!dir) return 0; 
	P_SPACE(dir);
	if(!strnicmp(dir,"news:",5)) return 1;
	To_CN(dir,':',10); 
	if(*dir==':'&&dir[1]=='/'&&dir[2]=='/') return 1; 
	return 0;   
}

char *ParseLinkWeb(char *s,char *sNew,int *index)
{	char *t=s;
	
	To_C2(t,SPACE,'|');	 // if has link text (after space or |), *t at its start
	if(*t) { *t=0; W_RE(s,t); N_S(t); P_SPACE2(t,'|',SPACE); }
	else W_RE(s,t);
	
	sNew=scAin(sNew,"<a href=\"");
	sNew=scAin(sNew,s);
	*sNew++=C_QQ; *sNew++='>'; 

	if(*t) sNew=sc2in(sNew,t,"</a>"); //use link text
	else
	{	wsprintf(sNew," ([%d] ",*index);	*index+=1; To_0(sNew); 
		To_WN_EX(s,t);
		if(strlen(t)<40)
			sNew=sc2in(sNew,t,"</a>");	//use link address
		else
		{	S_CPYN(sNew,t,36);
			sNew=scAin(sNew," ...</a>");
		}
		*sNew++=')'; *sNew++=SPACE; 
	}
	return sNew;
}
extern int HasExt(char *,char *);
extern char *UtfLowN(char *in,char *out,int cnt); //*in *out can be same buf

BOOL IsLinkImg(char *s)
{	char low[MAX_NS_LEN],*t=s;  int i;  char c;

	P_SPACE1(t,':'); 
	i=glt[iLang].nsStr[iDomain].maxLen+4;
	To_CN(t,':',i);
	if(*t!=':') return 0;	 
	t++; c=*t; *t=0; UtfLow(s,low);	*t=c;
	if(!strcmp(low,GetNSStr(6))) return 1; 
	if(IsEn()) return 0; 

	if(!strcmp(low,"image:")) return 1;
	return 0; 
}


extern int *offSPre,*offEPre;
extern void BufShift(char **b1,char **b2); 
extern int WMarkDeep2(char *buf,int *offS,int *offE,char cS,char cE,int max);
extern int WMarkDeep(char *buf,int *offS,int *offE,char cS,char cE,int max);
extern int ParseGallery(char *s,char *sNew);
extern char *ParseImgOne(char *s,char *sNew);

int IsInTag(char *s)
{	To_CN2(s,'<','>',500);
	if(*s=='>') return 1;
	return 0;  
}
extern char *UtfUp1(char *inOut);

char *LinkGetCatName(char *s)
{	scLess1(s,GetNSStr(14));
	UtfUp1(s);
	To_0(s);
	return s;
}


int ParseLink(char *bufOld,char *bufNew,int bDoImg, CategoryList *categoryList)
{	char *s,*sNew=0,*tp;
	int k,i,bytes,cLoop;
	char *b1,*b2,*t,*tail=bufOld+SizeMem(bufOld)-200;
	int bCat,cntCat;
	char *catStart,*catCur;
	int index=1;

	
	b1=bufOld; b2=bufNew; 
	
 	if(strstr(bufOld,"<gallery>"))
	{	ParseGallery(b1,b2);
		BufShift(&b1,&b2);
	}
	
	if(strstr(b1,"[["))
	{	cLoop=4;  //actually 2 is enough: [[Image:aa.jpg|[[bb]]]]
		
		while(cLoop--) 
		{	
			if(!(k=WMarkDeep2(b1,offSPre,offEPre,'[',']',MAX_OFF-100))) break;
			
			s=b1; sNew=b2; 	cntCat=0;
			catStart=b1+offEPre[k-1]; To_0(catStart); Z4(catStart); catCur=catStart;	
			for(i=0;i<k;i++)
			{	
				bytes=offSPre[i]-(s-b1);
				memcpy(sNew,s,bytes); 
				sNew+=bytes; s+=bytes;
			
				t=b1+offEPre[i]; //terminate this temp
				*t=0; tp=GetTail(t+2,tail);  W_RE(b1,t);
			
				s+=2; 


				 //do img and wiki link simultaneously even if 
				 //	wiki links nested within img link
				 //	the trick is: we always parse the inner-most first
				if(IsLinkImg(s))
				{	if(bDoImg) sNew=ParseImgOne(s,sNew); }
				else
				{	t=sNew; 
					sNew=ParseLinkWiki(s,sNew,tail,&bCat, categoryList); *sNew=0;	 
					if(bCat)
					{  sNew=t; S_CPY(catCur,t); catCur++; cntCat++;	}
				}
				s=tp; 
			}
			S_CPY(sNew,s);
			
			if(catCur!=catStart)
			{	
				sNew=scA(sNew,	 // 
          "\r\n\r\n<hr><div id=\"catlinks\" style=\"width: 90%;\"><p class=\"catlinks\">"
			    //"<span style=\"float: right; margin: 4px 10px 4px 4px\">"
			    //"<a href=\"#ArticleTop\">Up</a></span>" 
			    "<a name=\"CategoryStart\"></a><b>");
				sNew=LinkGetCatName(sNew);
				sNew=scA(sNew,"</b><p>");
				for(i=0;i<cntCat;i++)
				{	if(i) sNew=scA(sNew," | ");
					sNew=scA(sNew,"<span>");
					S_CPY(sNew,catStart); catStart++;
					sNew=scA(sNew,"</span>");
				}
				sNew=scA(sNew,"</p></p></div>"); 
				/*
				t=sNew;
				sNew=scA(sNew,
			"<span style=\"float: right; margin: 4px 10px 4px 4px\">"
			"<a href=\"#CategoryStart\">");
				sNew=LinkGetCatName(sNew);
				sNew=scA(sNew,"</a></span>");
				k=sNew-t; //categoy bytes
				i=sNew-b2;	//text+category bytes
				memmove(b2+k,b2,i);	//move all down
				memmove(b2,sNew,k); //move category at start
				Z4(sNew); //discard category block
			  	  */
			}

			BufShift(&b1,&b2); 
		}
	}
	
	if(strchr(b1,'['))  
	{	cLoop=2;  
		while(cLoop--) //avoid infinite recursion
		{	
			if(!(k=WMarkDeep(b1,offSPre,offEPre,'[',']',MAX_OFF))) break;
		
			s=b1; sNew=b2; 
			for(i=0;i<k;i++)
			{	
				bytes=offSPre[i]-(s-b1);
				memcpy(sNew,s,bytes); 
				sNew+=bytes; s+=bytes;
			
				t=b1+offEPre[i]; //terminate this temp
				*t=0;  W_RE(b1,t);
			
				s+=1; P_SPACE2(s,':','[');

				if(IsWeb(s))
				{	if(!IsInTag(b1+offEPre[i]+1))	
						sNew=ParseLinkWeb(s,sNew,&index);  
				}
				else  //not web link
				{  	*sNew++='['; sNew=scA(sNew,s); *sNew++=']';	}
				s=b1+offEPre[i]+1; 
			}
			S_CPY(sNew,s);
			BufShift(&b1,&b2);
		}					  
	}
	if(sNew) Z4(sNew);
	
	if(bTest)
		WWriteTest("ParseLink",b1);

	if(b1!=bufNew) return 0; 
	return 1; 
}

	
	




