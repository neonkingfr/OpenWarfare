//parse templates
//supports new style: use of default value (not very well though)
//currently, dates are only supported for English
//ie, Today's featured article/2005,July, 15
// other languages may not work if in local format 
#include "WF_Main.h"



#define MAX_LABEL	500
#define MAX_BR_BUF	(400*KB)

extern BOOL bTest;

typedef struct
{	char label[100]; //must use buf: if no label, has to store ints
	char *value;
}INFO_TEMP_T;  



typedef struct
{	char *rBuf1,*rBuf2;
	char *temp1,*temp2;
	int *offS,*offE;
	int *rOffS,*rOffE;
	INFO_TEMP_T *it;
}TR_T;

extern char *TempGetFixed(char *fn);
extern char *GetTemplateInter(char *tn,char *tnBack,BOOL *bFree);



extern void PipeReplaceLink(char *s);


void TempGetMem(TR_T *tt)
{	int k;
	
	if(tt->rBuf1) return; 
				 //get one mem block
	k=MAX_BR_BUF*2+MAX_LABEL*sizeof(INFO_TEMP_T)+MAX_OFF*2*sizeof(int);
	
	tt->rBuf1=GmC(k);
	tt->rBuf2=tt->rBuf1+MAX_BR_BUF;
	
	tt->temp1=tt->rBuf1+MAX_BR_BUF-300;
	tt->temp2=tt->rBuf2+MAX_BR_BUF-300;
	
	tt->offS=(int*)(tt->rBuf2+MAX_BR_BUF); //these offs must use local 
	tt->offE=tt->offS+MAX_OFF;		   //buf, because offSPre 
									   //is used simutaneously by PreParse()
	tt->rOffS=tt->offS+MAX_OFF-300;
	tt->rOffE=tt->offE+MAX_OFF-300;

	tt->it=(INFO_TEMP_T *)(tt->offE+MAX_OFF); 
}

void TempFreeMem(TR_T *tt)
{	Fm(tt->rBuf1);  }


// *s is past temp name and its pipe, and ends with NULL
int TempMark(char *s,INFO_TEMP_T *it)
{	int cnt=0,add=0; char *start,*tp;
	
	while(*s)
	{	start=s;
		To_C2(s,'|','=');

		if(*s=='=') //label can be empty, *value can be NULL
		{	tp=it[cnt].label; S_CPYPOSN(tp,start,s,98);
			if(RidSpace(it[cnt].label))
				it[cnt].value=s+1;
			else it[cnt].value=0;
			To_C(s,'|');
			if(*s) *s++=0;
			
		}
		else 
		{	wsprintf(it[cnt].label,"%d",++add);
			it[cnt].value=start; 
			if(*s) *s++=0;
		}
			  //ignore spaces of value, but do not ignore if followed by list chars
			  // or table chars {|, |}, \n|
		if(it[cnt].value&&it[cnt].value[0])
		{	tp=it[cnt].value;
			P_SPACE(tp);
							
			if(*tp)		   //not all spaces, and not list chars, ignore spaces
			{	if(*tp!='*'&&*tp!='#'&&*tp!=':'&&*tp!='|'&&*tp!='{'&&*tp!='}')	 
				{	it[cnt].value=tp; }	
				To_0(tp); W_RE(it[cnt].value,tp);
			}
		}
		cnt++;
		if(cnt>=MAX_LABEL) cnt=MAX_LABEL-1; //do not overflow!
	}
	return cnt;
}

static int bBraceTest;
//pass cnt=0 if no labels, in text, but {{{ }}} in template
//then only check default
char *TempReplace(char *label,char *sNew,INFO_TEMP_T *it,int cnt)
{	int i; char *tp,*pipe; 
	
	P_SPACE1(label,'|');  if(!*label) return sNew;
	pipe=label;

	To_C(pipe,'|'); // {{{value | default}}} ?
	if(*pipe) { *pipe++=0;	P_SPACE(pipe); }
	
	if(!*pipe&&!cnt) return sNew;	//no label and no default

	tp=label; To_0(tp); W_RE(label,tp);	 

	for(i=cnt-1;i>=0;i--)	 //check from last to first, note: if no label
	{	if(!it[i].label[0]) continue; //go directly to default 
		
		if(!strcmp(label,it[i].label))	//labels are case sensitive
		{	if(it[i].value&&it[i].value[0]) //lable with empty value: ignore 
				sNew=scA(sNew,it[i].value); 
			return sNew;
		}
	}
	  
	if(*pipe) //for default value, if it's empty label, ignore it too
	{	tp=pipe; To_0(tp); W_RE(pipe,tp); label=pipe;
		S_CPY(sNew,pipe);	//has default, use it
	}
	return sNew;
}

// replace a brace recursively, "{{{.. {{{ .{{{.. }}}. }}} }}}"
char *ParseBrace(TR_T *tt,char *bufOld,char *bufNew,int cntLabel)
{	char *b1,*b2,*t,*s,*sNew; 
	int i,k,cLoop=40,bytes; 

	b1=tt->rBuf1; strcpyN(b1,bufOld,MAX_BR_BUF-304);
	b2=tt->rBuf2;
	

	while(cLoop--)
	{	if(!(k=WMarkDeep3(b1,tt->rOffS,tt->rOffE,'{','}',298))) break;
		
		s=b1; sNew=b2; 
		for(i=0;i<k;i++)
		{	bytes=tt->rOffS[i]-(s-b1);
			
			memcpy(sNew,s,bytes); 
			sNew+=bytes; s+=bytes;
			
			t=b1+tt->rOffE[i]; //terminate this temp
			*t=0;  W_RE(b1,t);

			sNew=TempReplace(s+3,sNew,tt->it,cntLabel);
			s=b1+tt->rOffE[i]+3;		
		}
		S_CPY(sNew,s);
		t=b1; b1=b2; b2=t; //shift buffer
	}
	return scA(bufNew,b1);
}

char *TempParseOne(TR_T *tt,char *s,char *sNew)
{	char *tempStart;	int bFree=0,cntLabel=0;	
	char *sNewOrg=sNew;	  

	P_SPACE(s); 
	if(!*s||*s=='|') return sNew; //if *s=='|', then you get a random
								  //page from template. how funny!
	tempStart=s;
							 // {{localurl:abc | edit}}
	s=TempGetFixed(tempStart); //fixed temps, like {{pagename}}
	if(s) 	
	{	S_CPY(sNew,s); 
		return sNew;	
	}
	
	s=tempStart; To_CN(s,'|',200); //has name?
	
	if(*s=='|')	//has name. go on even no label: must always check defalt
	{	cntLabel=TempMark(s+1,tt->it);	
		s=strcpyTo(tt->temp1,tempStart,'|');  //*s=0; 
		W_RE(tt->temp1,s);
	}
	else  //parse or wiki text error
	{	if(*s) {  S_CPY(sNew,s); return sNew; }
		strcpyN(tt->temp1,tempStart,200);
	}
	
	//if(!(s=GetTemplateInter(tt->temp1,tt->temp2,&bFree))) return sNew; 
  s = GWikiFilesList.GetTemplate(tt->temp1); //returns content such as: "<strike>{{{1}}}</strike>";

	if (s)
  {
    if(strstr(s,"{{{"))	
      sNew=ParseBrace(tt,s,sNew,cntLabel);
    else sNew=scA(sNew,s);
  }
  else
  { // no template found
    // todo: would be nice to preserve the invalid template text in such case
  }
	
	if(bFree) Fm(s);
	return sNew;
}

void RidBrace(char *s,char *sNew)
{	int k=0; 
	while(*s)
	{	if(*s=='{'&&s[1]=='{')
		{	s+=2; k++; continue; }
		
		if(*s=='}'&&s[1]=='}')
		{	s+=2;  k--; continue; }
		if(!k) *sNew++=*s++;
		else s++;
	}
}


void TempClean(char *s)	
{	
	while(*s)
	{	if(*s==PIPE_TEMP) { *s++='|'; 	continue; }
		
		s++;
	}
}


extern void TempRidStuff(char *buf); //more nuisance stuff will come soon!

//use risky function, reason: a few templates missing ending tag
// this template miss 1 ending brace
//{{Audio ja-hokkaido.ogg listen }		    

int ParseTemplate(char *bufOld,char *bufNew,int bShowTemp)
{	int k,i,bytes,cLoop=40;	char *s, *sNew=0;
	TR_T tt;  	 
	char *b1,*b2,*t;

	if(!bShowTemp) 
	{	RidBrace(bufOld,bufNew);
		return 1; 
	}

	zm(&tt,sizeof(TR_T));

				//meaningless stuff in article (rarely happens)
				//only use is to show defaults since there is nothing to replace them
	if((s=strstr(bufOld,"{{{"))&&s[3]!='{') 
	{	TempGetMem(&tt);
		ParseBrace(&tt,bufOld,bufNew,0); 
		strcpy(bufOld,bufNew);
	}

	if(!(s=strstr(bufOld,"{{")))  //no temp
	{	TempFreeMem(&tt); //just in case, never leak mem
		return 0; 
	}
	
	TempGetMem(&tt);

	TempRidStuff(bufOld);

	b1=bufOld; b2=bufNew;

	while(cLoop--) //avoid infinite recursion. Z 
	{			 //recurse from the deepest, update for each round	  
		if(!(k=WMarkDeep2Risky(b1,tt.offS,tt.offE,'{','}',MAX_OFF))) break;
		
		s=b1; sNew=b2; 
		for(i=0;i<k;i++)
		{	bytes=tt.offS[i]-(s-b1); 
			if(bytes<0) bytes=0; //just in case
			memcpy(sNew,s,bytes); 
			sNew+=bytes; s+=bytes;
			
			t=b1+tt.offE[i]; //terminate this temp
			*t=0;  W_RE(b1,t);	
			
			PipeReplaceLink(s);	//hide link pipes

			sNew=TempParseOne(&tt,s+2,sNew);
			
			s=b1+tt.offE[i]+1; 
			if(*s=='}') s++;		
		}
		S_CPY(sNew,s);
		t=b1; b1=b2; b2=t; //shift buffer
	}
	TempFreeMem(&tt);
	if(sNew) Z4(sNew);
	TempClean(b1);
	
	if(bTest)
		WWriteTest("ParseTempEnd",b1);

	if(b1!=bufNew) return 0; 
	return 1; 	
}



