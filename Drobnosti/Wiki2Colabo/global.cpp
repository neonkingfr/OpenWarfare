#include "../WikiTasks/WikiTasks.h"

//global variables
const char *productsStr[] = {"OFP2", "ArmA", "VBS2", "ARMA 2"};
const char *prioritiesStr[] = {"", "NiceToHave", "Desired", "Required"};
const char *stagesStr[NStages] = {"Analysis", "Prototype", "Alpha", "Beta"};
