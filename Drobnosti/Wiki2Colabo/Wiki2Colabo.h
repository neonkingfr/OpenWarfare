#ifndef WIKI_TASKS_H
#define WIKI_TASKS_H

#include "wikiFilterExt.h"

RString ParseWiki(RString wikitext, CategoryList *categoryList);

#endif
