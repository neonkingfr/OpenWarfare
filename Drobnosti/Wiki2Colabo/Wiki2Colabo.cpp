#include <El/elementpch.hpp>
#include <Es/Common/win.h>
#include <Es/Strings/rString.hpp>

#include <El/XML/inet.hpp>
#include <El/XML/xml.hpp>
#include <El/QStream/qStream.hpp>
#include <El/ParamFile/paramFile.hpp>

#include "Wiki2Colabo.h"
#include "wikiFilterExt.h"

QOStream &operator<<(QOStream &out, const float f)
{
  char buff[64];
  sprintf(buff, "%.1f", f);
  return (out << buff);
}
QOStream &operator<<(QOStream &out, const int i)
{
  char buff[64];
  sprintf(buff, "%d", i);
  return (out << buff);
}

#ifdef WIKI_TOOLS_XML
//! free Win32 allocated memory when variable is destructed
class AutoFree
{
  //! handle to Win32 memory obtained by GlobalAlloc call
  void *_mem;

private:
  //! no copy constructor
  AutoFree(const AutoFree &src);

public:
  //! attach Win32 memory handle
  /*!
  \param mem handle to Win32 memory obtained by GlobalAlloc call
  */
  void operator = (void *mem)
  {
    if (mem==_mem) return;
    Free();
    _mem = mem;
  }
  //! copy - reassing Win32 handle to new object
  /*! source will be NULL after assignement */
  void operator = (AutoFree &src)
  {
    if (src._mem == _mem) return;
    Free();
    _mem = src._mem;
    src._mem = NULL;
  }
  //! empty constructor
  AutoFree(){_mem = NULL;}
  //! construct from Win32 memory handle
  AutoFree(void *mem){_mem = mem;}
  //! convert to pointer
  operator void * () {return _mem;}
  //! destruct - free pointer
  ~AutoFree(){Free();}
#ifdef _WIN32
  //! free pointer explicitelly
  void Free(){if (_mem) GlobalFree(_mem),_mem = NULL;}
#else
  //! free pointer explicitelly
  void Free(){if (_mem) free(_mem),_mem = NULL;}
#endif
};

RString ConvertCharset(RString in)
{
  const int bufferSize = 8192;
  WCHAR buffer[bufferSize];
  MultiByteToWideChar(CP_UTF8, 0, in, -1, buffer, bufferSize);
  char out[bufferSize];
  WideCharToMultiByte(CP_ACP, 0, buffer, -1, out, bufferSize, NULL, NULL);
  return out;
}

template<int size>
void strCat(TempString<size> &out, const char *str)
{
  while(*str)
  {
    out.Add(*str);
    str++;
  }
}

RString ConvertSpecialChars(RString in)
{
  TempString<1024> out;
  for (int i=0, siz=in.GetLength(); i<siz; i++)
  {
    switch (in[i])
    {
    case '&': strCat(out,"&amp;"); break;
    case '<': strCat(out,"&lt;"); break;
    case '>': strCat(out,"&gt;"); break;
    case '"': strCat(out,"&quot;"); break;
    case '\'': strCat(out,"&apos;"); break;
    default: out.Add(in[i]);
    }
  }
  out.Add(0);
  return out;
}

class ExtractArticles : public SAXParser
{
private:
  RString _currentArticle;
protected:
  RString _category;
  AutoArray<RString> _articleNames;  
  int _articleElement;

public:
  ExtractArticles() {_articleElement = 0; }
  AutoArray<RString> &GetArticles() {return _articleNames;}
  RString GetCategory() const {return _category; }

  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "CategoryExport") == 0) {
      XMLAttribute *pcatg = attributes.Find("category");
      if (pcatg) _category = pcatg->value;
    }
    else if (stricmp(name, "article") == 0) {
      _currentArticle=RString("");
      _articleElement++;
    }
  }
  virtual void OnEndElement(RString name)
  {
    if (stricmp(name, "article") == 0) {
      _articleElement--;
      _articleNames.Add(_currentArticle);
    }
  }
  virtual void OnCharacters(RString chars)
  {
    if (_articleElement > 0) _currentArticle = _currentArticle + chars;
  }
};

RString ConvertDate(RString date)
{
  char *dateStr = date.MutableData();
  for (int i=0; i<date.GetLength(); i++)
  {
    if (dateStr[i]=='T' || dateStr[i]=='Z') dateStr[i]=' '; //convert to space
  }
  return date;
}

const unsigned char utf8prefix[3] = {0xEF, 0xBB, 0xBF};
void ExportXML(AutoArray<Article>&articles, WikiFilesList &images, RString categoryName)
{
  RString win1250FileName = categoryName=="-1" ? "wikiImportInfo" : categoryName; //::ConvertCharset(categoryName);
  RString outXmlFileName = win1250FileName + ".xml";
  QOFStream out(cc_cast(outXmlFileName));
  // utf8 prefix EF BB BF
  out.write(utf8prefix, 3);
  out << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n\r\n";

  // there must be top level element inside XML file!
  out << "<Wiki>\r\n";
  // information about ARTICLES
  out << "<WikiArticles category=\"" << categoryName << "\">\r\n";
  for (int i=0; i<articles.Size(); i++)
  {
    Article &article = articles[i];
    out << "\t<Article>\r\n";
    {
      //title
      out << "\t\t<Title>" << article.title<< "</Title>\r\n";
      //author
      out << "\t\t<Author>" << article.author<< "</Author>\r\n";
      //date
      out << "\t\t<Date>" << ConvertDate(article.date)<< "</Date>\r\n";
      //url
      out << "\t\t<Url>" << article.url << "</Url>\r\n";
      //path to file
      out << "\t\t<Path>" << article.path << "</Path>\r\n";
      //categories list
      out << "\t\t<Categories>" << article.categories << "</Categories>\r\n";
      //close tag
    }
    out << "\t</Article>\r\n";
  }
  out << "</WikiArticles>\r\n";
  
  // information about IMAGES
  images.Export2Xml(out);
  out << "</Wiki>\r\n";
  out.close();
}

void SaveArticlesContent(AutoArray<Article>&wikiArticles, RString pathToSave, RString url, InternetConnectInfo &inetInfo)
{
  // Set the path and url required for wikitext parser
  GWikiFilesList.SetPaths(pathToSave, url, RString("https://") + inetInfo.serverName + "/index.php/");
  GWikiFilesList.SetInetInfo(inetInfo);
  // Parse all articles and save them
  for (int i=0; i<wikiArticles.Size(); i++)
  {
    extern char szTitle[];
    strcpy(szTitle, wikiArticles[i].title);
    //save text to file
    RString win1250FileName = ::ConvertCharset(wikiArticles[i].path);
    WikiFilesList::CreatePath(win1250FileName);
    FILE *f = fopen(win1250FileName, "wb");
    if (f)
    {
      fwrite(utf8prefix, sizeof(char), sizeof(utf8prefix)/sizeof(char), f);
      CategoryList categoryList;
      RString parsedText = ParseWiki(wikiArticles[i].text, &categoryList);
      //add article title as a <h1> caption and add also the link to the source wiki page
      wikiArticles[i].originalUrl = GWikiFilesList.GetWikiURLRoot() + GWikiFilesList.ConvertToFileName(wikiArticles[i].title);
      RString articleTitleCaption = Format("<h1>%s</h1>*(Article was imported from wiki, see [original wiki page](%s).)*\r\n", \
           cc_cast(wikiArticles[i].title), cc_cast(wikiArticles[i].originalUrl));
      //add wiki style
      RString wikiStyle = Format("<style type=\"text/css\">\r\n  @import url(\"%scolaboWiki.css\");\r\n</style>\r\n", cc_cast(url));
      parsedText = wikiStyle + articleTitleCaption + parsedText;
      wikiArticles[i].categories = categoryList.GetCategories();
      fwrite(parsedText, sizeof(char), parsedText.GetLength(), f);
      fclose(f);
    }
    else
    { //Error report
      printf("Error: Unable to open file '%s' while saving the wiki article.\n", cc_cast(wikiArticles[i].path));
    }
  }
  // Download also used wiki images
  GWikiFilesList.DownloadImages();
}

BYTE toHex(BYTE n)
{
  if (n<10) return (n+'0');
  if (n<16) return (n+'A'-10);
  return '0';
}

/**
@param text string in UTF8
*/
RString UrlEncode(RString text)
{
  RString sOut;
  const int nLen = text.GetLength()+1;
  LPBYTE pOutTmp = NULL;
  LPBYTE pOutBuf = NULL;
  LPBYTE pInTmp = NULL;
  LPBYTE pInBuf =(LPBYTE)text.Data();
  //alloc out buffer
  pOutBuf = (LPBYTE)sOut.CreateBuffer(nLen  * 3 - 2);
  if(pOutBuf)
  {
    pInTmp	= pInBuf;
    pOutTmp = pOutBuf;
    // do encoding
    while (*pInTmp)
    {
      if (*pInTmp>=0x80) {
        *pOutTmp++ = '%';
        *pOutTmp++ = toHex(*pInTmp>>4);
        *pOutTmp++ = toHex(*pInTmp%16);
      } 
      else if(isalnum(*pInTmp)) {
        *pOutTmp++ = *pInTmp;
      }
      else if(isspace(*pInTmp))*pOutTmp++ = '_';
      else
      {
        *pOutTmp++ = '%';
        *pOutTmp++ = toHex(*pInTmp>>4);
        *pOutTmp++ = toHex(*pInTmp%16);
      }
      pInTmp++;
    }
    *pOutTmp = '\0';
  }
  return sOut;
}

//Force LogF to write into stderr (in release configuration)
#if !_DEBUG
#include "Es/Framework/appFrame.hpp"
class ConsoleAppFrameFunctions : public AppFrameFunctions
{
public:
#if _ENABLE_REPORT
  virtual void LogF(const char *format, va_list argptr);
#endif
};

#if _ENABLE_REPORT
void ConsoleAppFrameFunctions::LogF(const char *format, va_list argptr)
{
  BString<512> buf;
  vsprintf(buf,format,argptr);
  strcat(buf,"\n");
  fputs(buf,stderr);
}
#endif

static ConsoleAppFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;
#endif

int main(int argc, char* argv[])
{
  RString path = "/index.php/Special:Export/"; //https://wiki.bistudio.com/index.php/Special:Export/
  RString pathCategory = "/index.php/Special:CategoryExport/Category:"; //https://wiki.bistudio.com/index.php/Special:CategoryExport/Category:
  RString categoryName;
  RString pathToSave;
  RString url;
  RString articleToImport;

  InternetConnectInfo inetInfo;
  if (argc>=6) {
    categoryName = argv[1];
    inetInfo.userName = argv[2];
    inetInfo.password = argv[3];
    pathToSave = argv[4];
    int pathLen = pathToSave.GetLength();
    if (pathLen>0 && pathToSave[pathLen-1]!='\\') pathToSave = pathToSave + "\\"; //backslash terminate
    inetInfo.port = 443;
    inetInfo.serverName = "wiki.bistudio.com";
    url  = argv[5];
    if (url[url.GetLength()-1]!='/') url = url + RString("/");
    if (categoryName=="-1" && argc>6)
    { // import only one article (always saves info inside wikiImportInfo.xml file)
      char *articleName = strrchr(argv[6], '/');
      if (articleName)
        articleToImport = articleName+1; //full path, take only article name
      else
        articleToImport = argv[6]; //probably only article name specified
    }
  }
  else {
    printf("Usage:\n");
    printf("1.) wiki2Colabo.exe category username password pathToSaveFiles urlToLinkImages\n");
    printf("      * articles and images are downloaded to specified path\n");
    printf("      * imported files info stored in categoryName.xml\n");
    printf("2.) wiki2Colabo.exe -1 user pass pathToSaveFiles urlToLinkImages articleURL\n");
    printf("      * articles and images are downloaded to specified path\n");
    printf("      * imported files info stored in wikiImportInfo.xml\n");
    return 1;
  }

  AutoArray<RString> *articlesPtr = NULL;
  AutoArray<RString> oneArticle;
  ExtractArticles parser;
  if ( !(categoryName=="-1") )
  {
    size_t size = 0;
    char *data = DownloadFile(pathCategory + categoryName, size, NULL, &inetInfo);
    if (!data) {
      printf("Error: Unable to download file: %s\n", cc_cast(pathCategory + categoryName));
      return -1;
    }
    data[size]=0;
    printf("Downloaded file: %s (size = %d)\n", cc_cast(pathCategory + categoryName), strlen(data));
    printf("Content: ((\n%s\n))\n", data);
    AutoFree buffer(data);

    QIStrStream in(data, size);

    parser.Parse(in);
    articlesPtr = &parser.GetArticles();
    RString category = parser.GetCategory();
  }
  else
  {
    const int bufferSize = 8192;
    WCHAR buffer[bufferSize];
    MultiByteToWideChar(CP_ACP, 0, articleToImport, -1, buffer, bufferSize);
    char out[bufferSize];
    WideCharToMultiByte(CP_UTF8, 0, buffer, -1, out, bufferSize, NULL, NULL);
    articleToImport = RString(out);
    oneArticle.Add(articleToImport);
    articlesPtr = &oneArticle;
  }
  AutoArray<RString> &articles = *articlesPtr;

  // read content of pages
  const char *avoid = "Category:";
  AutoArray<Article> wikiArticles;
  wikiArticles.Realloc(articles.Size());
  for (int i=articles.Size()-1; i>=0; i--)
  {
    if (strnicmp(articles[i], avoid, strlen(avoid)) == 0)
    {
      articles.Delete(i);
      continue;
    }

    size_t size = 0;
    printf("(Done: %d%%) %s\n", (100*(articles.Size()-i))/articles.Size(), cc_cast(articles[i]));
#if ENCODE_URL
    RString nameUrlEncoded = UrlEncode(articles[i]);
#else
    RString nameUrlEncoded = articles[i]; //do not encode
#endif
    char *data = DownloadFile(path + nameUrlEncoded, size, NULL, &inetInfo);
    if (!data) {
      printf("Error: Unable to download file: %s\n", cc_cast(path + nameUrlEncoded));
      printf("Hint: Possibly wrong UrlEncoded file name\n");
      return -1;
    }
    AutoFree buffer(data);

    QIStrStream in(data, size);
#if _DEBUG
    int doIt=-1;
    if (i==doIt)
    {
      QOFStream outf("testOutput.xml");
      RString debugData(data,size);
      outf << debugData;
    }
#endif

    ExtractArticle parser;
    parser.Parse(in, true); //preserveSpaces
    // add article to array at last
    if ( !parser.Invalid() )
    {
      int ix = wikiArticles.Add();
      wikiArticles[ix] = parser.GetArticle();
      RString fileNameConverted = WikiFilesList::ConvertToFileName(nameUrlEncoded);
      RString hashSubdirUrl = WikiFilesList::GetHashingSubdir(fileNameConverted,false);
      wikiArticles[ix].url = url+hashSubdirUrl+fileNameConverted+".txt";
      RString hashSubdir = WikiFilesList::GetHashingSubdir(fileNameConverted);
      wikiArticles[ix].path = pathToSave+hashSubdir+fileNameConverted+".txt"; //it is backslash terminated already
    }
  }
#ifdef CONVERT_CHARSET
  // Convert Charset for all articles
  for (int i=0; i<wikiArticles.Size(); i++)
  {
    wikiArticles[i].ConvertCharset();
  }
#endif
  SaveArticlesContent(wikiArticles, pathToSave, url, inetInfo);
  ExportXML(wikiArticles, GWikiFilesList, categoryName);

	return 1;
}

#endif
//end of #ifdef WIKI_TOOLS_XML
