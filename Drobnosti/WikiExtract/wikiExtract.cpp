#include <El/elementpch.hpp>
#include <Es/Common/win.h>
#include <Es/Strings/rString.hpp>

#include <El/XML/inet.hpp>
#include <El/XML/xml.hpp>
#include <El/QStream/qStream.hpp>
#include <El/ParamFile/paramFile.hpp>

#include "../WikiTasks/WikiTasks.h"

QOStream &operator<<(QOStream &out, const float f)
{
  char buff[64];
  sprintf(buff, "%.1f", f);
  return (out << buff);
}
QOStream &operator<<(QOStream &out, const int i)
{
  char buff[64];
  sprintf(buff, "%d", i);
  return (out << buff);
}

//! free Win32 allocated memory when variable is destructed
class AutoFree
{
  //! handle to Win32 memory obtained by GlobalAlloc call
  void *_mem;

private:
  //! no copy constructor
  AutoFree(const AutoFree &src);

public:
  //! attach Win32 memory handle
  /*!
  \param mem handle to Win32 memory obtained by GlobalAlloc call
  */
  void operator = (void *mem)
  {
    if (mem==_mem) return;
    Free();
    _mem = mem;
  }
  //! copy - reassing Win32 handle to new object
  /*! source will be NULL after assignement */
  void operator = (AutoFree &src)
  {
    if (src._mem == _mem) return;
    Free();
    _mem = src._mem;
    src._mem = NULL;
  }
  //! empty constructor
  AutoFree(){_mem = NULL;}
  //! construct from Win32 memory handle
  AutoFree(void *mem){_mem = mem;}
  //! convert to pointer
  operator void * () {return _mem;}
  //! destruct - free pointer
  ~AutoFree(){Free();}
#ifdef _WIN32
  //! free pointer explicitelly
  void Free(){if (_mem) GlobalFree(_mem),_mem = NULL;}
#else
  //! free pointer explicitelly
  void Free(){if (_mem) free(_mem),_mem = NULL;}
#endif
};

class Task
{
public:
  RString name;
  RString priority;
  int orders[NStages];
  float time[NStages];
  Task() : priority(0)
  {
    for (int i=0; i<NStages; i++) { orders[i]=0; time[i]=0.0f; }
  }
};
TypeIsMovable(Task)

class ExtractTasks : public SAXParser
{
private:
  RString _curValue;
  AutoArray<Task> _tasks;
  int _curIx;
  int _orIx;
  int _tmIx;
protected:
  int _defaultElement;
public:
  ExtractTasks(const char *filename)
  {
    QIFStream in;
    in.open(filename);
    //initialize parser
    _defaultElement = 0; _orIx=_tmIx=0;
    Parse(in);
  }
  const AutoArray<Task> &GetTasks() const {return _tasks;}

public:
  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "orders") == 0) {
      _orIx=0;
    }
    else if (stricmp(name, "order") == 0) {
      _defaultElement++;
    }
    if (stricmp(name, "timeleft") == 0) {
      _tmIx=0;
    }
    else if (stricmp(name, "time") == 0) {
      _defaultElement++;
    }
    else if (stricmp(name, "name") == 0) {
      _curIx = _tasks.Add();
      _defaultElement++;
    }
    else if (stricmp(name, "priority") == 0) {
      _defaultElement++;
    }
  }
  virtual void OnEndElement(RString name)
  {
    if (stricmp(name, "order") == 0) {
      _defaultElement--;
      _tasks[_curIx].orders[_orIx]=atoi(cc_cast(_curValue));
      _orIx++;
    }
    else if (stricmp(name, "time") == 0) {
      _defaultElement--;
      _tasks[_curIx].time[_tmIx]=atof(cc_cast(_curValue));
      _tmIx++;
    }
    else if (stricmp(name, "name") == 0) {
      _defaultElement--;
      _tasks[_curIx].name=_curValue;
    }
    else if (stricmp(name, "priority") == 0) {
      _defaultElement--;
      _tasks[_curIx].priority=_curValue;
    }
    _curValue="";
  }
  virtual void OnCharacters(RString chars)
  {
    if (_defaultElement > 0) _curValue = _curValue + chars;
  }
};

RString ConvertCharset(RString in)
{
  const int bufferSize = 8192;
  WCHAR buffer[bufferSize];
  MultiByteToWideChar(CP_UTF8, 0, in, -1, buffer, bufferSize);
  char out[bufferSize];
  WideCharToMultiByte(CP_ACP, 0, buffer, -1, out, bufferSize, NULL, NULL);
  return out;
}

template<int size>
void strCat(TempString<size> &out, const char *str)
{
  while(*str)
  {
    out.Add(*str);
    str++;
  }
}

RString ConvertSpecialChars(RString in)
{
  TempString<1024> out;
  for (int i=0, siz=in.GetLength(); i<siz; i++)
  {
    switch (in[i])
    {
    case '&': strCat(out,"&amp;"); break;
    case '<': strCat(out,"&lt;"); break;
    case '>': strCat(out,"&gt;"); break;
    case '"': strCat(out,"&quot;"); break;
    case '\'': strCat(out,"&apos;"); break;
    default: out.Add(in[i]);
    }
  }
  out.Add(0);
  return out;
}

struct UserDefault
{
  RString name;
  float daysInWeek;
  UserDefault() : daysInWeek(0.0f) {}
  UserDefault(const char *name1) : daysInWeek(0.0f), name(name1) {}
};
TypeIsMovable(UserDefault)

class WikiTasksDefaults : public SAXParser
{
private:
  RString _curValue;
  int _prIx, _stIx;
  int _usIx;
protected:
  int _defaultElement;
public:
  int orders[NPriorities][NStages];
  AutoArray<UserDefault> users;
  WikiTasksDefaults()
  {
    for (int i=0; i<NPriorities; i++)
      for (int j=0; j<NStages; j++) orders[i][j]=0;
    QIFStream in;
    in.open("wikiTasksDefaults.xml");
    //initialize parser
    _defaultElement = 0; _prIx=_stIx=-1;
    Parse(in);
  }

public:
  virtual void OnStartElement(RString name, XMLAttributes &attributes)
  {
    if (stricmp(name, "order") == 0) {
      XMLAttribute *ppr = attributes.Find("priority");
      if (ppr) {
        XMLAttribute *pst = attributes.Find("stage");
        if (pst) {
          _prIx=GetPriorityIx(ppr->value);
          _stIx=GetStageIx(pst->value);
        }
      }
      _defaultElement++;
    }
    else if (stricmp(name, "user") == 0) {
      XMLAttribute *pname = attributes.Find("name");
      if (pname) {
        _usIx=-1;
        for (int i=0; i<users.Size(); i++)
          if (stricmp(cc_cast(users[i].name),pname->value)==0) {_usIx=i; break;}
          if (_usIx==-1) _usIx = users.Add(UserDefault(pname->value));
          _defaultElement++;
      }
    }
  }
  virtual void OnEndElement(RString name)
  {
    if (stricmp(name, "order") == 0) {
      _defaultElement--;
      if (_prIx!=-1 && _stIx!=-1) {
        orders[_prIx][_stIx]=atoi(cc_cast(_curValue));
      }
    }
    else if (stricmp(name, "user") == 0) {
      _defaultElement--;
      users[_usIx].daysInWeek=(float)atof(_curValue);
    }
    _curValue="";
  }
  virtual void OnCharacters(RString chars)
  {
    if (_defaultElement > 0) _curValue = _curValue + chars;
  }
};

void ExportXML(AutoArray<Task> &tasks, char *filename, const char *categoryName, int orderFrom, int orderTo)
{
  QOFStream out(cc_cast(filename));
  out << "<?xml version=\"1.0\" encoding=\"windows-1250\"?>\r\n"
    "<?xml-stylesheet href=\"tasks.xsl\" type=\"text/xsl\"?>\r\n\r\n";

  out << "<Tasks category=\"" << categoryName << "\">\r\n";
  for (int i=0; i<tasks.Size(); i++)
  {
    Task &task = tasks[i];
    int stageIx=-1;
    for (int j=0; j<NStages; j++) 
    {
      if (task.orders[j]>=orderFrom && task.orders[j]<=orderTo 
          && task.time[j]>0.0f) 
      {
        stageIx=j; 
      }
    }
    if (stageIx<0) continue;
    out << "\t<Task>\r\n";
    //name
    out << "\t\t<Name>" << task.name << "</Name>\r\n";
    //priority
    out << "\t\t<Priority>" << task.priority << "</Priority>\r\n";
    //order
    out << "\t\t<Order>" << task.orders[stageIx] << "</Order>\r\n";
    //stage
    out << "\t\t<Stage>" << stagesStr[stageIx] << "</Stage>\r\n";
    out << "\t</Task>\r\n";
  }
  out << "</Tasks>\r\n";
}

//Force LogF to write into stderr (in release configuration)
#if !_DEBUG
#include "Es/Framework/appFrame.hpp"
class ConsoleAppFrameFunctions : public AppFrameFunctions
{
public:
#if _ENABLE_REPORT
  virtual void LogF(const char *format, va_list argptr);
#endif
};

#if _ENABLE_REPORT
void ConsoleAppFrameFunctions::LogF(const char *format, va_list argptr)
{
  BString<512> buf;
  vsprintf(buf,format,argptr);
  strcat(buf,"\n");
  fputs(buf,stderr);
}
#endif

static ConsoleAppFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;
#endif

int main(int argc, char* argv[])
{
  RString categoryName;

  const char *productName;
  int orderFrom = 0;
  int orderTo = 0;
  if (argc>=4) {
    productName = argv[1];
    orderFrom = atoi(argv[2]);
    orderTo = atoi(argv[3]);
  }
  else {
    printf("Usage: wikiExtract.exe productName orderFrom orderTo\n\n");
    return 1;
  }

  char filename[1024]; sprintf(filename, "%s.xml", productName);
  ExtractTasks exTasks(filename);
  AutoArray<Task> tasks = exTasks.GetTasks();
  char outfilename[1024]; sprintf(outfilename, "%s_%d_%d.xml", productName, orderFrom, orderTo);
  ExportXML(tasks, outfilename, productName, orderFrom, orderTo);

  return 0;
}

