// CDKeys.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <windows.h>

#include <stdlib.h>
#include <time.h>

#include "rsa\rsa.h"

#define NBITS	 120
#define NBYTES NBITS / 8

#define VBS									0
#define BIS_INTERNAL				0
#define SECUROM_BI_INTERNAL	0
#define ARMA        				0
#define ARMA2        				0
#define ARMA2OAH        		0
#define BAF             		0
#define ARMA2PMC            0
#define ARMA2FREE           0
#define ARMA2RFT            0
#define TAKE_ON_HELICOPTERS 0
#define ARMAX               0
#define LIBERATION          0
#define TAKE_ON_HIND        0
#define CARRIER_COMMAND     0
#define ARMA2ACR            0

#define PROCESS_INPUT				0
#define OUTPUT_BINARY				0

#define RESISTANCE					0
#define RESISTANCE_BEGIN		10000000 // 10.000.000
#define RESISTANCE_END			30000000 // 30.000.000

#if SECUROM_BI_INTERNAL
  #define FILE_OUTPUT					"Data\\Internal\\Internal KEYS %d - %d.txt"
  #define FILE_PRIVATE_KEY		"Data\\Internal\\internal_private.key"
  #define FILE_PUBLIC_KEY			"Data\\Internal\\internal_public.key"
  #define OUTPUT_REG  				0
  #define DISTRIBUTIONS       0
#elif BIS_INTERNAL
  #define FILE_OUTPUT					"Data\\BIS\\BISKEYS.txt"
  #define FILE_PRIVATE_KEY		"Data\\BIS\\bis_private.key"
  #define FILE_PUBLIC_KEY			"Data\\BIS\\bis_public.key"
  #define OUTPUT_REG  				1
  #define REG_PATH            "Data\\BIS\\RegFiles\\%02d.reg"
  #define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_CURRENT_USER\\SOFTWARE\\Bohemia Interactive Studio]\n\"KEY\"=hex:"
  #define DISTRIBUTIONS       0
#elif ARMA
  #define FILE_OUTPUT					"Data\\ArmA\\ARMA %s KEYS %d - %d.txt"
  #define FILE_PRIVATE_KEY		"Data\\ArmA\\arma_private.key"
  #define FILE_PUBLIC_KEY			"Data\\ArmA\\arma_public.key"
  #define OUTPUT_REG  				0
  #define REG_PATH            "Data\\ArmA\\RegFiles\\%s %d.reg"
  #define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\ArmA]\n\"KEY\"=hex:"
  #define DISTRIBUTIONS       1

  #define DISTRIBUTIONS_ENUM(XX) \
    XX(Internal) \
    XX(BetaTest) \
    XX(Russian) \
    XX(Czech) \
    XX(Polish) \
    XX(German) \
    XX(US) \
    XX(International) \
    XX(EnglishOnline)
#elif ARMA2
  #define FILE_OUTPUT					"Data\\ArmA2\\ARMA 2 %s KEYS %d - %d.txt"
  #define FILE_PRIVATE_KEY		"Data\\ArmA2\\arma2_private.key"
  #define FILE_PUBLIC_KEY			"Data\\ArmA2\\arma2_public.key"
  #define OUTPUT_REG  				0
  #define REG_PATH            "Data\\ArmA2\\RegFiles\\%s %d.reg"
  #define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\ArmA 2]\n\"KEY\"=hex:"
  #define DISTRIBUTIONS       1
  #define PRODUCT_ID          0

  #define DISTRIBUTIONS_ENUM(XX) \
    XX(US) \
    XX(Czech) \
    XX(German) \
    XX(Hungarian) \
    XX(Polish) \
    XX(Russian) \
    XX(International) \
    XX(Online) \
    XX(Brasilian)
#elif ARMA2OAH
  #define FILE_OUTPUT					"Data\\ArmA2OA\\OA %s KEYS %d - %d.txt"
  #define FILE_PRIVATE_KEY		"Data\\ArmA2OA\\arma2_OA_private.key"
  #define FILE_PUBLIC_KEY			"Data\\ArmA2OA\\arma2_OA_public.key"
  #define OUTPUT_REG  				0
  #define REG_PATH            "Data\\ArmA2OA\\RegFiles\\%s %d.reg"
  #define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\ArmA 2 OA]\n\"KEY\"=hex:"
  #define DISTRIBUTIONS       1
  #define PRODUCT_ID          1

  #define DISTRIBUTIONS_ENUM(XX) \
    XX(US) \
    XX(English) \
    XX(German) \
    XX(Polish) \
    XX(Russian) \
    XX(Czech) \
    XX(Euro) \
    XX(Hungarian) \
    XX(Brasilian)
#elif BAF
  #define FILE_OUTPUT					"Data\\ArmA2BAF\\BAF %s KEYS %d - %d.txt"
  #define FILE_PRIVATE_KEY		"Data\\ArmA2BAF\\arma2_BAF_private.key"
  #define FILE_PUBLIC_KEY			"Data\\ArmA2BAF\\arma2_BAF_public.key"
  #define OUTPUT_REG  				0
  #define REG_PATH            "Data\\ArmA2BAF\\RegFiles\\%s %d.reg"
  #define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\ArmA 2 OA BAF]\n\"KEY\"=hex:"
  #define DISTRIBUTIONS       1
  #define PRODUCT_ID          2

  #define DISTRIBUTIONS_ENUM(XX) \
    XX(US) \
    XX(English) \
    XX(German) \
    XX(Polish) \
    XX(Russian) \
    XX(Czech) \
    XX(Euro) \
    XX(Hungarian) \
    XX(Brasilian)
#elif ARMA2PMC
  #define FILE_OUTPUT					"Data\\ArmA2PMC\\PMC %s KEYS %d - %d.txt"
  #define FILE_PRIVATE_KEY		"Data\\ArmA2PMC\\arma2_PMC_private.key"
  #define FILE_PUBLIC_KEY			"Data\\ArmA2PMC\\arma2_PMC_public.key"
  #define OUTPUT_REG  				0
  #define REG_PATH            "Data\\ArmA2PMC\\RegFiles\\%s %d.reg"
  #define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\ArmA 2 OA PMC]\n\"KEY\"=hex:"
  #define DISTRIBUTIONS       1
  #define PRODUCT_ID          3

  #define DISTRIBUTIONS_ENUM(XX) \
    XX(US) \
    XX(English) \
    XX(German) \
    XX(Polish) \
    XX(Russian) \
    XX(Czech) \
    XX(Euro) \
    XX(Hungarian) \
    XX(Brasilian)
#elif ARMA2FREE
  #define FILE_OUTPUT					"Data\\Arma2Free\\A2Free %s KEYS %d - %d.txt"
  #define FILE_PRIVATE_KEY		"Data\\Arma2Free\\arma2_free_private.key"
  #define FILE_PUBLIC_KEY			"Data\\Arma2Free\\arma2_free_public.key"
  #define OUTPUT_REG  				0
  #define REG_PATH            "Data\\Arma2Free\\RegFiles\\%s %d.reg"
  #define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\Arma 2 Free]\n\"KEY\"=hex:"
  #define DISTRIBUTIONS       1
  #define PRODUCT_ID          4
  #define OUTPUT_SQL          1
  #define FILE_SQL  					"Data\\Arma2Free\\A2Free %s KEYS %d - %d.sql"

  #define DISTRIBUTIONS_ENUM(XX) \
    XX(Online)
#elif ARMA2RFT
#define FILE_OUTPUT					"Data\\ArmA2RFT\\RFT %s KEYS %d - %d.txt"
#define FILE_PRIVATE_KEY		"Data\\ArmA2RFT\\arma2_RFT_private.key"
#define FILE_PUBLIC_KEY			"Data\\ArmA2RFT\\arma2_RFT_public.key"
#define OUTPUT_REG  				0
#define REG_PATH            "Data\\ArmA2RFT\\RegFiles\\%s %d.reg"
#define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\ArmA 2 OA RFT]\n\"KEY\"=hex:"
#define DISTRIBUTIONS       1
#define PRODUCT_ID          5

#define DISTRIBUTIONS_ENUM(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(Hungarian) \
  XX(Brasilian)
#elif TAKE_ON_HELICOPTERS
#define FILE_OUTPUT					"Data\\TakeOnHelicopters\\TOH %s KEYS %d - %d.txt"
#define FILE_PRIVATE_KEY		"Data\\TakeOnHelicopters\\TakeOnHelicopters_private.key"
#define FILE_PUBLIC_KEY			"Data\\TakeOnHelicopters\\TakeOnHelicopters_public.key"
#define OUTPUT_REG  				0
#define REG_PATH            "Data\\TakeOnHelicopters\\RegFiles\\%s %d.reg"
#define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\Take On Helicopters]\n\"KEY\"=hex:"
#define DISTRIBUTIONS       1
#define PRODUCT_ID          6

#define DISTRIBUTIONS_ENUM(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(International) \
  XX(Japanese) \
  XX(Internal) \
  XX(Italian) \
  XX(Spanish)
#elif ARMAX
#define FILE_OUTPUT					"Data\\ArmAX\\amrax %s KEYS %d - %d.txt"
#define FILE_PRIVATE_KEY		"Data\\ArmAX\\armax_private.key"
#define FILE_PUBLIC_KEY			"Data\\ArmAX\\armax_public.key"
#define OUTPUT_REG  				0
#define REG_PATH            "Data\\ArmAX\\RegFiles\\%s %d.reg"
#define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\ArmAX]\n\"KEY\"=hex:"
#define DISTRIBUTIONS       1
#define PRODUCT_ID          7

#define DISTRIBUTIONS_ENUM(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(Hungarian) \
  XX(Brasilian)
#elif LIBERATION
#define FILE_OUTPUT					"Data\\Liberation\\Liberation %s KEYS %d - %d.txt"
#define FILE_PRIVATE_KEY		"Data\\Liberation\\Liberation_private.key"
#define FILE_PUBLIC_KEY			"Data\\Liberation\\Liberation_public.key"
#define OUTPUT_REG  				0
#define REG_PATH            "Data\\Liberation\\RegFiles\\%s %d.reg"
#define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\Liberation]\n\"KEY\"=hex:"
#define DISTRIBUTIONS       1
#define PRODUCT_ID          8

#define DISTRIBUTIONS_ENUM(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(Hungarian) \
  XX(Brasilian)
#elif TAKE_ON_HIND
#define FILE_OUTPUT					"Data\\TakeOnHind\\TOHind %s KEYS %d - %d.txt"
#define FILE_PRIVATE_KEY		"Data\\TakeOnHind\\TakeOnHinds_private.key"
#define FILE_PUBLIC_KEY			"Data\\TakeOnHind\\TakeOnHinds_public.key"
#define OUTPUT_REG  				0
#define REG_PATH            "Data\\TakeOnHinds\\RegFiles\\%s %d.reg"
#define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\Take On Helicopters:HIND]\n\"KEY\"=hex:"
#define DISTRIBUTIONS       1
#define PRODUCT_ID          9

#define DISTRIBUTIONS_ENUM(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(International) \
  XX(Japanese) \
  XX(Internal) \
  XX(Italian) \
  XX(Spanish)
#elif CARRIER_COMMAND
#define FILE_OUTPUT					"Data\\CarrierCommand\\CarrierCommand %s KEYS %d - %d.txt"
#define FILE_PRIVATE_KEY		"Data\\CarrierCommand\\CarrierCommand_private.key"
#define FILE_PUBLIC_KEY			"Data\\CarrierCommand\\CarrierCommand_public.key"
#define OUTPUT_REG  				0
#define REG_PATH            "Data\\CarrierCommand\\RegFiles\\%s %d.reg"
#define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\CarrierCommand]\n\"KEY\"=hex:"
#define DISTRIBUTIONS       1
#define PRODUCT_ID          10

#define DISTRIBUTIONS_ENUM(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(International) \
  XX(Japanese) \
  XX(Internal) \
  XX(Italian) \
  XX(Spanish)
#elif ARMA2ACR
#define FILE_OUTPUT					"Data\\ArmA2ACR\\ArmA2ACR %s KEYS %d - %d.txt"
#define FILE_PRIVATE_KEY		"Data\\ArmA2ACR\\ArmA2ACR_private.key"
#define FILE_PUBLIC_KEY			"Data\\ArmA2ACR\\ArmA2ACR_public.key"
#define OUTPUT_REG  				0
#define REG_PATH            "Data\\ArmA2ACR\\RegFiles\\%s %d.reg"
#define REG_PREFIX          "Windows Registry Editor Version 5.00\n\n[HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\ArmA2ACR]\n\"KEY\"=hex:"
#define DISTRIBUTIONS       1
#define PRODUCT_ID          11

#define DISTRIBUTIONS_ENUM(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(International) \
  XX(Japanese) \
  XX(Internal) \
  XX(Italian) \
  XX(Spanish)
#else
  #define FILE_OUTPUT					"Data\\OFP\\CDKEYS.txt"
  #define FILE_PRIVATE_KEY		"Data\\OFP\\private.key"
  #define FILE_PUBLIC_KEY			"Data\\OFP\\public.key"
  #define OUTPUT_REG  				0
  #define DISTRIBUTIONS       0
#endif

#if SECUROM_BI_INTERNAL

#define FIRST_MESSAGE \
{ \
  0, 0,         																		                /* 16 - bits ID */					  \
  'B', 'I', ' ', 'S', 'R', 'I', 'n', 't', 'e', 'r', 'n', 'a', 'l'   /* 104 - bits check value */	\
};

#elif BIS_INTERNAL

#define FIRST_MESSAGE \
{ \
  0, 0,         																		                /* 16 - bits ID */					  \
  'B', 'I', 'S', ' ', 'I', 'n', 't', 'e', 'r', 'n', 'a', 'l', '.'   /* 104 - bits check value */	\
};

#elif VBS

#define FIRST_MESSAGE \
{ \
	0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
	'V', 'B', 'S', 'y', 's', 't', 'e', 'm', ' ', '1'	/* 80 - bits check value */	\
};

#elif RESISTANCE

#define FIRST_MESSAGE \
{ \
	0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
	'R', 'e', 's', 'i', 's', 't', 'a', 'n', 'c', 'e'	/* 80 - bits check value */	\
};

#elif ARMA

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'A', 'r', 'm', 'A', 's', 's', 'a', 'u', 'l', 't'	/* 80 - bits check value */	\
};

#elif ARMA2

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'A', 'r', 'm', 'A', 's', 's', 'a', 'u', 'l', 't'	/* 80 - bits check value */	\
};

#elif ARMA2OAH

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'A', 'r', 'm', 'A', ' ', '2', ' ', 'O', 'A', ' '	/* 80 - bits check value */	\
};

#elif BAF

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'B', 'A', 'F', ' ', ' ', ' ', ' ', ' ', ' ', ' '	/* 80 - bits check value */	\
};

#elif ARMA2PMC

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'A', 'r', 'm', 'A', ' ', '2', ' ', 'P', 'M', 'C'	/* 80 - bits check value */	\
};


#elif ARMA2FREE

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'A', 'r', 'm', 'a', '2 ', ' ', 'F', 'r', 'e', 'e'	/* 80 - bits check value */	\
};

#elif ARMA2RFT

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'A', 'r', 'm', 'A', ' ', '2', ' ', 'R', 'F', 'T'	/* 80 - bits check value */	\
};

#elif TAKE_ON_HELICOPTERS

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'T', 'a', 'k', 'e', 'O', 'n', 'H', 'e', 'l', 'i'	/* 80 - bits check value */	\
};

#elif ARMAX

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'A', 'r', 'm', 'A', ' ', 'X', ' ', ' ', ' ', ' '	/* 80 - bits check value */	\
};

#elif LIBERATION

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'L', 'I', 'B', 'E', 'R', 'A', 'T', 'I', 'O', 'N'	/* 80 - bits check value */	\
};

#elif TAKE_ON_HIND

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'T', 'a', 'k', 'e', 'O', 'n', 'H', 'i', 'n', 'd'	/* 80 - bits check value */	\
};

#elif CARRIER_COMMAND

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'C', 'C', 'o', 'm', 'm', 'a', 'n', 'd', ' ', ' '	/* 80 - bits check value */	\
};

#elif ARMA2ACR

// different key construction for ArmA 2
#define NEW_KEYS 1

#define DISTR_BITS 6
#define SIMPLE_HASH_BITS 4
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

#define FIRST_MESSAGE \
{ \
  0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
  'A', 'r', 'm', 'A', ' ', '2', ' ', 'A', 'C', 'R'	/* 80 - bits check value */	\
};

#else

#define FIRST_MESSAGE \
{ \
	0, 0, 0, 0, 0,																		/* 40 - bits ID */					\
	'F', 'l', 'a', 's', 'h', 'p', 'o', 'i', 'n', 't'	/* 80 - bits check value */	\
};

#endif

#if DISTRIBUTIONS
#define ENUM_VALUE(name) Dist##name,
#define ENUM_NAME(name) #name,

enum Distribution
{
  DISTRIBUTIONS_ENUM(ENUM_VALUE)
};

const char *DistributionNames[] =
{
  DISTRIBUTIONS_ENUM(ENUM_NAME)
};

#endif

#if NEW_KEYS
#include <Wincrypt.h>
static bool AcquireContext(HCRYPTPROV *provider, bool privateKeyAccess)
{
  DWORD flags = privateKeyAccess ? 0 : CRYPT_VERIFYCONTEXT;
  if (CryptAcquireContext(provider, NULL, NULL, PROV_RSA_FULL, flags)) return true;
  // create a new key container
  if (GetLastError() == NTE_BAD_KEYSET)
  {
    flags |= CRYPT_NEWKEYSET;
    if (CryptAcquireContext(provider, NULL, NULL, PROV_RSA_FULL, flags)) return true;
  }
  return false;
}

/// Update 8-bit CRC value using polynomial X^8 + X^5 + X^4 + 1
static void UpdateCRC8(unsigned char &crc, unsigned char src)
{
  static const int polyVal = 0x8C;

  for (int i=0; i<8; i++)
  {
    if ((crc ^ src) & 1) crc = (crc >> 1 ) ^ polyVal;
    else crc >>= 1;
    src >>= 1;
  }
}
#endif

/* Reserved for internal purposes
#define FIRST_KEY			1
#define LAST_KEY			1,000
#define _JAPAN				0
*/

/* European version:
#define FIRST_KEY			1,001
#define LAST_KEY			401,000
#define _JAPAN				0
*/

/* US version:
#define FIRST_KEY			401,001
#define LAST_KEY			801,000
#define _JAPAN				0
*/

/* Japan version (selected only keys starting with 'J'):
#define FIRST_KEY			801,001
#define LAST_KEY			1,000,000
#define _JAPAN				1
*/

/* European version 2:
#define FIRST_KEY			1,000,001
#define LAST_KEY			1,400,000
#define _JAPAN				0
*/

/* Japan version 2 (selected only keys starting with 'J'):
#define FIRST_KEY			1,400,001
#define LAST_KEY			2,700,000
#define _JAPAN				1
*/

/* European version 3:
#define FIRST_KEY			2,700,001
#define LAST_KEY			2,950,000
#define _JAPAN				0
*/

/* European / US version 4:
#define FIRST_KEY			2,950,001
#define LAST_KEY			3,200,000
#define _JAPAN				0
*/

/* Czech version:
#define FIRST_KEY			100,000,001
#define LAST_KEY			100,006,000
#define _JAPAN				0
*/

/* Polish version:
#define FIRST_KEY			100,010,001
#define LAST_KEY			100,025,000
#define _JAPAN				0
*/

/* Czech version 2:
#define FIRST_KEY			100,025,001
#define LAST_KEY			100,030,000
#define _JAPAN				0
*/

/* Czech version 3:
#define FIRST_KEY			100,030,001
#define LAST_KEY			100,034,000
#define _JAPAN				0
*/

/* Czech version 4:
#define FIRST_KEY			100,034,001
#define LAST_KEY			100,037,000
#define _JAPAN				0
*/

/* Resistance (Reserved for internal purposes)
#define FIRST_KEY			RESISTANCE_BEGIN + 1
#define LAST_KEY			RESISTANCE_BEGIN + 1000
#define _JAPAN				0
*/

/* Resistance 1
#define FIRST_KEY			RESISTANCE_BEGIN + 1001
#define LAST_KEY			RESISTANCE_BEGIN + 301000
#define _JAPAN				0
*/

/* Resistance Czech 1
#define FIRST_KEY			RESISTANCE_BEGIN + 301001
#define LAST_KEY			RESISTANCE_BEGIN + 304003
#define _JAPAN				0
*/

/* Resistance 2
#define FIRST_KEY			RESISTANCE_BEGIN + 305001
#define LAST_KEY			RESISTANCE_BEGIN + 455000
#define _JAPAN				0
*/

/* Resistance Polish 1
#define FIRST_KEY			RESISTANCE_BEGIN + 455001
#define LAST_KEY			RESISTANCE_BEGIN + 458000
#define _JAPAN				0
*/

/* Resistance Polish 2
#define FIRST_KEY			RESISTANCE_BEGIN + 458001
#define LAST_KEY			RESISTANCE_BEGIN + 461000
#define _JAPAN				0
*/

/* GOTY 1:
#define FIRST_KEY			3,200,001
#define LAST_KEY			3,700,000
#define _JAPAN				0
*/

/* Resistance Czech 2
#define FIRST_KEY			RESISTANCE_BEGIN + 461001
#define LAST_KEY			RESISTANCE_BEGIN + 464000
#define _JAPAN				0
*/

/* Resistance Russian 1
#define FIRST_KEY			RESISTANCE_BEGIN + 464001
#define LAST_KEY			RESISTANCE_BEGIN + 614000
#define _JAPAN				0
*/

/* Resistance Polish 3
#define FIRST_KEY			RESISTANCE_BEGIN + 614001
#define LAST_KEY			RESISTANCE_BEGIN + 617000
#define _JAPAN				0
*/

/* Resistance Czech 3
#define FIRST_KEY			RESISTANCE_BEGIN + 617001
#define LAST_KEY			RESISTANCE_BEGIN + 620000
#define _JAPAN				0
*/

/* Polish GOTY 1:
#define FIRST_KEY			100,037,001
#define LAST_KEY			100,040,000
#define _JAPAN				0
*/

/* Resistance Czech 4
#define FIRST_KEY			RESISTANCE_BEGIN + 620001
#define LAST_KEY			RESISTANCE_BEGIN + 620600
#define _JAPAN				0
*/

/* Czech version 5:
#define FIRST_KEY			100,040,001
#define LAST_KEY			100,040,600
#define _JAPAN				0
*/

/* Polish GOTY 2:
#define FIRST_KEY			100,040,601
#define LAST_KEY			100,043,600
#define _JAPAN				0
*/

/* Czech version 6 (Platinum edition):
#define FIRST_KEY			100,043,601
#define LAST_KEY			100,046,600
#define _JAPAN				0
*/

/* Czech version 7 (Platinum edition):
#define FIRST_KEY			100,046,601
#define LAST_KEY			100,047,600
#define _JAPAN				0
*/

/* Polish GOTY 3:
#define FIRST_KEY			100,047,601
#define LAST_KEY			100,049,600
#define _JAPAN				0
*/

/* GOTY 2:
#define FIRST_KEY			3,700,001
#define LAST_KEY			3,800,000
#define _JAPAN				0
*/

/* Czech version 8 (Platinum edition):
#define FIRST_KEY			100,049,601
#define LAST_KEY			100,050,800
#define _JAPAN				0
*/

/* Czech version 9 (Level):
#define FIRST_KEY			100,050,801
#define LAST_KEY			100,091,800
#define _JAPAN				0
*/

/* Resistance Czech 5
#define FIRST_KEY			RESISTANCE_BEGIN + 620601
#define LAST_KEY			RESISTANCE_BEGIN + 622100
#define _JAPAN				0
*/

/* GOTY 3:
#define FIRST_KEY			3,800,001
#define LAST_KEY			3,900,000
#define _JAPAN				0
*/

/* Polish GOTY 4:
#define FIRST_KEY			100,091,801
#define LAST_KEY			100,095,300
#define _JAPAN				0
*/

/* Polish version 5 (magazine):
#define FIRST_KEY			100,095,301
#define LAST_KEY			100,255,300
#define _JAPAN				0
*/

/* Czech version 10 (Platinum edition):
#define FIRST_KEY			100,255,301
#define LAST_KEY			100,257,300
#define _JAPAN				0
*/

/* Polish version 6 (Platinum edition):
#define FIRST_KEY			100,257,301
#define LAST_KEY			100,267,300
#define _JAPAN				0
*/

/* Polish version 7 (Platinum edition):
#define FIRST_KEY			100,267,301
#define LAST_KEY			100,270,300
#define _JAPAN				0
*/

/* Armed Assault - old
#define FIRST_KEY			1
#define LAST_KEY			500
#define _JAPAN				0
*/

/* Czech version 11 (Platinum edition):
#define FIRST_KEY			100,270,301
#define LAST_KEY			100,270,600
#define _JAPAN				0
*/

/* Czech version 12 (Platinum edition):
#define FIRST_KEY			100,270,601
#define LAST_KEY			100,275,600
#define _JAPAN				0
*/

/* Resistance Czech 6
#define FIRST_KEY			RESISTANCE_BEGIN + 622101
#define LAST_KEY			RESISTANCE_BEGIN + 622600
#define _JAPAN				0
*/

/* Czech version 13 (Platinum edition):
#define FIRST_KEY			100,275,601
#define LAST_KEY			100,277,600
#define _JAPAN				0
*/

/* Czech version 14 (Platinum edition):
#define FIRST_KEY			100,277,601
#define LAST_KEY			100,278,100
#define _JAPAN				0
*/

/* GOTY 4:
#define FIRST_KEY			3,900,001
#define LAST_KEY			4,100,000
#define _JAPAN				0
*/

/* Czech version 15 (Platinum edition):
#define FIRST_KEY			100,278,101
#define LAST_KEY			100,282,100
#define _JAPAN				0
*/

/* GOTY 5:
#define FIRST_KEY			4,100,001
#define LAST_KEY			4,102,500
#define _JAPAN				0
*/

/* Czech version 16 (Platinum edition):
#define FIRST_KEY			100,282,101
#define LAST_KEY			100,285,100
#define _JAPAN				0
*/

/* GOTY 6:
#define FIRST_KEY			4,102,501
#define LAST_KEY			4,602,500
#define _JAPAN				0
*/

/* Czech version 17 (Platinum edition):
#define FIRST_KEY			100,285,101
#define LAST_KEY			100,288,100
#define _JAPAN				0
*/

/* GOTY 7:
#define FIRST_KEY			4,602,501
#define LAST_KEY			4,652,500
#define _JAPAN				0
*/

/* Czech version 18 (Platinum edition):
#define FIRST_KEY			100,288,101
#define LAST_KEY			100,289,100
#define _JAPAN				0
*/

/* Czech version 19 (Platinum edition):
#define FIRST_KEY			100,289,101
#define LAST_KEY			100,295,100
#define _JAPAN				0
*/

/* GOTY 8:
#define FIRST_KEY			4,652,501
#define LAST_KEY			4,656,500
#define _JAPAN				0
*/

/* Czech version 20 (Platinum edition):
#define FIRST_KEY			100,295,101
#define LAST_KEY			100,298,100
#define _JAPAN				0
*/

/* Czech version 21 (Platinum edition):
#define FIRST_KEY			100,298,101
#define LAST_KEY			100,326,100
#define _JAPAN				0
*/

/* Cold War Assault 1
#define FIRST_KEY			4,656,501
#define LAST_KEY			4,657,500
#define _JAPAN				0
*/

/* Cold War Assault 2
#define FIRST_KEY			4,657,501
#define LAST_KEY			4,692,500
#define _JAPAN				0
*/

/* Cold War Assault 3
#define FIRST_KEY			4,692,501
#define LAST_KEY			4,792,500
#define _JAPAN				0
*/

/* Cold War Assault 4
#define FIRST_KEY			4,792,501
#define LAST_KEY			4,802,500
#define _JAPAN				0
*/

/* Cold War Assault 1 Czech
#define FIRST_KEY			100,326,101
#define LAST_KEY			100,328,600
#define _JAPAN				0
*/

/* Cold War Assault 5
#define FIRST_KEY			4,802,501
#define LAST_KEY			4,972,500
#define _JAPAN				0
*/

/* Cold War Assault 2 Czech
#define FIRST_KEY			100,328,601
#define LAST_KEY			100,333,600
#define _JAPAN				0
*/

/* Cold War Assault 2 Czech
#define FIRST_KEY			100,333,601
#define LAST_KEY			100,336,600
#define _JAPAN				0
*/

/* Cold War Assault 6
#define FIRST_KEY			4,972,501
#define LAST_KEY			5,972,500
#define _JAPAN				0
*/


/*************************************** Armed Assault ***************************************/

/* Armed Assault - BetaTest #1
#define FIRST_KEY			1
#define LAST_KEY			100
#define _JAPAN				0
static const Distribution distribution = DistBetaTest;
*/

/* Armed Assault - German #0 (marketing)
#define FIRST_KEY			11
#define LAST_KEY			100
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - Czech #0 (marketing)
#define FIRST_KEY			11
#define LAST_KEY			100
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - German #1
#define FIRST_KEY			101
#define LAST_KEY			100100
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - Czech #1
#define FIRST_KEY			101
#define LAST_KEY			10100
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - Polish #1
#define FIRST_KEY			101
#define LAST_KEY			10130
#define _JAPAN				0
static const Distribution distribution = DistPolish;
*/

/* Armed Assault - International #1
#define FIRST_KEY			1001
#define LAST_KEY			1100
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - Czech #2
#define FIRST_KEY			10101
#define LAST_KEY			11100
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - Russian #1
#define FIRST_KEY			101
#define LAST_KEY			100100
#define _JAPAN				0
static const Distribution distribution = DistRussian;
*/

/* Armed Assault - US #0 (marketing)
#define FIRST_KEY			1
#define LAST_KEY			500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - English Online #0 (marketing)
#define FIRST_KEY			1
#define LAST_KEY			500
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - International #2
#define FIRST_KEY			1101
#define LAST_KEY			73100
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - International #3
#define FIRST_KEY			73101
#define LAST_KEY			81100
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - International #4
#define FIRST_KEY			81101
#define LAST_KEY			111100
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - International #5
#define FIRST_KEY			111101
#define LAST_KEY			114100
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - International #6
#define FIRST_KEY			114101
#define LAST_KEY			120100
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - International #7
#define FIRST_KEY			120101
#define LAST_KEY			155100
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - English Online #1
#define FIRST_KEY			501
#define LAST_KEY			20500
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - International #8
#define FIRST_KEY			155101
#define LAST_KEY			185100
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - US #1
#define FIRST_KEY			501
#define LAST_KEY			50500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #2
#define FIRST_KEY			50501
#define LAST_KEY			60500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #3
#define FIRST_KEY			60501
#define LAST_KEY			63500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #4
#define FIRST_KEY			63501
#define LAST_KEY			68500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #5
#define FIRST_KEY			68501
#define LAST_KEY			78500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #6
#define FIRST_KEY			78501
#define LAST_KEY			84500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - Czech #3
#define FIRST_KEY			11101
#define LAST_KEY			13100
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - German #2
#define FIRST_KEY			100101
#define LAST_KEY			120100
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - International #9
#define FIRST_KEY			185101
#define LAST_KEY			207100
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - US #7
#define FIRST_KEY			84501
#define LAST_KEY			94500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #8 (Gold)
#define FIRST_KEY			94501
#define LAST_KEY			111500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #9 (Gold)
#define FIRST_KEY			111501
#define LAST_KEY			112500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #10 (Gold)
#define FIRST_KEY			112501
#define LAST_KEY			121500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #11 (Gold)
#define FIRST_KEY			121501
#define LAST_KEY			131500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - English Online #2
#define FIRST_KEY			20501
#define LAST_KEY			22500
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - English Online #3
#define FIRST_KEY			22501
#define LAST_KEY			24700
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - US #12 (Gold)
#define FIRST_KEY			131501
#define LAST_KEY			134500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #13 (Gold)
#define FIRST_KEY			134501
#define LAST_KEY			135500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - Russian #2
#define FIRST_KEY			100101
#define LAST_KEY			200100
#define _JAPAN				0
static const Distribution distribution = DistRussian;
*/

/* Armed Assault - German #3
#define FIRST_KEY			120101
#define LAST_KEY			120700
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - English Online #4
#define FIRST_KEY			24701
#define LAST_KEY			26700
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - International #10
#define FIRST_KEY			207101
#define LAST_KEY			257100
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - Czech #4
#define FIRST_KEY			13101
#define LAST_KEY			14100
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - German #4
#define FIRST_KEY			120701
#define LAST_KEY			130700
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - US #14 (Gold)
#define FIRST_KEY			135501
#define LAST_KEY			136500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - Russian #3
#define FIRST_KEY			200101
#define LAST_KEY			205100
#define _JAPAN				0
static const Distribution distribution = DistRussian;
*/

/* Armed Assault - German #5
#define FIRST_KEY			130701
#define LAST_KEY			140700
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - English Online #5
#define FIRST_KEY			26701
#define LAST_KEY			27700
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - International #11
#define FIRST_KEY			257101
#define LAST_KEY			258200
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - Czech #5
#define FIRST_KEY			14101
#define LAST_KEY			19100
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - International #12
#define FIRST_KEY			258201
#define LAST_KEY			259300
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - Czech #6
#define FIRST_KEY			19101
#define LAST_KEY			47100
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - International #13
#define FIRST_KEY			259301
#define LAST_KEY			262450
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - English Online #6
#define FIRST_KEY			27701
#define LAST_KEY			52700
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - German #6
#define FIRST_KEY			140701
#define LAST_KEY			145700
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - English Online #7
#define FIRST_KEY			52701
#define LAST_KEY			53700
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - Czech #7
#define FIRST_KEY			47101
#define LAST_KEY			50600
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - International #14
#define FIRST_KEY			262451
#define LAST_KEY			273450
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - International #15
#define FIRST_KEY			273451
#define LAST_KEY			278450
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - International #16
#define FIRST_KEY			278451
#define LAST_KEY			288450
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - Polish #2
#define FIRST_KEY			10131
#define LAST_KEY			16130
#define _JAPAN				0
static const Distribution distribution = DistPolish;
*/

/* Armed Assault - German #7
#define FIRST_KEY			145701
#define LAST_KEY			148700
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - International #17
#define FIRST_KEY			288451
#define LAST_KEY			340450
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - US #15
#define FIRST_KEY			136501
#define LAST_KEY			156500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - English Online #8
#define FIRST_KEY			53701
#define LAST_KEY			54700
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - Czech #8
#define FIRST_KEY			50601
#define LAST_KEY			51350
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - US #16
#define FIRST_KEY			156501
#define LAST_KEY			176500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - US #17
#define FIRST_KEY			176501
#define LAST_KEY			186500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/* Armed Assault - German #8
#define FIRST_KEY			148701
#define LAST_KEY			154700
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - Czech #9
#define FIRST_KEY			51351
#define LAST_KEY			72350
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - English Online #9
#define FIRST_KEY			54701
#define LAST_KEY			65700
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - International #18
#define FIRST_KEY			340451
#define LAST_KEY			440450
#define _JAPAN				0
static const Distribution distribution = DistInternational;
*/

/* Armed Assault - English Online #10
#define FIRST_KEY			65701
#define LAST_KEY			75700
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - Czech #10
#define FIRST_KEY			72351
#define LAST_KEY			74850
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - English Online #11
#define FIRST_KEY			75701
#define LAST_KEY			175700
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - German #9
#define FIRST_KEY			154701
#define LAST_KEY			174700
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - German #10
#define FIRST_KEY			174701
#define LAST_KEY			374700
#define _JAPAN				0
static const Distribution distribution = DistGerman;
*/

/* Armed Assault - Czech #11
#define FIRST_KEY			74851
#define LAST_KEY			84850
#define _JAPAN				0
static const Distribution distribution = DistCzech;
*/

/* Armed Assault - English Online #12
#define FIRST_KEY			175701
#define LAST_KEY			975700
#define _JAPAN				0
static const Distribution distribution = DistEnglishOnline;
*/

/* Armed Assault - US #18
#define FIRST_KEY			186501
#define LAST_KEY			986500
#define _JAPAN				0
static const Distribution distribution = DistUS;
*/

/*************************************** SecuROM ***************************************/

/* SecuROM Internal #1
#define FIRST_KEY			1
#define LAST_KEY			100
#define _JAPAN				0
*/

/* SecuROM Internal #2
#define FIRST_KEY			101
#define LAST_KEY			200
#define _JAPAN				0
*/

/* SecuROM Internal #3
#define FIRST_KEY			201
#define LAST_KEY			300
#define _JAPAN				0
*/

/* SecuROM Internal #4
#define FIRST_KEY			301
#define LAST_KEY			400
#define _JAPAN				0
*/

/*************************************** ArmA 2 ***************************************/

/* ArmA 2 - Testing keys
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistUS;
static const Distribution distribution = DistCzech;
static const Distribution distribution = DistGerman;
static const Distribution distribution = DistHungarian;
static const Distribution distribution = DistPolish;
static const Distribution distribution = DistRussian;
static const Distribution distribution = DistInternational;
static const Distribution distribution = DistOnline;
static const Distribution distribution = DistBrasilian;
*/

/* ArmA 2 - German #1
#define FIRST_KEY			101
#define LAST_KEY			70100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 - Czech #1
#define FIRST_KEY			101
#define LAST_KEY			7100
static const Distribution distribution = DistCzech;
*/

/* ArmA 2 - International #1
#define FIRST_KEY			101
#define LAST_KEY			200
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - International #2
#define FIRST_KEY			201
#define LAST_KEY			300
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - International #3
#define FIRST_KEY			301
#define LAST_KEY			55300
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - US #1
#define FIRST_KEY			101
#define LAST_KEY			30100
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Polish #1
#define FIRST_KEY			101
#define LAST_KEY			7100
static const Distribution distribution = DistPolish;
*/

/* ArmA 2 - Hungarian #1
#define FIRST_KEY			101
#define LAST_KEY			2100
static const Distribution distribution = DistHungarian;
*/

/* ArmA 2 - Russian #1
#define FIRST_KEY			101
#define LAST_KEY			60100
static const Distribution distribution = DistRussian;
*/

/* ArmA 2 - Online #1
#define FIRST_KEY			101
#define LAST_KEY			4350
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #2
#define FIRST_KEY			30101
#define LAST_KEY			34350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - German #2
#define FIRST_KEY			70101
#define LAST_KEY			73100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 - International #4
#define FIRST_KEY			55301
#define LAST_KEY			70300
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - International #5
#define FIRST_KEY			70301
#define LAST_KEY			85300
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - German #3
#define FIRST_KEY			73101
#define LAST_KEY			78100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 - US #3
#define FIRST_KEY			34351
#define LAST_KEY			49350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #2
#define FIRST_KEY			4351
#define LAST_KEY			19350
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - Czech #2
#define FIRST_KEY			7101
#define LAST_KEY			7800
static const Distribution distribution = DistCzech;
*/

/* ArmA 2 - International #6
#define FIRST_KEY			85301
#define LAST_KEY			89300
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - International #7
#define FIRST_KEY			89301
#define LAST_KEY			89500
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - US #4
#define FIRST_KEY			49351
#define LAST_KEY			54350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - International #8
#define FIRST_KEY			89501
#define LAST_KEY			91500
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - International #9
#define FIRST_KEY			91501
#define LAST_KEY			111500
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - US #5
#define FIRST_KEY			54351
#define LAST_KEY			55350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #3
#define FIRST_KEY			19351
#define LAST_KEY			20350
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #6
#define FIRST_KEY			55351
#define LAST_KEY			56350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - International #10
#define FIRST_KEY			111501
#define LAST_KEY			151500
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - US #7
#define FIRST_KEY			56351
#define LAST_KEY			59350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #4
#define FIRST_KEY			20351
#define LAST_KEY			22350
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - Polish #2
#define FIRST_KEY			7101
#define LAST_KEY			10100
static const Distribution distribution = DistPolish;
*/

/* ArmA 2 - US #8
#define FIRST_KEY			59351
#define LAST_KEY			60350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - US #9
#define FIRST_KEY			60351
#define LAST_KEY			70350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - International #11
#define FIRST_KEY			151501
#define LAST_KEY			186500
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - US #10
#define FIRST_KEY			70351
#define LAST_KEY			120350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - International #12
#define FIRST_KEY			186501
#define LAST_KEY			236500
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - US #11
#define FIRST_KEY			120351
#define LAST_KEY			121350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Hungarian #2
#define FIRST_KEY			2101
#define LAST_KEY			3100
static const Distribution distribution = DistHungarian;
*/

/* ArmA 2 - US #12
#define FIRST_KEY			121351
#define LAST_KEY			131350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - German #4 (Black Edition)
#define FIRST_KEY			78101
#define LAST_KEY			84100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 - US #13
#define FIRST_KEY			131351
#define LAST_KEY			191350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #5
#define FIRST_KEY			22351
#define LAST_KEY			82350
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - German #5
#define FIRST_KEY			84101
#define LAST_KEY			86100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 - Online #6
#define FIRST_KEY			82351
#define LAST_KEY			83350
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - Online #7
#define FIRST_KEY			83351
#define LAST_KEY			84650
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - German #6
#define FIRST_KEY			86101
#define LAST_KEY			96100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 - Hungarian #3
#define FIRST_KEY			3101
#define LAST_KEY			5600
static const Distribution distribution = DistHungarian;
*/

/* ArmA 2 - Online #8
#define FIRST_KEY			84651
#define LAST_KEY			104650
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #14
#define FIRST_KEY			191351
#define LAST_KEY			211350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Hungarian #4
#define FIRST_KEY			5601
#define LAST_KEY			8600
static const Distribution distribution = DistHungarian;
*/

/* ArmA 2 - Hungarian #5
#define FIRST_KEY			8601
#define LAST_KEY			18600
static const Distribution distribution = DistHungarian;
*/

/* ArmA 2 - Online #9
#define FIRST_KEY			104651
#define LAST_KEY			106650
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #15
#define FIRST_KEY			211351
#define LAST_KEY			213350
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #10
#define FIRST_KEY			106651
#define LAST_KEY			107650
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - Online #11
#define FIRST_KEY			107651
#define LAST_KEY			111050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - Czech #3
#define FIRST_KEY			7801
#define LAST_KEY			9800
static const Distribution distribution = DistCzech;
*/

/* ArmA 2 - Hungarian #6
#define FIRST_KEY			18601
#define LAST_KEY			19100
static const Distribution distribution = DistHungarian;
*/

/* ArmA 2 - US #16
#define FIRST_KEY			213351
#define LAST_KEY			218550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - US #17
#define FIRST_KEY			218551
#define LAST_KEY			220550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - US #18
#define FIRST_KEY			220551
#define LAST_KEY			228550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Polish #3
#define FIRST_KEY			10101
#define LAST_KEY			130100
static const Distribution distribution = DistPolish;
*/

/* ArmA 2 - Online #12
#define FIRST_KEY			111051
#define LAST_KEY			116050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #19
#define FIRST_KEY			228551
#define LAST_KEY			278550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #13
#define FIRST_KEY			116051
#define LAST_KEY			166050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #20
#define FIRST_KEY			278551
#define LAST_KEY			313550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #14
#define FIRST_KEY			166051
#define LAST_KEY			173050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - Online #15
#define FIRST_KEY			173051
#define LAST_KEY			183050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - Czech #4
#define FIRST_KEY			9801
#define LAST_KEY			12300
static const Distribution distribution = DistCzech;
*/

/* ArmA 2 - Online #16
#define FIRST_KEY			183051
#define LAST_KEY			233050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - German #7
#define FIRST_KEY			96101
#define LAST_KEY			116100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 - Hungarian #7
#define FIRST_KEY			19101
#define LAST_KEY			20100
static const Distribution distribution = DistHungarian;
*/

/* ArmA 2 - German #7
#define FIRST_KEY			116101
#define LAST_KEY			316100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 - Polish #4
#define FIRST_KEY			130101
#define LAST_KEY			160100
static const Distribution distribution = DistPolish;
*/

/* ArmA 2 - US #21
#define FIRST_KEY			313551
#define LAST_KEY			343550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #17
#define FIRST_KEY			233051
#define LAST_KEY			333050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #22
#define FIRST_KEY			343551
#define LAST_KEY			443550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #18
#define FIRST_KEY			333051
#define LAST_KEY			433050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #23
#define FIRST_KEY			443551
#define LAST_KEY			593550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #19
#define FIRST_KEY			433051
#define LAST_KEY			633050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #24
#define FIRST_KEY			593551
#define LAST_KEY			793550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - German #8
#define FIRST_KEY			316101
#define LAST_KEY			326100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 - Online #20
#define FIRST_KEY			633051
#define LAST_KEY			733050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - Online #21
#define FIRST_KEY			733051
#define LAST_KEY			933050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #25
#define FIRST_KEY			793551
#define LAST_KEY			993550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Czech #5
#define FIRST_KEY			12301
#define LAST_KEY			13950
static const Distribution distribution = DistCzech;
*/


/* ArmA 2 - German #9
#define FIRST_KEY			326101
#define LAST_KEY			346100
static const Distribution distribution = DistGerman;
*/


/* ArmA 2 - Czech #6
#define FIRST_KEY			13951
#define LAST_KEY			14950
static const Distribution distribution = DistCzech;
*/

/* ArmA 2 - Czech #7
#define FIRST_KEY			14951
#define LAST_KEY			24950
static const Distribution distribution = DistCzech;
*/

/* ArmA 2 - Online #22
#define FIRST_KEY			933051
#define LAST_KEY			1433050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - US #26
#define FIRST_KEY			993551
#define LAST_KEY			1493550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Brasilian #1
#define FIRST_KEY			101
#define LAST_KEY			20100
static const Distribution distribution = DistBrasilian;
*/

/* ArmA 2 - International #13
#define FIRST_KEY			236501
#define LAST_KEY			246500
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - German #10
#define FIRST_KEY			346101
#define LAST_KEY			396100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 - Russian #2
#define FIRST_KEY			60101
#define LAST_KEY			80100
static const Distribution distribution = DistRussian;
*/

/* ArmA 2 - International #14
#define FIRST_KEY			246501
#define LAST_KEY			296500
static const Distribution distribution = DistInternational;
*/

/* ArmA 2 - US #27
#define FIRST_KEY			1493551
#define LAST_KEY			1993550
static const Distribution distribution = DistUS;
*/

/* ArmA 2 - Online #23
#define FIRST_KEY			1433051
#define LAST_KEY			1933050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - Online #24
#define FIRST_KEY			1933051
#define LAST_KEY			2933050
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 - Online #25
#define FIRST_KEY			2933051
#define LAST_KEY			3433050
static const Distribution distribution = DistOnline;
*/

/*************************************** ArmA 2 OAH ***************************************/

/* ArmA 2 OAH - English #1 (Preview)
#define FIRST_KEY			1
#define LAST_KEY			200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #2 (Preview)
#define FIRST_KEY			201
#define LAST_KEY			300
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - US #1 (Preview)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistUS;
*/

/* ArmA 2 OAH - German #1 (Preview)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 OAH - Polish #1 (Preview)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistPolish;
*/

/* ArmA 2 OAH - Russian #1 (Preview)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistRussian;
*/

/* ArmA 2 OAH - Czech #1 (Preview)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistCzech;
*/

/* ArmA 2 OAH - Euro #1 (Preview)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Hungarian #1 (Preview)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistHungarian;
*/

/* ArmA 2 OAH - Brasilian #1 (Preview)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistBrasilian;
*/

/* ArmA 2 OAH - US #2 
#define FIRST_KEY			101
#define LAST_KEY			40100
static const Distribution distribution = DistUS;
*/

/* ArmA 2 OAH - Euro #2
#define FIRST_KEY			101
#define LAST_KEY			20100
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Russian #2
#define FIRST_KEY			101
#define LAST_KEY			95100
static const Distribution distribution = DistRussian;
*/

/* ArmA 2 OAH - English #3 (Hungary)
#define FIRST_KEY			301
#define LAST_KEY			1300
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #4 (Singapore)
#define FIRST_KEY			1301
#define LAST_KEY			3100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #5 (Tchajwan)
#define FIRST_KEY			3101
#define LAST_KEY			5100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #6 (Thailand)
#define FIRST_KEY			5101
#define LAST_KEY			6100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #7 (Japan)
#define FIRST_KEY			6101
#define LAST_KEY			8700
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #8
#define FIRST_KEY			8701
#define LAST_KEY			13950
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #9
#define FIRST_KEY			13951
#define LAST_KEY			16950
static const Distribution distribution = DistEnglish;
*/


/* ArmA 2 OAH - German #2
#define FIRST_KEY			101
#define LAST_KEY			26100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 OAH - English #10
#define FIRST_KEY			16951
#define LAST_KEY			21950
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - Euro #3
#define FIRST_KEY			20101
#define LAST_KEY			21100
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #4
#define FIRST_KEY			21101
#define LAST_KEY			22600
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #5
#define FIRST_KEY			22601
#define LAST_KEY			47600
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #6
#define FIRST_KEY			47601
#define LAST_KEY			49600
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #7
#define FIRST_KEY			49601
#define LAST_KEY			51600
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #8
#define FIRST_KEY			51601
#define LAST_KEY			54600
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - US #3 
#define FIRST_KEY			40101
#define LAST_KEY			55100
static const Distribution distribution = DistUS;
*/

/* ArmA 2 OAH - Euro #9
#define FIRST_KEY			54601
#define LAST_KEY			56600
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - English #11
#define FIRST_KEY			21951
#define LAST_KEY			23250
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #12
#define FIRST_KEY			23251
#define LAST_KEY			28250
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #13
#define FIRST_KEY			28251
#define LAST_KEY			28500
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - Euro #10
#define FIRST_KEY			56601
#define LAST_KEY			57650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Polish #2
#define FIRST_KEY			101
#define LAST_KEY			7100
static const Distribution distribution = DistPolish;
*/

/* ArmA 2 OAH - Euro #11
#define FIRST_KEY			57651
#define LAST_KEY			58650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #12
#define FIRST_KEY			58651
#define LAST_KEY			83650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - US #4 
#define FIRST_KEY			55101
#define LAST_KEY			60200
static const Distribution distribution = DistUS;
*/

/* ArmA 2 OAH - US #5 
#define FIRST_KEY			60201
#define LAST_KEY			65400
static const Distribution distribution = DistUS;
*/

/* ArmA 2 OAH - German #3
#define FIRST_KEY			26101
#define LAST_KEY			36100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 OAH - English #14
#define FIRST_KEY			28501
#define LAST_KEY			30500
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #15
#define FIRST_KEY			30501
#define LAST_KEY			32500
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - Euro #13
#define FIRST_KEY			83651
#define LAST_KEY			84650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - English #16
#define FIRST_KEY			32501
#define LAST_KEY			35800
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - Euro #14
#define FIRST_KEY			84651
#define LAST_KEY			124650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #15
#define FIRST_KEY			124651
#define LAST_KEY			126650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - English #17
#define FIRST_KEY			35801
#define LAST_KEY			36800
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #18
#define FIRST_KEY			36801
#define LAST_KEY			37800
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - English #19
#define FIRST_KEY			37801
#define LAST_KEY			41200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - US #6 
#define FIRST_KEY			65401
#define LAST_KEY			67400
static const Distribution distribution = DistUS;
*/


/* ArmA 2 OAH - US #7 
#define FIRST_KEY			67401
#define LAST_KEY			70400
static const Distribution distribution = DistUS;
*/

/* ArmA 2 OAH - English #20
#define FIRST_KEY			41201
#define LAST_KEY			43200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH - Euro #16
#define FIRST_KEY			124651
#define LAST_KEY			126650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #17
#define FIRST_KEY			126651
#define LAST_KEY			226650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #18
#define FIRST_KEY			226651
#define LAST_KEY			246650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - German #4
#define FIRST_KEY			36101
#define LAST_KEY			46100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 OAH - Russian #3
#define FIRST_KEY			95101
#define LAST_KEY			145100
static const Distribution distribution = DistRussian;
*/

/* ArmA 2 OAH - Polish #3
#define FIRST_KEY			7101
#define LAST_KEY			37100
static const Distribution distribution = DistPolish;
*/

/* ArmA 2 OAH - US #7
#define FIRST_KEY			70401
#define LAST_KEY			100400
static const Distribution distribution = DistUS;
*/

/* ArmA 2 OAH - Euro #19
#define FIRST_KEY			246651
#define LAST_KEY			346650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #20
#define FIRST_KEY			346651
#define LAST_KEY			596650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #21
#define FIRST_KEY			596651
#define LAST_KEY			796650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #22
#define FIRST_KEY			796651
#define LAST_KEY			1046650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - German #5
#define FIRST_KEY			46101
#define LAST_KEY			56100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 OAH - Euro #23
#define FIRST_KEY			1046651
#define LAST_KEY			1146650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - German #6
#define FIRST_KEY			56101
#define LAST_KEY			66100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 OAH - Euro #24
#define FIRST_KEY			1146651
#define LAST_KEY			1346650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - German #7
#define FIRST_KEY			66101
#define LAST_KEY			86100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 OAH - US #8
#define FIRST_KEY			100401
#define LAST_KEY			110400
static const Distribution distribution = DistUS;
*/

/* ArmA 2 OAH - Czech #2 
#define FIRST_KEY			101
#define LAST_KEY			10100
static const Distribution distribution = DistCzech;
*/

/* ArmA 2 OAH - Euro #25
#define FIRST_KEY			1346651
#define LAST_KEY			1546650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #26
#define FIRST_KEY			1546651
#define LAST_KEY			2046650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #27
#define FIRST_KEY			2046651
#define LAST_KEY			2096650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - US #9
#define FIRST_KEY			110401
#define LAST_KEY			160400
static const Distribution distribution = DistUS;
*/

/* ArmA 2 OAH - German #8
#define FIRST_KEY			86101
#define LAST_KEY		 106100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 OAH - US #10
#define FIRST_KEY			160401
#define LAST_KEY			210400
static const Distribution distribution = DistUS;
*/

/* ArmA 2 OAH - Euro #28
#define FIRST_KEY			2096651
#define LAST_KEY			2596650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Russian #4
#define FIRST_KEY			145101
#define LAST_KEY			245100
static const Distribution distribution = DistRussian;
*/

/* ArmA 2 OAH - German #9
#define FIRST_KEY		 106101
#define LAST_KEY		 156100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 OAH - Euro #29
#define FIRST_KEY			2596651
#define LAST_KEY			3096650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - Euro #30
#define FIRST_KEY			3096651
#define LAST_KEY			4096650
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 OAH - US #10
#define FIRST_KEY			210401
#define LAST_KEY			710400
static const Distribution distribution = DistUS;
*/

/*************************************** ArmA 2 OAH BAF ***************************************/

/* ArmA 2 OAH BAF - English #1 (Preview - with securom keys from preview OA)
#define FIRST_KEY			1
#define LAST_KEY			45
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #2 (Preview)
#define FIRST_KEY			46
#define LAST_KEY			200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #3 (Steam)
#define FIRST_KEY			201
#define LAST_KEY			5200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #4 (Sprocket)
#define FIRST_KEY			5201
#define LAST_KEY			7200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #5 (Online)
D2D - 1000
Gamersgate - 1000
Meridian4 - 1000
Impulse - 1000

#define FIRST_KEY			7201
#define LAST_KEY			11200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #5 (Online)
Sprocket - 2000
#define FIRST_KEY			11201
#define LAST_KEY			13200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #6 (Online)
Gamersgate - 2000
#define FIRST_KEY			13201
#define LAST_KEY			15200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #7 (Online)
Steam - 2000

#define FIRST_KEY			15201
#define LAST_KEY			30200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #8 (Online)
Steam - 2000

#define FIRST_KEY			30201
#define LAST_KEY			32200
static const Distribution distribution = DistEnglish;

/* ArmA 2 OAH BAF - English #9 (Online)
Steam - 40000
#define FIRST_KEY			32201
#define LAST_KEY			72200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #10 (Online)
GamersGate - 2000
#define FIRST_KEY			72201
#define LAST_KEY			74200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #11 (Online)
SprocketIdea - 2000
#define FIRST_KEY			74201
#define LAST_KEY			76200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #12 (Online)
Steam - 100000
#define FIRST_KEY			76201
#define LAST_KEY		 176200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #13 (Online)
Impulse/GOO - 20000
#define FIRST_KEY		 176201
#define LAST_KEY		 196200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #14 (Online)
Impulse/GOO - 20000
#define FIRST_KEY		 196201
#define LAST_KEY		 216200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #15 (Online)
Impulse/GOO - 150000
#define FIRST_KEY		 216201
#define LAST_KEY		 366200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH BAF - English #16 (Online)
Steam/Online - 500000
#define FIRST_KEY		366201
#define LAST_KEY		 866200
static const Distribution distribution = DistEnglish;
*/

/*************************************** ArmA 2 OAH PMC ***************************************/

/* ArmA 2 OAH PMC - English #1 (internal)
Steam - 2000
#define FIRST_KEY			1
#define LAST_KEY			101
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #2 (Sprocket)
Steam - 2000
#define FIRST_KEY			101
#define LAST_KEY			4100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #3 (Steam)
Steam - 2000
#define FIRST_KEY			4101
#define LAST_KEY			19100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #4 (Impulse)
Steam - 2000
#define FIRST_KEY			19101
#define LAST_KEY			23100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #5 (Impulse)
Steam - 2000
#define FIRST_KEY			23101
#define LAST_KEY			25100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #6
Steam - 2000
#define FIRST_KEY			25101
#define LAST_KEY			65100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #7
Steam - 2000
#define FIRST_KEY			65101
#define LAST_KEY			67100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #8
Steam - 2000
#define FIRST_KEY			67101
#define LAST_KEY			69100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #9
#define FIRST_KEY			67101
#define LAST_KEY			69100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #10
#define FIRST_KEY			69101
#define LAST_KEY			169100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #11
#define FIRST_KEY			169101
#define LAST_KEY			189100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #12
#define FIRST_KEY			189101
#define LAST_KEY			209100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #13
#define FIRST_KEY			209101
#define LAST_KEY			359100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 OAH PMC - English #14
#define FIRST_KEY			359101
#define LAST_KEY			859100
static const Distribution distribution = DistEnglish;
*/

/*************************************** ArmA 2 Free ***************************************/

/* ArmA 2 Free - Online #1
#define FIRST_KEY			1
#define LAST_KEY			1000
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 Free - Online #2
#define FIRST_KEY			1001
#define LAST_KEY			501000
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 Free - Online #3
#define FIRST_KEY			501001
#define LAST_KEY			1001000
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 Free - Online #4
#define FIRST_KEY			1001001
#define LAST_KEY			1251000
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 Free - Online #5
#define FIRST_KEY			1251001
#define LAST_KEY			1451000
static const Distribution distribution = DistOnline;
*/

/* ArmA 2 Free - Online #6
#define FIRST_KEY			1451001
#define LAST_KEY			2451000
static const Distribution distribution = DistOnline;
*/

/*************************************** ArmA 2 RFT ***************************************/

/* ArmA 2 RFT - Online #1
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 RFT - czech #1
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistCzech;
*/

/* ArmA 2 RFT - euro #1
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 RFT - english #2
#define FIRST_KEY			101
#define LAST_KEY			200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 RFT - german #1
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 RFT - russian #1
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistRussian;
*/

/* ArmA 2 RFT - english #2 (japan)
#define FIRST_KEY			201
#define LAST_KEY			2200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 RFT - euro #2
#define FIRST_KEY			101
#define LAST_KEY			18000
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 RFT - english #3
#define FIRST_KEY			2201
#define LAST_KEY			4200
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 RFT - english #4 (asia)
#define FIRST_KEY			4201
#define LAST_KEY			8210
static const Distribution distribution = DistEnglish;
*/

/* ArmA 2 RFT - russian #2
#define FIRST_KEY			101
#define LAST_KEY			17100
static const Distribution distribution = DistRussian;
*/

/* ArmA 2 RFT - russian #3
#define FIRST_KEY			17101
#define LAST_KEY			22100
static const Distribution distribution = DistRussian;
*/

/* ArmA 2 RFT - euro #3
#define FIRST_KEY			18001
#define LAST_KEY			21150
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 RFT - german #2
#define FIRST_KEY			101
#define LAST_KEY			10100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 RFT - US #1
#define FIRST_KEY			1
#define LAST_KEY			11000 
static const Distribution distribution = DistUS;
*/

/* ArmA 2 RFT - russian #4
#define FIRST_KEY			22101
#define LAST_KEY			27100
static const Distribution distribution = DistRussian;
*/

/* ArmA 2 RFT - euro #4
#define FIRST_KEY			21151
#define LAST_KEY			22850
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 RFT - euro #5
#define FIRST_KEY			22851
#define LAST_KEY			23850
static const Distribution distribution = DistEuro;
*/

/* ArmA 2 RFT - german #3
#define FIRST_KEY			10101
#define LAST_KEY			20100
static const Distribution distribution = DistGerman;
*/

/* ArmA 2 RFT - euro #6
#define FIRST_KEY			23851
#define LAST_KEY			48850
static const Distribution distribution = DistEuro;
*/

/*************************************** TakeOn helicopters ***************************************/

/* TakeOn helicopters - English #1 (internal)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistEnglish;
*/

/* TakeOn helicopters - US #1 (internal)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistUS;
*/

/* TakeOn helicopters - Czech #1 (internal)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistCzech;
*/

/* TakeOn helicopters - English #2 (internal)
#define FIRST_KEY			101
#define LAST_KEY			300
static const Distribution distribution = DistEnglish;
*/

/* TakeOn helicopters - English #3 (internal)
#define FIRST_KEY			301
#define LAST_KEY			400
static const Distribution distribution = DistEnglish;
*/

/* TakeOn helicopters - Czech #2 (internal)
#define FIRST_KEY			101
#define LAST_KEY			200
static const Distribution distribution = DistCzech;
*/

/* TakeOn helicopters - International #1 (internal)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistInternational;
*/

/* TakeOn helicopters - International #2 
#define FIRST_KEY			101
#define LAST_KEY			20100
static const Distribution distribution = DistInternational;
*/

/* TakeOn helicopters - International #3 
#define FIRST_KEY			20101
#define LAST_KEY			40100
static const Distribution distribution = DistInternational;
*/

/* TakeOn helicopters - International #4 
#define FIRST_KEY			40101
#define LAST_KEY			80100
static const Distribution distribution = DistInternational;
*/

/* TakeOn helicopters - Euro #1
#define FIRST_KEY			1
#define LAST_KEY			10000
static const Distribution distribution = DistEuro;
*/

/* TakeOn helicopters - Polish #1
#define FIRST_KEY			1
#define LAST_KEY			5000
static const Distribution distribution = DistPolish;
*/

/* TakeOn helicopters - German #1
#define FIRST_KEY			1
#define LAST_KEY			20000
static const Distribution distribution = DistGerman;
*/

/* TakeOn helicopters - Russian #1
#define FIRST_KEY			1
#define LAST_KEY			20000
static const Distribution distribution = DistRussian;
*/

/* TakeOn helicopters - English #4
#define FIRST_KEY			401
#define LAST_KEY			20400
static const Distribution distribution = DistEnglish;
*/

/* TakeOn helicopters - English #5
#define FIRST_KEY			20401
#define LAST_KEY			60400
static const Distribution distribution = DistEnglish;
*/

/* TakeOn helicopters - Czech #3 
#define FIRST_KEY			201
#define LAST_KEY			10200
static const Distribution distribution = DistCzech;
*/

/* TakeOn helicopters - Polish #2 
#define FIRST_KEY			5001
#define LAST_KEY			15000
static const Distribution distribution = DistPolish;
*/

/* TakeOn helicopters - International #5
#define FIRST_KEY			80101
#define LAST_KEY			130100
static const Distribution distribution = DistInternational;
*/

/* TakeOn helicopters - Japanese #1
#define FIRST_KEY			1
#define LAST_KEY			25000
static const Distribution distribution = DistJapanese;
*/

/* TakeOn helicopters - Italian #1
#define FIRST_KEY			1
#define LAST_KEY			25000
static const Distribution distribution = DistItalian;
*/

/* TakeOn helicopters - Spanish #1
#define FIRST_KEY			1
#define LAST_KEY			25000
static const Distribution distribution = DistSpanish;
*/

/* TakeOn helicopters - Russian #2
#define FIRST_KEY			20001
#define LAST_KEY			60000
static const Distribution distribution = DistRussian;
*/

/* TakeOn helicopters - International #6
#define FIRST_KEY			130101
#define LAST_KEY			1130100
static const Distribution distribution = DistInternational;
*/

/* TakeOn helicopters - International #7
#define FIRST_KEY			1130101
#define LAST_KEY			1630100
static const Distribution distribution = DistInternational;
*/

/*************************************** ArmAX ***************************************/

/* ArmAX  - test US #1 (internal)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistUS;
*/

/* ArmAX  - US #2 
#define FIRST_KEY			101
#define LAST_KEY			11100
static const Distribution distribution = DistUS;
*/

/* ArmAX  - US #3 
#define FIRST_KEY			11101
#define LAST_KEY			21100
static const Distribution distribution = DistUS;
*/


/* ArmAX  - US #4
#define FIRST_KEY			21101
#define LAST_KEY			31100
static const Distribution distribution = DistUS;
*/

/* ArmAX  - CZ #1
#define FIRST_KEY			1
#define LAST_KEY			2500
static const Distribution distribution = DistCzech;
*/

/* ArmAX  - US #5
#define FIRST_KEY			31101
#define LAST_KEY			51100
static const Distribution distribution = DistUS;
*/

/* ArmAX  - german #5
#define FIRST_KEY			1
#define LAST_KEY			20000
static const Distribution distribution = DistGerman;
*/

/* ArmAX  - english #1
#define FIRST_KEY			1
#define LAST_KEY			20000
static const Distribution distribution = DistEnglish;
*/

/* ArmAX  - euro #1
#define FIRST_KEY			1
#define LAST_KEY			20000
static const Distribution distribution = DistEuro;
*/

/* ArmAX  - english #2
#define FIRST_KEY			20001
#define LAST_KEY			70000
static const Distribution distribution = DistEnglish;
*/

/* ArmAX  - euro #2
#define FIRST_KEY			20001
#define LAST_KEY			70000
static const Distribution distribution = DistEuro;
*/

/* ArmAX  - german #2
#define FIRST_KEY			20001
#define LAST_KEY			70000
static const Distribution distribution = DistGerman;
*/

/*************************************** TakeOn Helicopters Hind **********************************/

/* TakeOn Hind - English #1 (internal)
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistEnglish;
*/

/* TakeOn Hind - International #1
#define FIRST_KEY			1
#define LAST_KEY			50000
static const Distribution distribution = DistInternational;
*/

/* TakeOn Hind - International #2
#define FIRST_KEY			50001
#define LAST_KEY			60000
static const Distribution distribution = DistInternational;
*/

/* TakeOn Hind - International #3
#define FIRST_KEY			60001
#define LAST_KEY			70000
static const Distribution distribution = DistInternational;
*/

/* TakeOn Hind - International #4
#define FIRST_KEY			70001
#define LAST_KEY		 270000
static const Distribution distribution = DistInternational;
*/

/*************************************** Carrier Command *************************************/

/*
Carrier Command International #1
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistInternational;
*/

/*
Carrier Command International #2
#define FIRST_KEY			101
#define LAST_KEY			100100
static const Distribution distribution = DistInternational;
*/

/*
Carrier Command Euro #1
#define FIRST_KEY			1
#define LAST_KEY			100000
static const Distribution distribution = DistEuro;
*/

/*************************************** Liberation *************************************/

/*
Liberation English #1
#define FIRST_KEY			1
#define LAST_KEY			25000
static const Distribution distribution = DistEuro;
*/

/*
Liberation Euro #1
#define FIRST_KEY			1
#define LAST_KEY			200
static const Distribution distribution = DistEuro;
*/

/*
Liberation Euro #2
#define FIRST_KEY			201
#define LAST_KEY			100200
static const Distribution distribution = DistEuro;
*/

/*
Liberation Euro #3
#define FIRST_KEY			100201
#define LAST_KEY			150200
static const Distribution distribution = DistEuro;
*/

/*
Liberation Euro #4
#define FIRST_KEY			150201
#define LAST_KEY			180200
static const Distribution distribution = DistEuro;
*/

/*
Liberation Euro #5
#define FIRST_KEY			180201
#define LAST_KEY			230200
static const Distribution distribution = DistEuro;
*/

/*************************************** Arma2 ACR **************************************/

/*
Arma2 ACR #1
#define FIRST_KEY			1
#define LAST_KEY			100
static const Distribution distribution = DistEnglish;
*/

/*
Arma2 ACR #2
#define FIRST_KEY			101
#define LAST_KEY			10100
static const Distribution distribution = DistEnglish;
*/

/*
Arma2 ACR #3
#define FIRST_KEY			10101
#define LAST_KEY			20100
static const Distribution distribution = DistEnglish;
*/

/*
Arma2 ACR #4
#define FIRST_KEY			20101
#define LAST_KEY			35100
static const Distribution distribution = DistEnglish;
*/

/*
Arma2 ACR #5
#define FIRST_KEY			35101
#define LAST_KEY			135100
static const Distribution distribution = DistEnglish;
*/

/*
Arma2 ACR #6
#define FIRST_KEY			135101
#define LAST_KEY			335100
static const Distribution distribution = DistEnglish;
*/

/*
Arma2 ACR #7
#define FIRST_KEY			335101
#define LAST_KEY			835100
static const Distribution distribution = DistEnglish;
*/

#define FIRST_KEY			4972501
#define LAST_KEY			5972500
#define _JAPAN				0

const char *UsableChars = "0123456789ABCDEFGHJKLMNPRSTVWXYZ";

// encode / decode 120 bit number into / from CD KEY

void EncodeMsg(const unsigned char *msg, char *buffer)
{
	int s = 4;
	for (int i=0; i<3; i++)
	{
		unsigned __int64 value = 0;
		for (int j=0; j<5; j++)
		{
			value <<= 8;
			value += *(msg++);
		}
		for (int k=0; k<8; k++)
		{
			if (s == 0)
			{
				*buffer++ = '-';
				s = 5; 
			}
			unsigned __int64 ch = (byte)(value & 0x1F);
			value >>= 5;

			*buffer++ = UsableChars[ch];
			s--;
		}
	}
	*buffer = 0;
}

bool DecodeMsg(unsigned char *msg, const char *buffer)
{
	for (int i=0; i<3; i++)
	{
		unsigned __int64 value = 0;
		int offset = 0;
		for (int k=0; k<8; k++)
		{
			int c = *buffer++;
			while (c && !isalnum((int)c)) c = *buffer++;
			if (!c) return false;
			c = toupper(c);
			const char *pos = strchr(UsableChars, c);
			if (!pos) return false;
			c = pos - UsableChars;
			unsigned __int64 cc = c;
			value |= cc << offset;
			offset += 5;
		}
		offset = 4 * 8;
		for (int j=0; j<5; j++)
		{
			*msg++ = (byte)((value >> offset) & 0xff);
			offset -= 8;
		}
	}
	return true;
}

// create / output MPI number from / into buffer

MPI CreateMPI(BYTE *buffer, DWORD size)
{
	MPI val = MPI_NULL;
	mpi_limb_t a;

	int nbytes = size;
	int nlimbs = (size + BYTES_PER_MPI_LIMB - 1) / BYTES_PER_MPI_LIMB;
	val = mpi_alloc(nlimbs);

	buffer += size - 1;

	int i = BYTES_PER_MPI_LIMB - nbytes % BYTES_PER_MPI_LIMB;
	i %= BYTES_PER_MPI_LIMB;
	val->nbits = nbytes * 8;
	int j = val->nlimbs = nlimbs;
	val->sign = 0;
	for( ; j > 0; j-- )
	{
		a = 0;
		for(; i < BYTES_PER_MPI_LIMB; i++)
		{
			a <<= 8;
			a |= *buffer--;
		}
		i = 0;
		val->d[j-1] = a;
	}
	return val;
}

void OutputMPI(MPI &val, BYTE *buffer, DWORD size)
{
	mpi_limb_t a;

	int nbytes = size;
	int nlimbs = (size + BYTES_PER_MPI_LIMB - 1) / BYTES_PER_MPI_LIMB;

	buffer += size - 1;

	int i = BYTES_PER_MPI_LIMB - nbytes % BYTES_PER_MPI_LIMB;
	i %= BYTES_PER_MPI_LIMB;
	int j = nlimbs;
	for( ; j > 0; j-- )
	{
		a = val->d[j-1];
		int offset = (BYTES_PER_MPI_LIMB - i - 1) * 8;
		for(; i < BYTES_PER_MPI_LIMB; i++)
		{
			*buffer-- = (a >> offset) & 0xff;
			offset -= 8;
		}
		i = 0;
	}
}

// save / load public or private key into / from file

void SaveKey(const char *name, RSA_public_key &key)
{
	FILE *out;
	out = fopen(name, "wb");
	if (!out) return;
	BYTE buffer[NBYTES];

	// public exponent
	OutputMPI(key.e, buffer, 4);
	fwrite(buffer, sizeof(BYTE), 4, out);
	// modulus
	OutputMPI(key.n, buffer, NBYTES);
	fwrite(buffer, sizeof(BYTE), NBYTES, out);

	fclose(out);
}

void SaveKey(const char *name, RSA_secret_key &key)
{
	FILE *out;
	out = fopen(name, "wb");
	if (!out) return;
	BYTE buffer[NBYTES];

	// public exponent
	OutputMPI(key.e, buffer, 4);
	fwrite(buffer, sizeof(BYTE), 4, out);
	// modulus
	OutputMPI(key.n, buffer, NBYTES);
	fwrite(buffer, sizeof(BYTE), NBYTES, out);
	// first prime
	OutputMPI(key.p, buffer, NBYTES / 2);
	fwrite(buffer, sizeof(BYTE), NBYTES / 2, out);
	// second prime
	OutputMPI(key.q, buffer, NBYTES / 2);
	fwrite(buffer, sizeof(BYTE), NBYTES / 2, out);
	// coefficient
	OutputMPI(key.u, buffer, NBYTES / 2);
	fwrite(buffer, sizeof(BYTE), NBYTES / 2, out);
	// private exponent
	OutputMPI(key.d, buffer, NBYTES);
	fwrite(buffer, sizeof(BYTE), NBYTES, out);

	fclose(out);
}

bool LoadKey(const char *name, RSA_public_key &key)
{
	FILE *in;
	in = fopen(name, "rb");
	if (!in) return false;
	BYTE buffer[NBYTES];

	// public exponent
	fread(buffer, sizeof(BYTE), 4, in);
	key.e = CreateMPI(buffer, 4);
	// modulus
	fread(buffer, sizeof(BYTE), NBYTES, in);
	key.n = CreateMPI(buffer, NBYTES);

	fclose(in);
	return true;
}

bool LoadKey(const char *name, RSA_secret_key &key)
{
	FILE *in;
	in = fopen(name, "rb");
	if (!in) return false;
	BYTE buffer[NBYTES];

	// public exponent
	fread(buffer, sizeof(BYTE), 4, in);
	key.e = CreateMPI(buffer, 4);
	// modulus
	fread(buffer, sizeof(BYTE), NBYTES, in);
	key.n = CreateMPI(buffer, NBYTES);
	// first prime
	fread(buffer, sizeof(BYTE), NBYTES / 2, in);
	key.p = CreateMPI(buffer, NBYTES / 2);
	// second prime
	fread(buffer, sizeof(BYTE), NBYTES / 2, in);
	key.q = CreateMPI(buffer, NBYTES / 2);
	// coefficient
	fread(buffer, sizeof(BYTE), NBYTES / 2, in);
	key.u = CreateMPI(buffer, NBYTES / 2);
	// private exponent
	fread(buffer, sizeof(BYTE), NBYTES, in);
	key.d = CreateMPI(buffer, NBYTES);

	fclose(in);
	return true;
}

static void CreateKey(RSA_secret_key &secretKey)
{
	// Try to import key
	if (LoadKey(FILE_PRIVATE_KEY, secretKey)) return;

	// Create keys
	generate(&secretKey, NBITS);

	RSA_public_key publicKey;
	publicKey.e = secretKey.e;
	publicKey.n = secretKey.n;
	SaveKey(FILE_PUBLIC_KEY, publicKey);
	SaveKey(FILE_PRIVATE_KEY, secretKey);
}

/*

// Extended versions of EncodeMsg, DecodeMsg working with any CD key size
// Could not be used directly, coding is different (in the original functions some permutation of message occurs)

static void EncodeMsgEx(const unsigned char *msg, char *buffer, int bits)
{
  // grouping of characters
  static const int CharsInGroup = 5;
  int chars = (bits + 5 - 1) / 5; // total number of characters in the output
  int group = chars % CharsInGroup; // characters remaining in the current group
  if (group == 0) group = 5;

  // the first currently processing bit in *msg
  int msgBit = 8;
  
  // do piece of work while some data remains
  while (bits > 0)
  {
    int value;
    if (msgBit >= 5)
    {
      value = (*msg) >> (msgBit - 5);
    }
    else
    {
      // higher byte
      value = (*msg) << (5 - msgBit);
      // add lower byte (only if present in msg)
      if (bits - msgBit > 0)
      {
        msg++;
        msgBit += 8;
        value |= (*msg) >> (msgBit - 5);
      }
    }
    value &= (1 << 5) - 1; // mask - leave only 5 lower bits
    msgBit -= 5;
    bits -= 5;

    // remove obsolete bits (lower -bits)
    if (bits < 0)
    {
      int mask = (1 << -bits) - 1;
      value &= ~mask;
    }

    // write value to the output
    *buffer++ = UsableChars[value];

    if (--group == 0 && bits > 0)
    {
      // new group of characters
      *buffer++ = '-';
      group = CharsInGroup;
    }
  }
  *buffer = 0;
}

static bool DecodeMsgEx(unsigned char *msg, const char *buffer, int bits)
{
  // the first currently processing bit in *buffer
  int bufferBit = 0;

  int c = 0; // read character can be valid during more iterations
  while (bits > 0)
  {
    int value = 0;
    int msgBits = 8; // bits need to be processed to *msg
    while (msgBits > 0)
    {
      // move to the next character if needed
      if (bufferBit == 0)
      {
        bufferBit = 5;

        // read the valid character from buffer
        c = *buffer++;
        while (c && !isalnum((int)c)) c = *buffer++;
        // check if really valid
        if (!c) return false;
        c = toupper(c);
        // FIX: there are no letters O and I in serial number
        // if users enters it, it must be misread number
        if (c == 'O') c = '0';
        else if (c == 'I') c = '1';
        // convert character to the raw value
        const char *pos = strchr(UsableChars, c);
        if (!pos) return false;
        c = pos - UsableChars;
      }
      // select only valid bits from c
      int cBits = c & ((1 << bufferBit) - 1);
      // read only bits we need
      if (bufferBit > msgBits)
      {
        value <<= msgBits;
        value |= cBits >> (bufferBit - msgBits);

        bufferBit -= msgBits;
        msgBits = 0;
      }
      else
      {
        value <<= bufferBit;
        value |= cBits;

        msgBits -= bufferBit;
        bufferBit = 0;
      }
    }
    
    // write the byte to the output
    *msg++ = value;
    bits -= 8;
  }

  // handle bits < 0 (free remaining bits)
  if (bits < 0)
  {
    int mask = (1 << -bits) - 1;
    *(msg - 1) &= ~mask;
  }

  return true;
}
*/

int main(int argc, char* argv[])
{
#if PROCESS_INPUT
	argv++;
	argc--;
	if (argc != 2)
	{
		printf("Usage\n");
		printf("   CDKeys first last\n");
		printf("   first - # of first key\n");
		printf("   last - # of last key\n");
		return 1;		
	}

	int FirstKey = atoi(argv[0]);
	int LastKey = atoi(argv[1]);

#else
	int FirstKey = FIRST_KEY;
	int LastKey = LAST_KEY;
#endif
	
	srand( (unsigned)time( NULL ) );

	RSA_secret_key secretKey;
	CreateKey(secretKey);

	RSA_public_key publicKey;
	publicKey.e = secretKey.e;
	publicKey.n = secretKey.n;

#if DISTRIBUTIONS
  char filename[256];
  sprintf(filename, FILE_OUTPUT, DistributionNames[distribution], FirstKey, LastKey);
  FILE *list = fopen(filename, "wt");
#else
  char filename[256];
  sprintf(filename, FILE_OUTPUT, FirstKey, LastKey);
  FILE *list = fopen(filename, "wt");
#endif
	if (!list) return -1;

#if OUTPUT_SQL
  char filenameSQL[256];
  #if DISTRIBUTIONS
    sprintf(filenameSQL, FILE_SQL, DistributionNames[distribution], FirstKey, LastKey);
  #else
    sprintf(filenameSQL, FILE_SQL, FirstKey, LastKey);
  #endif
  FILE *listSQL = fopen(filenameSQL, "wt");
#endif

#if NEW_KEYS
  if (LastKey >= (1 << PLAYER_BITS))
  {
    printf("Error: id too large %d", LastKey);
    return -1;
  }
  if (distribution >= (1 << DISTR_BITS))
  {
    printf("Error: distribution too large %d", distribution);
    return -1;
  }

  // initialize the MS CryptoAPI
  HCRYPTPROV provider = NULL;
  if (!AcquireContext(&provider, false)) 
  {
    printf("Error: AcquireContext failed 0x%x", GetLastError());
    return -1;
  }

  for (int i=FirstKey; i<=LastKey; i++)
  {
    // encode id and distribution to the message
    unsigned int message = (i << DISTR_BITS) | distribution;
    // add a simple signature
    unsigned int signature =
      ((unsigned char *)&message)[0] * 7 +
      ((unsigned char *)&message)[1] * 11 +
      ((unsigned char *)&message)[2] * 13 +
      ((unsigned char *)&message)[3] * 17;
    signature = signature & ((1 << SIMPLE_HASH_BITS) - 1);
    message |= signature << (DISTR_BITS + PLAYER_BITS);
    
    // calculate the SHA hash
    HCRYPTHASH handle = NULL;
    if (!CryptCreateHash(provider, CALG_SHA, NULL, 0, &handle))
    {
      printf("Error: CryptCreateHash failed 0x%x", GetLastError());
      return -1;
    }
    if (!CryptHashData(handle, (BYTE *)&message, sizeof(message), 0))
    {
      printf("Error: CryptHashData failed 0x%x", GetLastError());
      return -1;
    }
    // export the hash
    DWORD size = 0;
    if (!CryptGetHashParam(handle, HP_HASHVAL, NULL, &size, 0)) 
    {
      printf("Error: CryptGetHashParam failed 0x%x", GetLastError());
      return -1;
    }
    unsigned char *buffer = new unsigned char[size];
    if (!CryptGetHashParam(handle, HP_HASHVAL, buffer, &size, 0))
    {
      printf("Error: CryptGetHashParam failed 0x%x", GetLastError());
      return -1;
    }
    if (handle) CryptDestroyHash(handle);
    unsigned char hashByte = buffer[0];
    delete [] buffer;
    
    // prepare the message
    unsigned char binaryKey[15];
    memset(binaryKey, 0, sizeof(binaryKey));
    *((unsigned int *)binaryKey) = message; // id, distribution, simple hash
    binaryKey[4] = hashByte; // single byte from the SHA hash
    
    // GameSpy key - use RSA signature as a source of "random" data
    MPI output = mpi_alloc(mpi_get_nlimbs(publicKey.n));
    MPI input = CreateMPI((BYTE *)binaryKey, 15);
    secr(output, input, &secretKey);
    unsigned char binaryOutput[15];
    OutputMPI(output, (BYTE *)binaryOutput, 15);	
    m_free(input);
    m_free(output);
    // copy part of RSA signature to the key
    for (int j=5; j<14; j++) binaryKey[j] = binaryOutput[j];

    // the last byte will contain the CRC-8 to easy check validity
    unsigned char crc = 0;
    for (int j=0; j<14; j++) UpdateCRC8(crc, binaryKey[j]);
    binaryKey[14] = crc ^ PRODUCT_ID;

    // write CD Key into file
    char textKey[64];
    EncodeMsg(binaryKey, textKey);
    fprintf(list, "%s", textKey);
#if OUTPUT_BINARY
    for (int j=0; j<15; j++) fprintf(list, " %02x", binaryKey[j]);
#endif
    fprintf(list, "\n");

#if OUTPUT_SQL
    fprintf(listSQL, "INSERT INTO CDKeys (CDKey) VALUES ('%s');\n", textKey);
#endif

    // output to the registry key
#if OUTPUT_REG
    char filename[256];
#if DISTRIBUTIONS
    sprintf(filename, REG_PATH, DistributionNames[distribution], i);
#else
    sprintf(filename, REG_PATH, i);
#endif
    FILE *reg = fopen(filename, "wt");
    if (reg)
    {
      fprintf(reg, REG_PREFIX);
      for (int j=0; j<15; j++)
      {
        if (j > 0) fprintf(reg, ",");
        fprintf(reg, "%02x", binaryKey[j]);
      }
      fclose(reg);
    }
#endif
  }

  if (provider) CryptReleaseContext(provider, 0);
  fclose(list);
#if OUTPUT_SQL
  fclose(listSQL);
#endif

#else // NEW_KEYS

  MPI output = mpi_alloc(mpi_get_nlimbs(publicKey.n));
  MPI check = mpi_alloc(mpi_get_nlimbs(publicKey.n));

  const unsigned char init[15] = FIRST_MESSAGE;

#if DISTRIBUTIONS
  (*((__int64 *)init)) += 256 * (__int64)FirstKey + distribution;
#else
	(*((__int64 *)init)) += FirstKey;
#endif

#if ARMA
  if (LastKey >= (1 << 20))
  {
    printf("Error: invalid id %d", LastKey);
    return -1;
  }
#endif

	for (int i=FirstKey; i<=LastKey; i++)
	{
		// create message and encode
		MPI input = CreateMPI((BYTE *)init, 15);
		secr(output, input, &secretKey);
		m_free(input);

		// write CD Key into file
		unsigned char bufOutput[16]; bufOutput[15] = 0;
		OutputMPI(output, (BYTE *)bufOutput, 15);	
		char textBuffer[64];
		EncodeMsg(bufOutput, textBuffer);
		
#if _JAPAN
		if (textBuffer[0] != 'J') goto Next;
#endif
		
		fprintf(list, "%s", textBuffer);
#if OUTPUT_BINARY
		for (int j=0; j<15; j++) fprintf(list, " %02x", bufOutput[j]);
#endif
		fprintf(list, "\n");

#if OUTPUT_SQL
    fprintf(listSQL, "INSERT INTO CDKeys (CDKey) VALUES ('%s');\n", textBuffer);
#endif

#if OUTPUT_REG
    char filename[256];
#if DISTRIBUTIONS
    sprintf(filename, REG_PATH, DistributionNames[distribution], i);
#else
    sprintf(filename, REG_PATH, i);
#endif
    FILE *reg = fopen(filename, "wt");
    if (reg)
    {
      fprintf(reg, REG_PREFIX);
      for (int j=0; j<15; j++)
      {
        if (j > 0) fprintf(reg, ",");
        fprintf(reg, "%02x", bufOutput[j]);
      }
      fclose(reg);
    }
#endif

		unsigned char bufOutput2[16];
		DecodeMsg(bufOutput2, textBuffer);

		// decode message
		publ(check, output, &publicKey);
		unsigned char bufCheck[15];
		OutputMPI(check, (BYTE *)bufCheck, 15);

		// check it
		if (memcmp(init, bufCheck, 15) != 0)
		{
			printf("Error: publ(secr(key)) != key");
			return -1;
		}

		// next message
Next:
#if DISTRIBUTIONS
    (*((__int64 *)init)) += 256;
#else
		(*((__int64 *)init))++;
#endif
	}
	fclose(list);
#if OUTPUT_SQL
  fclose(listSQL);
#endif

	m_free(check);
	m_free(output);
	// TODO: free private key
#endif // NEW_KEYS

	return 0;
}
