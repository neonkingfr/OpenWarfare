// TreeGenerator.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include <El/elementpch.hpp>
#include <Es/Framework/appFrame.hpp>
#include <El/Evaluator/express.hpp>
#include <El/Modules/modules.hpp>

#include <RV/Tree/mytree.h>
#include <stdio.h>
#include <d3d8.h>

// Set the compiling priority of this file to be high
#if _MSC_VER && !defined INIT_SEG_COMPILER
	#pragma warning(disable:4074)
	#pragma init_seg(compiler)
	#define INIT_SEG_COMPILER
#endif

#define MAX_LOD_NUMBER 64

// Create and register own logging class
class TreeditorFrameFunctions : public AppFrameFunctions {
private:
  // File to redirect logging to
  FILE *_file;
public:
	TreeditorFrameFunctions() {
    _file = fopen("debug.log", "w");
  };
	virtual ~TreeditorFrameFunctions() {
    if (_file) fclose(_file);
  };
#if _ENABLE_REPORT
	virtual void LogF(const char *format, va_list argptr)
	{
    if (_file) {
		  char buf[1024];
		  vsprintf(buf,format,argptr);
      fprintf(_file, "%s\n", buf);
    }
	}
#endif
};
static TreeditorFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;

void Generate(CTreeEngine &treeEngine, RString fileName) {

  Matrix4 origin;
  origin.SetPosition(Vector3(0, 0, 0));
  origin.SetDirectionAndUp(Vector3(0, 1, 0), Vector3(1, 0, 0));

  // Open the source param file
  ParamFile pf;
  pf.Parse(fileName);
  int entryCount = pf.GetEntryCount();

  // Open merge param file
  ParamFile pfMerge;
  //pfMerge.Parse("merged.ptm");
  ParamClass *pMainMergeClass = pfMerge.AddClass("InfTexMerge");

  // Get common parameters
  ParamEntry *item;
	item = pf.FindEntry("inFolder");
  RString inFolder;
  if (!item) {
    LogF("Entry 'inFolder' doesn't exist.");
    return;
  }
  else {
    inFolder = *item;
  }
	item = pf.FindEntry("outFolder");
  RString outFolder;
  if (!item) {
    LogF("Entry 'outFolder' doesn't exist.");
    return;
  }
  else {
    outFolder = *item;
  }
	item = pf.FindEntry("texturePrefix");
  RString texturePrefix;
  if (!item) {
    LogF("Entry 'texturePrefix' doesn't exist.");
    return;
  }
  else {
    texturePrefix = *item;
  }
	item = pf.FindEntry("enlightPolyplanes");
  bool enlightPolyplanes;
  if (!item) {
    LogF("Entry 'enlightPolyplanes' doesn't exist.");
    return;
  }
  else {
    enlightPolyplanes = *item;
  }

  // For all classes
  for (int i = 0; i < entryCount; i++) {

    const ParamEntry &subEntry = pf.GetEntry(i);

		item = subEntry.FindEntry("polyplaneMaxTextureSize");
    int polyplaneMaxTextureSize;
    if (!item) {
      LogF("Entry 'polyplaneMaxTextureSize' doesn't exist."); continue;
    }
    else {
      polyplaneMaxTextureSize = item->GetInt();
    }
		item = subEntry.FindEntry("polyplaneCurrTextureSize");
    int polyplaneCurrTextureSize;
    if (!item) {
      LogF("Entry 'polyplaneCurrTextureSize' doesn't exist."); continue;
    }
    else {
      polyplaneCurrTextureSize = item->GetInt();
    }

/*
    RString inFolder = subEntry.FindEntry("inFolder")->GetValue();
    RString outFolder = subEntry.FindEntry("outFolder")->GetValue();
    RString texturePrefix = subEntry.FindEntry("texturePrefix")->GetValue();
    int polyplaneMaxTextureSize = subEntry.FindEntry("polyplaneMaxTextureSize")->GetInt();
    int polyplaneCurrTextureSize = subEntry.FindEntry("polyplaneCurrTextureSize")->GetInt();
*/

    
    ParamEntry *itemsArray = subEntry.FindEntry("items");
    if (!itemsArray) {
      LogF("Entry 'items' doesn't exist.");
    }
    else {
      if (!itemsArray->IsArray()) {
        LogF("Entry 'items' is not an array.");
      }
      else {
        int itemsArraySize = itemsArray->GetSize();
        // For all items
        for (int j = 0; j < itemsArraySize; j++) {
          RString itemName = (*itemsArray)[j];
          // Load the plant
          if (!treeEngine.LoadPlantFromFile(inFolder + RString("\\") + itemName + RString(".txt"))) {
            LogF("Entry while loading the plant '%s' from file.", itemName.Data());
          }
          else {


            ParamEntry *ageArray = subEntry.FindEntry("age");
            if (!ageArray) {
              LogF("Entry 'age' doesn't exist.");
            }
            else {
              if (!ageArray->IsArray()) {
                LogF("Entry 'age' is not an array.");
              }
              else {

                // Save new layer to ptm file
                ParamClass *pLODMergeClass = pMainMergeClass->AddClass(itemName);
                ParamClass *pLODItemsMergeClass = pLODMergeClass->AddClass("Items");
                pLODMergeClass->Add("w", 8.0f);
                pLODMergeClass->Add("h", 8.0f);
                pLODMergeClass->Add("alpha", 0.0f);
                pLODMergeClass->Add("file", itemName + RString(".pac"));
                pLODMergeClass->AddArray("models");

                int ageArraySize = ageArray->GetSize();
                // For all ages
                for (int l = 0; l < ageArraySize; l++) {
                  float age = (*ageArray)[l];

                  StringArray LODTextures[MAX_LOD_NUMBER];
                  int LODTexturesCount = 0;

      		        ParamEntry *seedArray = subEntry.FindEntry("seed");
                  if (!itemsArray) {
                    LogF("Entry 'seed' doesn't exist.");
                  }
                  else {
                    if (!itemsArray->IsArray()) {
                      LogF("Entry 'seed' is not an array.");
                    }
                    else {
                      int seedArraySize = seedArray->GetSize();
                      // For all seeds
                      for (int k = 0; k < seedArraySize; k++) {
                        int seed = (*seedArray)[k];

/*
      		  ParamEntry *seedArray = subEntry.FindEntry("seed");
            if (!itemsArray) {
              LogF("Entry 'seed' doesn't exist.");
            }
            else {
              if (!itemsArray->IsArray()) {
                LogF("Entry 'seed' is not an array.");
              }
              else {
                int seedArraySize = seedArray->GetSize();
                // For all seeds
                for (int k = 0; k < seedArraySize; k++) {
                  int seed = (*seedArray)[k];
*/



/*
                  ParamEntry *ageArray = subEntry.FindEntry("age");
                  if (!ageArray) {
                    LogF("Entry 'age' doesn't exist.");
                  }
                  else {
                    if (!ageArray->IsArray()) {
                      LogF("Entry 'age' is not an array.");
                    }
                    else {
                      int ageArraySize = ageArray->GetSize();
                      // For all ages
                      for (int l = 0; l < ageArraySize; l++) {
                        float age = (*ageArray)[l];
*/




                        ParamEntry *detailArray = subEntry.FindEntry("detail");
                        if (!detailArray) {
                          LogF("Entry 'detail' doesn't exist.");
                        }
                        else {
                          if (!detailArray->IsArray()) {
                            LogF("Entry 'detail' is not an array.");
                          }
                          else {
                            // Remove all levels
                            treeEngine.DeleteLODObjectLevels();
                            // Cycle through all details
                            int detailArraySize = detailArray->GetSize();
                            LODTexturesCount = detailArraySize;
                            // For all details
                            for (int m = 0; m < detailArraySize; m++) {
                              float detail = (*detailArray)[m];

                              int polyplaneTextureSize;
                              if (m == 0) {
                                polyplaneTextureSize = polyplaneMaxTextureSize;
                              }
                              else {
                                polyplaneTextureSize = polyplaneCurrTextureSize;
                              }
                              treeEngine.PreparePlantData(seed, 1.0f, origin, age, detail, texturePrefix + itemName, polyplaneTextureSize, enlightPolyplanes, NULL);
                              
                              // Add a level to the tree
                              treeEngine.AddLevelToLODObject(1 << m);

                              // Add involved textures
                              treeEngine.AddPolyplaneTextureNames(LODTextures[m]);
                              
                              // Create a param entry for texture merging
/*
                              char s[256];
                              sprintf(s, "%s_%d_%d", itemName.Data(), seed, m);
                              ParamClass *pLODMergeClass = pMainMergeClass->AddClass(s);
                              ParamClass *pLODItemsMergeClass = pLODMergeClass->AddClass("Items");
                              pLODMergeClass->Add("w", 0.0f);
                              pLODMergeClass->Add("h", 0.0f);
                              pLODMergeClass->Add("alpha", 0.0f);
                              pLODMergeClass->Add("file", texturePrefix + itemName);
*/

                            }

                            // Delete the first level created by default
                            treeEngine.DeleteLODObjectFirstLevel();

                            // Save the tree with all details to file
                            char s[256];
                            sprintf(s, "%s\\%s_%d_%f", outFolder.Data(), itemName.Data(), seed, age);
                            char *ps = strchr(s, '.');
                            if (ps != NULL) {
                              ps[0] = 'd';
                            }
                            treeEngine.SaveLODObject(RString(s) + RString(".p3d"));
                          }
                        }
                      }
                    }
                  }
/*                  
                  // Save new layers to ptm file
                  for (int x = 0; x < LODTexturesCount; x++) {
                    if (LODTextures[x].Size() > 0) {
                      
                      char s[256];
                      sprintf(s, "%s_%f_%d", itemName.Data(), age, x);
                      char *ps = strchr(s, '.');
                      if (ps != NULL) {
                        ps[0] = 'd';
                      }
                      ParamClass *pLODMergeClass = pMainMergeClass->AddClass(s);
                      ParamClass *pLODItemsMergeClass = pLODMergeClass->AddClass("Items");
                      pLODMergeClass->Add("w", 8.0f);
                      pLODMergeClass->Add("h", 8.0f);
                      pLODMergeClass->Add("alpha", 0.0f);
                      pLODMergeClass->Add("file", RString(s) + RString(".pac"));
                      for (int y = 0; y < LODTextures[x].Size(); y++) {
                        ParamClass *pItemClass = pLODItemsMergeClass->AddClass(LODTextures[x][y]);

                      }
                    }
                  }
*/

                }
                
                // Save all the textures from polyplane space
                treeEngine.SavePolyplaneSpace(outFolder + RString("\\") + itemName);

                // Save entries from polyplane space
                treeEngine.UpdateMergeFile(pLODItemsMergeClass, texturePrefix, itemName);
              }
            }
          }
        }
      }
    }
  }
  pfMerge.Save(outFolder + RString("\\merged.ptm"));
}

// The Main
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
  // Check the program argument
  //strcmp(lpCmdLine, "") == 0;

  // Create the D3D object
	IDirect3D8 *pd3d8;
  pd3d8 = Direct3DCreate8(D3D_SDK_VERSION);
  if (pd3d8 == NULL) {
    MessageBox(NULL, "Error", "Failed to create d3d object.", MB_OK);
    return 0;
  }

	// Get default display mode
	D3DDISPLAYMODE mode;
	HRESULT hr = pd3d8->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &mode);
	if (FAILED(hr)) {
    MessageBox(NULL, "Error", "Failed to retrieve the display mode.", MB_OK);
    return 0;
	}

  // Create some window to be associated with the d3d device

  // Set the window's initial style
  DWORD dwWindowStyle = WS_OVERLAPPED|WS_CAPTION|
    WS_SYSMENU|WS_THICKFRAME|
    WS_MINIMIZEBOX|WS_VISIBLE;

  // Set the window's initial width
  RECT rc;
  SetRect(&rc, 0, 0, 100, 100);
  AdjustWindowRect(&rc, dwWindowStyle, TRUE);

  // Register the window class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX); 
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= DefWindowProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= NULL;
	wcex.hCursor		= NULL;
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= "D3D Window";
	wcex.hIconSm		= NULL;
	int result = RegisterClassEx(&wcex);
  if (result == 0) {
    MessageBox(NULL, "Error", "Failed to register the window class.", MB_OK);
    return 0;
  }

  // Create the render window
  HWND hWnd = CreateWindow(
    "D3D Window", "TreeGenerator", dwWindowStyle,
    CW_USEDEFAULT, CW_USEDEFAULT,
    (rc.right-rc.left), (rc.bottom-rc.top),
    0L, NULL, hInstance, 0L);
  if (hWnd == NULL) {
    MessageBox(NULL, "Error", "Failed to create the window.", MB_OK);
    return 0;
  }
  
	// Set Direct3D parameters
	D3DPRESENT_PARAMETERS d3dpp; 
	ZeroMemory (&d3dpp, sizeof (d3dpp));
	d3dpp.Windowed = TRUE;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.BackBufferFormat = mode.Format;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;

	// Create device
  IDirect3DDevice8* pd3dDevice8;
	hr = pd3d8->CreateDevice (
    D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_MIXED_VERTEXPROCESSING, &d3dpp, &pd3dDevice8);
	if (FAILED(hr)) {
    MessageBox(NULL, "Error", "Failed to create the d3d device.", MB_OK);
    return 0;
	}

	// Enable alpha testing (skips pixels with less than a certain alpha.)
	hr = pd3dDevice8->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
  hr = pd3dDevice8->SetRenderState(D3DRS_ALPHAREF, 128);
	hr = pd3dDevice8->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATER);
	if (FAILED(hr)) {
    MessageBox(NULL, "Error", "Failed while setting the render state.", MB_OK);
    return 0;
	}

  // Initialize defined script modules
	InitModules();
  
  // Initialize the tree engine
  CTreeEngine *pTreeEngine = new CTreeEngine();
  pTreeEngine->Init(pd3dDevice8);

  // Generate trees
  Generate(*pTreeEngine, RString(lpCmdLine));

  // Cleanup
  delete pTreeEngine;
  pd3dDevice8->Release();
  DestroyWindow(hWnd);
  UnregisterClass("D3D Window", hInstance);
  pd3d8->Release();

	return 1;
}
