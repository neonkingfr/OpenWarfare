// Configuration parameters for internal release / debug version of FP2

// used for ArmA datadisc build now

#undef _ENABLE_CONVERSATION
#undef _ENABLE_IDENTITIES
#undef _ENABLE_INDEPENDENT_AGENTS
#undef _ENABLE_EDITOR2

#define _ENABLE_CONVERSATION        1
#define _ENABLE_IDENTITIES          1
#define _ENABLE_INDEPENDENT_AGENTS  1
#define _ENABLE_EDITOR2             1
#define _ENABLE_EDITOR2_MP          0
/*
#undef _VERIFY_KEY
#undef _VERIFY_BLACKLIST
#undef _ENABLE_NEWHEAD
#undef _ENABLE_SKINNEDINSTANCING
#undef _ENABLE_BB_TREES  
#undef _ENABLE_CONVERSATION
#undef _ENABLE_SPEECH_RECOGNITION
#undef _ENABLE_EDITOR2
#undef _ENABLE_IDENTITIES
#undef _ENABLE_INVENTORY
#undef _ENABLE_MISSION_CONFIG
#undef _ENABLE_INDEPENDENT_AGENTS
#undef _ENABLE_DIRECT_MESSAGES
#undef _ENABLE_FILE_FUNCTIONS
#undef _ENABLE_HAND_IK
#undef _ENABLE_WALK_ON_GEOMETRY
#undef _ENABLE_BULDOZER
#undef _ENABLE_DISTRIBUTIONS
#undef _USE_FCPHMANAGER
#undef _ENABLE_DX10

#define _VERIFY_KEY								0 // avoid CD key in FP2 demo
#define _VERIFY_BLACKLIST					0
#define _ENABLE_NEWHEAD           1
#define _ENABLE_SKINNEDINSTANCING 0
#define _ENABLE_BB_TREES          1
#define _ENABLE_CONVERSATION      1
#define _ENABLE_SPEECH_RECOGNITION  1
#define _ENABLE_EDITOR2           1
#define _ENABLE_EDITOR2_MP        0
#define _ENABLE_IDENTITIES        1
#define _ENABLE_INVENTORY         1
#define _ENABLE_MISSION_CONFIG    1
#define _ENABLE_INDEPENDENT_AGENTS  1
#define _ENABLE_DIRECT_MESSAGES   1
#define _ENABLE_FILE_FUNCTIONS    1
#define _ENABLE_HAND_IK           1
#define _ENABLE_WALK_ON_GEOMETRY  1
#define _ENABLE_BULDOZER          1
#define _ENABLE_DISTRIBUTIONS     0
#define _USE_FCPHMANAGER          1
#define _ENABLE_DX10              0

#ifdef _DISABLE_CRC_PROTECTION
#undef _DISABLE_CRC_PROTECTION
#endif

#ifdef _FORCE_DS_CONTEXT
#undef _FORCE_DS_CONTEXT
#endif

#define _DISABLE_CRC_PROTECTION   1
#define _FORCE_DS_CONTEXT         1

// Testing of initial signatures test
#define _ACCEPT_ONLY_SIGNED_DATA  0
// Accepted public keys
#define ACCEPTED_KEYS { \
  { \
    "BI Test", \
    { \
      0x06, 0x02, 0x00, 0x00, 0x00, 0x24, 0x00, 0x00, 0x52, 0x53, 0x41, 0x31, \
      0x00, 0x04, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x4F, 0x70, 0xB2, 0x54, 0x2F, 0xA9, 0xE8, 0x8D, \
      0xF2, 0x7E, 0x30, 0xCD, 0xC1, 0xC8, 0xFD, 0x17, 0x88, 0xCC, 0x0A, 0xB3, 0x9C, 0xD2, 0xC2, 0xCA, \
      0x02, 0xCF, 0x11, 0x48, 0x1C, 0x24, 0x8C, 0x6B, 0x6E, 0x1C, 0x49, 0xC4, 0xF9, 0x7B, 0x5C, 0x72, \
      0x6E, 0x49, 0x92, 0x6C, 0x37, 0xC7, 0x16, 0xED, 0x46, 0xC6, 0xE7, 0x5E, 0xD0, 0xAF, 0xC1, 0x4D, \
      0x50, 0x51, 0x70, 0xA0, 0x1C, 0xA4, 0x94, 0x6E, 0x26, 0x78, 0x56, 0x0E, 0xCE, 0x67, 0x61, 0x00, \
      0x52, 0x63, 0x46, 0x9E, 0x5A, 0xB9, 0x83, 0x13, 0xBB, 0xB7, 0xF1, 0x90, 0xBA, 0x83, 0x0C, 0xAD, \
      0xC3, 0x1A, 0xF8, 0x0E, 0x4A, 0xF2, 0xA8, 0x13, 0x52, 0x33, 0x06, 0xFA, 0xF0, 0x01, 0x68, 0x42, \
      0xE6, 0x7C, 0x43, 0x30, 0x6D, 0xB4, 0xCB, 0xCD, 0x52, 0x8A, 0x4F, 0xE8, 0x7C, 0x45, 0xE7, 0xD7, \
      0xB9, 0x0B, 0xAA, 0x7A, 0x65, 0xE6, 0xC3, 0xCE} \
  } \
}
// Additional checked files
#define ADDITIONAL_SIGNED_FILES { \
  "bin\\config.bin" \
}
*/

