#if _PROFILE
# pragma optimize( "", off )
#endif

/*!
\file
implementation for Transport class
*/
#include "wpch.hpp"
#include "transport.hpp"
#include "AI/ai.hpp"
#include "proxyWeapon.hpp"
#include "person.hpp"
#include "global.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include <El/Common/randomGen.hpp>
#include "keyInput.hpp"
#include "diagModes.hpp"
#include "soldierOld.hpp"
#include "shots.hpp"

#include "Marina/carCollisions.hpp" // new physics

#include "paramArchiveExt.hpp"
#include "stringtableExt.hpp"
#include "sentences.hpp"
#include "camera.hpp"
#include "fileLocator.hpp"

#include "Network/network.hpp"
#include "UI/chat.hpp"
#include "UI/uiActions.hpp"
#include "integrity.hpp"

#include "Shape/specLods.hpp"

#include <El/Enum/enumNames.hpp>
#include <El/Common/perfProf.hpp>

#include "txtPreload.hpp"

#include "mbcs.hpp"
#include "progress.hpp"

#if _VBS2 // interact with vehs
#include "gameStateExt.hpp"

enum ExtCtrl {EC_NONE, EC_HLA, EC_AAR};
#endif

#if _VBS3
#include "Hla/AAR.hpp"
#endif

//static const float CmdPlanDist = 100.0f;
//static const float CmdMinDist = 40.0f;
static const float CmdPlanDist = 30.0f;
static const float CmdMinDist = 10.0f;

const float DriverReactionTime=0.3;

// common implementation of transport vehicles

DEFINE_CASTING(Transport)

/*
static float TransportGetFieldCost(int x, int z, void *param)
{
  if (!param) return SET_UNACCESSIBLE;
  Transport *vehicle = (Transport *)param;
  //return subgrp->GetFieldCost(x, z);

  if( !InRange(x,z) ) return SET_UNACCESSIBLE;
  GeographyInfo geogr = GLOB_LAND->GetGeography(x,z);

  float cost = vehicle->GetCost(geogr)*vehicle->GetFieldCost(geogr);
  cost *= LandGrid; // cost = time for distance == LandGrid
  return cost;
}
*/

#pragma warning(disable:4355)

#define DIAG_TRANSPORT 0
#define DIAG_SOUND_SAMPLE 0

bool HasPrimaryGunnerTurret(ParamEntryPar cls)
{
  ParamEntryVal turrets = cls >> "Turrets";
  for (int i=0; i<turrets.GetEntryCount(); i++)
  {
    ParamEntryVal entry = turrets.GetEntry(i);
    if (!entry.IsClass())
      continue;
    bool hasGunner = entry >> "primaryGunner";
    if (hasGunner)
      return true;
    if (HasPrimaryGunnerTurret(entry))
      return true;
  }
  return false;
}

bool HasPrimaryObserverTurret(ParamEntryPar cls)
{
  ParamEntryVal turrets = cls >> "Turrets";
  for (int i=0; i<turrets.GetEntryCount(); i++)
  {
    ParamEntryVal entry = turrets.GetEntry(i);
    if (!entry.IsClass())
      continue;
    bool hasObserver = entry >> "primaryObserver";
    if (hasObserver)
      return true;
    if (HasPrimaryObserverTurret(entry))
      return true;
  }
  return false;
}

TransportType::TransportType( ParamEntryPar param )
:base(param), _camShakeCoef(CameraShakeManager::DefaultVehicleAttenuationCoef)
{
}

/// Functor checks if some gunner is better commander than driver
class GunnerIsCommander : public ITurretTypeFunc
{
public:
  virtual bool operator () (const EntityAIType *entityType, const TurretType &turretType, TurretPath &path) const
  {
    return turretType._commanding > 0;
  };
};

/// Functor checks if some turret has primary gunner flag set
class HasPrimaryGunner : public ITurretTypeFunc
{
public:
  virtual bool operator () (const EntityAIType *entityType, const TurretType &turretType, TurretPath &path) const
  {
    return turretType._primaryGunner;
  };
};

/// Functor checks if some turret has primary observer flag set
class HasPrimaryObserver : public ITurretTypeFunc
{
public:
  virtual bool operator () (const EntityAIType *entityType, const TurretType &turretType, TurretPath &path) const
  {
    return turretType._primaryObserver;
  };
};


/* used as key for submix groups */
struct SubmixKey
{
  float _vol;
  float _low;
  float _up;
  bool _useCone;

  SubmixKey() {}
  SubmixKey(float vol, bool useCone): _vol(floatMax(vol, 1e-5f)), _useCone(useCone)
  {
    static float dividend = logf(10.0f);

    float db = 20.0f * (logf(vol) / dividend);

    _low = powf(10.0f, (db - 2.01f) * (1.0f/20));
    _up = powf(10.0f, (db + 2.01) * (1.0f/20));
  }

  bool operator== (const SubmixKey &with) const
  {
    if (_useCone != with._useCone) 
      return false;
    else
    {
      return  _vol >= with._low && _vol <= with._up;
    }
  }
};

TypeIsSimple(SubmixKey)

template<class Type>
struct BaseSubmixGroup
{
  SubmixKey _key;
  AutoArray<Type> _sounds;

  BaseSubmixGroup(): _sounds(), _key() {}
  virtual ~BaseSubmixGroup() { _sounds.Clear(); }
};

/* struct used for temporary holding audio controllers groups */
struct SubmixGroup: public BaseSubmixGroup<SoundParsEx>
{
  typedef BaseSubmixGroup<SoundParsEx> base;
  SubmixGroup(): base() {}
};

TypeIsMovable(SubmixGroup)

/* struct used for temporary audio controllers grouping */
struct SubmixEventGroup: public BaseSubmixGroup<SoundEventPars>
{
  typedef BaseSubmixGroup<SoundEventPars> base;
  SubmixEventGroup(): base() {}
};

TypeIsMovable(SubmixEventGroup)

// used for sorting waves into submix groups
template<>
struct FindArrayKeyTraits<SubmixGroup>
{
  typedef const SubmixKey &KeyType;
  static bool IsEqual(KeyType a, KeyType  b) { return (a == b); }
  static KeyType GetKey(const SubmixGroup &sg) { return sg._key; }
};

// used for sorting event waves into submix groups
template<>
struct FindArrayKeyTraits<SubmixEventGroup>
{
  typedef const SubmixKey &KeyType;
  static bool IsEqual(KeyType a, KeyType  b) { return (a == b); }
  static KeyType GetKey(const SubmixEventGroup &esg) { return esg._key; }
};

inline static bool LoadSoundConePars(const ParamEntryVal &ie, SoundCone &cone)
{
  ConstParamEntryPtr coneEntry = ie.FindEntry("cone");
  if (coneEntry.NotNull())
  {
    if (coneEntry->IsArray() && coneEntry->GetSize() == 4)
    {
      cone._angleIn = (*coneEntry)[0];
      cone._angleOut = (*coneEntry)[1];
      cone._volIn = (*coneEntry)[2];
      cone._volOut = (*coneEntry)[3];
      return true;
    }
  }

  return false;
}

/*!
\patch 5500 Date 10/8/2007 by Jirka
- New: Different get in / out animations for different positions
*/

void TransportType::Load(ParamEntryPar par, const char *shape)
{
  base::Load(par,shape);

#define GET_PAR(x) _##x=par>>#x

  GET_PAR(getInRadius);

  GET_PAR(hasDriver);
  GET_PAR(driverForceOptics);

  GET_PAR(crewVulnerable);
  GET_PAR(enableManualFire);

  _crew = par >> "crew";
  _driverAction = ExtractActionName(par >> "driverAction");
  _driverInAction = ExtractActionName(par >> "driverInAction");
  _driverGetInAction = ExtractActionName(par >> "getInAction");
  _driverGetOutAction = ExtractActionName(par >> "getOutAction");

  _maxManCargo=par>>"transportSoldier";
  //_maxVehicleCargo=par>>"transportVehiclesCount";
  //_maxVehicleCargoMass=par>>"transportVehiclesMass";
  _typicalCargo.Resize(0);
  ParamEntryVal typCargo=par>>"typicalCargo";
  for (int i=0; i<typCargo.GetSize(); i++)
  {
    RString name=typCargo[i];
    Ref<const EntityType> vType=VehicleTypes.New(name);
    const EntityAIType *type=dynamic_cast<const EntityAIType *>(vType.GetRef());
    if (!type)
      continue;
    _typicalCargo.Add(const_cast<EntityAIType *>(type));
  }
  _typicalCargo.Compact();

  ParamEntryVal cargo = par >> "cargoAction";
  _cargoAction.Realloc(cargo.GetSize());
  _cargoAction.Resize(cargo.GetSize());
  for (int i=0; i<cargo.GetSize(); i++)
    _cargoAction[i] = ExtractActionName(cargo[i]);
  cargo = par >> "cargoGetInAction";
  _cargoGetInAction.Realloc(cargo.GetSize());
  _cargoGetInAction.Resize(cargo.GetSize());
  for (int i=0; i<cargo.GetSize(); i++)
    _cargoGetInAction[i] = ExtractActionName(cargo[i]);
  cargo = par >> "cargoGetOutAction";
  _cargoGetOutAction.Realloc(cargo.GetSize());
  _cargoGetOutAction.Resize(cargo.GetSize());
  for (int i=0; i<cargo.GetSize(); i++)
    _cargoGetOutAction[i] = ExtractActionName(cargo[i]);

  GET_PAR(driverCompartments);
  ParamEntryVal cargoCompartments = par >> "cargoCompartments";
  _cargoCompartments.Realloc(cargoCompartments.GetSize());
  _cargoCompartments.Resize(cargoCompartments.GetSize());
  for (int i=0; i<cargoCompartments.GetSize(); i++)
    _cargoCompartments[i] = cargoCompartments[i];

  GET_PAR(hideProxyInCombat);
  GET_PAR(forceHideDriver);

#if _VBS3 //added optional parameter _viewDriverInExternal
  _viewDriverInExternal = par.ReadValue("viewDriverInExternal", false);
#endif

  GET_PAR(unloadInCombat);
  GET_PAR(insideSoundCoef);
  GET_PAR(outsideSoundFilter);
  GET_PAR(occludeSoundsWhenIn);
  GET_PAR(obstructSoundsWhenIn);

  _driverOpticsColor = GetPackedColor(par >> "driverOpticsColor");

  _viewCargo.Load(par>>"ViewCargo");
  _viewOptics.Load(par>>"ViewOptics");

  GET_PAR(castDriverShadow);
  GET_PAR(castCargoShadow);
  GET_PAR(viewDriverShadow);
  GET_PAR(viewDriverShadowDiff);
  GET_PAR(viewDriverShadowAmb);
  GET_PAR(viewCargoShadow);
  GET_PAR(viewCargoShadowDiff);
  GET_PAR(viewCargoShadowAmb);
  GET_PAR(ejectDeadDriver);
  GET_PAR(ejectDeadCargo);
  GET_PAR(hideWeaponsDriver);
  GET_PAR(hideWeaponsCargo);

  _lockTargetAction = UAVehLockTargets;

#undef GET_PAR

  // load MFD description
  ParamEntryVal mfdList = par>>"MFD";
  _mfd.Realloc(mfdList.GetEntryCount());
  for (int i=0; i<mfdList.GetEntryCount(); i++)
  {
    ParamEntryVal entry = mfdList.GetEntry(i);
    if (!entry.IsClass())
      continue;
    MFDDesc &mfd = _mfd.Append();
    mfd.Load(entry);
  }
  _mfd.Compact();

  // Turrets
  _turrets.Resize(0);
  ParamEntryVal turrets = par >> "Turrets";
  for (int i=0; i<turrets.GetEntryCount(); i++)
  {
    ParamEntryVal entry = turrets.GetEntry(i);
    if (!entry.IsClass())
      continue;
    _turrets.Add(new TurretType(entry));
  }
  _turrets.Compact();

  {
    HasPrimaryGunner func;
    _hasGunner = ForEachTurret(func);
  }
  {
    HasPrimaryObserver func;
    _hasObserver = ForEachTurret(func);
  }
  {
    // if there are no gunners with higher commanding level, driver is commander
    GunnerIsCommander func;
    _driverIsCommander = !ForEachTurret(func);
  }

  /// controllers list
  static const RString SoundPars[]=
  {
    "rpm", "randomizer", "speed", "thrust", "camPos", "engineOn", "rotorSpeed", "rotorThrust",  "angVelocity",
    "gmeterZ", "roughness", "dustness", "damper0", "rock", "sand", "grass", "mud", "gravel", "asphalt",
    "gear", "flaps", "rotPos", "water"
  };

  /* Load individual sound and store them in groups. */

  /// audio controllers
  ParamEntryVal entry = par >> "Sounds";

  // used for dividing into submix groups
  FindArrayKey< SubmixGroup, FindArrayKeyTraits< SubmixGroup > > tmpGroups;
  if (entry.IsClass() )
  {
    for (ParamClass::Iterator<> i(entry.GetClass()); i; ++i)
    {
      const ParamEntryVal &ie = *i;

      // each sound component is sub class of sound class
      if (!ie.IsClass())
        continue;

      SoundParsEx sParsEx;

      ConstParamEntryPtr sItem = ie.FindEntry("sound");
      if (sItem)
      {
        if (sItem->IsArray())
        {
          // load sound base parameters: sound[]={"path",base_volume,base_frequency};
          GetValue(sParsEx._pars, ie >> "sound");
        }
        else
        {
          // ensure compatibility, sound source and values are set in var. soundEngine and soundEnviron
          RString text = ie >> "sound";
          GetValue(sParsEx._pars, par >> text);
        }
      }

      if (sParsEx._pars.name.IsEmpty()) continue;

      // load frequency expression: frequency = "rpm*A";
      RString freqText = ie >> "frequency";
      // load volume expression: volume = "rpm*B+speed*C";
      RString volText = ie >> "volume";

      bool result = sParsEx._freqExpr.Compile(freqText, SoundPars, lenof(SoundPars));
      Assert(result);
      if (!result)
      {
        RptF("Error: Sound frequency expression: %s", cc_cast(freqText));
        continue;
      }

      result = sParsEx._volExpr.Compile(volText, SoundPars, lenof(SoundPars));
      Assert(result);
      if (!result)
      {
        RptF("Error: Sound volume expression: %s", cc_cast(volText));
        continue;
      }

      sParsEx._coneUsed = LoadSoundConePars(ie, sParsEx._cone);

      // create key
      SubmixKey key(sParsEx._pars.vol, sParsEx._coneUsed);

      // exist
      int index = tmpGroups.FindKey(key);

      if (index < 0)
      {
        // create group
        SubmixGroup &sgTmp = tmpGroups.Append();
        sgTmp._key = key;
        sgTmp._sounds.Append(sParsEx);
      }
      else
      {
        // insert into existing group
        tmpGroups[index]._sounds.Append(sParsEx);
      }
    }
  }

  if (tmpGroups.Size() != 0)
  {
    // copy groups
    _submixPars.Realloc(tmpGroups.Size());
    _submixPars.Resize(tmpGroups.Size());

    for (int i = 0; i < tmpGroups.Size(); i++)
    {
      SubmixPars &spa = _submixPars[i];
      // copy ref
      spa._sParsEx.Append(tmpGroups[i]._sounds);
    }
  }

  // clear temporal array
  tmpGroups.Clear();

  // used for dividing samples into submix groups
  FindArrayKey< SubmixEventGroup, FindArrayKeyTraits< SubmixEventGroup > > tmpEventGroups;

  /// audio events
  ParamEntryVal eventEntry = par >> "SoundEvents";

  if (eventEntry.IsClass())
  {
    for (ParamClass::Iterator<> i(eventEntry.GetClass()); i; ++i)
    {
      const ParamEntryVal &ie = *i;

      // each sound event component is sub class of sound event class
      if (!ie.IsClass())
        continue;

      SoundEventPars eventPars;

      // sound[]={"path",base_volume,base_frequency};
      GetValue(eventPars._pars, ie >> "sound");

      eventPars._started = false;
      eventPars._limit = ie >> "limit";
      RString expText = ie >> "expression";

      // expression compilation
      bool result = eventPars._expression.Compile(expText, SoundPars, lenof(SoundPars));
      Assert(result);
      if (!result)
      {
        RptF("Error: Sound event expression: %s", cc_cast(expText));
        continue;
      }

      eventPars._coneUsed = LoadSoundConePars(ie, eventPars._cone);

      // create key
      SubmixKey key(eventPars._pars.vol, eventPars._coneUsed);

      // exist
      int index = tmpEventGroups.FindKey(key);

      if (index < 0)
      {
        // create group
        SubmixEventGroup &sgTmp = tmpEventGroups.Append();
        sgTmp._key = key;
        sgTmp._sounds.Append(eventPars);
      }
      else
      {
        // append into existing group
        tmpEventGroups[index]._sounds.Append(eventPars);
      }
    }
  }

  // copy groups
  if (tmpEventGroups.Size() != 0)
  {
    _submixEventPars.Realloc(tmpEventGroups.Size());
    _submixEventPars.Resize(tmpEventGroups.Size());

    for (int i = 0; i < tmpEventGroups.Size(); i++)
    {
      SubmixEventPars &sep = _submixEventPars[i];
      // copy ref
      sep._sEventPars.Append(tmpEventGroups[i]._sounds);
    }
  }

  // clear temporal array
  tmpEventGroups.Clear();

  // read camera shake params
  _camShakeCoef = CameraShakeManager::DefaultVehicleAttenuationCoef;
  ParamEntryPtr camShakePar = par.FindEntry("camShakeCoef");
  if (camShakePar)
  {
    _camShakeCoef = *camShakePar;
  }
  camShakePar = par.FindEntry("CamShake");
  if (camShakePar)
  {
    _camShake.Load(*camShakePar);
  }
}

#include <Es/Common/delegate.hpp>

/*!
\patch 5161 Date 5/31/2007 by Ondra
- New: Animation controllers gmeterx, gmetery, gmeterz
*/
AnimationSource *TransportType::CreateAnimationSource(const AnimationType *type, RStringB source)
{
  static const struct AnimSourceDesc
  {
    const char *name;
    float (Transport::*func)(ObjectVisualState const&) const;
  } desc[]=
  {
    {"altRadar",&Transport::GetCtrlAltRadar},
    {"altBaro",&Transport::GetCtrlAltBaro},
    {"speed",&Transport::GetCtrlSpeedHorizontal},
    {"vertSpeed",&Transport::GetCtrlSpeedVertical},
    {"rpm",&Transport::GetCtrlRPM},
    {"direction",&Transport::GetCtrlDirection},
    {"turretDir",&Transport::GetCtrlTurretDirection},  // TODO
    {"hatchDriver",&Transport::GetCtrlDriverHatchPos},
    {"drivingWheel",&Transport::GetCtrlDrivingWheelPos},
    {"horizonBank",&Transport::GetCtrlHorizonBank},
    {"horizonDive",&Transport::GetCtrlHorizonDive},
    {"fuel",&Transport::GetCtrlFuel},
    {"gmeter",&Transport::GetCtrlGMeter},
    {"gmeterx",&Transport::GetCtrlGMeterX},
    {"gmetery",&Transport::GetCtrlGMeterY},
    {"gmeterz",&Transport::GetCtrlGMeterZ},
  };
  for (int i=0; i<sizeof(desc)/sizeof(desc[0]); i++)
  {
    if (!strcmpi(source,desc[i].name))
      return _animSources.CreateAnimationSource(desc[i].func);
  }

  // check if some turret uses this animation source
  // TODO: add turrets to visual state
  AutoArray<int, MemAllocLocal<int, 16> > path;
  for (int i=0; i<_turrets.Size(); i++)
  {
    path.Resize(1);
    path[0] = i;
    AnimationSource *result = _turrets[i]->CreateAnimationSource(source, path);
    if (result)
      return result;
  }
  return base::CreateAnimationSource(type,source);
}

void TransportType::AddProxy(ManProxy &mProxy, const ProxyObjectTyped &proxy, const ProxyObject &proxyObj)
{
  //LogF("%s: add proxy %s",(const char *)GetName(),(const char *)anim);
  Matrix4 &trans=mProxy.transform;
  trans = proxy.obj->FutureVisualState().Transform();
  LODShapeWithShadow *pshape = proxy.obj->GetShape();
  // reset position BC correction
  Vector3 offset = VZero;
  if (pshape) offset = -pshape->BoundingCenter();
  trans.SetPosition(trans.FastTransform(offset));
  // rotate around y-axis (shape is reversed)
  trans.SetOrientation(trans.Orientation() * Matrix3(MScale,-1,1,-1));
  mProxy._proxy = &proxyObj;
}

Threat TransportType::GetStrategicThreat( float distance2, float visibility, float cosAngle ) const
{
  Threat threat = base::GetStrategicThreat(distance2,visibility,cosAngle);
  // cargo not known, assume typical cargo
  for( int i=0; i<_typicalCargo.Size(); i++ )
  {
    const EntityAIType *type=_typicalCargo[i];
    Assert(this != type);
    threat+=type->GetStrategicThreat(distance2,visibility,-1);
  }
  return threat;
}

Threat TransportType::GetDamagePerMinute(float distance2, float visibility) const
{
  Threat threat = base::GetDamagePerMinute(distance2,visibility);
  // cargo not known, assume typical cargo
  for( int i=0; i<_typicalCargo.Size(); i++ )
  {
    const EntityAIType *type=_typicalCargo[i];
    Assert (type != this);
    threat+=type->GetDamagePerMinute(distance2,visibility);
  }
  return threat;
}

class ScanWeaponRange: public ITurretTypeFunc
{
  float &_maxRange;
public:
  explicit ScanWeaponRange(float &maxRange):_maxRange(maxRange){}

  bool operator () (const EntityAIType *entityType, const TurretType &turretType, TurretPath &path) const
  {
    float maxRange = turretType._weapons.GetMaxRange();
    saturateMax(_maxRange,maxRange);
    return false;
  }
};

float TransportType::GetMaxWeaponRange() const
{
  float maxRange = base::GetMaxWeaponRange();
  // consider all turrets as well
  ScanWeaponRange scanWeaponRange(maxRange);
  ForEachTurret(scanWeaponRange);
  return maxRange;
}

Threat Transport::GetDamagePerMinute(float distance2, float visibility) const
{
  Threat threat = base::GetDamagePerMinute(distance2,visibility);
  // if we know cargo, calculate exact
  const ManCargo &cargo = GetManCargo();
  for (int i=0; i<cargo.Size(); i++)
  {
    Person *man=cargo[i];
    if (man)
      threat += man->GetDamagePerMinute(distance2,visibility);
  }
  return threat;
}

#include <Es/Files/filenames.hpp>

static bool NameInList( ParamEntryPar cfg, const char *name )
{
  for (int i=0; i<cfg.GetSize(); i++)
  {
    RStringB val = cfg[i];
    if (!strcmpi(val,name))
      return true;
  }
  return false;
}

size_t TransportType::GetMemoryControlled() const
{
  // who knows how much memory is controlled?
  return sizeof(*this);
}

static void LoadGetInPoints(LODShapeWithShadow *shape, const Shape *memory, RString namedSel, GetInPoints &points)
{
  int index = shape->FindNamedSel(memory, namedSel);
  if (index >= 0)
  {
    const NamedSelection::Access sel(memory->NamedSel(index),shape,memory);
    for (int i=0; i<sel.Size(); i++)
    {
      if (sel[i] >= 0)
      {
        GetInPointIndex& pt = points.Append();
        pt._pos = sel[i];
        pt._dir = -1;
      }
    }
  }
}

static bool LoadGetInPointsDir(LODShapeWithShadow *shape, const Shape *memory, RString namedSel, GetInPoints &points)
{
  bool ok = true;

#define GET_CLOSEST_POINT(array, imin, pt) \
  { \
    float min = FLT_MAX; \
    for(int j = 0; j < (array).Size(); j++) \
    { \
      float dist = ((pt) - memory->Pos((array)[j]._pos)).SquareSize(); \
      if (dist < min) { min = dist; (imin) = j;}; \
    } \
  };

  int index = shape->FindNamedSel(memory, namedSel);
  if (index >= 0)
  {
    const NamedSelection::Access sel(memory->NamedSel(index),shape,memory);
    for (int i=0; i<sel.Size(); i++)
    {
      if (sel[i] >= 0)
      {
        int imin = -1;
        Vector3 dirpt = memory->Pos(sel[i]);
        GET_CLOSEST_POINT(points, imin, dirpt);

        if (imin>=0)
        {
          GetInPointIndex& pt = points[imin];
          pt._dir = sel[i];
        }
        else
          ok = false;
      }
    }
  }
  return ok;
}

void TransportType::InitShape()
{
  base::InitShape();

  _proxies.Init(_shape->NLevels());

  const ParamEntry &par = *_par;

#ifndef _XBOX
  _squadTitles.Init(_shape, par >> "SquadTitles", GetFontID(Pars >> "fontClanName"));
#endif

  const Shape *memory = _shape->MemoryLevel();
  if (memory)
  {
    ParamEntryVal entry = par >> "memoryPointDriverOptics";
    if (entry.IsArray())
    {
      for (int i=0; i<entry.GetSize(); i++)
      {
        _driverOpticsPos = _shape->FindNamedSel(memory,entry[i].operator RString());
        if (_driverOpticsPos >= 0)
          break;
      }
    }
    else
      _driverOpticsPos = _shape->FindNamedSel(memory,(RString)entry.operator RString());
  }
  else
    _driverOpticsPos = -1;

  RStringB oModelName = par >> "driverOpticsModel";
  _driverOpticsModel = oModelName.GetLength() > 0 ? Shapes.New(::GetShapeName(oModelName), true, false) : NULL;
  if (_driverOpticsModel && _driverOpticsModel->NLevels() > 0)
  {
    _driverOpticsModel->Level(0)->MakeCockpit();
    _driverOpticsModel->OrSpecial(BestMipmap);
  }

  ParamEntryVal cfgOpticsPars = Pars >> "CfgOpticsEffect";

  ParamEntryVal optEntry = par >> "driverOpticsEffect";
  if (optEntry.IsArray())
  {
    int size = optEntry.GetSize();

    _opticsType.Realloc(size);
    _opticsType.Resize(size);

    for (int i = 0; i < size; i++)
    {
      // effect type
      RString name = optEntry[i];
      if (cfgOpticsPars.FindEntry(name))
      {
        ParamEntryVal eEntry = cfgOpticsPars >> name;
        if (eEntry.IsClass())
          _opticsType[i].Load(eEntry);
      }
    }
  }

  _driverGetInPos.Resize(0);
  _cargoGetInPos.Resize(0);

  if (memory)
  {
    LoadGetInPoints(_shape, memory, par >> "memoryPointsGetInDriver", _driverGetInPos);
    if (!LoadGetInPointsDir(_shape, memory, par >> "memoryPointsGetInDriverDir", _driverGetInPos))
    {
      RptF("No get in driver point in %s (%s)", cc_cast(_shape->GetName()), cc_cast(GetName()));
    }
    // verify that all get in/out points has direction, if not add default value
    for(int i = 0; i < _driverGetInPos.Size(); i++)
      if (_driverGetInPos[i]._dir < 0)
        RptF("In Vehicle: %s missing driver get in direction point", _shape->Name());

    ParamEntryVal cargoEntry = par >> "memoryPointsGetInCargo";
    if (cargoEntry.IsArray())
    {
      // new style of definition (separate for each cargo position)
      int nPos = cargoEntry.GetSize();
      ParamEntryVal cargoEntryDir = par >> "memoryPointsGetInCargoDir";
      int nDir = cargoEntryDir.GetSize();

      _cargoGetInPos.Realloc(nPos);
      _cargoGetInPos.Resize(nPos);
      for (int i=0; i<nPos; i++)
      {
        GetInPoints &cargoPoints = _cargoGetInPos[i];
        LoadGetInPoints(_shape, memory, cargoEntry[i], cargoPoints);
        if (nDir > 0)
        {
          int iDir = intMin(i, nDir - 1);
          if (!LoadGetInPointsDir(_shape, memory, cargoEntryDir[iDir], cargoPoints))
            RptF("No get in cargo %d point in %s (%s)", i, cc_cast(_shape->GetName()), cc_cast(GetName()));
        }
        for(int i = 0; i < cargoPoints.Size(); i++)
        {
          if (cargoPoints[i]._dir < 0)
            RptF("In Vehicle: %s missing cargo %d get in direction point", _shape->Name(), i);
        }
        cargoPoints.Compact();
      }
    }
    else
    {
      GetInPoints cargoPoints, coDriverPoints;

      // get in points for cargo
      LoadGetInPoints(_shape, memory, cargoEntry, cargoPoints);
      if (!LoadGetInPointsDir(_shape, memory, par >> "memoryPointsGetInCargoDir", cargoPoints))
        RptF("No get in cargo point in %s (%s)", cc_cast(_shape->GetName()), cc_cast(GetName()));
      for(int i = 0; i < cargoPoints.Size(); i++)
      {
        if (cargoPoints[i]._dir < 0)
          RptF("In Vehicle: %s missing cargo get in direction point", _shape->Name());
      }
      cargoPoints.Compact();

      // get in points for coDriver
      LoadGetInPoints(_shape, memory, par >> "memoryPointsGetInCoDriver", coDriverPoints);
      if (!LoadGetInPointsDir(_shape, memory, par >> "memoryPointsGetInCoDriverDir", coDriverPoints))
        RptF("No get in co-driver point in %s (%s)",cc_cast(_shape->GetName()),cc_cast(GetName()));
      for(int i = 0; i < coDriverPoints.Size(); i++)
      {
        if (coDriverPoints[i]._dir < 0)
          RptF("In Vehicle: %s missing codriver get in direction point", _shape->Name());
      }
      coDriverPoints.Compact();

      // distribute points to cargo positions
      ParamEntryVal cargoCoDriver = par >> "cargoIsCoDriver";
      int n = cargoCoDriver.GetSize();
      if (n == 0)
      {
        // all positions are cargo
        _cargoGetInPos.Realloc(1);
        _cargoGetInPos.Resize(1);
        _cargoGetInPos[0] = cargoPoints;
      }
      else
      {
        _cargoGetInPos.Realloc(n);
        _cargoGetInPos.Resize(n);
        for (int i=0; i<n; i++)
        {
          bool coDriver = cargoCoDriver[i];
          if (coDriver)
            _cargoGetInPos[i] = coDriverPoints;
          else
            _cargoGetInPos[i] = cargoPoints;
        }
      }
    }
  }

  if (_driverGetInPos.Size() == 0)
  {
    LogF("Type %s missing driver position", (const char *)GetName());

    // default fall back
    GetInPointIndex& pt = _driverGetInPos.Append();
    pt._pos = -1;
    pt._dir = -1;
  }
  _driverGetInPos.Compact();

  if (_cargoGetInPos.Size() == 0)
  {
    // if there is no cargo points, use driver point instead
    _cargoGetInPos.Realloc(1);
    _cargoGetInPos.Resize(1);
    _cargoGetInPos[0] = _driverGetInPos;
  }
  else
  {
    // check points for all cargo slots
    for (int i=0; i<_cargoGetInPos.Size(); i++)
    {
      if (_cargoGetInPos[i].Size() == 0)
        _cargoGetInPos[i] = _driverGetInPos;
    }
  }

  for (int i=0; i<_mfd.Size(); i++)
    _mfd[i].InitShape(_shape);

  for (int i=0; i<_turrets.Size(); i++)
    _turrets[i]->Init(_shape, this);
}

void TransportType::DeinitShape()
{
  _driverOpticsModel.Free();
  // clear point arrays which are no longer valid without a shape
  _driverGetInPos.Clear();
  _cargoGetInPos.Clear();

  for (int i=0; i<_turrets.Size(); i++)
    _turrets[i]->Deinit(_shape, this);
  base::DeinitShape();
}

void LevelProxies::Clear()
{
  _driverProxy = ManProxy();
  _cargoProxy.Resize(0);
  _normalProxy.Resize(0);
}

void TransportType::InitShapeLevel(int level)
{
  base::InitShapeLevel(level);

#ifndef _XBOX
  _squadTitles.InitLevel(_shape,level);
#endif

  LevelProxies &proxies=_proxies[level];
  //Shape *shape=_shape->Level(level);
  // clear proxy
  proxies.Clear();

  ShapeUsed shape = _shape->Level(level);

  proxies._normalProxy.Resize(0);

  // convert shape proxies to my proxies
  for( int i=0; i<GetProxyCountForLevel(level); i++ )
  {
    const ProxyObjectTyped &proxy=GetProxyForLevel(level,i);
    const ProxyObject &proxyObj = shape->Proxy(i);
    // first check for ProxyCrew
    ProxyCrew *proxyCrew = dyn_cast<ProxyCrew,Object>(proxy.obj);
    if (proxyCrew)
    {
      // check crew type
      CrewPosition pos = proxyCrew->Type()->GetCrewPosition();
      if (pos==CPDriver)
        AddProxy(proxies._driverProxy,proxy,proxyObj);
      else if (pos==CPCargo)
      {
        int id = proxy.id;
        if (id < 1)
        {
          Fail("Bad cargo proxy index");
          continue;
        }
        if (proxies._cargoProxy.Size() < id)
          proxies._cargoProxy.Resize(id);
        ManProxy &mProxy=proxies._cargoProxy[id - 1];
        AddProxy(mProxy,proxy,proxyObj);
      }
      continue;
    }

    // try to get vehicle class name
    char shortname[1024];
    Entity *proxyVeh = dyn_cast<Entity,Object>(proxy.obj);
    if (proxyVeh)
    {
      //LogF("%s: veh proxy %s",(const char *)GetName(),shortname);
      strcpy(shortname,proxyVeh->GetName());
    }
    else
    {
      //LogF("%s: obj proxy %s",(const char *)GetName(),shortname);
      if (proxy.obj->GetShape())
        GetFilename(shortname,proxy.obj->GetShape()->Name());
      else
        *shortname=0;
    }
    // check proxy class, if it is always visible, count it
    const EntityType *type = proxy.obj->GetEntityType();
    if (!type || !strcmp(type->_simName, "alwaysshow"))
      proxies._normalProxy.Add(i);
  }
  proxies._cargoProxy.Compact();
  proxies._normalProxy.Compact();

  for (int i=0; i<_turrets.Size(); i++)
    _turrets[i]->InitLevel(_shape, level, this);
}

void TransportType::DeinitShapeLevel(int level)
{
#ifndef _XBOX
  _squadTitles.DeinitLevel(_shape,level);
#endif

  _proxies[level].Clear();

  for (int i=0; i<_turrets.Size(); i++)
    _turrets[i]->DeinitLevel(_shape, level, this);

  base::DeinitShapeLevel(level);
}

bool TransportType::ForEachTurret(ITurretTypeFunc &func) const
{
  TurretPath path;

  int n = _turrets.Size();
  for (int i=0; i<n; i++)
  {
    path.Resize(1);
    path[0] = i;
    if (_turrets[i]->ForEachTurret(this, path, func))
      return true;
  }
  return false;
}

/*!
\patch 5501 Date 6/12/2008 by Jirka
- Fixed: Crash at ArmA.exe:0x56536 - caused by wrong turret state in MP.
*/

const TurretType *TransportType::GetTurret(const int *path, int size) const
{
  if (size <= 0)
  {
    LogF("Empty turret path");
    return NULL;
  }
  int i = path[0];
  if (i < 0 || i >= _turrets.Size())
  {
    LogF("Invalid turret path");
    return NULL;
  }
  return _turrets[i]->GetTurret(path + 1, size - 1);
}

RString TransportType::GetCargoGetInAction(int pos) const
{
  int n = _cargoGetInAction.Size();
  if (n == 0)
    return RString();
  saturate(pos, 0, n - 1);
  return _cargoGetInAction[pos];
}

RString TransportType::GetCargoGetOutAction(int pos) const
{
  int n = _cargoGetOutAction.Size();
  if (n == 0)
    return RString();
  saturate(pos, 0, n - 1);
  return _cargoGetOutAction[pos];
}

/*!
\patch 5138 Date 3/9/2007 by Jirka
- Fixed: Separate get in point can be defined for each cargo position
*/

int TransportType::NCargoGetInPos(int cargoIndex) const
{
  if (cargoIndex < 0)
    return 0;
  int n = _cargoGetInPos.Size();
  if (cargoIndex >= n)
    return _cargoGetInPos[n - 1].Size();
  return _cargoGetInPos[cargoIndex].Size();
}

GetInPointIndexVal TransportType::GetCargoGetInPos(int cargoIndex, int i) const
{
  int n = _cargoGetInPos.Size();
  if (cargoIndex >= n)
    return _cargoGetInPos[n - 1][i];
  return _cargoGetInPos[cargoIndex][i];
}

#pragma warning( disable: 4355 )

static const EnumName LandingModeNames[]=
{
  EnumName(Transport::LMNone, "NONE"),
  EnumName(Transport::LMLand, "LAND"),
  EnumName(Transport::LMGetIn, "GET IN"),
  EnumName(Transport::LMGetOut, "GET OUT"),
  EnumName()
};

template<>
const EnumName *GetEnumNames(Transport::LandingMode dummy)
{
  return LandingModeNames;
}

#if _VBS3 //check override dependency
class LinkOverrideTurret : public ITurretFunc
{
public:
  LinkOverrideTurret(){}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    context._turret->SetOverrideTurretLink();
    return false; // continue
  }
};
#endif


class InitWeaponStateFunc: public ITurretFunc
{
public:
  InitWeaponStateFunc() {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turretType)
      context._weapons->Init(context._turretType->_weapons, *context._turretType->_par, entity, true);
    // open fire by default
    context._weapons->_fire._firePrepareOnly = false;
    return false; //all turrets
  }
};


TransportVisualState::TransportVisualState(TransportType const& type)
:base(type),
_fuel(type.GetFuelCapacity()),
_driverHidden(type._hideProxyInCombat)
{
  int n = type._turrets.Size();
  _turretVisualStates.Realloc(n);
  _turretVisualStates.Resize(n);
  for (int i=0; i<n; i++)
    _turretVisualStates[i] = new TurretVisualState(*type._turrets[i], type);
}

void TransportVisualState::Interpolate(Object *obj, ObjectVisualState const& t1state, float t, ObjectVisualState& interpolatedResult) const
{
  base::Interpolate(obj, t1state, t, interpolatedResult);

  TransportVisualState const& s = static_cast<TransportVisualState const&>(t1state);
  TransportVisualState& res = static_cast<TransportVisualState&>(interpolatedResult);

  #define INTERPOLATE(x) res.x = x + t*(s.x - x)

  INTERPOLATE(_fuel);  // continuous
  INTERPOLATE(_driverHidden);  // continuous

  #undef INTERPOLATE

  int n = _turretVisualStates.Size();
//  res._turretVisualStates.Realloc(n);
//  res._turretVisualStates.Resize(n);
  for (int i=0; i<n; i++) {
    _turretVisualStates[i]->Interpolate(*s._turretVisualStates[i], t, *res._turretVisualStates[i]);
  }
}


Transport::Transport(const EntityAIType *name, Person *driver, bool fullCreate)
:base(name, fullCreate),

//_planner(TransportGetFieldCost,this),

//_crewDammage(0), // TODO: remove - not used
_getinTime(0),
_getoutTime(Glob.time-60),
_engineOff(true),
_respawnSide(TSideUnknown),
_respawning(false),
_respawnFlying(false),
_respawnDelay(-1),
_respawnCount(0),
_zoom(),
_ptrSurfInfo(NULL),
_getOutAfterDamage(TIME_MAX), // never get out
_assignDriverAfterDamage(TIME_MAX), // never get out
_assignGunnerAfterDamage(TIME_MAX), // never get out
_explosionTime(TIME_MAX), // never explode
_turretFrontUntil(TIME_MIN),
_driverPos(VZero),
_dirWanted(VZero),
_moveMode(VMMFormation),
_turnMode(VTMNone),
_fireEnabled(false),
//_useStrategicPlan(false),
//_strategicPlanValid(false),
//_lastStrategicIndex(-1),
//_completedSent(false),
_lock(LSDefault),
_locked(LdSFree),
_driverLocked(false),
_landing(LMNone),
_radio(CCVehicle, Pars >> "RadioChannels" >> "VehicleChannel"),
_showDmg(false),
_showDmgValid(Glob.time-60),
_randomizer(GRandGen.RandomValue()), // do not use same sound frequency
// all hatches are by default closed
_driverHiddenWanted(Type()->_hideProxyInCombat),
_manualFire(false),
_azimutWanted(0),
_doCrash(CrashNone),
_oldEnergy(0.0f),
_maxContactSpeed(0),
_validEnergy(false),
_isWreck(false),
_visionMode(VMNormal),
_tiIndex(0),
_enableVisionMode(true),
_turretGunTemperatureCoef(0),
_destroyMaxHeatTime(0),
_lastCamShakeTime(0)
{
  const TransportType *type = Type();
  _zoom.Init(Type()->GetViewOptics());

  _destrType = type->GetDestructType();
  if (_destrType==DestructDefault)
    _destrType=DestructEngine;

  // note: passing driver is obsolete
  DoAssert(driver==NULL);

  DriverConstruct(driver);
  _manCargo.Resize(type->_maxManCargo);
  _cargoAssigned.Resize(type->_maxManCargo);
  
  _engineTemperatureCoef = 0.0f;
  _wheelTemperatureCoef = 0.0f;

  // open fire by default
  _weaponsState._fire._firePrepareOnly = false;

  int n = type->_turrets.Size();
  _turrets.Realloc(n);
  _turrets.Resize(n);
  for (int i=0; i<n; i++)
    _turrets[i] = new Turret(*type->_turrets[i], this);

  // FIX: WeaponState.Init cannot be called inside upper new Turret, as the _turrets contains NULLs
  // and Turret constructor uses ForEachTurret call which assumes the _turrets are initialized already
  // more info: news:hvagam$qjg$1@new-server.localdomain
  InitWeaponStateFunc func;
  ForEachTurret(func);
  // initialize vision modes
  if (type->_viewOptics.HasVisionModes())
    type->_viewOptics.GetInitVisionMode(_visionMode, _tiIndex);

  //AI is testing thread only a few times in second, random value is to avoid synchronized launch
  _lastTestedCM = Glob.uiTime + GRandGen.RandomValue();

  //units will eject from vehicle without wheels/tracks
  _allowCrewInImmobile = false;

#if _VBS3
  //this has to be done after all turrets are created
  LinkOverrideTurret func;
  for (int i=0; i<n; i++)
    _turrets[i]->ForEachTurret(unconst_cast(this), Type()->_turrets[i], func);
#endif
}

Person *Transport::Observer() const
{
  TurretContext context;
  if (!GetPrimaryObserverTurret(context) || !context._turret)
    return NULL;
  return context._gunner;
}

Person *Transport::Gunner() const
{
  TurretContext context;
  if (!GetPrimaryGunnerTurret(context) || !context._turret)
    return NULL;
  return context._gunner;
}

AIBrain *Transport::GetGunnerAssigned() const
{
  TurretContext context;
  if (!GetPrimaryGunnerTurret(context) || !context._turret)
    return NULL;
  return context._turret->GetGunnerAssigned();
}

AIBrain *Transport::GetCommanderAssigned() const
{
  TurretContext context;
  if (!GetPrimaryObserverTurret(context) || !context._turret)
    return NULL;
  return context._turret->GetGunnerAssigned();
}

void Transport::AssignGunner(AIBrain *unit)
{
  TurretContext context;
  if (!GetPrimaryGunnerTurret(context) || !context._turret)
    return;
  context._turret->AssignGunner(unit);
}

void Transport::AssignCommander(AIBrain *unit)
{
  TurretContext context;
  if (!GetPrimaryObserverTurret(context) || !context._turret)
    return;
  context._turret->AssignGunner(unit);
}

void Transport::AssignCargo(int index, AIBrain *unit)
{
  Assert(index >= 0 && index < _cargoAssigned.Size());
  _cargoAssigned[index] = unit;
}

void Transport::LeaveCargo(AIGroup *grp)
{
  for (int i=0; i<_cargoAssigned.Size(); i++)
  {
    if (_cargoAssigned[i] && _cargoAssigned[i]->GetGroup()==grp)
      _cargoAssigned[i] = NULL;
  }
}

void Transport::DriverConstruct( Person *driver )
{
  _driver=driver;
  // change brain vehicle
  if( driver )
  {
    AIBrain *brain=_driver->Brain();
    if (brain)
      brain->SetVehicleIn(this);
  }
}

bool Transport::GetEnableCompass(AIBrain *unit) const
{
  int canSee = 0;
  if (unit == ObserverUnit())
    canSee = Type()->_commanderCanSee;
  else if (unit == PilotUnit())
    canSee = Type()->_driverCanSee;
  else if (unit == GunnerUnit())
    canSee = Type()->_gunnerCanSee;
  return (canSee & CanSeeCompass) != 0;
}

float TransportVisualState::GetAltRadar(Ref<LODShapeWithShadow> shape) const
{
  const Shape *geom = shape->LandContactLevel();
  if (!geom)
    geom = shape->GeometryLevel();
  Vector3Val min = geom ? geom->Min() : shape->Min();
  Vector3Val wPos = PositionModelToWorld(Vector3(0.0f, min.Y(), 0.0f));

  // altRadar is used to animate dampers for helicopters
  // therefore we sometimes need it to be quite accurate and to consider roadways
  // to avoid computing it when not necessary, we do not perform this computation for fast moving vehicles
  float surfY = (Speed().SquareSize() < Square(50.0f)) ?
    GLandscape->RoadSurfaceYAboveWater(wPos, NULL, NULL, NULL, Landscape::UONoLock) : GLandscape->SurfaceYAboveWaterNoWaves(wPos.X(), wPos.Z());
  return wPos.Y() - surfY;
}


float TransportVisualState::GetAltBaro() const
{
  return Position().Y();
}

float TransportVisualState::GetSpeedHorizontal() const
{
  return fabs(ModelSpeed()[2]);
}

float TransportVisualState::GetSpeedVertical() const
{
  return Speed().Y();
}

float TransportVisualState::GetDirection() const
{
  Vector3Val dir = Direction();
  return atan2(dir.X(), dir.Z());
}

float Transport::GetCtrlTurretDirection(ObjectVisualState const& vs) const
{
  TurretContext context;
  Vector3 dir = GetPrimaryGunnerTurret(context) ? GetWeaponDirection(vs, context, 0) : vs.Direction();
  return atan2(dir.X(), dir.Z());
}

float TransportVisualState::GetHorizonBank() const
{
  Vector3 asideDir;
  asideDir[0] = +Direction().Z();
  asideDir[1] = 0;
  asideDir[2] = -Direction().X();

  Vector3 relAside = DirectionWorldToModel(asideDir);
  relAside[2] = 0;
  relAside.Normalize();

  float dive = atan2(-relAside.Y(),relAside.X());
  return dive;
}

float TransportVisualState::GetHorizonDive() const
{
  float dive = asin(Direction().Y());
  return dive;
}

float TransportVisualState::GetFuelFraction(TransportType const* type) const
{
  float capacity = type->GetFuelCapacity();
  if (capacity <= 0.0f)
    return 0.0f;
  return GetFuel() / capacity;
}

float TransportVisualState::GetGMeter() const {return _acceleration.Size() * (1.0f / G_CONST);}
float TransportVisualState::GetGMeterX() const {return DirectionWorldToModel(_acceleration).X() * (1.0f / G_CONST);}
float TransportVisualState::GetGMeterY() const {return DirectionWorldToModel(_acceleration).Y() * (1.0f / G_CONST);}
float TransportVisualState::GetGMeterZ() const {return DirectionWorldToModel(_acceleration).Z() * (1.0f / G_CONST);}

TurretVisualState* TransportVisualState::GetTurret(const int *path, int size)
{
  if (size <= 0)
  {
    LogF("Empty turret path");
    return NULL;
  }
  int i = path[0];
  if (i<0 || i>=_turretVisualStates.Size())
  {
    LogF("Invalid turret path");
    return NULL;
  }
  return _turretVisualStates[i]->GetTurret(path + 1, size - 1);
}

/*!
\patch 5163 Date 6/1/2007 by Ondra
- Fixed: Scripting function person moveInTurret [vehicle,[x,y,z]] crashed when [x,y,z] was invalid.
*/
TurretVisualState const* TransportVisualState::GetTurret(const int *path, int size) const
{
  if (size <= 0)
  {
    LogF("Empty turret path");
    return NULL;
  }
  int i = path[0];
  if (i<0 || i>=_turretVisualStates.Size())
  {
    LogF("Invalid turret path");
    return NULL;
  }
  return _turretVisualStates[i]->GetTurret(path + 1, size - 1);
}

/*!
\patch 1.23 Date 9/12/2001 by Ondra
- Fixed: Empty tractor and Rapid cars were flying 1 m above ground.
\patch_internal 1.23 Date 9/12/2001 by Ondra
- Fixed: positioning on objects containing proxies might be incorrect.
*/
void Transport::PlaceOnSurface(Matrix4 &trans)
{
  base::PlaceOnSurface(trans);

  if (!GetShape() || GetShape()->NLevels()<=0)
    return;

  // place in steady position
  Vector3 pos=trans.Position();
  Matrix3 orient=trans.Orientation();

  float dx,dz;
#if _ENABLE_WALK_ON_GEOMETRY
  pos[1] = GLandscape->RoadSurfaceYAboveWater(pos,Landscape::FilterPrimary(), -1, &dx,&dz);
#else
  pos[1] = GLandscape->RoadSurfaceYAboveWater(pos,&dx,&dz);
#endif

  //if( type->IsKindOf(GWorld->Preloaded(VTypeStatic)) )
  if (!Type()->HasDriver())
  {
    // static vehicle - place as in bulldozer
    pos += orient * GetShape()->BoundingCenter();
  }
  else
  {
    // move and change orientation
    Vector3 up(-dx,1,-dz);
    Matrix3 orient;
    orient.SetUpAndDirection(up,trans.Direction());
    trans.SetOrientation(orient);

    const Shape *geom = _shape->LandContactLevel();
    if (!geom)
      geom = _shape->GeometryLevel();
    if (geom)
    {
      Vector3 minC(0, geom->Min().Y(), 0);
      pos -= orient*minC;
    }
    else if (_shape->NLevels()>0)
    {
      ShapeUsed level0 = _shape->Level(0);
      Vector3 minC(0, level0->Min().Y(), 0);
      pos -= orient*minC;
    }
    else
    {
      Vector3 minC(0, _shape->Min().Y(), 0);
      pos -= orient*minC;
    }
  }
  trans.SetPosition(pos);
}

void Transport::EngineOn()
{
  bool isStatic = GetType()->GetFuelCapacity()<=0;
  if (isStatic || !_engineOff)
    return;
  if (!isStatic && GetFuel()<=0) return; // fuel expected, no fuel, no engine
  _engineOff = false;
  OnEvent(EEEngine,!_engineOff);
}

void Transport::EngineOff()
{
  if (_engineOff || !EngineCanBeOff())
    return;
  _engineOff = true;
  OnEvent(EEEngine,!_engineOff);
}

Vector3 Transport::GetEnginePos() const
{
  // sound source position in model coord
  return Vector3(0,0,1);
}

Vector3 Transport::GetEnvironPos() const
{
  // sound source position in model coord
  // aerodynamic sound is usually in front of us
  // tanks and other vehicles might be more ground-based
  return Vector3(0,-1,2);
}

AIBrain *Transport::DriverBrain() const
{
  return _driver ? _driver->Brain() : NULL;
}

AIBrain *Transport::GunnerBrain() const
{
  Person *person = Gunner();
  return person ? person->Brain() : NULL;
}

AIBrain *Transport::ObserverBrain() const
{
  Person *person = Observer();
  return person ? person->Brain() : NULL;
}

AIBrain *Transport::PilotUnit() const
{
  return DriverBrain();
}

AIBrain *Transport::GunnerUnit() const
{
  if (Type()->_hasGunner)
    return GunnerBrain();
  else
    return DriverBrain();
}

AIBrain *Transport::ObserverUnit() const
{
  return ObserverBrain();
}

AIBrain *Transport::EffectiveObserverUnit() const
{
  #if _ENABLE_CHEATS
    static bool obsCom = false;
    if (obsCom)
    {
      return CommanderUnit();
    }
  #endif
  // if effective commander is not in cargo, use him as an effective observer as well
  AIBrain *commander = CommanderUnit();
  if (commander && !commander->IsInCargo())
  {
    return commander;
  }
  // roles of commander / observer separated. Observer commands gunners, commanders commands movement
  // this is done to handle situation when player is commanding the vehicle from cargo (or optinally also driver) position, and is unable to command gunners efficiently
  // see http://dev-heaven.net/issues/2353
  AIBrain *unit = ObserverUnit();
  if (!unit)
  {
    unit = CommandingGunner();
  }
  return unit;
}

AIBrain *Transport::EffectiveGunnerUnit(const TurretContext &context) const
{
  if (_manualFire && context._turretType && context._turretType->_primaryGunner)
    return CommanderUnit();
  else
    return base::EffectiveGunnerUnit(context);
}

/// Functor searching the person with the best commanding level
class FindCommander : public ITurretFunc
{
protected:
  AIBrain *&_commander;
  float &_maxCommanding;

public:
  FindCommander(AIBrain *&commander, float &maxCommanding)
    : _commander(commander), _maxCommanding(maxCommanding) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turretType) return false; // continue
    if (!context._gunner) return false; // continue
    if (!context._gunner->Brain()) return false; // continue
    if (context._turretType->_commanding > _maxCommanding)
    {
      _commander = context._gunner->Brain();
      _maxCommanding = context._turretType->_commanding;
    }
    return false; // check all turrets
  }
};

#if _VBS3
/// Functor searches for the player inside vehicle.
/// he becomes the commander
class FindPlayer : public ITurretFunc
{
protected:
  AIBrain *&_player;

public:
  FindPlayer(AIBrain *&player): _player(player){}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turretType) return false; // continue
    if (!context._gunner) return false; // continue
    if (!context._gunner->Brain()) return false; // continue
    if(context._gunner->Brain()->IsPlayer())
      _player = context._gunner->Brain();

    return false; // check all turrets
  }
};
#endif

AIBrain *Transport::CommandingGunner() const
{
#if _VBS3
  // Player takes precedence over config vehicle
  AIBrain *commander = DriverBrain();
  FindPlayer funcFP(commander);
  ForEachTurret(funcFP);
#else
  AIBrain *commander = DriverBrain();
#endif

  // when driver is commander and driver is set, no gunner can be commander
  if (!commander || !Type()->_driverIsCommander)
  {
    // search for commander in turrets
    // find the person with the best commanding level (driver has commanding level 0)
    float maxCommanding = commander ? 0 : -FLT_MAX;
    FindCommander func(commander, maxCommanding);
    ForEachTurret(func);
  }
  return commander;
}

AIBrain *Transport::CommanderUnit() const
{
  if (_effCommander)
  {
    // check if effective commander is in the vehicle
    AIBrain *unit = _effCommander->Brain();
    if (unit && unit->GetVehicleIn() == this)
      return unit;
  }

  AIBrain *commander = CommandingGunner();

  if (!commander)
  {
    // search for commander in cargo
    for (int i=0; i<_manCargo.Size(); i++)
    {
      Person *man = _manCargo[i];
      if (man && man->Brain())
      {
        commander = man->Brain();
        break;
      }
    }
  }
  return commander;
}

Person *Transport::CommanderPerson() const
{
  AIBrain *com = CommanderUnit();
  return com ? com->GetPerson() : NULL;
}

bool Transport::IsCrewed() const
{
  if (Driver() && !Driver()->IsDamageDestroyed())
    return true;
  class IsSomeGunner : public ITurretFunc
  {
  public:
    IsSomeGunner() {}
    bool operator () (EntityAIFull *entity, TurretContext &context)
    {
      if (!context._turret) return false;
      if (!context._gunner) return false;  // continue
      if (context._gunner->IsDamageDestroyed()) return false;
      return true; // break - found
    }
  };
  IsSomeGunner func;
  if (ForEachTurret(func))
    return true;
  for (int i=0; i<GetManCargo().Size(); i++)
  {
    if (GetManCargo()[i] && !GetManCargo()[i]->IsDamageDestroyed())
      return true;
  }
  // conditions similar to TransportCanMove
  AIGroup *group = GetGroupAssigned();
  if (
    // group empty, we know the vehicle is harmless now
    !group || group->NAliveUnits()==0
    // owning side changed, we need to report civilian, as old side makes no longer any sense
    || group->GetCenter() && group->GetCenter()->GetSide()!=_targetSide
  )
  {
    return false;
  }
  if (GetType()->HasDriver())
  {
    if (!DriverBrain())
    { // owning group knows the driver is dead
      AIBrain * newDriver = GetDriverAssigned();
      if (!newDriver) return false; // no new driver, assume the vehicle is empty for good
      // note: the replacement driver might be dead as well, and group does not know it. This does not matter to us.
      Vector3 newDriverPos = newDriver->GetCurrentPosition(); // check how far is the assigned driver, if he is too far, we do not need to care about him
      float dist2 = newDriverPos.Distance2(FutureVisualState().Position());
      const float farEnough = 100.0f; // unit at 100 will run ~ 20 seconds before reaching the vehicle
      if (dist2>Square(farEnough)) return false;
    }
  }
  return true;
}

TargetSide Transport::GetTargetSideAsIfAlive() const
{
  if (!IsCrewed()) return TCivilian;
  return base::GetTargetSideAsIfAlive();
}

TargetSide Transport::GetTargetSide() const
{
  if (!IsDamageDestroyed())
  {
    if (!IsCrewed()) return TCivilian;
  }
  return base::GetTargetSide();
}

float Transport::VisibleLights() const
{
  // TODO: consider light dammage
  return _pilotLight;
}

bool Transport::QIsManual() const
{
  if (!GWorld->PlayerManual()) return false;
  if (!GWorld->PlayerOn()) return false;
  if (GWorld->PlayerOn() == Observer()) return true;
  if (GWorld->PlayerOn() == Driver()) return true;
  if (GWorld->PlayerOn() == Gunner()) return true;
  return false;
}

bool Transport::QIsManualMovement() const
{
  if (!GWorld->PlayerManual()) return false;
  if (!GWorld->PlayerOn()) return false;
  if (GWorld->PlayerOn() == Observer()) return true;
  if (GWorld->PlayerOn() == Driver()) return true;
  return false;
}

bool Transport::QIsManual(const AIBrain *unit) const
{
  if (!GWorld->PlayerManual()) return false;
  if (!GWorld->PlayerOn()) return false;
  if (!unit) return false;
  return unit==GWorld->PlayerOn()->Brain();
}

/*!
\patch 5137 Date 3/6/2007 by Bebul
- Fixed: head movement not visible on players in vehicle
*/
static void MovePersonHead(Transport *vehicle, Person *person, const ViewPars &viewPars)
{
  if (!person) return;
  AIBrain *unit = person->Brain();
  if (!unit || unit->IsAnyPlayer()/*vehicle->QIsManual(unit)*/ )
    return;

  // if there is any target assigned, track it
  Vector3 watchDir = unit->GetWatchHeadDirection();
  Vector3 relDir = vehicle->DirectionWorldToModel(watchDir);
  // if target is forward, look forward
  float heading = atan2(relDir.X(), relDir.Z());
  // TODO: calculate dive as well
  float dive = 0.0f;
  float fov = 1.0f;
  // apply head constraints
  viewPars.LimitVirtual(CamInternal,heading,dive,fov);
  Matrix3 rotHeading(MRotationY,-heading);
  Vector3 dir = rotHeading.Direction();
  // TODO: convert to the proxy space
  // currently we assume proxy space is the same as model space
  person->AimHeadCrew(dir);
}

/// Functor aiming head of the AI gunner
class MoveGunnerHead : public ITurretFunc
{
public:
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turretType) return false; // continue
    Person *person = context._gunner;
    if (!person) return false; // continue
    Transport *vehicle = static_cast<Transport *>(entity); // we know where we call it

    int opticsMode = (context._turret)?context._turret->_currentViewGunnerMode : 0;
    MovePersonHead(vehicle, person, context._turretType->_viewGunner[opticsMode]);

    return false; // continue
  }
};

void Transport::MoveCrewHead()
{
  //driver
  MovePersonHead(this, _driver, Type()->_viewPilot);
  //cargo
  for (int i=0; i < _manCargo.Size(); i++)
  {
    MovePersonHead(this, _manCargo[i], Type()->_viewCargo);
  }
  //gunners
  MoveGunnerHead func;
  ForEachTurret(func); //these works for persons in turrets (excluding driver and cargo)
}

static void RemoveMan( Person *man, EntityAI *killer )
{
  //if( man==GLOB_WORLD->PlayerOn() ) GWorld->PlayerKilled(killer);
  //GLOB_WORLD->RemoveOutVehicle(man);
  GLOB_WORLD->RemoveSensor(man);
}

/*
static void RemoveManUnit( AIUnit *unit )
{
  if (unit)
    unit->SendAnswer(AI::UnitDestroyed);
}
*/

static bool CrewDamage(EntityAIFull *entity, Person *man, EntityAI *killer, float overkill, RString ammo)
{
  if (!man)
    return false;
  float dammage = overkill*(GRandGen.RandomValue()*0.5+0.5);
  if (dammage >= 0.0f)
  {
    Vector3 cDammagePoint = man->GetCommonDamagePoint();

    Object::DoDamageResult result;
    result.damage = dammage;
    man->ApplyDoDamage(result,killer, ammo);

    // if man is not local, IsDamageDestroyed is unlikely to be true
    // however it is safer not to rely upon it too much
    // we must not return true for remote units, as KilledBy cannot be called for them
    if (man->IsDamageDestroyed() && man->IsLocal())
    {
      RemoveMan(man,killer);
      return true;
    }
  }
  return false;
}

/// Functor damaging gunner
class DammageGunner : public ITurretFunc
{
protected:
  EntityAI *_killer;
  float _howMuch;
  RString _ammo;

public:
  DammageGunner(EntityAI *killer, float howMuch, RString ammo)
    : _killer(killer), _howMuch(howMuch), _ammo(ammo) {}

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turret && context._gunner && CrewDamage(entity, context._gunner, _killer, _howMuch, _ammo) )
      context._gunner->KilledBy(_killer);
    return false;
  }
};

void Transport::DamageCrew( EntityAI *killer, float howMuch, RString ammo )
{
  if (!IsLocal())
  {
    ErrF("Cannot dammage remote transport %s",(const char *)GetDebugName());
    //return;
  }
  Ref<Person> man;
  for(int i=_manCargo.Size(); --i>=0;)
  {
    man = _manCargo[i];
    if (!man)
      continue;
    if (CrewDamage(this,man,killer,howMuch,ammo))
      man->KilledBy(killer);
  }
  // crew can also survive
  man = _driver;
  if (man && CrewDamage(this,man,killer,howMuch,ammo))
  {  
    man->KilledBy(killer);
    //try to assign new driver
    if (_assignDriverAfterDamage>Glob.time+15)
      _assignDriverAfterDamage = Glob.time+GRandGen.Gauss(1.5,3,7);
  }
  DammageGunner func(killer, howMuch, ammo);
  ForEachTurret(func);

  //vehicle is under fire - if main gunner is dead, try to assign new one
  TurretContext context;
  bool valid = GetPrimaryGunnerTurret(context);
  if (valid && context._gunner)
  {
    AIBrain *gunnerUnit = context._gunner->Brain();
    if (!gunnerUnit)
    {
      // dead gunner found and his death is known - we will try to find replacement
      if (_assignGunnerAfterDamage>Glob.time+15)
        // not set yet
        _assignGunnerAfterDamage = Glob.time+GRandGen.Gauss(1.5,3,7);
    }
  }

  if (EjectNeeded())
  {
    if (_getOutAfterDamage>Glob.time+15)
      // not set yet
      _getOutAfterDamage = Glob.time+GRandGen.Gauss(1.5,3,7);
  }
}

void Transport::HitBy( EntityAI *killer, float howMuch, RString ammo, bool wasDestroyed)
{
  if (!IsLocal())
    return;
  base::HitBy(killer,howMuch, ammo,wasDestroyed);
  if (howMuch > 0.0f)
    DamageCrew(killer,howMuch, ammo);
}

ArcadeMarkerInfo *FindMarker(RString name);

static ArcadeMarkerInfo *FindVehicleRespawnMarker(Transport *vehicle)
{
  if (vehicle->GetVarName().GetLength() > 0)
  {
    RString name = RString("respawn_") + vehicle->GetVarName();
    ArcadeMarkerInfo *info = FindMarker(name);
    if (info)
      return info;
  }

  TargetSide side = vehicle->GetRespawnSide();
  RString name1, name2;
  switch (side)
  {
  case TWest:
    name1 = "respawn_vehicle_west";
    name2 = "respawn_west";
    break;
  case TEast:
    name1 = "respawn_vehicle_east";
    name2 = "respawn_east";
    break;
  case TGuerrila:
    name1 = "respawn_vehicle_guerrila";
    name2 = "respawn_guerrila";
    break;
  case TCivilian:
    name1 = "respawn_vehicle_civilian";
    name2 = "respawn_civilian";
    break;
  }
  if (name1.GetLength() > 0)
  {
    ArcadeMarkerInfo *info = FindMarker(name1);
    if (info)
      return info;
  }
  if (name2.GetLength() > 0)
  {
    ArcadeMarkerInfo *info = FindMarker(name2);
    if (info)
      return info;
  }
  return FindMarker("respawn");
}

Vector3 SelectPositionInMarker(ArcadeMarkerInfo *info);

Vector3 VehicleRespawnInBasePosition(Transport *vehicle)
{
  Vector3 pos = vehicle->WorldPosition(vehicle->FutureVisualState());

  ArcadeMarkerInfo *info = FindVehicleRespawnMarker(vehicle);
  if (info)
    pos = SelectPositionInMarker(info);

  // select free position when processing respawn
  /*
  AIUnit::FindNearestEmpty(pos, false, vehicle);
  pos[1] = GLandscape->RoadSurfaceYAboveWater(pos + VUp * 0.5f);
  */

  return pos;
}

void Transport::Respawn()
{
  if (GetRespawnSide() != TSideUnknown && !IsRespawning() &&
    GWorld->GetMode() == GModeNetware && GetRespawnDelay() >= 0)
  {
    RespawnMode mode = GetNetworkManager().GetRespawnMode();
    if (mode == RespawnAtPlace || mode == RespawnInBase)
    {
      // process respawn
      Vector3 respawnPos;
      if (mode == RespawnInBase)
        respawnPos = VehicleRespawnInBasePosition(this);
      else
        respawnPos = WorldPosition(FutureVisualState());
      GetNetworkManager().Respawn(this, respawnPos);
      SetRespawning(true);
      GetNetworkManager().UpdateObject(this);
    }
  }
}

// dammage vehicle is heated up
static float MaxDamageHeatUpTime = 5.0f;

void Transport::Destroy(EntityAI *killer, float overkill, float minExp, float maxExp)
{
  // vehicles respawn
  Respawn();
  if (GetRespawnSide() != TSideUnknown && !IsRespawning() &&
    GWorld->GetMode() == GModeNetware && GetRespawnDelay() >= 0)
  {
    RespawnMode mode = GetNetworkManager().GetRespawnMode();
    if (mode == RespawnAtPlace || mode == RespawnInBase)
    {
      // process respawn
      Vector3 respawnPos;
      if (mode == RespawnInBase)
        respawnPos = VehicleRespawnInBasePosition(this);
      else
        respawnPos = WorldPosition(FutureVisualState());
      GetNetworkManager().Respawn(this, respawnPos);
      SetRespawning(true);
      GetNetworkManager().UpdateObject(this);
    }
  }
  base::Destroy(killer,overkill,minExp,maxExp);
  // kill or eject all brains
  DamageCrew(killer,overkill,"");
  // set all hitpoints to 1
  for (int i=0; i<_hit.Size(); i++)
    ChangeHit(killer, i, 1.0f, true);
  // make sure lights go off
  _pilotLight = false;
  GLOB_WORLD->RemoveSensor(this); // vehicle destroyed
  UpdateStop();
  // dammage vehicle is heated up
  _destroyMaxHeatTime = Glob.time + MaxDamageHeatUpTime;
}

/*!
\patch 1.22 Date 8/30/2001 by Ondra
- Fixed: AI units now eject from damaged helicopter or plane.
\patch 1.34 Date 12/6/2001 by Ondra
- Fixed: MP: When plane or helicopter was damaged, player auto ejected.
*/

bool Transport::EjectCrew(Person *man)
{
  AIBrain *unit = man->Brain();
  if (!unit)
    return false;
  AIGroup *group = unit->GetGroup();
  if (!group)
    return false;
  Eject(unit);
  if (group)
    group->UnassignVehicle(this);
  return true;
}

bool Transport::EjectIfAlive(Person *man)
{
  if (!man || man->IsDamageDestroyed()) return false;
  if (man->IsNetworkPlayer()) return false;
  AIBrain *unit = man->Brain();
  if (!unit || !unit->LSIsAlive()) return false;

  Eject(unit);

  AIGroup *group = unit->GetGroup();
  if (group) group->UnassignVehicle(this);

  return true;
}

bool Transport::EjectIfAliveCargo()
{
  int ejected = 0;
#define EJECT_GRID_SIZE 0.7f
  for(int i = 0; i < _manCargo.Size(); i++)
  {
    Person * man = _manCargo[i];
    if (!man || man->IsDamageDestroyed())
      continue;

    if (man->IsNetworkPlayer())
      continue;

    AIBrain *unit = man->Brain();
    if (!unit || !unit->LSIsAlive()) continue;

    Vector3 diff(0,ejected / 3 * EJECT_GRID_SIZE,0);
    int xd = ejected % 3;
    switch(xd)
    {
    case 0:diff[0] = 0; break;
    case 1:diff[0] = EJECT_GRID_SIZE; break;
    case 2:diff[0] = -EJECT_GRID_SIZE; break;
    }

    Eject(unit, diff);
    ejected++;

    AIGroup *group = unit->GetGroup();
    if (group) group->UnassignVehicle(this);
  }

  return ejected > 0;
}

bool Transport::AssignCargoToDriver()
{//move man from cargo to dead driver position

  _assignDriverAfterDamage = TIME_MAX;

  if(_driverAssigned)
  {
    //try to reset _getOutAfterDamage, if only reason was dead driver
    if (!EjectNeeded())
    {  
      _getOutAfterDamage = TIME_MAX;
      return true;
    }
    return false;
  }

  if(!IsPossibleToGetIn()) return false;

  for(int i = 0; i < _manCargo.Size(); i++)
  {
    Person * man = _manCargo[i];
    if (!man || man->IsDamageDestroyed())
      continue;

    if (man->IsNetworkPlayer())
      continue;

    AIBrain *unit = man->Brain();
    if (!unit || !unit->LSIsAlive() || !unit->GetPerson()) continue;

    int currentCompartments = GetPersonCompartments(unit->GetPerson());
    if (CanMoveToDriver(unit, currentCompartments))
    {
      UIActionType type = GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)) ? ATMoveToPilot : ATMoveToDriver;
      Ref<Action> action = new ActionBasic(type, this);
      ChangePosition(action, unit->GetPerson(),false, true);
    }

    //try to reset _getOutAfterDamage, if only reason was dead driver
    if (!EjectNeeded())
      _getOutAfterDamage = TIME_MAX;

    return true;

  }
  return false;
}

bool Transport::AssignCargoToGunner()
{//move man from cargo to dead driver position

  _assignGunnerAfterDamage = TIME_MAX;

  TurretContextV context;
  if(!GetPrimaryGunnerTurret(unconst_cast(FutureVisualState()), context))    return false;

  if(context._gunner && context._gunner->Brain())
  {
    return false;
  }

  if(!IsPossibleToGetIn()) return false;

  for(int i = 0; i < _manCargo.Size(); i++)
  {
    Person * man = _manCargo[i];
    if (!man || man->IsDamageDestroyed())
      continue;

    if (man->IsNetworkPlayer())
      continue;

    AIBrain *unit = man->Brain();
    if (!unit || !unit->LSIsAlive() || !unit->GetPerson()) continue;

    int currentCompartments = GetPersonCompartments(unit->GetPerson());
    if (CanMoveToTurret(context, unit, currentCompartments))
    {
      UIActionType type = ATMoveToGunner;
      Ref<Action> action = new ActionBasic(type, this);
      ChangePosition(action, unit->GetPerson(),false, true);

      return true;
    }
  }
  return false;
}

bool Transport::EjectIfDead(Person *man)
{
  if (!man || !man->IsDamageDestroyed()) return false;
  AIBrain *unit = man->Brain();
  if (!unit) return false;
  Assert(IsLocal());
  unit->DoGetOut(this,true,false);
  return true;
}

bool Transport::EjectIfDeadCargo()
{
  int ejected = 0;

  for(int i = 0; i < _manCargo.Size(); i++)
  {
    Person * man = _manCargo[i];
    if (!man || !man->IsDamageDestroyed()) continue;

    AIBrain *unit = man->Brain();
    if (!unit) continue;



    Vector3 diff(0,ejected / 3 * EJECT_GRID_SIZE,0);
    int xd = ejected % 3;
    switch(xd)
    {
    case 0:diff[0] = 0; break;
    case 1:diff[0] = EJECT_GRID_SIZE; break;
    case 2:diff[0] = -EJECT_GRID_SIZE; break;
    }

    Assert(IsLocal());
    unit->DoGetOut(this, true, false, diff);
    ejected++;
  }

  return ejected > 0;
}

/// Functor ejecting gunner from vehicle
class EjectGunner : public ITurretFunc
{
public:
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    Transport *vehicle = static_cast<Transport *>(entity); // we know where we call it
    if (context._gunner && context._turretType && context._turretType->_ejectDeadGunner)
      vehicle->EjectCrew(context._gunner);
    return false;
  }
};

/**
eject all crew-member who is sitting on a seat where he is not fixed.
We recognize such seats based on ejectDeadXxxx config flags.
*/
void Transport::EjectAllNotFixed()
{
  if (Driver() && Type()->_ejectDeadDriver)
  {
    EjectCrew(Driver());
  }
  EjectGunner func;
  ForEachTurret(func);
  if (Type()->_ejectDeadCargo)
  {
    for (int i=0; i<GetManCargo().Size(); i++)
    {
      Person *crew = GetManCargo()[i];
      if (crew) EjectCrew(crew);
    }
  }
}

void Transport::SetTotalDamageHandleDead(float damage)
{
  bool doRepair = damage < GetTotalDamage();
  base::SetTotalDamageHandleDead(damage);
  if (doRepair)
  {
    _getOutAfterDamage = TIME_MAX;
    _explosionTime = TIME_MAX;
  }
}

bool Transport::CheckEject() const
{
  return ManType::CheckEject(FutureVisualState().Position(),5);
}

/// Functor ejecting alive gunner from vehicle
class EjectAliveGunner : public ITurretFunc
{
public:
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    Transport *vehicle = static_cast<Transport *>(entity); // we know where we call it
    vehicle->EjectIfAlive(context._gunner);
    return false;
  }
};

/// Functor ejecting dead gunner from vehicle
class EjectDeadGunner : public ITurretFunc
{
public:
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turretType) return false; // continue
    Transport *vehicle = static_cast<Transport *>(entity); // we know where we call it
    if (context._turretType->_ejectDeadGunner) vehicle->EjectIfDead(context._gunner);
    return false; // continue
  }
};

void Transport::ReactToDamage()
{
  if (!_isDead && IsDamageDestroyed()) 
  {
    _isDead=true;
  }

  // engine off only when upside down and reached a stable state
  if( _isDead || _isUpsideDown && GetStopped())
  {
    // tank  up - it is finished
    EngineOff();
    _pilotLight = false;
  }

  if(Glob.time>_assignGunnerAfterDamage)
  {
    AssignCargoToGunner();
  }

  if(Glob.time>_getOutAfterDamage)
  {
    // all must get out - vehicle unusable
    // unassign all assigned soldiers
    // man->AssignAsCargo(NULL);
    // TODO: there may be some assigned soldier outside the vehicle
    // do not eject if the place is not suitable for the soldier
    //  if(!AssignCargoToDriver())
    if(_assignDriverAfterDamage > Glob.time+15 || !AssignCargoToDriver())
    {
      if (CheckEject())
      {
        EjectIfAliveCargo();
        EjectIfAlive(_driver);
        EjectAliveGunner func;
        ForEachTurret(func);
        _getOutAfterDamage = TIME_MAX;
      }
    }
  }

  if (IsLocal())
  {
    if (Type()->_ejectDeadDriver) EjectIfDead(_driver);
    EjectDeadGunner func;
    ForEachTurret(func);
    if (Type()->_ejectDeadCargo) EjectIfDeadCargo();
  }

  if (Glob.time>_explosionTime)
  {
    if (GetRawTotalDamage()<1 )
    {
      EntityAI *killer = this;
      if (_lastDammage && _lastDammageTime > Glob.time - 60)
        killer = _lastDammage;

      // kill crew (with slight chance of escape)
      DamageCrew(killer,10,"");
      // explode
      //LogF("%.3f Start smoke",Glob.time.toFloat());
      Destroy(killer,1.0,0.5,0.75);
      // set both the total damage and all hitpoints to 1
      SetTotalDamage(1, true);
      for (int i=0; i<_hit.Size(); i++) ChangeHit(killer, i, 1.0f, true);
    }
  }

}

float Transport::GetExplosives() const
{
  // if there is an explicit value, use it
  // if it is negative, it is coefficient
  // how much explosives is in
  return base::GetExplosives(); // ammunition
}

/*!
\patch 1.25 Date 09/29/2001 by Ondra
- Fixed: MP: When bad client-side prediction led to crash,
vehicle was damaged and could be even destroyed.
*/

void Transport::CrashDammage( float ammount, const Vector3 &pos, float range )
{
  // only local vehicles may crash
  if (IsLocal())
  {
    // if the vehicle has a driver, he is responsible for the crash
    AIBrain *brain = PilotUnit();
    EntityAI *responsible = this;
    if (brain && brain->GetPerson())
    {
      responsible = brain->GetPerson();
    }

    ammount*=GetType()->GetInvArmor();
    DoDamageResult result;
    LocalDamage(result,NULL,responsible,pos,ammount,(range > 0) ? range : GetRadius());
    ApplyDoDamageLocalHandleHitBy(result,responsible, RString());
  }
}

class RemoveAssignementFunc : public ITurretFunc
{
protected:
  AIBrain *_unit;

public:
  RemoveAssignementFunc(AIBrain *unit) : _unit(unit) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turret && context._turret->GetGunnerAssigned() == _unit)
      context._turret->AssignGunner(NULL);
    return false; // continue
  }
};

void Transport::RemoveAssignement(AIBrain *unit)
{
  if (_driverAssigned == unit)
    _driverAssigned = NULL;
  RemoveAssignementFunc func(unit);
  ForEachTurret(func);
  int i, n = _cargoAssigned.Size();
  for (i=0; i<n; i++)
  {
    if (_cargoAssigned[i] == unit) _cargoAssigned[i] = NULL;
  }
}

void Transport::KeyboardAny(AIUnit *unit, float deltaT)
{
}

RString Transport::DriverAction() const
{
  if (FutureVisualState().IsDriverHidden()) return Type()->_driverInAction;
  return Type()->_driverAction;
}
RString Transport::CargoAction(int pos) const
{
  int maxPos = Type()->_cargoAction.Size()-1;
  if (maxPos<0) return RString();
  saturateMin(pos,maxPos);
  return Type()->_cargoAction[pos];
}

float Transport::DriverAnimSpeed() const
{
  return 1;
}
float Transport::CargoAnimSpeed(int position) const
{
  return 1;
}

bool Transport::IsPersonHidden(ObjectVisualState const& vs, Person *person) const
{
  TransportVisualState const& tvs = vs.cast<Transport>();
  if (person == Driver())
    return tvs.IsDriverHidden();
  else
  {
    TurretContextV context;
    if (FindTurret(unconst_cast(tvs), person, context))
      return context._turret && context._turretVisualState->IsGunnerHidden();
  }
  if (!person->Brain()) return true;
  // not a driver, not a gunner, must be a cargo.
  // When commander is in cargo, decide based on a combat mode only (see also Transport::AICommander)
  return person->Brain()->GetCombatModeLowLevel()>=CMAware;
}

void Transport::HidePerson(Person *person, float hide)
{
  if (person == Driver())
    HideDriver(hide);
  else
  {
    TurretContext context;
    if (FindTurret(person, context) && context._turret && context._turretType->_canHideGunner)
      context._turret->HideGunner(hide);
  }
}

void Transport::SelectWeaponCommander(AIBrain *unit, const TurretContext &context, int weapon)
{
  // used when weapon is selected from UI
  Assert(unit);

#if _VBS3 //special case for convoy trainer
  if(unit->GetPerson() && unit->GetPerson()->IsPersonalItemsEnabled()
    && unit->GetPerson()->IsLocal())
  {
    context._weapons->SelectWeapon(this, weapon);
    return;
  }
#endif

  // manual fire is valid only for the primary gunner
  bool manualFire = false;
  if (context._turretType && context._turretType->_primaryGunner)
  {
    manualFire = IsManualFire();
  }

  AIBrain *gunner = context._gunner ? context._gunner->Brain() : NULL;
  if (unit == gunner || manualFire)
  {
    base::SelectWeaponCommander(unit, context, weapon);
  }
  else if (gunner)
  {
    if (IsLocal())
    {
      SendLoad(context._gunner, weapon);
    }
    else
    {
      RadioMessageVLoad msg(this, context._gunner, weapon);
      GetNetworkManager().SendMsg(&msg);
    }
  }
}

void Transport::CommanderLoadMagazine(CreatorId creator, int id, RString weapon, RString muzzle)
{
  AIBrain *gunnerUnit = GunnerUnit();
  if (!gunnerUnit) return;
  Person *gunner = gunnerUnit->GetPerson();
  if (!gunner) return;

  if (gunner->IsLocal())
  {
    SendLoadMagazine(gunner, creator, id, weapon, muzzle);
  }
  else
  {
    RadioMessageVLoadMagazine msg(this, gunner, creator, id, weapon, muzzle);
    GetNetworkManager().SendMsg(&msg);
  }
}

#if _ENABLE_AI

void Transport::CommandPrimaryGunner(AIBrain *unit, TurretContext &context)
{
  // old state
  int oldWeapon = _commanderFire._gunner == context._gunner ? _commanderFire._weapon : -1;
  // commander must remember his decision
  SelectFireWeapon(context, _commanderFire);

  if (_commanderFire._firePrepareOnly)
  {
    // we do not want to fire
    Target *tgt = _commanderFire._fireTarget;
    if (tgt)
    {
      // FIX
      // we have some target - check if we should cancel it
      // we cannot do it if our leader is player and it is out assigned target
      AIGroup *grp = unit->GetGroup();
      if (grp && !grp->IsAnyPlayerGroup() || tgt!=unit->GetTargetAssigned())
      {
        //AICenter *center = grp->GetCenter();
        // check if target is unsuitable
        if (tgt->State(unit)<TargetFireMin || unit->IsFriendly(tgt->GetSide()))
        {
#if 0
          LogF
            (
            "*** commander canceling target %s",
            (const char *)tgt->idExact->GetDebugName()
            );
#endif
          _commanderFire._fireTarget = NULL;
          _commanderFire._gunner = NULL;
          _commanderFire._weapon = -1;
          // unassign target if necessary
          if (tgt==unit->GetTargetAssigned())
          {
            unit->AssignTarget(TargetToFire());
          }
#if LOG_TARGET_CHANGES
          LogF(
            "%.1f: %s - Commander target canceled",
            Glob.time.toFloat(), (const char *)GetDebugName());
#endif
        }
      }
      // FIX END
    }
  }

  // send load only when weapon changed
  if (_commanderFire._gunner == context._gunner && _commanderFire._weapon >= 0 && _commanderFire._weapon != oldWeapon)
  {
    if (IsLocal())
      SendLoad(context._gunner, _commanderFire._weapon);
    else
    {
      RadioMessageVLoad msg(this, context._gunner, _commanderFire._weapon);
      GetNetworkManager().SendMsg(&msg);
    }
    //      SelectWeapon(_commanderFire._fireMode);
  }

  // TODO: better check the radio channel
  if (_radio.IsEmpty()) // radio protocol is empty
  {
    bool isAutonomous = false;
    int selected = context._weapons->ValidatedCurrentWeapon();
    if (selected >= 0)
    {
      const WeaponModeType *mode = context._weapons->GetWeaponMode(selected);
      if (mode && mode->_autoFire)
      {
        isAutonomous = true;
      }
    }

    if (_commanderFire._fireTarget != context._weapons->_fire._fireTarget)
    {
      if (
        !isAutonomous ||
        _commanderFire._fireTarget && _commanderFire._fireTarget->State(context._gunner->Brain())<TargetEnemyCombat
      )
      {
        if (_commanderFire._fireTarget)
        {
          SendTarget(context._gunner, _commanderFire._fireTarget);
        }
        else if (_radio.IsSilent())
        {
          // when channel is silent, we may transmit "no target"
          // (very low priority message)
          SendTarget(context._gunner, NULL);
        }
        if (!context._gunner->IsLocal())
        {
          context._weapons->_fire.SetTarget(context._gunner->Brain(), _commanderFire._fireTarget);
        }
      }
    }
    else
    {
      // FIX: do not send fire / cease fire to gunner for weapons with autoFire
      // (make gunner more independent)
      if (!_commanderFire._firePrepareOnly)
      {
        // gunner has _firePrepareOnly set when no fire order was received
        if (context._weapons->_fire._firePrepareOnly && _commanderFire._fireTarget)
        {
          if (!isAutonomous)
          {
            PreloadFireWeaponEffects(*context._weapons, selected);
            if (
              GetWeaponLoaded(*context._weapons, selected) &&
              GetAimed(context, selected, _commanderFire._fireTarget, false, true) >= 0.75 &&
              GetWeaponReady(*context._weapons, selected, _commanderFire._fireTarget))
            {
              if (!GetAIFireEnabled(_commanderFire._fireTarget)) ReportFireReady();
              else
              {
                SendSimpleCommand(context._gunner, SCFire);
              }
            }
          }
        }
      }
    }
  }
}

/*!
\patch 1.08 Date 7/21/2001 by Ondra.
- Fixed: Sometimes tank was aiming at target in 'prepare' mode only,
because the target was already inactive. Tank commander now cancels target
in such situation.

\patch 5156 Date 5/12/2007 by Ondra
- Fixed: M1A1 was never turning out in safe or careless mode.

\patch 5159 Date 5/16/2007 by Ondra
- Fixed: AI Commander no longer commands individual targets when using weapons
which gunner can operate autonomously, like machine guns.
*/

void Transport::AICommander(AIBrain *unit, float deltaT )
{
  Assert(unit);

  bool isDriver = unit == PilotUnit();
  if (PilotUnit())
  {
    if (unit->GetFormationLeader() == unit)
    {
      switch (unit->GetPlanningMode())
      {
      case AIUnit::LeaderPlanned:
        {
          // new valid path
          Vector3Val wantedPosition = unit->GetWantedDestination();
          float prec = GetPrecision();
          if (_moveMode != VMMMove || wantedPosition.Distance2(_driverPos) > 0)
          {
            if (isDriver)
            {
              // avoid radio protocol
              _moveMode = VMMMove;
              _driverPos = wantedPosition;
#if DIAG_WANTED_POSITION
              if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
                LogF("Commander %s: _driverPos set to %.0f, %.0f", cc_cast(GetDebugName()), wantedPosition.X(), wantedPosition.Z());
#endif
            }
            else if (wantedPosition.Distance2(_driverPos) > Square(prec))
            {
              if (_radio.IsEmpty()) // radio protocol is empty
              {
                SendMove(wantedPosition);
#if DIAG_WANTED_POSITION
                if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
                  LogF("Commander %s: _driverPos %.0f, %.0f sent", cc_cast(GetDebugName()), wantedPosition.X(), wantedPosition.Z());
#endif
              }
            }
            else
            {
              _moveMode = VMMMove;
              _driverPos = wantedPosition;
#if DIAG_WANTED_POSITION
              if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
                LogF("Commander %s: _driverPos set to %.0f, %.0f", cc_cast(GetDebugName()), wantedPosition.X(), wantedPosition.Z());
#endif
            }
          }
        }
        break;
      case AIUnit::DoNotPlan:
        {
          // FIX: set the direction we will move to when command like Forward will be received
          if (_moveMode == VMMFormation || _moveMode == VMMMove)
          {
            _dirWanted = FutureVisualState().Direction();
            _azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
          }

          _moveMode = VMMStop;
          _driverPos = FutureVisualState().Position();
        }
        break;
      }
    }
    else
    {
      if (_moveMode != VMMFormation)
      {
        if (isDriver)
          // avoid radio protocol
          ReceivedJoin();
        else if (_radio.IsEmpty())
          SendJoin();
      }
    }
  }

  // TODO: consider: turn in / turn out might be better controlled by the observer
  // turn in / turn out
  if (Type()->_hideProxyInCombat)
  {
    // find turret where the commander is
    // we need to check if turret allows turning out
    TurretContext commanderContext;
    bool commanderValid = FindTurret(unit->GetPerson(),commanderContext);
    if (unit->GetCombatModeLowLevel() >= CMAware ||
      (commanderValid && commanderContext._turretType && commanderContext._turretType->_forceHideGunner))
    {
      // some danger - head in
      Person *person = unit->GetPerson();
      HidePerson(person, 1);
    }
    else if (unit->GetCombatModeLowLevel() <= CMSafe)
    {
      // safe - we may turn heads out
      Person *person = unit->GetPerson();
      HidePerson(person, 0);
    }
  }
}

void Transport::AIObserver(AIBrain *unit, float deltaT )
{
  // command only the primary gunner, secondary gunners behave autonomously (their decisions are in Transport::SimulateGunner)
  TurretContext context;
  bool valid = GetPrimaryGunnerTurret(context);
  if (valid && context._gunner)
  {
    if (unit == context._gunner->Brain())
    {
      SelectFireWeapon(context);
      if (context._weapons->_fire._gunner == context._gunner && context._weapons->_fire._weapon >= 0)
        context._weapons->SelectWeapon(this, context._weapons->_fire._weapon);
    }
    else
      CommandPrimaryGunner(unit, context);
  }

}
#endif //_ENABLE_AI

static bool ReportIfDead(AIBrain *unit, const AIBrain *reportedBy)
{
  if (unit && !unit->LSIsAlive())
  {
    if (reportedBy)
    {
      AIGroup *grp = unit->GetGroup();
      if (grp)
        grp->SetReportBeforeTime(unit->GetUnit(), Glob.time + 10);
    }
    return true; // dead
  }
  return false; // alive
}

bool Transport::HasSomePilot() const
{
#if _ENABLE_CHEATS
  // pretend we have a pilot, but do not perform any activity
  extern bool disableUnitAI;
  if (disableUnitAI)
    return true;
#endif
  AIBrain *driverUnit = PilotUnit();
  return driverUnit!=NULL;
}

/// Functor cleans up gunner
class CleanUpGunner : public ITurretFunc
{
public:
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turret)
      context._turret->SetGunner(NULL);
    return false; // continue
  }
};

void Transport::CleanUp()
{
  _effCommander = NULL;
  _driver = NULL;
  CleanUpGunner func;
  ForEachTurret(func);
  _manCargo.Clear();
  base::CleanUp();
}

bool Transport::ForEachTurret(ITurretFunc &func) const
{
  // driver controlled weapons
  TurretContext context;
  context._turret = NULL;
  context._turretType = NULL;
  context._gunner = _driver;
  context._weapons = unconst_cast(&_weaponsState);
  if (func(unconst_cast(this), context))
    return true;

  int n = _turrets.Size();
  for (int i=0; i<n; i++)
  {
    if (_turrets[i]->ForEachTurret(unconst_cast(this), Type()->_turrets[i], func))
      return true;
  }
  return false;
}

class CrewInTurretFunc : public ITurretFunc
{
protected:
  ICrewFunc &func;
public:
  CrewInTurretFunc(ICrewFunc &functor) : func(functor) {}

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if (!context._gunner) return false;
    return func(context._gunner);
  }
};

bool Transport::ForEachCrew(ICrewFunc &func) const
{
  if (_driver && func(_driver))
    return true;
  CrewInTurretFunc crewEnumFunc(func);
  if (ForEachTurret(crewEnumFunc))
    return true;
  for (int i=0; i<_manCargo.Size(); i++)
  {
    if (_manCargo[i] && func(_manCargo[i]))
      return true;
  }
  return false;
}

bool Transport::ForEachTurret(EntityVisualState& vs, ITurretFuncV &func) const
{
  // driver controlled weapons
  TurretContextV context;
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = _driver;
  context._weapons = unconst_cast(&_weaponsState);
  if (func(unconst_cast(this), context)) return true;
  int n = _turrets.Size();
  if (n > 0)
  {
    Assert(static_cast_checked<TransportVisualState&>(vs)._turretVisualStates.Size() == n);
    for (int i=0; i<_turrets.Size(); i++)
    {
      if (_turrets[i]->ForEachTurret(*(static_cast_checked<TransportVisualState&>(vs))._turretVisualStates[i], unconst_cast(this), Type()->_turrets[i], func))
        return true;
    }
  }
  return false;
}

bool Transport::ForEachTurretEx(EntityVisualState& vs, ITurretFuncEx &func) const
{
  // driver controlled weapons
  TurretContextEx context;
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = _driver;
  context._weapons = unconst_cast(&_weaponsState);
  context._parentTrans = vs.Orientation();
  if (func(unconst_cast(this), context)) return true;
  int n = _turrets.Size();
  if (n > 0)
  {
    Assert(static_cast_checked<TransportVisualState&>(vs)._turretVisualStates.Size() == n);
    Matrix3 trans = vs.Orientation();
    for (int i=0; i<_turrets.Size(); i++)
    {
      if (_turrets[i]->ForEachTurretEx(*(static_cast_checked<TransportVisualState&>(vs))._turretVisualStates[i], unconst_cast(this), Type()->_turrets[i], trans, func))
        return true;
    }
  }
  return false;
}

/// Functor searching for turret
class FindTurretFunc : public ITurretFunc
{
protected:
  const Person *_gunner;
  TurretContext &_context; // out

public:
  FindTurretFunc(const Person *gunner, TurretContext &context) : _gunner(gunner), _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._gunner == _gunner)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};

#if _VBS3 //Commander override
//Get the turret for which that gunner gets his optics from
//This is used to get the optics for commanders
bool Transport::FindOpticsTurret(const Person *gunner, TurretContext &context) const
{
  FindTurretFunc func(gunner, context);
  if(!ForEachTurret(func)) return false;

  //check if we are currently looking through a different optic
  if(context._turret
    && context._turret->_overrideTurret
    &&(context._turret->_showOverrideOptics || context._turret->_overrideTurret->_overriddenByTurret)
    )
  {
    TurretContext contextNew;
    if(FindTurret(context._turret->_overrideTurret, contextNew))
    {
      context = contextNew;
    }
  }
  return true;
}

//find the turret the gunner currently controls
bool Transport::FindControlledTurret(const Person *gunner, TurretContext &context) const
{
  FindTurretFunc func(gunner, context);
  if(!ForEachTurret(func)) return false;

  //is the gunner currently controlling a different turret?
  if(context._turret
    && context._turret->_overrideTurret
    && context._turret->_overrideTurret->_overriddenByTurret == context._turret
    )
  {
    TurretContext contextNew;
    if(FindTurret(context._turret->_overrideTurret, contextNew))
    {
      //The gunner will be different to the one passed into this function
      context = contextNew;
    }
#if _ENABLE_CHEATS
    else
      Fail("override turret cannot be found");
#endif
  }
  return true;
}
#endif //_VBS3

bool Transport::FindTurret(const Person *gunner, TurretContext &context) const
{
  FindTurretFunc func(gunner, context);
  return ForEachTurret(func);
}

/// Functor searching for turret
class FindTurretFuncVisualState : public ITurretFuncV
{
protected:
  const Person *_gunner;
  TurretContextV &_context; // out

public:
  FindTurretFuncVisualState(const Person *gunner, TurretContextV &context) : _gunner(gunner), _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (context._gunner == _gunner)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};

bool Transport::FindTurret(EntityVisualState& vs, const Person *gunner, TurretContextV &context) const
{
  FindTurretFuncVisualState func(gunner, context);
  return ForEachTurret(vs, func);
}

/// Functor searching for turret
class FindTurretContextFunc : public ITurretFunc
{
protected:
  const Turret *_turret;
  TurretContext &_context; // out

public:
  FindTurretContextFunc(const Turret *turret, TurretContext &context) : _turret(turret), _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turret == _turret)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};

bool Transport::FindTurret(const Turret *turret, TurretContext &context) const
{
  FindTurretContextFunc func(turret, context);
  return ForEachTurret(func);
}

/// Functor searching for turret
class FindTurretContextFuncVisualState : public ITurretFuncV
{
protected:
  const Turret *_turret;
  TurretContextV &_context; // out

public:
  FindTurretContextFuncVisualState(const Turret *turret, TurretContextV &context) : _turret(turret), _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (context._turret == _turret)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};


bool Transport::FindTurret(EntityVisualState& vs, const Turret *turret, TurretContextV &context) const
{
  FindTurretContextFuncVisualState func(turret, context);
  return ForEachTurret(vs, func);
}

/// Functor searching for assigned position
class FindAssignedGunner : public ITurretFunc
{
protected:
  const AIBrain *_brain;
  TurretContext &_context; // out

public:
  FindAssignedGunner(const AIBrain *brain, TurretContext &context) : _brain(brain), _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turret && context._turret->GetGunnerAssigned() == _brain)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};

bool Transport::FindAssignedTurret(const AIBrain *brain, TurretContext &context) const
{
  FindAssignedGunner func(brain, context);
  return ForEachTurret(func);
}


class FindAssignedGunnerVisualState : public ITurretFuncV
{
protected:
  const AIBrain *_brain;
  TurretContextV &_context; // out

public:
  FindAssignedGunnerVisualState(const AIBrain *brain, TurretContextV &context) : _brain(brain), _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (context._turret && context._turret->GetGunnerAssigned() == _brain)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};


bool Transport::FindAssignedTurret(EntityVisualState& vs, const AIBrain *brain, TurretContextV &context) const
{
  FindAssignedGunnerVisualState func(brain, context);
  return ForEachTurret(vs, func);
}

/// Functor searching for primary gunner turret
class FindPrimaryGunnerTurretFunc : public ITurretFunc
{
protected:
  TurretContext &_context; // out

public:
  FindPrimaryGunnerTurretFunc(TurretContext &context) : _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turretType && context._turretType->_primaryGunner)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};

bool Transport::GetPrimaryGunnerTurret(TurretContext &context) const
{
  FindPrimaryGunnerTurretFunc func(context);
  if (ForEachTurret(func)) return true;

  // if no turret is marked as primary gunner, select driver weapon system
  context._turret = NULL;
  context._turretType = NULL;
  context._gunner = _driver;
  context._weapons = unconst_cast(&_weaponsState);
  return true;
}

bool Transport::GetDriverTurret(TurretContext &context) const
{
  // if no turret is marked as primary gunner, select driver weapon system
  context._turret = NULL;
  context._turretType = NULL;
  context._gunner = _driver;
  context._weapons = unconst_cast(&_weaponsState);
  return true;
}

/// Functor searching for primary gunner turret
class FindPrimaryGunnerTurretFuncVisualState : public ITurretFuncV
{
protected:
  TurretContextV &_context; // out

public:
  FindPrimaryGunnerTurretFuncVisualState(TurretContextV &context) : _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (context._turretType && context._turretType->_primaryGunner)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};

bool Transport::GetPrimaryGunnerTurret(EntityVisualState& vs, TurretContextV &context) const
{
  FindPrimaryGunnerTurretFuncVisualState func(context);
  if (ForEachTurret(vs, func)) return true;

  // if no turret is marked as primary gunner, select driver weapon system
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = _driver;
  context._weapons = unconst_cast(&_weaponsState);
  return true;
}

/// Functor searching for primary gunner turret
class FindPrimaryGunnerTurretFuncEx : public ITurretFuncEx
{
protected:
  TurretContextEx &_context; // out

public:
  FindPrimaryGunnerTurretFuncEx(TurretContextEx &context) : _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContextEx &context)
  {
    if (context._turretType && context._turretType->_primaryGunner)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};

bool Transport::GetPrimaryGunnerTurret(EntityVisualState& vs, TurretContextEx &context) const
{
  FindPrimaryGunnerTurretFuncEx func(context);
  if (ForEachTurretEx(vs, func)) return true;

  // if no turret is marked as primary gunner, select driver weapon system
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = _driver;
  context._weapons = unconst_cast(&_weaponsState);
  context._parentTrans = vs.Orientation();
  return true;
}

/// Functor searching for primary observer turret
class FindPrimaryObserverTurretFunc : public ITurretFunc
{
protected:
  TurretContext &_context; // out

public:
  FindPrimaryObserverTurretFunc(TurretContext &context) : _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turretType && context._turretType->_primaryObserver)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};

bool Transport::GetPrimaryObserverTurret(TurretContext &context) const
{
  FindPrimaryObserverTurretFunc func(context);
  return ForEachTurret(func);
}

/// Functor searching for primary observer turret
class FindPrimaryObserverTurretFuncVisualState : public ITurretFuncV
{
protected:
  TurretContextV &_context; // out

public:
  FindPrimaryObserverTurretFuncVisualState(TurretContextV &context) : _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (context._turretType && context._turretType->_primaryObserver)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};

bool Transport::GetPrimaryObserverTurret(EntityVisualState& vs, TurretContextV &context) const
{
  FindPrimaryObserverTurretFuncVisualState func(context);
  return ForEachTurret(vs, func);
}

/// Functor searching for primary observer turret
class FindPrimaryObserverTurretFuncEx : public ITurretFuncEx
{
protected:
  TurretContextEx &_context; // out

public:
  FindPrimaryObserverTurretFuncEx(TurretContextEx &context) : _context(context) {}
  bool operator () (EntityAIFull *entity, TurretContextEx &context)
  {
    if (context._turretType && context._turretType->_primaryObserver)
    {
      _context = context;
      return true; // found
    }
    return false; // continue
  }
};

bool Transport::GetPrimaryObserverTurret(EntityVisualState& vs, TurretContextEx &context) const
{
  FindPrimaryObserverTurretFuncEx func(context);
  return ForEachTurretEx(vs, func);
}

bool Transport::CanFireUsing(const TurretContext &context) const
{
  if (!base::CanFireUsing(context)) return false;

#if _VBS2
  if(context._turret && context._turret->_disableGunnerInput) return false;
#endif

  if (context._turretType && Type()->GetHideProxyInCombat() &&
    !context._turretType->_inGunnerMayFire && IsPersonHidden(FutureVisualState(), context._gunner)) return false;

  return true;
}

bool Transport::GetWeaponAim(TurretContext context, Vector3 &position, Vector3 &direction)
{

  Matrix4Val shootTrans = GunTurretTransform(FutureVisualState(), *context._turretType);

  Vector3Val gPosL = context._turretType->GetTurretPos(shootTrans);
  position = FutureVisualState().PositionModelToWorld(gPosL);
  direction = FutureVisualState().DirectionModelToWorld(context._turretType->GetTurretDir(shootTrans));

  return true;
}

static bool IsRemoteControlled(AIBrain *unit)
{
  Person *by = unit->GetRemoteControlled();
  return by && by == GWorld->PlayerOn();
}

static bool IsRemoteControlling(AIBrain *unit)
{
  if(unit) return unit->IsRemoteControlling();
  return false;
}

void Transport::SimulateGunner(TurretContextEx &context, float deltaT, SimulationImportance prec)
{
  AIBrain *gunnerUnit = context._gunner ? context._gunner->Brain() : NULL;

  bool manual = GLOB_WORLD->PlayerManual() && GLOB_WORLD->PlayerOn();

#if _VBS3 //Commander Override
  bool playerIsGunner = manual && gunnerUnit && gunnerUnit == GLOB_WORLD->PlayerOn()->Brain();
  //should this turret be controlled by someone else?
  bool override = false;
  if(context._turret)
  {
    Turret* overriddenByTurret = context._turret->_overriddenByTurret;
    if(overriddenByTurret)
    {
      if(overriddenByTurret->UpdateOverrideStatus())
      {
        if(overriddenByTurret->_gunner)
        {
          context._gunner = overriddenByTurret->_gunner;
          gunnerUnit = overriddenByTurret->_gunner->Brain();
          override = true;
        }
      }
    }
    else //check if gunner is currently controlling another turret
    {
      //player is able to override a turret
      if(playerIsGunner && context._turret->_overrideTurret)
      {
        if(context._turret->UpdateOverrideStatus())
          return;
      }
    }
  }
#endif //_VBS3

  if (!gunnerUnit || !gunnerUnit->LSIsAlive()) return;

  AIBrain *player = manual ? GLOB_WORLD->PlayerOn()->Brain() : NULL;
  if (manual && (gunnerUnit == player || IsRemoteControlled(gunnerUnit)))
  {
    // KeyboardGunner is partially realized in InGameUI
    if (context._turret)
    {
#if _VBS2 //convoy trainer
      Soldier* soldier = dyn_cast<Soldier>(context._gunner);
      if(soldier && soldier->IsPersonalItemsEnabled() && !override)
      {
        soldier->SimulateAI(deltaT,prec);
      }
      else
#endif
        KeyboardGunner(context, deltaT);
    }
  }
  else if (context._gunner->IsRemotePlayer())
  {
    // remote player - avoid fake commander fires
  }
#if _ENABLE_AI
  else if (gunnerUnit->IsLocal())
  {
    if (!context._weapons->_fire._fireTarget || context._weapons->_fire.GetTargetFinished(CommanderUnit()))
    {
      /*
      if (weapons._fire._fireTarget)
      {
      LogF("Finished: %d", context._weapons->_fire.GetTargetFinished(CommanderUnit()));
      }
      */
#if LOG_TARGET_CHANGES
      if (context._weapons._fire._fireTarget) LogF(
        "%.1f: %s - Fire target finished",
        Glob.time.toFloat(), (const char *)GetDebugName());
#endif
      context._weapons->_fire._gunner = NULL;
      context._weapons->_fire._weapon = -1;
      context._weapons->_fire._fireTarget = NULL;
    }


    // for the observer himself and the secondary gunners, the full SelectFireWeapon is called (fully autonomous behavior)
    if (gunnerUnit == EffectiveObserverUnit() || !(context._turretType && context._turretType->_primaryGunner))
    {
      SelectFireWeapon(context);
      if (context._weapons->_fire._gunner == context._gunner && context._weapons->_fire._weapon >= 0)
        context._weapons->SelectWeapon(this, context._weapons->_fire._weapon);
    }
    else // simplified version of decisions (use only automatic weapons, others are controlled by the commander)
    {
      // check if there is some weapon (machine gun) we can fire with
      bool someAutoFire = false;

      // when some automatic weapon (or no weapon) is selected, gunner can decide himself what to do
      int currentWeapon = context._weapons->ValidatedCurrentWeapon();
      if (currentWeapon < 0 || context._weapons->_magazineSlots[currentWeapon]._weaponMode && context._weapons->_magazineSlots[currentWeapon]._weaponMode->_autoFire)
      {
        for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
        {
          const MagazineSlot &slot = context._weapons->_magazineSlots[i];
          if (slot._weaponMode && slot._weaponMode->_autoFire && slot._magazine && slot._magazine->GetAmmo() > 0)
          {
            someAutoFire = true;
            if (context._weapons->ValidatedCurrentWeapon() < 0)
            {
              // fall back - when no weapon is selected, select the first suitable
              context._weapons->_currentWeapon = i; 
            }
            break;
          }
        }
      }

      if (someAutoFire)
      {
        // machine gun automate

        // enable weapons selection only when fully controlling weapons
        bool enableWeaponsSelection = !IsManualFire();

        // if we do not know target precision exactly enough
        if (context._weapons->_fire._fireTarget)
        {
          // target may be very old
          // target may be also very fresh - the information is still pending to the gunner
          // the current weapon also may not be suitable for the current target
          // in such case we want to set it, gunner will react once he is able to do so
          int selected = context._weapons->ValidatedCurrentWeapon();
          const AmmoType *ammo = NULL;
          if (selected >= 0)
          {
            const MagazineSlot &slot = context._weapons->_magazineSlots[selected];
            if (slot._magazine && slot._magazine->_type) ammo = slot._magazine->_type->_ammo;
          }

          if(
            //!context._weapons->_fire._fireTarget->IsKnownBySome() ||
            context._weapons->_fire.GetTargetFinished(gunnerUnit) ||
            context._weapons->_fire._fireTarget->GetLastSeen() < Glob.time-30 ||
            context._weapons->_fire._fireTarget->GetSide() == TCivilian ||
            context._weapons->_fire._fireTarget->idExact &&
            (!ammo || !context._weapons->_fire._fireTarget->idExact->LockPossible(ammo))
            )
          {
            context._weapons->_fire.SetTarget(gunnerUnit,NULL);
          }
        }

        float timeToLive = gunnerUnit->GetTimeToLive();
        FireResult result;
        if (!gunnerUnit->IsHoldingFire() &&
          (
          !context._weapons->_fire._fireTarget ||
          !context._weapons->_fire._fireTarget->IsKnownBy(gunnerUnit) ||
          WhatFireResult(result, *context._weapons->_fire._fireTarget, context, timeToLive, false, false) != FPCan // check all weapons for the commanded target
          )
          )
        {
          result.loan = FLT_MAX; // allow firing even when loan is high - better fire than wait
          // preferred target not visible - fire on anything else
          // select only few nearest enemy targets
          // select enemy target with best fire result
          //            _firePrepareOnly = true;
          const TargetList *list = gunnerUnit->AccessTargetList();
          bool enemyFound = false;
          if (list)
          {
            int maxEnemies = 4; // limit not considering FPCannot
            int maxEnemiesRelaxed = 16; // limit considering even FPCannot
            for (int i=0; i<list->EnemyCount(); i++)
            {
              TargetNormal *tgtI = list->GetEnemy(i);
              if (!tgtI->IsKnownBy(gunnerUnit)|| tgtI->destroyed || tgtI->vanished) continue;
              // we want to engage enemy targets only
              if (tgtI->State(gunnerUnit)<TargetEnemyCombat) continue;
              if (!gunnerUnit->IsEnemy(tgtI->side)) continue;
              // TODO: time argument? - max of reload time?
              FireResult tResult;
              FirePossibility fp;
              // decide whether weapon selection is possible
              if (enableWeaponsSelection) fp = WhatFireResult(tResult, *tgtI, context, timeToLive, false, true); // check only autofire weapons
              else
              {
                int weapon = context._weapons->ValidatedCurrentWeapon();
                Assert(weapon >= 0);
                fp = WhatFireResult(tResult, *tgtI, context, weapon, timeToLive); // check only the selected weapon
                tResult.weapon = weapon;
              }
              if (fp==FPCan)
              {
                if (tResult.Surplus() > result.Surplus())
                {
                  result = tResult;
                  context._weapons->_fire.SetTarget(gunnerUnit,tgtI);
                  context._weapons->_currentWeapon = tResult.weapon;
                  enemyFound = true;
                }
              }
              // if result is FPCannot, the test was very quick and we do not count it against the limit
              // this is done to avoid a few high cost, but impossible to fire at targets (like aircraft) exhausting the limit
              if( --maxEnemiesRelaxed<=0 || fp!=FPCannot && --maxEnemies<=0 )
                break;
            }
            
          }

          // if the target is explicitly ordered, fire at it even when not enemy
          if (!enemyFound && gunnerUnit->GetEnableFireTarget())
          {
            Target *tgtI = gunnerUnit->GetEnableFireTarget();
            // TODO: time argument? - max of reload time?
            FireResult tResult;
            FirePossibility fp;
            // decide whether weapon selection is possible
            if (enableWeaponsSelection) fp = WhatFireResult(tResult, *tgtI, context, timeToLive, false, true); // check only autofire weapons
            else
            {
              int weapon = context._weapons->ValidatedCurrentWeapon();
              Assert(weapon >= 0);
              fp = WhatFireResult(tResult, *tgtI, context, weapon, timeToLive); // check only the selected weapon
              tResult.weapon = weapon;
            }

            if (fp == FPCan)
            {
              if (tResult.Surplus() > result.Surplus())
              {
                result = tResult;
                context._weapons->_fire.SetTarget(gunnerUnit,tgtI);
                context._weapons->_currentWeapon = tResult.weapon;
                enemyFound = true;
              }
            }
          }

          // no target for direct fire, check suppressive fire possibility
          if (list && enableWeaponsSelection && CheckSuppressiveFire()>=SuppressYes && !enemyFound)
          {
            int maxEnemies = 4; // limit not considering FPCannot
            int maxEnemiesRelaxed = 16; // limit considering even FPCannot
            for (int i=0; i<list->EnemyCount(); i++)
            {
              TargetNormal *tgtI = list->GetEnemy(i);
              if (!tgtI->IsKnownBy(gunnerUnit) || tgtI->destroyed || tgtI->vanished) continue;
              // we want to engage enemy targets only
              if (tgtI->State(gunnerUnit)<TargetEnemyCombat) continue;
              if (!gunnerUnit->IsEnemy(tgtI->side)) continue;
              FireResult tResult;
              FirePossibility fp = WhatFireResult(tResult, *tgtI, context, timeToLive, true, true); // check only autofire weapons
              if (fp==FPCan)
              {
                if (tResult.Surplus() > result.Surplus())
                {
                  result = tResult;
                  context._weapons->_fire.SetTarget(gunnerUnit,tgtI);
                  context._weapons->_currentWeapon = tResult.weapon;
                }
              }
              // if result is FPCannot, the test was very quick and we do not count it against the limit
              // this is done to avoid a few high cost, but impossible to fire at targets (like aircraft) exhausting the limit
              if( --maxEnemiesRelaxed<=0 || fp!=FPCannot && --maxEnemies<=0 )
                break;
            }
            
          }
        }
      }
    }

    AIGunner(context, deltaT);

    if (Type()->_hideProxyInCombat)
    {
      AIBrain *commanderUnit = CommanderUnit();
      if (gunnerUnit != commanderUnit)
      {
        bool isCommanderHidden = false;
        if (commanderUnit && commanderUnit->LSIsAlive())
          isCommanderHidden = IsPersonHidden(FutureVisualState(), commanderUnit->GetPerson());
        HidePerson(context._gunner, isCommanderHidden || (context._turretType && context._turretType->_forceHideGunner));
      }
    }
  }
  // we want to trace fire sectors for both AI and players, both local and remote
  TraceFireSector(context);
#endif //_ENABLE_AI
}

void Transport::SimulateObserver(TurretContextEx &context, float deltaT, SimulationImportance prec)
{
  AIBrain *observerUnit = context._gunner->Brain();
  if (!observerUnit || !observerUnit->LSIsAlive()) return;

  bool manual = GLOB_WORLD->PlayerManual() && GLOB_WORLD->PlayerOn();
  AIBrain *player = manual ? GLOB_WORLD->PlayerOn()->Brain() : NULL;
  if (manual && (observerUnit == player || IsRemoteControlled(observerUnit)))
  {
    // observerUnit is partially realized in InGameUI
#if _VBS2 //convoy trainer
    Soldier* soldier = dyn_cast<Soldier>(context._gunner);
    if(soldier && soldier->IsPersonalItemsEnabled())
    {
      soldier->SimulateAI(deltaT,prec);
    }
    else
#endif
      KeyboardObserver(context, deltaT);
  }
  else if (observerUnit->IsLocal())
  {
    AIBrain *commanderUnit = CommanderUnit();
    if (observerUnit != commanderUnit)
    {
      bool isCommanderHidden = false;
      if (commanderUnit && commanderUnit->LSIsAlive())
        isCommanderHidden = IsPersonHidden(FutureVisualState(), commanderUnit->GetPerson());
      HidePerson(
        observerUnit->GetPerson(),
        isCommanderHidden || (context._turretType && context._turretType->_forceHideGunner)
        );
    }
  }
}

/// Functor searching for alive gunner
class FindAliveGunner : public ITurretFunc
{
protected:
  const AIBrain *&_unit; // out
public:
  FindAliveGunner(const AIBrain *&unit) : _unit(unit) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if (!context._gunner) return false; // continue

    AIBrain *unit = context._gunner->Brain();
    if (unit && unit->LSIsAlive())
    {
      _unit = unit;
      return true; // found
    }
    return false; // continue
  }
};

/// Functor reporting dead gunner
class ReportDeadGunner : public ITurretFunc
{
protected:
  const AIBrain *_reporting;
public:
  ReportDeadGunner(const AIBrain *reporting) : _reporting(reporting) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if (!context._gunner) return false; // continue

    AIBrain *unit = context._gunner->Brain();
    ReportIfDead(unit, _reporting);
    return false; // do it for all gunners
  }
};

/// Functor reporting dead gunner
class SimulateGunnerFunc : public ITurretFuncEx
{
protected:
  float _deltaT;
  SimulationImportance _prec;

public:
  SimulateGunnerFunc(float deltaT, SimulationImportance prec) : _deltaT(deltaT), _prec(prec) {}
  bool operator () (EntityAIFull *entity, TurretContextEx &context)
  {
#if _VBS3 //check if it might be overridden
    if (!context._gunner && context._turret && !context._turret->_overriddenByTurret) return false; // continue
#else
    if (!context._gunner) return false; // continue
#endif

    Transport *vehicle = static_cast<Transport *>(entity);

    if (context._weapons->_magazineSlots.Size() == 0)
    {
      if (context._turret)
        vehicle->SimulateObserver(context, _deltaT, _prec);
    }
    else
      vehicle->SimulateGunner(context, _deltaT, _prec);
    return false; // do it for all gunners
  }
};

/*!
\patch 1.33 Date 11/29/2001 by Ondra.
- Fixed: MP: Airplanes autofiring from MG or unguided missiles.
*/

void Transport::SimulateAI(float deltaT, SimulationImportance prec)
{
  PROFILE_SCOPE_EX(siATr,sim);
  bool manual = GLOB_WORLD->PlayerManual() && GLOB_WORLD->PlayerOn();
  AIBrain *player = manual ? GLOB_WORLD->PlayerOn()->Brain() : NULL;

  // Check whether some units are in _getoutUnits array in spite of being currently out
  // Note: it can happen in MP game, when AI, say gunner is simulated on different client than leader giving the GetOut command.
  for (int i=0; i<_getoutUnits.Size(); i++)
  {
    if(!_getoutUnits[i] || _getoutUnits[i]->GetVehicleIn() != this )
    {
      _getoutUnits.Delete(i);
      break; // it is sufficient to delete one such forgotten unit in one AI simulation step
    }
  }

#if _ENABLE_CHEATS
  // pretend we hava a pilot, but fo not perform any activity
  extern bool disableUnitAI;
  if( disableUnitAI ) return;
#endif

  bool isCommanderHidden = true;

  AIBrain *commanderUnit = CommanderUnit();
  AIBrain *driverUnit = PilotUnit();
  AIBrain *observerUnit = EffectiveObserverUnit();

  /**/
  const AIBrain *reportedBy = NULL;
  // find any unit that is alive and is able to report the vehicle
  if (driverUnit && driverUnit->LSIsAlive())
  {
    reportedBy = driverUnit;
  }
  FindAliveGunner funcAlive(reportedBy);
  ForEachTurret(funcAlive);
  // search in cargo
  for (int i=0; i<_manCargo.Size(); i++)
  {
    Person *person = _manCargo[i];
    if (!person) continue;
    AIBrain *unit = person->Brain();
    if (!unit || !unit->LSIsAlive()) continue;
    reportedBy = unit;
  }

  // this unit should report any dead units
  ReportDeadGunner funcDead(reportedBy);
  ForEachTurret(funcDead);
  if (ReportIfDead(driverUnit, reportedBy)) driverUnit = NULL;

  if (commanderUnit && commanderUnit->LSIsAlive())
  {
    if (manual && commanderUnit == player)
    {
    }
    else if (commanderUnit->GetPerson()->IsRemotePlayer())
    {
      // remote player - avoid fake commander commands
    }
#if _ENABLE_AI
    else if (commanderUnit->IsLocal())
    {
      AICommander(commanderUnit, deltaT);
    }
#endif

    isCommanderHidden = IsPersonHidden(FutureVisualState(), commanderUnit->GetPerson());
  }

  if (observerUnit && observerUnit->LSIsAlive())
  {
    if (manual && observerUnit == player)
    {
    }
    else if (observerUnit->GetPerson()->IsRemotePlayer())
    {
      // remote player - avoid fake commander commands
    }
#if _ENABLE_AI
    else if (observerUnit->IsLocal())
    {
      AIObserver(observerUnit, deltaT);
    }
#endif
  }

  VisualState& vs = unconst_cast(FutureVisualState());
  SimulateGunnerFunc funcSimulate(deltaT, prec);
  ForEachTurretEx(vs, funcSimulate);

  if (player && player->GetVehicleIn() == this && player->IsInCargo())
  {
#if _VBS2 //convoy trainer
    Person* person = GLOB_WORLD->PlayerOn();
    Soldier* soldier = dyn_cast<Soldier>(person);
    if(soldier && soldier->IsPersonalItemsEnabled())
    {
      soldier->SimulateAI(deltaT,prec);
    }
    else
#endif
      KeyboardLookAround(player, deltaT);
  }
  if (Type()->_hasDriver && Type()->_hideProxyInCombat)
  {
    // if driver hatch is not closed, sometimes we need to close it
    if (IsLocal() && _driverHiddenWanted<0.5f)
    {
      AIBrain *commander = CommanderUnit();
      // TODO: which gunner
      TurretContext context;
      bool valid = GetPrimaryGunnerTurret(context);
      // check if commander is hidden. If he is, hide as well
      // if there is any fire target ordered, close the has as well
      if (
        commander && PilotUnit()!=commander && IsPersonHidden(FutureVisualState(), commander->GetPerson()) ||
        (valid && context._weapons->_fire._fireTarget != NULL)
        )
      {
        HideDriver(true);
      }
    }
  }

  if (driverUnit)
  {
    if (manual && (driverUnit == player || IsRemoteControlled(driverUnit)) )
    {
#if _VBS3
      if ( GWorld->GetAllowMovementCtrlsInDlg() || !GWorld->GetPlayerSuspended() )
#else
      if( !GWorld->GetPlayerSuspended() && !IsRemoteControlling(driverUnit))
#endif
      {
        CheckAway();
        if (_inputFrameID != GInput.frameID) KeyboardPilot(driverUnit, deltaT);
        _inputFrameID = GInput.frameID;
      }
      else
      {
        SuspendedPilot(driverUnit, deltaT);
      }
    }
    // else if (driverUnit->GetPerson()->IsRemotePlayer())
    else if (!driverUnit->GetPerson()->IsLocal())
    {
      FakePilot(deltaT);
    }
#if _ENABLE_AI
    else
    {
      AIPilot(driverUnit, deltaT);
      if (driverUnit && driverUnit != CommanderUnit() && Type()->_hideProxyInCombat)
      {
        // TODO: which gunner
        TurretContext context;
        bool valid = GetPrimaryGunnerTurret(context);
        bool hideDriver = isCommanderHidden || (valid && context._weapons->_fire._fireTarget != NULL);
        HidePerson(
          driverUnit->GetPerson(),
          hideDriver || Type()->_forceHideDriver
          );
      }
    }
#endif //_ENABLE_AI
  }

  //test if some counter measures are needed
  if(_lastTestedCM < Glob.uiTime)
  {
    TurretContextV fireContext;
    VisualState& vs = FutureVisualState();
    AIBrain *observer = EffectiveObserverUnit();
    if(observer && observer->IsLocal() && !observer->IsPlayer() && observer->GetPerson() && FindTurret(vs, observer->GetPerson(),fireContext) )
    {
      //if there are no active CM already launched
      if(_activeCounterMeasures.Size()==0)
      { //try to avoid incoming missiles, random value is to avoid perfect CM launch
        if(_incomingMissiles.Size()>0 && GRandGen.RandomValue() <= 0.35f)
        {
          for (int i=0; i< _incomingMissiles.Size(); i++)
          { //AI reacts only on missiles, which it can detect
            if(_incomingMissiles[i] && ((_incomingMissiles[i]->Type()->weaponLockSystem) & GetType()->GetIncommingMissileDetectionSystem())>0) 
            {
              float distance = FutureVisualState().Position().Distance(_incomingMissiles[i]->FutureVisualState().Position());
              for (int j=0; j<fireContext._weapons->_magazineSlots.Size(); j++)      
              {//select correct fire mod
                MagazineSlot mag = fireContext._weapons->_magazineSlots[j];
                if(mag._weapon->GetSimulation() == WSCMLauncher && mag._weaponMode->minRange < distance && mag._weaponMode->maxRange > distance)
                { //if CM can affect incoming missile
                  if (mag._magazine && 
                    (mag._magazine->_type->_ammo->weaponLockSystem  & _incomingMissiles[i]->Type()->weaponLockSystem)>0)
                  {
                    fireContext._weapons->SelectCounterMeasures(this,j);
                    FireWeapon(fireContext,j, NULL, false);
                    goto fired;
                  }
                }
              }
            }
          }
        }
        else if(_targetingEnemys.Size()>0 && GRandGen.RandomValue() <= 0.03f)
        {
          TurretContextV context;
          for (int j=0; j<_targetingEnemys.Size(); j++)      
          {
            Person *gunner = _targetingEnemys[j];
            // check if enemy has acquire a lock on us
            if (!gunner || !gunner->Brain() || !gunner->Brain()->GetVehicle()) continue;
            EntityAIFull *vehicle = gunner->Brain()->GetVehicle();
            if(!vehicle->FindTurret(unconst_cast(vehicle->FutureVisualState()), gunner, context)) continue;
            int weapon = context._weapons->ValidatedCurrentWeapon();
            if (weapon < 0) continue;
            //we cannot detect unit's lock
            if((context._weapons->_magazineSlots[weapon]._weapon->_weaponLockSystem & GetType()->GetLockDetectionSystem()) == 0) 
              return;

            const WeaponType *type = context._weapons->_magazineSlots[weapon]._weapon;
            float distance = FutureVisualState().Position().Distance(vehicle->FutureVisualState().Position());

            for (int i=0; i<fireContext._weapons->_magazineSlots.Size(); i++)      
            {//select correct weapon mode
              MagazineSlot mag = fireContext._weapons->_magazineSlots[i];
              if (mag._magazine && mag._weapon->GetSimulation() == WSCMLauncher && 
                (mag._magazine->_type->_ammo->weaponLockSystem  & type->_weaponLockSystem)>0)
              {
                if(mag._weaponMode->minRange < distance && mag._weaponMode->maxRange > distance)
                {//keep some CM for incoming missile
                  if(mag._magazine->GetAmmo() > 0.5f * mag._magazine->_type->_maxAmmo)
                  {
                    fireContext._weapons->SelectCounterMeasures(this,i);
                    FireWeapon(fireContext,i, NULL, false);
                  }
                  goto fired;
                }
              }
            }
          }
        }
      }
    }
    //we don't need to test it every frame
fired:
    _lastTestedCM+=0.2f;
  }

  base::SimulateAI(deltaT,prec);
}

/// Functor hiding the gunner (if possible)
class HideGunnerFunc : public ITurretFuncV
{
protected:
  bool _hide;

public:
  HideGunnerFunc(bool hide) : _hide(hide) {}
  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (!context._turret) return false;
    if (!context._gunner) return false;  // continue
    float hidden = context._turretType->_canHideGunner && (_hide || context._turretType->_forceHideGunner) ? 1.0f : 0.0f;
    context._turretVisualState->_crewHidden = hidden;
    context._turret->_crewHiddenWanted = hidden;
    context._gunner->SwitchAction(context._turret->GunnerAction(*context._turretVisualState, *context._turretType));
    context._gunner->OnGetInFinished();
    return false; // continue
  }
};

void Transport::InitUnits()
{
  // called after vehicle is placed in landscape
  // unit are setup and initialized
  AIBrain *unit = CommanderUnit();
  if (unit && Type()->_hideProxyInCombat)
  {
    // initialize hatches - open when in aware or safe mode
    CombatMode cm = unit->GetCombatModeLowLevel();
    bool hide = cm >= CMAware;
    if (Type()->_hasDriver && Driver())
    {
      FutureVisualState()._driverHidden = _driverHiddenWanted = hide || Type()->_forceHideDriver;
      _driver->SwitchAction(DriverAction());
      _driver->OnGetInFinished();
    }
    VisualState& vs = unconst_cast(FutureVisualState());
	HideGunnerFunc func(hide);
    ForEachTurret(vs, func);
  }
  base::InitUnits();
}

bool Transport::ValidateCrew(Person *crew, bool complex) const
{
  if (!crew) return true;
  // check if crew is out
  bool ok = true;
  if (crew->IsInLandscape())
  {
    RptF
      (
      "Error: Crew %s of %s in landscape",
      (const char *)crew->GetDebugName(),
      (const char *)GetDebugName()
      );
    ok = false;
  }
  return ok;
}

/// Functor validating gunner person
class ValidateGunner : public ITurretFunc
{
protected:
  bool &_ok; // out
  bool _complex;

public:
  ValidateGunner(bool &ok, bool complex) : _ok(ok), _complex(complex) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue

    Transport *vehicle = static_cast<Transport *>(entity); // we now where we call it
    if (!vehicle->ValidateCrew(context._gunner, _complex))
    {
      RptF("Gunner invalid");
      _ok = false;
    }
    return false; // continue
  }
};

bool Transport::Validate(bool complex) const
{
  // check if all crew members are out
  bool ok = true;
  if (!ValidateCrew(_driver,complex))
  {
    RptF("Driver invalid");
    ok = false;
  }

  ValidateGunner func(ok, complex);
  ForEachTurret(func);

  for (int i=0; i<_manCargo.Size(); i++)
  {
    if (!ValidateCrew(_manCargo[i],complex))
    {
      RptF("ManCargo %d invalid",i);
      ok = false;
    }

  }
  return ok;
}

void Transport::Init( Matrix4Par pos, bool init )
{
  if (!EngineCanBeOff())
  {
    _engineOff = false;
  }
  base::Init(pos, init);
}

class GunnerTrackTargets : public ITurretFunc
{
protected:
  bool _initialize;
  Transport *_vehicle;
  TargetList &_res;
  float *_trackTargetsPeriod;

public:
  GunnerTrackTargets(Transport *vehicle, TargetList &res, bool initialize, float trackTargetsPeriod[NTgtCategory]): _vehicle(vehicle),_initialize(initialize), _res(res)
  {
    _trackTargetsPeriod = trackTargetsPeriod;
  }
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turretType && context._gunner && context._gunner->Brain())
    {
      _vehicle->EntityAIFull::TrackTargets(
        _res, context._gunner->Brain(), _vehicle->GetType()->_gunnerCanSee, _initialize, 1e10,
        _trackTargetsPeriod
        );
    }
    return false;
  }
};

void Transport::TrackTargets(
                             TargetList &res, bool initialize, float trackTargetsPeriod[NTgtCategory]
)
{
  const EntityAIType *type = GetType();
  AIBrain *unit=ObserverBrain();
  if (unit) base::TrackTargets(
    res,unit,type->_commanderCanSee,initialize,1e10,
    trackTargetsPeriod
    );

  GunnerTrackTargets func(this, res, initialize, trackTargetsPeriod);
  ForEachTurret(func);

  //unit=GunnerBrain();
  //if (unit) base::TrackTargets(
  //  res,unit,type->_gunnerCanSee,initialize,1e10,
  //  trackTargetsPeriod
  //  );

  // all units may have driver
  unit=DriverBrain();
  if (unit) base::TrackTargets(
    res,unit,type->_driverCanSee,initialize,1e10,
    trackTargetsPeriod
    );
}

class GunnerTrackOneTarget : public ITurretFunc
{
protected:
  Transport *_vehicle;
  float _trackTargetsPeriod;
  float _maxDist;
  TargetList &_res;
  Target *_target;

public:
  GunnerTrackOneTarget(Transport *vehicle, TargetList &res, float trackTargetsPeriod, float maxDist, Target *target): 
      _vehicle(vehicle),
      _trackTargetsPeriod(trackTargetsPeriod), 
      _maxDist(maxDist), 
      _res(res),
      _target(target)
  {
  }
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turretType && context._gunner && context._gunner->Brain())
    {
        TargetNormal::TrackContext *ctx = _vehicle->InitTrackContext(_res,context._gunner->Brain(), _vehicle->GetType()->_gunnerCanSee,false,_maxDist);
        if (ctx)
        {
          _vehicle->EntityAIFull::TrackOneTarget(_target,context._gunner->Brain(),_trackTargetsPeriod,_maxDist,*ctx);
        }
    }
    return false;
  }
};

void Transport::TrackOneTarget(TargetList &res, Target *target, float trackTargetsPeriod)
{
  const EntityAIType *type = GetType();
  float maxDist = TACTICAL_VISIBILITY;
  AIBrain *unit=ObserverBrain();
  if (unit)
  {
    TargetNormal::TrackContext *ctx = InitTrackContext(res,unit,type->_commanderCanSee,false,maxDist);
    if (ctx)
    {
      base::TrackOneTarget(target,unit,trackTargetsPeriod,maxDist,*ctx);
    }
  }

  GunnerTrackOneTarget func(this, res, trackTargetsPeriod, maxDist, target);
  ForEachTurret(func);


  // all units may have driver
  unit=DriverBrain();
  if (unit)
  {
    TargetNormal::TrackContext *ctx = InitTrackContext(res,unit,type->_driverCanSee,false,maxDist);
    if (ctx)
    {
      base::TrackOneTarget(target,unit,trackTargetsPeriod,maxDist,*ctx);
    }
  }
}

Vector3 Transport::GetCameraDirection( CameraType camType ) const
{
  VisualState& vs = unconst_cast(RenderVisualState());
  // used for external camera
  AIBrain *unit = GWorld->FocusOn();
  Person *man = unit ? unit->GetPerson() : NULL;

  // first check if person is hidden
  if (man)
  {
    // check proxy direction
    Matrix4 transf;
    Vector3 proxyEyeDir = VForward;
    if (GetProxyCamera(transf, camType))
    { //looking around for 3rd person view (using head direction)
      proxyEyeDir = man->GetCrewLookDirection(man->RenderVisualState());
    }
    if (man == _driver)
    {
      if (vs.IsDriverHidden()) return vs.Transform().Rotate(proxyEyeDir);
    }
    else
    {
      TurretContextV context;
      if (FindTurret(vs, man, context))
      {
        if (
          (context._turret && (context._turretVisualState->IsGunnerHidden() || context._turretType->OutGunnerMayFire(context._gunner))) ||
          !Type()->_hideProxyInCombat)
        {
          transf.SetUpAndDirection(VUp, proxyEyeDir); //combine head direction with weapon/turret
          int weapon = context._weapons->ValidatedCurrentWeapon();
          if (weapon >= 0)
            return transf.Rotate(GetWeaponDirection(vs, context, weapon));
          else
            return transf.Rotate(GetLookAroundDirection(vs, context));
        }
      }
    }
  }

  // check proxy direction
  Matrix4 transf;
  if (GetProxyCamera(transf, camType))
  {
    if (man /*&& !QIsManual(unit)*/)
    {
      Vector3 eyeDir = man->GetCrewLookDirection(man->RenderVisualState());
      return vs.DirectionModelToWorld(transf.Rotate(eyeDir));
    }
    return vs.DirectionModelToWorld(transf.Direction());
  }

  // failed
  return vs.Direction();
}

Matrix4 Transport::TurretTransform(const TurretType &type) const
{
  Matrix4 mat = MIdentity;
  int memory = GetShape()->FindMemoryLevel();
  if (memory >= 0)
  {
    SkeletonIndex bone = type.GetBodyBone(Type(), memory);
    AnimateBoneMatrix(mat, FutureVisualState(), memory, bone);
  }
  return mat;
}

Matrix4 Transport::GunTurretTransform(ObjectVisualState const& vs, const TurretType &type) const
{
  Matrix4 mat = MIdentity;
  int memory = GetShape()->FindMemoryLevel();
  if (memory >= 0)
  {
    SkeletonIndex bone = type.GetGunBone(Type(), memory);
    AnimateBoneMatrix(mat, vs, memory, bone);
  }
  return mat;
}

Vector3 Transport::GetEyeDirection(ObjectVisualState const& vs, const TurretContext &context) const
{
  AIBrain *unit = EffectiveObserverUnit();
  if (!unit || unit!=PilotUnit() || QIsManual(unit))
    return base::GetEyeDirection(vs, context);
  Person *person = unit->GetPerson();
  if (!person)
    return base::GetEyeDirection(vs, context);
  Vector3 dir = person->GetCrewHeadDirection();
  // TODO: convert from the proxy space
  return vs.DirectionModelToWorld(dir);
}

Vector3 Transport::GetEyeDirectionWanted(ObjectVisualState const& vs, const TurretContextEx &context) const
{
  AIBrain *unit = EffectiveObserverUnit();
  if (!unit || unit!=PilotUnit() || QIsManual(unit))
    return base::GetEyeDirectionWanted(vs, context);
  Person *person = unit->GetPerson();
  if (!person)
    return base::GetEyeDirectionWanted(vs, context);
  Vector3 dir = person->GetCrewHeadDirectionWanted();
  // TODO: convert from the proxy space
  return vs.DirectionModelToWorld(dir);
}

Vector3 Transport::GetLookAroundDirection(ObjectVisualState const& vs, const TurretContext &context) const
{
  AIBrain *unit = EffectiveObserverUnit();
  if (!unit || unit!=PilotUnit() || QIsManual(unit))
    return base::GetLookAroundDirection(vs, context);
  Person *person = unit->GetPerson();
  if (!person)
    return base::GetLookAroundDirection(vs, context);
  Vector3 dir = person->GetCrewLookDirection(person->GetVisualStateByAge(GetVisualStateAge(vs)));
  // TODO: convert from the proxy space
  return vs.DirectionModelToWorld(dir);
}

Matrix4 Transport::InsideCamera( CameraType camType ) const
{
  Matrix4 transf;
  if (camType == CamGunner && GetOpticsCamera(transf, camType))
    return transf;
  if (GetProxyCamera(transf, camType))
    return transf;
  // no proxy: return vehicle position
  return MIdentity;
}


Matrix4 Transport::InternalCameraTransform(Matrix4 &base, CameraType camType) const
{
  // perform calculations in relative space to avoid world space rounding errors
  const VisualState &vs = RenderVisualStateScope();
  base = vs.Transform();
  Matrix4 transf = InsideCamera(camType);

  AIBrain *unit = GWorld->FocusOn();
  Person *man = unit ? unit->GetPerson() : NULL;

  // first check if person is hidden
  if (man)
  {
    if (man == _driver)
    {
      if (vs.IsDriverHidden())
        transf.SetOrientation(M3Identity);
    }
    else
    {
#if _VBS2
      //convoy trainer, is this used at all?
      if(man->IsNetworkPlayer() && man->IsPersonalItemsEnabled())
      {
        TurretContextV contextDummy;
        transf.SetDirectionAndUp(vs.DirectionWorldToModel(man->GetEyeDirectionWanted(contextDummy)), VUp);
        return transf;
      }
#endif
      TurretContextV context;
      if (FindTurret(unconst_cast(vs), man, context))
      {
        if (
          (context._turret && (context._turretType->GunnerMayFire(context))) ||
          !Type()->_hideProxyInCombat)
        {
          int weapon = context._weapons->ValidatedCurrentWeapon();
          // TODO: calculate GetWeaponDirection/GetEyeDirection in model space
          if (weapon >= 0)
            transf.SetDirectionAndUp(vs.DirectionWorldToModel(GetWeaponDirection(vs, context, weapon)), VUp);
          else
            transf.SetDirectionAndUp(vs.DirectionWorldToModel(GetLookAroundDirection(vs, context)), VUp);
        }
      }
    }
  }

  if (man)
  {
    // we want to change the view direction based on the crew eye direction
    Vector3 eyeDir = transf.Rotate(man->GetCrewLookDirection(man->GetVisualStateByAge(GetVisualStateAge(vs))));
    Matrix3 rot(MDirection,eyeDir,VUp);

#if _VBS3
    if(man == _driver)
    {
      float heading,dive,fov;
      Type()->_viewPilot.InitVirtual(camType,heading,dive,fov);

      Matrix3 mdive(MRotationX,-dive);
      Matrix3 mheading(MRotationY,heading);

      rot = rot*(mheading*mdive);
    }
#endif
    transf.SetOrientation(rot);
  }

  // add camera shake
  GWorld->GetCameraShakeManager().UpdateCameraTransform(transf);
  return transf;
}

int Transport::InternalLOD(UIActionType position) const
{
  int index = -1;
  switch (position)
  {
  case ATGetInCommander:
    {
      TurretContext context;
      if (GetPrimaryObserverTurret(context) && (!context._turretType || context._turretType->_gunnerUsesPilotView))
        index = GetShape()->FindSpecLevel(VIEW_PILOT);
      else
        index = GetShape()->FindSpecLevel(VIEW_CARGO);
    }
    if (index < 0) index = GetShape()->FindSpecLevel(VIEW_GUNNER);
    break;
  case ATGetInGunner:
    index = GetShape()->FindSpecLevel(VIEW_GUNNER);
    if (index < 0)
    {
      // check if we should use gunner or cargo view
      TurretContext context;
      if (GetPrimaryGunnerTurret(context) && (!context._turretType || context._turretType->_gunnerUsesPilotView))
      {
        index = GetShape()->FindSpecLevel(VIEW_PILOT);
        if (index < 0) index = GetShape()->FindSpecLevel(VIEW_CARGO);
      }
      else
      {
        index = GetShape()->FindSpecLevel(VIEW_CARGO);
        if (index < 0) index = GetShape()->FindSpecLevel(VIEW_PILOT);
      }
    }
    break;
  case ATGetInDriver:
  case ATGetInPilot:
    index = GetShape()->FindSpecLevel(VIEW_PILOT);
    if (index < 0) index = GetShape()->FindSpecLevel(VIEW_CARGO);
    break;
  case ATGetInCargo:
    index = GetShape()->FindSpecLevel(VIEW_CARGO);
    if (index < 0) index = GetShape()->FindSpecLevel(VIEW_PILOT);
    break;
  }
  return index;
}

int Transport::InsideLOD(CameraType camType) const
{
  AIBrain *unit = GWorld->FocusOn();
  if (!unit) return 0;
  Person *player = unit->GetPerson();
  if (!player) return 0;

  VisualState& vs = unconst_cast(RenderVisualState());

// moved to ARMA2
//#if _VBS3 //select LOD based on turret entry (if available)
  TurretContextV context;
  if (FindTurret(vs, player, context) && context._turret && context._turretType)
  {
    int index = -1;
    if(context._turretVisualState->IsGunnerHidden())
      index = context._turretType->_lodTurnedIn >=0 ? GetShape()->FindSpecLevel(context._turretType->_lodTurnedIn) : -1;
    else
      index = context._turretType->_lodTurnedOut >= 0 ? GetShape()->FindSpecLevel(context._turretType->_lodTurnedOut) : -1;

    if(index >= 0)
      return index;
  }
// moved to ARMA2
//#endif

  // view for out positions
  if (Type()->_hideProxyInCombat)
  {
    if (player == Driver())
    {
      if (!vs.IsDriverHidden()) return 0;
    }
    else
    {
      TurretContextV context; 
      if (FindTurret(vs, player, context) && context._turret)
      {
        if (!context._turretVisualState->IsGunnerHidden()) return 0;
      }
    }
  }

  // gunner camera
  if (camType == CamGunner)
  {
    int index = GetShape()->FindSpecLevel(VIEW_GUNNER);
    if (index >= 0) return index;
    return 0;
  }

  // internal camera
  if (player == Gunner())
    return InternalLOD(ATGetInGunner);
  else if (player == Observer())
    return InternalLOD(ATGetInCommander);
  else if (player == Driver())
    return InternalLOD(ATGetInDriver);
  else
    return InternalLOD(ATGetInCargo);
}

bool Transport::IsVirtual(CameraType camType) const
{
  if (camType == CamGunner)
  {
    AIBrain *unit = GWorld->FocusOn();
    Person *player = unit ? unit->GetPerson() : NULL;
    if (player && player == Gunner())
      return false;
  }
  return true;
}

bool Transport::IsVirtualX(CameraType camType) const
{
  AIBrain *unit = GWorld->FocusOn();
  Person *player = unit ? unit->GetPerson() : NULL;
  if (player)
  {
    if (player==Driver())
    {
      if (GWorld->LookAroundEnabled())
        return true;
      return camType!=CamInternal && camType!=CamExternal;
    }
  }
  return true;
}

bool Transport::IsGunner(CameraType camType) const
{
  // check unit position
  AIBrain *unit = GWorld->FocusOn();
  if (unit && unit==PilotUnit())
    return base::IsGunner(camType);
  if (camType == CamInternal)
  {
    if (!Type()->_hideProxyInCombat)
      return true;
    TurretContext context;
    if (unit && FindTurret(unit->GetPerson(), context) && context._turretType)
    {
      if (context._turretType->OutGunnerMayFire(context._gunner))
        return true;
    }
  }
  return camType==CamGunner || camType==CamExternal;
}

void Transport::LimitVirtual(CameraType camType, float &heading, float &dive, float &fov) const
{
  if (camType==CamInternal || camType==CamExternal)
  {
    // internal camera - default processing
    AIBrain *unit = GWorld->FocusOn();
    Person *person = unit ? unit->GetPerson() : NULL;
    if (person)
    {
      VisualState const& vs = RenderVisualState();

      TurretContextV context;
      if (person == _driver)
        Type()->_viewPilot.LimitVirtual(camType,heading,dive,fov);
      else if (FindTurret(unconst_cast(vs), person, context) && context._turret)
      {
        int opticsMode = (context._turret)?context._turret->_currentViewGunnerMode : 0;
        context._turretType->_viewGunner[opticsMode].LimitVirtual(camType, heading, dive, fov);
        if (context._turretVisualState->GetGunnerHidden() < 0.5 && context._turretType->OutGunnerMayFire(context._gunner))
        {
          heading = 0;
          dive = 0;
        }
      }
      else
        Type()->_viewCargo.LimitVirtual(camType,heading,dive,fov);
      return;
    }
  }
  else if (camType == CamGunner)
    Type()->_viewOptics.LimitVirtual(camType,heading,dive,fov);

  base::LimitVirtual(camType,heading,dive,fov);
}

/*!
\patch_internal 1.27 Date 10/10/2001 by Jirka
- Changed: optics fov, heading and dive limits and initials moved from program into config
\patch_internal 1.28 Date 10/26/2001 by Jirka
- Fixed: initial parameters for optics was replaced by base::InitVirtual
*/
void Transport::InitVirtual(CameraType camType, float &heading, float &dive, float &fov) const
{
  AIBrain *unit = GWorld->FocusOn();
  // driver or not internal - default processing
  Person *person = unit ? unit->GetPerson() : NULL;
  if (camType == CamGunner)
    Type()->_viewOptics.InitVirtual(camType,heading,dive,fov);
  else if (person)
  {
    TurretContext context;
    if (person==_driver)
      Type()->_viewPilot.InitVirtual(camType,heading,dive,fov);
    else if (FindTurret(person, context) && context._turret)
    { 
      int opticsMode = (context._turret)?context._turret->_currentViewGunnerMode : 0;
      context._turretType->_viewGunner[opticsMode].InitVirtual(camType, heading, dive, fov);
    }
    else
      Type()->_viewCargo.InitVirtual(camType,heading,dive,fov);
  }
  else
    // driver or not internal - default processing
    base::InitVirtual(camType,heading,dive,fov);
}

void Transport::OverrideCursor(Vector3 &dir) const
{
  AIBrain *unit = GWorld->FocusOn();
  Person *person = unit ? unit->GetPerson() : NULL;
  if (person)
  {
    TurretContextV context;
    VisualState& vs = unconst_cast(RenderVisualState());
    if (FindTurret(vs, person, context) && !IsGunnerLookingAround(context))
    {
      int weapon = context._weapons->ValidatedCurrentWeapon();
      if (weapon >= 0)
      {
        dir = GetWeaponDirection(vs, context, weapon);
        return;
      }
    }
    if (GWorld->LookAroundEnabled())
    { //freelook
      //return the center of the screen
      const FrameBase *cam = GScene->GetCamera();
      if (cam)
      {
        dir = cam->Direction();
        return;
      }
      else
        dir = vs.Direction();
    }
  }
  // driver or not internal - default processing
  base::OverrideCursor(dir);
}

bool Transport::ConsumeFuel(float ammount)
{
#if _VBS3
  if(Glob.config.IsEnabled(DTUnlimitedFuel)
#if _EXT_CTRL  //no fuel consumption
    || IsExternalControlled()
#endif
    ) //ext. Controller like HLA, AAR
    return true;
#endif
  bool wasFuel = FutureVisualState()._fuel>0;
  FutureVisualState()._fuel -= ammount;
  bool isFuel = FutureVisualState()._fuel>0;
  if (isFuel!=wasFuel) OnEvent(EEFuel,isFuel);
  saturate(FutureVisualState()._fuel,0,GetType()->GetFuelCapacity());
  return isFuel;
}

void Transport::Refuel(float ammount)
{
  ConsumeFuel(-ammount);
}

/*!
\patch 5135 Date 2/27/2007 by Jirka
- Fixed: AI deprecated to get in heavy damaged vehicle even when repaired again
*/

void Transport::Repair(float amount)
{
  base::Repair(amount);
  if (!EjectNeeded()) _getOutAfterDamage = TIME_MAX; // never get out
}

void Transport::StabilizeTurrets(Matrix3Val oldTrans, Matrix3Val newTrans)
{
  for (int i=0; i<_turrets.Size(); i++)
  {
    TurretType *turretType = Type()->_turrets[i];
    Turret *turret = _turrets[i];
    TurretVisualState *turretVisualState = FutureVisualState()._turretVisualStates[i];
    turret->Stabilize(*turretVisualState, this, *turretType, oldTrans, newTrans);
  }
}

/// Functor executing the basic simulation of gunner
class BasicGunnerSimulation : public ITurretFunc
{
protected:
  float _deltaT;
  SimulationImportance _prec;
#if _VBS2
  bool _simulatePost;
  Matrix3 _diffFrame;
#endif


public:
  BasicGunnerSimulation(float deltaT, SimulationImportance prec)
  : _deltaT(deltaT), _prec(prec)
  {
#if _VBS2
    _simulatePost = false;
#endif
  }
#if _VBS3
  BasicGunnerSimulation(float deltaT, SimulationImportance prec,bool simulatepost,Matrix3 diffFrame)
  : _deltaT(deltaT), _prec(prec), _simulatePost(simulatepost), _diffFrame(diffFrame)
  {}
#endif
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turret && context._gunner)
    {
      float animSpeed = context._turret ? context._turret->GunnerAnimSpeed() : 1.0f;
#if _VBS3 //convoy
      bool personalItems = context._gunner->IsPersonalItemsEnabled();
      if(personalItems && _simulatePost)
      {
        Person *man = context._gunner;
        if(!man)
          return false;

        Matrix4 newManPos = *man;
        Matrix3 newOrien = man->Orientation()*_diffFrame;

        if(man->Brain()&&man->Brain()->GetVehicleIn())
        {
          Transport* transport = man->Brain()->GetVehicleIn();
          Matrix4 transf = transport->ProxyWorldTransform(man);
          newManPos.SetPosition(transf.Position()); //move to proxy position

          newOrien.SetUpAndDirection(man->Brain()->GetVehicleIn()->Orientation().DirectionUp(),newOrien.Direction());
          newManPos.SetOrientation(newOrien);
        }
        newManPos.Orthogonalize();

        FrameBase frame(newManPos);

        man->Move(frame);
        man->Simulate(_deltaT,_prec);
      }
      else if(personalItems && !_simulatePost)
      {
        return false;
      }
      else
#endif
      {
        context._gunner->BasicSimulation(_deltaT, _prec, animSpeed);
      }
    }
    return false; // continue
  }
};

#if _VBS3 // convoy trainer
void Transport::SimulatePost(float deltaT, SimulationImportance prec)
{
  // calculcate the difference between this orientation and the last
  Matrix3 diff = _postFrame.Orientation().InverseRotation()*Orientation();

  BasicGunnerSimulation func(deltaT, prec,true,diff);
  ForEachTurret(func);

  for (int i=0; i<_manCargo.Size(); i++)
  {
    ManCargoItem &item = _manCargo.Set(i);
    Person *man = item.man;
    if (man && man->IsPersonalItemsEnabled())
    {
      Matrix4 newManPos = *man;
      Matrix3 newOrien = man->Orientation()*diff;

      if(man->Brain()&&man->Brain()->GetVehicleIn())
      {
        Transport* transport = man->Brain()->GetVehicleIn();
        Matrix4 transf = transport->ProxyWorldTransform(man);
        newManPos.SetPosition(transf.Position()); //move to proxy position

        newOrien.SetUpAndDirection(man->Brain()->GetVehicleIn()->Orientation().DirectionUp(),newOrien.Direction());
        newManPos.SetOrientation(newOrien);
      }
      newManPos.Orthogonalize();

      FrameBase frame(newManPos);
      man->Move(frame);

      man->SetSpeed(Speed());
      man->Simulate(deltaT,prec);
    }
  }
  base::SimulatePost(deltaT,prec);
}
#endif

class GunnerShakeEffect : public ITurretFunc
{
protected:
  Vector3 _force;
  float _deltaT;
  Transport *_vehicle;
public:
  GunnerShakeEffect(float deltaT,Transport *vehicle, Vector3 force): _deltaT(deltaT), _force(force),_vehicle(vehicle) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turretType && context._gunner)
    {
      Matrix4 turretTrans = _vehicle->TurretTransform(*context._turretType);
      if (turretTrans.Scale()>0)
      {
        context._gunner->RecalcShakeTransform(turretTrans.InverseRotation() * _force,_deltaT);
      }
    }
    return false;
  }
};

void Transport::Simulate(float deltaT, SimulationImportance prec)
{
  // turn in / turn out
  const float turnInOutSpeed = 1.0;
  if (_driverHiddenWanted > FutureVisualState()._driverHidden)
  {
    FutureVisualState()._driverHidden += turnInOutSpeed * deltaT;
    saturateMin(FutureVisualState()._driverHidden, _driverHiddenWanted);
    if (_driver)
      _driver->SwitchAction(DriverAction());
  }
  else if (_driverHiddenWanted < FutureVisualState()._driverHidden)
  {
    FutureVisualState()._driverHidden -= turnInOutSpeed * deltaT;
    saturateMax(FutureVisualState()._driverHidden, _driverHiddenWanted);
    if (_driver)
      _driver->SwitchAction(DriverAction());
  }

  // advance animation of proxy objects
  if (_driver)
  {
    _driver->BasicSimulation(deltaT, prec, DriverAnimSpeed());
    _driver->RecalcShakeTransform(DirectionWorldToModel(FutureVisualState()._acceleration+_impulseForce),deltaT);
  }
  BasicGunnerSimulation func(deltaT, prec);
  ForEachTurret(func);

  GunnerShakeEffect funcShake(deltaT,this, DirectionWorldToModel(FutureVisualState()._acceleration+_impulseForce));
  ForEachTurret(funcShake);
  for (int i=0; i<_manCargo.Size(); i++)
  {
    ManCargoItem &item = _manCargo.Set(i);
    Person *man = item.man;
    if (man)
    {
#if _VBS3  // convoy trainer
      // When personalized items, don't do cargo simulation
      //! used instead in postprocessing
      if(man->IsPersonalItemsEnabled()) continue;
#endif
      man->BasicSimulation(deltaT, prec, CargoAnimSpeed(i));
      man->RecalcShakeTransform(DirectionWorldToModel(FutureVisualState()._acceleration+_impulseForce),deltaT);
    }
  }

  bool moved = false;

  Assert(FutureVisualState()._turretVisualStates.Size() == _turrets.Size());
  for (int i=0; i<_turrets.Size(); i++)
  {
    TurretType *turretType = Type()->_turrets[i];
    Turret *turret = _turrets[i];
    TurretVisualState *turretVisualState = FutureVisualState()._turretVisualStates[i];
    if (turret->Simulate(*turretVisualState, this, *turretType, deltaT, prec, FutureVisualState().Orientation()))
      moved = true;
  }
  if (moved)
  {
    CancelStop();
    EngineOn();
  }
  if (_isDestroyed && _destroyMaxHeatTime > Glob.time)
  {
    float dt = 1.0f - ((_destroyMaxHeatTime - Glob.time) / MaxDamageHeatUpTime);
    _engineTemperatureCoef = dt * 0.6f;
  }
  // Simulate the engine temperature (alive factor)
  if (_engineOff)
  {
    static float hoursToCoolDown = 0.5f; // 1 hour to cool down
    const float invSecToCoolDown = 1.0f / (hoursToCoolDown * 60.0f * 60.0f);
    _engineTemperatureCoef -= deltaT * invSecToCoolDown;
  }
  else
  {
    static float hoursToHeatUp = 10.0f / 60.0f; // 10 minutes to heat up
    const float invSecToHeatUp = 1.0f / (hoursToHeatUp * 60.0f * 60.0f);
    _engineTemperatureCoef += deltaT * invSecToHeatUp;
  }
  saturate(_engineTemperatureCoef, 0.0f, 1.0f);

  // Simulate the wheel temperature (movement factor)
  const float minSpeedToHeatUp = 1.0f; // Speed in m/s
  if (FutureVisualState().GetSpeedHorizontal() < minSpeedToHeatUp)
  {
    static float hoursToCoolDown = 10.0f / 60.0f; // 10 minutes to cool down
    const float invSecToCoolDown = 1.0f / (hoursToCoolDown * 60.0f * 60.0f);
    _wheelTemperatureCoef -= deltaT * invSecToCoolDown;
  }
  else
  {
    static float hoursToHeatUp = 2.0f / 60.0f; // 2 minutes to heat up
    const float invSecToHeatUp = 1.0f / (hoursToHeatUp * 60.0f * 60.0f);
    _wheelTemperatureCoef += deltaT * invSecToHeatUp;
  }
  saturate(_wheelTemperatureCoef, 0.0f, 1.0f);

  // Simulate (main) turret gun heat factor
  TurretContext context;
  if (GetPrimaryGunnerTurret(context))
  {
    if (context._turret)
      _turretGunTemperatureCoef = context._turret->_heatFactor;
  }

  _radio.Simulate(deltaT,this);
  base::Simulate(deltaT,prec);
  ReactToDamage();

  // markers on / off
  if (!CommanderUnit() || CommanderUnit()->IsInCargo() || !CommanderUnit()->LSCanProcessActions())
  {
    // empty vehicle - leave light status as it is
    // _pilotLight = false;
  }
  else if (CommanderUnit()->HasAI()
#if _VBS2 //don't override lights if the driver is human
    && (!PilotUnit() || PilotUnit()->HasAI())
#endif
    )
  {
    // vehicle with AI driver
#if _VBS3 && _EXT_CTRL
    // leave lights alone when replaying
    if(!GAAR.IsPlaying())
#endif
    bool pilotLightBak = _pilotLight;
    _pilotLight = LightsNeeded();
    float thold = _randomizer * 0.6 + 0.2;
    if (GScene->MainLight()->NightEffect() > thold)
    {
      if (!IsCautiousOrDanger())
      {
        //LogF("%s: %.0f on",(const char *)GetDebugName(),Glob.time-Time(0));
#if _VBS3 && _EXT_CTRL
        // leave lights alone when replaying
        if(!GAAR.IsPlaying())
#endif
          _pilotLight = true;
      }
      else
      {
        //LogF("%s: %.0f off",(const char *)GetDebugName(),Glob.time-Time(0));
#if _VBS3 && _EXT_CTRL
        Soldier* soldier = dyn_cast<Soldier>(Driver());
        if(soldier && soldier->IsWearingNVG())
        {
#if _VBS3
          // leave lights alone when replaying
          if(!GAAR.IsPlaying())
#endif
            _pilotLight = false;
        }
#endif
      }
    }
    if ( _pilotLight!=pilotLightBak && !IsLocal() )
    {
      GetNetworkManager().AskForPilotLight(this, _pilotLight);
    }
  }

  if (Glob.time > _showDmgValid)
    _showDmg = false;

  _locked = LdSFree;
  for (int i = _targetingEnemys.Size()-1; i>=0; i--)
  {//delete invalid vehicles which are trying to lock _this
    if(!_targetingEnemys[i] || ! _targetingEnemys[i]->CommanderUnit() || !_targetingEnemys[i]->CommanderUnit()->GetVehicle()
      || _targetingEnemys[i]->CommanderUnit()->GetLifeState() > LifeStateAlive) _targetingEnemys.DeleteAt(i);
    else 
    {
      EntityAIFull *entity = _targetingEnemys[i]->CommanderUnit()->GetVehicle();
      TurretContextV context;
      if(!entity->FindTurret(unconst_cast(entity->FutureVisualState()), _targetingEnemys[i], context)) _targetingEnemys.DeleteAt(i);
      else 
      {
        int weapon = context._weapons->ValidatedCurrentWeapon();
        if (weapon >= 0)
        {
          const WeaponType *type = context._weapons->_magazineSlots[weapon]._weapon;
          if(context._weapons->_targetAimed > 0.1 && (GetType()->GetLockDetectionSystem() & type->_weaponLockSystem) > 0)
            _locked = LdSWeaponLocked;
        }
        else _targetingEnemys.DeleteAt(i);
      }
    } 
  }

  for (int i = _incomingMissiles.Size()-1; i>=0; i--)
  {//delete old incoming missiles or those which missed
    if(!_incomingMissiles[i] || _incomingMissiles[i]->GetTimeToLive()<=0 || _incomingMissiles[i]->IsMarkedToDelete())
      _incomingMissiles.DeleteAt(i);
    else if(_incomingMissiles[i]->FutureVisualState().Direction().DotProduct(this->FutureVisualState().Position() - _incomingMissiles[i]->FutureVisualState().Position())<= 0)
      _incomingMissiles.DeleteAt(i);
    else if(((_incomingMissiles[i]->Type()->weaponLockSystem) & this->GetType()->GetIncommingMissileDetectionSystem())>0)
      _locked = LdSIncomigMissile;
  }

  for (int i = _activeCounterMeasures.Size()-1; i>=0; i--)
  {//delete old CM
    if(!_activeCounterMeasures[i] || _activeCounterMeasures[i]->GetTimeToLive()<=0 || _activeCounterMeasures[i]->IsMarkedToDelete())
      _activeCounterMeasures.DeleteAt(i);
    else  if(_activeCounterMeasures[i]->Type()->maxControlRange > 0 &&  _activeCounterMeasures[i]->FutureVisualState().Position().Distance(FutureVisualState().Position()) > _activeCounterMeasures[i]->Type()->maxControlRange )
      _activeCounterMeasures.DeleteAt(i);
      }

  /// camera shake
  if (!_engineOff)
      {
    if (Glob.time < _lastCamShakeTime || (Glob.time - _lastCamShakeTime) > CameraShakeManager::DefaultPassingVehicleUpdateTime)
      {
      GWorld->GetCameraShakeManager().AddSource(CameraShakeParams(CameraShakeSource::CSVehicle, FutureVisualState().Position(), 0, this));
      _lastCamShakeTime = Glob.time;
      }
    }

  //check if driver is alive
  if (_driver)
  {
    AIBrain *unit = _driver->Brain();
    if (!unit)
    {
      // dead driver found and his death is known - we will try to find replacement
      if (!_driverAssigned)
      {
        if (_assignDriverAfterDamage>Glob.time+15)
          _assignDriverAfterDamage = Glob.time+GRandGen.Gauss(1.5,3,7);
      }
    }
  }

  //driver is dead, try to assign new
  if(Glob.time>_assignDriverAfterDamage)
    AssignCargoToDriver();

}

bool Transport::HasTiOptics() const
{
  TurretContext context;
  if (GetPrimaryGunnerTurret(context) && context._turret)
  {
    return context._turret->HasThermalModes(context._turretType);
  }

  return false;
}

bool Transport::NextVisionMode()
{
  const ViewPars &vPars = Type()->_viewOptics;

  if (vPars.HasVisionModes())
    {
    vPars.NextVisionMode(_visionMode, _tiIndex);
    return true;
  }

  return false;
}

bool Transport::HasVisionModes() const
{
  return (Type()->_viewOptics._visionModes.Size() != 0);
}

int Transport::VisionModesCount() const
{
  return (Type()->_viewOptics._visionModes.Size());
}

void Transport::EnableVisionModes(bool val)
{
  if (IsLocal()) 
    _enableVisionMode = val;
  else
  {
    GetNetworkManager().AskForEnableVisionModes(this, val);
  }
}

void Transport::SetAllowCrewInImmobile(bool allow) 
{
  if (IsLocal()) 
    _allowCrewInImmobile = allow;
  else
  {
    GetNetworkManager().AskForAllowCrewInImmobile(this, allow);
  }
}


VisionMode Transport::GetVisionModePars(int &flirIndex)
{
  flirIndex = _tiIndex;
  return _visionMode;
}

bool Transport::GetPostProcessEffects(const TurretContextV &context, AutoArray<PPEffectType> **ppEffects) const
{
  // if person is a driver
  if (context._gunner == Driver())
      {
    if (!Type()->_hideProxyInCombat) return false;

    if ((GWorld->GetCameraType() != CamExternal) && RenderVisualState().IsDriverHidden()) 
    {
      *ppEffects = const_cast< AutoArray<PPEffectType> * >(&Type()->_opticsType);
      
      return (Type()->_opticsType.Size() > 0);
    }
  }
  else
  {
    // when _viewGunnerInExternal is set, we assume gunner is actually outside
    bool isInside = (GWorld->GetCameraType() == CamGunner) && context._turret && (!Type()->_hideProxyInCombat || context._turretVisualState->IsGunnerHidden()) && !context._turretType->_viewGunnerInExternal;
   
    if (context._turretType)
    {
      const AutoArray<PPEffectType> &tmp = isInside ? context._turretType->_opticsType : context._turretType->_opticsOutType;
      *ppEffects = const_cast< AutoArray<PPEffectType> * > (&tmp);

      return (tmp.Size() > 0);
    }
  }

  return false;
}

#if _ENABLE_CHEATS
# if !_RELEASE
#   define ARROWS 0
# else
#   define ARROWS 0
# endif
#endif

// Simplified interaction of persons with cars. (Persons are not physicals objects, they must be simulated in a simplyfied way.
void Transport::ObjectCollisionWithPerson(CollisionBuffer objColl, unsigned int fromIndx, unsigned int toIndx, float& appliedImpulseObj)
{
  Assert(fromIndx < toIndx);
  Assert(IsLocal());

  // Find average point and direction.
  Vector3 avgPoint = VZero;
  Vector3 avgDir = VZero;
  Person * obj = dyn_cast<Person, Object>(objColl[fromIndx].object);

  Assert(obj != NULL);

  int lastcomponent = -1;
  for(unsigned int i = fromIndx; i < toIndx; i++)
  {
    CollisionInfo & info =  objColl[i];
    avgPoint += info.pos;

    if (lastcomponent != info.component)  // it finds better direction
    {
      avgDir += info.dirOutNotNorm;  
      lastcomponent = info.component;
    }
  }

  avgPoint /= (toIndx - fromIndx);  
  avgDir.Normalize();

  // Calculate Collision in this point
  float carSpeed = SpeedAtPoint(avgPoint) * avgDir;
  float personSpeed = obj->FutureVisualState().DirectionModelToWorld(obj->FutureVisualState().ModelSpeed()) * avgDir;
  float diffSpeed = carSpeed - personSpeed;

  if (diffSpeed >= 0) // nothing to do
    return;

  // In situation where man crashed into car let man solve it...
  if (personSpeed >= -diffSpeed/2)
    return;

  float reaction = avgDir * DiffAccelerationAtPointOnForceAtPoint(avgPoint, avgDir ,avgPoint) + obj->GetInvMass();

  float restitutionCoef = 1 + (RigidityCoef() + 0.25) / 2;

  float impulseSize = (-diffSpeed * restitutionCoef / reaction);
  Vector3 impulse = avgDir * impulseSize;
  impulseSize = fabsf(impulseSize);

  Vector3 centerOfMass(VFastTransform,FutureVisualState().ModelToWorld(),GetCenterOfMass());
  Vector3 torque = (avgPoint - centerOfMass).CrossProduct(impulse);

  // Apply Impulses
  ApplyImpulse(impulse, torque);
  DamageOnImpulse(this, impulseSize, PositionWorldToModel(avgDir));

  // Because to switch a person into freefall mode, impulseForce must be set, ApplyImpulse cannot be used.
  // I am adding impulse also for local objects. 
  if (!obj->IsLocal())
    obj->AddImpulse(-impulse, VZero);

  obj->AddImpulseNetAware(-impulse, VZero);

  //obj->DammageOnImpulse(obj, fImpulseSize, cAvgDirModel);

  appliedImpulseObj += impulseSize;
}

Vector3 Transport::WreckFriction( Vector3Par speed )
{
  Vector3 friction;
  friction.Init();
  friction[0]=speed[0]*fabs(speed[0])*10+speed[0]*20+fSign(speed[0])*10;
  friction[1]=speed[1]*fabs(speed[1])*7+speed[1]*20+fSign(speed[1])*10;
  //friction[2]=speed[2]*fabs(speed[2])*5+speed[2]*20+fSign(speed[2])*10;
  friction[2]=speed[2]*fabs(speed[2])*9+speed[2]*110+fSign(speed[2])*10;
  return friction*GetMass()*(1.0/1700);
}

#define LimitFriction(f,s,t,im) (f)

void Transport::SimulateWreck(float deltaT, SimulationImportance prec)
{
  //***************************
  // Section 1. Not really connected with physical simulation.
  //***************************

  _isUpsideDown=FutureVisualState().DirectionUp().Y()<0.3f;

  Transport::Simulate(deltaT, prec);  

  const TransportType *type = Type();
  static const float terrainCoef = 6.0f;

  Vector3 speed=FutureVisualState().ModelSpeed();
  float speedSize=fabs(speed.Z());

  if (FutureVisualState()._speed.SquareSize() > 0.1f || _angVelocity.SquareSize() > 0.1f)
    OnMoved();

  // handle impulse
  float impulse2=_impulseForce.SquareSize() + _impulseTorque.SquareSize();
  if (impulse2>/*Square(GetMass()*0.01f)*/ 0.0f)
  {
    OnMoved();
    DamageOnImpulse(this,_impulseForce.Size(), VZero);
  }

  if (GetStopped())
  {
    // reset impulse - avoid cumulation
    _impulseForce = VZero;
    _impulseTorque = VZero;
    FutureVisualState()._modelSpeed = VZero;
    FutureVisualState()._speed = VZero;
    _angVelocity = VZero;
    _angMomentum = VZero;
  }

  //***************************
  // Section 2. Physical simulation
  //***************************

  // calculate all forces, frictions and torques
  Vector3 force(VZero),friction(VZero);
  Vector3 torque(VZero),torqueFriction(VZero);

  Vector3 pForce(VZero); // partial force
  Vector3 pCenter(VZero); // partial force application point

  float newPotencialEnergy = 0.0f;
  bool validPotencialEnergy = (_impulseForce == VZero && _impulseTorque == VZero);

  if (!_isStopped && !CheckPredictionFrozen())
  {
    //***************************
    // Section 2.1 Physical simulation - calculate new position prediction according to velocities
    //***************************
    Ref<EntityVisualState> moveTrans = CloneFutureVisualState();  // we don't know the type of the wreck - can't use PredictPos
    moveTrans->SetTransform(ApplySpeed(deltaT));
    ApplyRemoteState(deltaT,*moveTrans);

    //***************************
    // Section 2.2 Physical simulation - check if in new position are deep contacts
    //***************************

#if _ENABLE_WALK_ON_GEOMETRY
    const float MAX_IN  = 1.5f;
    const float ABOVE_ROAD = 0.5f;
#else
    const float MAX_IN  = 0.2f;
    const float ABOVE_ROAD = 0.05f;
#endif

    GroundCollisionBuffer groundCollBuffer;
    CollisionBuffer objCollBuffer;

    bool doObjectColl = prec<=SimulateVisibleFar && IsLocal();


    // Objects are setposed. If car "eat" the pillar it starts shaking because of setposing.
#define SET_POS_OBJECT 1

    float softFactor=floatMin(4000/GetMass()/2 * speedSize/4,1.0f);
    //float softFactor=0;

    PROFILE_SCOPE(carCD);
    GLOB_LAND->GroundCollision(groundCollBuffer,this,*moveTrans,ABOVE_ROAD,softFactor);
#if SET_POS_OBJECT
    if (doObjectColl)
      GLOB_LAND->ObjectCollision(objCollBuffer,this,*moveTrans);
#endif

    Vector3 averageRepairMove = VZero;
    unsigned int numberOfDeepColl = 0;

    // Find all deep contacts and calculate average repair move.
    for(int i = 0; i < groundCollBuffer.Size(); i++)
    {
      if (MAX_IN < groundCollBuffer[i].under && groundCollBuffer[i].type != GroundWater)
      {
        Vector3 dirOut=Vector3(0,groundCollBuffer[i].dZ,1).CrossProduct(Vector3(1,groundCollBuffer[i].dX,0)).Normalized();
        averageRepairMove += (groundCollBuffer[i].under - MAX_IN) * dirOut[1] * dirOut;
        numberOfDeepColl++;                
      }
    }
#if SET_POS_OBJECT

#define MOVE_OBJECT_LESS 1.0f  // collisions with objects are less important or big repairs does not looks good.
    if (doObjectColl)
    {       
      for(int i = 0; i < objCollBuffer.Size();)
      {
        Object *obj=objCollBuffer[i].object;
        obj->SimulationNeeded(GetVisualStateAge(FutureVisualState()));
        if (dyn_cast<Person,Object>(obj)) 
        {
          i++;
          continue;
        }

        unsigned int imax = i;
        float underMax = objCollBuffer[i].under;

        i++;
        while(i < objCollBuffer.Size() && obj == objCollBuffer[i].object)
        {
          if (objCollBuffer[i].under > underMax)
          {
            imax = i;
            underMax = objCollBuffer[i].under;
          }
          i++;
        }

        if (MAX_IN < objCollBuffer[imax].under && !obj->IsPassable())
        {                    
          averageRepairMove += (objCollBuffer[imax].under - MAX_IN) * objCollBuffer[imax].dirOut * MOVE_OBJECT_LESS;
          numberOfDeepColl++;                
        }
      }
    }
#endif        

    if (numberOfDeepColl > 0)
    {
      // Apply repair move to avoid deep contacts.
      averageRepairMove /= numberOfDeepColl;

      Matrix4 transform=moveTrans->Transform();
      Point3 newPos=transform.Position();            
      newPos+=averageRepairMove;
      transform.SetPosition(newPos);
      moveTrans->SetTransform(transform);

      groundCollBuffer.Clear();
      objCollBuffer.Clear();

      GLOB_LAND->GroundCollision(groundCollBuffer,this,*moveTrans,ABOVE_ROAD,softFactor);
#if SET_POS_OBJECT
      if (doObjectColl)
        GLOB_LAND->ObjectCollision(objCollBuffer,this,*moveTrans);

      ///             if (objCollBuffer.Size() > 0)
      //                validPotencialEnergy = false;
#endif
    }

#if !SET_POS_OBJECT
    if (doObjectColl)
      GLOB_LAND->ObjectCollision(objCollBuffer,this,*moveTrans);
#endif

    //***************************
    // Section 2.3 Physical simulation - turning + trust
    //***************************

    Vector3 wCenter(VFastTransform,FutureVisualState().ModelToWorld(),GetCenterOfMass());

    // convert forces to world coordinates
    FutureVisualState().DirectionModelToWorld(torque,torque);
    FutureVisualState().DirectionModelToWorld(force,force);

    //***************************
    // Section 2.4 Physical simulation - gravitation
    //***************************
    // apply gravity
    pForce=Vector3(0,-G_CONST*GetMass(),0);
    force+=pForce;

#if ARROWS
    AddForce(wCenter, pForce*InvMass());
#endif

    //***************************
    // Section 2.5 Physical simulation - friction angular + air friction.
    //***************************

    // angular velocity causes also some angular friction, this should be simulated as torque
    torqueFriction = _angMomentum*0.5f;
    // body air friction
    FutureVisualState().DirectionModelToWorld(friction, WreckFriction(speed));
#if ARROWS
    AddForce(wCenter,-friction*InvMass());
#endif

    //***************************
    // Section 2.6 Physical simulation - collision resolving
    //***************************

    ApplyImpulses(); // Apply all collected impulses

    wCenter.SetFastTransform(moveTrans->ModelToWorld(),GetCenterOfMass());

    float maxImpactSpeed = 0.0f;
    // how much in the collision is the max. under detected vertex
    // this can be used to rewind the time back to make sure high speed collisions are handled better
    float maxImpactUnder = 0.0f;
    // surface normal direction corresponding to maxImpactSpeed
    Vector3 maxImpactDirOut(VZero);
    Vector3 maxImpactSpeedLocal(VZero);

    float soft = 0.0f;
    float dust = 0.0f;
    if (deltaT>0)
    {
      // check collision on new position
      //float crash=0;
      //float sFactor=type->GetMaxSpeedMs()*1.3;
      Vector3 fSpeed=speed/*-Vector3(0,0,_thrust*sFactor)*/;
      // avoid too fast accel/deccel
      float maxAcc = floatMin(5,type->GetMaxSpeedMs()*0.14);
      saturate(fSpeed[0],-maxAcc,+maxAcc);
      saturate(fSpeed[1],-maxAcc,+maxAcc);
#if _VBS3 //increase brake
      saturate(fSpeed[2],-maxAcc,+maxAcc);
#else
      saturate(fSpeed[2],-maxAcc*0.6,+maxAcc);
#endif

      float brakeFriction = 1;

      Vector3 objForce(VZero); // total object force
      Vector3 landForce(VZero); // total land force

      Vector3 objTorque(VZero); // total torque made by object forces.

      _objectContact=false;

      // For crash sound.
      float maxImpactSpeedWater = 0.0f;
      float appliedImpulseObj = 0.0f;
      float maxImpactSpeedLand = 0.0f;
      float maxUnderWater = 0.0f;

      bool onlyStaticObjectContact = true;
      //float maxCFactor = 0;

      // check if we would rather go front or back
      float frontBackPreference = 0.0f;
      float leftRightPreference = 0.0f;
      if (doObjectColl)
      {
        //***************************
        // Section 2.6.1 Physical simulation - collision resolving with objects
        //***************************

#define MAX_IN_FORCE 0.1        

        int i=0;
        bool takeStatic = false;  // first resolve non-static objects and then static ones 
        // it tries to avoid penetration with static objects                                   

        while (i < objCollBuffer.Size())
        {
          //CollisionInfo &info=objCollBuffer[i];
          Object *obj=(objCollBuffer[i].hierLevel == 0) ? objCollBuffer[i].object : objCollBuffer[i].parentObject;

          if( !obj || obj->IsPassable() || (!takeStatic && obj->Static()) ||
            (takeStatic && !obj->Static()))
          {
            i++;
            if (!takeStatic && i == objCollBuffer.Size())
            {
              takeStatic = true;
              i = 0;
            }
            continue;
          }

          _objectContact=true;

          // Find all contacts with the object
          int fromIndx = i;
          int toIndx = i + 1;                    
          while( toIndx < objCollBuffer.Size() && obj == ((objCollBuffer[toIndx].hierLevel == 0) ? objCollBuffer[toIndx].object : objCollBuffer[toIndx].parentObject))
            toIndx++;

          EntityAI * bodyTmp = dyn_cast<EntityAI,Object>(obj); 
          if (bodyTmp && bodyTmp->GetStopped())
            bodyTmp->OnMoved();

          bool calculatedImpulses = false;

          DestructType destType = obj->GetDestructType();
          bool staticObj = obj->Static();

          onlyStaticObjectContact = onlyStaticObjectContact && staticObj;

          if (!staticObj)
            validPotencialEnergy = false;

          bool isPerson = dyn_cast<Person, Object>(obj) != NULL;
          if (isPerson)
          {
#if !defined _XBOX || !(_XBOX_VER>=2)
            // following line was causing ICE with X360 SDK 5632
            // TODOX360: fix instead of removing
            // Persons are not physical objects, that is why they must be handled in different way..
            ObjectCollisionWithPerson(objCollBuffer, fromIndx, toIndx, appliedImpulseObj);
#endif
            calculatedImpulses = true;
          }
          else
          {                                                
            if ((!staticObj /*&& destType != DestructMan*/) || (staticObj && !(destType == DestructTree|| destType == DestructTent || destType == DestructMan)))
            {
              // for such objects run marina simulation.
              // fixed static object
              CarCollisionsContactResolver resolver;
              resolver.Init(deltaT, this, objCollBuffer, fromIndx, toIndx);
              resolver.Resolve();

              calculatedImpulses = true;

              appliedImpulseObj += resolver.GetAppliedImpulse();

              Vector3 contactPos = objCollBuffer.GetContactPos(fromIndx,toIndx);
              Vector3 contactRel = PositionWorldToModel(contactPos);

              frontBackPreference -= contactRel.Z();
              leftRightPreference -= contactRel.X();

            }
            else
            {
              if (staticObj && (destType == DestructTree|| destType == DestructTent || destType == DestructMan))
              {       
                // The object can be destroyed. Apply just impulse till value that destroys it,.
                float maxAllowedImpulse = 1.0f/ (obj->GetInvArmor()* obj->GetInvMass() * 5.0f);

                CarCollisionsResolverMaxImpulse resolver;
                resolver.Init(deltaT,this, objCollBuffer, fromIndx, toIndx, maxAllowedImpulse);                                
                resolver.Resolve();

                if (maxAllowedImpulse <= resolver.GetAppliedImpulse())
                {
                  // the impulse was limited - this means the object is not strong enough to stop it
                  // crush it down
                  DoDamageResult result;
                  result.damage = 1;
                  obj->ApplyDoDamage(result, this, RString());
                }
                else
                {
                  Vector3 contactPos = objCollBuffer.GetContactPos(fromIndx,toIndx);
                  Vector3 contactRel = PositionWorldToModel(contactPos);

                  frontBackPreference -= contactRel.Z();
                  leftRightPreference -= contactRel.X();
                }

                calculatedImpulses = true;
                appliedImpulseObj += resolver.GetAppliedImpulse();
              }             
            }                 
          }

          for(;i < toIndx; i++)
          {
            CollisionInfo &info=objCollBuffer[i];
            // info.pos is relative to object

            float penFactor ; // factor for penetration forces
            if (staticObj)
              penFactor= GetMass();//floatMin(info.object->GetMass(), GetMass());                                                                        
            else
            {
              if (isPerson)
              { // For man lower penFactor, otherwise people would pushing cars without problem
                penFactor = obj->GetMass() / 10.0f;
              }
              else
              {                            
                penFactor = (GetMass() + obj->GetMass()) / 2;
                saturateMax(penFactor, GetMass() / 2);
                saturateMin(penFactor, GetMass() * 2);
              }
            }

            /// Penetration force in contact
            Vector3 pos=info.pos;
            Vector3 dirOut=info.dirOut;
            // create a force pushing "out" of the collision
            float forceIn=floatMin(info.under,MAX_IN);
            //float forceIn = info.under;
#define MAX_SPRING_POINTS 10
#define MAX_OUT_ACCEL 5.0f 
            penFactor *= MAX_OUT_ACCEL / MAX_IN;

            if (objCollBuffer.Size() > MAX_SPRING_POINTS)
            {
              penFactor *= MAX_SPRING_POINTS * 1.0f/objCollBuffer.Size();
            }                                                

            Vector3 pForce=dirOut*penFactor*forceIn;

            newPotencialEnergy += 0.5 * penFactor * forceIn * forceIn;

            Vector3 pTorque = pForce;

            pCenter=pos-wCenter;                        
            objForce+=pForce;
            objTorque+=pCenter.CrossProduct(pTorque);      

#if ARROWS
            AddForce(pos,pForce*InvMass(),Color(0,1,0));
#endif
            // Friction
            Vector3 speedLocal = SpeedAtPoint(pos);

            if (!obj->Static())
            {
              Entity *pcBody2 = dyn_cast<Entity,Object>(obj);
              if (pcBody2)
              {
                speedLocal -= pcBody2->SpeedAtPoint(pos);
              }
            }

            Vector3 speedFriction = speedLocal - dirOut * (dirOut * speedLocal);

            float speedFrictionSize = speedFriction.Size();
            if (speedFrictionSize > 0.01f)
            {
              //speedFriction.Normalize();
              //if (speedFrictionSize < 1.0f)
              //    speedFrictionSize *= 10.0f;
              //else
              //    saturateMax(speedFrictionSize, 10.0f);                            

#define FRICTION_COEF 0.4f
              pForce = speedFriction / speedFrictionSize * penFactor*forceIn* FRICTION_COEF;

              friction += pForce;
              torqueFriction +=pCenter.CrossProduct(pForce);                             
#if ARROWS
              AddForce(pos, pForce*InvMass(), Color(1,1,0));
#endif
            }


            //damping
            float speedInDirection = (dirOut * speedLocal);
            if (!isPerson)
            {
#if !_ENABLE_WALK_ON_GEOMETRY // damping is really huge and can stop car... 

              pForce = 15 * dirOut * speedInDirection * GetMass() / objCollBuffer.Size();

              friction += pForce;
              torqueFriction +=pCenter.CrossProduct(pForce);
#endif
            }
#if ARROWS
            AddForce(pos, pForce*InvMass(), Color(1,0,1));
#endif
            // DoDamage
            if (!calculatedImpulses && speedInDirection > 0 /*&& obj->GetDestructType() != DestructMan*/&& IsLocal())
            {                                                        
              float damage = speedInDirection * obj->GetMass() * GetInvMass() * GetInvArmor() * 0.02f / (toIndx - fromIndx);
              if( damage>0.1 )
              {
                LocalDamageMyself(VZero,damage,GetRadius());
              }

              appliedImpulseObj += speedInDirection * GetMass() / 10.0f;
            }
          }
          if (!takeStatic && i == objCollBuffer.Size())
          {
            takeStatic = true;
            i = 0;
          }
        }
      } // if( object collisions enabled )


      // if there was some contact, we would like to know what can we do to avoid it
      /*
      if (frontBackPreference!=0)
      {
      LogF(
      "%s: front/back %g, left/right %g",
      cc_cast(GetDebugName()),frontBackPreference,leftRightPreference
      );
      }
      // frontBackPreference
      */


      //***************************
      // Section 2.6.2 Physical simulation - collision resolving with ground
      //***************************                   

      // check for collisions
      _landContact=false;     
      _waterContact=false;

      if( groundCollBuffer.Size()>0 )
      {       
#define MAX_UNDER_FORCE 0.1f
        const Shape *landcontact = GetShape()->LandContactLevel();
        int numberOfContactPoints = landcontact ? landcontact->NPos() : 8;
        // if there are more collision points than possible contact points,
        // number of contact points must be low
        // this may be due to object geometry used instead of landcontact
        //saturateMax(numberOfContactPoints,groundCollBuffer.Size());
        float contactCoef = 8.0f/numberOfContactPoints;

        Vector3 damperDir = FutureVisualState().DirectionUp();
        Vector3 forwardDir = FutureVisualState().Direction();

        SetSurfaceInfo(groundCollBuffer);

        // information for the detected collision
        //LogF("nCollision %d",gCollision.Size());
        for( int i=0; i<groundCollBuffer.Size(); i++ )
        {
          // info.pos is world space
          UndergroundInfo &info=groundCollBuffer[i];
          // we consider two forces
          if( info.under<0 )
          {
            /*
            LogF("Negative under %.3f",info.under);
            LogF("  tex %s",info.texture ? info.texture->Name() :"<null>");
            LogF("  obj %s",info.obj ? (const char *)info.obj->GetDebugName() : "<null>");
            */
            continue;
          }

          float under;

          if (info.type == GroundWater)
          {
            _waterContact=true;

            // simulate swimming force
            //const float coefNPoints=12.0/12.0;
#if _VBS3
            const float coefNPoints=16.0/numberOfContactPoints;
#else
            const float coefNPoints=(16.0/1.5)/numberOfContactPoints;
#endif
            // first is water is "pushing" everything up - causing some momentum
            saturateMax(maxUnderWater,info.under);

            pForce=Vector3(0,GetMass()*0.5f*info.under*coefNPoints,0);
            pForce*=0.1; // cannot float
            pCenter=info.pos-wCenter;
            torque+=pCenter.CrossProduct(pForce);
            landForce+=pForce;

            // add stabilizing torque
            // stabilized means DirectionUp() is (0,1,0)
            Vector3 stabilize=VUp-moveTrans->DirectionUp();
            torque+=Vector3(0,coefNPoints*1.5f*GetMass(),0).CrossProduct(stabilize);

#if ARROWS
            AddForce(wCenter+pCenter,pForce*InvMass());
#endif

            // second is "water friction" - causing no momentum
            pForce[0]=speed[0]*fabs(speed[0])*15;
            pForce[1]=speed[1]*fabs(speed[1])*15+speed[1]*160;
            pForce[2]=speed[2]*fabs(speed[2])*6;

            pForce=FutureVisualState().DirectionModelToWorld(pForce*info.under)*GetMass()*(coefNPoints/700);
#if ARROWS
            AddForce(wCenter+pCenter,-pForce*InvMass());
#endif
            friction+=pForce;
            torqueFriction+=_angMomentum*0.3;

            float colSpeed2 = FutureVisualState()._speed.SquareSize();
            if( colSpeed2>Square(8) )
            {             
              saturateMax(maxImpactSpeedWater,colSpeed2);              
            }
          }
          else
          {
            // Collision with solid ground

            _landContact=true;

            Vector3 contactForce = VZero;                        
            Vector3 contactFriction = VZero;   

            // some friction is caused by moving the land aside
            // this applies only to soft surfaces
            if( info.texture )
            {
              soft=info.texture->Roughness()*2.5f;
              dust=info.texture->Dustness()*2.5f;

              saturateMin(dust,1);
            }


            Vector3 dirOut=Vector3(0,info.dZ,1).CrossProduct(Vector3(1,info.dX,0)).Normalized();
            Vector3 speedLocal = SpeedAtPoint(info.pos);

            // If the damper dir is nearly the same as dirOut
            // It is simple and more stable if it is really in dirOut direction.
            Vector3 localDamperDir = (damperDir * dirOut > 0.9f) ? dirOut : damperDir;


            // braking friction for this point
            float brakeFrictionWheel = brakeFriction;

            bool foundWheel = false;
            under = (info.under - ABOVE_ROAD) * dirOut[1] ; // under is in VUp direction

            float speedToGround = -dirOut * speedLocal;
            if (under > 0)
            {
              contactForce += dirOut * GetMass()*40.0f*contactCoef*under;
              contactFriction -= dirOut * GetMass()*contactCoef * 
                (speedToGround * 0.2f + fSign(speedToGround) * 0.25f);

              newPotencialEnergy += 
                0.5 * GetMass()*40.0f*contactCoef*under*under;
            }

            // points other than wheels brake almost as much as wheel brake would
            saturateMax(brakeFrictionWheel,0.8f);
            // contact with something else but the wheel
            // depending on the speed we may want to damage the vehicle
            // we may also want to simulate some kind of impact / absorb some part of the inertia

            if (speedToGround>0)
            {
              if (speedToGround>maxImpactSpeed)
              {
                maxImpactSpeed = speedToGround;
                maxImpactUnder = info.under;
                maxImpactDirOut = dirOut;
                maxImpactSpeedLocal = speedLocal;
              }
              saturateMax(maxImpactSpeedLand,speedToGround);
            }

            pCenter=info.pos-wCenter;
            torque+=pCenter.CrossProduct(contactForce);
            torqueFriction+=pCenter.CrossProduct(contactFriction);           

#if ARROWS
            AddForce(wCenter+pCenter, contactForce*InvMass(), Color(1,0,0));

            AddForce(wCenter+pCenter + Vector3(0,2,0), contactForce, Color(0,0,1));

            AddForce(wCenter+pCenter, -contactFriction*InvMass(), Color(0,1,1));
#endif
            // Friction                                             

            Vector3 contactFriction2 = VZero;

            Vector3 forwardDirLocal = forwardDir - dirOut * (forwardDir * dirOut);
            if (forwardDirLocal.SquareSize() < 0.1f)
            {
              // Forward dir is nearly parallel with dirOut
              // choose arbitrary perpendicular vector. 
              forwardDirLocal = localDamperDir - dirOut * (localDamperDir * dirOut);
              Assert(forwardDirLocal.SquareSize() >= 0.1f);
            }

            forwardDirLocal.Normalize();
            Vector3 sideDirLocal = forwardDirLocal.CrossProduct(dirOut);

            DirectionWorldToModel(forwardDirLocal, forwardDirLocal);
            DirectionWorldToModel(sideDirLocal, sideDirLocal);


            // Friction in point
            // second is "land friction" - causing little momentum                            

            float forwardSpeed1 = fSpeed * forwardDirLocal;
            float sideSpeed1 = fSpeed * sideDirLocal;                                       

            pForce = forwardDirLocal * (forwardSpeed1*30+fSign(forwardSpeed1)*(3000 + brakeFrictionWheel * 40000));

            if (speed[2] > 1 && foundWheel)
              torque += 0.3 * FutureVisualState().DirectionModelToWorld(Vector3(pForce[2],0,0))*(GetMass()*(1.0/40000)*contactCoef);

            pForce += sideDirLocal * (sideSpeed1*500+fSign(sideSpeed1)*90000);
            pForce=FutureVisualState().DirectionModelToWorld(pForce)*(GetMass()*(1.0/40000)*contactCoef);

            contactFriction2+=LimitFriction(pForce,speed,deltaT,InvMass());
            torqueFriction+=_angMomentum*0.5f*contactCoef;

            // Friction according to land roughness
            float landMoved=0.02;
            //            saturateMin(landMoved,0.1);

            float forwardSpeed2 = speed * forwardDirLocal;
            float sideSpeed2 = speed * sideDirLocal;          

            pForce = forwardDirLocal * (forwardSpeed2 * 4500);

            if (speed[2] > 1 && foundWheel)
              torque += 0.5 * FutureVisualState().DirectionModelToWorld(Vector3(pForce[2],0,0))*(GetMass()*(1.0/1000)*contactCoef*landMoved*soft*terrainCoef);

            pForce += sideDirLocal * (sideSpeed2 * 2000);

            pForce=FutureVisualState().DirectionModelToWorld(pForce)*( GetMass()*(1.0/1000)*contactCoef*landMoved*soft*terrainCoef);

            contactFriction2+=LimitFriction(pForce,speed,deltaT,InvMass());

#if ARROWS
            AddForce(wCenter+pCenter, -contactFriction2*InvMass(), Color(1,1,1));  
#endif              
            contactFriction += contactFriction2;             

            landForce += contactForce;                        
            friction += contactFriction;

            pCenter=info.pos-wCenter;            
          }
        }

#if 0 //_ENABLE_CHEATS
        GlobalShowMessage(100,"Under %.2f, brake %.2f, force %.1f, fric %.1f",totalUnder,brakeFriction,DirectionWorldToModel(force).Z(),DirectionWorldToModel(friction).Z());
#endif

        if (_waterContact)
        {
          const SurfaceInfo &info = GLandscape->GetWaterSurface();
          soft = info._roughness * 2.5f;
          dust = info._dustness * 2.5f;

          saturateMin(dust , 1);
        }
      }                        

      force += objForce;
      torque += objTorque;

      force += landForce;

      //***************************
      // Section 2.6.4 Physical simulation - Crash
      //*************************** 

      if (IsLocal())
      {

        const float maxAllowedImpactSpeed = 10;
        if (maxImpactSpeed > maxAllowedImpactSpeed && _maxContactSpeed<maxAllowedImpactSpeed)
        {
#if ENABLE_REPORT
          if (this==GWorld->CameraOn())
            LogF("Impact maxImpactSpeed=%g, under=%g",maxImpactSpeed,maxImpactUnder);
#endif
          // reduce the object impact speed
          // maxImpactSpeed was computed as -maxImpactDirOut*maxImpactSpeedLocal;
          // target is maxAllowedImpactSpeed
          // speed is in, and quite large - we know the angle is < 90 deg
          // reduce overall speed to avoid excessive jumping back
          // TODO: some part may be caused by the rotation
          // we may need to reduce the rotation as well

          float speedImpact = -maxImpactDirOut*FutureVisualState().Speed();
          if (speedImpact>maxAllowedImpactSpeed)
          {
            Vector3 newSpeed = FutureVisualState().Speed()+maxImpactDirOut*(speedImpact-maxAllowedImpactSpeed);

#if ENABLE_REPORT
            if (this==GWorld->CameraOn())
            {
              LogF(
                "Reduced speed from %g to %g (%g,%g,%g to (%g,%g,%g)",
                speedImpact,maxAllowedImpactSpeed,
                FutureVisualState().Speed()[0],FutureVisualState().Speed()[1],FutureVisualState().Speed()[2],
                newSpeed[0],newSpeed[1],newSpeed[2]
              );
              FutureVisualState()._speed = newSpeed;
            }
#endif
          }
          //

        }
        // remember the information from previous frame
        _maxContactSpeed = maxImpactSpeed;

        // starting time when no crash are evaluate
        if (Glob.time>_disableDamageUntil && _doCrash == CrashNone)
        {
          // Choose the biggest crash for sound
          float crashLand = fabsf(maxImpactSpeedLand);
          float crashObj = appliedImpulseObj * GetInvMass();
          float crashWater = fabsf(maxImpactSpeedWater);
          float crash = 0;

          if (crashLand > crashObj)
          {
            if (crashLand > crashWater)
            {
              _doCrash = CrashLand;
              crash = crashLand;
            }
            else
            {
              _doCrash = CrashWater;
              crash = crashWater;
            }                        
          }
          else
          {
            if (crashObj > crashWater)
            {
              _doCrash = CrashObject;
              crash = crashObj;
            }
            else
            {
              _doCrash = CrashWater;
              crash = crashWater;
            }
          }

          // specify CrashObject
          if (_doCrash == CrashObject)
          {
            // check collision buffer
            if (objCollBuffer.Size() > 0)
            {
              // CrashObject distinguish types
              int destTree = 0;
              int destBuild = 0;
              int destEng = 0;

              for (int i = 0; i < objCollBuffer.Size(); i++)
              {
                CollisionInfo &cInfo = objCollBuffer[i];

                if (cInfo.object)
                {
                  // distinguish crash object on the basis of destruction types
                  DestructType dt = cInfo.object->GetDestructType();

                  if (dt == DestructTree)
                    destTree++;
                  else if (dt == DestructBuilding)
                    destBuild++;
                  else if (dt == DestructEngine || dt == DestructWreck)
                    destEng++;
                }
              }

              // no suitable category found
              if (destEng == 0 && destTree == 0 && destBuild == 0)
              {
                _doCrash = CrashNone;
              }
              else
              {
                // select the most principal object
                if (destTree > destBuild)
                {
                  if (destTree > destEng) 
                    _doCrash = CrashTree;
                  else
                    _doCrash = CrashArmor;
                }
                else
                {
                  if (destBuild > destEng)
                    _doCrash = CrashBuilding;
                  else
                    _doCrash = CrashArmor;
                }
              }
            }
          }

          _crashVolume = floatMinMax(0, 1, crash * 0.1);

          if (crash < 0.01)
          {
            _doCrash = CrashNone;
            _crashVolume = 0;
          }
        }
      }
    }

    //***************************
    // Section 3. Dust, head movement ... 
    //***************************        
#if 0
    if (this==GWorld->CameraOn())
    {
      float ratio = _angMomentum.Size()>0 ? torqueFriction.Size()/_angMomentum.Size() : 0;
      GlobalShowMessage(200, "ratio %7.2f, torqueF %9.2f, angMom %9.2f angVel %7.2f", ratio, torqueFriction.Size(), _angMomentum.Size(), _angVelocity.Size());
    }
#endif    

    StabilizeTurrets(FutureVisualState().Transform().Orientation(), moveTrans->Orientation());

    //***************************
    // Section 2.7 Move to new position and re-normalize energy
    //***************************
    Vector3 oldPosition = FutureVisualState().Position();
    Move(*moveTrans);

    // Energy renormalization
    float newKinetic=GetMass()*FutureVisualState()._speed.SquareSize()*0.5f; // E=0.5*m*v^2
    newKinetic += _angMomentum * _angVelocity * 0.5f;
    float newDiffPotencial = (FutureVisualState().Position()[1] - oldPosition[1]) *GetMass() * G_CONST;                

    if (_validEnergy && validPotencialEnergy && (newDiffPotencial + newKinetic + newPotencialEnergy) > _oldEnergy /*+ GetMass()/10*/)
    {
      float repairedKinetic =  _oldEnergy - newDiffPotencial - newPotencialEnergy;                        

      if (repairedKinetic <= 0)
      {                
        FutureVisualState()._speed = VZero;
        _angVelocity = VZero;
        _angMomentum = VZero;
      }
      else
      {
        float renormalizationCoef = sqrt(repairedKinetic/newKinetic);                
        FutureVisualState()._speed *= renormalizationCoef;
        _angVelocity *= renormalizationCoef;
        _angMomentum *= renormalizationCoef;

      }
    }

    newKinetic = GetMass()*FutureVisualState()._speed.SquareSize()*0.5f; // E=0.5*m*v^2
    newKinetic += _angMomentum * _angVelocity * 0.5f;

    _oldEnergy = newKinetic + newPotencialEnergy;
    _validEnergy = validPotencialEnergy;        

    //***************************
    // Section 2.7 Calculate new velocities
    //***************************       

#if  ARROWS
    // Show force and torque.
    AddForce(wCenter + Vector3(0,4,0), force * InvMass(), Color(1,0.5, 0.25));
    AddForce(wCenter + Vector3(0,4,0), torque * InvMass(), Color(0.25,0.5, 1));
#endif

    ApplyForces(deltaT,force,torque,friction,torqueFriction/*, 5.5f * GetMass()*/);

    // apply static friction
    bool stopCondition = false;
    if (_landContact && !_waterContact /*&& !_objectContact*/)        
    {
      float maxSpeed = Square(0.7f);
      if (!Driver())
        maxSpeed = Square(1.2f);
      if (FutureVisualState()._speed.SquareSize()<maxSpeed && _angVelocity.SquareSize()<maxSpeed*0.3f)
        stopCondition = true;
    }
    if (stopCondition)
      StopDetected();
    else
      OnMoved();
    DirectionWorldToModel(FutureVisualState()._modelSpeed, FutureVisualState()._speed);
  }
  // Simulate items that must be simulate after transformation change
  SimulatePost(deltaT,prec);  
}

void Transport::ShowDamage(int part, EntityAI *killer)
{
  if (EngineIsOn() && !IsDamageDestroyed())
  {
    _showDmg = true;
    _showDmgValid = Glob.time + 5.0f;
  }
  base::ShowDamage(part,killer);
}

AnimationStyle Transport::IsAnimated( int level ) const {return AnimatedGeometry;}

bool Transport::PreloadMuzzleFlash(const WeaponsState &weapons, int weapon) const
{
  if (weapon < 0 || weapon >= weapons._magazineSlots.Size()) return true;
  const MagazineSlot &slot = weapons._magazineSlots[weapon];
  if (!slot._magazine || !slot._magazine->_type) return true;
  const AmmoType *ammo = slot._magazine->_type->_ammo;
  if (!ammo) return true;

  float z2 = GScene->GetCamera()->Position().Distance2(FutureVisualState().Position());

  if (EnableVisualEffects(SimulateVisibleNear))
  {
    switch (ammo->_simulation)
    {
      case AmmoShotBullet:
      case AmmoShotSpread:
        // preload muzzle flash texture - level not known
        // TODO: correct WeaponsType
        return Type()->_weapons._animFire.PreloadTexture(_shape, z2);
    }
  }
  return true;
}


void Transport::Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2)
{
  if (GScene->MainLight()->NightEffect() > 0.01 && EngineIsOn())
    GetType()->_dashboard.Unhide(animContext, _shape, level);
  else
    GetType()->_dashboard.Hide(animContext, _shape, level);

  //float dt = _showDmgValid - Glob.time;
  bool on = (toInt(2.0 * (_showDmgValid - Glob.time)) % 2) != 0;
  if (_showDmg && on)
    GetType()->_showDmg.Unhide(animContext, _shape, level);
  else
    GetType()->_showDmg.Hide(animContext, _shape, level);

  // animate turrets
  for (int i=0; i<_turrets.Size(); i++)
  {
    TurretType *turretType = Type()->_turrets[i];
    Turret *turret = _turrets[i];
    turret->Animate(*turretType, animContext, _shape, level, setEngineStuff, parentObject);
  }

  base::Animate(animContext, level, setEngineStuff, parentObject, dist2);
}

void Transport::AnimateManProxyMatrix(ObjectVisualState const& vs, int level, const ManProxy &proxy, Matrix4 &proxyTransform) const
{
  AnimateBoneMatrix(proxyTransform,vs,level,proxy._proxy->boneIndex);
  // default: do not know how to animate man proxy
}

void Transport::Deanimate(int level, bool setEngineStuff)
{
  // animate turrets
  for (int i=0; i<_turrets.Size(); i++)
  {
    TurretType *turretType = Type()->_turrets[i];
    Turret *turret = _turrets[i];
    turret->Deanimate(*turretType, _shape, level, setEngineStuff);
  }
  base::Deanimate(level,setEngineStuff);
}


LODShapeWithShadow *Transport::GetOpticsModel(const Person *person) const
{
  if (!person) return NULL;
  // if person can be hidden and is not, it means he cannot use optics

  VisualState& vs = unconst_cast(RenderVisualState());

  if (person == _driver)
  {
    if (Type()->_hideProxyInCombat && vs._driverHidden<0.5) return NULL;
    return Type()->_driverOpticsModel;
  }
  else
  {
    TurretContextV context;
#if _VBS3
    if (FindOpticsTurret(vs, person, context) && context._turret)
#else
    if (FindTurret(vs, person, context) && context._turret)
#endif
    {
      if (Type()->_hideProxyInCombat && context._turretVisualState->GetGunnerHidden() < 0.5)
        return context._turretType->_viewGunner[context._turret->_currentViewGunnerMode]._gunnerOpticsModel;
      else
        return context._turretType->_viewOptics[context._turret->_currentViewOpticsMode]._gunnerOpticsModel;
    }
  }
  return NULL;
}


bool Transport::GetForceOptics(const Person *person, CameraType camType) const
{
  if (camType != CamInternal) return false;

  if (person == _driver) return Type()->_driverForceOptics;

  TransportVisualState const& vs = RenderVisualState();
  TurretContextV context;
  if (FindTurret(unconst_cast(vs), person, context) && context._turretType)
  {
    if (Type()->_hideProxyInCombat && context._turretVisualState->GetGunnerHidden() < 0.5)
      return context._turretType->_gunnerOutForceOptics;
    else
      return context._turretType->_gunnerForceOptics;
  }
  return false;
}

bool Transport::PreloadView(Person *person, CameraType camType) const
{
  bool ret = base::PreloadView(person,camType);
  return ret;
}

PackedColor Transport::GetOpticsColor(const Person *person)
{
  if (!person) return PackedBlack;

  if (person == _driver) return Type()->_driverOpticsColor;
  else
  {
    TransportVisualState const& vs = RenderVisualState();
    TurretContextV context;
    if (FindTurret(unconst_cast(vs), person, context) && context._turretType)
    {
      if (Type()->_hideProxyInCombat && context._turretVisualState->GetGunnerHidden() < 0.5)
        return context._turretType->_gunnerOutOpticsColor;
      else
        return context._turretType->_gunnerOpticsColor;
    }
  }
  return PackedBlack;
}

bool Transport::ShowAim(const TurretContextV &context, int weapon, CameraType camType, Person *person) const
{
  if (camType == CamGunner && context._turretType)
  {
    if (Type()->_hideProxyInCombat && context._turretVisualState->GetGunnerHidden() < 0.5)
    {
      if (!context._turretType->_gunnerOutOpticsShowCursor) return false;
    }
    else
    {
      if (!context._turretType->_gunnerOpticsShowCursor) return false;
    }
  }

  // if player is gunner, he needs to see where the weapon is aiming to
  // if player is commander, he also sometimes need to see aiming point
  // this is esp. important for heli. unguided rockers
  AIBrain *gunUnit = EffectiveGunnerUnit(context);
  AIBrain *comUnit = EffectiveObserverUnit();
  if (
    comUnit && person==comUnit->GetPerson() ||
    gunUnit && person==gunUnit->GetPerson()
    )
  {
    return base::ShowAim(context, weapon, camType, person);
  }
  return false;
}

bool Transport::ShowCursor(const TurretContextV &context, int weapon, CameraType camType, Person *person) const
{
  if (camType == CamGunner && context._turretType)
  {
    if (Type()->_hideProxyInCombat && context._turretVisualState->GetGunnerHidden() < 0.5)
    {
      if (!context._turretType->_gunnerOutOpticsShowCursor) return false;
    }
    else
    {
      if (!context._turretType->_gunnerOpticsShowCursor) return false;
    }
  }

  AIBrain *driver = PilotUnit();
  if (driver && person == driver->GetPerson())
  {
    // commander or effective observer need a cursor for commanding
    if (
      EffectiveObserverUnit()->GetPerson() && person != EffectiveObserverUnit()->GetPerson() &&
      CommanderUnit()->GetPerson() && person != CommanderUnit()->GetPerson()
    ) return false;
  }

  // if gunner can hide but is not hidden and may not fire, do not show cursor
  if (context._turret && !context._turretVisualState->IsGunnerHidden() && Type()->GetHideProxyInCombat() && !context._turretType->OutGunnerMayFire(context._gunner)) return false;
  if (GWorld->LookAroundEnabled() && camType != CamGunner) return false;
  return base::ShowCursor(context, weapon, camType, person);
}

/// Functor returning the texture of flag carried by the gunner
class GunnerFlagTexture : public ITurretFunc
{
protected:
  Texture *&_texture;

public:
  GunnerFlagTexture(Texture *&texture) : _texture(texture) {}

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._gunner) return false; // continue
    Texture *texture = context._gunner->GetFlagTexture();
    if (!texture) return false; // continue
    _texture = texture;
    return true; // found
  }
};

Texture *Transport::GetFlagTexture()
{
  Texture *texture = NULL;
  GunnerFlagTexture func(texture);
  if (ForEachTurret(func)) return texture;
  for( int i=0; i<_manCargo.Size(); i++ )
  {
    Person *cargo = _manCargo[i];
    if (cargo && cargo->GetFlagTexture()) return cargo->GetFlagTexture();
  }
  return NULL;
}

ManProxy::ManProxy()
{
  //selection = -1;
  _proxy = NULL;
  transform = MIdentity;
}

int Transport::PassNum( int lod )
{
#if _VBS3
  // For proxie drawing of man,
  // Use second draw pass instead of first pass
  // Engine limitation that has no work around
  Man *man = dyn_cast<Man>(GWorld->CameraOn());
  if(man&&
    man->Brain()&&
    man->IsPersonalItemsEnabled()&&
    man->Brain()->GetVehicleIn() == this)
    return 2;
#endif

  if (DrawInPass3()) return 3;
  int basePass = base::PassNum(lod);
  //   // if there are any proxies, we need to make sure we are rendered after the crew
  //   // we do this by forcing in pass "Slight alpha - rough z-order preferred"
  //   if (basePass<0) basePass = 0;
  return basePass;
}

bool Transport::CastPass3VolShadow(int level, bool zSpace, float &castShadowDiff, float &castShadowAmb) const
{
  // Check the cargo, gunner and pilot geometries
  bool isCargo = false, isGunner = false, isPilot = false;
  if (level>=_shape->FindSimplestLevel())
  {
    if (_shape->IsSpecLevel(level,VIEW_CARGO)) isCargo = true;
    else if (_shape->IsSpecLevel(level,VIEW_GUNNER)) isGunner = true;
    else if (_shape->IsSpecLevel(level,VIEW_PILOT)) isPilot = true;
  }

  // Determine shadow casting
  bool castVolShadow = false;
  castShadowDiff = 1.0f;
  castShadowAmb = 1.0f;
  if (zSpace)
  {
    if (isCargo)
    {
      castVolShadow = Type()->_viewCargoShadow;
      castShadowDiff = Type()->_viewCargoShadowDiff;
      castShadowAmb = Type()->_viewCargoShadowAmb;
    }
    else if (isPilot)
    {
      castVolShadow = Type()->_viewDriverShadow;
      castShadowDiff = Type()->_viewDriverShadowDiff;
      castShadowAmb = Type()->_viewDriverShadowAmb;
    }
    else if (isGunner)
    {
      TurretContext context;
      if (GetPrimaryGunnerTurret(context) && context._turretType)
      {
        castVolShadow = context._turretType->_viewGunnerShadow;
        castShadowDiff = context._turretType->_viewGunnerShadowDiff;
        castShadowAmb = context._turretType->_viewGunnerShadowAmb;
      }
    }
  }
  return castVolShadow;
}


void TransportType::DrawMFDs(Transport *veh, int cb, const PositionRender &pos) const
{
  for (int i=0; i<_mfd.Size(); i++)
  {
    _mfd[i].Draw(cb,veh,pos);
  }
}

struct DrawMFDs: public Engine::MainThreadRenderTask
{
  Transport*veh;
  PositionRender pos;

  DrawMFDs(Transport*veh, const PositionRender & pos):veh(veh),pos(pos){}
  
  void operator () (Engine *engine) const
  {
    Object::ProtectedVisualState<const ObjectVisualState> vs = veh->RenderVisualStateScope();
    veh->Type()->DrawMFDs(veh,-1,pos);
  }

};

void Transport::Draw(int cb, int level, const SectionMaterialLODs &matLOD, ClipFlags clipFlags, const DrawParameters &dp,
  const InstanceParameters &ip, float dist2, const PositionRender &pos, SortObject *oi)
{
  if (level == LOD_INVISIBLE) return; // invisible LOD
#if _ENABLE_CHEATS
  if (CHECK_DIAG(DETransparent))
    return;
#endif

  float castShadowDiff = Pass3IntensityMod(level,dp);

  // during the rendering we may want to modulate diffuse color
  Color instColor = GEngine->GetInstanceColor(cb);
  float instShadow = GEngine->GetInstanceShadowIntensity(cb);
  GEngine->SetInstanceInfo(cb,instColor,instShadow*castShadowDiff);
  base::Draw(cb,level, matLOD, clipFlags, dp, ip, dist2, pos, oi);
  GEngine->SetInstanceInfo(cb,instColor,instShadow);

  if (!dp._sbIsBeingRendered)
  {
    if (!_shape->IsNormalGraphicalLod(level)) // glass would hide the MFD/HUD anyway, no reason to draw it
    {
      Engine::MainThreadRenderTask *task = new DrawMFDs(this,pos);
      GEngine->PerformTaskOnMainThread(task);
    }
  }
  
#ifndef _XBOX
  RString text;
#if _VBS2 //draw URN instead of squad sign
  GameVarSpace *vars = GetVars();
  if (vars)
  {
    GameValue var;
    if (vars->VarGet("URN", var))
      text = var.GetData()->GetString();
  }
  if(text.GetLength() == 0) //assign clan squadtitle only if no URN is present
#endif
  if (CommanderUnit())
    text = CommanderUnit()->GetPerson()->GetInfo()._squadTitle;

  if (text.GetLength() > 0)
    Type()->_squadTitles.Draw(cb,level,clipFlags,pos,text);
#endif
}

void Transport::DrawDiags()
{
#if _ENABLE_CHEATS
  LODShapeWithShadow *forceArrow=GScene->ForceArrow();
#define DRAW_OBJ(obj) GScene->DrawObject(obj)

  if (CHECK_DIAG(DEPath))
  {
    LODShapeWithShadow *shape = GScene->Preloaded(SphereModel);
    PackedColor colorGetIn = PackedColor(Color(0,1,0,1));
    PackedColor colorGetOut = PackedColor(Color(1,0,0,1));
    Person *player = GWorld->FocusOn() ? GWorld->FocusOn()->GetPerson() : NULL;
    if (ObserverBrain())
    {
      Vector3Val pos = GetCommanderGetOutPos(Observer())._pos;
      Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
      obj->SetPosition(pos);
      obj->SetConstantColor(colorGetOut);
      DRAW_OBJ(obj);
    }
    else if (GetCommanderAssigned())
    {
      Vector3Val pos = GetCommanderGetInPos(GetCommanderAssigned()->GetPerson(), GetCommanderAssigned()->GetPerson()->WorldPosition(GetCommanderAssigned()->GetPerson()->RenderVisualState()))._pos;
      Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
      obj->SetPosition(pos);
      obj->SetConstantColor(colorGetIn);
      DRAW_OBJ(obj);
    }
    else if (player)
    {
      Vector3Val pos = GetCommanderGetInPos(player, player->WorldPosition(player->RenderVisualState()))._pos;
      Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
      obj->SetPosition(pos);
      obj->SetConstantColor(colorGetIn);
      obj->SetScale(0.2);
      DRAW_OBJ(obj);
    }
    if (DriverBrain())
    {
      Vector3Val pos = GetDriverGetOutPos(Driver())._pos;
      Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
      obj->SetPosition(pos);
      obj->SetConstantColor(colorGetOut);
      DRAW_OBJ(obj);
    }
    else if (GetDriverAssigned())
    {
      Vector3Val pos = GetDriverGetInPos(GetDriverAssigned()->GetPerson(), GetDriverAssigned()->GetPerson()->WorldPosition(GetDriverAssigned()->GetPerson()->RenderVisualState()))._pos;
      Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
      obj->SetPosition(pos);
      obj->SetConstantColor(colorGetIn);
      DRAW_OBJ(obj);
    }
    else if (player)
    {
      Vector3Val pos = GetDriverGetInPos(player, player->WorldPosition(player->RenderVisualState()))._pos;
      Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
      obj->SetPosition(pos);
      obj->SetConstantColor(colorGetIn);
      obj->SetScale(0.2);
      DRAW_OBJ(obj);
    }
    if (GunnerBrain())
    {
      Vector3Val pos = GetGunnerGetOutPos(Gunner())._pos;
      Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
      obj->SetPosition(pos);
      obj->SetConstantColor(colorGetOut);
      DRAW_OBJ(obj);
    }
    else if (GetGunnerAssigned())
    {
      Vector3Val pos = GetGunnerGetInPos(GetGunnerAssigned()->GetPerson(), GetGunnerAssigned()->GetPerson()->WorldPosition(GetGunnerAssigned()->GetPerson()->RenderVisualState()))._pos;
      Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
      obj->SetPosition(pos);
      obj->SetConstantColor(colorGetIn);
      DRAW_OBJ(obj);
    }
    else if (player)
    {
      Vector3Val pos = GetGunnerGetInPos(player, player->WorldPosition(player->RenderVisualState()))._pos;
      Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
      obj->SetPosition(pos);
      obj->SetConstantColor(colorGetIn);
      obj->SetScale(0.2);
      DRAW_OBJ(obj);
    }
    bool drawn = false;
    for (int i=0; i<_cargoAssigned.Size(); i++)
    {
      AIBrain *unit = _cargoAssigned[i];
      if (unit && unit->GetVehicleIn() == this)
      {
        Vector3Val pos = GetCargoGetOutPos(unit->GetPerson())._pos;
        Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
        obj->SetPosition(pos);
        obj->SetConstantColor(colorGetOut);
        DRAW_OBJ(obj);
        drawn = true;
      }
      else if (unit && !unit->GetVehicleIn())
      {
        Vector3Val pos = GetCargoGetInPos(i, unit->GetPerson(), unit->GetPerson()->WorldPosition(unit->GetPerson()->RenderVisualState()))._pos;
        Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
        obj->SetPosition(pos);
        obj->SetConstantColor(colorGetIn);
        DRAW_OBJ(obj);
        drawn = true;
      }
    }
    if (!drawn && player)
    {
      Vector3Val pos = GetCargoGetInPos(0, player, player->WorldPosition(player->RenderVisualState()))._pos;
      Ref<Object> obj = new ObjectColored(shape, VISITOR_NO_ID);
      obj->SetPosition(pos);
      obj->SetConstantColor(colorGetIn);
      obj->SetScale(0.1);
      DRAW_OBJ(obj);
    }

    {
      {
        Matrix3 mat(MRotationY,-_azimutWanted);
        Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);

        float size=0.15;
        arrow->SetPosition(RenderVisualState().Position()+VUp*1.0);
        arrow->SetOrient(mat.Direction(),VUp);
        arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
        arrow->SetScale(size);
        arrow->SetConstantColor(PackedColor(Color(1,1,0,0.5)));

        DRAW_OBJ(arrow);
      }
      {
        float dirWanted = atan2(_dirWanted.X(), _dirWanted.Z());
        Matrix3 mat(MRotationY,-dirWanted);
        Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);

        float size=0.1;
        arrow->SetPosition(RenderVisualState().Position()+VUp*1.5);
        arrow->SetOrient(mat.Direction(),VUp);
        arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
        arrow->SetScale(size);
        arrow->SetConstantColor(PackedColor(Color(1,1,0,0.5)));

        DRAW_OBJ(arrow);
      }
    }
  }
  if (CHECK_DIAG(DEPath) && QIsManual())
  {
    {
      Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);

      float size=1.0f;
      arrow->SetPosition(RenderVisualState().Position()+RenderVisualState().Direction()*10+VUp);
      arrow->SetOrient(VForward,VUp);
      arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
      arrow->SetScale(size);
      arrow->SetConstantColor(PackedColor(Color(0.7,1,0,0.5)));

      DRAW_OBJ(arrow);
    }
  }
#endif
  base::DrawDiags();
}

#if _VBS3
// Generic behavior of get weapon point
Vector3 Transport::GetWeaponPoint(const TurretContext &context, int weapon) const
{
  if (weapon<0 || weapon >= context._weapons->_magazineSlots.Size()) return VZero;
  const AmmoType *ammo = context._weapons->GetAmmoType(weapon);
  if (ammo)
  {
    switch (ammo->_simulation )
    {
    case AmmoShotLaser:
      if(!context._weapons->_laserTargetOn)
        return VZero;
      break;
    case AmmoShotBullet:
      if (!context._turretType)
        return VZero;
      else
        return context._turretType->GetTurretPos(GunTurretTransform(*context._turretType));
      break;
    }
  }
  return EntityAIFull::GetWeaponPoint(context,weapon);
}
#endif

void Transport::DrawCameraCockpit()
{
  AIBrain *unit = GWorld->FocusOn();
  if (!unit) return;
  Person *person = unit->GetPerson();
  if (!person) return;

  VisualState& vs = unconst_cast(RenderVisualState());

  if (GWorld->GetCameraType() == CamGunner)
  {
    LODShapeWithShadow *oShape = GetOpticsModel(person);
#if _VBS3
    // Draw Lod 1 as the inner optic
    if (oShape && oShape->NLevels()>1)
    {
      float zoom = 1.0f;
      TurretContext context;
      if (FindOpticsTurret(person, context) && context._turret)
      {
        const ViewPars* viewPars = context._turret->GetCurrentViewPars();
        zoom = viewPars->_initFov / GetCameraFOV();
      }
      else
      {
        if (!unit->GetVehicleIn()) //gunner not inside vehicle
        {
          float curWeapon = context._weapons->ValidatedCurrentWeapon();
          if (curWeapon >= 0)
          {
            const MagazineSlot &slot = context._weapons->_magazineSlots[curWeapon];
            const MuzzleType *muzzle = slot._muzzle;
            zoom = muzzle->_opticsZoomInit / GetCameraFOV();
          }
        }
      }
      Draw2D(NULL, oShape, 1, GetOpticsColor(person), zoom);    }
#endif
    if (oShape && oShape->NLevels()>0) Draw2D(NULL, oShape, 0, GetOpticsColor(person));
  }


  float alpha = 0;
  // decision value is not 0.5 but 0.1
  const float posFactor = 1/0.1f;
  // make sure there is time enough for preloading during the transition
  const float negFactor = 1/0.6f;
  if (person == _driver)
    alpha = floatMin(vs._driverHidden*posFactor, (1.0f - vs._driverHidden)*negFactor);
  else
  {
    TurretContextV context;
    if (FindTurret(vs, person, context) && context._turret)
    {
      float hidden = context._turretVisualState->GetGunnerHidden();
      alpha = floatMin(hidden*posFactor, (1.0f - hidden)*negFactor);
    }
  }

  if (alpha <= 0) return;

  saturateMin(alpha, 1);
  PackedColor color = PackedColor(Color(0, 0, 0, alpha));
  const float w = GEngine->WidthBB();
  const float h = GEngine->HeightBB();
  MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
  GEngine->Draw2D(mip, color, Rect2DAbs(0, 0, w, h));
}

bool Transport::PreloadCrewMember(const ManProxy &proxy, Person *man, float dist2) const
{
  // animate and draw driver
  LODShapeWithShadow *manShape=man->GetShape();
  int level=GScene->PreloadLevelFromDistance2(manShape,dist2,FutureVisualState().Scale());
  if (level>=0)
  {
    // shape ready - preload textures as well
    ShapeUsed lock = manShape->Level(level);
    return lock->PreloadTextures(dist2, NULL) && manShape->PreloadVBuffer(level,dist2);
  }
  return false;
}

bool Transport::NeedsPrepareProxiesForDrawing() const
{
  return base::NeedsPrepareProxiesForDrawing();
}

/// Functor for preparing of gunner proxy for drawing
class PrepareGunnerForDrawing : public ITurretFuncV
{
protected:
  Object *_rootObj;
  int _rootLevel;
  const AnimationContext &_animContext;
  const PositionRender &_transform;
  int _level;
  ClipFlags _clipFlags;
  SortObject *_so;
  bool _external;

public:
  PrepareGunnerForDrawing(
    Object *rootObj, int rootLevel, const AnimationContext &animContext, const PositionRender &transform, int level, ClipFlags clipFlags, SortObject *so, bool external)
    : _rootObj(rootObj), _rootLevel(rootLevel), _animContext(animContext), _transform(transform), _level(level),
    _clipFlags(clipFlags), _so(so), _external(external) {}

  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (!context._turret) return false; // continue
    const ManProxy &proxy = context._turretType->_gunnerProxy[_level];
    if (context._gunner && proxy.Present() && !(_external && context._turretVisualState->IsGunnerHidden() && !context._turretType->_viewGunnerInExternal))
    {
      Transport *vehicle = static_cast<Transport *>(entity); // we know where we call it
      vehicle->PrepareCrewMemberForDrawing(
        _rootObj, _rootLevel, _animContext, _transform, _level, _clipFlags, _so, proxy, context._gunner);
    }
    return false; // continue
  }
};

/*!
\patch_internal 1.05 Date 7/18/2001 by Jirka
- Changed: enable drawing gunner in external camera even if turned in
\patch 1.85 Date 9/10/2002 by Jirka
- Added: CfgVehicles properties hideWeaponsDriver, hideWeaponsGunner, hideWeaponsCommander, hideWeaponsCargo
\patch 5126 Date 2/7/2007 by Flyman
- Fixed: Hellfires (maverickweapon) on a helicopter were visible even when the helicopter was destroyed
*/

void Transport::PrepareProxiesForDrawing(
  Object *rootObj, int rootLevel, const AnimationContext &animContext,
  const PositionRender &transform, int level, ClipFlags clipFlags, SortObject *so)
{
  bool external = !_shape->IsSpecLevel(level,VIEW_CARGO) && !_shape->IsSpecLevel(level,VIEW_PILOT) && !_shape->IsSpecLevel(level,VIEW_GUNNER);
  const TransportType *type = Type();
  const LevelProxies &proxies = type->_proxies[level];

  // driver proxy
  if (_driver && proxies._driverProxy.Present() && !(external && RenderVisualState().IsDriverHidden()
#if _VBS3
    && !Type()->_viewDriverInExternal
#endif
    ))
  {
    PrepareCrewMemberForDrawing(
      rootObj, rootLevel, animContext, transform, level, clipFlags, so, proxies._driverProxy, _driver);
  }

  // gunners proxies
  PrepareGunnerForDrawing func(rootObj, rootLevel, animContext, transform, level, clipFlags, so, external);
  ForEachTurret(unconst_cast(RenderVisualState()), func);

  // cargo proxies
  for (int i=0; i<_manCargo.Size(); i++)
  {
    Person *cargo = _manCargo[i];
    if (!cargo) continue;
    // if cargo position is not defined, do not draw
    if (i >= proxies._cargoProxy.Size()) break;
    if (!proxies._cargoProxy[i].Present()) continue;
    PrepareCrewMemberForDrawing(
      rootObj, rootLevel, animContext, transform, level, clipFlags, so, proxies._cargoProxy[i], cargo);
  }

  // missile proxies
  Assert(_shape->IsLevelLocked(level));
  const Shape *shape = _shape->GetLevelLocked(level);

  // check current number of missiles
  for (int i=0; i<Type()->GetProxyCountForLevel(level); i++)
  {
    // Check if the corresponding proxy face is hidden. In such case don't draw the proxy
    {
      int section = Type()->GetProxySectionForLevel(level, i);
      if (section >= 0)
      {
        const PolyProperties &pp = animContext.GetSection(shape, section);
        int spec = pp.Special();
        if (spec & IsHidden) continue;
      }
    }

    const ProxyObjectTyped &proxy = Type()->GetProxyForLevel(level,i);
    Object *obj = proxy.obj;
    const EntityType *type = obj->GetEntityType();
    if (!type) continue;

    if (!strcmp(type->_simName, "maverickweapon"))
    {
      // Decide whether draw or not
      LODShapeWithShadow *pshape = GetMissileShape(proxy.id);
      if (!pshape) continue;

      // Calculate proxy transformation (might be 0 which means the proxy is not visible)
      PositionRender pTransform;
      {
        SkeletonIndex boneIndex = Type()->GetProxyBoneForLevel(level, i);
        Matrix4Val proxyTransform = AnimateProxyMatrix(RenderVisualState(), level, proxy.GetTransform(), boneIndex);
        pTransform = transform * proxyTransform;
      }

      // LOD detection
      // check weapon proxy

      // TODO: propagate Scale instead of Scale() all way down from here
      int level = GScene->LevelFromDistance2(pshape, so->distance2, pTransform.position.Scale());
      if (level == LOD_INVISIBLE) continue;

      // temporary proxy object
      Ref<Object> temp = new Object(pshape, VISITOR_NO_ID);
      // store and register for drawing
      GScene->TempProxyForDrawing(temp, rootObj, level, rootLevel, clipFlags, so->distance2, pTransform);
    }
  }

  base::PrepareProxiesForDrawing(rootObj, rootLevel, animContext, transform, level, clipFlags, so);
}

void Transport::PrepareCrewMemberForDrawing(
  Object *rootObj, int rootLevel, const AnimationContext &animContext, const PositionRender &transform, int level, ClipFlags clipFlags, SortObject *so,
  const ManProxy &proxy, Person *man)
{
  LODShapeWithShadow *manShape = man->GetShape();

  // decide the transformation
  Matrix4 proxyTransform = proxy.transform;
#if _VBS3
  if (man->IsPersonalItemsEnabled())
    proxyTransform.SetPosition(proxyTransform.FastTransform(man->_personalItemsOffset));
#endif
  // animate proxy matrix
  AnimateManProxyMatrix(animContext.GetVisualState(), level, proxy, proxyTransform);
  proxyTransform.SetPosition(proxyTransform.FastTransform(manShape->BoundingCenter()));
#if _VBS3
  // convoy trainer
  if (man->IsPersonalItemsEnabled())
  {
    proxyTransform.SetOrientation(animContext.GetVisualState().Orientation().InverseRotation()*man->RenderVisualState().Orientation());
  }
#endif
  PositionRender pTransform = transform * proxyTransform;

  // decide the proxy LOD
  int pLevel;
  bool insidePlayer = false;
  float dist2 = so->distance2;
  CameraType camType = GWorld->GetCameraType();
  if (camType != CamExternal && camType != CamGroup)
  {
    if (!GWorld->GetCameraEffect())
    {
      AIBrain *unit = GWorld->FocusOn();
      Person *player = unit ? unit->GetPerson() : NULL;
      insidePlayer = man == player;
    }
  }
  // LOD detection
  if (insidePlayer)
  {
    if (man->UseInternalLODInVehicles())
      pLevel = man->InsideLOD(CamInternal);
    else
      pLevel = 0; // select best external LOD
  }
  else
  {
    // TODO: propagate Scale2 instead of Scale() all way down from here
    pLevel = GScene->LevelFromDistance2(manShape,dist2,pTransform.position.Scale());
  }
#if _VBS3
  if (pLevel < 0) return;
#endif

  if (pLevel == LOD_INVISIBLE) return;

  Man::ProtectedVisualState<const Man::VisualState> mvs = man->RenderVisualStateScope();
  // registration for drawing
  // avoid clipping because the bounding box is not correctly animated (pass clipFlags 0)
  GScene->ProxyForDrawing(man, rootObj, pLevel, rootLevel, 0, dist2, pTransform);

  // scan if there are some proxies inside of this proxy
  if (man->NeedsPrepareProxiesForDrawing())
  {
    const EntityType *manType = man->GetEntityType();
    if (manType)
    {
      // LOD selection for the proxy of the proxy
      int ppLevel = -1;
      LODShapeWithShadow *manShape = man->GetShape();
      if (insidePlayer)
      {
        // find inside view
        ppLevel = manShape->FindSpecLevel(VIEW_PILOT);
        // if there is no inside view, use the best LOD (if there is any)
        if (ppLevel < 0 && manShape->FindSimplestLevel() >= 0) ppLevel = 0;
      }
      if (ppLevel < 0)
      {
        // TODO: propagate Scale2 instead of Scale() all way down from here
        ppLevel = GScene->LevelFromDistance2(manShape, dist2, pTransform.position.Scale());
      }
      // most likely the Animate is empty, but we call it for consistency
      ShapeUsed pLock = manShape->Level(ppLevel);
      AnimationContextStorageGeom storage;
      AnimationContext animContext(man->RenderVisualState(),storage);
      pLock->InitAnimationContext(animContext, manShape->GetConvexComponents(ppLevel), false);
      man->Animate(animContext, ppLevel, true, man, dist2);
      if (ppLevel != LOD_INVISIBLE)
      {
        man->PrepareProxiesForDrawing(rootObj, rootLevel, animContext, pTransform, ppLevel, ClipAll, so);
      }
      man->Deanimate(ppLevel, true);
    }
  }
}

int Transport::GetCrewMemberComplexity(int level, const Matrix4 &pos, float dist2, const ManProxy &proxy, Person *man) const
{
  //similar to Object::DrawProxies

  // animate and draw driver
  LODShapeWithShadow *manShape=man->GetShape();

  Matrix4 proxyTransform=proxy.transform;

  // animate proxy matrix
  AnimateManProxyMatrix(RenderVisualState(), level,proxy,proxyTransform);

  proxyTransform.SetPosition(proxyTransform.FastTransform(manShape->BoundingCenter()));
  Matrix4Val pTransform = pos * proxyTransform;

  int pLevel;
  bool insidePlayer = false;
  CameraType camType = GWorld->GetCameraType();
  if (camType != CamExternal && camType != CamGroup)
  {
    AIBrain *unit = GWorld->FocusOn();
    Person *player = unit ? unit->GetPerson() : NULL;
    insidePlayer = man == player;
  }

  // LOD detection
  if (insidePlayer)
  {
#if 0
    pLevel = man->InsideLOD(CamInternal);
#else
    pLevel = 0; // select best external LOD
#endif
  }
  else
  {
    pLevel = GScene->LevelFromDistance2(
      manShape,dist2,pTransform.Scale()
      );
  }
  if (pLevel != LOD_INVISIBLE)
    return man->GetComplexity(pLevel,pos);
  return 0;
}

/// Functor checking if gunner is casting shadow
class CastGunnerShadow : public ITurretFuncV
{
protected:
  int &_i; // out
  const ManProxy *&_proxy; // out
  Person *&_man; // out
  bool &_castShadow; // out
  int _level;

public:
  CastGunnerShadow(int &i, const ManProxy *&proxy, Person *&man, bool &castShadow, int level)
    : _i(i), _proxy(proxy), _man(man), _castShadow(castShadow), _level(level) {}

  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (!context._turret) return false; // continue
    const ManProxy &proxy = context._turretType->_gunnerProxy[_level];
    if (proxy.Present())
    {
      if (_i-- == 0 && !context._turretVisualState->IsGunnerHidden())
      {
        _proxy = &proxy;
        _man = context._gunner;
        _castShadow = context._turretType->_castGunnerShadow;
        return true; // found
      }
    }
    return false; // continue
  }
};

bool Transport::CastProxyShadow(int level, int i) const
{
  const TransportType *type=Type();
  const LevelProxies &proxies=type->_proxies[level];
  // find corresponding proxy
  int cargoCount = proxies._cargoProxy.Size();
  const ManProxy *proxy = NULL;
  bool castShadow = false;

  if (i<proxies._normalProxy.Size())
  {
    // fixed proxies should cast shadows
    return true;
  }

  i -= proxies._normalProxy.Size();

  Person *man = NULL;
  if (i<cargoCount)
  {
    proxy = &proxies._cargoProxy[i];
    if (i<_manCargo.Size())
    {
      man = _manCargo[i];
      castShadow = type->_castCargoShadow;
    }
  }
  else
  {
    i -= cargoCount;
    if (proxies._driverProxy.Present())
    {
      if (i==0 && !RenderVisualState().IsDriverHidden()
#if _VBS3
        && !Type()->_viewDriverInExternal
#endif
        )
      {
        proxy = &proxies._driverProxy, man = _driver;
        castShadow = type->_castDriverShadow;
      }
      i--;
    }

    CastGunnerShadow func(i, proxy, man, castShadow, level);
    ForEachTurret(unconst_cast(RenderVisualState()), func);
  }
  return proxy && man && castShadow;
}

/// Functor counting gunner proxies
class CountGunnerProxy : public ITurretFunc
{
protected:
  int &_count; // out
  int _level;

public:
  CountGunnerProxy(int &count, int level)
    : _count(count), _level(level) {}

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    const ManProxy &proxy = context._turretType->_gunnerProxy[_level];
    if (proxy.Present()) _count++;
    return false; // continue
  }
};

int Transport::GetProxyCount(int level) const
{
  const TransportType *type=Type();
  const LevelProxies &proxies=type->_proxies[level];
  int count = proxies._cargoProxy.Size();
  if (proxies._driverProxy.Present()) count++;
  CountGunnerProxy func(count, level);
  ForEachTurret(func);

  return count+proxies._normalProxy.Size();
}

/// Functor searching for gunner proxy
class GetGunnerProxy : public ITurretFuncV
{
protected:
  int &_i; // out
  const ManProxy *&_proxy; // out
  Person *&_man; // out
  int _level;

public:
  GetGunnerProxy(int &i, const ManProxy *&proxy, Person *&man, int level)
    : _i(i), _proxy(proxy), _man(man), _level(level) {}

  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (!context._turret) return false; // continue
    const ManProxy &proxy = context._turretType->_gunnerProxy[_level];
    if (proxy.Present())
    {
      // sometimes even turned in proxies may be hit
      if (_i-- == 0 && (!context._turretVisualState->IsGunnerHidden() || context._turretType->_viewGunnerInExternal))
      {
        _proxy = &proxy;
        _man = context._gunner;
        return true; // found
      }
    }
    return false; // continue
  }
};

Object *Transport::GetProxy(VisualStateAge age, LODShapeWithShadow *&shape, Object *&parentObject, int level,
  Matrix4 &transform, const Matrix4 &parentPos, int i) const
{
  const TransportType *type=Type();
  TransportVisualState const& vs = GetVisualStateByAge(age).cast<Transport>();
  // may return NULL
  const LevelProxies &proxies=type->_proxies[level];
  // first enumerate normal proxies
  if (i<proxies._normalProxy.Size())
  {
    int proxyIndex = proxies._normalProxy[i];
    const ProxyObjectTyped &proxy = type->GetProxyForLevel(level,proxies._normalProxy[i]);

    SkeletonIndex boneIndex = GetType()->GetProxyBoneForLevel(level, proxyIndex);
    Matrix4 proxyTransform = AnimateProxyMatrix(vs, level, proxy.GetTransform(), boneIndex);

    transform = parentPos * proxyTransform;

    shape = proxy.GetShape();
    parentObject = proxy.obj;
    return proxy.obj;

  }
  i -= proxies._normalProxy.Size();


  // find corresponding proxy
  int cargoCount = proxies._cargoProxy.Size();
  const ManProxy *proxy = NULL;
  Person *man = NULL;
  if (i<cargoCount)
  {
    proxy = &proxies._cargoProxy[i];
    if (i<_manCargo.Size())
    {
      man = _manCargo[i];
    }
  }
  else
  {
    i -= cargoCount;
    if (proxies._driverProxy.Present())
    {
      if (i==0 && !vs.IsDriverHidden()
#if _VBS3
        && !Type()->_viewDriverInExternal
#endif
        ) proxy = &proxies._driverProxy, man = _driver;
      i--;
    }
    if (i >= 0)
    {
      GetGunnerProxy func(i, proxy, man, level);
      ForEachTurret(unconst_cast(vs), func);
    }
  }
  if (!proxy || !man || !proxy->_proxy) return NULL;

  LODShapeWithShadow *manShape=man->GetShape();

  Matrix4 proxyTransform=proxy->transform;

#if _VBS2 //-VBS2 shift convoy proxy
  if(man->IsPersonalItemsEnabled())
    proxyTransform.SetPosition(proxyTransform.FastTransform(man->_personalItemsOffset));
#endif //-!
  AnimateManProxyMatrix(vs, level,*proxy,proxyTransform);

  proxyTransform.SetPosition(proxyTransform.FastTransform(manShape->BoundingCenter()));
  transform = parentPos*proxyTransform;

  shape = manShape;
  parentObject = man;
  return man;
}

Matrix4 Transport::ProxyWorldTransform(VisualStateAge age, const Object *obj) const
{
  ObjectVisualState const& vs = GetVisualStateByAge(age);
  // find proxy
  int level = 0;
  int n = GetProxyCount(level);
  for (int i=0; i<n; i++)
  {
    // check if it is the wanted proxy
    // problem: which level?
    Matrix4 trans;
    LODShapeWithShadow *shape = NULL;
    Object *parentObject = NULL;
    Object *proxy = GetProxy(age, shape, parentObject,level,trans,vs,i);
    if (proxy==obj)
    {
      return trans;
    }
  }
  return vs.Transform();
}
Matrix4 Transport::ProxyInvWorldTransform(VisualStateAge age, const Object *obj) const
{
  ObjectVisualState const& vs = GetVisualStateByAge(age);
  // find proxy
  // TODO: search not only level 0
  // man should be placed somewhere not to be dependent on level
  int level=0;
  int n = GetProxyCount(level);
  for (int i=0; i<n; i++)
  {
    // check if it is the wanted proxy
    // problem: which level?
    Matrix4 trans;
    LODShapeWithShadow *shape = NULL;
    Object *parentObject = NULL;
    Object *proxy = GetProxy(age, shape, parentObject,level,trans,vs,i);
    if (proxy==obj)
    {
      return trans.InverseGeneral();
    }
  }
  return vs.GetInvTransform();
}

/// Functor for preload of gunner proxy
class PreloadGunner : public ITurretFuncV
{
protected:
  bool &_ret; // out
  int _level;
  float _dist2;
  bool _external;

public:
  PreloadGunner(bool &ret, int level, float dist2, bool external)
    : _ret(ret), _level(level), _dist2(dist2), _external(external) {}

  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (!context._turret) return false; // continue
    const ManProxy &proxy = context._turretType->_gunnerProxy[_level];
    if (context._gunner && proxy.Present() && !(_external && context._turretVisualState->IsGunnerHidden() && !context._turretType->_viewGunnerInExternal))
    {
      Transport *vehicle = static_cast<Transport *>(entity); // we now where we call it
      if (!vehicle->PreloadCrewMember(proxy, context._gunner, _dist2)) _ret = false;
    }
    return false; // continue
  }
};

bool Transport::PreloadProxies(int level, float dist2, bool preloadTextures) const
{
  bool ret = true;

  bool external = !_shape->IsSpecLevel(level,VIEW_CARGO) && !_shape->IsSpecLevel(level,VIEW_PILOT) && !_shape->IsSpecLevel(level,VIEW_GUNNER);

  const TransportType *type=Type();
  const LevelProxies &proxies=type->_proxies[level];
  if (_driver && proxies._driverProxy.Present() && !(external && RenderVisualState().IsDriverHidden()
#if _VBS3
    && !Type()->_viewDriverInExternal
#endif
    ))
  {
    if (!PreloadCrewMember(proxies._driverProxy,_driver,dist2)) ret = false;
  }
  // CHANGED
  PreloadGunner func(ret, level, dist2, external);
  ForEachTurret(unconst_cast(RenderVisualState()), func);

  for( int i=0; i<_manCargo.Size(); i++ )
  {
    Person *cargo=_manCargo[i];
    if (!cargo) continue;
    // if cargo position is not defined, do not draw
    if (i>=proxies._cargoProxy.Size()) break;
    if (!proxies._cargoProxy[i].Present()) break;
    if (!PreloadCrewMember(proxies._cargoProxy[i],cargo,dist2)) ret = false;
  }

  // draw missiles
  // check number of missiles


  //Shape *sShape = _shape->Level(level);
  for (int i=0; i<Type()->GetProxyCountForLevel(level); i++)
  {
    const ProxyObjectTyped &proxy = Type()->GetProxyForLevel(level,i);
    Object *obj = proxy.obj;
    const EntityType *type = obj->GetEntityType();
    if (!type) continue;
    if (!strcmp(type->_simName, "maverickweapon"))
    {
      // LOD detection
      // check weapon proxy
      LODShapeWithShadow *pshape = GetMissileShape(proxy.id);
      if (!pshape) continue;
      int level = GScene->PreloadLevelFromDistance2(pshape, dist2, FutureVisualState().Scale());
      if (level<0) ret = false;
    }
  }

  if (!base::PreloadProxies(level, dist2, preloadTextures)) ret = false;
  return ret;
}

/// Functor calculating complexity of gunner proxy
class GetGunnerComplexity : public ITurretFuncV
{
protected:
  int &_nFaces; // out
  int _level;
  const Matrix4 &_pos;
  float _dist2;
  bool _external;

public:
  GetGunnerComplexity(int &nFaces, int level, const Matrix4 &pos, float dist2, bool external)
    : _nFaces(nFaces), _level(level), _pos(pos), _dist2(dist2), _external(external) {}

  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (!context._turret) return false; // continue
    const ManProxy &proxy = context._turretType->_gunnerProxy[_level];
    if (context._gunner && proxy.Present() && !(_external && context._turretVisualState->IsGunnerHidden()))
    {
      Transport *vehicle = static_cast<Transport *>(entity); // we now where we call it
      _nFaces += vehicle->GetCrewMemberComplexity(
        _level, _pos, _dist2,
        proxy, context._gunner);
    }
    return false; // continue
  }
};

int Transport::GetProxyComplexity(int level, const Matrix4 &pos, float dist2) const
{
  int nFaces = 0;

  bool external = !_shape->IsSpecLevel(level,VIEW_CARGO) && !_shape->IsSpecLevel(level,VIEW_PILOT);

  const TransportType *type=Type();
  const LevelProxies &proxies=type->_proxies[level];
  if (_driver && proxies._driverProxy.Present() && !(external && RenderVisualState().IsDriverHidden()
#if _VBS3
    && !Type()->_viewDriverInExternal
#endif
    ))
  {
    nFaces += GetCrewMemberComplexity
      (
      level,pos,dist2,
      proxies._driverProxy,_driver
      );
  }

  GetGunnerComplexity func(nFaces, level, pos, dist2, external);
  ForEachTurret(unconst_cast(RenderVisualState()), func);

  for( int i=0; i<_manCargo.Size(); i++ )
  {
    Person *cargo=_manCargo[i];
    if (!cargo) continue;
    // if cargo position is not defined, do not draw
    if (i>=proxies._cargoProxy.Size()) break;
    if (!proxies._cargoProxy[i].Present()) break;
    nFaces += GetCrewMemberComplexity
      (
      level,pos,dist2,
      proxies._cargoProxy[i],cargo
      );
  }

  int nMissiles = CountMissiles();

  //Shape *sShape = _shape->Level(level);
  for (int i=0; i<Type()->GetProxyCountForLevel(level); i++)
  {
    const ProxyObjectTyped &proxy = Type()->GetProxyForLevel(level,i);
    Object *obj = proxy.obj;
    if (!obj) continue;
    const EntityType *type = obj->GetEntityType();
    if (!type) continue;
    RString simulation = type->_simName;
    if (strcmp(simulation, "maverickweapon") == 0)
    {
      if (proxy.id>nMissiles) continue;
      // smart clipping par of obj->Draw
      // LOD detection
      Matrix4Val pTransform=pos*proxy.obj->FutureVisualState().Transform();

      LODShapeWithShadow *pshape = obj->GetShapeOnPos(pTransform.Position());
      if (!pshape) continue;
      int level = GScene->LevelFromDistance2(
        pshape, dist2, pTransform.Scale()
        );
      if (level == LOD_INVISIBLE) continue;

      nFaces += obj->GetComplexity(level,pTransform);
    }
  }

  return nFaces;
}

Matrix4 Transport::FindMissilePos(int index, bool &found) const
{
  found = false;
  //ShapeUsed  lock = _shape->Level(0);
  for (int i=0; i<Type()->GetProxyCountForLevel(0); i++)
  {
    const ProxyObjectTyped &proxy = Type()->GetProxyForLevel(0,i);
    Object *obj = proxy.obj;
    const EntityType *type = obj->GetEntityType();
    if (!type) continue;
    if (strcmp(type->_simName, "maverickweapon")) continue;
    if (proxy.id!=index) continue;
    found = true;
    Matrix4 pos;
    pos.SetPosition(proxy.obj->FutureVisualState().Position());
    // proxy matrix is XZ reversed
    pos.SetDirectionAside(-proxy.obj->FutureVisualState().DirectionAside());
    pos.SetDirection(-proxy.obj->FutureVisualState().Direction());
    pos.SetDirectionUp(proxy.obj->FutureVisualState().DirectionUp());
    return pos;
  }

  return MIdentity;
}

bool Transport::GetOpticsCamera(Matrix4 &transf, CameraType camType) const
{
  AIBrain *unit = GWorld->FocusOn();
  if (!unit)
    return false;

  Person *man = unit->GetPerson();
  if (!man)
    return false;

  VisualState& vs = unconst_cast(RenderVisualState());
  
  const TransportType *type = Type();

  TurretContextV context;
  context._turret = NULL;

  int index = -1;
  if (man == _driver)
    index = type->_driverOpticsPos;
  else
  {
#if _VBS3 //CommanderOverride
    if (FindOpticsTurret(vs, man, context) && context._turret)
#else
    if (FindTurret(vs, man, context) && context._turret)
#endif
    {
      if (Type()->_hideProxyInCombat && context._turretVisualState->GetGunnerHidden() < 0.5f)
        index = context._turretType->_gunnerOutOpticsPos;
      else
        index = context._turretType->_gunnerOpticsPos;
    }
  }
  if (index < 0)
    return false;

  // animate
  const Shape *memory = _shape->MemoryLevel();
  const NamedSelection::Access ptSel(memory->NamedSel(index),_shape,memory);
  int ptIndex = ptSel[0];
  transf.SetPosition(memory->Pos(ptIndex));
  transf.SetOrientation(M3Identity);
  if (_shape->GetSkeleton())
  {
    // get bone index of the point
    const AnimationRTWeight &weight = memory->GetVertexBoneRef()[ptIndex];
    if (weight.Size() > 0)
    {
      Assert(weight.Size() == 1);
      SubSkeletonIndex ssBone = weight[0].GetSel();
      SkeletonIndex bone = memory->GetSkeletonIndexFromSubSkeletonIndex(ssBone);
      AnimateBoneMatrix(transf, vs, _shape->FindMemoryLevel(), bone);
    }
  }

  // eventually apply offsets provided by laser targeting or ballistics computer
  if (camType == CamGunner)
  {
#if _VBS3_LASE_LEAD
    Turret *turret = context._turret;
    if (turret)
    {
      Matrix3 rot1(MRotationX,turret->_laseOffsetX);
      //    Matrix3 rot2(MRotationY,turret->_laseOffsetY);
      //    transf.SetOrientation(transf.Orientation() * rot1 * rot2);
      transf.SetOrientation(transf.Orientation() * rot1);
    }
#endif

// WIP:BALLISTICS_COMPUTER: what to do in VBS?
#if !_VBS2 && !_VBS3
    /*
    static BallisticsComputer bc;
    // if target can be hit (according to the ballistics computer), correct the view accordingly
    if (bc.Compute(this, context, GWorld->UI()->GetLockedTarget()))
      transf.SetOrientation(transf.Orientation() * bc.ViewCorrection());
    GScene->DrawDiagSphere( bc._targetCorrection, 3.0f, PackedColor(Color(1,0,0,1)));
    */
#endif
  }

  // add camera shake
  GWorld->GetCameraShakeManager().UpdateCameraTransform(transf);

  return true;
}

Vector3 Transport::CameraPointRel(CameraType camType) const
{
  return _shape->AimingCenter();
}

bool Transport::GetProxyCamera(Matrix4 &transf, CameraType camType) const
{
  AIBrain *unit = GWorld->FocusOn();
  if (!unit) return false;

  Person *man = unit->GetPerson();
  if (!man) return false;

  const TransportType *type = Type();
  int level = InsideLOD(camType);
  if (level == LOD_INVISIBLE) return false;

  if (level < 0) return false;

  if (camType==CamExternal)
  { //for 3rd person view, do not return the ProxyCamera, but vehicle Transform (in model coordinates), ie. identity
    // we want the camera to point at the CameraPosition
    // TODO: define via config
    transf.SetTranslation(CameraPointRel(camType));
    return true;
  }

  const VisualState &vs = RenderVisualStateScope();
  
  // the shape needs to be loaded for proxy animation
  ShapeUsed lock = _shape->Level(level);

  const LevelProxies &proxies = type->_proxies[level];

  const ManProxy *proxy = NULL;
  if (man == _driver)
  {
    proxy = &proxies._driverProxy;
  }
  else
  {
    TurretContext context;
    if (FindTurret(man, context) && context._turretType)
    {
      proxy = &context._turretType->_gunnerProxy[level];
    }
    else for (int i=0; i<_manCargo.Size(); i++)
    {
      if (i >= proxies._cargoProxy.Size()) break;
      if (!proxies._cargoProxy[i].Present()) continue;
      if (man == _manCargo[i])
      {
        proxy = &proxies._cargoProxy[i];
        break;
      }
    }
  }
  if (!proxy || !proxy->Present()) return false;

  // animate
  LODShapeWithShadow *manShape = man->GetShape();
  transf = proxy->transform;
  // TODO:
  Vector3 pos = man->GetPilotPosition(camType);
  transf.SetPosition(transf.FastTransform(manShape->BoundingCenter()));
  transf.SetPosition(transf.FastTransform(pos));
  const ProxyObject &proxyObj = *proxy->_proxy;
  AnimateBoneMatrix(transf,vs,level,proxyObj.boneIndex);
  return true;
}

static inline bool CrewCanBeHit(const Person *crew, float hidden)
{
  return crew && hidden<0.5f && crew->Brain() && crew->Brain()->LSIsAlive();
}

struct CanBeHitResult
{
  const ManProxy *_proxy; // out
  const Person *_man; // out
  float _bestPenalty;
  Vector3 _modelDirToAim;

  explicit CanBeHitResult(Vector3Par modelDirToAim)
  :_proxy(NULL),_man(NULL),_bestPenalty(FLT_MAX),_modelDirToAim(modelDirToAim)
  {
  }

  void Evaluate(const ManProxy &proxy, Person *person, float hidden)
  {
    if (proxy.Present() && CrewCanBeHit(person, hidden))
    {
      float cosAlpha = -_modelDirToAim.CosAngle(proxy.transform.Position());
      if (cosAlpha<_bestPenalty)
      {
        _bestPenalty = cosAlpha;
        _proxy = &proxy;
        _man = person;
      }

    }
  }
};

/// Functor checking if gunner in turret can be hit
class GunnerCanBeHit : public ITurretFuncV
{
protected:
  CanBeHitResult &_result;
  int _level;

public:
  GunnerCanBeHit(CanBeHitResult &result, int level)
    : _result(result), _level(level) {}

  bool operator () (EntityAIFull *entity, TurretContextV &context) override
  {
    if (!context._turret) return false; // continue
    _result.Evaluate(context._turretType->_gunnerProxy[_level],context._gunner,context._turretVisualState->GetGunnerHidden());
    // always continue, we may find a better one
    return false;
  }
};

Vector3 Transport::AimingPositionCrew(ObjectVisualState const& ovs, const AmmoType *ammo, Vector3Par aimFromDir) const
{
  int fireGeom = _shape->FindFireGeometryLevel();
  if (fireGeom>=0 && CheckCrewVulnerable())
  {
    VisualState& vs = unconst_cast(ovs.cast<Transport>());

    // check if anyone of the crew can be hit
    // first target gunner, commander, driver
    const LevelProxies &proxies = Type()->_proxies[fireGeom];
    CanBeHitResult result(vs.DirectionWorldToModel(aimFromDir));
    {
      GunnerCanBeHit func(result, fireGeom);
      ForEachTurret(vs, func);
    }
    #if _VBS3
      bool driverInExternal = Type()->_viewDriverInExternal;
    #else
      bool driverInExternal = false;
    #endif

    result.Evaluate(proxies._driverProxy,_driver,vs._driverHidden || driverInExternal);
    if (result._proxy)
    {
      // we know who should we aim at

      LODShapeWithShadow *manShape=result._man->GetShape();
      Matrix4 proxyTransform = result._proxy->transform;
      AnimateManProxyMatrix(vs, fireGeom,*result._proxy,proxyTransform);

      proxyTransform.SetPosition(proxyTransform.FastTransform(manShape->BoundingCenter()));

      // get man aiming position - it should already be animated
      Vector3 aimPos = result._man->GetLocalAimingPosition();
      return FastTransform(proxyTransform.FastTransform(aimPos));
    }
  }
  return base::AimingPosition(ovs, ammo,aimFromDir);
}

Vector3 Transport::AimingPosition(ObjectVisualState const& vs, const AmmoType *ammo, Vector3Par aimFromDir) const
{
  DoAssert(!ammo || aimFromDir.SquareSize()>0); // when ammo is specified, aiming direction needs to be specified as well
  // if the ammo is weak it might be better to target crew instead of tank
  if (ammo && (ammo->_simulation==AmmoShotBullet || ammo->_simulation==AmmoShotSpread) && ammo->hit*Type()->GetInvArmor()<0.5f)
  {
    return AimingPositionCrew(vs, ammo, aimFromDir);
  }
  // if no crew can be hit, target vehicle
  return base::AimingPosition(vs, ammo, aimFromDir);
}


float Transport::NeedsLoadFuel() const
{
  if (IsDamageDestroyed()) return 0;
  float max=GetType()->GetMaxFuelCargo();
  if( max<=0 ) return 0;
  return 1-GetFuelCargo()/max;
}

float Transport::NeedsLoadAmmo() const
{
  if (IsDamageDestroyed()) return 0;
  float max=GetType()->GetMaxAmmoCargo();
  if( max<=0 ) return 0;
  return 1-GetAmmoCargo()/max;
}

float Transport::NeedsLoadRepair() const
{
  if (IsDamageDestroyed()) return 0;
  float max=GetType()->GetMaxRepairCargo();
  if( max<=0 ) return 0;
  return 1-GetRepairCargo()/max;
}


bool Transport::IsPossibleToGetIn() const
{
  if( _isDead ) return false;
  if( _isUpsideDown ) return false;
  if (IsDamageDestroyed()) return false;
  return true;
}

bool Transport::IsWorking() const
{
  if( _isDead ) return false;
  if( _isUpsideDown ) return false;
  if (IsDamageDestroyed()) return false;
  return true;
}

bool Transport::IsAbleToMove() const
{
  // check if there is dead driver
  Person *driver = Driver();
  if (driver)
  {
    AIBrain *unit = driver->Brain();
    if (!unit)
    {
      // dead driver found and his death is known - if no replacement is assigned, report a problem
      if (!_driverAssigned) return false;
    }
  }
  // if vehicle has no fuel and should have some, it cannot move
  //if (GetFuel()<=0 && Type()->GetFuelCapacity()>0) return false;
  return IsWorking();
}

bool Transport::EjectNeeded() const
{
  return !IsPossibleToGetIn() || !IsAbleToMove();
}

struct IsAbleToFireTurret : public ITurretFunc
{
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    return entity->IsAbleToFire(context);
  }
};

bool Transport::IsAbleToFireAnyTurret() const
{
  IsAbleToFireTurret func;
  return ForEachTurret(func);
}

bool Transport::IsAbleToFire(const TurretContext &context) const
{
  // if there is a dead gunner, we cannot fire
  AIBrain *gunner = context._gunner ? context._gunner->Brain() : NULL;
  // ? commander or pilot can move to this turret ?
  if (!gunner || !gunner->LSIsAlive()) gunner = EffectiveObserverUnit();
  if (!gunner || !gunner->LSIsAlive()) gunner = PilotUnit();
  if (!gunner || !gunner->LSIsAlive()) gunner = context._turret ? context._turret->GetGunnerAssigned() : NULL;
  if (!gunner || !gunner->LSIsAlive())
  {
    return false;
  }

  if (context._weapons->_magazines.Size() == 0) return false;
  return IsWorking();
}

/// Functor returning cost of the gunner
class GetGunnerCost : public ITurretFunc
{
protected:
  float &_cost;

public:
  GetGunnerCost(float &cost) : _cost(cost) {}

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turret && context._gunner) _cost += context._gunner->GetType()->GetCost();
    return false; // continue
  }
};

float Transport::CalculateTotalCost() const
{
  float cost = GetType()->GetCost();
  if (_driver) cost += _driver->GetType()->GetCost();
  GetGunnerCost func(cost);
  ForEachTurret(func);
  for (int i=0; i<_manCargo.Size(); i++)
  {
    if (_manCargo[i]) cost += _manCargo[i]->GetType()->GetCost();
  }
  return cost;
}

bool Transport::QCanIBeIn(Person *who) const
{
  if (!IsWorking()) return false;
  /*
  if (!who || who->QIsManual())
    // let player decide if its worth getting in
    return true;
  else
    // check IsAbleToFire?
    return IsAbleToMove();
  */
  return true;
}

bool Transport::GetActionParams(UIActionParams &params, const Action *action, AIBrain *unit) const
{
  switch (action->GetType())
  {
    case ATMoveToDriver:
    case ATMoveToPilot:
    case ATMoveToGunner:
    case ATMoveToCommander:
    case ATMoveToCargo:
    case ATManualFire:
    case ATManualFireCancel:
    case ATArtilleryComputer:
      // No arguments
      return true;
  }
  return base::GetActionParams(params, action, unit);
}

typedef bool (AIBrain::*AssignF)(Transport *veh);

/// interface for functor assigning unit to vehicle
class IAssignFunc
{
public:
  virtual void operator ()(AIBrain *unit, Transport *vehicle) = 0;
};

class AssignFuncDriver : public IAssignFunc
{
public:
  AssignFuncDriver() {}
  virtual void operator ()(AIBrain *unit, Transport *vehicle)
  {
    unit->AssignAsDriver(vehicle);
  }
};

class AssignFuncCargo : public IAssignFunc
{
protected:
  int _index;

public:
  AssignFuncCargo(int index = -1) : _index(index) {}
  void SetIndex(int index) {_index = index;}
  virtual void operator ()(AIBrain *unit, Transport *vehicle)
  {
    unit->AssignAsCargo(vehicle, _index);
  }
};

class AssignFuncTurret : public IAssignFunc
{
protected:
  Turret *_turret;

public:
  AssignFuncTurret(Turret *turret = NULL) : _turret(turret) {}
  void SetTurret(Turret *turret) {_turret = turret;}
  virtual void operator ()(AIBrain *unit, Transport *vehicle)
  {
    unit->AssignToTurret(vehicle, _turret);
  }
};

static void SwapPositions(Transport *vehicle,
 Ref<Person> &p1, RString a1, bool cargo1, IAssignFunc &assign1,
 Ref<Person> &p2, RString a2, bool cargo2, IAssignFunc &assign2)
{
  // switch animation state
  swap(p1,p2);
  if (p1) p1->SwitchAction(a1);
  if (p2) p2->SwitchAction(a2);
  AIBrain *unit1 = p1 ? p1->Brain() : NULL;
  AIBrain *unit2 = p2 ? p2->Brain() : NULL;
  if (unit1) unit1->UnassignVehicle();
  if (unit2) unit2->UnassignVehicle();
  if (unit1)
  {
    if (cargo1)
      unit1->SetInCargo(true);
    else if (unit1->IsInCargo())
      unit1->SetInCargo(false);
    assign1(unit1, vehicle);
  }
  if (unit2)
  {
    if (cargo2)
      unit2->SetInCargo(true);
    else if (unit2->IsInCargo())
      unit2->SetInCargo(false);
    assign2(unit2, vehicle);
  }
  Assert(!unit1 || unit1->VehicleAssigned() == vehicle);
  Assert(!unit2 || unit2->VehicleAssigned() == vehicle);
}

bool Transport::CanMoveToDriver(AIBrain *unit, int currentCompartments) const
{
  if (!Type()->_hasDriver) return false; // no driver position
  if (_driverLocked) return false; // locked driver position
  if (unit == PilotUnit()) return false; // already in driver position
  if (PilotUnit() && PilotUnit()->IsAnyPlayer()) return false; // driver position occupied by a player
  if ((Type()->_driverCompartments & currentCompartments) == 0) return false; // different compartment
  // check whether driver animation is available
  Person *person = unit->GetPerson();
  if (person)
  {
    if (!person->IsAction(DriverAction()))
      return false;
  }

  return true;
}

bool Transport::CanMoveToTurret(TurretContextV &context, AIBrain *unit, int currentCompartments) const
{
  if (!context._turret) return false; // no such turret
  if (context._turret->IsGunnerLocked()) return false; // no locked gunner
  AIBrain *gunnerUnit = context._gunner ? context._gunner->Brain() : NULL;
  if (unit == gunnerUnit) return false; // already in gunner position
  if (gunnerUnit && gunnerUnit->IsAnyPlayer()) return false; // gunner position occupied by a player
  if ((context._turretType->_gunnerCompartments & currentCompartments) == 0) return false; // different compartment
  // check whether gunner animation is available
  Person *person = unit->GetPerson();
  if (person)
  {
    if (!person->IsAction(context._turret->GunnerAction(*context._turretVisualState, *context._turretType)))
      return false;
  }

  return true;
}

bool Transport::CanMoveToCargo(int index, AIBrain *unit, int currentCompartments) const
{
  AIBrain *cargoUnit = _manCargo[index] ? _manCargo[index]->Brain() : NULL;
  if (unit == cargoUnit) return false; // already in this cargo position
  if (_manCargo.IsLocked(index)) return false; // locked cargo position
  if (cargoUnit && cargoUnit->IsAnyPlayer()) return false; // cargo position occupied by a player
  if ((GetCargoCompartments(index) & currentCompartments) == 0) return false; // different compartment
  // check whether cargo animation is available
  Person *person = unit->GetPerson();
  if (person)
  {
    if (!person->IsAction(CargoAction(index)))
      return false;
  }

  return true;
}

bool Transport::CanChangePosition(AIBrain *unit) const
{
  // if there is no effective commander, we may still be able to change position
  AIBrain *commander = CommanderUnit();

  bool canChangePosition = !commander || unit == commander;
  if (!canChangePosition && GWorld->GetMode() == GModeNetware)
  {
    // special conditions for player in MP
    if (unit->IsAnyPlayer())
      canChangePosition = !_effCommander || !_effCommander->Brain() || !_effCommander->Brain()->IsAnyPlayer();
  }

  if (!canChangePosition) return false;

  bool canHideGunner = true;
  TurretContext context;
  if (FindTurret(unit->GetPerson(), context) && context._turret)
    canHideGunner = context._turretType->_canHideGunner;
  return (!Type()->_hideProxyInCombat || (IsPersonHidden(FutureVisualState(), unit->GetPerson()) || !canHideGunner) || unit->IsInCargo());
}

/*!
\patch 5117 Date 1/11/2007 by Ondra
- Fixed: Vehicle side was assumed incorrectly when player switched into a crew position from cargo.
\patch 5140 Date 3/15/2007 by Jirka
- Fixed: Vehicle commanding - after player switched to driver and then to commander, driver did not follow orders sometimes
*/

void Transport::ChangePosition(const Action *action, Person *soldier, bool netAware, bool forced)
{
#if DIAG_CREW
  LogF("### Change position in %s - implementation", cc_cast(GetDebugName()));
#endif

  VisualState& vs = unconst_cast(FutureVisualState());

  AIBrain *unit = soldier->Brain();
  // ??? Can be Transport::CanChangePosition used here ???
  AIBrain *commander = CommanderUnit();
  bool canChangePosition = !commander || unit == commander || forced;
  if (!canChangePosition && GWorld->GetMode() == GModeNetware)
  {
    // special conditions for player in MP
    if (unit && unit->IsAnyPlayer())
      canChangePosition = !_effCommander || !_effCommander->Brain() || !_effCommander->Brain()->IsAnyPlayer();
  }
#if _VBS2
  canChangePosition = true;
  //convoy trainer
  if(soldier->IsPersonalItemsEnabled())
    soldier->SetEnablePersonalItems(false);
#endif

  if (!canChangePosition)
  {
#if DIAG_CREW
    LogF(" not enabled");
#endif
    return;
  }

  // check current unit position
  bool unitCargo = false;
  // assign functors
  AssignFuncDriver assignAsDriver;
  AssignFuncCargo assignAsCargo;
  AssignFuncTurret assignToTurret;

  IAssignFunc *unitAssign = NULL;
  Ref<Person> *unitPos = NULL;
  RString unitAct;

  if (soldier == _driver)
  {
    unitPos = &_driver;
    unitAct = DriverAction();
    unitAssign = &assignAsDriver;
    if (_driver->Brain() == EffectiveObserverUnit())
      _manualFire = false;
    // driver is changing, cancel current command
    _moveMode = VMMFormation;
  }
  else
  {
    TurretContextV context;
    if (FindTurret(vs, soldier, context) && context._turret)
    {
      unitPos = &context._turret->_gunner;
      unitAct = context._turret->GunnerAction(*context._turretVisualState, *context._turretType);
      assignToTurret.SetTurret(context._turret);
      unitAssign = &assignToTurret;
      if (context._turret->_gunner->Brain() == EffectiveObserverUnit())
        _manualFire = false;
    }
    else
    {
      // find if cargo
      for (int i=0; i<_manCargo.Size(); i++)
      {
        if (soldier==_manCargo[i])
        {
          unitPos = &_manCargo.Set(i).man;
          unitAct = CargoAction(i);
          assignAsCargo.SetIndex(i);
          unitAssign = &assignAsCargo;
          unitCargo = true;
          break;
        }
      }
    }
  }
  if (!unitPos)
  {
#if DIAG_CREW
    LogF(" original position not found");
#endif
    return;
  }
  switch (action->GetType())
  {
    case ATMoveToDriver:
    case ATMoveToPilot:
      if (!(_driver && _driver->Brain() && _driver->Brain()->IsAnyPlayer()))
      {
        SwapPositions(this,
          _driver,DriverAction(),false,assignAsDriver,
          *unitPos,unitAct,unitCargo,*unitAssign);
        // driver is changing, cancel current command
        _moveMode = VMMFormation;
      }
      else
      {
#if DIAG_CREW
        LogF(" target position occupied");
#endif
      }
      break;
    case ATMoveToGunner:
      {
        Ref<Person> *unitPos2 = NULL;
        RString unitAct2;
        TurretContextV context;
        if (GetPrimaryGunnerTurret(vs, context) && context._turret)
        {
          unitPos2 = &context._turret->_gunner;
          unitAct2 = context._turret->GunnerAction(*context._turretVisualState, *context._turretType);
        }
        if (!unitPos2)
        {
  #if DIAG_CREW
          LogF(" target position not found");
  #endif
          return;
        }
        Person *gunner = *unitPos2;
        if (!(gunner && gunner->Brain() && gunner->Brain()->IsAnyPlayer()))
        {
          AssignFuncTurret assignAsGunner(context._turret);
          SwapPositions(this,
            *unitPos2, unitAct2, false, assignAsGunner,
            *unitPos, unitAct, unitCargo, *unitAssign);
        }
        else
        {
  #if DIAG_CREW
          LogF(" target position occupied");
  #endif
        }
      }
      break;
    case ATMoveToCommander:
      {
        Ref<Person> *unitPos2 = NULL;
        RString unitAct2;
        TurretContextV context;
        if (GetPrimaryObserverTurret(vs, context) && context._turret)
        {
          unitPos2 = &context._turret->_gunner;
          unitAct2 = context._turret->GunnerAction(*context._turretVisualState, *context._turretType);
        }
        if (!unitPos2)
        {
  #if DIAG_CREW
          LogF(" target position not found");
  #endif
          return;
        }
        Person *commander = *unitPos2;
        if (!(commander && commander->Brain() && commander->Brain()->IsAnyPlayer()))
        {
          AssignFuncTurret assignAsCommander(context._turret);
          SwapPositions(this,
            *unitPos2, unitAct2, false, assignAsCommander,
            *unitPos, unitAct, unitCargo, *unitAssign);
        }
        else
        {
  #if DIAG_CREW
          LogF(" target position occupied");
  #endif
        }
      }
      break;
    case ATMoveToCargo:
      if (_manCargo.Size()>0)
      {
        const ActionIndex *actionIndex = static_cast<const ActionIndex *>(action);
        int index = actionIndex->GetIndex();
        if (index >= 0 && index < _manCargo.Size())
        {
          AssignFuncCargo assignAsCargo(index);
          SwapPositions(this,
            _manCargo.Set(index).man,CargoAction(index),true,assignAsCargo,
            *unitPos,unitAct,unitCargo,*unitAssign);
        }
        else
        {
  #if DIAG_CREW
          LogF(" target position occupied");
  #endif
        }
      }
      break;
    case ATMoveToTurret:
      {
        const ActionTurret *actionTurret = static_cast<const ActionTurret *>(action);
        Turret *turret = actionTurret->GetTurret();
        TurretContextV context;
        if (turret && FindTurret(vs, turret, context))
        {
          if (!(turret->_gunner && turret->_gunner->Brain() && turret->_gunner->Brain()->IsAnyPlayer()))
          {
            AssignFuncTurret assignToTurret(turret);
            SwapPositions(this,
              turret->_gunner, turret->GunnerAction(*context._turretVisualState, *context._turretType), false, assignToTurret,
              *unitPos, unitAct, unitCargo, *unitAssign);
          }
          else
          {
  #if DIAG_CREW
            LogF(" target position occupied");
  #endif
          }
        }
        else
        {
  #if DIAG_CREW
          LogF(" target position not found");
  #endif
          return;
        }
      }
      break;
  }
  if (unit && unit == GWorld->FocusOn())
  {
    // camera is attached to a soldier which is changing a position
    // reset settings as necessary
    GWorld->SwitchCameraTo(this,GWorld->GetCameraType(), false);
    GWorld->UI()->ResetVehicle(this);
    // we may need to select a weapon
    if (QIsManual(unit))
    {
      TurretContext context;
      if (FindTurret(unit->GetPerson(), context))
        AutoselectWeapon(unit->GetPerson());
    }
  }
}

/// implement ClassTypeInfo for ActivityGetOut
class ActivityClassTypeInfoGetOut: public ActivityClassTypeInfo
{
  typedef ActivityClassTypeInfo base;

public:
  ActivityClassTypeInfoGetOut():base("GetOut"){}
  //@{ implement ActivityClassTypeInfo
  virtual Activity *CreateObject(ParamArchive &ar) const {return NULL;}
  virtual Activity *LoadRef(ParamArchive &ar) const {return NULL;}
  //@}
};

#include <Es/Memory/normalNew.hpp>

/// get out of the vehicle
class ActivityGetOut: public EntityActivity
{
  typedef EntityActivity base;

  static ActivityClassTypeInfoGetOut _typeInfo;

  OLinkPermNO(AIBrain) _unit;
  float _fadeOut;
  /// when ejecting, we do not switch the engine off
  bool _eject;

public:
  ActivityGetOut(AIBrain *unit, bool eject);

  //@{ implement Activity
  virtual void OnEvent(ActivityEvent *event){}
  virtual const ActivityClassTypeInfo &GetTypeInfo() const {return _typeInfo;}
  virtual LSError Serialize(ParamArchive &ar)
  {
    CHECK(ar.Serialize("eject",_eject,0,false));
    return LSOK;
  }
  //@}

  //@{ implementation of EntityActivity
  virtual bool Simulate(Entity *entity, float deltaT, SimulationImportance prec);
  virtual void Draw(Entity *entity);
  //@}

  //@{ When activity is to be added, try to find the same prior activity and update its eject
  const AIBrain *GetUnit() const { return _unit; }
  void SetEject(bool eject) { _eject = eject; }
  static const ActivityClassTypeInfo &GetOutTypeInfo() {return _typeInfo;}
  //@}

  USE_FAST_ALLOCATOR
};

ActivityClassTypeInfoGetOut ActivityGetOut::_typeInfo;

CameraType ValidateCamera(CameraType cam);


#include <Es/Memory/debugNew.hpp>

DEFINE_FAST_ALLOCATOR(ActivityGetOut)

ActivityGetOut::ActivityGetOut(AIBrain *unit, bool eject)
:_unit(unit),_fadeOut(0),_eject(eject)
{
}

bool ActivityGetOut::Simulate(Entity *entity, float deltaT, SimulationImportance prec)
{
  // before we can start getting out, we want the vehicle to open doors for us
  if (_unit)
  {
    Transport *trans = dyn_cast<Transport>(entity);
    if (trans && !trans->PrepareGetOut(_unit))
    {
      // if door opening in progress, wait
      return true;
    }
  }

  if (_unit && GWorld->FocusOn() == _unit)
  {
    _fadeOut += deltaT*3;
    Person *person = _unit->GetPerson();

    person->PreloadView(person,ValidateCamera(GWorld->GetCameraType()));
    // preload LOD and texture for external viewing
    LODShapeWithShadow *shape = entity->GetShape();
    if (shape)
    {
      if (shape->CheckLevelLoaded(0,true))
      {
        ShapeUsed lock = shape->Level(0);
        lock->PreloadTextures(0, NULL);
      }
    }
  }
  else
  {
    _fadeOut = 1;
  }
  if (_fadeOut>=1)
  {
    if (_unit)
    {
      _unit->ProcessGetOut(_eject,false);
      Transport *trans = dyn_cast<Transport>(entity);
      if (trans)
      {
        trans->FinishGetOut(_unit);
      }

    }
#if 0 // _ENABLE_CHEATS
    if (_unit && GWorld->FocusOn() == _unit)
    {
      extern bool StopLoadingTextures;
      StopLoadingTextures = true;
    }
#endif
    return false; // terminate immediatelly
  }
  return true;
}

void ActivityGetOut::Draw(Entity *entity)
{
  if (_unit && GWorld->FocusOn() == _unit)
  {
    GEngine->Draw2DWholeScreen(PackedBlack,_fadeOut);
  }
}

void Transport::StartGetOutActivity(AIBrain *unit, bool eject)
{
  // Check whether other GetOutActivity is pending. If so, change only its parameters (eject).
  for (int i=0; i<_activities.Size(); i++)
  {
    if ( &_activities[i]->GetTypeInfo() == &ActivityGetOut::GetOutTypeInfo() )
    {
      ActivityGetOut *getOut = static_cast<ActivityGetOut *>(_activities[i].GetRef());
      if (getOut->GetUnit()==unit)
      {
        getOut->SetEject(eject);
        return; // found: no new GetOut activity needed
      }
    }
  }
  ActivityGetOut *act = new ActivityGetOut(unit,eject);
  _activities.Add(act);
}

void Transport::PerformAction(const Action *action, AIBrain *unit)
{
  // check if we are able to handle the action
  switch (action->GetType())
  {
#if _VBS2 // interact with vehs
    case ATInteractWithVehicle:
      {
        ConstParamEntryPtr userActions = Pars.FindEntry("CfgActions");
        if(!userActions) break;

        ConstParamEntryPtr entry = userActions->FindEntry("InteractWithVehicle");
        if (!entry) break;

        if (entry->FindEntry("statement"))
        {
          RString statement = *entry >> "statement";
          GameState *gstate = GWorld->GetGameState();
          gstate->VarSet("this", GameValueExt(this), true, false, GWorld->GetMissionNamespace());
          gstate->Execute(statement, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
        }
      }
      return;
    case ATQuickEnterVehicle:
      {
        {
          ConstParamEntryPtr userActions = Pars.FindEntry("CfgActions");
          if(!userActions) break;

          ConstParamEntryPtr entry = userActions->FindEntry("QuickEnterVehicle");
          if (!entry) break;

          if (entry->FindEntry("statement"))
          {
            RString statement = *entry >> "statement";
            GameState *gstate = GWorld->GetGameState();
            gstate->VarSet("this", GameValueExt(this), true, false, GWorld->GetMissionNamespace());
            gstate->Execute(statement, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
          }
        }
      }
      return;
    case ATTogglePersonalItems: //Convoy trainer
      {
        Person* person = unit->GetPerson();
        if(person)
        {
          bool isEnabled = person->IsPersonalItemsEnabled();
          RString anim;
          Vector3 offset;
          person->GetPersonalItemsParams(anim, offset);
          person->SetEnablePersonalItems(!isEnabled, anim, offset);
          if (GWorld->GetMode() == GModeNetware)
            GetNetworkManager().EnablePersonalItems(person,!isEnabled, anim, offset);
        }
      }
      return;
#endif
    case ATMoveToDriver:
    case ATMoveToPilot:
    case ATMoveToGunner:
    case ATMoveToCommander:
    case ATMoveToCargo:
    case ATMoveToTurret:
      {
        Turret *fromTurret = NULL;
        TurretContext context;
        if (FindTurret(unit->GetPerson(), context))
          fromTurret = context._turret;

        Turret *toTurret = NULL;
        if (action->GetType() == ATMoveToTurret)
        {
          const ActionTurret *actionTurret = static_cast<const ActionTurret *>(action);
          toTurret = actionTurret->GetTurret();
        }
        else if (action->GetType() == ATMoveToGunner)
        {
          if (GetPrimaryGunnerTurret(context))
            toTurret = context._turret;
        }
        else if (action->GetType() == ATMoveToCommander)
        {
          if (GetPrimaryObserverTurret(context))
            toTurret = context._turret;
        }

#if DIAG_CREW
        LogF("### Change position in %s - action", cc_cast(GetDebugName()));
#endif
        if (fromTurret && toTurret)
        {
#if DIAG_CREW
          RString fromName, toName;
          TurretContext context;
          if (FindTurret(fromTurret, context) && context._turretType) fromName = context._turretType->_gunnerName;
          if (FindTurret(toTurret, context) && context._turretType) toName = context._turretType->_gunnerName;
          LogF(" from turret '%s'", cc_cast(fromName));
          LogF(" to turret '%s'", cc_cast(toName));
#endif
          if (!fromTurret->IsLocal() && toTurret->Gunner())
          {
#if DIAG_CREW
            LogF(" sent to turret '%s'", cc_cast(fromName));
#endif
            GetNetworkManager().AskForChangePosition(toTurret->Gunner(), this, ATMoveToTurret, fromTurret, -1);
          }
          if (!toTurret->IsLocal() && fromTurret->Gunner())
          {
#if DIAG_CREW
            LogF(" sent to turret '%s'", cc_cast(toName));
#endif
            GetNetworkManager().AskForChangePosition(fromTurret->Gunner(), this, ATMoveToTurret, toTurret, -1);
          }
          if (fromTurret->IsLocal() || toTurret->IsLocal())
          {
#if DIAG_CREW
            LogF(" processed locally");
#endif
            ChangePosition(action, unit->GetPerson());
          }
        }
        else if (fromTurret)
        {
#if DIAG_CREW
          RString fromName;
          TurretContext context;
          if (FindTurret(fromTurret, context) && context._turretType) fromName = context._turretType->_gunnerName;
          LogF(" from turret '%s'", cc_cast(fromName));
#endif
          if (!fromTurret->IsLocal())
          {
            Person *person = NULL;
            if (action->GetType() == ATMoveToDriver || action->GetType() == ATMoveToPilot)
            {
              person = _driver;
#if DIAG_CREW
              LogF(" to driver");
#endif
            }
            else if (action->GetType() == ATMoveToCargo)
            {
              const ActionIndex *actionIndex = static_cast<const ActionIndex *>(action);
              int index = actionIndex->GetIndex();
              if (index >= 0 && index < _manCargo.Size())
                person = _manCargo[index];
#if DIAG_CREW
              LogF(" to cargo");
#endif
            }
            if (person)
            {
#if DIAG_CREW
              LogF(" sent to turret '%s'", cc_cast(fromName));
#endif
              GetNetworkManager().AskForChangePosition(person, this, ATMoveToTurret, fromTurret, -1);
            }
          }
          else
          {
#if DIAG_CREW
            LogF(" to local vehicle");
#endif
          }
          if (!IsLocal() && fromTurret->Gunner())
          {
#if DIAG_CREW
            LogF(" sent to vehicle");
#endif
            int index = -1;
            if (action->GetType() == ATMoveToCargo)
            {
              const ActionIndex *actionIndex = static_cast<const ActionIndex *>(action);
              index = actionIndex->GetIndex();
            }
            GetNetworkManager().AskForChangePosition(fromTurret->Gunner(), this, action->GetType(), NULL, index);
          }
          if (fromTurret->IsLocal() || IsLocal())
          {
#if DIAG_CREW
            LogF(" processed locally");
#endif
            ChangePosition(action, unit->GetPerson());
          }
        }
        else if (toTurret)
        {
#if DIAG_CREW
          if (unit->GetPerson() == Driver())
            LogF(" from driver");
          else
            LogF(" from cargo");
          RString toName;
          TurretContext context;
          if (FindTurret(toTurret, context) && context._turretType) toName = context._turretType->_gunnerName;
          LogF(" to turret '%s'", cc_cast(toName));
#endif
          if (!IsLocal() && toTurret->Gunner())
          {
#if DIAG_CREW
            LogF(" sent to vehicle");
#endif
            UIActionType type = ATNone;
            int index = -1;
            if (unit->GetPerson() == Driver())
            {
              if (GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
                type = ATMoveToPilot;
              else
                type = ATMoveToDriver;
            }
            else
            {
              // search in cargo
              for (int i=0; i<_manCargo.Size(); i++)
              {
                if (unit->GetPerson() == _manCargo[i])
                {
                  type = ATMoveToCargo;
                  index = i;
                  break;
                }
              }
            }
            GetNetworkManager().AskForChangePosition(toTurret->Gunner(), this, type, NULL, index);
          }
          if (!toTurret->IsLocal())
          {
#if DIAG_CREW
            LogF(" sent to turret '%s'", cc_cast(toName));
#endif
            GetNetworkManager().AskForChangePosition(unit->GetPerson(), this, ATMoveToTurret, toTurret, -1);
          }
          if (IsLocal() || toTurret->IsLocal())
          {
#if DIAG_CREW
            LogF(" processed locally");
#endif
            ChangePosition(action, unit->GetPerson());
          }
        }
        else
        {
#if DIAG_CREW
          LogF(" inside vehicle");
#endif
          if (IsLocal())
          {
#if DIAG_CREW
            LogF(" processed locally");
#endif
            ChangePosition(action, unit->GetPerson());
          }
          else
          {
#if DIAG_CREW
            LogF(" sent to vehicle");
#endif
            int index = -1;
            if (action->GetType() == ATMoveToCargo)
            {
              const ActionIndex *actionIndex = static_cast<const ActionIndex *>(action);
              index = actionIndex->GetIndex();
            }
            GetNetworkManager().AskForChangePosition(unit->GetPerson(), this, action->GetType(), NULL, index);
          }
        }
      }
      return;
    case ATManualFire:
      _manualFire = true;
      // make sure some weapon is selected
      {
        Person *gunner = GunnerUnit() ? GunnerUnit()->GetPerson() : NULL;
        AutoselectWeapon(gunner);
        SendSimpleCommand(gunner, SCManualFire);
      }
      return;
    case ATManualFireCancel:
      _manualFire = false;
      {
        Person *gunner = GunnerUnit() ? GunnerUnit()->GetPerson() : NULL;
        SendSimpleCommand(gunner, SCCancelManualFire);
      }
      return;
    case ATArtilleryComputer:
      {
        AbstractOptionsUI *CreateArtilleryComputerDialog(bool userDialog);
        GWorld->DestroyUserDialog();
        GWorld->SetUserDialog(CreateArtilleryComputerDialog(true));
      }
      return;
    case ATGetOut:
      // perform an activity which will getout given soldier
      StartGetOutActivity(unit,false);
      return;
    case ATEngineOn:
      EngineOnAction();
      return;
    case ATEngineOff:
      EngineOffAction();
      return;
    case ATEject:
      if (IsStopped())
        // if stopped, get out action is available - use get out, but do not switch off the enginestill keep the engine on
        StartGetOutActivity(unit,true);
      else
        Eject(unit);
      return;
    case ATLand:
      Land();
      return;
    case ATCancelLand:
      CancelLand();
      return;
    case ATTurnIn:
      {
#if _VBS2 //Convoy trainer
        Person* person = unit->GetPerson();
        if(person->IsPersonalItemsEnabled())
          person->SetEnablePersonalItems(false);
# if _VBS3
        if (GWorld->GetMode() == GModeNetware)
          GetNetworkManager().EnablePersonalItems(person, false);
# endif
#endif
        HidePerson(unit->GetPerson(), 1.0);
      }
      return;
    case ATTurnOut:
      {
#if _VBS2 //Convoy trainer
        Person* person = unit->GetPerson();
        if(person)
        {
          //default to personal items?
          TurretContext context;
          if(FindTurret(person, context) && context._turret && context._turretType->_defaultToPersonalItems)
          {
            person->SetEnablePersonalItems(true, context._turretType->_personalItemsAction
              , context._turretType->_personalItemsOffset);
            if (GWorld->GetMode() == GModeNetware)
              GetNetworkManager().EnablePersonalItems(person, true, context._turretType->_personalItemsAction, context._turretType->_personalItemsOffset);
          }
        }
#endif
        HidePerson(unit->GetPerson(), 0.0);
        if(GWorld->GetCameraType() == CamGunner) GWorld->SetCameraType(CamInternal);
      }
      return;
    case ATGear:
      // gear action inside the vehicle
#if _VBS3_INVENTORY
      if(!action->PerformScripted(unit))
#endif
      {
        AbstractOptionsUI *CreateGearSupplyDialog(AIBrain *unit, EntityAI *supply);
        GWorld->DestroyUserDialog();
        GWorld->SetUserDialog(CreateGearSupplyDialog(unit, this));
      }
      return;
  }
  // fall-back to parent
  base::PerformAction(action,unit);
}

int Transport::GetCargoCompartments(int index) const
{
  Assert(index >= 0);
  const TransportType *type = Type();
  int n = type->_cargoCompartments.Size();
  if (n <= 0) return 0;
  if (index >= n) return type->_cargoCompartments[n - 1];
  return type->_cargoCompartments[index];
}

int Transport::GetPersonCompartments(Person *person) const
{
  const TransportType *type = Type();
  if (_driver == person) return type->_driverCompartments;
  TurretContext context;
  if (FindTurret(person, context))
  {
    if (context._turretType) return context._turretType->_gunnerCompartments;
    return 0;
  }
  for (int i=0; i<_manCargo.Size(); i++)
  {
    if (person == _manCargo[i]) return GetCargoCompartments(i);
  }
  return 0;
}

/// Functor searching for dead flag owner in turrets
class FindFlagOwner : public ITurretFunc
{
protected:
  Person *&_flagOwner; // out

public:
  FindFlagOwner(Person *&flagOwner) : _flagOwner(flagOwner) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if (context._gunner && context._gunner->IsDamageDestroyed() && context._gunner->GetFlagTexture())
    {
      _flagOwner = context._gunner;
      return true; // found
    }
    return false; // continue
  }
};

/// Functor collecting actions MoveToTurret
class GetMoveToTurretActions : public ITurretFunc
{
protected:
  UIActions &_actions; // out
  AIBrain *_unit;
  int _compartments;

public:
  GetMoveToTurretActions(UIActions &actions, AIBrain *unit, int compartments)
    : _actions(actions), _unit(unit), _compartments(compartments) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    AIBrain *unit = context._gunner ? context._gunner->Brain() : NULL;
    if (
      _unit != unit &&
      !(unit && unit->IsAnyPlayer()) &&
      (context._turretType->_gunnerCompartments & _compartments) != 0)
    {
      UIAction action(new ActionTurret(ATMoveToTurret, entity, context._turret));
      _actions.Add(action);
    }
    return false; // continue
  }
};

/// Functor checking if there is some enemy person in the vehicle
class IsEnemyFunc : public ITurretFunc
{
protected:
  AIBrain *_brain;

public:
  IsEnemyFunc(AIBrain *brain) : _brain(brain) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false;
    if (!context._gunner) return false;
    return _brain->IsEnemy(context._gunner->GetTargetSide(1));
  }
};

/// Functor searching the place where commander is usually sitting
class FindCommanderPlace : public ITurretFunc
{
protected:
  Person *&_commander;
  float _maxCommanding;

public:
  FindCommanderPlace(Person *&commander)
    : _commander(commander) {_maxCommanding = 0;}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turretType) return false; // continue
    if (context._turretType->_commanding > _maxCommanding)
    {
      _commander = context._gunner;
      _maxCommanding = context._turretType->_commanding;
    }
    return false; // check all turrets
  }
};

/*!
\patch 1.27 Date 10/17/2001 by Jirka
- Fixed: Manual fire in MP enabled if gunner is not player
*/

void Transport::GetActions(UIActions &actions, AIBrain *unit, bool now, bool strict)
{
  base::GetActions(actions, unit, now, strict);
  if (!unit)
    return;

#if _VBS2 // interact with vehs
  if (GWorld->IsIWVActive() && unit->IsPlayer()
    && (unit->GetVehicleIn() == this || unit->IsFreeSoldier())
    && !IsDestroyed())
  {
    bool allowIWV = true;

    Person *person = unit->GetPerson();
    Assert(person);

    // avoid get in enemy vehicle
    // TargetSide(float) for Person returns Civilian if they are captive
    // TargetSide for brain always returns the real side
    if(!unit->GetCaptive())
    {
      if (Driver() && unit->IsEnemy(Driver()->GetTargetSide(1))) allowIWV = false;
      IsEnemyFunc func(unit);
      if (allowIWV && ForEachTurret(func)) allowIWV = false;
      if (allowIWV)
        for (int i=0; i<GetManCargo().Size(); i++)
        {
          Person *man = GetManCargo()[i];
          if (!man) continue;
          if (unit->IsEnemy(man->GetTargetSide(1))) {allowIWV = false; break;}
        }
    }
    if (allowIWV && unit->IsFreeSoldier())
    {
      // check if stopped
      if (Speed().SquareSize() > Square(1.5)) allowIWV = false;

      // check if in front
      Vector3Val relPos = person->RenderVisualStateScope(true)->PositionWorldToModel(Position());
      if (relPos.Z() <= 0) allowIWV = false;

      Vector3 minMax[2];
      ClippingInfo(minMax,ClipGeometry);
      minMax[0] *= 1.3; minMax[1] *= 1.3; //increase Size


      //avoid going under ground with unit position, using ComPosition instead of position
      Vector3 RelPos = RenderVisualStateScope(true)->PositionWorldToModel(unit->COMPosition());

      if( RelPos.X() < minMax[0].X() || RelPos.X() > minMax[1].X()
        || RelPos.Y() < minMax[0].Y() || RelPos.Y() > minMax[1].Y()
        ||RelPos.Z() < minMax[0].Z() || RelPos.Z() > minMax[1].Z()
        )
        allowIWV = false;
    }

    // check lock
    if (allowIWV && (unit->GetVehicleIn() == this || _lock < LSLocked))
    {
      UIAction action(new ActionBasic(ATInteractWithVehicle, this));
      actions.Add(action);
      // Add quick enter action for vehicles
      UIAction quickEnterAction(new ActionBasic(ATQuickEnterVehicle, this));
      actions.Add(quickEnterAction);
    }
  }
  //Enable Personal Items for Convoy trainer

  if (unit->IsPlayer() && unit->GetVehicleIn() == this
    && !IsDestroyed()
    && !GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)) //Air vehicle gunners are turned out by default, TODO: might look for a cleaner way
    )
  {
    Person* person = unit->GetPerson();
    if(person)
    {
      bool addToggleAction = false;
      TurretContext context;
      bool inTurret = FindTurret(person, context);

      //are we allowed to toggle?
      if(!(inTurret && context._turretType && context._turretType->_disablePersonalItemsToggle))
      {
        if(person->IsPersonalItemsEnabled())
        {
          //don't add action if turret is locked to personalItems
          addToggleAction = true;
        }
        else //PersonalItems not enabled
        {
          if(inTurret && context._turret && !context._turret->IsGunnerHidden())
            addToggleAction = true;
        }
      }

      if(addToggleAction)
      {
        UIAction action(new ActionBasic(ATTogglePersonalItems, this));
        actions.Add(action);
      }
    }
  }
#endif

  if (unit->IsFreeSoldier())
  {
    if (unit->IsPlayer() && _lock == LSLocked) return;
    bool leader = _lock == LSUnlocked || unit->IsGroupLeader() || GWorld->GetMode() == GModeNetware;
    if (!leader && unit->VehicleAssigned() != this) return;

    // search for flag (test all crew)
    Person *flagOwner = NULL;
    if (Driver() && Driver()->IsDamageDestroyed() && Driver()->GetFlagTexture())
      flagOwner = Driver();
    else
    {
      FindFlagOwner func(flagOwner);
      if (!ForEachTurret(func))
      {
        // search in cargo
        for (int i=0; i<GetManCargo().Size(); i++)
        {
          Person *man = GetManCargo()[i];
          if (man && man->IsDamageDestroyed() && man->GetFlagTexture())
          {
            flagOwner = man;
            break;
          }
        }
      }
    }

    if (flagOwner)
    {
      EntityAI *flagCarrier = flagOwner->GetFlagCarrier();

      TargetSide side = flagCarrier->GetFlagSide();
      if (unit->IsEnemy(side))
      {
        if (!unit->GetPerson()->GetFlagCarrier())
        {
          UIAction action(new ActionBasic(ATTakeFlag, flagOwner));
          actions.Add(action);
        }
      }
      else if (unit->IsFriendly(side))
      {
        UIAction action(new ActionBasic(ATReturnFlag, flagOwner));
        actions.Add(action);
      }
    }

    GetGetInActions(actions, unit, now, strict);
  }
  else if (unit->GetVehicleIn() == this)
  {
    bool isPlayer = unit->IsPlayer() || (unit->GetRemoteControlled() && unit->GetRemoteControlled()->Brain() && unit->GetRemoteControlled()->Brain()->IsPlayer());
    if (!isPlayer || GetLock() != LSLocked /*|| unit->VehicleAssigned() != this*/)
    {
      if (IsStopped())
      {
        actions.Add(UIAction(new ActionBasic(ATGetOut, this)));
        if (EngineIsOn())
          actions.Add(UIAction(new ActionBasic(ATEject, this)));
      }
      else
        actions.Add(UIAction(new ActionBasic(ATEject, this)));

      // check if I can change position
      if (CanChangePosition(unit))
      {
        int currentCompartments = GetPersonCompartments(unit->GetPerson());

        if (CanMoveToDriver(unit, currentCompartments))
        {
          UIActionType type = GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)) ? ATMoveToPilot : ATMoveToDriver;
          UIAction action(new ActionBasic(type, this));
          if (Type()->_driverIsCommander)
          {
            // default shortcut
          }
          else
          {
            // no shortcut
            action.shortcut = (UserAction)-1;
          }
          actions.Add(action);
        }

        GetMoveToTurretActions func(actions, unit, currentCompartments);
        ForEachTurret(func);

        if (_manCargo.Size()>0 && !unit->IsInCargo())
        {
          int index = -1;
          // search for empty position
          for (int i=0; i<_manCargo.Size(); i++)
          {
            if(_manCargo.IsLocked(i)) continue;
            Person *man = _manCargo[i];

            if (!man && (GetCargoCompartments(i) & currentCompartments) != 0)
            {
              index = i;
              break;
            }
          }
          // if not found, search for position occupied by a AI
          if (index < 0)
          {
            for (int i=0; i<_manCargo.Size(); i++)
            {
              if(_manCargo.IsLocked(i)) continue;
              Person *man = _manCargo[i];
              if (!man) continue; // checked in the previous cycle
              if (
                (!man->Brain() || !man->Brain()->IsAnyPlayer()) &&
                (GetCargoCompartments(i) & currentCompartments) != 0)
              {
                index = i;
                break;
              }
            }
          }

          if (index >= 0)
          {
            UIAction action(new ActionIndex(ATMoveToCargo, this, index));
            actions.Add(action);
          }
        }
      }
    }
    if (isPlayer)
    {
      if (PilotUnit() == unit && GetFuel()>0)
      {
        if (EngineIsOn())
        {
          UIAction action(new ActionBasic(ATEngineOff, this));
          actions.Add(action);
        }
        else
        {
          UIAction action(new ActionBasic(ATEngineOn, this));
          actions.Add(action);
        }
      }
      if
        (
        // vehicle is supporting manual fire, only commander can manual fire, gunner is not player
        Type()->CanManualFire() && EffectiveObserverUnit() == unit &&
        (!GunnerUnit() || !GunnerUnit()->IsAnyPlayer())
        )
      {
        // commander can manual fire only when sitting on his place
        bool hisPlace = false;
        if (Type()->DriverIsCommander())
          hisPlace = unit->GetPerson() == _driver;
        else
        {
          Person *commander = NULL;
          FindCommanderPlace func(commander);
          ForEachTurret(func);
          hisPlace = unit->GetPerson() == commander;
        }
        if (hisPlace)
        {
          UIAction action(new ActionBasic(IsManualFire() ? ATManualFireCancel : ATManualFire, this));
          action.shortcut = GetManualFireAction();
          actions.Add(action);
        }
      }

      //open artillery computer for player
      if (GunnerUnit() == unit && GetType()->_artilleryScanner && GWorld->IsArtilleryEnabled())
      {
        UIAction action(new ActionBasic(ATArtilleryComputer, this));
        actions.Add(action);
      }

      if (Type()->_hideProxyInCombat)
      {
        Person *person = unit->GetPerson();
        bool turn = false;
        bool out = false;
        if (person == Driver())
        {
          AIBrain *commUnit = CommanderUnit();
          if (
            // if driver is commander, allow hiding
            unit==commUnit ||
            // if there is no commander or commander is not hidden, allow hiding
            (!Type()->_hasObserver || Observer() || !IsPersonHidden(FutureVisualState(), Observer()))
            )
          {
            // if there is a commander and the commander is not hidden, do not allow hiding
            if (unit==commUnit || !commUnit || !IsPersonHidden(FutureVisualState(), commUnit->GetPerson()))
            {
              turn = !Type()->_forceHideDriver;
              out = FutureVisualState().IsDriverHidden();
            }
          }
        }
        else
        {
          TurretContextV context;
          if (FindTurret(unconst_cast(FutureVisualState()), person, context) && context._turret)
          {
            turn = context._turretType->_canHideGunner && !context._turretType->_forceHideGunner;
            out = context._turretVisualState->IsGunnerHidden();
          }
        }

        if (turn)
        {
          if (out)
          {
            UIAction action(new ActionBasic(ATTurnOut, this));
            actions.Add(action);
          }
          else
          {
            UIAction action(new ActionBasic(ATTurnIn, this));
            actions.Add(action);
          }
        }
      }
    }
  }
}

GetInPoint Transport::AnimateGetInPoint(GetInPointIndexVal ptIndex) const
{
  const VisualState &vs = RenderVisualState();
  GetInPoint result;

  // indices of points in the memory LOD
  int posIndex = -1, dirIndex = -1;
  int memory = _shape->FindMemoryLevel();
  if (memory >= 0)
  {
    posIndex = ptIndex._pos;
    dirIndex = ptIndex._dir;
  }

  // animate point if given, use default if not
  if (posIndex >= 0) result._pos = AnimatePoint(vs, memory, posIndex);
  else
  {
    // some reasonable default
    float sizeCoef = _shape->GeometrySphere() * (1.0 / 4);
    result._pos = Vector3(+3 * sizeCoef, +1, -3 * sizeCoef);
  }

  // animate direction if given, use default if not
  result._dir = (dirIndex >= 0) ? AnimatePoint(vs,memory, dirIndex) - result._pos : -result._pos;
  result._dir.Normalize();

  return result;
}

bool CheckGetInPosition(GetInPoint &pt, Person *person, float maxDist2, float &dist2, bool strict)
{
  // check distance
  dist2 = pt._pos.Distance2(person->RenderVisualState().Position());
  if (dist2 > maxDist2) return false;

  if (!strict) return true;

  // check direction
  float cosAngle = pt._dir.DotProduct(person->RenderVisualState().Direction());
  return cosAngle >= H_INVSQRT2;
}

bool Transport::CheckDriverGetInPosition(Person *person, float maxDist2, float &dist2, bool strict) const
{
  GetInPoint pt = GetDriverGetInPos(person, person->RenderVisualState().Position());
  return CheckGetInPosition(pt, person, maxDist2, dist2, strict);
}

bool Transport::CheckCommanderGetInPosition(Person *person, float maxDist2, float &dist2, bool strict) const
{
  GetInPoint pt = GetCommanderGetInPos(person, person->RenderVisualState().Position());
  return CheckGetInPosition(pt, person, maxDist2, dist2, strict);
}

bool Transport::CheckGunnerGetInPosition(Person *person, float maxDist2, float &dist2, bool strict) const
{
  GetInPoint pt = GetGunnerGetInPos(person, person->RenderVisualState().Position());
  return CheckGetInPosition(pt, person, maxDist2, dist2, strict);
}

bool Transport::CheckTurretGetInPosition(Turret *turret, Person *person, float maxDist2, float &dist2, bool strict) const
{
  GetInPoint pt = GetTurretGetInPos(turret, person, person->RenderVisualState().Position());
  return CheckGetInPosition(pt, person, maxDist2, dist2, strict);
}

bool Transport::CheckCargoGetInPosition(int cargoIndex, Person *person, float maxDist2, float &dist2, bool strict) const
{
  GetInPoint pt = GetCargoGetInPos(cargoIndex, person, person->RenderVisualState().Position());
  return CheckGetInPosition(pt, person, maxDist2, dist2, strict);
}

bool Transport::CheckUnitGetInPosition(Person *person, float maxDist2, float &dist2, bool strict) const
{
  GetInPoint pt = GetUnitGetInPos(person->Brain());
  return CheckGetInPosition(pt, person, maxDist2, dist2, strict);
}

/// Functor collecting actions GetInTurret
class GetGetInTurretActionsLeader : public ITurretFunc
{
protected:
  UIActions &_actions; // out
  Person *_person;
  bool _now;
  bool _strict;
  float _maxDist2;
  float _invMaxDist2;

public:
  GetGetInTurretActionsLeader(
    UIActions &actions, Person *person, bool now, bool strict, float maxDist2, float invMaxDist2)
    : _actions(actions), _person(person), _now(now), _strict(strict), _maxDist2(maxDist2), _invMaxDist2(invMaxDist2) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if (!context._turretType->_hasGunner) return false; // continue
    Transport *vehicle = static_cast<Transport *>(entity); // we know where we call it
    if (vehicle->QCanIGetInTurret(context._turret, _person))
    {
      if (_now)
      {
        Object::ProtectedVisualState<const Person::VisualState> pvs = _person->RenderVisualStateScope(true);
        Object::ProtectedVisualState<const Transport::VisualState> rvs = vehicle->RenderVisualStateScope(true);
        // check if near
        float dist2 = 0;
        if (vehicle->CheckTurretGetInPosition(context._turret, _person, _maxDist2, dist2, _strict))
        {
          UIAction action(new ActionTurret(ATGetInTurret, vehicle, context._turret));
          action.position = vehicle->GetTurretGetInPos(context._turret, _person, pvs->Position())._pos;
          action.priority += -0.3 * dist2 * _invMaxDist2;
          action.highlight = _person->IsActionHighlighted(ATGetInTurret, vehicle);
          _actions.Add(action);
        }
      }
      else
      {
        UIAction action(new ActionTurret(ATGetInTurret, vehicle, context._turret));
        action.highlight = _person->IsActionHighlighted(ATGetInTurret, vehicle);
        _actions.Add(action);
      }
    }
    return false; // for all turrets
  }
};

/// Functor collecting actions GetInTurret
class GetGetInTurretActions : public ITurretFunc
{
protected:
  UIActions &_actions; // out
  Person *_person;

public:
  GetGetInTurretActions(UIActions &actions, Person *person)
    : _actions(actions), _person(person) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if (!context._turretType->_hasGunner) return false; // continue
    if (context._turret->GetGunnerAssigned() == _person->Brain())
    {
      Transport *vehicle = static_cast<Transport *>(entity); // we know where we call it
      if (vehicle->QCanIGetInTurret(context._turret, _person))
      {
        UIAction action(new ActionTurret(ATGetInTurret, vehicle, context._turret));
        action.highlight = _person->IsActionHighlighted(ATGetInTurret, vehicle);
        _actions.Add(action);
      }
      return true; // position found
    }
    return false; // continue
  }
};

/*!
\patch 5138 Date 3/8/2007 by Jirka
- Fixed: Getting into vehicles - both player and AI now handle correctly exact cargo position
*/

void Transport::GetGetInActions(UIActions &actions, AIBrain *unit, bool now, bool strict)
{
  bool leader = _lock == LSUnlocked || unit->IsGroupLeader() || GWorld->GetMode() == GModeNetware;
  Person *person = unit->GetPerson();
  Assert(person);

  // avoid get in enemy vehicle
  if (Driver() && unit->IsEnemy(Driver()->GetTargetSide())) return;
  IsEnemyFunc func(unit);
  if (ForEachTurret(func)) return;
  for (int i=0; i<GetManCargo().Size(); i++)
  {
    Person *man = GetManCargo()[i];
    if (!man) continue;
    if (unit->IsEnemy(man->GetTargetSide())) return;
  }

  float maxDist2 = 0;
  float invMaxDist2 = 0;
  if (now)
  {
    // check if stopped
    if (FutureVisualState().Speed().SquareSize() > Square(1.5)) return;

    // check if in front
    {
      const ProtectedVisualState<const VisualState> &rvs = RenderVisualStateScope(true);
      Vector3Val relPos = person->RenderVisualStateScope(true)->PositionWorldToModel(rvs->Position());
      if (relPos.Z() <= 0) return;
    }

    // check if near
    maxDist2 = Square(GetGetInRadius());
    invMaxDist2 = 1.0 / maxDist2;
  }

  float dist2 = 0;
  if (leader)
  {
    if (QCanIGetIn(person))
    {
      UIActionType type = GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)) ? ATGetInPilot : ATGetInDriver;
      if (now)
      {
        ProtectedVisualState<const Person::VisualState> pvs = person->RenderVisualStateScope(true);
        ProtectedVisualState<const VisualState> rvs = RenderVisualStateScope(true);
        // check if near
        if (CheckDriverGetInPosition(person, maxDist2, dist2, strict))
        {
          UIAction action(new ActionBasic(type, this));
          action.position = GetDriverGetInPos(person, pvs->Position())._pos;
          action.priority += -0.3 * dist2 * invMaxDist2;
          action.highlight = person->IsActionHighlighted(type, this);
          actions.Add(action);
        }
      }
      else
      {
        UIAction action(new ActionBasic(type, this));
        action.highlight = person->IsActionHighlighted(type, this);
        actions.Add(action);
      }
    }
    GetGetInTurretActionsLeader func(actions, person, now, strict, maxDist2, invMaxDist2);
    ForEachTurret(func);

    // check the cargo (create a single action only)
    if (now)
    {
      for (int i=0; i<_manCargo.Size(); i++)
      {
        if (QCanIGetInCargo(person, i) != i) continue;
        // check if near

        ProtectedVisualState<const Person::VisualState> pvs = person->RenderVisualStateScope(true);
        ProtectedVisualState<const VisualState> rvs = RenderVisualStateScope(true);

        if (CheckCargoGetInPosition(i, person, maxDist2, dist2, strict))
        {
          UIAction action(new ActionIndex(ATGetInCargo, this, i));
          action.position = GetCargoGetInPos(i, person, pvs->Position())._pos;
          action.priority += -0.3 * dist2 * invMaxDist2;
          action.highlight = person->IsActionHighlighted(ATGetInCargo, this);
          actions.Add(action);
          break;
        }
      }
    }
    else // !now
    {
      int cargoIndex = QCanIGetInCargo(person);
      if (cargoIndex >= 0)
      {
        UIAction action(new ActionIndex(ATGetInCargo, this, cargoIndex));
        action.highlight = person->IsActionHighlighted(ATGetInCargo, this);
        actions.Add(action);
      }
    }
  }
  else
  {
    if (now)
    {
      // check if near
      if (!CheckUnitGetInPosition(person, maxDist2, dist2, strict)) return;
    }
    if (GetDriverAssigned() == unit)
    {
      if (QCanIGetIn(person))
      {
        UIActionType type = GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)) ? ATGetInPilot : ATGetInDriver;
        UIAction action(new ActionBasic(type, this));
        action.highlight = person->IsActionHighlighted(type, this);
        actions.Add(action);
      }
    }
    else
    {
      GetGetInTurretActions func(actions, person);
      if (!ForEachTurret(func))
      {
        // try the cargo
        int index = QCanIGetInCargo(person);
        if (index >= 0)
        {
          UIAction action(new ActionIndex(ATGetInCargo, this, index));
          action.highlight = person->IsActionHighlighted(ATGetInCargo, this);
          actions.Add(action);
        }
      }
    }
  }
}

bool Transport::QCanIGetIn( Person *who ) const
{
#if _VBS3 && _EXT_CTRL
  if( IsExternalControlled() && !AllowExternalControl(EC_AAR))
    return false;
#endif

  if (_driverLocked) return false;
  if (!QCanIBeIn(who)) return false;
  if (!Type()->HasDriver()) return false;

  // check whether driver animation is available
  if (who)
  {
    if (!who->IsAction(DriverAction())) return false;
  }

  return _driver==NULL || _driver->IsDamageDestroyed();
}

bool Transport::QCanIGetInGunner( Person *who ) const
{
  TurretContext context;
  if (!GetPrimaryGunnerTurret(context)) return false;
  if (!context._turret) return false;
  return QCanIGetInTurret(context._turret, who);
}

bool Transport::QCanIGetInCommander( Person *who ) const
{
  TurretContext context;
  if (!GetPrimaryObserverTurret(context)) return false;
  if (!context._turret) return false;
  return QCanIGetInTurret(context._turret, who);
}

bool Transport::QCanIGetInTurret(Turret *turret, Person *who) const
{
#if _VBS3 && _EXT_CTRL
  if( turret->IsExternalControlled() && !turret->AllowExternalControl(EC_AAR))
    return false;
#endif

  if (!turret) return false;
  if (turret->IsGunnerLocked()) return false;

  if (!QCanIBeIn(who)) return false;

  // check whether gunner animation is available
  if (who)
  {
	  TurretContextV context;
    if (FindTurret(unconst_cast(FutureVisualState()), turret, context))
    {
      if (!who->IsAction(turret->GunnerAction(*context._turretVisualState, *context._turretType))) return false;
    }
  }

  return turret->_gunner == NULL || turret->_gunner->IsDamageDestroyed();
}

int Transport::QCanIGetInCargo(Person *who, int index) const
{
  if (!QCanIBeIn(who)) return -1;

  int n = _manCargo.Size();
  // check the given position
  if (index >= 0 && index < n && (!_manCargo[index] || _manCargo[index]->IsDamageDestroyed()))
  {
    if (IsCargoLocked(index)) index = -1;
    // check whether cargo animation is available
    else if (who)
    {
      if (!who->IsAction(CargoAction(index))) index = -1;
    }
    if (index >= 0) return index; // correct position
  }

  // check if not assigned somewhere
  AIBrain *unit = who ? who->Brain() : NULL;
  if (unit)
  {
    for (int i=0; i<n; i++)
    {
      if (GetCargoAssigned(i) == unit)
      {
        if (_manCargo[i] && !_manCargo[i]->IsDamageDestroyed()) break; // occupied
        if (IsCargoLocked(i)) break;
        if (who && !who->IsAction(CargoAction(i))) break; // cargo animation not available
        return i;
      }
    }
  }

  // find the position
  // first check for empty cargo positions
  for (int i=0; i<n; i++)
  {
    if (_manCargo[i]) continue; // occupied
    if (IsCargoLocked(i)) continue;
    if (who && !who->IsAction(CargoAction(i))) continue; // cargo animation not available
    return i;
  }
  // if not found, search who to replace
  for (int i=0; i<n; i++)
  {
    if (!_manCargo[i]) continue; // already checked
    if (!_manCargo[i]->IsDamageDestroyed()) continue; // occupied by alive
    if (IsCargoLocked(i)) continue;
    if (who && !who->IsAction(CargoAction(i))) continue; // cargo animation not available
    return i;
  }
  return -1; // not found
}

/// Functor checking if person can get in turrets
class CanGetInSomeTurret : public ITurretFunc
{
protected:
  Person *_who;
  bool &_free;

public:
  CanGetInSomeTurret(Person *who, bool &free) : _who(who), _free(free) {}

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    Transport *veh = static_cast<Transport *>(entity); // safe here, we know how we called it (+ turret exist)
    return veh->QCanIGetInTurret(context._turret, _who); // end when some position found
  }
};

bool Transport::QCanIGetInAny( Person *who ) const
{
  if (QCanIGetIn(who)) return true;
  if (QCanIGetInCargo(who) >= 0) return true;
  bool free = false;
  CanGetInSomeTurret func(who, free);
  return ForEachTurret(func);
}

void Transport::GetInStarted( AIUnit *unit )
{
  // TODO: BUG Assert failed
  // Assert( _getinUnits.Find(unit)<0 );
  if( _getinUnits.Find(unit)<0 ) _getinUnits.Add(unit);
}
void Transport::GetOutStarted( AIUnit *unit )
{
  Assert( _getoutUnits.Find(unit)<0 );
  if( _getoutUnits.Find(unit)<0 )
  {
    _getoutUnits.Add(unit);
  }
}

void Transport::GetInFinished( AIBrain *unit )
{
  // done/canceled
  for (int i=0; i<_getinUnits.Size(); i++)
  {
    if( unit == _getinUnits[i] )
    {
      _getinUnits.Delete(i);
      break;
    }
  }
  // TODO: Manual GetIn
  //Fail("Getin finished and never started.");

  // avoid too long waiting when player gets in
  if (unit->IsAnyPlayer() && _getinTime > Glob.time + 60.0f)
  {
    // check if not waiting for more players
    bool noPlayers = true;
    for (int i=0; i<_getinUnits.Size(); i++)
    {
      if (_getinUnits[i] && _getinUnits[i]->IsAnyPlayer())
      {
        noPlayers = false; break;
      }
    }
    if (noPlayers) _getinTime = Glob.time + 60.0f;
  }
}
void Transport::GetOutFinished( AIBrain *unit )
{
  // done/canceled
  for (int i=0; i<_getoutUnits.Size(); i++)
  {
    if( unit == _getoutUnits[i] )
    {
      _getoutUnits.Delete(i);
      return;
    }
  }
  // TODO: Manual GetOut
  //Fail("Getout finished and never started.");
}

void Transport::WaitForGetIn( AIUnit *unit)
{
  if (unit->GetVehicle() == this) return;

  AIBrain *brain = PilotUnit();
  if (brain)
  {
    if (brain->GetState() == AIUnit::Stopped)
    {
      // Calculate cost from starting field
      int x = toIntFloor(unit->Position(unit->GetFutureVisualState()).X() * InvLandGrid);
      int z = toIntFloor(unit->Position(unit->GetFutureVisualState()).Z() * InvLandGrid);
      GeographyInfo geogr = GLOB_LAND->GetGeography(x, z);
      float dist = (FutureVisualState().Position() - unit->Position(unit->GetFutureVisualState())).SizeXZ();
      Time time = Glob.time + 2.0 * unit->GetVehicle()->GetBaseCost(geogr, false, true) * dist + 30.0;
      if (time > GetGetInTimeout()) SetGetInTimeout(time);
    }
    else
    {
      SetGetInTimeout(TIME_MAX);
      if (brain->GetState() != AIUnit::Stopping)
        Verify(brain->SetState(AIUnit::Stopping));
    }
  }

  GetInStarted(unit);

#if 0
  LogF
    (
    "%.2f: Vehicle %s, wait for get in %s until %.2f",
    Glob.time.toFloat(),
    (const char *)veh->GetDebugName(),
    (const char *)unit->GetDebugName(),
    time.toFloat()
    );
#endif
}

void Transport::WaitForGetOut(AIUnit *unit)
{
  AIBrain *brain = PilotUnit();

  GetOutStarted(unit);
  if (brain && brain->GetState() != AIUnit::Stopping && brain->GetState() != AIUnit::Stopped)
  {
    Verify(brain->SetState(AIUnit::Stopping));
  }
}

void Transport::LandStarted(LandingMode landing)
{
  AIBrain *brain = PilotUnit();
  if (!brain) return;
  _landing = landing;
  if (landing==LMNone)
  {
    // if there is some stopping in progress, cancel it
    if( brain->GetState()== AIUnit::Stopping)
    {
      brain->OnStepTimedOut();
    }
  }
  else
  {
    // initiating stopping
    if (brain->GetState() != AIUnit::Stopping && brain->GetState() != AIUnit::Stopped)
    {
      Verify(brain->SetState(AIUnit::Stopping));
    }
  }
}

bool Transport::IsStopped() const
{
  AIBrain *unitInto = PilotUnit();
  if (!unitInto ) return true;
  if( !unitInto->IsAnyPlayer() ) //FIX: IsPlayer() condition was not sufficient in MP
  {
    // Stopped may timeout - test for Stopping
    if( unitInto->GetState()== AIUnit::Stopping) return false;
    if( unitInto->GetState()== AIUnit::Stopped) return true;
  }
  return FutureVisualState().Speed().SquareSize()<Square(0.5);
}

bool Transport::CanCancelStop() const
{
  if (_landing != LMNone) return false;
  if (!base::CanCancelStop()) return false;
  if
    (
    WhoIsGettingIn().Count() == 0 &&
    WhoIsGettingOut().Count() == 0
    )
  {
    _getinUnits.Clear();
    _getoutUnits.Clear();
    return true;
  }
  return false;
}

Vector3 Transport::GetStopPosition() const
{
  // predict stop position based on current speed
  float estT = 2.0f;
  return FutureVisualState().Position() + FutureVisualState().Speed() * estT + 0.5 * Square(estT) * FutureVisualState().Acceleration();
}

/*!
\patch_internal 1.27 Date 10/17/2001 by Ondra
- Changed: ResetMovement moved after SetVehicleIn.
Required because new testing of IsInLandscape in ResetMovement.
\patch 1.27 Date 10/17/2001 by Ondra
- Fixed: speed of vehicle not kept after jumping out (ejecting) from vehicle.
\patch 1.82 Date 8/9/2002 by Ondra
- Fixed: MP: Problems with get-in very quickly followed by get-out.
\patch 5089 Date 11/23/2006 by Ondra
- Fixed: AI reaction improved for enemies disembarking vehicles.
\patch 5101 Date 12/12/2006 by Ondra
- New: Headlights are now not switched when player leaves the vehicle.
*/
void Transport::GetOutAny(const Matrix4 &outPos, Person *soldier, bool sound, const char *name, RString getOutAction)
{
  Assert( !soldier->ToDelete() );

  Assert( soldier );
  //soldier->ForgetMove();

  Assert (GWorld->ValidateOutVehicle(soldier));
  DoAssert (ValidateCrew(soldier));

  soldier->VisualCut();
  
#if _VBS2 //convoy trainer
  if(soldier->IsPersonalItemsEnabled())
    soldier->SetEnablePersonalItems(false);
#endif

  if (soldier->IsInLandscape())
    RptF("GetOutAny soldier %s already in landscape", (const char *)soldier->GetDebugName());
#if 0
  LogF("%s moved from %s into landscape", (const char *)soldier->GetDebugName(), (const char *)GetDebugName());
#endif
  if (soldier->IsMoveOutInProgress())
  {
    RptF("%s: Getting out while IsMoveOutInProgress", (const char *)soldier->GetDebugName());
    soldier->CancelMoveOutInProgress();
    soldier->Move(outPos);
    soldier->Init(outPos, false); // do not reset movement
    soldier->OnGetOutFinished();
  }
  else
  {
    soldier->SetTransform(outPos);
    soldier->Init(outPos, false); // do not reset movement
    soldier->ResetMoveOut(); // mark it is in landscape
    GLOB_WORLD->AddVehicle(soldier);
    GLOB_WORLD->RemoveOutVehicle(soldier);
    soldier->OnGetOutFinished();
  }

  // we need other entities nearby to be notified of our presence
  GWorld->OnEntityMovedFar(soldier);

  // there was lock position here, but there is no need to connect locking with getting out
  //LockPosition();

  AIBrain *brain = soldier->Brain();
  if( brain )
  {
    // AI switches the light off when leaving the vehicle
    if (CommanderUnit()==brain && !brain->IsAnyPlayer())
    {
#if _VBS3 && _EXT_CTRL
      if(!GAAR.IsPlaying())
#endif
        if ( _pilotLight )
        {
          _pilotLight = false;
          if ( !IsLocal() )
          {
            GetNetworkManager().AskForPilotLight(this, _pilotLight);
          }
        }
    }
    brain->SetVehicleIn(NULL);
  }
  soldier->ResetMovement(2.5, getOutAction);
  soldier->SetSpeed(FutureVisualState()._speed);

  if (QIsManual())
    DecFadeSemaphore(FadeGetOut);
  // we want to give near units chance of noticing us

  if (!_doorSound && sound)
  {
    const SoundPars &pars=GetType()->GetGetOutSound();

    if (pars.name.GetLength() > 0 && GSoundScene->CanBeAudible(WaveEffect,FutureVisualState().Position(),pars.vol))
    {
      _doorSound=GSoundScene->OpenAndPlayOnce(pars.name,this,true,FutureVisualState().Position(),FutureVisualState().Speed(),pars.vol, pars.freq, pars.distance);
      if( _doorSound )
      {
        GSoundScene->SimulateSpeedOfSound(_doorSound);
        GSoundScene->AddSound(_doorSound);
      }
    }

  }
#if _VBS3 //always call event
  OnEvent(EEGetOut,name,soldier);
  soldier->OnEvent(EEGetOutMan,name,this);
#else
  if (sound) OnEvent(EEGetOut,name,soldier);
#endif
  /*
  if (!CommanderUnit() || CommanderUnit()->IsInCargo())
  {
  // empty vehicle - leave light status as it is
  // _pilotLight = false;
  }
  */

  if (_effCommander == soldier) _effCommander = NULL;
}


void Transport::GetInAny(Person *soldier, bool sound, const char *name)
{
  // check if soldier is in some vehicle
  // if yes, get him out before gettin in "this"
  AIBrain *unit=soldier->Brain();
  if (unit)
  {
    Transport *in = unit->GetVehicleIn();
    Assert (in!=this);
    if (in)
    {
      Assert(IsLocal());
      unit->DoGetOut(in,false,false);
#if 0
      LogF
        (
        "Unit %s forced out from %s",
        (const char *)unit->GetDebugName(),
        (const char *)GetDebugName()
        );
#endif
    }
  }

#if _VBS2 //convoy trainer
  //Assert(soldier->IsPersonalItemsEnabled());
#endif

  soldier->UnlockPosition();
  if (soldier->IsInLandscape())
  {
    soldier->SetMoveOut(this); // remove reference from the world
  }
  else
  {
    Assert(!soldier->IsMoveOutInProgress());
    Assert(soldier->GetHierachyParent()==this);
  }
#if 0
  LogF
    (
    "%s will move into %s",
    (const char *)soldier->GetDebugName(),
    (const char *)GetDebugName()
    );
#endif
  soldier->SetPilotLight(false);
  soldier->SwitchLight(false);
  if (unit)
  {
    unit->SetVehicleIn(this);
  }
  soldier->OnGetInFinished();

  if (!_doorSound && sound)
  {
    const SoundPars &pars=GetType()->GetGetInSound();
    if (pars.name.GetLength() > 0 && GSoundScene->CanBeAudible(WaveEffect,FutureVisualState().Position(),pars.vol))
    {
      _doorSound=GSoundScene->OpenAndPlayOnce
        (
        pars.name,this,true,FutureVisualState().Position(),FutureVisualState().Speed(),pars.vol, pars.freq, pars.distance
        );
      if( _doorSound )
      {
        GSoundScene->SimulateSpeedOfSound(_doorSound);
        GSoundScene->AddSound(_doorSound);
      }
    }
  }
#if _VBS3 // always fire event
  OnEvent(EEGetIn,name,soldier);
  // Fire the event if the soldier has one too
  soldier->OnEvent(EEGetInMan,name,this);

#else
  if (sound) OnEvent(EEGetIn,name,soldier);
#endif

  GWorld->OnEntityGetIn(soldier,this);
  soldier->OnMovedFromLandscape(); //fix: soldier picked up from water and dropped out on land was still swimming
}

void Transport::PreloadGetIn(UIActionType position)
{
  LODShapeWithShadow *lShape = GetShape();
  if (lShape)
  {
    int level = InternalLOD(position);
    if (level >= 0)
    {
      bool loaded = lShape->CheckLevelLoaded(level,true);
      if (loaded)
      {
        // we may preload textures as well
        ShapeUsed shape = lShape->Level(level);
        shape->PreloadTextures(0, NULL);
      }
    }
  }

  // UI textures
  // - compass
  int canSee = 0;
  switch (position)
  {
  case ATGetInCommander:
    canSee = Type()->_commanderCanSee; break;
  case ATGetInGunner:
    canSee = Type()->_gunnerCanSee; break;
  case ATGetInDriver:
  case ATGetInPilot:
    canSee = Type()->_driverCanSee; break;
  }
  if (canSee & CanSeeCompass)
  {
    PreloadedTexture seg[4] = {Compass180, Compass270, Compass000, Compass090};
    for (int i=0; i<4; i++)
    {
      Texture *texture = GScene->Preloaded(seg[i]);
      if (!texture) continue;
      if (!texture->HeadersReady()) continue;
      GEngine->TextBank()->UseMipmap(texture, 1, 0);
    }
  }
}

bool Transport::CanBeCommander(AIBrain *unit) const
{
  // when player can become commander if get in to driver / gunner / cargo position
  if
    (
    _effCommander && _effCommander->Brain() &&
    (_effCommander->GetGroup() != unit->GetGroup() || _effCommander->Brain()->IsAnyPlayer())
    ) return false; // commander position occupied by other player or unit from other group

  if (unit->IsGroupLeader()) return true;
  AIBrain *commander = CommanderUnit();
  if (!commander || !commander->GetPerson()) return true;
  if (commander == unit || commander->IsAnyPlayer()) return true;
  Assert(unit->GetPerson());
  return commander->GetPerson()->GetInfo()._rank < unit->GetPerson()->GetInfo()._rank;
}

class OpenFireFunc : public ITurretFunc
{
public:
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    context._weapons->_fire._firePrepareOnly = false;
    return false; // continue
  }
};

/// Functor searching if person with the better commanding level than given is in vehicle
class IsBetterCommander : public ITurretFunc
{
protected:
  float _limit;

public:
  IsBetterCommander(float limit) : _limit(limit) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turretType) return false; // continue
    if (!context._gunner) return false; // continue
    if (!context._gunner->Brain()) return false; // continue
    return context._turretType->_commanding > _limit;
  }
};

void Transport::ControlTakenBy(Person *man)
{
  // suppresive fire needs to be explicitely enabled by the FSM
  _useSuppressiveFire = SuppressNo;
}

void Transport::GetInDriver(Person *driver, bool sound)
{
  bool wasCommander = CommanderUnit() && CommanderUnit()->IsPlayer();

  if (_driver)
  {
    DoAssert(_driver->IsDamageDestroyed());

    GetInPoint pt = AnimateGetInPoint(Type()->GetDriverGetInPos(0));
    Matrix4 transform;
    FutureVisualState().PositionModelToWorld(pt._pos, pt._pos);
    FutureVisualState().DirectionModelToWorld(pt._dir,pt._dir);
    transform.SetPosition(pt._pos);
    transform.SetUpAndDirection(VUp, -pt._dir);

    GetOutDriver(transform, true, false);
  }

  _driver=driver;
  if( driver==GLOB_WORLD->CameraOn() )
  {
    GLOB_WORLD->SwitchCameraTo(this,GLOB_WORLD->GetCameraType(), false);
  }
  /*
  EngineOn();
  */
  GetInAny(driver,sound,"driver");
  _driverHiddenWanted = FutureVisualState()._driverHidden = Type()->_hideProxyInCombat;
  driver->SwitchAction(DriverAction());

  // driver is changing, cancel current command
  _moveMode = VMMFormation;

  // if driver is commander, _effCommander is never set (vehicle does not work with different commander than driver)
  if (!Type()->DriverIsCommander())
  {
    AIBrain *unit = _effCommander ? _effCommander->Brain() : NULL;
    if (unit && unit->IsAnyPlayer() && unit->IsGroupLeader() && !unit->IsInCargo())
    {
      // do not change effective commander
    }
    else
    {
      // driver will sometimes become effective commander
      AIBrain *unit = driver->Brain();
      if (unit && unit->IsAnyPlayer() && CanBeCommander(unit))
      {
        _effCommander = driver;
      }
      else
      {
        IsBetterCommander func(0);
        if (!ForEachTurret(func))
        {
          _effCommander = driver;
        }
      }
    }
  }

  if (driver->Brain()==CommanderUnit())
  {
    ControlTakenBy(driver);
  }

  if (!wasCommander && CommanderUnit() && CommanderUnit()->IsPlayer())
  {
    // player becomes commander
    // reset gunner state to open fire
    OpenFireFunc func;
    ForEachTurret(func);
  }
}

int Transport::GetManCargoSize() const
{
  int n = 0;
  for (int i=0; i<_manCargo.Size(); i++)
  {
    if (_manCargo[i]) n++;
  }
  return n;
}

int Transport::GetFreeManCargo() const
{
  return Type()->_maxManCargo - GetManCargoSize();
}

/*!
\patch 5137 Date 3/1/2007 by Ondra
- New: Player can use Eject to disembark the vehicle while leaving engine on.
*/
void Transport::GetOutDriver(const Matrix4 &outPos, bool eject, bool sound)
{
  if( !_driver ) return;

  bool wasCommander = CommanderUnit() && CommanderUnit()->IsPlayer();

  GetOutAny(outPos, _driver, sound, "driver", Type()->GetDriverGetOutAction());

  if (!eject)
  {
    // use EngineOffAction to make sure it switches off even for helicopters
    EngineOffAction();
  }
  GWorld->VehicleSwitched(this,_driver);
  if (_driver->Brain() == CommanderUnit())
    _manualFire = false;
  _driver=NULL;

  if (!wasCommander && CommanderUnit() && CommanderUnit()->IsPlayer())
  {
    // player becomes commander
    // reset gunner state to open fire
    OpenFireFunc func;
    ForEachTurret(func);
  }
}

bool Transport::CheckCrewVulnerable() const
{
  // by default assume there is no crew or it is nor vulnerable to small arms fire
  // if crew cannot turn out, assume it is not vulnerable
  if (!Type()->_hideProxyInCombat)
  {
    // check if crew is sitting always out
    return Type()->_crewVulnerable;
  }
  // check if some crew is currently turned out?
  return true;
}

void Transport::GetInCargo(Person *soldier, bool sound, int index)
{
  bool wasCommander = CommanderUnit() && CommanderUnit()->IsPlayer();

  // if position is not specified, search for first free one
  if (index<0)
  {
    for (int i=0; i<_manCargo.Size(); i++)
    {
      if (_manCargo[i] == NULL || _manCargo[i]->IsDamageDestroyed())
      {
        index = i;
        break;
      }
    }
    if (index < 0)
    {
      Fail("No free position in cargo");
      return;
    }
  }

  if (_manCargo[index])
  {
    GetInPoint pt = AnimateGetInPoint(Type()->GetCargoGetInPos(index, 0));
    Matrix4 transform;
    FutureVisualState().PositionModelToWorld(pt._pos, pt._pos);
    FutureVisualState().DirectionModelToWorld(pt._dir,pt._dir);
    transform.SetPosition(pt._pos);
    transform.SetUpAndDirection(VUp, -pt._dir);

    GetOutCargo(_manCargo[index], transform, false);
  }

  _manCargo.Set(index).man = soldier;
  GetInAny(soldier,sound,"cargo");
  soldier->SwitchAction(CargoAction(index));
  GWorld->VehicleSwitched(soldier,this);

  // if driver is commander, _effCommander is never set (vehicle does not work with different commander than driver)
  if (!Type()->DriverIsCommander())
  {
    AIBrain *unit = _effCommander ? _effCommander->Brain() : NULL;
    if (unit && unit->IsAnyPlayer() && unit->IsGroupLeader())
    {
      // do not change effective commander
    }
    else
    {
      // we become effective commander
      AIBrain *unit = soldier->Brain();
      if (unit && unit->IsAnyPlayer() && CanBeCommander(unit))
      {
        _effCommander = soldier;
      }
      else if (!_driver)
      {
        // check for any crew
        IsBetterCommander func(-FLT_MAX);
        if (!ForEachTurret(func))
        {
          _effCommander = soldier;
        }
      }
    }
  }

  if (soldier->Brain()==CommanderUnit())
  {
    ControlTakenBy(soldier);
  }

  if (!wasCommander && CommanderUnit() && CommanderUnit()->IsPlayer())
  {
    // player becomes commander
    // reset gunner state to open fire
    OpenFireFunc func;
    ForEachTurret(func);
  }
}

void Transport::GetOutCargo(Person *cargo, const Matrix4 &outPos, bool sound)
{
  bool wasCommander = CommanderUnit() && CommanderUnit()->IsPlayer();

  int pos = -1;
  for (int i=0; i<_manCargo.Size(); i++)
  {
    if (_manCargo[i] == cargo)
    {
      pos = i;
      break;
    }
  }
  if (pos < 0)
  {
    Fail("Unit is not in cargo");
    return;
  }

  GetOutAny(outPos, cargo, sound, "cargo", Type()->GetCargoGetOutAction(pos));

  _manCargo.Set(pos).man = NULL;

  if (!wasCommander && CommanderUnit() && CommanderUnit()->IsPlayer())
  {
    // player becomes commander
    // reset gunner state to open fire
    OpenFireFunc func;
    ForEachTurret(func);
  }
}

void Transport::GetInGunner(Person *soldier, bool sound)
{
  // check if gunner position exist
  TurretContextV context;
  if (!GetPrimaryGunnerTurret(unconst_cast(FutureVisualState()), context)) return;
  if (!context._turret) return;

  Person *gunner = context._turret->Gunner();

  bool wasCommander = CommanderUnit() && CommanderUnit()->IsPlayer();

  if (gunner)
  {
    DoAssert(gunner->IsDamageDestroyed());

    GetInPoint pt = AnimateGetInPoint(context._turretType->GetGunnerGetInPos(0));
    Matrix4 transform;
    FutureVisualState().PositionModelToWorld(pt._pos, pt._pos);
    FutureVisualState().DirectionModelToWorld(pt._dir,pt._dir);
    transform.SetPosition(pt._pos);
    transform.SetUpAndDirection(VUp, -pt._dir);

    GetOutGunner(transform, false);
  }

  context._turret->SetGunner(soldier);

  float hidden = Type()->_hideProxyInCombat && context._turretType->_canHideGunner ? 1.0f : 0.0f;
  context._turretVisualState->_crewHidden = hidden;
  context._turret->_crewHiddenWanted = hidden;
  RString action = context._turret->GunnerAction(*context._turretVisualState, *context._turretType);
  GetInAny(soldier, sound, "gunner");
  soldier->SwitchAction(action);
  GWorld->VehicleSwitched(soldier, this);

  // if driver is commander, _effCommander is never set (vehicle does not work with different commander than driver)
  if (!Type()->DriverIsCommander())
  {
    AIBrain *unit = _effCommander ? _effCommander->Brain() : NULL;
    if (unit && unit->IsAnyPlayer() && unit->IsGroupLeader() && !unit->IsInCargo())
    {
      // do not change effective commander
    }
    else
    {
      // gunner will sometimes become effective commander
      AIBrain *unit = soldier->Brain();
      if (unit && unit->IsAnyPlayer() && CanBeCommander(unit))
      {
        _effCommander = soldier;
      }
      else
      {
        if (!_driver || context._turretType->_commanding > 0)
        {
          IsBetterCommander func(context._turretType->_commanding);
          if (!ForEachTurret(func))
          {
            _effCommander = soldier;
          }
        }
      }
    }
  }

  if (soldier->Brain()==CommanderUnit())
  {
    ControlTakenBy(soldier);
  }

  // disable manual fire in MP when player gunner gets in
  if (GunnerUnit() && GunnerUnit()->IsAnyPlayer()) _manualFire = false;

  if (!wasCommander && CommanderUnit() && CommanderUnit()->IsPlayer())
  {
    // player becomes commander
    // reset gunner state to open fire
    OpenFireFunc func;
    ForEachTurret(func);
  }
}

void Transport::GetOutGunner(const Matrix4 &outPos, bool sound)
{
  TurretContext context;
  if (!GetPrimaryGunnerTurret(context)) return;
  if (!context._turret) return;
  Person *gunner = context._turret->Gunner();

  if (!gunner) return;

  bool wasCommander = CommanderUnit() && CommanderUnit()->IsPlayer();

  GetOutAny(outPos, gunner, sound, "gunner", context._turretType->GetGunnerGetOutAction());
  context._turret->SetGunner(NULL);

  if (!wasCommander && CommanderUnit() && CommanderUnit()->IsPlayer())
  {
    // player becomes commander
    // reset gunner state to open fire
    OpenFireFunc func;
    ForEachTurret(func);
  }
}

/*!
\patch 1.28 Date 10/30/2001 by Ondra
- Fixed: MP character animation in cargo (looking through roof).
*/

void Transport::GetInCommander(Person *soldier, bool sound)
{
  // check if observer position exist
  TurretContextV context;
  if (!GetPrimaryObserverTurret(unconst_cast(FutureVisualState()), context)) return;
  if (!context._turret) return;

  Person *commander = context._turret->Gunner();

  bool wasCommander = CommanderUnit() && CommanderUnit()->IsPlayer();

  if (commander)
  {
    DoAssert(commander->IsDamageDestroyed());

    GetInPoint pt = AnimateGetInPoint(context._turretType->GetGunnerGetInPos(0));
    Matrix4 transform;
    FutureVisualState().PositionModelToWorld(pt._pos, pt._pos);
    FutureVisualState().DirectionModelToWorld(pt._dir,pt._dir);
    transform.SetPosition(pt._pos);
    transform.SetUpAndDirection(VUp, -pt._dir);

    GetOutCommander(transform, false);
  }

  context._turret->SetGunner(soldier);

  float hidden = Type()->_hideProxyInCombat && context._turretType->_canHideGunner ? 1.0f : 0.0f;
  context._turretVisualState->_crewHidden = hidden;
  context._turret->_crewHiddenWanted = hidden;
  RString action = context._turret->GunnerAction(*context._turretVisualState, *context._turretType);
  GetInAny(soldier,sound,"commander");
  soldier->SwitchAction(action);
  GWorld->VehicleSwitched(soldier,this);

  // if driver is commander, _effCommander is never set (vehicle does not work with different commander than driver)
  if (!Type()->DriverIsCommander())
  {
    AIBrain *unit = _effCommander ? _effCommander->Brain() : NULL;
    // any player will become effective commander
    // if AI is getting in and effective commander is a group leader, let him be
    if (unit && unit->IsAnyPlayer() && unit->IsGroupLeader() && !unit->IsInCargo())
    {
      // do not change effective commander
    }
    else
    {
      // observer will sometimes become effective commander
      AIBrain *unit = soldier->Brain();
      if (unit && unit->IsAnyPlayer() && CanBeCommander(unit))
      {
        _effCommander = soldier;
      }
      else
      {
        if (!_driver || context._turretType->_commanding > 0)
        {
          IsBetterCommander func(context._turretType->_commanding);
          if (!ForEachTurret(func))
          {
            _effCommander = soldier;
          }
        }
      }
    }
  }

  if (soldier->Brain() == CommanderUnit())
  {
    ControlTakenBy(soldier);
  }

  if (!wasCommander && CommanderUnit() && CommanderUnit()->IsPlayer())
  {
    // player becomes commander
    // reset gunner state to open fire
    OpenFireFunc func;
    ForEachTurret(func);
  }
}

void Transport::GetOutCommander(const Matrix4 &outPos, bool sound)
{
  TurretContext context;
  if (!GetPrimaryObserverTurret(context)) return;
  if (!context._turret) return;
  Person *commander = context._turret->Gunner();

  if (!commander) return;

  bool wasCommander = CommanderUnit() && CommanderUnit()->IsPlayer();

  GetOutAny(outPos, commander, sound, "commander", context._turretType->GetGunnerGetOutAction());
  if (commander->Brain() == CommanderUnit())
    _manualFire = false;

  context._turret->SetGunner(NULL);

  if (!wasCommander && CommanderUnit() && CommanderUnit()->IsPlayer())
  {
    // player becomes commander
    // reset gunner state to open fire
    OpenFireFunc func;
    ForEachTurret(func);
  }
}

void Transport::GetInTurret(Turret *turret, Person *soldier, bool sound)
{
  // need the turret type
  TurretContextV context;
  if (!FindTurret(unconst_cast(FutureVisualState()), turret, context)) return;
  if (!context._turret) return;

  Person *gunner = context._turret->Gunner();

  bool wasCommander = CommanderUnit() && CommanderUnit()->IsPlayer();

  if (gunner)
  {
    DoAssert(gunner->IsDamageDestroyed());

    GetInPoint pt = AnimateGetInPoint(context._turretType->GetGunnerGetInPos(0));
    Matrix4 transform;
    FutureVisualState().PositionModelToWorld(pt._pos, pt._pos);
    FutureVisualState().DirectionModelToWorld(pt._dir,pt._dir);
    transform.SetPosition(pt._pos);
    transform.SetUpAndDirection(VUp, -pt._dir);

    GetOutTurret(context, transform, false);
  }

  context._turret->SetGunner(soldier);

  float hidden = Type()->_hideProxyInCombat && context._turretType->_canHideGunner ? 1.0f : 0.0f;
  context._turretVisualState->_crewHidden = hidden;
  context._turret->_crewHiddenWanted = hidden;
  RString action = context._turret->GunnerAction(*context._turretVisualState, *context._turretType);
  GetInAny(soldier, sound, "gunner");
  soldier->SwitchAction(action);
  GWorld->VehicleSwitched(soldier, this);

  // if driver is commander, _effCommander is never set (vehicle does not work with different commander than driver)
  if (!Type()->DriverIsCommander())
  {
    AIBrain *unit = _effCommander ? _effCommander->Brain() : NULL;
    // any player will become effective commander
    // if AI is getting in and effective commander is a group leader, let him be
    if (unit && unit->IsAnyPlayer() && unit->IsGroupLeader() && !unit->IsInCargo())
    {
      // do not change effective commander
    }
    else
    {
      // gunner will sometimes become effective commander
      AIBrain *unit = soldier->Brain();
      if (unit && unit->IsAnyPlayer() && CanBeCommander(unit))
      {
        _effCommander = soldier;
      }
      else
      {
        if (!_driver || context._turretType->_commanding > 0)
        {
          IsBetterCommander func(context._turretType->_commanding);
          if (!ForEachTurret(func))
          {
            _effCommander = soldier;
          }
        }
      }
    }
  }

  if (soldier->Brain()==CommanderUnit())
  {
    ControlTakenBy(soldier);
  }

  // disable manual fire in MP when player gunner gets in
  if (context._turretType && context._turretType->_primaryGunner && GunnerUnit() && GunnerUnit()->IsAnyPlayer()) _manualFire = false;

  if (!wasCommander && CommanderUnit() && CommanderUnit()->IsPlayer())
  {
    // player becomes commander
    // reset gunner state to open fire
    OpenFireFunc func;
    ForEachTurret(func);
  }
}

void Transport::GetOutTurret(TurretContext &context, const Matrix4 &outPos, bool sound)
{
  if (!context._turret) return;

  Person *gunner = context._gunner;
  if (!gunner) return;

  bool wasCommander = CommanderUnit() && CommanderUnit()->IsPlayer();

  GetOutAny(outPos, gunner, sound, "gunner", context._turretType->GetGunnerGetOutAction());
  if (gunner->Brain() == CommanderUnit())
    _manualFire = false;

  context._turret->SetGunner(NULL);

  if (!wasCommander && CommanderUnit() && CommanderUnit()->IsPlayer())
  {
    // player becomes commander
    // reset gunner state to open fire
    OpenFireFunc func;
    ForEachTurret(func);
  }
}

void Transport::Eject(AIBrain *unit, Vector3Val diff)
{
  // TODO: create parachute if necessary
  unit->ProcessGetOut(true, false, diff);
}

void Transport::Land()
{
}

void Transport::CancelLand()
{
}

GetInPoint Transport::GetUnitGetInPos(AIBrain *unit) const
{
  Assert(unit);
  Person *person = unit->GetPerson();
  Vector3Val unitPos = person->RenderVisualState().Position();
  if (GetDriverAssigned() == unit)
    return GetDriverGetInPos(person, unitPos);
  else
  {
    TurretContext context;
    if (FindAssignedTurret(unit, context) && context._turret)
      return GetTurretGetInPos(context._turret, person, unitPos);
    else
    {
      int index = 0;
      for (int i=0; i<_manCargo.Size(); i++)
      {
        if (GetCargoAssigned(i) == unit)
        {
          index = i; break;
        }
      }
      return GetCargoGetInPos(index, person, unitPos);
    }
  }
}

GetInPoint Transport::GetUnitGetOutPos(AIBrain *unit) const
{
  Assert(unit);
  Person *person = unit->GetPerson();
  if (Driver() == person)
    return GetDriverGetOutPos(person);
  else
  {
    TurretContext context;
    if (FindTurret(person, context) && context._turret)
      return GetTurretGetOutPos(context._turret, person);
    else
      return GetCargoGetOutPos(person);
  }
}

GetInPoint Transport::GetDefaultGetInPos(Person *person, Vector3Par pos) const
{
  GetInPoint best;
  best._dir = pos-RenderVisualState().Position();
  if (best._dir.SquareSize()>Square(0.1f))
  {
    best._dir.Normalize();
  }
  else
  {
    best._dir = VForward;
  }
  best._pos = pos;
  return best;
}

GetInPoint Transport::GetDriverGetInPos(Person *person, Vector3Par pos) const
{
  Vector3Val unitPos = pos;
  GetInPoint best;
  float minDist2 = FLT_MAX;
  int n = Type()->NDriverGetInPos();
  for (int i=0; i<n; i++)
  {
    GetInPoint pt = AnimateGetInPoint(Type()->GetDriverGetInPos(i));
    RenderVisualState().PositionModelToWorld(pt._pos, pt._pos);
    float dist2 = pt._pos.Distance2(unitPos);
    if (dist2 < minDist2)
    {
      minDist2 = dist2;
      best = pt;
    }
  }
  if (minDist2<FLT_MAX)
  {
    RenderVisualState().DirectionModelToWorld(best._dir, best._dir);
    return best;
  }
  else
  {
    return GetDefaultGetInPos(person,pos);
  }
}

GetInPoint Transport::GetCommanderGetInPos(Person *person, Vector3Par pos) const
{
  Vector3Val unitPos = pos;
  GetInPoint best;
  float minDist2 = FLT_MAX;

  TurretContext context;
  if (GetPrimaryObserverTurret(context) && context._turret)
  {
    int n = context._turretType->NGunnerGetInPos();
    for (int i=0; i<n; i++)
    {
      GetInPoint pt = AnimateGetInPoint(context._turretType->GetGunnerGetInPos(i));
      RenderVisualState().PositionModelToWorld(pt._pos, pt._pos);
      float dist2 = pt._pos.Distance2(unitPos);
      if (dist2 < minDist2)
      {
        minDist2 = dist2;
        best = pt;
      }
    }
  }
  if (minDist2<FLT_MAX)
  {
    RenderVisualState().DirectionModelToWorld(best._dir, best._dir);
    return best;
  }
  else
  {
    return GetDefaultGetInPos(person,pos);
  }
}

GetInPoint Transport::GetGunnerGetInPos(Person *person, Vector3Par pos) const
{
  Vector3Val unitPos = pos;
  GetInPoint best;
  float minDist2 = FLT_MAX;
  TurretContext context;
  if (GetPrimaryGunnerTurret(context) && context._turret)
  {
    int n = context._turretType->NGunnerGetInPos();
    for (int i=0; i<n; i++)
    {
      GetInPoint pt = AnimateGetInPoint(context._turretType->GetGunnerGetInPos(i));
      RenderVisualState().PositionModelToWorld(pt._pos, pt._pos);
      float dist2 = pt._pos.Distance2(unitPos);
      if (dist2 < minDist2)
      {
        minDist2 = dist2;
        best = pt;
      }
    }
  }
  if (minDist2<FLT_MAX)
  {
    RenderVisualState().DirectionModelToWorld(best._dir, best._dir);
    return best;
  }
  else
  {
    return GetDefaultGetInPos(person,pos);
  }
}

GetInPoint Transport::GetTurretGetInPos(Turret *turret, Person *person, Vector3Par pos) const
{
  Vector3Val unitPos = pos;
  GetInPoint best;
  float minDist2 = FLT_MAX;
  TurretContext context;
  if (FindTurret(turret, context) && context._turret)
  {
    int n = context._turretType->NGunnerGetInPos();
    for (int i=0; i<n; i++)
    {
      GetInPoint pt = AnimateGetInPoint(context._turretType->GetGunnerGetInPos(i));
      RenderVisualState().PositionModelToWorld(pt._pos, pt._pos);
      float dist2 = pt._pos.Distance2(unitPos);
      if (dist2 < minDist2)
      {
        minDist2 = dist2;
        best = pt;
      }
    }
  }
  if (minDist2<FLT_MAX)
  {
    RenderVisualState().DirectionModelToWorld(best._dir, best._dir);
    return best;
  }
  else
  {
    return GetDefaultGetInPos(person,pos);
  }
}

GetInPoint Transport::GetCargoGetInPos(int cargoIndex, Person *person, Vector3Par pos) const
{
  const VisualState &vs = RenderVisualState();
  Vector3Val unitPos = pos;
  GetInPoint best;
  float minDist2 = FLT_MAX;
  int n = Type()->NCargoGetInPos(cargoIndex);
  for (int i=0; i<n; i++)
  {
    GetInPoint pt = AnimateGetInPoint(Type()->GetCargoGetInPos(cargoIndex, i));
    vs.PositionModelToWorld(pt._pos, pt._pos);
    float dist2 = pt._pos.Distance2(unitPos);
    if (dist2 < minDist2)
    {
      minDist2 = dist2;
      best = pt;
    }
  }
  if (minDist2<FLT_MAX)
  {
    vs.DirectionModelToWorld(best._dir, best._dir);
    return best;
  }
  else
  {
    return GetDefaultGetInPos(person,pos);
  }
}

GetInPoint Transport::GetDriverGetOutPos(Person *person) const
{
  Vector3Val pos = person->WorldPosition(person->RenderVisualState());
  return GetDriverGetInPos(person, pos);
}

GetInPoint Transport::GetCommanderGetOutPos(Person *person) const
{
  Vector3Val pos = person->WorldPosition(person->RenderVisualState());
  return GetCommanderGetInPos(person, pos);
}

GetInPoint Transport::GetGunnerGetOutPos(Person *person) const
{
  Vector3Val pos = person->WorldPosition(person->RenderVisualState());
  return GetGunnerGetInPos(person, pos);
}

GetInPoint Transport::GetTurretGetOutPos(Turret *turret, Person *person) const
{
  Vector3Val pos = person->WorldPosition(person->RenderVisualState());
  return GetTurretGetInPos(turret, person, pos);
}

GetInPoint Transport::GetCargoGetOutPos(Person *person) const
{
  Vector3Val pos = person->WorldPosition(person->RenderVisualState());

  // check which cargo slot are we in
  int index = -1;
  for (int i=0; i<_manCargo.Size(); i++)
  {
    if (person == _manCargo[i])
    {
      index = i;
      break;
    }
  }
  return GetCargoGetInPos(index, person, pos);
}

void Transport::PerformFF( FFEffects &effects )
{
  base::PerformFF(effects);

  //  if (_engineOff)
  //  {
  //    effects.engineMag = 0;
  //    effects.engineFreq = 50;
  //    return;
  //  }

  float vol,freq;
  vol=GetEngineVol(freq);
  if (freq<=0.001) vol = 0;

  effects.engineMag = vol*0.09f;
  effects.engineFreq = freq*50;
}

/*!
\patch 1.24 Date 09/26/2001 by Ondra
- Fixed: Speed of sound delay was missing on vehicle crash sound.
*/

class CrewSoundFunc : public ICrewFunc
{
protected:
  float _deltaT;
  Transport *_parent;

public:
  CrewSoundFunc(Transport *veh, float timeDelta) : _parent(veh), _deltaT(timeDelta)
  {}

  virtual bool operator () (Person *person)
  {
    if (person->SoundInTransportNeeded())
    {
      Object::ProtectedVisualState<const ObjectVisualState> vs = person->RenderVisualStateScope(); 
      Matrix4 worldPos = _parent->ProxyWorldTransform(person->GetVisualStateAge(vs), person);
      person->SoundInTransport(_parent,worldPos,_deltaT);
    }
    return false;
  }
};

/*
Member function evaluate frequency and volume values on the basis of expression, expression is given in config,
more info about sound controllers can be found at https://wiki.bistudio.com/index.php/Kontrolery_pro_zvuky_%28Audio_Controllers%29
*/
float Transport::GetSoundsVol(float &freq, const SoundParsEx &soundPars, float* evalControllers, int length) const
{
  bool result = soundPars._freqExpr.Evaluate(freq, evalControllers, length);
  if (!result) freq = 1;

  float volume;
  result = soundPars._volExpr.Evaluate(volume, evalControllers, length);
  if (!result) volume = 0;

  return volume;
}

float Transport::EvaluateSoundEvent(const SoundEventPars &soundEPars, float* evalControllers, int length) const
{
  float val;
  bool result = soundEPars._expression.Evaluate(val, evalControllers, length);

  return (result ? val : 0);
}

/*
Camera inside = 0, outside = 1, between interpolation in range [0, 1]
*/
float Transport::GetCamPos() const
{
  if (GWorld->CameraOn() == this)
  {
    bool listenerInside = false;
    OLink(Person) person = GWorld->FocusOn() ? GWorld->FocusOn()->GetPerson() : NULL;

    if (person) { listenerInside = ListenerIsInside(person); }

    // listener inside vehicle
    if (listenerInside)
    {
      bool cameraTypeNew = GWorld->GetCameraTypeNew() == CamInternal || GWorld->GetCameraTypeNew() == CamGunner;
      bool cameraTypeOld = GWorld->GetCameraTypeOld() == CamInternal || GWorld->GetCameraTypeOld() == CamGunner;

      // camera inside vehicle
      if (cameraTypeNew && cameraTypeOld) return 0.0f;

      // transition from internal camera to external
      if (cameraTypeOld && GWorld->GetCameraTypeNew() == CamExternal)
      {
        return (GWorld->GetCameraTransition());
      }

      // transition from external to internal camera
      if (GWorld->GetCameraTypeOld() == CamExternal && cameraTypeNew)
      {
        return (1.0f-GWorld->GetCameraTransition());
      }

      return 1.0f;
    }
    }

  return 1.0f;
}

float Transport::GetSpeed() const
{
  const VisualState &vs = RenderVisualState();
  Vector3 tmp = vs.DirectionWorldToModel(vs.Speed());

  return tmp.Size() * fSign(tmp.Z());
}

/// surface usage count
/* used for audio controllers */
struct SurfInfoCount
{
  const SurfaceInfo *surf;
  int count;
};

TypeIsSimple(SurfInfoCount);

template <>
struct FindArrayKeyTraits<SurfInfoCount>
{
  typedef const SurfaceInfo * KeyType;
  static bool IsEqual(KeyType a, KeyType b) {return a==b;}
  static KeyType GetKey(const SurfInfoCount &a) {return a.surf;}
};

// Select surface info
void Transport::SetSurfaceInfo(GroundCollisionBuffer &groundCollBuffer)
{
  // check which surfaces are we in contact with
  FindArrayKey<SurfInfoCount, FindArrayKeyTraits<SurfInfoCount>, MemAllocLocal<SurfInfoCount,16> > surfsInfo;

  for (int i = 0; i < groundCollBuffer.Size(); i++)
  {
    // info.pos is world space
    UndergroundInfo &info = groundCollBuffer[i];

    if (info.under < 0) { continue; }

    // surfaces occurrences, used for selection the most incident surface
    if (info.texture)
    {
      const SurfaceInfo *surf = &info.texture->GetSurfaceInfo();
      int index = surfsInfo.FindKey(surf);
      if (index<0)
      {
        SurfInfoCount &sInfo = surfsInfo.Append();
        sInfo.count = 1;
        sInfo.surf = surf;
      }
      else
      {
        surfsInfo[index].count++;
      }
    }
  }

  // select the most incident
  int maxCount = 0;
  const SurfaceInfo *surf = NULL;
  for (int j = 0; j < surfsInfo.Size(); j++)
  {
    if (surfsInfo[j].count > maxCount) maxCount = surfsInfo[j].count, surf = surfsInfo[j].surf;
  }

  _ptrSurfInfo = surf;
}

template<class Type>
void SoundsHolder::UpdateSubmixWaves(const AutoArray<Type> &arr, const TransportSoundContex &context)
{
  PROFILE_SCOPE_EX(sndUS, sound);

  if (!_submix) return;

  float distSum = 0;
  float volSum = 0;
  int nWaves = 0;
  SoundCone *cone = NULL;

  // update waves included in submix
  for (int i = 0; i < arr.Size(); i++)
  {
    SoundParsEx &parEx = const_cast<SoundParsEx &>(arr[i]);

    // set cone ptr, cone is same for whole group
    if (!cone && parEx._coneUsed) cone = &parEx._cone;

    float vol, freq;
    // volume & frequency expressions evaluation
    vol = context._transport->GetSoundsVol(freq, parEx, context._ctr, context._length);

    if (freq>0.001f && vol>0.001f && !parEx._pars.name.IsEmpty() &&
      (context._inside || GSoundScene->CanBeAudible(context._engPos, parEx._pars.distance))
    )
    {
      // create wave
      if (_waves[i].IsNull())
      {
        // create wave
        _waves[i] = GSoundScene->OpenAndPlay(parEx._pars.name, context._engPos, context._speed, 
          vol, freq, parEx._pars.distance);

        // include wave into submix
        if (_waves[i].NotNull()) { _submix->Include(_waves[i]); }
      }

      // update wave params
      if (_waves[i].NotNull())
      {
        _waves[i]->SetVolume(vol, freq);
        _waves[i]->SetObstruction(context._obstruction, context._occlusion, context._deltaT);
        _waves[i]->SetPosition(context._engPos, context._speed);

        distSum += _waves[i]->GetMaxDistance();
        volSum += vol;
        nWaves++;
      }
    }
    else
    {
      // release wave
      if (_waves[i]) _submix->Exclude(_waves[i].GetRef());
      _waves[i].Free();
    }
  }

  if (_waves.Size() == 0 || nWaves == 0) return;

  _submix->EnableDirectionalSound(false);
  // external sounds
  if (!context._inside && cone) { AbstractSoundsHolder::SetDirectSound(*cone, context._transport); }
  if (context._inside) volSum *= context._transport->GetInsideSoundCoeff();

  float avgDist = (nWaves > 0) ? (distSum / (float) nWaves) : 0;
  float avgVol = (nWaves > 0) ? (volSum / (float) nWaves) : 0;

  float doppler = 1.0f;
  if (_submix) doppler = _submix->Calculate3D(true, context._engPos, context._speed, avgDist, avgVol);

  for (int i = 0; i < _waves.Size(); i++)
  {
    if (_waves[i].NotNull()) 
      _waves[i]->SetVolume(_waves[i]->GetVolume(), doppler * _waves[i]->GetFrequency());
  }
}

template<class Type>
void SoundsEventHolder::UpdateSubmixWaves(const AutoArray<Type> &arr, const TransportSoundContex &context)
{
  PROFILE_SCOPE_EX(sndUE, sound);

  if (!_submix) return;
 
  float distSum = 0;
  float volSum = 0;
  int nWaves = 0;
  SoundCone *cone = NULL;
 
  // for each wave included into submix
  for (int i = 0; i < arr.Size(); i++)
  {
    SoundEventPars &pars = const_cast<SoundEventPars &>(arr[i]);

    // set cone ptr, cone is same for whole group
    if (!cone && pars._coneUsed) cone = &pars._cone;

    float expression = context._transport->EvaluateSoundEvent(pars, context._ctr, context._length);
    bool limitReached = expression > pars._limit;

    // remember limit overrun
    if (!limitReached) pars._started = false;

    if (_waves[i].IsNull())
    {
      if ((pars._pars.name.GetLength() > 0) && (limitReached) && (!pars._started)
        && GSoundScene->CanBeAudible(context._engPos, pars._pars.distance))
      {
        // start non loop sample
        _waves[i] = GSoundScene->OpenAndPlayOnce(
          pars._pars.name, const_cast<Transport *> (context._transport), context._inside, context._engPos, 
          context._speed, pars._pars.vol, pars._pars.freq, pars._pars.distance);

        if (_waves[i])
        {
          _submix->Include(_waves[i].GetRef());
          _waves[i]->SetObstruction(context._obstruction, context._occlusion, context._deltaT);
          pars._started = true;
        }
      }
    }
    else
    {
      // update position
      _waves[i]->SetPosition(context._engPos, context._speed);
      distSum += _waves[i]->GetMaxDistance();
      volSum += pars._pars.vol;
      nWaves++;

      // wave played, so we can release them
      if (_waves[i]->IsStopped()) { _submix->Exclude(_waves[i].GetRef()); _waves[i].Free(); }
    }
  }

  _submix->EnableDirectionalSound(false);
  // external sounds
  if (!context._inside && cone) { AbstractSoundsHolder::SetDirectSound(*cone, context._transport); }

  // minimal distance is avg. from all distances,
  float avgDist = (nWaves > 0) ? (distSum / (float) nWaves) : 0;
  float avgVol = (nWaves > 0) ? (volSum / (float) nWaves) : 0;

  // calculate 3d sound, if 2D then has no effect
  float doppler = _submix ? _submix->Calculate3D(true, context._engPos, context._speed, avgDist, avgVol) : 1.0f;

  // applying doppler effect to source voices
  for (int i = 0; i < _waves.Size(); i++)
  {
    if (_waves[i].NotNull())
      _waves[i]->SetVolume(_waves[i]->GetVolume(), _waves[i]->GetFrequency() * doppler);
  }
}

void AbstractSoundsHolder::Construct(int count)
{
  Assert(!_submix);
  _submix = GSoundsys->CreateSubmix();
  Assert(_submix);

  if (_submix) // for dSound or openAL it is always NULL
  {
    _submix->SetFilter(SubmixLowPFilter);
    _waves.Realloc(count);
    _waves.Resize(count);
  }
}

void AbstractSoundsHolder::Destruct()
{
  for (int i = 0; i < _waves.Size(); i++)
  {
    if (_waves[i]) _submix->Exclude(_waves[i]);
    _waves[i].Free();
  }

  _waves.Clear();
  
  if (_submix)
  {
    _submix->Unload();
    delete _submix;
    _submix = NULL;
  }
}

void AbstractSoundsHolder::SetDirectSound(SoundCone &cone, const Transport *transport)
{
  Assert(_submix && transport);

  // directional sound
  if (_submix && transport)
  {
    Vector3 frontDir = transport->RenderVisualState().DirectionModelToWorld(transport->GetFrontOrientation());
    frontDir.Normalize();

    Vector3 topDir = transport->RenderVisualState().DirectionModelToWorld(transport->GetTopOrientation());
    topDir.Normalize();

    _submix->EnableDirectionalSound(true);
    _submix->SetOrientation(topDir, frontDir);
    _submix->SetDirSoundConePars(cone._angleIn, cone._angleOut, cone._volIn, cone._volOut);
  }
}

const SoundPars& Transport::GetEngineSoundPars(bool inside)
{
  const TransportType *tType = Type();

  if (_engineOff)
    return inside ? tType->_engineOffSoundInt :  tType->_engineOffSoundExt;
  else
    return inside ? tType->_engineOnSoundInt : tType->_engineOnSoundExt;
}

// used in StartStopHolder for wave callback
struct OnTerminateCallback: public RefCount
{
  StartStopHolder &_holder;
  OnTerminateCallback(StartStopHolder &holder): _holder(holder) {}
};

bool StartStopHolder::StartPlayFromPos(Transport *transport, const SoundPars &pars, bool inside, bool reverse)
{
  const Transport::VisualState &vs = transport->RenderVisualState();
  // necessary params
  if ((pars.name.GetLength() > 0) && (pars.freq > 0.001f) && (pars.vol > 0.001f) &&
    GSoundScene->CanBeAudible(WaveEffect, vs.Position(), pars.vol))
  {
    RefAbstractWave tmp = GSoundScene->OpenAndPlayOnce(pars.name, transport, inside,
      vs.PositionModelToWorld(transport->GetEnginePos()), vs.Speed(), pars.vol, pars.freq, pars.distance);

    if (tmp)
    {
      // set callback
      tmp->SetOnTerminate(WaveCallBack, new OnTerminateCallback(*this));
      float vol = pars.vol;
      if (inside) vol *= transport->GetInsideSoundCoeff();
      tmp->SetVolume(vol, pars.freq);
      float pos = 1.0f;
      // if there is some wave playing
      if (_wave) pos = _wave->GetCurrPosition();
      pos = reverse ? (1.0f - pos) : pos;
      // set starting position
      tmp->SetOffset(pos);
      // release actually playing wave and set new wave
      if (_wave) _wave->Unload(), _wave.Free();
      // set as current playing
      _wave = tmp;
      _waveStop = false;
      _wavePlaying = true;

      return true;
    }
  }

  return false;
}

/* on terminate callback */
void StartStopHolder::WaveCallBack(AbstractWave *wave, RefCount *context)
{
  OnTerminateCallback* callback = static_cast<OnTerminateCallback *> (context);

  if (callback)
  {
    // set flags
    callback->_holder._wavePlaying = false;
    callback->_holder._waveStop = true;
  }
}

/* Release wave */
void StartStopHolder::Unload()
{
  if (_wave) _wave->Unload();
  _wave.Free();
}

void StartStopHolder::Sound(Transport *transport)
{
  // recalculate channels
  if (_wave) _wave->SetPosition(transport->RenderVisualState().Position(), VZero);

  // initialization - first pass, set init states
  if (!_stateInitilized)
  {
    _engineState = transport->_engineOff;
    _cameraState = transport->GetCamPos() < 0.5f;

    _stateInitilized = true;
    return;
  }

  int hitIndex = -1;

  //// planes has no hit point "hit engine"
  if (!transport->GetType()->IsKindOf(GWorld->Preloaded(VTypePlane)))
    hitIndex = transport->GetType()->FindHitPoint("HitEngine");

  // when engine damage - no start/stop sound
  if (hitIndex >= 0)
  {
    float engineHit = transport->GetHit(hitIndex);

    // engine is damage -> no start/stop sound
    if (engineHit >= 0.9f) return;
  }

  // engine state change
  if (_engineState != transport->_engineOff)
  {
    // select sound pars
    const SoundPars &pars = transport->GetEngineSoundPars(_cameraState);
    StartPlayFromPos(transport, pars, _cameraState, true);
    _engineState = transport->_engineOff;
  }

  bool cameraState = transport->GetCamPos() < 0.5f;

  // camera position change
  if (cameraState != _cameraState)
  {
    if (_wavePlaying)
    {
      // select sound pars
      const SoundPars &pars = transport->GetEngineSoundPars(cameraState);
      StartPlayFromPos(transport, pars, cameraState, false);
    }

    _cameraState = cameraState;
  }

  // wave is stopped, so my be released
  if (_waveStop) _wave.Free();
}

void Transport::Sound(bool inside, float deltaT)
{
  // note: XAudio2 has introduced submixes into the sound engine which makes the game crash when USE_OPEN_AL is set to 1 in main.cpp
#define USE_OPEN_AL 0
#if !USE_OPEN_AL
  PROFILE_SCOPE_EX(sndTr, sound);

  const TransportType *tType = Type();

  // audio controllers initialization
  if (_sounds.Size() == 0)
  {
    int subParsCount = tType->_submixPars.Size();

    _sounds.Realloc(subParsCount);
    _sounds.Resize(subParsCount);

    for (int i = 0; i < _sounds.Size(); i++)
    {
      _sounds[i] = new SoundsHolder(tType->_submixPars[i]._sParsEx.Size());
    }
  }

  // audio events initializations
  if (_soundsEvents.Size() == 0)
  {
    int subParsCount = tType->_submixEventPars.Size();

    _soundsEvents.Realloc(subParsCount);
    _soundsEvents.Resize(subParsCount);

    for (int i = 0; i < subParsCount; i++)
    {
      _soundsEvents[i] = new SoundsEventHolder(tType->_submixEventPars[i]._sEventPars.Size());
    }
  }

#if 0

  for (int i = 0; i < tType->_submixPars.Size(); i++)
  {
    const SubmixPars &sPars = tType->_submixPars[i];

    LogF("SubmixPars: %d", i);

    for (int j = 0; j < sPars._sParsEx.Size(); j++)
    {
      SoundParsEx *sEx = sPars._sParsEx[j];

      LogF("\tSoundparsEx %d, %s, %f", j, cc_cast(sEx->_pars.name), sEx->_pars.vol);
    }
  }

#endif

  const VisualState &vs = RenderVisualState();
  // evaluate controllers
  float controllers[] = {
    GetRenderVisualStateRPM(), GetRandomizer(), GetSpeed(), Thrust(), GetCamPos(), GetEngineOn(), GetRotorSpeed(),
    GetMainRotorThrust(), GetAngularVelocity().Size(), GetGMeterZ(),
    GetRoughness(), GetDustness(), GetDamperState(0),
    GetRockness(), GetSandness(), GetGrassness(), GetMudness(), GetGravelness(), GetAsphaltness(),
    GetGearPos(), GetFlapPos(), GetRotorPos(), GetWaterContact()
  };
  TransportSoundContex tsc;
  tsc._transport = this;
  tsc._ctr = controllers;
  tsc._length = lenof(controllers);
  tsc._inside = inside;
  tsc._deltaT = deltaT;
  tsc._engPos = vs.PositionModelToWorld(GetEnginePos());
  tsc._speed = vs.Speed();
  tsc._obstruction = 1.0f;
  tsc._occlusion = 1.0f;

  // we currently assume the engine sound is correct outside however we do not want to occlude it inside,
  // we only change its volume instead
  if (!inside) 
  {
    GWorld->CheckSoundObstruction(this, inside, tsc._obstruction, tsc._occlusion); 
  }

  for (int i = 0; i < _sounds.Size(); i++)
  {
    const SubmixPars &subPars = tType->_submixPars[i];
    _sounds[i]->UpdateSubmixWaves(subPars._sParsEx, tsc);
  }

  for (int i = 0; i < _soundsEvents.Size(); i++)
  {
    const SubmixEventPars &subEventPars = tType->_submixEventPars[i];
    _soundsEvents[i]->UpdateSubmixWaves(subEventPars._sEventPars, tsc);
  }

  // start & stop sounds
  if (!_holder) _holder = new StartStopHolder();
  _holder->Sound(this);

  if (_showDmg)
  {
    const SoundPars &sound=GetType()->GetDmgSound();

    float freq=sound.freq;
    float vol=sound.vol;
    if (sound.name.GetLength() > 0 && (inside || GSoundScene->CanBeAudible(WaveEffect,vs.Position(),vol)))
    {
      if (!_dmgSound)
      {
        _dmgSound = GSoundScene->OpenAndPlay(
          sound.name, vs.PositionModelToWorld(GetType()->_showDmgPoint), vs.Speed(), vol, freq, sound.distance);
      }
      if (_dmgSound)
      {
        float obstruction = 1, occlusion = 1;
        // warnings are obstructed by the vehicle itself
        GWorld->CheckSoundObstruction(this,true,obstruction,occlusion);
        _dmgSound->Set3D(!inside);
        _dmgSound->SetVolume(vol,freq); // volume, frequency
        _dmgSound->SetPosition(vs.PositionModelToWorld(GetType()->_showDmgPoint), vs.Speed());
        _dmgSound->SetObstruction(obstruction,occlusion,deltaT);
      }
    }
  }
  else
  {
    _dmgSound.Free();
  }

  if( _locked > LdSFree && CommanderUnit() && CommanderUnit() == GWorld->FocusOn())
  {
    const SoundPars &sound= (_locked == LdSIncomigMissile)? GetType()->GetIncommingMissileSound():GetType()->GetLockedSound();
    float freq=sound.freq;
    //float vol=sound.vol;
    if(sound.name.GetLength() > 0)
    {
      if(!_lockedSound)
      {
        _lockedSound = GSoundScene->OpenAndPlay2D(
          sound.name, 1, freq);
      }
      if(_lockedSound)
      {
        //float obstruction = 1, occlusion = 1;
        // warnings are obstructed by the vehicle itself
        //GWorld->CheckSoundObstruction(this,true,obstruction,occlusion);
        _lockedSound->SetVolume(1,freq); // volume, frequency
        _lockedSound->SetPosition(vs.Position(),Vector3(0,0,0));
        //_lockedSound->SetObstruction(obstruction,occlusion,deltaT);
      }
    }
  }
  else
  {
    _lockedSound.Free();
  }

  if( _doorSound )
  {
    float obstruction = 1, occlusion = 1;
    GWorld->CheckSoundObstruction(this,true,obstruction,occlusion);
    const SoundPars &pars=GetType()->GetGetOutSound();
    float vol=pars.vol;
    _doorSound->SetVolume(vol); // volume, frequency
    _doorSound->Set3D(!inside);
    _doorSound->SetPosition(vs.Position(), vs.Speed());
    _doorSound->SetObstruction(obstruction,occlusion,deltaT);
  }

  if (_doCrash != CrashNone && Glob.time > _timeCrash + 1.0)
  {
    _timeCrash = Glob.time;
    const SoundPars *pars = NULL;

    switch (_doCrash)
    {
    case CrashObject:
      pars = &GetType()->GetCrashSound();
      break;

    case CrashLand:
      pars = &GetType()->GetLandCrashSound();
      break;

    case CrashWater:
      pars = &GetType()->GetWaterCrashSound();
      break;

    case CrashBuilding:
      pars = &GetType()->GetBuildingCrashSound(GRandGen.RandomValue());
      break;

    case CrashTree:
      pars = &GetType()->GetWoodCrashSound(GRandGen.RandomValue());
      break;

    case CrashArmor:
      pars = &GetType()->GetArmorCrashSound(GRandGen.RandomValue());
      break;
    }

    if (pars)
    {
      float volume=pars->vol*_crashVolume;
      if (inside) volume *= GetInsideSoundCoeff();

      if (pars->name.GetLength() > 0 && GSoundScene->CanBeAudible(vs.Position(), pars->distance))
      {
        float freq=pars->freq;
        AbstractWave *sound=GSoundScene->OpenAndPlayOnce(
          pars->name,this,false,vs.Position(),vs.Speed(),volume,freq, pars->distance);

        if (sound)
        {
          GSoundScene->SimulateSpeedOfSound(sound);
          GSoundScene->AddSound(sound);
        }
      }
    }
  }
  _doCrash=CrashNone;

  for (int i=0; i<_turrets.Size(); i++)
  {
    TurretType *turretType = Type()->_turrets[i];
    Turret *turret = _turrets[i];
    turret->Sound(this,*turretType, inside, deltaT, unconst_cast(vs), vs.Speed());
  }

  //For each crew call Man::UpdateSpeechPosition
  CrewSoundFunc crewSound(this, deltaT);
  ForEachCrew(crewSound);
#endif

  base::Sound(inside,deltaT);
}

bool Transport::ListenerIsInside(Person *listener) const
{
  // if person is a driver or a gunner of some turret, check if it is hidden or not
  if (listener == Driver())
  {
    if (!Type()->_hideProxyInCombat) return true;
    return FutureVisualState().IsDriverHidden();
  }
  else
  {
    TurretContextV context;
    if (FindTurret(unconst_cast(RenderVisualState()), listener, context))
    {
      // when _viewGunnerInExternal is set, we assume gunner is actually outside
      return context._turret && (!Type()->_hideProxyInCombat || context._turretVisualState->IsGunnerHidden()) && !context._turretType->_viewGunnerInExternal;
    }
  }
  // not a gunner, not a driver - if vehicle matches, it must be a cargo
  // cargo is currently supposed to be always inside
  return listener->GetHierachyParent() == this;
}

void Transport::OccludeAndObstructSound(EntityAI *focus, float &occlusion, float &obstruction, ObstructType soundInside) const
{
  //if (focus==this && soundInside ) return;

  // focus should be a crew
  Person *crew = dyn_cast<Person>(focus);
  // turned out crews have no occlusions/obstructions
  bool soundIsInside = soundInside==SameInside;
  bool focusInside = !crew || ListenerIsInside(crew);
  if (focusInside!=soundIsInside)
  {
    float occ = Type()->_occludeSoundsWhenIn;
    float obs = Type()->_obstructSoundsWhenIn;
    if (soundInside==SameOutside)
    {
      // limited occlusion for our own sounds
      // assume they are partially transmitted through the vehicle body
      static float maxSameOcc = 0.5f;
      static float maxSameObs = 0.8f;
      saturateMax(occ,maxSameOcc);
      saturateMax(obs,maxSameObs);
    }
    occlusion *= occ;
    obstruction *= obs;
  }
}

void Transport::UnloadSound()
{
  _engineSound.Free();
  _envSound.Free();
  _dmgSound.Free();

  _holder.Free();

  if(_lockedSound) _lockedSound.Free();

  _sounds.Clear();
  _soundsEvents.Clear();

  for (int i=0; i<_turrets.Size(); i++)
  {
    Turret *turret = _turrets[i];
    turret->UnloadSound();
  }

  base::UnloadSound();
}

bool Transport::IsFireEnabled(const TurretContext &context)
{
  // who is firing
  if (IsManualFire())
    return EffectiveObserverUnit() != NULL;
  else
    return context._gunner && context._gunner->Brain();
  /*
  if (Type()->HasGunner())
  {
  if (!EffectiveGunnerUnit()) return false;
  }
  else
  {
  if (!PilotUnit()) return false;
  }
  return true;
  */
}

#define DIAG_SPEED 0

#include "dikCodes.h"

bool Transport::StopAtStrategicPos() const
{
  switch (_moveMode)
  {
    case VMMBackward:
    case VMMSlowForward:
    case VMMForward:
    case VMMFastForward:
      return false;
    default:
      return true;
  }
}

float Transport::CalculateDriverPos(float &headChange, float minDist, Vector3 &instant)
{
  instant = _driverPos;
  const float reactionTime = DriverReactionTime;
  float speedCoef = 1.0f;
  switch (_moveMode)
  {
    case VMMFormation:
      Fail("No command");
      return speedCoef;
    case VMMMove:
      break;
    case VMMStop:
      if (_turnMode == VTMAbs)
      {
        Vector3 dir = DirectionWorldToModel(_dirWanted);
        headChange = atan2(dir.X(), dir.Z());
        if (fabs(headChange) < 0.01 * H_PI) _turnMode = VTMNone;
      }
      return speedCoef;
    case VMMBackward:
      {
        speedCoef = -1.0;
        Vector3 from = FutureVisualState().Position() + FutureVisualState().Speed()*reactionTime;
        float maxSpeed = GetType()->GetMaxSpeedMs()*0.5f;
        instant = from - (CmdPlanDist+maxSpeed*speedCoef) * _dirWanted;
        if (_turnMode != VTMNone)
          _turnMode = VTMNone;
        else
        {
          float dist2 = (_driverPos - FutureVisualState().Position()).SquareSizeXZ();
          if (dist2 >= Square(minDist))
            break;
        }
        _driverPos = instant;
        break;
      }
    case VMMSlowForward:
      speedCoef = 0.5;
      goto Forward;
    case VMMForward:
      speedCoef = 1.0;
      goto Forward;
    case VMMFastForward:
      speedCoef = 2.0;
      goto Forward;
Forward:
      {
        Vector3 from = FutureVisualState().Position() + FutureVisualState().Speed()*reactionTime;
        float maxSpeed = GetType()->GetMaxSpeedMs()*0.5f;
        instant = from + (CmdPlanDist+maxSpeed*speedCoef) * _dirWanted;
        if (_turnMode != VTMNone)
          _turnMode = VTMNone;
        else
        {
          float dist2 = (_driverPos - FutureVisualState().Position()).SquareSizeXZ();
          if (dist2 >= Square(minDist))
            break;
        }
        _driverPos = instant;
        break;
      }
  }
  return speedCoef;
}

#if _ENABLE_AI

void Transport::CommandPilot(SteerInfo &info)
{
  // driver has any command
  info.speedWanted = 0;
  info.headChange = 0;
  info.turnPredict=0;

  AIBrain *unit = PilotUnit();
  Assert(unit);
  if (unit->GetAIDisabled() & (AIUnit::DAMove | AIUnit::DATeamSwitch)) return;

  if (_turnMode == VTMLeft)
  {
    info.speedWanted = FutureVisualState().ModelSpeed().Z();
    if (VMMBackward == _moveMode && ReverseLeftRightAIPilot())
      info.headChange = +0.25 * H_PI;
    else
    info.headChange = -0.25 * H_PI;
    return;
  }
  else if (_turnMode == VTMRight)
  {
    info.speedWanted = FutureVisualState().ModelSpeed().Z();
    if (VMMBackward == _moveMode && ReverseLeftRightAIPilot())
      info.headChange = -0.25 * H_PI;
    else
    info.headChange = +0.25 * H_PI;
    return;
  }

  Vector3 instantPos;
  float speedCoef = CalculateDriverPos(info.headChange,CmdMinDist,instantPos);

  if (_moveMode==VMMFormation || _moveMode==VMMStop)
  {
    return;
  }

  if (_moveMode==VMMMove)
  {
    if (unit != CommanderUnit())
    {
      // FIX: need to check wanted values, not the current ones
      if (unit->GetPlanningModeWanted() != AIUnit::VehiclePlanned || unit->GetWantedPosition().Distance2(_driverPos) > Square(1))
      {
        // FIX: clean up the path to avoid fulfillment of previous destination
        unit->SetWantedDestination(PlanPosition(_driverPos,false), AIUnit::DoNotPlan, true);
        unit->SetWantedPosition(PlanPosition(_driverPos,false), 0, AIUnit::VehiclePlanned, false);
      }
    }

    // driving to some position
    LeaderPathPilot(unit, info, speedCoef);
    return;
  }

  if (unit->GetPlanningMode() != AIUnit::VehiclePlanned || unit->GetWantedDestination().Distance2(_driverPos) > Square(1))
  {
    FindDriverPath(_driverPos,false);
  }

  // directional driving
  DriverPathPilot(unit, info, speedCoef, instantPos);
}

void Transport::FindDriverPath(Vector3Par pos, bool forceReplan)
{
  //AIUnit *unit = PilotUnit();
  //unit->SetWantedPosition(pos, AIUnit::VehiclePlanned, forceReplan);
}

void Transport::DriverPathPilot(AIBrain *unit, SteerInfo &info, float speedCoef, Vector3Par instant)
{
  // head towards _driverPos
  Vector3 pos = unit->NearestIntersection(FutureVisualState().Position(),instant);
  //NearestIntersection
  // head toward pos - only if pos is impossible, slow down
  float dist = pos.Distance(FutureVisualState().Position());

  Vector3 posRel = PositionWorldToModel(pos);
  if (speedCoef >= 0)
    info.headChange = atan2(posRel.X(), posRel.Z());
  else
    info.headChange = atan2(-posRel.X(), -posRel.Z());
  if (info.headChange>H_PI/2)
    Log("Big headChange %.2f",info.headChange*180/H_PI);

  info.speedWanted = 0;
  float precision = GetPrecision();
  if (dist>=precision*0.25f)
    info.speedWanted = dist*0.3f * speedCoef;

  float speedMin = floatMax(1,GetType()->GetMaxSpeedMs()*0.1f);
  if (speedCoef>0)
    saturateMax(info.speedWanted,speedMin*speedCoef);
  else if (speedCoef<0)
    saturateMin(info.speedWanted,speedMin*speedCoef);

  if (fabs(info.speedWanted)>=0.01)
    EngineOn();

  //LeaderPathPilot(unit, speedWanted, headChange, turnPredict, speedCoef);
}

/*!
\patch 1.82 Date 8/14/2002 by Ondra
- Fixed: MP: When AI soldier was gunner in vehicle where driver was player who was not group leader,
turret movement was not visible on driver's computer.
*/

void Transport::AIGunner(TurretContextEx &context, float deltaT )
{
  if( _isDead || _isUpsideDown ) return;

  AIBrain *unit = context._gunner->Brain();
  Assert(unit);

  int selected = context._weapons->ValidatedCurrentWeapon();

  if (!context._weapons->_fire._fireTarget)
  {
    // aim weapon based on formation / watch
    /*
    AIBrain *commander = EffectiveObserverUnit();
    if (!commander) commander = unit;
    */
    if (Glob.time < _turretFrontUntil)
    {
      AimWeapon(context, selected, FutureVisualState().Direction());
      return;
    }
    _turretFrontUntil = TIME_MIN;

    Vector3Val watchDir = unit->GetWatchDirection();
    float watchDirSize2 = watchDir.SquareSize();

    if (watchDirSize2 > 0.1f) AimWeapon(context, selected, watchDir);
    return;
  }

  // note: vehicle may be remote
  AimWeaponTgt(context, selected, context._weapons->_fire._fireTarget);

  if (selected < 0) return;

  if (context._weapons->_fire._firePrepareOnly) return;

  AIBrain *obsUnit = EffectiveObserverUnit();
  if (obsUnit && obsUnit->IsPlayer() && !context._weapons->_fire._fireCommanded)
  {
    // fire individually only with autoFire weapons
    const WeaponModeType *mode = context._weapons->_magazineSlots[selected]._weaponMode;
    if (!mode || !mode->_autoFire) return;
  }

  // we will fire very soon - preload what is needed
  PreloadFireWeaponEffects(*context._weapons, selected);

  // manual fire disable gunner only in the primary turret
  bool manualFire = false;
  if (context._turretType && context._turretType->_primaryGunner)
  {
    manualFire = IsManualFire();
  }

  // check if weapon is aimed
  if
    (
    GetWeaponLoaded(*context._weapons, selected) 
    && GetWeaponReady(*context._weapons, selected, context._weapons->_fire._fireTarget) && !manualFire
    )
  {
    float aimed = GetAimed(context, selected, context._weapons->_fire._fireTarget, false, true);
    if( aimed >= 0.75)
    {
      //weapon is not locked
      if(!GetWeaponLockReady(context, context._weapons->_fire._fireTarget, selected, aimed)) return;

    if (!GetAIFireEnabled(context._weapons->_fire._fireTarget)) ReportFireReady();
    else
    {
        if(!unit->IsFriendly(context._weapons->_fire._fireTarget->GetSide())) 
          FireWeapon(context, selected, context._weapons->_fire._fireTarget->idExact, false);
    }
    //    LogF("Fired: aimed %.3f", GetAimed(selected, weapons._fire._fireTarget));
    //    _firePrepareOnly = true;
    context._weapons->_fire._fireCommanded = false;
  }
  }
}

void Transport::OnIncomingMissile(EntityAI *target, Shot *shot, EntityAI *owner)
{
  Missile *missile = dyn_cast<Missile>(shot);
  if(missile)
  {
    if (target && target->IsEventHandler(EEIncomingMissile))
      target->OnEvent(EEIncomingMissile,shot->Type()->GetName(),owner);

    if(IsLocal())
    { 
      //add all missiles to list
      //CM can confuse it even if we cannot detect it
      _incomingMissiles.Add(missile);
    }
  }
}

void Transport::OnWeaponLock(EntityAI *target,Person *gunner, bool locked)
{
  if(locked && gunner) 
  {
    AIBrain *observer = EffectiveObserverUnit();
    if(!gunner->Brain() || !gunner->Brain()->GetVehicle() || !observer) return;
    //ignore friendly lock
    if(gunner->Brain()->IsFriendly(observer->GetSide())) return; 
    if(_targetingEnemys.AddUnique(gunner)>=0)//another vehicle locked this transport; add all, so we can break lock with CM
    {
      TurretContextV context;
      EntityAIFull *vehicle = gunner->Brain()->GetVehicle();
      if(!vehicle->FindTurret(unconst_cast(vehicle->FutureVisualState()), gunner, context)) return;
      if(context._weapons->ValidatedCurrentWeapon() < 0) return;
      //we cannot detect unit's lock
      if((context._weapons->_magazineSlots[context._weapons->ValidatedCurrentWeapon()]._weapon->_weaponLockSystem & GetType()->GetLockDetectionSystem()) == 0) 
        return;

      //reveal detected unit
      if(observer && observer->GetGroup() && observer->GetGroup()->Leader())
        observer->GetGroup()->Leader()->AddTarget(vehicle,1,1,0);
      //do not react on friendly lock
      if(observer->IsFriendly(gunner->Brain()->GetSide())) return;


      //weapon has locked on existing flares
      if(_activeCounterMeasures.Size()>0) 
      {
        TurretContextV context;
        if(!vehicle->FindTurret(unconst_cast(vehicle->FutureVisualState()), gunner, context)) return;

        if(gunner->IsLocal()) context._weapons->TestCounterMeasures(_activeCounterMeasures[0],_activeCounterMeasures.Size());
        else GetNetworkManager().OnLaunchedCounterMeasures(_activeCounterMeasures[0], vehicle, _activeCounterMeasures.Size());
      }
    }
  }
  else if(gunner) for (int i= 0; i<_targetingEnemys.Size(); i++)
  {//another vehicle released lock
    if(_targetingEnemys[i] == gunner) 
    {
      _targetingEnemys.DeleteAt(i);
      return;
    }
  }
}

bool Transport::LaunchCM(Missile *shot, Person *person, bool forced)
{
  TurretContextV fireContext;
  //player's CM launch
  if(!person) return false;
  if(forced && FindTurret(unconst_cast(FutureVisualState()), person,fireContext))
  {
    int currentCM = fireContext._weapons->ValidatedCurrentCounterMeasures();
    if (currentCM >= 0)
    {
      const WeaponType *type = fireContext._weapons->_magazineSlots[currentCM]._weapon;
      if (type->GetSimulation() == WSCMLauncher)
      {
        fireContext._weapons->SelectCounterMeasures(person,currentCM);
        return FireWeapon(fireContext,currentCM, NULL, false);
      }
    }
  }

  return false;
}

/*!
\patch 5164 Date 6/15/2007 by Bebul
- Fixed: Turret cannot move in 3rd person view without weapons
*/
void Transport::LookAround(ValueWithCurve aimX, ValueWithCurve aimY, float deltaT, const ViewPars *viewPars, AIBrain *unit, bool forceLookAround, bool forceDiveAround)
{
  bool discrete = false;
  if (GWorld->GetCameraType()==CamGunner)
  {
    AIBrain *unit = GWorld->FocusOn();
    Person *person = unit ? unit->GetPerson() : NULL;
    TurretContext context;
#if _VBS3 //there are viewPars for turned in and out
    if (person && FindOpticsTurret(person, context) && context._turret)
    {
      viewPars = context._turret->GetCurrentViewPars();
    }
#else
    if (person && FindTurret(person, context) && context._turret)
    {
      int opticsMode = (context._turret)?context._turret->_currentViewOpticsMode : 0;
      viewPars = &context._turretType->_viewOptics[opticsMode];
      //if there are multiple optics, zoomin/out will cycle trough them
      if(context._turretType->_viewOptics.Size() > 1) discrete = true;
    }
#endif
    else viewPars=&Type()->_viewOptics;
  }
  // aiming is scaled based on current FOV
  // default FOV is assumed to be 0.7
  float fov = GetCameraFOV();
  //float fovFactor = Square(fov*(1.0/0.7));
  static float lookSpeed = 1.0f/0.7f;
  static float maxXSpeed = 6.0f;
  static float maxYSpeed = 4.0f;
  float fovFactor = fov*lookSpeed;

  // factors are combined with Input::MouseRangeFactor and applied during curve.GetValue()
  aimX.MultFactor(fovFactor);
  aimY.MultFactor(fovFactor);

  // look around
  float headingSpeed = -aimX.GetValue(fovFactor);
  saturate(headingSpeed, -maxXSpeed, +maxXSpeed);
  float diveSpeed = -aimY.GetValue(fovFactor);
  saturate(diveSpeed, -maxYSpeed, +maxYSpeed);
  //GWorld->ChangeCameraDirection(GWorld->GetCameraType(), headingSpeed * deltaT, diveSpeed * deltaT);
  if (unit)
  {
    //Assert(unit->GetVehicleIn()==this);
    Person *player = unit->GetPerson();
    //Get look around actions and continuous look around actions (TrackIR)
    if (player && GWorld->GetCameraType()!=CamGunner)
    {
      player->UpdateHeadLookAround(deltaT, forceLookAround); //Get LookAround Actions
      //Simulate and saturate LookAround angles
      player->ProcessLookAround(headingSpeed * deltaT, diveSpeed * deltaT, viewPars, deltaT, forceLookAround, forceDiveAround);
    }
    Zoom *zoom = GetZoom(); //Get the proper Zoom instance (optics of driver, gunner, observer, ...)
    UpdateZoom(unit); //updates UALockTarget and determine whether to zoom in, or not
    if (zoom) zoom->Update(deltaT, this, viewPars, discrete); //Get Zooming Actions and both process and saturate zoom
  }
}

/*!
\patch 5128 Date 2/12/2007 by Bebul
- Fixed: Zoom instead of optics in transport
\patch 5134 Date 2/26/2007 by Bebul
- Fixed: Look around in vehicles (faster and better limits now)
*/
void Transport::UpdateZoom(AIBrain *unit)
{
  Person *player = unit->GetPerson();
//   static bool doIt = false;
//   if (GetZoom() && doIt) GetZoom()->_zoomInsteadOfOptics=false; //patch zoomInsteadOfOptics
  if (player && GInput.GetAction(UALockTarget) && (GWorld->IsCameraActive(CamInternal) || GWorld->IsCameraActive(CamExternal)))
  {
//    EntityAIFull *veh = FocusOn()->GetVehicle();
//    Assert(veh);
    //which turret?
    TurretContext context;
//    if (veh->GetPrimaryGunnerTurret(context))
    if (FindTurret(player, context))
    {
      int curWeapon = context._weapons->ValidatedCurrentWeapon();
      bool allowZoom = true;
      if (curWeapon >= 0)
      {
        const MagazineSlot &slot = context._weapons->_magazineSlots[curWeapon];
        const MuzzleType *muzzle = slot._muzzle;
        bool canLock =
          muzzle->_canBeLocked == 2 ||
          muzzle->_canBeLocked == 1 && Glob.config.IsEnabled(DTAutoGuideAT);
        if (canLock) allowZoom = false;
      }
      if( allowZoom )
      {
        // zoom in
//        forcedZoom=true;
        if (GetZoom())
          GetZoom()->_zoomInsteadOfOptics=true;
      }
    }
  }
}

Object::Zoom *Transport::GetZoom()
{
  AIBrain *unit = GWorld->FocusOn();
  Person *man = unit ? unit->GetPerson() : NULL;
  if (!man) return NULL;
  if (GWorld->CameraOn()!=unit->GetVehicle()) return NULL; //camera is not in this vehicle
  if (GWorld->GetCameraType()!=CamGunner) return man->GetZoom();
  if (man == _driver)
    return &_zoom;
  TurretContext context;
  if (FindTurret(man, context) && context._turret)
    return context._turret->GetZoom();
  //cargo ???
  //man is not driver nor gunner
  return man->GetZoom(); //TODO
}

float Transport::GetCameraFOV() const
{
  Object::Zoom* zoom = const_cast<Transport *>(this)->GetZoom();
  return zoom ? zoom->_camFOV : base::GetCameraFOV();
}

void Transport::KeyboardLookAround(AIBrain *unit, float deltaT)
{
  if( _isDead || _isUpsideDown ) return;
  Assert(unit);

  // aim and turn with aiming actions
  ValueWithCurve aimX(0, NULL), aimY(0, NULL);
  aimX = GInput.GetActionWithCurve(UAAimHeadRight, UAAimHeadLeft)*Input::MouseRangeFactor;
  aimY = GInput.GetActionWithCurve(UAAimHeadUp, UAAimHeadDown)*Input::MouseRangeFactor;
  LookAround(aimX, aimY, deltaT, &Type()->_viewCargo, unit, true);
}


CombatMode Transport::GetCombatModeAutoDetected(const AIBrain *brain) const
{
  if (brain->IsAnyPlayer())
  {
    const TransportType *type = Type();
    if (type->_hideProxyInCombat)
    {
      bool hidden = false;
      Person *person = brain->GetPerson();
      if (person == Driver())
        hidden = FutureVisualState().IsDriverHidden();
      else
      {
        TurretContextV context;
        if (FindTurret(unconst_cast(FutureVisualState()), person, context) && context._turret)
        {
          // when the gunner cannot turn out/it, we cannot rely on testing it
          // to make sure we are able to fight, we rather assume the player wants to fight
          hidden = context._turretType->_viewGunnerInExternal || context._turretVisualState->IsGunnerHidden();
        }
      }
      return hidden ? CMAware : CMSafe;
    }
  }


  return CMSafe;
}

void Transport::KeyboardObserver(TurretContextEx &context, float deltaT)
{
  if (_isDead || _isUpsideDown)
    return;

  // aim and turn with aiming actions
  ValueWithCurve aimX(0, NULL), aimY(0, NULL);
  GetAimActions(aimX, aimY);

  CameraType type = GWorld->GetCameraType();
  if (type != CamGunner)
  {
    // do not move with turret
    if (IsGunnerLookingAround(context))
    {
      SetObserverSpeed(context, 0, 0, deltaT);
      int opticsMode = (context._turret)?context._turret->_currentViewGunnerMode : 0;
      LookAround(aimX, aimY, deltaT, &context._turretType->_viewGunner[opticsMode], context._gunner->Brain(),true);
    }
    else
    {
      int opticsMode = (context._turret)?context._turret->_currentViewGunnerMode : 0;
      LookAround(aimX, aimY, deltaT, &context._turretType->_viewGunner[opticsMode], context._gunner->Brain(),false);
      float fov = GetCameraFOV();
      float fovFactor = fov*(1.0/0.7);
      // factors are combined with Input::MouseRangeFactor and applied during curve.GetValue()
      aimX.MultFactor(fovFactor);
      aimY.MultFactor(fovFactor);
      // move with turret
      SetObserverSpeed(context, aimY.GetValue(fovFactor), -aimX.GetValue(fovFactor), deltaT);
    }
  }
  else
  {
    int opticsMode = (context._turret)?context._turret->_currentViewGunnerMode : 0;
    LookAround(aimX, aimY, deltaT, &context._turretType->_viewGunner[opticsMode], context._gunner->Brain(),false);
    // aiming is scaled based on current FOV
    // default FOV is assumed to be 0.7
    float fov = GetCameraFOV();
    //float fovFactor = Square(fov*(1.0/0.7));
    float fovFactor = fov*(1.0/0.7);

    // factors are combined with Input::MouseRangeFactor and applied during curve.GetValue()
    aimX.MultFactor(fovFactor);
    aimY.MultFactor(fovFactor);

    // cursor-level autoaim
    /*
    if (ShowWeaponAim())
      AutoAimCursor(aimX,aimY,deltaT,fov);
    */

    // move with turret
    SetObserverSpeed(context, aimY.GetValue(fovFactor), -aimX.GetValue(fovFactor), deltaT);
  }
}

bool Transport::IsGunnerLookingAround(const TurretContextV &context) const
{
#if _VBS3 //Commander override
  if(context._turret
    && context._turret->_overriddenByTurret
    && context._turret->_overriddenByTurret->_gunner == context._gunner
    )
    return false;
#endif

  CameraType cameraType = GWorld->GetCameraType();
  bool gunnerLookAround = false;
  if  (context._turret)
    gunnerLookAround = cameraType == CamInternal && !context._turretType->GunnerMayFire(context);
  return cameraType != CamGunner && (GWorld->LookAroundEnabled() || IsTurretLocked(context) || gunnerLookAround);
}

void Transport::KeyboardGunner(TurretContextEx &context, float deltaT)
{
  if (_isDead || _isUpsideDown)
    return;

  if (context._turret && context._gunner == GWorld->PlayerOn() && GWorld->UI()->GetAtilleryTargetLocked())
  {
    AimWeapon(context, context._weapons->ValidatedCurrentWeapon(), GWorld->UI()->GetAtilleryTarget() - FutureVisualState().Position());
    return;
  }

  // aim and turn with aiming actions
  ValueWithCurve aimX(0, NULL), aimY(0, NULL);
  GetAimActions(aimX, aimY);

  bool forceLookAround = false;
#if _VBS3
  if(context._turret && !context._turret->_overriddenByTurret)
#endif
    int opticsMode = (context._turret)?context._turret->_currentViewGunnerMode : 0;
    if (context._turret)
      forceLookAround = !context._turret->_gunStabilized || IsGunnerLookingAround(context);
  /*if (GWorld->GetCameraType() != CamGunner) */LookAround(aimX, aimY, deltaT, &context._turretType->_viewGunner[opticsMode], context._gunner->Brain(),forceLookAround);
  if (!IsGunnerLookingAround(context))
  {
#if _VBS3_LASE_LEAD
    //only for gunner cam?
    if(context._turretType->_canLase && GInput.GetActionToDo(UALase,true,false))
    {
      // sight direction is where the 'laser' points
      Camera *camera = GScene->GetCamera();
      Vector3 camDir = camera->Direction();

      Vector3 targetPos,dir;
      int weapon = context._weapons->ValidatedCurrentWeapon();
      bool found = CalculateLaser(targetPos,dir,context,weapon,camDir);
      if(found)
      {
        float leadTime;
#if 0 //Debug
        Vector3 aimPos;
        if(CalculateAimWeaponPos(context,weapon,aimPos,leadTime,targetPos))
        {
          GScene->DrawDiagSphere(aimPos, 1.0f, PackedColor(Color(1,0,0,1)));
        }
#endif
        Vector3 aimDir;
        if(CalculateAimWeapon(context,weapon,aimDir,leadTime,targetPos))
        {
          context._turret->Lase(targetPos, aimDir, camDir);
        }
      }
    }
#endif //_VBS3_LASE_LEAD

    // aiming is scaled based on current FOV
    // default FOV is assumed to be 0.7
    float fov = GetCameraFOV();
    //float fovFactor = Square(fov*(1.0/0.7));
    float fovFactor = fov*(1.0/0.7);

    // factors are combined with Input::MouseRangeFactor and applied during curve.GetValue()
    aimX.MultFactor(fovFactor);
    aimY.MultFactor(fovFactor);

    // cursor-level autoaim
    /*
    if (ShowWeaponAim())
      AutoAimCursor(aimX,aimY,deltaT,fov);
    */

    if (context._turret)
    {
      context._turret->SetSpeed(*context._turretVisualState, *context._turretType, aimY.GetValue(fovFactor), -aimX.GetValue(fovFactor), deltaT, context._parentTrans);
      Assert(context._gunner && context._gunner->QIsManual());
      ManualControlMissile(context);
    }
  }
}

bool Transport::GIsManual(const Person *gunner) const
{
  if (!gunner)
    return false;
  if (!GLOB_WORLD->PlayerManual())
    return false;
  if (!GLOB_WORLD->PlayerOn())
    return false;
  return GLOB_WORLD->PlayerOn() == gunner;
}

// adjust weapon for distance fov
Vector3 Transport::AdjustWeapon(const TurretContext &context, int weapon, float fov, Vector3 direction) const
{
  if (weapon<0 || weapon>=context._weapons->_magazineSlots.Size())
    return direction;
  
  const MagazineSlot &mag = context._weapons->_magazineSlots[weapon];
  const Magazine *magazine = mag._magazine;
  if (!magazine)
    return direction;
  const MagazineType *aInfo = magazine->_type;
  if (!aInfo)
    return direction;

  if(GWorld->UI()->GetAtilleryTargetLocked()) 
    return direction;

  const MuzzleType *muzzle = mag._muzzle;
  Vector3 newDirection;

  if (!muzzle || muzzle->_ballisticsComputer == 0) return direction;

  if (muzzle->_ballisticsComputer == 1  && GWorld->UI()->GetLockedTarget()) 
  {
    Target *target = GWorld->UI()->GetLockedTarget();

    // 18.12.2012 - ball. computer removed https://dev-heaven.net/issues/6823
    // instead algorithm for AI is used

    // static BallisticsComputer bc;
    //// if target can be hit (according to the ballistics computer), correct the view accordingly
    //if (bc.Compute(this, context, GWorld->UI()->GetLockedTarget()))
    //{
    //  newDirection = direction;
    //  newDirection[0] -= bc._targetCorrection[0] / bc._targetCorrection[2];
    //  newDirection[1] -= bc._targetCorrection[1] / bc._targetCorrection[2];

    //  return newDirection;
    //}

    Vector3 tgtAimPos;
    float timeToLead(0);
    if(target->idExact && CalculateAimWeaponPos(context, weapon, tgtAimPos, timeToLead, target, true))
    {
      Vector3 targetPosition = target->idExact->FutureVisualState().Position();
      Vector3 correction = tgtAimPos - targetPosition;
      float distance = FutureVisualState().PositionModelToWorld(GetWeaponPoint(FutureVisualState(), context, weapon)).Distance(targetPosition);
      return (direction.Normalized() * distance + correction).Normalized();
    }
    else return direction;
  }


  if(!context._turretType || !context._turret) return direction;

  // calculate adjustment distance
  // interpolate between adjustment distances depending on fov
  float distance = 0;
  if(context._turretType->_discreteDistance.Size() > 0)
  {
    distance =  context._turretType->_discreteDistance[std::min(context._turret->_distanceIndex,context._turretType->_discreteDistance.Size()-1)];
  }
  else return direction;

  const AmmoType *ammo = aInfo->_ammo;
  float fall = 0.0f;
  Vector3 dir = direction.Normalized();
  PreviewBallistics(Vector3(0,0,0), direction*distance, aInfo, ammo, fall);

  float fallPerM = fall / distance;
  // this correction should be applied to weapon aiming
  newDirection = direction;
  newDirection[1] += fallPerM;

  return newDirection;
}

bool Transport::FormationPilot(SteerInfo &info)
{
  if (_userStopped || !PilotUnit())
  {
    info.speedWanted=0;
    info.headChange=0;
    info.turnPredict=0;
    return true;
  }

  if (_moveMode == VMMFormation)
    return base::FormationPilot(info);
  else
  {
    CommandPilot(info);
    return true;
  }
}

void Transport::LeaderPilot(SteerInfo &info)
{
  if (_userStopped || !PilotUnit())
  {
    info.speedWanted=0;
    info.headChange=0;
    info.turnPredict=0;
    return;
  }

  if (_moveMode == VMMFormation)
    base::LeaderPilot(info);
  else
    CommandPilot(info);
}

#endif // _ENABLE_AI


// crew channel xmit done
void Transport::ReceivedMove( Vector3Par tgt )
{
  _moveMode = VMMMove;
  //  UpdatePath(tgt);
  _driverPos = tgt;

#if DIAG_WANTED_POSITION
  if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
    LogF("Driver %s: _driverPos %.0f, %.0f received", cc_cast(GetDebugName()), tgt.X(), tgt.Z());
#endif
}

void Transport::ReceivedWatchTgt(Person *receiver, Target *tgt)
{
  AIBrain *gunner = receiver ? receiver->Brain() : NULL;
  if (gunner) gunner->SetWatchTarget(tgt);
}

void Transport::ReceivedWatchPos(Person *receiver, Vector3Par pos)
{
  AIBrain *gunner = receiver ? receiver->Brain() : NULL;
  if (gunner) gunner->SetWatchPosition(pos);
}

void Transport::ReceivedJoin()
{
  _moveMode = VMMFormation;
//  _useStrategicPlan = false;

  if (CommanderUnit() && CommanderUnit()->GetUnit())
  {
    AISubgroup *subgrp = CommanderUnit()->GetUnit()->GetSubgroup();
    if (subgrp)
    {
      AIGroup *grp = subgrp->GetGroup();
      Assert(grp);
      if (subgrp != grp->MainSubgroup())
      {
        subgrp->JoinToSubgroup(grp->MainSubgroup());
      }
    }
  }

  // delete current plan
//  if (PilotUnit()) PilotUnit()->SetState(AIUnit::Wait);
  if (PilotUnit())
    PilotUnit()->SetWantedPosition(PlanPosition(_driverPos,false), 0, AIUnit::DoNotPlan, true);
}

void Transport::ReceivedFire(Person *receiver, Target *tgt)
{
  TurretContext context;
  if (!FindTurret(receiver, context)) return;

  AIBrain *unit = receiver ? receiver->Brain() : NULL;
  context._weapons->_fire.SetTarget(unit, tgt);
  if (EffectiveObserverUnit() && EffectiveObserverUnit()->IsAnyPlayer())
  {
    if (context._weapons->_fire._initState > TargetEnemy)
      context._weapons->_fire._initState = TargetEnemy;
  }
  context._weapons->_fire._gunner = context._gunner;
  context._weapons->_fire._weapon = context._weapons->ValidatedCurrentWeapon();
  context._weapons->_fire._firePrepareOnly = false;
  context._weapons->_fire._fireCommanded = true;
  //LogF("Received fire, weapon %d",_currentWeapon);
}

void Transport::ReceivedTarget(Person *receiver, Target *tgt)
{
  // do not aim / fire on yourself
  if (tgt && tgt->idExact == this) return;

  TurretContext context;
  if (!FindTurret(receiver, context)) return;

  // it is possible the group does not know the target yet
  // however the gunner really should be aware of it now
  // as he was told to target it and he was told the azimuth
  // there are two dangers:
  // 1) gunner knowing late about the unit
  // 2) other units knowing too soon
  // we can switch the "sensor" to make sure gunner knows about him
  // this can cause leader not knowing about the target any more

  if (tgt)
    tgt->RevealInVehicle(this);

  AIBrain *unit = receiver ? receiver->Brain() : NULL;
  context._weapons->_fire.SetTarget(unit, tgt);
  if (EffectiveObserverUnit() && EffectiveObserverUnit()->IsAnyPlayer())
  {
    if (context._weapons->_fire._initState > TargetEnemy)
      context._weapons->_fire._initState = TargetEnemy;
  }
  context._weapons->_fire._gunner = receiver;
  context._weapons->_fire._weapon = context._weapons->ValidatedCurrentWeapon();
  context._weapons->_fire._fireCommanded = false;
  // _fire._firePrepareOnly = true;
  //LogF("Received target, weapon %d",_currentweapon);
}

void Transport::ReceivedSimpleCommand(Person *receiver, SimpleCommand cmd)
{
  const float reactionTime=DriverReactionTime;
  switch (cmd)
  {
    case SCForward:
      if (!_driver) return;
      if (_moveMode == VMMFormation || _moveMode == VMMMove)
      {
        _dirWanted = FutureVisualState().Direction();
        _azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
      }
      _moveMode = VMMForward;
      //      _useStrategicPlan = false;
      _driverPos = FutureVisualState().Position()+FutureVisualState().Speed()*reactionTime;
      //      _completedSent = false;
      break;
    case SCStop:
      if (!_driver) return;
      if (_moveMode == VMMFormation || _moveMode == VMMMove)
      {
        _dirWanted = FutureVisualState().Direction();
        _azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
      }
      _moveMode = VMMStop;
      //      _useStrategicPlan = false;
      _driverPos = FutureVisualState().Position()+FutureVisualState().Speed()*reactionTime;
      //      _completedSent = false;
      break;
    case SCBackward:
      if (!_driver) return;
      if (_moveMode == VMMFormation || _moveMode == VMMMove)
      {
        _dirWanted = FutureVisualState().Direction();
        _azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
      }
      _moveMode = VMMBackward;
      //      _useStrategicPlan = false;
      _driverPos = FutureVisualState().Position()+FutureVisualState().Speed()*reactionTime;
      //      _completedSent = false;
      break;
    case SCFaster:
      if (!_driver) return;
      if (_moveMode == VMMFormation || _moveMode == VMMMove)
      {
        _dirWanted = FutureVisualState().Direction();
        _azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
      }
      _moveMode = VMMFastForward;
      //      _useStrategicPlan = false;
      _driverPos = FutureVisualState().Position()+FutureVisualState().Speed()*reactionTime;
      //      _completedSent = false;
      break;
    case SCSlower:
      if (!_driver) return;
      if (_moveMode == VMMFormation || _moveMode == VMMMove)
      {
        _dirWanted = FutureVisualState().Direction();
        _azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
      }
      _moveMode = VMMSlowForward;
      //      _useStrategicPlan = false;
      _driverPos = FutureVisualState().Position()+FutureVisualState().Speed()*reactionTime;
      //      _completedSent = false;
      break;
    case SCLeft:
    case SCRight:
      if (!_driver) return;
      if (_moveMode == VMMFormation || _moveMode == VMMMove)
      {
        _dirWanted = FutureVisualState().Direction();
        _azimutWanted = atan2(_dirWanted.X(), _dirWanted.Z());
        if (_moveMode == VMMFormation && CommanderUnit() && CommanderUnit()->IsGroupLeader())
          _moveMode = VMMStop;
        else
          _moveMode = VMMForward;
      }
      if( cmd==SCRight )
        _turnMode = VTMRight;
      else
        _turnMode = VTMLeft;
      break;
    case SCFire:
      {
        TurretContextV context;
        if (FindTurret(unconst_cast(FutureVisualState()), receiver, context))
        {
          int selected = context._weapons->ValidatedCurrentWeapon();
          if (selected >= 0)
          {
            if (context._gunner && context._gunner->Brain() && context._gunner->Brain()->IsAnyPlayer())
              // note: we must remember we already ordered player to fire
              context._weapons->_fire._firePrepareOnly = false;
            else
            {
              const WeaponModeType *mode = context._weapons->_magazineSlots[selected]._weaponMode;
              if (mode && mode->_autoFire)
                context._weapons->_fire._firePrepareOnly = false;
              else if (GetWeaponLoaded(*context._weapons, selected))
                FireWeapon(context, selected, context._weapons->_fire._fireTarget ? context._weapons->_fire._fireTarget->idExact : NULL, false);
              else
                SendFireFailed(receiver);
            }
          }
        }
      }
      break;
    case SCCeaseFire:
      {
        TurretContext context;
        if (FindTurret(receiver, context))
          context._weapons->_fire._firePrepareOnly = true;
      }
      break;
    case SCManualFire:
    case SCCancelManualFire:
      break;
    default:
      RptF("Simple command %d", cmd);
      break;
  }
}

void Transport::ReceivedLoad(Person *receiver, int weapon)
{
  //LogF("Receiving load %d->%d", _currentWeapon, weapon);
  // TODO: add addressed gunner to the message
  TurretContext context;
  if (FindTurret(receiver, context))
  {
    if (!(context._gunner && context._gunner->Brain() && context._gunner->Brain()->IsAnyPlayer()))
    {
      //LogF("Received load %d->%d", _currentWeapon, weapon);
      context._weapons->SelectWeapon(this, weapon);
      context._weapons->_fire._gunner = context._gunner;
      context._weapons->_fire._weapon = weapon;
      DoAssert(weapon < context._weapons->_magazineSlots.Size());
      // _fire._firePrepareOnly = true; - do not change this flag, autofire continue even when weapons changed
    }
  }
}

void Transport::ReceivedLoadMagazine(Person *receiver, CreatorId creator, int id, RString weapon, RString muzzle)
{
  TurretContext context;
  if (FindTurret(receiver, context) && context._gunner && context._gunner->Brain() && !context._gunner->Brain()->IsAnyPlayer())
    GunnerLoadMagazine(context, creator, id, weapon, muzzle);
}

void Transport::ReceivedAzimut(float azimut)
{
  _azimutWanted = azimut;
  _dirWanted = Vector3(sin(_azimutWanted), 0, cos(_azimutWanted));
  if (_moveMode == VMMFormation || _moveMode == VMMMove)
    _moveMode = VMMForward;
  _turnMode = VTMAbs;
  //  _useStrategicPlan = false;
  //  _completedSent = false;
}

/*!
\patch 1.52 Date 4/19/2002 by Ondra
- Fixed: Improved tank driver reactions for turning commands.
*/

void Transport::ReceivedStopTurning(float azimut)
{
  _azimutWanted = azimut;
  _dirWanted = Vector3(sin(_azimutWanted), 0, cos(_azimutWanted));
  _turnMode = VTMAbs;
  AIBrain *unit = PilotUnit();
  if (unit)
  {
    // check if moving
    switch (_moveMode)
    {
      case VMMBackward:
      case VMMSlowForward:
      case VMMForward:
      case VMMFastForward:
        {
          // replan path immediatelly - to get short reaction time
          float headChange;
          Vector3 instant;
          CalculateDriverPos(headChange,0,instant);

          FindDriverPath(_driverPos,true);
        }
        break;
    }
  }
}

RadioMessage *CreateVehicleMessage(int type)
{
  switch (type)
  {
    case RMTVehicleMove:
      return new RadioMessageVMove();
    case RMTVehicleFire:
      return new RadioMessageVFire();
    case RMTVehicleFormation:
      return new RadioMessageVFormation();
    case RMTVehicleSimpleCommand:
      return new RadioMessageVSimpleCommand();
    case RMTVehicleTarget:
      return new RadioMessageVTarget();
    case RMTVehicleLoad:
      return new RadioMessageVLoad();
    case RMTVehicleAzimut:
      return new RadioMessageVAzimut();
    case RMTVehicleStopTurning:
      return new RadioMessageVStopTurning();
    case RMTVehicleFireFailed:
      return new RadioMessageVFireFailed();
    case RMTVehicleLoadMagazine:
      return new RadioMessageVLoadMagazine();
    case RMTVehicleWatchTgt:
      return new RadioMessageVWatchTgt();
    case RMTVehicleWatchPos:
      return new RadioMessageVWatchPos();
  }
  Fail("Message Type");
  return NULL;
}

RadioMessageVFireFailed::RadioMessageVFireFailed(Transport *vehicle, Person *sender)
{
  _vehicle = vehicle;
  _sender = sender;
}

LSError RadioMessageVFireFailed::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
  CHECK(ar.SerializeRef("Sender", _sender, 1))
  return LSOK;
}

void RadioMessageVFireFailed::Transmitted(ChatChannel channel)
{
}

AIBrain *RadioMessageVFireFailed::GetSender() const
{
  return _sender ? _sender->Brain() : NULL;
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVFireFailed, MsgVFireFailed)

DEFINE_NET_INDICES_NO_NMT(VMessage, V_MESSAGE_MSG)

#define MSG_V_FIRE_FAILED_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), sender, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Gunner - sender of the message"), TRANSF_REF)

DECLARE_NET_INDICES_EX(MsgVFireFailed, VMessage, MSG_V_FIRE_FAILED_MSG)
DEFINE_NET_INDICES_EX(MsgVFireFailed, VMessage, MSG_V_FIRE_FAILED_MSG)

NetworkMessageFormat &RadioMessageVFireFailed::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  V_MESSAGE_MSG(VMessage, MSG_FORMAT)
  MSG_V_FIRE_FAILED_MSG(MsgVFireFailed, MSG_FORMAT)
  return format;
}

TMError RadioMessageVFireFailed::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(MsgVFireFailed)

  TRANSF_REF(vehicle)
  TRANSF_REF(sender)
  return TMOK;
}

void RadioMessageVFireFailed::Send()
{
  if (_vehicle)
    _vehicle->SendFireFailed(_sender);
}

RString RadioMessageVFireFailed::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle)
    return RString();
  AIBrain *receiver = _vehicle->EffectiveObserverUnit();
  if (!receiver)
    return RString();
  return "VehicleFireFailed";
}

RadioMessageWithTarget::RadioMessageWithTarget(Transport *vehicle, Person *receiver, Target *tgt)
{
  _vehicle = vehicle;
  _receiver = receiver;
  _target = tgt;
}

AIBrain *RadioMessageWithTarget::GetSender() const
{
  return _vehicle ? _vehicle->EffectiveObserverUnit() : NULL;
}

#define MESSAGE_WITH_TARGET_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), receiver, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Gunner - receiver of the message"), TRANSF_REF) \
  XX(MessageName, LinkTarget, target, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Target"), TRANSF_REF)

DECLARE_NET_INDICES_EX_NO_NMT(MessageWithTarget, VMessage, MESSAGE_WITH_TARGET_MSG)
DEFINE_NET_INDICES_EX_NO_NMT(MessageWithTarget, VMessage, MESSAGE_WITH_TARGET_MSG)

NetworkMessageFormat &RadioMessageWithTarget::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  V_MESSAGE_MSG(VMessage, MSG_FORMAT)
  MESSAGE_WITH_TARGET_MSG(MessageWithTarget, MSG_FORMAT)
  return format;
}

TMError RadioMessageWithTarget::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(MessageWithTarget)

  TRANSF_REF(vehicle)
  TRANSF_REF(receiver)

  if (ctx.IsSending())
  {
    OLink(EntityAI) target;
    if (_target) target = _target->idExact;
    TRANSF_REF_EX(target, target)
  }
  else
  {
    _target = NULL;
    OLink(EntityAI) target;
    TRANSF_REF_EX(target, target)
    if (target)
    {
      AIBrain *unit = _vehicle->EffectiveObserverUnit();
      if (unit)
      {
        Target *newTarget = unit->FindTargetAll(target);
        if (!newTarget)
          // add target to target database
          newTarget = unit->AddTarget(target, 4.0f, 4.0f, 0);
        _target = newTarget;
        if (!unit->IsLocal() && _target)
          // update position if commander is remote
          _target->UpdateRemotePosition(target);
      }
    }
  }
  return TMOK;
}

static bool CheckUnitAlive(AIBrain *unit)
{
  // check if unit is alive
  // it should be responding to some message
  // if it is not, it must be dead
  if (!unit) return false;
  if (unit->LSIsAlive()) return true;
  AIGroup *grp = unit->GetGroup();
  // TODO: set also who should report unit is dead (vehicle commander)
  // or do hack that in-vehicle units are always reported by the vehicle commander
  if (grp) grp->SetReportBeforeTime(unit->GetUnit(),Glob.time+10);
  return false;
}

RadioMessageVTarget::RadioMessageVTarget(Transport *vehicle, Person *receiver, Target *tgt)
:base(vehicle, receiver, tgt)
{
}

void RadioMessageVTarget::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!CheckUnitAlive(receiver)) return;

  if (_target)
  {
    if (receiver == GWorld->FocusOn() && GWorld->UI())
      GWorld->UI()->ShowTarget();
  }

  _vehicle->ReceivedTarget(_receiver, _target);
}

LSError RadioMessageWithTarget::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
  CHECK(ar.SerializeRef("Receiver", _receiver, 1))
  CHECK(ar.SerializeRef("Target", _target, 1))
  return LSOK;
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVTarget, MsgVTarget)

#define MSG_V_TARGET_MSG(MessageName, XX)

DECLARE_NET_INDICES_EX(MsgVTarget, MessageWithTarget, MSG_V_TARGET_MSG)
DEFINE_NET_INDICES_EX(MsgVTarget, MessageWithTarget, MSG_V_TARGET_MSG)

void RadioMessageVTarget::Send()
{
  LogF("Resending target");
  if (_vehicle)
    _vehicle->SendTarget(_receiver, _target);
}

/*!
\patch 1.31 Date 11/20/2001 by Ondra.
- New: heading is transmitted when assigning targets to gunner.
This makes finding targets in MP much easier.
*/

RString RadioMessageVTarget::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle) return RString();
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!receiver) return RString();

  if (!_target)
  {
    return "VehicleNoTarget";
  }

  //TargetSide side = _target->side;
  const EntityAIType *type = _target->GetType();
  params.AddWord(type->GetNameSound(), type->GetDisplayName());
  params.AddRelativePosition(_target->AimingPosition()-_vehicle->FutureVisualState().Position());
  return "VehicleTarget";
}

RadioMessageVFire::RadioMessageVFire(Transport *vehicle, Person *receiver, Target *tgt)
:base(vehicle, receiver, tgt)
{
}

void RadioMessageVFire::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!CheckUnitAlive(receiver)) return;

  _vehicle->ReceivedFire(_receiver, _target);
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVFire,MsgVFire)

#define MSG_V_FIRE_MSG(MessageName, XX)

DECLARE_NET_INDICES_EX(MsgVFire, MessageWithTarget, MSG_V_FIRE_MSG)
DEFINE_NET_INDICES_EX(MsgVFire, MessageWithTarget, MSG_V_FIRE_MSG)

void RadioMessageVFire::Send()
{
  if (_vehicle)
    _vehicle->SendFire(_receiver, _target);
}

RString RadioMessageVFire::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle) return RString();
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!receiver) return RString();

  if (_target)
  {
    //TargetSide side = _target->side;
    const EntityAIType *type = _target->GetType();
    params.AddWord(type->GetNameSound(), type->GetDisplayName());
  }
  else
    params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
  return "VehicleFire";
}

RadioMessageVMove::RadioMessageVMove(Transport *vehicle, Vector3Par pos)
{
  _vehicle=vehicle;
  _destination=pos;
}

AIBrain *RadioMessageVMove::GetSender() const
{
  return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

void RadioMessageVMove::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _vehicle->PilotUnit();
  if (!CheckUnitAlive(receiver)) return;

  _vehicle->ReceivedMove(_destination);
}

LSError RadioMessageVMove::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
  CHECK(::Serialize(ar, "destination", _destination, 1))
  return LSOK;
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVMove,MsgVMove)

#define MSG_V_MOVE_MSG(MessageName, XX) \
  XX(MessageName, Vector3, destination, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Destination position"), TRANSF)

DECLARE_NET_INDICES_EX(MsgVMove, VMessage, MSG_V_MOVE_MSG)
DEFINE_NET_INDICES_EX(MsgVMove, VMessage, MSG_V_MOVE_MSG)

NetworkMessageFormat &RadioMessageVMove::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  V_MESSAGE_MSG(VMessage, MSG_FORMAT)
  MSG_V_MOVE_MSG(MsgVMove, MSG_FORMAT)
  return format;
}

TMError RadioMessageVMove::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(MsgVMove)

  TRANSF_REF(vehicle)
  TRANSF(destination)
  return TMOK;
}

void RadioMessageVMove::Send()
{
  if (_vehicle)
    _vehicle->SendMove(_destination);
}

RString RadioMessageVMove::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle) return RString();
  AIBrain *receiver = _vehicle->PilotUnit();
  if (!receiver) return RString();

  const OLinkPermNOArray(AIUnit) recipients; //empty
  _moveToHelper.PrepareSEHelperVariables(receiver, receiver->GetGroup(), recipients, _destination, NULL, NULL);
  _moveToHelper.SetParams(params, this, receiver, _destination);

  return "SelectVehicleMoveSentence";
}

RadioMessageVWatchTgt::RadioMessageVWatchTgt(Transport *vehicle, Person *receiver, Target *target)
: base(vehicle, receiver, target)
{
}

void RadioMessageVWatchTgt::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!CheckUnitAlive(receiver)) return;

  _vehicle->ReceivedWatchTgt(_receiver, _target);
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVWatchTgt, MsgVWatchTgt)

#define MSG_V_WATCH_TGT_MSG(MessageName, XX)

DECLARE_NET_INDICES_EX(MsgVWatchTgt, MessageWithTarget, MSG_V_WATCH_TGT_MSG)
DEFINE_NET_INDICES_EX(MsgVWatchTgt, MessageWithTarget, MSG_V_WATCH_TGT_MSG)

void RadioMessageVWatchTgt::Send()
{
  if (_vehicle)
    _vehicle->SendWatchTgt(_receiver, _target);
}

RString RadioMessageVWatchTgt::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle) return RString();
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!receiver) return RString();

  if (!_target) return RString();

  const EntityAIType *type = _target->GetType();
  params.AddWord(type->GetNameSound(), type->GetDisplayName());
  params.AddRelativePosition(_target->AimingPosition()-_vehicle->RenderVisualState().Position());
  return "VehicleWatchTgt";
}

RadioMessageVWatchPos::RadioMessageVWatchPos(Transport *vehicle, Person *receiver, Vector3Par pos)
{
  _vehicle = vehicle;
  _receiver = receiver;
  _position = pos;
}

LSError RadioMessageVWatchPos::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
  CHECK(ar.SerializeRef("Receiver", _receiver, 1))
  CHECK(::Serialize(ar, "position", _position, 1))
  return LSOK;
}

void RadioMessageVWatchPos::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!CheckUnitAlive(receiver)) return;

  _vehicle->ReceivedWatchPos(_receiver, _position);
}

AIBrain *RadioMessageVWatchPos::GetSender() const
{
  return _vehicle ? _vehicle->EffectiveObserverUnit() : NULL;
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVWatchPos, MsgVWatchPos)

#define MSG_V_WATCH_POS_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), receiver, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Gunner - receiver of the message"), TRANSF_REF) \
  XX(MessageName, Vector3, position, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Watch position"), TRANSF)

DECLARE_NET_INDICES_EX(MsgVWatchPos, VMessage, MSG_V_WATCH_POS_MSG)
DEFINE_NET_INDICES_EX(MsgVWatchPos, VMessage, MSG_V_WATCH_POS_MSG)

NetworkMessageFormat &RadioMessageVWatchPos::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  V_MESSAGE_MSG(VMessage, MSG_FORMAT)
  MSG_V_WATCH_POS_MSG(MsgVWatchPos, MSG_FORMAT)
  return format;
}

TMError RadioMessageVWatchPos::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(MsgVWatchPos)

  TRANSF_REF(vehicle)
  TRANSF_REF(receiver)
  TRANSF(position)
  return TMOK;
}

void RadioMessageVWatchPos::Send()
{
  if (_vehicle)
    _vehicle->SendWatchPos(_receiver, _position);
}

RString RadioMessageVWatchPos::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle) return RString();
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!receiver) return RString();

  Vector3 dir = _position - _vehicle->RenderVisualState().Position();
  int azimut = toInt(0.1 * atan2(dir.X(), dir.Z()) * (180 / H_PI));
  if (azimut < 0) azimut += 36;
  int dist = toInt(0.1 * dir.SizeXZ());
  params.Add("%02d", azimut);
  params.Add(dist);
  return "VehicleWatchPos";
}

RadioMessageVFormation::RadioMessageVFormation( Transport *vehicle )
{
  _vehicle=vehicle;
}

AIBrain *RadioMessageVFormation::GetSender() const
{
  return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

void RadioMessageVFormation::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _vehicle->PilotUnit();
  if (!CheckUnitAlive(receiver)) return;

  _vehicle->ReceivedJoin();
}

LSError RadioMessageVFormation::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
  return LSOK;
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVFormation,MsgVFormation)

#define MSG_V_FORMATION_MSG(MessageName, XX)

DECLARE_NET_INDICES_EX(MsgVFormation, VMessage, MSG_V_FORMATION_MSG)
DEFINE_NET_INDICES_EX(MsgVFormation, VMessage, MSG_V_FORMATION_MSG)

NetworkMessageFormat &RadioMessageVFormation::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  V_MESSAGE_MSG(VMessage, MSG_FORMAT)
  MSG_V_FORMATION_MSG(MsgVFormation, MSG_FORMAT)
  return format;
}

TMError RadioMessageVFormation::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(MsgVFormation)

  TRANSF_REF(vehicle)
  return TMOK;
}

void RadioMessageVFormation::Send()
{
  if (_vehicle)
    _vehicle->SendJoin();
}

RString RadioMessageVFormation::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle) return RString();
  AIBrain *receiver = _vehicle->PilotUnit();
  if (!receiver) return RString();

  return "VehicleJoin";
}

RadioMessageVSimpleCommand::RadioMessageVSimpleCommand(Transport *vehicle, Person *receiver, SimpleCommand cmd)
{
  _vehicle = vehicle;
  _receiver = receiver;
  _cmd = cmd;
}

AIBrain *RadioMessageVSimpleCommand::GetSender() const
{
  switch (_cmd)
  {
    case SCKeyFire:
    case SCFire:
    case SCCeaseFire:
    case SCManualFire:
    case SCCancelManualFire:
      return _vehicle ? _vehicle->EffectiveObserverUnit() : NULL;
  }
  return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

void RadioMessageVSimpleCommand::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (_cmd == SCManualFire || _cmd == SCCancelManualFire)
  {
    if (!CheckUnitAlive(receiver)) receiver = _vehicle->CommanderUnit();
  }
  if (!CheckUnitAlive(receiver)) return;

  _vehicle->ReceivedSimpleCommand(receiver->GetPerson(), MapCommand());
}

static const EnumName SimpleCommandNames[]=
{
  EnumName(SCForward, "FORWARD"),
  EnumName(SCStop, "STOP"),
  EnumName(SCBackward, "BACK"),
  EnumName(SCFaster, "FAST"),
  EnumName(SCSlower, "SLOW"),
  EnumName(SCLeft, "LEFT"),
  EnumName(SCRight, "RIGHT"),
  EnumName(SCFire, "FIRE"),
  EnumName(SCCeaseFire, "CEASE FIRE"),
  EnumName(SCKeyUp, "KEY UP"),
  EnumName(SCKeyDown, "KEY DOWN"),
  EnumName(SCKeySlow, "KEY SLOW"),
  EnumName(SCKeyFast, "KEY FAST"),
  EnumName(SCKeyFire, "KEY FIRE"),
  EnumName(SCManualFire, "MANUAL FIRE"),
  EnumName(SCCancelManualFire, "CANCEL MANUAL FIRE"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(SimpleCommand dummy)
{
  return SimpleCommandNames;
}

LSError RadioMessageVSimpleCommand::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
  CHECK(ar.SerializeRef("Receiver", _receiver, 1))
  CHECK(ar.SerializeEnum("cmd", _cmd, 1))
  return LSOK;
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVSimpleCommand, MsgVSimpleCommand)

#define MSG_V_SIMPLE_COMMAND_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), receiver, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Receiver of the message"), TRANSF_REF) \
  XX(MessageName, int, cmd, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Command"), TRANSF)

DECLARE_NET_INDICES_EX(MsgVSimpleCommand, VMessage, MSG_V_SIMPLE_COMMAND_MSG)
DEFINE_NET_INDICES_EX(MsgVSimpleCommand, VMessage, MSG_V_SIMPLE_COMMAND_MSG)

NetworkMessageFormat &RadioMessageVSimpleCommand::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  V_MESSAGE_MSG(VMessage, MSG_FORMAT)
  MSG_V_SIMPLE_COMMAND_MSG(MsgVSimpleCommand, MSG_FORMAT)
  return format;
}

TMError RadioMessageVSimpleCommand::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(MsgVSimpleCommand)

  TRANSF_REF(vehicle)
  TRANSF_REF(receiver)
  TRANSF_ENUM(cmd)
  return TMOK;
}

void RadioMessageVSimpleCommand::Send()
{
  if (_vehicle)
    _vehicle->SendSimpleCommand(_receiver, _cmd);
}

SimpleCommand RadioMessageVSimpleCommand::MapCommand() const
{
  VehicleMoveMode mode = _vehicle->GetMoveMode();
  if (mode == VMMFormation || mode == VMMMove)
  {
    float speed = _vehicle->FutureVisualState().ModelSpeed().Z();
    if (speed < -0.1) mode = VMMBackward;
    else if (speed > 0.1) mode = VMMForward;
    else mode = VMMStop;
  }

  switch (_cmd)
  {
    case SCKeyUp:
      if (mode == VMMBackward)
        return SCStop;
      else if (mode == VMMForward || mode == VMMFastForward)
        return SCFaster;
      else
        return SCForward;
    case SCKeyDown:
      if (mode == VMMSlowForward || mode == VMMForward || mode == VMMFastForward)
        return SCStop;
      else
        return SCBackward;
    case SCKeySlow:
      if (mode == VMMBackward)
        return SCStop;
      else
        return SCSlower;
    case SCKeyFast:
      if (mode == VMMBackward)
        return SCStop;
      else
        return SCFaster;
      break;
    case SCKeyFire:
      {
        TurretContext context;
        if (_vehicle->FindTurret(_receiver, context))
        {
          // check if CEASE FIRE command is more suitable
          if (!context._weapons->_fire._firePrepareOnly)
          {
            int weapon = context._weapons->ValidatedCurrentWeapon();
            if (weapon >= 0)
            {
              const WeaponModeType *mode = context._weapons->GetWeaponMode(weapon);
              if (mode && mode->_autoFire)
                return SCCeaseFire;
            }
          }
        }
      }
      return SCFire;
    default:
      return _cmd;
  }
}

RString RadioMessageVSimpleCommand::PrepareSentence(SentenceParams &params) const
{
  // translate keys
  if (!_vehicle) return RString();
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;

  if (_cmd == SCManualFire || _cmd == SCCancelManualFire)
  {
    if (!CheckUnitAlive(receiver))
      receiver = _vehicle->EffectiveObserverUnit();
  }
  if (!CheckUnitAlive(receiver))
    return RString();

  SimpleCommand cmd = MapCommand();
  switch (cmd)
  {
    case SCForward:
      return "VehicleForward";
    case SCStop:
      return "VehicleStop";
    case SCBackward:
      return "VehicleBackward";
    case SCFaster:
      return "VehicleFaster";
    case SCSlower:
      return "VehicleSlower";
    case SCLeft:
      return "VehicleLeft";
    case SCRight:
      return "VehicleRight";
    case SCFire:
      return "VehicleDirectFire";
    case SCCeaseFire:
      return "VehicleCeaseFire";
    case SCManualFire:
      return "VehicleManualFire";
    case SCCancelManualFire:
      return "VehicleCancelManualFire";
    default:
      Fail("Simple command");
      return RString();
  }
}

RadioMessageVLoad::RadioMessageVLoad(Transport *vehicle, Person *receiver, int weapon)
{
  _vehicle = vehicle;
  _receiver = receiver;
  _weapon = weapon;
}

AIBrain *RadioMessageVLoad::GetSender() const
{
  return _vehicle ? _vehicle->EffectiveObserverUnit() : NULL;
}

LSError RadioMessageVLoad::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
  CHECK(ar.SerializeRef("Receiver", _receiver, 1))
  CHECK(ar.Serialize("weapon", _weapon, 1))
  return LSOK;
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVLoad, MsgVLoad)

#define MSG_V_LOAD_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), receiver, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Gunner - receiver of the message"), TRANSF_REF) \
  XX(MessageName, int, weapon, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Weapon index"), TRANSF)

DECLARE_NET_INDICES_EX(MsgVLoad, VMessage, MSG_V_LOAD_MSG)
DEFINE_NET_INDICES_EX(MsgVLoad, VMessage, MSG_V_LOAD_MSG)

NetworkMessageFormat &RadioMessageVLoad::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  V_MESSAGE_MSG(VMessage, MSG_FORMAT)
  MSG_V_LOAD_MSG(MsgVLoad, MSG_FORMAT)
  return format;
}

TMError RadioMessageVLoad::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(MsgVLoad)

  TRANSF_REF(vehicle)
  TRANSF_REF(receiver)
  TRANSF(weapon)
  return TMOK;
}

void RadioMessageVLoad::Send()
{
  if (_vehicle)
    _vehicle->SendLoad(_receiver, _weapon);
}

void RadioMessageVLoad::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!CheckUnitAlive(receiver)) return;

  _vehicle->ReceivedLoad(_receiver, _weapon);
}

RString RadioMessageVLoad::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle) return RString();
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!receiver) return RString();

  TurretContext context;
  if (!_vehicle->FindTurret(_receiver, context)) return RString();

  if (_weapon < 0 || _weapon >= context._weapons->_magazineSlots.Size()) return RString();

  const Magazine *magazine = context._weapons->_magazineSlots[_weapon]._magazine;
  if (!magazine) return RString();
  const MagazineType *type = magazine->_type;
  params.AddWord(type->GetNameSound(), type->GetDisplayName());
  return "VehicleLoad";
}

RadioMessageVLoadMagazine::RadioMessageVLoadMagazine(Transport *vehicle, Person *receiver, CreatorId magazineCreator, int magazineId, RString weapon, RString muzzle)
{
  _vehicle = vehicle;
  _receiver = receiver;
  _magazineCreator = magazineCreator;
  _magazineId = magazineId;
  _weapon = weapon;
  _muzzle = muzzle;
}

LSError RadioMessageVLoadMagazine::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
  CHECK(ar.SerializeRef("Receiver", _receiver, 1))
  CHECK(_magazineCreator.Serialize(ar,"creator", 1))
  CHECK(ar.Serialize("magazineId", _magazineId, 1))
  CHECK(ar.Serialize("weapon", _weapon, 1))
  CHECK(ar.Serialize("muzzle", _muzzle, 1))
  return LSOK;
}

void RadioMessageVLoadMagazine::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!CheckUnitAlive(receiver)) return;

  if (receiver->IsAnyPlayer()) return;

  TurretContext context;
  if (_vehicle->FindTurret(_receiver, context))
    _vehicle->GunnerLoadMagazine(context, _magazineCreator, _magazineId, _weapon, _muzzle);
}

AIBrain *RadioMessageVLoadMagazine::GetSender() const
{
  return _vehicle ? _vehicle->EffectiveObserverUnit() : NULL;
}

void RadioMessageVLoadMagazine::SetParams(CreatorId magazineCreator, int magazineId, RString weapon, RString muzzle)
{
  _magazineCreator = magazineCreator;
  _magazineId = magazineId;
  _weapon = weapon;
  _muzzle = muzzle;
}

bool RadioMessageVLoadMagazine::CheckParams(CreatorId magazineCreator, int magazineId, RString weapon, RString muzzle) const
{
  return magazineCreator == _magazineCreator && magazineId == _magazineId &&
    stricmp(weapon, _weapon) == 0 && stricmp(muzzle, _muzzle) == 0;
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVLoadMagazine, MsgVLoadMagazine)

DEFINE_NET_INDICES_EX(MsgVLoadMagazine, VMessage, MSG_V_LOAD_MAGAZINE_MSG)

NetworkMessageFormat &RadioMessageVLoadMagazine::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  V_MESSAGE_MSG(VMessage, MSG_FORMAT)
  MSG_V_LOAD_MAGAZINE_MSG(MsgVLoadMagazine, MSG_FORMAT)
  return format;
}

TMError RadioMessageVLoadMagazine::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(MsgVLoadMagazine)

  TRANSF_REF(vehicle)
  TRANSF_REF(receiver)
  TRANSF(magazineCreator)
  TRANSF(magazineId)
  TRANSF(weapon)
  TRANSF(muzzle)
  return TMOK;
}

void RadioMessageVLoadMagazine::Send()
{
  if (_vehicle)
    _vehicle->SendLoadMagazine(_receiver, _magazineCreator, _magazineId, _weapon, _muzzle);
}

RString RadioMessageVLoadMagazine::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle) return RString();
  AIBrain *receiver = _receiver ? _receiver->Brain() : NULL;
  if (!receiver) return RString();

  const Magazine *magazine = _vehicle->FindMagazine(_magazineCreator, _magazineId);
  if (!magazine) return RString();
  const MagazineType *type = magazine->_type;
  params.AddWord(type->GetNameSound(), type->GetDisplayName());
  return "VehicleLoadMagazine";
}

RadioMessageVAzimut::RadioMessageVAzimut(Transport *vehicle, float azimut)
{
  _vehicle = vehicle;
  _azimut = azimut;
}

AIBrain *RadioMessageVAzimut::GetSender() const
{
  return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

LSError RadioMessageVAzimut::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
  CHECK(ar.Serialize("azimut", _azimut, 1))
  return LSOK;
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVAzimut, MsgVAzimut)

#define MSG_V_AZIMUT_MSG(MessageName, XX) \
  XX(MessageName, float, azimut, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Orientation azimuth"), TRANSF)

DECLARE_NET_INDICES_EX(MsgVAzimut, VMessage, MSG_V_AZIMUT_MSG)
DEFINE_NET_INDICES_EX(MsgVAzimut, VMessage, MSG_V_AZIMUT_MSG)

NetworkMessageFormat &RadioMessageVAzimut::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  V_MESSAGE_MSG(VMessage, MSG_FORMAT)
  MSG_V_AZIMUT_MSG(MsgVAzimut, MSG_FORMAT)
  return format;
}

TMError RadioMessageVAzimut::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(MsgVAzimut)

  TRANSF_REF(vehicle)
  TRANSF(azimut);
  return TMOK;
}

void RadioMessageVAzimut::Send()
{
  if (_vehicle)
    _vehicle->SendAzimut(_azimut);
}

void RadioMessageVAzimut::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _vehicle->PilotUnit();
  if (!CheckUnitAlive(receiver)) return;

  _vehicle->ReceivedAzimut(_azimut);
}

RString RadioMessageVAzimut::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle) return RString();
  AIBrain *receiver = _vehicle->PilotUnit();
  if (!receiver) return RString();

  int azimut = toInt(_azimut * (4.0 / H_PI));
  switch (azimut)
  {
    case 0:
      params.AddWord("north", LocalizeString(IDS_Q_NORTH));
      break;
    case 1:
      params.AddWord("northEast", LocalizeString(IDS_Q_NORTH_EAST));
      break;
    case 2:
      params.AddWord("east", LocalizeString(IDS_Q_EAST));
      break;
    case 3:
      params.AddWord("southEast", LocalizeString(IDS_Q_SOUTH_EAST));
      break;
    case 4:
      params.AddWord("south", LocalizeString(IDS_Q_SOUTH));
      break;
    case 5:
      params.AddWord("southWest", LocalizeString(IDS_Q_SOUTH_WEST));
      break;
    case 6:
      params.AddWord("west", LocalizeString(IDS_Q_WEST));
      break;
    case 7:
      params.AddWord("northWest", LocalizeString(IDS_Q_NORTH_WEST));
      break;
  }

  return "VehicleAzimut";
}

// class RadioMessageVStopTurning
RadioMessageVStopTurning::RadioMessageVStopTurning(Transport *vehicle, float azimut)
{
  _vehicle = vehicle;
  _azimut = azimut;
}

LSError RadioMessageVStopTurning::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Vehicle", _vehicle, 1))
  CHECK(ar.Serialize("azimut", _azimut, 1))
  return LSOK;
}

void RadioMessageVStopTurning::Transmitted(ChatChannel channel)
{
  if (!_vehicle) return;
  AIBrain *receiver = _vehicle->PilotUnit();
  if (!CheckUnitAlive(receiver)) return;

  _vehicle->ReceivedStopTurning(_azimut);
}

AIBrain *RadioMessageVStopTurning::GetSender() const
{
  return _vehicle ? _vehicle->CommanderUnit() : NULL;
}

DEFINE_NETWORK_OBJECT_SIMPLE(RadioMessageVStopTurning, MsgVStopTurning)

#define MSG_V_STOP_TURNING_MSG(MessageName, XX) \
  XX(MessageName, float, azimut, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Final azimuth"), TRANSF)

DECLARE_NET_INDICES_EX(MsgVStopTurning, VMessage, MSG_V_STOP_TURNING_MSG)
DEFINE_NET_INDICES_EX(MsgVStopTurning, VMessage, MSG_V_STOP_TURNING_MSG)

NetworkMessageFormat &RadioMessageVStopTurning::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  V_MESSAGE_MSG(VMessage, MSG_FORMAT)
  MSG_V_STOP_TURNING_MSG(MsgVStopTurning, MSG_FORMAT)
  return format;
}

TMError RadioMessageVStopTurning::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(MsgVStopTurning)

  TRANSF_REF(vehicle)
  TRANSF(azimut);
  return TMOK;
}

void RadioMessageVStopTurning::Send()
{
  if (_vehicle)
    _vehicle->SendStopTurning(_azimut);
}

RString RadioMessageVStopTurning::PrepareSentence(SentenceParams &params) const
{
  if (!_vehicle) return RString();
  AIBrain *receiver = _vehicle->PilotUnit();
  if (!receiver) return RString();

  switch (_vehicle->GetMoveMode())
  {
    default:
    case VMMStop:
      return "VehicleStop";
    case VMMBackward:
      return "VehicleBackward";
    case VMMSlowForward:
      return "VehicleSlower";
    case VMMForward:
      return "VehicleForward";
    case VMMFastForward:
      return "VehicleFaster";
  }
}

// xmit into crew channel
void Transport::SendMove( Vector3Par pos )
{
  AIBrain *commander=CommanderUnit();
  if( !commander ) return;

  AIBrain *receiver = PilotUnit();
  if (!receiver || receiver == commander) return;

  // ignore very short moves
  if (pos.Distance2(FutureVisualState().Position()) < Square(2))
  {
    commander->SetState(AIUnit::Completed);
    return;
  }

  _radio.Transmit(new RadioMessageVMove(this, pos));
}

void Transport::SendWatchTgt(Person *receiver, Target *tgt)
{
  AIBrain *commander=EffectiveObserverUnit();
  if( !commander ) return;

  _radio.Transmit(new RadioMessageVWatchTgt(this, receiver, tgt));
}

void Transport::SendWatchPos(Person *receiver, Vector3Par pos)
{
  AIBrain *commander=EffectiveObserverUnit();
  if( !commander ) return;

  _radio.Transmit(new RadioMessageVWatchPos(this, receiver, pos));
}

class UpdateVTargetMessage
{
protected:
  Target *_tgt;
  bool &_done;

public:
  UpdateVTargetMessage(Target *tgt, bool &done) : _done(done)
  {
    _tgt = tgt;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool UpdateVTargetMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  switch (msg->GetType())
  {
    case RMTVehicleTarget:
      {
        RadioMessageVTarget *msgT = static_cast<RadioMessageVTarget *>(msg);
        if (msgT->GetTarget() == _tgt)
          _done = true;
        else if (actual)
          channel->CancelActualMessage();
        else
        {
          msgT->SetTarget(_tgt);
          _done = true;
        }
        return true;
      }
    case RMTVehicleSimpleCommand:
      {
        RadioMessageVSimpleCommand *msgT = static_cast<RadioMessageVSimpleCommand *>(msg);
        return msgT->GetCommand() == SCFire;
      }
    default:
      return false;
  }
}

void Transport::SendTarget(Person *receiver, Target *tgt)
{
  AIBrain *commander = EffectiveObserverUnit();
  if( !commander ) return;

  AIBrain *gunner = receiver ? receiver->Brain() : NULL;
  if (!gunner || gunner == commander) return;

  // check radio channel
  bool done = false;
  UpdateVTargetMessage func(tgt, done);
  bool found = _radio.ForEachMessage(func);
  if (done) return;

  TurretContext context;
  if (!FindTurret(receiver, context)) return;

  if (!found && tgt == context._weapons->_fire._fireTarget) return;

#if LOG_TARGET_CHANGES
  LogF("%.1f: Sending TARGET: Commander %s, New Target %s, Old Target %s",
    Glob.time.toFloat(), (const char *)commander->GetDebugName(),
    tgt && tgt->idExact ? (const char *)tgt->idExact->GetDebugName() : "NULL",
    context._weapons->_fire._fireTarget && context._weapons->_fire._fireTarget->idExact ? (const char *)context._weapons->_fire._fireTarget->idExact->GetDebugName() : "NULL");
#endif

  _radio.Transmit(new RadioMessageVTarget(this, receiver, tgt));
  _radio.Simulate(0, this);
}

class FindFireMessage
{
public:
  FindFireMessage() {}
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool FindFireMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  switch (msg->GetType())
  {
    case RMTVehicleFire:
      return true;
  }
  return false;
}

/*!
\patch 1.51 Date 4/19/2002 by Ondra
- Fixed: Vehicle crew "Fire" command is now higher priority
than reporting enemies messages.
*/

void Transport::SendFire(Person *receiver, Target *tgt)
{
  AIBrain *commander=EffectiveObserverUnit();
  if( !commander ) return;

  AIBrain *gunner = receiver ? receiver->Brain() : NULL;
  if (!gunner || gunner == commander) return;

  // if there is already some fire waiting, do not send it again
  FindFireMessage func;
  if (_radio.ForEachMessage(func)) return;

  _radio.Transmit(new RadioMessageVFire(this, receiver, tgt));
  _radio.Simulate(0, this);
}

void Transport::SendJoin()
{
  AIBrain *commander=CommanderUnit();
  if( !commander ) return;

  AIBrain *receiver = PilotUnit();
  if (!receiver || receiver == commander) return;

  _radio.Transmit(new RadioMessageVFormation(this));
}

static bool IsSimpleMove( SimpleCommand type )
{
  switch (type)
  {
    case SCForward:
    case SCStop:
    case SCBackward:
    case SCFaster:
    case SCSlower:
      //  SCLeft,
      //  SCRight,
    case SCKeyUp:
    case SCKeyDown:
    case SCKeySlow:
    case SCKeyFast:
      return true;
  }
  return false;
}

class RemoveMoveMessage
{
protected:
  Transport *_vehicle;

public:
  RemoveMoveMessage(Transport *vehicle) {_vehicle = vehicle;}
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool RemoveMoveMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (!actual && msg->GetType() == RMTVehicleSimpleCommand)
  {
    // check if it is left or right
    RadioMessageVSimpleCommand *cmd = static_cast<RadioMessageVSimpleCommand *>(msg);
    if (IsSimpleMove(cmd->GetCommand()))
      channel->Cancel(msg, _vehicle);
  }
  return false;
}

void Transport::SendSimpleCommand(Person *person, SimpleCommand cmd)
{
  AIBrain *commander = CommanderUnit();
  if (!commander) return;

  AIBrain *receiver = person ? person->Brain() : NULL;
  if (cmd == SCManualFire || cmd == SCCancelManualFire)
  {
    if (!CheckUnitAlive(receiver))
      receiver = CommanderUnit();
  }
  else if (!receiver || receiver == commander)
    return;

  if (IsSimpleMove(cmd))
  {
    // remove all other simple moves from queue
    RemoveMoveMessage func(this);
    _radio.ForEachMessage(func);
  }
  _radio.Transmit(new RadioMessageVSimpleCommand(this, person, cmd));
}

class UpdateLoadMessage
{
protected:
  int _weapon;
  int &_lastWeapon;
  bool &_done;

public:
  UpdateLoadMessage(int weapon, int &lastWeapon, bool &done) : _lastWeapon(lastWeapon), _done(done)
  {
    _weapon = weapon;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool UpdateLoadMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  switch (msg->GetType())
  {
  case RMTVehicleLoad:
    {
      RadioMessageVLoad *msgT = static_cast<RadioMessageVLoad *>(msg);
      if (actual)
      {
        _lastWeapon = msgT->GetWeapon();
        return false;
      }
      else
      {
        msgT->SetWeapon(_weapon);
        _done = true;
        return true;
      }
    }
  case RMTVehicleSimpleCommand:
    {
      RadioMessageVSimpleCommand *msgT = static_cast<RadioMessageVSimpleCommand *>(msg);
      return msgT->GetCommand() == SCFire;
    }
  default:
    return false;
  }
}

/*!
\patch 1.08 Date 7/21/2001 by Ondra.
- Fixed:
When tank commander was changing weapons often,
weapon change command was sometimes lost.
*/

void Transport::SendLoad(Person *receiver, int weapon)
{
  AIBrain *commander = CommanderUnit();
  if (!commander) return;

  AIBrain *gunner = receiver ? receiver->Brain() : NULL;
  if (!gunner || gunner == commander) return;

  TurretContext context;
  if (!FindTurret(receiver, context)) return;

  //LogF("Request to send load %d->%d", _currentWeapon, weapon);

  // check radio channel
  bool done = false;
  int actWeapon = context._weapons->ValidatedCurrentWeapon();
  UpdateLoadMessage func(weapon, actWeapon, done);
  bool found = _radio.ForEachMessage(func);
  if (done) return;
  if (!found && actWeapon == weapon) return;

  //LogF("Sending load %d->%d", _currentWeapon, weapon);
  _radio.Transmit(new RadioMessageVLoad(this, receiver, weapon));
}

class UpdateLoadMagazineMessage
{
protected:
  CreatorId _creator;
  int _id;
  RString _weapon;
  RString _muzzle;
  bool &_done;

public:
  UpdateLoadMagazineMessage(CreatorId creator, int id, RString weapon, RString muzzle, bool &done) : _done(done)
  {
    _creator = creator; _id = id; _weapon = weapon; _muzzle = muzzle;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool UpdateLoadMagazineMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  switch (msg->GetType())
  {
    case RMTVehicleLoadMagazine:
      {
        RadioMessageVLoadMagazine *msgT = static_cast<RadioMessageVLoadMagazine *>(msg);
        if (actual)
          _done = msgT->CheckParams(_creator, _id, _weapon, _muzzle);
        else
        {
          msgT->SetParams(_creator, _id, _weapon, _muzzle);
          _done = true;
        }
        return true;
      }
    case RMTVehicleSimpleCommand:
      {
        RadioMessageVSimpleCommand *msgT = static_cast<RadioMessageVSimpleCommand *>(msg);
        return msgT->GetCommand() == SCFire;
      }
    default:
      return false;
  }
}

void Transport::SendLoadMagazine(Person *receiver, CreatorId creator, int id, RString weapon, RString muzzle)
{
  AIBrain *commander = CommanderUnit();
  if (!commander) return;
  AIBrain *gunner = receiver ? receiver->Brain() : NULL;
  if (!gunner || gunner == commander) return;

  //LogF("Request to send load %d->%d", _currentWeapon, weapon);

  // check radio channel
  bool done = false;
  UpdateLoadMagazineMessage func(creator, id, weapon, muzzle, done);
  _radio.ForEachMessage(func);
  if (done) return;

  _radio.Transmit(new RadioMessageVLoadMagazine(this, receiver, creator, id, weapon, muzzle));
}

void Transport::SendAzimut(float azimut)
{
  AIBrain *commander = CommanderUnit();
  if (!commander) return;

  AIBrain *receiver = PilotUnit();
  if (!receiver || receiver == commander) return;


  _radio.Transmit(new RadioMessageVAzimut(this, azimut));
}

class CancelStopTurningMessage
{
protected:
  Transport *_vehicle;
  mutable bool _oneTurnSkipped;

public:
  CancelStopTurningMessage(Transport *vehicle)
  {
    _vehicle = vehicle; _oneTurnSkipped = false;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool CancelStopTurningMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  switch (msg->GetType())
  {
    case RMTVehicleStopTurning:
      if (!actual)
        channel->Cancel(msg, _vehicle);
      break;
    case RMTVehicleSimpleCommand:
      if (!actual)
      {
        RadioMessageVSimpleCommand *cmd = static_cast<RadioMessageVSimpleCommand *>(msg);
        if (cmd->GetCommand() == SCLeft || cmd->GetCommand() == SCRight)
        {
          if (!_oneTurnSkipped)
            _oneTurnSkipped = true;
          else
            channel->Cancel(msg, _vehicle);
        }
      }
      break;
  }
  return false;
}

void Transport::SendStopTurning(float azimut)
{
  AIBrain *commander = CommanderUnit();
  if (!commander) return;

  AIBrain *receiver = PilotUnit();
  if (!receiver || receiver == commander) return;

  // remove all previous turn / stop turning from the queue
  // leave only corresonding turn
  // remove all stop turning
  CancelStopTurningMessage func(this);
  _radio.ForEachMessage(func);

  _radio.Transmit(new RadioMessageVStopTurning(this, azimut));
}

void Transport::SendFireFailed(Person *sender)
{
  AIBrain *from = sender ? sender->Brain() : NULL;
  if (!from) return;

  AIBrain *receiver = CommanderUnit();
  if (!receiver || receiver == from) return;

  _radio.Transmit(new RadioMessageVFireFailed(this, sender));
}

void Transport::ResetStatus()
{
  base::ResetStatus();
}

void Transport::UpdateStopTimeout()
{
  float maxTime = 0;

  for (int i=0; i<_getinUnits.Size(); i++)
  {
    AIUnit *unit = _getinUnits[i];
    if (!unit) continue;

    // Calculate cost from starting field
    int x = toIntFloor(unit->Position(unit->GetFutureVisualState()).X() * InvLandGrid);
    int z = toIntFloor(unit->Position(unit->GetFutureVisualState()).Z() * InvLandGrid);
    GeographyInfo geogr = GLOB_LAND->GetGeography(x, z);
    float dist = FutureVisualState().Position().Distance(unit->Position(unit->GetFutureVisualState()));
    float time = 2.0 * unit->GetVehicle()->GetBaseCost(geogr, false, true) * dist + 30.0;
    saturateMax(maxTime, time);
  }

  _getinTime = Glob.time + maxTime;
}

static const EnumName MoveModeNames[]=
{
  EnumName(VMMFormation,"FORMATION"),
  EnumName(VMMMove,"MOVE"),
  EnumName(VMMBackward,"BACK"),
  EnumName(VMMStop,"STOP"),
  EnumName(VMMSlowForward,"SLOW"),
  EnumName(VMMForward,"FORWARD"),
  EnumName(VMMFastForward,"FAST"),
  EnumName(),
};

template<>
const EnumName *GetEnumNames( VehicleMoveMode dummy ) {return MoveModeNames;}

static const EnumName TurnModeNames[]=
{
  EnumName(VTMNone,"NONE"),
  EnumName(VTMLeft,"LEFT"),
  EnumName(VTMRight,"RIGHT"),
  EnumName(VTMAbs,"ABS"),
  EnumName()
};

template<>
const EnumName *GetEnumNames( VehicleTurnMode dummy ) {return TurnModeNames;}

LSError ManCargoItem::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Man", man, 1))
  CHECK(ar.Serialize("locked", _locked, 1))
  return LSOK;
}

/*!
\patch 2.01 Date 1/3/2003 by Jirka
- Fixed: after load, engine was off for cars, tanks and planes
*/

LSError Transport::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))

  if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
  {
    // turret need to know its owner even in the first pass
    void *oldParams = ar.GetParams();
    ar.SetParams(this);
    CHECK(ar.Serialize("Turrets", _turrets, 1))
    ar.SetParams(oldParams);

    CHECK(ar.SerializeRef("Driver", _driver, 1))
    CHECK(ar.SerializeRef("EffCommander", _effCommander, 1))

    CHECK(ar.Serialize("driverLocked", _driverLocked, 1, false))
    CHECK(ar.Serialize("ManCargo", _manCargo, 1))
    if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond)
    {
      int n = Type()->_maxManCargo;
      if (_manCargo.Size() != n) _manCargo.Resize(n); // handle type change

      // switch light off for soldiers in vehicle
      if (_driver)
      {
        _driver->SetPilotLight(false);
        _driver->SwitchLight(false);
      }
      for (int i=0; i<n; i++)
      {
        Person *man = _manCargo[i];
        if (man)
        {
          man->SetPilotLight(false);
          man->SwitchLight(false);
        }
      }
    }

    // assigned to ...
    CHECK(ar.SerializeRef("GroupAssigned", _groupAssigned, 1))
    CHECK(ar.SerializeRef("DriverAssigned", _driverAssigned, 1))
    CHECK(ar.SerializeRefs("CargoAssigned", _cargoAssigned, 1))

    // _cargoAssigned is now sized by _manCargo (slots)
    if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond)
      _cargoAssigned.Resize(Type()->_maxManCargo);

    CHECK(ar.SerializeRefs("GetoutUnits", _getoutUnits, 1))
    CHECK(ar.SerializeRefs("GetinUnits", _getinUnits, 1))
    CHECK(::Serialize(ar, "getinTime", _getinTime, 1))
    CHECK(::Serialize(ar, "getoutTime", _getoutTime, 1))

    CHECK(::Serialize(ar, "driverPos", _driverPos, 1, VZero))
    CHECK(::Serialize(ar, "dirWanted", _dirWanted, 1, VZero))

    CHECK(ar.SerializeEnum("moveMode", _moveMode, 1, VMMFormation))
    CHECK(ar.SerializeEnum("turnMode", _turnMode, 1, VTMNone))

    CHECK(ar.SerializeRef("comFireGunner", _commanderFire._gunner, 1))
    CHECK(ar.Serialize("comFireMode", _commanderFire._weapon, 1, -1))
    CHECK(ar.Serialize("comFirePrepareOnly", _commanderFire._firePrepareOnly, 1, true))
    CHECK(ar.Serialize("comFireCommanded", _commanderFire._fireCommanded, 1, false))
    CHECK(ar.SerializeRef("comFireTarget", _commanderFire._fireTarget, 1))

    //  CHECK(ar.Serialize("lastStrategicIndex", _lastStrategicIndex, 1, -1))
    //  CHECK(ar.Serialize("useStrategicPlan", _useStrategicPlan, 1, 0))
    //  CHECK(ar.Serialize("strategicPlanValid", _strategicPlanValid, 1, 0))
    // TODO: check and remove  _fireEnabled

    //  CHECK(ar.Serialize("Planner", _planner, 1))
    CHECK(ar.Serialize("Radio", _radio, 1))

    if (ar.IsSaving() || ar.GetArVersion() >= 10)
      CHECK(ar.SerializeEnum("lock", _lock, 1, ENUM_CAST(LockState,LSDefault)))
    else if (ar.GetPass() == ParamArchive::PassFirst)
    {
      bool locked;
      CHECK(ar.Serialize("locked", locked, 1, false))
      _lock = locked ? LSDefault : LSLocked;
    }

    CHECK(ar.Serialize("manualFire", _manualFire, 1, false))
    CHECK(::Serialize(ar, "turretFrontUntil", _turretFrontUntil, 1, TIME_MIN))

    CHECK(ar.Serialize("zoom", _zoom, 1));
  }
  
  CHECK(ar.Serialize("fuel", FutureVisualState()._fuel, 1, Type()->GetFuelCapacity()))
  CHECK(ar.Serialize("engineOff", _engineOff, 1, false))
  CHECK(ar.Serialize("engineTemperatureCoef", _engineTemperatureCoef, 1, 0.0f))
  CHECK(ar.Serialize("wheelTemperatureCoef", _wheelTemperatureCoef, 1, 0.0f))
  CHECK(ar.Serialize("turretGunTemperatureCoef", _turretGunTemperatureCoef, 1, 0.0f))
  CHECK(::Serialize(ar, "destroyMaxHeatTime", _destroyMaxHeatTime, 1, TIME_MIN))

  CHECK(ar.Serialize("driverHidden", FutureVisualState()._driverHidden, 1, 1.0f))
  CHECK(ar.Serialize("driverHiddenWanted", _driverHiddenWanted, 1, 1.0f))

  CHECK(ar.SerializeEnum("visionMode", _visionMode, 1, ENUM_CAST(VisionMode, VMNormal)))
  CHECK(ar.Serialize("tiIndex", _tiIndex, 1, 0))
  CHECK(ar.Serialize("enableVisionMode", _enableVisionMode, 1, true))

  CHECK(ar.Serialize("respawnSide", (int &)_respawnSide, 1, 0))
  CHECK(ar.SerializeRef("respawnUnit",_respawnUnit, 1))

  CHECK(ar.Serialize("respawning",_respawning, 1, false))
  CHECK(ar.Serialize("respawnDelay",_respawnDelay, 1, -1.0f))
  CHECK(ar.Serialize("respawnFlying",_respawnFlying, 1, false))
  CHECK(ar.Serialize("respawnCount",_respawnCount, 1, 0))

  CHECK(ar.Serialize("allowCrewInImmobile",_allowCrewInImmobile, 1, false))


  ProgressRefresh();
  return LSOK;
}

#define TRANSPORT_MSG_LIST(XX) \
  XX(UpdateGeneric, UpdateTransport)

DEFINE_NETWORK_OBJECT(Transport, base, TRANSPORT_MSG_LIST)

DEFINE_NET_INDICES_EX_ERR(UpdateTransport, UpdateVehicleAIFull, UPDATE_TRANSPORT_MSG, NoErrorInitialFunc)

NetworkMessageFormat &Transport::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  switch (cls)
  {
    case NMCUpdateGeneric:
      base::CreateFormat(cls, format);
      UPDATE_TRANSPORT_MSG(UpdateTransport, MSG_FORMAT_ERR)
        break;
    default:
      base::CreateFormat(cls, format);
      break;
  }
  return format;
}

bool IsInVehicle(Person *soldier, Transport *veh)
{
  if (veh->Driver() == soldier) return true;
  TurretContext context;
  if (veh->FindTurret(soldier, context)) return true;
  for (int i=0; i<veh->GetManCargo().Size(); i++)
  {
    if (veh->GetManCargo()[i] == soldier)
      return true;
  }
  return false;
}

/*!
\patch 1.28 Date 10/30/2001 by Ondra
- Fixed: MP: bug in cargo position transfer. Caused problems when
unit from first cargo seat disembarked while someone was still
sitting in second. It could also lead to player animation corruption
or character duplication when player made double attempt to enter vehicle,
thinking first ateempt failed when result was lagged.
\patch_internal 1.28 Date 10/30/2001 by Ondra
- Fixed: GetInCargo can now be position specific
\patch 1.31 Date 11/07/2001 by Jirka
- Fixed: Problem with dead bodies inside vehicle in multiplayer
\patch 1.47 Date 3/7/2002 by Ondra
- Fixed: Different number of cargo slot could crash server or client in MP.
*/

TMError Transport::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
    case NMCUpdateGeneric:
      TMCHECK(base::TransferMsg(ctx))
      {
        PREPARE_TRANSFER(UpdateTransport)

        TRANSF(driverLocked)
        if (ctx.IsSending())
        {
          TRANSF_REF(driver)
          TRANSF_REF(effCommander)
          RefArray<Person> manCargo;
          AutoArray<bool> cargoLocked;
          int n = _manCargo.Size();
          manCargo.Resize(n);
          cargoLocked.Resize(n);
          for (int i=0; i<n; i++)
          {
            manCargo[i] = _manCargo[i];
            cargoLocked[i] = _manCargo.IsLocked(i);
          }
          TRANSF_REFS_EX(manCargo, manCargo)
          TRANSF_EX(cargoLocked, cargoLocked)
          EntityAI *target = NULL;
          if (_commanderFire._fireTarget)
            target = _commanderFire._fireTarget->idExact;
          TRANSF_PTR_REF_EX(comFireTarget, target)
        }
        else
        {
          OLinkPerm<Person> driver;
          TRANSF_REF_EX(driver, driver)

          // first check position changes
          if (driver != _driver && driver && IsInVehicle(driver, this))
          {
#if DIAG_CREW
            LogF("### Turret update arrived - '%s' found in the vehicle", cc_cast(driver->GetDebugName()));
#endif
            UIActionType type = GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)) ? ATMoveToPilot : ATMoveToDriver;
            Ref<Action> action = new ActionBasic(type, this);
            ChangePosition(action, driver);
          }

          if (driver != _driver)
          {
#if DIAG_CREW
            LogF("### Turret update arrived - '%s' not found in the vehicle", cc_cast(driver ? driver->GetDebugName() : "NULL"));
#endif
            if (_driver)
            {
              // get out
              //GetOutDriver(*_driver);
              AIBrain *unit = _driver->Brain();
              if (unit)
              {
                bool isFocused = (GWorld->FocusOn() == unit);
                unit->DoGetOut(this, true, false);
                if (isFocused)
                  GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()), false);
              }
              else
              {
                // FIX - enable get out dead body (without brain)
                // calculate get out position
                GetInPoint pt = GetDriverGetOutPos(_driver);
                Matrix4 transform;
                transform.SetPosition(pt._pos);
                transform.SetUpAndDirection(VUp, - pt._dir);
                // get out, use eject (do not turn off the engine - owner will decide)
                GetOutDriver(transform,true);
              }
            }
            if (driver)
            {
              // get in
              GetInDriver(driver);
              if (GWorld->GetRealPlayer() == driver)
                GWorld->SwitchCameraTo(this, ValidateCamera(GWorld->GetCameraType()), false);
              AIBrain *unit = driver->Brain();
              if (unit)
              {
                unit->AssignAsDriver(this);
                unit->OrderGetIn(true);
              }
            }
          }

          RefArray<Person> manCargo;
          AutoArray<bool> cargoLocked;
          TRANSF_REFS_EX(manCargo, manCargo)
          TRANSF_EX(cargoLocked, cargoLocked)
          //DoAssert(manCargo.Size() == _manCargo.Size());
          for (int i=0; i<_manCargo.Size(); i++)
          {
            if (i < cargoLocked.Size())
              _manCargo.Lock(i, cargoLocked[i]);
            else
              _manCargo.Lock(i, false);
            OLinkPerm<Person> soldier = i<manCargo.Size() ? manCargo[i].GetRef() : NULL;
            if (soldier != _manCargo[i])
            {
              if (_manCargo[i])
              {
                // get out
                //GetOutCargo(_manCargo[i], *_manCargo[i]);
                AIBrain *unit = _manCargo[i]->Brain();
                if (unit)
                {
                  bool isFocused = (GWorld->FocusOn() == unit);
                  unit->DoGetOut(this, true, false);
                  if (isFocused)
                    GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()), false);
                }
                else
                {
                  // FIX - enable get out dead body (without brain)
                  // calculate get out position
                  GetInPoint pt = GetCargoGetOutPos(_manCargo[i]);
                  Matrix4 transform;
                  transform.SetPosition(pt._pos);
                  transform.SetUpAndDirection(VUp, - pt._dir);
                  // get out
                  GetOutCargo(_manCargo[i], transform);
                }
              }
              if (soldier)
              {
                // get in
                GetInCargo(soldier,true,i);
                if (GWorld->GetRealPlayer() == soldier)
                  GWorld->SwitchCameraTo(this, ValidateCamera(GWorld->GetCameraType()), false);
                AIBrain *unit = soldier->Brain();
                if (unit)
                {
                  unit->AssignAsCargo(this);
                  unit->OrderGetIn(true);
                  unit->SetState(AIBrain::InCargo);
                }
              }
            }
          }

          OLink(EntityAI) target;
          _commanderFire._fireTarget = NULL;
          TRANSF_REF_EX(comFireTarget, target)
          if (target)
          {
            AIBrain *unit = CommanderUnit();
            if (unit)
            {
              Target *newTarget = unit->FindTargetAll(target);
              if (!newTarget)
                // add target to target database
                newTarget = unit->AddTarget(target, 4.0f, 4.0f, 0);
              _commanderFire._fireTarget = newTarget;
              if (!unit->IsLocal() && newTarget)
                // update position if commander is remote
                _commanderFire._fireTarget->UpdateRemotePosition(target);
            }
          }

        }
#if _VBS3
        float amount = 0.0f;
#if _LASERSHOT // if there are attached objects like cameras, increase the update rate
        if(_attachedObjects.Size()>0)
          amount = 2.0f;
#endif
        if (TRANSF_BASE(UpdateCoef,amount) != TMOK)
          RptF("Failed to transfer updateCoef");
#endif
        TRANSF_ENUM(lock)
        TRANSF(driverHiddenWanted)
        TRANSF_ENUM(respawnSide)
        TRANSF(respawning)
        TRANSF(respawnFlying)
        TRANSF_REF(respawnUnit)
        TRANSF(respawnDelay)
        TRANSF(respawnCount)
        TRANSF(manualFire)
        if (ctx.IsSending())
        {
          TRANSF(engineOff)
          TRANSF_EX(fuel, FutureVisualState()._fuel)

#if _VBS3 && _EXT_CTRL
          int controller = GetExternalController();
          TRANSF_BASE(HLAController,controller);
#endif
        }
        else
        {
          bool engineOff;
          float fuel;
          TRANSF_EX(engineOff, engineOff)
          TRANSF_EX(fuel, fuel)
          if (engineOff!=_engineOff)
          {
            _engineOff = engineOff;
            OnEvent(EEEngine,_engineOff);
          }
          if ((FutureVisualState()._fuel>0)!=(fuel>0))
            OnEvent(EEFuel,fuel>0);
          FutureVisualState()._fuel = fuel;

#if _VBS3 && _EXT_CTRL
          int controller = 0;
          TRANSF_BASE(HLAController,controller);
          SetExternalController(controller);
#endif
        }
        TRANSF(enableVisionMode)
        TRANSF(allowCrewInImmobile)
      }
      break;
    default:
      return base::TransferMsg(ctx);
  }
  return TMOK;
}

float Transport::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = 0;
  switch (ctx.GetClass())
  {
    case NMCUpdateGeneric:
      {
        error += base::CalculateError(ctx);

        PREPARE_TRANSFER(UpdateTransport)

        ICALCERR_NEQREF_PERM(Person, driver, ERR_COEF_STRUCTURE)

        /*
        RefArray<Person> manCargo;
        if (TRANSF_REFS_BASE(manCargo, manCargo) == TMOK)
        {
        const float coef = 0.5 * ERR_COEF_STRUCTURE;
        for (int i=0; i<_manCargo.Size(); i++)
        {
        Person *soldier = i<manCargo.Size() ? manCargo[i] : NULL;
        if (soldier != _manCargo[i]) error += coef;
        }
        }
        */
        const float coef = 0.5 * ERR_COEF_STRUCTURE;
        for (int i=0; i<_manCargo.Size(); i++)
        {
          NetworkId id = i < message->_manCargo.Size() ? message->_manCargo[i] : NetworkId::Null();
          if (_manCargo[i])
          {
            if (_manCargo[i]->GetNetworkId() != id)
              error += coef;
          }
          else
          {
            if (!id.IsNull())
              error += coef;
          }
        }

        OLink(EntityAI) target;
        if (_commanderFire._fireTarget) target = _commanderFire._fireTarget->idExact;
        ICALCERRE_NEQREF_SOFT(Object, comFireTarget, target, ERR_COEF_MODE)

        ICALCERR_NEQ(int, lock, ERR_COEF_STRUCTURE)
        ICALCERR_ABSDIF(float, driverHiddenWanted, ERR_COEF_VALUE_MAJOR)
        {
          error += ERR_COEF_VALUE_MINOR * fabs(message->_fuel - FutureVisualState()._fuel);
        }
        ICALCERR_NEQ(bool, engineOff, ERR_COEF_MODE)
        ICALCERR_NEQ(bool, manualFire, ERR_COEF_MODE)
        ICALCERR_NEQ(TargetSide, respawnSide, ERR_COEF_MODE)
        ICALCERR_NEQ(bool, respawning, ERR_COEF_MODE)
        ICALCERR_NEQ(bool, respawnFlying, ERR_COEF_MODE)
        ICALCERR_NEQREF_PERM(AIUnit, respawnUnit, ERR_COEF_MODE)
        ICALCERR_NEQ(float, respawnDelay, ERR_COEF_MODE)
        ICALCERR_NEQ(int, respawnCount, ERR_COEF_MODE)
        ICALCERR_NEQ(bool, enableVisionMode, ERR_COEF_MODE)
        ICALCERR_NEQ(bool, allowCrewInImmobile, ERR_COEF_MODE)
      }
      break;
    default:
      error += base::CalculateError(ctx);
      break;
  }
  return error;
}

void Transport::CreateSubobjects()
{
  TurretPath path;
  for (int i=0; i<_turrets.Size(); i++)
  {
    path.Resize(1);
    path[0] = i;
    _turrets[i]->CreateSubobjects(this, path);
  }
}

Time Transport::GetDestroyedTime() const
{
  if (!_isDead)
    return TIME_MAX;

  if (_explosionTime<Glob.time+60)
  {
    if (_whenDestroyed<_explosionTime)
      return _whenDestroyed;
    return _explosionTime;
  }
  return base::GetDestroyedTime();
}

/*!
\patch 1.37 Date 12/18/2001 by Jirka
- Fixed: MP parachute - get out units on clients when parachute destroyed
*/
void Transport::DestroyObject()
{
  if (Observer())
  {
    AIBrain *unit = Observer()->Brain();
    if (unit)
    {
      // get out
      bool isFocused = (GWorld->FocusOn() == unit);
      unit->DoGetOut(this, true, false);
      if (isFocused)
        GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()), false);
    }
    else
    {
      Ref<Person> soldier = Observer();
      // calculate get out position
      GetInPoint pt = GetCommanderGetOutPos(Observer());
      Matrix4 transform;
      transform.SetPosition(pt._pos);
      transform.SetUpAndDirection(VUp, - pt._dir);
      // get out
      GetOutCommander(transform);
      // switch camera if dead player gets out
      if (GWorld->CameraOn() == this && GWorld->PlayerOn() == soldier)
      {
        GWorld->SwitchCameraTo(soldier, ValidateCamera(GWorld->GetCameraType()), false);
      }
    }
  }
  if (_driver)
  {
    AIBrain *unit = _driver->Brain();
    if (unit)
    {
      // get out
      bool isFocused = (GWorld->FocusOn() == unit);
      unit->DoGetOut(this, true, false);
      if (isFocused)
        GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()), false);
    }
    else
    {
      Ref<Person> soldier = _driver;
      // calculate get out position
      GetInPoint pt = GetDriverGetOutPos(_driver);
      Matrix4 transform;
      transform.SetPosition(pt._pos);
      transform.SetUpAndDirection(VUp, - pt._dir);
      // get out - do not turn off the engine
      GetOutDriver(transform,true);
      // switch camera if dead player gets out
      if (GWorld->CameraOn() == this && GWorld->PlayerOn() == soldier)
        GWorld->SwitchCameraTo(soldier, ValidateCamera(GWorld->GetCameraType()), false);
    }
  }
  if (Gunner())
  {
    AIBrain *unit = Gunner()->Brain();
    if (unit)
    {
      // get out
      bool isFocused = (GWorld->FocusOn() == unit);
      unit->DoGetOut(this, true, false);
      if (isFocused)
        GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()), false);
    }
    else
    {
      Ref<Person> soldier = Gunner();
      // calculate get out position
      GetInPoint pt = GetGunnerGetOutPos(Gunner());
      Matrix4 transform;
      transform.SetPosition(pt._pos);
      transform.SetUpAndDirection(VUp, - pt._dir);
      // get out
      GetOutGunner(transform);
      // switch camera if dead player gets out
      if (GWorld->CameraOn() == this && GWorld->PlayerOn() == soldier)
        GWorld->SwitchCameraTo(soldier, ValidateCamera(GWorld->GetCameraType()), false);
    }
  }
  for (int i=0; i<_manCargo.Size(); i++)
  {
    if (_manCargo[i])
    {
      AIBrain *unit = _manCargo[i]->Brain();
      if (unit)
      {
        // get out
        bool isFocused = (GWorld->FocusOn() == unit);
        unit->DoGetOut(this, true, false);
        if (isFocused)
          GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()), false);
      }
      else
      {
        Ref<Person> soldier = _manCargo[i];
        // calculate get out position
        GetInPoint pt = GetCargoGetOutPos(_manCargo[i]);
        Matrix4 transform;
        transform.SetPosition(pt._pos);
        transform.SetUpAndDirection(VUp, - pt._dir);
        // get out
        GetOutCargo(_manCargo[i], transform);
        // switch camera if dead player gets out
        if (GWorld->CameraOn() == this && GWorld->PlayerOn() == soldier)
          GWorld->SwitchCameraTo(soldier, ValidateCamera(GWorld->GetCameraType()), false);
      }
    }
  }

  //there can still remain some units in the vehicle's turrets
  //get them also out
  class EjectRemainingFunc: public ICrewFunc
  {
    Transport* _veh;
  public:
    EjectRemainingFunc(Transport* veh){_veh = veh;}
    virtual bool operator () (Person *entity)
    {
      AIBrain *unit = entity->Brain();
      if (unit)
      {
        bool isFocused = (GWorld->FocusOn() == unit);
        unit->DoGetOut(_veh, true, false);
        if (isFocused)
          GWorld->SwitchCameraTo(unit->GetVehicle(), ValidateCamera(GWorld->GetCameraType()), false);
      }
      return false;
    }
  };
  EjectRemainingFunc func(this);
  ForEachCrew(func);

  base::DestroyObject();
}

void Transport::SimulateImpulseDamage(DoDamageResult &result, EntityAI *owner, float impulse, Vector3Par modelPos)
{
  float fInvMass = GetInvMass();
  float damage = impulse * fInvMass * 0.06f + impulse * fInvMass * impulse * fInvMass * 0.024;
  float damageObj = damage * (0.01f + GetInvArmor());

  if (damageObj > 0.05f)
    LocalDamage(result, NULL,owner, modelPos, damageObj, GetRadius() * 0.5);
  damage *= (0.03f + GetInvArmor())/2;
  if (damage > 0.03f && IsLocal())
    DamageCrew(owner, damage , "");
}

Turret *Transport::GetTurret(const int *path, int size)
{
  if (size <= 0)
  {
    LogF("Empty turret path");
    return NULL;
  }
  int i = path[0];
  if (i<0 || i>=_turrets.Size())
  {
    LogF("Invalid turret path");
    return NULL;
  }
  return _turrets[i]->GetTurret(path + 1, size - 1);
}

/*!
\patch 5163 Date 6/1/2007 by Ondra
- Fixed: Scripting function person moveInTurret [vehicle,[x,y,z]] crashed when [x,y,z] was invalid.
*/
const Turret *Transport::GetTurret(const int *path, int size) const
{
  if (size <= 0)
  {
    LogF("Empty turret path");
    return NULL;
  }
  int i = path[0];
  if (i<0 || i>=_turrets.Size())
  {
    LogF("Invalid turret path");
    return NULL;
  }
  return _turrets[i]->GetTurret(path + 1, size - 1);
}
