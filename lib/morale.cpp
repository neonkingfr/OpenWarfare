#include "wpch.hpp"
#if _VBS3
#include "morale.hpp"
#include "soldierOld.hpp"
// A model for the morale of an individual in response to "inate" properites
// of the individual and external events thatcan raise or lower morale.


	// The constants that influence the performance of the morale system.
  float MORALE_MIN = 30.0;       // Worst soldier has this as their starting morale. Best has 100

  float MIN_RECOVERY = 0.1;     // Even worst soldier should regain something
  float RECOVERY_FACTOR = 0.03; // Best possible soldier will regain 3 per second.

  float ENDURANCE_SCALE_FACTOR = 0.2;  // Allows a +/- 20% change in exertion levels on basis of endurance

  float EXERTION_LOSS_FACTOR = 0.015;     // Losing 1.5 morale per sec if exhausted.

 float SUPPRESSION_MORALE_COST = 2.0; // Immediate morale lost when suppressed.

  float FIRE_VOLUME_COST = 1.5;      // Morale lost per sec when under extreme fire

  float STRESS_FROM_MORALE = 0.005;    // Low morale impacts anaerobic stress. 0 morale = 0.5 of anaerobic activity this second

  float MIN_SUPPRESS_RADIUS = 1.0;  // Minimum radius about a soldier within which they will be suppressed - even the best.
  float MAX_INITIAL_SUPPRESS_RADIUS = 3.0;  // Maximum initial radius that the worst soldier will be suppressed by rounds this close - 3m
  float MORALE_IMPACT_ON_SUPPRESSION = 0.6;  // Expansion of suppression circle when morale is at it lowest.

  int INTER_SHOT_INTERVAL_MORALE = 10; // Last shot must have been at least this (10) seconds ago to receive a morale boost.
  float SHOT_MORALE_BOOST = 1.0;         // Immediate increase in morale when a shot is fired.

  float INJURED_SECTION_MORALE_SCALE = 0.6;  // All morale gains are multiplied by this factor if a player's section mate is injured.
  int INJURED_MORALE_DURATION = 300;          // Duration of penalty to morale when a squad mate is injured. After 5 minutes (300 seconds) no longer has impact.
  float INJURED_SECTION_MORALE_PENALTY = 10.0; // Immediate drop in morale when a section member is injured.
  float DEAD_SECTION_MORALE_SCALE = 0.2;     // All morale gains are multiplied by this factor if a player's section mate has been killed.
  int DEAD_MORALE_DURATION = 300;             // Duration of penalty to morale when a squad mate is killed. After 5 minutes (300 seconds) no longer has impact.
  float DEAD_SECTION_MORALE_PENALTY = 25.0;  // Immediate drop in morale when a section mate is killed.
  int IN_THE_PAST = -99999;                   // Initialisation value for a number of the time variables.

  float MORALE_UPDATE_PERIOD = 1.0;		// Number of seconds before morale is updated.


// Construct a morale object with the passed man as the
// owner and using the passed soldier qualities.
Morale::Morale(Man* own, float train, float exp, float end) 
{
	_owner = own;
	train*=100;
	exp*=100;
	end*=100;
	_training = (train>1)?train:1.0;
	_experience = (exp>1)?exp:1.0;
	_endurance = (end>1)?end:1.0;
	Recalculate();
}

// Construct a default morale object
Morale::Morale(Man* own) 
{
	_owner = own;
  _training = 50;
  _experience = 30;
  _leadership = 20;
  _endurance = 50;
	Recalculate();
}

// One or more of the primary soldier quality attributes
// (endurance, training, experience) has changed; so we need
// to recalculate everything.
void Morale::Recalculate() 
{
	_enduranceFactor = 1+((_endurance-50.0)/50.0)*ENDURANCE_SCALE_FACTOR;

	// Soldier quality is geometric mean. morale recovery is mean (more forgiving of a low score).
	_soldierQuality = pow(_training*_experience*_endurance,0.333f);
	_basicMoraleRecovery = (_training+_experience)/2.0*RECOVERY_FACTOR + MIN_RECOVERY;
	_moraleRecovery = _basicMoraleRecovery;
	_moraleMax = MORALE_MIN + (100-MORALE_MIN)*_soldierQuality/100.0;
	_morale = _moraleMax;
	_moraleLongTerm = _morale;
	_windowIndex = 0;
	for (int i=0;i<MORALE_WINDOW_SIZE;i++)
		_moraleWindow[i] = _morale;
	_moraleWindow[_windowIndex++] = _morale;
	_exertion = 0;
	_moraleLevel = 100.0;
	_tick = 0;
	_accTime = 0;
	_moraleLoss = 0;
	// Suppression
	float background = (_training+_experience)/200.0;
	_initialSuppressionRadius = MIN_SUPPRESS_RADIUS + (MAX_INITIAL_SUPPRESS_RADIUS-MIN_SUPPRESS_RADIUS)*(1.0-background);
	_harshPenalty = 0;
	_mildPenalty = 0;
	_tunnelVision = 0;
	_timeOfLastShot = IN_THE_PAST;
	_timeOfLastTeamWounding = IN_THE_PAST;
	_timeOfLastTeamDeath = IN_THE_PAST;
	_teamCasualtyScale = 0;
	_woundLevel = 0;
	_injured = false;
	_killed = false;
	_timesSuppressed = 0;
	_fired = false;
	_moraleAdjustment = 0;
}

// Accessor methods.

float Morale::GetMorale() { return _morale; }

float Morale::GetMoraleMax() { return _moraleMax; }

float Morale::GetMoraleLongTerm() { return _moraleLongTerm / _moraleMax; }

float Morale::GetMoraleLevel() { return _moraleLevel; }

float Morale::GetMoraleRecovery() { return _moraleRecovery; }

float Morale::GetMoraleLoss() { return _moraleLoss; }

float Morale::GetExertion() { return _exertion; }

float Morale::GetNetMoraleChange() {
	if (_moraleRecovery>_moraleLoss)
	  return (_moraleRecovery-_moraleLoss) * (_moraleLevel+50.0)/150.0;
	else
	  return _moraleRecovery-_moraleLoss;
}

float Morale::GetSuppressionRadius() { return _initialSuppressionRadius *(1.0+(100-_moraleLevel)/100.0*MORALE_IMPACT_ON_SUPPRESSION); }

float Morale::GetInitialSuppressionRadius() { return _initialSuppressionRadius; }

float Morale::GetHarshPenalty() { return _harshPenalty; }

float Morale::GetMildPenalty() { return _mildPenalty; }

float Morale::GetOrderMissed() { return GetMildPenalty(); }

float Morale::GetTunnelVision() const { return _tunnelVision; }

int Morale::GetTick() { return _tick; }

// TODO: If a soldierQualities class is written (which I feel should be the case)
// then these methods should belong to it (and be removed from here).
float Morale::GetTraining() { return _training/100.0f; }
float Morale::GetExperience() { return _experience/100.0f; }
float Morale::GetEndurance() { return _endurance/100.0f; }
float Morale::GetSoldierQuality() { return _soldierQuality/100.0f; }
float Morale::GetLeadership() { return _leadership/100.0f; }
float Morale::GetSkill() { return (_training+_experience)/200.0f; }
// A scaling factor that affects the maximum fatigue levels of
// the individual.
float Morale::GetEnduranceAdjustment() {return _enduranceFactor; }



// Mutator methods from here on down.

void Morale::SetEndurance(float val) 
{
	_endurance = 100.0 * val;
	if (_endurance<1)
		_endurance = 1.0;
	Recalculate();
}

void Morale::SetTraining(float val) 
{
	_training = 100.0 * val;
	if (_training<1)
		_training = 1.0;
	Recalculate();
}
void Morale::SetExperience(float val) 
{
	_experience = 100.0 * val;
	if (_experience<1)
		_experience = 1.0;
	Recalculate();
}
void Morale::SetLeadership(float val) 
{
	_leadership = 100.0 * val;
	if (_leadership<1)
		_leadership = 1.0;
}

void Morale::SquadMateInjured() 
{
	_timeOfLastTeamWounding = _tick;
	_injured = true;
	}

void Morale::SquadMateKilled() 
{
	_timeOfLastTeamDeath = _tick;
	_killed = true;
	}

void Morale::SetWoundLevel(float level) { _woundLevel = level; }

// The soldier has been suppressed. "Store" that result so it can be utilised
// next time we do an update.
void Morale::HaveBeenSuppressed() 
{
	_timesSuppressed++;
}


// Soldier has fired their weapon. Record that as may lead
// to increase in morale.
void Morale::HaveFired() 
{
	_fired = true;
}

// Adjust (for the next second only) the morale recovery rate
// Passed argument should be in raw morale points.
// Provided for outsiders to "tweak" the morale system.
void Morale::AddToMoraleRaw(float val) 
{
	_moraleAdjustment += val;
}

// Adjust (for the next second only) the morale recovery rate
// Passed argument should be in morale levels.
// Provided for outsiders to "tweak" the morale system.
void Morale::AddToMorale(float val) 
{
	AddToMoraleRaw(val/100.0*_moraleMax);
}


// Another frame. Morale only updates once per second so first check whether
// is time to do update. If not, return. If appropriate then recalculate all
// the primary and derived stats associated with morale.
 void Morale::Simulate(float deltaT, float currentExertion, float volumeFire) 
 {
	 _accTime += deltaT;
	 if (_accTime<MORALE_UPDATE_PERIOD)
		 return;
	 _accTime = 0;
	_moraleRecovery = _basicMoraleRecovery + _moraleAdjustment;
	// Gain some morale for firing, but not too foten.
	if (_fired && _tick>=_timeOfLastShot+INTER_SHOT_INTERVAL_MORALE) {
		_moraleRecovery += SHOT_MORALE_BOOST;
		_timeOfLastShot = _tick;
	}
	_fired = false;
	// Wounded or killed squadmates affect morale recovery but have a
	// diminishing impact as time goes on.
	_teamCasualtyScale = 1.0;
	float woundScale = 1.0;
	float killScale = 1.0;
	float timeSince;
	if (_timeOfLastTeamWounding+INJURED_MORALE_DURATION>_tick) {
		timeSince = _tick-_timeOfLastTeamWounding;
		woundScale = INJURED_SECTION_MORALE_SCALE + timeSince/INJURED_MORALE_DURATION*(1-INJURED_SECTION_MORALE_SCALE);
	}
	if (_timeOfLastTeamDeath+DEAD_MORALE_DURATION>_tick) {
		timeSince = _tick-_timeOfLastTeamDeath;
		killScale = DEAD_SECTION_MORALE_SCALE + timeSince/DEAD_MORALE_DURATION*(1-DEAD_SECTION_MORALE_SCALE);
	}
	if (killScale<woundScale)
	  _teamCasualtyScale = killScale;
	else
	  _teamCasualtyScale = woundScale;
	_moraleRecovery *= _teamCasualtyScale;
	_exertion = (currentExertion)*100.0;
	if (_exertion>100)
	  _exertion = 100.0;
	_moraleLoss = _exertion*EXERTION_LOSS_FACTOR;
	_moraleLoss += _timesSuppressed*SUPPRESSION_MORALE_COST;
	_moraleLoss += volumeFire*FIRE_VOLUME_COST;
	if (_injured)
	  _moraleLoss += INJURED_SECTION_MORALE_PENALTY;
	if (_killed)
	  _moraleLoss += DEAD_SECTION_MORALE_PENALTY;
	_tick++;
	if (_moraleRecovery>_moraleLoss)
	  _morale += (_moraleRecovery-_moraleLoss) * (_moraleLevel+50.0)/150.0;
	else
	  _morale -= (_moraleLoss-_moraleRecovery);
	if (_morale>_moraleMax)
	  _morale = _moraleMax;
	else if (_morale<0)
	  _morale = 0.0;
	_woundLevel = _owner->GetTotalDamage();
	if (_morale>_moraleMax*(1-_woundLevel))
	  _morale = _moraleMax * (1-_woundLevel);

	if (_windowIndex>=MORALE_WINDOW_SIZE)
		_windowIndex = 0;;
	float replace = _moraleWindow[_windowIndex];
	_moraleWindow[_windowIndex++] = _morale;
	_moraleLongTerm += (_morale-replace)/MORALE_WINDOW_SIZE;
	_moraleLevel = _morale/_moraleMax*100.0;

	// Update the stress/fatigue/anaerobic based on how low morale is.
	float stressFromMorale = (100-_moraleLevel)*STRESS_FROM_MORALE;
	_owner->AddAnaerobicExertionBySecond(stressFromMorale);

	// Calculate the chance of missing an order
	float term;
	if (Glob.config.IsEnabled(DTLongTermMorale))
		term = (_moraleLongTerm+0.1)/_moraleMax;
	else
		term = (_moraleLevel+1)/100.0f;
	if (term>1)
		term=1;
	_mildPenalty = 1 - sqrt(sqrt(term));
	_harshPenalty = 1 - sqrt(term);

  // Tunnel vision can be invoked also by fatigue
  _tunnelVision = max(1 - sqrt(term), _owner->GetFatigue() * _owner->GetFatigue());

	_injured = false;
	_killed = false;
	_timesSuppressed = 0;
	_moraleAdjustment = 0;
}
#endif



