#if _BULDOZER
#define APP_NAME				"Buldozer Viewer"
#define APP_NAME_SHORT	"Buldozer"
#define ICONNAME        "buldozer.ico"
#define APP_ICONS(XX)
#elif _VBS2
#define APP_NAME				"VBS2"
#define APP_NAME_SHORT	"VBS2"
#define ICONNAME        "VBS2.ico"
#define APP_REGISTRY(XXX)  XXX(APP_NAME)
#define APP_ICONS(XX)
#elif _FP2
#define APP_NAME				"FP2"
#define APP_NAME_SHORT	"FP2"
#define ICONNAME        "Game2.ico"
#define APP_REGISTRY(XXX)  XXX(APP_NAME)
#define APP_ICONS(XX)
#elif _VBS1
#define APP_NAME				"VBS1"
#define APP_NAME_SHORT	"VBS1"
#define ICONNAME        "VBS1.ico"
#define APP_REGISTRY(XXX)  XXX(APP_NAME)
#define APP_ICONS(XX)
#elif _VBS1_DEMO
#define APP_NAME				"VBS1 Demo"
#define APP_NAME_SHORT	"VBS1"
#define ICONNAME        "VBS1.ico"
#define APP_REGISTRY(XXX)  XXX(APP_NAME)
#define APP_ICONS(XX)
#elif _DEMO
#define APP_NAME				"ArmA 2 OA DEMO"
#define APP_NAME_SHORT	"ArmA2OADEMO"
#define ICONNAME        "Arma2OADemo.ico"
#define APP_REGISTRY(XXX)  XXX(APP_NAME)
#define APP_ICONS(XX)
#elif _LIBERATION
#define APP_NAME				"Ironfront"
#define APP_NAME_SHORT	"IF"
#define ICONNAME        "Logo_IFA.ico"
#define APP_REGISTRY(XXX)  XXX(APP_NAME)
#define APP_ICONS(XX)
#else
#define APP_NAME				"ArmA 2 OA"
#define APP_NAME_SHORT	"ArmA2OA"
#define ICONNAME        "ArrowHead_01.ico"
#define APP_REGISTRY(XXX) \
  XXX("ArmA 2 OA") \
  XXX("Arma 2 REINFORCEMENTS")
#define APP_ICONS(XX) \
  XX(APPICON+1, "ArrowHead_02.ico") \
  XX(APPICON+2, "ArrowHead_03.ico") \
  XX(APPICON+3, "ArrowHead_04.ico") \
  XX(APPICON+4, "ArrowHead_05.ico") \
  XX(APPICON+5, "ArrowHead_06.ico") \
  XX(APPICON+6, "ArrowHead_07.ico") \
  XX(APPICON+7, "ArrowHead_08.ico") \
  XX(APPICON+8, "ArrowHead_09.ico") \
  XX(APPICON+9, "ArrowHead_10.ico")
#endif

#define RC_ICONS(id, filename) \
  id ICON DISCARDABLE filename

#define AppName APP_NAME

