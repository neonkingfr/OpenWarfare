// Configuration parameters for VBS1

#define _VERIFY_KEY								1
#define _VERIFY_CLIENT_KEY  			1  // server is checking the CD key of clients on GameSpy
#define _VERIFY_BLACKLIST					0
#define _ENABLE_EDITOR						1
#define _ENABLE_WIZARD						1
#define _ENABLE_ALL_MISSIONS			0
#define _ENABLE_MP								1
#define _ENABLE_AI								1
#define _ENABLE_CAMPAIGN					0
#define _ENABLE_SINGLE_MISSION		1
#define _ENABLE_UNSIGNED_MISSIONS	1
#define _ENABLE_BRIEF_GRP					1
#define _ENABLE_PARAMS						1
#define _ENABLE_CHEATS						0
#define _ENABLE_ADDONS						1
#define _ENABLE_DEDICATED_SERVER	1
#define _FORCE_DEMO_ISLAND				0
#define _FORCE_SINGLE_VOICE				0
#define _ENABLE_AIRPLANES					1
#define _ENABLE_HELICOPTERS				1
#define _ENABLE_SHIPS							1
#define _ENABLE_CARS							1
#define _ENABLE_TANKS							1
#define _ENABLE_MOTORCYCLES				1
#define _ENABLE_PARACHUTES				1
#define _ENABLE_DATADISC					1
#define _ENABLE_VBS								1
#define _DISABLE_CRC_PROTECTION   0
#define _FORCE_DS_CONTEXT         0
#define _ENABLE_GAMESPY           0
#define _ENABLE_NEWHEAD           0
#define _ENABLE_SKINNEDINSTANCING 0
#define _ENABLE_BB_TREES          0
#define _ENABLE_CONVERSATION      0
#define _ENABLE_SPEECH_RECOGNITION  0
#define _ENABLE_EDITOR2           0
#define _ENABLE_EDITOR2_MP        0
#define _ENABLE_IDENTITIES        0
#define _ENABLE_INVENTORY         0
#define _ENABLE_MISSION_CONFIG    0
#define _ENABLE_INDEPENDENT_AGENTS  0
#define _ENABLE_DIRECT_MESSAGES   0
#define _ENABLE_FILE_FUNCTIONS    1
#define _ENABLE_HAND_IK           0
#define _ENABLE_WALK_ON_GEOMETRY  0
#define _ENABLE_BULDOZER          0
#define _ENABLE_COMPILED_SHADER_CACHE 1
#define _ENABLE_DISTRIBUTIONS     0
#define _USE_FCPHMANAGER          1
#define _ENABLE_DX10              0
#define _USE_BATTL_EYE_SERVER     1
#define _USE_BATTL_EYE_CLIENT     1
#define _ENABLE_STEAM             0

#define _TIME_ACC_MIN							1.0
#define _TIME_ACC_MAX							4.0
																
#define _MACRO_CDP							  0

// Registry key for saving player's name, IP address of server etc.
#undef ConfigApp
#define ConfigApp "Software\\BIS\\VBS1"

#define _MOD_PATH_DEFAULT "VBS"

// CD Key parameters
const bool CDKeyRegistryUser = false;
const unsigned char CDKeyPublicKey[] =
{
  0x11, 0x00, 0x00, 0x00,
  0xF9, 0x54, 0x9E, 0x33, 0x73, 0xCD, 0x0F, 0x5A, 0x15, 0x85, 0xE2, 0xAC, 0x32, 0x80, 0xA1
};
const char CDKeyFixed[] = "VBSystem 1";
const int CDKeyIdLength = (sizeof(CDKeyPublicKey) / sizeof(*CDKeyPublicKey) - 4) - strlen(CDKeyFixed);
