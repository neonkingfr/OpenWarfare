#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PLUGINS_HPP
#define _PLUGINS_HPP

#include "wpch.hpp"
#include <El/FileServer/fileServer.hpp>
#include <El/Evaluator/expressImpl.hpp>

#ifdef _VBS3
#define _PLUGINSYSTEM
#endif

#ifdef _PLUGINSYSTEM

//!{ Plugin DLL function declarations
typedef void (WINAPI *REGISTERHWND)(void *hwnd, void *hins);
typedef void (WINAPI *REGISTERCOMMANDFNC)(void *executeCommandFnc);
typedef void (WINAPI *ONSIMULATIONSTEP)(float deltaT);
typedef const char* (WINAPI *PLUGINFUNCTION)(const char *input);
//!}

//! Class to hold informations about a particular registered plugin
class PluginInfo
{
private:
  //! Handle to DLL that stores the plugin content
  HINSTANCE _hDll;
  //! Name of the plugin dll (without the extension), name can be used as the pluginFunction script function parameter
  //! RStringB is used here, because we use the name for locating the right plugin in the plugin list
  RStringB _name;
  //! Function to be called every simulation step
  ONSIMULATIONSTEP _onSimulationStep;
  //! Function to be called every time the script in engine asks
  PLUGINFUNCTION _pluginFunction;
  //! Variable space of the plugin
  GameVarSpace _vars;
  //! Zero the object
  void Clear();
public:
  //! Constructor
  PluginInfo() : _vars(true) {Clear();};
  //!{ Degenerate copy and assignment - we can't afford to copy the object, as _hDll is destructed in the destructor
  PluginInfo(const PluginInfo&) {Clear();}
  PluginInfo& operator=(const PluginInfo&) {Clear();}
  //!}
  //! Destructor
  ~PluginInfo();
  //! Initialization
  void Init(HINSTANCE hDll, RStringB name, ONSIMULATIONSTEP onSimulationStep, PLUGINFUNCTION pluginFunction);
  //! Determine whether specified name matches the name of the plugin
  bool NameMatches(RStringB pluginName) const;
  //!{ Script variable context handling
  void BeginContext();
  void EndContext();
  //!}
  //! Run the event OnSimulationStep
  void OnSimulationStep(float deltaT);
  //! Run the command PluginFunction
  const char *PluginFunction(const char *input);
};
TypeIsGeneric(PluginInfo)

//! List of registered plugins
extern AutoArray< PluginInfo > Plugins;

//! Load the plugins
void RegisterPlugins();

#endif

#endif
