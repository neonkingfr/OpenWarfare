#ifdef _MSC_VER
#pragma once
#endif

#ifndef _OBJECT_ID_HPP
#define _OBJECT_ID_HPP

#include <Es/Strings/rString.hpp>
#include <Es/Containers/listBidir.hpp>
#include <El/ParamArchive/serializeClass.hpp>

class ParamArchive;

//! unique object id (given by visitor)
class VisitorObjId
{
	int _id;

	public:

	//! constructor
	explicit VisitorObjId(int id):_id(id) {}
	//! default constructor - uninitialized Id
	VisitorObjId():_id(-1) {}

	//! get numerical value
	operator int() const {return _id;}

	//! serialize into ParamArchive
	LSError Serialize(ParamArchive &archive, const RStringB &name, int minVersion, const VisitorObjId &defValue);
	//! serialize into ParamArchive, default value nill
	LSError Serialize(ParamArchive &archive, const RStringB &name, int minVersion)
	{
		return Serialize(archive,name,minVersion,VisitorObjId());
	}

	//! encode for saving/transfer etc.
	int Encode() const {return _id;}
	//! decode
	void Decode(int val) {_id=val;}
	//! prepare a new ID
	void Increment() {_id++;}

	//! equality operator
	bool operator ==(const VisitorObjId &with) const {return _id == with._id;}
};

//! no id assigned (positive)
#define VISITOR_NO_ID VisitorObjId(-1)
//! no id assigned (uninitialized, need to be assigned before it can be used)
#define VISITOR_UNINIT_ID VisitorObjId()

enum _vehicleId {VehicleId};

//! unique object id, including information necessary for object loading
class ObjectId
{
	int _encoded;

	enum
	{
		// define bit layout:
		// B31 - isObjectFlag
		// if isObjectFlag:
		//    B30-B21: z
		//    B20-B11: x
		//    B10- B0: id
		// else
		//    B30- B0: id

		ObjZBits=10U,
		ObjXBits=10U,
		ObjIdBits=11U,

		VehicleIdBits=ObjIdBits+ObjXBits+ObjZBits,
		IsObjectMask=0x80000000,
		VehicleIdMask=0x7fffffff,
	};

  public:
	enum
	{
	  // some numbers need to be accessed publicly
		MaxObjectId=(1U<<ObjIdBits)-1U,
		MaxVehicleId=0x7fffffff,
    MaxXLandGrid=(1U<<ObjXBits)-1U,
    MaxZLandGrid=(1U<<ObjZBits)-1U
  };

  private:
	static inline int EncodeVehicle(int id)
	{
		Assert(id>=-1);
		Assert(id<MaxVehicleId);
		return id+1;
	}
	static inline int EncodeObject(int x, int z, int id)
	{
		Assert(id>=0);
		Assert(id<=MaxObjectId);
		return IsObjectMask|(z<<(ObjIdBits+ObjXBits))|(x<<ObjIdBits)|id;
	}

	public:
	//! static object ID
	ObjectId(int x, int z, int id)
	{
		// explicit coding required?
		_encoded = EncodeObject(x,z,id);
	}
	//! vehicle ID
	ObjectId(_vehicleId,int id)
	{
		_encoded = EncodeVehicle(id);
	}
	//! no ID
	ObjectId()
	{
		_encoded = 0;
	}
	bool IsNull() const
	{
		return _encoded == 0;
	}
	bool IsObject() const
	{
		return (_encoded&IsObjectMask)!=0;
	}
	int GetObjId() const
	{
		Assert(IsObject());
		return _encoded&((1U<<ObjIdBits)-1);
	}
	int GetObjX() const
	{
		Assert(IsObject());
		return (_encoded>>ObjIdBits)&((1U<<ObjXBits)-1);
	}
	int GetObjZ() const
	{
		Assert(IsObject());
		return (_encoded>>(ObjIdBits+ObjXBits))&((1U<<ObjZBits)-1);
	}
	int GetVehId() const
	{
		Assert(!IsObject());
		return (_encoded&VehicleIdMask)-1;
	}
	static int GetObjIdCandidate(int i)
	{
		return i&MaxObjectId;
	}

	RString GetDebugName() const;

	//! serialize into ParamArchive
	LSError Serialize(ParamArchive &archive, const RStringB &name, int minVersion, const ObjectId &defValue);
	//! serialize into ParamArchive, default value nill
	LSError Serialize(ParamArchive &archive, const RStringB &name, int minVersion)
	{
		return Serialize(archive,name,minVersion,ObjectId());
	}

	//! encode for saving/transfer etc.
	int Encode() const {return _encoded;}
	//! decode
	void Decode(int val) {_encoded = val;}

	//! equality operator
	bool operator ==(const ObjectId &with) const {return _encoded == with._encoded;}

	//! equality operator
	bool operator !=(const ObjectId &with) const {return _encoded != with._encoded;}
};


//! id that will be destroyed, once landscape it points to is destroyed

class ObjectIdAutoDestroy: public ObjectId, public TLinkBidirD
{
  public:
  ObjectIdAutoDestroy()
  {
  }
  ObjectIdAutoDestroy(ObjectId id, TListBidir<ObjectIdAutoDestroy> &list)
  :ObjectId(id)
  {
    if (!IsNull())
    {
      list.Insert(this);
    }
  }
  ~ObjectIdAutoDestroy()
  {
    if (IsInList())
    {
      TLinkBidirD::Delete();
    }
  }

  // make new copy a member of the same list as source link is
  ObjectIdAutoDestroy(const ObjectIdAutoDestroy &src);

  void operator = (const ObjectIdAutoDestroy &src);

};

#endif

