@echo off
set errorlvl=%errorlevel%

pushd
md O:\Arma2_Expansion\verHistory\output

O:
cd \Arma2_Expansion\

REM -cpuCount=1
echo -noCB "-cpuCount=1" > O:\Arma2_Expansion\verhistory\output\bedar_fps0.rpt
echo ArmA2ExpInt.exe -mod=expansion -nopause -nosplash -autotest=O:\Arma2_Expansion\autotest\autotestFunct_EXP.cfg -noCB -cpuCount=1
ArmA2ExpInt.exe -mod=expansion -nopause -nosplash -autotest=O:\Arma2_Expansion\autotest\autotestFunct_EXP.cfg -noCB -cpuCount=1
if errorlevel 1 if errorlevel %errorlvl% echo Autotest failed! - autotestFPSpc failed on (-cpuCount=1) errorlevel: %errorlevel%
if errorlevel %errorlvl% set errorlvl=%errorlevel%

c:\bis\bedar\deriveFPS.pl "C:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA\debug.log" O:\Arma2_Expansion\verhistory\output\bedar_fps0.rpt

REM -cpuCount=2
echo -noCB "-cpuCount=2" > O:\Arma2_Expansion\verhistory\output\bedar_fps1.rpt
echo ArmA2ExpInt.exe -mod=expansion -nopause -nosplash -autotest=O:\Arma2_Expansion\autotest\autotestFunct_EXP.cfg -noCB -cpuCount=2
ArmA2ExpInt.exe -mod=expansion -nopause -nosplash -autotest=O:\Arma2_Expansion\autotest\autotestFunct_EXP.cfg -noCB -cpuCount=2
if errorlevel 1 if errorlevel %errorlvl% echo Autotest failed! - autotestFPSpc failed on (-cpuCount=2) errorlevel: %errorlevel%
if errorlevel %errorlvl% set errorlvl=%errorlevel%

c:\bis\bedar\deriveFPS.pl "C:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA\debug.log" O:\Arma2_Expansion\verhistory\output\bedar_fps1.rpt


REM -useCB -cpuCount=2
echo "-useCB -cpuCount=2" > O:\Arma2_Expansion\verhistory\output\bedar_fps2.rpt
echo ArmA2ExpInt.exe -mod=expansion -nopause -nosplash -autotest=O:\Arma2_Expansion\autotest\autotestFunct_EXP.cfg -useCB -cpuCount=2
ArmA2ExpInt.exe -mod=expansion -nopause -nosplash -autotest=O:\Arma2_Expansion\autotest\autotestFunct_EXP.cfg -useCB -cpuCount=2
if errorlevel 1 if errorlevel %errorlvl% echo Autotest failed! - autotestFPSpc failed on (-useCB -cpuCount=2) errorlevel: %errorlevel%
if errorlevel %errorlvl% set errorlvl=%errorlevel%

c:\bis\bedar\deriveFPS.pl "C:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA\debug.log" O:\Arma2_Expansion\verhistory\output\bedar_fps2.rpt


REM -useCB -cpuCount=4
echo "-useCB -cpuCount=4" > O:\Arma2_Expansion\verhistory\output\bedar_fps3.rpt
echo ArmA2ExpInt.exe -mod=expansion -nopause -nosplash -autotest=O:\Arma2_Expansion\autotest\autotestFunct_EXP.cfg -useCB -cpuCount=4
ArmA2ExpInt.exe -mod=expansion -nopause -nosplash -autotest=O:\Arma2_Expansion\autotest\autotestFunct_EXP.cfg -useCB -cpuCount=4
if errorlevel 1 if errorlevel %errorlvl% echo Autotest failed! - autotestFPSpc failed on (-useCB -cpuCount=4) errorlevel: %errorlevel%
if errorlevel %errorlvl% set errorlvl=%errorlevel%

c:\bis\bedar\deriveFPS.pl "C:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2 OA\debug.log" O:\Arma2_Expansion\verhistory\output\bedar_fps3.rpt
c:\bis\bedar\deriveSlowFrames.pl "C:\Documents and Settings\%username%\Local Settings\Application Data\ArmA 2\debug.log" o:\arma\verhistory\output\bedar_slowFrames.rpt

type O:\Arma2_Expansion\verhistory\output\bedar_fps0.rpt >  O:\Arma2_Expansion\verhistory\output\bedar_fpsPerftest.rpt
type O:\Arma2_Expansion\verhistory\output\bedar_fps1.rpt >> O:\Arma2_Expansion\verhistory\output\bedar_fpsPerftest.rpt
type O:\Arma2_Expansion\verhistory\output\bedar_fps2.rpt >> O:\Arma2_Expansion\verhistory\output\bedar_fpsPerftest.rpt
type O:\Arma2_Expansion\verhistory\output\bedar_fps3.rpt >> O:\Arma2_Expansion\verhistory\output\bedar_fpsPerftest.rpt


REM Last line! :)
popd