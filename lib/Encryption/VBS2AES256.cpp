#include "../wpch.hpp"

#if _VBS2
#include <El/QStream/qbStream.hpp>
#include <Es/Common/win.h>
#include <El/Encryption/aes.h>

#define AESDebug 0 && _ENABLE_CHEATS

#if AESDebug 
	#define AESLogF LogF
#else
	#define AESLogF		    
#endif

#define AES_IVSIZE   16
#define AES_KEYSIZE  16  // 128 bit key 16 bytes

// AES_KEYSIZE
// 16 bytes = 128 bit key
// 32 bytes = 256 bit key
// 0x76 == 1 byte.
unsigned char VBS2Key[AES_KEYSIZE] = 
{ 
	0x76,0x3f,0x7d,0x6e,
	0x53,0x27,0x46,0x4e,
	0x32,0x3d,0x63,0x22,
	0x6c,0x69,0x4f,0x2d
};
unsigned char VBS2MKey[AES_KEYSIZE] =
{
  0xb6,0x94,0x9e,0xc7, 
  0x66,0x8f,0x32,0xe8, 
  0x08,0x47,0x5d,0x57, 
  0x92,0x27,0xac,0xcc 
};

unsigned char Blank[AES_KEYSIZE] = { 0x0 };
                   
class FilebankEncryptionVBS2 : public IFilebankEncryption
{
  bool _addonEnc;
protected:

public:
  FilebankEncryptionVBS2(bool addonEnc):_addonEnc(addonEnc){};
	~FilebankEncryptionVBS2(){}

	/**
	 dst - Destination
	 uncompressedSize - logic file size, no padding
	 inBuf - physical size, 16 byte chunks
     initVec[16] - inilization vector, 16 bytes long.

	 requirement: inBuf data to start at 16 byte boundaires. Cannot start at
	 5 for example, only at offsets%16==0.

	 initVec should be the inilization vector, if offset is at 16, this should be
	 from 0 to 16th byte. If offset is at 0, this should be the inilization vector
	 of the file header!
	 */
	bool Decode(char *dst,long uncompressedSize,QIStream &inBuf,unsigned char initVec[16] )
	{
		aes_context ctx;
		AutoArray<char> buf;
		WriteAutoArrayChar writeBuf(buf);
		inBuf.Process(writeBuf);  // writes everthing into writeBuffer
		
    unsigned char hash[AES_KEYSIZE] = { 
      0x2D, 0xE5, 0x7A, 0x9F,
			0xA0, 0xE0, 0x3B, 0x4D,
			0xC8, 0xEB, 0xBB, 0x3C,
      0x17, 0x2B, 0x04, 0x7E};

    for( int i = 0; i < AES_KEYSIZE; i++ )
      hash[i] ^= (_addonEnc ? VBS2Key[i] : VBS2MKey[i]);

    // set the key and decode the data
    aes_set_key( &ctx, hash, AES_KEYSIZE*8 );
		aes_cbc_decrypt(&ctx,initVec,(unsigned char*)buf.Data(),(unsigned char*)buf.Data(),buf.Size());

    // uninialise key
    ZeroMemory(hash,sizeof(hash));
		aes_set_key( &ctx, Blank, AES_KEYSIZE*8 ); 
		
		memcpy(dst,buf.Data(),uncompressedSize);

	  return true;
	}

	bool Decode(char *dst, long uncompressedSize, QIStream &inBuf)
	{
		AESLogF("AES Decode shouldn't be called");
		DoAssert(false);
		return false;
	};

	void Encode(QOStream &out, QIStream &in)
	{					
		AESLogF("AES Encode shouldn't be called");
		DoAssert(false);
	};

	void Encode(QOStream &out, QIStream &in,unsigned char intVec[16])
	{
		aes_context ctx;
		AutoArray<char> buffer;
		WriteAutoArrayChar writeBuffer(buffer);
		in.Process(writeBuffer);

		char padd = '\0';
		while((buffer.Size()%16)!=0)
			buffer.Add(padd);

    unsigned char hash[AES_KEYSIZE] = { 
      0x2D, 0xE5, 0x7A, 0x9F,
			0xA0, 0xE0, 0x3B, 0x4D,
			0xC8, 0xEB, 0xBB, 0x3C,
      0x17, 0x2B, 0x04, 0x7E};

    for( int i = 0; i < AES_KEYSIZE; i++ )
      hash[i] ^= (_addonEnc ? VBS2Key[i] : VBS2MKey[i]);

    // set key and encrypt the data
    aes_set_key( &ctx, hash, AES_KEYSIZE*8 );   // setup key
		aes_cbc_encrypt(&ctx,intVec,(unsigned char*)buffer.Data(),(unsigned char*)buffer.Data(),buffer.Size());
    
    // uninilaise the hash key and aes key from the stack
    ZeroMemory(hash,sizeof(hash));
		aes_set_key( &ctx, Blank, AES_KEYSIZE*8 ); 

    out.write(buffer.Data(), buffer.Size());
	};
};

IFilebankEncryption *CreateEncryptVBS2AESMission(const void *context)
{
	return new FilebankEncryptionVBS2(false);
}

IFilebankEncryption *CreateEncryptVBS2AESAddon(const void *context)
{
	return new FilebankEncryptionVBS2(true);
}

#endif