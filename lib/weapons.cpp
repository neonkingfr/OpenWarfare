// weapons table

#include "wpch.hpp"

#include "landscape.hpp"
#include "weapons.hpp"
#include "global.hpp"
#include "vehicleAI.hpp"
#include "AI/ai.hpp"
#include "world.hpp"
#include "fileLocator.hpp"
#include "rtAnimation.hpp"
#include "Network/network.hpp"
#include <Es/Algorithms/qsort.hpp>
#include <Es/ErrorProp/errorProp.hpp>

#if _ENABLE_HAND_IK
#include "motion.hpp"
#endif

void RandomSound::Load( ParamEntryPar entry, const char *name )
{
  ParamEntryVal list=entry>>name;
  _pars.Realloc(list.GetSize()/2);
  _pars.Resize(0);
  for( int i=0; i<list.GetSize(); i+=2 )
  {
    RString singleName=list[i];
    // load single sound parameter
    SoundProbab extPars;
    ParamEntryVal single=entry>>singleName;
    GetValue(extPars, single);
    extPars._probability=list[i+1];
    _pars.Add(extPars);
  }
  _pars.Compact();
}

void PPEffectType::Load(ParamEntryPar cfg)
{
  // effect type
  _effectType = cfg >> "type";
  _priority = cfg >> "priority";

  // load effect parameters
  ParamEntryVal parEntry = cfg >> "params";

  if (parEntry.IsArray())
  {
    // number of pars
    int size = parEntry.GetSize();
    _pars.Realloc(size);
    _pars.Resize(size);

    // load effect parameters
    for (int i = 0; i < size; i++)
    {
      _pars[i] = parEntry[i];
    }
  }
}

const SoundPars &RandomSound::SelectSound( float probab ) const
{
  Assert( _pars.Size()>0 );
  if( _pars.Size()<=0 )
  {
    static const SoundPars nil;
    return nil;
  }
  int i;
  for( i=0; i<_pars.Size()-1; i++ )
  {
    probab-=_pars[i]._probability;
    if( probab<=0 ) return _pars[i];
  }
  return _pars[i];
}

bool RandomSound::PreloadSounds() const
{
  for (int i = 0; i < _pars.Size(); i++)
  {
    if (_pars[i].name.GetLength() > 0)
    {
      Ref<AbstractWave> wave = GSoundScene->Open(_pars[i].name);

      if (wave)
      {
        if (!wave->PreparePlaying()) return false;
      }
    }
  }

  return true;
}

// =================================================
// CameraShakePars implementation
// =================================================
void CameraShakePars::Load(ParamEntryPar cfg)
{
  if (!cfg.IsClass())  
    return; //bad cfg

  ConstParamEntryPtr par = cfg.FindEntry("power");
  if (par)
  {
    _power = *par;
  }
  par = cfg.FindEntry("duration");
  if (par)
  {
    _duration = *par;
  }
  par = cfg.FindEntry("frequency");
  if (par)
  {
    _frequency = *par;
  }
  par = cfg.FindEntry("distance");
  if (par)
  {
    _distance = *par;
  }
  par = cfg.FindEntry("minSpeed");
  if (par)
  {
    _minSpeed = *par;
  }

  _isValid = true;
}

#if _VBS3 //MuzzleEvent

DEFINE_ENUM(AmmoEvent,AE,AMMO_EVENT_ENUM)

void AmmoType::OnEvent(AmmoEvent event, const GameValue &pars) const
{
  // execute type based event handler
  RString handler = _eventHandlers[event];
  GameVarSpace local(false);
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&local);
  gstate->VarSetLocal("_this",pars,true);
  gstate->Execute(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  gstate->EndContext();
}

bool AmmoType::IsEventHandler(AmmoEvent event) const
{
  RString handler = _eventHandlers[event];
  if (handler.GetLength()>0) return true;
  return false;
}
#endif

void AmmoType::InitShape()
{
  //LogF("Init shape ammo %s",(const char *)GetName());
  base::InitShape();
  // load cartridge shape
  RStringB cartridge = ParClass()>>"cartridge";
  if (cartridge.GetLength()>0)
  {
    Ref<EntityType> type=VehicleTypes.New(cartridge);
    EntityAIType *typeAI = dynamic_cast<EntityAIType *>(type.GetRef());
    if (typeAI)
    {
      _cartridgeType = typeAI;
      _cartridgeType->VehicleAddRef(); // force loading shape
    }
    else
    {
      RptF("Warning: unable to create ammo cartridge: %s",(const char *)cartridge);
  }
  }
  RStringB proxyShape = ParClass()>>"proxyShape";
  if (proxyShape.GetLength()>0)
  {
    LODShapeWithShadow *pshape = Shapes.New(::GetShapeName(proxyShape), false, false);
    _proxyShape = pshape;
  }
}

void AmmoType::DeinitShape()
{
  //LogF("Deinit shape ammo %s",(const char *)GetName());
  if (_cartridgeType)
  {
    _cartridgeType->VehicleRelease(); // allow releasing shape
    _cartridgeType.Free();
  }
  _proxyShape.Free();
  base::DeinitShape();
}

void AmmoType::Load(ParamEntryPar par, const char *shape)
{
  base::Load(par,shape);
  //LogF("Load ammo %s",(const char *)GetName());

  #define GET_PAR(x) x=ParClass(#x)
  GET_PAR(cost);
  GET_PAR(hit);
  GET_PAR(indirectHit);
  GET_PAR(indirectHitRange);
  GET_PAR(maxControlRange);
  GET_PAR(maneuvrability);
  GET_PAR(trackOversteer);
  GET_PAR(trackLead);
  GET_PAR(thrust);
  GET_PAR(thrustTime);
  GET_PAR(initTime);
  GET_PAR(explosionTime);
  GET_PAR(fuseDistance);
  GET_PAR(maxSpeed);
  GET_PAR(sideAirFriction);
  GET_PAR(simulationStep);
  
  _airFriction = _par->ReadValue("airFriction",-0.0005f);
  _coefGravity = _par->ReadValue("coefGravity",1.0f);
  _typicalSpeed2 = Square((*_par)>>"typicalSpeed");

  _tracerScale      = _par->ReadValue("tracerScale",1.0f);
  _nvgOnly          = _par->ReadValue("nvgOnly",false);
  _tracerStartTime  = _par->ReadValue("tracerStartTime",-1.0f); //disables tracer
  _tracerEndTime    = _par->ReadValue("tracerEndTime",4.0f);

  GET_PAR(visibleFire);
  GET_PAR(audibleFire);
  GET_PAR(visibleFireTime);

#if _VBS3
  audibleFireTime = _par->ReadValue("audibleFireTime", visibleFireTime);
#endif

  GET_PAR(irLock);
  GET_PAR(laserLock); // laser lock only
  GET_PAR(nvLock);    // nv marker
  GET_PAR(artilleryLock); // artillery lock only
  
  if (artilleryLock)
  { // disable friction for artillery ammo
    // would be to complex to correctly compute ballistic curve
    _airFriction = sideAirFriction = 0;
  }

  GET_PAR(airLock);
  GET_PAR(manualControl);
  GET_PAR(explosive);
  GET_PAR(caliber);
  GET_PAR(deflecting);

  _deflectionSlowDown = _par->ReadValue("deflectionSlowDown",0.8f);

  GET_PAR(weaponLockSystem);
  GET_PAR(cmImmunity);

  #undef GET_PAR

  _explosionEffects = ParClass("explosionEffects");
  _craterEffects = ParClass("craterEffects");
  ParamEntryVal cls = (*_par) >> "HitEffects";
  int n = cls.GetEntryCount();
  _hitEffects.Realloc(n);
  _hitEffects.Resize(n);
  for (int i=0; i<n; i++)
  {
    ParamEntryVal entry = cls.GetEntry(i);
    _hitEffects[i]._name = entry.GetName();
    _hitEffects[i]._effects = entry;
  }
  _effectsMissile = ParClass("effectsMissile");
  _effectsSmoke = ParClass("effectsSmoke");

  if (deflecting>0) deflecting = sin(HDegree(deflecting));
  _timeToLive = ParClass("timeToLive");
  if (_timeToLive<0) _timeToLive = FLT_MAX;
  if (explosionTime<=0) explosionTime = FLT_MAX;
  
  ConstParamEntryPtr entry = par.FindEntry("defaultMagazine");
  if (entry) _defaultMagazine = *entry;
  
  //LogF("%s: man %.3f",name,maneuvrability);

  // ammo is by default reversed
  _shapeReversed = true;

  /*
  RString model=ParClass("model");
  if( model.GetLength()>0 )
  {
    _shape=Shapes.New(::GetShapeName(model),true,IS_SHADOW_VEHICLE);
  }
  else _shape=NULL;
  */
  _texture=NULL;
  // hit sounds for different materials
  _hit[SurfaceInfo::SHGround].Load(ParClass(),"hitGroundSoft"); // TODO: replace with a factory
  _hit[SurfaceInfo::SHGroundHard].Load(ParClass(),"hitGroundHard");
  _hit[SurfaceInfo::SHMan].Load(ParClass(),"hitMan");
  _hit[SurfaceInfo::SHArmor].Load(ParClass(),"hitArmor");
  _hit[SurfaceInfo::SHIron].Load(ParClass(),"hitIron");
  _hit[SurfaceInfo::SHBuilding].Load(ParClass(),"hitBuilding");
  _hit[SurfaceInfo::SHFoliage].Load(ParClass(),"hitFoliage");
  _hit[SurfaceInfo::SHWood].Load(ParClass(),"hitWood");
  _hit[SurfaceInfo::SHGlass].Load(ParClass(), "hitGlass");
  _hit[SurfaceInfo::SHGlassArmored].Load(ParClass(), "hitGlassArmored");
  _hit[SurfaceInfo::SHPlastic].Load(ParClass(), "hitPlastic");
  _hit[SurfaceInfo::SHConcrete].Load(ParClass(), "hitConcrete");
  _hit[SurfaceInfo::SHRubber].Load(ParClass(), "hitRubber");
  _hit[SurfaceInfo::SHMetalPlate].Load(ParClass(), "hitMetalplate");
  _hit[SurfaceInfo::SHMetal].Load(ParClass(), "hitMetal");
  _hit[SurfaceInfo::SHDefault].Load(ParClass(), "hitDefault"); // used when no suitable material is found
  
  ConstParamEntryPtr bulletEntry = par.FindEntry("bulletFly");
  if (bulletEntry)
  {
    _bulletFly.Load(ParClass(), "bulletFly");
  }

  GetValue(_soundFly, ParClass("soundFly"));
  GetValue(_soundEngine, ParClass("soundEngine"));
  GetValue(_supersonicCrackNear, ParClass("supersonicCrackNear"));
  GetValue(_supersonicCrackFar, ParClass("supersonicCrackFar"));

  _tracerColor = GetPackedColor(ParClass("tracerColor"));
  _tracerColorR = GetPackedColor(ParClass("tracerColorR"));

  RString simName=ParClass("simulation");
  _simulation=AmmoNone;
  if( !strcmpi(simName,"shotShell") ) _simulation=AmmoShotShell;
  else if( !strcmpi(simName,"shotMissile") ) _simulation = AmmoShotMissile;
#if 0 && _VBS3 
  //TODO: first check all places where AmmoShotMissile is used
  else if( !strcmpi(simName,"shotRocket") ) _simulation = AmmoShotRocket;
#else
  else if( !strcmpi(simName,"shotRocket") ) _simulation = AmmoShotMissile;
#endif
  else if( !strcmpi(simName,"shotBullet") ) _simulation = AmmoShotBullet;
  else if( !strcmpi(simName,"shotSpread") ) _simulation = AmmoShotSpread;  
  else if( !strcmpi(simName,"shotIlluminating") ) _simulation=AmmoShotIlluminating;
  else if( !strcmpi(simName,"shotSmoke") ) _simulation = AmmoShotSmoke;
  else if( !strcmpi(simName,"shotTimeBomb") ) _simulation = AmmoShotTimeBomb;
  else if( !strcmpi(simName,"shotPipeBomb") ) _simulation = AmmoShotPipeBomb;
  else if( !strcmpi(simName,"shotMine") ) _simulation = AmmoShotMine;
  else if( !strcmpi(simName,"shotStroke") ) _simulation = AmmoShotStroke;
  else if( !strcmpi(simName,"laserDesignate") ) _simulation = AmmoShotLaser;
  else if( !strcmpi(simName,"shotcm") ) _simulation = AmmoShotCM;
  else if (!strcmpi(simName,"shotNVGMarker") ) _simulation = AmmoShotMarker;
  else if( !strcmpi(simName,"") ) _simulation = AmmoNone;
  else
  {
    ErrF("%s: Bad ammo simulation %s",(const char *)GetName(),(const char *)simName);
  }

#if _VBS3 // AmmoEvent
  ConstParamEntryPtr array = par.FindEntry("EventHandlers");
  if (array)
  {
    for (int i=0; i<NAmmoEvent; i++)
    {
      AmmoEvent e = (AmmoEvent)i;
      RString name = FindEnumName(e);
      ConstParamEntryPtr entry = array->FindEntry(name);
      if (entry)
      {
        RStringB expression = *entry;
        _eventHandlers[e] = expression;
      }
    }
  }
#endif

  _whistleDist = ParClass("whistleDist");
  _whistleOnFire = ParClass("whistleOnFire");

  // read camera shake params
  ParamEntryPtr camShakePar = par.FindEntry("CamShakeExplode");
  if (camShakePar)
  {
    _camShakeExplode.Load(*camShakePar);
  }
  camShakePar = par.FindEntry("CamShakeFire");
  if (camShakePar)
  {
    _camShakeFire.Load(*camShakePar);
  }
  camShakePar = par.FindEntry("CamShakePlayerFire");
  if (camShakePar)
  {
    _camShakePlayerFire.Load(*camShakePar);
  }
  camShakePar = par.FindEntry("CamShakeHit");
  if (camShakePar)
  {
    _camShakeHit.Load(*camShakePar);
  }
}

AmmoType::AmmoType( ParamEntryPar name )
:base(name), _deflectionSlowDown(0.8f)
{
}

RString AmmoType::GetHitEffects(RString name) const
{
  for (int i=0; i<_hitEffects.Size(); i++)
  {
    if (stricmp(_hitEffects[i]._name, name) == 0)
      return _hitEffects[i]._effects;
  }
  return "ImpactEffectsSmall";
}

// class WeaponObject

DEFINE_FAST_ALLOCATOR(WeaponObject)

WeaponObject::WeaponObject(LODShapeWithShadow *shape, const EntityPlainType *type, const CreateObjectId &id)
: base(shape, type, id, false)
{
  Clear();
}

void WeaponObject::Init(const WeaponType *weapon, const MagazineSlot *magazineSlot, const AnimationAnimatedTexture *animFire, int phaseFire, float gunHeatFactor)
{
  _weapon = weapon;
  _magazineSlot = magazineSlot;
  _animFire = animFire;
  _phaseFire = phaseFire;

  _gunHeatFactor = gunHeatFactor;
  _htMin = 0.0f;
  _htMax = 0.0f;
  _afMax = 0.0f;
  _mfMax = 0.0f;
  _mFact = 0.0f;
  _tBody = 0.0f;
}

void WeaponObject::Clear()
{
  _weapon = NULL;
  _magazineSlot = NULL;
  _animFire = NULL;
  _phaseFire = -1;
  _gunHeatFactor = 0.0f;
  _htMin = 0.0f;
  _htMax = 0.0f;
  _afMax = 0.0f;
  _mfMax = 0.0f;
  _mFact = 0.0f;
  _tBody = 0.0f;
}

bool WeaponObject::PrepareShapeDrawMatrices(Matrix4Array &matrices, const ObjectVisualState &vs, LODShape *shape, int level)
{
  if (_weapon && _magazineSlot)
  {
    return unconst_cast(_weapon)->PrepareShapeDrawMatrices(matrices, level, vs, _magazineSlot);
  }
  return false;
}

AnimationStyle WeaponObject::IsAnimated(int level) const {return AnimatedGeometry;}

bool WeaponObject::CanBeInstanced(int level, float distance2, const DrawParameters &dp) const
{
  return false;
}

void WeaponObject::Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2)
{
  if (_animFire)
  {
    LODShapeWithShadow *shape = GetShape();
    if (_phaseFire >= 0)
    {
      _animFire->Unhide(animContext, shape, level);
      _animFire->SetPhaseLoadedOnly(animContext, shape, level, _phaseFire);
    }
    else
    {
      _animFire->Hide(animContext, shape, level);
    }
  }
}

void WeaponObject::Deanimate(int level, bool setEngineStuff)
{
}

float WeaponObject::GetMetabolismFactor() const
{
  return _gunHeatFactor;
}


// class WeaponModeType

WeaponModeType::WeaponModeType()
{
}

void WeaponModeType::Init(ParamEntryPar cls)
{
  _parClass = cls.GetClassInterface();

  _displayName = cls >> "displayName";
  _mult = cls >> "multiplier";
  _burst = cls >> "burst"; // how many bullets are there in single burst

  //GetValue(_sound, cls >> "sound");
  
  _beginSound.Load(cls, "soundBegin"); // semi: single shot variants, burst: first shot, full auto: first shot
  _soundDuration = 0;
  
  _reloadTime = cls >> "reloadTime";
  _dispersion = cls >> "dispersion";
  _recoilName = cls >> "recoil";
  _recoilProneName = cls >> "recoilProne";
  if (_recoilProneName.GetLength()==0) _recoilProneName=_recoilName;
  _autoFire = cls >> "autoFire";
  _aiRateOfFire = cls >> "aiRateOfFire";
  _aiRateOfFireDistance = cls >> "aiRateOfFireDistance";
  _soundContinuous = cls>>"soundContinuous";
  _soundBurst = cls>>"soundBurst";
    /// when burst is fired, only a first bullet is making a sound


  _useAction = cls >> "useAction";
  _useActionTitle = cls >> "useActionTitle";

  _showToPlayer = cls >> "showToPlayer";

  #define GET_PAR(x) x = cls >> #x;
  GET_PAR(minRange); GET_PAR(minRangeProbab);
  GET_PAR(midRange); GET_PAR(midRangeProbab);
  GET_PAR(maxRange); GET_PAR(maxRangeProbab);
  GET_PAR(artilleryDispersion);
  GET_PAR(artilleryCharge);
  #undef GET_PAR

  invMidRangeMinusMinRange=midRange>minRange ? 1/(midRange-minRange) : 1e10;
  invMidRangeMinusMaxRange=maxRange>midRange ? 1/(midRange-maxRange) : -1e10;

  // premultiply with probabilities
  invMidRangeMinusMinRange*=(midRangeProbab-minRangeProbab);
  invMidRangeMinusMaxRange*=(midRangeProbab-maxRangeProbab);
}

// class MagazineType

MagazineType::MagazineType()
:_maxThrowHoldTime(0),
_minThrowIntensityCoef(1.0f),
_maxThrowIntensityCoef(1.0f)
{
  _modelRefCount = 0;
  _magazineShapeRef = 0;
}

void MagazineType::InitShape()
{
  // force ammo type to load shape
  // scan all modes
  if (_ammo) _ammo->VehicleAddRef(); // force loading model

  if (_modelName.GetLength()>0)
  {
    Ref<LODShapeWithShadow> lodShape = Shapes.New(GetShapeName(_modelName),ShapeNormal);
    Ref<EntityPlainType> type = new EntityPlainType(lodShape);

    _model = new WeaponObject(lodShape, type, VISITOR_NO_ID);
  }
  else
  {
    _model.Free();
  }
  if (_model)
  {
    _model->GetShape()->AddLoadHandler(this);
  }
}
void MagazineType::DeinitShape()
{
  if (_model)
  {
    _model->GetShape()->RemoveLoadHandler(this);
  }
  // allow used ammo type to release shape
  // scan all modes
  if (_ammo) _ammo->VehicleRelease(); // force loading model
}

void MagazineType::LODShapeLoaded(LODShape *shape)
{
  if (_model && shape == _model->GetShape())
  {
    const ParamEntry &par = *_parClass;
    _animFire.Init(shape, (par >> "selectionFireAnim").operator RString(), NULL);
  }
}
void MagazineType::LODShapeUnloaded(LODShape *shape)
{
}
void MagazineType::ShapeLoaded(LODShape *shape, int level)
{
  if (_model && shape == _model->GetShape())
  {
    _animFire.InitLevel(shape, level);
  }
}
void MagazineType::ShapeUnloaded(LODShape *shape, int level)
{
  if (_model && shape == _model->GetShape())
  {
    _animFire.DeinitLevel(shape, level);
  }
}

void MagazineType::AmmoAddRef() const
{
  if (_modelRefCount++==0)
  {
    unconst_cast(this)->InitShape();
  }
}

void MagazineType::AmmoRelease() const
{
  if (--_modelRefCount==0)
  {
    unconst_cast(this)->DeinitShape();
  }
}

void MagazineType::InitMagazineShape() const
{
  RStringB wModelName = (*_parClass)>>"model";
  if (wModelName.GetLength()>0)
  {
    _modelMagazine = Shapes.New(GetShapeName(wModelName),false,false);
  }
  else
  {
    _modelMagazine.Free();
  }
}

void MagazineType::DeinitMagazineShape() const
{
  _modelMagazine.Free();
}

void MagazineType::MagazineShapeAddRef() const
{
  if (_magazineShapeRef++==0)
  {
    InitMagazineShape();
  }
}

void MagazineType::MagazineShapeRelease() const
{
  if (--_magazineShapeRef==0)
  {
    DeinitMagazineShape();
  }
  Assert(_magazineShapeRef>=0);
}

RStringB MagazineType::GetPictureName() const
{
  return _picName.GetLength()>0 ? _picName : GetName();
}

void MagazineType::Init(const char *name)
{
  ParamEntryVal cls = Pars >> "CfgMagazines" >> name;
  //CheckAccess(cls);

#if _VBS2 // fatigue model
  _mass = cls.ReadValue("weight",0.5f);
#endif

  _parClass = cls.GetClassInterface();

  _picName = cls>>"picture";
  _scope = cls >> "scope";
  if (_scope == 0)
  {
    WarningMessage("Error: creating magazine %s with scope=private", name);
  }
  _displayName = cls >> "displayName";
  _displayNameShort = cls >> "displayNameShort";

//  _shortName = cls >> "shortName";
  _nameSound = cls >> "nameSound";
  _description = cls >> "Library" >> "libTextDesc";
  _magazineType = cls >> "type";
  _maxAmmo = cls >> "count";
  _maxLeadSpeed = cls >> "maxLeadSpeed";
  _initSpeed = cls >> "initSpeed";
  _invInitSpeed = _initSpeed>0 ? 1.0f / _initSpeed : 1e10f;
  _reloadAction = ExtractActionName(cls >> "reloadAction");
  _quickReload = cls.ReadValue<bool>("quickReload",false);

  ConstParamEntryPtr entry = cls.FindEntry("value");
  if (entry) _value = *entry;
  else _value = 1;

  entry = cls.FindEntry("useAction");
  if (entry) _useAction = *entry;
  else _useAction = false;
  
  entry = cls.FindEntry("useActionTitle");
  if (entry) _useActionTitle = *entry;
  else _useActionTitle = RString();

  _modelName = cls >> "modelSpecial";

  RStringB ammo = cls >> "ammo";
  if (ammo.GetLength() > 0)
  {
    Ref<EntityType> type = VehicleTypes.New(ammo);
    if (!type) ErrF("No class %s", (const char *)ammo);
    _ammo = dynamic_cast<AmmoType *>(type.GetRef());
    if (!_ammo) ErrF("No ammo class %s", (const char *)ammo);
  }
  else
  {
    _ammo = NULL;
  }

  _tracersEvery = cls.ReadValue("tracersEvery", 0);
  _lastRoundsTracer = cls.ReadValue("lastRoundsTracer", 0);

  // throwing params
  _maxThrowHoldTime = cls.ReadValue("maxThrowHoldTime", 2.0f);
  _minThrowIntensityCoef = cls.ReadValue("minThrowIntensityCoef", 0.3f);
  _maxThrowIntensityCoef = cls.ReadValue("maxThrowIntensityCoef", 1.5f);
}

LSError MagazineType::Serialize(ParamArchive &ar)
{
  // note: used only for saving
  if (ar.IsSaving())
  {
    RString name = GetName();
    CHECK(ar.Serialize("name", name, 1));
  }
  return LSOK;
}

MagazineType *MagazineType::CreateObject(ParamArchive &ar)
{
  RString name;
  // get name
  if (ar.Serialize("name", name, 1) != LSOK)
  {
    return NULL;
  }
  return MagazineTypes.New(name);
}


// class MuzzleType
MuzzleType::MuzzleType()
{
}

MuzzleType::~MuzzleType()
{
}

void MuzzleType::Init(ParamEntryPar cls, const WeaponType *weapon)
{
  _parClass = cls.GetClassInterface();
  OpticsInfo opticInfo;
  bool multipleOptics = cls.FindEntry("OpticsModes");
 
  _displayName = cls >> "displayName";
  _magazineReloadTime = cls >> "magazineReloadTime";
  GetValue(_sound, cls >> "drySound");
  _soundContinuous = cls>>"soundContinuous";
  GetValue(_reloadSound, cls >> "reloadSound");
  GetValue(_reloadMagazineSound, cls >> "reloadMagazineSound");
  _soundDuration = _reloadSoundDuration = _reloadMagazineSoundDuration = 0;
  _bullets.Load(cls, "soundBullet");
  if (_reloadSound.name.GetLength() > 0)
    _reloadSoundDuration = GSoundsys ? GSoundsys->GetWaveDuration(_reloadSound.name) : 0;
  if (_reloadMagazineSound.name.GetLength() > 0)
    _reloadMagazineSoundDuration = GSoundsys ? GSoundsys->GetWaveDuration(_reloadMagazineSound.name) : 0;
  if (_sound.name.GetLength() > 0)
    _soundDuration = GSoundsys ? GSoundsys->GetWaveDuration(_sound.name) : 0;
  
  _aiDispersionCoefX = cls >> "aiDispersionCoefX";
  _aiDispersionCoefY = cls >> "aiDispersionCoefY";

  _canBeLocked = cls >> "canLock";
  _ballisticsComputer = cls >> "ballisticsComputer";
  _enableAttack = cls >> "enableAttack";
  _optics = cls >> "optics";
  _primary = cls >> "primary";
  _showSwitchAction = cls >> "showSwitchAction";
  if (_primary == 0)
    _showSwitchAction = true;
  _showEmpty = cls >> "showEmpty";
  _autoReload = cls >> "autoReload";
  _backgroundReload = cls >> "backgroundReload";
  _canAutoAim = cls >> "autoAimEnabled";
  _showAimCursorInternal = cls >> "showAimCursorInternal";
  //_ffMag = cls >> "ffMagnitude";
  //_ffFreq = cls >> "ffFrequency";
  _irDistance = cls >> "irDistance";

  _shotSpreadAngle = cls >> "fireSpreadAngle";

  if(!multipleOptics)
  {
    opticInfo._opticsZoomMin = cls >> "opticsZoomMin";
    opticInfo._opticsZoomMax = cls >> "opticsZoomMax";
    opticInfo._opticsZoomInit = cls >> "opticsZoomInit";
    opticInfo._distanceZoomMin = cls >> "distanceZoomMin";
    opticInfo._distanceZoomMax = cls >> "distanceZoomMax";

    if(cls.FindEntry("opticsDisplayName")) 
      opticInfo._opticsDisplayName = cls >> "opticsDisplayName";

    opticInfo._opticsDisablePeripherialVision = cls >> "opticsDisablePeripherialVision";
    opticInfo._opticsFlare = cls>>"opticsFlare";
    opticInfo._id = cls >> "opticsID";

    if(cls.FindEntry("discretefov"))
    {
      ParamEntryVal discreteFov = cls >> "discretefov";
      if(discreteFov.GetSize() > 0)
      {
        opticInfo._discreteFov.Realloc(discreteFov.GetSize());
        opticInfo._discreteFov.Resize(discreteFov.GetSize());
        for (int i=0; i<discreteFov.GetSize(); i++)
          opticInfo._discreteFov[i] = discreteFov[i];
      }
      opticInfo._discreteInitIndex = cls >> "discreteInitIndex";
    }
    if(cls.FindEntry("discreteDistance"))
    {
      ParamEntryVal discreteDist = cls >> "discreteDistance";
      if(discreteDist.GetSize() > 0)
      {
        opticInfo._discreteDistance.Realloc(discreteDist.GetSize());
        opticInfo._discreteDistance.Resize(discreteDist.GetSize());
        for (int i=0; i<discreteDist.GetSize(); i++)
          opticInfo._discreteDistance[i] = discreteDist[i];
      }
      opticInfo._discreteDistanceInitIndex = cls >> "discreteDistanceInitIndex";
    }
  }

  _forceOptics = (cls>>"forceOptics").GetInt();
  _useAsBinocular = cls>>"useAsBinocular";

#if _VBS3
  //weapons also use viewoptics
  if(cls.FindEntry("ViewOptics"))
  {
    _viewOptics.Load(cls >> "ViewOptics");
    _opticsZoomMin = _viewOptics._minFov;
    _opticsZoomMax = _viewOptics._maxFov;
    _opticsZoomInit = _viewOptics._initFov;
  }
#endif

  _muzzlePos = VZero;
  _muzzleEnd = VZero;
  _muzzleDir = VForward;
  //_cameraPos = VZero;
  //_cameraDir = VForward;
  _irLaserPos = VZero;
  _irLaserDir = VForward;
#if _VBS3
  _laserPos = VZero;
#endif

  _cartridgeOutPosIndex = -1;
  _cartridgeOutEndIndex = -1;

  int n = (cls >> "magazines").GetSize();
  _magazines.Resize(n);
  for (int i=0; i<n; i++)
  {
    RStringB magazine = (cls >> "magazines")[i];
/*
    if (stricmp(magazine, "this") == 0)
      _magazines[i] = MagazineTypes.New(GetName());
    else
*/
    _magazines[i] = MagazineTypes.New(magazine);
  }
  if (_magazines.Size() > 0)
    _typicalMagazine = _magazines[0];

  n = (cls >> "modes").GetSize();
  _modes.Resize(n);
  for (int i=0; i<n; i++)
  {
    _modes[i] = new WeaponModeType();
    RStringB mode = (cls >> "modes")[i];
    if (stricmp(mode, "this") == 0)
      _modes[i]->Init(cls);
    else
      _modes[i]->Init(cls >> mode);
  }

#if _VBS3 // MuzzleEvent
  ConstParamEntryPtr array = cls.FindEntry("EventHandlers");
  if (array)
  {
    for (int i=0; i<NMuzzleEvent; i++)
    {
      MuzzleEvent e = (MuzzleEvent)i;
      RString name = FindEnumName(e);
      ConstParamEntryPtr entry = array->FindEntry(name);
      if (entry)
      {
        RStringB expression = *entry;
        _eventHandlers[e] = expression;
      }
    }
  }
  _showTrajectory = cls.ReadValue("showtrajectory", false);
#endif

  ParamEntryPtr entry = cls.FindEntry("flashLight");
  if (entry)
  {
    _gunLightInfo = new FlashLigthInfo;
    
    if (!_gunLightInfo->Load(*entry.GetPointer()))
    {
      _gunLightInfo.Free();
    }
  }

  if(!multipleOptics)
  {
    ParamEntryVal cfgOpticsPars = Pars >> "CfgOpticsEffect";
    ParamEntryVal ppeEntry = cls >> "opticsPPEffects";
    if (ppeEntry.IsArray())
    {
      int size = ppeEntry.GetSize();
      opticInfo._ppEffectType.Realloc(size);
      opticInfo._ppEffectType.Resize(size);

      for (int i = 0; i < size; i++)
      {
        // effect type
        RString name = ppeEntry[i];
        if (cfgOpticsPars.FindEntry(name))
        {
          ParamEntryVal eEntry = cfgOpticsPars >> name;
          if (eEntry.IsClass())
            opticInfo._ppEffectType[i].Load(eEntry);
        }
      }
    }


    if (cls.FindEntry("visionMode"))
    {
      ParamEntryVal entry = cls >> "visionMode";
      bool tiWanted = false;

      if (entry.IsArray())
      {
        int count = entry.GetSize();
        opticInfo._visionModes.Realloc(count);
        opticInfo._visionModes.Resize(count);

        for (int i = 0; i < count; i++)
        {
          const IParamArrayValue &arVal = entry[i];

          RString effectName = arVal;
          VisionMode vMode = GetEnumValue<VisionMode>(cc_cast(effectName));

          if (vMode < 0 || vMode >= VMCount) vMode = VMNormal;

          opticInfo._visionModes[i] = vMode;
          if (vMode == VMTi) tiWanted = true;
        }

        if (tiWanted)
        {
          if (cls.FindEntry("thermalMode"))
          {
            entry = cls >> "thermalMode";

            int count = entry.GetSize();
            opticInfo._thermalModes.Realloc(count);
            opticInfo._thermalModes.Resize(count);

            for (int i = 0; i < count; i++)
            {
              const IParamArrayValue &arVal = entry[i];
              opticInfo._thermalModes[i] = arVal;
            }
          }
          else
          {
            RptF(Format("Warning: Thermal vision mode defined, but modes not. WeaponType: %s", 
              weapon ? cc_cast(weapon->GetName()) : ""));
          }
        }
      }
    }
    _opticsInfo.Add(opticInfo);
  }

  InitOpticModes(cls, weapon);

  _opticsInfo.Compact();

  _hasTiVision = false;

  // look for Ti vision
  for (int i = 0; i < _opticsInfo.Size(); i++)
  {
    if (_opticsInfo[i].HasVisionMode(VMTi))
    {
      _hasTiVision = true;
      break;
    }
  }
}

void MuzzleType::InitOpticModes(ParamEntryPar cls, const WeaponType *weapon)
{
  if(cls.FindEntry("OpticsModes"))
  {
    ParamEntryVal entry = cls >> "OpticsModes";
    for(int i=0; i<entry.GetEntryCount(); i++)
    {
      OpticsInfo opticInfo;
      ParamEntryVal param = entry.GetEntry(i);
      opticInfo._opticsZoomMin = param >> "opticsZoomMin";
      opticInfo._opticsZoomMax = param >> "opticsZoomMax";
      opticInfo._opticsZoomInit = param >> "opticsZoomInit";
      opticInfo._distanceZoomMin = param >> "distanceZoomMin";
      opticInfo._distanceZoomMax = param >> "distanceZoomMax";

      if(cls.FindEntry("opticsDisplayName")) 
        opticInfo._opticsDisplayName = cls >> "opticsDisplayName";

      opticInfo._opticsFlare = param >> "opticsFlare";
      opticInfo._opticsDisablePeripherialVision = param >> "opticsDisablePeripherialVision";
      opticInfo._id = param >> "opticsID";

      if(param.FindEntry("discretefov"))
      {
        ParamEntryVal discreteFov = param >> "discretefov";
        if(discreteFov.GetSize() > 0)
        {
          opticInfo._discreteFov.Realloc(discreteFov.GetSize());
          opticInfo._discreteFov.Resize(discreteFov.GetSize());
          for (int i=0; i<discreteFov.GetSize(); i++)
          {
            opticInfo._discreteFov[i] = discreteFov[i];

          }
        }
        opticInfo._discreteInitIndex = param >> "discreteInitIndex";
      }
      if(param.FindEntry("discreteDistance"))
      {
        ParamEntryVal discreteDist = param >> "discreteDistance";
        if(discreteDist.GetSize() > 0)
        {
          opticInfo._discreteDistance.Realloc(discreteDist.GetSize());
          opticInfo._discreteDistance.Resize(discreteDist.GetSize());
          for (int i=0; i<discreteDist.GetSize(); i++)
            opticInfo._discreteDistance[i] = discreteDist[i];
        }
        opticInfo._discreteDistanceInitIndex = param >> "discreteDistanceInitIndex";
      }

      ParamEntryVal cfgOpticsPars = Pars >> "CfgOpticsEffect";
      ParamEntryVal ppeEntry = param >> "opticsPPEffects";
      if (ppeEntry.IsArray())
      {
        int size = ppeEntry.GetSize();
        opticInfo._ppEffectType.Realloc(size);
        opticInfo._ppEffectType.Resize(size);

        for (int j = 0; j < size; j++)
        {
          // effect type
          RString name = ppeEntry[j];
          if (cfgOpticsPars.FindEntry(name))
          {
            ParamEntryVal eEntry = cfgOpticsPars >> name;
            if (eEntry.IsClass())
              opticInfo._ppEffectType[j].Load(eEntry);
          }
        }
      }

      if (param.FindEntry("visionMode"))
      {
        ParamEntryVal entry = param >> "visionMode";
        bool tiWanted = false;

        if (entry.IsArray())
        {
          int count = entry.GetSize();
          opticInfo._visionModes.Realloc(count);
          opticInfo._visionModes.Resize(count);

          for (int j = 0; j < count; j++)
          {
            const IParamArrayValue &arVal = entry[j];

            RString effectName = arVal;
            VisionMode vMode = GetEnumValue<VisionMode>(cc_cast(effectName));

            if (vMode < 0 || vMode >= VMCount) vMode = VMNormal;

            opticInfo._visionModes[j] = vMode;
            if (vMode == VMTi) tiWanted = true;
          }

          if (tiWanted)
          {
            if (param.FindEntry("thermalMode"))
            {
              entry = param >> "thermalMode";

              int count = entry.GetSize();
              opticInfo._thermalModes.Realloc(count);
              opticInfo._thermalModes.Resize(count);

              for (int j = 0; j < count; j++)
              {
                const IParamArrayValue &arVal = entry[j];
                opticInfo._thermalModes[j] = arVal;
              }
            }
            else
            {
              RptF(Format("Warning: Thermal vision mode defined, but modes not. WeaponType: %s", 
                weapon ? cc_cast(weapon->GetName()) : ""));
            }
          }
        }
      }

      _opticsInfo.Add(opticInfo);
    }
  }

}

#if _VBS3 //MuzzleEvent

DEFINE_ENUM(MuzzleEvent,ME,MUZZLE_EVENT_ENUM)

void MuzzleType::OnEvent(MuzzleEvent event, const GameValue &pars)
{
  // execute type based event handler
  RString handler = _eventHandlers[event];
  GameVarSpace local(false);
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&local);
  gstate->VarSetLocal("_this",pars,true);
  gstate->Execute(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  gstate->EndContext();
}

bool MuzzleType::IsEventHandler(MuzzleEvent event) const
{
  RString handler = _eventHandlers[event];
  if (handler.GetLength()>0) return true;
  return false;
}
#endif

void ViewPars::Load(ParamEntryPar cfg)
{
#define GET_PAR(x) _##x=cfg>>#x
  GET_PAR(initAngleY);GET_PAR(minAngleY);GET_PAR(maxAngleY);
  GET_PAR(initAngleX);GET_PAR(minAngleX);GET_PAR(maxAngleX);
  GET_PAR(initFov);GET_PAR(minFov);GET_PAR(maxFov);
#undef GET_PAR
  saturateMax(_maxFov,_initFov); // force: fov is always in valid range
  saturateMin(_minFov,_initFov);

  saturateMax(_minFov, 0.0001);
  saturateMax(_maxFov, 0.0001);

  if (cfg.FindEntry("directionStabilized")) _directionStabilized = cfg >> "directionStabilized";
  else _directionStabilized= false;

  if (cfg.FindEntry("opticsDisplayName")) _opticsDisplayName = cfg >> "opticsDisplayName";

#if 1
  if (cfg.FindEntry("visionMode"))
  {
    ParamEntryVal entry = cfg >> "visionMode";
    bool tiWanted = false;

    if (entry.IsArray())
    {
      int count = entry.GetSize();
      _visionModes.Realloc(count);
      _visionModes.Resize(count);

      for (int i = 0; i < count; i++)
      {
        const IParamArrayValue &arVal = entry[i];

        RString effectName = arVal;
        VisionMode vMode = GetEnumValue<VisionMode>(cc_cast(effectName));

        if (vMode < 0 || vMode >= VMCount) vMode = VMNormal;

        _visionModes[i] = vMode;
        if (vMode == VMTi) tiWanted = true;
      }

      if (tiWanted)
      {
        if (cfg.FindEntry("thermalMode"))
        {
          entry = cfg >> "thermalMode";

          int count = entry.GetSize();
          _thermalModes.Realloc(count);
          _thermalModes.Resize(count);

          for (int i = 0; i < count; i++)
          {
            const IParamArrayValue &arVal = entry[i];
            _thermalModes[i] = arVal;
          }
        }
        else
        {
          RptF(Format("Warning: Thermal vision mode defined, but thermal modes not. Optics: %s",
            _opticsDisplayName.GetLength() ? cc_cast(_opticsDisplayName) : ""));
        }
      }
    }
  }
#endif //add discrete zoom levels

#if _VBS3
  _initialized = true;
  if(cfg.FindEntry("discretefov"))
  {
    ParamEntryVal discreteFov = cfg >> "discretefov";
    if(discreteFov.GetSize() > 0)
    {
      _discreteFov.Realloc(discreteFov.GetSize());
      _discreteFov.Resize(discreteFov.GetSize());
      for (int i=0; i<discreteFov.GetSize(); i++)
      {
        _discreteFov[i] = discreteFov[i];
      }
    }
  }
  _hasNV = cfg.ReadValue("nightvision", true); //support Arma
  _hasDV = cfg.ReadValue("dayVision",true);

  if(cfg.FindEntry("timodes"))
  {
    ParamEntryVal thermalModes = cfg >> "timodes";
    if(thermalModes.GetSize() > 0)
    {
      _thermalModes.Realloc(thermalModes.GetSize());
      _thermalModes.Resize(thermalModes.GetSize());
      for (int i=0; i<thermalModes.GetSize(); i++)
      {
        _thermalModes[i] = int(thermalModes[i]);
      }
    }
  }
  if(cfg.FindEntry("OpticsModel"))
  {
    _opticsModel = cfg >> "OpticsModel";
    _opticsColor = GetPackedColor(cfg >> "OpticsColor");
  }
  _canSee = cfg.ReadValue("CanSee",0);

  if(!_hasNV && !_hasDV && !_thermalModes.Size())
    Fail(Format("No vision mode supported by optics: %s", cfg.GetName()));
#endif //_VBS3
}

void ViewPars::InitVirtual(CameraType camType, float &heading, float &dive, float &fov) const
{
  fov = _initFov;
  dive = _initAngleX*(H_PI/180);
  heading = _initAngleY*(H_PI/180);
}

#if _VBS3
int ViewPars::GetNextThermalMode(int oldThermalmode) const
{
  if(_thermalModes.Size() == 0)
    return -1;
  ++oldThermalmode;
  if(oldThermalmode >= _thermalModes.Size())
    return -1;
  return oldThermalmode;
}

void ViewPars::DiscretizeValue(float fov, float &prev, float &closest, float &next) const
{
  int n = _discreteFov.Size();
  if(n <= 0)
  {
    prev = closest = next = fov;
    return;
  }

  int closestIndex = n-1;
  for(int i= 0; i < n;++i)
  {
    if(fov < _discreteFov[i])
    {
      if(fov - _discreteFov[i-1] > 0.5 * (_discreteFov[i] - _discreteFov[i-1]))
        closestIndex = i;
      else
        closestIndex = i-1;
      break;
    }
  }
  closest = _discreteFov[closestIndex];
  prev = _discreteFov[closestIndex > 0 ? closestIndex-1 : 0];
  next = _discreteFov[closestIndex + 1 < n ? closestIndex+1 : closestIndex];
}
#endif //_VBS3

void ViewPars::LimitVirtual(CameraType camType, float &heading, float &dive, float &fov) const
{
  float minHeading = _minAngleY*(H_PI/180);
  float maxHeading = _maxAngleY*(H_PI/180);
  if( maxHeading-minHeading<H_PI*15/8 )
  {
    float initHeading = _initAngleY*(H_PI/180);
    minHeading = AngleDifference(minHeading,initHeading);
    maxHeading = AngleDifference(maxHeading,initHeading);
    heading = AngleDifference(heading,initHeading);

    saturate(heading,minHeading,maxHeading);

    heading += initHeading;
  }
  float minDive = _minAngleX*(H_PI/180);
  float maxDive = _maxAngleX*(H_PI/180);
  saturate(dive,minDive,maxDive);
  saturate(fov,_minFov,_maxFov);
}

inline void SelectNextVisionMode(VisionMode &mode, const AutoArray<int> &modes)
{
  // find current mode
  int currSelMode = 0;
  for (int i = 0; i < modes.Size(); i++)
  {
    if (modes[i] == mode) 
    {
      currSelMode = i;
      break;
    }
  }

  int nextMode = currSelMode + 1;  
  if (nextMode >= modes.Size()) nextMode = 0;

  Assert(nextMode >= 0 && nextMode < modes.Size());

  mode = (VisionMode)modes[nextMode];
}

inline bool SelectNextThermalMode(int &tiIndex, const AutoArray<int> &modes)
{
  // find current thermal mode
  int currSelMode = 0;
  for (int i = 0; i < modes.Size(); i++)
  {
    if (modes[i] == tiIndex) 
    {
      currSelMode = i;
      break;
    }
  }

  int nextMode = currSelMode + 1;  
  if (nextMode >= modes.Size()) 
  {
    tiIndex = (modes.Size()>0)?modes[0]:-1;
    return false;
  }

  Assert(nextMode >= 0 && nextMode < modes.Size());
  tiIndex = modes[nextMode];

  return true;
}

void ViewPars::NextVisionMode(VisionMode& mode, int &tiModeIndex) const
{
  if (_visionModes.Size() > 0)
  {
    // iterate through thermal modes
    if (!(mode == VMTi && SelectNextThermalMode(tiModeIndex, _thermalModes)))
    {
      // select next vision mode
      SelectNextVisionMode(mode, _visionModes);
    }
  }
}

void ViewPars::GetInitVisionMode(VisionMode& mode, int &tiModeIndex) const
{
  mode = VMNormal;
  tiModeIndex = -1;
  if (_visionModes.Size() > 0) mode = (VisionMode)_visionModes[0];
  if (_thermalModes.Size() > 0) tiModeIndex = _thermalModes[0];
}

void OpticsInfo::NextVisionMode(VisionMode& mode, int &tiModeIndex) const
{
  if (_visionModes.Size() > 0)
  {
    // iterate through thermal modes
    if (!(mode == VMTi && SelectNextThermalMode(tiModeIndex, _thermalModes)))
    {
      // select next vision mode
      SelectNextVisionMode(mode, _visionModes);
    }
  }
}

bool OpticsInfo::HasVisionMode(VisionMode mode) const
{
  for (int i = 0; i < _visionModes.Size(); i++)
  {
    if (_visionModes[i] == mode) { return true; }
  }

  return false;
}

void OpticsInfo::GetInitVisionMode(VisionMode& mode, int &tiModeIndex) const
{
  mode = VMNormal;
  tiModeIndex = -1;
  if (_visionModes.Size() > 0) mode = (VisionMode)_visionModes[0];
  if (_thermalModes.Size() > 0) tiModeIndex = _thermalModes[0];
}

void MuzzleType::NextVisionMode(VisionMode& mode, int &tiModeIndex, int opticsMode ) const
{
  _opticsInfo[opticsMode].NextVisionMode(mode, tiModeIndex);
}

void MuzzleType::GetInitVisionMode(VisionMode& mode, int &tiModeIndex, int opticsMode) const
{
  _opticsInfo[opticsMode].GetInitVisionMode(mode, tiModeIndex);
}

static const EnumName VisionModeNames[]=
{
  EnumName(VMNormal, "Normal"),
  EnumName(VMNvg, "NVG"),
  EnumName(VMTi, "Ti"),
  EnumName()
};

template<>
const EnumName *GetEnumNames(VisionMode dummy)
{
  return VisionModeNames;
}

static void InitCursorTexture(CursorTextureInfo &info, RString name)
{
  ConstParamEntryPtr entry = (Pars >> "CfgWeaponCursors").FindEntry(name);
  if (!entry)
  {
    info.texture = GlobLoadTexture(GetPictureName(name));
    info.fade = 1.0f;
    info.sections.Clear();
    return;
  }

  RStringB cursorName = (*entry) >> "texture";
  info.texture = GlobLoadTexture(GetPictureName(cursorName));
  info.fade = (*entry) >> "fade";
  ConstParamEntryPtr val = entry->FindEntry("color");
  if (val) info.color = GetPackedColor(*val);
  val = entry->FindEntry("shadowEnabled");
  if (val) info.shadow = *val;

  ParamEntryVal sections = (*entry) >> "Sections";
  int n = sections.GetEntryCount();
  info.sections.Realloc(n);
  info.sections.Resize(n);
  for (int i=0; i<n; i++)
  {
    ParamEntryVal src = sections.GetEntry(i);
    CursorTextureSection &dst = info.sections[i];
    dst.uMin = src >> "uMin";
    dst.vMin = src >> "vMin";
    dst.uMax = src >> "uMax";
    dst.vMax = src >> "vMax";
    dst.xOffset = src >> "xOffset";
    dst.yOffset = src >> "yOffset";
  }
}

/*!
\patch 5153 Date 4/12/2007 by Jirka
- New: Weapons - optics camera can have a different direction than muzzle direction now
*/

void MuzzleType::InitShape(const WeaponType *weapon)
{
  const ParamEntry &cls = *_parClass;

  bool multipleOptics = cls.FindEntry("OpticsModes");
  _opticsEnable = false;
  
  if(!multipleOptics && _opticsInfo.Size()>0)
  {
    if(cls.FindEntry("useModelOptics")) _opticsInfo[0]._useModelOptics = cls>>"useModelOptics";
    else _opticsInfo[0]._useModelOptics = true;
  }

  RStringB oModelName = cls>>"modelOptics";
#if _VBS3 //opticsmodel supported in viewOptics class
  if(_viewOptics._opticsModel.GetLength() > 0) oModelName = _viewOptics._opticsModel;
#endif

  if (oModelName.GetLength()>0)
  {
    static RStringB noModel("-");
    if (oModelName!=noModel)
    {
      _opticsModel = Shapes.New(GetShapeName(oModelName),true,false);
      _opticsEnable = true;
    }
    else
      _opticsEnable = true;
  }
  if (_opticsModel)
    _opticsModel->AddLoadHandler(this);

  RStringB cursorName = cls >> "cursor";
  if (cursorName.GetLength() > 0)
    InitCursorTexture(_cursorTexture, cursorName);

  cursorName = cls >> "cursorAim";
  if (cursorName.GetLength()>0)
    InitCursorTexture(_cursorAimTexture, cursorName);

  _cursorSize = cls>>"cursorSize";
  
  _muzzlePos = VZero;
  _muzzleEnd = VZero;
  _muzzleDir = VForward;
#if _VBS3
  _laserPos = VZero;
#endif

  _cartridgeOutPosIndex = -1;
  _cartridgeOutEndIndex = -1;

  LODShapeWithShadow *shape = weapon->_model ? weapon->_model->GetShape() : NULL;

  if (shape)
  {
    // get weapon positions
    const Shape *mem = shape->MemoryLevel();
    if (mem)
    {
      {
        RString pos = cls >> "muzzlePos";
        RString end = cls >> "muzzleEnd";
        Vector3Val vPos = shape->NamedPoint(shape->FindMemoryLevel(),pos);
        Vector3Val vEnd = shape->NamedPoint(shape->FindMemoryLevel(),end);
#if _VBS3
        if(cls.FindEntry("laserPos"))
        {
          RString lpPos = cls >> "laserPos";
          _laserPos = shape->NamedPoint(shape->FindMemoryLevel(),lpPos);
        }
#endif
        
        _muzzlePos = vPos;
        _muzzleEnd = vEnd;
        _muzzleDir = (vPos - vEnd).Normalized();
        if (_muzzleDir.SquareSize()<Square(0.1))
        {
          if (pos.GetLength()>0 && end.GetLength()>0)
            RptF("Bad muzzle direction in %s:'%s'-'%s'",cc_cast(GetName()),cc_cast(pos),cc_cast(end));
          _muzzleDir = VForward;
        }

        ConstParamEntryPtr entryIrPos = cls.FindEntry("irLaserPos");
        ConstParamEntryPtr entryIrEnd = cls.FindEntry("irLaserEnd");

        if (entryIrPos && entryIrEnd)
        {
          RString posIr = *entryIrPos;
          RString endIr = *entryIrEnd;

          if (shape->FindNamedSel(mem, posIr) >= 0 && shape->FindNamedSel(mem, endIr) >= 0)
          {
            Vector3Val vPos = shape->NamedPoint(shape->FindMemoryLevel(),posIr);
            Vector3Val vEnd = shape->NamedPoint(shape->FindMemoryLevel(),endIr);

            _irLaserPos = vPos;
            _irLaserDir = (vPos - vEnd).Normalized();

            if (_irLaserDir.SquareSize()<Square(0.1))
            {
              if (pos.GetLength()>0 && end.GetLength()>0)
                RptF("Bad ir laser direction in %s:'%s'-'%s'",cc_cast(GetName()),cc_cast(pos),cc_cast(end));
              _irLaserDir = VForward;
            }
          }
        }

        if(!multipleOptics && _opticsInfo.Size()>0)
        {
          RString nameCameraPos = cls >> "memoryPointCamera";
          if (shape->FindNamedSel(mem,nameCameraPos) >= 0)
            _opticsInfo[0]._cameraPos = shape->NamedPoint(shape->FindMemoryLevel(),nameCameraPos);
          else
          {
            // temporary work-around - camera position not in model
            //LogF("Camera position missing in weapon %s",cc_cast(weapon->_model->GetName()));
            _opticsInfo[0]._cameraPos = Vector3(0.193,0.166,0) - shape->BoundingCenter();
          }

          _opticsInfo[0]._cameraDir = _muzzleDir;

          ConstParamEntryPtr entry = cls.FindEntry("cameraDir");
          if (entry)
          {
            RString nameCameraDir = *entry;
            if (shape->FindNamedSel(mem, nameCameraDir) >= 0)
            {
              _opticsInfo[0]._cameraDir = _opticsInfo[0]._cameraPos - shape->NamedPoint(shape->FindMemoryLevel(), nameCameraDir);
              _opticsInfo[0]._cameraDir.Normalize();
              if (_opticsInfo[0]._cameraDir.SquareSize() < Square(0.1))
              {
                RptF("Bad camera direction in %s:'%s'-'%s'", cc_cast(GetName()), cc_cast(nameCameraPos), cc_cast(nameCameraDir));
                _opticsInfo[0]._cameraDir = _muzzleDir;
              }
            }
          }
          _opticsInfo[0]._neutralXRot = atan2(_opticsInfo[0]._cameraDir.Y(), _opticsInfo[0]._cameraDir.SizeXZ());
          // TODO: 
          _opticsInfo[0]._neutralYRot = 0;
        }
      }
      {
        RString pos = cls >> "cartridgePos";
        RString end = cls >> "cartridgeVel";

        // point indices
        _cartridgeOutPosIndex = shape->PointIndex(mem,pos);
        _cartridgeOutEndIndex = shape->PointIndex(mem,end);

        _cartridgeOutPos = shape->NamedPoint(shape->FindMemoryLevel(),pos);
        Vector3 vEnd = shape->NamedPoint(shape->FindMemoryLevel(),end);
        _cartridgeOutVel = (vEnd - _cartridgeOutPos)*50;
      }

      if (_gunLightInfo)
      {
        _gunLightInfo->Init(shape);
      }
    }
  }

  InitShapeOpticsShape(weapon);
}

void MuzzleType::InitShapeOpticsShape(const WeaponType *weapon)
{
  const ParamEntry &cls = *_parClass;

  RString pos = cls >> "muzzlePos";
  RString end = cls >> "muzzleEnd";

  if(cls.FindEntry("OpticsModes"))
  {
    ParamEntryVal entry = cls >> "OpticsModes";
    for(int i=0; i<entry.GetEntryCount(); i++)
    {
      if(i >= _opticsInfo.Size()) break;
      OpticsInfo *opticInfo = &_opticsInfo[i];
      ParamEntryVal param = entry.GetEntry(i);

      opticInfo->_useModelOptics = param>>"useModelOptics";

      LODShapeWithShadow *shape = weapon->_model ? weapon->_model->GetShape() : NULL;
      if (shape)
      {
        // get weapon positions
        const Shape *mem = shape->MemoryLevel();
        if (mem)
        {
          {
            Vector3Val vPos = shape->NamedPoint(shape->FindMemoryLevel(),pos);
            Vector3Val vEnd = shape->NamedPoint(shape->FindMemoryLevel(),end);

            _muzzlePos = vPos;
            _muzzleEnd = vEnd;
            _muzzleDir = (vPos - vEnd).Normalized();
            if (_muzzleDir.SquareSize()<Square(0.1))
            {
              if (pos.GetLength()>0 && end.GetLength()>0)
                RptF("Bad muzzle direction in %s:'%s'-'%s'",cc_cast(GetName()),cc_cast(pos),cc_cast(end));
              _muzzleDir = VForward;
            }

            {
              RString nameCameraPos = param >> "memoryPointCamera";
              if (shape->FindNamedSel(mem,nameCameraPos) >= 0)
                opticInfo->_cameraPos = shape->NamedPoint(shape->FindMemoryLevel(),nameCameraPos);
              else
              {
                // temporary work-around - camera position not in model
                //LogF("Camera position missing in weapon %s",cc_cast(weapon->_model->GetName()));
                opticInfo->_cameraPos = Vector3(0.193,0.166,0) - shape->BoundingCenter();
              }

              opticInfo->_cameraDir = _muzzleDir;

              ConstParamEntryPtr entry = param.FindEntry("cameraDir");
              if (entry)
              {
                RString nameCameraDir = *entry;
                if (shape->FindNamedSel(mem, nameCameraDir) >= 0)
                {
                  opticInfo->_cameraDir = opticInfo->_cameraPos - shape->NamedPoint(shape->FindMemoryLevel(), nameCameraDir);
                  opticInfo->_cameraDir.Normalize();
                  if (opticInfo->_cameraDir.SquareSize() < Square(0.1))
                  {
                    RptF("Bad camera direction in %s:'%s'-'%s'", cc_cast(GetName()), cc_cast(nameCameraPos), cc_cast(nameCameraDir));
                    opticInfo->_cameraDir = _muzzleDir;
                  }
                }
              }
              opticInfo->_neutralXRot = atan2(opticInfo->_cameraDir.Y(), opticInfo->_cameraDir.SizeXZ());
              // TODO: 
              opticInfo->_neutralYRot = 0;
            }
          }
        }
      }
    }
  }
}

void MuzzleType::LODShapeLoaded(LODShape *shape)
{
  if (shape==_opticsModel)
  {
    if (_opticsModel->NLevels() > 0)
    {
      _opticsModel->Level(0)->MakeCockpit();
      _opticsModel->OrSpecial(BestMipmap);
    }
    const ParamEntry &par = *_parClass;
    _animFire.Init(_opticsModel, (par >> "selectionFireAnim").operator RString(), NULL);
  }
}

void MuzzleType::LODShapeUnloaded(LODShape *shape)
{
  if (shape==_opticsModel)
    _animFire.Deinit(_opticsModel);
}

void MuzzleType::ShapeLoaded(LODShape *shape, int level)
{
    if (shape==_opticsModel)
      _animFire.InitLevel(_opticsModel, level);
}

void MuzzleType::ShapeUnloaded(LODShape *shape, int level)
{
    if (shape==_opticsModel)
      _animFire.DeinitLevel(_opticsModel, level);
}

void MuzzleType::DeinitShape(const WeaponType *weapon)
{
  if (_opticsModel)
  {
    _opticsModel->RemoveLoadHandler(this);
    _opticsModel.Free();
  }

  _cursorTexture.texture.Free();
  _cursorTexture.sections.Clear();
  _cursorAimTexture.texture.Free();
  _cursorAimTexture.sections.Clear();
}

void MuzzleType::InitShapeLevel(const WeaponType *weapon, int level)
{
  //_animFire.InitLevel(_opticsModel,level);
}

void MuzzleType::DeinitShapeLevel(const WeaponType *weapon, int level)
{
  //_animFire.DeinitLevel(_opticsModel,level);
}

bool MuzzleType::CanUse(const MagazineType *type) const
{
  for (int i=0; i<_magazines.Size(); i++)
  {
    if (_magazines[i] == type)
      return true;
  }
  return false;
}

void MuzzleTypeLaser::InitShape(const WeaponType *weapon)
{
  base::InitShape(weapon);

  const ParamEntry &cls = *_parClass;
  RStringB cursorName = cls >> "cursorAimOn";
  if (cursorName.GetLength()>0)
    InitCursorTexture(_cursorAimOnTexture, cursorName);
}

void MuzzleTypeLaser::DeinitShape(const WeaponType *weapon)
{
  base::DeinitShape(weapon);
  _cursorAimOnTexture.texture.Free();
  _cursorAimOnTexture.sections.Clear();
}

const CursorTextureInfo *MuzzleTypeLaser::GetCursorAimTexture(bool laserOn) const
{
  return laserOn ? &_cursorAimOnTexture : &_cursorAimTexture;
}


DEFINE_ENUM(WeaponSimulation, WS, WEAPON_SIMULATION_ENUM)

// class WeaponType
WeaponType::WeaponType()
{
  _shapeRef = 0;
  _shotFromTurret = false;
  _loadedFromCfg = false;
  _weaponLockDelay = 0;
}

#include <Es/Common/delegate.hpp>

AnimationSource *WeaponType::CreateAnimationSource(const AnimationType *type, RStringB source)
{
  if (!strcmpi(source, "revolving"))
    return _animSources.CreateAnimationSource(&MagazineSlot::GetRevolvingPos);
  if (!strcmpi(source, "reload"))
    return _animSources.CreateAnimationSource(&MagazineSlot::GetReloadPos);
  if (!strcmpi(source, "reloadmagazine"))
    return _animSources.CreateAnimationSource(&MagazineSlot::GetMagazineReloadPos);
  if (!strcmpi(source, "isempty"))
    return _animSources.CreateAnimationSource(&MagazineSlot::IsEmpty);
#if _VBS2
  if (!strcmpi(source, "ammo"))
    return _animSources.CreateAnimationSource(&MagazineSlot::GetAmmo);
#endif
  AnimationSource *animSource = AnimatedType::CreateAnimationSource(type, source);
  if (animSource) return animSource;

  RptF
  (
    "%s - unknown animation source %s",
    cc_cast(type->GetName()), cc_cast(source)
  );
  return AnimatedType::CreateAnimationSource(type, "time");
}

void WeaponType::InitShape()
{
  //LogF("Init weapon %s",(const char *)GetName());

  LODShapeWithShadow *shape = _model ? _model->GetShape() : NULL;
  if (shape)
  {
    //LogF("Loading weapon model %s - %s",(const char *)GetName(),(const char *)wModelName);
    //bool shadow = true;
    // TODO: use shadow member of CfgNonAIVehicles::ProxyWeapon
    //(*_parClass)>>"shadow";
    shape->SetAutoCenter(false);
    // do not change minmax and bounding radius, there are loaded and not enough info is available now
    shape->CalculateBoundingSphere(false);
    const ParamEntry &par = *_parClass;
    _animFire.Init(shape, (par >> "selectionFireAnim").operator RString(), NULL);

    if (shape->GetSkeletonSourceName().GetLength() == 0)
    {
      shape->SetAnimationType(AnimTypeNone);
    }
    else
    {
      shape->SetAnimationType(AnimTypeHardware);
      shape->LoadSkeletonFromSource();
    }

    ConstParamEntryPtr array = _parClass->FindEntry("Animations");
    if (array)
    {
      RptF("Obsolete class Animations defined in %s",cc_cast(GetName()));
    }
    _animSources.Load(this, GetAnimations());

    if (!_loadedFromCfg)
    {
      //try to read thermal properties to override stored values in model.cfg
      float htMin = _parClass->ReadValue("htMin", shape->_htMin);
      float htMax = _parClass->ReadValue("htMax", shape->_htMax);
      float afMax = _parClass->ReadValue("afMax", shape->_afMax);
      float mfMax = _parClass->ReadValue("mfMax", shape->_mfMax);
      float mFact = _parClass->ReadValue("mFact", shape->_mFact);
      float tBody = _parClass->ReadValue("tBody", shape->_tBody);
      if ((htMin != shape->_htMin) ||
        (htMax != shape->_htMax) ||
        (afMax != shape->_afMax) ||
        (mfMax != shape->_mfMax) ||
        (mFact != shape->_mFact) ||
        (tBody != shape->_tBody))
      {
        shape->_htMin = htMin;
        shape->_htMax = htMax;
        shape->_afMax = afMax;
        shape->_mfMax = mfMax;
        shape->_mFact = mFact;
        shape->_tBody = tBody;
        LogF("[m] Overwrite TI Parameters for model: %s", cc_cast(shape->GetName()));
      }
    }

  }
  for (int j=0; j<_muzzles.Size(); j++)
  {
    MuzzleType *muzzle = _muzzles[j];
    muzzle->InitShape(this);
  }

#if _ENABLE_HAND_IK
  // read animation of weapon holding
  ParamEntryPtr entryTmp = _parClass->FindEntry("handAnim");
  if (entryTmp)
  {
    DoAssert(entryTmp->IsArray());
    int n = entryTmp->GetSize() / 2;
    for(int i = 0; i < n; i++)
    {
      RString skeletonName = (*entryTmp)[2*i];
      AnimationRTName name;
      name.name = GetAnimationName((*entryTmp)[2*i+1]);
      name.skeleton = Skeletons.New(skeletonName);
      name.reversed = false;
      name.streamingEnabled = false;

      Ref<AnimationRT> anim = AnimationRTBank.New(name);
      // we want all hand animations to be loaded all the time
      // streaming them in and out would be too complicated, and they are short anyway
      anim->AddPreloadCount();
      _handAnim.Add(anim);
    }
  } 
#endif 

}

void WeaponType::DeinitShape()
{
#if _ENABLE_HAND_IK
  for (int i=0; i<_handAnim.Size(); i++)
  {
    _handAnim[i]->ReleasePreloadCount();
  }
  _handAnim.Clear();
#endif
  
  for (int j=0; j<_muzzles.Size(); j++)
  {
    MuzzleType *muzzle = _muzzles[j];
    muzzle->DeinitShape(this);
  }
  //LogF("Deinit weapon %s",(const char *)GetName());
  LODShapeWithShadow *shape = _model ? _model->GetShape() : NULL;
  if (shape)
  {
    _animFire.Deinit(shape);

    _animSources.Clear();
  }
}

void WeaponType::InitShapeLevel(int level)
{
  LODShapeWithShadow *shape = _model ? _model->GetShape() : NULL;
  if (shape)
  {
    _animFire.InitLevel(shape, level);
  }
  for (int j=0; j<_muzzles.Size(); j++)
  {
    MuzzleType *muzzle = _muzzles[j];
    muzzle->InitShapeLevel(this,level);
  }
}
void WeaponType::DeinitShapeLevel(int level)
{
  for (int j=0; j<_muzzles.Size(); j++)
  {
    MuzzleType *muzzle = _muzzles[j];
    muzzle->DeinitShapeLevel(this,level);
  }
  LODShapeWithShadow *shape = _model ? _model->GetShape() : NULL;
  if (shape)
  {
    _animFire.DeinitLevel(shape, level);
  }
}

void WeaponType::LODShapeLoaded(LODShape *shape)
{
  Assert(_model);
  InitShape();
}
void WeaponType::LODShapeUnloaded(LODShape *shape)
{
  Assert(_model);
  DeinitShape();
}
void WeaponType::ShapeLoaded(LODShape *shape, int level)
{
  Assert(_model);
  InitShapeLevel(level);
}
void WeaponType::ShapeUnloaded(LODShape *shape, int level)
{
  Assert(_model);
  DeinitShapeLevel(level);
}

void WeaponType::ShapeAddRef()
{
  if (_shapeRef++ == 0)
  {
    if (_modelName.GetLength()>0)
    {
      Ref<LODShapeWithShadow> lodShape = Shapes.New(GetShapeName(_modelName),ShapeShadow);
      Ref<EntityPlainType> type = new EntityPlainType(lodShape);

      _model = new WeaponObject(lodShape, type, VISITOR_NO_ID);
    }
    else
    {
      _model = NULL;
    }
    if (_model)
    {
      _model->GetShape()->AddLoadHandler(unconst_cast(this));
    }
    else
    {
      InitShape();
    }
  }
}

void WeaponType::ShapeRelease()
{
  if (--_shapeRef==0)
  {
    if (_model)
    {
      _model->GetShape()->RemoveLoadHandler(unconst_cast(this));
    }
    else
    {
      DeinitShape();
    }
    _model.Free();
  }
  Assert(_shapeRef>=0);
}

bool WeaponType::PrepareShapeDrawMatrices(Matrix4Array &matrices, int level, const ObjectVisualState &vs, const MagazineSlot *magazineSlot) const
{
  if (!_model) return false;

  _model->GetShape()->InitDrawMatrices(matrices, level);
  return _animSources.PrepareShapeDrawMatrices(matrices, _model->GetShape(), level, vs, magazineSlot, GetAnimations());
}

RStringB WeaponType::GetPictureName() const
{
  return _picName.GetLength()>0 ? _picName : GetName();
}

bool FlashLigthInfo::Load(ParamEntryPar cls)
{
  if (cls.GetEntryCount() != 7) return false;

  GetValue(_color, cls >> "color");
  GetValue(_colorAmbient, cls >> "ambient");  
  _positionMem = cls >> "position";
  _directionMem = cls >> "direction";
  _angle = cls >> "angle";
  GetValue(_scale, cls >> "scale");
  _brightness = cls >> "brightness";

  return true;
}

void FlashLigthInfo::Init(const LODShapeWithShadow* shape)
{
  if (shape)
  {
    int mem = shape->FindMemoryLevel();

    _position = shape->NamedPoint(mem, _positionMem);
    Vector3 direction = shape->NamedPoint(mem, _directionMem);
    _direction = _position - direction;
    _direction.Normalize();
  }
}

/*!
\patch_internal 1.30 Date 11/02/2001 by Jirka
- Added: parameter "shotFromTurret"
*/
void WeaponType::Init(const char *name)
{
  // ERROR_CATCH_ANY can be executed multiple times, but we want only one error message
  bool errorOnce = false;
  ERROR_TRY()
  
  ParamEntryVal cls = Pars >> "CfgWeapons" >> name;

  _parClass = cls.GetClassInterface();

  _scope = cls >> "scope";

  if (_scope == 0)
  {
    WarningMessage("Error: creating weapon %s with scope=private", name);
  }

  _displayName = cls >> "displayName";
  _nameSound = cls >> "nameSound";
  _weaponType = cls >> "type";
  _picName = cls>>"picture";
  _description = cls >> "Library" >> "libTextDesc";
  _modelName = cls>>"model";

  RStringB simName = cls >> "simulation";
  _simulation = GetEnumValue<WeaponSimulation>(simName);

  _fireLightDuration = cls >> "fireLightDuration";
  _fireLightIntensity = cls >> "fireLightIntensity";

  _weaponLockDelay = cls >> "weaponLockDelay";
  _weaponLockSystem = cls >> "weaponLockSystem";
  _cmImmunity  = cls >> "cmImmunity";

  GetValue(_lockingTargetSound, cls>>"lockingTargetSound");
  GetValue(_lockedTargetSound, cls>>"lockedTargetSound");

  if(cls.FindEntry("weaponInfoType")) 
  {
    ParamEntryVal types = cls >> "weaponInfoType";
    if(types.IsArray())
    {
      int n = types.GetSize();
      _weaponInfoTypes.Realloc(n);
      _weaponInfoTypes.Resize(n);
      for (int i=0; i<n; i++) _weaponInfoTypes[i] = types[i];
    }

    else
    {
      _weaponInfoTypes.Realloc(1);
      _weaponInfoTypes.Resize(1);
      _weaponInfoTypes[0] = types;
    }
  }

#if _VBS2 // fatigue model
  _mass = cls.ReadValue("weight",4.0f);
  _lightMode = (int)cls.ReadValue("lightmode",0.0f);
#endif

  ConstParamEntryPtr entry = cls.FindEntry("shotFromTurret");
  if (entry) _shotFromTurret = *entry;
  else _shotFromTurret = false;

  RString picture;
  entry = cls.FindEntry("uiPicture");
  if (entry) picture = *entry;
  if (picture.GetLength() > 0) _picture = GlobLoadTexture(::GetPictureName(picture));

#if _ENABLE_DATADISC
  entry = cls.FindEntry("canDrop");
  if (entry) _canDrop = *entry;
  else _canDrop = true;
#else
  _canDrop = false;
#endif

  entry = cls.FindEntry("dexterity");
  if (entry) _dexterity = *entry;
  else _dexterity = 1;

  entry = cls.FindEntry("value");
  if (entry) _value = *entry;
  else _value = 1;

  entry = cls.FindEntry("htMin");
  _htMin = entry ? *entry : 0.0f;

  if (entry) _loadedFromCfg = true;

  entry = cls.FindEntry("htMax");
  _htMax = entry ? *entry : 0.0f;

  entry = cls.FindEntry("afMax");
  _afMax = entry ? *entry : 0.0f;

  entry = cls.FindEntry("mfMax");
  _mfMax = entry ? *entry : 0.0f;

  entry = cls.FindEntry("mFact");
  _mFact = entry ? *entry : 0.0f;

  entry = cls.FindEntry("tBody");
  _tBody = entry ? *entry : 0.0f;

  //ShapeAddRef(); // TODO: move ShapeAddRef to some more reasonable place
  int n = (cls >> "muzzles").GetSize();
  _muzzles.Resize(n);
  for (int i=0; i<n; i++)
  {
    RStringB muzzle = (cls >> "muzzles")[i];
    ParamEntryVal muzzleCls = stricmp(muzzle, "this") == 0 ? cls : (cls >> muzzle);
    bool laser = false;
    ConstParamEntryPtr entry = muzzleCls.FindEntry("laser");
    if (entry) laser = *entry;
    _muzzles[i] = laser ? new MuzzleTypeLaser() : new MuzzleType();
    _muzzles[i]->Init(muzzleCls, this);
  }
  ERROR_CATCH_ANY(exc)
    if (!errorOnce)
    {
      errorOnce = true;
      LogF("Error loading WeaponType %s",cc_cast(cls.GetContext()));
    }
  ERROR_END()
}


bool WeaponType::IsMagazineUsableInWeapon(const MagazineType *type) const
{
  for (int j=0; j<_muzzles.Size(); j++)
  {
    const MuzzleType *muzzle = _muzzles[j];
    for (int k=0; k<muzzle->_magazines.Size(); k++)
      if (muzzle->_magazines[k] == type) return true;
  }
  return false;
}

#if _ENABLE_HAND_IK
/// find animation with such skeleton... 
Ref<AnimationRT> WeaponType::GetHandAnim(Skeleton * sk) const
{
  DoAssert(_shapeRef>0);
  for(int i = 0; i < _handAnim.Size(); i++)
  {
    if (_handAnim[i]->GetSkeleton() == sk)
      return _handAnim[i];
  }
  return NULL;
}
#endif

LSError WeaponType::Serialize(ParamArchive &ar)
{
  // note: used only for saving
  if (ar.IsSaving())
  {
    RString name = GetName();
    CHECK(ar.Serialize("name", name, 1));
  }
  return LSOK;
}

WeaponType *WeaponType::CreateObject(ParamArchive &ar)
{
  RString name;
  // get name
  if (ar.Serialize("name", name, 1) != LSOK)
  {
    return NULL;
  }
  if (name.GetLength() == 0) return NULL;
  return WeaponTypes.New(name);
}

DEFINE_FAST_ALLOCATOR(Magazine)

Magazine::Magazine(const MagazineType *type)
:_type(const_cast<MagazineType *>(type))
{
  SetAmmo(0);
  _reload = 0;
  _reloadDuration = 1;
  _reloadMagazine = 0;
  _reloadMagazineTotal = 0;
  _burstLeft = 0;

  _creator = GetNetworkManager().GetCreatorId();
  _id = GWorld->GetMagazineID();
  if (_type) _type->AmmoAddRef();
  //LogF("Magazine %d: %s",_id,(const char *)type->GetName());
}

Magazine::~Magazine()
{
  if (_type) _type->AmmoRelease();
}

Magazine *Magazine::CreateObject(ParamArchive &ar)
{
  RString name;
  if( ar.Serialize("type", name, 1)!=LSOK)
  {
    return NULL;
  }
  MagazineType *type = NULL;
  if (name.GetLength() > 0) type = MagazineTypes.New(name);
  // load type from archive
  return type ? new Magazine(type) : NULL;
};  

LSError Magazine::Serialize(ParamArchive &ar)
{
  if (ar.IsSaving())
  {
    RString name = _type ? _type->GetName() : "";
    CHECK(ar.Serialize("type", name, 1));
  }
  // type is loaded during CreateObject
  /*
  else if (ar.GetPass() == ParamArchive::PassFirst)
  {
    RString name;
    CHECK(ar.Serialize("type", name, 1));
    if (name.GetLength() > 0)
      _type = MagazineTypes.New(name);
    else
      _type = NULL;
  }
  */
  int ammo = GetAmmo();
  CHECK(ar.Serialize("ammo", ammo, 1, 0));
  SetAmmo(ammo);
  CHECK(ar.Serialize("burstLeft", _burstLeft, 1, 0));
  CHECK(ar.Serialize("reload", _reload, 1, 0));
  CHECK(ar.Serialize("reloadDuration", _reloadDuration, 1, 1));
  CHECK(ar.Serialize("reloadMagazine", _reloadMagazine, 1, 0));
  CHECK(ar.Serialize("reloadMagazineTotal", _reloadMagazineTotal, 1, 0));
  if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
  {
    // for SaveStatus / LoadStatus leave id unchanged (avoid collision with existing id)
    CHECK(_creator.Serialize(ar, "creator", 1, 0))
    CHECK(ar.Serialize("id", _id, 1, 0))
  }
  return LSOK;
}

DEFINE_NETWORK_OBJECT_SIMPLE(Magazine, Magazine)

DEFINE_NET_INDICES_INIT_MSG(Magazine, MAGAZINE_MSG)

NetworkMessageFormat &Magazine::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  MAGAZINE_MSG(Magazine, MSG_FORMAT)
  return format;
}

Magazine *Magazine::CreateObject(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(Magazine)

  RString name;
  if (TRANSF_BASE(type, name)!=TMOK)
  {
    return NULL;
  }
  MagazineType *type = NULL;
  if (name.GetLength() > 0) type = MagazineTypes.New(name);

  return type ? new Magazine(type) : NULL;
}

TMError Magazine::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(Magazine)

  if (ctx.IsSending())
  {
    RString name = _type ? _type->GetName() : "";
    TRANSF_EX(type, name)
  }
  // type is loaded in CreateObject
  /*
  else
  {
    RString name;
    TMCHECK(ctx.Transfer("type", name))
    if (name.GetLength() > 0)
      _type = MagazineTypes.New(name);
    else
      _type = NULL;
  }
  */
  int ammo = GetAmmo();
  TRANSF_EX(ammo, ammo)
  SetAmmo(ammo);
  TRANSF(burstLeft)
  TRANSF(reload)
  TRANSF(reloadDuration)
  TRANSF(reloadMagazine)
  TRANSF(reloadMagazineTotal)
  TRANSF(creator)
  TRANSF(id)
  return TMOK;
}

// returns instead of NULL when reference is expected

MagazineSlot::MagazineSlot()
{
  _reloadMagazineProgress = 0;
#if _VBS3
  //will be initialized on first ApplyVision
  _visionMode = VMInvalid;
  _tiModeIndex = 0;
#endif

  _opticsMode = 0;
  _discreteIndex = 0;
  _discreteDistanceIndex = 0;
}

void MagazineSlot::SelectOpticsMode()
{
  _opticsMode = 0;
}

LSError MagazineSlot::Serialize(ParamArchive &ar)
{   
  CHECK(ar.Serialize("discreteIndex", _discreteIndex, 1, 0))
  CHECK(ar.Serialize("discreteDistanceIndex", _discreteDistanceIndex, 1, 0))
  CHECK(ar.Serialize("opticsMode",_opticsMode,1,0))
  return LSOK;
}

bool MagazineSlot::IsDefaultValue(ParamArchive &ar) const
{
  return _discreteIndex==0 && _discreteDistanceIndex==0 && _opticsMode==0;
}
void MagazineSlot::LoadDefaultValues(ParamArchive &ar)
{
  _discreteIndex=0;
  _discreteDistanceIndex=0;
  _opticsMode=0;
}

bool MagazineSlot::ShowToPlayer() const
{
  if (!_weaponMode) return true;
  return _weaponMode->_showToPlayer;
}

float MagazineSlot::GetRevolvingPos(const ObjectVisualState &vs) const
{
  if (!_magazine) return 0;
  float reload = _magazine->_reload;
  saturate(reload, 0, 1);
  return (_magazine->GetAmmo() + reload) / _magazine->_type->_maxAmmo;
}

float MagazineSlot::GetReloadPos(const ObjectVisualState &vs) const
{
  if (!_magazine) return 0;
  float reload = _magazine->_reload;
  saturate(reload, 0, 1);
  return reload;
}

float MagazineSlot::IsEmpty(const ObjectVisualState &vs) const
{
  if (!_magazine) return 0;
  return _magazine->GetAmmo() > 0 ? 0.0f : 1.0f;
}

float MagazineSlot::GetMagazineReloadPos(const ObjectVisualState &vs) const
{
  if (!_magazine) return 0;
  float byTime = _magazine->_reloadMagazineTotal>0 ? _magazine->_reloadMagazine/_magazine->_reloadMagazineTotal : 0;
  float byAnim = _reloadMagazineProgress;
  // combine both ways of animation
  return floatMin(byTime+byAnim,1);
}

#if _VBS2
//returns the amount of ammunition
float MagazineSlot::GetAmmo() const
{
  if (!_magazine) return 0;
  return _magazine->GetAmmo();
}
#endif

bool EntityAIType::IsSupply() const
{
  if (GetMaxFuelCargo() > 0) return true;
  if (GetMaxRepairCargo() > 0) return true;
  if (GetMaxAmmoCargo() > 0) return true;
  if (GetMaxWeaponsCargo() > 0) return true;
  if (GetMaxMagazinesCargo() > 0) return true;
  if( IsAttendant() ) return true;
  if( IsEngineer() ) return true;
  for (int i=0; i<_weaponCargo.Size(); i++)
  {
    if (_weaponCargo[i].count > 0) return true;
  }
  for (int i=0; i<_magazineCargo.Size(); i++)
  {
    if (_magazineCargo[i].count > 0) return true;
  }
  return _forceSupply;
}

bool EntityAIType::CanStoreWeapons() const
{
  if (GetMaxWeaponsCargo() > 0) return true;
  if (GetMaxMagazinesCargo() > 0) return true;
  for (int i=0; i<_weaponCargo.Size(); i++)
  {
    if (_weaponCargo[i].count > 0) return true;
  }
  for (int i=0; i<_magazineCargo.Size(); i++)
  {
    if (_magazineCargo[i].count > 0) return true;
  }

  return false;
}

MagazineTypeBank MagazineTypes;
WeaponTypeBank WeaponTypes;
