#include "wpch.hpp"
#include "Network/networkImpl.hpp"

#include "arcadeTemplate.hpp"
#include "AI/ai.hpp"
#include <El/ParamFile/paramFile.hpp>
#include "paramArchiveExt.hpp"
#include "landscape.hpp"
#include "global.hpp"
#include <El/Common/randomGen.hpp>

#include "UI/resincl.hpp"
#include "UI/uiControls.hpp"
#include "world.hpp"
#include "fileLocator.hpp"

#include <El/Enum/enumNames.hpp>

#include "stringtableExt.hpp"
#include "UI/chat.hpp"


RString GetVehicleIcon(RString name);

static const EnumName TitleTypeNames[]=
{
  EnumName(TitleNone, "NONE", &IDS_TITTYPE_NONE),
  EnumName(TitleObject, "OBJECT", &IDS_TITTYPE_OBJECT),
  EnumName(TitleResource, "RES", &IDS_TITTYPE_RESOURCE),
  EnumName(TitleText, "TEXT", &IDS_TITTYPE_TEXT),
  EnumName()
};
template<>
const EnumName *GetEnumNames(TitleType dummy)
{
  return TitleTypeNames;
}

static const EnumName ArcadeWaypointTypeNames[]=
{
  EnumName(ACUNDEFINED, "UNDEF", &DUPLICATE_IDS),
  EnumName(ACMOVE, "MOVE", &IDS_AC_MOVE),
  EnumName(ACDESTROY, "DESTROY", &IDS_AC_DESTROY),
  EnumName(ACGETIN, "GETIN", &IDS_AC_GETIN),
  EnumName(ACSEEKANDDESTROY, "SAD", &IDS_AC_SEEKANDDESTROY),
  EnumName(ACJOIN, "JOIN", &IDS_AC_JOIN),
  EnumName(ACLEADER, "LEADER", &IDS_AC_LEADER),
  EnumName(ACGETOUT, "GETOUT", &IDS_AC_GETOUT),
  EnumName(ACCYCLE, "CYCLE", &IDS_AC_CYCLE),
  EnumName(ACLOAD, "LOAD", &IDS_AC_LOAD),
  EnumName(ACUNLOAD, "UNLOAD", &IDS_AC_UNLOAD),
  EnumName(ACTRANSPORTUNLOAD, "TR UNLOAD", &IDS_AC_TRANSPORTUNLOAD),
  EnumName(ACHOLD, "HOLD", &IDS_AC_HOLD),
  EnumName(ACSENTRY, "SENTRY", &IDS_AC_SENTRY),
  EnumName(ACGUARD, "GUARD", &IDS_AC_GUARD),
  EnumName(ACTALK, "TALK", &IDS_AC_TALK),
  EnumName(ACSCRIPTED, "SCRIPTED", &IDS_AC_SCRIPTED),
  EnumName(ACSUPPORT, "SUPPORT", &IDS_AC_SUPPORT),
  EnumName(ACGETINNEAREST, "GETIN NEAREST", &IDS_AC_GETINNEAREST),
  EnumName(ACDISMISS, "DISMISS", &IDS_AC_DISMISS),
  EnumName(ACAND, "AND", &IDS_AC_AND),
  EnumName(ACOR, "OR", &IDS_AC_OR),
  EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeWaypointType dummy)
{
  return ArcadeWaypointTypeNames;
}

static const EnumName SpeedModeNames[]=
{
  EnumName(SpeedUnchanged, "UNCHANGED", &IDS_SPEED_UNCHANGED),
  EnumName(SpeedLimited, "LIMITED", &IDS_SPEED_LIMITED),
  EnumName(SpeedNormal, "NORMAL", &IDS_SPEED_NORMAL),
  EnumName(SpeedFull, "FULL", &IDS_SPEED_FULL),
  EnumName()
};
template<>
const EnumName *GetEnumNames(SpeedMode dummy)
{
  return SpeedModeNames;
}

static const EnumName VehSpeedModeNames[]=
{
  EnumName(VehSpeedAuto, "AUTO"),
    EnumName(VehSpeedSlow, "SLOW"),
    EnumName(VehSpeedNormal, "NORMAL"),
    EnumName(VehSpeedFast, "FAST"),
    EnumName()
};
template<>
const EnumName *GetEnumNames(VehSpeedMode dummy)
{
  return VehSpeedModeNames;
}

static const EnumName CombatModeNames[]=
{
  EnumName(CMUnchanged, "UNCHANGED", &IDS_COMBAT_UNCHANGED),
  EnumName(CMCareless, "CARELESS", &IDS_COMBAT_CARELESS),
  EnumName(CMSafe, "SAFE", &IDS_COMBAT_SAFE),
  EnumName(CMAware, "AWARE", &IDS_COMBAT_AWARE),
  EnumName(CMCombat, "COMBAT", &IDS_COMBAT_COMBAT),
  EnumName(CMStealth, "STEALTH", &IDS_COMBAT_STEALTH),
  EnumName()
};
template<>
const EnumName *GetEnumNames(CombatMode dummy)
{
  return CombatModeNames;
}

static const EnumName AWPShowNames[]=
{
  EnumName(ShowNever, "NEVER", &IDS_DISP_ARCWP_SHOW_NEVER),
  EnumName(ShowEasy, "EASY", &IDS_DISP_ARCWP_SHOW_EASY),
  EnumName(ShowAlways, "ALWAYS", &IDS_DISP_ARCWP_SHOW_ALWAYS),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AWPShow dummy)
{
  return AWPShowNames;
}

static const EnumName ArcadeUnitSpecialNames[]=
{
  EnumName(ASpNone, "NONE", &IDS_SPECIAL_NONE),
  EnumName(ASpCargo, "CARGO", &IDS_SPECIAL_CARGO),
  EnumName(ASpFlying, "FLY", &IDS_SPECIAL_FLYING),
  EnumName(ASpForm, "FORM", &IDS_SPECIAL_FORM),
  EnumName(ASpCanCollide, "CAN_COLLIDE", &IDS_SPECIAL_CAN_COLLIDE),
  EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeUnitSpecial dummy)
{
  return ArcadeUnitSpecialNames;
}

static const EnumName ArcadeUnitAgeNames[]=
{
  EnumName(AAActual, "ACTUAL", &IDS_AGE_ACTUAL),
  EnumName(AA5Min, "5 MIN", &IDS_AGE_5MIN),
  EnumName(AA10Min, "10 MIN", &IDS_AGE_10MIN),
  EnumName(AA15Min, "15 MIN", &IDS_AGE_15MIN),
  EnumName(AA30Min, "30 MIN", &IDS_AGE_30MIN),
  EnumName(AA60Min, "60 MIN", &IDS_AGE_60MIN),
  EnumName(AA120Min, "120 MIN", &IDS_AGE_120MIN),
  EnumName(AAUnknown, "UNKNOWN", &IDS_AGE_UNKNOWN),
  EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeUnitAge dummy)
{
  return ArcadeUnitAgeNames;
}

static const EnumName ArcadeUnitPlayerNames[]=
{
  EnumName(APNonplayable, "NONPLAY"),
  EnumName(APPlayerCommander, "PLAYER COMMANDER"),
  EnumName(APPlayerDriver, "PLAYER DRIVER"),
  EnumName(APPlayerGunner, "PLAYER GUNNER"),
  EnumName(APPlayableC, "PLAY C"),
  EnumName(APPlayableD, "PLAY D"),
  EnumName(APPlayableG, "PLAY G"),
  EnumName(APPlayableCD, "PLAY CD"),
  EnumName(APPlayableCG, "PLAY CG"),
  EnumName(APPlayableDG, "PLAY DG"),
  EnumName(APPlayableCDG, "PLAY CDG"),
  // old versions
  EnumName(APPlayableCDG, "PLAY"),
  EnumName(APPlayerCommander, "P1 COMMANDER"),
  EnumName(APPlayerDriver, "P1 DRIVER"),
  EnumName(APPlayerGunner, "P1 GUNNER"),
  EnumName(APPlayerCommander, "P2 COMMANDER"),
  EnumName(APPlayerDriver, "P2 DRIVER"),
  EnumName(APPlayerGunner,"P2 GUNNER"),
  EnumName(APNonplayable, "-1"),
  EnumName(APPlayableCDG, "0"),
  EnumName(APPlayerCommander, "1"),
  EnumName(APPlayerCommander, "2"),
  EnumName(APNonplayable, "-1.000000"),
  EnumName(APPlayableCDG, "0.000000"),
  EnumName(APPlayerCommander, "1.000000"),
  EnumName(APPlayerCommander, "2.000000"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeUnitPlayer dummy)
{
  return ArcadeUnitPlayerNames;
}

static const EnumName LockStateNames[]=
{
  EnumName(LSUnlocked, "UNLOCKED", &IDS_VEHICLE_UNLOCKED),
  EnumName(LSDefault, "DEFAULT", &IDS_VEHICLE_DEFAULT),
  EnumName(LSLocked, "LOCKED", &IDS_VEHICLE_LOCKED),
  EnumName()
};
template<>
const EnumName *GetEnumNames(LockState dummy)
{
  return LockStateNames;
}

static const EnumName ArcadeSensorActivationNames[]=
{
  EnumName(ASANone, "NONE", &IDS_SENSORACTIV_NONE),
  EnumName(ASAEast, "EAST", &IDS_EAST),
  EnumName(ASAWest, "WEST", &IDS_WEST),
  EnumName(ASAGuerrila, "GUER", &IDS_GUERRILA),
  EnumName(ASACivilian, "CIV", &IDS_CIVILIAN),
  EnumName(ASALogic, "LOGIC", &IDS_LOGIC),
  EnumName(ASAAnybody, "ANY", &IDS_SENSORACTIV_ANYBODY),
  EnumName(ASAAlpha, "ALPHA", &IDS_SENSORACTIV_ALPHA),
  EnumName(ASABravo, "BRAVO", &IDS_SENSORACTIV_BRAVO),
  EnumName(ASACharlie, "CHARLIE", &IDS_SENSORACTIV_CHARLIE),
  EnumName(ASADelta, "DELTA", &IDS_SENSORACTIV_DELTA),
  EnumName(ASAEcho, "ECHO", &IDS_SENSORACTIV_ECHO),
  EnumName(ASAFoxtrot, "FOXTROT", &IDS_SENSORACTIV_FOXTROT),
  EnumName(ASAGolf, "GOLF", &IDS_SENSORACTIV_GOLF),
  EnumName(ASAHotel, "HOTEL", &IDS_SENSORACTIV_HOTEL),
  EnumName(ASAIndia, "INDIA", &IDS_SENSORACTIV_INDIA),
  EnumName(ASAJuliet, "JULIET", &IDS_SENSORACTIV_JULIET),
  EnumName(ASAEastSeized, "EAST SEIZED", &IDS_EAST_SEIZED),
  EnumName(ASAWestSeized, "WEST SEIZED", &IDS_WEST_SEIZED),
  EnumName(ASAGuerrilaSeized, "GUER SEIZED", &IDS_GUERRILA_SEIZED),
  EnumName(ASAStatic, "STATIC", &IDS_SENSORACTIV_STATIC),
  EnumName(ASAVehicle, "VEHICLE", &IDS_SENSORACTIV_VEHICLE),
  EnumName(ASAGroup, "GROUP", &IDS_SENSORACTIV_GROUP),
  EnumName(ASALeader, "LEADER", &IDS_SENSORACTIV_LEADER),
  EnumName(ASAMember, "MEMBER", &IDS_SENSORACTIV_MEMBER),
  EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeSensorActivation dummy)
{
  return ArcadeSensorActivationNames;
}

static const EnumName ArcadeSensorActivationTypeNames[]=
{
  EnumName(ASATPresent, "PRESENT", &IDS_DISP_ARCSENS_PRESYES),
  EnumName(ASATNotPresent, "NOT PRESENT", &IDS_DISP_ARCSENS_PRESNO),
  EnumName(ASATWestDetected, "WEST D", &IDS_DISP_ARCSENS_DETWEST),
  EnumName(ASATEastDetected, "EAST D", &IDS_DISP_ARCSENS_DETEAST),
  EnumName(ASATGuerrilaDetected, "GUER D", &IDS_DISP_ARCSENS_DETGUERRILA),
  EnumName(ASATCiviliansDetected, "CIV D", &IDS_DISP_ARCSENS_DETCIVILIAN),
  EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeSensorActivationType dummy)
{
  return ArcadeSensorActivationTypeNames;
}

static const EnumName ArcadeSensorTypeNames[]=
{
  EnumName(ASTNone, "NONE", &IDS_SENSORTYPE_NONE),
  EnumName(ASTEastGuarded, "EAST G", &IDS_SENSORTYPE_GUARD_EAST),
  EnumName(ASTWestGuarded, "WEST G", &IDS_SENSORTYPE_GUARD_WEST),
  EnumName(ASTGuerrilaGuarded, "GUER G", &IDS_SENSORTYPE_GUARD_GUERRILA),
  EnumName(ASTSwitch, "SWITCH", &IDS_SENSORTYPE_SWITCH),
  EnumName(ASTEnd1, "END1", &IDS_SENSORTYPE_END1),
  EnumName(ASTEnd2, "END2", &IDS_SENSORTYPE_END2),
  EnumName(ASTEnd3, "END3", &IDS_SENSORTYPE_END3),
  EnumName(ASTEnd4, "END4", &IDS_SENSORTYPE_END4),
  EnumName(ASTEnd5, "END5", &IDS_SENSORTYPE_END5),
  EnumName(ASTEnd6, "END6", &IDS_SENSORTYPE_END6),
  EnumName(ASTLoose, "LOOSE", &IDS_SENSORTYPE_LOOSE),
  EnumName(ASTEnd1, "WIN", &DUPLICATE_IDS),
  EnumName()
};
template<>
const EnumName *GetEnumNames(ArcadeSensorType dummy)
{
  return ArcadeSensorTypeNames;
}

static const EnumName MarkerTypeNames[]=
{
  EnumName(MTIcon, "ICON", &IDS_DISP_ARCMARK_ICON),
  EnumName(MTRectangle, "RECTANGLE", &IDS_DISP_ARCMARK_RECT),
  EnumName(MTEllipse, "ELLIPSE", &IDS_DISP_ARCMARK_ELLIPSE),
  EnumName()
};
template<>
const EnumName *GetEnumNames(MarkerType dummy)
{
  return MarkerTypeNames;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeUnitInfo

const float MinAbility=0.2; // private
const float MinRank=RankPrivate;
const float MaxAbility=1;
const float MaxRank=RankColonel;

float RankToSkill(int rank)
{
  float factor = (MaxAbility - MinAbility) / (MaxRank - MinRank);
  return (rank - MinRank) * factor + MinAbility;
}

ArcadeUnitInfo::ArcadeUnitInfo()
{
  Init();
}

ArcadeUnitInfo::ArcadeUnitInfo(const ArcadeUnitInfo &src)
{
  presence = src.presence;
  presenceCondition = src.presenceCondition;
  position = src.position;
#if _VBS3 // mission.sqm positionASL
  positionASL = src.positionASL;
#endif
  placement = src.placement;
  azimut = src.azimut;
  special = src.special;
  age = src.age;
  id = src.id;
  side = src.side;
  vehicle = src.vehicle;
  icon = src.icon;
  size = src.size;
  player = src.player;
  forceInServer = src.forceInServer;
  forceHeadlessClient = src.forceHeadlessClient;
  leader = src.leader;
  lock = src.lock;
  rank = src.rank;
  skill = src.skill;
  health = src.health;
  fuel = src.fuel;
  ammo = src.ammo;
  name = src.name;
  markers = src.markers;
  selected = src.selected;
  init = src.init;
  description = src.description;
  synchronizations = src.synchronizations;
  module = src.module;
}

/*!
\patch 1.30 Date 11/03/2001 by Jirka
- Changed: Default skill of units inserted in editor is now half skilled,
not minimum skill as before.
*/

void ArcadeUnitInfo::Init()
{
  presence = 1.0;
  presenceCondition = "true";
  position = VZero;
#if _VBS3 // mission.sqm positionASL
  positionASL = VZero;
#endif
  placement = 0;
  azimut = 0;
  special = ASpForm;
  age = AAUnknown;
  id = 0;
  side = TWest;
  vehicle = "";
  type = NULL;
  icon = NULL;
  size = 0;
  player = APNonplayable;
  forceInServer = false;
  forceHeadlessClient = false;
  leader = 0;
  lock = LSDefault;
  rank = RankPrivate;
  skill = (MaxAbility + MinAbility)*0.5f;
  health = 1.0;
  fuel = 1.0;
  ammo = 1.0;
  name = "";
  markers.Clear();
  selected = false;
  init = "";
  description = "";
  module = false;
  
  synchronizations.Clear();
}

void ArcadeUnitInfo::AddOffset(Vector3Par offset)
{
  position += offset;
  position[1] = GLOB_LAND->RoadSurfaceYAboveWater
  (
    position[0], position[2]
  );
}

void ArcadeUnitInfo::Rotate(Vector3Par center, float angle, bool sel)
{
  if (sel && !selected) return;

  // rotation
  azimut += (180.0 / H_PI) * angle;

  Vector3 dir = position - center;
  Matrix3 rot(MRotationY, -angle);
  dir = rot * dir;

  position = center + dir;
  position[1] = GLandscape->RoadSurfaceYAboveWater
  (
    position[0], position[2]
  );
}

void ArcadeUnitInfo::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
  if (sel && !selected) return;
  
  sum += position;
  count++;
}

/*!
\patch 1.63 Date 6/2/2002 by Ondra
- Changed: Addon requirements are now determined by addon entry creator,
not only by CfgPatches config section. This makes addon list more reliable
with addons that ommited to list some added types.
*/

// ADDED in patch 1.01 - AddOns check
void ArcadeUnitInfo::RequiredAddons(FindArrayRStringCI &addOns)
{
  ConstParamEntryPtr patches = Pars.FindEntry("CfgPatches");
  if (!patches) return;
  for (int i=0; i<patches->GetEntryCount(); i++)
  {
    ParamEntryVal patch = patches->GetEntry(i);
    if (!patch.IsClass()) continue;
    for (int j=0; j<(patch >> "units").GetSize(); j++)
    {
      RStringB patchVehicle = (patch >> "units")[j];
      if (stricmp(patchVehicle, vehicle) == 0)
      {
        addOns.AddUnique(patch.GetName());
        goto Break;
      }
    }
  }
  Break:
  // more robust check - check owner of given unit type
  ConstParamEntryPtr entry = (Pars>>"CfgVehicles").FindEntry(vehicle);
  if (entry)
  {
    const RStringB &owner = entry->GetOwnerName();
    if (owner.GetLength()>0)
    {
      addOns.AddUnique(owner);
    }
  }
}

LSError ArcadeUnitInfo::Serialize(ParamArchive &ar)
{
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst) Init();

  CHECK(ar.Serialize("presence", presence, 1, 1.0)) 
  CHECK(ar.Serialize("presenceCondition", presenceCondition, 1, "true"))  
  CHECK(::Serialize(ar, "position", position, 1)) 
  CHECK(ar.Serialize("placement", placement, 1, 0)) 
  CHECK(ar.Serialize("azimut", azimut, 1, 0)) 

#if _VBS3 // mission.sqm positionASL
  if (ar.IsSaving() || ar.GetArVersion() >= 12)
    CHECK(::Serialize(ar, "positionASL", positionASL, 1))
#endif

  CHECK(ar.SerializeEnum("special", special, 1, ASpForm)) 
  CHECK(ar.SerializeEnum("age", age, 1, AAUnknown)) 
  CHECK(ar.Serialize("id", id, 1))  
  CHECK(ar.SerializeEnum("side", side, 1))  

  CHECK(ar.Serialize("vehicle", vehicle, 1))  
  CHECK(ar.SerializeEnum("player", player, 1, APNonplayable)) 
  CHECK(ar.Serialize("forceInServer", forceInServer, 1, false)) 
  CHECK(ar.Serialize("forceHeadlessClient", forceHeadlessClient, 1, false)) 
  CHECK(ar.Serialize("leader", leader, 1, false)) 
  if (ar.IsSaving() || ar.GetArVersion() >= 11)
    CHECK(ar.SerializeEnum("lock", lock, 1, ENUM_CAST(LockState,LSDefault)))  
  else
  {
    bool locked;
    CHECK(ar.Serialize("locked", locked, 7, false)) 
      lock = locked ? LSLocked : LSDefault;
  }
  CHECK(ar.SerializeEnum("rank", rank, 1, RankPrivate)) 
  CHECK(ar.Serialize("skill", skill, 1, -1.0))  
  CHECK(ar.Serialize("health", health, 1, 1.0)) 
  CHECK(ar.Serialize("fuel", fuel, 1, 1.0)) 
  CHECK(ar.Serialize("ammo", ammo, 1, 1.0)) 
  CHECK(ar.Serialize("text", name, 7, ""))  
  CHECK(ar.SerializeArray("markers", markers, 1)) 
  CHECK(ar.Serialize("init", init, 7, ""))  
  CHECK(ar.Serialize("description", description, 1, ""))  

  CHECK(ar.SerializeArray("synchronizations", synchronizations, 1)) 


  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    ConstParamEntryPtr entry = (Pars >> "CfgVehicles").FindEntry(vehicle);
    if (entry)
    {
      RString iconName = Pars >> "CfgVehicles" >> vehicle >> "icon";
      RString vehicleClass = Pars >> "CfgVehicles" >> vehicle >> "vehicleClass";

      if(strcmpi(vehicleClass.Data(),"Modules")==0) module = true;

      icon = GlobLoadTexture(GetVehicleIcon(iconName));
      size = Pars >> "CfgVehicles" >> vehicle >> "mapSize";
    }
    else
    {
      icon = NULL;
      size = 1;
      WarningMessage("%s: Vehicle class %s no longer exists",cc_cast(ar.GetContext("vehicle")),cc_cast(vehicle));
    }

    ATSParams *params = (ATSParams *)ar.GetParams();
    Assert(params);
    Assert(id >= 0);
//    id += params->baseVeh;
    saturateMax(params->nextVehId, id + 1);
//    if (params->merge) player = APNonplayable;

    if (skill < 0) skill = RankToSkill(rank);
  }

  return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeSensorInfo

ArcadeSensorInfo::ArcadeSensorInfo()
{
  Init();
}

ArcadeSensorInfo::ArcadeSensorInfo(const ArcadeSensorInfo &src)
{
  position = src.position;
  a = src.a;
  b = src.b;
  angle = src.angle;
  rectangular = src.rectangular;
  activationBy = src.activationBy;
  activationType = src.activationType;
  repeating = src.repeating;
  timeoutMin = src.timeoutMin;
  timeoutMid = src.timeoutMid;
  timeoutMax = src.timeoutMax;
  interruptable = src.interruptable;
  type = src.type;
  object = src.object;
  age = src.age;
  idObject = src.idObject;
  idVisitorObj = src.idVisitorObj;
  idVehicle = src.idVehicle;
  text = src.text;
  name = src.name;
  expCond = src.expCond;
  expActiv = src.expActiv;
  expDesactiv = src.expDesactiv;
  effects = src.effects;
  synchronizations = src.synchronizations;
  selected = src.selected;
}

void ArcadeSensorInfo::Init()
{
  position = VZero;
  a = 50.0;
  b = 50.0;
  angle = 0;
  rectangular = false;
  activationBy = ASANone;
  activationType = ASATPresent;
  repeating = false;
  timeoutMin = 0;
  timeoutMid = 0;
  timeoutMax = 0;
  interruptable = true;
  type = ASTNone;
  object = "EmptyDetector";
  age = AAUnknown;
  idObject = ObjectId();
  idVisitorObj = VisitorObjId();
  idVehicle = -1;
  text = "";
  name = "";
  expCond = "this";
  expActiv = "";
  expDesactiv = "";
  effects.Init();
  synchronizations.Clear();
  selected = false;
}

void ArcadeSensorInfo::AddOffset(Vector3Par offset)
{
  position += offset;
  position[1] = GLOB_LAND->RoadSurfaceYAboveWater
  (
    position[0], position[2]
  );
}

void ArcadeSensorInfo::Rotate(Vector3Par center, float alpha, bool sel)
{
  if (sel && !selected) return;

  // rotation
  angle += (180.0 / H_PI) * alpha;

  Vector3 dir = position - center;
  Matrix3 rot(MRotationY, -alpha);
  dir = rot * dir;

  position = center + dir;
  position[1] = GLandscape->RoadSurfaceYAboveWater
  (
    position[0], position[2]
  );
}

void ArcadeSensorInfo::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
  if (sel && !selected) return;
  
  sum += position;
  count++;
}

void ArcadeSensorInfo::RepairObjectIds()
{
  // check id of static object
  if (idVisitorObj != VisitorObjId())
  {
    // linked to static object
    OLink(Object) obj = idObject.IsNull() ? NULL : GLandscape->GetObject(idObject);
    if (!obj || obj->ID() != idVisitorObj)
    {
      // id is invalid
      idObject = GLandscape->GetObjectId(idVisitorObj, position);
    }
  }
}

bool ArcadeSensorInfo::CheckObjectIds()
{
  // check id of static object
  if (idVisitorObj == VisitorObjId()) return true;
  // linked to static object
  OLink(Object) obj = idObject.IsNull() ? NULL : GLandscape->GetObject(idObject);
  if (!obj || obj->ID() != idVisitorObj)
  {
    #if _ENABLE_REPORT
      idObject = GLandscape->GetObjectId(idVisitorObj, position);
    #endif
    return false;
  }
  return true;
}

LSError ArcadeSensorInfo::Serialize(ParamArchive &ar)
{
  CHECK(::Serialize(ar, "position", position, 1)) 
  CHECK(ar.Serialize("a", a, 1, 50.0))  
  CHECK(ar.Serialize("b", b, 1, 50.0))  
  CHECK(ar.Serialize("angle", angle, 1, 0)) 
  CHECK(ar.Serialize("rectangular", rectangular, 7, false)) 

  CHECK(ar.SerializeEnum("activationBy", activationBy, 1, ASANone)) 
  CHECK(ar.SerializeEnum("activationType", activationType, 1, ASATPresent)) 
  CHECK(ar.Serialize("repeating", repeating, 1, false)) 
  CHECK(ar.Serialize("timeoutMin", timeoutMin, 1, 0)) 
  CHECK(ar.Serialize("timeoutMid", timeoutMid, 1, 0)) 
  CHECK(ar.Serialize("timeoutMax", timeoutMax, 1, 0)) 
  CHECK(ar.Serialize("interruptable", interruptable, 1, false)) 
  CHECK(ar.SerializeEnum("type", type, 1, ASTNone)) 
  CHECK(ar.Serialize("object", object, 1, "EmptyDetector")) 
  CHECK(ar.SerializeEnum("age", age, 1))  

  CHECK(idVisitorObj.Serialize(ar, "idStatic", 1))  
  CHECK(idObject.Serialize(ar, "idObject", 1))  
  CHECK(ar.Serialize("idVehicle", idVehicle, 1, -1))  

  CHECK(ar.Serialize("text", text, 3, ""))  
  CHECK(ar.Serialize("name", name, 7, ""))  

  // if value not present, use default - possible in all versions
  CHECK(ar.Serialize("expCond", expCond, 1, "this"))  
  CHECK(ar.Serialize("expActiv", expActiv, 1, ""))  
  CHECK(ar.Serialize("expDesactiv", expDesactiv, 1, ""))  

  CHECK(ar.Serialize("Effects", effects, 1))  

  CHECK(ar.SerializeArray("synchronizations", synchronizations, 1)) 

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    ATSParams *params = (ATSParams *)ar.GetParams();
    Assert(params);
    for (int i=0; i<synchronizations.Size(); i++)
    {
      int &sync = synchronizations[i];
      Assert(sync >= 0);
      saturateMax(params->nextSyncId, sync + 1);
    }
  }

  return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeMarkerInfo

ArcadeMarkerInfo::ArcadeMarkerInfo()
{
  Init();
}

ArcadeMarkerInfo::ArcadeMarkerInfo(const ArcadeMarkerInfo &src)
{
  position = src.position;
  name = src.name;
  text = src.text;
  markerType = src.markerType;
  type = src.type;
  colorName = src.colorName;
  color = src.color;
  alpha = src.alpha;
  fillName = src.fillName;
  fill = src.fill;
  icon = src.icon;
  size = src.size;
  a = src.a;
  b = src.b;
  angle = src.angle;
  selected = src.selected;
  drawBorder = src.drawBorder;
  shadow = src.shadow;
#if _VBS2 // conditional markers
  autosize = src.autosize;
  attachCondition = src.attachCondition;
  attachTo = src.attachTo;
  attachCtx = src.attachCtx;
  attachOffset = src.attachOffset;
  condition = src.condition;
  isHidden = src.isHidden;
#endif
}

void ArcadeMarkerInfo::Init()
{
  position = VZero;
  name = "";
  text = "";
  markerType = MTIcon;
  type = "";
  colorName = "Default";
  color = PackedBlack;
  alpha = 1.0f;
  fillName = "Solid";
  fill = NULL;
  icon = NULL;
  size = 24;
  a = 1;
  b = 1;
  angle = 0;
  selected = false;
  drawBorder = false;
#if _VBS2 // conditional markers
  autosize = true;
  attachCondition = "";
  attachTo = NULL;
  attachOffset = VZero;
  condition = "";  
  isHidden = false;
#endif
}

void ArcadeMarkerInfo::AddOffset(Vector3Par offset)
{
  position += offset;
  position[1] = GLOB_LAND->RoadSurfaceYAboveWater
  (
    position[0], position[2]
  );
}

void ArcadeMarkerInfo::Rotate(Vector3Par center, float alpha, bool sel)
{
  if (sel && !selected) return;

  // rotation
  angle += (180.0 / H_PI) * alpha;

  Vector3 dir = position - center;
  Matrix3 rot(MRotationY, -alpha);
  dir = rot * dir;

  position = center + dir;
  position[1] = GLandscape->RoadSurfaceYAboveWater
  (
    position[0], position[2]
  );
}

void ArcadeMarkerInfo::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
  if (sel && !selected) return;
  
  sum += position;
  count++;
}

LSError ArcadeMarkerInfo::Serialize(ParamArchive &ar)
{
  CHECK(::Serialize(ar, "position", position, 1)) 
  CHECK(ar.Serialize("name", name, 1))  
  CHECK(ar.Serialize("text", text, 1, ""))  
  CHECK(ar.SerializeEnum("markerType", markerType, 1, MTIcon))
  CHECK(ar.Serialize("type", type, 1))  
  CHECK(ar.Serialize("colorName", colorName, 1, "Default")) 
  CHECK(ar.Serialize("alpha", alpha, 1, 1.0f))
  CHECK(ar.Serialize("fillName", fillName, 1, "Solid")) 
  CHECK(ar.Serialize("a", a, 1, 1.0f))  
  CHECK(ar.Serialize("b", b, 1, 1.0f))  
  CHECK(ar.Serialize("angle", angle, 1, 0.0f))  
  CHECK(ar.Serialize("drawBorder", drawBorder, 1, false))  
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    OnTypeChanged();
    OnColorChanged();
    OnFillChanged();
  }

  return LSOK;
}

DEFINE_NETWORK_OBJECT_SIMPLE(ArcadeMarkerInfo, Marker)

#if _VBS2 // conditional markers
#define MARKER_MSG(MessageName, XX) \
  XX(MessageName, Vector3, position, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Marker position"), TRANSF) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker (unique) name"), TRANSF) \
  XX(MessageName, RString, text, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker title"), TRANSF) \
  XX(MessageName, int, markerType, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, MTIcon), DOC_MSG("Marker type (icon, rectangle, ellipse)"), TRANSF) \
  XX(MessageName, RString, type, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker icon"), TRANSF) \
  XX(MessageName, RString, colorName, NDTString, RString, NCTNone, DEFVALUE(RString, "Default"), DOC_MSG("Marker color name"), TRANSF) \
  XX(MessageName, RString, fillName, NDTString, RString, NCTNone, DEFVALUE(RString, "Solid"), DOC_MSG("Marker fill name"), TRANSF) \
  XX(MessageName, float, a, NDTFloat, float, NCTNone, DEFVALUE(float, 1.0f), DOC_MSG("Width"), TRANSF) \
  XX(MessageName, float, b, NDTFloat, float, NCTNone, DEFVALUE(float, 1.0f), DOC_MSG("Height"), TRANSF) \
  XX(MessageName, float, angle, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Rotation"), TRANSF) \
  XX(MessageName, RString, attachCondition, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker attach condition"), TRANSF) \
  XX(MessageName, Vector3, attachOffset, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Marker attach offset"), TRANSF) \
  XX(MessageName, OLink(Object), attachTo, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle to attach marker to"), TRANSF_REF) \
  XX(MessageName, OLinkArray(Object), attachCtx, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("Objects to pass into condition as _this"), TRANSF_REFS) \
  XX(MessageName, RString, condition, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker visibility condition"), TRANSF) \
  XX(MessageName, bool, isHidden, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Marker is hidden (invisible)"), TRANSF) \
  XX(MessageName, AutoArray<RString>, layeredIcons, NDTStringArray, AutoArray<RString>, NCTNone, DEFVALUESTRINGARRAY, DOC_MSG("List of icons to layer when drawing marker"), TRANSF) \
  XX(MessageName, AutoArray<float>, layeredIconsSizeA, NDTFloatArray, AutoArray<float>, NCTNone, DEFVALUEFLOATARRAY, DOC_MSG("Layered icons size A"), TRANSF) \
  XX(MessageName, AutoArray<float>, layeredIconsSizeB, NDTFloatArray, AutoArray<float>, NCTNone, DEFVALUEFLOATARRAY, DOC_MSG("Layered icons size B"), TRANSF) \
  XX(MessageName, AutoArray<float>, layeredIconsOffsetX, NDTFloatArray, AutoArray<float>, NCTNone, DEFVALUEFLOATARRAY, DOC_MSG("Layered icons offset X"), TRANSF) \
  XX(MessageName, AutoArray<float>, layeredIconsOffsetY, NDTFloatArray, AutoArray<float>, NCTNone, DEFVALUEFLOATARRAY, DOC_MSG("Layered icons offset Y"), TRANSF) \
  XX(MessageName, bool, autosize, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Marker will maintain the same size regardless of map zoom"), TRANSF) \
  XX(MessageName, float, alpha, NDTFloat, float, NCTFloat0To1, DEFVALUE(float, 1.0f), DOC_MSG("Marker fading"), TRANSF) \
  XX(MessageName, bool, drawBorder, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Draw Border"), TRANSF)
#else
#define MARKER_MSG(MessageName, XX) \
  XX(MessageName, Vector3, position, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Marker position"), TRANSF) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker (unique) name"), TRANSF) \
  XX(MessageName, RString, text, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker title"), TRANSF) \
  XX(MessageName, int, markerType, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, MTIcon), DOC_MSG("Marker type (icon, rectangle, ellipse)"), TRANSF) \
  XX(MessageName, RString, type, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Marker icon"), TRANSF) \
  XX(MessageName, RString, colorName, NDTString, RString, NCTNone, DEFVALUE(RString, "Default"), DOC_MSG("Marker color name"), TRANSF) \
  XX(MessageName, float, alpha, NDTFloat, float, NCTFloat0To1, DEFVALUE(float, 1.0f), DOC_MSG("Marker fading"), TRANSF) \
  XX(MessageName, RString, fillName, NDTString, RString, NCTNone, DEFVALUE(RString, "Solid"), DOC_MSG("Marker fill name"), TRANSF) \
  XX(MessageName, float, a, NDTFloat, float, NCTNone, DEFVALUE(float, 1.0f), DOC_MSG("Width"), TRANSF) \
  XX(MessageName, float, b, NDTFloat, float, NCTNone, DEFVALUE(float, 1.0f), DOC_MSG("Height"), TRANSF) \
  XX(MessageName, float, angle, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Rotation"), TRANSF) \
  XX(MessageName, bool, drawBorder, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Draw Border"), TRANSF)
#endif

DECLARE_NET_INDICES_SERIALIZABLE(Marker, MARKER_MSG)
DEFINE_NET_INDICES_SERIALIZABLE(Marker, MARKER_MSG)

bool IsEqualMarker(NetworkMessage *marker1, NetworkMessage *marker2)
{
  NetworkMessageMarker *msg1 = (NetworkMessageMarker *)marker1;
  NetworkMessageMarker *msg2 = (NetworkMessageMarker *)marker2;

  return msg1->_name == msg2->_name;
}

bool IsEqualMarker(RString name, NetworkMessage *marker2)
{
  NetworkMessageMarker *msg = (NetworkMessageMarker *)marker2;
  return name==msg->_name;
}

NetworkMessageFormat &ArcadeMarkerInfo::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  MARKER_MSG(Marker, MSG_FORMAT)
  return format;
}

TMError ArcadeMarkerInfo::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(Marker)

  TRANSF_EX(position, position)
  TRANSF_EX(name, name)
  TRANSF_EX(text, text)
  TRANSF_ENUM_EX(markerType, markerType)
  TRANSF_EX(type, type)
  TRANSF_EX(colorName, colorName)
  TRANSF_EX(alpha, alpha)
  TRANSF_EX(fillName, fillName)
  TRANSF_EX(a, a)
  TRANSF_EX(b, b)
  TRANSF_EX(angle, angle)
  TRANSF_EX(drawBorder, drawBorder)
#if _VBS2 // conditional markers
  TRANSF_EX(autosize, autosize)
  TRANSF_EX(attachCondition, attachCondition)
  TRANSF_EX(attachOffset, attachOffset)
  TRANSF_REF_EX(attachTo, attachTo)
  TRANSF_REFS_EX(attachCtx, attachCtx)
  TRANSF_EX(condition, condition)
  TRANSF_EX(isHidden, isHidden)
  AutoArray<RString> t; 
  AutoArray<float> sA, sB, oX, oY;  
  if (ctx.IsSending())
  {
    for (int i=0; i<layeredIcons.Size(); i++)
    {
      if (layeredIcons[i] && layeredIcons[i]->icon)
      {
        t.Add("\\" + layeredIcons[i]->icon->GetName());
        sA.Add(layeredIcons[i]->a);
        sB.Add(layeredIcons[i]->b);
        oX.Add(layeredIcons[i]->x);
        oY.Add(layeredIcons[i]->y);
      }
    }
    TRANSF_EX(layeredIcons, t)
    TRANSF_EX(layeredIconsSizeA, sA)
    TRANSF_EX(layeredIconsSizeB, sB)
    TRANSF_EX(layeredIconsOffsetX, oX)
    TRANSF_EX(layeredIconsOffsetY, oY)
  }
  else
  {
    TRANSF_EX(layeredIcons, t)
    TRANSF_EX(layeredIconsSizeA, sA)
    TRANSF_EX(layeredIconsSizeB, sB)
    TRANSF_EX(layeredIconsOffsetX, oX)
    TRANSF_EX(layeredIconsOffsetY, oY)
    layeredIcons.Clear();
    for (int i=0; i<t.Size(); i++)
    {
      int j = layeredIcons.Add(new LayeredMarkerInfo());
      LayeredMarkerInfo *newLayer = layeredIcons[j];
      newLayer->icon = GlobLoadTexture (GetPictureName(t[i]));
      newLayer->a = sA[i];
      newLayer->b = sB[i]; 
      newLayer->x = oX[i]; 
      newLayer->y = oY[i]; 
    }
  }
#endif
  if (!ctx.IsSending())
  {
    OnTypeChanged();
    OnColorChanged();
    OnFillChanged();
  }
  return TMOK;
}

/*!
\patch 5088 Date 11/15/2006 by Bebul
- Fixed: Message: No entry 'bin\config.bin/CfgMarkers.'
*/

void ArcadeMarkerInfo::OnColorChanged()
{
  if (markerType == MTIcon && stricmp(colorName, "Default") == 0)
  {
    if (!type.IsEmpty())
    {
      ParamEntryVal cls = Pars >> "CfgMarkers" >> type;
      color = GetPackedColor(cls >> "color");
    }
  }
  else
  {
    ParamEntryVal cls = Pars >> "CfgMarkerColors" >> colorName;
    color = GetPackedColor(cls >> "color");
  }
}

void ArcadeMarkerInfo::OnFillChanged()
{
  ParamEntryVal cls = Pars >> "CfgMarkerBrushes" >> fillName;
  RString brush = cls >> "texture";
  drawBorder =  cls >> "drawBorder";

  if (brush.GetLength() == 0)
    fill = NULL;
  else
    fill = GlobLoadTexture(GetPictureName(brush));
}

void ArcadeMarkerInfo::OnTypeChanged()
{
  if (markerType == MTIcon)
  {
    if (!type.IsEmpty())
    {
      ParamEntryVal cls = Pars >> "CfgMarkers" >> type;
      icon = GlobLoadTexture
        (
        GetPictureName(cls >> "icon")
        );
      size = cls >> "size";
      if(cls.FindEntry("shadow")) shadow = cls >> "shadow";
      else shadow = true;

      if (stricmp(colorName, "Default") == 0)
      {
        color = GetPackedColor(cls >> "color");
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeEffects

ArcadeEffects::ArcadeEffects()
{
  Init();
}

ArcadeEffects::ArcadeEffects(const ArcadeEffects &src)
{
  condition = src.condition;
  sound = src.sound;
  voice = src.voice;
  soundEnv = src.soundEnv;
  soundDet = src.soundDet;
  track = src.track;
  titleType = src.titleType;
  titleEffect = src.titleEffect;
  title = src.title;
}

void ArcadeEffects::Init()
{
  condition = "true";
  sound = "$NONE$";
  voice = "";
  soundEnv = "";
  soundDet = "";
  track = "$NONE$";
  titleType = TitleNone;
  titleEffect = TitPlain;
  title = "";
}

LSError ArcadeEffects::Serialize(ParamArchive &ar)
{
  if (ar.IsSaving() || ar.GetArVersion() >= 9)
    CHECK(ar.Serialize("condition", condition, 9, "true"))
  else
  {
    bool playerOnly;
    CHECK(ar.Serialize("playerOnly", playerOnly, 1, false)) 
    if (playerOnly)
      condition = "thisList";
    else
      condition = "true";
  }

  CHECK(ar.Serialize("sound", sound, 1, "$NONE$"))
  CHECK(ar.Serialize("voice", voice, 5, ""))
  CHECK(ar.Serialize("soundEnv", soundEnv, 5, ""))
  CHECK(ar.Serialize("soundDet", soundDet, 5, ""))
  CHECK(ar.Serialize("track", track, 1, "$NONE$"))

  CHECK(ar.SerializeEnum("titleType", titleType, 1, TitleNone)) 
  CHECK(ar.SerializeEnum("titleEffect", titleEffect, 1, ENUM_CAST(TitEffectName,TitPlain)))
  CHECK(ar.Serialize("title", title, 1, ""))  

  return LSOK;
}

LSError ArcadeEffects::WorldSerialize(ParamArchive &ar)
{
  if (ar.IsSaving() || ar.GetArVersion() >= 5)
    CHECK(ar.Serialize("condition", condition, 5, "true"))
  else
  {
    bool playerOnly;
    CHECK(ar.Serialize("playerOnly", playerOnly, 1, false)) 
    if (playerOnly)
      condition = "thisList";
    else
      condition = "true";
  }

  CHECK(ar.Serialize("sound", sound, 1, "$NONE$"))
  CHECK(ar.Serialize("voice", voice, 1, ""))
  CHECK(ar.Serialize("soundEnv", soundEnv, 1, ""))
  CHECK(ar.Serialize("soundDet", soundDet, 1, ""))
  CHECK(ar.Serialize("track", track, 1, "$NONE$"))

  CHECK(ar.SerializeEnum("titleType", titleType, 1, TitleNone)) 
  CHECK(ar.SerializeEnum("titleEffect", titleEffect, 1, ENUM_CAST(TitEffectName,TitPlain)))
  CHECK(ar.Serialize("title", title, 1, ""))  

  return LSOK;
}

DEFINE_NETWORK_OBJECT_SIMPLE(ArcadeEffects, Effects)

#define EFFECTS_MSG(MessageName, XX) \
  XX(MessageName, RString, condition, NDTString, RString, NCTNone, DEFVALUE(RString, "true"), DOC_MSG("Condition when effect is performed"), TRANSF) \
  XX(MessageName, RString, sound, NDTString, RString, NCTNone, DEFVALUE(RString, "$NONE$"), DOC_MSG("Sound effect (2D)"), TRANSF) \
  XX(MessageName, RString, voice, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sound effect (3D)"), TRANSF) \
  XX(MessageName, RString, soundEnv, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Enviromental sound effect"), TRANSF) \
  XX(MessageName, RString, soundDet, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Detector sound effect"), TRANSF) \
  XX(MessageName, RString, track, NDTString, RString, NCTNone, DEFVALUE(RString, "$NONE$"), DOC_MSG("Musical track"), TRANSF) \
  XX(MessageName, int, titleType, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, TitleNone), DOC_MSG("Type of title effect (text, object, resource)"), TRANSF) \
  XX(MessageName, int, titleEffect, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, TitPlain), DOC_MSG("Type (placement) of text title effect"), TRANSF) \
  XX(MessageName, RString, title, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Content of title effect"), TRANSF)

DECLARE_NET_INDICES_INIT_MSG(Effects, EFFECTS_MSG)
DEFINE_NET_INDICES_INIT_MSG(Effects, EFFECTS_MSG)
//DECLARE_NET_INDICES(Effects, EFFECTS_MSG)
//DEFINE_NET_INDICES(Effects, EFFECTS_MSG)

NetworkMessageFormat &ArcadeEffects::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  EFFECTS_MSG(Effects, MSG_FORMAT)
  return format;
}

TMError ArcadeEffects::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(Effects)

  TRANSF_EX(condition, condition)
  TRANSF_EX(sound, sound)
  TRANSF_EX(voice, voice)
  TRANSF_EX(soundEnv, soundEnv)
  TRANSF_EX(soundDet, soundDet)
  TRANSF_EX(track, track)
  TRANSF_ENUM_EX(titleType, titleType)
  TRANSF_ENUM_EX(titleEffect, titleEffect)
  TRANSF_EX(title, title)
  return TMOK;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeWaypointInfo

WaypointInfo::WaypointInfo()
#if _VBS3 // allow waypoints to have a variable space
: vars(true)
#endif
{
  InitWp();
}

void WaypointInfo::InitWpId(AIGroup *grp)
{
  idWP = grp->NewWaypointId();
}

void WaypointInfo::InitWp()
{
  position = VZero;
  placement = 0;
  completitionRadius = 0;
  id = -1;
  idObject = ObjectId();
  housePos = -1;
  speed = SpeedUnchanged;
  combat = CMUnchanged;
  type = ACUNDEFINED;
  timeoutMin = 0;
  timeoutMid = 0;
  timeoutMax = 0;
  combatMode = (AI::Semaphore)-1; // TODO: AI::SemaphoreNoChange
  formation = (AI::Formation)-1; // TODO: AI::SemaphoreNoChange
  description = "";
  _expCond = "true";
  _expActiv = "";
#if USE_PRECOMPILATION
  GGameState.CompileMultiple(_expCond, _compiledCond, GWorld->GetMissionNamespace()); // mission namespace
#endif
  script = "";
  showWP = ShowNever;
  synchronizations.Clear();
  effects.Init();
  selected = false;
  visible = true;
}

ArcadeWaypointInfo::ArcadeWaypointInfo()
{
  idVisitorObj = VisitorObjId();
}

void ArcadeWaypointInfo::Init()
{
  InitWp();
  idVisitorObj = VisitorObjId();
}

ArcadeWaypointInfo::ArcadeWaypointInfo(const ArcadeWaypointInfo &src)
{
  position = src.position;
  placement = src.placement;
  completitionRadius = src.completitionRadius;
  id = src.id;
  idObject = src.idObject;
  idVisitorObj = src.idVisitorObj;
  housePos = src.housePos;
  speed = src.speed;
  combat = src.combat;
  type = src.type;
  timeoutMin = src.timeoutMin;
  timeoutMid = src.timeoutMid;
  timeoutMax = src.timeoutMax;
  combatMode = src.combatMode;
  formation = src.formation;
  description = src.description;
  _expCond = src._expCond;
  _expActiv = src._expActiv;
#if USE_PRECOMPILATION
  GGameState.CompileMultiple(_expCond, _compiledCond, GWorld->GetMissionNamespace()); // mission namespace
  GGameState.CompileMultiple(_expActiv, _compiledActiv, GWorld->GetMissionNamespace()); // mission namespace
#endif
  script = src.script;
  showWP = src.showWP;
  synchronizations = src.synchronizations;
  effects = src.effects;
  selected = src.selected;
  visible = src.visible;
}

void ArcadeWaypointInfo::AddOffset(Vector3Par offset)
{
  position += offset;
  position[1] = GLOB_LAND->RoadSurfaceYAboveWater
  (
    position[0], position[2]
  );
}

void ArcadeWaypointInfo::Rotate(Vector3Par center, float angle, bool sel)
{
  if (sel && !selected) return;

  // rotation

  Vector3 dir = position - center;
  Matrix3 rot(MRotationY, -angle);
  dir = rot * dir;

  position = center + dir;
  position[1] = GLandscape->RoadSurfaceYAboveWater
  (
    position[0], position[2]
  );
}

void ArcadeWaypointInfo::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
  if (sel && !selected) return;
  
  sum += position;
  count++;
}

void ArcadeWaypointInfo::RepairObjectIds()
{
  // check id of static object
  if (idVisitorObj != VisitorObjId())
  {
    // linked to static object
    OLink(Object) obj = idObject.IsNull() ? NULL : GLandscape->GetObject(idObject);
    if (!obj || obj->ID() != idVisitorObj)
    {
      // id is invalid
      idObject = GLandscape->GetObjectId(idVisitorObj, position);
    }
  }
}

bool ArcadeWaypointInfo::CheckObjectIds()
{
  // check id of static object
  if (idVisitorObj == VisitorObjId()) return true;
  // linked to static object
  OLink(Object) obj = idObject.IsNull() ? NULL : GLandscape->GetObject(idObject);
  if (!obj || obj->ID() != idVisitorObj)
  {
    // id is invalid
    idObject = GLandscape->GetObjectId(idVisitorObj, position);
    return false;
  }
  return true;
}

bool WaypointInfo::HasEffect() const
{
  if (stricmp(effects.sound, "$NONE$") != 0) return true;
  if (effects.voice.GetLength() > 0) return true;
  if (effects.soundEnv.GetLength() > 0) return true;
  if (effects.soundDet.GetLength() > 0) return true;
  if (stricmp(effects.track, "$NONE$") != 0) return true;
  if (effects.titleType != TitleNone) return true;
  return false;
}

bool WaypointInfo::RequiresStop() const
{
  return type == ACGETOUT || type == ACLOAD || type == ACUNLOAD || type == ACTRANSPORTUNLOAD;
}

LSError ArcadeWaypointInfo::Serialize(ParamArchive &ar)
{
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst) Init();

  CHECK(::Serialize(ar, "position", position, 1)) 
  CHECK(ar.Serialize("placement", placement, 1, 0)) 
  CHECK(ar.Serialize("completitionRadius", completitionRadius, 1, 0)) 
  CHECK(ar.Serialize("id", id, 1, -1))  
  CHECK(idVisitorObj.Serialize(ar, "idStatic", 1))  
  CHECK(idObject.Serialize(ar, "idObject", 1, ObjectId()))  
  CHECK(ar.Serialize("housePos", housePos, 2, -1))  
  CHECK(ar.SerializeEnum("type", type, 1, ACMOVE))  
  CHECK(ar.SerializeEnum("combatMode", combatMode, 1, (AI::Semaphore)-1)) 
  CHECK(ar.SerializeEnum("formation", formation, 1, (AI::Formation)-1)) 
  CHECK(ar.SerializeEnum("speed", speed, 1, SpeedUnchanged))  
  CHECK(ar.SerializeEnum("combat", combat, 4, CMUnchanged)) 
  CHECK(ar.Serialize("description", description, 1, ""))  
  CHECK(ar.Serialize("expCond", _expCond, 7, "true"))  
  CHECK(ar.Serialize("expActiv", _expActiv, 7, ""))  
  CHECK(ar.Serialize("visible", visible,1, true))
#if USE_PRECOMPILATION
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    GGameState.CompileMultiple(_expCond, _compiledCond, GWorld->GetMissionNamespace()); // mission namespace
    GGameState.CompileMultiple(_expActiv, _compiledActiv, GWorld->GetMissionNamespace()); // mission namespace
  }
#endif
  CHECK(ar.Serialize("script", script, 7, ""))  
  CHECK(ar.SerializeArray("synchronizations", synchronizations, 1)) 
  CHECK(ar.Serialize("Effects", effects, 1))  
  CHECK(ar.Serialize("timeoutMin", timeoutMin, 1, 0)) 
  CHECK(ar.Serialize("timeoutMid", timeoutMid, 1, 0)) 
  CHECK(ar.Serialize("timeoutMax", timeoutMax, 1, 0)) 
  if (ar.GetArVersion() >= 10)
  {
    CHECK(ar.SerializeEnum("showWP", showWP, 1, ShowEasy))
  }
  else
  {
    bool show = showWP != ShowNever;
    CHECK(ar.Serialize("show", show, 1, false))
    if (show) showWP = ShowEasy;
    else showWP = ShowNever;
  }

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    ATSParams *params = (ATSParams *)ar.GetParams();
    Assert(params);
    for (int i=0; i<synchronizations.Size(); i++)
    {
      int &sync = synchronizations[i];
      Assert(sync >= 0);
      saturateMax(params->nextSyncId, sync + 1);
    }
  }
  return LSOK;
}

LSError WaypointInfo::Serialize(ParamArchive &ar)
{
  // world serialization
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst) InitWp();

  CHECK(::Serialize(ar, "position", position, 1)) 
  CHECK(ar.Serialize("placement", placement, 1, 0)) 
  CHECK(ar.Serialize("completitionRadius", completitionRadius, 1, 0)) 
  CHECK(ar.Serialize("id", id, 1, -1))  
  CHECK(idObject.Serialize(ar, "idObject", 1, ObjectId()))  
  CHECK(ar.Serialize("housePos", housePos, 1, -1))  
  CHECK(ar.SerializeEnum("type", type, 1, ACMOVE))  
  CHECK(ar.SerializeEnum("combatMode", combatMode, 1, (AI::Semaphore)-1)) 
  CHECK(ar.SerializeEnum("formation", formation, 1, (AI::Formation)-1)) 
  CHECK(ar.SerializeEnum("speed", speed, 1, SpeedUnchanged))  
  CHECK(ar.SerializeEnum("combat", combat, 1, CMUnchanged)) 
  CHECK(ar.Serialize("description", description, 1, ""))  
  CHECK(ar.Serialize("expCond", _expCond, 1, "true"))  
  CHECK(ar.Serialize("expActiv", _expActiv, 1, ""))
  CHECK(ar.Serialize("visible", visible,1, true))
#if USE_PRECOMPILATION
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    GGameState.CompileMultiple(_expCond, _compiledCond, GWorld->GetMissionNamespace()); // mission namespace
    GGameState.CompileMultiple(_expActiv, _compiledActiv, GWorld->GetMissionNamespace()); // mission namespace
  }
#endif
  CHECK(ar.Serialize("script", script, 1, ""))  
  CHECK(ar.SerializeArray("synchronizations", synchronizations, 1)) 
  ParamArchive arSubcls;
  if (!ar.OpenSubclass("Effects", arSubcls)) return LSStructure;
  effects.WorldSerialize(arSubcls);
  CHECK(ar.Serialize("timeoutMin", timeoutMin, 1, 0)) 
  CHECK(ar.Serialize("timeoutMid", timeoutMid, 1, 0)) 
  CHECK(ar.Serialize("timeoutMax", timeoutMax, 1, 0)) 
  if (ar.GetArVersion() >= 9)
  {
    CHECK(ar.SerializeEnum("showWP", showWP, 1, ShowEasy))
  }
  else
  {
    bool show = showWP != ShowNever;
    CHECK(ar.Serialize("show", show, 1, false))
    if (show) showWP = ShowEasy;
    else showWP = ShowNever;
  }
  return LSOK;
}


DEFINE_NETWORK_OBJECT_SIMPLE(WaypointInfo, Waypoint)

DEFINE_NET_INDICES_INIT_MSG(Waypoint, WAYPOINT_MSG)
//DECLARE_NET_INDICES(Waypoint, WAYPOINT_MSG)
//DEFINE_NET_INDICES(Waypoint, WAYPOINT_MSG)

NetworkMessageFormat &WaypointInfo::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  WAYPOINT_MSG(Waypoint, MSG_FORMAT)
  return format;
}

TMError WaypointInfo::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(Waypoint)
  
  TRANSF_EX(position, position)
  TRANSF_EX(placement, placement)
  TRANSF_EX(completitionRadius, completitionRadius)
  TRANSF_EX(id, id)
  if (ctx.IsSending())
  {
    int encoded = idObject.Encode();
    TRANSF_EX(idObject, encoded)
  }
  else
  {
    int encoded = -1;
    TRANSF_EX(idObject, encoded)
    idObject.Decode(encoded);
  }
  TRANSF_EX(housePos, housePos)
  TRANSF_ENUM_EX(type, type)
  TRANSF_ENUM_EX(combatMode, combatMode)
  TRANSF_ENUM_EX(formation, formation)
  TRANSF_ENUM_EX(speed, speed)
  TRANSF_ENUM_EX(combat, combat)
  TRANSF_EX(timeoutMin, timeoutMin)
  TRANSF_EX(timeoutMid, timeoutMid)
  TRANSF_EX(timeoutMax, timeoutMax)
  TRANSF_EX(description, description)
  TRANSF_EX(expCond, _expCond)
  TRANSF_EX(expActiv, _expActiv)
#if USE_PRECOMPILATION
  if (!ctx.IsSending())
  {
    GGameState.CompileMultiple(_expCond, _compiledCond, GWorld->GetMissionNamespace()); // mission namespace
    GGameState.CompileMultiple(_expActiv, _compiledActiv, GWorld->GetMissionNamespace()); // mission namespace
  }
#endif
  TRANSF_EX(script, script)
  TRANSF_ENUM_EX(showWP, showWP)
  TRANSF_EX(synchronizations, synchronizations)
  TRANSF_OBJECT_EX(effects, effects)
  TRANSF_EX(visible, visible)
  DoAssert(!ctx.IsSending() || idWP!=0); // when sending we know the format is new, verify the waypoint id was set
  TRANSF_EX(idWP, idWP)
  return TMOK;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeGroupInfo

void ArcadeGroupInfo::AddOffset(Vector3Par offset)
{
  for (int i=0; i<units.Size(); i++)
  {
    units[i].AddOffset(offset);
  }
  for (int i=0; i<sensors.Size(); i++)
  {
    sensors[i].AddOffset(offset);
  }
  for (int i=0; i<waypoints.Size(); i++)
  {
    waypoints[i].AddOffset(offset);
  }
}

void ArcadeGroupInfo::Rotate(Vector3Par center, float angle, bool sel)
{
  for (int i=0; i<units.Size(); i++)
  {
    units[i].Rotate(center, angle, sel);
  }
  for (int i=0; i<sensors.Size(); i++)
  {
    sensors[i].Rotate(center, angle, sel);
  }
  for (int i=0; i<waypoints.Size(); i++)
  {
    waypoints[i].Rotate(center, angle, sel);
  }
}

void ArcadeGroupInfo::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
  for (int i=0; i<units.Size(); i++)
  {
    units[i].CalculateCenter(sum, count, sel);
  }
  for (int i=0; i<sensors.Size(); i++)
  {
    sensors[i].CalculateCenter(sum, count, sel);
  }
  for (int i=0; i<waypoints.Size(); i++)
  {
    waypoints[i].CalculateCenter(sum, count, sel);
  }
}

void ArcadeGroupInfo::Select(bool select)
{
  for (int i=0; i<units.Size(); i++)
  {
    units[i].selected = select;
  }
  for (int i=0; i<sensors.Size(); i++)
  {
    sensors[i].selected = select;
  }
  for (int i=0; i<waypoints.Size(); i++)
  {
    waypoints[i].selected = select;
  }
}

// ADDED in patch 1.01 - AddOns check
void ArcadeGroupInfo::RequiredAddons(FindArrayRStringCI &addOns)
{
  for (int i=0; i<units.Size(); i++) units[i].RequiredAddons(addOns);
}

bool ArcadeGroupInfo::CheckObjectIds()
{
  bool ret = true;
  for (int i=0; i<waypoints.Size(); i++)
    if (!waypoints[i].CheckObjectIds()) ret = false;
  for (int i=0; i<sensors.Size(); i++)
    if (!sensors[i].CheckObjectIds()) ret = false;
  return ret;
}

void ArcadeGroupInfo::RepairObjectIds()
{
  for (int i=0; i<waypoints.Size(); i++)
    waypoints[i].RepairObjectIds();
  for (int i=0; i<sensors.Size(); i++)
    sensors[i].RepairObjectIds();
}

LSError ArcadeGroupInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeEnum("side", side, 1))  
  CHECK(ar.Serialize("Vehicles", units, 1))
  CHECK(ar.Serialize("Waypoints", waypoints, 1))
  CHECK(ar.Serialize("Sensors", sensors, 1))
  return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// struct ArcadeIntel

ArcadeIntel::ArcadeIntel()
{
  Init();
}

void ArcadeIntel::Init()
{
  friends[TEast][TEast] = 1.0;      friends[TEast][TWest] = 0.0;      friends[TEast][TGuerrila] = 0.0;
  friends[TWest][TEast] = 0.0;      friends[TWest][TWest] = 1.0;      friends[TWest][TGuerrila] = 1.0;
  friends[TGuerrila][TEast] = 0.0;  friends[TGuerrila][TWest] = 1.0;  friends[TGuerrila][TGuerrila] = 1.0;
  
  ConstParamEntryPtr cfg = NULL;
  if (*Glob.header.worldname) cfg = (Pars >> "CfgWorlds").FindEntry(Glob.header.worldname);

  year = 1985;
  month = 5;
  day = 10;
  hour = 7;
  minute = 30;

  if (cfg)
  {
    // initialize from the world config
    weather = *cfg >> "startWeather";
    weatherForecast = *cfg >> "forecastWeather";
    fog = *cfg >> "startFog";
    fogForecast = *cfg >> "forecastFog";

    RString date = *cfg >> "startDate";
    RString time = *cfg >> "startTime";
    sscanf(date, "%d/%d/%d", &day, &month, &year);
    if (year < 100) year += 1900;
    sscanf(time, "%d:%d", &hour, &minute);
  }
  else
  {
    weather = 0.5;
    fog = 0;
    weatherForecast = 0.5;
    fogForecast = 0;

  }
  viewDistance = 0;

  briefingName = "";
  briefingDescription = "";
}

void SendIntel(ArcadeIntel *intel);

LSError ArcadeIntel::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("briefingName", briefingName, 2, ""))  
  CHECK(ar.Serialize("briefingDescription", briefingDescription, 2, ""))  

  if (ar.GetArVersion() >= 10)
  {
    CHECK(ar.Serialize("resistanceWest", friends[TWest][TGuerrila], 1, 1.0))
    CHECK(ar.Serialize("resistanceEast", friends[TEast][TGuerrila], 1, 0.0))
  }
  else
  {
    CHECK(ar.Serialize("resistance", friends[TWest][TGuerrila], 1, 1.0))
    friends[TEast][TGuerrila] = 1.0f - friends[TWest][TGuerrila];
  }
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    friends[TEast][TEast] = 1.0f; friends[TEast][TWest] = 0.0f;
    friends[TWest][TEast] = 0.0f; friends[TWest][TWest] = 1.0f;
    friends[TGuerrila][TGuerrila] = 1.0f;

    friends[TGuerrila][TEast] = friends[TEast][TGuerrila];
    friends[TGuerrila][TWest] = friends[TWest][TGuerrila];
  }

  CHECK(ar.Serialize("startWeather", weather, 1, 0.5))  
  CHECK(ar.Serialize("startFog", fog, 1, 0))  
  CHECK(ar.Serialize("forecastWeather", weatherForecast, 1, 0.5)) 
  CHECK(ar.Serialize("forecastFog", fogForecast, 1, 0)) 
  CHECK(ar.Serialize("viewDistance", viewDistance, 1, 0)) 
  CHECK(ar.Serialize("year", year, 1, 1985))  
  CHECK(ar.Serialize("month", month, 1, 5)) 
  CHECK(ar.Serialize("day", day, 1, 10))  
  CHECK(ar.Serialize("hour", hour, 1, 7)) 
  CHECK(ar.Serialize("minute", minute, 1, 30))  

  return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// class ArcadeTemplate

void SelectLeader(ArcadeGroupInfo &gInfo)
{
  Assert(gInfo.units.Size() > 0);
  int maxRank = RankPrivate - 1;
  int i, iBest = -1;
  for (i=0; i<gInfo.units.Size(); i++)
  {
    ArcadeUnitInfo &uInfo = gInfo.units[i];
    uInfo.leader = false;
    if (uInfo.rank > maxRank)
    {
      maxRank = uInfo.rank;
      iBest = i;
    }
  }
  Assert(iBest >= 0);
  saturateMax(iBest, 0);
  Assert(maxRank >= RankPrivate);
  gInfo.units[iBest].leader = true;
  gInfo.side = gInfo.units[iBest].side;
}

void SendBuildingUpdate(int id, int condition);

ArcadeTemplate::ArcadeTemplate()
{
  showHUD = true;
  showMap = true;
  showWatch = true;
  showCompass = true;
  showNotepad = true;
  showGPS = false;

  nextSyncId = 0;
  nextVehId = 0;

  randomSeed = toLargeInt(GRandGen.RandomValue()*0x1000000)+3;
}

void ArcadeTemplate::CheckSynchro()
{
  // Synchronizations waypoint - waypoint, waypoint - sensor
  AutoArray<int, MemAllocLocal<int, 256> > syncCountsW;
  AutoArray<int, MemAllocLocal<int, 256> > syncCountsS;

  nextSyncId = 0;
  int n = groups.Size();
  int m = sensors.Size();
  
  // find the greatest sync number used
  for (int i=0; i<n; i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    int m = gInfo.waypoints.Size();
    for (int j=0; j<m; j++)
    {
      ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
      int p = wInfo.synchronizations.Size();
      for (int l=0; l<p; l++)
        saturateMax(nextSyncId, wInfo.synchronizations[l] + 1);
    }
    m = gInfo.sensors.Size();
    for (int j=0; j<m; j++)
    {
      ArcadeSensorInfo &sInfo = gInfo.sensors[j];
      int p = sInfo.synchronizations.Size();
      for (int l=0; l<p; l++)
        saturateMax(nextSyncId, sInfo.synchronizations[l] + 1);
    }
  }
  for (int j=0; j<m; j++)
  {
    ArcadeSensorInfo &sInfo = sensors[j];
    int p = sInfo.synchronizations.Size();
    for (int l=0; l<p; l++)
      saturateMax(nextSyncId, sInfo.synchronizations[l] + 1);
  }
  
  syncCountsW.Resize(nextSyncId);
  syncCountsS.Resize(nextSyncId);
  for (int i=0; i<nextSyncId; i++)
  {
    syncCountsW[i] = 0;
    syncCountsS[i] = 0;
  }

  // for each synchronization, find how much times is used
  for (int i=0; i<n; i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    int m = gInfo.waypoints.Size();
    for (int j=0; j<m; j++)
    {
      ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
      int p = wInfo.synchronizations.Size();
      for (int l=0; l<p; l++)
      {
        int sync = wInfo.synchronizations[l];
        Assert(sync >= 0);
        syncCountsW[sync]++;
      }
    }
    m = gInfo.sensors.Size();
    for (int j=0; j<m; j++)
    {
      ArcadeSensorInfo &sInfo = gInfo.sensors[j];
      int p = sInfo.synchronizations.Size();
      for (int l=0; l<p; l++)
      {
        int sync = sInfo.synchronizations[l];
        Assert(sync >= 0);
        syncCountsS[sync]++;
      }
    }
  }
  for (int j=0; j<m; j++)
  {
    ArcadeSensorInfo &sInfo = sensors[j];
    int p = sInfo.synchronizations.Size();
    for (int l=0; l<p; l++)
    {
      int sync = sInfo.synchronizations[l];
      Assert(sync >= 0);
      syncCountsS[sync]++;
    }
  }

  // delete synchronizations not used twice (orphan)
  for (int i=0; i<n; i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    int m = gInfo.waypoints.Size();
    for (int j=0; j<m; j++)
    {
      ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
      int p = wInfo.synchronizations.Size();
      for (int l=0; l<p;)
      {
        int sync = wInfo.synchronizations[l];
        Assert(sync >= 0);
        Assert(syncCountsW[sync] >= 1);
        if (syncCountsW[sync] + syncCountsS[sync] < 2)
        {
          wInfo.synchronizations.Delete(l);
          p--;
        }
        else
        {
          l++;
        }
      }
    }
    m = gInfo.sensors.Size();
    for (int j=0; j<m; j++)
    {
      ArcadeSensorInfo &sInfo = gInfo.sensors[j];
      int p = sInfo.synchronizations.Size();
      for (int l=0; l<p;)
      {
        int sync = sInfo.synchronizations[l];
        Assert(sync >= 0);
        Assert(syncCountsS[sync] >= 1);
        if (syncCountsW[sync] < 1)
        {
          sInfo.synchronizations.Delete(l);
          p--;
        }
        else
        {
          l++;
        }
      }
    }
  }
  for (int j=0; j<m; j++)
  {
    ArcadeSensorInfo &sInfo = sensors[j];
    int p = sInfo.synchronizations.Size();
    for (int l=0; l<p;)
    {
      int sync = sInfo.synchronizations[l];
      Assert(sync >= 0);
      Assert(syncCountsS[sync] >= 1);
      if (syncCountsW[sync] < 1)
      {
        sInfo.synchronizations.Delete(l);
        p--;
      }
      else
      {
        l++;
      }
    }
  }

  // update nextSyncId
  int maxSync = -1;
  for (int i=nextSyncId-1; i>=0; i--)
    if (syncCountsW[i] >= 1 || syncCountsS[i] >= 1)
    {
      Assert(syncCountsW[i] >= 1);
      Assert(syncCountsW[i] + syncCountsS[i] >= 2);
      maxSync = i;
      break;
    }
  nextSyncId = maxSync + 1;

  // Synchronizations unit - unit
  nextVehId = 0;
  FindArray<int, MemAllocLocal<int, 256> > ids;
  // create the list of all used vehicle ids, update nextVehId
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    for (int j=0; j<gInfo.units.Size(); j++)
    {
      int id = gInfo.units[j].id;
      ids.AddUnique(id);
      saturateMax(nextVehId, id + 1);
    }
  }
  for (int j=0; j<emptyVehicles.Size(); j++)
  {
    int id = emptyVehicles[j].id;
    ids.AddUnique(id);
    saturateMax(nextVehId, id + 1);
  }
  
  // remove the synchronizations to units not present
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    for (int j=0; j<gInfo.units.Size(); j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      for (int k=uInfo.synchronizations.Size()-1; k>=0; k--)
      {
        if (ids.Find(uInfo.synchronizations[k]) < 0) uInfo.synchronizations.DeleteAt(k);
      }
    }
  }
  for (int j=0; j<emptyVehicles.Size(); j++)
  {
    ArcadeUnitInfo &uInfo = emptyVehicles[j];
    for (int k=uInfo.synchronizations.Size()-1; k>=0; k--)
    {
      if (ids.Find(uInfo.synchronizations[k]) < 0) uInfo.synchronizations.DeleteAt(k);
    }
  }
  // avoid references to unused vehicles also from sensors
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    for (int j=0; j<gInfo.sensors.Size(); j++)
    {
      ArcadeSensorInfo &sInfo = gInfo.sensors[j];
      if (sInfo.activationBy == ASAVehicle && ids.Find(sInfo.idVehicle) < 0)
      {
        sInfo.activationBy = ASANone;
        sInfo.idVehicle = -1;
      }
    }
  }
  for (int i=0; i<sensors.Size(); i++)
  {
    ArcadeSensorInfo &sInfo = sensors[i];
    if (sInfo.activationBy == ASAVehicle && ids.Find(sInfo.idVehicle) < 0)
    {
      sInfo.activationBy = ASANone;
      sInfo.idVehicle = -1;
    }
  }
}

bool ArcadeTemplate::IsEmpty()
{
  // FindPlayer will find ambient ("independent agents") as well
  ArcadeUnitInfo *uInfo = FindPlayer(); 
  if (uInfo) return false;

  return groups.Size() == 0;
}
bool ArcadeTemplate::IsConsistent(Display *disp, bool multiplayer, bool showErrors)
{
  int nPlayers1 = 0;
  int side1;
  int nWestGroups = 0;
  int nEastGroups = 0;
  int nGuerrilaGroups = 0;
  int nCivilianGroups = 0;
  int nLogicGroups = 0;

  int i, n = groups.Size();
  for (i=0; i<n; i++)
  {
    ArcadeGroupInfo &info = groups[i];
    switch (info.side)
    {
    case TWest:
      nWestGroups++;
      break;
    case TEast:
      nEastGroups++;
      break;
    case TGuerrila:
      nGuerrilaGroups++;
      break;
    case TCivilian:
      nCivilianGroups++;
      break;
    case TLogic:
      nLogicGroups++;
      break;
    default:
      Fail("Side !!!");
      RptF("Side %d",(int)info.side);
      break;
    }
    int nUnits = 0;
    for (int j=0; j<info.units.Size(); j++)
    {
      ArcadeUnitInfo &uInfo = info.units[j];
      switch (uInfo.player)
      {
      case APPlayerCommander:
      case APPlayerDriver:
      case APPlayerGunner:
        nPlayers1++;
        side1 = uInfo.side;
        break;
      }
      Ref<EntityType> eType = VehicleTypes.New(uInfo.vehicle);
      const EntityAIType *type = dynamic_cast<EntityAIType *>(eType.GetRef());
      if( !type )
      {
        RptF("Type %s is not VehicleType",cc_cast(uInfo.vehicle));
        continue;
      }
      if (type->HasDriver()) nUnits++;
      if (type->HasObserver()) nUnits++;
      if (type->HasGunner()) nUnits++;
    }
    Assert(nUnits > 0);
  }
  for (int i=0; i<emptyVehicles.Size(); i++)
  {
    ArcadeUnitInfo &uInfo = emptyVehicles[i];
    switch (uInfo.player)
    {
    case APPlayerCommander:
    case APPlayerDriver:
    case APPlayerGunner:
      nPlayers1++;
      side1 = uInfo.side;
      break;
    }
  }
  
  Assert(nPlayers1 <= 1);
  if (!multiplayer && nPlayers1 == 0)
  {
    if (showErrors)
    {
      WarningMessage(LocalizeString(IDS_MSG_NO_PLAYER));
    }
#if !_ENABLE_CHEATS || _VBS2 // avoid real time editor freeze
    return false;
#endif
  }

  if
  (
    nWestGroups > MaxGroups ||
    nEastGroups > MaxGroups ||
    nGuerrilaGroups > MaxGroups ||
    nCivilianGroups > MaxGroups ||
    nLogicGroups > MaxGroups
  )
  {
    if (showErrors)
    {
      WarningMessage(LocalizeString(IDS_MSG_LOT_GROUPS), MaxGroups);
    }
    return false;
  }

  return true;
}

bool ArcadeTemplate::CheckObjectIds()
{
  bool ret = true;
  for (int i=0; i<groups.Size(); i++)
    if (!groups[i].CheckObjectIds()) ret = false;
  for (int i=0; i<sensors.Size(); i++)
    if (!sensors[i].CheckObjectIds()) ret = false;
  return ret;
}

void ArcadeTemplate::RepairObjectIds()
{
  for (int i=0; i<groups.Size(); i++)
    groups[i].RepairObjectIds();
  for (int i=0; i<sensors.Size(); i++)
    sensors[i].RepairObjectIds();
}

/*!
  \patch_internal 1.01 Date 06/19/2001 by Jirka - added check of required AddOns
*/
void ArcadeTemplate::RequiredAddons(FindArrayRStringCI &addOns, RString worldName)
{
  for (int i=0; i<groups.Size(); i++) groups[i].RequiredAddons(addOns);
  for (int i=0; i<emptyVehicles.Size(); i++) emptyVehicles[i].RequiredAddons(addOns);
  // island/world - check owner of the island config
  ConstParamEntryPtr entry = (Pars>>"CfgWorlds").FindEntry(worldName);
  if (entry)
  {
    const RStringB &owner = entry->GetOwnerName();
    if (owner.GetLength()>0)
    {
      addOns.AddUnique(owner);
    }
  }
}

void CheckPatch(FindArrayRStringCI &addOns, FindArrayRStringCI &missing)
{
  ConstParamEntryPtr patches = Pars.FindEntry("CfgPatches");
  if (!patches)
  {
    missing = addOns;
    RptF("Missing addons detected:");
    for (int i=0; i<missing.Size(); i++)
    {
      RptF("  %s", (const char *)missing[i]);
    }
    return;
  }
  // activate all addons requested by given mission
  GWorld->ActivateAddons(addOns,RString());

  int m = patches->GetEntryCount();

  for (int i=0; i<addOns.Size(); i++)
  {
    RString addOn = addOns[i];
    bool found = false;
    for (int j=0; j<m; j++)
    {
      if (stricmp(addOn, patches->GetEntry(j).GetName()) == 0)
      {
        found = true; break;
      }
    }
    if (!found) missing.Add(addOn);
  }
  if (missing.Size() > 0)
  {
    RptF("Missing addons detected:");
    for (int i=0; i<missing.Size(); i++)
    {
      RptF("  %s", (const char *)missing[i]);
    }
  }
}

/*!
\patch 5153 Date 4/17/2007 by Jirka
- Fixed: UI - Missing addons error message - show the list of missing addons
*/

/// display message about missing addons
RString ReportMissingAddons(FindArrayRStringCI missing)
{
  RString message = LocalizeString(IDS_MSG_ADDON_MISSING);
#ifndef _XBOX
  RString addons;
  for (int i=0; i<missing.Size(); i++)
  {
    if (!addons.IsEmpty()) addons = addons + RString(", ");
    addons = addons + missing[i];
  }
  message = message + addons;
  CommandingPipeRespond(RString("Missing_addons ")+addons);
#endif
  return message;
}

/// check if some addons are missing, display message if so
bool CheckMissingAddons(FindArrayRStringCI &addOns)
{
  FindArrayRStringCI missing;
  CheckPatch(addOns, missing);
  if (missing.Size() > 0)
  {
    RString message = ReportMissingAddons(missing);
    WarningMessage(message);
    return false;
  }
  return true;
}

/*!
\patch 1.82 Date 8/15/2002 by Ondra
- Fixed: Addon was not removed from addons[] list
when last unit using that addon was removed in the mission editor.
*/

void ArcadeTemplate::ScanRequiredAddons(RString worldName)
{
  FindArrayRStringCI oldAddOnsAuto = addOnsAuto;
  addOnsAuto.Resize(0);
  RequiredAddons(addOnsAuto,worldName);
  // check which items in oldAddOnsAuto are no longer listed in addOnsAuto
  // such items can be removed from addon list
  for (int i=0; i<oldAddOnsAuto.Size(); i++)
  {
    RString old = oldAddOnsAuto[i];
    if (addOnsAuto.Find(old)<0)
    {
      // was listed in old list, but is not in new
      // we can remove it from the required list as 
      // it might be listed several times?
      addOns.Delete(old);
    }
  }
  for (int i=0; i<addOnsAuto.Size(); i++)
  {
    addOns.AddUnique(addOnsAuto[i]);
  }
}

/*!
\patch 1.30 Date 11/03/2001 by Jirka
- Fixed: Check of AddOns in multiplayer game
\patch 1.63 Date 6/2/2002 by Ondra
- Improved: Addons added by mission editor as required are also removed
by the mission editor.
*/


LSError ArcadeTemplate::Serialize(ParamArchive &ar)
{
  ATSParams *params = (ATSParams *)ar.GetParams();
  if (ar.IsSaving())
  {
    // ADDED - AddOns check
    // hack to enable scripted missions to work in MP
    if (!params->avoidScanAddons)
      ScanRequiredAddons(Glob.header.worldname);
    // addOns.Resize(0);
    // items from this list can be removed if necessary
    CHECK(ar.SerializeArray("addOns", addOns, 1))
    CHECK(ar.SerializeArray("addOnsAuto", addOnsAuto, 1))

    if (!params->avoidCheckSync)
    {
      CheckSynchro(); // remove invalid synchronizations
      Compact();
    }
    RepairObjectIds();
  }
  else if (ar.GetPass() == ParamArchive::PassFirst)
  {
    // ADDED - AddOns check
    addOns.Resize(0);
    missingAddOns.Resize(0);
    CHECK(ar.SerializeArray("addOns", addOns, 1))
    CheckPatch(addOns, missingAddOns);
    if (missingAddOns.Size() > 0)
    {
      return LSNoAddOn;
    }

    if (!params)
    {
      Fail("Params needed");
      return LSStructure;
    }
    FindArrayRStringCI addOnsBackup = addOns;
    Clear();
    addOns = addOnsBackup;
    params->nextSyncId = 0;
    params->nextVehId = 0;
  }

  CHECK(ar.Serialize("showHUD", showHUD, 8, true))
  CHECK(ar.Serialize("showMap", showMap, 8, true))
  CHECK(ar.Serialize("showWatch", showWatch, 8, true))
  CHECK(ar.Serialize("showCompass", showCompass, 8, true))
  CHECK(ar.Serialize("showNotepad", showNotepad, 8, true))
  CHECK(ar.Serialize("showGPS", showGPS, 8, false))
  CHECK(ar.Serialize("randomSeed", randomSeed, 1, 1))

  CHECK(ar.Serialize("Intel", intel, 7))
  CHECK(ar.Serialize("Groups", groups, 1))
  CHECK(ar.Serialize("Vehicles", emptyVehicles, 1))
  CHECK(ar.Serialize("Markers", markers, 1))
  CHECK(ar.Serialize("Sensors", sensors, 1))

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    Assert(params);
    nextSyncId = params->nextSyncId;
    nextVehId = params->nextVehId;
    if (!params->avoidCheckSync)
    {
      CheckSynchro(); // remove invalid synchronizations
      Compact();
    }
    if (!params->avoidCheckIds && !CheckObjectIds())
    {
      WarningMessage(LocalizeString(IDS_BAD_OBJECT_ID),(const char *)Localize(intel.briefingName));
    }
  }
  return LSOK;
}

ArcadeUnitInfo *ArcadeTemplate::FindUnit(int id, int &idGroup, int &idUnit)
{
  int i, n = groups.Size();
  for (i=0; i<n; i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];

    int j, m = gInfo.units.Size();
    for (j=0; j<m; j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      if (uInfo.id == id)
      {
        idGroup = i;
        idUnit = j;
        return &uInfo;
      }
    }
  }
  n = emptyVehicles.Size();
  for (i=0; i<n; i++)
  {
    ArcadeUnitInfo &uInfo = emptyVehicles[i];
    if (uInfo.id == id)
    {
      idGroup = -1;
      idUnit = i;
      return &uInfo;
    }
  }
  idGroup = -1;
  idUnit = -1;
  return NULL;
}

ArcadeUnitInfo *ArcadeTemplate::FindPlayer()
{
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];

    for (int j=0; j<gInfo.units.Size(); j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      switch (uInfo.player)
      {
      case APPlayerCommander:
      case APPlayerDriver:
      case APPlayerGunner:
        return &uInfo;
      }
    }
  }

  // ambient life players are supported now
  for (int i=0; i<emptyVehicles.Size(); i++)
  {
    ArcadeUnitInfo &uInfo = emptyVehicles[i];
    switch (uInfo.player)
    {
    case APPlayerCommander:
    case APPlayerDriver:
    case APPlayerGunner:
      return &uInfo;
    }
  }

  return NULL;
}

ArcadeGroupInfo *ArcadeTemplate::FindPlayerGroup()
{
  int i, n = groups.Size();
  for (i=0; i<n; i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];

    int j, m = gInfo.units.Size();
    for (j=0; j<m; j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      switch (uInfo.player)
      {
      case APPlayerCommander:
      case APPlayerDriver:
      case APPlayerGunner:
        return &gInfo;
      }
    }
  }

  return NULL;
}

ArcadeMarkerInfo *ArcadeTemplate::FindMarker(const char *name)
{
  for (int i=0; i<markers.Size(); i++)
  {
    ArcadeMarkerInfo &mInfo = markers[i];
    if (stricmp(name, mInfo.name) == 0)
    {
      return &mInfo;
    }
  }

  return NULL;
}

ArcadeUnitInfo *ArcadeTemplate::FindVehicle(const char *name)
{
  for (int i=0; i<emptyVehicles.Size(); i++)
  {
    ArcadeUnitInfo &uInfo = emptyVehicles[i];
    if (stricmp(name, uInfo.name) == 0)
    {
      return &uInfo;
    }
  }
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    for (int j=0; j<gInfo.units.Size(); j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      if (stricmp(name, uInfo.name) == 0)
      {
        return &uInfo;
      }
    }
  }
  return NULL;
}

ArcadeSensorInfo *ArcadeTemplate::FindSensor(const char *name)
{
  for (int i=0; i<sensors.Size(); i++)
  {
    ArcadeSensorInfo &sInfo = sensors[i];
    if (stricmp(name, sInfo.name) == 0)
    {
      return &sInfo;
    }
  }
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    for (int j=0; j<gInfo.sensors.Size(); j++)
    {
      ArcadeSensorInfo &sInfo = gInfo.sensors[j];
      if (stricmp(name, sInfo.name) == 0)
      {
        return &sInfo;
      }
    }
  }
  return NULL;
}

void ArcadeTemplate::Clear()
{
  groups.Clear();
  emptyVehicles.Clear();
  sensors.Clear();
  markers.Clear();
  
  showHUD = true;
  showMap = true;
  showWatch = true;
  showCompass = true;
  showNotepad = true;
  showGPS = false;

  nextSyncId = 0;
  nextVehId = 0;

  addOns.Clear();
  missingAddOns.Clear();

  // assign a new random seed
  randomSeed = toLargeInt(GRandGen.RandomValue()*0x1000000)+3;
}

void ArcadeTemplate::GroupDelete(int ig)
{
  Assert(ig >= 0);
  if (ig >= 0)
  {
    groups.Delete(ig);
  }
}

void ArcadeTemplate::UnitDelete(int ig, int iu)
{
  int idVehicle = -1;
  if (ig == -1)
  {
    idVehicle = emptyVehicles[iu].id;
    emptyVehicles.Delete(iu);
  }
  else
  {
    ArcadeGroupInfo &gInfo = groups[ig];
    idVehicle = gInfo.units[iu].id;
    gInfo.units.Delete(iu);
    if (gInfo.units.Size() == 0)
      groups.Delete(ig);
    else
      SelectLeader(gInfo);
  }
  // delete obsolete synchronizations
  if (idVehicle >= 0)
  {
    for (int i=0; i<emptyVehicles.Size(); i++)
    {
      ArcadeUnitInfo &unit = emptyVehicles[i];
      unit.synchronizations.DeleteAllKey(idVehicle);
    }
    for (int i=0; i<groups.Size(); i++)
    {
      ArcadeGroupInfo &group = groups[i];
      for (int j=0; j<group.units.Size(); j++)
      {
        ArcadeUnitInfo &unit = group.units[j];
        unit.synchronizations.DeleteAllKey(idVehicle);
      }
    }
  }
}

void ArcadeTemplate::WaypointDelete(int ig, int iw)
{
  ArcadeGroupInfo &gInfo = groups[ig];
  gInfo.waypoints.Delete(iw);
}

bool ArcadeTemplate::UnitChangeGroup(int ig, int iu, int ignew)
{
  if (ignew >= 0)
  {
    if (ignew == ig) return false;
    // remove from old group
    ArcadeGroupInfo &gInfoOld = groups[ig];
    ArcadeUnitInfo uInfo = gInfoOld.units[iu];
    gInfoOld.units.Delete(iu);
    if (gInfoOld.units.Size() == 0)
    {
      groups.Delete(ig);
      if (ignew > ig) ignew--;
    }
    else
    {
      SelectLeader(gInfoOld);
    }
    // insert into new group
    ArcadeGroupInfo &gInfoNew = groups[ignew];
    gInfoNew.units.Add(uInfo);
    // select leader
    SelectLeader(gInfoNew);
    return true;
  }
  else
  {
    // split group
    ArcadeGroupInfo &gInfoOld = groups[ig];
    if (gInfoOld.units.Size() <= 1) return false;
    // remove from old group
    ArcadeUnitInfo uInfo = gInfoOld.units[iu];
    gInfoOld.units.Delete(iu);
    SelectLeader(gInfoOld);
    // insert into new group
    ArcadeGroupInfo gInfoNew;
    gInfoNew.units.Add(uInfo);
    gInfoNew.side = uInfo.side;
    // select leader
    SelectLeader(gInfoNew);

    groups.Add(gInfoNew);
    return true;
  }
}

void ArcadeTemplate::WaypointChangeSynchro(int ig, int iw, int ig1, int iw1)
{
  if (ig1 >= 0)
  {
    if (ig1 != ig)
    {
      // synchronization
      ArcadeGroupInfo &gInfo1 = groups[ig];
      ArcadeWaypointInfo &wInfo1 = gInfo1.waypoints[iw];
      ArcadeGroupInfo &gInfo2 = groups[ig1];
      ArcadeWaypointInfo &wInfo2 = gInfo2.waypoints[iw1];
      
      // check if synchronization does not exist
      int found = 0;
      for (int i=0; i<wInfo1.synchronizations.Size(); i++)
      {
        int sync = wInfo1.synchronizations[i];
        for (int j=0; j<wInfo2.synchronizations.Size(); j++)
        {
          if (wInfo2.synchronizations[j] == sync) found++;
        }
      }

      if (found == 0)
      {
        int sync = nextSyncId++;
        wInfo1.synchronizations.Add(sync);
        wInfo2.synchronizations.Add(sync);
      }
      else
      {
        Assert(found == 1);
        // TODO: MessageBox - both waypoints are synchronized already
      }
    }
    else
    {
      // TODO: MessageBox - cannot synchronize waypoints of the same group
    }
  }
  else
  {
    // split synchronization
    ArcadeGroupInfo &gInfo = groups[ig];
    ArcadeWaypointInfo &wInfo = gInfo.waypoints[iw];
    wInfo.synchronizations.Clear();
    CheckSynchro();
  }
}

bool ArcadeTemplate::UnitChangePosition(int ig, int iu, Vector3Val pos)
{
  ArcadeUnitInfo* uInfo = NULL;
  if (ig == -1)
  {
    Assert(iu >= 0 && iu < emptyVehicles.Size());
    if (iu < 0 || iu >= emptyVehicles.Size()) return false;
    uInfo = &emptyVehicles[iu];
  }
  else
  {
    Assert(ig >= 0 && ig < groups.Size());
    if (ig < 0 || ig >= groups.Size()) return false;
    ArcadeGroupInfo& gInfo = groups[ig];

    Assert(iu >= 0 && iu < gInfo.units.Size());
    if (iu < 0 || iu >= gInfo.units.Size()) return false;
    uInfo = &gInfo.units[iu];
  }

  if (!uInfo) return false;
  bool changedEnough = (uInfo->position - pos).SquareSizeXZ() >= Square(2);
  uInfo->position = pos;
  return changedEnough;
}

bool ArcadeTemplate::GroupChangePosition(int ig, int iu, Vector3Val pos)
{
  Assert(ig >= 0);
  ArcadeGroupInfo &gInfo = groups[ig];
  ArcadeUnitInfo &uInfo = gInfo.units[iu];

  Vector3 diff = pos - uInfo.position;
  bool changedEnough = diff.SquareSizeXZ() >= Square(2);

  for (int i=0; i<gInfo.units.Size(); i++)
  {
    ArcadeUnitInfo &uInfo = gInfo.units[i];
    Point3 pos = uInfo.position + diff;
    pos[1] = GLOB_LAND->RoadSurfaceY(pos[0], pos[2]);
    uInfo.position = pos;
  }
  
  return changedEnough;
}

bool ArcadeTemplate::WaypointChangePosition(int ig, int iw, Vector3Val pos)
{
  ArcadeGroupInfo &gInfo = groups[ig];
  ArcadeWaypointInfo &wInfo = gInfo.waypoints[iw];

  bool changedEnough = (wInfo.position - pos).SquareSizeXZ() >= Square(2);
  wInfo.position = pos;
  return changedEnough;
}

void ArcadeTemplate::UnitUpdate
(
  int &ig, int &iu,
  ArcadeUnitInfo &uInfo
)
{
  ArcadeUnitInfo *info = NULL;
  if (iu < 0)
  {
    // insert unit
    Assert(ig < 0);
    if (uInfo.side == TEmpty || uInfo.side == TAmbientLife)
    {
      // empty vehicles
      ig = -1;
      uInfo.id = -1;      
      iu = emptyVehicles.Add(uInfo);
      info = &emptyVehicles[iu];
    }
    else
    {
      // Find group to insert into
      float minDist2 = Square(100);

      int i, n = groups.Size();
      for (i=0; i<n; i++)
      {
        ArcadeGroupInfo &gInfo = groups[i];
        if (gInfo.side != uInfo.side) continue;
        int j, m = gInfo.units.Size(), jLeader = -1;
        for (j=0; j<m; j++)
        {
          ArcadeUnitInfo &unit = gInfo.units[j];
          if (unit.leader)
          {
            jLeader = j;
            break;
          }
        }
        if (jLeader >= 0)
        {
          ArcadeUnitInfo &unit = gInfo.units[jLeader];
          float dist2 = (unit.position - uInfo.position).SquareSizeXZ();
          if (dist2 <= minDist2)
          {
            minDist2 = dist2;
            ig = i;
          }
        }
      }
      if (ig < 0)
      {
        ig = groups.Add();
        groups[ig].side = uInfo.side;
      }
      uInfo.id = -1;      
      ArcadeGroupInfo &gInfo = groups[ig];
      iu = gInfo.units.Add(uInfo);
      info = &gInfo.units[iu];
    }
  }
  else
  {
    // edit unit
    if (uInfo.side == TEmpty || uInfo.side == TAmbientLife)
    {
      // empty vehicles
      ig = -1;
      emptyVehicles[iu] = uInfo;
      info = &emptyVehicles[iu];
    }
    else
    {
      Assert(ig >= 0);
      ArcadeGroupInfo &gInfo = groups[ig];
      gInfo.units[iu] = uInfo;
      info = &gInfo.units[iu];
    }
  }

  if (info->player == APPlayerCommander || info->player == APPlayerDriver || info->player == APPlayerGunner)
  {
    ArcadeUnitPlayer p = info->player;
    info->player = APNonplayable;
    // reset player flag for previous player
    ArcadeUnitInfo *unit = FindPlayer();
    if (unit) unit->player = APNonplayable;
    // set player flag
    info->player = p;

    Assert(info->player != APNonplayable && info->player != APPlayableCDG);
    if (info->player == p)
      Glob.header.playerSide = (TargetSide)info->side;
  }

  RString iconName = Pars >> "CfgVehicles" >> info->vehicle >> "icon";
  info->icon = GlobLoadTexture(GetVehicleIcon(iconName));
  info->size = Pars >> "CfgVehicles" >> info->vehicle >> "mapSize";
  if (info->id < 0) info->id = nextVehId++;
  if (ig >= 0)
  {
    ArcadeGroupInfo &gInfo = groups[ig];
    SelectLeader(gInfo);
  }
}

void ArcadeTemplate::WaypointUpdate
(
  int ig, int iw, int &iwnew,
  ArcadeWaypointInfo &waypoint
)
{
  ArcadeGroupInfo &gInfo = groups[ig];
  if (iw < 0)
  {
    DoAssert(iwnew >= 0);
    gInfo.waypoints.Insert(iwnew, waypoint);
  }
  else if (iwnew == iw || iwnew < 0)
  {
    iwnew = iw;
    gInfo.waypoints[iw] = waypoint;
  }
  else
  {
    gInfo.waypoints.Delete(iw);
    if (iwnew > iw) iwnew--;
    gInfo.waypoints.Insert(iwnew, waypoint);
  }
}

void ArcadeTemplate::SensorUpdate
(
  int ig, int index,
  ArcadeSensorInfo &sInfo
)
{
  if (ig < 0)
  {
    if (index < 0)
    {
      sensors.Add(sInfo);
    }
    else
    {
      sensors[index] = sInfo;
    }
  }
  else
  {
    ArcadeGroupInfo &gInfo = groups[ig];
    if (index < 0)
    {
      index = gInfo.sensors.Add(sInfo);
    }
    else
    {
      gInfo.sensors[index] = sInfo;
    }
  }
}

void ArcadeTemplate::SensorDelete(int ig, int index)
{
  if (ig < 0)
    sensors.Delete(index);
  else
  {
    ArcadeGroupInfo &gInfo = groups[ig];
    gInfo.sensors.Delete(index);
  }
}

bool ArcadeTemplate::SensorChangePosition(int ig, int index, Vector3Val pos)
{
  ArcadeSensorInfo *sInfo = NULL;
  if (ig < 0)
  {
    sInfo = &sensors[index];
  }
  else
  {
    ArcadeGroupInfo& gInfo = groups[ig];
    sInfo = &gInfo.sensors[index];
  }

  bool changedEnough = (sInfo->position - pos).SquareSizeXZ() >= Square(2);
  sInfo->position = pos;
  return changedEnough;
}

bool ArcadeTemplate::SensorChangeGroup(int ig, int index, int ignew)
{
  if (ignew == ig) return false;
  
  ArcadeSensorInfo sInfo;
  if (ig >= 0)
  {
    // remove from old group
    ArcadeGroupInfo &gInfoOld = groups[ig];
    sInfo = gInfoOld.sensors[index];
    gInfoOld.sensors.Delete(index);
  }
  else
  {
    // remove from flags
    sInfo = sensors[index];
    sensors.Delete(index);
  }

  if (ignew >= 0)
  {
    // insert into new group
    ArcadeGroupInfo &gInfoNew = groups[ignew];
    sInfo.activationBy = ASAGroup;
    sInfo.idVisitorObj = VisitorObjId();
    sInfo.idObject = ObjectId();
    sInfo.idVehicle = -1;
    gInfoNew.sensors.Add(sInfo);
  }
  else
  {
    if (sInfo.activationBy == ASAGroup)
      sInfo.activationBy = ASANone;
    sensors.Add(sInfo);
  }
  return true;
}

void ArcadeTemplate::SensorChangeVehicle(int ig, int index, int id)
{
  ArcadeSensorInfo *sInfo = NULL;
  if (ig >= 0)
  {
    ArcadeGroupInfo &gInfo = groups[ig];
    sInfo = &gInfo.sensors[index];
  }
  else
  {
    sInfo = &sensors[index];
  }
  sInfo->idVisitorObj = VisitorObjId();
  sInfo->idObject = ObjectId();
  sInfo->idVehicle = id;
  sInfo->activationBy = ASAVehicle;
  if (ig >= 0) SensorChangeGroup(ig, index, -1);
}

void ArcadeTemplate::SensorChangeStatic(int ig, int index, const VisitorObjId &id, Vector3Val pos)
{
  ArcadeSensorInfo *sInfo = NULL;
  if (ig >= 0)
  {
    ArcadeGroupInfo &gInfo = groups[ig];
    sInfo = &gInfo.sensors[index];
  }
  else
  {
    sInfo = &sensors[index];
  }
  sInfo->idVisitorObj = id;
  sInfo->idObject = id == VisitorObjId() ? ObjectId() : GLandscape->GetObjectId(id, pos);
  sInfo->idVehicle = -1;
  sInfo->activationBy = ASAStatic;
  if (ig >= 0) SensorChangeGroup(ig, index, -1);
}

void ArcadeTemplate::SensorChangeSynchro(int ig, int index, int ig1, int iw1)
{
  ArcadeSensorInfo *sInfo = NULL;
  if (ig >= 0)
  {
    ArcadeGroupInfo &gInfo = groups[ig];
    sInfo = &gInfo.sensors[index];
  }
  else
  {
    sInfo = &sensors[index];
  }
  Assert(sInfo);

  if (ig1 >= 0)
  {
    ArcadeGroupInfo &gInfo = groups[ig1];
    ArcadeWaypointInfo &wInfo = gInfo.waypoints[iw1];

    // check if synchronization does not exist
    int found = 0;
    for (int i=0; i<wInfo.synchronizations.Size(); i++)
    {
      int sync = wInfo.synchronizations[i];
      for (int j=0; j<sInfo->synchronizations.Size(); j++)
      {
        if (sInfo->synchronizations[j] == sync) found++;
      }
    }

    if (found == 0)
    {
      int sync = nextSyncId++;
      wInfo.synchronizations.Add(sync);
      sInfo->synchronizations.Add(sync);
    }
    else
    {
      Assert(found == 1);
      // TODO: MessageBox - waypoint and sensor are synchronized already
    }
  }
  else
  {
    // split synchronization
    sInfo->synchronizations.Clear();
    CheckSynchro();
  }
}

void ArcadeTemplate::UnitChangeSynchro(int ig, int index, int ig1, int index1)
{
  ArcadeUnitInfo *unit1 = NULL;
  if (ig >= 0)
  {
    ArcadeGroupInfo &group = groups[ig];
    unit1 = &group.units[index];
  }
  else
  {
    unit1 = &emptyVehicles[index];
  }

  if (index1 >= 0)
  {
    // synchronize / remove synchronization
    ArcadeUnitInfo *unit2 = NULL;
    if (ig1 >= 0)
    {
      ArcadeGroupInfo &group = groups[ig1];
      unit2 = &group.units[index1];
    }
    else
    {
      unit2 = &emptyVehicles[index1];
    }

    // synchronization is symmetric
    if (unit1->synchronizations.FindKey(unit2->id) >= 0)
    {
      // remove synchronization
      unit1->synchronizations.DeleteKey(unit2->id);
      unit2->synchronizations.DeleteKey(unit1->id);
    }
    else
    {
      unit1->synchronizations.AddUnique(unit2->id);
      unit2->synchronizations.AddUnique(unit1->id);
    }
  }
  else
  {
    // remove all synchronizations
    for (int i=0; i<unit1->synchronizations.Size(); i++)
    {
      int id = unit1->synchronizations[i];
      int idDummy, idGroupDummy;
      ArcadeUnitInfo *unit = FindUnit(id, idDummy, idGroupDummy);
      if (unit) unit->synchronizations.DeleteKey(unit1->id);
    }
    unit1->synchronizations.Clear();
  }
}

void ArcadeTemplate::MarkerUpdate(int index, ArcadeMarkerInfo &mInfo)
{
  if (index < 0)
  {
    index = markers.Add(mInfo);
  }
  else
  {
    if (index >= markers.Size())
      markers.Resize(index + 1);
    markers[index] = mInfo;
  }

  ParamEntryVal cls = Pars >> "CfgMarkers" >> markers[index].type;
  markers[index].icon = GlobLoadTexture
  (
    GetPictureName(cls >> "icon")
  );
  markers[index].size = cls >> "size";
}

void ArcadeTemplate::MarkerDelete(int index)
{
  markers.Delete(index);
}

bool ArcadeTemplate::MarkerChangePosition(int index, Vector3Val pos)
{
  ArcadeMarkerInfo &mInfo = markers[index];

  bool changedEnough = (mInfo.position - pos).SquareSizeXZ() >= Square(2);
  mInfo.position = pos;
  return changedEnough;
}

void ArcadeTemplate::UnitAddMarker(int ig, int index, int indexMarker)
{
  ArcadeUnitInfo *uInfo = NULL;
  if (ig < 0)
  {
    if ( index<emptyVehicles.Size() ) uInfo = &emptyVehicles[index];
  }
  else
  {
    if ( ig < groups.Size() )
    {
      ArcadeGroupInfo &gInfo = groups[ig];
      if ( index < gInfo.units.Size() ) uInfo = &gInfo.units[index];
    }
  }
  if (!uInfo) return;
  RString name = markers[indexMarker].name;
  int n = uInfo->markers.Size();
  for (int i=0; i<n; i++)
  {
    if (stricmp(uInfo->markers[i], name) == 0)
    {
      return; // already in list
    }
  }
  uInfo->markers.Add(name);
}

void ArcadeTemplate::RemoveMarker(int indexMarker)
{
  RString name = markers[indexMarker].name;
  int n = groups.Size();
  for (int i=0; i<n; i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    int m = gInfo.units.Size();
    for (int j=0; j<m; j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      int o = uInfo.markers.Size();
      for (int k=0; k<o; k++)
      {
        if (stricmp(uInfo.markers[k], name) == 0)
        {
          uInfo.markers.Delete(k);
          break; // max once in each list
        }
      }
    }
  }
  int m = emptyVehicles.Size();
  for (int j=0; j<m; j++)
  {
    ArcadeUnitInfo &uInfo = emptyVehicles[j];
    int o = uInfo.markers.Size();
    for (int k=0; k<o; k++)
    {
      if (stricmp(uInfo.markers[k], name) == 0)
      {
        uInfo.markers.Delete(k);
        break; // max once in each list
      }
    }
  }
}

void ArcadeTemplate::AddGroup(ParamEntryPar cls, Vector3Par position)
{
  int ig = groups.Add();
  ArcadeGroupInfo &group = groups[ig];
  for (int i=0; i<cls.GetEntryCount(); i++)
  {
    ParamEntryVal entry = cls.GetEntry(i);
    if (!entry.IsClass()) continue;
    int iu = group.units.Add();
    ArcadeUnitInfo &unit = group.units[iu];
    int side = entry >> "side";
    unit.side = (TargetSide)side;
    unit.vehicle = entry >> "vehicle";
    RString iconName = Pars >> "CfgVehicles" >> unit.vehicle >> "icon";
    unit.icon = GlobLoadTexture(GetVehicleIcon(iconName));
    unit.size = Pars >> "CfgVehicles" >> unit.vehicle >> "mapSize";

    RStringB rank = entry >> "rank";
    unit.rank = GetEnumValue<Rank>(rank);
    unit.skill = RankToSkill(unit.rank);
    unit.position = position;
    float offset = (entry >> "position")[0];
    unit.position[0] += offset;
    offset = (entry >> "position")[1];
    unit.position[2] += offset;
    offset = (entry >> "position")[2];
    unit.position[1] += offset;
    unit.id = nextVehId++;
  }
  SelectLeader(group);
}

struct ConversionItem
{
  bool used;
  int newIndex;
};
TypeIsSimple(ConversionItem)

/// compact vehicle and synchronization IDs to use small numbers
void ArcadeTemplate::Compact()
{
  AutoArray<ConversionItem> table;
  
  // vehicle IDs

  // find what IDs are used
  table.Resize(nextVehId);
  for (int i=0; i<nextVehId; i++)
  {
    table[i].used = false;
    table[i].newIndex = -1;
  }
  for (int i=0; i<emptyVehicles.Size(); i++)
  {
    ArcadeUnitInfo &uInfo = emptyVehicles[i];
    Assert(uInfo.id >= 0);
    Assert(uInfo.id < nextVehId);
    table[uInfo.id].used = true;
  }
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    for (int j=0; j<gInfo.units.Size(); j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      Assert(uInfo.id >= 0);
      Assert(uInfo.id < nextVehId);
      table[uInfo.id].used = true;
    }
  }

  // create a mapping table
  int index = 0;
  for (int i=0; i<nextVehId; i++)
    if (table[i].used) table[i].newIndex = index++;
  nextVehId = index;

  // replace IDs using the table
  for (int i=0; i<emptyVehicles.Size(); i++)
  {
    ArcadeUnitInfo &uInfo = emptyVehicles[i];
    uInfo.id = table[uInfo.id].newIndex;
    for (int j=uInfo.synchronizations.Size()-1; j>=0; j--) 
    {
      if (uInfo.synchronizations[j] >= 0 && uInfo.synchronizations[j] < table.Size())
      {
        uInfo.synchronizations[j] = table[uInfo.synchronizations[j]].newIndex;
      }
      else
      {
        // recover from the wrong synchronization
        uInfo.synchronizations.DeleteAt(j);
        RptF("Wrong unit synchronization fixed in mission %s", cc_cast(intel.briefingName));
      }
    }
  }
  for (int i=0; i<sensors.Size(); i++)
  {
    ArcadeSensorInfo &sInfo = sensors[i];
    if (sInfo.idVehicle >= 0)
    {
      if(sInfo.idVehicle < table.Size())
      {
      sInfo.idVehicle = table[sInfo.idVehicle].newIndex;
    }
      else
      {
        RptF("Wrong vehicle synchronization fixed in mission %s", cc_cast(intel.briefingName));
        sInfo.idVehicle = -1;
  }
    }
  }
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    for (int j=0; j<gInfo.units.Size(); j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      uInfo.id = table[uInfo.id].newIndex;
      for (int k=uInfo.synchronizations.Size()-1; k>=0; k--) 
      {
        if (uInfo.synchronizations[k] >= 0 && uInfo.synchronizations[k] < table.Size())
        {
          uInfo.synchronizations[k] = table[uInfo.synchronizations[k]].newIndex;
        }
        else
        {
          // recover from the wrong synchronization
          uInfo.synchronizations.DeleteAt(k);
          RptF("Wrong unit synchronization fixed in mission %s", cc_cast(intel.briefingName));
        }
      }
    }
    for (int j=0; j<gInfo.sensors.Size(); j++)
    {
      ArcadeSensorInfo &sInfo = gInfo.sensors[j];
      if (sInfo.idVehicle >= 0)
      {
        Assert(sInfo.idVehicle < table.Size());
        sInfo.idVehicle = table[sInfo.idVehicle].newIndex;
      }
    }
    for (int j=0; j<gInfo.waypoints.Size(); j++)
    {
      ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
      if (wInfo.id >= 0)
      {
        Assert(wInfo.id < table.Size());
        wInfo.id = table[wInfo.id].newIndex;
      }
    }
  }

  // synchronization IDs

  // reset the table
  table.Resize(nextSyncId);
  for (int i=0; i<nextSyncId; i++)
  {
    table[i].used = false;
    table[i].newIndex = -1;
  }

  // find what IDs are used
  for (int i=0; i<sensors.Size(); i++)
  {
    ArcadeSensorInfo &sInfo = sensors[i];
    for (int s=0; s<sInfo.synchronizations.Size(); s++)
    {
      int sync = sInfo.synchronizations[s];
      Assert(sync >= 0);
      Assert(sync < nextSyncId);
      table[sync].used = true;
    }
  }
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    for (int j=0; j<gInfo.sensors.Size(); j++)
    {
      ArcadeSensorInfo &sInfo = gInfo.sensors[j];
      for (int s=0; s<sInfo.synchronizations.Size(); s++)
      {
        int sync = sInfo.synchronizations[s];
        Assert(sync >= 0);
        Assert(sync < nextSyncId);
        table[sync].used = true;
      }
    }
    for (int j=0; j<gInfo.waypoints.Size(); j++)
    {
      ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
      for (int s=0; s<wInfo.synchronizations.Size(); s++)
      {
        int sync = wInfo.synchronizations[s];
        Assert(sync >= 0);
        Assert(sync < nextSyncId);
        table[sync].used = true;
      }
    }
  }

  // create a mapping table
  index = 0;
  for (int i=0; i<nextSyncId; i++)
    if (table[i].used) table[i].newIndex = index++;
  nextSyncId = index;

  // replace IDs using the table
  for (int i=0; i<sensors.Size(); i++)
  {
    ArcadeSensorInfo &sInfo = sensors[i];
    for (int s=0; s<sInfo.synchronizations.Size(); s++)
    {
      int sync = sInfo.synchronizations[s];
      sInfo.synchronizations[s] = table[sync].newIndex;
    }
  }
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    for (int j=0; j<gInfo.sensors.Size(); j++)
    {
      ArcadeSensorInfo &sInfo = gInfo.sensors[j];
      for (int s=0; s<sInfo.synchronizations.Size(); s++)
      {
        int sync = sInfo.synchronizations[s];
        sInfo.synchronizations[s] = table[sync].newIndex;
      }
    }
    for (int j=0; j<gInfo.waypoints.Size(); j++)
    {
      ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
      for (int s=0; s<wInfo.synchronizations.Size(); s++)
      {
        int sync = wInfo.synchronizations[s];
        wInfo.synchronizations[s] = table[sync].newIndex;
      }
    }
  }
}

struct TranslationTableItem
{
  RString from;
  RString to;

  TranslationTableItem(){}
  TranslationTableItem(RString f, RString t)
  {
    from = f; to = t;
  }
};
TypeIsMovableZeroed(TranslationTableItem)

class TranslationTable : public AutoArray<TranslationTableItem>
{
  typedef AutoArray<TranslationTableItem> base;
public:
  int Add(RString from, RString to)
  {
    return base::Add(TranslationTableItem(from, to));
  }
  RString Translate(RString from);
};

RString TranslationTable::Translate(RString from)
{
  if (from.GetLength() == 0) return from;
  for (int i=0; i<Size(); i++)
  {
    const TranslationTableItem &item = Get(i);
    if (stricmp(from, item.from) == 0) return item.to;
  }
  return from;
}

void ArcadeTemplate::Merge(ArcadeTemplate &t, Vector3Par offset)
{
  // unique names - ArcadeMarkerInfo::name
  TranslationTable markerNames;
  for (int i=0; i<t.markers.Size(); i++)
  {
    ArcadeMarkerInfo &mInfo = t.markers[i];
    if (mInfo.name.GetLength() == 0) continue;
    if (FindMarker(mInfo.name))
    {
      char name[256];
      for (int l=1; ; l++)
      {
        sprintf(name, "%s_%d", (const char *)mInfo.name, l);
        if (!FindMarker(name)) break;
      }
      markerNames.Add(mInfo.name, name);
    }
  }

  // unique names - ArcadeUnitInfo::text
  TranslationTable unitNames;
  for (int i=0; i<t.emptyVehicles.Size(); i++)
  {
    ArcadeUnitInfo &uInfo = t.emptyVehicles[i];
    if (uInfo.name.GetLength() == 0) continue;
    if (FindVehicle(uInfo.name))
    {
      char name[256];
      for (int l=1; ; l++)
      {
        sprintf(name, "%s_%d", (const char *)uInfo.name, l);
        if (!FindVehicle(name)) break;
      }
      unitNames.Add(uInfo.name, name);
    }
  }
  for (int i=0; i<t.groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = t.groups[i];
    for (int i=0; i<gInfo.units.Size(); i++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[i];
      if (uInfo.name.GetLength() == 0) continue;
      if (FindVehicle(uInfo.name))
      {
        char name[256];
        for (int l=1; ; l++)
        {
          sprintf(name, "%s_%d", (const char *)uInfo.name, l);
          if (!FindVehicle(name)) break;
        }
        unitNames.Add(uInfo.name, name);
      }
    }
  }

  // unique names - ArcadeSensorInfo::name
  TranslationTable sensorNames;
  for (int i=0; i<t.sensors.Size(); i++)
  {
    ArcadeSensorInfo &sInfo = t.sensors[i];
    if (sInfo.name.GetLength() == 0) continue;
    if (FindSensor(sInfo.name))
    {
      char name[256];
      for (int l=1; ; l++)
      {
        sprintf(name, "%s_%d", (const char *)sInfo.name, l);
        if (!FindSensor(name)) break;
      }
      sensorNames.Add(sInfo.name, name);
    }
  }
  for (int i=0; i<t.groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = t.groups[i];
    for (int i=0; i<gInfo.sensors.Size(); i++)
    {
      ArcadeSensorInfo &sInfo = gInfo.sensors[i];
      if (sInfo.name.GetLength() == 0) continue;
      if (FindSensor(sInfo.name))
      {
        char name[256];
        for (int l=1; ; l++)
        {
          sprintf(name, "%s_%d", (const char *)sInfo.name, l);
          if (!FindSensor(name)) break;
        }
        sensorNames.Add(sInfo.name, name);
      }
    }
  }

  // merge templates
  for (int i=0; i<t.groups.Size(); i++)
  {
    int index = groups.Add(t.groups[i]);
    ArcadeGroupInfo &gInfo = groups[index];
    // offset positions
    gInfo.AddOffset(offset);
    // offset synchronizations and vehicle IDs
    for (int j=0; j<gInfo.units.Size(); j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      Assert(uInfo.id >= 0);
      Assert(uInfo.id < t.nextVehId);
      uInfo.id += nextVehId;
#if _VBS2 // retain playable status when copying unit in editor
      if (uInfo.player == APPlayerCommander)
        uInfo.player = APPlayableCDG;
      else if (uInfo.player == APPlayerDriver)
        uInfo.player = APPlayableD;
      else if (uInfo.player == APPlayerGunner)
        uInfo.player = APPlayableG;      
#else
      uInfo.player = APNonplayable; // avoid 2 players
#endif
      uInfo.name = unitNames.Translate(uInfo.name);
      for (int m=0; m<uInfo.markers.Size(); m++)
      {
        uInfo.markers[m] = markerNames.Translate(uInfo.markers[m]);
      }
      // offset vehicle IDs in synchronizations
      for (int k=0; k<uInfo.synchronizations.Size(); k++) uInfo.synchronizations[k] += nextVehId;
    }
    for (int j=0; j<gInfo.sensors.Size(); j++)
    {
      ArcadeSensorInfo &sInfo = gInfo.sensors[j];
      if (sInfo.idVehicle >= 0)
      {
        Assert(sInfo.idVehicle < t.nextVehId);
        sInfo.idVehicle += nextVehId;
      }
      for (int s=0; s<sInfo.synchronizations.Size(); s++)
      {
        Assert(sInfo.synchronizations[s] >= 0);
        Assert(sInfo.synchronizations[s] < t.nextSyncId);
        sInfo.synchronizations[s] += nextSyncId;
      }
      sInfo.name = sensorNames.Translate(sInfo.name);
    }
    for (int j=0; j<gInfo.waypoints.Size(); j++)
    {
      ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
      if (wInfo.id >= 0)
      {
        Assert(wInfo.id < t.nextVehId);
        wInfo.id += nextVehId;
      }
      for (int s=0; s<wInfo.synchronizations.Size(); s++)
      {
        Assert(wInfo.synchronizations[s] >= 0);
        Assert(wInfo.synchronizations[s] < t.nextSyncId);
        wInfo.synchronizations[s] += nextSyncId;
      }
    }
  }
  for (int i=0; i<t.emptyVehicles.Size(); i++)
  {
    int index = emptyVehicles.Add(t.emptyVehicles[i]);
    ArcadeUnitInfo &uInfo = emptyVehicles[index];
    // offset positions
    uInfo.AddOffset(offset);
    // offset vehicle IDs
    Assert(uInfo.id >= 0);
    Assert(uInfo.id < t.nextVehId);
    uInfo.id += nextVehId;
    uInfo.name = unitNames.Translate(uInfo.name);
    for (int m=0; m<uInfo.markers.Size(); m++)
    {
      uInfo.markers[m] = markerNames.Translate(uInfo.markers[m]);
    }
    // offset vehicle IDs in synchronizations
    for (int k=0; k<uInfo.synchronizations.Size(); k++) uInfo.synchronizations[k] += nextVehId;
  }
  for (int i=0; i<t.sensors.Size(); i++)
  {
    int index = sensors.Add(t.sensors[i]);
    // offset positions
    ArcadeSensorInfo &sInfo = sensors[index];
    sInfo.AddOffset(offset);
    // offset synchronizations and vehicle IDs
    if (sInfo.idVehicle >= 0)
    {
      Assert(sInfo.idVehicle < t.nextVehId);
      sInfo.idVehicle += nextVehId;
    }
    for (int s=0; s<sInfo.synchronizations.Size(); s++)
    {
      Assert(sInfo.synchronizations[s] >= 0);
      Assert(sInfo.synchronizations[s] < t.nextSyncId);
      sInfo.synchronizations[s] += nextSyncId;
    }
    sInfo.name = sensorNames.Translate(sInfo.name);
  }
  for (int i=0; i<t.markers.Size(); i++)
  {
    int index = markers.Add(t.markers[i]);
    // offset positions
    ArcadeMarkerInfo &mInfo = markers[index];
    mInfo.AddOffset(offset);
    mInfo.name = markerNames.Translate(mInfo.name);
  }

  nextVehId += t.nextVehId;
  nextSyncId += t.nextSyncId;
}

void ArcadeTemplate::Join(ArcadeTemplate &t)
{
  // no duplicity checks - template was splitted before
  // add groups
  for (int i=0; i<t.groups.Size(); i++)
    groups.Add(t.groups[i]);
  // add empty vehicles
  for (int i=0; i<t.emptyVehicles.Size(); i++)
    emptyVehicles.Add(t.emptyVehicles[i]);
  // add sensors
  for (int i=0; i<t.sensors.Size(); i++)
    sensors.Add(t.sensors[i]);
  // add markers
  for (int i=0; i<t.markers.Size(); i++)
    markers.Add(t.markers[i]);

  saturateMax(nextVehId, t.nextVehId);
  saturateMax(nextSyncId, t.nextSyncId);
}

void ArcadeTemplate::AddOffset(Vector3Par offset)
{
  for (int i=0; i<emptyVehicles.Size(); i++)
    emptyVehicles[i].AddOffset(offset);
  for (int i=0; i<sensors.Size(); i++)
    sensors[i].AddOffset(offset);
  for (int i=0; i<markers.Size(); i++)
    markers[i].AddOffset(offset);
  for (int i=0; i<groups.Size(); i++)
    groups[i].AddOffset(offset);
}

void ArcadeTemplate::Rotate(Vector3Par center, float angle, bool sel)
{
  for (int i=0; i<emptyVehicles.Size(); i++)
    emptyVehicles[i].Rotate(center, angle, sel);
  for (int i=0; i<sensors.Size(); i++)
    sensors[i].Rotate(center, angle, sel);
  for (int i=0; i<markers.Size(); i++)
    markers[i].Rotate(center, angle, sel);
  for (int i=0; i<groups.Size(); i++)
    groups[i].Rotate(center, angle, sel);
}

void ArcadeTemplate::CalculateCenter(Vector3 &sum, int &count, bool sel)
{
  for (int i=0; i<emptyVehicles.Size(); i++)
    emptyVehicles[i].CalculateCenter(sum, count, sel);
  for (int i=0; i<sensors.Size(); i++)
    sensors[i].CalculateCenter(sum, count, sel);
  for (int i=0; i<markers.Size(); i++)
    markers[i].CalculateCenter(sum, count, sel);
  for (int i=0; i<groups.Size(); i++)
    groups[i].CalculateCenter(sum, count, sel);
}

void ArcadeTemplate::ClearSelection()
{
  for (int i=0; i<emptyVehicles.Size(); i++)
    emptyVehicles[i].selected = false;
  for (int i=0; i<sensors.Size(); i++)
    sensors[i].selected = false;
  for (int i=0; i<markers.Size(); i++)
    markers[i].selected = false;
  for (int i=0; i<groups.Size(); i++)
  {
    ArcadeGroupInfo &gInfo = groups[i];
    for (int j=0; j<gInfo.units.Size(); j++)
      gInfo.units[j].selected = false;
    for (int j=0; j<gInfo.sensors.Size(); j++)
      gInfo.sensors[j].selected = false;
    for (int j=0; j<gInfo.waypoints.Size(); j++)
      gInfo.waypoints[j].selected = false;
  }
}
