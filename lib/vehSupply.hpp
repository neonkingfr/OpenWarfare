/*!
\file
Interface for resource supplies
*/

#ifdef _MSC_VER
#pragma once
#endif

#ifndef _VEHICLE_SUPPLY_HPP
#define _VEHICLE_SUPPLY_HPP

#include "Network/networkObject.hpp"
#include "weapons.hpp"
#include "memberCalls.hpp"
#include "vehicleAI.hpp"
#include "UI/uiActions.hpp"

#define LOG_WEAPONS_CHANGES 0

class Magazine;
class WeaponType;

class ResourceSupply
{
	private:
	float _fuelCargo,_repairCargo,_ammoCargo;
	RefArray<WeaponType> _weaponCargo;
	RefArray<Magazine> _magazineCargo;
	OLinkO(EntityAIFull) _supplying;
	OLinkO(EntityAIFull) _alloc;
	Ref<Action> _action;
  Ref<EntityAI> _parent;
	RefArray<EntityAI> _backpacks;
	
	/// make sure we do not start automatic refuel unless needed, but we finish it once we have started
	bool _wasRefuelingPlayer;
	
  /// even empty resource with _forceSupply set cannot be deleted when locked
  /// safe REF for the case when MP client disconnects and cannot unlock supply 
  RefArray<AIBrain> _resourceLock;

  //! units that are services by this resource
	mutable SupplyUnitList _supplyUnits;

	public:
	ResourceSupply( EntityAI *vehicle, bool fullCreate = true);
  ~ResourceSupply();

  void Destroy(EntityAI *parent);

//	EntityAI *Scan( SupportCheckF check, float limit=0.05 ) const;
	bool Check(EntityAI *parent, EntityAI *vehicle, SupportCheckF check, float limit, bool now) const;

	void Simulate( EntityAI *parent, float deltaT, SimulationImportance prec );

	void SetAlloc(EntityAIFull *vehicle) {_alloc=vehicle;}
	EntityAIFull *GetAlloc() const {return _alloc;}

  const Action *GetAction() const {return _action;}

  void PutMagazine(EntityAI *parent); //helper function to process ATPutMagazine
  void PutWeapon(EntityAI *parent);   //helper function to process ATPutWeapon
  void PutBackpack(EntityAI *parent);   //helper function to process ATDropBackpack
  bool Supply(EntityAIFull *vehicle, const Action *action);
  void CancelSupply();
	EntityAIFull *GetSupplying() const {return _supplying;}

  void InitCargoBackpacks();

  SupplyUnitList &GetSupplyUnits() const {return _supplyUnits;}

	float GetFuelCargo() const {return _fuelCargo;}
	float GetRepairCargo() const {return _repairCargo;}
	float GetAmmoCargo() const {return _ammoCargo;}

	int GetWeaponCargoSize() const {return _weaponCargo.Size();}
	const WeaponType *GetWeaponCargo(int weapon) const {return _weaponCargo[weapon];}
  const RefArray<WeaponType> GetWeaponCargo() const {return _weaponCargo;}
  
  int GetMagazineCargoSize() const {return _magazineCargo.Size();}
	const Magazine *GetMagazineCargo(int magazine) const {return _magazineCargo[magazine];}
  const RefArray<Magazine> GetMagazineCargo() const {return _magazineCargo;}

  int GetBackpackCargoSize() const {return _backpacks.Size();}
  EntityAI *GetBackpackCargo(int backpack) const {return _backpacks[backpack];}
  RefArray<EntityAI> GetBackpackCargo() const {return _backpacks;}

  int GetFreeWeaponCargo(const EntityAI *parent) const;
  int GetFreeMagazineCargo(const EntityAI *parent) const;
  int GetFreeBackpackCargo(const EntityAI *parent) const;

  int GetFreeWeaponCargoBackpack(const EntityAI *parent, WeaponType *type) const;
  int GetFreeMagazineCargoBackpack(const EntityAI *parent, MagazineType *type) const;
  int GetFreeCargoBackpack(const EntityAI *parent) const;

	void LoadFuelCargo( float cargo ) {_fuelCargo+=cargo;}
	void LoadRepairCargo( float cargo ) {_repairCargo+=cargo;}
	void LoadAmmoCargo( float cargo ) {_ammoCargo+=cargo;}

  void DeleteIfNotNeeded(EntityAI *parent);
  void LockResources(AIBrain *user) {if(user) _resourceLock.AddUnique(user);}
  void UnlockResources(AIBrain *user) {if(user)  _resourceLock.Delete(user);}
  bool AreResourcesLocked() const {return _resourceLock.Size() > 0;}

	void ClearWeaponCargo(EntityAI *parent, bool localOnly = true);
	int AddWeaponCargo(EntityAI *parent, WeaponType *weapon, int count, bool localOnly);
	bool RemoveWeaponCargo(EntityAI *parent, const WeaponType *weapon);

	void ClearMagazineCargo(EntityAI *parent, bool localOnly = true);
	int AddMagazineCargo(EntityAI *parent, Magazine *magazine, bool localOnly);
	int AddMagazineCargoCount(EntityAI *parent, MagazineType *type, int count, bool localOnly);
	bool RemoveMagazineCargo(EntityAI *parent, Magazine *magazine);
  bool RemoveMagazineCargo(EntityAI *parent, RString type, int ammo);

  void ClearBackpackCargo(EntityAI *parent, bool localOnly = true);
  int AddBackpackCargo(EntityAI *parent, EntityAI *backpack, bool localOnly, EntityAI *man = NULL);
  bool RemoveBackpackCargo(EntityAI *parent, EntityAI *backpack);
  RefArray<EntityAI> GetBackpacks() const {return _backpacks;}

	void GetActions(EntityAI *parent, UIActions &actions, AIBrain *unit, bool now, bool strict);
#if _VBS3
  float GetMagazineWeightKg();
  float GetWeaponWeightKg();
#endif

	bool FindWeapon(const WeaponType *weapon, int *count = NULL) const;
	bool FindMagazine(const Magazine *magazine) const;
  EntityAI *FindBackpack(RString name);

	Magazine *FindMagazine(CreatorId creator, int id) const;
	const Magazine *FindMagazine(RString name, int *count = NULL) const;
	const Magazine *FindMagazine(const MagazineType *type, int *count = NULL) const;

  void OfferMagazine(EntityAI *parent, EntityAIFull *to, const MagazineType *type, bool useBackpack);
  void OfferWeapon(EntityAI *parent, EntityAIFull *to, const WeaponType *weapon, int slots, bool useBackpack);
  void OfferBackpack(EntityAI *parent, EntityAIFull *to, RString name);
  void ReturnWeapon(EntityAI *parent, const WeaponType *weapon);
  void ReturnMagazine(EntityAI *parent, Magazine *magazine);
  void ReturnBackpack(EntityAI *parent, EntityAI *backpack);
  void ReturnEquipment(EntityAI *parent, AutoArray<RString> weapons, RefArray<Magazine> magazines, EntityAI *backpack);

	LSError Serialize(ParamArchive &ar);
	static ResourceSupply *CreateObject(ParamArchive &ar) {return new ResourceSupply();}

  DECLARE_NETWORK_OBJECT
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContextWithError &ctx);
	static ResourceSupply *CreateObject(const NetworkMessageContext &ctx) {return new ResourceSupply();}
	
	//void SetParent(EntityAI *vehicle) {_parent = vehicle;}
private:
	ResourceSupply(); // used for serialization only

};

RETVALUES_NO_SPEC(MagazineType *)
RETVALUES_NO_SPEC(WeaponType *)
RETVALUES_NO_SPEC(Magazine *)

RETVALUES_NO_SPEC(const MagazineType *)
RETVALUES_NO_SPEC(const WeaponType *)
RETVALUES_NO_SPEC(const Magazine *)

CALL_ARGS(ResourceSupply,EntityAI,GetSupply())

#define SUPPLY(veh,x,arg) CALLARGS(veh,ResourceSupply,x,arg)
#define SUPPLY_DEF(veh,x,arg,def) CALLARGS_DEF(veh,ResourceSupply,x,arg,def)


inline float EntityAI::GetFuelCargo() const {return SUPPLY(this,GetFuelCargo, ());}
inline float EntityAI::GetRepairCargo() const {return SUPPLY(this,GetRepairCargo, ());}
inline float EntityAI::GetAmmoCargo() const {return SUPPLY(this,GetAmmoCargo, ());}

inline int EntityAI::GetWeaponCargoSize() const {return SUPPLY(this,GetWeaponCargoSize, ());}
inline const WeaponType *EntityAI::GetWeaponCargo(int weapon) const {return SUPPLY(this,GetWeaponCargo, (weapon));}
inline int EntityAI::GetMagazineCargoSize() const {return SUPPLY(this,GetMagazineCargoSize, ());}
inline const Magazine *EntityAI::GetMagazineCargo(int magazine) const {return SUPPLY(this,GetMagazineCargo, (magazine));}
inline int EntityAI::GetBackpackCargoSize() const {return SUPPLY(this,GetBackpackCargoSize, ());}
inline EntityAI *EntityAI::GetBackpackCargo(int backpack) {return SUPPLY(this,GetBackpackCargo, (backpack));}
inline int EntityAI::GetFreeWeaponCargo() const {return SUPPLY(this,GetFreeWeaponCargo, ());}
inline int EntityAI::GetFreeMagazineCargo() const {return SUPPLY(this,GetFreeMagazineCargo, ());}
inline int EntityAI::GetFreeBackpackCargo() const {return SUPPLY(this,GetFreeBackpackCargo, ());}

inline void EntityAI::LoadFuelCargo( float cargo ) {SUPPLY(this,LoadFuelCargo, (cargo));}
inline void EntityAI::LoadRepairCargo( float cargo ) {SUPPLY(this,LoadRepairCargo, (cargo));}
inline void EntityAI::LoadAmmoCargo( float cargo ) {SUPPLY(this,LoadAmmoCargo, (cargo));}

inline void EntityAI::DeleteIfNotNeeded() {SUPPLY(this, DeleteIfNotNeeded, ());}
inline void EntityAI::LockResources(AIBrain *user) {SUPPLY(this, LockResources, (user));}
inline void EntityAI::UnlockResources(AIBrain *user) {SUPPLY(this, UnlockResources, (user));}
inline bool EntityAI::AreResourcesLocked() const {return SUPPLY(this, AreResourcesLocked, ());}

inline void EntityAI::ClearWeaponCargo(bool localOnly) {SUPPLY(this,ClearWeaponCargo, (localOnly));}

inline int EntityAI::AddWeaponCargo(WeaponType *weapon, int count, bool localOnly)
{
	return SUPPLY_DEF(this,AddWeaponCargo, (weapon, count, localOnly), -1);
}

inline bool EntityAI::RemoveWeaponCargo(const WeaponType *weapon) {return SUPPLY(this,RemoveWeaponCargo, (weapon));}
inline void EntityAI::ClearMagazineCargo(bool localOnly) {SUPPLY(this,ClearMagazineCargo, (localOnly));}
inline int EntityAI::AddMagazineCargo(Magazine *magazine, bool localOnly)
{
	return SUPPLY_DEF(this,AddMagazineCargo, (magazine, localOnly), -1);
}
inline int EntityAI::AddMagazineCargoCount(MagazineType *type, int count, bool localOnly)
{
	return SUPPLY_DEF(this,AddMagazineCargoCount, (type, count, localOnly), -1);
}
inline bool EntityAI::RemoveMagazineCargo(Magazine *magazine) {return SUPPLY(this,RemoveMagazineCargo, (magazine));}
inline bool EntityAI::RemoveMagazineCargo(RString type, int ammo) {return SUPPLY(this,RemoveMagazineCargo, (type, ammo));}

inline int EntityAI::AddBackpackCargo(EntityAI *backpack, bool localOnly)
{
  if(!_supply || !backpack) return -1;
  return _supply->AddBackpackCargo(this,backpack,localOnly);
}

inline int EntityAI::RemoveBackpackCargo(EntityAI *backpack)
{
  if(!_supply || !backpack) return -1;
  return _supply->RemoveBackpackCargo(this,backpack);
}

inline void EntityAI::ClearBackpackCargo(bool localOnly)
{
  if(!_supply) return;
  _supply->ClearBackpackCargo(this, localOnly);
}

#endif
