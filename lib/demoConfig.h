// Configuration parameters for ArmA demo
// - public SP & MP demo

#define _VERIFY_KEY								0
#define _VERIFY_CLIENT_KEY  			0  // server is checking the CD key of clients on GameSpy
#define _VERIFY_BLACKLIST					0
#define _ENABLE_EDITOR						1
#define _ENABLE_WIZARD						1
#define _ENABLE_ALL_MISSIONS			0
#define _ENABLE_MP								1
#define _ENABLE_AI								1
#define _ENABLE_CAMPAIGN					1
#define _ENABLE_SINGLE_MISSION		1
#define _ENABLE_UNSIGNED_MISSIONS	0
#define _ENABLE_BRIEF_GRP					1
#define _ENABLE_PARAMS						1
#define _ENABLE_CHEATS						0
#define _ENABLE_ADDONS						1
#define _ENABLE_DEDICATED_SERVER	1
#define _FORCE_DEMO_ISLAND				0
#define _FORCE_SINGLE_VOICE				0
#define _ENABLE_AIRPLANES					1
#define _ENABLE_HELICOPTERS				1
#define _ENABLE_SHIPS							1
#define _ENABLE_CARS							1
#define _ENABLE_TANKS							1
#define _ENABLE_MOTORCYCLES				1
#define _ENABLE_PARACHUTES				1
#define _ENABLE_DATADISC					1
#define _ENABLE_VBS								0
#define _DISABLE_CRC_PROTECTION   1
#define _FORCE_DS_CONTEXT         0
#define _ENABLE_GAMESPY           1
#define _ENABLE_NEWHEAD           1
#define _ENABLE_SKINNEDINSTANCING 0
#define _ENABLE_BB_TREES          0
#define _ENABLE_CONVERSATION      1
#define _ENABLE_SPEECH_RECOGNITION  0
#define _ENABLE_EDITOR2           1
#define _ENABLE_EDITOR2_MP        0
#define _ENABLE_IDENTITIES        1
#define _ENABLE_INVENTORY         0
#define _ENABLE_MISSION_CONFIG    1
#define _ENABLE_INDEPENDENT_AGENTS  1
#define _ENABLE_DIRECT_MESSAGES   0
#define _ENABLE_FILE_FUNCTIONS    0
#define _ENABLE_HAND_IK           1
#define _ENABLE_WALK_ON_GEOMETRY  0
#define _ENABLE_BULDOZER          0
#define _ENABLE_COMPILED_SHADER_CACHE 1
#define _ENABLE_DISTRIBUTIONS     0
#define _USE_FCPHMANAGER          1
#define _ENABLE_DX10              0
#define _USE_BATTL_EYE_SERVER     1
#define _USE_BATTL_EYE_CLIENT     1
#define _ENABLE_STEAM             0
#define _ENABLE_TEXTURE_HEADERS   0 //if reading texture headers from texture headers file is enabled

#define _TIME_ACC_MIN							1.0
#define _TIME_ACC_MAX							4.0

#define _MACRO_CDP							  0

// default mod path 
#define _MOD_PATH_DEFAULT "CA;Expansion"

// Testing of initial signatures test
#define _ACCEPT_ONLY_SIGNED_DATA  1
// Accepted public keys
#define ACCEPTED_KEYS { \
  { \
  "OA DEMO", \
    { \
    0x06, 0x02, 0x00, 0x00, 0x00, 0x24, 0x00, 0x00, 0x52, 0x53, 0x41, 0x31, \
    0x00, 0x04, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x0F, 0x26, 0x60, 0xBE, 0x71, 0x07, 0x14, 0xC8, \
    0x27, 0x27, 0xB6, 0x6B, 0xDC, 0x01, 0x93, 0x7B, 0x9E, 0x3F, 0xC8, 0x72, 0x17, 0x05, 0xA0, 0xBB, \
    0x88, 0x3F, 0xDB, 0x1C, 0x4D, 0x22, 0x84, 0xBD, 0xB4, 0xC3, 0xFE, 0x67, 0xDF, 0xB8, 0xDE, 0x85, \
    0xB0, 0x69, 0xC6, 0xEB, 0x89, 0x81, 0xA8, 0xD9, 0x32, 0xA4, 0x1C, 0xF5, 0x19, 0xF9, 0x08, 0x36, \
    0x1D, 0x36, 0x9D, 0xEF, 0xBB, 0xD4, 0x04, 0x0C, 0x15, 0x19, 0x0C, 0xC0, 0xE6, 0x76, 0x32, 0xF5, \
    0x68, 0x52, 0xE9, 0x22, 0xD7, 0x32, 0xA4, 0x53, 0x86, 0xDD, 0x3C, 0x77, 0xE4, 0xE4, 0x65, 0x39, \
    0x63, 0x65, 0x53, 0xB2, 0xE6, 0xE7, 0x9D, 0xAB, 0xDB, 0x86, 0x99, 0x99, 0x37, 0xF6, 0xE5, 0x1B, \
    0xB8, 0x4A, 0xF4, 0x7C, 0x69, 0xDB, 0xAB, 0x7E, 0x2A, 0x29, 0x3A, 0xC7, 0x69, 0x3C, 0xDA, 0x4C, \
    0xDA, 0xD1, 0x6B, 0x8D, 0x45, 0xEF, 0xA0, 0xB3} \
  } \
}

// Additional checked files
#define ADDITIONAL_SIGNED_FILES {0}
