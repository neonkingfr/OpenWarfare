#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PROXY_WEAPON_HPP
#define _PROXY_WEAPON_HPP

#include "vehicle.hpp"

#if _ENABLE_NEWHEAD

class ProxySubpartType : public EntityType
{
  typedef EntityType base;
public:
  ProxySubpartType(ParamEntryPar param);
  void Load(ParamEntryPar par, const char *shape);
  void InitShape();
  bool AbstractOnly() const {return false;}

  virtual Object* CreateObject(bool unused) const;
};

class ProxySubpart : public Vehicle
{
  typedef Vehicle base;
public:
  ProxySubpart(const EntityType *type, LODShapeWithShadow *shape);
  const ProxySubpartType *Type() const
  {
    return static_cast<const ProxySubpartType *>(GetNonAIType());
  }
  USE_CASTING(base)
};

#endif

//////////////////////////////////////////////////////////////////////////

#define INVENTORY_ID_ENUM(type, prefix, XX) \
  XX(type, prefix, PrimaryWeapon) \
  XX(type, prefix, SecondaryWeapon) \
  XX(type, prefix, RightHand) \
  XX(type, prefix, LeftHand) \
  XX(type, prefix, Goggles) \
  XX(type, prefix, Backpack)

#ifndef DECL_ENUM_INVENTORY_CONTAINER_ID
#define DECL_ENUM_INVENTORY_CONTAINER_ID
DECL_ENUM(InventoryContainerId)
#endif
DECLARE_ENUM(InventoryContainerId, ICI, INVENTORY_ID_ENUM)

class ProxyWeaponType: public EntityType
{
  typedef EntityType base;

  //	Vector3 _muzzlePos,_muzzleDir;
  //Vector3 _cameraPos;
  InventoryContainerId _inventoryId;

public:
  ProxyWeaponType(ParamEntryPar param, InventoryContainerId id);
  ProxyWeaponType(ParamEntryPar param);

  void Load(ParamEntryPar par, const char *shape);
  void InitShape();
  bool AbstractOnly() const {return false;}

  virtual Object* CreateObject(bool unused) const { return new ObjectTyped(GetShape(), this, VISITOR_NO_ID, false); }  // for _simname="proxyweapon", "proxysecweapon", "proxyhandgun", "proxyinventoryold"

  virtual InventoryContainerId GetInventoryContainerId() const {return _inventoryId;}

  //Vector3Val GetCameraPos() const {return _cameraPos;}
};

// CrewPosition enum: see also config: CfgNonAIVehicles
enum CrewPosition {CPDriver,CPGunner,CPCommander,CPCargo};

class ProxyCrewType: public EntityType
{
	typedef EntityType base;
	CrewPosition _pos;

	public:
	ProxyCrewType(ParamEntryPar param);

	void Load(ParamEntryPar par, const char *shape);
	void InitShape();
	bool AbstractOnly() const {return false;}

  virtual Object* CreateObject(bool unused) const;

	CrewPosition GetCrewPosition() const {return _pos;}

};


class ProxyCrew: public Entity
{
	typedef Entity base;

	public:
	ProxyCrew(const EntityType *type);

	const ProxyCrewType *Type() const
	{
		return static_cast<const ProxyCrewType *>(GetNonAIType());
	}

	USE_CASTING(base)
};

#endif
