#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TRANSPORT_HPP
#define _TRANSPORT_HPP

/*!
\file
interface for Transport class
*/

#include "vehicleAI.hpp"
#include "hudDesc.hpp"
#include "speaker.hpp"
#include "person.hpp"
#include "landscape.hpp"
#include "camShake.hpp"
#include "global.hpp"

struct ManCargoItem
{
  Ref<Person> man;
  //float animTime;
  /// cargo position locked  (nobody can get in / move to it)
  bool _locked;

  ManCargoItem() : _locked(false) {}
  LSError Serialize(ParamArchive &ar);
};
TypeIsMovableZeroed(ManCargoItem)

//typedef RefArray<Person> ManCargo;
class ManCargo : public AutoArray<ManCargoItem>
{
  typedef AutoArray<ManCargoItem> base;
public:
  Person *operator [](int i) const
  {return base::operator [](i).man;}
  bool IsLocked(int i) const {return base::operator [](i)._locked;}
  void Lock(int i, bool lock) {base::operator [](i)._locked = lock;}
};

#include "turret.hpp"

enum RadioMessageVehicleType
{
  RMTFirstVehicle=0x100,
  RMTVehicleMove=RMTFirstVehicle,
  RMTVehicleFire,
  RMTVehicleFormation,
  RMTVehicleSimpleCommand,
  RMTVehicleTarget,
  RMTVehicleLoad,
  RMTVehicleAzimut,
  RMTVehicleStopTurning,
  RMTVehicleFireFailed,
  RMTVehicleLoadMagazine,
  RMTVehicleWatchTgt,
  RMTVehicleWatchPos,
};

RadioMessage *CreateVehicleMessage(int type);

#define V_MESSAGE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Transport), vehicle, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("In vehicle"), TRANSF_REF)

DECLARE_NET_INDICES_NO_NMT(VMessage, V_MESSAGE_MSG, NetworkMessage)

class RadioMessageWithTarget: public RadioMessage, public NetworkSimpleObject
{
protected:
  OLinkPermO(Transport) _vehicle;
  OLinkPermO(Person) _receiver;
  LinkTarget _target;

public:
  RadioMessageWithTarget() {}
  RadioMessageWithTarget(Transport *vehicle, Person *receiver, Target *target);
  LSError Serialize(ParamArchive &ar);
  AIBrain *GetSender() const;

  Target *GetTarget() const {return _target;}
  void SetTarget(Target *target) {_target = target;}

  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
};

class RadioMessageVFire: public RadioMessageWithTarget
{
  typedef RadioMessageWithTarget base;
public:
  RadioMessageVFire() {}
  RadioMessageVFire(Transport *vehicle, Person *receiver, Target *target);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "UrgentCommand";}
  int GetType() const {return RMTVehicleFire;}

  DECLARE_NETWORK_OBJECT

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageVTarget: public RadioMessageWithTarget
{
  typedef RadioMessageWithTarget base;

public:
  RadioMessageVTarget() {}
  RadioMessageVTarget(Transport *vehicle, Person *receiver, Target *target);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "UrgentCommand";}
  int GetType() const {return RMTVehicleTarget;}

  DECLARE_NETWORK_OBJECT

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageVMove: public RadioMessageWithMove, public NetworkSimpleObject
{
protected:
  OLinkPermO(Transport) _vehicle;
  Vector3 _destination;

public:
  RadioMessageVMove() {}
  RadioMessageVMove( Transport *vehicle, Vector3Par pos );
  LSError Serialize(ParamArchive &ar);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "NormalCommand";}
  int GetType() const {return RMTVehicleMove;}
  AIBrain *GetSender() const;

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageVWatchTgt: public RadioMessageWithTarget
{
  typedef RadioMessageWithTarget base;

public:
  RadioMessageVWatchTgt() {}
  RadioMessageVWatchTgt(Transport *vehicle, Person *receiver, Target *target);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "NormalCommand";}
  int GetType() const {return RMTVehicleWatchTgt;}

  DECLARE_NETWORK_OBJECT

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageVWatchPos: public RadioMessage, public NetworkSimpleObject
{
protected:
  OLinkPermO(Transport) _vehicle;
  OLinkPermO(Person) _receiver;
  Vector3 _position;

public:
  RadioMessageVWatchPos() {}
  RadioMessageVWatchPos(Transport *vehicle, Person *receiver, Vector3Par pos);
  LSError Serialize(ParamArchive &ar);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "NormalCommand";}
  int GetType() const {return RMTVehicleWatchPos;}
  AIBrain *GetSender() const;

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageVAzimut: public RadioMessage, public NetworkSimpleObject
{
protected:
  OLinkPermO(Transport) _vehicle;
  float _azimut;

public:
  RadioMessageVAzimut() {}
  RadioMessageVAzimut(Transport *vehicle, float azimut);
  LSError Serialize(ParamArchive &ar);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "NormalCommand";}
  int GetType() const {return RMTVehicleAzimut;}
  AIBrain *GetSender() const;

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

enum SimpleCommand
{
  SCForward,
  SCStop,
  SCBackward,
  SCFaster,
  SCSlower,
  SCLeft,
  SCRight,
  SCFire,
  SCCeaseFire,
  SCKeyUp,
  SCKeyDown,
  SCKeySlow,
  SCKeyFast,
  SCKeyFire,
  SCManualFire,
  SCCancelManualFire
};

class RadioMessageVSimpleCommand: public RadioMessage, public NetworkSimpleObject
{
protected:
  OLinkPermO(Transport) _vehicle;
  OLinkPermO(Person) _receiver;
  SimpleCommand _cmd;

public:
  RadioMessageVSimpleCommand() {}
  RadioMessageVSimpleCommand(Transport *vehicle, Person *receiver, SimpleCommand cmd);
  LSError Serialize(ParamArchive &ar);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass()
  {
    return _cmd == SCFire || _cmd == SCCeaseFire || _cmd == SCKeyFire ? "UrgentCommand" : "NormalCommand";
  }
  int GetType() const {return RMTVehicleSimpleCommand;}
  SimpleCommand GetCommand() const {return _cmd;}
  AIBrain *GetSender() const;

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
  /// translate virtual commands (SCKey...) to real
  SimpleCommand MapCommand() const;
};

class RadioMessageVStopTurning: public RadioMessage, public NetworkSimpleObject
{
protected:
  OLinkPermO(Transport) _vehicle;
  float _azimut;

public:
  RadioMessageVStopTurning() {}
  RadioMessageVStopTurning(Transport *vehicle, float azimut);
  LSError Serialize(ParamArchive &ar);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "NormalCommand";}
  int GetType() const {return RMTVehicleStopTurning;}
  AIBrain *GetSender() const;

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageVFormation:  public RadioMessage, public NetworkSimpleObject
{
protected:
  OLinkPermO(Transport) _vehicle;

public:
  RadioMessageVFormation() {}
  RadioMessageVFormation( Transport *vehicle );
  LSError Serialize(ParamArchive &ar);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "NormalCommand";}
  int GetType() const {return RMTVehicleFormation;}
  AIBrain *GetSender() const;

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageVLoad: public RadioMessage, public NetworkSimpleObject
{
protected:
  OLinkPermO(Transport) _vehicle;
  OLinkPermO(Person) _receiver;
  int _weapon;

public:
  RadioMessageVLoad() {}
  RadioMessageVLoad(Transport *vehicle, Person *receiver, int weapon);
  LSError Serialize(ParamArchive &ar);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "UrgentCommand";}
  int GetType() const {return RMTVehicleLoad;}
  int GetWeapon() const {return _weapon;}
  void SetWeapon(int weapon) {_weapon = weapon;}
  AIBrain *GetSender() const;

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

#define MSG_V_LOAD_MAGAZINE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), receiver, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Gunner - receiver of the message."), TRANSF_REF) \
  XX(MessageName, CreatorId, magazineCreator, NDTInteger, CreatorId, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Network ID of magazine."), TRANSF) \
  XX(MessageName, int, magazineId, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Network ID of magazine."), TRANSF) \
  XX(MessageName, RString, weapon, NDTString, RString, NCTNone, DEFVALUE(RString, RString()), DOC_MSG("Weapon name"), TRANSF) \
  XX(MessageName, RString, muzzle, NDTString, RString, NCTNone, DEFVALUE(RString, RString()), DOC_MSG("Muzzle name"), TRANSF)

DECLARE_NET_INDICES_EX(MsgVLoadMagazine, VMessage, MSG_V_LOAD_MAGAZINE_MSG)

class RadioMessageVLoadMagazine: public RadioMessage, public NetworkSimpleObject
{
protected:
  OLinkPermO(Transport) _vehicle;
  OLinkPermO(Person) _receiver;
  CreatorId _magazineCreator;
  int _magazineId;
  RString _weapon;
  RString _muzzle;

public:
  RadioMessageVLoadMagazine() {}
  RadioMessageVLoadMagazine(Transport *vehicle, Person *receiver, CreatorId magazineCreator, int magazineId, RString muzzle, RString weapon);
  LSError Serialize(ParamArchive &ar);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "UrgentCommand";}
  int GetType() const {return RMTVehicleLoadMagazine;}
  AIBrain *GetSender() const;
  void SetParams(CreatorId magazineCreator, int magazineId, RString weapon, RString muzzle);
  bool CheckParams(CreatorId magazineCreator, int magazineId, RString weapon, RString muzzle) const;

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
    (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
    );
  TMError TransferMsg(NetworkMessageContext &ctx);

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageVFireFailed: public RadioMessage, public NetworkSimpleObject
{
protected:
  OLinkPermO(Transport) _vehicle;
  OLinkPermO(Person) _sender;

public:
  RadioMessageVFireFailed() {}
  RadioMessageVFireFailed(Transport *vehicle, Person *sender);
  LSError Serialize(ParamArchive &ar);
  void Transmitted(ChatChannel channel);
  const char *GetPriorityClass() {return "Failure";}
  int GetType() const {return RMTVehicleFireFailed;}
  AIBrain *GetSender() const;

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);

  void Send();  // send itself to radio protocol

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

#include "AI/pathPlanner.hpp"
  
enum VehicleMoveMode
{
  VMMFormation,
  VMMMove,
  VMMBackward,
  VMMStop,
  VMMSlowForward,
  VMMForward,
  VMMFastForward,
};

enum VehicleTurnMode
{
  VTMNone,
  VTMLeft,
  VTMRight,
  VTMAbs,
};

typedef RStringB ManAnimationType;

/// proxy crew information for given level
struct LevelProxies
{
  ManProxy _driverProxy; // positioning of different crew members
  AutoArray<ManProxy> _cargoProxy;
  /// count proxies which should be enumerated as normal
  AutoArray<int> _normalProxy;
  
  void Clear();
};
TypeIsMovable(LevelProxies)

//////////////////////////////////////////////////////////////////////////

// used for directional sound, store cone info
struct SoundCone
{
  float _volIn;
  float _volOut;
  float _angleIn;
  float _angleOut;

  bool  operator == (const SoundCone &with) const
  {
    const float Delta = 0.05f;

    return (fabs(_volIn - with._volIn) < Delta && fabs(_volOut - with._volOut) < Delta &&
      fabs(_angleIn - with._angleIn) < Delta && fabs(_angleOut - with._angleOut) < Delta);
  }
};

TypeIsSimple(SoundCone)

// used for audio controllers
struct SoundParsEx
{
  SoundPars _pars;
  ExpressionCode _freqExpr;
  ExpressionCode _volExpr;
  SoundCone _cone;
  bool _coneUsed;
};

TypeIsMovable(SoundParsEx)

// used for audio events
struct SoundEventPars
{
  SoundPars _pars;
  ExpressionCode _expression;
  float _limit;
  bool _started;
  SoundCone _cone;
  bool _coneUsed;
};

TypeIsMovable(SoundEventPars)

struct SubmixPars
{
  AutoArray<SoundParsEx> _sParsEx;

  SubmixPars(): _sParsEx() {}
  ~SubmixPars() { _sParsEx.Clear(); }
};

TypeIsMovable(SubmixPars)

struct SubmixEventPars
{
  AutoArray<SoundEventPars> _sEventPars;

  SubmixEventPars(): _sEventPars() {}
  ~SubmixEventPars() { _sEventPars.Clear(); }
};

TypeIsMovable(SubmixEventPars)

//////////////////////////////////////////////////////////////////////////

class TransportType;


class TransportVisualState : public EntityVisualState
{
  typedef EntityVisualState base;
  friend class Transport;

protected:
  float _fuel;
  float _driverHidden;
  TurretVisualStateArray _turretVisualStates;

private:
  TransportVisualState();  // don't call the implicit ctor!

public:
  explicit TransportVisualState(TransportType const& type);

  virtual void Interpolate(Object *obj, ObjectVisualState const& t1state, float t, ObjectVisualState& interpolatedResult) const;  // TODO
  virtual void Copy(ObjectVisualState const& src) { *this = static_cast_checked<const TransportVisualState &>(src); }
  virtual TransportVisualState *Clone() const {return new TransportVisualState(*this);}

  float GetAltRadar(Ref<LODShapeWithShadow> shape) const;
  float GetAltBaro() const;
  float GetSpeedHorizontal() const;
  float GetSpeedVertical() const;
  float GetDirection() const;
  float GetDriverHatchPos() const { return 1.0f - _driverHidden; }
  float GetHorizonBank() const;
  float GetHorizonDive() const;
  float GetFuelFraction(TransportType const* type) const;
  float GetFuel() const { return _fuel; }
  float GetGMeter() const;
  float GetGMeterX() const;
  float GetGMeterY() const;
  float GetGMeterZ() const;

  bool IsDriverHidden() const {return _driverHidden > 0.1f;}

  TurretVisualState* GetTurret(const int *path, int size);
  TurretVisualState const* GetTurret(const int *path, int size) const;

  virtual float GetDamperState(int index) const { return 0.0f; }  // damper state for visual and sound controllers
};

//////////////////////////////////////////////////////////////////////////

/// search the vehicle config for the turret with primary gunner flag set
bool HasPrimaryGunnerTurret(ParamEntryPar cls);
/// search the vehicle config for the turret with primary observer flag set
bool HasPrimaryObserverTurret(ParamEntryPar cls);

class TransportType: public EntityAIType
{
  friend class Transport;
  friend class TransportVisualState;
  friend class Man;

  typedef EntityAIType base;

protected:
  Buffer<LevelProxies> _proxies;
  RString _crew;

  /// list of MFDs
  AutoArray<MFDDesc> _mfd;  

  

#ifndef _XBOX
  PlateInfos _squadTitles;
#endif

  //@{
  //! Action names corresponding to crew position
  RString _driverAction;
  RString _driverInAction; // actions inside vehicle (turn in)
  RString _driverGetInAction;
  RString _driverGetOutAction;
  AutoArray<RString> _cargoAction;
  AutoArray<RString> _cargoGetInAction;
  AutoArray<RString> _cargoGetOutAction;
  //@}

  //@{ some crew proxies should cast shadows
  bool _castDriverShadow;
  bool _castCargoShadow;
  //@}

  //@{ shadows should be casted in some internal (cockpit) views, light may be attenuated
  bool _viewDriverShadow;
  bool _viewCargoShadow;
  float _viewDriverShadowDiff;
  float _viewCargoShadowDiff;
  float _viewDriverShadowAmb;
  float _viewCargoShadowAmb;
  //@}

  //@{ some crew position should be ejected when killed
  bool _ejectDeadDriver;
  bool _ejectDeadCargo;
  //@}

  //@{ weapon should be hidden for some crew positions
  bool _hideWeaponsDriver;
  bool _hideWeaponsCargo;
  //@}

  GetInPoints _driverGetInPos;
  // store get in points for each position separately
  AutoArray<GetInPoints> _cargoGetInPos;
  float _getInRadius;

  /// where driver can switch position
  int _driverCompartments;
  /// where cargo can switch position
  AutoArray<int> _cargoCompartments;

  float _insideSoundCoef;
  bool _outsideSoundFilter;
  //@{ vehicle body sound absorption characteristics
  float _occludeSoundsWhenIn;
  float _obstructSoundsWhenIn;
  //@}

  bool _hideProxyInCombat;
  bool _forceHideDriver;

#if _VBS3 //added optional parameter _viewDriverInExternal
  bool _viewDriverInExternal;
#endif


  bool _unloadInCombat;

  bool _hasDriver;
  bool _hasGunner;
  bool _hasObserver;
  // the driver commands the vehicle
  // CHANGED: before, the meaning was "driver commands more than gunner"
  bool _driverIsCommander;
  bool _driverForceOptics;

  /// crew is sitting always out or is vulnerable when inside
  bool _crewVulnerable;

  /// Manual Fire action is available for this vehicle
  bool _enableManualFire;

  // _viewDriver is member of EntityAIType
  ViewPars _viewCargo;
  ViewPars _viewOptics;

  int _driverOpticsPos;

  Ref<LODShapeWithShadow> _driverOpticsModel;

  RefArray<EntityAIType> _typicalCargo;

  PackedColor _driverOpticsColor;
	
  int _maxManCargo;

  AutoArray<SubmixPars> _submixPars;
  AutoArray<SubmixEventPars> _submixEventPars;
  
  // driver optics post effects
  AutoArray<PPEffectType> _opticsType;

  // camera shake that is caused by this transport passing near player
  CameraShakePars _camShake;
  // coefficient that is applied to all camera shakes, when the player is inside this vehicle
  float _camShakeCoef;

protected:
  void AddProxy(
    ManProxy &mProxy,
    const ProxyObjectTyped &proxy, const ProxyObject &proxyObj
  );

  /// list of attached turrets types
  RefArray<TurretType> _turrets;

public:
  TransportType( ParamEntryPar param );
  virtual void Load(ParamEntryPar par, const char *shape);

  AnimationSource *CreateAnimationSource(const AnimationType *type, RStringB source);

  void InitShape();
  void DeinitShape(); // before shape is unloaded
  virtual size_t GetMemoryControlled() const;

  void InitShapeLevel(int level);
  void DeinitShapeLevel(int level);

  virtual EntityVisualState* CreateVisualState() const {return new TransportVisualState(*this);}

  virtual bool ForEachTurret(ITurretTypeFunc &func) const;
  virtual bool HasTurret() const {return _turrets.Size() > 0;}
  virtual const TurretType *GetTurret(const int *path, int size) const;

  void DrawMFDs(Transport *veh, int cb, const PositionRender & pos) const;
    
  RString GetDriverAction() const {return _driverAction;}
  RString GetDriverInAction() const {return _driverInAction;}
  RString GetDriverGetInAction() const {return _driverGetInAction;}
  RString GetDriverGetOutAction() const {return _driverGetOutAction;}

  RString GetCargoAction(int pos) const {return _cargoAction[pos];}
  RString GetCargoGetInAction(int pos) const;
  RString GetCargoGetOutAction(int pos) const;

  bool GetHideProxyInCombat() const {return _hideProxyInCombat;}
  
  bool GetUnloadInCombat() const {return _unloadInCombat;}

  __forceinline int NDriverGetInPos() const {return _driverGetInPos.Size();}
  int NCargoGetInPos(int cargoIndex) const;

  __forceinline GetInPointIndexVal GetDriverGetInPos(int i) const {return _driverGetInPos[i];}
  GetInPointIndexVal GetCargoGetInPos(int cargoIndex, int i) const;
 
  __forceinline float GetGetInRadius() const {return _getInRadius;}

  __forceinline int GetMaxManCargo() const {return _maxManCargo;}

  bool HasDriver() const {return _hasDriver;}
  bool HasGunner() const {return _hasGunner;}
  bool HasObserver() const {return _hasObserver;}
  bool HasCargo() const {return _maxManCargo>0;}
  __forceinline bool DriverIsCommander() const {return _driverIsCommander;}
  bool CanManualFire() const {return _hasGunner && _enableManualFire;}

  virtual Threat GetStrategicThreat( float distance2, float visibility, float cosAngle ) const;
  virtual Threat GetDamagePerMinute(float distance2, float visibility) const;
  virtual float GetMaxWeaponRange() const;

  RString GetCrew() const {return _crew;}
  const ViewPars *GetViewOptics() const {return &_viewOptics;}
  
  const LevelProxies *GetLevelProxies(int level) const {return (level >=0 && (size_t)level <_proxies.Length()) ? &_proxies[level] : NULL;}

  const CameraShakePars& GetCamShake() const {return _camShake;}
  float GetCamShakeCoef() const {return _camShakeCoef;}
};

typedef TransportType VehicleTransportType;

DECL_ENUM(LockState)
DECL_ENUM(LockedState)

#if _VBS2
#define UPDATE_TRANSPORT_MSG(MessageName, XX) \
  XX(MessageName, Ref<Person>, driver, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Driver person"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_STRUCTURE) \
  XX(MessageName, bool, driverLocked, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Driver position is locked"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, OLinkPerm<Person>, effCommander, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Currently commanding person"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_STRUCTURE) \
  XX(MessageName, RefArray<Person>, manCargo, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("Men in cargo space"), TRANSF_REFS, ET_NOT_EQUAL_COUNT, 0.5 * ERR_COEF_STRUCTURE) \
  XX(MessageName, AutoArray<bool>, cargoLocked, NDTBoolArray, AutoArray<bool>, NCTNone, DEFVALUEBOOLARRAY, DOC_MSG("Locked cargo positions"), TRANSF, ET_NOT_EQUAL, 0.5 * ERR_COEF_MODE) \
  XX(MessageName, TargetId, comFireTarget, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Fire target, selected by commander"), TRANSF_PTR_REF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, lock, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, LSDefault), DOC_MSG("Lock state"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, float, driverHiddenWanted, NDTFloat, float, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("Wanted position of (tank) driver - inside / outside"), TRANSF, ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, float, fuel, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Current amount of fuel"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MINOR) \
  XX(MessageName, float, UpdateCoef, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Error coefficent for multiplayer"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MINOR) \
  XX(MessageName, bool, engineOff, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Engine is off / on"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, bool, manualFire, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Commander has control over the main gun"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, respawnSide, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, TSideUnknown), DOC_MSG("Respawn side for this vehicle (disabled when TSideUnknown)"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, bool, respawning, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Vehicle respawn is in progress"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, bool, respawnFlying, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Vehicle is respawned flying"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, OLinkPermNO(AIUnit), respawnUnit, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Which unit will be respawned in this vehicle"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, float, respawnDelay, NDTFloat, float, NCTNone, DEFVALUE(float, -1.0f), DOC_MSG("Respawn delay for this vehicle"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, respawnCount, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Count of respawns to process (0 for unlimited)"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, HLAController, NDTInteger, int, NCTNone, DEFVALUE(int, LSDefault), DOC_MSG("External controlled"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE)
#else
#define UPDATE_TRANSPORT_MSG(MessageName, XX) \
  XX(MessageName, Ref<Person>, driver, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Driver person"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_STRUCTURE) \
  XX(MessageName, bool, driverLocked, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Driver position is locked"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, OLinkPerm<Person>, effCommander, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Currently commanding person"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_STRUCTURE) \
  XX(MessageName, RefArray<Person>, manCargo, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("Men in cargo space"), TRANSF_REFS, ET_NOT_EQUAL_COUNT, 0.5 * ERR_COEF_STRUCTURE) \
  XX(MessageName, AutoArray<bool>, cargoLocked, NDTBoolArray, AutoArray<bool>, NCTNone, DEFVALUEBOOLARRAY, DOC_MSG("Locked cargo positions"), TRANSF, ET_NOT_EQUAL, 0.5 * ERR_COEF_MODE) \
  XX(MessageName, TargetId, comFireTarget, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Fire target, selected by commander"), TRANSF_PTR_REF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, lock, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, LSDefault), DOC_MSG("Lock state"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, float, driverHiddenWanted, NDTFloat, float, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("Wanted position of (tank) driver - inside / outside"), TRANSF, ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, float, fuel, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Current amount of fuel"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MINOR) \
  XX(MessageName, bool, engineOff, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Engine is off / on"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, bool, manualFire, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Commander has control over the main gun"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, respawnSide, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, TSideUnknown), DOC_MSG("Respawn side for this vehicle (disabled when TSideUnknown)"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, bool, respawning, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Vehicle respawn is in progress"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, bool, respawnFlying, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Vehicle is respawned flying"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, OLinkPermNO(AIUnit), respawnUnit, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Which unit will be respawned in this vehicle"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, float, respawnDelay, NDTFloat, float, NCTNone, DEFVALUE(float, -1.0f), DOC_MSG("Respawn delay for this vehicle"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, respawnCount, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Count of respawns to process (0 for unlimited)"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, bool, enableVisionMode, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Vision modes are off / on"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, bool, allowCrewInImmobile, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("allowcrew to remain in immobile vehicle"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE)

#endif

DECLARE_NET_INDICES_EX_ERR(UpdateTransport, UpdateVehicleAIFull, UPDATE_TRANSPORT_MSG)

#define DIAG_CREW 0

//////////////////////////////////////////////////////////////////////////

/// Used for start & stop sounds. Manage switching between samples.
class StartStopHolder: public RefCount
{
private:
  RefAbstractWave _wave;

  bool _engineState;
  bool _cameraState;
  bool _stateInitilized;
  bool _wavePlaying;
  bool _waveStop;

public:
  StartStopHolder(): RefCount(), _stateInitilized(false), _wavePlaying(false), _waveStop(false) {}
  ~StartStopHolder() { Unload(); }

public:
  // play sound
  void Sound(Transport *transport);
  // wave terminated callback
  void static WaveCallBack(AbstractWave *wave, RefCount *context);

private:
  // start playing from spec. position
  bool StartPlayFromPos(Transport *transport, const SoundPars &pars, bool inside, bool reverse);
  // release wave
  void Unload();
};

struct TransportSoundContex
{
  const Transport *_transport;
  float *_ctr;
  int _length;
  bool _inside;
  float _deltaT;
  Vector3 _engPos;
  Vector3 _speed;
  float _obstruction;
  float _occlusion;
};

// class hold waves included into submix
class AbstractSoundsHolder: public RefCount
{
protected:
  AbstractSubmix *_submix;
  AutoArray<RefAbstractWave> _waves;

public:
  AbstractSoundsHolder(int count): _submix(0) { Construct(count); }
  virtual ~AbstractSoundsHolder() { Destruct(); }

protected:
  void SetDirectSound(SoundCone &cone, const Transport *transport);

private:
  void Construct(int count);
  void Destruct();
};

/* used for sound controllers - hold group of similar waves */
class SoundsHolder: public AbstractSoundsHolder
{
public:
  SoundsHolder(int count): AbstractSoundsHolder(count) {}

public:
  template<class Type>
  void UpdateSubmixWaves(const AutoArray<Type> &arr, const TransportSoundContex &context);
};

/* used for sound events - hold group of similar waves */
class SoundsEventHolder: public AbstractSoundsHolder
{
public:
  SoundsEventHolder(int count): AbstractSoundsHolder(count) {}

public:
  template<class Type>
  void UpdateSubmixWaves(const AutoArray<Type> &arr, const TransportSoundContex &context);
};

//////////////////////////////////////////////////////////////////////////

//! base class for all vehicles capable of hosting crew
/*!
  all common stuff related to transporting personnel or cargo 
  is implemented in this class  
*/

class Transport: public EntityAIFull
{
  typedef EntityAIFull base;

  friend class GetGetInTurretActionsLeader;
  friend class SoundsHolder;
  friend class StartStopHolder;

public:
  enum LandingMode
  {
    LMNone,
    LMLand,
    LMGetIn,
    LMGetOut,
  };

protected:
  Vector3 _driverPos; // commander gave command (VZero->invalid)
  Vector3 _dirWanted;
  VehicleMoveMode _moveMode;
  VehicleTurnMode _turnMode;
  bool _fireEnabled;
  bool _showDmg;
  //! vehicle commander has direct control over the weapons
  bool _manualFire;
  //! vehicle engine is off
  bool _engineOff;
  //! vehicle respawn is in progress
  bool _respawning;
  //! vehicle will respawn flying
  bool _respawnFlying;
  /// driver position is locked (nobody can get in / move to it)
  bool _driverLocked;
  /// the simulation and drawing switched to wreck
  bool _isWreck;
  //! respawn side of this vehicle (TSideUnknown if disabled)
  TargetSide _respawnSide;
  //! unit which is respawning in this vehicle
  OLinkPermNO(AIBrain) _respawnUnit;
  //! respawn delay for this vehicle
  float _respawnDelay;
  //! respawn count for this vehicle
  int _respawnCount;
  //! zooming
  Object::Zoom _zoom;

 
  /* surface info always exists as long as the bank exists */
  const SurfaceInfo *_ptrSurfInfo;
  Ref<SurfaceInfo> _surfaceInfo;
  // sound controllers
  RefArray<SoundsHolder> _sounds;
  // sound events
  RefArray<SoundsEventHolder> _soundsEvents;

  //! Coefficient to hold the engine temperature coefficient (0 - Off, 1 - fully hot)
  float _engineTemperatureCoef;
  //! Coefficient to hold the wheel temperature coefficient (0 - Off, 1 - fully hot)
  float _wheelTemperatureCoef;
  //! Coefficient to hold gun temperature coefficient, common factor for all turrets (0 - Off, 1 - fully hot)
  float _turretGunTemperatureCoef;
  //! Time when destroyed vehicle reach max heat up temperature 
  Time _destroyMaxHeatTime;

  //! when we collide with some hard object, we revert turret to neutral position
  //! so that we can get out of the crash
  Time _turretFrontUntil;

  LockState _lock;
  LockedState _locked;
  UITime _lastTestedCM;

  /// script initiated landing
  LandingMode _landing;

  float _azimutWanted;
  
  float _randomizer;

  //! Person on a place of the driver. It is not possible to set it directly, it must be done using GetIn or Swap functions that do many other things
  //! See _driverAssigned
  Ref<Person> _driver; // store driver handle (when in)
  OLinkPerm<Person> _effCommander; // who is giving commands
  
  Link<AbstractWave> _doorSound;
  RefAbstractWave _dmgSound;
  //warning of locked / incoming missiles
  RefAbstractWave _lockedSound;
  
  RefAbstractWave _engineSound,_envSound;
  //AutoArray< RefAbstractWave > _soundWaves;
  // start & stop sounds
  Ref<StartStopHolder> _holder;

  // commander must remember his decision
  FireDecision _commanderFire;

  float _driverHiddenWanted;

  ManCargo _manCargo; // transported soldiers

  // crash sound parameters
  enum CrashType {CrashNone,CrashLand,CrashWater,CrashObject,CrashTree,CrashBuilding,CrashArmor};
  CrashType _doCrash;
  float _crashVolume;
  // time of last crash sound (will not repeat for some time)
  Time _timeCrash;
  //float _crewDammage; // how much the crew is affected by dammage
  Time _getOutAfterDamage;
  Time _assignDriverAfterDamage;
  Time _assignGunnerAfterDamage;

  Time _explosionTime; // vehicle is going to explode

  //units will eject from vehicle without wheels/tracks
  bool _allowCrewInImmobile;
  //@{
  /**
  Place reservation for a given unit. It is legal when _driverAssigned is set and _driver is NULL. The mechanism that keeps consistency
  between _driver and _driverAssigned is inside the function AIGroup::GetInVehicles(). It uses standard commands to do that.
  See _driver
  */
  OLinkPermNO(AIGroup) _groupAssigned;
  OLinkPermNO(AIBrain) _driverAssigned;
  OLinkPermNOArray(AIBrain) _cargoAssigned;
  //@}

  mutable OLinkPermNOArray(AIUnit) _getoutUnits;
  mutable OLinkPermNOArray(AIUnit) _getinUnits;
  mutable Time _getinTime; // get-in time out
  mutable Time _getoutTime; // next get out allowed (door free)

  /// list of directly attached turrets (may have their own turrets)
  RefArray<Turret> _turrets;

  RadioChannel _radio;

  Time _showDmgValid;

  /// first contact detection - used for vehicle/ground or vehicle/object crashes
  float _maxContactSpeed;

  // Values for energy renormalization.
  float _oldEnergy;   //< Energy in the last simulation step,
  bool _validEnergy;  //< Was energy in last simulation step valid?
  
  // enable/disable VM
  bool _enableVisionMode;
  // current vision mode
  VisionMode _visionMode;
  // current ti mode
  int _tiIndex;

  // last time then the camera shake was performed
  Time _lastCamShakeTime;

public:
  const TransportType *Type() const { return static_cast<const TransportType *>(GetType()); }

  /// @{ Visual state
public:
  TransportVisualState typedef VisualState;
  friend class TransportVisualState;

  
  VisualState& FutureVisualState() { return static_cast<VisualState &>(base::FutureVisualState()); }
  VisualState const& FutureVisualState() const { return static_cast_checked<VisualState const &>(base::FutureVisualState());}
  VisualState const& CurrentVisualState() const { return static_cast_checked<VisualState const &>(base::CurrentVisualState());}
  const VisualState& RenderVisualState() const{ return static_cast<const VisualState &>(base::RenderVisualState()); }

  ProtectedVisualState<const VisualState> RenderVisualStateScope(bool ignoreParent=false) const {return ProtectedVisualState<const VisualState>(base::RenderVisualStateScope(ignoreParent));}
  ProtectedVisualState<VisualState> FutureVisualStateScope(bool ignoreParent=false) { return ProtectedVisualState<VisualState>(base::FutureVisualStateScope(ignoreParent)); }
  ProtectedVisualState<const VisualState> FutureVisualStateScope(bool ignoreParent=false) const { return ProtectedVisualState<const VisualState>(base::FutureVisualStateScope(ignoreParent)); }
  /// @}

  //! consume some fuel (used during simulation), return false if tank is empty
  bool ConsumeFuel(float ammount);

public:
  Transport(const EntityAIType *name, Person *driver, bool fullCreate = true);

  //! Set alive factor - typical use in cutscene
  void SetAliveFactor(float aliveFactor) {_engineTemperatureCoef = aliveFactor;}
  //! Set movement factor (wheels, trucks) - typical use in cutscene
  void SetMovementFactor(float movementFactor) {_wheelTemperatureCoef = movementFactor;}
  //! Set metabolism factor (main gun) - typical use in cutscene
  void SetMetabolismFactor(float metabolismFactor) {_turretGunTemperatureCoef = metabolismFactor;}
  //! Virtual method
  virtual float GetAliveFactor() const {return _engineTemperatureCoef;}
  //! Virtual method
  virtual float GetMovementFactor() const {return _wheelTemperatureCoef;}
  //! Virtual method
  virtual float GetMetabolismFactor() const {return _turretGunTemperatureCoef;}

  bool HasTiOptics() const;
  /// Switch to next driver vision mode
  bool NextVisionMode();
  /// Check if  driver optics support vision modes
  bool HasVisionModes() const;
  int VisionModesCount() const;
  /// Get currently driver set mode and ti index
  VisionMode GetVisionModePars(int &flirIndex);
  /// Allow / deny VM use
  void EnableVisionModes(bool val);
  /// VM allowed
  bool VisionModesEnabled() const {return _enableVisionMode;}
  bool GetPostProcessEffects(const TurretContextV &context, AutoArray<PPEffectType> **ppEffects) const;
  void CleanUp();
  void Simulate( float deltaT, SimulationImportance prec );

  void SetAllowCrewInImmobile(bool allow);

protected:
  Vector3 WreckFriction(Vector3Par speed);
  void ObjectCollisionWithPerson(CollisionBuffer objColl, unsigned int fromIndx, unsigned int toIndx, float& appliedImpulseObj);
  /// Simplified physical simulation, used for the destroyed vehicle
  void SimulateWreck(float deltaT, SimulationImportance prec);

public:
  /// Check if the simulation is switched to wreck
  bool IsWreck() const {return _isWreck;}

  virtual void StabilizeTurrets(Matrix3Val oldTrans, Matrix3Val newTrans);

  /// make sure crew moves their heads as necessary
  void MoveCrewHead();

  TargetSide GetRespawnSide() const {return _respawnSide;}
  void SetRespawnSide(TargetSide side) {_respawnSide = side;}

  AIBrain *GetRespawnUnit() const {return _respawnUnit;}
  void SetRespawnUnit(AIBrain *unit) {_respawnUnit = unit;}

  bool IsRespawning() const {return _respawning;}
  void SetRespawning(bool set) {_respawning = set;}

  bool IsRespawnFlying() const {return _respawnFlying;}
  void SetRespawnFlying(bool set) {_respawnFlying = set;}

  float GetRespawnDelay() const {return _respawnDelay;}
  void SetRespawnDelay(float delay) {_respawnDelay = delay;}

  int GetRespawnCount() const {return _respawnCount;}
  void SetRespawnCount(int count) {_respawnCount = count;}

  virtual float GetFuel() const { return FutureVisualState().GetFuel(); }
  virtual float GetRenderVisualStateFuel() const { return RenderVisualState().GetFuel(); }
  void Refuel(float ammount);

  virtual void Repair(float amount=1.0f);

  // @{ sound controllers
  // revolutions per minute
  virtual float GetRPM() const {return 0.0f;}
  virtual float GetRenderVisualStateRPM() const {return 0.0f;}
  // random value from range [0, 1], used for small differences in vehicle frequencies
  virtual float GetRandomizer() const {return _randomizer;}
  // camera position - values are from range [0, 1], where inside = 0, outside = 1
  virtual float GetCamPos() const;
  // vehicle thrust
  virtual float Thrust() const {return 0.0f;}
  // engine state, discrete values 0 = engine OFF, 1 = engine ON
  virtual float GetEngineOn() const {return _engineOff ? 0.0f : 1.0f;}

  // main rotor rotor speed
  virtual float GetRotorSpeed() const {return 0.0f;}
  // main rotor thrust
  virtual float GetMainRotorThrust() const {return 0.0f;}
  // tank angular velocity
  virtual Vector3 GetAngularVelocity() const {return VZero;}
  // flaps position normally - 0, down 1x- 0.5, down 2x - 1
  virtual float GetFlapPos() const {return 0.0f;}
  // horizontal -1
  virtual float GetThrustVectorPos() const {return 0.0f;}
  // outside 0, inside 1
  virtual float GetGearPos() const {return 0.0f;}
  // rotor position
  virtual float GetRotorPos() const {return 0.0f;}
  // vehicle in water
  float GetWaterContact() const { return _waterContact; }
  // damper simulation access for sound
  float GetDamperState(int index) const { return RenderVisualState().GetDamperState(index); }
  // @}

  virtual float GetSpeed() const;

  virtual bool GetAutoHover() {return false;}
  
  // @{ surface characteristics
  float GetRoughness() const {return _ptrSurfInfo ? _ptrSurfInfo->_roughness : 0.0f;}
  float GetDustness() const {return _ptrSurfInfo ? _ptrSurfInfo->_dustness : 0.0f;}
  float GetRockness() const {return GetSurfaceSounds(SurfSRockness);}
  float GetSandness() const {return GetSurfaceSounds(SurfSSandness);}
  float GetGrassness() const {return GetSurfaceSounds(SurfSGrassness);}
  float GetMudness() const {return GetSurfaceSounds(SurfSMudness);} 
  float GetGravelness() const {return GetSurfaceSounds(SurfSGravelness);}
  float GetAsphaltness() const {return GetSurfaceSounds(SurfSAsphaltness);}

  // sounds surface
  virtual float GetSurfaceSounds(SufraceSound type) const {return _ptrSurfInfo ? _ptrSurfInfo->GetSurfSound(type) : 0.0f;}
  // @}

  // @{ directional sound params
  virtual Vector3 GetFrontOrientation() const {return -VForward;}
  Vector3 GetTopOrientation() const {return VUp;}
  // @}
  
  void SetSurfaceInfo(GroundCollisionBuffer &groundCollBuffer);
  // internal sound coefficient
  float GetInsideSoundCoeff() const { return (Type()->_insideSoundCoef); }
  // filtering surrounding sounds when camera is located inside vehicle
  bool EnableOutsideSoundFilter() const { return (Type()->_outsideSoundFilter); }
  /// check if the vehicle has a compass device
  bool GetEnableCompass(AIBrain *unit) const;

  float GetSpeedHorizontal() const { return RenderVisualState().GetSpeedHorizontal(); }
  float GetHorizonBank() const { return RenderVisualState().GetHorizonBank(); }  // also used in HudDesc
  float GetHorizonDive() const { return RenderVisualState().GetHorizonDive(); }  // also used in HudDesc
  float GetGMeterZ() const { return RenderVisualState().GetGMeterZ(); }

  /// @{ Visual controllers (\sa Type()->CreateAnimationSource())
  float GetCtrlAltRadar(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetAltRadar(_shape); }
  float GetCtrlAltBaro(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetAltBaro(); }
  float GetCtrlSpeedHorizontal(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetSpeedHorizontal(); }
  float GetCtrlSpeedVertical(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetSpeedVertical(); }
  float GetCtrlDirection(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetDirection(); }
  float GetCtrlTurretDirection(ObjectVisualState const& vs) const;
  float GetCtrlHorizonBank(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetHorizonBank(); }
  float GetCtrlHorizonDive(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetHorizonDive(); }
  float GetCtrlFuel(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetFuelFraction(Type()); }
  float GetCtrlGMeter(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetGMeter(); }
  float GetCtrlGMeterX(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetGMeterX(); }
  float GetCtrlGMeterY(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetGMeterY(); }
  float GetCtrlGMeterZ(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetGMeterZ(); }
  float GetCtrlDriverHatchPos(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetDriverHatchPos(); }

  // virtual controllers
  virtual float GetCtrlRPM(ObjectVisualState const& vs) const {return 0.0f;}  // overridden in TankOrCar, Airplane, Helicopter, Motorcycle, Ship
  virtual float GetCtrlDrivingWheelPos(ObjectVisualState const& vs) const {return 0.0f;}  // overridden in Car, Motorcycle, Ship
  // @}

  VehicleMoveMode GetMoveMode() const {return _moveMode;}
  VehicleTurnMode GetTurnMode() const {return _turnMode;}

  void PlaceOnSurface(Matrix4 &trans);

  virtual Vector3 GetCameraDirection( CameraType camType ) const;
  Matrix4 InternalCameraTransform(Matrix4 &base, CameraType camType) const;
  virtual Matrix4 InsideCamera( CameraType camType ) const;
  //! Determine what LOD to use for the vehicle player is in
  virtual int InsideLOD( CameraType camType ) const;
  //! Determine what LOD to use for the vehicle player is in and when not using optics
  virtual int InternalLOD(UIActionType position) const;

  Matrix4 TurretTransform(const TurretType &type) const;
  Matrix4 GunTurretTransform(ObjectVisualState const& vs, const TurretType &type) const;

  Vector3 GetEyeDirection(ObjectVisualState const& vs, const TurretContext &context) const;
  Vector3 GetEyeDirectionWanted(ObjectVisualState const& vs, const TurretContextEx &context) const;
  Vector3 GetLookAroundDirection(ObjectVisualState const& vs, const TurretContext &context) const;

  bool IsVirtual( CameraType camType ) const;
  bool IsVirtualX( CameraType camType ) const;

  virtual void PreloadGetIn(UIActionType position);

  /// prepare getting out - open doors, ...
  virtual bool PrepareGetOut(AIBrain *unit){return true;}

  /// finish getting out - close doors, ...
  virtual void FinishGetOut(AIBrain *unit){}

  /// prepare getting in - open doors, ...
  virtual bool PrepareGetIn(UIActionType pos, Turret *turret, int cargoIndex){return true;}

  /// finish getting in - close doors, ...
  virtual void FinishGetIn(UIActionType pos, Turret *turret, int cargoIndex){}

  virtual bool IsGunner( CameraType camType ) const;

  virtual void LimitVirtual(CameraType camType, float &heading, float &dive, float &fov) const;
  virtual void InitVirtual(CameraType camType, float &heading, float &dive, float &fov) const;
  virtual void OverrideCursor(Vector3 &dir) const;

  void EngineOn();
  void EngineOff();
  virtual bool EngineIsOn() const {return !_engineOff;}
  virtual bool IRSignatureOn() const {return !_engineOff;}
  //! some transport types may have no engine or may have it always on
  bool EngineCanBeOff() const {return true;}

  void Init( Matrix4Par pos, bool init );
  void InitUnits();

  bool ValidateCrew(Person *crew, bool complex=true) const;
  bool Validate(bool complex=true) const;
  virtual void TrackTargets(TargetList &res, bool initialize, float trackTargetsPeriod[NTgtCategory]);
  virtual void TrackOneTarget(TargetList &res, Target *target, float trackTargetsPeriod);

  void SetTotalDamageHandleDead(float damage);
  void ReactToDamage();

  // @{ locking of positions in the vehicle
  bool IsDriverLocked() const {return _driverLocked;}
  void LockDriver(bool lock) {_driverLocked = lock;}
  bool IsCargoLocked(int i) const {return _manCargo.IsLocked(i);}
  void LockCargo(int i, bool lock) {_manCargo.Lock(i, lock);}
  //@}

  /// check if current situation is suitable for ejection
  virtual bool CheckEject() const;

  //! eject given crew member
  bool EjectCrew(Person *man);
  //! eject given crew member if he is dead
  bool EjectIfDead(Person *man);
  //! eject given crew member if he is alive
  bool EjectIfAlive(Person *man);

  //! eject all alive persons in cargo 
  bool EjectIfAliveCargo();
  //! eject all dead persons in cargo 
  bool EjectIfDeadCargo();

  //! move unit from cargo to driver position
  bool AssignCargoToDriver();
  //! move unit from cargo to gunner position
  bool AssignCargoToGunner();

  //! eject all crew member that are not fixed
  void EjectAllNotFixed();

  bool HasSomePilot() const; // false if no pilot - vehicle should brake

  /// enumerate all turrets
  virtual bool ForEachTurret(ITurretFunc &func) const;
  /// enumerate all turrets
  virtual bool ForEachTurret(EntityVisualState& vs, ITurretFuncV &func) const;
  /// enumerate all turrets
  virtual bool ForEachTurretEx(EntityVisualState& vs, ITurretFuncEx &func) const;
  /// enumerate all crew
  virtual bool ForEachCrew(ICrewFunc &func) const;

#if _VBS3 //Commander override
  bool FindOpticsTurret(const Person *gunner, TurretContext &context) const;
  bool FindControlledTurret(const Person *gunner, TurretContext &context) const;
#endif

  virtual bool FindTurret(const Person *gunner, TurretContext &context) const;
  virtual bool FindTurret(EntityVisualState& vs, const Person *gunner, TurretContextV &context) const;
  virtual bool FindTurret(const Turret *turret, TurretContext &context) const;
  virtual bool FindTurret(EntityVisualState& vs, const Turret *turret, TurretContextV &context) const;
  bool FindAssignedTurret(const AIBrain *brain, TurretContext &context) const;
  bool FindAssignedTurret(EntityVisualState& vs, const AIBrain *brain, TurretContextV &context) const;
  virtual bool GetPrimaryGunnerTurret(TurretContext &context) const;
  virtual bool GetPrimaryGunnerTurret(EntityVisualState &vs, TurretContextV &context) const;
  virtual bool GetPrimaryGunnerTurret(EntityVisualState &vs, TurretContextEx &context) const;
  virtual bool GetPrimaryObserverTurret(TurretContext &context) const;
  virtual bool GetPrimaryObserverTurret(EntityVisualState &vs, TurretContextV &context) const;
  virtual bool GetPrimaryObserverTurret(EntityVisualState &vs, TurretContextEx &context) const;

  bool GetDriverTurret(TurretContext &context) const;

  virtual bool CanFireUsing(const TurretContext &context) const;

  virtual bool GetWeaponAim(TurretContext context, Vector3 &position, Vector3 &direction);


  void SimulateAI(float deltaT, SimulationImportance prec);  // false if no pilot
  void CrashDammage(float ammount, const Vector3 &pos=VZero, float range=0.0f);

  virtual void SelectWeaponCommander(AIBrain *unit, const TurretContext &context, int weapon);

  void SimulateGunner(TurretContextEx &context, float deltaT, SimulationImportance prec);
  void SimulateObserver(TurretContextEx &context, float deltaT, SimulationImportance prec);

#if _ENABLE_AI
  virtual void AIObserver(AIBrain *unit, float deltaT);
  virtual void AICommander(AIBrain *unit, float deltaT);
  virtual void AIPilot(AIBrain *unit, float deltaT) {}
  virtual void AIGunner(TurretContextEx &context, float deltaT);
  /// helper function called from AIGunner
  virtual void CommandPrimaryGunner(AIBrain *unit, TurretContext &context);
#endif

  virtual void KeyboardPilot(AIBrain *unit, float deltaT) {}
  virtual void SuspendedPilot(AIBrain *unit, float deltaT) {}
  virtual void KeyboardAny(AIUnit *unit, float deltaT);
  
  // KeyboardCommander and KeyboardGunner are partially realized in InGameUI
  virtual void KeyboardObserver(TurretContextEx &context, float deltaT);
  virtual void KeyboardGunner(TurretContextEx &context, float deltaT);
  virtual void KeyboardLookAround(AIBrain *unit, float deltaT);

  virtual Vector3 AdjustWeapon(const TurretContext &context, int weapon, float fov, Vector3 direction) const;
  virtual bool GIsManual(const Person *gunner) const;

  virtual CombatMode GetCombatModeAutoDetected(const AIBrain *brain) const;
  
  void LookAround(ValueWithCurve aimX, ValueWithCurve aimY, float deltaT, const ViewPars *viewPars, AIBrain *unit, bool forceLookAround=false, bool forceDiveAround=false);
  // UpdateZoom gets LockTarget action and update zoom
  void UpdateZoom(AIBrain *unit);
  // get current zoom for the player (from transport or from appropriate turret)
  virtual float GetCameraFOV() const;
  Object::Zoom *GetZoom();

#if _VBS3 // Airborne() function moved up to class Transport
  virtual bool Airborne() const {return !_landContact;}
  //! Get weapon position (where the projectiles leave the weapon)
  virtual Vector3 GetWeaponPoint(const TurretContext &context, int weapon) const;
#endif

  virtual void SetObserverSpeed(const TurretContextEx &context, float speedX, float speedY, float deltaT) {}

  /// opportunity to perform stabilization after network update
  virtual void OnTurretUpdate(Turret *turret, Matrix3Par oldOrient, Matrix3Par newOrient){}
  virtual bool IsTurretLocked(const TurretContextV &context) const {return false;}
  bool IsGunnerLookingAround(const TurretContextV &context) const;

  /// get turret based on path in hierarchy
  Turret *GetTurret(const int *path, int size);
  /// get turret based on path in hierarchy
  const Turret *GetTurret(const int *path, int size) const;

  /// get path in hierarchy for given turret
  template <class Allocator>
  bool GetTurretPath(AutoArray<int, Allocator> &path, const Turret *turret) const
  {
    for (int i=0; i<_turrets.Size(); i++)
    {
      path.Resize(1);
      path[0] = i;
      if (_turrets[i]->GetTurretPath(path, turret)) return true;
    }
    return false;
  }

  void FindDriverPath(Vector3Par pos, bool forceReplan);
  void DriverPathPilot
  (
    AIBrain *unit, SteerInfo &info,
    float speedCoef, Vector3Par instantPos
  );

  virtual void FakePilot( float deltaT ) {};

  // prepare for drawing
  AnimationStyle IsAnimated( int level ) const; // appearance changed with Animate
  void Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2);
  void Deanimate( int level, bool setEngineStuff );
  
  virtual bool PreloadMuzzleFlash(const WeaponsState &weapons, int weapon) const;
  
  virtual void AnimateManProxyMatrix(ObjectVisualState const& vs, int level, const ManProxy &proxy, Matrix4 &proxyTransform) const;
  
  bool IsManualFire() const {return _manualFire;}

  /**
  0.5 decision value seems natural, but it causes closing hatches hitting
  the proxies which are still outside
  see also Turret::IsGunnerHidden 
  */
  bool IsPersonHidden(ObjectVisualState const& vs, Person *person) const;
  void HideDriver(float hide=1.0f) 
  {  
#if _VBS3
    if(_driverHiddenWanted != hide ) hide == 1.0 ? OnHideEvent(EETurnIn,_driver) : OnHideEvent(EETurnOut,_driver);
#endif
    _driverHiddenWanted = hide;
  }
  void HidePerson(Person *person, float hide=1.0f);

  float GetDriverHidden() const {return FutureVisualState()._driverHidden;}
  float GetDriverHiddenWanted() const {return _driverHiddenWanted;}

  int GetDriverCompartments() const {return Type()->_driverCompartments;}
  int GetCargoCompartments(int index) const;
  int GetPersonCompartments(Person *person) const;

  //@{
  //!Get action corresponding to given position in vehicle
  virtual RString DriverAction() const;
  virtual RString CargoAction(int position) const;

  virtual float DriverAnimSpeed() const;
  virtual float CargoAnimSpeed(int position) const;
  //@}

  virtual UserAction GetManualFireAction() const {return (UserAction)-1;}

  virtual LODShapeWithShadow *GetOpticsModel(const Person *person) const;
  virtual bool GetEnableOptics(ObjectVisualState const& vs, const Person *person) const { return false; }
  virtual PackedColor GetOpticsColor(const Person *person);
  virtual bool GetForceOptics(const Person *person, CameraType camType) const; 
  virtual bool PreloadView(Person *person, CameraType camType) const;

  virtual bool ShowAim(const TurretContextV &context, int weapon, CameraType camType, Person *person) const;
  virtual bool ShowCursor(const TurretContextV &context, int weapon, CameraType camType, Person *person) const;

  virtual void DrawDiags();
  void DrawCameraCockpit();

  bool NeedsPrepareProxiesForDrawing() const;
  void PrepareProxiesForDrawing(
    Object *rootObj, int rootLevel, const AnimationContext &animContext,
    const PositionRender &transform, int level, ClipFlags clipFlags, SortObject *so
    );
  /// prepare single crew member for drawing
  void PrepareCrewMemberForDrawing(
    Object *rootObj, int rootLevel, const AnimationContext &animContext, const PositionRender &transform, int level, ClipFlags clipFlags, SortObject *so,
    const ManProxy &proxy, Person *man);

  int GetCrewMemberComplexity(
    int level, const Matrix4 &pos, float dist2,
    const ManProxy &proxy, Person *man
  ) const;
  
  bool PreloadCrewMember(const ManProxy &proxy, Person *man, float dist2) const;
  bool PreloadProxies(int level, float dist2, bool preloadTextures = false) const;
  
  virtual int GetProxyComplexity(int level, const Matrix4 &pos, float dist2) const;
  Matrix4 FindMissilePos(int index, bool &found) const;

  int PassNum( int lod );

  //! Determine whether volume shadows should be casted in pass3
  virtual bool CastPass3VolShadow(int level, bool zSpace, float &castShadowDiff, float &castShadowAmb) const;

  //! Virtual method
  virtual void Draw(
    int cb, int level, const SectionMaterialLODs &matLOD, ClipFlags clipFlags, const DrawParameters &dp,
    const InstanceParameters &ip, float dist2, const PositionRender &pos, SortObject *oi
  );


  // proxy access
  virtual bool CastProxyShadow(int level, int index) const;

  virtual int GetProxyCount(int level) const;
  virtual Object *GetProxy(
    VisualStateAge age, LODShapeWithShadow *&shape, Object *&parentObject,
    int level, Matrix4 &transform, const Matrix4 &parentPos, int i
  ) const;

  virtual Matrix4 ProxyWorldTransform(VisualStateAge age, const Object *obj) const;
  virtual Matrix4 ProxyInvWorldTransform(VisualStateAge age, const Object *obj) const;


  Texture *GetFlagTexture();

  /// helper for AimingPosition creation
  Vector3 AimingPositionCrew(ObjectVisualState const& vs, const AmmoType *ammo, Vector3Par aimFromDir) const;
  bool GetOpticsCamera(Matrix4 &transf, CameraType camType) const; // returns true if transf ok
  bool GetProxyCamera(Matrix4 &transf, CameraType camType) const; // returns true if transf ok

  virtual Vector3 AimingPosition(ObjectVisualState const& vs, const AmmoType *ammo=NULL, Vector3Par aimFromDir=VZero) const override;
  
  /// used to provide a way how to calculate camera position for external camera
  virtual Vector3 CameraPointRel(CameraType camType) const;

  void PerformFF( FFEffects &effects );

  void Sound( bool inside, float deltaT );
  void UnloadSound();

  bool ListenerIsInside(Person *listener) const;

  virtual void OccludeAndObstructSound(EntityAI *focus, float &occlusion, float &obstruction, ObstructType soundInside) const;
  virtual float GetEngineVol( float &freq ) const {freq=1;return 0;}
  virtual float GetEnvironVol( float &freq ) const {freq=1;return 0;}

  virtual float GetSoundsVol(float &freq, const SoundParsEx &soundPars, float* evalControllers, int length) const;
  virtual float EvaluateSoundEvent(const SoundEventPars &soundEPars, float* evalControllers, int length) const;
  const SoundPars& GetEngineSoundPars(bool inside);

  virtual Vector3 GetEnginePos() const; // sound source position in model coord
  virtual Vector3 GetEnvironPos() const; // sound source position in model coord
    
  AIBrain *DriverBrain() const;
  AIBrain *GunnerBrain() const;
  AIBrain *ObserverBrain() const;

  AIBrain *CommandingGunner() const;

  virtual AIBrain *PilotUnit() const;
  virtual AIBrain *GunnerUnit() const;
  virtual AIBrain *ObserverUnit() const;
  virtual AIBrain *CommanderUnit() const;
  virtual AIBrain *EffectiveGunnerUnit(const TurretContext &context) const;
  virtual AIBrain *EffectiveObserverUnit() const;

  TargetSide GetTargetSide() const;

  bool IsCrewed() const;

  TargetSide GetTargetSideAsIfAlive() const;

  float VisibleLights() const; // visibility

  virtual bool QIsManual() const;
  virtual bool QIsManualMovement() const;
  bool QIsManual(const AIBrain *unit) const;

  RadioChannel &GetRadio() {return _radio;}
  const RadioChannel &GetRadio() const {return _radio;}

  VehicleMoveMode GetMoveMode() {return _moveMode;}
  bool IsFireEnabled(const TurretContext &context);

//  bool IsLocked() const {return _locked;}
//  void SetLocked(bool lock) {_locked = lock;}
  LockState GetLock() const {return _lock;}
  void SetLock(LockState lock) {_lock = lock;}

  virtual bool LaunchCM(Missile *shot, Person *person, bool forced = false);

  virtual void OnIncomingMissile(EntityAI *target, Shot *shot, EntityAI *owner);
  virtual void OnWeaponLock(EntityAI *target,Person *gunner, bool locked);
  
  virtual bool StopAtStrategicPos() const;

#if _ENABLE_AI
  virtual bool FormationPilot(SteerInfo &info);
  virtual void LeaderPilot(SteerInfo &info);
  void CommandPilot(SteerInfo &info);
  virtual bool ReverseLeftRightAIPilot() {return false;}
#endif

  private:
  float CalculateDriverPos(float &headChange, float minDist, Vector3 &instant);
  bool CanBeCommander(AIBrain *unit) const;

  public:
#if _VBS2 //Convoy trainer
    void Transport::SimulatePost( float deltaT, SimulationImportance prec );
#endif
  // void UpdatePath(Vector3Par dest);

  void ShowDamage(int part, EntityAI *killer);

  // crew channel xmit done
  void ReceivedMove( Vector3Par tgt );
  void ReceivedTarget(Person *receiver, Target *tgt);
  void ReceivedFire(Person *receiver, Target *tgt);
  void ReceivedJoin();
  void ReceivedSimpleCommand(Person *receiver, SimpleCommand cmd);
  void ReceivedLoad(Person *receiver, int weapon);
  void ReceivedAzimut(float azimut);
  void ReceivedStopTurning(float azimut);
  void ReceivedLoadMagazine(Person *receiver, CreatorId creator, int id, RString weapon, RString muzzle);
  void ReceivedWatchTgt(Person *receiver, Target *tgt);
  void ReceivedWatchPos(Person *receiver, Vector3Par pos);

  // xmit into crew channel
  void SendMove( Vector3Par pos );
  void SendTarget(Person *receiver, Target *tgt);
  void SendFire(Person *receiver, Target *tgt);
  void SendJoin();
//  void SendCeaseFire();
  void SendSimpleCommand(Person *receiver, SimpleCommand cmd);
  void SendLoad(Person *receiver, int weapon);
  void SendAzimut(float azimut);
  void SendStopTurning(float azimut);
  void SendFireFailed(Person *sender);
  void SendLoadMagazine(Person *receiver, CreatorId creator, int id, RString weapon, RString muzzle);
  void SendWatchTgt(Person *receiver, Target *tgt);
  void SendWatchPos(Person *receiver, Vector3Par pos);

  void CommanderLoadMagazine(CreatorId creator, int id, RString weapon, RString muzzle);

  OLinkPermNOArray(AIUnit) &WhoIsGettingOut() {return _getoutUnits;}
  OLinkPermNOArray(AIUnit) &WhoIsGettingIn() {return _getinUnits;}

  const OLinkPermNOArray(AIUnit) &WhoIsGettingOut() const {return _getoutUnits;}
  const OLinkPermNOArray(AIUnit) &WhoIsGettingIn() const {return _getinUnits;}

  void GetInStarted( AIUnit *unit );
  void GetOutStarted( AIUnit *unit );
  void GetInFinished( AIBrain *unit ); // done/canceled
  void GetOutFinished( AIBrain *unit ); // done/canceled

  void WaitForGetIn(AIUnit *unit);
  void WaitForGetOut(AIUnit *unit);
  virtual void LandStarted(LandingMode landing);
  void LandFinished() {_landing = LMNone;}

  virtual bool IsStopped() const;
  bool CanCancelStop() const;

  Time GetGetOutTime() const {return _getoutTime;}
  void SetGetOutTime( Time time ){_getoutTime=time;}

  Time GetGetInTimeout() const {return _getinTime;}
  void SetGetInTimeout(Time time) {_getinTime = time;}

  GetInPoint GetUnitGetInPos(AIBrain *unit) const;
  GetInPoint GetUnitGetOutPos(AIBrain *unit) const;

  AIGroup *GetGroupAssigned() const {return _groupAssigned;}
  AIBrain *GetDriverAssigned() const {return _driverAssigned;}
  AIBrain *GetGunnerAssigned() const;
  AIBrain *GetCommanderAssigned() const;
  AIBrain *GetCargoAssigned(int i) const {return _cargoAssigned[i];}
  void RemoveAssignement(AIBrain *unit);
  void AssignGroup(AIGroup *grp) {_groupAssigned = grp;}
  void AssignDriver(AIBrain *unit) {_driverAssigned = unit;}
  void AssignGunner(AIBrain *unit);
  void AssignCommander(AIBrain *unit);
  void AssignCargo(int index, AIBrain *unit);
  void LeaveCargo(AIGroup *grp);

  Person *Driver() const {return _driver;}
  Person *Observer() const;
  Person *Gunner() const;

  void DriverConstruct( Person *driver );
        
  virtual void SimulateImpulseDamage(DoDamageResult &result, EntityAI *owner, float impulse, Vector3Par modelPos);

  private:
  void GetInAny(Person *driver, bool sound, const char *name);
  void GetOutAny(const Matrix4 & outPos, Person *driver, bool sound, const char *name, RString getOutAction);

protected:
  virtual GetInPoint GetDefaultGetInPos(Person *person, Vector3Par pos) const;
  virtual GetInPoint GetDriverGetInPos(Person *person, Vector3Par pos) const;
  virtual GetInPoint GetCommanderGetInPos(Person *person, Vector3Par pos) const;
  virtual GetInPoint GetGunnerGetInPos(Person *person, Vector3Par pos) const;
  virtual GetInPoint GetTurretGetInPos(Turret *turret, Person *person, Vector3Par pos) const;
  virtual GetInPoint GetCargoGetInPos(int cargoIndex, Person *person, Vector3Par pos) const;

  virtual GetInPoint GetDriverGetOutPos(Person *person) const;
  virtual GetInPoint GetCommanderGetOutPos(Person *person) const;
  virtual GetInPoint GetGunnerGetOutPos(Person *person) const;
  virtual GetInPoint GetCargoGetOutPos(Person *person) const;

  float GetGetInRadius() const {return Type()->GetGetInRadius();}

  /// helper function to return get in point in model coordinates
  GetInPoint AnimateGetInPoint(GetInPointIndexVal ptIndex) const;

  public:
  virtual GetInPoint GetTurretGetOutPos(Turret *turret, Person *person) const;

  virtual Vector3 GetStopPosition() const;

  virtual void GetInDriver( Person *driver, bool sound=true);
  void GetOutDriver( const Matrix4 & outPos, bool eject=false, bool sound=true);
  bool QIsDriverIn() const {return _driver!=NULL;}

  void GetInGunner( Person *man, bool sound=true );
  void GetOutGunner( const Matrix4 &outPos, bool sound=true);
  bool QIsGunnerIn() const {return Gunner() != NULL;}

  void GetInCommander( Person *man, bool sound=true);
  void GetOutCommander( const Matrix4 &outPos, bool sound=true);
  bool QIsCommanderIn() const {return Observer() != NULL;}

  void GetInTurret(Turret *turret, Person *man, bool sound = true);
  void GetOutTurret(TurretContext &context, const Matrix4 &outPos, bool sound = true);

  /// called when a person becomes a leader or the vehicle after get-in
  void ControlTakenBy(Person *man);

  /// check if crew can be attacked with small arms
  virtual bool CheckCrewVulnerable() const;

  virtual void GetInCargo( Person *driver, bool sound=true, int posIndex=-1);
  void GetOutCargo( Person *driver, const Matrix4 &outPos, bool sound=true);
  const ManCargo &GetManCargo() const {return _manCargo;}
  int GetManCargoSize() const;
  int GetFreeManCargo() const;

  int GetMaxManCargo() const {return Type()->_maxManCargo;}
  float GetMaxFuelCargo() const {return Type()->_maxFuelCargo;}
  float GetMaxRepairCargo() const {return Type()->_maxRepairCargo;}
  float GetMaxAmmoCargo() const {return Type()->_maxAmmoCargo;}
  bool IsAttendant() const {return Type()->_attendant;}
  bool IsEngineer() const {return Type()->_engineer;}

  float GetFreeFuelCargo() const {return Type()->_maxFuelCargo-SUPPLY(this,GetFuelCargo,());}
  float GetFreeRepairCargo() const {return Type()->_maxRepairCargo-GetRepairCargo();}
  float GetFreeAmmoCargo() const {return Type()->_maxAmmoCargo-GetAmmoCargo();}

  virtual float GetExplosives() const; // how much explosives is in

  float NeedsLoadFuel() const;
  float NeedsLoadAmmo() const;
  float NeedsLoadRepair() const;

  // if there is no driver, you can get in
  virtual bool IsPossibleToGetIn() const;
  virtual bool IsWorking() const;
  virtual bool IsAbleToMove() const;
  virtual bool IsAbleToFire(const TurretContext &context) const;
  virtual bool IsAbleToFireAnyTurret() const;
  virtual bool EjectNeeded() const;

  virtual float CalculateTotalCost() const;
  
  virtual bool GetActionParams(UIActionParams &params, const Action *action, AIBrain *unit) const;
  virtual void PerformAction(const Action *action, AIBrain *unit);

  /// initiate process of getting out
  void StartGetOutActivity(AIBrain *unit, bool eject);

  virtual void GetActions(UIActions &actions, AIBrain *unit, bool now, bool strict);
  void GetGetInActions(UIActions &actions, AIBrain *unit, bool now, bool strict);

  virtual bool CheckDriverGetInPosition(Person *person, float maxDist2, float &dist2, bool strict) const;
  virtual bool CheckCommanderGetInPosition(Person *person, float maxDist2, float &dist2, bool strict) const;
  virtual bool CheckGunnerGetInPosition(Person *person, float maxDist2, float &dist2, bool strict) const;
  virtual bool CheckTurretGetInPosition(Turret *turret, Person *person, float maxDist2, float &dist2, bool strict) const;
  virtual bool CheckCargoGetInPosition(int cargoIndex, Person *person, float maxDist2, float &dist2, bool strict) const;
  virtual bool CheckUnitGetInPosition(Person *person, float maxDist2, float &dist2, bool strict) const;

  virtual Threat GetDamagePerMinute(float distance2, float visibility) const;

  /// is there any commander for the vehicle?
  Person *CommanderPerson() const;

  /// check if the unit can switch position to the driver
  bool CanMoveToDriver(AIBrain *unit, int currentCompartments) const;
  /// check if the unit can switch position to the turret
  bool CanMoveToTurret(TurretContextV &context, AIBrain *unit, int currentCompartments) const;
  /// check if the unit can switch position to the cargo
  bool CanMoveToCargo(int index, AIBrain *unit, int currentCompartments) const;

  /// check if the unit can switch position in the vehicle
  bool CanChangePosition(AIBrain *unit) const;

  void ChangePosition(const Action *action, Person *soldier, bool netAware = false, bool forced = false);

  bool QCanIBeIn( Person *who ) const;

  virtual bool QCanIGetIn( Person *who=NULL ) const; // TODO: remove who argument
  virtual bool QCanIGetInGunner( Person *who=NULL ) const; // TODO: remove who argument
  virtual bool QCanIGetInCommander( Person *who=NULL ) const; // TODO: remove who argument
  virtual bool QCanIGetInTurret(Turret *turret, Person *who = NULL) const; // TODO: remove who argument
  /// check if person can get in specified cargo position (-1 for any), return the index of free cargo position (-1 if none exist)
  virtual int QCanIGetInCargo(Person *who = NULL, int index = -1) const; // both cargo and driver will do
  virtual bool QCanIGetInAny( Person *who=NULL ) const; // both cargo and driver will do

  virtual void Respawn();
  virtual void DamageCrew( EntityAI *killer, float howMuch, RString ammo );
  virtual void HitBy( EntityAI *owner, float howMuch, RString ammo, bool wasDestroyed );
  virtual void Destroy( EntityAI *killer, float overkill, float minExp, float maxExp  ); // overkill==1 means no overkill
  virtual void Eject( AIBrain *unit, Vector3Val diff = VZero); // eject is possible, diff - move person from getinpos...
  virtual void Land(); // start landing autopilot
  virtual void CancelLand(); // start landing autopilot

  virtual void GetAimActions(ValueWithCurve &aimX, ValueWithCurve &aimY) const {}

  virtual LSError Serialize(ParamArchive &ar);

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);

  virtual void CreateSubobjects();

  void DestroyObject();

  virtual Time GetDestroyedTime() const;

  virtual void ResetStatus();

  void UpdateStopTimeout();

  USE_CASTING(base)
  INHERIT_SOFTLINK(Transport,base);

};

typedef Transport VehicleTransport;

#endif
