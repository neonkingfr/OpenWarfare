// shots and explosions simulation

#include "wpch.hpp"
#include "laserTarget.hpp"
#include "AI/ai.hpp"
#include "Network/network.hpp"
#include "scene.hpp"
#include "landscape.hpp"
#include "camera.hpp"
#include "global.hpp"
#include "diagModes.hpp"

LaserTargetType::LaserTargetType( ParamEntryPar param )
:base(param)
{
}

void LaserTargetType::Load(ParamEntryPar par, const char *shape)
{
	base::Load(par,shape);
}

void LaserTargetType::InitShape()
{
	base::InitShape();
	_shape->OrSpecial(IsColored|ZBiasMask|IsAlpha|IsAlphaFog);
	if (_shape->NLevels()>=2)
	{
		ShapeUsed lock = _shape->Level(1);
    Shape *points = lock.InitShape();
		points->ExpandClip();
		for (int i=0; i<points->NVertex(); i++)
		{
			ClipFlags clip = points->Clip(i);
			clip &= ~ClipLightMask;
			clip |= ClipLightLine;
			points->SetClip(i,clip);
		}
		points->CalculateHints();
		_shape->CalculateHints();
	}
}


Object* LaserTargetType::CreateObject(bool unused) const
{ 
  return new LaserTarget(this, unused);
}


DEFINE_CASTING(LaserTarget)

LaserTarget::LaserTarget(const EntityAIType *name, bool fullCreate)
:base(name,fullCreate), _ownerFOV(1.0f)
{
	static PackedColor laserCol(Color(1,0,0,1));
	SetConstantColor(laserCol);
	_lastActivation = Glob.time;
  SetTimeOffset(0);
}

void LaserTarget::CheckDesignatedTarget(Matrix4Par pos)
{
#if _ENABLE_CHEATS
	Object *odt = _designatedTarget;
#endif
	//AUTO_STATIC_ARRAY(OLink(Object),objects,16);
        typedef OLink(Object) helperType;
	AUTO_STATIC_ARRAY(helperType,objects,16);
	_designatedTarget = NULL;
	// check if we are inside or very close of some object
	// if yes, it is designated target
	GLandscape->IsInside(objects,this,pos.Position(),ObjIntersectFire);
	if (objects.Size()>0) 
	{
		// if more targets, we may select any
		for (int i=0; i<objects.Size(); i++)
		{
			if (objects[0]->GetShape())
			{
				_designatedTarget = objects[0];
				Assert( !dynamic_cast<LaserTarget *>(_designatedTarget.GetLink()) );
				break;
			}
		}
	}
	Object *obj = GLandscape->NearestObject
	(
		pos.Position(),50,(ObjectType)(Primary|TypeVehicle),this
	);
	if (obj)
	{
		LODShape *shape = obj->GetShape();
		if (shape)
		{
			Vector3Val geomCenter = obj->FutureVisualState().PositionModelToWorld(shape->GeometryCenter());
			float objDist2 = geomCenter.Distance2(pos.Position());
			if (objDist2>=Square(shape->GeometrySphere()))
			{
				// if no normal object is near, try road objects
				obj = NULL;
			}
			_designatedTarget = obj;
			Assert( !dynamic_cast<LaserTarget *>(_designatedTarget.GetLink()) );
		}
	}
	if (!obj)
	{
		// if no normal object is designated, try to designate road objects
		obj = GLandscape->NearestObject(pos.Position(),50,Network,this);
		if (obj)
		{
			LODShape *shape = obj->GetShape();
			if (shape)
			{
				Vector3Val geomCenter = obj->FutureVisualState().PositionModelToWorld(shape->GeometryCenter());
				float objDist2 = geomCenter.Distance2(pos.Position());
				if (objDist2>=Square(shape->GeometrySphere()))
				{
					obj = NULL;
				}
				_designatedTarget = obj;
				Assert( !dynamic_cast<LaserTarget *>(_designatedTarget.GetLink()) );
			}
		}
	}
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DECombat))
	{
		if (odt!=_designatedTarget)
		{
			LogF
			(
				"Designated target %s",
				_designatedTarget ? (const char *)_designatedTarget->GetDebugName() : "<NULL>"
			);
		}
	}
#endif
}
void LaserTarget::Init(Matrix4Par pos, bool init)
{
	CheckDesignatedTarget(pos);
}

void LaserTarget::Move(Matrix4Par transform)
{
	base::Move(transform);
	_lastActivation = Glob.time;
	CheckDesignatedTarget(transform);
}
void LaserTarget::Move(Vector3Par position)
{
	base::Move(position);
	_lastActivation = Glob.time;
	CheckDesignatedTarget(GetFrameBase());
}

bool LaserTarget::IgnoreObstacle(Object *obstacle, ObjIntersect type) const
{
	if (type==ObjIntersectFire)
	{
		// if designated target blocks line-of-fire, we do not mind and fire
		if (obstacle==_designatedTarget) return true;
		// note: we do mind when target block line-of-sight
	}
	else if (type==ObjIntersectView)
	{
		// if designated target blocks line-of-sight, we pretend we can see the dot anyway
    // this was done because we failed to make LOS working properly
		if (obstacle==_designatedTarget) return true;
	}
	return base::IgnoreObstacle(obstacle,type);

}

void LaserTarget::Simulate( float deltaT, SimulationImportance prec )
{
	if (IsLocal())
	{
		if (_lastActivation<Glob.time-10)
		{
			// if target was not activated for a long time,
			// delete it - it cannot be considered the same target any more
			SetDelete();
		}
	}
	base::Simulate(deltaT,prec);
  ApplyRemoteState(deltaT);
  SimulatePost(deltaT,prec);
}

void LaserTarget::DrawDiags()
{
#if _ENABLE_CHEATS
	if (CHECK_DIAG(DECombat))
	{
		GScene->DrawCollisionStar(RenderVisualState().Position(),0.1,PackedWhite);
		GScene->DrawCollisionStar(AimingPosition(RenderVisualState()),0.075,PackedBlack);
	}
#endif
}

void LaserTarget::Draw(
  int cb, int level, const SectionMaterialLODs &matLOD, ClipFlags clipFlags, const DrawParameters &dp,
  const InstanceParameters &ip, float dist2, const PositionRender &pos, SortObject *oi
)
{
	//if (!Glob.config.IsEnabled(DTTracers))
	//{
	//	return;
	//}
	// check if target was recently activated
	// it may be "ghost" that was left
	if (IsLocal())
	{
		if (_lastActivation<Glob.time-0.3f) return;
	}
	else
	{
		if (_lastActivation<Glob.time-2.0f) return;
	}

  static float intensityCoef = 300.0f;
  static Color color(1,0,0);

  float fov = 1.0f / _ownerFOV;
  saturate(fov, 1.0f, 100.0f);

  if (!GEngine->GetThermalVision())
    fov *= GScene->MainLight()->NightEffect();

  saturateMax(fov, 1.0f);

  float exactDist2 = (GScene->GetCamera()->Position() - RenderVisualState().Position()).SquareSize();
  float intensity = intensityCoef / exactDist2 / fov; 
  
  GLandscape->DrawPoint(cb,pos.PositionWorld(*GScene->GetCamera()), intensity, color);
}

bool LaserTarget::CanObjDrawAsTask(int level) const {return false;}
bool LaserTarget::CanObjDrawShadowAsTask(int level) const {return false;}

float LaserTarget::EstimateArea() const {return 1000.0f;} // Consider the area to be big enough for the point to be drawn
float LaserTarget::GetFeatureSize() const {return 25.0f;} // pretend we are a feature, so that object size limits are not applied to us

float LaserTarget::VisibleSize(ObjectVisualState const& vs) const
{
	// make sure we can detect it on very long distance
	return 40;
}

Vector3 LaserTarget::AimingPosition(ObjectVisualState const& vs, const AmmoType *ammo) const
{
	// it is often directly on surface - make sure we can see it
	/*
	if (_designatedTarget)
	{
		Vector3 base = _designatedTarget->COMPosition();
		Vector3 offset = Position()-base;
		Vector3 offsetNorm = offset.Normalized();
		float size = offsetNorm*offset;
		// make point "further out"
		return base + offsetNorm*(size+1.0f);
	}
	*/
	// drag aiming point nearer to the source
	return vs.Position()-vs.Direction()*0.5;
}


float LaserTarget::VisibleSizeRequired(ObjectVisualState const& vs) const
{
	return 0.01;
}


LSError LaserTarget::Serialize(ParamArchive &ar)
{
	return base::Serialize(ar);
}

#define LASER_TARGET_MSG_LIST(XX)

DEFINE_NETWORK_OBJECT(LaserTarget, base, LASER_TARGET_MSG_LIST)

NetworkMessageFormat &LaserTarget::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	base::CreateFormat(cls,format);
	return format;
}
TMError LaserTarget::TransferMsg(NetworkMessageContext &ctx)
{
	TMCHECK( base::TransferMsg(ctx) );
	return TMOK;
}
float LaserTarget::CalculateError(NetworkMessageContextWithError &ctx)
{
	float error = base::CalculateError(ctx);
	return error;
}

/*------------------------------------------------------*/
NVMarkerTargetType::NVMarkerTargetType(ParamEntryPar param)
:base(param)
{
  ParamEntryVal entry = param >> "NVGMarker";

  GetValue(_diffuse, entry >> "diffuse");
  GetValue(_ambient, entry >> "ambient");
  _brightness = entry >> "brightness";
  _flashing = entry >> "blinking";
  _nvVisible = entry >> "onlyInNvg";
}

void NVMarkerTargetType::Load(ParamEntryPar par, const char *shape)
{
  ParamEntryVal entry = par >> "NVGMarker";

  GetValue(_diffuse, entry >> "diffuse");
  GetValue(_ambient, entry >> "ambient");
  _brightness = entry >> "brightness";
  _flashing = entry >> "blinking";
  _nvVisible = entry >> "onlyInNvg";

  base::Load(par,shape);
  Assert(_destrType != DestructNo);
}

void NVMarkerTargetType::InitShape()
{
  base::InitShape();
  _shape->OrSpecial(IsColored|ZBiasMask|IsAlpha|IsAlphaFog);
  if (_shape->NLevels()>=2)
  {
    ShapeUsed lock = _shape->Level(1);
    Shape *points = lock.InitShape();
    points->ExpandClip();
    for (int i=0; i<points->NVertex(); i++)
    {
      ClipFlags clip = points->Clip(i);
      clip &= ~ClipLightMask;
      clip |= ClipLightLine;
      points->SetClip(i,clip);
    }
    points->CalculateHints();
    _shape->CalculateHints();
  }

  Assert(_destrType != DestructNo);
}


Object* NVMarkerTargetType::CreateObject(bool unused) const
{ 
  return new NVMarkerTarget(this, unused);
}


DEFINE_CASTING(NVMarkerTarget)

NVMarkerTarget::NVMarkerTarget(const EntityAIType *name, bool fullCreate)
:base(name,fullCreate)
{
  _lastActivation = Glob.time;
  _on = false;
  SetTimeOffset(0);
  SetDestructType(GetType()->_destrType);
}

void NVMarkerTarget::Move(Matrix4Par transform)
{
  base::Move(transform);
  _lastActivation = Glob.time;
}

void NVMarkerTarget::Move(Vector3Par position)
{
  base::Move(position);
  _lastActivation = Glob.time;
}

void NVMarkerTarget::Simulate( float deltaT, SimulationImportance prec )
{
  const NVMarkerTargetType *type = static_cast<const NVMarkerTargetType*>(Type());

  if (_light.IsNull())
  {
    _light = new LightPointOnVehicle(NULL, type->_diffuse, type->_ambient, this, VZero);
    _light->SetBrightness(type->_brightness);
    _light->Switch(_on);
    GLOB_SCENE->AddLight(_light);
    _lightOn = Glob.time;
  }
  else
  {
    const Person *person = GWorld->FocusOn() ? GWorld->FocusOn()->GetPerson() : NULL;
    bool nightVision = person ? person->IsNVWanted() : false;
    _on = type->_flashing ? ((toInt(2.0 * (Glob.time - _lightOn)) % 2) != 0) : true;
    _on = type->_nvVisible ? (_on && nightVision) : _on;
    _light->Switch(_on);
  }

  const float MaxActiveTimeInSec = 30*60;
  if (GetRawTotalDamage() > 0.1f || _lastActivation<(Glob.time-MaxActiveTimeInSec))
  {
    if (IsLocal())
      SetDelete();
    else
      GetNetworkManager().AskForDeleteVehicle(this);

    _light.Free();
    _on = false;
  }
  base::Simulate(deltaT,prec);
}

void NVMarkerTarget::DrawDiags()
{
#if _ENABLE_CHEATS
  if (CHECK_DIAG(DECombat))
  {
    GScene->DrawCollisionStar(RenderVisualState().Position(),0.1,PackedWhite);
    GScene->DrawCollisionStar(AimingPosition(RenderVisualState()),0.075,PackedBlack);
  }
#endif
}

void NVMarkerTarget::Draw(int cb, int level, const SectionMaterialLODs &matLOD, ClipFlags clipFlags,
 const DrawParameters &dp, const InstanceParameters &ip, float dist2, const PositionRender &pos, SortObject *oi)
{
  if (_on && !GEngine->GetThermalVisionWanted())
  {
    static float intensityCoef = 150.0f;
    const Color clr(Color(1.0f, 0.0f, 0.0f, 1.0f));

    float exactDist2 = (GScene->GetCamera()->Position() - RenderVisualState().Position()).SquareSize();
    float intensity = intensityCoef / exactDist2; 

    if (!GEngine->GetThermalVision())
      intensity *= GScene->MainLight()->NightEffect();

    GLandscape->DrawPoint(cb,pos.PositionWorld(*GScene->GetCamera()), intensity, clr);
  }
}

bool NVMarkerTarget::CanObjDrawAsTask(int level) const {return false;}
bool NVMarkerTarget::CanObjDrawShadowAsTask(int level) const {return false;}
// Consider the area to be big enough for the point to be drawn
float NVMarkerTarget::EstimateArea() const { return 1000.0f; }
// pretend we are a feature, so that object size limits are not applied to us
float NVMarkerTarget::GetFeatureSize() const { return 25.0f; }

// make sure we can detect it on very long distance
float NVMarkerTarget::VisibleSize(ObjectVisualState const& vs) const { return 40.0f; }
float NVMarkerTarget::VisibleSizeRequired(ObjectVisualState const& vs) const { return 1.0f; }

Vector3 NVMarkerTarget::AimingPosition(ObjectVisualState const& vs, const AmmoType *ammo) const
{
  // it is often directly on surface - make sure we can see it
  // drag aiming point nearer to the source
  return vs.Position()-vs.Direction()*0.5;
}


LSError NVMarkerTarget::Serialize(ParamArchive &ar)
{
  CHECK(::Serialize(ar, "lastActivation", _lastActivation, 1, TIME_MIN))
  return base::Serialize(ar);
}

#define NV_TARGET_MSG_LIST(XX) \
  XX(UpdateDamage, None)

DEFINE_NETWORK_OBJECT(NVMarkerTarget, base, NV_TARGET_MSG_LIST)

NetworkMessageFormat &NVMarkerTarget::CreateFormat
(
 NetworkMessageClass cls,
 NetworkMessageFormat &format
 )
{
  base::CreateFormat(cls,format);
  return format;
}

TMError NVMarkerTarget::TransferMsg(NetworkMessageContext &ctx)
{
  TMCHECK( base::TransferMsg(ctx) );
  return TMOK;
}

float NVMarkerTarget::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = base::CalculateError(ctx);
  return error;
}


/*------------------------------------------------------*/
ArtilleryMarkerTargetType::ArtilleryMarkerTargetType( ParamEntryPar param )
:base(param)
{

}

void ArtilleryMarkerTargetType::Load(ParamEntryPar par, const char *shape)
{
  base::Load(par,shape);
  Assert(_destrType != DestructNo);
}

void ArtilleryMarkerTargetType::InitShape()
{
  base::InitShape();
}


Object* ArtilleryMarkerTargetType::CreateObject(bool unused) const
{ 
  return new ArtilleryMarkerTarget(this, unused);
}




DEFINE_CASTING(ArtilleryMarkerTarget)

ArtilleryMarkerTarget::ArtilleryMarkerTarget(const EntityAIType *name, bool fullCreate)
:base(name,fullCreate)
{
  _createTime = Glob.time;
}

void ArtilleryMarkerTarget::Init(Matrix4Par pos, bool init)
{
}

void ArtilleryMarkerTarget::Move(Matrix4Par transform)
{
}

void ArtilleryMarkerTarget::Move(Vector3Par position)
{
}

void ArtilleryMarkerTarget::Simulate( float deltaT, SimulationImportance prec )
{
  //ArtilleryMarkerTarget is just temporary object for targeting, 3 minutes should be enough for aim and fire
  if(_createTime < Glob.time - 180 && !this->ToDelete())
  {
    this->SetDelete();
  }
}

void ArtilleryMarkerTarget::DrawDiags()
{
#if _ENABLE_CHEATS
  if (CHECK_DIAG(DECombat))
  {
    GScene->DrawCollisionStar(RenderVisualState().Position(),0.1,PackedWhite);
    GScene->DrawCollisionStar(AimingPosition(RenderVisualState()),0.075,PackedBlack);
  }
#endif
}

void ArtilleryMarkerTarget::Draw(
                          int cb, int level, const SectionMaterialLODs &matLOD, ClipFlags clipFlags, const DrawParameters &dp,
                          const InstanceParameters &ip, float dist2, const PositionRender &pos, SortObject *oi
                          )
{
  //base::Draw(cb, level, matLOD, clipFlags, dp, ip, dist2, pos, oi);
}

bool ArtilleryMarkerTarget::CanObjDrawAsTask(int level) const {return false;}
bool ArtilleryMarkerTarget::CanObjDrawShadowAsTask(int level) const {return false;}
// Consider the area to be big enough for the point to be drawn
float ArtilleryMarkerTarget::EstimateArea() const { return 0.0f; }
// pretend we are a feature, so that object size limits are not applied to us
float ArtilleryMarkerTarget::GetFeatureSize() const { return 0.0f; }

// nobody is supposed to detect this object
float ArtilleryMarkerTarget::VisibleSize(ObjectVisualState const& vs) const { return 0.0f; }
float ArtilleryMarkerTarget::VisibleSizeRequired(ObjectVisualState const& vs) const { return 1.0f; }

Vector3 ArtilleryMarkerTarget::AimingPosition(ObjectVisualState const& vs, const AmmoType *ammo, Vector3Par dirToAim) const
{
  // it is often directly on surface - make sure we can see it
  // drag aiming point nearer to the source
  return vs.Position();
}
