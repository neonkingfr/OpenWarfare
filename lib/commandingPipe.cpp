#ifdef _WIN32
#include <Es/Common/use_exceptions.h>
#endif
#include "wpch.hpp"

#include <Es/Strings/rString.hpp>
#include <Es/Strings/bString.hpp>
#include <El/Credits/credits.hpp>

#include "global.hpp"

#include "Network/network.hpp"

#include "jsonBinding.h"

#if defined(_WIN32) && !defined(_XBOX)

struct CommandingPipeContextWin
{
  HANDLE handle;

  ~CommandingPipeContextWin();
  void Close();
  void Open(const char *name);
  RString PeekCommand();
  void Respond(const char *text);
};


void CommandingPipeContextWin::Open(const char *name)
{
  Close();
  BString<1024> pipeName;
  sprintf(pipeName,"\\\\.\\pipe\\%s",name);

  handle = CreateNamedPipe(
    pipeName,PIPE_ACCESS_DUPLEX|FILE_FLAG_FIRST_PIPE_INSTANCE,
    PIPE_TYPE_MESSAGE|PIPE_READMODE_MESSAGE|PIPE_NOWAIT,
    1, 4*1024, 4*1024, 100, NULL
  );
}

void CommandingPipeContextWin::Close()
{
  if (handle!=NULL)
  {
    CloseHandle(handle);
    handle = NULL;
  }
}

CommandingPipeContextWin::~CommandingPipeContextWin()
{
  Close();
}

RString CommandingPipeContextWin::PeekCommand()
{
  RString ret;
  for (;;)
  {
    char buffer[4*1024];
    DWORD read = 0;
  
    BOOL ok = ReadFile(handle,buffer,sizeof(buffer),&read,NULL); 
    if (ok)
    {
      // append data and stop reading
      ret = ret + RString(buffer,read);
      break;
    }
    else
    {
      HRESULT hr = GetLastError();
      if (hr!=ERROR_MORE_DATA) return RString();
      // append data and continue reading
      ret = ret + RString(buffer,read);
    }
  }
  return ret;
}

void CommandingPipeContextWin::Respond(const char *text)
{
  if (handle!=NULL && handle!=INVALID_HANDLE_VALUE)
  {
    DWORD written;
    WriteFile(handle,text,strlen(text),&written,NULL);
  }
}


typedef CommandingPipeContextWin CommandingPipeContext;

#else

struct CommandingPipeContextDummy
{
  void Open(const char *name){}
  RString PeekCommand() {return RString();}
  void Respond(const char *text){}
};

typedef CommandingPipeContextDummy CommandingPipeContext;

#endif

static CommandingPipeContext CommandingPipe;

void NOTHROW CommandingPipeRespond(const char *text)
{
  CommandingPipe.Respond(text);
}

void NOTHROW CommandingPipeOpen(const char *text)
{
  CommandingPipe.Open(text);
}

extern bool CloseRequest;

static const char *CommandArg(const char *beg, const char *opt)
{
  // when this is used, the last character of opt should be '=', like in -x=800
  Assert(opt[0]!=0 && (opt[strlen(opt)-1]=='=' || opt[strlen(opt)-1]==' '));
  if (strnicmp(beg,opt,strlen(opt))) return NULL;
  return beg+strlen(opt);
}

void NOTHROW ProcessPipeCommands()
{
  RString pipeCommand = CommandingPipe.PeekCommand();
  if (pipeCommand.IsEmpty()) return; // most common situation - no command at all
  if (pipeCommand=="shutdown")
  {
    CloseRequest = true;
  }
  else if (const char *chatText=CommandArg(pipeCommand,"message "))
  {
    void SystemChatMessage(const char *text);
    SystemChatMessage(chatText);
  }
  else if (const char *replyText=CommandArg(pipeCommand,"reply "))
  {
    // send the response back
    CommandingPipe.Respond(pipeCommand);
  }
  else if (pipeCommand=="session")
  {
    // check session attributes and respond
    /*
    What server is the user connected to (ip/port, maybe password?)
    How long is the user connected to the server
    Player UID
    Is the user in Single Player or MultiPlayer mission, main menu, or editor
    (starred items can actually be achieved by callExt)
    What mission/island is the user playing (*)
    Player coordinates, and in what group and on what side is the player (e.g for communication channel) (*)
    How long is the mission and game running? (*)
    */
    // format a JSON response
    rapidjson_binding::DocumentValue response;
    // construct a JSON object containing the error message
    response.AddMemberString("playerId", GetPublicKey());
    if (GetNetworkManager().IsClient() || GetNetworkManager().IsServer())
    {
      response.AddMember("hosting",GetNetworkManager().IsServer());

      response.AddMemberString("island",GetNetworkManager().GetMissionHeader()->_island);
      response.AddMemberString("mission",GetNetworkManager().GetMissionHeader()->GetLocalizedMissionName());
      if (GetNetworkManager().IsClient())
      {
        response.AddMemberString("host",GetNetworkManager().GetHostName());

        RString addr = GetNetworkManager().GetServerGUID();
        response.AddMemberString("hostIP",addr);
      }
    }

    {
      rapidjson::StringBuffer dataBuffer;
      rapidjson::Writer<rapidjson::StringBuffer> writer(dataBuffer);
      response->Accept(writer);
      const char *dataString = dataBuffer.GetString();
      CommandingPipe.Respond(RString("session ") + dataString);
    }


  }
  else if (const char *connectText=CommandArg(pipeCommand,"connect "))
  {
    void RequestConnect(RString ip, int port, RString password);
    int GetNetworkPort();

    rapidjson::Document d;
    d.Parse<0>(connectText);
    RString ip;
    if (d["ip"].IsString())
    {
      ip = d["ip"].GetString();
    }
    int port = GetNetworkPort();
    if (d["port"].IsNumber())
    {
      port = d["port"].GetInt();
    }
    RString password;
    if (d["password"].IsString())
    {
      password = d["password"].GetString();
    }
    RequestConnect(ip,port,password);

  }
}
