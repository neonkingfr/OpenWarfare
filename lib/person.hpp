#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PERSON_HPP
#define _PERSON_HPP

/*!
\file
Interface for Person class
*/

#include "vehicleAI.hpp"
#include "vehSupply.hpp"
#include "identity.hpp"

#include "turret.hpp"

#if _VBS2 // suppression
#define SUPPRESSION_RADIUS_INTEREST      3.0    // rounds that might go within this radius need watching
#define SUPPRESSION_VERTICAL_TOLERANCE   2.0    // scale factor for vertical dimension - be more tolerant
#define ADDITIONAL_SUPPRESS_MILLISECONDS 2      // time (milliseconds) to add to expected suppression time - better to let round
                                                // fly a little bit longer before doing the check rather than too short a time
                                                // as then won't be detected as suppressing.
#define ESTIMATED_FRAMES_PER_SEC         25.0   // an estimate of the number of frames (simulations) per second. This is used
                                                // for the exact flight time calculations of suppression because friction is
                                                // applied every time a round gets to move/simulate. Hence there can be non-trivial
                                                // differences in position.
#define ESTIMATED_FRICTION               0.0005 // the air friction constant being used for ShotShells and bullets.
#define SUPPRESSION_TOLERANCE_CHECK1     1.2    // a scaling factor to apply to the 1st round of suppression checking (where
                                                // we consider if a round could potentially [in the future] suppress so we
                                                // don't over-hastily eliminate rounds that fly close.
#define FRIENDLY_NO_SUPPRESSION_RADIUS2  100.0  // friendlies don't get suppressed if within 10 metres of firer.
#define SUPPRESSION_Y_TWITCH             1.25    // maximum twitch in the horizontal plane if round passes the player
#define SUPPRESSION_X_TWITCH             0.25    // maximum twitch in the vertical plane if round passes the player

#define MAX_AI_SUPPRESSION_TIME            3.0  // maximum amount of time that an AI unit with no skill will be suppressed
#define MAX_AI_SUPPRESSION_TIME_CUMULATIVE 8.0  // maximum cumulative amount of time that an AI unit will be suppressed
#endif

class Building;
class AIBrain;

struct CheckAmmoInfo
{
  const MuzzleType *muzzle1;
  const MuzzleType *muzzle2;
  const MuzzleType *muzzleHG;
  int slots1;
  int slots2;
  int slots3;
  int slotsHG;
  int hasSlots1;
  int hasSlots2;
  int hasSlots3;
  int hasSlotsHG;
  float hasAmmo1;
  float hasAmmo2;
  float hasAmmo3;
  float hasAmmoHG;

  CheckAmmoInfo();
};

#define UPDATE_VEHICLE_BRAIN_MSG(MessageName, XX) \
  XX(MessageName, int, remotePlayer, NDTInteger, int, NCTNone, DEFVALUE(int, 1), DOC_MSG("Person is controled by player on some client"), TRANSF, ET_NOT_EQUAL, ERR_COEF_IMMEDIATE)

DECLARE_NET_INDICES_EX_ERR(UpdateVehicleBrain, UpdateVehicleAIFull, UPDATE_VEHICLE_BRAIN_MSG)

//! Abstract class Person - interface for any person related activites
/*!
	This class defines interface used to control any person activity.
	It also provides some minimal default implementation for some functions.
*/
class Person: public EntityAIFull, public CountInstances<Person>
{
	typedef EntityAIFull base;

protected:
	Ref<AIBrain> _brain;

  AIUnitInfo _info; // TODO: move content to _identity
#if _ENABLE_IDENTITIES
  Identity _identity;
#endif

	SensorRowID _sensorRowID;
	/*!
	_remotePlayer contain multiplayer ID of player
	_remotePlayer == 1 - AI Player
	*/
	int _remotePlayer;
  Time _disableDamageFromObjUntil;

  #if _VBS2 // fatigue model
protected:
  bool _isSuppressed;
  bool _suppressedWince;
  Time _suppressedUntil;
#endif

#if _VBS3 // convoy trainer
public:
  bool IsPersonalItemsEnabled() const {return _enablePersonalItems;}
  virtual void SetEnablePersonalItems(bool value, RString animAction = "", Vector3 personalItemsOffset=VZero);
  bool GetPersonalItemsParams(RString& animAction, Vector3& personalItemsOffset);
  Vector3 _personalItemsOffset;
  RString _personalItemsAnim;
protected:
  bool _enablePersonalItems;
#endif

public:
	Person(const EntityAIType *name, bool fullCreate=true);
  ~Person() {}

  AIBrain *Brain() const {return _brain;}
  void SetBrain( AIBrain *brain );
  /// Create the correct type of brain, based on EntityAIType::_agentTasks
  AIBrain *CreateBrain(bool forceAgent=false);
  void ApplyIdentity();

  /// select the first speaker matching to given speaker type and identity type of given person
  RString SelectSpeaker(RString speakerType);

  virtual AIBrain *PilotUnit() const {return _brain;}
  virtual AIBrain *GunnerUnit() const {return _brain;}
  virtual AIBrain *ObserverUnit() const {return _brain;}
  virtual AIBrain *CommanderUnit() const {return _brain;}

#if _ENABLE_IDENTITIES
  Identity &GetIdentity() {return _identity;}
  const Identity &GetIdentity() const {return _identity;}

  void CopyIdentity(const Person *source);
#endif

  /// Check whether given identity type is in the list of enabled types
  virtual bool IsIdentityEnabled(RString identityType) const {return true;}

	AIUnitInfo& GetInfo() {return _info;}
	void SetInfo(AIUnitInfo& info) {_info = info;}
  /// Get name from PlayerIdentity for player or _info._name otherwise
  RString GetPersonName() const;

	float GetExperience() const {return _info._experience;}
  void  SetExperience(float exp) { _info._experience = exp; }
	Rank GetRank() const {return _info._rank;}
	void SetRank(Rank rank) {_info._rank = rank;}

  bool IsRenegade() const;

	bool QIsManual() const;
  /// SetRemotePlayer also applies the identity (Face, Glasses, ...)
  void SetRemotePlayer(int player);
	int GetRemotePlayer() const {return _remotePlayer;}
	bool IsRemotePlayer() const {return !IsLocal() && _remotePlayer != 1;}
  bool IsNetworkPlayer() const;

#if _ENABLE_CHEATS
  int  _vonDpnid;   //fake one. It servers only to VoNSay script purposes
  int  GetVoNSayDpnid() {return _vonDpnid;}
  void SetVoNSayDpnid(int dpnid) {_vonDpnid = dpnid;}
#endif

  void SetSensorRowID(SensorRowID sensorRowID) {_sensorRowID = sensorRowID;}
	SensorRowID GetSensorRowID() const {return _sensorRowID;}

	virtual LODShapeWithShadow *GetOpticsModel(const Person *person) const;
	virtual bool GetEnableOptics(ObjectVisualState const& vs, const Person *person) const; 
  virtual PackedColor GetOpticsColor(const Person *person) {return PackedWhite;} // TODO: get color from muzzle
	virtual bool GetForceOptics(const Person *person, CameraType camType) const; 

  virtual void ToggleZoomIn() {}; //UAZoomInToggle helper method
  virtual bool IsZoomInToggled() {return false;} //UAZoomInToggle status
  virtual void ResetZoomInToggle() {}

  virtual void ToggleZoomOut() {} //UAZoomOutToggle helper method
  virtual bool IsZoomOutToggled() {return false;} //UAZoomOutToggle status
  virtual void ResetZoomOutToggle() {};

  /// search for special item of given type in the inventory
  virtual const WeaponType *GetSpecialItem(WeaponSimulation simulation) const {return NULL;}

  // check if person has active gun light
  virtual bool IsGunLightEnabled() const {return false;}
  /// check if person has some night vision equipment
  virtual bool IsNVEnabled() const {return false;}
	/// check if person wants to use the night vision equipment
	virtual bool IsNVWanted() const {return false;}
	/// check if night vision equipment is ready for rendering
	virtual bool IsNVReady() const {return true;}
  virtual void SetNVWanted(bool set=true) {}
	/// render night vision camera effects
  virtual void DrawNVOptics() {}
  /// allow/deny using ir laser for AI during CM
  virtual void EnableIRLaser(bool enable) {}
  /// allow/deny using flash light mounted on gun for AI during CM
  virtual void ForceGunLight(bool force) {}
  virtual bool IsFlirWanted() const {return false;}

  //take/drop soldier's backpack
  virtual void TakeBackpack(EntityAI *backpack) {};
  virtual void DropBackpack() {};

#if _VBS3
  //! Return coefficient describing the current tunnel vision (0 - no tunnel vision, 1 - full tunnel vision)
  virtual float GetTunnelVisionCoef() const {return 0.0f;}
  virtual VisionMode GetVisionMode() const {return VMDv;}
  virtual int GetTiModus() const {return 0;}
  virtual const ViewPars* GetViewPars(bool includeTurrets = true) const {return NULL;}
#endif

  virtual void ApplyVisionMode() {};
  virtual void NextVisionMode() {};

  virtual void KilledBy(EntityAI *owner) {}
  
  /// get aiming position - used for proxies
  virtual Vector3 GetLocalAimingPosition() const {return VZero;}

  virtual GameVarSpace *GetVars();

	void CheckAmmo(CheckAmmoInfo &info);

  virtual bool CanFloat() const {return true;}

	void Simulate(float deltaT, SimulationImportance prec);
  void CleanUp();
  void CleanUpMoveOut() {base::CleanUpMoveOut();}
  
#if _VBS2 // suppression
  void Suppress(ShotShell *shot, float closestDistance, Vector3 closestPosition);
  virtual void SuppressSecondaryEffects() {};
#endif

#if _VBS3
  virtual void ShowTrajectory() {};
#endif

  virtual void ResetMovement(float speed, int action=-1) = 0;
	virtual void ResetMovement(float speed, RString action) = 0;

	//! Action based interface to control person - smooth transition
  virtual bool PlayAction(ManAction action, ActionContextBase *context=NULL, bool callFuncNow=true){return false;}
  virtual bool PlayAction(RString action, ActionContextBase *context=NULL, bool callFuncNow=true){return false;}
  virtual void SwitchAction(ManAction action, ActionContextBase *context=NULL){}
	virtual void SwitchAction(RString action, ActionContextBase *context=NULL, bool callFuncNow=true){}

  /// start get in and add it to activity list
  virtual void CreateActivityGetIn(Transport *veh, UIActionType pos, Turret *turret=NULL, int cargoIndex=-1) = 0;

  /// process actions needed when person gets in vehicle
  virtual void OnGetInFinished() {};
  /// process actions needed when person gets out from vehicle
  virtual void OnGetOutFinished() {};

  /// access to direct speaking radio
  virtual RadioChannel *GetRadio() {return NULL;}
  /// access to direct speaking radio
  virtual const RadioChannel *GetRadio() const {return NULL;}
  /// attach 3D sound to person to update position and perform lip sync
  virtual void SetDirectSpeaking(AbstractWave *wave) {}

  /// attach 3D sound to person to update position and perform lip sync
  virtual void SetSpeaking3D(AbstractWave *wave) {}
  
  /// sometimes sound needs to be called even in a vehicle, like when speaking
  virtual bool SoundInTransportNeeded() const {return false;}

  /// perform sounds while in a vehicle
  virtual void SoundInTransport(Transport *parent, Matrix4Par worldPos, float deltaT){}

  /// helper for UI action  
  virtual ActionContextBase *CreateActionContextUIAction(const Action *action) = 0;
  
  //! Shift of the action move. 
  virtual Vector3 GetActionMoveShift(RString action) const {return VZero;};
  
  virtual bool IsActionHighlighted(UIActionType type, EntityAI *target) const {return false;}
  virtual bool CanCancelAction() const {return false;}

	virtual void ResetStatus();

  virtual void PutDownEnd() {}

	virtual UnitPosition GetUnitPosition() const;
  virtual void SetUnitPositionCommanded(UnitPosition status) {}
  virtual void SetUnitPositionScripted(UnitPosition status) {}
  virtual void SetUnitPositionFSM(UnitPosition status) {}

  virtual void SetFace(RString name, RString player="") {}
  virtual void SetGlasses(RString name) {}
  /// Set the Face using _info for AI but PlayerIdentity for Players
  virtual void SetFace();
  /// Set the Glasses using _info for AI but PlayerIdentity for Players
  virtual void SetGlasses();

	virtual void BasicSimulation(float deltaT, SimulationImportance prec, float speedFactor) {} // also inside vehicle
  virtual float GetLegPhase() const {return 0.0f;}

  virtual bool HasBinoculars() const {return false;}
  /// make sure person is watching given direction (in model space)
  virtual void AimHeadCrew(Vector3Par dir) {}
  /// model space - current direction we are looking at
  virtual Vector3 GetCrewHeadDirection() const {return VForward;}
  /// model space - wanted direction to look at
  virtual Vector3 GetCrewHeadDirectionWanted() const {return VForward;}
  /// Looking around
  virtual void ProcessLookAround(float headingWanted, float diveWanted, const ViewPars *viewPars, float deltaT, bool forceLookAround=false, bool forceDiveAround=false) { Assert(false); }
  virtual void UpdateHeadLookAround(float deltaT, bool forceLookAround=false) { Assert(false); }
  /// used for instinctive reactions (e.g. looking into corners)
  virtual void ProcessLookAroundAuto(Vector3Par dir) {}
  virtual Zoom *GetZoom() {return NULL;}
  virtual void UpdateHeadLookWanted(float heading, float dive) {Assert(false);}
  /// model space - current direction we are looking at
  virtual Vector3 GetCrewLookDirection(ObjectVisualState const &vs) const {return VForward;}
  /// reset dive camera view direction
  virtual void ResetXRotWanted() {}
  /// it currently stops swimming animation (used when person enters the vehicle while swimming)
  virtual void OnMovedFromLandscape() {}
  /// return true when soldier is swimming
  virtual bool IsSwimming() const {return false;}
  
	virtual float GetAnimSpeed(RStringB move) {return 1.0f;}
	virtual Vector3 GetPilotPosition(CameraType camType) const {return VZero;}
  virtual void RecalcShakeTransform(Vector3Par v,float deltaT){};
	
  virtual bool UseInternalLODInVehicles()	const {return false;}

	virtual void ShowHead(bool show=true) {}
	virtual void ShowWeapons(bool showPrimary=true, bool showSecondary=true) {}

  virtual EntityAI *GetFlagCarrier() {return NULL;}
  virtual void SetFlagCarrier(EntityAI *veh) {}

	virtual void HideBody() {}

  virtual Vector3 GetCommonDamagePoint() {return VZero;}

	virtual LSError Serialize(ParamArchive &ar);
	virtual LSError SerializeIdentity(ParamArchive &ar);

  virtual void CatchLadder(Building *obj, int ladder, bool up) {}
  virtual void DropLadder(Building *obj, int ladder) {}
  virtual bool IsOnLadder(Building *obj, int ladder) const {return false;}

	virtual bool IsWoman() const {return false;}
  virtual RString GetGenericNames() const {return "Default";} // default set of names for non-Man Persons (required in the core config)
  virtual RString GetFaceType() const {return RString();}

  virtual Vector3 GetHUDOffset() const {return VZero;};
	
	DECLARE_NETWORK_OBJECT
	static NetworkMessageFormat &CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContextWithError &ctx);
  
  void SimulateImpulseDamage(DoDamageResult &result, EntityAI *owner, float impulse, Vector3Par modelPos);

  void DisableDamageFromObj(float seconds);
  virtual void FreeFall(float seconds=1.0f) {}

  //! Find weapon fitting in given slots
  int FindWeaponType(int maskInclude, int maskExclude=0) const;

  virtual bool ForEachTurret(ITurretFunc &func) const;
  virtual bool ForEachTurret(EntityVisualState& vs, ITurretFuncV &func) const;
  virtual bool ForEachTurretEx(EntityVisualState& vs, ITurretFuncEx &func) const;
  /// enumerate all crew
  virtual bool ForEachCrew(ICrewFunc &func) const;

#if _VBS3 //Commander override
  bool FindOpticsTurret(const Person *gunner, TurretContext &context) const { return FindTurret(gunner, context);}
  bool FindControlledTurret(const Person *gunner, TurretContext &context) const { return FindTurret(gunner, context);}
#endif

  virtual bool FindTurret(const Person *gunner, TurretContext &context) const;
  virtual bool FindTurret(EntityVisualState& vs, const Person *gunner, TurretContextV& context) const;
  virtual bool GetPrimaryGunnerTurret(TurretContext& context) const;
  virtual bool GetPrimaryGunnerTurret(EntityVisualState& vs, TurretContextV& context) const;
  virtual bool GetPrimaryGunnerTurret(EntityVisualState& vs, TurretContextEx& context) const;
  virtual bool GetPrimaryObserverTurret(TurretContext& context) const;
  virtual bool GetPrimaryObserverTurret(EntityVisualState& vs, TurretContextV& context) const;
  virtual bool GetPrimaryObserverTurret(EntityVisualState& vs, TurretContextEx& context) const;

  //! number of weapons
  int NWeaponSystems() const {return _weaponsState._weapons.Size();}
  //! return weapon with index i
  const WeaponType *GetWeaponSystem(int i) const {return _weaponsState._weapons[i];}
  //! number of magazines
  int NMagazines() const {return _weaponsState._magazines.Size();}
  //! return magazine with index i
  const Magazine *GetMagazine(int i) const {return _weaponsState._magazines[i];}
  //! return magazine with index i
  Magazine *GetMagazine(int i) {return _weaponsState._magazines[i];}

  //! add new weapon
  int AddWeapon(RStringB name, bool force=false, bool reload=true, bool checkSelected=true); // force == do not check slots
  //! add new weapon
  int AddWeapon(WeaponType *weapon, bool force=false, bool reload=true, bool checkSelected=true); // force == do not check slots
  //! remove weapon
  void RemoveWeapon(RStringB name, bool checkSelected=true);
  //! remove weapon
  void RemoveWeapon(const WeaponType *weapon, bool checkSelected=true);
  //! remove all weapons
  void RemoveAllWeapons(bool removeItems);
  /// remove all special items
  void RemoveAllItems();
  //! adds only minimal equipment for given class
  virtual void MinimalWeapons();

  //! add new magazine
  int AddMagazine(RStringB name, bool force=false, bool autoload = false); // force == do not check slots
  //! add new magazine
  int AddMagazine(Magazine *magazine, bool force=false, bool autoload=false); // force == do not check slots
  //! add new backpack
  int AddBackpack(EntityAI *backpack, bool force=false); // force == do not check slots
  //! remove magazine
  void RemoveMagazine(RStringB name);
  //! remove magazine
  void RemoveMagazine(const Magazine *magazine);
  //! remove all magazines of given type
  void RemoveMagazines(RStringB name);
  //! remove all magazines
  void RemoveAllMagazines();

  //! check if weapon can be added and retrieves weapons must be removed
  bool CheckWeapon(const WeaponType *weapon, AutoArray<Ref<const WeaponType>, MemAllocSA> &conflict,
    const Ref<WeaponType> *weapons, int weaponsCount) const;
  //! check if weapon can be added and retrieves weapons must be removed
  bool CheckWeapon(const WeaponType *weapon, AutoArray<Ref<const WeaponType>, MemAllocSA> &conflict) const;
  //! check if magazine can be added and retrieves magazines must be removed
  bool CheckMagazine(const MagazineType *type, AutoArray<Ref<const Magazine>, MemAllocSA> &conflict,
    const Ref<Magazine> *magazines, int magazinesCount, bool inGame=false) const;
  //! check if magazine can be added and retrieves magazines must be removed
  bool CheckMagazine(const MagazineType *type, AutoArray<Ref<const Magazine>, MemAllocSA> &conflict) const;
  //! check if magazine can be added and retrieves magazines must be removed
  bool CheckMagazine(const Magazine *magazine, AutoArray<Ref<const Magazine>, MemAllocSA> &conflict) const;

  bool CheckBackpackSpace(const MagazineType *type, AutoArray<Ref<const Magazine>, MemAllocSA> &conflictMagazine,
    AutoArray<Ref<WeaponType>, MemAllocSA> &conflictWeapon, EntityAI *bag) const;
  bool CheckBackpackSpace(const WeaponType *weapon, AutoArray<Ref<const Magazine>, MemAllocSA> &conflictMagazine,
    AutoArray<Ref<WeaponType>, MemAllocSA> &conflictWeapon, EntityAI *bag) const;


  //! check if magazine fits in some weapon
  bool IsMagazineUsable(const MagazineType *magazine) const;
  //! check if given magazine is used (loaded) in some weapon
  bool IsMagazineUsed(const Magazine *magazine) const;

  //! event handler: some weapon changed
  virtual void OnWeaponChanged();

  INHERIT_SOFTLINK(Person,base);
	USE_CASTING(base)

private: // no default copy
	Person( const Person &src );
	void operator=(const Person &src);
};

typedef Person VehicleWithBrain;
//template Ref<Person>;

#endif
