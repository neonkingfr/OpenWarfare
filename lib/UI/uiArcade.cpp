// Implementation of mission editor dialogs
#include "../wpch.hpp"
#include "uiMap.hpp"
#include <Es/Common/win.h>
#include "../dikCodes.h"
#include "../Protect/selectProtection.h"

#include "resincl.hpp"
#include "../keyInput.hpp"
#include <El/QStream/qbStream.hpp>
#include <Es/Algorithms/qsort.hpp>
#include <El/ParamFile/classDbParamFile.hpp>
#include <El/ParamArchive/paramArchiveDb.hpp>

#include <El/Common/randomGen.hpp>
#include "../camEffects.hpp"
#include "../titEffects.hpp"
#include "../landscape.hpp"

#include <El/Evaluator/expressImpl.hpp>
#include "../house.hpp"

#include "../stringtableExt.hpp"
#include "../fileLocator.hpp"

#include "../saveVersion.hpp"

#include "missionDirs.hpp"
#include "../gameDirs.hpp"
#include "../saveGame.hpp"

RString GetVehicleIcon(RString name);

RStringB GetExtensionWizardMission();

bool HasPrimaryGunnerTurret(ParamEntryPar cls);
bool HasPrimaryObserverTurret(ParamEntryPar cls);

/*!
\file
Implementation file for particular displays (mission editor).
*/

///////////////////////////////////////////////////////////////////////////////
// Arcade Map subdisplays

#if _ENABLE_EDITOR

void CStaticAzimut::OnDraw(UIViewport *vp, float alpha)
{
  CStatic::OnDraw(vp, alpha);

  CEdit *edit = dynamic_cast<CEdit *>(_parent->GetCtrl(_idcEdit));
  if (!edit) return;
  float azimut = edit->GetText() ? atof(edit->GetText()) : 0;
  azimut = HDegree(azimut);
  
  PackedColor color(Color(0.08, 0.08, 0.12, alpha));
  float xc = _x + 0.5 * _w;
  float zc = _y + 0.5 * _h;
  const float r1 = 0.32;
  const float r2 = 0.26;
  const float diff = 0.08;
  float x1 = xc + sin(azimut) * r1 * _w;
  float z1 = zc - cos(azimut) * r1 * _h;
  float x2 = xc + sin(azimut - diff) * r2 * _w;
  float z2 = zc - cos(azimut - diff) * r2 * _h;
  float x3 = xc + sin(azimut + diff) * r2 * _w;
  float z3 = zc - cos(azimut + diff) * r2 * _h;

  float w = GLOB_ENGINE->Width2D();
  float h = GLOB_ENGINE->Height2D();

  int xx1 = toIntFloor(x1 * w + 0.5);
  int zz1 = toIntFloor(z1 * h + 0.5);
  int xx2 = toIntFloor(x2 * w + 0.5);
  int zz2 = toIntFloor(z2 * h + 0.5);
  int xx3 = toIntFloor(x3 * w + 0.5);
  int zz3 = toIntFloor(z3 * h + 0.5);

  GLOB_ENGINE->DrawLine(Line2DPixel(xx1, zz1, xx2, zz2), color, color);
  GLOB_ENGINE->DrawLine(Line2DPixel(xx2, zz2, xx3, zz3), color, color);
  GLOB_ENGINE->DrawLine(Line2DPixel(xx3, zz3, xx1, zz1), color, color);
}

void CStaticAzimut::OnLButtonClick(float x, float y)
{
  CStatic::OnLButtonClick(x, y);

  CEdit *edit = dynamic_cast<CEdit *>(_parent->GetCtrl(_idcEdit));
  if (!edit) return;

  float xc = _x + 0.5 * _w;
  float yc = _y + 0.5 * _h;
  float azimut = atan2(x - xc, -y + yc) * 180.0 / H_PI;
  int roundAzimut = 5 * toInt(0.2 * azimut);
  if (roundAzimut < 0) roundAzimut += 360;

  char buffer[16];
  sprintf(buffer, "%d", roundAzimut);
  edit->SetText(buffer);
}

void DisplayArcadeUnit::OnLBSelChanged(IControl *ctrl, int curSel)
{
  switch (ctrl->IDC())
  {
    case IDC_ARCUNIT_SIDE:
      {
        CCombo *combo = dynamic_cast<CCombo *>(ctrl);
        int side = combo ? combo->GetValue(curSel) : TWest;
        bool enable = side == TWest || side == TEast || side == TGuerrila || side == TCivilian || side == TAmbientLife;
        IControl *ctrl2;
        ctrl2 = GetCtrl(IDC_ARCUNIT_RANK);
        if (ctrl2) ctrl2->ShowCtrl(enable);
        ctrl2 = GetCtrl(IDC_ARCUNIT_CTRL);
        if (ctrl2) ctrl2->ShowCtrl(enable);
        //show/hide faction comboBox
        enable = side == TWest || side == TEast || side == TGuerrila || side == TCivilian;
        ctrl2 = GetCtrl(IDC_ARCUNIT_FACTION);
        if (ctrl2) ctrl2->ShowCtrl(enable);

        RString vehicle;
        combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
        if (combo)
        {
          int i = combo->GetCurSel();
          if (i < 0)
            vehicle = "";
          else
            vehicle = combo->GetData(i);
        }
        combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_FACTION));
        if (combo) UpdateFactions(combo, vehicle);
      }
      // continue
    case IDC_ARCUNIT_FACTION:
      {//faction has been changed
        RString vehicle;
        CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
        if (combo)
        {//get selected vehicle
          int i = combo->GetCurSel();
          if (i < 0)
            vehicle = "";
          else
            vehicle = combo->GetData(i);
        }
        //update faction according to selected vehicle
        combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CLASS));
        if (combo) UpdateClasses(combo, vehicle);
      }
      // continue (update combo with faction is called with false)
    case IDC_ARCUNIT_CLASS:
      {
        CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
        if (combo)
        {
          int i = combo->GetCurSel();
          RString vehicle;
          if (i < 0)
            vehicle = "";
          else
            vehicle = combo->GetData(i);
          UpdateVehicles(combo, vehicle);
        }
      }
      break;
    case IDC_ARCUNIT_VEHICLE:
      {
        CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CTRL));
        if (combo)
        {
          UpdatePlayer(combo, (ArcadeUnitPlayer)combo->GetValue(combo->GetCurSel()));
        }
      }
      break;
    case IDC_ARCUNIT_CTRL:
      {
        CListBoxContainer *list = GetListBoxContainer(ctrl);
        if (!list) break;

        // update the content of Side combo
        CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
        if (combo && curSel >= 0)
        {
          int value = list->GetValue(curSel);
          if (value != APNonplayable && combo->GetSize() == 5)
          {
            // Playable
            // combo content WEST, EAST, GUERRILA, CIVILIAN, AMBIENT_LIFE - OK
            break;
          }
          if (value == APNonplayable && combo->GetSize() == 7)
          {
            // non Playable
            // combo content WEST, EAST, GUERRILA, CIVILIAN, AMBIENT_LIFE, LOGIC, EMPTY - OK
            break;
          }
          int sel = combo->GetCurSel();
          combo->ClearStrings();
          int index;
          index = combo->AddString(LocalizeString(IDS_WEST));
          combo->SetValue(index, TWest);
          index = combo->AddString(LocalizeString(IDS_EAST));
          combo->SetValue(index, TEast);
          index = combo->AddString(LocalizeString(IDS_GUERRILA));
          combo->SetValue(index, TGuerrila);
          index = combo->AddString(LocalizeString(IDS_CIVILIAN));
          combo->SetValue(index, TCivilian);
          index = combo->AddString(LocalizeString(IDS_AMBIENT_LIFE));
          combo->SetValue(index, TAmbientLife);
          if (curSel == 0)
          {
            index = combo->AddString(LocalizeString(IDS_LOGIC));
            combo->SetValue(index, TLogic);
            index = combo->AddString(LocalizeString(IDS_EMPTY));
            combo->SetValue(index, TEmpty);
          }
          if (sel >= combo->GetSize()) sel = 0;
          combo->SetCurSel(sel, false);
        }
      }
      break;
  }
  Display::OnLBSelChanged(ctrl, curSel);
}

// note: faction has given order (if BLUEFOR, USMC will be first)
// otherwise if same faction are added, use alphabetical sorting
// other is always last
static int CmpFaction(const RStringB *a, const RStringB *b)
{
  int aPrio =  Pars >> "CfgFactionClasses" >> *a >> "priority";
  int bPrio =  Pars >> "CfgFactionClasses" >> *b >> "priority";
  return bPrio<aPrio;
}

// note: if "men" class is present, we want it to be first
// other classes should be sorted alphabetically
static int CmpClass(const RStringB *a, const RStringB *b)
{
  if (!strcmpi(*a,*b)) return 0;
#if _VBS2 // categorize game vehicle classes seperately from VBS2 classes
  bool hasGame1 = strstr(*a,LocalizeString("STR_EDITOR_GAME_CLASS")) != NULL;
  bool hasGame2 = strstr(*b,LocalizeString("STR_EDITOR_GAME_CLASS")) != NULL;
  if (hasGame1 && hasGame2)
    return stricmp(*a, *b);
  if (!hasGame1 && hasGame2)
    return -1;
  if (hasGame1 && !hasGame2)
    return +1;
#else
  if (!strcmpi(*a,"men")) return -1;
  if (!strcmpi(*b,"men")) return +1;
#endif
  return strcmpi(*a,*b);
}

//15.7. 2008 - new filter for 2D editor allowing to faction in sides
void DisplayArcadeUnit::UpdateFactions(CCombo *combo, const char *vehicle)
{ //get current side
  CCombo *comboSides = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (!comboSides) return;
  int side = comboSides->GetValue(comboSides->GetCurSel());
  //only following sides uses factions
  bool enableFaction = side == TWest || side == TEast || side == TGuerrila || side == TCivilian;

  ParamEntryVal clsVeh = Pars >> "CfgVehicles";
  int n = clsVeh.GetEntryCount();
  FindArrayRStringBCI vehClasses;
  ConstParamEntryPtr manCls = clsVeh.FindEntry("Man");

  // scan all CfgVehicles for vehicleClass list
  for (int i=0; i<n; i++)
  {
    ParamEntryVal vehEntry = clsVeh.GetEntry(i);
    if (!vehEntry.IsClass()) continue;
    int scope = vehEntry >> "scope";
    if (scope != 2) continue; //2 is unit placeable in editor
    int vehSide = vehEntry >> "side";

    if (side == TEmpty)
    {
      if (vehSide == TLogic) continue;
      const ParamClass *vehCls = vehEntry.GetClassInterface();
      if (manCls && vehCls && vehCls->IsDerivedFrom(*manCls)) continue;
    }
    else if (side == TAmbientLife)
    {
      // list only agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (!entry || entry->GetSize() == 0) continue;
    }
    else
    {
      if (side != vehSide) continue;

      // do not list agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (entry && entry->GetSize() > 0) continue;

      // note: some objects may be inserted only as empty
      bool hasDriver = vehEntry>>"hasDriver";
      bool hasGunner = HasPrimaryGunnerTurret(vehEntry);
      bool hasCommander = HasPrimaryObserverTurret(vehEntry);
      if (!hasDriver && !hasGunner && !hasCommander)
      {
        // simulation should be static or thing
        RStringB sim = vehEntry>>"simulation";
#if _ENABLE_REPORT
        if
          (
          strcmpi(sim,"house") && strcmpi(sim,"thing") &&
          strcmpi(sim,"fire") && strcmpi(sim,"fountain") &&
          strcmpi(sim,"flagcarrier") && strcmpi(sim,"church")
          )
        {
          RptF
            (
            "Empty vehicle %s - simulation %s (this simulation is considered non-empty only)",
            (const char *)vehEntry.GetName(),(const char *)sim
            );
        }
#endif
        continue;
      }
    }

    if(enableFaction)
    {
      RStringB name = vehEntry >> "faction";
      if (name.GetLength() == 0) continue;

      int index = vehClasses.FindKey(name);
      if (index<0) index = vehClasses.AddUnique(name);
    }
  }

  QSort(vehClasses.Data(),vehClasses.Size(),CmpFaction);
  //get display name of currently selected faction / faction of edited vehicle
  RStringB vehicleFaction;
  if (vehicle && *vehicle && enableFaction)
  {
    ParamClassEntry paramVeh = Pars >> "CfgVehicles" >> vehicle;
    SRef<ClassEntry>  entry = paramVeh.FindEntry("faction");   
    if(entry)
    {
      vehicleFaction = Pars >> "CfgVehicles" >> vehicle >> "faction";
    }
  }

  combo->ClearStrings();
  int sel = 0; 
  n = vehClasses.Size();
  for (int i=0; i<n; i++)
  {
    RStringB name = vehClasses[i];
    int index = combo->AddString(Pars >> "CfgFactionClasses" >> name >> "displayName");
    combo->SetData(index, name);
    //if unit is only edited, find correct faction 
    if (vehicle && *vehicle) 
      if (stricmp(vehicleFaction, name) == 0) sel = index;
  }
  combo->SetCurSel(sel,false);
}

/*!
\patch 1.82 Date 8/22/2002 by Ondra
- Improved: Editor: Vehicle classes (Armored, Car, ...) are now fully dynamic.
Any addon maker can now introduce his own classes.
\patch 1.82 Date 8/22/2002 by Ondra
- Fixed: Some empty objects could also be inserted as Civilian, using group slots
and causing confusion inside of the editor.
*/


void DisplayArcadeUnit::UpdateClasses(CCombo *combo, const char *vehicle)
{
  CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (!combo2) return;
  int side = combo2->GetValue(combo2->GetCurSel());
  //only following sides uses factions
  bool enableFaction = side == TWest || side == TEast || side == TGuerrila || side == TCivilian;
  RString selectedFaction;
  if(enableFaction)
  {//get selected faction, empty has no faction
    combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_FACTION));
    if (!combo2) return;
    int seldFactionIndex = combo2->GetCurSel();
    if (seldFactionIndex < 0) return; //units have default faction, so something should be selected
    selectedFaction = combo2->GetText(seldFactionIndex);
  }
  
  ParamEntryVal clsVeh = Pars >> "CfgVehicles";
  int n = clsVeh.GetEntryCount();
  FindArrayRStringBCI vehClasses;
  ConstParamEntryPtr manCls = clsVeh.FindEntry("Man");
  // scan all CfgVehicles for vehicleClass list
  for (int i=0; i<n; i++)
  {
    ParamEntryVal vehEntry = clsVeh.GetEntry(i);
    if (!vehEntry.IsClass()) continue;
    int scope = vehEntry >> "scope";
    if (scope != 2) continue;
    int vehSide = vehEntry >> "side";

    if (side == TEmpty)
    {
      if (vehSide == TLogic) continue;
      const ParamClass *vehCls = vehEntry.GetClassInterface();
      if (manCls && vehCls && vehCls->IsDerivedFrom(*manCls)) continue;
    }
    else if (side == TAmbientLife)
    {
      // list only agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (!entry || entry->GetSize() == 0) continue;
    }
    else
    {
      if (side != vehSide) continue;
      
      // do not list agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (entry && entry->GetSize() > 0) continue;

      // note: some objects may be inserted only as empty
      bool hasDriver = vehEntry>>"hasDriver";
      bool hasGunner = HasPrimaryGunnerTurret(vehEntry);
      bool hasCommander = HasPrimaryObserverTurret(vehEntry);
      if (!hasDriver && !hasGunner && !hasCommander)
      {
        // simulation should be static or thing
        RStringB sim = vehEntry>>"simulation";
        #if _ENABLE_REPORT
        if
        (
          strcmpi(sim,"house") && strcmpi(sim,"thing") &&
          strcmpi(sim,"fire") && strcmpi(sim,"fountain") &&
          strcmpi(sim,"flagcarrier") && strcmpi(sim,"church")
        )
        {
          RptF
          (
            "Empty vehicle %s - simulation %s (this simulation is considered non-empty only)",
            (const char *)vehEntry.GetName(),(const char *)sim
          );
        }
        #endif
        continue;
      }
    }
    if(enableFaction)
    {//check if vehicle is in correct faction (if not skip)
      RString faction = vehEntry >> "faction";
      if (faction.GetLength() == 0) continue;
      //in combo box is used displayName
      RString factionName = Pars >> "CfgFactionClasses" >> faction >> "displayName";
      if (stricmp(factionName, selectedFaction) != 0) continue;
    }

    RStringB name = vehEntry >> "vehicleClass";
    if (name.GetLength() == 0) continue;
#if _VBS2 // categorize game vehicle classes seperately from VBS2 classes
    bool isGame = true;
    if (ConstParamEntryPtr isVBS = vehEntry.FindEntry("vbs_entity"))
      if (isVBS)
      {
        bool isVBS = vehEntry >> "vbs_entity";          
        isGame = !isVBS;
      }
    if (isGame) name = LocalizeString("STR_EDITOR_GAME_CLASS") + " " + name;
#endif
    int index = vehClasses.FindKey(name);
    if (index<0) index = vehClasses.AddUnique(name);
  }
  
  RStringB vehicleClass;
  if (vehicle && *vehicle)
  {
    vehicleClass = Pars >> "CfgVehicles" >> vehicle >> "vehicleClass";
  }

  // note: if "men" class is present, we want it to be first
  // other classes should be sorted alphabetically
  QSort(vehClasses.Data(),vehClasses.Size(),CmpClass);

  combo->ClearStrings();
  int sel = 0;
  n = vehClasses.Size();
  for (int i=0; i<n; i++)
  {
    //if (classCounts[i] == 0) continue;
    RStringB name = vehClasses[i];
    if(side == TLogic && strcmpi(name.Data(),"Modules") == 0) continue;
#if _VBS2 // categorize game vehicle classes seperately from VBS2 classes  
    int index;
    RString gameText = LocalizeString("STR_EDITOR_GAME_CLASS");
    if (strstr(name,gameText))  // contains (Game) or similar
    {
      const char *ptr = name + gameText.GetLength() + 1;
      name = RString(ptr,name.GetLength());
      index = combo->AddString(gameText + " " + RString(Pars >> "CfgVehicleClasses" >> name >> "displayName"));
    }
    else
      index = combo->AddString(Pars >> "CfgVehicleClasses" >> name >> "displayName");
    if (vehicleClass.GetLength() == 0 && strstr(name, "Men") && sel == 0) sel = index;
#else
    int index = combo->AddString(Pars >> "CfgVehicleClasses" >> name >> "displayName");
#endif
    combo->SetData(index, name);
    if (stricmp(vehicleClass, name) == 0) sel = index;
  }
  combo->SetCurSel(sel);
}

/*!
\patch 1.66 Date 6/6/2002 by Jirka
- Improved: Mission editor - sort vehicle types in Insert unit dialog
*/

void DisplayArcadeUnit::UpdateVehicles(CCombo *combo, const char *vehicle)
{
  bool isVehicle = vehicle && *vehicle;

  CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (!combo2) return;
  int side = combo2->GetValue(combo2->GetCurSel());
  //only following sides uses factions
  bool enableFaction = side == TWest || side == TEast || side == TGuerrila || side == TCivilian;
  RString selectedFaction;
  if(enableFaction)
  {//get selected faction, empty has no faction
    combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_FACTION));
    if (!combo2) return;
    int seldFactionIndex = combo2->GetCurSel();
    if (seldFactionIndex < 0) return; //units have default faction, so something should be selected
    selectedFaction = combo2->GetText(seldFactionIndex);
  }
 
  combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CLASS));
  if (!combo2) return;
  int sel2 = combo2->GetCurSel();
  if (sel2 < 0) return;
  RString vehicleClass = combo2->GetData(sel2);

  combo->ClearStrings();
  ConstParamClassPtr cls = Pars.GetClass("CfgVehicles");
  RString firstVehicle;
  for (int i=0; i<cls->GetEntryCount(); i++)
  {
    ParamEntryVal vehEntry = cls->GetEntry(i);
    const ParamClass *vehCls = vehEntry.GetClassInterface();
    if (!vehCls) continue;
    int scope = vehEntry >> "scope";
    if (scope != 2) continue;
    int vehSide = vehEntry >> "side";  
    bool enableVehicleFaction = vehSide == TWest || vehSide == TEast || vehSide == TGuerrila || vehSide == TCivilian;
    if (side == TEmpty)
    {
      if (vehSide == TLogic) continue;
      if (vehCls && vehCls->IsDerivedFrom(*cls->GetClass("Man"))) continue;
    }
    else if (side == TAmbientLife)
    {
      // list only agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (!entry || entry->GetSize() == 0) continue;
    }
    else
    {
      if (side != vehSide) continue;
      // do not list agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (entry && entry->GetSize() > 0) continue;
    }

    if(enableFaction)
    { //check if vehicle is in correct faction (if not skip)
      RString faction = vehEntry >> "faction";
      if (faction.GetLength() == 0) continue;
      //in combo box is used displayName
      RString factionName = Pars >> "CfgFactionClasses" >> faction >> "displayName";
      if (stricmp(factionName, selectedFaction) != 0) continue;
    }
    //check if vehicle is in correct class 
    RString name = vehEntry >> "vehicleClass";
    if ( name.GetLength() == 0) continue;
    if (stricmp(name, vehicleClass) != 0) continue;

    RString displayName = vehEntry >> "displayName";
    if(side == TEmpty)
    {
      RString faction =  vehEntry >> "faction";
      RString factionClass = Pars >> "CfgFactionClasses" >> faction >> "displayname";

      if(enableVehicleFaction && faction.GetLength()>0 && strcmpi(faction,"default")!=0)
        displayName = displayName + "  [" + factionClass + "]";
    }
    int index = combo->AddString(displayName);
    combo->SetData(index, vehEntry.GetName());
    
    if (firstVehicle.GetLength() == 0)
      firstVehicle = vehEntry.GetName();
  }
  combo->SortItems();

  int sel = -1;
  if (isVehicle)
    for (int i=0; i<combo->GetSize(); i++)
    {
      if (stricmp(combo->GetData(i), vehicle) == 0)
      {
        sel = i;
        break;
      }
    }
  if (sel < 0 && firstVehicle.GetLength() > 0)
    for (int i=0; i<combo->GetSize(); i++)
    {
      if (stricmp(combo->GetData(i), firstVehicle) == 0)
      {
        sel = i;
        break;
      }
    }
  saturateMax(sel, 0);
  combo->SetCurSel(sel);
}

void DisplayArcadeUnit::UpdatePlayer(CCombo *combo, ArcadeUnitPlayer player)
{
  combo->ClearStrings();
  int index = combo->AddString(LocalizeString(IDS_NONPLAYABLE));
  combo->SetValue(index, APNonplayable);

  CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (!combo2) return;
  int side = combo2->GetValue(combo2->GetCurSel());
  if (side != TWest && side != TEast && side != TGuerrila && side != TCivilian && side != TAmbientLife)
  {
    combo->SetCurSel(0);
    return;
  }

  // West, East, Guerrila, Civilian
  combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
  if (!combo2) return;
  int i = combo2->GetCurSel();
  if (i < 0) return;
  Ref<EntityType> eType = VehicleTypes.New(combo2->GetData(i));
  EntityAIType *type = dynamic_cast<EntityAIType *>(eType.GetRef());
  if( !type )
  {
    Fail("Non-AI type");
    return;
  }
  
  int sel = 0;
  bool commander = type->HasObserver();
  bool gunner = type->HasGunner();
  bool driver = type->HasDriver();
  bool air = type->IsKindOf(GWorld->Preloaded(VTypeAir));

  // find nearest valid position
  if (!commander)
  {
    if (!gunner)
      switch (player)
      {
      case APPlayerDriver:
      case APPlayerGunner:
        player = APPlayerCommander; break;
      case APPlayableC:
      case APPlayableD:
      case APPlayableG:
      case APPlayableCD:
      case APPlayableCG:
      case APPlayableDG:
        player = APPlayableCDG; break;
      }
    else
      switch (player)
      {
      case APPlayerCommander:
        {
          TransportType *tt = dynamic_cast<TransportType *>(type);
          bool driverIsCommander = true;
          if (tt) driverIsCommander = tt->DriverIsCommander();
          if (driverIsCommander) player = APPlayerDriver;
          else player = APPlayerGunner;
        }
        break;
      case APPlayableC:
        player = APPlayableD; break;
      case APPlayableCD:
        player = APPlayableD; break;
      case APPlayableCG:
        player = APPlayableG; break;
      case APPlayableCDG:
        player = APPlayableCDG; break; // FIX (APPlayableDG was not recognized)
      }
  }
  else if (!gunner)
  {
    switch (player)
    {
    case APPlayerGunner:
      player = APPlayerDriver; break;
    case APPlayableG:
      player = APPlayableD; break;
    case APPlayableCG:
      player = APPlayableC; break;
    case APPlayableDG:
      player = APPlayableD; break;
    case APPlayableCDG:
      player = APPlayableCG; break;
    }
  }
  if (!driver && gunner)
  {
    if (player!=APNonplayable)
    {
      switch(player)
      {
        case APPlayerDriver:
        case APPlayerGunner:
        case APPlayerCommander:
          player = APPlayerGunner; break;
        default:
          player = APPlayableG; break;
      }
    }
  }

  if (commander)
  {
    // Player as commander
    index = combo->AddString(LocalizeString(IDS_PLAYER_COMMANDER));
    combo->SetValue(index, APPlayerCommander);
    if (player == APPlayerCommander) sel = index;
    // Player as pilot / driver
    if (driver)
    {
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYER_PILOT));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYER_DRIVER));
      combo->SetValue(index, APPlayerDriver);
      if (player == APPlayerDriver) sel = index;
    }
    // Player as gunner
    if (gunner)
    {
      index = combo->AddString(LocalizeString(IDS_PLAYER_GUNNER));
      combo->SetValue(index, APPlayerGunner);
      if (player == APPlayerGunner) sel = index;
    }
    // Playable as commander
    index = combo->AddString(LocalizeString(IDS_PLAYABLE_C));
    combo->SetValue(index, APPlayableC);
    if (player == APPlayableC) sel = index;
    if (driver)
    {
      // Playable as pilot / driver
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_P));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_D));
      combo->SetValue(index, APPlayableD);
      if (player == APPlayableD) sel = index;
    }
    // Playable as gunner
    if (gunner)
    {
      index = combo->AddString(LocalizeString(IDS_PLAYABLE_G));
      combo->SetValue(index, APPlayableG);
      if (player == APPlayableG) sel = index;
    }
    if (driver)
    {
      // Playable as commander and pilot / driver
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_CP));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_CD));
      combo->SetValue(index, APPlayableCD);
      if (player == APPlayableCD) sel = index;
    }
    if (gunner)
    {
      // Playable as commander and gunner
      index = combo->AddString(LocalizeString(IDS_PLAYABLE_CG));
      combo->SetValue(index, APPlayableCG);
      if (player == APPlayableCG) sel = index;
      // Playable as pilot / driver and gunner
      if (driver)
      {
        if (air)
          index = combo->AddString(LocalizeString(IDS_PLAYABLE_PG));
        else
          index = combo->AddString(LocalizeString(IDS_PLAYABLE_DG));
        combo->SetValue(index, APPlayableDG);
        if (player == APPlayableDG) sel = index;
        // Playable as commander, pilot / driver and gunner
        if (air)
          index = combo->AddString(LocalizeString(IDS_PLAYABLE_CPG));
        else
          index = combo->AddString(LocalizeString(IDS_PLAYABLE_CDG));
        combo->SetValue(index, APPlayableCDG);  // all available
        if (player == APPlayableCDG) sel = index;
      }
    }
    else
    {
      combo->SetValue(index, APPlayableCDG);  // all available
      if (player == APPlayableCDG) sel = index;
    }
  }
  else if (gunner)
  {
    // Player as pilot / driver
    if (driver)
    {
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYER_PILOT));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYER_DRIVER));
      combo->SetValue(index, APPlayerDriver);
      if (player == APPlayerDriver) sel = index;
    }
    // Player as gunner
    index = combo->AddString(LocalizeString(IDS_PLAYER_GUNNER));
    combo->SetValue(index, APPlayerGunner);
    if (player == APPlayerGunner) sel = index;
    if (driver)
    {
      // Playable as pilot / driver
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_P));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_D));
      combo->SetValue(index, APPlayableD);
      if (player == APPlayableD) sel = index;
    }
    // Playable as gunner
    index = combo->AddString(LocalizeString(IDS_PLAYABLE_G));
    combo->SetValue(index, APPlayableG);
    if (player == APPlayableG) sel = index;
    // Playable as pilot / driver and gunner
    if (driver)
    {
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_PG));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_DG));
      combo->SetValue(index, APPlayableCDG);  // all available
      if (player == APPlayableCDG) sel = index;
    }
  }
  else
  {
    // Player
    index = combo->AddString(LocalizeString(IDS_PLAYER));
    combo->SetValue(index, APPlayerCommander);
    if (player == APPlayerCommander) sel = index;
    // Playable
    index = combo->AddString(LocalizeString(IDS_PLAYABLE));
    combo->SetValue(index, APPlayableCDG);  // all available
    if (player == APPlayableCDG) sel = index;
  }

  combo->SetCurSel(sel);
}

Control *DisplayArcadeUnit::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_ARCUNIT_TITLE:
    {
      CStatic *text = new CStatic(this, idc, cls);
      if (_index < 0)
        text->SetText(LocalizeString(IDS_ARCUNIT_TITLE2));
      else
        text->SetText(LocalizeString(IDS_ARCUNIT_TITLE4));
      return text;
    }
  case IDC_ARCUNIT_AZIMUT_PICTURE:
    return new CStaticAzimut(this, idc, cls, IDC_ARCUNIT_AZIMUT);
  case IDC_ARCUNIT_SIDE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int index, sel = 0;
      index = combo->AddString(LocalizeString(IDS_WEST));
      combo->SetValue(index, TWest);
      if (_unit.side == TWest) sel = index;
      index = combo->AddString(LocalizeString(IDS_EAST));
      combo->SetValue(index, TEast);
      if (_unit.side == TEast) sel = index;
      index = combo->AddString(LocalizeString(IDS_GUERRILA));
      combo->SetValue(index, TGuerrila);
      if (_unit.side == TGuerrila) sel = index;
      index = combo->AddString(LocalizeString(IDS_CIVILIAN));
      combo->SetValue(index, TCivilian);
      if (_unit.side == TCivilian) sel = index;
      index = combo->AddString(LocalizeString(IDS_AMBIENT_LIFE));
      combo->SetValue(index, TAmbientLife);
      if (_unit.side == TAmbientLife) sel = index;
      if (_unit.player == APNonplayable)
      {
        index = combo->AddString(LocalizeString(IDS_LOGIC));
        combo->SetValue(index, TLogic);
        if (_unit.side == TLogic) sel = index;
        index = combo->AddString(LocalizeString(IDS_EMPTY));
        combo->SetValue(index, TEmpty);
        if (_unit.side == TEmpty) sel = index;
      }
      combo->SetCurSel(sel);
      if (_index >= 0)
      {
        // cannot change side when edited
        combo->EnableCtrl(false);
      }
      return combo;
    }
  case IDC_ARCUNIT_FACTION:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      UpdateFactions(combo, _unit.vehicle);
      if (_unit.side != TWest && _unit.side != TEast && _unit.side != TGuerrila && _unit.side != TCivilian )
      {
        combo->SetCurSel(0);
        combo->ShowCtrl(false);
      }
      return combo;
    }
  case IDC_ARCUNIT_CLASS:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      UpdateClasses(combo, _unit.vehicle);
      return combo;
    }
  case IDC_ARCUNIT_VEHICLE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      UpdateVehicles(combo, _unit.vehicle);
      return combo;
    }
  case IDC_ARCUNIT_TEXT:
    {
      CEdit *text = new CEdit(this, idc, cls);
      text->SetText(_unit.name);
      return text;
    }
  case IDC_ARCUNIT_INIT:
    {
      CEdit *text = new CEdit(this, idc, cls);
      text->SetText(_unit.init);
      return text;
    }
  case IDC_ARCUNIT_DESC:
    {
      CEdit *text = new CEdit(this, idc, cls);
      text->SetText(_unit.description);
      return text;
    }
  case IDC_ARCUNIT_RANK:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      for (int i=RankPrivate; i<=RankColonel; i++)
      {
        RString rank = LocalizeString(IDS_PRIVATE + i);
        combo->AddString(rank);
      }
      combo->SetCurSel(_unit.rank - RankPrivate);
      if (_unit.side == TEmpty || _unit.side == TLogic)
      {
        combo->SetCurSel(0);
        combo->ShowCtrl(false);
      }
      return combo;
    }
  case IDC_ARCUNIT_AZIMUT:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _unit.azimut);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCUNIT_SPECIAL:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      combo->AddString(LocalizeString(IDS_SPECIAL_NONE));
      combo->AddString(LocalizeString(IDS_SPECIAL_CARGO));
      combo->AddString(LocalizeString(IDS_SPECIAL_FLYING));
      combo->AddString(LocalizeString(IDS_SPECIAL_FORM));
      combo->SetCurSel(_unit.special);
      return combo;
    }
  case IDC_ARCUNIT_CTRL:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      UpdatePlayer(combo, _unit.player);
      return combo;
    }
  case IDC_ARCUNIT_AGE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      for (int i=0; i<AAN; i++)
        combo->AddString(LocalizeString(IDS_AGE_ACTUAL + i));
      combo->SetCurSel(_unit.age);
      return combo;
    }
  case IDC_ARCUNIT_SKILL:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0.2, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_unit.skill);
      }
      return ctrl;
    }
  case IDC_ARCUNIT_HEALTH:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_unit.health);
      }
      return ctrl;
    }
  case IDC_ARCUNIT_FUEL:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_unit.fuel);
      }
      return ctrl;
    }
  case IDC_ARCUNIT_AMMO:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_unit.ammo);
      }
      return ctrl;
    }
  case IDC_ARCUNIT_PRESENCE:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_unit.presence);
      }
      return ctrl;
    }
  case IDC_ARCUNIT_PRESENCE_COND:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_unit.presenceCondition);
      return edit;
    }
  case IDC_ARCUNIT_PLACE:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _unit.placement);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCUNIT_LOCK:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int sel = 1; // default
      int index = combo->AddString(LocalizeString(IDS_VEHICLE_UNLOCKED));
      combo->SetValue(index, LSUnlocked);
      if (_unit.lock == LSUnlocked) sel = index;
      index = combo->AddString(LocalizeString(IDS_VEHICLE_DEFAULT));
      combo->SetValue(index, LSDefault);
      if (_unit.lock == LSDefault) sel = index;
      index = combo->AddString(LocalizeString(IDS_VEHICLE_LOCKED));
      combo->SetValue(index, LSLocked);
      if (_unit.lock == LSLocked) sel = index;
      combo->SetCurSel(sel);
      return combo;
    }
  default:
    return Display::OnCreateCtrl(type, idc, cls);
  }
}

bool DisplayArcadeUnit::CanDestroy()
{
  if (!Display::CanDestroy()) return false;

  if (_exit == IDC_OK)
  {
    CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_TEXT));
    if (edit)
    {
      RString text = edit->GetText();
      if (text.GetLength() > 0)
      {
        if (!GWorld->GetGameState()->IdtfGoodName(text))
        {
          CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_NAME));
          return false;
        }
        // check if text is not used
        for (int i=0; i<_template->groups.Size(); i++)
        {
          ArcadeGroupInfo &gInfo = _template->groups[i];
          for (int j=0; j<gInfo.units.Size(); j++)
          {
            if (i == _indexGroup && j == _index) continue;
            if (stricmp(text, gInfo.units[j].name) == 0)
            {
              CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
              return false;
            }
          }
          for (int j=0; j<gInfo.sensors.Size(); j++)
          {
            if (stricmp(text, gInfo.sensors[j].name) == 0)
            {
              CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
              return false;
            }
          }
        }
        for (int j=0; j<_template->emptyVehicles.Size(); j++)
        {
          if (_indexGroup == -1 && j == _index) continue;
          if (stricmp(text, _template->emptyVehicles[j].name) == 0)
          {
            CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
            return false;
          }
        }
        for (int j=0; j<_template->sensors.Size(); j++)
        {
          if (stricmp(text, _template->sensors[j].name) == 0)
          {
            CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
            return false;
          }
        }
      }
    }

    GameState *gstate = GWorld->GetGameState();

    edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_INIT));
    if (edit)
    {
      RString exp = edit->GetText();
      if (!gstate->CheckExecute(exp, GWorld->GetMissionNamespace())) // mission namespace
      {
        FocusCtrl(IDC_ARCUNIT_INIT);
        CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
        edit->SetCaretPos(gstate->GetLastErrorPos(exp));
        return false;
      }
    }

    edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_PRESENCE_COND));
    if (edit)
    {
      RString exp = edit->GetText();
      if (!gstate->CheckEvaluateBool(exp, GWorld->GetMissionNamespace())) // mission namespace
      {
        FocusCtrl(IDC_ARCUNIT_PRESENCE_COND);
        CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
        edit->SetCaretPos(gstate->GetLastErrorPos(exp));
        return false;
      }
    }

    CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
    if (combo)
    {
      if (combo->GetCurSel() < 0)
      {
        CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_NO_VEH_SELECT));
        return false;
      }
    }
  }
  return true;
}

void DisplayArcadeUnit::Destroy()
{
  Display::Destroy();
  
  if (_exit != IDC_OK) return;

  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (combo)
    _unit.side = (TargetSide)combo->GetValue(combo->GetCurSel());
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CTRL));
  if (combo)
  {
    _unit.player = (ArcadeUnitPlayer)combo->GetValue(combo->GetCurSel());
    if (_unit.side == TEmpty || _unit.side == TLogic) _unit.player = APNonplayable;
  }
  CSliderContainer *slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_ARCUNIT_HEALTH));
  if (slider)
    _unit.health = slider->GetThumbPos();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_ARCUNIT_FUEL));
  if (slider)
    _unit.fuel = slider->GetThumbPos();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_ARCUNIT_AMMO));
  if (slider)
    _unit.ammo = slider->GetThumbPos();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_ARCUNIT_SKILL));
  if (slider)
    _unit.skill = slider->GetThumbPos();
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
  if (combo)
  {
    int indexVehicle = combo->GetCurSel();
    Assert(indexVehicle >= 0);
    _unit.vehicle = combo->GetData(indexVehicle);
  }
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_RANK));
  if (combo)
  {
    _unit.rank = (Rank)(RankPrivate + combo->GetCurSel());
    if (_unit.side == TEmpty || _unit.side == TLogic) _unit.rank = RankPrivate;
  }
  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_AZIMUT));
  if (edit)
    _unit.azimut = edit->GetText() ? atof(edit->GetText()) : 0;
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SPECIAL));
  if (combo)
    _unit.special = (ArcadeUnitSpecial)combo->GetCurSel();
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_AGE));
  if (combo)
    _unit.age = (ArcadeUnitAge)combo->GetCurSel();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_ARCUNIT_PRESENCE));
  if (slider)
    _unit.presence = slider->GetThumbPos();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_PRESENCE_COND));
  if (edit)
    _unit.presenceCondition = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_PLACE));
  if (edit)
    _unit.placement = edit->GetText() ? atof(edit->GetText()) : 0;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_TEXT));
  if (edit)
    _unit.name = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_INIT));
  if (edit)
    _unit.init = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_DESC));
  if (edit)
    _unit.description = edit->GetText();
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_LOCK));
  if (combo)
  {
    int sel = combo->GetCurSel();
    if (sel >= 0) _unit.lock = (LockState)combo->GetValue(sel);
  }
}

//--------------
void DisplayArcadeModules::OnLBSelChanged(IControl *ctrl, int curSel)
{
  switch (ctrl->IDC())
  {
  case IDC_ARCUNIT_SIDE:
    {
      CCombo *combo = dynamic_cast<CCombo *>(ctrl);
      int side = combo ? combo->GetValue(curSel) : TWest;
      bool enable = side == TWest || side == TEast || side == TGuerrila || side == TCivilian || side == TAmbientLife;
      IControl *ctrl2;
      ctrl2 = GetCtrl(IDC_ARCUNIT_RANK);
      if (ctrl2) ctrl2->ShowCtrl(enable);
      ctrl2 = GetCtrl(IDC_ARCUNIT_CTRL);
      if (ctrl2) ctrl2->ShowCtrl(enable);
      //show/hide faction comboBox
      enable = side == TWest || side == TEast || side == TGuerrila || side == TCivilian;
      ctrl2 = GetCtrl(IDC_ARCUNIT_FACTION);
      if (ctrl2) ctrl2->ShowCtrl(enable);

      RString vehicle;
      combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
      if (combo)
      {
        int i = combo->GetCurSel();
        if (i < 0)
          vehicle = "";
        else
          vehicle = combo->GetData(i);
      }
      combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_FACTION));
      if (combo) UpdateFactions(combo, vehicle);
    }
    // continue
  case IDC_ARCUNIT_FACTION:
    {//faction has been changed
      RString vehicle;
      CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
      if (combo)
      {//get selected vehicle
        int i = combo->GetCurSel();
        if (i < 0)
          vehicle = "";
        else
          vehicle = combo->GetData(i);
      }
      //update faction according to selected vehicle
      combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CLASS));
      if (combo) UpdateClasses(combo, vehicle);
    }
    // continue (update combo with faction is called with false)
  case IDC_ARCUNIT_CLASS:
    {
      CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
      if (combo)
      {
        int i = combo->GetCurSel();
        RString vehicle;
        if (i < 0)
          vehicle = "";
        else
          vehicle = combo->GetData(i);
        UpdateVehicles(combo, vehicle);
      }
    }
    break;
  case IDC_ARCUNIT_VEHICLE:
    {
      CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CTRL));
      if (combo)
      {
        UpdatePlayer(combo, (ArcadeUnitPlayer)combo->GetValue(combo->GetCurSel()));
      }
    }
    break;
  case IDC_ARCUNIT_CTRL:
    {
      CListBoxContainer *list = GetListBoxContainer(ctrl);
      if (!list) break;

      // update the content of Side combo
      CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
      if (combo && curSel >= 0)
      {
        int value = list->GetValue(curSel);
        if (value != APNonplayable && combo->GetSize() == 5)
        {
          // Playable
          // combo content WEST, EAST, GUERRILA, CIVILIAN, AMBIENT_LIFE - OK
          break;
        }
        if (value == APNonplayable && combo->GetSize() == 7)
        {
          // non Playable
          // combo content WEST, EAST, GUERRILA, CIVILIAN, AMBIENT_LIFE, LOGIC, EMPTY - OK
          break;
        }
        int sel = combo->GetCurSel();
        combo->ClearStrings();
        int index;
        index = combo->AddString(LocalizeString(IDS_WEST));
        combo->SetValue(index, TWest);
        index = combo->AddString(LocalizeString(IDS_EAST));
        combo->SetValue(index, TEast);
        index = combo->AddString(LocalizeString(IDS_GUERRILA));
        combo->SetValue(index, TGuerrila);
        index = combo->AddString(LocalizeString(IDS_CIVILIAN));
        combo->SetValue(index, TCivilian);
        index = combo->AddString(LocalizeString(IDS_AMBIENT_LIFE));
        combo->SetValue(index, TAmbientLife);
        if (curSel == 0)
        {
          index = combo->AddString(LocalizeString(IDS_LOGIC));
          combo->SetValue(index, TLogic);
          index = combo->AddString(LocalizeString(IDS_EMPTY));
          combo->SetValue(index, TEmpty);
        }
        if (sel >= combo->GetSize()) sel = 0;
        combo->SetCurSel(sel, false);
      }
    }
    break;
  }
  Display::OnLBSelChanged(ctrl, curSel);
}


//15.7. 2008 - new filter for 2D editor allowing to faction in sides
void DisplayArcadeModules::UpdateFactions(CCombo *combo, const char *vehicle)
{ //get current side
  CCombo *comboSides = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (!comboSides) return;
  int side = comboSides->GetValue(comboSides->GetCurSel());
  //only following sides uses factions
  bool enableFaction = side == TWest || side == TEast || side == TGuerrila || side == TCivilian;

  ParamEntryVal clsVeh = Pars >> "CfgVehicles";
  int n = clsVeh.GetEntryCount();
  FindArrayRStringBCI vehClasses;
  ConstParamEntryPtr manCls = clsVeh.FindEntry("Man");

  // scan all CfgVehicles for vehicleClass list
  for (int i=0; i<n; i++)
  {
    ParamEntryVal vehEntry = clsVeh.GetEntry(i);
    if (!vehEntry.IsClass()) continue;
    int scope = vehEntry >> "scope";
    if (scope != 2) continue; //2 is unit placeable in editor
    int vehSide = vehEntry >> "side";

    if (side == TEmpty)
    {
      if (vehSide == TLogic) continue;
      const ParamClass *vehCls = vehEntry.GetClassInterface();
      if (manCls && vehCls && vehCls->IsDerivedFrom(*manCls)) continue;
    }
    else if (side == TAmbientLife)
    {
      // list only agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (!entry || entry->GetSize() == 0) continue;
    }
    else
    {
      if (side != vehSide) continue;

      // do not list agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (entry && entry->GetSize() > 0) continue;

      // note: some objects may be inserted only as empty
      bool hasDriver = vehEntry>>"hasDriver";
      bool hasGunner = HasPrimaryGunnerTurret(vehEntry);
      bool hasCommander = HasPrimaryObserverTurret(vehEntry);
      if (!hasDriver && !hasGunner && !hasCommander)
      {
        // simulation should be static or thing
        RStringB sim = vehEntry>>"simulation";
#if _ENABLE_REPORT
        if
          (
          strcmpi(sim,"house") && strcmpi(sim,"thing") &&
          strcmpi(sim,"fire") && strcmpi(sim,"fountain") &&
          strcmpi(sim,"flagcarrier") && strcmpi(sim,"church")
          )
        {
          RptF
            (
            "Empty vehicle %s - simulation %s (this simulation is considered non-empty only)",
            (const char *)vehEntry.GetName(),(const char *)sim
            );
        }
#endif
        continue;
      }
    }

    if(enableFaction)
    {
      RStringB name = vehEntry >> "faction";
      if (name.GetLength() == 0) continue;

      int index = vehClasses.FindKey(name);
      if (index<0) index = vehClasses.AddUnique(name);
    }
  }

  QSort(vehClasses.Data(),vehClasses.Size(),CmpFaction);
  //get display name of currently selected faction / faction of edited vehicle
  RStringB vehicleFaction;
  if (vehicle && *vehicle && enableFaction)
  {
    ParamClassEntry paramVeh = Pars >> "CfgVehicles" >> vehicle;
    SRef<ClassEntry>  entry = paramVeh.FindEntry("faction");   
    if(entry)
    {
      vehicleFaction = Pars >> "CfgVehicles" >> vehicle >> "faction";
    }
  }

  combo->ClearStrings();
  int sel = 0; 
  n = vehClasses.Size();
  for (int i=0; i<n; i++)
  {
    RStringB name = vehClasses[i];
    int index = combo->AddString(Pars >> "CfgFactionClasses" >> name >> "displayName");
    combo->SetData(index, name);
    //if unit is only edited, find correct faction 
    if (vehicle && *vehicle) 
      if (stricmp(vehicleFaction, name) == 0) sel = index;
  }
  combo->SetCurSel(sel,false);
}

/*!
\patch 1.82 Date 8/22/2002 by Ondra
- Improved: Editor: Vehicle classes (Armored, Car, ...) are now fully dynamic.
Any addon maker can now introduce his own classes.
\patch 1.82 Date 8/22/2002 by Ondra
- Fixed: Some empty objects could also be inserted as Civilian, using group slots
and causing confusion inside of the editor.
*/


void DisplayArcadeModules::UpdateClasses(CCombo *combo, const char *vehicle)
{
  CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (!combo2) return;
  int side = combo2->GetValue(combo2->GetCurSel());
  //only following sides uses factions
  bool enableFaction = side == TWest || side == TEast || side == TGuerrila || side == TCivilian;
  RString selectedFaction;
  if(enableFaction)
  {//get selected faction, empty has no faction
    combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_FACTION));
    if (!combo2) return;
    int seldFactionIndex = combo2->GetCurSel();
    if (seldFactionIndex < 0) return; //units have default faction, so something should be selected
    selectedFaction = combo2->GetText(seldFactionIndex);
  }

  ParamEntryVal clsVeh = Pars >> "CfgVehicles";
  int n = clsVeh.GetEntryCount();
  FindArrayRStringBCI vehClasses;
  ConstParamEntryPtr manCls = clsVeh.FindEntry("Man");
  // scan all CfgVehicles for vehicleClass list
  for (int i=0; i<n; i++)
  {
    ParamEntryVal vehEntry = clsVeh.GetEntry(i);
    if (!vehEntry.IsClass()) continue;
    int scope = vehEntry >> "scope";
    if (scope != 2) continue;
    int vehSide = vehEntry >> "side";

    if (side == TEmpty)
    {
      if (vehSide == TLogic) continue;
      const ParamClass *vehCls = vehEntry.GetClassInterface();
      if (manCls && vehCls && vehCls->IsDerivedFrom(*manCls)) continue;
    }
    else if (side == TAmbientLife)
    {
      // list only agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (!entry || entry->GetSize() == 0) continue;
    }
    else
    {
      if (side != vehSide) continue;

      // do not list agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (entry && entry->GetSize() > 0) continue;

      // note: some objects may be inserted only as empty
      bool hasDriver = vehEntry>>"hasDriver";
      bool hasGunner = HasPrimaryGunnerTurret(vehEntry);
      bool hasCommander = HasPrimaryObserverTurret(vehEntry);
      if (!hasDriver && !hasGunner && !hasCommander)
      {
        // simulation should be static or thing
        RStringB sim = vehEntry>>"simulation";
#if _ENABLE_REPORT
        if
          (
          strcmpi(sim,"house") && strcmpi(sim,"thing") &&
          strcmpi(sim,"fire") && strcmpi(sim,"fountain") &&
          strcmpi(sim,"flagcarrier") && strcmpi(sim,"church")
          )
        {
          RptF
            (
            "Empty vehicle %s - simulation %s (this simulation is considered non-empty only)",
            (const char *)vehEntry.GetName(),(const char *)sim
            );
        }
#endif
        continue;
      }
    }
    if(enableFaction)
    {//check if vehicle is in correct faction (if not skip)
      RString faction = vehEntry >> "faction";
      if (faction.GetLength() == 0) continue;
      //in combo box is used displayName
      RString factionName = Pars >> "CfgFactionClasses" >> faction >> "displayName";
      if (stricmp(factionName, selectedFaction) != 0) continue;
    }

    RStringB name = vehEntry >> "vehicleClass";
    if (name.GetLength() == 0) continue;
#if _VBS2 // categorize game vehicle classes seperately from VBS2 classes
    bool isGame = true;
    if (ConstParamEntryPtr isVBS = vehEntry.FindEntry("vbs_entity"))
      if (isVBS)
      {
        bool isVBS = vehEntry >> "vbs_entity";          
        isGame = !isVBS;
      }
      if (isGame) name = LocalizeString("STR_EDITOR_GAME_CLASS") + " " + name;
#endif
      int index = vehClasses.FindKey(name);
      if (index<0) index = vehClasses.AddUnique(name);
  }

  RStringB vehicleClass;
  if (vehicle && *vehicle)
  {
    vehicleClass = Pars >> "CfgVehicles" >> vehicle >> "vehicleClass";
  }

  // note: if "men" class is present, we want it to be first
  // other classes should be sorted alphabetically
  QSort(vehClasses.Data(),vehClasses.Size(),CmpClass);

  combo->ClearStrings();
  int sel = 0;
  n = vehClasses.Size();
  for (int i=0; i<n; i++)
  {
    //if (classCounts[i] == 0) continue;
    RStringB name = vehClasses[i];
    if(side != TLogic || strcmpi(name.Data(),"Modules") != 0) continue;
#if _VBS2 // categorize game vehicle classes seperately from VBS2 classes  
    int index;
    RString gameText = LocalizeString("STR_EDITOR_GAME_CLASS");
    if (strstr(name,gameText))  // contains (Game) or similar
    {
      const char *ptr = name + gameText.GetLength() + 1;
      name = RString(ptr,name.GetLength());
      index = combo->AddString(gameText + " " + RString(Pars >> "CfgVehicleClasses" >> name >> "displayName"));
    }
    else
      index = combo->AddString(Pars >> "CfgVehicleClasses" >> name >> "displayName");
    if (vehicleClass.GetLength() == 0 && strstr(name, "Men") && sel == 0) sel = index;
#else
    int index = combo->AddString(Pars >> "CfgVehicleClasses" >> name >> "displayName");
#endif
    combo->SetData(index, name);
    if (stricmp(vehicleClass, name) == 0) sel = index;
  }
  combo->SetCurSel(sel);
}

/*!
\patch 1.66 Date 6/6/2002 by Jirka
- Improved: Mission editor - sort vehicle types in Insert unit dialog
*/

void DisplayArcadeModules::UpdateVehicles(CCombo *combo, const char *vehicle)
{
  bool isVehicle = vehicle && *vehicle;

  CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (!combo2) return;
  int side = combo2->GetValue(combo2->GetCurSel());
  //only following sides uses factions
  bool enableFaction = side == TWest || side == TEast || side == TGuerrila || side == TCivilian;
  RString selectedFaction;
  if(enableFaction)
  {//get selected faction, empty has no faction
    combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_FACTION));
    if (!combo2) return;
    int seldFactionIndex = combo2->GetCurSel();
    if (seldFactionIndex < 0) return; //units have default faction, so something should be selected
    selectedFaction = combo2->GetText(seldFactionIndex);
  }

  combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CLASS));
  if (!combo2) return;
  int sel2 = combo2->GetCurSel();
  if (sel2 < 0) return;
  RString vehicleClass = combo2->GetData(sel2);

  combo->ClearStrings();
  ConstParamClassPtr cls = Pars.GetClass("CfgVehicles");
  RString firstVehicle;
  for (int i=0; i<cls->GetEntryCount(); i++)
  {
    ParamEntryVal vehEntry = cls->GetEntry(i);
    const ParamClass *vehCls = vehEntry.GetClassInterface();
    if (!vehCls) continue;
    int scope = vehEntry >> "scope";
    if (scope != 2) continue;
    int vehSide = vehEntry >> "side";  
    bool enableVehicleFaction = vehSide == TWest || vehSide == TEast || vehSide == TGuerrila || vehSide == TCivilian;
    if (side == TEmpty)
    {
      if (vehSide == TLogic) continue;
      if (vehCls && vehCls->IsDerivedFrom(*cls->GetClass("Man"))) continue;
    }
    else if (side == TAmbientLife)
    {
      // list only agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (!entry || entry->GetSize() == 0) continue;
    }
    else
    {
      if (side != vehSide) continue;
      // do not list agents with the nonempty agentTasks[]
      ConstParamEntryPtr entry = vehEntry.FindEntry("agentTasks");
      if (entry && entry->GetSize() > 0) continue;
    }

    if(enableFaction)
    { //check if vehicle is in correct faction (if not skip)
      RString faction = vehEntry >> "faction";
      if (faction.GetLength() == 0) continue;
      //in combo box is used displayName
      RString factionName = Pars >> "CfgFactionClasses" >> faction >> "displayName";
      if (stricmp(factionName, selectedFaction) != 0) continue;
    }
    //check if vehicle is in correct class 
    RString name = vehEntry >> "vehicleClass";
    if ( name.GetLength() == 0) continue;
    if (stricmp(name, vehicleClass) != 0) continue;

    RString displayName = vehEntry >> "displayName";
    if(side == TEmpty)
    {
      RString faction =  vehEntry >> "faction";
      RString factionClass = Pars >> "CfgFactionClasses" >> faction >> "displayname";

      if(enableVehicleFaction && faction.GetLength()>0 && strcmpi(faction,"default")!=0)
        displayName = displayName + "  [" + factionClass + "]";
    }
    int index = combo->AddString(displayName);
    combo->SetData(index, vehEntry.GetName());

    if (firstVehicle.GetLength() == 0)
      firstVehicle = vehEntry.GetName();
  }
  combo->SortItems();

  int sel = -1;
  if (isVehicle)
    for (int i=0; i<combo->GetSize(); i++)
    {
      if (stricmp(combo->GetData(i), vehicle) == 0)
      {
        sel = i;
        break;
      }
    }
    if (sel < 0 && firstVehicle.GetLength() > 0)
      for (int i=0; i<combo->GetSize(); i++)
      {
        if (stricmp(combo->GetData(i), firstVehicle) == 0)
        {
          sel = i;
          break;
        }
      }
      saturateMax(sel, 0);
      combo->SetCurSel(sel);
}

void DisplayArcadeModules::UpdatePlayer(CCombo *combo, ArcadeUnitPlayer player)
{
  combo->ClearStrings();
  int index = combo->AddString(LocalizeString(IDS_NONPLAYABLE));
  combo->SetValue(index, APNonplayable);

  CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (!combo2) return;
  int side = combo2->GetValue(combo2->GetCurSel());
  if (side != TWest && side != TEast && side != TGuerrila && side != TCivilian && side != TAmbientLife)
  {
    combo->SetCurSel(0);
    return;
  }

  // West, East, Guerrila, Civilian
  combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
  if (!combo2) return;
  int i = combo2->GetCurSel();
  if (i < 0) return;
  Ref<EntityType> eType = VehicleTypes.New(combo2->GetData(i));
  EntityAIType *type = dynamic_cast<EntityAIType *>(eType.GetRef());
  if( !type )
  {
    Fail("Non-AI type");
    return;
  }

  int sel = 0;
  bool commander = type->HasObserver();
  bool gunner = type->HasGunner();
  bool driver = type->HasDriver();
  bool air = type->IsKindOf(GWorld->Preloaded(VTypeAir));

  // find nearest valid position
  if (!commander)
  {
    if (!gunner)
      switch (player)
    {
      case APPlayerDriver:
      case APPlayerGunner:
        player = APPlayerCommander; break;
      case APPlayableC:
      case APPlayableD:
      case APPlayableG:
      case APPlayableCD:
      case APPlayableCG:
      case APPlayableDG:
        player = APPlayableCDG; break;
    }
    else
      switch (player)
    {
      case APPlayerCommander:
        {
          TransportType *tt = dynamic_cast<TransportType *>(type);
          bool driverIsCommander = true;
          if (tt) driverIsCommander = tt->DriverIsCommander();
          if (driverIsCommander) player = APPlayerDriver;
          else player = APPlayerGunner;
        }
        break;
      case APPlayableC:
        player = APPlayableD; break;
      case APPlayableCD:
        player = APPlayableD; break;
      case APPlayableCG:
        player = APPlayableG; break;
      case APPlayableCDG:
        player = APPlayableCDG; break; // FIX (APPlayableDG was not recognized)
    }
  }
  else if (!gunner)
  {
    switch (player)
    {
    case APPlayerGunner:
      player = APPlayerDriver; break;
    case APPlayableG:
      player = APPlayableD; break;
    case APPlayableCG:
      player = APPlayableC; break;
    case APPlayableDG:
      player = APPlayableD; break;
    case APPlayableCDG:
      player = APPlayableCG; break;
    }
  }
  if (!driver && gunner)
  {
    if (player!=APNonplayable)
    {
      switch(player)
      {
      case APPlayerDriver:
      case APPlayerGunner:
      case APPlayerCommander:
        player = APPlayerGunner; break;
      default:
        player = APPlayableG; break;
      }
    }
  }

  if (commander)
  {
    // Player as commander
    index = combo->AddString(LocalizeString(IDS_PLAYER_COMMANDER));
    combo->SetValue(index, APPlayerCommander);
    if (player == APPlayerCommander) sel = index;
    // Player as pilot / driver
    if (driver)
    {
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYER_PILOT));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYER_DRIVER));
      combo->SetValue(index, APPlayerDriver);
      if (player == APPlayerDriver) sel = index;
    }
    // Player as gunner
    if (gunner)
    {
      index = combo->AddString(LocalizeString(IDS_PLAYER_GUNNER));
      combo->SetValue(index, APPlayerGunner);
      if (player == APPlayerGunner) sel = index;
    }
    // Playable as commander
    index = combo->AddString(LocalizeString(IDS_PLAYABLE_C));
    combo->SetValue(index, APPlayableC);
    if (player == APPlayableC) sel = index;
    if (driver)
    {
      // Playable as pilot / driver
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_P));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_D));
      combo->SetValue(index, APPlayableD);
      if (player == APPlayableD) sel = index;
    }
    // Playable as gunner
    if (gunner)
    {
      index = combo->AddString(LocalizeString(IDS_PLAYABLE_G));
      combo->SetValue(index, APPlayableG);
      if (player == APPlayableG) sel = index;
    }
    if (driver)
    {
      // Playable as commander and pilot / driver
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_CP));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_CD));
      combo->SetValue(index, APPlayableCD);
      if (player == APPlayableCD) sel = index;
    }
    if (gunner)
    {
      // Playable as commander and gunner
      index = combo->AddString(LocalizeString(IDS_PLAYABLE_CG));
      combo->SetValue(index, APPlayableCG);
      if (player == APPlayableCG) sel = index;
      // Playable as pilot / driver and gunner
      if (driver)
      {
        if (air)
          index = combo->AddString(LocalizeString(IDS_PLAYABLE_PG));
        else
          index = combo->AddString(LocalizeString(IDS_PLAYABLE_DG));
        combo->SetValue(index, APPlayableDG);
        if (player == APPlayableDG) sel = index;
        // Playable as commander, pilot / driver and gunner
        if (air)
          index = combo->AddString(LocalizeString(IDS_PLAYABLE_CPG));
        else
          index = combo->AddString(LocalizeString(IDS_PLAYABLE_CDG));
        combo->SetValue(index, APPlayableCDG);  // all available
        if (player == APPlayableCDG) sel = index;
      }
    }
    else
    {
      combo->SetValue(index, APPlayableCDG);  // all available
      if (player == APPlayableCDG) sel = index;
    }
  }
  else if (gunner)
  {
    // Player as pilot / driver
    if (driver)
    {
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYER_PILOT));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYER_DRIVER));
      combo->SetValue(index, APPlayerDriver);
      if (player == APPlayerDriver) sel = index;
    }
    // Player as gunner
    index = combo->AddString(LocalizeString(IDS_PLAYER_GUNNER));
    combo->SetValue(index, APPlayerGunner);
    if (player == APPlayerGunner) sel = index;
    if (driver)
    {
      // Playable as pilot / driver
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_P));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_D));
      combo->SetValue(index, APPlayableD);
      if (player == APPlayableD) sel = index;
    }
    // Playable as gunner
    index = combo->AddString(LocalizeString(IDS_PLAYABLE_G));
    combo->SetValue(index, APPlayableG);
    if (player == APPlayableG) sel = index;
    // Playable as pilot / driver and gunner
    if (driver)
    {
      if (air)
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_PG));
      else
        index = combo->AddString(LocalizeString(IDS_PLAYABLE_DG));
      combo->SetValue(index, APPlayableCDG);  // all available
      if (player == APPlayableCDG) sel = index;
    }
  }
  else
  {
    // Player
    index = combo->AddString(LocalizeString(IDS_PLAYER));
    combo->SetValue(index, APPlayerCommander);
    if (player == APPlayerCommander) sel = index;
    // Playable
    index = combo->AddString(LocalizeString(IDS_PLAYABLE));
    combo->SetValue(index, APPlayableCDG);  // all available
    if (player == APPlayableCDG) sel = index;
  }

  combo->SetCurSel(sel);
}

Control *DisplayArcadeModules::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_ARCUNIT_TITLE:
    {
      CStatic *text = new CStatic(this, idc, cls);
      if (_index < 0)
        text->SetText(LocalizeString(IDS_ARCUNIT_TITLE2));
      else
        text->SetText(LocalizeString(IDS_ARCUNIT_TITLE4));
      return text;
    }
  case IDC_ARCUNIT_AZIMUT_PICTURE:
    return new CStaticAzimut(this, idc, cls, IDC_ARCUNIT_AZIMUT);
  case IDC_ARCUNIT_SIDE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int index, sel = 0;

      index = combo->AddString(LocalizeString(IDS_LOGIC));
      combo->SetValue(index, TLogic);
      if (_unit.side == TLogic) sel = index;

      combo->SetCurSel(sel);
      if (_index >= 0)
      {
        // cannot change side when edited
        combo->EnableCtrl(false);
      }
      combo->ShowCtrl(false);
      return combo;
    }
  case IDC_ARCUNIT_FACTION:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      UpdateFactions(combo, _unit.vehicle);
      if (_unit.side != TWest && _unit.side != TEast && _unit.side != TGuerrila && _unit.side != TCivilian )
      {
        combo->SetCurSel(0);
        combo->ShowCtrl(false);
      }
      return combo;
    }
  case IDC_ARCUNIT_CLASS:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      UpdateClasses(combo, _unit.vehicle);
      combo->ShowCtrl(false);
      return combo;
    }
  case IDC_ARCUNIT_VEHICLE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      UpdateVehicles(combo, _unit.vehicle);
      return combo;
    }
  case IDC_ARCUNIT_TEXT:
    {
      CEdit *text = new CEdit(this, idc, cls);
      text->SetText(_unit.name);
      return text;
    }
  case IDC_ARCUNIT_INIT:
    {
      CEdit *text = new CEdit(this, idc, cls);
      text->SetText(_unit.init);
      return text;
    }
  case IDC_ARCUNIT_DESC:
    {
      CEdit *text = new CEdit(this, idc, cls);
      text->SetText(_unit.description);
      return text;
    }
  case IDC_ARCUNIT_RANK:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      for (int i=RankPrivate; i<=RankColonel; i++)
      {
        RString rank = LocalizeString(IDS_PRIVATE + i);
        combo->AddString(rank);
      }
      combo->SetCurSel(_unit.rank - RankPrivate);
      if (_unit.side == TEmpty || _unit.side == TLogic)
      {
        combo->SetCurSel(0);
        combo->ShowCtrl(false);
      }
      return combo;
    }
  case IDC_ARCUNIT_AZIMUT:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _unit.azimut);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCUNIT_SPECIAL:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      combo->AddString(LocalizeString(IDS_SPECIAL_NONE));
      combo->AddString(LocalizeString(IDS_SPECIAL_CARGO));
      combo->AddString(LocalizeString(IDS_SPECIAL_FLYING));
      combo->AddString(LocalizeString(IDS_SPECIAL_FORM));
      combo->SetCurSel(_unit.special);
      return combo;
    }
  case IDC_ARCUNIT_CTRL:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      UpdatePlayer(combo, _unit.player);
      return combo;
    }
  case IDC_ARCUNIT_AGE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      for (int i=0; i<AAN; i++)
        combo->AddString(LocalizeString(IDS_AGE_ACTUAL + i));
      combo->SetCurSel(_unit.age);
      return combo;
    }
  case IDC_ARCUNIT_SKILL:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0.2, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_unit.skill);
      }
      return ctrl;
    }
  case IDC_ARCUNIT_HEALTH:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_unit.health);
      }
      return ctrl;
    }
  case IDC_ARCUNIT_FUEL:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_unit.fuel);
      }
      return ctrl;
    }
  case IDC_ARCUNIT_AMMO:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_unit.ammo);
      }
      return ctrl;
    }
  case IDC_ARCUNIT_PRESENCE:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_unit.presence);
      }
      return ctrl;
    }
  case IDC_ARCUNIT_PRESENCE_COND:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_unit.presenceCondition);
      return edit;
    }
  case IDC_ARCUNIT_PLACE:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _unit.placement);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCUNIT_LOCK:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int sel = 1; // default
      int index = combo->AddString(LocalizeString(IDS_VEHICLE_UNLOCKED));
      combo->SetValue(index, LSUnlocked);
      if (_unit.lock == LSUnlocked) sel = index;
      index = combo->AddString(LocalizeString(IDS_VEHICLE_DEFAULT));
      combo->SetValue(index, LSDefault);
      if (_unit.lock == LSDefault) sel = index;
      index = combo->AddString(LocalizeString(IDS_VEHICLE_LOCKED));
      combo->SetValue(index, LSLocked);
      if (_unit.lock == LSLocked) sel = index;
      combo->SetCurSel(sel);
      return combo;
    }
  default:
    return Display::OnCreateCtrl(type, idc, cls);
  }
}

bool DisplayArcadeModules::CanDestroy()
{
  if (!Display::CanDestroy()) return false;

  if (_exit == IDC_OK)
  {
    CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_TEXT));
    if (edit)
    {
      RString text = edit->GetText();
      if (text.GetLength() > 0)
      {
        if (!GWorld->GetGameState()->IdtfGoodName(text))
        {
          CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_NAME));
          return false;
        }
        // check if text is not used
        for (int i=0; i<_template->groups.Size(); i++)
        {
          ArcadeGroupInfo &gInfo = _template->groups[i];
          for (int j=0; j<gInfo.units.Size(); j++)
          {
            if (i == _indexGroup && j == _index) continue;
            if (stricmp(text, gInfo.units[j].name) == 0)
            {
              CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
              return false;
            }
          }
          for (int j=0; j<gInfo.sensors.Size(); j++)
          {
            if (stricmp(text, gInfo.sensors[j].name) == 0)
            {
              CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
              return false;
            }
          }
        }
        for (int j=0; j<_template->emptyVehicles.Size(); j++)
        {
          if (_indexGroup == -1 && j == _index) continue;
          if (stricmp(text, _template->emptyVehicles[j].name) == 0)
          {
            CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
            return false;
          }
        }
        for (int j=0; j<_template->sensors.Size(); j++)
        {
          if (stricmp(text, _template->sensors[j].name) == 0)
          {
            CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
            return false;
          }
        }
      }
    }

    GameState *gstate = GWorld->GetGameState();

    edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_INIT));
    if (edit)
    {
      RString exp = edit->GetText();
      if (!gstate->CheckExecute(exp, GWorld->GetMissionNamespace())) // mission namespace
      {
        FocusCtrl(IDC_ARCUNIT_INIT);
        CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
        edit->SetCaretPos(gstate->GetLastErrorPos(exp));
        return false;
      }
    }

    edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_PRESENCE_COND));
    if (edit)
    {
      RString exp = edit->GetText();
      if (!gstate->CheckEvaluateBool(exp, GWorld->GetMissionNamespace())) // mission namespace
      {
        FocusCtrl(IDC_ARCUNIT_PRESENCE_COND);
        CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
        edit->SetCaretPos(gstate->GetLastErrorPos(exp));
        return false;
      }
    }

    CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
    if (combo)
    {
      if (combo->GetCurSel() < 0)
      {
        CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_NO_VEH_SELECT));
        return false;
      }
    }
  }
  return true;
}

void DisplayArcadeModules::Destroy()
{
  Display::Destroy();

  if (_exit != IDC_OK) return;

  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (combo)
    _unit.side = (TargetSide)combo->GetValue(combo->GetCurSel());
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_CTRL));
  if (combo)
  {
    _unit.player = (ArcadeUnitPlayer)combo->GetValue(combo->GetCurSel());
    if (_unit.side == TEmpty || _unit.side == TLogic) _unit.player = APNonplayable;
  }
  CSliderContainer *slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_ARCUNIT_HEALTH));
  if (slider)
    _unit.health = slider->GetThumbPos();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_ARCUNIT_FUEL));
  if (slider)
    _unit.fuel = slider->GetThumbPos();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_ARCUNIT_AMMO));
  if (slider)
    _unit.ammo = slider->GetThumbPos();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_ARCUNIT_SKILL));
  if (slider)
    _unit.skill = slider->GetThumbPos();
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_VEHICLE));
  if (combo)
  {
    int indexVehicle = combo->GetCurSel();
    Assert(indexVehicle >= 0);
    _unit.vehicle = combo->GetData(indexVehicle);
  }
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_RANK));
  if (combo)
  {
    _unit.rank = (Rank)(RankPrivate + combo->GetCurSel());
    if (_unit.side == TEmpty || _unit.side == TLogic) _unit.rank = RankPrivate;
  }
  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_AZIMUT));
  if (edit)
    _unit.azimut = edit->GetText() ? atof(edit->GetText()) : 0;
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SPECIAL));
  if (combo)
    _unit.special = (ArcadeUnitSpecial)combo->GetCurSel();
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_AGE));
  if (combo)
    _unit.age = (ArcadeUnitAge)combo->GetCurSel();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_ARCUNIT_PRESENCE));
  if (slider)
    _unit.presence = slider->GetThumbPos();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_PRESENCE_COND));
  if (edit)
    _unit.presenceCondition = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_PLACE));
  if (edit)
    _unit.placement = edit->GetText() ? atof(edit->GetText()) : 0;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_TEXT));
  if (edit)
    _unit.name = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_INIT));
  if (edit)
    _unit.init = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCUNIT_DESC));
  if (edit)
    _unit.description = edit->GetText();
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_LOCK));
  if (combo)
  {
    int sel = combo->GetCurSel();
    if (sel >= 0) _unit.lock = (LockState)combo->GetValue(sel);
  }
}

DisplayArcadeGroup::DisplayArcadeGroup
(
  ControlsContainer *parent, Vector3Par position,
  RString side,RString faction,RString type, RString name,
  float azimut
)
  : Display(parent)
{
  _enableSimulation = false;
  _enableDisplay = false;

  _position = position;
  _azimut = azimut;

  Load("RscDisplayArcadeGroup");

  UpdateFactions();
  UpdateTypes();
  UpdateNames();

  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_SIDE));
  if (combo) for (int i=0; i<combo->GetSize(); i++)
  {
    if (combo->GetData(i) == side)
    {
      combo->SetCurSel(i); break;
    }
  }

  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_FACTION));
  if (combo) for (int i=0; i<combo->GetSize(); i++)
  {
    if (combo->GetData(i) == faction)
    {
      combo->SetCurSel(i); break;
    }
  }
  
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_TYPE));
  if (combo) for (int i=0; i<combo->GetSize(); i++)
  {
    if (combo->GetData(i) == type)
    {
      combo->SetCurSel(i); break;
    }
  }

  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_NAME));
  if (combo) for (int i=0; i<combo->GetSize(); i++)
  {
    if (combo->GetData(i) == name)
    {
      combo->SetCurSel(i); break;
    }
  }
}

Control *DisplayArcadeGroup::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_ARCGRP_SIDE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      ParamEntryVal groups = Pars >> "CfgGroups";
      for (int i=0; i<groups.GetEntryCount(); i++)
      {
        ParamEntryVal entry = groups.GetEntry(i);
        int index = combo->AddString(entry >> "name");
        combo->SetData(index, entry.GetName());
      }
      combo->SetCurSel(0);
      return combo;
    }
  case IDC_ARCGRP_AZIMUT:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _azimut);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCGRP_AZIMUT_PICTURE:
    return new CStaticAzimut(this, idc, cls, IDC_ARCGRP_AZIMUT);
  }
  return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayArcadeGroup::OnLBSelChanged(IControl *ctrl, int curSel)
{
  switch (ctrl->IDC())
  {
  case IDC_ARCGRP_SIDE:
    UpdateFactions();
    UpdateTypes();
    UpdateNames();
    break;
  case IDC_ARCGRP_FACTION:
    UpdateTypes();
    UpdateNames();
    break;
  case IDC_ARCGRP_TYPE:
    UpdateNames();
    break;
  }
}

//fill combobox with possible factions in given side
void DisplayArcadeGroup::UpdateFactions()
{
  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_SIDE));
  if (!combo) return;
  int i = combo->GetCurSel();
  if (i < 0) return;
  RString side = combo->GetData(i);

  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_FACTION));
  if (!combo) return;
  combo->ClearStrings();
  ParamEntryVal types = Pars >> "CfgGroups" >> side;
  for (int i=0; i<types.GetEntryCount(); i++)
  {
    ParamEntryVal entry = types.GetEntry(i);
    if (!entry.IsClass()) continue;
    int index = combo->AddString(entry >> "name");
    combo->SetData(index, entry.GetName());
  }
  combo->SetCurSel(0);
}

void DisplayArcadeGroup::UpdateTypes()
{
  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_SIDE));
  if (!combo) return;
  int i = combo->GetCurSel();
  if (i < 0) return;
  RString side = combo->GetData(i);

  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_FACTION));
  if (!combo) return;
  i = combo->GetCurSel();
  if (i < 0) return;
  RString faction = combo->GetData(i);

  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_TYPE));
  if (!combo) return;
  combo->ClearStrings();
  ParamEntryVal types = Pars >> "CfgGroups" >> side >> faction;
  for (int i=0; i<types.GetEntryCount(); i++)
  {
    ParamEntryVal entry = types.GetEntry(i);
    if (!entry.IsClass()) continue;
    int index = combo->AddString(entry >> "name");
    combo->SetData(index, entry.GetName());
  }
  combo->SetCurSel(0);
}

void DisplayArcadeGroup::UpdateNames()
{
  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_SIDE));
  if (!combo) return;
  int i = combo->GetCurSel();
  if (i < 0) return;
  RString side = combo->GetData(i);

  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_FACTION));
  if (!combo) return;
  i = combo->GetCurSel();
  if (i < 0) return;
  RString faction = combo->GetData(i);

  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_TYPE));
  if (!combo) return;
  i = combo->GetCurSel();
  if (i < 0) return;
  RString type = combo->GetData(i);

  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCGRP_NAME));
  if (!combo) return;
  combo->ClearStrings();
  ParamEntryVal names = Pars >> "CfgGroups" >> side >> faction >> type;
  for (int i=0; i<names.GetEntryCount(); i++)
  {
    ParamEntryVal entry = names.GetEntry(i);
    if (!entry.IsClass()) continue;
    int index = combo->AddString(entry >> "name");
    combo->SetData(index, entry.GetName());
  }
  combo->SetCurSel(0);
}

Control *DisplayArcadeSensor::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_ARCSENS_TITLE:
    {
      CStatic *text = new CStatic(this, idc, cls);
      if (_index < 0)
        text->SetText(LocalizeString(IDS_ARCSENS_TITLE1));
      else
        text->SetText(LocalizeString(IDS_ARCSENS_TITLE2));
      return text;
    }
  case IDC_ARCSENS_A:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _sensor.a);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCSENS_B:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _sensor.b);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCSENS_ANGLE:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _sensor.angle);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCSENS_RECT:
    {
      CToolBox *toolbox = new CToolBox(this, idc, cls);
      if (_sensor.rectangular)
        toolbox->SetCurSel(1);
      else
        toolbox->SetCurSel(0);
      return toolbox;
    }
  case IDC_ARCSENS_ACTIV:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      if (_ig >= 0)
      {
        combo->AddString(LocalizeString(IDS_SENSORACTIV_GROUP));
        combo->SetCurSel(0);
      }
      else if (_sensor.idVisitorObj >= 0)
      {
        combo->AddString(LocalizeString(IDS_SENSORACTIV_STATIC));
        combo->SetCurSel(0);
      }
      else if (_sensor.idVehicle >= 0)
      {
        int ig;
        int index;
        if (!_t->FindUnit(_sensor.idVehicle, ig, index))
        {
          // vehicle doesn't exist
          _sensor.idVehicle = -1;
          goto NoVehicle;
        }
        else if (ig >= 0)
        {
          // vehicle in group
          combo->AddString(LocalizeString(IDS_SENSORACTIV_VEHICLE));
          combo->AddString(LocalizeString(IDS_SENSORACTIV_GROUP));
          combo->AddString(LocalizeString(IDS_SENSORACTIV_LEADER));
          combo->AddString(LocalizeString(IDS_SENSORACTIV_MEMBER));
          int sel = _sensor.activationBy - ASAVehicle;
          saturate(sel, 0, combo->GetSize() - 1);
          combo->SetCurSel(sel);
        }
        else
        {
          // empty vehicle
          combo->AddString(LocalizeString(IDS_SENSORACTIV_VEHICLE));
          combo->SetCurSel(0);
        }
      }
      else
      {
NoVehicle:
        combo->AddString(LocalizeString(IDS_SENSORACTIV_NONE));
        combo->AddString(LocalizeString(IDS_EAST));
        combo->AddString(LocalizeString(IDS_WEST));
        combo->AddString(LocalizeString(IDS_GUERRILA));
        combo->AddString(LocalizeString(IDS_CIVILIAN));
        combo->AddString(LocalizeString(IDS_LOGIC));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_ANYBODY));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_ALPHA));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_BRAVO));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_CHARLIE));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_DELTA));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_ECHO));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_FOXTROT));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_GOLF));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_HOTEL));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_INDIA));
        combo->AddString(LocalizeString(IDS_SENSORACTIV_JULIET));
        combo->AddString(LocalizeString(IDS_EAST_SEIZED));
        combo->AddString(LocalizeString(IDS_WEST_SEIZED));
        combo->AddString(LocalizeString(IDS_GUERRILA_SEIZED));
        if (_sensor.activationBy > ASAGuerrilaSeized) _sensor.activationBy = ASANone;
        combo->SetCurSel(_sensor.activationBy);
      }
      return combo;
    }
  case IDC_ARCSENS_PRESENCE:
    {
      CToolBox *toolbox = new CToolBox(this, idc, cls);
      toolbox->SetCurSel(_sensor.activationType);
      return toolbox;
    }
  case IDC_ARCSENS_REPEATING:
    {
      CToolBox *toolbox = new CToolBox(this, idc, cls);
      if (_sensor.repeating)
        toolbox->SetCurSel(1);
      else
        toolbox->SetCurSel(0);
      return toolbox;
    }
  case IDC_ARCSENS_INTERRUPT:
    {
      CToolBox *toolbox = new CToolBox(this, idc, cls);
      if (_sensor.interruptable)
        toolbox->SetCurSel(1);
      else
        toolbox->SetCurSel(0);
      return toolbox;
    }
  case IDC_ARCSENS_TIMEOUT_MIN:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _sensor.timeoutMin);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCSENS_TIMEOUT_MID:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _sensor.timeoutMid);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCSENS_TIMEOUT_MAX:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _sensor.timeoutMax);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCSENS_TYPE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      for (int i=0; i<ASTN; i++)
      {
        combo->AddString(LocalizeString(IDS_SENSORTYPE_NONE + i));
      }
      combo->SetCurSel(_sensor.type);
      return combo;
    }
  case IDC_ARCSENS_OBJECT:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      ParamEntryVal objects = Pars >> "CfgDetectors" >> "objects";
      int sel = 0;
      for (int i=0; i<objects.GetSize(); i++)
      {
        RString objectName = objects[i];
        if (stricmp(objectName, _sensor.object) == 0) sel = i;
        ParamEntryVal objectCls = Pars >> "CfgNonAIVehicles" >> objectName;
        RString displayName = objectCls >> "displayName";
        int index = combo->AddString(displayName);
        combo->SetData(index, objectName);
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCSENS_TEXT:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_sensor.text);
      return edit;
    }
  case IDC_ARCSENS_NAME:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_sensor.name);
      return edit;
    }
  case IDC_ARCSENS_EXPCOND:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_sensor.expCond);
      return edit;
    }
  case IDC_ARCSENS_EXPACTIV:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_sensor.expActiv);
      return edit;
    }
  case IDC_ARCSENS_EXPDESACTIV:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_sensor.expDesactiv);
      return edit;
    }
  case IDC_ARCSENS_AGE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      for (int i=0; i<AAN; i++)
        combo->AddString(LocalizeString(IDS_AGE_ACTUAL + i));
      combo->SetCurSel(_sensor.age);
      return combo;
    }
  }

  return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayArcadeSensor::OnButtonClicked(int idc)
{
  if (idc == IDC_ARCSENS_EFFECTS)
  {
    CreateChild(new DisplayArcadeEffects(this, _sensor.effects));
  }
  else
  {
    Display::OnButtonClicked(idc);
  }
}

void DisplayArcadeSensor::OnChildDestroyed(int idd, int exit)
{
  if (idd == IDD_ARCADE_EFFECTS && exit == IDC_OK)
  {
    DisplayArcadeEffects *display = dynamic_cast<DisplayArcadeEffects *>((ControlsContainer *)_child);
    Assert(display);
    _sensor.effects = display->_effects;
  }
  Display::OnChildDestroyed(idd, exit);
}

bool DisplayArcadeSensor::CanDestroy()
{
  if (_exit != IDC_OK) return true;

  GameState *gstate = GWorld->GetGameState();

  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPCOND));
  if (edit)
  {
    RString exp = edit->GetText();
    if (!gstate->CheckEvaluateBool(exp, GWorld->GetMissionNamespace())) // mission namespace
    {
      FocusCtrl(IDC_ARCSENS_EXPCOND);
      CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
      edit->SetCaretPos(gstate->GetLastErrorPos(exp));
      return false;
    }
  }
  
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPACTIV));
  if (edit)
  {
    RString exp = edit->GetText();
    if (!gstate->CheckExecute(exp, GWorld->GetMissionNamespace())) // mission namespace
    {
      FocusCtrl(IDC_ARCSENS_EXPACTIV);
      CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
      edit->SetCaretPos(gstate->GetLastErrorPos(exp));
      return false;
    }
  }
  
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPDESACTIV));
  if (edit)
  {
    RString exp = edit->GetText();
    if (!gstate->CheckExecute(exp, GWorld->GetMissionNamespace())) // mission namespace
    {
      FocusCtrl(IDC_ARCSENS_EXPDESACTIV);
      CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
      edit->SetCaretPos(gstate->GetLastErrorPos(exp));
      return false;
    }
  }

  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_NAME));
  if (edit)
  {
    RString text = edit->GetText();
    if (text.GetLength() > 0)
    {
      if (!GWorld->GetGameState()->IdtfGoodName(text))
      {
        CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_NAME));
        return false;
      }
      // check if text is not used
      for (int i=0; i<_t->groups.Size(); i++)
      {
        ArcadeGroupInfo &gInfo = _t->groups[i];
        for (int j=0; j<gInfo.units.Size(); j++)
        {
          if (stricmp(text, gInfo.units[j].name) == 0)
          {
            CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
            return false;
          }
        }
        for (int j=0; j<gInfo.sensors.Size(); j++)
        {
          if (i == _ig && j == _index) continue;
          if (stricmp(text, gInfo.sensors[j].name) == 0)
          {
            CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
            return false;
          }
        }
      }
      for (int j=0; j<_t->emptyVehicles.Size(); j++)
      {
        if (stricmp(text, _t->emptyVehicles[j].name) == 0)
        {
          CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
          return false;
        }
      }
      for (int j=0; j<_t->sensors.Size(); j++)
      {
        if (_ig == -1 && j == _index) continue;
        if (stricmp(text, _t->sensors[j].name) == 0)
        {
          CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_VEH_TEXT_USED));
          return false;
        }
      }
    }
  }

  return true;
}

void DisplayArcadeSensor::Destroy()
{
  Display::Destroy();

  if (_exit != IDC_OK) return;

  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_A));
  if (edit)
    _sensor.a = edit->GetText() ? atof(edit->GetText()) : 0;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_B));
  if (edit)
    _sensor.b = edit->GetText() ? atof(edit->GetText()) : 0;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_ANGLE));
  if (edit)
    _sensor.angle = edit->GetText() ? atof(edit->GetText()) : 0;
  CToolBox *toolbox = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCSENS_RECT));
  if (toolbox)
    _sensor.rectangular = toolbox->GetCurSel() == 1;

  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCSENS_ACTIV));
  if (combo)
  {
    if (_ig >= 0)
    {
      _sensor.activationBy = ASAGroup;
    }
    else if (_sensor.idVisitorObj >= 0)
    {
      _sensor.activationBy = ASAStatic;
    }
    else if (_sensor.idVehicle >= 0)
    {
      _sensor.activationBy = (ArcadeSensorActivation)(ASAVehicle + combo->GetCurSel());
    }
    else
    {
      _sensor.activationBy = (ArcadeSensorActivation)(combo->GetCurSel());
    }
  }
  toolbox = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCSENS_PRESENCE));
  if (toolbox)
    _sensor.activationType = (ArcadeSensorActivationType)(toolbox->GetCurSel());
  toolbox = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCSENS_REPEATING));
  if (toolbox)
    _sensor.repeating = toolbox->GetCurSel() == 1;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_TIMEOUT_MIN));
  if (edit)
    _sensor.timeoutMin = edit->GetText() ? atof(edit->GetText()) : 0;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_TIMEOUT_MID));
  if (edit)
    _sensor.timeoutMid = edit->GetText() ? atof(edit->GetText()) : 0;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_TIMEOUT_MAX));
  if (edit)
    _sensor.timeoutMax = edit->GetText() ? atof(edit->GetText()) : 0;
  toolbox = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCSENS_INTERRUPT));
  if (toolbox)
    _sensor.interruptable = toolbox->GetCurSel() == 1;
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCSENS_TYPE));
  if (combo)
    _sensor.type = (ArcadeSensorType)(combo->GetCurSel());
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCSENS_OBJECT));
  if (combo)
    _sensor.object = combo->GetData(combo->GetCurSel());
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_TEXT));
  if (edit)
    _sensor.text = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_NAME));
  if (edit)
    _sensor.name = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPCOND));
  if (edit)
    _sensor.expCond = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPACTIV));
  if (edit)
    _sensor.expActiv = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCSENS_EXPDESACTIV));
  if (edit)
    _sensor.expDesactiv = edit->GetText();
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCSENS_AGE));
  if (combo)
    _sensor.age = (ArcadeUnitAge)(combo->GetCurSel());
}

Control *DisplayArcadeMarker::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_ARCMARK_TITLE:
    {
      CStatic *text = new CStatic(this, idc, cls);
      if (_index < 0)
        text->SetText(LocalizeString(IDS_ARCMARK_TITLE1));
      else
        text->SetText(LocalizeString(IDS_ARCMARK_TITLE2));
      return text;
    }
  case IDC_ARCMARK_NAME:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_marker.name);
      return edit;
    }
  case IDC_ARCMARK_TEXT:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_marker.text);
      return edit;
    }
  case IDC_ARCMARK_MARKER:
    {
      CToolBox *ctrl = new CToolBox(this, idc, cls);
      ctrl->SetCurSel(_marker.markerType);
      return ctrl;
    }
  case IDC_ARCMARK_TYPE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      ParamEntryVal cls = Pars >> "CfgMarkers";
      int n = cls.GetEntryCount();
      int sel = 0;
      for (int i=0; i<n; i++)
      {
        ParamEntryVal entry = cls.GetEntry(i);
        int scope = entry >> "scope";
        if(scope>=1)
        {
          int index = combo->AddString(entry >> "name");
          combo->SetData(index, entry.GetName());
          if (stricmp(_marker.type, entry.GetName()) == 0) sel = index;
        }
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCMARK_COLOR:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      ParamEntryVal cls = Pars >> "CfgMarkerColors";
      int n = cls.GetEntryCount();
      int sel = 0;
      for (int i=0; i<n; i++)
      {
        ParamEntryVal entry = cls.GetEntry(i);
        int scope = entry >> "scope";
        if(scope==2)
        {
          int index = combo->AddString(entry >> "name");
          combo->SetData(index, entry.GetName());
          if (stricmp(_marker.colorName, entry.GetName()) == 0) sel = index;
        }
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCMARK_FILL:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      ParamEntryVal cls = Pars >> "CfgMarkerBrushes";
      int n = cls.GetEntryCount();
      int sel = 0;
      for (int i=0; i<n; i++)
      {
        ParamEntryVal entry = cls.GetEntry(i);
        int scope = entry >> "scope";
        if(scope==2)
        {   
          int index = combo->AddString(entry >> "name");
          combo->SetData(index, entry.GetName());
          if (stricmp(_marker.fillName, entry.GetName()) == 0) sel = index;
        }
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCMARK_A:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _marker.a);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCMARK_B:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _marker.b);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCMARK_ANGLE:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _marker.angle);
      edit->SetText(buffer);
      return edit;
    }
  }

  return Display::OnCreateCtrl(type, idc, cls);
}

bool DisplayArcadeMarker::CanDestroy()
{
  if (!Display::CanDestroy()) return false;

  if (_exit == IDC_OK) 
  {
    CEdit *edit = check_cast<CEdit *>(GetCtrl(IDC_ARCMARK_NAME));
    if (edit)
    {
      RString name = edit->GetText();
      if (name.GetLength() == 0)
      {
        CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MARKER_EMPTY));
        return false;
      }
      int n = _template->markers.Size();
      for (int i=0; i<n; i++)
      {
        if (i == _index) continue;
        if (stricmp(name, _template->markers[i].name) == 0)
        {
          CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_MARKER_EXIST));
          return false;
        }
      }
    }
  }

  return true;
}

void DisplayArcadeMarker::Destroy()
{
  Display::Destroy();

  if (_exit != IDC_OK) return;

  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_NAME));
  if (edit)
    _marker.name = edit->GetText();

  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_TEXT));
  if (edit)
    _marker.text = edit->GetText();

  float defSize = 1;

  CToolBox *ctrl = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCMARK_MARKER));
  if (ctrl)
  {
    _marker.markerType = (MarkerType)ctrl->GetCurSel();
    if (_marker.markerType != MTIcon) defSize = 20;
  }

  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCMARK_TYPE));
  if (combo)
  {
    _marker.type = combo->GetData(combo->GetCurSel());
    _marker.OnTypeChanged();
  }

  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCMARK_COLOR));
  if (combo)
  {
    _marker.colorName = combo->GetData(combo->GetCurSel());
    _marker.OnColorChanged();
  }

  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCMARK_FILL));
  if (combo)
  {
    _marker.fillName = combo->GetData(combo->GetCurSel());
    _marker.OnFillChanged();
  }

  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_A));
  if (edit)
    _marker.a = edit->GetText() ? atof(edit->GetText()) : defSize;
  
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_B));
  if (edit)
    _marker.b = edit->GetText() ? atof(edit->GetText()) : defSize;

  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_ANGLE));
  if (edit)
    _marker.angle = edit->GetText() ? atof(edit->GetText()) : 0;
}

void DisplayArcadeMarker::OnToolBoxSelChanged(int idc, int curSel)
{
  if (idc == IDC_ARCMARK_MARKER)
    ChangeType(curSel);
  else
    Display::OnToolBoxSelChanged(idc, curSel);
}

void DisplayArcadeMarker::ChangeType(int curSel)
{
  switch (curSel)
  {
  case MTIcon:
    {
      IControl *ctrl = GetCtrl(IDC_ARCMARK_TYPE);
      if (ctrl) ctrl->ShowCtrl(true);
      ctrl = GetCtrl(IDC_ARCMARK_FILL);
      if (ctrl) ctrl->ShowCtrl(false);
      CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_ARCMARK_TYPE_TEXT));
      if (text) text->SetText(LocalizeString(IDS_ARCMARK_TYPE1));
      if (_oldType != MTIcon)
      {
        CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_A));
        if (edit)
        {
          float value = edit->GetText() ? atof(edit->GetText()) : 1;
          char buffer[256];
          sprintf(buffer, "%g", value / 20.0f);
          edit->SetText(buffer);
        }
        edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_B));
        if (edit)
        {
          float value = edit->GetText() ? atof(edit->GetText()) : 1;
          char buffer[256];
          sprintf(buffer, "%g", value / 20.0f);
          edit->SetText(buffer);
        }
      }
    }
    break;
  case MTRectangle:
  case MTEllipse:
    {
      IControl *ctrl = GetCtrl(IDC_ARCMARK_TYPE);
      if (ctrl) ctrl->ShowCtrl(false);
      ctrl = GetCtrl(IDC_ARCMARK_FILL);
      if (ctrl) ctrl->ShowCtrl(true);
      CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_ARCMARK_TYPE_TEXT));
      if (text) text->SetText(LocalizeString(IDS_ARCMARK_TYPE2));
      if (_oldType == MTIcon)
      {
        CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_A));
        if (edit)
        {
          float value = edit->GetText() ? atof(edit->GetText()) : 1;
          char buffer[256];
          sprintf(buffer, "%g", value * 20.0f);
          edit->SetText(buffer);
        }
        edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCMARK_B));
        if (edit)
        {
          float value = edit->GetText() ? atof(edit->GetText()) : 1;
          char buffer[256];
          sprintf(buffer, "%g", value * 20.0f);
          edit->SetText(buffer);
        }
      }
    }
    break;
  default:
    Fail("Bad marker type");
    break;
  }
  _oldType = curSel;
}

DisplayArcadeEffects::DisplayArcadeEffects(ControlsContainer *parent, ArcadeEffects effects)
  : Display(parent)
{
  _enableSimulation = false;
  _enableDisplay = false;
  _effects = effects;
  Load("RscDisplayArcadeEffects");
  ChangeTitleType(_effects.titleType);
}

/*!
\patch 1.56 Date 5/15/2002 by Ondra
- Changed: CfgMusic no longer requires "tracks" list. This makes music addons easier to use.
*/
/*!
\patch 1.61 Date 5/27/2002 by Ondra
- Changed: CfgSounds, RscTitles and CfgSFX no longer require array listing class names.
This enables addons to add new entries that can be used in mission editor.
*/

Control *DisplayArcadeEffects::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
/*
  case IDC_ARCEFF_PLAYERONLY:
    {
      CToolBox *toolbox = new CToolBox(this, idc, cls);
      if (_effects.playerOnly)
        toolbox->SetCurSel(1);
      else
        toolbox->SetCurSel(0);
      return toolbox;
    }
*/
  case IDC_ARCEFF_CONDITION:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_effects.condition);
      return edit;
    }
  case IDC_ARCEFF_SOUND:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int index = combo->AddString(LocalizeString(IDS_SOUND_NONE));
      combo->SetData(index, "$NONE$");
      int sel = index;
      RString sound = _effects.sound;
      if (sound[0] == '@') sound = (const char *)sound + 1;
      ConstParamEntryPtr cls = Pars.FindEntry("CfgSounds");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      cls = ExtParsCampaign.FindEntry("CfgSounds");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      cls = ExtParsMission.FindEntry("CfgSounds");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCEFF_VOICE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int index = combo->AddString(LocalizeString(IDS_SOUND_NONE));
      combo->SetData(index, "");
      int sel = index;
      RString sound = _effects.voice;
      if (sound[0] == '@') sound = (const char *)sound + 1;
      ConstParamEntryPtr cls = Pars.FindEntry("CfgSounds");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      cls = ExtParsCampaign.FindEntry("CfgSounds");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      cls = ExtParsMission.FindEntry("CfgSounds");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCEFF_SOUND_ENV:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int index = combo->AddString(LocalizeString(IDS_SOUND_NONE));
      combo->SetData(index, "");
      int sel = index;
      RString sound = _effects.soundEnv;
      if (sound[0] == '@') sound = (const char *)sound + 1;
      ConstParamEntryPtr cls = Pars.FindEntry("CfgEnvSounds");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      cls = ExtParsCampaign.FindEntry("CfgEnvSounds");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      cls = ExtParsMission.FindEntry("CfgEnvSounds");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCEFF_SOUND_DET:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int index = combo->AddString(LocalizeString(IDS_SOUND_NONE));
      combo->SetData(index, "");
      int sel = index;
      RString sound = _effects.soundDet;
      if (sound[0] == '@') sound = (const char *)sound + 1;
      ConstParamEntryPtr cls = Pars.FindEntry("CfgSFX");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      cls = ExtParsCampaign.FindEntry("CfgSFX");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      cls = ExtParsMission.FindEntry("CfgSFX");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCEFF_MUSIC:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      RString sound = _effects.track;
      int index = combo->AddString(LocalizeString(IDS_MUSIC_NONE));
      combo->SetData(index, "$NONE$");
      int sel = index;
      index = combo->AddString(LocalizeString(IDS_MUSIC_SILENCE));
      combo->SetData(index, "$STOP$");
      if (stricmp("$STOP$", sound) == 0) sel = index;
      ConstParamEntryPtr cls = Pars.FindEntry("CfgMusic");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      cls = ExtParsCampaign.FindEntry("CfgMusic");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      cls = ExtParsMission.FindEntry("CfgMusic");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), sound) == 0) sel = index;
        }
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCEFF_TITTYPE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      combo->AddString(LocalizeString(IDS_TITTYPE_NONE));
      combo->AddString(LocalizeString(IDS_TITTYPE_OBJECT));
      combo->AddString(LocalizeString(IDS_TITTYPE_RESOURCE));
      combo->AddString(LocalizeString(IDS_TITTYPE_TEXT));
      combo->SetCurSel(_effects.titleType);
      return combo;
    }
  case IDC_ARCEFF_TITEFF:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int i;
      for (i=0; i<NTitEffects; i++)
      {
        combo->AddString(LocalizeString(IDS_TITEFFECT_PLAIN + i));
      }
      combo->SetCurSel(_effects.titleEffect);
      return combo;
    }
  case IDC_ARCEFF_TITTEXT:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_effects.title ? _effects.title : "");
      return edit;
    }
  case IDC_ARCEFF_TITRES:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int sel = 0;
      ConstParamEntryPtr cls = Pars.FindEntry("RscTitles");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), _effects.title) == 0) sel = index;
        }
      }
      cls = ExtParsCampaign.FindEntry("RscTitles");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), _effects.title) == 0) sel = index;
        }
      }
      cls = ExtParsMission.FindEntry("RscTitles");
      if (cls)
      {
        int n = cls->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal ci = cls->GetEntry(i);
          if (!ci.IsClass()) continue;
          if (!ci.FindEntry("name")) continue;
          RString displayName = ci >> "name";
          int index = combo->AddString(displayName);
          combo->SetData(index, ci.GetName());
          if (stricmp(ci.GetName(), _effects.title) == 0) sel = index;
        }
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCEFF_TITOBJ:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      ParamEntryVal cls = Pars >> "CfgTitles";
      int sel = 0;
      int i, n = (cls >> "titles").GetSize();
      for (i=0; i<n; i++)
      {
        RString name = (cls >> "titles")[i];
        RString displayName = cls >> name >> "name";
        int index = combo->AddString(displayName);
        combo->SetData(index, name);
        if (stricmp(name, _effects.title) == 0) sel = index;
      }
      combo->SetCurSel(sel);
      return combo;
    }
  default:
    return Display::OnCreateCtrl(type, idc, cls);
  }
}

void DisplayArcadeEffects::OnLBSelChanged(IControl *ctrl, int curSel)
{
  if (ctrl->IDC() == IDC_ARCEFF_TITTYPE)
  {
    ChangeTitleType(curSel);
  }

  Display::OnLBSelChanged(ctrl, curSel);
}

void DisplayArcadeEffects::Destroy()
{
  Display::Destroy();

  if (_exit != IDC_OK) return;

  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCEFF_CONDITION));
  if (edit)
    _effects.condition = edit->GetText();
  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_SOUND));
  if (combo)
    _effects.sound = combo->GetData(combo->GetCurSel());
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_VOICE));
  if (combo)
    _effects.voice = combo->GetData(combo->GetCurSel());
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_SOUND_ENV));
  if (combo)
    _effects.soundEnv = combo->GetData(combo->GetCurSel());
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_SOUND_DET));
  if (combo)
    _effects.soundDet = combo->GetData(combo->GetCurSel());
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_MUSIC));
  if (combo)
    _effects.track = combo->GetData(combo->GetCurSel());
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_TITTYPE));
  if (combo)
    _effects.titleType = (TitleType)combo->GetCurSel();
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_TITEFF));
  if (combo)
    _effects.titleEffect = (TitEffectName)combo->GetCurSel();
  _effects.title = "";
  switch (_effects.titleType)
  {
  case TitleObject:
    combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_TITOBJ));
    if (combo)
      _effects.title = combo->GetData(combo->GetCurSel());
    break;
  case TitleResource:
    combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCEFF_TITRES));
    if (combo)
      _effects.title = combo->GetData(combo->GetCurSel());
    break;
  case TitleText:
    {
      CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCEFF_TITTEXT));
      if (edit)
      {
        _effects.title = edit->GetText();
        if (_effects.title.GetLength() == 0) _effects.titleType = TitleNone;
      }
    }
    break;
  }
}

void DisplayArcadeEffects::ChangeTitleType(int type)
{
  bool showText = false;
  bool showRes = false;
  bool showObj = false;

  switch (type)
  {
  case TitleNone:
    break;
  case TitleObject:
    showObj = true;
    break;
  case TitleResource:
    showRes = true;
    break;
  case TitleText:
    showText = true;
    break;
  }

  IControl *ctrl;
  ctrl = GetCtrl(IDC_ARCEFF_TITTEXT);
  if (ctrl) ctrl->ShowCtrl(showText);
  ctrl = GetCtrl(IDC_ARCEFF_TITRES);
  if (ctrl) ctrl->ShowCtrl(showRes);
  ctrl = GetCtrl(IDC_ARCEFF_TITOBJ);
  if (ctrl) ctrl->ShowCtrl(showObj);
  ctrl = GetCtrl(IDC_ARCEFF_TEXT_TITTEXT);
  if (ctrl) ctrl->ShowCtrl(showObj || showRes || showText);
}

DisplayArcadeWaypoint::DisplayArcadeWaypoint
(
  ControlsContainer *parent, ArcadeTemplate *templ,
  int indexGroup, int index, ArcadeWaypointInfo &waypoint
)
  : Display(parent)
{
  _enableSimulation = false;
  _enableDisplay = false;

  _template = templ;
  _indexGroup = indexGroup;
  _index = index;
  _waypoint = waypoint;

  Load("RscDisplayArcadeWaypoint");

  if (!GetCtrl(IDC_ARCWP_HOUSEPOS))
  {
    if (GetCtrl(IDC_ARCWP_HOUSEPOSTEXT)) GetCtrl(IDC_ARCWP_HOUSEPOSTEXT)->ShowCtrl(false);
  }
}

Control *DisplayArcadeWaypoint::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_ARCWP_TITLE:
    {
      CStatic *text = new CStatic(this, idc, cls);
      if (_index < 0)
        text->SetText(LocalizeString(IDS_ARCWP_TITLE1));
      else
        text->SetText(LocalizeString(IDS_ARCWP_TITLE2));
      return text;
    }
  case IDC_ARCWP_TYPE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      ArcadeGroupInfo &gInfo = _template->groups[_indexGroup];
      int sel = 0;
      if (gInfo.side == TLogic)
      {
        for (int i=ACLOGIC; i<ACLOGICN; i++)
        {
          int index = combo->AddString(LocalizeString(IDS_AC_AND + i - ACAND));
          combo->SetValue(index, i);
          if (i == _waypoint.type) sel = index;
        }
      }
      else
      {
        for (int i=ACMOVE; i<ACN; i++)
        {
          int index = combo->AddString(LocalizeString(IDS_AC_MOVE + i - ACMOVE));
          combo->SetValue(index, i);
          if (i == _waypoint.type) sel = index;
        }
      }
      combo->SetCurSel(sel);
      return combo;
    }
  case IDC_ARCWP_SEMAPHORE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      combo->AddString(LocalizeString(IDS_NO_CHANGE));
      int i;
      for (i=0; i<AI::NSemaphores; i++)
      {
        combo->AddString(LocalizeString(IDS_IGNORE + i));
      }
      combo->SetCurSel(_waypoint.combatMode + 1);
      return combo;
    }
  case IDC_ARCWP_FORM:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      combo->AddString(LocalizeString(IDS_NO_CHANGE));
      int i;
      for (i=0; i<AI::NForms; i++)
      {
        combo->AddString(LocalizeString(IDS_COLUMN + i));
      }
      combo->SetCurSel(_waypoint.formation + 1);
      return combo;
    }
  case IDC_ARCWP_SPEED:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      combo->AddString(LocalizeString(IDS_SPEED_UNCHANGED));
      combo->AddString(LocalizeString(IDS_SPEED_LIMITED));
      combo->AddString(LocalizeString(IDS_SPEED_NORMAL));
      combo->AddString(LocalizeString(IDS_SPEED_FULL));
      combo->SetCurSel(_waypoint.speed);
      return combo;
    }
  case IDC_ARCWP_COMBAT:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      combo->AddString(LocalizeString(IDS_COMBAT_UNCHANGED));
      combo->AddString(LocalizeString(IDS_COMBAT_CARELESS));
      combo->AddString(LocalizeString(IDS_COMBAT_SAFE));
      combo->AddString(LocalizeString(IDS_COMBAT_AWARE));
      combo->AddString(LocalizeString(IDS_COMBAT_COMBAT));
      combo->AddString(LocalizeString(IDS_COMBAT_STEALTH));
      combo->SetCurSel(_waypoint.combat);
      return combo;
    }
  case IDC_ARCWP_SEQ:
    {
      CCombo *combo = new CCombo(this, idc, cls);

      char buffer[256];
      ArcadeGroupInfo &gInfo = _template->groups[_indexGroup];
      int i, n = gInfo.waypoints.Size();
      for (i=0; i<n; i++)
      {
        ArcadeWaypointInfo &info = gInfo.waypoints[i];
        sprintf
        (
          buffer, "%d: %s %s",
          i,
          (const char *)LocalizeString(IDS_AC_MOVE + info.type - ACMOVE),
          info.description ? (const char *)info.description : ""
        );
        combo->AddString(buffer);
      }
      sprintf
      (
        buffer, "%d:",
        n
      );
      combo->AddString(buffer);

      if (_index < 0)
        combo->SetCurSel(n);
      else
        combo->SetCurSel(_index);
      return combo;
    }
  case IDC_ARCWP_DESC:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_waypoint.description ? _waypoint.description : "");
      return edit;
    }
  case IDC_ARCWP_EXPCOND:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_waypoint._expCond);
      return edit;
    }
  case IDC_ARCWP_EXPACTIV:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_waypoint._expActiv);
      return edit;
    }
  case IDC_ARCWP_SCRIPT:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_waypoint.script);
      return edit;
    }
  case IDC_ARCWP_PLACE:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _waypoint.placement);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCWP_PREC:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _waypoint.completitionRadius);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCWP_TIMEOUT_MIN:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _waypoint.timeoutMin);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCWP_TIMEOUT_MID:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _waypoint.timeoutMid);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCWP_TIMEOUT_MAX:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      char buffer[256];
      sprintf(buffer, "%g", _waypoint.timeoutMax);
      edit->SetText(buffer);
      return edit;
    }
  case IDC_ARCWP_HOUSEPOS:
    {
      if (_waypoint.idVisitorObj < 0) return NULL;
      OLink(Object) veh = GLandscape->GetObject(_waypoint.idObject);
      if (!veh) return NULL;
      DoAssert(veh->ID()==_waypoint.idVisitorObj);
      if (!veh->GetShape()) return NULL;
      if (veh->GetShape()->FindPaths() < 0) return NULL;
      
      const IPaths *build = veh->GetIPaths();
      if (!build) return NULL;
      int n = build->NPos();
      if (n == 0) return NULL;
      CCombo *combo = new CCombo(this, idc, cls);
      for (int i=0; i<n; i++)
      {
        char buffer[256];
        sprintf(buffer, LocalizeString(IDS_HOUSE_POSITION), i + 1);
        combo->AddString(buffer);
      }
      combo->SetCurSel(_waypoint.housePos);
      return combo;
    }
  case IDC_ARCWP_SHOW:
    {
      CToolBox *toolbox = new CToolBox(this, idc, cls);
      toolbox->SetCurSel(_waypoint.showWP);
/*
      if (_waypoint.show)
        toolbox->SetCurSel(1);
      else
        toolbox->SetCurSel(0);
*/
      return toolbox;
    }
  default:
    return Display::OnCreateCtrl(type, idc, cls);
  }
}

void DisplayArcadeWaypoint::OnButtonClicked(int idc)
{
  if (idc == IDC_ARCWP_EFFECTS)
  {
    CreateChild(new DisplayArcadeEffects(this, _waypoint.effects));
  }
  else
  {
    Display::OnButtonClicked(idc);
  }
}

void DisplayArcadeWaypoint::OnChildDestroyed(int idd, int exit)
{
  if (idd == IDD_ARCADE_EFFECTS && exit == IDC_OK)
  {
    DisplayArcadeEffects *display = dynamic_cast<DisplayArcadeEffects *>((ControlsContainer *)_child);
    Assert(display);
    _waypoint.effects = display->_effects;
  }
  Display::OnChildDestroyed(idd, exit);
}

bool DisplayArcadeWaypoint::CanDestroy()
{
  if (_exit != IDC_OK) return true;

  GameState *gstate = GWorld->GetGameState();

  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_EXPCOND));
  if (edit)
  {
    RString exp = edit->GetText();
    if (!gstate->CheckEvaluateBool(exp, GWorld->GetMissionNamespace())) // mission namespace
    {
      FocusCtrl(IDC_ARCWP_EXPCOND);
      CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
      edit->SetCaretPos(gstate->GetLastErrorPos(exp));
      return false;
    }
  }

  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_EXPACTIV));
  if (edit)
  {
    RString exp = edit->GetText();
    if (!gstate->CheckExecute(exp, GWorld->GetMissionNamespace())) // mission namespace
    {
      FocusCtrl(IDC_ARCWP_EXPACTIV);
      CreateMsgBox(MB_BUTTON_OK, gstate->GetLastErrorText());
      edit->SetCaretPos(gstate->GetLastErrorPos(exp));
      return false;
    }
  }

  return true;
}

void DisplayArcadeWaypoint::Destroy()
{
  Display::Destroy();

  if (_exit != IDC_OK) return;

  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_TYPE));
  if (combo)
    _waypoint.type = (ArcadeWaypointType)combo->GetValue(combo->GetCurSel());
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_SEMAPHORE));
  if (combo)
    _waypoint.combatMode = (AI::Semaphore)(combo->GetCurSel() - 1);
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_FORM));
  if (combo)
    _waypoint.formation = (AI::Formation)(combo->GetCurSel() - 1);
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_SPEED));
  if (combo)
    _waypoint.speed = (SpeedMode)combo->GetCurSel();
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_COMBAT));
  if (combo)
    _waypoint.combat = (CombatMode)combo->GetCurSel();
  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_DESC));
  if (edit)
    _waypoint.description = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_EXPCOND));
  if (edit)
  {
    _waypoint._expCond = edit->GetText();
#if USE_PRECOMPILATION
    GGameState.CompileMultiple(_waypoint._expCond, _waypoint._compiledCond, GWorld->GetMissionNamespace()); // mission namespace
#endif
  }
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_EXPACTIV));
  if (edit)
  {
    _waypoint._expActiv = edit->GetText();
#if USE_PRECOMPILATION
    GGameState.CompileMultiple(_waypoint._expActiv, _waypoint._compiledActiv, GWorld->GetMissionNamespace()); // mission namespace
#endif
  }
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_SCRIPT));
  if (edit)
    _waypoint.script = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_PLACE));
  if (edit)
    _waypoint.placement = edit->GetText() ? atof(edit->GetText()) : 0;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_PREC));
  if (edit)
    _waypoint.completitionRadius = edit->GetText() ? atof(edit->GetText()) : 0;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_TIMEOUT_MIN));
  if (edit)
    _waypoint.timeoutMin = edit->GetText() ? atof(edit->GetText()) : 0;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_TIMEOUT_MID));
  if (edit)
    _waypoint.timeoutMid = edit->GetText() ? atof(edit->GetText()) : 0;
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_ARCWP_TIMEOUT_MAX));
  if (edit)
    _waypoint.timeoutMax = edit->GetText() ? atof(edit->GetText()) : 0;
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCWP_HOUSEPOS));
  if (combo)
    _waypoint.housePos = combo->GetCurSel();
  else
    _waypoint.housePos = -1;

  CToolBox *toolbox = dynamic_cast<CToolBox *>(GetCtrl(IDC_ARCWP_SHOW));
  if (toolbox)
//    _waypoint.show = toolbox->GetCurSel() == 1;
    _waypoint.showWP = (AWPShow)toolbox->GetCurSel();
}

Control *DisplayTemplateSave::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_TEMPL_NAME:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetMaxChars(30);
      edit->SetText(DecodeFileName(Glob.header.filename));
      return edit;
    }
  case IDC_TEMPL_MODE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      combo->AddString(LocalizeString(IDS_EXPORT_NONE));
      combo->AddString(LocalizeString(IDS_EXPORT_SINGLE));
      combo->AddString(LocalizeString(IDS_EXPORT_MULTI));
      combo->AddString(LocalizeString(IDS_EXPORT_MAIL));
      combo->SetCurSel(0);
      return combo;
    }
  default:
    return Display::OnCreateCtrl(type, idc, cls);
  }
}

/*!
  \patch_internal 1.01 Date 07/09/2001 by Jirka
  - added - offers only islands where wrp file exists
*/
Control *DisplayTemplateLoad::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  if (idc == IDC_TEMPL_TITLE)
  {
    CStatic *text = new CStatic(this, idc, cls);
    if (_merge)
    {
      text->SetText(LocalizeString(IDS_TEMPL_MERGE));
    }
    return text;
  }
  else if (idc == IDC_TEMPL_ISLAND)
  {
    CCombo *combo = new CCombo(this, idc, cls);
    int sel = 0;
    // int n = (Pars>>"CfgWorlds">>"worlds").GetSize();
    int n = (Pars >> "CfgWorldList").GetEntryCount();
    for (int i=0; i<n; i++)
    {
      ParamEntryVal entry = (Pars >> "CfgWorldList").GetEntry(i);
      if (!entry.IsClass()) continue;
      RString name = entry.GetName();
      // RString name = (Pars>>"CfgWorlds">>"worlds")[i];

#if _FORCE_DEMO_ISLAND
      RString demo = Pars >> "CfgWorlds" >> "demoWorld";
      if (stricmp(name, demo) != 0) continue;
#endif

      // ADDED - check if wrp file exists
      RString fullname = GetWorldName(name);
      if (!QFBankQueryFunctions::FileExists(fullname)) continue;
      
      int index = combo->AddString
      (
        Pars >> "CfgWorlds" >> name >> "description"
      );
      combo->SetData(index, name);

      if (stricmp(name, Glob.header.worldname) == 0)
        sel = index;
    }
    combo->SetCurSel(sel);
    return combo;
  }
  return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayTemplateLoad::OnLBSelChanged(IControl *ctrl, int curSel)
{
  if (ctrl->IDC() == IDC_TEMPL_ISLAND) OnIslandChanged();

  Display::OnLBSelChanged(ctrl, curSel);
}

void DisplayTemplateLoad::OnIslandChanged()
{
  CCombo *combo = check_cast<CCombo *>(GetCtrl(IDC_TEMPL_ISLAND));
  if (!combo) return;
  int index = combo->GetCurSel();
  if (index < 0) return;
  RString island = combo->GetData(index);

  combo = check_cast<CCombo *>(GetCtrl(IDC_TEMPL_NAME));
  if (!combo) return;

  combo->ClearStrings();

  // New mission
  index = combo->AddString(LocalizeString(IDS_SINGLE_NEW_MISSION));
  combo->SetData(index, RString());
  combo->SetValue(index, 0); // only for sorting

  // List of missions
  #ifdef _WIN32
  char buffer[256];
  sprintf
  (
    buffer, "%s*.%s",
    (const char *)GetMissionsDirectory(),
    (const char *)island
  );
  WIN32_FIND_DATA info;
  HANDLE h = FindFirstFile(buffer, &info);
  if (h != INVALID_HANDLE_VALUE)
  {
    do
    {
      if 
      (
        (info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0 &&
        info.cFileName[0] != '.'
      )
      {
        char name[256];
        strcpy(name, info.cFileName);
        char *ext = strrchr(name, '.');
        if (ext) *ext = 0;
        int index = combo->AddString(DecodeFileName(name));
        combo->SetData(index, name);
        combo->SetValue(index, 1); // only for sorting
      }
    }
    while (FindNextFile(h, &info));
    FindClose(h);
  }
  #endif
  combo->SortItemsByValue();
  combo->SetCurSel(0);
}

// static const int daysInMonth[]={31,28,31,30,31,30,31,31,30,31,30,31};
int GetDaysInMonth(int year, int month);

void DisplayIntel::UpdateDays(CCombo *combo, int day)
{
  CListBoxContainer *ctrl = GetListBoxContainer(GetCtrl(IDC_INTEL_YEAR));
  int sel = ctrl ? ctrl->GetCurSel() : -1;
  int year = sel >= 0 ? ctrl->GetValue(sel) : _intel.year;

  ctrl = GetListBoxContainer(GetCtrl(IDC_INTEL_MONTH));
  int month = ctrl ? ctrl->GetCurSel() : _intel.month;
  Assert(month >= 0);

  int curSel = day > 0 ? day - 1 : combo->GetCurSel();
  combo->ClearStrings();
  char buffer[4];
  for (int i=0; i<GetDaysInMonth(year, month); i++)
  {
    sprintf(buffer, "%d", i + 1);
    combo->AddString(buffer);
  }
  if (curSel < 0) curSel = 0;
  if (curSel >= combo->GetSize()) curSel = combo->GetSize() - 1;
  combo->SetCurSel(curSel);
}

void DisplayIntel::OnLBSelChanged(IControl *ctrl, int curSel)
{
  switch (ctrl->IDC())
  {
    case IDC_INTEL_MONTH:
    case IDC_INTEL_YEAR:
      {
        CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_DAY));
        if (combo) UpdateDays(combo);
      }
      break;
  }
}

/*!
\patch 5153 Date 4/17/2007 by Jirka
- New: Mission editor - year control in intel dialog
*/

Control *DisplayIntel::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
    case IDC_INTEL_YEAR:
    {
      int from = cls >> "from";
      int to = cls >> "to";
      if (from > to) return NULL;

      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      CListBoxContainer *list = GetListBoxContainer(ctrl);
      int sel = 0;
      for (int i=from; i<=to; i++)
      {
        int index = list->AddString(Format("%d", i));
        list->SetValue(index, i);
        if (i == _intel.year) sel = index;
      }
      list->SetCurSel(sel);
      return ctrl;
    }
    case IDC_INTEL_MONTH:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      int i;
      for (i=0; i<12; i++)
        combo->AddString(LocalizeString(IDS_JANUARY + i));
      combo->SetCurSel(_intel.month - 1);
      return combo;
    }
    case IDC_INTEL_DAY:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      UpdateDays(combo, _intel.day);
      return combo;
    }
    case IDC_INTEL_HOUR:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      char buffer[4];
      for (int i=0; i<24; i++)
      {
        sprintf(buffer, "%d", i);
        combo->AddString(buffer);
      }
      combo->SetCurSel(_intel.hour);
      return combo;
    }
    case IDC_INTEL_MINUTE:
    {
      CCombo *combo = new CCombo(this, idc, cls);
      char buffer[4];
      for (int i=0; i<60; i+=5)
      {
        sprintf(buffer, "%02d", i);
        combo->AddString(buffer);
      }
      combo->SetCurSel(toInt(_intel.minute / 5.0));
      return combo;
    }
    case IDC_INTEL_RESISTANCE:
    {
      CToolBox *ctrl = new CToolBox(this, idc, cls);
      if (_intel.friends[TGuerrila][TEast] < 0.5)
        if (_intel.friends[TGuerrila][TWest] < 0.5)
          ctrl->SetCurSel(0);
        else
          ctrl->SetCurSel(1);
      else
        if (_intel.friends[TGuerrila][TWest] < 0.5)
          ctrl->SetCurSel(2);
        else
          ctrl->SetCurSel(3);
      return ctrl;
    }
/*
    {
      CSlider *slider = new CSlider(this, idc, cls);
      slider->SetRange(0, 1);
      slider->SetSpeed(0.01, 0.1);
      slider->SetThumbPos(_intel.friends[TGuerrila][TWest]);
      return slider;
    }
*/
    case IDC_INTEL_WEATHER:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(1.0 - _intel.weather);
      }
      return ctrl;
    }
    case IDC_INTEL_FOG:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 0.75); // avoid alpha artefact news:hqp9ma$lph$1@new-server.localdomain
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_intel.fog);
      }
      return ctrl;
    }
    case IDC_INTEL_WEATHER_FORECAST:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(1.0 - _intel.weatherForecast);
      }
      return ctrl;
    }
    case IDC_INTEL_FOG_FORECAST:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      InitPtr<CSliderContainer> slider = GetSliderContainer(ctrl);
      if(slider)
      {
        slider->SetRange(0, 1);
        slider->SetSpeed(0.01, 0.1);
        slider->SetThumbPos(_intel.fogForecast);
      }
      return ctrl;
    }
    case IDC_INTEL_BRIEFING_NAME:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_intel.briefingName);
      return edit;
    }
    case IDC_INTEL_BRIEFING_DESC:
    {
      CEdit *edit = new CEdit(this, idc, cls);
      edit->SetText(_intel.briefingDescription);
      return edit;
    }
  }
  return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayIntel::Destroy()
{
  Display::Destroy();

  if (_exit != IDC_OK) return;
        
  CListBoxContainer *list = GetListBoxContainer(GetCtrl(IDC_INTEL_YEAR));
  if (list)
  {
    int sel = list->GetCurSel();
    if (sel >= 0) _intel.year = list->GetValue(sel);
  }

  CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_MONTH));
  if (combo)
    _intel.month = combo->GetCurSel() + 1;
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_DAY));
  if (combo)
    _intel.day = combo->GetCurSel() + 1;
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_HOUR));
  if (combo)
    _intel.hour = combo->GetCurSel();
  combo = dynamic_cast<CCombo *>(GetCtrl(IDC_INTEL_MINUTE));
  if (combo)
    _intel.minute = 5 * combo->GetCurSel();
  CToolBox *ctrl = dynamic_cast<CToolBox *>(GetCtrl(IDC_INTEL_RESISTANCE));
  if (ctrl)
    switch (ctrl->GetCurSel())
    {
    case 0:
      _intel.friends[TWest][TGuerrila] = _intel.friends[TGuerrila][TWest] = 0.0;
      _intel.friends[TEast][TGuerrila] = _intel.friends[TGuerrila][TEast] = 0.0;
      break;
    case 1:
      _intel.friends[TWest][TGuerrila] = _intel.friends[TGuerrila][TWest] = 1.0;
      _intel.friends[TEast][TGuerrila] = _intel.friends[TGuerrila][TEast] = 0.0;
      break;
    case 2:
      _intel.friends[TWest][TGuerrila] = _intel.friends[TGuerrila][TWest] = 0.0;
      _intel.friends[TEast][TGuerrila] = _intel.friends[TGuerrila][TEast] = 1.0;
      break;
    case 3:
      _intel.friends[TWest][TGuerrila] = _intel.friends[TGuerrila][TWest] = 1.0;
      _intel.friends[TEast][TGuerrila] = _intel.friends[TGuerrila][TEast] = 1.0;
      break;
    }
  CSliderContainer *slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_INTEL_WEATHER));
  if (slider)
    _intel.weather = 1.0 - slider->GetThumbPos();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_INTEL_FOG));
  if (slider)
    _intel.fog = slider->GetThumbPos();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_INTEL_WEATHER_FORECAST));
  if (slider)
    _intel.weatherForecast = 1.0 - slider->GetThumbPos();
  slider = dynamic_cast<CSliderContainer *>(GetCtrl(IDC_INTEL_FOG_FORECAST));
  if (slider)
    _intel.fogForecast = slider->GetThumbPos();
  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_INTEL_BRIEFING_NAME));
  if (edit)
    _intel.briefingName = edit->GetText();
  edit = dynamic_cast<CEdit *>(GetCtrl(IDC_INTEL_BRIEFING_DESC));
  if (edit)
    _intel.briefingDescription = edit->GetText();
}

#endif // #if _ENABLE_EDITOR

//! returns current (topmost) display
Display *CurrentDisplay()
{
  if (!GWorld) return NULL;

  Display *disp = dynamic_cast<Display *>(GWorld->Options());
  if (!disp) return NULL;

  ControlsContainer *ptr = disp;
  while (ptr->Child())
    ptr = ptr->Child();
  return dynamic_cast<Display *>(ptr);
}

//! creates message box in topmost display (NOT CURRENTLY USED)
bool MsgBox(RString text, int flags, int idd)
{
  Display *disp = CurrentDisplay();
  if (!disp) return false;

  disp->CreateMsgBox(flags, text, idd);
  return true;
}

//! creates message box in topmost display (NOT CURRENTLY USED)
bool MsgBox(int ids, int flags, int idd)
{
  return MsgBox(LocalizeString(ids), flags, idd);
}

//! parse name of template (only file name) and sets current mission
bool ProcessTemplateName(RString name, RString dir)
{
  char buffer[256];
  strcpy(buffer, name);

  // parse mission name

  // mission
  char *world = strchr(buffer, '.');
  if (!world) return false;
  *world = 0;
  world++;

  // world
  char *ext = strchr(world, '.');
  if (ext)
  {
    *ext = 0;
    ext++;
    Assert(stricmp(ext, "sqm") == 0);
  }

  SetMission(world, buffer, dir);
  return true;
}

//! parse name of template (only file name) and sets current mission
bool ProcessTemplateName(RString name)
{
  return ProcessTemplateName(name, MissionsDir);
}

//! parse name of template (full path) and sets current base directory and mission
bool ProcessFullName(RString name)
{
  // parse filename
  char buffer[256];
  strcpy(buffer, name);
  strlwr(buffer);

  // filename
  char *ext = strrchr(buffer, '\\');
  if (!ext) return false;
  *ext = 0;

  // world
  ext = strrchr(buffer, '.');
  if (!ext) return false;
  Assert(ext);
  RString world = ext + 1;
  *ext = 0;

  // mission
  ext = strrchr(buffer, '\\');
  if (!ext) return false;

  RString mission = ext + 1;
  *(ext + 1) = 0;

  // base directory and subdirectory
  RString dir, subdir;
  const char *ptr = "campaigns\\";
  ext = strstr(buffer, ptr);
  if (ext)
  {
    ext = strchr(ext + strlen(ptr), '\\');
    if (!ext) return false;
    subdir = ext + 1;
    *(ext + 1) = 0;
    dir = buffer;
  }
  else subdir = buffer;

  CurrentCampaign = "";
  CurrentBattle = "";
  CurrentMission = "";
  SetBaseDirectory(false, dir);
  SetMission(world, mission, subdir);
  return true;
}

extern AutoArray<char> GTransferredUserMission;

//! reads mission file (mission.sqm) and parse given cutscene / mission into CurrentTemplate
bool ParseCutscene(RString cutscene, bool multiplayer, bool avoidCheckIds, bool allowEmpty = false)
{
  SECUROM_MARKER_SECURITY_ON(14)
  FreeOnDemandGarbageCollect(1024*1024,256*1024);
  
  RString name = GetMissionDirectory() + RString("mission.sqm");

  GameVarSpace local(false);
  GGameState.BeginContext(&local);

  GameDataNamespace globals(NULL, RString(), false); // TODO: parsing namespace if access to globals needed

  RString missionParamsName = GetMissionParameters();
  bool hasMissionParams = missionParamsName.GetLength() > 0;
  if (hasMissionParams)
  {

    // mission parameters file
    ParamFile missionParams;
#ifdef _XBOX

# if _XBOX_VER >= 200
    XSaveGame save = GSaveSystem.OpenSave(missionParamsName);
    if (!save)
    {
      // Errors already handled
      GGameState.EndContext();
      CurrentTemplate.Clear();
      return false;
    }
    if (missionParams.Parse(RString(SAVE_ROOT) + RString("mission.") + GetExtensionWizardMission(), NULL, &local, &globals) != LSOK)
    {
      ReportUserFileError(missionParamsName, RString("mission.") + GetExtensionWizardMission(), GetMissionParametersDisplayName(), false);
      return false;
    }
# else
    if (stricmp(missionParamsName, ".") == 0)
    {
      QIStrStream stream(GTransferredUserMission.Data(), GTransferredUserMission.Size());
      if (!missionParams.ParseSigned(stream, NULL, NULL, &globals))
      {
        // Warning message ???

        GGameState.EndContext();
        CurrentTemplate.Clear();
        return false;
      }
    }
    else
    {
      if (!missionParams.ParseSigned(missionParamsName, NULL, NULL, &globals))
      {
        const char *ptr = strrchr(missionParamsName, '\\');
        ptr++;
        RString dir = missionParamsName.Substring(0, ptr - (const char *)missionParamsName);

        ReportUserFileError(dir, ptr, RString(), false);

        GGameState.EndContext();
        CurrentTemplate.Clear();
        return false;
      }
    }
# endif

#else
    missionParams.Parse(missionParamsName, NULL, NULL, &globals);
#endif

    // parameters
    float weather = missionParams >> "weather";
    GGameState.VarSetLocal("_weather", weather);
    float weatherForecast = missionParams >> "weatherForecast";
    GGameState.VarSetLocal("_weatherForecast", weatherForecast);
    float fog = missionParams >> "fog";
    GGameState.VarSetLocal("_fog", fog);
    float fogForecast = missionParams >> "fogForecast";
    GGameState.VarSetLocal("_fogForecast", fogForecast);
    float viewDistance = 900.0f;
    ConstParamEntryPtr entry = missionParams.FindEntry("viewDistance");
    if (entry) viewDistance = *entry;
    GGameState.VarSetLocal("_viewDistance", viewDistance);
    int year = missionParams >> "year";
    GGameState.VarSetLocal("_year", (float)year);
    int month = missionParams >> "month";
    GGameState.VarSetLocal("_month", (float)month);
    int day = missionParams >> "day";
    GGameState.VarSetLocal("_day", (float)day);
    int hour = missionParams >> "hour";
    GGameState.VarSetLocal("_hour", (float)hour);
    int minute = missionParams >> "minute";
    GGameState.VarSetLocal("_minute", (float)minute);

    // positions
    ConstParamEntryPtr parPos = missionParams.FindEntry("Positions");
    if (parPos)
    {
      for (int i=0; i<parPos->GetEntryCount(); i++)
      {
        ParamEntryVal entry = parPos->GetEntry(i);
        RString name = entry.GetName();
        float x = entry[0];
        float y = entry[1];
        float z = entry[2];
        GGameState.VarSetLocal(RString("_") + name + RString("_X"), x);
        GGameState.VarSetLocal(RString("_") + name + RString("_Y"), y);
        GGameState.VarSetLocal(RString("_") + name + RString("_Z"), z);
      }
    }

    // parameters
    ConstParamEntryPtr parParams = missionParams.FindEntry("Params");
    if (parParams)
    {
      for (int i=0; i<parParams->GetEntryCount(); i++)
      {
        ParamEntryVal entry = parParams->GetEntry(i);
        RString name = entry.GetName();
        float value = missionParams >> "Params" >> name; 

        GGameState.VarSetLocal(RString("_") + name, value);
      }
    }
  }

  {
    ParamArchiveLoad ar;
    if (!ar.LoadBin(name, &local, &globals) && !ar.Load(name, &local, &globals))
    {
      GGameState.EndContext();
      CurrentTemplate.Clear();
      return false;
    }

    ATSParams params;
    params.avoidCheckIds = avoidCheckIds;
    params.avoidCheckSync = true;
    ar.SetParams(&params);
    LSError result = ar.Serialize(cutscene, CurrentTemplate, 1);
    if (result == LSNoAddOn)
    {
      RString ReportMissingAddons(FindArrayRStringCI missing);
      RString message = ReportMissingAddons(CurrentTemplate.missingAddOns);
      WarningMessage(message);
    }
    else if (result != LSOK)
    {
      WarningMessage("Cannot load mission");
    }
    if (ar.GetArVersion() < 7)
    {
      if (ar.Serialize("Intel", CurrentTemplate.intel, 1) != LSOK)
      {
        WarningMessage("Cannot load intel (old mission format)");
      }
    }
  }

  if (hasMissionParams && stricmp(cutscene, "Mission") == 0)
  {
    GameDataNamespace globals(NULL, RString(), false); // TODO: parsing namespace if access to globals needed

    // mission parameters file
    ParamFile missionParams;
#ifdef _XBOX

# if _XBOX_VER >= 200
    XSaveGame save = GSaveSystem.OpenSave(missionParamsName);
    if (!save)
    {
      // Errors already handled
      GGameState.EndContext();
      CurrentTemplate.Clear();
      return false;
    }
    if (missionParams.Parse(RString(SAVE_ROOT) + RString("mission.") + GetExtensionWizardMission(), NULL, &local, &globals) != LSOK)
    {
      ReportUserFileError(missionParamsName, RString("mission.") + GetExtensionWizardMission(), GetMissionParametersDisplayName(), false);
      return false;
    }
# else
    if (stricmp(missionParamsName, ".") == 0)
    {
      QIStrStream stream(GTransferredUserMission.Data(), GTransferredUserMission.Size());
      if (!missionParams.ParseSigned(stream, NULL, NULL, &globals))
      {
        // Warning message ???

        GGameState.EndContext();
        CurrentTemplate.Clear();
        return false;
      }
    }
    else
    {
      if (!missionParams.ParseSigned(missionParamsName, NULL, &local, &globals))
      {
        const char *ptr = strrchr(missionParamsName, '\\');
        ptr++;
        RString dir = missionParamsName.Substring(0, ptr - (const char *)missionParamsName);

        ReportUserFileError(dir, ptr, RString(), false);

        GGameState.EndContext();
        CurrentTemplate.Clear();
        return false;
      }
    }
# endif

#else
    missionParams.Parse(missionParamsName, NULL, &local, &globals);
#endif

    // addons
    ConstParamEntryPtr addons = missionParams.FindEntry("addons");
    if (addons && addons->GetSize() > 0)
    {
      for (int i=0; i<addons->GetSize(); i++)
      {
        RString addon = (*addons)[i];
        CurrentTemplate.addOns.AddUnique(addon);
      }
      // check presence of addons
      bool CheckMissingAddons(FindArrayRStringCI &addOns);
      if (!CheckMissingAddons(CurrentTemplate.addOns))
      {
        GGameState.EndContext();
        CurrentTemplate.Clear();
        return false;
      }
    }

    // units
    ConstParamEntryPtr units = missionParams.FindEntry("Units");
    if (units)
    {
      for (int i=0; i<units->GetEntryCount(); i++)
      {
        ParamEntryVal unit = units->GetEntry(i);
        int index = unit >> "group";
        if (index < 0 || index >= CurrentTemplate.groups.Size())
        {
          RptF("Error: wrong group index %d in %s", index, (const char *)missionParamsName);
          continue;
        }
        ArcadeGroupInfo &group = CurrentTemplate.groups[index];
        ConstParamEntryPtr entry = unit.FindEntry("file");
        if (entry)
        {
          RString file = *entry;
          RString path = GetMissionDirectory() + file;
          ParamFile in;
          if (in.ParseBinOrTxt(path, NULL, &local, &globals))
          {
            int m = in.GetEntryCount();
            group.units.Realloc(m);
            group.units.Resize(m);
            for (int j=0; j<m; j++)
            {
              ParamEntryVal entry = in.GetEntry(j);
              ParamArchiveLoadEntry ar(new ParamClassEntry(entry));
              ar.SetArVersion(MissionsVersion);
              ATSParams params;
              params.avoidCheckIds = avoidCheckIds;
              params.avoidCheckSync = true;
              ar.SetParams(&params);
              ArcadeUnitInfo &unit = group.units[j];
              unit.Serialize(ar);
              if (unit.id < 0) unit.id = CurrentTemplate.nextVehId++;
              else saturateMax(CurrentTemplate.nextVehId, unit.id);
            }
          }
        }
        else
        {
          // create group from units
          RString type = unit >> "type";
          bool TypeIsGroup(RString name);
          int count = unit >> "count";
          if (count == 0)
          {
            group.units.Clear();
          }
          else if (TypeIsGroup(type))
          {
            ParamEntryVal TypeFindGroupEntry(RString name);
            ParamEntryVal clsType = TypeFindGroupEntry(type);
            AUTO_STATIC_ARRAY(RString, list, 32);
            for (int i=0; i<clsType.GetEntryCount(); i++)
            {
              ParamEntryVal clsUnit = clsType.GetEntry(i);
              if (clsUnit.IsClass()) list.Add(clsUnit.GetName());
            }

            DoAssert(group.units.Size() == 1);

            int n = list.Size();
            group.units.Realloc(n);
            group.units.Resize(n);
            
            for (int i=0; i<n; i++)
            {
              ParamEntryVal clsUnit = clsType >> list[i];
              RString vehicle = clsUnit >> "vehicle";
              RString rank = clsUnit >> "rank";

              RString iconName = Pars >> "CfgVehicles" >> vehicle >> "icon";
              Ref<Texture> icon = GlobLoadTexture(GetVehicleIcon(iconName));
              float size = Pars >> "CfgVehicles" >> vehicle >> "mapSize";

              ArcadeUnitInfo &unit = group.units[i];
              if (i == 0)
              {
                Assert(unit.leader);
              }
              else
              {
                unit = group.units[0];
                unit.description = RString();
                unit.id = CurrentTemplate.nextVehId++;
                unit.init = RString();
                unit.leader = false;
                unit.name = RString();
                if (unit.player != APNonplayable)
                  unit.player = APPlayableCDG;
              }
              unit.vehicle = vehicle;
              unit.icon = icon;
              unit.size = size;
              unit.rank = GetEnumValue<Rank>((const char *)rank);
              float RankToSkill(int rank);
              unit.skill = RankToSkill(unit.rank);
            }
          }
          else
          {
            DoAssert(group.units.Size() == 1);

            RString iconName = Pars >> "CfgVehicles" >> type >> "icon";
            Ref<Texture> icon = GlobLoadTexture(GetVehicleIcon(iconName));
            float size = Pars >> "CfgVehicles" >> type >> "mapSize";

            group.units.Realloc(count);
            group.units.Resize(count);

            // leader unit
            ArcadeUnitInfo &leader = group.units[0];
            Assert(leader.leader);
            leader.vehicle = type;
            leader.icon = icon;
            leader.size = size;

            int rank = leader.rank - 1;
            if (rank < RankPrivate) rank = RankPrivate;
            float RankToSkill(int rank);
            float skill = RankToSkill(rank);

            // other units
            for (int j=1; j<count; j++)
            {
              ArcadeUnitInfo &unit = group.units[j];
              unit = leader;
              unit.description = RString();
              unit.id = CurrentTemplate.nextVehId++;
              unit.init = RString();
              unit.leader = false;
              unit.name = RString();
              if (unit.player != APNonplayable)
                unit.player = APPlayableCDG;
              unit.rank = (Rank)rank;
              unit.skill = skill;
            }
          }
        }
      }
    }

    // Added units
    units = missionParams.FindEntry("Added");
    if (units)
    {
      for (int i=0; i<units->GetEntryCount(); i++)
      {
        ParamEntryVal unit = units->GetEntry(i);

        RStringB sideName = unit >> "side";
        TargetSide side = GetEnumValue<TargetSide>(sideName);

        RString type = unit >> "type";
        Vector3 position;
        position[0] = (unit >> "position")[0];
        position[1] = (unit >> "position")[1];
        position[2] = (unit >> "position")[2];
        float angle = 0;
        ConstParamEntryPtr ptr = unit.FindEntry("angle");
        if (ptr) angle = *ptr;

        if (side == TEmpty || side == TAmbientLife)
        {
          RString iconName = Pars >> "CfgVehicles" >> type >> "icon";
          Ref<Texture> icon = GlobLoadTexture(GetVehicleIcon(iconName));
          float size = Pars >> "CfgVehicles" >> type >> "mapSize";

          // empty vehicle
          int index = CurrentTemplate.emptyVehicles.Add();
          ArcadeUnitInfo &unit = CurrentTemplate.emptyVehicles[index];
          unit.position = position;
          unit.azimut = angle;
          unit.id = CurrentTemplate.nextVehId++;
          unit.side = TSideUnknown;
          unit.vehicle = type;
          unit.icon = icon;
          unit.size = size;
        }
        else
        {
          int count = unit >> "count";
          if (count == 0) continue;

          // group
          int index = CurrentTemplate.groups.Add();
          ArcadeGroupInfo &group = CurrentTemplate.groups[index];
          group.side = side;

          bool TypeIsGroup(RString name);
          if (TypeIsGroup(type))
          {
            ParamEntryVal TypeFindGroupEntry(RString name);
            ParamEntryVal clsType = TypeFindGroupEntry(type);
            AUTO_STATIC_ARRAY(RString, list, 32);
            for (int i=0; i<clsType.GetEntryCount(); i++)
            {
              ParamEntryVal clsUnit = clsType.GetEntry(i);
              if (clsUnit.IsClass()) list.Add(clsUnit.GetName());
            }

            int n = list.Size();
            group.units.Realloc(n);
            group.units.Resize(n);

            for (int i=0; i<n; i++)
            {
              ParamEntryVal clsUnit = clsType >> list[i];
              RString vehicle = clsUnit >> "vehicle";
              RString rank = clsUnit >> "rank";

              RString iconName = Pars >> "CfgVehicles" >> vehicle >> "icon";
              Ref<Texture> icon = GlobLoadTexture(GetVehicleIcon(iconName));
              float size = Pars >> "CfgVehicles" >> vehicle >> "mapSize";

              ArcadeUnitInfo &unit = group.units[i];
              unit.leader = i == 0;
              unit.position = position;
              unit.azimut = angle;
              unit.id = CurrentTemplate.nextVehId++;
              unit.side = side;
              unit.special = ASpFlying;
              unit.vehicle = vehicle;
              unit.icon = icon;
              unit.size = size;
              unit.rank = GetEnumValue<Rank>((const char *)rank);
              float RankToSkill(int rank);
              unit.skill = RankToSkill(unit.rank);
              unit.player = APNonplayable;
            }
          }
          else
          {
            group.units.Realloc(count);
            group.units.Resize(count);

            RString iconName = Pars >> "CfgVehicles" >> type >> "icon";
            Ref<Texture> icon = GlobLoadTexture(GetVehicleIcon(iconName));
            float size = Pars >> "CfgVehicles" >> type >> "mapSize";

            // leader
            ArcadeUnitInfo &leader = group.units[0];
            leader.position = position;
            leader.azimut = angle;
            leader.id = CurrentTemplate.nextVehId++;
            leader.side = side;
            leader.vehicle = type;
            leader.icon = icon;
            leader.size = size;
            leader.leader = true;
            leader.special = ASpFlying;
            // ?? rank, skill

            // other units
            for (int j=1; j<count; j++)
            {
              ArcadeUnitInfo &unit = group.units[j];
              unit.position = position;
              unit.azimut = angle;
              unit.id = CurrentTemplate.nextVehId++;
              unit.side = side;
              unit.vehicle = type;
              unit.icon = icon;
              unit.size = size;
              unit.special = ASpFlying;
              // ?? rank, skill
            }
          }

          // waypoints
          ConstParamEntryPtr clsWp = unit.FindEntry("Waypoints");
          if (clsWp)
          {
            int m = clsWp->GetEntryCount();
            group.waypoints.Realloc(m);
            group.waypoints.Resize(m);
            for (int j=0; j<m; j++)
            {
              ParamEntryVal entry = clsWp->GetEntry(j);
              Vector3 position;
              position[0] = (entry >> "position")[0];
              position[1] = (entry >> "position")[1];
              position[2] = (entry >> "position")[2];
              RStringB name = entry >> "type";
              ArcadeWaypointType type = GetEnumValue<ArcadeWaypointType>(name);
              name = entry >> "behaviour";
              CombatMode behaviour = GetEnumValue<CombatMode>(name);
              name = entry >> "formation";
              AI::Formation formation = GetEnumValue<AI::Formation>(name);
              ConstParamEntryPtr ptr = entry.FindEntry("combatMode");
              AI::Semaphore combatMode = (AI::Semaphore)-1;
              if (ptr) combatMode = GetEnumValue<AI::Semaphore>(*ptr);
              
              ArcadeWaypointInfo &wp = group.waypoints[j];
              wp.position = position;
              wp.type = type;
              wp.combat = behaviour;
              wp.formation = formation;
              wp.combatMode = combatMode;
            }
          }
        }
      }
    }
/*
    // Testing save
    ATSParams params;
    params.avoidCheckSync = true;
    ParamArchiveSave out(11, &params);
    CurrentTemplate.Serialize(out);
    out.Save("test.sqm");
*/
  }

  GGameState.EndContext();
  
  FreeOnDemandGarbageCollect(1024*1024,256*1024);

  if
  (
    !allowEmpty && CurrentTemplate.IsEmpty() ||
    !CurrentTemplate.IsConsistent(NULL, multiplayer)
  )
  {
    CurrentTemplate.Clear();
    return false;
  }

  ArcadeUnitInfo *uInfo = CurrentTemplate.FindPlayer(); 
  if (uInfo)
  {
    Glob.header.playerSide = (TargetSide)uInfo->side;
  }
  SECUROM_MARKER_SECURITY_OFF(14)

  return true;
}

//! reads mission file (mission.sqm) and parse intro cutscene into CurrentTemplate
bool ParseIntro(bool avoidCheckIds)
{
  return ParseCutscene("Intro", false, avoidCheckIds);
}

//! reads mission file (mission.sqm) and parse mission into CurrentTemplate
bool ParseMission(bool multiplayer, bool avoidCheckIds, bool allowEmpty = false)
{
  return ParseCutscene("Mission", multiplayer, avoidCheckIds, allowEmpty);
}

