/*!
\file
HUD UI - In-game UIl layer
\patch_internal 1.01 Date 6/29/2001 by Ondra.
Win32 call replaced by corresponding C counterparts.
Win32 are DLL linked and make protection impossible,
as they are changed by macromedia protection.
*/

#include "../wpch.hpp"
#include "../world.hpp"
#include "../AI/ai.hpp"
#include "../person.hpp"
#include "../keyInput.hpp"
#include "../joystick.hpp"
#include "InGameUIImpl.hpp"
#include "../engine.hpp"
#include "../camera.hpp"
#include "../landscape.hpp"
#include "../detector.hpp"
#include "../house.hpp"
#include <El/QStream/qbStream.hpp>
#include "../Network/network.hpp"
#include <El/Common/randomGen.hpp>
#include "../cameraHold.hpp"
#include "../diagModes.hpp"

#include "../move_actions.hpp"
#include "../manActs.hpp"
#include "../fileLocator.hpp"

#include <El/ParamFile/paramFile.hpp>
#include <El/Evaluator/express.hpp>
#include <El/Common/perfProf.hpp>

#include "resincl.hpp"
#include <ctype.h>
#ifdef _WIN32
  #include <io.h>
#endif
#include "../dikCodes.h"

#include <Es/Algorithms/qsort.hpp>

#include "chat.hpp"

#include "../stringtableExt.hpp"
#include "../mbcs.hpp"

#include "../drawText.hpp"
#include "../progress.hpp"

#include "../gameStateExt.hpp"

/*
#define FontS "tahomaB24"
#define FontM "tahomaB36"
*/

#if SPEEREO_SPEECH
#include <Es/Common/win.h>
#include <SpeereoSpeech/hdlcut.h>
#pragma comment(lib, "hdlcutmt")
#include <El/Interfaces/iAppInfo.hpp>
#endif

//when LSHIFT_SELECT_WHOLE_TEAM, the lShift + Fn or mouse click selects the whole team, instead of unit only
#define LSHIFT_SELECT_WHOLE_TEAM 1

///////////////////////////////////////////////////////////////////////////////
// Global selected units - valid for IngameUI and map

Team InGameUI::GetTeam(AIUnit *unit)
{
  for (int i=0; i<NTeams; i++)
  {
    if (i == TeamMain) continue;
    if (_teamMembers[i].FindKey(unit) >= 0) return (Team)i;
  }
  return TeamMain;
}

void InGameUI::SetTeam(AIUnit *unit, Team team)
{
  // remove from the old one
  for (int i=0; i<NTeams; i++)
  {
    if (i == TeamMain) continue;
    if (_teamMembers[i].DeleteKey(unit)) break;
  }
  if (team != TeamMain) _teamMembers[team].AddUnique(unit);
}

void InGameUI::ClearTeams()
{
  for (int i=0; i<NTeams; i++)
    _teamMembers[i].Clear();
}

void InGameUI::ListTeam(OLinkPermNOArray(AIUnit) &list, Team team)
{
  list.Resize(0);
  if (team == TeamMain)
  {
    // player group
    AIBrain *player = GWorld->FocusOn();
    if (!player) return;
    AIGroup *group = player->GetGroup();
    if (!group) return;

    // list all units in the player group
    for (int i=0; i<group->NUnits(); i++)
    {
      AIUnit *unit = group->GetUnit(i);
      if (unit && unit != player) list.Add(unit);
    }

    // remove team members
    for (int t=0; t<NTeams; t++)
    {
      if (t == TeamMain) continue;
      TeamMembers &members = _teamMembers[t];
      for (int i=0; i<members.Size(); i++)
      {
        AIUnit *unit = members[i];
        if (unit)
        {
          // remove it
          for (int j=0; j<list.Size(); j++)
          {
            if (list[j] == unit)
            {
              list.Delete(j);
              break;
            }
          }
        }
      }
    }
  }
  else
  {
    TeamMembers &members = _teamMembers[team];
    for (int i=0; i<members.Size(); i++)
    {
      AIUnit *unit = members[i];
      if (unit) list.Add(unit);
    }
  }
  list.Compact();
}

bool InGameUI::IsSelectedUnit(AIUnit *unit) const
{
  for (int i=0; i<_selectedUnits.Size(); i++)
  {
    if (_selectedUnits[i]._unit == unit) return true;
  }
  return false;
}

UITime InGameUI::WhenSelectedUnit(AIUnit *unit) const
{
  for (int i=0; i<_selectedUnits.Size(); i++)
  {
    if (_selectedUnits[i]._unit == unit) return _selectedUnits[i]._lastSelTime;
  }
  return Glob.uiTime - 120;
}

void InGameUI::SelectUnit(AIUnit *unit)
{
  if (unit != GWorld->FocusOn())
  {
    for (int i=0; i<_selectedUnits.Size(); i++)
    {
      if (_selectedUnits[i]._unit == unit)
      {
        _selectedUnits[i]._lastSelTime = Glob.uiTime;
        return;
      }
    }
    int i = _selectedUnits.Add();
    _selectedUnits[i]._unit = unit;
    _selectedUnits[i]._lastSelTime = Glob.uiTime;
  }
}

void InGameUI::UnselectUnit(AIUnit *unit)
{
  for (int i=0; i<_selectedUnits.Size(); i++)
  {
    if (_selectedUnits[i]._unit == unit)
    {
      _selectedUnits.Delete(i);
      return;
    }
  }
}

void InGameUI::ClearSelectedUnits()
{
  _selectedUnits.Clear();
}

bool InGameUI::IsEmptySelectedUnits()
{
  AIBrain *player = GWorld->FocusOn();
  for (int i=0; i<_selectedUnits.Size();)
  {
    AIUnit *unit = _selectedUnits[i]._unit;
    if (!unit || unit == player) _selectedUnits.Delete(i);
    else i++;
  }
  return _selectedUnits.Size() == 0;
}

void InGameUI::ListSelectedUnits(OLinkPermNOArray(AIUnit) &list)
{
  list.Resize(0);

  AIBrain *player = GWorld->FocusOn();
  for (int i=0; i<_selectedUnits.Size();)
  {
    AIUnit *unit = _selectedUnits[i]._unit;
    if (!unit || unit == player) _selectedUnits.Delete(i);
    else
    {
      list.Add(unit);
      i++;
    }
  }

  list.Compact();
}

void InGameUI::ListSelectingUnits(OLinkPermNOArray(AIUnit) &list)
{
  ListSelectedUnits(list);

  // player group
  AIBrain *player = GWorld->FocusOn();
  if (!player) return;
  AIGroup *group = player->GetGroup();
  if (!group) return;

  // check units bar
  if (player->GetUnit() && player->GetUnit()->ID() - 1 == _groupInfoSelected)
  {
    // select all
    for (int i=0; i<group->NUnits(); i++)
    {
      AIUnit *unit = group->GetUnit(i);
      if (unit && unit != player) list.AddUnique(unit);
    }
  }
  else if (_groupInfoSelected >= 0)
  {
    AIUnit *unit = group->GetUnit(_groupInfoSelected);
    if (unit && unit != player) list.AddUnique(unit);
  }

  // check menu
  Menu *menuCurrent = GetCurrentMenu();
  if (!menuCurrent) return;
  int index = menuCurrent->_selected;
  if (index < 0 || index >= menuCurrent->_items.Size()) return;
  MenuItem *item = menuCurrent->_items[index];
  if (!item->_visible || !item->_enable) return;

  switch (item->_cmd)
  {
  case CMD_UNIT_1:
  case CMD_UNIT_2:
  case CMD_UNIT_3:
  case CMD_UNIT_4:
  case CMD_UNIT_5:
  case CMD_UNIT_6:
  case CMD_UNIT_7:
  case CMD_UNIT_8:
  case CMD_UNIT_9:
  case CMD_UNIT_10:
  case CMD_UNIT_11:
  case CMD_UNIT_12:
    {
      int i = item->_cmd - CMD_UNIT_1;
      AIUnit *unit = group->GetUnit(i);
      if (unit && unit != player) list.AddUnique(unit);
    }
    break;
  case CMD_VEHICLE_1:
  case CMD_VEHICLE_2:
  case CMD_VEHICLE_3:
  case CMD_VEHICLE_4:
  case CMD_VEHICLE_5:
  case CMD_VEHICLE_6:
  case CMD_VEHICLE_7:
  case CMD_VEHICLE_8:
  case CMD_VEHICLE_9:
  case CMD_VEHICLE_10:
  case CMD_VEHICLE_11:
  case CMD_VEHICLE_12:
    {
      int i = item->_cmd - CMD_VEHICLE_1;
      AIUnit *unit = group->GetUnit(i);
      if (unit)
      {
        Transport *veh = unit->GetVehicleIn();
        if (veh)
        {
          AIBrain *agent = veh->CommanderUnit();
          unit = agent ? agent->GetUnit() : NULL;
          if (unit && unit->GetGroup() == group)
            list.AddUnique(unit);
        }
      }
    }
    break;
  case CMD_SELECT_MAIN:
    for (int i=0; i<group->NUnits(); i++)
    {
      AIUnit *unit = group->GetUnit(i);
      if (unit && unit != player && GetTeam(unit) == TeamMain) list.AddUnique(unit);
    }
    break;
  case CMD_SELECT_RED:
    for (int i=0; i<_teamMembers[TeamRed].Size(); i++)
    {
      AIUnit *unit = _teamMembers[TeamRed][i];
      if (unit && unit != player) list.AddUnique(unit);
    }
    break;
  case CMD_SELECT_GREEN:
    for (int i=0; i<_teamMembers[TeamGreen].Size(); i++)
    {
      AIUnit *unit = _teamMembers[TeamGreen][i];
      if (unit && unit != player) list.AddUnique(unit);
    }
    break;
  case CMD_SELECT_BLUE:
    for (int i=0; i<_teamMembers[TeamBlue].Size(); i++)
    {
      AIUnit *unit = _teamMembers[TeamBlue][i];
      if (unit && unit != player) list.AddUnique(unit);
    }
    break;
  case CMD_SELECT_YELLOW:
    for (int i=0; i<_teamMembers[TeamYellow].Size(); i++)
    {
      AIUnit *unit = _teamMembers[TeamYellow][i];
      if (unit && unit != player) list.AddUnique(unit);
    }
    break;
  case CMD_UNITS_ALL:
    for (int i=0; i<group->NUnits(); i++)
    {
      AIUnit *unit = group->GetUnit(i);
      if (unit && unit != player) list.AddUnique(unit);
    }
    break;
  }
  list.Compact();
}

int InGameUI::SelectHCGroupsCount(AIUnit *commander)
{
  if(commander)
  {
    AutoArray<AIHCGroup> groups;
    int selected =0;
    for (int i=0; i< groups.Size();i++)
    {
      if(groups[i].IsSelected()) selected++;
    }
    return selected;
  }
  return 0;
}

extern void  OnCommandModeChanged();
// show high command bar
void InGameUI::ShowHCCommand(bool show) 
{
  _drawHCCommand = show;
  OnCommandModeChanged();
};

///////////////////////////////////////////////////////////////////////////////
// class UIActions - action list

#define ACTIONS_CREATE(name, type) \
  case AT##name: \
    return new type(AT##name);

Action *CreateAction(UIActionType type)
{
  switch (type)
  {
    ACTIONS_DEFAULT(ACTIONS_CREATE)
#if _ENABLE_CONVERSATION
    ACTIONS_CONVERSATION(ACTIONS_CREATE)
#endif    
#if _VBS3 && _ENABLE_CARRY_BODY //BattlefieldClearance
    ACTIONS_CARRY_BODY(ACTIONS_CREATE)
#endif
#if _VBS2
    ACTIONS_VBS2(ACTIONS_CREATE)
#endif
  }
  RptF("Unknown action type: %d", safe_cast<int>(type));
  return NULL;
}

bool Action::HasEqualType(const Action *to) const
{
  return to && _type == to->GetType();
}

int Action::Compare(const Action *with) const
{
  int s = (int)GetTarget() - (int)with->GetTarget();
  if (s != 0) return s;
  s = (int)GetType() - (int)with->GetType();
  return s;
}

INode *FormatText(const char *format, RefR<INode> *args, int nArgs, bool structured);

RefR<INode> Action::GetText(AIBrain *unit) const
{
  UIActionParams params;
  if (!GetParams(params, unit)) return NULL;
  return FormatText(UIActions::actionTypes[GetType()].text, params.Data(), params.Size(), true);
}

RefR<INode> Action::GetTextDefault(AIBrain *unit) const
{
  UIActionParams params;
  if (!GetParams(params, unit)) return NULL;
  return FormatText(UIActions::actionTypes[GetType()].textDefault, params.Data(), params.Size(), true);
}

RString Action::GetTextSimple(AIBrain *unit) const
{
  UIActionParams params;
  if (!GetParams(params, unit)) return RString();
  RefR<INode> text = FormatText(UIActions::actionTypes[GetType()].textSimple, params.Data(), params.Size(), true);
  return GetTextPlain(text);
}

bool Action::GetParams(UIActionParams &params, AIBrain *unit) const
{
  if (!_target)
  {
    // regular option for commanding menu
    // RptF("Action %s - missing target", cc_cast(FindEnumName(GetType())));
    return false;
  }
  return _target->GetActionParams(params, this, unit);
}

bool Action::Process(AIBrain *unit) const
{
  if (_target)
  {
    _target->PerformAction(this, unit);
    return true;
  }
  else
  {
    RptF("Action::Process - No target [action: %s]", cc_cast( FindEnumName(GetType()) ) );
    return false;
  }
}

#if _VBS3
bool Action::PerformScripted(AIBrain *unit) const
{
  UIActionTypeDesc desc = UIActions::actionTypes[_type];

  EntityAI *tgt = GetTarget();

  if(desc.script.GetLength() > 0)
  {
    GameVarSpace local(false);
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(&local);

    GameValue value = gstate->CreateGameValue(GameArray);
    GameArrayType &array = value;
    array.Add(GameValueExt(tgt));
    array.Add(GameValueExt(unit->GetPerson()));
    gstate->VarSetLocal("_this",value,true);
    gstate->Execute(desc.script, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    gstate->EndContext();
    return true;
  }
  return false;
}
#endif

bool Action::SetParams(const GameState *state, GameValuePar oper)
{
  // no additional parameters are expected
  const GameArrayType &array = oper;
  Assert(array.Size() >= 3);
  state->SetError(EvalDim, array.Size(), 2);
  return false;
}

Action *Action::CreateObject(ParamArchive &ar)
{
  UIActionType type;
  if (ar.SerializeEnum("type", type, 1, ATNone) != LSOK) return NULL;
  return CreateAction(type);
}

LSError Action::Serialize(ParamArchive &ar)
{
  if (ar.IsSaving())
  {
    CHECK(ar.SerializeEnum("type", _type, 1, ATNone))
  }
  CHECK(ar.SerializeRef("target", _target, 1))
  CHECK(ar.Serialize("fromGUI", _fromGUI, 1, false))
  CHECK(ar.Serialize("processed", _processed, 1, false))
  return LSOK;
}

Ref<Action> ActionBasic::Copy() const
{
  Action *action = new ActionBasic(_type, _target);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionBasic::GetParams(UIActionParams &params, AIBrain *unit) const
{
  switch (GetType())
  {
  case ATGetInCommander:
  case ATGetInDriver:
  case ATGetInPilot:
  case ATGetInGunner:
  case ATHeal:
  case ATHealSoldier:
  case ATFirstAid:
  case ATRepairVehicle:
  case ATRepair:
  case ATRefuel:
  case ATRearm:
  case ATMarkEntity:
#if _VBS2 // interact with vehs
  case ATInteractWithVehicle:
  case ATQuickEnterVehicle:
#endif
    // argument %1 - action target
    if (!GetTarget())
    {
      // regular option for commanding menu
      // RptF("Action %s - missing target", cc_cast(FindEnumName(GetType())));
      return false;
    }
    params.Add(CreateTextASCII(NULL, GetTarget()->GetShortName()));
    return true;
#if _VBS3 && _ENABLE_CARRY_BODY
  case ATCaptureUnit:
  case ATReleaseUnit:
  case ATHandcuff:
  case ATReleaseHandcuff:
  case ATCarryBody:
    {
      // argument %1 - action target
      if (!GetTarget())
      {
        // regular option for commanding menu
        // RptF("Action %s - missing target", cc_cast(FindEnumName(GetType())));
        return false;
      }
      RString name = GetTarget()->GetType(1)->GetDisplayName();
      Person* person = dyn_cast<Person>(GetTarget());
      if(person) name = name + RString(" (") + person->GetPersonName() + RString(")");

      params.Add(CreateTextASCII(NULL, name));
      return true;
    }
#endif
  case ATGetOut:
  case ATLightOn:
  case ATLightOff:
  case ATEngineOn:
  case ATEngineOff:
  case ATEject:
  case ATTakeFlag:
  case ATCancelTakeFlag:
  case ATCancelAction:
  case ATReturnFlag:
  case ATTurnIn:
  case ATTurnOut:
  case ATSitDown:
  case ATSalute:
  case ATHideBody:
  case ATNVGoggles:
  case ATNVGogglesOff:
  case ATIngameMenu:
  case ATMarkWeapon:
  case ATTeamSwitch:
  case ATGear:
  case ATGunLightOn:
  case ATGunLightOff:
  case ATIRLaserOn:
  case ATIRLaserOff:
#if _VBS2 //Convoy trainer
  case ATTogglePersonalItems:
#endif
#if _VBS3_WEAPON_ON_BACK
  case ATPutWeaponOnBack:
#endif
    // no parameters
    return true;
  case ATOpenBag:
    {
      // argument %1 - action target
      if (!GetTarget())
      {
        // regular option for commanding menu
        // RptF("Action %s - missing target", cc_cast(FindEnumName(GetType())));
        return false;
      }
      RString name = "";
      if(GetTarget()->GetBackpack()) name = GetTarget()->GetBackpack()->GetType()->GetDisplayName();
      else name = GetTarget()->GetType()->GetDisplayName();
      params.Add(CreateTextASCII(NULL, name));
      return true;
    }
  case ATDropBag:
  case ATTakeBag:
  case ATAddBag:
  case ATDisAssemble:
    {
      // argument %1 - action target
      if (!GetTarget())
      {
        // regular option for commanding menu
        // RptF("Action %s - missing target", cc_cast(FindEnumName(GetType())));
        return false;
      }
      RString name = GetTarget()->GetType()->GetDisplayName();
      params.Add(CreateTextASCII(NULL, name));
      return true;
    }
  }
  return base::GetParams(params, unit);
}

bool CheckSupply(EntityAI *vehicle, EntityAI *parent, SupportCheckF check, float limit, bool now);

/// Functor checking if crew member is enemy to given unit
class IsCrewEnemyInside : public ICrewFunc
{
protected:
  AIBrain *_unit;

public:
  IsCrewEnemyInside(AIBrain *unit) : _unit(unit) {}
  virtual bool operator () (Person *entity)
  {
    return entity && _unit->IsEnemy(entity->GetTargetSide()); // true -> break enumeration and return true
  }
};

class IsCrewEmpty : public ICrewFunc
{
protected:
  int  *_count;

public:
  IsCrewEmpty(int &count) {_count = &count;}

  virtual bool operator () (Person *person)
  {
    if(person) return true; // continue
    return false;
  }
};

bool ActionBasic::Process(AIBrain *unit) const
{
  TargetType *veh = GetTarget();
  switch (GetType())
  {
  case ATGetInCommander:
  case ATGetInDriver:
  case ATGetInPilot:
  case ATGetInGunner:
    {
      if (!unit->IsFreeSoldier()) return false;
      Transport *trans = dyn_cast<Transport>(veh);
      if (!trans) return false;
      if (unit->IsPlayer() && trans->GetLock() == LSLocked) return false;
      
      // avoid get in enemy vehicle
      IsCrewEnemyInside func(unit);
      if (trans->ForEachCrew(func)) return false;
      
      RString getInAction;
      switch (GetType())
      {
      case ATGetInCommander:
        if (!trans->GetGroupAssigned() && unit->GetGroup())
          unit->GetGroup()->AddVehicle(trans);
        unit->AssignAsCommander(trans);
        {
          TurretContext context;
          if (trans->GetPrimaryObserverTurret(context) && context._turretType)
            getInAction = context._turretType->GetGunnerGetInAction();
        }
        break;
      case ATGetInDriver:
      case ATGetInPilot:
        if (!trans->GetGroupAssigned() && unit->GetGroup())
          unit->GetGroup()->AddVehicle(trans);
        unit->AssignAsDriver(trans);
        getInAction = trans->Type()->GetDriverGetInAction();
        break;
      case ATGetInGunner:
        if (!trans->GetGroupAssigned() && unit->GetGroup())
          unit->GetGroup()->AddVehicle(trans);
        unit->AssignAsGunner(trans);
        {
          TurretContext context;
          if (trans->GetPrimaryGunnerTurret(context) && context._turretType)
            getInAction = context._turretType->GetGunnerGetInAction();
        }
        break;
      }
      unit->AllowGetIn(true);
      unit->OrderGetIn(true);
      DoAssert(unit->VehicleAssigned() == trans);
      void MoveToGetInPos(AIBrain *unit, Transport &veh, const Vector3& shift);
      Vector3 shift = unit->GetPerson()->GetActionMoveShift(getInAction);
      MoveToGetInPos(unit, *trans, shift);
      // Disable dammage see Man.Simulate
#define GET_IN_TIME 5.0f
      unit->GetPerson()->DisableDamageFromObj(GET_IN_TIME); 

      unit->GetPerson()->CreateActivityGetIn(trans, GetType());
    }
    return true;
  case ATTakeFlag:
    if (veh && unit->IsFreeSoldier() && !unit->GetPerson()->GetFlagCarrier())
    {
      Person *person = dyn_cast<Person>(veh);
      EntityAI *flag = person ? person->GetFlagCarrier() : veh;
      // test conditions once more (action can be launched through procedure)
      if
      (
        flag &&
        (!person && !flag->GetFlagOwner() || flag->GetFlagOwner() == person && !person->IsAbleToMove()) &&
        unit->IsEnemy(flag->GetFlagSide()) &&
        CheckSupply(unit->GetPerson(), veh, NULL, 0, true)
      )
      {
        if (flag->IsLocal())
          flag->SetFlagOwner(unit->GetPerson());
        else
          GetNetworkManager().SetFlagOwner(unit->GetPerson(), flag);
        return true;
      }
    }
    return false;
  case ATCancelTakeFlag:
    if (veh && unit->IsFreeSoldier())
    {
      Person *person = dyn_cast<Person>(veh);
      EntityAI *flag = person ? person->GetFlagCarrier() : veh;
      if (flag->IsLocal())
        flag->CancelTakeFlag();
      else
        GetNetworkManager().CancelTakeFlag(flag);
      unit->GetPerson()->PutDownEnd();
      return true;
    }
    return false;
  case ATReturnFlag:
    if (veh && unit->IsFreeSoldier())
    {
      Person *person = dyn_cast<Person>(veh);
      EntityAI *flag = person ? person->GetFlagCarrier() : veh;
      // test conditions once more (action can be launched through procedure)
      if
      (
        flag && person &&
        (flag->GetFlagOwner() == person && !person->IsAbleToMove()) &&
        unit->IsFriendly(flag->GetFlagSide()) &&
        CheckSupply(unit->GetPerson(), veh, NULL, 0, true)
      )
      {
        if (flag->IsLocal())
          flag->SetFlagOwner(NULL);
        else
          GetNetworkManager().SetFlagOwner(NULL, flag);
        return true;
      }
    }
    return false;
  case ATNVGoggles:
    {
      Person *person = unit->GetPerson();
      person->SetNVWanted(true);
    }
    return true;
  case ATNVGogglesOff:
    {
      Person *person = unit->GetPerson();
      person->SetNVWanted(false);
    }
    return true;
  case ATIngameMenu:
    {
      AbstractUI *ui = GWorld->UI();
      if (ui) ui->LaunchMenu();
    }
    return true;
  case ATMarkEntity:
  case ATMarkWeapon:
    bool EnableMarkEntities();
    if (veh && EnableMarkEntities())
    {
      void CampaignMarkEntity(EntityAI *entity);
      CampaignMarkEntity(veh);
    }
    return true;
  case ATTeamSwitch:
    AbstractOptionsUI *CreateTeamSwitchDialog(Person *player, EntityAI *killer, bool respawn, bool userDialog);
    GWorld->DestroyUserDialog();
    GWorld->SetUserDialog(CreateTeamSwitchDialog(unit->GetPerson(), NULL, false, true));
    return true;
  }

  return base::Process(unit);
}

DEFINE_FAST_ALLOCATOR(ActionBasic)

Ref<Action> ActionTurret::Copy() const
{
  Action *action = new ActionTurret(_type, _target, _turret);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionTurret::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionTurret *actionTyped = static_cast<const ActionTurret *>(to);
  return GetTurret() == actionTyped->GetTurret();
}

int ActionTurret::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionTurret *actionTyped = static_cast<const ActionTurret *>(with);
  s = (int)GetTurret() - (int)actionTyped->GetTurret();
  return s;
}

bool ActionTurret::GetParams(UIActionParams &params, AIBrain *unit) const
{
  switch (GetType())
  {
  case ATGetInTurret:
  case ATMoveToTurret:
    if (!GetTarget())
    {
      // regular option for commanding menu
      // RptF("Action %s - missing target", cc_cast(FindEnumName(GetType())));
      return false;
    }
    if (!GetTurret())
    {
      RptF("Action %s - missing turret", cc_cast(FindEnumName(GetType())));
      return false;
    }
    else
    {
      Transport *vehicle = dyn_cast<Transport>(GetTarget());
      TurretContext context; // need access to turret type
      if (!vehicle->FindTurret(GetTurret(), context))
      {
        RptF("Action %s - wrong turret", cc_cast(FindEnumName(GetType())));
        return false;
      }
      params.Add(CreateTextASCII(NULL, vehicle->GetShortName()));
      params.Add(CreateTextASCII(NULL, context._turretType->_gunnerName));
      return true;
    }
  }

  return base::GetParams(params, unit);
}

bool ActionTurret::Process(AIBrain *unit) const
{
  TargetType *veh = GetTarget();
  switch (GetType())
  {
  case ATGetInTurret:
    {
      if (!unit->IsFreeSoldier()) return false;
      Transport *trans = dyn_cast<Transport>(veh);
      if (!trans) return false;
      Turret *turret = GetTurret();
      if (!turret) return false;
      if (unit->IsPlayer() && trans->GetLock() == LSLocked) return false;

      // avoid get in enemy vehicle
      IsCrewEnemyInside func(unit);
      if (trans->ForEachCrew(func)) return false;

      if (!trans->GetGroupAssigned() && unit->GetGroup())
        unit->GetGroup()->AddVehicle(trans);
      unit->AssignToTurret(trans, turret);
      unit->AllowGetIn(true);
      unit->OrderGetIn(true);
      DoAssert(unit->VehicleAssigned() == trans);
      TurretContext context;
      if (trans->FindTurret(unit->GetPerson(), context) && context._turretType)
      {
        Vector3 shift = unit->GetPerson()->GetActionMoveShift(context._turretType->GetGunnerGetInAction());
        void MoveToGetInPos(AIBrain *unit, Transport &veh, const Vector3 &shift);
        MoveToGetInPos(unit, *trans, shift);
      }
      // Disable damage see Man.Simulate
#define GET_IN_TIME 5.0f
      unit->GetPerson()->DisableDamageFromObj(GET_IN_TIME); 

      unit->GetPerson()->CreateActivityGetIn(trans, GetType(), turret);
    }
    return true;
  }

  return base::Process(unit);
}

bool ActionTurret::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 3)
  {
    // path to turret
    if (array[2].GetType() != GameArray)
    {
      state->TypeError(GameArray, array[2].GetType());
      return false;
    }
    const GameArrayType &subarray = array[2];
    int n = subarray.Size();
    for (int i=0; i<n; i++)
    {
      if (subarray[i].GetType() != GameScalar)
      {
        state->TypeError(GameScalar, subarray[i].GetType());
        return false;
      }
    }

    Transport *vehicle = dyn_cast<Transport>(GetTarget());
    if (!vehicle) return false;

    AutoArray<int, MemAllocLocal<int, 16> > path;
    path.Resize(n);
    for (int i=0; i<n; i++) path[i] = toInt((float)subarray[i]);
    _turret = vehicle->GetTurret(path.Data(), path.Size());
    return _turret != NULL;
  }

  state->SetError(EvalDim, n, 3);
  return false;
}

LSError ActionTurret::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeRef("Turret", _turret, 1))
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionTurret)

Ref<Action> ActionObject::Copy() const
{
  Action *action = new ActionObject(_type, _target, _object);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionObject::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionObject *actionTyped = static_cast<const ActionObject *>(to);
  return GetObject() == actionTyped->GetObject();
}

int ActionObject::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionObject *actionTyped = static_cast<const ActionObject *>(with);
  s = (int)GetObject() - (int)actionTyped->GetObject();
  return s;
}

bool ActionObject::GetParams(UIActionParams &params, AIBrain *unit) const
{
  return base::GetParams(params, unit);
}

bool ActionObject::Process(AIBrain *unit) const
{
  return base::Process(unit);
}

bool ActionObject::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 3)
  {
    if (array[2].GetType() != GameObject)
    {
      state->TypeError(GameObject, array[2].GetType());
      return false;
    }
    Object *GetObject(GameValuePar oper);
    _object = GetObject(array[2]);
    return true;
  }

  state->SetError(EvalDim, n, 3);
  return false;
}

LSError ActionObject::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeRef("Object", _object, 1))
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionObject)

Ref<Action> ActionWeapon::Copy() const
{
  Action *action = new ActionWeapon(_type, _target, _weapon, _backpack);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionWeapon::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionWeapon *actionTyped = static_cast<const ActionWeapon *>(to);
  if (_backpack!=actionTyped->_backpack) return false;
  return stricmp(GetWeapon(), actionTyped->GetWeapon()) == 0;
}

int ActionWeapon::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionWeapon *actionTyped = static_cast<const ActionWeapon *>(with);
  s = _backpack-actionTyped->_backpack;
  if (s) return s;
  s = stricmp(GetWeapon(), actionTyped->GetWeapon());
  return s;
}

bool ActionWeapon::GetParams(UIActionParams &params, AIBrain *unit) const
{
  switch (GetType())
  {
  case ATDropWeapon:
    {
      Ref<WeaponType> weapon = WeaponTypes.New(GetWeapon());
      Assert(weapon);
      params.Add(CreateTextASCII(NULL, weapon->GetDisplayName()));
      return true;
    }
  case ATPutWeapon:
    if (!GetTarget())
    {
      // regular option for commanding menu
      // RptF("Action %s - missing target", cc_cast(FindEnumName(GetType())));
      return false;
    }
    else
    {
      Ref<WeaponType> weapon = WeaponTypes.New(GetWeapon());
      Assert(weapon);
      params.Add(CreateTextASCII(NULL, weapon->GetDisplayName()));
      params.Add(CreateTextASCII(NULL, GetTarget()->GetShortName()));
      return true;
    }
  case ATPutBag:
  case ATAssemble:
    {
      params.Add(CreateTextASCII(NULL, GetWeapon()));
      return true;
    }
  }

  return base::GetParams(params, unit);
}

bool ActionWeapon::Process(AIBrain *unit) const
{
  TargetType *veh = GetTarget();
  switch (GetType())
  {
  case ATDropWeapon:
  case ATPutWeapon:
    if (veh && veh != unit->GetPerson())
    {
      veh->PerformAction(this, unit);
      return true;
    }
    else //if (unit->IsFreeSoldier())
    {
      // find weapon    
      Ref<WeaponType> weapon = WeaponTypes.New(GetWeapon());
      if (!weapon || !weapon->_canDrop) return false;

      Person *person = unit->GetPerson();
      if (person->FindWeapon(weapon))
      {
        // create container
        Ref<EntityAI> container = GWorld->NewVehicleWithID("WeaponHolder");
/*
        int slots = weapon->_weaponType;
        if ((slots & MaskSlotSecondary) != 0 && (slots & MaskSlotPrimary) == 0)
          container = GWorld->NewVehicleWithID("SecondaryWeaponHolder");
        else
          container = GWorld->NewVehicleWithID("WeaponHolder");
*/
        if (!container) return false;

        Vector3 pos = person->FutureVisualState().Position() + 0.5f * person->FutureVisualState().Direction() + VUp*0.5f;
        Matrix3 dir;
        dir.SetUpAndDirection(VUp, person->FutureVisualState().Direction());
        Matrix4 transform;
        transform.SetPosition(pos);
        transform.SetOrientation(dir);

        container->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
        container->SetTransform(transform);
        container->Init(transform, true);
        container->OnEvent(EEInit);

        // add container to world
        GWorld->AddSlowVehicle(container);
        if (GWorld->GetMode() == GModeNetware)
          GetNetworkManager().CreateVehicle(container, VLTSlowVehicle, "", -1);
        unit->AddTarget(container, 4.0f, 4.0f, 0);

        // remove weapon
        person->RemoveWeapon(weapon);
        if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == person)
          GWorld->UI()->ResetVehicle(person);

        container->AddWeaponCargo(weapon, 1, false);

        // remove unusable magazines
        for (int i=0; i<person->NMagazines();)
        {
          Ref<Magazine> magazine = person->GetMagazine(i);
          if (!magazine || !weapon->IsMagazineUsableInWeapon(magazine->_type))
          {
            i++;
            continue;
          }
          person->RemoveMagazine(magazine);
          if (magazine->GetAmmo() > 0) container->AddMagazineCargo(magazine, false);
        }
        void UpdateWeaponsInBriefing();
        UpdateWeaponsInBriefing();
        return true;
      }
      return false;
    }
  }

  return base::Process(unit);
}

bool ActionWeapon::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 3)
  {
    if (array[2].GetType() != GameString)
    {
      state->TypeError(GameString, array[2].GetType());
      return false;
    }
    _weapon = array[2];
    return true;
  }

  state->SetError(EvalDim, n, 3);
  return false;
}

LSError ActionWeapon::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.Serialize("weapon", _weapon, 1, RString()))
  CHECK(ar.Serialize("backpack", _backpack, 1, false))
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionWeapon)

Ref<Action> ActionMagazineType::Copy() const
{
  Action *action = new ActionMagazineType(_type, _target, _magazineType, _backpack);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionMagazineType::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionMagazineType *actionTyped = static_cast<const ActionMagazineType *>(to);
  if (_backpack!=actionTyped->_backpack) return false;
  return stricmp(GetMagazineType(), actionTyped->GetMagazineType()) == 0;
}

int ActionMagazineType::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionMagazineType *actionTyped = static_cast<const ActionMagazineType *>(with);
  s = _backpack-actionTyped->_backpack;
  if (s) return s;
  s = stricmp(GetMagazineType(), actionTyped->GetMagazineType());
  return s;
}

bool ActionMagazineType::GetParams(UIActionParams &params, AIBrain *unit) const
{
  switch (GetType())
  {
  case ATDropMagazine:
    {
      Ref<MagazineType> type = MagazineTypes.New(GetMagazineType());
      Assert(type);
      params.Add(CreateTextASCII(NULL, type->GetDisplayName()));
      return true;
    }
  case ATPutMagazine:
    if (!GetTarget())
    {
      // regular option for commanding menu
      // RptF("Action %s - missing target", cc_cast(FindEnumName(GetType())));
      return false;
    }
    else
    {
      Ref<MagazineType> type = MagazineTypes.New(GetMagazineType());
      Assert(type);
      params.Add(CreateTextASCII(NULL, type->GetDisplayName()));
      params.Add(CreateTextASCII(NULL, GetTarget()->GetShortName()));
      return true;
    }
  }

  return base::GetParams(params, unit);
}

bool ActionMagazineType::Process(AIBrain *unit) const
{
  TargetType *veh = GetTarget();
  switch (GetType())
  {
  case ATDropMagazine:
  case ATPutMagazine:
    if (veh && veh != unit->GetPerson())
    {
      veh->PerformAction(this, unit);
      return true;
    }
    else // if (unit->IsFreeSoldier())
    {
      Person *person = unit->GetPerson();

      // find magazine
      Ref<MagazineType> type = MagazineTypes.New(GetMagazineType());
      if (!type) return false;

      Ref<const Magazine> magazine;
      int minCount = INT_MAX;
      // find in nonused magazines
      for (int i=0; i<person->NMagazines(); i++)
      {
        const Magazine *m = person->GetMagazine(i);
        if (!m) continue;
        if (m->_type != type) continue;
        if (person->IsMagazineUsed(m)) continue;
        if (m->GetAmmo() < minCount)
        {
          magazine = m;
          minCount = m->GetAmmo(); 
        }
      }
      // find in all magazines
      if (!magazine) for (int i=0; i<person->NMagazines(); i++)
      {
        const Magazine *m = person->GetMagazine(i);
        if (!m) continue;
        if (m->_type != type) continue;
        if (m->GetAmmo() < minCount)
        {
          magazine = m;
          minCount = m->GetAmmo(); 
        }
      }

      if (magazine)
      {
        // remove magazine
        person->RemoveMagazine(magazine);
        if (minCount > 0)
        {
          // create container
          Ref<EntityAI> container = GWorld->NewVehicleWithID("WeaponHolder");
          if (!container) return false;

          Vector3 pos = person->FutureVisualState().Position() + 0.5f * person->FutureVisualState().Direction() + VUp*0.5f;
          Matrix3 dir;
          dir.SetUpAndDirection(VUp, person->FutureVisualState().Direction());
          Matrix4 transform;
          transform.SetPosition(pos);
          transform.SetOrientation(dir);

          container->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
          container->SetTransform(transform);
          container->Init(transform, true);
          container->OnEvent(EEInit);

          // add container to world
          GWorld->AddSlowVehicle(container);
          if (GWorld->GetMode() == GModeNetware)
            GetNetworkManager().CreateVehicle(container, VLTSlowVehicle, "", -1);
          unit->AddTarget(container, 4.0f, 4.0f, 0);

          // add magazine to container
          container->AddMagazineCargo(const_cast<Magazine *>(magazine.GetRef()), false);
        }
        void UpdateWeaponsInBriefing();
        UpdateWeaponsInBriefing();
        return true;
      }
      return false;
    }
  }

  return base::Process(unit);
}

bool ActionMagazineType::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 3)
  {
    if (array[2].GetType() != GameString)
    {
      state->TypeError(GameString, array[2].GetType());
      return false;
    }
    _magazineType = array[2];
    return true;
  }

  state->SetError(EvalDim, n, 3);
  return false;
}

LSError ActionMagazineType::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.Serialize("magazineType", _magazineType, 1, RString()))
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionMagazineType)

Ref<Action> ActionWeaponIndex::Copy() const
{
  Action *action = new ActionWeaponIndex(_type, _target, _gunner, _index);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionWeaponIndex::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionWeaponIndex *actionTyped = static_cast<const ActionWeaponIndex *>(to);
  return GetGunner() == actionTyped->GetGunner() && GetWeaponIndex() == actionTyped->GetWeaponIndex();
}

int ActionWeaponIndex::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionWeaponIndex *actionTyped = static_cast<const ActionWeaponIndex *>(with);
  s = (int)GetGunner() - (int)actionTyped->GetGunner();
  if (s != 0) return s;
  s = GetWeaponIndex() - actionTyped->GetWeaponIndex();
  return s;
}

bool ActionWeaponIndex::GetParams(UIActionParams &params, AIBrain *unit) const
{
  return base::GetParams(params, unit);
}

bool ActionWeaponIndex::Process(AIBrain *unit) const
{
  return base::Process(unit);
}

bool ActionWeaponIndex::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 4)
  {
    if (array[2].GetType() != GameObject)
    {
      state->TypeError(GameObject, array[2].GetType());
      return false;
    }
    Object *obj = static_cast<GameDataObject *>(array[2].GetData())->GetObject();
    _gunner = dyn_cast<Person>(obj);

    if (array[3].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[3].GetType());
      return false;
    }
    _index = toInt(safe_cast<float>(array[3]));
    return true;
  }

  state->SetError(EvalDim, n, 4);
  return false;
}

LSError ActionWeaponIndex::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeRef("gunner", _gunner, 1))
  CHECK(ar.Serialize("index", _index, 1, -1))
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionWeaponIndex)

Ref<Action> ActionIndex::Copy() const
{
  Action *action = new ActionIndex(_type, _target, _index);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionIndex::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionIndex *actionTyped = static_cast<const ActionIndex *>(to);
  return GetIndex() == actionTyped->GetIndex();
}

int ActionIndex::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionIndex *actionTyped = static_cast<const ActionIndex *>(with);
  s = GetIndex() - actionTyped->GetIndex();
  return s;
}

bool ActionIndex::GetParams(UIActionParams &params, AIBrain *unit) const
{
  switch (GetType())
  {
  case ATGetInCargo:
    // argument %1 - action target
    if (!GetTarget())
    {
      // regular option for commanding menu
      // RptF("Action %s - missing target", cc_cast(FindEnumName(GetType())));
      return false;
    }
    params.Add(CreateTextASCII(NULL, GetTarget()->GetShortName()));
    return true;
  }

  return base::GetParams(params, unit);
}

bool ActionIndex::Process(AIBrain *unit) const
{
  switch (GetType())
  {
  case ATGetInCargo:
    {
      if (!unit->IsFreeSoldier()) return false;
      Transport *trans = dyn_cast<Transport>(GetTarget());
      if (!trans) return false;
      if (unit->IsPlayer() && trans->GetLock() == LSLocked) return false;

      // avoid get in enemy vehicle
      IsCrewEnemyInside func(unit);
      if (trans->ForEachCrew(func)) return false;

      unit->AssignAsCargo(trans, _index, true);

      unit->AllowGetIn(true);
      unit->OrderGetIn(true);
      DoAssert(unit->VehicleAssigned() == trans);
      void MoveToGetInPos(AIBrain *unit, Transport &veh, const Vector3& shift);
      Vector3 shift = unit->GetPerson()->GetActionMoveShift(trans->Type()->GetCargoGetInAction(_index));
      MoveToGetInPos(unit, *trans, shift);
      // Disable dammage see Man.Simulate
#define GET_IN_TIME 5.0f
      unit->GetPerson()->DisableDamageFromObj(GET_IN_TIME); 

      unit->GetPerson()->CreateActivityGetIn(trans, GetType(), NULL, _index);
    }
    return true;
  }
  return base::Process(unit);
}

bool ActionIndex::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 3)
  {
    if (array[2].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[2].GetType());
      return false;
    }
    _index = toInt(safe_cast<float>(array[2]));
    return true;
  }

  state->SetError(EvalDim, n, 3);
  return false;
}

LSError ActionIndex::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.Serialize("index", _index, 1, -1))
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionIndex)

Ref<Action> ActionMagazine::Copy() const
{
  Action *action = new ActionMagazine(_type, _target, _gunner, _creator, _id);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionMagazine::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionMagazine *actionTyped = static_cast<const ActionMagazine *>(to);
  return GetGunner() == actionTyped->GetGunner() &&
    GetCreator() == actionTyped->GetCreator() && GetId() == actionTyped->GetId();
}

int ActionMagazine::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionMagazine *actionTyped = static_cast<const ActionMagazine *>(with);
  s = (int)GetGunner() - (int)actionTyped->GetGunner();
  if (s != 0) return s;
  s = GetCreator().CreatorInt() - actionTyped->GetCreator().CreatorInt();
  if (s != 0) return s;
  s = GetId() - actionTyped->GetId();
  return s;
}

bool ActionMagazine::GetParams(UIActionParams &params, AIBrain *unit) const
{
  return base::GetParams(params, unit);
}

bool ActionMagazine::Process(AIBrain *unit) const
{
  return base::Process(unit);
}

bool ActionMagazine::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 5)
  {
    if (array[2].GetType() != GameObject)
    {
      state->TypeError(GameObject, array[2].GetType());
      return false;
    }
    Object *obj = static_cast<GameDataObject *>(array[2].GetData())->GetObject();
    _gunner = dyn_cast<Person>(obj);
    if (array[3].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[3].GetType());
      return false;
    }
    _creator.SetCreator( toInt(safe_cast<float>(array[3])) );
    if (array[4].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[4].GetType());
      return false;
    }
    _id = toInt(safe_cast<float>(array[4]));
    return true;
  }

  state->SetError(EvalDim, n, 5);
  return false;
}

LSError ActionMagazine::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeRef("gunner", _gunner, 1))
  CHECK(_creator.Serialize(ar, "creator", 1, -1))
  CHECK(ar.Serialize("id", _id, 1, -1))
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionMagazine)

static MuzzleType *FindMuzzle(WeaponType *weapon, RString muzzle)
{
  if (weapon)
  {
    for (int i=0; i<weapon->_muzzles.Size(); i++)
    {
      if (stricmp(weapon->_muzzles[i]->GetName(), muzzle) == 0)
        return weapon->_muzzles[i];
    }
  }
  return NULL;
}

Ref<Action> ActionMagazineAndMuzzle::Copy() const
{
  Action *action = new ActionMagazineAndMuzzle(_type, _target, _gunner, _creator, _id, _weapon, _muzzle);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionMagazineAndMuzzle::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionMagazineAndMuzzle *actionTyped = static_cast<const ActionMagazineAndMuzzle *>(to);
  return GetWeapon() == actionTyped->GetWeapon() && GetMuzzle() == actionTyped->GetMuzzle();
}

int ActionMagazineAndMuzzle::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionMagazineAndMuzzle *actionTyped = static_cast<const ActionMagazineAndMuzzle *>(with);
  if (GetWeapon()->GetName() != actionTyped->GetWeapon()->GetName())
    return stricmp(GetWeapon()->GetName(), actionTyped->GetWeapon()->GetName());
  if (GetMuzzle()->GetName() != actionTyped->GetMuzzle()->GetName())
    return stricmp(GetMuzzle()->GetName(), actionTyped->GetMuzzle()->GetName());
  return 0;
}

bool ActionMagazineAndMuzzle::GetParams(UIActionParams &params, AIBrain *unit) const
{
  return base::GetParams(params, unit);
}

bool ActionMagazineAndMuzzle::Process(AIBrain *unit) const
{
  return base::Process(unit);
}

bool ActionMagazineAndMuzzle::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 7)
  {
    if (array[2].GetType() != GameObject)
    {
      state->TypeError(GameObject, array[2].GetType());
      return false;
    }
    Object *obj = static_cast<GameDataObject *>(array[2].GetData())->GetObject();
    _gunner = dyn_cast<Person>(obj);
    if (array[3].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[3].GetType());
      return false;
    }
    if (array[4].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[4].GetType());
      return false;
    }
    if (array[5].GetType() != GameString)
    {
      state->TypeError(GameString, array[5].GetType());
      return false;
    }
    if (array[6].GetType() != GameString)
    {
      state->TypeError(GameString, array[6].GetType());
      return false;
    }
    _creator.SetCreator( toInt(safe_cast<float>(array[3])) );
    _id = toInt(safe_cast<float>(array[4]));
    RString weapon = array[5];
    RString muzzle = array[6];
    _weapon = weapon.GetLength() > 0 ? WeaponTypes.New(weapon) : 0;
    _muzzle = FindMuzzle(_weapon, muzzle);
    return true;
  }

  state->SetError(EvalDim, n, 7);
  return false;
}

LSError ActionMagazineAndMuzzle::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  if (ar.IsSaving())
  {
    RString weapon = _weapon ? cc_cast(_weapon->GetName()) : "";
    RString muzzle = _muzzle ? cc_cast(_muzzle->GetName()) : "";
    CHECK(ar.Serialize("weapon", weapon, 1, RString()))
    CHECK(ar.Serialize("muzzle", muzzle, 1, RString()))
  }
  else if (ar.GetPass() == ParamArchive::PassFirst)
  {
    RString weapon;
    RString muzzle;
    CHECK(ar.Serialize("weapon", weapon, 1, RString()))
    CHECK(ar.Serialize("muzzle", muzzle, 1, RString()))
    _weapon = weapon.GetLength() > 0 ? WeaponTypes.New(weapon) : 0;
    _muzzle = FindMuzzle(_weapon, muzzle);
  }
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionMagazineAndMuzzle)

Ref<Action> ActionLadder::Copy() const
{
  Action *action = new ActionLadder(_type, _target, _index);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionLadder::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionLadder *actionTyped = static_cast<const ActionLadder *>(to);
  return GetLadder() == actionTyped->GetLadder();
}

int ActionLadder::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionLadder *actionTyped = static_cast<const ActionLadder *>(with);
  s = GetLadder() - actionTyped->GetLadder();
  return s;
}

bool ActionLadder::GetParams(UIActionParams &params, AIBrain *unit) const
{
  return base::GetParams(params, unit);
}

bool ActionLadder::Process(AIBrain *unit) const
{
  return base::Process(unit);
}

bool ActionLadder::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 3)
  {
    if (array[2].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[2].GetType());
      return false;
    }
    _index = toInt(safe_cast<float>(array[2]));
    return true;
  }

  state->SetError(EvalDim, n, 3);
  return false;
}

LSError ActionLadder::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.Serialize("index", _index, 1, -1))
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionLadder)

Ref<Action> ActionLadderPos::Copy() const
{
  Action *action = new ActionLadderPos(_type, _target, _index, _pos);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionLadderPos::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionLadderPos *actionTyped = static_cast<const ActionLadderPos *>(to);
  return GetLadderPos() == actionTyped->GetLadderPos();
}

int ActionLadderPos::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionLadderPos *actionTyped = static_cast<const ActionLadderPos *>(with);
  s = GetLadderPos() - actionTyped->GetLadderPos();
  return s;
}

bool ActionLadderPos::GetParams(UIActionParams &params, AIBrain *unit) const
{
  return base::GetParams(params, unit);
}

bool ActionLadderPos::Process(AIBrain *unit) const
{
  return base::Process(unit);
}

bool ActionLadderPos::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 4)
  {
    if (array[2].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[2].GetType());
      return false;
    }
    if (array[3].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[3].GetType());
      return false;
    }
    _index = toInt(safe_cast<float>(array[2]));
    _pos = toInt(safe_cast<float>(array[3]));
    return true;
  }

  state->SetError(EvalDim, n, 4);
  return false;
}

LSError ActionLadderPos::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.Serialize("pos", _pos, 1, -1))
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionLadderPos)

#if _ENABLE_CONVERSATION

Ref<Action> ActionTell::Copy() const
{
  Action *action = new ActionTell(_type, _target, _text, _message);
  action->SetFromGUI(_fromGUI);
  return action;
}

bool ActionTell::HasEqualType(const Action *to) const
{
  if (!base::HasEqualType(to)) return false;
  const ActionTell *actionTyped = static_cast<const ActionTell *>(to);
  return strcmp(GetText(), actionTyped->GetText()) == 0;
}

int ActionTell::Compare(const Action *with) const
{
  int s = base::Compare(with);
  if (s != 0) return s;
  const ActionTell *actionTyped = static_cast<const ActionTell *>(with);
  return strcmp(GetText(), actionTyped->GetText());
}

bool ActionTell::GetParams(UIActionParams &params, AIBrain *unit) const
{
  return base::GetParams(params, unit);
}

bool ActionTell::Process(AIBrain *unit) const
{
  return base::Process(unit);
}

bool ActionTell::SetParams(const GameState *state, GameValuePar oper)
{
  const GameArrayType &array = oper;
  int n = array.Size();
  if (n == 6)
  {
    // _text
    if (array[2].GetType() == GameString)
    {
      _text = array[2];
    }
    else if (array[2].GetType() == GameArray)
    {
      const GameArrayType &textDesc = array[2];
      if (textDesc.Size() == 0)
      {
        state->SetError(EvalDim, textDesc.Size(), 2);
        return false;
      }
      if (textDesc[0].GetType() != GameString)
      {
        state->TypeError(GameString, textDesc[0].GetType());
        return false;
      }
      _text = textDesc[0];
    }
    else
    {
      state->TypeError(GameString | GameArray, array[2].GetType());
      return false;
    }
    // _message
    if (array[3].GetType() != GameString)
    {
      state->TypeError(GameString, array[3].GetType());
      return false;
    }
    if (array[4].GetType() != GameString)
    {
      state->TypeError(GameString, array[4].GetType());
      return false;
    }
    if (array[5].GetType() != GameArray)
    {
      state->TypeError(GameArray, array[5].GetType());
      return false;
    }
    // TODO: access to askingUnit
    /*
    _message = askingUnit->CreateKBMessage(array[3], array[4]);
    // attributes
    const GameArrayType &attrArray = array[5];
    for (int i=0; i<attrArray.Size(); i++)
    {
      // TODO: check the format of arguments
      askingUnit->SetKBMessageArgument(_message, attrArray[i]);
    }
    return true;
    */
    return false;
  }

  state->SetError(EvalDim, n, 6);
  return false;
}

LSError ActionTell::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.Serialize("text", _text, 1, RString()))
  CHECK(ar.Serialize("message", _message, 1))
  return LSOK;
}

DEFINE_FAST_ALLOCATOR(ActionTell)

#endif

UIActionTypeDesc::UIActionTypeDesc()
{
  priority = 0;
  show = true;
  showWindow = false;
  hideOnUse = true;
  shortcut = (UserAction)-1;
}

void UIActionTypeDesc::Load(ParamEntryPar cls)
{
  priority = cls >> "priority";
  show = cls >> "show";
  showWindow = cls >> "showWindow";
  hideOnUse = cls >> "hideOnUse";
  shortcut = (UserAction)-1;
  RString name = cls >> "shortcut";
  if (name.GetLength() > 0) 
  {
    for (int i=0; i<UAN; i++)
    {
      if (stricmp(name, Input::userActionDesc[i].name) == 0)
      {
        shortcut = (UserAction)i;
        break;
      }
    }
  }
  text = cls >> "text";
  textDefault = cls >> "textDefault";
  if (textDefault.GetLength() == 0) textDefault = text;
  textSimple = cls >> "textSimple";
  if (textSimple.GetLength() == 0) textSimple = text;

#if _VBS3 //support for scripted actions
  if(cls.FindEntry("statement"))
    script = cls >> "statement";
#endif
}

UIActionTypeDesc UIActions::actionTypes[NUIActionType];

void UIActions::Load(ParamEntryPar cls)
{
  for (int i=0; i<NUIActionType; i++)
  {
    RString name = FindEnumName((UIActionType)i);
    actionTypes[i].Load(cls >> name);
  }
}

UIAction::UIAction()
: position(-FLT_MAX, -FLT_MAX, -FLT_MAX)
{
  priority = 0;
  show = true;
  showWindow = false;
  hideOnUse = true;
  highlight = false;
  shortcut = (UserAction)-1;
  created = Glob.uiTime;
}

UIAction::UIAction(Action *act)
: position(-FLT_MAX, -FLT_MAX, -FLT_MAX)
{
  action = act;
  highlight = false;
  created = Glob.uiTime;

  // read defaults from type description
  UIActionType type = act->GetType();
  const UIActionTypeDesc &desc = UIActions::actionTypes[type];
  
  priority = desc.priority;
  show = desc.show;
  showWindow = desc.showWindow;
  hideOnUse = desc.hideOnUse;
  shortcut = desc.shortcut;
}

/// check if we want to process actions for given unit
static bool CanProcessActions(AIBrain *unit)
{
  // check the unit existence and life state
  if (!unit) return false;
  if (!unit->LSCanProcessActions()) return false;

  // check if person is not attached
  Person *person = unit->GetPerson();
  if (!person) return false;
  if (person->IsAttached()) return false;

  return true;
}

/*!
\patch 1.21 Date 08/20/2001 by Jirka
- Fixed: better processing of conflict in "Take Flag" and "Return Flag" actions
\patch 1.78 Date 7/22/2002 by Jirka
- Fixed: Behaviour of AI in Capture the flag or Flag Fight missions
(sometimes flag was placed on wrong unit)
*/

void UIAction::Process(AIBrain *unit) const
{
  if (!CanProcessActions(unit)) return;

  if (action) action->Process(unit);
}

static const float dimTime = 10.0f;
static const float fadeTime = dimTime + 2.0f;
static const float protectionTime = 0.5f;
static const float fadeCoef = 1.0f / (fadeTime - dimTime);

UIActionsDrawable::UIActionsDrawable()
{
  // action menu parameters
  ParamEntryVal cls = Pars >> "CfgInGameUI" >> "Actions";
  _font = GEngine->LoadFont(GetFontID(cls >> "font"));
  _size = cls >> "size";

  // actions menu position
  ConstParamEntryPtr entry = cls.FindEntry("align");
  if (entry)
  {
    _align = *entry;
    _x = cls >> "x";
    _y = cls >> "y";
  }
  else
  {
    _align = ST_RIGHT | ST_DOWN;
    _x = cls >> "right";
    _y = cls >> "bottom";
  }
  
  _rows = cls >> "rows";

  _bgColor = GetPackedColor(cls >> "colorBackground");
  _selBgColor = GetPackedColor(cls >> "colorBackgroundSelected");
  _textColor = GetPackedColor(cls >> "colorText");
  _selColor = GetPackedColor(cls >> "colorSelect");

  _underlineSelected = cls >> "underlineSelected";

  _background = GlobLoadTextureUI(GetPictureName(cls >> "background"));

  _shadow = cls >> "shadow";

  _arrowWidth = cls >> "arrowWidth";
  _arrowHeight = cls >> "arrowHeight";
  _iconArrowUp = GlobLoadTextureUI(GetPictureName(cls >> "iconArrowUp"));
  _iconArrowDown = GlobLoadTextureUI(GetPictureName(cls >> "iconArrowDown"));

  // default action parameters
  cls = Pars >> "CfgInGameUI" >> "DefaultAction";
  _showHint = cls >> "showHint";
  _showNext = cls >> "showNext";
  _showLine = cls >> "showLine";
  _defaultFont = GEngine->LoadFont(GetFontID(cls >> "font"));
  _defaultSize = cls >> "size";
  _defaultColor = GetPackedColor(cls >> "colorText");
  _nextFont = GEngine->LoadFont(GetFontID(cls >> "fontNext"));
  _nextSize = cls >> "sizeNext";
  _xOffset = cls >> "offsetX";
  _yOffset = cls >> "offsetY";
  _xHotspot = cls >> "hotspotX";
  _yHotspot = cls >> "hotspotY";
  _relativeToCursor = cls >> "relativeToCursor";
  _defaultBackground = GlobLoadTextureUI(GetPictureName(cls >> "background"));
  _defaultShadow = cls >> "shadow";

  // others
  _default = false;
  _selectedPriority = -FLT_MAX;
  Hide();
}

void UIActionsDrawable::Refresh(bool user)
{
  _default = !user;

  if (user || Glob.uiTime < _dimStart + dimTime)
    _dimStart = Glob.uiTime - protectionTime; // avoid protection
  else
    _dimStart = Glob.uiTime;                    // set protection
}

bool UIActionsDrawable::IsListVisible() const
{
  if (_default) return false;

  float age = GetAge();
  return age >= protectionTime && age <= dimTime;
}

void UIActionsDrawable::Hide()
{
  // hide immediatelly
  _dimStart = Glob.uiTime - fadeTime - 1.0;
  _default = false;
  _selected = NULL;
  _selectedPriority = -FLT_MAX;
}

float UIActionsDrawable::GetAge() const
{
  return Glob.uiTime - _dimStart;
}


float UIActionsDrawable::GetAlpha() const
{
  float age = GetAge();
  if (age <= dimTime) return 1.0f;
  if (age >= fadeTime) return 0.0f;
  return fadeCoef * (fadeTime - age);
}

int UIActionsDrawable::FindSelected()
{
  int n = Size();

  int first = -1;
  for (int i=0; i<n; i++)
  {
    const UIAction &item = Get(i);
    if (item.show)
    {
      first = i;
      break;
    }
  }
  if (first < 0) return -1;

  if (!_selected) return first; // default
  for (int i=0; i<n; i++)
  {
    const UIAction &item = Get(i);
    DoAssert(item.action);
    if (item.show && item.action->IsEqual(_selected)) return i;
  }
  _selected = NULL;
  _selectedPriority = -FLT_MAX;
  if (_default) Hide();
  return first; // default
}

void UIActionsDrawable::SelectPrev(bool cycle)
{
  if (Size() == 0) return;

  int selected = FindSelected();
  for (int i=0; i<Size(); i++) // avoid infinite loop
  {
    if (selected >= 0)
    {
      selected--;
      if (selected < 0)
      {
        if (cycle) selected = Size() - 1;
        else selected = 0;
      }
    }
    else selected = 0;

    const UIAction &action = Get(selected);
    if (action.show && !action.action->IsEqual(_selected))
    {
      _selected = Get(selected).action;
      _selectedPriority = Get(selected).priority;
      return;
    }
  }
}

void UIActionsDrawable::SelectNext(bool cycle)
{
  if (Size() == 0) return;

  int selected = FindSelected();
  for (int i=0; i<Size(); i++) // avoid infinite loop
  {
    if (selected >= 0)
    {
      selected++;
      if (selected >= Size())
      {
        if (cycle) selected = 0;
        else selected = Size() - 1;
      }
    }
    else selected = 0;

    const UIAction &action = Get(selected);
    if (action.show && !action.action->IsEqual(_selected))
    {
      _selected = Get(selected).action;
      _selectedPriority = Get(selected).priority;
      return;
    }
  }
}

void UIActionsDrawable::ProcessAction(int index, AIBrain *unit, bool fromGUI, bool byShortcut)
{
  Assert(unit);
  Assert(index >= 0);
  UIAction &action = Set(index);
  DoAssert(action.action);
  action.action->SetFromGUI(fromGUI);

  bool canHide = true;
  if (!_default)
  {
    for (int i=0; i<Size(); i++)
    {
      if (i != index && Get(i).showWindow)
      {
        // other action enforces action menu
        canHide = false;
        break;
      }
    }
  }

  if (action.hideOnUse )//&& canHide)
  {
    Hide();
    _selected = NULL;
    _selectedPriority = -FLT_MAX;
  }
  else if (fromGUI && !byShortcut)
  {
    _dimStart = Glob.uiTime;
  }
  unit->GetVehicle()->StartActionProcessing(action.action, unit);
}

static int CmpActions(const UIAction *action1, const UIAction *action2)
{
  int s = sign(action2->priority - action1->priority);
  if (s != 0) return s;
  DoAssert(action1->action);
  DoAssert(action2->action);
  return action1->action->Compare(action2->action);
}

void UIActions::Sort()
{
  QSort(Data(), Size(), CmpActions);
}

void UIActionsDrawable::Update(UIActions &src)
{
  int n = src.Size();

  // update creation time of actions
  for (int i=0; i<n; i++)
  {
    UIAction &action = src[i];
    bool found = false;
    for (int j=0; j<Size(); j++)
    {
      const UIAction &compare = Get(j);
      if (compare.IsEqual(action))
      {
        action.created = compare.created;
        break;
      }
    }
    if (!found) for (int j=0; j<_newActions.Size(); j++)
    {
      const UIAction &compare = _newActions[j];
      if (compare.IsEqual(action))
      {
        action.created = compare.created;
        break;
      }
    }
  }

  // move too new actions to separate array
  const float minAge = 0.1f;
  _newActions.Resize(0);
  for (int i=0; i<src.Size();)
  {
    const UIAction &action = src[i];
    float age = Glob.uiTime - action.created;
    if (age < minAge)
    {
      _newActions.Add(action);
      src.Delete(i);
    }
    else i++;
  }
  n = src.Size();

  // check for new action
  int newAction = -1;
  if (n == 0)
  {
    if (Size() > 0) Hide();
  }
  else if (_default || GetAge() > dimTime)
  {
    for (int i=0; i<n; i++)
    {
      const UIAction &action = src[i];
      if (!action.show) continue;
      if (!action.showWindow) continue;

      // update selected action
      DoAssert(action.action);
      if (action.action->IsEqual(_selected))
      {
        _selected = action.action;
        _selectedPriority = action.priority;
      }

      bool found = false;
      if (!_default)
      {
        // if default action is shown, action with higher priority always replace it
        for (int j=0; j<Size(); j++)
        {
          const UIAction &compare = Get(j);
          if (compare.IsEqual(action))
          {
            found = compare.showWindow;
            break;
          }
        }
      }
      if (!found)
      {
        newAction = i;
        break;
      }
    }
  }
  if (newAction >= 0)
  {
    // do not show action with less priority
    if (_default && src[newAction].priority <= _selectedPriority)
      newAction = -1;
    else
    {
      _selected = src[newAction].action;
      _selectedPriority = src[newAction].priority;
    }
  }

  Resize(n);
  for (int i=0; i<n; i++) Set(i) = src[i];

  if (newAction >= 0)  Refresh(false);
  if(FindSelected()<0) Hide();
 
  
  _newActions.CompactIfNeeded();
}

static float ActionSourceCost(EntityAI *veh, Target *tgt)
{
  // check target focus
  Assert(tgt->idExact);
  Vector3 relPos = veh->PositionWorldToModel(tgt->idExact->AimingPosition(tgt->idExact->FutureVisualState()));
  float dist2 = relPos.SquareSize();
  if (relPos.Z()<0)
  {
    dist2 *= 8;
  }
  if (fabs(relPos.X())>relPos.Z())
  {
    dist2 *= 4;
  }
  return dist2;
}

/// virtual target list - provide access to all static entities in the given area
class TargetStaticList
{
  Vector3 _pos;
  float _radius;
  
  public:
  
  TargetStaticList(Vector3Par pos, float radius)
  :_pos(pos),_radius(radius)
  {
  }
  
  template <class Functor>
  bool ForEach(const Functor &f) const;
};

template <class Functor>
bool TargetStaticList::ForEach(const Functor &f) const
{
  int xMin,xMax,zMin,zMax;
  ObjRadiusRectangle(xMin,xMax,zMin,zMax,_pos,_pos,_radius);
  for (int x=xMin; x<=xMax; x++) for (int z=zMin; z<=zMax; z++)
  {
    const ObjectListUsed &list = GLandscape->UseObjects(x,z,Landscape::UONoWait);
    int n = list.Size();
    for (int i=0; i<n; i++)
    {
      Object *obj = list[i];
      // check if it is a target entity
      EntityAI *ai = dyn_cast<EntityAI>(obj);
      if (!ai) continue;
      float dist2 = ai->FutureVisualState().Position().Distance2Inline(_pos);
      if (dist2>Square(_radius+ai->GetRadius())) continue;
      if (f(ai)) return true;
    }
  }
  
  return false;
}

/// gather actions from targets - player
class GatherTargetActions
{
  EntityAI *_ignore;
  UIActions &_actions;
  AIBrain *_unit;
  bool _now;
  bool _strict;
  
  public:
  GatherTargetActions(
    EntityAI *ignore, UIActions &actions, AIBrain *unit, bool now, bool strict
  )
  :_ignore(ignore),_actions(actions),_unit(unit),_now(now),_strict(strict)
  {
  }
  bool operator() (EntityAI *entity) const
  {
    if (entity == _ignore || entity->Invisible()) return false;
    Object::ProtectedVisualState<const ObjectVisualState> vs = entity->RenderVisualStateScope(); 
    entity->GetActions(_actions, _unit, _now, _strict);
    return false;
  }
};

/// gather actions from targets - one AI unit
class GatherTargetActionsOneUnit
{
  UIActions &_actions;
  AIBrain *_unit;
  
  public:
  GatherTargetActionsOneUnit(UIActions &actions, AIBrain *unit)
  :_actions(actions),_unit(unit)
  {
  }
  bool operator () (Target *target) const
  {
    if (!target->IsKnown() || target->IsVanished()) return false;
    EntityAI *veh = target->idExact;
    if (!veh || veh->Invisible()) return false;
    Object::ProtectedVisualState<const ObjectVisualState> vs = veh->RenderVisualStateScope(); 
    veh->GetActions(_actions, _unit, false, false);
    return false;
  }
};

/// gather actions from virtual targets - one AI unit
class GatherBuildingActionsOneUnit
{
  UIActions &_actions;
  AIBrain *_unit;
  AIGroup *_group;
  float _radius;
  
  public:
  GatherBuildingActionsOneUnit(UIActions &actions, AIBrain *unit, AIGroup *group, float radius)
  :_actions(actions),_unit(unit),_group(group),_radius(radius)
  {
  }
  bool operator () (const StaticEntityLink &info) const
  {
    Assert(info.GetId().IsObject())
    float x = info.GetId().GetObjX()*LandGrid+LandGrid*0.5f;
    float z = info.GetId().GetObjZ()*LandGrid+LandGrid*0.5f;
    float maxRadius2 = Square(_radius+LandGrid*0.7f);
    if (_group)
    {
      // check if the building is close enough to any of the group units
      for (int i=0; i<_group->NUnits(); i++)
      {
        AIUnit *unit = _group->GetUnit(i);
        if (!unit || !unit->IsUnit()) continue;
        Object::ProtectedVisualState<const ObjectVisualState> vs = unit->GetVehicle()->RenderVisualStateScope(); 
        Vector3Val pos = unit->GetVehicle()->RenderVisualState().Position();
        float dist2 = pos.DistanceXZ2(Vector3(x,0,z));
        if (dist2>maxRadius2) continue;

        // once any unit has processed the object, there is no need to process it any longer
        if (info.IsReady())
        {
          OLinkLPtr(Object) obj = info.GetLock();

          EntityAI *veh = dyn_cast<EntityAI,Object>(obj);
          if (!veh) return false;
          Object::ProtectedVisualState<const ObjectVisualState> vs = veh->RenderVisualStateScope(); 
          veh->GetActions(_actions, _unit, false, false);
        }
        break;
      }
    }
    else if (_unit && _unit->IsUnit())
    {
      // check if the building is close enough to unit
      Object::ProtectedVisualState<const ObjectVisualState> vs = _unit->GetVehicle()->RenderVisualStateScope(); 
      Vector3Val pos = _unit->GetVehicle()->RenderVisualState().Position();
      float dist2 = pos.DistanceXZ2(Vector3(x,0,z));
      if (dist2 <= maxRadius2)
      {
        if (info.IsReady())
        {
          OLinkLPtr(Object) obj = info.GetLock();
          EntityAI *veh = dyn_cast<EntityAI,Object>(obj);
          if (veh)
          {
            Object::ProtectedVisualState<const ObjectVisualState> vs = veh->RenderVisualStateScope(); 
            veh->GetActions(_actions, _unit, false, false);
          }
        }
      }
    }
    return false;
  }
};

#if _ENABLE_CONVERSATION
static void InterruptConversation(ConversationContext *conversationContext)
{
  if (!conversationContext || conversationContext->_topic.GetLength() == 0) return;

  // player do not want to answer, we may want to react somehow
  AIBrain *sender = conversationContext->_askingUnit;
  AIBrain *receiver = conversationContext->_askedUnit;
  if (!sender || !receiver) return;

  Ref<KBMessageInfo> message = receiver->CreateKBMessage(conversationContext->_topic, "interrupted");
  if (message)
  {
    if (receiver->IsLocal())
    {
      receiver->KBReact(sender, message);
    }
    else
    {
      GetNetworkManager().KBReact(sender, receiver, message);
    }
  }
}
#endif

DEFINE_ENUM(InGameUIEvent, IE, INGAMEUI_EVENT_ENUM)

void InGameUI::SetEventHandler(RString name, RString value)
{
  InGameUIEvent event = GetEnumValue<InGameUIEvent>(cc_cast(name));
  if (event != INT_MIN) _eventHandlers[event] = value;
}

bool InGameUI::OnEvent(InGameUIEvent event)
{
  RString handler = _eventHandlers[event];
  if (handler.GetLength() == 0) return false;

  GameVarSpace local(false);
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&local);
  bool result = gstate->EvaluateBool(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
  gstate->EndContext();
  return result;
}

/*!
\patch_internal 1.27 Date 10/17/2001 by Jirka
- Fixed: ProcessActions assumed unit->GetGroup() != NULL - crash occured when this condition where not satisfied
\patch 1.79 Date 7/26/2002 by Ondra
- New: MP: Player who is disconnected from server cannot activate actions.
\patch 5117 Date 1/15/2007 by Bebul
- Fixed: When the command menu is up, many keys/functions do not work.
\patch 5160 Date 5/17/2007 by Bebul
- Fixed: Some Joystick buttons have fixed function in UI and game.
*/
void InGameUI::ProcessActions(AIBrain *unit)
{
  PROFILE_SCOPE_EX(proAc,ui);
  if (!CanProcessActions(unit))
  {
    _actions.Clear();
    return;
  }

  if (IsCommandingMode() || IsCommandingForced() /*|| unit->IsSpeaking() || unit->IsListening()*/ ||
    (_actions.IsListVisible() && _actions.FindSelected() >= 0 && GInput.GetActionToDo(UAMenuBack,true,true,true,2,/*BLOCK*/true)))
  {
    _actions.Hide();
  }

  // first process user input, than collect new actions (actions may changed)

  //////////////////////////////////////////////////////////////////////////
  // user input

  float age = _actions.GetAge();
  bool def = _actions.IsDefault();
  bool visible = def || age >= protectionTime && age <= dimTime;

  bool strict = def || _actions.GetAlpha() <= 0.01;  // strict for default action

  if (!IsCommandingMode() && !IsCommandingForced() && _showHUD)
  {
    if(!GInput.keyPressed[DIK_LCONTROL] && !GInput.keyPressed[DIK_RCONTROL])
    {
      if (GInput.GetActionToDo(UANextAction) && !OnEvent(IENextAction))
      {
        if (visible && !def)
          _actions.SelectNext(false);
        if(_actions.FindSelected()>=0) _actions.Refresh(true);
      }
      if (GInput.GetActionToDo(UAPrevAction) && !OnEvent(IEPrevAction))
      {
        if (visible && !def)
          _actions.SelectPrev(false);
        if(_actions.FindSelected()>=0) _actions.Refresh(true);
      }
    }
    bool confirmMenu = false;
    if(
      ModeIsStrategy(_modeAuto) &&
      _actions.FindSelected()>=0 &&  
      _actions.IsListVisible() &&
      GInput.GetActionToDo(UAActionContext,true,true,false,2, true) 
      )
        confirmMenu = true;
    if (confirmMenu ||( GInput.GetActionToDo(UAAction)) && !OnEvent(IEAction))
    {
      if (visible)
      {
        if (_actions.Size() > 0)
        {
          if (!GetNetworkManager().IsControlsPaused())
          {
            int index = _actions.FindSelected();          
            if (index >= 0 && _actions[index].shortcut != UACancelAction) _actions.ProcessAction(index, unit, true);
          }
        }
        else
          _actions.Hide();
      }
      else
        _actions.Refresh(true);
    }
  }
  // shortcuts
  for (int i=0; i<_actions.Size(); i++)
  {
    UIAction &action = _actions[i];
    if (action.shortcut >= 0 && GInput.GetActionToDo(action.shortcut))
      _actions.ProcessAction(i, unit, true, true);
  }

  //////////////////////////////////////////////////////////////////////////
  // collect all actions

  UIActions actions;
  static StaticStorage<UIAction> actionStorage;
  actions.SetStorage(actionStorage.Init(64));
  
  Person *person = unit->GetPerson();
  if (person->CanCancelAction())
  {
    UIAction action(new ActionBasic(ATCancelAction, person));
    actions.Add(action);
  }
  else
  {
    // my vehicle
    EntityAI *veh = unit->GetVehicle();
    Object::ProtectedVisualState<const ObjectVisualState> vs = veh->FutureVisualStateScope(); 
    veh->GetActions(actions, unit, true, strict);
#if _VBS2 //convoy trainer
     if(person->IsPersonalItemsEnabled())
       person->GetActions(actions, unit, true, strict);
#else
    // FIX: enable TouchOff action in vehicles
    if (person != veh) 
    {
      Object::ProtectedVisualState<const ObjectVisualState> vs = person->FutureVisualStateScope(); 
      person->GetActions(actions, unit, true, strict);
    }
#endif

    // target
/*
    TargetStaticList iterate(veh->Position(),10);
    iterate.ForEach(GatherTargetActions(veh,actions,unit,true,strict));
*/
    if (_target)
    {
      EntityAI *entity = _target->idExact;
      if (entity && entity != veh)
      {
        Object::ProtectedVisualState<const ObjectVisualState> vs = entity->FutureVisualStateScope(); 
        entity->GetActions(actions, unit, true, strict);
      }
    }

    // special
    if (person->QIsManual())
    {
      // night vision
      if (person->IsNVEnabled())
      {
        UIActionType type = person->IsNVWanted() ? ATNVGogglesOff : ATNVGoggles;
        UIAction action(new ActionBasic(type, person));
        actions.Add(action);
      }

      // team switch
      if (GWorld->IsTeamSwitchEnabled())
      {
        // collect the units
        bool multiplayer = GWorld->GetMode() == GModeNetware;
        OLinkPermNOArray(AIBrain) netUnits;
        if (multiplayer) GetNetworkManager().GetSwitchableUnits(netUnits, unit);
        const OLinkPermNOArray(AIBrain) &units = multiplayer ? netUnits : GWorld->GetSwitchableUnits();

        // check if free positions exist
        for (int i=0; i<units.Size(); i++)
        {
          AIBrain *unit = units[i];
          if (!unit || !unit->LSIsAlive()) continue;
          if (!unit->GetPerson() || unit->GetPerson() == person) continue;
          // position found
          UIAction action(new ActionBasic(ATTeamSwitch, person));
          actions.Add(action);
          break;
        }
      }
    }
#if 0
    if (_xboxStyle && !IsMenuEmpty())
    {
      UIAction action(new ActionBasic(ATIngameMenu, person));
      actions.Add(action);
    }
#endif

    // Gear action (if not already present)
    bool found = false;
    for (int i=0; i<actions.Size(); i++)
    {
      if (actions[i].action->GetType() == ATGear)
      {
        found = true;
        break;
      }
    }
    if (!found)
    {
      UIAction action(new ActionBasic(ATGear, NULL));
      action.show = false;
      action.showWindow = false;
      actions.Add(action);
    }
  }

  actions.Sort();
  _actions.Update(actions);
}

///////////////////////////////////////////////////////////////////////////////
// Global selected units - valid for IngameUI and map

DisplayUnitInfo::DisplayUnitInfo(ControlsContainer *parent)
  : Display(parent)
{
  SetCursor(NULL);
  backgroundHeight = 0;
  backgroundWidth = 0;
  //InitControls();
}

void DisplayUnitInfo::InitControls()
{
  // InitPtr used, no NULL assignment is required
  time = NULL;
  date = NULL;
  name = NULL;
  unit = NULL;
  valueExp = NULL;
  formation = NULL;
  combatMode = NULL;
  valueHealth = NULL;
  weapon = NULL;
  ammo = NULL;
  gunnerWeapon = NULL;
  ammoMode = NULL;
  background = NULL;
  vehicle = NULL;
  speed = NULL;
  alt = NULL;
  heading = NULL;
  alt_wanted = NULL;
  speed_wanted = NULL;
  position = NULL;
  opticZoom = NULL;
  radarRange = NULL;
#if _VBS3
  magazine = NULL;
  override_status = NULL;
  //TI
  ti_background = NULL;
  ti_mode= NULL;
  ti_brightness_txt = NULL;
  ti_brightness = NULL;
  ti_contrast_txt = NULL;
  ti_contrast = NULL;
  ti_autocontrast = NULL;
#endif
  valueArmor = NULL;
  valueFuel = NULL;
  valueReload = NULL;
  cargoMan = NULL;
  cargoFuel = NULL;
  cargoRepair = NULL;
  cargoAmmo = NULL;
  commander = NULL;
  driver = NULL;
  gunner = NULL;
  gunnerWeapon = NULL;
  ammoMode = NULL;
  counterMeasuresAmmo = NULL;
  counterMeasuresMode = NULL;
}

void DisplayUnitInfo::Reload(ParamEntryVal clsEntry)
{
  Init();
  InitControls();
  LoadFromEntry(clsEntry);

  _updateHeightByCrew = true;
  ConstParamEntryPtr entry = clsEntry.FindEntry("updateHeightByCrew");
  if (entry) _updateHeightByCrew = *entry;
  
  _updateWidthByCrew = true;
  entry = clsEntry.FindEntry("updateWidthByCrew");
  if (entry) _updateWidthByCrew = *entry;

  _updateWidthByWeapon = true;
  entry = clsEntry.FindEntry("updateWidthByWeapon");
  if (entry) _updateWidthByWeapon = *entry;
}

#define STATIC_CTRL(CTRL_IDC,NAME) \
    case CTRL_IDC: \
    NAME = new CStatic(this, idc, cls); \
    return NAME;

#define PROGRESS_CTRL(CTRL_IDC,NAME) \
    case CTRL_IDC: \
    NAME = new CProgressBar(this, idc, cls); \
    return NAME;

#if _VBS3_LASE_LEAD
DisplayLaserRange::DisplayLaserRange(ControlsContainer *parent)
: Display(parent)
{
  SetCursor(NULL);
  InitControls();
  LoadFromEntry(Pars >> "RscInGameUI" >> "RscLaserRange");
}

void DisplayLaserRange::InitControls()
{
  range = NULL;
  rangeNum = NULL;
}

Control *DisplayLaserRange::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
    STATIC_CTRL(IDC_IGLR_BG,background)
      STATIC_CTRL(IDC_IGLR_RANGE,range)
      STATIC_CTRL(IDC_IGLR_RANGE_NUM,rangeNum)
  }
  return Display::OnCreateCtrl(type, idc, cls);
}
#endif
Control *DisplayUnitInfo::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
    case IDC_IGUI_BG:
      background = new CStatic(this, idc, cls);
      backgroundHeight = background->H();
      backgroundWidth = background->W();
      return background;
    case IDC_IGUI_TIME:
#if _VBS3
      time = new CStaticTime(this, idc, cls, false); //no blinking
#else
      time = new CStaticTime(this, idc, cls, true);
#endif
      return time;
    case IDC_IGUI_DATE:
      date = new CStatic(this, idc, cls);
      return date;
    case IDC_IGUI_NAME:
      name = new CStatic(this, idc, cls);
      return name;
    case IDC_IGUI_UNIT:
      unit = new CStatic(this, idc, cls);
      return unit;
    case IDC_IGUI_VALUE_EXP:
      valueExp = new CProgressBar(this, idc, cls);
      return valueExp;
    case IDC_IGUI_FORMATION:
      formation = new CStatic(this, idc, cls);
      return formation;
    case IDC_IGUI_COMBAT_MODE:
      combatMode = new CStatic(this, idc, cls);
      return combatMode;
    case IDC_IGUI_VALUE_HEALTH:
      valueHealth = new CProgressBar(this, idc, cls);
      return valueHealth;
    case IDC_IGUI_WEAPON:
      weapon = new CStatic(this, idc, cls);
      return weapon;
    case IDC_IGUI_AMMO:
      ammo = new CStatic(this, idc, cls);
      return ammo;
    case IDC_IGUI_COUNTER_MEASURES_AMMO:
      counterMeasuresAmmo = new CStatic(this, idc, cls);
      return counterMeasuresAmmo;
    case IDC_IGUI_COUNTER_MEASURES_MODE:
      counterMeasuresMode = new CStatic(this, idc, cls);
      return counterMeasuresMode;
    case IDC_IGUI_WEAPON_GUNNER:
      gunnerWeapon = new CStatic(this, idc, cls);
      return gunnerWeapon;
    case IDC_IGUI_WEAPON_MODE:
      ammoMode = new CStatic(this, idc, cls);
      return ammoMode;
    case IDC_IGUI_VEHICLE:
      vehicle = new CStatic(this, idc, cls);
      return vehicle;
    case IDC_IGUI_SPEED:
      speed = new CStatic(this, idc, cls);
      return speed;
    case IDC_IGUI_ALT:
      alt = new CStatic(this, idc, cls);
      return alt;
    case IDC_IGUI_RADARRANGE:
      radarRange = new CStatic(this, idc, cls);
      return radarRange;
    case IDC_IGUI_HEADING:
      heading = new CStatic(this, idc, cls);
      return heading;
    STATIC_CTRL(IDC_IGUI_ALT_WANTED,alt_wanted)
    STATIC_CTRL(IDC_IGUI_SPEED_WANTED,speed_wanted)
    STATIC_CTRL(IDC_IGUI_POSITION,position)
    STATIC_CTRL(IDC_IGUI_OPTIC,opticZoom)
#if _VBS3
      STATIC_CTRL(IDC_IGUI_MAGAZINE,magazine)
      STATIC_CTRL(IDC_IGUI_OVR_STATUS,override_status)
      //TI
      STATIC_CTRL(IDC_IGUI_TI_BACK,ti_background)
      STATIC_CTRL(IDC_IGUI_TI_MODE,ti_mode)
      STATIC_CTRL(IDC_IGUI_TI_BRIGHTNESS_TXT,ti_brightness_txt)
      STATIC_CTRL(IDC_IGUI_TI_CONTRAST_TXT,ti_contrast_txt)
      STATIC_CTRL(IDC_IGUI_TI_AUTO_CONTRAST,ti_autocontrast)
      PROGRESS_CTRL(IDC_IGUI_TI_BRIGHTNESS,ti_brightness)
      PROGRESS_CTRL(IDC_IGUI_TI_CONTRAST,ti_contrast)
#endif
    case IDC_IGUI_VALUE_ARMOR:
      valueArmor = new DisplayHitZoneInfo(this, idc, cls);
      return valueArmor;
    case IDC_IGUI_VALUE_FUEL:
      valueFuel = new CProgressBar(this, idc, cls);
      return valueFuel;
    case IDC_IGUI_VALUE_RELOAD:
      valueReload = new CProgressBar(this, idc, cls);
      return valueReload;
    case IDC_IGUI_CARGO_MAN:
      cargoMan = new CStatic(this, idc, cls);
      return cargoMan;
    case IDC_IGUI_CARGO_FUEL:
      cargoFuel = new CStatic(this, idc, cls);
      return cargoFuel;
    case IDC_IGUI_CARGO_REPAIR:
      cargoRepair = new CStatic(this, idc, cls);
      return cargoRepair;
    case IDC_IGUI_CARGO_AMMO:
      cargoAmmo = new CStatic(this, idc, cls);
      return cargoAmmo;
    case IDC_IGUI_COMMANDER:
      commander = new CStatic(this, idc, cls);
      return commander;
    case IDC_IGUI_DRIVER:
      driver = new CStatic(this, idc, cls);
      return driver;
    case IDC_IGUI_GUNNER:
      gunner = new CStatic(this, idc, cls);
      return gunner;
  }
  return Display::OnCreateCtrl(type, idc, cls);
}

DisplayHitZoneInfo::DisplayHitZoneInfo(ControlsContainer *parent, int idc, ParamEntryPar cls)
:CListBox(parent, idc, cls)
{

}

void DisplayHitZoneInfo::DrawItem(
 UIViewport *vp, float alpha, int i, bool selected, float top, const Rect2DFloat &rect)
{
  const int w = GLOB_ENGINE->Width2D();
  const int h = GLOB_ENGINE->Height2D();
  const float border = TEXT_BORDER;

  PackedColor color;
  float left = _x + border;
  int shadow = GWorld->UI()->GetHitZonesShadow();

  Texture *texture = GWorld->UI()->GetHitZonesTexture();
  if (texture)
  {
    float height = texture->AHeight();
    if (height > 0)
    {
      float width = SCALED(_rowHeight) * (texture->AWidth() * h) / (height * w);

      PackedColor pictureColor = GetFtColor(i);

      float textureU = ((GetValue(i)-1)%8)/8.0f;
      float textureV = ((GetValue(i)-1)/8)/4.0f;;

      Draw2DParsExt pars;
      pars.mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
      pars.SetColor(pictureColor);

      pars.Init();
      pars.SetU(textureU,textureU + 1/8.0f);
      pars.SetV(textureV,textureV + 1/4.0f);
      pars.spec |= (shadow==2)? UISHADOW : 0;

      vp->Draw2D
        (
        pars,
        Rect2DPixel(CX(rect.x), CY(top), CW(rect.w), CH(SCALED(_rowHeight))),
        Rect2DPixel(CX(rect.x), CY(top), CW(rect.w), CH(SCALED(_rowHeight)))
        );
      left += width + border;
    }
  }
}

DisplayWeaponInfo::DisplayWeaponInfo(ControlsContainer *parent)
: Display(parent)
{
  showCompass = false;
  SetCursor(NULL);
}

void DisplayWeaponInfo::InitControls()
{
  // InitPtr used, no NULL assignment is required
  distance = NULL;
  visionMode = NULL;
  FLIRMode = NULL;
  FOVMode = NULL;
  compass = NULL;
  heading = NULL;
  autohover = NULL;
  laserMarkerON = NULL;
  BallEnabled = NULL;
  BallRange = NULL;
  elevation = NULL;
  elevationText = NULL;
  GPSPlayer = NULL;
  GPSTarget = NULL;

  javelinDay = NULL;
  javelinFLTR = NULL;
  javelinNFOV = NULL;
  javelinWFOV = NULL;
  javelinSEEK = NULL;
  javelinMISSILE = NULL;

  BallEnabledImg = NULL;

  staticItems = NULL;
}

void DisplayWeaponInfo::Reload(ParamEntryVal clsEntry)
{
  Init();
  InitControls();
  LoadFromEntry(clsEntry);
  if(clsEntry.FindEntry("showCompass")) showCompass = clsEntry >> "showCompass";
}

Control *DisplayWeaponInfo::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_IGUI_WEAPON_DISTANCE:
    distance = new CStatic(this, idc, cls);
    return distance;
  case IDC_IGUI_WEAPON_VISION_MODE:
    visionMode = new CStatic(this, idc, cls);
    return visionMode;
  case IDC_IGUI_WEAPON_FLIR_MODE:
    FLIRMode = new CStatic(this, idc, cls);
    return FLIRMode;
  case IDC_IGUI_WEAPON_FOV_MODE:
    FOVMode = new CStatic(this, idc, cls);
    return FOVMode;
  case IDC_IGUI_WEAPON_COMPASS:
    compass = new CStatic(this, idc, cls);
    return compass;
  case IDC_IGUI_WEAPON_HEADING:
    heading = new CStatic(this, idc, cls);
    return heading;
  case IDC_IGUI_WEAPON_AUTOHOVER:
    autohover = new CStatic(this, idc, cls);
    return autohover;
  case IDC_IGUI_WEAPON_LASER_MARKER_ON:
    laserMarkerON = new CStatic(this, idc, cls);
    return laserMarkerON;
  case IDC_IGUI_WEAPON_BALL_ENABLED:
    BallEnabled = new CStatic(this, idc, cls);
    return BallEnabled;
  case IDC_IGUI_WEAPON_BALL_RANGE:
    BallRange = new CStatic(this, idc, cls);
    return BallRange;
  case IDC_IGUI_WEAPON_JAVELIN_DAY:
    javelinDay = new CStatic(this, idc, cls);
    return javelinDay;
  case IDC_IGUI_WEAPON_JAVELIN_FLTR:
    javelinFLTR = new CStatic(this, idc, cls);
    return javelinFLTR;
  case IDC_IGUI_WEAPON_JAVELIN_NFOV:
    javelinNFOV = new CControlsGroup(this, idc, cls);
    return javelinNFOV;
  case IDC_IGUI_WEAPON_JAVELIN_WFOV:
    javelinWFOV = new CControlsGroup(this, idc, cls);
    return javelinWFOV;
  case IDC_IGUI_WEAPON_BALISTIC_COMP:
    BallEnabledImg = new CStatic(this, idc, cls);
    return BallEnabledImg;
  case IDC_IGUI_WEAPON_JAVELIN_SEEK:
    javelinSEEK = new CStatic(this, idc, cls);
    return javelinSEEK;
  case IDC_IGUI_WEAPON_JAVELIN_MISSLE:
    javelinMISSILE = new CStatic(this, idc, cls);
    return javelinMISSILE;
  case IDC_IGUI_WEAPON_ELEVATION:
    elevation = new CStatic(this, idc, cls);
    return elevation;
  case IDC_IGUI_WEAPON_ELEVATION_TEXT:
    elevationText = new CStatic(this, idc, cls);
    return elevationText;
  case IDC_IGUI_WEAPON_STATIC_ITEMS:
    staticItems = new CControlsGroup(this, idc, cls);
    return staticItems;
  case IDC_IGUI_WEAPON_GPS_PLAYER:
    GPSPlayer = new CStatic(this, idc, cls);
    return GPSPlayer;
  case IDC_IGUI_WEAPON_GPS_TARGET:
    GPSTarget = new CStatic(this, idc, cls);
    return GPSTarget;
  }
  return Display::OnCreateCtrl(type, idc, cls);
}

// Hints

DisplayHint::DisplayHint(ControlsContainer *parent)
: Display(parent)
{
  SetCursor(NULL);
  LoadFromEntry(Pars >> "RscInGameUI" >> "RscHint");
}

Control *DisplayHint::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_IGHINT_BG:
    _background = ctrl;
    break;
  case IDC_IGHINT_HINT:
    ctrl->EnableCtrl(false);
    _hint = ctrl;
    break;
  }
  return ctrl;
}

RString DisplayHint::GetHint() const
{
  ITextContainer *ctrl = GetTextContainer(_hint);
  return ctrl ? ctrl->GetText() : RString();
}

void DisplayHint::SetHint(RString hint)
{
  ITextContainer *ctrl = GetTextContainer(_hint);
  if (!ctrl) return;

  ctrl->SetText(hint);
  float h = 0;
  switch (_hint->GetType())
  {
  case CT_STATIC:
    h = static_cast<CStatic *>(_hint.GetRef())->GetTextHeight();
    break;
  case CT_STRUCTURED_TEXT:
    h = static_cast<CStructuredText *>(_hint.GetRef())->GetTextHeight();
    break;
  }
  
  float dh = _background->H() - _hint->H();
  _hint->SetPos(_hint->X(), _hint->Y(), _hint->W(), h );
  _background->SetPos(_background->X(), _background->Y(), _background->W(), h + dh);
}

void DisplayHint::SetHint(INode *hint)
{
  CStructuredText *ctrl = GetStructuredText(_hint);

  ctrl->SetText(hint);
  float h = ctrl->GetTextHeight();

  float dh = _background->H() - _hint->H();
  _hint->SetPos(_hint->X(), _hint->Y(), _hint->W(), h );
  _background->SetPos(_background->X(), _background->Y(), _background->W(), h + dh);
}


void DisplayHint::SetPosition(float top)
{
  if (!_hint) return;
  float dy = _hint->Y() - _background->Y();
  _background->SetPos
  (
    _background->X(), top, _background->W(), _background->H()
  );
  _hint->SetPos
  (
    _hint->X(), top + dy, _hint->W(), _hint->H()
  );
}

//TASK hint

DisplayTaskHint::DisplayTaskHint(ControlsContainer *parent)
: Display(parent)
{
  _timeDown = Glob.uiTime; 
  _timeUp =  Glob.uiTime;
  SetCursor(NULL);
  _textureRect = Rect2DPixel(
    GEngine->Width2D() * (float)(Pars >> "RscInGameUI" >> "RscTaskHint" >> "TaskIcon" >> "x"),
    GEngine->Height2D() * (float)(Pars >> "RscInGameUI" >> "RscTaskHint" >> "TaskIcon" >> "y"),
    GEngine->Width2D() *  (float)(Pars >> "RscInGameUI" >> "RscTaskHint" >> "TaskIcon" >> "w"),
    GEngine->Height2D() * (float)(Pars >> "RscInGameUI" >> "RscTaskHint" >> "TaskIcon" >> "h")
    );
  LoadFromEntry(Pars >> "RscInGameUI" >> "RscTaskHint");

}

Control *DisplayTaskHint::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_IGTASKHINT_BG:
    _background = ctrl;
    break;
  case IDC_IGTASKHINT_HINT:
    ctrl->EnableCtrl(false);
    _taskHint = ctrl;
    break;
  }
  return ctrl;
}

RString DisplayTaskHint::GetTaskHint() const
{
  ITextContainer *ctrl = GetTextContainer(_taskHint);
  return ctrl ? ctrl->GetText() : RString();
}

void DisplayTaskHint::AddTaskHint(RString hint, PackedColor color,Ref<Texture> texture)
{
  _colorQue.Add(color);
  _textureQue.Add(texture);
  _textQue.Add(hint);

  if(_textQue.Size() == 1) SetTaskHint();
}

void DisplayTaskHint::SetTaskHint()
{
  ITextContainer *ctrl = GetTextContainer(_taskHint);
  if (!ctrl) return;
  if(_textQue.Size()==0 || _colorQue.Size()==0) return;

  _timeUp = Glob.uiTime;
  _timeDown = Glob.uiTime;

  ctrl->SetText(_textQue[0]);
  float textH = 0;
  switch (_taskHint->GetType())
  {
  case CT_STATIC:
    textH = static_cast<CStatic *>(_taskHint.GetRef())->GetTextHeight();
    break;
  case CT_STRUCTURED_TEXT:
    textH = static_cast<CStructuredText *>(_taskHint.GetRef())->GetTextHeight();
    break;
  }

  textH = floatMax(textH,_textureRect.h/(float)GEngine->Width2D()+0.02f);
  float dh = _background->H() -_taskHint->H();
  _taskHint->SetPos(_taskHint->X(), _taskHint->Y(), _taskHint->W(), textH );
  _background->SetPos(_background->X(), _background->Y(), _background->W(),textH + dh);

  ctrl->SetColor(_colorQue[0]);
}


void DisplayTaskHint::SetPosition(float top)
{
  if (!_taskHint) return;
  float dy = _taskHint->Y() - _background->Y();
  _background->SetPos
    (
    _background->X(), top, _background->W(), _background->H()
    );
  _taskHint->SetPos
    (
    _taskHint->X(), top + dy, _taskHint->W(), _taskHint->H()
    );
}

// menu attributes

// EntityAI attribute
class AttrEntityAI : public IAttribute
{
protected:
  OLink(EntityAI) _value;

public:
  void Load(RString value);
  EntityAI *GetValue() const {return _value;}
  void SetValue(EntityAI *value) {_value = value;}

  LSError Serialize(ParamArchive &ar);
  bool IsEqualTo(const IAttribute *with) const;
};

void AttrEntityAI::Load(RString value)
{
  Fail("Not implemented");
}

LSError AttrEntityAI::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("value", _value, 1));
  return LSOK;
}

bool AttrEntityAI::IsEqualTo(const IAttribute *with) const
{
  const AttrEntityAI *w = static_cast<const AttrEntityAI *>(with);
  return GetValue() == w->GetValue();
}

IAttribute *CreateAttrEntityAI() {return new AttrEntityAI();}
EntityAI *GetAttrEntityAI(const IAttribute *attr) {return static_cast<const AttrEntityAI *>(attr)->GetValue();}
void SetAttrEntityAI(IAttribute *attr, EntityAI *value) {static_cast<AttrEntityAI *>(attr)->SetValue(value);}

// Action attribute
class AttrAction : public IAttribute
{
protected:
  Ref<Action> _value;

public:
  void Load(RString value);
  Action *GetValue() const {return _value;}
  void SetValue(Action *value) {_value = value;}

  LSError Serialize(ParamArchive &ar);
  bool IsEqualTo(const IAttribute *with) const;
};

void AttrAction::Load(RString value)
{
  Fail("Not implemented");
}

LSError AttrAction::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("value", _value, 1));
  return LSOK;
}

bool AttrAction::IsEqualTo(const IAttribute *with) const
{
  const AttrAction *w = static_cast<const AttrAction *>(with);
  return GetValue() == w->GetValue();
}

IAttribute *CreateAttrAction() {return new AttrAction();}
Action *GetAttrAction(const IAttribute *attr) {return static_cast<const AttrAction *>(attr)->GetValue();}
void SetAttrAction(IAttribute *attr, Action *value) {static_cast<AttrAction *>(attr)->SetValue(value);}

// Target attribute
class AttrTarget : public IAttribute
{
protected:
  LinkTarget _value;

public:
  void Load(RString value);
  Target *GetValue() const {return _value;}
  void SetValue(Target *value) {_value = value;}

  LSError Serialize(ParamArchive &ar);
  bool IsEqualTo(const IAttribute *with) const;
};

void AttrTarget::Load(RString value)
{
  Fail("Not implemented");
}

LSError AttrTarget::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("value", _value, 1));
  return LSOK;
}

bool AttrTarget::IsEqualTo(const IAttribute *with) const
{
  const AttrTarget *w = static_cast<const AttrTarget *>(with);
  return GetValue() == w->GetValue();
}

IAttribute *CreateAttrTarget() {return new AttrTarget();}
Target *GetAttrTarget(const IAttribute *attr) {return static_cast<const AttrTarget *>(attr)->GetValue();}
void SetAttrTarget(IAttribute *attr, Target *value) {static_cast<AttrTarget *>(attr)->SetValue(value);}

#if _ENABLE_CONVERSATION
// Message attribute
class AttrMessage : public IAttribute
{
protected:
  Ref<const KBUINodeInfo> _value;

public:
  void Load(RString value);
  const KBUINodeInfo *GetValue() const {return _value;}
  void SetValue(const KBUINodeInfo *value) {_value = value;}

  LSError Serialize(ParamArchive &ar);
  bool IsEqualTo(const IAttribute *with) const;
};

void AttrMessage::Load(RString value)
{
  Fail("Not implemented");
}

LSError AttrMessage::Serialize(ParamArchive &ar)
{
  Fail("Not implemented");
  return LSOK;
}

bool AttrMessage::IsEqualTo(const IAttribute *with) const
{
  const AttrMessage *w = static_cast<const AttrMessage *>(with);
  return GetValue() == w->GetValue();
}

IAttribute *CreateAttrMessage() {return new AttrMessage();}
const KBUINodeInfo *GetAttrMessage(const IAttribute *attr) {return static_cast<const AttrMessage *>(attr)->GetValue();}
void SetAttrMessage(IAttribute *attr, const KBUINodeInfo *value) {static_cast<AttrMessage *>(attr)->SetValue(value);}

// GameValue attribute
class AttrGameValue : public IAttribute
{
protected:
  GameValue _value;

public:
  void Load(RString value);
  const GameValue &GetValue() const {return _value;}
  void SetValue(const GameValue &value) {_value = value;}

  LSError Serialize(ParamArchive &ar);
  bool IsEqualTo(const IAttribute *with) const;
};

void AttrGameValue::Load(RString value)
{
  Fail("Not implemented");
}

LSError AttrGameValue::Serialize(ParamArchive &ar)
{
  Fail("Not implemented");
  return LSOK;
}

bool AttrGameValue::IsEqualTo(const IAttribute *with) const
{
  const AttrGameValue *w = static_cast<const AttrGameValue *>(with);
  return GetValue().IsEqualTo(w->GetValue());
}

IAttribute *CreateAttrGameValue() {return new AttrGameValue();}
const GameValue &GetAttrGameValue(const IAttribute *attr) {return static_cast<const AttrGameValue *>(attr)->GetValue();}
void SetAttrGameValue(IAttribute *attr, const GameValue &value) {static_cast<AttrGameValue *>(attr)->SetValue(value);}

#endif

// menu attributes registration

AttributeTypes MenuAttributeTypes;

#if _ENABLE_DIRECT_MESSAGES && _ENABLE_CONVERSATION

#define ATTRIBUTES_LIST(XX) \
  XX("dir", attrDir, CreateAttrFloat) \
  XX("dist", attrDist, CreateAttrFloat) \
  XX("vehicle", attrVehicle, CreateAttrEntityAI) \
  XX("target", attrTarget, CreateAttrTarget) \
  XX("action", attrAction, CreateAttrAction) \
  XX("getinpos", attrGetInPos, CreateAttrInt) \
  XX("direct", attrDirect, CreateAttrBool) \
  XX("gesture", attrGesture, CreateAttrString) \
  XX("message", attrMessage, CreateAttrMessage) \
  XX("gamevalue", attrGameValue, CreateAttrGameValue) \
  XX("expression", attrExpression, CreateAttrString) \
  // name, variable, create function

#elif _ENABLE_CONVERSATION

#define ATTRIBUTES_LIST(XX) \
  XX("dir", attrDir, CreateAttrFloat) \
  XX("dist", attrDist, CreateAttrFloat) \
  XX("vehicle", attrVehicle, CreateAttrEntityAI) \
  XX("target", attrTarget, CreateAttrTarget) \
  XX("action", attrAction, CreateAttrAction) \
  XX("getinpos", attrGetInPos, CreateAttrInt) \
  XX("message", attrMessage, CreateAttrMessage) \
  XX("gamevalue", attrGameValue, CreateAttrGameValue) \
  XX("expression", attrExpression, CreateAttrString) \
  // name, variable, create function

#else

#define ATTRIBUTES_LIST(XX) \
  XX("dir", attrDir, CreateAttrFloat) \
  XX("dist", attrDist, CreateAttrFloat) \
  XX("vehicle", attrVehicle, CreateAttrEntityAI) \
  XX("target", attrTarget, CreateAttrTarget) \
  XX("action", attrAction, CreateAttrAction) \
  XX("getinpos", attrGetInPos, CreateAttrInt) \
  XX("expression", attrExpression, CreateAttrString) \
  // name, variable, create function

#endif


#define DEFINE_ATTR_VARIABLE(name, variable, func) static int variable;
ATTRIBUTES_LIST(DEFINE_ATTR_VARIABLE);

#define REGISTER_ATTR(name, variable, func) \
  variable = MenuAttributeTypes.attributes.AddValue(name); \
  MenuAttributeTypes.createFunctions.Access(variable); \
  MenuAttributeTypes.createFunctions[variable] = func;
  
#include <El/Modules/modules.hpp>

INIT_MODULE(InGameUI, 4)
{
  // Register attributes
  ATTRIBUTES_LIST(REGISTER_ATTR);
  MenuAttributeTypes.attributes.Close();
};

void SetMenuAttribute(MenuItem *item, RString name, RString value)
{
  item->SetAttribute(name, value, MenuAttributeTypes);
}

void SetMenuAttrBool(MenuItem *item, int index, bool value)
{
  IAttribute *attr = item->AccessAttribute(index, MenuAttributeTypes);
  ::SetAttrBool(attr, value);
}

void SetMenuAttrInt(MenuItem *item, int index, int value)
{
  IAttribute *attr = item->AccessAttribute(index, MenuAttributeTypes);
  ::SetAttrInt(attr, value);
}

void SetMenuAttrFloat(MenuItem *item, int index, float value)
{
  IAttribute *attr = item->AccessAttribute(index, MenuAttributeTypes);
  ::SetAttrFloat(attr, value);
}

void SetMenuAttrString(MenuItem *item, int index, RString value)
{
  IAttribute *attr = item->AccessAttribute(index, MenuAttributeTypes);
  ::SetAttrString(attr, value);
}

void SetMenuAttrEntityAI(MenuItem *item, int index, EntityAI *value)
{
  IAttribute *attr = item->AccessAttribute(index, MenuAttributeTypes);
  ::SetAttrEntityAI(attr, value);
}

void SetMenuAttrAction(MenuItem *item, int index, Action *value)
{
  IAttribute *attr = item->AccessAttribute(index, MenuAttributeTypes);
  ::SetAttrAction(attr, value);
}

void SetMenuAttrTarget(MenuItem *item, int index, Target *value)
{
  IAttribute *attr = item->AccessAttribute(index, MenuAttributeTypes);
  ::SetAttrTarget(attr, value);
}

#if _ENABLE_CONVERSATION

void SetMenuAttrMessage(MenuItem *item, int index, const KBUINodeInfo *value)
{
  IAttribute *attr = item->AccessAttribute(index, MenuAttributeTypes);
  ::SetAttrMessage(attr, value);
}

void SetMenuAttrGameValue(MenuItem *item, int index, const GameValue &value)
{
  IAttribute *attr = item->AccessAttribute(index, MenuAttributeTypes);
  ::SetAttrGameValue(attr, value);
}
#endif

/// list of all menu conditions (simple expression variables)
#define MENU_CONDITION_VARIABLES(XXX) \
  XXX(HasRadio) \
  XXX(CanAnswer) \
  XXX(IsLeader) \
  XXX(IsAlone) \
  XXX(IsAloneInVehicle) \
  XXX(IsCommander) \
  XXX(VehicleCommander) \
  XXX(CommandsToGunner) \
  XXX(CommandsToPilot) \
  XXX(NotEmpty) \
  XXX(NotEmptySoldiers) \
  XXX(NotEmptyCommanders) \
  XXX(NotEmptyMainTeam) \
  XXX(NotEmptyRedTeam) \
  XXX(NotEmptyGreenTeam) \
  XXX(NotEmptyBlueTeam) \
  XXX(NotEmptyYellowTeam) \
  XXX(NotEmptySubgroups) \
  XXX(NotEmptyInVehicle) \
  XXX(SelectedTeam) \
  XXX(SelectedUnit) \
  XXX(FuelLow) \
  XXX(AmmoLow) \
  XXX(Injured) \
  XXX(Multiplayer) \
  XXX(AreActions) \
  XXX(CursorOnGroupMember) \
  XXX(CursorOnHoldingFire) \
  XXX(CursorOnEmptyVehicle) \
  XXX(CursorOnVehicleCanGetIn) \
  XXX(CursorOnFriendly) \
  XXX(CursorOnEnemy) \
  XXX(CursorOnEnemyTargeted) \
  XXX(CursorOnEnemyEngaged) \
  XXX(EnemyTargeted) \
  XXX(EnemyEngaged) \
  XXX(CursorOnGround) \
  XXX(CanSelectUnitFromBar) \
  XXX(CanDeselectUnitFromBar) \
  XXX(CanSelectVehicleFromBar) \
  XXX(CanDeselectVehicleFromBar) \
  XXX(CanSelectTeamFromBar) \
  XXX(CanDeselectTeamFromBar) \
  XXX(FormationLine) \
  XXX(FormationDiamond) \
  XXX(SomeSelectedHoldingFire) \
  XXX(PlayableLeader) \
  XXX(PlayableSelected) \
  XXX(IsWatchCommanded) \
  XXX(IsSelectedToAdd) \
  XXX(HCIsLeader) \
  XXX(HCCursorOnIcon) \
  XXX(HCCursorOnIconSelectable) \
  XXX(HCCanSelectUnitFromBar) \
  XXX(HCCanDeselectUnitFromBar) \
  XXX(HCCanSelectTeamFromBar) \
  XXX(HCCanDeselectTeamFromBar) \
  XXX(HCNotEmpty) \
  XXX(PlayerVehicleCanGetIn) \
  XXX(IsXbox) \
  XXX(IsTeamSwitch) \
  XXX(CursorOnNotEmptySubgroups) \
  XXX(SomeSelectedHaveTarget) \
  XXX(CursorOnGroupMemberSelected) \
  XXX(HCCursorOnIconSelectableSelected) \
  XXX(HCCursorOnIconenemy) \
  XXX(PlayerOwnRadio) \
  XXX(CursorOnNeedFirstAID) \
  XXX(CursorOnNeedHeal) \
  XXX(SelectedArtillery) \
  XXX(CursorOnNeedRepair) \
  XXX(CursorOnBackpackCanTake) \
  XXX(CursorOnAssemble) \
  XXX(CursorOnDisassemble)

/// Variable names
#define MENU_CONDITION_VAR_NAME(name) #name,

static const RString MenuConditionVariables[]=
{
  MENU_CONDITION_VARIABLES(MENU_CONDITION_VAR_NAME)
};

/// Variable values
#define MENU_CONDITION_VAR_VALUE(name) var##name ? 1.0f : 0.0f,

InGameUI::InGameUI()
:_mode(UIFire),_modeAuto(UIFire),
_groundPointValid(false),
_visibleListTemp(MemAllocSA(_visibleListTempStorage,sizeof(_visibleListTempStorage))),
_fireStartTime(0), _weaponCursorScaleCoef(1.0f), _throwCursorMinScale(1.0f), _throwCursorMaxScale(2.0f), 
_throwCursorFadeSpeed(2.0f)
{
#if _ENABLE_CHEATS
  _showAll=false;
#endif

#if SPEEREO_SPEECH
  _idSpeechEngine = AddRegisterApplication((HWND)CurrentAppInfoFunctions->GetAppHWnd());
#endif

  _modelCursor=VForward;
  //_camGroupPos=VForward;
  _camGroupHeading = 0;
  _camGroupDive = _camGroupDiveOld = 0;
  _camGroupZoom = 1;
  _camGroupCursor = Point2DFloat(0,0);

  _lastMeTime = Glob.uiTime;
  _lastFollowMeTime = Glob.uiTime;
  _lastCmdId = -1;
  _lastCmdTime = Glob.uiTime - 120;
  _lastTargetTime = Glob.uiTime - 120;
  _lastGroupDirTime = Glob.uiTime - 120;
  _lastFormTime = Glob.uiTime;
  _lastWpTime = Glob.uiTime;
  _lastMenuTime = Glob.uiTime;
  #if PROTECTION_ENABLED && !CHECK_FADE && !ALWAYS_FAIL_CRC
    // random time before message is shown
    _timeToPlay = Glob.uiTime+10*60+GRandGen.RandomValue()*(50*60);
  #else
    _timeToPlay = Glob.uiTime+GRandGen.PlusMinus(60,30);
  #endif

  _target = NULL;
  _lockTarget = NULL;
  _lastTarget = NULL;
  _preferedTarget = NULL;
  _wantLock = false; // users wants to lock enemy target
  _hcTarget = NULL;

  _artilleryLocked = false;

  _lockAimValidUntil = Glob.uiTime-60;
//  _curWeapon = 0;
  
  _blinkState = false;
  _blinkStateChange = UITime(0);

  _drawHCCommand = false;
  _drawGroupIcons2D = false;
  _drawGroupIcons3D = false;
  _selectableGroupIcons = false;

  _weaponInfoAlpha = -1.0f;

#ifndef _XBOX
  _dragging = false;
#endif
  _mouseDown = false;

  // do not show quick commanding menu
  _showQuickMenu = UITIME_MAX;

  Init();

#if _VBS3_LASE_LEAD
  _laserRange = new DisplayLaserRange(NULL);
#endif

  _hint = new DisplayHint(NULL);
  _hint->SetHint("");
  _hintTime = Glob.uiTime;

  _taskHint = new DisplayTaskHint(NULL);
  _taskHint->ClearHints();

  _tmPos = 1;
  _tmTime = UITime(0);
  _tmIn = false;
  _tmOut = false;

  _tankPos = 1;
  _tankTime = UITime(0);
  _tankIn = false;
  _tankOut = false;

/*
  _font24=GLOB_ENGINE->LoadFont(GetFontID(FontS));
  _font36=GLOB_ENGINE->LoadFont(GetFontID(FontM));
*/

  _leftPressed = false;
  _rightPressed = false;
  
  _lastGroupOver = NULL;
  _hcSelected = false;

  InitMenu();
  ProgressRefresh();

  // TODO: remove
  tdName = "";
  giName = "";
  piName = "";
  uiName = "";

  ParamEntryVal mainCfg = Pars >> "CfgInGameUI";

  _xboxStyle = false;
  ConstParamEntryPtr entry = mainCfg.FindEntry("xboxStyle");
  if (entry) _xboxStyle = *entry;
  
  _playerIsLeader = false;

  if (_xboxStyle)
  {
    _basicMenu = new Menu();
    _basicMenu->Load("RscMenuBasicLevel", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
  }

  unfocusMenuAlpha = mainCfg >> "unfocusMenuAlpha";

  bgColor = GetPackedColor(mainCfg >> "colorBackground");
  bgColorCmd = GetPackedColor(mainCfg >> "colorBackgroundCommand");
  bgColorHelp = GetPackedColor(mainCfg >> "colorBackgroundHelp");
  ftColor = GetPackedColor(mainCfg >> "colorText");

  _imageBar = GLOB_ENGINE->TextBank()->Load
  (
    GetPictureName(mainCfg >> "Bar" >> "imageBar")
  );

  {
    ParamEntryVal cfg = mainCfg >> "Picture";
    pictureColor = GetPackedColor(cfg >> "color");
    pictureProblemsColor = GetPackedColor(cfg >> "colorProblems");
    pictureShadow = mainCfg >> "Shadow";
  }

  {
    ParamEntryVal cfg = mainCfg >> "Capture";
    capBgColor = GetPackedColor(cfg >> "colorBackground");
    capFtColor = GetPackedColor(cfg >> "colorText");
    capLnColor = GetPackedColor(cfg >> "colorLine");
    capShadow = cfg >> "shadow";
  }
  ProgressRefresh();

  {
    ParamEntryVal cfg = mainCfg >> "Menu";
    // hide animation
    ConstParamEntryPtr entry = cfg.FindEntry("hide");
    if (entry) _tmHide = *entry;
    else _tmHide = HTNone;
    // where to align
    tmAlign = cfg >> "align";
    tmX = cfg >> "x";
    tmWMin = cfg >> "widthMin";
    tmWMax = cfg >> "widthMax";
    tmY = cfg >> "top";
    tmH = cfg >> "height";
    if (_xboxStyle)
    {
      tmX2 = cfg >> "left2";
      tmY2 = cfg >> "top2";
      tmW2 = cfg >> "width2";
      tmH2 = cfg >> "height2";
    }
#if _VBS3 // command mode in RTE
    tmEditorY = cfg.ReadValue("topInEditor",tmY);
#endif
    menuCheckedColor = GetPackedColor(cfg >> "colorChecked");
    menuEnabledColor = GetPackedColor(cfg >> "colorEnabled");
    menuDisabledColor = GetPackedColor(cfg >> "colorDisabled");
    menuSelectedColor = GetPackedColor(cfg >> "colorSelected");
    menuSelectedTextColor = GetPackedColor(cfg >> "colorSelectedText");
    menuHideTime = cfg >> "hideTime";
    _menuFont = GEngine->LoadFont(GetFontID(cfg >> "font"));
    _menuSize = cfg >> "size";
    _menuShadow = cfg >> "shadow";

    _quickMenuDelay = cfg >> "quickMenuDelay";
  }

  {
    ParamEntryVal cfg = mainCfg >> "Messages";
    msg1Color = GetPackedColor(cfg >> "color1");
    msg2Color = GetPackedColor(cfg >> "color2");
    msg3Color = GetPackedColor(cfg >> "color3");
    msgShadow = cfg >> "shadow";
  }

  {
    ParamEntryVal cfg = mainCfg >> "TacticalDisplay";
    tdX = cfg >> "left";
    tdY = cfg >> "top";
    tdW = cfg >> "width";
    tdH = cfg >> "height";
    _radarGroundTargetTexture = GlobLoadTextureUI(GetPictureName(cfg >> "targetTexture"));
    _radarAirTargetTexture = GlobLoadTextureUI(GetPictureName(cfg >> "targetAirTexture"));
    friendlyColor = GetPackedColor(cfg >> "colorFriendly");
    enemyColor = GetPackedColor(cfg >> "colorEnemy");
    neutralColor = GetPackedColor(cfg >> "colorNeutral");
    civilianColor = GetPackedColor(cfg >> "colorCivilian");
    unknownColor = GetPackedColor(cfg >> "colorUnknown");
    cameraColor = GetPackedColor(cfg >> "colorCamera");

    ParamEntryVal cursor = cfg >> "Cursor";
    tdCurW = cursor >> "width";
    tdCurH = cursor >> "height";
    tdCursorColor = GetPackedColor(cursor >> "color");
    tdCurShadow = cursor >> "shadow"; 

    cfg = mainCfg >> "Radar";
    radarX = cfg >> "left";
    radarY = cfg >> "top";
    radarW = cfg >> "width";
    radarH = cfg >> "height";
    radarShadow = cfg >> "shadow";
    _radarAirBackgroundTexture = GlobLoadTextureUI(GetPictureName(cfg >> "radarAirBackgroundTexture"));
    _radarTankBackgroundTexture = GlobLoadTextureUI(GetPictureName(cfg >> "radarTankBackgroundTexture"));

    _radarAirDangerSector= GlobLoadTextureUI(GetPictureName(cfg >> "radarAirDangerSector"));
    _radarIncommingMissile= GlobLoadTextureUI(GetPictureName(cfg >> "radarIncommingMissile"));
    _radarVehicleTarget= GlobLoadTextureUI(GetPictureName(cfg >> "radarVehicleTarget"));
    _radarTargetingEnemy = GlobLoadTextureUI(GetPictureName(cfg >> "radarTargetingEnemy"));
    _radarFOV = GlobLoadTextureUI(GetPictureName(cfg >> "radarFOV"));

     _turretFOVColor = GetPackedColor(cfg >> "radarFOVCrew");
     _turretPlayerFOVColor = GetPackedColor(cfg >> "radarFOVPlayer");
     _radarLockDangerColor = GetPackedColor(cfg >> "radarLockDangerColor");
     _radarIncommingDangerColor = GetPackedColor(cfg >> "radarIncommingDangerColor");
  }

  {
    ParamEntryVal cfg = mainCfg >> "TankDirection";

    tankX = cfg >> "left";
    tankY = cfg >> "top";
    tankW = cfg >> "width";
    tankH = cfg >> "height";

    tankColor = GetPackedColor(cfg >> "color");
    tankColorFullDammage = GetPackedColor(cfg >> "colorFullDammage");
    tankColorHalfDammage = GetPackedColor(cfg >> "colorHalfDammage");
    tankShadow = cfg >> "shadow";

    _imageTurret = GlobLoadTextureUI(GetPictureName(cfg >> "imageTurret"));
    _imageGun = GlobLoadTextureUI(GetPictureName(cfg >> "imageGun"));
    _imageObsTurret = GlobLoadTextureUI(GetPictureName(cfg >> "imageObsTurret"));

    _imageHull = GlobLoadTextureUI(GetPictureName(cfg >> "imageHull"));
    _imageEngine = GlobLoadTextureUI(GetPictureName(cfg >> "imageEngine"));

    _imageLTrack = GlobLoadTextureUI(GetPictureName(cfg >> "imageLTrack"));
    _imageRTrack = GlobLoadTextureUI(GetPictureName(cfg >> "imageRTrack"));

    _imageMoveStop = GlobLoadTextureUI(GetPictureName(cfg >> "imageMoveStop"));
    _imageMoveBack = GlobLoadTextureUI(GetPictureName(cfg >> "imageMoveBack"));
    _imageMoveForward = GlobLoadTextureUI(GetPictureName(cfg >> "imageMoveForward"));
    _imageMoveFast = GlobLoadTextureUI(GetPictureName(cfg >> "imageMoveFast"));
    _imageMoveLeft = GlobLoadTextureUI(GetPictureName(cfg >> "imageMoveLeft"));
    _imageMoveRight = GlobLoadTextureUI(GetPictureName(cfg >> "imageMoveRight"));
    _imageMoveAuto = GlobLoadTextureUI(GetPictureName(cfg >> "imageMoveAuto"));
  }

  {
    ParamEntryVal cfg = mainCfg >> "FLIRModeNames";
    
      int n = std::min((cfg >> "FLIRModeName").GetSize(), (int)sizeof(_FLIRModeNames));

      for (int i=0; i<n; i++)
        _FLIRModeNames[i] = (cfg >> "FLIRModeName")[i];   
  }

  ProgressRefresh();

  {
    ParamEntryVal cfg = mainCfg >> "GroupDir";
    gd1X = cfg >> "left";
    gd1Y = cfg >> "top";
    gd1W = cfg >> "width";
    gd1H = cfg >> "height";
    ConstParamEntryPtr entry = cfg.FindEntry("left2");
    if (entry) gd2X = *entry;
    else gd2X = gd1X;
    entry = cfg.FindEntry("top2");
    if (entry) gd2Y = *entry;
    else gd2Y = gd1Y;
    entry = cfg.FindEntry("width2");
    if (entry) gd2W = *entry;
    else gd2W = gd1W;
    entry = cfg.FindEntry("height2");
    if (entry) gd2H = *entry;
    else gd2H = gd1H;
    groupDirDimStartTime = cfg >> "dimmStartTime";
    groupDirDimEndTime = cfg >> "dimmEndTime";
    groupDirShadow = cfg >> "shadow";
    _imageGroupDir = GlobLoadTextureUI(GetPictureName(cfg >> "image"));
  }
  ProgressRefresh();

  {
    ParamEntryVal cfg = mainCfg >> "Compass";
    coX = cfg >> "left";
    coY = cfg >> "top";
    coW = cfg >> "width";
    coH = cfg >> "height";
    coShadow = cfg >> "shadow";
    compassColor = GetPackedColor(cfg >> "color");
    
    compassDirColor = GetPackedColor(cfg >> "dirColor");
    compassTurretDirColor = GetPackedColor(cfg >> "turretDirColor");
  }
  
  {
    ParamEntryVal cfg = mainCfg >> "GameInfo";
    giX = cfg >> "left";
    giY = cfg >> "top";
    giW = cfg >> "width";
    giH = cfg >> "height";
  }

  {
    ParamEntryVal cfg = mainCfg >> "PlayerInfo";
    timeColor = GetPackedColor(cfg >> "colorTime");
    piX = cfg >> "left";
    piY = cfg >> "top";
    piW = cfg >> "width";
    piH = cfg >> "height";
    piDimStartTime = cfg >> "dimmStartTime";
    piDimEndTime = cfg >> "dimmEndTime";
    abarW = cfg >> "ArmorBar" >> "width";
    fbarW = cfg >> "FuelBar" >> "width";
    hbarW = cfg >> "HealthBar" >> "width";
    ebarW = cfg >> "ExperienceBar" >> "width";
    ebarColor = GetPackedColor(cfg >> "ExperienceBar" >> "color");
    ppicW = cfg >> "UnitPicture" >> "width";
    ppicH = cfg >> "UnitPicture" >> "height";
    piSideH = cfg >> "Side" >> "height";
    piSideW = cfg >> "Side" >> "width";
    ParamEntryVal sign = cfg >> "Sign";
    piSignH = sign >> "height";
    piSignSW = sign >> "widthSector";
    piSignGW = sign >> "widthGroup";
    piSignUW = sign >> "widthUnit";
    stallWarning = cfg >> "HealthBar" >> "stallWarning";
  }

  {
    ParamEntryVal cfg = mainCfg >> "CommandBar";
    uiX = cfg >> "left";
    uiY = cfg >> "top";
    uiW = cfg >> "width";
    uiH = cfg >> "height";
    groupInfoDim = cfg >> "dimm";
    uiColorNone = GetPackedColor(cfg >> "colorIDNone");
    uiColorNormal = GetPackedColor(cfg >> "colorIDNormal");
    uiColorSelected = GetPackedColor(cfg >> "colorIDSelected");
    uiColorSelecting = GetPackedColor(cfg >> "colorIDSelecting");
    uiColorPlayer = GetPackedColor(cfg >> "colorIDPlayer");
    _imageDefaultWeapons = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "imageDefaultWeapons")
    );
    _imageNoWeapons = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "imageNoWeapons")
    );
    _imageCommander = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "imageCommander")
    );
    _imageDriver = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "imageDriver")
    );
    _imageGunner = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "imageGunner")
    );
    _imageCargo = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "imageCargo")
    );
    _imageSpecialRole= GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "imageDefaultWeapons")
    );

    _prevPageX = cfg >> "prevPage" >> "x";
    _prevPageY = cfg >> "prevPage" >> "y";
    _prevPageW = cfg >> "prevPage" >> "w";
    _prevPageH = cfg >> "prevPage" >> "h";
    _prevPageShadow = cfg >> "prevPage" >> "shadow";
    _prevPageColor = GetPackedColor( cfg >> "prevPage" >> "color");
    _imagePrevPage = GEngine->TextBank()->Load(GetPictureName(cfg >> "prevPage" >> "texture"));

    _nextPageX = cfg >> "nextPage" >> "x";
    _nextPageY = cfg >> "nextPage" >> "y";
    _nextPageW = cfg >> "nextPage" >> "w";
    _nextPageH = cfg >> "nextPage" >> "h";
    _nextPageShadow = cfg >> "nextPage" >> "shadow";
    _nextPageColor = GetPackedColor( cfg >> "nextPage" >> "color");
    _imageNextPage = GEngine->TextBank()->Load(GetPictureName(cfg >> "nextPage" >> "texture"));

    _unitBackgroundX = cfg >> "UnitInfo" >> "UnitBackground" >> "x";
    _unitBackgroundY = cfg >> "UnitInfo" >> "UnitBackground" >> "y";
    _unitBackgroundW = cfg >> "UnitInfo" >> "UnitBackground" >> "w";
    _unitBackgroundH = cfg >> "UnitInfo" >> "UnitBackground" >> "h";
    _unitBackgroundShadow = cfg >> "UnitInfo" >> "UnitBackground" >> "shadow";
    _cmdbarBackground = GLOB_ENGINE->TextBank()->Load
      (
      GetPictureName(cfg >> "UnitInfo" >> "UnitBackground" >>"textureNormal")
      );
    _cmdbarPlayer = GLOB_ENGINE->TextBank()->Load
      (
      GetPictureName(cfg >> "UnitInfo" >> "UnitBackground" >>"texturePlayer")
      );
    _cmdbarSelected = GLOB_ENGINE->TextBank()->Load
      (   
      GetPictureName(cfg >> "UnitInfo" >> "UnitBackground" >> "textureSelected")
      );
    _cmdbarFocus = GLOB_ENGINE->TextBank()->Load
      (   
      GetPictureName(cfg >> "UnitInfo" >> "UnitBackground" >> "textureFocus")
      );

    _cmdbarMCareless = GLOB_ENGINE->TextBank()->Load
      (   
      GetPictureName(cfg >> "UnitInfo" >> "CombatMode" >> "textureMCareless")
      );
    _cmdbarMSafe = GLOB_ENGINE->TextBank()->Load
      (   
      GetPictureName(cfg >> "UnitInfo" >> "CombatMode" >> "textureMSafe")
      );
    _cmdbarMAware = GLOB_ENGINE->TextBank()->Load
      (   
      GetPictureName(cfg >> "UnitInfo" >> "CombatMode" >> "textureMAware")
      );
    _cmdbarMCombat = GLOB_ENGINE->TextBank()->Load
      (   
      GetPictureName(cfg >> "UnitInfo" >> "CombatMode" >> "textureMCombat")
      );
    _cmdbarMStealth = GLOB_ENGINE->TextBank()->Load
      (   
      GetPictureName(cfg >> "UnitInfo" >> "CombatMode" >> "textureMStealth")
      );

    _cmdbarModeX = cfg >> "UnitInfo" >> "CombatMode" >> "x";
    _cmdbarModeY = cfg >> "UnitInfo" >> "CombatMode" >> "y";
    _cmdbarModeW = cfg >> "UnitInfo" >> "CombatMode" >> "w";
    _cmdbarModeH = cfg >> "UnitInfo" >> "CombatMode" >> "h";
    _cmdbarModeShadow = cfg >> "UnitInfo" >> "CombatMode" >> "shadow";


    _unitIconX = cfg >> "UnitInfo" >> "UnitIcon" >> "x";
    _unitIconY = cfg >> "UnitInfo" >> "UnitIcon" >> "y";
    _unitIconW = cfg >> "UnitInfo" >> "UnitIcon" >> "w";
    _unitIconH = cfg >> "UnitInfo" >> "UnitIcon" >> "h";
    _unitIconShadow = cfg >> "UnitInfo" >> "UnitIcon" >> "shadow";
    _unitIconColor =        GetPackedColor(cfg >> "UnitInfo" >> "UnitIcon" >> "color");
    _unitIconColorPlayer =  GetPackedColor(cfg >> "UnitInfo" >> "UnitIcon" >> "colorPlayer");
    _unitIconColorNoAmmo =  GetPackedColor(cfg >> "UnitInfo" >> "UnitIcon" >> "colorNoAmmo");
    _unitIconColorWounded = GetPackedColor(cfg >> "UnitInfo" >> "UnitIcon" >> "colorWounded");
    _unitIconColorNoFuel = GetPackedColor(cfg >> "UnitInfo" >> "UnitIcon" >> "colorNoFuel");
    _unitIconColorWounded2 = GetPackedColor(cfg >> "UnitInfo" >> "UnitIcon" >> "colorWoundedFade");

    _groupIconX = cfg >> "UnitInfo" >> "GroupIcon" >> "x";
    _groupIconY = cfg >> "UnitInfo" >> "GroupIcon" >> "y";
    _groupIconW = cfg >> "UnitInfo" >> "GroupIcon" >> "w";
    _groupIconH = cfg >> "UnitInfo" >> "GroupIcon" >> "h";
    _groupIconShadow = cfg >> "UnitInfo" >> "GroupIcon" >> "shadow";

    _semaphoreX = cfg >> "UnitInfo" >> "Semaphore" >> "x";
    _semaphoreY = cfg >> "UnitInfo" >> "Semaphore" >> "y";
    _semaphoreW = cfg >> "UnitInfo" >> "Semaphore" >> "w";
    _semaphoreH = cfg >> "UnitInfo" >> "Semaphore" >> "h";
    _semaphoreShadow = cfg >> "UnitInfo" >> "Semaphore" >> "shadow";
    _semaphoreColor = GetPackedColor(cfg >> "UnitInfo" >> "Semaphore" >> "color");
    _cmdbarDontFire = GLOB_ENGINE->TextBank()->Load
      (
      GetPictureName(cfg >> "UnitInfo" >> "Semaphore" >> "texture")
      );

    _commandBackgroundX = cfg >> "UnitInfo" >> "CommandBackground" >> "x";
    _commandBackgroundY = cfg >> "UnitInfo" >> "CommandBackground" >> "y";
    _commandBackgroundW = cfg >> "UnitInfo" >> "CommandBackground" >> "w";
    _commandBackgroundH = cfg >> "UnitInfo" >> "CommandBackground" >> "h";
    _commandBackgroundShadow = cfg >> "UnitInfo" >> "CommandBackground" >> "shadow";
    _commandBackgroundColor = GetPackedColor(cfg >> "UnitInfo" >> "CommandBackground" >> "color");
    _cmdbarCommand = GLOB_ENGINE->TextBank()->Load
      (
      GetPictureName(cfg >> "UnitInfo" >> "CommandBackground" >> "texture")
      );

    _unitSpecialRoleX = cfg >> "UnitInfo" >> "UnitSpecialRole" >> "x";
    _unitSpecialRoleY = cfg >> "UnitInfo" >> "UnitSpecialRole" >> "y";
    _unitSpecialRoleW = cfg >> "UnitInfo" >> "UnitSpecialRole" >> "w";
    _unitSpecialRoleH = cfg >> "UnitInfo" >> "UnitSpecialRole" >> "h";
    _unitSpecialRoleShadow = cfg >> "UnitInfo" >> "UnitSpecialRole" >> "shadow";
    _unitSpecialRoleColor = GetPackedColor(cfg >> "UnitInfo" >> "UnitSpecialRole" >> "color");

    _vehicleNumberBackgroundX = cfg >> "UnitInfo" >> "VehicleNumberBackground" >> "x";
    _vehicleNumberBackgroundY = cfg >> "UnitInfo" >> "VehicleNumberBackground" >> "y";
    _vehicleNumberBackgroundW = cfg >> "UnitInfo" >> "VehicleNumberBackground" >> "w";
    _vehicleNumberBackgroundH = cfg >> "UnitInfo" >> "VehicleNumberBackground" >> "h";
    _vehicleNumberBackgroundColor = GetPackedColor(cfg >> "UnitInfo" >> "VehicleNumberBackground" >> "color");
    _vehicleNumberBackgroundShadow = cfg >> "UnitInfo" >> "VehicleNumberBackground" >> "shadow";
    _cmdbarCommander = GLOB_ENGINE->TextBank()->Load
      (
      GetPictureName(cfg >> "UnitInfo" >> "VehicleNumberBackground" >> "texture")
      );

    _unitRoleX = cfg >> "UnitInfo" >> "UnitRole" >> "x";
    _unitRoleY = cfg >> "UnitInfo" >> "UnitRole" >> "y";
    _unitRoleW = cfg >> "UnitInfo" >> "UnitRole" >> "w";
    _unitRoleH = cfg >> "UnitInfo" >> "UnitRole" >> "h";
    _unitRoleShadow = cfg >> "UnitInfo" >> "UnitRole" >> "shadow";
    _unitRoleColor = GetPackedColor(cfg >> "UnitInfo" >> "UnitRole" >> "color");

    _unitNumberBackgroundX = cfg >> "UnitInfo" >> "UnitNumberBackground" >> "x";
    _unitNumberBackgroundY = cfg >> "UnitInfo" >> "UnitNumberBackground" >> "y";
    _unitNumberBackgroundW = cfg >> "UnitInfo" >> "UnitNumberBackground" >> "w";
    _unitNumberBackgroundH = cfg >> "UnitInfo" >> "UnitNumberBackground" >> "h";
    _unitNumberBackgroundShadow = cfg >> "UnitInfo" >> "UnitNumberBackground" >> "shadow";
    _unitNumberBackgroundColor = GetPackedColor( cfg >>"UnitInfo" >> "UnitNumberBackground" >> "color");
    _cmdbarNumber = GLOB_ENGINE->TextBank()->Load
      (
      GetPictureName(cfg >> "UnitInfo" >> "UnitNumberBackground" >> "texture")
      );


    _uiUnitIDFont = GEngine->LoadFont(GetFontID(cfg >> "UnitInfo" >> "UnitNumberText" >> "font"));
    _uiUnitIDSize = cfg >> "UnitInfo" >> "UnitNumberText" >> "SizeEx";
    _uiUnitIDShadow = cfg >> "UnitInfo" >> "UnitNumberText" >> "shadow";
    _uiCommandFont = GEngine->LoadFont(GetFontID(cfg >> "UnitInfo" >> "CommandText" >> "font"));
    _uiCommandSize = cfg >> "UnitInfo" >> "CommandText" >> "SizeEx";
    _uiCommandShadow = cfg >> "UnitInfo" >> "CommandText" >> "shadow";
    _uiHCGroupFont = GEngine->LoadFont(GetFontID(cfg >> "UnitInfo" >> "HcGroupText" >> "font"));
    _uiHCGroupSize = cfg >> "UnitInfo" >> "HcGroupText" >> "SizeEx";
    _uiHCGroupShadow = cfg >> "UnitInfo" >> "HcGroupText" >> "shadow";
    _uiVehicleNumberFont = GEngine->LoadFont(GetFontID(cfg >> "UnitInfo" >> "VehicleNumberText" >> "font"));
    _uiVehicleNumberSize = cfg >> "UnitInfo" >> "VehicleNumberText" >> "SizeEx";
    _uiVehicleNumberShadow = cfg >> "UnitInfo" >> "VehicleNumberText" >> "shadow";

    _uiUnitIDX = cfg >> "UnitInfo" >> "UnitNumberText" >> "x";
    _uiUnitIDY = cfg >> "UnitInfo" >> "UnitNumberText" >> "y";
    _uiHCGroupX = cfg >> "UnitInfo" >> "HcGroupText" >> "x";
    _uiHCGroupY = cfg >> "UnitInfo" >> "HcGroupText" >> "y";
    _uiCommandX = cfg >> "UnitInfo" >> "CommandText" >> "x";
    _uiCommandY = cfg >> "UnitInfo" >> "CommandText" >> "y";
    _uiVehicleNumberX = cfg >> "UnitInfo" >> "VehicleNumberText" >> "x";
    _uiVehicleNumberY = cfg >> "UnitInfo" >> "VehicleNumberText" >> "y";

    ParamEntryVal semaphore = cfg >> "UnitInfo" >> "Semaphore";
    semW = semaphore >> "w";
    semH = semaphore >> "h";
    holdFireColor = GetPackedColor(semaphore >> "color");
    _imageSemaphore = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "UnitInfo" >> "Semaphore" >> "texture")
    );
  }
  ProgressRefresh();

  {
    ParamEntryVal cfg = mainCfg >> "Cursor";
    actW = cfg >> "activeWidth";
    actH = cfg >> "activeHeight";
    actMin = cfg >> "activeMinimum";
    actMax = cfg >> "activeMaximum";
    cursorColor = GetPackedColor(cfg >> "color");
    cursorBgColor = GetPackedColor(cfg >> "colorBackground");
    cursorDim = cfg >> "dimm";
    cursorLockColor = GetPackedColor(cfg >> "colorLocked");
    enemyActColor = GetPackedColor(cfg >> "enemyActiveColor");
    cursorShadow = cfg >> "shadow";
    cursorInfoShadow= cfg >> "infoTextShadow";
    _iconMe = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "me")
    );
    _iconSelect = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "select")
    );
    _iconLeader = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "leader")
    );
    _iconMission = GLOB_ENGINE->TextBank()->Load
    (
      GetPictureName(cfg >> "mission")
    );
    _iconMissionMaxFade = cfg >> "missionFade";

    _iconCustomMark = GEngine->TextBank()->Load
    (
      GetPictureName(cfg >> "customMark")
    );
    _iconAttack = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "assault")
      );
    _iconBoard = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "board")
      );

    _iconUnitUnconscious = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "unitUnconscious")
      );
    _iconBoardIn = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconBoardIn")
      );
    _iconBoardOut = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconBoardOut")
      );
    _iconMove = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconMove")
      );
    _iconJoin = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconJoin")
      );

    _iconRepairAt = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconRepairAt")
      );
    _iconRearmAt = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconRearmAt")
      );
    _iconRefuelAt = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconRefuelAt")
      );
    _iconCallSupport = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconCursorSupport")
      ); 

    _iconHealAt = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconHealAt")
      );
    _iconSupport = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconSupport")
      );
    _iconInFormation = GEngine->TextBank()->Load
      (
      GetPictureName(cfg >> "iconInFormation")
      );

    _iconBoardColor = GetPackedColor(cfg >> "boardColor");
    _iconAttackColor = GetPackedColor(cfg >> "assaultColor");

    _iconMoveColor = GetPackedColor(cfg >> "iconMoveColor");
    _iconJoinColor = GetPackedColor(cfg >> "iconJoinColor");
    _iconHealAtColor = GetPackedColor(cfg >> "iconHealAtColor");
    _iconRepairAtColor = GetPackedColor(cfg >> "iconRepairAtColor");
    _iconRearmAtColor = GetPackedColor(cfg >> "iconRearmAtColor");
    _iconRefuelAtColor = GetPackedColor(cfg >> "iconRefuelAtColor");
    _iconSupportColor = GetPackedColor(cfg >> "iconSupportColor");
    _iconInFormationColor = GetPackedColor(cfg >> "iconInFormationColor");

    ProgressRefresh();

    meColor = GetPackedColor(cfg >> "meColor");
    selectColor = GetPackedColor(cfg >> "selectColor");
    leaderColor = GetPackedColor(cfg >> "leaderColor");
    missionColor1 = GetPackedColor(cfg >> "missionColor1");
    missionColor2 = GetPackedColor(cfg >> "missionColor2");
    customMarkColor = GetPackedColor(cfg >> "customMarkColor");
    unitUnconsciousColor = GetPackedColor(cfg >> "unitUnconsciousColor");
    float blinkingPeriod = cfg >> "blinkingPeriod";
    blinkingSpeed = blinkingPeriod > 0 ? 1.0f / blinkingPeriod : 0;

    meDim = cfg >> "dimmMe";
    meDimStartTime = cfg >> "dimmMeStartTime";
    meDimEndTime = cfg >> "dimmMeEndTime";
    cmdDimStartTime = cfg >> "dimmCmdStartTime";
    cmdDimEndTime = cfg >> "dimmCmdEndTime";

    // TODO: read from the right place
    formDimStartTime = cfg >> "dimmCmdStartTime";
    formDimEndTime = cfg >> "dimmCmdEndTime";
    targetDimStartTime = cfg >> "dimmCmdStartTime";
    targetDimEndTime = cfg >> "dimmCmdEndTime";

    medicDimEndTime = float(cfg >> "dimmCmdEndTime") * 2;

    _cursorFont = GEngine->LoadFont(GetFontID(cfg >> "font"));
    _cursorSize = cfg >> "size";

    ParamEntryVal sign = cfg >> "Sign";
    curSignH = sign >> "height";
    curSignSW = sign >> "widthSector";
    curSignGW = sign >> "widthGroup";
    curSignUW = sign >> "widthUnit";

    _throwCursorMinScale = cfg >> "throwCursorMinScale";
    _throwCursorMaxScale = cfg >> "throwCursorMaxScale";
    _throwCursorFadeSpeed = cfg >> "throwCursorFadeSpeed";
  }

  {
    ParamEntryVal cfg = mainCfg >> "Bar";
    barBgColor = GetPackedColor(cfg >> "colorBackground");
    barGreenColor = GetPackedColor(cfg >> "colorGreen");
    barYellowColor = GetPackedColor(cfg >> "colorYellow");
    barRedColor = GetPackedColor(cfg >> "colorRed");
    barBlinkOnColor = GetPackedColor(cfg >> "colorBlinkOn");
    barBlinkOffColor = GetPackedColor(cfg >> "colorBlinkOff");
    barH = cfg >> "height";
  }

  {
    //HC command cursors
    ParamEntryVal cfg = Pars >> "cfgGroupIcons";
    _hcGroupSelected = GLOB_ENGINE->TextBank()->Load
      (
      GetPictureName(cfg >> "hc_selected" >> "icon")
      );
    _hcGroupSelectable = GLOB_ENGINE->TextBank()->Load
      (
      GetPictureName(cfg >> "hc_selectable" >> "icon")
      );

    _hcGroupSelectedSize = (float(cfg >> "hc_selected" >> "size"))/32.0f;
    _hcGroupSelectableSize = (float(cfg >> "hc_selectable" >> "size"))/32.0f;
    _hcGroupWaypointSize= (float(cfg >> "waypoint" >> "size"))/32.0f;
    _hcWaypoint =  GLOB_ENGINE->TextBank()->Load
      (
      GetPictureName(cfg >> "waypoint" >> "icon")
      );

    _hcGroupSelectableShadow = cfg >> "hc_selectable" >> "shadow";
    _hcGroupSelectedShadow = cfg >> "hc_selected" >> "shadow";

    _maxHCDistanceAlphaEnd = (float(cfg >> "maxHCDistanceAlphaEnd"));
    _maxHCDistanceAlphaStart = (float(cfg >> "maxHCDistanceAlphaStart"));
    _HC3DGroupAlpha = (float(cfg >> "HC3DGroupAlpha"));
  }

  {
    // TODO: into config
    ParamEntryVal cfg = mainCfg >> "CommandBar";
    teamColors[TeamMain] =  GetPackedColor(cfg  >>  "colorWhiteTeam");
    teamColors[TeamRed] = GetPackedColor(cfg  >>  "colorRedTeam");
    teamColors[TeamGreen] = GetPackedColor(cfg  >>  "colorGreenTeam");
    teamColors[TeamBlue] = GetPackedColor(cfg >> "colorBlueTeam");
    teamColors[TeamYellow] = GetPackedColor(cfg >> "colorYellowTeam");
  }

  // TODO: into config
  dragColor = PackedColor(Color(0,1,0,1));

  {
    ParamEntryVal cfg = mainCfg >> "Hint";
    hintDimStartTime = cfg >> "dimmStartTime";
    hintDimEndTime = cfg >> "dimmEndTime";
    GetValue(_hintSound, cfg >> "sound");
  }

  {
    ParamEntryVal cfg = mainCfg >> "TaskHint";
    taskHintDimStartTime = cfg >> "dimmStartTime";
    taskHintDimEndTime = cfg >> "dimmEndTime";
    taskHintDimShowTime = cfg >> "dimShowTime";
  }

  {
    ParamEntryVal cfg = mainCfg >> "ConnectionLost";

    _clX = cfg >> "left";
    _clY = cfg >> "top";
    _clW = cfg >> "width";
    _clH = cfg >> "height";
    _clFont = GEngine->LoadFont(GetFontID(cfg >> "font"));
    _clSize = cfg >> "size";
    _clColor = GetPackedColor(cfg >> "color");
    _clShadow = cfg >> "shadow";
  }

  {
    ParamEntryVal perVision = mainCfg >> "PeripheralVision";
    
    _cueTexture = GlobLoadTextureUI(GetPictureName(perVision>>"cueTexture"));
    _cueColor = GetPackedColor(perVision>>"cueColor");
    _cueEnemyColor = GetPackedColor(perVision>>"cueEnemyColor");
    _cueFriendlyColor = GetPackedColor(perVision>>"cueFriendlyColor");
    _cueShadow = perVision>>"shadow";

    _bloodTexture = GlobLoadTextureUI(GetPictureName(perVision>>"bloodTexture"));
    _bloodColor = GetPackedColor(perVision>>"bloodColor");
  }

  {
    ParamEntryVal hitZones = mainCfg >> "HitZones";
    _hitZones = GlobLoadTextureUI(GetPictureName(hitZones >> "hitZonesTexture"));
    _hitShadow =  hitZones >> "Shadow";
  }

  ProgressRefresh();

  {
    ParamEntryVal cfg = Pars >> "RscInGameUI";
    _weaponReadyColor = GetPackedColor(cfg >> "colorReady");
    _weaponPrepareColor = GetPackedColor(cfg >> "colorPrepare");
    _weaponUnloadColor = GetPackedColor(cfg >> "colorUnload");
  }

#if PROTECTION_ENABLED
  ParamEntryVal fontPars = Pars >> "CfgInGameUI" >> "FadeFont";
  _fadeFont = GEngine->LoadFont(GetFontID(fontPars >> "font"));
  _fadeSize = fontPars >> "size";
  _fadeShadow = fontPars >> "shadow";
#endif

  UIActions::Load(Pars >> "CfgActions");

#if _VBS3_RANKS
  GVBSVisuals.LoadRanks(Pars >> "CfgRanks");
#endif
}

InGameUI::~InGameUI()
{
  _visibleListTemp.Clear();
  _actionsTemp.Clear();

#if SPEEREO_SPEECH
  UnregisterApplication(_idSpeechEngine);
#endif
}

#define SPEERREO_WORD_ENUM(XX) \
  XX("White", SWWhite) \
  XX("Red", SWRed) \
  XX("Green", SWGreen) \
  XX("Blue", SWBlue) \
  XX("Yellow", SWYellow)
/*
  XX("Forward", SWForward) \
  XX("Reverse", SWReverse) \
  XX("Fast", SWFast) \
  XX("Slow", SWSlow) \
  XX("Left", SWLeft) \
  XX("Right", SWRight) \
  XX("Fire", SWFire)
*/
/*
  XX("Column", SWColumn) \
  XX("Stagered Column", SWStageredColumn) \
  XX("Wedge", SWWedge) \
  XX("Echelon Left", SWEchelonLeft) \
  XX("Echelon Right", SWEchelonRight) \
  XX("Vee", SWVee) \
  XX("Line", SWLine)
*/
  /*
  XX("All", SWAll) \
  XX("One", SWOne) \
  XX("Two", SWTwo) \
  XX("Three", SWThree) \
  XX("Four", SWFour) \
  XX("Five", SWFive)
  XX("Six", SWSix) \
  XX("Seven", SWSeven) \
  XX("Eight", SWEight) \
  XX("Nine", SWNine) \
  XX("Ten", SWTen) \
  XX("Eleven", SWEleven) \
  XX("Twelve", SWTwelve) \
  */
  /*
  XX("Switch To", SWSwitchTo) \
  XX("Form", SWForm) \
  XX("At Ease", SWAtEase) \
  XX("Stay Alert", SWStayAlert) \
  XX("Danger", SWDanger) \
  XX("Fire At Will", SWFireAtWill) \
  XX("Go Go Go", SWGoGoGo) \
  XX("Hold Fire", SWHoldFire) \
  XX("Engage At Will", SWEngageAtWill) \
  XX("Get Down", SWGetDown) \
  XX("Stand Up", SWStandUp) \
  XX("Keep Low", SWKeepLow) \
  XX("Advance", SWAdvance) \
  XX("Stay Back", SWStayBack) \
  XX("Flank Left", SWFlankLeft) \
  XX("Flank Right", SWFlankRight) \
  XX("Keep Formation", SWKeepFormation) \
  XX("Report Status", SWReportStatus) \
  XX("Return To Formation", SWReturnToFormation) \
  XX("Get In", SWGetIn) \
  XX("Disembark", SWDisembark) \
  XX("Stop", SWStop) \
  XX("Take Cover", SWTakeCover) \
  */

#define SW_VALUE(name,value) \
  value,

#define SW_NAME(name,value) \
  EnumName(value,name),

enum SpeerreoWord
{
  SPEERREO_WORD_ENUM(SW_VALUE)
  NSpeerreoWord
};

static const EnumName SpeerreoWordNames[] =
{
  SPEERREO_WORD_ENUM(SW_NAME)
  EnumName()
};

template <>
const EnumName *GetEnumNames(SpeerreoWord dummy) {return SpeerreoWordNames;}

LONG InGameUI::ProcessUserMessage(UINT msg, WORD wParam, DWORD lParam)
{
#if SPEEREO_SPEECH
  switch (msg)
  {
  case WM_SRT_CHECKKEY:
    return GenStartKey(wParam, lParam);
  case WM_SRT_MAKEHYPO:
    for (int i=0; i<NSpeerreoWord; i++)
      AddPhrase(SpeerreoWordNames[i].name, SpeerreoWordNames[i].value);
    return NSpeerreoWord;
  case WM_SRT_ACCEPTHYPO:
    DIAG_MESSAGE(500, SpeerreoWordNames[wParam].name);
    return TRUE;
  }

  HWND hwnd = CurrentAppInfoFunctions->GetAppHWnd();
  if (IsWindowUnicode(hwnd))
    return DefWindowProcW(hwnd, msg, wParam, lParam);
  else
    return DefWindowProc(hwnd, msg, wParam, lParam);
#endif
  return 0;
}

AbstractUI *CreateInGameUI() {return new InGameUI;}

/*
const char *InGameUI::GetMissionEnd()
{
  if (_missionEnd && _missionEnd->_title)
  {
    RString text = _missionEnd->_title->GetText();
    if ( text.GetLength() > 0)
      return text;
  }
  return NULL;
}

void InGameUI::SetMissionEnd(const char *text, int colorIndex)
{
  if (_missionEnd && _missionEnd->_title)
  {
    _missionEnd->_title->SetText(text);
    switch (colorIndex)
    {
    case 0:
      _missionEnd->_title->SetFtColor(unknownColor);
      break;
    case 1:
      _missionEnd->_title->SetFtColor(friendlyColor);
      break;
    case 2:
      _missionEnd->_title->SetFtColor(enemyColor);
      break;
    }
  }
}
*/

void AbstractUI::ShowAll(bool show)
{
  _showUnitInfo = show;
  _showTacticalDisplay = show;
  _showCompass = show;
  _showMenu = show;
  _showTankDirection = show;
  _showGroupInfo = show;
}

bool AbstractUI::IsHUDShown() const
{
  return _showCursors;
}

bool IsHUDShown()
{
  if (!GWorld) return true;
  AbstractUI *ui = GWorld->UI();
  if (!ui) return true;
  return ui->IsHUDShown();
}

void InGameUI::ShowSelectedUnits()
{
  for (int i=0; i<_selectedUnits.Size(); i++)
    _selectedUnits[i]._lastSelTime = Glob.uiTime;
}

void InGameUI::Init()
{
  ShowAll();
  _showHUD = true;
  ShowCursors();
  InitMenu();

  ShowFormPosition();
  ShowWaypointPosition();
  ShowTarget();
  ShowCommand();
  ShowSelectedUnits();
  ShowMe();
  ShowFollowMe();

  _actions.Clear();

  _groupInfoChanged = true;
  _groupInfoOffset = 0;
  _groupInfoSelected = -1;

  _menuScroll = false;
}


void InGameUI::ResetVehicle( EntityAIFull *vehicle )
{
  AIBrain *unit = GWorld->FocusOn();
  Person *player = unit ? unit->GetPerson() : NULL;

  TurretContext context;
  bool valid = vehicle->FindTurret(player, context);

  if (valid)
  {
    int weapon = context._weapons->FirstWeapon(vehicle);
    weapon = context._weapons->ValidateWeapon(weapon);
    if (weapon < 0 && !vehicle->IsHandGunSelected())
    {
      // FIX: try to select hand gun when no weapon is selected
      vehicle->SelectHandGun(true);
      weapon = context._weapons->FirstWeapon(vehicle);
      weapon = context._weapons->ValidateWeapon(weapon);
      vehicle->SelectHandGun(false);
    }
    // if the weapon is not weapon, but rather special item, do not select it
    if (weapon >= 0)
    {
      const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
      const WeaponType *type = slot._weapon;
      if (
        !(type->_weaponType & MaskSlotPrimary) &&
        (type->_weaponType & (MaskSlotBinocular | MaskSlotSecondary | MaskSlotInventory)))
      {
        // do not autoselect binocular, secondary weapon or inventory
        weapon = -1;
      }
    }
    if (GLOB_WORLD->PlayerManual())
    {
      context._weapons->SelectWeapon(vehicle, weapon, true);
    }
    context._weapons->FirstCM(vehicle);
  }

//LogF("Select weapon %d (ResetVehicle)", weapon);

  if( vehicle==_target.IdExact() ) _target=NULL;
  if( vehicle==_lockTarget.IdExact() ) _lockTarget = NULL, _lockAimValidUntil = Glob.uiTime-60;
  _lastTarget = NULL;
  _preferedTarget = NULL;

  _modelCursor=VForward;

/*
  // commander should always use eye direction
  Vector3 dir;
  if (!valid)
    dir = vehicle->Direction();
  else if (unit && unit == vehicle->ObserverUnit())
    dir = vehicle->GetEyeDirection(context);
  else if (unit && unit == vehicle->GunnerUnit())
    dir = vehicle->GetWeaponDirection(context, 0);
  else
    dir = vehicle->Direction();
  SetCursorDirection(dir);
*/

//  _timeSendLoad = UITIME_MAX;

  _lastCmdId = -1;
}

void InGameUI::ResetHUD()
{
  _target = NULL;
  _lockTarget = NULL;
  _lastTarget = NULL;
  _preferedTarget = NULL;
  _hcTarget = NULL;

  _artilleryLocked = false;

  _lockAimValidUntil = Glob.uiTime-60;

  _modeAuto = _mode = UIFire;
  _groundPointValid = false;

  _menuScroll = false;

#if !_VBS3_MAPRESETSGROUPSFIX
  ClearSelectedUnits();
  ClearTeams();
#endif
  
  _menu.Resize(0);
  _units = NULL;
  _unitsSingle = NULL;
#if _ENABLE_CONVERSATION
  _conversationContext = NULL;
#endif
  // do not show quick commanding menu
  _showQuickMenu = UITIME_MAX;

  _tmPos = 1;
  _tmIn = false;
  _tmOut = false;

#ifndef _XBOX
  _dragging = false;
#endif
  _mouseDown = false;

  _visibleListTemp.Clear();
  _actionsTemp.Clear();

  _hint->SetHint("");
  _taskHint->ClearHints();

  _drawGroupIcons2D = false;
  _drawGroupIcons3D = false;
  _drawHCCommand = false;

  _lastCmdId = -1;

  _actions.Clear();

  _groupInfoChanged = true;
  _groupInfoOffset = 0;
  _groupInfoSelected = -1;

  _lockedSound.Free();
}

/*!
\patch 5129 Date 2/20/2007 by Jirka
- Fixed: Display all (fading) HUD elements when mission is restarted
*/

void InGameUI::InitMission()
{
  if (_xboxStyle && GWorld->GetMode() != GModeIntro)
  {
    AIBrain *unit = GWorld->FocusOn();
    AIGroup *grp = unit ? unit->GetGroup() : NULL;
    _playerIsLeader = grp && grp->Leader() == unit && grp->UnitsCount() > 1;
    if (_playerIsLeader)
    {
      _units = new Menu();
      _initialMenu = true;
      CreateDynamicMenu(_units, "UNITS");
      ShowMenu();
    }
  }

#if _VBS3_MAPRESETSGROUPSFIX
  // Reset units selection and created groups (like Group Red)
  ClearSelectedUnits();
  ClearTeams();
#endif

  // reset all UI elements
  ShowFormPosition();
  ShowWaypointPosition();
  ShowTarget();
  ShowCommand();
  ShowSelectedUnits();
  ShowMe();
  ShowFollowMe();
  _lastUnitInfos.Clear();
  _lastWeaponInfos.Clear();
}

DEFINE_ENUM_BEG(VCommand)
  VCFire,
  VCMove,
  VCCancelFire,
  VCCancelMove,
  VCWatch
DEFINE_ENUM_END(VCommand)

void InGameUI::IssueVCommand(EntityAI *vehicle, VCommand cmd)
{
  AIBrain *unit = GWorld->FocusOn();
  Transport *transport=dyn_cast<Transport>(vehicle);
  if( !transport ) return ;
  if (transport->CommanderUnit() != unit) return;
  // unit is vehicle commander
  // issue command to vehicle driver or gunner

  switch (cmd)
  {
    case VCMove:
      if ( _groundPointValid)
      {
        if (transport->IsLocal())
          transport->SendMove(_groundPoint);
        else
        {
          RadioMessageVMove msg(transport, _groundPoint);
          GetNetworkManager().SendMsg(&msg);
        }
      }
      break;
    case VCWatch:
      {
        Person *gunner = transport->GunnerUnit() ? transport->GunnerUnit()->GetPerson() : NULL;
        if (gunner)
        {
          if (_target.IdExact())
          {
            if (transport->IsLocal())
              transport->SendWatchTgt(gunner, _target);
            else
            {
              RadioMessageVWatchTgt msg(transport, gunner, _target);
              GetNetworkManager().SendMsg(&msg);
            }
          }
          else
          {
            if (transport->IsLocal())
              transport->SendWatchPos(gunner, _groundPoint);
            else
            {
              RadioMessageVWatchPos msg(transport, gunner, _groundPoint);
              GetNetworkManager().SendMsg(&msg);
            }
          }
        }
      }
      break;
    case VCFire:
      {
        Person *gunner = transport->GunnerUnit() ? transport->GunnerUnit()->GetPerson() : NULL;
        if (gunner)
        {
          if (transport->IsLocal())
            transport->SendFire(gunner, _target);
          else
          {
            RadioMessageVFire msg(transport, gunner, _target);
            GetNetworkManager().SendMsg(&msg);
          }
        }
      }
      break;
    case VCCancelMove:
      if (transport->IsLocal())
        transport->SendJoin();
      else
      {
        RadioMessageVFormation msg(transport);
        GetNetworkManager().SendMsg(&msg);
      }
      break;
    case VCCancelFire:
      {
        Person *gunner = transport->GunnerUnit() ? transport->GunnerUnit()->GetPerson() : NULL;
        SendCommandHelper(transport, gunner, SCCeaseFire);
      }
      break;
  }
}

void InGameUI::IssueWatchDir()
{
  AIBrain *unit = GWorld->FocusOn();
  if (!unit) return;
  AIGroup *grp = unit->GetGroup();
  if (!grp) return;

  OLinkPermNOArray(AIUnit) list;
  ListSelectingUnits(list);

  if (list.Size() == 0)
  {
    // try to issue command to my vehicle
    Transport *transport = unit->GetVehicleIn();
    if (!transport) return;
    if (transport->CommanderUnit() != unit) return;
    IssueVCommand(transport, VCWatch);
  }
  else
  {
    // send command to the selected units
    if (_target.IdExact())
    {
      grp->SendState(new RadioMessageWatchTgt(grp, list, _target));
    }
    else
    {
      grp->SendState(new RadioMessageWatchPos(grp, list, _groundPoint));
    }
  }
}

bool InGameUI::IssueCommand(EntityAI *vehicle, Command::Message cmd, Command::Discretion discretion, Command::Movement movement)
{
  AIBrain *unit = GWorld->FocusOn();

  //AIUnit *unit = vehicle->Brain();
  if (!unit) return true;
  AIGroup *grp = unit->GetGroup();
  if (!grp) return true;

  OLinkPermNOArray(AIUnit) list;
  ListSelectingUnits(list);
  if (cmd == Command::NoCommand)
  { // auto command - command type is selected by context
    switch (_modeAuto)
    {
      case UIStrategySelect:
        {
          EntityAI *vTarget=_target.IdExact();
          Assert( vTarget );
          if (!vTarget) return false; // do not hide menu
          AIUnit *u = vTarget->CommanderUnit() ? vTarget->CommanderUnit()->GetUnit() : NULL;

#if !LSHIFT_SELECT_WHOLE_TEAM
          if (!GInput.keys[DIK_LSHIFT] && !GInput.keys[DIK_RSHIFT]) ClearSelectedUnits();
#else
          ClearSelectedUnits();
#endif
          if (u)
          {
            if (u->GetVehicle() != vehicle)
            {
#if LSHIFT_SELECT_WHOLE_TEAM
              if (GInput.keys[DIK_LSHIFT] || GInput.keys[DIK_RSHIFT])
              {
                Team team = GetTeam(u);
                if (team!=TeamMain)
                {
                  OLinkPermNOArray(AIUnit) list;
                  ListTeam(list, team);
                  for (int i=0; i<list.Size(); i++)
                  {
                    SelectUnit(list[i]);
                  }
                }
                else SelectUnit(u);
              }
              else SelectUnit(u);
#else
              SelectUnit(u);
#endif
              // command individual
              Menu *menu = new Menu();
              if(!_drawHCCommand)
                menu->Load("RscGroupRootMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
              _menu.Resize(1);
              _menu[0] = menu;
#if _ENABLE_CONVERSATION
              _conversationContext = NULL;
#endif
              CheckMenuItems(menu);// _menu.Size() can change inside CheckMenuItems
              _menu[0]->ValidateIndex();
              ShowMenu();
              // do not show quick commanding menu
              _showQuickMenu = UITIME_MAX;
            }
          }
          return false; // do not hide menu
        }
      case UIStrategyMove:
        cmd = Command::Move;
        break;
      case UIStrategyAttack:
      case UIStrategyFire:
        cmd = Command::AttackAndFire;
        break;
      case UIStrategyFireAtPosition:
        cmd = Command::FireAtPosition;
        break;
      case UIStrategyGetIn:
        cmd = Command::GetIn;
        break;
      case UIStrategyGetOut:
        cmd = Command::GetOut;
        break;
      case UIStrategyHeal:
      //  AIUnit *unit =  list[0];
      //  EntityAI *vTarget=_target.IdExact();
        return true;
      case UIStrategyWatch:
        IssueWatchDir();
        return true;
      default:
        return true;
    }
  }

  if (cmd <= 0)
    return true;

  vehicle = unit->GetVehicle();
  // count selected units
  if (list.Size() == 0)
  {
    // try to issue command to my vehicle
    Transport *transport = unit->GetVehicleIn();
    if (!transport) return true;
    if (transport->CommanderUnit() != unit) return true;
    // unit is vehicle commander
    // issue command to vehicle driver or gunner

    switch (cmd)
    {
      case Command::Move:
        IssueVCommand(transport,VCMove);
        //if ( _groundPointValid) transport->SendMove(_groundPoint);
      break;
      case Command::Attack:
      case Command::AttackAndFire:
        IssueVCommand(transport,VCFire);
        // if (_target) transport->SendFire(_target);
      break;
      case Command::Join:
        IssueVCommand(transport,VCCancelMove);
        //transport->SendJoin();
      break;
      /*
      case Command::Join:
        IssueVCommand(transport,VCCancelMove);
        //transport->SendJoin();
      break;
      */
    }
    return true;
  }

  // create command
  Command command;
  command._message = cmd;
  command._context = Command::CtxUI;
  command._discretion = discretion;
  command._movement = movement;
  switch (cmd)
  { // add command type specific parameters
  case Command::Stop:
    // no destination
    break;
  case Command::Move:
    if (_cover)
    {
      // need to offset slightly, so that we get a correct side of the wall
      command._destination = _cover->_coverPos.FastTransform(Vector3(0,0,-1));
      //command._destination = _cover->_coverPos.Position();
      command._movement = Command::MoveFastIntoCover;
      command._precision = 5.0f; // allow cover finding
    }
    else if (_target.IdExact())
    {
      // Move into house
      command._target = _target.IdExact();
      command._intParam = _housePos;
      const IPaths *house = command._target->GetIPaths();
      if (house && _housePos >= 0 && _housePos < house->NPos())
        command._destination = house->GetPosition(house->GetPos(_housePos));
      else
        command._destination = command._target->FutureVisualState().Position();
    }
    else
    {
      if (!_groundPointValid)
        return true;
      command._destination = _groundPoint;
    }
    break;
  case Command::Heal:
    if (!grp->FindHealPosition(command)) return true;
    goto JoinAfterCommand;
  case Command::Repair:
    if (!grp->FindRepairPosition(command)) return true;
    goto JoinAfterCommand;
  case Command::Refuel:
    if (!grp->FindRefuelPosition(command)) return true;
    goto JoinAfterCommand;
  case Command::Rearm:
  {
    for (int i=0; i<list.Size();)
    {
      AIUnit *unit = list[i];
      Assert(unit);
      if (unit->IsSoldier())
      {
        const AITargetInfo *target = unit->CheckAmmo(AIUnit::RSCritical);
        if (target)
        {
          Command cmd;
          cmd._message = Command::Rearm;
          cmd._destination = target->_realPos;
          cmd._target = target->_idExact;
          if (unit->GetSubgroup() == grp->MainSubgroup())
          {
            cmd._context = Command::CtxUIWithJoin;
            cmd._joinToSubgroup = grp->MainSubgroup();
          }
          else
          {
            cmd._context = Command::CtxUI;
          }
          OLinkPermNOArray(AIUnit) list;
          list.Realloc(1);
          list.Resize(1);
          list[0] = unit;
          grp->SendCommand(cmd, list);
        }
        list.Delete(i);
        UnselectUnit(unit);
      }
      else i++;
    }
    if (list.Size() == 0) return true;
    if (!grp->FindRearmPosition(command)) return true;
    goto JoinAfterCommand;
  }
  case Command::Join:
    command._joinToSubgroup = grp->MainSubgroup();
    break;
  case Command::Attack:
  case Command::AttackAndFire:
  {
    // command destination is not used
    // we give commander's destination so that is contains something
    command._destination = vehicle->FutureVisualState().Position();
    command._targetE = _target;
    if (!command._targetE) command._targetE = _lockTarget;
    if (!command._targetE) return true;
    command._direction = command._targetE->AimingPosition() - vehicle->FutureVisualState().Position();
    goto JoinAfterCommand;
  }
  case Command::FireAtPosition:
    {
      Vector3 position;
      Matrix4 origin;
      if (command._target)
      {
        const IPaths *house = command._target->GetIPaths();
        if (house && _housePos >= 0 && _housePos < house->NPos())
          position = house->GetPosition(house->GetPos(_housePos));
        else
          position = command._target->FutureVisualState().Position();
      }
      else
      {
        if (!_groundPointValid)
          return true;
        position= _groundPoint;
      }
      origin.SetPosition(position);

      EntityAI *artilleryTgt = NULL;

      if(grp->Leader())
      {
        switch (grp->Leader()->GetSide())
        {
        case TWest:
          artilleryTgt = GWorld->NewVehicleWithID("ArtilleryTargetE","");
          break;
        case TEast:
          artilleryTgt = GWorld->NewVehicleWithID("ArtilleryTargetW","");
          break;
        case TGuerrila:
          {
            if(grp->Leader()->IsEnemy(TWest)) artilleryTgt = GWorld->NewVehicleWithID("ArtilleryTargetW","");
            else if(grp->Leader()->IsEnemy(TEast)) artilleryTgt = GWorld->NewVehicleWithID("ArtilleryTargetE","");
            else artilleryTgt = GWorld->NewVehicleWithID("ArtilleryTarget","");
            break;
          }
        default:
          artilleryTgt = GWorld->NewVehicleWithID("ArtilleryTarget","");
          break;
        }
      }
      else artilleryTgt = NULL;

      if (artilleryTgt)
      {
        artilleryTgt->Init(origin, true);
        artilleryTgt->SetTransform(origin);
        artilleryTgt->OnEvent(EEInit);
        GWorld->AddVehicle(artilleryTgt);
        GetNetworkManager().CreateVehicle(artilleryTgt, VLTVehicle, "", -1);

        // command destination is not used
        // we give commander's destination so that is contains something
        //command._destination = vehicle->Position();
        command._target = artilleryTgt;
        command._targetE = grp->Leader()->AddTarget(artilleryTgt,FLT_MAX,FLT_MAX,0);
      } 
      else return true;

      if (!command._target) return true;
      if (!command._targetE) return true;
      command._direction = command._target->AimingPosition(command._target->FutureVisualState()) - vehicle->FutureVisualState().Position();
      break;
    }
  case Command::GetOut:
    for (int i=0; i<grp->NUnits(); i++)
    {
      AIUnit *unit = grp->GetUnit(i);
      Assert(unit);
      if (list.FindKey(unit) < 0) continue;
      unit->AllowGetIn(false);
    }
JoinAfterCommand:
    if (CheckJoin(grp))
    {
      command._context = Command::CtxUIWithJoin;
      command._joinToSubgroup = grp->MainSubgroup();
    }
    break;
  case Command::GetIn:
    // find the target
    if (!_target.IdExact())
    {
      Transport *transport = unit->GetVehicleIn();
      if (transport && transport->QCanIGetInAny() )
      {
        command._target = transport;
      }
      else
        return true;
    }
    else
    {
      Transport *transport = dyn_cast<Transport, Object>(_target.IdExact());
      if (transport)
      {
        command._target = transport;
      }
      else
      {
        transport = unit->GetVehicleIn();
        if (transport && transport->QCanIGetInAny())
        {
          command._target = transport;
        }
      }
      
      if(!command._target) return true;
    }

    { // assign vehicle
      Transport *veh = dyn_cast<Transport, EntityAI>(command._target);
      Assert(veh);
      IssueGetIn(grp, list, veh, GIPAny);
    }
    return true;
  }

  grp->SendCommand(command, list);
  return true;
}

TargetSide InGameUI::RadarTargetSide(AIBrain *unit, Target &tar )
{
  TargetSide side = tar.idExact->GetType()->GetTypicalSide();
  if (!tar.idExact->EngineIsOn())
  {
    AIGroup *grp = tar.idExact->GetGroup();
    if (!grp || grp != unit->GetGroup())
    {
      side = TCivilian; // empty marked as civilian
    }
  }

  if
  (
    Glob.config.IsEnabled(DTEnemyTag)
#if _ENABLE_CHEATS
    || _showAll
#endif
  )
  {
    side = tar.GetSide();
    if (tar.IsDestroyed()) side = TCivilian;
  }
  return side;
}

void InGameUI::SelectUnit(AIUnit *unit, bool openMenu)
{
  SelectUnit(unit);
  if(_selectedUnits.Size() == 1 && openMenu)
  {
    Menu *menu = new Menu();
    menu->Load("RscGroupRootMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));

    _menu.Resize(1);
    _menu[0] = menu;
    ShowMenu();
    _menuChanged = true;
    // handled
    _showQuickMenu = Glob.uiTime + _quickMenuDelay;
  }
}

/*!
\patch 1.28 Date 10/24/2001 by Ondra.
- Fixed: Locking missile was possible even on some targets that cannot be locked.
*/

void InGameUI::LockTarget(Target *tgt)
{
  _lockTarget=tgt;
  // we have to disclose and report target
  if (tgt)
  {
    RevealTarget(tgt,0.3);

    _timeSendTarget = Glob.uiTime + 0.3f;
    _lockAimValidUntil=Glob.uiTime-60;
  }
  else
  {
    _timeSendTarget = Glob.uiTime + 0.3f;
  }
}

Target *InGameUI::GetCursorTarget() const
{
  return _target;
}


Target *InGameUI::GetLockedTarget() const
{
  return _lockTarget;
}

Object *InGameUI::GetCursorObject() const
{
  return _cursorObject;
}


/// comparison for radar target sorting
/*!
\patch 5149 Date 3/26/2007 by Ondra
- Fixed: Improved target Tab locking prioritization for aircraft.
*/

class CmpRadarTarget
{
  EntityAIFull *_me;
  AIBrain *_agent;
  Target *_target;
  /// how much do we prefer some direction
  Vector3 _dirPrefer;
  
  public:
  CmpRadarTarget(EntityAIFull *me, AIBrain *agent, Target *target, Vector3Par dirPrefer)
  :_me(me), _agent(agent), _target(target), _dirPrefer(dirPrefer)
  {
  }
  int operator () (const Ref<Target> *t1, const Ref<Target> *t2) const
  {
    const Target *tgt1 = *t1;
    const Target *tgt2 = *t2;
    // preferred target is first
    if (tgt1 == _target) return -1;
    if (tgt2 == _target) return +1;
    // sort by cost
    Vector3 tgt1Dir = tgt1->GetPosition() - _me->FutureVisualState().Position();
    Vector3 tgt2Dir = tgt2->GetPosition() - _me->FutureVisualState().Position();
    tgt1Dir.Normalize();
    tgt2Dir.Normalize();
    // common situation is when there is no preference - in such situation the dot is always zero
    float dir1Factor = _dirPrefer*tgt1Dir+1.0f;
    float dir2Factor = _dirPrefer*tgt2Dir+1.0f;
    float cost1 = floatMax(tgt1->GetType()->GetCost(),tgt1->GetSubjectiveCost())*dir1Factor;
    float cost2 = floatMax(tgt2->GetType()->GetCost(),tgt2->GetSubjectiveCost())*dir2Factor;
    if (cost1>cost2) return -1;
    if (cost1<cost2) return +1;
    // keep sort order stable
    return ComparePointerAddresses(t1,t2);
  }
};

void InGameUI::FindTarget(EntityAIFull *me, const TurretContext &context, bool prev)
{
  Object::ProtectedVisualState<const ObjectVisualState> mevs = me->FutureVisualStateScope(); 

  int weapon = context._weapons->ValidatedCurrentWeapon();
  if (weapon < 0) return;

  Vector3 curDir=me->GetWeaponDirection(*mevs, context, weapon);
  AIBrain *agent = me->CommanderUnit();
  if (!agent) return; // no commander - no TAB switching
  
  // we want to enable switch for gunner but disallow for free soldiers
  if (agent->IsFreeSoldier()) return; // soldier - no TAB switching
/*
#if !_VBS3
  if (agent->IsGunner()) return; // soldier - no TAB switching
#endif
*/

  if (_lockTarget) curDir=_lockTarget->AimingPosition() - mevs->Position();
  else if ( _lockAimValidUntil>=Glob.uiTime ) curDir=_lockAim;
  // check radar visible targets
  // lock only enemy or unknown

  // sort targets based on their "importance"
  // consider cost, and how dangerous they are
  AUTO_STATIC_ARRAY(Ref<Target>, targets, 64);

  // check if new preferred target occurs
  {
    // backup current target
    Target *temp = _lockTarget;
    _lockTarget = NULL;

    Vector3 pos;
    Vector3 cursorDir = me->GetWeaponDirection(*mevs, context, weapon);
    const Camera &camera = *GScene->GetCamera();
    CameraType cam = GWorld->GetCameraType();
    Target *target = CheckCursorTarget(pos, cursorDir, camera, cam, false, false);
    if (target)
    {
      _preferedTarget = target;
    }

    _lockTarget = temp;
  }

  const TargetList *visibleList = VisibleList();
  if (visibleList)
  {
    
    // use Unknown as well
    for (int i=0; i<visibleList->SelectedCount<TgtCatEnemy,TgtCatEnemyUnknown>(); i++)
    {
      TargetNormal *tar = visibleList->GetSelected<TgtCatEnemy,TgtCatEnemyUnknown>(i);
      if (tar->vanished) continue;
      if (tar->destroyed) continue;

      EntityAI *ai = tar->idExact;
      if (!ai || ai->Invisible()) continue;
      if (ai==me) continue; // skip my own vehicle
      if(!ai->GetType()->GetIRTarget() && !ai->GetType()->GetLaserTarget() && !ai->GetType()->GetNvTarget()) continue; // skip non-IR targets
      if(ai->GetType()->GetArtilleryTarget()) continue;
      if (!me->CanLock(ai, context)) continue;
      Vector3Val pos = ai->AimingPosition(ai->FutureVisualState());
      float dist2 = me->FutureVisualState().Position().Distance2(pos);
      float visible = me->CalcVisibility(ai,dist2);
      if (visible<0.01) continue;
      TargetSide side = RadarTargetSide(agent,*tar);
      if (!agent->IsEnemy(side) && side!=TSideUnknown) continue;

      // ignore targets with negative "sorting cost"
      // for some vehicles this can be used to ignore targets outside of the locking cone

      if (!me->AllowLockDir(tar->GetPosition() - mevs->Position()))
      {
        continue;
      }

      targets.Add(tar);
    }
  }

  int limitLockTargets = me->LimitLockTargets();
  if (limitLockTargets>0 && targets.Size()>limitLockTargets)
  {
    targets.Resize(limitLockTargets);
  }
  int targetsCount = targets.Size();
  QSort(targets.Data(), targetsCount, CmpRadarTarget(me, agent, _preferedTarget,  me->GetLockPreferDir()));
  
  // select targets in order 1 2 1 3 1 4 ...
  int minIE = 0;
  if (_lockTarget && targetsCount > 0)
  {
    if (_lockTarget == targets[0])
    {
      // select target after last target which was not best
      LinkTarget lastTarget = _lockTarget;
      // avoid selection of _lockTarget again
      if (_lastTarget && targetsCount > 1)
      {
        if (prev)
        {
          if (_lastTarget != targets[1]) lastTarget = _lastTarget;
        }
        else
        {
          if (_lastTarget != targets[targetsCount - 1]) lastTarget = _lastTarget;
        }
      }
      // find last target
      int currentIndex = -1;
      for (int i=0; i<targets.Size(); i++)
      {
        Target *tar = targets[i];
        if (tar == lastTarget) currentIndex = i;
      }
      // select next/prev
      minIE = currentIndex + (prev ? -1 : 1);
    }
    else
    {
      // select best target
    }
  }
  _lastTarget = _lockTarget;

  if (minIE>=targetsCount) minIE = 0;
  if( minIE<0 || minIE>=targetsCount)
  {
    Matrix3 rotY(MRotationY,prev ? +H_PI/4 : -H_PI/4);
    _lockTarget=NULL;
    _timeSendTarget = Glob.uiTime + 0.3f;
    _lockAim=rotY*curDir;
    _lockAimValidUntil=Glob.uiTime+3.0;
    return; // no target
  }
  Target *tgt = targets[minIE];
  _lockTarget=tgt;
  // we have to disclose and report target
  RevealTarget(tgt,0.3f);

  _timeSendTarget = Glob.uiTime + 0.3f;
  _lockAimValidUntil=Glob.uiTime-60;
}

#define CameraFrame() GScene->GetCamera()

/*!
\patch 5260 Date 7/30/2008 by Bebul
- Fixed: Dedicated server crashed when script made soldier fire with Binocular.
*/

Vector3 InGameUI::GetCursorDirection() const
{
  Vector3 dir;
  const FrameBase *cam = CameraFrame();
  if (cam)
  {
    dir = cam->DirectionModelToWorld(_modelCursor);
  }
  else
  {
    Fail("No camera");
    dir = _modelCursor;
  }
  if (GWorld && GWorld->GetCameraType() != CamGroup)
  {
    Object *camObject = GWorld->CameraOn();
    if (camObject)
    { 
      camObject->OverrideCursor(dir);
    }
    else dir = _modelCursor; // there was fail on dedicated server with camObject==NULL
  }
  return dir;
}

/**
@param dir world space
*/
void InGameUI::SetCursorDirection( Vector3Par dir )
{
  const FrameBase *cam=CameraFrame();
  if( cam )
  {
    Matrix4Val invTransform=cam->GetInvTransform();
    _modelCursor=invTransform.Rotate(dir);
  }
}

bool InGameUI::CheckJoin(AIGroup *grp)
{
  for (int i=0; i<_selectedUnits.Size(); i++)
  {
    AIUnit *u = _selectedUnits[i]._unit;
    if (!u) continue;
    if (u->GetSubgroup() == grp->MainSubgroup()) return true;
//    if (grp->CommandSent(u, Command::Join)) return true;
    // TODO: check stack and radio
  }
  return false;
}

TargetBackupList::TargetBackupList(const MemAllocSA &storage)
{
  SetStorage(storage);
}


void InGameUI::BackupTargets(TargetBackupList &targets)
{
  targets.Clear();
  const TargetList *visibleList=VisibleList();
  if (visibleList) for (int i=0; i<visibleList->AnyCount(); i++)
  {
    Target *target = visibleList->GetAny(i);
    if (!target) continue;
    // minimal targets are handled separately
    if (target->IsMinimal()) continue;
    // ignore invisible targets
    if (!target->idExact || target->idExact->Invisible()) continue;
    targets.Add(target);
  }
}

#define COMMAND_TIMEOUT     480.0   // 8 min

void InGameUI::IssueAction(AIGroup *grp, MenuItem &item)
{
  const IAttribute *attr = FindMenuAttribute("action");
  if (!attr) return;
  Action *action = GetAttrAction(attr);
  TargetType *vehicle = action->GetTarget();

  // send command
  Command cmd;
  cmd._message = Command::Action;
  cmd._context = Command::CtxAuto;
  cmd._action = action;
  cmd._target = vehicle;
  cmd._destination = vehicle ? vehicle->FutureVisualState().Position() : VZero;
  cmd._time = Glob.time + COMMAND_TIMEOUT;

  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);
  grp->SendCommand(cmd, list);
}


void InGameUI::IssueMove(AIGroup *grp)
{
  if (IsEmptySelectedUnits()) return;

  AIUnit *unit = NULL;
  for (int i=0; i<_selectedUnits.Size(); i++)
  {
    AIUnit *u = _selectedUnits[i]._unit;
    if (u)
    {
      unit = u;
      break;
    }
  }
  if (!unit) return;

  const IAttribute *attr = FindMenuAttribute("dist");
  if (!attr) return;
  float dist = GetAttrFloat(attr);

  attr = FindMenuAttribute("dir");
  if (!attr) return;
  float dir = (H_PI / 180.0) * GetAttrFloat(attr);

  Matrix3 rotY(MRotationY, dir);
  Vector3 move = Vector3(0, 0, dist) * rotY;

  Command cmd;
  cmd._message = Command::Move;
  cmd._context = Command::CtxUI;
  cmd._destination = unit->Position(unit->GetFutureVisualState()) + move;
  
  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);
  grp->SendCommand(cmd, list);
}

void InGameUI::IssueHeal()
{
  if (!_target)  return;
  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);

  if (list.Size()!=1) return;

  AIUnit *unitSel = list[0];
  if (!unitSel || !unitSel->GetGroup() || !unitSel->GetVehicle()) return;
  
  if (!_target || !_target.IdExact()) return;
  EntityAI *targetAI = _target.IdExact();
  AIBrain *u = targetAI->CommanderUnit();
  if(!u ||!u->GetUnit()) return;

  EntityAIFull *entity = unitSel->GetVehicle();
  const EntityAIType *type = entity->GetType();

  Command cmd;
  if(type->IsAttendant())
    cmd._message = Command::HealSoldier;
  else if(u->GetLifeState()==LifeStateUnconscious)
    cmd._message = Command::FirstAid;
  else return;
  cmd._time = Glob.time + COMMAND_TIMEOUT;
  targetAI->SetAssignedAttendant(unitSel->GetVehicle());
  cmd._target = targetAI;
  cmd._context = Command::CtxUI;
  cmd._destination = u->Position(u->GetFutureVisualState());

  unitSel->GetGroup()->SendAutoCommandToUnit(cmd, unitSel, true, false);
}

void InGameUI::IssueRepair()
{
  if (!_target)  return;
  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);

  if (list.Size()!=1) return;

  AIUnit *unitSel = list[0];
  if (!unitSel || !unitSel->GetGroup() || !unitSel->GetVehicle()) return;

  if (!_target || !_target.IdExact()) return;
  EntityAI *targetAI = _target.IdExact();

  EntityAIFull *entity = unitSel->GetVehicle();
  const EntityAIType *type = entity->GetType();

  Command cmd;
  if(type->IsEngineer())
    cmd._message = Command::RepairVehicle;
  else return;
  cmd._time = Glob.time + COMMAND_TIMEOUT;
  targetAI->SetAssignedAttendant(unitSel->GetVehicle());
  cmd._target = targetAI;
  cmd._context = Command::CtxUI;
  cmd._destination = targetAI->FutureVisualState().Position();

  unitSel->GetGroup()->SendAutoCommandToUnit(cmd, unitSel, true, false);
}

void InGameUI::IssueTakeBackpack()
{
  if (!_target)  return;
  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);

  if (list.Size()!=1) return;

  AIUnit *unitSel = list[0];
  if (!unitSel || !unitSel->GetGroup() || !unitSel->GetVehicle()) return;

  if (!_target || !_target.IdExact()) return;
  EntityAI *targetAI = _target.IdExact();

  EntityAIFull *entity = unitSel->GetVehicle();

  Command cmd;
  if(entity->CanCarryBackpack() && targetAI->Type()->IsBackpack())
    cmd._message = Command::TakeBag;
  else return;
  cmd._time = Glob.time + COMMAND_TIMEOUT;
  cmd._target = targetAI;
  cmd._context = Command::CtxUI;
  cmd._destination = targetAI->FutureVisualState().Position();

  unitSel->GetGroup()->SendAutoCommandToUnit(cmd, unitSel, true, false);
}

void InGameUI::IssueAssemble()
{
  if (!_target)  return;
  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);

  if (list.Size()!=1) return;

  AIUnit *unitSel = list[0];
  if (!unitSel || !unitSel->GetGroup() || !unitSel->GetVehicle()) return;

  if (!_target || !_target.IdExact()) return;
  EntityAI *targetAI = _target.IdExact();

  Command cmd;
  //assemble weapon
  EntityAI *bag = unitSel->GetVehicle()->GetBackpack();
  const EntityAIType *type = _target.IdExact()->GetType();
  if(bag)
  {
    if(bag->Type()->_assembleInfo.type == PrimaryPart && type->_assembleInfo.type == SecondaryPart)
    {
      if( strcmpi(bag->Type()->_assembleInfo.base, type->GetName())!= 0) return;
    }
    else if(bag->Type()->_assembleInfo.type == SecondaryPart && type->_assembleInfo.type == PrimaryPart)
    {
      if( strcmpi(bag->Type()->GetName(), type->_assembleInfo.base) != 0) return;
    }
  }
  else return;

  cmd._message = Command::Assemble;
  cmd._time = Glob.time + COMMAND_TIMEOUT;
  cmd._target = targetAI;
  cmd._context = Command::CtxUI;
  cmd._destination = targetAI->FutureVisualState().Position();

  unitSel->GetGroup()->SendAutoCommandToUnit(cmd, unitSel, true, false);
}

void InGameUI::IssueDisassemble()
{
  if (!_target)  return;
  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);

  if (list.Size()!=1) return;

  AIUnit *unitSel = list[0];
  if (!unitSel || !unitSel->GetGroup() || !unitSel->GetVehicle()) return;

  if (!_target || !_target.IdExact()) return;
  EntityAI *targetAI = _target.IdExact();

  Command cmd;
  if(targetAI->GetType()->_assembleInfo.type == Assembled)
    cmd._message = Command::DisAssemble;
  else return;
  cmd._time = Glob.time + COMMAND_TIMEOUT;
  cmd._target = targetAI;
  cmd._context = Command::CtxUI;
  cmd._destination = targetAI->FutureVisualState().Position();

  unitSel->GetGroup()->SendAutoCommandToUnit(cmd, unitSel, true, false);
}

void InGameUI::IssueVMove(Transport *vehicle)
{
  if (!vehicle) return;

  const IAttribute *attr = FindMenuAttribute("dist");
  if (!attr) return;
  float dist = GetAttrFloat(attr);

  attr = FindMenuAttribute("dir");
  if (!attr) return;
  float dir = (H_PI / 180.0) * GetAttrFloat(attr);

  Matrix3 rotY(MRotationY, dir);
  Vector3 move = Vector3(0, 0, dist) * rotY;
  Vector3 pos = vehicle->FutureVisualState().Position() + move;

  if (vehicle->IsLocal())
    vehicle->SendMove(pos);
  else
  {
    RadioMessageVMove msg(vehicle, pos);
    GetNetworkManager().SendMsg(&msg);
  }
}

void InGameUI::IssueVWatchTarget(Transport *vehicle, MenuItem &item)
{
  const IAttribute *attr = FindMenuAttribute("target");
  if (!attr) return;
  Target *target = GetAttrTarget(attr);

  if (target)
  {
    Person *gunner = vehicle->GunnerUnit() ? vehicle->GunnerUnit()->GetPerson() : NULL;
    if (gunner)
    {
      if (vehicle->IsLocal())
        vehicle->SendTarget(gunner, target);
      else
      {
        RadioMessageVTarget msg(vehicle, gunner, target);
        GetNetworkManager().SendMsg(&msg);
      }
      _lockTarget = target;
      _timeSendTarget = UITIME_MAX;
      _lockAimValidUntil = Glob.uiTime - 60;
    }
  }
}

void InGameUI::IssueVWatchAuto(Transport *vehicle)
{
  Person *gunner = vehicle->GunnerUnit() ? vehicle->GunnerUnit()->GetPerson() : NULL;
  if (gunner)
  {
    if (vehicle->IsLocal())
      vehicle->SendTarget(gunner, NULL);
    else
    {
      RadioMessageVTarget msg(vehicle, gunner, NULL);
      GetNetworkManager().SendMsg(&msg);
    }
    _lockTarget = NULL;
    _timeSendTarget = UITIME_MAX;
  }
}

void InGameUI::IssueVWatchAround(Transport *vehicle)
{
  // TODO: implementation
}

void InGameUI::IssueWatchTarget(AIGroup *grp, MenuItem &item)
{
  const IAttribute *attr = FindMenuAttribute("target");
  if (!attr) return;
  Target *target = GetAttrTarget(attr);

  if (target)
  {
    OLinkPermNOArray(AIUnit) list;
    ListSelectedUnits(list);
    if (list.Size()>0)
    {
      grp->SendTarget(TargetToFire(target,target->State(list[0])), false, false, list);
    }
  }
}

void InGameUI::IssueWatchAround(AIGroup *grp)
{
  if (IsEmptySelectedUnits()) return;
  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);
  grp->SendState(new RadioMessageWatchAround(grp, list));
}

void InGameUI::IssueEngage(AIGroup *grp)
{
  if (IsEmptySelectedUnits()) return;
  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);
  grp->SendState(new RadioMessageTarget(grp, list, TargetToFire(), true, false, false));
}

void InGameUI::IssueFire(AIGroup *grp)
{
  if (IsEmptySelectedUnits()) return;
  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);
  grp->SendState(new RadioMessageTarget(grp, list, TargetToFire(), false, true, false));
}

void InGameUI::IssueWatchAuto(AIGroup *grp)
{
  if (IsEmptySelectedUnits()) return;
  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);
  grp->SendState(new RadioMessageWatchAuto(grp, list));
}

void InGameUI::SendFireReady(AIBrain *brain, bool ready)
{
  if (!brain) return;
  AIUnit *unit = brain->GetUnit();
  if (!unit) return;
  AIGroup *grp = unit->GetGroup();
  if (!grp) return;
  grp->ReportFire(unit, ready);  
}

/*!
\patch 1.82 Date 8/23/2002 by Ondra
- Fixed: Several fixes in radio submenu 0 - Reply.
- Custom radio key was the same as key for Repeat.
- Mission radio key was the same as key for Copy.
- Done could be issued only when some command was active.
- Negative could be issued only when some command was active.
*/

void InGameUI::SendAnswer(AIBrain *brain, AI::Answer answer)
{
  if (!brain) return;
  AIUnit *unit = brain->GetUnit();
  if (!unit) return;
  AIGroup *grp = unit->GetGroup();
  if (!grp) return;
  if (unit->IsSubgroupLeader())
  {
    AISubgroup *subgrp = unit->GetSubgroup();
    subgrp->SendAnswer(answer);
    subgrp->FailCommand();
    if (unit == grp->Leader())
    {
      // ??? answer to himself
      RadioChannel *activeChannel = &grp->GetRadio();
#if _ENABLE_DIRECT_MESSAGES
      RadioChannel *directChannel = unit->GetPerson()->GetRadio();
      if (directChannel) activeChannel = directChannel;
#endif
      Assert(activeChannel);

      Command temp;
      temp._message = Command::Wait;
      activeChannel->Transmit(new RadioMessageSubgroupAnswer(unit->GetSubgroup(), NULL, answer, &temp, true, unit));
    }
  }
  else
  {
    // active channel selection
    RadioChannel *activeChannel = &grp->GetRadio();
#if _ENABLE_DIRECT_MESSAGES
    AIUnit *leader = grp->Leader();
    if (leader && !unit->UseRadio(leader)) activeChannel = unit->GetPerson()->GetRadio();
#endif
    Assert(activeChannel);

    Command temp;
    temp._message = Command::Wait;
    activeChannel->Transmit(new RadioMessageSubgroupAnswer(unit->GetSubgroup(), NULL, answer, &temp, true, unit));
  }
}

void InGameUI::SendConfirm(AIBrain *brain)
{
  // get command
  AIUnit *unit = brain->GetUnit();
  if (!unit) return;
  AISubgroup *subgrp = unit->GetSubgroup();
  if (!subgrp) return;
  Command *cmd = subgrp->GetCommand();
  Command temp;
  if (!cmd)
  {
    temp._message = Command::Wait;
    cmd = &temp;
  }
  AIGroup *group = subgrp->GetGroup();
  if (!group) return;

  // active channel selection
  RadioChannel *activeChannel = &group->GetRadio();
#if _ENABLE_DIRECT_MESSAGES
  if (unit)
  {
    AIUnit *leader = group->Leader();
    if (leader && !unit->UseRadio(leader)) activeChannel = unit->GetPerson()->GetRadio();
  }
#endif

  Assert(activeChannel);
  activeChannel->Transmit(new RadioMessageCommandConfirm(unit, group, *cmd));
}

void InGameUI::SendResourceState(AIBrain *brain, AI::Answer answer)
{
  AIUnit *unit = brain->GetUnit();
  if (unit) unit->SendAnswer(answer);
  // report some state
}

/*!
\patch 2.01 Date 1/10/2003 by Ondra
- Improved: Menu command "I've got him" now reports only recent kills.
*/

void InGameUI::SendObjectDestroyed(AIBrain *brain, AIGroup *grp)
{
  // find if there is some target to report
  if (!grp) return;
  AIUnit *unit = brain->GetUnit();
  if (!unit) return;
  const TargetList &list = grp->GetTargetList();
  int foundCount = 0;
  // we report only dead enemies here
  for (int i=0; i<list.EnemyCount(); i++)
  {
    TargetNormal *tar = list.GetEnemy(i);
    EntityAI *killer = tar->idKiller;
    if (!killer) continue;
    if
    (
      (killer==unit->GetVehicle() || killer==unit->GetPerson()) &&
      tar->destroyed &&
      tar->timeReported<=TIME_MIN
    )
    {
      // report only recent kills, but report them all
      if (tar->idExact && tar->idExact->GetDestroyedTime()<Glob.time-30)
      {
        continue;
      }
      // report order? most recent kills first?
      // mark as reported
      tar->timeReported = Glob.time;
      tar->posReported = tar->position;
      grp->GetRadio().Transmit(new RadioMessageObjectDestroyed(unit, grp, tar ? tar->type : NULL));
      foundCount++;
    }
  }
  if (foundCount==0)
  {
    grp->GetRadio().Transmit(new RadioMessageObjectDestroyed(unit, grp, NULL));
  }
}

void InGameUI::SendKilled(AIBrain *brain, OLinkPermNOArray(AIUnit) &list)
{
  AIUnit *unit = brain->GetUnit();
  if (!unit) return;
  AIGroup *grp = unit->GetGroup();
  if (!grp) return;
  // TODO: multiple units is killed list
  for (int i=0; i<list.Size(); i++)
  {
    grp->SendUnitDown(unit, list[i]);
  }
}

void InGameUI::IssueWatch(AIGroup *grp)
{
  if (IsEmptySelectedUnits()) return;

  const IAttribute *attr = FindMenuAttribute("dir");
  if (!attr) return;
  float dir = -(H_PI / 180.0) * GetAttrFloat(attr);

  Matrix3 rotY(MRotationY, dir);
  Vector3 tgtDir = rotY.Direction();

  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);
  grp->SendState(new RadioMessageWatchDir(grp,list,tgtDir));
}

void InGameUI::IssueAttack(AIGroup *grp, int tgt)
{
  if (IsEmptySelectedUnits()) return;

  Target *target = _visibleListTemp[tgt];
  if (!target) return;

  AIUnit *unit = NULL;
  for (int i=0; i<_selectedUnits.Size(); i++)
  {
    AIUnit *u = _selectedUnits[i]._unit;
    if (u)
    {
      unit = u;
      break;
    }
  }
  if (!unit) return;

  Command cmd;
  cmd._message = Command::AttackAndFire;
  cmd._targetE = target;
  cmd._destination = unit->Position(unit->GetFutureVisualState());

  for (int i=0; i<_selectedUnits.Size(); i++)
  {
    AIUnit *u = _selectedUnits[i]._unit;
    if (u) u->AssignTarget(TargetToFire(target,target->State(u)));
  }

  if (CheckJoin(grp))
  {
    cmd._context = Command::CtxUIWithJoin;
    cmd._joinToSubgroup = grp->MainSubgroup();
  }
  else
  {
    cmd._context = Command::CtxUI;
  }

  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);
  grp->SendCommand(cmd, list);
}

/// Functor adding free gunner positions
template <class Allocator>
class AddFreeTurrets : public ITurretFunc
{
protected:
  AutoArray<PositionInVehicle, Allocator> &_result;
  bool _commander;

public:
  AddFreeTurrets(AutoArray<PositionInVehicle, Allocator> &result, bool commander):
    _result(result), _commander(commander) {}

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if (!context._turretType->_hasGunner) return false; // continue

    // filter gunners / observers
    bool commander;
    if (context._turretType->_primaryObserver)
      commander = true;
    else if (context._turretType->_primaryGunner)
      commander = false;
    else if (context._weapons->_magazineSlots.Size() == 0)
      commander = true;
    else
      commander = false;
    if (commander != _commander) return false; // continue

    // check if position is already occupied
    AIBrain *gunner = context._turret->GetGunnerAssigned();
    if (gunner && !gunner->GetPerson()->IsDamageDestroyed())
    {
      if (context._gunner) return false; // continue
      if (gunner->GetGroup() && gunner->GetGroup()->CommandSent(gunner->GetUnit(), Command::GetIn)) return false; // continue
    }

    PositionInVehicle &item = _result.Append();
    item._type = GIPTurret; item._turret = context._turret;
    return false; // continue
  }
};

/*!
\patch 5128 Date 2/14/2007 by Jirka
- Fixed: Group leader now can command subordinates to get in secondary turrets
\patch 5153 Date 4/12/2007 by Jirka
- Fixed: AI now gets in vehicles on the positions where dead units are
*/

void InGameUI::IssueGetIn(AIGroup *grp, OLinkPermNOArray(AIUnit) &units, Transport *veh, int pos)
{
  AIBrain *player = GWorld->FocusOn();
  for (int i=0; i<units.Size();)
  {
    AIUnit *unit = units[i];
    if (!unit || unit == player) units.Delete(i);
    else i++;
  }
  if (units.Size() == 0) return;

  // collect possible positions
  bool canAsDriver = false, canAsCommander = false, canAsGunner = false, canAsCargo = false;
  switch (pos)
  {
  case GIPAny:
    canAsDriver = true; canAsCommander = true; canAsGunner = true; canAsCargo = true;
    break;
  case GIPDriver:
    canAsDriver = true;
    break;
  case GIPCommander:
    canAsCommander = true;
    break;
  case GIPGunner:
    canAsGunner = true;
    break;
  case GIPCargo:
    canAsCargo = true;
    break;
  }
  
  AutoArray<PositionInVehicle, MemAllocLocal<PositionInVehicle, 16> > possible;
  if (canAsDriver && veh->GetType()->HasDriver())
  {
    AIBrain *driver = veh->GetDriverAssigned();
    if (driver && !driver->GetPerson()->IsDamageDestroyed())
    {
      if (veh->QIsDriverIn())
        canAsDriver = false;
      else if (driver->GetGroup() && driver->GetGroup()->CommandSent(driver->GetUnit(), Command::GetIn))
        canAsDriver = false;
    }
    if (canAsDriver)
    {
      PositionInVehicle &item = possible.Append();
      item._type = GIPDriver; item._turret = NULL;
    }
  }
  if (canAsCommander)
  {
    AddFreeTurrets< MemAllocLocal<PositionInVehicle, 16> > func(possible, true);
    veh->ForEachTurret(func);
  }
  if (canAsGunner)
  {
    AddFreeTurrets< MemAllocLocal<PositionInVehicle, 16> > func(possible, false);
    veh->ForEachTurret(func);
  }
  if (canAsCargo)
  {
    int n = veh->GetManCargo().Size();
    for (int i=0; i<n; i++)
    {
      AIBrain *cargo = veh->GetCargoAssigned(i);
      // FIX - add also cargo positions with dead units
      // check if position is free
      if (cargo && !cargo->GetPerson()->IsDamageDestroyed())
      {
        if (cargo->GetVehicleIn() == veh)
        {
          // occupied (already in vehicle)
          continue;
        }
        if (cargo->GetGroup() && cargo->GetGroup()->CommandSent(cargo->GetUnit(), Command::GetIn))
        {
          // occupied (getting into vehicle)
          continue;
        }
      }
      // free
      PositionInVehicle &item = possible.Append();
      item._type = GIPCargo;
      item._cargoIndex = i;
    }
  }

  if (possible.Size() == 0) return;

  // assign vehicle to selected units
  OLinkPermNOArray(AIUnit) assigned;
  for (int i=0; i<units.Size(); i++)
  {
    AIUnit *unit = units[i];
    unit->AllowGetIn(true);
    //if (unit->VehicleAssigned() == veh && pos == GIPAny)
    //{
    //  // already assigned, need not to assign once more when any position is good
    //  Hladas -  it is needed, when assigned position is occupied
    //  assigned.Add(unit);
    //  continue;
    //}

    // find the best position this unit can occupy
    int index = -1;
    for (int j=0; j<possible.Size(); j++)
    {
      PositionInVehicle &item = possible[j];
      switch (item._type)
      {
      case GIPDriver:
        if (veh->QCanIGetIn(unit->GetPerson()))
        {
          if (veh->GetGroupAssigned() != grp) grp->AddVehicle(veh);
          unit->AssignAsDriver(veh);
          index = j;
        }
        break;
      case GIPTurret:
        if (veh->QCanIGetInTurret(item._turret, unit->GetPerson()))
        {
          unit->AssignToTurret(veh, item._turret);
          index = j;
        }
        break;
      case GIPCargo: // cargo
        if (veh->QCanIGetInCargo(unit->GetPerson(), item._cargoIndex) == item._cargoIndex)
        {
          unit->AssignAsCargo(veh, item._cargoIndex);
          index = j;
        }
        break;
      }
      if (index >= 0) break;
    }
    if (index >= 0)
    {
      assigned.Add(unit);
      possible.Delete(index);
      if (possible.Size() == 0) break;
    }
  }

  if (assigned.Size() > 0)
  {
    Command cmd;
    cmd._message = Command::GetIn;
    cmd._target = veh;
    cmd._context = Command::CtxUI;
    cmd._movement = Command::MoveFast;
    grp->SendCommand(cmd, assigned);
  }
}

void ActivateSensor(ArcadeSensorActivation activ);

class CollectAction
{
};

void InGameUI::CollectActions(const TargetBackupList &targets, UIActions &actions, bool optimize)
{
  actions.Clear();

  AIUnit *unit = NULL;
  int nUnits = 0;
  for (int i=0; i<_selectedUnits.Size(); i++)
  {
    if (CanProcessActions(_selectedUnits[i]._unit))
    {
      unit = _selectedUnits[i]._unit;
      nUnits++;
    }
  }

  if (nUnits == 0) return;
  if (nUnits == 1)
  {
    targets.ForEach(GatherTargetActionsOneUnit(actions,unit));
    float radius = floatMin(300,Glob.config.horizontZ);
    GatherBuildingActionsOneUnit gather(actions,unit,unit->GetGroup(),radius);
    for (int i=0; i<GLandscape->NBuildings(); i++)
    {
      const StaticEntityLink &info = GLandscape->GetBuildingInfo(i);
      gather(info);
    }
    //ForEachBuilding(GatherBuildingActionsOneUnit(actions,unit))
  }
  else
  {
    // more units selected
    for (int i=0; i<_selectedUnits.Size(); i++)
    {
      AIUnit *unit = _selectedUnits[i]._unit;
      if (CanProcessActions(unit) && unit->GetVehicle()) 
      {
        Object::ProtectedVisualState<const ObjectVisualState> vs = unit->GetVehicle()->RenderVisualStateScope(); 
        unit->GetVehicle()->GetActions(actions, unit, true, false);
      }
    }
    for (int i=0; i<actions.Size(); i++)
    {
      DoAssert(actions[i].action);
      actions[i].action->SetTarget(NULL);
    }
  }

  for (int i=0; i<actions.Size();)
  {
    DoAssert(actions[i].action);
    UIActionType type = actions[i].action->GetType();
    if
    (
      type == ATGetInCommander ||
      type == ATGetInDriver ||
      type == ATGetInPilot ||
      type == ATGetInGunner ||
      type == ATGetInTurret ||
      type == ATGetInCargo ||
      type == ATGetOut
    ) actions.Delete(i);
    else i++;
  }

  if (!optimize) return;

  // aggregation
  for (int i=0; i<actions.Size(); i++)
  {
    UIAction &action1 = actions[i];
    for (int j=i+1; j<actions.Size();)
    {
      UIAction &action2 = actions[j];
      if (action1.HasEqualType(action2))
      {
        // select action with better target
        TargetType *target1 = action1.action->GetTarget();
        TargetType *target2 = action2.action->GetTarget();
        if (target1)
        {
          if (target2)
          {
            // select better target
            Vector3Val pos = unit->Position(unit->GetRenderVisualState());
            if (pos.Distance2(target1->RenderVisualState().Position()) > pos.Distance2(target2->RenderVisualState().Position()))
              action1.action->SetTarget(target2);
          }
          // else action2 has no target - delete it
        }
        else
        {
          // action1 has no target - delete it
          action1.action->SetTarget(target2);
        }
        actions.Delete(j);
      }
      else j++;
    }
  }
  
  // sorting
  actions.Sort();
}

const Menu *InGameUI::GetCurrentMenu() const
{
  int menuSize = _menu.Size();
  if (menuSize > 0) return _menu[menuSize - 1];
  if (_unitsSingle) return _unitsSingle;
  return _units;
}

Menu *InGameUI::GetCurrentMenu()
{
  int menuSize = _menu.Size();
  if (menuSize > 0) return _menu[menuSize - 1];
  if (_unitsSingle) return _unitsSingle;
  return _units;
}

void InGameUI::CheckMenuItems(Menu *menu)
{
  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return;
  AIGroup *group = agent->GetGroup();

  OLinkPermNOArray(AIUnit) selectingUnits;
  ListSelectingUnits(selectingUnits);

  OLinkPermNOArray(AIUnit) list;
  ListSelectedUnits(list);

  bool varIsLeader = group && group->Leader() == agent && group->UnitsCount() > 1;
  bool varIsAlone = !group || group->UnitsCount() == 1;
  bool enableCommands = varIsLeader && agent->LSCanCommand();

  bool varPlayerOwnRadio = GWorld->HasUnitRadio(agent);

  // check list of selected units
  bool varNotEmpty = false;
  bool varNotEmptySoldiers = false;
  bool varNotEmptyCommanders = false;
  bool varNotEmptyMainTeam = false;
  bool varNotEmptyRedTeam = false;
  bool varNotEmptyGreenTeam = false;
  bool varNotEmptyBlueTeam = false;
  bool varNotEmptyYellowTeam = false;
  bool varNotEmptySubgroups = false;
  bool varNotEmptyInVehicle = false;
  bool varSelectedArtillery = false;

  if (enableCommands)
  {
    Assert(group);
    for (int i=0; i<selectingUnits.Size(); i++)
    {
      AIUnit *u = selectingUnits[i];
      if (u)
      {
        varNotEmpty = true;
        if (u->IsFreeSoldier()) varNotEmptySoldiers = true;
        if (u->GetVehicle()->CommanderUnit() == u) varNotEmptyCommanders = true;
/*
        if (u->GetSubgroup() == subgroup)
          notEmptyMySubgroup = true;
*/
        if (u->GetSubgroup() != group->MainSubgroup()) varNotEmptySubgroups = true;
        if (u->GetVehicleIn()) varNotEmptyInVehicle = true;
        if (GWorld->IsArtilleryEnabled() && u->GetVehicleIn() && u->GetVehicleIn()->GetType()->GetArtilleryScanner()) 
          varSelectedArtillery = true;
      }
    }

    for (int i=0; i<group->NUnits(); i++)
    {
      AIUnit *unit = group->GetUnit(i);
      if (!unit) continue;
      switch (GetTeam(unit))
      {
      case TeamMain:
        varNotEmptyMainTeam = true; break;
      case TeamRed:
        varNotEmptyRedTeam = true; break;
      case TeamGreen:
        varNotEmptyGreenTeam = true; break;
      case TeamBlue:
        varNotEmptyBlueTeam = true; break;
      case TeamYellow:
        varNotEmptyYellowTeam = true; break;
      }
    }
  }

  // show / hide radio menu items
  static const int commandRadio[]=
  {
    CMD_RADIO_ALPHA,
    CMD_RADIO_BRAVO,
    CMD_RADIO_CHARLIE,
    CMD_RADIO_DELTA,
    CMD_RADIO_ECHO,
    CMD_RADIO_FOXTROT,
    CMD_RADIO_GOLF,
    CMD_RADIO_HOTEL,
    CMD_RADIO_INDIA,
    CMD_RADIO_JULIET
  };
  bool varHasRadio = false;
  for (int i=0; i<sizeof(commandRadio)/sizeof(*commandRadio); i++)
  {
    MenuItem *item = menu->Find(commandRadio[i]);
    if (item) item->_visible = false;
    if (!(group && group->Leader() == agent)) continue;

    for (int j=sensorsMap.Size()-1; j>=0; j--)
    {
      Vehicle *veh = sensorsMap[j];
      if (!veh) continue;
      Detector *det = dyn_cast<Detector>(veh);
      Assert(det);
      if (det->IsActive() && !det->IsRepeating()) continue;
      if (det->GetActivationBy() - ASAAlpha != commandRadio[i] - CMD_RADIO_ALPHA) continue;

      RString text = det->GetText();
      if (stricmp(text, "null") == 0) continue;

      varHasRadio = true;
      if (item)
      {
        item->_visible = true;
        text = Localize(text);
        if (text.GetLength() > 0)
          item->_text = CreateTextASCII(NULL, text);
        else
          item->_text = item->_baseText;
      }
    }
  }

  EntityAI *vehicle = agent->GetVehicle();
  Transport *transport = agent->GetVehicleIn();
  bool varVehicleCommander = transport && transport->CommanderUnit() == agent;
  bool varCommandsToGunner =
  (
    varVehicleCommander && transport->GunnerUnit() && transport->GunnerUnit()!=agent
  );
  bool varCommandsToPilot =
  (
    varVehicleCommander && transport->PilotUnit() && transport->PilotUnit()!=agent
  );

  bool varIsCommander = agent == vehicle->CommanderUnit();

  bool varIsAloneInVehicle = true;
  if (transport)
  {
    if (transport->DriverBrain() && transport->DriverBrain() != agent) varIsAloneInVehicle = false;
    else if (transport->ObserverBrain() && transport->ObserverBrain() != agent) varIsAloneInVehicle = false;
    else if (transport->GunnerBrain() && transport->GunnerBrain() != agent) varIsAloneInVehicle = false;
    else if (transport->CommanderUnit() && transport->CommanderUnit() != agent) varIsAloneInVehicle = false;
    const ManCargo &manCargo = transport->GetManCargo();
    for (int c=0; c<manCargo.Size(); c++)
    {
      Person *man = manCargo[c];
      if (!man) continue;
      AIBrain *cargoBrain = man->Brain();
      if (cargoBrain && cargoBrain!=agent) varIsAloneInVehicle = false;
    }
  }

  AIUnit *unit = agent->GetUnit();
  AISubgroup *subgroup = unit ? unit->GetSubgroup() : NULL;

  bool varCanAnswer = subgroup && (subgroup->HasCommand() || GWorld->GetMode()==GModeNetware);

  bool varSelectedTeam = _selection == STTeamRed || _selection == STTeamGreen || _selection == STTeamBlue || _selection == STTeamYellow;
  bool varSelectedUnit = _selection == STUnit;

  bool varFuelLow = unit && unit->GetFuelState() >= AIUnit::RSLow;
  bool varAmmoLow = unit && unit->GetAmmoState() >= AIUnit::RSLow;
  bool varInjured = unit && unit->GetHealthState() >= AIUnit::RSLow;

  bool varMultiplayer = GWorld->GetMode() == GModeNetware;

  bool varAreActions = false;
  if (varNotEmpty)
  {
    int targetsStorage[128];
    TargetBackupList targets(MemAllocSA(targetsStorage,sizeof(targetsStorage)));
    UIActions actions;
    BackupTargets(targets);
    CollectActions(targets, actions, false);
    for (int i=0; i<actions.Size(); i++)
    {
      UIAction &action = actions[i];
      RefR<INode> text = action.action->GetText(NULL);
      if (text)
      {
        varAreActions = true;
        break;
      }
    }
  }

  // check what the cursor is pointing at
  bool varCursorOnGroupMember = false;
  bool varCursorOnHoldingFire = false;
  bool varCursorOnEmptyVehicle = false;
  bool varCursorOnVehicleCanGetIn = false;
  bool varCursorOnFriendly = false;
  bool varCursorOnEnemy = false;
  bool varCursorOnEnemyTargeted = false;
  bool varCursorOnEnemyEngaged = false;
  bool varEnemyTargeted = false;
  bool varEnemyEngaged = false;
  bool varCursorOnGround = _groundPointValid;
  bool varCursorOnNotEmptySubgroups = false;
  bool varCursorOnGroupMemberSelected = false;
  bool varPlayerVehicleCanGetIn = false;

  bool varCursorOnNeedFirstAID = false;
  bool varCursorOnNeedHeal = false;
  bool varCursorOnNeedRepair = false;
  bool varCursorOnBackpackCanTake = false;
  bool varCursorOnAssemble = false;
  bool varCursorOnDisassemble = false;

  bool varIsWatchCommanded = GInput.GetAction(UACommandWatch) > 0;

  bool varIsSelectedToAdd = (GInput.keyPressed[DIK_LCONTROL] || GInput.keyPressed[DIK_RCONTROL]);

  AIGroup *grp = agent->GetGroup();
  if (grp)
  {
    varEnemyEngaged = varEnemyTargeted = selectingUnits.Size()>0;
    for (int i=0; i<selectingUnits.Size(); i++)
    {
      AIUnit *u = selectingUnits[i];
      if (u)
      {
        if (varEnemyTargeted)
        {
          Target *tgt = grp->TargetSent(u);
          varEnemyTargeted = tgt && !tgt->IsVanishedOrDestroyed();
        }
        if (varEnemyEngaged)
        {
          Target *tgt = grp->EngageSent(u);
          varEnemyEngaged = tgt && !tgt->IsVanishedOrDestroyed();
        }
      }
    }
  }

  EntityAI *targetAI = _target.IdExact();
  if (targetAI)
  {
    AIBrain *u = targetAI->CommanderUnit();
    if (u && u->GetGroup() && u->GetGroup() == grp)
    {
      // unit from my group
      if(!u->IsGroupLeader()) varCursorOnGroupMember = true;
      varCursorOnHoldingFire = u->IsHoldingFire();
      
      AIUnit *targetUnit = u->GetUnit();
      if(targetUnit && u->GetGroup())
      {
        if (targetUnit->GetSubgroup() != u->GetGroup()->MainSubgroup()) varCursorOnNotEmptySubgroups = true;
      }

      if(u->GetUnit() && list.Find(u->GetUnit())>=0) 
        varCursorOnGroupMemberSelected = true;
    } 

    const VehicleType *typeTarget = GWorld->Preloaded(VTypeTarget);
    if (_target->IsKindOf(typeTarget) || !agent->IsFriendly(_target->GetSide()))
    {
      // enemy target
      varCursorOnEnemy = true;
      if (grp)
      {
        varCursorOnEnemyTargeted = false;
        varCursorOnEnemyEngaged = false;
        for (int i=0; i<selectingUnits.Size(); i++)
        {
          AIUnit *u = selectingUnits[i];
          if (u)
          {
            varCursorOnEnemyTargeted = varCursorOnEnemyTargeted || grp->TargetSent(u)==_target;
            varCursorOnEnemyEngaged = varCursorOnEnemyTargeted || grp->EngageSent(u)==_target;
          }
        }
      }

    }
    else
    {
      // friendly target
      varCursorOnFriendly = true;

      Transport *targetTransport = dyn_cast<Transport>(targetAI);
      varCursorOnEmptyVehicle = targetTransport != NULL;
      
      if (targetTransport && targetTransport->QCanIGetInAny())
      {
        // check if some unit is outside
        for (int i=0; i<selectingUnits.Size(); i++)
        {
          AIUnit *unit = selectingUnits[i];
          if (unit && unit->GetVehicleIn() != targetTransport)
          {
            varCursorOnVehicleCanGetIn = true;
            break;
          }
        }
      }

      if(u && u->IsFreeSoldier() && u->GetUnit() && u->GetPerson())
      {
        AIUnit *unit = u->GetUnit();
        if (selectingUnits.Size() == 1)
        {
          AIUnit *selectedUnit = selectingUnits[0];
          if(selectedUnit && selectedUnit->IsFreeSoldier() && selectedUnit->GetLifeState() != LifeStateUnconscious && selectedUnit->GetVehicle())
          {
            EntityAIFull *entity = selectedUnit->GetVehicle();
            const EntityAIType *type = entity->GetType();

            if ((unit->GetLifeState() == LifeStateUnconscious || u->GetPerson()->NeedsAmbulance()>=0.5) && type->IsAttendant()) 
              varCursorOnNeedHeal = true;
            else if (unit->GetLifeState() == LifeStateUnconscious) 
              varCursorOnNeedFirstAID = true; 
          }
        }
      }

      if(targetTransport && !targetTransport->IsDamageDestroyed())
      {
        if (selectingUnits.Size() == 1)
        {
          AIUnit *selectedUnit = selectingUnits[0];
          if(selectedUnit && selectedUnit->IsFreeSoldier() && selectedUnit->GetLifeState() != LifeStateUnconscious && selectedUnit->GetVehicle())
          {
            EntityAIFull *entity = selectedUnit->GetVehicle();
            const EntityAIType *type = entity->GetType();

            if ( targetTransport->GetMaxHitCont()>0.7f && type->IsEngineer()) 
              varCursorOnNeedRepair = true;
          }
        }
      }
    }

    if(selectingUnits.Size() == 1 && _target->idExact)
    {
      const EntityAIType *type = _target->idExact->GetType();
      AIUnit *u = selectingUnits[0];
      if (u && u->GetVehicle() && u->IsFreeSoldier())
      {
        if(type->IsBackpack())
        {
          //take weapon
          if (u->GetVehicle()->CanCarryBackpack()) varCursorOnBackpackCanTake = true;

          //assemble weapon
          EntityAI *bag = u->GetVehicle()->GetBackpack();
          if(bag)
          {
            if(bag->Type()->_assembleInfo.type == PrimaryPart && type->_assembleInfo.type == SecondaryPart)
            {
              if( strcmpi(bag->Type()->_assembleInfo.base, type->GetName())== 0) varCursorOnAssemble = true;
            }
            else if(bag->Type()->_assembleInfo.type == SecondaryPart && type->_assembleInfo.type == PrimaryPart)
            {
              if( strcmpi(bag->Type()->GetName(), type->_assembleInfo.base) == 0) varCursorOnAssemble = true;
            }
          }
        }
        else if(type->_assembleInfo.type == Assembled)
        { //disassemble weapon
          int count = 0;
          IsCrewEmpty func(count);
          if(!_target->idExact->ForEachCrew(func))  varCursorOnDisassemble = true;
        }
      }

    }
  }
  varPlayerVehicleCanGetIn = (agent->GetVehicleIn() && agent->GetVehicleIn()->QCanIGetInAny());

  bool varCanSelectUnitFromBar = false;
  bool varCanDeselectUnitFromBar = false;
  bool varCanSelectVehicleFromBar = false;
  bool varCanDeselectVehicleFromBar = false;
  bool varCanSelectTeamFromBar = false;
  bool varCanDeselectTeamFromBar = false;

  if (enableCommands && _groupInfoSelected >= 0)
  {
    AIUnit *u = group->GetUnit(_groupInfoSelected);
    if (u && u != agent)
    {
      if (list.Find(u) >= 0)
      {
        // unit is already selected
        Transport *vehicle = u->GetVehicleIn();
        if (vehicle)
        {
          // (de)select vehicle
          bool wholeVehicleSelected = true;
          for (int i=0; i<group->NUnits(); i++)
          {
            AIUnit *unit = group->GetUnit(i);
            if (unit && unit != agent && unit->GetVehicleIn() == vehicle && list.Find(unit) < 0)
            {
              wholeVehicleSelected = false;
              break;
            }
          }
          if (wholeVehicleSelected)
            varCanDeselectVehicleFromBar = true;
          else
            varCanSelectVehicleFromBar = true;
        }
        else
        {
          Team team = GetTeam(u);
          if (team != TeamMain)
          {
            // (de)select team
            bool wholeTeamSelected = true;
            TeamMembers &members = _teamMembers[team];
            for (int i=0; i<members.Size(); i++)
            {
              AIUnit *unit = members[i];
              if (unit && unit != agent && list.Find(unit) < 0)
              {
                wholeTeamSelected = false;
                break;
              }
            }
            if (wholeTeamSelected)
              varCanDeselectTeamFromBar = true;
            else
              varCanSelectTeamFromBar = true;
          }
          else
          {
            // deselect unit
            varCanDeselectUnitFromBar = true;
          }
        }
      }
      else
      {
        // select unit
        varCanSelectUnitFromBar = true;
      }
    }
  }


  bool varHCCanSelectUnitFromBar = false;
  bool varHCCanSelectTeamFromBar = false;

  bool varHCCanDeselectUnitFromBar = false;
  bool varHCCanDeselectTeamFromBar = false;

  if (_groupInfoSelected >= 0 && agent->GetUnit())
  {
    AIUnit *commander =  agent->GetUnit();
    AutoArray<AIHCGroup> &groups = commander->GetHCGroups();
    if(_groupInfoSelected < groups.Size() && groups[_groupInfoSelected].IsSelected())
      varHCCanDeselectUnitFromBar = true;
    else  varHCCanSelectUnitFromBar = true;

    bool wholeTeam = false;
    if(_groupInfoSelected < groups.Size())
    {
      AIHCGroup hcgroup = groups[_groupInfoSelected];
      for (int i=0; i< groups.Size();i++)
      {
        if(_groupInfoSelected != i && (hcgroup.IsInTeam(groups[i]) && !groups[i].IsSelected()))
        {
          wholeTeam = false;
          break;
        }
      }
    }
    if(wholeTeam) varHCCanDeselectTeamFromBar = true;
    else varHCCanSelectTeamFromBar = true;

  }

  bool varHCIsLeader = agent->GetUnit() && agent->GetUnit()->GetHCGroups().Size()>0;
  bool varHCCursorOnIcon = _hcTarget ? true : false;
  bool varHCCursorOnIconSelectable  = false;
  bool varHCNotEmpty = false;
  bool varHCCursorOnIconSelectableSelected = false;
  bool varHCCursorOnIconenemy = false;

  if(_hcTarget && _hcTarget->Leader() && agent)
    varHCCursorOnIconenemy = !_hcTarget->Leader()->IsFriendly(agent->GetSide());
  
  if(agent->GetUnit())
  {//is selectable if group belong under player command
     AutoArray<AIHCGroup> &hcgroups =  agent->GetUnit()->GetHCGroups();
     for(int i=0; i<hcgroups.Size(); i++)
     {
       if(_hcTarget && hcgroups[i].GetGroup() && _hcTarget == hcgroups[i].GetGroup()) 
       {       
         varHCCursorOnIconSelectable = true;
         if(hcgroups[i].IsSelected()) varHCCursorOnIconSelectableSelected = true;
       }
       if(hcgroups[i].IsSelected()) varHCNotEmpty = true;
       if(varHCNotEmpty && varHCCursorOnIconSelectable) break;
     }
  }

  bool varFormationLine = false;
  bool varFormationDiamond = false;
  if (enableCommands && subgroup)
  {
    switch (subgroup->GetFormation())
    {
    case AI::FormLine:
      varFormationLine = true;
      break;
    case AI::FormDiamond:
      varFormationDiamond = true;
      break;
    }
  }

  bool varSomeSelectedHaveTarget = false; 
  bool varSomeSelectedHoldingFire = false;
  if (enableCommands)
  {
    for (int i=0; i<selectingUnits.Size(); i++)
    {
      AIUnit *unit = selectingUnits[i];
      if (unit && unit != agent && unit->IsHoldingFire())
      {
        varSomeSelectedHoldingFire = true;
        break;
      }
      if (unit && unit != agent && unit->GetTargetAssigned())
      {
        varSomeSelectedHaveTarget = true;
        break;
      }
    }
  }

  bool varPlayableLeader = false;
  bool varPlayableSelected = false;
  if (GWorld && GWorld->IsTeamSwitchEnabled())
  {
    // collect the units
    bool multiplayer = GWorld->GetMode() == GModeNetware;
    OLinkPermNOArray(AIBrain) netUnits;
    if (multiplayer) GetNetworkManager().GetSwitchableUnits(netUnits, unit);
    const OLinkPermNOArray(AIBrain) &units = multiplayer ? netUnits : GWorld->GetSwitchableUnits();

    if (selectingUnits.Size() == 1)
    {
      AIUnit *unit = selectingUnits[0];
      if (unit) varPlayableSelected = units.Find(unit) >= 0;
    }

    AIUnit *leader = group ? group->Leader() : NULL;
    if (leader) varPlayableLeader = units.Find(leader) >= 0;
  }

  // check formation
  for (int i=0; i<=AI::NForms; i++)
    menu->CheckCommand(CMD_FORM_COLUMN + i, false);
  if (subgroup) menu->CheckCommand(CMD_FORM_COLUMN + subgroup->GetFormation(), true);

  // check voice channels
  for (int i=0; i<CCDirect; i++)
    menu->CheckCommand(CMD_MP_CHANNEL_GLOBAL + i, false);
  int channel = GetNetworkManager().GetVoiceChannel();
  if (channel >= 0 && channel < CCDirect)
    menu->CheckCommand(CMD_MP_CHANNEL_GLOBAL + channel, true);

  bool varIsTeamSwitch = GWorld->IsTeamSwitchEnabled();

#if _XBOX
  bool varIsXbox = true; 
#else
  bool varIsXbox = false; 
#endif

  /////////////////////////////////////////////////////////////////////////////
  
  const float MenuConditionValues[] =
  {
    MENU_CONDITION_VARIABLES(MENU_CONDITION_VAR_VALUE)
  };
  for (int i=0; i<menu->_items.Size(); i++)
  {
    MenuItem *item = menu->_items[i];
    if (!item) continue;
    if (item->_condVisible.Size() > 0)
    {
      float result;
      item->_condVisible.Evaluate(result, MenuConditionValues, lenof(MenuConditionValues));
      bool visible = result > 0.5f;
      if (visible != item->_visible)
      {
        item->_visible = visible;
        _menuChanged = true;
      }
    }
    if (item->_condEnable.Size() > 0)
    {
      float result;
      item->_condEnable.Evaluate(result, MenuConditionValues, lenof(MenuConditionValues));
      bool enable = result > 0.5f;
      if (enable != item->_enable)
      {
        item->_enable = enable;
        _menuChanged = true;
      }
    }
  }

  // hide obsolete separators
  bool separator = true;
  MenuItem *lastSeparator = NULL; // last visible separator item
  for (int i=0; i<menu->_items.Size(); i++)
  {
    MenuItem* item = menu->_items[i];
    if (item->_cmd == CMD_SEPARATOR)
    {
      item->_visible = !separator;
      separator = true;
      if (item->_visible) lastSeparator = item;
    }
    else if (item->_visible)
    {
      separator = false;
    }
  }
  if (separator && lastSeparator) lastSeparator->_visible = false;

  // do not show menu with all items disabled
  for (int i=_menu.Size()-1; i>0; i--)
  {
    if (menu->_visible && menu->_enable) break;
    menu = _menu[i - 1];
    _menu.Delete(i);
  }

  // do not draw "no menu"
  if (_menu.Size() == 0 && !_units && !_unitsSingle)
  {
    _tmPos = 1;
    _tmOut = false;
    _tmIn = false;
#if _ENABLE_CONVERSATION
    _conversationContext = NULL;
#endif
  }
}

bool InGameUI::IsMenuEmpty()
{
  AIBrain *unit = GWorld->FocusOn();
  if (!unit) return true;
  AIGroup *group = unit->GetGroup();
  if (!group) return true;
  if (group->Leader() == unit && group->UnitsCount() > 1) return false;

  Transport *transport = unit->GetVehicleIn();
  if (transport && transport->CommanderUnit() == unit) return false;

  CheckMenuItems(_basicMenu.GetRef());
  for (int i=0; i<_basicMenu->_items.Size(); i++)
  {
    MenuItem* item = _basicMenu->_items[i];
    if (item->_cmd == CMD_SEPARATOR) continue;
    if (item->_visible && item->_enable) return false;
  }
  return true;
}

bool InGameUI::SelectAllUnits(AIBrain *agent)
{
  AIGroup *group = agent->GetGroup();
  if (!group) return false;

  bool allSelected = true;
  bool mainSubgrpSelected = true;
  for (int i=0; i<group->NUnits(); i++)
  {
    AIUnit *u = group->GetUnit(i);
    if (!u || u == agent) continue;
    if (!IsSelectedUnit(u))
    {
      allSelected = false;
      if (u->GetSubgroup() == group->MainSubgroup())
        mainSubgrpSelected=false;
    }
  }
  if (allSelected)
    ClearSelectedUnits();
  else if (mainSubgrpSelected)
  {
    for (int i=0; i<group->NUnits(); i++)
    {
      AIUnit *u = group->GetUnit(i);
      if (!u || u == agent) continue;
      SelectUnit(u);
    }
  }
  else
  {
    for (int i=0; i<group->NUnits(); i++)
    {
      AIUnit *u = group->GetUnit(i);
      if (!u || u == agent) continue;
      if (u->GetSubgroup() == group->MainSubgroup())
        SelectUnit(u);
      else
        UnselectUnit(u);
    }
  }
  return true;
}

static bool GetShortcutToDo(int key)
{
  // higher priority for the shortcuts - disable other actions mapped to this keys
  static const unsigned char shortcutLevel = 10;

  if (!GInput.InputNotClosed(key, shortcutLevel)) return false; //input is closed (was already read exclusively)
  
  switch (key & INPUT_DEVICE_MASK)
  {
  case INPUT_DEVICE_KEYBOARD:
    return GInput.GetKeyToDo(key - INPUT_DEVICE_KEYBOARD, true, true, shortcutLevel);
  case INPUT_DEVICE_MOUSE:
    return GInput.GetMouseButtonToDo(key - INPUT_DEVICE_MOUSE, true, true, shortcutLevel);
  case INPUT_DEVICE_STICK:
#ifdef _WIN32
    if (!GJoystickDevices) return false;
#endif
    return GInput.GetJoystickButtonToDo(key - INPUT_DEVICE_STICK, true, true, shortcutLevel);
  case INPUT_DEVICE_XINPUT:
    if (!GInput.IsXInputPresent()) return false;
    return GInput.GetXInputButtonToDo(key - INPUT_DEVICE_XINPUT, true, true, shortcutLevel);
  case INPUT_DEVICE_SPECIAL_COMBINATIONS:
    return GInput.GetSpecialCombinationToDo(key - INPUT_DEVICE_SPECIAL_COMBINATIONS, true, true, shortcutLevel);
  }
  return false;
}

/*!
\patch 1.01 Date 06/19/2001 by Jirka
- Added: "NEXT WAYPOINT" command implemented for selected units
- was only for commanded vehicle in 1.00
\patch 1.22 Date 08/27/2001 by Jirka
- Added: hide in-game menu after mouse command or after some time
\patch 1.78 Date 7/10/2002 by Jirka
- Added: Radio commands India & Juliet
*/

//extern GameValue GHCGroupSelectionChanged;
//perform scrip action when unit selection has changed
extern void OnHCSlectionChanged(AIGroup *group, bool selected);


void InGameUI::ProcessMenu(CameraType cam)
{
  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return;

  AIUnit *unit = agent->GetUnit();
  
  _menuChanged = false; // check if the menu content is changing

  bool enabledCommanding = true;
  if(GWorld->GetCameraEffect())
  {
    Vehicle *veh = dynamic_cast<Entity *>(GWorld->GetCameraEffect()->GetObject());
    if(veh) enabledCommanding = (veh->EnabledCommanding());
  }

  if (_xboxStyle && GWorld->GetMode() != GModeIntro && !_playerIsLeader)
  {
    AIGroup *group = agent->GetGroup();
    _playerIsLeader = group && group->Leader() == agent && group->UnitsCount() > 1;
    if (_playerIsLeader && _menu.Size() == 0 && !_units && !_unitsSingle)
    {
      // player becomes leader
      _units = new Menu();
      CreateDynamicMenu(_units, "UNITS");
      ShowMenu();
      _menuChanged = true;
    }
  }

//  Menu *menu = GetCurrentMenu();
//  if ( menu && menu->_visible && (_menuScroll || menu->IsContexSensitive()) 
//    && GInput.GetActionToDo(UAMenuBack,true,true,true,2,/*BLOCK*/true))
//  {
//    //RptF(" UAMenuBack!");
//    int menuSize = _menu.Size();
//    if (menuSize > 1)
//    { 
//      _menu.Delete(--menuSize);
//      CheckMenuItems(_menu[menuSize - 1]); // _menu.Size() can change inside CheckMenuItems
//      _menu[_menu.Size() - 1]->ValidateIndex();
//    }
//    else
//    {
//      ClearSelectedUnits();
//#if _ENABLE_CONVERSATION
//      InterruptConversation(_conversationContext);
//      _conversationContext = NULL;
//#endif
//
//      /*
//      if (_forcingCommandingMode)_forcingCommandingMode=false; //so it starts the "UNITS" menu again
//      else HideMenu();
//      */
//      HideMenu();
//    }
//  }


  bool forcedMenu = false;
  bool susspendCommand = ( (agent->IsSpeaking() || agent->IsListening()) && (GetCurrentMenu() && GetCurrentMenu()->_locked));
  // during the conversation, do not offer the commanding menu
  if (!susspendCommand &&  enabledCommanding)
  {
    if (GInput.GetAction(UAForceCommandingMode) != 0) forcedMenu = true;
    // quick menu invocation
    if (GInput.GetActionToDo(UAForceCommandingMode))
    { 
      // pressed - different reaction based on the menu state
      if (_menu.Size() > 0 && GWorld->GetCameraType() != CamGroup )
      {
        ClearSelectedUnits();
        HideMenu();
        _showQuickMenu = UITIME_MAX;
        _menuChanged = true;
      }
      else
      {
        Menu *menu = new Menu();
        AIGroup *group = agent->GetGroup();
        if(group && !_drawHCCommand)
        {
          for (int i=0; i<group->NUnits(); i++)
          {
            AIUnit *u = group->GetUnit(i);
            if (!u || u == agent) continue;
            SelectUnit(u);
          }
           menu->Load("RscGroupRootMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
        }
        else if(agent->GetUnit())
        {
            AutoArray<AIHCGroup> &groups = agent->GetUnit()->GetHCGroups();
            for (int i=0; i<groups.Size(); i++)
            {
              agent->GetUnit()->SelectHCGroup(i,true);
            }
            menu->Load("RscHCGroupRootMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
        }
        _menu.Resize(1);
        _menu[0] = menu;
        ShowMenu();
        _menuChanged = true;
        // handled
        _showQuickMenu = Glob.uiTime + _quickMenuDelay;
      }
    }
    else if (GInput.GetAction(UAForceCommandingMode) == 0)
    {
      if (Glob.uiTime >= _showQuickMenu)
        HideMenu();
      // released
      _showQuickMenu = UITIME_MAX;
      AIGroup *group = agent->GetGroup();
      if(group && !_drawHCCommand)
      {
        if(!GetCurrentMenu() && _selectedUnits.Size()>0) ClearSelectedUnits();
      }
      else if(group && !GetCurrentMenu() && agent->GetUnit())
      {
        AutoArray<AIHCGroup> &groups = agent->GetUnit()->GetHCGroups();
        for (int i=0; i<groups.Size(); i++)
        {
           agent->GetUnit()->SelectHCGroup(i,false);
        }
      }
    }
    if ( _menu.Size() == 0 && GWorld->GetCameraType() == CamGroup) // force quick menu when commanding camera is on
    {
      // time to invocate quick menu
      Menu *menu = new Menu();
      if(!_drawHCCommand)
        menu->Load("RscGroupRootMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
      else
         menu->Load("RscHCGroupRootMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
      _menu.Resize(1);
      _menu[0] = menu;
      ShowMenu();
      _menuChanged = true;
      // handled
      _showQuickMenu = UITIME_MAX;
    }
  }

  // blink state
  if (Glob.uiTime > _blinkStateChange)
  {
    _blinkStateChange = Glob.uiTime + 0.5;
    _blinkState = !_blinkState;
  }
  
  AIGroup *group = agent->GetGroup();
  bool isLeader = group && group->Leader() == agent && group->UnitsCount() > 1;

  //switch group command bar to high command bar
  if(GInput.GetActionToDo(UASwitchCommand))
  { //switch to HC only if there are some units to command
      if(unit && (unit->GetHCGroups()).Size()>0)
      {
        ShowHCCommand(!_drawHCCommand);
        _groupInfoOffset = 0; //reset page
        if(!_drawHCCommand) unit->UnselectAllHCGroups();  //un-select groups from HC bar
        else
        {//un-select units from bar and hide command menu
          ClearSelectedUnits();
          HideMenu();
        }
      }
  }
  if(_drawHCCommand && (unit->GetHCGroups()).Size()<=0)
  {//make sure, that if all groups are removed, HC bar is disabled
    _groupInfoOffset = 0;
    ShowHCCommand(false);
  }

  //open complex menu with backspace, if nothing else is opened
  if(!GetCurrentMenu() && GInput.GetKeyToDo(DIK_BACK) && enabledCommanding)
  {
    Menu *menu = new Menu();
    if(_drawHCCommand)
      menu->Load("RscHCMainMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
    else 
      menu->Load("RscMainMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
    _menu.Add(menu);
#if _ENABLE_CONVERSATION
    _conversationContext = NULL;
#endif
    CheckMenuItems(menu);// _menu.Size() can change inside CheckMenuItems
    _menu[0]->ValidateIndex();
    ShowMenu();
    _menuChanged = true;
    return;
  }

  // during the conversation, do not offer the commanding menu
  if (!susspendCommand && enabledCommanding)
  {
    // units selection
    if (!_xboxStyle)
    {
      static const int unitKeys[] =
      {
        DIK_F1, DIK_F2, DIK_F3, DIK_F4, DIK_F5, DIK_F6, DIK_F7, DIK_F8, DIK_F9, DIK_F10, DIK_F11, DIK_F12
      };
      if (_drawHCCommand)
      {
        if(!unit) return;
        AutoArray<AIHCGroup> &hcGroups = unit->GetHCGroups();
        if(hcGroups.Size()<=0) {
          _drawHCCommand = false;
          return;
        }
        // Units selection
        bool selectionChanged = false;
        int selectedCount = 0;

        //get first group to draw
        int offset=0;
        for(int i=0; i<hcGroups.Size();i++)
        {
          if(hcGroups[i].ID() < _groupInfoOffset)
            offset++;
          else break;
        }

        //check all ten F-keys (stop if there is no more units)
        for (int i=0; i<10; i++)
        { // - if(there is still some unit  && unit is not from next page)
          if ((offset + i < hcGroups.Size() && hcGroups[offset + i].ID() <_groupInfoOffset + 10))
          { 
            AIHCGroup hcGroup = hcGroups[offset + i];
            {
#if LSHIFT_SELECT_WHOLE_TEAM
              if (GInput.keys[DIK_LSHIFT] || GInput.keys[DIK_RSHIFT])
              {
                //check F-key for current group
                if(GInput.GetKeyToDo(unitKeys[(hcGroup.ID() -_groupInfoOffset)]))
                {  
                  bool selectAll = false;
                  //check if all unit are already selected
                  for(int i= 0;i<hcGroups.Size();i++)
                  {
                    if(hcGroups[i].IsInTeam(hcGroup))
                    {
                      if(hcGroups[i].IsSelected() == false) 
                      {//if not select all (otherwise unselect all)
                        selectAll = true;
                        break;
                      }
                    }
                  }

                  //apply selection
                  for(int i= 0;i<hcGroups.Size();i++) 
                  {
                    if(hcGroups[i].GetGroup() && hcGroups[i].IsInTeam(hcGroup))
                    {
                      if(hcGroups[i].IsSelected()!=selectAll)
                      {
                        unit->SelectHCGroup(i,selectAll);
                        OnHCSlectionChanged(hcGroups[i].GetGroup(),selectAll);
                        selectionChanged = true;
                      }
                    }
                  }
                }
              }
              else 
#endif
              {
                //check F-key for current group
                if(hcGroup.GetGroup() && GInput.GetKeyToDo(unitKeys[(hcGroup.ID() -_groupInfoOffset)]))
                {  
                  unit->SelectHCGroup(offset + i,!hcGroup.IsSelected());
                  selectionChanged = true;
                  OnHCSlectionChanged(hcGroup.GetGroup(),!hcGroup.IsSelected());
                }
              }
            }
          }
        }
        if (!selectionChanged)
        {//move to next/previous page
          if (GInput.GetKeyToDo(unitKeys[10])&& _groupInfoOffset > 0 )
          {
            _groupInfoOffset -= 10;
          }
          if (GInput.GetKeyToDo(unitKeys[11]) && _groupInfoOffset + 10 <= hcGroups[hcGroups.Size()-1].ID())
          {
            _groupInfoOffset += 10;
          }
        }
        if (GInput.GetActionToDo(UASelectAll))
        {
          bool selectAll = false;
          //check if all unit are already selected
          for(int i= 0;i<hcGroups.Size();i++)
          {
            if(hcGroups[i].IsSelected() == false) 
            {//if not select all (otherwise unselect all)
              selectAll = true;
              break;
            }
          }

          //apply selection
          for(int i= 0;i<hcGroups.Size();i++) 
          {
            if(hcGroups[i].GetGroup() && hcGroups[i].IsSelected()!=selectAll)
            {
              unit->SelectHCGroup(i,selectAll);
              OnHCSlectionChanged(hcGroups[i].GetGroup(),selectAll);
              selectionChanged = true;
            }
          }
        }
        hcGroups = unit->GetHCGroups();
        for(int i= 0;i<hcGroups.Size();i++) 
          if(hcGroups[i].IsSelected())  
          {
            selectedCount++; break;
          }

        if (selectionChanged && selectedCount>0)
        { //Fn key not used for unit selection. So show toggle commanding menu
          if (_menu.Size() > 0 && GetCurrentMenu())
          { // if dynamic menu is opened, reload items
            RString menuName = GetCurrentMenu()->GetName();
            if(selectedCount>0 && menuName.GetLength()>0 && menuName[0]=='#')
            {//remove current old dynamic menu
              _menu.Resize(_menu.Size()-1);
              Menu *menu = new Menu();
              CreateDynamicMenu(menu, menuName.Substring(1,menuName.GetLength()).Data());
              _menu.Add(menu);
#if _ENABLE_CONVERSATION
              _conversationContext = NULL;
#endif
              CheckMenuItems(menu);// _menu.Size() can change inside CheckMenuItems
              _menu[0]->ValidateIndex();
              ShowMenu();
              _menuChanged = true;
            }
          }
          else if (_menu.Size() == 0 && selectedCount>0)
          {
            Menu *menu = new Menu();
            menu->Load("RscHCGroupRootMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
            _menu.Add(menu);
          }
          if(selectedCount>0) ShowMenu();
          _menuChanged = true;
        }
        else if (selectionChanged && selectedCount<=0 && GWorld->GetCameraType() != CamGroup)
        {
          HideMenu();
        }
      }
      else if (isLeader)
      {
        int n = group->NUnits();
        // avoid listing through empty pages on the end
        while (n > 0)
        {
          AIUnit *u = group->GetUnit(n - 1);
          bool valid = u && u->GetSubgroup() && !group->GetReportedDown(u);
          if (valid) break;
          // we can safely ignore this unit
          n--;
        }

        // Units selection
        bool selectionChanged = false;
        for (int i=0; i<10; i++)
        {
          bool keyFnPressed = GInput.GetKeyToDo(unitKeys[i]);
          if (keyFnPressed)
          {
#if LSHIFT_SELECT_WHOLE_TEAM
            if (GInput.keys[DIK_LSHIFT] || GInput.keys[DIK_RSHIFT])
            {
              Team team = i<NTeams-TeamMain-1 ? Team(i+TeamMain+1) : TeamMain;
              OLinkPermNOArray(AIUnit) list;
              ListTeam(list, team);
              bool allMembersSelected = true;
              for (int i=0; i<list.Size(); i++)
              {
                if (!IsSelectedUnit(list[i]))
                {
                  allMembersSelected = false;
                  break;
                }
              }
              for (int i=0; i<list.Size(); i++)
              {
                if (allMembersSelected) UnselectUnit(list[i]);
                else SelectUnit(list[i]);
              }
              selectionChanged = true;
            }
            else if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
            {
              Team team = i<NTeams-TeamMain-1 ? Team(i+TeamMain+1) : TeamMain;
              if (_selectedUnits.Size()>0)
              {
                AssignTeam(agent,team);
                if (!forcedMenu && GWorld->GetCameraType() != CamGroup)
                {
                  ClearSelectedUnits();
                  HideMenu();
                }
                
              }

              selectionChanged = true;
            }
            else 
#endif
            {
              if (_groupInfoOffset + i < n)
              {
                AIUnit *u = group->GetUnit(_groupInfoOffset + i);
                if (u && !u->IsPlayer())
                {
                  ToggleSelection(group, _groupInfoOffset + i);
                  selectionChanged = true;
                }
              }
            }
            if (!selectionChanged && _selectedUnits.Size()==0)
            { //Fn key not used for unit selection. So show toggle commanding menu
              if ((_tmIn || _tmPos==0) && GWorld->GetCameraType() != CamGroup) HideMenu();
              else
              {
                if (_menu.Size() == 0)
                {
                  Menu *menu = new Menu();
                  menu->Load("RscGroupRootMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  _menu.Add(menu);
                }
                ShowMenu();
                _menuChanged = true;
              }
            }
          }
        }

        if (!selectionChanged)
        {
          if (_groupInfoOffset > 0 && GInput.GetKeyToDo(unitKeys[10]))
          {
            _groupInfoOffset -= 10;
            selectionChanged = true;
          }
          if (_groupInfoOffset + 10 < n && GInput.GetKeyToDo(unitKeys[11]))
          {
            _groupInfoOffset += 10;
            selectionChanged = true;
          }
        }
        if (GInput.GetActionToDo(UASelectAll))
        {
          selectionChanged = SelectAllUnits(agent);
        }
        if(selectionChanged && _selectedUnits.Size()<=0 && GWorld->GetCameraType() != CamGroup)
        {     
          HideMenu();
        }
        if (selectionChanged)
        {
          if (_menu.Size() > 0 && GetCurrentMenu())
          { // if dynamic menu is opened, reload items
            RString menuName = GetCurrentMenu()->GetName();
            if(menuName.GetLength()>0 && menuName[0]=='#')
            {//remove current old dynamic menu
              _menu.Resize(_menu.Size()-1);
              Menu *menu = new Menu();
              CreateDynamicMenu(menu, menuName.Substring(1,menuName.GetLength()).Data());
              _menu.Add(menu);
#if _ENABLE_CONVERSATION
              _conversationContext = NULL;
#endif
              CheckMenuItems(menu);// _menu.Size() can change inside CheckMenuItems
              _menu[0]->ValidateIndex();
              if(!IsEmptySelectedUnits()) ShowMenu();
              _menuChanged = true;
            }
          }
          else if (_menu.Size() == 0 && !IsEmptySelectedUnits())
          {
            Menu *menu = new Menu();
            menu->Load("RscGroupRootMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
            _menu.Add(menu);
          }
          if(_selectedUnits.Size()>0) ShowMenu();
          _menuChanged = true;
        }
      }
      else // not group leader
      {
        for (int i=0; i<sizeof(unitKeys)/sizeof(*unitKeys); i++)
        {
          if (GInput.GetKeyToDo(unitKeys[i]))
          {
            if (_menu.Size() == 0)
            {
              Menu *menu = new Menu();
              menu->Load("RscMainMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
              _menu.Add(menu);
            }
            ShowMenu();
            _menuChanged = true;
            break;
          }
        }
        ClearSelectedUnits();
      }
    }

    if (!isLeader)
    {
      ClearSelectedUnits();
    }

    // show / hide menu
    if (_xboxStyle)
    {
      if (_menu.Size() == 0)
      {
        if (IsMenuEmpty())
        {
          if (_units) HideMenu();
          else return;
        }
        else if (!_units)
        {
          // select units
          if
            (
            GInput.GetActionToDo(UAMenuSelect) ||
            //        || !_actions.IsListVisible() && GInput.GetActionToDo(UAMenuBack)
            GInput.GetActionToDo(UASelectAll)
            )
          {
            _units = new Menu();
            CreateDynamicMenu(_units, "UNITS");
            ShowMenu();
            _menuChanged = true;
          }
          return;
        }
      }
    }
    else if(enabledCommanding)
    {
      Menu *currentMenu = GetCurrentMenu();
      if (_menu.Size() == 0 
        || (currentMenu && strcmpi(currentMenu->GetName(),"rscgrouprootmenu")==0)
        || (currentMenu && strcmpi(currentMenu->GetName(),"rschcgrouprootmenu")==0))
      {
        static const int shortcuts[] =
        {
          DIK_1, DIK_2, DIK_3, DIK_4, DIK_5, DIK_6, DIK_7, DIK_8, DIK_9, DIK_0
        };
        int shortcut = -1;
        for (int i=0; i<sizeof(shortcuts)/sizeof(*shortcuts); i++)
        {
          if (GInput.GetKeyToDo(shortcuts[i], false, true, 10)) // pass shortcut level equal to GetShortcutToDo
          {
            shortcut = i+1;
            break;
          }
        }
        if(shortcut>0)
        {
          Menu *menu = new Menu();
          if(!_drawHCCommand)
          {
            if(unit && unit->GetGroup() && unit->GetGroup()->Leader() == unit && unit->GetGroup()->NUnits()>1)
            {
              switch (shortcut)
              {
              case 1:
                {
                  menu->Load("RscMoveHigh", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  break;
                }
              case 2:
                {
                  CreateDynamicMenu(menu, "WATCH");
                  break;
                }
              case 3:
                {
                  menu->Load("RscWatchDir", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  break;
                }
              case 4:
                {
                  CreateDynamicMenu(menu, "GET_IN");
                  break;
                }
              case 5:
                {
                  menu->Load("RscStatus", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  break;
                }
              case 6:
                {
                  CreateDynamicMenu(menu, "ACTION");
                  break;
                }
              case 7:
                {
                  menu->Load("RscCombatMode", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  break;
                }
              case 8:
                {
                  menu->Load("RscFormations", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  break;
                }
              case 9:
                {
                  menu->Load("RscTeam", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  break;
                }
              case 10:
                {
                  menu->Load("RscReply", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  break;
                }
              }
            }
            else
            {
              switch (shortcut)
              {
              case 5:
                {
                  menu->Load("RscStatus", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  break;
                }
              case 10:
                {
                  menu->Load("RscReply", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  break;
                }
              default:
                {
                  menu->Load("RscMainMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                  break;
                }
              }
            }
          }
          else
          {
            switch (shortcut)
            {
            case 1:
              {
                menu->Load("RscHCMoveHigh", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                break;
              }
            case 2:
              {
                CreateDynamicMenu(menu, "USER:HC_Targets_0");
                break;
              }
            case 3:
              {
                menu->Load("RscHCWatchDir", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                break;
              }
            case 4:
              {
                menu->Load("RscHCSpeedMode", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                break;
              }
            case 5:
              {
                CreateDynamicMenu(menu, "USER:HCMissions_0");
                break;
              }
            case 6:
              {
                CreateDynamicMenu(menu, "USER:HC_Custom_0");
                break;
              }
            case 7:
              {
                menu->Load("RscHCCombatMode", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                break;
              }
            case 8:
              {
                menu->Load("RscHCFormations", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                break;
              }
            case 9:
              {
                menu->Load("RscHcTeam", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                break;
              }
            case 10:
              {
                menu->Load("RscHCReply", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
                break;
              }
            }
          }
          _menu.Add(menu);
#if _ENABLE_CONVERSATION
          _conversationContext = NULL;
#endif
          CheckMenuItems(menu);// _menu.Size() can change inside CheckMenuItems
          _menu[0]->ValidateIndex();
          ShowMenu();
          _menuChanged = true;
          return;
        }
        else if(_menu.Size() == 0) return;
      }
    }
  }
  else
  {
    // during conversation
    if (_menu.Size() == 0) 
      return;
  }

  if (Glob.uiTime >= _lastMenuTime + menuHideTime && !_tmOut)
  {
    if (_xboxStyle) ClearSelectedUnits();
#if _ENABLE_CONVERSATION
    InterruptConversation(_conversationContext);
#endif
    HideMenu();
  }
  
  if (_tmIn)
  {
    if (_tmPos > 0)
    {
      _tmPos -= 6.0 * (Glob.uiTime - _tmTime);
      if (_tmPos <= 0)
      {
        _tmPos = 0;
        _tmIn = false;
      }
    }
  }
  else if (_tmOut)
  {
    if (_tmPos < 1)
    {
      _tmPos += 6.0 * (Glob.uiTime - _tmTime);
      if (_tmPos >= 1)
      {
        _tmPos = 1;
        _tmOut = false;

        _menu.Resize(0);
        _units = NULL;
        _unitsSingle = NULL;
#if _ENABLE_CONVERSATION
        _conversationContext = NULL;
#endif
      }
    }
    return;
  }

  Assert(_menu.Size() > 0 || _units);

  Menu *menuCurrent = GetCurrentMenu();
  CheckMenuItems(menuCurrent);

  // update values
  menuCurrent = GetCurrentMenu();

  // Validate menu index
  menuCurrent->ValidateIndex();

  // Menu actions
  for (int i=0; i<menuCurrent->_items.Size(); i++)
  {
    MenuItem* item = menuCurrent->_items[i];
    if (item->_cmd == CMD_SEPARATOR) continue;
    if (!item->_visible || !item->_enable) continue;
    bool shortcut = false;
    for (int j=0; j<item->_shortcuts.Size(); j++)
    {
      if (GetShortcutToDo(item->_shortcuts[j]))
      {
        shortcut = true;
        break;
      }
    }

    if (shortcut)
    {
      int oldSelected = menuCurrent->_selected;
      menuCurrent->_selected = i;
      bool resetMenu;
      if (ProcessMenuItem(item, resetMenu, false))
      {
        // menu processed - can hide
        if (_xboxStyle && cam == CamGroup)
        {
          _menu.Resize(0);
#if _ENABLE_CONVERSATION
          _conversationContext = NULL;
#endif
          // leave _units menu active
        }
        else
        {
          if (!forcedMenu && GWorld->GetCameraType() != CamGroup)
          {
            ClearSelectedUnits();
            HideMenu();
          }
          /*
          if (_forcingCommandingMode) _forcingCommandingMode=false; //show UNITS menu
          else HideMenu();
          */
        }
      }
      else if (resetMenu)
      {
        _menu.Resize(1);
        CheckMenuItems(_menu[0]);
        _menu[0]->ValidateIndex();
        _menuChanged = true;
      }
      else if (GetCurrentMenu() == menuCurrent)
      {
        // restore the selection
        menuCurrent->_selected = oldSelected;
      }
      return;
    }
  } // for( menu items )

  bool actionKey = false;
  bool selectKey = false;
  bool backKey = false;

  // Different controlling of quick commanding menu
  if (menuCurrent && (_menuScroll || menuCurrent->IsContexSensitive()))
  {
    selectKey =  GInput.GetActionToDo(UAActionContext,true,true,false,2,true);
    if(!selectKey) selectKey = GInput.GetActionToDo(UAAction);
    if(_groupInfoSelected <=0)
      backKey = GInput.GetActionToDo(UAMenuBack,true,true,true,2,/*BLOCK*/true);
  }
  else
  {
    actionKey = GInput.GetActionToDo(UAAction);
    selectKey = GInput.GetActionToDo(UAMenuSelect);
    if(_groupInfoSelected <=0) 
      backKey = GInput.GetKeyToDo(DIK_BACK, true, true, 2);
  }

  if (selectKey || actionKey)
  {
    if (menuCurrent->_selected >= 0 && menuCurrent->_selected < menuCurrent->_items.Size())
    {
      MenuItem* item = menuCurrent->_items[menuCurrent->_selected];
      bool resetMenu = false;
      if (item->_enable && item->_visible && ProcessMenuItem(item, resetMenu, actionKey))
      {
        // menu processed - can hide
        if (_xboxStyle && cam == CamGroup)
        {
          _menu.Resize(0);
#if _ENABLE_CONVERSATION
          _conversationContext = NULL;
#endif
          // leave _units menu active
        }
        else
        {
          if (!forcedMenu && GWorld->GetCameraType() != CamGroup)
          {
            ClearSelectedUnits();
            HideMenu();
          }
/*
          if (_forcingCommandingMode) _forcingCommandingMode=false; //show UNITS menu
          else HideMenu();
*/
        }
      }
      else if (resetMenu)
      {
        _menu.Resize(1);
        CheckMenuItems(_menu[0]);
        _menu[0]->ValidateIndex();
        _menuChanged = true;
      }
      return;
    }
  }

  OLinkPermNOArray(AIUnit) oldList;
  ListSelectingUnits(oldList);

  if(!GWorld->HasMap() || (!GInput.keyPressed[DIK_LCONTROL] && !GInput.keyPressed[DIK_RCONTROL]))
  {//do not navigate in menu, when control is pressed (because of zoom in map)
    if (GInput.GetActionToDo(UAPrevAction)) //mouse wheel should be mapped to UAPrevAction
    {
      if(!GetCurrentMenu()->IsContexSensitive()) _menuScroll = true;
      menuCurrent->PrevIndex();
    }
    if (GInput.GetActionToDo(UANextAction))
    {
      if(!GetCurrentMenu()->IsContexSensitive()) _menuScroll = true;
      menuCurrent->NextIndex();
    }
  }
  //in map all menus are contex sensitive
  if(GWorld->HasMap() || GWorld->GetCameraType() == CamGroup) _menuScroll = true;

  OLinkPermNOArray(AIUnit) newList;
  ListSelectingUnits(newList);
  // check if list of selecting units changed
  if (newList.Size() != oldList.Size()) ShowSelectedUnits();
  for (int i=0; i<newList.Size(); i++)
  {
    AIUnit *unit = newList[i];
    if (oldList.FindKey(unit) < 0)
    {
      ShowSelectedUnits();
      break;
    }
  }

  bool bKey = false;
#ifdef _XBOX
  if (GInput.GetXInputButtonToDo(XBOX_B))
  {
    bKey = true;
    if (menuCurrent->_selected >= 0 && menuCurrent->_selected < menuCurrent->_items.Size())
    {
      MenuItem* item = menuCurrent->_items[menuCurrent->_selected];
      if (item && item->_cmd >= CMD_UNIT_1 && item->_cmd <= CMD_UNIT_12)
      {
        int index = item->_cmd - CMD_UNIT_1;
        AIUnit *unit = group->GetUnit(index);
        if (unit && IsSelectedUnit(unit))
        {
          UnselectUnit(unit);
          bKey = false;
        }
      }
    }
  }
#endif
  if (backKey || bKey || _xboxStyle && GInput.GetActionToDo(UASelectAll))
  {
    _menuChanged = true;
    int menuSize = _menu.Size();
    if (menuSize > 1)
    {
      _menu.Delete(--menuSize);
      CheckMenuItems(_menu[menuSize - 1]); // _menu.Size() can change inside CheckMenuItems
      _menu[_menu.Size() - 1]->ValidateIndex();
    }
    else if (menuSize == 1 && _units)
    {
      _menu.Delete(--menuSize);
#if _ENABLE_CONVERSATION
      InterruptConversation(_conversationContext);
      _conversationContext = NULL;
#endif
      ClearSelectedUnits();
      if (_unitsSingle)
      {
        CheckMenuItems(_unitsSingle); // _menu.Size() can change inside CheckMenuItems
        _unitsSingle->ValidateIndex();
      }
      else
      {
        CheckMenuItems(_units); // _menu.Size() can change inside CheckMenuItems
        _units->ValidateIndex();
      }
    }
    else if (_unitsSingle && _units)
    {
      _unitsSingle = NULL;
      CheckMenuItems(_units); // _menu.Size() can change inside CheckMenuItems
      _units->ValidateIndex();
    }
    else
    {
      ClearSelectedUnits();
/*
      if (_forcingCommandingMode) _forcingCommandingMode=false;
      else HideMenu();
*/
#if _ENABLE_CONVERSATION
      InterruptConversation(_conversationContext);
#endif
      GWorld->DisableTacticalView();
      HideMenu();
      if(_modeAuto == UIStrategyComplex) _modeAuto = UIFire;
    }
    return;
  }

#if _ENABLE_SPEECH_RECOGNITION
  _communicator.PushToTalk();
#if 0 // diagnostics
  FCState state = _communicator.GetState();
  switch (state)
  {
  case FCSUnavailable:
    DIAG_MESSAGE(500, "Speech recognition: Uninitialized");
    break;
  case FCSNotConnected:
    DIAG_MESSAGE(500, "Speech recognition: Not connected");
    break;
  case FCSIdle:
    DIAG_MESSAGE(500, "Speech recognition: Idle");
    break;
  case FCSListening:
    DIAG_MESSAGE(500, "Speech recognition: Listening");
    break;
  case FCSFinishing:
    DIAG_MESSAGE(500, "Speech recognition: Finishing");
    break;
  case FCSDisplaying:
    DIAG_MESSAGE(500, "Speech recognition: Displaying");
    break;
  }
#endif

  AutoArray<char> data;
  if (_capture.GetData(data))
  {
    int id = -1;
    _communicator.Process(data.Data(), data.Size(), id);
    if (id >= 0)
    {
      int selected = -1;
      if (id == 999)
      {
        // Back
        _menuChanged = true;
        int menuSize = _menu.Size();
        if (menuSize > 1)
        {
          _menu.Delete(--menuSize);
          CheckMenuItems(_menu[menuSize - 1]); // _menu.Size() can change inside CheckMenuItems
          _menu[_menu.Size() - 1]->ValidateIndex();
        }
        else if (menuSize == 1 && _units)
        {
          _menu.Delete(--menuSize);
#if _ENABLE_CONVERSATION
          _conversationContext = NULL;
#endif
          ClearSelectedUnits();
          if (_unitsSingle)
          {
            CheckMenuItems(_unitsSingle); // _menu.Size() can change inside CheckMenuItems
            _unitsSingle->ValidateIndex();
          }
          else
          {
            CheckMenuItems(_units); // _menu.Size() can change inside CheckMenuItems
            _units->ValidateIndex();
          }
        }
        else if (_unitsSingle && _units)
        {
          _unitsSingle = NULL;
          CheckMenuItems(_units); // _menu.Size() can change inside CheckMenuItems
          _units->ValidateIndex();
        }
        else if (!IsEmptySelectedUnits())
        {
          ClearSelectedUnits();
        }
        else
        {
          HideMenu();
        }
        DIAG_MESSAGE(500, Format("Recognized word: Back"));
        return;
      }
      else
      {
        // menu item
        for (int i=0; i<menuCurrent->_items.Size(); i++)
        {
          MenuItem* item = menuCurrent->_items[i];
          if (item->_speechId == id)
          {
            selected = i;
            break;
          }
        }
      }
      if (selected >= 0)
      {
        MenuItem* item = menuCurrent->_items[selected];
        DIAG_MESSAGE(500, Format("Recognized word: %s", (const char *)GetTextPlain(item->_text)));
        if (item->_visible && item->_enable)
        {
          menuCurrent->_selected = selected;
          bool resetMenu;
          if (ProcessMenuItem(item, resetMenu, false))
          {
            // menu processed - can hide
            if (_xboxStyle && cam == CamGroup)
            {
              _menu.Resize(0);
#if _ENABLE_CONVERSATION
              _conversationContext = NULL;
#endif
              // leave _units menu active
            }
            else
            {
              ClearSelectedUnits();
/*
              if (_forcingCommandingMode) _forcingCommandingMode=false; //show UNITS menu
              else HideMenu();
*/
              HideMenu();
            }
          }
          else if (resetMenu)
          {
            _menu.Resize(1);
            CheckMenuItems(_menu[0]);
            _menu[0]->ValidateIndex();
            _menuChanged = true;
          }
          return;
        }
      }
      else
      {
        DIAG_MESSAGE(500, Format("Recognized word %d not found", id));
      }
    }
  }
#endif
}

void InGameUI::LaunchMenu()
{
  Assert(_xboxStyle);
  if (_xboxStyle)
  {
    // select units
    if (!_units)
    {
      _units = new Menu();
      CreateDynamicMenu(_units, "UNITS");
      ShowMenu();
    }
  }
}

bool InGameUI::IsMenuVisible()
{
  if(GetCurrentMenu() && GetCurrentMenu()->_visible) return true;
  return false;
}

RString InGameUI::GetMenu() const
{
  const Menu *menuCurrent = GetCurrentMenu();
  return menuCurrent ? menuCurrent->GetName() : RString();
}

void InGameUI::SetMenu(RString name)
{
  if (name.GetLength() == 0)
  {
    HideMenu();
    return;
  }

  InitMenu();

  Ref<Menu> menu = new Menu();
  if (name.GetLength() > 0 && name[0] == '#')
    CreateDynamicMenu(menu, cc_cast(name) + 1);
  else
    menu->Load(name, MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
  _menu.Resize(1);
  _menu[0] = menu;

  CheckMenuItems(menu); // _menu.Size() can change inside CheckMenuItems
  if (_menu.Size() > 0)
  {
    _menu[0]->ValidateIndex();
    ShowMenu();
  }
}

extern int MaxCustomSoundSize;

int CmpStringI(const RString *str1, const RString *str2)
{
  return stricmp(*str1, *str2);
}

void InGameUI::UpdateCustomRadio() const
{
  AUTO_STATIC_ARRAY(RString, sounds, 32);
#ifdef _WIN32
  _finddata_t info;
  RString GetUserDirectory();
  long h = _findfirst(GetUserDirectory() + RString("Sound\\*.*"), &info); // not used on Xbox
  if (h != -1)
  {
    do
    {
      if ((info.attrib & _A_SUBDIR) == 0)
      {
        const char *SupportedFiles[] = { {".wav"}, {".ogg"}, {".wss"} };
        const char *ext = strrchr(info.name, '.');        
        
        if (!ext) continue;

        bool isValidSoundFile = false;
        for (int i = 0; i < 3; i++)
        {
          if (strcmpi(ext, SupportedFiles[i]) == 0) 
          {
            isValidSoundFile = true;
            break;
          }
        }

        // ignore sounds larger than 40 KB
        if (isValidSoundFile && info.size <= (size_t)MaxCustomSoundSize)
          sounds.Add(info.name);
      }
    }
    while (_findnext(h, &info) == 0);
    _findclose(h);
  }
#endif
  int n = sounds.Size();
  if (n > 0)
  {
    QSort(sounds.Data(), n, CmpStringI);
    saturateMin(n, 10);
    _customRadio.Realloc(n);
    _customRadio.Resize(n);
    for (int i=0; i<n; i++)
      _customRadio[i] = sounds[i];
  }
}

/// Functor checking for free gunner positions
class CheckFreeTurrets : public ITurretFunc
{
protected:
  bool &_freeCommander;
  bool &_freeGunner;

public:
  CheckFreeTurrets(bool &freeCommander, bool &freeGunner):
    _freeCommander(freeCommander), _freeGunner(freeGunner) {}

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    Transport *veh = static_cast<Transport *>(entity); // safe here, we know how we called it (+ turret exist)
    if (veh->QCanIGetInTurret(context._turret))
    {
      if (context._turretType->_primaryObserver)
        _freeCommander = true;
      else if (context._turretType->_primaryGunner)
        _freeGunner = true;
      else if (context._weapons->_magazineSlots.Size() == 0)
        _freeCommander = true;
      else
        _freeGunner = true;
    }
    return _freeCommander && _freeGunner; // end when both observer and gunner position found
  }
};

/// Helper function to create a menu item from the description given by the scripting value
static MenuItem *GameValueToMenuItem(GameValuePar value)
{
  if (value.GetType() != GameArray)
  {
    RptF("Item description is not an array: %s.", cc_cast(value.GetText()));
    return NULL;
  }
  // description of the menu item
  // check the size and structure
  const GameArrayType &itemDesc = value;
  if (itemDesc.Size() < 7)
  {
    RptF("Item description - size 7 expected: %s.", cc_cast(value.GetText()));
    return NULL;
  }
  // text
  Ref<INode> text;
  if (itemDesc[0].GetType() == GameString)
    text = CreateTextASCII(NULL, itemDesc[0]);
  else if (!itemDesc[0].GetNil() && itemDesc[0].GetType() == GameText)
    text = static_cast<GameDataText *>(itemDesc[0].GetData())->GetValue();
  else
  {
    RptF("Item description - menu text expected as an item #1: %s.", cc_cast(value.GetText()));
    return NULL;
  }
  // shortcuts
  AutoArray< int, MemAllocDataStack<int, 32> > shortcuts;
  if (itemDesc[1].GetType() != GameArray)
  {
    RptF("Item description - list of shortcuts expected as an item #2: %s.", cc_cast(value.GetText()));
    return NULL;
  }
  const GameArrayType &shortcutDesc = itemDesc[1];
  for (int i=0; i<shortcutDesc.Size(); i++)
  {
    if (shortcutDesc[i].GetType() != GameScalar)
    {
      RptF("Item description - list of shortcuts expected as an item #2: %s.", cc_cast(value.GetText()));
      return NULL;
    }
    shortcuts.Add(toInt(shortcutDesc[i].operator GameScalarType()));
  }
  // submenu
  RString submenu;
  if (itemDesc[2].GetType() != GameString)
  {
    RptF("Item description - submenu name expected as an item #3: %s.", cc_cast(value.GetText()));
    return NULL;
  }
  submenu = itemDesc[2];
  // command id
  int cmd = -1;
  if (itemDesc[3].GetType() != GameScalar)
  {
    RptF("Item description - command id expected as an item #4: %s.", cc_cast(value.GetText()));
    return NULL;
  }
  cmd = toInt(itemDesc[3].operator GameScalarType());
  // attributes
  AttributeList attributes;
  if (itemDesc[4].GetType() != GameArray)
  {
    RptF("Item description - list of attributes expected as an item #5: %s.", cc_cast(value.GetText()));
    return NULL;
  }
  const GameArrayType &attrsDesc = itemDesc[4];
  for (int i=0; i<attrsDesc.Size(); i++)
  {
    if (attrsDesc[i].GetType() != GameArray)
    {
      RptF("Item description - list of attributes expected as an item #5: %s.", cc_cast(value.GetText()));
      return NULL;
    }
    const GameArrayType &attrDesc = attrsDesc[i];
    if (attrDesc.Size() != 2 || attrDesc[0].GetType() != GameString || attrDesc[1].GetType() != GameString)
    {
      RptF("Item description - list of attributes expected as an item #5: %s.", cc_cast(value.GetText()));
      return NULL;
    }
    attributes.SetAttribute(attrDesc[0], attrDesc[1], MenuAttributeTypes);
  }
  // show condition
  RString condShow;
  if (itemDesc[5].GetType() != GameString)
  {
    RptF("Item description - show condition expected as an item #6: %s.", cc_cast(value.GetText()));
    return NULL;
  }
  condShow = itemDesc[5];
  // enable condition
  RString condEnable;
  if (itemDesc[6].GetType() != GameString)
  {
    RptF("Item description - enable condition expected as an item #7: %s.", cc_cast(value.GetText()));
    return NULL;
  }
  condEnable = itemDesc[6];

  RString textureName;
  if (itemDesc.Size()>=8 && itemDesc[7].GetType() == GameString)
  {
    textureName = itemDesc[7];
  }

  MenuItem *item = new MenuItem(RString(), -1, cmd, submenu,true, textureName);
  item->_text = item->_baseText = text;
  item->AddShortcuts(shortcuts.Data(), shortcuts.Size());
  item->_attributes = attributes;
  item->_condVisible.Compile(condShow, MenuConditionVariables, lenof(MenuConditionVariables));
  item->_condEnable.Compile(condEnable, MenuConditionVariables, lenof(MenuConditionVariables));

  return item;
}

void InGameUI::CreateDynamicMenu(Menu *menu, const char *name)
{
  menu->SetName(RString("#") + RString(name));

  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return;
  Transport *transport = agent->GetVehicleIn();

  if (stricmp(name, "CUSTOM_RADIO") == 0)
  {
    UpdateCustomRadio();
    for (int i=0; i<_customRadio.Size(); i++)
    { 
      char buffer[256]; strncpy(buffer, _customRadio[i], 256); buffer[255] = 0;
      char *ext = strrchr(buffer, '.');
      if (ext) *ext = 0;
      int key = DIK_1 + i;
      int cmd = CMD_RADIO_CUSTOM_1 + i;
      menu->_items.Add(new MenuItem(buffer, key, cmd, RString(), false));
    }
    menu->_items.Add(new MenuItem(RString(), DIK_BACK, CMD_BACK));
    return;
  }

  // user defined menu
  const char *ptr = "USER:";
  int n = strlen(ptr);
  if (strnicmp(name, ptr, n) == 0)
  {
    name += n;
    // description stored in the global variable with given name
    GameValue value = GGameState.VarGet(name, false, GWorld->GetMissionNamespace()); // mission namespace
    if (value.GetNil())
    {
      RptF("User menu description '%s' not defined.", name);
      return;
    }
    if (value.GetType() != GameArray)
    {
      RptF("User menu description '%s' is not an array: %s.", name, cc_cast(value.GetText()));
      return;
    }
    const GameArrayType &menuDesc = value;
    // menu title
    if (menuDesc.Size() < 1 || menuDesc[0].GetType() != GameArray)
    {
      RptF("User menu description '%s' - array with title and contex sensitive parameter expected as the first argument: %s.", name, cc_cast(value.GetText()));
      return;
    }
    const GameArrayType &menuParams = menuDesc[0];
    if (menuParams.Size() < 1 || menuParams[0].GetType() != GameString)
    {
      RptF("User menu description '%s' -  title expected as a first argument of first array: %s.", name, cc_cast(value.GetText()));
      return;
    }
    menu->_text = CreateTextStructured(NULL, menuParams[0]);
    if (menuParams.Size() < 2 || menuParams[1].GetType() != GameBool) 
    {
      RptF("User menu description '%s' -  contex sensitive parameter as a second argument of first array: %s.", name, cc_cast(value.GetText()));
      return;
    }
    menu->SetContexSensitive((bool)menuParams[1]);

    // menu items
    for (int i=1; i<menuDesc.Size(); i++)
    {
      MenuItem *item = GameValueToMenuItem(menuDesc[i]);
      if (item) menu->AddItem(item);
      else
      {
        // report error context
        RptF(" - in user menu description '%s', item #%d: %s.", name, i + 1, cc_cast(value.GetText()));
      }
    }
    return;
  }

  ptr = "ACTION";
  n = strlen(ptr);
  if (strnicmp(name, ptr, n) == 0)
  {
    name += n;
    int offset = 0;
    if (*name == 0)
    {
      BackupTargets(_visibleListTemp);
      CollectActions(_visibleListTemp, _actionsTemp);
    }
    else
    {
      offset = atoi(name);
    }

    menu->_text = CreateTextStructured(NULL, LocalizeString(IDS_ACTION));

    // actions
    for (int i=0; i<8;)
    {
      if (offset >= _actionsTemp.Size()) break;
      
      // add action
      UIAction &action = _actionsTemp[offset++];
      RefR<INode> text = action.action->GetText(NULL);
      if (text == NULL) continue;
      MenuItem *item = new MenuItem
      (
        RString(), DIK_1 + i, CMD_ACTION, RString(), false
      );
      item->_text = item->_baseText = text;
      SetMenuAttrAction(item, attrAction, action.action);
      menu->AddItem(item);
      i++;
    }

    // more
    if (offset < _actionsTemp.Size())
    {
      menu->AddItem
      (
        new MenuItem
        (
          LocalizeString(IDS_MORE_MENU), DIK_0, CMD_NOTHING, Format("#ACTION%d", offset)
        )
      );
    }

    // back
    menu->AddItem
    (
      new MenuItem (RString(), DIK_BACK, CMD_BACK)
    );
    return;
  }

  ptr = "GET_IN";
  n = strlen(ptr);
  if (strnicmp(name, ptr, n) == 0)
  {
    name += n;
    int start = 0;
    int offset = 0;
    if (*name == 'T')
    {
      // submenu for concrete target
      menu->_text = CreateTextStructured(NULL, LocalizeString(IDS_GETIN_POS));

      // select target
      name++;
      offset = atoi(name);
      Target *target = (offset>=0 && offset<_visibleListTemp.Size()) ? _visibleListTemp[offset] : NULL;
      Object *obj = target ? target->idExact : NULL;
      Transport *veh = dyn_cast<Transport>(obj);
      if (veh)
      {
        bool asDriver = veh->QCanIGetIn();
        bool asCargo = veh->QCanIGetInCargo() >= 0;
        bool asCommander = false, asGunner = false;
        CheckFreeTurrets func(asCommander, asGunner);
        veh->ForEachTurret(func);

        MenuItem *item = new MenuItem
        (
          LocalizeString(IDS_GETIN_POS_ANY), DIK_1, CMD_GETIN
        );
        SetMenuAttrEntityAI(item, attrVehicle, veh);
        SetMenuAttrInt(item, attrGetInPos, GIPAny);
        menu->AddItem(item);
        if (asDriver)
        {
          if (target->IsKindOf(GWorld->Preloaded(VTypeAir)))
            item = new MenuItem
            (
              LocalizeString(IDS_GETIN_POS_PILOT), DIK_2, CMD_GETIN
            );
          else
            item = new MenuItem
            (
              LocalizeString(IDS_GETIN_POS_DRIVER), DIK_2, CMD_GETIN
            );
          SetMenuAttrEntityAI(item, attrVehicle, veh);
          SetMenuAttrInt(item, attrGetInPos, GIPDriver);
          menu->AddItem(item);
        }
        if (asCommander)
        {
          item = new MenuItem
          (
            LocalizeString(IDS_GETIN_POS_COMM), DIK_3, CMD_GETIN
          );
          SetMenuAttrEntityAI(item, attrVehicle, veh);
          SetMenuAttrInt(item, attrGetInPos, GIPCommander);
          menu->AddItem(item);
        }
        if (asGunner)
        {
          item = new MenuItem
          (
            LocalizeString(IDS_GETIN_POS_GUNN), DIK_4, CMD_GETIN
          );
          SetMenuAttrEntityAI(item, attrVehicle, veh);
          SetMenuAttrInt(item, attrGetInPos, GIPGunner);
          menu->AddItem(item);
        }
        if (asCargo)
        {
          item = new MenuItem
          (
            LocalizeString(IDS_GETIN_POS_CARGO), DIK_5, CMD_GETIN
          );
          SetMenuAttrEntityAI(item, attrVehicle, veh);
          SetMenuAttrInt(item, attrGetInPos, GIPCargo);
          menu->AddItem(item);
        }
        menu->AddItem
        (
          new MenuItem
          (
            RString(), DIK_BACK, CMD_BACK
          )
        );
      }
      return;
    }
    else if (*name == 0)
    {
      BackupTargets(_visibleListTemp);
      // insert my vehicle as first in list
      if (transport)
      {
        for (int i=0; i<_visibleListTemp.Size(); i++)
        {
          Target *target = _visibleListTemp[i];
          Assert(target);
          if (target->idExact == transport)
          {
            _visibleListTemp.Delete(i);
            _visibleListTemp.Insert(0, target);
            break;
          }
        }
      }

      // get out command
      menu->AddItem
      (
        new MenuItem
        (
          LocalizeString(IDS_GETOUT_IM),
          DIK_1, CMD_GETOUT
        )
      );
      start = 1;
    }
    else
    {
      offset = atoi(name);
    }

    menu->_text = CreateTextStructured(NULL, LocalizeString(IDS_GETIN));
    
    for (int i=start; i<8;)
    {
      if (offset >= _visibleListTemp.Size()) break;

      // check target
      Target *target = _visibleListTemp[offset++];
      if (!target) continue;
      if (!target->IsKnown()) continue;
      if (target->IsVanishedOrDestroyed()) continue;
      Object *obj = target->idExact;
      Transport *veh = dyn_cast<Transport>(obj);
      if (!veh) continue;
      if (!agent->IsFriendly(target->GetSide())) continue;
      if (!veh->GetType()->ShowGetIn()) continue;
      bool asDriver = veh->QCanIGetIn();
      bool asCargo = veh->QCanIGetInCargo() >= 0;
      bool asCommander = false, asGunner = false;
      CheckFreeTurrets func(asCommander, asGunner);
      veh->ForEachTurret(func);
      if (!asDriver && !asCommander && !asGunner && !asCargo) continue;

      // get in position submenu
      BString<256> text;
      RString name = target->GetType()->GetShortName();
      AIBrain *a = veh ? veh->CommanderUnit() : NULL;
      AIGroup *g = a ? a->GetGroup() : NULL;
      if (g && g == agent->GetGroup())
      {
        RString id = a->IsAnyPlayer() ? cc_cast(a->GetPerson()->GetPersonName()) : g->GetName();
        sprintf
        (
          text, LocalizeString(IDS_TARGET_MENU_GROUP),
          (const char *)id,
          (const char *)name, a->GetUnit() ? a->GetUnit()->ID() : 0
        );
      }
      else
      {
        Vector3 pos = GWorld->CameraOn()->FutureVisualState().GetInvTransform() * target->GetPosition();
        int azimut = toInt(atan2(pos.X(), pos.Z()) * (6 / H_PI));
        if (azimut <= 0) azimut += 12;
        sprintf(
          text, LocalizeString(IDS_TARGET_MENU),
          (const char *)name, azimut
        );
      }
      menu->AddItem
      (
        new MenuItem(
          text.cstr(), DIK_1 + i, CMD_NOTHING, Format("#GET_INT%d", offset - 1), // offset is already incremented
          false
        )
      );
      i++;
    }

    // more
    if (offset < _visibleListTemp.Size())
    {
      menu->AddItem
      (
        new MenuItem
        (
          LocalizeString(IDS_MORE_MENU), DIK_0, CMD_NOTHING, Format("#GET_IN%d", offset)
        )
      );
    }

    // back
    menu->AddItem
    (
      new MenuItem
      (
        RString(), DIK_BACK, CMD_BACK
      )
    );
    return;
  }

  ptr = "WATCH";
  n = strlen(ptr);
  if (strnicmp(name, ptr, n) == 0)
  {
    name += n;
    int start = 0;
    int offset = 0;
    if (*name == 0)
    {
      BackupTargets(_visibleListTemp);
      menu->AddItem
      (
        new MenuItem
        (
          LocalizeString(IDS_WATCH_AUTO), DIK_1, CMD_WATCH_AUTO
        )
      );
      start = 1;
    }
    else
    {
      offset = atoi(name);
    }

    bool empty = IsEmptySelectedUnits();
    bool vehicleCommander = transport && transport->EffectiveObserverUnit() == agent;

    menu->_text = CreateTextStructured(NULL, LocalizeString(IDS_WATCH_TARGET));

    bool friendly = false;
    for (int i=start; i<8;)
    {
      if (offset >= _visibleListTemp.Size()) break;

      // check target
      Target *target = _visibleListTemp[offset++];
      if (!target) continue;
      if (!target->IsKnown()) continue;
      if (target->IsVanishedOrDestroyed()) continue;
      EntityAI *tgtAI = target->idExact;
      if (!tgtAI) continue;
      // do not aim on yourself
      if (empty)
      {
        if (vehicleCommander && tgtAI == transport) continue;
      }
      else
      {
        bool found = false;
        for (int i=0; i<_selectedUnits.Size(); i++)
        {
          AIUnit *unit = _selectedUnits[i]._unit;
          if (!unit) continue;
          if (unit == agent) continue;
          if (tgtAI == unit->GetVehicleIn() || tgtAI == unit->GetPerson()) continue;
          found = true;
        }
        if (!found) continue;
      }
      if (target->IsKindOf(GWorld->Preloaded(VTypeNonStrategic))) continue;
      if (!tgtAI->GetType()->ShowGetIn()) continue;
      // split menu between enemy (or unknown) and friendly targets
      bool split = !friendly && target->GetSide() != TSideUnknown && !agent->IsEnemy(target->GetSide());
      if (split)
      {
        friendly = true;
        if (i > 0)
        {
          offset--;
          break;
        }
      }

      RString name = target->GetType()->GetShortName();

      char text[256];
      EntityAI *veh = target->idExact;
      AIBrain *a = veh ? veh->CommanderUnit() : NULL;
      AIGroup *g = a ? a->GetGroup() : NULL;
      if (g && g == agent->GetGroup())
      {
        sprintf
        (
          text, LocalizeString(IDS_TARGET_MENU_GROUP),
          (const char *)a->GetPerson()->GetPersonName(),
          (const char *)name, a->GetUnit() ? a->GetUnit()->ID() : 0
        );
      }
      else
      {
        Vector3 pos = GWorld->CameraOn()->FutureVisualState().GetInvTransform() * target->GetPosition();
        int azimut = toInt(atan2(pos.X(), pos.Z()) * (6 / H_PI));
        if (azimut <= 0) azimut += 12;

        bool showSensor = false;
        AIBrain *sensorBrain = target->SeenBy();
        AIUnit *sensor = sensorBrain ? sensorBrain->GetUnit() : NULL;
        if (sensor)
        {
          AISubgroup *subSensor = sensor->GetSubgroup();
          if (subSensor)
          {
            AIGroup *grpSensor = subSensor->GetGroup();
            if (grpSensor && grpSensor == agent->GetGroup())
            {
              showSensor = subSensor != agent->GetGroup()->MainSubgroup();
            }
          }
        }
        if (showSensor)
        {
          sprintf
          (
            text, LocalizeString(IDS_TARGET_MENU_SENSOR),
            (const char *)name, sensor->ID(), azimut
          );
        }
        else
        {
          sprintf
          (
            text, LocalizeString(IDS_TARGET_MENU),
            (const char *)name, azimut
          );
        }
      }

      MenuItem *item = new MenuItem
      (
        text, DIK_1 + i, CMD_WATCH, RString(), false
      );
      SetMenuAttrTarget(item, attrTarget, target);
      menu->AddItem(item);

      i++;
    }   
    
    // more
    if (offset < _visibleListTemp.Size())
    {
      menu->AddItem
      (
        new MenuItem
        (
          LocalizeString(IDS_MORE_MENU), DIK_0, CMD_NOTHING, Format("#WATCH%d", offset)
        )
      );
    }

    // back
    menu->AddItem
    (
      new MenuItem
      (
        RString(), DIK_BACK, CMD_BACK
      )
    );
    return;
  }

  if (stricmp(name, "UNITS") == 0)
  {
    menu->_text = CreateTextStructured(NULL, LocalizeString(IDS_MENU_UNITS));
    // Create menu dynamically
    return;
  }

  if (stricmp(name, "SINGLE_UNITS") == 0)
  {
    menu->_text = CreateTextStructured(NULL, LocalizeString(IDS_MENU_UNITS));
    // Create menu dynamically
    return;
  }

#if _ENABLE_CONVERSATION
  if (stricmp(name, "CONVERSATION") == 0)
  {
    if (!_conversationContext) return;

    menu->_text = NULL; // CreateTextStructured(NULL, "Conversation");

    if (_conversationContext->_handlerResult.GetNil())
    {
      // old reaction (based on class UI)
      AIBrain *unit = _conversationContext->_askingUnit;
      Person *person = unit ? unit->GetPerson() : NULL;
      GameVarSpace *vars = person ? person->GetVars() : NULL;
      if (vars)
      {
        GGameState.BeginContext(vars);

        vars->VarLocal("_me");
        Person *me = unit ? unit->GetPerson() : NULL;
        vars->VarSet("_me", GameValueExt(me));
        vars->VarLocal("_you");
        Person *you = _conversationContext->_askedUnit ? _conversationContext->_askedUnit->GetPerson() : NULL;
        vars->VarSet("_you", GameValueExt(you));
        vars->VarLocal("_random");
        vars->VarSet("_random", GRandGen.RandomValue());

        const KBUINodeInfo *current = _conversationContext->_current;

        // TODO: handle parents properly
        const KBCenter *kbParent = _conversationContext->_askingUnit->GetKBParent();
        int n = _conversationContext->_center->NChilds(current, kbParent);
        int index = 0;
        for (int i=0; i<n; i++)
        {
          Ref<const KBUINodeInfo> node = _conversationContext->_center->GetChild(current, i, kbParent);
          if (!node) continue;

          // check condition
          RString condition = _conversationContext->_center->GetCondition(node);
          if (condition.GetLength() > 0 && !GGameState.EvaluateBool(condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace())) continue; // mission namespace

          // automatic shortcut
          int key = -1;
          KBUIType type = node->GetUINode() ? node->GetUINode()->GetType() : KBUITNone;
          switch (type)
          {
          case KBUITPrev:
            key = DIK_MINUS;
            break;
          case KBUITNext:
            key = DIK_EQUALS;
            break;
          case KBUITParent:
            key = DIK_0;
            break;
          default:
            if (index < 9)
            {
              key = DIK_1 + index;
            }
            index++;
            break;
          }

          // create menu item
          MenuItem *item = new MenuItem(RString(), -1, CMD_CONVERSATION);
          // manual shortcuts
          const AutoArray<int> *shortcuts = NULL;
          if (node->GetUINode()) shortcuts = node->GetUINode()->GetShortcuts();
          if (shortcuts && shortcuts->Size() > 0) item->AddShortcuts(shortcuts->Data(), shortcuts->Size());
          // automatic shortcut
          if (key >= 0) item->AddShortcuts(&key, 1);
          // other attributes
          item->_text = item->_baseText = CreateTextStructured(NULL, _conversationContext->_center->GetText(node));
          SetMenuAttrMessage(item, attrMessage, node);
          menu->AddItem(item);
        }

        GGameState.EndContext();
      }
    }
    else
    {
      // new reaction (based on event handler)
      const GameArrayType &array = _conversationContext->_handlerResult;
      int index = 0;
      for (int i=0; i<array.Size(); i++)
      {
        // single menu item
        const GameArrayType &subarray = array[i];
        if (subarray.Size() == 0) continue;
        // the first argument is the UI text
        RString text;
        AutoArray< int, MemAllocDataStack<int, 16> > shortcuts;

        if (subarray[0].GetType() == GameString)
        {
          // text
          text = subarray[0];
        }
        else if (subarray[0].GetType() == GameArray)
        {
          // text
          const GameArrayType &textDesc = subarray[0];
          if (textDesc.Size() == 0 || textDesc[0].GetType() != GameString) continue;
          text = textDesc[0];
          // shortcuts
          if (textDesc.Size() >= 2 && textDesc[1].GetType() == GameArray)
          {
            const GameArrayType &shortcutsDesc = textDesc[1];
            for (int j=0; j<shortcutsDesc.Size(); j++)
            {
              if (shortcutsDesc[j].GetType() == GameScalar)
              {
                shortcuts.Add(toInt(shortcutsDesc[j].operator GameScalarType()));
              }
            }
          }
        }
        else continue;

        MenuItem *item = new MenuItem(RString(), -1, CMD_CONVERSATION);
        // other attributes
        item->_text = item->_baseText = CreateTextStructured(NULL, text);
        SetMenuAttrGameValue(item, attrGameValue, array[i]);
        menu->AddItem(item);
        // manual shortcuts
        if (shortcuts.Size() > 0) item->AddShortcuts(shortcuts.Data(), shortcuts.Size());
        // automatic shortcuts
        int key = -1;
        if (index < 9) key = DIK_1 + index;
        if (key >= 0) item->AddShortcuts(&key, 1);
        index++;
      }
    }
    return;
  }
#endif
}

void InGameUI::SelectTeam(Team team)
{
  AIBrain *unit = GWorld->FocusOn();
  Assert(unit);
  AIGroup *group = unit->GetGroup();
  if (!group) return;

  ClearSelectedUnits();
  for (int i=0; i<group->NUnits(); i++)
  {
    AIUnit *u = group->GetUnit(i);
    if (!u || u == unit) continue;
    if (GetTeam(u) == team)
      SelectUnit(u);
  }

  if(_menu.Size()>0 && strcmp(_menu[0]->GetName(),"RscGroupRootMenu")!=0 && strcmp(_menu[0]->GetName(),"RscMainMenu")!=0)
  {
    Menu *menu = new Menu();
    menu->Load("RscMainMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
    _menu.Resize(1);
    _menu[0] = menu;
}
}

void InGameUI::ShowWeaponInfo()
{
  for (int i=0; i<_lastUnitInfos.Size(); i++)
  { //if Menu with weapon is hidden, first show menu
    UnitInfoDesc &desc = _lastUnitInfos[i];
    if (!desc._display) continue;
    DisplayUnitInfo *disp = desc._display;

    if(disp->ammo)
    {
      desc._changed = Glob.uiTime;
    }
  }
}

#if _ENABLE_CONVERSATION

/// more advanced channel selection (based on the units in the conversation)
static RadioChannel *SelectRadioChannel(AIBrain *askingUnit, AIBrain *askedUnit, bool forceRadio)
{
  const float MaxDirectDistance = 20.0f;

  Person *askingPerson = askingUnit->GetPerson();
  Person *askedPerson = askedUnit->GetPerson();
  Transport *askingVehicle = askingUnit->GetVehicleIn();
  Transport *askedVehicle = askedUnit->GetVehicleIn();

  if (askingVehicle && askingVehicle == askedVehicle)
  {
    // 1. if both persons are in the same vehicle and the same compartment and the radio is not forced, use the direct channel
    if (!forceRadio && (askingVehicle->GetPersonCompartments(askingPerson) & askingVehicle->GetPersonCompartments(askedPerson)) != 0)
    {
      return askingPerson->GetRadio();
    }
    // 2. if both persons are in the same vehicle, use the vehicle channel
    return &askingVehicle->GetRadio();
  }

  // 3. if both persons are outside vehicles, radio is not forced and the distance is lower than 20 m, use the direct channel
  if (!forceRadio && !askingVehicle && !askedVehicle &&
    askingPerson->WorldPosition(askingPerson->FutureVisualState()).Distance2(askedPerson->WorldPosition(askedPerson->FutureVisualState())) < Square(MaxDirectDistance))
  {
    return askingPerson->GetRadio();
  }

  AIGroup *askingGroup = askingUnit->GetGroup();
  AIGroup *askedGroup = askedUnit->GetGroup();
  if (askingGroup && askedGroup)
  {
    // 4. if both persons are in the same group, use the group channel
    if (askingGroup == askedGroup) return &askingGroup->GetRadio();

    // 5. if both persons are on the same side, use the side channel
    AICenter *askingCenter = askingGroup->GetCenter();
    AICenter *askedCenter = askedGroup->GetCenter();
    if (askingCenter && askingCenter == askedCenter)
    {
      // design request - do not use Command radio for conversation
/*
      // 6. if both persons are group leaders, use the command channel
      if (askingGroup->Leader() == askingUnit && askedGroup->Leader() == askedUnit)
        return &askingCenter->GetCommandRadio();
*/
      return &askingCenter->GetSideRadio();
    }
  }

  // 7. otherwise, use the global channel
  return &GWorld->GetRadio();
}

/// tell something to someone (verbal communication)
void KBTell(AIBrain *askingUnit, AIBrain *askedUnit, const KBMessageInfo *info, bool forceRadio = false)
{
  if (!askingUnit || !askingUnit->LSCanSpeak() || !askingUnit->GetKBCenter()) return;
  if (!askedUnit || !askedUnit->LSIsAlive()) return;

  Ref<RadioMessage> msg = new RadioMessageTalk(info, askingUnit, askedUnit);
  RadioChannel *radio = SelectRadioChannel(askingUnit, askedUnit, forceRadio);
  if (radio) radio->Transmit(msg);
}
#endif

/// helper function which will find the candidate for the next waypoint
static bool FindNextWaypoint(Vector3 &result, AIBrain *agent)
{
  // find criteria when the position is good enough
  EntityAIFull *veh = agent->GetVehicle();
  if (!veh) return false;
  Vector3Val myPos = veh->FutureVisualState().Position();
  float prec2 = Square(veh->GetPrecision());

  // first, check the mission waypoint
  AIGroup *group = agent->GetGroup();
  if (group)
  {
    int &index = group->GetCurrent()->_fsm->Var(0);
    if (index >= 0 && index < group->NWaypoints())
    {
      result = group->GetWaypointPosition(index);
      if (myPos.DistanceXZ2(result) > prec2) return true; // far enough
    }
  }

#if _ENABLE_IDENTITIES
  Person *person = agent->GetPerson();
  if (!person) return false;
  const Identity &identity = agent->GetPerson()->GetIdentity();

  // second, the custom mark position
  if (identity.GetCustomMark(result))
  {
    if (myPos.DistanceXZ2(result) > prec2) return true; // far enough
  }

  // third, the current task position
  const Task *task = identity.GetCurrentTask();
  if (task && !task->IsCompleted() && task->GetDestination(result))
  {
    if (myPos.DistanceXZ2(result) > prec2) return true; // far enough
  }
#endif

  // not found
  return false;
}

void InGameUI::AssignTeam(AIBrain * agent, Team team)
{
  AIGroup *group = agent->GetGroup();
  AICenter *center = group ? group->GetCenter() : NULL;
  if (center)
  {
    OLinkPermNOArray(AIUnit) list;
    ListSelectedUnits(list);
    group->GetRadio().Transmit(new RadioMessageTeam(group, list, team));
  }
}

bool InGameUI::ProcessMenuItem(MenuItem* item, bool &resetMenu, bool actionKey)
{
  if (item->_submenu.GetLength() > 0)
  {
    Menu *menuCurrent = new Menu();
    const char *ptr = item->_submenu;
    if (*ptr == '#')
    {
      menuCurrent->_text = item->_text;
      CreateDynamicMenu(menuCurrent, ptr + 1);
    }
    else
      menuCurrent->Load(item->_submenu, MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
    _menu.Add(menuCurrent);
    CheckMenuItems(menuCurrent);// _menu.Size() can change inside CheckMenuItems
    _menu[_menu.Size() - 1]->ValidateIndex();
    ShowMenu();
    resetMenu = false;
    return false;
  }

  int cmd = item->_cmd;
  int menuSize = _menu.Size();
  for (int i=menuSize-1; i>=0; i--)
  {
    const Menu *menu = _menu[i];
    Assert(menu);
    const MenuItem *item = menu->_items[menu->_selected];
    Assert(item);

    if (item->_cmd != CMD_NOTHING)
    {
      cmd = item->_cmd;
      break;
    }
  }

  bool processedMenu = true;
  resetMenu = true;
  if(GInput.keyPressed[DIK_LCONTROL] || GInput.keyPressed[DIK_RCONTROL])
  {
    processedMenu = false;
    resetMenu = false;
  }

  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return false;

  EntityAI *vehicle = agent->GetVehicle();
  Transport *transport = agent->GetVehicleIn();
  bool vehicleCommander = transport && transport->CommanderUnit() == agent;
  bool vehicleObserver = transport && transport->EffectiveObserverUnit() == agent;

  bool notEmpty = !IsEmptySelectedUnits();

#if _VBS3 // command menu eventhandlers
  if (item->_eventHandler.GetLength() > 0)
  {
    // first argument is cmd
    GameState *gstate = GWorld->GetGameState();
    GameValue value = gstate->CreateGameValue(GameArray);
    GameArrayType &arguments = value;
    arguments.Add((float)cmd);

    // second argument is a list of selected units
    OLinkPermNOArray(AIUnit) list;
    ListSelectedUnits(list);
    GameValue valList = gstate->CreateGameValue(GameArray);
    GameArrayType &array = valList;
    for (int i=0; i<list.Size(); i++)
    {
      AIUnit *unit = list[i];
      if (!unit) continue;

      Person *person = unit->GetPerson();
      if (!person) continue;

      array.Add(GameValueExt(person));
    }
    arguments.Add(valList);

    GameVarSpace local(false);  
    gstate->BeginContext(&local);
    gstate->VarSetLocal("_this", value, true);
    gstate->Execute(item->_eventHandler, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    gstate->EndContext();
  }
#endif

  switch (cmd)
  {
  case CMD_HIDE_MENU:
    return processedMenu;
  case CMD_BACK:
    {
      if (menuSize > 1)
      {
        _menu.Delete(--menuSize);
        CheckMenuItems(_menu[menuSize - 1]); // _menu.Size() can change inside CheckMenuItems
        _menu[_menu.Size() - 1]->ValidateIndex();
      }
      else
      {
        ClearSelectedUnits();
/*
        if (_forcingCommandingMode)_forcingCommandingMode=false; //so it starts the "UNITS" menu again
        else HideMenu();
*/
        HideMenu();
      }
    }
    resetMenu = false;
    return false;
  case CMD_MOVE:
    // move
    if (notEmpty)
    {
      AIGroup *group = agent->GetGroup();
      if (group) IssueMove(group);
    }
    else if (vehicleCommander)
      IssueVMove(transport);
    return processedMenu;
  case CMD_GETIN:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        const IAttribute *attrVeh = FindMenuAttribute("vehicle");
        const IAttribute *attrPos = FindMenuAttribute("getinpos");
        if (attrVeh && attrPos)
        {
          Transport *veh = dyn_cast<Transport>(GetAttrEntityAI(attrVeh));
          if (veh)
          {
            int pos = GetAttrInt(attrPos);
            OLinkPermNOArray(AIUnit) list;
            ListSelectedUnits(list);
            IssueGetIn(group, list, veh, pos);
          }
        }
      }
    }
    _visibleListTemp.Clear();
    return processedMenu;
  case CMD_GETOUT:
    IssueCommand(vehicle, Command::GetOut);
    return processedMenu;
  case CMD_ACTION:
    {
      AIGroup *group = agent->GetGroup();
      if (group) IssueAction(group, *item);
    }
    _visibleListTemp.Clear();
    _actionsTemp.Clear();
    return processedMenu;
    // heal
  case CMD_HEAL:
    IssueHeal();
    return processedMenu;
  case CMD_REPAIR_VEHICLE:
    IssueRepair();
    return processedMenu;
  case CMD_TAKE_BACKPACK:
    IssueTakeBackpack();
    return processedMenu;
  case CMD_ASSEMBLE_WEAPON:
    IssueAssemble();
    return processedMenu;
  case CMD_DISASSEMBLE_WEAPON:
    IssueDisassemble();
    return processedMenu;
  case CMD_SUPPRESS:
    {// suppress
      OLinkPermNOArray(AIUnit) list;
      ListSelectingUnits(list);
      if (list.Size() > 0)
      {
        AIGroup *group = agent->GetGroup();
        if (group) group->SendSuppress(Glob.time+ 30.0f, list, false);
      }
    }
    return processedMenu;
  case CMD_WATCH:
    if (notEmpty)
    {
      AIGroup *group = agent->GetGroup();
      if (group) IssueWatchTarget(group, *item);
    }
    else if (vehicleObserver)
      IssueVWatchTarget(transport, *item);
    _visibleListTemp.Clear();
    return processedMenu;
  case CMD_HIDE:
    IssueCommand(vehicle, Command::Hide, Command::MinorHidden, Command::MoveCautios);
    return processedMenu;
  case CMD_STOP:
    if (notEmpty)
    {
      IssueCommand(vehicle, Command::Stop);
    }
    else if (vehicleCommander)
    {
      Person *driver = transport->PilotUnit() ? transport->PilotUnit()->GetPerson() : NULL;
      SendCommandHelper(transport, driver, SCStop);
    }
    return processedMenu;
  case CMD_EXPECT:
    IssueCommand(vehicle, Command::Expect);
    return processedMenu;
  case CMD_WATCH_DIR:
    {
      AIGroup *group = agent->GetGroup();
      if (group) IssueWatch(group);
    }
    return processedMenu;
  case CMD_WATCH_AROUND:
    if (notEmpty)
    {
      AIGroup *group = agent->GetGroup();
      if (group) IssueWatchAround(group);
    }
    else if (vehicleObserver)
      IssueVWatchAround(transport);
    return processedMenu;
  case CMD_WATCH_AUTO:
    if (notEmpty)
    {
      AIGroup *group = agent->GetGroup();
      if (group) IssueWatchAuto(group);
    }
    else if (vehicleObserver)
      IssueVWatchAuto(transport);
    _visibleListTemp.Clear();
    return processedMenu;
  case CMD_ENGAGE:
    {
      AIGroup *group = agent->GetGroup();
      if (group) IssueEngage(group);
    }
    return processedMenu;
  case CMD_FIRE:
    {
      AIGroup *group = agent->GetGroup();
      if (group) IssueFire(group);
    }
    return processedMenu;
  case CMD_JOIN:
    IssueCommand(vehicle, Command::Join);
    // changed: update formation direction whenever CMD_JOIN is issued
    // if (_selection == STAll)
    {
      AIUnit *unit = agent->GetUnit();
      AISubgroup *subgroup = unit ? unit->GetSubgroup() : NULL;
      if (subgroup) subgroup->SetDirection(unit->Direction(unit->GetFutureVisualState()));
    }
    return processedMenu;
  case CMD_JOIN_ALL:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        Command cmd;
        cmd._message = Command::Join;
        cmd._context = Command::CtxUI;
        cmd._joinToSubgroup = group->MainSubgroup();

        OLinkPermNOArray(AIUnit) list;
        for (int i=0; i<group->NUnits(); i++)
        {
          AIUnit *unit = group->GetUnit(i);
          if (unit && unit != agent) list.Add(unit);
        }

        group->SendCommand(cmd, list);
      }
    }
    return processedMenu;
  case CMD_FORM_COLUMN:
  case CMD_FORM_STAGCOL:
  case CMD_FORM_WEDGE:
  case CMD_FORM_ECHLEFT:
  case CMD_FORM_ECHRIGHT:
  case CMD_FORM_VEE:
  case CMD_FORM_LINE:
  case CMD_FORM_DIAMOND:
  case CMD_FORM_FILE:
    {
      AIUnit *unit = agent->GetUnit();
      AISubgroup *subgroup = unit ? unit->GetSubgroup() : NULL;
      AIGroup *group = subgroup ? subgroup->GetGroup() : NULL;
      if (group) group->SendFormation
      (
        (AI::Formation)(AI::FormColumn + item->_cmd - CMD_FORM_COLUMN),
        subgroup
      );              
    }
    return processedMenu;
  case CMD_STEALTH:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        bool direct = false;
#if _ENABLE_DIRECT_MESSAGES
        const IAttribute *attr = FindMenuAttribute("direct");
        if (attr) direct = GetAttrBool(attr);
#endif
        OLinkPermNOArray(AIUnit) list;
        ListSelectedUnits(list);
        group->SendBehaviour(CMStealth, list, direct);
      }
    }
    return processedMenu;
  case CMD_COMBAT:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        bool direct = false;
#if _ENABLE_DIRECT_MESSAGES
        const IAttribute *attr = FindMenuAttribute("direct");
        if (attr) direct = GetAttrBool(attr);
#endif
        OLinkPermNOArray(AIUnit) list;
        ListSelectedUnits(list);
        group->SendBehaviour(CMCombat, list, direct);
      }
    }
    return processedMenu;
  case CMD_AWARE:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        bool direct = false;
#if _ENABLE_DIRECT_MESSAGES
        const IAttribute *attr = FindMenuAttribute("direct");
        if (attr) direct = GetAttrBool(attr);
#endif
        OLinkPermNOArray(AIUnit) list;
        ListSelectedUnits(list);
        group->SendBehaviour(CMAware, list, direct);
      }
    }
    return processedMenu;
  case CMD_SAFE:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        bool direct = false;
#if _ENABLE_DIRECT_MESSAGES
        const IAttribute *attr = FindMenuAttribute("direct");
        if (attr) direct = GetAttrBool(attr);
#endif
        OLinkPermNOArray(AIUnit) list;
        ListSelectedUnits(list);
        group->SendBehaviour(CMSafe, list, direct);
      }
    }
    return processedMenu;
  case CMD_KEEP_FORM:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        OLinkPermNOArray(AIUnit) list;
        ListSelectedUnits(list);
        group->SendLooseFormation(false, list);
      }
    }
    return processedMenu;
  case CMD_LOOSE_FORM:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        OLinkPermNOArray(AIUnit) list;
        ListSelectedUnits(list);
        group->SendLooseFormation(true, list);
      }
    }
    return processedMenu;
  case CMD_HOLD_FIRE:
    {
      OLinkPermNOArray(AIUnit) list;
      ListSelectingUnits(list);
      if (list.Size() > 0)
      {
        AIGroup *group = agent->GetGroup();
        if (group) group->SendOpenFire(OFSHoldFire, list);
      }
      else if (vehicleObserver)
      {
        Person *gunner = transport->GunnerUnit() ? transport->GunnerUnit()->GetPerson() : NULL;
        SendCommandHelper(transport, gunner, SCCeaseFire);
      }
      return processedMenu;
    }
  case CMD_OPEN_FIRE:
    {
      OLinkPermNOArray(AIUnit) list;
      ListSelectingUnits(list);
      if (list.Size() > 0)
      {
        AIGroup *group = agent->GetGroup();
        if (group) group->SendOpenFire(OFSOpenFire, list);
      }
      else if (vehicleObserver)
      {
        Person *gunner = transport->GunnerUnit() ? transport->GunnerUnit()->GetPerson() : NULL;
        SendCommandHelper(transport, gunner, SCFire);
      }
    }
    return processedMenu;
  case CMD_ADVANCE:
    SetFormationPos(agent, AI::PosAdvance);
    return processedMenu;
  case CMD_STAY_BACK:
    SetFormationPos(agent, AI::PosStayBack);
    return processedMenu;
  case CMD_FLANK_LEFT:
    SetFormationPos(agent, AI::PosFlankLeft);
    return processedMenu;
  case CMD_FLANK_RIGHT:
    SetFormationPos(agent, AI::PosFlankRight);
    return processedMenu;
  case CMD_POS_UP:
    SetUnitPosition(agent, UPUp);
    return processedMenu;
  case CMD_POS_MIDDLE:
    SetUnitPosition(agent, UPMiddle);
    return processedMenu;
  case CMD_POS_DOWN:
    SetUnitPosition(agent, UPDown);
    return processedMenu;
  case CMD_POS_AUTO:
    SetUnitPosition(agent, UPAuto);
    return processedMenu;
  case CMD_REPORT:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        OLinkPermNOArray(AIUnit) list;
        ListSelectedUnits(list);
        group->SendReportStatus(list);
      }
    }
    return processedMenu;
  case CMD_TEAM_MAIN:
    SelectTeam(TeamMain);
    return false;
  case CMD_TEAM_RED:
    SelectTeam(TeamRed);
    return false;
  case CMD_TEAM_GREEN:
    SelectTeam(TeamGreen);
    return false;
  case CMD_TEAM_BLUE:
    SelectTeam(TeamBlue);
    return false;
  case CMD_TEAM_YELLOW:
    SelectTeam(TeamYellow);
    return false;
  case CMD_ASSIGN_MAIN:
    AssignTeam(agent,TeamMain);
    return processedMenu;
  case CMD_ASSIGN_RED:
    AssignTeam(agent,TeamRed);
    return processedMenu;
  case CMD_ASSIGN_GREEN:
    AssignTeam(agent,TeamGreen);
    return processedMenu;
  case CMD_ASSIGN_BLUE:
    AssignTeam(agent,TeamBlue);
    return processedMenu;
  case CMD_ASSIGN_YELLOW:
    AssignTeam(agent,TeamYellow);
    return processedMenu;
  case CMD_TEAM_DISSOLVE:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center)
      {
        Team team = TeamMain;
        switch (_selection)
        {
        case STTeamRed:
          team = TeamRed; break;
        case STTeamGreen:
          team = TeamGreen; break;
        case STTeamBlue:
          team = TeamBlue; break;
        case STTeamYellow:
          team = TeamYellow; break;
        }
        if (team != TeamMain)
        {
          OLinkPermNOArray(AIUnit) list;
          ListSelectedUnits(list);
          ListTeam(list, team);
          group->GetRadio().Transmit(new RadioMessageTeam(group, list, TeamMain));
        }
      }
    }
    return processedMenu;
  case CMD_RADIO_ALPHA:
    ActivateSensor(ASAAlpha);
    return processedMenu;
  case CMD_RADIO_BRAVO:
    ActivateSensor(ASABravo);
    return processedMenu;
  case CMD_RADIO_CHARLIE:
    ActivateSensor(ASACharlie);
    return processedMenu;
  case CMD_RADIO_DELTA:
    ActivateSensor(ASADelta);
    return processedMenu;
  case CMD_RADIO_ECHO:
    ActivateSensor(ASAEcho);
    return processedMenu;
  case CMD_RADIO_FOXTROT:
    ActivateSensor(ASAFoxtrot);
    return processedMenu;
  case CMD_RADIO_GOLF:
    ActivateSensor(ASAGolf);
    return processedMenu;
  case CMD_RADIO_HOTEL:
    ActivateSensor(ASAHotel);
    return processedMenu;
  case CMD_RADIO_INDIA:
    ActivateSensor(ASAIndia);
    return processedMenu;
  case CMD_RADIO_JULIET:
    ActivateSensor(ASAJuliet);
    return processedMenu;
  case CMD_NEXT_WAYPOINT:
    {
      // find the candidate for the next waypoint 
      Vector3 pos;
      if (FindNextWaypoint(pos, agent))
      {
        if (notEmpty)
        {
          // FIX - implementation of command "NEXT WAYPOINT" for selected units
          AIGroup *group = agent->GetGroup();
          if (group)
          {
            Command cmd;
            cmd._message = Command::Move;
            cmd._context = Command::CtxUI;
            cmd._destination = pos;
            OLinkPermNOArray(AIUnit) list;
            ListSelectedUnits(list);
            group->SendCommand(cmd, list);
            GWorld->UI()->ShowWaypointPosition();
          }
        }
        else if (vehicleCommander)
        {
          if (transport->IsLocal())
            transport->SendMove(pos);
          else
          {
            RadioMessageVMove msg(transport, pos);
            GetNetworkManager().SendMsg(&msg);
          }
          GWorld->UI()->ShowWaypointPosition();
        }
      }
    }
    return processedMenu;
  case CMD_REPLY_FIREREADY:
    SendFireReady(agent, true);
    return processedMenu;
  case CMD_REPLY_FIRENOTREADY:
    SendFireReady(agent, false);
    return processedMenu;
  case CMD_REPLY_DONE:
    SendAnswer(agent, AI::CommandCompleted);
    return processedMenu;
  case CMD_REPLY_FAIL:
    SendAnswer(agent, AI::CommandFailed);
    return processedMenu;
  case CMD_REPLY_COPY:
    SendConfirm(agent);
    return processedMenu;
  case CMD_REPLY_KILLED:
    {
      OLinkPermNOArray(AIUnit) list;
      ListSelectedUnits(list);
      SendKilled(agent, list);
    }
    return processedMenu;
  case CMD_REPLY_HIT:
    if (agent->GetVehicleIn())
    {
      SendResourceState(agent, AI::DamageCritical);
    }
    else
    {
      SendResourceState(agent, AI::HealthCritical);
    }
    return processedMenu;
  case CMD_REPLY_INJURED:
    SendResourceState(agent, AI::HealthCritical);
    {
      AIGroup *group = agent->GetGroup();
      if (group && group->Leader() == agent)
      {
        Assert(agent->GetUnit());
        // check both group and center radio channel

        //already taken care of wounded soldier
        if (agent->GetVehicle() && agent->GetVehicle()->AssignedAttendant()) 
          return processedMenu;
        if (agent->GetGroup()->CommandSent(agent->GetUnit(), Command::Heal)
          && agent->GetLifeState() != LifeStateUnconscious) 
          return processedMenu;


        const AITargetInfo *info = group->FindHealPosition(AIUnit::RSCritical, agent->GetUnit());
        if (info)
        {
          EntityAI *veh = info->_idExact;
          veh->CommanderUnit();
          AIBrain *brain =  veh->CommanderUnit();
          if (brain && brain->IsFreeSoldier() && agent->GetVehicle() && brain->GetVehicle()
            )
          {//medic
            agent->GetVehicle()->SetAssignedAttendant(brain->GetVehicle());
            Command cmd;
            cmd._message = Command::HealSoldier;
            cmd._destination = agent->Position(agent->GetFutureVisualState());
            cmd._target = agent->GetVehicle();
            cmd._time = Glob.time + COMMAND_TIMEOUT;
            group->SendAutoCommandToUnit(cmd, brain->GetUnit(), true);
          }
          else if (veh)
          {//ambulance
            Command cmd;
            cmd._message = Command::Heal;
            cmd._destination = veh->FutureVisualState().Position();
            cmd._target = veh;
            cmd._time = Glob.time + COMMAND_TIMEOUT;
            group->SendAutoCommandToUnit(cmd, agent->GetUnit(), true);
          }
        }
        else if(agent->GetLifeState() == LifeStateUnconscious)
        {//first aid
          const AIUnit *info = group->FindFirstAid(AIUnit::RSCritical, agent->GetUnit());
          if (info)
          {
            EntityAI *veh = info->GetVehicle();
            veh->CommanderUnit();
            AIBrain *brain =  veh->CommanderUnit();
            if (brain && brain->IsFreeSoldier() && agent->GetVehicle() && brain->GetVehicle())
            {
              agent->GetVehicle()->SetAssignedAttendant(brain->GetVehicle());
              Command cmd;
              cmd._message = Command::FirstAid;
              cmd._destination = agent->Position(agent->GetFutureVisualState());
              cmd._target = agent->GetVehicle();
              cmd._time = Glob.time + COMMAND_TIMEOUT;
              group->SendAutoCommandToUnit(cmd, brain->GetUnit(), true);
            }
          }
        }
      }
      //show assigned medic
      _lastMedicTime = Glob.uiTime;
    }
    return processedMenu;
  case CMD_REPLY_AMMO_LOW:
    SendResourceState(agent, AI::AmmoLow);
    return processedMenu;
  case CMD_REPLY_FUEL_LOW:
    SendResourceState(agent, AI::FuelLow);
    return processedMenu;
  case CMD_REPLY_REPEAT:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center)
      {
        Assert(agent->GetUnit());
        group->GetRadio().Transmit(new RadioMessageRepeatCommand(agent->GetUnit(), group));
      }
    }
    return processedMenu;
  case CMD_REPLY_WHERE_ARE_YOU:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center)
      {
        Assert(agent->GetUnit());
        group->GetRadio().Transmit(new RadioMessageWhereAreYou(agent->GetUnit(), group));
      }
    }
    return processedMenu;
  case CMD_REPLY_ENGAGING:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center)
      {
        Command cmd;
        cmd._message = Command::Attack;
        Assert(agent->GetUnit());
        OLinkPermNOArray(AIUnit) list;
        AIUnit *unit = agent->GetUnit();
        if (unit)
        {
          // if I am a subgroup leader, report the whole subgroup
          if (unit->GetSubgroup() && unit->GetSubgroup()->Leader()==unit)
          {
            unit->GetSubgroup()->GetUnitsListNoCargo(list);
          }
          else
          {
            list.Add(unit);
          }
          
          group->GetRadio().Transmit(new RadioMessageNotifyCommand(list, group, cmd));
        }
      }
    }
    return processedMenu;
  case CMD_REPLY_UNDER_FIRE:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center)
      {
        Assert(agent->GetUnit());
        group->GetRadio().Transmit(new RadioMessageUnderFire(agent->GetUnit(), group));
      }
    }
    return processedMenu;
  case CMD_REPLY_ONE_LESS:
    {
      AIGroup *group = agent->GetGroup();
      if (group) SendObjectDestroyed(agent, group);
    }
    return processedMenu;
  case CMD_SUPPORT_MEDIC:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center) center->GetCommandRadio().Transmit(new RadioMessageSupportAsk(group, ATHealSoldier));
    }
    return processedMenu;
  case CMD_SUPPORT_AMBULANCE:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center) center->GetCommandRadio().Transmit(new RadioMessageSupportAsk(group, ATHeal));
    }
    return processedMenu;
  case CMD_SUPPORT_REPAIR:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center) center->GetCommandRadio().Transmit(new RadioMessageSupportAsk(group, ATRepair));
    }
    return processedMenu;
  case CMD_SUPPORT_REARM:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center) center->GetCommandRadio().Transmit(new RadioMessageSupportAsk(group, ATRearm));
    }
    return processedMenu;
  case CMD_SUPPORT_REFUEL:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center) center->GetCommandRadio().Transmit(new RadioMessageSupportAsk(group, ATRefuel));
    }
    return processedMenu;
  case CMD_SUPPORT_DONE:
    {
      AIGroup *group = agent->GetGroup();
      AICenter *center = group ? group->GetCenter() : NULL;
      if (center) center->GetCommandRadio().Transmit(new RadioMessageSupportDone(group));
    }
    return processedMenu;
#if _ENABLE_CONVERSATION
  case CMD_CONVERSATION:
    if (!_conversationContext) return true;

    Assert(_conversationContext->_center);
    Assert(_conversationContext->_askingUnit);
    if (!_conversationContext->_askingUnit->IsLocal())
    {
      RptF("Conversation - need to be initiated by speaker owner");
      return true;
    }
    if (_conversationContext->_handlerResult.GetNil())
    {
      // old reaction (based on class UI)
      const KBUINodeInfo *info = NULL;
      const IAttribute *attr = FindMenuAttribute("message");
      if (attr) info = GetAttrMessage(attr);
      Assert(info);

      const KBMessageInfo *message = _conversationContext->_center->GetSentence(info);
      if (message)
      {
        // tell
        KBTell(_conversationContext->_askingUnit, _conversationContext->_askedUnit, message);
        return true;
      }
      else
      {
        // create submenu
        _conversationContext->_current = info;
        Ref<Menu> menu = new Menu();
        CreateDynamicMenu(menu, "CONVERSATION");
        if (menu->_items.Size() > 0)
        {
          _menu.Add(menu);
          ShowMenu();
        }
        resetMenu = false;
        return false;
      }
    }
    else
    {
      // new reaction (based on event handler)
      const IAttribute *attr = FindMenuAttribute("gamevalue");
      if (!attr)
      {
        RptF("Conversation - corrupted menu");
        return true;
      }
      const GameValue &value = GetAttrGameValue(attr);
      Assert(value.GetType() == GameArray);
      const GameArrayType &array = value;

      if (array.Size() == 4 && array[1].GetType() == GameString && array[2].GetType() == GameString && array[3].GetType() == GameArray)
      {
        // Sentence - create and send the message
        Ref<KBMessageInfo> message = _conversationContext->_askingUnit->CreateKBMessage(array[1], array[2]);
        // attributes
        const GameArrayType &attrArray = array[3];
        for (int i=0; i<attrArray.Size(); i++)
        {
          // TODO: check the format of arguments
          if(message) _conversationContext->_askingUnit->SetKBMessageArgument(message, attrArray[i]);
        }
        // send it
        KBTell(_conversationContext->_askingUnit, _conversationContext->_askedUnit, message);
        return true;
      }
      
      if (array.Size() == 2 && array[1].GetType() == GameArray)
      {
        // Submenu - update the conversation context and creat the menu
        _conversationContext->_handlerResult = array[1];
        Ref<Menu> menu = new Menu();
        CreateDynamicMenu(menu, "CONVERSATION");
        if (menu->_items.Size() > 0)
        {
          // back
          menu->AddItem(new MenuItem(RString(), DIK_BACK, CMD_BACK));

          _menu.Add(menu);
          ShowMenu();
        }
        resetMenu = false;
        return false;
      }

      return true;
    }
#endif
  case CMD_RADIO_CUSTOM_1:
  case CMD_RADIO_CUSTOM_2:
  case CMD_RADIO_CUSTOM_3:
  case CMD_RADIO_CUSTOM_4:
  case CMD_RADIO_CUSTOM_5:
  case CMD_RADIO_CUSTOM_6:
  case CMD_RADIO_CUSTOM_7:
  case CMD_RADIO_CUSTOM_8:
  case CMD_RADIO_CUSTOM_9:
  case CMD_RADIO_CUSTOM_0:
    {
      RString name = RString("#") + _customRadio[item->_cmd - CMD_RADIO_CUSTOM_1];
      ChatChannel channel = CCGroup;
      if (GWorld->GetMode() == GModeNetware) channel = ActualChatChannel();
      
      RadioChannel *FindChannel(AIBrain *unit, int channel);
      RadioChannel *radio = FindChannel(agent, channel);

      if (radio)
      {
      radio->Say(name, agent, "", "", 2.0);
      SendRadioChatWave
      (
        channel, name, agent, ""
      );
    }
    }
    return processedMenu;
  case CMD_MP_CHANNEL_GLOBAL:
    GetNetworkManager().SetVoiceChannel(CCGlobal);
    GWorld->OnChannelChanged();
    return processedMenu;
  case CMD_MP_CHANNEL_SIDE:
    GetNetworkManager().SetVoiceChannel(CCSide);
    GWorld->OnChannelChanged();
    return processedMenu;
  case CMD_MP_CHANNEL_COMMAND:
    GetNetworkManager().SetVoiceChannel(CCCommand);
    GWorld->OnChannelChanged();
    return processedMenu;
  case CMD_MP_CHANNEL_GROUP:
    GetNetworkManager().SetVoiceChannel(CCGroup);
    GWorld->OnChannelChanged();
    return processedMenu;
  case CMD_MP_CHANNEL_VEHICLE:
    GetNetworkManager().SetVoiceChannel(CCVehicle);
    GWorld->OnChannelChanged();
    return processedMenu;
  case CMD_SINGLE_UNITS:
    _unitsSingle = new Menu();
    CreateDynamicMenu(_unitsSingle, "SINGLE_UNITS");
    resetMenu = false;
    return false;
  case CMD_UNIT_1:
  case CMD_UNIT_2:
  case CMD_UNIT_3:
  case CMD_UNIT_4:
  case CMD_UNIT_5:
  case CMD_UNIT_6:
  case CMD_UNIT_7:
  case CMD_UNIT_8:
  case CMD_UNIT_9:
  case CMD_UNIT_10:
  case CMD_UNIT_11:
  case CMD_UNIT_12:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        if (actionKey)
        {
          int index = cmd - CMD_UNIT_1;
          AIUnit *u = group->GetUnit(index);
          if (u && IsSelectedUnit(u))
          {
            if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
            {
              if (u != agent)
              {
                UnselectUnit(u);
              }
              resetMenu = false;
              return false;
            }
            else
            {
              _selection = STUnit;
              goto ShowCommandsMenu;
            }
          }
          else
          {
            if (u && u != agent)
            {
              SelectUnit(u);
            }
            resetMenu = false;
            return false;
          }
        }
        else
        {
          int index = cmd - CMD_UNIT_1;
          AIUnit *u = group->GetUnit(index);
          if (!u || u == agent)
          {
            resetMenu = false;
            return false;
          }
          SelectUnit(u);
          _selection = STUnit;
          goto ShowCommandsMenu;
        }
      }
    }
    return processedMenu;
  case CMD_MY_VEHICLE:
    ClearSelectedUnits();
    _selection = STNone;
    goto ShowCommandsMenu;
  case CMD_VEHICLE_1:
  case CMD_VEHICLE_2:
  case CMD_VEHICLE_3:
  case CMD_VEHICLE_4:
  case CMD_VEHICLE_5:
  case CMD_VEHICLE_6:
  case CMD_VEHICLE_7:
  case CMD_VEHICLE_8:
  case CMD_VEHICLE_9:
  case CMD_VEHICLE_10:
  case CMD_VEHICLE_11:
  case CMD_VEHICLE_12:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        ClearSelectedUnits();
        int index = cmd - CMD_VEHICLE_1;
        AIUnit *u = group->GetUnit(index);
        if (!u)
        {
          resetMenu = false;
          return false;
        }
        Transport *veh = u->GetVehicleIn();
        if (!veh)
        {
          resetMenu = false;
          return false;
        }
        u = veh->CommanderUnit() ? veh->CommanderUnit()->GetUnit() : NULL;
        if (!u || u->GetGroup() != group)
        {
          resetMenu = false;
          return false;
        }
        SelectUnit(u);
        _selection = STVehicle;
        goto ShowCommandsMenu;
      }
    }
    return processedMenu;
  case CMD_SELECT_MAIN:
    SelectTeam(TeamMain);
    _selection = STTeamMain;
    goto ShowCommandsMenu;
  case CMD_SELECT_RED:
    SelectTeam(TeamRed);
    _selection = STTeamRed;
    goto ShowCommandsMenu;
  case CMD_SELECT_GREEN:
    SelectTeam(TeamGreen);
    _selection = STTeamGreen;
    goto ShowCommandsMenu;
  case CMD_SELECT_BLUE:
    SelectTeam(TeamBlue);
    _selection = STTeamBlue;
    goto ShowCommandsMenu;
  case CMD_SELECT_YELLOW:
    SelectTeam(TeamYellow);
    _selection = STTeamYellow;
    goto ShowCommandsMenu;
  case CMD_UNITS_ALL:
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        ClearSelectedUnits();
        for (int i=0; i<group->NUnits(); i++)
        {
          AIUnit *u = group->GetUnit(i);
          if (u && u != agent)
          {
            SelectUnit(u);
          }
        }
        _selection = STAll;
        goto ShowCommandsMenu;
      }
    }
    return processedMenu;
  ShowCommandsMenu:
    {
      Menu *menu = new Menu();
      menu->Load("RscMainMenu", MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
      _menu.Resize(1);
      _menu[0] = menu;
#if _ENABLE_CONVERSATION
      _conversationContext = NULL;
#endif
      CheckMenuItems(menu);// _menu.Size() can change inside CheckMenuItems
      _menu[0]->ValidateIndex();
      ShowMenu();
    }
    resetMenu = false;
    return false;
  // Context sensitive commands
  case CMD_MOVE_AUTO:
    IssueCommand(vehicle, Command::Move, Command::Undefined, Command::MoveFast);
    return processedMenu;
  case CMD_STOP_AUTO:
    IssueCommand(vehicle, Command::Stop);
    return processedMenu;
  case CMD_WATCH_CTX:
    IssueWatchDir();
    return processedMenu;
  case CMD_GETIN_AUTO:
    IssueCommand(vehicle, Command::GetIn, Command::Undefined, Command::MoveFast);
    return processedMenu;
  case CMD_REARM_AUTO:
    if (_target.IdExact())
    {
      AIGroup *group = agent->GetGroup();
      if (group)
      {
        OLinkPermNOArray(AIUnit) list;
        ListSelectingUnits(list);

        bool join = false;
        for (int i=0; i<list.Size(); i++)
        {
          AIUnit *u = list[i];
          if (u && u->GetSubgroup() == group->MainSubgroup())
          {
            join = true;
            break;
          }
        }

        Command cmd;
        cmd._message = Command::Rearm;
        cmd._destination = _target->GetPosition();
        cmd._target = _target.IdExact();
        if (join)
        {
          cmd._context = Command::CtxUIWithJoin;
          cmd._joinToSubgroup = group->MainSubgroup();
        }
        else
        {
          cmd._context = Command::CtxUI;
        }
        group->SendCommand(cmd, list);
      }
    }
    return processedMenu;
  case CMD_ATTACK_AUTO:
    {
      OLinkPermNOArray(AIUnit) list;
      ListSelectedUnits(list);
      AIGroup *grp = agent->GetGroup();
      if (_target && grp)
        grp->SendTarget(TargetToFire(_target,_target->State(agent)), false, false, list);
    }
    return processedMenu;
  case CMD_ENGAGE_AUTO:
    {
      AIGroup *grp = agent->GetGroup();
      if (grp)
        IssueEngage(grp);
    }
    return processedMenu;
  case CMD_FIRE_AUTO:
    {
      AIGroup *grp = agent->GetGroup();
      if (grp)
        IssueFire(grp);
    }
    return processedMenu;
  case CMD_FIRE_AT_POSITION_AUTO:
    IssueCommand(vehicle, Command::FireAtPosition, Command::Minor);
    return processedMenu;
  case CMD_SELECT_AUTO:
    // select the unit we are pointing to and invoke Main Menu
    {
      EntityAI *targetAI = _target.IdExact();
      if (targetAI)
      {
        AIBrain *u = targetAI->CommanderUnit();
        if (agent->IsGroupLeader() && u && u->GetGroup() && u->GetGroup() == agent->GetGroup())
        { // inside my group / subgroup
          Team team = GetTeam(u->GetUnit());
          if (team!=TeamMain && (GInput.keys[DIK_LSHIFT] || GInput.keys[DIK_RSHIFT]))
          {
            OLinkPermNOArray(AIUnit) list;
            ListTeam(list, team);
            bool allMembersSelected = true;
            for (int i=0; i<list.Size(); i++)
            {
              if (!IsSelectedUnit(list[i]))
              {
                allMembersSelected = false;
                break;
              }
            }
          ClearSelectedUnits();
            for (int i=0; i<list.Size(); i++)
            {
              if (allMembersSelected) UnselectUnit(list[i]);
              else SelectUnit(list[i]);
            }
          }
          else 
          {
            ClearSelectedUnits();
          SelectUnit(u->GetUnit());
          }
        }
          _selection = STUnit;
          // goto ShowCommandsMenu;
        }
      }
    resetMenu = false;
    return false;
  case CMD_SELECT_AUTO_ADD:
    // select the unit we are pointing to and invoke Main Menu
    {
      EntityAI *targetAI = _target.IdExact();
      if (targetAI)
      {
        AIBrain *u = targetAI->CommanderUnit();
        if (agent->IsGroupLeader() && u && u->GetGroup() && u->GetGroup() == agent->GetGroup())
        { // inside my group / subgroup
          //ClearSelectedUnits();
          Team team = GetTeam(u->GetUnit());
          if (team!=TeamMain && (GInput.keys[DIK_LSHIFT] || GInput.keys[DIK_RSHIFT]))
          {
            OLinkPermNOArray(AIUnit) list;
            ListTeam(list, team);
            bool allMembersSelected = true;
            for (int i=0; i<list.Size(); i++)
            {
              if (!IsSelectedUnit(list[i]))
              {
                allMembersSelected = false;
                break;
              }
            }
            for (int i=0; i<list.Size(); i++)
            {
              if (allMembersSelected) UnselectUnit(list[i]);
              else SelectUnit(list[i]);
            }
          }
          else 
          {
          SelectUnit(u->GetUnit());
          }
          _selection = STUnit;
          // goto ShowCommandsMenu;
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_DESELECT_AUTO:
    // select the unit we are pointing to and invoke Main Menu
    {
      EntityAI *targetAI = _target.IdExact();
      if (targetAI)
      {
        AIBrain *u = targetAI->CommanderUnit();
        if (agent->IsGroupLeader() && u && u->GetGroup() && u->GetGroup() == agent->GetGroup())
        { // inside my group / subgroup
          //ClearSelectedUnits();
          Team team = GetTeam(u->GetUnit());
          if (team!=TeamMain && (GInput.keys[DIK_LSHIFT] || GInput.keys[DIK_RSHIFT]))
          {
            OLinkPermNOArray(AIUnit) list;
            ListTeam(list, team);
            bool allMembersSelected = true;
            for (int i=0; i<list.Size(); i++)
            {
              if (!IsSelectedUnit(list[i]))
              {
                allMembersSelected = false;
                break;
              }
            }
            for (int i=0; i<list.Size(); i++)
            {
              if (allMembersSelected) UnselectUnit(list[i]);
              else SelectUnit(list[i]);
            }
          }
          else 
          {
          UnselectUnit(u->GetUnit());
          _selection = STUnit;
          }
          // goto ShowCommandsMenu;
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_JOIN_AUTO:
    // send join to the unit we are pointing to
    {
      AIGroup *group = agent->GetGroup();
      if (group && group->Leader() == agent)
      {
        EntityAI *targetAI = _target.IdExact();
        if (targetAI)
        {
          AIBrain *u = targetAI->CommanderUnit();
          if (u && u->GetGroup() == group)
          { // inside my group / subgroup
            Command cmd;
            cmd._message = Command::Join;
            cmd._context = Command::CtxUI;
            cmd._joinToSubgroup = group->MainSubgroup();
            OLinkPermNOArray(AIUnit) list;
            list.Realloc(1);
            list.Resize(1);
            list[0] = u->GetUnit();
            group->SendCommand(cmd, list);
          }
        }
      }
    }
    return processedMenu;
  case CMD_OPEN_FIRE_AUTO:
    {
      AIGroup *group = agent->GetGroup();
      if (group && group->Leader() == agent)
      {
        EntityAI *targetAI = _target.IdExact();
        if (targetAI)
        {
          AIBrain *u = targetAI->CommanderUnit();
          if (u && u->GetGroup() == group)
          { // inside my group / subgroup
            OLinkPermNOArray(AIUnit) list;
            list.Realloc(1);
            list.Resize(1);
            list[0] = u->GetUnit();
            group->SendOpenFire(OFSOpenFire, list);
          }
        }
      }
    }
    return processedMenu;
  case CMD_HOLD_FIRE_AUTO:
    {
      AIGroup *group = agent->GetGroup();
      if (group && group->Leader() == agent)
      {
        EntityAI *targetAI = _target.IdExact();
        if (targetAI)
        {
          AIBrain *u = targetAI->CommanderUnit();
          if (u && u->GetGroup() == group)
          { // inside my group / subgroup
            OLinkPermNOArray(AIUnit) list;
            list.Realloc(1);
            list.Resize(1);
            list[0] = u->GetUnit();
            group->SendOpenFire(OFSHoldFire, list);
          }
        }
      }
    }
    return processedMenu;
  case CMD_COMPLEX_COMMAND:
    // select the units marked on units bar and invoke Main Menu
    {
      OLinkPermNOArray(AIUnit) list;
      ListSelectingUnits(list);
      
      ClearSelectedUnits();
      for (int i=0; i<list.Size(); i++) SelectUnit(list[i]);

      goto ShowCommandsMenu;
    }
  case CMD_SELECT_UNIT_FROM_BAR:
    {
      AIGroup *group = agent->GetGroup();
      if (group && _groupInfoSelected >= 0)
      {
        AIUnit *u = group->GetUnit(_groupInfoSelected);
        if (u) SelectUnit(u);
      }
    }
    resetMenu = false;
    return false;
  case CMD_DESELECT_UNIT_FROM_BAR:
    {
      AIGroup *group = agent->GetGroup();
      if (group && _groupInfoSelected >= 0)
      {
        AIUnit *u = group->GetUnit(_groupInfoSelected);
        if (u) UnselectUnit(u);
      }
    }
    resetMenu = false;
    return false;
  case CMD_SELECT_VEHICLE_FROM_BAR:
    {
      AIGroup *group = agent->GetGroup();
      if (group && _groupInfoSelected >= 0)
      {
        AIUnit *u = group->GetUnit(_groupInfoSelected);
        if (u)
        {
          Transport *vehicle = u->GetVehicleIn();
          if (vehicle)
          {
            for (int i=0; i<group->NUnits(); i++)
            {
              AIUnit *unit = group->GetUnit(i);
              if (unit && unit != agent && unit->GetVehicleIn() == vehicle) SelectUnit(unit);
            }
          }
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_DESELECT_VEHICLE_FROM_BAR:
    {
      AIGroup *group = agent->GetGroup();
      if (group && _groupInfoSelected >= 0)
      {
        AIUnit *u = group->GetUnit(_groupInfoSelected);
        if (u)
        {
          Transport *vehicle = u->GetVehicleIn();
          if (vehicle)
          {
            for (int i=0; i<group->NUnits(); i++)
            {
              AIUnit *unit = group->GetUnit(i);
              if (unit && unit != agent && unit->GetVehicleIn() == vehicle) UnselectUnit(unit);
            }
          }
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_SELECT_TEAM_FROM_BAR:
    {
      AIGroup *group = agent->GetGroup();
      if (group && _groupInfoSelected >= 0)
      {
        AIUnit *u = group->GetUnit(_groupInfoSelected);
        if (u)
        {
          Team team = GetTeam(u);
          if (team != TeamMain)
          {
            TeamMembers &members = _teamMembers[team];
            for (int i=0; i<members.Size(); i++)
            {
              AIUnit *unit = members[i];
              if (unit && unit != agent) SelectUnit(unit);
            }
          }
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_DESELECT_TEAM_FROM_BAR:
    {
      AIGroup *group = agent->GetGroup();
      if (group && _groupInfoSelected >= 0)
      {
        AIUnit *u = group->GetUnit(_groupInfoSelected);
        if (u)
        {
          Team team = GetTeam(u);
          if (team != TeamMain)
          {
            TeamMembers &members = _teamMembers[team];
            for (int i=0; i<members.Size(); i++)
            {
              AIUnit *unit = members[i];
              if (unit && unit != agent) UnselectUnit(unit);
            }
          }
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_SWITCH_TO_LEADER:
    {
      AIGroup *group = agent->GetGroup();
      AIUnit *unit = group ? group->Leader() : NULL;
      if (unit)
      {
        void ProcessTeamSwitch(Person *newPlayer, Person *oldPlayer, EntityAI *killer, bool respawn);
        ProcessTeamSwitch(unit->GetPerson(), agent->GetPerson(), NULL, false);
      }
    }
    return true;
  case CMD_SWITCH_TO_SELECTED:
    {
      OLinkPermNOArray(AIUnit) list;
      ListSelectingUnits(list);
      if (list.Size() == 1)
      {
        AIUnit *unit = list[0];
        if (unit)
        {
          void ProcessTeamSwitch(Person *newPlayer, Person *oldPlayer, EntityAI *killer, bool respawn);
          ProcessTeamSwitch(unit->GetPerson(), agent->GetPerson(), NULL, false);
        }
      }
    }
    return true;
  case CMD_EXECUTE:
    {
      const IAttribute *attr = FindMenuAttribute("expression");
      if (attr)
      {
        RString expression = GetAttrString(attr)
          ;
        if (expression.GetLength() > 0)
        {
          GameVarSpace local(false);

          GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
          GameArrayType &arguments = value;

          if (_groundPointValid)
          {
            arguments.Resize(3);
            arguments[0] = _groundPoint.X();
            arguments[1] = _groundPoint.Z(); 
            arguments[2] = _groundPoint.Y()- GLandscape->RoadSurfaceYAboveWater(_groundPoint.X(),_groundPoint.Z());
          }
          else arguments.Resize(0);

          GameState *gstate = GWorld->GetGameState();
          gstate->BeginContext(&local);
          gstate->VarSetLocal("_pos",value,true); 
          gstate->VarSetLocal("_is3D",!GWorld->HasMap(),true); 
          gstate->VarSetLocal("_target",GameValueExt((Target *)_target),true); 
          gstate->Execute(expression, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
          gstate->EndContext();
        }
      }
    }
    return processedMenu;
  case CMD_NOTARGET:
    {
      AIGroup *group = agent->GetGroup();
      if(group)
      {
        OLinkPermNOArray(AIUnit) list;
        ListSelectedUnits(list);
        for (int i=0; i<list.Size(); i++)
        {
          AIUnit *u = list[i];
          if(u) u->AssignTarget(TargetToFire());
        }
      }
    }
    return processedMenu;  
  case CMD_HC_SELECT_AUTO:
    if(_hcTarget)
    {     
      AIGroup *group = agent->GetGroup();
      if(group && agent->GetUnit())
      {
        AIUnit *hcCommander = agent->GetUnit();
        AutoArray<AIHCGroup> &hcGroups = hcCommander->GetHCGroups();
        for (int i=0; i<hcGroups.Size();i++)
        {
          if(hcGroups[i].GetGroup() && hcGroups[i].GetGroup() == _hcTarget)
          {
            hcCommander->SelectHCGroup(i,true);
            OnHCSlectionChanged(hcGroups[i].GetGroup(),true);
          }
          else if(hcGroups[i].GetGroup())
          {
            hcCommander->SelectHCGroup(i,false);
            OnHCSlectionChanged(hcGroups[i].GetGroup(),false);
          }
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_HC_SELECT_AUTO_ADD:
    if(_hcTarget)
    {     
      AIGroup *group = agent->GetGroup();
      if(group && agent->GetUnit())
      {
        AIUnit *hcCommander = agent->GetUnit();
        AutoArray<AIHCGroup> &hcGroups = hcCommander->GetHCGroups();
        for (int i=0; i<hcGroups.Size();i++)
        {
          if(hcGroups[i].GetGroup() && hcGroups[i].GetGroup() == _hcTarget)
          {
            hcCommander->SelectHCGroup(i,true);
            OnHCSlectionChanged(hcGroups[i].GetGroup(),true);
            break;
          }
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_HC_DESELECT_AUTO:
    if(_hcTarget)
    {     
      AIGroup *group = agent->GetGroup();
      if(group && agent->GetUnit())
      {
        AIUnit *hcCommander = agent->GetUnit();
        AutoArray<AIHCGroup> &hcGroups = hcCommander->GetHCGroups();
        for (int i=0; i<hcGroups.Size();i++)
        {
          if(hcGroups[i].GetGroup() && hcGroups[i].GetGroup() == _hcTarget)
          {
            hcCommander->SelectHCGroup(i,false);
            OnHCSlectionChanged(hcGroups[i].GetGroup(),false);
            break;
          }
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_HC_SELECT_UNIT_FROM_BAR:
    {
      AIGroup *group = agent->GetGroup();
      if(group && agent->GetUnit()) 
      {
        AIUnit *hccommander = agent->GetUnit();
        if (hccommander && _groupInfoSelected >= 0)
        {
          hccommander->SelectHCGroup(_groupInfoSelected,true);
          OnHCSlectionChanged(group,true);
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_HC_DESELECT_UNIT_FROM_BAR:
    {
      AIGroup *group = agent->GetGroup();
      if(group && agent->GetUnit())  
      {
        AIUnit *hccommander = agent->GetUnit();
        if (hccommander && _groupInfoSelected >= 0)
        {
          hccommander->SelectHCGroup(_groupInfoSelected,false);
          OnHCSlectionChanged(group,false);
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_HC_SELECT_TEAM_FROM_BAR:
    {
      AIGroup *group = agent->GetGroup();
      if(group && agent->GetUnit())
      {
        AIUnit *hccommander = agent->GetUnit();
        if (hccommander && _groupInfoSelected >= 0)
        {
          AutoArray<AIHCGroup> &hcGroups = hccommander->GetHCGroups();
          if(_groupInfoSelected>=hcGroups.Size()) return true;
          AIHCGroup hcGroup = hcGroups[_groupInfoSelected];
          for(int i= 0;i<hcGroups.Size();i++)
          {
            if(hcGroups[i].IsInTeam(hcGroup))
            {
              hccommander->SelectHCGroup(i,true);
            }
          }
        }
      }
    }
    resetMenu = false;
    return false;
  case CMD_HC_DESELECT_TEAM_FROM_BAR:
    {
      AIGroup *group = agent->GetGroup();
      if(group && agent->GetUnit()) 
      {
        AIUnit *hccommander = agent->GetUnit(); 
        if (hccommander && _groupInfoSelected >= 0)
        {
          AutoArray<AIHCGroup> &hcGroups = hccommander->GetHCGroups();
          if(_groupInfoSelected>=hcGroups.Size()) return true;
          AIHCGroup hcGroup = hcGroups[_groupInfoSelected];
          for(int i= 0;i<hcGroups.Size();i++)
          {
            if(hcGroups[i].IsInTeam(hcGroup))
            {
              hccommander->SelectHCGroup(i,false);
            }
          }
        }
      }
    }
    resetMenu = false;
    return false;
  default:
    LogF("Bad command id %d",item->_cmd);
    return true;
  } // switch( command )
}


void InGameUI::SelectPriorCommand(AIBrain *agent, bool vehicleCommander)
{
  //////////////////////////////////////////////////////////////////////////
  // Select the cursor shape based on what are we pointing at

  OLinkPermNOArray(AIUnit) list;
  ListSelectingUnits(list);
  bool notEmpty = list.Size() > 0;

  _modeAuto = _mode;
  if (_mode == UIStrategy)
  {
    if (GInput.GetAction(UACommandWatch))
    {
      _modeAuto = UIStrategyWatch;
    }
    else if (_target)
    {
      EntityAI *targetAI=_target.IdExact();
      if( targetAI )
      {
        if (!notEmpty)
        {
          AIBrain *u = targetAI->CommanderUnit();
          if (
            agent->IsGroupLeader() &&
            u && u->GetGroup() &&
            u->GetGroup() == agent->GetGroup())
          { // inside my group / subgroup
            _modeAuto = UIStrategySelect;
          }
          if (vehicleCommander)
          {
            if (!agent->IsFriendly(_target->GetSide()))
            {
              // Enemy target - attack
              _modeAuto = UIStrategyFire;
            }
            else
            {
              // no target - move
              _modeAuto=UIStrategyMove;
            }
          }
        }
        else
        {
          // TODO: preload
          const VehicleType *target=GWorld->Preloaded(VTypeTarget);
          if( _target->IsKindOf(target) )
          {
            _modeAuto = UIStrategyAttack;
          }
          else if (agent->IsFriendly(_target->GetSide()))
          { // Friendly target
            Transport *targetTransport = dyn_cast<Transport>(targetAI);
            if (targetTransport)
            {
              // check if all are already inside
              OLinkPermNOArray(AIUnit) list;
              ListSelectingUnits(list);
              bool some = false;
              bool allInside = true;
              for (int i=0; i<list.Size(); i++)
              {
                if (list[i])
                {
                  some = true;
                  if (list[i]->GetVehicleIn() != targetTransport)
                  {
                    allInside = false;
                    break;
                  }
                }
              }
              if (some)
              {
                if(agent->GetVehicleIn() != targetTransport)
                {
                  if (allInside) _modeAuto = UIStrategyGetOut;
                  else if (targetTransport->QCanIGetInAny()) _modeAuto = UIStrategyGetIn;
                }
                else _modeAuto = UIStrategyMove;

                if (list.Size() == 1)
                {
                  AIUnit *selectedUnit = list[0];
                  if(selectedUnit && selectedUnit->IsFreeSoldier() && selectedUnit->GetLifeState() != LifeStateUnconscious && selectedUnit->GetVehicle())
                  {
                    EntityAIFull *entity = selectedUnit->GetVehicle();
                    const EntityAIType *type = entity->GetType();

                    if (!targetAI->IsDamageDestroyed() && targetAI->GetMaxHitCont()>0.7f && type->IsEngineer())
                      _modeAuto = UIStrategyRepairvehicle;

                  }
                }
              }
            }
            else
            {
              AIBrain *u = targetAI->CommanderUnit();
              if(u && u->IsFreeSoldier() && u->GetUnit() && u->GetPerson())
              {
                AIUnit *unit = u->GetUnit();
                if (list.Size() == 1)
                {
                  AIUnit *selectedUnit = list[0];
                  if(selectedUnit && selectedUnit->IsFreeSoldier() && selectedUnit->GetLifeState() != LifeStateUnconscious && selectedUnit->GetVehicle())
                  {
                    EntityAIFull *entity = selectedUnit->GetVehicle();
                    const EntityAIType *type = entity->GetType();

                    if ((unit->GetLifeState() == LifeStateUnconscious || u->GetPerson()->NeedsAmbulance()>=0.5) && type->IsAttendant()) 
                      _modeAuto = UIStrategyHeal;
                    else if (unit->GetLifeState() == LifeStateUnconscious) 
                      _modeAuto = UIStrategyHeal;
                  }
                }
              }
            }
            if (_modeAuto == UIStrategy) // not assigned yet
            {
              AIBrain *u = targetAI->CommanderUnit();
              if
                (
                agent->IsGroupLeader() &&
                u && u->GetGroup() &&
                u->GetGroup() == agent->GetGroup()
                )
              { // inside my group / subgroup
                _modeAuto = UIStrategySelect;
              }
              else
              {
                // outside my group
                if (notEmpty)
                  _modeAuto = UIStrategyMove;
              }
            }
          }
          else
          { // enemy / neutral / unknown target
            _modeAuto = UIStrategyAttack;
          }

          if (list.Size() == 1)
          {
            AIUnit *selectedUnit = list[0];
            if(selectedUnit && selectedUnit->IsFreeSoldier() && selectedUnit->GetLifeState() != LifeStateUnconscious && selectedUnit->GetVehicle())
            {
              EntityAIFull *entity = selectedUnit->GetVehicle();
  
              if(entity->CanCarryBackpack() && targetAI->Type()->IsBackpack())
                _modeAuto = UIStrategyTakeBackpack;
            }
          }
        }
      }
    }
    else if (_lockTarget && !notEmpty)
    {
      _modeAuto = UIStrategyFire;
    }
    else
    {
      AIGroup *grp = agent->GetGroup();
      if (grp)
      {
        bool enemyTargeted = list.Size()>0;
        for (int i=0; i<list.Size(); i++)
        {
          AIUnit *u = list[i];
          if (u)
          {
            enemyTargeted = enemyTargeted && grp->TargetSent(u)!=NULL;
          }
        }
        if (enemyTargeted)
        {
          _modeAuto = UIStrategyAttack;
        }
      }
    }
  }
  else if (_target)
  {
    if (_mode == UIFire)
    {
      _modeAuto = UIFirePosLock;
    }
  }

  if (_groundPointValid)
  {
    if (_modeAuto == UIStrategy)
    {
      _modeAuto = UIStrategyMove;
    }
  }

  Menu *curMenu = GetCurrentMenu();
  if(curMenu && curMenu->_items.Size()>0 && curMenu->_visible && 
    curMenu->_selected>=0)
    if((curMenu->IsContexSensitive() || _menuScroll == true) && _modeAuto<=UIFirePosLock)
      _modeAuto =  UIStrategyComplex ; 

  Person *person = agent->GetPerson();

  bool playerRemoteControlled =  (person && person == GWorld->PlayerOn()) 
    ||(GWorld->PlayerOn() ==  GWorld->FocusOn()->GetRemoteControlled() );
  /*if (!agent->IsSpeaking() && !agent->IsListening())*/
  {
    if(_modeAuto<=UIFirePosLock && _actions.IsListVisible() && _actions.FindSelected()>=0) 
      if (!GWorld->GetPlayerSuspended() && GWorld->UserDialog() == NULL && playerRemoteControlled)
        _modeAuto =  UIStrategyAction; 
  }

  if (_mode == UIStrategy)
  {
    Menu *menuCurrent = GetCurrentMenu();
    if (menuCurrent && menuCurrent->IsContexSensitive())
    {
      bool cursorHandled = false; // set when selected menu item and the cursor shape matches
      if (_menuChanged)
      {
        // what command need to be selected
        struct CommandList: public FindArrayKey<int, FindArrayKeyTraits<int>, MemAllocLocal<int,4> >
        {
          CommandList(){}
          CommandList(int x){Add(x);}
          void operator = (int x) {Clear();Add(x);}
          void operator += (int x) {Add(x);}
        };
        CommandList defaultCommand;
        if(!GWorld->UI()->IsHcCommandVisible()) //hc has priorities defined in config 
        {
          switch (_modeAuto)
          {
          case UIStrategySelect:
            defaultCommand = CMD_SELECT_AUTO;
            break;
          case UIStrategyMove:
            defaultCommand = CMD_MOVE_AUTO;
            break;
          case UIStrategyWatch:
            defaultCommand = CMD_WATCH_CTX;
            break;
          case UIStrategyAttack:
          case UIStrategyFire:
            defaultCommand = CMD_ATTACK_AUTO;
            defaultCommand += CMD_ENGAGE_AUTO;
            defaultCommand += CMD_FIRE_AUTO;
            break;
          case UIStrategyFireAtPosition:
            defaultCommand = CMD_FIRE_AT_POSITION_AUTO;
            break;
          case UIStrategyGetIn:
            defaultCommand = CMD_GETIN_AUTO;
            break;
          case UIStrategyGetOut:
            defaultCommand = CMD_GETOUT;
            break;
          case UIStrategyHeal:
            defaultCommand = CMD_HEAL;
            break;
          case UIStrategyRepairvehicle:
            defaultCommand = CMD_REPAIR_VEHICLE;
            break;
          case UIStrategyTakeBackpack:
            defaultCommand = CMD_TAKE_BACKPACK;
            break;
          case UIStrategyAssemble:
            defaultCommand = CMD_ASSEMBLE_WEAPON;
            break;
          case UIStrategyDisassemble:
            defaultCommand = CMD_DISASSEMBLE_WEAPON;
            break;
          }
        }
        // select the command in the menu
        if (defaultCommand.Size()>0)
        {
          for (int i=0; i<menuCurrent->_items.Size(); i++)
          {
            const MenuItem *item = menuCurrent->_items[i];
            if (defaultCommand.FindKey(item->_cmd)>=0 && item->_visible && item->_enable)
            {
              menuCurrent->_selected = i;
              cursorHandled = true;
              break;
            }
          }
        }
        else
        {
          int maxPriority = 0;
          int maxPriorityindex = -1;
          for (int i=0; i<menuCurrent->_items.Size(); i++)
          {
            const MenuItem *item = menuCurrent->_items[i];
            if(item->_priority > maxPriority && item->_enable && item->_visible)
            {
              maxPriority = item->_priority;
              maxPriorityindex =i;
            }
          }
          if(maxPriorityindex >=0)
          {
            menuCurrent->_selected = maxPriorityindex;
            cursorHandled = true;
          }
        }
      }
      // set the cursor shape based on the selected menu item
      if (!cursorHandled)
      {
        _modeAuto = UIStrategy;
        if (menuCurrent->_selected >= 0 && menuCurrent->_selected < menuCurrent->_items.Size())
        {
          MenuItem *item = menuCurrent->_items[menuCurrent->_selected];
          if (item->_visible && item->_enable)
          {
            switch (item->_cmd)
            {
            case CMD_MOVE_AUTO:
              _modeAuto = UIStrategyMove;
              break;
            case CMD_WATCH_CTX:
              _modeAuto = UIStrategyWatch;
              break;
            case CMD_GETIN_AUTO:
              _modeAuto = UIStrategyGetIn;
              break;
            case CMD_GETOUT:
              _modeAuto = UIStrategyGetOut;
              break;
            case CMD_ATTACK_AUTO:
            case CMD_ENGAGE_AUTO:
            case CMD_FIRE_AUTO:
              _modeAuto = UIStrategyAttack;
              break;
            case CMD_FIRE_AT_POSITION_AUTO:
              _modeAuto = UIStrategyFireAtPosition;
              break;
            case CMD_HEAL:
              _modeAuto = UIStrategyHeal;
              break;
            case CMD_REPAIR_VEHICLE:
              _modeAuto = UIStrategyRepairvehicle;
              break;
            case CMD_TAKE_BACKPACK:
              _modeAuto = UIStrategyTakeBackpack;
              break;
            case CMD_ASSEMBLE_WEAPON:
              _modeAuto = UIStrategyAssemble;
              break;
            case CMD_DISASSEMBLE_WEAPON:
              _modeAuto = UIStrategyDisassemble;
              break;
            case CMD_SELECT_AUTO:
            case CMD_DESELECT_AUTO:
            case CMD_HC_SELECT_AUTO:
            case CMD_HC_DESELECT_AUTO:
              _modeAuto = UIStrategySelect;
              break;
            }
          }
        }
      }
    }
  }
}

Menu *InGameUI::PreviewMenu()
{
  int n = _menu.Size();
  Assert(n > 0);
  Menu *currentMenu = _menu[n - 1];
  // FIX: crash
  if (currentMenu->_items.Size() == 0) currentMenu->_selected = -1;

  int i = currentMenu->_selected;
  if (i < 0) return NULL;
  MenuItem *item = currentMenu->_items[i];

  if (item->_submenu.GetLength() == 0) return NULL;

  Menu *menu = new Menu();
  const char *ptr = item->_submenu;
  if (*ptr == '#')
  {
    menu->_text = item->_text;
    CreateDynamicMenu(menu, ptr + 1);
  }
  else
    menu->Load(item->_submenu, MenuAttributeTypes, MenuConditionVariables, lenof(MenuConditionVariables));
  CheckMenuItems(menu);
  menu->_selected = -1;
  return menu;
}

/// some actions only can be performed when player have control over camera vehicle
static bool IsManual( EntityAI *veh, AIBrain *unit )
{
  if (!unit->IsPlayer())
  {
    // either unit is player or remote controlled by a player
    Person *controlledBy = unit->GetRemoteControlled();
    if (!controlledBy || controlledBy != GWorld->PlayerOn()) return false;
  }

  if( !GWorld->PlayerManual() ) return false;
  if( GWorld->GetPlayerSuspended() ) return false;
  return true;
}

void InGameUI::RevealTarget( Target *target, float spot )
{
  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return;
  EntityAI *vehicle = agent->GetVehicle();
  if (!vehicle) return;

  if (target->IsMinimal()) return;
  
  TargetNormal *tgt = static_cast<TargetNormal *>(target);
  if
  (
    !tgt->IsKnownNoDelay() ||
    tgt->delay>Glob.time+1 ||
    tgt->FadingSpotability()<spot
  )
  {
    // report only targets that are not known

    const SideInfo *side = agent->GetSideInfo();
    if (side)
    {
      TargetList *list = agent->AccessTargetList();
      tgt->Reveal(*side,list,1.5f);

      if (tgt->delay>Glob.time+1) tgt->delay=Glob.time+1;
      tgt->delaySensor=Glob.time-5;
      tgt->idSensor = agent;
      if (agent && agent->GetUnit())
      {
        tgt->_idSeen.Set(agent->GetUnit()->ID(),true);
      }
      tgt->lastSeen = Glob.time;
      // use current properties
      EntityAI *exact=tgt->idExact;
      tgt->position=exact->AimingPosition(exact->FutureVisualState());
      tgt->speed=exact->FutureVisualState().Speed();
      tgt->LimitError(0.1f,0.1f);
      if( tgt->FadingSpotability()<spot )
      {
        tgt->spotability=spot;
        tgt->spotabilityTime=Glob.time;
      }
    }
  }
  // if it is dead body of my group unit, report it
  EntityAI *obj = tgt->idExact;
  AIGroup *grp = agent->GetGroup();
  if (obj && grp)
  {
    // TODO: report all our units that are in the vehicle
    AIBrain *tgtUnit = obj->CommanderUnit();
    if
    (
      tgtUnit && !tgtUnit->LSIsAlive()
      && tgtUnit->GetGroup() == grp
    )
    {
      Assert(agent->GetUnit());
      Assert(tgtUnit->GetUnit());
      grp->SendUnitDown(agent->GetUnit(), tgtUnit->GetUnit());
    }
    else if (obj->IsDamageDestroyed())
    {
      // check taregt side
      if (grp->GetCenter()->IsEnemy(obj->Vehicle::GetTargetSide()))
      {
        // if it is dead enemy, you may report it
        // but only when I killed him
        if
        (
          (tgt->idKiller == vehicle || tgt->idKiller == agent->GetPerson()) &&
          tgt->timeReported<=TIME_MIN
        )
        {
          if (grp->UnitsCount()>1)
          {
            // send radio message
            Assert(agent->GetUnit());
            grp->SendObjectDestroyed(agent->GetUnit(), tgt->type);
          }
          // mark as reported
          tgt->timeReported = Glob.time;
          tgt->posReported = tgt->position;
        }
      }
    }
  }
}

/**
@param itPos position of the collision with the target (world coordinates)
 or the nearest point on the line camera-cursor to the target
*/

Target *InGameUI::CheckCursorTarget(
  Vector3 &itPos, Vector3Par cursorDir,
  const Camera &camera, CameraType cam, bool knownOnly, bool includeBuildings
)
{
  _cursorObject = NULL;
  
  if (_lockTarget)
  {
    // use locked target even as a "under cursor" one
    _groundPoint = Vector3(0,-10,0);
    _cursorObject = _lockTarget->idExact.GetLink();
    _cursorObjectPos = _lockTarget->AimingPosition();
    return NULL;
  }

  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return NULL;
  EntityAI *vehicle = agent->GetVehicle();

  float maxVisDist=floatMax(vehicle->GetType()->GetIRScanRange(),TACTICAL_VISIBILITY);

  CollisionBuffer retVal;
  // view test is not available for soldiers - use fire as a secondary test
  GLandscape->ObjectCollisionLine(
    vehicle->GetRenderVisualStateAge(),retVal,Landscape::FilterIgnoreOne(vehicle),
    camera.Position(),camera.Position()+cursorDir*maxVisDist,0,
    ObjIntersectView,ObjIntersectFire
  );
  // select first intersected object
  int minI = -1;
  float minT = 1e10;
  for (int i=0; i<retVal.Size(); i++)
  {
    const CollisionInfo &info = retVal[i];
    if (!info.object) continue;
    if (info.under<minT)
    {
      minT = info.under;
      minI = i;
    }
  }
  Target *iTarget = NULL;

  itPos = camera.Position();

  #if 1
  if (minI>=0)
  { 
    // check intersection with land
    const CollisionInfo &info = retVal[minI];

    // if we are on the proxy, it would be nice to have the underlying object information
    Object *obj = info.parentObject;
    if (!obj) obj = info.object;
    
    _cursorObject = obj;
    _cursorObjectPos = info.pos;
    
    EntityAI *ai = dyn_cast<EntityAI,Object>(obj);
    if (ai)
    {
      Vector3 iPos = info.pos;
      Vector3 iDir = iPos-camera.Position();
      if (iDir.SquareSize() > Square(0.001f))
      {
        Vector3 iDirN = iDir.Normalized();
        float iDirSize = iDir*iDirN;
        Vector3 lPos;
        float isect = GLandscape->IntersectWithGround(
          &lPos,camera.Position(),iDirN,0,iDirSize*1.1);
        // we know what is 
        if (isect>iDirSize)
        {
          // no ground obstacle
          // check if not too deep in sea
          isect = GLandscape->IntersectWithGroundOrSea(
            &lPos,camera.Position(),iDirN,0,iDirSize*1.1);
          if (iDirSize - isect < 1.0f)
          {
            // check if we know the target
            if (knownOnly)
            {
              iTarget = agent->FindTarget(ai,includeBuildings);
            }
            else
            {
              iTarget = agent->FindTargetAll(ai,includeBuildings);
            }
            if (iTarget) itPos = iPos;
          }
          //GScene->DrawCollisionStar(iPos,1,PackedColor(Color(0,1,0)));
        }
        else
        {
          // ground occluding
          //GScene->DrawCollisionStar(lPos,0.5,PackedColor(Color(1,0,0)));
        }
      }
    }
  }

  // if we are aiming at something directly, return it
  if (iTarget) return iTarget;
  #endif

  // if not, we should use cursor range look-up

  float distNearest = 1e10;
  const TargetList *visibleList = VisibleList();
  if (visibleList)
  {
    int i,n = visibleList->AnyCount(); 
    for (i=0; i<n; i++)
    {
      TargetNormal *tar = visibleList->GetAny(i);

      TargetType *veh = tar->idExact;
      if( !veh || veh->Invisible() ) continue; // invisible

      if
        (
        tar->vanished
#if _ENABLE_CHEATS
        && !_showAll
#endif
        ) continue; // invisible

      if ( vehicle==veh )
      {
        //player is allways under cursor in 3rd person view
        continue;
        /*
        if (_mode != UIStrategy)
          continue;
        if (GWorld->GetCameraType() < CamExternal)
          continue;
        // avoid yourself if subgroup command is not possible
        if (IsEmptySelectedUnits()) continue;
        */
      }

      // static (mostly large) objects do not use range look-up
      if (veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeStatic)))
      {
        continue;
      }

      if (knownOnly)
      {
        if
          (
          !tar->IsKnownBy(agent)
#if _ENABLE_CHEATS
          && !_showAll
#endif
          ) continue;
      }
      else
      {
        // visible only
        // check distance to target
        float irRange = vehicle->GetType()->GetIRScanRange();
        if (!veh || !veh->GetType()->GetIRTarget()) irRange = 0;
        float maxDistance=floatMax(irRange,TACTICAL_VISIBILITY);

        float dist2 = veh->FutureVisualState().Position().Distance2(vehicle->FutureVisualState().Position());
        if (dist2>Square(maxDistance)) continue;
        // check LOS to target
        float vis = GLandscape->Visible(vehicle,veh);
        if (vis<0.25) continue;

      }



#if _ENABLE_CHEATS
      bool exact = _showAll || !knownOnly;
#else
      bool exact = !knownOnly;
#endif

      if (!exact)
      {
        // in some situations we want to use the exact information about the object
        // check if we think we should know the accurate information
        if (tar->FadingPosAccuracy()<1)
        {
          // check if the information really is accurate
          if (tar->AimingPosition().Distance2(tar->idExact->AimingPosition(tar->idExact->FutureVisualState()))<1)
          {
            exact = true;
          }
        }
      }

      Vector3 cDir = cursorDir.Normalized();

      Vector3 nearP = tar->AimingPosition();
      // distance in the z direction
      float distance = (nearP-camera.Position()).DotProduct(cDir);
      float maxDist = 0.03f * camera.Left()*distance;
      float distToObj = FLT_MAX;
      if (!exact)
      { // exact information not available, we need to work with the inexact one
        Vector3Val pos = tar->AimingPosition();
        float bounding = veh->GetShape()->GeometrySphere();

        Vector3Val dir = pos-camera.Position();

        // check nearest point on line camPos,cursorDir to target aiming position
        float nearestT = cDir*dir;
        saturateMax(nearestT,0);
        nearP = camera.Position()+cDir*nearestT;

        maxDist += bounding;

        distToObj = nearP.Distance(pos);
      }
      else
      {
        Vector3 minMax[2];
        Vector3 bCenter;
        float bSphere = tar->idExact->BoundingInfoUI(bCenter,minMax);

        Vector3 pos = tar->idExact->RenderVisualState().PositionModelToWorld(bCenter);

        Vector3Val dir = pos-camera.Position();

        // check nearest point on line camPos,cursorDir to target aiming position
        Vector3 cDir = cursorDir.Normalized();
        float nearestT = cDir*dir;
        saturateMax(nearestT,0);
        nearP = camera.Position()+cDir*nearestT;

        maxDist += bSphere;
        // TODO: check distance from bounding box
        distToObj = nearP.Distance(pos);
      }

      // check if object is within cursor range
      if (distToObj<maxDist)
      {
        //GScene->DrawCollisionStar(nearP);
        if (distToObj<distNearest)
        {
          distNearest = distToObj;
          iTarget = tar;
          itPos = nearP;
        }
        /*
        GScene->DrawDiagModel(
        pos,GScene->Preloaded(SphereModel),
        bounding,PackedColor(Color(1,1,1,0.5))
        );
        */
      }


    }

  }
  
  // if there is no object detected directly, use a target if possible
  if (!_cursorObject && iTarget)
  {
    _cursorObject = iTarget->idExact.GetLink();
    _cursorObjectPos = iTarget->AimingPosition(); // ????
  }
  return iTarget;
}

void InGameUI::SimulateHUDNonAI
(
  const Camera &camera, Entity *vehicle, CameraType cam, float deltaT
)
{
  _modeAuto = UIFire;
  _mode = UIFire;
}

bool InGameUI::IsCommandingMode() const
{
  return (_menu.Size() > 0 || _units);
}

bool InGameUI::IsLMBHandled() const
{
  const Menu *menuCurrent = GetCurrentMenu();
  return (menuCurrent && (menuCurrent->IsContexSensitive() || _menuScroll));
}

bool InGameUI::IsCompassShown() const
{
  AIBrain *unit = GWorld->FocusOn();
  if (!unit) return false;
  Transport *vehicle = unit->GetVehicleIn();
  if (!vehicle) return false;

  int canSee = 0;
  if (unit == vehicle->ObserverUnit()) canSee = vehicle->Type()->_commanderCanSee;
  else if (unit == vehicle->PilotUnit()) canSee = vehicle->Type()->_driverCanSee;
  else if (unit == vehicle->GunnerUnit()) canSee = vehicle->Type()->_gunnerCanSee;

  return (canSee & CanSeeCompass) != 0;
}

void InGameUI::RevealCursorTarget()
{
  Vector3 pos;
  Vector3 cursorDir = GetCursorDirection();
  const Camera &camera = *GScene->GetCamera();
  CameraType cam = GWorld->GetCameraType();
  Target *target = CheckCursorTarget(
    pos, cursorDir, camera, cam, false, false
  );

  if (target) RevealTarget(target, 0.3);
}

Vector3 InGameUI::GetCamGroupCameraPos() const
{
  Vector3 pos;
  Matrix3 camOrient = Matrix3(MRotationY,_camGroupHeading)*Matrix3(MRotationX,_camGroupDive);
  pos = camOrient*VForward*_camGroupZoom;
  return pos;
}

void InGameUI::InitCamGroupCameraPos()
{
  //position camera behind the vehicle
  _camGroupDive=-0.5f; _camGroupZoom=1;
  Object *camOn = GWorld->CameraOn();
  if (camOn)
  {
    Vector3 direct = camOn->FutureVisualState().Transform().Orientation().Direction();
    float angle = atan2(direct.Z(), direct.X());
    _camGroupHeading=angle+H_PI/2;
  }
  else _camGroupHeading=H_PI;
}

void InGameUI::CamGroupActions(float deltaT)
{
  if (GWorld->GetCameraType()==CamGroup)
  {
    //Update camera position
    //Get UALookXXX actions (Keyboard)
    _camGroupDiveOld = _camGroupDive;
    const float diag = 0.5 * H_SQRT2;
    float headSpeed = (
      GInput.GetActionExclusive(UALookLeft)
      + GInput.GetActionExclusive(UALookLeftUp)*diag
      + GInput.GetActionExclusive(UALookLeftDown)*diag
      - GInput.GetActionExclusive(UALookRight)
      - GInput.GetActionExclusive(UALookRightUp)*diag
      - GInput.GetActionExclusive(UALookRightDown)*diag
      );
    float heading = deltaT*headSpeed;
    float diveSpeed = (
      GInput.GetActionExclusive(UALookDown)
      + GInput.GetActionExclusive(UALookLeftDown)*diag
      + GInput.GetActionExclusive(UALookRightDown)*diag
      - GInput.GetActionExclusive(UALookUp)
      - GInput.GetActionExclusive(UALookRightUp)*diag
      - GInput.GetActionExclusive(UALookLeftUp)*diag
      );
    float dive = -deltaT*diveSpeed;
    _camGroupHeading += heading;
    _camGroupDive += dive;
    //Get zoom actions
    float expChange=GInput.GetActionExclusive(UAZoomOut)-GInput.GetActionExclusive(UAZoomIn);
    if( expChange )
    {
      const float ZoomSpeed=8;
      float change=pow(ZoomSpeed,expChange*deltaT);
      _camGroupZoom*=change;
      saturate(_camGroupZoom,0.25,4);
    }
    //Get cursor movement actions (Mouse)
    ValueWithCurve aimX(0, NULL), aimY(0, NULL);
    aimX = GInput.GetActionWithCurveExclusive(UAAimRight, UAAimLeft)*Input::MouseRangeFactor;
    aimY = GInput.GetActionWithCurveExclusive(UAAimUp, UAAimDown)*Input::MouseRangeFactor;
    float aimXVal = aimX.GetValue();
    float aimYVal = aimY.GetValue();
    float moveX = (aimXVal/GInput.GetMouseSensitivityMultX())*deltaT*(1000/200.0f);
    float moveY = (aimYVal/GInput.GetMouseSensitivityMultY())*deltaT*(1000/150.0f);
    static float sensitivityX=1.0f;
    static float sensitivityY=1.0f;
    _camGroupCursor.x += moveX*sensitivityX;
    _camGroupCursor.y += moveY*sensitivityY;
  }
}

// TODO: make a member of EntityAIFull, similar to EntityAIFull::CanFireUsing
static bool CanTargetUsing(EntityAIFull *vehicle,const TurretContext &context)
{
  // if the weapon has no locking capabilities, we cannot target
  // prototype: assume commander has no lockable weapons
  int weapon = context._weapons->ValidatedCurrentWeapon();
  if (weapon<0) return false;

  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const Magazine *magazine = slot._magazine;
  const AmmoType *ammo = NULL;
  if (magazine)
  {
    const MagazineType *type = magazine->_type;
    if (type) ammo = type->_ammo;
  }

  return (
    slot._muzzle->_canBeLocked == 2 ||
    slot._muzzle->_canBeLocked == 1 && Glob.config.IsEnabled(DTAutoGuideAT)
  );
}

bool InGameUI::ProcessTeamSwitch()
{
  AIBrain *agent = GWorld->FocusOn();
  if (!agent || !agent->LSIsAlive()) return false;
  Person *person = agent->GetPerson();

  bool prev = GInput.GetActionToDo(UATeamSwitchPrev);
  bool next = GInput.GetActionToDo(UATeamSwitchNext);

  if ((prev || next) && GWorld->IsTeamSwitchEnabled())
  {
    // collect the units
    bool multiplayer = GWorld->GetMode() == GModeNetware;
    OLinkPermNOArray(AIBrain) netUnits;
    if (multiplayer) GetNetworkManager().GetSwitchableUnits(netUnits, agent);
    const OLinkPermNOArray(AIBrain) &units = multiplayer ? netUnits : GWorld->GetSwitchableUnits();

    // check if free positions exist
    int count = 0;
    int index = 0;
    for (int i=0; i<units.Size(); i++)
    {
      AIBrain *unit = units[i];
      if (!unit || !unit->LSIsAlive()) continue;
      if (!unit->GetPerson()) continue;
      // position found
      if (unit->GetPerson() == person) index = i;
      else count++;
    }
    if (count > 0)
    {
      // find where to switch
      int newIndex = -1;
      if (prev)
      {
        for (int i=index-1; i>=0; i--)
        {
          AIBrain *unit = units[i];
          if (!unit || !unit->LSIsAlive()) continue;
          if (unit->GetPerson() && unit->GetPerson() != person)
          {
            // position found
            newIndex = i; break;
          }
        }
        if (newIndex < 0) for (int i=units.Size()-1; i>index; i--)
        {
          AIBrain *unit = units[i];
          if (!unit || !unit->LSIsAlive()) continue;
          if (unit->GetPerson() && unit->GetPerson() != person)
          {
            // position found
            newIndex = i; break;
          }
        }
      }
      else
      {
        for (int i=index+1; i<units.Size(); i++)
        {
          AIBrain *unit = units[i];
          if (!unit || !unit->LSIsAlive()) continue;
          if (unit->GetPerson() && unit->GetPerson() != person)
          {
            // position found
            newIndex = i; break;
          }
        }
        if (newIndex < 0) for (int i=0; i<index; i++)
        {
          AIBrain *unit = units[i];
          if (!unit || !unit->LSIsAlive()) continue;
          if (unit->GetPerson() && unit->GetPerson() != person)
          {
            // position found
            newIndex = i; break;
          }
        }
      }
      // switch to the new unit
      Assert(newIndex>=0);
      void ProcessTeamSwitch(Person *newPlayer, Person *oldPlayer, EntityAI *killer, bool respawn);
      if(newIndex>=0) ProcessTeamSwitch(units[newIndex]->GetPerson(), person, NULL, false);
      GWorld->ShowMap(false);
    }
  }

  if(GInput.GetActionToDo(UATeamSwitch,true) && GWorld->IsTeamSwitchEnabled())
  {
    AbstractOptionsUI *CreateTeamSwitchDialog(Person *player, EntityAI *killer, bool respawn, bool userDialog);
    GWorld->DestroyUserDialog();
    GWorld->SetUserDialog(CreateTeamSwitchDialog(agent->GetPerson(), NULL, false, true));
  }

  return true;
}

extern GameValue GGroupMarkerClicked;
void InGameUI::OnGroupMarkerClick(AIGroup *group,int RMB, float x, float y, int waypoint)
{
  bool issueCommand = true;

  bool eventHandlerValid = !GGroupMarkerClicked.GetNil();
  if (eventHandlerValid)
  {
#if USE_PRECOMPILATION
    if (GGroupMarkerClicked.GetType() == GameCode)
    {
      GameDataCode *code = static_cast<GameDataCode *>(GGroupMarkerClicked.GetData());
      eventHandlerValid = code->IsCompiled() && code->GetCode().Size() > 0;
    }
    else
#endif
    {
      RString code = GGroupMarkerClicked;
      eventHandlerValid = code.GetLength() > 0;
    }
  }

  if (eventHandlerValid)
  {
    GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
    GameArrayType &arguments = value;

    arguments.Add(true);
    arguments.Add(GameValueExt(group));
    arguments.Add((float)waypoint);
    arguments.Add(float(RMB));
    arguments.Add(x);
    arguments.Add(y);

    arguments.Add((GInput.keyPressed[DIK_LSHIFT]||GInput.keyPressed[DIK_RSHIFT]));
    arguments.Add((GInput.keyPressed[DIK_LCONTROL]||GInput.keyPressed[DIK_RCONTROL]));
    arguments.Add((GInput.keyPressed[DIK_LALT]||GInput.keyPressed[DIK_RALT]));

    // Vector3 pos = ScreenToWorld(DrawCoord(x, y));
    GameState *state = GWorld->GetGameState();
    GameVarSpace vars(false);
    state->BeginContext(&vars);
    //selected group
    state->VarSetLocal("_this",value,true); 

#if USE_PRECOMPILATION
    if (GGroupMarkerClicked.GetType() == GameCode)
    {
      GameDataCode *code = static_cast<GameDataCode *>(GGroupMarkerClicked.GetData());
      issueCommand = !state->EvaluateBool(code->GetString(), code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    }
    else
#endif
    {
      // make sure string is not destructed while being evaluated
      RString code = GGroupMarkerClicked;
      issueCommand = !state->EvaluateMultipleBool(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    }
    state->EndContext();
  }
}

extern GameValue GGroupMarkerEnter;
extern GameValue GGroupMarkerLeave;
//perform scrip action when unit selection has changed
void InGameUI::OnGroupMarkerEvent(AIGroup *group, float x, float y, GameValue GGroupMarkerEvent, int waypoint)
{
  bool issueCommand = true;

  bool eventHandlerValid = !GGroupMarkerEvent.GetNil();
  if (eventHandlerValid)
  {
#if USE_PRECOMPILATION
    if (GGroupMarkerEvent.GetType() == GameCode)
    {
      GameDataCode *code = static_cast<GameDataCode *>(GGroupMarkerEvent.GetData());
      eventHandlerValid = code->IsCompiled() && code->GetCode().Size() > 0;
    }
    else
#endif
    {
      RString code = GGroupMarkerEvent;
      eventHandlerValid = code.GetLength() > 0;
    }
  }

  if (eventHandlerValid)
  {
    GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
    GameArrayType &arguments = value;

    arguments.Add(true);
    arguments.Add(GameValueExt(group));
    arguments.Add((float)waypoint);
    arguments.Add(x);
    arguments.Add(y);

    arguments.Add((GInput.keyPressed[DIK_LSHIFT]||GInput.keyPressed[DIK_RSHIFT]));
    arguments.Add((GInput.keyPressed[DIK_LCONTROL]||GInput.keyPressed[DIK_RCONTROL]));
    arguments.Add((GInput.keyPressed[DIK_LALT]||GInput.keyPressed[DIK_RALT]));

    // Vector3 pos = ScreenToWorld(DrawCoord(x, y));
    GameState *state = GWorld->GetGameState();
    GameVarSpace vars(false);
    state->BeginContext(&vars);
    //selected group
    state->VarSetLocal("_this",value,true); 

#if USE_PRECOMPILATION
    if (GGroupMarkerEvent.GetType() == GameCode)
    {
      GameDataCode *code = static_cast<GameDataCode *>(GGroupMarkerEvent.GetData());
      issueCommand = !state->EvaluateBool(code->GetString(), code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    }
    else
#endif
    {
      // make sure string is not destructed while being evaluated
      RString code = GGroupMarkerEvent;
      issueCommand = !state->EvaluateMultipleBool(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    }
    state->EndContext();
  }
}

/*!
\patch 1.32 Date 11/26/2001 by Jirka
- Fixed: Disable action menu in cutscenes
\patch 5117 Date 1/15/2007 by Bebul
- Changed: Command Fire in vehicles works also when commanding mode is not active
\patch 5120 Date 1/18/2007 by Bebul
- Fixed: Locking target by RMB in tank does not activate optics action
\patch 5120 Date 1/19/2007 by Ondra
- Fixed: Commander can use Tab to issue target orders when sitting in the weapon station
  as long as his own weapons are not lockable.
\patch 5126 Date 1/31/2007 by Jirka
- Fixed: MP - manual fire rate was too high sometimes
\patch 5137 Date 3/5/2007 by Jirka
- Fixed: In-game UI: RMB now sends target also to the units selected in the menu (when SPACE is holding)
\patch 5161 Date 5/29/2007 by Jirka
- Fixed: Team switch shortcuts (previous / next unit) accessible also when map is shown
*/

void InGameUI::SimulateHUD
(
  const Camera &camera, CameraType cam, float deltaT
)
{
  UpdateWeaponCursorScaleCoef(deltaT);

  // Handle team switch first (to work on the correct person then)
  if (!ProcessTeamSwitch()) return;

  // Cheats
#if _ENABLE_CHEATS
  if( GInput.GetCheat1ToDo(DIK_A))
  {
    _showAll=!_showAll;
    GlobalShowMessage(500,"ShowAll %s",_showAll ? "On":"Off");
  }

  // show / hide status displays
  if (GInput.GetCheat1ToDo(DIK_SEMICOLON))
  {
    bool show = !_showUnitInfo;
    ShowAll(show);
    GChatList.Enable(show);
  }

  if (GInput.GetCheat1ToDo(DIK_APOSTROPHE))
  {
    ShowCursors(!_showCursors);
  }
#endif
#ifdef _XBOX
  if (GInput.GetCheatXToDo(CXTShowUI))
  {
    bool show = !_showCursors;
    ShowCursors(show);
    ShowAll(show);
    GChatList.Enable(show);
  }
  if (GInput.cheatX) return; // cheats have higher priority
#endif

  if (GInput.GetActionToDo(UAHelp))
  {
    ShowLastHint();
  }

  AIBrain *agent = GWorld->FocusOn();
  if (!agent || !agent->LSIsAlive()) return;
  Person *person = agent->GetPerson();
  // some actions (commanding or actions menu, group info) can be performed only by a player himself
  bool playerControlled = (person && person == GWorld->PlayerOn());

  bool playerRemoteControlled =  (person && person == GWorld->PlayerOn()) 
    ||(GWorld->PlayerOn() ==  GWorld->FocusOn()->GetRemoteControlled() );

  EntityAIFull *vehicle = agent->GetVehicle();

#if _VBS2 //convoy trainer
  if(agent->GetPerson()->IsPersonalItemsEnabled())
    vehicle = agent->GetPerson();  
#endif

  Assert(vehicle);
  if (!vehicle) return;

  //Commanding camera
  if (GWorld->GetCameraType()==CamGroup)
  {
    //CamGroupActions called before SimulateAllVehicles
    //Update cursor position
    //Get UAAimXXX actions (Mouse)
    float cgCurRange = 0.75f;
    Point2DFloat cgCurPos = _camGroupCursor;
    saturate(_camGroupCursor.x, -cgCurRange, cgCurRange);
    saturate(_camGroupCursor.y, -cgCurRange, cgCurRange);
    Vector3 newPos(_camGroupCursor.x * camera.Left(), _camGroupCursor.y * camera.Top(), 1.0f);
    Vector3 newDir = camera.Transform().Rotate(newPos);
    newDir.Normalize();
    SetCursorDirection(newDir);
    //rotate camera view, if the cursor wanted to leave the camGroup range
    static float rotSensitivityX=0.5f;
    static float rotSensitivityY=0.5f;
    Point2DFloat cgRotCam = cgCurPos-_camGroupCursor;
    _camGroupHeading -= cgRotCam.x*rotSensitivityX;
    _camGroupDive += cgRotCam.y*rotSensitivityY;
    if (_camGroupDiveOld != _camGroupDive )
    { //there was some dive camera movement, ground saturation needed
      OLinkObject cameraVehicle = GWorld->CameraOn();
      if (cameraVehicle)
      {
        float dist = cameraVehicle->OutsideCameraDistance(CamGroup);
        Vector3 cgPos = GetCamGroupCameraPos();
        cgPos = cgPos * dist;
        Matrix3 vehOrient;
        Vector3 focPos = cameraVehicle->CameraPosition();
        Vector3 cgPosWorld = focPos+cgPos;
        //test whether cgPosWorld is under the ground
        float minCamY = GScene->GetLandscape()->SurfaceYAboveWater(cgPosWorld.X(),cgPosWorld.Z());
        minCamY += 0.1f;
        if( cgPosWorld.Y()<minCamY ) 
        {
          //atan of triangle a,b,c
          float a = minCamY - cgPosWorld.Y();
          cgPosWorld[1]=minCamY;
          float b = cgPosWorld.Distance(focPos);
          float diveChange = atan2f(a,b);
          _camGroupDive -= diveChange;
        }
      }
    }
    saturateMax(_camGroupDive, -0.8*H_PI/2); //saturation looking from the top down to the group (cannot be at the north pole)
  }

  if (GWorld->HasMap())
  {
    // menu is now using info what we are pointing at
    // when map is shown, take this info from the map
    TargetType *target = GWorld->Map()->GetTarget();
    _target = NULL;
    if (target)
    {
      const TargetList *list = VisibleList();
      if (list) _target = unconst_cast(list->FindTarget(target));
    }
    _housePos = -1;
    _cover = NULL;
    _groundPointValid = GWorld->Map()->GetPosition(_groundPoint);
    if (_groundPointValid) _groundPointDistance = _groundPoint.Distance(vehicle->RenderVisualState().Position());

    _hcTarget = GWorld->Map()->GetHCTarget();
  }

  //find HC group under cursor
  if(_drawGroupIcons3D && _selectableGroupIcons && !GWorld->HasMap())
  {
    _hcTarget = NULL;
    Vector3 cursorDir = GetCursorDirection();
    Point2DFloat point =  WorldToScreen(camera,cursorDir,1);

    Point3 a = Point3(point.x,0,point.y);
    float isOver = false;

    for (int i=0; i<_groupMarkersPosition.Size();i++)
    {
      Point3 b = Point3(_groupMarkersPosition[i]._x,0,_groupMarkersPosition[i]._y);
      float dist = (a - b).SquareSizeXZ();

      if(dist < _groupMarkersPosition[i]._radius && _groupMarkersPosition[i]._group)
      {
        isOver = true;
        AIGroup *group = _groupMarkersPosition[i]._group;
        if(_groupMarkersPosition[i]._waipoint<0) _hcTarget = group;
        if(_lastGroupOver == _groupMarkersPosition[i]._group)       
          OnGroupMarkerEvent(_groupMarkersPosition[i]._group,_groupMarkersPosition[i]._x,_groupMarkersPosition[i]._y,GGroupMarkerEnter,_groupMarkersPosition[i]._waipoint);
        else
        {
          if(_lastGroupOver) OnGroupMarkerEvent(_lastGroupOver,a.X(),a.Y(),GGroupMarkerLeave,-1);
          OnGroupMarkerEvent(_groupMarkersPosition[i]._group,_groupMarkersPosition[i]._x,_groupMarkersPosition[i]._y,GGroupMarkerEnter,_groupMarkersPosition[i]._waipoint);
          _lastGroupOver = _groupMarkersPosition[i]._group;
        }

        if (GInput.GetActionDo(UADefaultAction))
        {
          if(!_hcSelected)
          {
            _hcSelected = true;
            OnGroupMarkerClick(_groupMarkersPosition[i]._group,false,_groupMarkersPosition[i]._x,_groupMarkersPosition[i]._y,_groupMarkersPosition[i]._waipoint);
          }
        }
        else _hcSelected = false;
        break;
      }
    }
    if(!isOver && _lastGroupOver)
    {
      OnGroupMarkerEvent(_lastGroupOver,a.X(),a.Y(),GGroupMarkerLeave,-1);
      _lastGroupOver = NULL;
    }
  }

  //////////////////////////////////////////////////////////////////////////
  // Handle menu

  if (playerControlled)
  {
    ProcessMenu(cam);
    _tmTime = Glob.uiTime;

#if _ENABLE_SPEECH_RECOGNITION
    Menu *menuCurrent = GetCurrentMenu();
    RString vocabulary;
    if (menuCurrent) vocabulary = menuCurrent->_vocabulary;
    if (vocabulary.GetLength() > 0)
    {
      _capture.Start();
      if (stricmp(vocabulary, _vocabulary) != 0)
      {
        _communicator.Initialize(vocabulary);
        _vocabulary = vocabulary;
      }
    }
    else
    {
      _capture.Stop();
      _communicator.Done();
      _vocabulary = RString();
    }
#endif
  }

  TurretContextV myContext;
#if _VBS3 //commander override
  bool myContextValid = vehicle->FindControlledTurret(unconst_cast(vehicle->RenderVisualState()), agent->GetPerson(), myContext);
#else
  bool myContextValid = vehicle->FindTurret(unconst_cast(vehicle->RenderVisualState()), agent->GetPerson(), myContext);
#endif

  // show / hide tank direction
  CameraType type = GWorld->GetCameraType();
  const VehicleType *vehType = vehicle->GetType();
  _tankPos = (vehType->HasObserver() && vehicle->IsTurret(type)) ? 0 : 1;

  if (GWorld->HasMap())
  {
    // focus may changed !!!
    agent = GWorld->FocusOn();
    if (!agent) return;
    vehicle = agent->GetVehicle();

#if _VBS2 //convoy trainer
    if(agent->GetPerson()->IsPersonalItemsEnabled())
      vehicle = agent->GetPerson();  
#endif

    Assert(vehicle);
    if (!vehicle) return;

    bool isCommander = agent == vehicle->CommanderUnit();
    bool isPilot = agent == vehicle->PilotUnit();

    Transport *transport = agent->GetVehicleIn();

    // bool isGunnerFire = agent == vehicle->GunnerUnit();
    // can player fire as a gunner? if not, we can use UAFire for commanding
    bool isGunnerFire = myContextValid && vehicle->CanFireUsing(myContext);

    if (transport && transport->IsManualFire() && isCommander)
    {
      // when manual fire is on, commander is controlling primary turret (even when has own weapons)
      isGunnerFire = false;
    }

    // bool vehicleCommander = (transport && transport->Commander() == agent->GetPerson());
    bool vehicleCommander = (transport && myContextValid && isCommander && !isGunnerFire && !isPilot);
    if(GetCurrentMenu()) _mode = UIStrategy;

    SelectPriorCommand(agent, vehicleCommander);
    return;
  }
  //////////////////////////////////////////////////////////////////////////
  // Handle actions

  if (playerRemoteControlled && !GWorld->Chat() && !GWorld->GetPlayerSuspended()) ProcessActions(GWorld->FocusOn());

  // focus may changed !!!
  agent = GWorld->FocusOn();
  if (!agent) return;
  person = agent->GetPerson();

  playerControlled = (person && person == GWorld->PlayerOn()) 
    ||(GWorld->PlayerOn() ==  GWorld->FocusOn()->GetRemoteControlled() );

  vehicle = agent->GetVehicle();

#if _VBS2 //convoy trainer
  if(agent->GetPerson()->IsPersonalItemsEnabled())
    vehicle = agent->GetPerson();  
#endif

  Assert(vehicle);
  if (!vehicle) return;

  bool isCommander = agent == vehicle->CommanderUnit();
  bool isObserver = agent == vehicle->EffectiveObserverUnit();
  bool isPilot = agent == vehicle->PilotUnit();

  Transport *transport = agent->GetVehicleIn();

  // bool isGunnerFire = agent == vehicle->GunnerUnit();
  // can player fire as a gunner? if not, we can use UAFire for commanding
  bool isGunnerFire = myContextValid && vehicle->CanFireUsing(myContext);

  if (transport && transport->IsManualFire() && isCommander)
  {
    // when manual fire is on, commander is controlling primary turret (even when has own weapons)
    isGunnerFire = false;
  }

  // can player target as a gunner? if not, we can use cursor and UALockTarget(s) for commanding
  // note: if player cannot fire, he cannot target as well
  // isGunnerFire implies myContextValid
  bool isGunnerTarget = isGunnerFire && CanTargetUsing(vehicle,myContext);

  TurretContextV fireContext = myContext;
  TurretContextV targetContext = myContext;
  // based on isGunnerFire and isGunnerTarget we can overwrite contexts with the primary turret ones
  // this context can be used for the commander to issue orders to the turret
  bool fireContextValid = isGunnerFire || vehicle->GetPrimaryGunnerTurret(unconst_cast(vehicle->RenderVisualState()), fireContext);
  bool targetContextValid = isGunnerTarget || vehicle->GetPrimaryGunnerTurret(unconst_cast(vehicle->RenderVisualState()), targetContext);

  // bool vehicleCommander = (transport && transport->Commander() == agent->GetPerson());
  bool vehicleCommander = (transport && myContextValid && isCommander && !isGunnerFire && !isPilot);
  
  AIGroup *group = agent->GetGroup();
  bool groupCommander = group && group->Leader() == agent && group->UnitsCount() > 1;

  //////////////////////////////////////////////////////////////////////////
  // Select cursor type (fire / strategy)

  OLinkPermNOArray(AIUnit) list;
  ListSelectingUnits(list);

  if (_xboxStyle)
  {
    if (groupCommander && list.Size() > 0)
      SwitchToStrategy(vehicle);
    else if(_drawHCCommand && SelectHCGroupsCount(agent->GetUnit())>0)
      SwitchToStrategy(vehicle);
    else if (vehicleCommander)
      SwitchToStrategy(vehicle);
    else
      SwitchToFire(vehicle);
  }
  else
  {
    if (!groupCommander && !vehicleCommander && !_drawHCCommand)
    {
      int n = _menu.Size();
      Menu *menu = n > 0 ? _menu[n - 1] : NULL; // the current menu
      if (menu && (stricmp(menu->GetName(), "RscGroupRootMenu")!=0 || stricmp(menu->GetName(), "RscHCGroupRootMenu")!=0))
      {        
        if (menu->IsContexSensitive())
          SwitchToStrategy(vehicle);
        else
          SwitchToFire(vehicle);
      }
      else SwitchToFire(vehicle);// nobody to command
    }
    else
    {
      // strategic cursor is available only before and during quick command menu
      int n = _menu.Size();
      Menu *menu = n > 0 ? _menu[n - 1] : NULL; // the current menu
      if (menu)
      {
        if (menu->IsContexSensitive())
          SwitchToStrategy(vehicle);
        else
          SwitchToFire(vehicle);
      }
      else
      {
        if (_showQuickMenu < UITIME_MAX)
          SwitchToStrategy(vehicle);
        else
          SwitchToFire(vehicle);
      }
    }
  }

  //////////////////////////////////////////////////////////////////////////
  // navigation in the units bar

#ifdef _XBOX
  bool enableGroupBarNavigation = true;
#else
  bool IsXboxUI();
  bool enableGroupBarNavigation = IsXboxUI();
#endif

  if (enableGroupBarNavigation && !_actions.IsListVisible())
  {
    // enable / disable the navigation
    if (ModeIsStrategy(_mode))
    {
      // enable navigation
      if (groupCommander)
      {//unit command
        if (_groupInfoSelected < 0) _groupInfoSelected = agent->GetUnit()->ID() - 1; // select all units
      }
      if(_drawHCCommand)
      {//groups command
        if (_groupInfoSelected < 0) _groupInfoSelected = 0; // select all units
      }
      else _groupInfoSelected = -1;
    }
    else _groupInfoSelected = -1;

    if (_groupInfoSelected >= 0)
    {
      if(!_drawHCCommand)
      {//untits command
        if (GInput.GetActionToDo(UAMenuSelect))
        {
          int sel = _groupInfoSelected;
          for (int i=_groupInfoSelected+1; i<group->NUnits(); i++)
          {
            AIUnit *u = group->GetUnit(i);
            if (u && u->GetSubgroup() && !group->GetReportedDown(u))
            {
              // shown in the bar
              sel = i;
              break;
            }
          }
          if (sel != _groupInfoSelected)
          {
            _groupInfoSelected = sel;
            _groupInfoOffset = 10 * (sel / 10);
            _lastGroupInfoTime = Glob.uiTime;
          }
        }
        if (GInput.GetActionToDo(UAMenuBack))
        {
          int sel = -1;
          for (int i=_groupInfoSelected-1; i>=0; i--)
          {
            AIUnit *u = group->GetUnit(i);
            if (u && u->GetSubgroup() && !group->GetReportedDown(u))
            {
              // shown in the bar
              sel = i;
              break;
            }
          }
          _groupInfoSelected = sel;
          _groupInfoOffset = intMax(10 * (sel / 10), 0);
          _lastGroupInfoTime = Glob.uiTime;
        }
      }
      else if(agent->GetUnit())
      {//hc groups command
        AutoArray<AIHCGroup> &groups = agent->GetUnit()->GetHCGroups();
        if (GInput.GetActionToDo(UAMenuSelect))
        {
          int sel = _groupInfoSelected;
          //for (int i=_groupInfoSelected+1; i<groups.Size(); i++)
          if(sel < groups.Size()-1) sel++;
          if (sel != _groupInfoSelected)
          {
            _groupInfoSelected = sel;
            _groupInfoOffset = 10 * (sel / 10);
            _lastGroupInfoTime = Glob.uiTime;
          }
        }
        if (GInput.GetActionToDo(UAMenuBack))
        {
          if(_groupInfoSelected >= 0) _groupInfoSelected--;
          _groupInfoOffset = intMax(10 * (_groupInfoSelected / 10), 0);
          _lastGroupInfoTime = Glob.uiTime;
        }
      }
    }
  }

  if (IsManual(vehicle, agent) && (isCommander || isGunnerFire))
  {
    // gunner uses own weapons, commander primary turret
    // always show information about the weapon we are firing with
    if (fireContextValid)
    {
      if (GInput.GetActionDo(UAReloadMagazine))
      {
        ShowWeaponInfo();
      }
      int weapon = fireContext._weapons->ValidatedCurrentWeapon();
      bool weaponChanged = 0; 
      if (GInput.GetActionToDo(UAToggleWeapons))
      { 
        bool handledToggleWeapons = false;
        for (int i=0; i<_lastUnitInfos.Size(); i++)
        { //if Menu with weapon is hidden, first show menu
          UnitInfoDesc &desc = _lastUnitInfos[i];
          if (!desc._display) continue;
          DisplayUnitInfo *disp = desc._display;

          if(disp->ammo && (Glob.uiTime - desc._changed) > piDimEndTime)
          {
            desc._changed = Glob.uiTime;
            handledToggleWeapons = true;
          }
        }
        if(!handledToggleWeapons)
        {
          weapon = fireContext._weapons->NextWeapon(vehicle, weapon);
          weaponChanged = true;
        }
      }
      else if (weapon >= 0)
      {
        // check if the current weapon is valid
        const MagazineSlot &slot = fireContext._weapons->_magazineSlots[weapon];
        if (!slot._muzzle->_showEmpty && fireContext._weapons->EmptySlot(slot))
        {
          // do not show empty weapon when not enabled
          weapon = fireContext._weapons->NextWeapon(vehicle, weapon);
          weaponChanged = true;
        }
        else if (!slot.ShowToPlayer() && (isGunnerFire || transport && transport->IsManualFire()))
        {
          // do not show weapons disabled for player when directly controlling the weapons (but avoid switching from weapon used autonomously by the gunner)
          weapon = fireContext._weapons->NextWeapon(vehicle, weapon);
          weaponChanged = true;
        }
      }

      if (weaponChanged)
      {
        weapon = fireContext._weapons->ValidateWeapon(weapon);
        SelectWeapon(agent, fireContext, weapon);
      }

      if (GInput.GetActionToDo(UAOpticsMode))
      { 
        if(agent->GetUnit() && agent->GetUnit()->IsFreeSoldier())
          fireContext._weapons->NextOpticsMode(agent->GetPerson());
     //   else if(fireContext._turret)
     //     fireContext._turret->NextOpticsMode(agent->GetPerson(),fireContext._turretType);
      }

      if(!agent->IsFreeSoldier() && fireContext._turret)
      {
        if(fireContext._turretType->_viewOptics.Size()>1 && GWorld->IsCameraActive(CamGunner) )
        {
          if(GInput.GetActionToDo(UAZoomIn))
          {
            fireContext._turret->NextOpticsMode(agent->GetPerson(),fireContext._turretType);
          }
          if(GInput.GetActionToDo(UAZoomOut))
          {
            fireContext._turret->PreviousOpticsMode(agent->GetPerson(),fireContext._turretType);
          }
        }

        if(GInput.GetActionToDo(UAZeroingUp))
        {
          fireContext._turret->ZeroingUp(fireContext._turretType);
        }
        if(GInput.GetActionToDo(UAZeroingDown))
        {
          fireContext._turret->ZeroingDown(fireContext._turretType);
        }
    }

    }
//str_usract_change_gunner_weapon
    if (GInput.GetActionToDo(UASwitchGunnerWeapon))
    { 
      TurretContextV fireContext;
      bool fireContextValid = vehicle->GetPrimaryGunnerTurret(unconst_cast(vehicle->RenderVisualState()), fireContext);
      if(fireContextValid)
      {
        int weapon = fireContext._weapons->ValidatedCurrentWeapon();
        weapon = fireContext._weapons->NextWeapon(vehicle, weapon);
        SelectWeapon(agent, fireContext, weapon);
      }

    }
  }
  /*
  {
    int weapon = vehicle->SelectedWeapon();
    if (weapon < 0)
    {
      weapon = vehicle->FirstWeapon();
      // select weapon only if it is primary weapon
      SelectWeapon(unit, weapon);
    }
  }
  */
  
  // list of visible objects
  const TargetList *visibleList=VisibleList();
  
  // find visible target with cursor on
  _target = NULL;
  _housePos = -1;
  _cover = NULL;
  Vector3 housePosition = VZero;

  {
    Vector3 cursorDir = GetCursorDirection();

    if (GInput.GetActionToDo(UARevealTarget))
    { // no reset - some processing needs to be done yet
      Vector3 itPos;
      Target *manualTarget= CheckCursorTarget(itPos, cursorDir, camera, cam, false, false);

      if (manualTarget)
      {
        RevealTarget(manualTarget,0.3);

        _target = manualTarget;

        // if some units are selecting, xmit Target
        OLinkPermNOArray(AIUnit) list;
        ListSelectingUnits(list);
        if (list.Size() > 0)
        {
          if (agent->IsEnemy(manualTarget->GetSide()))
          {
            AIGroup *group = agent->GetGroup();
            if (group) group->SendTarget(TargetToFire(manualTarget,manualTarget->State(agent)), false, false, list);
            ClearSelectedUnits();
          }
        }
      }
    }

    if (!_target)
    {
      // check which object is showed by cursor
      Vector3 itPos;
      Target *iTarget = CheckCursorTarget(itPos, cursorDir, camera, cam, true, true);
      if (iTarget)
      {
        _target = iTarget;
        // check if we should look-up in-house positions
        // find nearest in-house position to the point we are aiming at
        EntityAI *veh = iTarget->idExact;
        const IPaths *house = veh->GetIPaths();
        float minDist2 = 1e10;
        if (house && house->NPos() > 0)
        {
          for (int j=0; j<house->NPos(); j++)
          {
            Vector3Val pos = house->GetPosition(house->GetPos(j));

            float nearestT = NearestPointT(itPos,itPos+cursorDir,pos);
            if (nearestT<0) nearestT = 0;
            Vector3 nearest = itPos+nearestT*cursorDir;

            float dist2 = pos.Distance2(nearest);
            if (minDist2>dist2)
            {
              minDist2 = dist2;
              _housePos = j;
              housePosition = pos;
            }
          }
        }
      }

      #if _ENABLE_AI
      // prefer cover over targets?
      { // find nearest cover to where are we pointing to
        // TODO: check mode (cover has sense only for a move command)
        if (_selectedUnits.Size()==1 && _modeAuto==UIStrategyMove) // only one unit can be commanded to a specific cover
        {
          AIUnit *unitToCover = _selectedUnits[0]._unit;
          if (unitToCover->IsFreeSoldier()) // can use the cover
          {
            if (_cursorObject) // when pointing to an object
            {
              float xf(_cursorObjectPos.X() * InvOperItemGrid);
              float zf(_cursorObjectPos.Z() * InvOperItemGrid);
              
              OperMapIndex xMinO(toIntFloor(xf));
              OperMapIndex zMinO(toIntFloor(zf));
              OperMapIndex xMaxO(toIntCeil(xf));
              OperMapIndex zMaxO(toIntCeil(zf));
              
              LAND_INDICES(xMinO, zMinO, xMin, zMin);
              LAND_INDICES(xMaxO, zMaxO, xMax, zMax);
              
              if (OperMap::OperFieldRangeReady(xMin,xMax,zMin,zMax))
              {
                OperMap map;
                map.CreateMap(
                  unitToCover->GetVehicle(), xMin, zMin, xMax, zMax, MASK_AVOID_OBJECTS|MASK_PREFER_ROADS|MASK_USE_BUFFER|MASK_FOR_PATHING
                );
                // TODO: cover validation as in A*
                // scan all fields, find a nearest cover in them?
                CoverInfo cover = map.FindNearestCover(_cursorObjectPos,unitToCover->GetVehicle(),xMin,zMin,xMax,zMax);
                if (cover._payload)
                {
                  if (_housePos<0 || housePosition.Distance2(_cursorObjectPos)>cover._payload->_coverPos.Position().Distance2(_cursorObjectPos))
                  {
                    _cover = cover._payload;
                    _housePos = -1;
                  }
                }
                
                // check which is closer: house or cover?
                
                // 
              
              }
            }
          }
          
        }
      }
      #endif
    }

    // ground position we are pointing at (overridden by a target)
    if (_cover) _groundPoint = _cursorObjectPos;
    else if (_target)
    {
      if (_housePos >= 0) _groundPoint = housePosition;
#if _ENABLE_CHEATS
      else if (_showAll && _target->idExact) _groundPoint = _target->idExact->AimingPosition(_target->idExact->FutureVisualState());
#endif
      else _groundPoint = _target->AimingPosition();
    }
    else
    {
      _groundPoint = GLOB_LAND->IntersectWithGroundOrSea(camera.Position(), cursorDir);
    }

    _groundPointValid = camera.Position().Distance2(_groundPoint) < Square(0.9999f * Glob.config.horizontZ);
    if (_groundPoint.Y() < -5) _groundPointValid = false;
    if (_groundPointValid) _groundPointDistance = _groundPoint.Distance(vehicle->RenderVisualState().Position());
  }


  //////////////////////////////////////////////////////////////////////////
  // When quick menu is on and menu was changed, select the default command based on the mode
  // When quick menu is on, set the cursor based on the selected item

  SelectPriorCommand(agent, vehicleCommander);


#if _ENABLE_CHEATS
  if(GInput.GetCheat1ToDo(DIK_W))
  {
    if( _target.IdExact() )
    {
      GWorld->SwitchCameraTo(_target.IdExact(),GWorld->GetCameraType(), true);
      GWorld->UI()->ResetHUD();
    }
  }
#endif

  //////////////
  // Commander

  if (isCommander && IsManual(vehicle,agent))
  {
#if _VBS3
    if (!isPilot || agent->GetPerson()->IsPersonalItemsEnabled())
#else
    if (!isPilot)
#endif
    {
      DoAssert(transport);
      bool left = false, right = false;
      bool leftUp = false, rightUp = false;
      bool back = false;
      bool slow = false, forw = false, fast = false;
      if (GInput.GetActionToDo(UACommandLeft))
      {
        left = true;
      }
      if
      (
        _leftPressed && !GInput.GetAction(UACommandLeft)
      )
      {
        _leftPressed = false; leftUp = true;
      }
      if (GInput.GetActionToDo(UACommandRight))
      {
        right = true;
      }
      if
      (
        _rightPressed && !GInput.GetAction(UACommandRight)
      )
      {
        _rightPressed = false; rightUp = true;
      }
      if (GInput.GetActionToDo(UACommandSlow))
      {
        slow = true;
      }
      if (GInput.GetActionToDo(UACommandFast))
      {
        fast = true;
      }
      if (GInput.GetActionToDo(UACommandForward))
      {
        if (GInput.GetAction(UATurbo)) fast = true;
        else forw = true;
      }
      if (GInput.GetActionToDo(UACommandBack))
      {
        back = true;
      }

      Person *driver = transport && transport->PilotUnit() ? transport->PilotUnit()->GetPerson() : NULL;
      if (driver)
      {
        if (left)
        {
          SendCommandHelper(transport, driver, SCLeft);
          _leftPressed = true;
        }
        if (right)
        {
          SendCommandHelper(transport, driver, SCRight);
          _rightPressed = true;
        }
        if (leftUp || rightUp)
        {
          // predict orientation in estT
          const float estT = 0.35f;
          const Matrix3 &orientation=transport->RenderVisualState().Orientation();
          Matrix3Val derOrientation=transport->AngVelocity().Tilda()*orientation;
          Matrix3Val estOrientation=orientation+derOrientation*estT;

          Vector3Val estDirection=estOrientation.Direction().Normalized();
          float azimut = atan2(estDirection.X(), estDirection.Z());

          if (transport->IsLocal())
            transport->SendStopTurning(azimut);
          else
          {
            RadioMessageVStopTurning msg(transport, azimut);
            GetNetworkManager().SendMsg(&msg);
          }
        }
        if (back)
          SendCommandHelper(transport, driver, SCKeyDown);
        if (slow)
          SendCommandHelper(transport, driver, SCKeySlow);
        if (forw)
          SendCommandHelper(transport, driver, SCKeyUp);
        if (fast)
          SendCommandHelper(transport, driver, SCKeyFast);
      }
    }
  }

  if (isObserver && IsManual(vehicle,agent))
  {
    if (transport)
    {
      if (!isGunnerFire)
      {
        /// commander has no own weapons, controls primary turret
        if (transport->IsManualFire())
        {
          // primary turret / weapons state
          int weapon = -1;
          if (fireContextValid)
          {
            weapon = fireContext._weapons->ValidatedCurrentWeapon();
          }
          if (weapon >= 0)
          {
            const MagazineSlot &slot = fireContext._weapons->_magazineSlots[weapon];
            const WeaponModeType *mode = slot._weaponMode;
            const Magazine *mag = slot._magazine;
            bool fire = false;

            if (mag && mode && mode->_autoFire && mag->GetAmmo()>0)
            {
              if
                (
                // if not driver, click can be command to driver (if cursor mode is strategy)
                // rule is to fire only when target is locked
                (_lockTarget || isPilot || !ModeIsStrategy(_mode)) &&
                !GWorld->HasMap() &&
                (!ModeIsStrategy(_mode) || _modeAuto == UIStrategyFire) &&
                GInput.GetActionDo(UADefaultAction)
                ) fire = true;
            }
            else
            {
              if
                (
                // if not driver, click can be command to driver (if cursor mode is strategy)
                // rule is to fire only when target is locked
                (_lockTarget || isPilot || !ModeIsStrategy(_mode)) &&
                !GWorld->HasMap() &&
                GInput.GetActionToDo(UADefaultAction)
                ) fire = true;
            }
            if (fire)
            {
              bool local = fireContext._turret ? fireContext._turret->IsLocal() : vehicle->IsLocal();
              // workaround used to avoid sending of message for each autofire shot
              // shot is processed on the remote client in such case
              if (mode->_autoFire || local)
                vehicle->FireWeapon(fireContext, weapon,_lockTarget.IdExact(), false);
              else
                GetNetworkManager().AskForFireWeapon(vehicle, fireContext._turret, weapon, _lockTarget.IdExact(), false);
            }
          }
        }
        else
        {
          bool fire = false;
          if (GInput.GetActionToDo(UAFire))
          {
            fire = true;
          }
          if
            (
            // in not driver, click can be command to driver
            // rule is to fire only when target is locked
            (_lockTarget || isPilot) &&
            !GWorld->HasMap() &&
            (!ModeIsStrategy(_mode) || _modeAuto == UIStrategyFire) &&
            GInput.GetActionToDo(UADefaultAction)
            )
          {
            fire = true;
          }
          if (fire && fireContextValid)
            SendCommandHelper(transport, fireContext._gunner, SCKeyFire);
        }
      }
      else 
      {
        // fire command should be always given in gunners context
        // even if my own fire context is available
        TurretContextV gunnerContext = myContext;
        bool gunnerContextValid = vehicle->GetPrimaryGunnerTurret(unconst_cast(vehicle->RenderVisualState()), gunnerContext);
        if (GInput.GetActionToDo(UAFire))
        {
          if (gunnerContextValid)
            SendCommandHelper(transport, gunnerContext._gunner, SCKeyFire);
        }
      }
      
      // if we are not locking our own targets, send targets to the gunner
      if (!isGunnerTarget)
      {
        if (GInput.GetActionToDo(UALockTarget,true,true,true) && !GWorld->HasMap())
        {
          FindAndLockTarget(vehicle, targetContextValid, targetContext, transport);
        }

        SendTargetToGunner(targetContextValid, targetContext, transport);
      }

    }
  }

  if (isCommander && transport && person)
  {
    if(GInput.GetActionToDo(UALaunchCM)) 
      transport->LaunchCM(NULL, person  , true);
    if(GInput.GetActionToDo(UANextCM)) 
    {
      TurretContextV fireContext = myContext;
      if(transport->FindTurret(unconst_cast(transport->RenderVisualState()), person,fireContext))
        fireContext._weapons->NextCM(transport);
    }
  }

  //////////////
  // Gunner

  if (targetContextValid)
  {
    if( IsManual(vehicle,agent) )
    {
      // cannot switch targets when not a gunner and effective observer is somebody else

      if (isGunnerTarget || vehicle->EffectiveObserverUnit()==agent)
      {
        UserAction action = vehicle->GetLockTargetAction();
        if (GInput.GetActionToDo(action))
        {
          // switch target - use target list (tactical display)
          if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
            PrevTarget(vehicle, targetContext);
          else
            NextTarget(vehicle, targetContext);
        }
      }
    }
    else
    {
      Target *tgt = fireContext._weapons->_fire._fireTarget;
      if (!tgt) tgt=agent->GetTargetAssigned();
      if (tgt && tgt->idExact && visibleList)
      {
        const Target *tar = visibleList->FindTarget(tgt->idExact);
        if( tar)
        {
          _lockTarget = unconst_cast(tar);
          _timeSendTarget = UITIME_MAX;
        }
      }
      else
      {
        _lockTarget=NULL;
        _timeSendTarget = UITIME_MAX;
      }
    }
  }

  bool issueCommand = false;  
  // react to mouse buttons
  if( ModeIsStrategy(_mode) )
  { 
    if (GInput.GetActionToDo(UAActionContext))
    {
      _mouseDown = true;
#ifndef _XBOX
       _mouseDownTime = Glob.uiTime + 0.2;
      _startSelection = GetCursorDirection();
#endif
      _fireEnabled = false;
    }
    else
    {
      if (_mouseDown)
      {
#ifndef _XBOX
        Vector3 dir = GetCursorDirection();
        if
        (
          dir.Distance2(_startSelection) > Square(0.01) &&
          Glob.uiTime >= _mouseDownTime
        )
        {
          _mouseDown = false;
          _dragging = true;
        }
        else
#endif
        if (!GInput.GetActionDo(UAActionContext))
        {
          _mouseDown = false;
          issueCommand = true;
        }
      }
#ifndef _XBOX
      if (_dragging)
      {
        _endSelection = GetCursorDirection();
        if (!GInput.GetActionDo(UAActionContext))
        {
          AUTO_STATIC_ARRAY(OLinkPerm<AIUnit>,units,64);
          AUTO_STATIC_ARRAY(LinkTarget,enemies,64);

          Matrix4Val camInvTransform = camera.GetInvTransform();
          Vector3 posStart = camInvTransform.Rotate(_startSelection);
          Vector3 posEnd = camInvTransform.Rotate(_endSelection);
          if (posStart.Z() > 0 && posEnd.Z() > 0 && visibleList)
          {
            float invZ = 1.0 / posStart.Z();
            float xs = 0.5 * (1.0 + posStart.X() * invZ * camera.InvLeft());
            float ys = 0.5 * (1.0 - posStart.Y() * invZ * camera.InvTop());
            invZ = 1.0 / posEnd.Z();
            float xe = 0.5 * (1.0 + posEnd.X() * invZ * camera.InvLeft());
            float ye = 0.5 * (1.0 - posEnd.Y() * invZ * camera.InvTop());
            if (xs > xe) swap(xs, xe);
            if (ys > ye) swap(ys, ye);
            int n = visibleList->AnyCount();
            for (int i=0; i<n; i++)
            {
              TargetNormal &tar = *visibleList->GetAny(i);
              if (!tar.IsKnownBy(agent) || tar.vanished) continue;
              EntityAI *veh = tar.idExact;
              if (!veh) continue;
              // check position
              Vector3 dir = tar.AimingPosition() - camera.Position();
              Vector3 pos = camInvTransform.Rotate(dir);
              if (pos.Z() <= 0) continue;
              invZ = 1.0 / pos.Z();
              float x = 0.5 * (1.0 + pos.X() * invZ * camera.InvLeft());
              float y = 0.5 * (1.0 - pos.Y() * invZ * camera.InvTop());
              if (x < xs) continue;
              if (x > xe) continue;
              if (y < ys) continue;
              if (y > ye) continue;
              AIUnit *u = veh->CommanderUnit() ? veh->CommanderUnit()->GetUnit() : NULL;
              if (u && u->GetGroup() == agent->GetGroup())
              {
                if (u != agent)
                  units.Add(u);
              }
              else if (agent->IsEnemy(tar.side))
              {
                enemies.Add(&tar);
              }
            }
          }

          if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
          {
            for (int i=0; i<units.Size(); i++)
            {
              AIUnit *u = units[i];
              if (IsSelectedUnit(u))
                UnselectUnit(u);
              else
                SelectUnit(u);
            }
          }
          else if (IsEmptySelectedUnits() || enemies.Size() == 0)
          {
            ClearSelectedUnits();
            for (int i=0; i<units.Size(); i++)
            {
              AIUnit *u = units[i];
              SelectUnit(u);
            }
          }
          else
          {
            Command cmd;
            cmd._message = Command::AttackAndFire;
            cmd._targetE = enemies[0];
            cmd._destination = vehicle->RenderVisualState().Position();

            for (int i=0; i<_selectedUnits.Size(); i++)
            {
              AIUnit *u = _selectedUnits[i]._unit;
              if (u) u->AssignTarget(TargetToFire(enemies[0],enemies[0]->State(u)));
            }

            AIGroup *group = agent->GetGroup();
            if (group)
            {
              if (CheckJoin(group))
              {
                cmd._context = Command::CtxUIWithJoin;
                cmd._joinToSubgroup = group->MainSubgroup();
              }
              else
              {
                cmd._context = Command::CtxUI;
              }

              OLinkPermNOArray(AIUnit) list;
              ListSelectedUnits(list);
              group->SendCommand(cmd, list);
            }
            ClearSelectedUnits();
          } 
          _dragging = false;
        }
      }
#endif
    }

    // prior quick menu is on, also Action activates the default command
    bool confirmMenu = false;
    ///if(_showQuickMenu < UITIME_MAX && GetCurrentMenu() && GInput.GetActionToDo(UADefaultAction)) confirmMenu = true;
    if(_showQuickMenu < UITIME_MAX && (confirmMenu || GInput.GetActionDo(UAAction)))
    {
      issueCommand = true;
    }

    if (issueCommand)
    {
      if (IssueCommand(vehicle)) // do not hide menu when unit is selected
      {
        if (_xboxStyle && cam == CamGroup)
        {
          _menu.Resize(0);
#if _ENABLE_CONVERSATION
          _conversationContext = NULL;
#endif
          // leave _units menu active
        }
        else
        {
          ClearSelectedUnits();
          HideMenu();
          // do not show quick commanding menu
          _showQuickMenu = UITIME_MAX;
          /*
          if (_forcingCommandingMode) _forcingCommandingMode=false;
          else HideMenu();
          */
        }
      }
    }
  }

  if(!issueCommand)
  {
    if (vehicle)
      vehicle->ResetPermanentForceMove(); //this needs to be done, because throwing sets the forced move to be permanent

    bool fire = false;
    if (GInput.GetActionToDo(UADefaultAction))
    {
      _fireEnabled = true;
      _fireStartTime = Glob.time;
      fire = true;
    }
    if (IsManual(vehicle,agent) && _fireEnabled && vehicle->CanFire() && isGunnerFire )
    {
      // Gunner is firing using mouse
      int weapon = -1;
      if (myContextValid) weapon = myContext._weapons->ValidatedCurrentWeapon();
      if (weapon >= 0)
      {
        const MagazineSlot &slot = myContext._weapons->_magazineSlots[weapon];
        const WeaponModeType *mode = slot._weaponMode;
        const Magazine *mag = slot._magazine;

        // check if this is throw
        bool isThrow = myContext._weapons->IsThrowable(weapon);

        if (mode && (mode->_autoFire || isThrow) && mag && mag->GetAmmo() > 0)
        {
          fire = GInput.GetActionDo(UADefaultAction) && !GWorld->HasMap();
        }

        // check if we are holding fire button long enough
        if (isThrow)
        {
          if (Glob.time < _fireStartTime || (Glob.time - _fireStartTime) > myContext._weapons->GetMaxThrowHoldTime(weapon))
            fire = false; //this will cause the grenade to throw

          // compute weapon cursor scale coef
          float maxTime = myContext._weapons->GetMaxThrowHoldTime(weapon);
          if (maxTime > 0.0f)
          {
            _weaponCursorScaleCoef = (Glob.time - _fireStartTime)/maxTime;
            saturate(_weaponCursorScaleCoef, 0.0f, 1.0f);
            _weaponCursorScaleCoef = _throwCursorMinScale + (_throwCursorMaxScale - _throwCursorMinScale)*_weaponCursorScaleCoef; 
          }
        }

        if (fire)
        {
          //satchel and time etc. charges should be put down through animation
          // some actions are postponed
          const MagazineSlot &slot = myContext._weapons->_magazineSlots[weapon];
          Magazine *magazine = slot._magazine;          
          const AmmoType *ammo = myContext._weapons->GetAmmoType(weapon);
          if (!ammo || !magazine) vehicle->FireWeapon(myContext, weapon, _lockTarget.IdExact(), false);
          else
          {
            switch (ammo->_simulation)
            {
            case AmmoShotTimeBomb:
            case AmmoShotPipeBomb:
            case AmmoShotMine:
              {
                Ref<Action> action = new ActionMagazine(ATUseMagazine, vehicle, myContext._gunner, magazine->_creator, magazine->_id);
                vehicle->StartActionProcessing(action, agent);
              }
              break;
            default:
              if (!isThrow) 
              {
                vehicle->FireWeapon(myContext, weapon, _lockTarget.IdExact(), false);
              }
              else 
              {
                // we just want to play the prepare animation when throwing, fire will be done after the button is released
                vehicle->PrepareThrow(myContext, weapon);
              }
              break;
            }
          }
        }
        else 
        {
          if (isThrow)
          {
            // perform throw 

            // compute intensity coef
            float coef = 1.0f;
            float maxTime = myContext._weapons->GetMaxThrowHoldTime(weapon);
            if (maxTime > 0.0f)
            {
              coef = (Glob.time - _fireStartTime)/maxTime;
              saturate(coef, 0.0f, 1.0f);
            }
            float minCoef = myContext._weapons->GetMinThrowIntensityCoef(weapon);
            float maxCoef = myContext._weapons->GetMaxThrowIntensityCoef(weapon);
            coef = minCoef + (maxCoef - minCoef)*coef;

            myContext._weapons->_throwIntensityCoef = coef;

            vehicle->FireWeapon(myContext, weapon, _lockTarget.IdExact(), false);
          }
          _fireStartTime = Time(0);
          _fireEnabled = false;
        }
      }
    }
  }

  if( IsManual(vehicle,agent) && isGunnerTarget && GInput.GetActionToDo(UALockTarget))
  {
    int weapon = -1;
    if (myContextValid) weapon = myContext._weapons->ValidatedCurrentWeapon();
    if (weapon >= 0)
    {
      if (_target.IdExact() && _target.IdExact() != vehicle && !_target->IsVanished())
      {
        const MagazineSlot &slot = myContext._weapons->_magazineSlots[weapon];
      
        bool canLock =
          slot._muzzle->_canBeLocked == 2 ||
          slot._muzzle->_canBeLocked == 1 && Glob.config.IsEnabled(DTAutoGuideAT);

        if (canLock && vehicle->CanLock(_target.IdExact(), myContext))
        {
          _lockTarget = _target;
          _timeSendTarget = UITIME_MAX;
        }
      }
      else
      {
        _lockTarget = NULL;
        _timeSendTarget = UITIME_MAX;
      }
    }
  }

  // follow selected target, if the lock is no more possible, release it
  if( _lockTarget )
  {
    bool lockFound=false; //,targetFound=false;
    Target *oldTarget = _lockTarget;
    int weapon = -1;
    if (targetContextValid) weapon = targetContext._weapons->ValidatedCurrentWeapon();
    if (weapon >= 0)
    {
      const MagazineSlot &slot = targetContext._weapons->_magazineSlots[weapon];
      const Magazine *magazine = slot._magazine;
      const AmmoType *ammo = NULL;
      if (magazine)
      {
        const MagazineType *type = magazine->_type;
        if (type) ammo = type->_ammo;
      }

      bool canLock =
        slot._muzzle->_canBeLocked == 2 ||
        slot._muzzle->_canBeLocked == 1 && Glob.config.IsEnabled(DTAutoGuideAT);
      
      if (ammo && (canLock || !IsManual(vehicle,agent)) && visibleList)
      {
        const TargetNormal *tar = visibleList->FindTarget(_lockTarget.IdExact());
        if (tar)
        {
          Assert( tar->type );
          if (
            tar->type && tar->idExact && !tar->idExact->Invisible() &&
            tar->idExact->LockPossible(ammo) &&
            tar->IsKnownBy(agent)
          )
          {
            _lockTarget = unconst_cast(tar);
            lockFound=true;
            //_wantLock = true;
          }
        }
      }
    }
    if( !lockFound || _lockTarget.IdExact()==vehicle )
    {
      if( oldTarget && oldTarget->idExact!=vehicle )
      {
        if( !ModeIsStrategy(_mode) )
        {
          Vector3 norm = (oldTarget->AimingPosition() - vehicle->RenderVisualState().Position());
          SetCursorDirection(norm.Normalized());
        }
      }
      _lockTarget=NULL;
    }
  }

  //_wantLock=( _lockTarget.idExact );

  if (GInput.GetActionToDo(UANightVision))
  {
      person->NextVisionMode();
    }

  /*
  const float brightnessF = 4.0f;
  const float autoBCF = 2.0f;
  const float contrastF = 4.0f;

  GEngine->SetTIBrightness(GEngine->GetTIBrightness() + 
    (GInput.GetActionExclusive(UATI_Brightness_Inc) - GInput.GetActionExclusive(UATI_Brightness_Dec))
    * deltaT * brightnessF
    );
  if(GEngine->GetTIAutoBC())
  {
    GEngine->SetTIAutoBCContrastCoef(GEngine->GetTIAutoBCContrastCoef() - 
      (GInput.GetActionExclusive(UATI_Contrast_Inc) - GInput.GetActionExclusive(UATI_Contrast_Dec))
      * deltaT * autoBCF
      );
  }
  else
  {
    GEngine->SetTIContrast(GEngine->GetTIContrast() - 
      (GInput.GetActionExclusive(UATI_Contrast_Inc) - GInput.GetActionExclusive(UATI_Contrast_Dec))
      * deltaT * contrastF
      );
  }
  if(GInput.GetActionToDo(UATI_AutoContrast_Toggle))
    GEngine->SetTIAutoBC(!GEngine->GetTIAutoBC());
  */
}

void InGameUI::FindAndLockTarget( EntityAIFull * vehicle, bool gunnerContextValid, TurretContextV &gunnerContext, Transport * transport )
{
  if ((_target.IdExact() && _target.IdExact() != vehicle && !_target->IsVanished() && gunnerContextValid && gunnerContext._gunner) ||
    (!_target.IdExact() && _lockTarget /*i.e. unlock target*/) )
  {
    if (transport->IsLocal())
      transport->SendTarget(gunnerContext._gunner, _target);
    else
    {
      RadioMessageVTarget msg(transport, gunnerContext._gunner, _target);
      GetNetworkManager().SendMsg(&msg);
    }
    _lockTarget = _target;
    _timeSendTarget = UITIME_MAX;
    _lockAimValidUntil = Glob.uiTime - 60;
  }
}

/**
If the target is stable long enough, send it to the gunner
*/
void InGameUI::SendTargetToGunner( bool gunnerContextValid, TurretContextV &gunnerContext, Transport * transport )
{
  if (Glob.uiTime >= _timeSendTarget && gunnerContextValid && gunnerContext._gunner)
  {
    if (transport->IsLocal())
      transport->SendTarget(gunnerContext._gunner, _lockTarget);
    else
    {
      RadioMessageVTarget msg(transport, gunnerContext._gunner, _lockTarget);
      GetNetworkManager().SendMsg(&msg);
    }
    _timeSendTarget = UITIME_MAX;
  }
}

void InGameUI::SwitchToStrategy( EntityAIFull *vehicle )
{
  if( !ModeIsStrategy(_mode) )
  {
    _modeAuto = _mode = UIStrategy;

#ifndef _XBOX
    _dragging = false;
#endif
    _mouseDown = false;

    if (GWorld->GetCameraTypeWanted() == CamExternal)
    {
      ShowMe();
    }
  }
}

void InGameUI::SwitchToFire( EntityAIFull *vehicle )
{
  if( ModeIsStrategy(_mode) )
  {
    _modeAuto = _mode = UIFire;
    
    if (vehicle)
    {
      AIBrain *unit = GWorld->FocusOn();

      TurretContextV context;
      float valid = vehicle->FindTurret(unconst_cast(vehicle->RenderVisualState()), unit->GetPerson(), context);
      if (valid)
      {
        if (context._turret && !context._turretVisualState->IsGunnerHidden() && !context._turretType->OutGunnerMayFire(context._gunner))
          SetCursorDirection(vehicle->GetCameraDirection(GWorld->GetCameraType()));
        else
        {
          int weapon = context._weapons->ValidatedCurrentWeapon();
          if (weapon >= 0)
            SetCursorDirection(vehicle->GetWeaponDirection(vehicle->RenderVisualState(), context, weapon));
          else
            SetCursorDirection(vehicle->GetEyeDirection(vehicle->RenderVisualState(), context));
        }
      }
      else
        SetCursorDirection(vehicle->RenderVisualState().Direction());
    }

#ifndef _XBOX
    _dragging = false;
#endif
    _mouseDown = false;
  }
}

void InGameUI::SelectWeapon(AIBrain *unit, const TurretContext &context, int weapon)
{
  EntityAIFull *vehicle = unit->GetVehicle();
  vehicle->SelectWeaponCommander(unit, context, weapon);
}

void InGameUI::InitMenu()
{
  _menu.Resize(0);
  _units = NULL;
  _unitsSingle = NULL;
#if _ENABLE_CONVERSATION
  _conversationContext = NULL;
#endif
  _initialMenu = false;
}

void InGameUI::ToggleSelection(AIGroup *grp, int index)
{
  // check if there is unit id in grp
  AIUnit *u = grp->GetUnit(index);
  if (!u) return;

  if (IsSelectedUnit(u))
    UnselectUnit(u);
  else
    SelectUnit(u);
}

void InGameUI::SetSemaphore(AIBrain *unit, AI::Semaphore status)
{
  if (!unit) return;
  AIGroup *group = unit->GetGroup();
  if (!group) return;
  if (group->Leader() == unit)
  {
    OLinkPermNOArray(AIUnit) list;
    ListSelectedUnits(list);
    group->SendSemaphore(status, list);
  }
}

void InGameUI::SetBehaviour(CombatMode mode)
{
  for (int i=0; i<_selectedUnits.Size(); i++)
  {
    AIUnit *u = _selectedUnits[i]._unit;
    if (u) u->SetCombatModeMajorReactToChange(mode);
  }
}

void InGameUI::SetUnitPosition(AIBrain *unit, UnitPosition status)
{
  if (!unit) return;
  AIGroup *group = unit->GetGroup();
  if (!group) return;
  if (group->Leader() == unit)
  {
    OLinkPermNOArray(AIUnit) list;
    ListSelectedUnits(list);
    group->SendState(new RadioMessageUnitPos(group,list,status));
  }
}

void InGameUI::SetFormationPos(AIBrain *unit, AI::FormationPos status)
{
  if (!unit) return;
  AIGroup *group = unit->GetGroup();
  if (!group) return;
  if (group->Leader() == unit)
  {
    OLinkPermNOArray(AIUnit) list;
    ListSelectedUnits(list);
    group->SendState(new RadioMessageFormationPos(group,list,status));
  }
}

void InGameUI::ShowHint(RString hint, bool sound)
{
  _hint->SetHint(hint);
  _hintTime = Glob.uiTime;
  if (sound && hint.GetLength() > 0 && _hintSound.name.GetLength() > 0)
  {
    AbstractWave *wave = GSoundScene->OpenAndPlayOnce2D
    (
      _hintSound.name, _hintSound.vol, _hintSound.freq, false
    );
    if (wave)
    {
      wave->SetKind(WaveMusic); // UI sounds considered music???
      GSoundScene->AddSound(wave);
    }
  }
}

void InGameUI::ShowHint(INode *hint, bool sound)
{
  _hint->SetHint(hint);
  _hintTime = Glob.uiTime;
  if (sound && hint != 0 && _hintSound.name.GetLength() > 0)
  {
    AbstractWave *wave = GSoundScene->OpenAndPlayOnce2D(
      _hintSound.name, _hintSound.vol, _hintSound.freq, false);
    if (wave)
    {
      wave->SetKind(WaveMusic); // UI sounds considered music???
      GSoundScene->AddSound(wave);
    }
  }
}

void InGameUI::ShowTaskHint(RString taskHint, PackedColor color, Texture *texture)
{
  _taskHint->AddTaskHint(taskHint, color, texture);
}

const IAttribute *InGameUI::FindMenuAttribute(RString name) const
{
  int attrIndex = MenuAttributeTypes.attributes.GetValue(name);
  if (attrIndex < 0) return NULL;

  int n = _menu.Size();
  for (int i=n-1; i>=0; i--)
  {
    const Menu *menu = _menu[i];
    Assert(menu);
    const MenuItem *item = menu->_items[menu->_selected];
    Assert(item);
    if (attrIndex >= item->_attributes.Size()) continue;
    const IAttribute *attr = item->_attributes[attrIndex];
    if (attr) return attr;
  }

  return NULL;
}

void InGameUI::SendCommandHelper(Transport *transport, Person *receiver, SimpleCommand cmd)
{
  if (!transport || !receiver)
    return;
  if (transport->IsLocal())
    transport->SendSimpleCommand(receiver, cmd);
  else
  {
    RadioMessageVSimpleCommand msg(transport, receiver, cmd);
    GetNetworkManager().SendMsg(&msg);
  }
}

void InGameUI::UpdateWeaponCursorScaleCoef(float deltaT)
{
  if (_weaponCursorScaleCoef != 1.0f)
  {
    if (_weaponCursorScaleCoef > 1.0f)
    {
      _weaponCursorScaleCoef -= _throwCursorFadeSpeed * deltaT;
      if (_weaponCursorScaleCoef <= 1.0f)
        _weaponCursorScaleCoef = 1.0f;
    }
    else
    {
      _weaponCursorScaleCoef += _throwCursorFadeSpeed * deltaT;
      if (_weaponCursorScaleCoef >= 1.0f)
        _weaponCursorScaleCoef = 1.0f;
    }
  }
}

#if _ENABLE_CONVERSATION

void InGameUI::ProcessConversation(AIBrain *askingUnit, AIBrain *askedUnit, const KBMessageInfo *question)
{
  SRef<ConversationContext> oldContext = _conversationContext;
  _conversationContext = new ConversationContext(askingUnit, askedUnit, question);
  // create a menu
  Ref<Menu> menu = new Menu();
  CreateDynamicMenu(menu, "CONVERSATION");
  if (menu->_items.Size() > 0)
  {
    _units = NULL;
    _unitsSingle = NULL;
    _menu.Resize(1);
    _menu[0] = menu;
    menu->_locked = true;
    ShowMenu();
  }
  else
  {
    _conversationContext = oldContext;
  }
}

void ProcessConversation(AIBrain *askingUnit, AIBrain *askedUnit, const KBMessageInfo *question)
{
  if (GWorld && GWorld->UI()) GWorld->UI()->ProcessConversation(askingUnit, askedUnit, question);
}

void ClearConversationHistory()
{
}

#endif

