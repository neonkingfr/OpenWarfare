#include "../wpch.hpp"
#include "chat.hpp"
#include "uiControls.hpp"
#include "../world.hpp"
#include "../Network/network.hpp"
#include "resincl.hpp"
#include "../AI/ai.hpp"
#include "../transport.hpp"
#include "../person.hpp"
#include "../unitsOnLand.hpp"
#include "../landscape.hpp"

#include "../keyInput.hpp"
#include "../dikCodes.h"

#include "../stringtableExt.hpp"
#include "../mbcs.hpp"
#include <El/Common/perfProf.hpp>

#include <Es/Common/win.h>

/*!
\file
Implementation file for chat processing - chat edit, chat (radio message) list
*/

//! maximal total number of chat messages in history
#define CHAT_ITEMS					100

RString GetFullPlayerName()
{
	const PlayerIdentity *identity = GetNetworkManager().FindIdentity(GetNetworkManager().GetPlayer());
	if (identity)
		return identity->GetName();
	else
#if _VBS2 // VBS2 nickname
    return Glob.header.playerHandle;
#else
		return Glob.header.GetPlayerName();
#endif
}

RString GetFullPlayerName(Person *person)
{
	int player = person->GetRemotePlayer();
	if (player == 1) return person->GetInfo()._name;
	
	const PlayerIdentity *identity = GetNetworkManager().FindIdentity(player);
	if (identity)
		return identity->GetName();
	else
		return person->GetInfo()._name;
}

ChatList::ChatList()
{
	_x = 0;
	_y = 0;
	_w = 0;
  _h = 0;
	_rows = 0;
	_lastSpeaking = UITIME_MIN;
	
	_font = NULL;
	_size = 0;

	_bgColor = PackedColor(Color(0, 0, 0, 0.8));
	_colors[CCGlobal] = PackedColor(Color(0.8, 0.8, 0.8, 1));
	_colors[CCSide] = PackedColor(Color(0, 0.9, 0.9, 1));
  _colors[CCCommand] = PackedColor(Color(0, 0, 0.9, 1));
	_colors[CCGroup] = PackedColor(Color(0.1, 0.9, 0.2, 1));
	_colors[CCVehicle] = PackedColor(Color(0.9, 0.8, 0, 1));
	_colors[CCDirect] = PackedColor(Color(0.9, 0, 0.8, 1));
#if _ENABLE_DIRECT_MESSAGES
  _colors[CCDirectSpeaking] = PackedColor(Color(1, 0, 0, 1));
#endif
	_colors[CCSystem] = PackedColor(Color(1.0, 0.1, 0.1, 1));
	_colors[CCBattlEye] = PackedColor(Color(1.0, 0.1, 0.1, 1));
	_enable = true;
  _scriptEnable = true;
  _shadow = false;

	_offset = -1;
	_lastSpeaking = UITIME_MIN;
}

void ChatList::Free()
{
  _font = NULL;
 // _iconNormalMessage = NULL;
 // _iconPlayerMessage = NULL;
}

void ChatList::BrowseUp()
{
	if (_offset < Size() - 1) _offset++;
}

void ChatList::BrowseDown()
{
	if (_offset > 0) _offset--;
}

void ChatList::BrowseReset()
{
	_offset = -1;
}

void ChatList::Load(ParamEntryPar cls)
{
	_x = cls >> "x";
	_y = cls >> "y";
	_w = cls >> "w";
  _h = cls >> "h";
	_rows = cls >> "rows";

	_font = GEngine->LoadFont(GetFontID(cls >> "font"));
	_size = cls >> "size";

  _bgColor = GetPackedColor(cls >> "colorBackground");
  _bgPlayerColor = GetPackedColor(cls >> "colorPlayerBackground");
  _colors[CCGlobal] = GetPackedColor(cls >> "colorGlobalChannel");
  _colors[CCSide] = GetPackedColor(cls >> "colorSideChannel");
  _colors[CCCommand] = GetPackedColor(cls >> "colorCommandChannel");
  _colors[CCGroup] = GetPackedColor(cls >> "colorGroupChannel");
  _colors[CCVehicle] = GetPackedColor(cls >> "colorVehicleChannel");
  _colors[CCDirect] = GetPackedColor(cls >> "colorDirectChannel");
  if (cls.FindEntry("colorSystemChannel")) //else engine default is taken
    _colors[CCSystem] = GetPackedColor(cls >> "colorSystemChannel");
  if (cls.FindEntry("colorBattlEyeChannel")) //else engine default is taken
    _colors[CCBattlEye] = GetPackedColor(cls >> "colorBattlEyeChannel");
#if _ENABLE_DIRECT_MESSAGES
  // TODO:
#endif
  _shadow = cls >> "shadow";
  _shadowColor = GetPackedColor(cls >> "shadowColor");

//  _iconNormalMessage = GlobLoadTexture(cls >> "iconNormalMessage");
//  _iconPlayerMessage = GlobLoadTexture(cls >> "iconPlayerMessage");
}

void ChatList::Add(ChatChannel channel, RString from, RString text, bool playerMsg, bool forceDisplay)
{
  //original subtitles effect for direct speech
  /*if (channel == CCDirect)
	{
		GWorld->SetTitleEffect(CreateTitleEffect(TitPlainDown ,text));
		return;
	}*/
  //now is used chat 

	while (Size() >= CHAT_ITEMS)
	{
		Delete(CHAT_ITEMS - 1);
	}
	Insert(0);
	Set(0).channel = channel;
	Set(0).from = from;
	Set(0).time = Glob.clock;
	Set(0).text = text;
	Set(0).uiTime = Glob.uiTime;
  Set(0).gameTime = Glob.time;
	Set(0).playerMsg = playerMsg;
	Set(0).forceDisplay = forceDisplay;

	if (_offset > 0) _offset++;

  //@{ _DEDICATED_CLIENT
  if ( IsDedicatedClient() )
  {
    if (!text.IsEmpty())
    {
      RString format = Format(" > %s", cc_cast(text));
      int ConsoleF(const char *format, ...);
      ConsoleF(format);
    }
  }
  //@}
}


void ChatList::Add(ChatChannel channel, AIBrain *sender, RString text, bool playerMsg, bool forceDisplay)
{
#define FROM_BUFFER_SIZE 256
#define FROM_BUFFER_EMPTYSPACE intMax(FROM_BUFFER_SIZE - strlen(from) - 1,0)

  char from[FROM_BUFFER_SIZE];
  memset(from, 0, sizeof(char)*FROM_BUFFER_SIZE);
	if (sender)
	{
		Person *person = sender->GetPerson();
		Assert(person);

		switch (channel)
		{
		case CCGlobal:
			{
				AIGroup *grp = sender->GetGroup();
				if (!grp) break;
				AICenter *center = grp->GetCenter();
				if (!center) break;
				switch (center->GetSide())
				{
				case TWest:
					strcpy(from, LocalizeString(IDS_WEST)); break;
				case TEast:
					strcpy(from, LocalizeString(IDS_EAST)); break;
				case TGuerrila:
					strcpy(from, LocalizeString(IDS_GUERRILA)); break;
				case TCivilian:
					strcpy(from, LocalizeString(IDS_CIVILIAN)); break;
				}
			}
			break;
		case CCSide:
    case CCCommand:
			{
				AIGroup *grp = sender->GetGroup();
				if (!grp) break;
        AIUnit *unit = sender->GetUnit();
        Assert(unit); // if group is valid, unit must be valid as well
				snprintf(from, FROM_BUFFER_SIZE - 1, "%s %d", (const char *)grp->GetName(), unit->ID());
			}
			break;
		case CCGroup:
      {
        AIUnit *unit = sender->GetUnit();
        snprintf(from, FROM_BUFFER_SIZE - 1, "%d", unit ? unit->ID() : 0);
      }
			break;
		case CCVehicle:
			{
				Transport *veh = sender->GetVehicleIn();
				if (!veh) break;
        if (person == veh->Driver())
        {
          if (veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
            strcpy(from, LocalizeString(IDS_PILOT));
          else
            strcpy(from, LocalizeString(IDS_DRIVER));
        }
        else
        {
          TurretContext context;
          if (veh->FindTurret(person, context) && context._turretType)
          {
            strcpy(from, context._turretType->_gunnerName);
          }
          else if(person && !person->IsNetworkPlayer())
          {
            strcpy(from, GetFullPlayerName(person));
          }
        }
/*
				if (person == veh->Commander())
					strcpy(from, LocalizeString(IDS_COMMANDER));
				else if (person == veh->Driver())
				{
					if (veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
						strcpy(from, LocalizeString(IDS_PILOT));
					else
						strcpy(from, LocalizeString(IDS_DRIVER));
				}
				else if (person == veh->Gunner())
					strcpy(from, LocalizeString(IDS_GUNNER));
				else if (sender == veh->CommanderUnit())
					strcpy(from, LocalizeString(IDS_COMMANDER));
				else
					LogF("Warning: unknown position");
*/
			}
			break;
		case CCDirect:
#if _ENABLE_DIRECT_MESSAGES
    case CCDirectSpeaking:
#endif
      //original subtitles effect for direct speech
			/*
      GWorld->SetTitleEffect(CreateTitleEffect(TitPlainDown ,text));
			return;
      */
      //now is used chat
      if (person && person->IsNetworkPlayer())
      {
        strncat(from, " (", FROM_BUFFER_EMPTYSPACE);
        strncat(from, GetFullPlayerName(person), FROM_BUFFER_EMPTYSPACE);
        strncat(from, ")", FROM_BUFFER_EMPTYSPACE);
      }
      else
      {
        strcpy(from, GetFullPlayerName(person));
  		}
      Add(channel, from, text, playerMsg, forceDisplay);
      return;
		}
		
		if (person && person->IsNetworkPlayer())
		{
      if (from[0])
      {
        strncat(from, " (", FROM_BUFFER_EMPTYSPACE);
        strncat(from, GetFullPlayerName(person), FROM_BUFFER_EMPTYSPACE);
        strncat(from, ")", FROM_BUFFER_EMPTYSPACE);
      }
			else
			{
/*
				strcpy(from, person->GetInfo()._name);
*/
				strcpy(from, GetFullPlayerName(person));
			}
		}

	}
	Add(channel, from, text, playerMsg, forceDisplay);
}

RString StrUprUniversal(const char *text);

void ChatList::AddUC(ChatChannel channel, RString from, RString text, bool playerMsg, bool forceDisplay)
{
  // Make the text uppercase
  RString textUTF8 = StrUprUniversal(text);

  // Add text to the list
  Add(channel, from, textUTF8, playerMsg, forceDisplay);
}

void ChatList::AddUC(ChatChannel channel, AIBrain *sender, RString text, bool playerMsg, bool forceDisplay)
{
  // Make the text uppercase
  RString textUTF8 = StrUprUniversal(text);

  // Add text to the list
  Add(channel, sender, textUTF8, playerMsg, forceDisplay);
}

/*!
\patch 5119 Date 1/18/2007 by Jirka
- Fixed: Some international characters was clipped in the chat log
\patch 5148 Date 3/27/2007 by Jirka
- Improved: UI - Chat log text border
\patch 5196 Date 12/11/2007 by Ondra
- New: Added difficulty option VonID (VON ID), allowing to see in-game who is currently speaking over the radio.
\patch 5209 Date 1/2/2008 by Ondra
- Fixed: When switching VON channels quickly, the VON ID indication could still show the old channel.
*/

void ChatList::OnDraw()
{
	// if (GWorld->GetTitleEffect()) return; // hide chat when titles appears

	if (_rows <= 0 || !_font) return;

  SCOPE_GRF("chatList",0);
  
  bool multiplayer = GetNetworkManager().GetClientState() > NCSNone;
  if (!multiplayer && !GWorld->IsSimulationEnabled()) return;

	const float startDim = 25, endDim = 30;
	int w = GEngine->Width2D();
	int h = GEngine->Height2D();

// patch
	float top = _y + (_rows - 1) * _h;
	int row = _rows - 1;

  float shadowX = 0.075 * _h;
  float shadowY = 0.1 * _h;

  AutoArray< SpeakingIndication, MemAllocLocal<SpeakingIndication, 64> > speaking;
  
  // traverse all players
  const AutoArray<PlayerIdentity> *pid = GetNetworkManager().GetIdentities();
  if (pid && Glob.config.IsEnabled(DTVonID))
  {
    for (int i=0; i<pid->Size(); i++)
    {
      const PlayerIdentity &identity = pid->Get(i);
      bool isSpeaking = false;
      ChatChannel cc = CCGlobal;
      if (identity.dpnid == GetNetworkManager().GetPlayer())
      {
        isSpeaking = GetNetworkManager().IsVoiceRecording();
        // check which channel are we transmitting on
        cc = ActualChatChannel();
        if (cc==CCDirect) isSpeaking = false;
      }
      else
      {
        isSpeaking = GetNetworkManager().IsVoicePlaying(identity.dpnid);
        if (isSpeaking)
        {
          bool audible2D,audible3D;
          int channel = GetNetworkManager().CheckVoiceChannel(identity.dpnid,audible2D,audible3D);
          if (!audible2D)
          {
            isSpeaking = false;
          }
          else
          {
            // note: CheckVoiceChannel return the information as VoNChatChannel
            extern ChatChannel VoNChatChannel2ChatChannel(int channel);
            cc = VoNChatChannel2ChatChannel(channel);
          }
        }
      }
      
      if (isSpeaking)
      {
        SpeakingIndication &item = speaking.Append();
        item.name = identity.name;
        item.channel = cc;
        item.lastSpeaking = Glob.uiTime;
        _lastSpeaking = Glob.uiTime;
      }
    }
  }
  
  // merge the list of speakers into a more stable one
  for (int i=0; i<speaking.Size(); i++)
  {
    const SpeakingIndication &item = speaking[i];
    // check if the speaker is already present in the stable list
    bool alreadyThere = false;
    for (int j=0; j<_speaking.Size(); j++)
    {
      SpeakingIndication &stable = _speaking[j];
      if (!strcmp(stable.name,item.name))
      {
        stable.lastSpeaking = item.lastSpeaking;
        stable.channel = item.channel;
        alreadyThere = true;
        break;
      }
    }
    if (!alreadyThere)
    {
      _speaking.Append(item);
    }
  }
  // expire speakers which did not talk for some time
  int s,d;
  for (s=d=0; s<_speaking.Size(); s++)
  {
    if (_speaking[s].lastSpeaking>=Glob.uiTime-1.0f)
    {
      if (d!=s) _speaking[d] = _speaking[s];
      d++;
    }
  }
  _speaking.Resize(d);
  _speaking.CompactIfNeeded();

  const float border = TEXT_BORDER;
  // when we indicate somebody speaking, we use the last row for it
  // prevent the row disappearing too often by checking _lastSpeaking
  if (_speaking.Size()>0 || _lastSpeaking>Glob.uiTime-2.0f)
  {
    // display the information
    float x = _x + border;
    float lineWidth = _w - 2.0f * border;
    
    for(int i=0; i<_speaking.Size(); i++)
    {
      const SpeakingIndication &item = _speaking[i];
		  float alpha = 1;
		  PackedColor color = PackedColorRGB(_colors[item.channel], toIntFloor(_colors[item.channel].A8() * alpha));
		  PackedColor colorB = PackedColorRGB(_bgColor, toIntFloor(_bgColor.A8() * alpha));
  		PackedColor colorShadow = PackedColorRGB(_shadowColor, toIntFloor(_shadowColor.A8() * alpha));

      float left = _x + border;
		  float wline = GEngine->GetTextWidth(_size, _font, item.name) + 2.0f * border;
  		
		  // check if there is enough space to display
		  if (wline>lineWidth) break;
	    MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
	    GEngine->Draw2D(mip, colorB, Rect2DPixel((x-border) * w, top * h, wline * w, _h * h));
	    
      if(_shadow == 1)
      {
        GEngine->DrawText(
          Point2DFloat(x + shadowX, top + 0.5f * (_h - _size)+ shadowY) , _size,
          Rect2DFloat(left, top, _w + shadowX, _h + shadowX),
          _font, colorShadow, item.name, 0
          );
      }

      GEngine->DrawText(
        Point2DFloat(x, top + 0.5f * (_h - _size)), _size,
        Rect2DFloat(left, top, _w, _h),
        _font, color, item.name, _shadow
        );
	    
	    // insert one space between speaker names
	    float space = GEngine->GetTextWidth(_size, _font, " ");
	    x += wline+space;
	    lineWidth -= wline+space;
    }
    
		row--;
		if (row < 0) return;
		top -= _h;
	}

	int n = Size();
	if (n == 0) return;
	int begin = _offset;
	saturate(begin, 0, n - 1);
	for (int i=begin; i<n; i++)
	{
		const ChatItem &item = Get(i);
		if (item.text.GetLength() == 0) continue;
		if (!Enabled() && !item.forceDisplay) continue; 
    float age = multiplayer ? Glob.uiTime - item.uiTime : Glob.time - item.gameTime;
		float alpha = 1;
		if (_offset == -1 && age > startDim)
		{
			// interpolate dim
			if (age > endDim) continue;
			alpha = (endDim - age) * (1.0 / (endDim - startDim));
		}
		RString buffer;
		if (item.from.GetLength() == 0)
			buffer = item.text;
		else
			buffer = item.from + RString(": \"") + item.text + RString("\"");
		PackedColor color = PackedColorRGB(_colors[item.channel], toIntFloor(_colors[item.channel].A8() * alpha));
		PackedColor colorB = PackedColorRGB(_bgColor, toIntFloor(_bgColor.A8() * alpha));
		PackedColor colorShadow = PackedColorRGB(_shadowColor, toIntFloor(_shadowColor.A8() * alpha));
/*
		if (item.playerMsg)
		{
			// make background less intensive
			colorB = PackedColorRGB(color, color.A8()/2);
			// make foreground more intensive
			color = PackedColor(Color(0, 0, 0, alpha));
		}
*/
    // player messages indication now using the icons
    Texture *texture = NULL; //item.playerMsg ? _iconPlayerMessage : _iconNormalMessage;
    if(item.playerMsg)
    {
      colorB = color;
      color = PackedWhite; 
    }

    float texW = 0;
    if (texture && texture->AHeight() > 0)
    {
      // keep aspect of the texture source
      texW = _h * (texture->AWidth() * h) / (texture->AHeight() * w);
    }

		AUTO_STATIC_ARRAY(int, lines, 32);
		lines.Add(0);

    float left = _x + border;
    float lineWidth = _w - 2.0f * border;

		if (texW + GEngine->GetTextWidth(_size, _font, buffer) > lineWidth)
		{
			const char *p = buffer;
			const char *word = NULL;
			float width = texW;
			while (*p != 0)
			{
				const char *pLast = p;
				char c = *p++;
				if ((unsigned char)c <= 32)
				{
					word = p;
				}

        // read the single character in UTF-8 encoding
        char temp[7];
        int j = 0;
        temp[j++] = c;
        if (c & 0x80)
        {
          DoAssert((c & 0xc0) == 0xc0); // the first character
          while (((c = *p) & 0xc0) == 0x80)
          {
            p++;
            if (j < 5) temp[j++] = c;
            else
            {
              Fail("Character too long");
            }
          };
        }
        temp[j] = 0;
        width += GEngine->GetTextWidth(_size, _font, temp);

				if (width > lineWidth)
				{
					if (word) p = word;
					else p = pLast;
					lines.Add(p - buffer);
					word = NULL;
					width = 0;
				}
			}
		}

		for (int i=lines.Size()-1; i>=0; i--)
		{
			char *line = buffer.MutableData() + lines[i];

			float wline = GEngine->GetTextWidth(_size, _font, line) + 2.0f * border;
      if (i == 0) wline += texW;
			MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
			GEngine->Draw2D(mip, colorB, Rect2DPixel(_x * w, top * h, wline * w, _h * h));
      if (i == 0 && texW > 0)
      {
        MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
        GEngine->Draw2D(mip, color, Rect2DPixel(left * w, top * h, texW * w, _h * h));
        left += texW;
      }

      if(_shadow == 1)
      {
        GEngine->DrawText(
          Point2DFloat(left + shadowX, top + 0.5f * (_h - _size)+ shadowY) , _size,
          Rect2DFloat(_x, top, _w + shadowX, _h + shadowX),
          _font, colorShadow, line, 0
          );
      }
			GEngine->DrawText
			(
				Point2DFloat(left, top + 0.5 * (_h - _size)), _size,
				Rect2DFloat(_x, top, _w, _h),
				_font, color, line, _shadow
			);
			*line = 0;

			row--;
			if (row < 0) return;
			top -= _h;
		}
	}
}

class WhatUnitsCrewFunc : public ICrewFunc
{
protected:
  RefArray<NetworkObject> &_units;

public:
  WhatUnitsCrewFunc(RefArray<NetworkObject> &units) : _units(units) {}

  virtual bool operator () (Person *person)
  {
    if (person && person->IsRemotePlayer() && person->Brain() && person->Brain()->GetLifeState() != LifeStateDead)
      _units.Add(person);
    return false; // continue
  }
};

static void WhatUnitsVehicle(RefArray<NetworkObject> &units, Transport *veh)
{
  WhatUnitsCrewFunc func(units);
  veh->ForEachCrew(func);
}

static void WhatUnitsGroup(RefArray<NetworkObject> &units, AIGroup *grp)
{
	for (int i=0; i<grp->NUnits(); i++)
	{
		AIUnit *unit = grp->GetUnit(i);
		if (!unit) continue;
    if (unit->GetLifeState() == LifeStateDead) continue;
		Person *person = unit->GetPerson();
		if (person && person->IsRemotePlayer()) units.Add(person);
	}
}

static void WhatUnitsSide(RefArray<NetworkObject> &units, AICenter *center)
{
	for (int j=0; j<center->NGroups(); j++)
	{
		AIGroup *grp = center->GetGroup(j);
		if (!grp) continue;
		for (int i=0; i<grp->NUnits(); i++)
		{
			AIUnit *unit = grp->GetUnit(i);
			if (!unit) continue;
      if (unit->GetLifeState() == LifeStateDead) continue;
			Person *person = unit->GetPerson();
			if (person && person->IsRemotePlayer()) units.Add(person);
		}
	}
}

static void WhatUnitsCommand(RefArray<NetworkObject> &units, AICenter *center)
{
  for (int j=0; j<center->NGroups(); j++)
  {
    AIGroup *grp = center->GetGroup(j);
    if (!grp) continue;
    AIUnit *unit = grp->Leader();
    if (!unit) continue;
    if (unit->GetLifeState() == LifeStateDead) continue;
    Person *person = unit->GetPerson();
    if (person && person->IsRemotePlayer()) units.Add(person);
  }
}

/*
static void WhatUnitsAll(RefArray<NetworkObject> &units)
{
	AICenter *center = GWorld->GetEastCenter();
	if (center) WhatUnitsSide(units, center);
	center = GWorld->GetWestCenter();
	if (center) WhatUnitsSide(units, center);
	center = GWorld->GetGuerrilaCenter();
	if (center) WhatUnitsSide(units, center);
	center = GWorld->GetCivilianCenter();
	if (center) WhatUnitsSide(units, center);
}
*/

//! Retrieves list of players, which can receive given message
/*!
	\param units retrieved list of units
	\param channel channel on which communication runs
	\param object channel's owner 
*/
void WhatUnits(RefArray<NetworkObject> &units,  ChatChannel channel, NetworkObject *object)
{
	switch (channel)
	{
		case CCVehicle:
			{
				Transport *veh = dynamic_cast<Transport *>(object);
				if (veh) WhatUnitsVehicle(units, veh);
			}
			break;
		case CCGroup:
			{
				AIGroup *grp = dynamic_cast<AIGroup *>(object);
				if (grp) WhatUnitsGroup(units, grp);
			}
			break;
		case CCSide:
			{
				AICenter *center = dynamic_cast<AICenter *>(object);
				if (center) WhatUnitsSide(units, center);
			}
			break;
    case CCCommand:
      {
        AICenter *center = dynamic_cast<AICenter *>(object);
        if (center) WhatUnitsCommand(units, center);
      }
      break;
		case CCGlobal:
/*
			WhatUnitsAll(units);
*/
			break;
    case CCDirect:
#if _ENABLE_DIRECT_MESSAGES
    case CCDirectSpeaking:
#endif
      {
        Person *person = dynamic_cast<Person *>(object);
        if (person) WhatUnitsDirect(units, person);
      }
      break;
		default:
			ErrF("Bad radio channel %d (for object %s)",toInt(channel),cc_cast(object->GetDebugName()));
			break;
	}
}

//! Retrieves list of players, which can receive given message
/*!
	\param unit sender unit
	\param units retrieved list of units
	\param channel channel on which communication runs
*/
void WhatUnits(AIBrain *unit, RefArray<NetworkObject> &units, ChatChannel channel)
{
	switch (channel)
	{
		case CCVehicle:
			{
				Transport *veh = unit->GetVehicleIn();
				if (veh) WhatUnitsVehicle(units, veh);
			}
			break;
		case CCGroup:
			{
				AIGroup *grp = unit->GetGroup();
				if (grp) WhatUnitsGroup(units, grp);
			}
			break;
		case CCSide:
			{
				AIGroup *grp = unit->GetGroup();
				if (!grp) break;
				AICenter *center = grp->GetCenter();
				if (center) WhatUnitsSide(units, center);
			}
			break;
    case CCCommand:
      {
        AIGroup *grp = unit->GetGroup();
        if (!grp) break;
        AICenter *center = grp->GetCenter();
        if (center) WhatUnitsCommand(units, center);
      }
      break;
		case CCGlobal:
			break;
		case CCDirect:
#if _ENABLE_DIRECT_MESSAGES
    case CCDirectSpeaking:
#endif
			{
        Person *person = unit->GetPerson();
        if (person) WhatUnitsDirect(units, person);
      }
			break;
		case CCNone:
			break;
		default:
			ErrF("Bad radio channel %d (for unit %s)",toInt(channel), cc_cast(unit->GetDebugName()));
			break;
	}
}

//! Retrieves list of players, which can receive given message
/*!
	Assume player is a message sender.
	\param units retrieved list of units
	\param channel channel on which communication runs
*/
static void WhatUnits(RefArray<NetworkObject> &units, ChatChannel channel)
{
	Person *person = GWorld->GetRealPlayer();
	if (!person) return;
	AIBrain *unit = person->Brain();
	if (!unit) return;

	WhatUnits(unit, units, channel);
}

//! Sends chat message by player
/*!
	\param channel channel on which communication runs
	\text message text
*/
void SendChat(ChatChannel channel, RString text)
{
	if (text.GetLength() > 0 && text[0] == '#')
	{
		GChatList.Add(channel, "", text, false, true);
		GetNetworkManager().ProcessCommand(text);
		return;
	}

	AIBrain *sender = GWorld->GetRealPlayer() ? GWorld->GetRealPlayer()->Brain() : NULL; 

  if (
    GetNetworkManager().GetClientState() != NCSBriefingShown &&
    GetNetworkManager().GetClientState() != NCSBriefingRead ||
    !sender || sender->GetLifeState()==LifeStateDead
  )
	{
		// use playerRoles info
		GetNetworkManager().Chat(channel, text);
		GChatList.Add(channel, GetFullPlayerName(), text, false, true);
	}
	else
	{
		// send actual state
		RefArray<NetworkObject> units;
		WhatUnits(units, channel);
		if (channel == CCGlobal || units.Size() > 0)
			GetNetworkManager().Chat(channel, sender, units, text);

		GChatList.Add(channel, sender, text, false, true);
	}
}

//! Sends mission radio message
/*!
	\param channel channel on which communication runs
	\param object channel's owner 
	\param wave name of class with message description
	\param sender sender unit
	\param senderName sender name
*/
void SendRadioChatWave
(
	ChatChannel channel,
	NetworkObject *object,
	RString wave,
	AIBrain *sender, RString senderName
)
{
	if (GetNetworkManager().GetClientState() != NCSBriefingRead) return;
	RefArray<NetworkObject> units;
	WhatUnits(units, channel, object);
	if (channel == CCGlobal || units.Size() > 0)
		GetNetworkManager().RadioChatWave(channel, units, wave, sender, senderName);
}

//! Sends mission radio message
/*!
	\param channel channel on which communication runs
	\param wave name of class with message description
	\param sender sender unit
	\param senderName sender name
*/
void SendRadioChatWave
(
	ChatChannel channel,
	RString wave,
	AIBrain *sender, RString senderName
)
{
	if (GetNetworkManager().GetClientState() != NCSBriefingRead) return;
	RefArray<NetworkObject> units;
	WhatUnits(sender, units, channel);
	if (channel == CCGlobal || units.Size() > 0)
		GetNetworkManager().RadioChatWave(channel, units, wave, sender, senderName);
}

//! Sends radio message
/*!
	\param channel channel on which communication runs
	\param sender sender unit
	\param object channel's owner 
	\param text text of message
	\param sentence radio sentence of message
*/
void SendRadioChat
(
	ChatChannel channel, AIBrain *sender,
	NetworkObject *object,
	RString text, RadioSentence &sentence
)
{
	if (GetNetworkManager().GetClientState() != NCSBriefingRead) return;
	RefArray<NetworkObject> units;
	WhatUnits(units, channel, object);
	if (channel == CCGlobal || units.Size() > 0)
		GetNetworkManager().RadioChat(channel, sender, units, text, sentence);
}

//! Sends marker
/*!
	\param channel channel on which communication runs
	\param sender sender unit
	\param marker marker description
*/
void SendMarker
(
	ChatChannel channel, AIBrain *sender, ArcadeMarkerInfo &marker
)
{
	if (GetNetworkManager().GetClientState() < NCSGameLoaded) return;
	RefArray<NetworkObject> units;
	WhatUnits(units, channel);
  GetNetworkManager().MarkerCreate(channel, sender, units, marker);
}

//! Channel selection display
class DisplayChannel : public Display
{
public:
	//! actual selected channel
	static int _channel;
  //! backup of selected channel (forcing Push to Talk Channel actions to change _channel only temporary)
  static int _backupChannel;
  //! list of disabled channels (configured by disableChannels entry in mission descriptionExt)
  static AutoArray<int> _disabledChannels;

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayChannel(ControlsContainer *parent);
	void OnButtonClicked(int idc) {}
	void ResetHUD();
};

int DisplayChannel::_channel = CCGlobal;
int DisplayChannel::_backupChannel = CCGlobal;
AutoArray<int> DisplayChannel::_disabledChannels;

//! Creates channel selection display
AbstractOptionsUI *CreateChannelUI()
{
	return new DisplayChannel(NULL);
}

//! returns currently selected chat channel
ChatChannel ActualChatChannel()
{
	return (ChatChannel)DisplayChannel::_channel;
}

//! Backup chat channel
void BackupChatChannel()
{
  DisplayChannel::_backupChannel = DisplayChannel::_channel;
}

//! Restore chat channel
void RestoreChatChannel()
{
  if (DisplayChannel::_channel != DisplayChannel::_backupChannel)
  {
    DisplayChannel::_channel = DisplayChannel::_backupChannel;
#ifndef _XBOX
    GWorld->OnChannelChanged();
#endif
  }
}

bool IsPlayerDead();

static bool ChannelValid(int channel)
{
  // group channel is always valid
  if (channel==CCGroup) return true;
  if (channel>=CCSystem) return false;
  if (channel==CCGlobal)
  {
    // global channel is always valid for the admin
    if (GetNetworkManager().IsServer() || GetNetworkManager().IsGameMaster()) return true;
  }
  if (!IsPlayerDead())
  {
    NetworkClientState state = GetNetworkManager().GetClientState();
    if (state>=NCSBriefingShown && state<=NCSBriefingRead)
    {
      // for ingame player any channel is always valid
      // TODO: sometimes we may want to disable group or vehicle channels?
      for (int i=0; i<DisplayChannel::_disabledChannels.Size(); i++)
      {
        if (DisplayChannel::_disabledChannels[i]==channel) return false; //channel disabled by mission descriptionExt config
      }
      return true;
    }
  }
  return false;
}

//! Select next chat channel
void NextChatChannel()
{
  do 
  {
	  DisplayChannel::_channel++;
	  if (DisplayChannel::_channel >= CCN) DisplayChannel::_channel = 0;
	  // we need to validate if we are allowed to use the channel
	  // if not, we need to switch to another one
  } while(!ChannelValid(DisplayChannel::_channel));
}

//! Select previous chat channel
void PrevChatChannel()
{
  do 
  {
	  DisplayChannel::_channel--;
  	if (DisplayChannel::_channel < 0) DisplayChannel::_channel = CCN - 1;
	  // we need to validate if we are allowed to use the channel
	  // if not, we need to switch to another one
  } while(!ChannelValid(DisplayChannel::_channel));
}

//! Select chat channel
void SetChatChannel(ChatChannel channel)
{
  DisplayChannel::_channel = channel;
  if ( !ChannelValid(channel) ) NextChatChannel();
}

void ResetDisabledChannels()
{
  DisplayChannel::_disabledChannels.Clear();
}

void DisableChannel(int chan)
{
  DisplayChannel::_disabledChannels.Add(chan);
}

bool ChannelDisabled(int chan)
{
  for (int i=0; i<DisplayChannel::_disabledChannels.Size(); i++)
  {
    if (DisplayChannel::_disabledChannels[i]==chan) return true; //channel disabled by mission descriptionExt config
  }
  return false;
}

DisplayChannel::DisplayChannel(ControlsContainer *parent)
:Display(parent)
{
	Load("RscDisplayChannel");
	SetCursor(NULL);
	ResetHUD();
}

void DisplayChannel::ResetHUD()
{
#ifdef _XBOX
  int channel = GetNetworkManager().GetVoiceChannel();
  if (channel < 0) return;
#else
  int channel = _channel;
#endif

	CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_CHANNEL));
	if (text)
	{
		text->SetText(LocalizeString(IDS_CHANNEL_GLOBAL + channel));
		text->SetTextColor(GChatList.GetColor(channel));
	}

  text = dynamic_cast<CStatic *>(GetCtrl(IDC_CHANNEL_VOICE));
  if (text)
  {
    text->SetTextColor(GChatList.GetColor(channel));
  }
}

//! Chat line display
/*!
	Enables write chat messages
*/
class DisplayChatLine : public Display
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayChatLine(ControlsContainer *parent);
  ~DisplayChatLine();

	void OnButtonClicked(int idc);
	bool OnKeyDown(int dikCode);
	bool OnKeyUp(int dikCode);
	void DestroyHUD(int exit);
};

/*!
\patch 5139 Date 3/14/2007 by Ondra
- Fixed: All input ignored after MP session left while chat window still open.
*/
DisplayChatLine::DisplayChatLine(ControlsContainer *parent)
:Display(parent)
{
  GInput.ChangeGameFocus(+1); //call it as early as possible in order not to skip it by some possible return
	Load("RscDisplayChat");
	SetCursor(NULL);
	// make sure chat in a screen which disabled display does not enable it
  _enableDisplay = _enableDisplay && (!parent || parent->DisplayEnabled());

  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_CHAT));
  if (edit)
  {
    /// Limit for the chat text (avoid too long messages)
    edit->SetMaxChars(MaxChatLength);
  }
}

DisplayChatLine::~DisplayChatLine()
{
  GInput.ChangeGameFocus(-1);
}

void DisplayChatLine::OnButtonClicked(int idc)
{
	if (idc == IDC_OK)
	{
		CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_CHAT));
		if (edit)
		{
			SendChat(ActualChatChannel(), edit->GetText());
		}
	}
	Display::OnButtonClicked(idc);
}

//! Global chat history
ChatList GChatList;

/*!
\patch 1.42 Date 1/10/2002 by Jirka
- Added: browsing in chat history using Page Up and Page Down in chat line 
*/

bool DisplayChatLine::OnKeyDown(int dikCode)
{
	// catch RETURN, ESC, UP, DOWN, PAGE UP, PAGE DOWN (avoid transmit into game)
	if (dikCode == DIK_ESCAPE || dikCode == DIK_RETURN || dikCode == DIK_NUMPADENTER ||
		dikCode == DIK_UP || dikCode == DIK_DOWN ||
    dikCode == DIK_PRIOR || dikCode == DIK_NEXT) return true;

	return Display::OnKeyDown(dikCode);
}

bool DisplayChatLine::OnKeyUp(int dikCode)
{
	if (dikCode == DIK_RETURN || dikCode == DIK_NUMPADENTER)
	{
		OnButtonClicked(IDC_OK);
		return true;
	}
	else if (dikCode == DIK_ESCAPE)
	{
		OnButtonClicked(IDC_CANCEL);
		return true;
	}
	else if (dikCode == DIK_DOWN)
	{
		PrevChatChannel();
		if (GWorld->ChatChannel()) GWorld->ChatChannel()->ResetHUD();
		if (GWorld->VoiceChat()) GWorld->VoiceChat()->ResetHUD();
#ifndef _XBOX
		GWorld->OnChannelChanged();
#endif
		return true;
	}
	else if (dikCode == DIK_UP)
	{
		NextChatChannel();
		if (GWorld->ChatChannel()) GWorld->ChatChannel()->ResetHUD();
		if (GWorld->VoiceChat()) GWorld->VoiceChat()->ResetHUD();
#ifndef _XBOX
		GWorld->OnChannelChanged();
#endif
		return true;
	}
	else if (dikCode == DIK_PRIOR)
	{
		GChatList.BrowseUp();
		return true;
	}
	else if (dikCode == DIK_NEXT)
	{
		GChatList.BrowseDown();
		return true;
	}

	return Display::OnKeyUp(dikCode);
}

void DisplayChatLine::DestroyHUD(int exit)
{
	GWorld->DestroyChat(exit);
	GChatList.BrowseReset();
}

//! Creates chat line display
AbstractOptionsUI *CreateChatUI()
{
	return new DisplayChatLine(NULL);
}

//! Voice chat indicator display
/*!
	Display picture when voice recording is enabled.
	Color of picture indicates channel on which transfer runs.
*/
class DisplayVoiceChat : public Display
{
public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayVoiceChat(ControlsContainer *parent);
	bool OnKeyDown(int dikCode);
	bool OnKeyUp(int dikCode);
	void OnSimulate(EntityAI *vehicle);
	void DestroyHUD(int exit);

	void ResetHUD();
};

DisplayVoiceChat::DisplayVoiceChat(ControlsContainer *parent)
:Display(parent)
{
	Load("RscDisplayVoiceChat");
	SetCursor(NULL);

	ResetHUD();
}

bool DisplayVoiceChat::OnKeyDown(int dikCode)
{
	return false;
}

bool DisplayVoiceChat::OnKeyUp(int dikCode)
{
	return false;
}

/*!
\patch 5112 Date 1/3/2007 by Bebul
- New: Push to Talk actions for voice over net
*/
void DisplayVoiceChat::OnSimulate(EntityAI *vehicle)
{
	if (GInput.GetActionToDo(UAVoiceOverNet, true, false))
	{
    // just disable voice recording from microphone and request disabling of voice transmission
    // we can't disable transmission right now, because we have to wait some time for messages to be sent
    GetNetworkManager().SetVoiceCapture(false);
    GetNetworkManager().SetDisableVoiceRequest(INetworkManager::DVRDisableVoiceToggle);
    GInput.VoNToggleOn = false;
    GetNetworkManager().SetVoiceToggleOn(false);
    Exit(IDC_CANCEL);
	}
  else if ( !GInput.VoNToggleOn ) 
  {
    if (
      !GInput.GetAction(UAPushToTalk) && 
      !GInput.GetAction(UAPushToTalkAll) && 
      !GInput.GetAction(UAPushToTalkSide) && 
      !GInput.GetAction(UAPushToTalkCommand) && 
      !GInput.GetAction(UAPushToTalkGroup) && 
      !GInput.GetAction(UAPushToTalkVehicle) && 
      !GInput.GetAction(UAPushToTalkDirect)
    )
    {
      RestoreChatChannel();

      // just disable voice recording from microphone and request disabling of voice transmission
      // we can't disable transmission right now, because we have to wait some time for messages to be sent
      GetNetworkManager().SetVoiceCapture(false);
      GetNetworkManager().SetDisableVoiceRequest(INetworkManager::DVRDisableVoice);
      Exit(IDC_CANCEL);
    }
  }
}

void DisplayVoiceChat::ResetHUD()
{
	ChatChannel channel = ActualChatChannel();
  GetNetworkManager().SetVoiceChannel(channel);

	CStatic *text = dynamic_cast<CStatic *>(GetCtrl(IDC_VOICE_CHAT));
	if (text)
	{
//		text->SetText(LocalizeString(IDS_CHANNEL_GLOBAL + channel));
		text->SetTextColor(GChatList.GetColor(channel));
	}
}

void DisplayVoiceChat::DestroyHUD(int exit)
{
	GWorld->DestroyVoiceChat(exit);
}

AbstractOptionsUI *CreateVoiceChatUI()
{
	return new DisplayVoiceChat(NULL);
}
