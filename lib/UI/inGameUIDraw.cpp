// Poseidon - HUD UI drawing

#include "../wpch.hpp"
#include "../world.hpp"
#include "../AI/ai.hpp"
#include "../keyInput.hpp"
#include "../joystick.hpp"
#include "InGameUIImpl.hpp"
#include "../engine.hpp"
#include "../camera.hpp"
#include "../visibility.hpp"
#include "../landscape.hpp"
#include "../txtPreload.hpp"
#include "../house.hpp"
#include "../diagModes.hpp"
#include "../Network/network.hpp"
#include <Es/Strings/bString.hpp>

#include "resincl.hpp"

#include "../move_actions.hpp"

#include "../stringtableExt.hpp"

#include "../drawText.hpp"
#include "chat.hpp"

#include "uiViewport.hpp"

#include "../helicopter.hpp"

#define INV_H_PI (1/H_PI)

#if _ENABLE_CHEATS
static bool tdCheat;
#endif

struct DrawActionInfo
{
  RefR<INode> text;
  bool selected;
};
TypeIsMovableZeroed(DrawActionInfo);

#if _ENABLE_CHEATS && _PROFILE
# pragma optimize("",off)
#endif

/*!
\patch 5153 Date 4/3/2007 by Jirka
- Fixed: Actions menu - drawing of arrows when scrolling is enabled
*/

void UIActionsDrawable::OnDraw()
{
  if (Size() == 0) return;  // ??? draw NO_ACTION ???

  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return;

  int selected = FindSelected();
  if (selected < 0) return;

  float alpha = 1.0f;
  if (_default)
  {
    if (!_selected) return;
  }
  else
  {
    alpha = GetAlpha();
    if (alpha <= 0.01)
    {
      _selected = NULL;
      _selectedPriority = -FLT_MIN;
      return;
    }
  }

  if (!_selected)
  {
    _selected = Get(selected).action;
    _selectedPriority = Get(selected).priority;
  }
  Assert(_selected);

  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();
  const float border = TEXT_BORDER;

  UIViewport *vp = Create2DViewport();
  

  //////////////////////////////////////////////////////////////////////////
  //
  // Default action
  //
  {
    PackedColor bgColorA = PackedColorRGB(_bgColor, toIntFloor(_bgColor.A8() * alpha));

    // calculate position on the screen
    float x = _xOffset;
    float y = _yOffset;

    float cx = 0, cy = 0;
    if (_relativeToCursor > 0 && !agent->GetVehicleIn())
    {
      Vector3 dir = GWorld->UI()->GetCursorDirection();
      const Camera *camera = GScene->GetCamera();
      Matrix4Val camInvTransform = camera->GetInvTransform();
      Vector3Val camDir = camInvTransform.Rotate(dir);

      if (camDir.Z() >= 1e-6)
      {//disabled for vehicles because f.e. when in F-35, cursor went out of screen
        float invZ = 1.0 / camDir.Z();
        cx = camDir.X() * invZ * camera->InvLeft();
        cy = - camDir.Y() * invZ * camera->InvTop();
      }
    }

    float mScrX = cx * 0.5 + 0.5;
    float mScrY = cy * 0.5 + 0.5;

    // FIX: convert from 2D space to 3D, see InGameUI::DrawTargetInfo
    mScrX *= (float)GEngine->WidthBB() / w;
    mScrY *= (float)GEngine->HeightBB() / h;

    x += _relativeToCursor * mScrX;
    y += _relativeToCursor * mScrY;

    // create structured texts
    INode *FormatText(const char *format, RefR<INode> *args, int nArgs, bool structured);
    INode *GetActionKeysImages(UserAction action, int maxKeys);

    RefR<INode> hint;
    if (_showHint)
    {
      INode *GetKeyImageOrName(INode *parent, int dikKey);

      RefR<INode> image;
      if (Get(selected).shortcut >= 0 && GInput.GetUserKeys(Get(selected).shortcut).Size() > 0)
      {
        int key = GInput.GetUserKeys(Get(selected).shortcut)[0];
        image = GetKeyImageOrName(NULL, key);
      }
      else
      {
        image = GetActionKeysImages(UAAction, 1);
      }
      if (Get(selected).highlight)
      {
        const float period = 0.5f;
        const float speed = H_PI / period;
        float alpha = 0.5 * sin(speed * Glob.uiTime.toFloat()) + 0.5;
        SetTextColor(image, PackedColor(Color(1, 1, 1, alpha)));
        SetTextShadowColor(image, PackedColor(Color(0, 0, 0, alpha)));
      }

      RefR<INode> params[2];
      params[0] = image;
      params[1] = _selected->GetTextDefault(agent);
      if (!params[1])
        params[1] = CreateTextASCII(NULL, RString());
      hint = FormatText(LocalizeString(IDS_DISP_XBOX_HINT_IGUI_ACTION), params, 2, false);
    }
    else
    {
      hint = _selected->GetTextDefault(agent);
      if (!hint)
        hint = CreateTextASCII(NULL, RString());
    }
    SetTextFont(hint, _defaultFont);
    SetTextColor(hint, _defaultColor);
    SetShadow(hint, _defaultShadow);

    RefR<INode> imageNext;
    RefR<INode> hintNext;
    bool next = _showNext && Size() > 1;
    if (next)
    {
      imageNext = GetActionKeysImages(UANextAction, 1);
      hintNext = _default ?
        FormatText(LocalizeString(IDS_DISP_XBOX_HINT_IGUI_MORE_ACTIONS), &imageNext, 1, false) :
      FormatText(LocalizeString(IDS_DISP_XBOX_HINT_IGUI_NEXT_ACTION), &imageNext, 1, false);
      SetTextFont(hintNext, _nextFont);
      SetShadow(hintNext, _defaultShadow);
      SetShadow(imageNext, _defaultShadow);
    }

    // check if text can be drawn immediately
    bool ready = IsTextReadyToDraw(hint, _defaultSize);
    if (next && !IsTextReadyToDraw(hintNext, _nextSize)) ready = false;
    if (ready) 
    {

      // calculate width
      float width = 0;
      saturateMax(width, GetTextWidthFloat(hint, _defaultSize));
      if (next) saturateMax(width, GetTextWidthFloat(hintNext, _nextSize));

      float textHeight = GetTextHeight(hint, Rect2DFloat(0, 0, 2000, 2000), _defaultSize);
      float textHeightNext = 0;
      if (next) textHeightNext = GetTextHeight(hintNext, Rect2DFloat(0, 0, 2000, 2000), _nextSize);

      width += 2 * border;
      float height = textHeight + 2 * border;
      if (next) height += textHeightNext;

      float left = x - _xHotspot * width;
      float top = y - _yHotspot * height;

      // draw
      if (_showLine)
      {
        // line pointing to target
        const Camera *camera = GScene ? GScene->GetCamera() : NULL;
        TargetType *target = _selected->GetTarget(); 
        if (camera && target)
        {
          Matrix4Val camInvTransform = camera->GetInvTransform();
          Vector3 worldPos = Get(selected).position;
          bool worldPosValid = worldPos[0] != -FLT_MAX;
          if (target != agent->GetPerson() && target != agent->GetVehicle() || worldPosValid)
          {
            if (!worldPosValid) worldPos = target->CameraPosition();
            Vector3 pos = camInvTransform.FastTransform(worldPos);
            if (pos.Z() >= 1e-6)
            {
              // position of target
              float invZ = 1.0 / pos.Z();
              float targetX = pos.X() * invZ * camera->InvLeft();
              float targetY = -pos.Y() * invZ * camera->InvTop();
              float targetScreenX = toInt(GEngine->WidthBB() * (0.5 + 0.5 * targetX));
              float targetScreenY = toInt(GEngine->HeightBB() * (0.5 + 0.5 * targetY));

              // position of menu
              float menuScreenX = toInt(w * x);
              float menuScreenY = toInt(h * y);

              Line2DAbs line;
              line.beg = Point2DAbs(menuScreenX, menuScreenY);
              line.end = Point2DAbs(targetScreenX, targetScreenY);
              Line2DAbs lineShadow = line;
              lineShadow.beg.x++;
              lineShadow.beg.y++;
              lineShadow.end.x++;
              lineShadow.end.y++;
              /*
              PackedColor color = PackedColorRGB(PackedWhite, alpha);
              PackedColor colorShadow = PackedColorRGB(PackedBlack, alpha);
              */
              PackedColor color = PackedWhite;
              PackedColor colorShadow = PackedBlack;
              GEngine->DrawLine(lineShadow, colorShadow, colorShadow);
              GEngine->DrawLine(line, color, color);
            }
          }
        }
      }

      // FIX: hladas 19.6.2008 - parameter left (in rectagle in drawText) is not from 0 to 1 but from 0 to GEngine->WidthBB() / w
      // therefore center is not in 0.5,0.5; centerCorrectionX,Y  contain corrernction 
      float centerCorrectionX = 0.5f-(0.5*(float)GEngine->WidthBB() / w);
      float centerCorrectionY = 0.5f-(0.5*(float)GEngine->HeightBB() / h);

      // default action frame
      GEngine->DrawFrame(
        _defaultBackground,
        bgColorA,
        Rect2DPixel(left * w, top * h, width * w, height * h));
      // current action

      DrawText(vp, hint, Rect2DFloat(centerCorrectionX + left + border,centerCorrectionY + top + border, width, textHeight), _defaultSize, alpha,NULL, false);
      top += textHeight;
      // next action hint
      if (next) DrawText(vp, hintNext, Rect2DFloat(centerCorrectionX + left + border,centerCorrectionY + top + border, width, textHeightNext), _nextSize, alpha,NULL, false);
    }
  }
  //////////////////////////////////////////////////////////////////////////
  //
  // Actions menu
  //

  if (!_default)
  {
    float alpha = GetAlpha();
    if (alpha <= 0.01) return;

    PackedColor selBgColorA = PackedColorRGB(_selBgColor, toIntFloor(_selBgColor.A8() * alpha));
    PackedColor bgColorA = PackedColorRGB(_bgColor, toIntFloor(_bgColor.A8() * alpha));
    PackedColor selColorA = PackedColorRGB(_selColor, toIntFloor(_selColor.A8() * alpha));
    PackedColor textColorA = PackedColorRGB(_textColor, toIntFloor(_textColor.A8() * alpha));

    // skip disabled actions
    int n = 0;  // number of enabled actions
    int sel = 0; // index of selected action in enabled actions
    for (int i=0; i<Size(); i++)
    {
      if (Get(i).show)
      {
        if (i == selected) sel = n;
        n++;
      }
    }
    if (n == 0) return;

    int totalShown = n;
    saturate(n, 1, _rows);

    AUTO_STATIC_ARRAY(DrawActionInfo, array, 16);
    array.Resize(n);

    int start = sel - n + 1; // offset in enabled actions
    saturateMax(start, 0);
    bool canUp = start > 0;
    bool canDown = totalShown > start + _rows;
    int end = start + n;
    int index = 0;
    for (int i=0; i<Size(); i++)
    {
      const UIAction &action = Get(i);
      if (action.show)
      {
        if (start <= 0)
        {
          array[index].text = action.action->GetText(agent);
          if (!array[index].text)
            array[index].text = CreateTextASCII(NULL, RString());
          SetTextFont(array[index].text, _font);
          array[index].selected = i == selected;
          index++;
        }
        start--;
        end--;
        if (end <= 0) break;
      }
    }

    float width = 0;
    float height = 0;
    for (int i=0; i<n; i++)
    {
      saturateMax(width, GetTextWidthFloat(array[i].text, _size));
      height += GetTextHeight(array[i].text, Rect2DFloat(0, 0, 2000, 2000), _size); 
    }
    width += 2 * border;
    if (canUp || canDown) width += border + _arrowWidth;
    height += 2 * border;

    float left;
    switch (_align & ST_HPOS)
    {
    case ST_LEFT:
    default:
      saturateMin(width, 1 - _x);
      left = _x;
      break;
    case ST_RIGHT:
      saturateMin(width, _x);
      left = _x - width;
      break;
    case ST_CENTER:
      saturateMin(width, 1);
      left = _x - 0.5f * width;
      break;
    }
    float right = left + width;

    float top;
    switch (_align & ST_HPOS)
    {
    case ST_DOWN:
      top = _y - height;
      break;
    case ST_UP:
    default:
      top = _y;
      break;
    case ST_VCENTER:
      top = _y - 0.5 * height;
      break;
    }
    float bottom = _y + height;

    GEngine->DrawFrame(
      _background,
      bgColorA,
      Rect2DPixel(left * w, top * h, width * w, height * h)
      );
    top += border;
    bottom -= border;

    // scrolling arrows
    if (canUp)
    {
      MipInfo mip = GEngine->TextBank()->UseMipmap(_iconArrowUp, 0, 0);
      GEngine->Draw2D(mip, PackedWhite, Rect2DPixel(CX(right - border - _arrowWidth), CY(top), CW(_arrowWidth), CH(_arrowHeight)));
    }
    if (canDown)
    {
      MipInfo mip = GEngine->TextBank()->UseMipmap(_iconArrowDown, 0, 0);
      GEngine->Draw2D(mip, PackedWhite, Rect2DPixel(CX(right - border - _arrowWidth), CY(bottom - _arrowHeight), CW(_arrowWidth), CH(_arrowHeight)));
    }

    Rect2DFloat clipRect(left + border, 0, width - 2 * border, 1);
    for (int i=0; i<n; i++)
    {
      PackedColor color(array[i].selected ? selColorA : textColorA);
      SetTextColor(array[i].text, color);
      SetShadow(array[i].text,_shadow);
      float textHeight = GetTextHeight(array[i].text, Rect2DFloat(0, 0, 2000, 2000), _size);
      if (array[i].selected)
      {
        // selection background
        MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
        GEngine->Draw2D(
          mip, selBgColorA,
          Rect2DPixel((left + border) * w, top * h, (width - 2 * border) * w, textHeight * h),Rect2DClipPixel, _shadow
          );
      }
      DrawText(vp, array[i].text, Rect2DFloat(left + border, top, width, textHeight), _size, alpha, NULL, false );
      top += textHeight;
      if (_underlineSelected && array[i].selected)
      {
        float wT = GetTextWidthFloat(array[i].text, _size);
        saturateMin(wT, width - 2 * border);
        GEngine->DrawLine(
          Line2DPixel(CX(left + border), CY(top),
          CX(left + border + wT), CY(top)),
          color, color
          );
      }
    }
  }
}

PackedColor InGameUI::ColorFromHit(float hit)
{
  const float halfValue = 0.4;
  const float minValue = 0.1;
  const float fullValue = 0.9;

  if (hit>=fullValue) return tankColorFullDammage;
  if (hit<=minValue) return tankColor;

  if (hit<halfValue)
  {
    float f = (hit-minValue)/(halfValue-minValue);
    Color c = Color((ColorVal)tankColor)*(1-f)+Color((ColorVal)tankColorHalfDammage)*f;
    return PackedColor(c);
  }
  else
  {
    float f = (hit-halfValue)/(fullValue-halfValue);
    Color c = Color((ColorVal)tankColorHalfDammage)*(1-f)+Color((ColorVal)tankColorFullDammage)*f;
    return PackedColor(c);
  }
}

void InGameUI::PreloadTankDirection()
{
  Texture *textures[] = {_imageHull, _imageEngine, _imageLTrack, _imageRTrack, _imageTurret, _imageGun, _imageObsTurret};
  int n = sizeof(textures) / sizeof(*textures);
  for (int i=0; i<n; i++)
  {
    Texture *texture = textures[i];
    if (!texture) continue;
    if (!texture->HeadersReady()) continue;
    GEngine->TextBank()->UseMipmap(texture, 1, 0);
  }
}

/*!
\patch 1.30 Date 11/20/2001 by Ondra.
- Fixed: Left and right tank track dammage display was swapped.
*/

void InGameUI::DrawTankDirection(const Camera &camera)
{
  if (_tankPos >= 1) return;

  tankShadow = 2;
/*
  if (veh->NMagazineSlots() <= 0)
    return;

  float tankLeft = tankX + (1 - tankX) * _tankPos;
*/
  AIBrain *unit = GWorld->FocusOn();
  Assert(unit);
  EntityAIFull *veh = unit->GetVehicle();

  float tankLeft = tankX;
  
  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();

/*
  Texture *corner = GLOB_SCENE->Preloaded(Corner);
  GEngine->DrawFrame
  (
    corner,
    bgColor,
    tankLeft * w, tankY * h, tankW * w, tankH * h
  );
*/

  TurretContext context; 
  bool valid = veh->GetPrimaryGunnerTurret(context);
  TurretContext contextObs; 
  bool validObs = veh->GetPrimaryObserverTurret(contextObs);

  Matrix4Val camInvTransform = camera.GetInvTransform();
  Vector3 dirHull(VRotate, camInvTransform, veh->RenderVisualState().Direction());
  Vector3 dirTurret(
    VRotate, camInvTransform,
    valid ? veh->GetWeaponDirection(veh->RenderVisualState(), context, 0) : veh->RenderVisualState().Direction());
  Vector3 dirObsTurret(
    VRotate, camInvTransform,
    validObs ? veh->GetEyeDirection(veh->RenderVisualState(), contextObs) : veh->RenderVisualState().Direction());

  Matrix4 mrot(MZero);
  Matrix4 mtrans1 = Matrix4(MTranslation, Vector3(-0.5, 0, -0.5));
  Matrix4 mtrans2 = Matrix4(MTranslation, Vector3(0.5, 0, 0.5));

  Draw2DParsExt pars;
  pars.spec = NoZBuf|IsAlpha|IsAlphaFog|ClampU|ClampV;
  pars.spec|= (tankShadow == 2)?UISHADOW : 0; 

  //Rect2D rect(tankLeft * w, tankY * h, tankW * w, tankH * h);
  
  const float size = 0.67;

  Rect2DPixel rect
  (
    w * (tankLeft + tankW * 0.5 - tankW*size*0.5),
    h * (tankY + tankH * 0.5 - tankH*size*0.5),
    tankW * w * size, tankH * h * size
  );

  #define v000 VZero
  #define v001 VForward
  #define v100 VAside
  #define v101 Vector3(1, 0, 1)

  mrot.SetUpAndDirection(VUp, dirHull);
  Matrix4 m = mtrans2 * mrot * mtrans1;
  Vector3 uv = m.FastTransform(v000);
  pars.uTR = uv[2];
  pars.vTR = uv[0];
  uv = m.FastTransform(v001);
  pars.uTL = uv[2];
  pars.vTL = uv[0];
  uv = m.FastTransform(v100);
  pars.uBR = uv[2];
  pars.vBR = uv[0];
  uv = m.FastTransform(v101);
  pars.uBL = uv[2];
  pars.vBL = uv[0];
  Draw2DParsExt parsHull(pars);

  // draw all hull components
  pars.SetColor(ColorFromHit(veh->GetHitForDisplay(0)));
  pars.mip = GEngine->TextBank()->UseMipmap(_imageHull, 0, 0);
  GEngine->Draw2D(pars, rect, rect);

  pars.SetColor(ColorFromHit(veh->GetHitForDisplay(1)));
  pars.mip = GEngine->TextBank()->UseMipmap(_imageEngine, 0, 0);
  GEngine->Draw2D(pars, rect, rect);

  pars.SetColor(ColorFromHit(veh->GetHitForDisplay(3)));
  pars.mip = GEngine->TextBank()->UseMipmap(_imageLTrack, 0, 0);
  GEngine->Draw2D(pars, rect, rect);

  pars.SetColor(ColorFromHit(veh->GetHitForDisplay(2)));
  pars.mip = GEngine->TextBank()->UseMipmap(_imageRTrack, 0, 0);
  GEngine->Draw2D(pars, rect, rect);
  
  mrot.SetUpAndDirection(VUp, dirTurret);
  m = mtrans2 * mrot * mtrans1;
  uv = m.FastTransform(v000);
  pars.uTR = uv[2];
  pars.vTR = uv[0];
  uv = m.FastTransform(v001);
  pars.uTL = uv[2];
  pars.vTL = uv[0];
  uv = m.FastTransform(v100);
  pars.uBR = uv[2];
  pars.vBR = uv[0];
  uv = m.FastTransform(v101);
  pars.uBL = uv[2];
  pars.vBL = uv[0];

  // draw all turret components
  pars.SetColor(ColorFromHit(veh->GetHitForDisplay(4)));
  pars.mip = GEngine->TextBank()->UseMipmap(_imageTurret, 0, 0);
  GEngine->Draw2D(pars, rect, rect);

  pars.SetColor(ColorFromHit(veh->GetHitForDisplay(5)));
  pars.mip = GEngine->TextBank()->UseMipmap(_imageGun, 0, 0);
  GEngine->Draw2D(pars, rect, rect);

  Vector3 offset(0.05 * tankW, 0, 0);
  offset = mrot.FastTransform(offset);
  
  const float size2 = 0.25;
  //const float offX2 = 0.1;
  //const float offY2 = 0.0;

  Rect2DPixel rect2
  (
    w * (tankLeft + tankW * 0.5 - tankW*size2*0.5 + offset.X()),
    h * (tankY + tankH * 0.5 - tankH*size2*0.5) - w * offset.Z(),
    tankW * w * size2, tankH * h * size2
  );

  // observer turret
  pars.SetColor(ColorFromHit(veh->GetHitForDisplay(4)));
  if(dirObsTurret.Size()>0.001f)
    mrot.SetUpAndDirection(VUp, dirObsTurret);
  m = mtrans2 * mrot * mtrans1;
  uv = m.FastTransform(v000);
  pars.uTR = uv[2];
  pars.vTR = uv[0];
  uv = m.FastTransform(v001);
  pars.uTL = uv[2];
  pars.vTL = uv[0];
  uv = m.FastTransform(v100);
  pars.uBR = uv[2];
  pars.vBR = uv[0];
  uv = m.FastTransform(v101);
  pars.uBL = uv[2];
  pars.vBL = uv[0];
  pars.mip = GEngine->TextBank()->UseMipmap(_imageObsTurret, 0, 0);
  GEngine->Draw2D(pars, rect2, rect2);

  Transport *t = dyn_cast<Transport>(veh);
  if (t)
  {
    Texture *texture = NULL;
    VehicleMoveMode move = t->GetMoveMode();
    VehicleTurnMode turn = t->GetTurnMode();
    // reversed (hack) - uv coordinates are flipped
    if (turn == VTMLeft)
    {
      texture = _imageMoveRight;
    }
    else if (turn == VTMRight)
    {
      texture = _imageMoveLeft;
    }
    else switch (move)
    {
    case VMMFormation:
    case VMMMove:
      texture = _imageMoveAuto;
      break;
    case VMMBackward:
      texture = _imageMoveBack;
      break;
    case VMMStop:
      texture = _imageMoveStop;
      break;
    case VMMSlowForward:
    case VMMForward:
      texture = _imageMoveForward;
      break;
    case VMMFastForward:
      texture = _imageMoveFast;
      break;
    default:
      Fail("Unknown move mode");
      break;
    }
    if (texture)
    {
      if (texture->HeadersReady())
      {
        MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 1, 0);
        if (mip._level == 0)
        {
          parsHull.spec|= (tankShadow == 2)?UISHADOW : 0; 
          parsHull.SetColor(PackedWhite);
          parsHull.mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
          GEngine->Draw2D(parsHull, rect);
        }
      }
    }
  }
}

static Ref<INode> GetShortcutText(int shortcut)
{
  // check the shortcut image
  KeyImage keyImage = GInput.GetKeyImage(shortcut);
  if (keyImage.id >= 0)
  {
    INode *text = CreateTextImage(NULL);
    SetTextAttribute(text, "image", keyImage.image);
    // SetTextAttribute(text, "size", keyImage.size);
    SetTextAttribute(text, "size", "1.0");
    return text;
  }
  // no image - use the text
  RString GetKeyName(int dikCode);
  return CreateTextASCII(NULL, GetKeyName(shortcut));
}

static Ref<INode> FindShortcut(MenuItem *item, bool selected)
{
  // Filter shortcuts by platform
#ifdef _XBOX
  bool presentKeyboard = false;
  bool presentXInput = true;
#else
  bool presentKeyboard = true;
  bool presentXInput = GInput.IsXInputPresent();
  bool IsXboxUI();
  if (IsXboxUI())
  {
    // for tuning, show Xbox icons 
    presentKeyboard = false;
    presentXInput = true;
  }
#endif

  // find the first available shortcut
  for (int i=0; i<item->_shortcuts.Size(); i++)
  {
    int shortcut = item->_shortcuts[i];
    int device = shortcut & INPUT_DEVICE_MASK;

    // check if shortcut is available
    bool valid = 
      presentKeyboard && device == INPUT_DEVICE_KEYBOARD ||
      presentXInput && device == INPUT_DEVICE_XINPUT;
    if (valid) return GetShortcutText(shortcut);
  }
  if (selected && presentXInput) return GetShortcutText(INPUT_DEVICE_XINPUT + XBOX_A);
  return NULL;
}

/// Interface for the functor replacing parameters in the structured text
class ProcessMenuParams : public ITextProcessParams
{
protected:
  /// the operator will be redirected to the InGameUI::ProcessMenuParams
  InGameUI *_ui;

public:
  ProcessMenuParams(InGameUI *ui) : _ui(ui) {}
  /// reads the parameter identification starting on ptr and return the string which will replace it (see RStringCT::ParseFormat)
  virtual RString operator ()(const char *&ptr) {return _ui->ProcessMenuParams(ptr);}
};

RString InGameUI::ProcessMenuParams(const char *&ptr)
{
  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return RString();
  AIGroup *group = agent->GetGroup();

  const char *prefix = "SELECTED_UNIT_ID";
  int n = strlen(prefix);
  if (strnicmp(ptr, prefix, n) == 0)
  {
    ptr += n;
    // list of all selected units
    RString text;
    if(!_drawHCCommand)
    {
      OLinkPermNOArray(AIUnit) list;
      ListSelectingUnits(list);
      for (int i=0; i<list.Size(); i++)
      {
        AIUnit *unit = list[i];
        if (!unit || unit == agent) continue;
        if (text.GetLength() > 0) text = text + RString(", ");
        text = text + Format("%d", unit->ID());
      }
    }
    else
    {
      if(agent->GetUnit())
      {
        AIUnit *u= agent->GetUnit();
        AutoArray<AIHCGroup> &groups = u->GetHCGroups();
        for (int i=0; i<groups.Size(); i++)
        {
          if(!groups[i].GetGroup() || !groups[i].IsSelected()) continue;
          if (text.GetLength() > 0) text = text + RString(", ");
          text = text + Format("%d", groups[i].GetGroup()->ID());
        }
      }
    }
    return text;
  }

  prefix = "FOCUSED_UNIT_ID";
  n = strlen(prefix);
  if (strnicmp(ptr, prefix, n) == 0)
  {
    ptr += n;
    if(!_drawHCCommand)
    {
      if (group && _groupInfoSelected >= 0)
      {
        AIUnit *u = group->GetUnit(_groupInfoSelected);
        if (u) return Format("%d", u->ID());
      }
    }
    else
    {
      if (agent->GetUnit() && _hcTarget->HCCommander() && _hcTarget->HCCommander() == agent->GetUnit() && _groupInfoSelected >= 0)
      {
        AutoArray<AIHCGroup> &groups = _hcTarget->HCCommander()->GetHCGroups();
        if(_groupInfoSelected>=groups.Size() || !groups[_groupInfoSelected].GetGroup()) return RString();
        return Format("%d", groups[_groupInfoSelected].GetGroup()->ID());
      }
    }
    return RString();
  }

  prefix = "FOCUSED_VEHICLE_NAME";
  n = strlen(prefix);
  if (strnicmp(ptr, prefix, n) == 0)
  {
    ptr += n;
    if (group && _groupInfoSelected >= 0)
    {
      AIUnit *u = group->GetUnit(_groupInfoSelected);
      if (u)
      {
        EntityAIFull *vehicle = u->GetVehicle();
        if (vehicle) return vehicle->GetDisplayName();
      }
    }
    return RString();
  }

  prefix = "FOCUSED_TEAM_COLOR";
  n = strlen(prefix);
  if (strnicmp(ptr, prefix, n) == 0)
  {
    ptr += n;
    if (group && _groupInfoSelected >= 0)
    {
      AIUnit *u = group->GetUnit(_groupInfoSelected);
      if (u)
      {
        Team team = GetTeam(u);
        switch (team)
        {
        case TeamMain:
          return LocalizeString(IDS_TEAM_MAIN);
        case TeamRed:
          return LocalizeString(IDS_TEAM_RED);
        case TeamGreen:
          return LocalizeString(IDS_TEAM_GREEN);
        case TeamBlue:
          return LocalizeString(IDS_TEAM_BLUE);
        case TeamYellow:
          return LocalizeString(IDS_TEAM_YELLOW);
        }
      }
    }
    return RString();
  }

  prefix = "POINTED_UNIT_ID";
  n = strlen(prefix);
  if (strnicmp(ptr, prefix, n) == 0)
  {
    ptr += n;
    EntityAI *targetAI = _target.IdExact();
    if(!_drawHCCommand)
    {
      if (targetAI)
      {
        AIBrain *u = targetAI->CommanderUnit();
        if (u && u->GetGroup() && u->GetGroup() == group)
        { // inside my group / subgroup
          return Format("%d", u->GetUnit()->ID());
        }
      }
    }
    else
    {
      if(_hcTarget)
      {
        if (_hcTarget->HCCommander() && group->HCCommander() && _hcTarget->HCCommander() == group->HCCommander())
        { // inside my group / subgroup
          return Format("%d", _hcTarget->ID());
        }
      }
    }
    return RString();
  }

  prefix = "POINTED_TARGET_NAME";
  n = strlen(prefix);
  if (strnicmp(ptr, prefix, n) == 0)
  {
    ptr += n;
    if(!_drawHCCommand)
    {
      const EntityAIType *type = _target ? _target->GetType() : NULL;
      if (type) return type->GetDisplayName();
    }
    else if(_hcTarget)
    {
       if (_hcTarget->HCCommander())
       {
         AIUnit *u = _hcTarget->HCCommander();
         AutoArray<AIHCGroup> &groups = u->GetHCGroups();
         for (int j=0; j< groups.Size(); j++)
         {
            if(groups[j].GetGroup() && groups[j].GetGroup() == _hcTarget)
              return groups[j].Name();
         }

       }
    }
    return RString();
  }

  RptF("Unknown menu parameter: %s", ptr);
  return RString();
}

void InGameUI::DrawMenu(Menu *menu, float tmX, float tmY, float &tmW, float tmH, float alpha)
{
  bool ready = true;
  //menu without focus is more transparent
  float alphaFocused = alpha;
  if(!menu->IsContexSensitive() && !_menuScroll) alphaFocused*= unfocusMenuAlpha;

  for (int i=0; i<menu->_items.Size(); i++)
  {
    MenuItem* item = menu->_items[i];
    if (!item->_visible) continue;
    if (item->_cmd == CMD_SEPARATOR) continue;
    if (item->_text && !IsTextReadyToDraw(item->_text, _menuSize)) ready = false;
  }
  if (!ready) return;

  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();
  const float border = TEXT_BORDER;

  Texture *corner = GLOB_SCENE->Preloaded(Corner);
  MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);

  UIViewport *vp = Create2DViewport();

  class ProcessMenuParams func(this);

  if (_xboxStyle)
  {
    // calculate menu size
    float width = 0;
    float widthShortcuts = 0;
    float height = 0;
    for (int i=0; i<menu->_items.Size(); i++)
    {
      MenuItem *item = menu->_items[i];
      if (!item->_visible) continue;

      if (item->_cmd == CMD_SEPARATOR)
        height += 2 * border;
      else if (item->_text)
      {
        SetTextFont(item->_text, _menuFont);
        SetShadow(item->_text,_menuShadow);
        float wItem = GetTextWidthFloat(item->_text, _menuSize, &func);
        saturateMax(width, wItem);
        // shortcut text
        Ref<INode> shortcut = FindShortcut(item, i == menu->_selected);
        if (shortcut)
        {
          SetTextFont(shortcut, _menuFont);
          SetShadow(shortcut,_menuShadow);
          float wShortcuts = GetTextWidthFloat(shortcut, _menuSize);
          saturateMax(widthShortcuts, wShortcuts);
        }
        height += _menuSize;
      }
    }

    width += 2 * border + 0.001; // avoid rounding problems
    float widthTotal = width;
    if (widthShortcuts > 0)
    {
      widthTotal += widthShortcuts + border;
    }

    height += 2 * border;
    float top = tmY + border;

    PackedColor colBg = PackedColorRGB(bgColor, toIntFloor(bgColor.A8() * alphaFocused));
    PackedColor colSelected = PackedColorRGB(menuSelectedColor, toIntFloor(menuSelectedColor.A8() * alphaFocused));
    PackedColor colLine = PackedColorRGB(menuDisabledColor, toIntFloor(menuDisabledColor.A8() * alphaFocused));

    GEngine->DrawFrame
    (
      corner,
      colBg,
      Rect2DPixel(tmX * w, tmY * h, widthTotal * w, height * h)
    );

    for (int i=0; i<menu->_items.Size(); i++)
    {
      MenuItem* item = menu->_items[i];
      if (!item->_visible) continue;

      if (item->_cmd == CMD_SEPARATOR)
      {
        top += border;
        GEngine->DrawLine
        (
          Line2DPixel((tmX + border) * w, top * h, (tmX + widthTotal - border) * w, top * h),
          colLine, colLine
        );
        top += border;
      }
      else if (item->_text)
      {
        if (i == menu->_selected)
          GEngine->Draw2D
          (
            mip, colSelected,
            Rect2DPixel((tmX + border) * w, top * h, (widthTotal - 2 * border) * w, _menuSize * h)
          );

        if (item->_flagged)
        {
          PackedColor col = PackedColorRGB(Color(0, 1, 0, 1), toIntFloor(255 * alphaFocused));
          GEngine->DrawLine
          (
            Line2DPixel((tmX + border) * w, top * h, (tmX + widthTotal - border) * w, top * h),
            col, col
          );
          GEngine->DrawLine
          (
            Line2DPixel((tmX + widthTotal - border) * w, top * h, (tmX + widthTotal - border) * w, (top + _menuSize) * h),
            col, col
          );
          GEngine->DrawLine
          (
            Line2DPixel((tmX + widthTotal - border) * w, (top + _menuSize) * h, (tmX + border) * w, (top + _menuSize) * h),
            col, col
          );
          GEngine->DrawLine
          (
            Line2DPixel((tmX + border) * w, (top + _menuSize) * h, (tmX + border) * w, top * h),
            col, col
          );
        }

        PackedColor col;
        if (item->_check)
          col = menuCheckedColor;
        else if (item->_enable)
          col = menuEnabledColor;
        else
          col = menuDisabledColor;

        float left = tmX + border;
        if (widthShortcuts > 0)
        {
          Ref<INode> shortcut = FindShortcut(item, i == menu->_selected);
          if (shortcut)
          {
            SetTextFont(shortcut, _menuFont);
            SetTextColor(shortcut, col);
            SetShadow(shortcut,_menuShadow);
            DrawText(vp, shortcut, Rect2DFloat(left, top, widthShortcuts, _menuSize), _menuSize, alphaFocused, NULL,false);
          }
          left += widthShortcuts + border;
        }
        SetTextColor(item->_text, col);
        SetShadow(item->_text, _menuShadow);
        DrawText(vp, item->_text, Rect2DFloat(left, top, width - 2 * border, _menuSize), _menuSize, alphaFocused, &func, false);
        top += _menuSize;
      }
    }
    tmW = widthTotal;
  }
  else
  {
    // calculate menu size
    float width = 0;
    float widthShortcuts = 0;
    for (int i=0; i<menu->_items.Size(); i++)
    {
      MenuItem *item = menu->_items[i];
      if (!item->_visible) continue;

      if (item->_cmd == CMD_SEPARATOR) continue;
      if (!item->_text) continue;

      // text
      SetTextFont(item->_text, _menuFont);
      SetShadow(item->_text,_menuShadow);
      float wItem = GetTextWidthFloat(item->_text, _menuSize, &func);
      saturateMax(width, wItem);
      // shortcut
      Ref<INode> shortcut = FindShortcut(item, i == menu->_selected);
      if (shortcut)
      {
        SetTextFont(shortcut, _menuFont);
        SetShadow(shortcut,_menuShadow);
        float wShortcuts = GetTextWidthFloat(shortcut, _menuSize);
        saturateMax(widthShortcuts, wShortcuts);
      }
    }
    width += 2 * border + 0.001; // avoid rounding problems
    float widthTotal = width;
    if (widthShortcuts > 0)
    {
      widthTotal += widthShortcuts + border;
    }
    saturate(widthTotal, tmWMin, tmWMax);

    // update position based on width and alignment
    switch (tmAlign)
    {
    case ST_LEFT:
      break;
    case ST_CENTER:
      tmX -= 0.5 * widthTotal;
      break;
    case ST_RIGHT:
      tmX -= widthTotal;
      break;
    }

    // process hide animation
    switch (_tmHide)
    {
    case HTNone:
      {
        // animate based on the align parameter (backward compatibility)
        switch (tmAlign)
        {
        case ST_LEFT:
          // animation
          tmX -= tmWMax * _tmPos;
          break;
        case ST_CENTER:
          break;
        case ST_RIGHT:
          // animation
          tmX += tmWMax * _tmPos;
          break;
        }
      }
      break;
    case HTFade:
      alphaFocused *= (1.0f - _tmPos);
      break;
    case HTMoveLeft:
      tmX -= tmWMax * _tmPos;
      break;
    case HTMoveRight:
      tmX += tmWMax * _tmPos;
      break;
    }

    PackedColor colBg = PackedColorRGB(bgColor, toIntFloor(bgColor.A8() * alphaFocused));
    PackedColor colCapBg = PackedColorRGB(capBgColor, toIntFloor(capBgColor.A8() * alphaFocused));
    PackedColor colCapFt = PackedColorRGB(capFtColor, toIntFloor(capFtColor.A8() * alphaFocused));
    PackedColor colSelected = PackedColorRGB(menuSelectedColor, toIntFloor(menuSelectedColor.A8() * alphaFocused));
    PackedColor colLine = PackedColorRGB(menuDisabledColor, toIntFloor(menuDisabledColor.A8() * alphaFocused));
    PackedColor colShadow = PackedColorRGB(PackedBlack, toIntFloor(PackedBlack.A8() * alphaFocused));
    PackedColor selectedText = PackedColorRGB(menuSelectedTextColor, toIntFloor(PackedBlack.A8() * alphaFocused));

    // menu frame
    GEngine->DrawFrame
    (
      corner,
      colBg,
      Rect2DPixel(tmX * w, tmY * h, widthTotal * w, tmH * h)
    );

    // menu title
    if (menu->_text)
    {
      SetTextFont(menu->_text, _menuFont);
      SetShadow(menu->_text,_menuShadow);
      float width = GetTextWidthFloat(menu->_text, _menuSize) + 2 * border + 0.002; // avoid rounding problems
      float height = _menuSize + 2 * border;
      float left = tmX + widthTotal - border - width;
      float top = tmY + tmH;
      GEngine->Draw2D(mip, colCapBg, Rect2DPixel(left * w, top * h, width * w, height * h));
      SetTextColor(menu->_text, colCapFt);
      SetTextShadowColor(menu->_text, colShadow);
      SetShadow(menu->_text,_menuShadow);
      DrawText(vp, menu->_text, Rect2DFloat(left + border, top + border, width - 2 * border, _menuSize), _menuSize, 1.0, NULL, false);
    }

    float top = tmY + border;
    float height = _menuSize;
    PackedColor color;
    for (int i=0; i<menu->_items.Size(); i++)
    {
      MenuItem* item = menu->_items[i];
      if (!item->_visible)
        continue;

      if (item->_cmd == CMD_SEPARATOR)
      {
        top += border;
        GEngine->DrawLine
        (
          Line2DPixel((tmX + border) * w, top * h, (tmX + widthTotal - border) * w, top * h),
          colLine, colLine
        );
        top += border;
        continue;
      }

      if (i == menu->_selected && item->_enable)
        color = selectedText;
      else if (item->_check)
        color = PackedColorRGB(menuCheckedColor, toIntFloor(menuCheckedColor.A8() * alphaFocused));
      else if (item->_enable)
        color = PackedColorRGB(menuEnabledColor, toIntFloor(menuEnabledColor.A8() * alphaFocused));
      else
        color = colLine;
      
      bool bottom = item->_shortcuts.Find(DIK_BACK) >= 0;
      float t = bottom ? tmY + tmH - border - height : top;
      
      if (i == menu->_selected)
        GEngine->Draw2D(mip, colSelected, Rect2DPixel((tmX + border) * w, t * h, (widthTotal - 2 * border) * w, height * h),Rect2DClipPixel,capShadow);
      
      // check if char is short or long
      Ref<INode> shortcut = FindShortcut(item, i == menu->_selected);
      if (shortcut)
      {
        SetTextFont(shortcut, _menuFont);
        SetTextColor(shortcut, color);
        SetTextShadowColor(shortcut, colShadow);
        SetShadow(shortcut,_menuShadow);
        DrawText(vp, shortcut, Rect2DFloat(tmX + border, t, widthTotal - 2 * border, _menuSize), _menuSize, alphaFocused,NULL, false);
      }

      if (item->_text)
      {
        float left = tmX + border;
        if (widthShortcuts > 0) left += widthShortcuts + border;
        float width = widthTotal - (left - tmX) - border;

        if (item->_flagged)
        {
          PackedColor col = PackedColorRGB(Color(0, 1, 0, 1), toIntFloor(255 * alphaFocused));
          GEngine->DrawLine
            (
            Line2DPixel((tmX + border) * w, top * h, (tmX + widthTotal - border) * w, top * h),
            col, col
            );
          GEngine->DrawLine
            (
            Line2DPixel((tmX + widthTotal - border) * w, top * h, (tmX + widthTotal - border) * w, (top + _menuSize) * h),
            col, col
            );
          GEngine->DrawLine
            (
            Line2DPixel((tmX + widthTotal - border) * w, (top + _menuSize) * h, (tmX + border) * w, (top + _menuSize) * h),
            col, col
            );
          GEngine->DrawLine
            (
            Line2DPixel((tmX + border) * w, (top + _menuSize) * h, (tmX + border) * w, top * h),
            col, col
            );
        }

        SetTextFont(item->_text, _menuFont);
        SetTextColor(item->_text, color);
        SetTextShadowColor(item->_text, colShadow);
        SetShadow(item->_text,_menuShadow);
        DrawText(vp, item->_text, Rect2DFloat(left, t, width, _menuSize), _menuSize, 1.0, &func, false);
      }
      if (!bottom) top += height;
    }
  }
	Destroy2DViewport(vp);
}

/*!
\patch 5142 Date 3/20/2007 by Jirka
- Fixed: In-game UI - vehicle position icons in the status bar and commanding menu
*/

Ref<INode> InGameUI::CreateUnitDesc(int id, AIUnit *u)
{
  Person *person = u->GetPerson();
  Transport *vehicle = u->GetVehicleIn();

  RString text;
  if (GWorld->GetMode() == GModeNetware && person && !person->IsNetworkPlayer())
    text = Format("%d (%s):", id, cc_cast(LocalizeString(IDS_PLAYER_AI)));
  else
    text = Format("%d:", id);

  Ref<INode> root = CreateTextStructured(NULL);
  INode *node = CreateTextASCII(root, text);
  PackedColor color = teamColors[GetTeam(u)];
  SetTextColor(node, color);

  // soldier type
  if (person)
  {
    Texture *picture = person->GetPicture();
    if (picture)
    {
      INode *img = CreateTextImage(root);
      SetTextImage(img, picture);
      SetTextColor(img, pictureColor);
      SetShadow(img, pictureShadow);
    }
  }

  // command
  RString command;
  AISubgroup *subgrp = u->GetSubgroup();
  if (subgrp == subgrp->GetGroup()->MainSubgroup())
  {
    if (u->IsAway()) command = LocalizeString(IDS_AWAY);
  }
  else if (!subgrp->HasCommand())
    command = LocalizeString(IDS_STATE_WAIT);
  else
  {
    int cmd = subgrp->GetCommand()->_message;
    if (cmd==Command::AttackAndFire) cmd = Command::Attack;
    command = LocalizeString(IDS_STATE_NOCOMMAND + cmd);
  }
  if (command.GetLength() > 0) CreateTextASCII(root, command);

  // semaphore
  switch (u->GetSemaphore())
  {
  case AI::SemaphoreBlue:
  case AI::SemaphoreGreen:
  case AI::SemaphoreWhite:
    {
      INode *img = CreateTextImage(root);
      SetTextImage(img, _imageSemaphore);
      SetTextColor(img, holdFireColor);
    }
    break;
  }

  // weapon
  if (person)
  {
    int index = person->FindWeaponType(MaskSlotSecondary);
    if (index < 0) index = person->FindWeaponType(MaskSlotPrimary);
    if (index < 0) index = person->FindWeaponType(MaskSlotHandGun);

    Texture *picture = NULL;
    if (index >= 0)
    {
      const WeaponType *weapon = person->GetWeaponSystem(index);
      picture = weapon->GetPicture();
      if (!picture) picture = _imageDefaultWeapons;
    }
    else picture = _imageNoWeapons;
    if (picture)
    {
      INode *img = CreateTextImage(root);
      SetTextImage(img, picture);
      SetShadow(img, pictureShadow);
      PackedColor color = pictureColor;
      AIGroup *grp = u->GetGroup();
      if (grp)
      {
        if (grp->GetHealthStateReported(u) >= AIUnit::RSCritical || grp->GetDamageStateReported(u) >= AIUnit::RSCritical)
          color = pictureProblemsColor;
      }
      SetTextColor(img, color);
    }
  }

  // vehicle
  if (vehicle)
  {
    Texture *picture = vehicle->GetPicture();
    if (picture)
    {
      INode *img = CreateTextImage(root);
      SetTextImage(img, picture);
      SetTextColor(img, PackedWhite);
    }

    TurretContext context;
    if (u->IsDriver())
    {
      picture = _imageDriver;
    }
    else if (vehicle->FindTurret(u->GetPerson(), context) && context._turretType)
    {
      if (context._turretType->_primaryObserver || context._weapons->_magazineSlots.Size() == 0)
        picture = _imageCommander;
      else
        picture = _imageGunner;
    }
    else
    {
      picture = _imageCargo;
    }
    if (picture)
    {
      INode *img = CreateTextImage(root);
      SetTextImage(img, picture);
      SetTextColor(img, PackedWhite);
    }
  }

  return root;
}

Ref<INode> InGameUI::CreateVehicleDesc(RString text, Transport *veh)
{
  AIBrain *unit = GWorld->FocusOn();
  Assert(unit);
  AIGroup *group = unit->GetGroup();

  Ref<INode> root = CreateTextStructured(NULL);
  if (text.GetLength() > 0) CreateTextASCII(root, text + RString(" "));
  CreateTextASCII(root, veh->GetShortName());
  
  Texture *picture = veh->GetPicture();
  if (picture)
  {
    INode *img = CreateTextImage(root);
    SetTextImage(img, picture);
    SetTextColor(img, PackedWhite);
  }

  if (group)
  {
    RString units;
    AIBrain *crew[3] = 
    {
      veh->ObserverBrain(),
      veh->GunnerBrain(),
      veh->DriverBrain()
    };
    const TransportType *type = veh->Type();
    if (!type || type->DriverIsCommander())
      swap(crew[1], crew[1]);

    for (int j=0; j<3; j++)
      if (crew[j] && crew[j]->GetGroup() == group)
      {
        if (units.GetLength() > 0) units = units + RString(", ");
        units = units + Format("%d", crew[j]->GetUnit() ? crew[j]->GetUnit()->ID() : 0);
      }
    CreateTextASCII(root, units);
  }

  return root;
}

Ref<INode> InGameUI::CreateTeamDesc(RString text, Team team)
{
  Ref<INode> root = CreateTextStructured(NULL);
  INode *node = CreateTextASCII(root, text);
  PackedColor color = teamColors[team];
  SetTextColor(node, color);
  return root;
}

void InGameUI::DrawUnits(Menu *menu, float tmX, float tmY, float &tmW, float tmH, float alpha)
{
  OLinkPermNOArray(AIUnit) list;
  ListSelectingUnits(list);

  // save selected item
  int sel = menu->_selected;
  int selCmd = CMD_SEPARATOR;
  RString selMenu;
  if (_initialMenu)
  {
    _initialMenu = false;
    // select first item from commands
    if (_basicMenu->_items.Size() > 0)
    {
      const MenuItem *item = _basicMenu->_items[0];
      selCmd = item->_cmd;
      selMenu = item->_submenu;
    }
  }
  else
  {
    if (sel >= 0 && sel < menu->_items.Size())
    {
      const MenuItem *item = menu->_items[sel];
      selCmd = item->_cmd;
      selMenu = item->_submenu;
    }
  }

  // Create menu
  menu->_items.Clear();

  AIBrain *unit = GWorld->FocusOn();
  Assert(unit);
  AIGroup *group = unit->GetGroup();
  if (!group) return;

  if (group->Leader() == unit && group->UnitsCount() > 1)
  {
    // Everybody  
    MenuItem *item = new MenuItem
    (
      LocalizeString(IDS_MENU_UNITS_ALL),
      DIK_GRAVE, CMD_UNITS_ALL
    );
#if _ENABLE_SPEECH_RECOGNITION
    item->_speechId = 20;
#endif
    menu->AddItem(item);
    menu->AddItem(new MenuItem("", 0, CMD_SEPARATOR));

    // Teams
    bool notEmptyMainTeam = false;
    bool notEmptyRedTeam = false;
    bool notEmptyGreenTeam = false;
    bool notEmptyBlueTeam = false;
    bool notEmptyYellowTeam = false;
    for (int i=0; i<group->NUnits(); i++)
    {
      AIUnit *u = group->GetUnit(i);
      if (!u) continue;
      if (u == unit) continue;
      switch (GetTeam(u))
      {
      case TeamMain:
        notEmptyMainTeam = true; break;
      case TeamRed:
        notEmptyRedTeam = true; break;
      case TeamGreen:
        notEmptyGreenTeam = true; break;
      case TeamBlue:
        notEmptyBlueTeam = true; break;
      case TeamYellow:
        notEmptyYellowTeam = true; break;
      }
    }
    if (notEmptyRedTeam || notEmptyGreenTeam || notEmptyBlueTeam || notEmptyYellowTeam)
    {
      if (notEmptyRedTeam)
      {
        MenuItem *item = new MenuItem("", 0, CMD_SELECT_RED);
        item->_baseText = CreateTeamDesc(LocalizeString(IDS_TEAM_RED), TeamRed);
        item->_text = item->_baseText;
#if _ENABLE_SPEECH_RECOGNITION
        item->_speechId = 22;
#endif
        menu->AddItem(item);
      }
      if (notEmptyGreenTeam)
      {
        MenuItem *item = new MenuItem("", 0, CMD_SELECT_GREEN);
        item->_baseText = CreateTeamDesc(LocalizeString(IDS_TEAM_GREEN), TeamGreen);
        item->_text = item->_baseText;
#if _ENABLE_SPEECH_RECOGNITION
        item->_speechId = 23;
#endif
        menu->AddItem(item);
      }
      if (notEmptyBlueTeam)
      {
        MenuItem *item = new MenuItem("", 0, CMD_SELECT_BLUE);
        item->_baseText = CreateTeamDesc(LocalizeString(IDS_TEAM_BLUE), TeamBlue);
        item->_text = item->_baseText;
#if _ENABLE_SPEECH_RECOGNITION
        item->_speechId = 24;
#endif
        menu->AddItem(item);
      }
      if (notEmptyYellowTeam)
      {
        MenuItem *item = new MenuItem("", 0, CMD_SELECT_YELLOW);
        item->_baseText = CreateTeamDesc(LocalizeString(IDS_TEAM_YELLOW), TeamYellow);
        item->_text = item->_baseText;
#if _ENABLE_SPEECH_RECOGNITION
        item->_speechId = 25;
#endif
        menu->AddItem(item);
      }
      if (notEmptyMainTeam)
      {
        MenuItem *item = new MenuItem("", 0, CMD_SELECT_MAIN);
        item->_baseText = CreateTeamDesc(LocalizeString(IDS_TEAM_MAIN), TeamMain);
        item->_text = item->_baseText;
#if _ENABLE_SPEECH_RECOGNITION
        item->_speechId = 21;
#endif
        menu->AddItem(item);
      }
      menu->AddItem(new MenuItem("", 0, CMD_SEPARATOR));
    }

    // Vehicles
    bool anyVehicle = false;
    for (int i=0; i<group->NUnits(); i++)
    {
      AIUnit *u = group->GetUnit(i);
      if (!u) continue;
      Transport *veh = u->GetVehicleIn();
      if (!veh) continue;

      AIUnit *crew[3] = 
      {
        veh->ObserverBrain() ? veh->ObserverBrain()->GetUnit() : NULL,
        veh->GunnerBrain() ? veh->GunnerBrain()->GetUnit() : NULL,
        veh->DriverBrain() ? veh->DriverBrain()->GetUnit() : NULL
      };
      if (u != crew[0] && u != crew[1] && u != crew[2]) continue;

      int count = 0;
      for (int j=0; j<3; j++)
        if (crew[j])
        {
          if (crew[j]->GetGroup() != group) crew[j] = NULL;
          else
          {
            if (crew[j]->ID() < i + 1)
            {
              count = -1;
              break; // already listed
            }
            count++;
          }
        }
      if (count <= 1) continue;

      if (veh->CommanderUnit() == unit)
      {
        MenuItem *item = new MenuItem("", 0, CMD_MY_VEHICLE);
        item->_baseText = CreateVehicleDesc(LocalizeString(IDS_MY_VEHICLE), veh);
        item->_text = item->_baseText;
#if _ENABLE_SPEECH_RECOGNITION
        item->_speechId = 30;
#endif
        menu->AddItem(item);
      }
      else
      {
        MenuItem *item = new MenuItem("", 0, CMD_VEHICLE_1 + i);
        item->_baseText = CreateVehicleDesc(RString(), veh);
        item->_text = item->_baseText;
        menu->AddItem(item);
      }
      anyVehicle = true;
    }
    if (anyVehicle) menu->AddItem(new MenuItem("", 0, CMD_SEPARATOR));

    // Single units
    int count = menu->_items.Size() + group->UnitsCount();
    static const int maxCount = 20;
    if (count > maxCount)
    {
      MenuItem *item = new MenuItem(LocalizeString(IDS_MENU_UNITS), 0, CMD_SINGLE_UNITS);
#if _ENABLE_SPEECH_RECOGNITION
      item->_speechId = 60;
#endif
      menu->AddItem(item);
    }
    else
    {
      for (int i=0; i<group->NUnits(); i++)
      {
        AIUnit *u = group->GetUnit(i);
        if (!u) continue;
        if (u == unit) continue;
        int key = 0;
        if (i < 12)
        {
          key = DIK_F1 + i;
        }
        MenuItem *item = new MenuItem("", key, CMD_UNIT_1 + i);
        item->_baseText = CreateUnitDesc(i + 1, u);
        item->_text = item->_baseText;
        item->_flagged = list.FindKey(u) >= 0;
#if _ENABLE_SPEECH_RECOGNITION
        item->_speechId = 40 + i;
#endif
        menu->AddItem(item);
      }
    }
    menu->AddItem(new MenuItem("", 0, CMD_SEPARATOR));
  }
  else
  {
    Transport *veh = unit->GetVehicleIn();
    if (veh && veh->CommanderUnit() == unit)
    {
      AIUnit *crew[3] = 
      {
        veh->ObserverBrain() ? veh->ObserverBrain()->GetUnit() : NULL,
        veh->GunnerBrain() ? veh->GunnerBrain()->GetUnit() : NULL,
        veh->DriverBrain() ? veh->DriverBrain()->GetUnit() : NULL
      };
      int count = 0;
      for (int j=0; j<3; j++)
      {
        if (crew[j])
        {
/*
          if (crew[j]->GetGroup() != group) crew[j] = NULL;
          else count++;
*/
          count++;
        }
      }
      if (count > 1)
      {
        MenuItem *item = new MenuItem("", 0, CMD_MY_VEHICLE);
        item->_baseText = CreateVehicleDesc(LocalizeString(IDS_MY_VEHICLE), veh);
        item->_text = item->_baseText;
#if _ENABLE_SPEECH_RECOGNITION
        item->_speechId = 30;
#endif
        menu->AddItem(item);
        menu->AddItem(new MenuItem("", 0, CMD_SEPARATOR));
      }
    }
  }

  if (_basicMenu)
  {
    // Hi-level commands
    for (int i=0; i<_basicMenu->_items.Size(); i++)
      menu->_items.Add(_basicMenu->_items[i]);

#if _ENABLE_SPEECH_RECOGNITION
    menu->_vocabulary = _basicMenu->_vocabulary;
#endif
  }

  if (selCmd != CMD_SEPARATOR)
  {
    for (int i=0; i<menu->_items.Size(); i++)
    {
      const MenuItem *item = menu->_items[i];
      if (item->_cmd == selCmd && item->_submenu == selMenu)
      {
        menu->_selected = i;
        break;
      }
    }
  }

  CheckMenuItems(menu);

  // Draw menu
  DrawMenu(menu, tmX, tmY, tmW, tmH, alpha);
}

void InGameUI::DrawSingleUnits(Menu *menu, float tmX, float tmY, float &tmW, float tmH, float alpha)
{
  OLinkPermNOArray(AIUnit) list;
  ListSelectingUnits(list);

  // save selected item
  int sel = menu->_selected;
  int selCmd = CMD_SEPARATOR;
  RString selMenu;
  if (sel >= 0 && sel < menu->_items.Size())
  {
    const MenuItem *item = menu->_items[sel];
    selCmd = item->_cmd;
    selMenu = item->_submenu;
  }

  // Create menu
  menu->_items.Clear();

  AIBrain *unit = GWorld->FocusOn();
  Assert(unit);
  AIGroup *group = unit->GetGroup();
  if (!group) return;

  Assert(group->Leader() == unit && group->UnitsCount() > 1);

  for (int i=0; i<group->NUnits(); i++)
  {
    AIUnit *u = group->GetUnit(i);
    if (!u) continue;
    if (u == unit) continue;
    MenuItem *item = new MenuItem("", 0, CMD_UNIT_1 + i);
    item->_baseText = CreateUnitDesc(i + 1, u);
    item->_text = item->_baseText;
    item->_flagged = list.FindKey(u) >= 0;
    menu->AddItem(item);
  }
  if (selCmd != CMD_SEPARATOR)
  {
    for (int i=0; i<menu->_items.Size(); i++)
    {
      const MenuItem *item = menu->_items[i];
      if (item->_cmd == selCmd && item->_submenu == selMenu)
      {
        menu->_selected = i;
        break;
      }
    }
  }

  CheckMenuItems(menu);

  // Draw menu
  DrawMenu(menu, tmX, tmY, tmW, tmH, alpha);
}

/*!
\patch 2.01 Date 2/5/2003 by Jirka
- Improved: In game UI fonts can be configured in config.cpp
*/

#if _VBS3 // command mode in RTE
void InGameUI::DrawMenu(bool isInEditor)
#else
void InGameUI::DrawMenu()
#endif
{
  if (_tmPos >= 1.0) return;

  int n = _menu.Size();

  if (_xboxStyle)
  {
    if (n == 0 && !_units)
    {
      Fail("Missing menu");
      return;
    }
    float x = tmX, w = 0, y = tmY, h = tmH;
    if (_unitsSingle)
    {
      DrawSingleUnits(_unitsSingle, x, y, w, h, 1.0 - _tmPos);
      x += w + 0.01;
      w = 0;
      y = tmY2;
      h = tmH2;
    }
    else if (_units)
    {
      DrawUnits(_units, x, y, w, h, 1.0 - _tmPos);
      x += w + 0.01;
      w = 0;
      y = tmY2;
      h = tmH2;
    }
    if (n > 0)
    {
      DrawMenu(_menu[n - 1], x, y, w, h, 1.0 - _tmPos);
      Ref<Menu> preview = PreviewMenu();
      if (preview)
      {
        x += w + 0.01;
        w = 0;
        DrawMenu(preview, x, y, w, h, (1.0 - _tmPos) * 0.5);
      }
    }
  }
  else
  {
    if (n == 0)
    {
      Fail("Missing menu");
      return;
    }
    Menu *menu = _menu[n - 1];
    float w = 0;

#if _VBS3 // command mode in RTE
    if(isInEditor)
    	tmY = tmEditorY;  
#endif    
    if (stricmp(menu->GetName(), "#UNITS") == 0)
      DrawUnits(menu, tmX, tmY, w, tmH, 1.0);
    else
      DrawMenu(menu, tmX, tmY, w, tmH, 1.0);
  }
}

#define MIN_X     (-1.0 * H_PI)
#define MAX_X     (1.0 * H_PI)
#define MIN_Y     (-0.25 * H_PI)
#define MAX_Y     (0.25 * H_PI)
#define TOT_W     (MAX_X - MIN_X)
#define TOT_H     (MAX_Y - MIN_Y)
#define INV_W     (1.0 / TOT_W)
#define INV_H     (1.0 / TOT_H)

/*!
\patch 5117 Date 1/17/2007 by Bebul
- New: new config option alwaysTarget forcing training targets to be lockable

\patch 5149 Date 3/26/2007 by Ondra
- Fixed: Faint radar/IR contacts (distant targets) now rendered as unknown (yellow).
*/
void InGameUI::DrawTacticalDisplay
(
  const Camera &camera, AIBrain *unit, const TargetList &list
)
{
  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();

  Texture *corner = GLOB_SCENE->Preloaded(Corner);
  GEngine->DrawFrame
  (
    corner,
    bgColor,
    Rect2DPixel(tdX * w, tdY * h, tdW * w, tdH * h)
  );

  Vector3 dir = Vector3(0, 0, 1);
  float xAngle = atan2(dir.X(), dir.Z());
  float yAngle = atan2(dir.Y(), dir.SizeXZ());
  float leftAngle = atan(camera.Left());
  float topAngle = atan(camera.Top());
  float xMin = xAngle - leftAngle;
  float xMax = xAngle + leftAngle;
  float yMin = yAngle - topAngle;
  float yMax = yAngle + topAngle;
  if (xMax >= MIN_X && xMin <= MAX_X &&
    yMax >= MIN_Y && yMin <= MAX_Y)
  {
    if (yMin < MIN_Y) yMin = MIN_Y;
    if (yMax > MAX_Y) yMax = MAX_Y;
    MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
    if (xMin < MIN_X)
    {
      xMin += TOT_W;
      GEngine->Draw2D(mip, cameraColor,
        Rect2DPixel((tdX + 0.5 * tdW + xMin * tdW * INV_W) * w, (tdY + 0.5 * tdH - yMax * tdH * INV_H) * h,
        ((MAX_X - xMin) * tdW * INV_W) * w, ((yMax - yMin) * tdH * INV_H) * h));
      GEngine->Draw2D(mip, cameraColor,
        Rect2DPixel((tdX + 0.5 * tdW + MIN_X * tdW * INV_W) * w, (tdY + 0.5 * tdH - yMax * tdH * INV_H) * h,
        ((xMax - MIN_X) * tdW * INV_W) * w, ((yMax - yMin) * tdH * INV_H) * h));
    }
    else if (xMax > MAX_X)
    {
      xMax -= TOT_W;
      GEngine->Draw2D(mip, cameraColor,
        Rect2DPixel((tdX + 0.5 * tdW + xMin * tdW * INV_W) * w, (tdY + 0.5 * tdH - yMax * tdH * INV_H) * h,
        ((MAX_X - xMin) * tdW * INV_W) * w, ((yMax - yMin) * tdH * INV_H) * h));
      GEngine->Draw2D(mip, cameraColor,
        Rect2DPixel((tdX + 0.5 * tdW + MIN_X * tdW * INV_W) * w, (tdY + 0.5 * tdH - yMax * tdH * INV_H) * h,
        ((xMax - MIN_X) * tdW * INV_W) * w, ((yMax - yMin) * tdH * INV_H) * h));
    }
    else
    {
      GEngine->Draw2D(mip, cameraColor,
        Rect2DPixel((tdX + 0.5 * tdW + xMin * tdW * INV_W) * w, (tdY + 0.5 * tdH - yMax * tdH * INV_H) * h,
        ((xMax - xMin) * tdW * INV_W) * w, ((yMax - yMin) * tdH * INV_H) * h));
    }
    //GEngine->TextBank()->ReleaseMipmap();
  }

  Matrix4Val camInvTransform=camera.GetInvTransform();

  const VehicleType *nonstrategic = GWorld->Preloaded(VTypeNonStrategic);

  int n = list.AnyCount();
  PackedColor color;
  bool modeRadar = true;
#if _ENABLE_CHEATS
  if (_showAll) modeRadar = false;
  if (tdCheat) modeRadar = false;
#endif

  EntityAIFull *myVeh = unit->GetVehicle();
  const VehicleType *manType=GWorld->Preloaded(VTypeMan);

  for (int i=0; i<n; i++)
  {
    Target &tar = *list.GetAny(i);
    EntityAI *vehExact = tar.idExact;

    if (unit->GetVehicle() == vehExact ) continue;
    if (tar.IsVanished()) continue;

    // tactical display has two modes, radar and show targets
    // each mode displays different information
    // info is distinguished by different "visible" calculation
    float visible = 0;
    float tDim = 0.004;
    Vector3 pos;
    TargetSide side = TSideUnknown;

    if (modeRadar)
    {
      // check landscape visibility and range
      if (!vehExact) continue;
      pos = vehExact->AimingPosition(vehExact->RenderVisualState());

      // check visibility to target
      // (only for non-static)
      float dist2 = myVeh->RenderVisualState().Position().Distance2(pos);
      visible = myVeh->CalcVisibility(vehExact,dist2);
      /*
      LogF
      (
        "Radar: %s (%s) from %s (%s) - %g",
        (const char *)vehExact->GetDebugName(),vehExact->GetShape()->Name(),
        (const char *)myVeh->GetDebugName(),myVeh->GetShape()->Name(),
        visible
      );
      */

      // check if it is (or might be) man
      //if (vehExact->GetType()->IsKindOf(manType) )
      if (vehExact)
      {
        // should be detected when:
        // is IR target and we have IR scanner
        // or is laser target and we have laser scanner
        // IR scanner can be assumed as always present -
        // irTarget || laserTarget && laserScanner
        // !(!irTarget && (!laserTarget || !laserScanner))

        if(vehExact->GetType()->GetArtilleryTarget()) continue;
        if
        (
          !vehExact->GetType()->_irTarget &&
          // only laser scanner can see laser targets
          (!vehExact->GetType()->GetLaserTarget() || !myVeh->GetType()->GetLaserScanner())
        )
        {
          tDim = 0.002;
  #if _ENABLE_CHEATS
          if (!tdCheat)
  #endif
            continue;
        }
      }

      if (visible<0.1f) side = TSideUnknown;
      else side = RadarTargetSide(unit,tar);
    }
    else
    {
      if
      (
        !tar.IsKnownBy(unit)
#if _ENABLE_CHEATS
        && !_showAll
#endif
      ) continue;

      pos =
      (
#if _ENABLE_CHEATS
        _showAll && tar.idExact ?
        tar.idExact->AimingPosition(tar.idExact->RenderVisualState()) :
#endif
        tar.LandAimingPosition(unit)
      );
      visible=tar.FadingSpotability();
      //float visible=tar.FadingVisibility();
#if _ENABLE_CHEATS
      if( _showAll )
      {
        visible=1;
        if (vehExact && !vehExact->GetType()->_irTarget)
        {
          tDim = 0.002;
        }
      }
      else
#endif
      {
        if
        (
          tar.IsKindOf(manType) || tar.FadingAccuracy()<manType->GetAccuracy()
          || vehExact && !vehExact->GetType()->_irTarget
        )
        {
          tDim = 0.002;
#if _ENABLE_CHEATS
          if (!tdCheat)
#endif
            continue;
        }
      }

      side = tar.GetType()->_typicalSide;
    }
    
    if (visible<=0.01) continue;


    if (vehExact && vehExact->GetType()->IsKindOf(nonstrategic) && !vehExact->GetType()->GetAlwaysTarget()) continue;

    dir=camInvTransform.Rotate(pos - camera.Position());

    xAngle = atan2(dir.X(), dir.Z());
    yAngle = atan2(dir.Y(), dir.SizeXZ());
    if (xAngle >= MIN_X && xAngle <= MAX_X &&
      yAngle >= MIN_Y && yAngle <= MAX_Y)
    {
      float tDimX = tDim;
      float tDimY = (4.0f / 3.0f) * tDim;
      float xScreen = (tdX + 0.5 * tdW + xAngle * tdW * INV_W - tDimX) * w;
      float yScreen = (tdY + 0.5 * tdH - yAngle * tdH * INV_H - tDimY) * h;


      if (tar.idExact && !tar.idExact->IRSignatureOn())
      {
        AIGroup *grp = tar.idExact->GetGroup();
        if (!grp || grp != unit->GetGroup())
        {
          side = TCivilian; // empty marked as civilian
        }
      }

      if
      (
        Glob.config.IsEnabled(DTEnemyTag)
#if _ENABLE_CHEATS
        || _showAll
#endif
      )
      {
        side = tar.GetSide();
        if (tar.IsDestroyed()) side = TCivilian;
      }

      if (side == TCivilian)
        color = civilianColor;
      else if (unit->IsFriendly(side))
        color = friendlyColor;
      else if (unit->IsEnemy(side))
        color = enemyColor;
      else if (unit->IsNeutral(side))
        color = neutralColor;
      else
        color = unknownColor;

      //float visible=tar.visibility;
      saturateMin(visible,1);
      color.SetA8(toIntFloor(color.A8()*visible));
      MipInfo mip = GEngine->TextBank()->UseMipmap(_radarGroundTargetTexture, 0, 0);
      Rect2DPixel rect(GEngine->PixelAlignedX(xScreen), GEngine->PixelAlignedX(yScreen), 2 * tDimX * w, 2 * tDimY * h);
      GEngine->Draw2D(
        mip, color, rect, 
        Rect2DPixel(tdX * w, tdY * h, tdW * w, tdH * h)
      );
      //GEngine->TextBank()->ReleaseMipmap();
    }
  }
}



#define MAXDISTANCE  5000.0f
void InGameUI::DrawRadarObject(Entity *entity,Entity *owner, int w, int h, float distance, MipInfo mip, PackedColor color)
{
  Vector3 dirV = Vector3(owner->RenderVisualState().Direction().X(),0,owner->RenderVisualState().Direction().Z());
  Vector3 dirT= entity->RenderVisualState().Position() - owner->AimingPosition(owner->RenderVisualState());
  dirT = Vector3(dirT.X(),0,dirT.Z());

  float cosXa = dirV.CosAngle(dirT);
  float sinXa = dirV.CosAngle(Vector3(-dirT.Z(),0,dirT.X())); 

  float tDimX = 0.006f;
  float tDimY = (4.0f / 3.0f) * tDimX;
  float invdist = 1- pow(1-(distance/MAXDISTANCE),3); 
  float xScreen = (radarX + 0.5 * radarW + 0.5f * sinXa * radarW * invdist - tDimX) * w;
  float yScreen = (radarY + 0.5 * radarH - 0.5f * cosXa * radarH * invdist - tDimY) * h;

  Rect2DPixel rect(GEngine->PixelAlignedX(xScreen), GEngine->PixelAlignedX(yScreen), 2 * tDimX * w, 2 * tDimY * h);
  GEngine->Draw2D(
    mip, color, rect, 
    Rect2DPixel(radarX * w, radarY * h, radarW * w, radarH * h), radarShadow
    );
}



void InGameUI::DrawRadar
(
 const Camera &camera, AIBrain *unit, const TargetList &list, Texture *background
 )
{
  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();


  EntityAIFull *myVeh = unit->GetVehicle();
  const VehicleType *manType=GWorld->Preloaded(VTypeMan);
  if(!GWorld->FocusOn()->GetPerson()) return;

  //draw radar background
  MipInfo mip = GEngine->TextBank()->UseMipmap(background, 0, 0);
  Rect2DPixel rect(radarX * w, radarY * h, radarW * w, radarH * h);
  GEngine->Draw2D(
    mip, PackedWhite, rect, 
    rect, radarShadow
    );

  Person *player = GWorld->FocusOn()->GetPerson();
  if(!player) return;

  //gunner and observer turrets, used for FOV draw
  TurretContext context; 
  bool valid = myVeh->GetPrimaryGunnerTurret(context);
  TurretContext contextObs; 
  bool validObs = myVeh->GetPrimaryObserverTurret(contextObs);

  Draw2DParsExt pars;
  pars.spec = NoZBuf|IsAlpha|IsAlphaFog|ClampU|ClampV;
  pars.SetColor(PackedWhite);

  Matrix4 mtrans1 = Matrix4(MTranslation, Vector3(-0.5, 0, -0.5));
  Matrix4 mtrans2 = Matrix4(MTranslation, Vector3(0.5, 0, 0.5));
  Matrix4 mrot(MZero);
  Matrix4 mScale(M4Identity);

  bool playersFOVDrawn = false;
  //gunner FOV
  if(valid)
  {
    Vector3 dirTurret(myVeh->GetWeaponDirection(myVeh->RenderVisualState(), context, 0));
    int opticsMode = (context._turret)?context._turret->_currentViewOpticsMode : 0;
    float crewFOV = (context._turretType)? context._turretType->_viewOptics[opticsMode]._initFov : H_PI*0.25f ;

    float leftAngle = (player == context._gunner)? atan(camera.Left()) : crewFOV ;
    saturate(leftAngle,H_PI * 0.01f, H_PI*0.9f); 
    if(player == context._gunner) 
    {
      playersFOVDrawn = true;
      pars.SetColor(_turretPlayerFOVColor);
    }
    else pars.SetColor(_turretFOVColor);

    float scaleFOVX = sin(leftAngle); 
    float scaleFOVY = cos(leftAngle);

    Vector3 dirA = (player == context._gunner)? camera.Direction().Normalized():dirTurret.Normalized();
    Vector3 dirB = myVeh->RenderVisualState().Direction().Normalized();

    float cosXa = dirB.DotProduct(dirA);
    float sinXa = dirB.DotProduct(Vector3(-dirA.Z(),0,dirA.X())); 

    mrot.SetUpAndDirection(VUp, Vector3(sinXa, 0, cosXa));
    mScale.SetScale(1.0f/scaleFOVY,1,1.0f/scaleFOVX);
    Matrix4 m =   mtrans2 * mScale * mrot *  mtrans1;

    Vector3 uv = m.FastTransform(v000);
    pars.uTR = uv[2];
    pars.vTR = uv[0];
    uv = m.FastTransform(v001);
    pars.uTL = uv[2];
    pars.vTL = uv[0];
    uv = m.FastTransform(v100);
    pars.uBR = uv[2];
    pars.vBR = uv[0];
    uv = m.FastTransform(v101);
    pars.uBL = uv[2];
    pars.vBL = uv[0];
    pars.mip = GEngine->TextBank()->UseMipmap(_radarFOV, 0, 0);
    GEngine->Draw2D(pars, rect, Rect2DPixel(radarX * w, radarY * h,  radarW * w, radarH * h));
  }

  //observer FOV (draw only if player is gunner or observer)
  if(validObs && (playersFOVDrawn || player == contextObs._gunner))
  {
    Vector3 dirObsTurret(myVeh->GetEyeDirection(myVeh->RenderVisualState(), contextObs));
    int opticsMode = (contextObs._turret)?contextObs._turret->_currentViewGunnerMode : 0;
    float crewFOV = (contextObs._turretType)? contextObs._turretType->_viewGunner[opticsMode]._initFov : H_PI*0.25f ;

    float leftAngle = (player == contextObs._gunner)? atan(camera.Left()) : crewFOV ;
    saturate(leftAngle,H_PI * 0.01f, H_PI*0.9f); 
    if(player == contextObs._gunner)
    {
      playersFOVDrawn = true;
      pars.SetColor(_turretPlayerFOVColor);
    }
    else pars.SetColor(_turretFOVColor);
    
    float scaleFOVX = sin(leftAngle); 
    float scaleFOVY = cos(leftAngle);

    Vector3 dirA = (player == contextObs._gunner)? camera.Direction().Normalized():dirObsTurret.Normalized();
    Vector3 dirB = myVeh->RenderVisualState().Direction().Normalized();

    float cosXa = dirB.DotProduct(dirA);
    float sinXa = dirB.DotProduct(Vector3(-dirA.Z(),0,dirA.X())); 


    mrot.SetUpAndDirection(VUp, Vector3(sinXa, 0, cosXa));
    mScale.SetScale(1.0f/scaleFOVY,1,1.0f/scaleFOVX);
    Matrix4 m =   mtrans2 * mScale * mrot *  mtrans1;

    Vector3 uv = m.FastTransform(v000);
    pars.uTR = uv[2];
    pars.vTR = uv[0];
    uv = m.FastTransform(v001);
    pars.uTL = uv[2];
    pars.vTL = uv[0];
    uv = m.FastTransform(v100);
    pars.uBR = uv[2];
    pars.vBR = uv[0];
    uv = m.FastTransform(v101);
    pars.uBL = uv[2];
    pars.vBL = uv[0];
    pars.mip = GEngine->TextBank()->UseMipmap(_radarFOV, 0, 0);
    GEngine->Draw2D(pars, rect, Rect2DPixel(radarX * w, radarY * h,  radarW * w, radarH * h));
  }

  //players FOV (if not already drawn)
  if(!playersFOVDrawn)
  {
    float leftAngle =  atan(camera.Left());
    saturate(leftAngle,H_PI * 0.01f, H_PI*0.9f); 
    pars.SetColor(_turretPlayerFOVColor);

    float scaleFOVX = sin(leftAngle); 
    float scaleFOVY = cos(leftAngle);

    Vector3 dirA = camera.Direction().Normalized();
    Vector3 dirB = myVeh->RenderVisualState().Direction().Normalized();

    float cosXa = dirB.DotProduct(dirA);
    float sinXa = dirB.DotProduct(Vector3(-dirA.Z(),0,dirA.X())); 

    mrot.SetUpAndDirection(VUp, Vector3(sinXa, 0, cosXa));
    mScale.SetScale(1.0f/scaleFOVY,1,1.0f/scaleFOVX);
    Matrix4 m =   mtrans2 * mScale * mrot *  mtrans1;

    Vector3 uv = m.FastTransform(v000);
    pars.uTR = uv[2];
    pars.vTR = uv[0];
    uv = m.FastTransform(v001);
    pars.uTL = uv[2];
    pars.vTL = uv[0];
    uv = m.FastTransform(v100);
    pars.uBR = uv[2];
    pars.vBR = uv[0];
    uv = m.FastTransform(v101);
    pars.uBL = uv[2];
    pars.vBL = uv[0];
    pars.mip = GEngine->TextBank()->UseMipmap(_radarFOV, 0, 0);
    GEngine->Draw2D(pars, rect, Rect2DPixel(radarX * w, radarY * h,  radarW * w, radarH * h));
  }

  const VehicleType *nonstrategic = GWorld->Preloaded(VTypeNonStrategic);

  int n = list.AnyCount();
  PackedColor color;
  bool modeRadar = true;
#if _ENABLE_CHEATS
  if (_showAll) modeRadar = false;
  if (tdCheat) modeRadar = false;
#endif

  //draw radar elements

  //if(transpor)
  {
    float time = Glob.uiTime - _lastFormTime;
    PackedColor BlinkColor(PackedColor color1, PackedColor color2, float t);
    pars.SetColor(BlinkColor(_radarIncommingDangerColor, PackedColor(255,255,255,0), 3 * time));
    pars.mip = GEngine->TextBank()->UseMipmap(_radarAirDangerSector, 0, 0);
    pars.spec|= (radarShadow == 2)?UISHADOW : 0; 
    int activeSectors = 0;
  
    //incoming missile warning
    for (int i=0; i<myVeh->NIncomingMissiles(); i++)
    {
      Missile *missile = myVeh->GetIncomingMissile(i);
      if(!missile) continue;

      float dist = myVeh->RenderVisualState().Position().Distance(missile->RenderVisualState().Position());
      if(dist < 10.0) continue;

      if(((missile->Type()->weaponLockSystem) & myVeh->GetType()->GetIncommingMissileDetectionSystem())==0) continue;

      Vector3 pos = missile->RenderVisualState().Position();
      Vector3 dirV = Vector3(myVeh->RenderVisualState().Direction().X(),0,myVeh->RenderVisualState().Direction().Z());
      Vector3 dirT= missile->RenderVisualState().Position() - myVeh->AimingPosition(myVeh->RenderVisualState());
      dirT = Vector3(dirT.X(),0,dirT.Z());

      float cosXa = dirV.CosAngle(dirT);
      float sinXa = dirV.CosAngle(Vector3(-dirT.Z(),0,dirT.X())); 

      float angle = acos(cosXa);
      if(sinXa < 0) angle = (2*H_PI) - angle;
      
      int sector = (toIntFloor(angle / (H_PI * 0.25f)));
      if((activeSectors & (1 << sector)) == 0)
      {
        activeSectors|= 1 << sector;
        angle = sector *(H_PI * 0.25f) + (H_PI)/4.0f;
        
        mrot.SetUpAndDirection(VUp, Vector3(sin(angle),0,cos(angle)));
        Matrix4 m = mtrans2 * mrot * mtrans1;
        Vector3 uv = m.FastTransform(v000);
        pars.uTR = uv[2];
        pars.vTR = uv[0];
        uv = m.FastTransform(v001);
        pars.uTL = uv[2];
        pars.vTL = uv[0];
        uv = m.FastTransform(v100);
        pars.uBR = uv[2];
        pars.vBR = uv[0];
        uv = m.FastTransform(v101);
        pars.uBL = uv[2];
        pars.vBL = uv[0];
        GEngine->Draw2D(pars, Rect2DPixel(radarX * w, radarY * h, radarW * w, radarH * h), Rect2DPixel(radarX * w, radarY * h, radarW * w, radarH * h));

      }
    }

    //locked warning
    pars.SetColor(BlinkColor(_radarLockDangerColor, PackedColor(255,255,255,0), 2 * time));
    for (int i=0; i<myVeh->NTargetingEnemies(); i++)
    {
      Person *targetingEnemy = myVeh->GetTargetingEnemy(i);
      if(!targetingEnemy || !targetingEnemy->CommanderUnit()) continue;
      if(!targetingEnemy->CommanderUnit()->GetVehicle()) continue;

      EntityAIFull *entity = targetingEnemy->CommanderUnit()->GetVehicle();

      float dist = myVeh->RenderVisualState().Position().Distance(entity->RenderVisualState().Position());

      TurretContextV context;
      if(entity->FindTurret(unconst_cast(entity->RenderVisualState()), targetingEnemy,context))
      {//draw only if lock can be detected
        int weapon = context._weapons->ValidatedCurrentWeapon();
        if (weapon >= 0)
        {
          const WeaponType *type = context._weapons->_magazineSlots[weapon]._weapon;
          if(context._weapons->_targetAimed <= 0.1 || (myVeh->GetType()->GetLockDetectionSystem() & type->_weaponLockSystem) == 0)
            continue;
        }
        else continue;
      }
      else continue;

      Vector3 pos = entity->RenderVisualState().Position();
      Vector3 dirV = Vector3(myVeh->RenderVisualState().Direction().X(),0,myVeh->RenderVisualState().Direction().Z());
      Vector3 dirT= entity->RenderVisualState().Position() - myVeh->AimingPosition(myVeh->RenderVisualState());
      dirT = Vector3(dirT.X(),0,dirT.Z());

      float cosXa = dirV.CosAngle(dirT);
      float sinXa = dirV.CosAngle(Vector3(-dirT.Z(),0,dirT.X())); 

      float angle = acos(cosXa);
      if(sinXa < 0) angle = (2*H_PI) - angle;

      int sector = (toIntFloor(angle / (H_PI * 0.25f)));
      if((activeSectors & (1 << sector)) == 0)
      {
        activeSectors|= 1 << sector;
        angle = sector *(H_PI * 0.25f) + (H_PI)/4.0f;
        if(dist < 10.0) continue;
        {
          mrot.SetUpAndDirection(VUp, Vector3(sin(angle),0,cos(angle)));
          Matrix4 m = mtrans2 * mrot * mtrans1;
          Vector3 uv = m.FastTransform(v000);
          pars.uTR = uv[2];
          pars.vTR = uv[0];
          uv = m.FastTransform(v001);
          pars.uTL = uv[2];
          pars.vTL = uv[0];
          uv = m.FastTransform(v100);
          pars.uBR = uv[2];
          pars.vBR = uv[0];
          uv = m.FastTransform(v101);
          pars.uBL = uv[2];
          pars.vBL = uv[0];
          GEngine->Draw2D(pars, Rect2DPixel(radarX * w, radarY * h, radarW * w, radarH * h), Rect2DPixel(radarX * w, radarY * h, radarW * w, radarH * h));
        }
      }
    }

    //draw all targets
    for (int i=0; i<n; i++)
    {
      Target &tar = *list.GetAny(i);
      EntityAI *vehExact = tar.idExact;

      if (unit->GetVehicle() == vehExact ) continue;
      if (!vehExact) continue;
      if (tar.IsVanished()) continue;

      float dist2 = myVeh->RenderVisualState().Position().Distance2(vehExact->AimingPosition(vehExact->RenderVisualState()));
      float dist = myVeh->RenderVisualState().Position().Distance(vehExact->AimingPosition(vehExact->RenderVisualState()));

      dist = floatMin(dist, MAXDISTANCE);

      // tactical display has two modes, radar and show targets
      // each mode displays different information
      // info is distinguished by different "visible" calculation
      float visible = 0;
      float tDim = 0.004;
      Vector3 pos;
      TargetSide side = TSideUnknown;

      if (modeRadar)
      {
        // check landscape visibility and range
        if (!vehExact) continue;
        pos = vehExact->AimingPosition(vehExact->RenderVisualState());

        // check visibility to target
        // (only for non-static)
        visible = myVeh->CalcVisibility(vehExact,dist2);

        // check if it is (or might be) man
        //if (vehExact->GetType()->IsKindOf(manType) )
        if (vehExact)
        {
          // should be detected when:
          // is IR target and we have IR scanner
          // or is laser target and we have laser scanner
          // IR scanner can be assumed as always present -
          // irTarget || laserTarget && laserScanner
          // !(!irTarget && (!laserTarget || !laserScanner))

          if
            (
            !vehExact->GetType()->_irTarget &&
            // only laser scanner can see laser targets
            (!vehExact->GetType()->GetLaserTarget() || !myVeh->GetType()->GetLaserScanner())
            )
          {
            tDim = 0.002;
#if _ENABLE_CHEATS
            if (!tdCheat)
#endif
              continue;
          }
        }

        if (visible<0.1f) side = TSideUnknown;
        else side = RadarTargetSide(unit,tar);
      }
      else
      {
        if
          (
          !tar.IsKnownBy(unit)
#if _ENABLE_CHEATS
          && !_showAll
#endif
          ) continue;

        pos =
          (
#if _ENABLE_CHEATS
          _showAll && tar.idExact ?
          tar.idExact->AimingPosition(tar.idExact->RenderVisualState()) :
#endif
        tar.LandAimingPosition(unit)
          );
        visible=tar.FadingSpotability();
        //float visible=tar.FadingVisibility();
#if _ENABLE_CHEATS
        if( _showAll )
        {
          visible=1;
          if (vehExact && !vehExact->GetType()->_irTarget)
          {
            tDim = 0.002;
          }
        }
        else
#endif
        {
          if
            (
            tar.IsKindOf(manType) || tar.FadingAccuracy()<manType->GetAccuracy()
            || vehExact && !vehExact->GetType()->_irTarget
            )
          {
            tDim = 0.002;
#if _ENABLE_CHEATS
            if (!tdCheat)
#endif
              continue;
          }
        }

        side = tar.GetType()->_typicalSide;
      }

      if (visible<=0.01) continue;
      if (vehExact && vehExact->GetType()->IsKindOf(nonstrategic) && !vehExact->GetType()->GetAlwaysTarget()) continue;

      Vector3 dir= pos - myVeh->RenderVisualState().Position();


      float alt = pos.Y() - GLOB_LAND->RoadSurfaceYAboveWater(pos.X(), pos.Z());
      if (tar.idExact && !tar.idExact->IRSignatureOn())
      {
        AIGroup *grp = tar.idExact->GetGroup();
        if (!grp || grp != unit->GetGroup())
        {
          side = TCivilian; // empty marked as civilian
        }
      }

      if
        (
        Glob.config.IsEnabled(DTEnemyTag)
#if _ENABLE_CHEATS
        || _showAll
#endif
        )
      {
        side = tar.GetSide();
        if (tar.IsDestroyed()) side = TCivilian;
      }

      if (side == TCivilian)
        color = civilianColor;
      else if (unit->IsFriendly(side))
        color = friendlyColor;
      else if (unit->IsEnemy(side))
        color = enemyColor;
      else if (unit->IsNeutral(side))
        color = neutralColor;
      else
        color = unknownColor;

      //float visible=tar.visibility;
      saturate(visible,0.75f,1);
      color.SetA8(toIntFloor(color.A8()*visible));
      MipInfo mip = (alt<10.0f)? GEngine->TextBank()->UseMipmap(_radarGroundTargetTexture, 0, 0):GEngine->TextBank()->UseMipmap(_radarAirTargetTexture, 0, 0);
      DrawRadarObject(vehExact,myVeh,w,h,dist,mip,color);
    }

    // draw all units, where we can detect weapon lock on us
    for (int i=0; i<myVeh->NTargetingEnemies(); i++)
    {
      Person *targetingEnemy = myVeh->GetTargetingEnemy(i);
      if(!targetingEnemy || !targetingEnemy->CommanderUnit()) continue;
      if(!targetingEnemy->CommanderUnit()->GetVehicle()) continue;

      EntityAIFull *entity = targetingEnemy->CommanderUnit()->GetVehicle();

      float dist = myVeh->RenderVisualState().Position().Distance(entity->RenderVisualState().Position());
      dist = floatMin(dist, MAXDISTANCE);

      TurretContextV context;
      if(entity->FindTurret(unconst_cast(entity->RenderVisualState()), targetingEnemy,context))
      {//draw only if lock can be detected
        int weapon = context._weapons->ValidatedCurrentWeapon();
        if (weapon >= 0)
        {
          const WeaponType *type = context._weapons->_magazineSlots[weapon]._weapon;
          if(context._weapons->_targetAimed <= 0.1 || (myVeh->GetType()->GetLockDetectionSystem() & type->_weaponLockSystem) == 0)
            continue;
        }
        else continue;
      }
      else continue;

      MipInfo mip = GEngine->TextBank()->UseMipmap(_radarTargetingEnemy, 0, 0);
      DrawRadarObject(entity,myVeh,w,h,dist,mip, enemyColor);
    }

    //draw current target
    if(_lockTarget)
    {
      EntityAI *vehExact = _lockTarget->idExact;
      if (vehExact && !_lockTarget->IsVanished())
      {
        float dist = myVeh->RenderVisualState().Position().Distance(vehExact->AimingPosition(vehExact->RenderVisualState()));
        dist = floatMin(dist, MAXDISTANCE);
        {
          MipInfo mip = GEngine->TextBank()->UseMipmap(_radarVehicleTarget, 0, 0);
          DrawRadarObject(vehExact,myVeh,w,h,dist,mip, PackedWhite);
        }
      }
    }

    //draw detectable incoming missiles 
    MipInfo mip = GEngine->TextBank()->UseMipmap(_radarIncommingMissile, 0, 0);     
    for (int i=0; i<myVeh->NIncomingMissiles(); i++)
    {
      Missile *missile = myVeh->GetIncomingMissile(i);
      if(!missile) continue;

      float dist = myVeh->RenderVisualState().Position().Distance(missile->RenderVisualState().Position());
      if(dist > MAXDISTANCE) continue;
      if(((missile->Type()->weaponLockSystem) & myVeh->GetType()->GetIncommingMissileDetectionSystem())==0) continue;
      DrawRadarObject(missile, myVeh,w,h,dist,mip,enemyColor);
    }
  }
}

void InGameUI::DrawCompass(EntityAIFull *vehicle)
{
  //Texture *textureDef = GLOB_SCENE->Preloaded(TextureWhite);
  Texture *texture;
  MipInfo mip;
  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();

  Texture *corner = GLOB_SCENE->Preloaded(Corner);
  GEngine->DrawFrame
  (
    corner,
    bgColor,
    Rect2DPixel(coX * w, coY * h, coW * w, coH * h)
  );


  Camera &cam = *GLOB_SCENE->GetCamera();
  Vector3Val dir = cam.Direction();

  // avoid singularity when heading up
  float xAngle;
  if( fabs(dir.X())+fabs(dir.Z())>1e-3 )
    xAngle = atan2(dir.X(), dir.Z());
  else
    xAngle=0;
  xAngle *= 2 * INV_H_PI;
  for (int i=-2; i<=2; i++)
  {
    float xLeft = xAngle + i;
    int xBase = toIntFloor(xLeft);
    if (xLeft - xBase >= 1.0) xBase++;
    int xBaseMod4 = xBase&(4-1);
    xLeft = xLeft + (xBaseMod4-xBase);
    xBase = xBaseMod4;
    PreloadedTexture seg[4]={Compass180,Compass270,Compass000,Compass090};
    texture = GLOB_SCENE->Preloaded(seg[xBase]);
    float xOffset = xLeft - xBase;
    mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
    float xScreen = (coX + 0.5 * coW + 0.25 * coW * (i - xOffset)) * w;
    GEngine->Draw2D(mip, compassColor, Rect2DPixel(xScreen, coY * h, 0.25 * coW * w, coH * h), Rect2DPixel(coX * w, coY * h, coW * w, coH * h), coShadow);
    //GEngine->TextBank()->ReleaseMipmap();
  }

  Rect2DPixel compassClip(coX*w,0,coW*w,h);
  float ySize=coH*h*0.25;
  float xSize=ySize*w/h*(3.0/4);
  float xPos=(coX+coW*0.5)*w;
  PackedColor col=compassDirColor;
  GEngine->DrawLine
  (
    Line2DPixel(xPos,coY*h,xPos+xSize,coY*h-ySize),
    col,col,compassClip
  );
  GEngine->DrawLine
  (
    Line2DPixel(xPos,coY*h,xPos-xSize,coY*h-ySize),
    col,col,compassClip
  );

  int weapon = -1;
  TurretContext context; 
  if (vehicle->GetPrimaryGunnerTurret(context)) weapon = context._weapons->ValidatedCurrentWeapon();
  if (weapon >= 0)
  {
    // draw turret direction
    Vector3Val dir = vehicle->DirectionWorldToModel(
      vehicle->GetWeaponDirection(vehicle->RenderVisualState(), context, weapon));

    if( dir.SquareSizeXZ()>0.1 )
    {
      xAngle = atan2(dir.X(), dir.Z());
      float xPos = (coX + 0.5 * coW + xAngle * coW * INV_W) * w;
      PackedColor col=compassTurretDirColor;
      GEngine->DrawLine
      (
        Line2DPixel(xPos,coY*h,xPos+xSize,coY*h-ySize),
        col,col,compassClip
      );
      GEngine->DrawLine
      (
        Line2DPixel(xPos,coY*h,xPos-xSize,coY*h-ySize),
        col,col,compassClip
      );
    }
  }
}

TypeIsSimple(CStatic *)

extern void PositionToAA11(Vector3Val pos, char *buffer);

#if _VBS3
  //used for TempMax, ContrastMax
  #include "../d3d9/ShaderSources_3_0/common.h"
#endif

#if _VBS3_LASE_LEAD
void InGameUI::DrawLaserRange(const TurretContext &context)
{
  if (!_laserRange || !context._turret) return;
  if(!context._turretType || !context._turretType->_canLase) return;

  if(GWorld->GetCameraType() != CamGunner) return;

  int range = context._turret->_laseDistance;

  char buffer[256];
  bool dirty = false;

  Turret::LLStatus status = context._turret->GetLaseStatus();
  PackedColor color = barRedColor;
  switch(status)
  {
  case Turret::LLError: color = barRedColor;
    break;
  case Turret::LLAdjusting: color = barYellowColor;
    break;
  case Turret::LLReady: color = barGreenColor;
    break;
  }
/*
  if (_laserRange->range)
  {		
    if (_laserRange->range->SetText((const char *)LocalizeString(IDS_UI_RANGE))) dirty = true;
    _laserRange->range->SetColor(color);
  }
*/
  if (_laserRange->rangeNum)
  {		
    if (range > 0)
      sprintf(buffer, "%04d", range);		
    else
      sprintf(buffer, "0000");		

    _laserRange->rangeNum->SetColor(color);
    if (_laserRange->rangeNum->SetText(buffer)) dirty = true;
  }

  float alpha = 1.0;

  _laserRange->DrawHUD(unconst_cast(context._turret->GetOwner()), alpha);
}
#endif


void InGameUI::DrawUnitInfo(EntityAIFull *vehicle)
{
  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return;
#if _VBS2
  if(agent->GetPerson()->IsPersonalItemsEnabled())
    vehicle = agent->GetPerson();
  else
#endif
    vehicle = agent->IsInCargo() ? agent->GetPerson() : agent->GetVehicle(); 
  
  const VehicleType *type = vehicle->GetType();
  Transport *transport = dyn_cast<Transport>(vehicle);

  PackedColor color, barBlinkColor;
  if (_blinkState)
    barBlinkColor = barBlinkOnColor;
  else
    barBlinkColor = barBlinkOffColor;

  // resize 
  const AutoArray<RStringB> &infos = type->GetUnitInfoTypes();
  int n = infos.Size();
  _lastUnitInfos.Resize(n);
  if (n == 0) return;

  for (int i=0; i<n; i++)
  {
    // create and load the dialogs
    UnitInfoDesc &desc = _lastUnitInfos[i];
    if (!desc._display) desc._display =  new DisplayUnitInfo(NULL);
    if (stricmp(desc._name, infos[i]) != 0)
    {
      desc._name = infos[i];
      desc._display->Reload(Pars >> "RscInGameUI" >> desc._name);
      desc._changed = Glob.uiTime;
    }
    
    DisplayUnitInfo *disp = desc._display;

    // reset the dialog size to original
    CStatic *bg = disp->background;
    if (bg) bg->SetPos(bg->X(), bg->Y(), disp->backgroundWidth, disp->backgroundHeight);

    // set the controls

    char buffer[256];
    Rank rank = agent->GetPerson()->GetRank();
    bool dirty = false; 

    if (disp->time)
    {
      disp->time->SetTime(Glob.clock);
      dirty = true; // ???
    }
    if (disp->date)
    {
      Glob.clock.FormatDate(LocalizeString(IDS_DATE_FORMAT), buffer); 
      if (disp->date->SetText(buffer)) dirty = true;
    }
    if (disp->name)
    {
      if (disp->name->SetText(agent->GetPerson()->GetPersonName())) dirty = true;
    }

    if (disp->unit)
    {
      buffer[0] = 0;
      if (agent->GetUnit() && agent->GetGroup())
      {
        sprintf(
          buffer,
          "%d %s: %s",
          agent->GetUnit()->ID(),
          agent->GetGroup()->GetName(),
          (const char *)LocalizeString(IDS_PRIVATE + rank)
          );
      }
      if (disp->unit->SetText(buffer)) dirty = true;
    }

    if (disp->vehicle)
    {
      if (disp->vehicle->SetText(vehicle->GetType()->GetShortName())) dirty = true;
    }

    if (disp->speed)
    {
      float speed = toInt(3.6 * vehicle->RenderVisualState().ModelSpeed().Z());
#if _VBS3 //speed in knots
      if(vehicle->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
        speed *= 0.53996;
#endif
      sprintf(
        buffer,
        LocalizeString(IDS_UI_SPEED),
        speed
        );
      if (disp->speed->SetText(buffer)) dirty = true;
      if(vehicle->DetectStall()> stallWarning) disp->speed->SetTextColor(PackedColor(222,60,60,255));
      else disp->speed->SetTextColor(ftColor);
    }

    if (disp->radarRange)
    {
      float radarRange = 4000.0f;

      sprintf(
        buffer,
        LocalizeString(IDS_UI_RADARRANGE),
        radarRange
        );
      if (disp->radarRange->SetText(buffer)) dirty = true;
    }

    if (disp->alt)
    {
      Vector3Val pos = vehicle->RenderVisualState().Position();
      float y = pos.Y() + vehicle->GetShape()->Min().Y();
#if _VBS2
      float y0 = GLOB_LAND->SurfaceYAboveWater(pos.X(), pos.Z()); 
#else
      float y0 = GLOB_LAND->RoadSurfaceYAboveWater(pos.X(), pos.Z()); 
#endif
      float h = y - y0;
      saturate(h, 0, 99999);
#if _VBS3 //height in feet
      h *= 3.28084;
#endif
      sprintf(
        buffer,
        LocalizeString(IDS_UI_ALT),
        h
        );
      if (disp->alt->SetText(buffer)) dirty = true;
    }

    if (disp->heading)
    {
      int canSee = 0;
      if (transport)
      {
        if (agent == transport->ObserverUnit()) canSee = transport->Type()->_commanderCanSee;
        else if (agent == transport->PilotUnit()) canSee = transport->Type()->_driverCanSee;
        else if (agent == transport->GunnerUnit()) canSee = transport->Type()->_gunnerCanSee;
      }
      if (canSee & CanSeeCompass)
      {
        disp->heading->ShowCtrl(true);

        Camera &cam = *GLOB_SCENE->GetCamera();
        Vector3Val dir = cam.Direction();

        // avoid singularity when heading up
        float angle;
        if (fabs(dir.X()) + fabs(dir.Z()) > 1e-3)
          angle = atan2(dir.X(), dir.Z());
        else
          angle = 0;

        int degAngle = toInt(angle * (180.0f / H_PI));
        if (degAngle < 0) degAngle += 360;
        BString<16> buffer;
        sprintf(buffer, "%03d", degAngle);

        if (disp->heading->SetText(cc_cast(buffer))) dirty = true;
      }
      else
        disp->heading->ShowCtrl(false);
    }

    if (disp->alt_wanted)
    {
      buffer[0] = 0;
      HelicopterAuto* heli = dyn_cast<HelicopterAuto>(vehicle);
#if _VBS3
      bool enabled = Glob.config.IsEnabled(DTSimplifiedHeliModel);
#else
      bool enabled = true;
#endif
      if (heli && enabled)
      {
        sprintf
          (
          buffer,
          LocalizeString(IDS_UI_ALT_WANTED),
          heli->GetFlyingHeight() * 3.28084 //format in feet intervals
          );
      }
      if (disp->alt_wanted->SetText(buffer)) dirty = true;
    }
    if (disp->speed_wanted)
    {
      HelicopterAuto* heli = dyn_cast<HelicopterAuto>(vehicle);
      buffer[0] = 0;
#if _VBS3
      bool enabled = Glob.config.IsEnabled(DTSimplifiedHeliModel);
#else
      bool enabled = true;
#endif
      if (heli && enabled)
      {
        sprintf(
          buffer,
          LocalizeString(IDS_UI_SPEED_WANTED),
          heli->GetSpeedWanted() * 3.6 * 0.53996 //format in knots
          );
      }
      if (disp->speed_wanted->SetText(buffer)) dirty = true;
    }

    if (disp->position)
    {
      PositionToAA11(vehicle->RenderVisualState().Position(), buffer);
      if (disp->position->SetText(buffer)) dirty = true;
    }

    if (disp->opticZoom)
    {
      buffer[0]=0;
      if( GWorld->GetCameraType() == CamGunner)
      {
        const Camera *camera = GScene->GetCamera();
        if (camera && vehicle->GetOpticsModel(agent->GetPerson()))
          sprintf(buffer, "%.1fx", 0.25f / camera->Left());
      }
      if (disp->opticZoom->SetText(buffer)) dirty = true;
    }

#if _VBS3
    if(disp->override_status)
    {
      TurretContext context;
      RString text;
      if(transport && transport->FindTurret(agent->GetPerson(), context))
      {
        if(context._turret)
        {
          if(context._turret->IsOverridden())
            text = LocalizeString(IDS_TURRET_OVERRIDDEN);
          else
            if(context._turret->IsOverriding())
              text = LocalizeString(IDS_TURRET_OVERRIDING);
            else
              if(context._turret->_showOverrideOptics)
                text = LocalizeString(IDS_TURRET_GUNNER_VIEW);
        }
      }
      if(disp->override_status->SetText(text)) dirty = true;
    }

    //AVRS show nick and bank of helicopters
    if(transport)
    {
      if(disp->nick && disp->nick_val)
      {
        Vector3 front = transport->Direction();
        float nick = -asin(front.Y()) * (180.0f / H_PI);
        sprintf(buffer, "%.1f", nick);
        if(disp->nick_val->SetText(buffer)) dirty = true;
        PackedColor color = abs(nick) < 10.0f ? ftColor : enemyColor;
        disp->nick_val->SetColor(color);
        disp->nick->SetColor(color);
      }

      if(disp->bank && disp->bank_val)
      {
        Vector3 aside = transport->DirectionAside();
        float bank = asin(aside.Y()) * (180.0f / H_PI);
        sprintf(buffer, "%.1f", bank);
        if(disp->bank_val->SetText(buffer)) dirty = true;
        PackedColor color = abs(bank) < 10.0f ? ftColor : enemyColor;
        disp->bank_val->SetColor(color);
        disp->bank->SetColor(color);
      }
    }


#if _VBS3_TI
    if(agent->GetPerson()->GetVisionMode() == VMTi)
    {
      if(disp->ti_background)      disp->ti_background->ShowCtrl();
      if(disp->ti_brightness_txt)  disp->ti_brightness_txt->ShowCtrl();
      if(disp->ti_contrast_txt)    disp->ti_contrast_txt->ShowCtrl();
      if(disp->ti_contrast)        disp->ti_contrast->ShowCtrl();

      if(disp->ti_mode)
      {
        disp->ti_mode->ShowCtrl();
        int mode = agent->GetPerson()->GetTiModus();
        //TODO: query engine
        char* buf[7]={"WHITE - HOT", "BLACK - HOT", "GREEN - HOT", "BLACK - HOT", "ORANGE - HOT", "BLACK - HOT", "COLOR"};
        saturate(mode, 0, 6);
        if(disp->ti_mode->SetText(buf[mode])) dirty = true;
      }

      if(disp->ti_brightness)      
      {
        disp->ti_brightness->ShowCtrl();
        float bright= GEngine->GetTIBrightness();
        bright /= (TempMax - TempMin);
        saturate(bright, 0, 1);
        disp->ti_brightness->SetPos(bright);
      }

      if(disp->ti_contrast)        
      {
        disp->ti_contrast->ShowCtrl();
        float contr= GEngine->GetTIContrast();
        contr /= ContrastMax;
        saturate(contr, 0, 1);
        disp->ti_contrast->SetPos(contr);
      }

      if(disp->ti_autocontrast)
        disp->ti_autocontrast->ShowCtrl(GEngine->GetTIAutoBC());

    }
    else
    {
      if(disp->ti_background)        disp->ti_background->ShowCtrl(false);
      if(disp->ti_mode)              disp->ti_mode->ShowCtrl(false);
      if(disp->ti_brightness_txt)    disp->ti_brightness_txt->ShowCtrl(false);
      if(disp->ti_brightness)        disp->ti_brightness->ShowCtrl(false);
      if(disp->ti_contrast_txt)      disp->ti_contrast_txt->ShowCtrl(false);
      if(disp->ti_contrast)          disp->ti_contrast->ShowCtrl(false);
      if(disp->ti_autocontrast)      disp->ti_autocontrast->ShowCtrl(false);
    }
#endif //_VBS3_TI
#endif //_VBS3

    if (disp->valueExp)
    {
      float expCur = agent->GetPerson()->GetExperience() - AI::ExpForRank(rank);
      float expMax = rank >= RankColonel ?
        AI::ExpForRank(RankColonel) :
      AI::ExpForRank((Rank)(rank + 1)) - AI::ExpForRank(rank);
      Assert(expMax > 0);
      float expCoef = 0.9 * (expCur / expMax);
      saturate(expCoef, 0, 1);
      disp->valueExp->SetPos(expCoef);
    }

    if (disp->formation)
    {
      AIUnit *unit = agent->GetUnit();
      AISubgroup *subgroup = unit ? unit->GetSubgroup() : NULL;
      RString text;
      if (subgroup) text = LocalizeString(IDS_COLUMN + subgroup->GetFormation());
      if (disp->formation->SetText(text)) dirty = true;
    }

    if (disp->combatMode)
    {
      if (disp->combatMode->SetText(LocalizeString(IDS_IGNORE + agent->GetSemaphore()))) dirty = true;
    }

    if (disp->valueHealth)
    {
      float health = 0;
      Vehicle *person = agent->GetPerson();
      if (person)
      {
        health = 1 - person->GetTotalDamage();
        saturate(health, 0, 1);
      }
      if (health > 0.5)
        color = barGreenColor;
      else if (health > 0.3)
        color = barYellowColor;
      else if (health > 0.15)
        color = barRedColor;
      else
        color = barBlinkColor;
      disp->valueHealth->SetPos(health);
      disp->valueHealth->SetBarColor(color);
    }


    if (disp->valueArmor && vehicle->GetType())
    {
      RString state;
      disp->valueArmor->Clear();
      AutoArray<int> textureUV = vehicle->GetHitTextureUVForDisplay();
      for( int i=0; i<textureUV.Size(); i++ )
      {
        color = (ColorFromHit(vehicle->GetHitForDisplay(i)));

        int index = disp->valueArmor->AddString(RString());
        disp->valueArmor->SetValue(index,textureUV[i]);
        disp->valueArmor->SetFtColor(index,color);
      }
    }

    if (disp->valueFuel)
    {
      float fuel = 0;
      float fuelCapacity = vehicle->GetType()->GetFuelCapacity();
      if (fuelCapacity > 0)
      {
        fuel = vehicle->GetRenderVisualStateFuel() / fuelCapacity;
        saturate(fuel, 0, 1);
      }
      if (fuel > 0.5)
        color = barGreenColor;
      else if (fuel > 0.3)
        color = barYellowColor;
      else if (fuel > 0.15)
        color = barRedColor;
      else
        color = barBlinkColor;
      disp->valueFuel->SetPos(fuel);
      disp->valueFuel->SetBarColor(color);
    }

    if (disp->cargoMan)
    {
      if (transport && transport->GetMaxManCargo() > 0)
      {
        disp->cargoMan->ShowCtrl(true);
        sprintf(buffer, LocalizeString(IDS_UI_CARGO_INF), transport->GetManCargoSize());
        if (disp->cargoMan->SetText(buffer)) dirty = true;
      }
      else
      {
        disp->cargoMan->ShowCtrl(false);
      }
    }

    if (disp->cargoFuel)
    {
      if (transport && transport->GetMaxFuelCargo() > 0)
      {
        disp->cargoFuel->ShowCtrl(true);
        sprintf(buffer, LocalizeString(IDS_UI_CARGO_FUEL), transport->GetFuelCargo());
        if (disp->cargoFuel->SetText(buffer)) dirty = true;
      }
      else
      {
        disp->cargoFuel->ShowCtrl(false);
      }
    }

    if (disp->cargoRepair)
    {
      if (transport && transport->GetMaxRepairCargo() > 0)
      {
        disp->cargoRepair->ShowCtrl(true);
        sprintf(buffer, LocalizeString(IDS_UI_CARGO_REPAIR), transport->GetRepairCargo());
        if (disp->cargoRepair->SetText(buffer)) dirty = true;
      }
      else
      {
        disp->cargoRepair->ShowCtrl(false);
      }
    }

    if (disp->cargoAmmo)
    {
      if (transport && transport->GetMaxAmmoCargo() > 0)
      {
        disp->cargoAmmo->ShowCtrl(true);
        sprintf(buffer, LocalizeString(IDS_UI_CARGO_AMMO), transport->GetAmmoCargo());
        if (disp->cargoAmmo->SetText(buffer)) dirty = true;
      }
      else
      {
        disp->cargoAmmo->ShowCtrl(false);
      }
    }

    float crewWidth = disp->backgroundWidth;
    VerySmallArray<CStatic *, sizeof(int) + 3 * sizeof(CStatic *)> crew;
    if (disp->commander) crew.Add(disp->commander);
    if (disp->gunner) crew.Add(disp->gunner);
    if (disp->driver) crew.Add(disp->driver);
    if (crew.Size() == 3)
    {
      int pos = 0;
      float h = 0;
      float w = 0;
      if (transport && GWorld->GetMode() == GModeNetware)
      {
        AIBrain *unit = transport->ObserverUnit();
        if (unit && unit->IsAnyPlayer())
        {
          crew[pos]->ShowCtrl(true);
          crew[pos]->SetText(unit->GetPerson()->GetPersonName());
          saturateMax(w, crew[pos]->GetTextSize());
          if (disp->_updateHeightByCrew) h += crew[pos]->H();
          pos++;
        }
        unit = transport->GunnerUnit();
        if (unit && unit->IsAnyPlayer())
        {
          crew[pos]->ShowCtrl(true);
          crew[pos]->SetText(unit->GetPerson()->GetPersonName());
          saturateMax(w, crew[pos]->GetTextSize());
          if (disp->_updateHeightByCrew) h += crew[pos]->H();
          pos++;
        }
        unit = transport->PilotUnit();
        if (unit && unit->IsAnyPlayer())
        {
          crew[pos]->ShowCtrl(true);
          crew[pos]->SetText(unit->GetPerson()->GetPersonName());
          saturateMax(w, crew[pos]->GetTextSize());
          if (disp->_updateHeightByCrew) h += crew[pos]->H();
          pos++;
        }
      } 
      for (;pos<crew.Size();pos++) crew[pos]->ShowCtrl(false);
      CStatic *bg = disp->background;
      if (bg)
      {
        float offset = crew[0]->X() - bg->X();
        if (disp->_updateWidthByCrew) saturateMax(crewWidth, w + 2 * offset);
        for (int i=0; i<crew.Size(); i++)
        {
          CStatic *ctrl = crew[i];
          ctrl->SetPos(ctrl->X(), ctrl->Y(), w, ctrl->H());
        }

        bg->SetPos(bg->X(), bg->Y(), crewWidth, disp->backgroundHeight + h);
      }
    }

    // primary show own weapons, otherwise primary weapon state
    int weapon = -1;
    TurretContext context;
    int weaponGunner = -1;
    TurretContext contextGunner;
    int counterMeasures = -1;
    TurretContext contextCM;
    if (transport && transport->IsManualFire() && agent == transport->CommanderUnit() && vehicle->GetPrimaryGunnerTurret(context))
    {
      // when manual fire is on, commander is controlling gunner's weapons (even when has own weapons)
      weapon = context._weapons->ValidatedCurrentWeapon();
    }
#if _VBS3 //Commander override
    else if (vehicle->FindOpticsTurret(agent->GetPerson(), context) && vehicle->CanFireUsing(context))
#else
    else if (vehicle->FindTurret(agent->GetPerson(), context) && vehicle->CanFireUsing(context))
#endif
    {
      weapon = context._weapons->ValidatedCurrentWeapon();
#if _VBS3_LASE_LEAD
      DrawLaserRange(context);
#endif
    }
    if (vehicle->GetPrimaryGunnerTurret(contextGunner))
    {
      weaponGunner = contextGunner._weapons->ValidatedCurrentWeapon();
    }
    if (weapon >= 0)
    {
      // non-default weapon is always shown
      //if (weapon != 0) dirty = true; // not any more

      int ammoCur = 0;
      int maxAmmoCur = 0;
      bool noAmmo = true;
      //    int ammoSum = 0;
      //    int maxAmmoSum = 0;
      int magazines = 0;
      float reload = 0;
      float reloadTime = 0;
      float reloadTimeMax = 0;
      RString displayName;

      const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
      const MuzzleType *muzzle = slot._muzzle;
      const WeaponModeType *mode = slot._weaponMode;
      RString weaponMode =  mode->_displayName;
      displayName = slot._muzzle->_displayName; 

      if(_weaponInfoAlpha > 10) dirty = true;
      _weaponInfoAlpha = 0;

      const Magazine *magazine = slot._magazine;
      if (!magazine)
      {
        // find magazine of first compatible type
        for (int i=0; i<muzzle->_magazines.Size(); i++)
        {
          const MagazineType *type = muzzle->_magazines[i];
          for (int j=0; j<context._weapons->_magazines.Size(); j++)
          {
            const Magazine *mag = context._weapons->_magazines[j];
            if (!mag) continue;
            if (mag->_type != type) continue;
            if (mag->GetAmmo() == 0) continue;
            magazines++;
            if (!magazine)
            {
              magazine = mag;
            }
          }
          //if (magazine) break; // want all magazines
        }

        if (magazine)
        {
          maxAmmoCur = magazine->_type->_maxAmmo;
        }
        else if (slot._muzzle->_magazines.Size() > 0)
        {
          maxAmmoCur = slot._muzzle->_magazines[0]->_maxAmmo;
        }
      }
      else // FIX: wrong calculation of magazines
      {
        ammoCur = magazine->GetAmmo();
        maxAmmoCur = magazine->_type->_maxAmmo;
        if (magazine->_reloadMagazine > 0)
        {
          reload = 1;
          reloadTime = magazine->_reloadMagazine + magazine->_reload * mode->_reloadTime;
          reloadTimeMax = magazine->_reloadMagazineTotal + mode->_reloadTime;
        }
        else 
        {
          reload = magazine->_reload;
          reloadTime =  magazine->_reload * mode->_reloadTime;
          reloadTimeMax = mode->_reloadTime;
        }

        // find magazine of first compatible type
        for (int j=0; j<muzzle->_magazines.Size(); j++)
        {
          const MagazineType *allowedMagazine = muzzle->_magazines[j];
          // reserve magazines
          for (int i=0; i<context._weapons->_magazines.Size(); i++)
          {
            const Magazine *reserve = context._weapons->_magazines[i];
            if (!reserve || (reserve == magazine)) continue;
            if (reserve->_type == allowedMagazine)
            {
              if (reserve->GetAmmo() > 0) magazines++;
            }
          }
        }
        noAmmo = ammoCur == 0;
      }

      if (magazine && magazine->_type->_maxAmmo == 1)
      {
        ammoCur = magazines;
        if (slot._magazine && slot._magazine->GetAmmo() > 0) ammoCur++;
        magazines = 0;
      }
      
      if (maxAmmoCur > 0)
      {
        color = _weaponInfoColor;

        if (/*ammoCur == 0*/noAmmo || vehicle->IsActionInProgress(MFReload))
          _weaponInfoColor = _weaponUnloadColor;
        else if (reload <= 0)
          _weaponInfoColor = _weaponReadyColor;
        else if (reload <= 0.2)
          _weaponInfoColor = _weaponPrepareColor;
        else
          _weaponInfoColor = _weaponUnloadColor;

        if (disp->valueReload)
        {
          if(reload > 0.001f && !noAmmo && reloadTimeMax > 0.5f) 
          {
            disp->valueReload->ShowCtrl(true);
            disp->valueReload->SetPos(reloadTime);
            float range = disp->valueReload->GetRange();
            disp->valueReload->SetRange(0,floatMax(range,reloadTimeMax));
          }
          else 
          {
            disp->valueReload->SetRange(0,0);
            disp->valueReload->ShowCtrl(false);
          }
        }

        if (disp->weapon)
        {
          disp->weapon->ShowCtrl(true);
          disp->weapon->SetTextColor(color);
          if (disp->weapon->SetText(displayName)) dirty = true;
        }
        if (disp->ammo)
        {
          disp->ammo->ShowCtrl(true);
          disp->ammo->SetTextColor(color);
          //        sprintf(buffer, "%d", ammoSum);
          if (magazines > 0)
            sprintf(buffer, "%d | %d", ammoCur, magazines);
          else
            sprintf(buffer, "%d", ammoCur);
          if(Glob.config.IsEnabled(DTHUDPerm))
          {
            if (disp->ammo->SetText(buffer)) dirty = true;
            // non-default weapon is always shown
            if (weapon != 0) dirty = true;
          }
          else disp->ammo->SetText(buffer);    

          if((ammoCur + magazines <=0) && displayName.GetLength()<=0)
          {
            disp->weapon->ShowCtrl(false);
          }
        }
        if (disp->ammoMode)
        {
          disp->ammoMode->ShowCtrl(true);
          disp->ammoMode->SetTextColor(color);
        
          RString modeAndMag;
          if (weaponMode!=displayName && !weaponMode.IsEmpty())
          {
            if (magazine && !magazine->_type->_displayNameShort.IsEmpty())
              modeAndMag = weaponMode+RString(" | ")+magazine->_type->_displayNameShort;
            else 
              modeAndMag = weaponMode;
          }
          else
          {
            if (magazine && !magazine->_type->_displayNameShort.IsEmpty())
            {
              modeAndMag = magazine->_type->_displayNameShort;
            }
          }  
          if (disp->ammoMode->SetText(modeAndMag)) dirty = true;

          if((ammoCur + magazines <=0) && modeAndMag.GetLength()<=0)
          {
            disp->ammoMode->ShowCtrl(false);
          }
        }
        
#if _VBS3
        if (disp->magazine && magazine)
        {
          disp->magazine->ShowCtrl(true);
          disp->magazine->SetTextColor(color);
          if (disp->magazine->SetText(magazine->_type->_displayName)) dirty = true;
        }
#endif
      }
      else
      {
        // no ammo or no muzzle
        if (disp->weapon)
        {
          disp->weapon->ShowCtrl(true);
          disp->weapon->SetTextColor(_weaponReadyColor);
          if (disp->weapon->SetText(displayName)) dirty = true;
        }
#if _VBS3
        if (disp->magazine && magazine)
        {
          if (disp->magazine->SetText(RString())) dirty = true;
        }
#endif
        if (disp->ammo)
        {
          //if (disp->ammo->SetText(RString())) dirty = true;
          disp->ammo->SetText(RString());
        }
        if (disp->ammoMode)
        {
          RString modeAndMag;
          if (weaponMode!=displayName && !weaponMode.IsEmpty())
          {
            if (magazine > 0 && !magazine->_type->_displayNameShort.IsEmpty())
              modeAndMag = weaponMode+RString(" | ")+magazine->_type->_displayNameShort;
            else 
              modeAndMag = weaponMode;
          }
          else
          {
            if (magazine > 0 && !magazine->_type->_displayNameShort.IsEmpty())
            {
              modeAndMag = magazine->_type->_displayNameShort;
            }
          }  
          if (disp->ammoMode->SetText(modeAndMag)) dirty = true;

          if((ammoCur + magazines <=0) && modeAndMag.GetLength()<=0)
          {
            disp->ammoMode->ShowCtrl(false);
          }
        }
      }

      // update the width - ensure the weapon name will fit always
      CStatic *bg = disp->background;
      CStatic *weapon = disp->weapon;
      CStatic *ammo = disp->ammo;
      if (bg && weapon && ammo && disp->_updateWidthByWeapon)
      {
        float offset = weapon->X() - bg->X();
        float weaponWidth = weapon->GetTextSize();
        float ammoWidth = ammo->GetTextSize();
        float totalWidth = offset + weaponWidth + offset + ammoWidth + offset;
        if (totalWidth < crewWidth)
        {
          float diff = crewWidth - totalWidth;
          weaponWidth += 0.5 * diff;
          ammoWidth += 0.5 * diff;
          totalWidth += diff;
        }
        weapon->SetPos(weapon->X(), weapon->Y(), weaponWidth, weapon->H());
        ammo->SetPos(weapon->X() + weaponWidth + offset, ammo->Y(), ammoWidth, ammo->H());
        bg->SetPos(bg->X(), bg->Y(), totalWidth, bg->H());
      }
    }
    else
    {
      if (disp->weapon) disp->weapon->ShowCtrl(false);
      if (disp->ammo) disp->ammo->ShowCtrl(false);
      if (disp->ammoMode) disp->ammoMode->ShowCtrl(false);
      _weaponInfoAlpha = 0;
    }

    if(transport && transport->CommanderUnit()->GetPerson() && agent == transport->CommanderUnit() && vehicle->FindTurret(agent->GetPerson() ,contextCM))
    {
      counterMeasures = contextCM._weapons->ValidatedCurrentCounterMeasures(); 

      if (counterMeasures >= 0)
      {
        int ammoCur = 0;
        int maxAmmoCur = 0;
        bool noAmmo = true;
        //    int ammoSum = 0;
        //    int maxAmmoSum = 0;
        int magazines = 0;
        float reload = 0;


        const MagazineSlot &slot = contextCM._weapons->_magazineSlots[counterMeasures];
        const Magazine *magazine = slot._magazine;
        const MuzzleType *muzzle = slot._muzzle;
        const WeaponModeType *mode = slot._weaponMode;
        RString cmMode =  mode->_displayName;

        if (!magazine)
        {
          // find magazine of first compatible type
          for (int i=0; i<muzzle->_magazines.Size(); i++)
          {
            const MagazineType *type = muzzle->_magazines[i];
            for (int j=0; j<contextCM._weapons->_magazines.Size(); j++)
            {
              const Magazine *mag = contextCM._weapons->_magazines[j];
              if (!mag) continue;
              if (mag->_type != type) continue;
              if (mag->GetAmmo() == 0) continue;
              magazines++;
              if (!magazine)
              {
                magazine = mag;
              }
            }
            //if (magazine) break; // want all magazines
          }

          if (magazine)
          {
            maxAmmoCur = magazine->_type->_maxAmmo;
          }
          else if (slot._muzzle->_magazines.Size() > 0)
          {
            maxAmmoCur = slot._muzzle->_magazines[0]->_maxAmmo;
          }
        }
        else // FIX: wrong calculation of magazines
        {
          ammoCur = magazine->GetAmmo();
          maxAmmoCur = magazine->_type->_maxAmmo;
          if (magazine->_reloadMagazine > 0) reload = 1;
          else reload = magazine->_reload;

          // find magazine of first compatible type
          for (int j=0; j<muzzle->_magazines.Size(); j++)
          {
            const MagazineType *allowedMagazine = muzzle->_magazines[j];
            // reserve magazines
            for (int i=0; i<contextCM._weapons->_magazines.Size(); i++)
            {
              const Magazine *reserve = contextCM._weapons->_magazines[i];
              if (!reserve || (reserve == magazine)) continue;
              if (reserve->_type == allowedMagazine)
              {
                if (reserve->GetAmmo() > 0) magazines++;
              }
            }
          }
          noAmmo = ammoCur == 0;
        }

        if (maxAmmoCur > 0)
        {
          if (/*ammoCur == 0*/noAmmo || vehicle->IsActionInProgress(MFReload))
            color = _weaponUnloadColor;
          else if (reload <= 0)
            color = _weaponReadyColor;
          else if (reload <= 0.2)
            color = _weaponPrepareColor;
          else
            color = _weaponUnloadColor;
        }

        if(disp->counterMeasuresAmmo)
        {
          disp->counterMeasuresAmmo->ShowCtrl(true);
          disp->counterMeasuresAmmo->SetTextColor(color);
          //        sprintf(buffer, "%d", ammoSum);
          if (magazines > 0)
            sprintf(buffer, "%d | %d", ammoCur, magazines);
          else
            sprintf(buffer, "%d", ammoCur);
          if(Glob.config.IsEnabled(DTHUDPerm))
          {
            if (disp->counterMeasuresAmmo->SetText(buffer)) dirty = true;
            // non-default weapon is always shown
            if (counterMeasures != 0) dirty = true;
          }
          else disp->counterMeasuresAmmo->SetText(buffer);    
        }
        if(disp->counterMeasuresMode)
        {
          disp->counterMeasuresMode->ShowCtrl(true);
          disp->counterMeasuresMode->SetTextColor(color);
          if (disp->counterMeasuresMode->SetText(cmMode)) dirty = true;
        }
      }
      else
      {
        if (disp->counterMeasuresAmmo) disp->counterMeasuresAmmo->ShowCtrl(false);
        if (disp->counterMeasuresMode) disp->counterMeasuresMode->ShowCtrl(false);
      }
    }
    else
    {
      if (disp->counterMeasuresAmmo) disp->counterMeasuresAmmo->ShowCtrl(false);
      if (disp->counterMeasuresMode) disp->counterMeasuresMode->ShowCtrl(false);
    }

    if (disp->gunnerWeapon)
    {
      if ( ( contextGunner._gunner != agent->GetPerson() && !transport->IsManualFire()) && weaponGunner >= 0)
      {
        const MagazineSlot &slot = contextGunner._weapons->_magazineSlots[weaponGunner];
        const Magazine *magazine = slot._magazine;
        if(magazine)
        {
          RString displayName = slot._muzzle->_displayName; 

          float reload = 0;
          if (magazine->_reloadMagazine > 0) reload = 1;
          else reload = magazine->_reload;

          if(magazine->GetAmmo()<=0)
            color = _weaponUnloadColor;
          else if (reload <= 0)
            color = _weaponReadyColor;
          else if (reload <= 0.2)
            color = _weaponPrepareColor;
          else
            color = _weaponUnloadColor;

          disp->gunnerWeapon->ShowCtrl(true);
          disp->gunnerWeapon->SetTextColor(color);
          if (disp->gunnerWeapon->SetText(displayName)) dirty = true;
        }
      }
      else
      {
        if (disp->gunnerWeapon) disp->gunnerWeapon->ShowCtrl(false);
      }
    }


    if (dirty) 
      desc._changed = Glob.uiTime;

    float age = 0;
    if (vehicle->GetType()->_hideUnitInfo)
      age = Glob.uiTime - desc._changed;

    if (age < piDimEndTime && (
          (disp->weapon && disp->weapon->IsVisible())
       || (disp->gunnerWeapon && disp->gunnerWeapon->IsVisible())
       || (disp->speed && disp->speed->IsVisible())))
    {
      float alpha = 1.0;
      if (age > piDimStartTime)
        alpha = (piDimEndTime - age) / (piDimEndTime - piDimStartTime);
      if (disp->IsReady())
        disp->DrawHUD(vehicle, alpha);

      _weaponInfoAlpha = alpha;

      CStatic *bg = disp->background;
      if (bg) _hintTop = bg->Y() + bg->H() + 0.02;
    }
  }
}

void InGameUI::DrawWeaponInfo(const Camera &camera)
{
  AIBrain *agent = GWorld->FocusOn();
  if (!agent || !agent->GetVehicle()) return;
  EntityAIFull *vehicle = agent->GetVehicle(); 

  bool scope = (GWorld->GetCameraType() == CamGunner) && vehicle->EnableOptics(vehicle->RenderVisualState());

  TurretContext context;
  int weapon = -1;

  PackedColor colorElevation = PackedColorRGB(_weaponInfoColor, toIntFloor(_weaponInfoColor.A8() * _weaponInfoAlpha));

  if (vehicle->FindTurret(agent->GetPerson(), context))
  {
    // when manual fire is on, commander is controlling gunner's weapons (even when has own weapons)
    weapon = context._weapons->ValidatedCurrentWeapon();
  }
  else return;

  WeaponType *weaponType = NULL;
  if(agent->IsFreeSoldier())
  {
    if(weapon < 0) return;
    const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
    weaponType = slot._weapon;
  }
  else
    if(!agent->GetVehicleIn()) return;
  
  if(weaponType== NULL && context._turretType == NULL) return;

  const AutoArray<RStringB> &infos = (weaponType!= NULL)? weaponType->GetWeaponInfoTypes():context._turretType->GetTurretInfoTypes();
  int n = infos.Size();
  _lastWeaponInfos.Resize(n);
  if (n == 0) return;

  for (int i=0; i<n; i++)
  {
    bool dirty = false;
    // create and load the dialogs
    WeaponInfoDesc &desc = _lastWeaponInfos[i];
    if (!desc._display) desc._display =  new DisplayWeaponInfo(NULL);
    if (stricmp(desc._name, infos[i]) != 0)
    {
      desc._name = infos[i];
      desc._display->Reload(Pars >> "RscInGameUI" >> desc._name);
      desc._changed = Glob.uiTime;
    }

    DisplayWeaponInfo *disp = desc._display;
  
  
    if(disp->elevation)
    {
      disp->elevation->SetTextColor(colorElevation);

      char buffer[256]; sprintf(buffer,"");
      //Transport *transport = agent->GetVehicleIn();
      int weapon = context._weapons->ValidatedCurrentWeapon();
      if(agent->IsFreeSoldier() && agent->GetVehicle() && weapon >= 0)
      {
        const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
        const OpticsInfo &opticsInfo = slot._muzzle->_opticsInfo[slot._opticsMode];

        float distance = 0;
        if(opticsInfo._discreteDistance.Size() == 0)
        {
          distance= Interpolativ(agent->GetVehicle()->GetCameraFOV(), opticsInfo._opticsZoomMin, opticsInfo._opticsZoomMax,
            opticsInfo._distanceZoomMin, opticsInfo._distanceZoomMax);
        }
        else
        {
          MagazineSlot slot = context._weapons->_magazineSlots[weapon];
          distance =  opticsInfo._discreteDistance[std::min(slot._discreteDistanceIndex,opticsInfo._discreteDistance.Size()-1)];
        }
        sprintf(buffer, "%d", (int)distance);
        dirty = disp->elevation->SetText(buffer);
      }
      else if(weapon >= 0)
      {
        float distance = 0;
        const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
        const MuzzleType *muzzle = slot._muzzle;

        if (muzzle && (muzzle->_ballisticsComputer == 1 ||  muzzle->_ballisticsComputer == 2)
          && context._turretType->_discreteDistance.Size()>0 && context._turret->_distanceIndex >=0) 
        {
          disp->elevation->ShowCtrl(true);
          if(muzzle->_ballisticsComputer == 1 && GWorld->UI()->GetLockedTarget())
          {
            sprintf(buffer, " AUTO");
          }
          else
          {
            distance =  context._turretType->_discreteDistance[std::min(context._turret->_distanceIndex,context._turretType->_discreteDistance.Size()-1)];
            sprintf(buffer, "%d", (int)distance);
          }
          dirty = disp->elevation->SetText(buffer);
        }
        else disp->elevation->ShowCtrl(false);
      }
      else disp->elevation->ShowCtrl(false);

      if(disp->elevationText)
      {
        disp->elevationText->SetTextColor(colorElevation);
        //Transport *transport = agent->GetVehicleIn();
        if(!agent->IsFreeSoldier())
        {
          if(weapon >= 0)
          {
            const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
            const MuzzleType *muzzle = slot._muzzle;
            if (muzzle && (muzzle->_ballisticsComputer == 1 ||  muzzle->_ballisticsComputer == 2)
              && context._turretType->_discreteDistance.Size()>0 && context._turret->_distanceIndex >=0) 
            {
              disp->elevationText->ShowCtrl(true);
            }
            else disp->elevationText->ShowCtrl(false);
          }
          else disp->elevationText->ShowCtrl(false);
        }
      }
    }
    
    //distance from aimed target/ground
    if (disp->distance || disp->GPSTarget)
    {
      if(disp->distance) disp->distance->ShowCtrl(scope);
      if(disp->GPSTarget) disp->GPSTarget->ShowCtrl(scope);

      if(scope)
      {
        float distance = -1;
        char buffer[256];
        //distance to which we try to detect vehicles
        float MaxDist = 1500.0f;
        const float MaxDistGround = 5000.0f;
        /*Vector3 dir = agent->Direction();
        dir.Normalize();*/

        Matrix4 animTrans;
        Vector3 lPos = camera.Position();
        Vector3 lDir = camera.Direction().Normalized();

        Vector3 tmpEnd;
        float tGround = GLandscape->IntersectWithGroundOrSea(&tmpEnd, lPos, lDir, 0, MaxDistGround);
        MaxDist = floatMin(tGround,MaxDist);

        CollisionBuffer result;
        GLandscape->ObjectCollisionLine(vehicle->GetRenderVisualStateAge(), result, Landscape::FilterIgnoreOne(agent->GetVehicle()), lPos, lPos + lDir * MaxDist, 0.5f, ObjIntersectView, ObjIntersectFire);
        // select the nearest
        if (result.Size() > 0)
        {
          Vector3 tmpEnd = result[0].pos;
          float dist2 = lPos.Distance2(tmpEnd);
          float tmp2;

          for (int i = 1; i < result.Size(); i++)
          {
            CollisionInfo &collInfo = result[i];
            tmp2 = lPos.Distance2(collInfo.pos);
            if (tmp2 < dist2) tmpEnd = collInfo.pos, dist2 = tmp2;
          }
          distance = sqrtf(dist2);
        }
        else if(tGround < MaxDistGround)
        {
          distance = tGround;
        }

        if(distance>=0)
          sprintf(buffer,"%d", (int)distance);
        else
          sprintf(buffer,"--");

        if(disp->distance) disp->distance->SetText(buffer);

        if(distance>=0)
        {
          Vector3 targetPos = lPos + lDir*distance;
          PositionToAA11(targetPos, buffer);
        }
        else
          sprintf(buffer,"--");
        if(disp->GPSTarget) disp->GPSTarget->SetText(buffer);
      }
    }

    //position of player
    if (disp->GPSPlayer)
    {
      disp->GPSPlayer->ShowCtrl(scope);
      if(scope)
      {
        char buffer[256];
        PositionToAA11(vehicle->RenderVisualState().Position(), buffer);
        disp->GPSPlayer->SetText(buffer);
      }
    }

    //draw compass
    if(scope && disp->showCompass)
      DrawCompass(vehicle);


    //vision mode -  normal, night vision, FLIR
    if (disp->visionMode)
    {
      disp->visionMode->ShowCtrl(scope);
      if(scope)
      {
        char buffer[256];
        if (GEngine->GetThermalVisionWanted()) sprintf(buffer,"FLIR");
        else 
          if (agent->GetPerson() && agent->GetPerson()->IsNVWanted()) sprintf(buffer,"NVS");
          else sprintf(buffer,"DTV");

          disp->visionMode->SetText(buffer);
      }
    }

    //FLIR mode black-white, red-black, ....
    if (disp->FLIRMode)
    {
      disp->FLIRMode->ShowCtrl(scope);
      if(scope)
      {
        char buffer[256];
        if (GEngine->GetThermalVisionWanted())
        {
          int mode =GEngine->GetTIMode();
          if(mode < sizeof(_FLIRModeNames)) sprintf(buffer,"%s", _FLIRModeNames[mode].Data());
          else sprintf(buffer,"");
        }
        else
          sprintf(buffer,"");

        disp->FLIRMode->SetText(buffer);
      }
    }

    //show compass (if it is not already)
    if (disp->compass)
    {
      disp->compass->ShowCtrl(scope);
      if(scope)
      {
        bool canSeeCompass = false;
        Transport *transport = agent->GetVehicleIn();
        if (transport)
        {
          int canSee = 0;
          if (agent == transport->ObserverUnit()) canSee = transport->Type()->_commanderCanSee;
          else if (agent == transport->PilotUnit()) canSee = transport->Type()->_driverCanSee;
          else if (agent == transport->GunnerUnit()) canSee = transport->Type()->_gunnerCanSee;
          canSeeCompass = (canSee&CanSeeCompass)!=0;
        }

        if(!canSeeCompass) DrawCompass(vehicle);
      }
    }

    //show heading (if it is not already)
    if (disp->heading)
    {
      disp->heading->ShowCtrl(scope);
      if(scope)
      {
        bool canSeeCompass = false;
        Transport *transport = agent->GetVehicleIn();
        if (transport)
        {
          int canSee = 0;
          if (agent == transport->ObserverUnit()) canSee = transport->Type()->_commanderCanSee;
          else if (agent == transport->PilotUnit()) canSee = transport->Type()->_driverCanSee;
          else if (agent == transport->GunnerUnit()) canSee = transport->Type()->_gunnerCanSee;
          canSeeCompass = (canSee&CanSeeCompass)!=0;
        }

        if(!canSeeCompass) 
        {
          Camera &cam = *GLOB_SCENE->GetCamera();
          Vector3Val dir = cam.Direction();

          // avoid singularity when heading up
          float angle;
          if (fabs(dir.X()) + fabs(dir.Z()) > 1e-3)
            angle = atan2(dir.X(), dir.Z());
          else
            angle = 0;

          int degAngle = toInt(angle * (180.0f / H_PI));
          if (degAngle < 0) degAngle += 360;
          BString<16> buffer;
          sprintf(buffer, "%03d", degAngle);

          disp->heading->SetText(cc_cast(buffer));
        }
      }
    }

    //hovering mode
    if(disp->autohover)
    {
      disp->autohover->ShowCtrl(scope);
      if(scope)
      {
        Transport *transport = agent->GetVehicleIn();
        if(transport && transport->GetAutoHover()) disp->autohover->ShowCtrl(true);
        else disp->autohover->ShowCtrl(false);
      }
    }

    //laser designator ON
    if(disp->laserMarkerON)
    {
      disp->laserMarkerON->ShowCtrl(scope);
      if(scope)
      {
        if(context._weapons->_laserTargetOn) disp->laserMarkerON->ShowCtrl(true);
        else disp->laserMarkerON->ShowCtrl(false);
      }
    }

    //FOV mode
    if(disp->FOVMode)
    {
      disp->FOVMode->ShowCtrl(scope);
      if(scope)
      {
        char buffer[256]; sprintf(buffer,"");
        //Transport *transport = agent->GetVehicleIn();
        if(agent->IsFreeSoldier())
        {
          int weapon = context._weapons->ValidatedCurrentWeapon();
          if(weapon >= 0)
          {
            const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
            const OpticsInfo &opticsInfo = slot._muzzle->_opticsInfo[slot._opticsMode];
            sprintf(buffer, "%s", opticsInfo._opticsDisplayName.Data());
          }
          else sprintf(buffer, "");
        }
        else if (agent->GetVehicleIn() && context._turret)
        {
          const ViewPars *view = context._turret->GetCurrentViewPars(context._turretType);
          if(view) 
            sprintf(buffer, "%s", view->_opticsDisplayName.Data());
        }
        disp->FOVMode->SetText(buffer);
      }
    }

   
    //ballistic computer
    {
      //ballistic computer on
      if(disp->BallEnabled)
      {
        disp->BallEnabled->ShowCtrl(scope);
        if(scope)
        {
          int weapon = context._weapons->ValidatedCurrentWeapon();
          if(weapon >=0)
          {
            const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
            const MuzzleType *muzzle = slot._muzzle;
            if (muzzle && muzzle->_ballisticsComputer == 1) disp->BallEnabled->ShowCtrl(true);
            else disp->BallEnabled->ShowCtrl(false);
          }
          else disp->BallEnabled->ShowCtrl(false);
        }
      }

      if(disp->BallEnabledImg)
      {
        disp->BallEnabledImg->ShowCtrl(scope);
        if(scope)
        {
          int weapon = context._weapons->ValidatedCurrentWeapon();
          if(weapon >=0)
          {
            const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
            const MuzzleType *muzzle = slot._muzzle;
            if (muzzle && muzzle->_ballisticsComputer == 1 && GWorld->UI()->GetLockedTarget()) disp->BallEnabledImg->ShowCtrl(true);
            else disp->BallEnabledImg->ShowCtrl(false);
          }
          else disp->BallEnabledImg->ShowCtrl(false);
        }
      }

      //ballistic computer range
      if(disp->BallRange)
      {
        disp->BallRange->ShowCtrl(scope);
        if(scope)
        {
          char buffer[256];
#if !_VBS2 && !_VBS3
          int weapon = context._weapons->ValidatedCurrentWeapon();
          if(weapon >= 0)
          {
            const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
            const MuzzleType *muzzle = slot._muzzle;
            if (muzzle && muzzle->_ballisticsComputer == 1)
            {
              if(GWorld->UI()->GetLockedTarget()) sprintf(buffer, "%d",  (int)(agent->Position(agent->GetRenderVisualState()).Distance(GWorld->UI()->GetLockedTarget()->GetPosition())));
              else sprintf(buffer, "---");
            }
            else sprintf(buffer, "");
          }
          else sprintf(buffer, "");
#endif
          disp->BallRange->SetText(buffer);
        }
      }
    }

    //javelin icons
    {   
      if(disp->javelinDay)
      {
        disp->javelinDay->ShowCtrl(scope);
        if(scope)
        {
          bool day = true;
          if (GEngine->GetThermalVisionWanted()) day = false;
          else 
            if (agent->GetPerson() && agent->GetPerson()->IsNVWanted()) day = false;
          disp->javelinDay->ShowCtrl(day);
        }
      }

      if(disp->javelinFLTR)
      {
        disp->javelinFLTR->ShowCtrl(scope);
        if(scope)
        {
          disp->javelinFLTR->ShowCtrl(false);
          if (GEngine->GetThermalVisionWanted()) disp->javelinFLTR->ShowCtrl(true);
          else disp->javelinFLTR->ShowCtrl(false);
        }
      }
      if(disp->javelinNFOV)
      {
        disp->javelinNFOV->ShowCtrl(scope);
        if(scope)
        {
          bool show = (atan(camera.Left()) < 0.05f)? true:false;
          disp->javelinNFOV->ShowCtrl(show);
        }
      }
      if(disp->javelinWFOV)
      {
        disp->javelinWFOV->ShowCtrl(scope);
        if(scope)
        {
          bool show = (atan(camera.Left()) >= 0.05f)? true:false;
          disp->javelinWFOV->ShowCtrl(show);
        }
      }
      if(disp->javelinSEEK)
      {
        disp->javelinSEEK->ShowCtrl(scope);
        if(scope)
        {
          bool show = (_lockTarget != NULL)? true : false; 
          disp->javelinSEEK->ShowCtrl(show);
        }
      }
      if(disp->javelinMISSILE)
      {
        disp->javelinMISSILE->ShowCtrl(scope);
        if(scope)
        {
          AIBrain *unit = GWorld->FocusOn();
          bool show = (_lockTarget != NULL)? true : false; 
          if(show)
          {
            TurretContext context; 
            if (vehicle->FindTurret(unit->GetPerson(), context))
              if(context._weapons->_timeToMissileLock <= Glob.time) show = false; 
          }
          disp->javelinMISSILE->ShowCtrl(show);
        }
      }
    }
    //hide text if not in optics
    if(disp->staticItems)
      disp->staticItems->ShowCtrl(scope);

    desc._changed = Glob.uiTime;
    float alpha = 1.0;

    if (disp->IsReady())
      disp->DrawHUD(vehicle, alpha);

    if(dirty) _weaponInfoAlpha = 100.0f;
  }
}

void InGameUI::DrawGroupDir(const Camera &camera, AIGroup *grp)
{
  if (!grp) return;

  float age = Glob.uiTime - _lastGroupDirTime;
  if (age >= groupDirDimEndTime) return;

  float alpha = 1.0;
  if (age > groupDirDimStartTime)
    alpha = (groupDirDimEndTime - age) / (groupDirDimEndTime - groupDirDimStartTime);

  PackedColor bgColorA=PackedColorRGB(bgColor,toIntFloor(bgColor.A8()*alpha));
  PackedColor ftColorA=PackedColorRGB(ftColor,toIntFloor(ftColor.A8()*alpha));

  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();

  float gdX, gdY, gdW, gdH;
  if (GChatList.Enabled())
  {
    gdX = gd1X;
    gdY = gd1Y;
    gdW = gd1W;
    gdH = gd1H;
  }
  else
  {
    gdX = gd2X;
    gdY = gd2Y;
    gdW = gd2W;
    gdH = gd2H;
  }

  Texture *corner = GLOB_SCENE->Preloaded(Corner);
  MipInfo cornerMip = GLOB_ENGINE->TextBank()->UseMipmap(corner, 1, 0);
  
  Draw2DParsExt pars;
  pars.spec = NoZBuf|IsAlpha|IsAlphaFog|ClampU|ClampV;
  pars.spec|= (groupDirShadow == 2) ? UISHADOW : 0; 
  pars.SetColor(ftColorA);
  pars.mip = GEngine->TextBank()->UseMipmap(_imageGroupDir, 1, 0);
  if (pars.mip._level==0 && cornerMip._level==0)
  {
    GEngine->DrawFrame(corner, bgColorA,Rect2DPixel(gdX * w, gdY * h, gdW * w, gdH * h));

    Matrix4Val camInvTransform = camera.GetInvTransform();
    Vector3 dir(VRotate, camInvTransform,grp->GetGroupDirection());
    dir[0] = -dir[0];

    Matrix4 mrot(MZero);
    Matrix4 mtrans1 = Matrix4(MTranslation, Vector3(-0.5, 0, -0.5));
    Matrix4 mtrans2 = Matrix4(MTranslation, Vector3(0.5, 0, 0.5));


    const float size = 1.1;

    Rect2DPixel rect
    (
      w * (gdX + gdW * 0.5 - gdW * size * 0.5),
      h * (gdY + gdH * 0.5 - gdH * size * 0.5),
      gdW * w * size, gdH * h * size
    );

    #define v000 VZero
    #define v001 VForward
    #define v100 VAside
    #define v101 Vector3(1, 0, 1)

    mrot.SetUpAndDirection(-VUp, -dir);
    Matrix4 m = mtrans2 * mrot * mtrans1;
    Vector3 uv = m.FastTransform(v000);
    pars.uTR = uv[2];
    pars.vTR = uv[0];
    uv = m.FastTransform(v001);
    pars.uTL = uv[2];
    pars.vTL = uv[0];
    uv = m.FastTransform(v100);
    pars.uBR = uv[2];
    pars.vBR = uv[0];
    uv = m.FastTransform(v101);
    pars.uBL = uv[2];
    pars.vBL = uv[0];

    GEngine->Draw2D(pars, rect, rect);
  }
}

void InGameUI::DrawHint()
{
  if (_hint->GetHint().GetLength() == 0) return;
  float age = Glob.uiTime - _hintTime;
  if (age < hintDimEndTime)
  {
    float alpha = 1.0;
    if (age > hintDimStartTime)
      alpha = (hintDimEndTime - age) / (hintDimEndTime - hintDimStartTime);
    _hint->SetPosition(_hintTop);
    _hint->DrawHUD(NULL, alpha);
  }
}

void InGameUI::DrawTaskHint()
{
  if(_taskHint->Size()==0) return;
  
  float age = Glob.uiTime - _taskHint->_timeUp;
  if (age >= taskHintDimEndTime) 
  {//delete old taskHint and set new
    _taskHint->Delete(0);
    if(_taskHint->Size()>0) _taskHint->SetTaskHint();
    else return;
  }
  age = Glob.uiTime - _taskHint->_timeUp;
  if(_taskHint->GetTaskHint().GetLength() == 0) return;
  if (age < taskHintDimEndTime)
  {
    float alpha = 1.0;
    if (age < taskHintDimStartTime)
      alpha = 1-((taskHintDimStartTime - age) / (taskHintDimStartTime));
    else if (age < taskHintDimShowTime)
      alpha = 1.0;
    else
      alpha = ((taskHintDimEndTime - age) / (taskHintDimEndTime - taskHintDimShowTime));

    _taskHint->DrawHUD(NULL, alpha);

    PackedColor colorA=PackedColorRGB(_taskHint->GetColor(0),toIntFloor(ftColor.A8()*alpha));
    //unit number texture background (size, position taken form config)
    MipInfo mip = GEngine->TextBank()->UseMipmap(_taskHint->GetTexture(0), 0, 0);
    GEngine->Draw2D(mip, colorA,
      _taskHint->TextureRectangle());
  }
}

/*!
\patch 1.56 Date 5/14/2002 by Jirka
- Improved: Ingame UI - icons in unit list
*/

void InGameUI::DrawGroupUnit(AIUnit *u, UnitDescription &info, float left, float top, float alpha, int align, float width, float height)
{//draw single unit info in command bar
  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();

  //unit number texture background (size, position taken form config)
  MipInfo mip = GEngine->TextBank()->UseMipmap(_cmdbarNumber, 0, 0);
  GEngine->Draw2D(mip, _unitNumberBackgroundColor, Rect2DPixel((left+ _unitNumberBackgroundX ) * w  , (_unitNumberBackgroundY +top) * h, _unitNumberBackgroundW * w, _unitNumberBackgroundH  * h), Rect2DClipPixel, _unitNumberBackgroundShadow);
  //unit number
  int id = u->ID();
  RString unitID = FormatNumber(id);
  float textWidth = GEngine->GetTextWidth(_uiUnitIDSize, _uiUnitIDFont, unitID);
  //unit number color is based in unit's team
  Team team = GetTeam(u);
  PackedColor color = teamColors[team];
  color.SetA8(toIntFloor(color.A8() * alpha));

  GEngine->DrawText
  (
    Point2DFloat(left + _uiUnitIDX - 0.5f*textWidth , top + _uiUnitIDY), _uiUnitIDSize, _uiUnitIDFont, color, unitID, _uiUnitIDShadow
  );

  return;
}

Team GetTeam(int i);

/*!
\patch_internal 1.01 Date 6/26/2001 by Ondra. Add protection message.
*/
void InGameUI::DrawGroupInfo(EntityAI *vehicle)
{//draw unit''s command bar
  AIBrain *agent = GWorld->FocusOn();
  Assert(agent);
  AIUnit *unit = agent->GetUnit();
  if (!unit) return;
  AISubgroup *subgroup = unit->GetSubgroup();
  Assert(subgroup);
  AIGroup *group = subgroup->GetGroup();
  Assert(group);

  int n = group->NUnits();
  // avoid listing through empty pages on the end
  while (n > 0)
  {
    AIUnit *u = group->GetUnit(n - 1);
    bool valid = u && u->GetSubgroup() && !group->GetReportedDown(u);
    if (valid) break;
    // we can safely ignore this unit
    n--;
  }

  OLinkPermNOArray(AIUnit) listSelecting;
  ListSelectingUnits(listSelecting);
  OLinkPermNOArray(AIUnit) listSelected;
  ListSelectedUnits(listSelected);

  _groupInfoChanged = true;

  bool isValid = false;
  int i;
  for (i=0; i<10; i++)
  {//get unit's descripction
    UnitDescription &info = _groupInfo[i];
    AIUnit *u = NULL;
    if (i + _groupInfoOffset < n) u = group->GetUnit(i + _groupInfoOffset);

    bool valid =
    (
      u && u->GetSubgroup() && !group->GetReportedDown(u)
      && unit->IsGroupLeader() 
    );
    if (valid != info.valid)
    {
      info.valid = valid;
      _groupInfoChanged = true;
    }
    if (!valid)
      continue;

    isValid = true;
    EntityAI *veh = u->GetVehicle();
    Transport *trans = u->GetVehicleIn();
    //const VehicleType *type = veh->GetType();

    if (trans != info.vehicle)
    {
      info.vehicle = trans;
      _groupInfoChanged = true;
    }
    
    if (u->GetPerson() != info.person)
    {
      info.person = u->GetPerson();
      _groupInfoChanged = true;
    }
  
    AIUnit::ResourceType problemsType = group->GetWorstStateTypeReported(u);  
    /*
    bool problems = group()
    (
      u->GetPerson()->GetTotalDamage() > 0.5 ||
      !u->IsSoldier() &&
      (
        veh->GetTotalDamage() > 0.5 ||
        veh->GetRenderVisualStateFuel() < 0.5 * type->GetFuelCapacity()
      )
    );
    */
    if (problemsType != info.problemsType)
    {
      info.problemsType = problemsType;
      _groupInfoChanged = true;
    }

    if (u->GetSemaphore() != info.semaphore)
    {
      info.semaphore = u->GetSemaphore();
      _groupInfoChanged = true;
    }

    if (u->GetCombatMode() != info.combatMode)
    {
      info.combatMode = u->GetCombatMode();
      _groupInfoChanged = true;
    }

    UnitDescription::Status status,vehicleStatus;

    Command::Message cmd = Command::NoCommand;
    int commander = -1;
    
    if (!u->IsUnit())
    {
      Assert(trans);
      AIBrain *agent = trans->CommanderUnit();
      if (agent && agent->GetUnit() && agent->GetGroup() == group)
        commander = agent->GetUnit()->ID();
      
      TurretContext context;
      if (u->IsDriver())
      {
        status = UnitDescription::driver;
      }
      else if (trans->FindTurret(u->GetPerson(), context) && context._turretType)
      {
        if (context._turretType->_primaryObserver || context._weapons->_magazineSlots.Size() == 0)
          status = UnitDescription::commander;
        else
          status = UnitDescription::gunner;
      }
      else
      {
        status = UnitDescription::cargo;
      }
      vehicleStatus = UnitDescription::none;
    }
    else
    {
      AISubgroup *subgrp = u->GetSubgroup();
      if (subgrp == group->MainSubgroup())
      {
        if (u->IsAway())
          status = UnitDescription::away;
        else
          status = UnitDescription::none;
      }
      else if (!subgrp->HasCommand())
        status = UnitDescription::wait;
      else
      {
        cmd = subgrp->GetCommand()->_message;
        status = UnitDescription::command;
      }

      TurretContext context;
      Transport *vehIn = u->GetVehicleIn();
      if (u->IsDriver())
      {
        vehicleStatus = UnitDescription::driver;
      }
      else if(u->IsCommander())
      {
        vehicleStatus = UnitDescription::commander;      
      }
      else if (vehIn && vehIn->FindTurret(u->GetPerson(), context) && context._turretType)
      {
        if (context._turretType->_primaryObserver || context._weapons->_magazineSlots.Size() == 0)
          vehicleStatus = UnitDescription::commander;
        else
          vehicleStatus = UnitDescription::gunner;
      }
      else
      {
        vehicleStatus = UnitDescription::none;
      }
    }

    if (status != info.status)
    {
      info.status = status;
      _groupInfoChanged = true;
    }
    if (vehicleStatus != info.vehicleStatus)
    {
      info.vehicleStatus = vehicleStatus;
      _groupInfoChanged = true;
    }
    if (cmd != info.cmd)
    {
      info.cmd = cmd;
      _groupInfoChanged = true;
    }
    if (commander != info.vehCommander)
    {
      info.vehCommander = commander;
      _groupInfoChanged = true;
    }

    bool player = u == unit;
    if (player != info.player)
    {
      info.player = player;
      _groupInfoChanged = true;
    }

    bool playerVehicle;
    if (status == UnitDescription::cargo)
      playerVehicle = player;
    else
      playerVehicle = veh == unit->GetVehicle();
    if (playerVehicle != info.playerVehicle)
    {
      info.playerVehicle = playerVehicle;
      _groupInfoChanged = true;
    }

    bool selecting = listSelecting.Find(u) >= 0;
    if (selecting != info.selecting)
    {
      info.selecting = selecting;
      _groupInfoChanged = true;
    }

    bool selected = listSelected.Find(u) >= 0;
    if (selected != info.selected)
    {
      info.selected = selected;
      _groupInfoChanged = true;
    }

    Team team = GetTeam(u);
    if (team != info.team)
    {
      info.team = team;
      _groupInfoChanged = true;
    }

#if !_RELEASE
    int leader;
    if (u->GetSubgroup() == u->GetGroup()->MainSubgroup())
      leader = -1;
    else
      leader = u->GetSubgroup()->Leader()->ID();
    if (leader != info.leader)
    {
      info.leader = leader;
      _groupInfoChanged = true;
    }
#endif
  }

  if (!isValid && _groupInfoOffset == 0 && n <= 10)
    return;

  if (_groupInfoChanged)
  {
    _lastGroupInfoTime = Glob.uiTime;
    _groupInfoChanged = false;
  }
  float age = Glob.uiTime - _lastGroupInfoTime;
  const float startDim=90, endDim=95;
  float Alpha=1;
  if (age > startDim)
  {
    // interpolate dim
    if(age < endDim)
    {
      float factor=(age-startDim)*(1/(endDim-startDim));
      Alpha=(1-factor)+groupInfoDim*factor;
    }
    else Alpha=groupInfoDim;
    // update alpha
  }

  PackedColor ftColorA=PackedColorRGB(ftColor,toIntFloor(ftColor.A8()*Alpha));

  MipInfo mip;
  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();
  const float border = TEXT_BORDER;

  // ADD protection

  #if PROTECTION_ENABLED
    float fadeTime = 60.0f;
    if (Glob.uiTime>_timeToPlay && Glob.uiTime<_timeToPlay+fadeTime*2.0f)
    {
      float fade = 1-fabs(Glob.uiTime-_timeToPlay-fadeTime)*(1.0f/fadeTime);
      IF_FADE()
      {
        // preparing FADE message
        #define ENCODE_CHAR(c,i) (char)(c*2+i)

        static const char FadeMessage[]=
        {
          ENCODE_CHAR('I',0),
          ENCODE_CHAR('l',1),
          ENCODE_CHAR('l',2),
          ENCODE_CHAR('e',3),
          ENCODE_CHAR('g',4),
          ENCODE_CHAR('a',5),
          ENCODE_CHAR('l',6),
          ENCODE_CHAR(' ',7),
          ENCODE_CHAR('c',8),
          ENCODE_CHAR('o',9),
          ENCODE_CHAR('p',10),
          ENCODE_CHAR('i',11),
          ENCODE_CHAR('e',12),
          ENCODE_CHAR('s',13),
          ENCODE_CHAR(' ',14),
          ENCODE_CHAR('m',15),
          ENCODE_CHAR('a',16),
          ENCODE_CHAR('y',17),
          ENCODE_CHAR(' ',18),
          ENCODE_CHAR('d',19),
          ENCODE_CHAR('e',20),
          ENCODE_CHAR('g',21),
          ENCODE_CHAR('r',22),
          ENCODE_CHAR('a',23),
          ENCODE_CHAR('d',24),
          ENCODE_CHAR('e',25),
          ENCODE_CHAR('.',26),
          ENCODE_CHAR(0,27),
        };

        const int len = sizeof(FadeMessage);
        const char *src = FadeMessage;
        char decode[len];
        for (int i=0; i<len; i++)
        {
          decode[i] = ((unsigned char)(src[i])-i)>>1;
        }
        float w = GEngine->GetTextWidth(_fadeSize,_fadeFont,decode);
        static const PackedColor colorB=PackedBlack;
        static const PackedColor colorT=PackedWhite;
        int fadeA = toIntFloor(255*fade*0.5);
        saturate(fadeA,0,255);
        PackedColor colorBA=PackedColorRGB(colorB,fadeA);
        PackedColor colorTA=PackedColorRGB(colorT,fadeA);
        float offX = 0.001;
        float offY = 0.001;
        GEngine->DrawText(Point2DFloat(0.5-w*0.5,0.5),_fadeSize,_fadeFont,colorBA,decode,_fadeShadow);
        GEngine->DrawText(Point2DFloat(0.5-w*0.5-offX,0.5-offY),_fadeSize,_fadeFont,colorTA,decode,_fadeShadow);

      }
    }
  #endif

  //command bar size and location
  const float columns = 10 + 1; // units + prev + next (prev and next arrows has half of unit's width)  
  float left = uiX + border;

  float width = (uiW - (columns + 2) * border)
              * (1.0 / columns);
  float ratio = floatMax(w,1)/floatMax(h,1);
  float height = width * ratio;

  //position of first unit (x position plus arrow)
  left += 0.5f *width + border;

  PackedColor color;
  for (i=0; i<10; i++)
  {//draw units
    UnitDescription &info = _groupInfo[i];
    if (!info.valid || !info.person) continue;
    
    AIUnit *u = group->GetUnit(i + _groupInfoOffset);
    Assert(u);

    // draw picture
    Texture *picture1 = NULL; //unit picture
    Texture *picture2 = NULL; //weapon
    Texture *picture3 = NULL; //specialization

    //take icon according to unit's position in vehicle or according to unit's weapon
    if (info.vehicle == NULL ||
      info.status == UnitDescription::cargo ||
      info.status == UnitDescription::driver ||
      info.status == UnitDescription::gunner ||
      info.status == UnitDescription::commander)
    {
      int index = info.person->FindWeaponType(MaskSlotSecondary);
      if (index < 0) index = info.person->FindWeaponType(MaskSlotPrimary);
      if (index < 0) index = info.person->FindWeaponType(MaskSlotHandGun);


      //unit picture
      picture1 = info.person->GetPortrait();
      if(!picture1) picture1 = _imageDefaultWeapons;
      picture3 = info.person->GetPicture();

      switch (info.status) 
      {
      //case UnitDescription::cargo:  // if unit is in cargo, unit's specialization is used
      //  picture2 = _imageCargo;
      //  break;
      case UnitDescription::driver:
        picture2 = _imageDriver;
        break;
      case UnitDescription::gunner:
        picture2 = _imageGunner;
        break;
      case UnitDescription::commander:
        picture2 = _imageCommander;
        break;
      default:
        if (index >= 0)
        {
          const WeaponType *weapon = info.person->GetWeaponSystem(index);
          if(weapon) picture2 = weapon->GetPicture();
          break;
        }
      }
    }
    else
    {
      picture1 = info.vehicle->GetPicture(); //vehicle picture

      switch (info.vehicleStatus)
      {
      case UnitDescription::driver:
        picture2 = _imageDriver;
        break;
      case UnitDescription::gunner:
        picture2 = _imageGunner;
        break;
      case UnitDescription::commander:
        picture2 = _imageCommander;
        break;
      }
    }

    if (picture1)
    {//draw unit and background
      if (info.problemsType > AIUnit::RTNone)
      {      
        switch (info.problemsType)
        {
        case AIUnit::RTDamage:
        case AIUnit::RTHealth:
          color = _unitIconColorWounded;        
          break;
        case AIUnit::RTFuel:
          color = _unitIconColorNoFuel;
          break;
        case AIUnit::RTAmmo:
          color = _unitIconColorNoAmmo;
          break;
        }
      }
      else
      { 
        if(info.player) color = _unitIconColorPlayer;
        else color = _unitIconColor;
      }
      color.SetA8(toIntFloor(color.A8() * Alpha));
      //texture for (un)selected unit background 
      if(info.selected) mip = GEngine->TextBank()->UseMipmap(_cmdbarSelected, 0, 0); 
      else  mip = GEngine->TextBank()->UseMipmap(_cmdbarBackground, 0, 0);            
      GEngine->Draw2D(mip, PackedWhite, Rect2DPixel((left + _unitBackgroundX ) * w, (uiY + _unitBackgroundY ) * h, w *_unitBackgroundW , h * _unitBackgroundH), Rect2DClipPixel, _unitIconShadow);
      
      // draw background for the selected unit - x-box control
      if (i + _groupInfoOffset == _groupInfoSelected)
      {
        mip =  GEngine->TextBank()->UseMipmap(_cmdbarFocus, 0, 0);
        GEngine->Draw2D(mip, PackedWhite, Rect2DPixel((left + _unitBackgroundX ) * w, (uiY + _unitBackgroundY ) * h, w *_unitBackgroundW , h * _unitBackgroundH), Rect2DClipPixel, _unitBackgroundShadow);
      }
      
      //draw primary image color is based on unit's status (wounded, no ammo)
      mip = GEngine->TextBank()->UseMipmap(picture1, 0, 0);
      GEngine->Draw2D(mip, color, Rect2DPixel((left + _unitIconX) * w, (uiY + _unitIconY) * h,  w * _unitIconW, h * _unitIconH));
      //if unit is player, white frame is added
      if(info.player) 
      {
        mip = GEngine->TextBank()->UseMipmap(_cmdbarPlayer, 0, 0);
        GEngine->Draw2D(mip, PackedWhite, Rect2DPixel((left + _unitBackgroundX) * w, (uiY + _unitBackgroundY) * h, w *_unitBackgroundW , h * _unitBackgroundH), Rect2DClipPixel, _unitBackgroundShadow);
      }
    }

  
    if (picture2)
    {//draw unit's extra info
      color = _unitRoleColor;
      color.SetA8(toIntFloor(color.A8() * Alpha));
      mip = GEngine->TextBank()->UseMipmap(picture2, 0, 0);
      GEngine->Draw2D(mip, color, Rect2DPixel((left + _unitRoleX) * w  , (uiY +_unitRoleY) * h,  w * _unitRoleW, h * _unitRoleH), Rect2DClipPixel, _unitRoleShadow);
    }

    //draw specialization
    if(picture3)
    {
      mip = GEngine->TextBank()->UseMipmap(picture3, 0, 0);
      GEngine->Draw2D(mip, _unitSpecialRoleColor, Rect2DPixel((left + _unitSpecialRoleX) * w  , (uiY +_unitSpecialRoleY) * h, w * _unitSpecialRoleW, h * _unitSpecialRoleH), Rect2DClipPixel, _unitSpecialRoleShadow);
    }

    //draw combatMode
    /*if(!info.player)*/{
      switch (info.combatMode)
      {
      case CMUnchanged:
      case CMAware:
        mip = GEngine->TextBank()->UseMipmap(_cmdbarMAware, 0, 0);
        break; 
      case CMCareless:
        mip = GEngine->TextBank()->UseMipmap(_cmdbarMCareless, 0, 0);
        break;    
      case CMCombat:
        mip = GEngine->TextBank()->UseMipmap(_cmdbarMCombat, 0, 0);
        break;
      case CMSafe:
        mip = GEngine->TextBank()->UseMipmap(_cmdbarMSafe, 0, 0);
        break;
      case CMStealth:
        mip = GEngine->TextBank()->UseMipmap(_cmdbarMStealth, 0, 0);
        break;
      }
      GEngine->Draw2D(mip, PackedWhite, Rect2DPixel((left + _cmdbarModeX) * w  , (uiY +_cmdbarModeY) * h, w * _cmdbarModeW, h * _cmdbarModeH), Rect2DClipPixel,_cmdbarModeShadow);
    }

#if _VBS3_RANKS //draw rank

    if (info.problems)
      color = pictureProblemsColor;
    else
      color = pictureColor;

    AIUnitInfo &unitInfo =  info.person->GetInfo();

    texture = GVBSVisuals.GetRankImage(0,unitInfo._rank, true);
    if(texture)
    {
      color.SetA8(toIntFloor(color.A8() * Alpha));
      mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);

      float hh = 0.5 * height *1.3;
      float ww = 0.75 * hh*1.3;
      float xx = left;
      float yy = uiY;//);

      GEngine->Draw2D(mip, color, Rect2DPixel(xx * w, yy * h, ww * w, hh * h));
    }
#endif

    // draw status
    RString text;
    switch (info.status)
    {
    case UnitDescription::none:
      break;
    case UnitDescription::wait:
      text = LocalizeString(IDS_STATE_WAIT);
      goto DrawCommand;
    case UnitDescription::away:
      text = LocalizeString(IDS_AWAY);
      goto DrawCommand;
    case UnitDescription::command:
    {
      int icmd = info.cmd;
      switch (icmd)
      {
      case Command::AttackAndFire:
        icmd = Command::Attack;
        break;
      case Command::FireAtPosition:
        icmd = Command::Attack;
        break;
      case Command::RepairVehicle:
        icmd = Command::Repair;
        break;
      case Command::TakeBag:
      case Command::Assemble:
      case Command::DisAssemble:
        icmd = Command::Action;
        break;
      }
      text = LocalizeString(IDS_STATE_NOCOMMAND + icmd);
    }
DrawCommand:
      if (text.GetLength()>0)
      {
        float fontSize = _uiCommandSize;
        float positionY = uiY + _uiCommandY;
        //draw command background
        mip = GEngine->TextBank()->UseMipmap(_cmdbarCommand, 0, 0);
        GEngine->Draw2D(mip,_commandBackgroundColor, Rect2DPixel((left + _commandBackgroundX * width) * w, (uiY + _commandBackgroundY) * h, w * _commandBackgroundW, h * _commandBackgroundH), Rect2DClipPixel, _commandBackgroundShadow);
        float textWidth = GEngine->GetTextWidth(fontSize, _uiCommandFont, text);
        //write command
        if(textWidth > width*0.95f) 
        {
          fontSize = 0.65 * _uiCommandSize;
          textWidth = GEngine->GetTextWidth(fontSize, _uiCommandFont, text);
          positionY = uiY + _uiCommandY + 0.5*(_uiCommandSize - fontSize);
          if(textWidth > width*0.95f) 
          {
            text = Format("%s",  (text.Substring(0,9)).Data());
            text = text + ".";
            textWidth = GEngine->GetTextWidth(fontSize, _uiCommandFont, text);
          }
        }

        GEngine->DrawText
          (
          Point2DFloat(left + _uiCommandX - textWidth*0.5f , positionY),
          fontSize, _uiCommandFont, PackedWhite, text, _uiCommandShadow
          );

      }
      break;
    case UnitDescription::cargo:
    case UnitDescription::driver:
    case UnitDescription::gunner:
    case UnitDescription::commander:
      {//draw unit's vehicle ID

          mip = GEngine->TextBank()->UseMipmap(_cmdbarCommander, 0, 0);
          GEngine->Draw2D(mip, _vehicleNumberBackgroundColor, Rect2DPixel((left + _vehicleNumberBackgroundX) * w, (_vehicleNumberBackgroundY+ uiY) * h, _vehicleNumberBackgroundW * w, _vehicleNumberBackgroundH * h), Rect2DClipPixel, _vehicleNumberBackgroundShadow);
          //vehicle ID
          RString unitID;
          if (info.vehCommander >= 0)
          { //draw vehicle number background
            unitID = FormatNumber(info.vehCommander);
          }
          else unitID = "-";
          float textWidth = GEngine->GetTextWidth(_uiVehicleNumberSize, _uiVehicleNumberFont, unitID);

          GEngine->DrawText
            (
            Point2DFloat(left + _uiVehicleNumberX - 0.5f*textWidth , uiY + _uiVehicleNumberY), _uiVehicleNumberSize, _uiVehicleNumberFont, PackedColor(0,0,0,255), unitID, _uiVehicleNumberShadow
            );
        
      }
      break;
    }

    // draw unit,s number
    DrawGroupUnit(u, info, left, uiY, Alpha, ST_CENTER, width, height);

    // draw semaphore
    switch (info.semaphore)
    {
    case AI::SemaphoreBlue:
    case AI::SemaphoreGreen:
    case AI::SemaphoreWhite:
      // hold fire indication
      color = _semaphoreColor;
      color.SetA8(toIntFloor(holdFireColor.A8() * Alpha));
      mip = GEngine->TextBank()->UseMipmap(_imageSemaphore, 0, 0);
      GEngine->Draw2D(mip, color,
        Rect2DPixel((left + _semaphoreX ) * w, (uiY + _semaphoreY ) * h, w * _semaphoreW, h* _semaphoreH), Rect2DClipPixel, _semaphoreShadow);
      break;
    }
    //next unit location
    left += width + border;
  }
  //next page arrow
//  left = 0.5f * width + border;
  if (_groupInfoOffset + 10 < n)
  {
    mip = GEngine->TextBank()->UseMipmap(_imageNextPage, 0, 0);
    GEngine->Draw2D(mip, _nextPageColor, Rect2DPixel((left + _nextPageX) * w, (uiY+ _nextPageY) * h, w * _nextPageW, h * _nextPageH),Rect2DClipPixel,_nextPageShadow);
  }

  // previous page arrow
  left = uiX + border; 
  if (_groupInfoOffset > 0)
  {
    mip = GEngine->TextBank()->UseMipmap(_imagePrevPage, 0, 0);
    GEngine->Draw2D(mip, _prevPageColor, Rect2DPixel((left + _prevPageX) * w, (uiY + _prevPageY) * h,  w * _prevPageW, h * _prevPageH),Rect2DClipPixel,_prevPageShadow);
  }
}

//draw group's icon and name in high command bat
void InGameUI::DrawHCGroup(int index,RString name,PackedColor color,float left, float top, float alpha, int align, bool selected)
{
  //use first two letters as ID and add controlling F-key
  RString unitID = Format("%s",  (name.Substring(0,2)).Data());
  unitID.Trim();
  //text size
  float textWidth = GEngine->GetTextWidth(_uiHCGroupSize, _uiHCGroupFont, unitID);
  //group's descripction
  GEngine->DrawText
    (
    Point2DFloat(left + _uiHCGroupX- 0.5*textWidth, top + _uiHCGroupY), _uiHCGroupSize, _uiHCGroupFont, PackedWhite, unitID, 0
    );

  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();
  //group's number background
  MipInfo mip = GEngine->TextBank()->UseMipmap(_cmdbarNumber, 0, 0);
  GEngine->Draw2D(mip, _unitNumberBackgroundColor, Rect2DPixel((left+ _unitNumberBackgroundX ) * w  , (_unitNumberBackgroundY +top) * h, _unitNumberBackgroundW * w, _unitNumberBackgroundH  * h), Rect2DClipPixel, _unitNumberBackgroundShadow);

  int id = index + 1 -_groupInfoOffset;
  unitID = FormatNumber(id);

  textWidth = GEngine->GetTextWidth(_uiUnitIDSize, _uiUnitIDFont, unitID);
  //group's number
  GEngine->DrawText
    (
    Point2DFloat(left + _uiUnitIDX - 0.5*textWidth, top + _uiUnitIDY), _uiUnitIDSize, _uiUnitIDFont, color, unitID, _uiUnitIDShadow
    );

  return;
}

void InGameUI::DrawHCInfo(EntityAI *vehicle)
{//draw HC command bar
  AIBrain *agent = GWorld->FocusOn();
  Assert(agent);
  AIUnit *unit = agent->GetUnit();
  if (!unit) return;

  AutoArray<AIHCGroup> &hcGroups = unit->GetHCGroups();
  if(hcGroups.Size()<=0) return;

  MipInfo mip;
  const int w = GEngine->Width2D();
  const int h = GEngine->Height2D();
  const float border = TEXT_BORDER;

  const float columns = 10 + 1; // units + prev + next (prev, next has half width)
  float left = uiX + border;

  float width = (uiW - (columns + 2) * border)
    * (1.0 / columns);
  //first group position
  left += 0.5f *width + border;

  PackedColor color;

  //get first group to draw
  //(there can be five groups on first page and eight on second)
  int offset=0;
  for(int i=0; i<hcGroups.Size();i++)
  {
    if(hcGroups[i].ID() < _groupInfoOffset)
      offset++;
    else break;
  }

  for(int i=offset; i< offset + 10 ; i++)
  { //try to draw ten groups, stop if we get to next page
    if(i>=hcGroups.Size() || hcGroups[i].ID() >= _groupInfoOffset +10) break;
    //group icon

    AIGroup *group = hcGroups[i].GetGroup();
    if(!group) continue;
    AutoArray<GIcon> markers = group->GetGroupIcons();
    //draw background for (un)selected group
    if(hcGroups[i].IsSelected()) mip = GEngine->TextBank()->UseMipmap(_cmdbarSelected, 0, 0);
    else  mip = GEngine->TextBank()->UseMipmap(_cmdbarBackground, 0, 0);
    GEngine->Draw2D(mip, PackedWhite, Rect2DPixel((left + _unitBackgroundX ) * w, (uiY + _unitBackgroundY ) * h, w *_unitBackgroundW , h * _unitBackgroundH), Rect2DClipPixel, _unitBackgroundShadow);
    
    // draw background for the selected unit - x-box control
    if (i == _groupInfoSelected)
    {
      mip =  GEngine->TextBank()->UseMipmap(_cmdbarFocus, 0, 0);
      GEngine->Draw2D(mip, PackedWhite, Rect2DPixel((left + _unitBackgroundX ) * w, (uiY + _unitBackgroundY ) * h, w *_unitBackgroundW , h * _unitBackgroundH), Rect2DClipPixel, _unitBackgroundShadow);
    }

    //draw group icon (composed from several textures)
    PackedColor colorIc = group->GetGroupIcon().IconColor();
    colorIc.SetA8(255);
    for(int j=0; j< markers.Size(); j++)
    {
      mip = GEngine->TextBank()->UseMipmap(markers[j]._groupMaker, 0, 0);
      GEngine->Draw2D(mip, colorIc , Rect2DPixel((left + _groupIconX + markers[j]._offsetX * _groupIconW) * w, (uiY + _groupIconY + markers[j]._offsetY * _groupIconH) * h,  w * _groupIconW, h * _groupIconH), Rect2DClipPixel, _groupIconShadow);
    }

    //draw combatMode
    if(group->Leader())
    {
      switch (group->Leader()->GetCombatMode())
      {
      case CMUnchanged:
      case CMAware:
        mip = GEngine->TextBank()->UseMipmap(_cmdbarMAware, 0, 0);
        break; 
      case CMCareless:
        mip = GEngine->TextBank()->UseMipmap(_cmdbarMCareless, 0, 0);
        break;    
      case CMCombat:
        mip = GEngine->TextBank()->UseMipmap(_cmdbarMCombat, 0, 0);
        break;
      case CMSafe:
        mip = GEngine->TextBank()->UseMipmap(_cmdbarMSafe, 0, 0);
        break;
      case CMStealth:
        mip = GEngine->TextBank()->UseMipmap(_cmdbarMStealth, 0, 0);
        break;
      }
      GEngine->Draw2D(mip, PackedWhite, Rect2DPixel((left + _cmdbarModeX) * w  , (uiY +_cmdbarModeY) * h, w * _cmdbarModeW, h * _cmdbarModeH), Rect2DClipPixel, _cmdbarModeShadow);
    }

    //get team color
    PackedColor color = teamColors[hcGroups[i].Team()];

    // draw units No
    DrawHCGroup(hcGroups[i].ID(),hcGroups[i].Name(),color,left, uiY, 1, ST_CENTER, hcGroups[i].IsSelected());

    //if unit is player, white frame is added
    if(hcGroups[i].GetGroup() == agent->GetGroup()) 
    {
      mip = GEngine->TextBank()->UseMipmap(_cmdbarPlayer, 0, 0);
      GEngine->Draw2D(mip, PackedWhite, Rect2DPixel((left + _unitBackgroundX) * w, (uiY + _unitBackgroundY) * h, w *_unitBackgroundW , h * _unitBackgroundH));
    }

    left += width + border;
  }
  //next page arrow
  if (_groupInfoOffset + 10 <= hcGroups[hcGroups.Size()-1].ID())
  {
    mip = GEngine->TextBank()->UseMipmap(_imageNextPage, 0, 0);
    GEngine->Draw2D(mip, _nextPageColor, Rect2DPixel((left + _nextPageX) * w, (uiY+ _nextPageY) * h, w * _nextPageW, h * _nextPageH));
  }
  // previous page arrow
  left = uiX + border; 
  if (_groupInfoOffset > 0)
  {
    mip = GEngine->TextBank()->UseMipmap(_imagePrevPage, 0, 0);
    GEngine->Draw2D(mip, _prevPageColor, Rect2DPixel((left + _prevPageX) * w, (uiY + _prevPageY) * h,  w * _prevPageW, h * _prevPageH));
  }
}

bool InGameUI::DrawMouseCursor(const Camera &camera, AIBrain *unit, bool td)
{
  EntityAIFull *vehicle = unit->GetVehicle();

#if _VBS3 // show correct weapon cursor in convoy trainer
  if(unit->GetPerson() && unit->GetPerson()->IsPersonalItemsEnabled())
    vehicle = unit->GetPerson();
#endif

  PreloadedTexture cursor = CursorStrategy;
  PreloadedTexture cursor2 = CursorStrategyMove;
  const CursorTextureInfo *cursorTex = NULL;
  Texture *cursorTex2 = NULL;
  float cursorSize = 1.0f;
  bool strategic = true;
  bool complexMenu = false;
  switch (_modeAuto)
  {
    case UIFire:
    case UIFirePosLock:
      // check weapon cursor
      cursorTex = vehicle->GetCursorTexture(unit->GetPerson());
      cursorTex2 = cursorTex ? cursorTex->texture : NULL;
      cursorSize = vehicle->GetCursorSize(unit->GetPerson());
      cursor = cursor2 = CursorAim; strategic = false;
      break;
    case UIStrategy:
      {
        if(GetCurrentMenu() && GetCurrentMenu()->_selected>=0 && GetCurrentMenu()->_selected < GetCurrentMenu()->_items.Size())
        {
          const MenuItem *item = GetCurrentMenu()->_items[GetCurrentMenu()->_selected];
          if(item && item->_visible && item->_enable && item->_cursorTexture)
          {
            cursorTex2 = item->_cursorTexture;
          }
        }
        cursor = CursorStrategyMove; break;
      }
    case UIStrategyMove: cursor = CursorStrategy; break;
    case UIStrategySelect: cursor = CursorStrategySelect; break;
    case UIStrategyAttack: cursor = CursorStrategyAttack; break;
    case UIStrategyFire: cursor = CursorStrategyAttack; break;
    case UIStrategyFireAtPosition: cursor = CursorStrategyAttack; break;
    case UIStrategyGetIn: cursor = CursorStrategyGetIn; break;
    case UIStrategyWatch: cursor = CursorStrategyWatch; break; // TODO: new cursor
    case UIStrategyGetOut: cursor = CursorStrategyGetOut; break; 
    case UIStrategyHeal: cursor = CursorStrategyHeal; break; 
    case UIStrategyRepairvehicle: cursor = CursorStrategyRepairVehicle; break; 
    case UIStrategyTakeBackpack: cursor = CursorStrategyTakeBackpack; break; 
    case UIStrategyAssemble: cursor = CursorStrategyAssemble; break; 
    case UIStrategyDisassemble: cursor = CursorStrategyDisassemble; break; 
    case UIStrategyComplex:
    case UIStrategyAction: 
      {
        cursor = cursor2 = CursorStrategyComplex;
        if(GetCurrentMenu() && GetCurrentMenu()->_selected>=0 && GetCurrentMenu()->_selected < GetCurrentMenu()->_items.Size())
        {
          const MenuItem *item = GetCurrentMenu()->_items[GetCurrentMenu()->_selected];
          if(item && item->_visible && item->_enable && item->_cursorTexture)
          {
            cursorTex2 = item->_cursorTexture;
            cursor = CursorStrategyMove;
          }
        }
        complexMenu = true;
      }
  }

  // dim cursor with time when non-active
//  float age=GetCursorAge();
//  const float startDim=5,endDim=10;
#if _VBS3 //make cursor more transparent
  float cursorA=0.7;
#else
  float cursorA=1;
#endif
//  if( age>startDim )
//  {
//    // interpolate dim
//    if( age<endDim )
//    {
//      float factor=(age-startDim)*(1/(endDim-startDim));
//      cursorA=(1-factor)+cursorDim*factor;
//    }
//    else cursorA=cursorDim;
//    // update alpha
//  }
  
  bool locking = false;
  bool locked = false;

  float size = 1.0f * Glob.config.IGUIScale;
  int weapon = -1;
  bool commanderAim = false;
  bool valid = false;
  TurretContextV context;
  if (vehicle->FindTurret(unconst_cast(vehicle->RenderVisualState()), unit->GetPerson(), context) && context._weapons->_magazineSlots.Size() > 0)
  {
    valid = true;
    weapon = context._weapons->ValidatedCurrentWeapon();
    commanderAim = true;
  }
  if ( weapon==-1 && vehicle->GetPrimaryGunnerTurret(unconst_cast(vehicle->RenderVisualState()), context))
  {
    valid = true;
    weapon = context._weapons->ValidatedCurrentWeapon();
    commanderAim = false;
  }

  if
  (
    _showCursors && _showHUD &&
    _lockTarget.IdExact() != NULL && weapon >= 0 &&
    (vehicle->CommanderUnit() == unit || vehicle->GunnerUnit() == unit)
  )
  {
    // find locked target info
    Vector3 relPos=_lockTarget->LandAimingPosition(unit)-camera.Position();
    float aimed = vehicle->GetAimed(context, weapon, _lockTarget, false, true);
    vehicle->GetWeaponLockReady(context, _lockTarget, weapon, aimed);

    // check actual visibility
    float aim = aimed;

    if(context._weapons->_timeToMissileLock > Glob.time) aimed = 0;

    //missile locking
    if(aim > 0 && context._weapons->_timeToMissileLock > Glob.time && (context._weapons->_timeToMissileLock < Glob.time + context._weapons->_magazineSlots[weapon]._weapon->_weaponLockDelay - 0.05f))
    {
      if(context._weapons->_magazineSlots[weapon]._weapon->_weaponLockDelay>0)
      {
        float locked = (0.4*((context._weapons->_timeToMissileLock - Glob.time) / context._weapons->_magazineSlots[weapon]._weapon->_weaponLockDelay));
        PackedColor color = PackedColorRGB(cursorColor, toIntFloor(cursorColor.A8() * (0.75f-locked)));

        DrawTargetInfo(
          camera,unit,relPos,
          (1+locked) * size,
          GPreloadedTextures.New(CursorLocked), NULL, 0,
          GPreloadedTextures.New(CursorLocked),
          color, cursorShadow, 1, 0.0f,
          _lockTarget,false,
          false,false, td
          );

        locking = true;
      }
    }
    else 
     if(context._weapons->_magazineSlots[weapon]._weapon->_weaponLockDelay>0 && aimed>0.25) locked = true;

    if(!context._weapons->_magazineSlots[weapon]._magazine || 
      context._weapons->_magazineSlots[weapon]._magazine->GetAmmo() <= 0) locking = locked = false;

    if(locking || locked)
    {
      const WeaponType *weaponType = context._weapons->_magazineSlots[weapon]._weapon;
      const SoundPars &sound= (locking)? weaponType->_lockingTargetSound : weaponType->_lockedTargetSound;
      float freq=sound.freq;
      // float vol=sound.vol;
      if(sound.name.GetLength() > 0 && true)
      {
        if(!_lockedSound)
        {
          _lockedSound = GSoundScene->OpenAndPlay2D(
            sound.name, 1, freq);
        }
        if(_lockedSound)
        {
          //float obstruction = 1, occlusion = 1;
          // warnings are obstructed by the vehicle itself
          //GWorld->CheckSoundObstruction(this,true,obstruction,occlusion);
          _lockedSound->SetVolume(1,freq); // volume, frequency
          _lockedSound->SetPosition(unit->Position(unit->GetRenderVisualState()),Vector3(0,0,0));
          //_lockedSound->SetObstruction(obstruction,occlusion,deltaT);
        }
      }
    }
    else _lockedSound.Free();

    DrawTargetInfo(
      camera,unit,relPos,
      size,
      GPreloadedTextures.New(CursorTarget), NULL, 0,
      GPreloadedTextures.New(aimed>0.25 ? CursorLocked : CursorTarget),
      cursorColor, cursorShadow, 1, aimed,
      _lockTarget,false,
      _lockTarget.IdExact()!=_target.IdExact(),false, td
    );
    
    if(vehicle->CommanderUnit() == unit && vehicle->GunnerUnit() != unit && commanderAim)
    {
      if(vehicle->GunnerUnit() && vehicle->GetPrimaryGunnerTurret(unconst_cast(vehicle->RenderVisualState()), context))
      {    
        valid = true;
        weapon = context._weapons->ValidatedCurrentWeapon();

        // find locked target info
        Vector3 relPos=_lockTarget->LandAimingPosition(vehicle->GunnerUnit())-camera.Position();
        float aimed = vehicle->GetAimed(context, weapon, _lockTarget, false, false);
        // check actual visibility

        if(aimed>0.25f)
        {
          DrawTargetInfo(
            camera,unit,relPos,
            size,
            GPreloadedTextures.New(CursorGunnerLocked), NULL, 0,
            GPreloadedTextures.New(CursorGunnerLocked),
            cursorColor, cursorShadow, aimed, 0,
            _lockTarget,false,
            false,false, td
            );
        }
      }
    }
  }
  else 
  {
    _lockedSound.Free();
    int weapon = context._weapons->ValidatedCurrentWeapon();
    if(weapon >=0 && (vehicle->CommanderUnit() == unit || vehicle->GunnerUnit() == unit)) vehicle->GetWeaponLockReady(context, NULL, weapon, 0);
  }
  // keep cursor on screen
  // assign cursor direction
  //LogF("World %.1f,%.1f,%.1f",_worldCursor[0],_worldCursor[1],_worldCursor[2]);
  //Matrix4Val camInvTransform=camera.GetInvTransform();

  Vector3 newDir = GetCursorDirection();

  if ((_showCursors || complexMenu) && _showHUD)
  {
    float textA = cursorA;
    if
    (
#if _ENABLE_CHEATS
      !_showAll &&
#endif
      !(valid && vehicle->ShowCursor(context, weapon, GWorld->GetCameraType(), unit->GetPerson())) &&
      !strategic
    )
    {
      cursorA = 0;
    }

    const Target *target = _target;

    PackedColor curColor = vehicle->GetCursorColor(unit->GetPerson());
    if (curColor == PackedColor(0))
    {
      curColor = cursorColor;
      if (cursorTex) curColor = cursorTex->color;

      if (valid)
      {
        bool autoAim = vehicle->IsAutoAimEnabled(context, weapon);
        if (unit && autoAim && Glob.config.IsEnabled(DTAutoAim) && GWorld->GetCameraType() != CamGunner)
        {
          float autoAimAcc = vehicle->GetAimAccuracy(context, weapon, newDir);
          curColor = PackedColor(Color(safe_cast<Color>(cursorLockColor)) * autoAimAcc + Color(safe_cast<Color>(curColor)) * (1 - autoAimAcc));
        }
      }
    }

    bool drawInfo = strategic;
    if (!strategic && target && !target->IsKindOf(GWorld->Preloaded(VTypeStatic)))
    {
      if (target->GetSide() == TCivilian)
      {
        drawInfo = false;
      }
      else if (unit->IsFriendly(target->GetSide()))
      {
        drawInfo = Glob.config.IsEnabled(DTFriendlyTag);
      }
      else
      {
        drawInfo = Glob.config.IsEnabled(DTEnemyTag);
      }
    }
    
    #if _ENABLE_CHEATS
      if (CHECK_DIAG(DECombat) || _showAll || tdCheat) drawInfo = true;
    #endif

    // sections of cursor
    Ref<Texture> tex = cursorTex ? cursorTex->texture : NULL;
    const AutoArray<CursorTextureSection> *sections = NULL;
    if (cursorTex && cursorTex->sections.Size() > 0) sections = &cursorTex->sections;

    // offset of sections
    float offset = floatMin(1,vehicle->GetCursorInaccuracy()*20);

    // fading of cursor for greater inaccuracy
    float fade = 1.0f;
    int shadow = cursorShadow;
    if (cursorTex)
    {
      fade = 1.0f - offset * (1.0f - cursorTex->fade);
      saturate(fade, 0, 1);
      shadow = cursorTex->shadow;
    }
    PackedColor color = PackedColorRGB(curColor, toIntFloor(curColor.A8() * cursorA * fade));

    if (!tex) tex = GPreloadedTextures.New(cursor);
    if (!cursorTex2) cursorTex2 = GPreloadedTextures.New(cursor2);
    DrawTargetInfo(
      camera,unit,newDir,
      cursorSize * size,
      tex, sections, offset, cursorTex2,
      color, shadow, textA, textA, target,
      ModeIsStrategy(_mode) && !IsEmptySelectedUnits(),
      drawInfo,ModeIsStrategy(_mode), false
    );

    Pair<Vector3,float> drivingCursor = vehicle->ShowDrivingCursor(GWorld->GetCameraType(),unit->GetPerson());
    if (drivingCursor.first.SquareSize()>0.01f)
    {
      // based on DrawCursorWp
      
      // TODO: customize color / texture
      PackedColor color = cursorColor;
      Vector3 dir = drivingCursor.first.Normalized();
      float dim = drivingCursor.first.Size();
      color.SetA8(toInt(dim*color.A8()));
      DrawCursor(
        camera, vehicle, dir, 0,
        _iconMission, 0, -0.5f*actH, actW, actH,
        color, false, cursorShadow,true,
    		PackedWhite, CursorTexts(), false, drivingCursor.second
      );
    }
  }

  return true;
}

void InGameUI::CheckLockDelay(AIBrain *unit)
{
  EntityAIFull *vehicle = unit->GetVehicle();

  bool locking = false;
  bool locked = false;

  int weapon = -1;
  bool valid = false;
  TurretContext context; 
  if (vehicle->FindTurret(unit->GetPerson(), context) && context._weapons->_magazineSlots.Size() > 0)
  {
    valid = true;
    weapon = context._weapons->ValidatedCurrentWeapon();
  }
  if ( weapon==-1 && vehicle->GetPrimaryGunnerTurret(context))
  {
    valid = true;
    weapon = context._weapons->ValidatedCurrentWeapon();
  }

  if
    (
    _lockTarget.IdExact() != NULL && weapon >= 0 && 
    (vehicle->CommanderUnit() == unit || vehicle->GunnerUnit() == unit)
    )
  {
    // find locked target info
    float aimed = vehicle->GetAimed(context, weapon, _lockTarget, false, true);
    vehicle->GetWeaponLockReady(context, _lockTarget, weapon, aimed);

    // check actual visibility
    float aim = aimed;
    if(context._weapons->_timeToMissileLock > Glob.time) aimed = 0;

    //missile locking
    if(aim > 0 && context._weapons->_timeToMissileLock > Glob.time && (context._weapons->_timeToMissileLock < Glob.time + context._weapons->_magazineSlots[weapon]._weapon->_weaponLockDelay - 0.05f))
    {
      if(context._weapons->_magazineSlots[weapon]._weapon->_weaponLockDelay>0)
        locking = true;
    }
    else 
      if(context._weapons->_magazineSlots[weapon]._weapon->_weaponLockDelay>0 && aimed>0.25) locked = true;

    if(!context._weapons->_magazineSlots[weapon]._magazine || 
      context._weapons->_magazineSlots[weapon]._magazine->GetAmmo() <= 0) locking = locked = false;

    if(locking || locked)
    {
      const WeaponType *weaponType = context._weapons->_magazineSlots[weapon]._weapon;
      const SoundPars &sound= (locking)? weaponType->_lockingTargetSound : weaponType->_lockedTargetSound;
      float freq=sound.freq;
      // float vol=sound.vol;
      if(sound.name.GetLength() > 0 && true)
      {
        if(!_lockedSound)
        {
          _lockedSound = GSoundScene->OpenAndPlay2D(
            sound.name, 1, freq);
        }
        if(_lockedSound)
        {
          //float obstruction = 1, occlusion = 1;
          // warnings are obstructed by the vehicle itself
          //GWorld->CheckSoundObstruction(this,true,obstruction,occlusion);
          _lockedSound->SetVolume(1,freq); // volume, frequency
          _lockedSound->SetPosition(unit->Position(unit->GetRenderVisualState()),Vector3(0,0,0));
          //_lockedSound->SetObstruction(obstruction,occlusion,deltaT);
        }
      }
    }
    else _lockedSound.Free();
  }
  else 
  {
    _lockedSound.Free();
    int weapon = context._weapons->ValidatedCurrentWeapon();
    if(weapon >=0 && (vehicle->CommanderUnit() == unit || vehicle->GunnerUnit() == unit)) vehicle->GetWeaponLockReady(context, NULL, weapon, 0);
  }
}

float HowMuchInteresting(AIBrain *unit, const Target *tgt);

static void DrawCursorSections(const MipInfo &mip, PackedColor color, int shadow, int mx, int my, int mw, int mh,
                               const AutoArray<CursorTextureSection> *sections, float offset)
{
  // draw black to see cursor on light background
  PackedColor colorBlack = PackedColorRGB(PackedBlack, color.A8());

  if (sections && offset != 0)
  {
    Assert(sections->Size() > 0);

    Draw2DPars pars;
    pars.mip = mip;
    pars.spec = NoZBuf | IsAlpha | ClampU | ClampV | IsAlphaFog;
    pars.spec|= (shadow == 2) ? UISHADOW : 0; 

    for (int i=0; i<sections->Size(); i++)
    {
      const CursorTextureSection &section = sections->Get(i);

      pars.SetU(section.uMin, section.uMax);
      pars.SetV(section.vMin, section.vMax);

      Rect2DAbs rect;
      rect.x = mx + mw * section.uMin + offset * section.xOffset;
      rect.y = my + mh * section.vMin + offset * section.yOffset;
      rect.w = mw * (section.uMax - section.uMin);
      rect.h = mh * (section.vMax - section.vMin);

      // draw shadow
      if (shadow == 1)
      {
        rect.x += 1;
        rect.y += 1;
        pars.SetColor(colorBlack);
        GEngine->Draw2D(pars,rect);
        rect.x -= 1;
        rect.y -= 1;
      }

      // draw cursor
      pars.SetColor(color);
      GEngine->Draw2D(pars,rect);
    }
  }
  else
  {
    if (shadow == 1) GEngine->Draw2D(mip, colorBlack, Rect2DAbs(mx + 1, my + 1, mw, mh));
    GEngine->Draw2D(mip, color, Rect2DAbs(mx, my, mw, mh),Rect2DClipAbs, shadow);
  }
}

void InGameUI::DrawTargetLine( Vector3Par wPos, const Camera &camera, int mx, int mw, int my, int mh, PackedColor color )
{
  Vector3 pos = camera.GetInvTransform().FastTransform(wPos);
  if (pos.Z() >= 1e-6)
  {
    const int w3d = GEngine->WidthBB();
    const int h3d = GEngine->HeightBB();
    
    float invZ = 1.0 / pos.Z();
    float hx = pos.X() * invZ * camera.InvLeft();
    float hy = - pos.Y() * invZ * camera.InvTop();

    int mhx = toInt((hx*0.5f+0.5f) * w3d);
    int mhy = toInt((hy*0.5f+0.5f) * h3d);

    Line2DAbs line;
    line.beg = Point2DAbs(mx+mw/2,my+mh/2);
    line.end = Point2DAbs(mhx,mhy);
    Line2DAbs lineBack = line;
    lineBack.beg.x++;
    lineBack.beg.y++;
    lineBack.end.x++;
    lineBack.end.y++;
    PackedColor colorBlack = PackedColorRGB(PackedBlack, color.A8());
    GEngine->DrawLine(lineBack, colorBlack, colorBlack);
    GEngine->DrawLine(line, color, color);
  }
}

void InGameUI::DrawTargetLine(Vector3Par wPosB, Vector3Par wPosE, const Camera &camera, PackedColor color)
{
  Vector3 posB = camera.GetInvTransform().FastTransform(wPosB);
  Vector3 posE = camera.GetInvTransform().FastTransform(wPosE);
  if (posB.Z() >= 1e-6 && posE.Z() >= 1e-6)
  {
    const int w3d = GEngine->WidthBB();
    const int h3d = GEngine->HeightBB();
  
    float invZB = 1.0 / posB.Z();
    float bhx = posB.X() * invZB * camera.InvLeft();
    float bhy = - posB.Y() * invZB * camera.InvTop();

    int bmhx = toInt((bhx*0.5f+0.5f) * w3d);
    int bmhy = toInt((bhy*0.5f+0.5f) * h3d);

    float invZE = 1.0 / posE.Z();
    float ehx = posE.X() * invZE * camera.InvLeft();
    float ehy = - posE.Y() * invZE * camera.InvTop();

    int emhx = toInt((ehx*0.5f+0.5f) * w3d);
    int emhy = toInt((ehy*0.5f+0.5f) * h3d);
    
    Line2DAbs line;
    line.beg = Point2DAbs(emhx,emhy);
    line.end = Point2DAbs(bmhx,bmhy);
    
    Line2DAbs lineBack = line;
    lineBack.beg.x++;
    lineBack.beg.y++;
    lineBack.end.x++;
    lineBack.end.y++;
    PackedColor colorBlack = PackedColorRGB(PackedBlack, color.A8());
    GEngine->DrawLine(lineBack, colorBlack, colorBlack);
    GEngine->DrawLine(line, color, color);
  }
}

/**
@param cursorSize relative cursor size, 1 is normal
*/
bool InGameUI::DrawTargetInfo(
  const Camera &camera, AIBrain *unit, Vector3Par dir,
	float cursorSize,
  Texture *cursor, const AutoArray<CursorTextureSection> *sections, float offset, Texture *cursor2,
  PackedColor color, int shadow, float cursorA, float cursor2A,
  const Target *target, bool drawHousePos,
  bool info, bool extended, bool td
)
{
  EntityAIFull *vehicle = unit->GetVehicle();

  Matrix4Val camInvTransform=camera.GetInvTransform();

  Vector3Val mDir=camInvTransform.Rotate(dir);

  // width/height assume fovLeft/fovTop = 1/0.75
  AspectSettings asp;
  GEngine->GetAspectSettings(asp);

  const float mScrH=64.0f*cursorSize/(800*asp.topFOV);
  const float mScrW=64.0f*cursorSize/(800*asp.leftFOV);

  float cx = 0,cy = 0;
  bool cxValid = false;
  {
    Point3 pos = camInvTransform.Rotate(dir);
    if (pos.Z() >= 1e-6)
    {
      float invZ = 1.0 / pos.Z();
      cx = pos.X() * invZ * camera.InvLeft();
      cy = - pos.Y() * invZ * camera.InvTop();
      if (fabs(cx)<10 && fabs(cy)<10)
      {
        cxValid = true;
      }
    }
  }

  float mScrX=cx*0.5+0.5-mScrW*0.5;
  float mScrY=cy*0.5+0.5-mScrH*0.5;
  
  const int w2d = GEngine->Width2D();
  const int h2d = GEngine->Height2D();
  
  // we are rendering this during the UI phase - rendering into backbuffer, not render target
  const int w3d = GEngine->WidthBB();
  const int h3d = GEngine->HeightBB();

  int mx = cxValid ? toInt(mScrX * w3d) : 0;
  int my = cxValid ? toInt(mScrY * h3d) : 0;
  int mw = cxValid ? toInt(mScrW * w3d) : 0;
  int mh = cxValid ? toInt(mScrH * h3d) : 0;

  float xAngle = atan2(mDir.X(), mDir.Z());
  float yAngle = atan2(mDir.Y(), mDir.SizeXZ());
  bool tactical = td && xAngle >= MIN_X && xAngle <= MAX_X &&
    yAngle >= MIN_Y && yAngle <= MAX_Y;

  float xScreen = 0, yScreen = 0;
  if (tactical)
  {
    xScreen = toInt((tdX + 0.5 * tdW + xAngle * tdW * INV_W - 0.5 * tdCurW) * w2d);
    yScreen = toInt((tdY + 0.5 * tdH - yAngle * tdH * INV_H - 0.5 * tdCurH) * h2d);
  }

//  Texture *textureDef = GLOB_SCENE->Preloaded(TextureWhite);
  Texture *texture;
  MipInfo mip;
/*
  AICenter *center = unit->GetGroup()->GetCenter();
  if (vehicle->CommanderUnit() && vehicle->CommanderUnit()->GetGroup())
  {
    center = vehicle->CommanderUnit()->GetGroup()->GetCenter();
  }
  Assert(center);
*/
  AIBrain *vehicleCommander = unit;
  if (vehicle->CommanderUnit()) vehicleCommander = vehicle->CommanderUnit();

  // strategy cursor background shadow
  if (cursor2 != cursor && cxValid && shadow == 1)
  { // all strategy cursors use the same background
    PackedColor colorBlack = PackedColorRGB(PackedBlack,toIntFloor(color.A8()*cursor2A));
    texture = cursor2;
    mip = GEngine->TextBank()->UseMipmap(texture,0,0);

    GEngine->Draw2D(mip,colorBlack,Rect2DAbs(mx+1,my+1,mw,mh));
    //GEngine->TextBank()->ReleaseMipmap();
  }

  // primary cursor
  if (color.A8() != 0)
  { // mode dependent cursor
    texture = cursor;
    mip=GEngine->TextBank()->UseMipmap(texture,0,0);

    if (cxValid)
    {
      DrawCursorSections(mip, color, shadow, mx, my, mw, mh, sections, offset);
      
      // if house position designated, show it as well
      if (drawHousePos)
      {
        if (_cover)
        {
          float vertical = 0;
          switch (_cover->_type)
          {
            case CoverRightCornerHigh: case CoverLeftCornerHigh: case CoverWall: vertical = 1.3f; break;
            case CoverRightCornerMiddle: case CoverLeftCornerMiddle: case CoverEdgeMiddle: vertical = 0.8f; break;
            default: vertical = 0.3f; break;
          }
          Vector3 pos = _cover->_coverPos.FastTransform(Vector3(0,vertical,-0.02f));
          Vector3 posBIn = _cover->_coverPos.FastTransform(Vector3(0,0,-0.02f));
          Vector3 posBOut = _cover->_coverPos.FastTransform(Vector3(0,0,-0.5f));
          DrawTargetLine(posBIn, posBOut, camera, color);
          DrawTargetLine(pos, posBIn, camera, color);
          DrawTargetLine(pos, camera, mx, mw, my, mh, color);
        }
        else if (_housePos>=0 && _target.IdExact())
        {
          const IPaths *house = _target.IdExact()->GetIPaths();
          if (house && _housePos >= 0 && _housePos < house->NPos())
          {
            Vector3 pos = house->GetPosition(house->GetPos(_housePos));
            DrawTargetLine(pos, camera, mx, mw, my, mh, color);
          }
        }
      }
      
    }
    
    if (tactical)
      GEngine->Draw2D
      (
        mip, tdCursorColor,
        Rect2DPixel(xScreen, yScreen, tdCurW * w2d, tdCurH * h2d),
        Rect2DPixel(tdX * w2d, tdY * h2d, tdW * w2d, tdH * h2d), tdCurShadow
      );
    //GEngine->TextBank()->ReleaseMipmap();
  }

  // strategy cursor background
  if (cursor2 != cursor && cursor2A > 0)
  { // all strategy cursors use the same background
    texture = cursor2;
    mip=GEngine->TextBank()->UseMipmap(texture,0,0);

    if (cxValid)
    {
      PackedColor color2A=PackedColorRGB(color,toIntFloor(color.A8()*cursor2A));
      GEngine->Draw2D(mip,color2A,Rect2DAbs(mx,my,mw,mh), Rect2DClipAbs, shadow);
    }
    if (tactical)
      GEngine->Draw2D
      (
        mip, tdCursorColor,
        Rect2DPixel(xScreen, yScreen, tdCurW * w2d, tdCurH * h2d),
        Rect2DPixel(tdX * w2d, tdY * h2d, tdW * w2d, tdH * h2d), tdCurShadow
      );
  }



  if( !info || !cxValid ) return true;
  //hide target info if you are not leader
  bool showHUD =Glob.config.IsEnabled(DTHUDPerm)|| Glob.config.IsEnabled(DTHUD);
  if(unit && unit->GetGroup() && unit->GetGroup()->Leader() != unit
    && !showHUD)  return true;

  bool reallyExtended=false;
  Object *vObj=target ? target->idExact : NULL;
  EntityAI *v = dyn_cast<EntityAI>(vObj);

  if( extended && v && v->CommanderUnit() && vehicle->CommanderUnit() )
  {
    AIGroup *vGroup=v->CommanderUnit()->GetGroup();
    AIGroup *vehGroup=vehicle->CommanderUnit()->GetGroup();
    reallyExtended = vGroup && vGroup == vehGroup;
    // TODO: if it is a renegade and network player, show its name
  }
  // draw information texts

  Point2DAbs pos = Point2DAbs(mx + mw, my);
  GEngine->PixelAlignXY(pos);

  float width = 0, height = 0;

  // dimmed colors
  //PackedColor bgColorA=PackedColorRGB(cursorBgColor,toIntFloor(cursorBgColor.A8()*cursorA));
  PackedColor ftColorA=PackedColorRGB(ftColor,toIntFloor(ftColor.A8()*cursorA));

  RString vName;
  RString extInfo;
  if( v )
  {
    Assert( target->GetType() );
    vName=target->GetType()->GetDisplayName(); // GetTextSingular(); (we want rather display name shown under cursor instead)
    if (drawHousePos && _housePos >= 0)
    {
      char buffer[256];
      sprintf(buffer, LocalizeString(IDS_HOUSE_POSITION), _housePos + 1);
      vName = vName + RString(" ") + RString(buffer);
    }
    AIBrain *u = v->CommanderUnit();
    if (u)
    {
      Person *person = u->GetPerson();
      if (person->IsNetworkPlayer() && !person->IsDamageDestroyed())
      {
        vName = vName + RString(" (") + person->GetPersonName() + RString(")");
      }
#if _ENABLE_CHEATS
      else if (CHECK_DIAG(DECombat) && v->GetVarName().GetLength() > 0)
        vName = vName + RString(" (") + v->GetVarName() + RString(")");
#endif
    }
    float vLen=GEngine->GetTextWidthF(_cursorSize, _cursorFont, vName);
    saturateMax(width,vLen);
    height += _cursorSize;

    if (reallyExtended)
    { // extended info expected
      AIBrain *u = v->CommanderUnit();
      // extended target info
      extInfo = Format(
        "%d: %s",
        u->GetUnit() ? u->GetUnit()->ID() : 0,
        (const char *)LocalizeString(IDS_PRIVATE + u->GetPerson()->GetInfo()._rank)
        );
      float extLen = GEngine->GetTextWidthF(_cursorSize, _cursorFont, extInfo);
      saturateMax(width, extLen);
      height += _cursorSize;
    }
  }
  bool distanceValid=_groundPointValid;
  float distance=_groundPointDistance;
/*
  if( v )
  {
    distanceValid=true;
    distance=v->Position().Distance(vehicle->Position());
  }
*/
  if (v)
  {
    distanceValid = true;
#if _ENABLE_CHEATS
    if (_showAll && target->idExact)
    {
      distance = target->idExact->AimingPosition(target->idExact->RenderVisualState()).Distance(vehicle->RenderVisualState().Position());
    }
    else
#endif
    {
      distance = target->LandAimingPosition(unit).Distance(vehicle->RenderVisualState().Position());
    }
  }

  if( distanceValid  && Glob.config.difficulty < 2)
  {
    // make space for distance info
    float dLen=GEngine->GetTextWidthF(_cursorSize, _cursorFont, LocalizeString(IDS_UI_POSITION_DISTANCE), distance);
    height += _cursorSize;
    saturateMax(width,dLen);
  }

  Assert(cxValid);
  //mip=GEngine->TextBank()->UseMipmap(textureDef,0,0);
  //GEngine->Draw2D(mip, bgColorA, Rect2DAbs(pos.x, pos.y, width * w2d, height * h2d));

#if _VBS3 //draw health  status
  float health = 0;

  Person *person = dyn_cast<Person>(v);
  if (person)
  {
    health = 1 - person->GetTotalDamage();
    saturate(health, 0, 1);
    PackedColor color;
    if (health > 0.5)
      color = barGreenColor;
    else if (health > 0.3)
      color = barYellowColor;
    else if (health > 0.15)
      color = barRedColor;

    GEngine->Draw2D(mip, color, Rect2DAbs(pos.x, pos.y + (height * h2d), width * w2d * health, height * h2d * 0.1));

#if _VBS3_RANKS
    AIUnitInfo &unitInfo = person->GetInfo();
    
    Texture* texture = GVBSVisuals.GetRankImage(0,unitInfo._rank, true);
    if(texture)
    {
      mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
      float size = _cursorSize *1.2;
      GEngine->Draw2D(mip, pictureColor, 
        Rect2DAbs(pos.x + (width - size) * w2d, pos.y
        , size* w2d, size* h2d)
      );
    }
#endif
  }
#endif

  if (distanceValid && Glob.config.difficulty < 2)
  {
    GEngine->DrawTextF
    (
      pos, _cursorSize, _cursorFont,
      ftColorA, cursorInfoShadow,  LocalizeString(IDS_UI_POSITION_DISTANCE), distance
     );
    pos.y += GEngine->PixelAlignedY(_cursorSize * h2d);
  }
  if (v)
  {
    // get AI info
    TargetSide side = target->GetType()->_typicalSide;
    if (!target->idExact->IRSignatureOn())
    {
      AIGroup *grp = v->GetGroup();
      if (!grp || grp != unit->GetGroup())
      {
        side = TCivilian; // empty marked as civilian
      }
    }

    if
    (
      Glob.config.IsEnabled(DTEnemyTag)
#if _ENABLE_CHEATS
      || _showAll
#endif
    )
    {
      side = target->GetSide();
      if (target->IsDestroyed()) side = TCivilian;
    }
    if (side == TCivilian) color = civilianColor;
    else if (vehicleCommander->IsFriendly(side)) color = friendlyColor;
    else if (vehicleCommander->IsEnemy(side)) color = enemyColor;
    else if (vehicleCommander->IsNeutral(side)) color = neutralColor;
    else color = unknownColor;
    PackedColor colorA=PackedColorRGB(color,toIntFloor(color.A8()*cursorA));

#if _ENABLE_CHEATS
    // do not use stringtable - debug texts only
    if( CHECK_DIAG(DECombat) )
    {
      RString extVName =
      (
        vName + RString(" ") +
        target->GetType()->GetDisplayName() + RString(" ") +
        FindEnumName(target->GetSide()) + RString(target->GetSideChecked() ? "" : "?")
      );
      GEngine->DrawText(pos, _cursorSize, _cursorFont, colorA, extVName, 0);
      pos.y += GEngine->PixelAlignedY(_cursorSize * h2d); 
    }
    else
#endif
    if(Glob.config.difficulty < 2)
    {
      GEngine->DrawText(pos, _cursorSize, _cursorFont, colorA, vName, cursorInfoShadow);
      pos.y += GEngine->PixelAlignedY(_cursorSize * h2d); 
    }
    if( reallyExtended )
    {
      // extended target info
      GEngine->DrawTextF(pos, _cursorSize, _cursorFont, ftColorA, cursorInfoShadow,  extInfo);
      pos.y += GEngine->PixelAlignedY(_cursorSize * h2d);
    }
#if _ENABLE_CHEATS
    float dFontSize = GEngine->GetDebugFontSize();
    Font *dFont = GEngine->GetDebugFont();
    // do not use stringtable - debug texts only
    if( CHECK_DIAG(DECombat) && !target->IsMinimal())
    {
      const TargetNormal *tgt = static_cast<const TargetNormal *>(target);
      GEngine->DrawText(pos, dFontSize, dFont,ftColorA, v->DiagText(),0);
      pos.y += GEngine->PixelAlignedY(dFontSize*h2d); 

      if (tgt->vanished)
      {
        GEngine->DrawText(pos, dFontSize, dFont,ftColorA, "Vanished",0);
        pos.y += GEngine->PixelAlignedY(dFontSize * h2d); 
      }
      if (tgt->destroyed)
      {
        GEngine->DrawText(pos, dFontSize, dFont,ftColorA, "Destroyed",0);
        pos.y += GEngine->PixelAlignedY(dFontSize * h2d); 
      }
      if (GWorld->GetMode() == GModeNetware)
      {
        // draw network ID
        GEngine->DrawTextF
        (
          pos, dFontSize, dFont, ftColorA, 0, 
          "%s: %x",
          v->IsLocal() ? "Local" : "Remote", v->GetNetworkId()
        );
        pos.y += GEngine->PixelAlignedY(dFontSize * h2d); 
      }

      AIBrain *u = v->CommanderUnit();
      if( u )
      {
        int id = u->GetUnit() ? u->GetUnit()->ID() : 0;
        GEngine->DrawTextF
        (
          pos, dFontSize, dFont,
          ftColorA, 0,  "TTL %.2f",u->GetTimeToLive()
        );
        pos.y += GEngine->PixelAlignedY(dFontSize * h2d); 
        BString<256> state;
        switch (u->GetState())
        {
        case AIUnit::Wait:
          sprintf(state, "Unit %d - WAIT",id);
          break;
        case AIUnit::Init:
          sprintf(state, "Unit %d - INIT",id);
          break;
        case AIUnit::Busy:
          sprintf(state, "Unit %d - BUSY",id);
          break;
        case AIUnit::Completed:
          sprintf(state, "Unit %d - OK",id);
          break;
        case AIUnit::Delay:
          sprintf(state, "Unit %d - DELAY %.1f",id, u->GetDelay()-Glob.time);
          break;
        case AIUnit::InCargo:
          sprintf(state, "Unit %d - IN CARGO",id);
          break;
        case AIUnit::Stopping:
          sprintf(state, "Unit %d - STOPPING",id);
          break;
        case AIUnit::Stopped:
          {
            Transport *trans = dyn_cast<Transport>(v);
            float t = trans ? trans->GetGetInTimeout() - Glob.time : 0;
            sprintf(state, "Unit %d - STOPPED %.1f",id, t);
          }
          break;
        case AIUnit::Replan:
          sprintf(state, "Unit %d - REPLAN",id);
          break;
        case AIUnit::Planning:
          sprintf(state, "Unit %d - PLANNING",id);
          break;
        default:
          sprintf(state, "Unit %d - UNDEF",id);
          break;
        }
        if (*state)
        {
          GEngine->DrawText(pos, dFontSize, dFont,ftColorA, state,0);
          pos.y += GEngine->PixelAlignedY(dFontSize * h2d); 
        }
        strcpy(state,"");
        AISubgroup *subgrp = u->GetUnit() ? u->GetUnit()->GetSubgroup() : NULL;
        if (subgrp)
        {
          switch (subgrp->GetMode())
          {       
          case AISubgroup::Wait:
            sprintf(state, "Subgrp %d:%.1f - WAIT",subgrp->Leader()->ID(),subgrp->GetFormationCoef());
            break;
          case AISubgroup::PlanAndGo:
            sprintf(state, "Subgrp %d:%.1f - PLAN&GO",subgrp->Leader()->ID(),subgrp->GetFormationCoef());
            break;
          case AISubgroup::DirectGo:
            sprintf(state, "Subgrp %d:%.1f - DIRECT GO",subgrp->Leader()->ID(),subgrp->GetFormationCoef());
            break;
          default:
            sprintf(state, "Subgrp %d:%.1f - UNDEF",subgrp->Leader()->ID(),subgrp->GetFormationCoef());
            break;
          }
          if (subgrp->GetCurrent())
          {
            strcat(state, " ");
            strcat(state, LocalizeString(IDS_NOCOMMAND + subgrp->GetCommand()->_message));
            strcat(state, " ");
            strcat(state, subgrp->GetCurrent()->_fsm->GetStateName());
          }
        }
        if (*state)
        {
          GEngine->DrawText(pos, dFontSize, dFont,ftColorA, state,0);
          pos.y += GEngine->PixelAlignedY(dFontSize * h2d); 
        }
        // semaphore
        strcpy(state,"");
        switch (u->GetSemaphore())
        {
          case AI::SemaphoreBlue:
            strcpy(state, "Never Fire, Keep Formation");
            break;
          case AI::SemaphoreGreen:
            strcpy(state, "Hold Fire, Keep Formation");
            break;
          case AI::SemaphoreWhite:
            strcpy(state, "Hold Fire, Loose Formation");
            break;
          case AI::SemaphoreYellow:
            strcpy(state, "Open Fire, Keep Formation");
            break;
          case AI::SemaphoreRed:
            strcpy(state, "Open Fire, Loose Formation");
            break;
        }
        if (*state)
        {
          GEngine->DrawText
          (
            pos, dFontSize, dFont, ftColorA, state,0
          );
          pos.y += GEngine->PixelAlignedY(dFontSize * h2d); 
        }
        
        if (u->IsLocal())
        {
          int nBeh = u->NBehaviourDiags();
          for (int i=0; i<nBeh; i++)
          {
            RString str = u->BehaviourDiags(i);
            if (str.GetLength()>0)
            {
              GEngine->DrawText(pos, dFontSize, dFont, ftColorA, str,0);
              pos.y += GEngine->PixelAlignedY(dFontSize * h2d); 
            }
          }
        }

        // behaviour
        strcpy(state,"");
        switch (u->GetCombatMode())
        {
          case CMCareless:
            strcpy(state, "Careless");
            break;
          case CMSafe:
            strcpy(state, "Safe");
            break;
          case CMAware:
            strcpy(state, "Aware");
            break;
          case CMCombat:
            strcpy(state, "Combat");
            break;
          case CMStealth:
            strcpy(state, "Stealth");
            break;
        }
        if (*state)
        {
          GEngine->DrawText
          (
            pos, dFontSize, dFont,
            ftColorA, state,0
          );
          pos.y += GEngine->PixelAlignedY(dFontSize * h2d); 
        }

        AIGroup *grp = u->GetGroup();
        if (grp && grp->GetCurrent())
        {
          strcpy(state, "Grp - ");
          strcat(state, grp->GetCurrent()->_fsm->GetStateName());
          if (*state)
          {
            GEngine->DrawText
            (
              pos, dFontSize, dFont,
              ftColorA, state,0
            );
            pos.y += GEngine->PixelAlignedY(dFontSize * h2d); 
          }
        }
      }
    }
#endif

#if _ENABLE_CHEATS
    if( (CHECK_DIAG(DECombat) || tdCheat || _showAll) && !target->IsMinimal())
    {
      // do not use stringtable - debug texts only
      const TargetNormal *tgt = static_cast<const TargetNormal *>(target);
      
      // draw sensor information
      GEngine->DrawTextF
      (
        pos, dFontSize, dFont, ftColorA, 0, 
        "uncertainity %.2f",
        tgt->FadingAccuracy()
      );
      pos.y += GEngine->PixelAlignedY(dFontSize * h2d);
      GEngine->DrawTextF
      (
        pos, dFontSize, dFont, ftColorA, 0, 
        "side Uncertainity %.2f",
        tgt->FadingSideAccuracy()
      );
      pos.y += GEngine->PixelAlignedY(dFontSize * h2d);
      GEngine->DrawTextF
      (
        pos, dFontSize, dFont, ftColorA, 0, 
        "spotability %.2f %.0f",
        tgt->FadingSpotability(),Glob.time.Diff(tgt->spotabilityTime)
      );
      pos.y += GEngine->PixelAlignedY(dFontSize * h2d);
      GEngine->DrawTextF
      (
        pos, dFontSize, dFont, ftColorA, 0, 
        "seen %.1f k=%c (d=%.1f,sd=%.1f)",
        Glob.time.Diff(tgt->lastSeen),
        tgt->IsKnownNoDelay()? 'Y' : 'N',
        tgt->delay.Diff(Glob.time),
        tgt->delaySensor.Diff(Glob.time)
      );
      pos.y += GEngine->PixelAlignedY(dFontSize * h2d);
      GEngine->DrawTextF
      (
        pos, dFontSize, dFont, ftColorA, 0, 
        "Error %.2f",tgt->FadingPosAccuracy()
      );
      pos.y += GEngine->PixelAlignedY(dFontSize * h2d);
      
      GEngine->DrawTextF
      (
        pos, dFontSize, dFont, ftColorA, 0, 
        "Lit %.2f, Pat50 %.2f, Pat300 %.2f, move %.2f, v. fire %.2f, aud %.2f, a. fire %.2f",
        v->GetLit(),
        v->GetHidden(50),v->GetHidden(300),
        v->VisibleMovement(),v->VisibleFire(),
        v->Audible(),v->AudibleFire()
      );
      pos.y += GEngine->PixelAlignedY(dFontSize * h2d);

      GEngine->DrawTextF
      (
        pos, dFontSize, dFont, ftColorA, 0, 
        "Interesting %.2f, interest %.2f",
        HowMuchInteresting(unit,tgt),
        HowMuchInteresting(unit,tgt)/unit->Position(unit->GetRenderVisualState()).Distance(tgt->position)
      );
      pos.y += GEngine->PixelAlignedY(dFontSize * h2d);
      //
      AIBrain *u = v->CommanderUnit();
      Person *vb=( u ? u->GetPerson() : NULL );
      Person *vbMe=
      (
        vehicle->CommanderUnit() ? vehicle->CommanderUnit()->GetPerson() : NULL
      );
      //SensorRowID rIdMe=vbMe->GetSensorRowID();
      SensorRowID rId=vb ? vb->GetSensorRowID() : SensorRowID(-1);
      SensorColID cId=v->GetSensorColID();
      SensorList *list=GWorld->GetSensorList();

      {
        RString diag="Vis ignored";
        if
        (
          vehicle->CommanderUnit() ||
          //vehicle->GetType()->IsKindOf(GWorld->Preloaded(VTypeStrategic)) ||
          vehicle->GetType()->IsKindOf(GWorld->Preloaded(VTypeAllVehicles))
        )
        {
          if( vbMe && v )
          {
            // do not check acutal visibility of non-strategic targets
            diag=list->DiagText(vbMe,v);
          }
        }
        GEngine->DrawText(pos, dFontSize, dFont, ftColorA,diag,0);
        pos.y += GEngine->PixelAlignedY(dFontSize * h2d);
      }
      GEngine->DrawTextF(
        pos, dFontSize, dFont, ftColorA, 0, 
        "land vis %.2f, fire vis %.2f",
        GLandscape->Visible(vehicle,v),
        GLandscape->Visible(vehicle,v,1,ObjIntersectFire)
      );
      #if _ENABLE_CHEATS
      if (GLandscape->Visible(vehicle,v)==0)
      {
        // debugging opportunity - check why not visible
        __asm nop;
        GLandscape->Visible(vehicle,v);
      }
      #endif
      pos.y += GEngine->PixelAlignedY(dFontSize * h2d);
      
      if (u && u->GetUnit())
      {
        GEngine->DrawTextF
        (
          pos, dFontSize, dFont, ftColorA, 0,
          "Form: %.0f, %.0f",
          u->GetUnit()->GetFormationRelative().X(), u->GetUnit()->GetFormationRelative().Z()
        );
        pos.y += GEngine->PixelAlignedY(dFontSize * h2d);
      }

      GEngine->DrawTextF(pos, dFontSize, dFont, ftColorA, 0,"Row %d, Col %d, next update %.1f",rId,cId,list->TimeToNextUpdate(vbMe,v)-Glob.time);
    }
#endif
  }
  return true;
}

/*!
\patch 5154 Date 4/27/2007 by Jirka
- Fixed: In-game UI - In wide-screen aspects, the aiming cursor disappeared outside the UI area of the screen
*/

void InGameUI::DrawCursor
(
  const Camera &camera, EntityAI *vehicle,
  Vector3Val dir, float size,
  Texture *texture, float offsetX,float offsetY, float width, float height,
  PackedColor color, bool drawInCompass, int shadow, bool drawArrows,
  PackedColor colorText, CursorTexts texts, bool fadeToCenter, float overrideY
)
{
  // width/height assume fovLeft/fovTop = 1/0.75
  AspectSettings asp;
  GEngine->GetAspectSettings(asp);
  width *= 1.0f/asp.leftFOV;
  height *= 0.75f/asp.topFOV;
  offsetX *= 1.0f/asp.leftFOV;
  offsetY *= 0.75f/asp.topFOV;

  width *= Glob.config.IGUIScale;
  height *= Glob.config.IGUIScale;

  const int w3d = GEngine->WidthBB();
  const int h3d = GEngine->HeightBB();

  const int w2d = GEngine->Width2D();
  const int h2d = GEngine->Height2D();

  Matrix4Val camInvTransform=camera.GetInvTransform();
  Vector3Val dirCam = camInvTransform.Rotate(dir);
  if (drawInCompass)
  {
    float xAngle = atan2(dirCam.X(), dirCam.Z());
    if
    (
      xAngle >= MIN_X && xAngle <= MAX_X
    )
    {

      float ySize=coH*h2d*0.5;
      float xSize=ySize*w2d/h2d*(3.0/4);

      float xScreen = toInt((coX + 0.5 * coW + xAngle * coW * INV_W) * w2d);
      float yScreen = toInt(coY * h2d - ySize);
      MipInfo mip=GEngine->TextBank()->UseMipmap(texture,0,0);
      GEngine->Draw2D
        (
        mip, color,
        Rect2DPixel(xScreen, yScreen, xSize, ySize),
        Rect2DPixel(xScreen, yScreen, xSize, ySize),shadow
        );
    }
  }

  float coef;
  if (size > -0.1)
    coef = actMin;
  else
    coef = 1;

  // calculate edges of safe area on the screen
  float xMin, yMin, xMax, yMax;
  asp.GetScreenSafeArea(xMin, yMin, xMax, yMax);
  xMax -= width * coef;
  yMax -= height * coef;

  float xScreen = 0, yScreen = 0;
  bool visibleX = true;
  bool visibleY = true;
  bool rightEdge = false;
  bool inside = false;
  bool topEdge = false;
  if (dirCam.Z() > 0)
  {
    float invZ= 1/dirCam.Z();
    xScreen = 0.5 * (1.0 + dirCam.X() * invZ * camera.InvLeft() - width * coef);
    yScreen = 0.5 * (1.0 - dirCam.Y() * invZ * camera.InvTop() - height * coef);

    if (overrideY<FLT_MAX) yScreen = overrideY;

    if( xScreen<xMin ) {xScreen=xMin;visibleX=false;}
    if( xScreen>xMax ) {xScreen=xMax;visibleX=false;rightEdge=true;}
    if( yScreen<yMin ) {yScreen=yMin;visibleY=false;}
    if( yScreen>yMax ) {yScreen=yMax;visibleY=false;topEdge = true;}

    inside = visibleX && visibleY;
  }
  else
  {  
    visibleX = visibleY = false;
  }

  if(!inside && !drawArrows) return;

  if(fadeToCenter)
  {
    float dist = 5*((xScreen - 0.5f)*(xScreen - 0.5f) + (yScreen - 0.5f)*(yScreen - 0.5f));
    saturate(dist,_iconMissionMaxFade,1.0f);

    color = PackedColorRGB(color, toIntFloor(color.A8() * dist));
    colorText = PackedColorRGB(colorText, toIntFloor(colorText.A8() * dist));
  }
  
  if(!visibleY)
  {//if icon is bellow/over screen
    Vector3 dir = dirCam;
    // saturate x to fit in screen
    // calculation:
    // xs = dirCam.X()/dirCam.Z() * camera.InvLeft();
    // assume dirCam.Z()==1
    // xs should be in (-0.5,+0.5)
    // xs = dirCam.X()*camera.InvLeft();
    // dirCam.X() = xs/camera.InvLeft() = xs*camera.Left()
    float yAngle = atan2(dir[1],dir[2]);
    // wanted angle
    float wyAngle = atan2(fSign(dir[1])*camera.Left(),1);
    Matrix3 rotX(MRotationX,yAngle-wyAngle);
    dir = rotX*dir;
    topEdge = ( dir[1]>0 );

    coef = actMin;

    asp.GetScreenSafeArea(xMin, yMin, xMax, yMax);
    xMax -= width * coef;
    yMax -= height * coef;

    //float invZ= dir.Z()!=0 ? 1/dir.Z() : 0;
    //xScreen = 0.5 * (1.0 + dir.X() * invZ * camera.InvLeft() - width * coef);
    //yScreen = 0.5 * (1.0 - dir.Y() * invZ * camera.InvTop() - height * coef);

    saturate(xScreen,xMin,xMax);
    saturate(yScreen,yMin,yMax);

    texture = GLOB_SCENE->Preloaded(CursorOutArrow);
    //visibleX = true;
    //visibleY = true;
  }

  if( !visibleX )
  {
    Vector3 dir = dirCam;
    // saturate x to fit in screen
    // calculation:
    // xs = dirCam.X()/dirCam.Z() * camera.InvLeft();
    // assume dirCam.Z()==1
    // xs should be in (-0.5,+0.5)
    // xs = dirCam.X()*camera.InvLeft();
    // dirCam.X() = xs/camera.InvLeft() = xs*camera.Left()
    float xAngle = atan2(dir[0],dir[2]);
    // wanted angle
    float wxAngle = atan2(fSign(dir[0])*camera.Left(),1);
    Matrix3 rotY(MRotationY,xAngle-wxAngle);
    dir = rotY*dir;
    rightEdge = ( dir[0]>0 );

    coef = actMin;

    asp.GetScreenSafeArea(xMin, yMin, xMax, yMax);
    xMax -= width * coef;
    yMax -= height * coef;

    float invZ= dir.Z()!=0 ? 1/dir.Z() : 0;
    xScreen = 0.5 * (1.0 + dir.X() * invZ * camera.InvLeft() - width * coef);
    yScreen = 0.5 * (1.0 - dir.Y() * invZ * camera.InvTop() - height * coef);

    saturate(xScreen,xMin,xMax);
    saturate(yScreen,yMin,yMax);

    texture = GLOB_SCENE->Preloaded(CursorOutArrow);
    visibleX = true;
    visibleY = true;
  }
    
  if (visibleX)
  {
    Draw2DParsExt pars;
    pars.mip = GEngine->TextBank()->UseMipmap(texture,0,0);
    pars.spec = NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
    pars.spec|= (shadow == 2)?UISHADOW : 0; 

    if(visibleY)
    {
      if( rightEdge )
        pars.SetU(1,0);
      else
        pars.SetU(0,1);
      pars.SetV(0,1);
    }
    else
    {//hladas fix: arrows should be drawn on a bottom and top of a screen
      if( topEdge )
      {
        pars.uTL = 0; pars.vTL = 0;
        pars.uTR = 0; pars.vTR = 1;
        pars.uBL = 1; pars.vBL = 0;
        pars.uBR = 1; pars.vBR = 1;
      }
      else
      {
        pars.uTL = 1; pars.vTL = 1;
        pars.uTR = 1; pars.vTR = 0;
        pars.uBL = 0; pars.vBL = 1;
        pars.uBR = 0; pars.vBR = 0;
      }
    }
    Rect2DAbs rect;
    rect.x = xScreen*w3d;
    rect.y = yScreen*h3d;
    rect.w = width*coef*w3d;
    rect.h = height*coef*h3d;

    if(inside)
    {
      rect.x += offsetX*coef*w3d;
      rect.y += offsetY*coef*h3d;
    }

    if (shadow == 1)
    {
      rect.x += 1;
      rect.y += 1;

      PackedColor colorBlack=PackedColorRGB(PackedBlack, color.A8());
      pars.SetColor(colorBlack);
      GEngine->Draw2D(pars, rect);

      rect.x -= 1;
      rect.y -= 1;
    }
    pars.SetColor(color);

    GEngine->Draw2D(pars,rect);

    //GEngine->TextBank()->ReleaseMipmap();

    for (int i=0; i<texts.Size(); i++)
    {
      RString text = texts[i].text;
      if (!text || text.GetLength() == 0) continue;
      float upDown = texts[i].upDown;
      bool allignRight = texts[i].allignRight;

      float textW = GEngine->GetTextWidth(_cursorSize, _cursorFont, text)*w2d/w3d;
      float yScreenText = yScreen + height * coef * upDown;
      float xScreenText = xScreen;
      if(allignRight) xScreenText += width * coef;
      else xScreenText += 0.5*width * coef - 0.5*textW;
    
      if (xScreen>=xMax)
      {
        xScreenText = xScreen - textW - 0.0003;
        yScreenText = yScreen + 0.5*height * coef + (i-1) * _cursorSize;
      }
      else if(xScreen<=xMin)
      {
        xScreenText = xScreen + width * coef + 0.0003;
        yScreenText = yScreen + 0.5*height * coef + (i-1) * _cursorSize;
      }
      else if(yScreen<=yMin)
      {
        yScreenText = yScreen + height * coef + i * _cursorSize;
      }
      else if(yScreen>=yMax )
      {
        yScreenText = yScreen  - (texts.Size()-i) * _cursorSize;
      }

      //int a = toInt(colorText.A8() * cursorBgColor.A8() * (1.0f / 255.0f));
      //PackedColor bgColorText = PackedColorRGB(cursorBgColor, a);

      //MipInfo mip=GEngine->TextBank()->UseMipmap(GLOB_SCENE->Preloaded(TextureWhite),0,0);
      //GEngine->Draw2D
      //(
      //  mip, bgColorText,
      //  Rect2DAbs(xScreenText * w3d, yScreenText * h3d, textW * w3d, _cursorSize * h2d)
      //);
      //GEngine->TextBank()->ReleaseMipmap();

      Point2DAbs pos(xScreenText*w3d , yScreenText* h3d);
      GEngine->PixelAlignXY(pos);
      GEngine->DrawText(pos, _cursorSize, _cursorFont, colorText, text, shadow);
    }
  }
}

void InGameUI::DrawCursorDetail
(
 const Camera &camera, EntityAI *vehicle,
 Vector3Val dir, float size,
 Texture *texture, float offsetX,float offsetY, float width, float height,
 PackedColor color, int shadow, bool drawOnSide, bool fadeToCenter
 )
{
  // width/height assume fovLeft/fovTop = 1/0.75
  AspectSettings asp;
  GEngine->GetAspectSettings(asp);
  width *= 1.0f/asp.leftFOV;
  height *= 0.75f/asp.topFOV;
  offsetX *= 1.0f/asp.leftFOV;
  offsetY *= 0.75f/asp.topFOV;

  const int w3d = GEngine->WidthBB();
  const int h3d = GEngine->HeightBB();

  width *= Glob.config.IGUIScale;
  height *= Glob.config.IGUIScale;

  Matrix4Val camInvTransform=camera.GetInvTransform();
  Vector3Val dirCam = camInvTransform.Rotate(dir);

  float coef;
  if (size > -0.1)
    coef = actMin;
  else
    coef = 1;

  // calculate edges of safe area on the screen
  float xMin, yMin, xMax, yMax;
  asp.GetScreenSafeArea(xMin, yMin, xMax, yMax);
  xMax -= width * coef;
  yMax -= height * coef;

  float xScreen = 0, yScreen = 0;
  bool visibleX = true;
  bool visibleY = true;
  bool inside = false;

  if (dirCam.Z() > 0)
  {
    float invZ= 1/dirCam.Z();
    xScreen = 0.5 * (1.0 + dirCam.X() * invZ * camera.InvLeft() - width * coef);
    yScreen = 0.5 * (1.0 - dirCam.Y() * invZ * camera.InvTop() - height * coef);
    if( xScreen<xMin ) {xScreen=xMin;visibleX=false;}
    if( xScreen>xMax ) {xScreen=xMax;visibleX=false;}
    if( yScreen<yMin ) {yScreen=yMin;visibleY=false;}
    if( yScreen>yMax ) {yScreen=yMax;visibleY=false;}

    inside = visibleX && visibleY;
  }
  else
  {  
    visibleX = visibleY = false;
  }

  if(fadeToCenter)
  {
    float dist = 5*((xScreen - 0.5f)*(xScreen - 0.5f) + (yScreen - 0.5f)*(yScreen - 0.5f));
    saturate(dist,_iconMissionMaxFade,1.0f);

    color = PackedColorRGB(color, toIntFloor(color.A8() * dist));
  }

  if(!visibleY)
  {//if icon is bellow/over screen
    Vector3 dir = dirCam;
    float yAngle = atan2(dir[1],dir[2]);
    // wanted angle
    float wyAngle = atan2(fSign(dir[1])*camera.Left(),1);
    Matrix3 rotX(MRotationX,yAngle-wyAngle);
    dir = rotX*dir;

    coef = actMin;

    asp.GetScreenSafeArea(xMin, yMin, xMax, yMax);
    xMax -= width * coef;
    yMax -= height * coef;

    //float invZ= dir.Z()!=0 ? 1/dir.Z() : 0;
    //xScreen = 0.5 * (1.0 + dir.X() * invZ * camera.InvLeft() - width * coef);
    //yScreen = 0.5 * (1.0 - dir.Y() * invZ * camera.InvTop() - height * coef);

    saturate(xScreen,xMin,xMax);
    saturate(yScreen,yMin,yMax);
  }

  if( !visibleX )
  {
    Vector3 dir = dirCam;
    float xAngle = atan2(dir[0],dir[2]);
    // wanted angle
    float wxAngle = atan2(fSign(dir[0])*camera.Left(),1);
    Matrix3 rotY(MRotationY,xAngle-wxAngle);
    dir = rotY*dir;

    coef = actMin;

    asp.GetScreenSafeArea(xMin, yMin, xMax, yMax);
    xMax -= width * coef;
    yMax -= height * coef;

    float invZ= dir.Z()!=0 ? 1/dir.Z() : 0;
    xScreen = 0.5 * (1.0 + dir.X() * invZ * camera.InvLeft() - width * coef);
    yScreen = 0.5 * (1.0 - dir.Y() * invZ * camera.InvTop() - height * coef);

    saturate(xScreen,xMin,xMax);
    saturate(yScreen,yMin,yMax);

    visibleX = true;
    visibleY = true;
  }

  if(!inside && !drawOnSide) return;

  Draw2DParsExt pars;
  pars.mip = GEngine->TextBank()->UseMipmap(texture,0,0);
  pars.spec = NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
  pars.spec|= (shadow == 2)?UISHADOW : 0; 

  pars.SetU(0,1);
  pars.SetV(0,1);

  Rect2DAbs rect;
  rect.x = xScreen*w3d;
  rect.y = yScreen*h3d;
  rect.w = width*coef*w3d;
  rect.h = height*coef*h3d;

  if(inside)
  {
    rect.x += offsetX*coef*w3d;
    rect.y += offsetY*coef*h3d;
  }

  if (shadow == 1)
  {
    rect.x += 1;
    rect.y += 1;

    PackedColor colorBlack=PackedColorRGB(PackedBlack, color.A8());
    pars.SetColor(colorBlack);
    GEngine->Draw2D(pars, rect);

    rect.x -= 1;
    rect.y -= 1;
  }
  pars.SetColor(color);

  GEngine->Draw2D(pars,rect);  
  
}


Point2DFloat InGameUI::WorldToScreen
(
 const Camera &camera, Vector3Val dir, float size )
{
  // width/height assume fovLeft/fovTop = 1/0.75
  AspectSettings asp;
  GEngine->GetAspectSettings(asp);
  float width = 0;
  float height = 0;

  Matrix4Val camInvTransform=camera.GetInvTransform();
  Vector3Val dirCam = camInvTransform.Rotate(dir);

  float coef;
  if (size > 0)
  {
    float invSize = dirCam.SquareSize()>0 ? dirCam.InvSize() : 1e10f;
    coef = size * invSize * camera.InvLeft();
    saturate(coef, actMin, actMax);
  }
  else if (size > -0.1)
    coef = actMin;
  else
    coef = 1;

  // calculate edges of safe area on the screen
  float xMin, yMin, xMax, yMax;
  asp.GetScreenSafeArea(xMin, yMin, xMax, yMax);
  xMax -= width * coef;
  yMax -= height * coef;

  float xScreen = 0, yScreen = 0;
  if (dirCam.Z() > 0)
  {
    float invZ= 1/dirCam.Z();
    xScreen = 0.5 * (1.0 + dirCam.X() * invZ * camera.InvLeft() - width * coef);
    yScreen = 0.5 * (1.0 - dirCam.Y() * invZ * camera.InvTop() - height * coef);
  }

  return Point2DFloat(xScreen,yScreen);
}

PackedColor BlinkColor(PackedColor color1, PackedColor color2, float t)
{
  float phase = 2 * H_PI * (t - toIntFloor(t));
  float coef = 0.5 * (1 + sin(phase));
  Color color = Color(safe_cast<Color>(color1)) * coef + Color(safe_cast<Color>(color2)) * (1 - coef);
  return PackedColor(color);
}

void InGameUI::DrawCommand(const Command &cmd, AIUnit *unit, const Camera &camera, bool td, float alphaCursor, float alphaText, float age)
{
  PackedColor missionColor = missionColor1;

  Vector3 pos;
  float size = 0;
  Ref<Texture> taskIcon = _iconMission;
  Ref<Texture> taskIconDetail = NULL;
  float offsetY = 0;

  switch (cmd._message)
  {
  case Command::Move:
    taskIconDetail = _iconMove;
    missionColor = _iconMoveColor;
    pos = cmd._destination;
    offsetY = -0.5f;
    break;
  case Command::Heal:
    taskIconDetail = _iconHealAt;
    missionColor = _iconHealAtColor;
    pos = cmd._destination;
    break;
  case Command::HealSoldier:
    taskIconDetail = _iconHealAt;
    missionColor = _iconHealAtColor;
    pos = cmd._destination;
    break;
  case Command::FirstAid:
    taskIconDetail = _iconHealAt;
    missionColor = _iconHealAtColor;
    pos = cmd._destination;
    break;
  case Command::RepairVehicle:
    taskIconDetail = _iconRepairAt;
    missionColor = _iconRepairAtColor;
    pos = cmd._destination;
    break;
  case Command::Repair:
    taskIconDetail = _iconRepairAt;
    missionColor = _iconRepairAtColor;
    pos = cmd._destination;
    break;
  case Command::Refuel:
    taskIconDetail = _iconRefuelAt;
    missionColor = _iconRefuelAtColor;
    pos = cmd._destination;
    break;
  case Command::Rearm:
    taskIconDetail = _iconRearmAt;
    missionColor = _iconRearmAtColor;
    pos = cmd._destination;
    break;
  case Command::Support:
    taskIconDetail = _iconSupport;
    missionColor = _iconSupportColor;
    pos = cmd._destination;
    break;
  case Command::Dismiss:
    pos = cmd._destination;
    break;
  case Command::Attack:
  case Command::AttackAndFire:
  case Command::Fire:
    // TODO: only known position
    {
      Target *target=cmd._targetE;
      if( !target )
      {
        // this should be used very rarely - we use Target most of the time
        Object *tgt = cmd._target;

        const TargetList *pVisibleList = VisibleList();
        if (!pVisibleList) return;
        const TargetList &visibleList=*pVisibleList;
        int n = visibleList.AnyCount();
        for (int i=0; i<n; i++)
        {
          Target *tar = visibleList.GetAny(i);
          if (tgt == tar->idExact)
          {
            target = tar;
            break;
          }
        }
      }
      if (!target) return;
      pos = target->LandAimingPosition();
      size = target->VisibleSize();
      taskIcon = _iconAttack;
      missionColor = _iconAttackColor;
    }
    break;
  case Command::FireAtPosition:
    // TODO: only known position
    {
      Target *target=cmd._targetE;
      if( !target )
      {
        // this should be used very rarely - we use Target most of the time
        Object *tgt = cmd._target;

        const TargetList *pVisibleList = VisibleList();
        if (!pVisibleList) return;
        const TargetList &visibleList=*pVisibleList;

        int n = visibleList.AnyCount();
        for (int i=0; i<n; i++)
        {
          Target *tar = visibleList.GetAny(i);
          if (tgt == tar->idExact)
          {
            target = tar;
            break;
          }
        }
      }
      if (!target) return;
      pos = target->LandAimingPosition();
      size = target->VisibleSize();
      taskIcon = _iconAttack;
      missionColor = _iconAttackColor;
    }
    break;
  case Command::GetIn:
    // TODO: only known position
    {
      Target *target=cmd._targetE;
      if( !target )
      {
        // this should be used very rarely - we use Target most of the time
        Object *tgt = cmd._target;
        
        const TargetList *pVisibleList = VisibleList();
        if (!pVisibleList) return;
        const TargetList &visibleList=*pVisibleList;

        int n = visibleList.AnyCount();
        for (int i=0; i<n; i++)
        {
          Target *tar = visibleList.GetAny(i);
          if (tgt == tar->idExact)
          {
            target = tar;
            break;
          }
        }
      }
      if (!target) return;
      pos = target->LandAimingPosition();
      size = target->VisibleSize();
      taskIcon = _iconBoard;
      taskIconDetail = _iconBoardIn;
      missionColor = _iconBoardColor;
    }
    break;
  case Command::GetOut:
    {
      pos = unit->AimingPosition(unit->GetRenderVisualState());
      size = unit->VisibleSize(unit->GetRenderVisualState());
      taskIcon = _iconBoard;
      taskIconDetail = _iconBoardOut;
      missionColor = _iconBoardColor;
    }
    break;
  case Command::Join:
    {
      AISubgroup *s = cmd._joinToSubgroup;
      if (!s) s = unit->GetGroup()->MainSubgroup();
      Assert(s);
      Assert(s != unit->GetSubgroup());
      AIUnit *u = s->Leader();
      if (!u) return;
      pos = u->AimingPosition(u->GetRenderVisualState());
      size = u->VisibleSize(u->GetRenderVisualState());
      taskIconDetail = _iconJoin;
      missionColor = _iconJoinColor;
    }
  case Command::TakeBag:
  case Command::Assemble:
  case Command::DisAssemble:
    {
      taskIconDetail = _iconMove;
      missionColor = _iconMoveColor;
      pos = cmd._destination;
      break;
    }
    break;
  default:
    return;
  }

  bool commandtext = (cmd._message != (Command::Action));
  PackedColor colorCursor = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alphaCursor));
  PackedColor colorText = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alphaText));
  if (colorCursor.A8() == 0 && colorText.A8() == 0) return;

  Vector3 dir = pos - camera.Position();
  Vector3 dirU = pos - unit->AimingPosition(unit->GetRenderVisualState());
  EntityAIFull *vehicle = unit->GetVehicle();
  if(!vehicle) return;

  CursorTexts texts;
  if (colorText.A8() > 0)
  {
    int icmd = cmd._message;
    if (icmd==Command::AttackAndFire) icmd = Command::Attack;
    if (icmd==Command::FireAtPosition) icmd = Command::Attack;
    if (icmd==Command::RepairVehicle) icmd = Command::Repair;

    RString command = LocalizeString(IDS_NOCOMMAND + icmd);
    if (toInt(dirU.Size()) > 0)
    {     
      if(commandtext)
      {
        texts.Add(command, 1.0f - offsetY);
        char buffer[256];
        sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
        texts.Add(buffer, 1.7f - offsetY);
      }
      else
      { 
        char buffer[256];
        sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
        texts.Add(buffer, 1.0f - offsetY);
      }
    }
    else
      texts.Add(command, 1.0f - offsetY);
  }
  DrawCursor
  (
    camera, unit->GetVehicle(), dir,
    size,
    taskIcon, 0, offsetY*actH, actW, actH,
    colorCursor, td, cursorShadow, true, colorText, texts,true
  );
  if(taskIconDetail)
    DrawCursorDetail
    (
    camera, unit->GetVehicle(), dir,
    size,
    taskIconDetail, 0, offsetY*actH, actW, actH,
    colorCursor, cursorShadow, true,true
    );
}

void InGameUI::DrawHUDCameraEffect(const Camera &camera, CameraType cam)
{
  // draw unit related information
  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return;

  EntityAIFull *vehicle = agent->GetVehicle();
  if (!vehicle) return;

  bool enabledCommanding = true;
  if(GWorld->GetCameraEffect())
  {
    Vehicle *veh = dynamic_cast<Entity *>(GWorld->GetCameraEffect()->GetObject());
    if(veh) enabledCommanding = (veh->EnabledCommanding());
  }

  if(_showGroupInfo && enabledCommanding)
  {
    if (!_xboxStyle && !_drawHCCommand)
    {
      AIGroup *group = agent->GetGroup();
      if (group && group->Leader() == agent && group->UnitsCount() > 1)
        DrawGroupInfo(vehicle);
    }

    if (!_xboxStyle &&  _drawHCCommand)
    {
      DrawHCInfo(vehicle);
    }
  }

  if (_showMenu)
    DrawMenu();

  DrawHint();
  DrawTaskHint();
}

void InGameUI::DrawHUDNonAI
(
  const Camera &camera, Entity *vehicle, CameraType cam
)
{
  if (!GWorld->HasOptions())
  {

    // dim cursor with time when non-active
//    float age=GetCursorAge();
//    const float startDim=5,endDim=10;
    float cursorA=1;
//    if( age>startDim )
//    {
//      // interpolate dim
//      if( age<endDim )
//      {
//        float factor=(age-startDim)*(1/(endDim-startDim));
//        cursorA=(1-factor)+cursorDim*factor;
//      }
//      else cursorA=cursorDim;
//      // update alpha
//    }
    
    Vector3 newDir = GetCursorDirection();

    if (_showCursors && _showHUD)
    {
      PackedColor color=PackedColorRGB(cursorColor,toIntFloor(cursorColor.A8()*cursorA));

      Texture *cursor1 = GPreloadedTextures.New(CursorAim);

      //{{
      // adapted (simplified) from DrawTargetInfo

      Matrix4Val camInvTransform=camera.GetInvTransform();

      const float mScrH=32.0/600;
      const float mScrW=32.0/800;

      float cx,cy;
      Vector3 pos = camInvTransform.Rotate(newDir);
      if (pos.Z() > 0)
      {
        float invZ = 1.0 / pos.Z();
        cx = pos.X() * invZ * camera.InvLeft();
        cy = - pos.Y() * invZ * camera.InvTop();

        float mScrX=cx*0.5+0.5-mScrW*0.5;
        float mScrY=cy*0.5+0.5-mScrH*0.5;
        const int w = GEngine->WidthBB();
        const int h = GEngine->HeightBB();
        
        int mx = toInt(mScrX * w);
        int my = toInt(mScrY * h);
        int mw = toInt(mScrW * w);
        int mh = toInt(mScrH * h);

        MipInfo mip;

        mip=GEngine->TextBank()->UseMipmap(cursor1,0,0);

        // draw black to see cursor on light background
        PackedColor colorBlack=PackedColorRGB(PackedBlack, color.A8());
        GEngine->Draw2D(mip,colorBlack,Rect2DAbs(mx+1,my+1,mw,mh));
        
        GEngine->Draw2D(mip,color,Rect2DAbs(mx,my,mw,mh));
      }

      ////////////////////}}

    }

  }
}

void InGameUI::Draw3D()
{
  if (IsHUDShown() && ScriptIsHUDShown())
  {
    // TODO: check cover is interesting now for us based on the commanding mode?
//     if (_cover)
//     {
//       // TODO: nice cover display
//       float vertical = 0;
//       switch (_cover->_type)
//       {
//         case CoverRightCornerHigh: case CoverLeftCornerHigh: case CoverWall: vertical = 1.0f; break;
//         case CoverRightCornerMiddle: case CoverLeftCornerMiddle: case CoverEdgeMiddle: vertical = 0.7f; break;
//         default: vertical = 0.2f; break;
//       }
//       GScene->DrawDiagSphere(_cover->_coverPos.FastTransform(Vector3(0,vertical,-0.2f)),0.5f,cursorColor,true);
//     }
  }
}

void InGameUI::DrawHUD(const Camera &camera, CameraType cam)
{
  // draw overlay - called before FinishDraw
  // draw global information
  PackedColor color;

  // draw unit related information
  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return;

  EntityAIFull *vehicle = agent->GetVehicle();
  if (!vehicle) return;

  // some actions (commanding or actions menu, group info) can be performed only by a player himself
  bool playerControlled = agent->GetPerson() && agent->GetPerson() == GWorld->PlayerOn();

  bool isMap = GWorld->HasMap();
  AIBrain *commanderUnit = vehicle->CommanderUnit();
  if (!commanderUnit) commanderUnit = agent;

  bool leaderMode = _xboxStyle && (_menu.Size() > 0 || _units);

  bool canSeeTD = false;
  bool canSeeCompass = false;
  Transport *transport = agent->GetVehicleIn();
  if (transport)
  {
    int canSee = 0;
    if (agent == transport->ObserverUnit()) canSee = transport->Type()->_commanderCanSee;
    else if (agent == transport->PilotUnit()) canSee = transport->Type()->_driverCanSee;
    else if (agent == transport->GunnerUnit()) canSee = transport->Type()->_gunnerCanSee;
    canSeeTD = (canSee&CanSeeRadar)!=0;
    canSeeCompass = (canSee&CanSeeCompass)!=0;
  }
#if _VBS3 //enable weapons to show canSee options as well
  else
  {
    if(agent->GetPerson())
    {
      const ViewPars* viewPars = agent->GetPerson()->GetViewPars(false);
      if(viewPars && viewPars->Initialized())
      {
        canSeeTD = (viewPars->_canSee & CanSeeRadar)!=0;
        canSeeCompass = (viewPars->_canSee & CanSeeCompass)!=0;
      }
    }
  }
#endif

  bool td = (
#if _ENABLE_CHEATS
    tdCheat || _showAll ||
#endif
    _showTacticalDisplay && canSeeTD) && transport && (transport->Type()->_radarType & TacticalDisplay) > 0;

  bool radar =
#if _ENABLE_CHEATS
    tdCheat || _showAll ||
#endif
    _showTacticalDisplay && canSeeTD;

  bool compass = !leaderMode && canSeeCompass && _showCompass;

  if (leaderMode) td = false;

  if (!leaderMode)
  {
    if(!isMap) DrawWeaponInfo(camera);

    _hintTop = piY;
    if (_showUnitInfo && !isMap)
    {
      DrawUnitInfo(vehicle);
    }
  }

  if (!_xboxStyle && _showGroupInfo && !_drawHCCommand && playerControlled)
  {
    AIGroup *group = agent->GetGroup();
    if (group && group->Leader() == agent && group->UnitsCount() > 1)
      DrawGroupInfo(vehicle);
  }

  if (!_xboxStyle && _showGroupInfo && _drawHCCommand && playerControlled)
  {
    DrawHCInfo(vehicle);
  }

  if (_showMenu && playerControlled)
    DrawMenu();


#ifndef _XBOX
  if (GetNetworkManager().IsControlsPaused())
  {
    float age = GetNetworkManager().GetLastMsgAgeReliable();
    BString<256> text;
    sprintf(text, LocalizeString(IDS_MP_NO_MESSAGE), age);

    float w = GEngine->GetTextWidth(_clSize,_clFont,text);
    float h = _clSize;
    GEngine->DrawText
    (
      Point2DFloat(_clX + 0.5f * (_clW - w), _clY + 0.5f * (_clH - h)), _clSize,
      _clFont, _clColor, text, _clShadow
    );
  }
#endif

  if (isMap) return;

  bool playerRemoteControled =  (agent->GetPerson() && agent->GetPerson() == GWorld->PlayerOn()) 
    ||(GWorld->PlayerOn() ==  GWorld->FocusOn()->GetRemoteControlled() );

  if (!leaderMode && _showCursors && _showHUD && !GWorld->GetPlayerSuspended() /*&& !agent->IsSpeaking() && !agent->IsListening()*/ &&
    GWorld->UserDialog() == NULL && playerRemoteControled) _actions.OnDraw();

  // TD cheat
#if _ENABLE_CHEATS
  if (GInput.GetCheat1ToDo(DIK_D))
  {
    tdCheat=!tdCheat;
    GlobalShowMessage(500,"ShowDBase %s",tdCheat ? "On":"Off");
  }
#endif

  if (!leaderMode && Glob.config.IsEnabled(DTClockIndicator))
  {
    AIGroup *group = agent->GetGroup();
    if (group) DrawGroupDir(camera, group);
  }

  if(radar && transport)
  { // note: tactical display/radar does not necessary imply seeing compass

    const TargetList *pVisibleList = VisibleList();
    if (pVisibleList)
    {
      const TargetList &visibleList=*pVisibleList;

      if((transport->Type()->_radarType & TacticalDisplay) > 0)
        DrawTacticalDisplay(camera, agent, visibleList);

      if((transport->Type()->_radarType & TankRadar) > 0) 
        DrawRadar(camera, agent, visibleList,_radarTankBackgroundTexture);

      if((transport->Type()->_radarType & AirRadar) > 0)
        DrawRadar(camera, agent, visibleList,_radarAirBackgroundTexture);
    }
  }

  if (compass)
    DrawCompass(vehicle);
  

  if (!leaderMode && _showTankDirection)
    DrawTankDirection(camera);

  if (_showCursors && _showHUD)
  {
    DrawCursors(camera, canSeeCompass, compass);
  }

  CameraType type = GWorld->GetCameraType();
  if (_showCursors && _showHUD)
  {
    // draw weapon cursor
    const float mouseScrH=64.0/600;
    const float mouseScrW=64.0/800;

#if _VBS3 //draw proper weapon cursor, changes vehicle!!! 
    if(agent->GetPerson() && agent->GetPerson()->IsPersonalItemsEnabled())
      vehicle = agent->GetPerson();
#endif

    int weapon = -1;
    TurretContextV context; 
    if (vehicle->FindTurret(unconst_cast(vehicle->RenderVisualState()), agent->GetPerson(), context) && context._weapons->_magazineSlots.Size() > 0)
      weapon = context._weapons->ValidatedCurrentWeapon();
    // for vehicle commander without weapons, check the primary weapons instead
    else if (agent == vehicle->CommanderUnit() && vehicle->GetPrimaryGunnerTurret(unconst_cast(vehicle->RenderVisualState()), context) && context._weapons->_magazineSlots.Size() > 0)
      weapon = context._weapons->ValidatedCurrentWeapon();
    if
    (
      weapon >= 0 &&
      context._weapons->_magazineSlots[weapon]._magazine &&
      vehicle->ShowAim(context, weapon, type, agent->GetPerson()) &&
      GWorld->UserDialog() == NULL
    )
    {
      if( !ModeIsStrategy(_mode) || _lockTarget!=NULL || _lockAimValidUntil>=Glob.uiTime )
      {
        if (canSeeTD || Glob.config.IsEnabled(DTWeaponCursor))
        {
          //hide cursor when some is on
          if (!ModeIsStrategy(_modeAuto))
          {
            const CursorTextureInfo *cursorTex = vehicle->GetCursorAimTexture(agent->GetPerson());
            Ref<Texture> tex = cursorTex ? cursorTex->texture : NULL;
            PackedColor color = cursorTex ? cursorTex->color : cursorColor;
            int shadow = cursorTex ? cursorTex->shadow : 2;
            // do not apply sections on aim cursor
            // if (cursorTex && cursorTex->sections.Size() > 0) sections = &cursorTex->sections;
            float offsetX = 0;
            float offsetY = 0;

            if (!tex) tex = GPreloadedTextures.New(CursorWeapon);
            if (tex)
            {

              DrawCursor
                (
                camera, vehicle, vehicle->GetAimCursorDirection(context, weapon),
                -1,
                tex, offsetX,offsetY, mouseScrW  * _weaponCursorScaleCoef, mouseScrH * _weaponCursorScaleCoef,
                color, td, shadow,false
                );
            }
          }
        }
      }
    }

#if _ENABLE_CHEATS
    if (!agent->IsAnyPlayer() && CHECK_DIAG(DECombat))
    {
      // draw watch direction (green)
      Texture *texture=GLOB_SCENE->Preloaded(CursorTarget);
      static const PackedColor watchColorTgt(Color(0,1,0,0.5));
      static const PackedColor watchColorPos(Color(1,1,0,0.5));
      static const PackedColor watchColorDir(Color(0,1,1,0.5));
      static const PackedColor watchColorFrm(Color(0,1,1,0.25));

      static const PackedColor targetColor(Color(1,0,0));

      PackedColor color;
      switch (agent->GetWatchMode())
      {
        case AIUnit::WMTgt: color = watchColorTgt; break;
        case AIUnit::WMPos: color = watchColorPos; break;
        case AIUnit::WMNo:  color = watchColorFrm; break;
        //case AIUnit::WMDir:
        default: color = watchColorDir; break;
      }
      DrawCursor
      (
        camera, vehicle, agent->GetWatchDirection(),
        -1,
        texture, 0,0, mouseScrW, mouseScrH,
        color, td, cursorShadow
      );
      texture=GLOB_SCENE->Preloaded(CursorLocked);
      DrawCursor
      (
        camera, vehicle, agent->GetWatchHeadDirection(),
        -1,
        texture, 0,0, mouseScrW, mouseScrH,
        color, td, cursorShadow
      );


      // draw target (red)
    }
#endif
  }

  if (!GWorld->HasOptions() && GWorld->UserDialog() == NULL)
  {
    if (ModeIsStrategy(_mode) || canSeeTD || Glob.config.IsEnabled(DTWeaponCursor))
      DrawMouseCursor(camera, agent, td);
    else if(!Glob.config.IsEnabled(DTWeaponCursor))
      CheckLockDelay(agent);
  }

#ifndef _XBOX
  if (_dragging)
  {
    Matrix4Val camInvTransform = camera.GetInvTransform();
    Point3 posStart = camInvTransform.Rotate(_startSelection);
    Point3 posEnd = camInvTransform.Rotate(_endSelection);
    if (posStart.Z() > 0 && posEnd.Z() > 0)
    {
      float invZ = 1.0 / posStart.Z();
      float x1 = 0.5 * (1.0 + posStart.X() * invZ * camera.InvLeft());
      float y1 = 0.5 * (1.0 - posStart.Y() * invZ * camera.InvTop());
      invZ = 1.0 / posEnd.Z();
      float x2 = 0.5 * (1.0 + posEnd.X() * invZ * camera.InvLeft());
      float y2 = 0.5 * (1.0 - posEnd.Y() * invZ * camera.InvTop());
      const int w = GEngine->WidthBB();
      const int h = GEngine->HeightBB();
      saturate(x1, 0, 1); x1 *= w;
      saturate(y1, 0, 1); y1 *= h;
      saturate(x2, 0, 1); x2 *= w;
      saturate(y2, 0, 1); y2 *= h;
      GEngine->DrawLine(Line2DPixel(x1, y1, x2, y1), dragColor, dragColor);
      GEngine->DrawLine(Line2DPixel(x2, y1, x2, y2), dragColor, dragColor);
      GEngine->DrawLine(Line2DPixel(x2, y2, x1, y2), dragColor, dragColor);
      GEngine->DrawLine(Line2DPixel(x1, y2, x1, y1), dragColor, dragColor);
    }
  }
#endif
}

void InGameUI::DrawHints()
{
  if (!(_xboxStyle && (_menu.Size() > 0 || _units)))
  {
    DrawHint();
    DrawTaskHint();
  }
}

void InGameUI::DrawCursorWp(const Camera &camera, bool compass, float alphaCursor, float alphaText, float age)
{
  PackedColor missionColor = _iconMoveColor;

  PackedColor colorCursor = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alphaCursor));
  PackedColor colorText = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alphaText));
  if (colorCursor.A8() == 0 && colorText.A8() == 0) return;

  // draw unit related information
  AIBrain *agent = GWorld->FocusOn();
  Assert(agent);
  AIUnit *unit = agent->GetUnit();
  if (!unit) return;
  AISubgroup *subgroup = unit->GetSubgroup();
  Assert(subgroup);
  Assert(subgroup->Leader());
  AIGroup *group = subgroup->GetGroup();
  Assert(group);
  EntityAIFull *vehicle = unit->GetVehicle();
  Assert(vehicle);
  AIBrain *commanderUnit = vehicle->CommanderUnit();
  if (!commanderUnit) commanderUnit = unit;

  int &index = group->GetCurrent()->_fsm->Var(0);
  int &waiting = group->GetCurrent()->_fsm->Var(4);
  if (index < group->NWaypoints())
  {
    const WaypointInfo &wInfo = group->GetWaypoint(index);
    if(wInfo.visible)
    {
      int type = wInfo.type;
      Vector3 pos = group->GetWaypointPosition(index);

      RString description;
      if (waiting) description = LocalizeString(IDS_SYNC_WAITING);
      else
      {
        if (wInfo.description.GetLength() > 0) description = Localize(wInfo.description);
        else description = LocalizeString(IDS_AC_MOVE + type - ACMOVE);
      }

      Vector3 dir = pos - camera.Position();
      Vector3 dirU = pos - commanderUnit->AimingPosition(commanderUnit->GetRenderVisualState());
      CursorTexts texts;
      const float longTextLimit = 0.75;
      if (alphaText > longTextLimit)
      {
        texts.Add(description, 0.5f);
        char buffer[256];
        sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
        texts.Add(buffer, 1.2f);
        colorText = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * longTextLimit));
      }
      else
      {
        char buffer[256];
        sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
        texts.Add(buffer, 0.5f);
        colorText = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * longTextLimit));
      }
      DrawCursor
        (
        camera, vehicle, dir,
        0,
        _iconMission, 0, -0.5f*actH, actW, actH,
        colorCursor, compass, cursorShadow,true, colorText, texts,true
        );
      DrawCursorDetail
        (
        camera, vehicle, dir,
        0,
        _iconMove,  0, -0.5f*actH, actW, actH,
        colorCursor, cursorShadow, true, true
        );
    }
  }
}

void InGameUI::DrawCursors(const Camera &camera, bool enabled, bool compass)
{
  // draw unit related information
  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return;

  AIUnit *unit = agent->GetUnit();
  AISubgroup *subgroup = unit ? unit->GetSubgroup() : NULL;
  AIGroup *group = subgroup ? subgroup->GetGroup() : NULL;

  EntityAIFull *vehicle = agent->GetVehicle();
  if (!vehicle) return;

  AIBrain *commanderUnit = vehicle->CommanderUnit();
  if (!commanderUnit) commanderUnit = agent;

  bool showPermanently = enabled || Glob.config.IsEnabled(DTHUDPerm);
  bool show = enabled || Glob.config.IsEnabled(DTHUD);
  if (showPermanently || show)
  {
    // leader && position in formation
    float age = Glob.uiTime - _lastFormTime;
    bool ok = age < formDimEndTime;
    if (ok || showPermanently)
    {
      // - leader
      float alphaText = 1.0f;
      if (!ok) alphaText = 0;
      else if (age > formDimStartTime)
        alphaText = (formDimEndTime - age) / (formDimEndTime - formDimStartTime);
      float alphaCursor = floatMin(showPermanently ? 1.0 : alphaText,0.8);

      if (group && group->Leader() && group->Leader() != commanderUnit)
      {
        PackedColor colorCursor = PackedColorRGB(selectColor, toIntFloor(leaderColor.A8() * alphaCursor));
        PackedColor colorText = PackedColorRGB(selectColor, toIntFloor(leaderColor.A8() * alphaText));
        if (colorCursor.A8() > 0 || colorText.A8() > 0)
        {
          Vector3 dir = group->Leader()->AimingPosition(group->Leader()->GetRenderVisualState()) - camera.Position();
          Vector3 dirU = group->Leader()->AimingPosition(group->Leader()->GetRenderVisualState()) - commanderUnit->Position(commanderUnit->GetRenderVisualState());
          CursorTexts texts;
          if (colorText.A8() > 0)
          {
            char buffer[256];
            sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
            texts.Add(buffer, 1);
          }

          DrawCursorDetail
          (
            camera, vehicle, dir,
            group->Leader()->VisibleSize(group->Leader()->GetRenderVisualState()),
            _iconLeader, 0,0, actW, actH,
            colorCursor, cursorShadow,true 
          );

          colorCursor = PackedColorRGB(selectColor, toIntFloor(selectColor.A8() * alphaCursor));

          if(!Glob.config.IsEnabled(DTHUDGroupInfo))
            DrawCursor(
            camera, vehicle, dir,
            group->Leader()->VisibleSize(group->Leader()->GetRenderVisualState()),
            _iconSelect, 0,0, actW, actH,
            colorCursor, false, cursorShadow,true,
            colorText, texts);
        }
      }
      // - formation position
      if (subgroup && subgroup->Commander() && commanderUnit != subgroup->Commander() && commanderUnit->GetUnit())
      {
        if (!agent->IsInCargo())
        {
          PackedColor colorCursor = PackedColorRGB(_iconInFormationColor, toIntFloor(_iconInFormationColor.A8() * alphaCursor));
          if (colorCursor.A8() > 0)
          {
            Vector3 dir = commanderUnit->GetUnit()->GetFormationAbsolute() - camera.Position();
            /*
            // draw only if far enough
            dirU = commanderUnit->GetFormationAbsolute() - commanderUnit->AimingPosition();
            if (dirU.SquareSizeXZ() > Square(5))
            */
            {
              DrawCursor
              (
                camera, vehicle, dir,
                0,              // size - default
                _iconMission, 0, -0.5f*actH,
                actW, actH,
                colorCursor,
                false,           // do not draw in tactical display
                cursorShadow,             // draw shadow
                true,
                PackedWhite,  CursorTexts(), true
              );
              DrawCursorDetail             
              (
                camera, vehicle, dir,
                0,              // size - default
                _iconInFormation,0, -0.5f*actH,
                actW, actH,
                colorCursor,
                cursorShadow,           // do not draw in tactical display
                true,             // draw shadow
                true
              );
            }
          }
        }
      }
    }

    // - assigned medic
    if (unit && unit->IsFreeSoldier() && unit->GetVehicle() && unit->GetVehicle()->AssignedAttendant())
    {
      EntityAIFull *attendant =  unit->GetVehicle()->AssignedAttendant();
      if(
        attendant->CommanderUnit()&& attendant->CommanderUnit()->GetUnit()
        && attendant->CommanderUnit()->GetUnit()->GetSubgroup())
      {
        EntityAIFull *vehicle =  unit->GetVehicle();
        //const EntityAIType *type =  vehicle->GetType();
        AISubgroup *subgroup = attendant->CommanderUnit()->GetUnit()->GetSubgroup();
        Command *cmd = subgroup->GetCommand();
        if(cmd && cmd->_message && (cmd->_message == Command::HealSoldier || cmd->_message == Command::FirstAid))
        {
          float alphaText = 0;
          age = floatMin(Glob.uiTime - _lastMedicTime,0.5*(float(Glob.uiTime - _lastFormTime)));

          if (age > medicDimEndTime) alphaText = 0;
          else alphaText = (medicDimEndTime - age) / (medicDimEndTime);
          float alphaCursor = floatMin(showPermanently ? 1.0 : alphaText,0.8);

          PackedColor colorCursor = PackedColorRGB(PackedColor(65,100,220,255), toIntFloor(leaderColor.A8() * alphaCursor));

          PackedColor colorText = PackedColorRGB(selectColor, toIntFloor(leaderColor.A8() * alphaText));
          if (colorCursor.A8() > 0)
          {
            Vector3 dir = attendant->AimingPosition(attendant->RenderVisualState()) - camera.Position();
            CursorTexts texts;
            if (colorText.A8() > 0)
            {
              char buffer[256];
              Vector3 dirU = attendant->AimingPosition(attendant->RenderVisualState()) - commanderUnit->Position(commanderUnit->GetRenderVisualState());
              sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
              texts.Add(buffer, 1);
            }

            DrawCursorDetail
              (
              camera, vehicle, dir,
              group->Leader()->VisibleSize(group->Leader()->GetRenderVisualState()),
              _iconUnitUnconscious, 0,0, actW, actH,
              colorCursor, cursorShadow,true 
              );

            colorCursor = PackedColorRGB(selectColor, toIntFloor(selectColor.A8() * alphaCursor));
            if(!Glob.config.IsEnabled(DTHUDGroupInfo))
              DrawCursor(
              camera, vehicle, dir,
              group->Leader()->VisibleSize(group->Leader()->GetRenderVisualState()),
              _iconSelect, 0,0, actW, actH,
              colorCursor, false, cursorShadow, true,
              colorText, texts);
          }
        }
      }
    }
    else _lastMedicTime = Glob.uiTime;

    // - target
    age = Glob.uiTime - _lastTargetTime;
    ok = age < targetDimEndTime;
    if (ok || showPermanently)
    {
      float alphaText = 1.0f;
      if (!ok) alphaText = 0;
      else if (age > targetDimStartTime)
        alphaText = (targetDimEndTime - age) / (targetDimEndTime - targetDimStartTime);
      float alphaCursor = showPermanently ? 1.0 : alphaText;

      Target *target = NULL;
      if (agent->IsUnit()) target = agent->GetTargetAssigned();
      else if (agent->IsGunner())
      {
        Transport *veh = agent->GetVehicleIn();
        if (veh)
        {
          TurretContext context;
          if (veh->FindTurret(agent->GetPerson(), context))
          {
            target = context._weapons->_fire._fireTarget;
          }
        }
      }
      if (target)
      {
        PackedColor colorCursor = PackedColorRGB(_iconAttackColor, toIntFloor(_iconAttackColor.A8() * alphaCursor));
        PackedColor colorText = PackedColorRGB(_iconAttackColor, toIntFloor(_iconAttackColor.A8() * alphaText));
        if (colorCursor.A8() > 0 || colorText.A8() > 0)
        {
          Vector3 tgtAimPos = target->LandAimingPosition(unit);
          Vector3 dir = tgtAimPos - camera.Position();
          Vector3 dirU = tgtAimPos - agent->AimingPosition(agent->GetRenderVisualState());

          CursorTexts texts;
          if (colorText.A8() > 0)
          {
            char buffer[256];
            texts.Add((const char *)LocalizeString(IDS_MENU_TARGET), 1);
            sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
            texts.Add(buffer, 1.7);
          }
          DrawCursor
          (
            camera, vehicle, dir,
            0, // size
            _iconAttack, 0,0, actW, actH,
            colorCursor, compass, cursorShadow, true, colorText, texts
          );
        }
      }
    }

    // - command
    const Command *cmd = subgroup ? subgroup->GetCommand() : NULL;
    if (cmd)
    {
      if (cmd->_id != _lastCmdId)
      {
        _lastCmdId = cmd->_id;
        _lastCmdTime = Glob.uiTime;
      }
    }

    age = Glob.uiTime - _lastCmdTime;
    ok = age < cmdDimEndTime;
    if (ok || showPermanently)
    {
      float alphaText = 1.0f;
      if (!ok) alphaText = 0;
      else if (age > cmdDimStartTime)
        alphaText = (cmdDimEndTime - age) / (cmdDimEndTime - cmdDimStartTime);
      float alphaCursor = showPermanently ? 1.0 : alphaText;

      if (cmd)
      {
        if (subgroup->Commander() && commanderUnit != subgroup->Commander())
        {
          if
          (
            cmd->_message == Command::Attack ||
            cmd->_message == Command::AttackAndFire ||
            cmd->_message == Command::Fire ||
            cmd->_message == Command::FireAtPosition
          )
          {
            Assert(commanderUnit->GetUnit());
            DrawCommand(*cmd, commanderUnit->GetUnit(), camera, compass, alphaCursor, alphaText, age);
          }
        }
        else
        {
          if (cmd->_context != Command::CtxMission)
          {
            Assert(commanderUnit->GetUnit());
            DrawCommand(*cmd, commanderUnit->GetUnit(), camera, compass, alphaCursor, alphaText, age);
          }
        }
      }
    }

    // - group units (only for recruit)
    if (group && Glob.config.IsEnabled(DTHUDGroupInfo))
    {
      AIGroup *playerGroup = unit->GetGroup();
      EntityAIFull *entity = unit->GetVehicle();
      const EntityAIType *type = entity->GetType();

      if (playerGroup && unit->GetUnit())
      { //
        for (int i=0; i<playerGroup->NUnits(); i++)
        {
          AIUnit *u = playerGroup->GetUnit(i);
          if (u && u != unit) 
          {
            float alphaCursor = 0.4f;
            PackedColor colorCursor = PackedColorRGB(selectColor, toIntFloor(selectColor.A8() * alphaCursor));

            Vector3 dir = u->AimingPosition(u->GetRenderVisualState()) - camera.Position();

            DrawCursor(
              camera, vehicle, dir,
              u->VisibleSize(u->GetRenderVisualState()),
              _iconSelect, 0,0, actW, actH,
              colorCursor, false, cursorShadow,true);

            if(type->IsAttendant())
            {
              EntityAIFull *vehicle = u->GetVehicle();
              AIUnit::ResourceState state = group->GetHealthStateReported(u);
              if(vehicle && state == AIUnit::RSCritical) 
              {
                //test state: alpha depends on distance - at this moment 200m is maximum  
                float distanceAlpha = floatMin(1-unit->Position(unit->GetRenderVisualState()).Distance(u->Position(u->GetRenderVisualState()))/500.0f,0.8f);  
                PackedColor missionColor = PackedColorRGB(_unitIconColorWounded, toIntFloor(selectColor.A8() * distanceAlpha));     
                saturate(distanceAlpha,0,0.85);
                if(u->GetLifeState() == LifeStateUnconscious)  
                {
                  PackedColor colorCursor1 = PackedColorRGB(_unitIconColorWounded, toIntFloor(selectColor.A8() * distanceAlpha));
                  PackedColor colorCursor2 = PackedColorRGB(_unitIconColorWounded2, toIntFloor(selectColor.A8() * distanceAlpha));
                  PackedColor missionColor = BlinkColor(colorCursor1, colorCursor2, blinkingSpeed * age);
                }
                DrawCursorDetail(
                  camera, vehicle, dir,
                  u->VisibleSize(u->GetRenderVisualState()),
                  _iconUnitUnconscious,  0,0, actW, actH,
                  missionColor,  cursorShadow,true);
              }
            }

            if(type->IsEngineer())
            {
              EntityAIFull *vehicle = u->GetVehicleIn();
              if(!vehicle) vehicle = u->VehicleAssigned();
              if(vehicle && !vehicle->IsDamageDestroyed())
              {
                if(vehicle->GetMaxHitCont() > 0.7f) 
                {
                  //test state: alpha depends on distance - at this moment 200m is maximum  
                  float distanceAlpha = floatMin(1-unit->Position(unit->GetRenderVisualState()).Distance(u->Position(u->GetRenderVisualState()))/500.0f,0.8f);  
                  PackedColor missionColor = PackedColorRGB(_iconRepairAtColor, toIntFloor(selectColor.A8() * distanceAlpha));     
                  saturate(distanceAlpha,0,0.85);

                  DrawCursorDetail(
                    camera, vehicle, dir,
                    u->VisibleSize(u->GetRenderVisualState()),
                    _iconRepairAt,  0,0, actW, actH,
                    missionColor,  cursorShadow,true);
                }
              }
            }
          }
        }
      }
    }

    // - selected units
    if (group)
    {
      OLinkPermNOArray(AIUnit) list;
      ListSelectingUnits(list);
      AutoArray<RString> vehicleUnitsText;
      RefArray<Transport> vehicleUnits;
      AutoArray<PackedColor> vehicleUnitsColorText;
      AutoArray<PackedColor> vehicleUnitsColorCursor;

      for (int i=0; i<list.Size(); i++)
      {
        AIUnit *u = list[i];
        Assert(u);
        if (u == unit && GWorld->GetCameraType() < CamExternal) continue;

        float age = Glob.uiTime - WhenSelectedUnit(u);
        bool ok = age < meDimEndTime;
        if (!ok && !showPermanently) continue;

        float alphaText = 1.0f;
        if (!ok) alphaText = 0;
        else if (age > meDimStartTime)
          alphaText = (meDimEndTime - age) / (meDimEndTime - meDimStartTime);
        float alphaCursor = showPermanently ? 1.0 : alphaText;

        PackedColor colorText = PackedColorRGB(selectColor, toIntFloor(selectColor.A8() * alphaText));
        PackedColor colorCursor = PackedColorRGB(selectColor, toIntFloor(selectColor.A8() * alphaCursor));
        
        //if unit is in vehicle - put info aside, to draw all at once
        Transport *t =  u->GetVehicleIn();
        if(t)
        {
          int i;
          for(i=0;i<vehicleUnits.Size();i++) 
          {
            if (t == vehicleUnits[i]) 
            {//put all unit's IDs in one string
              vehicleUnitsText[i] = vehicleUnitsText[i] + "," + FormatNumber(u->ID());
              if(vehicleUnitsColorText[i].A8()<colorText.A8()) vehicleUnitsColorText[i] = colorText;
              if(vehicleUnitsColorCursor[i].A8()<colorCursor.A8()) vehicleUnitsColorCursor[i] = colorCursor;
              break;
            }
          }
          if(i == vehicleUnits.Size())
          {//nobody from this vehicle in not yet to be drawn
            vehicleUnits.Add(t); vehicleUnitsText.Add(FormatNumber(u->ID()));
            vehicleUnitsColorText.Add(colorText);vehicleUnitsColorCursor.Add(colorCursor);
          }
          continue;
        }

        if (colorCursor.A8() > 0 && colorText.A8() > 0)
        {
          Vector3 dir = u->AimingPosition(u->GetRenderVisualState()) - camera.Position();
          CursorTexts texts;
          if (colorText.A8() > 0)
          {
            char buffer[256];
            sprintf(buffer, "%d", u->ID());
            texts.Add(buffer, -0.7);
            float dist = u->AimingPosition(u->GetRenderVisualState()).Distance(unit->AimingPosition(unit->GetRenderVisualState()));
            if (dist >= 50.0f)
            {
              sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dist);
              texts.Add(buffer, 1);
            };
          }

          DrawCursor(
            camera, vehicle, dir,
            u->VisibleSize(u->GetRenderVisualState()),
            _iconSelect, 0,0, actW, actH,
            colorCursor, false, cursorShadow,true,
            colorText, texts);
        }
      }
  
      //draw all units in one vehicle at once, so we can put IDs in one string
      for(int i=0;i<vehicleUnits.Size();i++) 
      {
        if (vehicleUnitsColorCursor[i].A8() > 0 &&  vehicleUnitsColorText[i].A8() > 0)
        {
          Vector3 dir = vehicleUnits[i]->AimingPosition(vehicleUnits[i]->RenderVisualState()) - camera.Position();
          CursorTexts texts;
          texts.Add(vehicleUnitsText[i],-0.7);
          char buffer[256];
          float dist = vehicleUnits[i]->AimingPosition(vehicleUnits[i]->RenderVisualState()).Distance(unit->AimingPosition(unit->GetRenderVisualState()));
          if (dist >= 50.0f)
          {
            sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dist);
            texts.Add(buffer, 1);
          };

          DrawCursor(
            camera, vehicle, dir,
            1,
            _iconSelect, 0,0, actW, actH,
            vehicleUnitsColorCursor[i], false, cursorShadow,true,
            vehicleUnitsColorText[i], texts);
        }
      }
    }

    // - draw all group units in agony (always)
    if (group)
    {
      AIGroup *playerGroup = unit->GetGroup();
      float distanceAlpha;
      if (playerGroup && unit->GetUnit())
      { // select all in group units which are in agony
        for (int i=0; i<playerGroup->NUnits(); i++)
        {
          AIUnit *u = playerGroup->GetUnit(i);
          if (u && u != unit) 
            if(u->GetLifeState() == LifeStateUnconscious)  
            {
              Assert(u);
              //test state: alpha depends on distance - at this moment 200m is maximum  
              distanceAlpha = floatMin(1-unit->Position(unit->GetRenderVisualState()).Distance(u->Position(u->GetRenderVisualState()))/500.0f,0.8f);       
              distanceAlpha = distanceAlpha<0?0:distanceAlpha; // to avoid negative alpha
              saturate(distanceAlpha,0,0.85);

              float ageA = 1.0;//Glob.uiTime - _lastFormTime;

              float alphaText = 1.0f;
              if (ageA>formDimEndTime) alphaText = 0;
              else if (ageA > formDimStartTime)
                alphaText = (formDimEndTime - ageA) / (formDimEndTime - formDimStartTime);
              float alphaCursor = showPermanently ? 1.0 : alphaText;

              PackedColor colorText = PackedColorRGB(unitUnconsciousColor, toIntFloor(selectColor.A8() * distanceAlpha * alphaText));
              PackedColor colorCursor1 = PackedColorRGB(_unitIconColorWounded, toIntFloor(selectColor.A8() * distanceAlpha * alphaCursor));
              PackedColor colorCursor2 = PackedColorRGB(_unitIconColorWounded2, toIntFloor(selectColor.A8() * distanceAlpha * alphaCursor));
              PackedColor missionColor = BlinkColor(colorCursor1, colorCursor2, blinkingSpeed * age);
              

              if (missionColor.A8() > 0)
              {
                Vector3 dir = u->AimingPosition(u->GetRenderVisualState()) - camera.Position();
                CursorTexts texts;

                DrawCursorDetail(
                  camera, vehicle, dir,
                  u->VisibleSize(u->GetRenderVisualState()),
                  _iconUnitUnconscious,  0,0, actW, actH,
                  missionColor,  cursorShadow,true);

                missionColor = PackedColorRGB(selectColor, toIntFloor(selectColor.A8() * distanceAlpha * distanceAlpha * alphaCursor));

                if(!Glob.config.IsEnabled(DTHUDGroupInfo))
                  DrawCursor(
                  camera, vehicle, dir,
                  u->VisibleSize(u->GetRenderVisualState()),
                  _iconSelect, 0,0, actW, actH,
                  missionColor, false, cursorShadow,true,
                  colorText, texts);

              }
            }
        }
      }
    }

    // - ourself (no text, fade cursor)
    age = Glob.uiTime - _lastMeTime;
    ok = age < meDimEndTime;
    if (ok)
    {
      float alphaCursor = 1.0f;
      if (age > meDimStartTime)
        alphaCursor = (meDimEndTime - age) / (meDimEndTime - meDimStartTime);

      if (GWorld->GetCameraType() == CamGroup)
      {
        PackedColor colorCursor = PackedColorRGB(meColor, toIntFloor(meColor.A8() * alphaCursor));
        if (colorCursor.A8() > 0)
        {
          Vector3 dir = commanderUnit->AimingPosition(commanderUnit->GetRenderVisualState()) - camera.Position();
          DrawCursor
          (
            camera, vehicle, dir,
            commanderUnit->VisibleSize(commanderUnit->GetRenderVisualState()),
            _iconMe, 0,0, actW, actH,
            colorCursor, false, cursorShadow
          );
        }
      }
    }

    age = Glob.uiTime - _lastFollowMeTime;
    ok = age < cmdDimEndTime;
    if (ok || showPermanently)
    {
      float alphaText = 1.0f;
      if (!ok) alphaText = 0;
      else if (age > cmdDimStartTime)
        alphaText = (cmdDimEndTime - age) / (cmdDimEndTime - cmdDimStartTime);
      float alphaCursor = showPermanently ? 1.0 : alphaText;

      PackedColor missionColor = BlinkColor(missionColor1, missionColor2, blinkingSpeed * age);
      PackedColor colorText = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alphaText));
      PackedColor colorCursor = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alphaCursor));
      if (colorCursor.A8() > 0 && colorText.A8() > 0)
      {
        if (subgroup && subgroup->Commander() && commanderUnit != subgroup->Commander())
        {
          if (group && subgroup->Leader() && subgroup->Leader() != group->Leader())
          {
            // no subgroup leader
            Vector3 dir = subgroup->Leader()->AimingPosition(subgroup->Leader()->GetRenderVisualState()) - camera.Position();
            Vector3 dirU = subgroup->Leader()->AimingPosition(subgroup->Leader()->GetRenderVisualState()) - commanderUnit->AimingPosition(commanderUnit->GetRenderVisualState());

            CursorTexts texts;
            if (colorText.A8() > 0)
            {
              char buffer[256];
              // draw FOLLOW ME
              texts.Add((const char *)LocalizeString(IDS_JOIN), 0.5f);
              sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
              texts.Add(buffer, 1.2f);
            }
            DrawCursor
            (
              camera, vehicle, dir,
              subgroup->Leader()->VisibleSize(subgroup->Leader()->GetRenderVisualState()),
              _iconMission, 0, -0.5f*actH, actW, actH,
              colorCursor, compass, cursorShadow,true, colorText, texts,true
            );
          }
        }
      }
    }
  }

  showPermanently = enabled || Glob.config.IsEnabled(DTHUDWpPerm);
  show = enabled || Glob.config.IsEnabled(DTHUDWp);

  if (showPermanently || show)
  {
    // waypoint position
    float age = Glob.uiTime - _lastWpTime;
    bool ok = age < cmdDimEndTime;

    if ((ok || showPermanently) && group && group->GetCurrent())
    {
      float alphaText = 1.0f;
      if (!ok) alphaText = 0;
      else if (age > cmdDimStartTime)
        alphaText = (cmdDimEndTime - age) / (cmdDimEndTime - cmdDimStartTime);
      float alphaCursor = showPermanently ? 1.0 : alphaText;
      if (commanderUnit->IsGroupLeader() || subgroup && subgroup == group->MainSubgroup()) 
      {
        DrawCursorWp(camera, compass , alphaCursor, alphaText, age);
      }
    }
  }

#if _ENABLE_IDENTITIES
  // tasks
  // TODO: add fading of tasks, show current task only
  if (showPermanently || show)
  {
# if _ENABLE_INDEPENDENT_AGENTS
    AITeamMember *member = agent->GetTeamMember();
    if (member)
    {
      for (int i=0; i<member->NTasks(); i++)
      {
        AITask *task = member->GetTask(i);
        Vector3 pos;
        if (task && task->IsRunning() && task->GetDestination(pos))
        {
          RString description;
          if (!task->GetDescriptionHUD(description)) description = task->GetName();

          float alphaText = 1.0f;
          float alphaCursor = 1.0;
          float age = Glob.uiTime.toFloat();

          PackedColor missionColor = BlinkColor(missionColor1, missionColor2, blinkingSpeed * age);
          PackedColor colorCursor = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alphaCursor));
          PackedColor colorText = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alphaText));

          Vector3 dir = pos - camera.Position();
          Vector3 dirU = pos - commanderUnit->AimingPosition(commanderUnit->GetRenderVisualState());
          CursorTexts texts;
          const float longTextLimit = 0.75;
          if (alphaText > longTextLimit)
          {
            char buffer[256];
            texts.Add(cc_cast(description), 0.5f);
            sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
            texts.Add(buffer, 1.2f);
          }
          else
          {
            char buffer[256];
            sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
            texts.Add(buffer, 0.5f);
            colorText = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * longTextLimit));
          }

          DrawCursor(
            camera, vehicle, dir,
            0,
            _iconMission, 0, -0.5f*actH,  actW,  actH,
            colorCursor, compass, cursorShadow,true, colorText, texts ,true);
        }
      }
    }
# endif // _ENABLE_INDEPENDENT_AGENTS
    if (agent->GetPerson())
    {
      const Identity &identity = agent->GetPerson()->GetIdentity();

      // custom mark
      {
        Vector3 pos;
        if (identity.GetCustomMark(pos))
        {
          // TODO: fade out?
          float alphaText = 1.0f;
          float alphaCursor = 1.0;
          const float longTextLimit = 0.75;
          saturateMax(alphaText, longTextLimit);

          Vector3 dir = pos - camera.Position();
          Vector3 dirU = pos - commanderUnit->AimingPosition(commanderUnit->GetRenderVisualState());

          CursorTexts texts;
          char buffer[256];
          sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
          texts.Add(buffer, 0.5f);

          PackedColor colorCursor = PackedColorRGB(customMarkColor, toIntFloor(customMarkColor.A8() * alphaCursor));
          PackedColor colorText = PackedColorRGB(customMarkColor, toIntFloor(customMarkColor.A8() * alphaText));
          DrawCursor(camera, vehicle, dir, 0,
            _iconCustomMark, 0, -0.5f*actH, actW,  actH,
            colorCursor, compass, cursorShadow,true, colorText, texts,true);
        }
      }

      if (showPermanently || show)
      {
        const Task *task = identity.GetCurrentTask();

        Vector3 pos;
        if (task && !task->IsCompleted() && task->GetDestination(pos))
        {
          // waypoint position
          float age = Glob.uiTime - _lastWpTime;
          bool ok = age < cmdDimEndTime;

          if (ok || showPermanently)
          {

            float alphaText = 1.0f;
            if (age > cmdDimStartTime)
              alphaText = (cmdDimEndTime - age) / (cmdDimEndTime - cmdDimStartTime);
            float alphaCursor = showPermanently ? 1.0 : alphaText;
            // simple tasks - show only the current task

            RString description;
            if (!task->GetDescriptionHUD(description)) description = task->GetName();

            // TODO: fade out?
            float ageBl = Glob.uiTime.toFloat();

            PackedColor missionColor = BlinkColor(missionColor1, missionColor2, blinkingSpeed * ageBl);
            PackedColor colorCursor = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alphaCursor));
            PackedColor colorText = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * alphaText));

            Vector3 dir = pos - camera.Position();
            Vector3 dirU = pos - commanderUnit->AimingPosition(commanderUnit->GetRenderVisualState());
            CursorTexts texts;
            const float longTextLimit = 0.75;
            if (alphaText > longTextLimit)
            {
              char buffer[256];
              texts.Add(cc_cast(description), 0.5f);
              sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
              texts.Add(buffer, 1.2f);
            }
            else
            {
              char buffer[256];
              sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
              texts.Add(buffer, 0.5f);
              colorText = PackedColorRGB(missionColor, toIntFloor(missionColor.A8() * longTextLimit));
            }
            DrawCursor(
              camera, vehicle, dir,
              0,
              _iconMission, 0, -0.5f*actH, actW,  actH,
              colorCursor, compass, cursorShadow,true, colorText, texts, true);
          }
        }
      }
    }
  }

  if (_drawGroupIcons3D)
  {//draw HC group Icons, group's waypoints and selectors

    //clear array to remember where which group was drawn (for easy iconClick event) 
    _groupMarkersPosition.Clear();  
    //draw group selection icon
    // draw unit related information
    AIBrain *agent = GWorld->FocusOn();
    if (!agent) return;
    AIUnit *player = agent->GetUnit();
    if(!player) return;

    for (int i=0; i<TSideUnknown; i++)
    {// unit new target system is ready - draw all group
      AICenter *center = GWorld->GetCenter((TargetSide)i);
      if (center)
      {
        for (int j=0; j<center->NGroups(); j++)
        {
          AIGroup *group = center->GetGroup(j);
          if (group && group->GetGroupIcon().IsVisible())
          {//if current group icons are on
            CursorTexts texts;
            AIUnit *unit = group->Leader();
            if(unit)
            {
              bool selected = false;
              float dist = unit->Position(unit->GetRenderVisualState()).Distance(player->Position(player->GetRenderVisualState())); 
              float aplhaDist = 0.0f;
              //draw selected groups  on great distances
              if(group->HCCommander()==player)
              {
                AIHCGroup *gr = player->GetHCGroup(group);
                if(gr && gr->IsSelected())
                {
                  selected = true;
                  aplhaDist = 0.9;

                  Vector3 dirU = player->Position(player->GetRenderVisualState()) - unit->AimingPosition(unit->GetRenderVisualState());

                  char buffer[256];
                  sprintf(buffer, "%d", gr->ID()+1);
                  texts.Add(buffer, -0.8);

                  sprintf(buffer, LocalizeString(IDS_UI_POSITION_DISTANCE), dirU.Size());
                  texts.Add(buffer, 1.15);
                }
              }
              if(!selected)
              {
                aplhaDist = (_maxHCDistanceAlphaEnd - dist)/(_maxHCDistanceAlphaEnd - _maxHCDistanceAlphaStart);
                saturate(aplhaDist,0.0f,_HC3DGroupAlpha);
                if(dist>_maxHCDistanceAlphaEnd)
                  if(group->HCCommander()==player) aplhaDist = 0.3f;
                  else continue;
              }
              PackedColor color = PackedColorRGB(group->GetGroupIcon().IconColor(), toIntFloor(group->GetGroupIcon().IconColor().A8() * aplhaDist));

              Vector3 dir = Vector3(unit->AimingPosition(unit->GetRenderVisualState()).X(),unit->AimingPosition(unit->GetRenderVisualState()).Y()+3,unit->AimingPosition(unit->GetRenderVisualState()).Z()) - camera.Position();             
              AutoArray<GIcon> markers = group->GetGroupIcons();

              if(markers.Size()>0)
              {//for the first texture side arrows are allowed and location is saved
                float invSize = dir.SquareSize()>0 ? dir.InvSize() : 1e10f;
                float coef =  invSize * camera.InvLeft();
                saturate(coef, actMin, actMax);
                float radius = Square(0.5f*coef * group->GetGroupIcon().Scale()*actW)*1.5f;

                //remember where on screen is which group - for click events
                Point2DFloat point = WorldToScreen(camera,dir,1);
                if(point.x>0 && point.x<1 && point.y>0 && point.y<1)
                  _groupMarkersPosition.Add(UnitScreenPosition(group,point.x,point.y,-1,radius));

                texts.Add(" " + group->GetGroupIcon()._text, 0.25f, true);

                DrawCursor(
                  camera, vehicle, dir,
                  1,
                  markers[0]._groupMaker, markers[0]._offsetX*group->GetGroupIcon().Scale()*actW,markers[0]._offsetY*group->GetGroupIcon().Scale()*actH,
                  group->GetGroupIcon().Scale()*actW,
                  group->GetGroupIcon().Scale()*actH,
                  color, false, markers[0]._shadow,true,
                  color, texts);
                texts.Clear();
              }

              for(int i=1; i< markers.Size(); i++)
              {//draw rest of textures used for icon, side arrow is disabled
                DrawCursor(
                  camera, vehicle, dir,
                  1,
                  markers[i]._groupMaker, markers[i]._offsetX*group->GetGroupIcon().Scale()*actW,markers[i]._offsetY*group->GetGroupIcon().Scale()*actH,
                  group->GetGroupIcon().Scale()*actW,
                  group->GetGroupIcon().Scale()*actH,
                  color, false, markers[i]._shadow,false,
                  color);
              }

              //draw waypoint for selected unit's
              if(selected)
              {
                int wSize = group->NWaypoints();
                int current = group->GetCurrentWaypoint();
                if(current<0 || wSize<=0) continue;

                for(int k=current; k< wSize; k++) 
                {
                  texts.Clear();
                  if(!group->GetWaypointVisible(k)) continue;

                  dir = Vector3(group->GetWaypointPosition(k)) - camera.Position();   
                  dist = group->GetWaypointPosition(k).Distance(unit->Position(unit->GetRenderVisualState()));

                  aplhaDist = (_maxHCDistanceAlphaEnd - dist)/(_maxHCDistanceAlphaEnd - _maxHCDistanceAlphaStart);
                  saturate(aplhaDist,0.2f, _HC3DGroupAlpha);
                  PackedColor colorW = PackedColorRGB(color, toIntFloor(color.A8() * aplhaDist));

                  texts.Add(FormatNumber(k-current+1), -0.7f, false);
                  texts.Add(FormatNumber(dist), 1.0f, false);

                  DrawCursor(
                    camera, vehicle, dir,
                    1,
                    _hcWaypoint, 0,0,
                    group->GetGroupIcon().Scale()*actW,
                    group->GetGroupIcon().Scale()*actH,
                    colorW, false, _hcGroupSelectableShadow ,false,
                    colorW, texts);
                }
              }
            }
          }
        }
      }
    }

    if(_drawHCCommand)
    {//draw selected/selectable group icon - only for groups under your command
      AutoArray<AIHCGroup> &groups = player->GetHCGroups();

      for(int i=0; i<groups.Size();i++)
      {
        if(!groups[i].GetGroup()) continue;
        Ref<Texture> picture;
        float size=1;
        //select texture
        AIUnit *unit = groups[i].GetGroup()->Leader();
        if(!unit) continue;

        CursorTexts texts;
        if(groups[i].IsSelected()) 
        {
          size = _hcGroupSelectedSize;
          picture = _hcGroupSelected;
        }
        else 
        {
          size = _hcGroupSelectableSize;
          picture = _hcGroupSelectable;
        }

        float dist = unit->Position(unit->GetRenderVisualState()).Distance(player->Position(player->GetRenderVisualState())); 
        float aplhaDist = (_maxHCDistanceAlphaEnd - dist)/(_maxHCDistanceAlphaEnd - _maxHCDistanceAlphaStart);
        saturate(aplhaDist,0.25f,_HC3DGroupAlpha);
        if(groups[i].IsSelected()) aplhaDist = 0.9f;
        //color is in textures
        PackedColor color = PackedColorRGB(PackedWhite, toIntFloor(groups[i].GetGroup()->GetGroupIcon()._markerColor.A8() * aplhaDist));
 
        if(unit)
        {
          Vector3 dir = Vector3(unit->AimingPosition(unit->GetRenderVisualState()).X(),unit->AimingPosition(unit->GetRenderVisualState()).Y()+3,unit->AimingPosition(unit->GetRenderVisualState()).Z()) - camera.Position();
          DrawCursor(
            camera, vehicle, dir,
            1,
            picture, 0,0,
            groups[i].GetGroup()->GetGroupIcon().Scale()*actW * size,
            groups[i].GetGroup()->GetGroupIcon().Scale()*actH * size,
            color, false, _hcGroupSelectableShadow, false,
            color);
        }
      }
    }
  }
#endif // _ENABLE_IDENTITIES 
}
