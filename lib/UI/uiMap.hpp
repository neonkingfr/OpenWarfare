#ifdef _MSC_VER
#pragma once
#endif

#ifndef _UI_MAP_HPP
#define _UI_MAP_HPP

#include "uiControls.hpp"
#include "../global.hpp"
#include "../engine.hpp"
#include "../world.hpp"

#include "../arcadeTemplate.hpp"
#include "../AI/ai.hpp"

#include "../interpol.hpp"

#include <El/Network/netXboxConfig.hpp>
#include "../Network/liveStats.hpp"

#include "../progress.hpp"

#if _VBS2 // integration of real time editor with main map
#include "missionEditorVBS.hpp"
#endif


#if defined _XBOX && _XBOX_VER >= 200
#include "../saveGame.hpp"
#endif

/*!
\file
Interface file for particular displays (maps, briefing, debriefing, mission editor).
*/

//! Structure describes coordinates in map control
struct MapCoord
{
  //! x coordinate
  int x;
  //! y coordinate
  int y;
  //! constructor
  MapCoord(int xx, int yy) {x = xx; y = yy;}
  //! constructor without initialization
  MapCoord() {} // no init
};

//! Type of editable object in map
enum SignType
{
  signNone,
  signUnit,
  /// vehicle as listed in the center target list
  signVehicle,
  /// vehicle as listed in the group target list
  signVehicleGroup,
  /// static object
  signStatic,
  signCheckpoint,
  signSeekAndDestroy,
  signArcadeUnit,
  signArcadeWaypoint,
  signArcadeSensor,
  signArcadeMarker,
#if _ENABLE_EDITOR2
  signEditorObject,
#endif
#if _ENABLE_IDENTITIES
  signTask,
  signCustomMark
#endif
};

//! Description of editable object in map
struct SignInfo
{
  //! type of object
  enum SignType _type;
  //! concrete unit
  OLinkPerm<AIBrain> _unit;
  //! concrete entity
  TargetId _id;
  //! position
  Point3 _pos;
  //! index of AI Group
  int _indexGroup;
  //! index of object (inside group or total)
  int _index;
#if _ENABLE_EDITOR2
  RString _name;
#endif

  SignInfo();
};
TypeContainsOLink(SignInfo)

//! Briefing notepad control
class Notepad : public ControlObjectContainer, public LODShapeLoadHandler
{
protected:
  //! paper (active area) selection
  AnimationSection _paper;
  //! name of selection
  RString _paperSelection;
  //@{
  //! paper texture
  Ref<Texture> _paper1;
  Ref<Texture> _paper2;
  Ref<Texture> _paper3;
  Ref<Texture> _paper4;
  Ref<Texture> _paper5;
  Ref<Texture> _paper6;
  Ref<Texture> _paper7;
  //@}

  //! briefing HTML control
  InitPtr<CHTMLContainer> _briefing;
public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  */
  Notepad(ControlsContainer *parent, int idc, ParamEntryPar cls);
  ~Notepad();
  void SetPosition(Vector3Par pos);
  void Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2);
  void Deanimate(int level, bool setEngineStuff);
  //! attach briefing to notepad
  void SetBriefing(CHTMLContainer *briefing) {_briefing = briefing;}

  //@{ LODShapeLoadHandler implementation
  virtual void LODShapeLoaded(LODShape *shape);
  virtual void LODShapeUnloaded(LODShape *shape);
  virtual void ShapeLoaded(LODShape *shape, int level);
  virtual void ShapeUnloaded(LODShape *shape, int level);
  //@}
};


class ControlObjectAnimated : public ControlObjectWithZoom, public LODShapeLoadHandler, public AnimatedType, public Animated
{
  typedef ControlObjectWithZoom base;

protected:
  /// source values for _animations
  AnimationSourceHolder _animSources;
  /// config parameters
  ConstParamClassPtr _par;

public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  */
  ControlObjectAnimated(ControlsContainer *parent, int idc, ParamEntryPar cls, bool ui);
  ~ControlObjectAnimated();

  //@{ LODShapeLoadHandler implementation
  virtual void LODShapeLoaded(LODShape *shape);
  virtual void LODShapeUnloaded(LODShape *shape);
  virtual void ShapeLoaded(LODShape *shape, int level);
  virtual void ShapeUnloaded(LODShape *shape, int level);
  //@}

  virtual bool PrepareShapeDrawMatrices(Matrix4Array &matrices, const ObjectVisualState &vs, LODShape *shape, int level);
  virtual void Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2);
  virtual void Deanimate(int level, bool setEngineStuff);
  virtual int PassNum(int lod) {return 4; /* UI Pass*/ }

  /// encapsulate access to animations
  const AnimationHolder &GetAnimations() const
  {
    // prefer shape based animations
    if (_shape && _shape->GetAnimations())
    {
      return *_shape->GetAnimations();
    }
    return AnimationHolder::Empty();
  }
};


//! Compass control
/* acts both as an object and as a type at the same time */
class Compass : public ControlObjectAnimated, public IControlObject
{
  typedef ControlObjectAnimated base;
protected:
  //! arrow selection
  AnimationSection _arrowHide;
  mutable float _pointerAngle;
  mutable UITime _lastPointerUpdateTime;

  mutable float _groupAngle;
  mutable float _groupAngleWanted;
  mutable UITime _lastUpdateTime;
#if _VBS2
  bool _trackNorth;
#endif
  Matrix3 _orient;

public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  \param ui is the object for UI, or for the 3D scene?
  */
  Compass(ControlsContainer *parent, int idc, ParamEntryPar cls, bool ui);
  ~Compass();

  AnimationSource *CreateAnimationSource(const AnimationType *type, RStringB source);

  float GetPointerPos(const ObjectVisualState &vs) const;
  float GetArrowPos(const ObjectVisualState &vs) const;
  float GetCoverPos(const ObjectVisualState &vs) const {return 1;}

#if _VBS2
  void TrackNorth(bool trackNorth) {_trackNorth = trackNorth;}
#endif

  //@{ LODShapeLoadHandler implementation
  virtual void LODShapeLoaded(LODShape *shape);
  virtual void LODShapeUnloaded(LODShape *shape);
  virtual void ShapeLoaded(LODShape *shape, int level);
  virtual void ShapeUnloaded(LODShape *shape, int level);
  //@}

  //@{ IControlObject implementation
  virtual void Draw() {OnDraw(1.0f);}
  //@}

  void Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2);
  void Deanimate(int level, bool setEngineStuff);
  //! calculates center of rotation
  Vector3 Center() const;
};

//! Watch control
class Watch : public ControlObjectAnimated, public IControlObject
{
  typedef ControlObjectAnimated base;
protected:
  //@{
  //! date selection
  AnimationUV _date1;
  AnimationUV _date2;
  AnimationUV _day;
  //@}

public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  */
  Watch(ControlsContainer *parent, int idc, ParamEntryPar cls, bool ui);
  ~Watch();

  //@{ LODShapeLoadHandler implementation
  virtual void LODShapeLoaded(LODShape *shape);
  virtual void LODShapeUnloaded(LODShape *shape);
  virtual void ShapeLoaded(LODShape *shape, int level);
  virtual void ShapeUnloaded(LODShape *shape, int level);
  //@}

  //@{ IControlObject implementation
  virtual void Draw() {OnDraw(1.0f);}
  //@}

  void Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2);
  void Deanimate(int level, bool setEngineStuff);
};


ControlObjectWithZoom *CreateCompass();
ControlObjectWithZoom *CreateWatch();

//! Describes map object type
struct MapTypeInfo
{
  //! picture
  Ref<Texture> icon;
  //! color
  PackedColor color;
  //! relative size
  float size;
  //! minimal size
  float coefMin;
  //! maximal size
  float coefMax;
  //! when to draw
  float importance;
  //! when to draw
  int shadow;

  //! reads description from resource
  void Load(ParamEntryPar cls);
};

//! Describes map object type
struct MapTypeInfoTask : MapTypeInfo
{
  //! picture
  Ref<Texture> iconCreated;
  Ref<Texture> iconCanceled;
  Ref<Texture> iconDone;
  Ref<Texture> iconFailed;

  PackedColor colorCreated;
  PackedColor colorCanceled;
  PackedColor colorDone;
  PackedColor colorFailed;
  //! reads description from resource
  virtual void Load(ParamEntryPar cls);
};

struct HCMapInfo
{
  //selected groups icon
  Ref<Texture> _hcGroupSelected;
  // selectable groups icon
  Ref<Texture> _hcGroupSelectable;
  //groups waypoint icon
  Ref<Texture> _hcWaypoint;

  int _hcGroupSelectedSize;
  int _hcGroupSelectableSize;
  int _hcGroupWaypointSize;
  int _hcGroupWaypointShadow;
  int _hcGroupSelectedShadow;
};

// forward declare
class MapObjectList;

//! Describes single animation state of map
struct MapAnimationPhase
{
  //! time of state
  UITime time;
  //! scale of map
  float scale;
  //! map center position in world coordinates
  Vector3 pos;
};
TypeIsSimple(MapAnimationPhase);

/// Legend properties
struct LegendInfo
{
  /// size and position
  float _x;
  float _y;
  float _w;
  float _h;

  /// text font
  Ref<Font> _font;
  /// text size
  float _size;

  /// background color
  PackedColor _colorBackground;
  /// text color
  PackedColor _color;

  void Load(ParamEntryPar cls);
};

class MapObject;
class MapObjectListRectUsed;

#include "../engine.hpp"

//! types of cursor used in map
enum CursorType
{
  CursorArrow,
  CursorTrack,
  CursorMove,
  CursorScroll,
  CursorWatch,
  CursorRotate,
#if _VBS3
  CursorRaise,
  CursorDrag,
#else
  // Mission editor cursors (3D mode)
  CursorTrack3D,
  CursorMove3D,
  CursorRotate3D,
  CursorRaise3D,
#endif
  //dummy cursor, so i can determine size of enum
  NCursorType
};

//! Generic map control
class CStaticMap : public CStatic
{
public:
#ifdef _XBOX
  static const int AllocLines = 100;
#else
  static const int AllocLines = 500;
#endif
  typedef AutoArray<Line2DPixelInfo, MemAllocLocal<Line2DPixelInfo,AllocLines> > LineArray;
  //@{
  //! position (top left point) of "window" on whole map
  float _mapX;
  float _mapY;
  //@}

  //! mouse position in world coordinates
  Vector3 _mouseWorld;
  //! default map center position in world coordinates
  /*!
  Defined in config for every island.
  Used for map center when no player is displayed.
  */
  Vector3 _defaultCenter;

  //! map object thats may becomes selected
  struct SignInfo _infoClickCandidate;
  //! selected map object
  struct SignInfo _infoClick;
  //! map object map is over
  struct SignInfo _infoMove;

  //!hc group cursor is over
  OLinkPermNO(AIGroup) _hcTarget;

  //@{
  //! current map scale
  float _scaleX;
  float _scaleY;
  float _invScaleX;
  float _invScaleY;
  //@}
  //! minimal map scale
  float _scaleMin;
  //! maximal map scale
  float _scaleMax;
  //! default (initial) map scale
  float _scaleDefault;

  //  bool _moveOnEdges;

  //! position of map control center
  /*!
  May not be in real center of control, for example if notepad hides some area of map.
  */
  Point2DFloat _center;
  Point2DFloat _halfSize;

  //@{
  //! precalculates for increasing drawing speed
  float _wScreen;
  float _hScreen;
  Rect2DPixel _clipRect;
  int _mipmapLevel;
  //@}

  //@{ coefficients which determine rendering density / threshold
  float _ptsPerSquareSea;   // seas
  float _ptsPerSquareTxt;   // textures
  float _ptsPerSquareCLn;   // count-lines
  float _ptsPerSquareExp;   // exposure
  float _ptsPerSquareCost;  // cost
  //@}

  //@{ coefficients which determine when rendering of given type is done
  float _ptsPerSquareFor;   // forests
  float _ptsPerSquareForEdge;   // forest edges
  float _ptsPerSquareRoad;  // roads
  float _ptsPerSquareObj;    // other objects
  //@}

  //! locked map region to avoid repeated reloading
  SRef<MapObjectListRectUsed> _mapUsed;

  //@{
  //! animation implementation
  UITime _animationStart;
  UITime _animationLast;
  AutoArray<MapAnimationPhase> _animation;
  SRefArray<Matrix4> _animMatrices;
  SRefArray<float> _animTimes;
  SRef<M4Function> _interpolator;
  //@}

  //@{
  //! moving implementation
  UITime _moveStart;
  UITime _moveLast;
  unsigned _moveKey;
  //@}

  //! map is moving
  bool _moving;
  //! some object is dragging
  bool _dragging;
  //! some area is selecting
  bool _selecting;
  //group mouse was over last frame
  OLinkPermNO(AIGroup) _lastGroupOver;

#if _ENABLE_CHEATS
  //@{
  //! debug show mode
  bool _show;
  bool _showCost;
  //@}
#endif
  //! show IDs of objects
  bool _showIds;
  //! show Grayscale / Textures
  bool _showScale;
  //! map texture alpha
  float _showScaleAlpha;

  //! player's punctuation picture
  Ref<Texture> _iconPlayer;
  //! selected unit's punctuation picture
  Ref<Texture> _iconSelect;
  //! camera effect picture
  Ref<Texture> _iconCamera;
  //! trigger picture
  Ref<Texture> _iconSensor;
  //! color of friendly units
  PackedColor _colorFriendly;
  //! color of enemy units
  PackedColor _colorEnemy;
  //! color of civilians
  PackedColor _colorCivilian;
  //! color of neutral units
  PackedColor _colorNeutral;
  //! color of unknown units
  PackedColor _colorUnknown;
  //! color of player unit's punctuation
  PackedColor _colorMe;
  //! secondary color of player unit's punctuation
  PackedColor _colorMe2;
  //! color of playable unit's punctuation
  PackedColor _colorPlayable;
  //! color of selected unit's punctuation
  PackedColor _colorSelect;
  //! trigger color
  PackedColor _colorSensor;
  //! color of line for dragged object (in group or synchronization mode)
  PackedColor _colorDragging;

  int _shadow;

  //! color of area with enemy exposure
  PackedColor _colorExposureEnemy;
  //! color of area with unknown exposure
  PackedColor _colorExposureUnknown;
  //! color of tracks
  PackedColor _colorTracks;
  //! color of roads
  PackedColor _colorRoads;
  //! color of main roads
  PackedColor _colorMainRoads;
  //! color of tracks
  PackedColor _colorTracksFill;
  //! color of roads
  PackedColor _colorRoadsFill;
  //! color of main roads
  PackedColor _colorMainRoadsFill;
  //! color of grid texts
  PackedColor _colorGrid;
  //! color of grid lines
  PackedColor _colorGridMap;
  //! not used
  PackedColor _colorCheckpoints;
  //! color of camera effect picture
  PackedColor _colorCamera;
  //! not used
  PackedColor _colorMissions;
  //! color of waypoint lines
  PackedColor _colorActiveMission;

  //! color of strategic path for camera unit
  PackedColor _colorPath;
  //! color of completed part of strategic path for camera unit
  PackedColor _colorPathDone;
  //! strategic path texture
  Ref<Texture> _texturePath;
  //! completed part of strategic path texture
  Ref<Texture> _texturePathDone;
  //! strategic path texture size
  float _sizePath;
  //! completed part of strategic path texture size
  float _sizePathDone;

  //! color of label for object mouse is over
  PackedColor _colorInfoMove;
  //! color of group lines
  PackedColor _colorGroups;
  //! color of group lines for active group
  PackedColor _colorActiveGroup;
  //! color of synchronization lines
  PackedColor _colorSync;
  //! background color of label
  PackedColor _colorLabelBackground;
  //! color of height levels
  PackedColor _colorLevels;

  //! width of railway lines
  //float _widthRailWay;

  //! Displays the grid Ids over the lines (VBS)
  bool _gridNumbersOverLines; 

  //! leader units size
  float _sizeLeader;
  //! other units size
  float _sizeUnit;
  //! Player indication size
  float _sizePlayer;

  //! coeficient of cursor line color alpha
  float _cursorLineWidth;

  float _blinkingSpeedMe;

  //@{
  //! mouse position in world coordinates in special cases
  Vector3 _special;
  Vector3 _lastPos;
  //@}
  //  DrawCoord _dragOffset;

  ////////////////////////////////////
  //                                //
  // NEW PROPERTIES - from resource //
  //                                //
  ////////////////////////////////////

  /// color outside the map area
  PackedColor _colorOutside;

  //@{
  //! color of sea
  PackedColor _colorSeaPacked;
  Color _colorSea;
  //@}
  //! color of forest
  PackedColor _colorForest;
  //! color of rocky area
  PackedColor _colorRocks;

  //! color of countlines
  PackedColor _colorCountlines;
  //! color of countlines in sea
  PackedColor _colorCountlinesWater;
  //! color of thicker countlines
  PackedColor _colorMainCountlines;
  //! color of thicker countlines in sea
  PackedColor _colorMainCountlinesWater;
  //! color of borders of forest
  PackedColor _colorForestBorder;
  //! color of borders of rocky area
  PackedColor _colorRocksBorder;
  /// color of power lines
  PackedColor _colorPowerLines;
  /// color of railway
  PackedColor _colorRailWay;
  //! color of geographic names (towns, villages, mounts etc.)
  PackedColor _colorNames;
  //satelite map fading
  float _maxSatelliteAlpha;
  float _alphaFadeStartScale;
  float _alphaFadeEndScale;

  //! color modifier for inactive objects
  PackedColor _colorInactive;

  //! font used for labels
  Ref<Font> _fontLabel;
  //! font size
  float _sizeLabel;
  //! font used for grid texts
  Ref<Font> _fontGrid;
  //! font size
  float _sizeGrid;
  //! font used for id of selected units
  Ref<Font> _fontUnits;
  //! font size
  float _sizeUnits;
  //! font used for geographic names
  Ref<Font> _fontNames;
  //! font size
  float _sizeNames;
  //! font used for enemy info
  Ref<Font> _fontInfo;
  //! font size
  float _sizeInfo;
  //! font used for natural ground levels
  Ref<Font> _fontLevels;
  //! font size
  float _sizeLevels;

  //! countour interval 
  bool _showCountourInterval;
  bool _miniMapMode; //when true, player icon is drawn even for Disabled DTMap

#ifdef _XBOX
  //@{ analog stick map control response curve
  float _mapScrollXDZone;
  float _mapScrollYDZone;
  SRef<IActionResponseCurve> _mapScrollX;
  SRef<IActionResponseCurve> _mapScrollY;
  //@}
#endif

  //@{
  //! map object type properties
  MAP_TYPES_ICONS_1(MAP_TYPE_INFO_DECL)
    MAP_TYPES_ICONS_2(MAP_TYPE_INFO_DECL)

    MapTypeInfo _infoWaypoint;
  MapTypeInfo _infoWaypointCompleted;
  //@}

  /// legend properties
  LegendInfo _legend;

  RString _cursorMapOverride[NCursorType];
  RString _cursorName;

#if _VBS3 // markers moved to CStaticMap
  bool _onlyDrawMarkers;
  bool _drawAttachedMarkers;
  bool _drawConditionalMarkers;
# if _VBS3_UDEFCHART
  //! List of cached user textures
  RefArray<Texture> _userTextures;
  //! List of texture names whose file has not been found
  AutoArray<RStringB> _userTexturesNotFound;
# endif
#endif //_VBS3

public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  \param scaleMin minimal map scale
  \param scaleMax maximal map scale
  \param scaleDefault default map scale
  */
  CStaticMap
    (
    ControlsContainer *parent, int idc, ParamEntryPar cls,
    float scaleMin, float scaleMax, float scaleDefault
    );
  ~CStaticMap();

#if _VBS3
  Rect2DAbs MapArea();
#endif

  void OnRButtonDown(float x, float y);
  void OnRButtonUp(float x, float y);
  void OnMouseMove(float x, float y, bool active = true);
  void OnMouseHold(float x, float y, bool active = true);
  virtual void OnMouseZChanged(float dz);
  virtual bool OnKeyDown(int dikCode);
  virtual bool OnKeyUp(int dikCode);

  //! processing of cheats (do nothing in SuperRelease)
  virtual void ProcessCheats();

  virtual void OnDraw(UIViewport *vp, float alpha);
  virtual void OnHide();

  virtual void DrawExt( float alpha ) {}

  //! initialization
  void Reset();

  //! sets actual scale
  void SetScale(float scale);
  //! returns actual scale
  float GetScale() const {return _scaleX;}

  //! sets visible area of map (calculates _center)
  void SetVisibleRect(float x, float y, float w, float h);  // in screen coordinates
  //! center map on given position
  void Center(Vector3Val pt, bool soft);
  //! center map (by player unit)
  virtual void Center(bool soft = false) {}
  //! returns position of map center in world coordinates
  Vector3 GetCenter();

  //! clear (interrupt) current map animation
  void ClearAnimation();
  //! adds animation phase to animation
  void AddAnimationPhase(float dt, float scale, Vector3Par pos);
  //! finish creation of animation
  void CreateInterpolator();
  //! Check if some animation is running
  bool HasAnimation() const {return _animation.Size() > 0;}

  //! translates world relative coordinates to screen relative coordinates
  Point2DFloat WorldSizeToScreen(Vector3Val pt) const;
  //! translates world coordinates to screen coordinates
  Point2DFloat WorldToScreen(Vector3Val pt) const;
  //! translates screen coordinates to world coordinates
  Point3 ScreenToWorld(Point2DFloat ptMap) const;

  bool IsMoving() const {return _moving;}

  //@{
  //! scroll (move) map window by <dif>
  void ScrollX(float dif);
  void ScrollY(float dif);
  //@}

  //! Is showing of objects' IDs enabled
  bool IsShowingIds() const {return _showIds;}
  //! Show / hide objects' IDs
  void ShowIds(bool show = true) {_showIds = show;}

  //! Is showing greyscale ore textures
  bool IsShowingScale() const {return _showScale;}
  //! Show grayscale / textures
  void ShowScale(bool show = true) {_showScale = show;}

  void SetScales(float scaleMin, float scaleMax, float scaleDefault);

  virtual void SetFont(Font *font) {_fontNames = font;}
  virtual void SetFontHeight(float h) {_sizeNames = h;}
#if _VBS3 // getFontHeight
  virtual float GetFontHeight() {return _sizeNames;}
#endif

#if _VBS2 // conditional markers
  bool DrawSignAutosize
    (
    Texture *texture, PackedColor color,
    Vector3Val pos, float w, float h, float azimut, RString text = RString(), bool shadow = false,
    Font *font = NULL, float textSize = -1 , bool autosize = true
    );
#endif

  //! draw map sign
  /*!
  \param texture texture of picture
  \param color color of picture
  \param pos position in world coordinates
  \param w width of sign (in pixels for resolution 640 x 480)
  \param h height of sign (in pixels for resolution 640 x 480)
  \param azimut rotation of sign
  \param text additional text (drawn on the right side of sign)
  \param font text font
  \param textSize base text size (can be modified in some zoom levels)
  */
  bool DrawSign(Texture *texture, PackedColor color,
    Vector3Val pos, float w, float h, float azimut, RString text = RString(),
    Font *font = NULL, float textSize = -1, int shadow = 0);

  //! draw line in map
  /*!
  \param pos1 start position in world coordinates
  \param pos2 end position in world coordinates
  \param color color of line
  */
  void DrawLine(Vector3Val pos1, Vector3Val pos2, PackedColor color);

  //! draw arrow in map
  /*!
  \param pos1 start position in world coordinates
  \param pos2 end position in world coordinates
  \param color color of arrow
  */
  void DrawArrow(Vector3Val pos1, Vector3Val pos2, PackedColor color);

  //! draw ellipse in map
  /*!
  \param position center of ellipse in world coordinates
  \param a axis a of ellipse (in meters)
  \param b axis b of ellipse (in meters)
  \param angle rotation
  \param color border color
  \param dashFill size of a solid part when rendering dashed
  \param dashGap size of a gap when rendering dashed
  */
  void DrawEllipse(
    Vector3Val position, float a, float b, float angle, PackedColor color,
    float dashSolid=FLT_MAX, float dashGap=0
    );
  //! draw rectangle in map
  /*!
  \param position center of ellipse in world coordinates
  \param a side a of rectangle (in meters)
  \param b side b of rectangle (in meters)
  \param angle rotation
  \param color border color
  */
  void DrawRectangle(Vector3Val position, float a, float b, float angle, PackedColor color);
  //! draw filled ellipse in map
  /*!
  \param position center of ellipse in world coordinates
  \param a axis a of ellipse (in meters)
  \param b axis b of ellipse (in meters)
  \param angle rotation
  \param color fill color
  \param texture fill texture
  */
  void FillEllipse(Vector3Val position, float a, float b, float angle, PackedColor color, Texture *texture);
  //! draw filled rectangle in map
  /*!
  \param position center of ellipse in world coordinates
  \param a side a of rectangle (in meters)
  \param b side b of rectangle (in meters)
  \param angle rotation
  \param color fill color
  \param texture fill texture
  */
  void FillRectangle(Vector3Val position, float a, float b, float angle, PackedColor color, Texture *texture);

  //! draw mount dimension
  void DrawMount(Vector3Par pos);
  //! draw (town or village) name
  void DrawName(Vector3Par pos, RString text, Font *font, float textSize, PackedColor color, int shadow);

  //!set override texture for given cursor texture type
  void SetOverrideCursor(RString cursorName, RString overrideTexture);
  //!get override texture for given texture type
  RString OverrideCursor(RString cursorName);
  //!get override texture for given texture type
  RString OverrideCursor(CursorType cursorType);


  // implementation
protected:
  //VBS:
  //! Flag to determine the user map is selected
  bool UserMapDrawing() const;
  //! Flag to determine the user map is being drawn
  bool UserMapIsBeingDrawn() const;
  //! Flag to determine objects should be drawn on the user map
  bool UserMapDrawObjects() const;

  //! precalculates constans to optimize some calculations
  void Precalculate();
  //! check if x map coordinate is valid (and saturate it)
  void SaturateX(float &x);
  //! check if y map coordinate is valid (and saturate it)
  void SaturateY(float &y);

  //! draw map sign
  /*!
  \param texture texture of picture
  \param color color of picture
  \param posMap position in map coordinates
  \param w width of sign (in pixels for resolution 640 x 480)
  \param h height of sign (in pixels for resolution 640 x 480)
  \param azimut rotation of sign
  \param text additional text (drawn on the right side of sign)
  \param font text font
  \param textSize base text size (can be modified in some zoom levels)
  */
  bool DrawSign(Texture *texture, PackedColor color,
    DrawCoord posMap, float w, float h, float azimut, RString text = RString(),
    Font *font = NULL, float textSize = -1, bool textSizeConstant = false, int shadow = 0);
  //! draw line in map
  /*!
  \param posMap1 start position in map coordinates
  \param posMap2 end position in map coordinates
  \param color color of line
  */
  void DrawLine(DrawCoord posMap1, DrawCoord posMap2, PackedColor color);
  //! draw map sign
  /*!
  \param info description of map object type
  \param pos position in world coordinates
  */
  bool DrawSign(MapTypeInfo &info, Vector3Val pos);
  //! draw label
  /*!
  \param info editable map object info
  \param color text color
  */
  virtual void DrawLabel(struct SignInfo &info, PackedColor color, int shadow) {}
  //! draw all airports on the map
  void DrawAirports();
  //! draw single runway/taxiway rectangle
  void DrawRunway(const Vector3 *runway);
  /// draw autopilot taxi guides
  void DrawTaxiGuide(const AutoArray<Vector3> &guide);
  //! draw map background (all informations from Landscape - sea, countlines, forests, roads, objects etc.)
  void DrawBackground();
  /// if we know where is the map moving, we can preload
  void PreloadData(Vector3Par center);
  //! draw map legend
  void DrawLegend();
  //! draw town or village name
  void DrawName(ParamEntryPar cls);
  //! draw real landscape texture in given square
  /*!
  \param x x position on screen
  \param y y position on screen
  \param xStep width of square on screen
  \param yStep height of square on screen
  \param i x position in landscape (in LandGrid coordinates)
  \param j z position in landscape (in LandGrid coordinates)
  \param iStep width in landscape (in LandGrid coordinates)
  \param jStep height in landscape (in LandGrid coordinates)

  */
  void DrawField
    (
    float x, float y, float xStep, float yStep,
    int i, int j, int iStep, int jStep
    );
  //! draw height scale in given square
  /*!
  \param x x position on screen
  \param y y position on screen
  \param xStep width of square on screen
  \param yStep height of square on screen
  \param i x position in landscape (in LandGrid coordinates)
  \param j z position in landscape (in LandGrid coordinates)
  \param iStep width in landscape (in LandGrid coordinates)
  \param jStep height in landscape (in LandGrid coordinates)

  */
  void DrawScale
    (
    float x, float y, float xStep, float yStep,
    int i, int j, int iStep, int jStep
    );
  //! draw sea in given square
  /*!
  \param x x position on screen
  \param y y position on screen
  \param xStep width of square on screen
  \param yStep height of square on screen
  \param i x position in landscape (in LandGrid coordinates)
  \param j z position in landscape (in LandGrid coordinates)
  \param iStep width in landscape (in LandGrid coordinates)
  \param jStep height in landscape (in LandGrid coordinates)

  */
  void DrawSea
    (
    float x, float y, float xStep, float yStep,
    int i, int j, int iStep, int jStep
    );
  //! draw abstract isometric lines
  /*!
  \param pt array of 4 bounding points of square
  \param height array of 4 values in bounding points
  \param step step of value for which lines are drawn
  \param stepMain step of value for which thicker lines are drawn
  \param minLevel minimal value for which line is drawn
  \param maxLevel maximal value for which line is drawn
  \param color lines color
  \param colorMain thicker lines color
  */

  void DrawLines
    (
    LineArray &lines, DrawCoord *pt, float *height,
    float step, float stepMain, float minLevel, float maxLevel, PackedColor color, PackedColor colorMain
    );
  //! draw countlines in given square
  /*!
  \param x x position on screen
  \param y y position on screen
  \param xStep width of square on screen
  \param yStep height of square on screen
  \param i x position in landscape (in LandGrid coordinates)
  \param j z position in landscape (in LandGrid coordinates)
  \param iStep width in landscape (in LandGrid coordinates)
  \param jStep height in landscape (in LandGrid coordinates)
  \param step step of height for which lines are drawn
  \param stepMain step of height for which thicker lines are drawn
  \param maxLevel maximal absolute value for which line is drawn
  */
  void DrawCountlines
    (
    LineArray &lines,
    float x, float y, float xStep, float yStep,
    int i, int j, int iStep, int jStep,
    float step, float stepMain, float maxLevel
    );
  //{@ helper functors DrawForests etc.
  struct DrawForestContext;
  struct DrawForestsNewFunc;
  struct DrawObjectsContext;
  struct DrawRoadContext;
  //@}

  //! draw forests in given square - new version (using iso-arbors)
  /*!
  Drawing is performed by single landscape squares.
  \param i x position in landscape (in LandGrid coordinates)
  \param j z position in landscape (in LandGrid coordinates)
  */
  void DrawForestsNew(
    const MapObjectList &list, int i, int j, const DrawForestContext &ctx, bool drawLines, bool rocks
    );
  //! draw objects in given square
  /*!
  Drawing is performed by single landscape squares.
  \param i x position in landscape (in LandGrid coordinates)
  \param j z position in landscape (in LandGrid coordinates)
  */
  void DrawObjects(const MapObjectList &list, int i, int j);
  //! draw forest borders in given square
  /*!
  Drawing is performed by single landscape squares.
  \param i x position in landscape (in LandGrid coordinates)
  \param j z position in landscape (in LandGrid coordinates)
  */
  void DrawForestBorders(const MapObjectList &list, int i, int j, const DrawForestContext &ctx);

  //! draw roads in given square
  /*!
  Drawing is performed by single landscape squares.
  \param i x position in landscape (in LandGrid coordinates)
  \param j z position in landscape (in LandGrid coordinates)
  */
  void DrawRoads(
    LineArray &lines, const MapObjectList &list, int i, int j
    );
  //! draw map grid
  void DrawGrid();

  //! find editable map object on given screen coordinates (<x>, <y>)
  virtual SignInfo FindSign(float x, float y) {return SignInfo();}

  //! draw label implementation
  /*!
  \param pos world position
  \param text label
  \param color text color
  */
  void DrawLabel(Vector3Par pos, RString text, PackedColor color, int shadow);

  /// draw building shape
  void DrawBuilding(MapObject *obj, PackedColor color);

  //! calculate color for inactive item from base color
  PackedColor InactiveColor(PackedColor color);
};

//! weather state (used for selection of icon for weather in mission editor)
enum WeatherState
{
  WSUndefined,
  WSClear,
  WSCloudly,
  WSOvercast,
  WSRainy,
  WSStormy
};

//! translates weather state into icon type
WeatherState GetWeatherState(float overcast);

#ifndef _XBOX

//! base class for 2D notebook display (currently used only for mission editor)
class DisplayNotebook : public Display
{
protected:
  /*
  Ref<Texture> _glassA;
  Ref<Texture> _glassB;
  Ref<Texture> _glassC;
  Ref<Texture> _glassD;
  Ref<Texture> _glassE;
  */

  //! displayed weather
  WeatherState _lastWeather;

  RString _textureClear;
  RString _textureCloudly;
  RString _textureOvercast;
  RString _textureRainy;
  RString _textureStormy;

public:
  //! constructor
  /*!
  \param parent parent display
  */
  DisplayNotebook(ControlsContainer *parent, ParamEntryVal cls);

  //! returns current weather state
  virtual WeatherState GetWeather() = 0;
  //! returns current game time
  virtual Clock GetTime() = 0;
  //! returns current player position
  virtual bool GetPosition(Point3 &pos) = 0;

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnDraw(EntityAI *vehicle, float alpha);
};

#endif


#if _ENABLE_EDITOR

//! Abstract mission editor display
class DisplayMapEditor : public DisplayNotebook
{
protected:
  //! current cursor type
  CursorType _cursor;
public:
  //! constructor
  /*!
  \param parent parent display
  */
  DisplayMapEditor(ControlsContainer *parent, ParamEntryVal cls);

  //! returns map control
  virtual CStaticMap *GetMap() = 0;
  void OnSimulate(EntityAI *vehicle);
  virtual bool OnKeyDown(int dikCode);
  virtual bool OnKeyUp(int dikCode);
  void OnDraw(EntityAI *vehicle, float alpha);
};

#endif // #if _ENABLE_EDITOR

// Main map (ingame)

//! Info about command passed from map
struct CommandInfo
{
  //! group
  OLinkPerm<AIGroup> grp;
  //! command id (in group)
  int id;
  //! command state
  CommandState state;
  //! subgroup executing command
  OLinkPerm<AISubgroup> subgrp;
  //! command target position
  Vector3 position;

  //! serialization
  LSError Serialize(ParamArchive &ar);
};
TypeIsGeneric(CommandInfo);

class DisplayMap;

/*!
\patch 1.01 Date 06/07/2001 by Ondra
- Fixed: friendly units info in map (veteran mode)
- was MapSubgroupInfo before
- information was stored about subgroups, not units
*/

//! Info about reported unit
struct MapUnitInfo
{
  //! reported unit
  OLinkPerm<AIUnit> unit;
  //! when report was done
  float time; // report status time
  //@{
  //! reported position
  int x;
  int z;
  //@}

  //! serialization
  LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(MapUnitInfo);

//! Info about reported enemy
struct MapEnemyInfo
{
  //! enemy id
  TargetId id;
  //! enemy type
  Ref<const VehicleType> type;
  //! when report was done
  float time; // report status time
  //@{
  //! reported position
  int x;
  int z;
  //@}

  //! serialization
  LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(MapEnemyInfo);

//! Encapsulates all infos in main map (units, enemies, commands)
struct MainMapInfo : public SerializeClass
{
  //! subgroups
  AutoArray<MapUnitInfo> unitInfo;
  //! enemies
  AutoArray<MapEnemyInfo> enemyInfo;
  //! commands
  AutoArray<CommandInfo> commands;

  LSError Serialize(ParamArchive &ar);
  //! remove all infos
  void Clear();
};

//! Main (ingame) map (and briefing) control
class CStaticMapMain : public CStaticMap
{
protected:
#if _ENABLE_CHEATS
  //@{
  //! debug show mode
  bool _showUnits;
  bool _showTargets;
  bool _showSensors;
  bool _showVariables;
  //@}
#endif

  //! focused marker
  int _activeMarker;
  /// continuously center the map around the camera position
  bool _autoCenter;

  //! map object type properties for command sign
  MapTypeInfo _infoCommand;
  //! default icon for the task (if not overridden by the task itself)
  MapTypeInfoTask _infoTask;
  //! icon for the custom mark
  MapTypeInfo _infoCustomMark;

  //! lines color for active marker enforcement
  PackedColor _colorActiveMarker;
  //! square size for active marker enforcement
  float _sizeActiveMarker;

  //! picture for current position in veteran mode (in tanks and air units)
  Ref<Texture> _iconPosition;

  //HC info
  HCMapInfo _hcMapInfo;

  //mouse down has been handled by some UI, therefore click has to be ignored
  bool _mouseClickHandled;

#if _VBS2 // conditional markers
  UITime _lastEvalMarkerConditions; 
#endif

  MainMapInfo _mapInfo;
public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  */
  CStaticMapMain(ControlsContainer *parent, int idc, ParamEntryPar cls);

#if _VBS2
  CStaticMapMain
    (
    ControlsContainer *parent, int idc, ParamEntryPar cls,
    float scaleMin, float scaleMax, float scaleDefault
    );
#endif

  void OnLButtonDown(float x, float y);
  void OnLButtonUp(float x, float y);
  void OnLButtonClick(float x, float y);
  void OnLButtonDblClick(float x, float y);
  void OnMouseHold(float x, float y, bool active = true);
  virtual void OnMouseZChanged(float dz);
  virtual bool OnKeyDown(int dikCode);
  virtual void ProcessCheats();
  void DrawExt(float alpha);
  void OnRButtonDown(float x, float y);
  void OnMouseMove(float x, float y, bool active = true );
  void OnGroupIconOver(float x, float y);

  //! returns parent display pointer (casted)
  DisplayMap *GetParent();
  void Center(bool soft = false);
  bool CanCenter();
  //! center and scale map using player's direction and speed
  void CenterMiniMap();
  /// enable / disable continuous centering of the map
  void SetAutocenter(bool set) {_autoCenter = set;}

  void OnDraw(UIViewport *vp, float alpha);

#if _ENABLE_CHEATS
  EntityAI *GetObserver() const;
#endif

  //! set command state
  void SetCommandState(int id, int state, AISubgroup *subgrp);

  //! sets active (focused) marker
  void SetActiveMarker(int marker) {_activeMarker = marker;}

  //! adds info about reported unit
  void AddUnitInfo(AIUnit *unit);
  //! adds info about reported enemy
  void AddEnemyInfo
    (
    TargetType *id, const VehicleType *type, int x, int z
    );

  /// return the target cursor is pointing at
  EntityAI *GetTarget() const;
  /// retrieve the position cursor is pointing at (returns true when succeeded)
  bool GetPosition(Vector3 &pos) const;
  /// return the high command target cursor is pointing at
  AIGroup *GetHCTarget() const;


  LSError Serialize(ParamArchive &ar, RString name, int minVersion);

#if _VBS3 // integration of real time editor with main map
  virtual bool IsDragging() {return false;}
  virtual void ClearDrawLinks() {}
  virtual bool IsSelecting() {return false;}
  virtual bool IsDraggingRM() {return false;} 
  virtual bool IsRealTime() {return false;}
  virtual void ClearSelectingBox() {}
  virtual void EditLink(EditorObject *obj, RString name, float key = -1, bool assignLinkArg = false) {}
  virtual Ref<EditorObject> GetLinkObject() {return NULL;}
  virtual void SetLinkObject(Ref<EditorObject> obj) {}
  virtual void RemoveAllDrawLinks(EditorObject *from) {}
  virtual GameVarSpace *GetVariables() {return NULL;}
  virtual EditorObject *FindEditorObject(GameValue value,RString type) {return NULL;}
  virtual RString AddObject
    (
    RString typeName, 
    const AutoArray<RString> &args, 
    bool create = true, 
    GameValue variable = NOTHING, 
    ConstParamEntryPtr subTypeConfig = NULL
    ) {return RString();}  
#endif

  //! returns player's center
  AICenter *GetMyCenter();
protected:
  //! returns player unit
  AIBrain *GetMyUnit();

  SignInfo FindSign(float x, float y);

  AIGroup *FindGroupMarker(float x, float y,int &waipoint);

  //! draws exposure map (from AICenter database)
  void DrawExposure();
  //! draws field costs (pathfinding) for player unit
  void DrawCost();
  //! draws strategic pathfinding progress
  void DrawPath();
#if _ENABLE_CHEATS
  //! draws status of the operative cache
  void DrawOperCache();
  /// draw how roads are connected
  void DrawRoadNet();
#endif
  //! draws all units for given center
  void DrawUnits(AICenter *center);

  //! process command issued from map
  void IssueCommand(float x, float y, bool alt, bool shift);
  void OnGroupMarkerClick(AIGroup *group, int RMB ,float x, float y,int waypoint);
  //! process group maker mouse over
  void OnGroupMarkerEvent(AIGroup *group, GameValue GGroupMarkerEvent,int waypoint);

  //! process move command issued from map
  void IssueMove(float x, float y);
  //! process watch command issued from map
  void IssueWatch(float x, float y);
  //! process attack command issued from map
  void IssueAttack(TargetType *target);
  //! process get in command issued from map
  void IssueGetIn(TargetType *target);

  //! draws command infos
  void DrawCommands();
  //! draws info about friendly units and enemies in each square
  void DrawInfo();
  //! draws all markers
  void DrawMarkers();
  //draw markers assigned to groups (shown in 3D world as well, used instead of MarTa)
  void DrawGroupIcons();
  //! draws all sensors
  void DrawSensors();
  //! draws waypoints for given group
  void DrawWaypoints(AICenter *center, AIGroup *myGroup);
  //! calculates real position of waypoint
  DrawCoord GetWaypointPosition(const WaypointInfo &wInfo);
  //skip irellevan items 
  bool CheckItemWantedDraw(const TargetNormal *target);

  void DrawLabel(struct SignInfo &info, PackedColor color, int shadow);

  //! draws exposure by enemy units in given square
  /*!
  \param center player's center
  \param x x position on screen
  \param y y position on screen
  \param xStep width of square on screen
  \param yStep height of square on screen
  \param i x position in landscape (in LandGrid coordinates)
  \param j z position in landscape (in LandGrid coordinates)
  \param iStep width in landscape (in LandGrid coordinates)
  \param jStep height in landscape (in LandGrid coordinates)

  */
  void DrawExposureEnemy
    (
    AICenter *center,
    float x, float y, float xStep, float yStep,
    int i, int j, int iStep, int jStep
    );
  //! draws field costs (pathfinding) in given square
  void DrawCost
    /*!
    \param subgroup player's subgroup
    \param x x position on screen
    \param y y position on screen
    \param xStep width of square on screen
    \param yStep height of square on screen
    \param i x position in landscape (in LandGrid coordinates)
    \param j z position in landscape (in LandGrid coordinates)
    \param iStep width in landscape (in LandGrid coordinates)
    \param jStep height in landscape (in LandGrid coordinates)

    */
    (
    AISubgroup *subgroup, float invCost,
    float x, float y, float xStep, float yStep,
    int i, int j, int iStep, int jStep
    );
#if _ENABLE_CHEATS
  //! draws status of the operative cache in given square
  void DrawOperCache(float x, float y, float xStep, float yStep, int i, int j, int iStep, int jStep);
  /// draw how roads are connected
  void DrawRoadNet(float x, float y, float xStep, float yStep, int i, int j);
#endif
  //! draws info about friendly units and enemies in given square
  /*!
  \param x x position on screen
  \param y y position on screen
  \param xStep width of square on screen
  \param yStep height of square on screen
  \param i x position in landscape (in LandGrid coordinates)
  \param j z position in landscape (in LandGrid coordinates)
  */
  void DrawInfo
    (
    float x, float y, float xStep, float yStep, int i, int j
    );

  //! find nearest unit
  /*!
  \param center center of searched unit
  \param pt position
  \param retrieved info nearest units description retrieved 
  \param minDist limit of distance, retrieved distance of nearest unit
  */
  void FindUnit(AICenter *center, Vector3Par pt, SignInfo &info, float &minDist);
  //! find nearest waypoint
  /*!
  \param myCenter center of searched unit
  \param myGroup group of searched unit
  \param pt position
  \param retrieved info nearest units description retrieved 
  \param minDist limit of distance, retrieved distance of nearest unit
  */
  void FindWaypoint
    (
    AICenter *myCenter, AIGroup *myGroup,
    Vector3Par pt, SignInfo &info, float &minDist
    );
};

#ifndef _XBOX

//! NOT USED CURRENTLY
class CWarrant : public Control
{
protected:
  Ref<Font> _font;
  float _size;
  PackedColor _color;

public:
  CWarrant(ControlsContainer *parent, int idc, ParamEntryPar cls);
  virtual void OnDraw(UIViewport *vp, float alpha);
};

//! Display for insertion of user marker in main map (or briefing)
class DisplayInsertMarker : public Display
{
public:
  //! written marker text
  RString _text;
  //! selected marker picture
  int _picture;
  //! selected marker color
  int _color;
  //! position of display
  float _x, _y;
  //! clipping rectangle for display
  float _cx, _cy, _cw, _ch;

public:
  //! constructor
  /*!
  \param parent parent display
  \param x position of display
  \param y position of display
  \param cx clipping rectangle for display
  \param cy clipping rectangle for display
  \param cw clipping rectangle for display
  \param ch clipping rectangle for display
  \param enableSimulation enable game simulation
  - true if called from main map
  - false if called from briefing
  */
  DisplayInsertMarker(ControlsContainer *parent, float x, float y, float cx, float cy, float cw, float ch, bool enableSimulation);
  //! destructor
  ~DisplayInsertMarker();

  // Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnButtonClicked(int idc);
  virtual bool OnKeyDown(int dikCode);
  virtual bool OnKeyUp(int dikCode);

  void Destroy();

protected:
  //! reads parameters of selected picture from config
  void UpdatePicture();
  //! select previous picture
  void PrevPicture();
  //! select next picture
  void NextPicture();
  //! select previous color
  void PrevColor();
  //! select next color
  void NextColor();
};

#endif

//! number of weapon slots (primary weapon, secondary weapon, handgun, two special items and 12 inventory slots)
#define WEAPON_SLOTS          17
//! the first weapon slot used for inventory
#define FIRST_INVENTORY_SLOT  5
//! maximal number of magazine slots
#define MAGAZINE_SLOTS        20
//! the first magazine slot used for primary weapon
#define FIRST_PRIMARY_SLOT    0
//! the first magazine slot obviously used for secondary weapon
#define FIRST_SECONDARY_SLOT  6
//! the first magazine slot used for hand gun
#define FIRST_HANDGUN_SLOT    12

//! Information about weapons owned by unit
struct UnitWeaponsInfo : public NetworkSimpleObject, public SerializeClass
{
  //! which unit
  OLinkPerm<AIBrain> unit;
  //! name of unit
  RString name;
  //! description of slots owned by units (bit fields)
  int weaponSlots;
  //! weapons currently in slots
  Ref<WeaponType> weapons[WEAPON_SLOTS];
  //! magazines currently in slots
  Ref<Magazine> magazines[MAGAZINE_SLOTS];
  //! backpacks currently in slots
  Ref<EntityAI> backpack;

  //! constructor
  UnitWeaponsInfo() {weaponSlots = 0;}
  //! constructor
  /*!
  Reads weapons information from given unit.
  */
  UnitWeaponsInfo(AIBrain *u);

  //! refresh the info by the unit
  void Update();

  //! removes weapon from slots
  void RemoveWeapon(const WeaponType *weapon)
  {
    for (int i=0; i<WEAPON_SLOTS; i++)
      if (weapons[i] == weapon) weapons[i] = 0;
  }
  //! removes magazine from slots
  void RemoveMagazine(const Magazine *magazine)
  {
    for (int i=0; i<MAGAZINE_SLOTS; i++)
      if (magazines[i] == magazine) magazines[i] = 0;
  }

  void RemoveBackpack()
  {
    backpack = NULL;
  }

  Ref<EntityAI> RemoveBackpack(RString name)
  {
    if(backpack && strcmpi(backpack->GetType()->GetName(),name)==0)
    {     
      Ref<EntityAI> bag = backpack;
      backpack = NULL;
      return bag;
    }
    return NULL;
  }

  bool HasWeapon(const WeaponType *weapon)
  {
    for (int i=0; i<WEAPON_SLOTS; i++)
      if (weapons[i] == weapon) return true;
    return false;
  }

  bool HasMagazine(const Magazine *magazine)
  {
    for (int i=0; i<MAGAZINE_SLOTS; i++)
      if (magazines[i] == magazine) return true;
    return false;
  }

  bool HasBackpack() const
  {
    if(backpack && !backpack->ToDelete()) return true;
    return false;
  }

  //! returns if magazine can be used for some owned weapon
  bool IsMagazineUsable(const MagazineType *type);

  LSError Serialize(ParamArchive &ar);

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
    (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
    );
  TMError TransferMsg(NetworkMessageContext &ctx);
};
TypeContainsOLink(UnitWeaponsInfo);

struct MagazinesSummaryItem
{
  const MagazineType *type;
  int count;
};
TypeIsSimple(MagazinesSummaryItem)

typedef AutoArray<MagazinesSummaryItem, MemAllocLocal<MagazinesSummaryItem, 64> > MagazinesSummary;

struct WeaponsSummaryItem
{
  const WeaponType *type;
  int count;
};
TypeIsSimple(WeaponsSummaryItem)

typedef AutoArray<WeaponsSummaryItem, MemAllocLocal<WeaponsSummaryItem, 64> > WeaponsSummary;

/// Interface for object handling pool of weapons and magazines
class IItemsPool : public RefCount
{
public:
  IItemsPool() {}
  virtual ~IItemsPool() {}

  /// check if invoked in-game or from briefing
  virtual bool IsInGame() const {return true;}
  /// check if given unit can drop items
  virtual bool CanDrop(const AIBrain *unit) const = 0;
  /// check if given unit can replace items
  virtual bool CanReplace(const AIBrain *unit) const = 0;
  /// check if given unit can rearm
  virtual bool CanRearm(const AIBrain *unit) const = 0;
  /// return description of Drop / Put action
  virtual RString GetDropActionName(const AIBrain *unit, bool magazine) const = 0;

  // Manipulation with pool members
  /// drop weapon back to the pool
  virtual void DropWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack) = 0;
  /// drop magazine back to the pool
  virtual void DropMagazine(UnitWeaponsInfo &info, Magazine *magazine, bool backpack) = 0;
  /// drop magazine back to the pool
  virtual void DropBackpack(UnitWeaponsInfo &info, RString name) = 0;
  /// add weapon from the pool (drop other weapons if necessary)
  virtual void AddWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack) = 0;
  /// add magazine from the pool (drop other magaziness if necessary)
  virtual void AddMagazine(UnitWeaponsInfo &info, MagazineType *type, bool backpack) = 0;
  /// add magazine from the pool (drop other magaziness if necessary)
  virtual void AddBackpack(UnitWeaponsInfo &info, RString name) = 0;
  /// replace weapon by the weapon from the pool
  virtual void ReplaceWeapon(UnitWeaponsInfo &info, int slot, WeaponType *weapon, bool backpack) = 0;
  /// replace magazine by the magazine from the pool
  virtual void ReplaceMagazine(UnitWeaponsInfo &info, int slot, MagazineType *type, bool backpack) = 0;
  /// replace magazine by the magazine from the pool
  virtual void ReplaceBackpack(UnitWeaponsInfo &info, RString name) = 0;
  /// take usable magazines
  virtual void Rearm(UnitWeaponsInfo &info) = 0;

  // Searching for pool members
  /// find weapon in pool by its slot type
  virtual WeaponType *FindWeapon(int mask, int maskExclude) const = 0;
  /// find magazine in pool by its slot type
  virtual Magazine *FindMagazine(int mask) const = 0;

  // Enumeration of pool members
  /// summarization of weapons
  virtual void SummarizeWeapons(WeaponsSummary &result, int mask, int maskExclude) const = 0;
  /// summarization of magazines
  virtual void SummarizeMagazines(MagazinesSummary &result, int mask, int maskExclude) const = 0;
  /// summarization of backpacks
  virtual  RefArray<EntityAI> SummarizeBackpacks() const = 0;
  /// name of a pool source
  virtual RString GetDisplayName() {return "";}
  /// pool source
  virtual EntityAI *GetSource() {return NULL;}

  virtual void SwitchSources() {};
};

/// List of weapons in pool
struct WeaponsPool : public NetworkSimpleObject, public RefArray<WeaponType>
{
  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
    (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
    );
  TMError TransferMsg(NetworkMessageContext &ctx);
};

/// List of magazines in pool
struct MagazinesPool : public NetworkSimpleObject, public RefArray<Magazine>
{
  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
    (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
    );
  TMError TransferMsg(NetworkMessageContext &ctx);
};

/// List of backpacks in pool
struct BackpacksPoolItem
{
  Ref<EntityAI> _backpack;
  NetworkId _id;
  RString _typmeName;

  BackpacksPoolItem()
  {
    _backpack = NULL;
  }

  BackpacksPoolItem(EntityAI *backpack)
  {
    _backpack = backpack;
    _id = backpack->GetNetworkId();
    _typmeName= backpack->GetType()->GetName();
  }

  BackpacksPoolItem(EntityAI *backpack, NetworkId id, RString typmeName)
  {
    _backpack = backpack;
    _id = id;
    _typmeName = typmeName;
  }

  RString GetName() {return _typmeName;}
  LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(BackpacksPoolItem);

struct BackpacksPool : public NetworkSimpleObject, public AutoArray<BackpacksPoolItem>
{
  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
    (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
    );
  TMError TransferMsg(NetworkMessageContext &ctx);
};


/// Implementation of IItemsPool interface handling fixed set of weapons and magazines
class FixedItemsPool : public IItemsPool
{
public:
  /// weapons in pool (available to group)
  WeaponsPool _weaponsPool;
  /// magazines in pool (available to group)
  MagazinesPool _magazinesPool;
  /// backpacks in pool (available to group)
  BackpacksPool _backpacksPool;
  //true if subordinate can take weapon from pool
  bool _canTakeWeapon;

public:
  FixedItemsPool() 
  {
    ConstParamEntryPtr clsWeapon = ExtParsMission.FindEntry("allowSubordinatesTakeWeapons");
    _canTakeWeapon = (clsWeapon ? (*clsWeapon) : false);
  }

  // implementation of IItemsPool
  virtual bool IsInGame() const {return false;}
  virtual bool CanDrop(const AIBrain *unit) const;
  virtual bool CanReplace(const AIBrain *unit) const;
  virtual bool CanRearm(const AIBrain *unit) const;
  virtual RString GetDropActionName(const AIBrain *unit, bool magazine) const;
  virtual void DropWeapon(UnitWeaponsInfo &info, WeaponType *weapon , bool backpack);
  virtual void DropMagazine(UnitWeaponsInfo &info, Magazine *magazine, bool backpack );
  virtual void DropBackpack(UnitWeaponsInfo &info, RString name);
  virtual void AddWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack);
  virtual void AddMagazine(UnitWeaponsInfo &info, MagazineType *type, bool backpack);
  virtual void AddBackpack(UnitWeaponsInfo &info, RString name);
  virtual void ReplaceWeapon(UnitWeaponsInfo &info, int slot, WeaponType *weapon, bool useBackpack);
  virtual void ReplaceMagazine(UnitWeaponsInfo &info, int slot, MagazineType *type, bool useBackpack);
  virtual void ReplaceBackpack(UnitWeaponsInfo &info, RString name);
  virtual void Rearm(UnitWeaponsInfo &info);
  virtual WeaponType *FindWeapon(int mask, int maskExclude) const;
  virtual Magazine *FindMagazine(int mask) const;
  virtual void SummarizeWeapons(WeaponsSummary &result, int mask, int maskExclude) const;
  virtual void SummarizeMagazines(MagazinesSummary &result, int mask, int maskExclude) const;
  virtual RefArray<EntityAI> SummarizeBackpacks() const;

  /// return weapon back to pool
  void ReturnWeapon(WeaponType *weapon);
  /// return magazine back to pool
  void ReturnMagazine(Magazine *magazine);
  /// return backpack back to pool
  void ReturnBackpack(EntityAI *backpack);

  /// loads pool content from ParamClass
  bool Import(ParamEntryVal superclass);

  /// reaction to message from the network server (weapon available) 
  void OnWeaponReplaced(UnitWeaponsInfo &info, int slot, WeaponType *weapon, bool useBackpack);
  /// reaction to message from the network server (magazine available) 
  void OnMagazineReplaced(UnitWeaponsInfo &info, int slot, Magazine *magazine, bool useBackpack);
  /// reaction to message from the network server (backpack available) 
  void OnBackpackReplaced(UnitWeaponsInfo &info, EntityAI *backpack);


  /// clear so that no data are referenced any more
  void Clear();
  /// find magazine in pool and remove it then
  //  (used also from the scripting function "fillWeaponsFromPool" and from the network server)
  Ref<Magazine> RemoveMagazine(const MagazineType *type);

  bool RemoveBackpack(EntityAI *backpack);
  bool RemoveBackpack(NetworkId id);

protected:
  // implementation
  /// find magazine in pool
  Magazine *FindMagazine(const MagazineType *type) const;
  /// find magazine in pool
  bool FindWeapon(const WeaponType *type) const;
  /// find weapon in pool and remove it then
  bool RemoveWeapon(WeaponType *weapon);
  /// add weapon to the pool
  void AddWeapon(WeaponType *weapon);
  /// add magazine to the pool
  void AddMagazine(Magazine *magazine);
  /// add backpack to the pool
  void AddBackpack(EntityAI *backpack);
  /// return all magazines that are at no use to the pool
  void RemoveUnusableMagazines(UnitWeaponsInfo &info, WeaponType *weapon);
  /// add magazines suitable for given weapon to given slots
  void AddUsableMagazines(UnitWeaponsInfo &info, WeaponType *weapon, int from, int to);
  /// add magazines suitable for given weapon to given slots
  void AddUsableHandGunMagazines(UnitWeaponsInfo &info, WeaponType *weapon, int from, int to);
};

//! Information about weapons owned by group
/*!
Contains weapons of all units in group and pool.
*/
struct WeaponsInfo : public SerializeClass
{
  //! descriptions of weapons for each unit in group
  AutoArray<UnitWeaponsInfo> _weapons;

#if defined _XBOX && _XBOX_VER >= 200
  //! loads info from stream
  bool Load(QIStream &in, FixedItemsPool &pool);
  //! saves info into stream
  bool Save(QOStream &out, FixedItemsPool &pool);
#else
  //! loads info from file
  bool Load(RString filename, FixedItemsPool &pool);
  //! saves info into file
  bool Save(RString filename, FixedItemsPool &pool);
#endif
  //! changes weapons of real units by information in this info
  void Apply();

  LSError Serialize(ParamArchive &ar);
};

//! States of mission objective
enum ObjectiveStatus
{
  OSActive,
  OSDone,
  OSFailed,
  OSHidden
};

/// custom control used for Item slots in the Gear dialog
class CItemSlot : public Control, public CTextContainer
{
public:
  //! text color
  PackedColor _color;
  //! ammo bar color
  PackedColor _ammoColor;
  //! color of background
  PackedColor _bgColor;
  //! color of background when selected
  PackedColor _bgColorSelected;
  //! color of rectangle when focused
  PackedColor _colorFocused;
private:
  //! picture
  Ref<Texture> _texture;
  //item data
  RString _data;
  int _itemType;
  //@{
  //! sounds
  SoundPars _pushSound;
  SoundPars _clickSound;
  SoundPars _dblClickSound;
  //@}

  /// is the control selected (inside a group of all slots)
  bool _selected;

  //assigned magazine (in any)
  Ref<Magazine> _magazine;

public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  */
  CItemSlot(ControlsContainer *parent, int idc, ParamEntryPar cls);

  virtual IControl *GetControl() {return this;}

  virtual PackedColor GetColor() const {return _color;}
  virtual void SetColor(PackedColor color) {_color = color;}

  virtual bool OnKeyDown(int dikCode);
  virtual bool OnKeyUp(int dikCode);
  virtual void OnLButtonDown(float x, float y);
  virtual void OnLButtonUp(float x, float y);
  virtual void OnLButtonDblClick(float x, float y);
  virtual void OnDraw(UIViewport *vp, float alpha);

  virtual bool SetText(RString text); // returns true if changed
  void SetTexture(Texture *texture, RString text); 
  bool IsSelected() const {return _selected;}
  void SetSelected(bool selected) {_selected = selected;}

  bool CanBeDefault() const {return false;}

  void SetMagazine(Magazine *magazine) {_magazine = magazine;}
  Magazine *GetMagazine() const {return _magazine;};

  void SetData(RString text);
  RString GetData();

  void SetItemType(int itemType);
  int GetItemType();

  void SetStyle(int style, PackedColor color, PackedColor colorBackground,
    PackedColor colorBackgroundSelected, PackedColor colorFocused, bool canDrag,PackedColor ammoColor)
  {
    _style = style;
    _color = color;
    _bgColor = colorBackground;
    _bgColorSelected = colorBackgroundSelected;
    _colorFocused = colorFocused;
    _ammoColor = ammoColor;
  }

protected:
  //! reaction when button is clicked
  void DoClick();
  //! reaction when button is double clicked
  void DoDblClick();
};


//! Base (abstract) map display
class DisplayMap : public Display
{
#if _VBS3
  // make aar a friend class here to get briefing page
  friend class AAR;
#endif
protected:
  //! briefing control
  InitPtr<IControl> _briefingCtrl;
  //! briefing content
  InitPtr<CHTMLContainer> _briefing;
  //! mission name control
  InitPtr<ITextContainer> _name;
  //@{
  //! bookmark control
  InitPtr<ITextContainer> _bookmark1;
  InitPtr<ITextContainer> _bookmark2;
  InitPtr<ITextContainer> _bookmark3;
  InitPtr<ITextContainer> _bookmark4;
  //@}
  //@{
  //! radio message control
  InitPtr<ITextContainer> _radioAlpha;
  InitPtr<ITextContainer> _radioBravo;
  InitPtr<ITextContainer> _radioCharlie;
  InitPtr<ITextContainer> _radioDelta;
  InitPtr<ITextContainer> _radioEcho;
  InitPtr<ITextContainer> _radioFoxtrot;
  InitPtr<ITextContainer> _radioGolf;
  InitPtr<ITextContainer> _radioHotel;
  InitPtr<ITextContainer> _radioIndia;
  InitPtr<ITextContainer> _radioJuliet;
  //@}
  //! gps text control
  InitPtr<ITextContainer> _gpsCtrl;
  InitPtr<ITextContainer> _gpsAltCtrl;
  InitPtr<ITextContainer> _gpsHeadingCtrl;
  //! map control
  InitPtr<CStaticMapMain> _map;

  //! compass object
  Ref<Compass> _compass;
  //! watch object
  Ref<Watch> _watch;
  //! walkie talkie (radio) object
  Ref<ControlObjectContainer> _walkieTalkie;
  //! briefing notepad object
  Ref<Notepad> _notepad;
  //! warrant object (not used currently)
  Ref<ControlObjectContainer> _warrant;
  //! GPS object
  Ref<ControlObjectContainer> _gps;
  
  /// tell us if GPS is enabled by the mission or / and scripting
  bool _showGPS;
  /// tell us if Compass is enabled by the mission or / and scripting
  bool _showCompass;
  /// tell us if Watch is enabled by the mission or / and scripting
  bool _showWatch;
  /// tell us if Map is enabled by the mission or / and scripting
  bool _showMap;

  //! allow select weapons
  bool _selectWeapons;
  bool _canTakeWeapon;

  //! load / save objects position into user profile
  bool _saveParams;
  //! xbox style groups / gear
  bool _xboxStyle;

  bool _updatePlanAsked;
  bool _showGearOnExit;

  bool _briefingEnabled;

  //! information about weapons owned by units
  WeaponsInfo _weaponsInfo;
  //! pool of weapons available to group
  FixedItemsPool _pool;

  //! currently displayed mouse cursor
  CursorType _cursor;

  //@{
  //! notepad textures
  Ref<Texture> _picturePlan;
  Ref<Texture> _pictureNotes;
  Ref<Texture> _pictureGroup;
  Ref<Texture> _picture;
  //@}

#if _VBS2
  Vector3 _compassDir;
#endif

public:
  //! constructor
#if _VBS2
  DisplayMap(ControlsContainer *parent);
#endif
  /*!
  \param parent parent display
  \param resource name of dialog resource template
  */
  DisplayMap(ControlsContainer *parent, RString resource);
  //! destructor
  ~DisplayMap();

  //! returns map control
  CStaticMapMain *GetMap() {return _map;}
  //! weapons owned by units
  WeaponsInfo &GetWeaponsInfo() {return _weaponsInfo;}
  //! pool of weapons available to group
  FixedItemsPool &GetWeaponsPool() {return _pool;}

#if _ENABLE_CHEATS
  Object *GetObserver() const {return _map ? _map->GetObserver() : NULL;}
#endif

  /// tell us if Map is enabled by the mission or / and scripting
  bool IsShownMap() const {return _showMap;}
  /// tell us if Compass is enabled by the mission or / and scripting
  bool IsShownCompass() const {return _showCompass;}
  /// tell us if Watch is enabled by the mission or / and scripting
  bool IsShownWatch() const {return _showWatch;}
  /// tell us if GPS is enabled by the mission or / and scripting
  bool IsShownGPS() const {return _showGPS;}

  //! returns if walkie talkie is shown
  bool IsShownWalkieTalkie() const;
  //! returns if briefing notepad is shown
  bool IsShownNotepad() const;
  //! returns if warrant is shown
  bool IsShownWarrant() const;

  //! enables / disables displaying of map
  void ShowMap(bool show = true) {_showMap = show;}
  //! enables / disables displaying of compass
  void ShowCompass(bool show = true) {_showCompass = show;}
  //! enables / disables displaying of watch
  void ShowWatch(bool show = true) {_showWatch = show;}
  //! enables / disables displaying of GPS
  void ShowGPS(bool show = true) {_showGPS = show;}
  
  //! enables / disables displaying of walkie talkie
  void ShowWalkieTalkie(bool show = true);
  //! enables / disables displaying of briefing notapad
  void ShowNotepad(bool show = true);
  //! enables / disables displaying of warrant
  void ShowWarrant(bool show = true);

  bool IsBriefingUsed() const;
  //! returns if notepad with briefing is shown
  bool IsShownBriefing() const;
  //! show / hide notepad with briefing
  void ShowBriefing(bool show);

  //! ingame (main) map
  virtual bool IsInGame() const {return false;}

  /// return the target cursor is pointing at
  virtual EntityAI *GetTarget() const {return _map ? _map->GetTarget() : NULL;}
  /// retrieve the position cursor is pointing at (returns true when succeeded)
  virtual bool GetPosition(Vector3 &pos) const {return _map ? _map->GetPosition(pos) : false;}
  /// return the high command target cursor is pointing at
  virtual AIGroup *GetHCTarget() const {return _map ? _map->GetHCTarget() : NULL;}

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  ControlObject *OnCreateObject(int type, int idc, ParamEntryPar cls);
 
  static int NextMarkerID;

  void OnChildDestroyed(int idd, int exit);

  bool OnKeyDown(int dikCode);
  bool OnKeyUp(int dikCode);
  void OnDraw(EntityAI *vehicle, float alpha);
  void OnSimulate(EntityAI *vehicle);

  void OnHide();

  void OnButtonClicked(int idc);
  void OnHTMLLink(int idc, RString link);

  RString GetKeyHint(int button, bool &structured) const;

  void ResetHUD();

  //! recalculates rectangle of map not overlapped by notepad
  void AdjustMapVisibleRect();

  //! updates mission plan section of briefing
  void UpdatePlan();

  //! updates mission plan section of briefing
  void AskForUpdatePlan();

  //! switch section in briefing
  virtual void SwitchBriefingSection(RString section);

#if defined _XBOX && _XBOX_VER >= 200
  //! loads weapons info from stream
  void LoadWeapons(QIStream &in) {_weaponsInfo.Load(in, _pool);}
  //! saves weapons info into stream
  void SaveWeapons(QOStream &out) {_weaponsInfo.Save(out, _pool);}
#else
  //! loads weapons info from file
  void LoadWeapons(RString filename) {_weaponsInfo.Load(filename, _pool);}
  //! saves weapons info into file
  void SaveWeapons(RString filename) {_weaponsInfo.Save(filename, _pool);}
#endif

  //! updates gear page of briefing by real situation
  void UpdateWeaponsInBriefing(EntityAI *owner = NULL, bool load = false);
  
  //! updates gear page of given unit
  void UpdateUnitWeaponsInBriefing(EntityAI *owner);

  //! updates units page of briefing by real situation
  void UpdateUnitsInBriefing();
  //return gear display
  Display *GetGearDisplay();

  void SetScales(float scaleMin, float scaleMax, float scaleDefault)
  {
    if (_map) _map->SetScales(scaleMin, scaleMax, scaleDefault);
  }

  /// sets active (focused) marker
  void SetActiveMarker(int marker) {if (_map) _map->SetActiveMarker(marker);}
  /// start map animation to the desired position
  void AnimateTo(Vector3Par position);

  //LSError Serialize(ParamArchive &ar);

  //! initialization of display
#if !_VBS3 // support for dynamic briefing
  void Init();
#else
  void Init(const RString& alternateBriefing = RString());
#endif

  //! external initialization of display
  void InitHUD();

  virtual LSError Serialize(ParamArchive &ar);

protected:
  //! loads parameters (f.e. placement of items) from user profile
  void LoadParams();
  //! saves parameters (f.e. placement of items) into user profile
  void SaveParams();
  //! serialization of parameters (f.e. placement of items)
  LSError SerializeParams(ParamArchive &ar);

  //! updates texts on walkie talkie by real situation
  void SetRadioText();

  /// update text in the button show / hide textures
  void UpdateTexturesButton();

  /// Update texts in buttons based on the current content
  void UpdateButtons();

  friend void SetCommandState(int id, CommandState state, AISubgroup *subgrp);
};

/// from which context diary was executed
enum DiaryContext
{
  DCInGame,
  DCBriefing,
  DCDebriefing
};

struct TextureByName
{
  RString _name;
  Ref<Texture> _texture;

  TextureByName() {}
  TextureByName(RString name, Ref<Texture> texture) : _name(name), _texture(texture) {}
  const char *GetKey() const {return _name;}
};
TypeIsMovableZeroed(TextureByName);

typedef MapStringToClass< TextureByName, AutoArray<TextureByName> > TexturesByName;

//special listbox for map diary where mouse wheel does not control scrollBar bar selection
class CMapListBox: public CListBox
{
private:
  /// map display which is our parent (to fast access without casting)
  LLink<DisplayMap> _map;
public:
  CMapListBox(DisplayMap *map, int idc, ParamEntryPar cls);
  CMapListBox(Display *parent, int idc, ParamEntryPar cls) : CListBox(parent, idc, cls){};
  virtual void OnMouseZChanged(float dz);
};
//special group for map diary where mouse wheel does not control childs but scrollbar
class CMapCGroup: public CControlsGroup
{
private:
  /// map display which is our parent (to fast access without casting)
  LLink<DisplayMap> _map;
public:
  CMapCGroup(DisplayMap *map, int idc, ParamEntryPar cls);
  CMapCGroup(Display *parent, int idc, ParamEntryPar cls) : CControlsGroup(parent, idc, cls){};
  virtual void OnMouseZChanged(float dz);
};

class DisplayDiaryInMap
{
protected:
  /// list of diary topics
  AutoArray<RString> _topics;
  //! diary topics control
  Ref<CListBox> _diaryTopics;
  //! diary index control
  Ref<CListBox> _diaryIndex;
  //! diary content control
  Ref<CHTML> _diary;
  //! HMTL container
  Ref<CControlsGroup> _controlsGroup;
  /// set of textures used as image icons
  TexturesByName _indexTextures;

  InitPtr<ITextContainer> _playerName;
  InitPtr<ITextContainer> _currentTask;
  InitPtr<ITextContainer> _currentMission;

  //max diary controls height
  float _maxDiaryTopicsH;
  float _maxDiaryIndexH;
  float _maxDiaryH;
  float _maxControlsGroupH;
  //@{
  //! diary buttons
  InitPtr<ITextContainer> _diaryAdd;
  //@}

  /// diary owner
  OLinkPermO(Person) _diaryPerson;

  /// for backward compatibility, handle content of briefing.html
  HTMLContent _briefing;

  /// Info about the record we want to delete (after user confirmation)
  RString _toDeleteSubject;
  /// Info about the record we want to delete (after user confirmation)
  int _toDeleteRecord;

  /// from which context diary was executed
  DiaryContext _context;
  /// map display which is our parent (to fast access without casting)
  LLink<DisplayMap> _map;

public:
  DisplayDiaryInMap(){};
  DisplayDiaryInMap(DisplayMap *parent);

  void OnButtonClicked(int idc);
  void OnLBSelChanged(IControl *ctrl, int curSel);
  void OnLBDblClick(int idc, int curSel);
  void OnHTMLLink(int idc, RString link, DiaryContext context);
  void OnChildDestroyed(int idd, int exit,Ref<ControlsContainer> child);
  void OnSimulate(EntityAI *vehicle);
  //! update diary if needed
  void UpdateDiary(bool force = false);
  //! synchronize diary with index
  void SyncDiary();
  //! update text in diary buttons
  void UpdateButtons();
  void UpdateTopics();

  /// check if the diary is owned by the current player
  void UpdateOwner();

  //! navigate to the record described by the link
  void ProcessLink(RString link);
  void ProcessLink(RString link,DiaryContext context);
  void SyncTopicList(int index);
  void ClearLink();
  void ResizeHTML();

  void ResetListBox();

protected:
  RString GetCurrentPage() const;
  /// select given diary page
  bool SelectPage(RString name);

  /// write briefing notes section to diary
  void WriteBriefingNotes();
  /// write briefing plan section to diary
  void WriteBriefingPlan();
  /// find references to other sections in srcSection, add these sections to _diary if needed
  void HandleReferences(const HTMLSection &section);
};

//! Main (ingame) map display
class DisplayMainMap : public  DisplayMap
{
protected:
#if _ENABLE_IDENTITIES
  //class for diary processing
  DisplayDiaryInMap _displayDiaryMap;
#endif


public:
  //! constructor
  /*!
  \param parent parent display
  */
  DisplayMainMap(ControlsContainer *parent);
  void ResetHUD();
  void DestroyHUD(int exit);
  virtual void OnButtonClicked(int idc);
  virtual bool OnKeyDown(int dikCode);
  virtual bool OnKeyUp(int dikCode);
  virtual void OnLBSelChanged(IControl *ctrl, int curSel);
  virtual void OnLBDblClick(int idc, int curSel);
  virtual void OnSimulate(EntityAI *vehicle);
  virtual void OnHTMLLink(int idc, RString link);
  virtual bool IsInGame() const {return true;}
  virtual void OnChildDestroyed(int idd, int exit);
  void ProcessDiaryLink(RString link, DiaryContext contex);
};


//! Briefing display
class DisplayGetReady : public DisplayMap
{
protected:
  bool _soundPlanPlayed;
  bool _soundNotesPlayed;
  bool _soundGroupPlayed;
  bool _soundTeamSwitchPlayed;

  RefAbstractWave _sound;

  //class for diary processing
#if _ENABLE_IDENTITIES
  DisplayDiaryInMap _displayDiaryMap;
  //max diary topic listBox height
  float _diaryTopicsHeight;
#endif

public:
  //! constructor
  /*!
  \param parent parent display
  */
  DisplayGetReady(ControlsContainer *parent);
  //! constructor
  /*!
  \param parent parent display
  \param resource name of dialog resource template
  */
  DisplayGetReady(ControlsContainer *parent, RString resource);
  ~DisplayGetReady();
  void OnButtonClicked(int idc);
  void OnChildDestroyed(int idd, int exit);
  void Destroy();
  virtual void OnSimulate(EntityAI *vehicle);
  virtual void SwitchBriefingSection(RString section);
  virtual bool IsInGame() const {return false;}

  virtual void OnLBSelChanged(IControl *ctrl, int curSel);
  virtual void OnLBDblClick(int idc, int curSel);
  virtual void OnHTMLLink(int idc, RString link)
  {
#if _ENABLE_IDENTITIES
    _displayDiaryMap.OnHTMLLink(idc,link,DCBriefing);
#endif
  }

  void PoolReplaceWeapon(AIUnit *unit, WeaponType *weapon, int slot, bool useBackpack);
  void PoolReplaceMagazine(AIUnit *unit, Magazine *magazine, int slot, bool useBackpack);
  void PoolReplaceBackpack(AIUnit *unit, EntityAI *backpakc);
  void PoolUpdateWeaponsPool(NetworkMessageContext &ctx);
  void PoolUpdateMagazinesPool(NetworkMessageContext &ctx);
  void PoolUpdateBackpacksPool(NetworkMessageContext &ctx);
  void PoolUpdateWeapons(NetworkMessageContext &ctx);

  void ProcessDiaryLink(RString link,DiaryContext contex);

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  /// handle invitation
  virtual void OnInvited();
#endif

protected:
  //! Play 2D sound (as reaction to switch sections in briefing)
  /*!
  \param var variable with sound name
  */
  void PlaySound(RString name);
};

// shortcut to briefing
static DisplayGetReady *GBriefing = NULL;

class LiveStatsUpdate;

//! Debriefing display
class DisplayDebriefing : public Display
{
protected:
  //! mission result
  InitPtr<ITextContainer> _result;
  //! mission name
  InitPtr<ITextContainer> _title;
  //! debriefing text
  InitPtr<CHTMLContainer> _debriefing;
  //! list of objectives
  InitPtr<CHTMLContainer> _overview;
  //! basic mission info
  InitPtr<CHTMLContainer> _info;
  /*
  //! left page control
  InitPtr<CHTMLContainer> _left;
  //! right page control
  InitPtr<CHTMLContainer> _right;
  */
  //! statistics page control
  InitPtr<CHTMLContainer> _stats;
  //! statistics of last mission
  AIStats _oldStats;
  //! NOT USED CURRENTLY
  bool _animation;
  //! restart mission hint enabled
  bool _restartEnabled;
  /*
  RString _oldMission;
  RString _oldDirectory;
  RString _oldSubdirectory;
  */
  //! Generate open / close statistics links
  bool _statisticsLinks;

  bool _server;
  bool _client;

  bool _objectives;

#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP
  // SP statistics are not possible on Xbox 360
  bool _exiting;
  SRef<LiveStatsUpdate> _statsUpdate;
#endif
  int _wantedExit;

public:
  //! constructor
  /*!
  \param parent parent display
  \param animation NOT USED CURRENTLY
  */
  DisplayDebriefing(ControlsContainer *parent, bool animation, bool restartEnabled = false);
  void Destroy();
  bool CanDestroy();

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnButtonClicked(int idc);
  void OnHTMLLink(int idc, RString link);
  bool OnKeyUp(int dikCode);
  bool OnKeyDown(int dikCode);
  void OnSimulate(EntityAI *vehicle);
  void OnChildDestroyed(int idd, int exit);

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  /// handle invitation
  virtual void OnInvited();
#endif

protected:
  //! create content of debriefing pages
  void CreateDebriefing();    
  void CreateDebriefingFail();

  /// Update texts in buttons based on the current context
  void UpdateButtons();
#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP
  // SP statistics are not possible on Xbox 360
  void SignIn();
  void UpdateSPStats(bool tempSignIn);
#endif
};

//! Briefing display for client machine in multiplayer
class DisplayClientDebriefing : public DisplayDebriefing
{
  typedef DisplayDebriefing base;

public:
  //! constructor
  /*!
  \param parent parent display
  \param animation NOT USED CURRENTLY
  */
  DisplayClientDebriefing(ControlsContainer *parent, bool animation);
  void OnSimulate(EntityAI *vehicle);
};

inline DisplayMap *CStaticMapMain::GetParent()
{
  return check_cast<DisplayMap *>(_parent.GetLink());
}

#if _ENABLE_IDENTITIES

class DisplayDiary : public Display
{
protected:
  /// list of diary topics
  AutoArray<RString> _topics;
  //! diary topics control
  Ref<CListBox> _diaryTopics;
  //! diary index control
  Ref<CListBox> _diaryIndex;
  //! diary content control
  Ref<CHTML> _diary;
  //! HMTL container
  Ref<CControlsGroup> _controlsGroup;


  InitPtr<ITextContainer> _playerName;
  InitPtr<ITextContainer> _currentTask;
  InitPtr<ITextContainer> _currentMission;

  //max diary controls height
  float _maxDiaryTopicsH;
  float _maxDiaryIndexH;
  float _maxDiaryH;
  float _maxControlsGroupH;

  /// set of textures used as image icons
  TexturesByName _indexTextures;

  //@{
  //! diary buttons
  InitPtr<ITextContainer> _diaryAdd;
  //@}

  /// diary owner
  OLinkPermO(Person) _diaryPerson;


  /// for backward compatibility, handle content of briefing.html
  HTMLContent _briefing;

  /// Info about the record we want to delete (after user confirmation)
  RString _toDeleteSubject;
  /// Info about the record we want to delete (after user confirmation)
  int _toDeleteRecord;

  /// from which context diary was executed
  DiaryContext _context;
  /// map display which is our parent (to fast access without casting)
  LLink<Display> _parent;

public:
  DisplayDiary(Display *parent, RString link, DiaryContext context = DCInGame);
  ~DisplayDiary();

  void ResetListBox();
  void ClearLink();
  void ResizeHTML();
  void SyncTopicList(int index);

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnButtonClicked(int idc);
  bool OnKeyDown(int dikCode);
  bool OnKeyUp(int dikCode);
  void OnLBSelChanged(IControl *ctrl, int curSel);
  void OnHTMLLink(int idc, RString link);
  void OnChildDestroyed(int idd, int exit);
  void OnDraw(EntityAI *vehicle, float alpha);
  void OnSimulate(EntityAI *vehicle);
  //! update diary if needed
  void UpdateDiary(bool force = false);
  //! synchronize diary with index
  void SyncDiary();
  //! update text in diary buttons
  void UpdateButtons();

  //! navigate to the record described by the link
  void ProcessLink(RString link);
  void ProcessLink(RString link,DiaryContext context);

protected:
  RString GetCurrentPage() const;
  /// select given diary page
  bool SelectPage(RString name);

  /// write briefing notes section to diary
  void WriteBriefingNotes();
  /// write briefing plan section to diary
  void WriteBriefingPlan();
  /// find references to other sections in srcSection, add these sections to _diary if needed
  void HandleReferences(const HTMLSection &section);
};

#endif

// Arcade map (mission editor)

#if _ENABLE_EDITOR

//! Modes of mission editor
enum InsertMode
{
  IMUnits,
  IMGroups,
  IMSensors,
  IMWaypoints,
  IMSynchronize,
  IMMarkers,
  IMModules,
  IMN
};

//! Mission viewer control
class CStaticMapArcadeViewer : public CStaticMap
{
protected:
  //! actual mission content (template)
  ArcadeTemplate *_template;

  //! size of empty marker (empty marker must be shown in editor)
  float _sizeEmptyMarker;
public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  \param scaleMin minimal map scale
  \param scaleMax maximal map scale
  \param scaleDefault default map scale
  */
  CStaticMapArcadeViewer
    (
    ControlsContainer *parent, int idc, ParamEntryPar cls,
    float scaleMin, float scaleMax, float scaleDefault
    );

  //! sets actual mission (template)
  void SetTemplate(ArcadeTemplate *templ) {_template = templ;}
  //! returns actual mission (template)
  const ArcadeTemplate *GetTemplate() const {return _template;}

  virtual bool OnKeyDown(int dikCode);
  void OnLButtonDown(float x, float y);

  void Center(bool soft = false);
  void DrawExt(float alpha);

protected:
  //! returns player unit
  AIBrain *GetMyUnit();
  //! returns player's center
  AICenter *GetMyCenter();

  //! returns current editing mode
  virtual InsertMode GetMode();

  void DrawLabel(struct SignInfo &info, PackedColor color, int shadow);
  SignInfo FindSign(float x, float y);

  //! returns if editing is possible
  virtual bool HasFullRights() {return false;}

  //! returns if given editable map object is in selection
  bool IsSelected(const SignInfo &info) const;
  //! toggle selection status of given editable map object
  void InvertSelection(const SignInfo &info);
};

//! access rights for editting of map objects
enum EditRights
{
  ERNone,
  ERGroupWP,
  ERSideWP,
  ERFull
};

class DisplayArcadeMap;
//! Mission editor control
class CStaticMapArcade : public CStaticMapArcadeViewer
{
protected:
  //! access rights
  EditRights _editRights;
  
  //@{
  //! info about last edited group (defaults for next inserted group)
  RString _lastGroupSide;
  RString _lastGroupFaction;
  RString _lastGroupType;
  RString _lastGroupName;
  //@}

public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  \param rights access rights
  \param scaleMin minimal map scale
  \param scaleMax maximal map scale
  \param scaleDefault default map scale
  */
  CStaticMapArcade
    (
    ControlsContainer *parent, int idc, ParamEntryPar cls, EditRights rights,
    float scaleMin, float scaleMax, float scaleDefault
    )
    : CStaticMapArcadeViewer(parent, idc, cls, scaleMin, scaleMax, scaleDefault)
  {
    _editRights = rights;
  }

  //! sets information about last edited group
  void SetLastGroup(RString side, RString faction, RString type, RString name)
  {
    _lastGroupSide = side; _lastGroupFaction = faction;
    _lastGroupType = type; _lastGroupName = name;
  }

  virtual bool OnKeyDown(int dikCode);
  void OnLButtonDown(float x, float y);
  void OnLButtonUp(float x, float y);
  void OnLButtonClick(float x, float y);
  void OnLButtonDblClick(float x, float y);
  void OnMouseHold(float x, float y, bool active = true);

  void DrawExt(float alpha);

  virtual void ProcessCheats();

  void UpdateMissionName(bool edited);

protected:
  //! delete selected editable objects
  void ClipboardDelete();
  //! copy selected editable objects to clipboard
  void ClipboardCopy();
  //! cut selected editable objects to clipboard
  void ClipboardCut();
  //! paste editable objects from clipboard (position depends on mouse cursor position)
  void ClipboardPaste();
  //! paste editable objects from clipboard (position is the same as original position)
  void ClipboardPasteAbsolute();

  InsertMode GetMode();
  //! returs info about last edited unit (defaults for next inserted unit)
  ArcadeUnitInfo *GetLastUnit();
  ArcadeUnitInfo *GetLastLogic();

  virtual bool HasFullRights() {return _editRights == ERFull;}
  //! returns if user can change waypoints of given group
  bool HasRight(int ig);
};

//! Waypoint properties display
class DisplayArcadeWaypoint : public Display
{
public:
  //@{
  //! waypoint identification
  int _indexGroup;
  int _index;
  //@}
  //! waypoint parameters
  ArcadeWaypointInfo _waypoint;

  //! whole mission template
  ArcadeTemplate *_template;

public:
  //! constructor
  /*!
  \param parent parent display
  \param templ whole mission template (order of waypoint can changed)
  \param indexGroup waypoint identification
  \param index waypoint identification
  \param waypoint waypoint parameters
  */
  DisplayArcadeWaypoint
    (
    ControlsContainer *parent, ArcadeTemplate *templ,
    int indexGroup, int index, ArcadeWaypointInfo &waypoint
    );
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);

  void OnButtonClicked(int idc);
  void OnChildDestroyed(int idd, int exit);

  bool CanDestroy();
  void Destroy();
};

//! Mission editor display
class DisplayArcadeMap : public DisplayMapEditor
{
public:
  //! Mission editor control
  CStaticMapArcade *_map;

  //! mission content (template)
  ArcadeTemplate _templateMission;
  //! intro content (template)
  ArcadeTemplate _templateIntro;
  //! outro content (template)
  ArcadeTemplate _templateOutroWin;
  //! outro content (template)
  ArcadeTemplate _templateOutroLoose;

  //! currently selected template
  InitPtr<ArcadeTemplate> _currentTemplate;

  //! current editing mode
  InsertMode _mode;
  //! info about last edited unit (defaults for next inserted unit)
  ArcadeUnitInfo _lastUnit;
  //! info about last edited logic (defaults for next inserted logic)
  ArcadeUnitInfo _lastLogic;

  //! editor called from server display
  bool _multiplayer;
  //! preview is running
  bool _running;
  //! change base subdirectory
  bool _setDirectory;
  //! button Continue can be enabled
  bool _canContinue;

  /// Copy of GWorld->CameraOn() for "Continue"
  OLinkObject _backupCamera;
  /// Copy of GWorld->PlayerOn() for "Continue"
  OLinkPermO(Person) _backupPlayer;
  /// Copy of GWorld->GetRealPlayer() for "Continue"
  OLinkPermO(Person) _backupRealPlayer;

public:
  //! constructor
  /*!
  \param parent parent display
  \param multiplayer editor called from server display
  */
  DisplayArcadeMap(ControlsContainer *parent, bool multiplayer = false, bool setDirectory = true);

  WeatherState GetWeather();
  Clock GetTime();
  bool GetPosition(Point3 &pos);
  CStaticMap *GetMap() {return _map;}

  void OnChildDestroyed(int idd, int exit);
  void OnHide();

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnButtonClicked(int idc);
  void OnLBSelChanged(IControl *ctrl, int curSel);
  void OnToolBoxSelChanged(int idc, int curSel);
  bool OnUnregisteredAddonUsed( RString addon );
  void OnSimulate(EntityAI *vehicle);

  bool CanDestroy();
  void Destroy();

  //! update buttons state
  void ShowButtons();

  // shows mission name and marks if mission is to be saved
  void UpdateMissionName(bool edited);

protected:
  //! serialize all templates
  LSError SerializeAll(ParamArchive &ar, bool merge = false);
  //! load all templates from given sqm file
  bool LoadTemplates(const char *filename);
  //! merge all templates with given sqm file
  bool MergeTemplates(const char *filename);
  //! save all templates into given sqm file
  bool SaveTemplates(const char *filename);

  //@{
  //! update buttons state
  void UpdateIdsButton();
  void UpdateTexturesButton();
  //@}

  /// report missing addons after mission load
  void ReportMissingAddons();
};

#endif // #if _ENABLE_EDITOR

// Mission, intro, outro, campaign intro displays

//! "Fake" display used when mission is running
/*!
No display. Game is running "in background".
Waits for special user input (ESCAPE) or game over.
*/
class DisplayMission : public Display
{
protected:
  //! if mission is called from missin editor (preview)
  bool _editor;

  UserFileErrorInfo _error;

#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER >= 200
  // Statistics handled by the NetworkManager
# else
  SRef<LiveStatsUpdate> _statsBegin;
# endif // _XBOX_VER >= 200
#endif

  enum SubdialogType
  {
    STNone,
    STMissionEnd,
    STInterrupt,
    STMissionFail,
  };

  //! child dialog for preload
  SubdialogType _wantedDialog;
  Ref<ControlsContainer> _wantedChild;
  RString _wantedResource;
#if defined _XBOX && _XBOX_VER >= 200
  /// Ensure the save game is accessible while checking for file time
  XSaveGame _saveHandle;
#endif
  Future<QFileTime> _saveTimeHandles[NSaveGameType];

public:
  //! constructor
  /*!
  \param parent parent display
  \param editor if mission is called from missin editor (preview)
  \param erase erase save and autosave file
  \param load mission is created by loading of fps file
  */
  DisplayMission(ControlsContainer *parent, bool editor = false, bool erase = true, bool initMap = false);
  //! destructor
  ~DisplayMission();

  ControlObject *OnCreateObject(int type, int idc, ParamEntryPar cls);
  void OnSimulate(EntityAI *vehicle);
  void OnChildDestroyed(int idd, int exit);
  void OnButtonClicked(int idc) {}
  bool OnKeyDown(int dikCode) {return false;}
  bool OnKeyUp(int dikCode) {return false;}

  //! create hint dialog (HintC function)
  /*!
  \param hint displayed string
  */
  void ShowHint(RString hint);
  //! create extended hint dialog (HintC function)
  /*!
  \param title hint title
  \param hints hint texts
  */
  void ShowHint(RString title, StaticArrayAuto< RefR<INode> > &hints);

  //! restart mission from beginning
  void DoRestartMission();

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  /// handle invitation
  virtual void OnInvited();
#endif

  //! retry mission from autosave or beginning
  bool RetryMission();

protected:
  //! sets InGameUI parameters
  void InitUI();
  //! sets DisplayMap parameters
  void InitMap();
  //! restart mission from beginning
  bool RestartMission();
  //! aborts mission
  void AbortMission();

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  /// handles request
  virtual void HandleRequest(RequestType request);
#endif


};

//! "Fake" display used when cutscene is performed
/*!
No display. Game is running "in background". Waits for user input.
*/
class DisplayCutscene : public Display
{
protected:
  bool _credits;

public:
  //! constructor
  /*!
  \param parent parent display
  */
  DisplayCutscene(ControlsContainer *parent);
  //! destructor
  ~DisplayCutscene();

  void OnSimulate(EntityAI *vehicle);
  void OnButtonClicked(int idc) {}
  bool OnKeyDown(int dikCode) {return false;}
  bool OnKeyUp(int dikCode) {return false;}

  void OnChildDestroyed(int idd, int exit);

  /// no default implementation
  virtual void PlayAgain() = 0;

  //@{ PlayAgain helpers for derived classes
  bool PlayAgainInit();
  void PlayAgainEnd();
  //@}

};

//! "Fake" display used when intro cutscene is performed
/*!
No display. Game is running "in background". Waits for user input.
*/
class DisplayIntro : public DisplayCutscene
{
public:
  enum NoInit {noInit};
  //! constructor
  /*!
  No cutscene is started.
  \param parent parent display
  */
  DisplayIntro(ControlsContainer *parent, NoInit, bool credits);
  //! constructor
  /*!
  Current cutscene is started.
  \param parent parent display
  */
  DisplayIntro(ControlsContainer *parent);
  //! constructor
  /*!
  Given cutscene is started.
  \param parent parent display
  \param cutscene name of mission which intro is started
  */
  DisplayIntro(ControlsContainer *parent, RString cutscene);
  ~DisplayIntro();
  //! Initialize game UI (create map etc.)
  void Init();

  void PlayAgain();
};

bool LaunchIntro(Display *parent, bool credits = false);
bool LaunchIntro(Display *parent, RString cutscene, bool credits = false);

RString GetSaveDirectory();
RString GetBriefingFile();

///////////////////////////////////////////////////////////////////////////////
// Mission editor subdisplays

#if _ENABLE_EDITOR

//! Control for interactive editation of azimuth
class CStaticAzimut : public CStatic
{
protected:
  //! id of attached edit control
  int _idcEdit;

public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  \param idcEdit id of attached edit control
  */
  CStaticAzimut(ControlsContainer *parent, int idc, ParamEntryPar cls, int idcEdit)
    : CStatic(parent, idc, cls) {_enabled = true; _idcEdit = idcEdit;}

  virtual void OnDraw(UIViewport *vp, float alpha);
  void OnLButtonClick(float x, float y);
};

//! Unit properties display
class DisplayArcadeUnit : public Display
{
  typedef Display base;
public:
  //@{
  //! unit identification
  int _indexGroup;
  int _index;
  //@}

  //! unit parameters
  ArcadeUnitInfo _unit;
  //! whole mission template
  ArcadeTemplate *_template;

public:
  //! constructor
  /*!
  \param parent parent display
  \param indexGroup unit identification
  \param index unit identification
  \param unit unit parameters
  \param t whole mission template
  */
  DisplayArcadeUnit
    (
    ControlsContainer *parent,
    int indexGroup, int index,
    ArcadeUnitInfo &unit,
    ArcadeTemplate *t
    )
    : base(parent)
  {
    _enableSimulation = false;
    _enableDisplay = false;

    _indexGroup = indexGroup;
    _index = index;
    _unit = unit;
    _unit.module = false;
    _template = t;
    if (_unit.side == TEmpty)
    {
      _unit.player = APNonplayable;
    }
    Load("RscDisplayArcadeUnit");
  }

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnLBSelChanged(IControl *ctrl, int curSel);
  bool CanDestroy();
  void Destroy();

  //! update list of factions
  /*!
  \param combo updated control
  \param vehicle current vehicle name
  */
  void UpdateFactions(CCombo *combo, const char *vehicle);
  //! update list of vehicle classes
  /*!
  \param combo updated control
  \param vehicle current vehicle name
  */
  void UpdateClasses(CCombo *combo, const char *vehicle);
  //! update list of vehicles
  /*!
  \param combo updated control
  \param vehicle current vehicle name
  */
  void UpdateVehicles(CCombo *combo, const char *vehicle);
  //! update list of playable states
  /*!
  \param combo updated control
  \param player current playable state
  */
  void UpdatePlayer(CCombo *combo, ArcadeUnitPlayer player);
};

//! Unit properties display
class DisplayArcadeModules : public Display
{
  typedef Display base;
public:
  //@{
  //! unit identification
  int _indexGroup;
  int _index;
  //@}

  //! unit parameters
  ArcadeUnitInfo _unit;
  //! whole mission template
  ArcadeTemplate *_template;

public:
  //! constructor
  /*!
  \param parent parent display
  \param indexGroup unit identification
  \param index unit identification
  \param unit unit parameters
  \param t whole mission template
  */
  DisplayArcadeModules
    (
    ControlsContainer *parent,
    int indexGroup, int index,
    ArcadeUnitInfo &unit,
    ArcadeTemplate *t
    )
    : base(parent)
  {
    _enableSimulation = false;
    _enableDisplay = false;

    _indexGroup = indexGroup;
    _index = index;
    _unit = unit;
    _template = t;

    _unit.player = APNonplayable;
    _unit.side = TLogic;
    _unit.module = true;

    Load("RscDisplayArcadeModules");
  }

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnLBSelChanged(IControl *ctrl, int curSel);
  bool CanDestroy();
  void Destroy();

  //! update list of factions
  /*!
  \param combo updated control
  \param vehicle current vehicle name
  */
  void UpdateFactions(CCombo *combo, const char *vehicle);
  //! update list of vehicle classes
  /*!
  \param combo updated control
  \param vehicle current vehicle name
  */
  void UpdateClasses(CCombo *combo, const char *vehicle);
  //! update list of vehicles
  /*!
  \param combo updated control
  \param vehicle current vehicle name
  */
  void UpdateVehicles(CCombo *combo, const char *vehicle);
  //! update list of playable states
  /*!
  \param combo updated control
  \param player current playable state
  */
  void UpdatePlayer(CCombo *combo, ArcadeUnitPlayer player);
};

//! Group properties display
class DisplayArcadeGroup : public Display
{
public:
  //! insert position
  Vector3 _position;
  //! initial azimuth
  float _azimut;

public:
  //! constructor
  /*!
  \param parent parent display
  \param position insert position
  \param side initial (default) side
  \param type initial (default) type
  \param name initial (default) name
  \param azimuth initial (default) azimuth
  */
  DisplayArcadeGroup
    (
    ControlsContainer *parent, Vector3Par position,
    RString side, RString faction,RString type, RString name, float azimut
    );

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnLBSelChanged(IControl *ctrl, int curSel);
  //! update list of group faction (if side changes)
  void UpdateFactions();
  //! update list of group types (if side changes)
  void UpdateTypes(); 
  //! update list of group names (if type changes)
  void UpdateNames(); 
};

//! Marker properties display
class DisplayArcadeMarker : public Display
{
public:
  //! marker identification
  int _index;
  //! marker properties
  ArcadeMarkerInfo _marker;

  //! whole mission template
  ArcadeTemplate *_template;

protected:
  //! previous marker type (if type changes, size recalculations can be made)
  int _oldType;

public:
  //! constructor
  /*!
  \param parent parent display
  \param index marker identification
  \param marker marker parameters
  \param templ whole mission template
  */
  DisplayArcadeMarker
    (
    ControlsContainer *parent,
    int index,
    ArcadeMarkerInfo &marker,
    ArcadeTemplate *templ
    )
    : Display(parent)
  {
    _enableSimulation = false;
    _enableDisplay = false;

    _index = index;
    _marker = marker;
    _template = templ;

    _oldType = marker.markerType;

    Load("RscDisplayArcadeMarker");

    ChangeType(marker.markerType);
  }

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnToolBoxSelChanged(int idc, int curSel);

  bool CanDestroy();
  void Destroy();

protected:
  //! change type of marker (some display updates and size recalculations must be done)
  void ChangeType(int curSel);
};

//! Sensor properties display
class DisplayArcadeSensor : public Display
{
public:
  //@{
  //! sensor identification
  int _ig;
  int _index;
  //@}
  //! sensor properties
  ArcadeSensorInfo _sensor;
  //! whole mission template
  ArcadeTemplate *_t;

public:
  //! constructor
  /*!
  \param parent parent display
  \param ig sensor identification
  \param index sensor identification
  \param sensor sensor parameters
  \param t whole mission template
  */
  DisplayArcadeSensor
    (
    ControlsContainer *parent,
    int ig, int index,
    ArcadeSensorInfo &sensor,
    ArcadeTemplate *t
    )
    : Display(parent)
  {
    _enableSimulation = false;
    _enableDisplay = false;
    _ig = ig;
    _index = index;
    _sensor = sensor;
    _t = t;

    Load("RscDisplayArcadeSensor");
  }

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);

  void OnButtonClicked(int idc);
  void OnChildDestroyed(int idd, int exit);

  bool CanDestroy();
  void Destroy();
};

//! Effects (on sensor or waypoint) properties display
class DisplayArcadeEffects : public Display
{
public:
  //! effects properties
  ArcadeEffects _effects;

public:
  //! constructor
  /*!
  \param parent parent display
  \param effects effects parameters
  */
  DisplayArcadeEffects(ControlsContainer *parent, ArcadeEffects effects);
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnLBSelChanged(IControl *ctrl, int curSel);

  void Destroy();

protected:
  //! change type of title effects (display update is performed)
  void ChangeTitleType(int type);
};

//! Mission (template) save display
class DisplayTemplateSave : public Display
{
public:
  //! constructor
  /*!
  \param parent parent display
  */
  DisplayTemplateSave(ControlsContainer *parent)
    : Display(parent)
  {
    _enableSimulation = false;
    _enableDisplay = false;
    Load("RscDisplayTemplateSave");
  }

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
};

//! Mission (template) load display
class DisplayTemplateLoad : public Display
{
public:
  //! merge operation will be performed
  bool _merge;

public:
  //! constructor
  /*!
  \param parent parent display
  \param merge merge operation will be performed
  */
  DisplayTemplateLoad(ControlsContainer *parent, bool merge = false)
    : Display(parent)
  {
    _enableSimulation = false;
    _enableDisplay = false;
    _merge = merge;
    Load("RscDisplayTemplateLoad");
    OnIslandChanged();
  }

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnLBSelChanged(IControl *ctrl, int curSel);

protected:
  //! Display updates when island changes (list of missions must be updated)
  void OnIslandChanged();
};

//! Mission intel properties
class DisplayIntel : public Display
{
public:
  //! Intel parameters
  ArcadeIntel _intel;

public:
  //! constructor
  /*!
  \param parent parent display
  \param intel intel parameters
  */
  DisplayIntel(ControlsContainer *parent, ArcadeIntel &intel)
    : Display(parent)
  {
    _enableSimulation = false;
    _enableDisplay = false;

    _intel = intel;
    Load("RscDisplayIntel");
  }

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void Destroy();

  void OnLBSelChanged(IControl *ctrl, int curSel);

  //! updates list of days (if month changes)
  /*!
  \param combo updated control
  \param day currently selected day
  */
  void UpdateDays(CCombo *combo, int day = -1);
};

#endif // #if _ENABLE_EDITOR

struct ArtilleryTargetInfo 
{
  Vector3 start;
  Vector3 end;
  float eta;
  Time time;
  float spread;
};
TypeIsSimple(ArtilleryTargetInfo);

/// Team switch display
class ArtilleryStaticMapMain;
class DisplayArtilleryComputer : public Display
{

private:

  ArtilleryStaticMapMain *_map;

  InitPtr<CStatic> _name;
  InitPtr<CStatic> _id;
  InitPtr<CStatic> _grid;
  InitPtr<CStatic> _distance;
  InitPtr<CStatic> _minRange;
  InitPtr<CStatic> _maxRange;
  InitPtr<CStatic> _direction;
  InitPtr<CStatic> _altitude;
  InitPtr<CCombo> _mode;
  InitPtr<CStatic> _ammo;
  InitPtr<CStatic> _shells;
  InitPtr<CStatic> _spread;
  InitPtr<CStatic> _ETA;
  InitPtr<CStatic> _ammoCount;
  InitPtr<CStatic> _warning;

  float _minRangeValue, _maxRangeValue;
  /// number of instances
  static int _running;
  Transport *_transport;
  float _targetETA, _targetSpread;

public:
  DisplayArtilleryComputer(ControlsContainer *parent, bool userDialog);
  ~DisplayArtilleryComputer();

  bool GetWeaponRanges(TurretContext context, float &minRange, float &maxRange, float &defaultTime);

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnButtonClicked(int idc);
  void OnSimulate(EntityAI *vehicle);

  void SetGrid(Vector3 pos);
  void SetETA(float time, float radius);

  static bool IsRunning() {return _running > 0;}
  void DestroyHUD(int exit);

  void OnLBSelChanged(IControl *ctrl, int curSel);
  
};

class ArtilleryStaticMapMain: public CStaticMapMain
{
private:
  Transport *_transport;
  float _minRange, _maxRange, _defaultTime;
  DisplayArtilleryComputer *_artilleryComputer;

  AutoArray<ArtilleryTargetInfo> _targetinfo;

public:
  //! constructor
  /*!
  Used when control is created by resource template.
  \param parent control container by which this control is owned
  \param idc id of control
  \param cls resource template
  */
  ArtilleryStaticMapMain(DisplayArtilleryComputer *parent, int idc, ParamEntryPar cls, Transport *transport, float minRange, float maxRange, float defaultTime);

  virtual void DrawExt(float alpha);
  void OnLButtonClick(float x, float y);
  void OnMouseMove(float x, float y, bool active = true );
  void AddArtilleryTargetInfo(ArtilleryTargetInfo atinfo);
  void SetRanges(float minRange, float maxRange, float defaultTime);
  void SetArtilleTarget(Vector3 position);
};

#endif
