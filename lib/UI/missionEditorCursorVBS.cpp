#include "../wpch.hpp"

#if _ENABLE_EDITOR2 && _VBS2

#include "missionEditorVBS.hpp"
#include "../world.hpp"
#include "missionEditorCursorVBS.hpp"
#include "../landscape.hpp"
#include "../keyInput.hpp"
#include "../dikCodes.h"
#include "../camera.hpp"
#include "../fileLocator.hpp"
#include <Es/Common/win.h> // GetKeyState

const float DistNearObject = 5;

const EditorProxy *MissionEditCursorContainer::FindProxy(Vector3Par pos, GameState *gstate) const
{
  float minDist2 = Square(DistNearObject);
  const EditorProxy *best = NULL;
  for (int i=0; i<_proxies.Size(); i++)
  {
    EditorObject *edObj = _proxies[i].edObj;
    if (edObj && 
      (
        !edObj->IsVisibleIn3D() || 
        !edObj->IsObjInCurrentOverlay() ||
        edObj->GetScope() == EOSAllNoSelect
      )
    ) continue;

    Object *obj = _proxies[i].object;
    if (!obj) continue;
    float dist2 = pos.Distance2(obj->Position());
    if (dist2 < minDist2)
    {
      minDist2 = dist2;
      best = &(_proxies[i]);
    }
  }
  return best;
}

const EditorProxy *MissionEditCursorContainer::FindProxy(Object *obj, bool onlyVisibleIn3D) const
{
  for (int i=0; i<_proxies.Size(); i++)
  {
    const EditorProxy &proxy = _proxies[i];
    if (
      proxy.object == obj && 
      proxy.edObj && 
      proxy.edObj->GetScope() != EOSAllNoSelect &&
      (proxy.edObj->IsVisibleIn3D() || !onlyVisibleIn3D) &&
      proxy.edObj->IsObjInCurrentOverlay()
    )
      return &proxy;
  }
  return NULL;
};

void MissionEditCursorContainer::AddProxy(Object *object,EditorObject *edObj)
{
  if (edObj) 
  {
    int index = _proxies.Add();
    _proxies[index].edObj = edObj;
    _proxies[index].object = object;
    _proxies[index].id = edObj->GetArgument("VARIABLE_NAME");
    _proxies[index].proxyVisibleInPreview = edObj->GetType()->ProxyVisibleInPreview();
  }
}

void MissionEditCursorContainer::UpdateProxy(Object *object,EditorObject *edObj)
{
  for (int i=0; i<_proxies.Size(); i++)
  {
    if (_proxies[i].edObj == edObj)
    {
      if (edObj) 
      {
        _proxies[i].id = edObj->GetArgument("VARIABLE_NAME"); 
        _proxies[i].proxyVisibleInPreview = edObj->GetType()->ProxyVisibleInPreview();
      }
      _proxies[i].object = object; 
      return;
    }
  }
  // proxy doesn't exist, add a new one
  AddProxy(object,edObj);
}

void MissionEditCursorContainer::DeleteProxy(EditorObject *edObj)
{
  for (int i=0; i<_proxies.Size(); i++)
  {
    if (_proxies[i].edObj == edObj)
      _proxies.Delete(i--);
  }
}

MissionEditorCamera::MissionEditorCamera(MissionEditCursorContainer *parent)
: base(VehicleTypes.New("camera"))
{
  Assert(parent);
  _parent = parent;

  _resetTargets = false;
  _dragging = false;
  _lockToTarget = false;
  _isMoving = false;
  _isRotating = false;
  _allowMoveOnEdge = false;

  _maxSpeed = MAX_CAMERA_SPEED;

  // read settings from config
  ParamEntryVal wrapper = ::Pars >> "CfgEditCamera";

  _speedFwdBack = wrapper >> "speedFwdBack";
  _speedLeftRight = wrapper >> "speedLeftRight";
  _speedUpDown = wrapper >> "speedUpDown";
  _speedRotate = wrapper >> "speedRotate";
  _speedElevation = wrapper >> "speedElevation";
  _speedTurboMultiplier = wrapper >> "speedTurboMultiplier";

  _selectedCursor = GlobLoadTexture(GetPictureName(wrapper >> "iconSelect"));
  _selectedCursorWidth = wrapper >> "iconSelectSizeX";
  _selectedCursorHeight = wrapper >> "iconSelectSizeY";
  _color = GetPackedColor(wrapper >> "iconSelectColor");

  _icon2D = GlobLoadTexture(GetPictureName(wrapper >> "icon2D"));
  _icon2Dh = wrapper >> "icon2Dh";
  _icon2Dw = wrapper >> "icon2Dw";
  _icon2Dcolor = GetPackedColor(wrapper >> "icon2Dcolor");

  SetFocus(0,0);
  Commit(0);
}

MissionEditorCamera::~MissionEditorCamera()
{
}

void MissionEditorCamera::ResetTargets()
{
  Detach();
  _target._target = NULL;
  _target._pos = VZero;
  _oldTarget = NULL;
  _lastTgtPos = _target._pos;
  _movePos = VZero;
  _resetTargets = false;
  _lockToTarget = false;
}

bool MissionEditorCamera::GetGroundIntercept(Vector3& isect, bool groundOnly, bool subSurface, bool camOnly)
{
  return GetGroundIntercept(isect, GInput.cursorX, GInput.cursorY, groundOnly, subSurface, camOnly);
}

bool MissionEditorCamera::GetGroundIntercept(Vector3& isect, float x, float y, bool groundOnly, bool subSurface, bool camOnly)
{
  AspectSettings _aspect;
  GEngine->GetAspectSettings(_aspect);
  x /= _aspect.leftFOV;

  Camera& camera = *GScene->GetCamera();
  float posX = x * camera.Left();
  float posY = -y * camera.Top();

  Vector3 cPos = Vector3(posX, posY, 1);
#if _VBS3 
  cPos.Normalize();
#endif
  Vector3 cursorPos = camera.PositionModelToWorld(cPos);
  Vector3 cursorDir = camera.DirectionModelToWorld(cPos);

  //only interested in CamPosition
  if(camOnly)
  {
    isect = cursorDir;
    return false;
  }

  float maxDist = HORIZONT_Z;
  float dist = subSurface ? GLandscape->IntersectWithGround(&isect,cursorPos,cursorDir,0,maxDist) 
    : GLandscape->IntersectWithGroundOrSea(&isect, cursorPos,cursorDir,0,maxDist);

  //  Vector3 isect = subSurface ? GLandscape->IntersectWithGround(cursorPos,cursorDir,0,maxDist) : GLandscape->IntersectWithGroundOrSea(cursorPos,cursorDir,0,maxDist);

  if (groundOnly) 
  {
    if(dist >= maxDist)
    {
      isect = cursorDir;
      return false;
    }
    return true;
  }
  CollisionBuffer collision;
  GLandscape->ObjectCollision(collision,this,NULL,Position(),isect,0,ObjIntersectGeom,ObjIntersectView);

  if( collision.Size()>0  )
  {
    int nIgnoreObjs = _ignoreObjects.Size();
    float minT=1e10;
    for( int i=0; i<collision.Size(); i++ )
    {
      // info.pos is relative to object
      CollisionInfo &info=collision[i];

      if( info.object )
      {
        if( minT>info.under )
        {
          if (nIgnoreObjs > 0)
          {
            if (_ignoreObjects.Find(info.object) >= 0)
              continue;
          }
          minT=info.under;
          dist = minT;
          isect = info.pos; 
        }
      }      
    }
  }
  if(dist >= maxDist)
  {
    isect = cursorDir;
    return false;
  }
  return true;
}

Vector3 MissionEditorCamera::GetCursorDir()
{
  AspectSettings _aspect;
  GEngine->GetAspectSettings(_aspect);

  Camera& camera = *GScene->GetCamera();
  float posX = GInput.cursorX /_aspect.leftFOV * camera.Left();
  float posY = -GInput.cursorY * camera.Top();

  Vector3 cPos = Vector3(posX, posY, 1);
  Vector3 cursorPos = camera.PositionModelToWorld(cPos);
  return camera.DirectionModelToWorld(cPos);
}

float MissionEditorCamera::MaxSpeed() 
{
  float maxSpeed = _maxSpeed;
  if (GInput.GetAction(UABuldTurbo) > 0.5f)
    maxSpeed *= _speedTurboMultiplier;
  else if (GInput.keys[DIK_LCONTROL]>0)
    maxSpeed /= _speedTurboMultiplier;
  return maxSpeed;
}

void MissionEditorCamera::Simulate(float deltaT, SimulationImportance prec, GameState *gstate, bool cursorOverCamera)
{
  // allow access to camera effects (lock onto object etc)
  base::Simulate(deltaT,prec);

  if (_resetTargets)
    ResetTargets();

  if (deltaT == 0)
    return;

  bool IsAppPaused();
  if (IsAppPaused()) return;

  // camera controlled by external script?
  if ((_target._target || _target._pos != VZero) && !_lockToTarget) return; // TODO: will need to be improved

  Vector3 position = Position();  

  // turn when mouse is at screen edge, rather than move
  bool turnOnEdge = false;

  // move when mouse is at screen edge
  bool moveOnEdge = true;

  float headingChange = 0;
  float diveChange = 0;

  float speedX = (GInput.GetAction(UABuldMoveRight) - GInput.GetAction(UABuldMoveLeft)) * deltaT*Input::MouseRangeFactor;
  float speedY = (GInput.GetAction(UABuldMoveForward) - GInput.GetAction(UABuldMoveBack)) * deltaT*Input::MouseRangeFactor;  

  // rotate camera?
  if (_isRotating && ((!GInput.keys[DIK_LALT] && !GInput.keys[DIK_LSHIFT]) || _isMoving))
  {
    _dragging = true;
    moveOnEdge = false;

    float maxSpeed = 1;
    //if (GInput.GetAction(UABuldTurbo) > 0.5f)
      //maxSpeed *= _speedTurboMultiplier;

    // change camera orientation
    headingChange -= speedX * _speedRotate * maxSpeed * GWorld->GetAcceleratedTime();
    diveChange -= speedY * _speedElevation * maxSpeed * GWorld->GetAcceleratedTime();
    
    /*
    // smooth movement based on mouse position
    if (fabs(GInput.cursorX) > 0.2)
      _heading -= GInput.cursorX * deltaT * 2;

    if (fabs(GInput.cursorY) > 0.2)
      _dive += GInput.cursorY * deltaT * 2;
    */ 
  }

  // user dragging some units?
  //if (_parent->IsDragging())
    //turnOnEdge = true;

  if (turnOnEdge || _dragging || (_allowMoveOnEdge && moveOnEdge))
  {
    // move on screen edges
    AspectSettings _aspect;
    GEngine->GetAspectSettings(_aspect);

    float mouseX = 0.5 + GInput.cursorX/ _aspect.leftFOV * 0.5;
    float mouseY = 0.5 + GInput.cursorY * 0.5;

    saturate(mouseX,0,1);
    saturate(mouseY,0,1);

    float x = 0;
    float y = 0;

    // distance from edge to trigger a move
    float dis = 0.1;
    float max = exp(dis * 100.0f);

    float dif = dis - mouseX;
    if (dif > 0)
      x = -exp(dif * 100.0f) / max;
    else
    {
      dif = mouseX - (1 - dis);
      if (dif > 0)
        x = exp(dif * 100.0f) / max;
    }

    dif = dis - mouseY;
    if (dif > 0)
      y = exp(dif * 100.0f) / max;
    else
    {
      dif = mouseY - (1 - dis);
      if (dif > 0)
        y = -exp(dif * 100.0f) / max;
    }

    // wait for mouse to re-enter clear space before allowing another screen edge detection
    if (turnOnEdge)// || GInput.keys[DIK_LCONTROL])
    {
      float maxSpeed = 1;
      if (GInput.GetAction(UABuldTurbo) > 0.5f)
        maxSpeed *= _speedTurboMultiplier;

      headingChange -= x * _speedRotate * maxSpeed * deltaT;
      diveChange -= y * _speedElevation * maxSpeed * deltaT;
    }
    else
    {
      if (_dragging && x == 0 && y == 0)
        _dragging = false;   

      if (!_dragging)
      {
        float maxSpeed = 30;
        if (GInput.GetAction(UABuldTurbo) > 0.5f)
          maxSpeed *= _speedTurboMultiplier;

        Matrix3 speedOrient;
        speedOrient.SetUpAndDirection(VUp, Direction());
        Vector3 speedWanted = speedOrient * Vector3(x * _speedLeftRight,0,y * _speedFwdBack);
        position += speedWanted * maxSpeed * deltaT;
      }
    }
  }

  //-!

  // keyboard movement
  // 
  if (GInput.keys[DIK_LBRACKET])
  {
    GlobalShowMessage(2000,"Camera Speed: %.1f",_maxSpeed);
    if (_maxSpeed > 1) _maxSpeed -= 0.5;
  }
  else if (GInput.keys[DIK_RBRACKET])
  {
    GlobalShowMessage(2000,"Camera Speed: %.1f",_maxSpeed);
    _maxSpeed += 0.5;
  }

  // heading / dive by keyboard
  float headSpeed = 0;//(GInput.keys[DIK_NUMPAD4] - GInput.keys[DIK_NUMPAD6]) + (GInput.keys[DIK_X] - GInput.keys[DIK_C]);
  float diveSpeed = 0;//GInput.keys[DIK_NUMPAD2] - GInput.keys[DIK_NUMPAD8];

  headingChange += headSpeed*2*deltaT;
  diveChange += diveSpeed*2*deltaT;
  
  // up/down
  float cursorZ = cursorOverCamera ? GInput.cursorMovedZ : 0;
  float up = (GInput.GetAction(UAHeliUp) - GInput.GetAction(UAHeliDown) + cursorZ) * deltaT * _speedUpDown * MaxSpeed();

  // fwd/back/left/right
  float forward =
  (
    //(GInput.GetAction(UABuldMoveForward)-GInput.GetAction(UABuldMoveBack)) +  // this is moving mouse forward and back
    (GInput.GetAction(UAMoveForward)-GInput.GetAction(UAMoveBack))
  ) * _speedFwdBack;

  float aside =
  (
    //(GInput.GetAction(UABuldMoveLeft)-GInput.GetAction(UABuldMoveRight)) +
    (GInput.GetAction(UATurnRight)-GInput.GetAction(UATurnLeft))
  ) * _speedLeftRight;

#if _SPACEMOUSE
  //SpaceMouse
  aside += GInput.spaceMouseAxis[0]*deltaT*10.0;
  forward += GInput.spaceMouseAxis[2]*deltaT*10.0;
  up += GInput.spaceMouseAxis[1]*deltaT*10.0;

  diveChange -= GInput.spaceMouseAxis[3]*deltaT;
  headingChange += GInput.spaceMouseAxis[4]*deltaT;

/*  LogF("SpaceMouse X:%.5f Y:%.5f Z:%.5f rx:%.5f ry:%.5f rz:%.5f  up:%.5f cursorMovedZ:%.5f"
          , GInput.spaceMouseAxis[0]
          , GInput.spaceMouseAxis[1]
          , GInput.spaceMouseAxis[2]
          , GInput.spaceMouseAxis[3]
          , GInput.spaceMouseAxis[4]
          , GInput.spaceMouseAxis[5]
          , up
          , GInput.cursorMovedZ
         );
*/
#endif
  _isMoving = forward != 0 || aside != 0 || up != 0;

  if (_lockToTarget)// && _isMoving)
  {
    if (!_attachedTo)
    {
      UnLock();
      return;
    }
    
    Vector3 dir = _attachedOffset;

    float dRoty = aside * deltaT * MaxSpeed() / MAX_CAMERA_SPEED;
    float dRotH = forward * deltaT * MaxSpeed() / MAX_CAMERA_SPEED;
    float dDist = -up * deltaT * 10 * MaxSpeed() / MAX_CAMERA_SPEED;

    dRoty += headingChange;
    dRotH += diveChange;

    Matrix3 rotYM(MRotationY, dRoty);
    Matrix3 rotHM(MRotationAxis, Orientation().DirectionAside(), dRotH);
    dir = rotYM * dir;
    dir = rotHM * dir;

    dir.Normalize();
    float newDist = _attachedOffset.Size() + dDist;
    saturateMax(newDist, 0.5f);

    _attachedOffset = newDist * dir;
    return;
  }

  //VBS added a factor for higher camera altitude
  float speedHeightFactor =  (Position().Y() - GLandscape->SurfaceYAboveWater(Position().X(), Position().Z())) / 50.0f;
  saturate(speedHeightFactor, 0.2f, 10.0f);
  float maxSpeed = MaxSpeed() * speedHeightFactor;

  Matrix3 speedOrient;
  speedOrient.SetUpAndDirection( VUp, Direction() );
  Vector3 speedWanted = speedOrient * Vector3(aside,0,forward);
  position += speedWanted * maxSpeed * deltaT;
  //-!

  float height = Position().Y() - GLandscape->SurfaceYAboveWater(Position().X(), Position().Z());
  if (height < 0.5 && up < 0) up = 0;
  position[1] = GLandscape->SurfaceYAboveWater(position[0], position[2]) + height + up * speedHeightFactor;

  // final calculation of transformation
  Matrix3 rotY(MRotationY, -headingChange);
  Matrix3 rotX(MRotationX, diveChange);
  Matrix3 rot = Orientation().InverseRotation() * rotY;
  rot = rot.InverseRotation() * rotX;
  
  Matrix4 moveTrans;
  moveTrans.SetOrientation(rot);
  moveTrans.SetPosition(position);
  SetTransform(moveTrans); // finally apply move
  //-!
}

void MissionEditorCamera::UnLock()
{
  ResetTargets();
}

void MissionEditorCamera::LockTo(Object *lockTo)
{
  if (!lockTo) return;
  _lockToTarget = true;
  SetTarget(lockTo); // keeps camera looking at the target
  Commit(0);
  AttachTo(lockTo, _target.PositionAbsToRel(Position()));
}

void MissionEditorCamera::LookAt(Object *target, bool lockCamera, Vector3 pos)
{
  // TODO: put this in the config
  if (pos.SquareSize() == 0)
    pos = Vector3(0,5,-15);

  // set new camera target
  SetTarget(target);
  Commit(0);

  if (lockCamera)
  {
    _lockToTarget = true;
    AttachTo(target, pos);
  }
  else
  {
    Detach();
    const CameraTarget &camTgt = GetTarget();  
    SetPos(camTgt.PositionRelToAbs(pos));
    Commit(0);
    _resetTargets = true; // reset to no target after 1 simulation loop
  }
}

void MissionEditorCamera::LookAt(Vector3Par target)
{
  // TODO: put this in the config
  Vector3 pos(0,5,-15);

  // allow the camera to reposition
  Detach();

  SetTarget(target);
  Commit(0);
  const CameraTarget &camTgt = GetTarget();  
  SetPos(camTgt.PositionRelToAbs(pos));
  Commit(0);
  _resetTargets = true; // reset to no target after 1 simulation loop
}

void MissionEditorCamera::DrawSelectedIcon(Vector3Val pos, PackedColor color)
{
  DrawIcon(pos, _selectedCursor, _selectedCursorWidth, _selectedCursorHeight, color, true);
}

#define MAX_DIS (150 * 150)

bool MissionEditorCamera::DrawIcon(Vector3Val pos, Texture *texture, float width, float height, PackedColor color, bool shadow)
{
  const Camera *camera = GScene->GetCamera();
  Vector3 dir = pos - camera->Position();

  float coef = 0.07;

  Matrix4Val camInvTransform=GetInvTransform();
  Vector3Val dirCam = camInvTransform.Rotate(dir);

  const int w3d = GEngine->Width();
  const int h3d = GEngine->Height();

  float xScreen = 0, yScreen = 0;
  if (dirCam.Z() > 0)
  {
    // crude scale depending on distance - TODO: move into config
    float dist = MAX_DIS - pos.Distance2(camera->Position());
    saturate(dist,1,MAX_DIS);
    width = width / 2 + ((width - width / 2) * dist / MAX_DIS);
    height = height / 2 + ((height - height / 2) * dist / MAX_DIS);

    float invZ= 1/dirCam.Z();
    xScreen = 0.5 * (1.0 + dirCam.X() * invZ * camera->InvLeft() - width * coef);
    yScreen = 0.5 * (1.0 - dirCam.Y() * invZ * camera->InvTop() - height * coef);

    Draw2DPars pars;
    pars.mip = GEngine->TextBank()->UseMipmap(texture,0,0);
    pars.spec = NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
    pars.SetU(0,1);
    pars.SetV(0,1);

    Rect2DAbs rect;
    rect.x = xScreen*w3d;
    rect.y = yScreen*h3d;
    rect.w = width*coef*w3d;
    rect.h = height*coef*h3d;

    // on screen?
    if (rect.x > -rect.w && rect.x < w3d && rect.y > -rect.h && rect.y < h3d)
    {
      // shadow
      if (shadow)
      {
        rect.x += 1;
        rect.y += 1;

        PackedColor colorBlack=PackedColorRGB(PackedBlack, color.A8());
        pars.SetColor(colorBlack);
        GEngine->Draw2D(pars,rect);

        rect.x -= 1;
        rect.y -= 1;
      }

      pars.SetColor(color);
      GEngine->Draw2D(pars,rect);
      return true;
    }
  }
  return false;
}

void MissionEditorCamera::DrawLine(Vector3Val from, Vector3Val to, PackedColor color, bool shadow)
{
  const Camera *camera = GScene->GetCamera();
  Vector3 dirFrom = from - camera->Position();
  Vector3 dirTo = to - camera->Position();

  if (from.Distance2(camera->Position()) > MAX_DIS) return;

  const int w3d = GEngine->Width();
  const int h3d = GEngine->Height();

  Matrix4Val camInvTransform = camera->GetInvTransform();
  Vector3 dirCam;
  float invZ;

  dirCam = camInvTransform.Rotate(dirFrom);
  invZ = 1/dirCam.Z();
  float xScreenFrom = 0.5 * (1.0 + dirCam.X() * invZ * camera->InvLeft()) * w3d;
  float yScreenFrom = 0.5 * (1.0 - dirCam.Y() * invZ * camera->InvTop()) * h3d;

  dirCam = camInvTransform.Rotate(dirTo);
  invZ = 1/dirCam.Z();
  float xScreenTo = 0.5 * (1.0 + dirCam.X() * invZ * camera->InvLeft()) * w3d;
  float yScreenTo = 0.5 * (1.0 - dirCam.Y() * invZ * camera->InvTop()) * h3d;

  Line2DAbs line;
  line.beg = Point2DAbs(xScreenFrom, yScreenFrom);
  line.end = Point2DAbs(xScreenTo, yScreenTo);  
  if (shadow) 
  {
    Line2DAbs lineShadow = line;
    lineShadow.beg.x++;
    lineShadow.beg.y++;
    lineShadow.end.x++;
    lineShadow.end.y++;
    PackedColor colorShadow = PackedBlack;
    GEngine->DrawLine(lineShadow, colorShadow, colorShadow);    
  }
  GEngine->DrawLine(line, color, color);
}

#endif // _ENABLE_EDITOR2
