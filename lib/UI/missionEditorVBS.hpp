/*!
\file
Mission editor functions and classes
*/

#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MISSION_EDITOR_HPP
#define _MISSION_EDITOR_HPP

#if _ENABLE_EDITOR2 && _VBS2

#include <Es/Strings/rString.hpp>
#include <El/Evaluator/expressImpl.hpp>
#include "El/ParamFile/paramFile.hpp"
#include "../objectId.hpp"
#include "../object.hpp"

#define MISSION_VERSION "3"

DEFINE_ENUM_BEG(EditorObjectScope)
	EOSHidden,      // object is invisible (not shown in tree or editor)
  EOSView,        // object is visible but may not be selected
  EOSSelect,      // object is visible and selectable
  EOSLinkTo,      // object may be linked to (but not from)
  EOSLinkFrom,    // object may be linked to and from
  EOSDrag,        // object may be edited and copied but not dragged
  EOSAllNoTree,   // object may be edited and copied but it is not visible in the object tree
  EOSAllNoSelect, // object may be edited and copied, it is visible in the object tree but not selectable in 2D or 3D
  EOSAllNoCopy,   // object may be fully edited but not copied
  EOSAll,         // object may be edited and copied
  NEditorObjectScopes
DEFINE_ENUM_END(EditorObjectScope)

DEFINE_ENUM_BEG(EditorTypePermission)
	ETSNone,
  ETSLink,
  ETSFull,
  NEditorTypePermissions
DEFINE_ENUM_END(EditorTypePermission)

DEFINE_ENUM_BEG(FogOfWar)
	FOWOff,            // disable all fog of war
  FOWAllUnits,       // no fog of war (all units visible)
  FOWAllSides,       // units of the same side are all visible, and units of other sides only if there is LOS
  FOWVisibleUnits,   // only units where there is LOS are visible
  FOWNone,           // map doesn't show any unit markers at all
  NFogOfWar
DEFINE_ENUM_END(FogOfWar)

struct EditorArgument
{
  RString name;
  RString value;
  GameValue evaluated;
};
TypeIsMovableZeroed(EditorArgument)

// TODO: use hash map instead
class EditorArguments : public AutoArray<EditorArgument>
{
private:
  typedef AutoArray<EditorArgument> base;

public:
  int Set(RString name, RString value, bool evaluate);
  RString Get(RString name) const
  {
    for (int i=0; i<Size(); i++)
      if (base::Get(i).name == name) return base::Get(i).value;
    return RString();
  }
  bool Get(EditorArgument &arg) const
  {
    for (int i=0; i<Size(); i++)
      if (base::Get(i).name == arg.name && base::Get(i).value == arg.value) return true;
    return false;
  }
  GameValue GetValue(RString name) const
  {
    for (int i=0; i<Size(); i++)
      if (base::Get(i).name == name) return base::Get(i).evaluated;
    return GameValue();
  }
};

enum EditorParamSource
{
  EPSDialog,
  EPSPosition,
  EPSLink,
  EPSParent,
  EPSId,
  EPSDirection,
  EPSCounter,
  EPSContext
};

struct EditorParam
{
  EditorParamSource source;
  RString name;
  RString type;
  RString subtype;
  RString description;
  RString activeMode;
  bool canChange;
  RString canChangeCondition;
  bool hasDefValue;
  RString defValue;
  /// new / edit dialog handlers
  RString onInit;
  RString onChanged;
  RString valid;
  bool hidden;
  int shortcutKey;
  bool passAsEditorObject;
  // redefined control settings
  float x,y,w,h;
  int idc;

  EditorParam(
    EditorParamSource src, 
    RString n, 
    RString t, 
    RString subt, 
    RString desc, 
    RString actMode, 
    bool canCh,
    RString canChCon,
    bool hasDefVal, 
    RString defVal, 
    bool hide, 
    float key, 
    bool eo,
    float xPos,
    float yPos,
    float width,
    float hgt,
    int i
  )
  {
    source = src;
    name = n;
    type = t;
    subtype = subt;
    description = desc;
    activeMode = actMode;
    canChange = canCh;
    canChangeCondition = canChCon;
    hasDefValue = hasDefVal;
    defValue = defVal;
    hidden = hide;
    shortcutKey = key;
    passAsEditorObject = eo;
    x = xPos; y = yPos; w = width; h = hgt;
    idc = i;
  }
};
TypeIsMovableZeroed(EditorParam)

// TODO: use hash map instead
class EditorParams : public AutoArray<EditorParam>
{
public:
  void Load(ParamEntryPar cfg, RString name);
  const EditorParam *Find(RString name) const
  {
    for (int i=0; i<Size(); i++)
      if (Get(i).name == name) return &Get(i);
    return NULL;
  }
};

class EditorObjectType : public RefCount
{
protected:
  RString _name;
  EditorParams _params;

  AutoArray<RString> _create;
  AutoArray<RString> _update;
  AutoArray<RString> _updatePosition;
  AutoArray<RString> _delete;
  AutoArray<RString> _select;
  RString _position;
  RString _proxy;
  RString _treeFilter;
  RString _displayName;
  RString _displayNameTree;
  // object visible on map?
  RString _visible;
  // execute draw map script for object?
  RString _execDrawMap;
  RString _execDraw3D;
  AutoArray<RString> _drawMap;
  AutoArray<RString> _draw3D;
  int _shortcutKey;
  RString _shortcutKeyDesc;
  int _nextID;
  bool _visibleIfTreeCollapsed;
  bool _contextMenuFollowsProxy;
  bool _proxyVisibleInPreview;
  bool _saveTemplateOnUpdate;
  bool _saveTemplateOnCreate;
  bool _allowMoveIfAttached;
  
  // used with real time editor
  EditorObjectScope _scope;

public:
  EditorObjectType() {}
  EditorObjectType(RString name, RString filename);
  EditorObjectType(RString name, ParamEntryVal entry);
  EditorObjectType(RString name, const EditorObjectType *type);

  RString GetName() const {return _name;}
  const EditorParams &GetParams() const {return _params;}

  // fill object type variables from editor definition file
  void LoadParams(ParamEntryVal &file);

  const AutoArray<RString> &GetCreateScript() const {return _create;}
  const AutoArray<RString> &GetUpdateScript() const {return _update;}
  const AutoArray<RString> &GetUpdatePositionScript() const {return _updatePosition;}
  const AutoArray<RString> &GetSelectScript() const {return _select;}
  const AutoArray<RString> &GetDeleteScript() const {return _delete;}
  void SetCreateScript(AutoArray<RString> create) {_create = create;}
  void SetUpdateScript(AutoArray<RString> update) {_update = update;}
  RString GetPosition() const {return _position;}
  RString GetProxy() const {return _proxy;}
  RString GetDisplayName() const {return _displayName;}
  RString GetDisplayNameTree() const {return _displayNameTree;}
  RString GetTreeFilter() const {return _treeFilter;}
  RString GetVisible() const {return _visible;}  
  RString GetExecDrawMap() const {return _execDrawMap;}  
  RString GetExecDraw3D() const {return _execDraw3D;}  
  RString GetShortcutKeyDesc() const {return _shortcutKeyDesc;}
  int GetShortcutKey() const {return _shortcutKey;}
  const AutoArray<RString> &GetDrawMapScript() const {return _drawMap;}
  const AutoArray<RString> &GetDraw3DScript() const {return _draw3D;}
  
  bool IsVisibleIfTreeCollapsed() const {return _visibleIfTreeCollapsed;}
  bool ContextMenuFollowsProxy() const {return _contextMenuFollowsProxy;}
  bool ProxyVisibleInPreview() const {return _proxyVisibleInPreview;}
  bool SaveTemplateOnUpdate() const {return _saveTemplateOnUpdate;}
  bool SaveTemplateOnCreate() const {return _saveTemplateOnCreate;}
  bool AllowMoveIfAttached() const {return _allowMoveIfAttached;}
  
  EditorObjectScope GetScope() const {return _scope;}
  void SetScope(EditorObjectScope scope) {_scope = scope;}

  RString NextVarName() {return Format("_%s_%d", (const char *)_name, _nextID++);}
  // update _nextID by this object name
  void RegisterObjectName(RString name);
};

struct EditorObjectIcon
{
  Ref<Texture> icon;
  PackedColor color;
  Vector3 offset;
  float width;
  float height;
  float angle;
  bool maintainSize;
  float minScale;   // minimum scale to maintain size (when map is below this scale, icon size will be maintained)
  RString id;
  bool shadow;
  bool line;
  bool is3D;
  float priority;
  RString text;
  bool rotateWithObj;
  EditorObjectIcon() 
  {
    icon = NULL;
    color = PackedWhite;
    offset = VZero;
    width = 1;
    height = 1;
    angle = 0;
    maintainSize = true;
    minScale = -1;
    shadow = true;
    line = true;
    is3D = false;
    priority = 0.0;
    text = RString();
    rotateWithObj = true;
  }
};
TypeIsMovableZeroed(EditorObjectIcon)

class EditorObject;

// editor object overlay
class EditorOverlay : public RefCount
{
private:
  RString _name;
  RString _extension;
  bool _isTemplate; // means that objects will be created only locally
  bool _isActive;

public:
  EditorOverlay(RString name, bool isTemplate, RString extension = "") 
  {
    _name = name; 
    _isTemplate = isTemplate;
    _isActive = false;
    _extension = extension;
  }

  void Save(const RefArray<EditorObject> &objects);  
  RString GetName() const {return _name;}
  void SetName(RString name) {_name = name;}
  RString GetExtension() const {return _extension;}
  bool IsTemplate() {return _isTemplate;}

  bool IsActive() {return _isActive;}
  void SetActive(bool active) {_isActive = active;}
};

class EditorObject : public RefCount // TODO: enable links for dependencies implementation
{
protected:
  Ref<EditorObjectType> _type;

  // custom editor object type for this object (optional)
  Ref<EditorObjectType> _subType;

  // list of all parameters (main type and subtype)
  EditorParams _allParams;

  EditorArguments _args;
  
  bool _isVisibleInTree;  // is the editor object visible in the object tree?
  bool _isVisibleOnMap;   // is the editor object visible on the map?

  bool _isAnchored;       // is the editor object movable?
  bool _stayAboveGround;  // is the editor object to stay above the ground?
  bool _allowMoveIfAttached; // allow the editor object to be moved if attached?

  bool _execDrawMap;      // execute drawMap script for this object?
  bool _execDraw3D;       // execute draw3D script for this object?

  AutoArray<EditorObjectIcon> _icons;
  bool _draw3DLine;

  // pointer to proxy object
  OLink(Object) _proxy;

  // transform for proxy object (used with undo)
  Matrix4 _proxyTransform;

#if _VBS2 // _staticProxy
  // is the proxy object static (ie not a unit or vehicle)?
  bool _staticProxy;
#endif

  // has this editor object been created dynamically, rather than through create scripts?
  bool _isDynamic;

  // drawMap script cache
  mutable RString _drawMapScript;
  mutable CompiledExpression _drawMapScriptCompiled;
  mutable bool _drawMapScriptChanged;  

  // draw3D script cache
  mutable RString _draw3DScript;
  mutable CompiledExpression _draw3DScriptCompiled;
  mutable bool _draw3DScriptChanged;

  // object selected?
  bool _selected;

  // overlay that this object belongs to (if any)
  EditorOverlay *_parentOverlay;
  bool _isInCurrentOverlay;

  // used with real time editor
  EditorObjectScope _scope;
  EditorObjectScope _defaultScope;

  // is the object visible even if tree is collapsed?
  bool _visibleIfTreeCollapsed;

  // evaluated position of the object (for drawing)
  Vector3 _evaluatedPos;

  // number of draw links from this object
  int _nDrawLinks;

  // used with merge
  float _hgt;

  // fow
  bool _doFade;
  UITime _fadeTime;
  Vector3 _fadePos;

  bool _isDeleting;

public:
  EditorObject();
  EditorObject(EditorObjectType *type);

  void SetNDrawLinks(int n) {_nDrawLinks+=n;}
  int GetNDrawLinks() {return _nDrawLinks;}

  // used with merging to ensure correct height is passed through
  void SetHeight(float hgt) {_hgt = hgt;}
  float GetHeight() {return _hgt;}

  // return parent overlay
  void SetParentOverlay(EditorOverlay *overlay, bool isCurrent = false);
  EditorOverlay *GetParentOverlay() {return _parentOverlay;}
  bool IsObjInCurrentOverlay() const {return _isInCurrentOverlay;}

  void SetDeleting() {_isDeleting = true;}
  bool IsDeleting() {return _isDeleting;}

  // 2D and 3D icons
  void AddIcon(EditorObjectIcon icon);
  EditorObjectIcon &GetIcon(int i) {return _icons[i];}
  void RemoveIcon(RString id);
  int NIcons() {return _icons.Size();}
  bool Draw3DLine() {return _draw3DLine;}

  bool IsSelected() {return _selected;}
  void SetSelected(bool selected) {_selected = selected;}

  // object position, pre-evaluated
  void UpdateEvaluatedPos();
  // returns proxy position (if available), otherwise evaluated position
  Vector3 GetEvaluatedPos() const {return _evaluatedPos;}

  // object visibility
  bool IsVisibleInTree() const {return _scope > EOSHidden && _isVisibleInTree;} // object visible in tree?
  void SetVisibleInTree(bool isVisible) {_isVisibleInTree = isVisible;}

  RString GetVisible() const; // object's visibility script (from editor object definition)
  void UpdateVisibleOnMap(GameState *gstate);

  // is the object visible even if tree is collapsed?
  bool IsVisibleIfTreeCollapsed() {return _visibleIfTreeCollapsed;}
  void SetVisibleIfTreeCollapsed(bool v) {_visibleIfTreeCollapsed = v;}

  EditorObjectScope GetScope() const {return _scope;} // editor object scope (may relate to visibility)
  void SetScope(EditorObjectScope scope) {_scope = scope;}

  EditorObjectScope GetDefaultScope() const {return _defaultScope;} // editor object scope (may relate to visibility)
  void SetDefaultScope(EditorObjectScope scope) {_defaultScope = scope;}

  // is the object visible?
  bool IsVisible() const {return (_isVisibleInTree || _visibleIfTreeCollapsed) && _scope > EOSHidden && _isVisibleOnMap;}
  bool IsVisibleIn3D() const {return _scope > EOSHidden && _isVisibleOnMap;}

  bool IsAnchored() {return _isAnchored;}
  void SetAnchored(bool anchored) {_isAnchored = anchored;}

  bool StayAboveGround() {return _stayAboveGround;}
  void SetStayAboveGround(bool ag) {_stayAboveGround = ag;}

  bool AllowMoveIfAttached() {return _allowMoveIfAttached;}
  void SetAllowMoveIfAttached(bool am) {_allowMoveIfAttached = am;}

  // editor object type related
  const EditorObjectType *GetType() const {return _type;}
  EditorObjectType *GetType() {return _type;}

  // subtype related
  const EditorParams &GetAllParams() const {return _allParams;}  
  void ListAllParameters();
  void ClearSubType();
  EditorObjectType *GetSubType() {return _subType;}
  void AssignSubType(const EditorObjectType *subType, RString name);

  // edit object arguments
  int NArguments() const {return _args.Size();}
  const EditorArgument &GetArgument(int i) const {return _args[i];}  
  void RemoveArgument(int i) {_drawMapScriptChanged = _draw3DScriptChanged = true; _args.Delete(i);}
  void SetArguments(const AutoArray<RString> &args);
  int SetArgument(RString name, RString value);
  RString GetArgument(RString name) const {return _args.Get(name);}
  bool HasArgument(EditorArgument &arg) {return _args.Get(arg);}
  GameValue GetArgumentValue(RString name) const {return _args.GetValue(name);}
  void RemoveArgument(RString name);
  // update an arguments value
  bool UpdateArgument(RString name, RString value, bool evaluate, bool updateMapScript = true);
  RString GetParent() const;

  // returns objects execDrawMap script from object type definition
  RString GetExecDrawMap() const;
  void UpdateExecDrawMap(GameState *gstate);
  bool ExecDrawMap() {return _execDrawMap;}

  RString GetExecDraw3D() const;
  void UpdateExecDraw3D(GameState *gstate);
  bool ExecDraw3D() {return _execDraw3D;}

  RString GetTreeFilter() const;

  // parse a single line of code
  RString WriteScript(RString code);

  void WriteCreateScript(QOStream &out);
  void WriteUpdateScript(QOStream &out);
  void WriteUpdatePositionScript(QOStream &out);
  void WriteSelectScript(QOStream &out);
  void WriteDeleteScript(QOStream &out);

  // get position script from editor definition file
  RString GetPosition() const;

  // execute the position script (from editor definition)
  bool GetPosition(GameState *gstate, Vector3 &pos) const;

  // get the actual value of the position attribute
  bool GetEvaluatedPosition(Vector3 &pos, bool isASL = false) const;

  RString GetProxy() const;
  RString GetDisplayName() const;
  RString GetDisplayNameTree() const;

  bool IsDrawMapScript(GameState *state) const;
  void ExecuteDrawMapScript(GameState *state) const;

  bool IsDraw3DScript(GameState *state) const;
  void ExecuteDraw3DScript(GameState *state) const;

  ParamClassPtr Save(ParamEntry &f) const;
  void Load(EditorObjectType *type, ParamEntryPar f);

  // proxy related
  void SetProxyObject(Object *proxy);
  Object *GetProxyObject() const {return _proxy;}
  void FreeProxy() {_proxy = NULL;}

  // used with undo
  void SetProxyTransform(const Matrix4 &trans) {_proxyTransform = trans;}
  Matrix4 &GetProxyTransform() {return _proxyTransform;}
 
  // _staticProxy
  bool IsStaticProxy() {return _staticProxy;}

  bool IsDynamic() {return _isDynamic;}
  void SetDynamic(bool dynamic) {_isDynamic = dynamic;}

  // update position and direction arguments based upon proxy
  void UpdateAzimuth();

  // fow
  void DoFade(bool fade);
  bool IsFading() const {return _doFade;}
  Vector3 FadePos() {return _fadePos;}
  float SimulateFade();

protected:
  void UpdateDrawMapScript(GameState *state) const;
  void UpdateDraw3DScript(GameState *state) const;
};

enum LineType
{
  LTLine,
  LTArrow,
  NLineTypes
};

struct ObjectLink
{
  bool showIn3D;
  const EditorObject *from;
  const EditorObject *to;
  RString linkName;
  LineType lineType;
  PackedColor color;
  float arrowScaleMin;  

  // only used with undo
  RString idFrom;
  RString idTo;

  ObjectLink() {from = NULL; to = NULL; lineType = LTLine; arrowScaleMin = 100; showIn3D = true;}
};
TypeIsMovableZeroed(ObjectLink)

struct ActionsList
{
  // a saved instance (used with undo)
  RefArray<EditorObject> objects;
  AutoArray<ObjectLink> objectLinks;
};
TypeIsMovableZeroed(ActionsList)

class EditorWorkspace
{
protected:
  // all object types (loaded at start)
  RefArray<EditorObjectType> _types;

  // editor objects
  RefArray<EditorObject> _objects;  

  // selected editor objects
  RefArray<EditorObject> _selected;

  // a list of editor objects (used with undo)
  AutoArray<ActionsList> _actionsList;

  // currently showing index
  int _actionListIndex;

  // speeds up drawing of links between objects
  AutoArray<ObjectLink> _objectLinks;

  // Fixed objects
  Ref<EditorObjectType> _typePrefix;
  Ref<EditorObjectType> _typePostfix;
  Ref<EditorObject> _objectPrefix;
  Ref<EditorObject> _objectPostfix;

  // loaded editor object overlays
  RefArray<EditorOverlay> _overlays;

public:
  EditorWorkspace() {Init();}

  void SaveInstance();
  void RevertToInstance(GameVarSpace *vars, int increment);
  void ClearInstances() {_actionListIndex = 0; _actionsList.Clear();}
 
  // overlays allow a user to create template missions that 'overlay' the main
  // mission (contains 'fake' editor objects)
  EditorOverlay *CreateOverlay(RString name, bool isTemplate, RString extension);
  void SetActiveOverlay(EditorOverlay *activeOverlay, bool exclusive = true);  
  EditorOverlay *GetActiveOverlay(); // return first active overlay
  int NOverlays() {return _overlays.Size();} 
  EditorOverlay *GetOverlay(int i) {return _overlays[i];} 
  void MergeActiveOverlays();
  void DeleteOverlay(const EditorOverlay *overlay);
  void SaveOverlay(EditorOverlay *overlay);
  EditorOverlay *LoadOverlay(RString name, bool isTemplate, RString extension);

  EditorObject *GetPrefixObject() {return _objectPrefix;}
  EditorObject *GetPostfixObject() {return _objectPostfix;}
  int NObjects() const {return _objects.Size();}
  const EditorObject *GetObject(int i) const {return _objects[i];}
  EditorObject *GetObject(int i) {return _objects[i];}

  // add an object that has already been created
  void AddObject(EditorObject *obj);  

  // create and add a new object of a certain type
  EditorObject *AddObject(EditorObjectType *type);

  EditorObject *CopyObject(EditorObject *newObj);
  void CopyObjects(RefArray<EditorObject> &clipboard);
  void RenameObjects(RefArray<EditorObject> &objects);

  void DeleteObject(EditorObject *obj);

  Entity *CreateEntityFromObject(Object *proxy);

  // select an object and subordinates - by default only select hidden suborindates
  void SetSelected(
    EditorObject *selectObj, 
    bool selected, 
    bool subordinatesAlso = true, 
    bool onlyHiddenSubordinates = true,
    bool isMultiple = false
  );
  void ClearSelected();
  EditorObject *AddSelected(Object *proxy);
  void RemoveFromSelected(Object *proxy);
  bool IsSelected(Object *proxy);
  RefArray<EditorObject> &GetSelected() {return _selected;}

  int NObjectTypes() const {return _types.Size();}
  const EditorObjectType *GetObjectType(int i) const {return _types[i];}
  EditorObjectType *FindObjectType(RString name);
  // EditorObject *FindObject(EditorObjectType *type, ...);
  EditorObject *FindObject(RString id);

  // object links
  AutoArray<ObjectLink> &GetDrawLinks() {return _objectLinks;}
  void SetDrawLink(EditorObject *from, EditorObject *to, RString linkName, LineType lineType, PackedColor color, float arrowScaleMin, bool showIn3D);
  void RemoveDrawLinks(EditorObject *from, RString linkName = "");
  void RemoveAllDrawLinks(EditorObject *from);
  void ClearDrawLinks() {_objectLinks.Clear();}

  // order objects by link dependencies  
  void OrderObjects(RefArray<EditorObject> &edObjects);
  void OrderAllObjects();

  // move an object to the end of the objects list
  void MoveObjectToEnd(EditorObject *obj);

  void WriteCreateScript(RString filename) const;
  void Clear();
  void Save(RString filename) const;
  void Load(RString filename, bool isMerge = false);

protected:
  void Init();
};

#endif // _ENABLE_EDITOR2

#endif

