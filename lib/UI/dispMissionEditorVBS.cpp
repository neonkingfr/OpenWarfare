#include "../wpch.hpp"

#if _ENABLE_EDITOR2 && _VBS2

#define _ENABLE_UNDO 0 //_ENABLE_CHEATS && _VBS3
#define _ENABLE_3D_LINKING 1

//#undef _VBS2_MODIFY_WORLD
//#define _VBS2_MODIFY_WORLD 1

/*!
\patch 2.92 Date 03/08/2006 by Pete
- Added: functions addEditorObject, setObjectArguments, getObjectProxy
*/

#include "missionEditorVBS.hpp"
#include "missionEditorCursorVBS.hpp"
#include "dispMissionEditor.hpp"
#include "uiControls.hpp"
#include "contextMenu.hpp"
#include "missionDirs.hpp"
#include "resincl.hpp"
#include "uiMap.hpp"
#include "chat.hpp"
#include "../gameDirs.hpp"
#include "../world.hpp"
#include "../landscape.hpp"
#include "../camera.hpp"
#include "../cameraHold.hpp"
#include "../camEffects.hpp"
#include "../gameStateExt.hpp"
#include "../AI/ai.hpp"
#include "../stringtableExt.hpp"
#include "../paramArchiveExt.hpp"
#include "../dikCodes.h"
#include "../fileLocator.hpp"
#include <El/QStream/packFiles.hpp>

#include <El/ParamArchive/paramArchiveDb.hpp>
#include "../saveVersion.hpp"

#include <Es/Files/fileContainer.hpp>

#include <El/PreprocC/preprocC.hpp>

#include <El/Evaluator/express.hpp>

#include "../hla/AAR.hpp"
#include "../detector.hpp"

#if _ENABLE_GAMESPY
  #include "../Network/winSockImpl.hpp"
  #include "../Network/network.hpp"
  #include <Es/Containers/bitmask.hpp>
  #include <Es/Common/win.h>
#endif
#include "displayUI.hpp"

// mission editor events
#ifndef _XBOX
#define EDITOR_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, SelectObject) \
  XX(type, prefix, UnselectObject) \
  XX(type, prefix, ClearSelected) \
  XX(type, prefix, EditOverlay) \
  XX(type, prefix, CloseOverlay) \
  XX(type, prefix, RestartCamera) \
  XX(type, prefix, MouseOver) \
  XX(type, prefix, SwitchCommandMenu) \
  XX(type, prefix, ShowMap) \
  XX(type, prefix, StopMovingObject) \
  XX(type, prefix, SwitchToVehicle) \
  XX(type, prefix, RestartMission) \
  XX(type, prefix, LinkFromObject) \
  XX(type, prefix, ClearMission)

#ifndef DECL_ENUM_EDITOR_EVENT
#define DECL_ENUM_EDITOR_EVENT
DECL_ENUM(EditorEvent)
#endif
DECLARE_ENUM(EditorEvent, ME, EDITOR_EVENT_ENUM)
#endif

#ifndef _XBOX
DEFINE_ENUM(EditorEvent, ME, EDITOR_EVENT_ENUM)
#endif

//-!

static const EnumName EditorObjectScopeNames[]=
{
  EnumName(EOSHidden, "Hide"),
  EnumName(EOSView, "View"),
  EnumName(EOSSelect, "Select"),
  EnumName(EOSLinkTo, "LinkTo"),
  EnumName(EOSLinkFrom, "LinkFrom"),
  EnumName(EOSDrag, "AllNoDrag"),
  EnumName(EOSAllNoTree, "AllNoTree"),
  EnumName(EOSAllNoSelect, "AllNoSelect"),
  EnumName(EOSAllNoCopy, "AllNoCopy"),
  EnumName(EOSAll, "All"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(EditorObjectScope dummy)
{
	return EditorObjectScopeNames;
}

static const EnumName FogOfWarNames[]=
{
  EnumName(FOWOff, "Off"),
  EnumName(FOWAllUnits, "All"),
  EnumName(FOWAllSides, "Side"),
  EnumName(FOWVisibleUnits, "Visible"),
  EnumName(FOWNone, "None"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(FogOfWar dummy)
{
	return FogOfWarNames;
}

enum EditorMode
{
  EMMap,
  EM3D,
  EMPreview,
  NEditorModes
};

enum EditorMapType
{
  EMTMainMap,
  EMTMiniMap,
  EMTNoMap,
  NEditorMapTypes
};

static const EnumName EditorModeNames[]=
{
	EnumName(EMMap, "Map"),
  EnumName(EM3D, "3D"),
	EnumName(EMPreview, "Preview"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(EditorMode dummy)
{
	return EditorModeNames;
}

enum EditorTreeFilter
{
  ETFNone,
  ETFUnitsAndVehicles,
  ETFPlayers,
  ETFObjects,
  NEditorTreeFilters
};

// dialog coords (0..1) to mouse coords (-1..1)
#define TO_MOUSE(pt) (pt - 0.5f) / 0.5f

RString GetUserDirectory();

GameValue CreateGameControl(Control *ctrl);
GameValue CreateGameControl(ControlObject *ctrl);

bool HasPrimaryGunnerTurret(ParamEntryPar cls);
bool HasPrimaryObserverTurret(ParamEntryPar cls);

extern GameValue GMapOnSingleClick;
extern GameValue GMapOnSingleClickParams;

enum EditorCommands
{
  ECSetCameraPosition,
  ECLockCamera,
  ECSwitchToVehicle,
  ECSnapToSurface,
  ECEditObject,
  ECEditLink,
  ECDeleteObject,
  ECCenterMap,
  ECNewObject,
  ECExecuteScript,
  ECMenuFileLoad,
  ECMenuFileSave,
  ECMenuFileClear,
  ECMenuFileEditBriefing,
  ECMenuFileOptions,
  ECMenuFileRestart,
  ECMenuFilePreview,
  ECMenuFileExit,
  ECMenuView2D,
  ECMenuView3D,
  ECMenuViewCommand,
  ECMenuAdditionalItem
};

enum EditorMenuType
{
  EMNone = -1,
  EMPopup,
  EMFile,
  EMView,
  EMUser  // must always be last
};

// used with help text
enum EditorSelectedStatus
{
  ESNone,
  ESUnSelected,
  ESSelected
};

class DisplayMissionEditor;

static const EnumName LineTypeNames[]=
{
	EnumName(LTLine, "Line"),
  EnumName(LTArrow, "Arrow"),
	EnumName()
};
template<>
const EnumName *GetEnumNames(LineType dummy)
{
	return LineTypeNames;
}

class CStaticMapEditor : public CStaticMapMain
{
protected:
  InitPtr<EditorWorkspace> _ws;
  InitPtr<GameVarSpace> _vars;    

  // if this is a mini-map, we need reference to main map
  CStaticMapEditor *_mainMap;
  bool _drawMainMapBox;

  // draw editor object icons?
  bool _drawEditorObjects;

  // dragging an editor object
  bool _dragging;

  // selecting multiple editor objects
  bool _selecting;

  // selected a single editor object?
  bool _selectOne;

  // moving the map or camera by holding right mouse button
  bool _draggingRM;

  // current position
  Vector3 _dragPos;

  // starting position for multiple select
  Vector3 _startPos;

  // record time that mouse was first moved (for clearing labels)
  UITime _firstMove;

  UITime _lastObjectVisibilityUpdate;

  Ref<EditorObject> _linkObject;
  RString _linkArgument;
  int _linkShortcutKey;

  // draw cursor lines out to edge of map?
  bool _drawCursorLines;

  // link to editor camera
  MissionEditorCamera *_camera;

  // lock map to editor camera?
  bool _lockToCamera;

public:
  CStaticMapEditor(
    ControlsContainer *parent, int idc, ParamEntryPar cls,
    float scaleMin, float scaleMax, float scaleDefault,
    EditorWorkspace *ws, GameVarSpace *vars);

  EditorWorkspace *GetWorkspace() {return _ws;}
  GameVarSpace *GetVariables() {return _vars;}

  virtual void OnLButtonDown(float x, float y);
  virtual void OnLButtonUp(float x, float y);
  virtual void OnRButtonDown(float x, float y);
  virtual void OnRButtonUp(float x, float y);
  virtual void OnRButtonClick(float x, float y);
  virtual void OnMouseMove(float x, float y, bool active);
  virtual void OnMouseHold(float x, float y, bool active);
  virtual void OnLButtonClick(float x, float y);
  virtual void OnLButtonDblClick(float x, float y);

  bool OnKeyDown(int dikCode) {return CStaticMap::OnKeyDown(dikCode);}
  bool OnJoystickButton(int button) {return false;}

  virtual void OnDraw(UIViewport *vp, float alpha);
  virtual void DrawExt(float alpha);  

  virtual void Center(bool soft = false);
  Vector3 GetCenter();
  virtual void DrawLabel(struct SignInfo &info, PackedColor color);
  virtual SignInfo FindSign(float x, float y);

  GameValue GetObjectArgument(RString id, RString name);
  EditorObject *FindEditorObject(RString type, AutoArray<EditorArgument> &args);
  EditorObject *FindEditorObject(GameValue value,RString type = "");

  void EditLink(EditorObject *obj, RString name, float key = -1, bool assignLinkArg = false);
  
  void ShowCursorLines(bool show) {_drawCursorLines = show;}

  void SetEditorCamera(MissionEditorCamera *camera) {_camera = camera;}
  MissionEditorCamera *GetEditorCamera() {return _camera;}
  void LockToCamera(bool lock) {_lockToCamera = lock;}
  bool IsLockedToCamera() {return _lockToCamera;}
  void LockCamera(RString id, Vector3 lockPos);

  void SetMainMap(CStaticMapEditor *map) {_mainMap = map;}
  void DrawMainMapBox(bool draw) {_drawMainMapBox = draw;}
  void DrawEditorObjects(bool draw) {_drawEditorObjects = draw;}

  // show hide legend
  void ShowLegend(bool showLegend) {_showCountourInterval = showLegend;}

  // add an object to the mission editor
  RString AddObject(
    RString typeName, 
    const AutoArray<RString> &args, 
    bool create = true, 
    GameValue variable = NOTHING, 
    ConstParamEntryPtr subTypeConfig = NULL
  );  

  // show add new object dialog
  void ShowNewObject(RString type, RString className, TargetSide side, Vector3 posn);
  void ShowEditObject(RString id);

  // return an object proxy corresponding to id
  Object *FindObject(RString id);

  // delete an object from the mission editor
  void DeleteObjectFromEditor(RString id);

  // return true if running in real time mode
  bool IsRealTime();

  bool CommandMenuVisible();
  void ShowCommandMenu(bool show);

  // camera related
  MissionEditorCamera *CameraGet();
  void CameraRestart();  

  // update the proxy object for an editor object
  void UpdateProxy(Object *object, EditorObject *edObj);

  // delete the proxy object for an editor object
  void DeleteProxy(EditorObject *edObj);

  // set the onDoubleClick eventhandler for the parent
  void SetOnDoubleClick(RString eh);

  // set the onShowNewObject eventhandler for the parent
  void SetOnShowNewObject(RString eh);

  // set the onQuickAddObject eventhandler for the parent
  void SetOnQuickAddObject(RString eh, int key);
  void SetOnQuickAddObjectToggled(RString eh);
  void ToggleOnQuickAddObject(bool on);

  // select a single object
  void SelectObject(EditorObject *obj);
  void UnselectObject(EditorObject *obj);  

  // clear selected editor objects
  void ClearSelected();

  // stop drawing the select box
  void ClearSelectingBox() {_selecting = false;}

  void UpdateObjectBrowser();
  void UpdateObjectBrowser(RString toUpdate, bool updateVisibility = false, bool expand = true);
  void UpdateObjectBrowser(const EditorObject *obj);

  // position map and camera
  void LookAtPosition(Vector3 pos);

  Ref<EditorObject> GetLinkObject() {return _linkObject;}
  void SetLinkObject(Ref<EditorObject> obj) {_linkObject = obj;}

  void ProcessMouseOverEvent();

  // add menus and menu items to the editor
  int AddMenu(RString text, float priority);
  void CreateMenu(int index);
  int AddMenuItem(RString type, RString text, RString command, int index, float priority, bool assignPriority);
  void UpdateMenuItem(int index, RString text, RString command);  
  void RemoveMenuItem(int index, RString text = RString());
  int NMenuItems(RString menuName, int index);
  int GetNextMenuItemIndex();

  // overlays
  void NewOverlay(ConstParamEntryPtr entry);
  void LoadOverlay(ConstParamEntryPtr entry);
  void SaveOverlay();
  void ClearOverlay(bool commit = false, bool close = false);
  void DeleteOverlay(EditorOverlay *overlay);

  // set editor mode
  void SetMode(EditorMode mode, bool overrideAllow3D);
  EditorMode GetMode();

  // execute create script for an editor object
  void CreateObject(EditorObject *obj, bool isNewObject = false, bool isPaste = false, bool isLoading = false, bool isCalledByExecEditorScriptCommand = false, bool isCreatedFromTemplate = false);

  void Show3DIcons(bool show);
  bool IsShow3DIcons();

  void Allow3DMode(bool allow);
  void Enable3DLinking(bool allow);
  void ExitOnMapKey(bool allow);
  void AllowFileOperations(bool allow);

  // set editor eventhandler
  void SetEditorEventHandler(RString name, RString value);

  // set fog of war
  void SetFogOfWar(FogOfWar fogOfWar, OLinkArray(Object) &objs, bool fromPlayer);

  // validate parent (ie is DisplayMissionEditor valid?)
  bool HasParent();

  bool IsDragging() {return _dragging;} 
  bool IsDraggingRM() {return _draggingRM;} 
  bool IsSelecting() {return _selecting;}

  void ImportAllGroups();
  void CreateWaypointEdObjs(AIGroup *aGroup, RString grpEdObj, RString unitEdObj);
  RString CreateUnitEdObj(Person *person, Transport *trans, RString vehEdObj, RString grpEdObj, RString leaderEdObj);
  RString CreateVehEdObj(Transport *trans, RString grpEdObj);

protected:
  DisplayMissionEditor *GetParent();
};

struct CCMContextEditor : public CCMContext
{
  int type;
  RString name;
  CStaticMapEditor *map;
  Vector3 position;

  CCMContextEditor(RString n, CStaticMapEditor *m, Vector3Par pos, int t = EMPopup) {name = n; map = m; position = pos; type = t;}
};

// String attribute
class AttrVector : public IAttribute
{
protected:
  Vector3 _value;

public:
  void Load(RString value);
  Vector3 GetValue() const {return _value;}
  void SetValue(Vector3Par value) {_value = value;}

  LSError Serialize(ParamArchive &ar);
  bool IsEqualTo(const IAttribute *with) const;
};

void AttrVector::Load(RString value)
{
  sscanf(value, "%.5f, %.5f, %.5f", &_value[0], &_value[1], &_value[2]);
}

LSError AttrVector::Serialize(ParamArchive &ar)
{
  CHECK(::Serialize(ar, "value", _value, 1));
  return LSOK;
}

bool AttrVector::IsEqualTo(const IAttribute *with) const
{
  const AttrVector *w = static_cast<const AttrVector *>(with);
  return GetValue() == w->GetValue();
}

IAttribute *CreateAttrVector() {return new AttrVector();}

AttributeTypes EditorMenuAttributeTypes;

#define ATTRIBUTES_LIST(XX) \
  XX("object", attrObject, CreateAttrString) \
  XX("argument", attrArgument, CreateAttrString) \
  XX("position", attrPosition, CreateAttrVector)

#define DEFINE_ATTR_VARIABLE(name, variable, func) static int variable;

ATTRIBUTES_LIST(DEFINE_ATTR_VARIABLE);

class CEditorMenuButton : public CButton
{
public:
  CEditorMenuButton(ControlsContainer *parent, int idc, ParamEntryPar cls)
    : CButton(parent, idc, cls) {}
  virtual void OnLButtonDblClick(float x, float y) {return;}
};

class CObjectBrowser : public CTree
{
public:
  CObjectBrowser(ControlsContainer *parent, int idc, ParamEntryPar cls)
    : CTree(parent, idc, cls) {}
  virtual void OnRButtonClick(float x, float y);
  virtual void OnMouseMove(float x, float y, bool active);
};

// this class serves only to 'catch' key and mouse events in 3D mode
class CBackground3D : public CListBox
{
public:
  CBackground3D(ControlsContainer *parent, int idc, ParamEntryPar cls)
    : CListBox(parent, idc, cls) {}
  virtual void OnLButtonDown(float x, float y);
  virtual void OnLButtonUp(float x, float y);
  virtual void OnLButtonClick(float x, float y);
  virtual void OnLButtonDblClick(float x, float y);
  virtual void OnMouseMove(float x, float y, bool active);
  virtual void OnRButtonDown(float x, float y);
  virtual void OnRButtonUp(float x, float y);
  virtual void OnRButtonClick(float x, float y);

protected:
  DisplayMissionEditor *GetParent();
};

class CompassMissionEditor : public Compass
{
  bool _isRotating;

public:
  CompassMissionEditor(ControlsContainer *parent, int idc, ParamEntryPar cls);
  virtual void OnRButtonDown(float x, float y);
  void OnSimulate(float deltaT, Vector3 &compassDir);

protected:
  bool IsRotating() 
  {
    if (GInput.keys[DIK_LSHIFT] > 0 && GInput.mouseR)
      return _isRotating;
    return _isRotating = false;
  }
};

// in-editor, user-defined briefing
enum EditorBriefingSection
{
  EBAll,
  EBWest,
  EBEast,
  EBRes,
  EBCiv,
  NEditorBriefingSections
};

struct EditorObjectives
{
private:
  AutoArray<RString> objectives;
  AutoArray<bool> hidden;
public:
  int NObjectives()
  {
    return objectives.Size();
  }
  bool IsHidden(int i)
  {
    return hidden[i];
  }
  void DeleteObjective(int i)
  {
    objectives.Delete(i);
    hidden.Delete(i);
  }
  RString GetObjective(int i)
  {
    return objectives[i];
  }
  int AddObjective(RString add, bool hide)
  {
    objectives.Add(add);
    return hidden.Add(hide);
  }
  void Clear() 
  {
    objectives.Clear(); 
    hidden.Clear();
  }
  bool IsValid()
  {
    for (int i=0; i<objectives.Size(); i++)
      if (objectives[i].GetLength() > 0)
        return true;
    return false;
  }
};
TypeIsMovableZeroed(EditorObjectives)

class EditorBriefing
{
  AutoArray<RString> _plan;
  AutoArray<RString> _notes;
  // one EditorObjectives for each side
  AutoArray<EditorObjectives> _objectives;
  bool _modified;

public:
  EditorBriefing();
  void WriteHTML(RString directory);
  void SetPlan(AutoArray<RString> plan) {_modified = true; _plan = plan;}
  void SetNotes(AutoArray<RString> notes) {_modified = true; _notes = notes;}
  void SetObjectives(AutoArray<EditorObjectives> objs) {_modified = true; _objectives = objs;}
  const AutoArray<RString> &GetPlan() {return _plan;}
  const AutoArray<RString> &GetNotes() {return _notes;}
  AutoArray<EditorObjectives> GetObjectives() {return _objectives;}
  void Clear();
  bool IsValid();
  void Load(CHTMLContainer *briefing);
  void LoadObjectives(CHTMLContainer *briefing, RString text, int idx);
  RString GetBriefing();
};

EditorBriefing::EditorBriefing()
{
  _modified = false;

  _plan.Realloc(NEditorBriefingSections);
  _plan.Resize(NEditorBriefingSections);

  _notes.Realloc(NEditorBriefingSections);
  _notes.Resize(NEditorBriefingSections);

  _objectives.Realloc(NEditorBriefingSections);
  _objectives.Resize(NEditorBriefingSections);
}

void EditorBriefing::LoadObjectives(CHTMLContainer *briefing, RString text, int idx)
{
  int i = 0;
  while (true)
  {
    bool hidden = false;
    int section = briefing->FindSection(text + Format("%d",++i));
    if (section == -1)
    {
      section = briefing->FindSection(text + RString("HIDDEN_") + Format("%d",i));
      hidden = true;
    }
    if (section > -1)
      _objectives[idx].AddObjective(briefing->GetSection(section).html,hidden);
    else
      break;
  }
}

void EditorBriefing::Load(CHTMLContainer *briefing)
{
  if (!briefing) return;
  int section;

#define READ_SECTION(ARRAY,TEXT,IDX) \
  section = briefing->FindSection(TEXT); \
  if (section > -1) ARRAY[IDX] = briefing->GetSection(section).html;

  READ_SECTION(_plan,"Plan",EBAll)
  READ_SECTION(_plan,"Plan.West",EBWest)
  READ_SECTION(_plan,"Plan.East",EBEast)
  READ_SECTION(_plan,"Plan.Guerrila",EBRes)
  READ_SECTION(_plan,"Plan.Civilian",EBCiv)

  READ_SECTION(_notes,"Main",EBAll)
  READ_SECTION(_notes,"Main.West",EBWest)
  READ_SECTION(_notes,"Main.East",EBEast)
  READ_SECTION(_notes,"Main.Guerrila",EBRes)
  READ_SECTION(_notes,"Main.Civilian",EBCiv)
  
  LoadObjectives(briefing,"OBJ_",EBAll);
  LoadObjectives(briefing,"OBJ_WEST_",EBWest);
  LoadObjectives(briefing,"OBJ_EAST_",EBEast);
  LoadObjectives(briefing,"OBJ_GUER_",EBRes);
  LoadObjectives(briefing,"OBJ_CIVIL_",EBCiv);
}

void EditorBriefing::Clear()
{
  for (int i=0; i<NEditorBriefingSections; i++)
  {
    _plan[i] = RString();
    _notes[i] = RString();
    _objectives[i].Clear();
  }
  _modified = false;
}

bool EditorBriefing::IsValid()
{
  for (int i=0; i<NEditorBriefingSections; i++)
  {
    if (_plan[i].GetLength() > 0) return _modified;
    if (_notes[i].GetLength() > 0) return _modified;
    if (_objectives[i].IsValid()) return _modified;
  }
  return false;
}

inline RString SectionStart(RString section, RString text)
{
  return "\r\n<h2><a name=\"" + section + "\"></a>\r\n" + text;
}

inline RString SectionEnd()
{
  return "\r\n</h2>\r\n<hr>";
}

#define PROCESS_SIDE_BRIEFING(SECTION_NOTES,SECTION_PLAN,SECTION_OBJ) \
  if (_notes[i].GetLength() > 0) line = line + SectionStart(SECTION_NOTES,_notes[i]) + SectionEnd(); \
  if (_plan[i].GetLength() > 0) line = line + SectionStart(SECTION_PLAN,_plan[i]) + SectionEnd(); \
  if (_objectives[i].IsValid()) \
  { \
    for (int j=0; j<_objectives[i].NObjectives(); j++) \
    { \
      RString hidden = _objectives[i].IsHidden(j) ? "HIDDEN_" : ""; \
      RString text = SECTION_OBJ + hidden + Format("%d",j+1); \
      line = line + SectionStart(text,_objectives[i].GetObjective(j)) + SectionEnd(); \
    } \
  }

RString EditorBriefing::GetBriefing()
{
  if (!IsValid()) return RString();
  RString header = "<html>\r\n<body>";
  RString line;
  for (int i=0; i<NEditorBriefingSections; i++)
  {
    switch (i)
    {
    case EBAll:
      PROCESS_SIDE_BRIEFING("Main","Plan","OBJ_")
      break;
    case EBWest:
      PROCESS_SIDE_BRIEFING("Main.West","Plan.West","OBJ_WEST_")
      break;
    case EBEast:
      PROCESS_SIDE_BRIEFING("Main.East","Plan.East","OBJ_EAST_")
      break;
    case EBRes:
      PROCESS_SIDE_BRIEFING("Main.Guerrila","Plan.Guerrila","OBJ_GUER_")
      break;
    case EBCiv:
      PROCESS_SIDE_BRIEFING("Main.Civilian","Plan.Civilian","OBJ_CIVIL_")
      break;
    }
  }
  RString footer = "\r\n</body>\r\n</html>";    
  return header + line + footer;
}

void EditorBriefing::WriteHTML(RString directory)
{
  if (!IsValid()) return;
  RString filename = directory + RString("briefing.html");
  QOFStream out(filename);
  RString line = GetBriefing();
  out.write(line, strlen(line));
  out.close();
}

struct AdditionalMenuItem
{
  int type;
  RString text;
  RString command;
  int index;
  float priority;
};
TypeIsMovableZeroed(AdditionalMenuItem)

struct UserMenu
{
  Control *ctrl;
  Control *bg;
  float priority;
  int index;
  UserMenu(Control *c, Control *b, float p, int i) {ctrl = c; bg = b; priority = p; index = i;}
};
TypeIsMovableZeroed(UserMenu)

class DisplayMissionEditor : public DisplayMap, public MissionEditCursorContainer
{
protected:  
  EditorWorkspace _ws;
  GameVarSpace _vars;
  EditorBriefing _editorBriefing;

  RefArray<EditorObject> _clipboard;

  // proxy objects that have been moved in a single simulation step
  OLinkArray(Object) _movedProxies;

  // this is a copy of the last created editor object
  EditorObject *_lastObjectCreated;  
  
  // additional menu items
  AutoArray<UserMenu> _userMenus;
  AutoArray<AdditionalMenuItem> _menuItems;

  Ref<MissionEditorCamera> _camera;

  UITime _lastSimulate;

  UITime _lastObjectDraw3DUpdate;

  Ref<CTree> _objectBrowser;
  Ref<CListBox> _attributeBrowser;
  Ref<CStaticMapEditor> _miniMap;
  Ref<CListBox> _objectTypeList;
  Ref<CBackground3D> _background3D;

  EditorTreeFilter _treeFilter;

  Ref<Person> _originalPlayer;
  Ref<AIBrain> _playerUnit;

  CursorType _mouseCursor;

  EditorMode _mode;

  EditorSelectedStatus _selectedObject;

  // active map type (used with popup menus)
  EditorMapType _activeMap;

  // the editor object the mouse is currently over in 2D or 3D
  EditorObject *_objMouseOver;

#if _VBS2_MODIFY_WORLD
  // temporary editor object (used for selecting static objects)
  EditorObject _objStaticMouseOver;
  bool IsMouseOnStaticObject() {return _objMouseOver && _objMouseOver == &_objStaticMouseOver;}
#endif

  // running in real time mode?
  bool _realTime;

  // C2 options visible?
  bool _showCommandMenu;

  bool _allow3DMode;
  bool _allow3DLinking;
  bool _allowFileOperations;
  bool _exitOnMapKey;

  bool _reShowLegend;

  // show / hide interface?
  bool _interfaceHidden;

  // started by server in a non-dedicated multiplayer session
  bool _multiplayer;
  bool _multiplayerPreview;

  // last saved mission name
  RString _mission;

  // used to auto-popup main menus in a similar manner to windows
  int _menuTypeActive;

  // onDoubleClick eventhandler
    // received from map and background controls
  RString _onDoubleClick;

  // onShowNewObject eventhandler
  RString _onShowNewObject;

  // onQuickAddObject eventhandler
  RString _onQuickAddObject;
  int _quickAddObjectKey;
  RString _onQuickAddObjectToggled;

  // dragging an editor object
  bool _dragging;
  Vector3 _dragPos;
  //used when there is no ground interception
  bool _dragStartedInAir;

  // selecting multiple editor objects
  bool _selecting;

  // selected a single editor object?
  bool _selectOne;

  // moving with right mouse held down
  bool _draggingRM;

  // rotating an editor object
  bool _rotating;

  // raising or lowering an editor object
  bool _raising;

  // rotating or raising on a map screen (either mini map or main map)?
  bool _rotatingOrRaisingOnMap;

  // moving an editor object by dragging
  bool _movingEdObj;

  // used with multiple select to record screen coords
  DrawCoord _pt1, _pt2;

  // last selected object in the tree
  RString _lastSelectedInTree;

  // timer used to re-enable simulation for selected objects in real time
//UITime _stoppedSimulate;
  bool _simulationDisabled;

  // point to overlay types config entry
  ConstParamEntryPtr _overlayType;

  // undo
  bool _hasChanged;
  bool _ignoreChanges;

  // add object on single left click?
  bool _addOnLeftClick;
  bool _addOnLeftClickOn;   // set through script command
  bool _quickAddByKey;

  // multiplier for object nudges
  float _nudgeSpeed;

  // pointer to editor version of compass
  CompassMissionEditor *_compassEditor;

  // hack to enable scripted missions to work in MP
    // template used for saving player and playable units to a mission.sqm file
  ArcadeTemplate _templateMission;

#ifndef _XBOX
  //! event handlers
  EventHandlersUI _eventHandlers[NEditorEvent];
#endif

  // draw 3D icons?
  bool _show3DIcons;

  // linking in 3D
  Ref<EditorObject> _linkObject;
  RString _linkArgument;
  int _linkShortcutKey;

  // country filter
  RString _filter;

public:
  DisplayMissionEditor(ControlsContainer *parent, RString resource, bool multiplayer = false);
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  ControlObject *OnCreateObject(int type, int idc, ParamEntryPar cls);

  virtual void Destroy();

  // hack to enable scripted missions to work in MP
  void SaveSQM(RString directory);

  void SaveAttachedObjectPositions();

  void ProcessMouseOverEvent();
  void SetObjMouseOver(EditorObject *over) {_objMouseOver = over;}
  EditorObject *GetObjMouseOver() {return _objMouseOver;}

  // undo
  void RecordAction(bool saveInstance = true);

  void SetLastSelectedInTree(RString id) {_lastSelectedInTree = id;}

  void OnButtonClicked(int idc);
  void OnTreeSelChanged(IControl *ctrl);
  void OnTreeItemExpanded(IControl *ctrl, CTreeItem *item, bool expanded);
  void OnTreeDblClick(int idc, CTreeItem *sel);
  void OnTreeLButtonDown(int idc, CTreeItem *sel);
  void OnTreeMouseMove(int idc, CTreeItem *sel);
  void OnTreeMouseHold(int idc, CTreeItem *sel);
  void OnTreeMouseExit(int idc, CTreeItem *sel);
  void OnLBSelChanged(IControl *ctrl, int curSel);
  bool OnKeyDown(int dikCode);
  bool OnKeyUp(int dikCode);

  EditorObject *LastObjectCreated() {return _lastObjectCreated;}
  void ClearLastObjectCreated() {_lastObjectCreated = NULL;}

  void OnSimulate(EntityAI *vehicle);
  void OnDraw(EntityAI *vehicle, float alpha);

  // received from CBackground3D (the control that catches these events)
  void OnLButtonDown(float x, float y);
  void OnLButtonUp(float x, float y);
  void OnLButtonClick(float x, float y);  
  void OnLButtonDblClick(float x, float y);
  void OnMouseMove(float x, float y, bool active);
  void OnRButtonDown(float x, float y);
  void OnRButtonUp(float x, float y);
  void OnRButtonClick(float x, float y);

  // linking in 3D
  void EditLink(EditorObject *obj, RString name, float key, bool assignLinkArg = false);

  // copy selected editable objects to clipboard  
  void ClipboardCopy(bool isCut);
  void ClipboardPaste();

  // is the mouse over a map control?
  CStaticMapEditor *MouseOverMap();

  void SetRotating(bool rotating) {_rotating = rotating;}
  void SetRaising(bool raising) {_raising = raising;}

  // return true if moving an editor object by dragging
  bool IsMovingEdObj() {return _movingEdObj;}
  void SetMovingEdObj(bool move) {_movingEdObj = move;}

  // return true if dragging
  bool IsDragging() 
  {
    if (_map) if (_map->IsDragging()) return true;
    if (_miniMap) if (_miniMap->IsDragging()) return true;
    return _dragging || _rotating || _raising;
  }

  bool IsAddOnLeftClick() {return _addOnLeftClick;}

  virtual void OnMenuSelected(MenuItem *item);
  virtual bool OnMenuCreated(Menu &menu, float x, float y, int idc, CCMContext &ctx);
  void InsertAdditionalMenuItems(Menu &menu, int menuType);
  void RemoveContextMenu();

  void OnObjectSelected(RString id);  
  void OnLinkEditted(EditorObject *obj, RString name, RString value, float key, CStaticMapEditor *map);
  
  virtual void OnChildDestroyed(int idd, int exit);

  virtual bool OnUnregisteredAddonUsed(RString addon);

  bool OneProxySelected();

  // MissionEditCursorContainer interface
  void OnObjectMoved(EditorObject *obj, Vector3Par offset);
  void RotateObject(EditorObject *obj, Vector3Par center, float angle);

  Object *FindObject(RString id);

  void UpdateObjectBrowser();
  void UpdateObjectBrowser(RString toUpdate, bool updateVisibility = false, bool expand = true);
  void UpdateObjectBrowser(const EditorObject *obj, bool updateVisibility = false, bool expand = true);
  void RemoveObjectFromBrowser(const EditorObject *obj);
  
  // position map and camera
  void LookAtPosition(Vector3 pos);
  void LockCamera(RString id, Vector3 lockPos);

  void CreateObject(EditorObject *obj,bool isNewObject = false, bool isPaste = false, bool isLoading = false, bool isCalledByExecEditorScriptCommand = false, bool isCreatedFromTemplate = false);

  // call the delete object script for the object
  void DeleteObject(RString id);  
  void DeleteObject(EditorObject *obj);

  // delete an object from the editor (does not call delete script)
  void DeleteObjectFromEditor(RString id);  

  // display new object resource
  void ShowNewObject(Vector3 pos, EditorObject *linkObj = NULL, RString linkArg = "", RString className = "", TargetSide side = TSideUnknown);

  // select a specific editor object type
  bool SelectEditorObject(RString select);

  // update position of remote objects (after a drag, for example)
  void UpdateRemoteSelObjects();

  // select a specific editor object
  void SelectObject(RString name);
  void UnSelectObject(RString name);
  void ClearSelected();

  // stop drawing the select box
  void ClearSelectingBox() {_selecting = false; if (_map) _map->ClearSelectingBox(); if (_miniMap) _miniMap->ClearSelectingBox();}

  // register all proxies
  void RegisterProxies();

  // display edit object resource
  virtual void ShowEditObject(RString id, bool disregardProxy = false);

  // find the editor object that owns the given proxy
  EditorObject *FindProxyOwner(Object *proxyObject);

  // return true if running in real time mode
  bool IsRealTime() {return _realTime;}

  // command menu (unit bar and command menu)
  bool CommandMenuVisible() {return _realTime && _showCommandMenu && (_mode == EM3D || _mode == EMMap);}
  void ShowCommandMenu(bool show) 
  {
    _showCommandMenu = show;
#ifndef _XBOX
    OnEditorEvent(MESwitchCommandMenu, show);
#endif
  }

  // set/get double click event handler
  void SetOnDoubleClick(RString eh) {_onDoubleClick = eh;}
  RString GetOnDoubleClick() {return _onDoubleClick;}

  // set show new object event handler
  void SetOnShowNewObject(RString eh) {_onShowNewObject = eh;}

  // set on quick add object event handler
  void SetOnQuickAddObject(RString eh, int key) 
  {
    _onQuickAddObject = eh;
    _quickAddObjectKey = key;
  }
  void SetOnQuickAddObjectToggled(RString eh) {_onQuickAddObjectToggled = eh;}
  void ToggleOnQuickAddObject(bool on) {_addOnLeftClickOn = on;}
  void ProcessOnQuickAddObjectEvent(Vector3 pos);
  void ProcessOnQuickAddObjectEventToggled(bool on);

  // update the controls help text
  void UpdateControlsHelp();
  void SetSelectedStatus(EditorSelectedStatus selectedObject);

  // enable simulation for selected units - used with raising in real time
  void EnableSimulationForSelected(bool enable);    

  // set the active overlay
  void SetActiveTemplateOverlay(EditorOverlay *newOverlay);

  // delete an overlay and all objects in it
  void DeleteOverlay(EditorOverlay *overlay);

  // show message box asking to save before closing overlay
  void AskForOverlaySave(int idd, RString text = LocalizeString("STR_EDITOR_PROMPT_SAVE"));

  // commit the active template overlay
  void CommitActiveTemplateOverlay();  

  // add a new scripted menu or menu item
  int AddMenu(RString text, float priority);
  void CreateMenu(int index);
  int AddMenuItem(RString type, RString text, RString command, int index, float priority, bool assignPriority);
  void UpdateMenuItem(int index, RString text, RString command);
  void RemoveMenuItem(int index, RString text = RString());
  int NMenuItems(RString menuName, int index);
  int GetNextMenuItemIndex();

  // overlays
  void NewOverlay(ConstParamEntryPtr entry);
  void LoadOverlay(ConstParamEntryPtr entry);
  void SaveOverlay();
  void ClearOverlay(bool commit = false, bool close = false);

  // set editor mode (2D, 3D etc)
  void SetMode(EditorMode mode, bool overrideAllow3D = false);
  EditorMode GetMode() {return _mode;}

  void Show3DIcons(bool show) {_show3DIcons = show;}
  bool IsShow3DIcons() {return _show3DIcons;}

  void Allow3DMode(bool allow) {_allow3DMode = allow;}
  void Enable3DLinking(bool allow) {_allow3DLinking = allow;}
  void AllowFileOperations(bool allow) {_allowFileOperations = allow;}
  void ExitOnMapKey(bool allow) {_exitOnMapKey = allow;}

  // execute 'select' script for an editor object
  void DoSelectObjectScript(EditorObject *obj, bool selected);

#ifndef _XBOX
  /// set event handler
  void SetEditorEventHandler(RString name, RString value);

  // process event handlers
  void OnEditorEvent(EditorEvent event);
  void OnEditorEvent(EditorEvent event, RString par1, const Object *par2 = NULL);
  void OnEditorEvent(EditorEvent event, RString par1, ConstParamEntryPtr par2 = NULL);
  void OnEditorEvent(EditorEvent event, RString par1, RString par2, bool par3);
  void OnEditorEvent(EditorEvent event, bool par1);
  void OnEditorEvent(EditorEvent event, const Object *par1, const Object *par2 = NULL);
#endif

  // camera related
  MissionEditorCamera *CameraGet();
  void CameraRestart();

  virtual void ImportAllGroups() {}
  virtual void CreateWaypointEdObjs(AIGroup *aGroup, RString grpEdObj, RString unitEdObj) {}
  virtual RString CreateUnitEdObj(Person *person, Transport *trans, RString vehEdObj, RString grpEdObj, RString leaderEdObj) {return RString();}
  virtual RString CreateVehEdObj(Transport *trans, RString grpEdObj) {return RString();}

  // set fog of war
  virtual void SetFogOfWar(FogOfWar fogOfWar, OLinkArray(Object) &objs, bool fromPlayer) {}

  void SetCameraPosition(RString id, bool lockCamera, Vector3 lockPos = Vector3());

protected:  
  void UpdateObject(EditorObject *obj, bool isRotate = false, bool isRaise = false, float raiseBy = 0.0);
  void UpdatePositionObject(EditorObject *obj,Vector3Par offset);  

  bool IsObjectBrowser() const {return _objectBrowser && _objectBrowser->IsVisible();}
  void ShowObjectBrowser(bool show);  
  void UpdateAttributeBrowser();

  void ShowBackground3D(bool show);

  bool IsMap() const {return _map && _map->IsVisible();}
  //CStaticMapEditor *GetMap() {return _map;}
  void ShowMap(bool show);  

  // is the editor camera currently active?
  bool EditorCameraActive() 
  {
    CameraEffect *effect = GWorld->GetCameraEffect();
    return effect && effect->GetObject() == _camera;
  }

  bool IsCameraOnVehicle() const {return _playerUnit.NotNull();}
  virtual bool SwitchToVehicle(RString id);
  void SwitchToCamera();

  void LoadMission();
  void RestartMission();
  void ClearMission();

  // read and process init.sqf
  void DoInit(RString initSQF = "");
};

class DisplayMissionEditorNormal : public DisplayMissionEditor
{
public:
  DisplayMissionEditorNormal(ControlsContainer *parent, bool multiplayer = false);
};

DisplayMissionEditorNormal::DisplayMissionEditorNormal(ControlsContainer *parent, bool multiplayer)
: DisplayMissionEditor(parent,"RscDisplayMissionEditor",multiplayer)
{
  // hide main map objects (they are only used in Real Time)
  ShowCompass(true);
  ShowWatch(false);
  ShowWalkieTalkie(false);
  ShowNotepad(false);
  ShowWarrant(false);
  ShowGPS(false);
  ShowBriefing(false); 

  if (*Glob.header.filename)
    _mission = Glob.header.filename;

  LoadMission();
  DoInit();
}

inline DisplayMissionEditor *CStaticMapEditor::GetParent()
{
  return static_cast<DisplayMissionEditor *>(_parent.GetLink());
}

Display *CreateMissionEditor(ControlsContainer *parent, bool multiplayer)
{
  return new DisplayMissionEditorNormal(parent, multiplayer);
}

static bool IsMenuItemActive(RString condition, EditorObject *obj, CStaticMapEditor *map, bool realTime, bool allow3DMode = false, bool overTree = false)
{
  if (condition.GetLength() == 0) return true;

  if (!stricmp(condition, "editor") || !stricmp(condition, "realtime"))
    return (
      (stricmp(condition, "editor") == 0 && !realTime) ||
      (stricmp(condition, "realtime") == 0 && realTime)
    );

  if (!map) return false;

  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(map->GetVariables()); // editor space
  GameVarSpace local(map->GetVariables(), false);
  gstate->BeginContext(&local); // local space

  gstate->VarLocal("_map");
  gstate->VarSet("_map", CreateGameControl(map), true, false, GWorld->GetMissionNamespace());

  gstate->VarLocal("_realtime");
  gstate->VarSet("_realtime", realTime, true, false, GWorld->GetMissionNamespace());

  RString objName;
  if (obj) 
  {
    objName = obj->GetArgument("VARIABLE_NAME");

    GameValue result = gstate->Evaluate(objName, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    gstate->VarLocal("_this");
    gstate->VarSet("_this", result, true, false, GWorld->GetMissionNamespace());

    gstate->VarLocal("_scope");
    gstate->VarSet("_scope", (float)obj->GetScope(), true, false, GWorld->GetMissionNamespace());
  }

  gstate->VarLocal("_allow3D"); // access to scripting functions on the workspace
  gstate->VarSet("_allow3D", allow3DMode, true, false, GWorld->GetMissionNamespace());

  gstate->VarLocal("_overTree"); // access to scripting functions on the workspace
  gstate->VarSet("_overTree", overTree, true, false, GWorld->GetMissionNamespace());

  gstate->VarLocal("_edObj"); // access to scripting functions on the workspace
  gstate->VarSet("_edObj", objName, true, false, GWorld->GetMissionNamespace());

  bool isValid = gstate->EvaluateBool(condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace());

  gstate->EndContext(); // local space
  gstate->EndContext(); // editor space

  return isValid;
}

struct SubTypeContainer
{
  Ref<EditorObjectType> subType; 
  // main parameter name from editor definitions for the current type of selected object
  RString paramName;  
  // name of subparameter - ie what is displayed in main parameter combo box
  RString paramSubType;
};
TypeIsMovableZeroed(SubTypeContainer)

struct EditArgument
{
  RString name;
  Ref<Control> control;
  Ref<CStatic> title;
  RString type;

  // subparam related
  // a pointer to the parent argument if this control is for a subtype
  const EditArgument *parentArgument;
  
  // a pointer to the SubTypeContainer that matches the currently selected option
    // valid only for combo boxes that control subtypes
  const SubTypeContainer *subTypeSelected;
  //-!

  bool IsValid(Display *parent) const; // control level validation
  RString GetValue() const;
};
TypeIsMovableZeroed(EditArgument)

bool EditArgument::IsValid(Display *parent) const
{
  switch (control->GetType())
  {
  case CT_EDIT:
    {
      CEditContainer *edit = GetEditContainer(control);
      Assert(edit);
      if (control->IsVisible() && (stricmp(type,"varname") == 0 || stricmp(type,"text") == 0))
      {
        if (edit->GetText().Find("\"") > -1 || edit->GetText().Find("'") > -1)
        {
          parent->CreateMsgBox(MB_BUTTON_OK, LocalizeString("STR_VBS2_EDITOR_WARNING_NO_INV_COMMAS"));
          return false;
        }
      }
    }
  case CT_TREE:
    {
#if !_VBS3
      CTree *tree = static_cast<CTree *>(control.GetRef());
      Assert(tree);
      CTreeItem *selected = tree->GetSelected();
      if (!selected || selected->value == 0)
      {
        parent->CreateMsgBox(MB_BUTTON_OK, Format(LocalizeString("STR_EDITOR_ERROR_TREE_SEL"), (const char *)name));
        return false;
      }
#endif
    }
    break;
  case CT_LISTBOX:
  case CT_XLISTBOX:
    {
#if !_VBS3
      CListBoxContainer *list = GetListBoxContainer(control);
      Assert(list);
      int sel = list->GetCurSel();
      if (sel < 0)
      {
        parent->CreateMsgBox(MB_BUTTON_OK, Format(LocalizeString("STR_EDITOR_ERROR_TREE_ITEM"), (const char *)name));
        return false;
      }
#endif
    }
    break;
  case CT_COMBO:
  case CT_XCOMBO:
    {
      CListBoxContainer *list = GetListBoxContainer(control);
      Assert(list);
      int sel = list->GetCurSel();
      if (sel < 0)
      {
        parent->CreateMsgBox(MB_BUTTON_OK, Format(LocalizeString("STR_EDITOR_ERROR_TREE_ITEM"), (const char *)name));
        return false;
      }
    }
    break;
  }
  return true;
}

RString EditArgument::GetValue() const
{
  switch (control->GetType())
  {
  case CT_STATIC:
    {
      CStatic *staticCtrl = static_cast<CStatic *>(control.GetRef());
      Assert(staticCtrl);
      return staticCtrl->GetText();
    }
  case CT_EDIT:
    {
      CEditContainer *edit = GetEditContainer(control);
      Assert(edit);
      return edit->GetText();
    }
  case CT_LISTBOX:
  case CT_XLISTBOX:
  case CT_COMBO:
  case CT_XCOMBO:
    {
      CListBoxContainer *list = GetListBoxContainer(control);
      Assert(list);
      int sel = list->GetCurSel();
      if (sel < 0) return RString();
      return list->GetData(sel);
    }
  case CT_SLIDER:
  case CT_XSLIDER:
    {
      CSliderContainer *slider = GetSliderContainer(control);
      Assert(slider);
      float val = slider->GetThumbPos();
      return Format("%.5f", val);
    }
  case CT_TREE:
    {
      CTree *tree = static_cast<CTree *>(control.GetRef());
      Assert(tree);
      CTreeItem *selected = tree->GetSelected();
      if (!selected || selected->value == 0) return RString();
      return selected->data;
    }
  }
  return RString();
}

class DisplayEditObject : public Display
{
protected:
  Ref<CStaticMapEditor> _map; // used for scripting access to editor workspace
  Ref<EditorObject> _object;
  
  AutoArray<SubTypeContainer> _subTypes;

  AutoArray<EditArgument> _arguments;
  bool _update;

  // used to link the new object once it is created (when control is destroyed)
  Ref<EditorObject> _linkObject;
  RString _linkArgument;

  Control *_toggleButton;
  bool _togglePressed;

  // filter
  RString _filter;

  // move view to bottom of window?
  bool _moveViewToBottom;

  // preview picture
  CStatic *_previewPic;

public:
  DisplayEditObject(
    ControlsContainer *parent, 
    CStaticMapEditor *map, 
    EditorObject *obj, 
    bool update, 
    EditorObject *linkObj = NULL, 
    RString linkArg = "", 
    RString className = "",
    TargetSide side = TSideUnknown
  );

  Control *AddControl(CControlsGroup *grp, const EditorParam &param, float &top, TargetSide side = TSideUnknown);

  EditorObject *GetObject() {return _object;}
  bool IsUpdate() const {return _update;}
  RString GetFilter() {return _filter;}
  
  EditorObject *LinkToObject(RString &linkType) {linkType = _linkArgument; return _linkObject;}

  virtual void OnLBSelChanged(IControl *ctrl, int curSel) {OnChanged(ctrl);}
  virtual void OnTreeSelChanged(IControl *ctrl) {OnChanged(ctrl);}
  virtual void OnTreeMouseMove(int idc, CTreeItem *sel);
  virtual void OnTreeMouseHold(int idc, CTreeItem *sel);
  virtual void OnTreeMouseExit(int idc, CTreeItem *sel);
  virtual void OnSliderPosChanged(IControl *ctrl, float pos) {OnChanged(ctrl);}

  void OnDraw(EntityAI *vehicle, float alpha);

  void OnButtonClicked(int idc);
  virtual bool CanDestroy();
  virtual void Destroy();

protected:
  void AddArgument(RString name, Control *control, CStatic *title, RString type);
  void OnChanged(IControl *ctrl);
};

void DisplayEditObject::OnDraw(EntityAI *vehicle, float alpha)
{
  Display::OnDraw(vehicle, alpha);

  if (_moveViewToBottom)
  {
    IControl *ctrl = GetCtrl(IDC_EDIT_OBJECT_CONTROLS);
    if (ctrl && ctrl->GetType() == CT_CONTROLS_GROUP)
    {
      CControlsGroup *grp = static_cast<CControlsGroup *>(ctrl);
      grp->SetViewPos(false,1);
    }
    _moveViewToBottom = false;
  }
}

static RString Trim(const char *ptr, int len)
{
  while (*ptr && isspace(*ptr))
  {
    ptr++; len--;
  }
  while (isspace(ptr[len - 1]))
  {
    len--;
  }
  return RString(ptr, len);
}

static bool ProcessPair(const char *ptr, RString &s1, RString &s2, char ch)
{
  if (*ptr != '(') return false;
  if (*(ptr + strlen(ptr) - 1) != ')') return false;
  ptr++;
  const char *ext = strchr(ptr, ch);
  if (!ext) return false;
  s1 = Trim(ptr, ext - ptr);
  ptr = ext + 1;
  s2 = Trim(ptr, strlen(ptr) - 1);
  return true;
}

static void ProcessConfigSubtype(const char *ptr, RString &config, RString &entryName, CCombo *combo)
{
  // config name
  const char *ext = strchr(ptr, ',');
  if (!ext)
  {
    config = Trim(ptr, strlen(ptr));
    entryName = "displayName";
    return;
  }
  config = Trim(ptr, ext - ptr);
  
  // entry name
  ptr = ext + 1;
  ext = strchr(ptr, ',');
  if (!ext)
  {
    entryName = Trim(ptr, strlen(ptr));
    return;
  }
  entryName = Trim(ptr, ext - ptr);

  // additional parameters
  while (ext)
  {
    ptr = ext + 1;
    ext = strchr(ptr, ',');
    RString param;
    if (!ext) param = Trim(ptr, strlen(ptr));
    else param = Trim(ptr, ext - ptr);
    const char *p = param;
    if (*p == '+')
    {
      p++;
      RString data;
      RString str;
      if (ProcessPair(p, data, str, '|') && combo)
      {
        int index = combo->AddString(LocalizeString(str));
        combo->SetData(index, data);
      }
    }
  }
}

static ConstParamEntryPtr FindConfigEntry(const ParamClass &cls, const char *path)
{
  ConstParamEntryPtr entry(&cls);
  while (entry)
  {
    const char *ext = strchr(path, '.');
    if (!ext) return entry->FindEntry(path);
    entry = entry->FindEntry(RString(path, ext - path));
    path = ext + 1;
  }
  return NULL;
}

// TODO: add hierarchy based on... side?
static void FillMarkerTree(CTree *tree, RString value)
{
  CTreeItem *root = tree->GetRoot();

  ParamEntryVal cls = Pars >> "CfgMarkers";
  for (int i=0; i<cls.GetEntryCount(); i++)
  {
    ParamEntryVal entry = cls.GetEntry(i);
    if (!entry.IsClass()) continue;

    CTreeItem *item = root->AddChild();
    item->texture = GlobPreloadTexture(GetPictureName(entry >> "icon"));
    item->text = entry >> "name";
    item->data = entry.GetName();
    item->value = 1;  // valid
  }

  for (int i=0; i<root->children.Size(); i++)
  {
    CTreeItem *group = root->children[i];
    group->SortChildren();

    if (group->children.Size() == 0)
      if (stricmp(group->data, value) == 0)
      {
        tree->SetSelected(group);      
        tree->EnsureVisible(group);
      }

    for (int j=0; j<group->children.Size(); j++)
    {
      CTreeItem *item = group->children[j];
      if (stricmp(item->data, value) == 0)
      {
        tree->SetSelected(item);
        CTreeItem *ptr = item;
        while (ptr)
        {
          tree->Expand(ptr);
          ptr = ptr->parent;
        }
        tree->EnsureVisible(item);
      }
    }
  }
}

static RString GetModelPreviewTexture(ParamEntryVal entry)
{
  RString path;
  if (entry.FindEntry("preview") || entry.FindEntry("model"))
  {
    path = entry.ReadValue("preview",RString());
    if(path.GetLength()) //no config entry found, look for model
    {
      const char *ptr = strchr(path, '.');
      if (!ptr)
        path = path +".paa";
    }
    else
    {
      RString model = entry >> "model";
      RString modelNoExt = model;

      const char *ptr = strchr(model, '.');
      if (ptr)
        modelNoExt = RString(model, ptr - model);

      ptr = strrchr(modelNoExt, '\\');
      if (ptr)
      {
        RString textName(ptr + 1); 
        path = RString(modelNoExt, ptr - modelNoExt);

        // full path to texture
        path = path + "\\data\\ico\\preview_" + textName + "_ca.paa";
      }
    }
  }
  return path;
}

static void FillVehicleTree(CTree *tree, TargetSide side, RString value, RString filter, bool menOnly = false, bool staticOnly = false, bool menAndVehiclesOnly = false)
{
  AutoArray<RString> excludeCountries;
  if (filter.GetLength() > 0)
  {
    ParamEntryVal cls = Pars >> "CfgEditorFilters"; 
    for (int i=0; i<cls.GetEntryCount(); i++)
    {
      ParamEntryVal entry = cls.GetEntry(i);
      if (!entry.IsClass() || stricmp(entry.GetName(), filter) != 0) continue;

      ConstParamEntryPtr toExclude = entry.FindEntry("excludeCountries");
      if (!toExclude) continue;

      for (int j=0; j<toExclude->GetSize(); j++)
      {
        RString exclude = (*toExclude)[j];
        excludeCountries.Add(exclude);
      }
    }
  }

  CTreeItem *root = tree->GetRoot();
  ParamEntryVal cls = Pars >> "CfgVehicles";
  for (int i=0; i<cls.GetEntryCount(); i++)
  {
    ParamEntryVal entry = cls.GetEntry(i);
    if (!entry.IsClass()) continue;
    int scope = entry >> "scope";
    if (scope != 2) continue;

    RString vehicleClass = entry >> "vehicleClass";
    if (vehicleClass.GetLength() == 0 || stricmp(vehicleClass, "sounds") == 0 || stricmp(vehicleClass, "mines") == 0) continue;    
    RString sim = entry >> "simulation";

    if (menAndVehiclesOnly) 
    {
      if (stricmp(sim, "invisible") == 0)
        continue;
    }
    else
    {
      if (menOnly && stricmp(sim, "soldier") != 0 && stricmp(sim, "invisible") != 0) continue; // diff
      if (!menOnly && (stricmp(sim, "soldier") == 0 || stricmp(sim, "invisible") == 0)) continue;
    }

    // filter by side
    int vehSide = entry >> "side";  
    if (side < TSideUnknown && side != (TargetSide)vehSide) continue;
    if (side == TLogic && side != (TargetSide)vehSide) continue;

    if (menAndVehiclesOnly && stricmp(sim, "soldier") != 0)
    {
      bool hasDriver = entry>>"hasDriver";
      bool hasGunner = HasPrimaryGunnerTurret(entry);
      bool hasCommander = HasPrimaryObserverTurret(entry);
      if (!hasDriver && !hasGunner && !hasCommander) continue;
    }
    else if (!menOnly)
    {
      bool hasDriver = entry>>"hasDriver";
      bool hasGunner = HasPrimaryGunnerTurret(entry);
      bool hasCommander = HasPrimaryObserverTurret(entry);
      if (staticOnly && (hasDriver || hasGunner || hasCommander)) continue;
      if (!staticOnly && !hasDriver && !hasGunner && !hasCommander) continue;
    }

    // filter by country
    RString country = (Pars >> "CfgVehicleClasses" >> vehicleClass).ReadValue("country",RString());
    if (country.GetLength() > 0)
    {
      int j; int n = excludeCountries.Size();
      for (j=0; j<n; j++)
        if (stricmp(excludeCountries[j],country) == 0)
          break;
      if (j < n) continue;
    }

    CTreeItem *found = NULL; 
    for (int i=0; i<root->children.Size(); i++)
    {
      CTreeItem *item = root->children[i];
      if (stricmp(item->data, vehicleClass) == 0)
      {
        found = item;
        break;
      }
    }
    if (!found)
    {
      found = root->AddChild();
      found->data = vehicleClass;
      found->value = 0; // invalid selection
      ConstParamEntryPtr ptr = (Pars >> "CfgVehicleClasses").FindEntry(vehicleClass);
      if (ptr) found->text = (*ptr) >> "displayName";
      else found->text = vehicleClass;
      // categorize game vehicle classes seperately from VBS2 classes
      bool isGame = true;
      if (ConstParamEntryPtr isVBS = entry.FindEntry("vbs_entity"))
        if (isVBS)
        {
          bool isVBS = entry >> "vbs_entity";          
          isGame = !isVBS;
        }
      if (isGame) found->text = LocalizeString("STR_EDITOR_GAME_CLASS") + " " + found->text;
    }

    CTreeItem *item = found->AddChild();
    item->data = entry.GetName();
    item->value = 1; // valid selection
    item->text = entry >> "displayName";

    // indicate which vehicles are editor defined
    bool isEditor = entry.ReadValue("isEditorVehicle",false);
    if (isEditor) item->text = item->text + "*";

    // preview picture
      // expected to be model path + \data\ico\preview_<model name>_ca.paa
    RString path = GetModelPreviewTexture(entry);
    if (path.GetLength() > 0 && QFBankQueryFunctions::FileExists(path + 1))
    {
      item->texture = GlobPreloadTexture(GetPictureName(path));
      item->drawTexture = false;  // don't draw icon in the tree, it will be drawn in preview pic
    }      
  }
  bool found = false;
  root->SortChildren();
  for (int i=0; i<root->children.Size(); i++)
  {
    CTreeItem *group = root->children[i];
    group->SortChildren();

    for (int j=0; j<group->children.Size(); j++)
    {
      CTreeItem *item = group->children[j];
      if (stricmp(item->data, value) == 0)
      {
        found = true;
        tree->SetSelected(item);
        CTreeItem *ptr = item;
        while (ptr)
        {
          tree->Expand(ptr);
          ptr = ptr->parent;
        }
        tree->EnsureVisible(item);
      }
    }
  }
  if (!found && value.GetLength() > 0)
  {
    ConstParamEntryPtr ptr = (Pars >> "CfgVehicleClasses").FindEntry(value);
    if (ptr) return;
    ParamEntryVal entry = Pars >> "CfgVehicles" >> value;
    if (entry.IsClass())
    {
      CTreeItem *item = root->AddChild();
      RString path = GetModelPreviewTexture(entry);
      if (path.GetLength() > 0 && QFBankQueryFunctions::FileExists(path + 1))
      {
        item->texture = GlobPreloadTexture(GetPictureName(path));
        item->drawTexture = false;
      }    
      item->text = entry >> "displayName";
      bool isEditor = entry.ReadValue("isEditorVehicle",false);
      if (isEditor) item->text = item->text + "*";
      item->data = entry.GetName();
      item->value = 1;  // valid
      root->SortChildren();
      tree->SetSelected(item);
      tree->EnsureVisible(item);
    }
  }
}

static Control *CreateCtrl(const EditorParam &param, RString value, Display *disp, EditorWorkspace *ws, EditorObject *obj, AutoArray<SubTypeContainer> &subTypes, TargetSide side, RString filter)
{
  if (stricmp(param.type, "enum") == 0)
  {
    const EnumName *names = NULL;
    AutoArray<RString> listText;
    if (stricmp(param.subtype, "ArcadeSensorActivation") == 0)
    {
      names = GetEnumNames(ArcadeSensorActivation());
      listText.Add(LocalizeString(IDS_SENSORACTIV_NONE));
      listText.Add(LocalizeString(IDS_EAST));
      listText.Add(LocalizeString(IDS_WEST));
      listText.Add(LocalizeString(IDS_GUERRILA));
      listText.Add(LocalizeString(IDS_CIVILIAN));
      listText.Add(LocalizeString(IDS_LOGIC));
      listText.Add(LocalizeString(IDS_SENSORACTIV_ANYBODY));
      listText.Add(LocalizeString(IDS_SENSORACTIV_ALPHA));
      listText.Add(LocalizeString(IDS_SENSORACTIV_BRAVO));
      listText.Add(LocalizeString(IDS_SENSORACTIV_CHARLIE));
      listText.Add(LocalizeString(IDS_SENSORACTIV_DELTA));
      listText.Add(LocalizeString(IDS_SENSORACTIV_ECHO));
      listText.Add(LocalizeString(IDS_SENSORACTIV_FOXTROT));
      listText.Add(LocalizeString(IDS_SENSORACTIV_GOLF));
      listText.Add(LocalizeString(IDS_SENSORACTIV_HOTEL));
      listText.Add(LocalizeString(IDS_SENSORACTIV_INDIA));
      listText.Add(LocalizeString(IDS_SENSORACTIV_JULIET));
      listText.Add(LocalizeString(IDS_EAST_SEIZED));
      listText.Add(LocalizeString(IDS_WEST_SEIZED));
      listText.Add(LocalizeString(IDS_GUERRILA_SEIZED));
    }
    else if (stricmp(param.subtype, "ArcadeSensorActivationType") == 0)
    {
      names = GetEnumNames(ArcadeSensorActivationType());
      listText.Add(LocalizeString("STR_DISP_ARCSENS_PRESYES"));
      listText.Add(LocalizeString("STR_DISP_ARCSENS_PRESNO"));
      listText.Add(LocalizeString("STR_DISP_ARCSENS_DETWEST"));
      listText.Add(LocalizeString("STR_DISP_ARCSENS_DETEAST"));
      listText.Add(LocalizeString("STR_DISP_ARCSENS_DETGUERRILA"));
      listText.Add(LocalizeString("STR_DISP_ARCSENS_DETCIVILIAN"));
    }
    else if (stricmp(param.subtype, "ArcadeSensorType") == 0)
    {
      names = GetEnumNames(ArcadeSensorType());
      for (int i=0; i<ASTN; i++)
        listText.Add(LocalizeString(IDS_SENSORTYPE_NONE + i));
    }
    else if (stricmp(param.subtype, "ArcadeUnitAge") == 0)
    {
      names = GetEnumNames(ArcadeUnitAge());
      for (int i=0; i<AAN; i++)
        listText.Add(LocalizeString(IDS_AGE_ACTUAL + i));
    }
    else if (stricmp(param.subtype, "CamEffectPosition") == 0)
    {
      names = GetEnumNames(CamEffectPosition());
    }
    else if (stricmp(param.subtype, "TitleType") == 0)
    {
      names = GetEnumNames(TitleType());
    }
    else if (stricmp(param.subtype, "TitEffectName") == 0)
    {
      names = GetEnumNames(TitEffectName());
    }
    else if (stricmp(param.subtype, "MarkerType") == 0)
    {
      names = GetEnumNames(MarkerType());
    }
    else if (stricmp(param.subtype, "ArcadeUnitSpecial") == 0)
    {
      names = GetEnumNames(ArcadeUnitSpecial());
      listText.Add(LocalizeString(IDS_SPECIAL_NONE));
      listText.Add(LocalizeString(IDS_SPECIAL_CARGO));
      listText.Add(LocalizeString(IDS_SPECIAL_FLYING));
      listText.Add(LocalizeString(IDS_SPECIAL_FORM));
      listText.Add("Can Collide");  // ToDo: Stringtable
    }
    else if (stricmp(param.subtype, "LockState") == 0)
    {
      names = GetEnumNames(LockState());
      listText.Add(LocalizeString(IDS_VEHICLE_UNLOCKED));
      listText.Add(LocalizeString(IDS_VEHICLE_DEFAULT));
      listText.Add(LocalizeString(IDS_VEHICLE_LOCKED));
    }
    else if (stricmp(param.subtype, "Rank") == 0)
    {
      names = GetEnumNames(Rank());
      listText.Add("Undefined");  // ToDo: Stringtable
      for (int i=RankPrivate; i<=RankColonel; i++)
      {
        RString rank = LocalizeString(IDS_PRIVATE + i);
        listText.Add(rank);
      }
    }
    else if (stricmp(param.subtype, "ArcadeWaypointType") == 0)
    {
      names = GetEnumNames(ArcadeWaypointType());
    }
    else if (stricmp(param.subtype, "Semaphore") == 0)
    {
      names = GetEnumNames(AI::Semaphore());
      listText.Add(LocalizeString(IDS_NO_CHANGE));
      for (int i=0; i<AI::NSemaphores; i++)
        listText.Add(LocalizeString(IDS_IGNORE + i));
    }
    else if (stricmp(param.subtype, "Formation") == 0)
    {
      names = GetEnumNames(AI::Formation());
      listText.Add(LocalizeString(IDS_NO_CHANGE));
      for (int i=0; i<AI::NForms; i++)
        listText.Add(LocalizeString(IDS_COLUMN + i));
    }
    else if (stricmp(param.subtype, "SpeedMode") == 0)
    {
      names = GetEnumNames(SpeedMode());
      listText.Add(LocalizeString(IDS_SPEED_UNCHANGED));
      listText.Add(LocalizeString(IDS_SPEED_LIMITED));
      listText.Add(LocalizeString(IDS_SPEED_NORMAL));
      listText.Add(LocalizeString(IDS_SPEED_FULL));
    }
    else if (stricmp(param.subtype, "CombatMode") == 0)
    {
      names = GetEnumNames(CombatMode());
      listText.Add(LocalizeString(IDS_COMBAT_UNCHANGED));
      listText.Add(LocalizeString(IDS_COMBAT_CARELESS));
      listText.Add(LocalizeString(IDS_COMBAT_SAFE));
      listText.Add(LocalizeString(IDS_COMBAT_AWARE));
      listText.Add(LocalizeString(IDS_COMBAT_COMBAT));
      listText.Add(LocalizeString(IDS_COMBAT_STEALTH));
    }
    else if (stricmp(param.subtype, "AWPShow") == 0)
    {
      names = GetEnumNames(AWPShow());
      listText.Add(LocalizeString("STR_DISP_ARCWP_SHOW_NEVER"));
      listText.Add(LocalizeString("STR_DISP_ARCWP_SHOW_EASY"));
      listText.Add(LocalizeString("STR_DISP_ARCWP_SHOW_ALWAYS"));
    }
    else
    {
      LogF("### ENUM: %s", (const char *)param.subtype);
      return NULL;
    }

    Assert(names);
    CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
    int sel = 0, i = 0;
    while (names->IsValid())
    {
      if (listText.Size() > 0 && i == listText.Size()) break;  // end of names
      int index;
      if (i < listText.Size())
        index = combo->AddString(listText[i++]);
      else
        index = combo->AddString(names->name);
      combo->SetData(index, names->name);
      combo->SetValue(index, names->value);
      if (stricmp(names->name, value) == 0) sel = index;
      names++;
    }
    combo->SetCurSel(sel, false);
    return combo;
  }
  else if (stricmp(param.type, "number") == 0)
  {
    {
      float from, to;
      if (sscanf(param.subtype, "range(%f,%f)", &from, &to) == 2)
      {
        CSlider *slider = new CSlider(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Slider");
        slider->SetRange(from, to);
        slider->SetSpeed(0.01 * (to - from), 0.1 * (to - from));
        float pos = strtod(value, NULL);
        slider->SetThumbPos(pos);
        return slider;
      }
    }
    {
      int from, to, step;
      if (sscanf(param.subtype, "list(%d,%d,%d)", &from, &to, &step) == 3)
      {
        CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
        int val = atoi(value);
        int sel = 0;
        for (int i=from; i<=to; i+=step)
        {
          RString value = Format("%d", i);
          int index = combo->AddString(value);
          combo->SetData(index, value);
          combo->SetValue(index, i);
          if (i == val) sel = index;
        }
        combo->SetCurSel(sel, false);
        return combo;
      }
    }
  }
  else if (stricmp(param.type, "bool") == 0)
  {
    CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
    RString t = "true";
    RString f = "false";
    if (ProcessPair(param.subtype, t, f, ','))
    {
      t = LocalizeString(t);
      f = LocalizeString(f);
    }
    int index = combo->AddString(f);
    combo->SetData(index, "false");
    index = combo->AddString(t);
    combo->SetData(index, "true");
    if (stricmp(value, "true") == 0) combo->SetCurSel(1, false);
    else combo->SetCurSel(0, false);
    return combo;
  }
  else if (stricmp(param.type, "button") == 0 || stricmp(param.type, "buttonToggle") == 0)
  {
     CButton *button = new CButton(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Button");
     return button;
  }
  else if (stricmp(param.type, "list") == 0)
  {
     CListBox *list = new CListBox(disp, param.idc, Pars >> "RscDisplayEditObject" >> "List");
     return list;
  }
  // preview picture (linked to a tree or list with icons)
  else if (stricmp(param.type, "preview") == 0)
  {
     CStatic *picture = new CStatic(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Picture");
     return picture;
  }
  else if (stricmp(param.type, "picture") == 0)
  {
     CStatic *picture = new CStatic(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Picture");
     return picture;
  }
  else if (stricmp(param.type, "static") == 0)
  {
     CStatic *box = new CStatic(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Static");
     box->SetText(value);
     return box;
  }
  else if (stricmp(param.type, "filter") == 0)
  {
    int sel = 0;
    // read available filters
    CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
    ParamEntryVal cls = Pars >> "CfgEditorFilters";
    for (int i=0; i<cls.GetEntryCount(); i++)
    {
      ParamEntryVal entry = cls.GetEntry(i);
      if (!entry.IsClass()) continue;
      i = combo->AddString(entry >> "name");
      combo->SetData(i, entry.GetName());
      if (stricmp(filter,entry.GetName()) == 0) sel = i;
    }
    combo->SetCurSel(sel, false);
    return combo;
  }
  else if (stricmp(param.type, "side") == 0)
  {
    CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
    int i = combo->AddString(LocalizeString(IDS_GAME_TYPE_ANY));
    int sel = 0;
    combo->SetData(i, "all");    
    i = combo->AddString(LocalizeString(IDS_WEST));
    if (side == TWest || stricmp(value, "west") == 0) sel = i;
    combo->SetData(i, "west");
    i = combo->AddString(LocalizeString(IDS_EAST));    
    if (side == TEast || stricmp(value, "east") == 0) sel = i;
    combo->SetData(i, "east");
    i = combo->AddString(LocalizeString(IDS_GUERRILA));
    if (side == TGuerrila || stricmp(value, "guer") == 0) sel = i;
    combo->SetData(i, "guer");
    i = combo->AddString(LocalizeString(IDS_CIVILIAN));
    if (side == TCivilian || stricmp(value, "civ") == 0) sel = i;
    combo->SetData(i, "civ");
    i = combo->AddString(LocalizeString(IDS_LOGIC));
    if (side == TLogic || stricmp(value, "logic") == 0) sel = i;
    combo->SetData(i, "logic");
    combo->SetCurSel(sel, false);
    return combo;
  }
  else if (stricmp(param.type, "config") == 0)
  {
    RString config;
    RString entryName;

    CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
    ProcessConfigSubtype(param.subtype, config, entryName, combo);

    ConstParamEntryPtr array = FindConfigEntry(Pars, config);
    if (array && array->IsClass())
    {
      int n = array->GetEntryCount();
      for (int i=0; i<n; i++)
      {
        ParamEntryVal entry = array->GetEntry(i);
        if (!entry.IsClass()) continue;
        if (!entry.FindEntry(entryName)) continue;

        // hidden?
        if (entry.CheckIfEntryExists("hidden"))
            if (entry >> "hidden")
              continue;

        // filter by side
        if (entry.CheckIfEntryExists("side"))
        {
          int entrySide = entry >> "side";        
          if (side < TSideUnknown && side != (TargetSide)entrySide) continue;
        }

        int index = combo->AddString(entry >> entryName);
        combo->SetData(index, entry.GetName());
      }
    }

    int sel = 0;
    for (int i=0; i<combo->GetSize(); i++)
    {
      if (stricmp(value, combo->GetData(i)) == 0)
      {
        sel = i;
        break;
      }
    }
    combo->SetCurSel(sel, false);
    return combo;
  }
  else if (stricmp(param.type, "configEx") == 0)
  {
    RString config;
    RString entryName;

    CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
    ProcessConfigSubtype(param.subtype, config, entryName, combo);

    for (int j=0; j<3; j++)
    {
      ParamFile *pFile = &Pars;
      if (j == 1) 
        pFile = &ExtParsCampaign;
      else if (j == 2)
        pFile = &ExtParsMission;
      ConstParamEntryPtr array = FindConfigEntry(*pFile, config);
      if (array && array->IsClass())
      {
        int n = array->GetEntryCount();
        for (int i=0; i<n; i++)
        {
          ParamEntryVal entry = array->GetEntry(i);
          if (!entry.IsClass()) continue;
          if (!entry.FindEntry(entryName)) continue;

          // hidden?
          if (entry.CheckIfEntryExists("hidden"))
             if (entry >> "hidden")
               continue;

          // filter by side
          if (entry.CheckIfEntryExists("side"))
          {
            int entrySide = entry >> "side";        
            if (side < TSideUnknown && side != (TargetSide)entrySide) continue;
          }

          int index = combo->AddString(entry >> entryName);
          combo->SetData(index, entry.GetName());

          // find subparameters and create a new type if required
          if (entry.CheckIfEntryExists("Params"))
          {
            int l = subTypes.Size();
            subTypes.Add();
            subTypes[l].subType = new EditorObjectType(RString(),entry);
            subTypes[l].paramName = param.name;
            subTypes[l].paramSubType = entry.GetName();
          }
          //-!
        }
      }
    }

    int sel = 0;
    for (int i=0; i<combo->GetSize(); i++)
    {
      if (stricmp(value, combo->GetData(i)) == 0)
      {
        sel = i;
        break;
      }
    }
    combo->SetCurSel(sel, false);
    return combo;
  }
  else if (stricmp(param.type, "special") == 0)
  {
    if (stricmp(param.subtype, "trigger") == 0)
    {
      CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
      ParamEntryVal objects = Pars >> "CfgDetectors" >> "objects";
      int sel = 0;
      for (int i=0; i<objects.GetSize(); i++)
      {
        RString objectName = objects[i];
        if (stricmp(objectName, value) == 0) sel = i;
        ParamEntryVal objectCls = Pars >> "CfgNonAIVehicles" >> objectName;
        RString displayName = objectCls >> "displayName";
        int index = combo->AddString(displayName);
        combo->SetData(index, objectName);
      }
      combo->SetCurSel(sel, false);
      return combo;
    }
    else if (stricmp(param.subtype, "waypointType") == 0)
    {
      CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
      int sel = 0; const EnumName *names = GetEnumNames(ArcadeWaypointType());
      for (int i=ACMOVE; i<ACN; i++)
      {
        RString type = LocalizeString(IDS_AC_MOVE + i - ACMOVE);
        int index = combo->AddString(type);        
        combo->SetData(index, names[i].name);
        if (stricmp(value, names[i].name) == 0) sel = index;
      }
      combo->SetCurSel(sel);
      return combo;
    }
    else if (stricmp(param.subtype, "unitClass") == 0)
    {
      CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");

      // get side combo
/*
      CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
      if (combo2)
        side = combo2->GetValue(combo2->GetCurSel());
*/
      // otherwise side reverts to what was passed in?

      ParamEntryVal clsVeh = Pars >> "CfgVehicles";
      int n = clsVeh.GetEntryCount();
      FindArrayRStringBCI vehClasses;
      ConstParamEntryPtr manCls = clsVeh.FindEntry("Man");

      // scan all CfgVehicles for vehicleClass list
      for (int i=0; i<n; i++)
      {
        ParamEntryVal vehEntry = clsVeh.GetEntry(i);
        if (!vehEntry.IsClass()) continue;
        int scope = vehEntry >> "scope";
        if (scope != 2) continue;
        int vehSide = vehEntry >> "side";
        if (side != vehSide) continue;
        RString sim = vehEntry >> "simulation";
        if (stricmp(sim, "soldier") != 0 && stricmp(sim, "invisible") != 0) continue;
        
        RStringB name = vehEntry >> "vehicleClass";
        if (name.GetLength() == 0) continue;
        int index = vehClasses.FindKey(name);
        if (index<0) index = vehClasses.AddUnique(name);
      }
/*
      RStringB vehicleClass;
      if (vehicle && *vehicle)
      {
        vehicleClass = Pars >> "CfgVehicles" >> vehicle >> "vehicleClass";
      }

      // note: if "men" class is present, we want it to be first
      // other classes should be sorted alphabetically
      QSort(vehClasses.Data(),vehClasses.Size(),CmpClass);
*/
      combo->ClearStrings();
      int sel = 0;
      n = vehClasses.Size();
      for (int i=0; i<n; i++)
      {
        //if (classCounts[i] == 0) continue;
        RStringB name = vehClasses[i];
        int index = combo->AddString(Pars >> "CfgVehicleClasses" >> name >> "displayName");
        combo->SetData(index, name);
        //if (stricmp(vehicleClass, name) == 0) sel = index;
      }
      combo->SetCurSel(sel);
      return combo;
    }
    else if (stricmp(param.subtype, "unit") == 0)
    {
      // find object side
      RString name = obj->GetArgument("GROUP");
      if (name.GetLength() == 0) return NULL;
      EditorObject *group = ws->FindObject(name);
      if (!group) return NULL;
      name = group->GetArgument("CENTER");
      if (name.GetLength() == 0) return NULL;
      EditorObject *center = ws->FindObject(name);
      if (!center) return NULL;
      name = center->GetArgument("SIDE");
      GameState *state = GWorld->GetGameState();
      GameVarSpace local(false);
      state->BeginContext(&local);
      GameValue result = state->Evaluate(name, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      state->EndContext();
      if (result.GetType() != GameSide) return NULL;
      TargetSide side = GetSide(result);

      CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
      ParamEntryVal cls = Pars >> "CfgVehicles";
      for (int i=0; i<cls.GetEntryCount(); i++)
      {
        ParamEntryVal entry = cls.GetEntry(i);
        if (!entry.IsClass()) continue;
        int scope = entry >> "scope";
        if (scope != 2) continue;
        int vehSide = entry >> "side";
        if (vehSide != side) continue;
        RString vehicleClass = entry >> "vehicleClass";
        if (vehicleClass.GetLength() == 0 || stricmp(vehicleClass, "sounds") == 0 || stricmp(vehicleClass, "mines") == 0) continue;
        RString sim = entry >> "simulation";
        if (stricmp(sim, "soldier") != 0 && stricmp(sim, "invisible") != 0) continue;

        RString displayName = entry >> "displayName";
        int index = combo->AddString(displayName);
        combo->SetData(index, entry.GetName());
      }
      combo->SortItems();

      int sel = 0;
      for (int i=0; i<combo->GetSize(); i++)
      {
        if (stricmp(value, combo->GetData(i)) == 0)
        {
          sel = i;
          break;
        }
      }
      combo->SetCurSel(sel, false);
      return combo;
    }
    else if (stricmp(param.subtype, "marker") == 0)
    {
      CTree *tree = new CTree(disp, param.idc, Pars >> "RscDisplayEditObject" >> "TreeWithIcons");
      FillMarkerTree(tree, value);
      return tree;
    }
    else if (stricmp(param.subtype, "agent") == 0)
    {
      CTree *tree = new CTree(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Tree");
      FillVehicleTree(tree, side, value, filter, true);
      return tree;
    }
    else if (stricmp(param.subtype, "vehicle") == 0)
    {
      CTree *tree = new CTree(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Tree");
      FillVehicleTree(tree, side, value, filter);
      return tree;
    }
    else if (stricmp(param.subtype, "agent_vehicle") == 0)
    {
      CTree *tree = new CTree(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Tree");
      FillVehicleTree(tree, side, value, filter, false, false, true);
      return tree;
    }
    else if (stricmp(param.subtype, "object") == 0)
    {
      CTree *tree = new CTree(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Tree");
      FillVehicleTree(tree, side, value, "", false, true);
      return tree;
    }
    else if (stricmp(param.subtype, "collection") == 0)
    {
      CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");
      ParamEntryVal cls = Pars >> "CfgVehicles";
      for (int i=0; i<cls.GetEntryCount(); i++)
      {
        ParamEntryVal entry = cls.GetEntry(i);
        if (!entry.IsClass()) continue;
        int scope = entry >> "scope";
        if (scope != 2) continue;
        RString vehicleClass = entry >> "vehicleClass";
        if (vehicleClass.GetLength() == 0 || stricmp(vehicleClass, "sounds") == 0 || stricmp(vehicleClass, "mines") == 0) continue;
        RString sim = entry >> "simulation";
        if (stricmp(sim, "collection") != 0 && stricmp(sim, "thing") != 0 ) continue;

        RString displayName = entry >> "displayName";
        int index = combo->AddString(displayName);
        combo->SetData(index, entry.GetName());
      }
      combo->SortItems();

      int sel = 0;
      for (int i=0; i<combo->GetSize(); i++)
      {
        if (stricmp(value, combo->GetData(i)) == 0)
        {
          sel = i;
          break;
        }
      }
      combo->SetCurSel(sel, false);
      return combo;
    }
    else if (stricmp(param.subtype, "mine") == 0)
    {
      CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");

      ParamEntryVal cls = Pars >> "CfgVehicles";
      for (int i=0; i<cls.GetEntryCount(); i++)
      {
        ParamEntryVal entry = cls.GetEntry(i);
        if (!entry.IsClass()) continue;
        int scope = entry >> "scope";
        if (scope != 2) continue;
        RString vehicleClass = entry >> "vehicleClass";
        if (stricmp(vehicleClass, "mines") != 0) continue;
        RString displayName = entry >> "displayName";
        int index = combo->AddString(displayName);
        combo->SetData(index, entry.GetName());
      }
      combo->SortItems();

      int sel = 0;
      for (int i=0; i<combo->GetSize(); i++)
      {
        if (stricmp(value, combo->GetData(i)) == 0)
        {
          sel = i;
          break;
        }
      }
      combo->SetCurSel(sel, false);
      return combo;
    }
    else if (stricmp(param.subtype, "soundSource") == 0)
    {
      CCombo *combo = new CCombo(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Combo");

      ParamEntryVal cls = Pars >> "CfgVehicles";
      for (int i=0; i<cls.GetEntryCount(); i++)
      {
        ParamEntryVal entry = cls.GetEntry(i);
        if (!entry.IsClass()) continue;
        int scope = entry >> "scope";
        if (scope != 2) continue;
        RString vehicleClass = entry >> "vehicleClass";
        if (stricmp(vehicleClass, "sounds") != 0) continue;
        RString displayName = entry >> "displayName";
        int index = combo->AddString(displayName);
        combo->SetData(index, entry.GetName());
      }
      combo->SortItems();

      int sel = 0;
      for (int i=0; i<combo->GetSize(); i++)
      {
        if (stricmp(value, combo->GetData(i)) == 0)
        {
          sel = i;
          break;
        }
      }
      combo->SetCurSel(sel, false);
      return combo;
    }
    return NULL;
  }

  CEdit *edit = new CEdit(disp, param.idc, Pars >> "RscDisplayEditObject" >> "Edit");
  edit->SetText(value);
  return edit;
}

class FindVehicleFunc
{
protected:
  RString _id;
  mutable const CTreeItem *_found;

public:
  FindVehicleFunc(RString id) {_id = id; _found = NULL;}
  const CTreeItem *GetFound() const {return _found;}
  bool operator () (const CTreeItem *item) const
  {
    if (stricmp(item->data, _id) == 0)
    {
      _found = item;
      return true; // stop searching now
    }
    return false;
  }
};

bool ParseUserParams(ParamFile &cfg, GameDataNamespace *globals);
void SaveUserParams(ParamFile &cfg);

DisplayEditObject::DisplayEditObject(
  ControlsContainer *parent, 
  CStaticMapEditor *map, 
  EditorObject *obj, 
  bool update, 
  EditorObject *linkObj, 
  RString linkArg, 
  RString className,
  TargetSide side
)
: Display(parent)
{    
  Load("RscDisplayEditObject");

  SetCursor("Wait");
  DrawCursor();

  //_enableSimulation = map->IsRealTime();
  //_enableDisplay = true;

  _linkObject = linkObj;
  _linkArgument = linkArg;

  _map = map;
  _object = obj;
  _update = update;

  _toggleButton = NULL;
  _togglePressed = false;

  _moveViewToBottom = false;
  _previewPic = NULL;

  GameDataNamespace globals(NULL, RString(), false); // save - no global namespace
  ParamFile cfg;
  ParseUserParams(cfg, &globals);
  ConstParamEntryPtr entry = cfg.FindEntry("editorFilter");
  if (entry) _filter = *entry;

  // add controls
  IControl *ctrl = GetCtrl(IDC_EDIT_OBJECT_CONTROLS);
  if (ctrl && ctrl->GetType() == CT_CONTROLS_GROUP)
  {
    CControlsGroup *grp = static_cast<CControlsGroup *>(ctrl);

    float top = 0.02;
    const EditorParams &params = obj->GetType()->GetParams();   

    int n = params.Size();
    for (int i=0; i<n; i++)
    {
      const EditorParam &param = params[i];

      int nParent = _arguments.Size();
      
      Control *ctrl = NULL;
      ctrl = AddControl(grp,param,top,side); 

      if (ctrl && !_previewPic && ctrl->GetStyle() & ST_PICTURE && stricmp(param.type,"preview") == 0)
        _previewPic = dynamic_cast<CStatic *>(ctrl);

      // did we add a toggle button?
      if (ctrl && ctrl->GetType() == CT_BUTTON)
      {
        if (!_toggleButton && stricmp(param.type,"buttonToggle") == 0)
        {
          _toggleButton = ctrl;
          
          // toggle button defaults to OFF?
          if (_update)
          {
            RString isAdvanced = obj->GetArgument("IS_ADVANCED");
            if (stricmp(isAdvanced,"true") == 0)
              _togglePressed = true;     
          }
          else  // look at default setting for advanced
          {
            for (int j=0; j<n; j++)
            {
              const EditorParam &param = params[j];
              if (stricmp(param.name,"IS_ADVANCED") == 0)
                if (stricmp(param.defValue,"true") == 0)
                {
                  _togglePressed = true;
                  break;
                }
            }
          }
        }
      }        

      // add controls for subparameters
      //
      // parameters of type "configEx" can define their own subparameters that can be changed in the dialog
      // subparameters are added to object parameters when dialog is closed
      // example (main editor object definition):
      //
      // class Params
      // {
      //   class TYPE
      //   {
      //     type = configEx;
      //     subtype = CfgTasks, name;
      //     ...
      //   };
      //   ...
      // };
      //
      // CfgTasks:
      //
      // class CfgTasks
      // {
      //   class Move
      //   {
      //     name = "Move";   
      //     ...
      //     class Params
      //     {
      //       class SUBPARAM1
      //       {
      //         type = text;
      //         canChange = false;
      //         default = "a subparameter";
      //       };
      //     };
      //     ...
      //   };
      //   ...
      // };
      // in this example a SUBPARAM1 text box would be displayed under the TYPE combo on the dialog
      // limitations - subparams must be named differently and subparam subparams won't be processed

      if (stricmp(param.type, "configEx") == 0)
      {
        CCombo *combo = static_cast<CCombo *>(ctrl);
        if (combo)
        {
          int nSubTypes = _subTypes.Size();
          for (int j=0; j<nSubTypes; j++)
          {
            if (
              param.name == _subTypes[j].paramName &&
              combo->GetData(combo->GetCurSel()) == _subTypes[j].paramSubType
            )
            {
              const EditorParams &subParams = _subTypes[j].subType->GetParams();   

              // add controls for the subparameters
              for (int k=0; k<subParams.Size(); k++)
              {
                const EditorParam *subParam = &subParams[k];

                // search object params for subparameter value (in case this subparameter was previously set)
                const EditorObjectType *existingSubType = obj->GetSubType();
                if (existingSubType)
                {                  
                  const EditorParams &params = existingSubType->GetParams();   
                  const EditorParam *existingSubParam = params.Find(subParam->name);                
                  if (existingSubParam)
                    subParam = existingSubParam;
                }

                // add control for subparameter
                int n = _arguments.Size();
                AddControl(grp,*subParam,top);         

                // store the parent argument for this control
                if (_arguments.Size() > n) _arguments[n].parentArgument = &_arguments[nParent];
              }

              // update parent editargument to point to the active subparameters 
                // (will change every time parent combo box is changed) - see OnChanged()
              for (int k=0; k<_arguments.Size(); k++)
                if (_arguments[k].control == ctrl)
                {
                  _arguments[k].subTypeSelected = &_subTypes[j];                 
                  break;
                }
              break;
            }
          }
        }
      }
    }

    grp->UpdateScrollbars();

    // search for tree control and process preview pic
    for (int i=0; i<_arguments.Size(); i++)
    {
      Control *ctrl = _arguments[i].control;
      if (ctrl->GetType() == CT_TREE)
      {
        CTree *tree = static_cast<CTree *>(ctrl);
        Assert(tree);
        OnTreeMouseMove(-1, tree->GetSelected());
      }
    }

    // handling of onInit events
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(_map->GetVariables()); // editor space
    GameVarSpace local(_map->GetVariables(), false);
    gstate->BeginContext(&local); // local space

    // initialization of variables
    gstate->VarLocal("_map"); // access to scripting functions on the workspace
    gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());
    for (int i=0; i<_arguments.Size(); i++)
    {
      RString name = RString("_control_") + _arguments[i].name;
      gstate->VarLocal(name);
      gstate->VarSet(name, CreateGameControl(_arguments[i].control), true, false, GWorld->GetMissionNamespace());
      name = RString("_control_title_") + _arguments[i].name;
      gstate->VarLocal(name);
      gstate->VarSet(name, CreateGameControl(_arguments[i].title), true, false, GWorld->GetMissionNamespace());
    }
    gstate->VarLocal("_linkArgument");
    gstate->VarSet("_linkArgument", _linkArgument, true, false, GWorld->GetMissionNamespace());
    gstate->VarLocal("_linkObject");
    if (_linkObject)
      gstate->VarSet("_linkObject", _linkObject->GetArgument("VARIABLE_NAME"), true, false, GWorld->GetMissionNamespace());
    else
      gstate->VarSet("_linkObject", RString(), true, false, GWorld->GetMissionNamespace());
    gstate->VarLocal("_this");
    gstate->VarLocal("_edObj");
    if (_update && _object) // _this == proxy
    {
      gstate->VarSet("_this", GameValueExt(unconst_cast(_object->GetProxyObject())), true, false, GWorld->GetMissionNamespace());
      gstate->VarSet("_edObj", _object->GetArgument("VARIABLE_NAME"), true, false, GWorld->GetMissionNamespace());
    }
    else
    {
      gstate->VarSet("_this", NOTHING, true, false, GWorld->GetMissionNamespace());
      gstate->VarSet("_edObj", RString(), true, false, GWorld->GetMissionNamespace());
    }

    if (_object)
    {
      gstate->VarLocal("_pos");
      gstate->VarSet("_pos", _object->GetArgument("POSITION"), true, false, GWorld->GetMissionNamespace());
    }

    // executing of handlers - subparameters
    for (int i=0; i<_arguments.Size(); i++)
    {
      const SubTypeContainer *subParamsContainer = _arguments[i].subTypeSelected;
      if (subParamsContainer) // contains subparams
      {
        const EditorParams &subParams = subParamsContainer->subType->GetParams();
        for (int j=0; j<subParams.Size(); j++)
        {
          const EditorParam &param = subParams[j];
          if (param.source == EPSDialog && param.onInit.GetLength() > 0)
            gstate->Execute(param.onInit, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
        }
      }
    }

    // executing of handlers - normal parameters
    for (int i=0; i<n; i++)
    {
      const EditorParam &param = params[i];
      if (param.source == EPSDialog && param.onInit.GetLength() > 0)
        gstate->Execute(param.onInit, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    }

    gstate->EndContext(); // local space
    gstate->EndContext(); // editor space

    if (!update)
    {
      //-- automatically select entry
      if (className.GetLength() > 0)
      {
        IControl *control = NULL;

        // find control for type
        for (int i=0; i<_arguments.Size(); i++)
          if (stricmp(_arguments[i].name,RString("type")) == 0)
             control = _arguments[i].control;
        
        // find matching class
        if (control)
        {
          CCombo *combo = dynamic_cast<CCombo *>(control);
          if (combo)
          {
            for (int i=0; i<combo->GetSize(); i++)
            {
              RString data = combo->GetData(i);
              if (stricmp(className, data) == 0)
              {
                combo->SetCurSel(i);
                break;
              }
            }
          }
          else
          {
            CTree *tree = dynamic_cast<CTree *>(control);
            if (tree)
            {
              FindVehicleFunc func(className);
              tree->ForEachItem(func);              
              const CTreeItem *found = func.GetFound();
              if (found)
              {
                CTreeItem *ptr = unconst_cast(found);
                tree->SetSelected(ptr);
                while (ptr)
                {
                  tree->Expand(ptr);
                  ptr = ptr->parent;
                }
              }
            }
          }
        }
      }
      //-!
    }
  }
  if (_toggleButton && _togglePressed)
  {
    _togglePressed = false;
    OnButtonClicked(IDC_EDIT_OBJECT_TOGGLE_BUTTON);
  }
  SetCursor("Arrow");
}

Control *DisplayEditObject::AddControl(CControlsGroup *grp, const EditorParam &param, float &top, TargetSide side)
{
  if (param.source != EPSDialog) return NULL;

  RString name = param.name;
  RString description = param.description.GetLength() > 0 ? param.description : name;
  RString value;
  if (stricmp(param.subtype, "multiple") == 0)
  {
    for (int j=0; j<_object->NArguments(); j++)
    {
      const EditorArgument &arg = _object->GetArgument(j);
      if (stricmp(arg.name, name) == 0)
      {
        if (value.GetLength() > 0) value = value + ", ";
        value = value + arg.value;
      }
    }
  }
  else value = _object->GetArgument(name);

  // no argument exists? might be a subparameter, assign default value
  if (value.GetLength() == 0 && param.hasDefValue)
    value = param.defValue;

  CStatic *title = new CStatic(this, -1, Pars >> "RscDisplayEditObject" >> "Title");
  title->SetText(description);
  title->SetPos(title->X(), top, title->W(), title->H());
  grp->AddControl(title);
  float h = title->H();

  bool positionAbs = false;

  // select control by param description
  Control *ctrl = CreateCtrl(param, value, this, _map->GetWorkspace(), _object, _subTypes, side, _filter);
  if (ctrl)
  {
    // absolute position specified?
    if (param.x != -1 || param.y != -1 || param.w != -1 || param.h != -1)
    {
      positionAbs = true;
      float x = param.x == -1 ? ctrl->X() : param.x;
      float y = param.y == -1 ? top : param.y; if (param.y == -1) positionAbs = false;
      float w = param.w == -1 ? ctrl->W() : param.w;
      float h = param.h == -1 ? ctrl->H() : param.h;
      ctrl->SetPos(x, y, w, h);      
    }
    else
      ctrl->SetPos(ctrl->X(), top, ctrl->W(), ctrl->H());
    grp->AddControl(ctrl);
    AddArgument(name, ctrl, title, param.type);

    bool canChangeCode = IsMenuItemActive(param.canChangeCondition, _object, (CStaticMapEditor *)_map.GetRef(), _map->IsRealTime());
    ctrl->EnableCtrl(!_update || (param.canChange && canChangeCode));
    saturateMax(h, ctrl->H());
  }

  bool isActive = IsMenuItemActive(param.activeMode, _object, (CStaticMapEditor *)_map.GetRef(), _map->IsRealTime());

  // hidden?
  if (!isActive || param.hidden || positionAbs || _toggleButton)
  {
    title->ShowCtrl(false);
    if (!isActive || param.hidden || _toggleButton) ctrl->ShowCtrl(false);
  }
  else
    top += h + 0.005;

  return ctrl;
}

void DisplayEditObject::OnButtonClicked(int idc)
{
  switch (idc)
  {
    case IDC_EDIT_OBJECT_TOGGLE_BUTTON:
      {
        // toggle button pressed, so show/hide hidden controls below it
        IControl *ctrl = GetCtrl(idc);
        if (ctrl && ctrl->GetType() == CT_BUTTON)
        {
          // find button in controls list
          const EditArgument *buttonArgument = NULL;          
          int i; for (i=0; i<_arguments.Size(); i++)
            if (_arguments[i].control == ctrl)
            {
              buttonArgument = &_arguments[i];
              break;
            }
          if (!buttonArgument) return;

          float top = 0.02;  
          if (i > 0)  // button is not the first argument
            top = _arguments[i-1].title->Y() + _arguments[i-1].title->H() + 0.005;

          const EditorParams &params = _object->GetAllParams();

          // find parameter of type buttonToggle 
          const EditorParam *buttonParam = NULL;
          for (int i=0; i<params.Size(); i++) // find parameter matching control
            if (stricmp(buttonArgument->name,params[i].name) == 0)
            {
              buttonParam = &params[i];
              break;
            }
          if (!buttonParam) return;

          // button expliticly positioned?
          if (buttonParam->y > -1)  
            top = buttonArgument->control->Y() + buttonArgument->control->H() + (0.005 * 3); // make top below the button

          // show all hidden controls
          for (int j=i+1; j<_arguments.Size(); j++)
          {     
            for (int i=0; i<params.Size(); i++) // find parameter matching control
            {
              if (params[i].name == _arguments[j].name)
              {
                Control *ctrl = _arguments[j].control;                
                Control *title = _arguments[j].title;

                if (_togglePressed) // hide all controls
                {
                  ctrl->SetPos(ctrl->X(), top, ctrl->W(), ctrl->H());
                  ctrl->ShowCtrl(false);
                  title->SetPos(title->X(), top, title->W(), title->H());
                  title->ShowCtrl(false);                    
                }
                else  // show all controls
                {                  
                  const EditorParam &param = params[i];
                  
                  bool positionAbs = false;
                  float h = title->H();

                  // absolute position specified?
                  if (param.x != -1 || param.y != -1 || param.w != -1 || param.h != -1)
                  {
                    positionAbs = true;
                    float x = param.x == -1 ? ctrl->X() : param.x;
                    float y = param.y == -1 ? top : param.y; if (param.y == -1) positionAbs = false;
                    float w = param.w == -1 ? ctrl->W() : param.w;
                    float h = param.h == -1 ? ctrl->H() : param.h;
                    ctrl->SetPos(x, y, w, h);   
                    title->SetPos(title->X(), y, title->W(), title->H());
                  }
                  else
                  {
                    ctrl->SetPos(ctrl->X(), top, ctrl->W(), ctrl->H());
                    title->SetPos(title->X(), top, title->W(), title->H());
                  }
                  saturateMax(h, ctrl->H());
                  
                  bool isActive = IsMenuItemActive(param.activeMode, _object, (CStaticMapEditor *)_map.GetRef(), _map->IsRealTime());

                  // hidden?
                  if (!isActive || param.hidden || positionAbs)
                  {
                    title->ShowCtrl(false);
                    ctrl->ShowCtrl(isActive && !param.hidden);
                  }
                  else
                  {
                    title->ShowCtrl(true);
                    ctrl->ShowCtrl(true);
                    top += h + 0.005;
                  }
                }
                break;
              }
            }
          }
          // move button to start or end, unless button position is explicitly defined
          if (buttonParam->y == -1)
          {
            Control *ctrl = buttonArgument->control;
            Control *title = buttonArgument->title;
            ctrl->SetPos(ctrl->X(), top, ctrl->W(), ctrl->H());
            title->SetPos(title->X(), top, title->W(), title->H());              
          }

          // move view (within controls group) to top or bottom depending on state of toggle button
          IControl *ctrl = GetCtrl(IDC_EDIT_OBJECT_CONTROLS);
          if (ctrl && ctrl->GetType() == CT_CONTROLS_GROUP)
          {
            CControlsGroup *grp = static_cast<CControlsGroup *>(ctrl);
            if (_togglePressed) // view to top
              grp->SetViewPos(false,0);
            else  // view to bottom
              _moveViewToBottom = true;
          }

          _togglePressed = !_togglePressed;
        }
      }
      return;
  }
  Display::OnButtonClicked(idc);
}

bool DisplayEditObject::CanDestroy()
{
  if (!Display::CanDestroy()) return false;

  if (_exit == IDC_OK)
  {
    // arguments level validation
    for (int i=0; i<_arguments.Size(); i++)
    {
      if (!_arguments[i].IsValid(this))
        return false;
    }

    // object level validation
    bool valid = true;

    // handling of onChanged event
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(_map->GetVariables()); // editor space
    GameVarSpace local(_map->GetVariables(), false);
    gstate->BeginContext(&local); // local space

    // initialization of variables
    gstate->VarLocal("_map"); // access to scripting functions on the workspace
    gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());
    gstate->VarLocal("_edit");
    gstate->VarSet("_edit", GameValue(_update), true, false, GWorld->GetMissionNamespace());
    gstate->VarLocal("_object");
    gstate->VarSet("_object", GameValue(_object->GetArgument("VARIABLE_NAME")), true, false, GWorld->GetMissionNamespace());
    for (int i=0; i<_arguments.Size(); i++)
    {
      RString name = RString("_control_") + _arguments[i].name;
      gstate->VarLocal(name);
      gstate->VarSet(name, CreateGameControl(_arguments[i].control), true, false, GWorld->GetMissionNamespace());
      name = RString("_control_title_") + _arguments[i].name;
      gstate->VarLocal(name);
      gstate->VarSet(name, CreateGameControl(_arguments[i].title), true, false, GWorld->GetMissionNamespace());
    }

    // executing of handlers    
    for (int i=0; i<_arguments.Size(); i++)
    {
      const EditorParams *params = &_object->GetType()->GetParams();
      const EditArgument &arg = _arguments[i];
      
      // is this a control for a subparameter?
      const EditArgument *parentArg = arg.parentArgument;
      if (parentArg)
        params = &parentArg->subTypeSelected->subType->GetParams();

      const EditorParam *param = params->Find(arg.name);
      Assert(param && param->source == EPSDialog);      

      RString value = arg.GetValue();
      RString message;

      if (stricmp(param->type,"code") == 0 || stricmp(param->type,"evalBool") == 0)
      {
        if (stricmp(param->type,"code") == 0)
          valid = gstate->CheckExecute(value, GWorld->GetMissionNamespace());
        else
          valid = gstate->CheckEvaluateBool(value, GWorld->GetMissionNamespace());
        if (!valid)
        {
          CEdit *edit = dynamic_cast<CEdit *>((IControl *)arg.control);
          if (edit)
          {
            FocusCtrl(edit);
            message = gstate->GetLastErrorText();
            edit->SetCaretPos(gstate->GetLastErrorPos(value));
          }
        }
      }

      if (param->valid.GetLength() > 0)
      {
        gstate->VarLocal("_value");
        gstate->VarSet("_value", GameValue(value), true, false, GWorld->GetMissionNamespace());

        GameValue result = gstate->Evaluate(param->valid, GameState::EvalContext::_default, GWorld->GetMissionNamespace());      
        if (result.GetNil())
        {
          message = LocalizeString("STR_EDITOR_ERROR_RTN_VAL_NIL"); 
          valid = false;
        }
        else if (result.GetType() == GameBool)
        {
          bool ok = result;
          if (!ok)
          {
            message = LocalizeString("STR_EDITOR_ERROR_RTN_VAL_FALSE");
            valid = false;
          }
        }
        else if (result.GetType() == GameString)
        {
          message = result;
          valid = false;
        }
        else
        {
          message = Format(LocalizeString("STR_EDITOR_ERROR_RTN_VAL_STR"), cc_cast(result.GetText()));
          valid = false;
        }
      }
      if (!valid)
      {
        FocusCtrl(arg.control);
        CreateMsgBox(MB_BUTTON_OK, message);
        break;
      }
    }
    gstate->EndContext(); // local space
    gstate->EndContext(); // editor space

    return valid;
  }

  return true;
}

void DisplayEditObject::Destroy()
{
  Display::Destroy();

  // update object
  if (_exit == IDC_OK)
  {
    // remove old arguments
    const EditorParams &params = _object->GetAllParams();
    for (int i=0; i<params.Size(); i++)
    {
      const EditorParam &param = params[i];
      if (param.source == EPSDialog) _object->RemoveArgument(param.name);
    }

    // add new arguments
    for (int i=0; i<_arguments.Size(); i++)
    {
      const EditorParams *params = &_object->GetType()->GetParams();
      const EditArgument &arg = _arguments[i];

      // does this control handle subparamaters?
      const SubTypeContainer *subTypeContainer = arg.subTypeSelected;
      if (subTypeContainer)
      {
        // process subparameters
        const EditorObjectType *subType = subTypeContainer->subType;
        if (subType)
        {
          // assign subtype to object
          bool assign = true;

          // does exiting subtype need to be changed?          
          if (_object->GetSubType())
            if (_object->GetSubType()->GetName() == arg.GetValue())
              assign = false;

          if (assign) _object->AssignSubType(subType,arg.GetValue());
        }
      }

      // is this a control for a subparameter?
      const EditArgument *parentArg = arg.parentArgument;
      if (parentArg)
        params = &parentArg->subTypeSelected->subType->GetParams();

      const EditorParam *param = params->Find(arg.name);
      Assert(param && param->source == EPSDialog);

      RString value = arg.GetValue();
      if (stricmp(param->subtype, "multiple") == 0)
      {
        const char *ptr = value;
        while (true)
        {
          const char *ext = strchr(ptr, ',');
          if (!ext)
          {
            _object->SetArgument(arg.name, Trim(ptr, strlen(ptr)));
            break;
          }
          _object->SetArgument(arg.name, Trim(ptr, ext - ptr));
          ptr = ext + 1;
        }
      }
      else
      {
        _object->SetArgument(arg.name, value);
      }
    } 
  }
}

void DisplayEditObject::AddArgument(RString name, Control *control, CStatic *title, RString type)
{
  AutoArray<int> tempArgs;

  for (int k=0; k<_arguments.Size(); k++)
  {
    const EditArgument *parentArgument = _arguments[k].parentArgument;
    if (parentArgument)
    {
      int i = tempArgs.Add(-1);
      for (int j=0; j<_arguments.Size(); j++)
      {
        if (_arguments[j].parentArgument == parentArgument)
        {
          tempArgs[i] = j;
          break;
        }
      }      
    }
  }

  int index = _arguments.Add();

  for (int k=0; k<tempArgs.Size(); k++)
  {
    int i = tempArgs[k];
    if (i > -1)
      _arguments[k].parentArgument = &_arguments[i];
  }

  _arguments[index].name = name;
  _arguments[index].control = control;
  _arguments[index].title = title;
  _arguments[index].type = type;
}

// TODO: reset to selected icon if mouse exits tree area
void DisplayEditObject::OnTreeMouseMove(int idc, CTreeItem *sel)
{
  if (!_previewPic) return;

  if (!sel)
  {
    _previewPic->SetTexture(NULL);
    return;
  }

  // show same picture as shown in currently selected item
  _previewPic->SetTexture(sel->texture);
}

void DisplayEditObject::OnTreeMouseHold(int idc, CTreeItem *sel)
{
  OnTreeMouseMove(idc, sel);
}

void DisplayEditObject::OnTreeMouseExit(int idc, CTreeItem *sel)
{
  OnTreeMouseMove(idc, sel);
}

// note: if "men" class is present, we want it to be first
// other classes should be sorted alphabetically
static int CmpClass(const RStringB *a, const RStringB *b)
{
  if (!strcmpi(*a,*b)) return 0;
  // categorize game vehicle classes seperately from VBS2 classes
  bool hasGame1 = strstr(*a,LocalizeString("STR_EDITOR_GAME_CLASS")) != NULL;
  bool hasGame2 = strstr(*b,LocalizeString("STR_EDITOR_GAME_CLASS")) != NULL;
  if (hasGame1 && hasGame2)
    return stricmp(*a, *b);
  if (!hasGame1 && hasGame2)
    return -1;
  if (hasGame1 && !hasGame2)
    return +1;
  return strcmpi(*a,*b);
}

void UpdateObjectList(CListBoxContainer *combo, RString &vehicleClass)
{
//  int side = 1; // set side
  bool isVehicle = false;
  RString vehicle = RString();

  combo->ClearStrings();
  ConstParamClassPtr cls = Pars.GetClass("CfgVehicles");
  RString firstVehicle;
  for (int i=0; i<cls->GetEntryCount(); i++)
  {
    ParamEntryVal vehEntry = cls->GetEntry(i);
    const ParamClass *vehCls = vehEntry.GetClassInterface();
    if (!vehCls) continue;
    int scope = vehEntry >> "scope";
    if (scope != 2) continue;
//    int vehSide = vehEntry >> "side";

    /*
    if (side == TEmpty)
    {
      if (vehSide == TLogic) continue;
      if (vehCls && vehCls->IsDerivedFrom(*cls->GetClass("Man"))) continue;
    }
    else if (side != vehSide) continue;
    */

    RString name = vehEntry >> "vehicleClass";
    if ( name.GetLength() == 0) continue;
    if (stricmp(name, vehicleClass) != 0) continue;

    RString displayName = vehEntry >> "displayName";
    int index = combo->AddString(displayName);
    combo->SetData(index, vehEntry.GetName());
    
    // preview picture
      // expected to be model path + \data\ico\preview_<model name>_ca.paa
    RString path = GetModelPreviewTexture(vehEntry);
    if (path.GetLength() > 0 && QFBankQueryFunctions::FileExists(path + 1))
      combo->SetTexture(index, GlobPreloadTexture(GetPictureName(path)));

    if (firstVehicle.GetLength() == 0)
      firstVehicle = vehEntry.GetName();
  }
  combo->SortItems();

  int sel = -1;
  if (isVehicle)
    for (int i=0; i<combo->GetSize(); i++)
    {
      if (stricmp(combo->GetData(i), vehicle) == 0)
      {
        sel = i;
        break;
      }
    }
  if (sel < 0 && firstVehicle.GetLength() > 0)
    for (int i=0; i<combo->GetSize(); i++)
    {
      if (stricmp(combo->GetData(i), firstVehicle) == 0)
      {
        sel = i;
        break;
      }
    }
  //saturateMax(sel, 0);
  //combo->SetCurSel(sel);
  combo->SetCurSel(0);
}

void UpdateCategory(CListBoxContainer *combo, const RString &selectedType, RString filter)
{
  /*
  CCombo *combo2 = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCUNIT_SIDE));
  if (!combo2) return;
  int side = combo2->GetValue(combo2->GetCurSel());
  */
//  int side = 1;

  ParamEntryVal clsVeh = Pars >> "CfgVehicles";
  int n = clsVeh.GetEntryCount();
  FindArrayRStringBCI vehClasses;
  ConstParamEntryPtr manCls = clsVeh.FindEntry("Man");
  if (!manCls) return;

  AutoArray<RString> excludeCountries;
  if (filter.GetLength() > 0)
  {
    ParamEntryVal cls = Pars >> "CfgEditorFilters"; 
    for (int i=0; i<cls.GetEntryCount(); i++)
    {
      ParamEntryVal entry = cls.GetEntry(i);
      if (!entry.IsClass() || stricmp(entry.GetName(), filter) != 0) continue;

      ConstParamEntryPtr toExclude = entry.FindEntry("excludeCountries");
      if (!toExclude) continue;

      for (int j=0; j<toExclude->GetSize(); j++)
      {
        RString exclude = (*toExclude)[j];
        excludeCountries.Add(exclude);
      }
    }
  }

  // scan all CfgVehicles for vehicleClass list
  for (int i=0; i<n; i++)
  {
    ParamEntryVal vehEntry = clsVeh.GetEntry(i);
    if (!vehEntry.IsClass()) continue;
    int scope = vehEntry >> "scope";
    if (scope != 2) continue;
    int vehSide = vehEntry >> "side";

    const ParamClass *vehCls = vehEntry.GetClassInterface();    
    if (!vehCls) continue;

    RStringB name = vehEntry >> "vehicleClass";
    if (name.GetLength() == 0) continue;
    if (stricmp(name, "sounds") == 0 || stricmp(name, "mines") == 0) continue;

    bool hasDriver = vehEntry>>"hasDriver";
    bool hasGunner = HasPrimaryGunnerTurret(vehEntry);
    bool hasCommander = HasPrimaryObserverTurret(vehEntry);

    if (!stricmp(selectedType,"object"))
    {
      //if (side != vehSide) continue;  // filter by side here
      if (vehSide == TLogic) continue;
      if (vehCls->IsDerivedFrom(*manCls)) continue;
      if (hasDriver || hasGunner || hasCommander)
        continue;
    }
    else
    {
      //if (side != vehSide) continue;  // filter by side here
      if (!stricmp(selectedType,"unit"))
      {
        if (!vehCls->IsDerivedFrom(*manCls)) continue;
      }
      else
      {
        if (vehCls->IsDerivedFrom(*manCls)) continue;
        if (!hasDriver && !hasGunner && !hasCommander)
          continue;
      }

      // filter by country
      RString country = (Pars >> "CfgVehicleClasses" >> name).ReadValue("country",RString());
      if (country.GetLength() > 0)
      {
        int j; int n = excludeCountries.Size();
        for (j=0; j<n; j++)
          if (stricmp(excludeCountries[j],country) == 0)
            break;
        if (j < n) continue;
      }
    }

    // categorize game vehicle classes seperately from VBS2 classes
    bool isGame = true;
    if (ConstParamEntryPtr isVBS = vehEntry.FindEntry("vbs_entity"))
      if (isVBS)
      {
        bool isVBS = vehEntry >> "vbs_entity";          
        isGame = !isVBS;
      }
    if (isGame) name = LocalizeString("STR_EDITOR_GAME_CLASS") + " " + name;

    int index = vehClasses.FindKey(name);
    if (index<0) 
    {
      index = vehClasses.AddUnique(name);
    }
  }
  
  RStringB vehicleClass;  // will be automatically selected
  /*
  if (vehicle && *vehicle)
  {
    vehicleClass = Pars >> "CfgVehicles" >> vehicle >> "vehicleClass";
  }
  */

  // note: if "men" class is present, we want it to be first
  // other classes should be sorted alphabetically
  QSort(vehClasses.Data(),vehClasses.Size(),CmpClass);

  combo->ClearStrings();
  int sel = 0;
  n = vehClasses.Size();
  for (int i=0; i<n; i++)
  {
    //if (classCounts[i] == 0) continue;
    RStringB name = vehClasses[i];
    // categorize game vehicle classes seperately from VBS2 classes  
    int index;
    RString gameText = LocalizeString("STR_EDITOR_GAME_CLASS");
    if (strstr(name,gameText))  // contains (Game) or similar
    {
      const char *ptr = name + gameText.GetLength() + 1;
      name = RString(ptr,name.GetLength());
      index = combo->AddString(gameText + " " + RString(Pars >> "CfgVehicleClasses" >> name >> "displayName"));
    }
    else
      index = combo->AddString(Pars >> "CfgVehicleClasses" >> name >> "displayName");
    if (vehicleClass.GetLength() == 0 && strstr(name, "Men") && sel == 0) sel = index;
    combo->SetData(index, name);
    if (stricmp(vehicleClass, name) == 0) sel = index;
  }
  combo->SetCurSel(sel);
}

void DisplayMissionEditor::OnLBSelChanged(IControl *ctrl, int curSel)
{
  CListBoxContainer *list = GetListBoxContainer(ctrl);
  if (!list) return;

  if (ctrl->IDC() == IDC_EDITOR_ADDOBJ_TYPES)
  {
    CListBoxContainer *categories = GetListBoxContainer(GetCtrl(IDC_EDITOR_ADDOBJ_CATEGORIES));
    if (!categories) return;

    int sel = list->GetCurSel();
    RString selectedType = list->GetData(sel);

    if (!stricmp(selectedType,"unit") || !stricmp(selectedType,"veh") || !stricmp(selectedType,"emptyveh") || !stricmp(selectedType,"object"))
      UpdateCategory(categories, selectedType, _filter);
    // else it'll be created by script
  }

  if (ctrl->IDC() == IDC_EDITOR_ADDOBJ_CATEGORIES)
  {
    CListBoxContainer *objList = GetListBoxContainer(GetCtrl(IDC_EDITOR_ADDOBJ_LISTING));

    int sel = list->GetCurSel();
    RString selectedCategory = list->GetData(sel);

    UpdateObjectList(objList, selectedCategory);
  }

  if (ctrl->IDC() == IDC_EDITOR_OBJECTS_FILTER && _objectBrowser)
  {
    CListBoxContainer *list = GetListBoxContainer(GetCtrl(IDC_EDITOR_OBJECTS_FILTER));
    if (!list) return;
    _treeFilter = (EditorTreeFilter)list->GetValue(list->GetCurSel());
    UpdateObjectBrowser();
  }
}

static TargetSide GetSideFromName(RString sideStr)
{
  TargetSide side = TSideUnknown;
  const EnumName *names = GetEnumNames(side);
  for (int i=0; names[i].IsValid(); i++)
    if (stricmp(names[i].name, sideStr) == 0)
    {
      side = (TargetSide)names[i].value;
      break;
    }
  return side;
}

void DisplayEditObject::OnChanged(IControl *ctrl)
{
  // search for the argument matching the control
  const EditArgument *arg = NULL;
  for (int i=0; i<_arguments.Size(); i++)
    if (_arguments[i].control == ctrl)
      arg = &_arguments[i];
  if (!arg) return;

  // change subparameter controls
  Control *control = static_cast<Control *>(ctrl);
  IControl *grpCtrl = GetCtrl(IDC_EDIT_OBJECT_CONTROLS);
  if (control && grpCtrl && grpCtrl->GetType() == CT_CONTROLS_GROUP)
  {      
    CControlsGroup *grp = static_cast<CControlsGroup *>(grpCtrl);
    CCombo *combo = dynamic_cast<CCombo *>(control);

    const SubTypeContainer *subParamsContainer = arg->subTypeSelected;

    if (!subParamsContainer && combo)
    {
      // attempt to find subType that matches the new selection
      for (int i=0; i<_subTypes.Size(); i++)
        if (_subTypes[i].paramSubType == combo->GetData(combo->GetCurSel()))
        {
          subParamsContainer = &_subTypes[i];
          break;
        }
    }

    if (subParamsContainer && combo)
    {
      const EditorParams &subParams = subParamsContainer->subType->GetParams();

      // for changing position of other controls
      float heightChange = 0;

      // loop through subparams removing existing controls
      for (int i=0; i<subParams.Size(); i++)
      {
        RString subParamName = subParams[i].name;

        // remove old subparameter arguments and controls
        AutoArray<EditArgument> arguments = _arguments;
        for (int j=0; j<_arguments.Size(); j++)
        {
          if (subParamName == _arguments[j].name)
          {
            Control *control = _arguments[j].control;
            
            // remove control + argument
            if (control)
            {
              for (int k=0; k<arguments.Size(); k++)
                if (arguments[k].control == control)
                {
                  arguments.Delete(k);
                  break;
                }

              if (subParams[i].y < 0 && !subParams[i].hidden) // don't take into account controls that set their own position
                heightChange -= control->H() + 0.005;

              grp->RemoveControl(control);      
              grp->RemoveControl(_arguments[j].title);      
            }               
            break;
          }     
        }
        _arguments = arguments;
        //-!
      }
      int argSize = _arguments.Size();
      //-!

      // height of first subparameter control...
      float top = control->Y() + control->H() + 0.005;

      // process init EH
      GameState *gstate = GWorld->GetGameState();
      gstate->BeginContext(_map->GetVariables()); // editor space
      GameVarSpace local(_map->GetVariables(), false);
      gstate->BeginContext(&local); // local space

      // initialization of variables
      gstate->VarLocal("_map"); // access to scripting functions on the workspace
      gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());
      for (int i=0; i<_arguments.Size(); i++)
      {
        RString name = RString("_control_") + _arguments[i].name;
        gstate->VarLocal(name);
        gstate->VarSet(name, CreateGameControl(_arguments[i].control), true, false, GWorld->GetMissionNamespace());
        name = RString("_control_title_") + _arguments[i].name;
        gstate->VarLocal(name);
        gstate->VarSet(name, CreateGameControl(_arguments[i].title), true, false, GWorld->GetMissionNamespace());
      }
      //-!

      // add new controls based on combo box selection
      int nSubTypes = _subTypes.Size();
      for (int i=0; i<nSubTypes; i++)
      {
        if (
          arg->name == _subTypes[i].paramName &&
          combo->GetData(combo->GetCurSel()) == _subTypes[i].paramSubType
        )
        {
          const EditorParams &subParams = _subTypes[i].subType->GetParams();   
          unconst_cast(arg)->subTypeSelected = &_subTypes[i];
          for (int j=0; j<subParams.Size(); j++)
          {
            const EditorParam *subParam = &subParams[j];

            // add control for subparameter
            float topBefore = top;
            int n = _arguments.Size();
            AddControl(grp,*subParam,top);                 
            if (_arguments.Size() > n)  // has control been created?
            {
              _arguments[n].parentArgument = arg;

              // process init EH
              RString name = RString("_control_") + _arguments[n].name;
              gstate->VarLocal(name);
              gstate->VarSet(name, CreateGameControl(_arguments[n].control), true, false, GWorld->GetMissionNamespace());
              name = RString("_control_title_") + _arguments[i].name;
              gstate->VarLocal(name);
              gstate->VarSet(name, CreateGameControl(_arguments[i].title), true, false, GWorld->GetMissionNamespace());
            }
            heightChange += top - topBefore;
          }
          // process init EH
          for (int j=0; j<subParams.Size(); j++)
          {
            const EditorParam &param = subParams[j];
            gstate->VarLocal("_this");
            if (param.source == EPSDialog && param.onInit.GetLength() > 0)
            {
              for (int i=0; i<_arguments.Size(); i++)
                if (_arguments[i].name == param.name)
                {                    
                    gstate->VarSet("_this", CreateGameControl(_arguments[i].control), true, false, GWorld->GetMissionNamespace());   
                    break;
                }
              gstate->Execute(param.onInit, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
            }
            gstate->VarSet("_this", NOTHING, true, false, GWorld->GetMissionNamespace());
          }
          break;
        }
      }
          
      gstate->EndContext(); // local space
      gstate->EndContext(); // editor space

      // reposition all combos below control      
      for (int i=0; i<argSize; i++)
      {
        Control *paramControl = _arguments[i].control;
        if (paramControl->Y() > control->Y())
        {
          paramControl->Move(0, heightChange);
          _arguments[i].title->Move(0, heightChange);
        }
      }
    }
    //-!
  }
  //-!

  RString handler;  
  const EditorParam *param = _object->GetType()->GetParams().Find(arg->name);

  // change type filter
  if (param && stricmp(param->type,"filter") == 0)
  {
    CCombo *filterCombo = dynamic_cast<CCombo *>(ctrl);
    _filter = filterCombo->GetData(filterCombo->GetCurSel());

    // update trees
    for (int i=0; i<_arguments.Size(); i++)
    {
      const EditArgument *typeArg = &_arguments[i];
      const EditorParam *eval = _object->GetType()->GetParams().Find(typeArg->name);
      if (eval && stricmp(eval->type,"special") == 0 &&
         (stricmp(eval->subtype,"agent") == 0 || stricmp(eval->subtype,"vehicle") == 0 || stricmp(eval->subtype,"agent_vehicle") == 0))
      {
        Control *control = static_cast<Control *>(typeArg->control);
        CTree *tree = dynamic_cast<CTree *>(control);
        if (tree)
        {
          TargetSide side = TSideUnknown;

          // figure out correct side
          for (int j=0; j<_arguments.Size(); j++)
          {
            const EditArgument *arg = &_arguments[j];
            const EditorParam *argParam = _object->GetType()->GetParams().Find(typeArg->name);

            if (argParam && stricmp(argParam->type,"side") == 0 && stricmp(typeArg->name,argParam->subtype) == 0)
            {              
              Control *control = static_cast<Control *>(arg->control);
              CCombo *sideCombo = dynamic_cast<CCombo *>(control);
              if (sideCombo)
              {
                side = GetSideFromName(sideCombo->GetData(sideCombo->GetCurSel()));
                break;
              }
            }
          }

          RString value;
          if (tree->GetSelected())
            value = tree->GetSelected()->data;
          if (stricmp(eval->subtype,"agent") == 0)
          {            
            tree->RemoveAll();              
            FillVehicleTree(tree, side, value, _filter, true);
          }
          else if (stricmp(eval->subtype,"vehicle") == 0)
          {
            tree->RemoveAll();              
            FillVehicleTree(tree, side, value, _filter);
          }
          else if (stricmp(eval->subtype,"agent_vehicle") == 0)
          {
            tree->RemoveAll();              
            FillVehicleTree(tree, side, value, _filter, false, false, true);
          }
        }
      }
    }

    GameDataNamespace globals(NULL, RString(), false); // save - no global namespace
    ParamFile cfg;    
    if (ParseUserParams(cfg, &globals))
    {
      cfg.Add("editorFilter", _filter);
      SaveUserParams(cfg);
    }
  }

  // SIDE control automatically re-populates TYPE controls
    // TODO: move to script
  if (param && stricmp(param->type,"side") == 0)
  {
    // find subtype - this is the control that the side combo will modify
    RString subType = param->subtype;
    const EditorParam *typeParam = NULL;
    const EditArgument *typeArg = NULL;
    for (int i=0; i<_arguments.Size(); i++)
    {
      typeArg = &_arguments[i];
      const EditorParam *eval = _object->GetType()->GetParams().Find(typeArg->name);
      if (eval && stricmp(eval->type,"special") == 0 && stricmp(typeArg->name,subType) == 0 &&
         (stricmp(eval->subtype,"agent") == 0 || stricmp(eval->subtype,"vehicle") == 0 || stricmp(eval->subtype,"agent_vehicle") == 0))
      {
        typeParam = eval;
        break;
      }
    }

    // control for type exists?
    if (typeParam && typeArg)
    {      
      CCombo *sideCombo = dynamic_cast<CCombo *>(ctrl);
      if (sideCombo)
      {
        TargetSide side = GetSideFromName(sideCombo->GetData(sideCombo->GetCurSel()));
        Control *control = static_cast<Control *>(typeArg->control);
        CTree *tree = dynamic_cast<CTree *>(control);
        if (tree)
        {
          RString value;
          if (tree->GetSelected())
            value = tree->GetSelected()->data;
          if (stricmp(typeParam->subtype,"agent") == 0)
          {            
            tree->RemoveAll();              
            FillVehicleTree(tree, side, value, _filter, true);
          }
          else if (stricmp(typeParam->subtype,"vehicle") == 0)
          {
            tree->RemoveAll();              
            FillVehicleTree(tree, side, value, _filter);
          }
          else if (stricmp(typeParam->subtype,"agent_vehicle") == 0)
          {
            tree->RemoveAll();              
            FillVehicleTree(tree, side, value, _filter, false, false, true);
          }
        }
      } // sideCombo
    } // typeParam && typeArg
  }
  //-!
      
  // is this a control for a subparameter?
  const EditArgument *parentArg = arg->parentArgument;
  if (parentArg)
    param = parentArg->subTypeSelected->subType->GetParams().Find(arg->name);

  if (param) 
    handler = param->onChanged;
  if (handler.GetLength() == 0) return;

  // handling of onChanged event
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(_map->GetVariables()); // editor space
  GameVarSpace local(_map->GetVariables(), false);
  gstate->BeginContext(&local); // local space

  // initialization of variables
  gstate->VarLocal("_map"); // access to scripting functions on the workspace
  gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());
  gstate->VarLocal("_this");
  gstate->VarSet("_this", CreateGameControl(arg->control), true, false, GWorld->GetMissionNamespace());
  for (int i=0; i<_arguments.Size(); i++)
  {
    RString name = RString("_control_") + _arguments[i].name;
    gstate->VarLocal(name);
    gstate->VarSet(name, CreateGameControl(_arguments[i].control), true, false, GWorld->GetMissionNamespace());
    name = RString("_control_title_") + _arguments[i].name;
    gstate->VarLocal(name);
    gstate->VarSet(name, CreateGameControl(_arguments[i].title), true, false, GWorld->GetMissionNamespace());
  }

  gstate->VarLocal("_linkArgument");
  gstate->VarSet("_linkArgument", _linkArgument, true, false, GWorld->GetMissionNamespace());
  if (_linkObject)
    gstate->VarSet("_linkObject", _linkObject->GetArgument("VARIABLE_NAME"), true, false, GWorld->GetMissionNamespace());
  else
    gstate->VarSet("_linkObject", RString(), true, false, GWorld->GetMissionNamespace());
  gstate->VarLocal("_edObj");
  if (_update && _object) // _this == proxy
  {
    gstate->VarSet("_edObj", _object->GetArgument("VARIABLE_NAME"), true, false, GWorld->GetMissionNamespace());
  }
  else
  {
    gstate->VarSet("_edObj", RString(), true, false, GWorld->GetMissionNamespace());
  }

  // executing of handler
  gstate->Execute(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace());

  gstate->EndContext(); // local space
  gstate->EndContext(); // editor space
}

class DisplayMissionLoad : public Display
{
protected:
  InitPtr<CListBoxContainer> _island;
  InitPtr<CListBoxContainer> _mission;
  bool _multiplayer;

public:
  DisplayMissionLoad(ControlsContainer *parent, bool multiplayer);

  RString GetIsland() const;
  RString GetMission() const;

  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnLBSelChanged(IControl *ctrl, int curSel);

protected:
  void OnIslandChanged(RString name);
};

DisplayMissionLoad::DisplayMissionLoad(ControlsContainer *parent, bool multiplayer)
: Display(parent)
{
  _enableSimulation = false;
  _enableDisplay = false;
  _multiplayer = multiplayer;

  SetCursor("Wait");
  DrawCursor();

  Load("RscDisplayMissionLoad");

  if (_island)
  {
    int sel = 0;
    int n = (Pars >> "CfgWorldList").GetEntryCount();
    for (int i=0; i<n; i++)
    {
      ParamEntryVal entry = (Pars >> "CfgWorldList").GetEntry(i);
      if (!entry.IsClass()) continue;
      RString name = entry.GetName();

#if _FORCE_DEMO_ISLAND
      RString demo = Pars >> "CfgWorlds" >> "demoWorld";
      if (stricmp(name, demo) != 0) continue;
#endif

      // Check if wrp file exists
      RString fullname = GetWorldName(name);
      if (!QFBankQueryFunctions::FileExists(fullname)) continue;

      int index = _island->AddString
      (
        Pars >> "CfgWorlds" >> name >> "description"
      );
      _island->SetData(index, name);

      if (stricmp(name, Glob.header.worldname) == 0) sel = index;
    }
    _island->SetCurSel(sel, false);
    OnIslandChanged(Glob.header.worldname);
  }

  if (_mission)
  {
    int sel = 0;
    for (int i=0; i<_mission->GetSize(); i++)
    {
      RString name = _mission->GetData(i);
      if (stricmp(name, Glob.header.filename) == 0) sel = i;
    }
    _mission->SetCurSel(sel);
  }

  SetCursor("Arrow");
}

RString DisplayMissionLoad::GetIsland() const
{
  if (!_island) return RString();
  int sel = _island->GetCurSel();
  if (sel < 0) return RString();
  return _island->GetData(sel);
}

RString DisplayMissionLoad::GetMission() const
{
  if (!_mission) return RString();
  int sel = _mission->GetCurSel();
  if (sel < 0) return RString();
  return _mission->GetData(sel);
}

Control *DisplayMissionLoad::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_MISSION_LOAD_ISLAND:
    _island = GetListBoxContainer(ctrl);
    break;
  case IDC_MISSION_LOAD_MISSION:
    _mission = GetListBoxContainer(ctrl);
    break;
  }
  return ctrl;
}

void DisplayMissionLoad::OnLBSelChanged(IControl *ctrl, int curSel)
{
  if (ctrl->IDC() == IDC_MISSION_LOAD_ISLAND && curSel >= 0)
    OnIslandChanged(_island->GetData(curSel));
}

struct AddMissionContext
{
  CListBoxContainer *list;
  RString island;
};

static bool AddMission(const FileItem &file, AddMissionContext &ctx)
{
  // remove trailing '\'
  int n = file.path.GetLength();
  DoAssert(n > 0 && file.path[n - 1] == '\\');
  RString path(file.path, n - 1);

  // world and mission name
  const char *ptr = strrchr(path, '\\');
  if (!ptr) return false;  // avoid missions on root
  RString name(ptr + 1);
  // RString dir(path, ptr + 1 - path);
  ptr = strrchr(name, '.');
  if (!ptr) return false; // bad directory name
  RString world(ptr + 1);
  RString mission(name, ptr - name);  

  if (stricmp(world, ctx.island) == 0 || !ctx.island.GetLength())
  {
    if (!ctx.island.GetLength()) 
      mission = mission + "." + world; // preserve island extension if no island is specified
    
    int index = ctx.list->AddString(DecodeFileName(mission));
    ctx.list->SetData(index, mission);
  }

  return false; // continue with enumeration
}

void DisplayMissionLoad::OnIslandChanged(RString name)
{
  if (!_mission) return;
  _mission->ClearStrings();

  AddMissionContext ctx;
  ctx.list = _mission;
  ctx.island = name;
  // all missions to be saved in MPMissions
  RString missionsDir = "MPMissions\\";
  ForMaskedFileR(GetUserDirectory() + missionsDir, "mission.biedi", AddMission, ctx);
  _mission->SortItems();
  _mission->SetCurSel(0);
}

enum MissionEdPlacement
{
  MPNone,
  MPSingleplayer,
  MPMultiplayer
};

class DisplayMissionSave : public Display
{
protected:
  InitPtr<CEditContainer> _mission;
  InitPtr<CEditContainer> _title;
  InitPtr<CEditContainer> _description;
  InitPtr<CListBoxContainer> _placement;
  bool _realTime;

public:
  DisplayMissionSave(ControlsContainer *parent, RString title, RString description, bool realTime);
  RString GetMission() const;
  RString GetTitle() const;
  RString GetDescription() const;
  MissionEdPlacement GetPlacement() const;
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  virtual bool CanDestroy();
};

DisplayMissionSave::DisplayMissionSave(ControlsContainer *parent, RString title, RString description, bool realTime)
: Display(parent)
{
  _enableSimulation = false;
  _enableDisplay = false;

  Load("RscDisplayMissionSave");

  if (_mission)
  {
    _mission->SetText(DecodeFileName(Glob.header.filename));
  }
  if (_title)
  {
    _title->SetText(title);
  }
  if (_description)
  {
    _description->SetText(description);
  }
  if (_placement)
  {
    int index = _placement->AddString(LocalizeString(IDS_EXPORT_NONE));
    _placement->SetValue(index, MPNone);
    index = _placement->AddString(LocalizeString(IDS_EXPORT_SINGLE));
    _placement->SetValue(index, MPSingleplayer);
    index = _placement->AddString(LocalizeString(IDS_EXPORT_MULTI));
    _placement->SetValue(index, MPMultiplayer);
    _placement->SetCurSel(0);
  }
  _realTime = realTime;
}

bool DisplayMissionSave::CanDestroy()
{
  if (!Display::CanDestroy()) return false;
  if (_exit == IDC_OK)
  {
    if (_mission->GetText().GetLength() == 0)
    {
      GWorld->CreateWarningMessage(LocalizeString(IDS_MSG_WRONG_MISSION_NAME));
      return false;
    }
    if (!stricmp(DecodeFileName(Glob.header.filename),_mission->GetText()) && _realTime)
    {
      GWorld->CreateWarningMessage(LocalizeString("STR_VBS2_EDITOR_WARNING_NO_MISSION_OVERWRITE"));
      return false;
    }
  }
  return true;
}

RString DisplayMissionSave::GetMission() const
{
  if (!_mission) return RString();
  return _mission->GetText();
}

RString DisplayMissionSave::GetTitle() const
{
  if (!_title) return RString();
  return _title->GetText();
}

RString DisplayMissionSave::GetDescription() const
{
  if (!_description) return RString();
  return _description->GetText();
}

MissionEdPlacement DisplayMissionSave::GetPlacement() const
{
  if (!_placement) return MPNone;
  int sel = _placement->GetCurSel();
  if (sel < 0) return MPNone;
  return (MissionEdPlacement)_placement->GetValue(sel);
}

Control *DisplayMissionSave::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_MISSION_SAVE_MISSION:
    _mission = GetEditContainer(ctrl);
    break;
  case IDC_MISSION_SAVE_TITLE:
    _title = GetEditContainer(ctrl);
    break;
  case IDC_MISSION_SAVE_DESCRIPTION:
    _description = GetEditContainer(ctrl);
    break;
  case IDC_MISSION_SAVE_PLACEMENT:
    _placement = GetListBoxContainer(ctrl);
    break;
  }
  return ctrl;
}

class DisplayEditBriefing : public Display
{
protected:
  InitPtr<CEditContainer> _plan;
  InitPtr<CEditContainer> _notes;
  InitPtr<CListBoxContainer> _side;

  InitPtr<CEditContainer> _newObj;
  InitPtr<CListBoxContainer> _objs;

  AutoArray<RString> _planArray;
  AutoArray<RString> _notesArray;
  AutoArray<EditorObjectives> _objectives;

  int _curSide;

public:
  DisplayEditBriefing(ControlsContainer *parent, EditorBriefing *briefing);
  AutoArray<RString> GetPlan() const {return _planArray;} // copy
  AutoArray<RString> GetNotes() const {return _notesArray;} // copy
  AutoArray<EditorObjectives> GetObjectives() const {return _objectives;} // copy
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnLBSelChanged(IControl *ctrl, int curSel);
  bool CanDestroy();
  void OnButtonClicked(int idc);
};

DisplayEditBriefing::DisplayEditBriefing(ControlsContainer *parent, EditorBriefing *briefing)
: Display(parent)
{
  _enableSimulation = false;
  _enableDisplay = false;
  _curSide = -1;

  if (briefing)
  {
    _planArray = briefing->GetPlan();
    _notesArray = briefing->GetNotes();
    _objectives = briefing->GetObjectives(); // copy
  }

  Load("RscDisplayEditBriefing");

  if (_side)
  {
    _side->AddString(LocalizeString(IDS_GAME_TYPE_ANY));
    _side->AddString(LocalizeString(IDS_WEST));
    _side->AddString(LocalizeString(IDS_EAST));    
    _side->AddString(LocalizeString(IDS_GUERRILA));
    _side->AddString(LocalizeString(IDS_CIVILIAN));
    _side->SetCurSel(0, true); // will trigger OnLBSelChanged()
  }  
}

void DisplayEditBriefing::OnLBSelChanged(IControl *ctrl, int curSel)
{
  if (ctrl->IDC() == IDC_EDITOR_EB_SIDE && _curSide != curSel)
  {
    if (_plan) 
    {
      if (_curSide > -1) _planArray[_curSide] = _plan->GetText();
      _plan->SetText(_planArray[curSel]);
    }
    if (_notes) 
    {
      if (_curSide > -1) _notesArray[_curSide] = _notes->GetText();
      _notes->SetText(_notesArray[curSel]);    
    }
    if (_objs)
    {
      // save objectives for old side
      if (_curSide > -1) 
      {
        _objectives[_curSide].Clear();
        for (int i=0; i<_objs->GetSize(); i++)
          _objectives[_curSide].AddObjective(_objs->GetText(i),false);
      }

      // fill with objectives for new side
      _objs->RemoveAll();
      for (int i=0; i<_objectives[curSel].NObjectives(); i++)
        _objs->AddString(_objectives[curSel].GetObjective(i));
      _objs->SetCurSel(0);
    }
    _curSide = curSel;
  }
}

void DisplayEditBriefing::OnButtonClicked(int idc)
{
  switch (idc)
  {
  case IDC_OK:
    {
      if (_side) 
      { 
        int i = _side->GetCurSel();
        if (_notes) _notesArray[i] = _notes->GetText();
        if (_plan) _planArray[i] = _plan->GetText();
      }
    }
    break;
  case IDC_EDITOR_EB_ADD_OBJECTIVE:
    {
      if (!_newObj || !_side || !_objs) return;
      RString newObj = _newObj->GetText();

      if (newObj.GetLength() > 0) 
      { 
        int i = _objectives[_side->GetCurSel()].AddObjective(newObj,false);
        _objs->AddString(newObj);
        _objs->SetCurSel(i);
        _newObj->SetText("");
      }
    }
    break;
  case IDC_EDITOR_EB_DEL_OBJECTIVE:
    {
      if (!_side || !_objs) return;
      
      int toDelete = _side->GetCurSel();
      if (toDelete == -1) return;

      _objectives[_side->GetCurSel()].DeleteObjective(toDelete);
      _objs->SetCurSel(toDelete-1);
      _objs->DeleteString(toDelete);      
    }
    break;
  }
  Display::OnButtonClicked(idc);
}

bool DisplayEditBriefing::CanDestroy()
{
  return Display::CanDestroy();
}

Control *DisplayEditBriefing::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_EDITOR_EB_PLAN:
    _plan = GetEditContainer(ctrl);
    break;
  case IDC_EDITOR_EB_NOTES:
    _notes = GetEditContainer(ctrl);
    break;
  case IDC_EDITOR_EB_SIDE:
    _side = GetListBoxContainer(ctrl);
    break;
  case IDC_EDITOR_EB_NEW_OBJECTIVE:
    _newObj = GetEditContainer(ctrl);
    break;
  case IDC_EDITOR_EB_OBJECTIVES:
    _objs = GetListBoxContainer(ctrl);
    break;
  }
  return ctrl;
}

class DisplayOverlayCreate : public Display
{
protected:
  InitPtr<CEditContainer> _name;
  ConstParamEntryPtr _overlayType;
  RString _overlayTypeName;

public:
  DisplayOverlayCreate(ControlsContainer *parent, ConstParamEntryPtr overlayType = NULL);
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnButtonClicked(int idc);
  RString GetName() const;
};

DisplayOverlayCreate::DisplayOverlayCreate(ControlsContainer *parent, ConstParamEntryPtr overlayType)
:Display(parent)
{
  _enableSimulation = false;
  _enableDisplay = false;

  _overlayType = overlayType;
  if (_overlayType)
    _overlayTypeName = overlayType->GetName();

  _overlayTypeName.Lower();

  Load("RscDisplayOverlayCreate");
}

static bool OverlayExists(const FileItem &file, RString &ctx)
{
  RString name = file.filename;
  const char *ptr = strrchr(name, '.');
  if (!ptr) return false;
  RString overlay(name, ptr - name);
  return stricmp(overlay,ctx) == 0;
}

void DisplayOverlayCreate::OnButtonClicked(int idc)
{
  switch (idc)
  {
  case IDC_OK:
    {
      // check if an overlay of the same name already exists
      if (ForMaskedFile(GetUserDirectory() + RString("Overlays\\"), "*." + _overlayTypeName + ".biedi", OverlayExists, GetName()))
      {
        GWorld->CreateWarningMessage(LocalizeString("STR_EDITOR_ERROR_OVERLAY_EXISTS"));
        return;
      }
      else if (_name->GetText().GetLength() == 0)
      {
        GWorld->CreateWarningMessage(LocalizeString("STR_EDITOR_ERROR_INVALID_OVERLAY_NAME"));
        return;
      }
    }
    break;
  }
  Display::OnButtonClicked(idc);
}

Control *DisplayOverlayCreate::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_OVERLAY_CREATE_NAME:
    _name = GetEditContainer(ctrl);
    if (_name)
    {
      RString filename;
      RString initialName("Overlay_"); 
      int i = 0;
      do
      {
        filename = GetUserDirectory() + RString("Overlays\\") + initialName + Format("%d",++i) + "." + _overlayTypeName + ".biedi";
      } while (QIFileFunctions::FileExists(filename));
      filename = initialName + Format("%d",i);
      _name->SetText(filename);
      _name->SetBlock(0,filename.GetLength());
    }
    break;
  }
  return ctrl;
}

RString DisplayOverlayCreate::GetName() const
{
  if (!_name) return RString();
  return EncodeFileName(_name->GetText());
}

class DisplayOverlayLoad : public Display
{
protected:
  InitPtr<CListBoxContainer> _overlay;
  InitPtr<CListBoxContainer> _overlayMission;
  ConstParamEntryPtr _overlayType;
  RString _overlayTypeName;

public:
  DisplayOverlayLoad(ControlsContainer *parent, ConstParamEntryPtr overlayType = NULL);
  RString GetOverlay() const;
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnLBSelChanged(IControl *ctrl, int curSel);
};

struct AddOverlayContext
{
  CListBoxContainer *list;
};

static bool AddOverlay(const FileItem &file, AddOverlayContext &ctx)
{
  RString name = file.filename;
  const char *ptr = strrchr(name, '.');
  if (!ptr) return false;
  RString overlay(name, ptr - name);

  ptr = strrchr(overlay, '.');
  RString overlayName = RString(overlay, ptr - overlay);

  int index = ctx.list->AddString(overlayName);
  ctx.list->SetData(index, overlayName);

  return false; // continue with enumeration
}

DisplayOverlayLoad::DisplayOverlayLoad(ControlsContainer *parent, ConstParamEntryPtr overlayType)
: Display(parent)
{
  _enableSimulation = false;
  _enableDisplay = false;

  _overlayType = overlayType;
  if (_overlayType)
    _overlayTypeName = overlayType->GetName();

  _overlayTypeName.Lower();

  if (!stricmp(_overlayTypeName,"layer"))
    Load("RscDisplayLayerLoad");  
  else
    Load("RscDisplayOverlayLoad");  

  if (_overlay)
  {
    if (_overlayMission) _overlay->AddString(LocalizeString("STR_EDITOR_LAYER_NONE"));
    AddOverlayContext ctx;
    ctx.list = _overlay;
    ForMaskedFile(GetUserDirectory() + RString("Overlays\\"), "*." + _overlayTypeName + ".biedi", AddOverlay, ctx);
    _overlay->SetCurSel(0,false);
  }
  if (_overlayMission)
  {
    _overlayMission->AddString(LocalizeString("STR_EDITOR_LAYER_NONE"));
    AddMissionContext ctx;
    ctx.list = _overlayMission;
    // all missions to be saved in MPMissions
    RString missionsDir = "MPMissions\\";
    ForMaskedFileR(GetUserDirectory() + missionsDir, "mission.biedi", AddMission, ctx);
    _overlayMission->SortItems();
    _overlayMission->SetCurSel(0,false);
  }
}

void DisplayOverlayLoad::OnLBSelChanged(IControl *ctrl, int curSel)
{
  if (!_overlayMission) return;
  
  if (ctrl->IDC() == IDC_OVERLAY_LOAD_NAME)
    _overlayMission->SetCurSel(0,false);
  
  if (ctrl->IDC() == IDC_OVERLAY_LOAD_MISSION)
    _overlay->SetCurSel(0,false);
}

RString DisplayOverlayLoad::GetOverlay() const
{
  RString rtn;
  if (_overlay)
  {
    int sel = _overlay->GetCurSel();
    if (sel > -1)
      rtn = _overlay->GetData(sel);
  }
  if (_overlayMission)
  {
    int sel = _overlayMission->GetCurSel();
    if (sel > 0)
      rtn = _overlayMission->GetData(sel);
  }
  return rtn;
}

Control *DisplayOverlayLoad::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_OVERLAY_LOAD_NAME:
    _overlay = GetListBoxContainer(ctrl);
    break;
  case IDC_OVERLAY_LOAD_MISSION:
    _overlayMission = GetListBoxContainer(ctrl);
    break;
  }
  return ctrl;
}

// for processing click eventhandlers
static void ProcessOnClick(RString command, Vector3 pos, InitPtr<CStaticMapMain> map, bool asl = false)
{
  bool alt = GInput.keys[DIK_LALT]>0;
  bool shift = GInput.keys[DIK_LSHIFT]>0;
 
  GameState *state = GWorld->GetGameState();

  GameValue posValue = state->CreateGameValue(GameArray);
  GameArrayType &posArray = posValue;
  posArray.Realloc(3);
  posArray.Add(pos[0]);
  posArray.Add(pos[2]);
  if (asl)
    posArray.Add(pos[1]);
  else
    posArray.Add(pos[1]-GLandscape->SurfaceYAboveWater(pos[0],pos[2]));

  GameVarSpace vars(false);
  state->BeginContext(&vars);
  state->VarSetLocal("_map", CreateGameControl(map), true);
  state->VarSetLocal("_pos",posArray,true);
  state->VarSetLocal("_alt",alt,true);
  state->VarSetLocal("_shift",shift,true);
  state->EvaluateMultiple(command, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  state->EndContext();
}

CStaticMapEditor::CStaticMapEditor(
  ControlsContainer *parent, int idc, ParamEntryPar cls,
  float scaleMin, float scaleMax, float scaleDefault,
  EditorWorkspace *ws, GameVarSpace *vars)
  : CStaticMapMain(parent, idc, cls, scaleMin, scaleMax, scaleDefault), _ws(ws), _vars(vars)
{
  _dragging = false;
  _draggingRM = false;
  _selecting = false;  
  _selectOne = false;
  _dragPos = VZero;
  _linkShortcutKey = -1;
  _drawCursorLines = true;
  _camera = NULL;
  _lockToCamera = false;
  _mainMap = NULL;
  _drawMainMapBox = false;
  _drawEditorObjects = true;
}

bool CStaticMapEditor::HasParent()
{
  return GetParent() ? true : false;
}

void CStaticMapEditor::Center(bool soft)
{
  Object *obj = GWorld->CameraOn();
  if (obj)
    CStaticMap::Center(obj->Position(), soft);
  else
    CStaticMap::Center(_defaultCenter, soft);
}

Vector3 CStaticMapEditor::GetCenter()
{
  return _defaultCenter;
}

void CStaticMapEditor::DrawLabel(struct SignInfo &info, PackedColor color)
{
  if (info._type == signEditorObject)
  {
    const EditorObject *obj = _ws->FindObject(info._name);
    if (obj)
    {      
      RString name = obj->GetDisplayName();
      if (name.GetLength() == 0)
      {
        RString varName = obj->GetArgument("VARIABLE_NAME");
        RString typeName = obj->GetType()->GetName();
        name = Format("%s \"%s\"", (const char *)typeName, (const char *)varName);
      }
      CStaticMap::DrawLabel(info._pos, name, color);
    }
  }
}

SignInfo CStaticMapEditor::FindSign(float x, float y)
{
  struct SignInfo info;
  info._type = signNone;
  if (!_ws) return info;

  Vector3 pos = ScreenToWorld(DrawCoord(x, y));
  const float sizeLand = LandGrid * LandRange;
  float minDist2 = Square(0.01 * sizeLand * _scaleX);

  RefArray<EditorObject> closestObjs; Vector3 closestPos;

  for (int i=0; i<_ws->NObjects(); i++)
  {
    EditorObject *obj = _ws->GetObject(i);
    if (
      obj->IsVisible() && 
      obj->GetScope() != EOSAllNoSelect && 
      obj->IsObjInCurrentOverlay()
    )
    {
      Vector3 objPos;

      Object *proxy = obj->GetProxyObject();
      if (proxy)
        objPos = proxy->WorldPosition(proxy->FutureVisualState());
      else
        obj->GetEvaluatedPosition(objPos);
     
      float x = objPos[0], y = objPos[2];
      float dist2 = Square(pos.X() - x) + Square(pos.Z() - y);
      if (dist2 <= minDist2)
      {
        minDist2 = dist2;
        closestObjs.Add(obj);
        closestPos[0] = x; closestPos[2] = y;
      }
    }
  }

  if (closestObjs.Size() > 0)
  {
/*
    LogF("[p] size: %d",closestObjs.Size());
    for (int i=closestObjs.Size()-1; i>=0; i--)
      LogF("[p] %s",closestObjs[i]->GetArgument("VARIABLE_NAME").Data());
*/
    
    info._type = signEditorObject;
    info._pos = closestPos;

    int n = closestObjs.Size(); 
    int firstVeh = -1; 
    bool foundVehicle = false, foundGroup = false;
    Object *closestProxy = closestObjs[n-1]->GetProxyObject();

    for (int i=n-1; i>=0; i--)
    {      
      EditorObject *obj = closestObjs[i];
      if (!obj) continue;
      Object *proxy = obj->GetProxyObject();
      
      if (!foundGroup && proxy == closestProxy)
      {
        RString name = obj->GetArgument("VARIABLE_NAME");
        if (name.Find("group") > -1)
        {
          firstVeh = i;
          foundGroup = true;
        }
        else if (name.Find("vehicle") > -1)
        {
          firstVeh = i;
          foundVehicle = true;
        }
        else if (!foundVehicle && firstVeh == -1)
        {
          if (name.Find("unit") > -1)
          {
            firstVeh = i;
          }
        }
        closestProxy = proxy;
      }      
    }
/*
    LogF("[p] %d | %d",firstVeh,n-1);
    LogF("[p]-------------------------------------------");
*/
    // no vehicles?
    if (firstVeh == -1) firstVeh = n-1;   
    info._name = closestObjs[firstVeh]->GetArgument("VARIABLE_NAME");
  }

  return info;
}

void CStaticMapEditor::OnDraw(UIViewport *vp, float alpha)
{
  // lock map to camera?
  if (_camera && _lockToCamera && !_drawMainMapBox)
    CStaticMap::Center(_camera->WorldPosition(_camera->FutureVisualState()), false);

  CStaticMap::OnDraw(vp, alpha);

  if (_drawMainMapBox && _mainMap)
  {
    // get coords of _mainMap
    Rect2DAbs mapArea = _mainMap->MapArea();

    PackedColor color = PackedColor(Color(0,0,0,1));
    DrawCoord pt1 = WorldToScreen(Vector3(mapArea.x, 0, mapArea.y));
    DrawCoord pt2 = WorldToScreen(Vector3(mapArea.x + mapArea.w, 0, mapArea.y + mapArea.h));
    GEngine->DrawLine
    (
      Line2DPixel(pt1.x * _wScreen, pt1.y * _hScreen,
      pt2.x * _wScreen, pt1.y * _hScreen),
      color, color, _clipRect
    );
    GEngine->DrawLine
    (
      Line2DPixel(pt2.x * _wScreen, pt1.y * _hScreen,
      pt2.x * _wScreen, pt2.y * _hScreen),
      color, color, _clipRect
    );
    GEngine->DrawLine
    (
      Line2DPixel(pt2.x * _wScreen, pt2.y * _hScreen,
      pt1.x * _wScreen, pt2.y * _hScreen),
      color, color, _clipRect
    );
    GEngine->DrawLine
    (
      Line2DPixel(pt1.x * _wScreen, pt2.y * _hScreen,
      pt1.x * _wScreen, pt1.y * _hScreen),
      color, color, _clipRect
    );
  }

  // CStaticMap::OnDraw prevents keyboard for working outside of the main map
    // this enables zoom by keyboard in realtime
  if (GetParent()->IsRealTime())
  {
    float dt = Glob.uiTime - _moveLast;
    saturateMin(dt, 0.1); // minimal fps 10 - avoid jumps of cursor
    if (_moveKey != 0)
    {
      _moveLast = Glob.uiTime;
      if (Glob.uiTime - _moveStart < 0.5) dt *= 0.5;

      switch (_moveKey)
      {
      case DIK_ADD:
        SetScale(exp(-dt) * GetScale());
        break;
      case DIK_SUBTRACT:
        SetScale(exp(dt) * GetScale());
        break;
      case DIK_NUMPAD1:
        _mapX += dt; SaturateX(_mapX);
        _mapY -= dt; SaturateY(_mapY);
        break;
      case DIK_NUMPAD2:
        _mapY -= dt; SaturateY(_mapY);
        break;
      case DIK_NUMPAD3:
        _mapX -= dt; SaturateX(_mapX);
        _mapY -= dt; SaturateY(_mapY);
        break;
      case DIK_NUMPAD4:
        _mapX += dt; SaturateX(_mapX);
        break;
      case DIK_NUMPAD6:
        _mapX -= dt; SaturateX(_mapX);
        break;
      case DIK_NUMPAD7:
        _mapX += dt; SaturateX(_mapX);
        _mapY += dt; SaturateY(_mapY);
        break;
      case DIK_NUMPAD8:
        _mapY += dt; SaturateY(_mapY);
        break;
      case DIK_NUMPAD9:
        _mapX -= dt; SaturateX(_mapX);
        _mapY += dt; SaturateY(_mapY);
        break;
      }
    }
  }

  if (GetParent() && GetParent()->IsTop() && !GetParent()->IsRealTime() && _drawCursorLines)  // can't use CStaticMap version because camera effect is active
  {
    // draw lines around tracking cursor
    RString cursorName = GetParent()->GetCursorName();
    if (cursorName.GetLength() > 0 && stricmp(cursorName, "Arrow") != 0)
    {
      int wScreen = GLOB_ENGINE->Width2D();
      int hScreen = GLOB_ENGINE->Height2D();
      const float mouseH = 16.0 / 600;
      const float mouseW = 16.0 / 800;
      float mouseX = 0.5 + GInput.cursorX * 0.5; //aspect ratio?
      float mouseY = 0.5 + GInput.cursorY * 0.5;
      PackedColor color = GetParent()->GetCursorColor();
      MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
      float pt = mouseX - mouseW;
      if (pt > _x)
        GEngine->Draw2D
        (
          mip, color,
          Rect2DPixel(wScreen * _x, hScreen * mouseY - 0.5 * _cursorLineWidth, wScreen * (pt - _x), _cursorLineWidth)
        );
      pt = mouseX + mouseW;
      if (pt < _x + _w)
        GEngine->Draw2D
        (
          mip, color,
          Rect2DPixel(wScreen * pt, hScreen * mouseY - 0.5 * _cursorLineWidth, wScreen * (_x + _w - pt), _cursorLineWidth)
        );
      pt = mouseY - mouseH;
      if (pt > _y)
        GEngine->Draw2D
        (
          mip, color,
          Rect2DPixel(wScreen * mouseX - 0.5 * _cursorLineWidth, hScreen * _y, _cursorLineWidth, hScreen * (pt - _y))
        );
      pt = mouseY + mouseH;
      if (pt < _y + _h)
        GEngine->Draw2D
        (
          mip, color,
          Rect2DPixel(wScreen * mouseX - 0.5 * _cursorLineWidth, hScreen * pt, _cursorLineWidth, hScreen * (_y + _h - pt))
        );
    }
  }  
  DrawLegend();
}

void CStaticMapEditor::DrawExt(float alpha)
{
  if (!_drawEditorObjects) return;
  if (!_ws || !_vars) return;

  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(_vars);
  gstate->VarSet("_map", CreateGameControl(this), true, false, GWorld->GetMissionNamespace());

  // pass scale so script may do icon autosize if it wishes
  float scale = _scaleX / (InvLandSize * 1000);
  gstate->VarSet("_scale", GameValue(scale), false, false, GWorld->GetMissionNamespace());  

  bool updateVisibility = Glob.uiTime - _lastObjectVisibilityUpdate > 0.2;
  if (updateVisibility)
    _lastObjectVisibilityUpdate = Glob.uiTime;

  // execute draw scripts
  for (int i=0; i<_ws->NObjects(); i++)
  {
    EditorObject *obj = _ws->GetObject(i);    

    if (updateVisibility)
    {
      obj->UpdateVisibleOnMap(gstate);
      obj->UpdateExecDrawMap(gstate);
    }

    obj->UpdateEvaluatedPos();
    
    if (!obj->IsVisible()) continue;

    // clipping
    Vector3 pos = obj->IsFading() ? obj->FadePos() : obj->GetEvaluatedPos();
    DrawCoord drawPos = WorldToScreen(pos);

    #define SCREEN_BUFFER 0.25 
    if (
      drawPos.x < _x - SCREEN_BUFFER ||
      drawPos.x > _x + _w + SCREEN_BUFFER ||
      drawPos.y < _y - SCREEN_BUFFER ||
      drawPos.y > _y + _h + SCREEN_BUFFER
      ) continue;    

    float alpha = 1.0f * obj->SimulateFade();
    if (!obj->IsObjInCurrentOverlay()) alpha = 0.1f;

    if (obj->IsSelected())
    {
      const float period = 0.3f;
      const float speed = H_PI / period;
      alpha = 0.33 * sin(speed * Glob.uiTime.toFloat()) + 0.67;
    }

    // draw object icons
    int n = obj->NIcons();

    PackedColor rotateColor = PackedColor(Color(0, 1, 1, 1));
    PackedColor raisingColor = PackedColor(Color(1, 0, 0, 1));
    PackedColor selectedColor = PackedColor(Color(0, 1, 0, 1));
    PackedColor mouseOverColor = PackedColor(Color(0.8, 0.8, 0, 1)); //yellow might be a bit hard to see!

    for (int j=0; j<n; j++)
    {
 
      EditorObjectIcon &icon = obj->GetIcon(j);
      if (!icon.is3D)
      {
        PackedColor color = icon.color;

        float iconScale = scale;

        EntityAI* veh = NULL;

        Object* realObj = obj->GetProxyObject();
        veh = dyn_cast<EntityAI>(realObj);

        // don't draw bounding box on person
        Person *soldier = dyn_cast<Person>(realObj);
        if (soldier)
          veh = NULL;

        float w,h;
        
        if(obj->IsSelected() && veh && veh->GetShape()) //TODO: enable selected states for map display as well!
        {
          EditorObject* mouseOverObj = GetParent()->GetObjMouseOver();
          PackedColor color = (mouseOverObj == obj || (mouseOverObj && mouseOverObj->IsSelected())) ? mouseOverColor : selectedColor;
//          if (GetParent()->_rotating) color = rotateColor;
//          if (GetParent()->_raising) color = raisingColor;

          Vector3 minMax[2];
          minMax[0] = veh->GetShape()->MinMax()[0];
          minMax[1] = veh->GetShape()->MinMax()[1];

          DrawCoord pt1 = WorldToScreen(veh->PositionModelToWorld(Vector3(minMax[0].X(), 0, minMax[0].Z())));
          DrawCoord pt2 = WorldToScreen(veh->PositionModelToWorld(Vector3(minMax[0].X(), 0, minMax[1].Z())));
          DrawCoord pt3 = WorldToScreen(veh->PositionModelToWorld(Vector3(minMax[1].X(), 0, minMax[1].Z())));
          DrawCoord pt4 = WorldToScreen(veh->PositionModelToWorld(Vector3(minMax[1].X(), 0, minMax[0].Z())));

          GEngine->DrawLine( Line2DPixel(pt1.x * _wScreen, pt1.y * _hScreen, pt2.x * _wScreen, pt2.y * _hScreen),color, color, _clipRect);
          GEngine->DrawLine( Line2DPixel(pt2.x * _wScreen, pt2.y * _hScreen, pt3.x * _wScreen, pt3.y * _hScreen),color, color, _clipRect);
          GEngine->DrawLine( Line2DPixel(pt3.x * _wScreen, pt3.y * _hScreen, pt4.x * _wScreen, pt4.y * _hScreen),color, color, _clipRect);
          GEngine->DrawLine( Line2DPixel(pt4.x * _wScreen, pt4.y * _hScreen, pt1.x * _wScreen, pt1.y * _hScreen),color, color, _clipRect);
        }

        //type is static, display icon according to size
        if(veh && veh->Type()->_mapUseRealSize && veh->GetShape() && obj->GetType()->ProxyVisibleInPreview())
        {
/*          static float myScaleFactor = 2.5f; 
          if(GInput.keys[DIK_RSHIFT] > 0.0f || GInput.keys[DIK_RCONTROL] > 0.0f)
          {
            if(GInput.keys[DIK_RSHIFT] > 0.0f) myScaleFactor += 0.01;
            if(GInput.keys[DIK_RCONTROL] > 0.0f) myScaleFactor -= 0.01;
          } */
          float iconScaleX = _scaleX / (InvLandSize * 1000) * 1.6; //isn't that Fov?
          float iconScaleY = _scaleY / (InvLandSize * 1000) * 2.0;
          Vector3 minMax[2];
          minMax[0] = veh->GetShape()->MinMax()[0];
          minMax[1] = veh->GetShape()->MinMax()[1];

          float xSize = minMax[1].X() - minMax[0].X();
          float zSize = minMax[1].Z() - minMax[0].Z();

          w = (xSize > zSize ? xSize : zSize) / iconScaleX;
          h = (xSize > zSize ? xSize : zSize) / iconScaleY;
        }
        else
        {
          saturateMax(iconScale, icon.minScale);

          w = icon.width * 1 / iconScale;
          h = icon.height * 1 / iconScale;

          if(icon.maintainSize)
          {
            w = icon.width;
            h = icon.height;
          }
          else
          {
            saturateMax(w, 5);
            saturateMax(h, 5);
          }
        }

        color.SetA8(toIntFloor(color.A8()*alpha));

        if (realObj && icon.rotateWithObj && !obj->IsFading())
          icon.angle = atan2(realObj->Direction().X(), realObj->Direction().Z());

        DrawSign(icon.icon,color,Vector3(pos.X() + icon.offset.X(),0,pos.Z() + icon.offset.Z()),w,h,icon.angle,icon.text,icon.shadow);
      }
    }
    

    if (!obj->ExecDrawMap()) continue; 

    // draw object
    gstate->VarSet("_alpha", GameValue(alpha), true, false, GWorld->GetMissionNamespace());
    gstate->VarSet("_inLayer", obj->IsObjInCurrentOverlay(), true, false, GWorld->GetMissionNamespace());
    obj->ExecuteDrawMapScript(gstate);
  }
  gstate->EndContext();

  // draw links
  AutoArray<ObjectLink> objectLinks = _ws->GetDrawLinks();
  for (int i=0; i<objectLinks.Size(); i++)
  {    
    ObjectLink &objectLink = objectLinks[i];
    if (objectLink.to && objectLink.from)
    {
      if (
        objectLink.from->IsVisible() && objectLink.to->IsVisible() &&
        !objectLink.from->IsFading() && !objectLink.to->IsFading()
      )
      {
        Vector3 pos1 = objectLink.to->GetEvaluatedPos();
        Vector3 pos2 = objectLink.from->GetEvaluatedPos();

        PackedColor color = objectLink.color;
        if (!objectLink.to->IsObjInCurrentOverlay() && !objectLink.from->IsObjInCurrentOverlay()) 
          color.SetA8(toIntFloor(color.A8()*0.1));

        if (objectLinks[i].lineType == LTLine || scale > objectLink.arrowScaleMin)  // WORK ON THIS
          DrawLine(pos2, pos1, color);
        else
          DrawArrow(pos2, pos1, color);
      }
    }
    else
      objectLinks.Delete(i--);
  }

  // draw arrow if linking objects
  if (_linkObject)
    DrawArrow(_linkObject->GetEvaluatedPos(), _dragPos, PackedBlack);

  // draw multiple select box
  else if (_selecting)
  {
    PackedColor color = PackedColor(Color(0,1,0,1));
    DrawCoord pt1 = WorldToScreen(_startPos);
    DrawCoord pt2 = WorldToScreen(_dragPos);
    GEngine->DrawLine
    (
      Line2DPixel(pt1.x * _wScreen, pt1.y * _hScreen,
      pt2.x * _wScreen, pt1.y * _hScreen),
      color, color, _clipRect
    );
    GEngine->DrawLine
    (
      Line2DPixel(pt2.x * _wScreen, pt1.y * _hScreen,
      pt2.x * _wScreen, pt2.y * _hScreen),
      color, color, _clipRect
    );
    GEngine->DrawLine
    (
      Line2DPixel(pt2.x * _wScreen, pt2.y * _hScreen,
      pt1.x * _wScreen, pt2.y * _hScreen),
      color, color, _clipRect
    );
    GEngine->DrawLine
    (
      Line2DPixel(pt1.x * _wScreen, pt2.y * _hScreen,
      pt1.x * _wScreen, pt1.y * _hScreen),
      color, color, _clipRect
    );
  }
  
  // draw camera position
  if (_camera)
  {
    DrawSign(
      _camera->GetIcon2D(),
      _camera->GetColorIcon2D(),
      _camera->WorldPosition(_camera->FutureVisualState()),
      _camera->GetWidthIcon2D(),
      _camera->GetHeightIcon2D(),
      atan2(_camera->Direction().X(), _camera->Direction().Z()),
      RString(),
      true
    );
  }

  // update help text
  if (GetParent() && GetParent()->MouseOverMap() && _ws)
  {
    if (_infoMove._type == signEditorObject)
    {
      EditorObject *obj = _ws->FindObject(_infoMove._name);
      if (obj)
        if (obj->IsSelected())
          GetParent()->SetSelectedStatus(ESSelected);
        else
          GetParent()->SetSelectedStatus(ESUnSelected);
    }
    else
      GetParent()->SetSelectedStatus(ESNone);
  }
  //-!

#if _VBS3 
  GVBSVisuals.DrawExt(alpha,*this);
#endif

  PackedColor colorLabel(Color(1, 1, 1, 1));
  DrawLabel(_infoMove, colorLabel);
}

Object *CStaticMapEditor::FindObject(RString id)
{
  if (GetParent())
    return GetParent()->FindObject(id);
  return NULL;
}

void CStaticMapEditor::DeleteObjectFromEditor(RString id)
{
  if (GetParent())
    GetParent()->DeleteObjectFromEditor(id);
};

bool CStaticMapEditor::IsRealTime() 
{
  if (GetParent())
    return GetParent()->IsRealTime();
  return false;
}

bool CStaticMapEditor::CommandMenuVisible() 
{
  if (GetParent())
    return GetParent()->CommandMenuVisible();
  return false;
}

void CStaticMapEditor::ShowCommandMenu(bool show) 
{
  if (GetParent())
    GetParent()->ShowCommandMenu(show);
}

MissionEditorCamera *CStaticMapEditor::CameraGet()
{
  if (GetParent())
    return GetParent()->CameraGet();
  return NULL;
}

void CStaticMapEditor::CameraRestart()
{
  if (GetParent())
    GetParent()->CameraRestart();
}

void CStaticMapEditor::UpdateProxy(Object *object, EditorObject *edObj) 
{
  if (GetParent())
    GetParent()->UpdateProxy(object,edObj);
}

void CStaticMapEditor::DeleteProxy(EditorObject *edObj)
{
  if (GetParent())
    GetParent()->DeleteProxy(edObj);
}

void CStaticMapEditor::LockCamera(RString id, Vector3 lockPos)
{
  if (GetParent())
    GetParent()->LockCamera(id, lockPos);
}

void CStaticMapEditor::SetOnDoubleClick(RString eh)
{
  if (GetParent())
    GetParent()->SetOnDoubleClick(eh);  
}

void CStaticMapEditor::SetOnShowNewObject(RString eh)
{
  if (GetParent())
    GetParent()->SetOnShowNewObject(eh);  
}

void CStaticMapEditor::SetOnQuickAddObject(RString eh, int key)
{
  if (GetParent())
    GetParent()->SetOnQuickAddObject(eh, key);  
}

void CStaticMapEditor::SetOnQuickAddObjectToggled(RString eh)
{
  if (GetParent())
    GetParent()->SetOnQuickAddObjectToggled(eh);  
}

void CStaticMapEditor::ToggleOnQuickAddObject(bool on)
{
  if (GetParent())
    GetParent()->ToggleOnQuickAddObject(on);  
}

void CStaticMapEditor::UpdateObjectBrowser() 
{
  if (GetParent()) 
    GetParent()->UpdateObjectBrowser();
}

void CStaticMapEditor::UpdateObjectBrowser(RString toUpdate, bool updateVisibility, bool expand) 
{
  if (GetParent()) 
    GetParent()->UpdateObjectBrowser(toUpdate, updateVisibility, expand);
}

void CStaticMapEditor::UpdateObjectBrowser(const EditorObject *obj)
{
  if (GetParent()) 
    GetParent()->UpdateObjectBrowser(obj);
}

void CStaticMapEditor::LookAtPosition(Vector3 pos) 
{
  if (GetParent()) 
    GetParent()->LookAtPosition(pos);
}

void CStaticMapEditor::ImportAllGroups() 
{
  if (GetParent()) 
    GetParent()->ImportAllGroups();
}

void CStaticMapEditor::CreateWaypointEdObjs(AIGroup *aGroup, RString grpEdObj, RString unitEdObj)
{
  if (GetParent()) 
    GetParent()->CreateWaypointEdObjs(aGroup, grpEdObj, unitEdObj);
}

RString CStaticMapEditor::CreateUnitEdObj(Person *person, Transport *trans, RString vehEdObj, RString grpEdObj, RString leaderEdObj)
{
  if (GetParent()) 
    return GetParent()->CreateUnitEdObj(person, trans, vehEdObj, grpEdObj, leaderEdObj);
  return RString();
}

RString CStaticMapEditor::CreateVehEdObj(Transport *trans, RString grpEdObj)
{
  if (GetParent()) 
    return GetParent()->CreateVehEdObj(trans,grpEdObj);
  return RString();
}

int CStaticMapEditor::AddMenu(RString text, float priority)
{
  if (GetParent()) 
    return GetParent()->AddMenu(text, priority);
  return -1;
}

void CStaticMapEditor::NewOverlay(ConstParamEntryPtr entry)
{
  if (GetParent()) 
    GetParent()->NewOverlay(entry);
}

void CStaticMapEditor::LoadOverlay(ConstParamEntryPtr entry)
{
  if (GetParent()) 
    GetParent()->LoadOverlay(entry);
}

void CStaticMapEditor::SaveOverlay()
{
  if (GetParent()) 
    GetParent()->SaveOverlay();
}

void CStaticMapEditor::ClearOverlay(bool commit, bool close)
{
  if (GetParent()) 
    GetParent()->ClearOverlay(commit, close);
}

void CStaticMapEditor::DeleteOverlay(EditorOverlay *overlay)
{
  if (GetParent()) 
    GetParent()->DeleteOverlay(overlay);
}

void CStaticMapEditor::CreateMenu(int index)
{
  if (GetParent()) 
    GetParent()->CreateMenu(index);
}

int CStaticMapEditor::AddMenuItem(RString type, RString text, RString command, int index, float priority, bool assignPriority)
{
  if (GetParent()) 
    return GetParent()->AddMenuItem(type, text, command, index, priority, assignPriority);
  return -1;
}

void CStaticMapEditor::UpdateMenuItem(int index, RString text, RString command) 
{
  if (GetParent()) 
    GetParent()->UpdateMenuItem(index, text, command);
}

void CStaticMapEditor::RemoveMenuItem(int index, RString text)
{
  if (GetParent()) 
    return GetParent()->RemoveMenuItem(index,text);
}

int CStaticMapEditor::NMenuItems(RString text, int index)
{
  if (GetParent()) 
    return GetParent()->NMenuItems(text,index);
  return 0;
}

int CStaticMapEditor::GetNextMenuItemIndex()
{
  if (GetParent()) 
    return GetParent()->GetNextMenuItemIndex();
  return -1;
}

void CStaticMapEditor::AllowFileOperations(bool allow)
{
  if (GetParent()) 
    GetParent()->AllowFileOperations(allow);
}

void CStaticMapEditor::Allow3DMode(bool allow)
{
  if (GetParent()) 
    GetParent()->Allow3DMode(allow);
}

void CStaticMapEditor::Enable3DLinking(bool allow)
{
  if (GetParent()) 
    GetParent()->Enable3DLinking(allow);
}

void CStaticMapEditor::ExitOnMapKey(bool allow)
{
  if (GetParent()) 
    GetParent()->ExitOnMapKey(allow);
}

bool CStaticMapEditor::IsShow3DIcons()
{
  if (GetParent()) 
    return GetParent()->IsShow3DIcons();
  return false;
}

void CStaticMapEditor::Show3DIcons(bool show)
{
  if (GetParent()) 
    GetParent()->Show3DIcons(show);
}

EditorMode CStaticMapEditor::GetMode()
{
  if (GetParent()) 
    return GetParent()->GetMode();
  return EMMap;
}

void CStaticMapEditor::CreateObject(EditorObject *obj, bool isNewObject, bool isPaste, bool isLoading, bool isCalledByExecEditorScriptCommand, bool isCreatedFromTemplate)
{
  if (GetParent()) 
    GetParent()->CreateObject(obj, isNewObject, isPaste, isLoading, isCalledByExecEditorScriptCommand, isCreatedFromTemplate);
}

void CStaticMapEditor::SetMode(EditorMode mode, bool overrideAllow3D)
{
  if (GetParent()) 
    GetParent()->SetMode(mode, overrideAllow3D);
}

void CStaticMapEditor::SetFogOfWar(FogOfWar fogOfWar, OLinkArray(Object) &objs, bool fromPlayer)
{
  if (GetParent()) 
    GetParent()->SetFogOfWar(fogOfWar, objs, fromPlayer);
}

void CStaticMapEditor::SetEditorEventHandler(RString name, RString value)
{
  if (GetParent()) 
    GetParent()->SetEditorEventHandler(name,value);
}

void CStaticMapEditor::SelectObject(EditorObject *obj) 
{
  if (GetParent())
    GetParent()->SelectObject(obj->GetArgument("VARIABLE_NAME"));
}

void CStaticMapEditor::UnselectObject(EditorObject *obj) 
{
  if (GetParent())
    GetParent()->UnSelectObject(obj->GetArgument("VARIABLE_NAME"));
}

void CStaticMapEditor::ClearSelected() 
{
  if (GetParent())
    GetParent()->ClearSelected();
}

void CStaticMapEditor::ShowEditObject(RString id)
{
  if (GetParent())
    GetParent()->ShowEditObject(id);
}

void CStaticMapEditor::ShowNewObject(RString type, RString className, TargetSide side, Vector3 posn)
{
  if (GetParent())
  {
    if (className.GetLength() > 0)
      GetParent()->ClearLastObjectCreated();

    GetParent()->SelectEditorObject(type);
    GetParent()->ShowNewObject(posn,NULL,RString(),className,side);
  }
};

RString CStaticMapEditor::AddObject(RString typeName, const AutoArray<RString> &args, bool create, GameValue variable, ConstParamEntryPtr subTypeConfig)
{
  ProgressRefresh();
  EditorObjectType *type = _ws->FindObjectType(typeName);
  if (type && GetParent())
  { 
    EditorObject *obj = _ws->AddObject(type);

    // assign subtype
    if (subTypeConfig)
    {
      Ref<EditorObjectType> subType = new EditorObjectType(RString(),subTypeConfig);
      obj->AssignSubType(subType,subTypeConfig->GetName());
    }

    obj->SetArguments(args);

    // set default arguments
    const EditorParams &params = obj->GetAllParams();
    for (int i=0; i<params.Size(); i++)
    {
      const EditorParam *param = &params.Get(i);
      if (param->hasDefValue)
      {
        // check if already added      
        if (obj->GetArgument(param->name).GetLength() == 0)
          obj->SetArgument(param->name,param->defValue);
      }
    }

    if (create)
      GetParent()->CreateObject(obj,true);    
    else
    {
      GameState *gstate = GWorld->GetGameState();

      // so variable name (eg _team_1) is visible as %VARIABLE_NAME in editor definitions
      _vars->VarSet(obj->GetArgument("VARIABLE_NAME"), variable, true);

      // set initial visibility (hide all but the parent)
      if (obj->GetArgument("PARENT").GetLength() > 0)
        obj->SetVisibleInTree(false);

      RString statement = obj->GetProxy();
      GameValue result;

      // add object    
      gstate->BeginContext(_vars);
      if (statement.GetLength() > 0) result = gstate->EvaluateMultiple(statement, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      gstate->EndContext();

      // register proxy
      if (!result.GetNil() && result.GetType() == GameObject)
      {
        Object *object = static_cast<GameDataObject *>(result.GetData())->GetObject();
        if (object)
        {
          GetParent()->AddProxy(object,obj);
          obj->SetProxyObject(object);
        }
      }
    }
    
    GetParent()->UpdateObjectBrowser(obj);
    return obj->GetArgument("VARIABLE_NAME");
  }
  return RString();
}

void CStaticMapEditor::EditLink(EditorObject *obj, RString name, float key, bool assignLinkArg)
{
  if (obj)
    if (obj->GetScope() < EOSLinkFrom) return;
  _linkObject = obj;
  _linkArgument = assignLinkArg ? name : RString();  // linkArgument is assigned when left-click next occurs
  _linkShortcutKey = key;

#ifndef _XBOX
  if (obj && GetParent()) GetParent()->OnEditorEvent(MELinkFromObject,_linkObject->GetArgument("VARIABLE_NAME"),_linkArgument,true);
#endif

  AspectSettings _aspect;
  GEngine->GetAspectSettings(_aspect);
  float x = 0.5 + GInput.cursorX/_aspect.leftFOV * 0.5;
  float y = 0.5 + GInput.cursorY * 0.5;
  _dragPos = ScreenToWorld(Point2DFloat(x, y));  
}

void CStaticMapEditor::OnLButtonDown(float x, float y)
{
  if (GetParent() && GetParent()->IsAddOnLeftClick())
  {
    // add object with default settings
    Vector3 pos = ScreenToWorld(DrawCoord(x, y));
    pos[1] = GLandscape->RoadSurfaceYAboveWater(pos[0],pos[2]);
    GetParent()->ProcessOnQuickAddObjectEvent(pos);
    return;
  }  

  _infoClick = FindSign(x, y);

  // linking one object to another?
  if (_linkObject)
  {    
    _selecting = false;
    _selectOne = true;

    bool showNewObj = true;

    if (GetParent())
    {
      const EditorParam *param = _linkObject->GetAllParams().Find(_linkArgument);

      // holding down shortcut key and linking to nothing?
        // search only for context link type with the matching shortcut key
      if (!param && _infoClick._type != signEditorObject)
      {
        const EditorParams &params = _linkObject->GetAllParams();
        int nParams = params.Size();
        for (int i=0; i<nParams; i++)
        {
          if (params[i].source == EPSContext && params[i].shortcutKey == _linkShortcutKey)
          {
            if (IsMenuItemActive(params[i].activeMode, _linkObject, this,GetParent()->IsRealTime()))
              param = &params[i];
          }
        }    
      }
      
      // link to new object?
      if (param && param->source == EPSContext)
      {
        const char *ptr = param->subtype;
        const char *ext = strchr(ptr, ',');

        // editor object type
        RString objectType = Trim(ptr, ext - ptr);
               
        // link type
        ptr = ext + 1;
        RString linkType = Trim(ptr, strlen(ptr));

        // is there a type in memory that matches the one we are trying to add?        
        if (GetParent()->LastObjectCreated())
        {
          EditorObjectType *type = GetParent()->LastObjectCreated()->GetType();
          if (type)
          {
            if (stricmp(objectType,type->GetName()) == 0)
            {
              // try and find a suitable link parameter that we can use to link the new (copied) object with the linkObject
                // look for the parameter with the same shortcut key TODO: is there a better way?
              const EditorParams &params = _linkObject->GetAllParams();
              int nParams = params.Size();
              for (int i=0; i<nParams; i++)
              {
                if (
                  params[i].source == EPSLink && 
                  (_linkShortcutKey == -1 || params[i].shortcutKey == _linkShortcutKey) &&
                  stricmp(params[i].type,type->GetName()) == 0 &&
                  stricmp(params[i].name,linkType) == 0
                )
                {                  
                  _linkArgument = params[i].name;
                  showNewObj = false;
                }
              }                  
            }
          }
        }

        // set correct object type (matching name)
        if (showNewObj)
        {
          if (GetParent()->SelectEditorObject(objectType))
          {
            Vector3 pos = ScreenToWorld(DrawCoord(x, y));
            pos[1] = GLandscape->SurfaceYAboveWater(pos[0],pos[2]);
            GetParent()->ShowNewObject(pos,_linkObject,linkType);
          }
        }
        else
        {
          // create the new object based on what is in memory, and link to it
          EditorObject *obj = _ws->CopyObject(GetParent()->LastObjectCreated());
          if (obj)
          {
            // assign new name (because old name has been copied over)
            EditorObjectType *type = obj->GetType();
            if (type)
            {        
              obj->RemoveArgument("VARIABLE_NAME");        
              obj->SetArgument("VARIABLE_NAME", type->NextVarName());
            }

            // convert position back to string
            Vector3 clickPos = ScreenToWorld(Point2DFloat(x, y));
            RString value = Format("[%.5f, %.5f]", clickPos.X(), clickPos.Z());

            // update argument - position
            obj->RemoveArgument("POSITION");
            obj->SetArgument("POSITION", value);

            obj->SetParentOverlay(_ws->GetActiveOverlay(), true);

            GetParent()->CreateObject(obj,false,false,false,false,true);

            RString id = obj->GetArgument("VARIABLE_NAME");            
            GetParent()->OnLinkEditted(_linkObject, _linkArgument, id, -1, this);       
            GetParent()->SelectObject(id);
          }
        }
      }
      else
      {
        RString name;

        // link to position?
        if (param && param->source == EPSLink && stricmp(param->type, "position") == 0)
        {
          Vector3 clickPos = ScreenToWorld(Point2DFloat(x, y));
          name = Format("[%.5f, %.5f]", clickPos.X(), clickPos.Z());
        }
        // link to existing object?
        else if (_infoClick._type == signEditorObject) 
          name = _infoClick._name;        

        // do linkage
        GetParent()->OnLinkEditted(_linkObject, _linkArgument, name, _linkShortcutKey, this);       
        GetParent()->SelectObject(name);
      }
    }
    
    if (showNewObj || GInput.keys[_linkShortcutKey] == 0)
    {
#ifndef _XBOX
      if (_linkObject && GetParent()) GetParent()->OnEditorEvent(MELinkFromObject,_linkObject->GetArgument("VARIABLE_NAME"),_linkArgument,false);
#endif
      _linkObject = NULL;
      _linkShortcutKey = -1;
    }
    if (GetParent()) GetParent()->UpdateControlsHelp();
  }
  // left mouse down on an editor object?
  else if (_infoClick._type == signEditorObject)
  {
    EditorObject *obj = NULL;

    // select object
    if (GetParent() && _ws)
    {
      obj = _ws->FindObject(_infoClick._name);      
      if (obj)
      {
        // select object in object browser
        GetParent()->OnObjectSelected(_infoClick._name);

        // select object if not selected
        if (!obj->IsSelected())
          GetParent()->SelectObject(_infoClick._name);          
        // ... unless control is held down, in which case we unselect the object
        else
          if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
            GetParent()->UnSelectObject(_infoClick._name);        
      }
    }

    // was the link shortcut key pressed (held down)?
    if (obj)
    {
      // search for shortcut keys
      const EditorParams &params = obj->GetAllParams();
      int nParams = params.Size();
      for (int i=0; i<nParams; i++)
      {
        if ((params[i].source != EPSLink && params[i].source != EPSParent && params[i].source != EPSContext) || params[i].shortcutKey < 0) continue;
        if (GInput.keys[params[i].shortcutKey] > 0)
        {
          EditLink(obj, params[i].name, params[i].shortcutKey);
          return;
        }
      }      
    }
    //-!

    _dragging = true;
    _selecting = false;
    _dragPos = ScreenToWorld(Point2DFloat(x, y));
  }
  _startPos = ScreenToWorld(Point2DFloat(x, y)); 
}

void CStaticMapEditor::OnLButtonUp(float x, float y)
{
  if (GetParent() && GetParent()->IsAddOnLeftClick()) return;

  if (_selecting && _ws && GetParent())
  {
    _selecting = false;

    if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
      GetParent()->ClearSelected();

    float xMin = floatMin(_startPos.X(), _dragPos.X());
    float xMax = floatMax(_startPos.X(), _dragPos.X());
    float zMin = floatMin(_startPos.Z(), _dragPos.Z());
    float zMax = floatMax(_startPos.Z(), _dragPos.Z());

    for (int i=0; i<_ws->NObjects(); i++)
    {
      EditorObject *obj = _ws->GetObject(i);
      if (!obj->IsVisibleInTree()) continue;
      Vector3 objPos = obj->GetEvaluatedPos();
      if
      (
        objPos.X() >= xMin && objPos.X() < xMax &&
        objPos.Z() >= zMin && objPos.Z() < zMax
      ) 
      {
        _ws->SetSelected(obj, !obj->IsSelected(), true, true, true);
        if (GetParent()) 
        {
#ifndef _XBOX
          GetParent()->OnEditorEvent(MESelectObject, obj->GetArgument("VARIABLE_NAME"), obj->GetProxyObject());
#endif
          GetParent()->UpdateControlsHelp();
        }
      }
    }
  }
  else if (!_dragging)
  {
    _infoClick = _infoMove = FindSign(x, y);
  }
  else if (_dragging && GetParent())
  {
    if (GetParent()->IsMovingEdObj())
    {
      GetParent()->UpdateRemoteSelObjects();
#ifndef _XBOX
      GetParent()->OnEditorEvent(MEStopMovingObject);
#endif
      GetParent()->RecordAction();
      
      // move camera to look at the moved object
      if (GetParent()->GetMode() == EMMap && _ws->GetSelected().Size() > 0)
      {
        EditorObject *obj = _ws->GetSelected()[0];
        if (obj)
          GetParent()->SetCameraPosition(obj->GetArgument("VARIABLE_NAME"),false);
      }
    }
  }
  if (GetParent()) GetParent()->SetMovingEdObj(false);
  _dragging = false;
}

static RString GetMapOnSingleClick()
{
  if (GMapOnSingleClick.GetNil()) return RString();
#if USE_PRECOMPILATION
  if (GMapOnSingleClick.GetType() == GameCode)
  {
    GameDataCode *code = static_cast<GameDataCode *>(GMapOnSingleClick.GetData());
    return code->GetString();
  }
  else
#endif
  {
    return GMapOnSingleClick;
  }
}

void CStaticMapEditor::OnLButtonClick(float x, float y)
{
  if (GetParent() && GetParent()->IsAddOnLeftClick()) return;

  // process onMapSingleClick    
  if (!_dragging)
  {
    if (GetParent() && !_selectOne && _infoClick._type != signEditorObject && !GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
      GetParent()->ClearSelected();

    RString eventHandler = GetMapOnSingleClick();
    if (eventHandler.GetLength()>0)
      ProcessOnClick(eventHandler,ScreenToWorld(DrawCoord(x, y)),this);
  
    // _selectOne below is reqd to prevent select box following linkage

    // start drawing multiple select box?
    if (!_selectOne && GInput.mouseL)
    {
      _dragging = false;
      _selecting = true;
      // startpos is defined when the mouse is first depressed in LButtonDown
    }

    _selectOne = false;
  }
}

// TODO: remove new object resource & code
void CStaticMapEditor::OnLButtonDblClick(float x, float y)
{
  if (GetParent() && GetParent()->IsAddOnLeftClick()) return;

  _infoClick = _infoMove = FindSign(x, y);
  if (GetParent())
  {    
    if (GInput.keys[DIK_LSHIFT]>0) return;
    if (!_infoMove._type || GInput.keys[DIK_LCONTROL])
    {
      Vector3 pos = ScreenToWorld(DrawCoord(x, y));
      pos[1] = GLandscape->SurfaceYAboveWater(pos[0],pos[2]);
      if (GetParent()->GetOnDoubleClick().GetLength()>0)
        ProcessOnClick(GetParent()->GetOnDoubleClick(),pos,this,true);
      else
        GetParent()->ShowNewObject(pos);
    }
    else
      GetParent()->ShowEditObject(_infoMove._name);
  }
}

void CStaticMapEditor::OnRButtonDown(float x, float y)
{
  if (_parent) _parent->RemoveContextMenu();
  _draggingRM = true;
  _dragPos = ScreenToWorld(Point2DFloat(x, y));

  if (GInput.keys[DIK_LALT] || GInput.keys[DIK_LSHIFT]) return;

  // for rotation
  _infoClick = FindSign(x, y);

  if (_infoClick._type == signEditorObject)
  {
    EditorObject *obj = NULL;

    // select object
    if (GetParent() && _ws)
    {
      obj = _ws->FindObject(_infoClick._name);      
      if (obj)
      {
        // select object in object browser
        GetParent()->OnObjectSelected(_infoClick._name);

        // select object if not selected
        if (!obj->IsSelected())
          GetParent()->SelectObject(_infoClick._name);          
        // ... unless control is held down, in which case we unselect the object
        else
          if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
            GetParent()->UnSelectObject(_infoClick._name);        
      }
    }
  }
}

void CStaticMapEditor::OnRButtonUp(float x, float y)
{
  if (GetParent()) 
  {
    if (GetParent()->IsDragging())
    {
      GetParent()->UpdateRemoteSelObjects();
      GetParent()->RecordAction();
    }
    GetParent()->SetRotating(false);
    GetParent()->SetRaising(false);
  }

  _draggingRM = false;
}

void DisplayMissionEditor::DoInit(RString initSQF)
{
  // read and preprocess init files
  GameState *gstate = GWorld->GetGameState();
  if (initSQF.GetLength() > 0)
  {
    RString filename = initSQF;
    FilePreprocessor preproc;
    QOStrStream processed;
    if (preproc.Process(&processed, filename))
    {
      RString script(processed.str(),processed.pcount());      
      gstate->BeginContext(&_vars);
      gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());
      gstate->VarLocal("_this");
      gstate->VarSet("_this", gstate->CreateGameValue(GameArray), true, false, GWorld->GetMissionNamespace());
      gstate->Execute(script, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      gstate->EndContext();
    }
  }
  else
  {
    ConstParamEntryPtr clsImport = Pars.FindEntry("CfgEditorInit");
    if (clsImport)
    {
      int n = clsImport->GetEntryCount();
      for (int i=0; i<n; i++)
      {
        ParamEntryVal entry = clsImport->GetEntry(i);
        if (entry.IsClass()) continue;
        RString filename = entry;
        FilePreprocessor preproc;
        QOStrStream processed;
        if (preproc.Process(&processed, filename))
        {
          RString script(processed.str(),processed.pcount());
          gstate->BeginContext(&_vars);
          gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());
          gstate->VarLocal("_this");
          gstate->VarSet("_this", gstate->CreateGameValue(GameArray), true, false, GWorld->GetMissionNamespace());
          gstate->Execute(script, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
          gstate->EndContext();
        }
      }
    }
  }

  // set up help text
  _selectedObject = ESNone;
  UpdateControlsHelp();

  // read country filter
  GameDataNamespace globals(NULL, RString(), false); // save - no global namespace
  ParamFile cfg;
  ParseUserParams(cfg, &globals);
  ConstParamEntryPtr entry = cfg.FindEntry("editorFilter");
  if (entry) _filter = *entry;

  // fill combo/list boxes
  OnLBSelChanged(GetCtrl(IDC_EDITOR_ADDOBJ_TYPES), 0);

  // execute showmap EH
  OnEditorEvent(MEShowMap, _mode == EMMap);

  // center map
  if (_map) _map->Center();
  if (_miniMap) 
  {
    _miniMap->Center();    
    _miniMap->SetMainMap((CStaticMapEditor *)_map.GetRef());
    _miniMap->DrawMainMapBox(true);
  }
}

void CStaticMapEditor::ProcessMouseOverEvent()
{
  if (GetParent() && _ws)
  { 
    EditorObject *oldOver = GetParent()->GetObjMouseOver();

    if (oldOver && _infoMove._type != signEditorObject)
      return GetParent()->SetObjMouseOver(NULL);

    RString oldName = oldOver ? oldOver->GetArgument("VARIABLE_NAME") : RString();
    
    if (stricmp(oldName,_infoMove._name))
      GetParent()->SetObjMouseOver(_ws->FindObject(_infoMove._name));
  }
}

void CStaticMapEditor::OnMouseHold(float x, float y, bool active)
{  
  if (!_dragging && IsInside(x, y))
  {
    _infoMove = FindSign(x, y);
    ProcessMouseOverEvent();
  }  
};

void CStaticMapEditor::OnMouseMove(float x, float y, bool active)
{
  Vector3 curPos = ScreenToWorld(Point2DFloat(x, y));
  
  // this will clear text labels as soon as mouse is moved off an object
  if (Glob.uiTime - _firstMove > 0.5)
  {
    _firstMove = Glob.uiTime;
    _infoMove = FindSign(x, y);
    ProcessMouseOverEvent();
  }

  if (_draggingRM)
  {
    // rotate object - note that actual rotation is completed in DisplayMissionEditor::OnSimulate
    if (GetParent())
    {
      if (GInput.keys[DIK_LSHIFT])
      {
        GetParent()->SetRotating(true);
        return;
      }
      else
        GetParent()->SetRotating(false);

      if (GInput.keys[DIK_LALT])
      {
        GetParent()->SetRaising(true);
        return;
      }
      else
        GetParent()->SetRaising(false);
    }

    DrawCoord mouseMap = WorldToScreen(_dragPos);
    _mapX += x - mouseMap.x;
    SaturateX(_mapX);
    _mapY += y - mouseMap.y;
    SaturateY(_mapY);

    // _dragPos must remain the original value when dragging commenced
    return;
  }
  else if (_dragging)
  {    
    Vector3 offset = curPos - _dragPos;

    // offset selected objects
    if (GetParent() && _ws) 
    {
      GetParent()->SetMovingEdObj(true);
      for (int i=0; i<_ws->GetSelected().Size(); i++)
      {        
        EditorObject *obj = _ws->GetSelected()[i];
        GetParent()->OnObjectMoved(obj, offset);
      }
    }
    //_dragPos = curPos;
  }
  /*
  else if (_linkObject || _selecting)
  {

  } 
  */
  _dragPos = curPos; 
}

void CStaticMapEditor::OnRButtonClick(float x, float y)
{
  SignInfo info = FindSign(x, y);

  RString id;
  if (info._type == signEditorObject) id = info._name;
  Vector3 pos = ScreenToWorld(DrawCoord(x, y));
  if (_parent) _parent->CreateContextMenu(
    x, y, IDC_EDITOR_MENU,
    Pars >> "RscDisplayMissionEditor" >> "Menu",
    CCMContextEditor(id, this, pos));
}

void CObjectBrowser::OnRButtonClick(float x, float y)
{
  if (!_parent) return;

  // select
  OnLButtonDown(x, y);

  CTreeItem *item = FindItem(x, y);

  // create menu
  if (item) _parent->CreateContextMenu(
    x, y, IDC_EDITOR_MENU,
    Pars >> "RscDisplayMissionEditor" >> "Menu",
    CCMContextEditor(item->data, NULL, VZero));
}

void CObjectBrowser::OnMouseMove(float x, float y, bool active)
{
  if (!_parent) return;

#if _VBS3
  CTreeItem *item = IsInside(x,y) ? FindItem(x, y) : NULL;
#else
  CTreeItem *item = FindItem(x, y);
#endif

  // create menu
  if (item)
    SetTooltip(item->text);
}

inline DisplayMissionEditor *CBackground3D::GetParent()
{
  return static_cast<DisplayMissionEditor *>(_parent.GetLink());
}

void CBackground3D::OnLButtonDown(float x, float y)
{
  if (GetParent()) GetParent()->OnLButtonDown(x, y);
}

void CBackground3D::OnLButtonUp(float x, float y)
{
  if (GetParent()) GetParent()->OnLButtonUp(x, y);
}

void CBackground3D::OnLButtonClick(float x, float y)
{
  if (GetParent()) GetParent()->OnLButtonClick(x, y);
}

void CBackground3D::OnLButtonDblClick(float x, float y)
{
  if (GetParent()) GetParent()->OnLButtonDblClick(x, y);
}

void CBackground3D::OnMouseMove(float x, float y, bool active)
{
  if (GetParent()) GetParent()->OnMouseMove(x, y, active);
}

void CBackground3D::OnRButtonDown(float x, float y)
{
  if (GetParent()) GetParent()->OnRButtonDown(x, y);
}

void CBackground3D::OnRButtonUp(float x, float y)
{
  if (GetParent()) GetParent()->OnRButtonUp(x, y);
}

void CBackground3D::OnRButtonClick(float x, float y)
{
  if (GetParent()) GetParent()->OnRButtonClick(x, y);
}

CompassMissionEditor::CompassMissionEditor(ControlsContainer *parent, int idc, ParamEntryPar cls)
  : Compass(parent, idc, cls)
{
  _isRotating = false;
}

void CompassMissionEditor::OnRButtonDown(float x, float y)
{
  _isRotating = true;
}

void CompassMissionEditor::OnSimulate(float deltaT, Vector3 &compassDir)
{
  if (IsRotating())      
  {
    float speedX = (GInput.GetAction(UABuldMoveRight) - GInput.GetAction(UABuldMoveLeft)) * deltaT*Input::MouseRangeFactor;
    float azimut = atan2(compassDir.X(), compassDir.Z());
    azimut += speedX;
    Matrix3 rotY(MRotationY, -azimut);
    compassDir = rotY.Direction();
  }
}

DisplayMissionEditor::DisplayMissionEditor(ControlsContainer *parent, RString resource, bool multiplayer)
: DisplayMap(parent), _vars(false)
{
  _enableDisplay = false;
  _enableSimulation = false;
  _alwaysShow = true;
  _menuTypeActive = EMNone;
  _mouseCursor = CursorArrow;
  _dragging = false;
  _draggingRM = false;
  _selecting = false;
  _selectOne = false;
  _rotating = false;
  _raising = false;
  _rotatingOrRaisingOnMap = true;
  _movingEdObj = false;
  _dragPos = VZero;
  _dragStartedInAir = false;
  _pt1.x = 0; _pt1.y = 0;
  _pt2.x = 0; _pt2.y = 0;
  _lastObjectCreated = NULL;
  _simulationDisabled = false;
  _overlayType = NULL;
  _hasChanged = false;
  _ignoreChanges = false;
  _addOnLeftClick = false;
  _addOnLeftClickOn = false;
  _quickAddByKey = false;
  _quickAddObjectKey = 0;
  _multiplayer = multiplayer;
  _multiplayerPreview = false;
  _mission = RString();
  _realTime = false;
  _showCommandMenu = false;
  _allow3DMode = true;
  _allow3DLinking = true;
  _exitOnMapKey = true;
  _allowFileOperations = true;
  _show3DIcons = true;
  _reShowLegend = false;
  _interfaceHidden = false;
  GVBSVisuals.interfaceHidden = _interfaceHidden;
  _objMouseOver = NULL;
  _treeFilter = ETFNone;

  // remainder of DisplayMap constructor
  _compassEditor = NULL;

  Load(resource);
  ParamEntryVal cls = Pars >> resource;

  _saveParams = true;
  ConstParamEntryPtr entry = cls.FindEntry("saveParams");
  if (entry) _saveParams = *entry;
  
  ParamEntryVal wrapper = Pars >> "CfgEditCamera";
  _nudgeSpeed = wrapper.ReadValue("nudgeSpeedModifier",0.05);

  _xboxStyle = false;
  entry = cls.FindEntry("xboxStyle");
  if (entry) _xboxStyle = *entry;
  
  _updatePlanAsked = false;

  // FIX
  AdjustMapVisibleRect();

  Init(GWorld->GetBriefing());
  ResetHUD(); 
  //-!
}

#ifndef _XBOX
void DisplayMissionEditor::SetEditorEventHandler(RString name, RString value)
{
  EditorEvent event = GetEnumValue<EditorEvent>((const char *)name);
  if (event != INT_MIN)
  {
    _eventHandlers[event].Clear();
    if (value.GetLength() > 0) _eventHandlers[event].Add(value);
  }
}

void DisplayMissionEditor::OnEditorEvent(EditorEvent event)
{
  EventHandlersUI &handlers = _eventHandlers[event];
  if (handlers.IsEmpty()) return;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(CreateGameControl(_map));
  handlers.Process(value);
}

void DisplayMissionEditor::OnEditorEvent(EditorEvent event, bool par1)
{
  EventHandlersUI &handlers = _eventHandlers[event];
  if (handlers.IsEmpty()) return;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(CreateGameControl(_map));
  arguments.Add(par1); 
  handlers.Process(value);
}

void DisplayMissionEditor::OnEditorEvent(EditorEvent event, const Object *par1, const Object *par2)
{
  EventHandlersUI &handlers = _eventHandlers[event];
  if (handlers.IsEmpty()) return;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(CreateGameControl(_map));
  if (par1)
    arguments.Add(GameValueExt(unconst_cast(par1),GameValExtObject));        
  else
    arguments.Add(GameValueExt((Object *)NULL)); 
  if (par2)
    arguments.Add(GameValueExt(unconst_cast(par2),GameValExtObject));        
  else
    arguments.Add(GameValueExt((Object *)NULL));  
  handlers.Process(value);
}

void DisplayMissionEditor::OnEditorEvent(EditorEvent event, RString par1, const Object *par2)
{
  EventHandlersUI &handlers = _eventHandlers[event];
  if (handlers.IsEmpty()) return;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(CreateGameControl(_map));
  arguments.Add(par1);
  if (par2)
    arguments.Add(GameValueExt(unconst_cast(par2),GameValExtObject));        
  else
    arguments.Add(GameValueExt((Object *)NULL));  
  handlers.Process(value);
}

void DisplayMissionEditor::OnEditorEvent(EditorEvent event, RString par1, RString par2, bool par3)
{
  EventHandlersUI &handlers = _eventHandlers[event];
  if (handlers.IsEmpty()) return;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(CreateGameControl(_map));
  arguments.Add(par1);
  arguments.Add(par2);
  arguments.Add(par3);
  handlers.Process(value);
}

void DisplayMissionEditor::OnEditorEvent(EditorEvent event, RString par1, ConstParamEntryPtr par2)
{
  EventHandlersUI &handlers = _eventHandlers[event];
  if (handlers.IsEmpty()) return;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(CreateGameControl(_map));
  arguments.Add(par1);
  if (par2)
  {
    GameConfigType result;
    result.entry = par2;
    result.path.Realloc(1);
    result.path.Resize(1);
    result.path[0] = par2->GetName();
    arguments.Add(GameValueExt(result));        
    handlers.Process(value);
  }
}
#endif

void DisplayMissionEditor::SetMode(EditorMode mode, bool overrideAllow3D)
{
  if (_multiplayerPreview)
    return;

  if (_mode == EMPreview)
  {
    // cancel preview mode
    GWorld->DestroyMap(IDC_OK);
    GWorld->DestroyUserDialog();
    GChatList.Clear();
    GChatList.ScriptEnable(true);
    GChatList.Load(Pars >> "RscChatListDefault");
    GetNetworkManager().SetPendingInvitationPos("RscPendingInvitation");
    SetCursor("Arrow");
    CameraRestart();
  }

  // ensure there is a playable unit
  if (GWorld->PlayerOn() == NULL && mode == EMPreview)
  {
    GWorld->CreateWarningMessage(LocalizeString("STR_EDITOR_ERROR_NO_PLAYABLE_PREVIEW"));
    return;
  }

  // preview in MP exits the editor
  if (_multiplayer && mode == EMPreview)
  {
    _multiplayerPreview = true;
    if (_mission.GetLength() == 0)
    {
      // mission has not been saved, so show save mission dialog
      OnButtonClicked(IDC_EDITOR_SAVE);
      return;
    }
    // save latest version of mission and exit editor
    OnChildDestroyed(IDD_MISSION_SAVE, IDC_OK);
    return;
  }

  // update controls help
  UpdateControlsHelp();

  switch (mode)
  {
  case EMMap:
    if (_compassEditor) _compassEditor->TrackNorth(true);
    GVBSVisuals.editor3DVisible = false;
    _mouseCursor = CursorArrow;
    if (IsCameraOnVehicle() || _enableSimulation)
    {
      //SwitchToCamera();
      if (!_realTime) _enableSimulation = false;
    }
    // enable display in realtime mode
    _enableDisplay = false;
    ShowBackground3D(false);
    ShowMap(true);
    break;
  case EM3D:
    if (!_allow3DMode && !overrideAllow3D) return;
    if (_compassEditor) _compassEditor->TrackNorth(false);
    GVBSVisuals.editor3DVisible = true;
    _enableDisplay = true;
    if (IsCameraOnVehicle() || _enableSimulation)
    {
      //SwitchToCamera();
      if (!_realTime) _enableSimulation = false;
    }    
    ShowBackground3D(true);
    ShowMap(false);
    break;
  case EMPreview:
    SetCursor("Wait");
    DrawCursor();
    GVBSVisuals.editor3DVisible = false;
    // update position and azimuth arguments for attached objects
    SaveAttachedObjectPositions();
    // save and clear overlay  - TODO: ask to save first?
    if (_hasChanged && _ws.GetActiveOverlay()) 
    {
      EditorOverlay *overlay = _ws.GetActiveOverlay();
      if (overlay->IsTemplate())
        _ws.SaveOverlay(overlay);         
    }
    SetActiveTemplateOverlay(NULL);   
    ClearSelected();
    // enable simulation for all vehicles
    for (int i=0; i<_proxies.Size(); i++)
    {      
      Object *objLink = _proxies[i].object;
      Entity *veh = dyn_cast<Entity>(objLink);
      if (!veh) continue;      
      if (!_proxies[i].proxyVisibleInPreview)
      {
        if (_proxies[i].edObj) _proxies[i].edObj->FreeProxy();
        veh->SetDelete();
      }
      veh->EditorDisableSimulation(false);
#if _VBS3
      veh->OnMoved(); //disable sleep mode
#endif
    }
    // order objects by link dependancy
    _ws.OrderAllObjects();
#if _ENABLE_UNDO && 0
    RestartMission(); // because undo can screw up linking :(    
#endif
    if (_miniMap) _miniMap->SetEditorCamera(NULL);
    GWorld->SetCameraEffect
    (
      CreateCameraEffect(_camera, "terminate", CamEffectBack, true)
    );
    _camera->SetDelete();
    if (IsCameraOnVehicle())
    {
      SwitchToCamera();
    }
    _enableSimulation = true;
    _enableDisplay = true;
    SetCursor(NULL);
    ShowMap(false);
    // switch camera to player
    {
      Person *person = GWorld->PlayerOn();
      if (person)
      {
        EntityAIFull *vehicle = person->Brain()->GetVehicle();
        DoAssert(vehicle);
        GWorld->SwitchCameraTo(vehicle, CamInternal, true);
        GWorld->SetPlayerManual(true);
        if (GWorld->UI())
        {
          GWorld->UI()->ResetHUD();
          GWorld->UI()->ResetVehicle(vehicle);
        }
      }
    }
    // prepare the UI for preview
    GWorld->SetBriefing(_editorBriefing.GetBriefing());
    GWorld->CreateMainMap();
    AbstractUI *ui = GWorld->UI();
    if (ui) ui->ShowAll(true);
    GWorld->DestroyUserDialog();
    GChatList.Clear();
    GChatList.ScriptEnable(true);
    GChatList.Load(Pars >> "RscChatListMission");
    GetNetworkManager().SetPendingInvitationPos("RscPendingInvitationInGame");
    // need to manually process init messages
    GWorld->EvalPostponedEvents();
    GWorld->ProcessInitMessages();
    RunInitScript();
    GameValue MissionFinishInit(const GameState *state);
    MissionFinishInit(GWorld->GetGameState());
    break;
  }
  _mode = mode;
}

void DisplayMissionEditor::LoadMission()
{
  // clean up editor objects
  _ws.Clear();
  _editorBriefing.Clear();
  if (_briefing) _briefing->Init();
  _objMouseOver = NULL;

  // load mission
  RString filename = GetMissionDirectory() + RString("mission.biedi");
  if (QIFileFunctions::FileExists(filename))
  {
    _ws.Load(filename);
  }

  // load briefing
  Init();
  if (_briefing) _editorBriefing.Load(_briefing);

  Vector3 pos = VZero;
  bool lookAt = false;

  // collapse tree
  for (int i=0; i<_ws.NObjects(); i++)
  {
    EditorObject *obj = _ws.GetObject(i);
    if (!obj) continue;
    if (obj->GetParent().GetLength() > 0)
      obj->SetVisibleInTree(false);
    else
      if (!lookAt)
        if (obj->GetEvaluatedPosition(pos))
          lookAt = true;
  }
  UpdateObjectBrowser();

  RestartMission();

#if _ENABLE_UNDO
  _ws.SaveInstance(); // this is the first undo recording
#endif

  // set to correct mission version
  EditorObject *prefix = _ws.GetPrefixObject();
  prefix->RemoveArgument("VERSION");
  prefix->SetArgument("VERSION",MISSION_VERSION);

  // look at first unit automatically
  if (!EditorCameraActive())
    CameraRestart();
  if (lookAt)
  {
    _camera->LookAt(pos);
    if (_map) _map->CStaticMap::Center(pos, false);
    if (_miniMap) _miniMap->CStaticMap::Center(pos, false);    
  }

  if (_ws.NOverlays() > 0)
  {
#ifndef _XBOX
    ParamEntryVal type = Pars >> "CfgEditorOverlayTypes" >> "Layer";
    OnEditorEvent(MEEditOverlay, "Layer", &type);
#endif
  }
}

void DisplayMissionEditor::ClearMission()
{
  // save and clear overlay  - TODO: ask to save first?
  if (_hasChanged && _ws.GetActiveOverlay()) 
  {
    EditorOverlay *overlay = _ws.GetActiveOverlay();
    if (overlay->IsTemplate())
      _ws.SaveOverlay(overlay);
  }
  SetActiveTemplateOverlay(NULL);

  // clean up editor objects
  _ws.Clear();
  _editorBriefing.Clear();
  if (_briefing) _briefing->Init();
  _objMouseOver = NULL;

  _mission = RString();

#ifndef _XBOX
  OnEditorEvent(MEClearMission);
#endif

  RestartMission();

#if _ENABLE_UNDO
  _ws.SaveInstance(); // this is the first undo recording
#endif
}

void DisplayMissionEditor::RestartMission()
{
  SetCursor("Wait");
  DrawCursor();

  // save and clear overlay  - TODO: ask to save first?
  if (_hasChanged && _ws.GetActiveOverlay()) 
  {
    EditorOverlay *overlay = _ws.GetActiveOverlay();
    if (overlay->IsTemplate())
      _ws.SaveOverlay(overlay);         
  }
  SetActiveTemplateOverlay(NULL); 

  // clean up landscape
  _vars._vars.Clear();
  _originalPlayer = NULL;
  _playerUnit = NULL;
  _proxies.Clear();
  _hasChanged = false;
  _ws.ClearDrawLinks();  

  GWorld->SwitchLandscape(Glob.header.worldname);
  GWorld->AdjustSubdivision(GWorld->GetMode());

  // fill object type list
  _objectTypeList->RemoveAll();
  for (int i=0; i<_ws.NObjectTypes(); i++)
  {    
    const EditorObjectType *type = _ws.GetObjectType(i);
    int index = _objectTypeList->AddString(type->GetName() + type->GetShortcutKeyDesc());
    _objectTypeList->SetData(index, type->GetName());
  }
  if (_objectTypeList->Size() > 0)
    _objectTypeList->SetCurSel(0);

  // set up some local variables automatically
  // TODO: remove other _map variable definitions?
  _vars.VarLocal("_map");
  _vars.VarSet("_map", CreateGameControl(_map), true);

  // create camera (cursor)
  _camera = new MissionEditorCamera(this);
  if (_miniMap) _miniMap->SetEditorCamera(_camera);

  // create mission editor objects
  CreateObject(_ws.GetPrefixObject(),false,false,true);
  for (int i=0; i<_ws.NObjects(); i++)
    CreateObject(_ws.GetObject(i),false,false,true);
  CreateObject(_ws.GetPostfixObject(),false,false,true);

  // switch camera to cursor
  _originalPlayer = GWorld->PlayerOn();
  SwitchToCamera();

  UpdateObjectBrowser();

  // select editor mode
  SetMode(EMMap);

  // FIX: remove mission fade in effect
  GWorld->ClearCutEffects();

#ifndef _XBOX
  OnEditorEvent(MERestartMission);
#endif

  SetCursor("Arrow");
}

void DisplayMissionEditor::RecordAction(bool saveInstance)
{
  if (!_ignoreChanges)
  {
    _hasChanged = true;
#if _ENABLE_UNDO
    if (!_realTime && saveInstance) _ws.SaveInstance();
#endif
  }
}

ControlObject *DisplayMissionEditor::OnCreateObject(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_MAP_COMPASS:
    _compass = new CompassMissionEditor(this, idc, cls);
    _compassEditor = static_cast<CompassMissionEditor *>(_compass.GetRef());
    return _compass;
  }
  return DisplayMap::OnCreateObject(type, idc, cls);
}

Control *DisplayMissionEditor::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_EDITOR_FILE:
  case IDC_EDITOR_VIEW:
  case IDC_EDITOR_USER:
    {
      CEditorMenuButton *button = new CEditorMenuButton(this, idc, cls);
      return button;
    }
  case IDC_MAP:
    _map = new CStaticMapEditor(this, idc, cls, 0.001, 1.0, 0.1, &_ws, &_vars);
    if (_map)
    {
      _map->SetScale(-1);
      _map->Reset();
    }
    return _map;
  case IDC_MINI_MAP:
    _miniMap = new CStaticMapEditor(this, idc, cls, 0.001, 1.0, 0.1, &_ws, &_vars);
    if (_miniMap)
    {
      _miniMap->SetScale(-1);
      _miniMap->Reset();
      _miniMap->ShowCursorLines(false);
      _miniMap->DrawEditorObjects(false);
    }
    return _miniMap;    
  case IDC_EDITOR_OBJECTS:
    _objectBrowser = new CObjectBrowser(this, idc, cls);    
    return _objectBrowser;
  case IDC_EDITOR_BACKGROUND:
    _background3D = new CBackground3D(this, idc, cls);
    return _background3D;
  }

  switch (idc)
  {
  case IDC_EDITOR_ATTRIBUTES:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      _attributeBrowser = dynamic_cast<CListBox *>(ctrl);
    }
    break;
  case IDC_EDITOR_TYPE_LIST:
    {
      Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
      _objectTypeList = dynamic_cast<CListBox *>(ctrl);
    }
    break;
  case IDC_EDITOR_OBJECTS_FILTER:
    {
      // fill tree filter control
      Control *ctrl = DisplayMap::OnCreateCtrl(type, idc, cls);
      CListBoxContainer *list = GetListBoxContainer(ctrl);
      if (!list) return ctrl;
      
      int i;
      i = list->AddString(LocalizeString("STR_EDITOR_TREE_FILTER_NONE"));
      list->SetValue(i, ETFNone);
      i = list->AddString(LocalizeString("STR_EDITOR_TREE_FILTER_UNITS_VEH"));
      list->SetValue(i, ETFUnitsAndVehicles);
      i = list->AddString(LocalizeString("STR_EDITOR_TREE_FILTER_PLAYERS"));
      list->SetValue(i, ETFPlayers);
      i = list->AddString(LocalizeString("STR_EDITOR_TREE_FILTER_OBJECTS"));
      list->SetValue(i, ETFObjects);
      list->SetCurSel(0, false);

      return ctrl;
    }
    break;
  }
  return DisplayMap::OnCreateCtrl(type, idc, cls);
}

class DisplayInterruptEditor : public DisplayInterrupt
{
public:
  DisplayInterruptEditor(ControlsContainer *parent) : DisplayInterrupt(parent, /*TODO: pass array of Future<QFileTime> here?*/) {}
  void OnButtonClicked(int idc);
};

void DisplayInterruptEditor::OnButtonClicked(int idc)
{
  switch (idc)
  {
    case IDC_INT_ABORT:
      DisplayInterrupt::OnChildDestroyed(IDD_MSG_TERMINATE_SESSION, IDC_OK);
      return;
    case IDC_INT_RETRY:
      // Fixed, used IDD_MSG_RETRY_MISSION instead of restart. 
      // Previous engine only had restart idc.
      DisplayInterrupt::OnChildDestroyed(IDD_MSG_RETRY_MISSION, IDC_OK);
      return;
  }
  DisplayInterrupt::OnButtonClicked(idc);
}

void DisplayMissionEditor::OnButtonClicked(int idc)
{
  switch (idc)
  {
  case IDC_CANCEL:
    if (_mode == EM3D)
    {
      // interrupt display
      //CreateChild(new DisplayEditorInterrupt(this, "RscDisplayInterruptEditor3D"));
      SetMode(EMMap);
    }
    else if (_mode == EMPreview)
    {
      // interrupt display
      CreateChild(new DisplayInterruptEditor(this));
    }
    else
    {
      if (_hasChanged && _ws.GetActiveOverlay()) 
      {
        EditorOverlay *overlay = _ws.GetActiveOverlay();
        if (overlay->IsTemplate())
          AskForOverlaySave(IDD_MSG_EXIT_OVERLAY);
      }        
      OnChildDestroyed(IDD_MSG_EXIT_OVERLAY, IDC_CANCEL); // skip straight to exit dialog
      return;
    }
    break;
  case IDC_EDITOR_TREE_EXPAND_ALL:
    {
      if (!_objectBrowser) break;  
      int n = _objectBrowser->GetRoot()->children.Size();
      for (int i=0; i<n; i++)
        _objectBrowser->GetRoot()->children[i]->ExpandTree(true);
      OnTreeItemExpanded(_objectBrowser, _objectBrowser->GetRoot(), true);
    }
    break;
  case IDC_EDITOR_TREE_COLLAPSE_ALL:
    {
      if (!_objectBrowser) break;
      int n = _objectBrowser->GetRoot()->children.Size();
      for (int i=0; i<n; i++)
        _objectBrowser->GetRoot()->children[i]->ExpandTree(false);
      OnTreeItemExpanded(_objectBrowser, _objectBrowser->GetRoot(), false);
    }
    break;
  case IDC_EDITOR_LOAD:
    // save and clear overlay  - TODO: ask to save first?
    if (_hasChanged && _ws.GetActiveOverlay()) 
    {
      EditorOverlay *overlay = _ws.GetActiveOverlay();
      if (overlay->IsTemplate())
        _ws.SaveOverlay(overlay);         
    }
    SetActiveTemplateOverlay(NULL); 
    //SetMode(EMMap);
    CreateChild(new DisplayMissionLoad(this,_multiplayer));
    break;
  case IDC_EDITOR_SAVE:
    {
      // ensure there is a playable unit
      if (GWorld->PlayerOn() == NULL)
      {
        GWorld->CreateWarningMessage(LocalizeString("STR_EDITOR_ERROR_NO_PLAYABLE_SAVE"));
        break;
      }

      // save and clear overlay  - TODO: ask to save first?
      if (_hasChanged && _ws.GetActiveOverlay()) 
      {
        EditorOverlay *overlay = _ws.GetActiveOverlay();
        if (overlay->IsTemplate())
          _ws.SaveOverlay(overlay);         
      }
      SetActiveTemplateOverlay(NULL); 
      //SetMode(EMMap);

      // find mission title and description
      RString title, description;
      EditorObject *prefix = _ws.GetPrefixObject();
      if (prefix)
        for (int i=0; i<prefix->NArguments(); i++)
        {
          const EditorArgument &arg = prefix->GetArgument(i);
          if (stricmp(arg.name, "TITLE") == 0)
            title = arg.value;
          if (stricmp(arg.name, "DESCRIPTION") == 0)
            description = arg.value;
        }

      CreateChild(new DisplayMissionSave(this, title, description, _realTime));
    }
    break;
  case IDC_EDITOR_RESTART:
    RestartMission();
    break;
  case IDC_EDITOR_CLEAR:
    {
      AutoArray<KeyHint> hints;
      hints.Realloc(2);
      hints.Resize(2);
      hints[0].key = INPUT_DEVICE_XINPUT + XBOX_A;
      hints[0].hint = LocalizeString(IDS_DISP_XBOX_HINT_YES);
      hints[1].key = INPUT_DEVICE_XINPUT + XBOX_B;
      hints[1].hint = LocalizeString(IDS_DISP_XBOX_HINT_NO);        
      CreateMsgBox
      (
        MB_BUTTON_OK | MB_BUTTON_CANCEL,
        LocalizeString(IDS_SURE),
        IDD_MSG_CLEAR_MISSION //, &hints
      );
    }
    break;
  case IDC_EDITOR_PREVIEW:
    if (_mode == EMPreview)
      RestartMission();
    else
      SetMode(EMPreview);
    break;
  case IDC_EDITOR_MAP:
    if (_mode == EMMap)
      SetMode(EM3D);
    else
      SetMode(EMMap);
    break;
  case IDC_EDITOR_FILE:
    {
      Control *ctrl = dynamic_cast<Control *>(GetCtrl(IDC_EDITOR_FILE));
      if (!ctrl) break;
      ParamEntryPar cls = Pars >> "RscDisplayMissionEditor" >> "Menu_File";
      CreateContextMenu(
        ctrl->X(), ctrl->Y() + ctrl->H(), IDC_EDITOR_MENU_FILE,
        cls, CCMContextEditor(RString(), NULL, VZero, EMFile));
    }
    break;
  case IDC_EDITOR_VIEW:
    {
      Control *ctrl = dynamic_cast<Control *>(GetCtrl(IDC_EDITOR_VIEW));
      if (!ctrl) break;
      ParamEntryPar cls = Pars >> "RscDisplayMissionEditor" >> "Menu_View";
      CreateContextMenu(
        ctrl->X(), ctrl->Y() + ctrl->H(), IDC_EDITOR_MENU_VIEW,
        cls, CCMContextEditor(RString(), NULL, VZero, EMView));
    }
    break;
  }
  DisplayMap::OnButtonClicked(idc);
}

void DisplayMissionEditor::SaveAttachedObjectPositions()
{
  for (int i=0; i<_ws.NObjects(); i++)
  {
    EditorObject *obj = _ws.GetObject(i);
    Object *proxy = obj->GetProxyObject();
    if (proxy && proxy->GetAttachedTo()) 
    {
      Vector3 pos = proxy->Position();
      obj->UpdateArgument("POSITION", Format("[%.5f, %.5f, %.5f]", pos.X(), pos.Z(), pos.Y()), true);
      obj->UpdateAzimuth();
    }
  }
}

void DisplayMissionEditor::OnChildDestroyed(int idd, int exit)
{
  switch (idd)
  {
  case IDD_EDIT_OBJECT:
    {
      DisplayEditObject *child = static_cast<DisplayEditObject *>(_child.GetRef());
      if (_filter != child->GetFilter())
      {
        _filter = child->GetFilter();
        
        // refresh quick add
        IControl *ctrl = GetCtrl(IDC_EDITOR_ADDOBJ_TYPES);
        CListBoxContainer *lb = GetListBoxContainer(ctrl);
        if (lb) OnLBSelChanged(ctrl, lb->GetCurSel());
      }
      if (exit == IDC_OK)
      {
        _mouseCursor = CursorArrow;
        EditorObject *obj = child->GetObject();
        if (obj)
        {
          if (child->IsUpdate())
          {
            UpdateObject(obj);
            UpdateAttributeBrowser();
            RecordAction();

            // take a copy of the new object, after it is updated
            if (obj->GetType()->SaveTemplateOnUpdate())
            {
              _lastObjectCreated = new EditorObject(*obj);
            }
          }
          else
          {  
            // take a copy of the new object
            if (obj->GetType()->SaveTemplateOnCreate())
            {
              _lastObjectCreated = new EditorObject(*obj);
            }

            _ws.AddObject(obj);
            
            // add a place in the tree for the object
            UpdateObjectBrowser(obj);

            SetCursor("Wait");
            DrawCursor();

            CreateObject(obj,true);

            // process any change in tree display name
            UpdateObjectBrowser(obj);

            RecordAction();

            SetCursor("Arrow");          

            RString id = obj->GetArgument("VARIABLE_NAME");          

            // link to this new object from some other object?
            RString linkArg;
            EditorObject *linkObj = child->LinkToObject(linkArg);        
            if (linkObj)
              OnLinkEditted(linkObj, linkArg, id, -1, NULL);

            SelectObject(id);
          }        
        }
      }
      Display::OnChildDestroyed(idd, exit);
    }
    return;
  case IDD_EDIT_BRIEFING:
    if (exit == IDC_OK)
    {
      DisplayEditBriefing *child = static_cast<DisplayEditBriefing *>(_child.GetRef());
      _editorBriefing.SetPlan(child->GetPlan());
      _editorBriefing.SetNotes(child->GetNotes());
      _editorBriefing.SetObjectives(child->GetObjectives());

      if (!_briefing) break;
      _briefing->Init();
      _briefing->Parse(_editorBriefing.GetBriefing());

      // assign briefing
      _briefing->AddName(_briefing->FindSection("Main"), "__BRIEFING");
      _briefing->AddName(_briefing->FindSection("Plan"), "__PLAN");
      //UpdatePlan();
      
      // find bookmarks
      _briefing->AddBookmark("__PLAN");
      _briefing->AddBookmark("__BRIEFING");
      SwitchBriefingSection("__PLAN");
    }
    break;
  case IDD_MISSION_LOAD:
    if (exit == IDC_OK)
    {
      // clean up references to objects
      _vars._vars.Clear();
      _originalPlayer = NULL;
      _playerUnit = NULL;
      _proxies.Clear();
      _ws.ClearDrawLinks();

      DisplayMissionLoad *child = static_cast<DisplayMissionLoad *>(_child.GetRef());
      RString island = child->GetIsland();
      _mission = child->GetMission();
      SetBaseDirectory(true, RString());
      // all missions to be saved in MPMissions
      ::SetMission(island, _mission, MPMissionsDir);
      LoadMission();
    }
    break;
  case IDD_MISSION_SAVE:
    if (exit == IDC_OK)
    {      
      DisplayMissionSave *child = dynamic_cast<DisplayMissionSave *>(_child.GetRef());
      if (child) 
      {
        _mission = EncodeFileName(child->GetMission());

        // save mission title and description to prefix
        EditorObject *prefix = _ws.GetPrefixObject();
        if (prefix)
        {     
          prefix->RemoveArgument("TITLE");
          prefix->SetArgument("TITLE",child->GetTitle());

          prefix->RemoveArgument("DESCRIPTION");
          prefix->SetArgument("DESCRIPTION",child->GetDescription());
        }
      }

      RString island = Glob.header.worldname;
     
      // save and clear overlay - TODO: ask to save first?
      if (_hasChanged && _ws.GetActiveOverlay()) 
      {
        EditorOverlay *overlay = _ws.GetActiveOverlay();
        if (overlay->IsTemplate())
          _ws.SaveOverlay(overlay);         
      }
      SetActiveTemplateOverlay(NULL); 

      // update position and azimuth arguments for attached objects
      SaveAttachedObjectPositions();

      // clean up all possible references to config before SetMission is called
      _vars._vars.Clear();
      _originalPlayer = NULL;
      _playerUnit = NULL;
      _proxies.Clear();
      _ws.ClearDrawLinks();
      GWorld->SwitchLandscape(island);
      GWorld->AdjustSubdivision(GWorld->GetMode());

      SetBaseDirectory(true, RString());
      // all missions to be saved in MPMissions
      ::SetMission(island, _mission, MPMissionsDir);
      // order objects by link dependancy
      _ws.OrderAllObjects();

      // save mission  
      RString directory = GetMissionDirectory();
      RString path = directory + RString("mission.biedi");
      void CreatePath(RString path);
      CreatePath(path);
      _ws.Save(path);
      _ws.WriteCreateScript(directory + RString("mission.sqf"));
      _editorBriefing.WriteHTML(directory);
      SaveSQM(directory);

#if _VBS3
      int i = directory.GetLength() - 1;
      if (i>=0 && directory[i] == '\\') 
        directory = directory.Substring(0, i); // remove trailing '\'
#endif

      if (child)
      {
        switch (child->GetPlacement())
        {
        case MPSingleplayer:
          {
            RString bankName =
              RString("Missions\\") +
              _mission + RString(".") +
              RString(Glob.header.worldname) + RString(".pbo");
            DoVerify(QIFileFunctions::CleanUpFile(bankName));
            FileBankManager mgr;
            mgr.Create(bankName, directory);
          }
          break;
        case MPMultiplayer:
          {
            RString bankName =
              RString("MPMissions\\") +
              _mission + RString(".") +
              RString(Glob.header.worldname) + RString(".pbo");
            DoVerify(QIFileFunctions::CleanUpFile(bankName));
            FileBankManager mgr;
            mgr.Create(bankName, directory);
          }
          break;
        }
      }
      // restart is needed (world was cleaned up)      
      EditorMode oldMode = _mode;      
      RestartMission();
      SetMode(oldMode);

      if (_multiplayer && _multiplayerPreview)
      {
        _multiplayerPreview = false;
        Exit(IDC_OK);
        return;
      }      
    }
    else
    {
      _multiplayerPreview = false;
    }
    break;
  case IDD_OVERLAY_CREATE:
    if (exit == IDC_OK)
    {
      DisplayOverlayCreate *child = static_cast<DisplayOverlayCreate *>(_child.GetRef());
      RString name = child->GetName();
      EditorOverlay *overlay = _ws.CreateOverlay(name, true, _overlayType->GetName());
      SetActiveTemplateOverlay(overlay);
      OnLBSelChanged(GetCtrl(IDC_EDITOR_ADDOBJ_TYPES),0);
    }
    break;
  case IDD_OVERLAY_LOAD:
  case IDD_LAYER_LOAD:
    if (exit == IDC_OK)
    {
      DisplayOverlayLoad *child = static_cast<DisplayOverlayLoad *>(_child.GetRef());
      RString name = child->GetOverlay();
      if (name.GetLength() == 0) break;

      bool isTemplate = true;
      
      ConstParamEntryPtr templatePtr = _overlayType->FindEntry("template");
      if (templatePtr)
      {
        float value = *templatePtr;
        isTemplate = toInt(value) == 0 ? false : true;
      }

      EditorOverlay *overlay = _ws.LoadOverlay(name, isTemplate, _overlayType->GetName());
      SetActiveTemplateOverlay(overlay);    
      OnLBSelChanged(GetCtrl(IDC_EDITOR_ADDOBJ_TYPES),0);
    }
    break;
  case IDD_MSG_CLEAR_MISSION:
    if (exit == IDC_OK)
      ClearMission();
    break;
  case IDD_MSG_EXITTEMPLATE:
    if (exit == IDC_OK)
    {
      // save and clear overlay - TODO: ask to save first?
      if (_hasChanged && _ws.GetActiveOverlay()) 
      {
        EditorOverlay *overlay = _ws.GetActiveOverlay();
        if (overlay->IsTemplate())
          _ws.SaveOverlay(overlay);         
      }
      SetActiveTemplateOverlay(NULL);  

      Exit(IDC_CANCEL);
    }
    break;
  case IDD_MSG_EXIT_OVERLAY:
    {
      if (exit == IDC_OK)
      {
        _ws.SaveOverlay(_ws.GetActiveOverlay());
      }
      Assert(_mode == EMMap);
      AutoArray<KeyHint> hints;
      hints.Realloc(2);
      hints.Resize(2);
      hints[0].key = INPUT_DEVICE_XINPUT + XBOX_A;
      hints[0].hint = LocalizeString(IDS_DISP_XBOX_HINT_YES);
      hints[1].key = INPUT_DEVICE_XINPUT + XBOX_B;
      hints[1].hint = LocalizeString(IDS_DISP_XBOX_HINT_NO);        
      CreateMsgBox
      (
        MB_BUTTON_OK | MB_BUTTON_CANCEL,
        LocalizeString(IDS_SURE),
        IDD_MSG_EXITTEMPLATE //, &hints
      );
    }
    break;  
  case IDD_MSG_CREATE_OVERLAY:
    {      
      EditorOverlay *overlay = _ws.GetActiveOverlay();
      if (overlay && overlay->IsTemplate())
      {
        if (exit == IDC_OK)
          _ws.SaveOverlay(overlay);
        SetActiveTemplateOverlay(NULL);
      }
      SetMode(EMMap);
      if (_overlayType) 
      {
        CreateChild(new DisplayOverlayCreate(this,_overlayType));  
      }
      return;
    }
    break;
  case IDD_MSG_LOAD_OVERLAY:
    {      
      EditorOverlay *overlay = _ws.GetActiveOverlay();
      if (overlay && overlay->IsTemplate())
      {
        if (exit == IDC_OK)
          _ws.SaveOverlay(overlay);
        SetActiveTemplateOverlay(NULL);
      }
      //SetMode(EMMap);
      if (_overlayType) CreateChild(new DisplayOverlayLoad(this,_overlayType)); 
      return;
    }
    break;
  case IDD_MSG_COMMIT_OVERLAY:
    {
      if (exit == IDC_OK)
      {
        _ws.SaveOverlay(_ws.GetActiveOverlay());  // save automatically
        CommitActiveTemplateOverlay();
        GWorld->CreateWarningMessage(LocalizeString("STR_EDITOR_PROMPT_OVERLAY_COMMITTED")); 
      }
    }
    break;
  case IDD_MSG_CLOSE_OVERLAY:
    {
      if (exit == IDC_OK)
        _ws.SaveOverlay(_ws.GetActiveOverlay());
      SetActiveTemplateOverlay(NULL);
    }
    break;
  case IDD_MSG_CLEAR_OVERLAY:
    {
      if (exit == IDC_OK)
      {
        ClearSelected();
        EditorOverlay *currentOverlay = _ws.GetActiveOverlay();

        // delete objects that are in currentOverlay
        for (int i=0; i<_ws.NObjects(); i++)
        {
          EditorObject *obj = _ws.GetObject(i);
          if (obj->GetParentOverlay() && obj->GetParentOverlay() == currentOverlay)
          {
            obj->SetParentOverlay(NULL);
            DeleteObject(obj);
            i--;
          }    
        }
        GWorld->CreateWarningMessage(LocalizeString("STR_EDITOR_PROMPT_OVERLAY_CLEARED")); 
      }
    }
    break;
  case IDD_INTERRUPT:
    switch (exit)
    {
    case IDC_INT_ABORT:
      OnChildDestroyed(idd, IDC_INT_EDIT_MAP);
      return;
    case IDC_INT_RETRY:
      {
        EditorMode oldMode = _mode;
        RestartMission();
        SetMode(oldMode);
      }
      break;
    case IDC_INT_EDIT_MAP:
      RestartMission();
      SetMode(EMMap);
      break;
    case IDC_INT_EDIT_3D:
      RestartMission(); 
      SetMode(EM3D);
      break;
    case IDC_INT_EDIT_PREVIEW:
      SetMode(EMPreview);
      break;
    }
    break;
  }
  Display::OnChildDestroyed(idd, exit);
}

// hack to enable scripted missions to work in MP
void DisplayMissionEditor::SaveSQM(RString directory)
{
  // create mission.sqm (used in MP)
  _templateMission.Clear();
  int id = 0;
  for (int i=0; i<_ws.NObjects(); i++)
  {
    const EditorObject *obj = _ws.GetObject(i);
    const EditorObjectType *type = obj->GetType();
    if (stricmp(type->GetName(),"unit") == 0)
    {
      ArcadeUnitInfo info;  

      // player
      bool isPlayer = stricmp(obj->GetArgument("PLAYER"),"true") == 0;
      bool isPlayable = stricmp(obj->GetArgument("PLAYABLE"),"true") == 0;
      if (isPlayer) info.player = APPlayerCommander;
      else if (isPlayable) info.player = APPlayableCDG;          

      // position
      Vector3 pos; 
      if (obj->GetEvaluatedPosition(pos, true))
      {
        info.position = info.positionASL = pos; // very important - store ASL position
      }

      RString type = obj->GetArgument("TYPE");
      ParamEntryVal entry = Pars >> "CfgVehicles" >> type;      

      // user-defined unit? if so, save base class
      if (entry.IsClass())
      {
        bool isEditor = entry.ReadValue("isEditorVehicle",false);
        if (isEditor)
        {
          ConstParamEntryPtr base = entry.GetPointer()->FindBase();
          if (base)
            type = base->GetName();
        }
      }

      // vehicle
      info.vehicle = type;

      // side      
      if (entry.IsClass())
      {
        int vehSide = entry >> "side";
        info.side = (TargetSide)vehSide;
      }

      // MP description == name for now
      info.description = obj->GetArgument("DESCRIPTION");

      // special
      info.special = ASpCanCollide;
      RString special = obj->GetArgument("SPECIAL");    
      if (!stricmp(special,"FORM")) 
        info.special = ASpForm;

      // name - remove leading underscore
      RString edObjName = obj->GetArgument("VARIABLE_NAME");
      info.name = RString(edObjName + 1,edObjName.GetLength() - 1);

#if _VBS3 // autoassign fix for new missions
      // actual name of unit (used with autoassign)
      RString actualName = obj->GetArgument("NAME");
      if (actualName.GetLength() > 0)
        info.name = actualName;
#endif

      info.id = id++;

      int ig = -1;

      // try and find existing group
      RString parent = obj->GetArgument("LEADER");          
      if (parent.GetLength() > 0)
      {
        const EditorObject *parentObj = _ws.FindObject(parent);
        parent = RString(parent + 1,parent.GetLength() - 1);        
        if (parentObj) 
        {
          actualName = parentObj->GetArgument("NAME");
          if (actualName.GetLength() > 0)
            parent = actualName;
        }
        for (int i=0; i<_templateMission.groups.Size(); i++)
        {
          AutoArray<ArcadeUnitInfo> &units = _templateMission.groups[i].units;
          for (int j=0; j<units.Size(); j++)
          {
            if (stricmp(units[j].name,parent) == 0)
              ig = i;
          }
        }
      }

      if (ig == -1)
      {
        info.leader = true;
        ig = _templateMission.groups.Add();
        _templateMission.groups[ig].side = info.side;            
      }

      _templateMission.groups[ig].units.Add(info);                 
    }
  }

  // add addons
  EditorObject *prefix = _ws.GetPrefixObject();
  if (prefix)
  {
    for (int i=0; i<prefix->NArguments(); i++)
    {
      const EditorArgument &arg = prefix->GetArgument(i);
      if (stricmp(arg.name, "ADDON") == 0)
      {
        _templateMission.addOns.AddUnique(arg.value);
        _templateMission.addOnsAuto.AddUnique(arg.value);
      }
      if (stricmp(arg.name, "TITLE") == 0)
        _templateMission.intel.briefingName = arg.value;
      if (stricmp(arg.name, "DESCRIPTION") == 0) 
        _templateMission.intel.briefingDescription = arg.value;
    }
  }

  // save mission.sqm
  ParamArchiveSave ar(MissionsVersion);
	ATSParams params;
  params.avoidScanAddons = true;
  params.avoidCheckSync = true;
	ar.SetParams(&params);
  LSError ok = ar.Serialize("Mission", _templateMission, 1);
  if (ok == LSOK) ar.Serialize("Intro", ArcadeTemplate(), 1);
  if (ok == LSOK) ar.Serialize("OutroWin", ArcadeTemplate(), 1);
  if (ok == LSOK) ar.Serialize("OutroLoose", ArcadeTemplate(), 1);
  if (ok == LSOK) ar.Save(directory + RString("mission.sqm"));

#if _VBS3
  DoAssert(ok == LSOK);
#endif
}

bool DisplayMissionEditor::OnUnregisteredAddonUsed(RString addon)
{
  EditorObject *prefix = _ws.GetPrefixObject();
  Assert(prefix);
  for (int i=0; i<prefix->NArguments(); i++)
  {
    const EditorArgument &arg = prefix->GetArgument(i);
    if (stricmp(arg.value,addon) == 0) return false;
  }
  prefix->SetArgument("ADDON", addon);
  UpdateAttributeBrowser();
  return true;
}

class SetAllObjectVisibility
{
protected:
  EditorWorkspace *_ws;
public:
  SetAllObjectVisibility(EditorWorkspace *ws){_ws = ws;}
  bool operator () (const CTreeItem *item) const
  {
    EditorObject *obj = _ws->FindObject(item->data);
    if (!obj || !item) return false;
    if (item->level > 1)
      obj->SetVisibleInTree(item->IsExpanded());
    if (item->parent)
      if (item->parent->IsExpanded())
        obj->SetVisibleInTree(true);  
    return false;
  }
};

void DisplayMissionEditor::OnTreeMouseMove(int idc, CTreeItem *sel)
{
  EditorObject *oldOver = _objMouseOver;
  if (sel)
    _objMouseOver = _ws.FindObject(sel->data);
  if (oldOver != _objMouseOver)
    ProcessMouseOverEvent();
}

void DisplayMissionEditor::OnTreeMouseHold(int idc, CTreeItem *sel)
{
  OnTreeMouseMove(idc, sel);
}

void DisplayMissionEditor::OnTreeMouseExit(int idc, CTreeItem *sel)
{
  OnTreeMouseMove(idc, sel);
}

// update visibility of icons when tree is expanded or collapsed
void DisplayMissionEditor::OnTreeItemExpanded(IControl *ctrl, CTreeItem *item, bool expanded)
{
  if (ctrl->IDC() == IDC_EDITOR_OBJECTS)
  {
    SetAllObjectVisibility func(&_ws);
    item->ForEachItem(func);
  }
}

void DisplayMissionEditor::OnTreeDblClick(int idc, CTreeItem *sel)
{
  // on double click center the item in the map and camera
  if (sel)
  {
    EditorObject *obj = _ws.FindObject(sel->data);
    if (obj)
    {
      Vector3 pos = obj->GetEvaluatedPos();
      if (_map) _map->CStaticMap::Center(pos, false);
      if (_miniMap) _miniMap->CStaticMap::Center(pos, false);        

      if (EditorCameraActive())
        SetCameraPosition(sel->data,_camera->IsLocked());
    }
  }

  /*
  if (sel)
    ShowEditObject(sel->data);
  */
}

void DisplayMissionEditor::OnTreeLButtonDown(int idc, CTreeItem *sel)
{
  if (sel)
  {
    EditorObject *selected = _ws.FindObject(sel->data);
    if (selected)
    {
      if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
        ClearSelected();
      _ws.SetSelected(selected,true);
#ifndef _XBOX
      OnEditorEvent(MESelectObject, sel->data, selected->GetProxyObject());
#endif
      DoSelectObjectScript(selected,true);
    }
    sel->Expand(true);
  }
}

void DisplayMissionEditor::OnTreeSelChanged(IControl *ctrl)
{
  if (ctrl->IDC() == IDC_EDITOR_OBJECTS)
    UpdateAttributeBrowser();
  if (_objectBrowser->GetSelected())
    SetLastSelectedInTree(_objectBrowser->GetSelected()->data);
}

bool DisplayMissionEditor::OnKeyDown(int dikCode)
{
   IControl *focused = GetFocused();
   if (focused && focused->GetType() == CT_EDIT)
     return Display::OnKeyDown(dikCode);
   
#if _VBS3 //don't process any keys in preview mode!
  if(_mode == EMPreview)
  {
    if(dikCode == DIK_ESCAPE)
      return Display::OnKeyDown(dikCode);
    else
      return false;
  }
#endif

  switch (dikCode)
  {
  case DIK_TAB:
    return true; // disable tab switching (causes problems with drop-down menus)
  case DIK_L: // lock camera
    if (EditorCameraActive() && _realTime)
    {
      if (_camera->IsLocked())
      {
        _camera->UnLock();
        return true;
      }
      Object *lockTo = _objMouseOver ? _objMouseOver->GetProxyObject() : NULL;
      if (!lockTo)
      {
        // find nearest selected object to camera
        float minDist2 = FLT_MAX;
        for (int i=0; i<_ws.GetSelected().Size(); i++)
        {
          EditorObject *obj = _ws.GetSelected()[i];
          const Object *proxy = obj->GetProxyObject();
          if (proxy)
          {
            float dist2 = _camera->Position().Distance2(proxy->Position());
            if (dist2 < minDist2)
            {
              minDist2 = dist2;
              lockTo = unconst_cast(proxy);
            }
          }
        }     
      }
      _camera->LockTo(lockTo);
    }
    return true;
  case DIK_H:
    OnButtonClicked(IDC_EDITOR_PREVIEW);
    return true;
  case DIK_M:
    OnButtonClicked(IDC_EDITOR_MAP);
    return true;
  case DIK_ESCAPE:
    {
      if (_linkObject) 
      {
#ifndef _XBOX
        OnEditorEvent(MELinkFromObject,_linkObject->GetArgument("VARIABLE_NAME"),_linkArgument,false);
#endif
        _linkObject = NULL;
        return true;
      }
      if (_map && _map->GetLinkObject())
      {
        _map->SetLinkObject(NULL);
        return true;
      }
      if (_ws.GetActiveOverlay()) 
      {
        EditorOverlay *overlay = _ws.GetActiveOverlay();
        if (overlay->IsTemplate())
        {
          if (_hasChanged)
            AskForOverlaySave(IDD_MSG_CLOSE_OVERLAY,LocalizeString("STR_EDITOR_PROMPT_OVERLAY_SAVE_CLOSE"));
          else
            OnChildDestroyed(IDD_MSG_CLOSE_OVERLAY, IDC_CANCEL);    // skip straight to create dialog 
          return true;
        }
      }    
      return Display::OnKeyDown(dikCode);
    }
  case DIK_DELETE:
    {
      if (_objMouseOver)
        if (_objMouseOver->GetScope() > EOSLinkFrom)
          DeleteObject(_objMouseOver);
    }
    return true;
  case DIK_C:
    if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
    {
      ClipboardCopy(false);
      return true;
    }
    break;
  case DIK_X:
    if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
    {
      ClipboardCopy(true);
      return true;
    }
    break;
  case DIK_V:
    if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
    {
      ClipboardPaste();
      //UpdateObjectBrowser();
      return true;
    }
    break;
#if _ENABLE_UNDO
  case DIK_Z:
  //case DIK_Y:
    if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
    {
      if (!_realTime)
      {
        ClearSelected();
        _ws.RevertToInstance(&_vars,dikCode == DIK_Z ? -1 : +1);
        RegisterProxies();
        UpdateObjectBrowser();
      }
      else
        WarningMessage(LocalizeString("STR_VBS2_EDITOR_WARNING_NO_UNDO"));
      return true;
    }
    break;
#endif
  case DIK_I:
    if (_mode == EM3D)
    {
      _interfaceHidden = !_interfaceHidden;
      GVBSVisuals.interfaceHidden = _interfaceHidden;
      if (_interfaceHidden)
      {
        for (int i=0; i<_proxies.Size(); i++)
        {      
          Object *objLink = _proxies[i].object;
          Entity *veh = dyn_cast<Entity>(objLink);
          if (!veh) continue;      
          if (!_proxies[i].proxyVisibleInPreview)
          {
            if (_proxies[i].edObj) _proxies[i].edObj->FreeProxy();
            veh->SetDelete();
            _proxies.Delete(i--); // note proxy will not be visible at all now
          }
        }
      }
      GlobalShowMessage(2000,LocalizeString("STR_VBS2_EDITOR_WARNING_INTERFACE_HIDDEN"));
      return true;
    }
    break;
  default:
    // check shortcut keys
    for (int i=0; i<_ws.NObjectTypes(); i++)
      if (dikCode == _ws.GetObjectType(i)->GetShortcutKey())
      {
        _objectTypeList->SetCurSel(i);
        break;
      }
    return Display::OnKeyDown(dikCode);    
  }
  return false;
}

bool DisplayMissionEditor::OnKeyUp(int dikCode)
{
  if (_mode != EMPreview) return Display::OnKeyUp(dikCode);
  return false;
}

void DisplayMissionEditor::RegisterProxies()
{
  // re-register proxies, will also delete objects as required
  /*
  int nProxies = _proxies.Size();
  int nObjs = _ws.NObjects();
  for (int i=0; i<nProxies; i++)
  {
    Object *proxy = _proxies[i].object;
    bool found = false;
    for (int i=0; i<nObjs; i++)
    {
      EditorObject *obj = _ws.GetObject(i);
      if (!obj) continue;
      if (proxy == obj->GetProxyObject())
      {
        found = true;
        break;
      }
    }
    if (!found)
    {
      Entity *veh = dyn_cast<Entity>(proxy);
      if (veh) veh->SetDelete();
    }
  }
  */

  _proxies.Clear();

  int nObjs = _ws.NObjects();
  for (int i=0; i<nObjs; i++)
  {
    EditorObject *obj = _ws.GetObject(i);
    if (!obj) continue;

    bool proxyExists = false;
    for (int i=0; i<_proxies.Size(); i++)
      if (_proxies[i].edObj == obj)
      {
        proxyExists = true;
        continue;
      }

    // register proxy
    if (!proxyExists)
    {    
      Object *proxy = obj->GetProxyObject();
      if (proxy)
        AddProxy(proxy,obj);
    }
  }
}

inline void ObjectCollision(CollisionBuffer &collision, Object *with, Vector3Par end, ObjIntersect type)
{
  float radius = 2.0f; //increase object pick tolerance
  GLandscape->ObjectCollision(collision,with,NULL,with->Position(),end,radius,type);
}

void DisplayMissionEditor::OnSimulate(EntityAI *vehicle)
{  
  bool IsAppPaused();
  if (IsAppPaused()) return;

  UITime now = Glob.uiTime;
  float deltaT = now - _lastSimulate;
  _lastSimulate = now;

  if (_mode != EMPreview && GInput.GetActionToDo(UANightVision))
  {
    GWorld->SetCamUseNVG(!GWorld->IsCamUseNVG());
    GWorld->OnCameraChanged();  // broken
  }

  if ( GWorld->GetTitleEffect() )
  {
    GWorld->GetTitleEffect()->Simulate(deltaT);
  }

  // main menu switching on mouse over
  if (_mode != EMPreview && _menuTypeActive > EMPopup)
  {
    IControl *ctrlFile = GetCtrl(IDC_EDITOR_FILE);
    IControl *ctrlView = GetCtrl(IDC_EDITOR_VIEW);

    if (_menuTypeActive != EMFile && ctrlFile && ctrlFile->IsMouseOver())
      OnButtonClicked(IDC_EDITOR_FILE);
    else if (_menuTypeActive != EMView && ctrlView && ctrlView->IsMouseOver())
      OnButtonClicked(IDC_EDITOR_VIEW);

    // user menus
    for (int i=0; i<_userMenus.Size(); i++)
    {
      Control *ctrl = _userMenus[i].ctrl;     
      if (ctrl && ctrl->IsMouseOver() && _menuTypeActive != EMUser + i)
      {
        CreateMenu(_userMenus[i].index);
        break;
      }
    }
  }
#if _VBS3
  // currently raising or rotating selected objects?
  if (_draggingRM && !_camera->IsMoving())
  {
    _rotating = GInput.keys[DIK_LSHIFT] > 0;
    _raising = GInput.keys[DIK_LALT] > 0;
  }
#endif

  // enable/disable simulation if moving objects in real time
//if (_simulationDisabled && now - _stoppedSimulate > 5)
//  EnableSimulationForSelected(true);

  if ((_movingEdObj && IsDragging()) || _rotating || _raising)
  {
//  _stoppedSimulate = Glob.uiTime;
    if (!_simulationDisabled)
      EnableSimulationForSelected(false);    
  }
  //-!

  // support for quick add
  if (GInput.keys[_quickAddObjectKey] > 0 && !_quickAddByKey)
  {
    _addOnLeftClickOn = true;
    _quickAddByKey = true;
    ProcessOnQuickAddObjectEventToggled(_addOnLeftClickOn);
  }
  else if (GInput.keys[_quickAddObjectKey] == 0 && _quickAddByKey)
  {
    _quickAddByKey = false;
    _addOnLeftClickOn = false;
    ProcessOnQuickAddObjectEventToggled(_addOnLeftClickOn);
  }
  _addOnLeftClick = _addOnLeftClickOn && !IsDragging() && !_rotating && !_raising;
  //-!

  // nudge selected objects
  bool nudge = false;
  if (GInput.keys[DIK_LALT])
  {
    if (GInput.GetAction(UAHeliUp) - GInput.GetAction(UAHeliDown) != 0)
    {
      _raising = true;
      nudge = true;
    }
    if (
      GInput.GetAction(UAMoveForward) - GInput.GetAction(UAMoveBack) != 0 ||
      GInput.GetAction(UATurnRight) - GInput.GetAction(UATurnLeft) != 0 ||
      GInput.keys[DIK_NUMPAD4] - GInput.keys[DIK_NUMPAD6] ||
      GInput.keys[DIK_NUMPAD2] - GInput.keys[DIK_NUMPAD8]
      )
    {
      _dragging = true;
      nudge = true;
    }      
    if (
      GInput.keys[DIK_C] - GInput.keys[DIK_X] != 0 ||
      GInput.keys[DIK_NUMPAD3] - GInput.keys[DIK_NUMPAD1] != 0
      )
    {
      _rotating = true;
      nudge = true;
    }
  };
  //-!

  if (IsDragging() || nudge)
    _movedProxies.Clear();

  // drag selected objects
  if (_dragging && (nudge || (_mode == EM3D && EditorCameraActive() && _movingEdObj)))
  {
    Vector3 curPos;
    Vector3 offset = VZero;
    bool valid = _camera->GetGroundIntercept(curPos, true, false, _dragStartedInAir);

    if (nudge)
    {
      float forward = ((GInput.GetAction(UAMoveForward)-GInput.GetAction(UAMoveBack)) + (GInput.keys[DIK_NUMPAD8] - GInput.keys[DIK_NUMPAD2])) * _camera->SpeedFwdBack() * _nudgeSpeed;
      float aside = ((GInput.GetAction(UATurnRight)-GInput.GetAction(UATurnLeft)) + (GInput.keys[DIK_NUMPAD6] - GInput.keys[DIK_NUMPAD4])) * _camera->SpeedLeftRight() * _nudgeSpeed;
      
      Vector3 dir = _camera->Direction();
      if (_ws.GetSelected().Size() == 1)
      {
        Object *proxy = _ws.GetSelected()[0]->GetProxyObject();
        if (proxy) dir = proxy->Direction();
      }

      Matrix3 speedOrient;
      speedOrient.SetUpAndDirection( VUp, dir );
      Vector3 speedWanted = speedOrient * Vector3(aside,0,forward);
      offset = speedWanted * _camera->MaxSpeed() * deltaT;
    }
    else
    {
      offset = curPos - _dragPos;
      if(valid && !_dragStartedInAir) offset[1] = 0;

      if(!valid && _dragStartedInAir)
      {
        float dist = 1.0f;

        if(_ws.GetSelected().Size())
        {
          Object *proxy = _ws.GetSelected()[0]->GetProxyObject();
          AspectSettings _aspect;
          GEngine->GetAspectSettings(_aspect);
          if(proxy) dist = proxy->Position().Distance(_camera->Position()) * _aspect.leftFOV;
        }
        offset *= dist;
      }
    }

    // offset selected objects
    for (int i=0; i<_ws.GetSelected().Size(); i++)
    {        
      EditorObject *obj = _ws.GetSelected()[i];
      OnObjectMoved(obj, offset);
    }

    if (!nudge) 
    {
      //if we started in AIR we don't switch to ground position
      if(valid != _dragStartedInAir) _dragPos = curPos;
    }
  }

  // rotate selected objects
  if (_rotating && (nudge || _rotatingOrRaisingOnMap || (_mode == EM3D && EditorCameraActive())))
  {
    RefArray<EditorObject> selected = _ws.GetSelected();
    if (selected.Size() > 0)
    {
      // find center
      Vector3 sum = VZero; int n = selected.Size(); Vector3 center;
      for (int i=0; i<selected.Size(); i++)
      {
        Object *proxy = selected[i]->GetProxyObject();
        if (proxy)
          if (proxy->GetAttachedTo()) {center = proxy->Position(); n--; continue;}
        Vector3 pos;
        if (selected[i]->GetEvaluatedPosition(pos))
          sum += pos;
      }      
      if (n > 0) center = (1.0f / n) * sum; 
      center[1] = 0;

      // rotate objects around center
      float speedX;
      if (nudge)
        speedX = ((GInput.keys[DIK_C] - GInput.keys[DIK_X]) + (GInput.keys[DIK_NUMPAD3] - GInput.keys[DIK_NUMPAD1])) * deltaT * (_camera->MaxSpeed() / MAX_CAMERA_SPEED);
      else
        speedX = (GInput.GetAction(UABuldMoveRight) - GInput.GetAction(UABuldMoveLeft)) * deltaT * Input::MouseRangeFactor * GWorld->GetAcceleratedTime();      
      for (int i=0; i<selected.Size(); i++)  
        RotateObject(selected[i], center, speedX);
    }
  }
  // raise/lower selected objects in 3D - TODO: make this faster?
  else if (_raising)
  {
    float speedY;
    if (nudge)
      speedY = (GInput.GetAction(UAHeliUp) - GInput.GetAction(UAHeliDown)) * deltaT * _camera->SpeedUpDown() * _camera->MaxSpeed() * _nudgeSpeed;
    else
      speedY = (GInput.GetAction(UABuldMoveForward) - GInput.GetAction(UABuldMoveBack)) * deltaT * Input::MouseRangeFactor * GWorld->GetAcceleratedTime();

    if ((nudge || !_rotatingOrRaisingOnMap) && _mode == EM3D && EditorCameraActive())    
    {
      // iterate through proxy positions instead of selected because objects may share proxies
      for (int i=0; i<_proxies.Size(); i++)
      {
        // is there another object sharing this proxy?
        bool found = false;
        for (int j=0; j<_proxies.Size(); j++)      
          if (_proxies[i].object == _proxies[j].object && _proxies[j].isMoved) {found = true; break;}
        if (found) continue;
        
        if (!_proxies[i].isMoved)
        {
          EditorObject *obj = _proxies[i].edObj;
          if (obj && obj->GetScope() >= EOSDrag && obj->IsSelected())
          {
            Object *proxy = _proxies[i].object;
            if (proxy)
            {
              float height = proxy->Position().Y();
              height += nudge ? speedY : speedY * 10;

              OnObjectMoved(obj, Vector3(0,height - proxy->Position().Y(),0));
              _proxies[i].isMoved = true;
            }
          }
        }
      }
      for (int i=0; i<_proxies.Size(); i++)
        _proxies[i].isMoved = false;
    }
    // raise/lower in 2D - let the script decide what to do
    else if (_rotatingOrRaisingOnMap)
    {
      for (int i=0; i<_ws.GetSelected().Size(); i++)
      {        
        EditorObject *obj = _ws.GetSelected()[i];
        if (obj->GetScope() >= EOSDrag)
          UpdateObject(obj,false,true,speedY);
      }      
    }
  }
  //-!

  // nudge selected objects
  if (nudge)
  {
    _raising = false;
    _dragging = false;
    _rotating = false;
    _movingEdObj = false;
  }
  //-!

  EditorObject *oldMouseOver = _objMouseOver;

  if (_mode != EMPreview)
  {
    if (CStaticMapEditor *map = MouseOverMap())
    {
      // 2D
      if (_dragging)
      {
        if (_mouseCursor != CursorDrag)
        {
          _mouseCursor = CursorDrag; SetCursor("Drag");
        }
      }
      else if (_rotating)
      {
        if (_mouseCursor != CursorRotate)
        {
          _mouseCursor = CursorRotate; SetCursor("Rotate");
        }
      }
      else if (map->IsSelecting())
      {
        if (_mouseCursor != CursorMove)
        {
          _mouseCursor = CursorMove;
          SetCursor("Move");
        }
      }
      else if (map->IsDraggingRM() && !GInput.keys[DIK_LSHIFT])
      {
        if (_mouseCursor != CursorScroll)
        {
          _mouseCursor = CursorScroll;
          SetCursor("Scroll");
        }
      }
      else
      {
        if (_mouseCursor != CursorTrack)
        {
          _mouseCursor = CursorTrack;
          SetCursor("Track");
        }
      }
    }
    else
    {
      // 3D
      if (_dragging && _movingEdObj)
      {
        if (_mouseCursor != CursorDrag)
        {
          _mouseCursor = CursorDrag; SetCursor("Drag");
        }
      }
      else
      if (_rotating)
      {
        if (_mouseCursor != CursorRotate)
        {
          _mouseCursor = CursorRotate; SetCursor("Rotate");
        }
      }
      else if (_raising)
      {
        if (_mouseCursor != CursorRaise)
        {
          _mouseCursor = CursorRaise; SetCursor("Raise");
        }
      }
      else if (_mouseCursor != CursorArrow)
      {
        _mouseCursor = CursorArrow; SetCursor("Arrow");
      }
    }
  }

  /*
      else
      {
        if (_mouseCursor != CursorArrow)
        {
          _mouseCursor = CursorArrow;
          SetCursor("Arrow");
        }
      }
  */

  switch (_mode)
  {
  case EMMap:
    {
      // rotate compass   
      if (_compassEditor) _compassEditor->OnSimulate(deltaT, _compassDir);
      Display::OnSimulate(vehicle);
    }
    break;
  case EM3D:
    {
      if (EditorCameraActive())
      {      
        if (_camera && !nudge) 
        {
          //VBS2 added aspect ratio
          AspectSettings _aspect;
          GEngine->GetAspectSettings(_aspect);

          // is the cursor over a control or over the camera?
//          float mouseX = 0.5 + GInput.cursorX * 0.5;
//          float mouseY = 0.5 + GInput.cursorY * 0.5;

          float mouseX = (1 + GInput.cursorX /_aspect.leftFOV)  * 0.5;
          float mouseY = (1 + GInput.cursorY)  * 0.5;

          bool cursorOverCamera = false;
          if (GetCtrl(mouseX, mouseY) == _background3D)
            cursorOverCamera = true;

          _objMouseOver = NULL;

          // determine currently selected proxy (only proxy position matters in 3D)
          if (_allow3DMode && cursorOverCamera && (!_dragging || _selectOne) && !_selecting)
          {            
#if _VBS2_MODIFY_WORLD
            Object *oldObject = _objStaticMouseOver.GetProxyObject();
            if (oldObject)
            {
              _objStaticMouseOver.SetProxyObject(NULL);
              _objStaticMouseOver.SetSelected(false);
              oldObject->_highlighted = 0;
            }
#endif

            // are we aiming directly at an object?
            Vector3 groundIntercept;
            bool found = _camera->GetGroundIntercept(groundIntercept, true, true);  // go through water
            
            Object *closestObj = NULL;
            if(!found)
            {
              groundIntercept = _camera->Position() + _camera->GetCursorDir() * 500.0f;
            }

            for (int i=0; i<3; i++)
            {
              CollisionBuffer collision;
              ObjectCollision(collision,_camera, groundIntercept,(ObjIntersect)i);

              float minT=1e10; int n = collision.Size();
              for (int j=0; j<n; j++)
              {
                CollisionInfo &info = collision[j];
                if (minT > info.under && info.object)
                {    
                  closestObj = info.object;
                  groundIntercept = closestObj->Position();
                  minT=info.under;
                  /*
                  GScene->DrawCollisionStar
                  (
                    info.pos,0.05,PackedColor(Color(0.25,0,0.25))
                  );
                  LogF("[p] %d: %.5f | %s",i,minT,closestObj->GetDebugName().Data());
                  */
                }
              }
            }

            if (closestObj)
            {
              //LogF("[p] %s is closest",closestObj->GetDebugName().Data());
              // fetch a list of all EditorObjects that have a proxy matching closestObj              
              const EditorProxy *proxy = FindProxy(closestObj, true);          
              if (proxy)
                _objMouseOver = proxy->edObj;
              else
              {
#if _VBS2_MODIFY_WORLD
                // bounding box around target
                if (closestObj->GetShape())
                {
                  _objStaticMouseOver.SetProxyObject(closestObj);                  
                  _objMouseOver = &_objStaticMouseOver;

                  // object already selected?
                  _objStaticMouseOver.SetSelected(_ws.IsSelected(closestObj));
                }
#endif
              }
            }

            // find object closest to cursor
#if 0
            if (!_objMouseOver)
            {
              Camera& camera = *GScene->GetCamera();
              Vector3 cDir = _camera->GetCursorDir().Normalized();                  

              int n = _ws.NObjects();
              float minDis2 = FLT_MAX;
              for (int i=0; i<n; i++)
              {
                EditorObject *obj = _ws.GetObject(i);
                if (!obj->IsVisibleIn3D() || !obj->IsObjInCurrentOverlay()) continue;
                
                const Object *proxy = obj->GetProxyObject();
                if (!proxy) continue;

                Vector3 pos = proxy->AimingPosition();
                Vector3Val dir = pos - camera.Position();

                // check nearest point on line
                float nearestT = cDir * dir;
                saturateMax(nearestT,0);
                Vector3 nearP = camera.Position() + cDir * nearestT;
               
                float dis = nearP.Distance(pos);

                if (dis < minDis2 && dis < 2)
                {
                  _objMouseOver = obj;
                  minDis2 = dis;
                }
              }
            }
#endif
          }

          GameState *gstate = GWorld->GetGameState(); 
          gstate->BeginContext(&_vars);
          _camera->Simulate(deltaT, SimulateInvisibleNear, gstate, cursorOverCamera);
          gstate->EndContext();
        }
      }
      Display::OnSimulate(vehicle);
    }
    break;
  case EMPreview:
    break;
  }
  if (_mode != EMPreview)
  {
    // need to manually move attached objects if simulation is disabled
    for (int i=0; i<_ws.GetSelected().Size(); i++)
    {        
      EditorObject *obj = _ws.GetSelected()[i];
      Entity *veh = dyn_cast<Entity>(obj->GetProxyObject());
      if (veh && (veh->IsEditorSimulationDisabled() || !_enableSimulation))
        veh->MoveAttached(1);
    }
  }
#if _VBS3 // command mode in RTE
  if (CommandMenuVisible())
  {
    AIBrain *agent = GWorld->FocusOn();
    if (agent && agent->GetLifeState() == LifeStateAlive)
    {
      AbstractUI *ui = GWorld->UI();
      EntityAIFull *vehicle = agent->GetVehicle();
      ui->ProcessMenu(*GWorld->GetScene()->GetCamera(),GWorld->GetCameraType(),vehicle);
      ui->UpdateTMTime();
    }
  }
#endif
#ifndef _XBOX
  if (oldMouseOver != _objMouseOver)
    ProcessMouseOverEvent();
#endif
}

void DisplayMissionEditor::ProcessMouseOverEvent()
{
#ifndef _XBOX
  if (_objMouseOver)
    OnEditorEvent(MEMouseOver, _objMouseOver->GetArgument("VARIABLE_NAME"), _objMouseOver->GetProxyObject());
  else
    OnEditorEvent(MEMouseOver, RString(), NULL);
#endif
}

void DisplayMissionEditor::ProcessOnQuickAddObjectEventToggled(bool on)
{
  GameState *state = GWorld->GetGameState();
  GameVarSpace vars(false);
  state->BeginContext(&vars);
  state->VarSetLocal("_map", CreateGameControl(_map), true);
  state->VarSetLocal("_on", on, true);
  state->EvaluateMultiple(_onQuickAddObjectToggled, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  state->EndContext();
}

void DisplayMissionEditor::ProcessOnQuickAddObjectEvent(Vector3 pos)
{
  // add object with default settings
  CListBoxContainer *types = GetListBoxContainer(GetCtrl(IDC_EDITOR_ADDOBJ_TYPES));
  CListBoxContainer *list = GetListBoxContainer(GetCtrl(IDC_EDITOR_ADDOBJ_LISTING));

  if (_onQuickAddObject.GetLength()>0 && types && list)
  {
    int nObjs = _ws.NObjects();

    int sel = list->GetCurSel();
    RString selectedClass = list->GetData(sel);

    sel = types->GetCurSel();
    RString selectedType = types->GetData(sel);

    GameState *state = GWorld->GetGameState();

    GameValue posValue = state->CreateGameValue(GameArray);
    GameArrayType &posArray = posValue;
    posArray.Realloc(3);
    posArray.Add(pos[0]);
    posArray.Add(pos[2]);
    posArray.Add(pos[1]);//-GLandscape->SurfaceYAboveWater(pos[0],pos[2]));

    GameVarSpace vars(false);
    state->BeginContext(&vars);
    state->VarSetLocal("_map", CreateGameControl(_map), true);
    state->VarSetLocal("_pos",posArray,true);
    state->VarSetLocal("_class",selectedClass,true);
    state->VarSetLocal("_type",selectedType,true);
    state->EvaluateMultiple(_onQuickAddObject, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    state->EndContext();

    if (_ws.NObjects() != nObjs)
      RecordAction();
  }
}

void DisplayMissionEditor::DoSelectObjectScript(EditorObject *obj, bool selected)
{
  QOStrStream out;
  obj->WriteSelectScript(out);
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&_vars);
  gstate->VarSet("_selected", selected, true, false, GWorld->GetMissionNamespace());
  gstate->Execute(RString(out.str(), out.pcount()), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  gstate->EndContext();
}

void DisplayMissionEditor::SelectObject(RString name)
{
#if _VBS2_MODIFY_WORLD
  if (IsMouseOnStaticObject())
  {
    if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
      ClearSelected();
    _ws.AddSelected(_objStaticMouseOver.GetProxyObject());
    UpdateControlsHelp();
    return;
  }
#endif

  EditorObject *selected = _ws.FindObject(name);
  if (selected)
  {    
    bool resetMouseOver = _objMouseOver == selected;
    if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
      ClearSelected();
    if (resetMouseOver) _objMouseOver = selected;
    _ws.SetSelected(selected,true);
#ifndef _XBOX
    OnEditorEvent(MESelectObject, name, selected->GetProxyObject());
#endif
    DoSelectObjectScript(selected,true);
    OnObjectSelected(name);
    UpdateControlsHelp();
  }
}

void DisplayMissionEditor::UnSelectObject(RString name)
{
#if _VBS2_MODIFY_WORLD
  if (IsMouseOnStaticObject())
  {    
    _ws.RemoveFromSelected(_objStaticMouseOver.GetProxyObject());
    UpdateControlsHelp();
    return;
  }
#endif

  EditorObject *selected = _ws.FindObject(name);

  if (_objMouseOver == selected)
    _objMouseOver = NULL;

  // TODO: re-enable simulation for unselected object in realtime?
  if (selected)
  {
    _ws.SetSelected(selected,false);
#ifndef _XBOX
    OnEditorEvent(MEUnselectObject, name, selected->GetProxyObject());
#endif
  }
  UpdateControlsHelp();
}

void DisplayMissionEditor::ClearSelected()
{
  EnableSimulationForSelected(true);
  _ws.ClearSelected();
  _objMouseOver = NULL;
  UpdateControlsHelp();
#ifndef _XBOX
  OnEditorEvent(MEClearSelected);
#endif
}

void DisplayMissionEditor::EnableSimulationForSelected(bool enable) 
{
  if (_realTime)
  {
    _simulationDisabled = !enable;

    RefArray<EditorObject> selected = _ws.GetSelected();
    for (int i=0; i<selected.Size(); i++) 
    {
      EditorObject *obj = selected[i];
      if (obj && obj->GetScope() >= EOSDrag)
      {
        Object *proxy = unconst_cast(selected[i]->GetProxyObject());
        if (proxy)
        {
          Entity *veh = dyn_cast<Entity>(proxy);
          if (veh)
          {
            veh->EditorDisableSimulation(!enable);
#if _VBS3
            if(!enable)
              veh->OnMoved(); //disable sleep mode
#endif
          }
        }
      }
    }
  }
}

void DisplayMissionEditor::AskForOverlaySave(int idd, RString text)
{
  CreateMsgBox
  (
    MB_BUTTON_OK | MB_BUTTON_CANCEL,
    text,
    idd
  );  
  MsgBox *msgBox = GetMsgBox();
  if (msgBox)
  {
    CButton *ok = dynamic_cast<CButton *>(msgBox->GetCtrl(IDC_OK));
    if (ok) ok->SetText(LocalizeString("STR_LIB_INFO_YES"));
    CButton *cancel = dynamic_cast<CButton *>(msgBox->GetCtrl(IDC_CANCEL));
    if (cancel) cancel->SetText(LocalizeString("STR_LIB_INFO_NO"));
  }
}

void DisplayMissionEditor::CommitActiveTemplateOverlay() 
{
  EditorOverlay *currentOverlay = _ws.GetActiveOverlay();
  if (!currentOverlay) return;
  if (!currentOverlay->IsTemplate()) return;

  ClearSelected();

  // recreate objects
  RefArray<EditorObject> newObjects;

  // delete objects that are in currentOverlay
  for (int i=0; i<_ws.NObjects(); i++)
  {
    EditorObject *obj = _ws.GetObject(i);
    if (obj->GetParentOverlay() && obj->GetParentOverlay() == currentOverlay)
    {
      EditorObject *copiedObj = new EditorObject(obj->GetType());
      newObjects.Add(copiedObj);
      *copiedObj = *obj;
      DeleteObject(obj);
      i--;
    }    
  }
  
  _ws.DeleteOverlay(currentOverlay);
  _ws.ClearInstances();
  _ws.SetActiveOverlay(NULL);

  // create objects again
  for (int i=0; i<newObjects.Size(); i++)
  {
    EditorObject *obj = _ws.CopyObject(newObjects[i]);    
    obj->SetParentOverlay(NULL,true);
    CreateObject(obj,false,false,true);  // _new = false in create scripts
  }

#if _ENABLE_UNDO
  _ws.SaveInstance(); // this is the first undo recording
#endif

  UpdateObjectBrowser();
  UpdateControlsHelp();

#ifndef _XBOX
  OnEditorEvent(MECloseOverlay, *_overlayType >> "name", _overlayType);
  _overlayType = NULL;
#endif
}

void DisplayMissionEditor::DeleteOverlay(EditorOverlay *overlay) 
{
  if (!overlay) return;

  if (overlay == _ws.GetActiveOverlay())
    _ws.SetActiveOverlay(NULL);

  // delete objects that are in currentOverlay, if it is a template overlay
  for (int i=0; i<_ws.NObjects(); i++)
  {
    EditorObject *obj = _ws.GetObject(i);
    if (obj->GetParentOverlay() && obj->GetParentOverlay() == overlay)
    {
      obj->SetParentOverlay(NULL);
      DeleteObject(obj);
      i--;
    }    
  }
  _ws.DeleteOverlay(overlay);
}

// only allows one overlay at a time for now, in future allow more than one?
void DisplayMissionEditor::SetActiveTemplateOverlay(EditorOverlay *newOverlay) 
{
  EditorOverlay *currentOverlay = _ws.GetActiveOverlay();
  if (currentOverlay == newOverlay) return;

  ClearSelected();

  // delete objects that are in currentOverlay, if it is a template overlay
  if (currentOverlay && currentOverlay->IsTemplate())
  {
    for (int i=0; i<_ws.NObjects(); i++)
    {
      EditorObject *obj = _ws.GetObject(i);
      if (obj->GetParentOverlay() && obj->GetParentOverlay() == currentOverlay)
      {
        obj->SetParentOverlay(NULL);
        DeleteObject(obj);
        i--;
      }    
    }
    _ws.DeleteOverlay(currentOverlay);
  }

  if (!_overlayType) return;

  _ws.ClearInstances();
  _ws.SetActiveOverlay(newOverlay);  

  // create objects in the active overlay
  if (newOverlay)
  {
    int nObj = _ws.NObjects();

    // add objects
    RString filename = GetUserDirectory() + "MPMissions\\" + newOverlay->GetName() + "\\mission.biedi";

    if (!QFBankQueryFunctions::FileExists(filename))
      filename = GetUserDirectory() + RString("Overlays\\") + newOverlay->GetName() + "." + newOverlay->GetExtension() + RString(".biedi");

    SetCursor("Wait");
    DrawCursor();

    _ws.Load(filename, true);    

    RefArray<EditorObject> newObjects; int lowest = -1; float hLowest = FLT_MAX;
    for (int i=nObj; i<_ws.NObjects(); i++)
    {
      EditorObject *obj = _ws.GetObject(i);
      newObjects.Add(obj);
    }

    _ws.RenameObjects(newObjects);

    for (int i=0; i<newObjects.Size(); i++)
    {
      EditorObject *obj = _ws.GetObject(i);
      GameValue result = obj->GetArgumentValue("POSITION");
      if (result.GetNil() || result.GetType() != GameArray) continue;

      const GameArrayType &array = result;
      if (array.Size() < 3 || array[2].GetType() != GameScalar) continue;
      float hgt = array[2];
      if (hgt < hLowest && fabs(hgt) > 0.0001)
      {
        hLowest = hgt;
        lowest = i;
      }      
    }    

    // find center of all pasted objects
    Vector3 sum = VZero;
    for (int i=0; i<newObjects.Size(); i++)
    {       
      Vector3 pos;
      if (newObjects[i]->GetEvaluatedPosition(pos))
        sum += pos;
    }
    Vector3 center = (1.0f / newObjects.Size()) * sum;
    center[1] = 0;
    
    float sizeLand = LandGrid * LandRange;
    bool outOfBounds = center.X() > sizeLand || center.Z() > sizeLand;

    // out of bounds, or a layer?
    Vector3 offset;
    if (newObjects.Size() > 0 && (!newOverlay->IsTemplate() || outOfBounds))
    {
      if (_mode == EMMap)
      {
        DrawCoord ptMap(_map->_center.x, _map->_center.y);
        offset = _map->ScreenToWorld(ptMap) - center;
      }
      else
      {
        Vector3 ground;
        bool found = _camera->GetGroundIntercept(ground,0,0,false,false);
        if (found && ground.Distance(_camera->Position()) > 200) ground = _camera->Position();
        offset = ground - center;
      }
      
      // work out height difference to move lowest object to same point
      if (lowest > -1)
      {
        EditorObject *obj = newObjects[lowest];
        Vector3 objPos;
        if (obj->GetEvaluatedPosition(objPos,true))
        {
          objPos = objPos + offset;
          float aslCheck = objPos[1] - GLandscape->SurfaceYAboveWater(objPos[0], objPos[2]);
          offset[1] -= aslCheck - obj->GetHeight();
        }
      }

      for (int i=0; i<newObjects.Size(); i++)
      {       
        EditorObject *obj = newObjects[i];
      
        // risky..?
        const EditorParam *param = obj->GetAllParams().Find("ANCHOR");
        if (param)
        {
          obj->RemoveArgument("ANCHOR");
          obj->SetArgument("ANCHOR","off");
        }

        GameValue result = obj->GetArgumentValue("POSITION");
        if (result.GetNil() || result.GetType() != GameArray) continue;
        
        const GameArrayType &array = result;
        if (array.Size() < 2 || array[0].GetType() != GameScalar || array[1].GetType() != GameScalar) continue;
                
        Vector3 objPos;
        objPos[0] = array[0];
        objPos[2] = array[1];
        if (array.Size() > 2) objPos[1] = array[2];

        objPos = objPos + offset;

        if (array.Size() == 2)
          objPos[1] = GLandscape->SurfaceYAboveWater(objPos[0], objPos[2]);
        else
        {
          // make sure all types except object are above the ground
          if (stricmp(obj->GetType()->GetName(),"object") != 0)  // if not an object
          {
            float aslCheck = objPos[1] - GLandscape->SurfaceYAboveWater(objPos[0], objPos[2]);
            if (aslCheck < 0) objPos[1] -= aslCheck;
          }
        }

        RString value = Format("[%.5f, %.5f, %.5f]", objPos.X(), objPos.Z(), objPos.Y());
        
        // update argument - position
        newObjects[i]->RemoveArgument("POSITION");
        newObjects[i]->SetArgument("POSITION", value);
      }
    }    

    // create objects
    CreateObject(_ws.GetPrefixObject(),false,false,true);
    
    for (int i=0; i<newObjects.Size(); i++)
      CreateObject(newObjects[i],false,false,true);  // _new = false in create scripts

    CreateObject(_ws.GetPostfixObject(),false,false,true);

    // look at created layer
    if (newObjects.Size() > 0 && (!newOverlay->IsTemplate() || outOfBounds))
    {
      center += offset;
      center[1] = GLandscape->SurfaceYAboveWater(center[0], center[2]);
      if (_mode == EM3D)
      {
        if (_map) _map->CStaticMap::Center(center, false);
        if (_miniMap) _miniMap->CStaticMap::Center(center, false);
      }
      else if (_mode == EMMap)
        _camera->LookAt(center);
    }

#ifndef _XBOX
    OnEditorEvent(MEEditOverlay, *_overlayType >> "name", _overlayType);
#endif

#if _ENABLE_UNDO
    _ws.SaveInstance(); // this is the first undo recording
#endif
    SetCursor("Arrow");
  }
  else
  {
#ifndef _XBOX
    OnEditorEvent(MECloseOverlay, *_overlayType >> "name", _overlayType);
    _overlayType = NULL;
#endif
  }
  _hasChanged = false;
  UpdateObjectBrowser();
  UpdateControlsHelp();
}  

void DisplayMissionEditor::EditLink(EditorObject *obj, RString name, float key, bool assignLinkArg)
{
  if (obj && obj->GetScope() < EOSLinkFrom) return;
  _linkObject = obj;
  _linkArgument = assignLinkArg ? name : RString();  // linkArgument is assigned when left-click next occurs
  _linkShortcutKey = key;

#ifndef _XBOX
  if (obj) OnEditorEvent(MELinkFromObject,_linkObject->GetArgument("VARIABLE_NAME"),_linkArgument,true);
#endif
}

void DisplayMissionEditor::OnLButtonDown(float x, float y)
{
  if (!EditorCameraActive()) return;

  Vector3 clickPos;
  bool clickValid = _camera->GetGroundIntercept(clickPos,false);

  if (clickValid && _addOnLeftClick)
  {
    // add object with default settings
    ProcessOnQuickAddObjectEvent(clickPos);
    return;
  }

  // ensure we process right mouse up before left button down
  OnRButtonUp(x, y);

  // linking one object to another?
#if _ENABLE_3D_LINKING
  if (_linkObject && _allow3DLinking)
  {    
    _selecting = false;
    _selectOne = true;

    bool showNewObj = true;

    const EditorParam *param = _linkObject->GetAllParams().Find(_linkArgument);    

    // holding down shortcut key and linking to nothing?
      // search only for context link type with the matching shortcut key
    if (!param && !_objMouseOver)
    {
      const EditorParams &params = _linkObject->GetAllParams();
      int nParams = params.Size();
      for (int i=0; i<nParams; i++)
      {
        if (params[i].source == EPSContext && params[i].shortcutKey == _linkShortcutKey)
        {
          if (IsMenuItemActive(params[i].activeMode, _linkObject, (CStaticMapEditor *)_map.GetRef(), IsRealTime()))
            param = &params[i];
        }
      }    
    }
  
    // link to new object?
    if (param && param->source == EPSContext)
    {   
      const char *ptr = param->subtype;
      const char *ext = strchr(ptr, ',');

      // editor object type
      RString objectType = Trim(ptr, ext - ptr);
             
      // link type
      ptr = ext + 1;
      RString linkType = Trim(ptr, strlen(ptr));

      // is there a type in memory that matches the one we are trying to add?      
      if (LastObjectCreated())
      {
        EditorObjectType *type = LastObjectCreated()->GetType();
        if (type)
        {
          if (stricmp(objectType,type->GetName()) == 0)
          {
            // try and find a suitable link parameter that we can use to link the new (copied) object with the linkObject
              // look for the parameter with the same shortcut key TODO: is there a better way?
            const EditorParams &params = _linkObject->GetAllParams();
            int nParams = params.Size();
            for (int i=0; i<nParams; i++)
            {
                if (
                  params[i].source == EPSLink && 
                  (_linkShortcutKey == -1 || params[i].shortcutKey == _linkShortcutKey) &&
                  stricmp(params[i].type,type->GetName()) == 0 &&
                  stricmp(params[i].name,linkType) == 0
                )
                {                  
                  _linkArgument = params[i].name;
                  showNewObj = false;
                }
            }                  
          }
        }
      }

      if(clickValid)
      {
        // set correct object type (matching name)
        if (showNewObj)
        {
          if (SelectEditorObject(objectType))
          {
            ShowNewObject(clickPos,_linkObject,linkType);
          }
        }
        else
        {
          // create the new object based on what is in memory, and link to it
          // convert position back to string
          EditorObject *obj = _ws.CopyObject(LastObjectCreated());
          if (obj)
          {
            // assign new name (because old name has been copied over)
            EditorObjectType *type = obj->GetType();
            if (type)
            {        
              obj->RemoveArgument("VARIABLE_NAME");        
              obj->SetArgument("VARIABLE_NAME", type->NextVarName());
            }

            RString value = Format("[%.5f, %.5f, %.5f]", clickPos.X(), clickPos.Z(), clickPos.Y());

            // update argument - position
            obj->RemoveArgument("POSITION");
            obj->SetArgument("POSITION", value);

            obj->SetParentOverlay(_ws.GetActiveOverlay(), true);

            CreateObject(obj,false,false,false,false,true);

            RString id = obj->GetArgument("VARIABLE_NAME");
            OnLinkEditted(_linkObject, _linkArgument, id, -1, NULL);                 
            SelectObject(id);
          }
        }
      }
    }
    else
    {
      RString name;

      // link to position?
      if (clickValid && param && param->source == EPSLink && stricmp(param->type, "position") == 0)
      {
        name = Format("[%.5f, %.5f]", clickPos.X(), clickPos.Z());
      }
      // link to existing object?
      else if (_objMouseOver) 
        name = _objMouseOver->GetArgument("VARIABLE_NAME");        

      // do linkage
      if(clickValid || _objMouseOver)
      {
        OnLinkEditted(_linkObject, _linkArgument, name, _linkShortcutKey, NULL);       
        SelectObject(name);
      }
    }
    
    if (showNewObj || GInput.keys[_linkShortcutKey] == 0)
    {
#ifndef _XBOX
      if (_linkObject) OnEditorEvent(MELinkFromObject,_linkObject->GetArgument("VARIABLE_NAME"),_linkArgument,false);
#endif
      _linkObject = NULL;
      _linkShortcutKey = -1;
    }
    UpdateControlsHelp();
    return;
  }
#endif

  // mouse over object?
  _selectOne = false;
  if (_objMouseOver && !_objMouseOver->IsAnchored())
  {
    // select object in object browser
    OnObjectSelected(_objMouseOver->GetArgument("VARIABLE_NAME"));

    if (!_objMouseOver->IsSelected())
      SelectObject(_objMouseOver->GetArgument("VARIABLE_NAME"));
    else
      if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
        UnSelectObject(_objMouseOver->GetArgument("VARIABLE_NAME")); 

    // update help text
    if (_objMouseOver && _objMouseOver->IsSelected())
      SetSelectedStatus(ESSelected);
    else
      SetSelectedStatus(ESUnSelected);    

    _selectOne = true;

    // was the link shortcut key pressed (held down)?
#if _ENABLE_3D_LINKING
    if (_objMouseOver && _allow3DLinking)
    {
      // search for shortcut keys
      const EditorParams &params = _objMouseOver->GetAllParams();
      int nParams = params.Size();
      for (int i=0; i<nParams; i++)
      {
        if ((params[i].source != EPSLink && params[i].source != EPSParent && params[i].source != EPSContext) || params[i].shortcutKey < 0) continue;
        if (GInput.keys[params[i].shortcutKey] > 0)
        {
          EditLink(_objMouseOver, params[i].name, params[i].shortcutKey);
          return;
        }
      }      
    }
#endif
    //-!

    _dragging = true;
    Vector3 isect;
    _dragStartedInAir = !_camera->GetGroundIntercept(isect);
    _dragPos = isect;
    _selecting = false;
  }
  else
  {
    AspectSettings _aspect;
    GEngine->GetAspectSettings(_aspect);
    x /= _aspect.leftFOV;

    _pt1 = Point2DFloat(x, y); 
    SetSelectedStatus(ESNone);
  }
}

void DisplayMissionEditor::OnLButtonUp(float x, float y)
{
  if (!EditorCameraActive() || _addOnLeftClick) return;

  if (_selecting)
  {
    _selecting = false;

    // do multiple selection
    if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
      ClearSelected();

    Camera& camera = *GScene->GetCamera();

    float posX = TO_MOUSE(_pt1.x) * camera.Left();
    float posY = -TO_MOUSE(_pt1.y) * camera.Top();
    Vector3 startSelection = camera.DirectionModelToWorld(Vector3(posX, posY, 1));

    posX = TO_MOUSE(_pt2.x) * camera.Left();
    posY = -TO_MOUSE(_pt2.y) * camera.Top();
    Vector3 endSelection = camera.DirectionModelToWorld(Vector3(posX, posY, 1));

    Matrix4Val camInvTransform = camera.GetInvTransform();
    Vector3 posStart = camInvTransform.Rotate(startSelection);
    Vector3 posEnd = camInvTransform.Rotate(endSelection);
    if (posStart.Z() > 0 && posEnd.Z() > 0)
    {
      AspectSettings _aspect;
      GEngine->GetAspectSettings(_aspect);
      x /= _aspect.leftFOV;

      float invZ = 1.0 / posStart.Z();
      float xs = 0.5 * (1.0 + posStart.X() * invZ * camera.InvLeft());
      float ys = 0.5 * (1.0 - posStart.Y() * invZ * camera.InvTop());
      invZ = 1.0 / posEnd.Z();
      float xe = 0.5 * (1.0 + posEnd.X() * invZ * camera.InvLeft());
      float ye = 0.5 * (1.0 - posEnd.Y() * invZ * camera.InvTop());
      if (xs > xe) swap(xs, xe);
      if (ys > ye) swap(ys, ye);
      int n = _ws.NObjects();
      for (int i=0; i<n; i++)
      {
        EditorObject *obj = _ws.GetObject(i);

        const Object *proxy = obj->GetProxyObject();
        if (!proxy) continue;

        // check position
        Vector3 dir = proxy->AimingPosition() - camera.Position();
        Vector3 pos = camInvTransform.Rotate(dir);
        if (pos.Z() <= 0) continue;
        invZ = 1.0 / pos.Z();
        float x = 0.5 * (1.0 + pos.X() * invZ * camera.InvLeft());
        float y = 0.5 * (1.0 - pos.Y() * invZ * camera.InvTop());
        if (x < xs) continue;
        if (x > xe) continue;
        if (y < ys) continue;
        if (y > ye) continue;

        // unlike 2D select, *don't* select hidden subordinates also
        _ws.SetSelected(obj,!obj->IsSelected(), false, false, true);
#ifndef _XBOX
        OnEditorEvent(MESelectObject, obj->GetArgument("VARIABLE_NAME"), obj->GetProxyObject());
#endif
        UpdateControlsHelp();
      }
    }
  }
  else if (_dragging && _movingEdObj)
  {
    UpdateRemoteSelObjects();
#ifndef _XBOX
    OnEditorEvent(MEStopMovingObject);
#endif
    RecordAction();        
  }
  _movingEdObj = false;  
  _dragging = false;
}

void DisplayMissionEditor::OnLButtonClick(float x, float y)
{
  if (!EditorCameraActive() || _addOnLeftClick) return;

  if (!_dragging)
  {    
    if (!_selectOne && !GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
      ClearSelected();

    // _selectOne below is reqd to prevent select box following linkage

    // start drawing multiple select box?
    if (!_selectOne && GInput.mouseL)
    {
      _dragging = false;
      _selecting = true;
      // _pt1 is set when mouse is first depressed
    }    
  }
  _selectOne = false;
}

void DisplayMissionEditor::OnLButtonDblClick(float x, float y)
{
  if (!EditorCameraActive() || _addOnLeftClick) return;
  if (GInput.keys[DIK_LSHIFT]>0) return;

  if (
    _objMouseOver && 
    !_objMouseOver->IsAnchored() &&
    !GInput.keys[DIK_LCONTROL]
  )
  {
    ShowEditObject(_objMouseOver->GetArgument("VARIABLE_NAME"));
  }
  else    
  {
    Vector3 pos;
    bool valid = _camera->GetGroundIntercept(pos, false);
    if(valid)
    {
      if (GetOnDoubleClick().GetLength()>0)      
      {
        ProcessOnClick(GetOnDoubleClick(),pos,_map,true);
      }
      else
        ShowNewObject(pos);
    }
  }
}

void DisplayMissionEditor::OnMouseMove(float x, float y, bool active)
{
  if (!EditorCameraActive()) return;
  /*
  Vector3 startPos = _camera->GetGroundIntercept(TO_MOUSE(_pt1.x), TO_MOUSE(_pt1.y));
  Vector3 dragPos = _camera->GetGroundIntercept(TO_MOUSE(_pt2.x), TO_MOUSE(_pt2.y));
  GScene->DrawCollisionStar
  (
    startPos,2,PackedColor(Color(0.25,0,0.25))
  );
  GScene->DrawCollisionStar
  (
    dragPos,2,PackedColor(Color(0.25,0,0.25))
  );
  */

  if (_objMouseOver)
  {
    if (_objMouseOver->IsSelected())
      SetSelectedStatus(ESSelected);
    else
      SetSelectedStatus(ESUnSelected);
  }
  else
    SetSelectedStatus(ESNone);
  
  // currently raising or rotating selected objects?
  if (_draggingRM && !_camera->IsMoving())
  {
    _rotating = GInput.keys[DIK_LSHIFT] > 0;
    _raising = GInput.keys[DIK_LALT] > 0;
    _rotatingOrRaisingOnMap = MouseOverMap() != NULL;

    if (!_rotating && !_raising)
      _camera->SetRotating(true);
  }
  else
  { 
    _rotating = false;
    _raising = false;
    _rotatingOrRaisingOnMap = true;
  }

  if (_dragging)
    _movingEdObj = true;

  // used with drawing select box in 3D
  _pt2 = Point2DFloat(x, y); 
}

void DisplayMissionEditor::OnRButtonDown(float x, float y)
{
  RemoveContextMenu();
  if (!EditorCameraActive()) return;  

  _draggingRM = true;

  if (GInput.keys[DIK_LALT] || GInput.keys[DIK_LSHIFT]) return;

  // mouse over object?
  if (_objMouseOver)
  {
    // select object in object browser
    OnObjectSelected(_objMouseOver->GetArgument("VARIABLE_NAME"));

    if (!_objMouseOver->IsSelected())
      SelectObject(_objMouseOver->GetArgument("VARIABLE_NAME"));
    else
      if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
        UnSelectObject(_objMouseOver->GetArgument("VARIABLE_NAME")); 
  }
}

void DisplayMissionEditor::OnRButtonUp(float x, float y)
{
  if (!EditorCameraActive()) return;

  if (_draggingRM)
    _camera->SetRotating(false);

  if (_rotating || _raising)
  {
    UpdateRemoteSelObjects();
    RecordAction();
  }

  _rotating = false;  
  _raising = false;
  _draggingRM = false;  
}

void DisplayMissionEditor::OnRButtonClick(float x, float y)
{
  if (!EditorCameraActive()) return;

  RString id;
  if (_objMouseOver)
    id = _objMouseOver->GetArgument("VARIABLE_NAME");
  
  Vector3 pos;
  _camera->GetGroundIntercept(pos,false);
  CreateContextMenu(
      x, y, IDC_EDITOR_MENU,
      Pars >> "RscDisplayMissionEditor" >> "Menu",
      CCMContextEditor(id, (CStaticMapEditor *)_map.GetRef(), pos)
  );
}

void DisplayMissionEditor::OnDraw(EntityAI *vehicle, float alpha)
{
  switch (_mode)
  {
  case EMMap:
    DisplayMap::OnDraw(vehicle, alpha);
    break;
  case EM3D: 
    if ( GWorld->GetTitleEffect() )
    {
      GWorld->GetTitleEffect()->Draw();
    }
    // draw multiple select box
    if (_selecting && _allow3DMode)
    {
      float wScreen = GLOB_ENGINE->Width2D();
      float hScreen = GLOB_ENGINE->Height2D();
      Rect2DPixel clipRect(0,0,wScreen,hScreen);
      PackedColor color = PackedColor(Color(0,1,0,1));
      GEngine->DrawLine
      (
        Line2DPixel(_pt1.x * wScreen, _pt1.y * hScreen,
        _pt2.x * wScreen, _pt1.y * hScreen),
        color, color, clipRect
      );
      GEngine->DrawLine
      (
        Line2DPixel(_pt2.x * wScreen, _pt1.y * hScreen,
        _pt2.x * wScreen, _pt2.y * hScreen),
        color, color, clipRect
      );
      GEngine->DrawLine
      (
        Line2DPixel(_pt2.x * wScreen, _pt2.y * hScreen,
        _pt1.x * wScreen, _pt2.y * hScreen),
        color, color, clipRect
      );
      GEngine->DrawLine
      (
        Line2DPixel(_pt1.x * wScreen, _pt2.y * hScreen,
        _pt1.x * wScreen, _pt1.y * hScreen),
        color, color, clipRect
      );
    }
    if (!_child && EditorCameraActive() && _allow3DMode && !_interfaceHidden)
    {            
      PackedColor rotateColor = PackedColor(Color(0, 1, 1, 1));
      PackedColor raisingColor = PackedColor(Color(1, 0, 0, 1));
      PackedColor selectedColor = PackedColor(Color(0, 1, 0, 1));
      PackedColor mouseOverColor = _camera->SelectedColor();

      Object *proxyMouseOver = NULL;
      if (_objMouseOver && !_objMouseOver->IsAnchored() && (!_dragging || _selectOne))
        proxyMouseOver = _objMouseOver->GetProxyObject();

      bool updateDraw3D = Glob.uiTime - _lastObjectDraw3DUpdate > 0.2;
      if (updateDraw3D)
        _lastObjectDraw3DUpdate = Glob.uiTime;

      GameState *gstate = GWorld->GetGameState();
      gstate->BeginContext(&_vars);
      gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());

      // draw 3D icons
        // TODO: optimize & draw in order of distance from camera (closest first)
      for (int i=0; i<_ws.NObjects(); i++)
      {
        // have to use proxy object as position has no Y value!
        EditorObject *obj = _ws.GetObject(i);        

        // exec draw3D scripts
        if (updateDraw3D) obj->UpdateExecDraw3D(gstate);

        if (obj->ExecDraw3D())
        {
          float alpha = 1.0f;
          if (!obj->IsObjInCurrentOverlay()) alpha = 0.1f;
          if (obj->IsSelected())
          {
            const float period = 0.3f;
            const float speed = H_PI / period;
            alpha = 0.33 * sin(speed * Glob.uiTime.toFloat()) + 0.67;
          }
          gstate->VarSet("_alpha", GameValue(alpha), true, false, GWorld->GetMissionNamespace());
          gstate->VarSet("_inLayer", obj->IsObjInCurrentOverlay(), true, false, GWorld->GetMissionNamespace());
          obj->ExecuteDraw3DScript(gstate);
        }

        Object *proxy = obj->GetProxyObject();
        if (proxy)
        {             
          proxy->_highlighted = 0;
          // it is possible to select objs not visible in the tree, so still draw hidden object icons
          if (!obj->IsVisibleIn3D()) continue;
          int n = obj->NIcons();
          bool lineDrawn = false;
          for (int j=0; j<n; j++)
          {
            EditorObjectIcon &icon = obj->GetIcon(j);
            if (icon.is3D)
            {
              PackedColor colorIcon = icon.color, colorLine = PackedWhite;

              if (proxy == proxyMouseOver || obj->IsSelected())
              {
                PackedColor specialColor = selectedColor;
                if (proxy == proxyMouseOver) specialColor = mouseOverColor;
                if (_rotating) specialColor = rotateColor;
                if (_raising) specialColor = raisingColor;

                colorIcon = colorLine = specialColor;
              }

              Vector3 pos = proxy->Position() + icon.offset;      
              if (
                _show3DIcons &&
                _camera->DrawIcon(pos, icon.icon, icon.width, icon.height, colorIcon, icon.shadow) &&
                !lineDrawn && 
                obj->Draw3DLine()
              )
              {
                _camera->DrawLine(pos, proxy->CameraPosition(), colorLine, icon.shadow);
                lineDrawn = true;
              }
            }
          }
        }
      }

      gstate->EndContext();

      if (!_rotating && !_raising)
      {
        if (proxyMouseOver)
        {
          // draw 'old' select icon for soldiers
          const Person* person = dyn_cast<const Person>(proxyMouseOver);
          if (person)
          {
            Vector3 pos = proxyMouseOver->GetShape() ? proxyMouseOver->COMPosition() : proxyMouseOver->Position();
            _camera->DrawSelectedIcon(pos, mouseOverColor);
          }
          else
          {
            proxyMouseOver->_highlighted = _objMouseOver->StayAboveGround() ? 
              HIGHLIGHT_FLAG | HIGHLIGHT_SNAP_TO_GROUND :
              HIGHLIGHT_FLAG;
            proxyMouseOver->_highlightedColor = mouseOverColor;
#if _VBS2_MODIFY_WORLD
            if (IsMouseOnStaticObject())
              DrawSelectBox(proxyMouseOver, mouseOverColor);  // temp - should be located in object draw code somewhere
#endif
          }
        }
      }

      // draw selected icons
      for (int i=0; i<_ws.GetSelected().Size(); i++)
      {
        EditorObject *obj = _ws.GetSelected()[i];
        if (!obj->IsVisibleIn3D()) continue;        
        Object *proxy = obj->GetProxyObject();
        if (proxy)
        {          
          PackedColor specialColor = selectedColor;
          if (_rotating) specialColor = rotateColor;
          if (_raising) specialColor = raisingColor;

          if (proxy->GetShape()) //draw 3d bounding box
          {
            if ((proxy != _camera->GetAttachedTo() && proxy != proxyMouseOver) || _rotating || _raising)
            {
              // draw 'old' select icon for soldiers
              const Person* person = dyn_cast<const Person>(proxy);
              if (person)
              {
                _camera->DrawSelectedIcon(proxy->COMPosition(), specialColor);
              }
              else
              {
                proxy->_highlighted = HIGHLIGHT_FLAG; //set correctly here
                proxy->_highlightedColor = specialColor;

                const EntityAI* veh = dyn_cast<const EntityAI>(proxy);
                if (!veh)
                  DrawSelectBox(proxy, specialColor); // need to manually draw box for the moment
              }
            }
          }
          else
            _camera->DrawSelectedIcon(proxy->Position(), specialColor);
        }
      }

    }
    // draw all controls in 3D also
    _compassDir = Vector3(0, 0, 1);
    if (!_interfaceHidden) DisplayMap::OnDraw(vehicle, alpha);

    // draw links in 3D
#if _ENABLE_3D_LINKING
    if (!_interfaceHidden && _allow3DLinking)
    {
      AutoArray<ObjectLink> objectLinks = _ws.GetDrawLinks();
      for (int i=0; i<objectLinks.Size(); i++)
      {    
        ObjectLink &objectLink = objectLinks[i];
        if (objectLink.to && objectLink.from)
        {
          if (objectLink.showIn3D && objectLink.from->IsVisible() && objectLink.to->IsVisible())
          {
            Object *from = objectLink.from->GetProxyObject();
            Object *to = objectLink.to->GetProxyObject();
            if (from && from->GetShape() && to && to->GetShape())
              GVBSVisuals.Add3dLine(from->COMPosition(), to->COMPosition(), objectLink.color);
          }
        }
        else
          objectLinks.Delete(i--);
      }

      // draw line if linking objects
      if (_linkObject)
      {
        Object *from = _linkObject->GetProxyObject();
        if (from)
        {
          Vector3 pos;
          _camera->GetGroundIntercept(pos,false);
          GVBSVisuals.Add3dLine(from->COMPosition(), pos, PackedBlack);              
        }
      }
    }
#endif
    
    break;
  case EMPreview:
//    DIAG_MESSAGE(500, LocalizeString("STR_EDITOR_MODE_PREVIEW"));
    break;
  }
#if _VBS3 // command mode in RTE
  if (CommandMenuVisible())
  {
    AIBrain *agent = GWorld->FocusOn();
    if (agent)
    {
      EntityAIFull *vehicle = agent->GetVehicle();
      if (vehicle)
      {
        AbstractUI *ui = GWorld->UI();
        if (ui->IsShownGroupInfo())
        {
          AIGroup *group = agent->GetGroup();
          if (group && group->Leader() == agent && group->UnitsCount() > 1)
          {
            ui->DrawGroupInfo(vehicle);
            if (ui->IsShownMenu())
              ui->DrawMenu(true);
          }
        }
      }
    }
  }
#endif
}

EditorObject *DisplayMissionEditor::FindProxyOwner(Object *proxyObject)
{
  EditorObject *obj = NULL;
  GameValue proxy = GameValueExt(proxyObject);   
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&_vars);
  for (int i=0; i<_ws.NObjects(); i++)
  {
    EditorObject *candidate = _ws.GetObject(i);
    GameValue result;
    result = gstate->Evaluate(candidate->GetArgument("VARIABLE_NAME"), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    if (result.IsEqualTo(proxy))
    {
      obj = candidate;
      break;
    }
  }
  gstate->EndContext(); 
  return obj;
};

void DisplayMissionEditor::ShowEditObject(RString id, bool disregardProxy)
{
  EditorObject *obj = _ws.FindObject(id);
  if (!obj) return;

  ClearSelectingBox();

  if (obj->GetType()->ContextMenuFollowsProxy() && !disregardProxy)
  {
    EditorObject *proxyOwner = FindProxyOwner(obj->GetProxyObject());
    if (proxyOwner)
    {
      obj = proxyOwner;
      id = obj->GetArgument("VARIABLE_NAME");
    }
  }

  if (obj->GetScope() < EOSDrag) return;

  OnObjectSelected(id);
  
  // fetch side from object
  TargetSide side = TSideUnknown;
  const EditorParams &params = obj->GetType()->GetParams();
  for (int i=0; i<params.Size(); i++)
  {
    const EditorParam &param = params[i];
    if (stricmp(param.name,"SIDE") == 0)
    {
      RString sideStr = obj->GetArgument("SIDE");        
      if ((side = GetSideFromName(sideStr)) != TSideUnknown)
        break;
    }
  }
  //-!

  CreateChild(new DisplayEditObject(this, (CStaticMapEditor *)_map.GetRef(), obj, true, NULL, RString(), RString(),side));
};

void DisplayMissionEditor::SetSelectedStatus(EditorSelectedStatus selectedObject) 
{
  bool updateHelpText = _selectedObject != selectedObject;
  _selectedObject = selectedObject;
  if (updateHelpText)
    UpdateControlsHelp();
}

void DisplayMissionEditor::UpdateControlsHelp()
{
  CStatic *help = dynamic_cast<CStatic *>(GetCtrl(IDC_EDITOR_CONTROLS_HELP));
  if (!help) return;

  RString text;

  if (_ws.GetActiveOverlay())
  {
    text = text + LocalizeString("STR_CONTROLS_OVERLAY");
    text = text + "\\n";
  }
  if ((_map && _map->GetLinkObject()) || (_miniMap && _miniMap->GetLinkObject()))
  {
    text = text + LocalizeString("STR_CONTROLS_MOUSE_LINK_OBJECT");
  }
  else
  {
    switch (_selectedObject)
    {
    case ESUnSelected:
      if (MouseOverMap())
        text = text + LocalizeString("STR_CONTROLS_MOUSE_UNSELECTED_OBJECT_MAP");
      else
        text = text + LocalizeString("STR_CONTROLS_MOUSE_UNSELECTED_OBJECT_3D");
      break;
    case ESSelected:
      if (MouseOverMap())
        text = text + LocalizeString("STR_CONTROLS_MOUSE_SELECTED_OBJECT_MAP");
      else
        text = text + LocalizeString("STR_CONTROLS_MOUSE_SELECTED_OBJECT_3D");
      break;
    default:
      text = text + LocalizeString("STR_CONTROLS_MOUSE_NO_OBJECT");
      break;
    }
  }
  text = text + "\\n";
  if (_ws.GetSelected().Size() > 0)
  {
    if (MouseOverMap())
      text = text + LocalizeString("STR_CONTROLS_MOUSE_SELECTED_MAP");
    else
      text = text + LocalizeString("STR_CONTROLS_MOUSE_SELECTED_3D");
    text = text + "\\n";
  }
  if (MouseOverMap())
    text = text + LocalizeString("STR_CONTROLS_MAP");
  else
    text = text + LocalizeString("STR_CONTROLS_3D");
  text = text + "\\n";
  if (_realTime)
    text = text + LocalizeString("STR_CONTROLS_KEYS_REAL_TIME");
  else
    text = text + LocalizeString("STR_CONTROLS_KEYS_EDITOR");

  help->SetText(text);
};

bool DisplayMissionEditor::SelectEditorObject(RString select)
{
  for (int i=0; i<_ws.NObjectTypes(); i++)
  {    
    const EditorObjectType *type = _ws.GetObjectType(i);
    if (stricmp(type->GetName(),select) == 0)
    {
      _objectTypeList->SetCurSel(i);
      return true;
    }
  }
  return false;
}

void DisplayMissionEditor::UpdateRemoteSelObjects()
{
  if (GWorld->GetMode() == GModeNetware)
  {
    int n = _ws.GetSelected().Size();
    bool oneSelected = OneProxySelected();

    // move remote units
    for (int i=0; i<n; i++)
    {
      EditorObject *obj = _ws.GetSelected()[i];
      Object *proxy = unconst_cast(obj->GetProxyObject());
      if (proxy && !proxy->IsLocal())
      {
        if (proxy->GetAttachedTo())
        {       
          if (!oneSelected) continue;
          if (obj->AllowMoveIfAttached() || obj->GetType()->AllowMoveIfAttached() || proxy->GetAttachFlags() > 0)
          {
            int flags = proxy->GetAttachFlags();
            Vector3 modelPos(VFastTransform, proxy->GetAttachedTo()->GetInvTransform(), proxy->Position());
            GetNetworkManager().DetachObject(proxy);
            GetNetworkManager().AttachObject(proxy, proxy->GetAttachedTo(), -1, modelPos, flags);
          }
        }
        else
          GetNetworkManager().AskForMove(proxy, proxy->Position());
      }
    }
  }
}


void DisplayMissionEditor::ShowNewObject(Vector3 pos, EditorObject *linkObj, RString linkArg, RString className, TargetSide side)
{
  // no item selected?
  if (_objectTypeList->GetCurSel() == -1)
      return;
  RString name = _objectTypeList->GetData(_objectTypeList->GetCurSel());
  EditorObjectType *type = _ws.FindObjectType(name);
  EditorObject *obj = new EditorObject(type);
  // fill object values
  const EditorParams &params = obj->GetType()->GetParams();
  for (int i=0; i<params.Size(); i++)
  {
    const EditorParam &param = params[i];
    if (param.source == EPSPosition)
    {
      // position
      RString value = Format("[%.5f, %.5f, %.5f]", pos.X(), pos.Z(), pos.Y());
      obj->SetArgument(param.name, value);
    }
    else if (param.source == EPSParent)
    {
      // parent
      //obj->SetArgument(param.name, child->GetParentObject());
    }
    else if (param.source == EPSId)
    {
      // do not change
    }
    else if (param.hasDefValue)
    {
      obj->SetArgument(param.name, param.defValue);
    }
  }
  ClearSelectingBox();
  if (!linkObj && _lastObjectCreated)
    className = _lastObjectCreated->GetArgument("TYPE");
  CreateChild(new DisplayEditObject(this, (CStaticMapEditor *)_map.GetRef(), obj, false, linkObj, linkArg, className, side));
}

void DisplayMissionEditor::CreateObject(EditorObject *obj, bool isNewObject, bool isPaste, bool isLoading, bool isCalledByExecEditorScriptCommand, bool isCreatedFromTemplate)
{
  bool cameraOnCursor = GWorld->CameraOn() == _camera;

  QOStrStream out;
  obj->WriteCreateScript(out);

// LogF("*** Creating object: %s", cc_cast(obj->GetArgument("VARIABLE_NAME")));
// LogF(RString(out.str(), out.pcount()));

  RString statement = obj->GetProxy();
  GameValue result;

  // create object

  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&_vars);
  gstate->VarLocal("_new");
  gstate->VarSet("_new", GameValue(isNewObject), true, false, GWorld->GetMissionNamespace());

  gstate->VarLocal("_loading");
  gstate->VarSet("_loading", GameValue(isLoading), true, false, GWorld->GetMissionNamespace());

  gstate->VarLocal("_paste");
  gstate->VarSet("_paste", GameValue(isPaste), true, false, GWorld->GetMissionNamespace());

  gstate->VarLocal("_calledByScript");
  gstate->VarSet("_calledByScript", GameValue(isCalledByExecEditorScriptCommand), true, false, GWorld->GetMissionNamespace());
  
  gstate->VarLocal("_createdFromTemplate");
  gstate->VarSet("_createdFromTemplate", GameValue(isCreatedFromTemplate), true, false, GWorld->GetMissionNamespace());
  
  // has the object been added in an overlay?
  gstate->VarLocal("_overlay");
  bool isOverlay = obj->GetParentOverlay() ? obj->GetParentOverlay()->IsTemplate() : false;
  gstate->VarSet("_overlay", GameValue(isOverlay), true, false, GWorld->GetMissionNamespace());

  gstate->Execute(RString(out.str(), out.pcount()), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  if (statement.GetLength() > 0) result = gstate->EvaluateMultiple(statement, GameState::EvalContext::_default, GWorld->GetMissionNamespace());  
  obj->UpdateVisibleOnMap(gstate);
  obj->UpdateExecDrawMap(gstate);
  obj->UpdateExecDraw3D(gstate);
  gstate->EndContext();

  // proxy object
  Object *object = NULL;

  // does a proxy for the object already exist?
    // this might occur through the use of setObjectProxy command in create script
  bool proxyExists = false;
  for (int i=0; i<_proxies.Size(); i++)
    if (_proxies[i].edObj == obj)
    {
      proxyExists = true;
      object = _proxies[i].object;
      break;
    }

  // register proxy
  if (!proxyExists && !result.GetNil() && result.GetType() == GameObject)
  {    
    object = static_cast<GameDataObject *>(result.GetData())->GetObject();
    if (object)
      AddProxy(object,obj);
  }

  obj->SetProxyObject(object);

  // just in case...
  if (object) object->CalcAttachedPosition(1);

  if (cameraOnCursor && GWorld->CameraOn() != _camera)
  {
    // keep camera on cursor
    _originalPlayer = GWorld->PlayerOn();
    SwitchToCamera();
  }  
}

void DisplayMissionEditor::UpdateObject(EditorObject *obj, bool isRotate, bool isRaise, float raiseBy)
{
  bool cameraOnCursor = GWorld->CameraOn() == _camera;

  QOStrStream out;
  obj->WriteUpdateScript(out);

  // update object
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&_vars);

  // has the object been updated in an overlay?
  gstate->VarLocal("_overlay");
  bool isOverlay = obj->GetParentOverlay() ? true : false;
  gstate->VarSet("_overlay", GameValue(isOverlay), true, false, GWorld->GetMissionNamespace());

  gstate->VarLocal("_rotate");
  gstate->VarSet("_rotate", GameValue(isRotate), true, false, GWorld->GetMissionNamespace());

  gstate->VarLocal("_raising");
  gstate->VarSet("_raising", GameValue(isRaise), true, false, GWorld->GetMissionNamespace());
  if (isRaise)
  {
    gstate->VarLocal("_raiseBy");
    gstate->VarSet("_raiseBy", GameValue(raiseBy), true, false, GWorld->GetMissionNamespace());
  }

  gstate->Execute(RString(out.str(), out.pcount()), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  gstate->EndContext();

  if (cameraOnCursor && GWorld->CameraOn() != _camera)
  {
    // keep camera on cursor
    _originalPlayer = GWorld->PlayerOn();
    SwitchToCamera();
  }
}

void DisplayMissionEditor::UpdatePositionObject(EditorObject *obj,Vector3Par offset)
{
  bool cameraOnCursor = GWorld->CameraOn() == _camera;

  QOStrStream out;
  obj->WriteUpdatePositionScript(out);

  // update object
  GameState *gstate = GWorld->GetGameState();

  // convert offset
  GameValue value = gstate->CreateGameValue(GameArray);
  GameArrayType &array = value;
  array.Resize(3);
  array[0] = offset.X();
  array[1] = offset.Z();
  array[2] = offset.Y();

  gstate->BeginContext(&_vars);
  gstate->VarSet("_offset", array, true, false, GWorld->GetMissionNamespace());
  gstate->Execute(RString(out.str(), out.pcount()), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  gstate->EndContext();

  if (cameraOnCursor && GWorld->CameraOn() != _camera)
  {
    // keep camera on cursor
    _originalPlayer = GWorld->PlayerOn();
    SwitchToCamera();
  }
}

void DisplayMissionEditor::DeleteObject(RString id)
{
  EditorObject *obj = _ws.FindObject(id);
  DeleteObject(obj);  
}

void DisplayMissionEditor::DeleteObject(EditorObject *obj)
{
  if (obj)
  {
#if _VBS2_MODIFY_WORLD
    // if it is static, just delete it
      // note that this screws artillery, need to look at a fix
    if (obj->IsStaticProxy())
    {
      // delete proxy
      Object *proxy = obj->GetProxyObject();
      _ws.RemoveFromSelected(proxy);
      for (int i=0; i<_proxies.Size(); i++)
        if (_proxies[i].object == proxy)
          _proxies.Delete(i--);
      Entity *veh = dyn_cast<Entity>(proxy);
      if (veh) veh->SetDelete();
      _ws.DeleteObject(obj);
    }
    else
#endif
    {
      QOStrStream out;
      obj->WriteDeleteScript(out);

      // call delete object script (is expected to use deleteEditorObject command)
      GameState *gstate = GWorld->GetGameState();
      gstate->BeginContext(&_vars);
      gstate->Execute(RString(out.str(), out.pcount()), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      gstate->EndContext();
    }
    RecordAction();
  }
};

void DisplayMissionEditor::DeleteObjectFromEditor(RString id)
{
  if (id.GetLength() == 0) return;

  bool cameraOnCursor = GWorld->CameraOn() == _camera;
  
  // check if object can be deleted
  bool link = false;
  bool parent = false;
  for (int i=0; i<_ws.NObjects(); i++)
  {
    const EditorObject *obj = _ws.GetObject(i);
    const EditorObjectType *type = obj->GetType();
    for (int j=0; j<type->GetParams().Size(); j++)
    {
      const EditorParam &param = type->GetParams()[j];
      if (param.source == EPSParent)
      {
        RString value = obj->GetArgument(param.name);
        if (stricmp(value, id) == 0) {parent = true; LogF("Unable to delete, %s is a child of %s",value.Data(),obj->GetArgument("VARIABLE_NAME").Data());}
      }
      else if (param.source == EPSLink)
      {
        RString value = obj->GetArgument(param.name);
        if (stricmp(value, id) == 0) {link = true; LogF("Unable to delete, link to %s remains in %s (%s)",value.Data(),obj->GetArgument("VARIABLE_NAME").Data(),param.name.Data());}
      }
      if (parent && link) break; // nothing more can be found
    }
  }
  if (parent)
  {
    CreateMsgBox(MB_BUTTON_OK, LocalizeString("STR_EDITOR_ERROR_CHILD_OBJS"));
    return;
  }
  else if (link)
  {
    CreateMsgBox(MB_BUTTON_OK, LocalizeString("STR_EDITOR_ERROR_HAS_LINKS"));
    return;
  }
  //-!

  EditorObject *obj = _ws.FindObject(id);
  if (obj)
  {
    if (obj->IsDeleting()) return;
    obj->SetDeleting();

    UnSelectObject(id);

    // delete proxy
    for (int i=0; i<_proxies.Size(); i++)
    {
      if (_proxies[i].id == id)
      {
        Object *objLink = _proxies[i].object;
        Entity *veh = dyn_cast<Entity>(objLink);
        if (veh) veh->SetDelete();
        _proxies.Delete(i--);
      }
    }

    // delete links that are drawn both from and to this object
    _ws.RemoveAllDrawLinks(obj);

    RemoveObjectFromBrowser(obj);

    if (obj == _objMouseOver) _objMouseOver = NULL;
    _ws.DeleteObject(obj);

    if (cameraOnCursor && GWorld->CameraOn() != _camera)
    {
      // keep camera on cursor
      _originalPlayer = GWorld->PlayerOn();
      SwitchToCamera();
    }    
  }
}

Object *DisplayMissionEditor::FindObject(RString id)
{
  for (int i=0; i<_proxies.Size(); i++)
  {
    if (stricmp(_proxies[i].id, id) == 0) return _proxies[i].object;
  }
  return NULL;
}

void DisplayMissionEditor::ShowObjectBrowser(bool show)
{
  if (_objectBrowser) _objectBrowser->ShowCtrl(show);
  if (_attributeBrowser) _attributeBrowser->ShowCtrl(show);
  if (_objectTypeList) _objectTypeList->ShowCtrl(show);
  SetCursor(show ? "Arrow" : NULL);
}

void DisplayMissionEditor::ShowBackground3D(bool show)
{
  if (_background3D) _background3D->ShowCtrl(show);
}

void DisplayMissionEditor::ShowMap(bool show)
{
  if (_map)
  {
    _map->ShowCtrl(show);
    OnEditorEvent(MEShowMap, show);
    if (_miniMap)
    {
    _miniMap->DrawMainMapBox(show);
    _miniMap->DrawEditorObjects(!show);    
    }
    //if (show) _map->Center();
  }
}

struct FindItemWithData
{
  RString _data;
  mutable CTreeItem *_item;

  FindItemWithData(RString data)
  {
    _data = data;
    _item = NULL;
  }

  bool operator ()(const CTreeItem *item) const
  {
    if (strcmp(item->data, _data) == 0)
    {
      _item = unconst_cast(item);
      return true;
    }
    return false;
  }
};

static RString GetItemDisplayName(const EditorObject *obj, RString &varName)
{  
  RString typeName = obj->GetType()->GetName();
  RString displayName = obj->GetDisplayNameTree();
  if (displayName.GetLength() == 0) displayName = obj->GetDisplayName();
  if (displayName.GetLength() == 0) displayName = Format("%s \"%s\"", (const char *)typeName, (const char *)varName);
  return displayName;
}

static void AddBrowserObject(CTree *tree, CTreeItem *parent, EditorWorkspace &ws, const EditorObject *obj, RString &selectedId, GameState *gstate, bool doFilter)
{
  if (!obj->IsObjInCurrentOverlay()) return;
  if (obj->GetScope() == EOSHidden || obj->GetScope() == EOSAllNoTree) return;

  if (doFilter)
  {
    RString editorFilter = obj->GetTreeFilter();
    if (editorFilter.GetLength() > 0)
    {
      if (!gstate->EvaluateBool(editorFilter, GameState::EvalContext::_default, GWorld->GetMissionNamespace())) 
        return;
    }
    else if (parent->level <= 1)
      return;
  }

  RString varName = obj->GetArgument("VARIABLE_NAME");
  RString displayName = GetItemDisplayName(obj, varName);

  CTreeItem *item = parent->AddChild();
  item->text = displayName;
  item->data = varName;

  if (selectedId.GetLength() > 0 && stricmp(selectedId,varName) == 0)
  {
    tree->SetSelected(item);
    selectedId = "";
  }

  // reset tree expansion
  if (item->parent && obj->GetParent().GetLength() > 0)
    item->parent->Expand(obj->IsVisibleInTree());
  
  // child objects
  for (int i=0; i<ws.NObjects(); i++)
  {
    EditorObject *obj = ws.GetObject(i);
    if (stricmp(obj->GetParent(), varName) == 0) 
      AddBrowserObject(tree, item, ws, obj, selectedId, gstate, doFilter);
  }
}

void DisplayMissionEditor::UpdateObjectBrowser(RString toUpdate, bool updateVisibility, bool expand) 
{
  if (!_objectBrowser) return;

  // find editor object
  UpdateObjectBrowser(_ws.FindObject(toUpdate), updateVisibility, expand);
}

static void RecreateChildren(CTreeItem *oldItem, CTreeItem *newItem)
{
  for (int i=0; i<oldItem->children.Size(); i++)
  {
    CTreeItem *child = oldItem->children[i];

    CTreeItem *newChild = newItem->AddChild();
    newChild->text = child->text;
    newChild->data = child->data;

    RecreateChildren(child, newChild);
  }
}
/*
static void FilterChildren(CTreeItem *item, GameState *gstate, EditorWorkspace &ws)
{
  for (int i=0; i<item->children.Size(); i++)
  {    
    CTreeItem *child = item->children[i];
    FilterChildren(child, gstate, ws);

    EditorObject *obj = ws.FindObject(item->data);
    if (!obj) continue;

    // do filter
    bool isFiltered = false;
    RString editorFilter = obj->GetTreeFilter();
    if (editorFilter.GetLength() > 0)
    {
      if (!gstate->EvaluateBool(editorFilter, GameState::EvalContext::_default, GWorld->GetMissionNamespace())) 
        isFiltered = true;
    }
    else if (item->level <= 1)
      isFiltered = true;
    if (isFiltered)
    {
      // delete me
      item->parent->children.Delete(item);
    }
  }
}
*/
void DisplayMissionEditor::RemoveObjectFromBrowser(const EditorObject *obj)
{
  if (!_objectBrowser) return;
  if (!obj) return;

  RString varName = obj->GetArgument("VARIABLE_NAME");

  CTreeItem *item = NULL;

  FindItemWithData func(varName);
  if (_objectBrowser->GetRoot()->ForEachItem(func))
    item = func._item;

  if (!item) return;

  // children now point to my parent
  RecreateChildren(item, item->parent);

  // update object visibility
  SetAllObjectVisibility func2(&_ws);
  item->parent->ForEachItem(func2);

  // delete me
  item->parent->children.Delete(item);
}

void DisplayMissionEditor::UpdateObjectBrowser(const EditorObject *obj, bool updateVisibility, bool expand) 
{
  if (!_objectBrowser) return;
  if (!obj) return;

  RString varName = obj->GetArgument("VARIABLE_NAME");

  CTreeItem *item = NULL;
  FindItemWithData func(varName);
  if (_objectBrowser->GetRoot()->ForEachItem(func))
    item = func._item;

  // tree filtering
  bool isFiltered = _treeFilter > 0;
  if (isFiltered)
  {        
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(&_vars);
    GameVarSpace local(_map->GetVariables(), false);
    gstate->BeginContext(&local); // local space

    gstate->VarLocal("_map");
    gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());

    gstate->VarLocal("_filter");
    gstate->VarSet("_filter", (float)_treeFilter, true, false, GWorld->GetMissionNamespace());

    RString editorFilter = obj->GetTreeFilter();
    if (editorFilter.GetLength() > 0)
    {
      if (gstate->EvaluateBool(editorFilter, GameState::EvalContext::_default, GWorld->GetMissionNamespace())) 
        isFiltered = false;
    }

    gstate->EndContext();
    gstate->EndContext();
  }

  if (isFiltered || !obj->IsObjInCurrentOverlay() || obj->GetScope() == EOSHidden || obj->GetScope() == EOSAllNoTree) 
  {
    if (item)
    {
      // children now point to my parent
      RecreateChildren(item, item->parent);

      // delete me
      item->parent->children.Delete(item);
    }
    return;
  }

  // create the item if it does not exist
  if (!item)
  {
    CTreeItem *parentItem = _objectBrowser->GetRoot();

    FindItemWithData func(obj->GetParent());
    if (_objectBrowser->GetRoot()->ForEachItem(func))
      parentItem = func._item;

    // create new item
    item = parentItem->AddChild();
    item->data = varName;    
  }
  
  RString displayName = GetItemDisplayName(obj, varName);
  item->text = displayName;

  // did this item have a parent?
  if (item->parent != _objectBrowser->GetRoot() || obj->GetParent().GetLength() > 0)
  {
    // parentItem will be the new parent
    CTreeItem *parentItem = _objectBrowser->GetRoot();

    if (obj->GetParent().GetLength() != 0)
    {
      FindItemWithData func(obj->GetParent());
      if (_objectBrowser->GetRoot()->ForEachItem(func))
        parentItem = func._item;
    }

    if (item->parent != parentItem)
    {
      // create new item
      CTreeItem *newItem = parentItem->AddChild();
      newItem->text = displayName;
      newItem->data = varName;

      // recreate my subordinates
      RecreateChildren(item, newItem);

      // delete old item
      item->parent->children.Delete(item);
      item = newItem;
    }
  }

  // update object visibility
  if (updateVisibility)
  {
    if (expand) item->parent->Expand(true);
    SetAllObjectVisibility func(&_ws);
    item->parent->ForEachItem(func);
  }
}

void DisplayMissionEditor::UpdateObjectBrowser()
{
  if (!_objectBrowser) return;
  RString lastSelected = _lastSelectedInTree;

  _objectBrowser->RemoveAll();
  CTreeItem *root = _objectBrowser->GetRoot();

  // add root objects
  EditorObject *obj = _ws.GetPrefixObject();

  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&_vars);
  GameVarSpace local(_map->GetVariables(), false);
  gstate->BeginContext(&local); // local space

  gstate->VarLocal("_map");
  gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());

  gstate->VarLocal("_filter");
  gstate->VarSet("_filter", (float)_treeFilter, true, false, GWorld->GetMissionNamespace());

  for (int i=0; i<_ws.NObjects(); i++)
  {
    obj = _ws.GetObject(i);
    if (!obj->IsObjInCurrentOverlay()) continue;
    if (obj->GetScope() == EOSHidden || obj->GetScope() == EOSAllNoTree) continue;
    if (obj->GetParent().GetLength() == 0) AddBrowserObject(_objectBrowser, root, _ws, obj, lastSelected, gstate, _treeFilter > ETFNone);
  }  
  
  gstate->EndContext();
  gstate->EndContext();

  // update object visibility
  SetAllObjectVisibility func(&_ws);
  _objectBrowser->ForEachItem(func);
}

void DisplayMissionEditor::UpdateAttributeBrowser()
{
  if (!_attributeBrowser) return;
  _attributeBrowser->RemoveAll();

  if (!_objectBrowser) return;
  CTreeItem *item = _objectBrowser->GetSelected();
  if (!item) return;
  RString varName = item->data;

  EditorObject *obj = _ws.FindObject(varName);
  if (obj)
  {
    for (int i=0; i<obj->NArguments(); i++)
    {
      const EditorArgument &arg = obj->GetArgument(i);
      if (stricmp(arg.name, "VARIABLE_NAME") != 0)
      {
        const EditorParam *param = obj->GetAllParams().Find(arg.name);
        if (param && param->hidden) continue;

        // don't show empty arguments
        if (arg.value.GetLength() == 0) continue;

        int index = _attributeBrowser->AddString(Format("%s: %s", (const char *)arg.name, (const char *)arg.value));
        _attributeBrowser->SetData(index, arg.name);

        EditorParamSource source = EPSDialog;
        if (param) source = param->source;
        _attributeBrowser->SetValue(index, source);

        PackedColor color = PackedBlack;
        switch (source)
        {
        case EPSPosition:
        case EPSDirection:
          color = PackedColor(Color(0, 1, 0, 1));
          break;
        case EPSLink:
          color = PackedColor(Color(0, 0, 1, 1));
          break;
        case EPSParent:
          color = PackedColor(Color(1, 0, 0, 1));
          break;
        case EPSCounter:
          color = PackedColor(Color(0.5, 0, 0.5, 1));
          break;
        }
        _attributeBrowser->SetSelColor(index, color);
        _attributeBrowser->SetFtColor(index, PackedColorRGB(color, toInt(0.75f * color.A8())));
      }
    }

    //if (_map) _map->SelectObject(obj);
  }
  _attributeBrowser->SetCurSel(0);
}

void DisplayMissionEditor::SetCameraPosition(RString id, bool lockCamera, Vector3 lockPos)
{
  // prefer external camera position on object
  Object *object = FindObject(id);
  if (object)
  {
    if (lockCamera && _mode == EMMap) _camera->ResetTargets();  // clear _resetTargets variable, reqd if in map mode (camera sim won't be running)
    _camera->LookAt(object, lockCamera, lockPos); 
    return;
  }

  EditorObject *obj = _ws.FindObject(id);
  if (!obj) return;

  /*
  RString statement = obj->GetPosition();
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&_vars);
  GameValue result = gstate->Evaluate(statement, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  gstate->EndContext();

  if (result.GetType() != GameArray) return;
  const GameArrayType &array = result;
  if (array.Size() < 2 || array.Size() > 3) return;
  if (array[0].GetType() != GameScalar) return;
  if (array[1].GetType() != GameScalar) return;
  if (array[2].GetType() != GameScalar) return;

  Vector3 pos(array[0], array[2], array[1]);
  */

  Vector3 pos;
  if (!obj->GetEvaluatedPosition(pos,true))
    return;

  // underground?
  float surface = GLandscape->SurfaceYAboveWater(pos.X(),pos.Z());
  if (pos[1] < surface) pos[1] = surface;

  _camera->LookAt(pos); 
}

bool DisplayMissionEditor::SwitchToVehicle(RString id)
{
  if (_miniMap) _miniMap->SetEditorCamera(NULL);
  GWorld->SetCameraEffect
  (
    CreateCameraEffect(_camera, "terminate", CamEffectBack, true)
  );
  _camera->SetDelete();

  Object *obj = FindObject(id);
  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (veh) _playerUnit = veh->PilotUnit();
  if (!_playerUnit) return false;

  _originalPlayer = GWorld->PlayerOn();
  GWorld->SwitchCameraTo(_playerUnit->GetVehicle(), CamInternal, true);

  GWorld->SetPlayerManual(true);
  GWorld->SwitchPlayerTo(_playerUnit->GetPerson());
  GWorld->SetRealPlayer(_playerUnit->GetPerson());    

  if (GWorld->GetEndMode() == EMKilled) GWorld->SetEndMode(EMContinue);

  GWorld->UI()->ResetHUD();

  _enableSimulation = true;

  // disable simulation for all vehicles (except camera vehicle)
  if (!_realTime)
  {
    for (int i=0; i<_proxies.Size(); i++)
    {
      Object *objLink = _proxies[i].object;
      Entity *veh = dyn_cast<Entity>(objLink);
      if (!veh) continue;
      veh->EditorDisableSimulation(veh != _playerUnit->GetPerson() && veh != _playerUnit->GetVehicleIn());
    }
  }

  return true;
}

MissionEditorCamera *DisplayMissionEditor::CameraGet()
{
  return _camera;
}

void DisplayMissionEditor::LockCamera(RString id, Vector3 lockPos)
{  
  if (id.GetLength() > 0)
  {
    if (!EditorCameraActive())
      CameraRestart();
    SetCameraPosition(id, true, lockPos);

    const EditorObject *obj = _ws.FindObject(id);
    if (!obj) return;        

    Vector3 pos;
    if (obj->GetEvaluatedPosition(pos))
    {
      if (_map) _map->CStaticMap::Center(pos, false);
      if (_miniMap) _miniMap->CStaticMap::Center(pos, false);
    }
  }
  SetMode(EM3D);
}

void DisplayMissionEditor::CameraRestart()
{
#ifndef _XBOX
  OnEditorEvent(MERestartCamera);
#endif
  SwitchToCamera();
}

void DisplayMissionEditor::SwitchToCamera()
{
  Object *obj = GWorld->CameraOn();
  if (obj)
  {
    _camera->LookAt(obj); 
  }
  else
  {
    Matrix4 transform;
    // orientation
    transform.SetOrientation(M3Identity);
    // position
    Vector3 pos;
    pos[0] = (Pars >> "CfgWorlds" >> Glob.header.worldname >> "centerPosition")[0];
    pos[2] = (Pars >> "CfgWorlds" >> Glob.header.worldname >> "centerPosition")[1];
    pos[1] = GLandscape->SurfaceYAboveWater(pos.X(),pos.Z()) + 10;
    transform.SetPosition(pos);
    _camera->LookAt(pos); 
  }

  GWorld->SetCameraEffect
  (
    CreateCameraEffect(_camera, "internal", CamEffectBack, true)
  );
  _camera->SetDelete();
  void ShowCinemaBorder(bool show);
  ShowCinemaBorder(false);
  
  _playerUnit = NULL;
  if (_originalPlayer)
  {
    GWorld->SwitchPlayerTo(_originalPlayer);
    _originalPlayer = NULL;
  }
  //GWorld->SetPlayerManual(false);
  GWorld->UI()->ResetHUD();

  if (!_realTime)
  {
    _enableSimulation = false;
    // disable simulation for all vehicles
    for (int i=0; i<_proxies.Size(); i++)
    {
      Object *objLink = _proxies[i].object;
      Entity *veh = dyn_cast<Entity>(objLink);
      if (!veh) continue;
      veh->EditorDisableSimulation(false);
#if _VBS3 //avoid sticking in the air
      veh->OnMoved();
#endif      
    }
  }
  //-!
}

void DisplayMissionEditor::ClipboardCopy(bool isCut)
{
  // copy selected and all subordinates to clipboard
  _clipboard.Clear();
  _ws.CopyObjects(_clipboard); 

  // update position from position script for all objects about to be copied
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&_vars); // editor space
  GameVarSpace local(&_vars, false);
  gstate->BeginContext(&local); // local space

  Vector3 firstPos;
  for (int i=0; i<_clipboard.Size(); i++)
  {
    Vector3 pos;
    EditorObject *obj = _clipboard[i];
    Object *proxy = obj->GetProxyObject();
    if (proxy) 
      pos = proxy->Position();
    else
      obj->GetEvaluatedPosition(pos);

    // store position relative to first object
    if (i == 0)
      firstPos = pos;
    else
      pos = firstPos - pos;

    obj->UpdateArgument("POSITION", Format("[%.5f, %.5f, %.5f]", pos.X(), pos.Z(), pos.Y()), true);
    obj->UpdateAzimuth();
  }

  gstate->EndContext(); // local space
  gstate->EndContext(); // editor space
  //-!

  // remove original objects if this is a cut
    // TODO: look at improving this, sometimes objects are missed?
  if (isCut)
  {
    // delete selected objects    
    RefArray<EditorObject> selected = _ws.GetSelected();
    for (int i=0; i<selected.Size(); i++) 
    {
      if (selected[i])
        DeleteObject(selected[i]);
    }
    RecordAction();
  }   
}

CStaticMapEditor *DisplayMissionEditor::MouseOverMap()
{
  CStaticMapEditor *map = NULL;
  if (_mode == EM3D && (!_miniMap || !_miniMap->IsVisible()))
    return map;

    float mouseX = 0.5 + GInput.cursorX * 0.5; //aspect ratio?
    float mouseY = 0.5 + GInput.cursorY * 0.5;
    IControl *ctrl = GetCtrl(mouseX, mouseY);
    if (
      (ctrl == _map && _map && _map->IsVisible()) || 
      (ctrl == _miniMap && _miniMap && _miniMap->IsVisible())
    )
    map = ctrl == _map ? (CStaticMapEditor *)_map.GetRef() : _miniMap;

  return map;
}

inline float ObjOffset(Object *proxy)
{
  float objOffset = 0;
  if (proxy)
  {
    EntityAI *veh = dyn_cast<EntityAI>(proxy);    
    if (veh)
    {
      LODShape *shape = veh->GetShape();
      if (!veh->Static() && shape)
      {
        const Shape *geom = shape->LandContactLevel();
        if (!geom) geom = shape->GeometryLevel();
        if (geom)
        {
          objOffset = -geom->Min().Y();
        }
        else if (shape->NLevels()>0)
        {
          ShapeUsed level0 = shape->Level(0);
          objOffset = -level0->Min().Y();
        }
        else
        {
          objOffset = -shape->Min().Y();
        }
      }
      else
      {
        Matrix4 trans = proxy->Transform();
        Vector3 pos = trans.Position();
        float surfaceHgt = GLandscape->SurfaceYAboveWater(pos[0],pos[2]);
        trans.SetPosition(Vector3(pos[0],surfaceHgt,pos[2]));
        veh->PlaceOnSurface(trans);
        objOffset = trans.Position()[1] - surfaceHgt;
      }
    }
  }
  return objOffset;
}

void DisplayMissionEditor::ClipboardPaste()
{
  // paste clipboard at mouse position
  if (_clipboard.Size() > 0)
  {
    ClearSelected();

    Vector3 pastePos;

    if (CStaticMapEditor *map = MouseOverMap())
    {
      float x = 0.5 + GInput.cursorX * 0.5; //aspect ratio?
      float y = 0.5 + GInput.cursorY * 0.5;
      pastePos = map->ScreenToWorld(Point2DFloat(x, y));     
      pastePos[1] = GLandscape->RoadSurfaceYAboveWater(pastePos[0],pastePos[2]);
    }
    else
    {
      _camera->GetGroundIntercept(pastePos, false);
    }

    // this assigns new names for all objects and fixes parent and link parameters
    _ws.RenameObjects(_clipboard);

    // find center of all pasted objects
    Vector3 sum = VZero;
    for (int i=1; i<_clipboard.Size(); i++)
    {       
      Vector3 pos;
      if (_clipboard[i]->GetEvaluatedPosition(pos))
        sum += pos;
    }
    Vector3 center = (1.0f / _clipboard.Size()) * sum; center[1] = 0;
    
    // fetch new height
    Vector3 pos;
    if (_clipboard[0]->GetEvaluatedPosition(pos,true))
      center[1] = pos[1] - GLandscape->SurfaceYAboveWater(pos[0], pos[2]);

    pastePos += center;

    Vector3 firstPos;
    for (int i=0; i<_clipboard.Size(); i++)
    {         
      // set to correct position
      Vector3 objPos, newPos;
      RString value("[0,0]");
      if (_clipboard[i]->GetEvaluatedPosition(objPos,true))
      {  
        if (i == 0) 
        {
          Vector3 offset = pastePos - objPos;
          newPos = objPos + offset;
          firstPos = newPos;
        }
        else
          newPos = firstPos - objPos;
        value = Format("[%.5f, %.5f, %.5f]", newPos.X(), newPos.Z(), newPos.Y());
      }

      EditorObject *obj = NULL;
#if _VBS2_MODIFY_WORLD
      if (_clipboard[i]->IsDynamic())
      {
        // create a new proxy object first, based upon the original
        Object *oldProxy = _clipboard[i]->GetProxyObject();
        if (!oldProxy) continue;
        Ref<Entity> newObj = _ws.CreateEntityFromObject(oldProxy);
        Matrix4 trans = newObj->Transform();
        trans.SetPosition(newPos);
        newObj->PlaceOnSurface(trans);
        newObj->SetTransform(trans);
        obj = _ws.AddSelected(newObj);
      }
      else
#endif
        obj = _ws.CopyObject(_clipboard[i]);

      obj->SetParentOverlay(_ws.GetActiveOverlay(), true);

      // update argument - position
      obj->RemoveArgument("POSITION");
      obj->SetArgument("POSITION", value);

#if _VBS2_MODIFY_WORLD
      if (!_clipboard[i]->IsDynamic())
#endif
      {
        // create the object in the world (calls create script, _new == false)
        CreateObject(obj,false,true);
        obj->SetVisibleInTree(_clipboard[i]->IsVisibleInTree());

        Object *proxy = obj->GetProxyObject();
        if (proxy) 
        {
          if (stricmp(obj->GetArgument("SPECIAL"),"NONE"))
          {
            proxy->MoveNetAware(newPos);
            proxy->OnPositionChanged();
            GScene->GetShadowCache().ShadowChanged(proxy);        
            value = Format("[%.5f, %.5f, %.5f]", newPos.X(), newPos.Z(), newPos.Y() - ObjOffset(proxy));
            obj->UpdateArgument("POSITION", value, true);          
          }
        }

        // add the object to the tree
        UpdateObjectBrowser(obj, true);

        // set as selected (not subordinates though)
        _ws.SetSelected(obj,true,false,false,true);
#ifndef _XBOX
        OnEditorEvent(MESelectObject, obj->GetArgument("VARIABLE_NAME"), obj->GetProxyObject());
#endif
        // select in tree, point camera at it if in 2D
        if (i == 0) OnObjectSelected(obj->GetArgument("VARIABLE_NAME"));
      }
    }
    RecordAction();
  }
}

class FindSelectedObjectFunc
{
protected:
  RString _id;
  mutable const CTreeItem *_found;

public:
  FindSelectedObjectFunc(RString id) {_id = id; _found = NULL;}
  const CTreeItem *GetFound() const {return _found;}
  bool operator () (const CTreeItem *item) const
  {
    if (stricmp(item->data, _id) == 0)
    {
      _found = item;
      return true; // stop searching now
    }
    return false;
  }
};

void DisplayMissionEditor::OnObjectSelected(RString id)
{
  if (_objectBrowser)
  {
    FindSelectedObjectFunc func(id);
    _objectBrowser->ForEachItem(func);
    const CTreeItem *found = func.GetFound();
    if (found)
    {
      CTreeItem *ptr = unconst_cast(found);
      _objectBrowser->SetSelected(ptr);
      while (ptr && ptr != _objectBrowser->GetRoot())
      {
        _objectBrowser->Expand(ptr);
        ptr = ptr->parent;
      }
    }
  }

  // since tree filtering was introducted we unfortunately need to manually set 
    // subordinate objects to visible :( - could be made much faster by giving parents
    // links to their subordinates
  for (int i=0; i<_ws.NObjects(); i++)
  {
    EditorObject *obj = _ws.GetObject(i);
    if (!stricmp(obj->GetParent(),id))
      obj->SetVisibleInTree(true);
  }

  if (_mode == EMMap)
    SetCameraPosition(id,_camera->IsLocked());
}

void DisplayMissionEditor::RotateObject(EditorObject *obj, Vector3Par center, float angle)
{
  if (obj)
  {
    if (obj->GetScope() < EOSAllNoTree) return;
    if (obj->IsAnchored()) return;

    Object *proxy = obj->GetProxyObject();
    if (proxy)
    {
      if (proxy->GetAttachedTo())
      {
        if (proxy->GetAttachFlags() == 0 && !obj->GetType()->AllowMoveIfAttached() && !obj->AllowMoveIfAttached())
          return;
        if (!OneProxySelected())
          return;
      }
    }

    const EditorParams &params = obj->GetType()->GetParams();
    for (int i=0; i<params.Size(); i++)
    {
      const EditorParam &param = params[i];
      if (param.source == EPSDirection)
      {        
        float azimut = atof(obj->GetArgument(param.name));
        azimut += (180.0 / H_PI) * angle;
        RString value = Format("%.5f", azimut);
               
        // update argument
        obj->RemoveArgument(param.name);
        obj->SetArgument(param.name, value);

#if _VBS2_MODIFY_WORLD        
        Object *proxy = obj->GetProxyObject();
        if (proxy && obj->IsStaticProxy())
        {
          // don't call update script, rotate manually
          Matrix3 rotY(MRotationY, -HDegree(azimut));
#if _VBS2 //for attached objects update _attachedTrans
          if (proxy->IsAttached())
            proxy->_attachedTrans = rotY;
          else 
#endif
          proxy->SetOrient(rotY);
        }
        else
#endif
          UpdateObject(obj,true);
      }
      else if (param.source == EPSPosition)
      {  
        Vector3 curPos;
        if (obj->GetEvaluatedPosition(curPos))
        {  
          Vector3 dir = curPos - center;
          Matrix3 rot(MRotationY, -angle);
          dir = rot * dir;

          Vector3 position = center + dir;
          Vector3 offset = position - curPos;          
          offset[1] = 0;  // no movement in vertical plane when rotating
          OnObjectMoved(obj, offset);
        }
      }
    }
    if (proxy && proxy->GetAttachedTo())
    {
      Entity *veh = dyn_cast<Entity>(proxy);
      if ((veh && veh->IsEditorSimulationDisabled()) || !_enableSimulation)
        proxy->CalcAttachedPosition(1);
    }
  }

  UpdateAttributeBrowser();
}

static float GetMinY(LODShape *shape)
{
  if (!shape) return 0;
  const Shape *geom = shape->LandContactLevel();
  if (!geom) geom = shape->GeometryLevel();
  if (geom)
  {
    return geom->Min().Y();
  }
  else if (shape->NLevels()>0)
  {
    ShapeUsed level0 = shape->Level(0);
    return level0->Min().Y();
  }
  else
  {
    return shape->Min().Y();
  }
}

bool DisplayMissionEditor::OneProxySelected()
{
  RefArray<EditorObject> selectedObjs = _ws.GetSelected();
  int n = selectedObjs.Size();
  if (n == 1) return true;
  
  OLinkArray(Object) proxies;
  for (int i=0; i<n; i++)
  {
    Object *proxy = selectedObjs[i]->GetProxyObject();
  
    int j; for (j=0; j<proxies.Size(); j++)
      if (proxies[j] == proxy) break;

    if (j == proxies.Size()) proxies.Add(proxy);
  }

  return proxies.Size() == 1;
}

// executes the user-defined updateposition code for the dragged item
void DisplayMissionEditor::OnObjectMoved(EditorObject *obj, Vector3Par offset)
{
  if (obj)
  {
    if (obj->GetScope() < EOSAllNoTree) return;
    if (offset.X() == 0.0f && offset.Y() == 0.0f && offset.Z() == 0.0f)
      return;

    if (obj->IsAnchored()) return;

    _movingEdObj = true;

    // offset
    const EditorParams &params = obj->GetType()->GetParams();
    for (int i=0; i<params.Size(); i++)
    {
      const EditorParam &param = params[i];
      if (param.source == EPSPosition)
      {
        // get position
        GameValue result = obj->GetArgumentValue(param.name);
        if (result.GetNil() || result.GetType() != GameArray) continue;

        const GameArrayType &array = result;
        int n = array.Size();
        if (n != 2 && n != 3) continue;
        if (array[0].GetType() != GameScalar) continue;
        if (array[1].GetType() != GameScalar) continue;
        float x = array[0];
        float y = 0;
        float z = array[1];
        if (n == 3)
        {
          if (array[2].GetType() != GameScalar) continue;
          y = array[2];
        }
        
        // calculate new position
        Vector3 newPos = Vector3(x, y, z) + offset;

        // update position of proxy
        // look for proxy for this object
        Object *proxy = obj->GetProxyObject();
        if (proxy)
        {
          // already moved?
          bool alreadyMoved = false;
          for (int i=0; i<_movedProxies.Size(); i++)
            if (_movedProxies[i] == proxy) {alreadyMoved = true; break;}

          if (alreadyMoved)
          {
            newPos = proxy->Position();
          }
          else
          {
            _movedProxies.Add(proxy);

            Person *soldier = dyn_cast<Person>(proxy);
            if (soldier)
              soldier->DisableDamageFromObj(5.0f);
            newPos = proxy->Position() + offset;

            // move local unit
            Entity *veh = dyn_cast<Entity>(proxy);
            if (!veh)
            {
              // static object (object is in landscape, streamed) - this should not occur
              GlobalShowMessage(500,"error: %s is static",proxy->GetDebugName().Data());
              GScene->GetLandscape()->MoveObject(proxy,newPos);
            }
            else
            {
              Matrix4 trans = proxy->Transform();             

              Vector3 oldPos = proxy->WorldPosition();

              if (proxy->GetAttachedTo())
              {
                if (obj->AllowMoveIfAttached() || obj->GetType()->AllowMoveIfAttached() || proxy->GetAttachFlags() > 0)
                {
                  if (obj->IsSelected() && OneProxySelected())
                  {
                    Vector3 modelPos(VFastTransform, proxy->GetAttachedTo()->GetInvTransform(), proxy->Position() + offset);
                    proxy->SetAttachedOffset(modelPos);

                    Entity *veh = dyn_cast<Entity>(proxy);
                    if ((veh && veh->IsEditorSimulationDisabled()) || !_enableSimulation)
                      proxy->CalcAttachedPosition(1);
                  }
                }
              }
              else
              {
                proxy->Move(Vector3(100,100,100)); // so PlaceOnSurface works correctly

                // ground heights at new and old position            
                float ghOldPos = GLandscape->SurfaceYAboveWater(oldPos.X(), oldPos.Z());
                float ghNewPos = GLandscape->SurfaceYAboveWater(newPos.X(), newPos.Z());
                float ghNewPosSurface = GLandscape->SurfaceY(newPos.X(), newPos.Z());

                newPos[1] += ghNewPos - ghOldPos; // maintain height above ground
                trans.SetPosition(newPos);

                if (obj->StayAboveGround())
                {
                  float oht = newPos.Y() + GetMinY(proxy->GetShape());
                  if (oht <= ghNewPosSurface + 0.3f && offset.Y() <= 0)
                    veh->PlaceOnSurfaceUnderWater(trans); // never clamp to surface of water
                  else if (oht > ghNewPosSurface + 0.3f)
                    trans.SetUpAndDirection(VUp,trans.Direction());
                }
                
                // prevent PlaceOnSurface from overriding XZ position
                trans.SetPosition(Vector3(newPos.X(),trans.Position().Y(),newPos.Z()));            

                proxy->Move(trans);
                proxy->OnPositionChanged();

                newPos = trans.Position();
              }
            }
            GScene->GetShadowCache().ShadowChanged(proxy);
          }
        }

        RString value;
        if (newPos.Y() == 0)
          value = Format("[%.5f, %.5f]", newPos.X(), newPos.Z());
        else
          value = Format("[%.5f, %.5f, %.5f]", newPos.X(), newPos.Z(), newPos.Y() - ObjOffset(proxy));

        // update argument - position
        obj->RemoveArgument(param.name);
        obj->SetArgument(param.name, value);

        // update object position
        UpdatePositionObject(obj,offset);

        break;        
      }
    }
  }
  UpdateAttributeBrowser();
}

void DisplayMissionEditor::OnLinkEditted(EditorObject *obj, RString name, RString value, float key, CStaticMapEditor *map)
{
  if (!obj) return;

  EditorObject *linked = _ws.FindObject(value);
  if ((linked && linked->GetScope() < EOSLinkTo) || obj == linked) return;

  const EditorParam *param = NULL;

  // if name is empty, search for first avilable link parameter beloing to obj 
  // that matches the type of the linked object
  if (name.GetLength() == 0 && key >= 0)
  {
    if (!linked)
    {
      // don't know which link to cancel! so do nothing
        // TODO: come up with a better solution (maybe a right click - clear links option?)
      return;
    }
    else
    {
      RString typeWanted = linked->GetType()->GetName();
      const EditorParams &params = obj->GetAllParams();
      int nParams = params.Size();

      // search for link or parent parameter that matches typeWanted
      for (int i=0; i<nParams; i++)
      {
        if (params[i].shortcutKey != key) continue;
        if (stricmp(typeWanted,params[i].type) == 0)
        {
          name = params[i].name;
          param = &params[i];
          break;
        }
      }    
      if (param && !IsMenuItemActive(param->activeMode, obj, (CStaticMapEditor *)_map.GetRef(), IsRealTime()))
        param = NULL;
      if (!param) return;
    }
  }
  else
  {
    param = obj->GetAllParams().Find(name);

    // check type of linked object
    if (linked && stricmp(linked->GetType()->GetName(), param->type) != 0)
    {
      // wrong type
      linked = NULL;
    }
  }

  Assert(param->source == EPSLink || param->source == EPSParent);

  // link to a position? allow processing to continue
  if (param->source == EPSLink && stricmp(param->type, "position") == 0)
    linked = obj;
  
  if (stricmp(param->subtype, "single") == 0)
  {
    if (linked)
    {
      obj->RemoveArgument(name);
      obj->SetArgument(name, value);
    }
  }
  else if (stricmp(param->subtype, "noneOrSingle") == 0)
  {
    obj->RemoveArgument(name);
    if (linked)
    {
      obj->SetArgument(name, value);
    }
  }
  else
  {
    if (linked)
    {
      bool found = false;
      for (int i=0; i<obj->NArguments(); i++)
      {
        const EditorArgument &arg = obj->GetArgument(i);
        if (stricmp(arg.name, name) == 0 && stricmp(arg.value, value) == 0)
        {
          obj->RemoveArgument(i);
          found = true;
          break;
        }
      }
      if (!found) obj->SetArgument(name, value);
    }
    else
    {
      obj->RemoveArgument(name);
    }
  }

  RString id = obj->GetArgument("VARIABLE_NAME");

  UpdateObject(obj);
  RecordAction();
  UpdateAttributeBrowser();

  // ensure obj was not deleted (in UpdateObject)
  obj = _ws.FindObject(id);
  if (obj)
  {
    LogF("[p] link %s | name %s",obj->GetArgument("VARIABLE_NAME").Data(),name.Data());
    UpdateObjectBrowser(obj,true);

    // is the link shortcut key still held down?
    const EditorParams &params = obj->GetAllParams();
    int nParams = params.Size();
    for (int i=0; i<nParams; i++)
    {
      if ((params[i].source != EPSLink && params[i].source != EPSParent && params[i].source != EPSContext) || params[i].shortcutKey < 0) continue;
      if (GInput.keys[params[i].shortcutKey] > 0)
      {
        if (map) 
          map->EditLink(linked, params[i].name, params[i].shortcutKey);
        else
          EditLink(linked, params[i].name, params[i].shortcutKey);
        break;
      }
    }      
  }
}

void DisplayMissionEditor::LookAtPosition(Vector3 pos)
{
  _camera->LookAt(pos);
  if (_map) _map->CStaticMap::Center(pos, false);
  if (_miniMap) _miniMap->CStaticMap::Center(pos, false);
}
     
void DisplayMissionEditor::OnMenuSelected(MenuItem *item)
{
  switch (item->_cmd)
  {
  case ECSetCameraPosition:
    {
      const IAttribute *attr = item->_attributes[attrObject];
      Assert(attr);
      RString id = GetAttrString(attr);
      if (id.GetLength() > 0)
      {
        if (!EditorCameraActive())
          CameraRestart();
        SetCameraPosition(id,false);

        const EditorObject *obj = _ws.FindObject(id);
        if (!obj) break;
        
        Vector3 pos;
        if (obj->GetEvaluatedPosition(pos))
        {
          if (_map) _map->CStaticMap::Center(pos, false);
          if (_miniMap) _miniMap->CStaticMap::Center(pos, false);          
        }
      }
      else
      {
        const IAttribute *iattr = item->_attributes[attrPosition];
        const AttrVector *attr = static_cast<const AttrVector *>(iattr);
        Assert(attr);
        Vector3 pos = attr->GetValue();
        _camera->LookAt(pos);
        if (_map) _map->CStaticMap::Center(pos, false);
        if (_miniMap) _miniMap->CStaticMap::Center(pos, false);          
      }
      SetMode(EM3D);
    }
    break;
  case ECLockCamera:
    {
      const IAttribute *attr = item->_attributes[attrObject];
      Assert(attr);
      RString id = GetAttrString(attr);
      LockCamera(id, Vector3());
    }
    break;    
  case ECSwitchToVehicle:
    {
      const IAttribute *attr = item->_attributes[attrObject];
      Assert(attr);
      RString id = GetAttrString(attr);
      EditorMode oldMode = _mode;
      SetMode(EMPreview);
      if (!SwitchToVehicle(id))
      {
        SetMode(oldMode);
      }
    }
    break;
  case ECSnapToSurface:
    {
      int n = _ws.GetSelected().Size();
      for (int h=0; h<n; h++)
      {        
        EditorObject *obj = _ws.GetSelected()[h];
        if (!obj) continue;

        Object *proxy = obj->GetProxyObject();
        if (!proxy) continue;

        if (proxy->GetAttachedTo() && !OneProxySelected())
          continue;

        Vector3 pos = proxy->Position();
        pos[1] = 0;

        bool found = false;
        for (int i=0; i<3; i++)
        {
          CollisionBuffer collision;
          ObjectCollision(collision, proxy, pos, (ObjIntersect)i);

          float minT=1e10; int n = collision.Size();
          for (int j=0; j<n; j++)
          {
            CollisionInfo &info = collision[j];
            if (minT > info.under && info.object)
            {
              found = true;
              pos = info.pos;
              minT=info.under;
            }
          }
        }

        Matrix4 trans = proxy->Transform();
        EntityAI *veh = dyn_cast<EntityAI>(proxy);

        if (!found) 
          pos[1] = GLandscape->SurfaceYAboveWater(pos[0],pos[2]);

        float offset = 0;
        if (found)
        {
          offset = ObjOffset(proxy);
          pos[1] += offset;
        }

        trans.SetPosition(pos);
        if (veh)
        {
          if (!found)
          {
            veh->PlaceOnSurface(trans);
            trans.SetPosition(Vector3(pos[0],trans.Position().Y(),pos[2]));
          }
          if (!veh->IsLocal()) veh->EditorDisableSimulation(false);
        }
        
        proxy->MoveNetAware(trans);
        proxy->OnPositionChanged();
        GScene->GetShadowCache().ShadowChanged(proxy);        
        RString value = Format("[%.5f, %.5f, %.5f]", pos.X(), pos.Z(), pos.Y() - offset);
        
        // update all objects that share the same proxy
        for (int i=0; i<_proxies.Size(); i++)
          if (_proxies[i].object == proxy)
            _proxies[i].edObj->UpdateArgument("POSITION", value, true);

        proxy->CalcAttachedPosition(1);
      }
    }
    break;
  case ECEditObject:
    {
      const IAttribute *attr = item->_attributes[attrObject];
      Assert(attr);
      RString id = GetAttrString(attr);
      ShowEditObject(id, true);
    }
    break;
  case ECDeleteObject:
    {
      const IAttribute *attr = item->_attributes[attrObject];
      Assert(attr);
      RString id = GetAttrString(attr);
      DeleteObject(id);
    }
    break;
  case ECNewObject:
    {
      const IAttribute *iattr = item->_attributes[attrPosition];
      const AttrVector *attr = static_cast<const AttrVector *>(iattr);
      Assert(attr);
      Vector3 pos = attr->GetValue();
      if (_onShowNewObject.GetLength()>0)
      {
        GameState *state = GWorld->GetGameState();

        GameValue posValue = state->CreateGameValue(GameArray);
        GameArrayType &posArray = posValue;
        posArray.Realloc(3);
        posArray.Add(pos[0]);
        posArray.Add(pos[2]);

        if (_activeMap == 2) // 3D
          posArray.Add(pos[1]);
        else  // 2D, either map
          posArray.Add(GLandscape->SurfaceYAboveWater(pos[0],pos[2]));

        GameVarSpace vars(false);
        state->BeginContext(&vars);

        Ref<CStaticMapEditor> map = (CStaticMapEditor *)_map.GetRef();    
        if (_activeMap == EMTMiniMap)
          map = _miniMap; 
        
        state->VarSetLocal("_map", CreateGameControl(map), true);
        state->VarSetLocal("_pos",posArray,true);
        state->EvaluateMultiple(_onShowNewObject, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
        state->EndContext();
      }
      else
        ShowNewObject(pos);
    }
    break;
  case ECCenterMap:
    if (_map)
    {
      SetMode(EMMap);
      const IAttribute *attr = item->_attributes[attrObject];
      Assert(attr);
      RString id = GetAttrString(attr);
      if (id.GetLength() > 0)
      {
        const EditorObject *obj = _ws.FindObject(id);
        if (!obj) break;
        
        Vector3 pos;
        if (obj->GetEvaluatedPosition(pos))
        {
          if (_map) _map->CStaticMap::Center(pos, false);
          if (_miniMap) _miniMap->CStaticMap::Center(pos, false);
          _camera->LookAt(pos);
        }
      }
      else
      {
        const IAttribute *iattr = item->_attributes[attrPosition];
        const AttrVector *attr = static_cast<const AttrVector *>(iattr);
        Assert(attr);
        Vector3 pos = attr->GetValue();
        if (_map) _map->CStaticMap::Center(pos, false);
        if (_miniMap) _miniMap->CStaticMap::Center(pos, false);
        _camera->LookAt(pos);
      }
    }
    break;
  case ECEditLink:
    {
      const IAttribute *attr = item->_attributes[attrObject];
      Assert(attr);
      RString id = GetAttrString(attr);
      EditorObject *obj = _ws.FindObject(id);
      if (!obj) break;
      
      attr = item->_attributes[attrArgument];
      Assert(attr);
      RString name = GetAttrString(attr);      

      if (_activeMap == EMTMainMap)
        _map->EditLink(obj, name, -1, true);     
      else if (_activeMap == EMTMiniMap)
        _miniMap->EditLink(obj, name, -1, true);     
      else
        EditLink(obj, name, -1, true);   
    }
    break;
  case ECExecuteScript:
    {
      const IAttribute *attr = item->_attributes[attrObject];
      Assert(attr);
      RString id = GetAttrString(attr);
      EditorObject *obj = _ws.FindObject(id);
      if (!obj) break;
      
      attr = item->_attributes[attrArgument];
      Assert(attr);
      RString name = GetAttrString(attr);      

      // retrieve code
      const EditorParam *param = obj->GetAllParams().Find(name);
      if (param && param->subtype.GetLength() > 0)
      {
        RString code = param->subtype;

        // insert parameters into a line of code
        code = obj->WriteScript(code);

        // execute code
        GameState *gstate = GWorld->GetGameState();
        gstate->BeginContext(_map->GetVariables()); // editor space
        GameVarSpace local(_map->GetVariables(), false);
        gstate->BeginContext(&local); // local space

        // initialization of variables
        gstate->VarLocal("_map"); // access to scripting functions on the workspace
        gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());

        gstate->VarLocal("_edObj"); // access to scripting functions on the workspace
        gstate->VarSet("_edObj", obj->GetArgument("VARIABLE_NAME"), true, false, GWorld->GetMissionNamespace());

        GameValue result = gstate->Evaluate(obj->GetArgument("VARIABLE_NAME"), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
        if (!result.GetNil())
        {
          gstate->VarLocal("_this");
          gstate->VarSet("_this", result, true, false, GWorld->GetMissionNamespace());
          gstate->Execute(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
        }

        gstate->EndContext(); // local space
        gstate->EndContext(); // editor space
      }
    }
    break;
  case ECMenuAdditionalItem:
    {
      const IAttribute *attr = item->_attributes[attrObject];
      Assert(attr);
      RString code = GetAttrString(attr);

      GameState *gstate = GWorld->GetGameState();
      gstate->BeginContext(_map->GetVariables()); // editor space
      GameVarSpace local(_map->GetVariables(), false);
      gstate->BeginContext(&local); // local space

      gstate->VarLocal("_map"); // access to scripting functions on the workspace
      gstate->VarSet("_map", CreateGameControl(_map), true, false, GWorld->GetMissionNamespace());
      gstate->VarLocal("_selected");
      RefArray<EditorObject> selectedObjs = _ws.GetSelected(); // selected objects - TODO: make an array     
      if (selectedObjs.Size() > 0)
        gstate->VarSet("_selected", GameValueExt(unconst_cast(selectedObjs[0]->GetProxyObject()),GameValExtObject), false, false, GWorld->GetMissionNamespace());        
      else
        gstate->VarSet("_selected", GameValueExt((Object *)NULL), false, false, GWorld->GetMissionNamespace());        

      gstate->VarLocal("_camera");
      gstate->VarSet("_camera", GameValueExt(_camera,GameValExtObject), true, false, GWorld->GetMissionNamespace());        

      gstate->Execute(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      gstate->EndContext(); // local space
      gstate->EndContext(); // editor space
    }
    break;
  case ECMenuFileLoad:
    OnButtonClicked(IDC_EDITOR_LOAD);
    break;
  case ECMenuFileSave:
    OnButtonClicked(IDC_EDITOR_SAVE);
    break;
  case ECMenuFileClear:
    OnButtonClicked(IDC_EDITOR_CLEAR);
    break;
  case ECMenuFileEditBriefing:
    CreateChild(new DisplayEditBriefing(this,&_editorBriefing));
    break;    
  case ECMenuFileRestart:
    OnButtonClicked(IDC_EDITOR_RESTART);
    break;
  case ECMenuFileOptions:
    CreateChild(new DisplayOptions(this, false, false));
    break;    
  case ECMenuFilePreview:
    OnButtonClicked(IDC_EDITOR_PREVIEW);
    break;
  case ECMenuFileExit:
    OnButtonClicked(IDC_CANCEL);
    break;
  case ECMenuView2D:
    SetMode(EMMap);
    break;
  case ECMenuView3D:
    SetMode(EM3D);
    break;   
  }  
  _menuTypeActive = EMNone;
}

void DisplayMissionEditor::NewOverlay(ConstParamEntryPtr entry)
{
  if (!entry) return;
  _overlayType = entry;

  if (_hasChanged && _ws.GetActiveOverlay()) 
  {
    EditorOverlay *overlay = _ws.GetActiveOverlay();
    if (overlay->IsTemplate())
    {
      AskForOverlaySave(IDD_MSG_CREATE_OVERLAY);
      return;
    }
  }     
  
  OnChildDestroyed(IDD_MSG_CREATE_OVERLAY, IDC_CANCEL);    // skip straight to create dialog      
}

void DisplayMissionEditor::LoadOverlay(ConstParamEntryPtr entry)
{
  _overlayType = entry;

  if (_hasChanged && _ws.GetActiveOverlay()) 
  {
    EditorOverlay *overlay = _ws.GetActiveOverlay();
    if (overlay->IsTemplate())
    {
      AskForOverlaySave(IDD_MSG_LOAD_OVERLAY);
      return;
    }
  }     
  
  OnChildDestroyed(IDD_MSG_LOAD_OVERLAY, IDC_CANCEL);    // skip straight to create dialog 
}

void DisplayMissionEditor::SaveOverlay()
{
  EditorOverlay *overlay = _ws.GetActiveOverlay();
  if (!overlay)
  {
    GWorld->CreateWarningMessage(LocalizeString("STR_EDITOR_ERROR_NO_OVERLAY"));  
    return;
  }      
  _ws.SaveOverlay(overlay);
  GWorld->CreateWarningMessage(LocalizeString("STR_EDITOR_PROMPT_OVERLAY_SAVED"));  
}

void DisplayMissionEditor::ClearOverlay(bool commit, bool close)
{
  bool valid = _ws.GetActiveOverlay() ? _ws.GetActiveOverlay()->IsTemplate() : false;
  if (!valid)
  {
    GWorld->CreateWarningMessage(LocalizeString("STR_EDITOR_ERROR_NO_OVERLAY"));  
    return;
  }   
  if (commit)
    AskForOverlaySave(IDD_MSG_COMMIT_OVERLAY,LocalizeString("STR_EDITOR_PROMPT_OVERLAY_SAVE_COMMIT"));
  else if (close)
    if (_hasChanged)
      AskForOverlaySave(IDD_MSG_CLOSE_OVERLAY,LocalizeString("STR_EDITOR_PROMPT_OVERLAY_SAVE_CLOSE"));
    else
      OnChildDestroyed(IDD_MSG_CLOSE_OVERLAY, IDC_CANCEL);    // skip straight to create dialog 
  else
    AskForOverlaySave(IDD_MSG_CLEAR_OVERLAY,LocalizeString("STR_EDITOR_PROMPT_OVERLAY_SAVE_CLEAR"));
}

void DisplayMissionEditor::RemoveContextMenu()
{
  _menuTypeActive = EMNone;
  ControlsContainer::RemoveContextMenu();
  if (_reShowLegend)
  {
    if (_map) _map->_showCountourInterval = true; 
    _reShowLegend = false;
  }
}

static int CmpMenus(const UserMenu *a, const UserMenu *b)
{
  return sign(b->priority - a->priority);
}

int DisplayMissionEditor::AddMenu(RString text, float priority)
{
  // last hard-coded button
  Control *lastCtrl = dynamic_cast<Control *>(GetCtrl(IDC_EDITOR_VIEW));
  if (!lastCtrl) return -1;

  // dynamically create new button background based upon Menu_Button_BG template
  ConstParamEntryPtr ptr = (Pars >> "RscDisplayMissionEditor").FindEntry("Menu_Button_BG");
  if (!ptr) return -1;

  int type = *ptr >> "type";

  Control *background = OnCreateCtrl(type, -1, *ptr);
  if (!background) return -1;

  // dynamically create new button based upon Menu_Button template
  ptr = (Pars >> "RscDisplayMissionEditor").FindEntry("Menu_Button");
  if (!ptr) return -1;

  type = *ptr >> "type";

  Control *btn = OnCreateCtrl(type, IDC_EDITOR_USER, *ptr);
  if (!btn) return -1;

  // set action
  char str[256];
  sprintf(str,"((findDisplay %d) displayCtrl %d) createMenu %d",_idd,IDC_MAP,_userMenus.Size());
  switch (type)
  {
    case CT_BUTTON:
      {
        CButton *s = static_cast<CButton *>(btn);
        s->SetAction(str);
        s->SetText(text);
        break;
      }
    case CT_ACTIVETEXT:
      {
        CActiveText *s = static_cast<CActiveText *>(btn);
        s->SetAction(str);
        s->SetText(text);
      }
    default:
      return -1;
  }

  _controlsForeground.Add(background);
  _controlsForeground.Add(btn);

  int index = _userMenus.Size();

  _userMenus.Add(UserMenu(btn,background,priority,index));

  // sort menu buttons by priority
  QSort(_userMenus.Data(),_userMenus.Size(),CmpMenus);

  // position new button
  float x = lastCtrl->X() + lastCtrl->W();
  float y = lastCtrl->Y();
  float w = lastCtrl->W();
  float h = lastCtrl->H();

  for (int i=0; i<_userMenus.Size(); i++, x += w)
  {
    _userMenus[i].ctrl->SetPos(x,y,w,h);
    _userMenus[i].bg->SetPos(x,y,_userMenus[i].bg->W(),_userMenus[i].bg->H());
  }

  return index;
}

void DisplayMissionEditor::CreateMenu(int index)
{
  if (index < 0 || index >= _userMenus.Size())
    return;
  ParamEntryPar cls = Pars >> "RscDisplayMissionEditor" >> "Menu_User";

  // find menu matching index
  int i; for (i=0; i<_userMenus.Size(); i++)
    if (index == _userMenus[i].index)
      break;

  Control *ctrl = _userMenus[i].ctrl;
  if (!ctrl) return;

  CreateContextMenu(
    ctrl->X(), ctrl->Y() + ctrl->H(), IDC_EDITOR_MENU_USER,
    cls, CCMContextEditor(RString(), NULL, VZero, EMUser + i)); // pass position in _userMenus array
}

void DisplayMissionEditor::RemoveMenuItem(int index, RString text)
{
  if (index < 0 && text.GetLength() == 0) return;

  for (int i=0; i<_menuItems.Size(); i++)
    if (_menuItems[i].index == index || stricmp(_menuItems[i].text,text) == 0)
    {
      _menuItems.Delete(i);
      break;
    }
}

static int CmpMenuItems(const AdditionalMenuItem *a, const AdditionalMenuItem *b)
{
  return sign(b->priority - a->priority);
}

void DisplayMissionEditor::UpdateMenuItem(int index, RString text, RString command) 
{
  for (int i=0; i<_menuItems.Size(); i++)
  {
    if (_menuItems[i].index == index)
    {
      if (text.GetLength() > 0) _menuItems[i].text = text;
      if (command.GetLength() > 0) _menuItems[i].command = command;  
      break;
    }
  }
}

int DisplayMissionEditor::NMenuItems(RString text, int index)
{
  int n = 0;
  for (int i=0; i<_menuItems.Size(); i++)
  {
    if (
      (stricmp(text,"file") == 0 && _menuItems[i].type == EMFile) ||
      (stricmp(text,"view") == 0 && _menuItems[i].type == EMView) ||
      _menuItems[i].type - EMUser == index
    )
      n++;
  }  
  return n;
}

int DisplayMissionEditor::GetNextMenuItemIndex()
{
  int id = 0;
  for (int i=0; i<_menuItems.Size(); i++)
    if (_menuItems[i].index >= id) id = _menuItems[i].index + 1;
  return id;
}

int DisplayMissionEditor::AddMenuItem(RString type, RString text, RString command, int index, float priority, bool assignPriority)
{
  AdditionalMenuItem menuItem;

  if (type.GetLength() > 0)
  {
    if (stricmp(type,"file") == 0)
      menuItem.type = EMFile;
    else if (stricmp(type,"view") == 0)
      menuItem.type = EMView;
    else
      return -1;
  }
  else
  {
    if (index < 0 || index >= _userMenus.Size())
      return -1;
    menuItem.type = EMUser + index;
  }
  menuItem.text = text;
  menuItem.command = command;  
  menuItem.priority = priority;
  
  int id = 0; float lowestPri = priority;
  for (int i=0; i<_menuItems.Size(); i++)
  {
    if (_menuItems[i].index >= id) id = _menuItems[i].index + 1;
    if (_menuItems[i].priority <= lowestPri) lowestPri = _menuItems[i].priority - 1;
  }
  menuItem.index = id;

  if (assignPriority) menuItem.priority = lowestPri;

  _menuItems.Add(menuItem);

  // sort menu buttons by priority
  QSort(_menuItems.Data(),_menuItems.Size(),CmpMenuItems);

  return id;
}

void DisplayMissionEditor::InsertAdditionalMenuItems(Menu &menu, int menuType)
{
  // menuType is the position in _userMenus that we are creating a menu for, not index
    // so find index (we need to do this because _userMenus may be ordered non-sequentially)
  if (menuType >= EMUser)
    menuType = EMUser + _userMenus[menuType - EMUser].index;

  for (int i=0; i<_menuItems.Size(); i++)
  {
    AdditionalMenuItem &menuItem = _menuItems[i];
    if (menuItem.type == menuType)
    {
      MenuItem *item;
      if (menuItem.text.GetLength() == 0)
        item = new MenuItem("", 0, CMD_SEPARATOR);
      else
      {
        item = new MenuItem(menuItem.text, 0, ECMenuAdditionalItem);
        item->SetAttribute("object", menuItem.command, EditorMenuAttributeTypes);        
      }
      menu.AddItem(item);
    }
  }
}

bool DisplayMissionEditor::OnMenuCreated(Menu &menu, float x, float y, int idc, CCMContext &ctx)
{
  CCMContextEditor &ctxEd = static_cast<CCMContextEditor &>(ctx);
  _menuTypeActive = ctxEd.type;
  switch (ctxEd.type)
  {
  case EMPopup:
    {
      RString id = ctxEd.name;
      CStaticMapEditor *map = ctxEd.map;

      _activeMap = _miniMap && map == _miniMap ? EMTMiniMap : EMTMainMap;
      if (_activeMap == EMTMainMap && _mode == EM3D) _activeMap = EMTNoMap;
      
      EditorObject *obj = _ws.FindObject(id);
      if (!obj)
      {
        // right click on empty map or 3D space
        if (map)
        {
          MenuItem *item = new MenuItem(LocalizeString("STR_EDITOR_MENU_CTX_NEW_OBJ"), 0, ECNewObject);
          IAttribute *iattr = item->AccessAttribute("position", EditorMenuAttributeTypes);
          AttrVector *attr = static_cast<AttrVector *>(iattr);
          attr->SetValue(ctxEd.position);
          menu.AddItem(item);

          if (_allow3DMode)
          {
            item = new MenuItem("", 0, CMD_SEPARATOR);
            menu.AddItem(item);

            if (map->IsVisible()) // right click in 2D
            {
              MenuItem *item = new MenuItem(LocalizeString("STR_EDITOR_MENU_CTX_SET_CAM_POS"), 0, ECSetCameraPosition);
              item->SetAttribute("object", RString(), EditorMenuAttributeTypes);
              IAttribute *iattr = item->AccessAttribute("position", EditorMenuAttributeTypes);
              AttrVector *attr = static_cast<AttrVector *>(iattr);
              Vector3 pos = ctxEd.position;
              pos[1] = GLandscape->SurfaceYAboveWater(pos[0],pos[2]);
              attr->SetValue(pos);
              menu.AddItem(item);
            }

            if (!map->IsVisible()) // right click in 3D
            {
              MenuItem *item = new MenuItem(LocalizeString("STR_EDITOR_MENU_CTX_CENTER_MAP"), 0, ECCenterMap);
              item->SetAttribute("object", RString(), EditorMenuAttributeTypes);
              IAttribute *iattr = item->AccessAttribute("position", EditorMenuAttributeTypes);
              AttrVector *attr = static_cast<AttrVector *>(iattr);
              Vector3 pos;
              _camera->GetGroundIntercept(pos,false);
              attr->SetValue(pos);
              menu.AddItem(item);
            }
          }
          return true;
        }
        return false;
      }

      MenuItem *item;

      if (obj->GetType()->ContextMenuFollowsProxy() && map)
      {        
        EditorObject *proxyOwner = FindProxyOwner(obj->GetProxyObject());
        if (proxyOwner)
        {
          obj = proxyOwner;
          id = obj->GetArgument("VARIABLE_NAME");
        }
      };

      if (obj->GetScope() >= EOSDrag)
      {
        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_CTX_EDIT_OBJ"), 0, ECEditObject);
        item->SetAttribute("object", id, EditorMenuAttributeTypes);
        menu.AddItem(item);
      }

      if (obj->GetScope() >= EOSDrag)
      {
        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_CTX_DEL_OBJ"), 0, ECDeleteObject);
        item->SetAttribute("object", id, EditorMenuAttributeTypes);
        menu.AddItem(item);
      }

      if (obj->GetScope() >= EOSDrag)
      {
        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_CTX_SNAP_SURFACE"), 0, RString(), ECSnapToSurface);
        menu.AddItem(item);
      }

      const EditorParams &params = obj->GetAllParams();
      int n = params.Size();       
      for (int i=0; i<n; i++)
      {
        const EditorParam &param = params[i];
        if (param.source != EPSLink && param.source != EPSParent && param.source != EPSContext) continue;
        if (param.hidden) continue;

        // link?
        if ((param.source == EPSLink || param.source == EPSParent) && obj->GetScope() >= EOSLinkFrom)// && map && map->IsVisible())
        {
          if (!IsMenuItemActive(param.activeMode, obj, map,_realTime,_allow3DMode))
            continue;
          RString name = param.name;
          if (param.description.GetLength() == 0)  
            item = new MenuItem(Format(LocalizeString("STR_EDITOR_MENU_CTX_EDIT_LINK"), (const char *)name), 0, ECEditLink);
          else
            item = new MenuItem(param.description, 0, ECEditLink);
          item->SetAttribute("object", id, EditorMenuAttributeTypes);
          item->SetAttribute("argument", name, EditorMenuAttributeTypes);
          menu.AddItem(item);
        }

        // context menu?
        if (param.source == EPSContext)// && obj->GetScope() >= EOSDrag)
        {
#if !_ENABLE_3D_LINKING
          if (obj->GetType()->ContextMenuFollowsProxy() && !map)
            continue;
#endif
          if (!IsMenuItemActive(param.activeMode, obj, (CStaticMapEditor *)_map.GetRef(),_realTime,_allow3DMode,ctxEd.position == VZero))
            continue;

          // seperator?
          if (param.subtype.GetLength() == 0) 
          {
            item = new MenuItem("", 0, CMD_SEPARATOR);
            menu.AddItem(item);    
            continue;
          }

          // context items can be "editorObject" or "code"
          EditorCommands ec = ECEditLink;
          if (stricmp(param.type, "code") == 0)
            ec = ECExecuteScript;
#if !_ENABLE_3D_LINKING
          //if (_mode != EMMap && ec == ECEditLink) continue;
          if ((!map || (map && !map->IsVisible())) && ec == ECEditLink) continue;
#endif
          RString name = param.name;
          if (param.description.GetLength() == 0)  
            item = new MenuItem(Format("%s", (const char *)name), 0, ec);
          else
            item = new MenuItem(param.description, 0, ec);
          item->SetAttribute("object", id, EditorMenuAttributeTypes);
          item->SetAttribute("argument", name, EditorMenuAttributeTypes);
          menu.AddItem(item);
        }
      }

      // Fill menu
      Object *object = FindObject(id);
      if (object && _allow3DMode)
      {
        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_CTX_SET_CAM_POS"), 0, ECSetCameraPosition);
        item->SetAttribute("object", id, EditorMenuAttributeTypes);
        menu.AddItem(item);

        if (_realTime)
        {
          item = new MenuItem(LocalizeString("STR_EDITOR_MENU_CTX_LOCK_CAM"), 0, ECLockCamera);
          item->SetAttribute("object", id, EditorMenuAttributeTypes);
          menu.AddItem(item);
        }

        if (obj->GetScope() >= EOSDrag && _realTime)
        {
          EntityAI *veh = dyn_cast<EntityAI>(object);
          if (veh && veh->PilotUnit() && !veh->IsDeadSet())
          {
            item = new MenuItem(LocalizeString("STR_EDITOR_MENU_CTX_SWITCH_VEH"), 0, ECSwitchToVehicle);
            item->SetAttribute("object", id, EditorMenuAttributeTypes);
            menu.AddItem(item);
          }
        }

        if (map && !map->IsVisible())
        {
          if (menu._items.Size() > 0)
          {
            item = new MenuItem("", 0, CMD_SEPARATOR);
            menu.AddItem(item);
          }

          MenuItem *item = new MenuItem(LocalizeString("STR_EDITOR_MENU_CTX_CENTER_MAP"), 0, ECCenterMap);
          item->SetAttribute("object", id, EditorMenuAttributeTypes);
          menu.AddItem(item);
        }
      }
    }
    break;
  case EMFile:
    {
      if (_map && _map->_showCountourInterval) {_map->_showCountourInterval = false; _reShowLegend = true;}
      MenuItem *item;
      bool allow = true;
      if (_ws.GetActiveOverlay())
      {
         if (_ws.GetActiveOverlay()->IsTemplate())
           allow = false;
      }
      if (_allowFileOperations && allow)
      {
        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_FILE_LOAD"), 0, ECMenuFileLoad);
        menu.AddItem(item);

        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_FILE_SAVE"), 0, ECMenuFileSave);
        menu.AddItem(item);
    
        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_FILE_CLEAR"), 0, ECMenuFileClear);
        menu.AddItem(item);

        item = new MenuItem("", 0, CMD_SEPARATOR);
        menu.AddItem(item); 

        item = new MenuItem(LocalizeString("STR_VBS2_EDITOR_EB_TITLE"), 0, ECMenuFileEditBriefing);
        menu.AddItem(item);

        item = new MenuItem("", 0, CMD_SEPARATOR);
        menu.AddItem(item);       

        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_FILE_RESTART"), 0, ECMenuFileRestart);
        menu.AddItem(item);

        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_FILE_PREVIEW"), 0, ECMenuFilePreview);
        menu.AddItem(item);

        item = new MenuItem("", 0, CMD_SEPARATOR);
        menu.AddItem(item);    
      }

      item = new MenuItem(LocalizeString("STR_CA_MAIN_OPTIONS"), 0, ECMenuFileOptions);
      menu.AddItem(item);

      if (!_ws.GetActiveOverlay())
      {
        item = new MenuItem("", 0, CMD_SEPARATOR);
        menu.AddItem(item); 

        InsertAdditionalMenuItems(menu,EMFile);

        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_FILE_EXIT"), 0, ECMenuFileExit);
        menu.AddItem(item);      
      }
    }
    break;
  case EMView:
    {
      if (_map && _map->_showCountourInterval) {_map->_showCountourInterval = false; _reShowLegend = true;}

      MenuItem *item = new MenuItem(LocalizeString("STR_EDITOR_MENU_VIEW_2D"), 0, ECMenuView2D);
      menu.AddItem(item);

      if (_allow3DMode)
      {
        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_VIEW_3D"), 0, ECMenuView3D);
        menu.AddItem(item);
      }

       InsertAdditionalMenuItems(menu,EMView);
    }
    break;
  default:
    {
      if (_map && _map->_showCountourInterval) {_map->_showCountourInterval = false; _reShowLegend = true;}
      // user menu - index passed as idc
      int index = ctxEd.type - EMUser;  // index here equals position in _userMenus array
      if (index < 0 || index >= _userMenus.Size())
        break;
      InsertAdditionalMenuItems(menu,ctxEd.type);
    }
    break;
  }

  return menu._items.Size() != 0;
}

void DisplayMissionEditor::Destroy()
{
  GWorld->SetCamUseNVG(false);
  GWorld->OnCameraChanged();

  ControlsContainer::Destroy();
}

// drawing commands

#define NOTHING GameValue()

IControl *GetControl(GameValuePar oper);

GameValue CStaticMapEditor::GetObjectArgument(RString id, RString name)
{
  if (!_ws) return RString();
  
  EditorObject *obj = _ws->FindObject(id);
  if (!obj) return RString();

  // return an array if type is multiple
  const EditorParams &params = obj->GetAllParams(); 
  for (int i=0; i<params.Size(); i++)
  {
    const EditorParam &param = params[i];
    if (stricmp(param.name, name) == 0 && stricmp(param.subtype, "multiple") == 0) 
    {
      // construct return array
      GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
      GameArrayType &array = value;

      for (int i=0; i<obj->NArguments(); i++)
        if (obj->GetArgument(i).name == name)
          array.Add(obj->GetArgument(i).value);

      return value;
    }
  }

  return obj->GetArgument(name);
}

GameValue MapEvalObjectArgument(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }
  RString value = map->GetObjectArgument(array[0], array[1]);
  if (value.GetLength() == 0) return NOTHING;

  // allows access to editor variables (for use in returning VARIABLE_NAME)
  state->BeginContext(map->GetVariables());
  GameValue result = state->Evaluate(value, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  state->EndContext();

  return result;
}

GameValue MapGetVariable(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  RString name = oper2;
  GameVarSpace *vars = map->GetVariables();
  if (vars)
  {
    GameValue var;
    if (vars->VarGet(name, var)) return var;
  }
  return NOTHING;
}

GameValue MapSetVariable(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }

  // set the argument in the variable space
  GameVarSpace *vars = map->GetVariables();
  RString varName = array[0];
  vars->VarLocal(varName);
  vars->VarSet(varName, array[1], false);

  return NOTHING;
}

GameValue MapIsShow3DIcons(const GameState *state, GameValuePar oper1)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return false;
  return map->IsShow3DIcons();
}

GameValue MapShowLegend(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameBool)
  {
    state->TypeError(GameBool, oper2.GetType());
    return NOTHING;
  }

  map->ShowLegend(oper2);

  return NOTHING;
}

GameValue MapAllowFileOperations(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameBool)
  {
    state->TypeError(GameBool, oper2.GetType());
    return NOTHING;
  }

  map->AllowFileOperations(oper2);

  return NOTHING;
}

GameValue MapAllow3DMode(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameBool)
  {
    state->TypeError(GameBool, oper2.GetType());
    return NOTHING;
  }

  map->Allow3DMode(oper2);

  return NOTHING;
}

GameValue MapEnable3DLinking(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameBool)
  {
    state->TypeError(GameBool, oper2.GetType());
    return NOTHING;
  }

  map->Enable3DLinking(oper2);

  return NOTHING;
}

GameValue MapExitOnMapKey(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameBool)
  {
    state->TypeError(GameBool, oper2.GetType());
    return NOTHING;
  }

  map->ExitOnMapKey(oper2);

  return NOTHING;
}

GameValue MapShow3DIcons(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameBool)
  {
    state->TypeError(GameBool, oper2.GetType());
    return NOTHING;
  }

  map->Show3DIcons(oper2);

  return NOTHING;
}

GameValue MapMoveObjectToEnd(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  EditorObject *obj = ws->FindObject(oper2);
  if (obj)
    ws->MoveObjectToEnd(obj);

  return NOTHING;
}

GameValue MapDeleteEditorObject(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  map->DeleteObjectFromEditor(oper2);
  return NOTHING;
}

GameValue MapGetObjectArgument(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return RString();

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return RString();
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return RString();
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return RString();
  }

  return map->GetObjectArgument(array[0], array[1]);
}


GameValue MapGetMode(const GameState *state, GameValuePar oper1)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const EnumName *eMode = GetEnumNames((EditorMode)map->GetMode());
  return eMode[map->GetMode()].name;
}

GameValue MapSetMode(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  GameStringType str = oper2;
  EditorMode editorMode = NEditorModes;
  const EnumName *names = GetEnumNames(editorMode);
  for (int i=0; names[i].IsValid(); i++)
  {
    if (stricmp(names[i].name, str) == 0)
    {
      editorMode = (EditorMode)names[i].value;
      break;
    }    
  }
  if (editorMode == NEditorModes) return NOTHING;

  map->SetMode(editorMode,true);
  return NOTHING;
}

GameValue MapSetEventHandler(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  for (int i=0; i<n; i++)
    if (array[i].GetType() != GameString)
    {
      state->TypeError(GameString, array[i].GetType());
      return NOTHING;
    }

  map->SetEditorEventHandler(array[0], array[1]);
  return NOTHING;
}

GameValue MapAddMenu(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 1 && n != 2)
  {
    state->SetError(EvalDim, array.Size(), 1);
    return NOTHING;
  }
  
  // text
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  RString text = array[0];

  // priority
  float priority = 0.0f;
  if (n > 1)
  {
    if (array[1].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[1].GetType());
      return NOTHING;
    }
    priority = array[1];
  }

  return (float)map->AddMenu(text,priority);
}

#ifndef _XBOX
inline const GameConfigType &GetConfig(GameValuePar oper)
{
  Assert(oper.GetType() == GameConfig);
  return static_cast<GameDataConfig *>(oper.GetData())->GetConfig();
}
#endif

GameValue MapOverlayNew(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() == GameConfig)
  {
    const GameConfigType &config = GetConfig(oper2);
    if (!config.entry) return NOTHING;
    
    ConstParamEntryPtr name = config.entry->FindEntry("name");
    if (!name) return NOTHING;

    ConstParamEntryPtr types = config.entry->FindEntry("editorObjects");
    if (!types) return NOTHING;

    map->NewOverlay(config.entry);
  }
  return NOTHING;
}

GameValue MapOverlayLoad(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() == GameConfig)
  {
    const GameConfigType &config = GetConfig(oper2);
    if (!config.entry) return NOTHING;
    
    ConstParamEntryPtr name = config.entry->FindEntry("name");
    if (!name) return NOTHING;

    ConstParamEntryPtr types = config.entry->FindEntry("editorObjects");
    if (!types) return NOTHING;

    map->LoadOverlay(config.entry);
  }
  return NOTHING;
}

GameValue MapOverlaySave(const GameState *state, GameValuePar oper1)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  map->SaveOverlay();

  return NOTHING;
}

GameValue MapOverlayClear(const GameState *state, GameValuePar oper1)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  map->ClearOverlay(false);

  return NOTHING;
}

GameValue MapOverlayClose(const GameState *state, GameValuePar oper1)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  map->ClearOverlay(false,true);

  return NOTHING;
}

GameValue MapOverlayCommit(const GameState *state, GameValuePar oper1)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  map->ClearOverlay(true);

  return NOTHING;
}

GameValue MapUpdateObjectTree(const GameState *state, GameValuePar oper1)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  LogF("[p] WARNING: UpdateObjectBrowser() called from script! This is very slow.");

  map->UpdateObjectBrowser();

  return NOTHING;
}

GameValue MapUpdateObjectTree2(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString && oper2.GetType() != GameArray)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  RString id; 
  bool updateVis = false; 
  bool expand = true;

  if (oper2.GetType() == GameString)
  {
    id = oper2;
  }
  else
  {
    const GameArrayType &array = oper2;
    if (array.Size() > 0)
    {
      if (array[0].GetType() == GameString)
      {
        id = array[0];
        if (array.Size() > 1)
        {
          if (array[1].GetType() == GameBool)
          {
            updateVis = array[1];
            if (array.Size() > 2)
            {
              if (array[2].GetType() == GameBool)
              {
                expand = array[2];
              }
            }
          }
        }
      }
    }
  }

  map->UpdateObjectBrowser(id, updateVis, expand);

  return NOTHING;
}

/// convert game value to group
static AIGroup *GetGroup( GameValuePar oper )
{
  if (oper.GetNil()) return NULL;
  if( oper.GetType()==GameGroup )
  {
    return static_cast<GameDataGroup *>(oper.GetData())->GetGroup();
  }
  return NULL;
}

GameValue MapObjGetSettings(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return value;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return value;
  }
  RString edObj = oper2;

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return value;

  EditorObject *obj = ws->FindObject(edObj);
  if (obj)
  {
    array.Resize(3);
    array[0] = obj->IsAnchored();
    array[1] = obj->StayAboveGround();
    array[2] = obj->AllowMoveIfAttached();
  }

  return value;
}

GameValue MapObjSetSettings(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n < 2 || n > 4)
  {
    state->SetError(EvalDim, array.Size(), 4);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  RString edObj = array[0];
  EditorObject *obj = ws->FindObject(edObj);
  if (!obj) return NOTHING;

  if (array[1].GetType() != GameBool)
  {
    state->TypeError(GameBool, array[1].GetType());
    return NOTHING;
  }
  bool isAnchored = array[1];
  obj->SetAnchored(isAnchored);

  if (n > 2)
  {
    if (array[2].GetType() != GameBool)
    {
      state->TypeError(GameBool, array[2].GetType());
      return NOTHING;
    }
    bool stayAboveGround = array[2];
    obj->SetStayAboveGround(stayAboveGround);

    if (n > 3)
    {
      if (array[3].GetType() != GameBool)
      {
        state->TypeError(GameBool, array[3].GetType());
        return NOTHING;
      }
      bool allowMoveIfAttached = array[3];
      obj->SetAllowMoveIfAttached(allowMoveIfAttached);
    }
  }

  return NOTHING;
}

Object *GetObject( GameValuePar oper );

GameValue MapCreateUnitEdObj(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return RString();

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return RString();
  }

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 3 && n != 5)
  {
    state->SetError(EvalDim, array.Size(), 3);
    return RString();
  }
  if (array[0].GetType() != GameObject)
  {
    state->TypeError(GameObject, array[0].GetType());
    return RString();
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return RString();
  }
  if (array[2].GetType() != GameString)
  {
    state->TypeError(GameString, array[2].GetType());
    return RString();
  }

  Transport *trans = NULL;
  RString vehEdObj = RString();
  if (n > 3)
  {
    if (array[3].GetType() != GameObject)
    {
      state->TypeError(GameObject, array[3].GetType());
      return RString();
    }    
    if (array[4].GetType() != GameString)
    {
      state->TypeError(GameString, array[4].GetType());
      return RString();
    }
    trans = dyn_cast<Transport>(GetObject(array[3]));
    vehEdObj = array[4];
  }

  Person *person = dyn_cast<Person>(GetObject(array[0]));
  RString grpEdObj = array[1];
  RString leaderEdObj = array[2];

  return map->CreateUnitEdObj(person, trans, vehEdObj, grpEdObj, leaderEdObj);
}

GameValue MapLockCamera(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 1 && n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }

  Vector3 pos;
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }

  // todo: fix rel pos (AVRS)
  if (n > 1)
  {
    if (array[1].GetType() != GameArray)
    {
      state->TypeError(GameArray, array[1].GetType());
      return NOTHING;
    }

    GameArrayType t = array[1];
    for (int i=0; i<t.Size(); i++)
    {
      if (t[i].GetType() != GameScalar)
      {
        state->TypeError(GameScalar, t[i].GetType());
        return NOTHING;
      }
    }
    pos = Vector3(t[0],t[1],t[2]);
  };

  map->LockCamera(array[0],pos);
  return NOTHING;
}

GameValue MapCreateVehEdObj(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return RString();

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return RString();
  }

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return RString();
  }

  if (array[0].GetType() != GameObject)
  {
    state->TypeError(GameObject, array[0].GetType());
    return RString();
  }

  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return RString();
  }

  Transport *trans = dyn_cast<Transport>(GetObject(array[0]));
  if (!trans) return RString();

  RString grpEdObj;
  grpEdObj = array[1];

  return map->CreateVehEdObj(trans,grpEdObj);
}

GameValue MapCreateWaypointEdObjs(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 3)
  {
    state->SetError(EvalDim, array.Size(), 3);
    return NOTHING;
  }
  if (array[0].GetType() != GameGroup)
  {
    state->TypeError(GameGroup, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }
  if (array[2].GetType() != GameString)
  {
    state->TypeError(GameString, array[2].GetType());
    return NOTHING;
  }

  AIGroup *aGroup = GetGroup(array[0]);
  if (!aGroup) return NOTHING;

  RString grpEdObj = array[1];
  RString unitEdObj = array[2];

  map->CreateWaypointEdObjs(aGroup, grpEdObj, unitEdObj);

  return NOTHING;
}

GameValue MapImportAllGroups(const GameState *state, GameValuePar oper1)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  map->ImportAllGroups();

  return NOTHING;
}

GameValue MapGetSelectedObjects(const GameState *state, GameValuePar oper1)
{
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &array = value;

  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return value;

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return value;

  for (int i=0; i<ws->NObjects(); i++)
  {
    EditorObject *obj = ws->GetObject(i);
    if (obj && obj->IsSelected())
      array.Add(obj->GetArgument("VARIABLE_NAME"));
  }

  return value;
}

static bool GetRelPos( const GameState *state, Vector3 &ret, GameValuePar oper )
{
  Object *obj = GetObject(oper);
  if (obj)
  {
    ret = obj->Position();
    // convert to relative position?
    ret[1] -= GLandscape->SurfaceYAboveWater(ret.X(),ret.Z());
    return true;
  }
  const GameArrayType &array = oper;
  if (array.Size() < 2 || array.Size() > 3)
  {
    if (state) state->SetError(EvalDim,array.Size(),3);
    return false;
  }
  if (array[0].GetType() != GameScalar)
  {
    if (state) state->TypeError(GameScalar,array[0].GetType());
    return false;
  }
  if (array[1].GetType() != GameScalar
  )
  {
    if (state) state->TypeError(GameScalar,array[1].GetType());
    return false;
  }

  float x = array[0];
  float z = array[1];
  float y = 0;
  if (array.Size()==3)
  {
    if (array[2].GetType() != GameScalar)
    {
      if (state) state->TypeError(GameScalar,array[2].GetType());
      return false;
    }
    y=array[2];
  }
  ret = Vector3(x,y,z);
  return true;
}

bool GetPos( const GameState *state, Vector3 &ret, GameValuePar oper );

GameValue MapLookAtPosition(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }

  Vector3 pos;

  const GameArrayType &array = oper2;
  if (array.Size() == 0)
    pos = map->GetCenter();
  else if (!GetPos(state, pos, oper2))
    return NOTHING;

  pos[1] = GLandscape->SurfaceYAboveWater(pos.X(), pos.Z());

  map->LookAtPosition(pos);

  return NOTHING;
}

GameValue MapCreateMenu(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameScalar)
  {
    state->TypeError(GameScalar, oper2.GetType());
    return NOTHING;
  }
  float index = oper2;
  map->CreateMenu((int)index);
  return NOTHING;
}

GameValue MapRemoveMenuItem(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameScalar)
  {
    state->TypeError(GameScalar, oper2.GetType());
    return NOTHING;
  }

  float findex = oper2;
  int index = (int)findex;

  map->RemoveMenuItem(index);

  return NOTHING;
}

GameValue MapRemoveMenuItemString(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  map->RemoveMenuItem(-1,oper2);

  return NOTHING;
}

GameValue MapIgnoreObjects(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  for (int i=0; i<array.Size(); i++)
    if (array[i].GetType() != GameObject)
    {
      state->TypeError(GameObject, array[i].GetType());
      return NOTHING;
    }

  MissionEditorCamera *camera;
  camera = map->CameraGet();
  if (!camera) return NOTHING;

  camera->ClearIgnoreObjects();
  for (int i=0; i<array.Size(); i++)
    camera->AddIgnoreObject(GetObject(array[i]));

  return NOTHING;
}

GameValue MapRemoveDrawLinks(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  for (int i=0; i<n; i++)
    if (array[i].GetType() != GameString)
    {
      state->TypeError(GameString, array[i].GetType());
      return NOTHING;
    }

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  RString strFrom = array[0];
  EditorObject *from = NULL;

  for (int i=0; i<ws->NObjects(); i++)
  {
    EditorObject *obj = ws->GetObject(i);
    if (obj)
      if (stricmp(obj->GetArgument("VARIABLE_NAME"),strFrom) == 0)
      {
        from = obj;
        break;
      }
  }
  if (!from) return NOTHING;
  ws->RemoveDrawLinks(from,array[1]);
  return NOTHING;
}

bool GetColor(const GameState *state, PackedColor &color, GameValuePar value);

GameValue MapDrawLink(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 5 && n != 6 && n != 7)
  {
    state->SetError(EvalDim, array.Size(), 5);
    return NOTHING;
  }
  for (int i=0; i<4; i++)
    if (array[i].GetType() != GameString)
    {
      state->TypeError(GameString, array[i].GetType());
      return NOTHING;
    }

  float arrowScaleMin = 100;
  if (n == 6)
  {
    if (array[5].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[5].GetType());
      return NOTHING;
    }
    arrowScaleMin = array[5];
  }

  bool showIn3D = true;
  if (n == 7)
  {
    if (array[6].GetType() != GameBool)
    {
      state->TypeError(GameBool, array[6].GetType());
      return NOTHING;
    }
    showIn3D = array[6];
  }

  //[from,to,param type,line type]

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  RString strFrom = array[0];
  EditorObject *from = NULL;

  RString strTo = array[1];
  EditorObject *to = NULL;

  for (int i=0; i<ws->NObjects(); i++)
  {
    EditorObject *obj = ws->GetObject(i);
    if (obj)
      if (stricmp(obj->GetArgument("VARIABLE_NAME"),strFrom) == 0)
      {
        from = obj;
        if (to) break;
      }
      else if (stricmp(obj->GetArgument("VARIABLE_NAME"),strTo) == 0)
      {
        to = obj;
        if (from) break;
      }
  }
  if (!to || !from || to == from) return NOTHING;

  RString lineTypeStr = array[3];
  LineType lineType = LTLine;
  const EnumName *names = GetEnumNames(lineType);
  for (int i=0; names[i].IsValid(); i++)
    if (stricmp(names[i].name, lineTypeStr) == 0)
    {
      lineType = (LineType)names[i].value;
      break;
    }

  PackedColor color;
  if (!GetColor(state, color, array[4])) return NOTHING;

  ws->SetDrawLink(from,to,array[2],lineType,color,arrowScaleMin,showIn3D);

  return NOTHING;
}

GameValue MapNextMenuItemIndex(const GameState *state, GameValuePar oper1)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return 0.0f;
  
  return (float)map->GetNextMenuItemIndex();
}

GameValue MapClearSelected(const GameState *state, GameValuePar oper1)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return 0.0f;
  
  map->ClearSelected();

  return NOTHING;
}

GameValue MapGetNMenuItems(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return 0.0f;

  if (oper2.GetType() != GameString && oper2.GetType() != GameScalar)
  {
    state->TypeError(GameScalar, oper2.GetType());
    return 0.0f;
  }

  float findex = -1;
  RString menuName;
 
  if (oper2.GetType() == GameString)
    menuName = oper2;

  if (oper2.GetType() == GameScalar)
    findex = oper2;

  return (float)map->NMenuItems(menuName,(int)findex);
}

GameValue MapUpdateMenuItem(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2 && n != 3)
  {
    state->SetError(EvalDim, array.Size(), 3);
    return NOTHING;
  }
  // index and text
  if (array[0].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }
  float fIndex = array[0];  
  int index = (int)fIndex;
  RString text = array[1];
  RString command;

  // command
  if (array.Size() > 2)
  {
    if (array[2].GetType() != GameString)
    {
      state->TypeError(GameString, array[2].GetType());
      return NOTHING;
    }
    command = array[2];
  }

  map->UpdateMenuItem(index, text, command);

  return NOTHING;
}

GameValue MapAddMenuItem(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  float value = -1;

  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return value;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return value;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 3 && n != 4)
  {
    state->SetError(EvalDim, array.Size(), 3);
    return value;
  }
  // text and command
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return value;
  }
  if (array[2].GetType() != GameString)
  {
    state->TypeError(GameString, array[2].GetType());
    return value;
  }
  RString text = array[1];  
  RString command = array[2];

  float priority = 0.0f;
  bool assignPriority = true;
  if (n == 4)
  {
    priority = array[3];
    assignPriority = false;
  }  

  // index can be text or index  
  if (array[0].GetType() == GameString)
  {
    RString type = array[0];
    value = (float)map->AddMenuItem(type,text,command,-1,priority,assignPriority);
  }
  else if (array[0].GetType() == GameScalar)
  {
    float index = array[0];
    value = (float)map->AddMenuItem(RString(),text,command,(int)index,priority,assignPriority);
  }
  else 
    state->TypeError(GameString, array[0].GetType());

  return value;
}

static void AssignScope(EditorWorkspace *ws, const EditorObject *parent, EditorObjectScope scope, bool isDefault = false, bool alsoDefault = false)
{
  for (int i=0; i<ws->NObjects(); i++)
  {
    EditorObject *obj = ws->GetObject(i);
    if (stricmp(obj->GetParent(),parent->GetArgument("VARIABLE_NAME")) == 0)
    {
      if (isDefault || alsoDefault)
        obj->SetDefaultScope(scope);
      
      if (!isDefault)
        obj->SetScope(scope);

      AssignScope(ws, obj, scope, isDefault, alsoDefault);
    }
  }
}

GameValue MapGetEditorObjectScopeN(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return RString();

  if (oper2.GetType() != GameString) // condition
  {
    state->TypeError(GameString, oper2.GetType());
    return RString();
  }  

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return RString();

  // find object
  RString id = oper2;
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return RString();

  int scope = obj->GetScope();
  return (float)scope;  
}

GameValue MapGetEditorObjectScope(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return RString();

  if (oper2.GetType() != GameString) // condition
  {
    state->TypeError(GameString, oper2.GetType());
    return RString();
  }  

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return RString();

  // find object
  RString id = oper2;
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return RString();

  const EnumName *eScope = GetEnumNames((EditorObjectScope)obj->GetScope());
  return eScope[obj->GetScope()].name;  
}

GameValue MapSetDefaultScope(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 3)
  {
    state->SetError(EvalDim, array.Size(), 3);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }
  if (array[2].GetType() != GameBool) // assign same scope to subordinates?
  {
    state->TypeError(GameString, array[2].GetType());
    return NOTHING;
  }

  RString objName = array[0];
  bool subordinatesAlso = array[2];

  // determine scope
  GameStringType str = array[1];
  EditorObjectScope scope = NEditorObjectScopes;
  const EnumName *names = GetEnumNames(scope);
  for (int i=0; names[i].IsValid(); i++)
  {
    if (stricmp(names[i].name, str) == 0)
    {
      scope = (EditorObjectScope)names[i].value;
      break;
    }    
  }
  if (scope == NEditorObjectScopes) return NOTHING;

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  EditorObject *obj = ws->FindObject(objName);
  obj->SetDefaultScope(scope);
  //LogF("[p] ERROR defaultscope | %s | %s",objName.Data(),str.Data());
  if (subordinatesAlso)
    AssignScope(ws,obj,scope,true);

  return NOTHING;
}

GameValue MapGetEditorTypeScopeN(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  RString typeName = oper2;

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  EditorObjectType *type = ws->FindObjectType(typeName);
  if (type)
    return (float)type->GetScope();

  return NOTHING;
}

GameValue MapSetEditorTypeScope(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }
  RString typeName = array[0];

  // determine scope
  GameStringType str = array[1];
  EditorObjectScope scope = NEditorObjectScopes;
  const EnumName *names = GetEnumNames(scope);
  for (int i=0; names[i].IsValid(); i++)
  {
    if (stricmp(names[i].name, str) == 0)
    {
      scope = (EditorObjectScope)names[i].value;
      break;
    }    
  }
  if (scope == NEditorObjectScopes) return NOTHING;

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  EditorObjectType *type = ws->FindObjectType(typeName);
  if (type)
    type->SetScope(scope);
  //LogF("[p] ERROR typescope | %s | %s",typeName.Data(),str.Data());
  return NOTHING;
}

GameValue MapSetEditorObjectScope(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 5 && n != 6)
  {
    state->SetError(EvalDim, array.Size(), 5);
    return NOTHING;
  }
  if (array[0].GetType() != GameArray)  // array of objects
  {
    state->TypeError(GameArray, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameString) // editor object type
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }  
  if (array[2].GetType() != GameString) // condition
  {
    state->TypeError(GameString, array[2].GetType());
    return NOTHING;
  }  
  if (array[3].GetType() != GameString) // editor object scope
  {
    state->TypeError(GameString, array[3].GetType());
    return NOTHING;
  }
  if (array[4].GetType() != GameBool) // assign same scope to subordinates?
  {
    state->TypeError(GameString, array[4].GetType());
    return NOTHING;
  }
  bool setAsDefault = true;
  if (n > 5)
  {
    if (array[5].GetType() != GameBool) // assign same scope to subordinates?
    {
      state->TypeError(GameString, array[4].GetType());
      return NOTHING;
    }
    setAsDefault = array[5];
  }
  // check objects
  const GameArrayType &objs = array[0];
  for (int i=0; i<objs.Size(); i++)
  {
    if (objs[i].GetType() != GameObject && objs[i].GetType() != GameString)
    {
      state->TypeError(GameObject, objs[i].GetType());
      return NOTHING;
    }
  }

  // determine scope
  GameStringType str = array[3];
  EditorObjectScope scope = NEditorObjectScopes;
  const EnumName *names = GetEnumNames(scope);
  for (int i=0; names[i].IsValid(); i++)
  {
    if (stricmp(names[i].name, str) == 0)
    {
      scope = (EditorObjectScope)names[i].value;
      break;
    }    
  }
  if (scope == NEditorObjectScopes) return NOTHING;

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  RString type = array[1];
  RString condition = array[2];
  bool includeSubordinates = array[4];

  const GameArrayType *allObjs = &objs;

  // process all editor objects? compile an array of strings
  GameArrayType array2;
  if (objs.Size() == 0)
  {
    for (int i=0; i<ws->NObjects(); i++)
    {
      EditorObject *obj = ws->GetObject(i);
      if (obj)
        array2.Add(obj->GetArgument("VARIABLE_NAME"));
    }
    allObjs = &array2;
  }

  for (int i=0; i<allObjs->Size(); i++)
  {
    EditorObject *obj = NULL;
    if (allObjs->Get(i).GetType() == GameObject)
    {
      // condition
      GameVarSpace local(state->GetContext(), false);        
      state->BeginContext(&local);
      state->VarSetLocal("_x", allObjs->Get(i), true);
      if (condition.GetLength() == 0 || state->EvaluateBool(condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace()))
        obj = map->FindEditorObject(allObjs->Get(i),type);
      state->EndContext();             
    }
    else if (allObjs->Get(i).GetType() == GameString)
    {
      obj = ws->FindObject(allObjs->Get(i)); 
      if (obj && stricmp(obj->GetType()->GetName(),type) == 0 || type.GetLength() == 0)
      {
        if (condition.GetLength() > 0 && stricmp(condition,"true") != 0)
        {
          // condition
          GameVarSpace local(state->GetContext(), false);        
          state->BeginContext(&local);
          state->BeginContext(map->GetVariables());
          GameValue result = state->Evaluate(RString(allObjs->Get(i)), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
          state->EndContext();
          state->VarSetLocal("_x", result, true);
          if (!state->EvaluateBool(condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace()))
            obj = NULL;
          state->EndContext();   
        }
      }
    }
    if (obj)
    {
      if (stricmp(obj->GetType()->GetName(),type) == 0 || type.GetLength() == 0)
      {          
        obj->SetScope(scope);
        //LogF("[p] ERROR objscope | %s | %d | setAsDefault %d",obj->GetArgument("VARIABLE_NAME").Data(),(int)scope,setAsDefault);
        if (setAsDefault) obj->SetDefaultScope(scope);
        map->UpdateObjectBrowser(obj);
        if (includeSubordinates)
          AssignScope(ws,obj,scope,false,setAsDefault);
      }
    }
  }
  return NOTHING;
}

GameValue MapUnselectEditorObject(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  // find the editor object
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  // find object
  RString id = oper2;
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return NOTHING;

  map->UnselectObject(obj);

  return NOTHING;
}

GameValue MapSelectEditorObject(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  // find the editor object
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  // find object
  RString id = oper2;
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return NOTHING;

  GInput.keys[DIK_LCONTROL] = true;
  map->SelectObject(obj);
  GInput.keys[DIK_LCONTROL] = false;

  return NOTHING;
}

// return the proxy object for given editor object
GameValue MapGetObjectProxy(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  // find the editor object
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  // find object
  RString id = oper2;
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return NOTHING;

  // return proxy
  Object *proxy = map->FindObject(id);
  if (proxy) 
    return GameValueExt(proxy);

  return NOTHING;
}

GameValue MapSetVisibleIfTreeCollapsed(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameBool)
  {
    state->TypeError(GameBool, array[1].GetType());
    return NOTHING;
  }

  // find the editor object
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  // find object
  RString id = array[0];
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return NOTHING;

  bool visible = array[1];
  obj->SetVisibleIfTreeCollapsed(visible);

  return NOTHING;
}

// set the proxy object for a given editor object
GameValue MapSetObjectProxy(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameObject)
  {
    state->TypeError(GameObject, array[1].GetType());
    return NOTHING;
  }

  // find the editor object
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  // find object
  RString id = array[0];
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return NOTHING;

  // set proxy
  Object *object = static_cast<const GameDataObject *>(array[1].GetData())->GetObject();
  if (object)
  {
    map->UpdateProxy(object,obj);
    obj->SetProxyObject(object);
  }
  else
  {
    map->DeleteProxy(obj);
    obj->SetProxyObject(NULL);
  }

  return NOTHING;
}

RString GetVehicleIcon(RString name);

GameValue MapDrawIconUpdate(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 9 && n != 10)
  {
    state->SetError(EvalDim, array.Size(), 10);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  // find the editor object
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  // find object
  RString id = array[0];
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return NOTHING;

  // identifier
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }
  RString name = array[1];

  // color
  if (array[2].GetType() != GameArray)
  {
    state->TypeError(GameArray, array[2].GetType());
    return NOTHING;
  }  
  PackedColor color;
  bool GetColor(const GameState *state, PackedColor &color, GameValuePar value);
  if (!GetColor(state, color, array[2])) return NOTHING;

  // offset
  if (array[3].GetType() != GameArray)
  {
    state->TypeError(GameArray, array[3].GetType());
    return NOTHING;
  }
  const GameArrayType &posArray = array[3];
  if (posArray.Size() != 3)
  {
    state->SetError(EvalDim, posArray.Size(), 3);
    return NOTHING;
  }
  for (int i=0; i<3; i++)
  {
    if (posArray[i].GetType() != GameScalar)
    {
      state->TypeError(GameScalar,posArray[i].GetType());
      return NOTHING;
    }
  }
  Vector3 offset;
  offset[0] = posArray[0];
  offset[1] = posArray[2];  
  offset[2] = posArray[1];
  //-!

  // width
  if (array[4].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, array[4].GetType());
    return NOTHING;
  }
  float width = array[4];

  // height
  if (array[5].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, array[5].GetType());
    return NOTHING;
  }
  float height = array[5];

  // maintain size? (don't grow/shrink as map scale changes?)
  if (array[6].GetType() != GameBool && array[6].GetType() != GameScalar)
  {
    state->TypeError(GameBool, array[6].GetType());
    return NOTHING;
  }
  bool maintainSize = false;
  float minScale = -1;
  if (array[6].GetType() == GameBool)
    maintainSize = array[6];
  else
    minScale = array[6];

  // direction
  if (array[7].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, array[7].GetType());
    return NOTHING;
  }
  float angle = array[7];

  // shadow?
  if (array[8].GetType() != GameBool)
  {
    state->TypeError(GameBool, array[8].GetType());
    return NOTHING;
  }
  bool shadow = array[8];

  // text
  RString text;
  if (n > 9)
  {
    if (array[9].GetType() != GameString)
    {
      state->TypeError(GameString, array[9].GetType());
      return NOTHING;
    }
    text = array[9];
  }

  int nIcons = obj->NIcons();
  for (int i=0; i<nIcons; i++)
  {
    EditorObjectIcon &icon = obj->GetIcon(i);
    if (stricmp(icon.id,name) == 0) 
    {
      icon.color = color;
      icon.offset = offset;
      icon.angle = (H_PI / 180.0f) * angle;
      icon.width = width;
      icon.height = height;
      icon.maintainSize = maintainSize;
      icon.minScale = minScale;
      icon.shadow = shadow;
      icon.text = text;
    }
  }

  return NOTHING;
}

GameValue MapRemoveDrawIcon(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  // find the editor object
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  // find object
  RString id = array[0];
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return NOTHING;

  // identifier
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }
  RString name = array[1];

  obj->RemoveIcon(name);

  return NOTHING;
}

GameValue MapSetDrawIcon(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 11 && n != 13 && n != 14 && n != 15)
  {
    state->SetError(EvalDim, array.Size(), 11);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  // find the editor object
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  // find object
  RString id = array[0];
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return NOTHING;

  // icon name
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }
  RString iconName = array[1];

  // color
  if (array[2].GetType() != GameArray)
  {
    state->TypeError(GameArray, array[2].GetType());
    return NOTHING;
  }  
  PackedColor color;
  bool GetColor(const GameState *state, PackedColor &color, GameValuePar value);
  if (!GetColor(state, color, array[2])) return NOTHING;

  // offset
  if (array[3].GetType() != GameArray)
  {
    state->TypeError(GameArray, array[3].GetType());
    return NOTHING;
  }
  const GameArrayType &posArray = array[3];
  if (posArray.Size() != 3)
  {
    state->SetError(EvalDim, posArray.Size(), 3);
    return NOTHING;
  }
  for (int i=0; i<3; i++)
  {
    if (posArray[i].GetType() != GameScalar)
    {
      state->TypeError(GameScalar,posArray[i].GetType());
      return NOTHING;
    }
  }
  Vector3 offset;
  offset[0] = posArray[0];
  offset[1] = posArray[2];  
  offset[2] = posArray[1];
  //-!

  // width
  if (array[4].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, array[4].GetType());
    return NOTHING;
  }
  float width = array[4];

  // height
  if (array[5].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, array[5].GetType());
    return NOTHING;
  }
  float height = array[5];

  // maintain size? (don't grow/shrink as map scale changes?)
  if (array[6].GetType() != GameBool && array[6].GetType() != GameScalar)
  {
    state->TypeError(GameBool, array[6].GetType());
    return NOTHING;
  }
  bool maintainSize = false;
  float minScale = -1;
  if (array[6].GetType() == GameBool)
    maintainSize = array[6];
  else
    minScale = array[6];

  // direction
  if (array[7].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, array[7].GetType());
    return NOTHING;
  }
  float angle = array[7];

  // string identifier
  if (array[8].GetType() != GameString)
  {
    state->TypeError(GameString, array[8].GetType());
    return NOTHING;
  }
  RString name = array[8];

  // shadow?
  if (array[9].GetType() != GameBool)
  {
    state->TypeError(GameBool, array[9].GetType());
    return NOTHING;
  }
  bool shadow = array[9];

  // text
  if (array[10].GetType() != GameString)
  {
    state->TypeError(GameString, array[10].GetType());
    return NOTHING;
  }
  RString text = array[10];

  bool is3D = false;
  bool line = false;
  float priority = 0;
  bool rotateWithObj = true;
  if (n > 11)
  {
    // is3D?
    if (array[11].GetType() != GameBool)
    {
      state->TypeError(GameBool, array[11].GetType());
      return NOTHING;
    }
    is3D = array[11];

    // show line?
    if (array[12].GetType() != GameBool)
    {
      state->TypeError(GameBool, array[12].GetType());
      return NOTHING;
    }
    line = array[12];

    if (n > 13)
    {
      // priority
      if (array[13].GetType() != GameScalar)
      {
        state->TypeError(GameScalar, array[13].GetType());
        return NOTHING;
      }
      priority = array[13];
    }

    // rotate with obj?    
    if (n > 14)
    {
      if (array[14].GetType() != GameBool)
      {
        state->TypeError(GameBool, array[14].GetType());
        return NOTHING;
      }
      rotateWithObj = array[14];
    }
  }

  // set icon 2D
  Ref<Texture> texture = GlobLoadTexture(GetVehicleIcon(iconName));
  if (texture)
  {
    // only one icon of each ID is allowed
    obj->RemoveIcon(name);

    EditorObjectIcon icon;

    icon.icon = texture;
    icon.color = color;
    icon.offset = offset;
    icon.width = width;
    icon.height = height;
    icon.angle = (H_PI / 180.0f) * angle;
    icon.shadow = shadow;
    icon.line = line;
    icon.is3D = is3D;
    icon.id = name;
    icon.maintainSize = maintainSize;
    icon.minScale = minScale;
    icon.priority = priority;
    icon.text = text;
    icon.rotateWithObj = rotateWithObj;

    obj->AddIcon(icon);

    //LogF("Set icon for %s to %s (id: %s)",obj->GetArgument("VARIABLE_NAME").Data(),iconName.Data(),name.Data());
  }

  return NOTHING;
}

// convert arguments from a GameArray to an array of RString
static bool ConvertArguments(const GameState *state,const GameArrayType &argArray,AutoArray<RString> &args)
{
  for (int i=0; i<argArray.Size()-1; i+=2)
  {
    // check type of argument
    if (argArray[i].GetType() != GameString)
    {
      state->TypeError(GameString, argArray[i].GetType());
      return false;
    }

    // convert value to string
    RString value;

    // is it an array? assume subtype multiple
    if (argArray[i+1].GetType() == GameArray)
    {
      RString name = argArray[i];
      const GameArrayType &array = argArray[i+1];
      for (int j=0; j<array.Size(); j++) 
      {
        if (array[j].GetType() == GameString)
          value = array[j];
        else
          value = array[j].GetText();
        args.Add(argArray[i]); args.Add(value);
      }
      continue;
    }

    if (argArray[i+1].GetType() == GameString)
      value = argArray[i+1];
    else
      value = argArray[i+1].GetText();
    args.Add(argArray[i]); args.Add(value);
  }
  args.Compact();

  return true;
}

#define OBJECT_NULL GameValueExt((Object *)NULL)

// TODO: enumerate possible editor scripts
GameValue MapExecEditorScript(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return OBJECT_NULL;

  const GameArrayType &array = oper2;
  if (array.Size() != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return OBJECT_NULL;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return OBJECT_NULL;
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameArray, array[1].GetType());
    return OBJECT_NULL;
  }

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return OBJECT_NULL;

  // find object
  RString id = array[0];
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return OBJECT_NULL;

  RString script = array[1];

  if (stricmp(script,"create") == 0)
  {
    map->CreateObject(obj, true, false, false, true);
    Object *proxy = obj->GetProxyObject();
    return GameValueExt(proxy);
  }
  else
  {
    QOStrStream out;

    if (stricmp(script,"update") == 0)
      obj->WriteUpdateScript(out);
    else if (stricmp(script,"updateposition") == 0)
      obj->WriteUpdatePositionScript(out);
    else if (stricmp(script,"select") == 0)
      obj->WriteSelectScript(out);
    else if (stricmp(script,"destroy") == 0)
      obj->WriteDeleteScript(out);
    else
      return OBJECT_NULL;

    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(map->GetVariables());
    /*
    // TODO: allow custom variables (ie _new?)
    gstate->VarLocal("_new");
    gstate->VarSet("_new", GameValue(isNewObject), true, false, GWorld->GetMissionNamespace());
    */
    gstate->Execute(RString(out.str(), out.pcount()), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    gstate->EndContext();
  }

  return OBJECT_NULL;
}

GameValue MapSetFogOfWar(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent() || !map->IsRealTime()) return NOTHING;

  const GameArrayType &array = oper2;
  if (array.Size() != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameArray)
  {
    state->TypeError(GameArray, array[1].GetType());
    return NOTHING;
  }

  // determine FOW
  GameStringType str = array[0];
  FogOfWar fogOfWar = NFogOfWar;
  const EnumName *names = GetEnumNames(fogOfWar);
  for (int i=0; names[i].IsValid(); i++)
  {
    if (stricmp(names[i].name, str) == 0)
    {
      fogOfWar = (FogOfWar)names[i].value;
      break;
    }    
  }
  if (fogOfWar == NFogOfWar) return NOTHING;

  bool fromPlayer = false;

  // any objects passed in?
  OLinkArray(Object) objs;
  const GameArrayType &objArray = array[1];
  for (int i=0; i<objArray.Size(); i++)
  {
    if (objArray[i].GetType() != GameObject)
    {
      state->TypeError(GameObject, objArray[i].GetType());
      return NOTHING; 
    }
    Object *obj = static_cast<GameDataObject *>(objArray[i].GetData())->GetObject();
    if (obj == GWorld->PlayerOn()) 
      fromPlayer = true; 
    else
      objs.Add(obj);
  }

  // from player?
  if (objs.Size() == 0)
    fromPlayer = true;

  map->SetFogOfWar(fogOfWar,objs,fromPlayer);

  return NOTHING;
}

GameValue MapSetObjectArguments(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  if (array.Size() != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameArray)
  {
    state->TypeError(GameArray, array[1].GetType());
    return NOTHING;
  }

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  // find object
  RString id = array[0];
  EditorObject *obj = ws->FindObject(id);
  if (!obj) return NOTHING;

  // any empty arrays passed in? if so, clear corresponding arguments of subtype multiple
  const GameArrayType &argArray = array[1];
  for (int i=0; i<argArray.Size()-1; i+=2)
  {  
    if (argArray[i].GetType() == GameString && argArray[i+1].GetType() == GameArray)
    {
      const GameArrayType &value = argArray[i+1];
      if (value.Size() == 0)
      {
        RString name = argArray[i];
        obj->RemoveArgument(name);
      }
    }
  }

  AutoArray<RString> args;
  if (!ConvertArguments(state,array[1],args)) return NOTHING;

  // assign arguments
  obj->SetArguments(args);

  // setting parent? we need to update the tree
  for (int i=0; i<args.Size(); i+=2)
    if (!stricmp(args[i],"PARENT"))
    {
      map->UpdateObjectBrowser(id);
      break;
    }

  return NOTHING;
}

EditorObject *CStaticMapEditor::FindEditorObject(RString type, AutoArray<EditorArgument> &args)
{
  for (int i=0; i<_ws->NObjects(); i++)
  {
    EditorObject *obj = _ws->GetObject(i);
    if (stricmp(obj->GetType()->GetName(), type) != 0) continue;
    bool match = true;
    for (int i=0; i<args.Size(); i++)
    {
      match = obj->HasArgument(args[i]);
      if (!match)
        break;
    }
    if (match) return obj;
  }
  return NULL;
}

EditorObject *CStaticMapEditor::FindEditorObject(GameValue value,RString type)
{
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(_vars);
  EditorObject *rtn = NULL;
  for (int i=0; i<_ws->NObjects(); i++)
  {
    EditorObject *obj = _ws->GetObject(i);
    if (!obj) continue;
    if (type.GetLength() > 0 && stricmp(obj->GetType()->GetName(),type) != 0) continue; 
    GameValue result;
    result = gstate->Evaluate(obj->GetArgument("VARIABLE_NAME"), GameState::EvalContext::_default, GWorld->GetMissionNamespace());    
    if (result.GetText() == value.GetText())  // IsEqualTo doesn't work for game arrays
    {
      rtn = obj;
      break;
    }
  }
  gstate->EndContext();
  return rtn;
}

GameValue MapFindEditorObjectFromValue(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return RString();

  // is a gamevalue passed in? find corresponding editor object
  EditorObject *obj = map->FindEditorObject(oper2);
  if (obj) return obj->GetArgument("VARIABLE_NAME");
  return RString();
}

GameValue MapFindEditorObjects(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &rtnArray = value;

  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return value;

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return value;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return value;
  }

  const GameArrayType &array = oper2;
  int n = array.Size();

  // find editor object from value?
  if (n == 2)
  {
    if (array[0].GetType() != GameString)
    {
      state->TypeError(GameString, array[0].GetType());
      return value;
    }
    EditorObject *obj = map->FindEditorObject(array[1],array[0]);
    if (obj) 
      rtnArray.Add(obj->GetArgument("VARIABLE_NAME"));
    return value;
  }

  if ((n % 2) != 1)
  {
    state->SetError(EvalDim, array.Size(), 3);
    return value;
  }
  for (int i=0; i<n; i++)
  {
    if (array[i].GetType() != GameString)
    {
      state->TypeError(GameString, array[i].GetType());
      return value;
    }
  }

  RString type = array[0];
  int count = (n - 1) / 2;
  
  AutoArray<EditorArgument> args;
  args.Realloc(count);
  args.Resize(count);

  int j = 1;
  for (int i=0; i<count; i++)
  {
    args[i].name = array[j++];
    args[i].value = array[j++];
  }

  for (int i=0; i<ws->NObjects(); i++)
  {
    EditorObject *obj = ws->GetObject(i);
    if (type.GetLength() == 0 || stricmp(obj->GetType()->GetName(), type) != 0) continue;
    bool match = true;
    for (int i=0; i<args.Size(); i++)
    {
      match = obj->HasArgument(args[i]);
      if (!match)
        break;
    }
    if (match) rtnArray.Add(obj->GetArgument("VARIABLE_NAME"));
  }
  
  return value;
}

GameValue MapFindEditorObject(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return RString();

  const GameArrayType &array = oper2;
  int n = array.Size();

  // find editor object from value?
  if (n == 2)
  {
    if (array[0].GetType() != GameString)
    {
      state->TypeError(GameString, array[0].GetType());
      return RString();
    }
    EditorObject *obj = map->FindEditorObject(array[1],array[0]);
    if (obj) return obj->GetArgument("VARIABLE_NAME");
    return RString();
  }

  if ((n % 2) != 1)
  {
    state->SetError(EvalDim, array.Size(), 3);
    return RString();
  }
  for (int i=0; i<n; i++)
  {
    if (array[i].GetType() != GameString)
    {
      state->TypeError(GameString, array[i].GetType());
      return RString();
    }
  }

  RString type = array[0];
  int count = (n - 1) / 2;
  
  AutoArray<EditorArgument> args;
  args.Realloc(count);
  args.Resize(count);

  int j = 1;
  for (int i=0; i<count; i++)
  {
    args[i].name = array[j++];
    args[i].value = array[j++];
  }

  EditorObject *obj = map->FindEditorObject(type, args);
  if (obj) return obj->GetArgument("VARIABLE_NAME");
  return RString();
}

GameValue MapGetObjectChildren(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return value;
  
  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return value;
  }

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return value;
  
  RString id = oper2;

  // find children
  for (int i=0; i<ws->NObjects(); i++)
  {
    EditorObject *obj = ws->GetObject(i);
    if (!obj) continue;
    if (stricmp(obj->GetArgument("PARENT"), id) == 0) array.Add(obj->GetArgument("VARIABLE_NAME"));
  }

  array.Compact();
  return value;
}

GameValue MapListObjects(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return value;

  RString type = oper2;

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return value;
  for (int i=0; i<ws->NObjects(); i++)
  {
    EditorObject *obj = ws->GetObject(i);
    if (!obj) continue;
    if (stricmp(obj->GetType()->GetName(), type) != 0) continue;
    if (obj->GetParentOverlay() != ws->GetActiveOverlay()) continue;
    array.Add(obj->GetArgument("VARIABLE_NAME"));
  }
  array.Compact();
  return value;
}

#if _VBS3
  #include <El/Debugging/debugTrap.hpp>
#endif

/*
\patch Date 1.7 20/09/2007 by Chad
- Fixed: When inserting objects into the real time editor would result in ctd because of delayed loading.
*/
GameValue MapInsertObject(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
#if _VBS3
  GDebugger.PauseCheckingAlive();
#endif

  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  if (array.Size() < 3 || array.Size() > 4)
  {
    state->SetError(EvalDim, array.Size(), 3);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  if (array[2].GetType() != GameArray)
  {
    state->TypeError(GameArray, array[2].GetType());
    return NOTHING;
  }

  // get arguments from array
  const GameArrayType &argArray = array[2];
  AutoArray<RString> args;
  if (!ConvertArguments(state,argArray,args)) return NOTHING;

  // subtype specified?
  ConstParamEntryPtr subTypeConfig = NULL;
  if (array.Size() == 4)
  {
    if (array[3].GetType() != GameConfig)
    {
      state->TypeError(GameConfig, array[3].GetType());
      return NOTHING;
    }
    const GameConfigType &configEntry = static_cast<GameDataConfig *>(array[3].GetData())->GetConfig();
    subTypeConfig = configEntry.entry;
  }

  // new object of given type
  RString typeName = array[0];
  return map->AddObject(typeName, args, false, array[1], subTypeConfig);

}

GameValue MapEditObject(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  map->ShowEditObject(oper2);

  return NOTHING;
}

GameValue MapShowNewObject(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  if (array.Size() != 4)
  {
    state->SetError(EvalDim, array.Size(), 4);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }

  TargetSide side = GetSide(array[2]);
  if (side > TSideUnknown) side = TSideUnknown;

  Vector3 pos;
  if (!GetRelPos(state, pos, array[3]))
  {
    return NOTHING;
  }
/*
  GScene->DrawCollisionStar
  (
    pos,0.5,PackedColor(Color(0.25,0,0.25))
  );
*/
  RString type = array[0];
  RString className = array[1];

  map->ShowNewObject(type,className,side,pos);
  return NOTHING;
}

GameValue MapAddObject(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
#if _VBS3
  GDebugger.NextAliveExpected(15*60*1000);
#endif

  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  const GameArrayType &array = oper2;
  if (array.Size() < 2 || array.Size() > 3)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }

  if (array[1].GetType() != GameArray)
  {
    state->TypeError(GameArray, array[1].GetType());
    return NOTHING;
  }

  // get arguments from array
  const GameArrayType &argArray = array[1];
  AutoArray<RString> args;
  if (!ConvertArguments(state,argArray,args)) return NOTHING;

  // subtype specified?
  ConstParamEntryPtr subTypeConfig = NULL;
  if (array.Size() == 3)
  {
    if (array[2].GetType() != GameConfig)
    {
      state->TypeError(GameConfig, array[2].GetType());
      return NOTHING;
    }
    const GameConfigType &configEntry = static_cast<GameDataConfig *>(array[2].GetData())->GetConfig();
    subTypeConfig = configEntry.entry;
  }

  // new object of given type
  RString typeName = array[0];
  return map->AddObject(typeName, args, true, NOTHING, subTypeConfig);
}

GameValue MapIsRealTime( const GameState *state, GameValuePar oper1 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  return map->IsRealTime();
}

GameValue MapIsCommandMenuVisible( const GameState *state, GameValuePar oper1 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  return map->CommandMenuVisible();
}

GameValue MapShowCommandMenu( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameBool)
  {
    state->TypeError(GameBool, oper1.GetType());
    return NOTHING;
  }
  
  map->ShowCommandMenu(oper2);

  return NOTHING;
}

GameValue MapIsLockedToCamera( const GameState *state, GameValuePar oper1 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  return map->IsLockedToCamera();
}

GameValue MapLockToCamera( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameBool)
  {
    state->TypeError(GameBool, oper1.GetType());
    return NOTHING;
  }

  map->LockToCamera(oper2);

  return NOTHING;
}

#define OBJECT_NULL GameValueExt((Object *)NULL)

GameValue CameraGet( const GameState *state, GameValuePar oper1 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return OBJECT_NULL;

  Object *obj;
  obj = map->CameraGet();
  if (obj)
    return GameValueExt(map->CameraGet());
  return OBJECT_NULL;
}

GameValue CameraRestart( const GameState *state, GameValuePar oper1 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  map->CameraRestart();

  return NOTHING;
}

GameValue MapCollapseTree( const GameState *state, GameValuePar oper1 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;
  
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  // hide all children
  for (int i=0; i<ws->NObjects(); i++)
  {
    EditorObject *obj = ws->GetObject(i);
    if (!obj) continue;
    if (obj->GetParent().GetLength() > 0)
      obj->SetVisibleInTree(false);
  }

  map->UpdateObjectBrowser();

  return NOTHING;
}


GameValue MapOnDoubleClick( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  map->SetOnDoubleClick(oper2);
  return NOTHING;
}

GameValue MapToggleQuickAddObject( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameBool)
  {
    state->TypeError(GameBool, oper2.GetType());
    return NOTHING;
  }

  map->ToggleOnQuickAddObject(oper2);
  return NOTHING;
}

GameValue MapOnQuickAddObject( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper1.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  int n = array.Size();
  if (n != 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, array[1].GetType());
    return NOTHING;
  }

  RString command = array[0];
  float key = array[1];

  map->SetOnQuickAddObject(command,(int)key);
  return NOTHING;
}

GameValue MapOnQuickAddObjectToggled( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  map->SetOnQuickAddObjectToggled(oper2);
  return NOTHING;
}

GameValue MapOnShowNewObject( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  map->SetOnShowNewObject(oper2);
  return NOTHING;
}

GameValue MapLayersGetAll( const GameState *state, GameValuePar oper1 )
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return value;

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return value;

  for (int i=0; i<ws->NOverlays(); i++)
  {
    EditorOverlay *overlay = ws->GetOverlay(i);
    if (!overlay) continue;
    array.Add(overlay->GetName());
  }

  return value;
}

GameValue MapLayerGetActive( const GameState *state, GameValuePar oper1 )
{
  float value = -1;

  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return value;

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return value;

  for (int i=0; i<ws->NOverlays(); i++)
  {
    EditorOverlay *overlay = ws->GetOverlay(i);
    if (!overlay) continue;
    if (overlay == ws->GetActiveOverlay())
      value = (float)i;
  }

  return value;
}

GameValue MapLayerUpdate( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  if (array.Size() != 2)
  {
    if (state) state->SetError(EvalDim,array.Size(),2);
    return NOTHING;
  }
  if (array[0].GetType() != GameScalar)
  {
    if (state) state->TypeError(GameScalar,array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameString)
  {
    if (state) state->TypeError(GameString,array[1].GetType());
    return NOTHING;
  }

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  int index = toInt(array[0]);
  RString name = array[1];

  if (index >= 0 && index < ws->NOverlays())
    ws->GetOverlay(index)->SetName(name);

  return NOTHING;
}

GameValue MapLayerNew( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  if (array.Size() != 2)
  {
    if (state) state->SetError(EvalDim,array.Size(),2);
    return NOTHING;
  }
  if (array[0].GetType() != GameString)
  {
    if (state) state->TypeError(GameString,array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameConfig)
  {
    if (state) state->TypeError(GameConfig,array[1].GetType());
    return NOTHING;
  }

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  RString name = array[0];

  const GameConfigType &config = GetConfig(array[1]);
  if (!config.entry) return NOTHING;
  RString extension = config.entry->GetName();
  
  EditorOverlay *overlay = ws->CreateOverlay(name, false, extension);
  ws->SetActiveOverlay(overlay);

  return (float)ws->NOverlays() - 1;
}

GameValue MapLayerIsActive( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return false;

  if (oper2.GetType() != GameScalar)
  {
    state->TypeError(GameScalar, oper2.GetType());
    return false;
  }

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return false;

  int index = toInt(oper2);

  if (index >= 0 && index < ws->NOverlays())
    return ws->GetOverlay(index)->IsActive();

  return false;
}

GameValue MapLayerSwitch( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameArray)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &array = oper2;
  if (array.Size() != 2)
  {
    if (state) state->SetError(EvalDim,array.Size(),1);
    return NOTHING;
  }
  if (array[0].GetType() != GameScalar)
  {
    if (state) state->TypeError(GameScalar,array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameBool)
  {
    if (state) state->TypeError(GameBool,array[1].GetType());
    return NOTHING;
  }

  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  int index = toInt(array[0]);
  bool exclusive = array[1];
  if (index < 0)  // switch to base layer
  {
    ws->SetActiveOverlay(NULL, exclusive);
  }
  else if (index < ws->NOverlays())
  {
    ws->SetActiveOverlay(ws->GetOverlay(index), exclusive);
  }

  return NOTHING;
}

GameValue MapLayerMergeActiveLayers( const GameState *state, GameValuePar oper1 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;
  
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  ws->MergeActiveOverlays();
  return NOTHING;
}

GameValue MapLayerDelete( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  CStaticMapEditor *map = dynamic_cast<CStaticMapEditor *>(GetControl(oper1));
  if (!map || !map->HasParent()) return NOTHING;

  if (oper2.GetType() != GameScalar)
  {
    state->TypeError(GameScalar, oper2.GetType());
    return NOTHING;
  }
  
  EditorWorkspace *ws = map->GetWorkspace();
  if (!ws) return NOTHING;

  int index = toInt(oper2);
  if (index >= 0 && index < ws->NOverlays())
  {
    map->DeleteOverlay(ws->GetOverlay(index));
  }

  return NOTHING;
}

#define FUNCTIONS_EDITOR(XX, Category) \
  XX(GameBool, "isRealTime", MapIsRealTime, GameControl, "map", "Returns true if the mission editor is operating in real time mode.", "_isRealTime = isRealTime _map", "", "2.92", "", Category) \
  XX(GameBool, "isCommandMenuVisible", MapIsCommandMenuVisible, GameControl, "map", "Returns true if the command menu is currently visible.", "_isCommandMenuVisible = isCommandMenuVisible _map", "", "2.92", "", Category) \
  XX(GameNothing, "collapseObjectTree", MapCollapseTree, GameControl, "map", "Collapse the object tree.", "collapseObjectTree _map", "", "2.92", "", Category) \
  XX(GameNothing, "saveOverlay", MapOverlaySave, GameControl, "map", "Save the current overlay.", "saveOverlay _map", "", "2.92", "", Category) \
  XX(GameNothing, "clearOverlay", MapOverlayClear, GameControl, "map", "Clear the current overlay.", "clearOverlay _map", "", "2.92", "", Category) \
  XX(GameNothing, "commitOverlay", MapOverlayCommit, GameControl, "map", "Commit the current overlay.", "commitOverlay _map", "", "2.92", "", Category) \
  XX(GameNothing, "closeOverlay", MapOverlayClose, GameControl, "map", "Closes the current overlay without committing.", "", "", "2.92", "", Category) \
  XX(GameNothing, "updateObjectTree", MapUpdateObjectTree, GameControl, "map", "Update the editor object tree.", "", "", "2.92", "", Category) \
  XX(GameNothing, "selectedEditorObjects", MapGetSelectedObjects, GameControl, "map", "Returns a list of currently selected editor objects.", "", "", "2.92", "", Category) \
  XX(GameObject, "getEditorCamera", CameraGet, GameControl, "map", "Fetches a reference to the mission editor camera.", "", "", "2.92", "", Category) \
  XX(GameNothing, "restartEditorCamera", CameraRestart, GameControl, "map", "Restarts the mission editor camera (if it was deleted by a script, for example).", "", "", "2.92", "", Category) \
  XX(GameNothing, "importAllGroups", MapImportAllGroups, GameControl, "map", "Imports all groups into the RTE.", "", "", "2.92", "", Category) \
  XX(GameString, "getEditorMode", MapGetMode, GameControl, "map", "Returns the current mode of the editor.", "", "", "2.92", "", Category) \
  XX(GameBool, "isShowing3DIcons", MapIsShow3DIcons, GameControl, "map", "Returns true if the editor is set to draw 3D icons.", "", "", "2.92", "", Category) \
  XX(GameScalar, "nextMenuItemIndex", MapNextMenuItemIndex, GameControl, "map", "Returns the next available menu item index.", "", "", "2.92", "", Category) \
  XX(GameScalar, "clearSelected", MapClearSelected, GameControl, "map", "Unselects all editor objects.", "", "", "2.92", "", Category) \
  XX(GameBool, "isLockedToCamera", MapIsLockedToCamera, GameControl, "map", "Returns true if the map is currently locked to the camera.", "", "", "2.92", "", Category) \
  XX(GameArray, "getLayers", MapLayersGetAll, GameControl, "map", "Returns a list of all layers in the current mission.", "", "", "2.92", "", Category) \
  XX(GameNothing, "mergeLayers", MapLayerMergeActiveLayers, GameControl, "map", "Merges the active layers. If only one layer is active then it will be merged with the base layer.", "", "", "2.92", "", Category) \

static const GameFunction EditorUnary[]=
{
  FUNCTIONS_EDITOR(REGISTER_FUNCTION, "Editor")
};

#if DOCUMENT_COMREF
static ComRefFunc ExtComRefFunc1[] =
{
  FUNCTIONS_EDITOR(COMREF_FUNCTION, "Editor")
};
#endif

#define OPERATORS_EDITOR(XX, Category) \
  XX(GameAny, "evalObjectArgument", function, MapEvalObjectArgument, GameControl, GameArray, "map", "[object, argument]", "Return object argument in mission editor.", "", "", "2.35", "", Category) \
  XX(GameNothing, "setVariable", function, MapSetVariable, GameControl, GameArray, "map", "[name, value]", "Set variable to given value in the variable space of given map.", "", "", "2.92", "", Category) \
  XX(GameNothing, "getVariable", function, MapGetVariable, GameControl, GameString, "map", "name", "Get variable from the variable space of given map.", "", "", "2.92", "", Category) \
  XX(GameString, "getObjectArgument", function, MapGetObjectArgument, GameControl, GameArray, "map", "[object, argument]", "Return name of object argument in mission editor.", "", "", "2.35", "", Category) \
  XX(GameString, "findEditorObject", function, MapFindEditorObject, GameControl, GameArray, "map", "[type, name1, value1, ...]", "Return object of given type with given arguments. Use [type, game value] to search by object reference of a specific editor object type.", "", "", "2.35", "", Category) \
  XX(GameString, "findEditorObject", function, MapFindEditorObjectFromValue, GameControl, GameAny, "map", "value", "Return object that matches the provided reference.", "", "", "2.92", "", Category) \
  XX(GameArray, "listObjects", function, MapListObjects, GameControl, GameString, "map", "type", "Return the list of all objects of given type.", "", "", "2.92", "", Category) \
  XX(GameString, "addEditorObject", function, MapAddObject, GameControl, GameArray, "map", "[type, [name1, value1, ...], subtype class]", "Add an object to the editor and assign arguments. Create script is called with _new equal to true. Returns the ID of the new EditorObject. Subtype class is optional.", "", "", "2.92", "", Category) \
  XX(GameString, "insertEditorObject", function, MapInsertObject, GameControl, GameArray, "map", "[type, value, [name1, value1, ...], subtype class]", "Insert an object to the editor and assign arguments. Create script is not called. Returns the ID of the new EditorObject. Subtype class is optional.", "", "", "2.92", "", Category) \
  XX(GameAny, "setObjectArguments", function, MapSetObjectArguments, GameControl, GameArray, "map", "[object, [name1, value1, ...]]", "Set object arguments in mission editor.", "", "", "2.92", "", Category) \
  XX(GameObject, "getObjectProxy", function, MapGetObjectProxy, GameControl, GameString, "map", "object", "Return the proxy object associated with the given editor object.", "", "", "2.92", "", Category) \
  XX(GameAny, "setObjectProxy", function, MapSetObjectProxy, GameControl, GameArray, "map", "[object, proxy object]", "Set the proxy object associated with the given editor object.", "", "", "2.92", "", Category) \
  XX(GameAny, "deleteEditorObject", function, MapDeleteEditorObject, GameControl, GameString, "map", "object", "Delete the editor object. Requires all editor object links to be removed prior.", "", "", "2.92", "", Category) \
  XX(GameArray, "getObjectChildren", function, MapGetObjectChildren, GameControl, GameString, "map", "object", "Return a list of all the children of the specified object.", "", "", "2.92", "", Category) \
  XX(GameObject, "execEditorScript", function, MapExecEditorScript, GameControl, GameArray, "map", "[object, script]", "Execute an editor script for the specified object.", "_map execEditorScript [\"_team_1\", \"create\"]", "", "2.92", "", Category) \
  XX(GameAny, "showNewEditorObject", function, MapShowNewObject, GameControl, GameArray, "map", "[type, class, side, position]", "Show the add editor object dialog, type is editor object type, class is class definition to automatically select, side filters by a certain side, pos is position to create the object.", "", "", "2.92", "", Category) \
  XX(GameAny, "editObject", function, MapEditObject, GameControl, GameString, "map", "object", "Show the edit object dialog for the given object.", "", "", "2.92", "", Category) \
  XX(GameAny, "onDoubleClick", function, MapOnDoubleClick, GameControl, GameString, "map", "command", "Defines an action performed when the user double clicks on the map. Command receives:<br/>\r\n<br/>\r\n_pos <t>array</t> position<br/>\r\n_units <t>array</t> selected units<br/>\r\n_shift,_alt <t>bool</t> key state", "", "", "2.92", "", Category) \
  XX(GameAny, "onShowNewObject", function, MapOnShowNewObject, GameControl, GameString, "map", "command", "Defines an action performed when the user right clicks on the map and selects New Object. Set to empty for default behavior. Command receives:<br/>\r\n<br/>\r\n_pos <t>array</t> position", "", "", "2.92", "", Category) \
  XX(GameNothing, "onQuickAddObject", function, MapOnQuickAddObject, GameControl, GameArray, "map", "[command,key]", "Defines an action performed when the user performs a quick add object in the editor. Set to empty for default behavior. Command receives:<br/>\r\n<br/>\r\n_pos <t>array</t> position<br/>\r\n_class <t>string</t> currently selected object class", "", "", "2.92", "", Category) \
  XX(GameNothing, "onQuickAddObjectToggled", function, MapOnQuickAddObjectToggled, GameControl, GameString, "map", "command", "Defines an action performed when quick add object is toggled on/off. Set to empty for default behavior. Command receives:<br/>\r\n<br/>\r\n_on <t>bool</t> quick add on or off", "", "", "2.92", "", Category) \
  XX(GameNothing, "toggleQuickAddObject", function, MapToggleQuickAddObject, GameControl, GameBool, "map", "bool", "if true objects will be added automatically on left click.", "", "", "1.18", "", Category) \
  XX(GameAny, "selectEditorObject", function, MapSelectEditorObject, GameControl, GameString, "map", "object", "Select an editor object. Does not un-select previously selected objects.", "", "", "2.92", "", Category) \
  XX(GameAny, "unSelectEditorObject", function, MapUnselectEditorObject, GameControl, GameString, "map", "object", "Select an editor object. Does not un-select previously selected objects.", "", "", "2.92", "", Category) \
  XX(GameNothing, "setEditorObjectScope", function, MapSetEditorObjectScope, GameControl, GameArray, "map", "[objects,editor type,condition,scope,subordinates also?,set as default?]", "This command defines the level of access a user has to editor objects. objects is an array of either Editor Objects (eg [""_unit_0""]) or actual Game Objects (eg [player]). If the array is empty then the command will automatically parse all editor objects. editor type is the editor type to effect (eg ""unit""), or """" for none. condition is an executable string that must evaluate to true or false. If true, the scope of the evaluated editor object will be modified. scope is one of ""HIDE"", ""VIEW"", ""SELECT"", ""LINKTO"", ""LINKFROM"", ""ALLNODRAG"", ""ALLNOTREE"", ""ALLNOSELECT"", ""ALLNOCOPY"" or ""ALL"". subordinates also is a boolean value, if true then subordinates in the editor will be assigned the same scope as the parent.", "_map setEditorObjectScope [[],""vehicle"",""side effectiveCommander _x != side player"",""HIDE"",false];", "", "2.92", "", Category) \
  XX(GameNothing, "setEditorTypeScope", function, MapSetEditorTypeScope, GameControl, GameArray, "map", "[object type,scope]", "Sets the default scope for the specified editor object type. scope is one of ""HIDE"", ""VIEW"", ""SELECT"", ""LINKTO"", ""LINKFROM"", ""ALLNODRAG"", ""ALLNOTREE"", ""ALLNOSELECT"", ""ALLNOCOPY"" or ""ALL"". subordinates also is a boolean value, if true then subordinates in the editor will be assigned the same scope as the parent.", "", "", "1.19", "", Category) \
  XX(GameScalar, "getEditorTypeScopeN", function, MapGetEditorTypeScopeN, GameControl, GameString, "map", "object type", "Returns the default scope for the specified editor object type as a number.", "", "", "1.19", "", Category) \
  XX(GameString, "getEditorObjectScope", function, MapGetEditorObjectScope, GameControl, GameString, "map", "object", "Returns the editor object scope of the specified editor object.", "", "", "2.92", "", Category) \
  XX(GameScalar, "getEditorObjectScopeN", function, MapGetEditorObjectScopeN, GameControl, GameString, "map", "object", "Returns the editor object scope of the specified editor object as a number.", "", "", "1.19", "", Category) \
  XX(GameNothing, "setDefaultScope", function, MapSetDefaultScope, GameControl, GameArray, "map", "[object,scope,subordinates also?]", "This command sets the default editor scope for a single object. scope is one of ""HIDE"", ""VIEW"", ""SELECT"", ""LINKTO"", ""LINKFROM"", ""ALLNODRAG"", ""ALLNOTREE"", ""ALLNOSELECT"", ""ALLNOCOPY"" or ""ALL"".", "", "", "1.19", "", Category) \
  XX(GameScalar, "addMenuItem", function, MapAddMenuItem, GameControl, GameArray, "map", "[menu or index,text,command,priority]", "Creates a new menu item. Menu can be \"file\" or \"view\", index is index as returned from addMenu command. priority is optional and determines where in the menu the item will reside (higher priority items first).", "", "", "2.92", "", Category) \
  XX(GameNothing, "updateMenuItem", function, MapUpdateMenuItem, GameControl, GameArray, "map", "[menu item index,text,command]", "Sets the text and command for the menu item. index is index as returned from addMenuItem command. command is optional.", "", "", "2.92", "", Category) \
  XX(GameNothing, "removeMenuItem", function, MapRemoveMenuItem, GameControl, GameScalar, "map", "index of menu item to delete", "Removes a previously added menu item.", "", "", "2.92", "", Category) \
  XX(GameNothing, "removeMenuItem", function, MapRemoveMenuItemString, GameControl, GameString, "map", "text of menu item to delete", "Removes a previously added menu item.", "", "", "2.92", "", Category) \
  XX(GameScalar, "nMenuItems", function, MapGetNMenuItems, GameControl, GameString | GameScalar, "map", "menu index", "Returns the total number of user-added menu items belonging to the given menu.", "", "", "2.92", "", Category) \
  XX(GameNothing, "setEditorMode", function, MapSetMode, GameControl, GameString, "map", "mode", "Sets map mode to ""MAP"", ""3D"" or ""PREVIEW"".", "", "", "2.92", "", Category) \
  XX(GameNothing, "editorSetEventHandler", function, MapSetEventHandler, GameControl, GameArray, "map", "[handler name, function]", "Sets given event handler of given editor.", "_map editorSetEventHandler [\"SelectObject\", \"\"]", "", "2.92", "", Category) \
  XX(GameNothing, "setVisibleIfTreeCollapsed", function, MapSetVisibleIfTreeCollapsed, GameControl, GameArray, "map", "[object, visible if tree collapsed]", "Sets whether or not the object is visible even if the tree is collapsed.", "", "", "2.92", "", Category) \
  XX(GameScalar, "addMenu", function, MapAddMenu, GameControl, GameArray, "map", "[text,priority]", "Adds a new menu button. Priority is optional.", "", "", "2.92", "", Category) \
  XX(GameNothing, "createMenu", function, MapCreateMenu, GameControl, GameScalar, "map", "index", "Creates a previously added menu.", "", "", "2.92", "", Category) \
  XX(GameNothing, "newOverlay", function, MapOverlayNew, GameControl, GameConfig, "map", "config", "Creates the new overlay dialog for the specified type of overlay.", "", "", "2.92", "", Category) \
  XX(GameNothing, "loadOverlay", function, MapOverlayLoad, GameControl, GameConfig, "map", "config", "Creates the load overlay dialog for the specified type of overlay.", "", "", "2.92", "", Category) \
  XX(GameNothing, "lookAtPos", function, MapLookAtPosition, GameControl, GameArray, "map", "position", "Center the map on, and point the camera at, the position.", "", "", "2.92", "", Category) \
  XX(GameNothing, "drawLink", function, MapDrawLink, GameControl, GameArray, "map", "[from, to, param type, line type, color, scale at which to not draw arrow]", "The editor will draw a line between the two specified editor objects. Line type can be ""LINE"" or ""ARROW"".", "", "", "2.92", "", Category) \
  XX(GameNothing, "removeDrawLinks", function, MapRemoveDrawLinks, GameControl, GameArray, "map", "[from, param type]", "Remove all drawn links for the given editor object for the given editor object type. Pass an empty string as param type to remove all draw links for an object.", "", "", "2.92", "", Category) \
  XX(GameNothing, "setDrawIcon", function, MapSetDrawIcon, GameControl, GameArray, "map", "[object, texture, color, offset, width, height, maintain size?, angle, string identifier, shadow, text, is3D, draw line?, priority, rotate with proxy?]", "Set the icon to be shown in 2D editor for the specified editor object. If maintain size is false, icon will not scale depending on the scale of the map. If maintain size is a number, the icon will maintain size if map scale is below that number. is3D, show line and priority are optional.", "", "", "2.92", "", Category) \
  XX(GameNothing, "updateDrawIcon", function, MapDrawIconUpdate, GameControl, GameArray, "map", "[object, string identifier, color, offset, width, height, maintain size?, angle, shadow, text]", "Updates the icon to be shown in 2D editor for the specified editor object. If maintain size is false, icon will not scale depending on the scale of the map. If maintain size is a number, the icon will maintain size if map scale is below that number.", "", "", "2.92", "", Category) \
  XX(GameNothing, "removeDrawIcon", function, MapRemoveDrawIcon, GameControl, GameArray, "map", "[object, string identifier]", "Removes an icon for an editor object.", "", "", "2.92", "", Category) \
  XX(GameNothing, "moveObjectToEnd", function, MapMoveObjectToEnd, GameControl, GameString, "map", "object", "Shifts an editor object to the end of the objects array. This means that the object will be drawn last (after all other objects).", "", "", "2.92", "", Category) \
  XX(GameNothing, "show3DIcons", function, MapShow3DIcons, GameControl, GameBool, "map", "bool", "Toggle the drawing of 3D icons.", "", "", "2.92", "", Category) \
  XX(GameNothing, "allow3DMode", function, MapAllow3DMode, GameControl, GameBool, "map", "bool", "Allow/dissallow 3D mode.", "", "", "2.92", "", Category) \
  XX(GameNothing, "exitOnMapKey", function, MapExitOnMapKey, GameControl, GameBool, "map", "bool", "Specify if the RTE/C2 interface will exit if the map key is pressed.", "", "", "2.92", "", Category) \
  XX(GameNothing, "enable3DLinking", function, MapEnable3DLinking, GameControl, GameBool, "map", "bool", "Specify if 3D linking is enabled.", "", "", "2.92", "", Category) \
  XX(GameNothing, "allowFileOperations", function, MapAllowFileOperations, GameControl, GameBool, "map", "bool", "Allow/dissallow file ops (load/save etc).", "", "", "2.92", "", Category) \
  XX(GameNothing, "showLegend", function, MapShowLegend, GameControl, GameBool, "map", "bool", "Show/hide map legend.", "", "", "2.92", "", Category) \
  XX(GameNothing, "createWPEdObjs", function, MapCreateWaypointEdObjs, GameControl, GameArray, "map", "[group, edObj for group, edObj for leader unit]", "Create waypoint editor objects for a group.", "", "", "2.92", "", Category) \
  XX(GameString,  "createUnitEdObj", function, MapCreateUnitEdObj, GameControl, GameArray, "map", "[unit object, groupEdObj, leaderEdObj, vehicle object, vehEdObj]", "Create an editor object for a unit. Vehicle object and vehEdObj are optional. Returns the string id of the new editor object. If editor object already exists the function will update the existing.", "", "", "2.92", "", Category) \
  XX(GameString,  "createVehEdObj", function, MapCreateVehEdObj, GameControl, GameArray, "map", "[vehicle object]", "Create an editor object for a vehicle. Returns the string id of the new editor object. If editor object already exists the function will update the existing.", "", "", "2.92", "", Category) \
  XX(GameNothing, "showCommandMenu", function, MapShowCommandMenu, GameControl, GameBool, "map", "bool", "Show/hide command menu.", "", "", "2.92", "", Category) \
  XX(GameNothing, "lockToCamera", function, MapLockToCamera, GameControl, GameBool, "map", "bool", "Lock the map to the camera.", "", "", "2.92", "", Category) \
  XX(GameNothing, "ignoreObjects", function, MapIgnoreObjects, GameControl, GameArray, "map", "[objects to ignore]", "Defines a list of objects that are ignored for the purpose of detecting ground intercept in 3D.", "", "", "2.92", "", Category) \
  XX(GameNothing, "updateObjectTree", function, MapUpdateObjectTree2, GameControl, GameString|GameArray, "map", "object", "Updates a single editor object in the tree.", "", "", "2.92", "", Category) \
  XX(GameArray,   "getEditorObjectSettings", function, MapObjGetSettings, GameControl, GameString, "map", "object", "Returns editor object settings.", "", "", "2.92", "", Category) \
  XX(GameNothing, "setEditorObjectSettings", function, MapObjSetSettings, GameControl, GameArray, "map", "[object, anchored?, can penetrate ground?, allow move if attached?]", "Sets editor object settings.", "", "", "2.92", "", Category) \
  XX(GameArray, "findEditorObjects", function, MapFindEditorObjects, GameControl, GameArray, "map", "[type, name1, value1, ...]", "Return a list of all objects with given arguments that match a specified type. Pass an empty type to test all objects.", "", "", "1.19", "", Category) \
  XX(GameArray, "setFogOfWar", function, MapSetFogOfWar, GameControl, GameArray, "map", "[fog of war setting,[units]]", "Enables or disables fog of war. If units are specified then fog of war will be from the perspective of these units. If no units are specified then fog of war will be from the perspective of the player. Fog of war setting may be one of: \"OFF\", \"ALL\", \"SIDE\", \"VISIBLE\", or \"NONE\".", "", "", "1.19", "", Category) \
  XX(GameScalar, "newLayer", function, MapLayerNew, GameControl, GameArray, "map", "[name, config]", "Creates a new layer, and assigns it as current.", "", "", "2.92", "", Category) \
  XX(GameNothing, "switchLayer", function, MapLayerSwitch, GameControl, GameArray, "map", "[index, exclusive]", "The given layer will be become the active layer. If -1 is passed then the base layer will become active.", "", "", "2.92", "", Category) \
  XX(GameNothing, "deleteLayer", function, MapLayerDelete, GameControl, GameScalar, "map", "index", "Deletes the given layer and all objects in it.", "", "", "2.92", "", Category) \
  XX(GameBool, "isActiveLayer", function, MapLayerIsActive, GameControl, GameScalar, "map", "index", "Returns true if the given layer is active.", "", "", "2.92", "", Category) \
  XX(GameNothing, "updateLayer", function, MapLayerUpdate, GameControl, GameArray, "map", "[index, name]", "Updates the given layer.", "", "", "2.92", "", Category) \
  XX(GameNothing, "lockCamera", function, MapLockCamera, GameControl, GameArray, "map", "[object,[rel pos]]", "Lock the camera to an editor object in 3D.", "", "", "2.92", "", Category) \

static const GameOperator EditorBinary[]=
{
  OPERATORS_EDITOR(REGISTER_OPERATOR, "Editor")
};

#if DOCUMENT_COMREF
static ComRefFunc ExtComRefFunc2[] =
{
  OPERATORS_EDITOR(COMREF_OPERATOR, "Editor")
};
#endif

#define REGISTER_ATTR(name, variable, func) \
  variable = EditorMenuAttributeTypes.attributes.AddValue(name); \
  EditorMenuAttributeTypes.createFunctions.Access(variable); \
  EditorMenuAttributeTypes.createFunctions[variable] = func;

#include <El/Modules/modules.hpp>

INIT_MODULE(MissionEditor, 2)
{
  for( int i=0; i<sizeof(EditorUnary)/sizeof(*EditorUnary); i++ )
  {
    GGameState.NewFunction(EditorUnary[i]);
  }
  for (int i=0; i<sizeof(EditorBinary)/sizeof(*EditorBinary); i++)
  {
    GGameState.NewOperator(EditorBinary[i]);
  }

#if DOCUMENT_COMREF
  for (int i=0; i<lenof(ExtComRefFunc1); i++)
  {
    GGameState.AddComRefFunction(ExtComRefFunc1[i]);
  }
  for (int i=0; i<lenof(ExtComRefFunc2); i++)
  {
    GGameState.AddComRefFunction(ExtComRefFunc2[i]);
  }
#endif

  // Register menu attributes
  ATTRIBUTES_LIST(REGISTER_ATTR);
  EditorMenuAttributeTypes.attributes.Close();
};

#if !_VBS2_LITE

#define FOW_UPDATE_RATE 0.1

class DisplayMissionEditorRealTime : public DisplayMissionEditor
{
protected:
  RString _initSQF;
  RString _importSQF;
  bool _cleanExit;

  // switch to tactical view on exit?
  bool _switchToTactical;

  Ref<ProgressHandle> p;

  void InitRTE();

  // fog of war settings
  FogOfWar _fow;
  OLinkArray(Object) _fowFrom;
  FindArray<int> _fowSides;
  bool _fowFromPlayer;
  UITime _fowUpdate;

public:
  DisplayMissionEditorRealTime(ControlsContainer *parent);
  DisplayMissionEditorRealTime(ControlsContainer *parent,RString clsName, RString initSQF, RString importSQF);
  ~DisplayMissionEditorRealTime();

  void CleanUp();
  void Destroy();
  void DestroyHUD(int exit);

  bool OnKeyDown(int dikCode);
  void OnButtonClicked(int idc);
  void OnChildDestroyed(int idd, int exit);
  void OnSimulate(EntityAI *vehicle);

  void OnMenuSelected(MenuItem *item);
  bool OnMenuCreated(Menu &menu, float x, float y, int idc, CCMContext &ctx);

  // switching to a vehicle turns off the real time editor
  bool SwitchToVehicle(RString id);

  void ImportAllGroups();
  void CreateWaypointEdObjs(AIGroup *aGroup, RString grpEdObj, RString unitEdObj);
  RString CreateUnitEdObj(Person *person, Transport *trans, RString vehEdObj, RString grpEdObj, RString leaderEdObj);
  RString CreateVehEdObj(Transport *trans, RString grpEdObj);

  // fog of war
  void SetFogOfWar(FogOfWar fogOfWar, OLinkArray(Object) &objs, bool fromPlayer);
  void SimulateFogOfWar();
  void SetObjectScope(EditorObject *obj, EditorObjectScope scope);
};

Display *CreateMissionEditorRealTime(ControlsContainer *parent)
{
  return new DisplayMissionEditorRealTime(parent);
}

Display *CreateMissionEditorRealTime(ControlsContainer *parent,RString clsName, RString initSQF, RString importSQF)
{
  return new DisplayMissionEditorRealTime(parent,clsName,initSQF,importSQF);
}

DisplayMissionEditorRealTime::DisplayMissionEditorRealTime(ControlsContainer *parent, RString clsName, RString initSQF, RString importSQF)
: DisplayMissionEditor(parent, clsName)
{
  _initSQF = initSQF;
  _importSQF = importSQF;
  InitRTE();
}

DisplayMissionEditorRealTime::DisplayMissionEditorRealTime(ControlsContainer *parent)
: DisplayMissionEditor(parent, "RscDisplayMissionEditorRealTime")
{
  InitRTE();
}

void DisplayMissionEditorRealTime::CreateWaypointEdObjs(AIGroup *aGroup, RString grpEdObj, RString unitEdObj)
{
  GameState *gstate = GWorld->GetGameState();

  // waypoints
  RString lastWPEdObj;

  // does the group have a cycle waypoint?
  int firstWaypoint = aGroup->GetCurrent()->_fsm->Var(0);

  int iBest = -1;

  for (int i=0; i<aGroup->NWaypoints(); i++)
  {
    WaypointInfo &wp = aGroup->GetWaypoint(i);
    if (wp.type == ACCYCLE)
    {
      iBest = aGroup->FindNearestPreviousWaypoint(i-1,wp.position);
      if (firstWaypoint > iBest) firstWaypoint = iBest;
      break;
    }
  }
  RString wpEdObj;
  for (int l=firstWaypoint; l<aGroup->NWaypoints(); l++)
  {
    if (!l) break;  // ignore waypoint 0
    WaypointInfo &wp = aGroup->GetWaypoint(l);
    if (wp.type == ACCYCLE)
    {
      // don't add cycle waypoints anymore
      GameArrayType wpArray;
      wpArray.Add(GameValueExt(aGroup));
      wpArray.Add((float)iBest);    
      
      EditorObject *wpObj = _map->FindEditorObject(wpArray,"waypoint");
      EditorObject *lastObj = _ws.FindObject(lastWPEdObj);     

      if (lastObj && wpObj)
        lastObj->SetArgument("NEXTTASK",wpObj->GetArgument("VARIABLE_NAME"));  

      break;
    }
    GameArrayType array;
    array.Add("SPEED");
    const EnumName *speeds = GetEnumNames(wp.speed);
    array.Add(speeds[wp.speed].name);
    GameVarSpace *vars = &wp.vars;
    if (vars)
    {
      GameValue var;
      if (vars->VarGet("WAYPOINT_AIR", var))
      {
        if (var.GetType() == GameArray)
        {
          const GameArrayType &a = var;
          if (a.Size() == 3) 
          {
            if
              (
                a[0].GetType() == GameString &&
                a[1].GetType() == GameString &&
                a[2].GetType() == GameString
              ) 
            {
              RString speed = a[2];
              if (stricmp(speed,"CUSTOM") == 0)
              {
                array.Add("AIRCRAFT_SPEED");
                array.Add(a[0]);
                array.Add("AIRCRAFT_HEIGHT");
                array.Add(a[1]);
                array.Add("SPEED");
                array.Add("CUSTOM");
              };
            }
          }
        }
      }
    }
    array.Add("DESCRIPTION");
    array.Add(wp.description);
    array.Add("COMBAT_MODE");
    const EnumName *combatModes = GetEnumNames(wp.combatMode);
    array.Add(combatModes[wp.combatMode+1].name);
    array.Add("FORMATION");
    const EnumName *formations = GetEnumNames(wp.formation);
    array.Add(formations[wp.formation+1].name);
    array.Add("BEHAVIOR");
    const EnumName *behaviours = GetEnumNames(wp.combat);
    array.Add(behaviours[wp.combat].name);
    array.Add("TIMEOUT_MIN");
    array.Add(wp.timeoutMin);
    array.Add("TIMEOUT_MID");
    array.Add(wp.timeoutMid);
    array.Add("TIMEOUT_MAX");
    array.Add(wp.timeoutMax);
    array.Add("SCRIPT");
    array.Add(wp.script);
    array.Add("SHOW");
    const EnumName *show = GetEnumNames(wp.showWP);
    array.Add(show[wp.showWP].name);
    array.Add("CONDITION");
    array.Add(wp.expCond);
    array.Add("ON_ACTIVATION");
    array.Add(wp.expActiv);
    array.Add("PARENT");
    array.Add(grpEdObj);
    array.Add("PARENT_UNIT");
    array.Add(unitEdObj);
    array.Add("TYPE");
    const EnumName *types = GetEnumNames(wp.type);
    RString type = types[wp.type].name;
    array.Add(type);
    RString text = wp.description.GetLength() > 0 ? type + " (" + wp.description + ")" : type;
    array.Add("DISPLAY_NAME");
    array.Add(text);
    array.Add("DISPLAY_NAME_TREE");
    array.Add(text);
    array.Add("POSITION");
    array.Add(Format("[%.5f,%.5f]",wp.position.X(),wp.position.Z()));
    array.Add("PREVTASK");
    array.Add(lastWPEdObj);
    AutoArray<RString> args;
    if (!ConvertArguments(gstate,array,args)) continue;
    GameArrayType wpArray;
    wpArray.Add(GameValueExt(aGroup));
    wpArray.Add((float)l);    
    EditorObject *wpObj = _map->FindEditorObject(wpArray,"waypoint");
    if (wpObj)
      wpEdObj = wpObj->GetArgument("VARIABLE_NAME");
    else
      wpEdObj = _map->AddObject("waypoint", args, false, wpArray);
    // last task links to this task
    if (lastWPEdObj.GetLength() > 0)
    {
      EditorObject *obj = _ws.FindObject(lastWPEdObj);
      if (obj)
        obj->SetArgument("NEXTTASK",wpEdObj);              
    }
    lastWPEdObj = wpEdObj;
  }
}

void DisplayMissionEditorRealTime::SetObjectScope(EditorObject *obj, EditorObjectScope scope = NEditorObjectScopes)
{
  if (scope == NEditorObjectScopes)
    scope = obj->GetDefaultScope();

  if (obj->GetScope() != scope)
  {
    obj->SetScope(scope);
    //LogF("[p] ERROR %s to %d",obj->GetArgument("VARIABLE_NAME").Data(),scope);
    UpdateObjectBrowser(obj); // todo: check recursion, it's probably causing a delay

    obj->DoFade(scope == EOSHidden);

    // this is a little nasty, but it's neccasary so subordinates move back under their parents tree item
    /*
    if (stricmp(obj->GetType()->GetName(),"group") != 0)
    {
      RString name = obj->GetArgument("VARIABLE_NAME");
      for (int i=0; i<_ws.NObjects(); i++)
      {
        EditorObject *subordinate = _ws.GetObject(i);
        if (stricmp(subordinate->GetParent(),name) != 0)
          UpdateObjectBrowser(subordinate);
      }
    }
    */
  }
}

void DisplayMissionEditorRealTime::SimulateFogOfWar()
{
  if (_fow == FOWOff || Glob.uiTime - _fowUpdate < FOW_UPDATE_RATE)
    return;

  Person *player = GWorld->PlayerOn();
  if (player->IsDamageDestroyed()) player = NULL;
  TargetSide playerSide = player ? player->GetTargetSide() : TSideUnknown;

  for (int i=0; i<_ws.NObjects(); i++)
  {
    EditorObject *obj = _ws.GetObject(i);
  
    // fow doesn't affect objects that may be modified
    if (obj->GetScope() >= EOSLinkFrom) continue;
      
    if (_fow == FOWNone)
    {
      SetObjectScope(obj,EOSHidden);
      continue;
    }

    if (_fow == FOWAllUnits)
    {
      SetObjectScope(obj);
      continue;
    }

    Object *proxy = obj->GetProxyObject();
    if (!proxy)
    {
      // assign parent scope, otherwise leave as-is
        // disabled, because waypoint scope needs to stay hidden in C2
      /*
      RString parent = obj->GetParent();
      if (parent.GetLength() > 0)
      {
        EditorObject *parentObj = _ws.FindObject(parent);
        if (parentObj)
          SetObjectScope(obj,parentObj->GetScope());  // todo: may need work?
      }
      */
      continue;
    }

    if ((_fowFromPlayer && proxy == player) || _fowFrom.Find(proxy) > -1)
    {
      SetObjectScope(obj);  
      continue;
    }

    if (_fow == FOWAllSides)
    {
      Entity *ai = dyn_cast<EntityAI>(proxy);
      if (ai)
      {
        TargetSide side = ai->GetTargetSide();
        if (side < TSideUnknown && (_fowSides.Find((int) side) > -1 || side == playerSide))
        {
          // unit is visible always
          SetObjectScope(obj);
          continue;
        }
      }
    }

    // if we get to here, FOW is FOWVisibleUnits or FOWAllSides, so start LOS checking
    bool visible = false;

    for (int i=0; i<_fowFrom.Size(); i++)
    {
      Object *from = _fowFrom[i];
      if (from)
      {
        Person *soldier = dyn_cast<Person>(from);
        if (soldier)
        {
          AIBrain *unit = soldier->Brain();
          if (unit)
          {
            Transport *transport = unit->GetVehicleIn();
            if (transport)
            {
              if (transport->IsDamageDestroyed())
                from = NULL;
              else 
                from = transport;
            }
          }
        }
      }
      if (!from || from->IsDamageDestroyed()) continue;

      //!{ Hotfix by Flyman
      if (!proxy)
      {
        Fail("Error: proxy is NULL");
        continue;
      }
      if (!from->GetShape())
      {
        Fail("Error: from doesn't have a shape");
        continue;
      }
      if (!proxy->GetShape())
      {
        Fail("Error: proxy doesn't have a shape");
        continue;
      }
      //!}
      if (visible = GLandscape->Visible(from->VisiblePosition(),proxy->VisiblePosition(),1,from,proxy) > 0)
        break;
    }

    // LOS from player
    if (_fowFromPlayer && !visible)
    {
      if (player) // player death check was carried out at top of function
      {
        Object *from = player;
        AIBrain *unit = player->Brain();
        if (unit)
        {
          Transport *transport = unit->GetVehicleIn();
          if (transport)
          {
            if (transport->IsDamageDestroyed())
              from = NULL;
            else 
              from = transport;
          }
        }
        if (from)
        {
          if (!proxy)
          {
            Fail("Error: proxy is NULL (2)");
          }
          else if (!from->GetShape())
          {
            Fail("Error: from doesn't have a shape (2)");
          }
          else if  (!proxy->GetShape())
          {
            Fail("Error: proxy doesn't have a shape");
          }
          else
          {
            visible = GLandscape->Visible(from->VisiblePosition(),proxy->VisiblePosition(),1,player,proxy) > 0;
          }
        }
      }
    }

    if (visible)
      SetObjectScope(obj);
    else
      SetObjectScope(obj,EOSHidden);
  }

  _fowUpdate = Glob.uiTime;
} 

void DisplayMissionEditorRealTime::SetFogOfWar(FogOfWar fogOfWar, OLinkArray(Object) &objs, bool fromPlayer)
{
  _fow = fogOfWar;
  _fowFrom = objs;
  _fowFromPlayer = fromPlayer;
  _fowUpdate -= FOW_UPDATE_RATE; // update immediately

  // record sides
  _fowSides.Clear();
  if (_fow == FOWAllSides)
  {
    for (int i=0; i<_fowFrom.Size(); i++)
    {
      Object *obj = _fowFrom[i];
      Entity *ai = dyn_cast<EntityAI>(obj);
      if (!ai)
        continue;
      TargetSide side = ai->GetTargetSide();
      if (side >= TSideUnknown || obj->IsDamageDestroyed()) 
        continue;
      if (_fowSides.Find((int)side) == -1)
        _fowSides.Add((int)side);
    }
  }

  // hide editor objects not included in _fowFrom
  for (int i=0; i<_ws.NObjects(); i++)
  {
    EditorObject *obj = _ws.GetObject(i);
    Object *proxy = obj->GetProxyObject();
    if (proxy)
    {
      if (_fowFromPlayer && proxy == GWorld->PlayerOn()) continue;
      if (_fowFrom.Size() > 0)
      {
        int j=0;
        for (j=0; j<_fowFrom.Size(); j++)
        {
          if (proxy == _fowFrom[j])
            break;
        }
        if (j < _fowFrom.Size()) continue;
      }
      obj->SetScope(EOSHidden);
    }
  }
}

/*
class ProcessDeadUnits
{
  RefArray<Person> &_deadUnits;
public:
  ProcessDeadUnits(RefArray<Person> &array):_deadUnits(array){}
  __forceinline bool operator () (Entity *veh) const
  {
    Person *person = dyn_cast<Person>(veh);
    if (person && person->IsDamageDestroyed())
    {
      AIBrain *unit = person->CommanderUnit();
      if (!unit || !unit->GetGroup()) 
        _deadUnits.Add(person);
    }
    return false;
  }
};
*/

void DisplayMissionEditorRealTime::ImportAllGroups()
{
  GameState *gstate = GWorld->GetGameState();

  // parse groups
  RefArray<AICenter> centers;
  if (GWorld->GetWestCenter()) centers.Add(GWorld->GetWestCenter());
  if (GWorld->GetEastCenter()) centers.Add(GWorld->GetEastCenter());
  if (GWorld->GetGuerrilaCenter()) centers.Add(GWorld->GetGuerrilaCenter());
  if (GWorld->GetCivilianCenter()) centers.Add(GWorld->GetCivilianCenter());

  for (int i=0; i<centers.Size(); i++)
  {
    AICenter *aCenter = centers[i];
    for (int j=0; j<aCenter->NGroups(); j++)
    {
      AIGroup *aGroup = aCenter->GetGroup(j);
      // add group to editor if it's not already added
      AIUnit *leader = aGroup->Leader(); 
      if (!leader) continue;
      Person *leaderPerson = leader->GetPerson();
      if (!leaderPerson) continue;
      RefArray<AIUnit> units;
      units.Add(leader);
      /*
      int nAlive = 0;
      for (int k=0; k<aGroup->NUnits(); k++)  // add leader first
      {
        AIUnit *unit = aGroup->GetUnit(k); 
        if (!unit) continue;        
        Person *person = unit->GetPerson();
        if (!person) continue;        
        if (!person->IsDamageDestroyed()) 
          nAlive++;
        else
        {
          CreateUnitEdObj(person,NULL,RString(),RString(),RString());
          continue;
        }        
        if (unit == leader)
        {
          units.Add(unit);
          break;
        }
      }
      if (nAlive == 0) continue;
      */
      for (int k=0; k<aGroup->NUnits(); k++)  // add subordinates
      {
        AIUnit *unit = aGroup->GetUnit(k); 
        if (!unit) continue;
        if (unit != leader)
          units.Add(unit);
      }
      // add/find group
      RString grpEdObj;
      EditorObject *grpObj = _map->FindEditorObject(GameValueExt(aGroup),"group");
      if (grpObj)
        grpEdObj = grpObj->GetArgument("VARIABLE_NAME");
      else if (units.Size() > 1 || aGroup->NWaypoints() > aGroup->GetCurrent()->_fsm->Var(0))
      {
        EntityAI *vehicle = leader->GetVehicle();
        GameArrayType array;
        array.Add("POSITION");
        array.Add(Format("[%.5f,%.5f,%.5f]",vehicle->Position().X(),vehicle->Position().Z(),vehicle->Position().Y()));
        array.Add("SIDE");
        array.Add(CreateGameSide(leaderPerson->GetTargetSide()));
        GameVarSpace *vars = aGroup->GetVars();
        RString text = aGroup->GetDebugName();
        if (vars)
        {  
          GameValue var;
          if (vars->VarGet("EDITOR_DESC", var)) 
          {
            RString str = var;
            if (str.GetLength() > 0)
            {
              text = str;
              array.Add("NAME");
              array.Add(text);
            }
          }
        }
        array.Add("DISPLAY_NAME");
        array.Add(text);
        array.Add("DISPLAY_NAME_TREE");
        array.Add(text);          
        AutoArray<RString> args;
        if (!ConvertArguments(gstate,array,args)) continue;
        grpEdObj = _map->AddObject("group", args, false, GameValueExt(aGroup));
        EditorObject *obj = _ws.FindObject(grpEdObj);
        if (obj)
        {
          obj->SetArgument("PARENT",grpEdObj);
          obj->SetVisibleIfTreeCollapsed(true);

          // execute update script, this fixes links
/*
          QOStrStream out;
          obj->WriteUpdateScript(out);
          GameState *gstate = GWorld->GetGameState();
          gstate->BeginContext(&_vars);
          gstate->Execute(RString(out.str(), out.pcount()), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
          gstate->EndContext();
*/
        }
      }
      // add units belonging to group
      RString leaderEdObj;
      for (int k=0; k<units.Size(); k++)
      {
        AIUnit *unit = units[k];       
        Person *person = unit->GetPerson();
        if (!person) continue; // || person->IsDamageDestroyed()) continue;
        Transport *trans = dyn_cast<Transport>(unit->GetVehicle());
        RString vehEdObj;
        if (trans && unit->GetLifeState()!=LifeStateDead) vehEdObj = CreateVehEdObj(trans,grpEdObj);
        RString unitEdObj = CreateUnitEdObj(person,trans,vehEdObj,grpEdObj,leaderEdObj);
        if (units.Size() == 1)
        {
          EditorObject *obj = _ws.FindObject(unitEdObj);
          if (obj) obj->SetVisibleIfTreeCollapsed(true);
          obj = _ws.FindObject(vehEdObj);
          if (obj) obj->SetVisibleIfTreeCollapsed(true);
        }
        if (k == 0)
        {
          leaderEdObj = unitEdObj;  // remember leader
          CreateWaypointEdObjs(aGroup,grpEdObj,unitEdObj);
        }
      }
    }
  }
  //-!

  // import dead units that have no group
  /*
  RefArray<Person> deadUnits;
  ProcessDeadUnits build(deadUnits);
  GWorld->ForEachVehicle(build);
  GWorld->ForEachOutVehicle(build);

  for (int i=0; i<deadUnits.Size(); i++)
  {  
    Person *person = deadUnits[i];  // person is valid, confirmed in ProcessDeadUnits
    AIBrain *unit = person->CommanderUnit();
    Transport *trans = unit ? dyn_cast<Transport>(unit->GetVehicle()) : NULL;
    RString vehEdObj;
    if (trans) vehEdObj = CreateVehEdObj(trans,RString());
    RString unitEdObj = CreateUnitEdObj(person,trans,vehEdObj,RString(),RString());
  }
  */
  //_!
}

RString DisplayMissionEditorRealTime::CreateVehEdObj(Transport *trans, RString grpEdObj)
{
  GameState *gstate = GWorld->GetGameState();
  RString vehEdObj;
  EditorObject *vehObj = _map->FindEditorObject(GameValueExt(trans),"vehicle");          
  if (!vehObj)
  {
    // insert vehicle into editor
    RString special = "CAN_COLLIDE";
    ObjectTyped *objTyped = dyn_cast<ObjectTyped>(trans);
    if (objTyped)
    {
      Ref<const EntityType> type = VehicleTypes.New("Air");
      if (type && objTyped->GetEntityType()->IsKindOf(type) && trans->Airborne())
        special = "FLY";
    }
    GameArrayType array;
    array.Add("POSITION");
    array.Add(Format("[%.5f,%.5f,%.5f]",trans->Position().X(),trans->Position().Z(),trans->Position().Y()));
    array.Add("NAME");
    array.Add(trans->GetVarName());
    array.Add("PARENT");
    array.Add(grpEdObj);
    array.Add("SPECIAL");
    array.Add(special);
    array.Add("HEALTH");
    array.Add(1 - trans->GetTotalDamage());
    array.Add("TYPE");
    ObjectTyped *ent = dyn_cast<ObjectTyped>(trans);
    RString type = "";
    if (ent)
    {
      const EntityType *etype = ent->GetNonAIType();
      if (etype) type = etype->GetName();
    }
    array.Add(type);
    array.Add("AZIMUT");
    Vector3Val dir = trans->Direction();
    float azimut = atan2(dir.X(), dir.Z()) * (180.0f / H_PI); if (azimut < 0) azimut += 360.0f;
    array.Add(azimut);
    AutoArray<RString> args;
    if (ConvertArguments(gstate,array,args))
      vehEdObj = _map->AddObject("vehicle", args, false, GameValueExt(trans));
/*
    if (units.Size() == 1)
    {
      EditorObject *obj = _ws.FindObject(vehEdObj);
      if (obj)
        obj->SetVisibleIfTreeCollapsed(true);
    }
*/
  }
  else
    vehEdObj = vehObj->GetArgument("VARIABLE_NAME");
  return vehEdObj;
}

RString DisplayMissionEditorRealTime::CreateUnitEdObj(Person *person, Transport *trans, RString vehEdObj, RString grpEdObj, RString leaderEdObj)
{
  GameState *gstate = GWorld->GetGameState();
  GameArrayType array;
  if (trans)
  {
    if (person == trans->Driver())
      array.Add("DRIVER");
    else if (person == trans->Observer())
      array.Add("COMMANDER");
    else if (person == trans->Gunner())
      array.Add("GUNNER");
    else
    {
      // multiple gunners
      TurretContext context;
      if (trans->FindTurret(person, context) && context._gunner)
        array.Add("GUNNER");
      else
        array.Add("CARGO");
    }
    array.Add(vehEdObj);
  }

 // playable?
  GameVarSpace *vars = person->GetVars();
  if (vars)
  {
    GameValue var;
    if (vars->VarGet("IS_PLAYABLE", var))
    {
      array.Add("PLAYABLE");
      array.Add(var);
    }
    if (vars->VarGet("DESCRIPTION", var))
    {
      array.Add("DESCRIPTION");
      array.Add(var);
    }
    if (vars->VarGet("URN", var))
    {
      array.Add("URN");
      array.Add(var);
    }
  }
  array.Add("NAME");
  array.Add(person->GetVarName());
  array.Add("HEALTH");
  array.Add(1 - person->GetTotalDamage());   
  array.Add("PARENT");
  if (!person->IsDamageDestroyed())
    array.Add(grpEdObj);   
  else
    array.Add("");   
  array.Add("TYPE");
  ObjectTyped *ent = dyn_cast<ObjectTyped>(person);
  RString type = "";
  if (ent)
  {
    const EntityType *etype = ent->GetNonAIType();
    if (etype) type = etype->GetName();
  }
  array.Add(type);
  array.Add("AZIMUT");
  Vector3Val dir = person->Direction();
  float azimut = atan2(dir.X(), dir.Z()) * (180.0f / H_PI); if (azimut < 0) azimut += 360.0f;
  array.Add(azimut);
  array.Add("POSITION");
  array.Add(Format("[%.5f,%.5f,%.5f]",person->Position().X(),person->Position().Z(),person->Position().Y()));
  array.Add("PLAYER");
  array.Add(GWorld->PlayerOn() == person);
  array.Add("LEADER");
  array.Add(leaderEdObj);
  AutoArray<RString> args;
  bool valid = ConvertArguments(gstate,array,args);
  RString unitEdObj;
  // add/find unit
  EditorObject *unitObj = _map->FindEditorObject(GameValueExt(person),"unit");
  if (unitObj)
  {
    unitObj->SetArguments(args);
    unitEdObj = unitObj->GetArgument("VARIABLE_NAME");          
  }
  else if (valid)
    unitEdObj = _map->AddObject("unit", args, false, GameValueExt(person));
  return unitEdObj;
}

void DisplayMissionEditorRealTime::DestroyHUD(int exit)
{
  // perhaps RTE was started as a user dialog?
  if (GWorld->UserDialog())
    GWorld->DestroyUserDialog();
}

void DisplayMissionEditorRealTime::InitRTE()
{
  //p = ProgressStartExt(false, LocalizeString(IDS_LOAD_WORLD));  
  IProgressDisplay *CreateDisplayLoadIsland(RString text, RString date);
  p = ProgressStart(CreateDisplayLoadIsland("Loading...", RString()));
  ProgressAdd(9);
  ProgressRefresh();

  SetCursor("Wait");
  DrawCursor();     

  GVBSVisuals.rteVisible = true; //set global variable

  _enableDisplay = false;
  _enableSimulation = true;
  _alwaysShow = true;
  _menuTypeActive = EMNone; 
  _switchToTactical = false;
  _cleanExit = false;
  _fow = FOWOff;
  _fowFromPlayer = true;

  _realTime = true;  

  if (GWorld->GetTitleEffect())
  {
    GWorld->GetTitleEffect()->Terminate();
    GWorld->LockTitleEffect(false);
  }

  //Load(_clsName);

  // clean up landscape
  _vars._vars.Clear();
  _originalPlayer = NULL;
  _playerUnit = NULL;
  _proxies.Clear();
  
  //GWorld->SwitchLandscape(Glob.header.worldname);

  // fill object type list
  _objectTypeList->RemoveAll();
  for (int i=0; i<_ws.NObjectTypes(); i++)
  {    
    const EditorObjectType *type = _ws.GetObjectType(i);
    int index = _objectTypeList->AddString(type->GetName() + type->GetShortcutKeyDesc());
    _objectTypeList->SetData(index, type->GetName());
  }
  if (_objectTypeList->Size() > 0)
    _objectTypeList->SetCurSel(0);

  // set up some local variables automatically
  // TODO: remove other _map variable definitions?
  _vars.VarLocal("_map");
  _vars.VarSet("_map", CreateGameControl(_map), true);

  // create camera (cursor)
  _camera = new MissionEditorCamera(this);
  if (_miniMap) _miniMap->SetEditorCamera(_camera);

  // create mission editor objects
  if (_importSQF.GetLength() > 0)
  {
    // read and preprocess specified import script
    RString filename = _importSQF;
    FilePreprocessor preproc;
    QOStrStream processed;
    if (preproc.Process(&processed, filename))
    {
      RString script(processed.str(),processed.pcount());
      GameState *gstate = GWorld->GetGameState();
      gstate->BeginContext(&_vars);
      gstate->Execute(script, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      gstate->EndContext();
    }
  }
  else
  {
    // read and preprocess import scripts defined in CfgRealTimeEditorInit
    ParamEntryVal clsImport = Pars >> "CfgRealTimeEditorInit";
    int n = clsImport.GetEntryCount();
    for (int i=0; i<n; i++)
    {
      ParamEntryVal entry = clsImport.GetEntry(i);
      if (entry.IsClass()) continue;
      RString filename = entry;
      FilePreprocessor preproc;
      QOStrStream processed;
      if (preproc.Process(&processed, filename))
      {
        RString script(processed.str(),processed.pcount());
        GameState *gstate = GWorld->GetGameState();
        gstate->BeginContext(&_vars);
        gstate->Execute(script, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
        gstate->EndContext();
      }
    }
  }

  // switch camera to cursor
  _originalPlayer = GWorld->PlayerOn();
  SwitchToCamera();

  UpdateObjectBrowser();

  // select editor mode
  SetMode(EMMap);

  // FIX: remove mission fade in effect
  GWorld->ClearCutEffects();

  DoInit(_initSQF);

  GInput.ResetGameFocus();  // game focus back to zero (reqd if launching from options)

  // display map integration
  ShowWalkieTalkie(false);
  for (int i=sensorsMap.Size()-1; i>=0; i--)
  {
    Vehicle *veh = sensorsMap[i];
    if (!veh) continue;
    Detector *det = dyn_cast<Detector>(veh);
    Assert(det);
    if (det->IsActive() && !det->IsRepeating()) continue;
    if
    (
      det->GetActivationBy() >= ASAAlpha &&
      det->GetActivationBy() <= ASAJuliet
    )
    {
      ShowWalkieTalkie(true);
      break;
    }
  }

  ProgressAdvance(9);
  ProgressRefresh();
  ProgressFinish(p); 

  SetCursor("Arrow");
}

void DisplayMissionEditorRealTime::CleanUp()
{
#if _VBS3
  if(GWorld->IsPaused() && Glob.vbsAdminMode )
  {
    void PauseSimulation(bool pause);
    PauseSimulation(false);
  }
#endif
  // clear overlay
  SetActiveTemplateOverlay(NULL);
  
  GVBSVisuals.rteVisible = false;
  GVBSVisuals.editor3DVisible = false;
 
  // delete helper proxies
  for (int i=0; i<_proxies.Size(); i++)
  {      
    Object *objLink = _proxies[i].object;
    Entity *veh = dyn_cast<Entity>(objLink);
    if (!veh) continue;      
    if (!_proxies[i].proxyVisibleInPreview)
      veh->SetDelete();
  }

  EnableSimulationForSelected(true);

  // switch camera back to original player
  if (_miniMap) _miniMap->SetEditorCamera(NULL);
  GWorld->SetCameraEffect
  (
    CreateCameraEffect(_camera, "terminate", CamEffectBack, true)
  );
  _camera->SetDelete();

  Person *player = GWorld->PlayerOn();
  if (player)
  {
    AIBrain *brain = player->Brain();
    if (brain)
    {
      EntityAI *veh = brain->GetVehicle();
      if (veh)
        GWorld->SwitchCameraTo(veh, CamInternal, true);
    }
    GWorld->SwitchPlayerTo(player);
    GWorld->SetPlayerManual(true);
    GWorld->UI()->ResetHUD();    
    player->EditorDisableSimulation(false);
  }  

  // remove global map variable
    // this was initially set in import_objects.sqf
  GWorld->GetGameState()->VarDelete("map", GWorld->GetMissionNamespace());
}

void DisplayMissionEditorRealTime::Destroy()
{
  CleanUp();

  if (_switchToTactical)
  {
    GWorld->SetCameraTypeWanted(CamGroup);
    AbstractUI *ui = GWorld->UI();
    if (ui) 
    {
      ui->InitCamGroupCameraPos();
      ui->ShowMe();
    }
  }
  
  _cleanExit = true;

  DisplayMissionEditor::Destroy();
}

// required if application was closed using ALT+F4
// Bug: Causes CTD to desktop on ALT+F4, using a clean up event
// on buffers that no longer exsist in memory.
DisplayMissionEditorRealTime::~DisplayMissionEditorRealTime()
{
  /*
  if (!_cleanExit)
  {
    // delete camera etc
    CleanUp();

    // clear editor variables (execute unload EH for the editor)
    OnEvent(DEUnload, IDC_CANCEL);
  }
  */
}

void DisplayMissionEditorRealTime::OnMenuSelected(MenuItem *item)
{
  switch (item->_cmd)
  {
  case ECSwitchToVehicle:
    {
      const IAttribute *attr = item->_attributes[attrObject];
      Assert(attr);
      RString id = GetAttrString(attr);      
      SwitchToVehicle(id);
    }
    return;
  case ECMenuFileExit:
    {
      Exit(IDC_CANCEL);
    }
    return;
  case ECMenuViewCommand:
    {
      AIBrain *focusOn = GWorld->FocusOn();
      if (focusOn && focusOn->IsGroupLeader() && focusOn->GetGroup()->UnitsCount() > 1)
      {
        _switchToTactical = true;
        Exit(IDC_CANCEL);
      }
    }
    return; 
  }
  DisplayMissionEditor::OnMenuSelected(item);
}

bool DisplayMissionEditorRealTime::OnMenuCreated(Menu &menu, float x, float y, int idc, CCMContext &ctx)
{
  CCMContextEditor &ctxEd = static_cast<CCMContextEditor &>(ctx);
  _menuTypeActive = ctxEd.type;
  switch (ctxEd.type)
  {
  case EMFile:
    {
      if (_map && _map->_showCountourInterval) {_map->_showCountourInterval = false; _reShowLegend = true;}

      MenuItem *item;

      if (_allowFileOperations && !_ws.GetActiveOverlay())
      {
        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_FILE_SAVE"), 0, ECMenuFileSave);
        menu.AddItem(item);
      }

      item = new MenuItem(LocalizeString("STR_CA_MAIN_OPTIONS"), 0, ECMenuFileOptions);
      menu.AddItem(item);

      item = new MenuItem("", 0, CMD_SEPARATOR);
      menu.AddItem(item); 

      InsertAdditionalMenuItems(menu,EMFile);

      item = new MenuItem(LocalizeString("STR_EDITOR_MENU_FILE_EXIT"), 0, ECMenuFileExit);
      menu.AddItem(item);
    }
    return true;
  case EMView:
    {
      if (_map && _map->_showCountourInterval) {_map->_showCountourInterval = false; _reShowLegend = true;}

      MenuItem *item = new MenuItem(LocalizeString("STR_EDITOR_MENU_VIEW_2D"), 0, ECMenuView2D);
      menu.AddItem(item);

      if (_allow3DMode)
      {
        item = new MenuItem(LocalizeString("STR_EDITOR_MENU_VIEW_3D"), 0, ECMenuView3D);
        menu.AddItem(item);
      }

      AIBrain *focusOn = GWorld->FocusOn();
      if (focusOn && focusOn->IsGroupLeader() && focusOn->GetGroup()->UnitsCount() > 1)
      {
        MenuItem *item = new MenuItem("Command View", 0, ECMenuViewCommand);
        menu.AddItem(item);
      }

      InsertAdditionalMenuItems(menu,EMView);
    }
    return true;
  }
  return DisplayMissionEditor::OnMenuCreated(menu, x, y, idc, ctx);
}

bool DisplayMissionEditorRealTime::SwitchToVehicle(RString id)
{
  // prohibit taking over human players
  Object *obj = FindObject(id);
  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (!veh) return false;
  AIBrain *brain = veh->PilotUnit();
  if (!brain) return false;
  Person *person = brain->GetPerson();
  if (!person) return false;
  if (GWorld->GetMode() == GModeNetware && GWorld->PlayerOn() != person && person->IsNetworkPlayer())
  {
    GWorld->CreateWarningMessage(LocalizeString("STR_EDITOR_ERROR_CTRL_PLAYER_UNIT"));
    return false;
  }
  //--!

  if (!DisplayMissionEditor::SwitchToVehicle(id))
    return false;

  person = _playerUnit->GetPerson();
  if (!person) return false;

  // multiplayer support - take full control of unit  
  if (GWorld->GetMode() == GModeNetware)
  {
    if (_originalPlayer)
      _originalPlayer->SetRemotePlayer(1);
    GetNetworkManager().SelectPlayer
    (
      GetNetworkManager().GetPlayer(), person, false
    );
  }  

  Glob.header.playerSide = person->GetTargetSide();

  WeaponsState &weapons = person->GetWeapons();
  weapons.SelectWeapon(person, weapons.FirstWeapon(person), true);

#ifndef _XBOX
  OnEditorEvent(MESwitchToVehicle,_originalPlayer,person);
#endif

  Exit(IDC_CANCEL);
  return true;
}

void DisplayMissionEditorRealTime::OnSimulate(EntityAI *vehicle)
{  
  bool IsAppPaused();
  if (IsAppPaused()) return;

  ProgressAdvance(9);
  ProgressRefresh();
  ProgressFinish(p); // finish it off

  if (GWorld->GetMode() == GModeNetware)
    if (GetNetworkManager().GetServerState() != NSSPlaying) 
    {
      Exit(IDC_CANCEL);
      return;
    }

  // switch out to tactical view?
  if (GInput.GetActionToDo(UATacticalView))
  {
    AIBrain *focusOn = GWorld->FocusOn();
    if (focusOn && focusOn->IsGroupLeader() && focusOn->GetGroup()->UnitsCount() > 1)
    {
      _switchToTactical = true;
      Exit(IDC_CANCEL);
      return;
    }
  }

  SimulateFogOfWar();  

  DisplayMissionEditor::OnSimulate(vehicle);
}

bool DisplayMissionEditorRealTime::OnKeyDown(int dikCode)
{
  switch (dikCode)
  {
  // no preview in real time mode
  case DIK_H:
    return true;
  case DIK_ESCAPE:
    if (_linkObject || (_map && _map->GetLinkObject()))
      break;
  case DIK_M:
    if (!_exitOnMapKey && dikCode == DIK_M)
      return true;
    else if (!_allow3DMode || dikCode == DIK_ESCAPE)
    {
      if (!_ws.GetActiveOverlay())
      {
        Exit(IDC_CANCEL);
        return true;
      }
      else
        dikCode = DIK_ESCAPE; // show save overlay dialogs
    }
    break;
  }
  return DisplayMissionEditor::OnKeyDown(dikCode);
}

void DisplayMissionEditorRealTime::OnButtonClicked(int idc)
{
  // no interrupt display, go back to map for now (later just exit)
  //if (idc == IDC_CANCEL && _mode == EM3D)
    //idc = IDC_EDITOR_MAP;
  DisplayMissionEditor::OnButtonClicked(idc);
}

void DisplayMissionEditorRealTime::OnChildDestroyed(int idd, int exit)
{
  switch (idd)
  {
  case IDD_MISSION_SAVE:
    if (exit == IDC_OK)
    {      
      // TODO: copy all mission contents (scripts etc) automatically when saving to user directory?

      DisplayMissionSave *child = static_cast<DisplayMissionSave *>(_child.GetRef());
      RString userDir = GetUserDirectory(); 
      RString island = Glob.header.worldname;
      _mission = EncodeFileName(child->GetMission());

      // update position and azimuth arguments based on current proxy position
      for (int i=0; i<_ws.NObjects(); i++)
      {
        EditorObject *obj = _ws.GetObject(i);
        Object *proxy = obj->GetProxyObject();
        if (proxy) 
        {
          Vector3 pos = proxy->Position();
          obj->UpdateArgument("POSITION", Format("[%.5f, %.5f, %.5f]", pos.X(), pos.Z(), pos.Y() - ObjOffset(proxy)), true);
          obj->UpdateAzimuth();
        }
      }

      // order objects by link dependancy
      _ws.OrderAllObjects();

      // save mission  
      RString directory = userDir + MPMissionsDir + _mission + "." + island + RString("\\");
      RString path = directory + RString("mission.biedi");      
      void CreatePath(RString path);
      CreatePath(path);
      _ws.Save(path);
      _ws.WriteCreateScript(directory + RString("mission.sqf"));
      SaveSQM(directory);
      _hasChanged = false;

#if _VBS3
      int i = directory.GetLength() - 1;
      if (i>=0 && directory[i] == '\\') 
        directory = directory.Substring(0, i); // remove trailing '\'
#endif

      switch (child->GetPlacement())
      {
      case MPSingleplayer:
        {
          RString bankName =
            RString("Missions\\") +
            _mission + RString(".") +
            RString(Glob.header.worldname) + RString(".pbo");
          DoVerify(QIFileFunctions::CleanUpFile(bankName));
          FileBankManager mgr;
          mgr.Create(bankName, directory);
        }
        break;
      case MPMultiplayer:
        {
          RString bankName =
            RString("MPMissions\\") +
            _mission + RString(".") +
            RString(Glob.header.worldname) + RString(".pbo");
          DoVerify(QIFileFunctions::CleanUpFile(bankName));
          FileBankManager mgr;
          mgr.Create(bankName, directory);
        }
        break;
      }
    }
    idd = -1;
    break;
  }
  DisplayMissionEditor::OnChildDestroyed(idd, exit);
}

#endif

#endif // _ENABLE_EDITOR2
