#ifdef _MSC_VER
#pragma once
#endif

#ifndef _DETECTOR_HPP
#define _DETECTOR_HPP

#define LOG_FLAG_CHANGES 0

//#include "house.hpp"
#include "arcadeWaypoint.hpp"
#include "AI/ai.hpp"
#include "rtAnimation.hpp"

class SoundObject;
class DynSoundObject;
class GameValue;

#define NON_AI_DETECTOR 1
#if NON_AI_DETECTOR
  typedef Vehicle DetectorBase;
#else
  typedef EntityAI DetectorBase;
#endif

#include "objectId.hpp"

#if _VBS3 // trigger activate EH
  struct TriggerActivationEH
  {
    RString onActivation;
    int id;
    TriggerActivationEH(RString a, int i) {onActivation = a; id = i;}
  };
  TypeIsMovableZeroed(TriggerActivationEH)
#endif


class Detector : public DetectorBase
{
  friend class CStaticMapMain; // drawing
protected:
  typedef DetectorBase base;
#if _VBS3 // trigger area fix
  bool _swapped;
#endif
  float _a;
  float _b;
  float _e;
  float _sinAngle;
  float _cosAngle;
  bool _rectangular;
#if _VBS2 // trigger get area
  float _angle;
#endif

  ArcadeSensorActivation _activationBy;
  ArcadeSensorActivationType _activationType;
  OLinkPermNO(AIGroup) _assignedGroup;
  ObjectId _assignedStatic;
  int _assignedVehicle;
  bool _repeating;
  bool _interruptable;
  float _timeoutMin;
  float _timeoutMid;
  float _timeoutMax;

  ArcadeSensorType _action;
  RString _text;

  RString _expCond;
  RString _expActiv;
  RString _expDesactiv;

#if USE_PRECOMPILATION
  CompiledExpression _compiledCond;
  CompiledExpression _compiledActiv;
  CompiledExpression _compiledDesactiv;
#endif

#if _VBS3 // quick attach objects to trigger
  OLinkArray(Object) _attachedObjects;
#endif

  AutoArray<int> _synchronizations;

  ArcadeEffects _effects;

  Time _nextCheck;
  Time _countdown;
  bool _active;
  bool _activeCountdown;

  SRef<GameValue> _vehicles;	

  Ref<DynSoundObject> _dynSound;
  Ref<SoundObject> _sound;
  Ref<SoundObject> _voice;
  OLink(Object) _voiceObject;

#if NON_AI_DETECTOR
  // TODO: create VehicleWithType
  Ref<const EntityType> _type;
  const EntityType *GetType() const {return _type;}
#endif

#if _VBS3 // trigger activate EH
  AutoArray<TriggerActivationEH> _onActivate;
  int _onActivateCounter;
#endif
#if NON_AI_DETECTOR
  /// attached space of variables
  GameVarSpace _vars;
#endif // otherwise, the _vars are implemented by the DetectorBase

public:
  Detector(const EntityType *name, const CreateObjectId &id);
  ~Detector();

  void FromTemplate(const ArcadeSensorInfo &info);
  void AssignGroup(AIGroup *group);
  void AssignStatic(const ObjectId &id);
  void AssignVehicle(int id);

  void SetArea(float a, float b, float angle, bool rectangular);
  void SetActivation(ArcadeSensorActivation by, ArcadeSensorActivationType type, bool repeating);
  void SetTriggerType(ArcadeSensorType type);
  void SetTimeout(float min, float mid, float max, bool interruptable);
  void AttachVehicle(EntityAI *vehicle);
#if _VBS3 // quick attach objects to trigger
  void AttachObject(Object *obj);
  void DetachObject(Object *obj);
  OLinkArray(Object) &GetAttachedObjs() {return _attachedObjects;}
#endif
  void SetStatements(RString cond, RString activ, RString desactiv);
  void SetSynchronizations(AutoArray<int> synchronizations) {_synchronizations = synchronizations;}
  ArcadeEffects &GetEffects() {return _effects;}
  void ReActivateSounds();

  void GetArea(float &a, float &b, float &angle, bool &rect) 
  {
#if _VBS2
    a = _swapped ? _b : _a;
    b = _swapped ? _a : _b;
#else
    a = _a; b = _b;
#endif
    angle = atan2(_sinAngle, _cosAngle); rect = _rectangular;
  }
  void GetTimeout(float &min, float &mid, float &max, bool &interruptable) {min = _timeoutMin; mid = _timeoutMax; max = _timeoutMax; interruptable = _interruptable;}
  void GetStatements(RString &cond, RString &activ, RString &desactiv) {cond = _expCond; activ = _expActiv; desactiv = _expDesactiv;}

  bool SimulationReady(SimulationImportance prec) const;
  void Simulate( float deltaT, SimulationImportance prec );
  bool QIsManual()const {return false;}

  TargetSide GetTargetSide() const {return _targetSide;}

  int GetActivationBy() const {return _activationBy;}
  int GetActivationType() const {return _activationType;}
  int GetAction() const {return _action;}
  int NSynchronizations() const {return _synchronizations.Size();}
  int GetSynchronization(int i) const {return _synchronizations[i];}
  float GetCountdown();
  RString GetText() const {return _text;}
  void SetText(RString text) {_text = text;}

  bool IsActive() const {return _active;}
  bool IsCountdown() const {return _activeCountdown;}
  bool IsRepeating() const {return _repeating;}

  /// return the vehicle attached to detector (if _activationBy is ASAVehicle)
  EntityAI *GetAttachedVehicle() const;

  Object *GetActiveVehicle();
  int NVehicles() const;
  const EntityAI *GetVehicle(int i) const;
  EntityAI *GetVehicle(int i);

  const GameValue &GetGameValue() const;

  void DoActivate();
#if _VBS3 // trigger activate EH
  int SetOnActivateEH(RString onActivate);
  void ClearOnActivateEH(int i);
#endif

#if NON_AI_DETECTOR
  /// access to attached space of variables
  virtual GameVarSpace *GetVars() {return &_vars;}
#endif // otherwise, the _vars are implemented by the DetectorBase

#if _VBS3 // setVehicleVarName now works for triggers
  //! Get name of object in script environment
  virtual RString GetVarName() const {return _varName;}
  //! Remember name of object in script environment
  virtual void SetVarName(RString name) {_varName = name;}
#endif

  LSError Serialize(ParamArchive &ar);

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  static Detector *CreateObject(NetworkMessageContext &ctx);
  void DestroyObject();
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);

protected:
  friend class DetectorScanOneEntity;
  
  bool IsInside(Vector3Par pos, Vector3Par f1, Vector3Par f2);
  bool TestSide(EntityAI *vehicle);
  bool TestSide(const AITargetInfo &target);
  AICenter *GetCenter() const;	// for detected by <center>
  bool TestVehicle(Vehicle *veh, Vector3Par f1, Vector3Par f2);
  bool TestVehicle(AICenter *center, Vehicle *veh, Vector3Par f1, Vector3Par f2);
  /// maintain list of vehicles in the trigger area
  void Scan();
  void OnActivate(Object *obj);	// who is activating
  void OnDesactivate();
  
  /*
  const BuildingType *Type() const
  {
    return static_cast<const BuildingType *>(GetType());
  }
  */

  USE_CASTING(base)
};

/// flag pole type
class FlagCarrierType: public EntityAIType
{
  typedef EntityAIType base;
  friend class FlagCarrier;
  
  float _animSpeed;
  
  public:
  FlagCarrierType( ParamEntryPar param );
  virtual void Load(ParamEntryPar par, const char *shape);
  void InitShape();
  void DeinitShape(); // before shape is unloaded

  void InitShapeLevel(int level);
  void DeinitShapeLevel(int level);

  virtual Object* CreateObject(bool unused) const;

  AnimationSource *CreateAnimationSource(const AnimationType *type, RStringB source);
  bool AbstractOnly() const {return false;}
  bool IsSimulationRequired() const;
  bool IsSoundRequired() const;
};

/// flag pole
// TODO: should have a VisualState?
class FlagCarrier: public EntityAI
{
  typedef EntityAI base;

protected:
  Ref<Texture> _flagTexture;
  TargetSide _flagSide;
  OLinkPermO(Person) _flagOwner;
  OLinkPermO(Person) _flagOwnerWanted;

  bool _down;
  bool _up;
  Time _animStart;

  float _phase;

public:
  FlagCarrier(const EntityAIType *type);
  ~FlagCarrier();

  __forceinline const FlagCarrierType *Type() const
  {
    return static_cast<const FlagCarrierType *>(GetType());
  }

  AnimationStyle IsAnimated(int level) const;
  bool IsAnimatedShadow(int level) const;

  float GetCtrlFlag(const ObjectVisualState &vs) const {return _phase;}

  void GetActions(UIActions &actions, AIBrain *unit, bool now, bool strict);

  Texture *GetFlagTexture();
  Texture *GetFlagTextureInternal();
  void SetFlagTexture(RString name);
  Person *GetFlagOwner() {return _flagOwner;}
  void SetFlagOwner(Person *veh);
  TargetSide GetFlagSide() const;
  void SetFlagSide(TargetSide side);
  void CancelTakeFlag();
  
  bool QIsManual() const {return false;}

  void Simulate( float deltaT, SimulationImportance prec );

  LSError Serialize(ParamArchive &ar);

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);

  USE_CASTING(base)
};

#endif
