// AI - implementation of AI and AICenter

#include "../wpch.hpp"

#include "ai.hpp"
#include "aiDefs.hpp"
#include "aiRadio.hpp"
#include "../global.hpp"
#include "../scene.hpp"
//#include "loadStream.hpp"
#include "../person.hpp"

#include "../landscape.hpp"
#include "../world.hpp"
#include "../dynSound.hpp"
#include "../detector.hpp"
#include "../shots.hpp"

//#include "strIncl.hpp"
#include "../stringtableExt.hpp"
#include <El/Common/randomGen.hpp>
#include "../camEffects.hpp"

#include <El/Common/perfLog.hpp>
#include <El/Common/perfProf.hpp>

#include <Es/Algorithms/qsort.hpp>

#include "../arcadeTemplate.hpp"
#include "../paramArchiveExt.hpp"

#include "../gameStateExt.hpp"
#include <El/HiResTime/hiResTime.hpp>

#include "../Network/network.hpp"
#include "../UI/chat.hpp"

#include <El/Enum/enumNames.hpp>

#include "../UI/uiActions.hpp"
#include "../UI/missionDirs.hpp"

#include "../mbcs.hpp"
#include <Es/Strings/bString.hpp>

#include "../progress.hpp"

#include "../timeManager.hpp"

///////////////////////////////////////////////////////////////////////////////
// Parameters

extern const float CoefAdvanceUnit = 200000;

extern const float AccuracyLast = 7200;             // how long we can remember the information
extern const float ExposureTimeout = 450;  // how long exposure persist when target is not visible

#define LOG_TARGETS 0

///////////////////////////////////////////////////////////////////////////////
// class AI

static const EnumName SemaphoreNames[]=
{
  EnumName(-1, "NO CHANGE", &IDS_NO_CHANGE),
  EnumName(AI::SemaphoreBlue, "BLUE", &IDS_IGNORE),
  EnumName(AI::SemaphoreGreen, "GREEN", &IDS_BYPASS),
  EnumName(AI::SemaphoreWhite, "WHITE", &IDS_WHITE),
  EnumName(AI::SemaphoreYellow, "YELLOW", &IDS_CONTACT),
  EnumName(AI::SemaphoreRed, "RED", &IDS_ENGAGE),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AI::Semaphore dummy)
{
  return SemaphoreNames;
}

static const EnumName FormationNames[]=
{
  EnumName(-1, "NO CHANGE", &IDS_NO_CHANGE),
  EnumName(AI::FormColumn, "COLUMN", &IDS_COLUMN),
  EnumName(AI::FormStaggeredColumn, "STAG COLUMN", &IDS_STAGGERED),
  EnumName(AI::FormWedge, "WEDGE", &IDS_WEDGE),
  EnumName(AI::FormEcholonLeft, "ECH LEFT", &IDS_ECHL),
  EnumName(AI::FormEcholonRight, "ECH RIGHT", &IDS_ECHR),
  EnumName(AI::FormVee, "VEE", &IDS_VEE),
  EnumName(AI::FormLine, "LINE", &IDS_LINE),
  EnumName(AI::FormDiamond, "DIAMOND", &IDS_DIAMOND),
  EnumName(AI::FormFile, "FILE", &IDS_FILE),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AI::Formation dummy)
{
  return FormationNames;
}

static const EnumName RankNames[]=
{
  EnumName(RankUndefined, "UNDEFINED", &DUPLICATE_IDS),
  EnumName(RankPrivate, "PRIVATE", &IDS_PRIVATE),
  EnumName(RankCorporal, "CORPORAL", &IDS_CORPORAL),
  EnumName(RankSergeant, "SERGEANT", &IDS_SERGEANT),
  EnumName(RankLieutenant, "LIEUTENANT", &IDS_LIEUTENANT),
  #if !_ENABLE_INDEPENDENT_AGENTS
  EnumName(RankLieutenant, "LIEUTNANT", &DUPLICATE_IDS), // backward compatibility - old missions contain typo
  #endif
  EnumName(RankCaptain, "CAPTAIN", &IDS_CAPTAIN),
  EnumName(RankMajor, "MAJOR", &IDS_MAJOR),
  EnumName(RankColonel, "COLONEL", &IDS_COLONEL),
  EnumName()
};
template<>
const EnumName *GetEnumNames(Rank dummy)
{
  return RankNames;
}

AITime AI::GetActualTime()
{
  // static member function
  float time=Glob.clock.GetTimeInYear();
  // convert in-day time and in-year time to single int represetation
  const float secPerYear=365*24*60*60;
  return toIntFloor(secPerYear*time);
}

float AI::ExpForRank(Rank rank, bool ingame)
{
  if (rank < RankPrivate)
    return 0;

  if (rank > RankColonel)
    return FLT_MAX;

  if (ingame)
    return 0.8 * ExperienceTable[rank];

  return ExperienceTable[rank];
}

Rank AI::RankFromExp(float exp, bool ingame)
{
  int i;
  for (i=RankPrivate+1; i<=RankColonel; i++)
    if (exp < ExpForRank((Rank)i, ingame))
      return (Rank)(i - 1);
  return RankColonel;
}

AutoArray<float> ExperienceTable;
AutoArray<ExperienceDestroyInfo> ExperienceDestroyTable;

float ExperienceDestroyEnemy;
float ExperienceDestroyFriendly;
float ExperienceDestroyCivilian;
float ExperienceDestroyStatic;

float ExperienceKilled;

float ExperienceRenegadeLimit;
  
// for subordinate soldier only
float ExperienceCommandCompleted;
float ExperienceCommandFailed;
float ExperienceFollowMe;

// for leadership only
float ExperienceDestroyYourUnit; 
float ExperienceMissionCompleted;
float ExperienceMissionFailed;

void AI::InitTables()
{
  ParamEntryVal cls = Pars >> "CfgExperience";
  
  int n = (cls >> "ranks").GetSize(); 
  Assert(n == NRanks);

  ExperienceTable.Resize(n);
  for (int i=0; i<n; i++)
  {
    ExperienceTable[i] = (cls >> "ranks")[i];
  }

  n = (cls >> "destroyUnit").GetSize();
  ExperienceDestroyTable.Resize(n);
  for (int i=0; i<n; i++)
  {
    RString name = (cls >> "destroyUnit")[i];
    ExperienceDestroyTable[i].maxCost = (cls >> name)[0];
    ExperienceDestroyTable[i].exp = (cls >> name)[1];
  }

  ExperienceDestroyEnemy = cls >> "destroyEnemy";
  ExperienceDestroyFriendly = cls >> "destroyFriendly";
  ExperienceDestroyCivilian = cls >> "destroyCivilian";
  ExperienceDestroyStatic = cls >> "destroyStatic";

  ExperienceRenegadeLimit = cls >> "renegadeLimit";


  ExperienceKilled = cls >> "playerKilled";
    
  // for subordinate soldier only
  ExperienceCommandCompleted = cls >> "commandCompleted";
  ExperienceCommandFailed = cls >> "commandFailed";
  ExperienceFollowMe = cls >> "followMe";

  // for leadership only
  ExperienceDestroyYourUnit = cls >> "destroyYourUnit"; 
  ExperienceMissionCompleted = cls >> "missionCompleted";
  ExperienceMissionFailed = cls >> "missionFailed";
}

///////////////////////////////////////////////////////////////////////////////
// AIStats classes

static const EnumName EventTypeNames[]=
{
  EnumName(SETUnitLost, "LOST"),
  EnumName(SETKillsEnemyInfantry, "ENEMY INF"),
  EnumName(SETKillsEnemySoft, "ENEMY SOFT"),
  EnumName(SETKillsEnemyArmor, "ENEMY ARMOR"),
  EnumName(SETKillsEnemyAir, "ENEMY AIR"),
  EnumName(SETKillsFriendlyInfantry, "FRIEND INF"),
  EnumName(SETKillsFriendlySoft, "FRIEND SOFT"),
  EnumName(SETKillsFriendlyArmor, "FRIEND ARMOR"),
  EnumName(SETKillsFriendlyAir, "FRIEND AIR"),
  EnumName(SETKillsCivilInfantry, "CIVIL INF"),
  EnumName(SETKillsCivilSoft, "CIVIL SOFT"),
  EnumName(SETKillsCivilArmor, "CIVIL ARMOR"),
  EnumName(SETKillsCivilAir, "CIVIL AIR"),
  EnumName(SETUser, "USER"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AIStatsEventType dummy)
{
  return EventTypeNames;
}

static const EnumName StatsKillsNames[]=
{
  EnumName(SKEnemyInfantry, "ENEMY INF"),
  EnumName(SKEnemySoft, "ENEMY SOFT"),
  EnumName(SKEnemyArmor, "ENEMY ARMOR"),
  EnumName(SKEnemyAir, "ENEMY AIR"),
  EnumName(SKFriendlyInfantry, "FRIEND INF"),
  EnumName(SKFriendlySoft, "FRIEND SOFT"),
  EnumName(SKFriendlyArmor, "FRIEND ARMOR"),
  EnumName(SKFriendlyAir, "FRIEND AIR"),
  EnumName(SKCivilianInfantry, "CIVIL INF"),
  EnumName(SKCivilianSoft, "CIVIL SOFT"),
  EnumName(SKCivilianArmor, "CIVIL ARMOR"),
  EnumName(SKCivilianAir, "CIVIL AIR"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AIStatsKills dummy)
{
  return StatsKillsNames;
}

AIStatsMPRow::AIStatsMPRow()
{
  _order = 0;
  _side = TSideUnknown;
  _killsInfantry = 0;
  _killsSoft = 0;
  _killsArmor = 0;
  _killsAir = 0;
  _killsPlayers = 0;
  _customScore = 0;
  _liveStats = 0;
  _killsTotal = 0;
  _killed = 0;

  _local = true;
}

AIStatsMPRow::~AIStatsMPRow()
{
  if (!_id.IsNull())
  {
    LogF("Destroying MP statistics row %s %s", (const char *)_name, _local ? "" : "(remote)");
  }
}

void AIStatsMPRow::RecalculateTotal()
{
  _killsTotal = _killsInfantry + 2 * _killsSoft + 3 * _killsArmor + 5 * _killsAir + _customScore;
}

#define AI_STATS_ROW_MSG_LIST(XX) \
  XX(Create, AIStatsMPRowCreate) \
  XX(UpdateGeneric, AIStatsMPRowUpdate)

DEFINE_NETWORK_OBJECT(AIStatsMPRow, NetworkObject, AI_STATS_ROW_MSG_LIST)

DEFINE_NET_INDICES_EX(AIStatsMPRowCreate, NetworkObject, AI_STATS_ROW_MSG_CREATE)
DEFINE_NET_INDICES_EX_ERR(AIStatsMPRowUpdate, NetworkObject, AI_STATS_ROW_MSG_UPDATE, NoErrorInitialFunc)

NetworkMessageFormat &AIStatsMPRow::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  switch (cls)
  {
  case NMCCreate:
    NetworkObject::CreateFormat(cls, format);
    AI_STATS_ROW_MSG_CREATE(AIStatsMPRowCreate, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    NetworkObject::CreateFormat(cls, format);
    AI_STATS_ROW_MSG_UPDATE(AIStatsMPRowUpdate, MSG_FORMAT_ERR)
    break;
  default:
    NetworkObject::CreateFormat(cls, format);
    break;
  }
  return format;
}

TMError AIStatsMPRow::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(AIStatsMPRowCreate)

      TRANSF(dpnid)
      TRANSF(role)
      TRANSF(name)
      TRANSF_ENUM(side)

      if (!ctx.IsSending())
      {
        RefArray<AIStatsMPRow> &table = GStats._mission._tableMP;

        // avoid duplicity in statistics
        int indexCurrent = -1;
        int indexOld = -1;
        if (_dpnid==AI_PLAYER)
        { // new one is AI
          for (int i=0; i<table.Size(); i++)
          { // duplicated statistics have the same role index
            if (table[i]->_role==_role && table[i]->_dpnid==AI_PLAYER)
            {
              if (table[i].GetRef() == this)
                indexCurrent = i;
              else
                indexOld = i;
            }
          }
        }
        else
        { // new one is player
          for (int i=0; i<table.Size(); i++)
          { // duplicated statistics have the same name and side
            if (table[i]->_name==_name && table[i]->_dpnid!=AI_PLAYER)
            {
              if (table[i].GetRef() == this)
                indexCurrent = i;
              else if (table[i]->_side==_side)
                indexOld = i;
              else 
              { // the same name of player but different side. We need to invalidate its former dpnid.
                table[i]->_dpnid = -1; // in order to keep GetRow(dpid) working
              }
            }
          }
        }

        Assert(indexCurrent >= 0);
        if (indexOld >= 0)
        {
          AIStatsMPRow *rowOld = table[indexOld];
          AIStatsMPRow *rowNew = table[indexCurrent];
LogF("Duplicity in statistics:");
LogF(" - old %s, dpnid=%d, role=%d", (const char *)rowOld->_name, rowOld->_dpnid, rowOld->_role);
LogF(" - new %s, dpnid=%d, role=%d", (const char *)rowNew->_name, rowNew->_dpnid, rowNew->_role);
          table.Delete(indexOld);
LogF(" - delete old");
LogF(" - table size = %d", table.Size());
        }

        // sort
        GStats._mission.Sort();
      }
    }
    break;
  case NMCUpdateGeneric:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(AIStatsMPRowUpdate)

      AI_STATS_ROW_MSG_UPDATE(AIStatsMPRowUpdate, MSG_TRANSFER_ERR)

      if (!ctx.IsSending())
      {
        RecalculateTotal();
        GStats._mission.Sort();
      }
    }
    break;
  default:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    break;
  }
  return TMOK;
}

float AIStatsMPRow::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = NetworkObject::CalculateError(ctx);
  if (ctx.GetClass() == NMCUpdateGeneric)
  {
    PREPARE_TRANSFER(AIStatsMPRowUpdate)

    ICALCERR_ABSDIF(int, killsInfantry, ERR_COEF_MODE)
    ICALCERR_ABSDIF(int, killsSoft, 2 * ERR_COEF_MODE)
    ICALCERR_ABSDIF(int, killsArmor, 3 * ERR_COEF_MODE)
    ICALCERR_ABSDIF(int, killsAir, 5 * ERR_COEF_MODE)
    ICALCERR_ABSDIF(int, customScore, ERR_COEF_MODE)
    ICALCERR_ABSDIF(int, liveStats, ERR_COEF_MODE)
    ICALCERR_ABSDIF(int, killed, ERR_COEF_MODE)
  }
  return error;
}

AIStatsMPRow *AIStatsMPRow::CreateObject(ParamArchive &ar)
{
  AIStatsMPRow *row = new AIStatsMPRow();
  row->SetLocal(true);
  return row;
}

AIStatsMPRow *AIStatsMPRow::CreateObject(NetworkMessageContext &ctx)
{
  Ref<AIStatsMPRow> row = new AIStatsMPRow();

  PREPARE_TRANSFER(AIStatsMPRowCreate)

  NetworkId objectId;
  if (TRANSF_BASE(objectCreator, objectId.creator) != TMOK) return NULL;
  if (TRANSF_BASE(objectId, objectId.id) != TMOK) return NULL;
  row->SetNetworkId(objectId);
  row->SetLocal(false);
  
  GStats._mission._tableMP.Add(row);

  row->TransferMsg(ctx);
  if (row->RefCounter() == 1) return NULL;

LogF("Creating remote statistics for player/AI %s, dpnid=%d, role=%d", (const char *)row->_name, row->_dpnid, row->_role);
LogF(" - table size = %d", GStats._mission._tableMP.Size());
  return row;
}

void AIStatsMPRow::DestroyObject()
{
LogF("Destroying statistics for player %s, dpnid=%d, role=%d", (const char *)_name, _dpnid, _role);
  RefArray<AIStatsMPRow> &table = GStats._mission._tableMP;
  for (int i=0; i<table.Size(); i++)
  {
    if (table[i].GetRef() == this)
    {
      table.Delete(i);
LogF(" - deleted");
LogF(" - table size = %d", table.Size());
      return;
    }
  }
}

RString AIStatsMPRow::GetDebugName() const
{
  return Format("Statistics for %s", (const char *)_name);
}

LSError AIStatsMPRow::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("dpid", _dpnid, 1, 1))
  CHECK(ar.Serialize("role", _role, 1, 0))
  CHECK(ar.Serialize("name", _name, 1))
  CHECK(ar.Serialize("side", (int&)_side, 1, TWest))

  CHECK(ar.Serialize("kinf", _killsInfantry, 1, 0))
  CHECK(ar.Serialize("ksoft", _killsSoft, 1, 0))
  CHECK(ar.Serialize("karm", _killsArmor, 1, 0))
  CHECK(ar.Serialize("kair", _killsAir, 1, 0))
  CHECK(ar.Serialize("kplayer", _killsPlayers, 1, 0))
  CHECK(ar.Serialize("customScore", _customScore, 1, 0))
  CHECK(ar.Serialize("liveStats", _liveStats, 1, 0))
  CHECK(ar.Serialize("killed", _killed, 1, 0))

  if (ar.IsLoading())
  {
    RecalculateTotal();
    _local = true; // Load is processed on server, local
    // we need to call GetNetworkManager().CreateObject(this), but it MUST be done after 
    // all other network objects are serialized along with their NetworkId (avoiding identical networkId)
  }
  return LSOK;
}

AIStatsMission::AIStatsMission()
{
  //Clear();

  _size = 0; 
}

void AIStatsMission::Init()
{
  ParamEntryVal cls = Pars >> "CfgInGameUI" >> "MPTable";
  _color = GetPackedColor(cls >> "color");
  _colorSelected = GetPackedColor(cls >> "colorSelected");
  _colorTitleBg = GetPackedColor(cls >> "colorTitleBg");
  _colorWest = GetPackedColor(cls >> "colorWest");
  _colorEast = GetPackedColor(cls >> "colorEast");
  _colorCiv = GetPackedColor(cls >> "colorCiv");
  _colorRes = GetPackedColor(cls >> "colorRes");
  _shadow = cls >> "shadow";
}

const int maxMPTableRows = 10;

/*!
\patch 1.11 Date 07/30/2001 by Jirka
- Fixed: score does not work if squad id entered
\patch_internal 1.11 Date 07/30/2001 by Jirka
- different names used in initialization of table and searching in table
*/
void AIStatsMission::Clear()
{
LogF("Clearing multiplayer statistics");  
  _events.Clear();
  _tableMP.Clear();
#if _ENABLE_VBS
  _vbsHeader.Clear();
  _vbsEvents.Clear();
  _vbsFooter.Clear();
#endif
  _lives = -1;
  _start = Glob.time;

  _font = NULL;
  for (int i=0; i<columns; i++) _pictures[i] = NULL;

  if (GetNetworkManager().GetClientState() < NCSMissionReceived) return;

  // CHANGED: statistics are now created on server (NetworkServer::OnClientStateChanged)
}

void AIStatsMission::AddEvent
(
  AIStatsEventType type, Vector3Par position, RString message,
  const VehicleType *killedType, RString killedName, bool killedPlayer,
  const VehicleType *killerType, RString killerName, bool killerPlayer
)
{
  int index = _events.Add();
  _events[index].type = type;
  _events[index].time = Glob.clock.GetTimeInYear();
  _events[index].position = position;
  _events[index].message = message;

  _events[index].killedType = killedType->GetName();
  _events[index].killedName = killedName;
  _events[index].killedPlayer = killedPlayer;
  _events[index].killerType = killerType->GetName();
  _events[index].killerName = killerName;
  _events[index].killerPlayer = killerPlayer;
}

Person *GetPlayerPerson(EntityAI *vehicle)
{
  if (!vehicle) return NULL;
  Person *person = dyn_cast<Person>(vehicle);
  if (person) return person;
  Transport *trans = dyn_cast<Transport>(vehicle);
  if (!trans) return NULL; // may be Thing or Building

  // check if player is inside
  Person *commander = trans->Observer();
  if (commander && commander->IsNetworkPlayer()) return commander;
  Person *gunner = trans->Gunner();
  if (gunner && gunner->IsNetworkPlayer()) return gunner;
  Person *driver = trans->Driver();
  if (driver && driver->IsNetworkPlayer()) return driver;

  // check if anybody is inside
  if (commander) return commander;
  if (gunner) return gunner;
  return driver;
}

#if _ENABLE_VBS
/*!
\patch_internal 1.78 Date 7/16/2002 by Jirka
- Added: Multiplayer report for VBS
*/

void AIStatsMission::VBSKilled(EntityAI *killed, EntityAI *killer)
{
  Assert(killed);
  Assert(killer);
  
  Person *killedPerson = GetPlayerPerson(killed);
  if (!killedPerson) return;
  Person *killerPerson = GetPlayerPerson(killer);
  if (!killerPerson) return;

  int index = _vbsEvents.Add();
  VBSStatsEvent &event = _vbsEvents[index];
  
  event.type = VBSETKill;
  event.t = Glob.time;

  // killed
  event.unit1.name = killedPerson->GetPersonName();
  event.unit1.type = killed->GetType()->GetName();
  event.unit1.position = killed->FutureVisualState().Position();
  event.unit1.side = killedPerson->Vehicle::GetTargetSide();
  event.unit1.player = killedPerson->IsNetworkPlayer();

  // killer
  event.unit2.name = killerPerson->GetPersonName();
  event.unit2.type = killer->GetType()->GetName();
  event.unit2.position = killer->FutureVisualState().Position();
  event.unit2.side = killerPerson->Vehicle::GetTargetSide();
  event.unit2.player = killerPerson->IsNetworkPlayer();
}

void AIStatsMission::VBSInjured(EntityAI *injured, EntityAI *killer, float damage, RString ammo)
{
  Assert(injured);
  Person *person = GetPlayerPerson(injured);
  if (!person) return;
  Person *killerPerson = GetPlayerPerson(killer);
  if (!killerPerson) return;

  int index = _vbsEvents.Add();
  VBSStatsEvent &event = _vbsEvents[index];
  
  event.type = VBSETInjury;
  event.t = Glob.time;

  // injured
  event.unit1.name = person->GetPersonName();
  event.unit1.type = injured->GetType()->GetName();
  event.unit1.position = injured->FutureVisualState().Position();
  event.unit1.side = person->Vehicle::GetTargetSide();
  event.unit1.player = person->IsNetworkPlayer();

  // killer
  event.unit2.name = killerPerson->GetPersonName();
  event.unit2.type = killer->GetType()->GetName();
  event.unit2.position = killer->FutureVisualState().Position();
  event.unit2.side = killerPerson->Vehicle::GetTargetSide();
  event.unit2.player = killerPerson->IsNetworkPlayer();

  event.value = damage;
  event.text = ammo;
}

void AIStatsMission::VBSAddHeader(RString text)
{
  _vbsHeader.Add(text);
}

void AIStatsMission::VBSAddEvent(RString text)
{
  int index = _vbsEvents.Add();
  VBSStatsEvent &event = _vbsEvents[index];
  
  event.type = VBSETUser;
  event.t = Glob.time;
  event.text = text;
}

void AIStatsMission::VBSAddFooter(RString text)
{
  _vbsFooter.Add(text);
}

#include <Es/Common/win.h>

void ReportOutput(HANDLE report, const char *text)
{
  DWORD cbWritten;
  WriteFile(report, text, strlen(text), &cbWritten, 0);
  WriteFile(report, "\r\n", 2, &cbWritten, 0);
}

void AIStatsMission::WriteVBSStatistics()
{
  HANDLE report = ::CreateFile
  (
    "mpreport.txt",
    GENERIC_WRITE,
    0,
    0,
    OPEN_ALWAYS, //CREATE_ALWAYS,
    FILE_FLAG_WRITE_THROUGH,
    0
  );
  if (!report) return;
  
  SetFilePointer(report, 0, 0, FILE_END);

  BString<512> buffer;

  // Header
  ReportOutput(report, "----------**Start**----------");
  sprintf
  (
    buffer, "Mission: %s.%s",
    (const char *)Glob.header.filenameReal, (const char *)Glob.header.worldname
  );
  ReportOutput(report, buffer);
  for (int i=0; i<_vbsFooter.Size(); i++) ReportOutput(report, _vbsFooter[i]);

  RString sides, players;
  for (int i=0; i<TSideUnknown; i++)
  {
    int PlayersNumber(TargetSide side);
    int count = PlayersNumber((TargetSide)i);
    if (count == 0) continue;
    
    if (sides.GetLength() > 0) sides = sides + RString("/");
    if (players.GetLength() > 0) players = players + RString("/");

    switch (i)
    {
    case TEast:
      sides = sides + LocalizeString(IDS_EAST); break;
    case TWest:
      sides = sides + LocalizeString(IDS_WEST); break;
    case TGuerrila:
      sides = sides + LocalizeString(IDS_GUERRILA); break;
    case TCivilian:
      sides = sides + LocalizeString(IDS_CIVILIAN); break;
    }
    
    players = players + FormatNumber(count);
  }
  sprintf
  (
    buffer, "Players: %s (%s)",
    (const char *)sides, (const char *)players
  );
  ReportOutput(report, buffer);
  
  struct tm *time = localtime(&_time);
  sprintf
  (
    buffer, "Start mission: %d/%d/%d, %d:%02d:%02d",
    time->tm_year + 1900, time->tm_mon + 1, time->tm_mday,
    time->tm_hour, time->tm_min, time->tm_sec
  );
  ReportOutput(report, buffer);

  for (int i=0; i<_vbsHeader.Size(); i++) ReportOutput(report, _vbsHeader[i]);

  // Events
  ReportOutput(report, "----------**Events**----------");
  int totalKills[TSideUnknown], totalKilled[TSideUnknown];
  for (int i=0; i<TSideUnknown; i++)
  {
    totalKills[i] = 0; totalKilled[i] = 0;
  }
  for (int i=0; i<_vbsEvents.Size(); i++)
  {
    VBSStatsEvent &event = _vbsEvents[i];

    int time = toInt(event.t.toFloat());
    int h = time / 3600;
    time -= 3600 * h;
    int m = time / 60;
    time -= 60 * m;
    int s = time;
    
    switch (event.type)
    {
    case VBSETKill:
      {
        sprintf
        (
          buffer, "%2d:%02d:%02d : %s%s killed by %s%s at [%.0f, %.0f]",
          h, m, s,
          (const char *)event.unit1.name, event.unit1.player ? "" : " (AI)",
          (const char *)event.unit2.name, event.unit2.player ? "" : " (AI)",
          event.unit1.position.X(), event.unit1.position.Z()
        );
        ReportOutput(report, buffer);
        totalKills[event.unit2.side]++;
        totalKilled[event.unit1.side]++;
      }
      break;
    case VBSETInjury:
      {
        BString<256> ammo;
        if (event.text.GetLength() > 0) sprintf(ammo, ", ammo %s", (const char *)event.text);
        sprintf
        (
          buffer, "%2d:%02d:%02d : %s%s injured by %s%s at [%.0f, %.0f], damage %.0f%%%s",
          h, m, s,
          (const char *)event.unit1.name, event.unit1.player ? "" : " (AI)",
          (const char *)event.unit2.name, event.unit2.player ? "" : " (AI)",
          event.unit1.position.X(), event.unit1.position.Z(),
          100.0f * event.value, (const char *)ammo
        );
        ReportOutput(report, buffer);
      }
      break;
    case VBSETUser:
      {
        sprintf
        (
          buffer, "%2d:%02d:%02d : %s",
          h, m, s,
          (const char *)event.text
        );
        ReportOutput(report, buffer);
      }
      break;
    }
  }

  // Footer
  ReportOutput(report, "----------**Summary**----------");
  RString kills, killed;
  for (int i=0; i<TSideUnknown; i++)
  {
    int PlayersNumber(TargetSide side);
    int count = PlayersNumber((TargetSide)i);
    if (count == 0) continue;
    
    if (kills.GetLength() > 0) kills = kills + RString("/");
    if (killed.GetLength() > 0) killed = killed + RString("/");

    kills = kills + FormatNumber(totalKills[i]);
    killed = killed + FormatNumber(totalKilled[i]);
  }
  sprintf
  (
    buffer, "Total amount of deaths: %s (%s)",
    (const char *)sides, (const char *)killed
  );
  ReportOutput(report, buffer);
  sprintf
  (
    buffer, "Total amount of kills: %s (%s)",
    (const char *)sides, (const char *)kills
  );
  ReportOutput(report, buffer);
  for (int i=0; i<_vbsHeader.Size(); i++) ReportOutput(report, _vbsFooter[i]);

  ReportOutput(report, "----------**End**----------");
  ReportOutput(report, "");

  CloseHandle(report);
}

#endif

void AIStatsMission::OnMPMissionStart()
{
  time(&_time);
}

/// Try to find existing matching row
int AIStatsMission::FindRowNoAdd(RString playerName, TargetSide playerSide, int dpnid, int playerRole)
{
  if (dpnid!=AI_PLAYER)
  {
    for (int i=0; i<_tableMP.Size(); i++)
      if (_tableMP[i]->_name == playerName && _tableMP[i]->_side == playerSide) return i; // players are identified by names
  }
  else
  {
    //AI possibly identified by role (with dpnid AI_PLAYER)
    for (int i=0; i<_tableMP.Size(); i++)
      if (_tableMP[i]->_role==playerRole && _tableMP[i]->_dpnid==AI_PLAYER) return i;
  }
  return -1;
}

/// Add statistic row when either aiKills or forceAdd is set on
int AIStatsMission::TryAddRow(RString playerName, TargetSide playerSide, int dpnid, int playerRole, bool forceAdd)
{
  const MissionHeader *header = GetNetworkManager().GetMissionHeader();
  if (header && header->_aiKills || forceAdd)
  {
#if _ENABLE_INDEPENDENT_AGENTS
    AIAgent *agent = GWorld->FindAgent(playerRole);
    if (agent)
    {
      Person *person = agent->GetPerson();
      if (person && person->IsAnimal()) return -1;
    }
#endif
    AIStatsMPRow *row = new AIStatsMPRow();
    row->_dpnid = dpnid;
    row->_name = playerName;
    row->_side = playerSide;
    row->_role = playerRole;
    GetNetworkManager().CreateObject(row);
    int index = _tableMP.Add(row);
    LogF("Creating local statistics for AI %s, dpnid=%d, role=%d during FindRow", (const char *)row->_name, row->_dpnid, row->_role);
    LogF(" - table size = %d", _tableMP.Size());
    Sort();
    // sort can change the index of added row, find it again
    for (int i=0; i<_tableMP.Size(); i++)
    {
      if (_tableMP[i]==row)
      {
        index = i;
        break;
      }
    }
    return index;
  }
  return -1;
}

/// Try to find existing matching row or add a new one (if required)
int AIStatsMission::FindRow(RString playerName, TargetSide playerSide, int dpnid, int playerRole, bool forceAdd)
{
  // Try to find existing matching row
  int ix = FindRowNoAdd(playerName, playerSide, dpnid, playerRole);
  if (ix>=0)
  { // use existing with possible update
    if (forceAdd)
    { // update role and dpid values
      AIStatsMPRow *row = _tableMP[ix];
      row->_dpnid = dpnid;
      row->_role = playerRole;
    }
    return ix;
  }
  // Add a row when forceAdd or aiKills are set
  return TryAddRow(playerName, playerSide, dpnid, playerRole, forceAdd);
}

const AIStatsMPRow *AIStatsMission::GetRow(int dpnid) const
{
  for (int i=0; i<_tableMP.Size(); i++)
    if (_tableMP[i]->_dpnid == dpnid) return _tableMP[i];
  return NULL;
}

int CmpMPTableRows(const Ref<AIStatsMPRow> *row1, const Ref<AIStatsMPRow> *row2)
{
  int value = (*row2)->_killsTotal - (*row1)->_killsTotal;
  if (value != 0) return value;
  value = (*row1)->_killed - (*row2)->_killed;
  if (value != 0) return value;
  return stricmp((*row1)->_name, (*row2)->_name);
}

void AIStatsMission::Sort()
{
  // sort 
  QSort(_tableMP.Data(), _tableMP.Size(), CmpMPTableRows);
  for (int i=0; i<_tableMP.Size(); i++) _tableMP[i]->_order = i + 1;

  // ensure local player statistics are visible
  int index = -1;
  for (int i=0; i<_tableMP.Size(); i++)
  {
    if (_tableMP[i]->_dpnid == GetNetworkManager().GetPlayer())
    {
      index = i; break;
    }
  }
  if (index >= maxMPTableRows) swap(_tableMP[index], _tableMP[maxMPTableRows - 1]);
}

#define LOG_MP_STATS 0

void AIStatsMission::AddMPKill(RString playerName, TargetSide playerSide, int dpnid, int playerRole, const VehicleType *killedType, bool killedPlayer, bool isEnemy)
{
  if (dpnid == AI_PLAYER)
  {
    // AI statistics is calculated on server (bot client)
    if (!GetNetworkManager().IsServer()) return;
  }
  if (killedType && killedType->IsAnimal()) return; // animals don't affect statistics at all
  int index = FindRowNoAdd(playerName, playerSide, dpnid, playerRole);
  if (dpnid!=AI_PLAYER && index>=0)
  { // change only when player's statistic is local here (which is always on server)
    AIStatsMPRow *row = _tableMP[index];
    if (!row->IsLocal()) return;
  }
  if (index<0) 
  { // row not found yet - try to add it (only for aiKills)
    index = TryAddRow(playerName, playerSide, dpnid, playerRole);
  }

  if (index < 0) return;
  AIStatsMPRow *row = _tableMP[index];

  if (killedType)
  {
    // add killed vehicle
    if (killedType->IsKindOf(GWorld->Preloaded(VTypeStatic))) return;
    if (killedType->IsKindOf(GWorld->Preloaded(VTypeMan)))
    {
      if (isEnemy)
      {
#if LOG_MP_STATS
        LogF("MP Stats %s infantry: %d -> %d", cc_cast(playerName), row->_killsInfantry, row->_killsInfantry + 1);
#endif
        row->_killsInfantry++;
      }
      else
      {
#if LOG_MP_STATS
        LogF("MP Stats %s infantry: %d -> %d", cc_cast(playerName), row->_killsInfantry, row->_killsInfantry - 1);
#endif
        row->_killsInfantry--;
      }
    }
    else switch (killedType->GetKind())
    {
    case VAir:
      if (isEnemy)
      {
#if LOG_MP_STATS
        LogF("MP Stats %s air: %d -> %d", cc_cast(playerName), row->_killsAir, row->_killsAir + 1);
#endif
        row->_killsAir++;
      }
      else
      {
#if LOG_MP_STATS
        LogF("MP Stats %s air: %d -> %d", cc_cast(playerName), row->_killsAir, row->_killsAir - 1);
#endif
        row->_killsAir--;
      }
      break;
    case VArmor:
      if (isEnemy)
      {
#if LOG_MP_STATS
        LogF("MP Stats %s armor: %d -> %d", cc_cast(playerName), row->_killsArmor, row->_killsArmor + 1);
#endif
        row->_killsArmor++;
      }
      else
      {
#if LOG_MP_STATS
        LogF("MP Stats %s armor: %d -> %d", cc_cast(playerName), row->_killsArmor, row->_killsArmor - 1);
#endif
        row->_killsArmor--;
      }
      break;
    case VSoft:
    default:
      if (isEnemy)
      {
#if LOG_MP_STATS
        LogF("MP Stats %s soft: %d -> %d", cc_cast(playerName), row->_killsSoft, row->_killsSoft + 1);
#endif
        row->_killsSoft++;
      }
      else
      {
#if LOG_MP_STATS
        LogF("MP Stats %s soft: %d -> %d", cc_cast(playerName), row->_killsSoft, row->_killsSoft - 1);
#endif
        row->_killsSoft--;
      }
      break;
    }

    if (killedPlayer)
    {
      if (isEnemy)
      {
#if LOG_MP_STATS
        LogF("MP Stats %s players: %d -> %d", cc_cast(playerName), row->_killsPlayers, row->_killsPlayers + 1);
#endif
        row->_killsPlayers++;
      }
      else
      {
#if LOG_MP_STATS
        LogF("MP Stats %s players: %d -> %d", cc_cast(playerName), row->_killsPlayers, row->_killsPlayers - 1);
#endif
        row->_killsPlayers--;
      }
    }

    row->RecalculateTotal();
  }
  else
  {
#if LOG_MP_STATS
    LogF("MP Stats %s killed: %d -> %d", cc_cast(playerName), row->_killed, row->_killed + 1);
#endif
    // player killed
    row->_killed++;
  }

  Sort();
}

void AIStatsMission::AddMPScore(RString playerName, TargetSide playerSide, int dpnid, int playerRole, int score)
{
  if (dpnid == AI_PLAYER)
  {
    // AI statistics is calculated on server (bot client)
    if (!GetNetworkManager().IsServer()) return;
  }
  int index = FindRowNoAdd(playerName, playerSide, dpnid, playerRole);
  if (dpnid!=AI_PLAYER && index>=0)
  { // change only when player's statistic is local here (which is always on server)
    AIStatsMPRow *row = _tableMP[index];
    if (!row->IsLocal()) return;
  }
  if (index<0) 
  { // row not found yet - try to add it (only for aiKills)
    index = TryAddRow(playerName, playerSide, dpnid, playerRole);
  }

  if (index < 0) return;
  AIStatsMPRow *row = _tableMP[index];

  // add score
#if LOG_MP_STATS
  LogF("MP Stats %s custom: %d -> %d", cc_cast(playerName), row->_customScore, row->_killsPlayers + score);
#endif
  row->_customScore += score;
  row->RecalculateTotal();

  Sort();
}

void AIStatsMission::AddLiveStats(RString playerName, TargetSide playerSide, int dpnid, int playerRole, int score)
{
  if (dpnid == AI_PLAYER)
  {
    // AI statistics is calculated on server (bot client)
    if (!GetNetworkManager().IsServer()) return;
  }
  int index = FindRowNoAdd(playerName, playerSide, dpnid, playerRole);
  if (dpnid!=AI_PLAYER && index>=0)
  { // change only when player's statistic is local here (which is always on server)
    AIStatsMPRow *row = _tableMP[index];
    if (!row->IsLocal()) return;
  }
  if (index<0) 
  { // row not found yet - try to add it (only for aiKills)
    index = TryAddRow(playerName, playerSide, dpnid, playerRole);
  }

  if (index < 0) return;
  AIStatsMPRow *row = _tableMP[index];

  // add score
  row->_liveStats += score;

  Sort();
}

/*!
\patch 1.22 Date 08/27/2001 by Jirka
- Improved: MP Statistics table

\patch 5160 Date 5/21/2007 by Ondra
- New: MP Statistics table now indicate who is speaking over VoN by flashing given players name.
*/

void AIStatsMission::DrawMPTable(float alpha)
{
  bool isEast = false, isWest = false, isRes = false, isCiv = false;
  AIStatsMPRow rowEast, rowWest, rowRes, rowCiv;
  rowEast._side = TEast;
  rowWest._side = TWest;
  rowRes._side = TGuerrila;
  rowCiv._side = TCivilian;
  for (int i=0; i<_tableMP.Size(); i++)
  {
    AIStatsMPRow *row = _tableMP[i];
    switch (row->_side)
    {
    case TEast:
      isEast = true;
      rowEast._name = LocalizeString(IDS_EAST);
      rowEast._killsInfantry += row->_killsInfantry;
      rowEast._killsSoft += row->_killsSoft;
      rowEast._killsArmor += row->_killsArmor;
      rowEast._killsAir += row->_killsAir;
      rowEast._killsPlayers += row->_killsPlayers;
      rowEast._killsTotal += row->_killsTotal;
      rowEast._killed += row->_killed;
      break;
    case TWest:
      isWest = true;
      rowWest._name = LocalizeString(IDS_WEST);
      rowWest._killsInfantry += row->_killsInfantry;
      rowWest._killsSoft += row->_killsSoft;
      rowWest._killsArmor += row->_killsArmor;
      rowWest._killsAir += row->_killsAir;
      rowWest._killsPlayers += row->_killsPlayers;
      rowWest._killsTotal += row->_killsTotal;
      rowWest._killed += row->_killed;
      break;
    case TGuerrila:
      isRes = true;
      rowRes._name = LocalizeString(IDS_GUERRILA);
      rowRes._killsInfantry += row->_killsInfantry;
      rowRes._killsSoft += row->_killsSoft;
      rowRes._killsArmor += row->_killsArmor;
      rowRes._killsAir += row->_killsAir;
      rowRes._killsPlayers += row->_killsPlayers;
      rowRes._killsTotal += row->_killsTotal;
      rowRes._killed += row->_killed;
      break;
    case TCivilian:
      isCiv = true;
      rowCiv._name = LocalizeString(IDS_CIVILIAN);
      rowCiv._killsInfantry += row->_killsInfantry;
      rowCiv._killsSoft += row->_killsSoft;
      rowCiv._killsArmor += row->_killsArmor;
      rowCiv._killsAir += row->_killsAir;
      rowCiv._killsPlayers += row->_killsPlayers;
      rowCiv._killsTotal += row->_killsTotal;
      rowCiv._killed += row->_killed;
      break;
    }
  }
  int nSides = 0;
  if (isEast) nSides++;
  if (isWest) nSides++;
  if (isRes) nSides++;
  if (isCiv) nSides++;

  if (!_font)
  {
    // load config
    ParamEntryVal cls = Pars >> "CfgInGameUI" >> "MPTable";
    _font = GEngine->LoadFont(GetFontID(cls >> "font"));
    _size = cls >> "size";

    _totalWidth = 0;
    ParamEntryVal list = cls >> "Columns";
    DoAssert(list.GetEntryCount() == columns);
    for (int i=0; i<columns; i++)
    {
      ParamEntryVal entry = list.GetEntry(i);
      _colWidths[i] = entry >> "width";
      _doubleLines[i] = entry >> "doubleLine";
      _totalWidth += _colWidths[i];
      RString FindPicture(RString name);
      ConstParamEntryPtr picture = entry.FindEntry("picture");
      if (picture) _pictures[i] = GlobLoadTexture(FindPicture(*picture));
      PackedColor color = GetPackedColor(entry >> "colorBg");
      _bgColors[i] = PackedColorRGB(color, toIntFloor(color.A8() * alpha));
    }
  }

  const float xb = 0.5 * (1 - _totalWidth);
  const float xe = xb + _totalWidth;
  float yb = 0.10;
  const float ye = 0.90;
  const float rowHeight = (ye - yb) / (maxMPTableRows + 5.5 + 1); //+1 to get the space for caption with hostname
  yb += rowHeight;

  PackedColor color(PackedColorRGB(_color, toIntFloor(_color.A8() * alpha)));
  PackedColor colorSelected(PackedColorRGB(_colorSelected, toIntFloor(_colorSelected.A8() * alpha)));
  PackedColor colorTitleBg(PackedColorRGB(_colorTitleBg, toIntFloor(_colorTitleBg.A8() * alpha)));
  PackedColor colorWest(PackedColorRGB(_colorWest, toIntFloor(_colorWest.A8() * alpha)));
  PackedColor colorEast(PackedColorRGB(_colorEast, toIntFloor(_colorEast.A8() * alpha)));
  PackedColor colorRes(PackedColorRGB(_colorRes, toIntFloor(_colorRes.A8() * alpha)));
  PackedColor colorCiv(PackedColorRGB(_colorCiv, toIntFloor(_colorCiv.A8() * alpha)));

  const float w = GEngine->Width2D();
  const float h = GEngine->Height2D();

  const float cxb = CX(xb);
  const float cxe = CX(xe);
  const float cyb = CY(yb);

  float fontHeight = _size;

  int nRows = _tableMP.Size();
  saturateMin(nRows, maxMPTableRows);

  // background
  MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);

  float left = xb;
  for (int i=0; i<columns; i++)
  {
    GEngine->Draw2D(mip, _bgColors[i], Rect2DPixel(CX(left), cyb, CW(_colWidths[i]), CH((nRows + 1) * rowHeight)));
    left += _colWidths[i];
  }

  // caption with hostname
  GEngine->Draw2D(mip, colorTitleBg, Rect2DPixel(cxb, cyb-CH(rowHeight), cxe-cxb, CH(rowHeight)));
  RString hostname = Format("%s: %s", cc_cast(LocalizeString(IDS_MP_SERVER)), cc_cast(GetNetworkManager().GetHostName()) );
  float x = xb + 0.5 * (_totalWidth - GEngine->GetTextWidthF(_size, _font, hostname));
  float top = yb + 0.5 * (rowHeight - fontHeight);
  GEngine->DrawTextF(Point2DFloat(x, top-rowHeight), _size, _font, color, _shadow, hostname);

  // row background
  GEngine->Draw2D(mip, colorTitleBg, Rect2DPixel(cxb, cyb, cxe-cxb, CH(rowHeight)));

  // titles
  left = xb;
  for (int i=0; i<columns; i++)
  {
    float width = _colWidths[i];
    Texture *picture = _pictures[i];
    if (picture)
    {
      MipInfo mip = GEngine->TextBank()->UseMipmap(picture, 0, 0);
      float tw = rowHeight * picture->AWidth() / picture->AHeight() * (3.0f / 4.0f);
      float tx = left + 0.5 * (width - tw);
      Rect2DPixel rect(CX(tx), cyb, CW(tw), CH(rowHeight));
      GEngine->Draw2D(mip, /*colorSelected*/ PackedWhite, rect);
    }
    left += width;
  }

  const float border = TEXT_BORDER;

  // values
  TargetSide playerSide = TSideUnknown;
  for (int i=0; i<nRows; i++)
  {
    AIStatsMPRow *row = _tableMP[i];
    bool isSpeaking = false;
    if (row->_dpnid == GetNetworkManager().GetPlayer())
    {
      float top = yb + (i + 1) * rowHeight;
      GEngine->Draw2D(mip, colorSelected, Rect2DPixel(cxb, CY(top), cxe-cxb, CY(top + rowHeight) - CY(top)));
      playerSide = row->_side;
      isSpeaking = GetNetworkManager().IsVoiceRecording();
    }
    else
    {
      isSpeaking = GetNetworkManager().IsVoicePlaying(row->_dpnid);
    }
    PackedColor colorAct;
    switch (row->_side)
    {
    case TWest:
      colorAct = colorWest;
      break;
    case TEast:
      colorAct = colorEast;
      break;
    case TGuerrila:
      colorAct = colorRes;
      break;
    case TCivilian:
      colorAct = colorCiv;
      break;
    default:
      colorAct = color;
      break;
    }
    PackedColor colorActName = colorAct;
#ifndef _XBOX
    if (isSpeaking)
    {
      // TODO: proper implementation (config values, icons...)
      // now: when speaking, flash color between the current one and transparent
      // flash continuously 1..0..1
      float period = 0.5f;
      float flash = fabs(Glob.uiTime.Mod(period)-period*0.5)*(2.0f/period);
      int a8 = toInt(flash*colorActName.A8());
      saturate(a8,0,255);
      colorActName.SetA8(a8);
    }
#endif

    top += rowHeight;

    left = xb;
    float x = left + 0.5 * (_colWidths[0] - GEngine->GetTextWidthF(_size, _font, "%d", row->_order));
    GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_order);

    left += _colWidths[0];
    float sizeName = 0.8 * _size;
    float topName = top + 0.5 * (_size - sizeName);
    // x = left + 0.5 * (_colWidths[1] - GEngine->GetTextWidth(sizeName, _font, row->_name));
    x = left + border; // avoid problems when clipping
    GEngine->DrawText(
      Point2DFloat(x, topName), sizeName,
      Rect2DFloat(left, topName, _colWidths[1], top + rowHeight),
      _font, colorActName, row->_name, 0
    );

    left += _colWidths[1];
    if (row->_killsInfantry != 0)
    {
      x = left + 0.5 * (_colWidths[2] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsInfantry));
      GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killsInfantry);
    }
    left += _colWidths[2];
    if (row->_killsSoft != 0)
    {
      x = left + 0.5 * (_colWidths[3] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsSoft));
      GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killsSoft);
    }
    left += _colWidths[3];
    if (row->_killsArmor != 0)
    {
      x = left + 0.5 * (_colWidths[4] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsArmor));
      GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killsArmor);
    }
    left += _colWidths[4];
    if (row->_killsAir != 0)
    {
      x = left + 0.5 * (_colWidths[5] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsAir));
      GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killsAir);
    }
    left += _colWidths[5];
/*
    if (row->_killsPlayers != 0)
    {
      x = left + 0.5 * (colWidths[6] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsPlayers));
      GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, "%d", row->_killsPlayers);
    }
    left += colWidths[6];
*/
    if (row->_killed != 0)
    {
      x = left + 0.5 * (_colWidths[6] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killed));
      GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killed);
    }
    left += _colWidths[6];
    if (row->_killsTotal != 0)
    {
      x = left + 0.5 * (_colWidths[7] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsTotal));
      GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killsTotal);
    }
  }

  // lines
  float bottom = yb;
  float cbottom = CY(bottom);
  GEngine->DrawLine(Line2DPixel(cxb, cbottom, cxe, cbottom), color, color);
  for (int i=0; i<nRows+1; i++)
  {
    bottom += rowHeight;
    cbottom = CY(bottom);
    GEngine->DrawLine(Line2DPixel(cxb, cbottom, cxe, cbottom), color, color);
  }
  GEngine->DrawLine(Line2DPixel(cxb, cyb, cxb, cbottom), color, color);
  left = xb;
  for (int i=0; i<columns; i++)
  {
    left += _colWidths[i];
    float cleft = CX(left);
    if (_doubleLines[i])
    {
      GEngine->DrawLine(Line2DPixel(cleft - 1, cyb, cleft - 1, cbottom), color, color);
      GEngine->DrawLine(Line2DPixel(cleft + 1, cyb, cleft + 1, cbottom), color, color);
    }
    else
      GEngine->DrawLine(Line2DPixel(cleft, cyb, cleft, cbottom), color, color);
  }

  // summary for sides
  if (nSides > 1)
  {
    const float yb = ye - 4 * rowHeight;
    const float cyb = CY(yb);

    // background
    float left = xb;
    for (int i=0; i<columns; i++)
    {
      GEngine->Draw2D(mip, _bgColors[i], Rect2DPixel(CX(left), cyb, CW(_colWidths[i]), CH(nSides * rowHeight)));
      left += _colWidths[i];
    }

    // values
    float top = yb + 0.5 * (rowHeight - fontHeight);
    int r = 0;
    for (int i=0; i<nSides; i++)
    {
      PackedColor colorAct;
      AIStatsMPRow *row = NULL;
      switch (i)
      {
      case 0:
        if (!isWest) continue;
        row = &rowWest;
        colorAct = colorWest;
        break;
      case 1:
        if (!isEast) continue;
        row = &rowEast;
        colorAct = colorEast;
        break;
      case 2:
        if (!isRes) continue;
        row = &rowRes;
        colorAct = colorRes;
        break;
      case 3:
        if (!isCiv) continue;
        row = &rowCiv;
        colorAct = colorCiv;
        break;
      }

      if (row->_side == playerSide)
      {
        float top = yb + r * rowHeight;
        GEngine->Draw2D(mip, colorSelected, Rect2DPixel(cxb, CY(top), cxe-cxb, CY(top + rowHeight) - CY(top)));
      }

      left = xb + _colWidths[0];
      float sizeName = 0.8 * _size;
      // float x = left + 0.5 * (_colWidths[1] - GEngine->GetTextWidth(sizeName, _font, row->_name));
      float x = left + border; // avoid problems when clipping
      GEngine->DrawText
      (
        Point2DFloat(x, top), sizeName,
        Rect2DFloat(left, top, _colWidths[1], top + rowHeight),
        _font, colorAct, row->_name, 0
      );

      left += _colWidths[1];
      if (row->_killsInfantry != 0)
      {
        x = left + 0.5 * (_colWidths[2] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsInfantry));
        GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killsInfantry);
      }
      left += _colWidths[2];
      if (row->_killsSoft != 0)
      {
        x = left + 0.5 * (_colWidths[3] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsSoft));
        GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killsSoft);
      }
      left += _colWidths[3];
      if (row->_killsArmor != 0)
      {
        x = left + 0.5 * (_colWidths[4] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsArmor));
        GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killsArmor);
      }
      left += _colWidths[4];
      if (row->_killsAir != 0)
      {
        x = left + 0.5 * (_colWidths[5] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsAir));
        GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killsAir);
      }
      left += _colWidths[5];
/*
      if (row->_killsPlayers != 0)
      {
        x = left + 0.5 * (_colWidths[6] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsPlayers));
        GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, "%d", row->_killsPlayers);
      }
      left += _colWidths[6];
*/
      if (row->_killed != 0)
      {
        x = left + 0.5 * (_colWidths[6] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killed));
        GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killed);
      }
      left += _colWidths[6];
      if (row->_killsTotal != 0)
      {
        x = left + 0.5 * (_colWidths[7] - GEngine->GetTextWidthF(_size, _font, "%d", row->_killsTotal));
        GEngine->DrawTextF(Point2DFloat(x, top), _size, _font, colorAct, _shadow, "%d", row->_killsTotal);
      }

      top += rowHeight;
      r++;
    }

    // lines
    float bottom = yb;
    float cbottom = CY(bottom);
    GEngine->DrawLine(Line2DPixel(cxb, cbottom, cxe, cbottom), color, color);
    for (int i=0; i<nSides; i++)
    {
      bottom += rowHeight;
      cbottom = CY(bottom);
      GEngine->DrawLine(Line2DPixel(cxb, cbottom, cxe, cbottom), color, color);
    }
    left = xb;
    GEngine->DrawLine(Line2DPixel(cxb, cyb, cxb, cbottom), color, color);
    for (int i=0; i<columns; i++)
    {
      left += _colWidths[i];
      float cleft = CX(left);
      if (_doubleLines[i])
      {
        GEngine->DrawLine(Line2DPixel(cleft - 1, cyb, cleft - 1, cbottom), color, color);
        GEngine->DrawLine(Line2DPixel(cleft + 1, cyb, cleft + 1, cbottom), color, color);
      }
      else
        GEngine->DrawLine(Line2DPixel(cleft, cyb, cleft, cbottom), color, color);
    }
  }
}

float AIStatsMission::ScoreSide(TargetSide side)
{
  int killsTotal = 0.0f;

  for (int i=0; i<_tableMP.Size(); i++)
  {
    AIStatsMPRow *row = _tableMP[i];
    if(row->_side == side) killsTotal += row->_killsTotal;
  }
  return killsTotal;
}

LSError AIStatsEvent::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeEnum("type", type, 1))
  CHECK(ar.Serialize("time", time, 1))
  CHECK(::Serialize(ar, "position", position, 1))
  CHECK(ar.Serialize("message", message, 1, ""))

  CHECK(ar.Serialize("killedType", killedType, 1, "AllVehicles"))
  CHECK(ar.Serialize("killedName", killedName, 1, ""))
  CHECK(ar.Serialize("killedPlayer", killedPlayer, 1, false))
  CHECK(ar.Serialize("killerType", killerType, 1, "AllVehicles"))
  CHECK(ar.Serialize("killerName", killerName, 1, ""))
  CHECK(ar.Serialize("killerPlayer", killerPlayer, 1, false))
  return LSOK;
}

LSError AIStatsMission::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("Events", _events, 1))
  CHECK(ar.Serialize("lives", _lives, 1, -1))
  CHECK(::Serialize(ar, "start", _start, 1))
  // GetModeBackup must be used, as World._mode is reset on some places during serialization
  if (GWorld->GetModeBackup() == GModeNetware)
  {
    CHECK(ar.Serialize("tableMP", _tableMP, 1))
  }
  return LSOK;
}

LSError AIUnitHeader::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("index", index, 1))
  CHECK(ar.Serialize("experience", experience, 1, 0))
  CHECK(ar.SerializeEnum("rank", rank, 1, RankUndefined))
  CHECK(ar.SerializeRef("Unit", unit, 1))
  CHECK(ar.Serialize("used", used, 1, false))
  return LSOK;
}

AIStatsCampaign::AIStatsCampaign()
//  : _westUnits(TWest), _eastUnits(TEast)
{
  Clear();
}

void AIStatsCampaign::Clear()
{
  _playerInfo.index = -1;
  _playerInfo.experience = 0;
  _playerInfo.rank = RankPrivate;
  _playerInfo.unit = NULL;
/*
  _westUnits.Clear();
  _eastUnits.Clear();
*/
  _variables.Clear();
  _casualties = 0;
  _inCombat = 0;
  for (int i=0; i<SKN; i++) _kills[i] = 0;

  _score = 0;
  _lastScore = 0;
  _liveScore = 0;

  _awards.Clear();
  _penalties.Clear();

  _date = 0;
}

void AIStatsCampaign::AddVariable(GameVariable &var)
{
  for (int i=0; i<_variables.Size(); i++)
  {
    if (_variables[i]._name == var._name)
    {
      _variables[i] = var;
      return;
    }
  }
  _variables.Add(var);
}

void AIStatsCampaign::Update(AIStatsMission &mission)
{
  _lastScore = _score;

  if (_playerInfo.unit)
  {
    float experience = _playerInfo.unit->GetPerson()->GetExperience();
    float score = experience - _playerInfo.experience;
    _playerInfo.experience = experience;
    _playerInfo.rank = _playerInfo.unit->GetPerson()->GetRank();

    // score
    if
    (
      ExtParsMission.FindEntry("minScore") &&
      ExtParsMission.FindEntry("avgScore") &&
      ExtParsMission.FindEntry("maxScore")
    )
    {
      float minScore = ExtParsMission >> "minScore";
      float avgScore = ExtParsMission >> "avgScore";
      float maxScore = ExtParsMission >> "maxScore";

      int points;
      if (score >= avgScore)
      {
        points = toInt(4 * (score - avgScore) / (maxScore - avgScore));
        saturateMin(points, 4);
      }
      else
      {
        points = toInt(3 * (score - avgScore) / (avgScore - minScore));
        saturateMax(points, -3);
      }
      _score += points;

      // Xbox Live statistics for campaign
      if (IsCampaign() && GWorld->GetEndMode() != EMLoser)
      {
        ConstParamEntryPtr stats = ExtParsCampaign.FindEntry("stats");
        if (stats)
        {
          RString entryName = *stats;
          ConstParamEntryPtr entry = (Pars >> "CfgLiveStats").FindEntry(entryName);
          if (entry)
          {
            int difficulty = Glob.config.difficulty;
            RString diffName = Glob.config.diffNames.GetName(difficulty);
            float coefDiff = (*entry) >> (RString("coef") + diffName);
            _liveScore += coefDiff * (points + 3);
            
            entry = ExtParsMission.FindEntry("statsBonus");
            if (entry)
            {
              float bonus = *entry;
              _liveScore += bonus;
            }
          }
          else
          {
            RptF("Statistics %s is not valid in campaign %s", (const char *)entryName, (const char *)CurrentCampaign);
          }
        }
        else
        {
          RptF("Missing 'stats' in campaign %s", (const char *)CurrentCampaign);
        }
      }
    }
  }

  // FIX - _playerInfo.unit is not set in MP
  else if (GWorld->GetRealPlayer())
  {
    _playerInfo.experience = GWorld->GetRealPlayer()->GetExperience();
    _playerInfo.rank = GWorld->GetRealPlayer()->GetRank();
  }

  _inCombat += Glob.time - mission._start;
  
  time(&_date);
  
  for (int i=0; i<mission._events.Size(); i++)
  {
    switch (mission._events[i].type)
    {
    case SETUnitLost:
      _casualties++;
      break;
    case SETKillsEnemyInfantry:
      _kills[SKEnemyInfantry]++;
      break;
    case SETKillsEnemySoft:
      _kills[SKEnemySoft]++;
      break;
    case SETKillsEnemyArmor:
      _kills[SKEnemyArmor]++;
      break;
    case SETKillsEnemyAir:
      _kills[SKEnemyAir]++;
      break;
    case SETKillsFriendlyInfantry:
      _kills[SKFriendlyInfantry]++;
      break;
    case SETKillsFriendlySoft:
      _kills[SKFriendlySoft]++;
      break;
    case SETKillsFriendlyArmor:
      _kills[SKFriendlyArmor]++;
      break;
    case SETKillsFriendlyAir:
      _kills[SKFriendlyAir]++;
      break;
    case SETKillsCivilInfantry:
      _kills[SKCivilianInfantry]++;
      break;
    case SETKillsCivilSoft:
      _kills[SKCivilianSoft]++;
      break;
    case SETKillsCivilArmor:
      _kills[SKCivilianArmor]++;
      break;
    case SETKillsCivilAir:
      _kills[SKCivilianAir]++;
      break;
    case SETUser:
      break;
    }
  }
}

struct KillsItem
{
  AIStatsKills index;
  int value;

  LSError Serialize(ParamArchive &ar);
};
TypeIsMovableZeroed(KillsItem)

LSError KillsItem::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeEnum("index", index, 1))
  CHECK(ar.Serialize("value", value, 1, 0))
  return LSOK;
}

LSError AIStatsCampaign::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("PlayerInfo", _playerInfo, 1))
//  CHECK(ar.Serialize("WestUnits", _westUnits, 1))
//  CHECK(ar.Serialize("EastUnits", _eastUnits, 1))
  ar.SetParams(GWorld->GetGameState());
  CHECK(ar.Serialize("Variables", _variables, 1))

  CHECK(ar.Serialize("casualities", _casualties, 1, 0))
  CHECK(ar.Serialize("inCombat", _inCombat, 1, 0))

  CHECK(ar.Serialize("score", _score, 1, 0))
  CHECK(ar.Serialize("lastScore", _lastScore, 1, 0))
  CHECK(ar.Serialize("liveScore", _liveScore, 1, 0))

  CHECK(ar.SerializeArray("awards", _awards, 1))
  CHECK(ar.SerializeArray("penalties", _penalties, 1))

  AutoArray<KillsItem> kills;
  if (ar.IsSaving())
  {
    kills.Resize(SKN);
    for (int i=0; i<SKN; i++)
    {
      kills[i].index = (AIStatsKills)i;
      kills[i].value = _kills[i];
    }
    CHECK(ar.Serialize("Kills", kills, 1))
  }
  else if (ar.GetPass() == ParamArchive::PassFirst)
  {
    CHECK(ar.Serialize("Kills", kills, 1))
    for (int i=0; i<kills.Size(); i++)
    {
      int index = kills[i].index;
      if (index >=0 && index < SKN) _kills[index] = kills[i].value;
    }
  }

  CHECK(ar.Serialize("date", (int &)_date, 1, 0))

  return LSOK;
}

LSError AIStatsCampaign::CampaignSerialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("PlayerInfo", _playerInfo, 1))
//  CHECK(ar.Serialize("WestUnits", _westUnits, 1))
//  CHECK(ar.Serialize("EastUnits", _eastUnits, 1))
  ar.SetParams(GWorld->GetGameState());
  CHECK(ar.Serialize("Variables", _variables, 1))

  CHECK(ar.Serialize("casualities", _casualties, 1))
  CHECK(ar.Serialize("inCombat", _inCombat, 1))

  CHECK(ar.Serialize("score", _score, 1, 0))
  CHECK(ar.Serialize("lastScore", _lastScore, 1, 0))
  CHECK(ar.Serialize("liveScore", _liveScore, 1, 0))

  CHECK(ar.SerializeArray("awards", _awards, 1))
  CHECK(ar.SerializeArray("penalties", _penalties, 1))

  if (ar.GetArVersion() >= 3)
  {
    AutoArray<KillsItem> kills;
    if (ar.IsSaving())
    {
      kills.Resize(SKN);
      for (int i=0; i<SKN; i++)
      {
        kills[i].index = (AIStatsKills)i;
        kills[i].value = _kills[i];
      }
      CHECK(ar.Serialize("Kills", kills, 3))
    }
    else if (ar.GetPass() == ParamArchive::PassFirst)
    {
      CHECK(ar.Serialize("Kills", kills, 3))
      for (int i=0; i<kills.Size(); i++)
      {
        int index = kills[i].index;
        if (index >=0 && index < SKN) _kills[index] = kills[i].value;
      }
    }
  }

  CHECK(ar.Serialize("date", (int &)_date, 1, 0))

  return LSOK;
}

void PositionToAA11(Vector3Val pos, char *buffer);

AIStats::AIStats()
{
}
AIStats::~AIStats()
{
}

void AIStats::OnMPMissionEnd()
{
#if _ENABLE_VBS
  bool IsVBS();
  if
  (
    IsVBS() && GetNetworkManager().IsServer()
  )
  {
    _mission.WriteVBSStatistics();
  }
#endif
}

void AIStats::OnMPMissionStart()
{
  _mission.OnMPMissionStart();
}

/*!
\patch 1.22 Date 8/30/2001 by Jirka
- Added: enable calculate all playable units into MP (I key) score
- add aiKills = true; into mission's description.ext
*/
void AIStats::OnVehicleDestroyed(EntityAI *killed, EntityAI *killer)
{
  if (!killed) return;

  Person *killedSoldier = dyn_cast<Person>(killed);
  Person *killerPerson = GetPlayerPerson(killer);

#if _ENABLE_VBS
  bool IsVBS();
  if (IsVBS() && killed && killer)
  {
    _mission.VBSKilled(killed, killer);
  }
#endif

  // MP statistics
  if (GWorld->GetMode() == GModeNetware)
  {
    // is it enemy?
    bool isEnemy;
    if (killer == killed) isEnemy = false; // myself
    else if (killed->IsRenegade()) isEnemy = true; // renegade
    else
    {
      if (killer)
      {
        // FIX: killer->Vehicle::GetTargetSide cannot be used as player shooting from the stolen enemy vehicle should get positive score
        AICenter *center = GWorld->GetCenter(killer->GetTargetSide());
        isEnemy = center && center->IsEnemy(killed->Vehicle::GetTargetSide());
      }
      else
      {
        isEnemy = true;
      }
    }

    const MissionHeader *header = GetNetworkManager().GetMissionHeader();
    if (header && header->_aiKills)
    {
      if (killer && killer != killed) // do not count suicide kills
      {
        if (killerPerson && killerPerson->Brain() && killerPerson->Brain()->IsPlayable())
        {
          RString playerName = killerPerson->GetPersonName();
          TargetSide playerSide = killerPerson->Brain()->GetSide();
#if LOG_MP_STATS
          LogF("MP Stats %s kill: %s %s", cc_cast(playerName), isEnemy ? "enemy" : "friendly", cc_cast(killed->GetType()->GetName()));
#endif
          _mission.AddMPKill
          (
            playerName, playerSide, killerPerson->GetRemotePlayer(), killerPerson->Brain()->GetRoleIndex(),
            killed->GetType(), killedSoldier && killedSoldier->IsNetworkPlayer(), isEnemy
          );
        }
      }
      if (killedSoldier && killedSoldier->Brain() && killedSoldier->Brain()->IsPlayable() && !killedSoldier->IsAnimal())
      {
        TargetSide playerSide = killedSoldier->Brain()->GetSide();
#if LOG_MP_STATS
        LogF("MP Stats %s killed", cc_cast(killedSoldier->GetPersonName()));
#endif
        _mission.AddMPKill
        (
          killedSoldier->GetPersonName(), playerSide, killedSoldier->GetRemotePlayer(), killedSoldier->Brain()->GetRoleIndex(),
          NULL, true, isEnemy
        );
      }
    }
    else
    {
      if (killer && killer != killed) // do not count suicide kills
      {
        if (killerPerson && killerPerson->IsNetworkPlayer())
        {
          RString playerName = killerPerson->GetPersonName();
          int player = killerPerson->GetRemotePlayer();
          int roleIndex = -1;
          if (killerPerson->Brain())
          {
            roleIndex = killerPerson->Brain()->GetRoleIndex();
            if (roleIndex < 0)
            {
              ErrF("Player %s (%d) has no role index set", (const char *)killerPerson->GetDebugName(), player);
            }
          }
          if (roleIndex < 0)
          {
            // find role in list of roles
            for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
            {
              if (GetNetworkManager().GetPlayerRole(i)->player == player)
              {
                roleIndex = i;
                break;
              }
            }
          }
          DoAssert(roleIndex >= 0);
          if (roleIndex >= 0)
          {
            const PlayerRole *role = GetNetworkManager().GetPlayerRole(roleIndex);
            if (role)
            {
              #if LOG_MP_STATS
            LogF("MP Stats %s kill: %s %s", cc_cast(playerName), isEnemy ? "enemy" : "friendly", cc_cast(killed->GetType()->GetName()));
              #endif
              _mission.AddMPKill(
              playerName, role->side, player, roleIndex,
              killed->GetType(), killedSoldier && killedSoldier->IsNetworkPlayer(), isEnemy
            );
          }
        }
      }
      }
      if (killedSoldier && killedSoldier->IsNetworkPlayer() && !killedSoldier->IsAnimal())
      {
        RString playerName = killedSoldier->GetPersonName();
        int player = killedSoldier->GetRemotePlayer();
        int roleIndex = -1;
        if (killedSoldier->Brain())
        {
          roleIndex = killedSoldier->Brain()->GetRoleIndex();
          if (roleIndex < 0)
          {
            ErrF("Player %s (%d) has no role index set", (const char *)killedSoldier->GetDebugName(), player);
          }
        }
        if (roleIndex < 0)
        {
          // find role in list of roles
          for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
          {
            if (GetNetworkManager().GetPlayerRole(i)->player == player)
            {
              roleIndex = i;
              break;
            }
          }
        }
        DoAssert(roleIndex >= 0);
        if (roleIndex >= 0)
        {
          const PlayerRole *role = GetNetworkManager().GetPlayerRole(roleIndex);
#if LOG_MP_STATS
          LogF("MP Stats %s killed", cc_cast(playerName));
#endif
          _mission.AddMPKill
          (
            playerName, role->side, player, roleIndex,
            NULL, true, isEnemy
          );
        }
      }
    }
  }

  Person *player = GWorld->GetRealPlayer();
  if (!player) return;
  AIBrain *me = player->Brain();
  AIGroup *myGroup = me ? me->GetGroup() : NULL;
  AICenter *myCenter = myGroup ? myGroup->GetCenter() : NULL;
  if (!myCenter)
  {
    switch (player->Vehicle::GetTargetSide())
    {
    case TEast:
      myCenter = GWorld->GetEastCenter(); break;
    case TWest:
      myCenter = GWorld->GetWestCenter(); break;
    case TGuerrila:
      myCenter = GWorld->GetGuerrilaCenter(); break;
    case TCivilian:
      myCenter = GWorld->GetCivilianCenter(); break;
    }
  }

  AIBrain *killedUnit = killedSoldier ? killedSoldier->Brain() : NULL;

  if
  (
    killedSoldier == player ||
    myGroup && killedUnit && killedUnit->GetGroup() == myGroup
  )
  {
    char time[32], position[32];
    float tod = 24.0 * Glob.clock.GetTimeOfDay();
    int hour = toIntFloor(tod);
    int minute = toIntFloor(60.0 * (tod - hour));
    if (minute >= 60)
    {
      hour++;
      minute -= 60;
    }
    sprintf(time, "% 2d:%02d", hour, minute);
    PositionToAA11(killed->FutureVisualState().Position(), position);

    RString message = LocalizeString(IDS_STAT_UNIT_LOST);
    char buffer[256];
    sprintf
    (
      buffer, message, time,
      (const char *)LocalizeString(IDS_PRIVATE + killedSoldier->GetRank()),
      (const char *)killedSoldier->GetPersonName(),
      position
    );
    if (killerPerson)
    {
      _mission.AddEvent
      (
        SETUnitLost, killed->FutureVisualState().Position(), buffer,
        killed->GetType(), killedSoldier->GetPersonName(), killedSoldier == player,
        killer->GetType(), killerPerson->GetPersonName(), killerPerson->IsNetworkPlayer()
      );
    }
    else
    {
      _mission.AddEvent
      (
        SETUnitLost, killed->FutureVisualState().Position(), buffer,
        killed->GetType(), killedSoldier->GetPersonName(), killed == player,
        GWorld->Preloaded(VTypeAllVehicles), "", false
      );
    }
//    return; // avoid message duplication
  }

  if (killerPerson == player && killed->GetType()->GetCost() > 2000 && killer != killed)
  {
    char time[32], position[32];
    float tod = 24.0 * Glob.clock.GetTimeOfDay();
    int hour = toIntFloor(tod);
    int minute = toIntFloor(60.0 * (tod - hour));
    if (minute >= 60)
    {
      hour++;
      minute -= 60;
    }
    sprintf(time, "% 2d:%02d", hour, minute);
    PositionToAA11(killed->FutureVisualState().Position(), position);

    // is it enemy?
    TargetSide side = killed->GetType()->GetTypicalSide();
    bool isEnemy = false;
    // check if himself
    if (killed == killer) isEnemy = false;
    // check if renegade
    else if (killed->IsRenegade())
      isEnemy = true;
    else if (myCenter)
    {
      // check target side 
      if (myCenter->IsEnemy(killed->Entity::GetTargetSide()))
        isEnemy = true;
      // check typical side
      else if (myCenter->IsEnemy(killed->GetType()->GetTypicalSide()))
        isEnemy = true;
    }

    if (killedSoldier)
    {
      RString message;
      AIStatsEventType type;
      if (isEnemy)
      {
        message = LocalizeString(IDS_STAT_KILL_ENEMY_SOLDIER);
        type = SETKillsEnemyInfantry;
      }
      else if (side == TCivilian)
      {
        message = LocalizeString(IDS_STAT_KILL_CIVIL);
        type = SETKillsCivilInfantry;
      }
      else
      {
        message = LocalizeString(IDS_STAT_KILL_FRIENDLY_SOLDIER);
        type = SETKillsFriendlyInfantry;
      }
      char buffer[256];
      sprintf(buffer, message, time, position);
      if (killedSoldier)
        _mission.AddEvent
        (
          type, killed->FutureVisualState().Position(), buffer,
          killed->GetType(), killedSoldier->GetPersonName(), killedSoldier->IsRemotePlayer(),
          killer->GetType(), killerPerson->GetPersonName(), true
        );
      else
        _mission.AddEvent
        (
          type, killed->FutureVisualState().Position(), buffer,
          killed->GetType(), "", false,
          killer->GetType(), killerPerson->GetPersonName(), true
        );
    }
    else
    {
      RString message;
      if (isEnemy)
        message = LocalizeString(IDS_STAT_KILL_ENEMY_UNIT);
      else if (side == TCivilian)
        message = LocalizeString(IDS_STAT_KILL_CIVILIAN_UNIT);
      else
        message = LocalizeString(IDS_STAT_KILL_FRIENDLY_UNIT);
      char buffer[256];
      sprintf
      (
        buffer, message, time,
        (const char *)killed->GetType()->GetDisplayName(),
        position
      );
      AIStatsEventType type;
      switch (killed->GetType()->GetKind())
      {
      default:
      case VSoft:
        if (isEnemy)
          type = SETKillsEnemySoft;
        else if (side == TCivilian)
          type = SETKillsCivilSoft;
        else
          type = SETKillsFriendlySoft;
        break;
      case VArmor:
        if (isEnemy)
          type = SETKillsEnemyArmor;
        else if (side == TCivilian)
          type = SETKillsCivilArmor;
        else
          type = SETKillsFriendlyArmor;
        break;
      case VAir:
        if (isEnemy)
          type = SETKillsEnemyAir;
        else if (side == TCivilian)
          type = SETKillsCivilAir;
        else
          type = SETKillsFriendlyAir;
        break;
      }
      _mission.AddEvent
      (
        type, killed->FutureVisualState().Position(), buffer,
        killed->GetType(), "", false,
        killer->GetType(), killerPerson->GetPersonName(), true
      );
    }
  }
}

#if _ENABLE_VBS
void AIStats::OnVehicleDamaged(EntityAI *injured, EntityAI *killer, float damage, RString ammo)
{
  if (injured) _mission.VBSInjured(injured, killer, damage, ammo);
}

void AIStats::VBSAddHeader(RString text)
{
  _mission.VBSAddHeader(text);
}

void AIStats::VBSAddEvent(RString text)
{
  _mission.VBSAddEvent(text);
}

void AIStats::VBSAddFooter(RString text)
{
  _mission.VBSAddFooter(text);
}

#endif

void AIStats::AddUserEvent(RString event, Vector3Par pos)
{
  _mission.AddEvent
  (
    SETUser, pos, event,
    GWorld->Preloaded(VTypeAllVehicles), "", false,
    GWorld->Preloaded(VTypeAllVehicles), "", false
  );
}

LSError AIStats::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("Mission", _mission, 1))
  CHECK(ar.Serialize("Campaign", _campaign, 1))
  return LSOK;
}

AIStats GStats;

///////////////////////////////////////////////////////////////////////////////
// Strategic database classes

DEFINE_FAST_ALLOCATOR(AITargetInfo)

AITargetInfo::AITargetInfo()
{
  _idExact = NULL;
  _side = TSideUnknown;
  _type = NULL;
  _x = 0;
  _z = 0;
  
  _accuracySide = 0;
  _timeSide = TIME_MIN;
  _accuracyType = 0;
  _timeType = TIME_MIN;

  _time = TIME_MIN;
  _precisionPos=0;
  
  _exposure = false;
  _vanished = false;
  _destroyed = false;
  _dir = VZero; // unknown 
}



LSError AITargetInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("IdExact", _idExact, 1))
  CHECK(ar.SerializeEnum("side", _side, 1))
  CHECK(::Serialize(ar, "type", _type, 1))

  CHECK(::Serialize(ar, "realPos", _realPos, 1))
  CHECK(::Serialize(ar, "pos", _pos, 1))
  CHECK(ar.Serialize("x", _x, 1))
  CHECK(ar.Serialize("z", _z, 1))
  CHECK(::Serialize(ar, "dir", _dir, 1, VZero))
  CHECK(ar.Serialize("accuracySide", _accuracySide, 1, 4.0))
  CHECK(::Serialize(ar, "timeSide", _timeSide, 1))
  CHECK(ar.Serialize("accuracyType", _accuracyType, 1, 4.0))
  CHECK(::Serialize(ar, "timeType", _timeType, 1))
  CHECK(::Serialize(ar, "time", _time, 1))
  CHECK(ar.Serialize("precisionPos", _precisionPos, 1, 0))

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    _exposure = false;  // AIMap is not serialized
  }
  return LSOK;
}

float AITargetInfo::FadingSideAccuracy() const
{
  if (_timeSide<Glob.time-AccuracyLast-2) return 0;
  const float invT=1.0/AccuracyLast;
  float fade = 1 - (Glob.time - _timeSide - 2) * invT;
  saturate(fade,0,1);
  return _accuracySide * fade;
}

float AITargetInfo::FadingTypeAccuracy() const
{
  if (_timeType<Glob.time-AccuracyLast-2) return 0;
  const float invT=1.0/AccuracyLast;
  float fade = 1 - (Glob.time - _timeType - 2) * invT;
  saturate(fade,0,1);
  return _accuracyType * fade;
}

float AITargetInfo::FadingPositionAccuracy() const
{
  if (_time<Glob.time-AccuracyLast-2) return 0;
  const float invT=1.0/AccuracyLast;
  float fade = 1 - (Glob.time - _time - 2) * invT;
  saturate(fade,0,1);
  return fade;
}

LSError AIGuardingGroup::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Group", group, 1))
  CHECK(ar.Serialize("guarding", guarding, 1, false))
  return LSOK;
}

LSError AIGurdedVehicle::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Vehicle", vehicle, 1))
  CHECK(ar.SerializeRef("By", by, 1))
  return LSOK;
}

LSError AIGurdedPoint::Serialize(ParamArchive &ar)
{
  CHECK(::Serialize(ar, "position", position, 1))
  CHECK(ar.SerializeRef("By", by, 1))
  return LSOK;
}

LSError AISupportTarget::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Group", group, 1))
  CHECK(::Serialize(ar, "pos", pos, 1))
  CHECK(ar.Serialize("medic", medic, 1))
  CHECK(ar.Serialize("requestedMedic", requestedMedic,1))
  CHECK(ar.Serialize("ambulance", ambulance, 1))
  CHECK(ar.Serialize("repair", repair, 1))
  CHECK(ar.Serialize("rearm", rearm, 1))
  CHECK(ar.Serialize("refuel", refuel, 1))
  return LSOK;
}

LSError AISupportGroup::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Group", group, 1))
  CHECK(ar.SerializeRef("Assigned", assigned, 1))
  CHECK(ar.Serialize("ambulance", ambulance, 1))
  CHECK(ar.Serialize("medic", medic, 1))
  CHECK(ar.Serialize("repair", repair, 1))
  CHECK(ar.Serialize("rearm", rearm, 1))
  CHECK(ar.Serialize("refuel", refuel, 1))
  return LSOK;
}

#include <Es/Containers/array2D.hpp>
#include <Es/Containers/quadtreeEx.hpp>

struct AIThreatTraits
{
  enum {LogSizeX = 0};
  enum {LogSizeY = 0};
  typedef AIThreat Type;
  static bool IsEqual(const AIThreat &a, const AIThreat &b)
  {
    return a.packed == b.packed;
  }
  static const AIThreat &Get(const AIThreat &value, int x, int y)
  {
    Assert(x == 0);
    Assert(y == 0);
    return value;
  }
  static void Set(AIThreat &value, int x, int y, const AIThreat &item)
  {
    Assert(x == 0);
    Assert(y == 0);
    value = item;
  }
};

typedef QuadTreeExRoot<AIThreat, AIThreatTraits>::QuadTreeType QuadTreeExAIThreat;
typedef QuadTreeExRoot<BYTE, QuadTreeExTraitsBYTE<BYTE> >::QuadTreeType QuadTreeExBYTE;

// DEFINE_FAST_ALLOCATOR(QuadTreeExAIThreat)
// DEFINE_FAST_ALLOCATOR(QuadTreeExBYTE)

/// info about exposure change on given field (to let subgroups know exposure has been changed)
struct ExposureChange
{
  int _x;
  int _z;
  float _changeEnemy;
};
TypeIsSimple(ExposureChange);

/// info about the target stored by AIMap - used to calculate the current exposure
struct AIMapTargetInfo
{
  /// unique target identifier
  ObjectId _id;
  /// locked reference to the type
  TaskOwnerRef<EntityAIType> _type;
  int _x;
  int _z;
  Vector3 _pos;
  Vector3 _dir;

  bool _erase;
/*
  bool enemy;
  Time time;
*/
};

#define USE_AIMAP_THREAD 1

#if USE_AIMAP_THREAD
#include <El/Multicore/jobs.hpp>
#endif

/// Influence map encapsulation
class AIMap
#if USE_AIMAP_THREAD
  : public Job
#endif
{
protected:
  int _row;
  int _column;

  /// internal (optimized) queue of targets changed
  AutoArray<SRef<AIMapTargetInfo>, MemAllocDSafe> _targetsQueueInternal;
  /// list of current targets
  AutoArray<SRef<AIMapTargetInfo>, MemAllocDSafe> _targets;

  AutoArray<BYTE> _dirtyRow;
  QuadTreeExRoot<BYTE, QuadTreeExTraitsBYTE<BYTE>, 2, 2, MemAllocDSafe> _dirty;

  // Quad tree cannot be used here, we want a safe MT read access from different thread
  // QuadTreeExRoot<AIThreat, AIThreatTraits> _exposureEnemy;
  Array2D<AIThreat> _exposureEnemy;

public:
  /// external queue of targets changed (filled continuously by the main thread, moved to the internal queue when planning the job)
  AutoArray<SRef<AIMapTargetInfo> > _targetsQueueExternal;
  /// list of changed fields (created by the job, consumed by the main thread)
  AutoArray<ExposureChange, MemAllocDSafe> _changes;
  /// queue of infos to destroy (created by the job, consumed by the main thread)
  AutoArray<SRef<AIMapTargetInfo>, MemAllocDSafe> _targetsToErase;

public:
  AIMap()
    : _dirty(256, 256, 0)
  {
    Init();
  }
  ~AIMap()
  {
    Done();
  }

#if USE_AIMAP_THREAD
  /// the main Job procedure
  virtual bool Process();
#endif

  void Init();
  /// process external queue
  void ProcessQueue();
  /// process some piece of work
  void Update();
  void ProcessAllChanges();

  void AddChange(bool erase, const AITargetInfo &info);
  AIThreat GetExposureEnemy(int x, int z) const
  // {return _exposureEnemy.Get(x, z);}
  {return _exposureEnemy.GetValue(x, z);}

protected:
  // clean up (finish the thread, empty internal data structures)
  void Done();
  void UpdateField(int x, int z);
  float RecalculateExposure(int x, int z, AIThreat &result);
  bool ProcessChange();
  void SetDirty(int x, int z);
};

void AIMap::Init()
{
  // called always from the main thread
  Done();

  // realloc all arrays (we need no care for locks now, thread is not running)
  _dirty.Dim(LandRange,LandRange);
  _dirty.Init(0);
  _dirtyRow.Realloc(LandRange);
  _dirtyRow.Resize(LandRange);

  _exposureEnemy.Dim(LandRange, LandRange);
  // _exposureEnemy.Init(AIThreat());

  for (int i=0; i<LandRange; i++)
  {
    for (int j=0; j<LandRange; j++)
    {
      _exposureEnemy(j,i).packed = 0;
      /*
      _exposureUnknown(j,i).packed = 0;
      _dirty(j,i) = 0;
      */
    }
    _dirtyRow[i] = 0;
  }

  _row = 0;
  _column = 0;
}

void AIMap::Done()
{
#if USE_AIMAP_THREAD
  // stop the job
  GJobManager.CancelJob(this);
#endif

  // empty queues

  // internal data structures
  _targetsQueueInternal.Resize(0);
  _targets.Resize(0);

  // queues
  _changes.Resize(0);
  _targetsQueueExternal.Resize(0);
  _targetsToErase.Resize(0);
}

#if USE_AIMAP_THREAD
bool AIMap::Process()
{
  // process piece of work
  Update();
  return !WantToBeCanceled(); // done or canceled (this job is never suspended)
}
#endif

void AIMap::ProcessAllChanges()
{
#if USE_AIMAP_THREAD
  if (GetJobState() != JSRunning && GetJobState() != JSWaiting) // changes are save only when job is not (will be not) running
#endif
  {
    while (ProcessChange());
  }
}

void AIMap::Update()
{
  // number of items from internal queue processed (dirty flags rendered) in the single call
  static const int targetsPerCycle = 8;
  // number of updates (dirty flags resolved) in the single call
  static const int updatesPerCycle = 100;

#if USE_AIMAP_THREAD
  bool mainThread = GetJobState() != JSRunning;
#else
  bool mainThread = true;
#endif

  // processing target changes is quite fast, only dirty flags are set
  // still dirty areas can be quite large, therefore we want to limit this
  {
    PROFILE_SCOPE_EX(aiMUp, *);
    PROFILE_SCOPE_EX(aiMPC, aiMap);

    TimeManagerScope time;
    if (mainThread) time.Start(TCAICenterMap);
    for (int i=0; i<targetsPerCycle; i++)
    {
      if (ProcessChange())
      {
        ADD_COUNTER(aiMaC, 1);
      }
#if USE_AIMAP_THREAD
      if (WantToBeCanceled()) return;
#endif
    }
  }

  int nRows = 16;
  int nAll = LandRange * LandRange;
  int nUpdates = 0;

  // finish row
  if (_column != 0)
  {
    bool dirty = false;
    for (int i=0; i<_column; i++)
      if (_dirty.Get(i, _row) != 0)
        dirty = true;

    while (_column != 0)
    {
      if (_dirty.Get(_column, _row) != 0)
      {
        {
          PROFILE_SCOPE_EX(aiMUp, *);

          TimeManagerScope time;
          if (mainThread) time.Start(TCAICenterMap);
          UpdateField(_column, _row);
        }
        nUpdates++;
        _column = (_column + 1) & (LandRangeMask);
        nAll--;
        if (nUpdates >= updatesPerCycle) goto EndOfUpdateMap; // avoid too long processing of results
        if (mainThread && GTimeManager.TimeLeft(TCAICenterMap) <= 0) goto EndOfUpdateMap;
#if USE_AIMAP_THREAD
        if (WantToBeCanceled()) goto EndOfUpdateMap;
#endif
      }
      else
      {
        _column = (_column + 1) & (LandRangeMask);
        nAll--;
      }
      if (nAll <= 0) goto EndOfUpdateMap;
    }
    if (!dirty)
      _dirtyRow[_row] = 0;
    _row = (_row + 1) & (LandRangeMask);
    nRows--;
  }

  while (true)
  {
    Assert(_column == 0);

    while (_dirtyRow[_row] == 0)
    {
      _row = (_row + 1) & (LandRangeMask);
      nAll -= LandRange;
      if (nAll <= 0) goto EndOfUpdateMap;
    }
    for (int i=0; i<LandRange; i++)
    {
      if (_dirty.Get(_column,_row) != 0)
      {
        {
          PROFILE_SCOPE_EX(aiMUp, *);

          TimeManagerScope time;
          if (mainThread) time.Start(TCAICenterMap);
          UpdateField(_column, _row);
        }
        nUpdates++;
        _column = (_column + 1) & (LandRangeMask);
        nAll--;
        if (nUpdates >= updatesPerCycle) goto EndOfUpdateMap; // avoid too long processing of results
        if (mainThread && GTimeManager.TimeLeft(TCAICenterMap) <= 0) goto EndOfUpdateMap;
#if USE_AIMAP_THREAD
        if (WantToBeCanceled()) goto EndOfUpdateMap;
#endif
      }
      else
      {
        _column = (_column + 1) & (LandRangeMask);
        nAll--;
      }
      if (nAll <= 0) goto EndOfUpdateMap;
    }
    _dirtyRow[_row] = 0;
    _row = (_row + 1) & (LandRangeMask);
    nRows--;
    if (nRows <= 0) goto EndOfUpdateMap;
  }

EndOfUpdateMap:;

  ADD_COUNTER(aiMaU, 1);
  ADD_COUNTER(aiMaD, nUpdates);
  ADD_COUNTER(aiMaA, ((LandRange * LandRange) - nAll) / 1000);
}

void AIMap::UpdateField(int x, int z)
{
  float changeEnemy = 0;
  BYTE dirty = _dirty.Get(x, z);
  if (dirty)
  {
    AIThreat exposure = _exposureEnemy.Get(x, z);
    changeEnemy = RecalculateExposure(x, z, exposure);
/*
    _exposureEnemy.Set(x, z, exposure);
*/
    _exposureEnemy.Set(x, z) = exposure;
  }

  _dirty.Set(x, z, 0);

  // handle change through output queue
  if (changeEnemy > 0)
  {
    ExposureChange &change = _changes.Append();
    change._x = x;
    change._z = z;
    change._changeEnemy = changeEnemy;
  }
}

void AIMap::SetDirty(int x, int z)
{
/*
  _dirty(x,z) |= dirty;
*/
  _dirty.Set(x, z, true);
  _dirtyRow[z] = true;
}

void AIMap::AddChange(bool erase, const AITargetInfo &info)
{
  if (info._idExact)
  {
    int index = _targetsQueueExternal.Add(new AIMapTargetInfo());
    AIMapTargetInfo *change = _targetsQueueExternal[index];
    change->_id = info._idExact->GetObjectId();
    change->_type = info._type.GetRef();
    change->_x = info._x;
    change->_z = info._z;
    change->_pos = info._pos;
    change->_dir = info._dir;
    change->_erase = erase;
  }
}

void AIMap::ProcessQueue()
{
  while (_targetsQueueExternal.Size() > 0)
  {
    // access to external queue
    SRef<AIMapTargetInfo> item = _targetsQueueExternal[0];
    _targetsQueueExternal.Delete(0);

    if (item->_erase)
    {
      // try to eliminate pairs insert - delete
      int index = -1;
      for (int i=_targetsQueueInternal.Size()-1; i>=0; i--)
      {
        const AIMapTargetInfo *change = _targetsQueueInternal[i];
        if (change->_id == item->_id)
        {
          if (!change->_erase && change->_type == item->_type)
          {
            // pair insert - delete found
            // delete after insert - unit destroyed sooner insert applied
            index = i;
          }
          break;
        }
      }
      if (index >= 0)
      {
        // delete both annihilated items
        SRef<AIMapTargetInfo> remove = _targetsQueueInternal[index];
        _targetsQueueInternal.Delete(index);

        _targetsToErase.Add(remove);
        _targetsToErase.Add(item);
        continue; // do not add to _targetsQueueInternal
      }
    }

    _targetsQueueInternal.Add(item);
  }
}

/*!
  \patch 5117 Date 1/15/2007 by Ondra
  - Fixed: AI did not properly considered enemy vehicles with turret mounted weapons
    in its threat maps, sometimes resulting to careless behaviour. 
*/
bool AIMap::ProcessChange()
{
  Assert(_dirty.GetXRange() == LandRange);
  Assert(_dirty.GetYRange() == LandRange);
  if (_targetsQueueInternal.Size() < 1) return false; // done
  
  SRef<AIMapTargetInfo> info = _targetsQueueInternal[0];
  _targetsQueueInternal.Delete(0);

#if 0
Log(
  "Update exposure: erase %d, id %d, type %s, x %d, z %d, [%.1f, %.1f %.1f]",
  info->_erase, info->_id,
  cc_cast(info->_type->GetDisplayName()), info->_x, info->_z,
  info->_pos.X(), info->_pos.Y(), info->_pos.Z());
#endif

  int x = info->_x;
  int z = info->_z;
  TaskPtr<EntityAIType> type = info->_type;

  // set dirty flags in the area of the change
  // caution: some weapons are turret located
  float maxRange = type->GetMaxWeaponRange();

  if (maxRange > 0)
  {
    float trackRange = floatMax(type->GetIRScanRange(), TACTICAL_VISIBILITY);
    saturateMin(maxRange, trackRange);

    int r = toIntCeil(maxRange * InvLandGrid);

    int xMin = x - r;
    int xMax = x + r;
    int zMin = z - r;
    int zMax = z + r;
    if (xMin < LandRange && xMax >= 0 && zMin < LandRange && zMax >= 0)
    {
      saturateMax(xMin, 0);
      saturateMin(xMax, LandRangeMask);
      saturateMax(zMin, 0);
      saturateMin(zMax, LandRangeMask);

      for (int i=zMin; i<=zMax; i++)
      {
        for (int j=xMin; j<=xMax; j++)
        {
          float dist2 = Square(j - x) + Square(i - z);
          if (dist2 > Square(r)) continue;
          SetDirty(j, i);
        }
      }
    }
  }

  // update the target list
  if (info->_erase)
  {
    // remove the target, push both items to the trash bin
    for (int i=0; i<_targets.Size(); i++)
    {
      if (_targets[i]->_id == info->_id)
      {
        SRef<AIMapTargetInfo> remove = _targets[i];
        _targets.Delete(i);
        _targetsToErase.Add(remove);
        break;
      }
    }
    _targetsToErase.Add(info);
  }
  else
  {
    _targets.Add(info);
  }
  return true;
}

/// max vehicle speed in km/h for which exposure is calculate
/**
ignore exposure from very fast vehicles - it would not describe anything usefull anyway
*/

const float MaxExposureSpeed = 150;

struct TargetRecalculateExposure
{
  int _x, _z;
  Vector3 _fieldPos;
  /// out - exposure result
  Threat &_exposure;

  TargetRecalculateExposure(int x, int z, Vector3Par fieldPos, Threat &exposure)
    :_x(x), _z(z), _fieldPos(fieldPos), _exposure(exposure)
  {
  }

  bool operator () (const AIMapTargetInfo *info) const
  {
    TaskPtr<EntityAIType> type = info->_type;
    // consider weapons of target
    float maxRange = type->GetMaxWeaponRange();
    if (maxRange <= 0) return false;
    // if the target is very fast (like airborne units),
    // there is no use to consider it in the exposure map
    // such target should be rather considered as "threat everywhere"
    // we currently ignore it
    if (type->GetMaxSpeed() > MaxExposureSpeed) return false;

    float trackRange = floatMax(type->GetIRScanRange(), TACTICAL_VISIBILITY);
    saturateMin(maxRange, trackRange);

    float dist2 = Square(info->_x - _x) + Square(info->_z - _z);
    dist2 *= Square(LandGrid);
    // check maxRange of vehicle
    if (dist2 <= Square(maxRange))
    {
      // check strategic visibility
      // here must be info->_pos (not info->_realPos)
      float dirCoef = info->_dir.CosAngle(_fieldPos - info->_pos);
      float visibility = GLOB_LAND->VisibleStrategic(info->_pos,_fieldPos);
      // when we are very near, we should rather assume full visibility
      // statistical approach is likely to fail
      const float assumeNear = 20.0f;
      const float assumeFar = 80.0f;
      float effVisibility = Interpolativ(dist2,Square(assumeNear),Square(assumeFar),1,visibility);
      if (visibility > 0.5f)
      {
        //LogF("  pos %.1f,%.1f",info->_pos.X(),info->_pos.Z());
        _exposure += type->GetStrategicThreat(dist2, effVisibility, dirCoef);
      }
    }
    return false;
  }
};

float AIMap::RecalculateExposure(int x, int z, AIThreat &result)
{
  // return value - change of exposure
  float change = 0;
  Threat exposure;

  Vector3 fieldPos(x * LandGrid + LandGrid * 0.5, 0, z * LandGrid + LandGrid * 0.5);
  fieldPos[1] = GLOB_LAND->SurfaceY(fieldPos[0], fieldPos[2]);
  saturateMax(fieldPos[1], 0); // zero - normal sea level
  fieldPos[1] += 3;

  //LogF("Update %.1f,%.1f",(float)x*LandGrid,(float)z*LandGrid);

  TargetRecalculateExposure calc(x, z, fieldPos, exposure);
  _targets.ForEachF(calc);

  GeographyInfo geog = GLandscape->GetGeography(x, z);
  // consider unit will be cover by objects
  float coverFactor = 1 - geog.u.howManyObjects * 0.3;
  exposure = exposure * coverFactor;
  int expInt;
  expInt = toInt(exposure[VSoft] * INV_EXPOSURE_COEF);
  saturate(expInt, 0, 255);
  change += EXPOSURE_COEF * abs(expInt - result.u.soft);
  result.u.soft = expInt;
  expInt = toInt(exposure[VArmor] * INV_EXPOSURE_COEF);
  saturate(expInt, 0, 255);
  change += EXPOSURE_COEF * abs(expInt - result.u.armor);
  result.u.armor = expInt;
  expInt = toInt(exposure[VAir] * INV_EXPOSURE_COEF);
  saturate(expInt, 0, 255);
  change += EXPOSURE_COEF * abs(expInt - result.u.air);
  result.u.air = expInt;

  return change;
}

///////////////////////////////////////////////////////////////////////////////
// class AICenter

#pragma warning( disable: 4355 )

AICenter::AICenter(TargetSide side, Mode mode)
  : _sideRadio(CCSide, Pars >> "RadioChannels" >> "SideChannel"),
  _commandRadio(CCCommand, Pars >> "RadioChannels" >> "CommandChannel")
{
  _nSoldier = 0;
  _nWoman = 0;

  _side = side;

  _mode = mode;

  _resources = 0;

  _skillCoef = 1;
  _precisionCoef = 1;

  _map.Free();

  if (side < TSideUnknown && mode != AICMDisabled)
    _map = new AIMap();
  else
    _map = NULL;
  _mapExpensiveMaintenanceTime = Glob.time;

  _guardingValid = Glob.time - 1.0f;
  _supportValid = Glob.time - 1.0f;

  for (int i=0; i<TSideUnknown; i++) _friends[i] = 1;

  //load side formations parameters
  InitFormations();
}

static const EnumName EndModeNames[]=
{
  EnumName(EMContinue,"CONTINUE"),
  EnumName(EMKilled,"KILLED"),
  EnumName(EMLoser,"LOSER"),
  EnumName(EMEnd1,"END1"),
  EnumName(EMEnd2,"END2"),
  EnumName(EMEnd3,"END3"),
  EnumName(EMEnd4,"END4"),
  EnumName(EMEnd5,"END5"),
  EnumName(EMEnd6,"END6"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(EndMode dummy)
{
  return EndModeNames;
}

static const EnumName CenterModeNames[]=
{
  EnumName(AICMDisabled, "DISABLED"),
  EnumName(AICMArcade, "ARCADE"),
  EnumName(AICMIntro, "INTRO"),
  EnumName(AICMNetwork, "NETWORK"),
  EnumName(AICMArcade, "NORMAL"),
  EnumName(AICMArcade, "TRAINING"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AICenter::Mode dummy)
{
  return CenterModeNames;
}
/* Load identity EH */
class EventLoadIdentity: public IPostponedEvent
{
  OLinkO(Person) _soldier;
  AICenter *_center;

public:
  EventLoadIdentity(Person *soldier, AICenter *center)
    : _soldier(soldier), _center(center)
  {
  }

  void DoEvent() const
  {
    if (!_soldier) return;

    if (!_soldier->OnEventRetBool(EEHandleIdentity))
    {
      if (_soldier->Brain() && _soldier->Brain()->CanLoadIdentityDelayed())
      {
        IdentityInfo info;
        _center->NextSoldierIdentity(info, _soldier);
        AIBrain *agent = _soldier->Brain();
        if (agent) agent->Load(info);
      }
    }
  }

  USE_FAST_ALLOCATOR
};

void AICenter::LoadIdentityInfo(Person *soldier)
{
  if ( GWorld->GetMode() == GModeNetware )
  { // HOTFIX: use immediate CreateIdentity call in MP, as CreateAIBrain message should contain the face,name,glasses,speaker and other properties set
    if (!soldier->OnEventRetBool(EEHandleIdentity))
    {
      IdentityInfo info;
      NextSoldierIdentity(info, soldier);
      AIBrain *agent = soldier->Brain();
      if (agent) agent->Load(info);
    }
  }
  else
  {
    // identity info will be loaded during event evaluation
    GWorld->AppendPostponedEvent(new EventLoadIdentity(soldier, this));
  }
}

LSError AICenter::Serialize(ParamArchive &ar)
{
  if (ar.GetArVersion() >= 6)
  {
    CHECK(ar.Serialize("Groups", _groups, 6))
  }
  else if (ar.IsLoading())
  {
    _groups.Resize(28);
    for (int i=0; i<28; i++)
    {
      char buffer[256];
      sprintf(buffer, "Group%d", i);
      CHECK(ar.Serialize(buffer, _groups[i], 1))
    }
  }
  else
  {
    return LSStructure;
  }
  CHECK(ar.Serialize("Radio", _sideRadio, 1))
  CHECK(ar.Serialize("CommandRadio", _commandRadio, 1))

  CHECK(ar.SerializeEnum("mode", _mode, 1, ENUM_CAST(AICenterMode,AICMArcade)))
  CHECK(ar.SerializeEnum("side", _side, 1))
  CHECK(ar.Serialize("resources", _resources, 1, 0))

  for (int i=0; i<TSideUnknown; i++)
  {
    char buffer[256];
    sprintf(buffer, "friends%d", i);
    CHECK(ar.Serialize(buffer, _friends[i], 1, 1.0))
  }

  CHECK(ar.Serialize("Targets", _targets, 1))
  
  if (ar.IsLoading() && ar.GetPass()==ParamArchive::PassSecond)
  {
    _presence.Clear();
    _presence.Reserve(_targets.Size());
    for (int i=0; i<_targets.Size(); i++)
    {
      _presence.Add(_targets[i]);
    }
  }

  CHECK(ar.Serialize("soldier", _nSoldier, 1))
  CHECK(ar.Serialize("woman", _nWoman, 1, 0))

  CHECK(ar.Serialize("skillCoef", _skillCoef, 1, 1))
  CHECK(ar.Serialize("precisionCoef", _precisionCoef, 1, 1))

  CHECK(ar.Serialize("GuardingGroups", _guardingGroups, 1))
  CHECK(ar.Serialize("GuardingVehicles", _guardedVehicles, 1))
  CHECK(ar.Serialize("GuardingPoints", _guardedPoints, 1))
  CHECK(::Serialize(ar, "guardingValid", _guardingValid, 1))

  CHECK(ar.Serialize("SupportGroups", _supportGroups, 1))
  CHECK(ar.Serialize("SupportTargets", _supportTargets, 1))
  CHECK(::Serialize(ar, "supportValid", _supportValid, 1, Glob.time))

  // Recreate strategic map
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond)
  {
    if (_side < TSideUnknown)
    {
      _map = new AIMap();
      _map->Init();
      _mapExpensiveMaintenanceTime = Glob.time;
      AddNewExposures();
      _map->ProcessAllChanges();
    }
    else
      _map = NULL;
  }

  //serialize NetworkObject
  NetworkObject::Serialize(ar); //base

  if ( ar.IsLoading() && (ar.GetPass()==ParamArchive::PassSecond) 
    && !GetNetworkId().IsNull() )
  {
    //Calling CreateObject is not sufficient
    //GetNetworkManager().CreateObject(this, false /*not setNetworkId*/);
    // we should call appropriate CreateXXX methods when everything is serialized (e.g. subobjects,...)
    // store the "action" to see, what should be created "at least"
    GetNetworkManager().RememberSerializedObject(this);
  }

  return LSOK;
}

AICenter *AICenter::LoadRef(ParamArchive &ar)
{
  TargetSide side = TSideUnknown;
  if (ar.SerializeEnum("side", side, 1) != LSOK) return NULL;
  switch (side)
  {
    case TWest:
      return GWorld->GetWestCenter();
    case TEast:
      return GWorld->GetEastCenter();
    case TGuerrila:
      return GWorld->GetGuerrilaCenter();
    case TCivilian:
      return GWorld->GetCivilianCenter();
    case TLogic:
      return GWorld->GetLogicCenter();
  }
  return NULL;
}

LSError AICenter::SaveRef(ParamArchive &ar) const
{
  TargetSide side = GetSide();
  CHECK(ar.SerializeEnum("side", side, 1))
  return LSOK;
}

#define AI_CENTER_MSG_LIST(XX) \
  XX(Create, CreateAICenter) \
  XX(UpdateGeneric, UpdateAICenter)

DEFINE_NETWORK_OBJECT(AICenter, NetworkObject, AI_CENTER_MSG_LIST)

#define CREATE_AI_CENTER_MSG(MessageName, XX) \
  XX(MessageName, int, side, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, TEast), DOC_MSG("Side of center"), TRANSF)

DECLARE_NET_INDICES_EX_SET_DIAG_NAME(CreateAICenter, NetworkObject, CREATE_AI_CENTER_MSG, "center")
DEFINE_NET_INDICES_EX(CreateAICenter, NetworkObject, CREATE_AI_CENTER_MSG)

#define UPDATE_AI_CENTER_MSG(MessageName, XX) \
  XX(MessageName, float, friends0, NDTFloat, float, NCTNone, DEFVALUE(float, 1.0), DOC_MSG("Friendship to other side"), TRANSF) \
  XX(MessageName, float, friends1, NDTFloat, float, NCTNone, DEFVALUE(float, 1.0), DOC_MSG("Friendship to other side"), TRANSF) \
  XX(MessageName, float, friends2, NDTFloat, float, NCTNone, DEFVALUE(float, 1.0), DOC_MSG("Friendship to other side"), TRANSF) \
  XX(MessageName, float, friends3, NDTFloat, float, NCTNone, DEFVALUE(float, 1.0), DOC_MSG("Friendship to other side"), TRANSF)

DECLARE_NET_INDICES_EX(UpdateAICenter, NetworkObject, UPDATE_AI_CENTER_MSG)
DEFINE_NET_INDICES_EX(UpdateAICenter, NetworkObject, UPDATE_AI_CENTER_MSG)

NetworkMessageFormat &AICenter::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  switch (cls)
  {
  case NMCCreate:
    NetworkObject::CreateFormat(cls, format);
    CREATE_AI_CENTER_MSG(CreateAICenter, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    NetworkObject::CreateFormat(cls, format);
    UPDATE_AI_CENTER_MSG(updateAICenter, MSG_FORMAT)
    break;
  default:
    NetworkObject::CreateFormat(cls, format);
    break;
  }
  return format;
}

AICenter *AICenter::CreateObject(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(CreateAICenter)

  TargetSide side;
  if (TRANSF_BASE(side, (int &)side) != TMOK) return NULL;
  AICenter *center = new AICenter(side, AICMNetwork);
  GWorld->AddCenter(center);

  NetworkId objectId;
  if (TRANSF_BASE(objectCreator, objectId.creator) != TMOK) return NULL;
  if (TRANSF_BASE(objectId, objectId.id) != TMOK) return NULL;
  center->SetNetworkId(objectId);
  center->SetLocal(false);

  center->TransferMsg(ctx);

  return center;
}

void AICenter::DestroyObject()
{
  DoAssert(NGroups() == 0);
  GWorld->RemoveCenter(this);
}

TMError AICenter::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    if (ctx.IsSending())
    {
      PREPARE_TRANSFER(CreateAICenter)

      TRANSF_ENUM(side)
    }
    break;
  case NMCUpdateGeneric:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateAICenter)

      TRANSF_EX(friends0, _friends[0])
      TRANSF_EX(friends1, _friends[1])
      TRANSF_EX(friends2, _friends[2])
      TRANSF_EX(friends3, _friends[3])
    }
    break;
  default:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    break;
  }
  return TMOK;
}

float AICenter::CalculateError(NetworkMessageContextWithError &ctx)
{
  // TODO: Not implemented
  return 0.0;
}

AICenter *CreateCenter(ArcadeTemplate &t, TargetSide side, AICenter::Mode mode)
{
  switch (mode)
  {
    case AICMNetwork:
      return new AICenter(side, mode);
    case AICMArcade:
    case AICMIntro:
    {
      int n = t.groups.Size();
      for (int i=0; i<n; i++)
      {
        if (t.groups[i].side == side)
           return new AICenter(side, mode);
      }
      return NULL;
    }
  }
  return NULL;
}

bool AICenter::IsFriendly( TargetSide side) const
{
  if (side==TFriendly) return true;
  if (side==TEnemy) return false;
  if (side < 0 || side >= TSideUnknown)
    return false;

  return _friends[side] >= 0.6;
}

bool AICenter::IsNeutral( TargetSide side) const
{
  return false;
}

bool AICenter::IsEnemy( TargetSide side) const
{
  if (side==TFriendly) return false;
  if (side==TEnemy) return true;
  if (side < 0 || side >= TSideUnknown)
    return false;

//  return _friends[side] <= 0.4;
  return _friends[side] < 0.6;
}

bool AICenter::SideIsFriendly(TargetSide side) const
{
  return IsFriendly(side);
}
bool AICenter::SideIsEnemy(TargetSide side) const
{
  return IsEnemy(side);
}
bool AICenter::SideIsNeutral(TargetSide side) const
{
  return IsNeutral(side);
}

bool AICenter::IsUnknown( TargetSide side) const
{
  return side==TSideUnknown;
}

bool AICenter::IsCivilian( TargetSide side) const
{
  return side==TCivilian;
}

bool AICenter::IsWest( TargetSide side) const
{
  return side==TWest;
}

bool AICenter::IsEast( TargetSide side) const
{
  return side==TEast;
}

bool AICenter::IsResistance( TargetSide side) const
{
  return side==TGuerrila;
}

void AICenter::UpdateSupport()
{
  _supportValid = Glob.time + 5.0f + 2.0f * GRandGen.RandomValue();

  // 1. remove destroyed groups
  for (int i=0; i<_supportTargets.Size();)
  {
    AISupportTarget &target = _supportTargets[i];
    if (!target.group)
      _supportTargets.Delete(i);
    else if (target.group->UnitsCount() == 0)
      SupportDone(target.group);
    else i++;
  }

  // 2. assign groups to targets
  for (int i=0; i<_supportGroups.Size(); i++)
  {
    AISupportGroup &group = _supportGroups[i];
    if (!group.group || !group.group->Leader()) continue;
    if (group.assigned) continue;
    Vector3Val posG = group.group->Leader()->Position(group.group->Leader()->GetFutureVisualState());
    
    // find target to assign
    AISupportTarget *best = NULL;
    float minDist2 = FLT_MAX;
    for (int j=0; j<_supportTargets.Size(); j++)
    {
      AISupportTarget &target = _supportTargets[j];
      if (!target.group->Leader()) continue;
      if
      (
        target.ambulance && group.ambulance ||
        target.medic && group.medic ||
        target.repair && group.repair ||
        target.refuel && group.refuel ||
        target.rearm && group.rearm
      )
      {
        float dist2 = target.group->Leader()->Position(target.group->Leader()->GetFutureVisualState()).Distance2(posG);
        if (dist2 < minDist2)
        {
          best = &target;
          minDist2 = dist2;
        }
      }
    }

    if (best)
    {
      // assign target
      group.assigned = best->group;
      if (group.ambulance) best->ambulance = false;
      if (group.repair) best->repair = false;
      if (group.refuel) best->refuel = false;
      if (group.rearm) best->rearm = false;
      if (group.medic) best->medic = false;
      group.group->Support(group.assigned, group.assigned->Leader()->Position(group.assigned->Leader()->GetFutureVisualState()));
    }
  }
}

void AICenter::ReadyForSupport(AIGroup *grp)
{
  if (!grp->UnitsCount()) return;

  int index = -1;
  for (int i=0; i<_supportGroups.Size(); i++)
  {
    if (_supportGroups[i].group == grp)
    {
      index = i;
      break;
    }
  }

  if (index < 0)
  {
    index = _supportGroups.Add();
    _supportGroups[index].group = grp;
  }

  _supportGroups[index].ambulance = false;
  _supportGroups[index].medic = false;
  _supportGroups[index].rearm = false;
  _supportGroups[index].refuel = false;
  _supportGroups[index].repair = false;

  for (int i=0; i<grp->NUnits(); i++)
  {
    AIUnit *unit = grp->GetUnit(i);
    if (!unit || !unit->LSCanSupport()) continue;

    Transport *transport = unit->GetVehicleIn();
    if (transport && !transport->IsDamageDestroyed())
    {
      if (transport->IsAttendant())
        _supportGroups[index].ambulance = true;
      if (transport->GetRepairCargo() > 10)
        _supportGroups[index].repair = true;
      if (transport->GetFuelCargo() > 10)
        _supportGroups[index].refuel = true;
      if (transport->GetAmmoCargo() > 10)
        _supportGroups[index].rearm = true;
    }
    if (unit->GetPerson()->GetType()->IsAttendant())
      _supportGroups[index].medic = true;
  }
}

void AICenter::AskSupport(AIGroup *grp, UIActionType type)
{
  if (!grp->Leader()) return;

  int index = -1;
  for (int i=0; i<_supportTargets.Size(); i++)
  {
    if (_supportTargets[i].group == grp)
    {
      index = i;
      break;
    }
  }

  if (!CanSupport(type))
  {
    GetCommandRadio().Transmit(new RadioMessageSupportNotAvailable(grp));
    return;
  }

  if (index < 0)
  {
    index = _supportTargets.Add();
    _supportTargets[index].group = grp;
    _supportTargets[index].ambulance = false;
    _supportTargets[index].medic = false;
    _supportTargets[index].repair = false;
    _supportTargets[index].refuel = false;
    _supportTargets[index].rearm = false;
  }
  
  _supportTargets[index].pos = grp->Leader()->Position(grp->Leader()->GetFutureVisualState());
  switch (type)
  {
  case ATHeal:
    _supportTargets[index].ambulance = true;
    break;
  case ATHealSoldier:
    _supportTargets[index].medic = true;
    _supportTargets[index].requestedMedic = true;
    break;
  case ATRepair:
    _supportTargets[index].repair = true;
    break;
  case ATRefuel:
    _supportTargets[index].refuel = true;
    break;
  case ATRearm:
    _supportTargets[index].rearm = true;
    break;
  default:
    Fail("Action");
    break;
  }
}

void AICenter::SupportDone(AIGroup *grp)
{
  for (int i=0; i<_supportTargets.Size(); i++)
  {
    if (_supportTargets[i].group == grp)
    {
      _supportTargets.Delete(i);
      break;
    }
  }

  for (int i=0; i<_supportGroups.Size(); i++)
  {
    if (_supportGroups[i].assigned == grp)
    {
      _supportGroups[i].assigned = NULL;
      if (_supportGroups[i].group)
        _supportGroups[i].group->CancelSupport();
    }
  }
}

bool AICenter::IsSupported(AIGroup *grp, UIActionType type)
{
  for (int i=0; i<_supportGroups.Size(); i++)
  {
    if (_supportGroups[i].assigned == grp && _supportGroups[i].group && _supportGroups[i].group->UnitsCount() > 0)
    {
      switch (type)
      {
      case ATHeal:
        if (_supportGroups[i].ambulance) return true;
        break;
      case ATHealSoldier:
        if (_supportGroups[i].medic) return true;
        break;
      case ATRepair:
        if (_supportGroups[i].repair) return true;
        break;
      case ATRefuel:
        if (_supportGroups[i].refuel) return true;
        break;
      case ATRearm:
        if (_supportGroups[i].rearm) return true;
        break;
      case ATNone:
        if (_supportGroups[i].ambulance) return true;
        if (_supportGroups[i].medic) return true;
        if (_supportGroups[i].repair) return true;
        if (_supportGroups[i].refuel) return true;
        if (_supportGroups[i].rearm) return true;
        break;
      }
    }
  }
  return false;
}

bool AICenter::WaitingForSupport(AIGroup *grp, UIActionType type)
{
  for (int i=0; i<_supportTargets.Size(); i++)
  {
    if (_supportTargets[i].group == grp)
    {
      switch (type)
      {
      case ATHeal:
        if (_supportTargets[i].ambulance) return true;
        break;
      case ATHealSoldier:
        if (_supportTargets[i].medic) return true;
        break;
      case ATRepair:
        if (_supportTargets[i].repair) return true;
        break;
      case ATRefuel:
        if (_supportTargets[i].refuel) return true;
        break;
      case ATRearm:
        if (_supportTargets[i].rearm) return true;
        break;
      case ATNone:
        if (_supportTargets[i].medic) return true;
        if (_supportTargets[i].ambulance) return true;
        if (_supportTargets[i].repair) return true;
        if (_supportTargets[i].refuel) return true;
        if (_supportTargets[i].rearm) return true;
        break;
      }
    }
  }
  return false;
}

bool AICenter::CanSupport(UIActionType type)
{
  for (int i=0; i<_supportGroups.Size(); i++)
  {
    if (_supportGroups[i].group && _supportGroups[i].group->UnitsCount() > 0)
    {
      switch (type)
      {
      case ATHeal:
        if (_supportGroups[i].ambulance) return true;
        break;
      case ATHealSoldier:
        if (_supportGroups[i].medic) return true;
        break;
      case ATRepair:
        if (_supportGroups[i].repair) return true;
        break;
      case ATRefuel:
        if (_supportGroups[i].refuel) return true;
        break;
      case ATRearm:
        if (_supportGroups[i].rearm) return true;
        break;
      }
    }
  }
  return false;
}

void AICenter::InitMedicalSupport(AIGroupContext *context)
{
  AIGroup *medicalGroup = context->_group;
  const AIGroup *targetGroup = medicalGroup->GetSupportedGroup();
  if(!medicalGroup || !targetGroup) return;

  for (int i=0; i<_supportTargets.Size();i++)
  {
    if(_supportTargets[i].group == targetGroup)
      if(_supportTargets[i].requestedMedic) goto reguestmedic;
  }
  return;
reguestmedic:
  AutoArray<AIUnit*> medics;
  for (int i=0; i< medicalGroup->NUnits(); i++)
  {
     AIUnit *u = medicalGroup->GetUnit(i);
     if(!u || !u->GetVehicle()) continue;
     const EntityAIType *type =  u->GetVehicle()->GetType();
     if(u->IsFreeSoldier() && type->IsAttendant())
       medics.Add(u);
  }

  int index = 0;
  if(medics.Size()>0)
  {
    for (int i=0; i< targetGroup->NUnits(); i++)
    {
      AIUnit *u = targetGroup->GetUnit(i);
      if(!u) continue;
      if(context->_task->_destination.Distance2(u->Position(u->GetFutureVisualState()))>2500) continue; //heal soldiers in 50m radius
      EntityAIFull *veh = u->GetVehicle();
      if(!veh) continue;
      if(veh->NeedsAmbulance() && !veh->AssignedAttendant())
      {
        AIUnit *medic = medics[index]; //leader medic is selected last
        if(medic && medic->GetVehicle())
        {
          Command cmd;
          cmd._message = Command::HealSoldier;
          cmd._destination = u->Position(u->GetFutureVisualState());
          cmd._target = u->GetVehicle();
          cmd._time = Glob.time + 480.0;
          medicalGroup->SendAutoCommandToUnit(cmd, medic, true, false);
          veh->SetAssignedAttendant(medic->GetVehicle());
        }
        index = (index+1)%medics.Size();
      }
    }
  }
}

int CmpGuardedVehicles
(
  const AITargetInfo * const *i1,
  const AITargetInfo * const *i2
)
{
  const AITargetInfo *info1 = *i1;
  const AITargetInfo *info2 = *i2;
  if (info1->_side == TSideUnknown)
  {
    if (info2->_side != TSideUnknown) return 1;
  }
  else if (info2->_side == TSideUnknown)
  {
    return -1;
  }

  float diff = info2->_type->GetCost() - info1->_type->GetCost();
  if (diff != 0)
    return sign(diff);
  else
    return ComparePointerAddresses(info1->_idExact, info2->_idExact);
}

TypeIsSimple(const AITargetInfo *);

void AICenter::UpdateGuarding()
{
  // algorithm for assigning groups to targets
  _guardingValid = Glob.time + 25.0f + 10.0f * GRandGen.RandomValue();

  // 1. remove (temporary) groups with no units
  int nGroups = 0;
  for (int i=0; i<_guardingGroups.Size(); i++)
  {
    if (_guardingGroups[i].group && _guardingGroups[i].group->UnitsCount() > 0)
    {
      _guardingGroups[i].guarding = false;
      nGroups++;
    }
    else
    {
      _guardingGroups[i].guarding = true;
    }
  }
  if (nGroups == 0) return;

  // 2. reset points
  for (int i=0; i<_guardedPoints.Size(); i++)
  {
    _guardedPoints[i].by = NULL;
  }

  // 3. prepare list of guarded vehicles
  AutoArray<const AITargetInfo *> targets;
  for (int i=0; i<NTargets(); i++)
  {
    const AITargetInfo &info = GetTarget(i);
    if (!info._idExact) continue;
    if (info._idExact->IsDamageDestroyed()) continue;
    if (info._type->IsKindOf(GWorld->Preloaded(VTypeStatic))) continue;
    // TODO: check unknown targets handling
    if (info._side == TSideUnknown)
      targets.Add(&info);
    else if (IsEnemy(info._side))
      targets.Add(&info);
    /*
    if (IsEnemy(info._side))
      targets.Add(&info);
    */
  }

  // 4. sort list
  QSort(targets.Data(), targets.Size(), CmpGuardedVehicles);

  // 5. assign vehicles to groups
  int n = targets.Size();
  _guardedVehicles.Resize(n);
  for (int i=0; i<n; i++)
  {
    AIGurdedVehicle &info = _guardedVehicles[i];
    info.vehicle = targets[i]->_idExact;
    info.by = NULL;
    // TODO: better solution
    // try to cover instead of 1:1 assignment
    if (nGroups > 0)
    {
      float minDist2 = FLT_MAX;
      int jBest = -1;
      Vector3Val posD = targets[i]->_realPos;
      for (int j=0; j<_guardingGroups.Size(); j++)
      {
        if (_guardingGroups[j].guarding) continue;
        AIGroup *grp = _guardingGroups[j].group;
        Assert(grp);
        Assert(grp->Leader());
        Vector3Val posL = grp->Leader()->Position(grp->Leader()->GetFutureVisualState());
        float dist2 = (posD - posL).SquareSizeXZ();
        if (dist2 < minDist2)
        {
          minDist2 = dist2;
          jBest = j;
        }
      }
      //hot fix if(jBest >= 0) - target realPos was non-defined number
      if(jBest >= 0)
      {
        info.by = _guardingGroups[jBest].group;
        _guardingGroups[jBest].guarding = true;
      }
      nGroups--;
    }
  }

  // 6. assign points to groups
  // TODO: sort _guardedPoints
  for (int i=0; i<_guardedPoints.Size() && nGroups > 0; i++)
  {
    AIGurdedPoint &info = _guardedPoints[i];
    float minDist2 = FLT_MAX;
    int jBest = -1;
    Vector3Val posD = info.position;
    for (int j=0; j<_guardingGroups.Size(); j++)
    {
      if (_guardingGroups[j].guarding) continue;
      AIGroup *grp = _guardingGroups[j].group;
      Assert(grp);
      Assert(grp->Leader());
      Vector3Val posL = grp->Leader()->Position(grp->Leader()->GetFutureVisualState());
      float dist2 = (posD - posL).SquareSizeXZ();
      if (dist2 < minDist2)
      {
        minDist2 = dist2;
        jBest = j;
      }
    }
    Assert(jBest >= 0);
    info.by = _guardingGroups[jBest].group;
    _guardingGroups[jBest].guarding = true;
    nGroups--;
  } 
}

AIGuardTarget AICenter::GetGuardTarget(AIGroup *grp)
{
  AIGuardTarget result;
  result.type = GTTNothing;
  result.position = VZero;
  
  Assert(grp);
  if (!grp) return result;

  int found = -1;
  for (int i=0; i<_guardingGroups.Size();)
  {
    if (!_guardingGroups[i].group)
    {
      _guardingGroups.Delete(i);
      continue;
    }
    
    if (_guardingGroups[i].group == grp)
    {
      found = i;
      // continue - delete obsolete items
    }
    i++;
  }

  if (found < 0)
  {
    found = _guardingGroups.Add();
    _guardingGroups[found].group = grp;
    _guardingGroups[found].guarding = false;
    UpdateGuarding(); // for new group recalculate guarding immediately
  }

  if (_guardingGroups[found].guarding)
  {
    for (int i=0; i<_guardedVehicles.Size(); i++)
    {
      if (_guardedVehicles[i].by == grp)
      {
        result.type = GTTVehicle;
        result.vehicle = _guardedVehicles[i].vehicle;
        return result;
      }
    }
    for (int i=0; i<_guardedPoints.Size(); i++)
    {
      if (_guardedPoints[i].by == grp)
      {
        result.type = GTTPoint;
        result.position = _guardedPoints[i].position;
        return result;
      }
    }
  }
  return result;
}

int AICenter::AddGuardedPoint(Vector3Par pos)
{
  int index = _guardedPoints.Add();
  _guardedPoints[index].position = pos;
  return index;
}

const AITargetInfo *AICenter::FindTargetInfo(TargetType *id) const
{
  const Ref<AITargetInfo> &info = _presence.Get(id);
  if (_presence.NotNull(info))
  {
    return info;
  }
  return NULL;
}

AITargetInfo *AICenter::FindTargetInfo(TargetType *id)
{
  const Ref<AITargetInfo> &info = _presence.Get(id);
  if (_presence.NotNull(info))
  {
    return info;
  }
  return NULL;
}

void AICenter::InitPreview( const ArcadeUnitInfo &info )
{
  if (_map) _map->Init();
  _mapExpensiveMaintenanceTime = Glob.time;
  BeginPreviewUnit(info);
}

void AICenter::Init(ArcadeTemplate &t, VehicleInitMessages &inits)
{
  if (_map) _map->Init();
  _mapExpensiveMaintenanceTime = Glob.time;

  BeginArcade(t, inits);

  if (!AssertValid())
  {
    Fail("Structure invalid after init");
  }
}

void AICenter::InitSensors(bool initialize)
{
  for (int i=0; i<NGroups(); i++)
  {
    AIGroup *grp = GetGroup(i);
    if (!grp) continue;
    grp->CreateTargetList(initialize, false);
    if (grp->NEnemiesDetected()>0 && !grp->IsAnyPlayerGroup())
    {
      grp->ReactToEnemyDetected(NULL);
      grp->SetLastEnemyDetected(Glob.time + GRandGen.PlusMinus(30.0f, 5.0f));
    }
  }

  if (initialize)
  {
    if (_map)
    {
      AddNewExposures();
      _map->ProcessAllChanges();
    }
    // setup combat mode accordingly


  }
}

void AICenter::InitFormations()
{
  RString side;

  switch(_side)
  {
  case TCivilian:
    side = RString("Civ");
    break;
  case TEast:
    side = RString("East");
    break;
  case TGuerrila: 
    side = RString("Guer");
    break;
  case TWest: 
    side = RString("West");
    break;
  default:
    side = RString("West");
    break;
  }
  ParamEntryVal cls= Pars>>  "cfgFormations" >> side ;

  int i =0;
  for (i = 0; i < cls.GetEntryCount();i++)
  {
    if(i >= NForms) return;
    AutoArray<FormationPositionInfo> formationInfoFixed;
    AutoArray<FormationPositionInfo> formationInfoPattern;

    ParamEntryVal param = cls.GetEntry(i);
    if(!param.IsClass()) break;

    ParamEntryVal entry = param >> "Fixed";
    for (int j = 0; j < entry.GetEntryCount();j++)
    {
      ParamEntryVal entryVal = entry.GetEntry(j);
      if(entryVal.IsArray()){
        formationInfoFixed.Add(
          FormationPositionInfo(
          entryVal[0].GetFloat(),
          entryVal[1].GetFloat(),
          entryVal[2].GetFloat(),
          entryVal[3].GetFloat()));
      }
      else formationInfoFixed.Add(FormationPositionInfo(0,0,0,0));
    }

    entry = param >> "Pattern";
    for (int j = 0; j < entry.GetEntryCount();j++)
    {
      ParamEntryVal entryVal = entry.GetEntry(j);
      if(entryVal.IsArray())
      {
        if(entryVal.GetSize() == 4) formationInfoPattern.Add(
          FormationPositionInfo(
          entryVal[0].GetFloat(),
          entryVal[1].GetFloat(),
          entryVal[2].GetFloat(),
          entryVal[3].GetFloat()));
        else if(entryVal.GetSize() == 5) formationInfoPattern.Add(
          FormationPositionInfo(
          entryVal[0].GetFloat(),
          entryVal[1].GetFloat(),
          entryVal[2].GetFloat(),
          entryVal[3].GetFloat(),
          entryVal[4]));
      }
      else formationInfoPattern.Add(FormationPositionInfo(0,0,0,0));
    }
    _formations[i] = FormationInfo(formationInfoFixed, formationInfoPattern);
  }

  if(i<NForms-1)
  {//default line formation - if cfg formations is not complete
    for(int j = i; j<NForms; j++)
    {
      AutoArray<FormationPositionInfo> formationInfoFixed;
      AutoArray<FormationPositionInfo> formationInfoPattern;

      formationInfoFixed.Add(FormationPositionInfo(-1, 0, 0, 0));
      formationInfoFixed.Add(FormationPositionInfo(0, 1, 0, 0));
      formationInfoPattern.Add(FormationPositionInfo(-2, -1, 0, 0));
      formationInfoPattern.Add(FormationPositionInfo(-1, 1, 0, 0));

      _formations[j] = FormationInfo(formationInfoFixed, formationInfoPattern);
    }
  }
}

#define MIN_DIST2   50.0F * 50.0F
bool ValidPos(Vector3Val pos, AutoArray<Point3> &forbidden)
{
  int i;
  for (i=0; i<forbidden.Size(); i++)
    if ((forbidden[i] - pos).SquareSizeXZ() < MIN_DIST2)
      return false;
  return true;
}

struct StartingPoint
{
  Vector3 _pos;
  float _cost;

  StartingPoint(){}
  StartingPoint(Vector3Par pos, float cost) : _pos(pos), _cost(cost) {}
};
TypeIsSimple(StartingPoint)

int CmpStartingPoints(const StartingPoint *pt1, const StartingPoint *pt2)
{
  float value = pt1->_cost - pt2->_cost;
  return toInt(fSign(value));
}

static Vector3 FindStartPosition(Vector3Val center, float radius, AutoArray<Point3> *avoid = NULL)
{
  if (radius <= 0.1) return center;

  //AutoArray<Point3> points;
  // use paper car - it is cheap in terms of textures and memory
  // it is defined so that it gives most constraints on position of all all vehicles
/*
  Vector3 bestPoint = center;
  bool bestPointValid = false;
  int j;
  float minCost=1e10;
*/
  AUTO_STATIC_ARRAY_16(StartingPoint, points, 100);
  for (int j=0; j<100; j++)
  {
    float xr = (2.0 * GRandGen.RandomValue() - 1.0) * radius;
    float zr = (2.0 * GRandGen.RandomValue() - 1.0) * radius;
    if (Square(xr) + Square(zr) > radius * radius) continue;
    Point3 pos;   
    pos.Init();
    pos[0] = center[0] + xr;
    pos[1] = 0;
    pos[2] = center[2] + zr;
    int xtest = toIntFloor(pos.X() * InvLandGrid);
    int ztest = toIntFloor(pos.Z() * InvLandGrid);
    for (int x=-1; x<=1; x++)
      for (int z=-1; z<=1; z++)
      {
        GeographyInfo info = GLOB_LAND->GetGeography(xtest + x, ztest + z);
        if (GWorld->Preloaded(VTypePaperCar)->GetBaseCost(info, false, true) > 1e10)
          goto Break;
      }
    if (avoid && !ValidPos(pos, *avoid))
        continue;

    {
      GeographyInfo info = GLOB_LAND->GetGeography(xtest, ztest);
      float cost = GWorld->Preloaded(VTypePaperCar)->GetBaseCost(info, false, true);
      /*
      if (cost < minCost)
      {
        minCost = cost;
        bestPoint = pos;
        bestPointValid = true;
      }
      */
      points.Add(StartingPoint(pos, cost));
    }

Break:
    continue;
  }
  if (points.Size() == 0)
  {
    //Fail("No valid start position.");
    return center;
  }

  QSort(points.Data(), points.Size(), CmpStartingPoints);
  float limit = 5.0f * points[0]._cost;
  int n = points.Size();
  for (int i=1; i<points.Size(); i++)
  {
    if (points[i]._cost > limit)
    {
      n = i;
      break;
    }
  }

  int index = toIntFloor(n * GRandGen.RandomValue());
  saturate(index, 0, n - 1);
  return points[index]._pos;
}

OLinkPermArray<EntityAI> vehiclesMap;
OLinkPermArray<Vehicle> sensorsMap;
AutoArray<ArcadeMarkerInfo> markersMap;
AutoArray<SynchronizedItem> synchronized;

#if _VBS3 && _VBS_WP_SYNCH // waypointSynchronizedWith
void SynchronizedItem::Add(AIGroup *grp, int idx)
#else
void SynchronizedItem::Add(AIGroup *grp)
#endif
{
  int index = groups.Add();
  groups[index].group = grp;
  groups[index].active = true;
#if _VBS3 && _VBS_WP_SYNCH // waypointSynchronizedWith
  wpIndex = idx;
#endif
}

void SynchronizedItem::Add(Vehicle *sensor)
{
  int index = sensors.Add();
  sensors[index].sensor = sensor;
  sensors[index].active = true;
}

void SynchronizedItem::SetActive(AIGroup *grp, bool active)
{
  int i, n = groups.Size();
  for (i=0; i<n; i++)
  {
    if (groups[i].group == grp)
    {
      groups[i].active = active;
    }
  }
}

void SynchronizedItem::SetActive(Vehicle *sensor, bool active)
{
  int i, n = sensors.Size();
  for (i=0; i<n; i++)
  {
    if (sensors[i].sensor == sensor)
    {
      sensors[i].active = active;
    }
  }
}

bool SynchronizedItem::IsActive(AIGroup *grp)
{
  // two items in one synchronization
  // return true <=> some is active
  int n = groups.Size();
  for (int i=0; i<n; i++)
  {
    if
    (
      groups[i].group && groups[i].group != grp &&
      groups[i].group->UnitsCount() > 0 && groups[i].active
    ) return true;
  }
  n = sensors.Size();
  for (int i=0; i<n; i++)
  {
    if (sensors[i].sensor && sensors[i].active)
      return true;
  }
  return false;
}

void AIGlobalCleanUp()
{
  synchronized.Clear();
  vehiclesMap.Clear();
  sensorsMap.Clear();
  markersMap.Clear();
  void ResetMarkerID();
  ResetMarkerID();
}

// Init / load / save
void AIGlobalInit()
{
  AIGlobalCleanUp();
}


LSError SynchronizedGroup::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Group", group, 1))
  CHECK(ar.Serialize("active", active, 1, false))
  return LSOK;
}

LSError SynchronizedSensor::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Sensor", sensor, 1))
  CHECK(ar.Serialize("active", active, 1, false))
  return LSOK;
}

  LSError SynchronizedItem::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("Groups", groups, 1))
  CHECK(ar.Serialize("Sensors", sensors, 1))
  return LSOK;
}

LSError AIGlobalSerialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("Synchronized", synchronized, 1))
  CHECK(ar.SerializeRefs("VehiclesMap", vehiclesMap, 1))
  CHECK(ar.SerializeRefs("SensorsMap", sensorsMap, 1))
  CHECK(ar.Serialize("Stats", GStats, 1))
  CHECK(ar.Serialize("Markers", markersMap, 1))
  return LSOK;
}

extern const float MinHealth=0.03;

/*
static void PlaceObject( EntityAI *veh, Matrix4 &transform )
{
  Vector3 pos=transform.Position();
  Matrix3 orient=transform.Orientation();
  
  if( veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeMan)) )
  {
    pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
  }
  else
  {
    pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0], pos[2]);
  }

  if( veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeStatic)) )
  {
    // static vehicle - place as in buldozer
    pos += orient * veh->GetShape()->BoundingCenter();

  }
  else
  {
    // dynamic vehicle
    pos[1] -= veh->GetShape()->Min().Y();
  }

  transform.SetPosition(pos);
}
*/

static Vehicle *CreateSoundSource(ArcadeUnitInfo &info, ArcadeTemplate &t, bool multiplayer)
{
  RString sim = Pars >> "CfgVehicles" >> info.vehicle >> "sound";
  DynSoundSource *vehicle = new DynSoundSource(sim);
  Vector3 pos = info.position;
  int n = info.markers.Size();
  if (n > 0)
  { // randomized placement
    int i = toIntFloor((n + 1) * GRandGen.RandomValue());
    if (i < n)
    {
      RString name = info.markers[i];
      int m = t.markers.Size();
      for (int j=0; j<m; j++)
      {
        ArcadeMarkerInfo &mInfo = t.markers[j];
        if (stricmp(mInfo.name, name) == 0)
        {
          pos = mInfo.position;
          break;
        }
      }
    }
  }
  float xr, zr;
  do
  {
    xr = (2.0 * GRandGen.RandomValue() - 1.0) * info.placement;
    zr = (2.0 * GRandGen.RandomValue() - 1.0) * info.placement;
  } while (Square(xr) + Square(zr) > info.placement * info.placement);
  pos[0] += xr;
  pos[2] += zr;
  pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0], pos[2]);
  vehicle->SetPosition(pos);
  GWorld->AddSlowVehicle(vehicle);
  if (multiplayer)
    GetNetworkManager().CreateVehicle(vehicle, VLTSlowVehicle, "", -1);
  return vehicle;
}

static Vehicle *CreateMine(ArcadeUnitInfo &info, ArcadeTemplate &t, bool multiplayer)
{
  RString typeName = Pars >> "CfgVehicles" >> info.vehicle >> "ammo";
  Ref<EntityType> type = VehicleTypes.New(typeName);
  AmmoType *aType = dynamic_cast<AmmoType *>(type.GetRef());
  Vehicle *vehicle = NewShot(NULL, aType, NULL, NULL);
  Vector3 pos = info.position;
  int n = info.markers.Size();
  if (n > 0)
  { // randomized placement
    int i = toIntFloor((n + 1) * GRandGen.RandomValue());
    if (i < n)
    {
      RString name = info.markers[i];
      int m = t.markers.Size();
      for (int j=0; j<m; j++)
      {
        ArcadeMarkerInfo &mInfo = t.markers[j];
        if (stricmp(mInfo.name, name) == 0)
        {
          pos = mInfo.position;
          break;
        }
      }
    }
  }
  float xr, zr;
  do
  {
    xr = (2.0 * GRandGen.RandomValue() - 1.0) * info.placement;
    zr = (2.0 * GRandGen.RandomValue() - 1.0) * info.placement;
  } while (Square(xr) + Square(zr) > info.placement * info.placement);
  pos[0] += xr;
  pos[2] += zr;
  if (vehicle->CanFloat())
    pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0], pos[2]);
  else
    pos[1] = GLOB_LAND->SurfaceY(pos[0], pos[2]);
  vehicle->SetPosition(pos);
  GWorld->AddFastVehicle(vehicle);
  if (multiplayer)
    GetNetworkManager().CreateObject(vehicle);
  return vehicle;
}

struct MagazineInfo
{
  const MagazineType *type;
  int n;
};
TypeIsSimple(MagazineInfo)

class SetAmmoFunc : public ITurretFunc
{
protected:
  float _amount;

public:
  SetAmmoFunc(float amount) : _amount(amount) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    AUTO_STATIC_ARRAY(MagazineInfo, infos, 32);
    for (int i=0; i<context._weapons->_magazines.Size(); i++)
    {
      Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine) continue;
      MagazineType *type = magazine->_type;
      if (!type || type->_maxAmmo == 0) continue;
      int index = -1;
      for (int j=0; j<infos.Size(); j++)
      {
        if (infos[j].type == type)
        {
          index = j;
          infos[j].n++;
          break;
        }
      }
      if (index < 0)
      {
        index = infos.Add();
        infos[index].type = type;
        infos[index].n = 1;
      }
      // magazine->_ammo = toInt(info.ammo * magazine->_type->_maxAmmo);
    }
    for (int k=0; k<infos.Size(); k++)
    {
      const MagazineType *type = infos[k].type;
      if(!type) continue;
      int ammo = toInt(_amount * infos[k].n * type->_maxAmmo);
      for (int i=0; i<context._weapons->_magazines.Size(); i++)
      {
        Magazine *magazine = context._weapons->_magazines[i];
        if (!magazine) continue;
        if (magazine->_type != type) continue;
        if (ammo == 0)
        {
          context._weapons->RemoveMagazine(entity, magazine);
          i--;
        }
        else if (ammo >= type->_maxAmmo)
        {
          magazine->SetAmmo(type->_maxAmmo);
          ammo -= type->_maxAmmo;
        }
        else
        {
          magazine->SetAmmo(ammo);
          ammo = 0;
        }
      }
    }

    return false; // all turrets
  }
};

void SetAmmo(EntityAIFull *veh, float amount)
{
  Assert(veh);
  SetAmmoFunc func(amount);
  veh->ForEachTurret(func);
}

static Matrix4 PlaceVehicle(EntityAI *veh, Vector3 pos, Matrix3Par rotation, bool flying)
{
  const EntityAIType *type = veh->GetType();

  // align position to the surface, calculate the normal as well
  Vector3 normal;
  if (
    // enable placing soldier onto object road
    type->IsPerson()
    // enable placing aircraft or helipad onto carrier deck (roadway above water)
    || (type->CanBeAirborne() || type->IsKindOf(GWorld->Preloaded(VTypeHelipad))) && GLandscape->SurfaceY(pos)<GLandscape->GetSeaLevel()-2
  )
  {
#if _ENABLE_WALK_ON_GEOMETRY
    pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2], Landscape::FilterPrimary());
#else
    pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
#endif
    normal = VUp;
    if (pos[1]>GLandscape->SurfaceY(pos))
    {
      veh->SetNotOnSurface(true);
    }
  }
  else
  {
    float dx, dz;
    pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0], pos[2], &dx, &dz);
    normal = Vector3(-dx, 1, -dz);
  }

  Matrix3 dir;
  Matrix4 transform;

  if (
    // Fix: enable empty flying parachutes
    // side != TSideUnknown &&
    flying && type->IsKindOf(GWorld->Preloaded(VTypeAir))
    )
  {
    // orientation
    normal = VUp;
    dir.SetUpAndDirection(normal, VForward);
    transform.SetOrientation(dir * rotation);

    // position
    pos[1] += veh->MakeAirborne(transform.Orientation().Direction());
    transform.SetPosition(pos);
  }
  else
  {
    // orientation
    dir.SetUpAndDirection(normal, VForward);
    transform.SetOrientation(dir * rotation);

    // position
    transform.SetPosition(pos);

    // update the transformation
    if (veh->CanFloat())
      veh->PlaceOnSurface(transform);
    else
      veh->PlaceOnSurfaceUnderWater(transform);
  }

  return transform;
}

static EntityAI *CreateVehicle
(
  const ArcadeUnitInfo &info, ArcadeTemplate &t, TargetSide side, bool multiplayer,
  bool disableD
)
{
  FreeOnDemandGarbageCollect(2*1024*1024,256*1024);
  
  // avoid sounds
  RString vehClass;
  if( info.type )
  {
    vehClass=info.type->_simName;
  }
  else
  {
    ConstParamEntryPtr entry = (Pars >> "CfgVehicles").FindEntry(info.vehicle);
    if (entry) vehClass =  (*entry)>>"vehicleClass";
    else
    {
      RptF("Vehicle type %s does not exist",cc_cast(info.vehicle));
      return NULL;
    }
  }
  if (stricmp(vehClass, "Sounds") == 0) return NULL;

  Ref<EntityType> type(info.type);
  if (!type) type = VehicleTypes.New(info.vehicle);
  if (!type) return NULL;
  ProgressRefresh();

  bool isSoldier = type->IsPerson();
  if (disableD && isSoldier) return NULL;

  Ref<EntityAI> veh;
  if( info.type )
  {
    veh = GWorld->NewVehicleWithID(info.type);
  }
  else
  {
    veh = GWorld->NewVehicleWithID(info.vehicle);
  }
  if (!veh) return NULL;
  ProgressRefresh();

  Vector3 pos = info.position;
  int n = info.markers.Size();
  if (n > 0)
  { // randomized placement
    int i = toIntFloor((n + 1) * GRandGen.RandomValue());
    if (i < n)
    {
      RString name = info.markers[i];
      int m = t.markers.Size();
      for (int j=0; j<m; j++)
      {
        ArcadeMarkerInfo &mInfo = t.markers[j];
        if (stricmp(mInfo.name, name) == 0)
        {
          pos = mInfo.position;
          break;
        }
      }
    }
  }

  // find some position inside a given radius
  pos = FindStartPosition(pos, info.placement);

  float azimut = -HDegree(info.azimut);
  ProgressRefresh();

  Person *soldier = dyn_cast<Person>(veh.GetRef());
  if (soldier)
  {
    soldier->CreateBrain();
  }

#if _DEBUG  
  if (isSoldier)
  {
    Assert(soldier);
  }
  else
  {
    Assert(!soldier);
  }
#endif

  if (side != TSideUnknown) veh->SetTargetSide(side);

  if (info.name.GetLength() > 0)
  {
    veh->SetVarName(info.name);
    GWorld->GetGameState()->VarSet(info.name, GameValueExt(veh.GetRef()), true, false, GWorld->GetMissionNamespace()); // mission namespace
  }
  ProgressRefresh();
  
  // Add into World
//  veh->Repair(floatMax(MinHealth, info.health) - 1 - veh->GetTotalDamage());
  veh->SetDamage(1 - floatMax(MinHealth, info.health));
  veh->Refuel(info.fuel * veh->GetType()->GetFuelCapacity() - veh->GetFuel());
  ProgressRefresh();

  EntityAIFull *vehFull = dyn_cast<EntityAIFull>(veh.GetRef());
  if (vehFull)
  {
    SetAmmo(vehFull, info.ammo);
    vehFull->SetDescription(info.description);
  }
  ProgressRefresh();

  Matrix3 rotY(MRotationY, azimut);

  Vector3 normalDummy;
  if (info.placement > 0)
  {
    if (!AIUnit::FindFreePosition(pos, normalDummy, isSoldier, veh->GetType()))
    {
      Fail("Collision in the starting position");
    }
  }

  // calculate the transformation
  Matrix4 transform = PlaceVehicle(veh, pos, rotY, info.special == ASpFlying);
  ProgressRefresh();

  // apply the transformation
  veh->SetTransformPossiblySkewed(transform);
  veh->Init(transform, true);

  ProgressRefresh();

  // if vehicle is static, add it to building list

  if( veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeStatic)) )
  {
    GWorld->AddSlowVehicle(veh);
    if (multiplayer)
      GetNetworkManager().CreateVehicle(veh, VLTSlowVehicle, info.name, info.id);
  }
  else
  {
    GWorld->AddVehicle(veh);     
    if (multiplayer)
      GetNetworkManager().CreateVehicle(veh, VLTVehicle, info.name, info.id);

  }

  // after CreateVehicle: the event handler may reference the object being created, it needs to exists as a network object
  veh->OnEvent(EEInit);

  if (info.id >= vehiclesMap.Size())
    vehiclesMap.Resize(info.id + 1);
  vehiclesMap[info.id] = veh.GetRef();

  //as vehicle will be hierParent of backpack, wemust do it after vehicle is created as network object
  if(veh->GetSupply())
  {
    veh->GetSupply()->InitCargoBackpacks();
  }

  // Add into target list
  if (side != Glob.header.playerSide)
  {
    AICenter *center = GWorld->GetCenter((TargetSide)Glob.header.playerSide);
    if(center)
    {
      float time = 0;
      switch (info.age)
      {
        case AAActual:
          time = 0;
          break;
        case AA5Min:
          time = 5 * 60;
          break;
        case AA10Min:
          time = 10 * 60;
          break;
        case AA15Min:
          time = 15 * 60;
          break;
        case AA30Min:
          time = 30 * 60;
          break;
        case AA60Min:
          time = 60 * 60;
          break;
        case AA120Min:
          time = 120 * 60;
          break;
        case AAUnknown:
          time = AccuracyLast;
          break;
      }
      center->InitTarget(veh, time);
    }
  }

  Transport *transport = dyn_cast<Transport>(veh.GetRef());
  if (transport)
  {
//    transport->SetLocked(info.locked);
    transport->SetLock(info.lock);

    // generate plate number from "regulations"
    ParamEntryVal world=Pars>>"CfgWorlds">> Glob.header.worldname;
    RString format = world>>"plateFormat";
    RString letters = world>>"plateLetters";

    int seed = info.id*4586 + t.randomSeed*871;

    int plateId = toLargeInt(GRandGen.RandomValue(seed)*0x1000000);

    int nLetters = letters.GetLength();

    char plate[256];
    char *p=plate;
    for (const char *f=format; *f; f++)
    {
      if(*f=='$')
      {
        *p++ = letters[plateId%nLetters];
        plateId/=nLetters;
      }
      else if (*f=='#')
      {
        *p++ = '0'+plateId%10;
        plateId/=10;
      }
      else
      {
        *p++=*f;
      }
    }
    *p=0;
    // offer the plate to the vehicle
    transport->SetPlateNumber(plate);
  }

  return veh;
}

Vehicle *CreateSensor(const ArcadeSensorInfo &info, AIGroup *grp, bool multiplayer)
{
  FreeOnDemandGarbageCollect(1024*1024,256*1024);
  
  Vector3 pos = info.position, normal = VUp;
  pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);

  AICenter *center = NULL;
  switch (info.type)  // special cases - guarded by ...
  {
    case ASTEastGuarded:
      center = GWorld->GetEastCenter();
      goto Guarded;
    case ASTWestGuarded:
      center = GWorld->GetWestCenter();
      goto Guarded;
    case ASTGuerrilaGuarded:
      center = GWorld->GetGuerrilaCenter();
      goto Guarded;
    Guarded:
      if (!center) return NULL;
      if (grp)
      {
        Assert(grp->Leader());
        if (grp->Leader()) pos = grp->Leader()->Position(grp->Leader()->GetFutureVisualState());
      }
      else
      {
        if (!info.idObject.IsNull())
        {
          OLink(Object) obj = GLandscape->GetObject(info.idObject);
          if (obj) pos = obj->FutureVisualState().Position();
        }
        else
        {
          if (info.idVehicle >= 0 && info.idVehicle < vehiclesMap.Size())
          {
            Vehicle *veh = vehiclesMap[info.idVehicle];
            if (veh) pos = veh->FutureVisualState().Position(); 
          }
        }
      }
      {
        int index = center->_guardedPoints.Add();
        center->_guardedPoints[index].position = pos;
      }
      return NULL;
  }

  Ref<Vehicle> vehicle = GWorld->NewNonAIVehicleWithID(info.object);
  if (!vehicle) return NULL;

  if (info.name.GetLength() > 0)
  {
    vehicle->SetVarName(info.name);
    GWorld->GetGameState()->VarSet(info.name, GameValueExt(vehicle.GetRef()), true, false, GWorld->GetMissionNamespace()); // mission namespace
  }

  // position is on sea level now
  if( vehicle->GetShape() )
  {
    pos += vehicle->GetShape()->BoundingCenter();
  }
  Matrix4 transform;
  // TODO: random azimut
  transform.SetUpAndDirection(normal, Vector3(0,0,1));
  transform.SetPosition(pos);
  vehicle->SetTransform(transform);

  Assert(dyn_cast<Detector>(vehicle.GetRef()));
  Detector *sensor = static_cast<Detector *>(vehicle.GetRef());
  sensor->FromTemplate(info);
  if (grp)
  {
    Assert(sensor->GetActivationBy() == ASAGroup);
    sensor->AssignGroup(grp);
  }

  // Add into World
  GWorld->AddSlowVehicle(vehicle);
  if (multiplayer)
    GetNetworkManager().CreateVehicle(vehicle, VLTSlowVehicle, info.name, -1);

  sensorsMap.Add(vehicle.GetRef());

  center = GWorld->GetCenter((TargetSide)Glob.header.playerSide);
  if(center)
  {
    float time = 0;
    switch (info.age)
    {
      case AAActual:
        time = 0;
        break;
      case AA5Min:
        time = 5 * 60;
        break;
      case AA10Min:
        time = 10 * 60;
        break;
      case AA15Min:
        time = 15 * 60;
        break;
      case AA30Min:
        time = 30 * 60;
        break;
      case AA60Min:
        time = 60 * 60;
        break;
      case AA120Min:
        time = 120 * 60;
        break;
      case AAUnknown:
        time = AccuracyLast;
        break;
    }

    // TODO: non-AI detectors InitTarget ???
    //center->InitTarget(sensor, time);
  }

  for (int k=0; k<sensor->NSynchronizations(); k++)
  {
    int sync = sensor->GetSynchronization(k);
    Assert(sync >= 0);
    if (sync >= synchronized.Size())
      synchronized.Resize(sync + 1);
    synchronized[sync].Add(sensor);
  }

  return vehicle;
}

void CreateMarker(const ArcadeMarkerInfo &info, bool multiplayer)
{
  markersMap.Add(info);
}

static void InitPlayer(AIBrain *playerUnit)
{
  playerUnit->GetPerson()->SetRemotePlayer(AI_PLAYER);

  // avoid identity overwrite when delay EH processed
  playerUnit->LoadIdentityDelayed();

  AIUnitInfo &info = playerUnit->GetPerson()->GetInfo();
#if _VBS2 // VBS2 nickname
  info._name = Glob.header.playerHandle;
#else
  info._name = Glob.header.GetPlayerName();
#endif
  info._face = Glob.header.playerFace;
  info._glasses = Glob.header.playerGlasses;
  info._speaker = playerUnit->GetPerson()->SelectSpeaker(Glob.header.playerSpeakerType);
  info._pitch = Glob.header.playerPitch;

  playerUnit->GetPerson()->SetFace(info._face);
  playerUnit->GetPerson()->SetGlasses(info._glasses);
  playerUnit->SetSpeaker(info._speaker, info._pitch);
};

void InitNoCenters(ArcadeTemplate &t, VehicleInitMessages &inits, AICenterMode mode)
{
  AIBrain *playerUnit = NULL;

  // empty vehicles
  int m = t.emptyVehicles.Size();
  for (int j=0; j<m; j++)
  {
    ArcadeUnitInfo &uInfo = t.emptyVehicles[j];
    if (GRandGen.RandomValue() > uInfo.presence) continue;
    if (!GWorld->GetGameState()->EvaluateBool(uInfo.presenceCondition, GameState::EvalContext::_default, GWorld->GetMissionNamespace())) continue; // mission namespace

    ConstParamEntryPtr entry = (Pars >> "CfgVehicles").FindEntry(uInfo.vehicle);
    if (!entry)
    {
      RptF("Vehicle type %s does not exist",cc_cast(uInfo.vehicle));
      return;
    }
    RString vehClass =  (*entry)>>"vehicleClass";

    if (stricmp(vehClass, "Sounds") == 0)
      CreateSoundSource(uInfo, t, mode == AICMNetwork);
    else if (stricmp(vehClass, "Mines") == 0)
      CreateMine(uInfo, t, mode == AICMNetwork);
    else
    {
      EntityAI *veh = CreateVehicle(uInfo, t, TSideUnknown, mode == AICMNetwork, false);
      if (veh)
      {
        // init expression
        if (uInfo.init.GetLength() > 0)
        {
          int index = inits.Add();
          inits[index]._vehicle = veh;
          inits[index]._init = uInfo.init;
        }
        // synchronizations - empty vehicle cannot store synchronization, add only to the unit
        for (int k=0; k<uInfo.synchronizations.Size(); k++)
        {
          int id = uInfo.synchronizations[k];
          if (id >= 0 && id < vehiclesMap.Size())
          {
            AIBrain *unit = vehiclesMap[id] ? vehiclesMap[id]->CommanderUnit() : NULL;
            if (unit) unit->AddSynchronizedObject(veh);
          }
        }
      }
      if (mode == AICMNetwork)
      {
        for (int r=0; r<GetNetworkManager().NPlayerRoles(); r++)
        {
          const PlayerRole *role = GetNetworkManager().GetPlayerRole(r);
          if ((role->player != AI_PLAYER || role->GetFlag(PRFEnabledAI)) &&
            role->side == uInfo.side && role->group == -1 && role->unit == j)
          {
            AIBrain *player = NULL;
            if (role->turret.Size() == 0)
            {
              player = veh->PilotUnit();
            }
            else
            {
              Transport *trans = dyn_cast<Transport>(veh);
              Assert(trans);
              const Turret *turret = trans->GetTurret(role->turret.Data(), role->turret.Size());
              Assert(turret);
              Assert(turret->Gunner());
              player = turret->Gunner()->Brain();
            }
            Assert(player);
            player->SetRoleIndex(r);
            if (role->player != AI_PLAYER)
            {
              // apply identity
              player->GetPerson()->SetRemotePlayer(role->player);
              const PlayerIdentity *identity = GetNetworkManager().FindIdentity(role->player);
              if (identity)
              {
                AIUnitInfo &info = player->GetPerson()->GetInfo();
                if (identity->squad)
                {
                  info._squadTitle = identity->squad->_title;
                  if (identity->squad->_picture.GetLength() > 0)
                  {
                    info._squadPicturePath = RString("\\squads\\") + identity->squad->_nick + RString("\\") + identity->squad->_picture;
                    RString GetClientCustomFilesDir();
                    RString picture = GetClientCustomFilesDir() + info._squadPicturePath;
                    picture.Lower();
                    if (QIFileFunctions::FileExists(picture))
                    {
                      info._squadPicture = GlobLoadTexture(picture);
                    }
                  }
                }
                // Set Face,Glasses,Speaker without parameters decides itself where to find its data (_info xor playerIdentity)
                player->GetPerson()->SetFace();
                player->GetPerson()->SetGlasses();
                player->SetSpeaker();
              }
            }
          }
        }
      }
      else switch (uInfo.player)
      {
      case APPlayerCommander:
        playerUnit = veh->CommanderUnit();
        if (playerUnit)
        {
          InitPlayer(playerUnit);
          GWorld->AddSwitchableUnit(playerUnit);
        }
        break;
      case APPlayerDriver:
        playerUnit = veh->PilotUnit();
        if (playerUnit)
        {
          InitPlayer(playerUnit);
          GWorld->AddSwitchableUnit(playerUnit);
        }
        break;
      case APPlayerGunner:
        playerUnit = veh->GunnerUnit();
        if (playerUnit)
        {
          InitPlayer(playerUnit);
          GWorld->AddSwitchableUnit(playerUnit);
        }
        break;
      case APPlayableD:
      case APPlayableCD:
      case APPlayableDG:
      case APPlayableCDG:
        // simplified - CreateVehicle cannot create more units
        if (veh->PilotUnit())
        {
          // add driver to the list of switchable units
          GWorld->AddSwitchableUnit(veh->PilotUnit());
        }
        break;
      }
    }
    ProgressAdvance(CoefAdvanceUnit);
    ProgressRefresh();
  }
  // create sensors - must be after empty vehicles
  m = t.sensors.Size();
  for (int j=0; j<m; j++)
  {
    ArcadeSensorInfo &sInfo = t.sensors[j];
    CreateSensor(sInfo, NULL, mode == AICMNetwork);
  }
  // markers
  m = t.markers.Size();
  for (int j=0; j<m; j++)
  {
    ArcadeMarkerInfo &mInfo = t.markers[j];
    CreateMarker(mInfo, mode == AICMNetwork);
  }

  if (playerUnit)
  {
    if (mode == AICMIntro)
    {
      if (playerUnit)
        GWorld->SwitchCameraTo(playerUnit->GetVehicle(), CamExternal, true);
    }
    else
    {
      GStats._campaign._playerInfo.unit = playerUnit;

      // keep rank and exp. from editor
      if (mode != AICMNetwork)
      {
        PlayerInfo().rank = playerUnit->GetPerson()->GetInfo()._rank;
        PlayerInfo().experience = playerUnit->GetPerson()->GetExperience();
      }

      #if _ENABLE_REPORT
      DWORD start = GlobalTickCount();
      #endif
      GWorld->SwitchCameraTo(playerUnit->GetVehicle(), CamInternal, true);
      #if _ENABLE_REPORT
      DWORD time = GlobalTickCount() - start;
      LogF("SwitchCamera takes %d ms", time);
      #endif
      GWorld->SetPlayerManual(true);
      GWorld->SwitchPlayerTo(playerUnit->GetPerson());
      GWorld->SetRealPlayer(playerUnit->GetPerson());
    }
  }
}

bool IsIdentityDead(RString identity);
void EmptyDeadIdentities();

/*!
\patch_disabled 5088 Date 11/15/2006 by Bebul
- Fixed: cfgVoices contain voices for man and femaleVoices for woman
*/

void CreateIdentity(IdentityInfo &result, const Person *person, TargetSide side, int &index)
{
  // the list of usable faces
  AUTO_STATIC_ARRAY(RStringB, faces, 100);
  RString faceType = person->GetFaceType();
  if (faceType.GetLength() > 0)
  {
    ParamEntryVal cfgFaces = Pars >> "CfgFaces" >> person->GetFaceType();
    for (int i=0; i<cfgFaces.GetEntryCount(); i++)
    {
      ParamEntryVal cfgFace = cfgFaces.GetEntry(i);
      if (!cfgFace.IsClass()) continue;

      // check if some identity type of this face is matching with the person
      ParamEntryVal identityTypes = cfgFace >> "identityTypes";
      bool found = false;
      for (int j=0; j<identityTypes.GetSize(); j++)
      {
        if (person->IsIdentityEnabled(identityTypes[j]))
        {
          found = true;
          break;
        }
      }
      if (!found) continue; // face is not matching this person

      faces.Add(cfgFace.GetName());
    }
  }
  if (faces.Size() == 0)
  {
    // avoid empty array - random selection is processed later
    faces.Add("Default");
  }

  // random glasses
  AUTO_STATIC_ARRAY(RStringB, glasses, 32);
  AUTO_STATIC_ARRAY(float, glassesWeight, 32);
  ParamEntryVal cfgGlasses = Pars >> "CfgGlasses";
  for (int i=0; i<cfgGlasses.GetEntryCount(); i++)
  {
    ParamEntryVal cls = cfgGlasses.GetEntry(i);
    if (!cls.IsClass()) continue;
    // decide weight by the identity type
    ParamEntryVal identityTypes = cls >> "identityTypes";
    if (identityTypes.GetSize() % 2 != 0)
    {
      RptF("%s.identityTypes[] - even number of items expected (pairs [name, weight])");
    }
    else
    {
      for (int j=0; j<identityTypes.GetSize(); j+=2) // pairs [type, weight]
      {
        if (person->IsIdentityEnabled(identityTypes[j]))
        {
          // matching identity type found
          glasses.Add(cls.GetName());
          glassesWeight.Add(identityTypes[j + 1]);
          break;
        }
      }
    }
  }
  if (glasses.Size() == 0)
  {
    // avoid empty array - random selection is processed later
    glasses.Add("None");
    glassesWeight.Add(1.0f);
  }

  // the list of usable voices
  AUTO_STATIC_ARRAY(RStringB, voices, 100);

  RString voiceStr;

  ParamEntryVal cls = Pars >> "CfgVoice";
  for (int i=0; i<cls.GetEntryCount(); i++)
  {
    ParamEntryVal voice = cls.GetEntry(i);
    if (! voice.IsClass()) continue;

    ConstParamEntryPtr scope = voice.FindEntry("scope");
    if (!scope || !(bool)*scope) continue;

    ConstParamEntryPtr entry = voice.FindEntry("disabled");
    if (entry && (bool)*entry) continue;

    // check if some identity type of this voice is matching with the person
    ParamEntryVal identityTypes = voice >> "identityTypes";
    bool found = false;
    for (int j=0; j<identityTypes.GetSize(); j++)
    {
      if (person->IsIdentityEnabled(identityTypes[j]))
      {
        found = true;
        break;
      }
    }
    if (!found) continue; // face is not matching this person

    voices.Add(voice.GetName());
  }

  // identities config 
  const char *nameSide = NULL;
  switch (side)
  {
  case TEast:
    nameSide = "East";
    break;
  case TWest:
    nameSide = "West";
    break;
  case TCivilian:
    nameSide = "Civilian";
    break;
  default:
    Fail("No such side");
    // continue
  case TGuerrila:
  case TLogic:
    nameSide = "Guerrila";
    break;
  }
  Assert(nameSide);
  RString names = person->GetGenericNames();
  ParamEntryVal cfg = Pars >> "CfgWorlds" >> "GenericNames" >> names;

  for (int j=0; j<100; j++) // check max. 100 identities
  {
    int seed = 100;
    // first name
    int nFirst = (cfg >> "FirstNames").GetEntryCount();
    int nLast = (cfg >> "LastNames").GetEntryCount();
    if (nFirst>0 && nLast>0)
    {
      int iFirst = toIntFloor(nFirst * GRandGen.RandomValue(CurrentTemplate.randomSeed, index, seed++));
      int iLast = toIntFloor(nLast * GRandGen.RandomValue(CurrentTemplate.randomSeed, index, seed++));
      saturate(iFirst, 0, nFirst - 1);
      saturate(iLast, 0, nLast - 1);
      RString firstName = (cfg >> "FirstNames").GetEntry(iFirst);
      RString lastName = (cfg >> "LastNames").GetEntry(iLast);
      // name
      result._name = firstName + RString(" ") + lastName;
    }

    // face
    int nFace = faces.Size();
    int iFace = toIntFloor(nFace * GRandGen.RandomValue(CurrentTemplate.randomSeed, index, seed++));
    saturate(iFace, 0, nFace - 1);
    result._face = faces[iFace];

    // glasses
    float totalWeight = 0;
    for (int i=0; i<glassesWeight.Size(); i++) totalWeight += glassesWeight[i];
    float weight = totalWeight * GRandGen.RandomValue(CurrentTemplate.randomSeed, index, seed++);

    int iGlasses = glasses.Size() - 1;
    for (int i=0; i<glassesWeight.Size(); i++)
    {
      weight -= glassesWeight[i];
      if (weight <= 0)
      {
        iGlasses = i; break;
      }
    }
    result._glasses = glasses[iGlasses];

    // speaker
    int voicesCount = voices.Size();
    if (voicesCount > 0)
    {
      int iVoice = toIntFloor(voicesCount * GRandGen.RandomValue(CurrentTemplate.randomSeed, index, seed++));
      saturate(iVoice, 0, voicesCount - 1);
      result._speaker = voices[iVoice];
    }

    // pitch
    result._pitch = 0.95f + 0.1f * GRandGen.RandomValue(CurrentTemplate.randomSeed, index, seed++);

    // check if not dead
    index++;
    if (!IsIdentityDead(result._name)) return;
  }
  // 100 attempts done
  EmptyDeadIdentities();
  // return the last one
}

void AICenter::NextSoldierIdentity(IdentityInfo &result, const Person *person)
{
  int &index = person->IsWoman() ? _nWoman : _nSoldier;
  CreateIdentity(result, person, _side, index);
}

extern void ApplyEffects(AIGroup *group, int index);

AIBrain *AICenter::CreateSoldier(Transport *transport, int rank, bool multiplayer)
{
  RString nameType = transport->Type()->GetCrew();
  ProgressRefresh();
  Person *soldier = dyn_cast<Person>(GWorld->NewVehicleWithID(nameType));
  ProgressRefresh();
  if (!soldier)
  {
    WarningMessage("Invalid crew %s in %s",cc_cast(nameType),cc_cast(transport->Type()->GetName()));
    // if given type does not exit, select any suitable soldier type
    return NULL;
  }
  soldier->SetTargetSide(_side);

  AIBrain *unit = soldier->CreateBrain();

  // Add into World
  // maintain correct position in hierarchy
  soldier->SetPosition(transport->FutureVisualState().Position());

  // we are creating the soldier inside of some vehicle
  GWorld->AddOutVehicle(soldier);
  soldier->SetMoveOutDone(transport);
  //GWorld->AddVehicle(soldier);

  LoadIdentityInfo(soldier);
  
  AIUnitInfo &info = soldier->GetInfo();
  DoAssert(rank >= RankPrivate);
  info._rank = (Rank)rank;
  info._initExperience = info._experience = ExpForRank((Rank)rank);
  return unit;
}

/*
void CheckPosition(Object *obj, const Vector3 &newPos)
{
  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (veh && stricmp(veh->GetVarName(), "com1") == 0)
  {
    Vector3 dif = obj->Position() - newPos;
    if (dif.SquareSize() > Square(0.01))
    {
      LogF("Move to: %.3f, %.3f, %.3f", newPos.X(), newPos.Y(), newPos.Z());
    }
  }
}
*/

/// Functor creating the gunner person
class CreateGunner : public ITurretFunc
{
protected:
  AICenter *_center;
  AIGroup *_grp;
  const ArcadeUnitInfo &_info;
  bool _multiplayer;
  int _groupIndex;
  int _unitIndex;

public:
  CreateGunner(
    AICenter *center, AIGroup *grp, 
    const ArcadeUnitInfo &info, bool multiplayer, int groupIndex, int unitIndex)
    : _center(center), _grp(grp),
    _info(info), _multiplayer(multiplayer), _groupIndex(groupIndex), _unitIndex(unitIndex) {}

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if (!context._turretType->_hasGunner) return false; // continue
    Transport *vehicle = static_cast<Transport *>(entity); // we know where we call it

    // check if role is disabled
    if (_multiplayer)
    {
      for (int r=0; r<GetNetworkManager().NPlayerRoles(); r++)
      {
        const PlayerRole *role = GetNetworkManager().GetPlayerRole(r);
        if (
          role->player == AI_PLAYER &&
          !role->GetFlag(PRFEnabledAI) &&
          role->side == _center->GetSide() &&
          role->group == _groupIndex &&
          role->unit == _unitIndex &&
          role->turret.Size() > 0)
        {
          // same vehicle, check turret
          Turret *turret = vehicle->GetTurret(role->turret.Data(), role->turret.Size());
          if (turret == context._turret)
          {
            return false; // continue
          }
        }
      }
    }

    // rank calculation
    int rank = _info.rank + toInt(context._turretType->_commanding);
    saturate(rank, 0, NRanks - 1);

    // create a soldier, assign a variable name
    AIBrain *gunner = _center->CreateSoldier(vehicle, (Rank)rank, _multiplayer);
    if (gunner)
    {
    RString name;
    if (_info.name.GetLength() > 0)
    {
      if (context._turretType->_primaryGunner)
        name = _info.name + RString("g");
      else if (context._turretType->_primaryObserver)
        name = _info.name + RString("c");
      if (name.GetLength() > 0)
      {
        gunner->GetPerson()->SetVarName(name);
        GWorld->GetGameState()->VarSet(name, GameValueExt(gunner->GetPerson()), true, false, GWorld->GetMissionNamespace()); // mission namespace
      }
    }
    if (_multiplayer)
      GetNetworkManager().CreateVehicle(gunner->GetPerson(), VLTVehicle, name, -1);

    // get into vehicle
    vehicle->GetInTurret(context._turret, gunner->GetPerson(), false);
    gunner->AssignToTurret(vehicle, context._turret);
    gunner->OrderGetIn(true);
    
    if (gunner->GetUnit()) _grp->AddUnit(gunner->GetUnit());
    gunner->SetRawAbility(_info.skill);
    }

    return false; // continue
  }
};

AIBrain *AICenter::CreateUnit(
  const ArcadeUnitInfo &info, ArcadeTemplate &t, bool multiplayer, AIGroup *grp, int groupIndex, int unitIndex
)
{
  FreeOnDemandGarbageCollect(2*1024*1024,256*1024);
  
  if (info.player == APNonplayable)
  {
    if (GRandGen.RandomValue() > info.presence) return NULL;
    if (!GWorld->GetGameState()->EvaluateBool(info.presenceCondition, GameState::EvalContext::_default, GWorld->GetMissionNamespace())) return NULL; // mission namespace
  }

  // check if driver position is disabled
  bool disable = false;
  if (multiplayer)
  {
    for (int r=0; r<GetNetworkManager().NPlayerRoles(); r++)
    {
      const PlayerRole *role = GetNetworkManager().GetPlayerRole(r);
      if (
        role->player == AI_PLAYER &&
        !role->GetFlag(PRFEnabledAI) &&
        role->side == _side &&
        role->group == groupIndex &&
        role->unit == unitIndex &&
        role->turret.Size() == 0)
      {
        disable = true;
        break;
      }
    }
  }

  Ref<EntityAI> veh = CreateVehicle(info, t, _side, multiplayer, disable);
  if (!veh) return NULL;

  Assert(veh->RefCounter()>1);

  FreeOnDemandGarbageCollect(2*1024*1024,256*1024);
  
  Person *soldier = dyn_cast<Person>(veh.GetRef());
  if (soldier)
  {
    AIBrain *agent = soldier->Brain(); // we can convert it here - old AI structures are used
    if (info.special == ASpCargo)
    {
      for (int i=0; i<grp->NUnits(); i++)
      {
        AIUnit *u = grp->GetUnit(i);
        if (!u || !u->IsUnit()) continue;
        Transport *veh = u->GetVehicleIn();
        if (!veh) continue;
        if (veh->GetFreeManCargo() > 0)
        {
          veh->GetInCargo(soldier,false);
          agent->SetInCargo(true);
          agent->AssignAsCargo(veh);
          agent->OrderGetIn(true);
          // TODO: if (multiplayer) ... 
          break;
        }
      }
    }
    else if (_side != TLogic)
    {
      // soldier is free soldier - we should add sensor
      GWorld->AddSensor(soldier);
    }

    LoadIdentityInfo(soldier);

    AIUnitInfo &aiInfo = soldier->GetInfo();
    DoAssert(info.rank >= RankPrivate);
    aiInfo._rank = (Rank)info.rank;
    aiInfo._initExperience = aiInfo._experience = ExpForRank((Rank)info.rank);
    agent->SetRawAbility(info.skill);

    // Add into AIGroup
    if (agent->GetUnit()) grp->AddUnit(agent->GetUnit());
/*
if (stricmp(info.name, "com1") == 0)
{
  LogF("Init: %.3f, %.3f, %.3f", soldier->Position().X(), soldier->Position().Y(), soldier->Position().Z());
} 
*/
    return agent;
  }
  else
  {
    Transport *transport = dyn_cast<Transport>(veh.GetRef());
    if( !transport )
    {
      RptF("%s is not soldier nor transport.",(const char *)veh->GetDebugName());
      Fail("No transport"); 
      return NULL;
    }

    Assert(transport);
    // Add transport into group
    grp->AddVehicle(transport);

    // Create crew
    if (transport->GetType()->HasDriver() && !disable)
    {
    
      AIBrain *driver = CreateSoldier(transport, info.rank, multiplayer);
      if (driver)
      {
        RString name;
        if (info.name.GetLength() > 0)
        {
          name = info.name + RString("d");
          driver->GetPerson()->SetVarName(name);
          GWorld->GetGameState()->VarSet(
            name, GameValueExt(driver->GetPerson()), true, false, GWorld->GetMissionNamespace()
          ); // mission namespace
        }
        if (multiplayer)
          GetNetworkManager().CreateVehicle(driver->GetPerson(), VLTVehicle, name, -1);
        transport->GetInDriver(driver->GetPerson(),false);
        driver->AssignAsDriver(transport);
        driver->OrderGetIn(true);
        if (driver->GetUnit()) grp->AddUnit(driver->GetUnit());
        driver->SetRawAbility(info.skill);
      }
    }

    CreateGunner func(this, grp, info, multiplayer, groupIndex, unitIndex);
    transport->ForEachTurret(func);

    AIBrain *unit = transport->CommanderUnit();
    if (unit)
    {
      if (_side != TLogic) GWorld->AddSensor(unit->GetPerson());
    }
    return unit;
  }
}

EntityAI *AICenter::CreateBackpack(EntityAIFull* vehicle, bool multiplayer)
{
  //EntityAIFull *vehicle = unit->GetVehicle();
  if(!vehicle) return NULL;

  if( vehicle->CanCarryBackpack() && vehicle->GetType()->_defaultBackPack.GetLength()>0)
  {
    EntityAI *backpack = GWorld->NewVehicleWithID(vehicle->GetType()->_defaultBackPack);

    // Add into World
    // maintain correct position in hierarchy
    backpack->SetPosition(vehicle->FutureVisualState().Position());

    // we are creating the soldier inside of some vehicle
    GWorld->AddOutVehicle(backpack);
    backpack->SetMoveOutDone(vehicle);
    //GWorld->AddVehicle(soldier);
    if(vehicle->GetWeapons().GetBackpack()) vehicle->GetWeapons().DropBackpack(vehicle);
    vehicle->GetWeapons().SetBackpack(backpack);

    if (multiplayer)
    {
      if(backpack)
        GetNetworkManager().CreateVehicle(backpack, VLTOut, "", -1);
    }

    return backpack;
  }
  return NULL;
}

static float ConvertSkill(float skill0to1)
{
  return skill0to1;
}

/// functor searching for gunners needed to be added to the list of switchable units
class AddTurretUnits : public ITurretFunc
{
protected:
  bool _gunner;
  bool _observer;

public:
  AddTurretUnits(bool gunner, bool observer) : _gunner(gunner), _observer(observer) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if (!context._gunner) return false; // continue
    AIBrain *gunnerUnit = context._gunner->Brain();
    if (!gunnerUnit) return false; // continue

    if (context._turret->GetWeapons()._magazineSlots.Size() > 0 && !context._turretType->_primaryObserver)
    {
      // gunner
      if (!_gunner) return false; // continue
    }
    else
    {
      // observer
      if (!_observer) return false; // continue
    }
    GWorld->AddSwitchableUnit(gunnerUnit);
    return false; // continue
  }
};

void AICenter::InitSkills(bool multiplayer)
{
  // check if we are friendly to the player or not
  bool enemy = false;
  if (!multiplayer)
  {
    TargetSide playerSide = Glob.header.playerSide;
    if (_friends[playerSide]<0.6) enemy = true;
  }
  else
  {
    enemy = true;
    // check for playable units for this side
    for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
    {
      if (_side == GetNetworkManager().GetPlayerRole(i)->side)
      {
        enemy = false; // playable unit found
        break;
      }
    }
  }
  _skillCoef = ConvertSkill(Glob.config.GetAISkill(enemy));
  _precisionCoef = ConvertSkill(Glob.config.GetAIPrecision(enemy));
}

/*!
\patch 5139 Date 3/14/2007 by Jirka
- Fixed: Mission editor - objects which cannot float are placed underwater now
*/

struct InFormationInfo
{
  AIBrain *_unit;
  bool _flying;

  InFormationInfo(AIBrain *unit, bool flying) : _unit(unit), _flying(flying) {}
  InFormationInfo(){}
  ClassIsSimple(InFormationInfo);
};

void AICenter::BeginArcade(ArcadeTemplate &t, VehicleInitMessages &inits)
{
  FreeOnDemandGarbageCollectMain(2*1024*1024,256*1024);
    
  _nSoldier = 0;
  _nWoman = 0;
  SetResources(0); // resources are not used

  bool multiplayer = _mode == AICMNetwork;

  // patch waypoints position for user missions
  bool patchWpPos = GetMissionParameters().GetLength() > 0;

  // create groups
  AIBrain *playerUnit = NULL;
  if (_side == TCivilian)
  {
    _friends[TCivilian] = 1.0f;
    
    // friendship of military units is equal to resistance
    _friends[TGuerrila] = 1.0f;
    _friends[TWest] = t.intel.friends[TGuerrila][TWest];
    _friends[TEast] = t.intel.friends[TGuerrila][TEast];
  }
  else if (_side >= TSideUnknown)
  {
    // logic etc
    _friends[TEast] = 1.0;
    _friends[TWest] = 1.0;
    _friends[TGuerrila] = 1.0;
    _friends[TCivilian] = 1.0;
  }
  else
  {
    _friends[TEast] = t.intel.friends[_side][TEast];
    _friends[TWest] = t.intel.friends[_side][TWest];
    _friends[TGuerrila] = t.intel.friends[_side][TGuerrila];
    _friends[TCivilian] = 1.0;
  }

  InitSkills(multiplayer);

  ProgressRefresh();
  
  int n = t.groups.Size();
  int group = -1;
  for (int i=0; i<n; i++)
  {
    FreeOnDemandGarbageCollect(2*1024*1024,256*1024);
    
    ArcadeGroupInfo &gInfo = t.groups[i];
    if (gInfo.side != _side) continue;
    group++;

    AIGroup *grp = new AIGroup();
    AddGroup(grp);
    grp->SetRoleIndex(group);
    AIUnit *leaderUnit = NULL;
    AutoArray< InFormationInfo, MemAllocDataStack<InFormationInfo, 32> > inFormation;
    // OLinkPermNOArray(AIBrain) inFormation;
    // create vehicles pass 1
    int m = gInfo.units.Size();
    for (int j=0; j<m; j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      if (uInfo.special == ASpCargo) continue;
      AIBrain *unit = CreateUnit(uInfo, t, multiplayer, grp, group, j);

      ProgressAdvance(CoefAdvanceUnit);
      ProgressRefresh();
    
      if (!unit) continue;

      EntityAI *veh = unit->GetVehicle();
      if (multiplayer)
      {
        for (int r=0; r<GetNetworkManager().NPlayerRoles(); r++)
        {
          const PlayerRole *role = GetNetworkManager().GetPlayerRole(r);
          if
          (
            (role->player != AI_PLAYER || role->GetFlag(PRFEnabledAI)) &&
            role->side == _side &&
            role->group == group &&
            role->unit == j
          )
          {
            AIBrain *player = NULL;
            if (role->turret.Size() == 0)
            {
              player = veh->PilotUnit();
            }
            else
            {
              Transport *trans = dyn_cast<Transport>(veh);
              Assert(trans);
              const Turret *turret = trans->GetTurret(role->turret.Data(), role->turret.Size());
              Assert(turret);
              Assert(turret->Gunner());
              player = turret->Gunner()->Brain();
            }
            Assert(player);
            player->SetRoleIndex(r);
            Transport *vehicle = unit->GetVehicleIn();
            if (vehicle && vehicle->PilotUnit() == player && GetNetworkManager().GetRespawnVehicleDelay() >= 0)
            {
              // mark vehicle as respawnable
              vehicle->SetRespawnSide(_side);
              vehicle->SetRespawnDelay(GetNetworkManager().GetRespawnVehicleDelay());
              vehicle->SetRespawnFlying(uInfo.special == ASpFlying && vehicle->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)));
              vehicle->SetRespawnUnit(player);
            }
            if (role->player != AI_PLAYER)
            {
              // apply identity
              player->GetPerson()->SetRemotePlayer(role->player);
              const PlayerIdentity *identity = GetNetworkManager().FindIdentity(role->player);
              if (identity)
              {
                AIUnitInfo &info = player->GetPerson()->GetInfo();
                if (identity->squad)
                {
                  info._squadTitle = identity->squad->_title;
                  if (identity->squad->_picture.GetLength() > 0)
                  {
                    info._squadPicturePath = RString("\\squads\\") + identity->squad->_nick + RString("\\") + identity->squad->_picture;
                    RString GetClientCustomFilesDir();
                    RString picture = GetClientCustomFilesDir() + info._squadPicturePath;
                    picture.Lower();
                    if (QIFileFunctions::FileExists(picture))
                    {
                      info._squadPicture = GlobLoadTexture(picture);
                    }
                  }
                }
                // Set Face,Glasses,Speaker without parameters decides itself where to find its data (_info xor playerIdentity)
                unit->GetPerson()->SetFace();
                unit->GetPerson()->SetGlasses();
                unit->SetSpeaker();
              }
            }
          }
        }
      }
      else
      {
        Transport *vehicle = unit->GetVehicleIn();
        switch (uInfo.player)
        {
        case APPlayerCommander:
          playerUnit = veh->CommanderUnit();
          if (playerUnit)
          {
            InitPlayer(playerUnit);
            GWorld->AddSwitchableUnit(playerUnit);
          }
          if (vehicle)
          {
            // add all observers to the list of switchable units
            AddTurretUnits func(false, true);
            vehicle->ForEachTurret(func);
          }
          break;
        case APPlayerDriver:
          playerUnit = veh->PilotUnit();
          if (playerUnit)
          {
            InitPlayer(playerUnit);
            // add driver to the list of switchable units
            GWorld->AddSwitchableUnit(playerUnit);
          }
          break;
        case APPlayerGunner:
          playerUnit = veh->GunnerUnit();
          if (playerUnit) InitPlayer(playerUnit);
          if (vehicle)
          {
            // add all gunners to the list of switchable units
            AddTurretUnits func(true, false);
            vehicle->ForEachTurret(func);
          }
          break;
        case APPlayableC:
          if (vehicle)
          {
            // add all observers to the list of switchable units
            AddTurretUnits func(false, true);
            vehicle->ForEachTurret(func);
          }
          break;
        case APPlayableD:
          if (veh->PilotUnit())
          {
            // add driver to the list of switchable units
            GWorld->AddSwitchableUnit(veh->PilotUnit());
          }
          break;
        case APPlayableG:
          if (vehicle)
          {
            // add all gunners to the list of switchable units
            AddTurretUnits func(true, false);
            vehicle->ForEachTurret(func);
          }
          break;
        case APPlayableCD:
          if (vehicle)
          {
            // add all observers to the list of switchable units
            AddTurretUnits func(false, true);
            vehicle->ForEachTurret(func);
          }
          if (veh->PilotUnit())
          {
            // add driver to the list of switchable units
            GWorld->AddSwitchableUnit(veh->PilotUnit());
          }
          break;
        case APPlayableCG:
          if (vehicle)
          {
            // add all observers and gunners to the list of switchable units
            AddTurretUnits func(true, true);
            vehicle->ForEachTurret(func);
          }
          break;
        case APPlayableDG:
          if (veh->PilotUnit())
          {
            // add driver to the list of switchable units
            GWorld->AddSwitchableUnit(veh->PilotUnit());
          }
          if (vehicle)
          {
            // add all gunners to the list of switchable units
            AddTurretUnits func(true, false);
            vehicle->ForEachTurret(func);
          }
          break;
        case APPlayableCDG:
          if (vehicle)
          {
            // add all observers and gunners to the list of switchable units
            AddTurretUnits func(true, true);
            vehicle->ForEachTurret(func);
          }
          if (veh->PilotUnit())
          {
            // add driver to the list of switchable units
            GWorld->AddSwitchableUnit(veh->PilotUnit());
          }
          break;
        }
      }

      if (uInfo.leader) leaderUnit = unit->GetUnit();
      if (uInfo.special == ASpForm || uInfo.special == ASpFlying)
        inFormation.Add(InFormationInfo(unit, uInfo.special == ASpFlying));

      if (uInfo.init.GetLength() > 0)
      {
        int index = inits.Add();
        inits[index]._vehicle = veh;
        inits[index]._init = uInfo.init;
      }

      // synchronizations - the unit created later will handle both links
      for (int k=0; k<uInfo.synchronizations.Size(); k++)
      {
        int id = uInfo.synchronizations[k];
        if (id >= 0 && id < vehiclesMap.Size())
        {
          AIBrain *syncUnit = vehiclesMap[id] ? vehiclesMap[id]->CommanderUnit() : NULL;
          if (syncUnit)
          {
            syncUnit->AddSynchronizedObject(unit->GetPerson());
            unit->AddSynchronizedObject(syncUnit->GetPerson());
          }
        }
      }

    }
    // create vehicles pass 2
    // m is still # of units
    for (int j=0; j<m; j++)
    {
      ArcadeUnitInfo &uInfo = gInfo.units[j];
      if (uInfo.special != ASpCargo) continue;
      
      if (multiplayer)
      {
        bool disable = false;
        for (int r=0; r<GetNetworkManager().NPlayerRoles(); r++)
        {
          const PlayerRole *role = GetNetworkManager().GetPlayerRole(r);
          if
          (
            role->side == _side &&
            role->group == group &&
            role->unit == j
          )
          {
            disable = role->player == AI_PLAYER && !role->GetFlag(PRFEnabledAI);
            break;
          }
        }
        if (disable) continue;
      }

      AIBrain *unit = CreateUnit(uInfo, t, multiplayer, grp);

      ProgressAdvance(CoefAdvanceUnit);
      ProgressRefresh();

      if (!unit) continue;

      if (multiplayer)
      {
        for (int r=0; r<GetNetworkManager().NPlayerRoles(); r++)
        {
          const PlayerRole *role = GetNetworkManager().GetPlayerRole(r);
          if
          (
            (role->player != AI_PLAYER || role->GetFlag(PRFEnabledAI)) &&
            role->side == _side &&
            role->group == group &&
            role->unit == j
          )
          {
            unit->SetRoleIndex(r);
            if (role->player != AI_PLAYER)
            {
              // apply identity
              unit->GetPerson()->SetRemotePlayer(role->player);
              const PlayerIdentity *identity = GetNetworkManager().FindIdentity(role->player);
              if (identity)
              {
                AIUnitInfo &info = unit->GetPerson()->GetInfo();
                if (identity->squad)
                {
                  info._squadTitle = identity->squad->_title;
                  if (identity->squad->_picture.GetLength() > 0)
                  {
                    info._squadPicturePath = RString("\\squads\\") + identity->squad->_nick + RString("\\") + identity->squad->_picture;
                    RString GetClientCustomFilesDir();
                    RString picture = GetClientCustomFilesDir() + info._squadPicturePath;
                    picture.Lower();
                    if (QIFileFunctions::FileExists(picture))
                    {
                      info._squadPicture = GlobLoadTexture(picture);
                    }
                  }
                }
                // Set Face,Glasses,Speaker without parameters decides itself where to find its data (_info xor playerIdentity)
                unit->GetPerson()->SetFace();
                unit->GetPerson()->SetGlasses();
                unit->SetSpeaker();
              }
            }
          }
        }
      }
      else
      {
        // in cargo
        switch (uInfo.player)
        {
        case APPlayerCommander:
        case APPlayerDriver:
        case APPlayerGunner:
          // player
          playerUnit = unit;
          InitPlayer(playerUnit);
          // continue
        case APPlayableC:
        case APPlayableD:
        case APPlayableG:
        case APPlayableCD:
        case APPlayableCG:
        case APPlayableDG:
        case APPlayableCDG:
          // playable
          GWorld->AddSwitchableUnit(playerUnit);
          break;
        }
      }
      if (uInfo.leader) leaderUnit = unit->GetUnit();

      if (uInfo.init.GetLength() > 0)
      {
        int index = inits.Add();
        inits[index]._vehicle = unit->GetPerson();
        inits[index]._init = uInfo.init;
      }

      // synchronizations - the unit created later will handle both links
      for (int k=0; k<uInfo.synchronizations.Size(); k++)
      {
        int id = uInfo.synchronizations[k];
        if (id >= 0 && id < vehiclesMap.Size())
        {
          AIBrain *syncUnit = vehiclesMap[id] ? vehiclesMap[id]->CommanderUnit() : NULL;
          if (syncUnit)
          {
            syncUnit->AddSynchronizedObject(unit->GetPerson());
            unit->AddSynchronizedObject(syncUnit->GetPerson());
          }
        }
      }
    }
    
    if (grp->UnitsCount() == 0)
    {
      grp->RemoveFromCenter();
      continue;
    }

    if (leaderUnit)
    {
      grp->SelectLeader(leaderUnit);
      grp->SortUnits();
    }
    else
    {
      grp->SortUnits();
      SelectLeader(grp);
      leaderUnit = grp->Leader();
      Assert(leaderUnit);
    }

    grp->PrepareGroup(gInfo, multiplayer, patchWpPos);

    // force units into formation
    AIUnit *leader = grp->MainSubgroup()->Leader();
    Vector3 direction = leader->Direction(leader->GetFutureVisualState());
    direction[1] = 0;
    direction.Normalize();
    grp->MainSubgroup()->SetDirection(direction);
    grp->MainSubgroup()->SetMoveDirection(direction);
    grp->MainSubgroup()->UpdateFormationPos();
    m = inFormation.Size();
    for (int j=0; j<m; j++)
    {
      AIUnit *unit = inFormation[j]._unit->GetUnit();
      Assert(unit);
      if (!unit) continue;
      if (unit == leader) continue;
      if (!unit->IsUnit()) continue;
      bool flying = inFormation[j]._flying;

      EntityAI *veh = unit->GetVehicle();

      Vector3 pos;
      if (veh->IsMovingInConvoy())
      {
        // convoy initialization
        AISubgroup *subgrp = unit->GetSubgroup();
        EntityAIFull *follow=subgrp->GetFormationPrevious(unit)->GetVehicle();
        // follow is never NULL for non-leader
        // each of vehicles has half influence to formation distance
        float factorZ = ( veh->GetFormationZ() +follow->GetFormationZ() )*0.5;
        pos = follow->FutureVisualState().Position()-subgrp->GetFormationDirection()*factorZ;
        // we do not care about Y now - PlaceOnSurface done later
      }
      else
      {
        // formation initialization
        pos = unit->GetFormationAbsolute(0,true);
      }
      
      Vector3 normal;

      if (!unit->FindNearestEmpty(pos, MASK_AVOID_OBJECTS))
      {
        Fail("Collision in the starting position");
      }

      // calculate the transformation
      Matrix3 orient;
      orient.SetUpAndDirection(VUp, direction);
      Matrix4 transform = PlaceVehicle(veh, pos, orient, flying);

      // apply the transformation
      veh->Move(transform);
      // InitPositionHistory needs to be called here, because Init was called in CreateVehicle 
      veh->InitPositionHistory(transform);
    }


    m = grp->NUnits();
    for (int j=0; j<m; j++)
    {
      AIUnit *unit = grp->GetUnit(j);
      if(unit) CreateBackpack(unit->GetVehicle(), multiplayer);
    }

    grp->MainSubgroup()->UpdateFormationCoef(true);
  }
  ProgressRefresh();
  
  if (playerUnit)
  {
//    AIGroup *grp = playerUnit ? playerUnit->GetGroup() : NULL;
    if (_mode == AICMIntro)
    {
      if (playerUnit)
        GWorld->SwitchCameraTo(playerUnit->GetVehicle(), CamExternal, true);
    }
    else
    {
      GStats._campaign._playerInfo.unit = playerUnit;

      // keep rank and exp. from editor
      if (_mode != AICMNetwork)
      {
        PlayerInfo().rank = playerUnit->GetPerson()->GetInfo()._rank;
        PlayerInfo().experience = playerUnit->GetPerson()->GetExperience();
      }

DWORD start = GlobalTickCount();
      GWorld->SwitchCameraTo(playerUnit->GetVehicle(), CamInternal, true);
DWORD time = GlobalTickCount() - start;
LogF("SwitchCamera takes %d ms", time);
      GWorld->SetPlayerManual(true);
      GWorld->SwitchPlayerTo(playerUnit->GetPerson());
      GWorld->SetRealPlayer(playerUnit->GetPerson());
    }
    GWorld->InitCameraPos();
  }
  ProgressRefresh();
}

void AICenter::BeginPreviewUnit( const ArcadeUnitInfo &info )
{
  if (_map) _map->Init();
  _mapExpensiveMaintenanceTime = Glob.time;

  ArcadeTemplate t;

  _nSoldier = 0;
  _nWoman = 0;
  SetResources(0); // resources are not used

  // create groups
  for (int i=0; i<TSideUnknown; i++) _friends[i] = 1.0f;
  
  AIGroup *grp = new AIGroup();
  AddGroup(grp);
  OLinkPermNOArray(AIBrain) inFormation;
  // create vehicles pass 1

  const ArcadeUnitInfo &uInfo = info;
  
  AIBrain *unit = CreateUnit(uInfo, t, false, grp);
  if (!unit) return;

  EntityAI *veh = unit->GetVehicle();
  GWorld->GetGameState()->VarSet("bis_buldozer_zero", GameValueExt(veh), true, false, GWorld->GetMissionNamespace()); // what namespace?

  AIBrain *playerUnit = veh->CommanderUnit();

  AIUnit *leaderUnit = playerUnit->GetUnit();

  if (grp->UnitsCount() == 0)
  {
    grp->RemoveFromCenter();
    return;
  }

  grp->SortUnits();
  grp->CalculateMaximalStrength();
  if (leaderUnit)
    grp->SelectLeader(leaderUnit);
  else
  {
    SelectLeader(grp);
    leaderUnit = grp->Leader();
    Assert(leaderUnit);
  }

  grp->MainSubgroup()->UpdateFormationCoef();

  if (playerUnit)
  {
    playerUnit->GetPerson()->GetInfo()._name = "Dummy Bummy";

    GWorld->SwitchCameraTo(playerUnit->GetVehicle(), CamInternal, true);
    GWorld->GetGameState()->VarSet("bis_buldozer_cursor", GameValueExt(playerUnit->GetVehicle()), true, false, GWorld->GetMissionNamespace()); // what namespace?
    GWorld->SetPlayerManual(true);
    GWorld->SwitchPlayerTo(playerUnit->GetPerson());
    GWorld->SetRealPlayer(playerUnit->GetPerson());

//    playerUnit->_speaker = -1;
  }
}


void AICenter::SelectLeader(AIGroup *grp)
{
  AIUnit *unit = grp->LeaderCandidate(NULL);
  if (unit)
  {
    grp->SelectLeader(unit);
  }
}

void AICenter::AddGroup(AIGroup *grp, int id)
{
  if (id == 0)
  { // attach new id
    id = 1;
    AutoArray<bool, MemAllocLocal<bool,256> > used;
    DoAssert(_groups.Size()<MaxGroups);
    used.SetAll(_groups.Size()+1,false);
    for (int i=0; i<_groups.Size(); i++)
    {
      AIGroup *grp = _groups[i];
      if (!grp) continue;
      DoAssert(grp->_id>0);
      if (grp->_id<used.Size()) used[grp->_id-1]=true;
    }
    // some unused slot must exist, as there are _groups.Size()+1 slots and we have set at most _groups.Size() of them
    for (int i=0; i<_groups.Size()+1; i++)
    {
      if (!used[i])
      {
        id = i+1;
        break;
      }
    }
    DoAssert(!used[id-1]);
  }
  #if _ENABLE_REPORT
    if (id>MaxGroups)
    {
      RptF("Group id over limit"); // debug opportunity when asserts are disabled
    }
  #endif
  DoAssert(id>0 && id<=MaxGroups);
  _groups.Add(grp);
  grp->_center = this;
  grp->_id = id;
  grp->Init();
}

void AICenter::GroupRemoved(AIGroup *grp)
{
  Assert(grp->UnitsCount() == 0);
  Assert(grp->ID() > 0);
  for (int i=0; i<NGroups(); i++)
  {
    if (grp == _groups[i])
    {
      _groups.Delete(i);
      return;
    }
  }
  Fail("Not found.");
}

struct TargetDeleteOldExposure
{
  AICenter::AITargetInfoPresence &_presence;
  
  TargetDeleteOldExposure(AICenter::AITargetInfoPresence &presence)
  :_presence(presence){}
  
  bool operator () (const AITargetInfo *info) const
  {
    // cannot delete while exposure is still tracked
    if (info->_exposure) return false;
    // check if target is old enough
    if (Glob.time > info->_time + AccuracyLast || info->_idExact.IsNull())
    {
      _presence.Remove(info->_idExact);
      return true;
    }
    return false;
  }
};

void AICenter::UpdateMap()
{
  if (!_map) return;

  // Expensive maintenance running once per second
  {
    PROFILE_SCOPE_EX(aiMIE, ai);
    if (Glob.time > _mapExpensiveMaintenanceTime + 1.0f)
    {
      _mapExpensiveMaintenanceTime = Glob.time;

      AddNewExposures();
      RemoveOldExposures();

      // Update database - remove targets older than AccuracyLast
      _targets.ForEachWithDelete(TargetDeleteOldExposure(_presence));
    }
  }

#if USE_AIMAP_THREAD
  // when job is still running, try in the next frame again
  if (_map->GetJobState() == Job::JSRunning ||  _map->GetJobState() == Job::JSWaiting) return;
#endif

#if USE_AIMAP_THREAD
  bool runAsJob = GJobManager.IsUsable();
#else
  bool runAsJob = false;
#endif

  // copy all changes from external to internal queue (prepare the next iteration)
  {
    TimeManagerScope time;
    time.Start(TCAICenterMap);
    _map->ProcessQueue();
  }

  // call Update now if 
  if (!runAsJob) _map->Update();

  // Process changes from the recent Update

  // Subgroup exposure changes
  {
    PROFILE_SCOPE_EX(aiSEC, ai);

    int nUpdates = 0;
    for (int i=0; i<_map->_changes.Size(); i++)
    {
      const ExposureChange &change = _map->_changes[i];
      for (int g=0; g<NGroups(); g++)
      {
        AIGroup *grp = GetGroup(g);
        if (!grp) continue;

        for (int sg=0; sg<grp->NSubgroups(); sg++)
        {
          AISubgroup *subgrp = grp->GetSubgroup(sg);
          if (!subgrp) continue;
          subgrp->ExposureChanged(change._x, change._z, change._changeEnemy);
          nUpdates++;
        }
      }
    }
    _map->_changes.Resize(0);
    ADD_COUNTER(aiMaE, nUpdates);
  }

  // Dealloc the queue items allocated during Update
  {
    _map->_targetsToErase.Resize(0);
  }

#if USE_AIMAP_THREAD
  // all data prepared, can launch job now
  // priority higher than for path planning
  if (runAsJob) GJobManager.CreateJob(_map, 2000);
#endif
}

void AICenter::Think()
{
  PROFILE_SCOPE_EX(aiCnt, ai);
  if (Glob.time >= _guardingValid) UpdateGuarding();
  if (Glob.time >= _supportValid) UpdateSupport();
}

void AICenter::SendMission(AIGroup *to, Mission &mis)
{
  Assert(to);

#if LOG_COMM
  Log("Send Mission: Group %s: Mission %d",
  (const char *)to->GetDebugName(), mis._action);
#endif

  to->ReceiveMission(mis);
}

void AICenter::ReceiveAnswer(AIGroup *from, Answer answer)
{
  if (!from)
    return;

#if LOG_COMM
  Log("Receive answer: Group %s: Answer %d",
  (const char *)from->GetDebugName(), answer);
#endif

  switch (answer)
  {
    case AI::MissionCompleted:
    case AI::WorkCompleted:
    {
      AIUnit *leader = from->Leader();
      if (leader)
      {
        leader->AddExp(ExperienceMissionCompleted);
      }
//      from->_missionStatus = MSCompleted;
    }
    break;
    case AI::MissionFailed:
    case AI::WorkFailed:
    {
      AIUnit *leader = from->Leader();
      if (leader)
      {
        leader->AddExp(ExperienceMissionFailed);
      }
//      from->_missionStatus = MSFailed;
    }
    break;
  }
}

#if LOG_COMM
static const char * sideNames[] =
{
  "East",
  "West",
  "Guerrila",
  "Civilian",
  "Unknown"
};
#endif  

struct TargetRemoveOldExposure
{
  AIMap * _map;
  
  explicit TargetRemoveOldExposure(AIMap *map):_map(map){}
  bool operator () (AITargetInfo *info) const
  {

    if (!info->_exposure) return false;
    
    if (Glob.time > info->_time + ExposureTimeout)
    {
      _map->AddChange(true, *info);
      info->_exposure = false;
    }
    return false;
  }
};

void AICenter::RemoveOldExposures()
{
  PROFILE_SCOPE_EX(aiREx, aiMap);

  _targets.ForEachF(TargetRemoveOldExposure(_map));
}

struct TargetAddNewExposure
{
  const AICenter *_center;
  AIMap * _map;
  
  explicit TargetAddNewExposure(const AICenter *center, AIMap *map):_center(center),_map(map){}
  bool operator () (AITargetInfo *info) const
  {
    if (info->_exposure) return false;
    if (info->_vanished) return false;
    if (info->_destroyed) return false;
    if (info->_type && info->_type->GetMaxSpeed()>MaxExposureSpeed) return false;
    
    if (Glob.time <= info->_time + ExposureTimeout)
    {
      if (_center->IsEnemy(info->_side))
      {
        _map->AddChange(false, *info);
        info->_exposure = true;
      }
    }
    return false;
  }
};

void AICenter::AddNewExposures()
{
  PROFILE_SCOPE_EX(aiAEx, aiMap);

  _targets.ForEachF(TargetAddNewExposure(this,_map));
}

void AICenter::InitTarget(EntityAI *veh, float age)
{
  // InitTarget can be called only once for every target
  // age is in seconds
  const AITargetInfo *info = FindTargetInfo(veh);
  Assert(!info);
  if (info) return;

  AITargetInfo *newInfo = new AITargetInfo;
  _targets.Add(newInfo);
  AITargetInfo &item = *newInfo;
  item._idExact = veh;
  // nasty trick: avoid detecting as empty
  item._side = veh->EntityAI::GetTargetSide();
  item._type = veh->GetType();
  item._realPos = veh->FutureVisualState().Position();
  item._pos = veh->FutureVisualState().Position();
  item._x = toIntFloor(item._pos.X() * InvLandGrid);
  item._z = toIntFloor(item._pos.Z() * InvLandGrid);
  // 2 hours is used to represent unknown
  if (age<7200)
  {
    item._accuracySide = 1.0;
    item._accuracyType = 1.0;
  }
  else
  {
    // when unknown, we know nothing until we are reported something
    item._accuracySide = 0;
    item._accuracyType = 0;
  }
  item._timeSide = Glob.time - age;
  item._timeType = Glob.time - age;
  item._time = Glob.time - age;
  item._exposure = false;
  if (age<=5*60)
  {
    item._dir = veh->FutureVisualState().Direction();
  }

  #if LOG_TARGETS
    LogF
    (
      "%s target inserted (init) %s (side %d, age %.0f)",
      (const char *)GetDebugName(),(const char *)item._idExact->GetDebugName(),
      item._side, age
    );
  #endif
  _presence.Add(newInfo);
}

void AICenter::DeleteTarget(TargetType *id)
{
  AITargetInfo *item = FindTargetInfo(id);
  if (!item) return;

  Assert(item->_idExact == id);

  #if LOG_TARGETS
    LogF
    (
      "%s target deleted %s (side %d)",
      (const char *)GetDebugName(),(const char *)id->GetDebugName(),
      item._side
    );
  #endif

  if (item->_exposure)
  {
    _map->AddChange(true, *item);
  }
  _targets.DeleteKey(item);
  _presence.Remove(id);
}

void AICenter::UpdateTarget(TargetNormal &target)
{
  AITargetInfo *was = FindTargetInfo(target.idExact);
  if (was)
  {
    AITargetInfo &item = *was;
    Assert(item._idExact == target.idExact);
    float oldAccuracyType = item.FadingTypeAccuracy();
    float oldAccuracySide = item.FadingSideAccuracy();
    float newAccuracyType = target.FadingAccuracy();
    float newAccuracySide = target.FadingSideAccuracy();
    TargetSide newSide;

    bool change = false;
    if (newAccuracySide >= floatMin(oldAccuracySide, 2.1))
    {
      newSide = target.side;
      item._timeSide = Glob.time;
      item._accuracySide = newAccuracySide;
      change = true;
    }
    else
      newSide = item._side;
    const VehicleType *newType;
    if (newAccuracyType > oldAccuracyType)
    {
      newType = target.type;
      item._timeType = Glob.time;
      item._accuracyType = newAccuracyType;
      change = true;
    }
    else
      newType = item._type;

    float errSize = target.FadingPosAccuracy();
    bool forceUpdate = false;
    
    Vector3 updatedDir = item._dir;
    // reports older than the info we have can be ignored
    if (target.lastSeen>item._time)
    {
      // LandAimingPosition is including any error
      Vector3 tgtPos = target.LandAimingPosition();
      // when multiple units are reporting info about the same unit, it can be confusing
      // some unit may have info which is recent, but inaccurate, another one may have old, but accurate
      // we want to prefer the more accurate info, unless the info is contradictory, in which case we prefer the new one
      
      
      if (
        // if the new information is a lot more accurate, use the new one
        errSize<=item._precisionPos*0.75f ||
        // if the new information contradicts the old one, use the new one - the old must be obsolete now
        tgtPos.Distance2(item._realPos)>=Square((item._precisionPos+errSize)*1.5f)
      )
      {
        #if 0 // _DEBUG
        // error trap: replacing good info with much worse
        float newError2 = tgtPos.Distance2(target.idExact->Position());
        float oldError2 = item._realPos.Distance2(target.idExact->Position());
        if (newError2>Square(100) && newError2>oldError2*Square(0.5))
        {
          __asm nop;
        }
        #endif
        // LandAimingPosition is including any error
        // if we are interested in the last position and the error, we should use AimingPosition + PosAccuracy instead
        item._realPos = tgtPos;
        item._precisionPos = errSize;
        change = true;

        const float cosAngleThold = 0.866f; // cos 30
        Vector3 tgtDir = target.GetDirection();
        if (tgtDir.CosAngle(item._dir)<cosAngleThold)
        {
          // if direction changed too much, recalculate exposure
          forceUpdate = true;
        }
        updatedDir = tgtDir;
      }
      // but even when target info is not updated, target time should be
      item._time = target.lastSeen;
    }


    if (target.vanished!=item._vanished)
    {
      item._vanished = target.vanished;
      change = true;
      forceUpdate = true;
    }

    if (target.destroyed!=item._destroyed)
    {
      item._destroyed = target.destroyed;
      newSide = target.side;
      change = true;
      forceUpdate = true;
    }
    

    if( change ) // some change in target information
    {
      int x = toIntFloor(item._realPos.X() * InvLandGrid);
      int z = toIntFloor(item._realPos.Z() * InvLandGrid);
      if
      (
        forceUpdate ||
        item._x != x || item._z != z ||
        item._side != newSide ||
        item._type != newType
      )
      {

        bool isExposureStored = item._exposure;
        if (isExposureStored)
        {
          // submit negative change - delete the target
          _map->AddChange(true, item);
          item._exposure = false;
        }
        item._pos = item._realPos;
        item._dir = updatedDir;
        item._x = x;
        item._z = z;
        item._type = newType;
        item._side = newSide;

        if (isExposureStored)
        {
          // submit positive change - insert the target again
          // TargetAddNewExposure will set the _exposure to true
          // unless the target is no longer interesting
          TargetAddNewExposure add(this,_map);
          add(&item);
        }
      }
    }
  }
  else
  {
    // target not found
    AITargetInfo *newItem = new AITargetInfo;
    _targets.Add(newItem);
    AITargetInfo &item = *newItem;
    item._idExact = target.idExact;
    item._side = target.side;
    item._type = target.type;
    item._realPos = target.LandAimingPosition();
    item._precisionPos = target.FadingPosAccuracy();
    item._pos = target.position;
    item._dir = target.GetDirection();
    item._x = toIntFloor(item._pos.X() * InvLandGrid);
    item._z = toIntFloor(item._pos.Z() * InvLandGrid);
    item._accuracySide = target.FadingSideAccuracy();
    item._timeSide = Glob.time;
    item._accuracyType = target.FadingAccuracy();
    item._timeType = Glob.time;
    item._time = target.lastSeen;
    item._exposure = false;

    #if LOG_TARGETS
      LogF
      (
        "%s target inserted (update) %s (side %d)",
        (const char *)GetDebugName(),(const char *)target.idExact->GetDebugName(),
        item._side
      );
    #endif
    _presence.Add(newItem);
  }
}

void AICenter::ReceiveReport(AIGroup *from, ReportSubject subject, TargetNormal &target)
{
  UpdateTarget(target);
}

AIThreat AIThreat::Unpacked::_null;

AIThreat AIThreat::operator + (const AIThreat &b) const
{
  int airSum = u.air + b.u.air;
  int softSum = u.soft + b.u.soft;
  int armorSum = u.armor + b.u.armor;
  if (airSum>0xff) airSum = 0xff;
  if (softSum>0xff) softSum = 0xff;
  if (armorSum>0xff) armorSum = 0xff;
  return AIThreat(softSum,armorSum,airSum);
}

AIThreat AICenter::GetThreatPessimistic(int x, int z) const
{
  if (!_map) return AIThreat::GetNull();
  if( !InRange(x,z) ) return AIThreat::GetNull();
  return _map->GetExposureEnemy(x,z);
}

AIThreat AICenter::GetThreatOptimistic(int x, int z) const
{
  if (!_map) return AIThreat::GetNull();
  if( !InRange(x,z) ) return AIThreat::GetNull();
  return _map->GetExposureEnemy(x,z);
}

float AICenter::GetExposurePessimistic(int x, int z) const
{
  if (!_map) return 0;
  if( !InRange(x,z) ) return 0;
  const AIThreat &expE=_map->GetExposureEnemy(x,z);
  return EXPOSURE_COEF * expE.Avg();
}

float AICenter::GetExposureOptimistic(int x, int z) const
{
  if (!_map) return 0;
  if( !InRange(x,z) ) return 0;
  const AIThreat &expE=_map->GetExposureEnemy(x,z);
  return EXPOSURE_COEF * expE.Avg();
}

float AICenter::GetExposurePessimistic(Vector3Par pos) const
{
  return GetExposurePessimistic
  (
    toIntFloor(pos.X()*InvLandGrid),toIntFloor(pos.Z()*InvLandGrid)
  );
}
float AICenter::GetExposureOptimistic(Vector3Par pos) const
{
  return GetExposureOptimistic
  (
    toIntFloor(pos.X()*InvLandGrid),toIntFloor(pos.Z()*InvLandGrid)
  );
}

RString AICenter::GetDebugName() const
{
  switch (_side)
  {
    case TEast:
      return "O"; // OPFOR
    case TWest:
      return "B"; // BLUFOR
    case TGuerrila:
      return "R"; // Resistance
    case TCivilian:
      return "C"; // Civilian
    case TLogic:
      return "L"; // Logic
    default:
      RptF("Wrong side %d", safe_cast<int>(_side));
      return "<No side>";
  }
}

void ReportGroup(const AIGroup *grp);

bool AICenter::AssertValid() const
{
  bool result = true;
  for (int i=0; i<NGroups(); i++)
  {
    AIGroup *grp = GetGroup(i);
    if (!grp) continue;
    if (grp->GetCenter() == NULL)
    {
      ReportGroup(grp);
      RptF(" - group with no center");
      result = false;
    }
    else if (grp->GetCenter() != this)
    {
      ReportGroup(grp);
      RptF(" - group from another center");
      result = false;
    }
    if (!grp->AssertValid()) result = false;
  }
  return result;
}

void AICenter::Dump(int indent) const
{
}

ArcadeTemplate CurrentTemplate;

int GetTemplateSeed()
{
  return CurrentTemplate.randomSeed;
}

RString CurrentCampaign;
RString CurrentBattle;
RString CurrentMission;
RString CurrentWorld;
RString CurrentFile;
