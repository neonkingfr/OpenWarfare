// AI - implementation of Units

#include "../wpch.hpp"
#include "ai.hpp"
#include "aiRadio.hpp"
#include "../global.hpp"
#include "../scene.hpp"
#include "../person.hpp"
#include "../diagModes.hpp"

#include "../world.hpp"
#include "../landscape.hpp"
//#include "loadStream.hpp"
#include "../paramArchiveExt.hpp"

#include <El/Common/randomGen.hpp>
#include <El/Common/perfLog.hpp>
#include <El/Common/perfProf.hpp>

#include <El/Evaluator/express.hpp>

//#include "strIncl.hpp"
#include "../stringtableExt.hpp"

#include "../roads.hpp"
#include "operMap.hpp"

#include "../house.hpp"
#include "../Network/network.hpp"
#include "../move_actions.hpp"

#include "../UI/uiActions.hpp"
#include "../UI/chat.hpp"

#include <El/Enum/enumNames.hpp>

#include <El/PreprocC/preprocC.hpp>

#include "../Protect/selectProtection.h"

#include <Es/Algorithms/qsort.hpp>
#include <Es/Algorithms/aStar.hpp>

#include "fsmScripted.hpp"
#include "../fsmEntity.hpp"
#include "../gameStateExt.hpp"

#if _ENABLE_CONVERSATION
#include "../KBExt/kbExt.hpp"
#endif

#if 0 //_PROFILE
#pragma optimize( "", off )
#endif

///////////////////////////////////////////////////////////////////////////////
// Parameters

#include "aiDefs.hpp"

// speed in nodes
#define SPEED_COEF  6.0F

// maximal length for operative path planning
#define MAX_OPER_PATH (6 * LandGrid)

/** dir is not used for strategic planning */
// A* algoritm for searching the best path
int directions20[20][3] =
{
  // dx,dz,dir
  {0,-1,0*16},
  {-1,-2,1*16},
  {-1,-1,2*16},
  {-2,-1,3*16},
  {-1,0,4*16},
  {-2,1,5*16},
  {-1,1,6*16},
  {-1,2,7*16},
  {0,1,8*16},
  {1,2,9*16},
  {1,1,10*16},
  {2,1,11*16},
  {1,0,12*16},
  {2,-1,13*16},
  {1,-1,14*16},
  {1,-2,15*16},
  {0,-2,0*16}, // directions 16..19 not used for operative planning
  {-2,0,4*16},
  {0,2,8*16},
  {2,0,12*16}
};

// exposures
#define VALID_EXPOSURE_CHANGE     10.0F

#define DIAG_PLANNING             0

///////////////////////////////////////////////////////////////////////////////
// class AILocker

// avoid inlined construct/destruct
AILocker::AILocker()
: _tempLocked(false),
_lockedSoldier(false),
_lockedBeg(VZero),
_lockedRadius(0),
_lockedSize(0),
_lastMovementTime(TIME_MIN)
{

} 

AILocker::~AILocker() {}

void AILocker::LockItem(OperMapIndex x, OperMapIndex z, bool soldier)
{
  if (x < 0 || x >= OperItemRange * LandRange || z < 0 || z >= OperItemRange * LandRange)
    return;

  SPLIT_INDICES(x, z, x0, z0, x1, z1);

  Ref<LockField> fld;
  for (int i=0; i<_fields.Size(); i++)
    if (_fields[i]->X() == x0 && _fields[i]->Z() == z0)
    {
      fld = _fields[i];
      break;
    }
  if (fld == NULL)
  // note: field may be locked twice with the same locker
  {
    fld = GLOB_LAND->LockingCache()->GetField(x0, z0, true);
    _fields.Add(fld);
  }
  fld->Lock(x1, z1, soldier, true);
}

void AILocker::UnlockItem(OperMapIndex x, OperMapIndex z, bool soldier)
{
  if (x < 0 || x >= OperItemRange * LandRange || z < 0 || z >= OperItemRange * LandRange)
    return;

  SPLIT_INDICES(x, z, x0, z0, x1, z1);

  LockField *fld = NULL;
  for (int i=0; i<_fields.Size(); i++)
    if (_fields[i]->X() == x0 && _fields[i]->Z() == z0)
    {
      fld = _fields[i];
      break;
    }
  if (fld == NULL)
  {
    Fail("Cannot unlock item that was not locked.");
    return;
  }
  fld->Lock(x1, z1, soldier, false);
}

const float ManLockRadius = 1.0;

void AILocker::Lock(Vector3Val pos, float radius, bool soldier, bool canWait, float size)
{
  bool waiting = canWait && Glob.time.Diff(_lastMovementTime)<20.0f; // 20 seconds is quite a long time inga,e
  // if already locked, decide if unlock / lock or keep current lock based on the position distance
  if (_tempLocked)
  {
    if (_lockedWaiting==waiting)
    {
      // avoid too frequent locking - do not lock when change is much less than OperItemGrid
      float dist2 = (pos - _lockedBeg).SquareSizeXZ();
      if (dist2 < Square(OperItemGrid * 0.4f)) return;
      _lastMovementTime = Glob.time;
    }
    Unlock();
  }
  Assert(!_tempLocked);

  float allRadius = soldier ? ManLockRadius : floatMax(4, radius * 0.6);

  // protect only write, called by the main thread only
  ScopeLockSection lock(_criticalSection);

  _lockedBeg = pos;
  _lockedRadius = radius;
  _lockedSize = size;
  _lockedSoldier = soldier;
  _lockedWaiting = waiting;

  // lock larger circle for vehicles only
  LockPosition(_lockedBeg, _lockedRadius, _lockedSoldier, _lockedWaiting, _lockedSize);
  // lock smaller circle for both soldiers and vehicles
  LockPositionAll(_lockedBeg, allRadius, _lockedSoldier, _lockedWaiting, _lockedSize);
  _tempLocked = true;
}

void AILocker::Move()
{
  _lastMovementTime = Glob.time;
}
void AILocker::Unlock(SavedState *state)
{
  if (_tempLocked)
  {
    float allRadius = _lockedSoldier ? ManLockRadius : floatMax(4, _lockedRadius * 0.6);

    // protect only write, called by the main thread only
    ScopeLockSection lock(_criticalSection);

    // unlock what was locked in both layers
    UnlockPositionAll(_lockedBeg, allRadius, _lockedWaiting, state);
    UnlockPosition(_lockedBeg, _lockedRadius, _lockedSoldier, _lockedWaiting, state);
    _tempLocked = false;
  }
}

void AILocker::Relock(SavedState &state)
{
  Assert(!_tempLocked);
  if (!_tempLocked)
  {
    float allRadius = _lockedSoldier ? ManLockRadius : floatMax(4, _lockedRadius * 0.6);

    // protect only write, called by the main thread only
    ScopeLockSection lock(_criticalSection);

    // lock larger circle for vehicles only
    RelockPosition(_lockedBeg, _lockedRadius, _lockedSoldier, _lockedWaiting, _lockedSize, state);
    // lock smaller circle for both soldiers and vehicles
    RelockPositionAll(_lockedBeg, allRadius, _lockedWaiting, state);
    _tempLocked = true;
  }
}

inline int MaxLockRange() {return toInt(50 * InvOperItemGrid);}

int AILocker::IsLocking(OperMapIndex x, OperMapIndex z, bool soldier) const
{
  // protect all, called from worker threads
  ScopeLockSection lock(_criticalSection);

  if (!_tempLocked) return 0;

  int locks = 0;
  
  OperMapIndex xBeg(toIntFloor(_lockedBeg.X() * InvOperItemGrid));
  OperMapIndex zBeg(toIntFloor(_lockedBeg.Z() * InvOperItemGrid));

  // locking by LockMaskAll
  if (x == xBeg && z == zBeg)
  {
    locks = 0x10;
  }
  else
  {
    float radius = _lockedSoldier ? ManLockRadius : floatMax(4, _lockedRadius * 0.6);

    int lockRange = toIntCeil(radius * InvOperItemGrid);
    if (lockRange < 0) lockRange = 0;
    if (!(lockRange<MaxLockRange())) {lockRange = MaxLockRange();}

    if (x >= xBeg - lockRange && x <= xBeg + lockRange && z >= zBeg - lockRange && z <= zBeg + lockRange)
    {
      float xr = (x + 0.5f) * OperItemGrid - _lockedBeg.X();
      float zr = (z + 0.5f) * OperItemGrid - _lockedBeg.Z();
      if (Square(xr) + Square(zr) < Square(radius)) locks = 0x10;
    }
  }

  // locking by LockMaskVeh (soldier can ignore)
  if (!soldier)
  {
    if (x == xBeg && z == zBeg)
    {
      locks |= 0x01;
    }
    else
    {
      float radius = _lockedRadius;

      int lockRange = toIntCeil(radius * InvOperItemGrid);
      if (lockRange < 0) lockRange = 0;
      if (!(lockRange<MaxLockRange())) {lockRange = MaxLockRange();}

      if (x >= xBeg - lockRange && x <= xBeg + lockRange && z >= zBeg - lockRange && z <= zBeg + lockRange)
      {
        float xr = (x + 0.5f) * OperItemGrid - _lockedBeg.X();
        float zr = (z + 0.5f) * OperItemGrid - _lockedBeg.Z();
        if (Square(xr) + Square(zr) < Square(radius)) locks |= 0x01;
      }
    }
  }
  
  return locks;
}

int AILocker::IsLocking(const RoadLink *road, bool soldier) const
{
  // protect all, called from worker threads
  ScopeLockSection lock(_criticalSection);

  if (!_tempLocked) return 0;

  int locks = 0;
  // locking by LockMaskAll
  for (int i=0; i<_roadsVehicles.Size(); i++)
  {
    if (_roadsVehicles[i] == road)
    {
      locks = 0x10;
      break;
    }
  }

  // locking by LockMaskVeh (soldier can ignore)
  if (!soldier)
  {
    for (int i=0; i<_roadsSoldiers.Size(); i++)
    {
      if (_roadsSoldiers[i] == road)
      {
        locks |= 0x01;
        break;
      }
    }
  }

  return locks;
}

int AILocker::IsLocking(const Object *house, int index) const
{
  // protect all, called from worker threads
  ScopeLockSection lock(_criticalSection);

  if (!_tempLocked) return 0;

  for (int i=0; i<_buildings.Size(); i++)
  {
    const BuildingLockInfo &info = _buildings[i];
    if (info.house == house && info.index == index) return 1;
  }
  return 0;
}

void AILocker::LockPosition( Vector3Val pos, float radius, bool soldier, bool waiting, float size)
{
  int lockRange = toIntCeil(radius * InvOperItemGrid);
  if (lockRange < 0) lockRange = 0;
  if (!(lockRange<MaxLockRange())) {lockRange = MaxLockRange();ErrF("Bad lock radius %g",radius);}

  // lock the fields
  {
    OperMapIndex x(toIntFloor(pos.X() * InvOperItemGrid));
    OperMapIndex z(toIntFloor(pos.Z() * InvOperItemGrid));
    for (OperMapIndex xx(x-lockRange); xx<=x+lockRange; xx++)
      for (OperMapIndex zz(z-lockRange); zz<=z+lockRange; zz++)
      {
        float xr=(xx+0.5f)*OperItemGrid-pos.X();
        float zr=(zz+0.5f)*OperItemGrid-pos.Z();
        if ( xr*xr+zr*zr<radius*radius || xx==x && zz==z )
        {
          LockItem(xx, zz, true);
          //LogF("DoLock  %d %d,%d",soldier, xx, zz);
        }
      }
  }

  // lock the roads
  {
    Assert( GLandscape->GetRoadNet() );
    LandIndex x(toIntFloor(pos.X() * InvLandGrid));
    LandIndex z(toIntFloor(pos.Z() * InvLandGrid));
    // lock road item if vehicle is inside "boundingSphere" + size
    for (LandIndex zz(z-1); zz<=z+1; zz++)
      for (LandIndex xx(x-1); xx<=x+1; xx++)
      {
        if (!InRange(xx, zz)) continue;
        const RoadList &roadList = GLandscape->GetRoadNet()->GetRoadList(xx, zz);
        int i;
        for (i=0; i<roadList.Size(); i++)
        {
          RoadLink *item = roadList[i];
          if (item->IsInside(pos, size, size))
          {
            item->Lock(true,waiting);
            _roadsSoldiers.Add(item);
          }
        }
      }
  }

  if (soldier)
  {
    // this is not clear - pos is AimingPosition, we need position on surface
    Vector3 realPos = pos - Vector3(0, 0, 0); // updated based on testing

    // lock position in house
    int xMin, xMax, zMin, zMax;
    ObjRadiusRectangle(xMin, xMax, zMin, zMax, pos, pos, 50);
    for (int x=xMin; x<=xMax; x++) for(int z=zMin; z<=zMax; z++)
    {
      const ObjectListUsed &list = GLandscape->UseObjects(x,z);
      int n = list.Size();
      for (int i=0; i<n; i++)
      {
        Object *obj = list[i];

        IPaths *building = unconst_cast(obj->GetIPaths());
        if (!building) continue;
        for (int j=0; j<building->NPos(); j++)
        {
          Vector3 dest = building->GetPosition(building->GetPos(j));
          float dist2 = dest.Distance2(realPos);
          if (dist2 <= Square(size))
          {
            building->Lock(j);
            int index = _buildings.Add();
            _buildings[index].house = unconst_cast(building->GetObject());
            _buildings[index].index = j;
          }
        }
      }
    }
  }
}

void AILocker::RelockPosition( Vector3Val pos, float radius, bool soldier, bool waiting, float size, SavedState &state)
{
  int lockRange = toIntCeil(radius * InvOperItemGrid);
  if (lockRange < 0) lockRange = 0;
  if (!(lockRange<MaxLockRange())) {lockRange = MaxLockRange();ErrF("Bad lock radius %g",radius);}

  OperMapIndex x(toIntFloor(pos.X() * InvOperItemGrid));
  OperMapIndex z(toIntFloor(pos.Z() * InvOperItemGrid));
  for (OperMapIndex xx(x-lockRange); xx<=x+lockRange; xx++)
    for(OperMapIndex zz(z-lockRange); zz<=z+lockRange; zz++)
    {
      float xr=(xx+0.5f)*OperItemGrid-pos.X();
      float zr=(zz+0.5f)*OperItemGrid-pos.Z();
      if ( xr*xr+zr*zr<radius*radius || xx==x && zz==z )
      {
        LockItem(xx, zz, true);
        //LogF("DoLock  %d %d,%d",soldier, xx, zz);
      }
    }

  _roadsSoldiers.Move(state._roadsSoldiers);

  // consider: we might avoid allocation, and move the array content instead
  // no need to search for roads - relock what was locked before
  for (int i=0; i<_roadsSoldiers.Size(); i++)
  {
    _roadsSoldiers[i]->Lock(true,waiting);
  }

  if (soldier)
  {
    _buildings.Move(state._buildings);

    // unlock position in house
    for (int i=0; i<_buildings.Size(); i++)
    {
      if (_buildings[i].house)
      {
        IPaths *building = unconst_cast(_buildings[i].house->GetIPaths());
        building->Lock(_buildings[i].index);
      }
    }
  }
}



void AILocker::UnlockPosition( Vector3Val pos, float radius, bool soldier, bool waiting, SavedState *state)
{
  int lockRange = toIntCeil(radius * InvOperItemGrid);
  if (lockRange < 0) lockRange = 0;
  if (!(lockRange<MaxLockRange())) {lockRange = MaxLockRange();ErrF("Bad lock radius %g",radius);}

  OperMapIndex x(toIntFloor(pos.X() * InvOperItemGrid));
  OperMapIndex z(toIntFloor(pos.Z() * InvOperItemGrid));
//Log("%s:Unlocking %d, %d", (const char *)GetDebugName(), x, z);
  for (OperMapIndex xx(x-lockRange); xx<=x+lockRange; xx++)
    for (OperMapIndex zz(z-lockRange); zz<=z+lockRange; zz++)
    {
      float xr=(xx+0.5f)*OperItemGrid-pos.X();
      float zr=(zz+0.5f)*OperItemGrid-pos.Z();
      if( xr*xr+zr*zr<radius*radius || xx==x && zz==z )
      {
        UnlockItem(xx, zz, true);
        //LogF("UnLock  %d %d,%d",soldier, xx, zz);
      }
    }

  int n = _roadsSoldiers.Size();
  for (int i=0; i<n; i++)
    if (_roadsSoldiers[i]) _roadsSoldiers[i]->Unlock(true,waiting);

  if (soldier)
  {
    if (_buildings.Size() > 0 && !_buildings.Data())
    {
      ErrF
      (
        "Error: AILocker::_buildings corrupted - data NULL, size %d when unlocking position [%.0f, %.0f], radius %.0f, soldier %s",
        _buildings.Size(), pos.X(), pos.Z(), radius, soldier ? "true" : "false"
      );
    }
    // unlock position in house
    for (int i=0; i<_buildings.Size(); i++)
    {
      if (_buildings[i].house)
      {
        IPaths *building = unconst_cast(_buildings[i].house->GetIPaths());
        building->Unlock(_buildings[i].index);
      }
    }
  }

  _fields.Resize(0);
  // TODO: some smart deallocation scheme
  // avoid dealocation
  //_fields.Clear();
  //_roads.Clear();
  if (state)
  {
    state->_roadsSoldiers.Move(_roadsSoldiers);
    state->_buildings.Move(_buildings);
  }
  else
  {
    _roadsSoldiers.Resize(0);
    _buildings.Resize(0);
  }
}

void AILocker::LockPositionAll( Vector3Val pos, float radius, bool soldier, bool waiting, float size)
{
  int lockRange = toIntCeil(radius * InvOperItemGrid);
  if (lockRange < 0) lockRange = 0;
  if (!(lockRange<MaxLockRange())) {lockRange = MaxLockRange();ErrF("Bad lock radius %g",radius);}

  {
    OperMapIndex x(toIntFloor(pos.X() * InvOperItemGrid));
    OperMapIndex z(toIntFloor(pos.Z() * InvOperItemGrid));
    for (OperMapIndex xx(x-lockRange); xx<=x+lockRange; xx++)
      for (OperMapIndex zz(z-lockRange); zz<=z+lockRange; zz++)
      {
        float xr=(xx+0.5f)*OperItemGrid-pos.X();
        float zr=(zz+0.5f)*OperItemGrid-pos.Z();
        if ( xr*xr+zr*zr<radius*radius || xx==x && zz==z )
        {
          //LogF("DoLockM %d %d,%d",false, xx, zz);
          LockItem(xx, zz, false);
        }
      }
  }

  if (!soldier) // soldier does not lock road for soldiers
  {
    Assert( GLandscape->GetRoadNet() );
    LandIndex x(toIntFloor(pos.X() * InvLandGrid));
    LandIndex z(toIntFloor(pos.Z() * InvLandGrid));
    // lock road item if vehicle is inside "boundingSphere" + size
    for (LandIndex zz(z-1); zz<=z+1; zz++)
      for (LandIndex xx(x-1); xx<=x+1; xx++)
      {
        if (!InRange(xx, zz)) continue;
        const RoadList &roadList = GLandscape->GetRoadNet()->GetRoadList(xx, zz);
        int i;
        for (i=0; i<roadList.Size(); i++)
        {
          RoadLink *item = roadList[i];
          if (item->IsInside(pos, size, size))
          {
            item->Lock(false,waiting);
            _roadsVehicles.Add(item);
          }
        }
      }
  }
}

void AILocker::RelockPositionAll( Vector3Val pos, float radius, bool waiting, SavedState &state)
{
  int lockRange = toIntCeil(radius * InvOperItemGrid);
  if (lockRange < 0) lockRange = 0;
  if (!(lockRange<MaxLockRange())) {lockRange = MaxLockRange();ErrF("Bad lock radius %g",radius);}

  OperMapIndex x(toIntFloor(pos.X() * InvOperItemGrid));
  OperMapIndex z(toIntFloor(pos.Z() * InvOperItemGrid));
  for (OperMapIndex xx(x-lockRange); xx<=x+lockRange; xx++)
    for (OperMapIndex zz(z-lockRange); zz<=z+lockRange; zz++)
    {
      float xr=(xx+0.5f)*OperItemGrid-pos.X();
      float zr=(zz+0.5f)*OperItemGrid-pos.Z();
      if ( xr*xr+zr*zr<radius*radius || xx==x && zz==z )
      {
        //LogF("DoLockM %d %d,%d",false, xx, zz);
        LockItem(xx, zz, false);
      }
    }

  // avoid allocation, and move the array content instead
  _roadsVehicles.Move(state._roadsVehicles);

  // no need to search for roads - relock what was locked before
  for (int i=0; i<_roadsVehicles.Size(); i++)
  {
    _roadsVehicles[i]->Lock(false,waiting);
  }
}

void AILocker::UnlockPositionAll( Vector3Val pos, float radius, bool waiting, SavedState *state)
{
  int lockRange = toIntCeil(radius * InvOperItemGrid);
  if (lockRange < 0) lockRange = 0;
  if (!(lockRange<MaxLockRange())) {lockRange = MaxLockRange();ErrF("Bad lock radius %g",radius);}

  OperMapIndex x(toIntFloor(pos.X() * InvOperItemGrid));
  OperMapIndex z(toIntFloor(pos.Z() * InvOperItemGrid));
  for (OperMapIndex xx(x-lockRange); xx<=x+lockRange; xx++)
    for (OperMapIndex zz(z-lockRange); zz<=z+lockRange; zz++)
    {
      float xr=(xx+0.5f)*OperItemGrid-pos.X();
      float zr=(zz+0.5f)*OperItemGrid-pos.Z();
      if ( xr*xr+zr*zr<radius*radius || xx==x && zz==z )
      {
        //LogF("UnLockM %d %d,%d",false, xx, zz);
        UnlockItem(xx, zz, false);
      }
    }

  int n = _roadsVehicles.Size();
  for (int i=0; i<n; i++)
    if (_roadsVehicles[i]) _roadsVehicles[i]->Unlock(false,waiting);

  // TODO: some smart deallocation scheme
  // avoid dealocation
  //_roads.Clear();
  if (state)
  {
    state->_roadsVehicles.Move(_roadsVehicles);
  }
  else
  {
    _roadsVehicles.Resize(0);
  }
}

CheckAmmoInfo::CheckAmmoInfo()
{
  muzzle1 = NULL;
  muzzle2 = NULL;
  muzzleHG = NULL;
  slots1 = 0;
  slots2 = 0;
  slots3 = 0;
  slotsHG = 0;
  hasSlots1 = 0;
  hasSlots2 = 0;
  hasSlots3 = 0;
  hasSlotsHG = 0;
  hasAmmo1 = 0;
  hasAmmo2 = 0;
  hasAmmo3 = 0;
  hasAmmoHG = 0;
}

///////////////////////////////////////////////////////////////////////////////
// class AIUnit

const Vector3 VUndefined(0, 1e9, 0);

static float AIUnitGetFieldCost(int x, int z, bool road, bool includeGradient, float gradient, void *param)
{
  if (!param) return SET_UNACCESSIBLE;
  AIBrain *unit = (AIBrain *)param;
  switch (unit->GetPlanningMode())
  {
  case AIBrain::DoNotPlan:
  case AIBrain::LeaderDirect:
  default:
    {
      Fail("planning mode");
      return SET_UNACCESSIBLE;
    }
  case AIUnit::LeaderPlanned:
    {
      AISubgroup *subgrp = unit->GetUnit() ? unit->GetUnit()->GetSubgroup() : NULL;
      if (subgrp) return subgrp->GetFieldCost(x, z, road, includeGradient, gradient);
      // continue
    }
  case AIBrain::FormationPlanned:
  case AIBrain::VehiclePlanned:
    {
      // base cost
      EntityAI *veh = unit->GetVehicle();
      GeographyInfo geogr = GLandscape->GetGeography(x, z);
      geogr.u.road = road; // if we are searching on road is now given externally
      float cost = veh->GetBaseCost(geogr, false, includeGradient) * veh->GetFieldCost(geogr);
      if (!includeGradient) cost *= veh->GetGradientPenalty(gradient); // exact gradient is used
      cost *= LandGrid; // cost = time for distance == LandGrid
      saturateMin(cost, SET_UNACCESSIBLE); // avoid inf. in cost

      // exposure
      float exposure = veh->CalculateExposure(x, z); // damage (in $) per time cost
      return cost * (1.0 + COEF_EXPOSURE * exposure);
    }
  }
}

DEFINE_ENUM(BehaviourFSMRole, BFSM, BEHAVIOUR_FSM_ENUM)
DEFINE_ENUM(ReactionFSMRole, RFSM, REACTION_FSM_ENUM)

#include <Es/Memory/normalNew.hpp>

class AIBrainFSMScripted : public FSMScriptedTyped<Person>
{
  typedef FSMScriptedTyped<Person> base;

protected:
  DECL_SERIALIZE_TYPE_INFO(AIBrainFSMScripted, FSM)

public:
  AIBrainFSMScripted(const FSMScriptedType *type, GameDataNamespace *globals, GameVarSpace *parentVars) : base(type, globals, parentVars) {}
  AIBrainFSMScripted(ForSerializationOnly dummy) : base(dummy) {}

  virtual void ApplyContext(GameState &state, void *context)
  {
    Person *person = reinterpret_cast<Person *>(context);
    state.VarSetLocal("_this", GameValueExt(person), true);
  }

  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

DEFINE_FAST_ALLOCATOR(AIBrainFSMScripted)
DEFINE_SERIALIZE_TYPE_INFO(AIBrainFSMScripted, FSM)

FSM *AIBrain::CreateFSM(const EntityType *type, RString name)
{
  if (name.GetLength() == 0) return NULL;
  FSM *ret = NULL;
  if ((Pars >> "CfgFSMs").FindEntry(name))
  {
    FSMEntityType *fsmType = FSMEntityTypes.New(FSMEntityTypeName(type, name));
    if (!fsmType) return NULL;
    ret = new FSMEntity(fsmType);
  }
  else
  {
    FSMScriptedType *fsmType = FSMScriptedTypes.New(name);
    if (!fsmType) return NULL;
    ret = new AIBrainFSMScripted(fsmType, GWorld->GetMissionNamespace(), NULL); // mission namespace
  }
  if (_fsmAutoDebug>=2)
  {
    ret->Debug();
  }
  return ret;
}

DEFINE_ENUM(SuppressState, Suppress, SUPPRESS_ENUM)

#pragma warning(disable:4355)

AIBrain::AIBrain(Person *vehicle)
: _person(vehicle), _map(true), // _map is used for path planning, create list of houses
  _loadIdentityDelayed(true)
#if _ENABLE_AI
, _planner(CreateAIPathPlanner(AIUnitGetFieldCost, this))
#endif
{
  _lifeState = LifeStateAlive;
  _disabledAI = 0;
#if !_ENABLE_IDENTITIES
  _ability = 1.0;
#endif
#if DEBUG_FSM
  _fsmAutoDebug = 0;
#endif
  _combatModeMajor = CMAware;
  _semaphore = SemaphoreYellow;
  _dangerUntil = Glob.time - 60.0f;
  _state = Wait;
  _planningMode = DoNotPlan;
  _mode = Normal;
  _iter = 0;
  _completedReceived = false;
  _getInAllowed = true;
  _getInOrdered = false;
  _roleIndex = -1;
  _maxCostToCover=0;
  _maxDelayByCover=0;
  _enemyDirL = VZero;
  _enemyDirR = VZero;
  _delay = Time(0);

  _expPosition = PlanPosition(VZero,false);
  _from = VZero;
  _fromBusy = false;
  _toRoad = false;

  _wantedPositionWanted = PlanPosition(VUndefined,false);
  _planningModeWanted = DoNotPlan;
  _forceReplanWanted = false;

  _remoteControlling = false;

  _forceSpeed = -1; //no forceSpeed as default

#if _VBS3
  _allowSpeech = true;
#endif
  _wantedPosition = PlanPosition(VUndefined,false);
  _plannedPosition = PlanPosition(VUndefined,false);
  // by default path finding is enabled
  _planParametersSet = true;
  _wantedPrecision = 0;

  ClearStrategicPlan();

  SetPathCosts(0, 0);
  _pathId = 0;

  _nearestEnemyDist2 = 100;

  _captive = 0;

  _watchDirHead = VZero;
  _watchDir = VZero;
  _watchPos = VZero;
  _glancePos = VZero;
  _watchMode = WMNo;

  SetSpeaker(RString(), PITCH_DEFAULT);

#if _ENABLE_DIRECT_MESSAGES
  _selectedChannel = SCAuto;
  _gestureEnabled = GEAuto;
  _voiceEnabled = VEAuto;
#endif

#if _ENABLE_CONVERSATION
  _speaking = 0;
  _listening = 0;
#endif

  // Create long-term FSMs
  const EntityType *type = vehicle ? vehicle->GetType() : NULL;
  if (type)
  {
    const ParamClass *cls = type->GetParamEntry();
    if (cls)
    {
      for (int i=0; i<NBehaviourFSMRole; i++)
      {
        RString name = RString("fsm") + FindEnumName((BehaviourFSMRole)i);
        _behaviourFSMs[i] = CreateFSM(type, (*cls) >> name);
      }
    }
  }
}

void AIBrain::SetPathCosts(float costReplan, float costForceReplan)
{
  _path.SetReplanCost(costReplan);
  _path.SetForceReplanCost(costForceReplan);
  _path.ResetMinCost();
}

void AIBrain::SetPerson(Person *vehicle)
{
  _person = vehicle;

  // re-create long-term FSMs
  for (int i=0; i<NBehaviourFSMRole; i++)
    _behaviourFSMs[i] = NULL;

  const EntityType *type = vehicle ? vehicle->GetType() : NULL;
  if (type)
  {
    const ParamClass *cls = type->GetParamEntry();
    if (cls)
    {
      for (int i=0; i<NBehaviourFSMRole; i++)
      {
        RString name = RString("fsm") + FindEnumName((BehaviourFSMRole)i);
        _behaviourFSMs[i] = CreateFSM(type, (*cls) >> name);
      }
    }
  }
}

void AIBrain::AddObjectIDHistory(int objectId)
{
  _objectIdHistory.Add(objectId);
}

EntityAIFull *AIBrain::GetVehicle() const
{
  EntityAIFull *veh = _inVehicle;
  if (veh) return veh;
  else return _person;
}

void AIBrain::UnassignVehicle()
{
  if (_vehicleAssigned)
  {
    _vehicleAssigned->RemoveAssignement(this);
    _vehicleAssigned = NULL;
  }
}

bool AIBrain::AssignAsDriver(Transport *veh)
{
  if (!veh->GetType()->HasDriver()) return false;

  if (veh && veh == _vehicleAssigned && veh->GetDriverAssigned() == this)
    return true;  // already assigned as driver

  if (_vehicleAssigned) // unassign
  {
    _vehicleAssigned->RemoveAssignement(this);
    _vehicleAssigned = NULL;
  }

  if (veh)
  {
    AIBrain *oldDriver = veh->GetDriverAssigned();
    if (oldDriver)  // unassign
    {
      if (veh->Driver() && !veh->Driver()->IsDamageDestroyed())
      { // cannot assign - driver is inside
        Assert(veh->DriverBrain() == oldDriver);
        return false;
      }
      veh->RemoveAssignement(oldDriver);
      oldDriver->_vehicleAssigned = NULL;
    }
    veh->AssignDriver(this);
    _vehicleAssigned = veh;
  }
  return true;
}

bool AIBrain::AssignAsGunner(Transport *veh)
{
  if (!veh->GetType()->HasGunner()) return false;

  if (veh == _vehicleAssigned && veh->GetGunnerAssigned() == this)
    return true;  // already assigned as gunner

  if (_vehicleAssigned) // unassign
  {
    _vehicleAssigned->RemoveAssignement(this);
    _vehicleAssigned = NULL;
  }

  if (veh)
  {
    AIBrain *oldGunner = veh->GetGunnerAssigned();
    if (oldGunner)  // unassign
    {
      if (veh->Gunner() && !veh->Gunner()->IsDamageDestroyed())
      { // cannot assign - driver is inside
        Assert(veh->GunnerBrain() == oldGunner);
        return false;
      }
      veh->RemoveAssignement(oldGunner);
      oldGunner->_vehicleAssigned = NULL;
    }
    veh->AssignGunner(this);
    _vehicleAssigned = veh;
  }
  return true;
}

bool AIBrain::AssignAsCommander(Transport *veh)
{
  if (veh && !veh->GetType()->HasObserver()) return false;

  if (veh && veh == _vehicleAssigned && veh->GetCommanderAssigned() == this)
    return true;  // already assigned as commander

  if (_vehicleAssigned) // unassign
  {
    _vehicleAssigned->RemoveAssignement(this);
    _vehicleAssigned = NULL;
  }

  if (veh)
  {
    AIBrain *oldCommander = veh->GetCommanderAssigned();
    if (oldCommander) // unassign
    {
      if (veh->Observer() && !veh->Observer()->IsDamageDestroyed())
      { // cannot assign - driver is inside
        Assert(veh->ObserverBrain() == oldCommander);
        return false;
      }
      veh->RemoveAssignement(oldCommander);
      oldCommander->_vehicleAssigned = NULL;
    }
    veh->AssignCommander(this);
    _vehicleAssigned = veh;
  }
  return true;
}

bool AIBrain::AssignAsCargo(Transport *veh, int index, bool reassignWhenConflict)
{
  // FIX: because of multiple turrets, use positive checking of presence in cargo
  
  int n = veh->GetManCargo().Size(); // number of cargo slots
  if (veh && veh == _vehicleAssigned)
  {
    if (index >= 0)
    {
      if (veh->GetCargoAssigned(index) == this) return true; // already assigned to this cargo position
    }
    else
    {
      // any cargo position is good
      for (int i=0; i<n; i++)
      {
        if (veh->GetCargoAssigned(i) == this) return true; // already assigned to cargo
      }
    }
  }

  if (_vehicleAssigned) // unassign
  {
    _vehicleAssigned->RemoveAssignement(this);
    _vehicleAssigned = NULL;
  }

  if (veh)
  {
    if (index >= 0)
    {
      // check the cargo position
      if (index >= n) return false;
      AIBrain *oldCargo = veh->GetCargoAssigned(index);
      if (oldCargo)
      {
        if (oldCargo->GetVehicle() == veh && !oldCargo->GetPerson()->IsDamageDestroyed())
        {
          Assert(oldCargo->IsInCargo());
          return false; // position already occupied
        }
        Assert(oldCargo->IsFreeSoldier() || oldCargo->GetPerson()->IsDamageDestroyed());
        // unassign
        veh->RemoveAssignement(oldCargo);
        oldCargo->_vehicleAssigned = NULL;
        // try to reassign if possible
        // empty cargo positions
        if (reassignWhenConflict)
        {
          for (int i=0; i<n; i++)
          {
            if (i == index) continue;
            if (!veh->GetCargoAssigned(i))
            {
              veh->AssignCargo(i, oldCargo);
              oldCargo->_vehicleAssigned = veh;
              reassignWhenConflict = false;
              break;
            }
          }
        }
        // cargo positions with the dead body
        if (reassignWhenConflict)
        {
          for (int i=0; i<n; i++)
          {
            if (i == index) continue;
            AIBrain *deadCargo = veh->GetCargoAssigned(i);
            if (deadCargo && deadCargo->GetPerson()->IsDamageDestroyed())
            {
              veh->RemoveAssignement(deadCargo);
              deadCargo->_vehicleAssigned = NULL;

              veh->AssignCargo(i, oldCargo);
              oldCargo->_vehicleAssigned = veh;
              reassignWhenConflict = false;
              break;
            }
          }
        }
      }
    }
    else
    {
      // find the cargo position
      // first check for empty cargo positions
      for (int i=0; i<n; i++)
      {
        AIBrain *oldCargo = veh->GetCargoAssigned(i);
        if (!oldCargo)
        {
          index = i;
          break;
        }
      }
      // if not found, search who to replace
      if (index < 0)
      {
        for (int i=0; i<n; i++)
        {
          AIBrain *oldCargo = veh->GetCargoAssigned(i);
          Assert(oldCargo);
          if (oldCargo->GetVehicle() == veh && !oldCargo->GetPerson()->IsDamageDestroyed())
          {
            Assert(oldCargo->IsInCargo());
            continue;
          }
          Assert(oldCargo->IsFreeSoldier() || oldCargo->GetPerson()->IsDamageDestroyed());
          // unassign
          veh->RemoveAssignement(oldCargo);
          oldCargo->_vehicleAssigned = NULL;
          index = i;
          break;
        }
      }
      if (index < 0) return false; // cannot assign - cargo is full
    }
    
    veh->AssignCargo(index, this);
    _vehicleAssigned = veh;
  }
  return true;
}

bool AIBrain::AssignToTurret(Transport *veh, Turret *turret)
{
  if (veh && veh == _vehicleAssigned && turret && turret->GetGunnerAssigned() == this)
    return true;  // already assigned to this turret

  if (_vehicleAssigned) // unassign
  {
    _vehicleAssigned->RemoveAssignement(this);
    _vehicleAssigned = NULL;
  }

  if (veh && turret)
  {
    AIBrain *oldCrew = turret->GetGunnerAssigned();
    if (oldCrew) // unassign
    {
      if (turret->Gunner() && !turret->Gunner()->IsDamageDestroyed())
      { // cannot assign - crew is inside
        Assert(turret->Gunner()->Brain() == oldCrew);
        return false;
      }
      veh->RemoveAssignement(oldCrew);
      oldCrew->_vehicleAssigned = NULL;
    }
    turret->AssignGunner(this);
    _vehicleAssigned = veh;
  }
  return true;
}

/*!
\patch 1.85 Date 9/18/2002 by Jirka
- Fixed: Allow captive be in vehicle with enemies
*/

static bool AvoidBeInWith(AIBrain *brain, Person *person)
{
  if (!person) return false;
  if (!brain->IsEnemy(person->GetTargetSide())) return false;
  AIBrain *unit = person->Brain();
  if (unit && unit->GetCaptive()) return false;
  return true;
}

/// Functor checking if there is some person in vehicle unit cannot be in with
class AvoidBeInWithFunc : public ITurretFunc
{
protected:
  AIBrain *_brain;

public:
  AvoidBeInWithFunc(AIBrain *brain) : _brain(brain) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false;
    return AvoidBeInWith(_brain, context._gunner);
  }
};

/// when getting out of the vehicle, Gunner (optics) camera is never used

void MoveToGetInPos(AIBrain *unit, Transport &veh, const Vector3& shift)
{
  GetInPoint pt = veh.GetUnitGetInPos(unit);

  Matrix4 trans;
  trans.SetUpAndDirection(VUp, pt._dir);

  Vector3 realShift = shift + Vector3(0,0,-0.4f);
  pt._pos += trans.Rotate(realShift) ;

#if _ENABLE_WALK_ON_GEOMETRY
  pt._pos[1] = GLandscape->RoadSurfaceYAboveWater(pt._pos+VUp*0.5f, Landscape::FilterPrimary());
#else
  pt._pos[1] = GLandscape->RoadSurfaceYAboveWater(pt._pos+VUp*0.5f);
#endif

  trans.SetPosition(pt._pos);
  unit->GetPerson()->Move(trans);
  unit->SetWantedPosition(PlanPosition(pt._pos,false), 0, AIUnit::DoNotPlan, true);

#if 0
  // adjust camera position
  if (unit == GWorld->FocusOn())
    GWorld->InitCameraPars();
#else
  GWorld->FocusOn();
#endif
}

bool AIBrain::ProcessGetIn(Transport &veh)
{
  //LogF("AIUnit::ProcessGetIn %s to %s", (const char *)GetDebugName(), (const char *)veh.GetDebugName());
  if (!IsLocal()) return false;
  if (!IsFreeSoldier()) return false;
  if (IsPlayer() && veh.GetLock() == LSLocked) return false;
  AIGroup *grp = GetGroup();
  if (veh.GetLock() == LSLocked)
  {
    if (grp)
    {
      if (grp->IsPlayerGroup()) return false;
    }
    else
    {
      if (IsPlayer()) return false;
    }
  }

  // avoid get in enemy vehicle
  if (AvoidBeInWith(this, veh.Driver()))
  {
    if (_vehicleAssigned == &veh)
    {
      UnassignVehicle();
      if (grp) grp->UnassignVehicle(&veh);
    }
    return false;
  }
  AvoidBeInWithFunc func(this);
  if (veh.ForEachTurret(func))
  {
    if (_vehicleAssigned == &veh)
    {
      UnassignVehicle();
      if (grp) grp->UnassignVehicle(&veh);
    }
    return false;
  }
  for (int i=0; i<veh.GetManCargo().Size(); i++)
  {
    Person *man = veh.GetManCargo()[i];
    if (AvoidBeInWith(this, man))
    {
      if (_vehicleAssigned == &veh)
      {
        UnassignVehicle();
        if (grp) grp->UnassignVehicle(&veh);
      }
      return false;
    }
  }

  if
  (
    IsPlayer() && (veh.GetLock() == LSUnlocked || grp && grp->Leader() == this || GWorld->GetMode() == GModeNetware)
  )
  {
    // can get into any vehicle - try to assign position
    if (!veh.QIsDriverIn() && veh.GetType()->HasDriver())
    {
      if (veh.QCanIGetIn(GetPerson()))
      {
        if (grp) grp->AddVehicle(&veh);
        AssignAsDriver(&veh);
        AllowGetIn(true);
        OrderGetIn(true);
      }
      else
      {
        // cannot get in as driver
        return false;
      }
    }
    else
    {
      Assert(veh.GetGroupAssigned()); // driver is in
      bool assigned = GetGroup() == veh.GetGroupAssigned();
      // if !assigned - can get in cargo only
      if (assigned && veh.GetType()->HasObserver() && !veh.QIsCommanderIn())
      {
        if (veh.QCanIGetInCommander(GetPerson()))
        {
          AssignAsCommander(&veh);
          AllowGetIn(true);
          OrderGetIn(true);
        }
        else
        {
          // cannot get in as driver
          return false;
        }
      }
      else if (assigned && veh.GetType()->HasGunner() && !veh.QIsGunnerIn())
      {
        if (veh.QCanIGetInGunner(GetPerson()))
        {
          AssignAsGunner(&veh);
          AllowGetIn(true);
          OrderGetIn(true);
        }
        else
        {
          // cannot get in as driver
          return false;
        }
      }
      else
      {
        int cargoIndex = veh.QCanIGetInCargo(GetPerson());
        if (cargoIndex >= 0)
        {
          AssignAsCargo(&veh, cargoIndex);
          AllowGetIn(true);
          OrderGetIn(true);
        }
        else
        {
          // cannot get in as cargo
          return false;
        }
      }
    }
  }

  if (VehicleAssigned() == &veh)
  {
    UIActionType pos = ATNone;
    Turret *turret = NULL;
    int cargoIndex = -1;
    RString getInAction;

    if (veh.GetDriverAssigned() == this)
    {
      pos = veh.GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)) ? ATGetInPilot : ATGetInDriver;
      getInAction = veh.Type()->GetDriverGetInAction();
    }
    else
    {
      TurretContext context;
      if (veh.FindAssignedTurret(this, context) && context._turret)
      {
        turret = context._turret;
        pos = ATGetInTurret;
        getInAction = context._turretType->GetGunnerGetInAction();
      }
      else
      {
        for (int i=0; i<veh.GetManCargo().Size(); i++)
        {
          if (veh.GetCargoAssigned(i) == this)
          {
            cargoIndex = i;
            pos = ATGetInCargo;
            getInAction = veh.Type()->GetCargoGetInAction(i);
            break;
          }
        }
        if (pos == ATNone)
        {
          Fail("Vehicle assigned in unit, but unit not assigned in vehicle");
          return false;
        }
      }
    }

    // move soldier to get in position
    Vector3 shift = GetPerson()->GetActionMoveShift(getInAction);
    MoveToGetInPos(this, veh, shift);
#define GET_IN_TIME 5.0f
    GetPerson()->DisableDamageFromObj(GET_IN_TIME);

    // play animation
    GetPerson()->CreateActivityGetIn(&veh, pos, turret, cargoIndex);
    //LogF(" - activity created");
    return true;
  }
  //    return ProcessGetIn2();
  else
    return false;
}

static void JoinSubgroups(AISubgroup *subgrp1, AISubgroup *subgrp2)
{
  if (!subgrp2 || subgrp2 == subgrp1) return;
  AIGroup *grp = subgrp1->GetGroup();
  if (subgrp2->GetGroup() != grp) return;

  if (subgrp2 == grp->MainSubgroup())
  {
    // swap subgroups
    AISubgroup *temp = subgrp2;
    subgrp2 = subgrp1;
    subgrp1 = temp;
  }

  if (!subgrp2->IsAnyPlayerSubgroup())
  {
    Command *cmd = subgrp2->GetCommand();
    if (cmd && cmd->_message == Command::GetIn)
      subgrp2->SendAnswer(AI::CommandCompleted);
  }
  subgrp2->JoinToSubgroup(subgrp1);
}

/*!
\patch 1.34 Date 12/03/2001 by Ondra
- Fixed: Occasional invalid memory access during getting/in out of vehicle.
\patch 5130 Date 2/22/2007 by Jirka
- Fixed: AI - After transport of player's group, AI was staying on the same position
*/

/// Try to find the subgroup to join with for each unit in the vehicle
class JoinSubgroupFunc : public ICrewFunc
{
public:
  virtual bool operator () (Person *person)
  {
    AIBrain *brain = person->Brain();
    if (!brain) return false; // continue
    Transport *veh = brain->GetVehicleIn();
    if (!veh) return false; // continue
    AIBrain *commander = veh->CommanderUnit();
    if (brain == commander) return false; // continue
    
    AIUnit *unit = brain->GetUnit();
    if (!unit) return false; // continue
    AISubgroup *subgrp = unit->GetSubgroup();
    if (!subgrp) return false; // continue
    AIGroup *grp = subgrp->GetGroup();
    if (!grp) return false; // continue

    // try to find some other unit in vehicle to join with
    // the commander of the vehicle is the first candidate
    AIUnit *commanderUnit = commander ? commander->GetUnit() : NULL;
    if (commanderUnit && commanderUnit->GetGroup() == grp)
    {
      if (commanderUnit->GetSubgroup() != subgrp) JoinSubgroups(commanderUnit->GetSubgroup(), subgrp);
      return false; // continue
    }

    // then try the group leader
    AIUnit *leader = grp->Leader();
    if (leader && leader->GetVehicleIn() == veh)
    {
      if (leader->GetSubgroup() != subgrp) JoinSubgroups(leader->GetSubgroup(), subgrp);
      return false; // continue
    }

    // finally check all units with the lower id
    for (int i=0; i<grp->NUnits(); i++)
    {
      AIUnit *u = grp->GetUnit(i);
      if (u == unit) break;
      if (u && u->GetVehicleIn() == veh)
      {
        if (u->GetSubgroup() != subgrp) JoinSubgroups(u->GetSubgroup(), subgrp);
        return false; // continue
      }
    }
    return false; // continue
  }
};


void JoinSubgroups(Transport *veh)
{
  JoinSubgroupFunc func;
  veh->ForEachCrew(func);
}

CameraType ValidateCamera(CameraType cam)
{
  if (cam == CamGunner) return CamInternal;
  return cam;
}

/*!
\patch_internal 1.03 Date 7/12/2001 by Jirka - changed placement of Czech CD Protection
- removed: Get in gunner position (CDPGunnerGetIn)
- removed: Heal (CDPHeal)
- added: Beginning of mission (DisplayMission::DisplayMission)
- added: Saving (World::SaveBin)
*/
void __cdecl CDPGunnerGetIn(Transport *veh, AIBrain *brain)
{
  SECUROM_MARKER_SECURITY_ON(1)
  Person *person = brain->GetPerson();

  if (veh->IsLocal())
  {
    veh->GetInFinished(brain);
    veh->GetInGunner(person);
    //LogF(" - get in gunner processed");
  }
  else
    GetNetworkManager().AskForGetIn(person, veh, GIPGunner);
  if (!veh->CanCancelStop())
  {
    veh->UpdateStopTimeout();
    Verify(brain->SetState(AIUnit::Stopped));
  }

  // join subgroups
  JoinSubgroups(veh);

  // big warning: subgrp may change now
  AIUnit *unit = brain->GetUnit();
  AISubgroup *subgroup = unit ? unit->GetSubgroup() : NULL;

  if (subgroup)
  {
    // avoid leader in cargo
    if (!subgroup->Leader() || !subgroup->Leader()->IsUnit())
      subgroup->SelectLeader();

    subgroup->RefreshPlan();
  }

  // fixed - remote getin can fail - do not switch camera yet (will be switched later)
  TurretContext context;
  bool valid = veh->GetPrimaryGunnerTurret(context);
  if (GLOB_WORLD->FocusOn() == brain && (veh->IsLocal() || (valid && context._turret && context._turret->IsLocal())))
  {
    GLOB_WORLD->SwitchCameraTo(veh, ValidateCamera(GLOB_WORLD->GetCameraType()), false);
    veh->StartFadeIn(brain);
  }

  SECUROM_MARKER_SECURITY_OFF(1)
}

/*!
\patch 1.01 Date 06/11/2001 by Jirka
- Fixed: respawn in MP - problems if player killed during get in animation
- do not continue with get in if unit is dead
- camera can switch to dead body
\patch 1.02 Date 07/12/2001 by Jirka
- Fixed: getin in MP - two players gets in concurrently
- camera has switched to vehicle on both players (but only one gets in)
*/

bool AIBrain::ProcessGetIn2(Transport *veh, UIActionType pos, Turret *turret, int cargoIndex)
{
  //LogF("AIUnit::ProcessGetIn2 %s to %s", (const char *)GetDebugName(), (const char *)veh->GetDebugName());
  // Transport *veh = VehicleAssigned();
  if (!veh) return false;

  // fixed - do not continue with get in if unit is dead
  if (!LSCanGetInGetOut()) return false;

  AIGroup *grp = GetGroup();

  /*
  Person *person = GetPerson();
  RptF
  (
  "Processing get in: %s (%s, id %d:%d) into %s (%s, id %d:%d)",
  (const char *)person->GetDebugName(), person->IsLocal() ? "LOCAL" : "REMOTE",
  person->GetNetworkId().creator.CreatorInt(), person->GetNetworkId().id,
  (const char *)veh->GetDebugName(), veh->IsLocal() ? "LOCAL" : "REMOTE",
  veh->GetNetworkId().creator.CreatorInt(), veh->GetNetworkId().id
  );
  */

  // avoid get in enemy vehicle
  if (AvoidBeInWith(this, veh->Driver()))
  {
    if (_vehicleAssigned == veh)
    {
      UnassignVehicle();
      if (grp) grp->UnassignVehicle(veh);
    }
    return false;
  }
  AvoidBeInWithFunc func(this);
  if (veh->ForEachTurret(func))
  {
    if (_vehicleAssigned == veh)
    {
      UnassignVehicle();
      if (grp) grp->UnassignVehicle(veh);
    }
    return false;
  }
  for (int i=0; i<veh->GetManCargo().Size(); i++)
  {
    Person *man = veh->GetManCargo()[i];
    if (AvoidBeInWith(this, man))
    {
      if (_vehicleAssigned == veh)
      {
        UnassignVehicle();
        if (grp) grp->UnassignVehicle(veh);
      }
      return false;
    }
  }

  switch (pos)
  {
  case ATGetInDriver:
  case ATGetInPilot:
    {
      if (veh->QCanIGetIn(GetPerson()))
      {
        if (veh->IsLocal())
        {
          veh->GetInFinished(this);
          veh->GetInDriver(GetPerson());
          //LogF(" - get in driver processed");
        }
        else
          GetNetworkManager().AskForGetIn(GetPerson(), veh, GIPDriver);
        if (grp) grp->CalculateMaximalStrength();
        if (!veh->CanCancelStop())
        {
          veh->UpdateStopTimeout();
          Verify(SetState(Stopped));
        }

        // join subgroups
        JoinSubgroups(veh);

        AIUnit *unit = GetUnit();
        AISubgroup *subgroup = unit ? unit->GetSubgroup() : NULL;

        if (subgroup)
        {
          // avoid leader in cargo
          if (!subgroup->Leader() || !subgroup->Leader()->IsUnit())
            subgroup->SelectLeader();

          subgroup->RefreshPlan();
        }

        // fixed - remote getin can fail - do not switch camera yet (will be switched later)
        if (GLOB_WORLD->FocusOn() == this && veh->IsLocal())
        {
          GLOB_WORLD->SwitchCameraTo(veh, ValidateCamera(GLOB_WORLD->GetCameraType()), false);
          veh->StartFadeIn(this);
        }
        return true;
      }
      else
      {
        // cannot get in as driver
        return false;
      }
    }
  case ATGetInGunner:
    {
      if (veh->QCanIGetInGunner(GetPerson()))
      {
        CDPGunnerGetIn(veh, this);
        return true;
      }
      else
      {
        // cannot get in as gunner
        return false;
      }
    }
  case ATGetInCommander:
    {
      if (veh->QCanIGetInCommander(GetPerson()))
      {
        if (veh->IsLocal())
        {
          veh->GetInFinished(this);
          veh->GetInCommander(GetPerson());
          //LogF(" - get in commander processed");
        }
        else
          GetNetworkManager().AskForGetIn(GetPerson(), veh, GIPCommander);
        if (!veh->CanCancelStop())
        {
          veh->UpdateStopTimeout();
          Verify(SetState(Stopped));
        }

        // join subgroups
        JoinSubgroups(veh);

        AIUnit *unit = GetUnit();
        AISubgroup *subgroup = unit ? unit->GetSubgroup() : NULL;

        if (subgroup)
        {
          // avoid leader in cargo
          if (!subgroup->Leader() || !subgroup->Leader()->IsUnit())
            subgroup->SelectLeader();

          subgroup->RefreshPlan();
        }

        // fixed - remote getin can fail - do not switch camera yet (will be switched later)
        TurretContext context;
        bool valid = veh->GetPrimaryObserverTurret(context);
        if (GLOB_WORLD->FocusOn() == this && (veh->IsLocal() || (valid && context._turret && context._turret->IsLocal())))
        {
          GLOB_WORLD->SwitchCameraTo(veh, ValidateCamera(GLOB_WORLD->GetCameraType()), false);
          veh->StartFadeIn(this);
        }
        return true;
      }
      else
      {
        // cannot get in as commander
        return false;
      }
    }
  case ATGetInTurret:
    {
      if (veh->QCanIGetInTurret(turret, GetPerson()))
      {
//        if (veh->IsLocal())
        if (turret->IsLocal())
        {
          veh->GetInFinished(this);
          veh->GetInTurret(turret, GetPerson());
          //LogF(" - get in turret processed");
        }
        else
          GetNetworkManager().AskForGetIn(GetPerson(), veh, GIPTurret, turret);
        if (!veh->CanCancelStop())
        {
          veh->UpdateStopTimeout();
          Verify(SetState(Stopped));
        }

        // join subgroups
        JoinSubgroups(veh);

        AIUnit *unit = GetUnit();
        AISubgroup *subgroup = unit ? unit->GetSubgroup() : NULL;

        if (subgroup)
        {
          // avoid leader in cargo
          if (!subgroup->Leader() || !subgroup->Leader()->IsUnit())
            subgroup->SelectLeader();

          subgroup->RefreshPlan();
        }

        // fixed - remote getin can fail - do not switch camera yet (will be switched later)
        if (GLOB_WORLD->FocusOn() == this && (veh->IsLocal() || turret->IsLocal()))
        {
          GLOB_WORLD->SwitchCameraTo(veh, ValidateCamera(GLOB_WORLD->GetCameraType()), false);
          veh->StartFadeIn(this);
        }
        return true;
      }
      else
      {
        // cannot get into turret
        return false;
      }
    }
  default:
    Assert(pos == ATGetInCargo);
    {
      cargoIndex = veh->QCanIGetInCargo(GetPerson(), cargoIndex);
      if (cargoIndex >= 0)
      {
        if (veh->IsLocal())
        {
          veh->GetInFinished(this);
          veh->GetInCargo(GetPerson(), true, cargoIndex);
          //LogF(" - get in cargo processed");
          SetState(InCargo);
        }
        else
          GetNetworkManager().AskForGetIn(GetPerson(), veh, GIPCargo, NULL, cargoIndex);

        // join subgroups
        JoinSubgroups(veh);

        AIUnit *unit = GetUnit();
        AISubgroup *subgroup = unit ? unit->GetSubgroup() : NULL;

        if (subgroup)
        {
          // avoid leader in cargo
          if (!subgroup->Leader() || !subgroup->Leader()->IsUnit())
            subgroup->SelectLeader();

          subgroup->RefreshPlan();
        }

        // fixed - remote getin can fail - do not switch camera yet (will be switched later)
        if (GLOB_WORLD->FocusOn() == this && veh->IsLocal())
        {
          GLOB_WORLD->SwitchCameraTo(veh, ValidateCamera(GLOB_WORLD->GetCameraType()), false);
          veh->StartFadeIn(this);
        }
        return true;
      }
      else
      {
        // cannot get in as cargo
        return false;
      }
    }
  }
}

bool AIBrain::HasAI() const
{
  if (_inVehicle)
  {
    if (_inVehicle->QIsManual(this)) return false;
  }
  else
    if (_person->QIsManual()) return false;

  return !_person->IsRemotePlayer();
}

bool AIBrain::IsPartOfFormation(const AISubgroup *subgrp) const
{
  AIUnit *unit = unconst_cast(this)->GetUnit();
  if (unit)
  {
    if (unit->GetSubgroup()!=subgrp) return false;
    // subgroup leader is always a part of formation, as the formation needs to form around him
    if (unit->IsSubgroupLeader()) return true;
  }
  if (!LSIsAlive()) return false;
  if (!_inVehicle) return true;
  // when the vehicle has no driver, it is unable to move, and cannot be a part of the formation
  if (!_inVehicle->PilotUnit()) return false;
  // only the commander is a part of formation
  return _inVehicle->CommanderUnit() == this;
}

bool AIBrain::IsUnit() const
{
  if (!_inVehicle) return true;
  return _inVehicle->CommanderUnit() == this;
}

bool AIBrain::IsCommander() const
{
  Transport *vehIn = GetVehicleIn();
  return vehIn && vehIn->ObserverBrain() == this;
}

bool AIBrain::IsDriver() const
{
  Transport *vehIn = GetVehicleIn();
  return vehIn && vehIn->DriverBrain() == this;
}

bool AIBrain::IsGunner() const
{
  Transport *vehIn = GetVehicleIn();
  return vehIn && vehIn->GunnerBrain() == this;
}

void ReportUnit(const AIBrain *unit)
{
  RptF
  (
    "Unit %s (0x%x) - network ID %d:%d",
    (const char *)unit->GetDebugName(),
    unit,
    unit->GetNetworkId().creator.CreatorInt(), unit->GetNetworkId().id
  );
}

bool AIBrain::AssertValid() const
{
  if (!GetPerson())
  {
    // all units must be represented by some soldier
    ReportUnit(this);
    RptF(" - no person");
    return false;
  }
  return true;
}

bool AIBrain::IsGroupLeader() const
{
  AIGroup *grp = GetGroup();
  return grp && grp->Leader() == this;
}

bool AIUnit::IsTeamLeader() const
{
  return IsSubgroupLeader() && GetSubgroup()->NUnits()>1 || IsGroupLeader();
}

bool AIUnit::IsTeamSpotter() const
{
  // if we do not have binoculars, we cannot be a spotter
  const Person *person = GetPerson();
  if (!person) return false;
  if (!person->HasBinoculars()) return false;
  // highest ranked unit in the subgroup is assumed a spotter
  const AISubgroup *subgrp = GetSubgroup();
  if (!subgrp) return false;
  if (IsGroupLeader()) return true;
  bool before = true;
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    const AIUnit *unit = subgrp->GetUnit(i);
    if (unit==this)
    {
      before = false;
      continue;
    }
    if (!unit || !unit->LSCanTrackTargets()) continue;

    const Person *unitPerson = unit->GetPerson();
    if (!unitPerson) continue;
    if (!unitPerson->HasBinoculars()) continue;
    if (unit->IsGroupLeader() || unitPerson->GetRank()>person->GetRank() || unitPerson->GetRank()==person->GetRank() && before)
    { // unit higher in the subgroup hierarchy than we has binoculars, therefore we cannot be a spotter
      return false;
    }
  }
  return true;
}

void AIBrain::SetLifeState(LifeState state)
{
  _lifeState = state;
  // update playerRole (if any)
  if (_roleIndex>=0)
  {
    GetNetworkManager().SetPlayerRoleLifeState(_roleIndex, _lifeState);
  }
}

/*!
\patch 1.33 Date 11/28/2001 by Jirka
- Fixed: ErrorMessage when MP server has VoiceRH addon installed and client hasn't
*/
void AIBrain::SetSpeaker(RString speaker, float pitch)
{
  Ref<BasicSpeaker> basic = new BasicSpeaker(speaker);
  _speaker = new Speaker(basic, pitch);
}

void AIBrain::SetSpeaker()
{
  Person *person = GetPerson();
  if (person)
  {
    int dpid = -1;
    if ( person->IsRemotePlayer() ) dpid = person->GetRemotePlayer();
    else if (person->IsLocal() && IsPlayer()) dpid = GetNetworkManager().GetPlayer();
    if (dpid!=-1)
    {
      const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpid);
      if (identity)
      {
        SetSpeaker(person->SelectSpeaker(identity->speakerType), identity->pitch);
        return;
      }
    }
    if (person->GetInfo()._pitch<0.001f)
    {
      ErrF("Setting invalid pitch %.4f for %s",person->GetInfo()._pitch,cc_cast(person->GetDebugName()));
    }
    SetSpeaker(person->GetInfo()._speaker, person->GetInfo()._pitch);
  }
}

float AIBrain::GetRawAbility() const
{
  // returns from 1 (able) to 0.2 (unable)
  if (IsAnyPlayer() || Glob.config.IsEnabled(DTUltraAI)) return 1; // always maximal
#if _ENABLE_IDENTITIES
  return _person->GetIdentity().GetRawAbility(AKGeneral);
#else
  return _ability;
#endif
}

void AIBrain::SetRawAbility(float ability)
{
#if _ENABLE_IDENTITIES
  _person->GetIdentity().SetAllAbilities(ability);
#else
  Assert(ability > 0);
  _ability = ability;
#endif
}

static inline float CombinedAbility(float ability, float abilityCoef)
{
  float lowerBound = floatMinMax(Lerp(-0.2f,0.6f,abilityCoef),0.01f,1.0f);
  float upperBound = floatMinMax(Lerp(0.8f,1.0f,abilityCoef),0.01f,1.0f);
  // ability below minEditorSkill means manipulation using setSkill - in such case let us respect the intent
  const float minEditorSkill = 0.2f;
  if (ability>=minEditorSkill)
    return InterpolativC(ability,minEditorSkill,1,lowerBound,upperBound);
  else
    return InterpolativC(ability,0,minEditorSkill,0,lowerBound);
}

#if 0 // _DEBUG
  static void PrintCombinedAbilityTable()
  {
    static const float coef[]={0.4,0.6,0.85,1};
    static const float abss[]={0.2,0.6,1.0};
    for (int a=0; a<lenof(abss); a++)
    for (int i=0; i<lenof(coef); i++)
    {
      float abilityCoef = coef[i];
      float ability = abss[a];
      float combinedAbility = CombinedAbility(ability,abilityCoef);
      LogF("Difficulty %.2f, skill %.2f -> %.2f",abilityCoef,ability,combinedAbility);
    }
  }
#endif


/*!
\patch 1.33 Date 12/03/2001 by Ondra
- New: Difficulty option "Super AI". When this option is enabled,
all AI units have maximal skill.
*/
float AIBrain::GetAbility(AbilityKind kind) const
{
  if( IsAnyPlayer())
  {
    // player skill maximum, but still subject to ability curves
    return Glob.config.GetAbility(kind, 1);
  }
  // returns from 1 (able) to 0.2 (unable)
  float abilityCoef = 1;
  if (!Glob.config.IsEnabled(DTUltraAI))
  {
    AIGroup *grp = GetGroup();
    if (grp)
    {
      AICenter *cnt = grp->GetCenter();
      if (cnt)
      {
        if (kind==AKAimingAccuracy || kind==AKAimingShake)
        {
          abilityCoef = cnt->GetPrecisionCoef();
        }
        else
        {
          abilityCoef = cnt->GetSkillCoef();
        }
      }
    }
  }
#if _ENABLE_IDENTITIES
  float ability = _person->GetIdentity().GetRawAbility(kind);
#else
  float ability = _ability;
#endif
  //float modifiedAbility = ability * abilityCoef;

  // multiplication not good - GetAbility will saturate in range 0..1 anyway
  float combinedAbility = CombinedAbility(ability,abilityCoef);

  return Glob.config.GetAbility(kind, combinedAbility);
}

float AIBrain::GetInvAbility(AbilityKind kind) const
{ // returns from 1 (able) to 5 (unable)
  return 1.0f/GetAbility(kind);
}

/*!
\patch 5129 Date 2/19/2007 by Jirka
- Fixed: MP - get out waypoint did not work as in single player (player not forced out, player commanded to get in)
*/

void AIBrain::OrderGetIn(bool flag)
{
  if (IsLocal()) OrderGetInLocal(flag);
  else GetNetworkManager().AskForOrderGetIn(this, flag);
}

void AIBrain::AllowGetIn(bool flag)
{
  if (IsLocal()) AllowGetInLocal(flag);
  else GetNetworkManager().AskForAllowGetIn(this, flag);
}

void AIBrain::AllowGetInLocal(bool flag)
{
  _getInAllowed = flag;
  if (!flag && GetUnit())
  {
    AISubgroup *subgrp = GetUnit()->GetSubgroup();
    if (subgrp)
    {
      subgrp->ClearGetInCommands();
      AIGroup *grp = subgrp->GetGroup();
      if (grp) grp->ClearGetInCommands(GetUnit());
    }
  }
}

bool AIBrain::IsPlayer() const
{
  return _person == GWorld->PlayerOn();
}

bool AIBrain::IsAnyPlayer() const
{
  if (IsPlayer()) return true;
  Person *person = GetPerson();
  return person && person->IsNetworkPlayer();
}

bool AIBrain::IsRemotePlayer() const
{
  Person *person = GetPerson();
  return person && person->IsRemotePlayer();
}

void AIBrain::SetUnitPositionCommanded(UnitPosition status)
{
  Person *person = GetPerson();
  if (person) person->SetUnitPositionCommanded(status);
}

void AIBrain::SetUnitPositionScripted(UnitPosition status)
{
  Person *person = GetPerson();
  if (person) person->SetUnitPositionScripted(status);
}

void AIBrain::SetUnitPositionFSM(UnitPosition status)
{
  Person *person = GetPerson();
  if (person) person->SetUnitPositionFSM(status);
}

UnitPosition AIBrain::GetUnitPosition() const
{
  Person *person = GetPerson();
  if (person) return person->GetUnitPosition();
  return UPAuto;
}

class WorldVehicleFindRespawnVehicle
{
protected:
  const AIBrain *_unit;
  mutable Transport **_found;

public:
  WorldVehicleFindRespawnVehicle(const AIBrain *unit, Transport **found) : _unit(unit), _found(found) {}
  bool operator () (Entity *vehicle) const
  {
    Transport *veh = dyn_cast<Transport>(vehicle);
    if (veh && veh->GetRespawnUnit() == _unit)
    {
      *_found = veh;
      return true; // finished
    }
    return false; // continue
  }
};

Transport *AIBrain::FindRespawnVehicle() const
{
  Transport *found = NULL;
  WorldVehicleFindRespawnVehicle check(this, &found);
  GWorld->ForEachVehicle(check);
  return found;
}

bool AIBrain::IsFiringWanted() const
{
    EntityAIFull *veh = GetVehicle();
    // TODO: which turret?
    TurretContext context;
  if (veh && veh->GetPrimaryGunnerTurret(context))
    {
      if (context._weapons->_fire._fireTarget && !context._weapons->_fire._firePrepareOnly)
      {
        // if the current weapon is a binocular style, there is no need to force combat
        // binocular animations work in Safe/Aware as well
        bool combatNeeded = true;
        int weapon = context._weapons->ValidateWeapon(context._weapons->_fire._weapon);
        if (weapon>0)
        {
          const MuzzleType *muzzle = context._weapons->_magazineSlots[weapon]._muzzle;
          if (muzzle && muzzle->_useAsBinocular) combatNeeded = false;
        }
        if (combatNeeded)
        {
        return true;
        }
      }
    }
  return false;  
  }

CombatMode AIBrain::GetCombatModeLowLevel() const
{
  CombatMode mode = GetCombatMode();
  if (mode!=CMCareless)
  {
    if (mode<CMCombat && IsFiringWanted())
    {
      return CMCombat;
    }
  }
  return mode;
}

CombatMode AIBrain::GetCombatMode() const
{
  CombatMode mode = _combatModeMajor;
  if (mode!=CMCareless)
  {
    AIGroup *grp = GetGroup();
    if (grp)
    {
      CombatMode gMode = grp->GetCombatModeMinor();
      if (mode<gMode) mode = gMode;
    }
    CombatModeRange cMode = GetCombatModeBasedOnCommand();
    if (mode<cMode.beg) mode = cMode.beg;
    if (mode>cMode.end) mode = cMode.end;
  }
  return mode;
}

void AIBrain::SetCombatModeMajorReactToChange(CombatMode mode)
{
  if (_combatModeMajor!=mode)
  {
    CombatMode oldCM = GetCombatMode();
    SetCombatModeMajor(mode);
    CombatMode newCM = GetCombatMode();
    if (oldCM!=newCM) OnCombatModeChanged(oldCM,newCM);
  }
}


CombatMode AIBrain::GetCombatModeAutoDetected() const
{
  EntityAIFull *vehicle = GetVehicle();
  if (!vehicle) return CMSafe;
  return vehicle->GetCombatModeAutoDetected(this);
}

void AIBrain::SetCombatModeMajor(CombatMode mode)
{
  _combatModeMajor = mode;
  #if 0 //_ENABLE_CHEATS
  if (GetUnit() && GetUnit()->IsSubgroupLeader())
  {
    LogF("%s: SetCombatModeMajor %s", cc_cast(GetDebugName()), cc_cast(FindEnumName(mode)));
}
  #endif
}

void AIBrain::OnCombatModeChanged(CombatMode oldMode, CombatMode newMode)
{
  // if we are executing some plan, we need to replan it
  
  AIBrain *updatePlan = this;
  Transport *transport = GetVehicleIn();
  if (transport && transport->CommanderUnit())
  {
    AIBrain *transportPlan = transport->CommanderUnit();
    if (transportPlan->GetGroup()==GetGroup() && transportPlan->_state!=InCargo) updatePlan = transportPlan;
  }
  if (updatePlan->_state!=Stopped && updatePlan->_state!=Stopping && updatePlan->_state!=InCargo)
  {
    updatePlan->UpdateOperativePlan();
    // disable planning until search parameters are updated
    updatePlan->_planParametersSet = false;
  }
  #if 0 //_ENABLE_CHEATS
  if (GetUnit() && GetUnit()->IsSubgroupLeader())
  {
    LogF("%s: Changed to %s", cc_cast(GetDebugName()), cc_cast(FindEnumName(newMode)));
  }
  #endif
}

bool AIBrain::IsDanger() const
{
  return Glob.time <= _dangerUntil;
}

void AIBrain::OnNewTarget(TargetNormal *target, Time knownSince)
{
  EntityAIFull * veh = GetVehicle();
  if (veh) veh->OnNewTarget(target,knownSince);
}

void AIBrain::SetDanger(DangerCause cause, Vector3Par position, const Target *causedBy, float until)
{
  PROFILE_SCOPE_EX(aiDng,ai);
  if (IsPlayer()) return;

  if (until < 0) until = GRandGen.PlusMinus(5.0f, 1.0f);

  EntityAI *causedByAI = causedBy ? causedBy->idExact : NULL;
  if (_reactionFSM && !IsAIDisabled(DAFSM))
  {
    // let the FSM know a new danger cause exists
    GameVarSpace *vars = _reactionFSM->GetVars();
    if (vars)
    {
      GameValue value = GGameState.CreateGameValue(GameArray);
      GameArrayType &array = value;
      array.Realloc(3);
      array.Resize(3);
      array[0] = position.X();
      array[1] = position.Z();
      array[2] = position.Y() - GLandscape->SurfaceYAboveWater(position.X(), position.Z());

      // add the event to the queue
      GameValue queue;
      // variable names must be in lowercase
      if (vars->VarGet("_queue", queue) && queue.GetType() == GameArray)
      {
        GameValue item = GGameState.CreateGameValue(GameArray);
        GameArrayType &array = item;

        array.Realloc(4);
        array.Resize(4);
        array[0] = (float)cause; // cause
        array[1] = value; // position
        array[2] = Glob.time.toFloat() + until; // until
        array[3] = GameValueExt(causedByAI);

        ((GameArrayType &)queue).Add(item);
      }
    }
  }
  else
  {
    const EntityType *type = _person ? _person->GetType() : NULL;
    if (type)
    {
      const ParamClass *cls = type->GetParamEntry();
      if (cls)
      {
        RString name = RString("fsm") + FindEnumName((ReactionFSMRole)RFSMDanger);
        _reactionFSM = CreateFSM(type, (*cls) >> name);
        if (_reactionFSM)
        {
          GameVarSpace *vars = _reactionFSM->GetVars();
          if (vars)
          {
            // variable names must be in lowercase
            vars->VarLocal("_dangercause");
            vars->VarLocal("_dangerpos");
            vars->VarLocal("_dangeruntil");
            vars->VarLocal("_dangercausedby");

            GameValue value = GGameState.CreateGameValue(GameArray);
            GameArrayType &array = value;
            array.Resize(3);
            array[0] = position.X();
            array[1] = position.Z();
            array[2] = position.Y() - GLandscape->SurfaceYAboveWater(position.X(), position.Z());

            vars->VarSet("_dangercause", (float)cause);
            vars->VarSet("_dangerpos", value);
            vars->VarSet("_dangeruntil", Glob.time.toFloat() + until);
            vars->VarSet("_dangercausedby", GameValueExt(causedByAI));

            GameValue queue = GGameState.CreateGameValue(GameArray);
            vars->VarLocal("_queue");
            vars->VarSet("_queue", queue);
          }
        }
      }
    }

    if (!(_reactionFSM || IsAIDisabled(DAFSM)) && cause != DCDeadBodyGroup && cause != DCDeadBody)
    {
      // no FSM created, force the default reaction
      switch (GetCombatMode())
      {
      case CMSafe:
      case CMAware:
        {
          bool wasDanger = IsDanger();
          _dangerUntil = Glob.time + until;
          if (!wasDanger)
          {
            EntityAIFull *veh = GetVehicle();
            if (veh) veh->OnDanger();
          }
        }
        break;
      // don't react in Combat mode
      }
    }
  }
}

bool AIBrain::IsHoldingFire() const
{
  return _semaphore == AI::SemaphoreBlue ||
    _semaphore == AI::SemaphoreGreen ||
    _semaphore == AI::SemaphoreWhite;
}

bool AIBrain::IsKeepingFormation() const
{
  return _semaphore == AI::SemaphoreBlue ||
    _semaphore == AI::SemaphoreGreen ||
    _semaphore == AI::SemaphoreYellow;
}

void AIBrain::SetSemaphore(Semaphore status)
{
  if (status == SemaphoreBlue)
  {
    // clear auto attack / hide commands in progress
    AISubgroup *subgrp = GetUnit() ? GetUnit()->GetSubgroup() : NULL;
    if (subgrp) subgrp->ClearAttackCommands();
  }

  _semaphore = status;
}

/*!
\patch 5132 Date 2/23/2007 by Jirka
- Fixed: AI did now follow player over the bridge

@return 1 on road, 2 on bridge
*/
int AIBrain::IsOnRoad() const
{
  if (IsAnyPlayer())
  {
    float size = 0;
    EntityAI *vehicle = GetVehicle();
    if (vehicle && vehicle->GetShape()) size = vehicle->GetShape()->GeometrySphere();
    RoadLink *road = unconst_cast(GLandscape->GetRoadNet()->IsOnRoad(Position(GetFutureVisualState()), size, size));
    if (!road) return 0;
    return road->IsBridge() ? 2 : 1;
  }

  int index = _path.FindNext(Position(GetFutureVisualState()));
  int onRoad = 0;
  if (index >=0 && index < _path.Size())
  {
    // moving to / on the road
    if (_path[index]._type == NTBridge) return 2;
    if (_path[index]._type == NTRoad) onRoad = 1;
  }
  index--;
  if (index >=0 && index < _path.Size())
  {
    // leaving the road
    if (_path[index]._type == NTBridge) return 2;
    if (_path[index]._type == NTRoad) onRoad = 1;
  }
  return onRoad;
}

Target *AIBrain::FindTargetAll(TargetType * id, bool includeBuildings) const
{
  if (!id) return NULL;

  const TargetList *targetList = AccessTargetList();
  if (!targetList) return NULL;

  const TargetNormal *t = targetList->FindTarget(id);
  if (t)
  {
    return unconst_cast(t);
  }

  if (includeBuildings && id->GetLinkId().IsObject())
  {
    // it is very likely it is a building - try to confirm it
    const StaticEntityLink *info = GLandscape->FindBuildingInfo(id->GetLinkId());
    if (!info) return NULL;
    return info->GetVirtualTarget();
  }
  return NULL;
}

Target *AIBrain::FindTarget(TargetType *id, bool includeBuildings) const
{
  Target *target = FindTargetAll(id, includeBuildings);
  if (target && !target->IsKnown()) return NULL;
  return target;
}

/*!
\patch 1.45 Date 2/18/2002 by Ondra
- Fixed: When enemy was killed, killer was always detected,
even when there was no one able to detect him.
(He was detected by the dead unit).
*/

Target *AIBrain::AddTarget(
  EntityAI *object, float accuracy, float sideAccuracy, float delay,
  const Vector3 *pos, AIBrain *sensor, float sensorDelay
  )
{
  TargetList *targetList = AccessTargetList();
  if (!targetList) return NULL;

  // check if target is already in list
  TargetNormal *target=NULL;
  const TargetNormal *t = targetList->FindTarget(object);
  bool isKnown = false;

  if( !t )
  {
    // is it normal, or minimal?
    // check if object is a building
    if (object->GetObjectId().IsObject())
    {
      // if it is from landscape, it might be a building
      // building may be used as a command target
      const StaticEntityLink *info = GLandscape->FindBuildingInfo(object->GetObjectId());
      if (info)
      {
        return info->GetVirtualTarget();
      }
    }
    const SideInfo *side = targetList->GetSideInfo(this);
    if (!side)
    {
      Fail("No side - cannot Add target");
      return NULL;
    }
    target = targetList->CreateTarget(this);
    target->idExact=object;
    //target->dammage=0;
    target->side=TSideUnknown;
    target->sideChecked = false;
    target->type=GWorld->Preloaded(VTypeAllVehicles);
    target->timeReported = TIME_MIN;

    targetList->Add(*side,target);
  }
  else
  {
    // we will set a new accuracy
    target=unconst_cast(t);
    isKnown = target->IsKnownNoDelay();
  }
  // find first type description so that accuracy matches
  if (!isKnown)
  {
    const SideInfo *side = targetList->GetSideInfo(this);
    if (side)
    {
      target->Reveal(*side,targetList,sideAccuracy);
    }
    if( !pos )
    {
      Vector3 apos=object->AimingPosition(object->FutureVisualState());
      // introduce some random error
      target->position=apos;
      target->RandomPosError();
      if (accuracy<1)
      {
        // the one who caused the explosion is expected to be not very far
        target->LimitError(100,100);
      }
      else if (accuracy<3.5f)
      {
        // as the accuracy is quite high, we know the approximate position
        target->LimitError(5,5);
      }
      else
      {
        // we know the almost exact position
        target->LimitError(0.05f,0.05f);
      }
    }
    else
    {
      target->position=*pos;
      // we will expect the target is most likely someplace near the explosion it has caused
      // we may be wrong, but we need somewhere to start from
      target->LimitError(200,200);
    }
    target->speed=VZero; // speed not known
    if (sensor)
    {
      if (sensorDelay>delay) sensorDelay = delay;
      target->lastSeen=Glob.time-10+sensorDelay;
      target->delaySensor = Glob.time+sensorDelay;
      target->delay = Glob.time+delay;
      target->idSensor = sensor;
      if (sensor->GetUnit())
      {
        target->_idSeen.Set(sensor->GetUnit()->ID(),true);
      }
    }
    else
    {
      target->lastSeen=Glob.time-10+delay;
      target->delay=Glob.time+delay;
    }
    // we assume we have seen it, but not very recently
    // we set lastSeen so that target is known only for 5 sec
    // that should be enough to activate defensive reaction
    // usually some Attack is issued
  }
  else
  {
    Time delayUntil=Glob.time+delay;
    if( target->delay>delayUntil ) target->delay=delayUntil;

    if (!target->idSensor && sensor)
    {
      target->idSensor = sensor;
      if (sensor->GetUnit())
      {
        target->_idSeen.Set(sensor->GetUnit()->ID(),true);
      }
    }

    if (!pos && accuracy>=3.5f)
    {
      target->LimitError(0.1f,0.1f);
    }
  }
  const VehicleType *type = object->GetTypeAtLeast(accuracy);
  accuracy = type->GetAccuracy();
  if( target->FadingAccuracy()<accuracy )
  {
    target->accuracy=accuracy;
    target->type=type;
  }
  if( target->FadingSideAccuracy()<sideAccuracy )
  {
    target->sideAccuracy=sideAccuracy;
    target->side=object->GetTargetSide(sideAccuracy);
    Assert( target->sideAccuracy<3 || target->side!=TSideUnknown );
    target->sideChecked = sideAccuracy>=1.5f;
  }
  float spotability=floatMin(accuracy*2.5,1);
  if( target->FadingSpotability()<spotability )
  {
    target->spotability=spotability;
    target->spotabilityTime=Glob.time;
  }
  return target;
}

/// ghost target is used to control unit fire sectors or suppressive fire
/**
Ghost target has very little properties - type and position is all we need.

Ghost target is always known only by one unit.
*/

class TargetGhost: public Target
{
  typedef Target base;

protected:
  /// owner of this target
  OLinkPermNO(AIBrain) _owner;

  Vector3 _position;

  Ref<const EntityAIType> _type;

  TargetSide _side;
  //@{ no accuracy fading for ghosts
  float _posAccuracy;
  float _sideAccuracy;
  //@}

  public:

  /// empty constructor used for serialization
  TargetGhost();
  TargetGhost(
    AIBrain *owner, const EntityAIType *type, Vector3Par position,
    TargetSide side, float posAccuracy, float sideAccuracy
  );

  ~TargetGhost();

  virtual const EntityAIType *GetType() const {return _type;}
  virtual Vector3 GetSpeed(const AIBrain *who) const {return VZero;}

  virtual bool IsVanishedOrDestroyed() const {return false;}
  virtual bool IsDestroyed() const {return false;}
  virtual bool IsVanished() const {return false;}
  virtual bool HasDiclosedUs() const {return false;}
  virtual TargetSide GetSide() const {return _side;}
  virtual bool GetSideChecked() const {return true;}
  virtual Time GetLastSeen() const;
  virtual float Distance(Vector3Par pos) const {return _position.Distance(pos);}
  virtual Vector3 GetPosition() const {return _position;}
  virtual Vector3 GetDirection() const;
  virtual float GetSubjectiveCost() const {return 0;}
  virtual void UpdateWhenUnassigned(AIGroup *group) {};
  virtual bool IsKindOf(const EntityType *supertype) const {return _type->IsKindOf(supertype);}
  virtual void UpdateRemotePosition(EntityAI *ent) {}

  virtual AIBrain *SeenBy() const;
  virtual void MarkKiller(EntityAI *by) {}

  virtual float FadingSideAccuracy() const {return _sideAccuracy;}
  virtual float FadingAccuracy() const {return _sideAccuracy;}
  virtual float FadingSpotability() const {return 4;}
  virtual float FadingPosAccuracy() const {return _posAccuracy;}

  virtual void UpdatePosition(Vector3Par pos, float typeAccuracy, float posAccuracy)
  {
    _position = pos; _sideAccuracy = typeAccuracy; _posAccuracy = posAccuracy;
  }

  virtual bool IsKnownBy( const AIBrain *unit ) const {return unit==_owner;}
  virtual bool IsKnownByAll() const {return false;}
  virtual bool IsKnownBySome() const {return true;}
  virtual void Reveal(const SideInfo &side, TargetList *list, float accuracy) {}
  virtual void RevealInVehicle(Transport *veh) {}
  virtual void DeleteFromOwner();
  virtual bool AccurateEnoughForHit(const AmmoType *ammo, AIBrain *brain) const;
  virtual float PositionAccuracy(const AIBrain *brain) const;
  virtual bool SeenRecently(AIBrain *brain, float maxAge=10.0f) const;

  virtual bool IsKnown() const {return true;}

  virtual float VisibleSize() const;
  virtual Vector3 AimingPosition() const {return _position;}
  virtual Vector3 LandAimingPosition() const {return _position;}
  virtual Vector3 LandAimingPosition(const AIBrain *who, const AmmoType *ammo=NULL, bool head=false, bool *isExact=NULL) const
  {
    return _position;
  }

  virtual TargetState State(AIBrain *sensor) const
  {
    // TODO: based on side we may select a different TargetState
    return TargetEnemyEmpty;
  }

  /// during serialization we need to know if the target is minimal or not
  virtual bool IsMinimal() const;

  static TargetGhost *CreateObject(ParamArchive &ar);
  virtual LSError Serialize(ParamArchive &ar);
  virtual LSError SaveRef(ParamArchive &ar);
};

TargetGhost::TargetGhost()
{
}

TargetGhost::~TargetGhost()
{
  if (_type)
  {
    _type->VehicleRelease();
  }
}

TargetGhost::TargetGhost(
  AIBrain *owner, const EntityAIType *type, Vector3Par position,
  TargetSide side, float posAccuracy, float sideAccuracy
):_owner(owner),_type(type),
_position(position),_side(side),
_posAccuracy(posAccuracy),_sideAccuracy(sideAccuracy)
{
  type->VehicleAddRef();
}

Vector3 TargetGhost::GetDirection() const
{
  Fail("Ghost targets cannot be queried for orientation");
  return VForward;
}

Time TargetGhost::GetLastSeen() const
{
  return Glob.time;
}

bool TargetGhost::IsMinimal() const
{
  return false;
}

float TargetGhost::PositionAccuracy(const AIBrain *brain) const
{
  return _posAccuracy;
}

bool TargetGhost::AccurateEnoughForHit(const AmmoType *ammo, AIBrain *brain) const
{
  // ghost targets have no memory error - ignore brain
  return _posAccuracy < ammo->indirectHitRange*2+0.5f;
}

bool TargetGhost::SeenRecently(AIBrain *brain, float maxAge) const
{
  return true;
}

float TargetGhost::VisibleSize() const
{
  LODShape *shape = _type->GetShape();
  if (shape)
  {
    return shape->GeometrySphere();
  }
  return 1.0f;
}

void TargetGhost::DeleteFromOwner()
{
  Assert(_owner->GetGhostTarget()==this);
  _owner->SetGhostTarget(NULL);
}

AIBrain *TargetGhost::SeenBy() const
{
  return _owner;
}

LSError TargetGhost::SaveRef(ParamArchive &ar)
{
  // there is only one ghost target in any unit - one reference is enough
  RString type("ghost");
  CHECK(ar.SerializeRef("owner",_owner,1))
  return LSOK;
}

TargetGhost *TargetGhost::CreateObject(ParamArchive &ar)
{
  return new TargetGhost();
}

LSError TargetGhost::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("owner", _owner, 1));

  CHECK(::Serialize(ar, "type", _type, 1))

  if (ar.IsLoading() && ar.GetPass()==ParamArchive::PassFirst)
  {
    // we did not perform VehicleAddRef in constructor - we need to do it now
    _type->VehicleAddRef();
  }

  CHECK(::Serialize(ar,"position", _position, 1, VZero));

  CHECK(ar.SerializeEnum("side", _side, 1, TCivilian));

  CHECK(ar.Serialize("posAccuracy", _posAccuracy, 1, 0));
  CHECK(ar.Serialize("sideAccuracy", _sideAccuracy, 1, 4));

  // idExact is always NULL and base::Serialize will do nothing in current version
  return base::Serialize(ar);
}

Target *LoadTargetGhostRef(ParamArchive &ar)
{
  OLinkPermNO(AIBrain) owner;
  if(ar.SerializeRef("owner",owner,1)==LSOK)
  {
    Assert(owner->GetGhostTarget());
    return owner->GetGhostTarget();
  }
  Fail("Bad ghost target owner");
  return NULL;
}


void AIBrain::SetGhostTarget(TargetGhost *tgt){_ghostTarget=tgt;}
TargetGhost *AIBrain::GetGhostTarget() const {return _ghostTarget;}

Target *AIBrain::CreateGhostTarget(
	const EntityAIType *type, Vector3Par pos,
	float typeAccuracy, float posAccuracy, float sensorDelay
)
{
  // create a dummy object in the landscape
  // add the object into the list
  // note: TargetGhost increases vehicle ref. count of type
  _ghostTarget = new TargetGhost(
    this,type,pos,type->GetTypicalSide(),posAccuracy,typeAccuracy
  );
  /// owner of this target
  return _ghostTarget;
}

void AIBrain::MoveTarget(Target *tgt, Vector3Par pos, float typeAccuracy, float posAccuracy)
{
  tgt->UpdatePosition(pos,typeAccuracy,posAccuracy);
}

void AIBrain::DeleteGhostTarget(Target *tgt)
{
  Assert(tgt==_ghostTarget)
  _ghostTarget = NULL;
}


void AIBrain::CheckAmmo(CheckAmmoInfo &info)
{
  Person *person = GetPerson();
  if (person) person->CheckAmmo(info);
}

void AIBrain::SetWatchDirection( Vector3Val dir )
{
  _watchDirHead = _watchDir = dir;
  _watchMode = WMDir;
  _watchDirSet = Glob.time;
  _targetAssigned = TargetToFire();
}

void AIBrain::SetWatchAround()
{
  _watchDirHead = _watchDir = GetVehicle()->FutureVisualState().Direction();
  _watchMode = WMAround;
  _watchDirSet = Glob.time;
  _targetAssigned = TargetToFire();
}

void AIBrain::SetNoWatch()
{
  _watchTgt = NULL, _watchMode = WMNo, _glanceTgt = NULL;
  _targetAssigned = TargetToFire();
}

void AIBrain::SetWatchPosition( Vector3Val pos, bool glanceOnly)
{
  if (glanceOnly)
  {
    _watchTgt = NULL;
    _watchMode = WMPosGlance;
    _glancePos = pos;
    Assert(_glancePos.IsFinite());
  }
  else
  {
    _watchPos = pos;
    _glancePos = VZero;
    _watchMode = WMPos;
  }
  _targetAssigned = TargetToFire();
}

void AIBrain::SetWatchTarget(Target *tgt, bool glanceOnly)
{
  if(tgt && tgt->idExact && tgt->idExact->IsArtilleryTarget()) return;
  if (!tgt)
  {
    if (glanceOnly)
    {
      // cancel glance only, leave the other one untouched
      _glanceTgt = NULL;
    }
    else
    {
      _glanceTgt = NULL;
      _watchTgt = NULL;
    }
    // WMTgt falls through to WMNo when there is no target
    _watchMode = WMTgt;
  }
  else if (glanceOnly)
  {
    _glanceTgt = tgt;
    _watchMode = WMTgtGlance;
  }
  else
  {
    _glanceTgt = NULL;
    _watchTgt = tgt;
    _watchMode= WMTgt;
  }
}

/*!
\patch 1.24 Date 9/20/2001 by Ondra
- New: Ejection from plane is real ejection, not only jumping out.
\patch 1.27 Date 10/09/2001 by Ondra
- Fixed: MP: Ejection on client problems fixed. This could also cause player duplication.
*/

void AIBrain::DoGetOut(Transport *veh, bool eject, bool parachute, Vector3Val diff)
{
  bool isFocused = (GLOB_WORLD->FocusOn() == this);

  Person *person = GetPerson();
  Vector3 ejectSpeed = person->FutureVisualState().Speed();
#define GET_OUT_TIME 5.0f
  person->DisableDamageFromObj(GET_OUT_TIME);
  /*
  RptF
  (
  "Processing get out: %s (%s, id %d:%d) from %s (%s, id %d:%d)",
  (const char *)person->GetDebugName(), person->IsLocal() ? "LOCAL" : "REMOTE",
  person->GetNetworkId().creator.CreatorInt(), person->GetNetworkId().id,
  (const char *)veh->GetDebugName(), veh->IsLocal() ? "LOCAL" : "REMOTE",
  veh->GetNetworkId().creator.CreatorInt(), veh->GetNetworkId().id
  );
  */

  // calculate get out position
  GetInPoint pt = veh->GetUnitGetOutPos(this);
  Matrix4 transform;
  transform.SetPosition(pt._pos);
  transform.SetUpAndDirection(VUp, -pt._dir);

  if (diff != VZero)
  {
    Vector3 diffR = transform.Rotate(diff);
    transform.SetPosition(pt._pos + diffR);
  }

  veh->GetOutFinished(this);

  if (veh->DriverBrain() == this) // driver
  {
    veh->GetOutDriver(transform,eject);
    if (IsLocal())
    {
      AIGroup *grp = GetGroup();
      if (grp) grp->CalculateMaximalStrength();
    }
  }
  else
  {
    TurretContext context;
    if (veh->FindTurret(person, context))
    {
      // turrets
      veh->GetOutTurret(context, transform);
    }
    else
    {
      // cargo
      veh->GetOutCargo(person, transform);

      // the only place where reset InCargo flag
      if (IsLocal())
      {
        Assert(IsInCargo());
        SetInCargo(false);
      }
    }
  }

  // vehicle changed, no longer report its state
  AIGroup *group = GetGroup();
  if (group)
  {
    group->SetDamageStateReported(GetUnit(), AIUnit::RSNormal);
    group->SetFuelStateReported(GetUnit(), AIUnit::RSNormal);
  }

  // FIX: check the state
  if (_state == Stopping || _state == Stopped)
  {
    Verify(SetState(Wait));
  }

  if (parachute)
  {
    const char *paraName = "ParachuteWest";
    TargetSide side = GetSide();
    if (side==TEast) paraName = "ParachuteEast";
    else if (side==TGuerrila) paraName = "ParachuteG";
    else if (side==TCivilian) paraName = "ParachuteC";

    Ref<EntityAI> ent = GWorld->NewVehicleWithID(paraName,"");
    Ref<Transport> getOutTo = dyn_cast<Transport,EntityAI>(ent);
    if (getOutTo)
    {
      getOutTo->SetAllowDammage(person->GetAllowDammage());
      if (getOutTo->Driver())
      {
        ErrF("%s already has a driver",(const char *)getOutTo->GetDebugName());
      }
      else
      {
        //Log("Getting out to %s",(const char *)getOutTo->GetDebugName());
        Vector3 aimCenter = getOutTo->GetShape()->AimingCenter();
        // aimCenter should be on pos position

        //getOutTo->AimingPosition();

        Matrix4 trans = person->FutureVisualState().Transform();
        trans.SetPosition(trans.Position()+getOutTo->FutureVisualState().Speed()*0.3f-aimCenter);

        getOutTo->SetSpeed(veh->FutureVisualState().Speed()+ejectSpeed);
        getOutTo->SetTransform(trans);

        GWorld->AddVehicle(getOutTo);
        GetNetworkManager().CreateVehicle(getOutTo, VLTVehicle, "", -1);

        DoAssert(getOutTo->IsLocal());

        getOutTo->GetInFinished(this);
        getOutTo->GetInDriver(person,false);
      }
    }

  }
  else
  {
    // set him into freefall mode.
    person->FreeFall(1.0f);
  }

  if (isFocused)
  {
    GLOB_WORLD->SwitchCameraTo(GetVehicle(), ValidateCamera(GLOB_WORLD->GetCameraType()), false);
    person->StartFadeIn(this);
  }
}

bool AIBrain::ProcessGetOut(bool eject, bool parachute, Vector3Val diff)
{
  // if (!IsLocal()) return false;
  if (IsFreeSoldier()) return false;

  Transport *veh = GetVehicleIn();
  if (!veh) return false;

  if (!LSCanGetInGetOut())
  {
    // when get out is impossible, do not longer wait and fail the command
    veh->GetOutFinished(this);
    return false;
  }

  // do not get out when vehicle is locked (for player or player group units)
  if (veh->GetLock() == LSLocked)
  {
    AIGroup *grp = GetGroup();
    if (grp && grp->IsPlayerGroup() && !IsPlayer()) // player is handled on different level (we want to get him out when this point was reached)
    {
      // when get out is impossible, do not longer wait and fail the command
      veh->GetOutFinished(this);
      return false;
    }
  }

  Turret *turret = NULL;
  if (GetPerson() != veh->Driver())
  {
    TurretContext context;
    if (veh->FindTurret(GetPerson(), context)) turret = context._turret;
  }

  bool local = turret ? turret->IsLocal() : veh->IsLocal();
  if (local)
    DoGetOut(veh, eject, parachute);
  else
  {
    GetNetworkManager().AskForGetOut(GetPerson(), veh, turret, eject, parachute);

    // actions on unit's side
    if (veh->DriverBrain() == this) // driver
    {
      AIGroup *grp = GetGroup();
      if (grp) grp->CalculateMaximalStrength();
    }
    if (IsInCargo()) SetInCargo(false);
  }

  AIUnit *unit = GetUnit();
  if (unit)
  {
    AISubgroup *subgrp = unit->GetSubgroup();
    if (subgrp)
    {
      if (unit->IsGroupLeader() && !unit->IsSubgroupLeader())
        subgrp->SelectLeader(unit);

      subgrp->RefreshPlan();
    }
  }

  return true;
}

void AIBrain::Load(const IdentityInfo &info)
{
  Person *soldier = GetPerson();
  if (soldier)
  {
    // avoid identity overwrite when delay EH processed
    LoadIdentityDelayed();

    AIUnitInfo &unitInfo = soldier->GetInfo();
    unitInfo._name = info._name;
    unitInfo._face = info._face;
    unitInfo._glasses = info._glasses;
    soldier->SetFace(unitInfo._face);
    soldier->SetGlasses(unitInfo._glasses);
    unitInfo._speaker = info._speaker;
    unitInfo._pitch = info._pitch;
    
    if (info._speaker.GetLength() <= 0)
    {
      RptF("No speaker given for %s",cc_cast(info._name));
    }

    SetSpeaker(unitInfo._speaker, unitInfo._pitch);
  }
}

bool AIBrain::CanHear(ChatChannel channel, AIBrain *sender) const
{
#if _ENABLE_DIRECT_MESSAGES
  if (channel == CCDirectSpeaking)
  {
    const float limit2 = Square(50.0f);
    float dist2 = Position().Distance2(sender->Position());
    return dist2 <= limit2;
  }
#endif
  return true;
}

float AIBrain::GetTimeToLive() const
{
  const float MinTimeToLive = 5.0;

  // assume we can live max. 24 hours
  float maxTimeToLive = 3600 * 24.0;
  // when in combat, assume we can live max. 1 hour
  if (GetCombatMode() >= CMCombat) maxTimeToLive = 3600;

  if (!GetGroup())
  {
    // LogF("No exposure map for agents.");
    return maxTimeToLive;
  }

  // exposure is updated quite slowly
  // when we calculate timeToLive from current target,
  // we will almost certainly not underestimate

  EntityAIFull *vehicle = GetVehicle();
  int x = toIntFloor(vehicle->FutureVisualState().Position().X() * InvLandGrid);
  int z = toIntFloor(vehicle->FutureVisualState().Position().Z() * InvLandGrid);
  float exposure = GetGroup()->GetCenter()->GetExposureOptimistic(x, z);
  if (exposure <= 0) return maxTimeToLive;

  float timeToLive = 60 * vehicle->GetArmor() / exposure;
  saturate(timeToLive, MinTimeToLive, maxTimeToLive);
  return timeToLive;
}

bool AIBrain::SetRemoteControlled(Person *by)
{
  if (IsLocal())
  {
    _remoteControlled = by;
    return true;
  }
  else
  {
    GetNetworkManager().AskRemoteControlled(by, this);
    return false;
  }
}

bool AIBrain::CheckEmpty(Vector3Par pos )
{
  EntityAI *veh = GetVehicle();
  AILockerSavedState state;
  veh->PerformUnlock(&state);

  OperMapIndex xe(toIntFloor(pos.X() * InvOperItemGrid));
  OperMapIndex ze(toIntFloor(pos.Z() * InvOperItemGrid));

  LAND_INDICES(xe, ze, xei, zei);

  const int border = 0;

  OperMap map;
  map.CreateMap(veh, LandIndex(xei - border), LandIndex(zei - border), LandIndex(xei + border), LandIndex(zei + border));

  bool ret = map.GetFieldCost(xe, ze, true, GetVehicle()->GetType(), CMAware, IsSoldier(), false).cost < GET_UNACCESSIBLE;
  veh->PerformRelock(state);
  return ret;
}

bool AIBrain::NearestEmptyReady(Vector3Par pos) const
{
  LandIndex xe(toIntFloor(pos.X() * InvLandGrid));
  LandIndex ze(toIntFloor(pos.Z() * InvLandGrid));

  // FindNearestEmpty will search in +-1 range
  // CreateFiled may use 1 more field
  // RoadSurfaceY may use 1 more field - this is handled inside of OperFieldRangeReady
  LandIndex xMin(xe - 2), xMax(xe + 2);
  LandIndex zMin(ze - 2), zMax(ze + 2);
  return OperMap::OperFieldRangeReady(xMin, xMax, zMin, zMax);
}

bool AIBrain::FindNearestEmpty(Vector3 &pos, int mask)
{
  PROFILE_SCOPE_EX(aiFNE,aiPath);
  return FindNearestEmpty(pos, IsSoldier(), GetVehicle(), mask);
}

bool AIBrain::FindNearestEmpty(Vector3 &pos, bool soldier, EntityAI *veh, int mask)
{
  AILockerSavedState state;
  veh->PerformUnlock(&state);

  float xef = pos.X() * InvOperItemGrid;
  float zef = pos.Z() * InvOperItemGrid;
  OperMapIndex xe(toIntFloor(xef));
  OperMapIndex ze(toIntFloor(zef));

  LAND_INDICES(xe, ze, xei, zei);

  const int border = 1;

  const EntityAIType *vehType = veh->GetType();
  OperMap map;
  map.CreateMap(veh, LandIndex(xei - border), LandIndex(zei - border), LandIndex(xei + border), LandIndex(zei + border), mask);

  if (map.GetFieldCost(xe, ze, true, vehType, CMAware, soldier, false).cost >= GET_UNACCESSIBLE)
  {
    OperMapIndex oxe(xe), oze(ze);
    if (map.FindNearestEmpty(
      xe, ze, xef, zef,
      OperMapIndex((xei - border) * OperItemRange), OperMapIndex((zei - border) * OperItemRange),
      OperMapIndex((1 + 2 * border) * OperItemRange), OperMapIndex((1 + 2 * border) * OperItemRange),
      true, vehType, soldier, true, DefFindFreePositionCallback, NULL))
    {
      // xEnd, zEnd must be changed (is forced into last path point)
      if( xe!=oxe || ze!=oze )
      {
        pos[0]= xe * OperItemGrid + 0.5 * OperItemGrid;
        pos[2] = ze * OperItemGrid + 0.5 * OperItemGrid;
      }
    }
    else
    {
#if LOG_PROBL
      Log("Unit %s: End point not found", (const char *)veh->GetDebugName());
      map.LogMap(xei - border, xei + border, zei - border, zei + border, xe, xe, ze, ze, false);
#endif
      veh->PerformRelock(state);
      return false;
    }
  }
  veh->PerformRelock(state);
  return true;
}

bool AIBrain::FindEmptyArea(Vector3 &center, float radius, float maxDistance, const EntityAIType *vehType)
{
  OperMap map;
  return map.FindEmptyArea(center, radius, maxDistance, vehType);
}

bool AIBrain::FindEmptyInRect(
  Vector3 &emptyPos, const FrameBase &pos, const Vector3 *minmax, Vector3Par nearTo, int mask
)
{
  PROFILE_SCOPE_EX(aiFNE,aiPath);
  return FindEmptyInRect(emptyPos, IsSoldier(), GetVehicle(), pos, minmax, nearTo, mask);
}

bool AIBrain::FindEmptyInRect(
  Vector3 &emptyPos, bool soldier, EntityAI *veh,
  const FrameBase &pos, const Vector3 *minmax, Vector3Par nearTo, int mask
)
{
  AILockerSavedState state;
  veh->PerformUnlock(&state);

  float xef = pos.Position().X() * InvOperItemGrid;
  float zef = pos.Position().Z() * InvOperItemGrid;
  OperMapIndex xe(toIntFloor(xef));
  OperMapIndex ze(toIntFloor(zef));

  LAND_INDICES(xe, ze, xei, zei);

  // start with no border - will be extended as needed (on demand)
  const int border = 0;

  const EntityAIType *vehType = veh->GetType();
  OperMap map;
  map.CreateMap(veh, LandIndex(xei - border), LandIndex(zei - border), LandIndex(xei + border), LandIndex(zei + border), mask);

  // provide a default value in case search is not needed
  emptyPos = pos.Position();

  OperMapIndex oxe(xe), oze(ze);
  if(
    map.FindEmptyInRect(
      xe, ze,
      pos,minmax,nearTo, true, vehType, soldier, true,
      DefFindFreePositionCallback, NULL
    )
  )
  {
    // xEnd, zEnd must be changed (is forced into last path point)
    if( xe!=oxe || ze!=oze )
    {
      emptyPos[0]= xe * OperItemGrid + 0.5 * OperItemGrid;
      emptyPos[2] = ze * OperItemGrid + 0.5 * OperItemGrid;
    }
  }
  else
  {
    veh->PerformRelock(state);
    return false;
  }
  veh->PerformRelock(state);
  return true;
}


void AIBrain::ClearOperativePlan()
{
  //DoAssert(GetVehicle()->PlanChangePossible());
  _path.Clear();
  SetPathCosts(0, 0);
}

void AIBrain::ClearStrategicPlan()
{
  _noPath = true;
  _updatePath = false;
  _lastPlan = false;

#if _ENABLE_AI
  _attemptPlan = 0;
  _planner->Init();
  _waitWithPlan = Time(0);
  _completedTime = Glob.clock.GetTimeInYear();
#endif

  if
  (
    GetState() != InCargo &&
    GetState() != Stopping && GetState() != Stopped
  )
  Verify(SetState(Wait));

  _exposureChange = 0;
}

void AIBrain::RefreshStrategicPlan()
{
#if _ENABLE_AI
  if (_noPath)
  {
    ClearStrategicPlan();
  }
  else
  {
    Assert(_planner->GetPlanSize() > 0);
    _planner->StopSearching();
    _updatePath = true;
  }
  _exposureChange = 0;
#endif
}

void AIBrain::ForceReplan(bool clear)
{
  _plannedPosition = _wantedPosition;
  if (clear)
  {
    ClearStrategicPlan();
    ClearOperativePlan();
  }
  else
    RefreshStrategicPlan();
}

void AIBrain::GetExpectedDestination(PlanPosition &destination, float &precision, PlanningMode &planningMode, bool &forceReplan)
{
  destination = _wantedPositionWanted;
  precision = _wantedPrecision;
  planningMode = _planningModeWanted;
  forceReplan = _forceReplanWanted;

  // Fix: avoid re-planning multiple times
  _forceReplanWanted = false;
}

bool AIBrain::CheckReplanNeeded(float baseCoef, PlanningMode planningMode, bool forceReplan) const
{
  if (planningMode == FormationPlanned && !_map.IsDone() && !GetVehicle()->IsMovingInConvoy())
  {
    // when moving in formation, planning target is jumping a lot
    // this is normal, and not a reason for frequent replanning
    // when operative path-finding is in progress, do not interrupt it if not needed (to avoid cycling)
    // this is especially important when the unit currently does not have any plan
    // in such case we strongly prefer to finish the one we are planning now
    float coef2 = Square(baseCoef);
    if (_path.Size()<2) coef2 *= 4;

    Vector3 offToPlan = _plannedPosition-Position(GetFutureVisualState());
    Vector3 offToWant = _wantedPosition-Position(GetFutureVisualState());
    
    float error2 = _plannedPosition.Distance2(_wantedPosition);
    // if _plannedPosition is the same as _wantedPosition, there is no use to force replanning
    if (GetVehicle()->IsInFormation() && offToWant.SquareSize()>GetVehicle()->GetPrecision() && error2>0)
    { // in formation - vehicle will not be moving any more, but we need to move
      forceReplan = true;
    }
    else
    {
      // check how much the two vector differ
      float cosAngleDiff = offToPlan.CosAngle(offToWant);
      // the more they point in the same direction, the less we care about the change
      coef2 *= Square(InterpolativC(cosAngleDiff,0,1,1.0f,4.0f));

      float limit2 = coef2 * Position(GetFutureVisualState()).Distance2(_wantedPosition);
      forceReplan = error2 > limit2;
    }

  }
  else if (!forceReplan)
  {
    float error2 = _plannedPosition.Distance2(_wantedPosition);
    float limit2 = baseCoef * Position(GetFutureVisualState()).Distance2(_wantedPosition);
    forceReplan = error2 > limit2;
  }

  return forceReplan;
}

void AIBrain::SetWantedDestination(const PlanPosition &destination, PlanningMode planningMode, bool forceReplan)
{
  PlanningMode oldPlanningMode = _planningMode;
  _planningMode = planningMode;
  _wantedPosition = destination;
  _completedReceived = false;
  _planParametersSet = true;
  #if 0 //_ENABLE_CHEATS
  if (GetUnit() && GetUnit()->IsSubgroupLeader())
  {
    LogF("%s: SetWantedDestination, planning mode %s", cc_cast(GetDebugName()), cc_cast(FindEnumName(planningMode)));
  }
  #endif
  
  // LogF("Unit %s: SetWantedDestination [%.0f, %.0f]", (const char *)GetDebugName(), pos.X(), pos.Z());
#if DIAG_WANTED_POSITION
if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == GetVehicle())
{
  LogF(
    "Unit %s: received wanted destination %s to %.0f, %.0f",
    cc_cast(GetDebugName()), cc_cast(FindEnumName(planningMode)),
    destination.X(), destination.Z());
}
#endif

  if (planningMode!=DoNotPlan && planningMode!=DoNotPlanFormation) GetVehicle()->GoToStrategic(destination);

  switch (planningMode)
  {
  case DoNotPlan: case  DoNotPlanFormation:
    // continue with plan
    if (forceReplan)
    {
      ClearStrategicPlan();
      ClearOperativePlan();
    }
    break;
  case LeaderPlanned:
  case VehiclePlanned:
    //    if (_state == Completed) SetState(Wait);
  case FormationPlanned:
#if 0
if (forceReplan)
{
  LogF("Force replan passed");
}
#endif
    forceReplan = CheckReplanNeeded(0.3f, planningMode, forceReplan);

    if (forceReplan)
    {
      _plannedPosition = _wantedPosition;
      // when moving in formation, almost any destination is temporary
      // we want to avoid clearing the path while switching the destination
      if (planningMode != FormationPlanned) _noPath = true;
      // when _noPath, RefreshStrategicPlan will call ClearStrategicPlan and ClearOperativePlan
      RefreshStrategicPlan();
    }
    // else replan only when _wantedPosition differs from _plannedPosition enough
    break;
  case LeaderDirect:
    // force replan
    if (forceReplan || planningMode!=oldPlanningMode || _plannedPosition.Distance2(destination)>Square(1.0f))
    {
      _plannedPosition = PlanPosition(destination,false);
      _expPosition = _plannedPosition;
      ClearStrategicPlan();
      ClearOperativePlan();
    }
    break;
  }
}

void AIBrain::SetPlanningMode(PlanningMode planningMode, bool forceReplan)
{
  _planningModeWanted = _planningMode = planningMode;
  if (planningMode != DoNotPlan && planningMode!=DoNotPlanFormation) GetVehicle()->GoToStrategic(_wantedPosition);

  switch (planningMode)
  {
  case DoNotPlan: case DoNotPlanFormation:
    // continue with plan
    if (forceReplan)
    {
      ClearStrategicPlan();
      ClearOperativePlan();
    }
    break;
  case LeaderPlanned:
  case VehiclePlanned:
    //    if (_state == Completed) SetState(Wait);
  case FormationPlanned:
    forceReplan = CheckReplanNeeded(0.1f, planningMode, forceReplan);

    if (forceReplan)
    {
      _plannedPosition = _wantedPosition;
      if (planningMode != FormationPlanned) _noPath = true;
      RefreshStrategicPlan();
    }
    // else replan only when _wantedPosition differs from _plannedPosition enough
    break;
  case LeaderDirect:
    // force replan
    _plannedPosition = _wantedPosition;
    _expPosition = _wantedPosition;
    ClearStrategicPlan();
    ClearOperativePlan();
    break;
  }
}

void AIBrain::SetWantedPosition(const PlanPosition &pos, float precision, PlanningMode mode, bool forceReplan)
{
#if DIAG_PLANNING
  LogF
    (
    "%.3f, %s: SetWantedPosition to %.0f, %.0f, mode %s %s",
    Glob.time.toFloat(), (const char *)GetDebugName(),
    pos.X(), pos.Z(), (const char *)FindEnumName(mode),
    forceReplan ? ", force replan" : ""
    );
#endif

#if DIAG_WANTED_POSITION
if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == GetVehicle())
{
  LogF(
    "Unit %s: received destination %s to %.0f, %.0f",
    cc_cast(GetDebugName()), cc_cast(FindEnumName(mode)),
    pos.X(), pos.Z());
}
#endif

  _wantedPositionWanted = pos;
  _wantedPrecision = precision;
  _planningModeWanted = mode;
  _forceReplanWanted = forceReplan;

  // when changing planning mode to DoNotPlan, we need to be sure no more planning will be done
  // therefore we set not only wanted values, but also real ones
  bool setDirectly = !_behaviourFSMs[BFSMFormation] || mode==DoNotPlan || mode==DoNotPlanFormation || IsAIDisabled(DAFSM);
  if (!setDirectly)
  {
    EntityAIFull *vehicle = GetVehicle();
    if (!vehicle) setDirectly = true;
    else
    {
      if (vehicle->PilotUnit() != this)
      {
        // set directly for non-drivers
        setDirectly = true;
      }
      else
      {
        // set directly for the subgroup leader
        //AIBrain *commander = vehicle->CommanderUnit();
        //setDirectly = commander->GetUnit() && commander->GetUnit()->IsSubgroupLeader();
      }
    }
  }

  if (setDirectly)
  {
    // no interface FSM - set the destination immediately
    SetWantedDestination(pos, mode, forceReplan);
  }
}

/*!
\patch_internal 5095 Date 12/4/2006 by Jirka
- Fixed: Avoid using simple path when moving onto / out of bridge
*/

bool AIBrain::IsSimplePath(Vector3Val from, Vector3Val pos) const
{
  // if FSM requested cover searching, we must always enforce it - simple test can never pass
  if (_maxCostToCover>0) return false;
  
  const float maxDist=MAX_OPER_PATH;
  float dist2=(from-pos).SquareSizeXZ();
  if( dist2>Square(maxDist) ) return false;

  EntityAIFull *vehicle = GetVehicle();

  // check if not starting / ending on the bridge
  if (!GLandscape->GetRoadNet()->IsOnRoadReady(from)) return false;
  if (!GLandscape->GetRoadNet()->IsOnRoadReady(pos)) return false;
  
  float size = vehicle->GetShape() ? vehicle->GetShape()->GeometrySphere() : 0;
  const RoadLink *road = unconst_cast(GLandscape->GetRoadNet()->IsOnRoad(from, size, size));
  if (road)
  {
    // check if the road is a bridge
    if (road->IsBridge()) return false;
  }
  road = GLandscape->GetRoadNet()->IsOnRoad(pos, size, size);
  if (road)
  {
    // check if the road is a bridge
    if (road->IsBridge()) return false;
  }

  AILockerSavedState state;
  vehicle->PerformUnlock(&state);

  OperMapIndex xs(toIntFloor(from.X() * InvOperItemGrid));
  OperMapIndex zs(toIntFloor(from.Z() * InvOperItemGrid));
  OperMapIndex xe(toIntFloor(pos.X() * InvOperItemGrid));
  OperMapIndex ze(toIntFloor(pos.Z() * InvOperItemGrid));

  LAND_INDICES(xs, zs, xMin, zMin);
  LAND_INDICES(xe, ze, xMax, zMax);
  if (xMax < xMin) swap(xMax,xMin);
  if (zMax < zMin) swap(zMax,zMin);

  OperMap map;
  if ((xMax-xMin)>50 || (zMax-zMin)>50)
  {
    LogF("xMax %d, xMin %d",xMax,xMin);
    LogF("zMax %d, zMin %d",xMax,xMin);
    LogF("pos %.2f,%.2f,%.2f",pos[0],pos[1],pos[2]);
    LogF("from %.2f,%.2f,%.2f",from[0],from[1],from[2]);
  }
  map.CreateMap(vehicle, xMin, zMin, xMax, zMax);
  bool retVal = map.IsSimplePath(this, xs, zs, xe, ze);

  vehicle->PerformRelock(state);
  return retVal;
}

bool AIBrain::IsSimplePathReady(Vector3Val from, Vector3Val pos) const
{
  const float maxDist=MAX_OPER_PATH;
  float dist2=(from-pos).SquareSizeXZ();
  if( dist2>Square(maxDist) ) return true;

  OperMapIndex xs(toIntFloor(from.X() * InvOperItemGrid));
  OperMapIndex zs(toIntFloor(from.Z() * InvOperItemGrid));
  OperMapIndex xe(toIntFloor(pos.X() * InvOperItemGrid));
  OperMapIndex ze(toIntFloor(pos.Z() * InvOperItemGrid));

  if (xs == xe && zs == ze) return true;

  LAND_INDICES(xs, zs, xMin, zMin);
  LAND_INDICES(xe, ze, xMax, zMax);
  if (xMax < xMin) swap(xMax,xMin);
  if (zMax < zMin) swap(zMax,zMin);

  EntityAIFull *vehicle = GetVehicle();
  AILockerSavedState state;
  vehicle->PerformUnlock(&state);

  bool retVal = OperMap::OperFieldRangeReady(xMin, xMax, zMin, zMax);

  vehicle->PerformRelock(state);
  return retVal;
}

// plan the path as far as possible
Vector3 AIBrain::NearestIntersection(Vector3Val from, Vector3Val pos)
{
  const float maxDist=MAX_OPER_PATH;
  float dist2=(from-pos).SquareSizeXZ();
  if( dist2>Square(maxDist) ) return from;

  EntityAI *veh = GetVehicle();
  AILockerSavedState state;
  veh->PerformUnlock(&state);

  float xsf(from.X() * InvOperItemGrid);
  float zsf(from.Z() * InvOperItemGrid);
  float xef(pos.X() * InvOperItemGrid);
  float zef(pos.Z() * InvOperItemGrid);

  OperMapIndex xMinO(toIntFloor(floatMin(xsf,xef)));
  OperMapIndex zMinO(toIntFloor(floatMin(zsf,zef)));
  OperMapIndex xMaxO(toIntCeil(floatMax(xsf,xef)));
  OperMapIndex zMaxO(toIntCeil(floatMax(zsf,zef)));
  
  LAND_INDICES(xMinO, zMinO, xMin, zMin);
  LAND_INDICES(xMaxO, zMaxO, xMax, zMax);
  Assert(xMin<=xMax);
  Assert(zMin<=zMax);
  
  OperMap map;
  if ((xMax-xMin)>50 || (zMax-zMin)>50)
  {
    LogF("xMax %d, xMin %d",xMax,xMin);
    LogF("zMax %d, zMin %d",xMax,xMin);
    LogF("pos %.2f,%.2f,%.2f",pos[0],pos[1],pos[2]);
    LogF("from %.2f,%.2f,%.2f",from[0],from[1],from[2]);
  }
  map.CreateMap(GetVehicle(), xMin, zMin, xMax, zMax);
  float dist = map.FirstIntersection(xsf, zsf, xef, zef, veh->GetType(), GetCombatMode(), IsSoldier());
  Assert(dist*OperItemGrid<=from.Distance(pos)*1.001f+OperItemGrid);
  
  veh->PerformRelock(state);
  if (dist<=0) return from;
  if (dist>=FLT_MAX) return pos;
  if (Square(dist*OperItemGrid)>=from.Distance2(pos)) return pos;

  Vector3 endDir = pos-from;
  endDir.Normalize();
  Vector3 retVal = from+endDir*dist*OperItemGrid;
  return retVal;
}

#if _ENABLE_AI


/// distance in OperItemGrid to avoid searching
const float DistNotSearch = 2.0f;



#define ROAD_DIAGS 0
#define HOUSE_DIAGS 0

bool AIBrain::VerifyPath(bool waiting, float timeAhead) const
{
  // do not verify fresh path
  if (_path.GetSearchTime()>Glob.time-0.5 && !waiting)
  {
    // assume fresh path is always valid
    return true;
  }

  bool ret = true;
  EntityAI *veh=GetVehicle();
  //if (_path.GetOnRoad())
  {
    AILockerSavedState state;
    veh->PerformUnlock(&state);
    int index = _path.FindNext(veh->FutureVisualState().Position());
    float startCost = _path.CostAtPos(veh->FutureVisualState().Position());
    bool isSoldier = IsFreeSoldier();
    for (int i=index; i<_path.Size(); i++)
    {
      const OperInfoResult &pathI = _path[i];
      if (pathI._type==NTRoad || pathI._type==NTBridge)
      {
        if (pathI._road && (waiting ? pathI._road->IsLockedByWaiting(isSoldier) : pathI._road->IsLocked(isSoldier)))
        {
          ret = false;
          break;
        }
      }
      if (pathI._cost>startCost+timeAhead) // do not sample behind the point which is interesting for us
        break;
      if (pathI._cost>_path.GetReplanCost()) // do not sample behind the point which is interesting for us
        break;
    }
    veh->PerformRelock(state);
  }
  #if 0
  else
  {
    Vector3 lastPos = Position();
    {
      OperMap map;
      int x = toIntFloor(lastPos.X() * InvOperItemGrid);
      int z = toIntFloor(lastPos.Z() * InvOperItemGrid);
      int xx = x / OperItemRange;
      int zz = z / OperItemRange;
      AILockerSavedState state;
      veh->PerformUnlock(&state);
      map.CreateMap(this, xx, zz, xx, zz);
      bool result = map.GetFieldCost(x, z, true, veh, IsSoldier()) >= GET_UNACCESSIBLE;
      veh->PerformRelock(state);
      if (result)
        return true;  // cannot find valid path - don't try it
    }

    int index = _path.GetOperIndex();
    for (int i=index; i<_path.GetMaxIndex(); i++)
    {
      if (!IsSimplePath(lastPos, _path[i]._pos))
      {
        /*
        LogF
        (
        "Unit %s: path is not valid - index %d/%d",
        (const char *)GetDebugName(), i, _path.GetMaxIndex() - 1
        );
        */
        return false;
      }
      lastPos = _path[i]._pos;
    }
  }
  #endif
  return ret;
}

void AIBrain::CopyPath(const IAIPathPlanner &planner)
{
  EntityAI *veh = GetVehicle();
  float combatHeight = veh->GetCombatHeight();

  int planIndex = planner.FindBestIndex(Position(GetFutureVisualState()));
  int i, n = planner.GetPlanSize() - planIndex;
  if (planner.GetPlanSize() > 0 && n > 0)
  {
    float sumCost = 0;
    Point3 lastPos = Position(GetFutureVisualState());

    _path.Resize(n + 1);
    _path[0]._pos = lastPos;
    _path[0]._cost = sumCost;
    _path[0]._type = NTGrid;

    for (i=0; i<n; i++)
    {
      PlanPosition pos;
      planner.GetPlanPosition(planIndex + i, pos);
      int x = toIntFloor(pos.X() * InvLandGrid);
      int z = toIntFloor(pos.Z() * InvLandGrid);
      GeographyInfo geogr = GLOB_LAND->GetGeography(x, z);
      float cost = veh->GetBaseCost(geogr, false, true) * veh->GetFieldCost(geogr);
      if (cost >= GET_UNACCESSIBLE)
        cost = 1.0;
      cost *= (pos - lastPos).SizeXZ();
      sumCost += cost;
      lastPos = GLandscape->PointOnSurface(pos[0],combatHeight,pos[2]);
      _path[i + 1]._pos = lastPos;
      _path[i + 1]._cost = sumCost;
      _path[i + 1]._type = NTGrid;
    }
  }
  else
  {
    int x = toIntFloor(Position(GetFutureVisualState()).X() * InvLandGrid);
    int z = toIntFloor(Position(GetFutureVisualState()).Z() * InvLandGrid);
    GeographyInfo geogr = GLOB_LAND->GetGeography(x, z);
    float cost = veh->GetBaseCost(geogr, false, true) * veh->GetFieldCost(geogr);
    if (cost >= GET_UNACCESSIBLE)
      cost = 1.0;
    cost *= (_expPosition - Position(GetFutureVisualState())).SizeXZ();
    _path.Resize(2);
    _path[0]._pos = Position(GetFutureVisualState());
    _path[0]._cost = 0;
    _path[0]._type = NTGrid;
    _path[1]._pos = GLandscape->PointOnSurface(_expPosition[0],combatHeight,_expPosition[2]);
    _path[1]._cost = cost;
    _path[1]._type = NTGrid;
  }
  if (_path.Size() >= 2)
    SetPathCosts(_path[1]._cost, _path[_path.Size() - 1]._cost);
  else
    SetPathCosts(0, 0);
  _path.SetSearchTime(Glob.time);
  _path.SetCover(CoverInfo(),false);
  ++_pathId;
  // once we are finished with strategic plan, we have nothing more to do
  _lastPlan = _path.Size()<=2;
}

void AIBrain::CreateStrategicPath(ThinkImportance prec)
{
  PROFILE_SCOPE_EX(pthSt, aiPath);

  const PlanPosition &dest = _plannedPosition;
  if (_noPath && _waitWithPlan <= Glob.time)
  {
#if LOG_THINK
    Log("  - Searching plan (strategic) to %.1f, %.1f (%d,%d).",
      dest.X(), dest.Z(), xe, ze);
#endif
    if (!_planner->IsSearching())
    {
      // begin new search
      if (!_planner->StartSearching(prec, GetVehicle(), Position(GetFutureVisualState()), dest))
        OnStrategicPathNotFound(false);
      else if (!_planner->IsSearching())
        OnStrategicPathFound();
    }
    if (_planner->IsSearching())
    {
      // continue with searching
      if (!_planner->ProcessSearching())
        OnStrategicPathNotFound(false);
      else if (!_planner->IsSearching())
        OnStrategicPathFound();
    }
  }
  else if (_updatePath && _waitWithPlan <= Glob.time)
  {
#if LOG_THINK
    Log("  - Update plan (strategic) to %.1f, %.1f (%d,%d).",
      dest.X(), dest.Z(), xe, ze);
#endif
    if (!_planner->IsSearching())
    {
      // begin new search
      if (!_planner->StartSearching(prec, GetVehicle(), Position(GetFutureVisualState()), dest))
        OnStrategicPathNotFound(true);
      else if (!_planner->IsSearching())
        OnStrategicPathFound();
    }
    if (_planner->IsSearching())
    {
      // continue with searching
      if (!_planner->ProcessSearching())
        OnStrategicPathNotFound(true);
      else if (!_planner->IsSearching())
        OnStrategicPathFound();
    }
  }
}

void AIBrain::OnStrategicPathFound()
{
  _noPath = false;
  _updatePath = false;
  _lastPlan = false;
  State state = GetState();
  if
    (
    state != Delay && state != InCargo &&
    state != Stopping && state != Stopped
    )
    Verify(SetState(Wait));
  _iter = 0;

  float totalCost = _planner->GetTotalCost();
  _completedTime = Glob.clock.GetTimeInYear() + totalCost * OneSecond;
}

void AIBrain::OnStrategicPathNotFound(bool update)
{
  _waitWithPlan = Glob.time + 10.0F;

  if (update)
  {
#if LOG_POSITION_PROBL
    LogF("Did not found path in update.");
#endif
    _completedTime = Glob.clock.GetTimeInYear() + OneDay; // set conditions for timeout
  }
  else
  {
    switch (_planningMode)
    {
    case LeaderPlanned:
      if (++_attemptPlan >= 3)
      {
        if (GetUnit())
          GetUnit()->GetSubgroup()->FailCommand();
      }
      else
      {
        if (GetUnit())
          GetUnit()->GetSubgroup()->SendAnswer(AI::SubgroupDestinationUnreacheable);
      }
      break;
    case FormationPlanned:
    case VehiclePlanned:
      // nothing to do
      break;
    default:
      Fail("Unexpected mode");
      break;
    }
  }
}

bool AIBrain::OperPath(ThinkImportance prec)
{
/*
  EntityAI *veh = GetVehicle();
*/

  SetHouse(NULL, -1);
  Vector3Val pos = Position(GetFutureVisualState());
  float dist2;

  if
  (
    _state != Stopped &&
    _state != Stopping &&
    _state != Delay &&
    _state != InCargo
  )
  {
    if (!_noPath)
    {
      // GetCoverUsed returns false as long as there is some cover and the cover was not consumed yet
      if (!GLandscape->GetRoadNet()->IsOnRoadReady(pos)) return false; // wait until files are loaded

      // construct path by strategic plan
      int planIndex = _planner->FindBestIndex(pos);
      int minPlanIndex = planIndex;
      do
      {
        _lastPlan = _planner->GetPlanPosition(planIndex, _expPosition);
        dist2 = (_expPosition - pos).SquareSizeXZ();
      }
      while (dist2 < Square(DIST_MIN_OPER) && ++planIndex < _planner->GetPlanSize());


      if (_iter == 1)
      {
        if (planIndex > minPlanIndex)
        {
          planIndex--;
          _lastPlan = _planner->GetPlanPosition(planIndex, _expPosition);
        }
      }
      else if (_iter == 2)
      {
        if (planIndex < _planner->GetPlanSize() - 1)
        {
          planIndex++;
          _lastPlan = _planner->GetPlanPosition(planIndex, _expPosition);
        }
      }

      //DoAssert(!_lastPlan || _expPosition.Distance2(_plannedPosition)<Square(1));

      saturateMin(planIndex, _planner->GetPlanSize() - 1);

      if (_lastPlan)
      {
        if (prec <= LevelFastOperative)
        {
          SetMode(AIUnit::Exact);
        }
        else
        {
          SetMode(AIUnit::DirectExact);
        }
        if (GetUnit())
        {
          Command *cmd = GetUnit()->GetSubgroup()->GetCommand();
          if (cmd && cmd->_message == Command::Move && cmd->_target)
          {
            const IPaths *house = cmd->_target->GetIPaths();
            if( house && cmd->_target->GetShape()->FindPaths()>=0 )
            {
              DoAssert(cmd->_intParam < house->NPos());
              SetHouse(cmd->_target, cmd->_intParam);
            }
          }
        }
      }
      else
      {
        if (prec <= LevelFastOperative)
        {
          SetMode(AIUnit::Normal);
        }
        else
        {
          SetMode(AIUnit::DirectNormal);
        }
      }

      Verify(SetState(AIUnit::Init));
    }
  }
  else
  {
    _expPosition = PlanPosition(pos,false);
    _lastPlan = false;
    // do not change state (Stopping, Stopped, InCargo, Delay)
  }
  return true;
}

bool AIBrain::CreateOperativePath()
{
  PROFILE_SCOPE_EX(pthOp, aiPath);

  EntityAIFull *veh = GetVehicle();

  // new attempt
  if (_state == Delay && _delay <= Glob.time)
  {
    Verify(SetState(Wait));
    return false;
  }

  if (_state == Planning)
  {
    // planning in progress
    AILockerSavedState state;
    veh->PerformUnlock(&state);
    bool ok = _map.IsDestinationNotLocked(this) || _map.IsDone();
    bool done = false;
    if (ok)
      done = _map.ProcessFindPath(this);
    veh->PerformRelock(state);
    if (!ok)
    {
      // replan
#if DIAG_A_STAR
      if (diags)
      {
        LogF(
          "%.2f %s destination is locked - restart searching",
          Glob.time.toFloat(), cc_cast(GetVehicle()->GetDebugName()));
      }
#endif
      SetState(Init);
      return false;
    }
    if (done)
    {
#if DIAG_A_STAR
      if (diags)
      {
        if (_map.IsDone())
        {
          LogF(
            "%.2f %s finished searching of operative path",
            Glob.time.toFloat(), cc_cast(GetVehicle()->GetDebugName())
          );
        }
        else
        {
          LogF(
            "%.2f %s did not found operative path",
            Glob.time.toFloat(), cc_cast(GetVehicle()->GetDebugName())
          );
        }
      }
#endif
#if DIAG_DELAY
      float delay = 0.001f * (GlobalTickCount() - _map._findStarted);
      if (GetGroup() && GetGroup()->Leader() == this)
      {
        LogF("*** %s - path %s, %s in %.3f s (%d iterations)", cc_cast(GetDebugName()),
          _map.IsDone() ? "done" : "not done", _map.IsFound() ? "found" : "not found",
          delay, _map._iterOp);
      }
#endif
      bool result = _map.ExportPath(this);
      if (result) OnOperativePathFound();
      else OnOperativePathNotFound();
      FinishOperativePath(result);
      // avoid too early replan when searching of path takes too long
      GetPath().SetSearchTime(Glob.time);
      return true;
    }
    return true;
  }

  if (_state != Init)
  {
    // check if current path is valid
    if (!VerifyPath(false,2.0f))
    {
      if (GetFormationLeader() == this)
      {
        Verify(SetState(Replan));
      }
      else
      {
        Verify(SetState(Wait)); // clear plan
      }
    }
    // do not plan
    return false;
  }

  if( veh->GetType()->StrategicPlanningOnly())
  {
    if (_mode == Exact)
      _mode = DirectExact;
    else if (_mode == Normal)
      _mode = DirectNormal;
  }
  if (_mode == DirectExact || _mode == DirectNormal)
  {
    // creates path from Position() to _expPosition if none in _planner
    CopyPath(*_planner);
    Verify(SetState(Busy));
    return false;
  }
  else
  {
    // check if plan is already completed (we are near destination point)
    // FIX: for subordinated driver, this situation is valid (commander must send command first)
    if (IsCommander())
    {
      // FIX: was SquareSizeXZ() - problems with searching inside buildings
      float dist2=(Position(GetFutureVisualState()) - _expPosition).SquareSize();
      if (dist2 < Square(DistNotSearch*OperItemGrid))
      {
        if (_lastPlan)
        {
          float precision = veh->GetPrecision();
          if (_wantedPosition.Distance(_plannedPosition) > Square(precision))
            ForceReplan();
          else
          {
            _completedReceived = false; // enforce state change
            Verify(SetState(Completed));
            // mark search time to avoid quickly repeating it
            _path.SetSearchTime(Glob.time);
          }
        }
        else
        {
          if (veh->PlanChangePossible())
          {
            Verify(SetState(Replan));
          }
        }
        return false;
      }
    }
    //_state = Busy;
    {
      // check if we need some plan
      // if not, simply report the completition
      // motivation of this is: when unit completes a waypoint because it has reached the completition circle
      // we want to avoid it planning while it may be waiting for other units
      Vector3 planFrom = veh->PositionToPlanFrom();
#if 0 // the following code is not necessary - if the unit has completed, it will not plan any more
      if (_wantedPrecision>0 && _lastPlan)
      {
        if (_expPosition.Distance2(planFrom)<Square(_wantedPrecision))
        {
          // pretend the search is waiting for something
          if (_state!=Completed)
          {
            _state = Init;
          }
          return false;
        }
      }
#endif

      bool result = CreateOperativePath(planFrom, _expPosition);
      if (_state==InitFailed)
      {
        _state = Init;
        return false;
      }
      {
        if (_state != Planning) FinishOperativePath(result);
      }
    }
    return true;
  }
}

void AIBrain::FinishOperativePath(bool found)
{
  if (!found || _path.Size() == 0)
  {
    // path not found
    LogF("%s: path not found - iteration %d", (const char *)GetDebugName(), _iter);
    if (++_iter > 2)
    {
      if (_planningMode==LeaderPlanned)
      {
        _completedReceived = false; // enforce state change - we can do it because command will be failed
        Verify(SetState(Completed));
        LogF("  fail command");
        if (GetUnit())
        {
          GetUnit()->GetSubgroup()->FailCommand();
        }
      }
      else
      {
        // pathfinding failed for a subordinate - wait a while before retrying
        _delay = Glob.time + 5.0;
        Verify(SetState(Delay));
      }
      _iter = 0;
    }
    else
    {
      _delay = Glob.time + 5.0;
      Verify(SetState(Delay));
    }
    return;
  }

  _iter = 0;

  if (!_path.IsIntoCover())
  {
    // add strategic plan to path
    int index = -1;
    int n = _planner->GetPlanSize();
    // FIX: at least the first point is covered by the operative map
    for (int i=1; i<n; i++)
    {
      PlanPosition pos;
      _planner->GetPlanPosition(i, pos);
      if ((pos - _expPosition).SquareSizeXZ() < 1.0f)
      {
        index = i;
        break;
      }
    }
    if (index >= 0)
    {
      EntityAI *veh = GetVehicle();
      float combatHeight = veh->GetCombatHeight();

      int j = _path.Size() - 1;
      float lastCost = _path[j]._cost;
      Point3 lastPos = GLandscape->PointOnSurface(_expPosition[0],combatHeight,_expPosition[2]);
      for (int i=index+1; i<n; i++)
      {
        PlanPosition pos;
        _planner->GetPlanPosition(i, pos);
        int x = toIntFloor(pos.X() * InvLandGrid);
        int z = toIntFloor(pos.Z() * InvLandGrid);
        GeographyInfo geogr = GLOB_LAND->GetGeography(x, z);
        float cost = veh->GetBaseCost(geogr, false, true) * veh->GetFieldCost(geogr);
        if (cost >= GET_UNACCESSIBLE)
          cost = 1.0;
        cost *= (pos - lastPos).SizeXZ();
        lastCost += cost;
        lastPos = GLandscape->PointOnSurface(pos[0],combatHeight,pos[2]);
        j = _path.Add();
        _path[j]._pos = lastPos;
        _path[j]._cost = lastCost;
        _path[j]._type = NTGrid;
        //_path[j]._dir = 0xff;
      }
    }
  }
  Verify(SetState(Busy));
}

static bool InsideBuildingReady(Vector3Val pos)
{
  PROFILE_SCOPE_EX(pthBR, aiPath);

  int xMin,xMax,zMin,zMax;
  ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,50);
  return GWorld->CheckObjectsReady(xMin,xMax,zMin,zMax,false);
}

static const IPaths *InsideBuilding(Vector3Val pos)
{
  int xMin,xMax,zMin,zMax;
  ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,50);
  int x,z;
  for( x=xMin; x<=xMax; x++ ) for( z=zMin; z<=zMax; z++ )
  {
    const ObjectListUsed &list=GLandscape->UseObjects(x,z);
    int n=list.Size();
    for( int i=0; i<n; i++ )
    {
      Object *obj=list[i];

      if (!obj->GetShape()) continue;
      if (obj->GetShape()->FindPaths() < 0) continue;
      // vehicle with paths - must be building
      float dist2 = obj->FutureVisualState().Position().Distance2(pos);
      if (dist2 > Square(obj->GetShape()->BoundingSphere())) continue;
      // pos is inside vehicle's bounding

      const IPaths *building = obj->GetIPaths();
      if (!building) continue;

      // check if point is near of some path in building
      Vector3 nearest;
      int nearestIndex = building->FindNearestPoint(pos,nearest,Square(2.5));
      if( nearestIndex>=0 )
      {
        return building;
      }
      // test if pos is inside building
      // test1
      /*
      for (int j=0; j<building->NPoints(); j++)
      {
      Vector3 bpos = building->GetPosition(j);
      if (bpos.Distance2(pos) < Square(2.5f))
      return building;
      }
      */
      // test2
      /*
      CollisionBuffer result;
      obj->Intersect(result, pos, pos + Vector3(0, 0.5, 0), 0);
      if (result.Size() > 0)
      {
      LogF
      (
      "Point %.1f,%.1f,%.1f is inside %s",
      pos.X(), pos.Y(), pos.Z(),
      (const char *)obj->GetDebugName()
      );
      return building;
      }
      */
    }
  }
  //LogF("Point %.1f,%.1f,%.1f is outside", pos.X(), pos.Y(), pos.Z());
  return NULL;
}


#pragma warning(disable:4101) // prevent warning C4101: 'AllowBPMemLock' : unreferenced local variable
bool AIBrain::CreateOperativePath(Vector3Par from, const PlanPosition &to)
{
  #if 0 // _DEBUG || _PROFILE
  extern int AllowBPMemLock;
  extern void StartBPScope();
  struct ScopeBP: private NoCopy
  {
    ScopeBP(){StartBPScope();AllowBPMemLock++;}
    ~ScopeBP(){AllowBPMemLock--;}
  } allowBPInScope;
  #endif
  //DoAssert(GetVehicle()->PlanChangePossible());
  //LogF("Time %.1f: Searching path (house = %x)", Glob.time.toFloat(), _house);
  
  // this function sets path.time to time of last search only if searched
  // when data are not ready, state is set to InitFailed

  if (IsFreeSoldier() && GetPerson()->IsLadderActionInProgress())
  {
    // do not replan now
    _state = InitFailed;
    return false;
  }

  bool result = BeginOperativePath(from, to);
  if (_state == InitFailed)
  {
    return false;
  }
  if (_state != Planning)
  {
    _path.SetSearchTime(Glob.time);
    return result;
  }
  
  return false;
}
#pragma warning(default:4101)

/// compute integer coordinates and ranges needed for path-finding
static inline void CalculateOperMapRange(
 OperMapIndex &xs, OperMapIndex &zs, OperMapIndex &xe, OperMapIndex &ze,
 float &xsf, float &zsf, float &xef, float &zef,
 LandIndex &xMin, LandIndex &xMax, LandIndex &zMin, LandIndex &zMax,
 Vector3Par begPos, Vector3Par endPos
)
{
  xsf = begPos.X() * InvOperItemGrid;
  zsf = begPos.Z() * InvOperItemGrid;
  xef = endPos.X() * InvOperItemGrid;
  zef = endPos.Z() * InvOperItemGrid;

  xs = OperMapIndex(toIntFloor(xsf));
  zs = OperMapIndex(toIntFloor(zsf));
  xe = OperMapIndex(toIntFloor(xef));
  ze = OperMapIndex(toIntFloor(zef));
  
  LandIndices(xs, zs, xMin, zMin);
  LandIndices(xe, ze, xMax, zMax);
  if (xMax < xMin) swap(xMax, xMin);
  if (zMax < zMin) swap(zMax, zMin);
}


/*!
\patch_internal 5100 Date 12/11/2006 by Jirka
- Fixed: Path item type was sometimes uninitialized, causing warnings in serialization
\patch 5142 Date 3/19/2007 by Jirka
- Fixed: AI - path planning improved (more smooth transition between plans)
*/


#define REUSE_PATH 1
#define DIAG_REUSE_PATH 0

bool AIBrain::BeginOperativePath(Vector3Par fromSrc, const PlanPosition &pos)
{
  #if 0 // _PROFILE
  if (GetUnit() && GetUnit()->ID()==2)
  {
    LogF("%s: search to %.1f,%.1f",cc_cast(GetDebugName()),pos.X(),pos.Z());
  }
  #endif
  Vector3 from = fromSrc;
  // from will be updated, make own copy

  PROFILE_SCOPE_EX(pthBe, aiPath);
  DoAssert(_state != Planning);

  // store search parameters
  _from = from;
  _fromBusy = false;
  _to = pos;
  
  AIUnit *thisUnit = GetUnit();
  bool inFormation = thisUnit && !IsFormationLeaderVehicle() && thisUnit->GetSubgroup();
  
  if (_maxCostToCover>0 && inFormation)
  {
    // if planning into a cover, plan a bit forward
    // cover selection will select a cover based on formation position anyway
    _to += thisUnit->GetSubgroup()->GetMoveDirection()*GetVehicle()->GetType()->GetFormationZ()*5.0f;
  }
  _toRoad = false;
  // keep original position in _from, manipulate with from (and set _fromBusy when changing)
#if DIAG_REUSE_PATH
  LogF("%.3f Path searching for %s started", Glob.time.toFloat(), cc_cast(GetDebugName()));
#endif

  GetPath().SetSearchTime(Glob.time);
  _oldPath.Clear();

  float dist2 = _to.DistanceXZ2(from);
  if (dist2 > Square(MAX_OPER_PATH))
  {
    LogF("Error: Unit %s is searching path per distance %.0f", (const char *)GetDebugName(), sqrt(dist2));
    ClearOperativePlan();
    return false;
  }

  EntityAI *veh = GetVehicle();
  bool isSoldier = IsFreeSoldier();
  const EntityAIType *vehType = veh->GetType();
  float combatHeight = veh->GetCombatHeight();
  float vehWidth = 0;
  if (!isSoldier)
  {
    LODShape *shape = vehType->GetShape();
    if (shape) vehWidth = shape->Max().X() - shape->Min().X();
  }

  AILockerSavedState state;
  veh->PerformUnlock(&state);

  // calculate the original direction (we can change it later when reusing the old path)
  int dir;
  if (GetVehicleIn() && GetVehicleIn()->GetMoveMode() == VMMBackward)
    dir = CalcDirection(-Direction(GetFutureVisualState()));
  else
    dir = CalcDirection(Direction(GetFutureVisualState()));

  
  {
#if REUSE_PATH
    if (_path.Size() >= 2)
    {
      // valid path
      //int next = _path.FindNext(from);
      
      float curCost = _path.CostAtPos(from);
      float maxCost = _path.GetForceReplanCost();
      // avoid reusing too short path, could create a singular path
      // note: on roads even a short (even a singular) path can be useful
      if (curCost < maxCost)
      {
        // possible to reuse part of path
        bool allowReuse = true;
        static float reuseCost = 3.0f;
        float fromCost = floatMin(curCost + reuseCost, curCost + 0.75f * (maxCost - curCost));

        int endIndex = _path.FindNextCost(fromCost);
        int startIndex = intMax(_path.FindNextCost(curCost)-1,0);
        
        // once on road, reuse the road part only, and only the part which is not locked
        bool roadOnly = _path[startIndex]._type==NTRoad || _path[startIndex]._type==NTBridge;
        if (roadOnly && _path[startIndex]._road && _path[startIndex]._road->IsLocked(isSoldier))
        {
          allowReuse = false;
        }
        else for (int i=startIndex+1; i<=endIndex; i++)
        {
          if (_path[i]._type==NTRoad || _path[i]._type==NTBridge)
          {
            if (_path[i]._road && _path[i]._road->IsLocked(isSoldier))
            {
              endIndex = i-1;
              fromCost = _path[endIndex]._cost;
              break;
            }
            roadOnly = true;
          }
          else if (roadOnly)
          {
            // no longer on the road - stop reusing
            endIndex = i-1;
            fromCost = _path[endIndex]._cost;
            break;
          }
          else if (_path[i]._type==NTGrid)
          {
            // check the lock
            ILockCache *locks = GLandscape->LockingCache();

            float invOperItemGrid = InvOperItemGrid;
            OperMapIndex x(toIntFloor(_path[i]._pos[0]*invOperItemGrid)); // inverse of _x * OperItemGrid + 0.5f * OperItemGrid
            OperMapIndex z(toIntFloor(_path[i]._pos[2]*invOperItemGrid)); // inverse of _z * OperItemGrid + 0.5f * OperItemGrid

            if (locks->IsLocked(x,z,isSoldier)) // do not copy through locked items
            {
              endIndex = i-1;
              fromCost = _path[endIndex]._cost;
              break;
            }
          }
        }

        if (allowReuse)
        {

          if (_path[endIndex]._type == NTGrid)
          {
            _from = _path.PosAtCost(fromCost); // interpolation enabled only in the free landscape
          }
          else
          {
            // in case of roads always select the previous node to make sure we are on the path
            const OperInfoResult &onPath = _path[endIndex];
            _from = onPath._pos;
            fromCost = onPath._cost;
            // adjust infoFrom to be on the road
          }

          if (endIndex > 0) dir = CalcDirection(_path[endIndex]._pos - _path[endIndex - 1]._pos);

          from = _from;
          _fromBusy = false;

          // if the difference is small and the copied path would be very short, do not copy it
          // but still adjust from
          if (fromCost-curCost>1e-3f)
          {
            // copy the segment of the path to _oldPath
            for (int i=0; i<_path.Size(); i++)
            {
              if (_path[i]._cost < curCost) continue; // skip the part before us
              if (_oldPath.Size() == 0)
              {
                // add the first node
                if (i == 0)
                {
                  _oldPath.Add(_path[0]);
                  continue;
                }
                if (_path[i]._cost > curCost)
                {
                  _oldPath.Add(_path[i - 1]);
                  _oldPath[0]._cost = 0;
                  _oldPath[0]._action = NULL;
                  // interpolate the position by cost
                  float denom = _path[i]._cost - _path[i - 1]._cost;
                  if (denom > 0)
                  {
                    float factor = (curCost - _path[i - 1]._cost) / denom;
                    _oldPath[0]._pos = (1.0f - factor) * _path[i - 1]._pos + factor * _path[i]._pos;
                  }
                }
              }
              if (_path[i]._cost < fromCost)
              {
                // add the intermediate node
                int index = _oldPath.Add(_path[i]);
                _oldPath[index]._cost -= curCost;
              }
              else
              {
                // add the last node
                Assert(i > 0);
                int index = _oldPath.Add(_path[i]);
                _oldPath[index]._cost = fromCost - curCost;
                _oldPath[index]._action = NULL;
                // interpolate the position by cost
                float denom = _path[i]._cost - _path[i - 1]._cost;
                if (denom > 0)
                {
                  float factor = (fromCost - _path[i - 1]._cost) / denom;
                  _oldPath[index]._pos = (1.0f - factor) * _path[i - 1]._pos + factor * _path[i]._pos;
                }
                break;
              }
            }
            _oldPath.Compact();
          }
        }
      }
#if DIAG_REUSE_PATH
      if (curCost < maxCost)
      {
        LogF(" - path section %.3f ... %.3f can be used", curCost, maxCost);
      }
      else
      {
        LogF(" - no path section can be used - %.3f >= %.3f", curCost, maxCost);
      }
#endif
    }
#if DIAG_REUSE_PATH
    else
    {
      LogF(" - no valid old path");
    }
#endif
#endif // REUSE_PATH
  }

  // starting and ending node info (this structure contains all needed info)
  OperInfo infoFrom, infoTo;

  // store the info about the road we are on for later use
  RoadNode roadNodeFrom(NULL, -1), roadNodeTo(NULL, -1);

  // check if moving from road / bridge
  if (veh->GetType()->CanUseRoads())
  {
    DoAssert(GLandscape->GetRoadNet()->IsOnRoadReady(from));
    float size = veh->GetShape() ? veh->GetShape()->GeometrySphere() : 0;
    RoadLink *road = unconst_cast(GLandscape->GetRoadNet()->IsOnRoad(from, size, size));
    if (road && !road->IsLocked(isSoldier))
    {
      roadNodeFrom = road->FindBestNode(isSoldier, vehWidth, from, veh->FutureVisualState().Direction());
      if (roadNodeFrom.IsValid())
      {
        // check if this is a bridge or the plain road
        if (road->IsBridge())
        {
          // force the end position on the bridge
          infoFrom._road = roadNodeFrom;
          infoFrom._type = NTBridge;
          from = roadNodeFrom.Position(isSoldier, vehWidth);
          _fromBusy = true; // use prepending to be sure vehicle is not far from the path
        }
        else
        {
          Vector3 roadPos = roadNodeFrom.Position(isSoldier,vehWidth);
          float abovePath = veh->GetCombatHeight();
          float dist2 = roadPos.DistanceXZ2(from-Vector3(0,abovePath,0));
          // Prepending removed when not necessary, final path looked strange often (acute angles)
          // when planning on road, place vehicle on road
          // when we have a perfect match, we will use it
          if (dist2<Square(0.05f))
          {
            infoFrom._road = roadNodeFrom;
            infoFrom._type = NTRoad;
            from = roadPos;
            _fromBusy = true; // use prepending to be sure vehicle is not far from the path
          }
        }
      }
    }
  }

  // check if moving to the house
  if (isSoldier && _house)
  {
    // target is the house position
    const IPaths *paths = _house->GetIPaths();
    if (paths)
    {
      // what position
      int posTo = paths->GetPos(_housePos);
      if (posTo < 0)
      {
        // try alternate (any) position
        posTo = paths->GetPos(0);
      }
      if (posTo >= 0)
      {
        // position found, store it
        infoTo._type = NTHouse;
        infoTo._house = _house;
        infoTo._housePos = posTo;
        // set the exact destination position
        _to = PlanPosition(paths->GetPosition(posTo),false);
      }
    }
  }
  // check if moving to road / bridge
  if (infoTo._type != NTHouse && veh->GetType()->CanUseRoads())
  {
    if (!GLandscape->GetRoadNet()->IsOnRoadReady(_to))
    {
      // wait with the planning until roads are loaded
      veh->PerformRelock(state);
      _state = InitFailed;
      return false;
    }
    
    // adjust ending node to be on the road net when possible
    float size = veh->GetShape() ? veh->GetShape()->GeometrySphere() : 0;
    RoadLink *road = unconst_cast(GLandscape->GetRoadNet()->IsOnRoad(_to, size, size));
    // Avoid planning to a locked road segment - IsDestinationFree will fail and the searching will be restarted repeatedly
    if (road && !road->IsLocked(isSoldier))
    {
      roadNodeTo = road->FindBestNode(isSoldier, vehWidth, _to, (_to - from).Normalized());
      // check if this is a bridge or the plain road
      if (road->IsBridge())
      {
        // force the end position on the bridge
        infoTo._road = roadNodeTo;
        infoTo._type = NTBridge;
        _to = PlanPosition(roadNodeTo.Position(isSoldier, vehWidth),false);
        _toRoad = true;
      }
      else if (!_lastPlan || roadNodeTo.Position(isSoldier,vehWidth).DistanceXZ2(_to)<OperItemGrid)
      {
        infoTo._road = roadNodeTo;
        infoTo._type = NTRoad;
        // when path end on this road, do not change target to _to
        _toRoad = true;
      }
      /*
      else if (veh->IsMovingInConvoy() && !isSoldier)
      {
        infoTo._road = roadNodeTo;
        infoTo._type = NTRoad;
        // when path end on this road, do not change target to _to
        _toRoad = true;
      }
      */
    }
  }


  // calculate the range of operative map
  float xsf, zsf, xef, zef;
  OperMapIndex xs(-1), zs(-1), xe(-1), ze(-1);
  LandIndex xMin(-1), xMax(-1), zMin(-1), zMax(-1);
  CalculateOperMapRange(xs, zs, xe, ze, xsf, zsf, xef, zef, xMin, xMax, zMin, zMax, from, _to);

  // create the operative map if possible
  if (!_map.OperFieldRangeReady(xMin, xMax, zMin, zMax))
  {
    // wait with the planning until objects are loaded
    veh->PerformRelock(state);
    _state = InitFailed;
    return false;
  }
  // we need the fields to be ready for pathing here
  _map.CreateMap(
    veh, xMin, zMin, xMax, zMax, MASK_AVOID_OBJECTS | MASK_PREFER_ROADS | MASK_USE_BUFFER | MASK_FOR_PATHING
  );
  Assert(_map._jobState == Job::JSNone || _map._jobState == Job::JSCanceled || _map._jobState == Job::JSDone);

#if 0
  // memory usage
  LogF("FindPath statistics:");
  LogF(
    "\t%s is searching path from %d, %d to %d,%d - distance %.0f",
    (const char *)veh()->GetType()->GetDisplayName(), xs, zs, xe, ze, sqrt(dist2));
  LogF(
    "\tusing %d fields, memory %d",
    _map._fields.Size(), _map._fields.Size() * sizeof(OperField));
  LogF(
    "\ttotal in cache %d(soldiers) + %d(others)",
    GLandscape->OperationalCacheSoldiers()->NFields() * sizeof(OperField),
    GLandscape->OperationalCache()->NFields() * sizeof(OperField));
#endif

  // check if starting place is accessible
  if (infoFrom._type != NTHouse && infoFrom._type != NTBridge && _map.GetFieldCost(xs, zs, true, vehType, CMAware, isSoldier, false).cost >= GET_UNACCESSIBLE)
  {
    if (isSoldier)
    {
      // first look if we are in some (passable) building
      if (!InsideBuildingReady(from))
      {
        veh->PerformRelock(state);
        _state = InitFailed;
        return false;
      }
      const IPaths *paths = InsideBuilding(from);
      if (paths)
      {
        Vector3 dummy;
        int posFrom = paths->FindNearestPoint(from - Vector3(0, combatHeight, 0), dummy);
        if (posFrom < 0)
        {
          // try alternate (any) position
          posFrom = paths->GetPos(0);
        }
        if (posFrom >= 0)
        {
          // position found, store it
          infoFrom._type = NTHouse;
          infoFrom._house = unconst_cast(paths->GetObject());
          infoFrom._housePos = posFrom;
        }
      }
    }
    if (infoFrom._type != NTHouse)
    {
      bool found = _map.FindNearestEmpty(xs, zs, xsf, zsf,
        OperMapIndex(xMin * OperItemRange), OperMapIndex(zMin * OperItemRange),
        OperMapIndex((xMax - xMin + 1) * OperItemRange), OperMapIndex((zMax - zMin + 1) * OperItemRange),
        true, vehType, isSoldier, false, DefFindFreePositionCallback, NULL);

      bool useRoad = false;
      if (found)
      {
        if (roadNodeFrom.IsValid())
        {
          Vector3 road = roadNodeFrom.Position(isSoldier, vehWidth);
          Vector3 nearest;
          nearest[0] = xs * OperItemGrid + 0.5 * OperItemGrid;
          nearest[1] = 0;
          nearest[2] = zs * OperItemGrid + 0.5 * OperItemGrid;
          useRoad = road.DistanceXZ2(from) < nearest.DistanceXZ2(from);
        }
      }
      else
      {
        // each fall-back is good enough
        useRoad = roadNodeFrom.IsValid();
      }

      if (useRoad)
      {
        infoFrom._road = roadNodeFrom;
        infoFrom._type = NTRoad;
        from = roadNodeFrom.Position(isSoldier, vehWidth);
        _fromBusy = true; // use prepending to be sure vehicle is not far from the path

        xs = OperMapIndex(toIntFloor(from.X() * InvOperItemGrid));
        zs = OperMapIndex(toIntFloor(from.Z() * InvOperItemGrid));
      }
      else if (found)
      {
        // mark the path is searched from a different position
        _fromBusy = true;
        /*
        // from must be changed (is forced into first path point)
        _from[0] = xs * OperItemGrid + 0.5 * OperItemGrid;
        _from[2] = zs * OperItemGrid + 0.5 * OperItemGrid;
        */
      }
      else
      {
        LogF("Unit %s: Start point not found", (const char *)GetDebugName());
#if LOG_PROBL
        _map.LogMap(xMin, xMax, zMin, zMax, xs, xe, zs, ze, false);
#endif
        _map.ClearMap();
        veh->PerformRelock(state);
        return false;
      }
    }
  }

  // check if ending place is accessible
  if (infoTo._type != NTHouse && infoTo._type != NTBridge && _map.GetFieldCost(xe, ze, true, vehType, CMAware, isSoldier, false).cost >= GET_UNACCESSIBLE)
  {
    if (isSoldier)
    {
      // first look if we are in some (passable) building
      if (!InsideBuildingReady(_to))
      {
        veh->PerformRelock(state);
        _state = InitFailed;
        return false;
      }
      const IPaths *paths = InsideBuilding(_to);
      if (paths)
      {
        Vector3 dummy;
        int posTo = paths->FindNearestPoint(_to - Vector3(0, combatHeight, 0), dummy);
        if (posTo < 0)
        {
          // try alternate (any) position
          posTo = paths->GetPos(0);
        }
        if (posTo >= 0)
        {
          // position found, store it
          infoTo._type = NTHouse;
          infoTo._house = unconst_cast(paths->GetObject());
          infoTo._housePos = posTo;
        }
      }
    }
    
    if (infoTo._type != NTHouse && (infoTo._type!=NTGrid || !_to._cover))
    {
      bool found = _map.FindNearestEmpty(xe, ze, xef, zef,
        OperMapIndex(xMin * OperItemRange), OperMapIndex(zMin * OperItemRange),
        OperMapIndex((xMax - xMin + 1) * OperItemRange), OperMapIndex((zMax - zMin + 1) * OperItemRange),
        true, vehType, isSoldier, false, DefFindFreePositionCallback, NULL);

      bool useRoad = false;
      if (found)
      {
        if (roadNodeTo.IsValid())
        {
          Vector3 road = roadNodeTo.Position(isSoldier, vehWidth);
          Vector3 nearest;
          nearest[0] = xe * OperItemGrid + 0.5 * OperItemGrid;
          nearest[1] = 0;
          nearest[2] = ze * OperItemGrid + 0.5 * OperItemGrid;
          useRoad = road.DistanceXZ2(_to) < nearest.DistanceXZ2(_to);
        }
      }
      else
      {
        // each fall-back is good enough
        useRoad = roadNodeTo.IsValid();
      }

      if (useRoad)
      {
        infoTo._road = roadNodeTo;
        infoTo._type = NTRoad;
        _to = PlanPosition(roadNodeTo.Position(isSoldier, vehWidth),false);
        _toRoad = true;
        xe = OperMapIndex(toIntFloor(_to.X() * InvOperItemGrid));
        ze = OperMapIndex(toIntFloor(_to.Z() * InvOperItemGrid));
      }
      else if (found)
      {
        // pos must be changed (is forced into last path point)
        // ignore vehicle locks for alternate targets - enable moving of soldiers near other soldiers or vehicles
        _to[0] = xe * OperItemGrid + 0.5 * OperItemGrid;
        _to[2] = ze * OperItemGrid + 0.5 * OperItemGrid;
        if (pos.Distance2(_expPosition) < Square(1))
        {
          // update also _expPosition to ensure unit reach "Complete" state
          _expPosition = PlanPosition(_to,_expPosition._cover);
        }
      }
      else
      {
        LogF("Unit %s: End point not found", (const char *)GetDebugName());
#if LOG_PROBL
        _map.LogMap(xMin - border, xMax + border, zMin - border, zMax + border, xs, xe, zs, ze, false);
#endif
        _map.ClearMap();
        veh->PerformRelock(state);
        return false;
      }
    }
  }

  // path inside the same field
  if (infoFrom._type != NTHouse && infoTo._type != NTHouse && xe == xs && ze == zs)
  {
    // trivial path
    _map._path.Resize(2);
      
    _map._path[0]._x = xs;
    _map._path[0]._z = zs;
    _map._path[0]._cost = 0;
    _map._path[0]._house = NULL;
    _map._path[0]._housePos = -1;
    _map._path[0]._road = infoFrom._road;
    _map._path[0]._type = infoFrom._type;
    
    int x = toIntFloor(from.X() * InvLandGrid);
    int z = toIntFloor(from.Z() * InvLandGrid);
    GeographyInfo info = GLOB_LAND->GetGeography(x, z);

    _map._path[1]._x = xe;
    _map._path[1]._z = ze;
    _map._path[1]._cost = floatMax(veh->GetBaseCost(info, false, true) * _to.DistanceXZ(from), 0.1); // avoid zero cost
    _map._path[1]._house = NULL;
    _map._path[1]._housePos = -1;
    _map._path[1]._road = infoTo._road;
    _map._path[1]._type = infoTo._type;

    // mark the path as not leading into any cover
    _map._pathToOpenCover = false;
    _map._pathToCover = CoverInfo();
    
    _map.SetAlternateGoal(infoTo._type != NTBridge);

    veh->PerformRelock(state);
    OnOperativePathFound();

    // FIX: clear after OperMap::ResultPath called from OnOperativePathFound (roads info is needed)
    _map.ClearMap();
    return true;
  }

  // path inside the house - the same position
  if (infoFrom._type == NTHouse && infoTo._type == NTHouse && infoFrom._house == infoTo._house && infoFrom._housePos == infoTo._housePos)
  {
    // trivial path
    _map._path.Resize(2);

    _map._path[0]._x = xs;
    _map._path[0]._z = zs;
    _map._path[0]._cost = 0;
    _map._path[0]._house = infoFrom._house;
    _map._path[0]._housePos = infoFrom._housePos;
    _map._path[0]._road = RoadNode(NULL, -1);
    _map._path[0]._type = NTHouse;

    _map._path[1]._x = xe;
    _map._path[1]._z = ze;
    _map._path[1]._cost = 0.1; // avoid zero cost
    _map._path[1]._house = infoTo._house;
    _map._path[1]._housePos = infoTo._housePos;
    _map._path[1]._road = RoadNode(NULL, -1);
    _map._path[1]._type = NTHouse;

    // mark the path as not leading into any cover
    _map._pathToOpenCover = false;
    _map._pathToCover = CoverInfo();
    
    _map.SetAlternateGoal(true);

    veh->PerformRelock(state);
    OnOperativePathFound();

    // FIX: clear after OperMap::ResultPath called from OnOperativePathFound (roads info is needed)
    _map.ClearMap();
    return true;
  }

  // finish the infoFrom, infoTo
  infoFrom._x = xs;
  infoFrom._z = zs;
  infoTo._x = xe;
  infoTo._z = ze;

  CoverDisposition dispo;
  dispo._maxCost = _maxCostToCover;
  dispo._maxDelay = _maxDelayByCover;
  GetHideFrom(dispo._hideFromL,dispo._hideFromR);
  if (!inFormation)
  {
    // for the formation leader or an agent we assume we are moving to a planned destination
    dispo._movingTo = pos;
  }
  else 
  {
    Vector3Val moveDir = thisUnit->GetSubgroup()->GetMoveDirection();
    float offset = veh->GetType()->GetFormationZ()* 10; // GetFormationZ() is 5 m for a soldier in Arma 2
    dispo._movingTo = PlanPosition(thisUnit->GetFormationAbsolute()+moveDir*offset,false);
  }

  if (_map.InitFindPath(this, infoFrom, infoTo, dir, true, dispo))
  {
    // simple path found
    veh->PerformRelock(state);
    OnOperativePathFound();
    return true;
  }

  SetState(Planning);
  veh->PerformRelock(state);
  return false;
}

/*!
\patch 5101 Date 12/12/2006 by Jirka
- Fixed: AI pathfinding improved (unexpected leaving of roads will happen less frequently)
*/

void AIBrain::OnOperativePathFound()
{
  EntityAI *veh = GetVehicle();
  AILockerSavedState state;
  veh->PerformUnlock(&state);

  _map.ResultPath(this,_fromBusy,_from);
  ++_pathId;

  #if 0 // _PROFILE
  if (GetUnit() && GetUnit()->ID()==2)
  {
    LogF("%s: found, id %d",cc_cast(GetDebugName()),_pathId);
  }
  #endif
  
  // 
  if (
    _lastPlan && _map.IsAlternateGoal() && _map.CanReplanLater()
    && !_to._cover // planning into cover: whatever we find is the final destination, we will never find a better one
  )
  {
    // real destination was not found yet, we need to prevent command completing
    // one exception is when we fit the requirements given by the command
    if (_wantedPrecision>0 && _path.Size()>=2 && _plannedPosition.Distance2(_path.Last()._pos)<Square(_wantedPrecision))
    {
      // even when alternate, the path is precise enough for the current command
    }
    else
    {
      // this will make sure next planning is done later
      // note: this is used for cover as well, not only for alternate destinations
      _lastPlan = false;
    }
  }



#if DIAG_REUSE_PATH
  LogF("%.3f Path searching for %s finished", Glob.time.toFloat(), cc_cast(GetDebugName()));
  LogF(" - current position %.2f, %.2f", Position().X(), Position().Z());
  LogF(" - planned from %.2f, %.2f", _from.X(), _from.Z());
#endif

#if REUSE_PATH
  int oldSize = _oldPath.Size();
  if (oldSize > 0 && _path.Size() > 0)
  {
    // check for shortcut
    for (int i=0; i<oldSize - 1; i++)
    {
      if (_oldPath[i]._pos.Distance2(_path[0]._pos) < 0.001f)
      {
        oldSize = i + 1;
        break;
      }
    }

    if (_oldPath[0]._pos.Distance2(_path[0]._pos)<Square(0.1f))
    {
      LogF("Reusing strange path - singularity");
    }
    else if (_oldPath[oldSize-1]._pos.Distance2(_path[0]._pos)<Square(0.1f))
    {
      //LogF("Reusing created a singular path (%s)",cc_cast(GetDebugName()));
    }
    
    float prefixCost = _oldPath[oldSize - 1]._cost;
    _path.InsertMultiple(0, oldSize - 1); // avoid duplicity
    for (int i=0; i<oldSize-1; i++) _path[i] = _oldPath[i];
    for (int i=oldSize-1; i<_path.Size(); i++) _path[i]._cost += prefixCost;

#if DIAG_REUSE_PATH
    LogF(" - part of old path used:");
    for (int i=0; i<oldPath.Size(); i++)
    {
      LogF("   Position %.2f, %.2f; cost %.3f", oldPath[i]._pos.X(), oldPath[i]._pos.Z(), oldPath[i]._cost);
    }
#endif
  }
#endif

  float combatHeight = veh->GetCombatHeight();

  // FIX: avoid moving of path origin at all - can cause problems when vehicle moves during the planning
  /*
  // move path origin - cost recalculation is needed
  // FIX: do not recalculate if unit starts in house
  if (_path.Size() >= 2 && !_path[1]._house)
  {
    float originalDistance = _path[1]._pos.Distance(_path[0]._pos);
    _path[0]._pos = GLandscape->PointOnSurface(_from[0], combatHeight, _from[2]);
    float newDistance = _path[1]._pos.Distance(_path[0]._pos);
    if (originalDistance > 0)
    {
      float costChange = _path[1]._cost * (newDistance - originalDistance) / originalDistance;
      for (int i=1; i<_path.Size(); i++)
        _path[i]._cost += costChange;
    }
    else
    {
      int x = toIntFloor(_from.X() * InvLandGrid);
      int z = toIntFloor(_from.Z() * InvLandGrid);
      GeographyInfo info = GLOB_LAND->GetGeography(x, z);
      float costChange = veh->GetCost(info) * newDistance - _path[1]._cost;
      for (int i=1; i<_path.Size(); i++)
        _path[i]._cost += costChange;
    }
  }
  */

  // move path target - cost recalculation is needed
  if (!_map.IsAlternateGoal())
  {
    int last = _path.Size() - 1;
    Assert(last > 0);
    // do not force the path target when moving to house or want to stay on road
    if (last > 0 && !_path[last]._house && !(_toRoad && _path[last]._type == NTRoad))
    {
      float originalDistance = _path[last]._pos.Distance(_path[last - 1]._pos);
      Vector3Val toPos = GLandscape->PointOnSurface(_to[0], combatHeight, _to[2]);
#if _ENABLE_REPORT
      float distanceMoved = _path[last]._pos.Distance(toPos);
      if (distanceMoved>5)
      {
        LogF(
          "%s: path end point moved %.1f m: %.1f,%.1f to %.1f,%.1f",
          cc_cast(GetDebugName()),distanceMoved,_path[last]._pos.X(),_path[last]._pos.Z(),toPos.X(),toPos.Z()
        );
      }
#endif
      _path[last]._pos = toPos;
      float newDistance = _path[last]._pos.Distance(_path[last - 1]._pos);
      if (originalDistance > 0)
      {
        float costChange = (_path[last]._cost - _path[last - 1]._cost) * (newDistance - originalDistance) / originalDistance;
        _path[last]._cost += costChange;
      }
      else
      {
        // insert some non-zero cost
        float costLast = newDistance * veh->GetType()->GetMinCost();
        _path[last]._cost = _path[last - 1]._cost + costLast;
      }
    }
  }
  /**/

  const float costReplanAbs = 10.0f;
  const float costReplanRel = 0.45f;
  const float costForceReplanAbs = 2.0f;
  const float costForceReplanRel = 0.05f;

  int n = _path.Size();
  if (n >= 2)
  {
    float totalCost = _path[n - 1]._cost;
    // when planning into a cover or into a last position, the whole path is valid
    if (_lastPlan || _path.IsIntoCover())
    {
      /*
      LogF("*** Last plan limit %.2f / %.2f", totalCost, totalCost);
      */
      SetPathCosts(totalCost, totalCost);
    }
    else
    {
      // mark end of the path so that we start replanning soon enough
      float costReplan = totalCost - floatMin(costReplanAbs, totalCost * costReplanRel);
      float costForceReplan = totalCost - floatMin(costForceReplanAbs, totalCost * costForceReplanRel);
      /*
      LogF("*** New limit max(%.2f, %.2f) = %.2f / max(%.2f, %.2f) = %.2f",
      totalCost - costReplanAbs, totalCost * (1.0f - costReplanRel), costReplan,
      totalCost - costForceReplanAbs, totalCost * (1.0f - costForceReplanRel), costForceReplan);
      */
      SetPathCosts(costReplan, costForceReplan);
    }
  }
  else
  {
    RptF(
      "Error %s: Invalid path from [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f].",
      (const char *)GetDebugName(),
      _from.X(), _from.Y(), _from.Z(),
      _to.X(), _to.Y(), _to.Z());
    SetPathCosts(0, 0);
  }

  // ????????
  if (_map.IsAlternateGoal())
  {
    // searching path to _expPosition?
    // DoAssert(_expPosition.Distance2(_to) < 1); - _to may be changed
    // update _expPosition to avoid state switching loop
    int last = _path.Size() - 1;
    _expPosition = PlanPosition(_path[last]._pos,_expPosition._cover);
  }
  // ????????

  _map.ClearMap();
  veh->PerformRelock(state);

  DoAssert (VerifyPath());
}

void AIBrain::OnOperativePathNotFound()
{
  LogF("Unit %s: Operative path not found", (const char *)GetDebugName());
#if LOG_PROBL
  _map.LogMap(xMin, xMax, zMin, zMax, xs, xe, zs, ze, false);
#endif
  _map.ClearMap();
}

bool AIBrain::ObstacleFree(const CoverInfo &object ) const
{
  AIGroup *grp = GetGroup();
  if (!grp) return true;

  for( int u=0; u<grp->NUnits(); u++ )
  {
    AIUnit *un = grp->GetUnit(u);
    if( !un || un==this || !un->IsUnit() ) continue;
    // check if any unit has obstacle allocated
    EntityAIFull *veh = un->GetVehicle();
    if (veh==GetVehicle()) continue;
    // two different hiding mechanism (vehicle and unit level) - check both of them
    if (veh->IsOccupyingCover(object))
    {
      return false;
    }
    if (un->IsAnyPlayer())
    {
      // player do not allocate - check if some player is very close or moving close
      Vector3Val pos = veh->FutureVisualState().Position();
      Vector3Val objPos = object.Position();
      Vector3 predict = pos+veh->PredictVelocity()*3.0f;
      if(NearestPointDistance(pos,predict,objPos)<3.0f)
      {
        // player unit is very close
        return false;
      }
    }
  }
  return true;
}

#endif

void AIBrain::RemoveAction(const PathAction *action)
{
  for (int i=0; i<_path.Size(); i++)
  {
    if (_path[i]._action == action)
    {
      _path[i]._action = NULL;
      break;
    }
  }
}

/*!
\patch 5137 Date 3/7/2007 by Bebul
- Fixed: Freelook still forced when changing from cargo to secondary turret 
*/
/// Functor changing the AIBrain::_state of gunner
class SetGunnerStateFunc : public ITurretFunc
{
protected:
  AIBrain::State _state;

public:
  SetGunnerStateFunc(AIBrain::State state) : _state(state) {}
  virtual bool operator ()(EntityAIFull *entity, TurretContext &context)
  {
    if (!context._gunner || !context._turret) return false; // continue
    AIBrain *unit = context._gunner->Brain();
    if (unit)
    {
      //if (state == Wait) ClearOperativePlan();
      unit->_state = _state;
    }
    return false; // for each    
  }
};

/// Functor clearing operative map of gunner
class ClearGunnerMapFunc : public ITurretFunc
{
public:
  ClearGunnerMapFunc() {}
  virtual bool operator ()(EntityAIFull *entity, TurretContext &context)
  {
    if (!context._gunner || !context._turret) return false; // continue
    AIBrain *unit = context._gunner->Brain();
    if (unit) unit->_map.ClearMap();
    return false; // for each    
  }
};

bool AIBrain::SetState(State state, bool vehChanged)
{
#if _ENABLE_REPORT
  if (_state == Stopping && state != Stopping && state!=Stopped)
  {
    RptF("# Vehicle %s - stopping state changed to %s", cc_cast(GetVehicle()->GetDebugName()), cc_cast(FindEnumName(state)));
  }
#endif

#if DIAG_A_STAR
  bool diags = GWorld->CameraOn() == GetVehicle();
  if (diags && (state == Wait || state == Replan))
  {
    LogF(
      "%.2f %s operative path replan needed",
      Glob.time.toFloat(), cc_cast(GetVehicle()->GetDebugName()));
  }
#endif

  if (_state == Planning && state != Planning)
  {
    // planning was interrupted, delete working space
    if (_inVehicle)
    {
      AIBrain *unit;
      unit = _inVehicle->PilotUnit();
      if (unit) unit->_map.ClearMap();
      ClearGunnerMapFunc clearGunnerMap;
      _inVehicle->ForEachTurret(clearGunnerMap);
    }
    else
    {
      _map.ClearMap();
    }
  }

  if (state == Completed)
  {
#if DIAG_WANTED_POSITION
if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == GetVehicle())
{
  LogF(
    "Unit %s: state changed to completed",
    cc_cast(GetDebugName()));
}
#endif

    if (_planningMode == DoNotPlan || _planningMode==DoNotPlanFormation) return true;

    if (_completedReceived) return true;
    _completedReceived = true;
    //LogF("Unit %s: Completed received", (const char *)GetDebugName());

    /*
    if (IsSubgroupLeaderVehicle() && GetSubgroup()->HasCommand())
    {
    Command *cmd = GetSubgroup()->GetCommand();
    LogF("%s: Completed (%d)", (const char *)GetDebugName(), _planningMode);
    LogF("  position: %.0f, %.0f", Position().X(), Position().Z());
    LogF("  planned: %.0f, %.0f", _plannedPosition.X(), _plannedPosition.Z());
    LogF("  wanted: %.0f, %.0f", _wantedPosition.X(), _wantedPosition.Z());
    LogF("  command: %.0f, %.0f", cmd->_destination.X(), cmd->_destination.Z());
    if (cmd->_destination.Distance2(_wantedPosition) > Square(1))
    {
    Fail("  Error !!!");
    }
    if (cmd->_destination.Distance2(Position()) > Square(10))
    {
    Fail("  Error !!!");
    }
    }
    */
  }


  if (state == InCargo || state == Planning)
  {
    _state = state;
    return true;
  }

  // all units in the vehicle should share the state
  if (_inVehicle)
  {
    
    AIBrain *unit = _inVehicle->PilotUnit();
    if (unit && unit->GetState()!=AIUnit::Stopping && (unit->GetState()!=AIUnit::Stopped || _inVehicle->CanCancelStop()))
    {
      //if (state == Wait) ClearOperativePlan();
      unit->_state = state;
    }
    
    SetGunnerStateFunc func(state);
    _inVehicle->ForEachTurret(func);

    // lines above may set state for other units in the vehicle, but may skip "this"
    // we need to make sure we always set it for the one we are calling it for
    _state = state;
  }
  else
  {
    if (!vehChanged)
    {
      Assert(_state != InCargo);
      if (_state == InCargo) return false;
    }
    //if (state == Wait) ClearOperativePlan();
    _state = state;
  }
  return true;
}



bool AIBrain::IsPlanning() const
{
  return _noPath || _updatePath || _state == Init || _state == Planning || _state == Delay;
}

void AIBrain::UpdateOperativePlan()
{
  if (IsAnyPlayer()) return;
#if DIAG_A_STAR
  bool diags = GWorld->CameraOn() == GetVehicle();
  if (diags)
  {
    LogF(
      "%.2f %s operative path replan forced",
      Glob.time.toFloat(), cc_cast(GetVehicle()->GetDebugName()));
  }
#endif

  ClearOperativePlan();
  if (_state != Planning && _state!=InCargo) SetState(Wait); // if planning is already in progress, finish it
}

void AIBrain::OnStepTimedOut()
{
  if (_state == Stopping)
  {
    // Get out/land/get in cannot be processed
    Transport *veh = dyn_cast<Transport>(GetVehicle());
    Assert(veh);
    if (veh)
    {
      // if anyone wanted to get out, we cancel the get out order
      veh->WhoIsGettingOut().Clear();
      // we cancel the stopping procedure if possible
      if (veh->CanCancelStop())
      {
        AIBrain *commander = veh->CommanderUnit();
        if (commander) commander->SetState(Wait); // valid pass Stopped -> Wait
      }
    }
    else
    {
      Verify(SetState(Wait)); // TODO: BUG
    }
  }
  else if (_state == AIUnit::Busy)
  {
    // in analogy to OnStepCompleted, we want to avoid replanning for formation members
    if (_planningMode!=FormationPlanned)
    {
      Verify(SetState(Replan));
    }
  }
}

void AIBrain::OnStepCompleted()
{
  if (_state == Stopping)
  {
    SetState(Stopped);
    Transport *veh = dyn_cast<Transport>(GetVehicle());
    if (veh)
    {
      veh->LandFinished();

      veh->WhoIsGettingOut().RemoveNulls();
      int n = veh->WhoIsGettingOut().Size();
      AIUnit *unit = NULL;
      if (n > 0)
      {
        for (int i=0; i<n; i++)
        {
          AIUnit *u = veh->WhoIsGettingOut()[i];
          if (u->IsInCargo())
          {
            unit = u;
            break;
          }
        }
        if (!unit) for (int i=0; i<n; i++)
        {
          AIUnit *u = veh->WhoIsGettingOut()[i];
          if (u->IsGunner())
          {
            unit = u;
            break;
          }
        }
        if (!unit) for (int i=0; i<n; i++)
        {
          AIUnit *u = veh->WhoIsGettingOut()[i];
          if (u->IsCommander())
          {
            unit = u;
            break;
          }
        }
        if (!unit)
          unit = veh->WhoIsGettingOut()[0];
        Assert(unit);
        if (unit && veh->PrepareGetOut(unit))
        {
          veh->SetGetOutTime(Glob.time + 2.0);
          veh->StartGetOutActivity(unit,false);
        }
      }
    }
  }
  else if (_state == AIUnit::Busy)
  {
    // Direct pilot cannot complete path planned for command
    Assert (_planningMode != LeaderDirect);
    if (_lastPlan)
    {
      // no lastPlan handling neededf for formation planning
      if (_planningMode!=AIBrain::FormationPlanned)
      {
        EntityAI *veh = GetVehicle();
        float precision = veh->GetPrecision();
        if (_wantedPosition.Distance(_plannedPosition) > Square(precision))
        {
          ForceReplan();
        }
        else
        {
          Vector3 pos = _path[_path.Size() - 1]._pos;
          if (pos.DistanceXZ2(_expPosition) > Square(1))
          {
            // try to find better path
            _iter = 0;
            Verify(SetState(Wait));
          }
          else
          {
            Verify(SetState(Completed));
          }
        }
      }
    }
    else
    {
      // even for formation movement we need to replan when the plan did not lead into the destination
      // _plannedPosition will prevent issuing a new planning to the same position, but the unit will never get there
      Verify(SetState(Replan));
    }
  }
}

bool AIBrain::IsFireEnabled(Target *tgt) const
{
  // check if fire enabled at given target
  if (!IsHoldingFire()) return true;
  if (tgt && tgt==GetEnableFireTarget()) return true;
  return false;
}

void AIBrain::Disclose(DangerCause cause, Vector3Par position, EntityAI *causedBy, bool discloseGroup)
{
  // no effect on player group
  if (IsPlayer()) return;
  if (!IsLocal()) return;
  // dead person can no longer disclose
  if (GetPerson() && GetPerson()->IsDamageDestroyed()) return;
  
  AIGroup *grp = GetGroup();
  if (grp)
  {
    // unit was disclosed - do something
    // do not execute danger reaction for fleeing groups
    // changed in ArmA: in OFP fleeing groups never opened fire
    if (!grp->GetFlee())
    {
      Target *tgt = grp->GetTargetList().FindTarget(causedBy);
      SetDanger(cause, position, tgt);
    }
    // do not perform whole group disclose if player is the leader
    // changed in ArmA: in OFP following code was not executed for player's subordinates
    // as disclose detection for enemy near is now much more reliable
    // we let even player's subordinates opening fire when they realize enemy has disclosed them
    if (!grp->IsAnyPlayerGroup() || cause==DCEnemyNear)
    {
      if( discloseGroup ) grp->Disclose(cause, GetUnit(), NULL, causedBy);
      if (_semaphore != SemaphoreBlue)
      {
        _semaphore = ApplyOpenFire(_semaphore, OFSOpenFire);
      }
    }
  }
  else
  {
    const TargetList *tgtList = AccessTargetList();
    const Target *tgt = tgtList ? tgtList->FindTarget(causedBy) : NULL;
    SetDanger(cause, position, tgt, GRandGen.PlusMinus(1.0f, 0.2f));
  }
}

/// "interest" value relative to given unit
float HowMuchInteresting(AIBrain *unit, const Target *tgt)
{
  EntityAI *veh = unit->GetVehicle();
  Vector3Val unitDir = veh->FutureVisualState().Direction();
  Vector3Val unitPos = veh->FutureVisualState().Position();
  float interesting = tgt->HowMuchInteresting();
  // we get some points when unknown
  TargetSide side = tgt->GetSide();
  if (side==TSideUnknown) interesting += 10;
  // we get many points for being enemy
  if (unit->IsEnemy(side)) interesting += 50;
  // we loose a lot for being out of natural focus area
  Vector3 tgtDir = tgt->GetPosition()-unitPos;
  float cosAlpha = tgtDir.CosAngle(unitDir);
  float focus = floatMax(cosAlpha,0.2);
  return interesting * focus;
}

Target *SelectInterestingTarget(AIBrain *unit, Target *thn)
{
  PROFILE_SCOPE_EX(aiSIT,ai);
  Vector3Val uPos = unit->Position(unit->GetFutureVisualState());

  if (unit->GetCombatMode()>=CMCombat)
  {
    // in combat we should be interested to see what other members of our group are doing
    // this is particularly true for the leader
    AIGroup *grp = unit->GetGroup();
    if (grp)
    {
      // scan all units periodically, each around 3 sec
      const int timePerUnit = 6*1024; // time in ms - pow(2) for fast division
      const int timeForEnemy = unit==grp->Leader() && unit->GetTargetAssigned()==NULL ? 3*1024 : 4*1024;
      // in between watch enemies
      int cycleMs = (Glob.time.toInt()+unit->RandomSeed()*7)%(timePerUnit*grp->NUnits());
      int index = cycleMs/timePerUnit;
      int cycleInUnit = cycleMs-index*timePerUnit;
      if (cycleInUnit>timeForEnemy)
      {
        AIUnit *other = grp->GetUnit(index);
        if (other==unit)
        {
          // instead of looking at yourself look around (watch no target)
          return NULL;
        }
        if (other)
        {
          EntityAIFull *veh = other->GetVehicle();
          // we should watch the vehicle the unit is sitting in
          // but only if it is different from our vehicle
          if (veh!=unit->GetVehicle())
          {
            return unit->FindTarget(veh,false);
          }
        }
      }
    }
  }

  // find something known that is near
  const TargetList *list = unit->AccessTargetList();
  if (!list) return NULL;
  // never select target less interesting than certain limit
  float thnInteresting = 20;
  float thnDist = 50;
  if (thn)
  {
    // increase old target interest to maintain some stability
    thnInteresting = HowMuchInteresting(unit,thn)*3.0f;
    thnDist = uPos.Distance(thn->GetPosition());
  }

  // scan enemies - we are interested only in a few most important enemy targets
  {
    int maxTargets = 6;
    for (int i=0; i<list->EnemyCount(); i++)
    {
      TargetNormal *tgt = list->GetEnemy(i);
      if (tgt->idExact && tgt->idExact->Static()) continue;
      if (tgt->idExact==unit->GetPerson()) continue;
      if (tgt->idExact==unit->GetVehicleIn()) continue;
      if (!tgt->IsKnownBy(unit)) continue;
      if (tgt->type->IsKindOf(GWorld->Preloaded(VTypeStatic))) continue;
      if (tgt->idExact && tgt->idExact->IsArtilleryTarget()) continue;
      // check if we are allowed to check more targets
      if (tgt->vanished) continue;
      if (--maxTargets<0) break;

      float tgtDist = tgt->position.Distance(uPos);
      float tgtInteresting = HowMuchInteresting(unit,tgt);
      if (tgtInteresting*thnDist>thnInteresting*tgtDist)
      {
        thn = tgt;
        thnDist = tgtDist;
        thnInteresting = tgtInteresting;
      }
    }
  }

  if (unit->GetCombatMode()<CMCombat)
  {
    // not combat - we are interested only in a few most important friendly targets
    // we want to track more targets in cut-scenes, for more reliable behaviour
    int maxTargets = GWorld->CheckCutscene() ? 20 : 10;
    for (int i=0; i<list->FriendlyCount(); i++)
    {
      TargetNormal *tgt = list->GetFriendly(i);
      if (tgt->idExact && tgt->idExact->Static()) continue;
      if (tgt->idExact==unit->GetPerson()) continue;
      if (tgt->idExact==unit->GetVehicleIn()) continue;
      if (!tgt->IsKnownBy(unit)) continue;
      if (tgt->idExact && tgt->idExact->IsArtilleryTarget()) continue;
      if (tgt->vanished) continue;
      if (tgt->type->IsKindOf(GWorld->Preloaded(VTypeStatic))) continue;
      // check if we are allowed to check more targets
      if (--maxTargets<0) break;

      float tgtDist = tgt->position.Distance(uPos);
      float tgtInteresting = HowMuchInteresting(unit,tgt);
      if (tgtInteresting*thnDist>thnInteresting*tgtDist)
      {
        thn = tgt;
        thnDist = tgtDist;
        thnInteresting = tgtInteresting;
      }
    }
  }
  return thn;
}

Vector3 AIBrain::ScanTargetOffset(const Target * tgt) const
{
  if (IsAnyPlayer())
  {
    // for a player a different visualization is needed
    return VZero;
  }
  float accuracy = tgt->PositionAccuracy(this);
  // TODO: limit might be FOV based here
  if (accuracy>5.0f)
  {
    // perform time based area scan
    // circle around the area, this converted to 1D
    // should give an impression of sweeping left and right

    const int duration = 31000;
    float dir = Glob.time.ModMs(duration)*(1.0f/duration);

    // per unit randomization
    dir += (RandomSeed()&0xf)*(1.0f/15);

    // convert number to angle (note: some approximation could work well)
    float x = sin(dir*H_PI*2);
    float z = cos(dir*H_PI*2);

    // consider: we may want follow terrain with the eye target
    return Vector3(x*accuracy,0,z*accuracy);
  }
  return VZero;
}

/*!
  \patch 5092 Date 11/29/2006 by Ondra
  - Fixed: AI more actively scanning visually for enemy when his position is not known.
*/
Vector3 AIBrain::ScanTarget(Target * tgt)
{
  return tgt->LandAimingPosition(this)+ScanTargetOffset(tgt);
}

void AIBrain::WatchTarget()
{
  _watchPos = ScanTarget(_watchTgt);

  // we assume we watch from the head (i.e. eye) of the target
  _watchDir = _watchPos - GetVehicle()->EyePosition(GetVehicle()->FutureVisualState());


  _watchDir.Normalize();
  _watchDirHead = _watchDir;
}

void AIBrain::WatchFormation()
{
  _watchDirHead = _watchDir = Direction(GetFutureVisualState());
}

void AIBrain::GlanceRandomDirection()
{
  // we need to set watch direction as well (similar to GlanceAt)
  // we know there is no target, otherwise GlanceAt would be called
  // therefore we can always call WatchFormation
  WatchFormation();

  if ((_disabledAI&DAAutoTarget)==0)
  {
    // we base our direction on time, so that it is continuous
    const int duration = 20000;
    float dir = Glob.time.ModMs(duration)*(1.0f/duration);

    // add some randomization, so that all units do not perform the same movement
    dir += (RandomSeed()&0xf)*(1.0f/15);

    dir = fabs(dir-0.5f)*2;

    Vector3 relDir(((dir-0.5f))*3,0,1);
    relDir.Normalize();
    _watchDirHead = GetVehicle()->FutureVisualState().DirectionModelToWorld(relDir);
  }
  else
  {
    _watchDirHead = GetVehicle()->FutureVisualState().Direction();
  }
}

void AIBrain::GlanceAt(Target *tgt)
{
  if (_watchTgt)
  {
    WatchTarget();
  }
  else
  {
    WatchFormation();
  }
  // if auto targeting is disabled, we must not be curious
  // mission designer has full control over us

  if (tgt)
  {
    // we see something interesting - watch it
    _glanceTgt = tgt;
    // if target position is not accurate, scan the area where the target could be

    _glancePos = ScanTarget(_glanceTgt);
    if (tgt->idExact)
    {
      // make correction for different aiming/camera position
      if (_glancePos.Distance2(tgt->idExact->AimingPosition(tgt->idExact->FutureVisualState()))<0.5f)
      {
        _glancePos = tgt->idExact->EyePosition(tgt->idExact->FutureVisualState());
      }
    }
    _watchDirHead = _glancePos - GetVehicle()->EyePosition(GetVehicle()->FutureVisualState());
    _watchDirHead.Normalize();
  }
}

/*!
\patch 1.52 Date 4/20/2002 by Ondra
- New: All men are now interested in what is happening around them.
*/

void AIBrain::SetWatch()
{
  // consider formation and watch mode
  if (!IsPlayer())
  {
    // we might have started watching before getting in
    if (_glanceTgt.IdExact()==GetVehicle()) _glanceTgt = NULL;
    if (_watchTgt.IdExact()==GetVehicle()) _watchTgt = NULL;
    if (_glanceTgt.IdExact() && !_glanceTgt.IdExact()->IsInLandscape()) _glanceTgt = NULL;
    if (_watchTgt.IdExact() && !_watchTgt.IdExact()->IsInLandscape()) _watchTgt = NULL;
    if (_watchTgt.IdExact() && _watchTgt.IdExact()->IsArtilleryTarget()) _watchTgt = NULL;
    if (_glanceTgt.IdExact() && _glanceTgt.IdExact()->IsArtilleryTarget()) _glanceTgt = NULL;

    switch (_watchMode)
    {
    case WMAround:
      {
        float time = Glob.time - _watchDirSet;
        Matrix3 rotY(MRotationY, time *((H_PI*2)/30));
        _watchDirHead = rotY * _watchDirHead;
        _watchDirHead[1] = 0; // TODO: adapt to terrain around us
        _watchDirHead.Normalize();
        _watchDirSet = Glob.time;
        // change body direction in large steps to prevent small movements which look ugly
        const float cos60 = 0.5f;
        if (_watchDir.DotProduct(_watchDirHead)<cos60 || !IsFreeSoldier())
        {
          _watchDir = _watchDirHead;
        }
      }
      break;
    case WMPos:
      _watchDir = _watchPos - GetVehicle()->EyePosition(GetVehicle()->FutureVisualState());
      _watchDir.Normalize();
      _watchDirHead = _watchDir;
      break;
    case WMPosGlance:
      if (_watchTgt)
      {
        WatchTarget();
      }
      else
      {
        WatchFormation();
      }
      _watchDirHead = _glancePos - GetVehicle()->EyePosition(GetVehicle()->FutureVisualState());
      _watchDirHead.Normalize();
      break;
    case WMTgt:
      if (_watchTgt)
      {
        WatchTarget();
        break;
      }

      // fall trough from case WMTgt to case WMNo
    case WMNo:
      {
        // if auto targeting is disabled, we must not be curious
        // mission designer has full control over us
        Target *tgt = NULL;
        if ((_disabledAI&DAAutoTarget)==0)
        {
          // when soldier is idle, automatically select "interesting" targets
          tgt = _glanceTgt;
          if (!tgt) tgt = _watchTgt;
          tgt = SelectInterestingTarget(this, tgt);
        }
        if (tgt)
        {
          GlanceAt(tgt);
        }
        else
        {
          GlanceRandomDirection();
        }
        break;
      }
    case WMTgtGlance:
      GlanceAt(_glanceTgt);
      break;
    }
  }
}

#if _ENABLE_CHEATS

void AIBrain::DebugFSM()
{
#if DEBUG_FSM
  for (int i=0; i<NBehaviourFSMRole; i++)
  {
    if (_behaviourFSMs[i]) _behaviourFSMs[i]->Debug();
  }
  if (_reactionFSM) _reactionFSM->Debug();

# if _ENABLE_CONVERSATION
    if (_conversationFSM) _conversationFSM->Debug();
# endif

#endif
}

void AIBrain::DebugFSM(int level)
{
#if DEBUG_FSM
  _fsmAutoDebug = level;
#endif
}


#endif

LSError AIBrain::Serialize(ParamArchive &ar)
{
  if (ar.IsSaving())
  {
    RString type = GetAgentType();
    CHECK(ar.Serialize("agentType", type, 1))
  }

  // structure
  if (ar.IsSaving())
  {
    CHECK(ar.SerializeRef("Person", _person, 1))
  }
  else if (ar.GetPass() == ParamArchive::PassSecond)
  {
    OLinkPermO(Person) person;
    CHECK(ar.SerializeRef("Person", person, 1))
    SetPerson(person);
  }
  CHECK(ar.SerializeRef("InVehicle", _inVehicle, 1))

  CHECK(ar.SerializeRef("RemoteControlled", _remoteControlled, 1))

  CHECK(ar.SerializeArray("objectIdHistory", _objectIdHistory, 1))

  // info
  //  CHECK(ar.Serialize("speaker", _speaker, 1, -1))
  if (ar.IsSaving())
  {
    if (_person)
      CHECK(ar.Serialize("Info", _person->GetInfo(), 1))
  }
  else if (ar.GetPass() == ParamArchive::PassSecond)
  {
    if (_person)
    {
      AIUnitInfo &info = _person->GetInfo();
      ar.FirstPass();
      CHECK(ar.Serialize("Info", info, 1))
      ar.SecondPass();
      CHECK(ar.Serialize("Info", info, 1))

      if (info._face.GetLength() > 0)
        _person->SetFace(info._face);

      if (info._glasses.GetLength() > 0)
        _person->SetGlasses(info._glasses);

      if (info._speaker.GetLength() > 0)
        SetSpeaker(info._speaker, info._pitch);
    }
  }
#if !_ENABLE_IDENTITIES
  CHECK(ar.Serialize("ability", _ability, 1, 0.2))
#endif

  CHECK(ar.SerializeRef("VehicleAssigned", _vehicleAssigned, 1))
  CHECK(ar.SerializeEnum("semaphore", _semaphore, 1, SemaphoreYellow))
  CHECK(ar.SerializeEnum("combatModeMajor", _combatModeMajor, 1, CMAware))
  CHECK(::Serialize(ar, "dangerUntil", _dangerUntil, 1, Time(0)))
  CHECK(ar.Serialize("captive", _captive, 1, 0))

  CHECK(ar.SerializeEnum("lifeState", _lifeState, 1, LifeStateAlive))
  CHECK(ar.Serialize("disabledAI", _disabledAI, 1, 0));

  CHECK(ar.SerializeRef("TargetAssigned", _targetAssigned._tgt, 1))
  CHECK(ar.SerializeEnum("TargetAssignedState", _targetAssigned._initState, 1, TargetFireMin))
  CHECK(ar.SerializeRef("TargetEnableFire", _targetEnableFire, 1))

  CHECK(ar.SerializeRefs("SynchronizedObjects", _synchronizedObjects, 1))

  CHECK(ar.Serialize("nearestEnemyDist2", _nearestEnemyDist2, 1))

  // actual instructions
  CHECK(ar.Serialize("Path", _path, 1))
  CHECK(ar.SerializeRef("House", _house, 1))
  CHECK(ar.Serialize("housePos", _housePos, 1, -1))
  CHECK(ar.Serialize("pathId", _pathId, 1, 0))

  CHECK(::Serialize(ar, "wantedPositionWanted", safe_cast<Vector3 &>(_wantedPositionWanted), 1, VUndefined))
  CHECK(::Serialize(ar, "wantedPositionWantedCover", _wantedPositionWanted._cover, 1, false))
  CHECK(ar.Serialize("wantedPrecision", _wantedPrecision, 1, 0.0f))
  CHECK(ar.SerializeEnum("planningModeWanted", _planningModeWanted, 1, DoNotPlan))
  CHECK(ar.Serialize("forceReplanWanted", _forceReplanWanted, 1, false))

  CHECK(::Serialize(ar, "wantedPosition", safe_cast<Vector3 &>(_wantedPosition), 1, VUndefined))
  CHECK(::Serialize(ar, "wantedPositionCover", _wantedPosition._cover, 1, false))
  CHECK(::Serialize(ar, "plannedPosition", safe_cast<Vector3 &>(_plannedPosition), 1, VUndefined))
  CHECK(::Serialize(ar, "plannedPositionCover", _plannedPosition._cover, 1, false))
  CHECK(ar.SerializeEnum("planningMode", _planningMode, 1, DoNotPlan))
  CHECK(ar.Serialize("completedReceived", _completedReceived, 1, false))

  CHECK(::Serialize(ar, "watchDirection", _watchDir, 1, VZero))
  CHECK(::Serialize(ar, "watchDirectionHead", _watchDirHead, 1, VZero))
  CHECK(::Serialize(ar, "watchPosition", _watchPos, 1, VZero))
  CHECK(::Serialize(ar, "glancePosition", _glancePos, 1, VZero))
  CHECK(ar.SerializeRef("watchTarget", _watchTgt, 1))
  CHECK(ar.SerializeRef("glanceTarget", _glanceTgt, 1))
  CHECK(ar.SerializeEnum("watchMode", _watchMode, 1, WMNo))
  

  CHECK(::Serialize(ar, "expPosition", safe_cast<Vector3 &>(_expPosition), 1))
  CHECK(::Serialize(ar, "expPositionCover", _expPosition._cover, 1, false))

  CHECK(ar.SerializeEnum("state", _state, 1, Busy))
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    if (_state == Planning)
    {
      // working space for pathfinding is not saved
      _state = Init;
    }
  }
  CHECK(ar.SerializeEnum("mode", _mode, 1, Normal))

  // counter for trying of finding path
  CHECK(::Serialize(ar, "delay", _delay, 1))
  CHECK(ar.Serialize("iter", _iter, 1, 0))

  CHECK(ar.Serialize("getInAllowed", _getInAllowed, 1, true))
  CHECK(ar.Serialize("getInOrdered", _getInOrdered, 1, false))

  // strategic plan
  if (ar.GetArVersion() >= 8)
  {
#if _ENABLE_AI
    CHECK(ar.Serialize("Planner", *_planner, 1))
    CHECK(ar.Serialize("completedTime", _completedTime, 1)) // not used - why ??
    CHECK(::Serialize(ar, "waitWithPlan", _waitWithPlan, 1))
    CHECK(ar.Serialize("attemptPlan", _attemptPlan, 1, 0))
#endif

    CHECK(ar.Serialize("lastPlan", _lastPlan, 1, false))
    CHECK(ar.Serialize("noPath", _noPath, 1, false))
    CHECK(ar.Serialize("updatePath", _updatePath, 1, false))

    CHECK(ar.Serialize("exposureChange", _exposureChange, 1, 0))
  }

#if _ENABLE_CONVERSATION
  CHECK(ar.Serialize("kbCenter", _kbCenter, 1))
  CHECK(ar.SerializeRef("conversingWith", _conversingWith, 1))
#endif
  // behavior FSMs
  if (ar.IsSaving())
  {
    for (int i=0; i<NBehaviourFSMRole; i++)
    {
      FSM *fsm = _behaviourFSMs[i];
      if (!fsm) continue;

      RString name = RString("FSM") + FindEnumName((BehaviourFSMRole)i);
      // FSM should already exist
      CHECK(ar.Serialize(name, *fsm, 1))
    }

  }
  else if (ar.GetPass() == ParamArchive::PassSecond)
  {
    // _person is valid now, we can serialize FSMs
    ar.FirstPass();
    for (int i=0; i<NBehaviourFSMRole; i++)
    {
      FSM *fsm = _behaviourFSMs[i];
      if (!fsm) continue;

      RString name = RString("FSM") + FindEnumName((BehaviourFSMRole)i);
      // FSM should already exist
      CHECK(ar.Serialize(name, *fsm, 1))
    }

    ar.SecondPass();
    for (int i=0; i<NBehaviourFSMRole; i++)
    {
      FSM *fsm = _behaviourFSMs[i];
      if (!fsm) continue;

      RString name = RString("FSM") + FindEnumName((BehaviourFSMRole)i);
      // FSM should already exist
      CHECK(ar.Serialize(name, *fsm, 1))
    }
  }

  // serialize also short-term reaction FSM
  // TODO: serialization of EntityFSM, not only ScriptedFSM
  CHECK(ar.Serialize("reactionFSM",_reactionFSM, 1))
#if _ENABLE_CONVERSATION
  CHECK(ar.Serialize("conversationFSM",_conversationFSM, 1))

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond)
  {
    // avoid talking to other person when reacting
    if (_conversationFSM) ChangeListening(+1);
  }

// _talkEvents
  KBSaveContext context(this, &GGameState);
  void *oldParams = ar.SetParams(&context);
  CHECK(ar.Serialize("talkEvents",_talkEvents, 1));
  ar.SetParams(oldParams);
#endif

  CHECK(ar.Serialize("ghostTarget",_ghostTarget,1));

  // MP related serialization
  CHECK(ar.Serialize("roleIndex", _roleIndex, 1, -1));

  //serialize NetworkObject
  NetworkObject::Serialize(ar); //base

  if ( ar.IsLoading() && (ar.GetPass()==ParamArchive::PassSecond) 
    && !GetNetworkId().IsNull() )
  {
    //Calling CreateObject is not sufficient
    //GetNetworkManager().CreateObject(this, false /*not setNetworkId*/);
    // we should call appropriate CreateXXX methods when everything is serialized (e.g. subobjects,...)
    // store the "action" to see, what should be created "at least"
    GetNetworkManager().RememberSerializedObject(this);
  }

  return LSOK;
}

#if _ENABLE_CONVERSATION
LSError KBMessageInfo::Serialize(ParamArchive &ar)
{
  if (ar.IsSaving())
  {
    CHECK(ar.Serialize("topic", _topic,1))

    const KBMessageTemplate *type = _message->GetType();
    RString sentenceId = type->GetName();
    CHECK(ar.Serialize("sentenceId", sentenceId, 1))

    // during serialization of GameValue, GameState* is expected as a context
    KBSaveContext *context = (KBSaveContext *)ar.GetParams();
    ar.SetParams(context->_gameState);
    CHECK(ar.Serialize("Arguments", unconst_cast(_message.GetRef())->GetArguments(), 1))
    ar.SetParams(context);
  }
  else if (ar.GetPass()==ParamArchive::PassSecond)
  {
    // we cannot load message content until the Person is known, which is in the 2nd pass
    // note: this may somehow limit the possibilities of what is contained in the message
    // we need to pretend we are in the first pass, otherwise the Serialize of RString would do nothing
    ar.FirstPass();
    RString sentenceId;
    CHECK (ar.Serialize("sentenceId", sentenceId, 1));

    // get topic based on current brain
    KBSaveContext *context = (KBSaveContext *)ar.GetParams();
    const KBCenter *kbCenter = context->_sender->GetKBCenter();
    if (!kbCenter)
    {
      RptF("No KB found for %s",cc_cast(context->_sender->GetDebugName()));
      return LSStructure;
    }
    const KBCenter *kbParent = context->_sender->GetKBParent();
    KBTopic *kbTopic = kbCenter->GetTopic(_topic,kbParent);
    if (!kbTopic)
    {
      RptF("Topic %s not found",cc_cast(_topic));
      return LSStructure;
    }
    const KBMessageTemplate *type = kbTopic->FindMessageTemplate(sentenceId);
    if (!type)
    {
      RptF("Message template %s not found in %s",cc_cast(sentenceId),cc_cast(_topic));
      return LSStructure;
    }
    _message = new KBMessage(type);

    // during serialization of GameValue, GameState* is expected as a context
    ar.SetParams(context->_gameState);

    // serialize both passes
    CHECK(ar.Serialize("Arguments", unconst_cast(_message.GetRef())->GetArguments(), 1))
    ar.SecondPass();
    CHECK(ar.Serialize("Arguments", unconst_cast(_message.GetRef())->GetArguments(), 1))

    ar.SetParams(context);
  }
  return LSOK;
}
KBMessageInfo *KBMessageInfo::CreateObject(ParamArchive &ar)
{
  RString topic;
  if (ar.Serialize("topic",topic,1)!=LSOK) return NULL;
  // create an empty info, will be filled in the 2nd pass
  return new KBMessageInfo(topic, NULL);
}

LSError AIBrain::TalkEvent::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("from",_from,1));
  // more verbose, but cleaner than (Ref<KBMessageInfo> &)_event
  Ref<KBMessageInfo> event = unconst_cast(_event.GetRef());
  CHECK(ar.Serialize("event",event,1));
  _event = event;
  return LSOK;
}
#endif

AIBrain *AIBrain::CreateObject(ParamArchive &ar)
{
  RString type;
  if (ar.Serialize("agentType", type, 1, "Unit") != LSOK) return NULL;
  if (stricmp(type, "Unit") == 0) return new AIUnit(NULL);
#if _ENABLE_INDEPENDENT_AGENTS
  if (stricmp(type, "Agent") == 0) return new AIAgent(NULL);
#endif

  ErrF("Unknown agent type: %s", cc_cast(type));
  return NULL;
}

AIBrain *AIBrain::LoadRef(ParamArchive &ar)
{
  if (ar.GetArVersion() < 15)
  {
    // find in AI hierarchy
    TargetSide side = TSideUnknown;
    int idGroup;
    int id;
    if (ar.SerializeEnum("side", side, 1) != LSOK) return NULL;
    if (ar.Serialize("idGroup", idGroup, 1) != LSOK) return NULL;
    if (ar.Serialize("id", id, 1) != LSOK) return NULL;
    AICenter *center = NULL;
    switch (side)
    {
    case TWest:
      center = GWorld->GetWestCenter();
      break;
    case TEast:
      center = GWorld->GetEastCenter();
      break;
    case TGuerrila:
      center = GWorld->GetGuerrilaCenter();
      break;
    case TCivilian:
      center = GWorld->GetCivilianCenter();
      break;
    case TLogic:
      center = GWorld->GetLogicCenter();
      break;
    }
    if (!center) return NULL;
    AIGroup *group = NULL;
    for (int i=0; i<center->NGroups(); i++)
    {
      AIGroup *grp = center->GetGroup(i);
      if (grp && grp->ID() == idGroup)
      {
        group = grp;
        break;
      }
    }
    if (!group) return NULL;
    return group->GetUnit(id - 1);
  }
  else
  {
    // get it from person
    Object *obj = Person::LoadRef(ar);
    Person *person = dyn_cast<Person>(obj);
    if (!person) return NULL;
    return person->Brain();
  }
}

LSError AIBrain::SaveRef(ParamArchive &ar) const
{
  if (ar.GetArVersion() < 15)
  {
    // position in AI hierarchy
    AIUnit *unit = unconst_cast(this)->GetUnit();
    AIGroup *grp = GetGroup();
    AICenter *center = grp ? grp->GetCenter() : NULL;
    TargetSide side = center ? center->GetSide() : TSideUnknown;
    int idGroup = grp ? grp->ID() : -1;
    int id = unit ? unit->ID() : -1;
    CHECK(ar.SerializeEnum("side", side, 1))
    CHECK(ar.Serialize("idGroup", idGroup, 1))
    CHECK(ar.Serialize("id", id, 1))
  }
  else
  {
    if (_person)
    {
      CHECK(_person->SaveRef(ar))
    }
    else
    {
      // save the NULL object
      ObjectId id;
      CHECK(id.Serialize(ar, "oid", 1))
    }
  }
  return LSOK;
}

#define AI_BRAIN_MSG_LIST(XX) \
  XX(Create, CreateAIBrain) \
  XX(UpdateGeneric, UpdateAIBrain)

DEFINE_NETWORK_OBJECT(AIBrain, NetworkObject, AI_BRAIN_MSG_LIST)

DEFINE_NET_INDICES_EX(CreateAIBrain, NetworkObject, CREATE_AI_BRAIN_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateAIBrain, NetworkObject, UPDATE_AI_BRAIN_MSG, NoErrorInitialFunc)

NetworkMessageFormat &AIBrain::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  switch (cls)
  {
  case NMCCreate:
    NetworkObject::CreateFormat(cls, format);
    CREATE_AI_BRAIN_MSG(CreateAIBrain, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    NetworkObject::CreateFormat(cls, format);
    UPDATE_AI_BRAIN_MSG(UpdateAIBrain, MSG_FORMAT_ERR)
    break;
  default:
    NetworkObject::CreateFormat(cls, format);
    break;
  }
  return format;
}

/*!
\patch 1.04 Date 07/13/2001 by Jirka
- Fixed: squad pictures and titles missing on client machines
\patch_internal 1.04 Date 07/13/2001 by Jirka
- added transfer of AIUnitInfo::_squadTitle and AIUnitInfo::_squadPicture
\patch_internal 1.12 Date 08/06/2001 by Jirka
- Fixed: improved reaction when bad order of incoming messages
\patch 5099 Date 12/8/2006 by Jirka
- Fixed: Squad logo was not shown on all clients in MP
*/

TMError AIBrain::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    {
      TMCHECK(NetworkObject::TransferMsg(ctx))
      {
        PREPARE_TRANSFER(CreateAIBrain)

        if (ctx.IsSending())
        {
          TRANSF_REF(person)
        }
        AIUnitInfo &info = GetPerson()->GetInfo();
        TRANSF_EX(name, info._name)
        TRANSF_EX(face, info._face)
        TRANSF_EX(glasses, info._glasses)
        TRANSF_EX(speaker, info._speaker)
        TRANSF_EX(pitch, info._pitch)
        TRANSF_EX(rank, (int &)info._rank)
        TRANSF_EX(experience, info._experience)
        TRANSF_EX(initExperience, info._initExperience)
        TRANSF_EX(roleIndex, _roleIndex)
        // ADDED in 1.04
        TRANSF_EX(squadTitle, info._squadTitle)
        TRANSF_EX(squadPicture, info._squadPicturePath)
        TRANSF(objectIdHistory)
      }
    }
    break;
  case NMCUpdateGeneric:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateAIBrain)

      if (ctx.IsSending())
      {
        TRANSF_REF(person)
      }
      else
      {
        OLinkPermO(Person) person;
        TRANSF_REF_EX(person, person)
        if (!person)
        {
          NetworkId id;
          TRANSF_GET_ID_BASE(person, id);
          LogF(
            "AIUnit::TransferMsg (%d:%d) receiving - person (%d:%d) == NULL",
            GetNetworkId().creator.CreatorInt(), GetNetworkId().id,
            id.creator.CreatorInt(), id.id);
        }
        // FIX
        if (person && person != _person)
        {
          AddRef();
          if (_person)
          {
            // move info to new person
            AIUnitInfo &info = _person->GetInfo();
            person->SetInfo(info);
            person->SetFace();
            person->SetGlasses();

            // keep variables (some scripting modules requires it)
            person->EntityAI::GetVars()->_vars = _person->EntityAI::GetVars()->_vars;

            LogF(
              "*** Remote: Identity %s transferred from %d:%d to %d:%d",
              (const char *)info._name,
              _person->GetNetworkId().creator.CreatorInt(), _person->GetNetworkId().id,
              person->GetNetworkId().creator.CreatorInt(), person->GetNetworkId().id);

            // remove old person
            GWorld->RemoveSensor(_person);
            _person->SetBrain(NULL);
            _person = NULL;
          }
          // set new person
          person->SetBrain(this);
          _person = person;
          Release();
          _inVehicle = NULL;
        }
      }
      if (_person)
      {
        TRANSF_EX(experience, _person->GetInfo()._experience)
      }
      TRANSF_REF(remoteControlled)
      if ( !ctx.IsSending() && _remoteControlled && _remoteControlled->IsLocal() )
      {
        AIBrain *brain = _remoteControlled->Brain();
        if (brain)
          brain->SetRemoteControlling(true);
      }

#if !_ENABLE_IDENTITIES
      TRANSF(ability)
#else
      {
        float dummy=0;
        TRANSF_EX(ability,dummy)
      }
#endif
      TRANSF_ENUM(semaphore)
      TRANSF_ENUM(combatModeMajor)
      TRANSF(dangerUntil)
      TRANSF(captive)
      TRANSF(objectIdHistory)
      // TODO: TRANSF_REF(targetAssigned)
      // TODO: TRANSF(targetAssignedValid)
      //      TMCHECK(_path.TransferMsg(ctx, indices->path))
      TRANSF_REF(house)
      TRANSF(housePos)
      TRANSF(wantedPosition)
      //      TRANSF(plannedPosition)
      TRANSF_ENUM(planningMode)
      //      TRANSF(completedReceived)
      //      TRANSF(expPosition)
      TRANSF_ENUM(state)
      TRANSF_ENUM(mode)
      if (!ctx.IsSending())
      {
        LifeState lsBackup = _lifeState;
        TRANSF_ENUM(lifeState);
        if (lsBackup!=_lifeState) 
        {
          GetNetworkManager().SetPlayerRoleLifeState(GetRoleIndex(), GetLifeState());
        }
      }
      else TRANSF_ENUM(lifeState)
      // ?? _delay
      TRANSF(getInAllowed)
      TRANSF(getInOrdered)
      // TODO: strategic plAn
      //      TRANSF(lastPlan)
      //      TRANSF(noPath)
      if (!ctx.IsSending())
      {
        _path.Clear();
        _plannedPosition = _wantedPosition;
        _completedReceived = false;
        _expPosition = _wantedPosition;
        _lastPlan = false;
        _noPath = true;
      }
    }
    break;
  default:
    TMCHECK(NetworkObject::TransferMsg(ctx))
      break;
  }
  return TMOK;
}

float AIBrain::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = NetworkObject::CalculateError(ctx);

  PREPARE_TRANSFER(UpdateAIBrain)

  if (_person)
  {
    ICALCERRE_ABSDIF(float, experience, _person->GetExperience(), AIUpdateErrorCoef * 0.01 * ERR_COEF_VALUE_MINOR)
  }
#if !_ENABLE_IDENTITIES
  ICALCERRE_ABSDIF(float, ability, _ability, AIUpdateErrorCoef * ERR_COEF_VALUE_MAJOR)
#endif
  ICALCERR_NEQ(int, semaphore, AIUpdateErrorCoef * ERR_COEF_MODE)
  ICALCERR_NEQ(int, combatModeMajor, AIUpdateErrorCoef * ERR_COEF_MODE)
  ICALCERR_NEQ(Time, dangerUntil, AIUpdateErrorCoef * ERR_COEF_MODE)
  ICALCERR_NEQ(int, captive, AIUpdateErrorCoef * ERR_COEF_MODE)
  ICALCERR_NEQREF_SOFT(Object, house, AIUpdateErrorCoef * ERR_COEF_VALUE_MAJOR)
  ICALCERR_NEQ(int, housePos, AIUpdateErrorCoef * ERR_COEF_VALUE_MAJOR)
  ICALCERR_NEQ(Vector3, wantedPosition, AIUpdateErrorCoef * ERR_COEF_VALUE_MINOR)
  //  ICALCERR_NEQ(Vector3, plannedPosition, AIUpdateErrorCoef * ERR_COEF_MODE)
  ICALCERR_NEQ(int, planningMode, AIUpdateErrorCoef * ERR_COEF_VALUE_MINOR)
  //  ICALCERR_DIST2(expPosition, AIUpdateErrorCoef * 0.1 * ERR_COEF_VALUE_MINOR)
  ICALCERR_NEQ(int, state, AIUpdateErrorCoef * ERR_COEF_MODE)
  // error not very important, as lifeState is sent after each change
  ICALCERR_NEQ(int, lifeState, AIUpdateErrorCoef * ERR_COEF_MODE)
  ICALCERR_NEQ(bool, getInAllowed, AIUpdateErrorCoef * ERR_COEF_MODE)
  ICALCERR_NEQ(bool, getInOrdered, AIUpdateErrorCoef * ERR_COEF_MODE)
  //  error += _path.CalculateError(ctx, indices->path);
  //  ICALCERR_NEQ(bool, noPath, ERR_COEF_MODE)
  return error;
}

#if _ENABLE_CONVERSATION

void AIBrain::AddKBTopic(RString name, RString filename, RString taskType, GameValuePar eventHandler)
{
  if (!_kbCenter) _kbCenter = new KBCenter();
  KBTopic *topic = new KBTopicExt();
  topic->Init(name, filename, taskType, eventHandler);
  _kbCenter->AddTopic(topic);
}

void AIBrain::RemoveKBTopic(RString name)
{
  if (_kbCenter) _kbCenter->RemoveTopic(name);
}

bool AIBrain::HasKBTopic(RString name)
{
  const KBCenter *kbCenter = GetKBCenter();
  if (!kbCenter) return false;
  const KBCenter *kbParent = GetKBParent();
  return kbCenter->GetTopic(name,kbParent) != NULL;
}

KBMessageInfo *AIBrain::CreateKBMessage(RString topic, GameValuePar sentence)
{
  const KBCenter *kbCenter = GetKBCenter();
  if (!kbCenter) return NULL;
  const KBCenter *kbParent = GetKBParent();
  return kbCenter->CreateMessage(topic, sentence,kbParent);
}

void AIBrain::SetKBMessageArgument(KBMessageInfo *message, GameValuePar argument)
{
  const KBCenter *kbCenter = GetKBCenter();
  if (kbCenter) kbCenter->SetMessageArgument(message, argument);
}

void AIBrain::KBTell(AIBrain *to, KBMessageInfo *message, bool forceRadio)
{
  if (!IsLocal())
  {
    RptF("Conversation - need to be initiated by the speaker owner");
    return;
  }

  // replace the special words
  const KBCenter *kbCenter = GetKBCenter();
  const KBCenter *kbParent = GetKBParent();
  if (kbCenter) kbCenter->CheckMessage(message, kbParent);
  void KBTell(AIBrain *askingUnit, AIBrain *askedUnit, const KBMessageInfo *info, bool forceRadio);
  KBTell(this, to, message, forceRadio);
}

void AIBrain::KBReact(AIBrain *sender, const KBMessageInfo *question)
{
  if (!IsLocal())
  {
    RptF("Conversation - reacting person need to be local");
    return;
  }

  // TODO: handle multiple agents we may be conversing with at the same time
  _conversingWith = sender;

  if (IsPlayer())
  {
    // Start conversation UI
    void ProcessConversation(AIBrain *askingUnit, AIBrain *askedUnit, const KBMessageInfo *question);
    ProcessConversation(this, sender, question);
  }
  else
  {
    const KBCenter *kbCenter = GetKBCenter();
    if (kbCenter)
    {
      if (!question) return;
      RString topic = question->GetTopic();
      if (topic.GetLength() == 0) return;
      #ifdef GetMessage
      #undef GetMessage
      #endif
      const KBMessage *message = question->GetMessage();
      if (!message) return;
      const KBMessageTemplate *messageType = message->GetType();
      if (!messageType) return;

      const KBCenter *kbParent = GetKBParent();
      RString handler = kbCenter->GetHandlerAI(question, kbParent);
      if (handler.GetLength() == 0) return;

  #if _ENABLE_INDEPENDENT_AGENTS
      FSMScripted *fsm = NULL;

      AITeamMember *teamMember = GetTeamMember();
      if (teamMember)
      {
        const AITaskType *taskType = teamMember->GetTaskType(handler);
        if (taskType)
        {
          // Task creation
          fsm = CreateTeamMemberFSMScripted(taskType->GetFSMType(), teamMember->GetVars());
          const float priority = 100.0f;
          AITask *task = new AITask(teamMember, teamMember, NULL, taskType, fsm, priority);
          teamMember->AddTask(task);
        }
      }
      else
  #endif
      {
        TalkEvent &ev = _talkEvents.Append();
        ev._from = sender;
        ev._event = question;
      }
    }
  }
}

const KBCenter *AIBrain::GetKBParent() const
{
  // if there is no explicit primary, type is primary and there is no parent (secondary)
  if (!_kbCenter) return NULL;
  // if there was primary, return based on type
  if (!_person) return NULL;
  const EntityAIType *type = _person->GetType();
  return type->GetKBCenter();
}

const KBCenter *AIBrain::GetKBCenter() const
{
  if (_kbCenter) return _kbCenter;
  // no explicit primary - check type
  if (!_person) return NULL;
  const EntityAIType *type = _person->GetType();
  return type->GetKBCenter();
}

ConversationContext::ConversationContext(AIBrain *askingUnit, AIBrain *askedUnit, const KBMessageInfo *question)
{
  _askingUnit = askingUnit;
  _askedUnit = askedUnit;
  _question = question;

  if (_askingUnit) _askingUnit->ChangeSpeaking(+1);
  if (_askedUnit) _askedUnit->ChangeListening(+1);

  _center = _askingUnit->GetKBCenter();
  const KBCenter *kbParent = _askingUnit->GetKBParent();
  KBCenter::ParentList list(kbParent);

  // decide if old (class UI) or new (event handlers) method will be used
  bool someHandler = false;
  if (question)
  {
    _topic = question->GetTopic();

    // ask the received topic for the reaction
    GameValue handler = _center->GetHandlerPlayer(question, list);
    if (!handler.GetNil())
    {
      someHandler = true;
      _handlerResult = ExecuteHandler(handler, question->GetTopic(), question->GetMessage());
      if (_handlerResult.GetNil()) _handlerResult = GGameState.CreateGameValue(GameArray);
    }
  }
  else
  {
    // collect reactions
    int topics = list.TopicCount(_center);
    for (int i=0; i<topics; i++)
    {
      const KBTopic *topic = list.GetTopic(_center, i);
      const GameValue &handler = topic->GetHandlerPlayer();
      if (!handler.GetNil())
      {
        if (!someHandler)
        {
          // first handler found, initialization
          someHandler = true;
          _handlerResult = GGameState.CreateGameValue(GameArray);
        }
        GameValue result = ExecuteHandler(handler, topic->GetName(), NULL);
        if (!result.GetNil())
        {
          const GameArrayType &srcArray = result;
          GameArrayType &dstArray = _handlerResult;
          for (int j=0; j<srcArray.Size(); j++) dstArray.Add(srcArray[j]);
        }
      }
    }
  }

  if (!someHandler) _current = _center->GetUIRoot(question, list);
}

ConversationContext::~ConversationContext()
{
  if (_askingUnit) _askingUnit->ChangeSpeaking(-1);
  if (_askedUnit) _askedUnit->ChangeListening(-1);
}

GameValue ConversationContext::ExecuteHandler(GameValuePar handler, RString topic, const KBMessage *message)
{
  PROFILE_SCOPE_EX(cnvEH, *);
  if (PROFILE_SCOPE_NAME(cnvEH).IsActive())
  {
    PROFILE_SCOPE_NAME(cnvEH).AddMoreInfo(Format("%s->%s:%s",
      _askingUnit ? cc_cast(_askingUnit->GetDebugName()) : "null", 
      _askedUnit ? cc_cast(_askedUnit->GetDebugName()) : "null",
      cc_cast(topic)));
  }

  GameVarSpace local(false);
  GGameState.BeginContext(&local);

  // attention, all variable names must be in lower case

  // player himself
  Person *me = _askingUnit ? _askingUnit->GetPerson() : NULL;
  local.VarLocal("_this");
  local.VarSet("_this", GameValueExt(me));
  // person we are reacting to
  Person *you = _askedUnit ? _askedUnit->GetPerson() : NULL;
  local.VarLocal("_from");
  local.VarSet("_from", GameValueExt(you));
  // conversation topic
  local.VarLocal("_topic");
  local.VarSet("_topic", GameValue(topic));
  // sentence
  RString sentenceId;
  if (message)
  {
    const KBMessageTemplate *messageType = message->GetType();
    if (messageType) sentenceId = messageType->GetName();
  }
  local.VarLocal("_sentenceid");
  local.VarSet("_sentenceid", GameValue(sentenceId));
  // sentence arguments
  if (message)
  {
    for (int i=0; i<message->NArguments(); i++)
    {
      const KBArgument &argument = message->GetArgument(i);
      RString name = RString("_") + argument._name;
      name.Lower();
      local.VarLocal(name);
      local.VarSet(name, argument._value);
    }
  }

  // execute the handler
  GameValue result;
#if USE_PRECOMPILATION
  if (handler.GetType() == GameCode)
  {
    GameDataCode *code = static_cast<GameDataCode *>(handler.GetData());
    if (code->IsCompiled() && code->GetCode().Size() > 0)
      result = GGameState.Evaluate(code->GetString(), code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
  }
  else
#endif
  if (handler.GetType() == GameString)
  {
    // make sure string is not destructed while being evaluated
    RString code = handler;
    if (code.GetLength() > 0)
      result = GGameState.EvaluateMultiple(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
  }

  GGameState.EndContext();

  // return the result (nil when empty or unexpected)
  if (result.GetNil()) return GameValue();
  if (result.GetType() != GameArray) return GameValue();
  const GameArrayType &array = result;
  if (array.Size() == 0) return GameValue();
  return result;
}

#endif // _ENABLE_CONVERSATION


#if _ENABLE_DIRECT_MESSAGES

bool AIBrain::IsGestureEnabled(ChatChannel channel) const
{
  switch (_gestureEnabled)
  {
  case GEDisabled:
    return false;
  case GEEnabled:
  default:
    return true;
  }
}

bool AIBrain::IsVoiceEnabled(ChatChannel channel) const
{
  switch (_voiceEnabled)
  {
  case VEDisabled:
    return false;
  case GEEnabled:
    return true;
  default:
    return GetCombatMode() != CMStealth;
  }
}

#endif

float AIBrain::GetSERank() const
{
  Person *person = GetPerson();
  if (!person) return 0;
  AIUnitInfo &info = person->GetInfo();
  // 0 for RankPrivate, 1 for max. rank
  return (float)info._rank / (NRanks - 1);
}

float AIBrain::GetSECaptive() const
{
  return _captive != 0 ? 1.0f : 0.0f;
}

float AIBrain::GetSESafe() const
{
  return GetCombatMode() == CMSafe ? 1.0f : 0.0f;
}

float AIBrain::GetSECombat() const
{
  return GetCombatMode() == CMCombat ? 1.0f : 0.0f;
}

float AIBrain::GetSEStealth() const
{
  return GetCombatMode() == CMStealth ? 1.0f : 0.0f;
}

float AIBrain::GetSEMorale() const
{
  // interpolate from interval <-1, 1> to <0, 1>
  float morale = 0.5f * (GetMorale() + 1.0f);
  saturate(morale, 0.0f, 1.0f);
  return morale;
}

float AIBrain::GetSEDanger() const
{
  AIGroup *group = GetGroup();
  if (!group) return 0.0f;
  AICenter *center = group->GetCenter();
  if (!center) return 0.0f;

  float exposure = (1.0f / 5000.0f) * center->GetExposureOptimistic(Position(GetFutureVisualState()));
  saturate(exposure, 0.0f, 1.0f);
  return exposure;
}

void AIBrain::AddExp(float exp)
{
  Person *person = GetPerson();
  AIUnitInfo &info = person->GetInfo();
  float limit = floatMin(info._experience, 0);
  info._experience += exp;
  if (person != GLOB_WORLD->GetRealPlayer())
    saturateMax(info._experience, limit);
}

void AIBrain::IncreaseExperience(const VehicleType& type, TargetSide side)
{
  if (type.IsAnimal()) return;

  float coef;

  if (type.IsKindOf(GWorld->Preloaded(VTypeStatic)))
  {
    coef = ExperienceDestroyStatic;
  }
  else
  {
    if (side == TCivilian)
      coef = ExperienceDestroyCivilian;
    else if (IsEnemy(side) || IsEnemy(type.GetTypicalSide()))
      coef = ExperienceDestroyEnemy;
    else
      coef = ExperienceDestroyFriendly;
  }

  if (!IsPlayer() && coef <= 0) return;

  int i, n = ExperienceDestroyTable.Size();
  float base = 0;
  float cost = type.GetCost();
  if (cost > ExperienceDestroyTable[n - 2].maxCost)
  {
    base = ExperienceDestroyTable[n - 1].exp;
  }
  else
    for (i=0; i<n-1; i++)
    {
      if (cost <= ExperienceDestroyTable[i].maxCost)
      {
        base = ExperienceDestroyTable[i].exp;
        break;
      }
    }

  AddExp(coef * base);
}

#if _ENABLE_IDENTITIES

LSError AITaskType::Serialize(ParamArchive &ar)
{
  if (ar.IsSaving())
  {
    CHECK(ar.Serialize("name", _name, 1))
  }
  CHECK(ar.Serialize("instancesCreated", _instancesCreated, 1, 0))
  CHECK(ar.Serialize("instancesDeleted", _instancesDeleted, 1, 0))
  return LSOK;
}

AITaskType *AITaskType::CreateObject(ParamArchive &ar)
{
  AITaskTypeName name;
  if (ar.Serialize("name", name, 1) != LSOK) return NULL;
  return AITaskTypes.New(name);
}

AITaskTypeInfo::AITaskTypeInfo(AITaskType *type)
{
  _type = type;
  _created = 0;
}

LSError AITaskTypeInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("type", _type, 1));
  CHECK(ar.Serialize("created", _created, 1, 0));
  return LSOK;
}

#include "fsmScripted.hpp"

static ConstParamEntryPtr FindParamEntry(RString parent, RString name)
{
  const ParamFile *path[] = {&ExtParsMission, &ExtParsCampaign, &Pars};
  for (int i=0; i<lenof(path); i++)
  {
    ConstParamEntryPtr cls = path[i]->FindEntry(parent);
    if (cls)
    {
      ConstParamEntryPtr entry = cls->FindEntry(name);
      if (entry) return entry;
    }
  }
  WarningMessage("Class %s.%s not found", cc_cast(parent), cc_cast(name));
  return ConstParamEntryPtr();
}

AITaskType::AITaskType(const AITaskTypeName &name)
{
  _instancesCreated = 0;
  _instancesDeleted = 0;

  _name = name;
  ConstParamEntryPtr cls = FindParamEntry("CfgTasks", name);
  _parClass = cls.GetClass();
  if (cls)
  {
    RString FindScript(RString name);

    const ParamEntry &par = *cls;
    _typeName = par >> "name";
    RString fsmFilename = FindScript(par >> "fsm");
    if (fsmFilename.GetLength() > 0)
    {
      _fsmType = FSMScriptedTypes.New(fsmFilename);
    }
    RString conditionFilename = par >> "condition";
    if (conditionFilename.GetLength() > 0)
    {
      conditionFilename = FindScript(conditionFilename);
      if (conditionFilename.GetLength() > 0)
      {
        FilePreprocessor preproc(true);
        QOStrStream processed;
        if (preproc.Process(&processed, conditionFilename))
        {
          _condition = RString(processed.str(), processed.pcount());
#if USE_PRECOMPILATION
          GameState *state = GWorld->GetGameState();
          GameVarSpace local(state->GetContext(), false);
          state->BeginContext(&local);
          SourceDoc doc; doc._content = _condition;
          SourceDocPos pos(doc);
          state->CompileMultiple(doc, pos, _conditionCompiled, false, GWorld->GetMissionNamespace()); // mission namespace
          state->EndContext();
#endif
        }
      }
    }
    _descriptionExp = par >> "description";

    ConstParamEntryPtr entry = par.FindEntry("descriptionShort");
    if (entry) _descriptionShortExp = *entry;
    else _descriptionShortExp = _descriptionExp;

    entry = par.FindEntry("descriptionHUD");
    if (entry) _descriptionHUDExp = *entry;
    else _descriptionHUDExp = _descriptionShortExp;

    _destinationExp = par >> "destination";

    ParamEntryVal resources = par >> "resources";
    int n = resources.GetSize();
    _resources.Realloc(n);
    _resources.Resize(n);
    for (int i=0; i<n; i++) _resources[i] = resources[i];
  }
}

const FSMScriptedType *AITaskType::GetFSMType() const
{
  return _fsmType;
}

RString AITaskType::GetSentence(RString name) const
{
  if (!_parClass) return RString();
  ConstParamEntryPtr entry = _parClass->FindEntry(name);
  if (!entry) return RString();
  return *entry;
}

AITaskTypeBank AITaskTypes;

#if _ENABLE_INDEPENDENT_AGENTS
AITask::AITask(
   AITeamMember *owner, AITeamMember *sender, Task *parent, const AITaskType *type,
   FSM *fsm, float priority)
   : Task(parent, type ? type->GetTypeName() : RString())
{
  _owner = owner;
  _sender = sender;
  Assert(type);
  _type = type;
  _fsm = fsm;
  _priority = priority;
  _running = false;
}
#else
AITask::AITask(
   AIBrain *owner, Task *parent, const AITaskType *type,
   FSM *fsm, float priority)
   : Task(parent, type ? type->GetTypeName() : RString())
{
  _owner = owner;
  Assert(type);
  _type = type;
  _fsm = fsm;
  _priority = priority;
  _running = false;
}

#endif

bool AITask::GetDescription(RString &result) const
{
  if (!_type || !_fsm) return false;
  RString expression = _type->GetDescriptionExp();
  if (expression.GetLength() == 0) return false;
  GameValue value;
  if (!_fsm->Evaluate(value, expression)) return false;
  if (value.GetType() != GameString) return false;
  result = value;
  return true;
}

bool AITask::GetDescriptionShort(RString &result) const
{
  if (!_type || !_fsm) return false;
  RString expression = _type->GetDescriptionShortExp();
  if (expression.GetLength() == 0) return false;
  GameValue value;
  if (!_fsm->Evaluate(value, expression)) return false;
  if (value.GetType() != GameString) return false;
  result = value;
  return true;
}

bool AITask::GetDescriptionHUD(RString &result) const
{
  if (!_type || !_fsm) return false;
  RString expression = _type->GetDescriptionHUDExp();
  if (expression.GetLength() == 0) return false;
  GameValue value;
  if (!_fsm->Evaluate(value, expression)) return false;
  if (value.GetType() != GameString) return false;
  result = value;
  return true;
}

bool AITask::GetDestination(Vector3 &result) const
{
  if (!_type || !_fsm) return false;
  RString expression = _type->GetDestinationExp();
  if (expression.GetLength() == 0) return false;
  GameValue value;
  if (!_fsm->Evaluate(value, expression)) return false;

  // avoid type error in GetPos() - check to ensure value is an array
  if (value.GetType() != GameArray) return false;

  GameState *state = GWorld->GetGameState();
  bool GetPos(const GameState *state, Vector3 &ret, GameValuePar oper);
  return GetPos(state, result, value);
}

AITask *AITask::CreateObject(ParamArchive &ar)
{
  return new AITask(SerializeConstructor);
}

LSError AITask::Serialize(ParamArchive &ar)
{
  CHECK(Task::Serialize(ar))
  CHECK(ar.SerializeRef("owner", _owner, 1))
  if (ar.IsSaving())
  {
    RString typeName = _type ? _type->GetTypeName() : RString();
    CHECK(ar.Serialize("type", typeName, 1))
  }
  else if (ar.GetPass() == ParamArchive::PassFirst)
  {
    // create type and fsm
    RString typeName;
    CHECK(ar.Serialize("type", typeName, 1))
    if (typeName.GetLength() > 0)
    {
#if _ENABLE_INDEPENDENT_AGENTS
      AITeamMember *teamMember = reinterpret_cast<AITeamMember *>(ar.GetParams());
      _type = teamMember->GetTaskType(typeName);
      if (_type) _fsm = CreateTeamMemberFSMScripted(_type->GetFSMType(), teamMember->GetVars());
#endif
    }
  }
  if (_fsm)
  {
    CHECK(ar.Serialize("FSM", *_fsm, 1))
  }
  CHECK(ar.Serialize("priority", _priority, 1, 0))
  CHECK(ar.Serialize("running", _running, 1, false))
  return LSOK;
}

AITask *AITask::LoadRef(ParamArchive &ar)
{
#if _ENABLE_INDEPENDENT_AGENTS
  AITeamMemberLink owner = AITeamMember::LoadTeamMemberRef(ar);
  if (!owner) return NULL;

  int taskId;
  if (ar.Serialize("taskId", taskId, 1, -1) != LSOK) return NULL;
  return owner->FindTask(taskId);
#else
  return NULL;
#endif
}

LSError AITask::SaveRef(ParamArchive &ar)
{
  CHECK(base::SaveRef(ar))
#if _ENABLE_INDEPENDENT_AGENTS
  DoAssert(_owner);
  CHECK(_owner->SaveTeamMemberRef(ar))
  CHECK(ar.Serialize("taskId", _id, 1, -1))
#endif
  return LSOK;
}

DEFINE_SERIALIZE_TYPE_REF_INFO(AITask, Task);

#endif

#if _ENABLE_INDEPENDENT_AGENTS

AITeamMember::~AITeamMember()
{
#if _ENABLE_IDENTITIES
  // remove tasks that are assigned to the team
  _tasks.Clear();
#endif
}

LSError AITeamMemberRef::Serialize(ParamArchive &ar)
{
  if (ar.IsSaving() || ar.GetPass() == ParamArchive::PassSecond)
  {
    DoAssert(_ref);
    if (_ref)
    {
      CHECK(_ref->Serialize(ar))
    }
  }
  else
  {
    _ref = AITeamMember::CreateObject(ar);
    DoAssert(_ref);
    if (_ref)
    {
      CHECK(_ref->Serialize(ar))
    }
  }
  return LSOK;
}

AITeamMember *AITeamMember::CreateObject(ParamArchive &ar)
{
  RString type;
  if (ar.Serialize("teamMemberType", type, 1) != LSOK) return NULL;

  // only teams are serialized this way
  if (stricmp(type, "Team") == 0)
    return new AITeam();

  return NULL;
}

LSError SaveRefT(AITeamMemberRef &value, ParamArchive &arRef)
{
  return value->SaveTeamMemberRef(arRef);
}

LSError LoadRefT(AITeamMemberRef &value, ParamArchive &arRef)
{
  value = AITeamMember::LoadTeamMemberRef(arRef);
  return LSOK;
}

LSError SaveRefT(AITeamMemberLink &value, ParamArchive &arRef)
{
  return value->SaveTeamMemberRef(arRef);
}

LSError LoadRefT(AITeamMemberLink &value, ParamArchive &arRef)
{
  value = AITeamMember::LoadTeamMemberRef(arRef);
  return LSOK;
}

AITeamMember *AITeamMember::LoadTeamMemberRef(ParamArchive &ar)
{
  RString type;
  if (ar.Serialize("teamMemberType", type, 1) != LSOK) return NULL;

  if (stricmp(type, "Team") == 0)
    return AITeam::LoadRef(ar);
  else if (stricmp(type, "Agent") == 0)
  {
    AIBrain *brain = AIAgent::LoadRef(ar);
    if (!brain) return NULL;
    return brain->GetTeamMember();
  }

  return NULL;
}

LSError AITeamMember::SaveLinkRef(ParamArchive &ar, AITeamMemberLink &link)
{
  return link->SaveTeamMemberRef(ar);
}

LSError AITeamMember::LoadLinkRef(ParamArchive &ar, AITeamMemberLink &link)
{
  link = LoadTeamMemberRef(ar);
  return LSOK;
}

LSError AITeamMember::Serialize(ParamArchive &ar)
{
#if _ENABLE_IDENTITIES
  CHECK(ar.Serialize("TaskTypes", _taskTypes, 1))
  // to search the type by the name, we need to access team member in task
  {
    void *old = ar.GetParams();
    ar.SetParams(this);
    LSError result = ar.Serialize("Tasks", _tasks, 1);
    ar.SetParams(old);
    if (result != LSOK) return result;
  }
  CHECK(ar.Serialize("nextTaskId", _nextTaskId, 1))
#endif

  CHECK(ar.SerializeArray("Resources", _resources, 1))
  // variables
  {
    void *old = ar.GetParams();
    ar.SetParams(&GGameState);
    DoAssert(_vars.IsSerializationEnabled());
    LSError result = ar.Serialize("Variables", _vars._vars, 1);
    ar.SetParams(old);
    if (result != LSOK) return result;
  }
  CHECK(ar.Serialize("fromEditor", _fromEditor, 1))

  return LSOK;
}

AITeamMember *AITeamMemberLinkTraits::GetObject(TrackSoftLinks<IdTraits> *tracker)
{
  NetworkObject *object = static_cast<NetworkObject *>(tracker->GetObject());
  if (!object) return NULL;
  return object->GetTeamMember();
}

RString AITeamMember::GetCallSignShort() const
{
  GameValue value;
  if (!_vars.VarGet("callSignShort", value)) return RString();
  return value;
}

RString AITeamMember::GetCallSign() const
{
  GameValue value;
  if (!_vars.VarGet("callSign", value)) return RString();
  return value;
}

RString AITeamMember::GetCallSignLong() const
{
  GameValue value;
  if (!_vars.VarGet("callSignLong", value)) return RString();
  return value;
}

#if _ENABLE_IDENTITIES

static int CmpAITasks(const RefR<AITask> *t1, const RefR<AITask> *t2)
{
  const AITask *task1 = *t1;
  const AITask *task2 = *t2;

  // sort by priorities (lexicographically)
  AUTO_STATIC_ARRAY(float, priorities1, 16);
  for (const Task *t=task1; t; t=t->GetParent())
    priorities1.Add(t->GetPriority());
  AUTO_STATIC_ARRAY(float, priorities2, 16);
  for (const Task *t=task2; t; t=t->GetParent())
    priorities2.Add(t->GetPriority());

  int i1 = priorities1.Size() - 1;
  int i2 = priorities2.Size() - 1;
  while (i1 >= 0 && i2 >= 0)
  {
    float diff = priorities2[i2] - priorities1[i1];
    if (diff != 0) return sign(diff);
    i1--; i2--;
  }
  if (i1 >= 0) return -1;
  if (i2 >= 0) return 1;

  // keep running task running
  if (task1->IsRunning())
  {
    if (!task2->IsRunning()) return -1;
  }
  else
  {
    if (task2->IsRunning()) return 1;
  }

  // sort by name
  int diff = stricmp(task1->GetName(), task2->GetName());
  if (diff != 0) return diff;

  return task1->GetId() - task2->GetId();
}

#endif

void AITeamMember::SimulateAI()
{
#if _ENABLE_IDENTITIES
  // task types simulation
  GameState *state = GWorld->GetGameState();
  state->BeginContext(&_vars);
  _vars.VarLocal("_this");
  _vars.VarLocal("_tasktype");
  _vars.VarLocal("_thiscreated");
  _vars.VarLocal("_thisrunning");
  _vars.VarLocal("_totalcreated");
  _vars.VarLocal("_totalrunning");
  _vars.VarLocal("_task");
  _vars.VarSet("_this", GameValueExt(this), true);
  for (int i=0; i<_taskTypes.Size(); i++)
  {
    AITaskTypeInfo &info = _taskTypes[i];
    AITaskType *taskType = info._type;
    RString condition = taskType->GetCondition();
    if (condition.GetLength() == 0) continue;

    _vars.VarSet("_tasktype", GameValue(taskType->GetTypeName()), true);
    _vars.VarSet("_thiscreated", GameValue((float)info._created), true);
    int count = 0;
    for (int j=0; j<_tasks.Size(); j++)
    {
      if (_tasks[j]->GetType() == taskType) count++;
    }
    _vars.VarSet("_thisrunning", GameValue((float)count), true);
    _vars.VarSet("_totalcreated", GameValue((float)taskType->GetInstancesCreated()), true);
    _vars.VarSet("_totalrunning", GameValue((float)(taskType->GetInstancesCreated() - taskType->GetInstancesDeleted())), true);

#if USE_PRECOMPILATION
    state->Execute(condition, taskType->GetConditionCompiled(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#else
    state->Execute(condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#endif
  }

  // task manager
  // sort tasks by priorities
  QSort(_tasks.Data(), _tasks.Size(), CmpAITasks);
  // resume tasks by priorities and resources
  AUTO_STATIC_ARRAY(char, states, 32);
  static const char Available = 0;
  static const char Assigned = 1;
  static const char Testing = 2;
  int n = _resources.Size();
  states.Resize(n);
  for (int i=0; i<n; i++) states[i] = Available;

  for (int i=0; i<_tasks.Size(); i++)
  {
    AITask *task = _tasks[i];
    Assert(task);
    const AITaskType *type = task->GetType();
    if (!type)
    {
      task->Run(false);
      continue;
    }

    bool ok = true;
    const AutoArray<AITaskResource> &required = type->GetResources();
    for (int j=0; j<required.Size(); j++)
    {
      // check if this resource is available
      ok = false;
      for (int k=0; k<n; k++)
      {
        if (states[k] == Available && stricmp(_resources[k], required[j]) == 0)
        {
          states[k] = Testing;
          ok = true;
          break;
        }
      }
      if (!ok) break;
    }
    task->Run(ok);
    if (ok)
    {
      for (int k=0; k<n; k++)
      {
        if (states[k] == Testing) states[k] = Assigned;
      }
    }
    else
    {
      for (int k=0; k<n; k++)
      {
        if (states[k] == Testing) states[k] = Available;
      }
    }
  }

  // tasks simulation
  for (int i=0; i<_tasks.Size();)
  {
    AITask *task = _tasks[i];
    Assert(task);
    FSM *fsm = task->GetFSM();
    if (!fsm)
    {
      if (task->GetType()) task->GetType()->OnInstanceDeleted();
      _tasks.Delete(i);
      continue;
    }
    if (!task->IsRunning())
    {
      i++;
      continue;
    }

    AITeamMemberContext ctx(this);
    GameValue CreateGameTask(Task *task);
    _vars.VarSet("_task", CreateGameTask(task), true);
    if (fsm->Update(&ctx))
    {
      // FSM finished
      if (task->GetType()) task->GetType()->OnInstanceDeleted();
      GameVarSpace *vars = fsm->GetVars();
      if (vars) vars->_vars.Clear();
      _tasks.Delete(i);
      continue;
    }
    i++;
  }
  _vars.VarSet("_task", GameValue(), true);
  state->EndContext();
#endif
}

void AITeamMember::SendMsg(RadioMessage *message)
{
#if _ENABLE_DIRECT_MESSAGES
  // TODO: better selection of channel
  // now, direct speaking is always used
  AIBrain *GetSpeaker(AITeamMember *member);
  AIBrain *speaker = GetSpeaker(this);
  if (!speaker) return;
  Person *person = speaker->GetPerson();
  if (!person) return;
  RadioChannel *channel = person->GetRadio();
  if (!channel) return;

  channel->Transmit(message, speaker->GetLanguage());
#endif
}

#if _ENABLE_IDENTITIES

const char *FindArrayKeyTraits<AITaskTypeInfo>::GetKey(const AITaskTypeInfo &a)
{
  return a._type->GetTypeName();
}

bool AITeamMember::RegisterTaskType(AITaskType *taskType)
{
  return _taskTypes.AddUnique(AITaskTypeInfo(taskType)) >= 0;
}

bool AITeamMember::UnregisterTaskType(RString name)
{
  return _taskTypes.DeleteKey(name);
}

const AITaskType *AITeamMember::GetTaskType(RString name) const
{
  int index = _taskTypes.FindKey(name);
  if (index < 0) return NULL;
  return _taskTypes[index]._type;
}

int AITeamMember::AddTask(AITask *task)
{
  task->SetId(_nextTaskId++);

  // increase number of created tasks
  const AITaskType *type = task->GetType();
  if (type)
  {
    // local
    for (int i=0; i<_taskTypes.Size(); i++)
    {
      if (_taskTypes[i]._type == type)
      {
        _taskTypes[i]._created++;
        break;
      }
    }
    // total
    type->OnInstanceCreated();
  }

  return _tasks.Add(task);
}

AITask *AITeamMember::FindTask(int id)
{
  for (int i=0; i<_tasks.Size(); i++)
  {
    if (_tasks[i]->GetId() == id) return _tasks[i];
  }
  return NULL;
}

AITask *AITeamMember::GetTask(int index)
{
  return _tasks[index];
}

const AITaskType *AITeamMember::GetTaskType(int index)
{
  return _taskTypes[index]._type;
}

#endif

// AIAgent implementation

AIAgent::AIAgent(Person *vehicle)
: base(vehicle)
{
  _expensiveThinkFrac = toIntFloor(GRandGen.RandomValue() * 1000);
  // perform first expensive think as soon as possible
  _expensiveThinkTime = Glob.time;

  // assign random identity
  if (vehicle)
  {
    void CreateIdentity(IdentityInfo &result, const Person *person, TargetSide side, int &index);
    int index = INT_MAX * GRandGen.RandomValue();
    IdentityInfo info;
    CreateIdentity(info, vehicle, vehicle->Entity::GetTargetSide(), index);
    Load(info);
  }
}

inline void AIAgent::ExpensiveThinkDone()
{
  // calculate time of next expensive think
  // find start of this second
  // advance to start of next second, advance to our frac
  _expensiveThinkTime = Glob.time.Floor().AddMs(1000 + _expensiveThinkFrac);
}

void AIAgent::ApplyContext(GameState &state)
{
  state.VarSetLocal("_agent", GameValueExt(GetPerson()), true);
}

bool AIAgent::IsInCargo() const
{
  // TODO: inline release version
  return _state == InCargo;
}

void AIAgent::SetInCargo(bool set)
{
  if (set)
  {
    SetState(InCargo);
  }
  else
  {
    // enable change InCargo => Wait
    SetState(Wait, true);
    ClearOperativePlan();
  }
}

void AIAgent::SetSearchTime(Time time)
{
  GetPath().SetSearchTime(time);
}

void AIAgent::OnRespawnFinished()
{
}

TargetSide AIAgent::GetSide() const
{
  if (_person) return _person->GetTargetSide();
  return TSideUnknown;
}

bool AIAgent::IsEnemy(TargetSide side) const
{
  if (!_person) return false;
  TargetSide mySide = _person->GetTargetSide();
// Try to avoid center
/*
  AICenter *center = GWorld->GetCenter(mySide);
  if (center) return center->IsEnemy(side);
*/
  return side == TEnemy || side != mySide && side != TFriendly && side != TCivilian && side >= 0 && side < TSideUnknown;
}

bool AIAgent::IsNeutral( TargetSide side) const
{
// Try to avoid center
/*
  if (!_person) return false;
  TargetSide mySide = _person->GetTargetSide();
  AICenter *center = GWorld->GetCenter(mySide);
  if (center) return center->IsNeutral(side);
*/
  return false;
}

bool AIAgent::IsFriendly(TargetSide side) const
{
  if (!_person) return true;
  TargetSide mySide = _person->GetTargetSide();
// Try to avoid center
/*
  AICenter *center = GWorld->GetCenter(mySide);
  if (center) return center->IsFriendly(side);
*/
  // no center
  return side == TFriendly || side == TCivilian || side == mySide && side != TEnemy && side >= 0 && side <TSideUnknown;
}

int AIAgent::RandomSeed() const
{
  int objId = GetPerson()->GetObjectId().Encode();
  return 233 * objId;
}

float AIAgent::GetMorale() const
{
  // TODO: implement some morale simulation
  return 0;
}

CombatModeRange AIAgent::GetCombatModeBasedOnCommand() const
{
  // TODO: check tasks here to check if any of them is enforcing a combat mode
  return CombatModeRange(CMSafe,CMStealth);
}

CautionRange AIAgent::GetCautionBasedOnCommand() const
{
  // TODO: check tasks here to check if any of them is enforcing a combat mode
  return CautionRange();
}

bool AIAgent::GetCoveringBasedOnCommand() const
{
  return true;
}

bool AIAgent::ForEachTarget(const TargetFunctor &func) const
{
  // TODO: implement ForEach in TargetList
  for (int i=0; i<_targetList.AnyCount(); i++)
  {
    if (func(_targetList.GetAny(i))) return true;
  }
#if _ENABLE_CONVERSATION
  for (int i=0; i<_targetKB.Size(); i++)
  {
    if (func(_targetKB[i])) return true;
  }
#endif
  return false;
}

void AIAgent::OnTargetDeleted(Target *t)
{
#if _ENABLE_CONVERSATION
  // the target may already be deleted before and therefore known
  if (!_targetKB.Find(t))
  {
    _targetKB.Add(new TargetKnowledge(t));
  }
#endif
}

#if _ENABLE_CHEATS

void AIAgent::DebugFSM()
{
  base::DebugFSM();

#if _ENABLE_IDENTITIES && DEBUG_FSM
  for (int i=0; i<_tasks.Size(); i++)
  {
    AITask *task = _tasks[i];
    if (task && task->GetFSM()) task->GetFSM()->Debug();
  }
#endif
}

#endif

void AIAgent::SimulateAI()
{
  if (Glob.time > _expensiveThinkTime)
  {
    ExpensiveThinkDone();
    CreateTargetList();
    CheckEnemies();
  }

  // now only pathfinding is performed, add more functionality later
  if (!IsLocal()) return;

  if (!LSIsAlive()) return;

#if LOG_THINK
  Log("    Agent %s think.", (const char *)GetDebugName());
#endif

  SetWatch();

  EntityAIFull *veh = GetVehicle();

  // if some enemy is near, we have to track near vicinity continuously
  float nearEnemy = 100;
  if (veh && (veh->CommanderUnit() == this || veh->GunnerUnit() == this))
  {
    // if we are not fighting, there is no need for frequent updates
    if (GetCombatMode() > CMSafe && veh->HasSomeWeapons())
    {
      if (_nearestEnemyDist2 < Square(nearEnemy))
      {
        veh->TrackNearTargets(_targetList,nearEnemy);
      }
      veh->TrackLockedTargets(_targetList);
    }
  }

  AITeamMember::SimulateAI();

  // fixed show-term AI reaction
  bool fsmProcessed = false;
  if (HasAI())
  {
    if (_reactionFSM && !IsAIDisabled(DAFSM))
    {
      PROFILE_SCOPE_EX(aiFSM,ai);
      if (PROFILE_SCOPE_NAME(aiFSM).IsActive()) PROFILE_SCOPE_NAME(aiFSM).AddMoreInfo(_reactionFSM->GetDebugState());
      bool done = _reactionFSM->Update(GetPerson());
#if 0
      LogF("%s %.2f: State %s (danger)", cc_cast(GetDebugName()), Glob.time.toFloat(), cc_cast(_reactionFSM->GetStateName()));
      // LogF(" - assigned target %s, watch mode %s", GetTargetAssigned() ? cc_cast(GetTargetAssigned()->GetDebugName()) : "NULL", cc_cast(FindEnumName(_watchMode)));
#endif
      if (done)
      {
        // finished
        _reactionFSM = NULL;
      }
      else
      {
        fsmProcessed = _reactionFSM->IsExclusive(GetPerson());
      }
    }
  }

  if (veh->PilotUnit() != this) return;

  if (!HasAI()) return;

#if _ENABLE_AI

  if (_state == Stopping || _state == Stopped) return;

  ThinkImportance prec = AI::LevelOperative;

  switch (_planningMode)
  {
  case DoNotPlan: case DoNotPlanFormation:
    // do not create new plan (if exist, can continue with the old one)
    if (_noPath) return;
    else
    {
      // operative planning target
      if (_state == Wait || _state == Replan)
      {
        if (!OperPath(prec)) return;
      }

      // operative planning
      CreateOperativePath();
      return;
    }
  case LeaderDirect:
    {
      SetMode(AIUnit::DirectExact);
      Verify(SetState(AIUnit::Init));
      _iter = 0;
      _lastPlan = true;

      // operative planning
      CreateOperativePath();
      return;
    }
  case LeaderPlanned:
  case FormationPlanned:
  case VehiclePlanned:
    {
      // check strategic target
      float error2 = _plannedPosition.Distance2(_wantedPosition);
      float limit2 = Square(0.1) * Position(GetFutureVisualState()).Distance2(_wantedPosition);
      // replan if target position changed by 10% of total distance
      if (error2 > limit2)
      {
        _plannedPosition = _wantedPosition;
        _noPath = true;
        RefreshStrategicPlan();
      }

      // strategic planning
      CreateStrategicPath(prec);

      // operative planning target
      if (_state == Wait || _state == Replan)
      {
        if (!OperPath(prec)) return;
      }

      // operative planning
      CreateOperativePath();
      return;
    }
  default:
    Fail("Planning mode");
    return;
  }

#else
  return;
#endif //_ENABLE_AI
}

// NetworkObject implementation
RString AIAgent::GetDebugName() const
{
  // TODO:
  return _person ? Format("Agent 0x%x", this) : "<No person>";
}

LSError AIAgent::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(AITeamMember::Serialize(ar))

  if (ar.IsSaving())
  {
    RString type = "Agent";
    CHECK(ar.Serialize("teamMemberType", type, 1))
  }

  CHECK(ar.Serialize("expensiveThinkFrac", _expensiveThinkFrac, 1, 0));
  CHECK(::Serialize(ar, "expensiveThinkTime", _expensiveThinkTime, 1, Time(0)));

  // targets
  CHECK(_targetList.Serialize(ar, "targetList", 1))
#if _ENABLE_CONVERSATION
  CHECK(ar.Serialize("targetKB", _targetKB, 1))
#endif

  return LSOK;
}

LSError AIAgent::SaveTeamMemberRef(ParamArchive &ar)
{
  RString type = "Agent";
  CHECK(ar.Serialize("teamMemberType", type, 1))
  return SaveRef(ar);
}

#define AI_AGENT_MSG_LIST(XX) \
  XX(Create, CreateAIAgent) \
  XX(UpdateGeneric, UpdateAIAgent)

DEFINE_NETWORK_OBJECT(AIAgent, AIBrain, AI_AGENT_MSG_LIST)

DEFINE_NET_INDICES_EX(CreateAIAgent, CreateAIBrain, CREATE_AI_AGENT_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateAIAgent, UpdateAIBrain, UPDATE_AI_AGENT_MSG, NoErrorInitialFunc)

NetworkMessageFormat &AIAgent::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  switch (cls)
  {
  case NMCCreate:
    AIBrain::CreateFormat(cls, format);
    CREATE_AI_AGENT_MSG(CreateAIAgent, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    AIBrain::CreateFormat(cls, format);
    UPDATE_AI_AGENT_MSG(UpdateAIAgent, MSG_FORMAT_ERR)
    break;
  default:
    AIBrain::CreateFormat(cls, format);
    break;
  }
  return format;
}

AIAgent *AIAgent::CreateObject(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(CreateAIAgent)

  OLinkPerm<Person> person;
  if (TRANSF_REF_BASE(person, person) != TMOK)
  {
    RptF("Agent not created: Error in transfer");
    return NULL;
  }
  if (!person)
  {
    NetworkId id;
    TRANSF_GET_ID_BASE(person, id);
    RptF("Agent not created: Person %d:%d not found", id.creator.CreatorInt(), id.id);
    return NULL;
  }

  Ref<AIBrain> agent = person->CreateBrain(true);

  NetworkId objectId;
  if (TRANSF_BASE(objectCreator, objectId.creator) != TMOK)
  {
    RptF("Agent not created: Error in transfer");
    return NULL;
  }
  if (TRANSF_BASE(objectId, objectId.id) != TMOK)
  {
    RptF("Agent not created: Error in transfer");
    return NULL;
  }
  agent->SetNetworkId(objectId);
  agent->SetLocal(false);

  agent->TransferMsg(ctx);
  return agent->GetAgent();
}

void AIAgent::DestroyObject()
{
  AddRef();

  // remove from vehicle
  Transport *veh = VehicleAssigned();
  if (veh) veh->UpdateStop();

  // remove from body
  Person *person = _person;
  if (person)
  {
    _person = NULL;
    GWorld->RemoveSensor(person);
    person->SetBrain(NULL);
  }
  Release();
}

TMError AIAgent::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    {
      TMCHECK(AIBrain::TransferMsg(ctx))
      PREPARE_TRANSFER(CreateAIAgent)
    }
    break;
  case NMCUpdateGeneric:
    {
      TMCHECK(AIBrain::TransferMsg(ctx))
      PREPARE_TRANSFER(UpdateAIAgent)
    }
    break;
  default:
    {
      TMCHECK(AIBrain::TransferMsg(ctx))
    }
    break;
  }
  return TMOK;
}

float AIAgent::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = AIBrain::CalculateError(ctx);

  PREPARE_TRANSFER(UpdateAIAgent)

  return error;
}

bool AIAgent::CreateTargetList(bool initialize, bool report)
{
  if (!LSCanTrackTargets()) return false;

  EntityAIFull *vehicle = GetVehicle();
  if (vehicle && !vehicle->GetType()->ScanTargets()) return false;
  
  if (vehicle && (vehicle->CommanderUnit() == this || vehicle->GunnerUnit() == this))
  {
    vehicle->WhatIsVisible(_targetList, initialize);
  }

  _targetList.Manage(this);

  int enemiesDetected = 0;
  int unknownsDetected = 0;
  bool someTarget = false;

  // TODO: use Unknown and Enemy instead of Any
  int n = _targetList.AnyCount();
  for (int i=0; i<n; i++)
  {
    TargetNormal *vtar = _targetList.GetAny(i);

    if (vtar->IsKnownBySome())
    {
      if (vtar->side == TSideUnknown)
      {
        someTarget = true;
        unknownsDetected++;
      }
      else if (IsEnemy(vtar->side))
      {
        Threat threat = vtar->type->GetDamagePerMinute(Square(200), 1);
        if (threat[VSoft] + threat[VArmor] + threat[VAir] > 0)
        { // calculate only dangerous enemies
          enemiesDetected++;
        }
        someTarget=true;
      }
    }
  }

	// prepare subjective costs
	// TODO: subject. cost - separate Enemy/Friendly
	for (int i=0; i<_targetList.AnyCount(); i++)
	{
		TargetNormal *tar = _targetList.GetAny(i);

    // tar->State will return at most TargetAlive for anything not IsEnemy
		if(
			!tar->idExact || !tar->IsKnown() ||
			tar->destroyed || tar->vanished ||
			tar->State(this)<TargetEnemy
		)
		{

		  if( tar->side==TSideUnknown )
		  {
			  tar->damagePerMinute=0;
			  tar->subjectiveCost=0;
		  }
		  else if (tar->side!=TCivilian && IsFriendly(tar->side))
		  {
			  // friendly - no need to test it
			  tar->damagePerMinute=0;
			  tar->subjectiveCost=-1e10;
		  }
		  else
		  {
		    // we want emoty/neutral to be listed before friendly
			  // reset subjective cost
			  tar->damagePerMinute=0;
			  tar->subjectiveCost=-1e5;
		  }

			continue;
		}
		else
		{
		  DoAssert (IsEnemy(tar->side));
		  // calculate damagePerMinute and subjectiveCost
		  tar->damagePerMinute=GetDamagePerMinute(tar);
		  tar->subjectiveCost=GetSubjectiveCost(tar);
		}


	}

  // sort
  _targetList.QSort(TargetNormal::BetterTarget());
  _targetList.CompactIfNeeded();

// LogF("Agent %s, %d targets, %d enemies, %d unknown", cc_cast(GetDebugName()), _targetList.Size(), enemiesDetected, unknownsDetected);

  return someTarget;
}

float AIAgent::GetDamagePerMinute(TargetNormal *tar) const
{
  // calculate how much are assigned unit able to destroy this target
  // TODO: check only when I have this target assigned

  EntityAI *veh = GetVehicle();
  if (!veh || !IsUnit()) return 0;

  // if (!assigned) return 0;
  if (false) return 0;

  const VehicleType *type = tar->type;
  VehicleKind kind = type->GetKind();
  float dist2 = tar->GetPosition().Distance2(veh->FutureVisualState().Position());
  Threat threat = veh->GetDamagePerMinute(dist2, 1.0);
  return threat[kind];
}

float AIAgent::GetSubjectiveCost(TargetNormal *tar) const
{
  const VehicleType *type = tar->type;
  float baseCost = type->GetCost();

  EntityAI *veh = GetVehicle();
  if (!veh || !IsUnit()) return baseCost;

  Object *obj = tar->idExact;
  EntityAIFull *enemy = dyn_cast<EntityAIFull>(obj);
  if (!enemy) return baseCost;

  TurretContext context;
  if (!enemy->GetPrimaryGunnerTurret(context) || !enemy->IsAbleToFire(context)) return baseCost;

  VehicleKind vehKind = veh->GetType()->GetKind();
  float dist2 = veh->FutureVisualState().Position().Distance2(tar->position);

  Threat threat = type->GetDamagePerMinute(dist2, 1.0);
  float damagePerMinute = threat[vehKind];
  if (damagePerMinute <= 0) return baseCost;

  float lTimeToLive = veh->GetArmor() * 60 / damagePerMinute;
  float danger = GetTimeToLive() / lTimeToLive;
  // note: lTimeToLive may be higher that timeToLive if tgt is relatively new
  saturateMin(danger, 10);
  return baseCost + danger * veh->GetType()->GetCost();
}

void AITeam::SimulateAI()
{
  AITeamMember::SimulateAI();
}

void AITeam::ApplyContext(GameState &state)
{
  state.VarSetLocal("_team", GameValueExt(this), true);
}

#define AI_TEAM_MSG_LIST(XX) \
  XX(Create, CreateAITeam) \
  XX(UpdateGeneric, UpdateAITeam)

DEFINE_NETWORK_OBJECT(AITeam, NetworkObject, AI_TEAM_MSG_LIST)

DEFINE_NET_INDICES_EX(CreateAITeam, NetworkObject, CREATE_AI_TEAM_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateAITeam, NetworkObject, UPDATE_AI_TEAM_MSG, NoErrorInitialFunc)

NetworkMessageFormat &AITeam::CreateFormat(
  NetworkMessageClass cls,
  NetworkMessageFormat &format)
{
  switch (cls)
  {
  case NMCCreate:
    NetworkObject::CreateFormat(cls, format);
    CREATE_AI_TEAM_MSG(CreateAITeam, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    NetworkObject::CreateFormat(cls, format);
    UPDATE_AI_TEAM_MSG(UpdateAITeam, MSG_FORMAT_ERR)
    break;
  default:
    NetworkObject::CreateFormat(cls, format);
    break;
  }
  return format;
}

AITeam *AITeam::CreateObject(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(CreateAITeam)

  NetworkId objectId;
  if (TRANSF_BASE(objectCreator, objectId.creator) != TMOK)
  {
    RptF("Team not created: Error in transfer");
    return NULL;
  }
  if (TRANSF_BASE(objectId, objectId.id) != TMOK)
  {
    RptF("Team not created: Error in transfer");
    return NULL;
  }

  AITeam *team = new AITeam();
  team->SetNetworkId(objectId);
  team->SetLocal(false);

  team->TransferMsg(ctx);

  GWorld->AddTeam(team);
  return team;
}

void AITeam::DestroyObject()
{
  GWorld->RemoveTeam(this);
}

TMError AITeam::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    {
      TMCHECK(NetworkObject::TransferMsg(ctx))
      {
        PREPARE_TRANSFER(CreateAITeam)
        TRANSF(typeName)
        TRANSF(name)
      }
    }
    break;
  case NMCUpdateGeneric:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateAITeam)
      TRANSF_REFS(teamMembers)
      TRANSF_REF(teamLeader)
      TRANSF(formation)
    }
    break;
  default:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    break;
  }
  return TMOK;
}

float AITeam::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = NetworkObject::CalculateError(ctx);

  PREPARE_TRANSFER(UpdateAITeam)
  for (int i=0; i<message->_teamMembers.Size(); i++)
  {
    NetworkId id = message->_teamMembers[i];
    if (id.IsNull()) continue;
    bool found = false;
    for (int j=0; j<_teamMembers.Size(); j++)
      if (_teamMembers[j] && _teamMembers[j]->GetNetworkId() == id)
      {
        found = true;
        break;
      }
    if (!found) error += ERR_COEF_STRUCTURE;
  }
  ICALCERR_NEQREF_PERM(AITeamMember, teamLeader, ERR_COEF_MODE)
  ICALCERR_NEQ(RString, formation, AIUpdateErrorCoef * ERR_COEF_MODE)

  return error;
}

LSError AITeam::Serialize(ParamArchive &ar)
{
  CHECK(AITeamMember::Serialize(ar))

  if (ar.IsSaving())
  {
    RString type = "Team";
    CHECK(ar.Serialize("teamMemberType", type, 1))
  }

  CHECK(ar.SerializeRef("TeamLeader", _teamLeader, 1))
  CHECK(ar.Serialize("typeName", _typeName, 1))
  CHECK(ar.Serialize("name", _name, 1))
  CHECK(ar.Serialize("formation", _formation, 1))
  CHECK(ar.Serialize("id", _id, 1))

  CHECK(ar.SerializeRefs("TeamMembers", _teamMembers, 1))

  //serialize NetworkObject
  NetworkObject::Serialize(ar); //base

  if ( ar.IsLoading() && (ar.GetPass()==ParamArchive::PassSecond) 
    && !GetNetworkId().IsNull() )
  {
    //Calling CreateObject is not sufficient
    //GetNetworkManager().CreateObject(this, false /*not setNetworkId*/);
    // we should call appropriate CreateXXX methods when everything is serialized (e.g. subobjects,...)
    // store the "action" to see, what should be created "at least"
    GetNetworkManager().RememberSerializedObject(this);
  }

  return LSOK;
}

LSError AITeam::SaveTeamMemberRef(ParamArchive &ar)
{
  RString type = "Team";
  CHECK(ar.Serialize("teamMemberType", type, 1))
  CHECK(ar.Serialize("id", _id, 1))
  return LSOK;
}

AITeamMember *AITeam::LoadRef(ParamArchive &ar)
{
  int id;
  if (!ar.Serialize("id", id, 1)) return NULL;
  return GWorld->GetTeam(id);
}

#else

NetworkMessage *CreateNetworkMessageUpdateAIAgent() {return NULL;}
void UpdateMessageFormatUpdateAIAgent(NetworkMessageFormatBase *,int &) {}
NetworkMessage *CreateNetworkMessageCreateAIAgent() {return NULL;}
void UpdateMessageFormatCreateAIAgent(NetworkMessageFormatBase *,int &) {}

NetworkMessage *CreateNetworkMessageUpdateAITeam() {return NULL;}
void UpdateMessageFormatUpdateAITeam(NetworkMessageFormatBase *,int &) {}
NetworkMessage *CreateNetworkMessageCreateAITeam() {return NULL;}
void UpdateMessageFormatCreateAITeam(NetworkMessageFormatBase *,int &) {}

#endif

// AIUnit implementation

AIUnit::AIUnit(Person *vehicle)
  : AIBrain(vehicle)
{
  _subgroup = NULL;
  _id = 0;

  _expensiveThinkFrac = toIntFloor(GRandGen.RandomValue()*1000);
  // perform first expensive think as soon as possible
  _expensiveThinkTime = Glob.time;

  _isAway = false;
  _gearActionProcessed = false;

  _lastFuelState = RSNormal;
  _lastHealthState = RSNormal;
  _lastArmorState = RSNormal;
  _lastAmmoState = RSNormal;

  _fuelCriticalTime = Time(0);
  _healthCriticalTime = Time(0);
  _dammageCriticalTime = Time(0);
  _ammoCriticalTime = Time(0);

  _formOffset = VZero;
  _formationPos = VZero;
  _formationAngle = 0;

  _housePos = -1;

#if _ENABLE_CHEATS
  _fsmDiagSize = 0;
  _fsmDiagColor = PackedColor(Color(1, 1, 1, 1));
#endif

  _fsmHideFromR = VZero;
  _fsmHideFromL = VZero;
  _fsmTimeStart = Time(0);
  _fsmTimeNow = Time(0);
  _fsmTimeMin = 3.0f;
  _fsmTimeMax = 8.0f;
  _fsmDelay = 1.0f;
  for (int i=0; i<lenof(_fsmTargets); i++)
    _fsmTargets[i] = VZero;
}

inline void AIUnit::ExpensiveThinkDone()
{
  // calculate time of next expensive think
  // find start of this second
  // advance to start of next second, advance to our frac
  _expensiveThinkTime = Glob.time.Floor().AddMs(1000+_expensiveThinkFrac);
  #if 0
  LogF
  (
    "%.3f: Think %s (%d)",
    Glob.time-Time(0),(const char *)GetDebugName(),_expensiveThinkFrac
  );
  #endif
}


AIUnit::~AIUnit()
{
  //DoAssert(GetLifeState() != LifeStateDeadInRespawn);
/*
  NetworkId id = GetNetworkId();
  if (!id.IsNull())
  {
    RptF("Unit %d:%d destroyed", id.creator.CreatorInt(), id.id);
  }
*/
}

static const EnumName ResourceStateNames[]=
{
  EnumName(AIUnit::RSNormal, "NORMAL"),
  EnumName(AIUnit::RSLow, "LOW"),
  EnumName(AIUnit::RSCritical, "CRITICAL"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::ResourceState dummy)
{
  return ResourceStateNames;
}

static const EnumName UnitModeNames[]=
{
  EnumName(AIUnit::DirectNormal, "DIRECT NORMAL"),
  EnumName(AIUnit::DirectExact, "DIRECT EXACT"),
  EnumName(AIUnit::Normal, "NORMAL"),
  EnumName(AIUnit::Exact, "EXACT"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::Mode dummy)
{
  return UnitModeNames;
}

static const EnumName UnitPlanningModeNames[]=
{
  EnumName(AIUnit::DoNotPlan, "DoNotPlan"),
  EnumName(AIUnit::DoNotPlanFormation, "DoNotPlanFormation"),
  EnumName(AIUnit::LeaderPlanned, "LEADER PLANNED"),
  EnumName(AIUnit::LeaderDirect, "LEADER DIRECT"),
  EnumName(AIUnit::FormationPlanned, "FORMATION PLANNED"),
  EnumName(AIUnit::VehiclePlanned, "VEHICLE PLANNED"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::PlanningMode dummy)
{
  return UnitPlanningModeNames;
}

static const EnumName UnitStateNames[]=
{
  EnumName(AIUnit::Wait, "WAIT"),
  EnumName(AIUnit::Init, "INIT"),
  EnumName(AIUnit::InitFailed, "INITFAILED"),
  EnumName(AIUnit::Busy, "BUSY"),
  EnumName(AIUnit::Completed, "OK"),
  EnumName(AIUnit::Delay, "DELAY"),
  EnumName(AIUnit::InCargo, "CARGO"),
  EnumName(AIUnit::Stopping, "STOPPING"),
  EnumName(AIUnit::Replan, "REPLAN"),
  EnumName(AIUnit::Stopped, "STOPPED"),
  EnumName(AIUnit::Planning, "PLANNING"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::State dummy)
{
  return UnitStateNames;
}

static const EnumName UnitWatchModeNames[]=
{
  EnumName(AIUnit::WMNo, "NO"),
  EnumName(AIUnit::WMDir, "DIR"),
  EnumName(AIUnit::WMPos, "POS"),
  EnumName(AIUnit::WMPosGlance, "POS-GLANCE"),
  EnumName(AIUnit::WMTgt, "TGT"),
  EnumName(AIUnit::WMTgtGlance, "TGT-GLANCE"),
  EnumName(AIUnit::WMAround, "AROUND"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::WatchMode dummy)
{
  return UnitWatchModeNames;
}

static const EnumName LifeStateNames[]=
{
  EnumName(LifeStateAlive, "ALIVE"),
  EnumName(LifeStateDead, "DEAD"),
  EnumName(LifeStateDeadInRespawn, "DEAD-RESPAWN"),
  EnumName(LifeStateDeadSwitching, "DEAD-SWITCHING"),
  EnumName(LifeStateAsleep, "ASLEEP"),
  EnumName(LifeStateUnconscious, "UNCONSCIOUS"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(LifeState dummy)
{
  return LifeStateNames;
}

static const EnumName DisabledAINames[]=
{
  EnumName(AIUnit::DATarget, "TARGET"),
  EnumName(AIUnit::DAAutoTarget, "AUTOTARGET"),
  EnumName(AIUnit::DAMove, "MOVE"),
  EnumName(AIUnit::DAAnim, "ANIM"),
  EnumName(AIUnit::DATeamSwitch, "TEAMSWITCH"),
#if _VBS3
  EnumName(AIUnit::DAPathPlan, "PATHPLAN"),
#endif
  EnumName(AIUnit::DAFSM, "FSM"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AIUnit::DisabledAI dummy)
{
  return DisabledAINames;
}

LSError AIUnitInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("name", _name, 1))
  CHECK(ar.Serialize("experience", _experience, 1, 0))
  CHECK(ar.SerializeEnum("rank", _rank, 1, RankPrivate))
  CHECK(ar.Serialize("face", _face, 1, ""))
  CHECK(ar.Serialize("glasses", _glasses, 1, "None"))
  CHECK(ar.Serialize("speaker", _speaker, 1, ""))
  CHECK(ar.Serialize("pitch", _pitch, 1, 1.0))
  return LSOK;
}

/*!
\patch 5126 Date 1/31/2007 by Ondra
- Fixed: status of disableAI is now saved and loaded.
*/
LSError AIUnit::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))

  // structure
  CHECK(ar.SerializeRef("Subgroup", _subgroup, 1))

  // info
  CHECK(ar.Serialize("id", _id, 1))

  CHECK(ar.Serialize("isAway", _isAway, 1, false))
  CHECK(ar.Serialize("gearActionProcessed", _gearActionProcessed, 1, false))

  CHECK(ar.SerializeRef("TargetEngage", _targetEngage, 1))

  CHECK(ar.Serialize("expensiveThinkFrac",_expensiveThinkFrac,1,0));
  CHECK(::Serialize(ar, "expensiveThinkTime",_expensiveThinkTime,1,Time(0)));

  CHECK(ar.Serialize("formationAngle", _formationAngle, 1))
  CHECK(::Serialize(ar, "formationPos", _formationPos, 1))
  CHECK(::Serialize(ar, "formOffset", _formOffset, 1))

  CHECK(ar.SerializeEnum("lastFuelState", _lastFuelState, 1, RSNormal))
  CHECK(ar.SerializeEnum("lastHealthState", _lastHealthState, 1, RSNormal))
  CHECK(ar.SerializeEnum("lastArmorState", _lastArmorState, 1, RSNormal))
  CHECK(ar.SerializeEnum("lastAmmoState", _lastAmmoState, 1, RSNormal))

  CHECK(::Serialize(ar, "fuelCriticalTime", _fuelCriticalTime, 1, Time(0)))
  CHECK(::Serialize(ar, "healthCriticalTime", _healthCriticalTime, 1, Time(0)))
  CHECK(::Serialize(ar, "dammageCriticalTime", _dammageCriticalTime, 1, Time(0)))
  CHECK(::Serialize(ar, "ammoCriticalTime", _ammoCriticalTime, 1, Time(0)))

  // variables used by FSMs
  CHECK(::Serialize(ar, "fsmHideFromR", _fsmHideFromR, 1, VZero))
  CHECK(::Serialize(ar, "fsmHideFromL", _fsmHideFromL, 1, VZero))
  CHECK(::Serialize(ar, "fsmTimeStart", _fsmTimeStart, 1, Time(0)))
  CHECK(::Serialize(ar, "fsmTimeNow", _fsmTimeNow, 1, Time(0)))
  CHECK(ar.Serialize("fsmTimeMin", _fsmTimeMin, 1, 3.0f))
  CHECK(ar.Serialize("fsmTimeMax", _fsmTimeMax, 1, 8.0f))
  CHECK(ar.Serialize("fsmDelay", _fsmDelay, 1, 1.0))
  CHECK(ar.SerializeRef("fsmDummyTarget", _fsmDummyTarget, 1))
  for (int i=0; i<lenof(_fsmTargets); i++)
  {
    RString name = Format("fsmTargets%d", i);
    CHECK(::Serialize(ar, name, _fsmTargets[i], 1, VZero))
  }
  CHECK(ar.Serialize("hcGroups", _hcGroups, 1))

  return LSOK;
}

#define AI_UNIT_MSG_LIST(XX) \
  XX(Create, CreateAIUnit) \
  XX(UpdateGeneric, UpdateAIUnit)

DEFINE_NETWORK_OBJECT(AIUnit, AIBrain, AI_UNIT_MSG_LIST)

DEFINE_NET_INDICES_EX(CreateAIUnit, CreateAIBrain, CREATE_AI_UNIT_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateAIUnit, UpdateAIBrain, UPDATE_AI_UNIT_MSG, NoErrorInitialFunc)

NetworkMessageFormat &AIUnit::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  switch (cls)
  {
  case NMCCreate:
    AIBrain::CreateFormat(cls, format);
    CREATE_AI_UNIT_MSG(CreateAIUnit, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    AIBrain::CreateFormat(cls, format);
    UPDATE_AI_UNIT_MSG(UpdateAIUnit, MSG_FORMAT_ERR)
    break;
  default:
    AIBrain::CreateFormat(cls, format);
    break;
  }
  return format;
}

AIUnit *AIUnit::CreateObject(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(CreateAIUnit)

  OLinkPerm<Person> person;
  if (TRANSF_REF_BASE(person, person) != TMOK)
  {
    RptF("Unit not created: Error in transfer");
    return NULL;
  }
  if (!person)
  {
    NetworkId id;
    TRANSF_GET_ID_BASE(person, id);
    RptF("Unit not created: Person %d:%d not found", id.creator.CreatorInt(), id.id);
    return NULL;
  }
  OLinkPerm<AISubgroup> subgrp;
  if (TRANSF_REF_BASE(subgroup, subgrp) != TMOK)
  {
    RptF("Unit not created: Error in transfer");
    return NULL;
  }
  if (!subgrp)
  {
    NetworkId id;
    TRANSF_GET_ID_BASE(subgroup, id);
    RptF("Unit not created: Subgroup %d:%d not found", id.creator.CreatorInt(), id.id);
    return NULL;
  }
  AIGroup *grp = subgrp->GetGroup();
  if (!grp)
  {
    RptF("Unit not created: Subgroup %d:%d has no group", subgrp->GetNetworkId().creator.CreatorInt(), subgrp->GetNetworkId().id);
    return NULL;
  }
  int id;
  if (TRANSF_BASE(id, id) != TMOK)
  {
    RptF("Unit not created: Error in transfer");
    return NULL;
  }
  AIUnit *unit = new AIUnit(person);
  person->SetBrain(unit);
  unit->_id = id;
  grp->SetUnit(id - 1, unit);
  subgrp->AddUnit(unit);

  // temporary select leader (avoid diag output)
  if (!grp->Leader()) grp->GetCenter()->SelectLeader(grp);
  if (!subgrp->Leader()) subgrp->SelectLeader();

  NetworkId objectId;
  if (TRANSF_BASE(objectCreator, objectId.creator) != TMOK)
  {
    RptF("Unit not created: Error in transfer");
    return NULL;
  }
  if (TRANSF_BASE(objectId, objectId.id) != TMOK)
  {
    RptF("Unit not created: Error in transfer");
    return NULL;
  }
  unit->SetNetworkId(objectId);
  unit->SetLocal(false);

  unit->TransferMsg(ctx);

  // person did not run its own Init yet, as it was not initialized fully, we need to run it now
  person->OnInitEvent();
  // TODO: init command from the editor should be processed now

  // once init was done, we can apply identity
  AIUnitInfo &info = person->GetInfo();
  if (info._squadPicturePath.GetLength() > 0)
  {
    RString GetClientCustomFilesDir();
    RString picture = GetClientCustomFilesDir() + info._squadPicturePath;
    picture.Lower();
    if (QIFileFunctions::FileExists(picture))
    {
      info._squadPicture = GlobLoadTexture(picture);
    }
  }

  person->SetFace();
  person->SetGlasses();
  unit->SetSpeaker();

  return unit;
}

void AIUnit::DestroyObject()
{
  AddRef();

  // remove from AI structure
  Ref<AISubgroup> oldSubgroup = (AISubgroup*)_subgroup;
  Ref<AIGroup> oldGroup = GetGroup();
  bool bSubgroupLeader = oldSubgroup && oldSubgroup->Leader() == this;
  bool bGroupLeader = oldGroup && oldGroup->Leader() == this;
  if (oldGroup)
  {
    oldGroup->UnitRemoved(this);
    _subgroup = NULL;
    _id = 0;
  }
  if (bGroupLeader) oldGroup->GetCenter()->SelectLeader(oldGroup);
  if (bSubgroupLeader) oldSubgroup->SelectLeader();

  // remove from vehicle
  Transport *veh = VehicleAssigned();
  if (veh) veh->UpdateStop();

  Person *person = _person;
  if (person)
  {
    _person = NULL;
    GWorld->RemoveSensor(person);
    person->SetBrain(NULL);
  }
  Release();
}

/*!
\patch 1.04 Date 07/13/2001 by Jirka
  - Fixed: squad pictures and titles missing on client machines
\patch_internal 1.04 Date 07/13/2001 by Jirka
  - added transfer of AIUnitInfo::_squadTitle and AIUnitInfo::_squadPicture
\patch_internal 1.12 Date 08/06/2001 by Jirka
  - Fixed: improved reaction when bad order of incoming messages
*/
TMError AIUnit::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    {
      TMCHECK(AIBrain::TransferMsg(ctx))
      PREPARE_TRANSFER(CreateAIUnit)
      if (ctx.IsSending())
      {
        TRANSF_REF(subgroup)
        TRANSF(id)
      }
    }
    break;
  case NMCUpdateGeneric:
    {
      TMCHECK(AIBrain::TransferMsg(ctx))
      PREPARE_TRANSFER(UpdateAIUnit)

      TRANSF(formationAngle)
      TRANSF(formationPos)
    }
    break;
  default:
    {
      TMCHECK(AIBrain::TransferMsg(ctx))
    }
    break;
  }
  return TMOK;
}

float AIUnit::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = AIBrain::CalculateError(ctx);

  PREPARE_TRANSFER(UpdateAIUnit)

  return error;
}

#if _ENABLE_DIRECT_MESSAGES

bool AIUnit::UseRadio(AIUnit *receiver) const
{
  if (!GetPerson()->GetRadio()) return true;
  if (!IsFreeSoldier() || !receiver->IsFreeSoldier()) return true;

  const float limit2 = Square(20.0f);
  Assert(receiver);
  return Position().Distance2(receiver->Position()) > limit2;
}

bool AIUnit::UseRadio(AIGroup *group, const OLinkPermNOArray(AIUnit) &list) const
{
  if (!GetPerson()->GetRadio()) return true;
  if (!IsFreeSoldier()) return true;
  for (int i=0; i<list.Size(); i++)
  {
    AIUnit *unit = list[i];
    if (!unit) continue;
    if (unit == this) continue;
    if (UseRadio(unit)) return true;
  }
  return false;
}

#endif

TargetSide AIUnit::GetSide() const
{
  AIGroup *grp = GetGroup();
  if (!grp) return TSideUnknown;
  AICenter *center = grp->GetCenter();
  if (!center) return TSideUnknown;
  return center->GetSide();
}

bool AIUnit::IsEnemy(TargetSide side) const
{
  AIGroup *grp = GetGroup();
  if (!grp) return false;
  AICenter *center = grp->GetCenter();
  if (!center) return false;
  return center->IsEnemy(side);
}

bool AIUnit::IsNeutral( TargetSide side) const
{
  AIGroup *grp = GetGroup();
  if (!grp) return false;
  AICenter *center = grp->GetCenter();
  if (!center) return false;
  return center->IsNeutral(side);
}

bool AIUnit::IsFriendly(TargetSide side) const
{
  AIGroup *grp = GetGroup();
  if (!grp) return true;
  AICenter *center = grp->GetCenter();
  if (!center) return true;
  return center->IsFriendly(side);
}

bool AIUnit::IsInCargo() const
{
  // TODO: inline release version
  //hotfix news:hsgpo3$8d4$1@new-server.localdomain
  //in MP server has not updated state ans therefore is forces freelook
  return (GetVehicleIn() && _state == InCargo);
  //return _state == InCargo;
}

void AIUnit::SetInCargo(bool set)
{
  if (set)
  {
    SetState(InCargo);
  }
  else
  {
    // enable change InCargo => Wait
    SetState(Wait, true);
    ClearOperativePlan();
  }
}

void AIUnit::OnNewTarget(TargetNormal *target, Time knownSince)
{
  AIGroup * grp = GetGroup();
  grp->RecomputeTargetSubjectiveCost(target,knownSince);
  base::OnNewTarget(target,knownSince);
}

void AIUnit::SetSearchTime(Time time)
{
  GetPath().SetSearchTime(time);
}

void AIUnit::OnRespawnFinished()
{
  SendAnswer(AI::ReportPosition);
}

void AIUnit::ExposureChanged(int x, int z, float optimistic)
{
#if _ENABLE_AI
  float exposure = fabs(optimistic);
  if (exposure < VALID_EXPOSURE_CHANGE) return;

  if
  (
    _planner->IsOnPath(
      x, z,
      _planner->FindBestIndex(Position(GetFutureVisualState())),
      _planner->GetPlanSize() - 1
    )
  ) _exposureChange += exposure;
#endif
}

// check size of formation

Vector3 AIUnit::GetFormationAbsoluteFreeY(float estT, bool init) const
{
  // most common case: no need to calculate formation size when there is no offset
  AIUnit *leader = GetSubgroup()->Leader();

  //move formation to leader space
  Vector3 formationPosIdeal = leader ? _formationPos - leader->GetFormationPosition() : _formationPos;
  
  formationPosIdeal += _formOffset;

  //no leader, nowhere to move
  if(!leader || !leader->GetVehicle()) return GetVehicle()->FutureVisualState().Position();

  EntityAIFull *lVehicle = leader->GetVehicle();
    
  Matrix4 toFormation;
  Vector3Val formDir = GetSubgroup()->GetFormationDirection();
  toFormation.SetDirectionAndUp(formDir,VUp);
  toFormation.SetPosition(lVehicle->FutureVisualState().Position());

  Vector3Val formationPosIdealAbs = toFormation.FastTransform(formationPosIdeal);
  if (init)
    return formationPosIdealAbs;

  Vector3 formPosAbs;
  if (
    GetCombatMode()==CMAware && Glob.header.CheckAIFeature(AIFAwareFormationSoft) ||
    GetCombatMode()>=CMCombat && Glob.header.CheckAIFeature(AIFCombatFormationSoft)
  )
  { // direct and immediate formation computation
    AIUnit *ledBy;
    formPosAbs = GetSubgroup()->GetFormationPosImmediate(this,ledBy);
    formPosAbs += toFormation.Rotate(_formOffset);

    Assert(GetVehicle()->FutureVisualState().Position().SquareSize()>0);
    Assert(lVehicle->FutureVisualState().Position().SquareSize()>0);

    Vector3Val posAbs = GetVehicle()->FutureVisualState().Position();
    // check if the unit is a lot closer to its "ideal formation" position than to its "immediate formation" position
    if (formPosAbs.Distance(posAbs)>formationPosIdealAbs.Distance(posAbs)*2.0f + floatMax(GetVehicle()->GetFormationX(),GetVehicle()->GetFormationZ()))
    {
      // if it is, use the ideal position. This is important when the base unit used for immediate formation is
      // currently returning to formation. It is considered a part of formation, yet it can be very far away
      formPosAbs = formationPosIdealAbs;
      _ledBy = leader;
    }
    else 
    {
      // if the immediate position is behind the ideal position, move the immediate position slightly forward
      // this is done to make subordinates react faster to the leader movement
      float immediateBehindIdeal = (formationPosIdealAbs-formPosAbs)*toFormation.Direction();
      float formZ = GetVehicle()->GetFormationZ();
      if (immediateBehindIdeal>0 && immediateBehindIdeal<formZ*2)
      {
        // do this only when not behind too much, to prevent trying to catch up with units far away, esp. when they are falling back
        float behindAdjusted = formZ-fabs(immediateBehindIdeal-formZ);
        formPosAbs += behindAdjusted*0.66f*formDir;
      }
      _ledBy = ledBy;
    }
  }
  else
  {
    formPosAbs = formationPosIdealAbs;
    _ledBy = leader;
  }
  
  formPosAbs += _ledBy->GetVehicle()->PredictFormationSpeed()*estT;
  return formPosAbs;
}

AIUnit *AIUnit::LedBy()
{
  AISubgroup *subgrp = GetSubgroup();
  if (!subgrp) return this;
  AIUnit *ledBy = _ledBy;
  // only a cached value - perform basic validation against possible changes
  if (!ledBy || ledBy->GetSubgroup()!=subgrp || ledBy==this)
  {
    ledBy = subgrp->Leader();
  }
  return ledBy;
}

Vector3 AIUnit::GetFormationRelative(bool init) const
{
  if (!GetSubgroup())
    return VZero;
  AIUnit *leader = GetSubgroup()->Leader();
  Vector3 formationPosIdeal = leader ? _formationPos - leader->GetFormationPosition() : _formationPos;
  if (init)
    return formationPosIdeal;

  EntityAIFull *lVehicle = leader->GetVehicle();
    
  Matrix4 transform;
  Vector3Val formDir = GetSubgroup()->GetFormationDirection();
  transform.SetDirectionAndUp(formDir,VUp);
  transform.SetPosition(lVehicle->FutureVisualState().Position());


  Vector3 formPosAbs = GetFormationAbsoluteFreeY(0,init);
  
  Matrix4 invTransform(MInverseRotation,transform);
  return invTransform.FastTransform(formPosAbs);
}

Vector3 AIUnit::GetFormationAbsolute(float estT, bool init) const
{
  EntityAIFull *vehicle = GetVehicle();
  
  if( !_subgroup ) {Fail("No subgroup");return vehicle->FutureVisualState().Position();}
  AIUnit *leader = _subgroup->Leader();
  if( !leader )  {Fail("No leader");return vehicle->FutureVisualState().Position();}

  // commander of the vehicle is the formation member
  AIBrain *commander = vehicle->CommanderUnit();
  const AIUnit *commanderUnit = commander ? commander->GetUnit() : NULL;
  if (!commanderUnit) commanderUnit = this;

  Vector3 pos = commanderUnit->GetFormationAbsoluteFreeY(estT,init);
  if( GetVehicle()->GetType()->GetKind()!=VAir )
  {
  // absolute formation position
    pos[1]=GLandscape->SurfaceYAboveWater(pos[0],pos[2]);
  }
  else
  {
    float landY=GLandscape->SurfaceYAboveWater(pos[0],pos[2]);
    saturateMax(pos[1],landY+25);
  }
  return pos;
}

float AIUnit::GetBehindFormation() const
{
  if( !_subgroup ) {Fail("No subgroup");return 0;}
  AIUnit *leader = _subgroup->Leader();
  if( !leader )  {Fail("No leader");return 0;}
  // absolute formation position
  // suppose leader is formation leader
  EntityAI *lVehicle = leader->GetVehicle();

  EntityAIFull *vehicle = GetVehicle();
  // formation leader is never behind the formation
  if (vehicle==lVehicle) return 0;

  // commander of the vehicle is the formation member
  AIBrain *commander = vehicle->CommanderUnit();
  const AIUnit *commanderUnit = commander ? commander->GetUnit() : NULL;
  if (!commanderUnit) commanderUnit = this;

  AISubgroup *subgrp = leader->GetSubgroup();
  // orientation space of the formation (determined by the leader position)
  Vector3 formDir = subgrp->GetMoveDirection();

  Vector3 expectedPosAbs = commanderUnit->GetFormationAbsoluteFreeY();
  // expected and current position relative to the formation
  float behind = (expectedPosAbs-vehicle->FutureVisualState().Position())*formDir;
  if (!IsAnyPlayer()) // pretending the player is not forward does not help moving, it could even prevent it
  {
    float formZ = GetVehicle()->GetFormationZ();
    const float maxForward =formZ*5.0f; // 25m for a soldier (formZ = 5m)
    if (behind<-maxForward) behind = -maxForward+(-maxForward-behind);
  }
  return behind;
}


float AIUnit::GetCautiousInCombat() const
{
  // caution or not? Depending on a context
  float caution = 0.5;
  if (Glob.time - 30.0f < GetGroup()->GetDisclosed())
  {
    // disclosed recently - this most likely means enemy is firing at us
    caution = 1.0f;
  }
  
  CautionRange range = GetCautionBasedOnCommand();

  if  (caution<range.beg) caution = range.beg;
  if  (caution>range.end) caution = range.end;
  
  return caution;
}

#define HEALTH_LOW      0.6F
#define HEALTH_CRITICAL 0.5F

#define ARMOR_LOW       0.6F
#define ARMOR_CRITICAL  0.5F

#define FUEL_LOW        0.3F
#define FUEL_CRITICAL   0.1F

#define AMMO_LOW        0.4F
#define AMMO_CRITICAL   0.2F

AIUnit::ResourceState AIUnit::GetFuelState() const
{
  if (IsSoldier()) return RSNormal;

  float curFuel = GetVehicle()->GetFuel();
  float maxFuel = GetVehicle()->GetType()->GetFuelCapacity();

  if (curFuel < FUEL_CRITICAL * maxFuel) return RSCritical;
  if (curFuel < FUEL_LOW * maxFuel) return RSLow;
  return RSNormal;
}

AIUnit::ResourceState AIUnit::GetHealthState() const
{
  float health = 1.0 - GetPerson()->NeedsAmbulance();

  if (health < HEALTH_CRITICAL || GetLifeState() == LifeStateUnconscious) return RSCritical;
  if (health < HEALTH_LOW) return RSLow;
  return RSNormal;
}

AIUnit::ResourceState AIUnit::GetArmorState() const
{
  if (IsSoldier()) return RSNormal;

  float armor = 1.0 - GetVehicle()->NeedsRepair();

  if (armor < ARMOR_CRITICAL) return RSCritical;
  if (armor < ARMOR_LOW) return RSLow;
  return RSNormal;
}

AIUnit::ResourceState AIUnit::GetAmmoState() const
{
  if (IsFreeSoldier())
  {
    float needs = GetPerson()->NeedsInfantryRearm();
    if (needs > 1 - AMMO_CRITICAL) return RSCritical;
    if (needs > 1 - AMMO_LOW) return RSLow;
    return RSNormal;
  }

  // used for soldiers in vehicle - ammo state of vehicle
  if (!IsUnit()) return RSNormal;

  float curAmmo = 0;
  float maxAmmo = 0;
  EntityAIFull *veh = GetVehicle();
  // TODO: which turret?
  TurretContext context;
  if (veh->GetPrimaryGunnerTurret(context))
  {
    for (int i=0; i<context._weapons->_magazines.Size(); i++)
    {
      Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine || !magazine->_type) continue;
      curAmmo += magazine->GetAmmo();
      maxAmmo += magazine->_type->_maxAmmo;
    }
  }

  if (curAmmo < AMMO_CRITICAL * maxAmmo) return RSCritical;
  if (curAmmo < AMMO_LOW * maxAmmo) return RSLow;
  return RSNormal;
}

#define INFANTRY_AMMO_NEAR      50.0f
#define INFANTRY_AMMO_FAR       100.0f
#define COMMAND_TIMEOUT         480.0f    // 8 min

void AIUnit::CheckAmmo()
{
  ResourceState state = GetAmmoState();
  if (state == RSNormal) return;

  const AITargetInfo *target = CheckAmmo(state);

  if (target)
  {
    bool channelCenter = false;
    EntityAI *veh = target->_idExact;
    if (veh)
    {
      AIBrain *unit = veh->CommanderUnit();
      if (unit && unit->LSIsAlive())
        channelCenter = unit->GetGroup() != GetGroup();
    }

    Command cmd;
    cmd._message = Command::Rearm;
    cmd._destination = target->_realPos;
    cmd._target = target->_idExact;
    cmd._time = Glob.time + COMMAND_TIMEOUT;
    GetGroup()->SendAutoCommandToUnit(cmd, this, true, channelCenter);
  }
}

const AITargetInfo *AIUnit::CheckAmmo(ResourceState state)
{
  float maxDist;
  if (state == RSCritical) maxDist = INFANTRY_AMMO_FAR;
  else maxDist = INFANTRY_AMMO_NEAR;
  float maxDist2 = Square(maxDist);

  // which magazines we needed
  CheckAmmoInfo ammo;
  base::CheckAmmo(ammo);
  if (ammo.slots1 == 0 && ammo.slots2 == 0 && ammo.slots3 == 0 && ammo.slotsHG == 0) return NULL; // no empty slots

  // check candidates
  float maxCoef = 0;
  const AITargetInfo *target = NULL;

  AICenter *center = GetGroup()->GetCenter();
  for (int i=0; i<center->NTargets(); i++)
  {
    const AITargetInfo &info = center->GetTarget(i);
    EntityAI *veh = info._idExact;
    if (!veh) continue;
    if (!info._type->IsKindOf(GWorld->Preloaded(VTypeMan)))
    {
      // rearm possible at bodies (even enemies)
      if (info._destroyed) continue;
      if (!(info._side == TCivilian) && !(center->IsFriendly(info._side))) continue;
    }
    float dist2 = Position(GetFutureVisualState()).DistanceXZ2(info._realPos);
    if (dist2 > maxDist2) continue;
//    if (center->GetExposurePessimistic(info._realPos)>0 ) continue;
    saturateMax(dist2, 1);
    float coef = 0;
    // check magazines
    int slots1Wanted = ammo.slots1, slots2Wanted = ammo.slots2, slots3Wanted = ammo.slots3, slotsHGWanted = ammo.slotsHG;
    for (int j=0; j<veh->NMagazinesToTake(); j++)
    {
      const Magazine *magazine = veh->GetMagazineToTake(j);
      if (!magazine) continue;
      if (magazine->GetAmmo() == 0) continue;
      const MagazineType *type = magazine->_type;
      int slots = GetItemSlotsCount(type->_magazineType);
      int hgslots = GetHandGunItemSlotsCount(type->_magazineType);
      if (slots > 0)
      {
        if (ammo.muzzle1 && ammo.muzzle1->CanUse(type))
        {
          if (slots <= slots1Wanted)
          {
            slots1Wanted -= slots;
            coef += 4.0 * slots;
          }
          goto NextMagazine;
        }
        if (ammo.muzzle2 && ammo.muzzle2->CanUse(type))
        {
          if (slots <= slots2Wanted)
          {
            slots2Wanted -= slots;
            coef += 2.0 * slots;
          }
          goto NextMagazine;
        }
        if (GetPerson()->IsMagazineUsable(type))
        {
          if (slots <= slots3Wanted)
          {
            slots3Wanted -= slots;
            coef += 1.0 * slots;
          }
        }
      }
      else if (hgslots > 0)
      {
        if (ammo.muzzleHG && ammo.muzzleHG->CanUse(type))
        {
          if (hgslots <= slotsHGWanted)
          {
            slotsHGWanted -= hgslots;
            coef += 2.0 * hgslots;
          }
        }
      }
NextMagazine:
      ;
    }
    coef *= InvSqrt(dist2);
    if (coef > maxCoef)
    {
      maxCoef = coef;
      target = &info;
    }
  }
  return target;
}

float AIUnit::GetFeeling() const
{
  int x = toIntFloor(Position(GetFutureVisualState()).X() * InvLandGrid);
  int z = toIntFloor(Position(GetFutureVisualState()).Z() * InvLandGrid);
  if( !GetGroup() || !GetGroup()->GetCenter() )
  {
    Fail("AI structure corrputed.");
    return 1;
  }
  float feeling = 0.0001 * GetGroup()->GetCenter()->GetExposureOptimistic(x, z);
  saturateMin(feeling, 1);
  return feeling;
}

AIBrain *AIUnit::GetFormationLeader()
{
  AISubgroup *subgrp = GetSubgroup();
  if (!subgrp) return this;
  return subgrp->Leader();
}

AIGroup* AIUnit::GetGroup() const
{
  if (!_subgroup) return NULL;
  return _subgroup->GetGroup();
}

const SideInfo *AIUnit::GetSideInfo() const
{
  AIGroup *grp = GetGroup();
  if (!grp) return NULL;
  return grp->GetCenter();
}

CombatModeRange AIUnit::GetCombatModeBasedOnCommand() const
{
  if (!GetSubgroup()) return CombatModeRange(CMSafe,CMStealth);
  return GetSubgroup()->GetCombatModeBasedOnCommand();
}

CautionRange AIUnit::GetCautionBasedOnCommand() const
{
  if (!GetSubgroup()) return CautionRange();
  return GetSubgroup()->GetCautionBasedOnCommand();
}

bool AIUnit::GetCoveringBasedOnCommand() const
{
  if (!GetSubgroup()) return true;
  return GetSubgroup()->GetCoveringBasedOnCommand();
}

bool AIUnit::CanUseCover() const
{
  if (!GetGroup()) return true;

  int slot = -1;
  int n = GetGroup()->NUnits();
  for (int i=0; i<n; i++)
  {
    if (GetGroup()->GetUnit(i)==this) {slot=i;break;}
  }
  if (slot<0)
    return true;
  
  // check formation position to see if covering is enabled for it
  return GetSubgroup()->GetCoveringEnabled(slot);
}

bool AIUnit::MustUseCover() const
{
  if (!GetSubgroup()) return false;
  // check current command
  Command *cmd = GetSubgroup()->GetCommand();
  if (!cmd) return false;
  switch (cmd->_discretion)
  {
    case Command::MinorHidden: return true;
    default: return false;
  }
}


void AIUnit::RemoveFromSubgroup()
{
  AISubgroup *subgroup = _subgroup;
  _subgroup = NULL;
  if (subgroup) subgroup->UnitRemoved(this);
}

/*!
\patch 1.43 Date 1/25/2002 by Ondra
- Fixed: MP: Player only partially removed from group after respawning to sea-gull.
This caused corrupted group list drawing and repeating "xxx is down" message
after "Report status" was issued.
*/

void AIUnit::RemoveFromGroup()
{
  Ref<AIUnit> unit = this;
  AIGroup *group = GetGroup();
  if (group)
  {
    if (!group->IsLocal()) return;
    #if _ENABLE_REPORT
    if (!IsLocal())
    {
      LogF
      (
        "Non-local unit %s removed from group",
        (const char *)GetDebugName()
      );
    }
    #endif
    group->UnitRemoved(this);
    _subgroup = NULL;
    _id = 0;
  }
}

void AIUnit::ForceRemoveFromGroup()
{
  Ref<AIUnit> unit = this;
  AIGroup *group = GetGroup();
  if (group)
  {
    group->UnitRemoved(this);
    _subgroup = NULL;
    _id = 0;
  }
}

void AIUnit::IssueGetOut()
{
  if (IsFreeSoldier())
    return;

  Assert(_inVehicle); // !IsFreeSoldier
  Person *driverMan = _inVehicle->Driver();
  AIBrain *driver = driverMan ? driverMan->Brain() : NULL;
  if( driver ) 
  {    
    _inVehicle->WaitForGetOut(this);
    if(!_inVehicle->IsLocal()) GetNetworkManager().AskWaitForGetOut(_inVehicle,this);
  }
  else
  {
    _inVehicle->GetOutStarted(this);
  }
}

/*!
\patch 1.43 Date 1/28/2002 by Ondra
- Fixed: Dead AI unit blocked getting out for other AI units
in the same vehicle.
\patch_internal 1.85 Date 9/11/2002 by Jirka
- Fixed: usage of function AIUnit::CheckIfAliveInTransport sometimes leads to crash, unit can be destroyed inside
*/

void AIUnit::CheckIfAliveInTransport()
{
  if (LSIsAlive()) return;
  Transport *veh = GetVehicleIn();
  if (!veh) return;
  AIGroup *grp = GetGroup();
  if (!grp) return;
  grp->SendIsDown(veh,this);
}

/*!
\patch 1.34 Date 12/6/2001 by Ondra
- Fixed: MP: Player messages "Injured", "Ammo low", "Fuel low" did not work
when player was not leader.
*/

void AIUnit::SendAnswer(Answer answer)
{
  if (!IsLocal()) return;
  AISubgroup *subgrp = GetSubgroup();

  if (!subgrp) return;
  AIGroup *grp = subgrp->GetGroup();
  if (!grp) return;

  // this message is served individually
  if (answer == AI::UnitDestroyed)
  {
    // subgroup does not know it yet (until someone will report it)
    // check: dead or deadinrespawn
    if (GetLifeState() != LifeStateDead)
    {
      SetLifeState(LifeStateDead);
      GetNetworkManager().UpdateObject(GetPerson());
      GetNetworkManager().UpdateObject(this);
      if (grp->Leader() == this && grp->IsLocal())
      {
        // if we are leader, units expect us to communicate
        grp->SetReportBeforeTime(this,Glob.time+30);
      }

      RString identity = GetPerson()->GetPersonName();
      if (identity.GetLength() > 0)
      {
        // check if unit is from player group
        Person *player = GWorld->GetRealPlayer();
        AIUnit *playerUnit = player ? (AIUnit *)player->Brain() : NULL; // we can convert it here - old AI structures is used
        AIGroup *playerGroup = playerUnit ? playerUnit->GetGroup() : NULL;
        void AddDeadIdentity(RString identity);
        if (playerGroup == grp)
        {
          AddDeadIdentity(identity);
//          RptF("** Identity %s is dead - prohibited", (const char *)identity);
        }
      }
    }

    //subgrp->ReceiveAnswer(this, answer);
    if (grp->IsLocal()) return; // else continue (see below)
  }

  if (!grp->IsLocal())
  {
    // note: this should be player that wants to transmit some information
    //  all AI units should be local where group is local
    DoAssert(IsAnyPlayer());
    if (IsAnyPlayer())
    {
      // we need to transmit the message to the remote group
      // do two things
      // 1) transmit it on the radio
      // 2) send answer accross network (done in Transmitted())
      grp->GetRadio().Transmit(new RadioMessageUnitAnswer(this, subgrp, answer));
    }
    return; // no simulation for remote groups
  }

#if LOG_COMM
  Log("Send answer: Unit %s: Answer %d",
  (const char *)GetDebugName(), answer);
#endif

  if (grp->UnitsCount()>1)
  {
    // transmit to radio only where there is somebody to listen
    grp->GetRadio().Transmit(new RadioMessageUnitAnswer(this, subgrp, answer));
  }
  else
  {
    subgrp->ReceiveAnswer(this,answer);
  }
}

void AIUnit::WatchFormation()
{
  // watch direction given by formation
  AISubgroup *subgrp = GetSubgroup();
  Vector3Val dir = subgrp->GetFormationDirection();
  float relAngle = _formationAngle;
  if (_inVehicle)
  {
    // keep source of formation position and direction the same
    AIBrain *commanderBrain = _inVehicle->CommanderUnit();
    AIUnit *commanderUnit = commanderBrain ? commanderBrain->GetUnit() : NULL;
    if (commanderUnit) relAngle = commanderUnit->GetFormationAngleRelative();
  }
  Matrix3 rot(MRotationY,-relAngle);
  _watchDirHead = _watchDir = rot * dir;
}

Vector3 AIUnit::GetFormationDirection() const
{
  // watch direction given by formation
  AISubgroup *subgrp = GetSubgroup();
  Vector3Val dir = subgrp->GetFormationDirection();
  float relAngle = _formationAngle;
  if (_inVehicle)
  {
    // keep source of formation position and direction the same
    AIBrain *commanderBrain = _inVehicle->CommanderUnit();
    AIUnit *commanderUnit = commanderBrain ? commanderBrain->GetUnit() : NULL;
    if (commanderUnit) relAngle = commanderUnit->GetFormationAngleRelative();
  }
  Matrix3 rot(MRotationY,-relAngle);
  return rot * dir;
}

void AIUnit::AutoAttack()
{
  // consider engaging target
  AIGroup *grp = GetGroup();
  if(
    !IsPlayer() &&
    IsUnit() && // ADDED: auto command can be issued only to vehicle commander
    _targetAssigned &&
    !grp->CommandSent(this, Command::Attack) &&
    !grp->CommandSent(this, Command::AttackAndFire) &&
    !grp->CommandSent(this, Command::GetOut) &&
    !grp->CommandSent(this, Command::GetIn) &&
    ( _targetAssigned==_targetEngage || !IsKeepingFormation() )
  )
  {
    EntityAIFull *veh = GetVehicle();
    FireResult result;
    Vector3 pos;
    if (veh->AttackThink(result,pos))
    {
      // TODO: which turret?
      TurretContext context;
      if (veh->GetPrimaryGunnerTurret(context))
      {
        // check if we are ready to fire
        // if not, we probably will have to attack
        float ttl = GetTimeToLive();
        FireResult fResult;
        veh->WhatFireResult(fResult,*_targetAssigned,context,ttl);
        // check if attack is giving a significantly better result than fire would
        if(result.CleanSurplus()>fResult.CleanSurplus()*1.2f+200)
        {
          // TODO: wait for a while
          // note: AttackThink has finished, which means some time has already passed
          // some good attack position found, issue auto attack command

          Command cmd;
          cmd._message = Command::Attack;
          cmd._targetE = _targetAssigned._tgt;
          cmd._destination = pos;
          cmd._direction = _targetAssigned->AimingPosition()-veh->FutureVisualState().Position();

          OLinkPermNOArray(AIUnit) list;
          
          // take any units which are in the same subgroup and are engaging the same target with us
          AISubgroup *subgrp = GetSubgroup();
          for (int i=0; i<subgrp->NUnits(); i++)
          {
            AIUnit *u = subgrp->GetUnit(i);
            // make sure we are inserted as well, do not rely upon engage target setting
            if (u->GetEngageTarget()==_targetAssigned || u==this)
            {
              list.Add(u);
            }
          }
          
          // automatic attack command
          grp->NotifyAutoCommand(cmd, list);
        }
      }
    }
  }
}

struct CompareFloatChE
{
  int operator () (const float *a, const float *b) const {return sign(*a-*b);}
};

void AIBrain::CheckEnemies()
{
  const TargetList *list = AccessTargetList();
  Vector3Val pos = Position(GetFutureVisualState());
  // find smallest range of two direction which will encompass all known dangerous enemies
  // this means: find a biggest angle where there is no dangerous enemy
  // or: find a largest angle between two dangerous enemies
  
  // TODO: if there are too many enemies, we might consider selecting only some of them
  // or, we could somehow discretize the headings so that we have an upper bound
  // we start by sorting enemy headings - we do not care about anything else
  AutoArray<float, MemAllocLocal<float,256> > headings;
  for (int i=0; i<list->EnemyCount(); i++)
  {
    TargetNormal *tgt = list->GetEnemy(i);
    if (!tgt->IsKnownBy(this)) continue;
    if (tgt->IsVanishedOrDestroyed()) continue;
    if (tgt->GetSubjectiveCost()<=0) continue;
 //   if (tgt->idExact &&  tgt->idExact->IsArtilleryTarget()) continue;
    Vector3 offset = tgt->AimingPosition()-pos;
    // if we are out of range of his  weapons, we do not care about him
    // note: following will be inaccurate for units which changed their weapon layouts
    float maxRange = tgt->type->GetMaxWeaponRange();
    if (offset.SquareSize()>Square(maxRange)) continue;
    float heading = atan2(offset.X(),offset.Z());
    headings.Add(heading);
  }
  
  QSort(headings,CompareFloatChE());
  
  // now traverse the whole list, and compute angular differences
  if (headings.Size()<=0)
  {
    // set to none
    _enemyDirL = _enemyDirR = VZero;
  }
  else
  {
    // when cycling across the list boundary, the difference is one full circle
    float prev = headings.Last()-2*H_PI;
    float maxDiff = -FLT_MAX;
    float headingL = 0;
    float headingR = 0;
    for (int i=0; i<headings.Size(); i++)
    {
      float curr = headings[i];
      // calculate left-right angular difference. Result in range <0,2*ip>
      float diff = curr-prev;
      if (maxDiff<diff)
      {
        maxDiff = diff;
        // prev..next is left..right of the no-enemy zone, which means right..left of the enemy zone
        headingR = prev;
        headingL = curr;
      }
      prev = curr;
    }
    _enemyDirR = Matrix3(MRotationY,-headingR).Direction();
    _enemyDirL = Matrix3(MRotationY,-headingL).Direction();
  }
}

float AIBrain::CheckDanger(Vector3Par pos) const
{
  // note: what sense does it have to pass pos, when we test visibility against our current position anyway?
  EntityAIFull *veh = GetVehicle();

  const TargetList *list = AccessTargetList();
  // danger is returned as damage per minute
  // we compute danger only from nearby enemies

  float sumThreat = 0;
  for (int i=0; i<list->EnemyCount(); i++)
  {
    TargetNormal *tgt = list->GetEnemy(i);
    if (!tgt->IsKnownBy(this)) continue;
    if (tgt->IsVanishedOrDestroyed()) continue;
    if (tgt->idExact &&  tgt->idExact->IsArtilleryTarget()) continue;
    if (!tgt->idExact) continue;
    Vector3 aimPos = tgt->AimingPosition();
    float dist2 = aimPos.Distance2(pos);
    float maxRange = tgt->type->GetMaxWeaponRange();
    if (dist2>Square(maxRange)) continue;
    // check how much he can see us
    AIBrain *enemyUnit = tgt->idExact->CommanderUnit();
    if (!enemyUnit) continue;
    float visibility = GWorld->Visibility(enemyUnit,veh);
    // note: following will be inaccurate for units which changed their weapon layouts
    Threat threat = tgt->type->GetDamagePerMinute(dist2,visibility);
    sumThreat += threat[veh->GetType()->GetKind()];
  }
  return sumThreat;
}

void AIUnit::CheckResources()
{
  ResourceState state = GetHealthState();
  if (state == RSCritical)
  {
    if (_lastHealthState < RSCritical)
    {
      _healthCriticalTime = Glob.time + FIRST_REPORT_TIME;
    }
    else if (Glob.time > _healthCriticalTime)
    {
      _healthCriticalTime = Glob.time + REPEAT_REPORT_TIME;
      SendAnswer(HealthCritical);
    }
  }
  _lastHealthState = state;

  //leader state autoUpadate
  if ( _lastHealthState == RSNormal && GetGroup()->GetHealthStateReported(this) >= RSLow &&
    IsPlayer() && GetGroup() && GetGroup()->Leader() == this)
    GetGroup()->SetHealthStateReported(this,RSNormal);

  if (IsUnit())
  {
    state = GetFuelState();
    if (state == RSCritical)
    {
      if (_lastFuelState < RSCritical)
      {
        _fuelCriticalTime = Glob.time + FIRST_REPORT_TIME;
      }
      else if (Glob.time > _fuelCriticalTime)
      {
        _fuelCriticalTime = Glob.time + REPEAT_REPORT_TIME;
        SendAnswer(FuelCritical);
      }
    }
    else if (state == RSLow)
    {
      if (_lastFuelState < RSLow)
        SendAnswer(FuelLow);
    }
    _lastFuelState = state;

    state = GetArmorState();
    if (state == RSCritical)
    {
      if (_lastArmorState < RSCritical || Glob.time > _dammageCriticalTime)
      {
        _dammageCriticalTime = Glob.time + REPEAT_REPORT_TIME;
        SendAnswer(DamageCritical);
      }
    }
    _lastArmorState = state;

    state = GetAmmoState();
    if (state == RSCritical)
    {
      if (_lastAmmoState < RSCritical || Glob.time > _ammoCriticalTime)
      {
        _ammoCriticalTime = Glob.time + REPEAT_REPORT_TIME;
        SendAnswer(AmmoCritical);
      }
    }
    else if (state == RSLow)
    {
      if (_lastAmmoState < RSLow)
        SendAnswer(AmmoLow);
    }
    _lastAmmoState = state;

    //leader state autoUpadate
    if(IsPlayer() && GetGroup() && GetGroup()->Leader() == this)
    {
      if (_lastArmorState == RSNormal &&  GetGroup()->GetDamageStateReported(this) >= RSLow)
        GetGroup()->SetDamageStateReported(this,RSNormal);

      if (_lastAmmoState == RSNormal &&  GetGroup()->GetAmmoStateReported(this) >= RSLow)
        GetGroup()->SetAmmoStateReported(this,RSNormal);

      if (_lastFuelState == RSNormal  && GetGroup()->GetFuelStateReported(this) >= RSLow)
        GetGroup()->SetFuelStateReported(this,RSNormal);
  }

  }
}

void AIUnit::CheckGetOut()
{
  Transport *veh = _inVehicle;
  if
  (
    veh && (!veh->Driver() || !veh->Driver()->Brain())
    && veh->GetGetOutTime() <= Glob.time
  )
  {
    veh->WhoIsGettingOut().RemoveNulls();
    int n = veh->WhoIsGettingOut().Size();
    for (int i=0; i<n; i++)
    {
      if (veh->WhoIsGettingOut()[i].GetLink() == this && veh->PrepareGetOut(this))
      {
        veh->SetGetOutTime(Glob.time + 2.0);
        veh->StartGetOutActivity(this,false);

        break;//FIX 18.8.2003 Fehy.
      }
    }
  }
}

static void GetInTimedOut(Transport *veh)
{
  veh->SetGetInTimeout(Time(0));

  if (GWorld->GetMode()==GModeNetware)  
  {
    veh->WhoIsGettingIn().Clear();
  }
  else
  {
    OLinkPermNOArray(AIUnit) &units = veh->WhoIsGettingIn();
    OLinkPermNOArray(AIUnit) toRemove;
    for (int i=0; i<units.Size(); i++)
    {
      AIUnit *unit = units[i];
      if (!unit) continue;
      if (unit->IsAnyPlayer()) continue;
      if (!unit->IsLocal()) continue;
      AIGroup *grp = unit->GetGroup();
      if (!grp) continue;
      if (grp->IsAnyPlayerGroup()) continue;
      if (GWorld->PlayerOn() && GWorld->PlayerOn()->GetGroup() == grp) continue;
      if (!grp->IsLocal()) continue;

      // remove the unit from the group
      toRemove.Add(unit);
      // unassign from the vehicle and stay out 
      unit->UnassignVehicle();
      unit->OrderGetIn(false);
      unit->AllowGetIn(false);
    }

    if (toRemove.Size() > 0)
    {
      void ProcessJoinGroups(OLinkPermNOArray(AIUnit) &units, AIGroup *group, bool silent, int forceId);
      ProcessJoinGroups(toRemove, NULL, false, -1);
    }

    units.Clear();
  }
}

bool AIUnit::IsCheckGetInGetOutNeeded() const
{
  Transport *veh = _inVehicle;
  if (veh)
  {
    if (veh->GetGetInTimeout() <= Glob.time || veh->WhoIsGettingIn().Count() <= 0)
    {
      GetInTimedOut(veh);
    }
    if (veh->WhoIsGettingOut().Count() > 0 && Glob.time >= veh->GetGetOutTime())
    {
      return true;
    }
  }

  return false;
}

/*!
\patch 5150 Date 3/27/2007 by Jirka
- Fixed: AI - car drivers sometimes ignored commands from player in cargo
*/

void AIUnit::CheckGetInGetOut()
{
  Transport *veh = _inVehicle;
  if (veh)
  {
#if 0
    if (veh->GetGetInTimeout() <= Glob.time)
    {
LogF
(
"%.2f: Vehicle %s, get in timed out (%.2f)",
Glob.time.toFloat(),
(const char *)veh->GetDebugName(),
veh->GetGetInTimeout().toFloat()
);
    }
#endif
#if 0
    if (veh->WhoIsGettingIn().Count() <= 0)
    {
LogF
(
"%.2f: Vehicle %s, all units got in",
Glob.time.toFloat(),
(const char *)veh->GetDebugName()
);
    }
#endif
    if (veh->GetGetInTimeout() <= Glob.time || veh->WhoIsGettingIn().Count() <= 0)
    {
#if 0
LogF
(
"%.2f: Vehicle %s, wait for get out ended",
Glob.time.toFloat(),
(const char *)veh->GetDebugName()
);
#endif
      GetInTimedOut(veh);
    }
    veh->WhoIsGettingOut().RemoveNulls();
    int n = veh->WhoIsGettingOut().Size();
    if (n > 0 && Glob.time >= veh->GetGetOutTime())
    {
      AIUnit *unit = NULL;
      for (int i=0; i<n; i++)
      {
        AIUnit *u = veh->WhoIsGettingOut()[i];
        if (u->IsInCargo())
        {
          unit = u;
          break;
        }
      }
      if (!unit) for (int i=0; i<n; i++)
      {
        AIUnit *u = veh->WhoIsGettingOut()[i];
        if (u->IsGunner())
        {
          unit = u;
          break;
        }
      }
      if (!unit) for (int i=0; i<n; i++)
      {
        AIUnit *u = veh->WhoIsGettingOut()[i];
        if (u->IsCommander())
        {
          unit = u;
          break;
        }
      }
      if (!unit)
        unit = veh->WhoIsGettingOut()[0];
      Assert(unit);
      if (unit && veh->PrepareGetOut(unit))
      {
        veh->SetGetOutTime( Glob.time + 2.0 );
        veh->StartGetOutActivity(unit,false);
      }
    }
    // check if everybody is in/out
    if( veh->CanCancelStop() )
    {
      AIBrain *unit = veh->CommanderUnit();
      if (!unit || unit->IsInCargo()) unit = veh->PilotUnit();
      // if we are stopped, un-stop
      if (unit && unit->GetState() == Stopped)
      {
        unit->SetState(Wait); // valid pass Stopped -> Wait
      }
    }
  }
  else
  {
    if (HasAI())
    {
      // this is probably intended for medic and other supply units
      // however it seems to be currently unused - commands are used instead for them
      if (GetPerson()->CanCancelStop())
      {
        if (GetState()==Stopped)
        {
          Verify(SetState(Wait));
        }
        // ?? Fail("Unit stopped");
      }
    }
  }
}

TargetList *AIUnit::AccessTargetList()
{
  AIGroup *group = GetGroup();
  if (!group) return NULL;
  return &group->GetTargetList();
}

const TargetList *AIUnit::AccessTargetList() const
{
  AIGroup *group = GetGroup();
  if (!group) return NULL;
  return &group->GetTargetList();
}

bool AIUnit::ForEachTarget(const TargetFunctor &func) const
{
  AIGroup *group = GetGroup();
  if (!group) return false;
  TargetList &list = group->GetTargetList();
  // TODO: implement ForEach in TargetList
  for (int i=0; i<list.AnyCount(); i++)
  {
    if (func(list.GetAny(i))) return true;
  }
#if _ENABLE_CONVERSATION
  const RefArray<TargetKnowledge> &listKB = group->GetTargetKB();
  for (int i=0; i<listKB.Size(); i++)
  {
    if (func(listKB[i])) return true;
  }
#endif
  return false;
}


int AIBrain::NBehaviourDiags() const
{
  return NBehaviourFSMRole;
}
RString AIBrain::BehaviourDiags(int i) const
{
  if (_behaviourFSMs[i])
  {
    RString fsmName = _behaviourFSMs[i]->GetDebugName();
    return fsmName + " - " + _behaviourFSMs[i]->GetStateName();
  }
  return RString();
}

#define DRAW_OBJ(obj) GScene->DrawObject(obj)

#if _ENABLE_CHEATS

void AIUnit::DrawDiags()
{
  if (CHECK_DIAG(DEPath) || CHECK_DIAG(DEFSM))
  {
    LODShapeWithShadow *forceArrow=GScene->ForceArrow();

    // draw both positions only when they are not identical
    RString text = " Formation";
    if (_wantedPositionWanted.Distance2(_wantedPosition)<Square(0.1f))
    {
      text = " Form+plan";
    }
    else
    { // position decided by FSM
      Ref<Object> obj=new ObjectColored(forceArrow,VISITOR_NO_ID);

      float size=0.3f;
      obj->SetPosition(_wantedPosition);
      obj->SetOrient(VUp,VForward);
      obj->SetPosition(obj->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
      obj->SetScale(size);
      obj->SetConstantColor(Color(0,0.3f,0));
      DRAW_OBJ(obj);
      GScene->DrawDiagText(GetDebugName()+" Planning",_wantedPosition+VUp*0.5f,5);
    }
    // position wanted by formation
    {
      Ref<Object> obj=new ObjectColored(forceArrow,VISITOR_NO_ID);

      float size=0.3f;
      obj->SetPosition(_wantedPositionWanted);
      obj->SetOrient(VUp,VForward);
      obj->SetPosition(obj->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
      obj->SetScale(size);
      obj->SetConstantColor(Color(0.3f,0.3f,0));
      DRAW_OBJ(obj);
      GScene->DrawDiagText(GetDebugName()+text,_wantedPositionWanted+VUp,4);
    }
    
  }
}

#endif

/*!
\patch 1.01 Date 6/26/2001 by Ondra.
- Improved: AI takes NV goggles on/off.
\patch 1.50 Date 4/12/2002 by Ondra
- Fixed: Multiple AI units asked "Where are you" same time.
\patch 5137 Date 3/7/2007 by Ondra
- Fixed: AI sometimes did not take off NVG after skipTime was used.
*/
bool AIUnit::Think(ThinkImportance prec)
{
  if (!IsLocal())
  {
    // remote driven vehicle is stopped - AI can get out
    Transport *veh = GetVehicleIn();
    // if (veh && veh->CommanderUnit() && veh->CommanderUnit()->IsLocal())
    if (veh && veh->IsLocal())
    {
      // the code is is strange - if commander unit is local, his Think will be called
      // executing the CheckGetInGetOut when needed
      if(veh->FutureVisualState().Speed().SquareSize() < Square(0.5f) && IsCheckGetInGetOutNeeded())
      {
        Vector3Val pos = veh->FutureVisualState().Position();
#if _ENABLE_WALK_ON_GEOMETRY
        float posY = GLandscape->RoadSurfaceYAboveWater(pos+VUp*0.5f, Landscape::FilterIgnoreOne(veh));
#else
        float posY = GLandscape->RoadSurfaceYAboveWater(pos+VUp*0.5f);
#endif
        if (pos.Y() - posY < 4.0f)
        {
          CheckGetInGetOut();
        }
      }
    }
    return false;
  }

  if (!LSIsAlive()) return false;
  PROFILE_SCOPE_EX(aiUnt, ai);
#if LOG_THINK
  Log("    Unit %s think.", (const char *)GetDebugName());
#endif

  EntityAIFull *veh = GetVehicle();
  // unassign target if not valid

  if (!_subgroup) return false;

  // TODO: move to separate function
  // find attack position if necessary

  {
    PROFILE_SCOPE_EX(aiUAA, ai);
    AutoAttack();
    SetWatch();
  }
  // some parts of AIUnit::Think may be executed less often

  if (Glob.time > _expensiveThinkTime)
  {
    PROFILE_SCOPE_EX(aiUET, ai);
    ExpensiveThinkDone();
    if (!IsAnyPlayer() || Glob.config.IsEnabled(DTAutoSpot)) CheckResources();
    CheckEnemies();
    if (!IsSubgroupLeader() && IsUnit())
    {
      bool away = veh->IsAway();
      if (away)
      {
        if (!_isAway)
        {
          _awayTime = Glob.time + FIRST_AWAY_TIME;
        }
        else if (Glob.time > _awayTime)
        {
          if (!_subgroup->IsPlayerSubgroup() && veh->IsAway(1.5))
          {
            _awayTime = Glob.time + REPEAT_AWAY_TIME;
            AIGroup *grp = GetGroup();
            grp->GetRadio().Transmit(new RadioMessageReturnToFormation(_subgroup, this));
          }
          else if (!IsPlayer())
          {
            AIGroup *grp = GetGroup();
            grp->GetRadio().Transmit(new RadioMessageWhereAreYou(this, grp));
            // all units of the group should be marked not to ask this question
            // to avoid asking it multiple times
            for (int i=0; i<grp->NUnits(); i++)
            {
              AIUnit *u = grp->GetUnit(i);
              if (!u) continue;
              u->SetAwayTime
              (
                Glob.time + REPEAT_AWAY_TIME + GRandGen.RandomValue()*20
              );
            }
          }
        }
      }
      _isAway = away;
    }

    if( _targetAssigned )
    {
      AIGroup *grp = GetGroup();
      if (
        _targetAssigned->IsVanishedOrDestroyed() ||
        _targetAssigned->State(this)<_targetAssigned._initState && GetEnableFireTarget()!=_targetAssigned && grp && !grp->IsPlayerGroup()
      )
      {
        AssignTarget(TargetToFire());
      }
      else
      {
        TurretContext context;
        if (!veh->GetPrimaryGunnerTurret(context) || !veh->IsAbleToFire(context))
        {
          AssignTarget(TargetToFire());
        }
      }
    }

    // FIX
    // consider if we should use nvg
    if (!IsPlayer())
    {
      Person *person = GetPerson();
      if (person->IsNVEnabled())
      {
        const LightSun *sun = GScene->MainLight();
        float night=sun->NightEffect();
        if (night>0 && !veh->LightsNeeded())
        {
          // check light intensity
          float dark = 1.02f-(sun->GetAmbient().Brightness()+sun->GetDiffuse().Brightness())*3;
          saturateMin(night,dark);

          // randomize based on unit ID. apply unit skill
          float thold = 0.5f + ID()*0.02f - GetAbility(AKGeneral)*0.2f;
          saturate(thold,0.3f,0.7f);

          person->SetNVWanted(night>thold);
        }
        else
        {
          person->SetNVWanted(false);
        }
      }
      #if 0 //_ENABLE_REPORT
      int reloaded = veh->AutoReloadAll();
      if (reloaded>=0)
      {
        RptF
        (
          "Unit %s autoreloaded weapon %d",
          (const char *)GetDebugName(),reloaded
        );
      }
      #endif
    }

#if _ENABLE_CONVERSATION
    if (IsLocal() && !IsPlayer())
    {
      if (_conversingWith)
      {
        EntityAIFull *with = _conversingWith->GetVehicle();
        EntityAIFull *me = GetVehicle();
        // if we were conversing with somebody, but he is now too far away, stop conversing with him
        if (!with || !me || with->FutureVisualState().Position().Distance2(me->FutureVisualState().Position())>Square(10))
        {
          // send message the one we were conversing with has withdrawn
          Ref<KBMessageInfo> message = _conversingWith->CreateKBMessage("core","withdrawn");
          if (message)
            KBReact(_conversingWith, message);

          _conversingWith = NULL;
        }
      }
      // TODO: even if we are already in conversation, when somebody important has approached, we should interrupt
      // TODO: enable approaching even for units inside vehicles
      if (!_conversingWith && IsFreeSoldier())
      {
        // check if somebody has approached us
        // by approaching we mean somebody is standing in front of us
        AIGroup *grp = GetGroup();
        EntityAIFull *me = GetVehicle();
        if (grp && me && me->FutureVisualState().Speed().SquareSize()<Square(0.2f))
        {
          AIBrain *approachedBy = NULL;
          const TargetList &list = grp->GetTargetList();
          for (int i=0; i<list.FriendlyCount(); i++)
          {
            const Target *tgt = list.GetFriendly(i);
            if (tgt->IsVanishedOrDestroyed()) continue;
            if (!tgt->idExact) continue;
            if (tgt->idExact->IsArtilleryTarget()) continue;
            // never approached by myself
            if (tgt->idExact==me) continue;
            if (!tgt->IsKnownBy(this)) continue;
            AIBrain *tgtUnit = tgt->idExact->CommanderUnit();
            // TODO: consider approached by a vehicle as well
            if (!tgtUnit || !tgtUnit->IsFreeSoldier()) continue;
            // only formation leader can "approach" - other units are ignored
            if (tgtUnit->GetFormationLeader()!=tgtUnit) continue;
            // check if he is close and in the direction we are looking to
            // do not assume approach until he is stopped
            // LandAimingPosition and GetSpeed should provide continuously accurate data for units we see
            if (tgt->GetSpeed(this).SquareSize()>Square(0.2f)) continue;
            Vector3 tgtPos = tgt->LandAimingPosition(this);
            if (tgtPos.Distance2(me->FutureVisualState().Position())<Square(5))
            {
              // check if he is in front of us
              // we will not greet behind our back
              Vector3 relPos = me->PositionWorldToModel(tgtPos);
              if (fabs(relPos.X())>relPos.Z()) continue;
              // if the unit is not facing us, we will not greet
              float facing = me->FutureVisualState().Direction().CosAngle(tgt->idExact->FutureVisualState().Direction());
              if (facing>0) continue;
              approachedBy = tgtUnit;
              break;
            }
          }
          // now we know who approached us, pass us a message
          if (approachedBy)
          {
            Ref<KBMessageInfo> message = approachedBy->CreateKBMessage("core","approached");
            if (message)
              KBReact(approachedBy, message);
              // note: KBReact sets _conversingWith as well
          }
        }
      }
    }
#endif    


  } // if (expensive think)

  // show-term AI reaction
  bool fsmProcessed = false;
  if (HasAI())
  {
    if (_reactionFSM && !IsAIDisabled(DAFSM))
    {
      PROFILE_SCOPE_EX(aiFSM,ai);
      if (PROFILE_SCOPE_NAME(aiFSM).IsActive()) PROFILE_SCOPE_NAME(aiFSM).AddMoreInfo(_reactionFSM->GetDebugState());
      bool done = _reactionFSM->Update(GetPerson());
#if 0
      LogF("%s %.2f: State %s (danger)", cc_cast(GetDebugName()), Glob.time.toFloat(), cc_cast(_reactionFSM->GetStateName()));
      // LogF(" - assigned target %s, watch mode %s", GetTargetAssigned() ? cc_cast(GetTargetAssigned()->GetDebugName()) : "NULL", cc_cast(FindEnumName(_watchMode)));
#endif
      if (done)
      {
        // finished
        _reactionFSM = NULL;
      }
      else
      {
        fsmProcessed = _reactionFSM->IsExclusive(GetPerson());
      }
    }
#if _ENABLE_CONVERSATION
    else
    {
      if (_talkEvents.Size()>0 && !_conversationFSM)
      {
        ProcessOneTalkEvent();
        _talkEvents.Delete(0);
      }


      if (_conversationFSM)
      {
        PROFILE_SCOPE_EX(aiFSM,ai);
        if (PROFILE_SCOPE_NAME(aiFSM).IsActive()) PROFILE_SCOPE_NAME(aiFSM).AddMoreInfo(_conversationFSM->GetDebugState());
        bool done = _conversationFSM->Update(GetPerson());
  #if 0
        LogF("%s %.2f: State %s (conversation)", cc_cast(GetDebugName()), Glob.time.toFloat(), cc_cast(_conversationFSM->GetStateName()));
  #endif
        if (done)
        {
          // finished
          _conversationFSM = NULL;
          // can talk to other person now
          ChangeListening(-1);
        }
        else
        {
          fsmProcessed = _conversationFSM->IsExclusive(GetPerson());
        }
      }
    }
#endif
  }

  if (veh->PilotUnit() != this)
  {
    // Warning: unit can be destroyed inside, return immediately
    CheckGetOut();
    // create path only for pilot units
    return false;
  }

  // free soldier or driver
  if (!HasAI())
  {
    // player driven vehicle is stopped - AI can get out
    if (veh->FutureVisualState().Speed().SquareSize() < Square(0.5) && IsCheckGetInGetOutNeeded())
    {
      Vector3Val pos = veh->FutureVisualState().Position();
#if _ENABLE_WALK_ON_GEOMETRY
      float posY = GLandscape->RoadSurfaceYAboveWater(pos + VUp * 0.5f, Landscape::FilterIgnoreOne(veh));
#else
      float posY = GLandscape->RoadSurfaceYAboveWater(pos + VUp * 0.5f);
#endif
      if (pos.Y() - posY < 4.0f)
      {
        CheckGetInGetOut();
      }
    }
    return false;
  }

#if _ENABLE_AI

  if (!fsmProcessed && _behaviourFSMs[BFSMFormation] && !IsAIDisabled(DAFSM))
  {
    PROFILE_SCOPE_EX(aiFSM,ai);
    // Find real destination based on the expected one
    if (PROFILE_SCOPE_NAME(aiFSM).IsActive()) PROFILE_SCOPE_NAME(aiFSM).AddMoreInfo(_behaviourFSMs[BFSMFormation]->GetDebugState());
    _behaviourFSMs[BFSMFormation]->Update(GetPerson());
#if 0
    LogF("%s %.2f: State %s (formation)", cc_cast(GetDebugName()), Glob.time.toFloat(), cc_cast(_behaviourFSMs[BFSMFormation]->GetStateName()));
    // LogF(" - assigned target %s, watch mode %s", GetTargetAssigned() ? cc_cast(GetTargetAssigned()->GetDebugName()) : "NULL", cc_cast(FindEnumName(_watchMode)));
#endif
  }

  if (_state == Stopped)
  {
    // this should currently never be called for a soldier, only for vehicle
    DoAssert(GetVehicleIn());
    // waiting for get in / get out
    CheckGetInGetOut();
    return false;
  }

// LogF("* %.2f: AIUnit(%s)::Think", Glob.time.toFloat(), cc_cast(GetDebugName()));

  PROFILE_SCOPE_EX(aiPth,ai)
  switch (_planningMode)
  {
  case DoNotPlan: case DoNotPlanFormation:
    // do not create new plan (if exist, can continue with the old one)
    if (_noPath || !IsSubgroupLeader()) return false;
    else
    {
      // operative planning target
      if (_state == Wait || _state == Replan)
      {
        if (!OperPath(prec)) return false;
      }

      // operative planning
      return CreateOperativePath();
    }
  case LeaderDirect:
    if (_state!=Busy)
    {
      DoAssert(IsSubgroupLeader());
      DoAssert(IsUnit());

      SetMode(AIUnit::DirectExact);
      Verify(SetState(AIUnit::Init));
      _iter = 0;
      _lastPlan = true;

      // operative planning
      return CreateOperativePath();
    }
    return false;
  case LeaderPlanned:
  case FormationPlanned:
  case VehiclePlanned:
    {
      if (!_planParametersSet || _path.Size()>2 && GetVehicle() && !GetVehicle()->PlanChangePossible())
      {
        // no plan change currently possible - do not plan until the cover is reached
        return false;
      }
      
      // replan if target position changed too much
      bool replan = CheckReplanNeeded(0.3,_planningMode,false);
      if (replan)
      {
        _plannedPosition = _wantedPosition;
        // _noPath would cause forgetting the path meanwhile
        //_noPath = true;
        RefreshStrategicPlan();
      }

      // strategic planning
      CreateStrategicPath(prec);

      // operative planning target
      if (_state == Wait || _state == Replan)
      {
        if (!OperPath(prec)) return false;
      }

      // operative planning
      return CreateOperativePath();
    }
  default:
    Fail("Planning mode");
    return false;
  }

#else
  return false;
#endif //_ENABLE_AI
}

void AIUnit::ProcessOneTalkEvent()
{
#if _ENABLE_CONVERSATION
  Assert(_talkEvents.Size()>0);
  TalkEvent &ev = _talkEvents[0];
  const KBCenter *kbCenter = GetKBCenter();
  if (!kbCenter) return;

  // parse the question
  const KBMessageInfo *question = ev._event;
  RString topic = question->GetTopic();
  if (topic.GetLength()==0) return;
  const KBMessage *message = question->GetMessage();
  if (!message) return;
  const KBMessageTemplate *messageType = message->GetType();
  if (!messageType) return;
  
  const KBCenter *kbParent = GetKBParent();
  RString handler = kbCenter->GetHandlerAI(question, kbParent);
  if (handler.GetLength()==0) return;

  // get a FSM name
  RString FindScript(RString name);
  RString name = FindScript(handler);
  if (name.GetLength() == 0) return;

  // all parameters ready, now create the FSM  - similar to CreateFSM
  FSMScriptedType *fsmType = FSMScriptedTypes.New(name);
  if (!fsmType) return;
  AIBrainFSMScripted *fsm = new AIBrainFSMScripted(fsmType, GWorld->GetMissionNamespace(), NULL); // mission namespace

  Person *from = ev._from ? ev._from->GetPerson() : NULL;
  fsm->SetParam("_from", GameValueExt(from));
  fsm->SetParam("_topic", GameValue(topic));
  fsm->SetParam("_sentenceId", GameValue(messageType->GetName()));
  for (int i=0; i<message->NArguments(); i++)
  {
    const KBArgument &argument = message->GetArgument(i);
    fsm->SetParam(RString("_") + argument._name, argument._value);
  }
  Assert(!_conversationFSM);
  _conversationFSM = fsm;
  // avoid talking to other person when reacting
  ChangeListening(+1);
#if DEBUG_FSM
  if (_fsmAutoDebug>0)
  {
    // log parameter values as well
    LogF(" _from %s",cc_cast(from->GetDebugName()));
    LogF(" _topic %s",cc_cast(topic));
    LogF(" _sentenceId %s",cc_cast(messageType->GetName()));
    for (int i=0; i<message->NArguments(); i++)
    {
      const KBArgument &argument = message->GetArgument(i);
      LogF("  %s: %s",cc_cast(argument._name),cc_cast(argument._value.GetDebugText()));
    }
    if (_fsmAutoDebug>=2)
    {
      fsm->Debug();
    }
  }
#endif
#endif
}

bool AIUnit::IsEngageEnabled(Target *tgt) const
{
  // check if fire enabled at given target
  if (!IsKeepingFormation()) return true;
  if (tgt && tgt==GetEngageTarget()) return true;
  return false;
}


void AIUnit::AddFormationPos(FormationPos status)
{
  if (status==PosInFormation)
  {
    _formOffset = VZero;
    return;
  }

  AISubgroup *subgrp = GetSubgroup();
  if (!subgrp) return;
  
  // consider formation position (advance etc...)
  // calculate formation size
  float sizeX=0;
  float sizeZ=0;
  int nUnits = 0;
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *u = subgrp->GetUnit(i);
    if (!u || !u->IsUnit()) continue;
    EntityAI *veh = u->GetVehicle();
    sizeX += veh->GetFormationX();
    sizeZ += veh->GetFormationZ();
    nUnits++;
  }
  // for smaller group compute formation size as if there are 8 units
  if (nUnits<8)
  {
    float nCoef = 8.0/nUnits;
    sizeX *= nCoef;
    sizeZ *= nCoef;
  }
  
  switch (status)
  {
    case AI::PosAdvance: _formOffset[2] += sizeZ; break;
    case AI::PosStayBack: _formOffset[2] -= sizeZ; break;
    case AI::PosFlankLeft: _formOffset[0] -= sizeX; break;
    case AI::PosFlankRight: _formOffset[0] += sizeX; break;
  }
}

/*!
\patch 2.01 Date 1/10/2003 by Ondra
- Fixed: Kills where reported again by AI units when 'Report status' was issued in Stealth mode.
*/

void AIUnit::ReportStatus(bool position)
{
  if (!_subgroup)
    return;

  ResourceState state;

  state = GetFuelState();
  if (state == RSCritical)
  {
    _fuelCriticalTime = Glob.time + REPEAT_REPORT_TIME;
    SendAnswer(FuelCritical);
  }
  else if (state == RSLow)
  {
    SendAnswer(FuelLow);
  }
  _lastFuelState = state;

  state = GetHealthState();
  if (state == RSCritical)
  {
    _healthCriticalTime = Glob.time + REPEAT_REPORT_TIME;
    SendAnswer(HealthCritical);
  }
  _lastHealthState = state;

  state = GetArmorState();
  if (state == RSCritical)
  {
    _dammageCriticalTime = Glob.time + REPEAT_REPORT_TIME;
    SendAnswer(DamageCritical);
  }
  _lastArmorState = state;

  state = GetAmmoState();
  if (state == RSCritical)
  {
    _ammoCriticalTime = Glob.time + REPEAT_REPORT_TIME;
    SendAnswer(AmmoCritical);
  }
  else if (state == RSLow)
  {
    SendAnswer(AmmoLow);
  }
  _lastAmmoState = state;

  if (GetCombatMode()==CMStealth)
  {
    // stealth: mark all targets watch by this unit as not reported
    const TargetList *list = AccessTargetList();
    if (list)
    {
      // TODO: use Unknown as well
      for (int i=0; i<list->EnemyCount(); i++)
      {
        TargetNormal *tgt = list->GetEnemy(i);
        if (tgt->idSensor==this && !tgt->destroyed)
        {
          // set as never reported - force reporting targets
          tgt->timeReported = TIME_MIN;
          tgt->posReported = VZero;
        }
      }
    }
  }

  if (position)
    SendAnswer(AI::ReportPosition);
}

bool AIUnit::IsPlayerDriven() const
{
  return
    _person == GLOB_WORLD->PlayerOn() ||
    _inVehicle &&
    _inVehicle->CommanderUnit() == this &&
    _inVehicle->Driver() == GLOB_WORLD->PlayerOn();
}

float AIUnit::GetRandomizedExperience() const
{
  float exp = GetPerson()->GetExperience();
  const float coef = 0.2;
  // result is from <(1 - coef) * exp ; (1 + coef) * exp>
  return exp * ((1.0 - coef) + (2.0 * coef) * GRandGen.RandomValue());
}

/**
Used for randomization and for hash tables.
*/
int AIUnit::RandomSeed() const
{
  int base = 0;
  AIGroup *grp = GetGroup();
  if (grp)
  {
    // include group and side if available
    base = grp->ID()*13;
    AICenter *center = grp->GetCenter();
    if (center)
    {
      base += center->GetSide()*3;
    }
  }
  return base + _id * 211;
}

float AIUnit::GetMorale() const
{
  Person *person = GetPerson();
  if (!person) return 0;
  // morale is normal, except when:
  float morale = 0;
  // injury means worse morale
  morale -= person->GetTotalDamage();
  // bad time to live expectations lower morale as well?
  morale += InterpolativC(GetTimeToLive(),0,120,-0.5,0.2);
  // being out of combat generally improves morale
  if (GetCombatMode()<=CMSafe) morale += 1.0;
  else if (GetCombatMode()<=CMAware) morale += 0.6;
  // courage reduces impact of the combat
  else morale += GetAbility(AKCourage)*0.3-0.1;

  // consider: seeing dead friendlies could make morale worst, seeing dead enemies could improve it
  return morale;
}

/*!
Function should return true during ratio*period time.
Average one decision change should be made per period.
Function requires no storage.
*/

bool AIBrain::RandomDecision(float period, float ratio) const
{
  int seed = RandomSeed()&0x7fff;
  float timeMod = fastFmod(Glob.time.toFloat()+seed,period);
  return timeMod<ratio*period;
}


float AIUnit::GetInvAverageSpeed() const
{
  GeographyInfo geogr(0);
  geogr.u.gradient = 1;

  return GetVehicle()->GetBaseCost(geogr, false, true);
}

float AIUnit::GetAverageSpeed() const
{
  return 1.0 / GetInvAverageSpeed();
}

bool DefFindFreePositionCallback(Vector3Par pos, void *context)
{
  return true;
}

bool AIUnit::FindFreePosition
(
  Vector3 &pos, Vector3 &normal, bool soldier, const EntityAIType *vehType,
  FindFreePositionCallback *isFree, void *context
)
{
  OperMap map;
  int mask = MASK_AVOID_OBJECTS | MASK_AVOID_VEHICLES;

  float xf = pos.X() * InvOperItemGrid;
  float zf = pos.Z() * InvOperItemGrid;
  OperMapIndex x(toIntFloor(xf));
  OperMapIndex z(toIntFloor(zf));
  LAND_INDICES(x, z, xLand, zLand);

  int border = 1;
  LandIndex xMin(xLand - border);
  LandIndex xMax(xLand + border + 1);
  LandIndex zMin(zLand - border);
  LandIndex zMax(zLand + border + 1);
  float dx,dz;
  if
  (
    xMax <= 0 || xMin >= LandRange ||
    zMax <= 0 || zMin >= LandRange
  )
  {
    pos[1] = GLOB_LAND->GetSeaLevel();
    normal=VUp;
//    Log("Out of map");
    return false;
  }
  //const EntityAIType *vehType = veh->GetType();
  if (xMin < 0) xMin = LandIndex(0);
  if (xMax > LandRange) xMax = LandIndex(LandRange);
  if (zMin < 0) zMin = LandIndex(0);
  if (zMax > LandRange) zMax = LandIndex(LandRange);
  for (LandIndex zz(zMin); zz<zMax; zz++)
    for (LandIndex xx(xMin); xx<xMax; xx++)
      map.CreateField(xx, zz, mask, vehType, CMAware, soldier);

  bool ok = true;
  if
  (
    map.GetFieldCost(x, z, true, vehType, CMAware, soldier, false).cost >= GET_UNACCESSIBLE ||
    !isFree(pos,context)
  )
  {
    ok = map.FindNearestEmpty
    (
      x, z, xf, zf,
      OperMapIndex(xMin * OperItemRange), OperMapIndex(zMin * OperItemRange),
      OperMapIndex((xMax - xMin) * OperItemRange), OperMapIndex((zMax - zMin) * OperItemRange),
      true, vehType, soldier, true, isFree, context
    );

    if (ok)
    {
      Assert(map.GetFieldCost(x, z, true, vehType, CMAware, soldier, false).cost < GET_UNACCESSIBLE);
    }


    pos[0] = x * OperItemGrid + 0.5 * OperItemGrid;
    pos[2] = z * OperItemGrid + 0.5 * OperItemGrid;
#if _ENABLE_WALK_ON_GEOMETRY
    pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2],Landscape::FilterPrimary(), &dx, &dz);
#else
    pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2], &dx, &dz);
#endif
  }
  else
  {
    // given field is accessible. Check neighbouring fields.
    // If all are accessible, do not change pos, at it is certainly valid
    // ignore locks
    if
    (
      map.GetFieldCost(OperMapIndex(x-1), OperMapIndex(z-1), false, vehType, CMAware, soldier, false).cost >= GET_UNACCESSIBLE ||
      map.GetFieldCost(x,                 OperMapIndex(z-1), false, vehType, CMAware, soldier, false).cost >= GET_UNACCESSIBLE ||
      map.GetFieldCost(OperMapIndex(x+1), OperMapIndex(z-1), false, vehType, CMAware, soldier, false).cost >= GET_UNACCESSIBLE ||
      map.GetFieldCost(OperMapIndex(x-1), z,                 false, vehType, CMAware, soldier, false).cost >= GET_UNACCESSIBLE ||
      map.GetFieldCost(OperMapIndex(x+1), z,                 false, vehType, CMAware, soldier, false).cost >= GET_UNACCESSIBLE ||
      map.GetFieldCost(OperMapIndex(x-1), OperMapIndex(z+1), false, vehType, CMAware, soldier, false).cost >= GET_UNACCESSIBLE ||
      map.GetFieldCost(x,                 OperMapIndex(z+1), false, vehType, CMAware, soldier, false).cost >= GET_UNACCESSIBLE ||
      map.GetFieldCost(OperMapIndex(x+1), OperMapIndex(z+1), false, vehType, CMAware, soldier, false).cost >= GET_UNACCESSIBLE
    )
    {
      pos[0] = x * OperItemGrid + 0.5 * OperItemGrid;
      pos[2] = z * OperItemGrid + 0.5 * OperItemGrid;
#if _ENABLE_WALK_ON_GEOMETRY
      pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2],Landscape::FilterPrimary(), &dx, &dz);
#else
      pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2], &dx, &dz);
#endif

    }
    else
    {
#if _ENABLE_WALK_ON_GEOMETRY
      pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos,Landscape::FilterPrimary(),-1,  &dx, &dz);
#else
      pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos, &dx, &dz);
#endif
    }
  }

  map.ClearMap();
  if (soldier)
  {
    normal=VUp;
  }
  else
  {
    // use difference
    if (vehType->GetShape())
      pos[1] -= vehType->GetShape()->Min().Y();
    normal=Vector3(-dx,1,-dz);
  }
//  Log("Field %d, %d - OK = %d", x, z, ok);
  return ok;
}

bool AIUnit::FindFreePosition()
{
  Point3 pos = Position(GetFutureVisualState());
  Vector3 normal;
  EntityAI *vehicle=GetVehicle();
  if (FindFreePosition(pos, normal, IsSoldier(), vehicle->GetType()))
  {
    Matrix4 transform=vehicle->FutureVisualState().Transform();
    transform.SetPosition(pos);
    transform.SetUpAndDirection(normal,vehicle->FutureVisualState().Direction());
    vehicle->MoveNetAware(transform);
    return true;
  }

  return false;
}

bool AIUnit::FindFreePosition( Vector3 &pos, Vector3 &normal )
{
  EntityAI *vehicle=GetVehicle();
  return FindFreePosition(pos, normal, IsSoldier(), vehicle->GetType());
}

void AIUnit::RefreshMission()
{
  if
  (
    _state != Stopped &&
    _state != Stopping &&
    _state != Delay &&
    _state != InCargo
  )
    Verify(SetState(AIUnit::Wait));

  AIGroup *grp = GetGroup();
  if (!grp)
    return;

  if (grp->Leader() == this && !GetSubgroup()->HasCommand())
  {
    grp->DoRefresh();
  }
}

RString AIUnit::GetDebugName() const
{
  RString buffer;
  AIGroup *grp = GetGroup();
  Person *person = GetPerson();
  if (grp)
    buffer = Format("%s:%d", (const char *)grp->GetDebugName(), ID());
  else if (person)
    buffer = Format("<No group>:%d (%s)", ID(),cc_cast(person->GetType()->GetName()));
  if (!GWorld)
  {
    return buffer + RString("- no world");
  }
  if (person && grp && person->IsNetworkPlayer())
    buffer = buffer + Format(" (%s)", (const char *)person->GetPersonName());
  if (person && !person->IsLocal()) buffer = buffer + RString(" REMOTE");

  if (person && person->GetVarName().GetLength() > 0) buffer = buffer + Format(" (%s)", cc_cast(person->GetVarName()));

  return buffer;
}

bool AIUnit::AssertValid() const
{
  bool ok = AIBrain::AssertValid();

  AISubgroup *subgrp = GetSubgroup();
  if (subgrp)
  {
    AIGroup *grp = subgrp->GetGroup();
    if (grp)
    {
      AICenter *center = grp->GetCenter();
      if (!center)
      {
        ReportUnit(this);
        RptF(" - no center");
        ok = false;
      }
    }
    else
    {
      ReportUnit(this);
      RptF(" - no group");
      ok = false;
    }
  }
  else
  {
    ReportUnit(this);
    RptF(" - no subgroup");
    ok = false;
  }

  // TODO: test position in driver / commander / gunner / cargo
  return ok;
}

void AIUnit::Dump(int indent) const
{
}

// Behavior FSM

#define FSM_RETURN(value) return (value) ? 1.0f : 0.0f

float AIUnit::FSMBehaviourCombat(const FSMEntity *fsm, const float *params, int paramsCount) const
{
  // when fleeing, we do not want the formation FSM to hinder our progress
  bool isCombat = (
    GetCombatMode() >= CMCombat && GetGroup() && !GetGroup()->GetFlee() && CanUseCover()
    && GetCoveringBasedOnCommand()
  );
  #if 0 //_ENABLE_CHEATS
  if (unconst_cast(this)->GetUnit() && unconst_cast(this)->GetUnit()->IsSubgroupLeader())
  {
    if (isCombat)
    {
      LogF("%s: is combat", cc_cast(GetDebugName()));
}
  }
  #endif
  FSM_RETURN(isCombat);
}

float AIUnit::FSMVehicleAir(const FSMEntity *fsm, const float *params, int paramsCount) const
{
  EntityAIFull *vehicle = GetVehicle();
  if (!vehicle) return 0.0f;
  const EntityAIType *type = vehicle->GetType();
  if (!type) return 0.0f;

  FSM_RETURN(type->IsKindOf(GWorld->Preloaded(VTypeAir)));
}

float AIUnit::FSMVehicle(const FSMEntity *fsm, const float *params, int paramsCount) const
{
  // detect any transportation (vehicle)
  EntityAIFull *vehicle = GetVehicle();
  if (!vehicle) return 0.0f;

  FSM_RETURN(dyn_cast<Transport>(vehicle));
}

float AIUnit::FSMFormationIsLeader(const FSMEntity *fsm, const float *params, int paramsCount) const
{

  // formation member never needs to keep the destination accurately
  switch (_planningModeWanted)
  {
  case VehiclePlanned: // vehicles are not using the cover
  case LeaderDirect: // when planning mode is direct, we cannot use the cover
    return 1.0f;
  case LeaderPlanned:
    // leader can use cover, unless command destination is near - in such case he needs to obey the command
    return 0;
    /*
    // note: the condition is not very important - the path finding should guarantee a convergence to target anyway
    {
      EntityAIFull *vehicle = GetVehicle();
      float prec = vehicle->GetPrecision();
      if (_wantedPositionWanted.Distance2(Position())>Square(15.0f*prec)) return 0.0f;
      return 1.0f;
    }
    */
  // even when we do not wan to plan, in case some cover is find, we can always discard it if needed
  //case DoNotPlan: return 1.0f;
  default: // case FormationPlanned: case DoNotPlanFormation:
    // formation members may use cover freely
    return 0.0f;
  }
}

void AIUnit::ProvideSuppressiveFireAsNeeded()
{
  // only vehicles are able to provide a suppressive fire while moving
  Transport *vehicle = GetVehicleIn();
  if (!vehicle)
  {
    EntityAIFull *veh = GetVehicle();
    if (veh) GetVehicle()->AllowSuppressiveFire(SuppressNo);
    return;
  }
  if (GetCombatMode()<CMCombat)
  {
    // only vehicles in combat mode may apply suppressive fire in this state
    vehicle->AllowSuppressiveFire(SuppressNo);
    return;
  }

#if 1
  // version 1
  // vehicles in combat mode always provide suppressive fire
  vehicle->AllowSuppressiveFire(SuppressYes);
#else
  // version 2
  // suppressive fire provided only as needed by formation neighbours
  // check our formation neighbours
  struct SuppressiveRequired: AIUnit::ForEachFormationCallback
  {
    bool suppress;
    
    SuppressiveRequired():suppress(false)
    void Do(const AIUnit *me, const AIUnit *next)
    {
      if (next->GetVehicle()->CheckSupressiveFireRequired())
        suppress = true;
    }
  } suppressiveRequired;
  ForEachFormationNeighbour(suppressiveRequired);
  if (suppressiveRequired.suppress)
  {
    vehicle->AllowSuppressiveFire(SuppressYes);
  }
  else
  {
    vehicle->AllowSuppressiveFire(SuppressNo);
  }
#endif
}

void AIUnit::FSMFormationExcludedIn(FSMEntity *fsm, const float *params, int paramsCount)
{
  SetUnitPositionFSM(UPAuto);
  ProvideSuppressiveFireAsNeeded();

  EntityAIFull *vehicle = GetVehicle();
  if (vehicle)
  {
    vehicle->EnableReload(true);
  }

#if _ENABLE_CHEATS
  const EntityAIType *type = vehicle ? vehicle->GetType() : NULL;

  if (GetCombatMode() == CMCombat && type && !type->IsKindOf(GWorld->Preloaded(VTypeAir)))
  {
    _fsmDiagSize = 0.1;
    _fsmDiagColor = PackedColor(Color(0, 1, 0, 1));
  }
#endif

}
void AIUnit::FSMFormationExcludedOut(FSMEntity *fsm, const float *params, int paramsCount)
{}

AIUnit *AIUnit::FSMGetFormationNeighbour() const
{
  // return the closest formation member
  AISubgroup *subgrp = GetSubgroup();
  if (!subgrp) return NULL;

  const AIBrain *commander = this;
  const Transport *vehicleIn = GetVehicleIn();
  if (vehicleIn) commander = vehicleIn->CommanderUnit();

  AIUnit *neighbour = NULL;
  float minDist2 = FLT_MAX;
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *unit = subgrp->GetUnit(i);
    if (!unit || unit->IsAnyPlayer() || unit == commander) continue;
    Transport *veh = unit->GetVehicleIn();
    if (veh && veh->CommanderUnit() != unit) continue;
    float dist2 = Position(GetFutureVisualState()).Distance2(unit->Position(unit->GetFutureVisualState()));
    if (dist2 < minDist2)
    {
      neighbour = unit;
      minDist2 = dist2;
    }
  }
  return neighbour;
}

AIUnit *AIUnit::FSMGetFormationNext() const
{
  // return the formation member with the closest upper ID
  AISubgroup *subgrp = GetSubgroup();
  if (!subgrp) return NULL;

  AIUnit *head = NULL;
  AIUnit *neighbour = NULL;
  int myId = ID();
  int bestId = INT_MAX;
  int minId = INT_MAX;
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *unit = subgrp->GetUnit(i);
    if (!unit || unit == this || !unit->IsUnit()) continue;
    int id = unit->ID();
    if (id < minId)
    {
      minId = id;
      head = unit;
    }
    if (id<=myId) continue;
    // we can respect positions of excluded members - they are moving with no covering
    //if (!strcmp(unit->GetFormationTask(),"EXCLUDED")) continue;
    if (bestId>id)
    {
      bestId = id;
      neighbour = unit;
    }
  }
  if (!neighbour) return head;
  return neighbour;
}

void AIUnit::ForEachFormationNext(ForEachFormationCallback &callback) const
{
  AISubgroup *subgrp = GetSubgroup();
  if (!subgrp) return;
  AIGroup *grp = subgrp->GetGroup();
  if (!grp) return;
  
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    // when I am his parent, execute the callback
    AIUnit *unit = subgrp->GetUnit(i);
    int parent = subgrp->GetFormationParent(unit);
    if (parent==ID()-1)
    {
      callback.Do(this,unit);
    }
  }
}

void AIUnit::ForEachFormationPrev(ForEachFormationCallback &callback) const
{
  // prev is the one is I am following
  AISubgroup *subgrp = GetSubgroup();
  if (!subgrp) return;
  AIGroup *grp = subgrp->GetGroup();
  if (!grp) return;

  int slot = ID()-1;
  if (slot<0) return;
  
  int parent = subgrp->GetFormationParent(this);
  
  if (parent >= 0)
  {
    const AIUnit *base = GetGroup()->GetUnit(parent);
    
    if (base) callback.Do(this,base);
  }
}

void AIUnit::ForEachFormationNeighbour(ForEachFormationCallback &callback) const
{
  ForEachFormationPrev(callback);
  ForEachFormationNext(callback);
}

AIUnit *AIUnit::FSMGetFormationPrev() const
{
  // return the formation member with the closest lower ID
  AISubgroup *subgrp = GetSubgroup();
  if (!subgrp) return NULL;

  AIUnit *neighbour = NULL;
  AIUnit *tail = NULL;
  int myId = ID();
  int bestId = INT_MIN;
  int maxId = INT_MIN;
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *unit = subgrp->GetUnit(i);
    if (!unit || unit == this || !unit->IsUnit()) continue;
    int id = unit->ID();
    if (id > maxId)
    {
      maxId = id;
      tail = unit;
    }
    if (id>=myId) continue;
    //if (!strcmp(unit->GetFormationTask(),"EXCLUDED")) continue;
    if (bestId<id)
    {
      bestId = id;
      neighbour = unit;
    }
  }
  if (!neighbour) return tail;
  return neighbour;
}


void AIUnit::FSMFormationInitIn(FSMEntity *fsm, const float *params, int paramsCount)
{

#if _ENABLE_CHEATS
  _fsmDiagSize = 0.1;
  _fsmDiagColor = PackedColor(Color(1, 1, 1, 1));
#endif
}
void AIUnit::FSMFormationInitOut(FSMEntity *fsm, const float *params, int paramsCount)
{}


float AIUnit::FSMReloadNeeded(const FSMEntity *fsm, const float *params, int paramsCount) const
{
  EntityAIFull *vehicle = GetVehicle();
  if (!vehicle) return 0;

  bool reloadNeeded = vehicle->GetReloadNeeded() >= 1.0f;

#if _ENABLE_CHEATS
  if (reloadNeeded)
  {
    _fsmDiagSize = 0.1;
    _fsmDiagColor = PackedColor(Color(0, 0, 0, 1));
  }
#endif

  FSM_RETURN(reloadNeeded);
}


float AIUnit::FSMCoverReached(const FSMEntity *fsm, const float *params, int paramsCount) const
{
  EntityAIFull *vehicle = GetVehicle();
  if (!vehicle) return 0;

  if (vehicle->CheckCoverEntered())
  {
    return 1;
  }
  return 0;
}

/** provide cover - assume firing position, suppress */
void AIUnit::FSMFormationProvideCoverIn(FSMEntity *fsm, const float *params, int paramsCount)
{
  EntityAIFull *vehicle = GetVehicle();

  float maxTime = 2.0f;
  if (paramsCount>0) maxTime = params[0];

  _fsmTimeStart = Glob.time;
  _fsmTimeNow = Glob.time;
    _fsmTimeMin = 3.0f + 5.0f * GRandGen.RandomValue();
    _fsmTimeMax = _fsmTimeMin + 5.0f + 10.0f * GRandGen.RandomValue();

  float height, dist, xSpacing, ySpacing;

  if (IsFreeSoldier())
  {
    dist = 15;
    UnitPosition pos = GetUnitPosition();
    if (pos != UPDown)
    {
      if (GRandGen.RandomValue() > 0.5)
        pos = UPMiddle;
      else
        pos = UPDown;
    }
    if (pos == UPMiddle)
    {
      height = 1.5;
      xSpacing = 6;
      ySpacing = 1.5;
    }
    else // pos == UPDown
    {
      height = 0.5;
      xSpacing = 4;
      ySpacing = 0.5;
    }
    SetUnitPositionFSM(pos);
  }
  else
  {
    dist = 30;
    height = 2;
    xSpacing = 10;
    ySpacing = 2;
  }

  Vector3 pos = vehicle->FutureVisualState().Position();
  pos[1] += height;

  Vector3 dir = vehicle->FutureVisualState().Direction();
  AIBrain *brain = vehicle->CommanderUnit();
  if (brain)
  {
    AIUnit *unit = brain->GetUnit();
    if (unit) dir = unit->GetFormationDirection();
  }

  Vector3 posC = pos + dist * dir;
  Vector3 aside = xSpacing * dir.CrossProduct(VUp);
  Vector3 up = ySpacing * VUp;

  _fsmTargets[0] = posC;
  _fsmTargets[1] = posC - aside + up;
  _fsmTargets[2] = posC + aside + up;
  _fsmTargets[3] = posC - aside - up;
  _fsmTargets[4] = posC + aside - up;

  const EntityAIType *tgtType = GWorld->Preloaded(VTypeFireSectorTarget);

  Transport *vehicleIn = GetVehicleIn();
  if (vehicleIn)
  {
    AIBrain *gunner = vehicleIn->GunnerUnit();
    if (gunner)
    {
      Target *target = gunner->CreateGhostTarget(tgtType,posC,4.0f, 0.0f, 0.0f);
      if (target)
      {
        gunner->SetWatchTarget(target);
        _fsmDummyTarget = target;
      }
    }
  }
  else
  {
    Target *target = CreateGhostTarget(tgtType,posC,4.0f, 0.0f, 0.0f);

    if (target)
    {
      if (GetTargetAssigned() == NULL)
      {
        SetWatchTarget(target);
      }
      _fsmDummyTarget = target;
    }
  }

  float cautious = GetCautiousInCombat();
  _fsmDelay = 0.5f + (0.5f + 4.0f * GRandGen.RandomValue()) * cautious;

#if _ENABLE_CHEATS
  _fsmDiagSize = 0.1;
  _fsmDiagColor = PackedColor(Color(0, 0, 1, 1));
#endif

  vehicle->AllowSuppressiveFire(SuppressYes);

  // prevent "covering" message while reloading - would be sent repeatedly
  if (vehicle->GetReloadNeeded() < 1.0f)
  {
    AIGroup *grp = GetGroup();
    if (grp && IsFreeSoldier() && GetSubgroup()->NUnits()>1) // no need to coordinate when alone
    {
      grp->SendCovering(this);
    }
  }
}

/** provide cover - hide in a cover, suppress only when possible while hidden*/
void AIUnit::FSMFormationProvideCoverOut(FSMEntity *fsm, const float *params, int paramsCount)
{}

void AIUnit::FSMFormationHideInCoverIn(FSMEntity *fsm, const float *params, int paramsCount)
{
  FSMFormationNextTargetIn(fsm,params,paramsCount);
  EntityAIFull *vehicle = GetVehicle();
  if (vehicle)
  {
    vehicle->AllowSuppressiveFire(SuppressNo);
  }
}

void AIUnit::FSMFormationHideInCoverOut(FSMEntity *fsm, const float *params, int paramsCount)
{}

void AIUnit::FSMFormationNextTargetIn(FSMEntity *fsm, const float *params, int paramsCount)
{
  _fsmTimeNow = Glob.time;

  if (IsFreeSoldier())
  {
    UnitPosition pos = GetUnitPosition();
    if (pos == UPMiddle && GRandGen.RandomValue() > 0.7)
    {
      // switch the pose from Middle to Down and recalculate scanned area
      float dist = 15;
      float height = 0.5;
      float xSpacing = 4;
      float ySpacing = 0.5;

      SetUnitPositionFSM(UPDown);

      EntityAIFull *vehicle = GetVehicle();

      Vector3 pos = vehicle->FutureVisualState().Position();
      pos[1] += height;

      Vector3 dir = vehicle->FutureVisualState().Direction();
      AIBrain *brain = vehicle->CommanderUnit();
      if (brain)
      {
        AIUnit *unit = brain->GetUnit();
        if (unit) dir = unit->GetFormationDirection();
      }

      Vector3 posC = pos + dist * dir;
      Vector3 aside = xSpacing * dir.CrossProduct(VUp);
      Vector3 up = ySpacing * VUp;

      _fsmTargets[0] = posC;
      _fsmTargets[1] = posC - aside + up;
      _fsmTargets[2] = posC + aside + up;
      _fsmTargets[3] = posC - aside - up;
      _fsmTargets[4] = posC + aside - up;
    }

    if (GetTargetAssigned() == NULL && _fsmDummyTarget)
    {
      SetWatchTarget(_fsmDummyTarget);
    };
  }

  int index = toInt(lenof(_fsmTargets) * GRandGen.RandomValue());
  saturate(index, 0, lenof(_fsmTargets) - 1);
  if (_fsmDummyTarget)
  {
    _fsmDummyTarget->UpdatePosition(_fsmTargets[index],4,0);
  }
  EntityAIFull *vehicle = GetVehicle();
  if (vehicle)
  {
    vehicle->AllowSuppressiveFire(SuppressYes);
  }
}
void AIUnit::FSMFormationNextTargetOut(FSMEntity *fsm, const float *params, int paramsCount)
{}



Target *AIUnit::GetFireSectorTarget() const
{
  return _fsmDummyTarget;
};

void AIUnit::FSMFormationLeaderIn(FSMEntity *fsm, const float *params, int paramsCount)
{
  ProvideSuppressiveFireAsNeeded();
}
void AIUnit::FSMFormationLeaderOut(FSMEntity *fsm, const float *params, int paramsCount)
{}


float AIUnit::FSMRandomDelay(const FSMEntity *fsm, const float *params, int paramsCount) const
{
  /*
  (time - _timeNow) >= _delay
  */
  FSM_RETURN((Glob.time - _fsmTimeNow) >= _fsmDelay);
}

void AIUnit::ComputeHideFrom(Vector3 &hideFromL, Vector3 &hideFromR)
{
  EntityAIFull *vehicle = GetVehicle();

  // set what target are we "hiding from" (used to select suitable cover)
  float radius = vehicle->GetType()->GetMaxSpeedMs() * 20;
  saturate(radius, 20, 500);

  if (_enemyDirL.SquareSize()>Square(0.1f))
  {
    // we need to hide from an angle, not a single location
    hideFromL =  vehicle->FutureVisualState().Position() + _enemyDirL * radius;
    hideFromR =  vehicle->FutureVisualState().Position() + _enemyDirR * radius;
  }
  else
  {
    // general enemy direction not known - use heuristics
    TurretContext context;
    Target *tgt = NULL;
    if (vehicle->GetPrimaryGunnerTurret(context))
    {
      if (context._weapons->_fire._fireTarget && !context._weapons->_fire._firePrepareOnly)
      {
        tgt = context._weapons->_fire._fireTarget;
      }
    }
    if (!tgt) tgt = GetTargetAssigned();
    // what position do we hide from
    if (tgt) // we prefer an enemy
    {
      hideFromR = hideFromL = tgt->LandAimingPosition(this);
    }
    else
    {
      Vector3 formDir;
      if (GetSubgroup())
      {
        // formation direction is a general direction of formation orientation, i.e. of expected threat
        formDir = GetSubgroup()->GetFormationDirection();
      }
      else
      {
        formDir = vehicle->FutureVisualState().Direction();
      }
      
      Matrix3 rotWiderL(MRotationY,+H_PI*0.2f);
      Matrix3 rotWiderR(MInverseRotation,rotWiderL);
      
      // hiding against one direction only
      // create a little bit wider assumed enemy zone
      Vector3 offFromL = rotWiderL*(formDir*radius);
      Vector3 offFromR = rotWiderR*(formDir*radius);
      hideFromL = vehicle->FutureVisualState().Position() + offFromL;
      hideFromR = vehicle->FutureVisualState().Position() + offFromR;
    }
  }

}


void AIUnit::FSMSearchPathIn(FSMEntity *fsm, const float *params, int paramsCount)
{
  EntityAIFull *vehicle = GetVehicle();
  if (!vehicle) return;

  float maxCost = paramsCount>0 ? params[0] : 0;  // note: this is 10 for ArmA 2
  float maxDelay = paramsCount>1 ? params[1] : 0; // note: this is 5 for ArmA 2
  // update destination for the path planner 
  PlanPosition position;
  AIBrain::PlanningMode planningMode;
  bool forceReplan;
  float precision;
  GetExpectedDestination(position, precision, planningMode, forceReplan);
  
  #if 0 //_ENABLE_CHEATS
  if (GetUnit() && GetUnit()->IsSubgroupLeader())
  {
    LogF("%s: maxCost %.2f, planningMode %s", cc_cast(GetDebugName()), maxCost,cc_cast(FindEnumName(planningMode)));
  }
  #endif
  
  // when leader is close to a command, he may need to respect the command precision
  // when some special planning mode is used, we need to avoid searching for a cover
  // TODO: better precision handling
  if (
    planningMode!=LeaderPlanned && planningMode!=FormationPlanned && planningMode!=DoNotPlanFormation && planningMode!=DoNotPlan
  )
  {
    maxDelay = 0, maxCost = 0;
  }
  // set path finding dispositions so that A* is seeking for a cover
  // note: A* tends to underestimate the cost a lot
  // the solution could be to use ModePenalty for GetCost, however we do not affect the path costing itself
  // rather we compensate for it here
  CombatMode mode = GetCombatMode();
  float costFactor = vehicle->GetType()->GetCostModeFactor(mode);

  // based on a distance from the leader we may use faster movement
  static float distFar = 300.0f;
  static float distNear = 30.0f;
  // move in longer steps
  static float coefCoverFar = 12.0f;
  static float coefCoverNear = 1.0f;
  // allow only a little longer delays
  static float coefDelayFar = 2.0f;
  static float coefDelayNear = 1.0f;
  float behindCoverFactor = InterpolativC(GetBehindFormation(),distNear,distFar,coefCoverNear,coefCoverFar);
  float behindDelayFactor = InterpolativC(GetBehindFormation(),distNear,distFar,coefDelayNear,coefDelayFar);
  
  _maxCostToCover = maxCost*costFactor*behindCoverFactor;
  _maxDelayByCover = maxDelay*costFactor*behindDelayFactor;

  ComputeHideFrom(_fsmHideFromL,_fsmHideFromR);

  #if 0 // _PROFILE
  if (GetUnit() && GetUnit()->ID()==2)
  {
    LogF("%s: SetWantedDestination %.1f,%.1f",cc_cast(GetDebugName()),position.X(),position.Z());
  }
  #endif

  // SetWantedDestination will allow planning - we call it after setting all other parameters
  SetWantedDestination(position, planningMode, forceReplan);
  
  // if we are allowed to leave cover, but did not leave it yet, we want to provide suppression
  if (vehicle->IsInCover() && GetCombatMode() >= CMCombat)
  {
    vehicle->AllowSuppressiveFire(SuppressYes);
  }
  else
  {
    // TODO: consider updating FSM instead
    if (vehicle->GetAllowSuppressiveFire()==SuppressYes)
    {
      // reduce suppression level when moving
      vehicle->AllowSuppressiveFire(SuppressAuto);
    }
  }
}


void AIUnit::FSMSearchPathOut(FSMEntity *fsm, const float *params, int paramsCount)
{
}



float AIUnit::FSMFormationCanLeaveCover(const FSMEntity *fsm, const float *params, int paramsCount) const
{

#if _ENABLE_CHEATS
  float age = Glob.time - _fsmTimeStart;
  _fsmDiagSize = 0.2 * (1.0 - age / _fsmTimeMin);
#endif
  if (!GetSubgroup()) return 1.0f;

  EntityAIFull *vehicle = GetVehicle();

  // while seeking for cover is in progress, we wait

  float prec = vehicle->GetPrecision();
  float form = vehicle->GetFormationZ();

  // synchronize with the one before us and the one behind us
  // run only when we are the most "behind" of all three
  // for the leader "prev" is the tail of the formation
  #if _DEBUG || _PROFILE
    static int debugId = -1;
    if (ID()==debugId)
    {
      __asm nop; // debug opportunity
    }
  #endif

  float iAmBehind = GetBehindFormation();

  // when not behind the formation, there is no need to ever leave the cover, unless we are the leader
  if (iAmBehind<prec && !IsSubgroupLeader())
    return 0.0f;
  
  if (
    // in a real cover
    //vehicle->IsInCover()>=EntityAIFull::InRealCover &&
    // subordinate of a player standing still with hiding allowed, or hiding enforced (e.g. by a hide command)
    (GetSubgroup()->IsAnyPlayerSubgroup() && GetSubgroup()->Leader()->GetVehicle()->IsStandingStill() && CanUseCover() || MustUseCover())
  )
  { // in player's subgroup do not leave a cover if close enough to the formation position
    // unless the cover is a real cover, apply quite strict criteria
    float maxDist = vehicle->IsInCover()<EntityAIFull::InRealCover ? form : MustUseCover() ? form*10 : form*2;
    
    Vector3 formPos = GetFormationAbsolute();
    float dist2 = formPos.DistanceXZ2(Position(GetFutureVisualState()));
    if (dist2<Square(maxDist))
      return 0.0f;
  }

  struct CheckBehind: AIUnit::ForEachFormationCallback
  {
    float lastBehindMe;
    float firstBehindMe;
    const AIUnit *lastBehindUnit;
    const AIUnit *firstBehindUnit;
    
    CheckBehind()
    {
      lastBehindMe = -FLT_MAX;
      firstBehindMe = +FLT_MAX;
      lastBehindUnit = NULL;
      firstBehindUnit = NULL;
    }
    void Do(const AIUnit *me, const AIUnit *next)
    {
      float behind = next->GetBehindFormation();
      if(lastBehindMe<behind)
        lastBehindMe = behind, lastBehindUnit = next;
      if (firstBehindMe>behind)
        firstBehindMe = behind, firstBehindUnit = next;
    }
  };
  CheckBehind checkBehindNext;
  CheckBehind checkBehindPrev;
  ForEachFormationNext(checkBehindNext);
  ForEachFormationPrev(checkBehindPrev);
  
  // we do not want to leave: somebody next is behind us - we expect him to start moving sooner and we will cover him
  // we want to leave: somebody prev is in front of us
  // we have to balance those two urges
  
  // GetBehindFormation tells how much I am behind my prev, with some respect to my leader
  // this should tell me 
  // wait for the last of both prev and next
  float lastBehindMe = checkBehindNext.lastBehindMe;
  float firstBehindMe = checkBehindNext.firstBehindMe;
  
  const AIUnit *lastUnit = checkBehindNext.lastBehindUnit;

  float behindFactor = 1-iAmBehind/floatMax(lastBehindMe*2.0f+form*2,form*8);
  saturate(behindFactor,0.1f,1.0f);
  
  // depending on how much we are behind formation (considering out neighbours as well), we want to be less cautious
  float cautious = floatMin(behindFactor,GetCautiousInCombat());
  //if (lastUnit)
  {
    EntityAIFull *mostBehindVeh = lastUnit ? lastUnit->GetVehicle() : NULL;

    // follow the the first of both prev and next
    // in front of me is always at most one unit
    Assert(IsSubgroupLeaderVehicle() || checkBehindPrev.lastBehindUnit==checkBehindPrev.firstBehindUnit);
    float firstInFrontOfMe = IsSubgroupLeaderVehicle() ? FLT_MAX : -checkBehindPrev.firstBehindMe;
    const AIUnit *firstUnit = IsSubgroupLeaderVehicle() ? NULL : checkBehindPrev.firstBehindUnit;
    #if _DEBUG || _PROFILE
    if (firstUnit && firstUnit->IsAnyPlayer())
    {
      __asm nop;
      if (firstInFrontOfMe>form*10)
      {
        __asm nop;
        if (ID()==debugId)
        {
          __asm nop; // debug opportunity
        }
      }
    }
    #endif

    // I need to run, when: none of my neighbours is behind me
    // some of my neighbours is in front of me, or I am formation leader


    if (mostBehindVeh && !mostBehindVeh->QIsManualMovement())
    {
      // when AI neighbour we are waiting for is not in cover, we need to cover him
      // unless the more forward unit is far in front of us, in which case we need to move
      // this is important with units which are never in cover, like when is careless mode
      if (firstInFrontOfMe>=form*4)  // too far in front of us, we may need to move
        {}
      else if (mostBehindVeh->GoingToCover()>=cautious)  // in cover, may we need to move
        {}
      else
        return 0.0f;
    }


    Vector3 formPos = GetFormationAbsolute();
    float dist2 = formPos.DistanceXZ2(Position(GetFutureVisualState()));
    if (dist2>Square(form*4) && iAmBehind>prec) // when in formation, do not force any movement
    {
      // when not caution, pretend we are more behind than we really are to leave the cover sooner
      iAmBehind += (1-cautious)*form;
    }
    // as long as some of our neighbours is behind us, we may need to provide a cover
    // however we do not do this when we are to much behind
    if (lastBehindMe>prec && iAmBehind<lastBehindMe+form)
    {
      // when the one behind is the player and not the leader, we may need to wait for him
      if (lastUnit && lastUnit->IsAnyPlayer() && !lastUnit->IsSubgroupLeader())
      {
        // as long as the player is moving, we may need to cover him
        if (mostBehindVeh && !mostBehindVeh->IsStandingStill())
        {
          // unless he is moving too long, in which case we can start moving
          if (Glob.time - _fsmTimeStart < _fsmTimeMax)
            return 0.0f;
        }
        
        // the one was a player - check our parent. If it behind us, we may still wait
        if (firstUnit && firstBehindMe>prec)
          return 0.0f;
        // wait longer if there are no AI neighbours of they are not - player cooperation more important
        // we want to wait for a while before we decide the player is not willing to cooperate
        if (Glob.time - _fsmTimeStart < _fsmTimeMax*3.0f)
        {
          // TODO: shout at the player to move
          return 0.0f;
        }
        
      }
      else
      {
        // as long as AI is behind us, cover him (and wait for him to run - we know he will)
        // exception: pair leader should not wait when too much behind, unless the last is a lot behind
        if (iAmBehind<form*2 || lastBehindMe>form*3)
          return 0.0f;
      }

    }
  }

  float delayFactor = floatMax(cautious,0.1f);

  //if (Glob.time - _fsmTimeStart >= _fsmTimeMax) return 1.0f;
  // we always want to stay in a cover for some minimal time
  if (Glob.time - _fsmTimeStart < _fsmTimeMin*delayFactor) return 0.0f;

  Assert(iAmBehind>-4*form);
  return 1.0f;
}

void AIUnit::FSMSetUnitPosToDownIn(FSMEntity *fsm, const float *params, int paramsCount)
{
  SetUnitPositionFSM(UPDown);
}
void AIUnit::FSMSetUnitPosToDownOut(FSMEntity *fsm, const float *params, int paramsCount)
{}


void AIUnit::FSMReloadIn(FSMEntity *fsm, const float *params, int paramsCount)
{
  EntityAIFull *vehicle = GetVehicle();
  if (!vehicle) return;
  vehicle->AllowSuppressiveFire(SuppressNo);
  vehicle->Reload();
}
void AIUnit::FSMReloadOut(FSMEntity *fsm, const float *params, int paramsCount)
{}

void AIUnit::FSMFormationCleanUpIn(FSMEntity *fsm, const float *params, int paramsCount)
{
  EntityAIFull *vehicle = GetVehicle();

  Transport *vehicleIn = GetVehicleIn();
  if (vehicleIn)
  {
    AIBrain *gunner = vehicleIn->GunnerUnit();
    if (gunner && _fsmDummyTarget)
    {
      gunner->DeleteGhostTarget(_fsmDummyTarget);
      gunner->SetWatchTarget(NULL);
    }
  }
  else
  {
    if (_fsmDummyTarget)
    {
      if (GetTargetAssigned() == _fsmDummyTarget)
      {
        AssignTarget(TargetToFire());
      }
      DeleteGhostTarget(_fsmDummyTarget);
    }
  }
  _fsmDummyTarget = NULL;

  SetUnitPositionFSM(UPAuto);
  // covering from us no longer needed - we can stop suppressing
  vehicle->AllowSuppressiveFire(SuppressAuto);

  // disable planning until search parameters are updated
  _planParametersSet = false;
  
  vehicle->LeaveCover();

#if _ENABLE_CHEATS
  _fsmDiagSize = 0.1;
#endif
}
void AIUnit::FSMFormationCleanUpOut(FSMEntity *fsm, const float *params, int paramsCount)
{
}

// redefines group parameters
void AIUnit::EditHCGroup(int index, AIHCGroup *group)
{
      if(group && group->Name().GetLength()>0)  _hcGroups[index].SetName(group->Name());
      _hcGroups[index].SetTeam(group->Team());
      return;
}

// add group under unit's command 
void AIUnit::AddHCGroup(AIHCGroup group)
{
  //delete old groups
  for(int i=_hcGroups.Size()-1; i>=0; i--)
  {
    if(!_hcGroups[i].GetGroup())
    {
      _hcGroups.Delete(i);
    }
  }

  if(!group.GetGroup()) return;
  //check if groups is not already under command
  for(int i=0; i<_hcGroups.Size(); i++)
  {
    if(_hcGroups[i].GetGroup() == group.GetGroup())
    {
      EditHCGroup(i,&group);
      return;
    }
  }
  //unit already has a commander
  if(group.GetGroup()->HCCommander()) return;

  //try to find some empty space after removed group
  // - if group is removed, other groups keeps their current position (needed fo F1-10) 
  for(int i=0; i<_hcGroups.Size(); i++)
  {
   if(i<_hcGroups[i].ID())
   {
     group.SetID(i); //(ID - _groupInfoOffset is F-key)
     _hcGroups.Insert(i,group);
     group.GetGroup()->SetHCCommander(this);
     return;
   }
  }
  //if there is no hole, put groups at the end
  group.SetID(_hcGroups.Size());
  group.GetGroup()->SetHCCommander(this);
  _hcGroups.Add(group);
}


// remove group from unit's command
void AIUnit::RemoveHCGroup(AIGroup *group)
{
  if(!group) return;
  for(int i=0; i<_hcGroups.Size(); i++)
  {
    if(_hcGroups[i].GetGroup() && _hcGroups[i].GetGroup() == group)
    {
      _hcGroups[i].GetGroup()->SetHCCommander(NULL);
      _hcGroups.Delete(i);
      return;
    }
  }
}

void AIUnit::ClearHCGroups()
{
  for(int i=0; i<_hcGroups.Size(); i++)
  {
    if(_hcGroups[i].GetGroup()) _hcGroups[i].GetGroup()->SetHCCommander(NULL);
  }
  _hcGroups.Clear();
};
  
// returns HCGroup from unit's command in HC 
AIHCGroup *AIUnit::GetHCGroup(AIGroup *group)
{
  if(!group) return NULL;
  for(int i=0; i<_hcGroups.Size(); i++)
  {
    if(_hcGroups[i].GetGroup() && _hcGroups[i].GetGroup() == group)
    {
      return (&_hcGroups[i]);
    }
  }
  return NULL;
};

//! select unit from unit's command
void AIUnit::SelectedHCGroups(RefArray<AIGroup> &selection)
{
  for(int i=0; i<_hcGroups.Size(); i++)
  {
    if(_hcGroups[i].IsSelected() && _hcGroups[i].GetGroup())
    {
      selection.Add(_hcGroups[i].GetGroup());
    }
  }
}
