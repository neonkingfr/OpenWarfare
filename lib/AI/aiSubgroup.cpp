// AI - implementation of Subgroups

#include "../wpch.hpp"
#include "ai.hpp"
#include "aiRadio.hpp"
#include "../global.hpp"
#include "../person.hpp"

#include "../world.hpp"
#include "../landscape.hpp"
//#include "loadStream.hpp"
#include "../paramArchiveExt.hpp"
#include <El/Common/perfProf.hpp>

#include "../roads.hpp"

//#include "strIncl.hpp"
#include "../stringtableExt.hpp"

#include "../Network/network.hpp"

#include <El/Enum/enumNames.hpp>
#include <El/HiResTime/hiResTime.hpp>

///////////////////////////////////////////////////////////////////////////////
// Parameters

#include "aiDefs.hpp"


#define LOST_UNIT_MIN                 45.0F
#define LOST_UNIT_MAX                 300.0F

#define CRITICAL_EXPOSURE_CHANGE        500.0F 

#define LOG_FORMATION_COEF  0

#if _ENABLE_AI
DEFINE_FAST_ALLOCATOR(PathTreeNode)
#endif

///////////////////////////////////////////////////////////////////////////////
// class Command

LSError Command::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeEnum("message", _message, 1))
  CHECK(ar.SerializeRef("Target", _target, 1))
  CHECK(ar.SerializeRef("TargetE", _targetE, 1))
  CHECK(::Serialize(ar, "destination", _destination, 1, VZero))
  CHECK(::Serialize(ar, "direction", _direction, 1, VZero))
  CHECK(::Serialize(ar, "time", _time, 1))
  CHECK(ar.SerializeRef("JoinTo", _joinToSubgroup, 1))
  CHECK(ar.Serialize("action", _action, 1))
  CHECK(ar.Serialize("intParam", _intParam, 1, -1))
  CHECK(ar.Serialize("strParam", _strParam, 1, RString()))
  CHECK(ar.SerializeEnum("discretion", _discretion, 1, Undefined))
  CHECK(ar.SerializeEnum("movement", _movement, 1, MoveUndefined))
  CHECK(ar.SerializeEnum("context", _context, 1, CtxAuto))
  CHECK(ar.Serialize("precision", _precision, 1, 0.0f))
  CHECK(ar.Serialize("id", _id, 1, -1))

  //serialize NetworkObject
  NetworkObject::Serialize(ar); //base

  if ( ar.IsLoading() && (ar.GetPass()==ParamArchive::PassSecond) 
    && !GetNetworkId().IsNull() )
  {
    //Calling CreateObject is not sufficient
    //GetNetworkManager().CreateObject(this, false /*not setNetworkId*/);
    // we should call appropriate CreateXXX methods when everything is serialized (e.g. subobjects,...)
    // store the "action" to see, what should be created "at least"
    GetNetworkManager().RememberSerializedObject(this);
  }

  return LSOK;
}

#define COMMAND_MSG_LIST(XX) \
  XX(Create, CreateCommand) \
  XX(UpdateGeneric, UpdateCommand)

DEFINE_NETWORK_OBJECT(Command, NetworkObject, COMMAND_MSG_LIST)

DEFINE_NET_INDICES_EX(CreateCommand, NetworkObject, CREATE_COMMAND_MSG)

#define UPDATE_COMMAND_MSG(MessageName, XX) \
  XX(MessageName, Vector3, destination, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Destination position"), TRANSF, ET_ABS_DIF, 1)

DECLARE_NET_INDICES_EX_ERR(UpdateCommand, NetworkObject, UPDATE_COMMAND_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateCommand, NetworkObject, UPDATE_COMMAND_MSG, NoErrorInitialFunc)

NetworkMessageFormat &Command::CreateFormat(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  switch (cls)
  {
  case NMCCreate:
    NetworkObject::CreateFormat(cls, format);
    CREATE_COMMAND_MSG(CreateCommand, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    NetworkObject::CreateFormat(cls, format);
    UPDATE_COMMAND_MSG(UpdateCommand, MSG_FORMAT_ERR)
    break;
  default:
    NetworkObject::CreateFormat(cls, format);
    break;
  }
  return format;
}

TMError Command::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(CreateCommand)

      TRANSF_ENUM(message)
      TRANSF_REF(target)
      if (ctx.IsSending())
      {
        OLink(EntityAI) target;
        if (_targetE) target = _targetE->idExact;
        TRANSF_REF_EX(targetE, target)
      }
      else
      {
        // see CreateObject
      }
      TRANSF(destination)
      TRANSF(time)
      TRANSF_REF_EX(join, _joinToSubgroup)
      TRANSF(intParam)
      TRANSF(strParam)
      TRANSF_ENUM(discretion)
      TRANSF_ENUM(context)
      TRANSF(id)
      TRANSF(precision)
    }
    break;
  case NMCUpdateGeneric:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateCommand)

      TRANSF(destination)
    }
    break;
  default:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    break;
  }
  return TMOK;
}

float Command::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = 0;
  
  PREPARE_TRANSFER(UpdateCommand)
  ICALCERR_DIST(destination, 1)

  return error;
}

Command *Command::CreateObject(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(CreateCommand)

  OLinkPerm<AISubgroup> subgrp;
  if (TRANSF_REF_BASE(subgroup, subgrp) != TMOK) return NULL;
  if (!subgrp) return NULL;

  bool enqueued;
  if (TRANSF_BASE(enqueued, enqueued) != TMOK) return NULL;

  Command *cmd = new Command();
  cmd->TransferMsg(ctx);
  cmd->_targetE = NULL;
  OLink(EntityAI) target;
  if (TRANSF_REF_BASE(targetE, target) && target)
  {
    AIGroup *grp = subgrp ? subgrp->GetGroup() : NULL;
    if (grp && grp->Leader())
    {
      cmd->_targetE = grp->Leader()->FindTarget(target);
    }
  }

  subgrp->InsertCommand(cmd, enqueued);

  NetworkId objectId;
  if (TRANSF_BASE(objectCreator, objectId.creator) != TMOK) return NULL;
  if (TRANSF_BASE(objectId, objectId.id) != TMOK) return NULL;
  cmd->SetNetworkId(objectId);
  cmd->SetLocal(false);

  return cmd;
}

void Command::DestroyObject()
{
  RptF("Do not use Command::DestroyObject");
  RptF("Command %s, id %d:%d", (const char *)FindEnumName(_message), _networkId.creator.CreatorInt(), _networkId.id);
}

Vector3 Command::GetCurrentPosition() const
{
  // command has no exact position, but we can find some spot it is related to
	if (_target) return _target->FutureVisualState().Position();
	if (_targetE) return _targetE->AimingPosition();
	return _destination;
}

RString Command::GetDebugName() const
{
  return RString("Command ")+FindEnumName(_message);
}

// GetEnumNames
static const EnumName CommandMessageNames[]=
{
  EnumName(Command::NoCommand,"NO CMD"),
  EnumName(Command::Wait,"WAIT"),
  EnumName(Command::Attack,"ATTACK"),
  EnumName(Command::Hide,"HIDE"),
  EnumName(Command::Move,"MOVE"),
  EnumName(Command::Heal,"HEAL"),
  EnumName(Command::Repair,"REPAIR"),
  EnumName(Command::Refuel,"REFUEL"),
  EnumName(Command::Rearm,"REARM"),
  EnumName(Command::Support,"SUPPORT"),
  EnumName(Command::Join,"JOIN"),
  EnumName(Command::GetIn,"GET IN"),
  EnumName(Command::Fire,"FIRE"),
  EnumName(Command::FireAtPosition,"FIRE AT POSITION"),
  EnumName(Command::GetOut,"GET OUT"),
  EnumName(Command::Stop,"STOP"),
  EnumName(Command::Expect,"EXPECT"),
  EnumName(Command::Action,"ACTION"),
  EnumName(Command::Scripted,"SCRIPTED"),
  EnumName(Command::Dismiss,"DISMISS"),
  EnumName(Command::HealSoldier,"HEAL SOLDIER"),
  EnumName(Command::FirstAid,"FIRST AID"),
  EnumName(Command::AttackAndFire,"ATTACK AND FIRE"),
  EnumName(Command::RepairVehicle,"REPAIR VEHICLE"),
  EnumName()
};
template<>
const EnumName *GetEnumNames( Command::Message dummy )
{
  return CommandMessageNames;
}

static const EnumName CommandDiscretionNames[]=
{
  EnumName(Command::Undefined, "UNDEF"),
  EnumName(Command::Major, "MAJOR"),
  EnumName(Command::Normal, "NORMAL"),
  EnumName(Command::Normal, "NORMAL NO HIDE"), // compatibility - NormalNoHide no longer exists
  EnumName(Command::Minor, "MINOR"),
  EnumName(Command::MinorHidden, "MINOR HIDDEN"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(Command::Discretion dummy)
{
  return CommandDiscretionNames;
}

static const EnumName CommandMovementNames[]=
{
  EnumName(Command::MoveUndefined, "UNDEF"),
  EnumName(Command::MoveFast, "FAST"),
  EnumName(Command::MoveFastIntoCover, "FAST COVER"),
  EnumName(Command::MoveCautios, "CAUTIOS"),
  EnumName(Command::MoveInDanger, "IN DANGER"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(Command::Movement dummy)
{
  return CommandDiscretionNames;
}

static const EnumName CommandContextNames[]=
{
  EnumName(Command::CtxUndefined, "UNDEF"),
  EnumName(Command::CtxAuto, "AUTO"),
  EnumName(Command::CtxAutoSilent, "AUTOSILENT"),
  EnumName(Command::CtxJoin, "JOIN"),
  EnumName(Command::CtxAutoJoin, "AUTOJOIN"),
  EnumName(Command::CtxEscape, "ESCAPE"),
  EnumName(Command::CtxMission, "MISSION"),
  EnumName(Command::CtxUI, "UI"),
  EnumName(Command::CtxUIWithJoin, "UIJOIN"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(Command::Context dummy)
{
  return CommandContextNames;
}

///////////////////////////////////////////////////////////////////////////////
// class AISubgroup

// Construction and destruction

AISubgroup::AISubgroup()
{
  //_formation = FormColumn;
  //_formation = FormStaggeredColumn;
  _formation = FormWedge;
  //_formation = FormEcholonLeft;
  //_formation = FormEcholonRight;
  //_formation = FormVee;
  //_formation = FormLine;
  
  _speedMode = SpeedNormal;

  _formationCoef = 1.0;
  _formationCoefChanged = Glob.time - 120.0f;
  
  _directionMove = _direction = Vector3(0, 0, 1);
  
  _directionChanged = TIME_MIN;

  _mode = Wait;
  _refreshTime = TIME_MAX;
  
  _lastPrec = LevelOperative;

  _avoidRefresh = false;
  _doRefresh = false;

  _wantedPosition = PlanPosition(VUndefined,false);

/*
ErrF("Subgroup 0x%x created", this);
*/
}

AISubgroup::~AISubgroup()
{
/*
  NetworkId id = GetNetworkId();
  if (!id.IsNull())
  {
    if (GWorld->IsSimulationEnabled())
      RptF("Simulation enabled");
    else
      RptF("Simulation not enabled");
    RptF("Game mode %s", (const char *)FindEnumName(GWorld->GetMode()));
    RptF("Server state %s", (const char *)FindEnumName(GetNetworkManager().GetServerState()));
    RptF("Client state %s", (const char *)FindEnumName(GetNetworkManager().GetClientState()));
    ErrF("Subgroup %d:%d destroyed", id.creator, id.id);
  }
*/
}

static const EnumName ThinkImportanceNames[]=
{
  EnumName(AI::LevelOperative, "OPER"),
  EnumName(AI::LevelFastOperative, "FAST"),
  EnumName(AI::LevelStrategic, "STRAT"),
  EnumName(AI::LevelCommands, "COMM"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AI::ThinkImportance dummy)
{
  return ThinkImportanceNames;
}

static const EnumName SubgroupModeNames[]=
{
  EnumName(AISubgroup::Wait, "WAIT"),
  EnumName(AISubgroup::PlanAndGo, "PLAN AND GO"),
  EnumName(AISubgroup::DirectGo, "DIRECT GO"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AISubgroup::Mode dummy)
{
  return SubgroupModeNames;
}

LSError AISubgroup::Serialize(ParamArchive &ar)
{
  base::Serialize(ar);

  if (ar.GetArVersion() < 15)
  {
    // units was stored here
    CHECK(ar.Serialize("Units", _units, 1))
  }
  else
  {
    // now, they are stored in Person
    CHECK(ar.SerializeRefs("Units", _units, 1))
  }
  if (ar.IsLoading() && ar.GetPass()==ParamArchive::PassSecond)
  { // there can be NULLs in MP Saves (Person with no brain, but subgroup not altered yet)
    _units.RemoveNulls();
  }
  CHECK(ar.SerializeRef("Leader", _whoAmI, 1))
  CHECK(ar.SerializeRef("Group", _group, 1))

  CHECK(ar.SerializeEnum("mode", _mode, 1, PlanAndGo))

  CHECK(::Serialize(ar, "wantedPosition", safe_cast<Vector3 &>(_wantedPosition), 1, VUndefined))
  CHECK(::Serialize(ar, "wantedPositionCover", _wantedPosition._cover, 1, false))

  CHECK(::Serialize(ar, "refreshTime", _refreshTime, 1))

  // strategic plan
  if (ar.GetArVersion() < 8)
  {
    AIUnit *leader = Leader();
    if (leader)
    {
#if _ENABLE_AI
      CHECK(ar.Serialize("Planner", *leader->_planner, 1))
      CHECK(ar.Serialize("completedTime", leader->_completedTime, 1)) // not used - why ??
      CHECK(::Serialize(ar, "waitWithPlan", leader->_waitWithPlan, 1))
      CHECK(ar.Serialize("attemptPlan", leader->_attemptPlan, 1, 0))
#endif

      CHECK(ar.Serialize("lastPlan", leader->_lastPlan, 1, false))
      CHECK(ar.Serialize("noPath", leader->_noPath, 1, false))
      CHECK(ar.Serialize("updatePath", leader->_updatePath, 1, false))

      CHECK(ar.Serialize("exposureChange", leader->_exposureChange, 1, 0))
    }
  }
  
  // operative control
  CHECK(ar.SerializeEnum("formation", _formation, 1, FormVee))
  
  CHECK(ar.SerializeEnum("speedMode", _speedMode, 1, SpeedNormal))

  // flags
  CHECK(ar.SerializeEnum("lastPrec", _lastPrec, 1, LevelOperative))
  
  CHECK(ar.Serialize("avoidRefresh", _avoidRefresh, 1, false))

  CHECK(ar.Serialize("formationCoef", _formationCoef, 1, 1.0))
  CHECK(::Serialize(ar, "formationCoefChanged", _formationCoefChanged, 1))

  CHECK(::Serialize(ar, "direction", _direction, 1))
  CHECK(::Serialize(ar, "directionMove", _directionMove, 1, _direction))
  CHECK(::Serialize(ar, "directionChanged", _directionChanged, 1))

  //serialize NetworkObject
  NetworkObject::Serialize(ar); //base

  if ( ar.IsLoading() && (ar.GetPass()==ParamArchive::PassSecond) 
    && !GetNetworkId().IsNull() )
  {
    //Calling CreateObject is not sufficient
    //GetNetworkManager().CreateObject(this, false /*not setNetworkId*/);
    // we should call appropriate CreateXXX methods when everything is serialized (e.g. subobjects,...)
    // store the "action" to see, what should be created "at least"
    GetNetworkManager().RememberSerializedObject(this);
  }

  return LSOK;
}

void AISubgroup::CreateCommandsFromLoad()
{
  for (int i=0; i<_stack.Size(); i++)
  {
    if (_stack[i]._task && !_stack[i]._task->GetNetworkId().IsNull())
      GetNetworkManager().CreateCommand(this, _stack[i]._task, false, false);
  }
}

AISubgroup *AISubgroup::LoadRef(ParamArchive &ar)
{
  TargetSide side = TSideUnknown;
  int idGroup;
  int index;
  if (ar.SerializeEnum("side", side, 1) != LSOK) return NULL;
  if (ar.Serialize("idGroup", idGroup, 1) != LSOK) return NULL;
  if (ar.Serialize("index", index, 1) != LSOK) return NULL;
  AICenter *center = GWorld->GetCenter(side);
  if (!center) return NULL;
  AIGroup *group = NULL;
  for (int i=0; i<center->NGroups(); i++)
  {
    AIGroup *grp = center->GetGroup(i);
    if (grp && grp->ID() == idGroup)
    {
      group = grp;
      break; 
    }
  }
  if (!group) return NULL;
  return group->GetSubgroup(index);
}

LSError AISubgroup::SaveRef(ParamArchive &ar) const
{
  AIGroup *grp = GetGroup();
  AICenter *center = grp ? grp->GetCenter() : NULL;
  TargetSide side = center ? center->GetSide() : TSideUnknown;
  int idGroup = grp ? grp->ID() : -1;
  int index = -1;
  if (grp) for (int i=0; i<grp->NSubgroups(); i++)
  {
    if (grp->GetSubgroup(i) == this)
    {
      index = i;
      break;
    }
  }
  CHECK(ar.SerializeEnum("side", side, 1))
  CHECK(ar.Serialize("idGroup", idGroup, 1))
  CHECK(ar.Serialize("index", index, 1))
  return LSOK;
}

#define AI_SUBGROUP_MSG_LIST(XX) \
  XX(Create, CreateAISubgroup) \
  XX(UpdateGeneric, UpdateAISubgroup)

DEFINE_NETWORK_OBJECT(AISubgroup, NetworkObject, AI_SUBGROUP_MSG_LIST)

DEFINE_NET_INDICES_EX(CreateAISubgroup, NetworkObject, CREATE_AI_SUBGROUP_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateAISubgroup, NetworkObject, UPDATE_AI_SUBGROUP_MSG, NoErrorInitialFunc)

NetworkMessageFormat &AISubgroup::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  switch (cls)
  {
  case NMCCreate:
    NetworkObject::CreateFormat(cls, format);
    CREATE_AI_SUBGROUP_MSG(CreateAISubgroup, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    NetworkObject::CreateFormat(cls, format);
    UPDATE_AI_SUBGROUP_MSG(UpdateAISubgroup, MSG_FORMAT_ERR)
    break;
  default:
    NetworkObject::CreateFormat(cls, format);
    break;
  }
  return format;
}

AISubgroup *AISubgroup::CreateObject(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(CreateAISubgroup)

  OLinkPerm<AIGroup> grp;
  if (TRANSF_REF_BASE(group, grp) != TMOK) return NULL;
  if (!grp) return NULL;
  AISubgroup *subgrp = new AISubgroup();
  grp->AddSubgroup(subgrp);

  NetworkId objectId;
  if (TRANSF_BASE(objectCreator, objectId.creator) != TMOK) return NULL;
  if (TRANSF_BASE(objectId, objectId.id) != TMOK) return NULL;
  subgrp->SetNetworkId(objectId);
  subgrp->SetLocal(false);

// RptF("Remote subgroup %d:%d created", objectId.creator, objectId.id);

  return subgrp;
}

void AISubgroup::DestroyObject()
{
  if (!_group)
  {
    DoAssert(NUnits() == 0);
    return;
  }

  AISubgroup *main = _group->MainSubgroup();
  if (this == main) return;
  if (main)
  {
    while (_units.Size() > 0)
    {
      AIUnit *unit = _units[0];
      if (unit)
      {
        main->AddUnit(unit);
      }
      else
        _units.Delete(0);
    }
    if (!main->Leader()) main->SelectLeader();
  }

  AIGroup *group = _group;
  if (group)
  {
    _group = NULL;
    group->SubgroupRemoved(this);
  }
}

TMError AISubgroup::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    if (ctx.IsSending())
    {
      PREPARE_TRANSFER(CreateAISubgroup)

      TRANSF_REF(group)
    }
    break;
  case NMCUpdateGeneric:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateAISubgroup)

      if (ctx.IsSending())
      {
        TRANSF_REF(group) // used only for owner changes
        TRANSF_REFS(units)
        TRANSF_REF_EX(leader, _whoAmI)
      }
      else
      {
        RefArray<AIUnit> units;
        TRANSF_REFS_EX(units, units)
        for (int i=0; i<units.Size(); i++)
        {
          // add new units, do not remove old units
          AIUnit *unit = units[i];
          if (!unit) continue;
          bool found = false;
          for (int j=0; j<_units.Size(); j++)
            if (_units[j] == unit)
            {
              found = true;
              break;
            }
          if (found) 
          { // after JIP the unit->GetGroup()==GetGroup() can hold in spite of group does not contain the unit yet
            AIGroup *group = GetGroup();
            found = false;
            for (int i=0; i<group->NUnits(); i++)
            {
              if (group->GetUnit(i) == unit) {found = true; break;}
            }
            if (!found)
            { // unit is not added into the group units yet, add it
              unit->AddRef();
              // note: there will be possible warning such as 
              //    Wrong unit index 0 (unit is not in the slot), but we need to reset the units 'group' ID
              unit->ForceRemoveFromGroup();
              group->AddUnit(unit, -1);
              unit->Release();
            }
            continue; // <- Subgroup is up to date
          }

          // add unit
          DoAssert(GetGroup());
          if (unit->GetGroup() != GetGroup())
          {
            // group changed
            Ref<AISubgroup> oldSubgroup = unit->GetSubgroup();
            Ref<AIGroup> oldGroup = unit->GetGroup();
            bool bSubgroupLeader = oldSubgroup && oldSubgroup->Leader() == unit;
            bool bGroupLeader = oldGroup && oldGroup->Leader() == unit;
            unit->AddRef();
            unit->ForceRemoveFromGroup();
            GetGroup()->AddUnit(unit, -1);
            unit->Release();
            if (bGroupLeader) oldGroup->GetCenter()->SelectLeader(oldGroup);
            if (bSubgroupLeader) oldSubgroup->SelectLeader();
          }
          if (unit->GetSubgroup() != this)
          {
            // subgroup changed
            Ref<AISubgroup> oldSubgroup = unit->GetSubgroup();;
            bool bSubgroupLeader = oldSubgroup && oldSubgroup->Leader() == unit;
            AddUnit(unit);
            if (bSubgroupLeader) oldSubgroup->SelectLeader();
          }
        }
        OLinkPermNO(AIUnit) leader;
        TRANSF_REF_EX(leader, leader)
        if (leader != _whoAmI)
        {
          if (!leader)
          {
            LogF("Warning: no subgroup leader");
          }
          else if (leader->GetSubgroup() != this)
          {
            LogF("Warning: leader not from this subgroup");
          }
          else
            _whoAmI = leader;
        }
        if (!GetGroup()->Leader())
        {
          GetGroup()->GetCenter()->SelectLeader(GetGroup());
          if (GetGroup()->Leader())
            LogF("Warning: group leader mising - selected %s", (const char *)GetGroup()->Leader()->GetDebugName());
        }
      }
      TRANSF_ENUM(mode)
      TRANSF(wantedPosition)
      TRANSF_ENUM(formation)
      TRANSF_ENUM(speedMode)
      TRANSF_ENUM(lastPrec)
      TRANSF(formationCoef)
      TRANSF(direction)
    }
    break;
  default:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    break;
  }
  return TMOK;
}

float AISubgroup::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = NetworkObject::CalculateError(ctx);

  PREPARE_TRANSFER(UpdateAISubgroup)

/*
  RefArray<AIUnit> units;
  if (TRANSF_REFS_BASE(units, units) == TMOK)
    for (int i=0; i<units.Size(); i++)
    {
      AIUnit *unit = units[i];
      if (!unit) continue;
      bool found = false;
      for (int j=0; j<_units.Size(); j++)
        if (_units[j] == unit)
        {
          found = true;
          break;
        }
      if (!found) error += ERR_COEF_STRUCTURE;
    }
*/
  for (int i=0; i<message->_units.Size(); i++)
  {
    NetworkId id = message->_units[i];
    if (id.IsNull()) continue;
    bool found = false;
    for (int j=0; j<_units.Size(); j++)
      if (_units[j] && _units[j]->GetNetworkId() == id)
      {
        found = true;
        break;
      }
    if (!found) error += ERR_COEF_STRUCTURE;
  }

  ICALCERRE_NEQREF_PERM(AIUnit, leader, _whoAmI, ERR_COEF_STRUCTURE)
  ICALCERR_NEQ(int, mode, ERR_COEF_MODE)
  ICALCERR_NEQ(Vector3, wantedPosition, ERR_COEF_MODE)
  ICALCERR_NEQ(int, formation, ERR_COEF_MODE)
  ICALCERR_NEQ(int, speedMode, ERR_COEF_MODE)
  // TODO: Not implemented
  return error;
}

void AISubgroup::OnTaskCreated(Command &cmd, bool enqueued)
{
  if (GWorld->GetMode() == GModeNetware && IsLocal())
  {
    AIGroup *grp = GetGroup();
    // if group contains any player, we want to create a command on remote computers
    // this is in case some player will respawn into it
    //if (grp && grp->ContainsAnyPlayer())
    if (grp && grp->ContainsRemotePlayer())
    {
      GetNetworkManager().CreateCommand(this, &cmd, enqueued);
    }
  }
}

void AISubgroup::OnTaskDeleted(Command &cmd)
{
  // delete command from network only if it was network object
  if (GWorld->GetMode() == GModeNetware && cmd.IsLocal() && !cmd.GetNetworkId().IsNull())
  {
    GetNetworkManager().DeleteCommand(this, &cmd);
  }
}

void AISubgroup::InsertCommand(Command *cmd, bool enqueued)
{
  if (!enqueued || _stack.Size() == 0)
  {
    // avoid processing of command - can be deleted
    int index = _stack.Add();
    _stack[index]._task = cmd;
    _stack[index]._fsm = CreateFSM(cmd);
    _stack[index]._fsm->SetState(0); // State 0 is starting state
  }
  else
  {
    _stack.Insert(0);
    _stack[0]._task = cmd;
    _stack[0]._fsm = CreateFSM(cmd);
    _stack[0]._fsm->SetState(0); // State 0 is starting state
  }
}

void AISubgroup::DeleteCommand(NetworkId id)
{
  int n = _stack.Size();
  for (int i=0; i<n; i++)
  {
    if (_stack[i]._task->GetNetworkId() == id)
    {
      _stack.Delete(i);
      if (i == n - 1)
      {
        if (GetCurrent())
        {
          AISubgroupContext context(this);
          context._task = GetCurrent()->_task;
          context._fsm = GetCurrent()->_fsm;
          PopTask(&context, false);
        }
      }
      else
      {
        RptF("Warning: Delete out of order");
      }
      break;
    }
  }
}

AIUnit *AISubgroup::Commander() const
{
  AIUnit *leader = Leader();
  if (!leader) return NULL;
  Transport *veh = leader->GetVehicleIn();
  if (veh) return veh->CommanderUnit() ? veh->CommanderUnit()->GetUnit() : NULL;
  else return leader;
}

void AISubgroup::SetDirection(Vector3Val dir)
{
  _direction = dir;
  _directionChanged = Glob.time;
}

// Changes of structure
void AISubgroup::UnitRemoved(AIUnit *unit)
{
  int index = _units.Find(unit);
  Assert(index >= 0);
  if (index < 0)
    return;

  bool bLeader = (unit == Leader());

  _units.Delete(index);
  if (NUnits() == 0)
  {
    _whoAmI = NULL;
    return;
  }

  if (bLeader)
    SelectLeader();

  RefreshPlan();

  Assert(Leader());
}

void AISubgroup::UnitReplaced(AIUnit *unitOld, AIUnit* unitNew)
{
  Assert(unitNew);

  int index = _units.Find(unitOld);
  Assert(index >= 0);
  if (index < 0)
    return;

  // replace all occurences
  if (_whoAmI == unitOld) _whoAmI = unitNew;
  if (GetGroup()->_leader == unitOld) GetGroup()->_leader = unitNew;
  _units[index] = unitNew;
  
  int grpIndex = unitOld->ID() - 1;
  Assert(unitNew->ID() - 1 == grpIndex);
  Assert(GetGroup()->_unitSlots[grpIndex]._unit == unitOld);
  GetGroup()->_unitSlots[grpIndex]._unit = unitNew;

  // maintain pointers back to subgroup
  unitOld->_subgroup = NULL;
  unitNew->_subgroup = this;
}

void AISubgroup::AddUnit(AIUnit *unit)
{
  if (!this)
  {
    Fail("No subgroup");
    return;
  }

  AIGroup *grp = GetGroup();
  if (!grp)
  {
    Fail("No group");
    return;
  }

  if (unit->GetSubgroup())
  {
    if (!unit->GetGroup())
    {
      Fail("Subgroup with no group");
      return;
    }
    if (unit->GetGroup() != grp)
    {
      Fail("Adds unit from another group");
      RptF
      (
        "From %s to %s",
        (const char *)unit->GetGroup()->GetDebugName(),
        (const char *)grp->GetDebugName()
      );
      return;
    }
  }
  DoAssert(unit->GetSubgroup() != this);

  unit->AddRef();
  if (unit->GetSubgroup())
    unit->RemoveFromSubgroup();
  int index = _units.Find(NULL);
  if (index >= 0) _units[index] = unit;
  else _units.Add(unit);
  unit->Release();

  unit->_subgroup = this;
  unit->_formOffset = VZero;

  // new unit is not leader unless selected by SelectLeader
  EntityAIFull *vehicle = unit->GetVehicle();
  if (vehicle) vehicle->SwitchToFormation(); // Hotfix: crash

  unit->SetWantedPosition(_wantedPosition, GetCommandPrecision(), AIUnit::DoNotPlan, true);
  // path is no longer valid 
  unit->GetPath().SetSearchTime(Glob.time-60);

  if (grp && grp->MainSubgroup() == this && !grp->IsAnyPlayerGroup())
  {
    OLinkPermNOArray(AIUnit) list;
    list.Realloc(1);
    list.Resize(1);
    list[0] = unit;
    grp->SendSemaphore(grp->_semaphore, list);
  }
}

void AISubgroup::AddUnitWithCargo(AIUnit *unit)
{
  if (!unit->IsUnit()) return;

  DoAssert( AssertValid() );
    
  Transport *veh = unit->GetVehicleIn();
  if (veh)
  {
    AIGroup *grp = unit->GetGroup();
    AIBrain *agent = veh->ObserverBrain();
    AIUnit *u = agent ? agent->GetUnit() : NULL;
    if (u && u->GetGroup() == grp && u->GetSubgroup() != this)
      AddUnit(u);
    agent = veh->DriverBrain();
    u = agent ? agent->GetUnit() : NULL;
    if (u && u->GetGroup() == grp && u->GetSubgroup() != this)
      AddUnit(u);
    agent = veh->GunnerBrain();
    u = agent ? agent->GetUnit() : NULL;
    if (u && u->GetGroup() == grp && u->GetSubgroup() != this)
      AddUnit(u);
    const ManCargo &cargo = veh->GetManCargo();
    for (int i=0; i<cargo.Size(); i++)
    {
      Person *man = cargo[i];
      if (!man) continue;
      agent = man->Brain();
      u = agent ? agent->GetUnit() : NULL;
      if (u && u->GetGroup() == grp && u->GetSubgroup() != this)
        AddUnit(u);
    }
  }
  else
    AddUnit(unit);

  SelectLeader();
  RefreshPlan();

  DoAssert( AssertValid() );
}

void AISubgroup::SetLeader(AIUnit *unit)
{
  Assert(unit);

  if (unit == _whoAmI) return;
  float commPrec = GetCommandPrecision();
  if (_whoAmI)
  {
    // reset old leader
    if (_whoAmI->GetVehicle()) _whoAmI->GetVehicle()->SwitchToFormation();
    _whoAmI->SetWantedPosition(_wantedPosition, commPrec, AIUnit::DoNotPlan, true);
  }
  unit->GetVehicle()->SwitchToLeader();
  _whoAmI = unit;

  if (_whoAmI->HasAI())
  {
    switch (_mode)
    {
    case Wait:
      _whoAmI->SetWantedPosition(_wantedPosition, commPrec, AIUnit::DoNotPlan);
      break;
    case PlanAndGo:
      _whoAmI->SetWantedPosition(_wantedPosition, commPrec, AIUnit::LeaderPlanned);
      break;
    case DirectGo:
      _whoAmI->SetWantedPosition(_wantedPosition, commPrec, AIUnit::LeaderDirect);
      break;
    }
  }
  else
    DoNotGo();

  // DoRefresh();
  _doRefresh = true; // process DoRefresh in next Think

  AIBrain *playerAgent = GWorld->FocusOn();
  AIUnit *player = playerAgent ? playerAgent->GetUnit() : NULL;
  if (player && player->GetSubgroup() == this && player != unit) GWorld->UI()->ShowFollowMe();

  DoAssert(AssertValid());
}

void AISubgroup::SelectLeader(AIUnit *unit)
{
  if (NUnits() == 0)
  {
    // empty subgroup
    _whoAmI = NULL;
    return;
  }

  int nUnits = 0;
  for (int i=0; i<NUnits(); i++)
  {
    AIUnit *u = GetUnit(i);
    if (u && u->GetPerson() && u->IsUnit()) nUnits++;
  }

  if (nUnits > 0 && unit && (!unit->GetPerson() || !unit->IsUnit()))
    unit = NULL;

  AIGroup *grp = GetGroup();
  AIUnit *grpLeader = grp ? grp->Leader() : NULL;
  bool leaderSelected = false;
  if (nUnits > 0 && grpLeader && grpLeader->GetSubgroup() == this)
  {
    // try to select leader's vehicle
    Transport *veh = grpLeader->GetVehicleIn();
    if (veh)
    {
      AIUnit *candidate = veh->CommanderUnit() ? veh->CommanderUnit()->GetUnit() : NULL;
      if (candidate && candidate->GetSubgroup() == this)
      {
        unit = candidate;
        leaderSelected = true;
      }
    }
    else
    {
      unit = grpLeader;
      leaderSelected = true;
    }
  }
  if (!leaderSelected)
  {
    Rank bestRank = RankPrivate;
    float bestExp = -FLT_MAX;
    for (int i=0; i<NUnits(); i++)
    {
      AIUnit *u = GetUnit(i);
      if (!u || !u->GetPerson()) continue;
      if (nUnits > 0 && !u->IsUnit()) continue;

      Rank rank = u->GetPerson()->GetRank();
      if (rank > bestRank)
      {
        bestRank = rank;
        bestExp = u->GetPerson()->GetExperience();
        unit = u;
      }
      else if (rank == bestRank)
      {
        float exp = u->GetPerson()->GetExperience();
        if (exp > bestExp)
        {
          bestExp = exp;
          unit = u;
        }
      }
    }
  }
  // fix: no unit found
  if (!unit)
  {
    RptF("Subgroup leader candidate did not found, NUnits() = %d, nUnits = %d", NUnits(), nUnits);
    _whoAmI = NULL;
    return;
  }
  SetLeader(unit);
}

void AISubgroup::RemoveFromGroup()
{
  if (!IsLocal()) return;

  AIGroup *group = _group;
  if (group)
  {
    _group = NULL;
    group->SubgroupRemoved(this);
  }
}

void AISubgroup::JoinToSubgroup(AISubgroup *subgrp)
{
  if (!subgrp)
  {
    Fail("Join to empty subgroup.");
    return;
  }
  if (subgrp == this)
  {
    Fail("Join with themselves.");
    return;
  }
  Assert(subgrp->GetGroup() == GetGroup());
  if (subgrp->GetGroup() != GetGroup())
  {
    Fail("Join to another group");
    return;
  }
  Assert(this != GetGroup()->MainSubgroup());

  DoAssert(AssertValid());

  ClearAllCommands();

  while (_units.Size() > 0)
  {
    Ref<AIUnit> unit = _units[0];
    _units.Delete(0);
    if (!unit)
    {
      Fail("No unit");
      continue;
    }
    if (unit->GetSubgroup() != this)
    {
      Fail("Bad structure");
      // TODO: ?? remove from unit->GetSubgroup()
    }
    // remove explicitly - avoid refresh of destroyed subgroup
    unit->_subgroup = NULL;
    subgrp->AddUnit(unit);
  }
  RemoveFromGroup();

  subgrp->SelectLeader();
  subgrp->RefreshPlan();
  subgrp->UpdateFormationPos();
  
  DoAssert(subgrp->AssertValid());
}

// Mind
AI::ThinkImportance AISubgroup::CalculateImportance()
{
  return LevelOperative;
}

bool AISubgroup::Think(ThinkImportance prec)
{
  PROFILE_SCOPE_EX(aiSub, ai);
#if LOG_THINK
    Log("  Subgroup %s think.", (const char *)GetDebugName());
#endif

  if (!IsLocal())
  {
    for (int u=0; u<NUnits(); u++)
    {
      AIUnit *unit = GetUnit(u);
      if (!unit || !unit->GetPerson())
        continue;
      unit->Think(prec);
    }
    return false;
  }
  else
  {
    for (int u=0; u<NUnits(); u++)
    {
      AIUnit *unit = GetUnit(u);
      if (!unit)
        continue;
      if (!unit->GetPerson())
      { // unit is not valid, better to Remove it completely (it would crash otherwise, see news:k574g4$n33$1@new-server.localdomain)
        if (unit->GetGroup()) 
        {
          unit->ForceRemoveFromGroup();
          if (u<NUnits() && GetUnit(u)!=unit) u--; // unit under index 'u' was deleted and _units array is shifted. Process index 'u' again
        }
        else _units[u] = NULL;
        continue;
      }
    }
  }
  int u;
  if ((_lastPrec == LevelCommands && prec != LevelCommands)
    || (_lastPrec != LevelCommands && prec == LevelCommands))
      if (Leader()) Leader()->ClearStrategicPlan();
  if (_lastPrec != prec && 
    (_lastPrec <= LevelFastOperative || prec <= LevelFastOperative))
  {
#if !_RELEASE
  Log("Subgroup %s: Precision changed from %d to %d",
  (const char *)GetDebugName(), _lastPrec, prec);
#endif
    for (u=0; u<NUnits(); u++)
    {
      AIUnit *unit = GetUnit(u);
      if (!unit)
        continue;
      AIUnit::State state = unit->GetState();
      if
      (
        state != AIUnit::Delay &&
        state != AIUnit::InCargo &&
        state != AIUnit::Stopping &&
        state != AIUnit::Stopped
      )
        Verify(unit->SetState(AIUnit::Wait));
    }
  }
  _lastPrec = prec;
  
/*
SectionTimeHandle sectionTime = StartSectionTime();
*/

  AddRef();
  AISubgroupContext context(this);
  if (_doRefresh && !_avoidRefresh)
  {
    UpdateAndRefresh(&context);
    _doRefresh = false;
  }
  else
    Update(&context);
  bool destroyed = RefCounter() <= 1;
  Release();

/*
float time = GetSectionTime(sectionTime); 
if (time > 0.01)
{
  RptF("!!! %s update takes %.0f ms", (const char *)GetDebugName(), 1000.0f * time);
  RptF("  - command %s, state %s", (const char *)FindEnumName(context._task->_message), context._fsm->GetStateName());
}
*/
  
  if (destroyed) return false;
  if (NUnits() <= 0)
    return false;
  Assert(GetGroup());
  Assert(Leader());
  
  // BUG:
  if (!Leader()) SelectLeader();
  
  if (!HasCommand() && !(Leader() && Leader()->GetState() == AIUnit::Stopping))
  {
    DoNotGo();
  }

  if (Leader() && Leader()->IsUnit())
  {
    const float updateCoefInterval = 2.0;
    if (Glob.time >= _formationCoefChanged+updateCoefInterval )
    {
      // TODO: Another placements - SetFormation, AddUnit, RemoveUnit, SelectLeader, GetIn, GetOut, ?...
      UpdateFormationPos();

      UpdateFormationDirection();

      UpdateFormationCoef();
  #if LOG_FORMATION_COEF 
  GlobalShowMessage(1000, "%.3f", _formationCoef);
  #endif
    }

  // Refresh command
    if
    (
      Leader() && 
      Leader()->GetExposureChange() >= CRITICAL_EXPOSURE_CHANGE ||
      Glob.time >= _refreshTime 
    )
    {
      if (GetCurrent())
      {
        AISubgroupContext context(this);
        context._fsm = GetCurrent()->_fsm;
        context._task = GetCurrent()->_task;
        GetCurrent()->_fsm->Refresh(&context);
      }

      Leader()->ClearExposureChange();

      #if 0
      // force subgroup leader to replan if necessary
      AIUnit *unit = Leader();
      if( unit && unit->IsUnit() )
      {
        Transport *transport = unit->GetVehicleIn();
        if( transport && !transport->GetType()->HasObserver() )
        {
          // TODO: force leader
        }
      }
      #endif
    }

  }

  bool path = false;
  for (u=0; u<NUnits(); u++)
  {
    AIUnit *unit = GetUnit(u);
    if (!unit)
      continue;
    if (!unit->GetPerson())
    { // unit is not valid, better to Remove it completely
      if (unit->GetGroup()) 
      {
        unit->ForceRemoveFromGroup();
        if (u<NUnits() && GetUnit(u)!=unit) u--; // unit under index 'u' was deleted and _units array is shifted. Process index 'u' again
      }
      else _units[u] = NULL;
      continue;
    }
    if (unit->Think(prec)) path = true;
  }

  return path; // OperPath called - return busy
}



// Communication with units
/*!
\patch 1.16 Date 8/10/2001 by Ondra.
- Fixed: occasional random crash after unit destroyed.
\patch 5111 Date 12/22/2006 by Jirka
- Fixed: Random DS crashes
*/

void AISubgroup::ReceiveAnswer(AIUnit* from, Answer answer)
{
  if (!from)
    return;

#if LOG_COMM
  Log("ReceiveAnswer: Subgroup %s: From %s: Answer %d", (const char *)GetDebugName(), (const char *)from->GetDebugName(), answer);
#endif
  switch (answer)
  {
    case AI::UnitDestroyed:
    #if _ENABLE_REPORT
    {
    AICenter *center = GetGroup()->GetCenter();
    DoAssert(center->AssertValid());
    #endif
    {
      Ref<AIGroup> group = GetGroup();  // group may be removed from center
      Assert(group);
      Assert(group->Leader());
      if (!group->Leader()) return;
      AICenter *center = group->GetCenter();
      Assert(center);
      center->DeleteTarget(from->GetVehicle());
      bool bGroupLeader = (from == group->Leader());
      if (!bGroupLeader)
      {
        // penalize leader for unit lost
        int i, n = ExperienceDestroyTable.Size();
        float base = 0;
        const VehicleType *type = from->GetVehicle()->GetType();
        float cost = type->GetCost();
        if (cost > ExperienceDestroyTable[n - 1].maxCost)
        {
          base = ExperienceDestroyTable[n - 1].exp;
        }
        else
          for (i=0; i<n-1; i++)
          {
            if (cost <= ExperienceDestroyTable[i].maxCost)
            {
              base = ExperienceDestroyTable[i].exp;
              break;
            }
          }
        Assert(base >= 0);

        group->Leader()->AddExp(ExperienceDestroyYourUnit * base);
      }
      // during AIUnit::RemoveFromGroup()
      // unit will be removed from subgroup
      // but subgroup should remain valid
      // TODO: need to be clarified
      // patch: hold temporary Ref<> to subgroup 
      Ref<AISubgroup> ref = this;
      bool bSubgroupLeader = from == Leader();
      {
        // add ref to ensure some valid reference exist
        Ref<AIUnit> unit = from;
        // remove unit from subgroup
        from->RemoveFromGroup();
        // remove unit from corresponding person
        // there are two placed where unit is destructed:
        // here and in AIGroup destructor
#if !_VBS3
        // dont delete the brain
        if (from->GetPerson()) from->GetPerson()->SetBrain(NULL);
#endif
      }
      if (NUnits() == 0)
      {
        if (group->UnitsCount() == 0)
        {
          group->SendAnswer(AI::GroupDestroyed);  // do nothing
          return;
        }
        else
        {
          // report if the only ref is the temporary
          // this would lead to crash without the patch
          #if _ENABLE_REPORT
          // TODO: remove Ref<AISubgroup> ref
          // we never received following error message
          // so the patch is not necessary
          if (ref->RefCounter()==1)
          {
            ErrF
            (
              "AISubgroup::ReceiveAnswer - no refs (%s)",
              (const char *)group->GetDebugName()
            );
          }
          #endif
          if (this != group->MainSubgroup())
          {
            RemoveFromGroup();
          }
          Assert(group->Leader());
          if (bGroupLeader && group->Leader())
          {
            group->Leader()->SendAnswer(AI::IsLeader);
          }
        }
      }
      else
      {
        Assert(group->Leader());
        if (bGroupLeader && group->Leader())
        {
          group->Leader()->SendAnswer(AI::IsLeader);
        }
        if (bSubgroupLeader) SelectLeader();
      }
      // group are not destroyed even when all units are destroyed
      // verify group is valid
      /*
      // TODO: following line is only hot-fix
      // bug should be fixed in some consistent manner
      DoVerify( group->AssertValid())
      */
    }
    #if _ENABLE_REPORT
    DoAssert(center->AssertValid());
    }
    #endif
    break;
    default:
    {
      AIGroup *group = GetGroup();  // group may be removed from center
      if (group)
      {
        // unit is alive - cancel MIA status
        group->SetReportedDown(from,false);
        group->SetReportBeforeTime(from,TIME_MAX);
        group->ReceiveUnitStatus(from,answer);
      }
      break;
    }
  }
}

void AISubgroup::ClearMissionCommands()
{
  // only one mission command can be in stack
#if !_RELEASE
  {
    int i, n=0;
    for (i=0; i<_stack.Size(); i++)
    {
      Command *cmd = _stack[i]._task;
      if (cmd->_context == Command::CtxMission)
        n++;
    }
    Assert(n <= 1);
  }
#endif

  int i, n=-1;
  for (i=0; i<_stack.Size(); i++)
  {
    Command *cmd = _stack[i]._task;
    if (cmd->_context == Command::CtxMission)
    {
      n = i;
      break;
    }
  }
  if (n < 0)
    return;

  AISubgroupContext context(this);
  base::Delete(n,&context);

  if (n == _stack.Size())
  {
    Stop();
  }
}

void AISubgroup::ClearAllCommands()
{
  AISubgroupContext context(this);
  base::Clear(&context);  // FSM - clear stack
  Stop();
}

void AISubgroup::ClearEscapeCommands()
{
  int i, n=-1;
  for (i=0; i<_stack.Size(); i++)
  {
    Command *cmd = _stack[i]._task;
    if (cmd->_context == Command::CtxEscape)
    {
      n = i;
      break;
    }
  }
  if (n < 0)
    return;

  AISubgroupContext context(this);
  base::Delete(n,&context);

  if (n == _stack.Size())
  {
    Stop();
  }
}

bool AISubgroup::CheckHide() const
{
  for ( int i=0; i<_stack.Size(); i++)
  {
    const Command *cmd = _stack[i]._task;
    if( cmd->_message==Command::Hide ) return true;
  }
  return false;
}

void AISubgroup::ClearAttackCommands()
{
  bool deletedLast = false;
  for (int i=0; i<_stack.Size();)
  {
    Command *cmd = _stack[i]._task;
    if (
      (cmd->_context == Command::CtxAuto || cmd->_context == Command::CtxAutoSilent) &&
      (cmd->_message == Command::Attack || cmd->_message == Command::AttackAndFire || cmd->_message==Command::Hide))
    {
      if (i == _stack.Size() - 1 ) deletedLast = true;
      AISubgroupContext context(this);
      base::Delete(i, &context, false); // do not refresh
    }
    else i++;
  }

  if (deletedLast) Stop();
}

void AISubgroup::ClearAttackCommands(Target *allowAttackTarget)
{
  int i;
  bool deletedLast = false;
  for (i=0; i<_stack.Size();)
  {
    Command *cmd = _stack[i]._task;
    if
    (
      (
        cmd->_context == Command::CtxAuto ||
        cmd->_context == Command::CtxAutoSilent
      ) &&
      (
        // if the attack command is related to this target, we want to keep it
        ( cmd->_message==Command::Attack || cmd->_message==Command::AttackAndFire ) &&
        allowAttackTarget && cmd->_targetE!=allowAttackTarget ||
        cmd->_message==Command::Hide
      )
    )
    {
      if( i==_stack.Size()-1 ) deletedLast = true;
      AISubgroupContext context(this);
      base::Delete(i, &context, false); // do not refresh
    }
    else i++;
  }

  if (deletedLast)
  {
    Stop();
  }
}

void AISubgroup::ClearGetInCommands()
{
  int i;
  bool deletedLast = false;
  for (i=0; i<_stack.Size();)
  {
    Command *cmd = _stack[i]._task;
    if
    (
      (
        cmd->_context == Command::CtxAuto ||
        cmd->_context == Command::CtxAutoSilent
      ) &&
      (
        cmd->_message == Command::GetIn
      )
    )
    {
      if( i==_stack.Size()-1 ) deletedLast = true;
      AISubgroupContext context(this);
      base::Delete(i, &context, false); // do not refresh
    }
    else i++;
  }

  if (deletedLast)
  {
    Stop();
  }
}

/*!
\patch 1.34 Date 12/03/2001 by Ondra.
- Fixed: Invalid memory access during "Return to formation"
command issued by the player.
*/

// Communication with group
void AISubgroup::ReceiveCommand(Command &cmd)
{
#if LOG_COMM
  char buffer[256];
  OLinkPermNOArray(AIUnit) list;
  GetUnitsList(list);
  CreateUnitsList(list, buffer, sizeof(buffer));
  Log("Receive command: Subgroup %s (%s): Command %d (context %d)",
  (const char *)GetDebugName(), buffer, cmd._message, cmd._context);
#endif

#if DIAG_WANTED_POSITION
AIBrain *player = GWorld->FocusOn();
if (player && player->GetUnit() && player->GetUnit()->GetSubgroup() == this)
{
  LogF(
    "Subgroup %s: received command %s to %.0f, %.0f",
    cc_cast(GetDebugName()), cc_cast(FindEnumName(cmd._message)),
    cmd._destination.X(), cmd._destination.Z());
}
#endif

  // when command received from the player, wake up the units frozen by the team switch
  AIGroup *group = GetGroup();
  AIUnit *groupLeader = group ? group->Leader() : NULL;
  if (groupLeader && groupLeader->IsAnyPlayer())
  {
    for (int i=0; i<_units.Size(); i++)
    {
      AIUnit *unit = _units[i];
      if (unit)
      {
        int state = unit->GetAIDisabled();
        unit->SetAIDisabled(state & ~AIBrain::DATeamSwitch);
      }
    }
  }

  DoAssert(cmd._id >= 0);
  SetDiscretion(cmd._discretion);

  if (cmd._direction.SquareSize()<=0.1f && Leader())
  {
    // if not set yet, we update the command direction based on current formation leader position
    cmd._direction = (cmd._destination - Leader()->Position(Leader()->GetFutureVisualState())).Normalized();
  }
  
  // note: it looks like AISubgroup may be destroyed during Clear(&context)
  SetCommandState(cmd._id, CSReceived, this);

  AISubgroupContext context(this);
  switch (cmd._context)
  {
    case Command::CtxMission:
      ClearMissionCommands();
      EnqueueTask(cmd, &context);
      break;
    case Command::CtxUI:
      Clear(&context);
      PushTask(cmd, &context);
      break;
    case Command::CtxEscape:
      // TODO: BUG: Command::operator = in PushTask called with this==4
      ClearEscapeCommands();
      PushTask(cmd, &context);
      break;
    case Command::CtxUIWithJoin:
      Clear(&context);
      // continue
    case Command::CtxAuto:
    case Command::CtxAutoSilent:
    case Command::CtxJoin:
    case Command::CtxAutoJoin:
      PushTask(cmd, &context);
      break;
    default:
      Fail("Context");
      return;
  }
}

class RemoveCommandConfirmMessage
{
protected:
  AIGroup *_group;
  AISubgroup *_subgroup;
  int _id;

public:
  RemoveCommandConfirmMessage(AIGroup *group, AISubgroup *subgroup, int id)
  {
    _group = group; _subgroup = subgroup; _id = id;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool RemoveCommandConfirmMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (actual) return false;
  if (msg->GetType() != RMTCommandConfirm) return false;

  Assert(dynamic_cast<RadioMessageCommandConfirm *>(msg));
  RadioMessageCommandConfirm *msgConfirm = static_cast<RadioMessageCommandConfirm *>(msg);
  Assert(msgConfirm->GetTo() == _group);
  if
  (
    msgConfirm->GetFrom() &&
    msgConfirm->GetFrom()->GetSubgroup() == _subgroup &&
    msgConfirm->GetCommand()._id == _id
  )
  {
    channel->Cancel(msg, _group);
    return true;
  }
  return false; // continue with the next message
}

void AISubgroup::SendAnswer(Answer answer)
{
#if LOG_COMM
  Log("Send answer: Subgroup %s: answer %d",
  (const char *)GetDebugName(), answer);
#endif

  if (_group)
  {
    if (Leader() == _group->Leader())
      _group->ReceiveAnswer(this, answer);
    else
    {
      bool display = false;
      int id = -1;
      Command *cmd = GetCommand();
      if (cmd)
      {
        display =
          cmd->_context != Command::CtxUndefined &&
          cmd->_context != Command::CtxAutoSilent &&
          cmd->_context != Command::CtxJoin;
        id = cmd->_id;
      }

      // active channel selection
      RadioChannel *radioChannel = &_group->GetRadio();
      RadioChannel *activeChannel = radioChannel;
#if _ENABLE_DIRECT_MESSAGES
      AIUnit *leader = _group->Leader();
      AIUnit *unit = Leader();
      RadioChannel *directChannel = NULL;
      if (unit)
      {
        directChannel = unit->GetPerson()->GetRadio();
        if (leader && !unit->UseRadio(leader)) activeChannel = directChannel;
      }
#endif
      Assert(activeChannel);

      if (answer == AI::CommandCompleted || answer == AI::CommandFailed)
      {
        // remove command confirm if not said
        RemoveCommandConfirmMessage func(_group, this, id);
        radioChannel->ForEachMessage(func);
#if _ENABLE_DIRECT_MESSAGES
        if (directChannel) directChannel->ForEachMessage(func);
#endif
      }

      activeChannel->Transmit(new RadioMessageSubgroupAnswer(this, _group, answer, cmd, display));
    }

    if (answer == AI::CommandCompleted)
    {
      for (int i=0; i<NUnits();i++)
      {
        AIUnit *unit = GetUnit(i);
        if (!unit || unit->IsGroupLeader())
          continue;

        unit->AddExp(ExperienceCommandCompleted);
      }
    }
    else if (answer == AI::CommandFailed)
    {
      for (int i=0; i<NUnits();i++)
      {
        AIUnit *unit = GetUnit(i);
        if (!unit)
          continue;

        unit->AddExp(ExperienceCommandFailed);
      }
    }
  }
}

#define OED_SURVIVE_TIME_OK   10.0f
#define OED_OVERFORCE         0.5f
#define OED_SAFE_DIST         1.25f
#define OED_SAFE_EXP          250.0f

void AISubgroup::OnEnemyDetected(const VehicleType *type, Vector3Val pos)
{
// TODO: why ??? temporary removed
return;
  #if 0
  if (!Leader())
    return;
  if (IsPlayerSubgroup())
    return;

  Vector3 direction = Leader()->Position() - pos;
  float dist2 = direction.SquareSizeXZ();

  // check if any exposure is possible
  float maxRange = 0;
  int i;
  for (i=0; i<type->NWeapons(); i++)
  {
    const WeaponInfo &wInfo = type->GetWeapon(i);
    if (!wInfo._ammo) continue;
    saturateMax(maxRange, wInfo._ammo->maxRange);
  }
  if (dist2 > Square(maxRange))
    return;

//LogF("%s:OnEnemyDetected - distance %.0f", (const char *)GetDebugName(), sqrt(dist2));

  // calculate our / enemy attack / defend force
  Threat attackEnemy = type->GetDamagePerMinute(dist2, 1.0);
  Threat defenseEnemy;
  defenseEnemy[type->GetKind()] = type->GetArmor();
  Threat attack;
  Threat defense;
  for (i=0; i<NUnits(); i++)
  {
    AIUnit *unit = GetUnit(i);
    if (!unit || !unit->IsUnit()) // no time for get out
      continue;
    const VehicleType *type = unit->GetVehicle()->GetType();
    attack += type->GetDamagePerMinute(dist2, 1.0);
    defense[type->GetKind()] += type->GetArmor();
  }

//LogF("  Our attack: %.1f, %.1f, %.1f", attack[VSoft], attack[VArmor], attack[VAir]);
//LogF("  Our defense: %.1f, %.1f, %.1f", defense[VSoft], defense[VArmor], defense[VAir]);
//LogF("  Enemy attack: %.1f, %.1f, %.1f", attackEnemy[VSoft], attackEnemy[VArmor], attackEnemy[VAir]);
//LogF("  Enemy defense: %.1f, %.1f, %.1f", defenseEnemy[VSoft], defenseEnemy[VArmor], defenseEnemy[VAir]);

  float maxTimeF = 0, maxTimeE = 0;
  int v;
  for (v=0; v<NVehicleKind; v++)
  {
    VehicleKind vk = (VehicleKind)v;
    // there may be no threat posed by attack or totalAttack
    float timeF = attackEnemy[vk]>0 ? defense[vk]/attackEnemy[vk] : 1e10;
    float timeE = attack[vk]>0 ? defenseEnemy[vk]/attack[vk] : 1e10;
    if (timeF > maxTimeF) maxTimeF = timeF;
    if (timeE > maxTimeE) maxTimeE = timeE;
  }
//LogF("  Our survive time: %.3f", maxTimeF);
//LogF("  Enemy survive time: %.3f", maxTimeE);

  if (maxTimeF > OED_SURVIVE_TIME_OK) // there is enough time for strategic replanning
    return;

  if (maxTimeF > OED_OVERFORCE * maxTimeE)
    return;

  // Escape
//LogF("  Our position: %.0f, %.0f", Leader()->Position().X(), Leader()->Position().Z());
//LogF("  Enemy position: %.0f, %.0f", pos.X(), pos.Z());
  direction[1] = 0;
  direction.Normalize();
  Point3 destination = Leader()->Position() + OED_SAFE_DIST * maxRange * direction;
//LogF("  Escape position: %.0f, %.0f", destination.X(), destination.Z());
  int x = toIntFloor(InvLandGrid * destination.X());
  int z = toIntFloor(InvLandGrid * destination.Z());
  if (!FindNearestSafe(x, z, OED_SAFE_EXP))
  {
    LogF("Escape position not found");
    return;
  }
  destination[0] = LandGrid * x + 0.5 * LandGrid;
  destination[2] = LandGrid * z + 0.5 * LandGrid;
  destination[1] = GLOB_LAND->RoadSurfaceY(destination[0], destination[2]);
//LogF("  After find safe: %.0f, %.0f", destination.X(), destination.Z());
  Command cmd;
  cmd._message = Command::Move;
  cmd._destination = destination;
  cmd._context = Command::CtxEscape;
  if (this == GetGroup()->MainSubgroup())
  {
    GetGroup()->SendCommand(cmd);
  }
  else
    GetGroup()->SendCommand(cmd, GetUnitsList());
  #endif
}

/*!
\patch_internal 1.82 Date 8/23/2002 by Jirka
- Fixed: Crashes during SelectLeader (DoRefresh called for destroyed subgroup)
*/

/*
void AISubgroup::DoRefresh()
{
  if (_avoidRefresh) return;
  AddRef();
  AISubgroupContext context(this);
  UpdateAndRefresh(&context);
  Release();
}
*/

void FailCommand(AISubgroupContext *context);

void AISubgroup::FailCommand()
{
  AISubgroupContext context(this);
  if (GetCurrent())
  {
    context._fsm = GetCurrent()->_fsm;
    context._task = GetCurrent()->_task;
    ::FailCommand(&context);
  }
}

CombatModeRange AISubgroup::GetCombatModeBasedOnCommand() const
{
  // this allows some commands to enforce/prevent combat mode even when the rest of the group is safe/combat
  const Command *cmd = GetCommand();
  if (!cmd) return CombatModeRange(CMSafe,CMStealth);
  switch (cmd->_discretion)
  {
    case Command::MinorHidden: return CombatModeRange(CMCombat,CMStealth);
    case Command::Minor: return CombatModeRange(CMCombat,CMStealth);
    case Command::Normal: return CombatModeRange(CMAware,CMStealth);
    case Command::Major: return CombatModeRange(CMSafe,CMAware);
    default: /* Command::Undefined - should not be used */ return CombatModeRange(CMSafe,CMStealth);
  }
}

CautionRange AISubgroup::GetCautionBasedOnCommand() const
{
  // this allows some commands to enforce combat mode even when the rest of the group is safe
  const Command *cmd = GetCommand();
  if (!cmd) return CautionRange();
  switch (cmd->_discretion)
  {
    case Command::MinorHidden: return CautionRange(0.5f);
    case Command::Minor:
    case Command::Normal:
      if (cmd->_movement>=Command::MoveInDanger) return CautionRange(0.4f,1.0f);
      else if (cmd->_movement>=Command::MoveCautios) return CautionRange(0.6f,1.0f);
      else return CautionRange();
    case Command::Major: return CautionRange(0,0.2f);
    default: /* Command::Undefined - should not be used */ return CautionRange();
  }
}

bool AISubgroup::GetCoveringBasedOnCommand() const
{
  // this allows some commands to enforce combat mode even when the rest of the group is safe
  const Command *cmd = GetCommand();
  if (!cmd) return true;
  // both undefined and anything over fast means some covering
  return cmd->_movement<Command::MoveFast || cmd->_movement>Command::MoveFastIntoCover;
}



void AISubgroup::SetFormation(Formation f)
{
  _formation = f;
  //reset direction
  if(Leader())
    SetFormationDirection(Leader()->Direction(Leader()->GetFutureVisualState()));
  // force all units to change formation
  for (int i=0; i<NUnits(); i++)
  {
    AIUnit *unit = GetUnit(i);
    if (!unit || !unit->IsPartOfFormation(this))
    {
      unit->GetVehicle()->FormationChanged();
    }
  }
}

void AISubgroup::GetUnitsList(OLinkPermNOArray(AIUnit) &list)
{
  int n = _units.Size();
  list.Realloc(n);
  list.Resize(n);
  for (int i=0; i<n; i++) list[i] = _units[i].GetRef();
}

/** used for convoy movement only */
AIUnit *AISubgroup::GetFormationPrevious( AIUnit *unit ) const
{
  // find convoy predecessor
  AIUnit *maxUnit=NULL;
  AIUnit *leader=Leader();
  int maxID=0;
  for (int i=0; i<NUnits(); i++)
  {
    AIUnit *u = GetUnit(i);
    if (!unit || u==unit || u==leader) continue;
    if (!u->IsPartOfFormation(this)) continue;

    // first check if id is good enough (optimization)
    int id = u->ID();
    if (id>maxID && id<unit->ID())
    {
      // cautious vehicles follow formation, not convoy
      EntityAI *veh = u->GetVehicle();
      if (veh && veh->IsMovingInConvoy())
      {
        maxID = u->ID();
        maxUnit = u;
      }
    }
  }

  // if we do not know whom to follow, follow leader
  if( !maxUnit ) maxUnit=Leader();
  return maxUnit;
}

/** used for convoy movement only */
AIUnit *AISubgroup::GetFormationNext( AIUnit *unit ) const
{
  // find convoy successor
  AIUnit *minUnit = NULL;
  AIUnit *leader = Leader();
  int minID = INT_MAX;
  for (int i=0; i<NUnits(); i++)
  {
    AIUnit *u = GetUnit(i);
    if (!unit || u == unit || u == leader) continue;
    if (!u->IsPartOfFormation(this)) continue;
    // cautious vehicles follow formation, not convoy
    EntityAI *veh = u->GetVehicle();
    if (!veh || !veh->IsMovingInConvoy()) continue;
    int id = u->ID();
    if (id < minID && id > unit->ID())
    {
      minID = u->ID();
      minUnit = u;
    }
  }
  return minUnit;
}

TypeIsSimple(AIUnit *)

/*!
\patch 5088 Date 11/15/2006 by Jirka
- Fixed: AI sometimes did not react to change formation command
*/

void AISubgroup::UpdateFormationPos()
{
  Assert(GetGroup());
  if (!GetGroup()) return;

  int nTanks = 0;
  for (int i=0; i<NUnits(); i++)
  {
    AIUnit *unit = GetUnit(i);
    if (!unit) continue;
    Transport *veh = unit->GetVehicleIn();
    if (!veh) continue;
    if (!unit->IsPartOfFormation(this)) continue;
    if (veh->GetType()->HasGunner()) nTanks++;
  }

  int n = GetGroup()->NUnits();

  AUTO_STATIC_ARRAY(AIUnit *, formUnits, 32);
  AUTO_STATIC_ARRAY(Vector3, formPos, 32);
  formUnits.Resize(n);
  formPos.Resize(n);
  for (int i=0; i<n; i++) 
  {
    formUnits[i] = NULL;
    formPos[i] = VZero;
  }

  const FormationInfo &formationInfo = GetGroup()->GetCenter()->GetFormation(_formation);

  for (int j=0; j<n; j++)
  {
    AIUnit *unit = GetGroup()->GetUnit(j);

    int parent;
    const FormationPositionInfo &info = formationInfo.GetInfo(j, parent);

    formUnits[j] = unit;
    formPos[j].Init();

    if (parent >= 0)
    {
      AIUnit *base = formUnits[parent];
      float fXBase = 1, fZBase = 1;
      float fXUnit = 1, fZUnit = 1;
      if (base && base->IsPartOfFormation(this))
      {
        EntityAI *baseVehicle = base->GetVehicle();
        fXBase = baseVehicle->GetFormationX();
        fZBase = baseVehicle->GetFormationZ();
      }
      if (unit && unit->IsPartOfFormation(this))
      {
        EntityAI *unitVehicle = unit->GetVehicle();
        if (unitVehicle) 
        {
          fXUnit = unitVehicle->GetFormationX();
          fZUnit = unitVehicle->GetFormationZ();
        }
      }
      float factorX = 0.5 * ( fXBase+fXUnit );
      float factorZ = 0.5 * ( fZBase+fZUnit );
      formPos[j][0] = formPos[parent][0] + factorX * info.position[0];
      formPos[j][1] = 0;
      formPos[j][2] = formPos[parent][2] + factorZ * info.position[2];
    }
    else
    {
      formPos[j][0] = info.position[0];
      formPos[j][1] = 0;
      formPos[j][2] = info.position[2];
    }

    if (unit && unit->GetSubgroup() == this)
    {
      unit->_formationAngle = info.angle;
      if (nTanks == 1)
      {
        Transport *veh = unit->GetVehicleIn();
        if (veh && veh->GetType()->HasGunner())
          unit->_formationAngle = 0;
      }
      unit->_formationPos.Init();
      unit->_formationPos[0] = formPos[j][0];
      unit->_formationPos[1] = 0;
      unit->_formationPos[2] = formPos[j][2];
    }
  }
}


int AISubgroup::GetFormationParent(const AIUnit *unit) const
{
  AIGroup *grp = GetGroup(); 
  Assert(grp);
  if (!grp) return -1;
  // array

  const FormationInfo &formationInfo = grp->GetCenter()->GetFormation(_formation);

  // if there are any units outside of the subgroup or formation, skip them
  // we have group array, convert to subgroup one
  
  int slot = unit->ID()-1;
  
  int parent = -1;
  formationInfo.GetInfo(slot, parent);
  if (parent>=0)
  {
    // traverse through any units not part of formation and skip them
    // performance remark: inner loop may appear suspicious, but most often we visit each position once
    // most formations are linear or branching at the leader only
    AIUnit *base = grp->GetUnit(parent);
    while (
      !base || !base->IsPartOfFormation(this) ||
      // if "advance status" does not match, we need to act as if the unit is not there
      unit->_formOffset.Distance2(base->_formOffset)>0.1f
    )
    {
      formationInfo.GetInfo(slot, parent);
      if (parent<0) break;

      // traverse to the next unit
      slot = parent;
      base = grp->GetUnit(parent);
    }
  }
  // a subgroup may have several units with no parent, as there may be multiple paths to the formation leader
  // we want only the subgroup leader to be without a parent
  if (parent<0 && Leader() && unit!=Leader())
  {
    // If somebody does not have a parent, he is most likely almost on top of formation chain
    // deriving from the leader should work fine
    return Leader()->ID()-1;
    #if 0 // more elaborate solution: find nearest previous unit in the same subgroup
    // it is very likely the results would be same anyway
    AIUnit *lastBefore = Leader(); // if we know no other, use leader
    for (int i=0; i<NUnits(); i++)
    {
      AIUnit *before = GetUnit(i);
      if (before==unit) break;
      if (unit && unit->IsUnit()) lastBefore = unit;
    }
    return lastBefore->ID()-1;
    #endif
  }
  return parent;
}


Vector3 AISubgroup::GetFormationPosImmediate(const AIUnit *formUnit, AIUnit *&ledBy) const
{
  ledBy = Leader();
  
  Assert(GetGroup());
  if (!GetGroup()) return VZero;

  int slot = formUnit->ID()-1;
  if (slot<0)
  {
    Fail("Unit slot not found");
    return VZero;
  }
  
  return GetFormationPosImmediate(slot,ledBy);
}
  
const FormationPositionInfo &FormationInfo::GetInfo(int slot, int &parent) const
{
  if (slot < _fixed.Size())
  {
    parent = _fixed[slot].base;
    return _fixed[slot];
  }
  else
  {
    int line = (slot - _fixed.Size()) % _pattern.Size();
    parent = slot - line + _pattern[line].base;
    return _pattern[line];
  }
 } 

Vector3 AISubgroup::GetFormationPosImmediate(int slot, AIUnit *&ledBy) const
{
  const FormationInfo &formationInfo = GetGroup()->GetCenter()->GetFormation(_formation);

  int parent = -1;
  const FormationPositionInfo &info = formationInfo.GetInfo(slot, parent);

  if (parent >= 0)
  {
    AIUnit *unit = GetGroup()->GetUnit(slot);
    AIUnit *base = GetGroup()->GetUnit(parent);
    Vector3 basePos;
    bool baseReal = base && base->IsPartOfFormation(this);
    bool unitReal = unit && unit->IsPartOfFormation(this);
    
    bool offset = false;
    // if "advance status" does not match, we cannot maintain any relation and need to act as if the unit is not there
    if (!unit || !base || unit->_formOffset.Distance2(base->_formOffset)>0.1f)
      offset = true;
    if (baseReal && !offset)
    {
      basePos = base->GetVehicle()->DesiredFormationPosition();
      Assert(basePos.SquareSize()>0);

      //leader does not need position of parent
      if(Leader() == unit)
      {
        ledBy = unit;
        return unit->Position(unit->GetFutureVisualState());
      }
      
      Vector3 formDir = GetFormationDirection();
      Matrix3 formOrient = Matrix3(MDirection,formDir,VUp);
      // subtract now, it will be added once again where the result is used
      basePos -= formOrient*base->_formOffset;
      ledBy = base;
    }
    else
    {
      // if the unit is dead or in other subgroup, we need to perform recursive traversal to check assumed unit pos
      // the traversal needs to be done as if the unit is there
      basePos = GetFormationPosImmediate(parent,ledBy);
    }

    float fXBase = 1, fZBase = 1;
    float fXUnit = 1, fZUnit = 1;

    if (baseReal)
    {
      EntityAI *baseVehicle = base->GetVehicle();
      fXBase = baseVehicle->GetFormationX();
      fZBase = baseVehicle->GetFormationZ();
    }
    if (unitReal)
    {
      EntityAI *unitVehicle = unit->GetVehicle();
      if (unitVehicle) 
      {
        fXUnit = unitVehicle->GetFormationX();
        fZUnit = unitVehicle->GetFormationZ();
      }
    }
    float factorX = 0.5f * ( fXBase+fXUnit );
    float factorZ = 0.5f * ( fZBase+fZUnit );

    Vector3 formDir = GetFormationDirection();
    // TODO: more straightforward formAside computation
    Matrix3 formOrient = Matrix3(MDirection,formDir,VUp);
    Vector3 formAside = formOrient.DirectionAside();

    float positionX = info.position[0]*factorX;
    float positionZ = info.position[2]*factorZ;
    
    return basePos + positionX*formAside + positionZ * formDir;
  }
  else
  {
    //move to leader space
    Vector3 relPos = info.position -  Leader()->GetFormationPosition();

    // transform into a formation space
    EntityAIFull *lVehicle = Leader()->GetVehicle();
    Matrix4 transform;
    transform.SetDirectionAndUp(GetFormationDirection(),VUp);
    transform.SetPosition(lVehicle->FutureVisualState().Position());

    ledBy = Leader();
    return transform.FastTransform(relPos);

  }
}

bool AISubgroup::GetCoveringEnabled(int slot) const
{
  const FormationInfo &formationInfo = GetGroup()->GetCenter()->GetFormation(_formation);
  int parent = -1;
  const FormationPositionInfo &info = formationInfo.GetInfo(slot, parent);
  return info.allowCovering;
}

/*!
\patch 1.06 Date 7/19/2001 by Ondra.
- Improved: Limited speed is ignored in behaviour combat.
This makes soldier walking with rifle on back ( safe / limited speed)
behave better when detecting enemy.
*/

void AISubgroup::UpdateFormationCoef(bool init)
{
  int u, un = NUnits();

  AIUnit *leader = Leader();
  EntityAI *veh = leader->GetVehicle();

  float leaderSpeed = veh->GetType()->GetMaxSpeedMs();
#if LOG_FORMATION_COEF 
LogF("Leader speed %.0f", leaderSpeed);
#endif
  saturateMax(leaderSpeed, 0.1); // avoid dividing by zero for fixed vehicles

  float maxDelay = 0;
  float maxDelaySpeed = leaderSpeed;
  //Vector3Val leaderDir = leader->Direction();
  // we used leader direction before, but formation direction
  // seems to be much better for estimation how much are individual units behind
  Vector3Val leaderDir = _direction;
//  const float delayLost = 20.0f;
  for (u=0; u<un; u++)
  {
    AIUnit *unit = GetUnit(u);
    if (!unit || unit == leader || !unit->IsPartOfFormation(this)) continue;
    EntityAI *uVeh = unit->GetVehicle();
    if (!uVeh) continue;
    if (uVeh->IsMovingInConvoy()) continue;  // ignore those who do not follow formation
    // TODO: consider: ignore those in cover? Old style covering via HideBehind was handled here
    Vector3 vect = unit->GetFormationAbsolute(0,init) - unit->Position(unit->GetFutureVisualState());
    float speed = uVeh->GetType()->GetMaxSpeedMs();
    float delay = (vect * leaderDir);
#if LOG_FORMATION_COEF 
LogF("Unit %d: speed %.0f, delay %.1f", unit->ID(), speed, delay);
#endif
    if (delay * maxDelaySpeed > maxDelay * speed)
    {
      maxDelay = delay;
      maxDelaySpeed = speed;
    }
  }

  const float timeToEqual = 3;
  const float maxCoefChange = 0.1;    // per second
  const float noDelay = leader->GetCombatMode()>=CMCombat ? 1.0f : 0.5f;

  maxDelay -= noDelay;
#if LOG_FORMATION_COEF 
LogF("Maximum: speed %.0f, delay %.1f", maxDelaySpeed, maxDelay);
#endif

  float wantedCoef = (maxDelaySpeed - (1.0f / timeToEqual) * maxDelay) * (1.0f / leaderSpeed);
  float diffCoef = wantedCoef - _formationCoef;
#if LOG_FORMATION_COEF 
LogF("Old coef: %.2f, wanted coef: %.2f (diff %.2f)", _formationCoef, wantedCoef, diffCoef);
#endif

  float age = Glob.time - _formationCoefChanged;
  saturate(diffCoef, -maxCoefChange * age, maxCoefChange * age);
#if LOG_FORMATION_COEF 
LogF("Saturated diff %.2f", diffCoef);
#endif
  
  _formationCoef += diffCoef; 
#if LOG_FORMATION_COEF 
LogF("New coef: %.2f", _formationCoef);
#endif
  saturate(_formationCoef, 0.1, 1.5);
#if LOG_FORMATION_COEF 
LogF("Saturated new coef: %.2f", _formationCoef);
#endif
  _formationCoefChanged = Glob.time; 
#if LOG_FORMATION_COEF 
LogF("");
#endif

  float limitSpeed = _formationCoef * leaderSpeed;
  float maxSpeed = leaderSpeed;
  switch (_speedMode)
  {
    case SpeedLimited:
      // FIX
      if (leader->GetCombatMode()!=CMCombat)
      {
        float limitedSpeed = veh->GetType()->GetLimitedSpeedMs();
        saturateMax(limitedSpeed, 0.1); // avoid dividing by zero for fixed vehicles
        saturateMin(limitSpeed, limitedSpeed);
      }
      // FIX END
      veh->LimitSpeed(limitSpeed);
      break;
    case SpeedNormal:
      veh->LimitSpeed(limitSpeed);
      break;
    case SpeedFull:
      veh->LimitSpeed(1.5 * maxSpeed);
      break;
  }
}

static void AdjustDirection( Vector3 &direction, Vector3 newFormDir, float maxAngleChange, float age ) 
{
  float angleL = atan2(newFormDir.X(), newFormDir.Z());
  float angleF = atan2(direction.X(), direction.Z());
  float angle = AngleDifference(angleL, angleF);
  saturate(angle, -maxAngleChange * age, maxAngleChange * age);
  direction = Matrix3(MRotationY, -angleF - angle).Direction();
}

void AISubgroup::UpdateFormationDirection()
{
  AIUnit *leader = Leader();
  if (!leader) return;

  // check if angle needs to be updated

  float age = Glob.time.Diff(_directionChanged);

  // special handling for a player - when moving, formation direction is the direction the player is looking to
  // this is important when player is moving backwards or sidestepping - without the formation direction is changing
  EntityAIFull *leaderVeh = leader->GetVehicle();
  Vector3 newFormDir = leaderVeh->PredictVelocity();
  Vector3 newMoveDir = newFormDir;
  if (leader->IsAnyPlayer() && newFormDir.SquareSize()>Square(0.5f))
  {
    TurretContextV context;
    if (leaderVeh->GetPrimaryObserverTurret(unconst_cast(leaderVeh->FutureVisualState()), context) || leaderVeh->GetPrimaryGunnerTurret(unconst_cast(leaderVeh->FutureVisualState()), context))
      newFormDir = leaderVeh->GetFormationDirectionWanted(context);
  }

  // for AI the direction of the command should be very reliable
  else if (GetCurrent())
  {
    const Command *cmd = GetCurrent()->_task;
    if (cmd && cmd->_message != Command::Stop)//do not change direction of stopped unit
    {
      if (cmd->_direction.SquareSize()>0.1)
      {
        // we want the direction to be sufficiently large to be considered "high speed"
        newMoveDir = newFormDir = cmd->_direction*5;
      }
      else
      {
        LogF("AI command with no direction, %s:%s",cc_cast(GetDebugName()),cc_cast(FindEnumName(cmd->_message)));
      }
    }
  }
  
  if (newMoveDir.SquareSize()>=Square(0.9f))
  {
    const float maxAngleChangeMove = 0.3f; // ~ 10 sec for a 180 deg turn
    AdjustDirection(_directionMove,newMoveDir,maxAngleChangeMove,age);
  }
  if (newFormDir.SquareSize()>=Square(0.9f))
  {
    const float maxAngleChange = 0.1f; // ~ 30 sec for a 180 deg turn
    AdjustDirection(_direction,newFormDir,maxAngleChange,age);
  }
  else
  {
    // look to enemies
    const float maxAngleChange = 0.05f; // ~ 60 sec for a 180 deg turn

    Vector3Val leaderPos = leader->Position(leader->GetFutureVisualState());
    Vector3 sumDir = VZero;
    bool found = false;
    for (int i=0; i<_group->GetTargetList().EnemyCount(); i++)
    {
      TargetNormal *tar = _group->GetTargetList().GetEnemy(i);
      if (tar->subjectiveCost > 0)
      {
        found = true;
        Vector3 dir = (tar->LandAimingPosition() - leaderPos).Normalized();
        sumDir += tar->subjectiveCost * dir;
      }
    }
    if (found)
    {
      float angleL = atan2(sumDir.X(), sumDir.Z());
      float angleF = atan2(_direction.X(), _direction.Z());
      float angle = AngleDifference(angleL, angleF);

      saturate(angle, -maxAngleChange * age, maxAngleChange * age);

      _direction = Matrix3(MRotationY, -angleF - angle).Direction();
      // _directionMove not updated here, only _direction
    }
  }

  _directionChanged = Glob.time;
}

void AISubgroup::GetUnitsListNoCargo(OLinkPermNOArray(AIUnit) &list)
{
  list.Resize(0);
  for (int i=0; i<_units.Size(); i++)
  {
    AIUnit *unit = _units[i];
    if (unit && unit->IsUnit()) list.Add(unit);
  }
  list.Compact();
}

// Implementation

// Strategic planning
float AISubgroup::GetExposure(int x, int z)
{
  Assert(Leader());
  return GetGroup()->GetCenter()->GetExposureOptimistic(x, z);
}

// Strategic planning
Threat AISubgroup::GetThreat(int x, int z)
{
  Assert(Leader());
  AIThreat t = GetGroup()->GetCenter()->GetThreatOptimistic(x, z);
  return Threat(t.u.soft*EXPOSURE_COEF,t.u.armor*EXPOSURE_COEF,t.u.air*EXPOSURE_COEF);
}

/**
Function assumes the threats registered in the threat map are real and will not be eliminated meanwhile.
*/

float AISubgroup::CalculateExposure(int x, int z)
{
  // returns damage (in $) per second
  Threat expField = GetThreat(x, z) *(1/60.0f); // damage per minute - per second

  float sumCost = 0;
  for (int i=0; i<NUnits(); i++)
  {
    AIUnit *unit = GetUnit(i);
    if (!unit)
      continue;
    EntityAIFull *veh = unit->GetVehicle();
    if (!veh) continue;
    float damage = veh->GetType()->GetInvArmor();
    float cost = unit->GetPerson()->GetType()->GetCost();
    if (!unit->IsFreeSoldier())
    { // may be driver, commander, gunner, cargo
      if (unit->IsUnit())
      { // calculate only "commander" units
        cost += veh->GetType()->GetCost();
      }
    }
    // if we are not able to defend, assume bigger damage
    bool ableToDefend = veh->CheckAmmoHitOver(50) && !unit->IsHoldingFire();
    if (!ableToDefend)
    {
      damage *= COEF_EXPOSURE_UNABLE_TO_DEFEND / COEF_EXPOSURE;
    }
    sumCost += Threat(veh->GetType()->GetKind(),cost) * damage*expField;
  }
  return sumCost;
}

float AISubgroup::GetFieldCost(int x, int z, bool road, bool includeGradient, float gradient)
{
  GeographyInfo geogr = GLOB_LAND->GetGeography(x,z);
  geogr.u.road = road; // if we are searching on road is now given externally

  float cost = -FLT_MAX;
  for (int i=0; i<NUnits(); i++)
  {
    AIUnit *unit = GetUnit(i);
    if (!unit) continue;
    if (unit->GetVehicleIn() && !unit->IsDriver()) continue;
    EntityAI *uVeh = unit->GetVehicle();
    if (uVeh)
    {
      float costU = uVeh->GetBaseCost(geogr, false, includeGradient) * uVeh->GetFieldCost(geogr);
      if (!includeGradient) costU *= uVeh->GetGradientPenalty(gradient); // exact gradient is used
      if (costU > cost) cost = costU;
    }
  }
  
  if (cost <= 0) cost = 1;
  cost *= LandGrid; // cost = time for distance == LandGrid
  saturateMin(cost, SET_UNACCESSIBLE); // avoid inf. in cost

  float costExposure = cost * CalculateExposure(x, z); // dammage (in $) per time cost
  return cost + COEF_EXPOSURE * costExposure;
}

bool AISubgroup::FindNearestSafe(int &x, int &z, float threshold)
{
  if (IsSafe(x, z, threshold))
    return true;
  int rMax = 10; // do not too far (max 500 m)
  int i, r, xt, zt;
  for (r=1; r<rMax; r++)
  {
    for (i=0; i<r; i++)
    {
      xt = x - i; zt = z - r;
      if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
      xt = x + i; zt = z - r;
      if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
      xt = x + r; zt = z - i;
      if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
      xt = x + r; zt = z + i;
      if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
      xt = x + i; zt = z + r;
      if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
      xt = x - i; zt = z + r;
      if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
      xt = x - r; zt = z + i;
      if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
      xt = x - r; zt = z - i;
      if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
    }
    xt = x - r; zt = z - r;
    if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
    xt = x + r; zt = z - r;
    if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
    xt = x + r; zt = z + r;
    if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
    xt = x - r; zt = z + r;
    if (IsSafe(xt, zt, threshold)) goto SafeFieldFound;
  }
  return false;
SafeFieldFound:
  x = xt;
  z = zt;
  return true;
}

#define CheckSafety \
  if (GetSafety(xt, zt, exposure) && exposure < minExposure) \
  { xBest = xt; zBest = zt; minExposure = exposure; }

bool AISubgroup::FindNearestSafer(int &x, int &z)
{
  int xt, xBest = x;
  int zt, zBest = z;
  float exposure = 0, minExposure = GetExposure(x, z);
  int r, rMax = 8;
  for (r=1; r<rMax; r++)
  {
    int i;
    for (i=0; i<r; i++)
    {
      xt = x - i; zt = z - r;
      CheckSafety
      xt = x + i; zt = z - r;
      CheckSafety
      xt = x + r; zt = z - i;
      CheckSafety
      xt = x + r; zt = z + i;
      CheckSafety
      xt = x + i; zt = z + r;
      CheckSafety
      xt = x - i; zt = z + r;
      CheckSafety
      xt = x - r; zt = z + i;
      CheckSafety
      xt = x - r; zt = z - i;
      CheckSafety
    }
    xt = x - r; zt = z - r;
    CheckSafety
    xt = x + r; zt = z - r;
    CheckSafety
    xt = x + r; zt = z + r;
    CheckSafety
    xt = x - r; zt = z + r;
    CheckSafety
  }
  if (x == xBest && z == zBest) return false;
  else
  {
    x = xBest;
    z = zBest;
    return true;
  }
}

void AISubgroup::ClearPlan()
{
  if (Leader())
  {
    Leader()->ClearStrategicPlan();
    Leader()->ClearOperativePlan();
  }
}

void AISubgroup::RefreshPlan()
{
  if (Leader()) Leader()->RefreshStrategicPlan();
}

void AISubgroup::ExposureChanged(int x, int z, float optimistic)
{
  if (Leader()) Leader()->ExposureChanged(x, z, optimistic);
}

bool AISubgroup::IsSafe(int x, int z, float threshold)
{
  return
    InRange(x, z) &&
    GetFieldCost(x, z, false) < GET_UNACCESSIBLE &&
    GetExposure(x, z) < threshold;
}
bool AISubgroup::GetSafety(int x, int z, float &exposure)
{
  if (InRange(x, z) && GetFieldCost(x, z, false) < GET_UNACCESSIBLE)
  {
    exposure = GetExposure(x, z);
    return true;
  }
  else return false;
}

// interface for fsm

void AISubgroup::GoDirect(Vector3Val pos)
{
  Command *cmd = GetCommand();
  if (cmd) cmd->_destination = pos;

  _mode = DirectGo;
  _wantedPosition = PlanPosition(pos,false);
  if (_whoAmI) _whoAmI->SetWantedPosition(_wantedPosition, 0, AIUnit::LeaderDirect);
}

void AISubgroup::GoPlanned(Vector3Val pos, float precision, bool intoCover)
{
  Command *cmd = GetCommand();
  if (cmd)
  {
    // update the command destination + precision
    cmd->_destination = pos;
    cmd->_precision = precision;
    // destination updated - we need to update direction as well
    if (_whoAmI && cmd->_direction.SquareSize()<0.1f)
    {
      cmd->_direction = (pos - _whoAmI->Position(_whoAmI->GetFutureVisualState())).Normalized();
    }
  }

  _mode = PlanAndGo;
  // TODO: sometimes we want a path to cover here
  _wantedPosition = PlanPosition(pos,intoCover);
  if (_whoAmI) _whoAmI->SetWantedPosition(_wantedPosition, precision, AIUnit::LeaderPlanned);
}

void AISubgroup::DoNotGo()
{
  _mode = Wait;
  /// ??? precision
  if (_whoAmI) _whoAmI->SetWantedPosition(_wantedPosition, 0, AIUnit::DoNotPlan);
}

void AISubgroup::Stop()
{
  _mode = Wait;
  if (_whoAmI) _whoAmI->SetWantedPosition(_wantedPosition, 0, AIUnit::DoNotPlan, true);
}

void AISubgroup::SetDiscretion(Command::Discretion discretion)
{
  if (!GetGroup() || GetGroup()->IsAnyPlayerGroup())
    return;

  if (discretion == Command::Undefined)
    return;

  if (!GetGroup()->GetFlee())
  {
    // when some enemy has disclosed us lately, change "Major" (safe) to "Normal" (Aware)
    if (Glob.time - 180.0 < GetGroup()->GetDisclosed() && discretion == Command::Major)
      discretion = Command::Normal;
  }
  
  // set formation and engage mode based on discretion
  Formation f;
  Semaphore s;
  switch (discretion)
  {
    case Command::Major:
      f = FormColumn;
      s = SemaphoreGreen;
      break;
    case Command::Normal:
      f = FormWedge;
      s = SemaphoreYellow;
      break;
    default:
    case Command::Minor:
      f = FormLine;
      s = SemaphoreRed;
      break;
  }
  GetGroup()->SendFormation(f, this);
  OLinkPermNOArray(AIUnit) list;
  GetUnitsList(list);
  GetGroup()->SendSemaphore(s, list);
  if (GetGroup() && GetGroup()->MainSubgroup() == this)
  {
    GetGroup()->_semaphore = s;
  }
}

bool AISubgroup::AllUnitsCompleted()
{
  AIUnit *leader=Leader();
  if (!leader || !leader->HasAI()) return true;
  if (leader->GetState() == AIUnit::Completed)
  {
    // avoid AllUnitsCompleted satisfy twice
    leader->SetState(AIUnit::Wait);
    return true;
  }
  else return false;
}

RString AISubgroup::GetDebugName() const
{
  RString buffer;
  if (GetGroup())
    buffer = GetGroup()->GetDebugName() + RString(":");
  else
    buffer = "<No group>:";

  if (NUnits() > 0)
  {
    if (Leader())
      buffer = buffer + Format("%d", Leader()->ID());
    else
      buffer = buffer + RString("<No leader>");
  }
  else
  {
    if (GetGroup() && GetGroup()->MainSubgroup() == this)
      buffer = buffer + RString("<Empty main subgroup>");
    else
      buffer = buffer + RString("<No units>");
  }
  return buffer;
}

void ReportUnit(const AIBrain *unit);
void ReportSubgroup(const AISubgroup *subgrp)
{
  RptF
  (
    "Subgroup %s (0x%x) - network ID %d:%d",
    (const char *)subgrp->GetDebugName(),
    subgrp,
    subgrp->GetNetworkId().creator.CreatorInt(), subgrp->GetNetworkId().id
  );
}

bool AISubgroup::AssertValid() const
{
  if (NUnits() == 0)
  {
    // OK for main subgroup or newly created subgroups
    return true;
  }

  bool result = true;
  if (!Leader())
  {
    ReportSubgroup(this);
    RptF(" - no leader");
    result = false;
    {
      // try to repair it
      AISubgroup *subgroup = const_cast<AISubgroup *>(this);
      subgroup->SelectLeader();
    }
  }
  else if (Leader()->GetSubgroup() == NULL)
  {
    ReportSubgroup(this);
    ReportUnit(Leader());
    RptF(" - leader with no subgroup");
    result = false;
  }
  else if (Leader()->GetSubgroup() != this)
  {
    ReportSubgroup(this);
    ReportUnit(Leader());
    RptF(" - leader from another subgroup");
    result = false;
    {
      // try to repair it
      AISubgroup *subgroup = const_cast<AISubgroup *>(this);
      subgroup->SelectLeader();
    }
  }
  for (int i=0; i<_units.Size(); i++)
  {
    AIUnit *unit = _units[i];
    if (!unit) continue;
    if (unit != Leader())
    {
      // do not check twice
      if (unit->GetSubgroup() == NULL)
      {
        ReportSubgroup(this);
        ReportUnit(unit);
        RptF(" - unit with no subgroup");
        result = false;
      }
      else if (unit->GetSubgroup() != this)
      {
        ReportSubgroup(this);
        ReportUnit(unit);
        RptF(" - unit from another subgroup");
        result = false;
      }
    }
    if (!unit->AssertValid()) result = false;
  }
  return result;
}

void AISubgroup::Dump(int indent) const
{
}

void AISubgroup::OnRemotePlayerAdded()
{
  for (int i=0; i<_stack.Size(); i++)
  {
    Command *cmd = _stack[i]._task;
    if (cmd && cmd->GetNetworkId().IsNull())
    {
      // create the command
      GetNetworkManager().CreateCommand(this, cmd, false);
    }
  }
}
