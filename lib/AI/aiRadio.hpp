#ifdef _MSC_VER
#pragma once
#endif

#ifndef _AI_RADIO_HPP
#define _AI_RADIO_HPP

/******************************************/
/*                                        */
/*              RadioMessages             */
/*                                        */
/******************************************/

enum RadioMessageType
{
	RMTCommand,
	RMTFormation,
	RMTBehaviour,
	RMTOpenFire,
	RMTCeaseFire,
	RMTLooseFormation,
	RMTReportStatus,
	RMTTarget,
	RMTSubgroupAnswer,
	RMTReturnToFormation,
	RMTFireStatus,
	RMTUnitAnswer,
	RMTUnitKilled,
	RMTText,
	RMTReportTarget,
	RMTObjectDestroyed,
	RMTContact, RMTUnderFire, RMTClear,
	RMTRepeatCommand,
	RMTWhereAreYou,
	RMTNotifyCommand,
	RMTCommandConfirm,
	RMTWatchAround,RMTWatchDir,RMTWatchPos,RMTWatchTgt,RMTWatchAuto,
	RMTPosition,
	RMTFormationPos,
	RMTTeam,
	RMTSupportAsk,
	RMTSupportConfirm,
	RMTSupportReady,
	RMTSupportDone,
	RMTJoin,
	RMTJoinDone,
  RMTTalk,
  RMTSupportNotAvailable,
  RMTTask,
  RMTTaskReceived,
  RMTTaskResult,
  /// entered cover, report ready to provide covering fire
  RMTCovering,
  /// leaving cover - request covering fire
  RMTCoverMe, 
	RMTSuppress,
	RMTThrowingGrenade,
	RMTThrowingSmoke,
	RMTReloading,
  RMTArtilleryFireStatus,
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitKilled

class RadioMessageUnitKilled : public RadioMessage
{
protected:
	//OLinkPerm<AIGroup> _from;
	OLinkPerm<AIUnit> _from;
	OLinkPerm<AIUnit> _who;

public:
	RadioMessageUnitKilled(AIUnit *from, AIUnit *who)
	{_from = from; _who = who;}
	RadioMessageUnitKilled() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTUnitKilled;}
	AIBrain *GetSender() const {return _from;}
	AIUnit *GetWhoKilled() const {return _who;}

protected:
	RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportAsk

// used when group asked for support
class RadioMessageSupportAsk : public RadioMessage
{
protected:
	OLinkPerm<AIGroup> _from;
	UIActionType _type;

public:
	RadioMessageSupportAsk(AIGroup *from, UIActionType type)
	{_from = from; _type = type;}
	RadioMessageSupportAsk() {_type = (UIActionType)0;}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTSupportAsk;}
	AIBrain *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportNotAvailable

// used when support for group not available
class RadioMessageSupportNotAvailable : public RadioMessage
{
protected:
  OLinkPerm<AIGroup> _to;
  RString _senderName;
  SRef<Speaker> _speaker;

public:
  RadioMessageSupportNotAvailable(AIGroup *to)
  {_to = to; SetSenderName();}
  RadioMessageSupportNotAvailable() {SetSenderName();}
  LSError Serialize(ParamArchive &ar);
  const char *GetPriorityClass();
  void Transmitted(ChatChannel channel);
  int GetType() const {return RMTSupportNotAvailable;}
  AIBrain *GetSender() const {return NULL;}
  RString GetSenderName() const {return _senderName;}
  virtual Speaker *GetSpeaker() const {return _speaker;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
  void SetSenderName();
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportConfirm

// used when support group is assigned
class RadioMessageSupportConfirm : public RadioMessage
{
protected:
	OLinkPerm<AIGroup> _from;

public:
	RadioMessageSupportConfirm(AIGroup *from) {_from = from;}
	RadioMessageSupportConfirm() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTSupportConfirm;}
	AIBrain *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportReady

// used when support group reached rendezvous place
class RadioMessageSupportReady : public RadioMessage
{
protected:
	OLinkPerm<AIGroup> _from;

public:
	RadioMessageSupportReady(AIGroup *from)
	{_from = from;}
	RadioMessageSupportReady() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTSupportReady;}
	AIBrain *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportDone

// used when supported group already doesn't need support
class RadioMessageSupportDone : public RadioMessage
{
protected:
	OLinkPerm<AIGroup> _from;

public:
	RadioMessageSupportDone(AIGroup *from)
	{_from = from;}
	RadioMessageSupportDone() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTSupportDone;}
	AIBrain *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageJoin

class RadioMessageJoin : public RadioMessage
{
protected:
	OLinkPerm<AIGroup> _from;
	OLinkPerm<AIUnit> _follow;
	OLinkPermNOArray(AIUnit) _to;

public:
	RadioMessageJoin(AIGroup *from, AIUnit *follow, OLinkPermNOArray(AIUnit) &to);
	RadioMessageJoin() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel) {}
	int GetType() const {return RMTJoin;}
	AIBrain *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
	bool IsTo(AIBrain *unit) const;
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageJoinDone

// used when supported group already doesn't need support
class RadioMessageJoinDone : public RadioMessage
{
protected:
	OLinkPerm<AIUnit> _from;
	OLinkPerm<AIGroup> _to;

public:
	RadioMessageJoinDone(AIUnit *from, AIGroup *to)
	{_from = from; _to = to;}
	RadioMessageJoinDone() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel) {}
	int GetType() const {return RMTJoinDone;}
	AIBrain *GetSender() const {return _from;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitAnswer

class RadioMessageUnitAnswer : public RadioMessage
{
protected:
	OLinkPerm<AIUnit> _from;
	OLinkPerm<AISubgroup> _to;
	AI::Answer _answer;

public:
	RadioMessageUnitAnswer(AIUnit *from, AISubgroup *to, AI::Answer answer)
	{_from = from; _to = to; _answer = answer;}
	RadioMessageUnitAnswer() {}
	AIUnit *GetFrom() const {return _from;}
	AI::Answer GetAnswer() const {return _answer;}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTUnitAnswer;}
	AIBrain *GetSender() const {return _from;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFireStatus

class RadioMessageFireStatus : public RadioMessage
{
protected:
	OLinkPerm<AIUnit> _from;
	bool _answer;

public:
	RadioMessageFireStatus(AIUnit *from, bool answer)
	{_from = from; _answer = answer;}
	RadioMessageFireStatus() {}
	AIUnit *GetFrom() const {return _from;}
	bool GetAnswer() const {return _answer;}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTFireStatus;}
	AIBrain *GetSender() const {return _from;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageArtilleryFireStatus

class RadioMessageArtilleryFireStatus : public RadioMessage
{
protected:
  OLinkPerm<AIUnit> _from;
  int _answer;

public:
  RadioMessageArtilleryFireStatus(AIUnit *from, int answer)
  {_from = from; _answer = answer;}
  RadioMessageArtilleryFireStatus() {}
  AIUnit *GetFrom() const {return _from;}
  int GetAnswer() const {return _answer;}
  LSError Serialize(ParamArchive &ar);
  const char *GetPriorityClass();
  void Transmitted(ChatChannel channel);
  int GetType() const {return RMTFireStatus;}
  AIBrain *GetSender() const {return _from;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};


///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSubgroupAnswer

class RadioMessageSubgroupAnswer : public RadioMessage
{
protected:
	OLinkPerm<AISubgroup> _from;
	OLinkPerm<AIUnit> _leader;
	OLinkPerm<AIGroup> _to;
	AI::Answer _answer;
	Command _cmd;
	bool _display;
	bool _forceLeader;

public:
	RadioMessageSubgroupAnswer
	(
		AISubgroup *from, AIGroup *to, AI::Answer answer, Command *cmd, bool display = true,
		AIUnit *leader = NULL
	);
	RadioMessageSubgroupAnswer();
	const char *GetPriorityClass();
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTSubgroupAnswer;}
	AISubgroup *GetFrom() const {return _from;}
	AIGroup *GetTo() const {return _to;}
	const Command &GetCommand() const {return _cmd;}
	AIBrain *GetSender() const;

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReturnToFormation

class RadioMessageReturnToFormation : public RadioMessage
{
protected:
	OLinkPerm<AISubgroup> _from;
	OLinkPerm<AIUnit> _leader;
	OLinkPerm<AIUnit> _to;

public:
	RadioMessageReturnToFormation(AISubgroup *from, AIUnit *to)
	{
		_from = from; _leader = from->Leader(); _to = to;
	}
	RadioMessageReturnToFormation() {}
	const char *GetPriorityClass();
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTReturnToFormation;}
	AIBrain *GetSender() const {return _from && _from->Leader() ? _from->Leader() : (AIUnit*)_leader;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReply
// abstract class for all reply information

class RadioMessageReply : public RadioMessage
{
protected:
	OLinkPerm<AIUnit> _from;
	OLinkPerm<AIGroup> _to;

public:
	RadioMessageReply(AIUnit *from, AIGroup *to);
	RadioMessageReply() {}
	LSError Serialize(ParamArchive &ar);
	AIUnit *GetFrom() const {return _from;}
	AIGroup *GetTo() const {return _to;}
	AIBrain *GetSender() const {return _from;}

  /// helper for derived classes
  RString PrepareSentenceHelper(SentenceParams &params, const char *sentence) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReportTarget

//! report target in group
struct ReportTargetInfo
{
	//! link to group target database
	LLink<TargetNormal> tgt; // link 
	//TargetId id;
	//TargetSide side;
	//const VehicleType *type;
	LSError Serialize(ParamArchive &ar);
};
TypeContainsLLink(ReportTargetInfo);

class RadioMessageReportTarget : public RadioMessageReply
{
	typedef RadioMessageReply base;
private:
  //@( Helper values to answer later Simple Expression questions
  mutable float _distanceToUnit;
  mutable float _distanceToSender;
  mutable float _distanceToGroup;
  mutable float _distanceToLocation;
  mutable float _groupCoreRadius;
  mutable float _groupCompactness;
  mutable float _unitDistanceFactor;
  mutable const Location *_location;
  //@)

protected:
	ReportSubject _subject;
	int _x;
	int _z;
	mutable AutoArray<ReportTargetInfo> _targets; //mutable as Friendly targets should be removed inside PrepareSentence

public:
  RadioMessageReportTarget(AIUnit *from, AIGroup *to, ReportSubject subject, AIGroup::ReportTargetList &targets);
	//RadioMessageReportTarget(AIUnit *from, AIGroup *to, ReportSubject subject, AITargetInfo &target);
	RadioMessageReportTarget() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTReportTarget;}
	ReportSubject GetSubject() {return _subject;}
	bool IsTarget(TargetType *id) const;
	bool HasType(const VehicleType *type) const;
	void DeleteTarget(TargetType *id);
	void AddTarget(TargetNormal &target);
	//void AddTarget(AITargetInfo &target);

  virtual float GetSEDistanceToUnit() const {return _distanceToUnit;}
  virtual float GetSEDistanceToSender() const {return _distanceToSender;}
  virtual float GetSEDistanceToGroup() const {return _distanceToGroup;}
  virtual float GetSEDistanceToLocation() const {return _distanceToLocation;}
  virtual float GetSEUnitDistanceFactor() const {return _unitDistanceFactor;}
  virtual float GetSEInsideLocation() const;
  virtual float GetSEGroupCompactness() const {return _groupCompactness;}
  virtual float GetSEGroupCoreRadius() const {return _groupCoreRadius;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageNotifyCommand

class RadioMessageNotifyCommand : public RadioMessageReply
{
	typedef RadioMessageReply base;

protected:
	Command _command;
	OLinkPermNOArray(AIUnit) _list;

public:
	RadioMessageNotifyCommand(const OLinkPermNOArray(AIUnit) &list, AIGroup *to, Command &command);
	RadioMessageNotifyCommand() {}
	const char *GetPriorityClass();
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTNotifyCommand;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageObjectDestroyed

class RadioMessageObjectDestroyed : public RadioMessageReply
{
	typedef RadioMessageReply base;

protected:
	Ref<const VehicleType> _vehicleType;

public:
	RadioMessageObjectDestroyed(AIUnit *from, AIGroup *to, const VehicleType *type);
	RadioMessageObjectDestroyed() {_vehicleType = NULL;}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTObjectDestroyed;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageContact

class RadioMessageContact : public RadioMessageReply
{
	typedef RadioMessageReply base;
public:
	RadioMessageContact(AIUnit *from, AIGroup *to);
	RadioMessageContact() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTContact;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageUnderFire : public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageUnderFire(AIUnit *from, AIGroup *to): base(from, to){}
	RadioMessageUnderFire() {}
	const char *GetPriorityClass() {return "UrgentCommand";}
	int GetType() const {return RMTUnderFire;}

protected:
  RString PrepareSentence(SentenceParams &params) const {return PrepareSentenceHelper(params, "SentUnderFire");}
};

class RadioMessageCoverMe: public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageCoverMe(AIUnit *from, AIGroup *to): base(from, to){}
	RadioMessageCoverMe() {}
	const char *GetPriorityClass() {return "Confirmation";}
	int GetType() const {return RMTCoverMe;}

protected:
  RString PrepareSentence(SentenceParams &params) const {return PrepareSentenceHelper(params, "SentCoverMe");}
};

class RadioMessageCovering: public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageCovering(AIUnit *from, AIGroup *to): base(from, to){}
	RadioMessageCovering() {}
	const char *GetPriorityClass() {return "Confirmation";}
	int GetType() const {return RMTCovering;}

protected:
  RString PrepareSentence(SentenceParams &params) const {return PrepareSentenceHelper(params, "SentCovering");}
};

class RadioMessageThrowingGrenade: public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageThrowingGrenade(AIUnit *from, AIGroup *to): base(from, to){}
	RadioMessageThrowingGrenade() {}
	const char *GetPriorityClass() {return "Confirmation";}
	int GetType() const {return RMTThrowingGrenade;}

protected:
  RString PrepareSentence(SentenceParams &params) const {return PrepareSentenceHelper(params, "SentThrowingGrenade");}
};

class RadioMessageThrowingSmoke: public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageThrowingSmoke(AIUnit *from, AIGroup *to): base(from, to){}
	RadioMessageThrowingSmoke() {}
	const char *GetPriorityClass() {return "Confirmation";}
	int GetType() const {return RMTThrowingSmoke;}

protected:
  RString PrepareSentence(SentenceParams &params) const {return PrepareSentenceHelper(params, "SentThrowingSmoke");}
};

class RadioMessageReloading: public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageReloading(AIUnit *from, AIGroup *to): base(from, to){}
	RadioMessageReloading() {}
	const char *GetPriorityClass() {return "Confirmation";}
	int GetType() const {return RMTReloading;}

protected:
  RString PrepareSentence(SentenceParams &params) const {return PrepareSentenceHelper(params, "SentReloading");}
};

class RadioMessageClear : public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageClear(AIUnit *from, AIGroup *to);
	RadioMessageClear() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTClear;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageRepeatCommand

class RadioMessageRepeatCommand : public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageRepeatCommand(AIUnit *from, AIGroup *to);
	RadioMessageRepeatCommand() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTRepeatCommand;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWhereAreYou

class RadioMessageWhereAreYou : public RadioMessageReply
{
	typedef RadioMessageReply base;

public:
	RadioMessageWhereAreYou(AIUnit *from, AIGroup *to);
	RadioMessageWhereAreYou() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTWhereAreYou;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageCommandConfirm

class RadioMessageCommandConfirm : public RadioMessageReply
{
	typedef RadioMessageReply base;

protected:
	Command _command;

public:
	RadioMessageCommandConfirm(AIUnit *from, AIGroup *to, Command &command);
	RadioMessageCommandConfirm() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTCommandConfirm;}
	const Command &GetCommand() const {return _command;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFormation

class RadioMessageFormation : public RadioMessage
{
protected:
	OLinkPerm<AIGroup> _from;
	OLinkPerm<AISubgroup> _to;
	AI::Formation _formation;

public:
	RadioMessageFormation(AIGroup *from, AISubgroup *to, AI::Formation formation)
	{_from = from; _to = to; _formation = formation;}
	RadioMessageFormation() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTFormation;}

	AIGroup *GetFrom() {return _from;}
	AISubgroup *GetTo() {return _to;}
	AI::Formation GetFormation() {return _formation;}
	void SetFormation(AI::Formation f) {_formation = f;}
	AIBrain *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageState
// abstract class for all state information

class RadioMessageState: public RadioMessage
{
	typedef RadioMessage base;

protected:
	OLinkPerm<AIGroup> _from;
	mutable OLinkPermNOArray(AIUnit) _to; //mutable as we need to remove some dead units sometimes inside PrepareSentence

public:
	RadioMessageState(AIGroup *from, OLinkPermNOArray(AIUnit) &to);
	RadioMessageState();
	virtual RadioMessageState *Clone() const = 0; 
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	
	AIGroup *GetFrom() const {return _from;}
	bool IsTo(AIBrain *unit) const;
	bool IsOnlyTo(AIBrain *unit) const;
	void DeleteTo(AIUnit *unit);
	void AddTo(AIUnit *unit);
	void ClearTo();
  const OLinkPermNOArray(AIUnit) GetTo() const {return _to;}
	bool IsToSomeone() const;

	AIBrain *GetSender() const {return _from ? _from->Leader() : NULL;}
};

class RadioMessageBehaviour : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	CombatMode _behaviour;

public:
	RadioMessageBehaviour(AIGroup *from, OLinkPermNOArray(AIUnit) &to, CombatMode behaviour);
	RadioMessageBehaviour() {}
	RadioMessageState *Clone() const {return new RadioMessageBehaviour(*this);}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTBehaviour;}
	
	CombatMode GetBehaviour() {return _behaviour;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageSuppress : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	Time _until;

public:
	RadioMessageSuppress(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Time until);
	RadioMessageSuppress() {}
	RadioMessageState *Clone() const {return new RadioMessageSuppress(*this);}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTSuppress;}
	
	Time GetUntil() const {return _until;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

enum OpenFireState;
AI::Semaphore ApplyOpenFire(AI::Semaphore s, OpenFireState open);

class RadioMessageOpenFire : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	OpenFireState _open;

public:
	RadioMessageOpenFire(AIGroup *from, OLinkPermNOArray(AIUnit) &to, OpenFireState open);
	RadioMessageOpenFire() {}
	RadioMessageState *Clone() const {return new RadioMessageOpenFire(*this);}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTOpenFire;}
	
	OpenFireState GetOpenFireState() {return _open;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageCeaseFire : public RadioMessage
{
	typedef RadioMessage base;

protected:
	OLinkPerm<AIBrain> _from;
	OLinkPerm<AIBrain> _to;
	bool _insideGroup;

public:
	RadioMessageCeaseFire(AIBrain *from, AIBrain *to, bool insideGroup);
	RadioMessageCeaseFire() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTCeaseFire;}
	AIBrain *GetSender() const {return _from;}
	AIBrain *GetTo() const {return _to;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageLooseFormation

AI::Semaphore ApplyLooseFormation(AI::Semaphore s, bool loose);

class RadioMessageLooseFormation : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	bool _loose;

public:
	RadioMessageLooseFormation(AIGroup *from, OLinkPermNOArray(AIUnit) &to, bool loose);
	RadioMessageLooseFormation() {}
	RadioMessageState *Clone() const {return new RadioMessageLooseFormation(*this);}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTLooseFormation;}
	
	bool IsLooseFormation() {return _loose;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitPos

class RadioMessageUnitPos : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	UnitPosition _state;

public:
	RadioMessageUnitPos(AIGroup *from, OLinkPermNOArray(AIUnit) &to, UnitPosition state);
	RadioMessageUnitPos() {}
	RadioMessageState *Clone() const {return new RadioMessageUnitPos(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTPosition;}
	
	UnitPosition GetSemaphore() {return _state;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitPos

class RadioMessageFormationPos : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	AI::FormationPos _state;

public:
	RadioMessageFormationPos(AIGroup *from, OLinkPermNOArray(AIUnit) &to, AI::FormationPos state);
	RadioMessageFormationPos() {}
	RadioMessageState *Clone() const {return new RadioMessageFormationPos(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTFormationPos;}
	
	AI::FormationPos GetFormationPos() {return _state;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWatchAround

class RadioMessageWatchAround : public RadioMessageState
{
	typedef RadioMessageState base;

public:
	RadioMessageWatchAround(AIGroup *from, OLinkPermNOArray(AIUnit) &to);
	RadioMessageWatchAround() {}
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTWatchAround;}
	RadioMessageState *Clone() const {return new RadioMessageWatchAround(*this);}
	
protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWatchAuto

class RadioMessageWatchAuto : public RadioMessageState
{
	typedef RadioMessageState base;

public:
	RadioMessageWatchAuto(AIGroup *from, OLinkPermNOArray(AIUnit) &to);
	RadioMessageWatchAuto() {}
	RadioMessageState *Clone() const {return new RadioMessageWatchAuto(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTWatchAuto;}
	
protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWatchDir

class RadioMessageWatchDir : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	Vector3 _dir;

public:
	RadioMessageWatchDir(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Vector3Val dir);
	RadioMessageWatchDir() {}
	RadioMessageState *Clone() const {return new RadioMessageWatchDir(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTWatchDir;}
	Vector3Val GetWatchDirection() const {return _dir;}
	
protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWatchPos

class RadioMessageWatchPos : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	Vector3 _pos;

public:
	RadioMessageWatchPos(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Vector3Val pos);
	RadioMessageWatchPos() {}
	RadioMessageState *Clone() const {return new RadioMessageWatchPos(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTWatchPos;}
	Vector3Val GetWatchPosition() const {return _pos;}
	
protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWatchTgt

class RadioMessageWatchTgt : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	LinkTarget _tgt;

public:
	RadioMessageWatchTgt(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Target *tgt);
	RadioMessageWatchTgt() {}
	RadioMessageState *Clone() const {return new RadioMessageWatchTgt(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTWatchTgt;}
	Target *GetWatchTarget() const {return _tgt;}
	
protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReportStatus

class RadioMessageReportStatus : public RadioMessageState
{
	typedef RadioMessageState base;

public:
	RadioMessageReportStatus(AIGroup *from, OLinkPermNOArray(AIUnit) &to);
	RadioMessageReportStatus() {}
	RadioMessageState *Clone() const {return new RadioMessageReportStatus(*this);}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTReportStatus;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageTarget

class RadioMessageTarget : public RadioMessageState
{
	typedef RadioMessageState base;

protected:
	TargetToFire _target;
	bool _engage;
	bool _fire;
  bool _cancelTarget;

public:
	RadioMessageTarget(
		AIGroup *from, OLinkPermNOArray(AIUnit) &to, TargetToFire target,
		bool engage, bool fire, bool cancelTarget
	);
	RadioMessageTarget();
	RadioMessageState *Clone() const {return new RadioMessageTarget(*this);}
	LSError Serialize(ParamArchive &ar);
	void Transmitted(ChatChannel channel);
	void Canceled();
	int GetType() const {return RMTTarget;}
	TargetToFire GetTarget() const {return _target;}
	const char *GetPriorityClass();

	bool GetEngage() const {return _engage;}
	bool GetFire() const {return _fire;}
  bool GetCancelTarget() const {return _cancelTarget;}

	void SetEngage(bool val) {_engage=val;}
	void SetFire(bool val) {_fire=val;}
	
protected:
  RString PrepareSentence(SentenceParams &params) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageTeam

class RadioMessageTeam : public RadioMessage
{
protected:
	OLinkPerm<AIGroup> _from;
	OLinkPermNOArray(AIUnit) _to;
	Team _team;

public:
	RadioMessageTeam(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Team team);
	RadioMessageTeam() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTTeam;}
	AIGroup *GetFrom() const {return _from;}
	bool IsTo(AIBrain *unit) const;
	Team GetTeam() const {return _team;}
	AIBrain *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

//////////////////////////////////////////////////////////////////////////////
// class RadioMessageCommand

class RadioMessageCommand : public RadioMessageWithMove
{
protected:
	OLinkPerm<AIGroup> _from;
	OLinkPermNOArray(AIUnit) _to;
	Command _command;
	bool _toMainSubgroup;
	bool _transmit;

public:
	RadioMessageCommand(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Command &command, bool toMainSubgroup = false, bool transmit = true);
	RadioMessageCommand() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
	int GetType() const {return RMTCommand;}
	AIGroup *GetFrom() {return _from;}
	bool IsTo(AIBrain *unit) const;
	void DeleteTo(AIUnit *unit);
	bool IsToMainSubgroup() const {return _toMainSubgroup;}
	Command::Message GetMessage() {return _command._message;}
	Command::Context GetContext() {return _command._context;}
	AIBrain *GetSender() const {return _from ? _from->Leader() : NULL;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
  void PrepareSEHelperVariables(AIUnit *first) const;
  RString PrepareCmdMoveSentence(SentenceParams &params, AIUnit *first) const;
};

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageText

class RadioMessageText : public RadioMessage
{
protected:
	RString _wave;
	OLinkPerm<AIBrain> _sender;
	RString _senderName;
	float _timeToLive;

public:
	RadioMessageText(RString wave, RString senderName, float ttl=2.0) {_wave = wave; _senderName = senderName; _timeToLive=ttl;}
	RadioMessageText(RString wave, AIBrain *sender, float ttl=2.0) {_wave = wave; _sender = sender; _timeToLive=ttl;}
	RadioMessageText() {}
	LSError Serialize(ParamArchive &ar);
	const char *GetPriorityClass();
	void Transmitted(ChatChannel channel);
  /// Do not send text message over network - serve only as a local background
  virtual bool SendOverNetwork() const {return false;}
	RString GetWave();
	int GetType() const {return RMTText;}
	float GetDuration() const;
	AIBrain *GetSender() const {return _sender;}
	RString GetSenderName() const {return _senderName;}

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

#if _ENABLE_CONVERSATION

#include <RV/KBBase/kbCenter.hpp>

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageTalk

class RadioMessageTalk : public RadioMessage
{
protected:
  Ref<KBMessageInfo> _message;
  OLinkPerm<AIBrain> _sender;
  OLinkPerm<AIBrain> _receiver;
  float _timeToLive;

public:
  RadioMessageTalk(const KBMessageInfo *message, AIBrain *sender, AIBrain *receiver, float ttl = 2.0);
  RadioMessageTalk() {}
  ~RadioMessageTalk();
  
  int GetType() const {return RMTTalk;}
  AIBrain *GetSender() const {return _sender;}

  RString GetWave();
  RString GetText();
  void GetSpeech(RadioSentence &sentence, const RadioChannelParams &cfg);
  float GetDuration() const;

  void Transmitted(ChatChannel channel);

  LSError Serialize(ParamArchive &ar);
  const char *GetPriorityClass();

  bool IsConversation() const { return true; }

protected:
  RString PrepareSentence(SentenceParams &params) const {return RString();}
};

#endif // _ENABLE_CONVERSATION

#if _ENABLE_IDENTITIES

class RadioMessageTask : public RadioMessage, public CountInstances<RadioMessageTask>
{
protected:
  Ref<AITask> _task;
  GameValue _params;

public:
  RadioMessageTask(AITask *task, GameValuePar params)
  {_task = task; _params = params;}
  RadioMessageTask() {}
  LSError Serialize(ParamArchive &ar);
  const char *GetPriorityClass();
  void Transmitted(ChatChannel channel);
  int GetType() const {return RMTTask;}
  AIBrain *GetSender() const;

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageTaskReceived : public RadioMessage, public CountInstances<RadioMessageTaskReceived>
{
protected:
  Ref<AITask> _task;

public:
  RadioMessageTaskReceived(AITask *task)
  {_task = task;}
  RadioMessageTaskReceived() {}
  LSError Serialize(ParamArchive &ar);
  const char *GetPriorityClass();
  void Transmitted(ChatChannel channel);
  int GetType() const {return RMTTaskReceived;}
  AIBrain *GetSender() const;

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

class RadioMessageTaskResult : public RadioMessage, public CountInstances<RadioMessageTaskResult>
{
protected:
  Ref<AITask> _task;
  TaskState _state;
  GameValue _result;
  RString _sentence;

public:
  RadioMessageTaskResult(AITask *task, TaskState state, GameValue result, RString sentence)
  {_task = task; _state = state; _result = result; _sentence = sentence;}
  RadioMessageTaskResult() {}
  LSError Serialize(ParamArchive &ar);
  const char *GetPriorityClass();
  void Transmitted(ChatChannel channel);
  int GetType() const {return RMTTaskResult;}
  AIBrain *GetSender() const;

protected:
  RString PrepareSentence(SentenceParams &params) const;
};

#endif // _ENABLE_IDENTITIES

#endif


