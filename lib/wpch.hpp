#ifdef _MSC_VER
#pragma once
#endif

#ifndef _POSEIDON_LIB_WPCH_HPP
#define _POSEIDON_LIB_WPCH_HPP

// precompiled header files - many files use something of Windows API
#include <El/elementpch.hpp>

#include "appName.h"
// Registry key for saving player's name, IP address of server etc.
#define COMPANY_NAME "Bohemia Interactive Studio"
#define ConfigAppRoot "Software\\" COMPANY_NAME "\\"
#define ConfigApp ConfigAppRoot AppName


#if _VBS2
	#include "VBS2Config.h"
#elif _VBS1
	#include "VBS1Config.h"
#elif _VBS1_DEMO
	#include "VBS1DemoConfig.h"
#elif _SERVER
	#include "serverConfig.h"
#elif _DEMO && !defined _DEMO_INT
	#include "demoConfig.h"
#elif _LIBERATION
  #include "liberationConfig.h"
#elif _SUPER_RELEASE
  #include "retailConfig.h"
#elif _CONSOLE
	#include "consoleConfig.h"
#elif _DEDICATED_CLIENT
  #include "dedicatedClientConfig.h"
#elif _USE_AFX
	#include "afxConfig.h"
#else
	#include "normalConfig.h"
#endif

#ifdef _XBOX
	// some platform specific overrides, configuration independent
	#include "xboxConfig.h"
#elif _FP2 && !_VBS2
  #include "fp2Config.h"
#elif _BULDOZER
  #include "buldozerConfig.h"
#endif



// user general include
#include "types.hpp"
#include <El/Math/math3d.hpp>

// global diagnostics

void CCALL GlobalShowMessage( int timeMs, const char *msg, ... );

#endif
