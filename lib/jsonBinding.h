#ifdef _MSC_VER
#pragma once
#endif

#ifndef json_binding_hpp
#define json_binding_hpp

#include <Es/Strings/rString.hpp>
#include <Es/Memory/normalNew.hpp>
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/reader.h>
#include <rapidjson/stringbuffer.h>
#include <Es/Memory/debugNew.hpp>


#ifdef _WIN32
#define NOTHROW __declspec(nothrow)
#else
#define NOTHROW __attribute__((nothrow))
#endif

#include <memory>
using std::shared_ptr;

namespace rapidjson_binding
{
  struct DocumentValue
  {
    shared_ptr<rapidjson::Document> val;
    DocumentValue():val(std::make_shared<rapidjson::Document>())
    {
      val->SetObject();
    }
    rapidjson::Document &operator * () {return *val.get();}
    rapidjson::Document *operator -> () {return val.get();}

    rapidjson::Value &operator [] (const char *index){return (*val.get())[index];}

	  template <typename T>
	  void AddMember(const rapidjson::UTF8<>::Ch* name, T value) {val->AddMember(name, value, val->GetAllocator());}

	  void AddMemberString(const rapidjson::UTF8<>::Ch* name, const rapidjson::UTF8<>::Ch* value)
    {
      val->AddMember(name, 0, val->GetAllocator());
      (*val)[name].SetString(value, val->GetAllocator());
    }
  };
};

#endif
