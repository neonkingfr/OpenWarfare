#include "wpch.hpp"

#include "vehicle.hpp"
#include <El/ParamFile/paramFile.hpp>

#include "proxyWeapon.hpp"

#if _ENABLE_NEWHEAD

ProxySubpartType::ProxySubpartType(ParamEntryPar param)
:base(param)
{
}

void ProxySubpartType::Load(ParamEntryPar par, const char *shape)
{
  base::Load(par,shape);

  // set HW animation type
  // check if the skinning engine is powerfull enough
  if (GEngine->GetMaxBonesCount()>=30)
  {
    _shapeAnimated = AnimTypeHardware;
  }
}

void ProxySubpartType::InitShape()
{
  base::InitShape();
}


Object* ProxySubpartType::CreateObject(bool unused) const
{ 
  return new ProxySubpart(this, GetShape());
}


DEFINE_CASTING(ProxySubpart)

ProxySubpart::ProxySubpart(const EntityType *type, LODShapeWithShadow *shape)
:base(shape, type, CreateObjectId())
{
}

#endif

//////////////////////////////////////////////////////////////////////////

ProxyWeaponType::ProxyWeaponType( ParamEntryPar param, InventoryContainerId type)
: base(param), _inventoryId(type)
{
}

ProxyWeaponType::ProxyWeaponType(ParamEntryPar param)
: base(param), _inventoryId((InventoryContainerId)-1)
{
}


void ProxyWeaponType::Load(ParamEntryPar par, const char *shape)
{
  base::Load(par,shape);

  ConstParamEntryPtr entry = par.FindEntry("inventoryType");
  if (entry)
  {
    RStringB typeName = *entry;
    _inventoryId = GetEnumValue<InventoryContainerId>(typeName);

    if (_inventoryId == INT_MIN)
    {
      RptF("Inventory container proxy type '%s' is unknown.", cc_cast(typeName));
    }
  }
}

void ProxyWeaponType::InitShape()
{
  base::InitShape();
}


DEFINE_ENUM(InventoryContainerId, ICI, INVENTORY_ID_ENUM)

ProxyCrewType::ProxyCrewType(ParamEntryPar param)
:base(param)
{
}

void ProxyCrewType::Load(ParamEntryPar par, const char *shape)
{
	base::Load(par,shape);
	int pos = par>>"crewPosition";
	_pos = (CrewPosition)pos;

}

void ProxyCrewType::InitShape()
{
	base::InitShape();
}


Object* ProxyCrewType::CreateObject(bool unused) const
{ 
  return new ProxyCrew(this);
}


DEFINE_CASTING(ProxyCrew)

ProxyCrew::ProxyCrew(const EntityType *type)
:base(type->GetShape(),type,CreateObjectId())
{
}

