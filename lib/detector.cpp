#include "wpch.hpp"

#include "detector.hpp"
#include <El/Common/randomGen.hpp>
#include "AI/ai.hpp"
#include "person.hpp"
#include "global.hpp"
#include "landscape.hpp"
#include "world.hpp"
#include "fileLocator.hpp"

#include "dynSound.hpp"

#include "arcadeTemplate.hpp"

#include "gameStateExt.hpp"
#include "paramArchiveExt.hpp"

#include "Network/network.hpp"
//#include "strIncl.hpp"
#include "stringtableExt.hpp"

#include "UI/uiActions.hpp"
#include <Es/Common/delegate.hpp>

#include <El/Evaluator/express.hpp>

DEFINE_CASTING(Detector)

Detector::Detector(const EntityType *name, const CreateObjectId &id)
:Vehicle(name->GetShape(),name,id)
#if NON_AI_DETECTOR
 ,_vars(true)
#endif
{
  SetSimulationPrecision(0.5f);
  RandomizeSimulationTime();
  SetTimeOffset(0); // no need for any visual state interpolation, the object is invisible

  SetType(TypeVehicle);

#if _VBS3 // trigger area fix
  _swapped = false;
#endif
  _a = 50;
  _b = 50;
  _e = 0;
  _sinAngle = 0;
  _cosAngle = 1;
  _rectangular = false;

  _activationBy = ASANone;
  _activationType = ASATPresent;
  _repeating = true;
  _timeoutMin = 0;
  _timeoutMid = 0;
  _timeoutMax = 0;
  _interruptable = true;

  _action = ASTNone;

#if _VBS3 // trigger activate EH
  _onActivateCounter = 0;
#endif

  _text = "";

  _expCond = "this";
  _expActiv = "";
  _expDesactiv = "";

#if USE_PRECOMPILATION
  GGameState.CompileMultiple(_expCond, _compiledCond, GWorld->GetMissionNamespace()); // mission namespace
#endif

  _assignedStatic = ObjectId();
  _assignedVehicle = -1;

  _effects.Init();
  
  _nextCheck = Glob.time + 2 + GRandGen.RandomValue() * 2;
  _active = false;
  _activeCountdown = false;

  _vehicles = new GameValue();

/*
  GameArrayType array;
  _vehicles = new GameValue(array);
*/
}

Detector::~Detector()
{
}


int Detector::NVehicles() const
{
  if (_vehicles->GetType() != GameArray) return 0;
  GameArrayType &vehicles = *_vehicles;
  return vehicles.Size();
}

const EntityAI *Detector::GetVehicle(int i) const
{
  if (_vehicles->GetType() != GameArray) return NULL;

  GameArrayType &vehicles = *_vehicles;
  Object *obj = static_cast<const GameDataObject *>(vehicles[i].GetData())->GetObject();
  return static_cast<const EntityAI *>(obj);
}

EntityAI *Detector::GetVehicle(int i)
{
  if (_vehicles->GetType() != GameArray) return NULL;

  GameArrayType &vehicles = *_vehicles;
  Object *obj = static_cast<const GameDataObject *>(vehicles[i].GetData())->GetObject();
  return static_cast<EntityAI *>(obj);
}

void Detector::ReActivateSounds()
{
  if (_active)
  {
    if (_dynSound.IsNull() && _effects.soundDet.GetLength() > 0) 
    {
      _dynSound = new DynSoundObject(_effects.soundDet);
    }

    if (_voice.IsNull() && _effects.voice.GetLength() > 0 && _voiceObject.NotNull()) 
    {
      _voice = new SoundObject(_effects.voice, _voiceObject);
    }
  }
}

const GameValue &Detector::GetGameValue() const
{
  return *_vehicles;
}

LSError Detector::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.Serialize("a", _a, 1)) 
  CHECK(ar.Serialize("b", _b, 1)) 
  CHECK(ar.Serialize("sinAngle", _sinAngle, 1)) 
  CHECK(ar.Serialize("cosAngle", _cosAngle, 1, sqrt(1 - Square(_sinAngle))))  

  CHECK(ar.Serialize("rectangular", _rectangular, 1)) 
  CHECK(ar.SerializeEnum("activationBy", _activationBy, 1)) 
  CHECK(ar.SerializeEnum("activationType", _activationType, 1)) 
  CHECK(ar.Serialize("repeating", _repeating, 1)) 
  CHECK(ar.Serialize("timeoutMin", _timeoutMin, 1)) 
  CHECK(ar.Serialize("timeoutMid", _timeoutMid, 1)) 
  CHECK(ar.Serialize("timeoutMax", _timeoutMax, 1)) 
  CHECK(ar.Serialize("interruptable", _interruptable, 1)) 
  CHECK(ar.SerializeEnum("action", _action, 1)) 
  CHECK(ar.SerializeRef("assignedGroup", _assignedGroup, 1))  
  CHECK(_assignedStatic.Serialize(ar, "assignedStatic", 1)) 
  CHECK(ar.Serialize("assignedVehicle", _assignedVehicle, 1)) 
  CHECK(ar.Serialize("text", _text, 1)) 
  CHECK(ar.Serialize("expCond", _expCond, 1)) 
  CHECK(ar.Serialize("expActiv", _expActiv, 1)) 
  CHECK(ar.Serialize("expDesactiv", _expDesactiv, 1)) 
#if USE_PRECOMPILATION
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    GGameState.CompileMultiple(_expCond, _compiledCond, GWorld->GetMissionNamespace()); // mission namespace
    GGameState.CompileMultiple(_expActiv, _compiledActiv, GWorld->GetMissionNamespace()); // mission namespace
    GGameState.CompileMultiple(_expDesactiv, _compiledDesactiv, GWorld->GetMissionNamespace()); // mission namespace
  }
#endif

  CHECK(ar.SerializeArray("synchronizations", _synchronizations, 1))  

  ParamArchive arSubcls;
  if (!ar.OpenSubclass("Effects", arSubcls)) return LSStructure;
  CHECK(_effects.WorldSerialize(arSubcls))
  
  CHECK(::Serialize(ar, "countdown", _countdown, 1))  
  CHECK(ar.Serialize("active", _active, 1)) 
  CHECK(ar.Serialize("activeCountdown", _activeCountdown, 1)) 

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    _e = sqrt(Square(_a) - Square(_b));
    _nextCheck = Glob.time - 1.0;
    // will be created: _vehicles;
  }

  // _dynSound;
  // _voice;
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond)
  {
    ReActivateSounds();
  }

#if NON_AI_DETECTOR
  // variables
  {
    void *old = ar.GetParams();
    ar.SetParams(&GGameState);
    DoAssert(_vars.IsSerializationEnabled());
    LSError result = ar.Serialize("Variables", _vars._vars, 1);
    ar.SetParams(old);
    // class Variables can be missing in older saves
    if (result != LSOK && result != LSNoEntry) return result;
  }
#endif // otherwise, the _vars are implemented by the DetectorBase

  return LSOK;
}

#define DETECTOR_MSG_LIST(XX) \
  XX(Create, CreateDetector) \
  XX(UpdateGeneric, UpdateDetector)

DEFINE_NETWORK_OBJECT(Detector, base, DETECTOR_MSG_LIST)

struct NetworkMessageEffects;

#define CREATE_DETECTOR_MSG(MessageName, XX) \
  XX(MessageName, float, a, NDTFloat, float, NCTNone, DEFVALUE(float, 50), DOC_MSG("Trigger radius"), TRANSF) \
  XX(MessageName, float, b, NDTFloat, float, NCTNone, DEFVALUE(float, 50), DOC_MSG("Trigger radius"), TRANSF) \
  XX(MessageName, float, sinAngle, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Rotation"), TRANSF) \
  XX(MessageName, float, cosAngle, NDTFloat, float, NCTNone, DEFVALUE(float, 1), DOC_MSG("Rotation"), TRANSF) \
  XX(MessageName, bool, rectangular, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Rectangular / elliptic trigger"), TRANSF) \
  XX(MessageName, int, activationBy, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, ASANone), DOC_MSG("Who is activating trigger"), TRANSF) \
  XX(MessageName, int, activationType, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, ASATPresent), DOC_MSG("How trigger is activated"), TRANSF) \
  XX(MessageName, bool, repeating, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Can be activated repeatedly"), TRANSF) \
  XX(MessageName, float, timeoutMin, NDTFloat, float, NCTNone, DEFVALUE(float, 50), DOC_MSG("Trigger timeout"), TRANSF) \
  XX(MessageName, float, timeoutMid, NDTFloat, float, NCTNone, DEFVALUE(float, 50), DOC_MSG("Trigger timeout"), TRANSF) \
  XX(MessageName, float, timeoutMax, NDTFloat, float, NCTNone, DEFVALUE(float, 50), DOC_MSG("Trigger timeout"), TRANSF) \
  XX(MessageName, bool, interruptable, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Timeout is interruptable"), TRANSF) \
  XX(MessageName, int, action, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, ASTNone), DOC_MSG("Action performed when trigger is activated"), TRANSF) \
  XX(MessageName, int, assignedStatic, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Attached static object"), TRANSF) \
  XX(MessageName, int, assignedVehicle, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Attached vehicle"), TRANSF) \
  XX(MessageName, RString, text, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Trigger description"), TRANSF) \
  XX(MessageName, RString, expCond, NDTString, RString, NCTNone, DEFVALUE(RString, "this"), DOC_MSG("Condition for activation of trigger"), TRANSF) \
  XX(MessageName, RString, expActiv, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Statement, processed when trigger is activated"), TRANSF) \
  XX(MessageName, RString, expDesactiv, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Statement, processed when trigger is deactivated"), TRANSF) \
  XX(MessageName, AutoArray<int>, synchronizations, NDTIntArray, AutoArray<int>, NCTSmallUnsigned, DEFVALUEINTARRAY, DOC_MSG("List of synchronizations"), TRANSF) \
  XX(MessageName, ArcadeEffects, effects, NDTObject, REF_MSG(NetworkMessageEffects), NCTNone, DEFVALUE_MSG(NMTEffects), DOC_MSG("Camera and title effects"), TRANSF_OBJECT)

DECLARE_NET_INDICES_EX_SET_DIAG_NAME(CreateDetector, CreateVehicle, CREATE_DETECTOR_MSG,"detector")
DEFINE_NET_INDICES_EX(CreateDetector, CreateVehicle, CREATE_DETECTOR_MSG)

#define UPDATE_DETECTOR_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), assignedGroup, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Attached group"), TRANSF_REF)

DECLARE_NET_INDICES_EX(UpdateDetector, UpdateVehicle, UPDATE_DETECTOR_MSG)
DEFINE_NET_INDICES_EX(UpdateDetector, UpdateVehicle, UPDATE_DETECTOR_MSG)

NetworkMessageFormat &Detector::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  switch (cls)
  {
  case NMCCreate:
    base::CreateFormat(cls, format);
    CREATE_DETECTOR_MSG(CreateDetector, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    base::CreateFormat(cls, format);
    UPDATE_DETECTOR_MSG(UpdateDetector, MSG_FORMAT)
    break;
  default:
    base::CreateFormat(cls, format);
    break;
  }
  return format;
}

Detector *Detector::CreateObject(NetworkMessageContext &ctx)
{
  base *veh = base::CreateObject(ctx);
  Detector *det = dyn_cast<Detector>(veh);
  if (!det) return NULL;
  sensorsMap.Add(det);
  det->TransferMsg(ctx);
  return det;
}

void Detector::DestroyObject()
{
  for (int i=0; i<sensorsMap.Size(); i++)
  {
    if (sensorsMap[i] == this)
    {
      sensorsMap.Delete(i);
      break;
    }
  }
  base::DestroyObject();
}

/*!
\patch 1.36 Date 12/14/2001 by Jirka
- Fixed: MP group assigned to detector was not transferred over network
*/
TMError Detector::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    if (ctx.IsSending())
    {
      TMCHECK(base::TransferMsg(ctx))
    }
    {
      PREPARE_TRANSFER(CreateDetector)

      TRANSF(a)
      TRANSF(b)
      TRANSF(sinAngle)
      TRANSF(cosAngle)

      TRANSF(rectangular)
      TRANSF_ENUM(activationBy)
      TRANSF_ENUM(activationType)
      TRANSF(repeating)
      TRANSF(timeoutMin)
      TRANSF(timeoutMid)
      TRANSF(timeoutMax)
      TRANSF(interruptable)
      TRANSF_ENUM(action)

      if (ctx.IsSending())
      {
        int encoded = _assignedStatic.Encode();
        TRANSF_EX(assignedStatic, encoded)
      }
      else
      {
        int encoded = -1;
        TRANSF_EX(assignedStatic, encoded)
        _assignedStatic.Decode(encoded);
      }

      //ITRANSF(assignedStatic)
      TRANSF(assignedVehicle)
      TRANSF(text)
      TRANSF(expCond)
      TRANSF(expActiv)
      TRANSF(expDesactiv)
      TRANSF(synchronizations)
      TRANSF_OBJECT(effects)
      if (!ctx.IsSending())
      {
        _e = sqrt(Square(_a) - Square(_b));
        // FIX: cosAngle must be transferred, sign is wrong if calculated
        // _cosAngle = sqrt(1 - Square(_sinAngle));
        _nextCheck = Glob.time - 1.0;
        // will be created: _vehicles;
        for (int i=0; i<_synchronizations.Size(); i++)
        {
          int sync = _synchronizations[i];
          Assert(sync >= 0);
          if (sync >= synchronized.Size())
            synchronized.Resize(sync + 1);
          synchronized[sync].Add(this);
        }

#if USE_PRECOMPILATION
        GGameState.CompileMultiple(_expCond, _compiledCond, GWorld->GetMissionNamespace()); // mission namespace
        GGameState.CompileMultiple(_expActiv, _compiledActiv, GWorld->GetMissionNamespace()); // mission namespace
        GGameState.CompileMultiple(_expDesactiv, _compiledDesactiv, GWorld->GetMissionNamespace()); // mission namespace
#endif
      }
    }
    break;
  case NMCUpdateGeneric:
    {
      TMCHECK(base::TransferMsg(ctx))
  
      PREPARE_TRANSFER(UpdateDetector)
  
      TRANSF_REF(assignedGroup)

      // TODO: ? _active, _vehicles
    }
    break;
  default:
    TMCHECK(base::TransferMsg(ctx))
    break;
  }
  return TMOK;
}

float Detector::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = 0;
  switch (ctx.GetClass())
  {
  case NMCUpdateGeneric:
    {
      error += base::CalculateError(ctx);
      // TODO: ? _active, _vehicles
    }
    break;
  default:
    error += base::CalculateError(ctx);
    break;
  }
  return error;
}

void Detector::SetArea(float a, float b, float angle, bool rectangular)
{
  _a = a;
  _b = b;
#if _VBS2 // trigger get area
  _angle = angle;
#endif
  float radAngle = HDegree(angle);
  if (_a < _b)
  {
#if _VBS3 // trigger area fix
    _swapped = true;
#endif
    swap(_a, _b); 
    radAngle += 0.5 * H_PI;
  }
  _e = sqrt(Square(_a) - Square(_b));
  _sinAngle = sin(radAngle);
  _cosAngle = cos(radAngle);
  _rectangular = rectangular;
}

void Detector::SetTriggerType(ArcadeSensorType type)
{
  _action = type;
}

void Detector::SetTimeout(float min, float mid, float max, bool interruptable)
{
  _timeoutMin = min;
  _timeoutMid = mid;
  _timeoutMax = max;
  _interruptable = interruptable;
}

void Detector::SetStatements(RString cond, RString activ, RString desactiv)
{
  _expCond = cond;
  _expActiv = activ;
  _expDesactiv = desactiv;
#if USE_PRECOMPILATION
  GGameState.CompileMultiple(_expCond, _compiledCond, GWorld->GetMissionNamespace()); // mission namespace
  GGameState.CompileMultiple(_expActiv, _compiledActiv, GWorld->GetMissionNamespace()); // mission namespace
  GGameState.CompileMultiple(_expDesactiv, _compiledDesactiv, GWorld->GetMissionNamespace()); // mission namespace
#endif
}

#if _VBS3 // quick attach objects to trigger
void Detector::AttachObject(Object *obj)
{
  _attachedObjects.AddUnique(obj);
}

void Detector::DetachObject(Object *obj)
{
// TODO: check
//  _attachedObjects.DeleteAllKey(*obj);
}
#endif

void Detector::AttachVehicle(EntityAI *vehicle)
{
  if (_activationBy == ASAGroup || _activationBy == ASALeader || _activationBy == ASAMember)
  {
    AIBrain *unit = vehicle ? vehicle->CommanderUnit() : NULL;
    AIGroup *group = unit ? unit->GetGroup() : NULL;
    _assignedGroup = group;
    _assignedStatic = ObjectId();
    _assignedVehicle = -1;
    if (!group) _activationBy = ASANone;
  }
  else
  {
    int id = -1;
    if (vehicle) for (int i=0; i<vehiclesMap.Size(); i++)
    {
      if (vehiclesMap[i] == vehicle)
      {
        id = i;
        break;
      }
    }
    _assignedGroup = NULL;
    _assignedStatic = ObjectId();
    _assignedVehicle = id;
    if (_activationBy == ASAVehicle)
    {
      if (id == -1) _activationBy = ASANone;
    }
    else
    {
      if (id >= 0) _activationBy = ASAVehicle;
    }
  }
}

EntityAI *Detector::GetAttachedVehicle() const
{
  if (_activationBy != ASAVehicle) return NULL;
  if (_assignedVehicle < 0 || _assignedVehicle >= vehiclesMap.Size()) return NULL;
  return vehiclesMap[_assignedVehicle];
}

void Detector::SetActivation(ArcadeSensorActivation by, ArcadeSensorActivationType type, bool repeating)
{
  if (by == ASAGroup || by == ASALeader || by == ASAMember)
  {
    if (_assignedGroup)
    {
      _activationBy = by;
    }
    else if (_assignedVehicle >= 0)
    {
      EntityAI *veh = vehiclesMap[_assignedVehicle];
      AIBrain *unit = veh ? veh->CommanderUnit() : NULL;
      AIGroup *grp = unit ? unit->GetGroup() : NULL;
      if (grp)
      {
        _assignedGroup = grp;
        _assignedStatic = ObjectId();
        _assignedVehicle = -1;
        _activationBy = by;
      }
    }
  }
  else if (by == ASAVehicle)
  {
    if (_assignedVehicle >= 0)
    {
      _activationBy = by;
    }
    else if (_assignedGroup)
    {
      // attach leader
      AIUnit *unit = _assignedGroup->Leader();
      EntityAI *veh = unit ? unit->GetVehicle() : NULL;
      int id = -1;
      if (veh) for (int i=0; i<vehiclesMap.Size(); i++)
      {
        if (vehiclesMap[i] == veh)
        {
          id = i;
          break;
        }
      }
      if (id >= 0)
      {
        _assignedGroup = NULL;
        _assignedStatic = ObjectId();
        _assignedVehicle = id;
        _activationBy = by;
      }
    }
  }
  else
  {
    _assignedGroup = NULL;
    _assignedStatic = ObjectId();
    _assignedVehicle = -1;
    _activationBy = by;
  }
  _activationType = type;
  _repeating = repeating;
}

void Detector::FromTemplate(const ArcadeSensorInfo &info)
{
  SetArea(info.a, info.b, info.angle, info.rectangular);

  _activationBy = info.activationBy;
  _activationType = info.activationType;
  _repeating = info.repeating;
  _timeoutMin = info.timeoutMin;
  _timeoutMid = info.timeoutMid;
  _timeoutMax = info.timeoutMax;
  _interruptable = info.interruptable;
  _action = info.type;

  if (!info.idObject.IsNull()) AssignStatic(info.idObject);
  if (info.idVehicle >= 0)
  {
    if (_activationBy == ASAVehicle)
      AssignVehicle(info.idVehicle);
    else
    {
      EntityAI *veh = dyn_cast<EntityAI>(vehiclesMap[info.idVehicle].GetLink());
      AIBrain *unit = veh ? veh->CommanderUnit() : NULL;
      AIGroup *grp = unit ? unit->GetGroup() : NULL;
      AssignGroup(grp);
    }
  }

  _text = info.text;
  _expCond = info.expCond;
  _expActiv = info.expActiv;
  _expDesactiv = info.expDesactiv;
#if USE_PRECOMPILATION
  GGameState.CompileMultiple(_expCond, _compiledCond, GWorld->GetMissionNamespace()); // mission namespace
  GGameState.CompileMultiple(_expActiv, _compiledActiv, GWorld->GetMissionNamespace()); // mission namespace
  GGameState.CompileMultiple(_expDesactiv, _compiledDesactiv, GWorld->GetMissionNamespace()); // mission namespace
#endif

  _synchronizations = info.synchronizations;
  _effects = info.effects;
}

void Detector::AssignGroup(AIGroup *group)
{
  _assignedGroup = group;
  _assignedStatic = ObjectId();
  _assignedVehicle = -1;
  Assert
  (
    _activationBy == ASAGroup ||
    _activationBy == ASALeader ||
    _activationBy == ASAMember
  );
}

void Detector::AssignStatic(const ObjectId &id)
{
  _assignedGroup = NULL;
  _assignedStatic = id;
  _assignedVehicle = -1;
  _activationBy = ASAStatic;
}

void Detector::AssignVehicle(int id)
{
  _assignedGroup = NULL;
  _assignedStatic = ObjectId();
  _assignedVehicle = id;
  Assert(_activationBy == ASAVehicle);
}

float Detector::GetCountdown()
{
  return _countdown - Glob.time;
}

bool Detector::SimulationReady(SimulationImportance prec) const
{
  /// trigger is not depended on the landscape around it
  return true;
}

void Detector::Simulate(float deltaT, SimulationImportance prec)
{
  if (_activeCountdown)
  {
    if (Glob.time >= _countdown)
    {
      _activeCountdown = false;
      OnActivate(GetActiveVehicle());
    }
  }

  Scan();

  if (_dynSound) _dynSound->Simulate(this,deltaT,prec);
  if (_voice)
  {
    if (!_voice->Simulate(_voiceObject, deltaT, prec)) _voice = NULL;
  }
  if (_sound)
  {
    if (!_sound->Simulate(NULL, deltaT, prec)) _sound = NULL;
  }
  
  base::Simulate(deltaT, prec);

  //SimulatePost(deltaT, prec);
}

bool Detector::TestSide(EntityAI *vehicle)
{
  if(vehicle && (vehicle->GetType()->_laserTarget || vehicle->GetType()->_artilleryTarget)) return false;

  switch (_activationBy)
  {
    case ASAAnybody:
    // for "Seized by ..." triggers we need to collect all vehicles
    case ASAEastSeized:
    case ASAWestSeized:
    case ASAGuerrilaSeized:
      return true;
    case ASAEast:
      return vehicle->GetTargetSide() == TEast;
    case ASAWest:
      return vehicle->GetTargetSide() == TWest;
    case ASAGuerrila:
      return vehicle->GetTargetSide() == TGuerrila;
    case ASACivilian:
      return vehicle->GetTargetSide() == TCivilian;
    case ASALogic:
      return vehicle->GetTargetSide() == TLogic;
    default:
      Fail("Activation");
      return false;
  }
}

bool Detector::TestSide(const AITargetInfo &target)
{
  TargetSide side = target._destroyed ? TCivilian : target._side;
  switch (_activationBy)
  {
    case ASAAnybody:
    // for "Seized by ..." triggers we need to collect all vehicles
    case ASAEastSeized:
    case ASAWestSeized:
    case ASAGuerrilaSeized:
      return true;
    case ASAEast:
      return side == TEast;
    case ASAWest:
      return side == TWest;
    case ASAGuerrila:
      return side == TGuerrila;
    case ASACivilian:
      return side == TCivilian;
    case ASALogic:
      return side == TLogic;
    default:
      Fail("Activation");
      return false;
  }
}

AICenter *Detector::GetCenter() const
{
  switch (_activationType)
  {
    case ASATWestDetected: // detected by West
      return GWorld->GetWestCenter();
    case ASATEastDetected: // detected by East
      return GWorld->GetEastCenter();
    case ASATGuerrilaDetected: // detected by Guerrila
      return GWorld->GetGuerrilaCenter();
    case ASATCiviliansDetected: // detected by Civilians
      return GWorld->GetCivilianCenter();
  }
  return NULL;
}

bool Detector::IsInside(Vector3Par pos, Vector3Par f1, Vector3Par f2)
{
  if (_rectangular)
  {
    Vector3 b = FutureVisualState().Position();
    Vector3 e = f1 - b;
    Vector3 p = pos - b;
    float eSize2 = e.SquareSize();
    if (eSize2==0) return false;
    float t = (e * p) / eSize2;
    if (t < -1.0 || t > 1.0) return false;
    
    e = f2 - b;
    eSize2 = e.SquareSize();
    if (eSize2==0) return false;
    t = (e * p) / eSize2;
    if (t < -1.0 || t > 1.0) return false;

    return true;
  }
  else
  {
    float p = (pos - f1).SizeXZ() + (pos - f2).SizeXZ();
    return p <= 2.0 * _a;
  }
}

bool Detector::TestVehicle(Vehicle *veh, Vector3Par f1, Vector3Par f2)
{
  if (veh && !veh->IsDamageDestroyed())
  {
    Vector3Val pos = veh->WorldTransform(veh->FutureVisualState()).Position();
    return IsInside(pos, f1, f2);
  }
  return false;
}

bool Detector::TestVehicle(AICenter *center, Vehicle *veh, Vector3Par f1, Vector3Par f2)
{
  for (int i=0; i<center->NTargets(); i++)
  {
    const AITargetInfo &target = center->GetTarget(i);
    if (target._idExact == veh)
    {
      return target.FadingPositionAccuracy() >= 0.1  && IsInside(target._realPos, f1, f2);
    }
  }
  return false;
}

/// helper functor for Detector::Scan
class DetectorScanOneEntity
{
  Detector *_obj;
  Vector3 _f1, _f2;
  GameArrayType &_vehicles;
  
  public:
  DetectorScanOneEntity(Detector *obj, Vector3Par f1, Vector3Par f2, GameArrayType &vehicles)
  :_obj(obj),_f1(f1),_f2(f2),_vehicles(vehicles)
  {
  }
  bool operator () (Entity *entity) const
  {
    if (entity->IsDamageDestroyed()) return false;
    EntityAI *veh = dyn_cast<EntityAI>(entity);
    if (!veh) return false;
    if (veh->GetType()->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic))) return false;
    if (!_obj->TestSide(veh)) return false;
    Vector3Val pos = veh->WorldTransform(veh->FutureVisualState()).Position();
    if (_obj->IsInside(pos, _f1, _f2))
    {
      _vehicles.Add(GameValueExt(veh));
    }
    return false;
  }
};

/*!
\patch 5124 Date 1/26/2007 by Jirka
- New: New trigger types "Seized by ..."
*/

void Detector::Scan()
{
  Vector3 f1, f2;
  if (_rectangular)
  {
    f1 = FutureVisualState().Position()
      + Vector3(+_cosAngle * _a, 0, -_sinAngle * _a);
    f2 = FutureVisualState().Position()
      + Vector3(+_sinAngle * _b, 0, +_cosAngle * _b);
  }
  else
  {
    Vector3 diff(+_cosAngle * _e, 0, -_sinAngle * _e);
    f1 = FutureVisualState().Position() - diff;
    f2 = FutureVisualState().Position() + diff;
  }

  if (_vehicles->GetType() != GameArray)
  {
    GameArrayType array;
    // create value as read-only: scripting cannot modify it
    _vehicles = new GameValue(array);
  }
  Assert(_vehicles->GetType() == GameArray);
  _vehicles->SetReadOnly(true);
  bool active = false;
  GameArrayType &vehicles = *_vehicles;

  const float ruleCoef = 1.5f;
  float friendlyPower = 0;
  float enemyPower = 0;

  vehicles.Resize(0);
  switch (_activationBy)
  {
    case ASANone:
      break;
    case ASAAlpha:
    case ASABravo:
    case ASACharlie:
    case ASADelta:
    case ASAEcho:
    case ASAFoxtrot:
    case ASAGolf:
    case ASAHotel:
    case ASAIndia:
    case ASAJuliet:
      return;
    case ASAEast:
    case ASAWest:
    case ASAGuerrila:
    case ASACivilian:
    case ASALogic:
    case ASAAnybody:
      if (_activationType <= ASATNotPresent)
      {
        // present / not present
        DetectorScanOneEntity scanOne(this,f1,f2,vehicles);
        GWorld->ForEachVehicle(scanOne);

        if (_activationType == ASATPresent)
        {
          // present
          active = vehicles.Size() > 0;
        }
        else
        {
          // not present
          active = vehicles.Size() == 0;
        }
      }
      else
      {
        AICenter *center = GetCenter();
        if (!center) return;
        for (int i=0; i<center->NTargets(); i++)
        {
          const AITargetInfo &target = center->GetTarget(i);
          if (target._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic))) continue;
          if (!TestSide(target)) continue;
          if (target.FadingPositionAccuracy() < 0.1) continue;
          if (IsInside(target._realPos, f1, f2))
          {
            // check if target is really there
            // check if target was detected recently
            if (target._time<Glob.time-100)
            {
              // target is not recent
              continue;
            }
            vehicles.Add(GameValueExt(target._idExact.GetLink()));
          }
        }
        active = vehicles.Size() > 0;
      }
      break;
    case ASAEastSeized:
    case ASAWestSeized:
    case ASAGuerrilaSeized:
      {
        // for which center the ruling will be calculated
        AICenter *myCenter = NULL;
        if (_activationBy == ASAEastSeized) myCenter = GWorld->GetEastCenter();
        else if (_activationBy == ASAWestSeized) myCenter = GWorld->GetWestCenter();
        else myCenter = GWorld->GetGuerrilaCenter();
        if (!myCenter) break;

        const float avgFireDistance = 100.0f; // used as an argument for GetDamagePerMinute

        bool enemyFired = false;

        if (_activationType <= ASATNotPresent)
        {
          // present / not present
          DetectorScanOneEntity scanOne(this,f1,f2,vehicles);
          GWorld->ForEachVehicle(scanOne);

          // calculate the strength of my and enemy sides
          for (int i=0; i<vehicles.Size(); i++)
          {
            // we know what we added to vehicles
            Assert(vehicles[i].GetType() == GameObject);
            Object *obj = static_cast<GameDataObject *>(vehicles[i].GetData())->GetObject();
            EntityAI *veh = static_cast<EntityAI *>(obj);
            TargetSide side = veh->GetTargetSide();
            // when target is empty or civilian, it should not be considered in the balance evaluation
            if (side==TCivilian) continue;
            if (myCenter->IsFriendly(side))
            {
              Threat t = veh->GetDamagePerMinute(avgFireDistance, 1.0f);
              friendlyPower += t[VSoft] + t[VArmor] + t[VAir];
            }
            else if (myCenter->IsEnemy(side))
            {
              Threat t = veh->GetDamagePerMinute(avgFireDistance, 1.0f);
              enemyPower += t[VSoft] + t[VArmor] + t[VAir];
              if (Glob.time - veh->GetLastShotTime() < 10.0f) enemyFired = true;
            }
          }
        }
        else
        {
          // detected by ...
          // which center database will be used
          AICenter *center = GetCenter();
          if (!center) return;
          for (int i=0; i<center->NTargets(); i++)
          {
            const AITargetInfo &target = center->GetTarget(i);
            if (target._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic))) continue;
            // check if we know target position good enough
            if (target.FadingPositionAccuracy() < 0.1) continue;
            // check if target was detected recently
            if (target._time < Glob.time - 100) continue;
            // check if target is really there
            if (IsInside(target._realPos, f1, f2))
            {
              TargetType *veh = target._idExact;
              vehicles.Add(GameValueExt(veh));
              TargetSide side = target._side;
              // when target is empty or civilian, it should not be considered in the balance evaluation
              if (side==TCivilian) continue;
              if (myCenter->IsFriendly(side))
              {
                Threat t = target._type->GetDamagePerMinute(avgFireDistance, 1.0f);
                friendlyPower += t[VSoft] + t[VArmor] + t[VAir];
              }
              else if (myCenter->IsEnemy(side))
              {
                Threat t = target._type->GetDamagePerMinute(avgFireDistance, 1.0f);
                enemyPower += t[VSoft] + t[VArmor] + t[VAir];
                if (Glob.time - veh->GetLastShotTime() < 10.0f) enemyFired = true;
              }
            }
          }
        }

        if (enemyFired) friendlyPower = 0;
        active = friendlyPower > ruleCoef * enemyPower;
        if (_activationType == ASATNotPresent)
        {
          active = !active;
          // when activated, keep friendlyPower / enemyPower >= ruleCoef even for opposite meaning of friendly and enemy
          friendlyPower *= Square(ruleCoef);
          swap(friendlyPower, enemyPower);
        }
      }
      break;
    case ASAStatic:
      if (_assignedStatic.IsNull()) return;
      {
        OLink(Object) obj = GLandscape->GetObject(_assignedStatic);
        if (_activationType <= ASATNotPresent)
        {
          EntityAI *veh = dyn_cast<EntityAI>(obj.GetLink());
          if (TestVehicle(veh, f1, f2))
            vehicles.Add(GameValueExt(veh));
          if (_activationType == ASATPresent)
          {
            // present
            active = vehicles.Size() > 0;
          }
          else
          {
            // not present
            active = vehicles.Size() == 0;
          }
        }
        else
        {
          if (obj)
          {
            AICenter *center = GetCenter();
            if (!center) return;
            EntityAI *veh = dyn_cast<EntityAI>(obj.GetLink());
            if (TestVehicle(center, veh, f1, f2))
              vehicles.Add(GameValueExt(veh));
          }
          active = vehicles.Size() > 0;
        }
      }
      break;
    case ASAVehicle:
      if (_assignedVehicle < 0) return;
      if (_assignedVehicle >= vehiclesMap.Size()) return;
      {
        Vehicle *veh = vehiclesMap[_assignedVehicle];
        if (_activationType <= ASATNotPresent)
        {
          if (TestVehicle(veh, f1, f2))
          {
            vehicles.Add(GameValueExt(veh));
          }
          if (_activationType == ASATPresent)
          {
            // present
            active = vehicles.Size() > 0;
          }
          else
          {
            // not present
            active = vehicles.Size() == 0;
          }
        }
        else
        {
          if (veh)
          {
            AICenter *center = GetCenter();
            if (!center) return;
            if (TestVehicle(center, veh, f1, f2))
              vehicles.Add(GameValueExt(veh));
          }
          active = vehicles.Size() > 0;
        }
      }
      break;
    case ASAGroup:
      if (_activationType <= ASATNotPresent)
      {
        bool ok = true;
        if (_assignedGroup && _assignedGroup->UnitsCount() > 0)
        {
          for (int i=0; i<_assignedGroup->NUnits(); i++)
          {
            AIUnit *unit = _assignedGroup->GetUnit(i);
//            if (!unit || !unit->IsUnit()) continue;
            if (!unit) continue;
            EntityAI *veh = unit->GetVehicle();
            if (TestVehicle(veh, f1, f2))
            {
              bool found = false;
              for (int j=0; j<NVehicles(); j++)
              {
                if (GetVehicle(j) == veh)
                {
                  found = true;
                  break;
                }
              }
              if (!found) vehicles.Add(GameValueExt(veh));
            }
            else
            {
              ok = false; // this vehicle is outside
            }
          }
        }
        else
        {
          ok = false;
        }
        if (_activationType == ASATPresent)
        {
          // present
          active = ok;
        }
        else
        {
          // not present
          active = !ok;
        }
      }
      else
      {
        active = true;
        if (_assignedGroup)
        {
          AICenter *center = GetCenter();
          if (!center) return;
          for (int i=0; i<_assignedGroup->NUnits(); i++)
          {
            AIUnit *unit = _assignedGroup->GetUnit(i);
//            if (!unit || !unit->IsUnit()) continue;
            if (!unit) continue;
            EntityAI *veh = unit->GetVehicle();
            if (TestVehicle(center, veh, f1, f2))
            {
              bool found = false;
              for (int j=0; j<NVehicles(); j++)
              {
                if (GetVehicle(j) == veh)
                {
                  found = true;
                  break;
                }
              }
              if (!found) vehicles.Add(GameValueExt(veh));
            }
            else
            {
              active = false; // this vehicle is outside
            }
          }
        }
        else
        {
          active = false;
        }
      }
      break;
    case ASALeader:
      {
        AIUnit *leader = _assignedGroup ? _assignedGroup->Leader() : NULL;
        EntityAI *vehLeader = leader ? leader->GetVehicle() : NULL;
        if (_activationType <= ASATNotPresent)
        {
          if (TestVehicle(vehLeader, f1, f2))
          {
            vehicles.Add(GameValueExt(vehLeader));
          }
          if (_activationType == ASATPresent)
          {
            // present
            active = vehicles.Size() > 0;
          }
          else
          {
            // not present
            active = vehicles.Size() == 0;
          }
        }
        else
        {
          if (vehLeader)
          {
            AICenter *center = GetCenter();
            if (!center) return;
            if (TestVehicle(center, vehLeader, f1, f2))
              vehicles.Add(GameValueExt(vehLeader));
          }
          active = vehicles.Size() > 0;
        }
      }
      break;
    case ASAMember:
      if (_activationType <= ASATNotPresent)
      {
        if (_assignedGroup)
        {
          for (int i=0; i<_assignedGroup->NUnits(); i++)
          {
            AIUnit *unit = _assignedGroup->GetUnit(i);
//            if (!unit || !unit->IsUnit()) continue;
            if (!unit) continue;
            EntityAI *veh = unit->GetVehicle();
            if (TestVehicle(veh, f1, f2))
            {
              bool found = false;
              for (int j=0; j<NVehicles(); j++)
              {
                if (GetVehicle(j) == veh)
                {
                  found = true;
                  break;
                }
              }
              if (!found) vehicles.Add(GameValueExt(veh));
            }
          }
        }
        if (_activationType == ASATPresent)
        {
          // present
          active = vehicles.Size() > 0;
        }
        else
        {
          // not present
          active = vehicles.Size() == 0;
        }
      }
      else
      {
        if (_assignedGroup)
        {
          AICenter *center = GetCenter();
          if (!center) return;
          for (int i=0; i<_assignedGroup->NUnits(); i++)
          {
            AIUnit *unit = _assignedGroup->GetUnit(i);
//            if (!unit || !unit->IsUnit()) continue;
            if (!unit) continue;
            EntityAI *veh = unit->GetVehicle();
            if (TestVehicle(center, veh, f1, f2))
            {
              bool found = false;
              for (int j=0; j<NVehicles(); j++)
              {
                if (GetVehicle(j) == veh)
                {
                  found = true;
                  break;
                }
              }
              if (!found) vehicles.Add(GameValueExt(veh));
            }
          }
        }
        active = vehicles.Size() > 0;
      }
      break;
  }

#if _VBS3 // quick attach objects to trigger
  // only activate when all attached objects are in the trigger also, at the same time
  if (active)
  {
    int nActive = 0;
    for (int i=0; i<_attachedObjects.Size(); i++)
    {
      Object *obj = _attachedObjects[i];
      if (obj && !obj->IsDamageDestroyed())
      {
        Vector3Val pos = obj->WorldTransform(obj->FutureVisualState()).Position();
        if (IsInside(pos, f1, f2)) nActive++;
      }
      else
        _attachedObjects.Delete(i--);
    }
    active = nActive == _attachedObjects.Size();
  }
#endif

  // optimize common case: expression=="this"
  if( strcmpi(_expCond,"this")!=0 ) // condition field != "this" ?
  {
    GameState *state = GWorld->GetGameState();
    GameVarSpace local(state->GetContext(), false);
    state->BeginContext(&local);

    state->VarSetLocal("this", active, true, true);
    state->VarSetLocal("thisList", GetGameValue(), true, true);
    state->VarSetLocal("thisTrigger", GameValueExt(this), true, true);
#if USE_PRECOMPILATION
    active = state->EvaluateBool(_expCond, _compiledCond, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#else
    active = state->EvaluateBool(_expCond, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#endif

    state->EndContext();
  }

  // activate
  if (active && !IsActive())
  {
    if (_timeoutMax < 0.1)
    {
      OnActivate(GetActiveVehicle());
    }
    else if (!_activeCountdown)
    {
      _activeCountdown = true;
      // for triggers "Seized by ..." use the ruling power for timeout calculation
      if (_activationBy == ASAEastSeized || _activationBy == ASAWestSeized || _activationBy == ASAGuerrilaSeized)
      {
        float timeout = 0;
        if (enemyPower <= 0) timeout = _timeoutMin; // infinite ruling
        else
        {
          float ruling = friendlyPower / (ruleCoef * enemyPower);
          float denom = ruling * (_timeoutMax - _timeoutMid) + (2.0f * _timeoutMid - _timeoutMin - _timeoutMax);
          if (denom <= 0)
            timeout = _timeoutMax;
          else
            timeout = (_timeoutMax - _timeoutMin) * (_timeoutMid - _timeoutMin) / denom + _timeoutMin;
        }
        _countdown = Glob.time + timeout;
      }
      else
      {
        _countdown = Glob.time + GRandGen.Gauss(_timeoutMin, _timeoutMid, _timeoutMax);
      }
    }
  }

  // deactivate
  if (!active)
  {
    if (IsActive())
    {
      if (_repeating)
        OnDesactivate();
    }
    else if (_activeCountdown)
    {
      if (_interruptable)
        _activeCountdown = false;
    }
  }
}

#if _VBS3 // trigger activate EH
int Detector::SetOnActivateEH(RString onActivate) 
{
  _onActivate.Add(TriggerActivationEH(onActivate,_onActivateCounter));
  return _onActivateCounter++;
}

void Detector::ClearOnActivateEH(int i) 
{
  for (int j=0; j<_onActivate.Size(); j++)
  {
    if (_onActivate[j].id == i)
    {
      _onActivate.Delete(j);
      break;
    }
  }
}
#endif

void Detector::DoActivate()
{
  if (strcmpi(_expCond, "this") != 0)
  {
    GameState *state = GWorld->GetGameState();
    GameVarSpace local(state->GetContext(), false);
    state->BeginContext(&local);

    state->VarSetLocal("this", true, true, true);
    state->VarSetLocal("thisTrigger", GameValueExt(this), true, true);
#if USE_PRECOMPILATION
    if (!state->EvaluateBool(_expCond, _compiledCond, GameState::EvalContext::_default, GWorld->GetMissionNamespace())) 
    {
      state->EndContext();
      return; // mission namespace
    }
#else
    if (!state->EvaluateBool(_expCond, GameState::EvalContext::_default, GWorld->GetMissionNamespace())) 
    {
      state->EndContext();
      return; // mission namespace
    }
#endif

    state->EndContext();
  }

  OnActivate(GetActiveVehicle());
}

Object *Detector::GetActiveVehicle()
{
  for (int i=0; i<NVehicles(); i++)
  {
    EntityAI *veh = GetVehicle(i);
    if (veh) return veh;
  }
  return this;
}

extern SoundPars ExplicitEnvSound;

void FindEnvSound(RString name, SoundPars &day);

ConstParamEntryPtr FindMusic(RString name, SoundPars &pars);
ConstParamEntryPtr FindRscTitle(RString name);

void Detector::OnActivate(Object *obj)
{
  _active = true;

  GameState *state = GWorld->GetGameState();
  GameVarSpace local(state->GetContext(), false);
  state->BeginContext(&local);

  state->VarSetLocal("thisList", GetGameValue(), true, true);
  state->VarSetLocal("thisTrigger", GameValueExt(this), true, true);
#if USE_PRECOMPILATION
  state->Execute(_expActiv, _compiledActiv, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#else
  state->Execute(_expActiv, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#endif

  state->EndContext();
  
  if (_action == ASTSwitch)
  {
    for (int i=0; i<_synchronizations.Size(); i++)
    {
      int sync = _synchronizations[i];
      SynchronizedItem &item = synchronized[sync];
      for (int j=0; j<item.groups.Size(); j++)
      {
        SynchronizedGroup &sgrp = item.groups[j];
        AIGroup *grp = sgrp.group;
        if (!grp) continue;
        for (int k=0; k<grp->NWaypoints(); k++)
        {
          const WaypointInfo &wInfo = grp->GetWaypoint(k);
          bool found = false;
          for (int l=0; l<wInfo.synchronizations.Size(); l++)
          {
            if (wInfo.synchronizations[l] == sync)
            {
              found = true;
              break;
            }
          }
          if (found)
          {
            if (grp->GetCurrent())
            {
              FSM *fsm = grp->GetCurrent()->_fsm;
              Assert(fsm);
              int &index = fsm->Var(0);
              index = k + 1;
              AIGroupContext ctx(grp);
              ctx._fsm = fsm;
              ctx._task = const_cast<Mission *>(grp->GetMission());
              fsm->SetState(1, &ctx);
              AIBrain *player = GWorld->FocusOn();
              if (player && player->GetGroup() == grp)
                GWorld->UI()->ShowWaypointPosition();
            }
            break;
          }
        }
      }
    }
  }
  else
  {
    for (int i=0; i<_synchronizations.Size(); i++)
    {
      synchronized[_synchronizations[i]].SetActive(this, false);
    }
  }

#if _VBS3 // trigger activate EH
  for (int i=0; i<_onActivate.Size(); i++)
  {
    if (_onActivate[i].onActivation.GetLength() > 0)
    {
      GameVarSpace vars(false);
      state->BeginContext(&vars);
      state->VarSetLocal("_this", GameValueExt(this), true);
      state->VarSetLocal("_thisList", GetGameValue(), true);
      state->VarSetLocal("_repeating", _repeating, true);
      state->EvaluateMultiple(_onActivate[i].onActivation, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      state->EndContext();
    }
  }
#endif

  state->BeginContext(&local);
  GameValue result = state->Evaluate(_effects.condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
  state->EndContext();

  AIBrain *player = GWorld->FocusOn();
  if (result.GetType() == GameObject)
  {
    if (!player) return;
    Object *obj = static_cast<GameDataObject *>(result.GetData())->GetObject();
    if
    (
      player->GetPerson() != obj &&
      player->GetVehicle() != obj
    ) return;
  }
  else if (result.GetType() == GameArray)
  {
    if (!player) return;
    bool found = false;
    const GameArrayType &array = (GameArrayType &)result;
    for (int i=0; i<array.Size(); i++)
    {
      const GameValue &item = array[i];
      if (item.GetType()==GameObject)
      {
        Object *obj = static_cast<GameDataObject *>(item.GetData())->GetObject();
        if
        (
          player->GetPerson() == obj ||
          player->GetVehicle() == obj
        )
        {
          found = true;
          break;
        }
      }
    }
    if (!found) return;
  }
  else if (result.GetType() & GameBool)
  {
    if (!(bool)result) return;
  }
  else return;

  if (stricmp(_effects.sound, "$NONE$") != 0)
  {
    _sound = new SoundObject(_effects.sound, NULL);
  }

  if (_effects.voice.GetLength() > 0)
  {
    _voice = new SoundObject(_effects.voice, obj);
    _voiceObject = obj;
  }

  if (_effects.soundEnv.GetLength() > 0)
    FindEnvSound(_effects.soundEnv, ExplicitEnvSound);

  if (_effects.soundDet.GetLength() > 0)
  {
    _dynSound = new DynSoundObject(_effects.soundDet);
  }

  if (stricmp(_effects.track, "$NONE$") == 0)
  {
    // nothing to do
  }
  else if (stricmp(_effects.track, "$STOP$") == 0)
  {
    // stop musical track
    GSoundScene->StopMusicTrack();
  }
  else
  {
    // start musical track
    SoundPars sound;
    if (FindMusic(_effects.track, sound))
      // start sound as musical track
      GSoundScene->StartMusicTrack(sound);
  }

  switch (_effects.titleType)
  {
    case TitleNone:
      break;
    case TitleObject:
      GLOB_WORLD->SetTitleEffect
      (
        CreateTitleEffectObj
        (
          _effects.titleEffect,
          Pars >> "CfgTitles" >> _effects.title
        )
      );
      break;
    case TitleResource:
      {
        ConstParamEntryPtr cls = FindRscTitle(_effects.title);
        if (cls) GWorld->SetTitleEffect
        (
          CreateTitleEffectRsc(_effects.titleEffect, *cls)
        );
      }
      break;
    case TitleText:
      GLOB_WORLD->SetTitleEffect
      (
        CreateTitleEffect
        (
          _effects.titleEffect,
          Localize(_effects.title)
        )
      );
      break;
  }
}

void Detector::OnDesactivate()
{
  GameState *state = GWorld->GetGameState();
  GameVarSpace local(state->GetContext(), false);
  state->BeginContext(&local);

  state->VarSetLocal("thisTrigger", GameValueExt(this), true, true);
#if USE_PRECOMPILATION
  state->Execute(_expDesactiv, _compiledDesactiv, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#else
  state->Execute(_expDesactiv, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#endif

  state->EndContext();

  _active = false;
  if (_action != ASTSwitch)
    for (int i=0; i<_synchronizations.Size(); i++)
    {
      synchronized[_synchronizations[i]].SetActive(this, true);
    }
  if (_dynSound) _dynSound = NULL;
}

#include "Cloth/ClothObject.h"


FlagType::FlagType(ParamEntryPar param)
:base(param)
{
  ParamEntryVal config = Pars>>"CfgCloth">>"flag";
  
  _clothType = new ClothObjectType;
  _clothType->Init(config);
}

void FlagType::InitShape()
{
  base::InitShape();

  ParamEntryVal par = *_par;

  _fabric.Init(_shape, (par >> "selectionFabric").operator RString(), NULL);
  // default size in case everything else fails
}


void FlagType::InitFabric()
{
  if (_shape->NLevels()<=0) return;
  ShapeUsed shape = _shape->Level(0);
  int selIndex = _fabric.GetSelection(0);
  if (selIndex>=0)
  {
    ShapeUsedGeometryLock<> lock(_shape,0);
    
    // scan points of selection
    const NamedSelection::Access sel(shape->NamedSel(selIndex),_shape,shape);

    // flag base
    // calculate base 
    float minX = +1e10, minY = +1e10;
    float maxX = -1e10, maxY = -1e10;
    for (int i=0; i<sel.Size(); i++)
    {
      // for each point:
      int index = sel[i];
      Vector3Val val = shape->Pos(index);
      saturateMin(minX,val.X());
      saturateMin(minY,val.Y());
      saturateMax(maxX,val.X());
      saturateMax(maxY,val.Y());
    }
    float xSize = maxX-minX;
    float ySize = maxY-minY;

    _clothType->InitShape(minX,minY,xSize,ySize);
  }
}

void FlagType::DeinitShape()
{
  base::DeinitShape();
  _fabric.Deinit(_shape);
}
void FlagType::InitShapeLevel(int level)
{
  base::InitShapeLevel(level);
  _fabric.InitLevel(_shape,level);
}
void FlagType::DeinitShapeLevel(int level)
{
  _fabric.DeinitLevel(_shape,level);
  base::DeinitShapeLevel(level);
}


Object* FlagType::CreateObject(bool unused) const
{
  Object* v;

  // TODO: what about detectorfhg? Relict?
  if (!stricmp(_simName,"detector")) v = new Detector(this,CreateObjectId());
  else if (!stricmp(_simName,"flag") ) v = new Flag(this,CreateObjectId());
  else 
  {
    LogF("Unknown vehicle simulation %s: type %s (FlagType called)",
      cc_cast(_simName), cc_cast(GetName())
      );
    v = NULL;
  }
  return v;
}



DEFINE_CASTING(Flag)

Flag::Flag(const EntityType *name, const CreateObjectId &id)
: base(name->GetShape(),name,id)
{
  // note: we need to simulate cloth quite often
  SetSimulationPrecision(0.1f);
  RandomizeSimulationTime();
  //SetTimeOffset(0.1f);
  SetTimeOffset(0);
}

bool Flag::InitAsync(Matrix4Par pos)
{
  if (_cloth)
  {
    return true;
  }
  if (_shape->CheckLevelLoaded(0,true))
  {
    Init(pos, true);
    // after we initialized the flag, it would be strange if there is no cloth
    // this probably means model is wrong
    DoAssert(_cloth);
    return true;
  }
  return false;
}


void Flag::Init(Matrix4Par pos, bool init)
{
  // make sure level 0 is loaded so that we can test the selection
  ShapeUsed level0 = _shape->Level(0);
  if (Type()->_fabric.GetSelection(0)>=0)
  {
    unconst_cast(Type())->InitFabric();
    _cloth = new ClothObject();
    _cloth->Init(*Type()->_clothType,pos);
  }
}

Flag::~Flag()
{
  _cloth.Free();
}

void Flag::SetFlagTexture(Texture *texture)
{
  if (_texture != texture)
  {
    _texture = texture;
  }
}

void Flag::FlagSimulate(Matrix4Par pos, float deltaT, SimulationImportance prec)
{
  base::Simulate(deltaT,prec);
  if (_cloth)
  {
    Vector3 wind = GLandscape->GetWind();

    // calculate world space transformation of flag
    Vector3 inertia = VZero; // global external force 
    _cloth->Simulate(*Type()->_clothType,pos,FutureVisualState().Speed(),deltaT,wind,inertia,prec);
  }
}


void Flag::Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2)
{
  // when we are passed setEngineStuff=true, it means we should not do any shape animation
  // this is used in Scene.AdjustComplexity to check proxy complexity
  if (_cloth && !setEngineStuff)
  {
    // we need to know world space coordinates of the flag
    // scan cloth particles and animates points of flag animation as appropriate
    ShapeUsed shape = _shape->Level(level);

    int selIndex = Type()->_fabric.GetSelection(level);
    if (selIndex>=0)
    {
      // scan points of selection
      const NamedSelection::Access sel(shape->NamedSel(selIndex),_shape,shape);

      float xSize = Type()->_clothType->GetSizeX();
      float ySize = Type()->_clothType->GetSizeY();
      float xMin = Type()->_clothType->GetMinX();
      float yMin = Type()->_clothType->GetMinY();
      float invXSize = 1/xSize;
      float invYSize = 1/ySize;

      //Matrix4 worldToModel = animContext.GetVisualState().cast<Flag>().GetInvTransform();
      Matrix4 worldToModel = _cloth->WorldPosUsed().InverseScaled(); // animContext.GetVisualState().cast<Flag>().GetInvTransform();

      for (int i=0; i<sel.Size(); i++)
      {
        // for each point:
        // estimate where in flag it is
        // 
        int index = sel[i];
        Vector3Val val = shape->Pos(index);

        float xIndexF = (val.X()-xMin)*invXSize;
        float yIndexF = (val.Y()-yMin)*invYSize;

        // get simulation result for point xIndexF, yIndexF
        Vector3 pos = _cloth->GetPosition(xIndexF,yIndexF);
        Vector3 norm = _cloth->GetNormal(xIndexF,yIndexF);

        Vector3 mpos = worldToModel.FastTransform(pos);
        Vector3 mnorm = worldToModel.Rotate(norm);

        animContext.SetPos(shape, index) = mpos;
        animContext.SetNorm(shape, index).Set(mnorm);

      }
      animContext.InvalidateNormals(shape);
    }
  }

  Type()->_fabric.SetTexture(animContext, _shape, level, _texture);

  base::Animate(animContext, level, setEngineStuff, this, dist2);
}

void Flag::Deanimate(int level, bool setEngineStuff)
{
  base::Deanimate(level, setEngineStuff);
}

FlagCarrierType::FlagCarrierType( ParamEntryPar param )
:base(param)
{
}
void FlagCarrierType::Load(ParamEntryPar par, const char *shape)
{
  base::Load(par,shape);

  _animSpeed = 0.5;
  //_invAnimSpeed = 1.0 / _animSpeed;

}
void FlagCarrierType::InitShape()
{
  base::InitShape();
}
void FlagCarrierType::DeinitShape()
{
  base::DeinitShape();
}

void FlagCarrierType::InitShapeLevel(int level)
{
  base::InitShapeLevel(level);
  if (_shape->GetSkeleton())
  {
    // shape initialization
    ShapeUsed shape = _shape->Level(level);
    _shape->GetSkeleton()->PrepareProxyBones(_shape, shape.InitShape());
  }
}
void FlagCarrierType::DeinitShapeLevel(int level)
{
  if (_shape->GetSkeleton())
  {
    // shape deinitialization
    ShapeUsed shape = _shape->Level(level);
    _shape->GetSkeleton()->FinishProxyBones(_shape, shape.InitShape());
  }
  base::DeinitShapeLevel(level);
}


bool FlagCarrierType::IsSimulationRequired() const
{
  return true;
}
bool FlagCarrierType::IsSoundRequired() const
{
  return false;
}

AnimationSource *FlagCarrierType::CreateAnimationSource(const AnimationType *type, RStringB source)
{
  if (!strcmpi(source,"flag")) return _animSources.CreateAnimationSource(&FlagCarrier::GetCtrlFlag);
  
  return base::CreateAnimationSource(type,source);
}


Object* FlagCarrierType::CreateObject(bool unused) const
{ 
  return new FlagCarrier(this);
}


DEFINE_CASTING(FlagCarrier)

FlagCarrier::FlagCarrier(const EntityAIType *type)
: base(type)
{
  _phase = 0;

  _flagSide = TSideUnknown;
  // the flag is moving, but the flagpole itself is not
  _static=true;

  // Default precision is too low, take at least 30FPS.
  SetSimulationPrecision(0.03f);
  RandomizeSimulationTime();
  SetTimeOffset(0.03f);
}

FlagCarrier::~FlagCarrier()
{
}

AnimationStyle FlagCarrier::IsAnimated(int level) const
{
  return NotAnimated;
}
bool FlagCarrier::IsAnimatedShadow(int level) const
{
  return false;
}

Texture *FlagCarrier::GetFlagTexture()
{
  return _flagOwner ? NULL : _flagTexture;
}

Texture *FlagCarrier::GetFlagTextureInternal()
{
  return _flagTexture;
}

void FlagCarrier::SetFlagTexture(RString name)
{
  _flagTexture = NULL;
  if (name.GetLength() > 0)
  {
    RString FindPicture(RString name);

    RString fullName = FindPicture(name);
    if (fullName.GetLength() > 0)
    {
      fullName.Lower();
      _flagTexture = GlobLoadTexture(fullName);      
    }
  }
  else
  {
    SetFlagOwner(NULL);
  }
}

bool CheckSupply(EntityAI *vehicle, EntityAI *parent, SupportCheckF check, float limit, bool now);

void FlagCarrier::GetActions(UIActions &actions, AIBrain *unit, bool now, bool strict)
{
  if (_flagTexture && !_flagOwner && unit && unit->IsFreeSoldier() && !unit->GetPerson()->GetFlagCarrier())
  {
    if (_flagOwnerWanted)
    {
      if (unit->GetPerson() == _flagOwnerWanted)
      {
        UIAction action(new ActionBasic(ATCancelTakeFlag, this));
        action.position = RenderVisualState().PositionModelToWorld(GetType()->GetSupplyPoint());
        actions.Add(action);
      }
    }
    else
    {
      if (unit->IsEnemy(_flagSide))
      {
        if (CheckSupply(unit->GetPerson(), this, NULL, 0, now))
        {
          UIAction action(new ActionBasic(ATTakeFlag, this));
          action.position = RenderVisualState().PositionModelToWorld(GetType()->GetSupplyPoint());
          actions.Add(action);
        }
      }
    }
  }
  base::GetActions(actions, unit, now, strict);
}

void FlagCarrier::SetFlagOwner(Person *veh)
{
  DoAssert(IsLocal());
  if (_flagOwner)
  {
    DoAssert(_flagOwner == _flagOwnerWanted);
    if (_flagOwner->IsLocal())
      _flagOwner->SetFlagCarrier(NULL);
    else
      GetNetworkManager().SetFlagCarrier(_flagOwner, NULL);

#if LOG_FLAG_CHANGES
    RptF
    (
      "Flags: Local flag %s owner changes: from %s (wanted %s) to %s",
      (const char *)GetDebugName(),
      _flagOwner ? (const char *)_flagOwner->GetDebugName() : "NULL",
      _flagOwnerWanted ? (const char *)_flagOwnerWanted->GetDebugName() : "NULL",
      veh ? (const char *)veh->GetDebugName() : "NULL"
    );
#endif

    _flagOwnerWanted = _flagOwner = veh;
    _phase = 0;

    if (veh)
    {
      if (veh->IsLocal())
        veh->SetFlagCarrier(this);
      else
        GetNetworkManager().SetFlagCarrier(veh, this);
    }
  }
  else
  {
    // !_flagOwner
    if (veh)
    {
#if LOG_FLAG_CHANGES
      RptF
      (
        "Flags: Local flag %s: wanted owner changes from %s to %s (real owner is NULL)",
        (const char *)GetDebugName(),
        _flagOwnerWanted ? (const char *)_flagOwnerWanted->GetDebugName() : "NULL",
        (const char *)veh->GetDebugName()
      );
      RptF("  Detail info about %s:", (const char *)veh->GetDebugName());
      RptF("    Local: %s", veh->IsLocal() ? "YES" : "NO");
      RptF("    Position [%.0f, %.0f]", veh->Position().X(), veh->Position().Z());
      RptF("    Flag position [%.0f, %.0f]", Position().X(), Position().Z());
      RptF("    Distance from flag %.1f", Position().Distance(veh->Position()));
      RptF("  Player: %s", GWorld->PlayerOn() ? (const char *)GWorld->PlayerOn()->GetDebugName() : "NULL");
#endif
      _flagOwnerWanted = veh;
      _phase = 0;
      _animStart = Glob.time;
    }
  }
}

void FlagCarrier::CancelTakeFlag()
{
  if (!_flagOwner && _flagOwnerWanted)
  {
#if LOG_FLAG_CHANGES
    RptF
    (
      "Flags: Local flag %s: wanted owner changes from %s to NULL (real owner is NULL)",
      (const char *)GetDebugName(),
      (const char *)_flagOwnerWanted->GetDebugName()
    );
#endif
/*
    if (_flagOwnerWanted->IsLocal())
      _flagOwnerWanted->SetFlagCarrier(NULL);
    else
      GetNetworkManager().SetFlagCarrier(_flagOwnerWanted, NULL);
*/
    _flagOwnerWanted = NULL;
    _phase = 0;
    _animStart = Glob.time;
  }
}

TargetSide FlagCarrier::GetFlagSide() const
{
  return _flagSide;
}

void FlagCarrier::SetFlagSide(TargetSide side)
{
  _flagSide = side;
}

void FlagCarrier::Simulate( float deltaT, SimulationImportance prec )
{
  base::Simulate(deltaT, prec);
  SimulatePost(deltaT, prec);
  
/*
  if (IsLocal())
  {
    if (_flagOwnerWanted && !_flagOwnerWanted->Brain()) _flagOwnerWanted = NULL; // disappearing
    if (_flagOwner && !_flagOwner->Brain()) SetFlagOwner(NULL); // disappearing
  }
*/
  if (!_flagOwner && _flagOwnerWanted)
  {
    if (IsLocal() && !_flagOwnerWanted->Brain())
    {
      _flagOwnerWanted = NULL;
      _phase = 0;
      return;
    }
    _phase = Type()->_animSpeed * (Glob.time - _animStart);
    if (_phase >= 1.0)
    {
      _phase = 1.0;
      if (IsLocal()) // only local flag can change flag carrier
      {
#if LOG_FLAG_CHANGES
        RptF
        (
          "Flags: Local flag %s: real owner changes from %s to wanted owner %s",
          (const char *)GetDebugName(),
          _flagOwner ? (const char *)_flagOwner->GetDebugName() : "NULL",
          _flagOwnerWanted ? (const char *)_flagOwnerWanted->GetDebugName() : "NULL"
        );
#endif
        _flagOwner = _flagOwnerWanted;
        if (_flagOwner->IsLocal())
          _flagOwner->SetFlagCarrier(this);
        else
          GetNetworkManager().SetFlagCarrier(_flagOwner, this);
      }
    }
  }
  else if (!_flagOwner && !_flagOwnerWanted)
  {
    // fix: owner disappeared
    _phase = 0;
  }
}

LSError FlagCarrier::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))

  CHECK(ar.SerializeRef("flagOwner", _flagOwner, 1))
  CHECK(ar.SerializeRef("flagOwnerWanted", _flagOwnerWanted, 1))
  if (ar.IsSaving())
  {
    RString name = _flagTexture ? _flagTexture->Name() : "";
    CHECK(ar.Serialize("flagTexture", name, 1))
  }
  else if (ar.GetPass() == ParamArchive::PassFirst)
  {
    RString name;
    CHECK(ar.Serialize("flagTexture", name, 1))
    _flagTexture = name.GetLength() > 0 ? GlobLoadTexture(name) : NULL;
  }
  CHECK(ar.SerializeEnum("flagSide", _flagSide, 1, TSideUnknown))

  return LSOK;
}

#define FLAG_CARRIER_MSG_LIST(XX) \
  XX(UpdateGeneric, UpdateFlag)

DEFINE_NETWORK_OBJECT(FlagCarrier, base, FLAG_CARRIER_MSG_LIST)

#define UPDATE_FLAG_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), flagOwner, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Flag owner"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, OLinkPermO(Person), flagOwnerWanted, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Wanted flag owner"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, RString, flagTexture, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Flag texture"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, flagSide, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, TSideUnknown), DOC_MSG("Flag side"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE)

DECLARE_NET_INDICES_EX_ERR(UpdateFlag, UpdateVehicleAI, UPDATE_FLAG_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateFlag, UpdateVehicleAI, UPDATE_FLAG_MSG, NoErrorInitialFunc)

NetworkMessageFormat &FlagCarrier::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  switch (cls)
  {
  case NMCUpdateGeneric:
    base::CreateFormat(cls, format);
    UPDATE_FLAG_MSG(UpdateFlag, MSG_FORMAT_ERR)
    break;
  default:
    base::CreateFormat(cls, format);
    break;
  }
  return format;
}

TMError FlagCarrier::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCUpdateGeneric:
    TMCHECK(base::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateFlag)

      TRANSF_REF(flagOwner)
      if (ctx.IsSending())
      {
        TRANSF_REF(flagOwnerWanted)
      }
      else
      {
        Person *oldOwner = _flagOwnerWanted;
        TRANSF_REF(flagOwnerWanted)
        if (_flagOwnerWanted != oldOwner)
        {
          _phase = 0;
          if (_flagOwnerWanted && !_flagOwner) _animStart = Glob.time;
#if LOG_FLAG_CHANGES
          RptF
          (
            "Flags: Remote flag %s wanted owner changes: from %s to %s",
            (const char *)GetDebugName(),
            oldOwner ? (const char *)oldOwner->GetDebugName() : "NULL",
            _flagOwnerWanted ? (const char *)_flagOwnerWanted->GetDebugName() : "NULL"
          );
#endif
        }
      }

      TRANSF_ENUM(flagSide)
      if (ctx.IsSending())
      {
        RString name = _flagTexture ? _flagTexture->Name() : "";
        TRANSF_EX(flagTexture, name)
      }
      else
      {
        RString name;
        TRANSF_EX(flagTexture, name)
        if (name.GetLength() > 0)
          _flagTexture = GlobLoadTexture(name);
        else
          _flagTexture = NULL;
      }
    }
    break;
  default:
    return base::TransferMsg(ctx);
  }
  return TMOK;
}

float FlagCarrier::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = 0;
  switch (ctx.GetClass())
  {
  case NMCUpdateGeneric:
    {
      error += base::CalculateError(ctx);
      
      PREPARE_TRANSFER(UpdateFlag)

      ICALCERRE_NEQREF_PERM(Person, flagOwner, _flagOwner, ERR_COEF_MODE)
      ICALCERRE_NEQREF_PERM(Person, flagOwnerWanted, _flagOwnerWanted, ERR_COEF_MODE)
      RString name = _flagTexture ? _flagTexture->Name() : "";
      ICALCERRE_NEQSTR(flagTexture, name, ERR_COEF_MODE)
      ICALCERR_NEQ(int, flagSide, ERR_COEF_MODE)
    }
    break;
  default:
    error += base::CalculateError(ctx);
    break;
  }
  return error;
}

