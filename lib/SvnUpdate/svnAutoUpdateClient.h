#ifndef _SVN_AUTO_UPDATER_H
#define _SVN_AUTO_UPDATER_H

class ISVNAutoUpdaterClient 
{
  public:
  virtual ~ISVNAutoUpdaterClient() {}

  virtual bool Update() = 0;
  virtual bool Init(const char *svnAUPath, const char *svnURL, const char *updateDir, const char *user, const char *passwd) = 0;
  virtual bool IsReady() const = 0;
};

ISVNAutoUpdaterClient *CreateSVNAutoUpdaterClient();

#endif
