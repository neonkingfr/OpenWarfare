#undef _VERIFY_KEY							
#undef _ENABLE_EDITOR						
#undef _ENABLE_ALL_MISSIONS			
#undef _ENABLE_MP								
//#undef _ENABLE_AI								
#undef _ENABLE_CAMPAIGN					
#undef _ENABLE_SINGLE_MISSION	
#undef _ENABLE_BRIEF_EQ					
#undef _ENABLE_BRIEF_GRP					
//#undef _ENABLE_PARAMS						
//#undef _ENABLE_CHEATS						
#undef _ENABLE_ADDONS						
#undef _ENABLE_DEDICATED_SERVER	
#undef _FORCE_DEMO_ISLAND				
#undef _FORCE_SINGLE_VOICE				
#undef _ENABLE_AIRPLANES					
#undef _ENABLE_HELICOPTERS				
#undef _ENABLE_SHIPS							
#undef _ENABLE_CARS							
#undef _ENABLE_TANKS							
#undef _ENABLE_MOTORCYCLES				
#undef _ENABLE_PARACHUTES				
#undef _ENABLE_DATADISC					
#undef _ENABLE_VBS								
#undef _ENABLE_GAMESPY           
#undef _ENABLE_CONVERSATION   
#undef _ENABLE_RIGID_BODIES
#undef _ENABLE_BB_TREES
#undef _SECUROM
#undef _ENABLE_IDENTITIES

//#define _VERIFY_KEY								1
#define _ENABLE_EDITOR						0
#define _ENABLE_ALL_MISSIONS			0
#define _ENABLE_MP								0
//#define _ENABLE_AI								0
#define _ENABLE_CAMPAIGN					0
#define _ENABLE_SINGLE_MISSION		0
#define _ENABLE_BRIEF_EQ					0
#define _ENABLE_BRIEF_GRP					0
//#define _ENABLE_PARAMS						1
//#define _ENABLE_CHEATS						0
#define _ENABLE_ADDONS						1 // parse addon configs
#define _ENABLE_DEDICATED_SERVER	0
#define _FORCE_DEMO_ISLAND				0
#define _FORCE_SINGLE_VOICE				0
#define _ENABLE_AIRPLANES					0
#define _ENABLE_HELICOPTERS				0
#define _ENABLE_SHIPS							0
#define _ENABLE_CARS							0
#define _ENABLE_TANKS							0
#define _ENABLE_MOTORCYCLES				0
#define _ENABLE_PARACHUTES				0
#define _ENABLE_DATADISC					1
#define _ENABLE_VBS								0
#define _ENABLE_GAMESPY           0
#define _ENABLE_CONVERSATION      0
#define _ENABLE_RIGID_BODIES      0
#define _ENABLE_BB_TREES          0
#define _ENABLE_COMPILED_SHADER_CACHE 1
#define _ENABLE_IDENTITIES        0

#define _VERIFY_KEY								0
#define _SECUROM                  0

