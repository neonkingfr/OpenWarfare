#ifndef registration_h__
#define registration_h__

#include <Es/Strings/rString.hpp>

#ifdef _WIN32
#define NOTHROW __declspec(nothrow)
#else
#define NOTHROW __attribute__((nothrow))
#endif

struct SecureKeyList
{
  FindArray<RString> keys; // blocked keys list
  AutoArray<char> signature; // signature of a blocked keys list
  AutoArray<char> signatureWithTime; // signature of a blocked keys list, including timestamp
  long long timestamp;
};

struct UserCertificate
{
  AutoArray<char> certificate;
  AutoArray<char> privateKey;
  SecureKeyList blockedKeys;
  RString errorMessage;

  UserCertificate(){}
  explicit UserCertificate(RString errorMessage):errorMessage(errorMessage){}
};

UserCertificate NOTHROW GetUserCertificate(RString url, RString productId, RString playerId, const Array<unsigned char> &cdKey);

enum SignValidationResultCode
{
  SignOK,
  SignBadUserName,
  SignBlockedUser,
  SignBadSignature,
  SignBadCertificate,
  SignUnknownError,
};

struct SignValidationResult
{
  SignValidationResultCode code;
  RString error;
  SignValidationResult(SignValidationResultCode code, RString error=RString()):code(code),error(error){}
  bool operator == (SignValidationResultCode c) const {return code==c;}
  bool operator != (SignValidationResultCode c) const {return !operator ==(c);}
};

SignValidationResult NOTHROW VerifySignedData(RString playerId, long long challenge, const Array<char> &signature, const Array<char> &certificate);
const AutoArray<unsigned char> NOTHROW SignData(long long challenge, const Array<unsigned char> &privateKey);
bool NOTHROW VerifySignedKeys(const Array<RString> &data, const Array<char> &signature, long long timestamp);

RString NOTHROW PrintSignedKeys(const Array<RString> &data, const Array<char> &signature, long long timestamp);
void NOTHROW ParseSignedKeys(FindArray<RString> &data, AutoArray<char> &signature, long long &timestamp, const char *input);

long long NOTHROW GetSecureRandom();

RString NOTHROW GetConfigAppFromRegistry();

#endif // registration_h__
