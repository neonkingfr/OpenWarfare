/*!
\file
Implemetation for Person class
*/

#include "wpch.hpp"

#include "transport.hpp"
#include "AI/ai.hpp"
#include "person.hpp"
#include "world.hpp"
#include "Network/network.hpp"
#include "UI/uiActions.hpp"
#include "global.hpp"

#include <Es/Algorithms/qsort.hpp>

#include <El/Enum/enumNames.hpp>
#include <El/Common/randomGen.hpp>

DEFINE_CASTING(Person)
Person::Person(const EntityAIType *name, bool fullCreate)
:base(name,fullCreate),_sensorRowID(-1),_remotePlayer(1),
_disableDamageFromObjUntil(Glob.time)
#if _VBS2 //convoy trainer
,_enablePersonalItems(false)
#endif
{
	_info._rank = RankPrivate;
	_info._experience = 0;
	_info._initExperience = 0;
  _info._pitch = 0;

#if _ENABLE_CHEATS
  _vonDpnid = 0; //means undef
#endif

#if _VBS2 // suppression
  _isSuppressed = false;
  _suppressedWince = false;
#endif
}

void Person::SetBrain(AIBrain *brain)
{
  if (_brain == brain) return;
#if _ENABLE_INDEPENDENT_AGENTS
  if ((_brain && _brain->GetTeamMember()) && !(brain && brain->GetTeamMember()))
  {
    // try to keep the variables for example when the agent is killed
    GameVarSpace *src = _brain->GetTeamMember()->GetVars();
    GameVarSpace *dst = EntityAI::GetVars();
    if (src && dst) dst->_vars = src->_vars;
  }
  if (_brain)
  {
    AITeamMember *teamMember = _brain->GetTeamMember();
    if (teamMember)
      GWorld->RemoveAgent(teamMember);
  }
  if (brain)
  {
    AITeamMember *teamMember = brain->GetTeamMember();
    if (teamMember)
      GWorld->AddAgent(teamMember);
  }
#endif
  if (_brain)
    GetNetworkManager().SetPlayerRoleLifeState(_brain->GetRoleIndex(), LifeStateDead);
  if (brain)
    GetNetworkManager().SetPlayerRoleLifeState(brain->GetRoleIndex(), brain->GetLifeState());
  _brain = brain;
}

AIBrain *Person::CreateBrain(bool forceAgent)
{
#if _ENABLE_INDEPENDENT_AGENTS && _ENABLE_IDENTITIES
  const AutoArray<RString> &tasks = GetType()->_agentTasks;
  if (forceAgent || tasks.Size() > 0)
  {
    // create agent
    AIAgent *agent = new AIAgent(this);
    _brain = agent;
    GWorld->AddAgent(agent);
    // register tasks
    for (int i=0; i<tasks.Size(); i++)
    {
      Ref<AITaskType> taskType = AITaskTypes.New(tasks[i]);
      if (taskType)
        agent->RegisterTaskType(taskType);
    }
    return _brain;
  }
#endif
  // create the unit
  _brain = new AIUnit(this);
  return _brain;
}

RString Person::SelectSpeaker(RString speakerType)
{
  RString speaker = speakerType;
  ParamEntryVal cls = Pars >> "CfgVoice";
  while (!speaker.IsEmpty())
  {
    ConstParamEntryPtr entry = (Pars >> "CfgVoiceTypes").FindEntry(speaker);
    if (!entry)
    {
      RptF("Speaker %s not found in CfgVoiceTypes",cc_cast(speaker));
      return RString();
    }
    for (int i=0; i<cls.GetEntryCount(); i++)
    {
      ParamEntryVal voice = cls.GetEntry(i);
      if (! voice.IsClass()) continue;

      ConstParamEntryPtr scope = voice.FindEntry("scope");
      if (!scope || !(bool)*scope) continue;

      RString voiceType = voice >> "voiceType";
      if (stricmp(voiceType,speaker)!=0) continue; 
      ParamEntryVal identityTypes = voice >> "identityTypes";
      for (int j=0; j<identityTypes.GetSize(); j++)
      {
        if (IsIdentityEnabled(identityTypes[j]))
          // found, this speaker is good enough
          return voice.GetName();
      }
    }
    speaker = (*entry) >> "alternative";
  }
  
  return RString();
}

void Person::ApplyIdentity()
{
  int dpnid = _remotePlayer;
  if (dpnid==AI_PLAYER)
  { // use Person._info
    AIUnitInfo &info = GetInfo();

    // set no squad for AI
    info._squadPicturePath = RString("");
    info._squadPicture = NULL;

    SetFace(info._face, info._name);
    SetGlasses(info._glasses);
    Assert(Brain());
    if (Brain())
      Brain()->SetSpeaker(info._speaker, info._pitch);
    LogF("*** AI identity %s applied on person %d:%d (%s)", (const char *)info._name, GetNetworkId().creator.CreatorInt(), GetNetworkId().id, cc_cast(GetDebugName()));
  }
  else
  { // use PlayerIdentity
    const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpnid);
    if (identity)
    {
      if (identity->squad)
      {
        AIUnitInfo &info = GetInfo();
        info._squadTitle = identity->squad->_title;
        if (identity->squad->_picture.GetLength() > 0)
        {
          info._squadPicturePath = RString("\\squads\\") + identity->squad->_nick + RString("\\") + identity->squad->_picture;
          RString GetClientCustomFilesDir();
          RString picture = GetClientCustomFilesDir() + info._squadPicturePath;
          picture.Lower();
          if (QIFileFunctions::FileExists(picture))
            info._squadPicture = GlobLoadTexture(picture);
        }
      }
      SetFace(identity->face, identity->name);
      SetGlasses(identity->glasses);
      Assert(Brain());
      if (Brain())
        Brain()->SetSpeaker(SelectSpeaker(identity->speakerType), identity->pitch);
      LogF("*** Player identity %s applied on person %d:%d (%s)", (const char *)identity->name, GetNetworkId().creator.CreatorInt(), GetNetworkId().id,cc_cast(GetDebugName()));
    }
    else
      RptF("*** Player identity cannot be applied on person %d:%d (%s) - player %d not found", GetNetworkId().creator.CreatorInt(), GetNetworkId().id, cc_cast(GetDebugName()), dpnid);
  }
  void UpdateUnitsInBriefing();
  UpdateUnitsInBriefing();
}

#if _ENABLE_IDENTITIES
void Person::CopyIdentity(const Person *source)
{
  const Identity &identity = source->GetIdentity();
  _identity = identity;

  // fix the ownership of tasks
  for (int i=0; i<_identity.GetSimpleTasks().Size(); i++)
  {
    Task *task = _identity.GetSimpleTasks()[i];
    task->ChangeOwner(source, this);
  }

  // re-create the diary UI
  _identity.SetDiaryChanged();
}
#endif

RString Person::GetPersonName() const
{
  int dpid = GetRemotePlayer();
  if (dpid!=1)
  {
    const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpid);
    if (identity)
      return identity->name;
  }
  return _info._name;
}

LODShapeWithShadow *Person::GetOpticsModel(const Person *person) const
{
	Assert(person == this);
  int weapon = _weaponsState.ValidatedCurrentWeapon();
	if (weapon < 0)
    return NULL;
	MuzzleType *muzzle = _weaponsState._magazineSlots[weapon]._muzzle;
	if (!muzzle)
    return NULL;
  const OpticsInfo &opticsInfo = _weaponsState._magazineSlots[weapon]._muzzle->_opticsInfo[_weaponsState._magazineSlots[weapon]._opticsMode];
  if(!opticsInfo._useModelOptics)
    return NULL;
	return muzzle->_opticsModel;
}

bool Person::GetEnableOptics(ObjectVisualState const& vs, const Person *person) const
{
	Assert(person == this);
  int weapon = _weaponsState.ValidatedCurrentWeapon();
	if (weapon < 0)
    return false;
  MuzzleType *muzzle = _weaponsState._magazineSlots[weapon]._muzzle;
	if (!muzzle)
    return false;
	return muzzle->_opticsEnable;
}

#if _VBS2 // convoy trainer
  #include "transport.hpp"
  #include "soldierOld.hpp"

  CameraType ValidateCamera(CameraType cam);
  void Person::SetEnablePersonalItems(bool value, RString animAction, Vector3 personalItemsOffset) 
  {
    if(!_brain)
      return;

    _enablePersonalItems = value;
    if(!_brain->GetVehicleIn())
      return;

    if(_brain->GetVehicleIn()->DriverBrain()==_brain)
      return;    

    if(value) //enable PersonalItems
    {
      if (GWorld->FocusOn() == _brain && IsLocal())
        GWorld->SwitchCameraTo(this,ValidateCamera(GWorld->GetCameraType()), false);

      if(animAction.GetLength() == 0)
        animAction = "vbs2_PersonalItems_Default";
      
      _personalItemsAnim = animAction;
      
      _personalItemsOffset = personalItemsOffset;

      SwitchAction(animAction);
    }
    else
    {
      if (GWorld->FocusOn() == _brain && IsLocal())
        GWorld->SwitchCameraTo(_brain->GetVehicleIn(),ValidateCamera(GWorld->GetCameraType()), false);

      // check to see if unit is inside cargo?
      Transport *transport = _brain->GetVehicleIn();
      const ManCargo &manCargo = transport->GetManCargo();
      int index = -1;
      for(int i =0;i<manCargo.Size();i++)
      {
        if(manCargo.Get(i).man == this)
        {
          index = i;
          break;
        }
      }

      if(index>=0)
        SwitchAction(transport->CargoAction(index));
      else
      {
          TurretContext context;
          if(transport->FindTurret(this, context))
        {
          RString action = context._turret->GunnerAction(*context._turretType);
          SwitchAction(action);
        }
      }
    }
  }

  bool Person::GetPersonalItemsParams(RString& animAction, Vector3& personalItemsOffset)
  {
    personalItemsOffset = VZero;
    animAction = "";

    if(Brain())
    {
      TurretContext context;
      Transport* transport = Brain()->GetVehicleIn();
      if(transport && transport->FindTurret(this, context) && context._turretType)
      {
        animAction = context._turretType->_personalItemsAction;
        personalItemsOffset = context._turretType->_personalItemsOffset;
        return true;
      }
    }
    return false;
  }

#endif

bool Person::GetForceOptics(const Person *person, CameraType camType) const
{
	Assert(person == this);
  int weapon = _weaponsState.ValidatedCurrentWeapon();
	if (weapon < 0)
    return false;
  MuzzleType *muzzle = _weaponsState._magazineSlots[weapon]._muzzle;
	if (!muzzle)
    return false;
  // with external view the optics may sometimes be not forced
  if (camType==CamExternal)
    return muzzle->_forceOptics==1;
	return muzzle->_forceOptics>0;
}

GameVarSpace *Person::GetVars()
{
#if _ENABLE_INDEPENDENT_AGENTS
  if (_brain)
  {
    AITeamMember *teamMember = _brain->GetTeamMember();
    // use variables in the team member for the AIAgent
    if (teamMember) return teamMember->GetVars();
    // otherwise use the variables in the entity
  }
#endif
  return EntityAI::GetVars();
}

void Person::CleanUp()
{
  _brain = NULL;
  base::CleanUp();
}

void Person::Simulate(float deltaT, SimulationImportance prec)
{
#ifdef ARROWS
    Vector3 cCenterBody1(VFastTransform,ModelToWorld(),GetCenterOfMass());
    AddForce(cCenterBody1, Vector3(0,2,0), Color(1,0,0));
#endif

#if _VBS2 // suppression
  if (_isSuppressed && Glob.time >= _suppressedUntil)
  {
    _isSuppressed = false;    
    if (this == GWorld->CameraOn())
    {
      _suppressedWince = 0;
      GWorld->SetEyeAccom(-1);     
    }
  }
#endif

  // FIX: After MPLoad some role can be in LifeStateDeadSwitching but no DisplayTeamSwitch running
  if (GWorld->GetRealPlayer()==this && Brain() && Brain()->GetLifeState()==LifeStateDeadSwitching)
  {
    bool IsTeamSwitchDialog();
    if (!IsTeamSwitchDialog())
    {
      AbstractOptionsUI *CreateTeamSwitchDialog(Person *player, EntityAI *killer, bool respawn, bool userDialog);
      GWorld->DestroyUserDialog();
      GWorld->SetUserDialog(CreateTeamSwitchDialog(this, NULL, true, true));
    }
  }
	base::Simulate(deltaT,prec);
}

#if _VBS2 // suppression
//TODO: rework! use Arma2 suppression map
void Person::Suppress(ShotShell *shot, float closestDistance, Vector3 closestPosition)
{  
  if (Glob.config.IsEnabled(DTSuppressPlayer) && this == GWorld->CameraOn())
  {
    _suppressedWince = 1;  
    _suppressedUntil = Glob.time + 0.1;
    GWorld->SetEyeAccom(0.01);    
  }
  else
  {
    // AI suppression
    AIBrain *brain = Brain();
    if (brain)
    {
      float skill = brain->GetRawAbility(); // 0 - 1
      if (!_isSuppressed) _suppressedUntil = Glob.time;
      // [1.1-skill] so even 100% skilled AI get some suppression.
      _suppressedUntil += GRandGen.RandomValue() * MAX_AI_SUPPRESSION_TIME * (1.1f - skill);
      if (_suppressedUntil - Glob.time > MAX_AI_SUPPRESSION_TIME_CUMULATIVE) _suppressedUntil = Glob.time + MAX_AI_SUPPRESSION_TIME_CUMULATIVE;
    }
  }
  _isSuppressed = true;  
  SuppressSecondaryEffects();
}
#endif

bool Person::IsRenegade() const
{
#if _VBS3_DISABLE_RENEGADE
  return false;
#endif
  // works even when no brain is present
  return GetExperience() < ExperienceRenegadeLimit;
}

bool Person::QIsManual() const
{
	if (!GLOB_WORLD->PlayerManual())
    return false;
	if (!GLOB_WORLD->PlayerOn())
    return false;
	return GLOB_WORLD->PlayerOn() == this;
}

void Person::SetFace()
{
  int dpid = GetRemotePlayer();
  if (dpid != 1)
  {
    const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpid);
    if (identity)
    {
      SetFace(identity->face, identity->name);
      return;
    }
  }
  SetFace(_info._face, _info._name);
}

void Person::SetGlasses()
{
  int dpid = GetRemotePlayer();
  if (dpid!=1)
  {
    const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpid);
    if (identity)
    {
      SetGlasses(identity->glasses);
      return;
    }
  }
  SetGlasses(_info._glasses);
}

void Person::ResetStatus()
{
	_sensorRowID=SensorRowID(-1);
	base::ResetStatus();
}

UnitPosition Person::GetUnitPosition() const {return UPAuto;}

void Person::CheckAmmo(CheckAmmoInfo &info)
{
	Person *man = this;
	int slotsMax = GetItemSlotsCount(man->GetType()->_weaponSlots);

	// primary and secondary weapon
	int index = man->FindWeaponType(MaskSlotPrimary);
	const WeaponType *primary = index >= 0 ? man->GetWeaponSystem(index) : NULL;
	index = man->FindWeaponType(MaskSlotSecondary);
	const WeaponType *secondary = index >= 0 ? man->GetWeaponSystem(index) : NULL;

	// calculate maximal usable slots
	if (primary && primary->_muzzles.Size() > 0) info.muzzle1 = primary->_muzzles[0];
	if (info.muzzle1) info.slots1 = slotsMax < 5 ? slotsMax : 5;

	if (secondary && secondary->_muzzles.Size() > 0) info.muzzle2 = secondary->_muzzles[0];
	else if (primary && primary->_muzzles.Size() > 1) info.muzzle2 = primary->_muzzles[1];
	if (info.muzzle2) info.slots2 = slotsMax - info.slots1;

	info.slots3 = slotsMax - info.slots1 - info.slots2;

	// subtract used slots
	for (int i=0; i<man->NMagazines(); i++)
	{
		const Magazine *magazine = man->GetMagazine(i);
		if (!magazine) continue;
		if (magazine->GetAmmo() == 0) continue;
		const MagazineType *type = magazine->_type;
		int slots = GetItemSlotsCount(type->_magazineType);
		
    if (slots > 0)
    {
		  if (info.muzzle1 && info.muzzle1->CanUse(type))
		  {
        // magazine can be used in muzzle1
        info.hasSlots1 += slots;
        info.hasAmmo1 += magazine->GetAmmo() / type->_maxAmmo;
				if (info.slots1 >= slots) {info.slots1 -= slots; slots = 0;}
				else
				{
					slots -= info.slots1; info.slots1 = 0;
					if (info.slots3 >= slots) {info.slots3 -= slots; slots = 0;}
					else
					{
						slots -= info.slots3; info.slots3 = 0;
						if (info.slots2 >= slots) {info.slots2 -= slots; slots = 0;}
						else break; // no empty slots
					}
				}
        DoAssert(slots == 0);
		  }
		  else if (info.muzzle2 && info.muzzle2->CanUse(type))
		  {
        // magazine can be used in muzzle2
        info.hasSlots2 += slots;
        info.hasAmmo2 += magazine->GetAmmo() / type->_maxAmmo;
				if (info.slots2 >= slots) {info.slots2 -= slots; slots = 0;}
				else
				{
					slots -= info.slots2; info.slots2 = 0;
					if (info.slots3 >= slots) {info.slots3 -= slots; slots = 0;}
					else
					{
						slots -= info.slots3; info.slots3 = 0;
						if (info.slots1 >= slots) {info.slots1 -= slots; slots = 0;}
						else break; // no empty slots
					}
				}
        DoAssert(slots == 0);
		  }
      else
      {
        // throw / put magazine
        info.hasSlots3 += slots;
        info.hasAmmo3 += magazine->GetAmmo() / type->_maxAmmo;
        if (info.slots3 >= slots) {info.slots3 -= slots; slots = 0;}
        else
        {
          slots -= info.slots3; info.slots3 = 0;
          if (info.slots2 >= slots) {info.slots2 -= slots; slots = 0;}
          else
          {
            slots -= info.slots2; info.slots2 = 0;
            if (info.slots1 >= slots) {info.slots1 -= slots; slots = 0;}
            else break; // no empty slots
          }
        }
        DoAssert(slots == 0);
      }
    }
  }

  // hand gun
  index = man->FindWeaponType(MaskSlotHandGun);
  const WeaponType *handgun = index >= 0 ? man->GetWeaponSystem(index) : NULL;

  // calculate maximal usable slots
  if (handgun && handgun->_muzzles.Size() > 0) info.muzzleHG = handgun->_muzzles[0];
  if (!info.muzzleHG) return;
  
  info.slotsHG = GetHandGunItemSlotsCount(man->GetType()->_weaponSlots);

  // subtract used slots
  for (int i=0; i<man->NMagazines(); i++)
  {
    const Magazine *magazine = man->GetMagazine(i);
    if (!magazine) continue;
    if (magazine->GetAmmo() == 0) continue;
    const MagazineType *type = magazine->_type;
    int slots = GetHandGunItemSlotsCount(type->_magazineType);

    if (slots > 0)
    {
      if (info.muzzleHG->CanUse(type))
      {
        // magazine can be used in muzzleHG
        info.hasSlotsHG += slots;
        info.hasAmmoHG += magazine->GetAmmo() / type->_maxAmmo;
        if (info.slotsHG >= slots) {info.slotsHG -= slots; slots = 0;}
        else
        {
          info.slotsHG = 0;
          return;
        }
      }
    }
  }
}

LSError Person::Serialize(ParamArchive &ar)
{
	CHECK(base::Serialize(ar))
	if (IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
	{
		CHECK(SerializeIdentity(ar))
	}
	else
	{
    if (ar.GetArVersion() < 15)
    {
      // get / set brain from AI structure
      if (ar.IsSaving())
      {
        // impossible to save old version
        Fail("Cannot be reached");
      }
      else if (ar.GetPass() == ParamArchive::PassSecond)
      {
        Ref<AIUnit> unit;
        CHECK(ar.SerializeRef("Brain", unit, 1))
        SetBrain(unit);
      }
    }
    else
    {
      // now, unit is stored here
      CHECK(ar.Serialize("Brain", _brain, 1))
    }
		CHECK(ar.Serialize("sensorRowID", _sensorRowID, 1))
	}
#if _ENABLE_IDENTITIES
  LSError result = ar.Serialize("Identity", _identity, 1);
  // class Identity can be missing in older saves
  if (result != LSOK && result != LSNoEntry) return result;
#endif
	return LSOK;
}

LSError Person::SerializeIdentity(ParamArchive &ar)
{
	CHECK(ar.Serialize("Info", _info, 1))
	if (ar.IsSaving())
	{
		float skill = _brain ? _brain->GetRawAbility() : 1.0;
		CHECK(ar.Serialize("skill", skill, 1, 1.0))
	}
	else if (ar.GetPass() == ParamArchive::PassSecond)
	{
		ar.FirstPass();
		if (_brain)
		{
			float skill;
			CHECK(ar.Serialize("skill", skill, 1, 1.0))
			_brain->SetRawAbility(skill);
		}
		ar.SecondPass();

		if (_info._face.GetLength() > 0)
			SetFace(_info._face);
		
		if (_info._glasses.GetLength() > 0)
			SetGlasses(_info._glasses);
		
		if (_info._speaker.GetLength() > 0 && _brain)
			_brain->SetSpeaker(_info._speaker, _info._pitch);
	}
	
	return LSOK;
}

#define PERSON_MSG_LIST(XX) \
  XX(UpdateGeneric, UpdateVehicleBrain)

DEFINE_NETWORK_OBJECT(Person, base, PERSON_MSG_LIST)

DEFINE_NET_INDICES_EX_ERR(UpdateVehicleBrain, UpdateVehicleAIFull, UPDATE_VEHICLE_BRAIN_MSG, NoErrorInitialFunc)

NetworkMessageFormat &Person::CreateFormat
(
	NetworkMessageClass cls,
	NetworkMessageFormat &format
)
{
	switch (cls)
	{
	case NMCUpdateGeneric:
		base::CreateFormat(cls, format);
    UPDATE_VEHICLE_BRAIN_MSG(UpdateVehicleBrain, MSG_FORMAT_ERR)
		break;
	default:
		base::CreateFormat(cls, format);
		break;
	}
	return format;
}

TMError Person::TransferMsg(NetworkMessageContext &ctx)
{
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		TMCHECK(base::TransferMsg(ctx))
		{
			PREPARE_TRANSFER(UpdateVehicleBrain)
			if (ctx.IsSending())
			{
				if (this == GWorld->GetRealPlayer())
					SetRemotePlayer(GetNetworkManager().GetPlayer());
        TRANSF(remotePlayer)
			}
      else
      {
        int remotePlayer;
        TRANSF_EX(remotePlayer, remotePlayer)
        if (_remotePlayer == AI_PLAYER && remotePlayer != AI_PLAYER)
        {
          AIBrain *brain = Brain();
          if (brain)
          {
            AIGroup *group = brain->GetGroup();
            if (group) group->OnRemotePlayerAdded();
          }
        }
        SetRemotePlayer(remotePlayer);
      }
			// TODO: ?? _sensorRowID
		}
		break;
	default:
		return base::TransferMsg(ctx);
	}
	return TMOK;
}

float Person::CalculateError(NetworkMessageContextWithError &ctx)
{
	float error = 0;
	switch (ctx.GetClass())
	{
	case NMCUpdateGeneric:
		error += base::CalculateError(ctx);
		{
			PREPARE_TRANSFER(UpdateVehicleBrain)
			ICALCERR_NEQ(int, remotePlayer, ERR_COEF_IMMEDIATE)
		}
		break;
	default:
		error += base::CalculateError(ctx);
		break;
	}
	return error;
}

void Person::SetRemotePlayer(int player)
{
  int old = _remotePlayer;
  _remotePlayer = player;
  if ( player!=old ) ApplyIdentity();
  if ( player!=1 ) GWorld->UpdateClientCameraPlayer(player, this);
}

bool Person::IsNetworkPlayer() const
{
	return _remotePlayer != 1 || this == GWorld->GetRealPlayer();
}

void Person::SimulateImpulseDamage(DoDamageResult &result, EntityAI *owner, float impulse, Vector3Par modelPos)
{
  float contact=impulse*GetInvMass();
  if (contact < 5.0f)
    contact = contact * (0.01f/5);
  else
    contact = contact * (1.0f/5)-0.99f;

  if( contact>0 )
  {
    float armorCoef = GetInvArmor()*3;
    contact *= armorCoef;

    //LogF("Contact %.2f",contact);
    saturateMin(contact,5);

    if (contact > 0.001f)
      LocalDamage(result, NULL, owner,VZero,contact,1.0f); 
  }   
}

void Person::DisableDamageFromObj(float seconds)
{ 
    _disableDamageFromObjUntil = Glob.time + seconds;
}

int Person::FindWeaponType(int maskInclude, int maskExclude) const
{
  for (int i=0; i<_weaponsState._weapons.Size(); i++)
  {
    const WeaponType *weapon = _weaponsState._weapons[i];
    if ((weapon->_weaponType & maskInclude) != 0 && (weapon->_weaponType & maskExclude) == 0)
      return i;
  }
  return -1;
}

bool CheckAccessCreate(ParamEntryPar entry);

bool Person::ForEachTurret(ITurretFunc &func) const
{
  TurretContext context;
  context._turret = NULL;
  context._turretType = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
  return func(unconst_cast(this), context);
}

bool Person::ForEachTurret(EntityVisualState& vs, ITurretFuncV &func) const
{
  TurretContextV context;
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
//  context._parentTrans = vs.Orientation();
  return func(unconst_cast(this), context);
}

  /// enumerate all turrets
bool Person::ForEachTurretEx(EntityVisualState& vs, ITurretFuncEx &func) const
{
  TurretContextEx context;
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
  context._parentTrans = vs.Orientation();
  return func(unconst_cast(this), context);
}


bool Person::ForEachCrew(ICrewFunc &func) const
{
  return func(unconst_cast(this));
}

bool Person::FindTurret(const Person *gunner, TurretContext &context) const
{
  if (gunner != this)
    return false;
  context._turret = NULL;
  context._turretType = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
  return true;
}

bool Person::FindTurret(EntityVisualState& vs, const Person *gunner, TurretContextV &context) const
{
  if (gunner != this)
    return false;
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
//  context._parentTrans = vs.Orientation();
  return true;
}

bool Person::GetPrimaryGunnerTurret(TurretContext &context) const
{
  context._turret = NULL;
  context._turretType = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
  return true;
}

bool Person::GetPrimaryGunnerTurret(EntityVisualState& vs, TurretContextV &context) const
{
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
  return true;
}

bool Person::GetPrimaryGunnerTurret(EntityVisualState& vs, TurretContextEx &context) const
{
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
  context._parentTrans = vs.Orientation();
  return true;
}

bool Person::GetPrimaryObserverTurret(TurretContext &context) const
{
  context._turret = NULL;
  context._turretType = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
  return true;
}

bool Person::GetPrimaryObserverTurret(EntityVisualState& vs, TurretContextV &context) const
{
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
  return true;
}

bool Person::GetPrimaryObserverTurret(EntityVisualState& vs, TurretContextEx &context) const
{
  context._turret = NULL;
  context._turretType = NULL;
  context._turretVisualState = NULL;
  context._gunner = unconst_cast(this);
  context._weapons = unconst_cast(&_weaponsState);
  context._parentTrans = vs.Orientation();
  return true;
}

/*!
\param weapon weapon type
\param force add weapon even if no free slots (bypass check)
\param reload try to reload weapon immediately
\return index of new weapon in array
*/
int Person::AddWeapon(WeaponType *weapon, bool force, bool reload, bool checkSelected)
{
  if (!CheckAccessCreate(*weapon->_parClass))
    return -1;
  // add weapon
  if (!force)
  {
    AUTO_STATIC_ARRAY(Ref<const WeaponType>, conflict, 16);
    if (!CheckWeapon(weapon, conflict))
      return -1;
    if (conflict.Size() > 0)
      return -1;
  }
  return _weaponsState.AddWeapon(this, weapon, reload, checkSelected);
}

/*!
\param name weapon type name
\param force add weapon even if no free slots (bypass check)
\param reload try to reload weapon immediatelly
\return index of new weapon in array
*/
int Person::AddWeapon(RStringB name, bool force, bool reload, bool checkSelected)
{
  Ref<WeaponType> weapon = WeaponTypes.New(name);
  return AddWeapon(weapon, force, reload, checkSelected);
}

/*!
\param name weapon type name
*/
void Person::RemoveWeapon(RStringB name, bool checkSelected)
{
  _weaponsState.RemoveWeapon(this, name, checkSelected);
}

/*!
\param weapon weapon type
*/
void Person::RemoveWeapon(const WeaponType *weapon, bool checkSelected)
{
  _weaponsState.RemoveWeapon(this, weapon, checkSelected);
}

void Person::RemoveAllWeapons(bool removeItems)
{
  _weaponsState.RemoveAllWeapons(this, removeItems);
}

void Person::RemoveAllItems()
{
  _weaponsState.RemoveAllItems(this);
}

void Person::MinimalWeapons()
{
  RemoveAllMagazines();
}

/*!
\param name magazine type name
\param force add magazine even if no free slots (bypass check)
\param autoreload reload magazine into weapon immediatelly
\return index of new weapon in array
*/
int Person::AddMagazine(Magazine *magazine, bool force, bool autoload)
{
  if (!CheckAccessCreate(*magazine->_type->_parClass))
    return -1;
  // add magazine type
  if (!force)
  {
    AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
    if (!CheckMagazine(magazine, conflict))
      return -1;
    //try to remove empty magazine 
    if (conflict.Size() == 1)
    {
       const Magazine *m = conflict[0];
       //this would be done later in ReplaceMagazine, but it is not called always 
       if(m && magazine && m->_type == magazine->_type && m->GetAmmo()==0)
       {
         _weaponsState.RemoveMagazine(this,m);
         conflict.Clear();
       }
    }
    if (conflict.Size() > 0)
      return -1;
  }
  return _weaponsState.AddMagazine(this, magazine, autoload);
}

/*!
\param name magazine type name
\param force add magazine even if no free slots (bypass check)
\return index of new weapon in array
*/
int Person::AddMagazine(RStringB name, bool force, bool autoload)
{
  Ref<MagazineType> magazineType = MagazineTypes.New(name);
  if (magazineType.IsNull()) return -1;
  Ref<Magazine> magazine = new Magazine(magazineType);
  magazine->SetAmmo(magazineType->_maxAmmo);
  int index = AddMagazine(magazine, force, autoload);
  if (index < 0)
    return index;
  magazine->_reload = 1;
  magazine->_reloadDuration = 0.8 + 0.2 * GRandGen.RandomValue();
  magazine->_reloadMagazine = 0;
  magazine->_reloadMagazineTotal = 0;
  return index;
}

bool FindDropPos(Matrix4 &transform);
int Person::AddBackpack(EntityAI *backpack, bool force)
{
  if(HaveBackpack())
  {
    Vector3 dirXZ = (Vector3(this->FutureVisualState().Direction().X(),0,this->FutureVisualState().Direction().Z()));
    dirXZ.Normalize();
    Vector3 position = this->FutureVisualState().Position()+ 0.5f*dirXZ + VUp*0.5f;

    Matrix3 dir;
    dir.SetUpAndDirection(VUp, FutureVisualState().Direction());

    Matrix4 transform;
    transform.SetPosition(position);
    transform.SetOrientation(dir);

    backpack->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
    backpack->SetTransform(transform);

    backpack->Init(transform, true); // do not reset movement
    backpack->OnEvent(EEInit);

    //if (backpack->IsLocal()) 
    {
      backpack->ResetMoveOut(); // mark it is in landscape
      GLOB_WORLD->AddSlowVehicle(backpack);
      GLOB_WORLD->RemoveOutVehicle(backpack);
      // we need other entities nearby to be notified of our presence
      backpack->OnMoved();
      GWorld->OnEntityMovedFar(backpack);
    }  
    if(GWorld->GetMode()  == GModeNetware)
    {      
      GetNetworkManager().DropBackpack(backpack, transform);
    }
  }
  else
  {
    GiveBackpack(backpack);
  }
  //Ref<MagazineType> magazineType = MagazineTypes.New(name);
  //Ref<Magazine> magazine = new Magazine(magazineType);
  //magazine->SetAmmo(magazineType->_maxAmmo);
  //int index = AddMagazine(magazine, force);
  //if (index < 0)
  //  return index;
  //magazine->_reload = 1;
  //magazine->_reloadDuration = 0.8 + 0.2 * GRandGen.RandomValue();
  //magazine->_reloadMagazine = 0;
  //magazine->_reloadMagazineTotal = 0;
  return -1;
}

/*!
\param name magazine type name
*/
void Person::RemoveMagazine(RStringB name)
{
  _weaponsState.RemoveMagazine(this, name);
}

/*!
\param magazine magazine to remove
*/
void Person::RemoveMagazine(const Magazine *magazine)
{
  _weaponsState.RemoveMagazine(this, magazine);
}

/*!
\param name name of magazine type to remove
*/
void Person::RemoveMagazines(RStringB name)
{
  _weaponsState.RemoveMagazines(this, name);
}

void Person::RemoveAllMagazines()
{
  _weaponsState.RemoveAllMagazines(this);
}

/*!
\param weapon checked type of weapon
\return true, if weapon can be added - in this case conflict contains weapons must be removed
*/
bool Person::CheckWeapon(const WeaponType *weapon, AutoArray<Ref<const WeaponType>, MemAllocSA> &conflict, const Ref<WeaponType> *weapons, int weaponsCount) const
{
  for (int i=0; i<weaponsCount; i++)
  {
    if (weapons[i] == weapon)
      return false;
  }

  int slots = weapon->_weaponType;
  if (slots == 0) return true;

  int maxSlots = GetType()->_weaponSlots;

  DoAssert((slots & MaskSlotItem) == 0);
  const int primaryMax = maxSlots & MaskSlotPrimary;
  const int secondaryMax = maxSlots & MaskSlotSecondary;
  const int binocularMax = maxSlots & MaskSlotBinocular;
  const int handgunMax = maxSlots & MaskSlotHandGun;
  const int inventoryMax = maxSlots & MaskSlotInventory;
  int primary = slots & MaskSlotPrimary;
  int secondary = slots & MaskSlotSecondary;
  int binocular = slots & MaskSlotBinocular;
  int handgun = slots & MaskSlotHandGun;
  int inventory = slots & MaskSlotInventory;
  if (primary > primaryMax) return false;
  if (secondary > secondaryMax) return false;
  if (binocular > binocularMax) return false;
  if (handgun > handgunMax) return false;
  if (inventory > inventoryMax) return false;

  ////if unit has back pack, it cannot carry any secondary weapon
  //if(secondary>0 && _brain && _brain->GetVehicle() && _brain->GetVehicle()->HaveBackpack()) 
  //  return false;

  // check weapons
  for (int i=0; i<weaponsCount; i++)
  {
    // check duplicity
    const WeaponType *weaponI = weapons[i];
    // check slots
    int slotsI = weaponI->_weaponType;
    int primaryI = slotsI & MaskSlotPrimary;
    int secondaryI = slotsI & MaskSlotSecondary;
    int binocularI = slotsI & MaskSlotBinocular;
    int handgunI = slotsI & MaskSlotHandGun;
    int inventoryI = slotsI & MaskSlotInventory;

    if (
      primary + primaryI > primaryMax ||
      secondary + secondaryI > secondaryMax ||
      binocular + binocularI > binocularMax ||
      handgun + handgunI > handgunMax ||
      inventory + inventoryI > inventoryMax)
    {
      conflict.Add(weaponI);
    }
    else
    {
      primary += primaryI;
      secondary += secondaryI;
      binocular += binocularI;
      handgun += handgunI;
      inventory += inventoryI;
    }
  }
  return true;
}


bool Person::CheckWeapon(const WeaponType *weapon, AutoArray<Ref<const WeaponType>, MemAllocSA> &conflict) const
{
  return CheckWeapon(weapon, conflict, _weaponsState._weapons.Data(), _weaponsState._weapons.Size());
}

static int CmpMagazines(const Ref<const Magazine> *pm1, const Ref<const Magazine> *pm2, const Person *person)
{
  const Magazine *m1 = *pm1;
  const Magazine *m2 = *pm2;

  // first remove empty magazines
  bool empty1 = m1->GetAmmo() == 0;
  bool empty2 = m2->GetAmmo() == 0;
  if (empty1 && !empty2) return -1;
  if (empty2 && !empty1) return 1;

  // then unused magazines
  bool used1 = false;
  bool used2 = false;
  for (int i=0; i<person->NWeaponSystems(); i++)
  {
    const WeaponType *weapon = person->GetWeaponSystem(i);
    for (int j=0; j<weapon->_muzzles.Size(); j++)
    {
      const MuzzleType *muzzle = weapon->_muzzles[j];
      for (int k=0; k<muzzle->_magazines.Size(); k++)
      {
        const MagazineType *type = muzzle->_magazines[k];
        if (type == m1->_type) used1 = true;
        if (type == m2->_type) used2 = true;
      }
    }
  }
  if (used1 && !used2) return 1;
  if (used2 && !used1) return -1;

  // then less full
  float full1 = m1->_type->_maxAmmo > 0 ? m1->GetAmmo() / m1->_type->_maxAmmo : 1;
  float full2 = m2->_type->_maxAmmo > 0 ? m2->GetAmmo() / m2->_type->_maxAmmo : 1;
  // CHENGED: remove magazines with lower remaining value
  float value1 = m1->_type->_value * full1;
  float value2 = m2->_type->_value * full2;

  return sign(value1 - value2);
}


/*!
\param magazine checked magazine
\return true, if magazine can be added - in this case conflict contains magazines must be removed
*/
bool Person::CheckMagazine(const Magazine *magazine, AutoArray<Ref<const Magazine>, MemAllocSA> &conflict) const
{
  return CheckMagazine(magazine->_type, conflict, _weaponsState._magazines.Data(), _weaponsState._magazines.Size(), true);
}

bool Person::CheckMagazine(const MagazineType *type, AutoArray<Ref<const Magazine>, MemAllocSA> &conflict) const
{
  return CheckMagazine(type, conflict, _weaponsState._magazines.Data(), _weaponsState._magazines.Size(), true);
}

bool Person::CheckMagazine(const MagazineType *type, AutoArray<Ref<const Magazine>, MemAllocSA> &conflict,
  const Ref<Magazine> *magazines, int magazinesCount, bool inGame) const
{
  int slots = type->_magazineType;
  if (slots == 0)
    return true;

  int maxSlots = GetType()->_weaponSlots;

  DoAssert((slots & (MaskSlotItem | MaskSlotHandGunItem)) == slots);
  const int itemMax = maxSlots & MaskSlotItem;
  const int itemMaxHandGun = maxSlots & MaskSlotHandGunItem;
  int item = slots & MaskSlotItem;
  int itemHandGun = slots & MaskSlotHandGunItem;
  int reserve = itemMax - item;
  int reserveHandGun = itemMaxHandGun - itemHandGun;
  if (reserve <= 0 && reserveHandGun <= 0) return false;

  // check magazines
  for (int i=0; i<magazinesCount; i++)
  {
    const Magazine *magazineI = magazines[i];
    if (!magazineI || !magazineI->_type) continue;
    // check slots
    int itemI = magazineI->_type->_magazineType & MaskSlotItem;
    int itemIHandGun = magazineI->_type->_magazineType & MaskSlotHandGunItem;
    if (itemI > 0 || itemIHandGun > 0)
    {
      reserve -= itemI;
      reserveHandGun -= itemIHandGun;
      // check if not used - avoid removing magazines in weapons
      if (inGame && IsMagazineUsed(magazineI)) continue;
      // do not change magazines of the same type (except empty)
      if (magazineI->_type != type || magazineI->GetAmmo() == 0)
        conflict.Add(magazineI);  // candidate for remove
    }
  }

  if (reserve >= 0 && reserveHandGun >= 0)
  {
    conflict.Clear();
    return true;
  }

  QSort(conflict.Data(), conflict.Size(), this, CmpMagazines);
  for (int i=0; i<conflict.Size();)
  {
    const Magazine *magazineI = conflict[i];
    int itemI = magazineI->_type->_magazineType & MaskSlotItem;
    int itemIHandGun = magazineI->_type->_magazineType & MaskSlotHandGunItem;
    reserve += itemI;
    reserveHandGun += itemIHandGun;
    if (itemI != 0)
    {
      DoAssert((itemI & MaskSlotItem) == itemI);
      if (reserve >= itemI)
        conflict.Delete(i);
      else
        i++;
    }
    else if (itemIHandGun != 0)
    {
      DoAssert((itemIHandGun & MaskSlotHandGunItem) == itemIHandGun);
      if (reserveHandGun >= itemIHandGun)
        conflict.Delete(i);
      else
        i++;
    }
    else
    {
      Fail("Unexpected magazine type");
      conflict.Delete(i);
    }
  }
  if (reserve >= 0 && reserveHandGun >= 0) return true;
  conflict.Clear();
  return false;
}

bool Person::CheckBackpackSpace(const MagazineType *type, AutoArray<Ref<const Magazine>, MemAllocSA> &conflictMagazine,
                                AutoArray<Ref<WeaponType>, MemAllocSA> &conflictWeapon, EntityAI *bag) const
{
  ResourceSupply *supply = NULL;
  if(bag && bag->GetSupply())  supply = bag->GetSupply();
  else return false;

  int slots = type->_magazineType;
  if (slots == 0) return true;

  //item size
  int item = 0;
  if((slots & MaskSlotItem) !=0) item = (slots & MaskSlotItem) >> 8;
  else if((slots & MaskSlotHandGunItem) !=0) item = (slots & MaskSlotHandGunItem) >> 4;

  //max items
  int maxSlots = bag->GetType()->_maxMagazinesCargo;
  // required space
  int reserve = maxSlots - item;
  // if bag is not big enough
  if (reserve <= 0) return false;

  // check magazines
  for (int i=0; i<supply->GetMagazineCargoSize(); i++)
  {
    const Magazine *magazineI = supply->GetMagazineCargo(i);
    if (!magazineI->_type) continue;
    // check slots
    int itemI = 0;
    if((magazineI->_type->_magazineType & MaskSlotItem) !=0) itemI = (magazineI->_type->_magazineType & MaskSlotItem) >> 8;
    else if((magazineI->_type->_magazineType & MaskSlotHandGunItem) !=0) itemI = (magazineI->_type->_magazineType & MaskSlotHandGunItem) >> 4;

    if (itemI > 0)
    {
      reserve -= itemI;
      // do not change magazines of the same type (except empty)
      if (magazineI->_type != type || magazineI->GetAmmo() == 0)
        conflictMagazine.Add(magazineI);  // candidate for remove
    }
  }

  //check weapons
  for (int i=0; i<supply->GetWeaponCargoSize(); i++)
  {
    const WeaponType *weaponI = supply->GetWeaponCargo(i);
    if (!weaponI->_weaponType) continue;
    // check slots
    int itemI = 1;
    if((weaponI->_weaponType & MaskSlotHandGun) !=0) itemI = 5;
    else if((weaponI->_weaponType & MaskSlotPrimary) !=0 || (weaponI->_weaponType & MaskSlotSecondary) !=0) itemI = 10;

    if (itemI > 0)
    {
      reserve -= itemI;
      conflictWeapon.Add(const_cast<WeaponType *>(weaponI));  // candidate for remove
    }
  }

  if (reserve >= 0)
  {
    conflictMagazine.Clear();
    conflictWeapon.Clear();
    return true;
  }

  QSort(conflictMagazine.Data(), conflictMagazine.Size(), this, CmpMagazines);
  for (int i=0; i<conflictMagazine.Size();)
  {
    const Magazine *magazineI = conflictMagazine[i];

    int itemI = 0;
    if((magazineI->_type->_magazineType & MaskSlotItem) !=0) itemI = (magazineI->_type->_magazineType & MaskSlotItem) >> 8;
    else if((magazineI->_type->_magazineType & MaskSlotHandGunItem) !=0) itemI = (magazineI->_type->_magazineType & MaskSlotHandGunItem) >> 4;
    reserve += itemI;

    if (itemI != 0)
    {
      if (reserve >= itemI)
        conflictMagazine.Delete(i);
      else
        i++;
    }
    else
    {
      Fail("Unexpected magazine type");
      conflictMagazine.Delete(i);
    }
  }
  
  if (reserve >= 0)
  {
    conflictWeapon.Clear();
    return true;
  }

  for (int i=0; i<conflictWeapon.Size();)
  {
    const WeaponType *weaponI = supply->GetWeaponCargo(i);
    if (!weaponI->_weaponType) continue;
    // check slots
    int itemI = 1;
    if((weaponI->_weaponType & MaskSlotHandGun) !=0) itemI = 5;
    else if((weaponI->_weaponType & MaskSlotPrimary) !=0 || (weaponI->_weaponType & MaskSlotSecondary) !=0) itemI = 10;

    reserve += itemI;

    if (itemI != 0)
    {
      if (reserve >= itemI)
        conflictWeapon.Delete(i);
      else
        i++;
    }
  }

  if (reserve >= 0) return true;
  conflictMagazine.Clear();
  conflictWeapon.Clear();
  return false;
}


bool Person::CheckBackpackSpace(const WeaponType *weapon, AutoArray<Ref<const Magazine>, MemAllocSA> &conflictMagazine,
                                AutoArray<Ref<WeaponType>, MemAllocSA> &conflictWeapon, EntityAI *bag) const
{
  ResourceSupply *supply = NULL;
  if(bag && bag->GetSupply())  supply = bag->GetSupply();
  else return false;

  //item size
  int item = 1;
  if((weapon->_weaponType & MaskSlotHandGun) !=0) item = 5;
  else if((weapon->_weaponType & MaskSlotPrimary) !=0 || (weapon->_weaponType & MaskSlotSecondary) !=0) item = 10; 

  //max items
  int maxSlots = bag->GetType()->_maxMagazinesCargo;
  // required space
  int reserve = maxSlots - item;
  // if bag is not big enough
  if (reserve <= 0) return false;

  // check magazines
  for (int i=0; i<supply->GetMagazineCargoSize(); i++)
  {
    const Magazine *magazineI = supply->GetMagazineCargo(i);
    if (!magazineI->_type) continue;
    // check slots
    int itemI = 0;
    if((magazineI->_type->_magazineType & MaskSlotItem) !=0) itemI = (magazineI->_type->_magazineType & MaskSlotItem) >> 8;
    else if((magazineI->_type->_magazineType & MaskSlotHandGunItem) !=0) itemI = (magazineI->_type->_magazineType & MaskSlotHandGunItem) >> 4;

    if (itemI > 0)
    {
      reserve -= itemI;
      conflictMagazine.Add(magazineI);  // candidate for remove
    }
  }

  //check weapons
  int nWeapons = 0;
  for (int i=0; i<supply->GetWeaponCargoSize(); i++)
  {
    const WeaponType *weaponI = supply->GetWeaponCargo(i);
    if (!weaponI->_weaponType) continue;
    // check slots
    int itemI = 1;
    if((weaponI->_weaponType & MaskSlotHandGun) !=0) itemI = 5;
    else if((weaponI->_weaponType & MaskSlotPrimary) !=0 || (weaponI->_weaponType & MaskSlotSecondary) !=0) itemI = 10; 
    //number of weapon in bag
    if(itemI>1) nWeapons++;

    if (itemI > 0)
    {
      reserve -= itemI;
      if (weaponI!= weapon)
        conflictWeapon.Add(const_cast<WeaponType *>(weaponI));  // candidate for remove
    }
  }

  //if there is space for 10 mags but not for weapon (skip for items)
  int maxWeapons = -1;
  WeaponType *weaponToRemove = NULL;
  if(item > 1 && bag->GetType()->_maxWeaponsCargo <= nWeapons)
  {
    for (int i = 0; i< conflictWeapon.Size(); i++)
    {
      int itemI = 1;
      if((conflictWeapon[i]->_weaponType & MaskSlotHandGun) !=0) itemI = 5;
      else if((conflictWeapon[i]->_weaponType & MaskSlotPrimary) !=0 || (conflictWeapon[i]->_weaponType & MaskSlotSecondary) !=0) itemI = 10;

      if(itemI > 1) 
      {
        weaponToRemove = conflictWeapon[i];
        maxWeapons = i;
        reserve += itemI;
        break;
      }
    }
  }

  if (reserve >= 0)
  {
    conflictMagazine.Clear();
    conflictWeapon.Clear();
    if(weaponToRemove) conflictWeapon.Add(weaponToRemove);
    return true;
  }

  if(bag->GetType()->_maxWeaponsCargo <= nWeapons && conflictWeapon.Size()<=0)
  {
    conflictMagazine.Clear();
    conflictWeapon.Clear();
    return true;
  }

  QSort(conflictMagazine.Data(), conflictMagazine.Size(), this, CmpMagazines);
  for (int i=0; i<conflictMagazine.Size();)
  {
    const Magazine *magazineI = conflictMagazine[i];

    int itemI = 0;
    if((magazineI->_type->_magazineType & MaskSlotItem) !=0) itemI = (magazineI->_type->_magazineType & MaskSlotItem) >> 8;
    else if((magazineI->_type->_magazineType & MaskSlotHandGunItem) !=0) itemI = (magazineI->_type->_magazineType & MaskSlotHandGunItem) >> 4;
    reserve += itemI;

    if (itemI != 0)
    {
      if (reserve >= itemI)
        conflictMagazine.Delete(i);
      else
        i++;
    }
    else
    {
      Fail("Unexpected magazine type");
      conflictMagazine.Delete(i);
    }
  }

  if (reserve >= 0)
  {
    conflictWeapon.Clear();
    if(weaponToRemove) conflictWeapon.Add(weaponToRemove);
    return true;
  }

  for (int i= 0; i<conflictWeapon.Size();)
  {
    if(i == maxWeapons)
    {
      i++;
      continue;
    }
    const WeaponType *weaponI = supply->GetWeaponCargo(i);
    if (!weaponI->_weaponType) continue;
    // check slots
    int itemI = 1;
    if((weaponI->_weaponType & MaskSlotHandGun) !=0) itemI = 5;
    else if((weaponI->_weaponType & MaskSlotPrimary) !=0 || (weaponI->_weaponType & MaskSlotSecondary) !=0) itemI = 10;

    reserve += itemI;

    if (itemI != 0)
    {
      if (reserve >= itemI)
        conflictWeapon.Delete(i);
      else
        i++;
    }
  }

  if (reserve >= 0) return true;
  conflictMagazine.Clear();
  conflictWeapon.Clear();
  return false;
}

/*!
\param magazine checked magazine taype
\return true, if magazine fits (can be loaded) in some weapon
*/
bool Person::IsMagazineUsable(const MagazineType *magazine) const
{
  for (int i=0; i<_weaponsState._weapons.Size(); i++)
  {
    const WeaponType *weapon = _weaponsState._weapons[i];
    for (int j=0; j<weapon->_muzzles.Size(); j++)
    {
      const MuzzleType *muzzle = weapon->_muzzles[j];
      for (int k=0; k<muzzle->_magazines.Size(); k++)
      {
        if (muzzle->_magazines[k] == magazine)
          return true;
      }
    }
  }
  return false;
}

/*!
\param magazine checked magazine
\return true if magazine is used (loaded) in some weapon
*/
bool Person::IsMagazineUsed(const Magazine *magazine) const
{
  for (int i=0; i<_weaponsState._magazineSlots.Size(); i++)
  {
    if (_weaponsState._magazineSlots[i]._magazine == magazine)
    {
      // Is the second part of condition needed
      if (magazine->GetAmmo() <= 0/* && GetMagazineSlot(i)._muzzle->_autoReload*/)
        // empty magazine with autoreload can be replaced
        return false;
      return true;
    }
  }
  return false;
}

/*!
\patch_internal 1.63 Date 6/3/2002 by Jirka
- Fixed: Better handgun manipulation
*/
void Person::OnWeaponChanged()
{
  if (_weaponsState._weapons.Size() > 0)
    _weaponsState.SelectWeapon(this, _weaponsState.FirstWeapon(this), true);
  else
    _weaponsState.SelectWeapon(this, -1, true);
  if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
    GWorld->UI()->ResetVehicle(this);
}
