#include "wpch.hpp"
#if _VBS3

#ifndef MORALE_HPP
// A model for the morale of an individual in response to "inate" properites
// of the individual and external events thatcan raise or lower morale.
class Morale {

#define MORALE_WINDOW_SIZE 60

public:

	


// Constructors
	Morale(Man* own, float train, float exp, float end);
	Morale(Man* own);

// Accessor methods
 float GetMorale();

 float GetMoraleMax();

 float GetMoraleLevel();
 float GetMoraleLongTerm();

 float GetSoldierQuality();
 float GetTraining();
 float GetExperience();
 float GetEndurance();
 float GetLeadership();
 float GetSkill();
 float GetEnduranceAdjustment();

 float GetMoraleRecovery();

 float GetMoraleLoss();

 float GetExertion();

 float GetNetMoraleChange();

 float GetSuppressionRadius();

 float GetInitialSuppressionRadius();

 float GetHarshPenalty();
 float GetMildPenalty();
 float GetOrderMissed();

 float GetTunnelVision() const;

 int GetTick();


// Mutator methods
 void SquadMateInjured();

 void SquadMateKilled();
 void SetWoundLevel(float level);
 void HaveBeenSuppressed();
 void HaveFired();

 void AddToMorale(float val);
 void AddToMoraleRaw(float val);
 void SetTraining(float val);
 void SetExperience(float val);
 void SetEndurance(float val);
 void SetLeadership(float val);

 void Simulate(float deltaT, float currentExertion, float volumeFire);


	private:
// Attributes
   float _moraleMax;
   float _morale;
   float _moraleLevel;
   float _moraleLongTerm;
   float _moraleWindow[MORALE_WINDOW_SIZE];
   int	 _windowIndex;
   float _basicMoraleRecovery;
   float _moraleRecovery;
   float _moraleLoss;
   float _moraleAdjustment;  // External objects can poke and prod morale by setting this value.

   float _training;
   float _experience;
   float _endurance;
   float _leadership;
   float _soldierQuality;
   float _enduranceFactor;

   float _exertion;

   float _suppressionRadius;
   float _initialSuppressionRadius;

   float _mildPenalty;
   float _harshPenalty;
   float _tunnelVision;

   int _tick;
   int _timeOfLastShot;
   int _timeOfLastTeamWounding;
   int _timeOfLastTeamDeath;

   float _woundLevel;
   float _teamCasualtyScale;

   bool _injured;   // Squad mate injured since last tick
   bool _killed;    // Squad mate killed since last tick

   float _accTime;  // Accumulated time - used to determine whether we should update morale yet or not.

   Man	 *_owner;   // The man to whom this morale "belongs"
   int _timesSuppressed;  // The number of times suppressed since last morale update.
   bool _fired;

   // Private helper methods
   void Recalculate();

};
#define MORALE_HPP
#endif
#endif //_VBS3