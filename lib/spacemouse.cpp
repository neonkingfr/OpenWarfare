/*
 * (C) 1998-2005 3Dconnexion. All rights reserved. 
 * Permission to use, copy, modify, and distribute this software for all
 * purposes and without fees is hereby grated provided that this copyright
 * notice appears in all copies.  Permission to modify this software is granted
 * and 3Dconnexion will support such modifications only is said modifications are
 * approved by 3Dconnexion.
 *
 */

/* SpaceWare Specific Includes */

#include "wpch.hpp"

#if _SPACEMOUSE
#if defined _WIN32 && !defined _XBOX

/* Program Specific Includes */

#include "SpaceMouse.hpp"

/* Function Definitions */

SpaceMouse::SpaceMouse(HWND hwnd)
{
  /* intitialize spaceball */
  int res = Init(hwnd);
  if (res < 1)
  {
    LogF("Error: No SpaceMouse found");
    //set init var    
  }
  else
  {
    LogF("SpaceMouse initialized");
  }
}

SpaceMouse::~SpaceMouse()
{
  SiTerminate();
}
/*--------------------------------------------------------------------------
 * Function: SbInit()
 *
 * Description:
 *    This function initializes the Spaceball and opens ball for use.
 * Return Value:
 *    int  res         result of SiOpen, =0 if Fail =1 if it Works
 *
 *--------------------------------------------------------------------------*/
int SpaceMouse::Init(HWND hwnd)
{
  SiOpenData oData;                    /* OS Independent data to open ball  */ 
  
  /*init the SpaceWare input library */
  if (SiInitialize() == SPW_DLL_LOAD_ERROR)  
  {
    LogF("Error: Could not load SiAppDll dll files");
    return 0;
  }

  SiOpenWinInit (&oData, hwnd);    /* init Win. platform specific data  */
  SiSetUiMode(devHdl, SI_UI_ALL_CONTROLS); /* Config SoftButton Win Display */

  /* open data, which will check for device type and return the device handle
     to be used by this function */ 
  if ( (devHdl = SiOpen ("3DxTest32", SI_ANY_DEVICE, SI_NO_MASK, SI_EVENT, &oData)) == NULL) 
  {
     SiTerminate();  /* called to shut down the SpaceWare input library */
                    /* could not open device */
     return 0; 
  }
  else
  {
     /* opened device succesfully */ 
     return 1;
  }  
}

/*--------------------------------------------------------------------------
 * Function: CheckMessage(MSG msg)
 *
 * called from expressCompiler.cpp
 * Description:
 *    Check if the incoming message is a SpaceMouse Message and Process it.
 * Return Value:
 *    bool  result true if it's a spacemouse message
 *
 *--------------------------------------------------------------------------*/
bool SpaceMouse::CheckMessage(const MSG& msg, float Axis[NAXIS], bool Buttons[NBUTTONS])
{
  SiSpwEvent     Event;   /* SpaceWare Event */ 
  SiGetEventData EData;   /* SpaceWare Event Data */
  //int  num;               /* number of button pressed */
  bool handled = false;   /* is message handled yet */ 
  
  const float scale_Axis = 80; 
 
  if(!devHdl)
    return false;
  /* init Window platform specific data for a call to SiGetEvent */
  SiGetEventWinInit(&EData, msg.message, msg.wParam, msg.lParam);
  
  /* check whether msg was a Spaceball event and process it */
  if (SiGetEvent (devHdl, 0, &EData, &Event) == SI_IS_EVENT)
  {
      if (Event.type == SI_MOTION_EVENT)
      {
        for(int i=0; i < NAXIS; ++i)
          Axis[i] = float(Event.u.spwData.mData[i]) / scale_Axis;
/*        LogF("SpaceMouse X:%d Y:%d Z:%d rx:%d ry:%d rz:%d"
          , Event.u.spwData.mData[SI_TX]
          , Event.u.spwData.mData[SI_TY]
          , Event.u.spwData.mData[SI_TZ]
          , Event.u.spwData.mData[SI_RX]
          , Event.u.spwData.mData[SI_RY]
          , Event.u.spwData.mData[SI_RZ]
         );
*/
      }
      if (Event.type == SI_ZERO_EVENT)
      {
        for(int i=0; i < NAXIS; ++i)
          Axis[i] = 0;

//        LogF("SpaceMouse Zero Event");
      }
      if (Event.type == SI_BUTTON_EVENT)
      {
        for(int i=0; i < NBUTTONS && i < 32; ++i)
          Buttons[i] = ((Event.u.spwData.bData.current & (1 << (i +1))) != 0);
        // If a button is pressed its bitfield contains a 1; if not, its bitfield contains a 0. 
        // Aside from the special button fields, the first button on the device is the 0x2 bit, 
        // the second button is the 0x4 bit, etc. These fields can be used if the SiButtonPressed 
        // and SiButtonReleased functions do not supply sufficient functionality in an application.

/*        if ((num = SiButtonPressed (&Event)) != SI_NO_BUTTON) 
        {
//          LogF("SpaceMouse Button Pressed");
        }
        if ((num = SiButtonReleased (&Event)) != SI_NO_BUTTON)  
        {
//          LogF("SpaceMouse ButtonReleased");
        }
*/
      }
    handled = true;         
  }
  return handled;
}

#endif
#endif