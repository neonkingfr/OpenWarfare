// Configuration parameters for console application

#define _VERIFY_KEY								0
#define _VERIFY_CLIENT_KEY  			0  // server is checking the CD key of clients on GameSpy
#define _VERIFY_BLACKLIST					0
#define _ENABLE_EDITOR						0
#define _ENABLE_WIZARD						0
#define _ENABLE_ALL_MISSIONS			0
#define _ENABLE_MP								0
#define _ENABLE_AI								1
#define _ENABLE_CAMPAIGN					0
#define _ENABLE_SINGLE_MISSION		0
#define _ENABLE_UNSIGNED_MISSIONS	1
#define _ENABLE_BRIEF_GRP					0
#define _ENABLE_PARAMS						1
#define _ENABLE_CHEATS						0
#define _ENABLE_ADDONS						1
#define _ENABLE_DEDICATED_SERVER	0
#define _FORCE_DEMO_ISLAND				0
#define _FORCE_SINGLE_VOICE				0
#define _ENABLE_AIRPLANES					0
#define _ENABLE_HELICOPTERS				0
#define _ENABLE_SHIPS							0
#define _ENABLE_CARS							0
#define _ENABLE_TANKS							0
#define _ENABLE_MOTORCYCLES				0
#define _ENABLE_PARACHUTES				0
#define _ENABLE_DATADISC					1
#define _ENABLE_VBS								0
#define _ENABLE_BULDOZER          1
#define _ENABLE_DISTRIBUTIONS     0
#define _USE_FCPHMANAGER          1
#define _ENABLE_DX10              0
#define _USE_BATTL_EYE_SERVER     1
#define _USE_BATTL_EYE_CLIENT     1
#define _ENABLE_NEWHEAD           1

#define _DISABLE_GUI              1

#define _TIME_ACC_MIN							(1.0 / 128.0)
#define _TIME_ACC_MAX							4.0


// default mod path 
#define _MOD_PATH_DEFAULT "CA;Expansion"
