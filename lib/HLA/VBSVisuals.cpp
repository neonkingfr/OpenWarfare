#include "VBSVisuals.hpp"

#if _VBS2
#include "VBSVisuals.hpp"
#include "AAR.hpp"

#include <El/ParamFile/paramFile.hpp>
#include <El/Evaluator/express.hpp>
#include "../global.hpp"
#include "../world.hpp"
#include "../transport.hpp"
#include "../mbcs.hpp"
#include "../camera.hpp"
#include "../landscape.hpp"
#include "../AI/ai.hpp"

#include "../diagModes.hpp"



#include "../Shape/specLods.hpp"
#include "../dikCodes.h"
#include "../keyInput.hpp"
#include "../vehicleAI.hpp"

extern ParamFile Pars;

VBSVisuals GVBSVisuals;


//tempory test, organize later
#if _VBS3_SHAPEFILES
  #include "ShapeLayers.hpp"
  ShapeLayers GShapeLayers;
#endif

PackedColor GetPackedColor(ParamEntryPar entry);

void SideColor::Init(ParamEntryPar param)
{
  static char* side[] = {"east", "west", "guerrila", "civilian"};
  for(int i=0; i < TSideUnknown; ++i)
    _color[i] = GetPackedColor(param >> side[i] >> "color");
}

void SoldierName::Init()
{
  if(!Pars.CheckIfEntryExists("VBS2Visuals")) return;

  ParamEntryVal cfgVBSVisuals = ::Pars.FindEntry("VBS2Visuals");    //read for every Line! Consider HistoryLineType
  ParamEntryVal SoldierNames = cfgVBSVisuals >> "SoldierNames";

  screen2DOffset   = SoldierNames >> "screen2DOffset";
  position3DOffset = SoldierNames >> "position3DOffset";
  textSize         = SoldierNames >> "textSize";
  maxDistance      = SoldierNames >> "maxDistance";
  alphaOutStart    = SoldierNames >> "alphaOutStart";


  static char* side[] = {"West", "East", "Guerrila", "Civilian"};
  for(int i=0; i < TSideUnknown; ++i)
  {
    ParamEntryVal item = SoldierNames >> side[i];
    
    // Load each config for each side from 
    LoadNameText(config[i],item);
    LoadDistanceText(config[i],item);
    LoadHealthBar(config[i],item);
    LoadRankImage(config[i],item);
  }
}


void SoldierName::LoadNameText(SConfig &cfg,ParamEntryPar param)
{
  ParamEntryVal nameText = param >> "NameText";
  
  cfg.nameText.color = GetPackedColor(nameText >> "nameColor");
  cfg.nameText.x     = nameText >> "x";
  cfg.nameText.y     = nameText >> "y";
}

void SoldierName::LoadDistanceText(SConfig &cfg,ParamEntryPar param)
{
  ParamEntryVal distanceText = param >> "DistanceText";
  
  cfg.distanceText.color = GetPackedColor(distanceText >> "color");
  cfg.distanceText.x = distanceText >> "x";
  cfg.distanceText.y = distanceText >> "y";
}
void SoldierName::LoadHealthBar(SConfig &cfg,ParamEntryPar param)
{
  ParamEntryVal healthBar = param >> "HealthBar";

  cfg.healthBar.barGreen  = GetPackedColor(healthBar >> "barGreen");
  cfg.healthBar.barYellow = GetPackedColor(healthBar >> "barYellowColor");
  cfg.healthBar.barRed    = GetPackedColor(healthBar >> "barRedColor");
  cfg.healthBar.x = healthBar >> "x";
  cfg.healthBar.y = healthBar >> "y";
  cfg.healthBar.h = healthBar >> "h";
}
void SoldierName::LoadRankImage(SConfig &cfg,ParamEntryPar param)
{
  ParamEntryVal rankImage = param >> "RankImage";

  cfg.rankImage.color = GetPackedColor(rankImage >> "color");
  cfg.rankImage.x = rankImage >> "x";
  cfg.rankImage.y = rankImage >> "y";
  cfg.rankImage.w = rankImage >> "w";
  cfg.rankImage.h = rankImage >> "h";
}

void VBSVisuals::Init()
{
  if(!Pars.CheckIfEntryExists("cfgAAR"))
    return;

  ParamEntryVal cfgAAR = ::Pars.FindEntry("cfgAAR");    //read for every Line! Consider HistoryLineType
  ParamEntryVal historyLine = cfgAAR >> "HistoryLine";

  historyColor.Init(historyLine);
  _historyLifeTime = historyLine >> "lifetime";

  _drawTrail = false; //read from config or editor config?
  _drawHitLine = true;
  _showTextures = false;
  _showGrid = true;
  _showContours = true;
  interfaceHidden = false;

  _soldierNameCfg.Init();
}

void VBSVisuals::ReleaseTextures()
{
  #if _VBS3_RANKS
    _rankSystems.Clear();
  #endif
}

#if _VBS3_RANKS
VBS_Rank::VBS_Rank(ParamEntryPar rank)
{
  gameRank = RankUndefined;

  // translate rank to internal system

  Rank foundRank = (Rank)(rank >> "internalRank").GetInt();
//  LogF("[m]     Adding Rank %s (%d)", cc_cast(rank.GetName()), (int)foundRank);

  if(foundRank >= RankUndefined && foundRank < NRanks )
    gameRank = foundRank;

  RString texture = rank >> "image";
  if(texture.GetLength()>0)
  {
    RString FindPicture (RString name);
    _image = GlobLoadTexture(FindPicture(texture));
    if(_image == NULL)
      LogF("[m] Error creating texture: %s", cc_cast(texture));
//    else
//      LogF("[m] Added Image: %s", cc_cast(texture));
  }

  RString textureCombat;
  if (rank.FindEntry("imageCombat"))
  {
    texture = rank >> "imageCombat";
    if(texture.GetLength()>0)
    {
      RString FindPicture (RString name);
      _imageCombat = GlobLoadTexture(FindPicture(texture));
      if(_imageCombat == NULL)
        LogF("[m] Error creating combat texture: %s", cc_cast(texture));
//      else
//        LogF("[m] Added combat Image: %s", cc_cast(texture));
    }
    else
      _imageCombat = _image;
  }
  else
    _imageCombat = _image;
}

RankSystem::RankSystem(ParamEntryPar rankSystem)
{
  Load(rankSystem);
}

void RankSystem::Load(ParamEntryPar rankSystem)
{
  _name = rankSystem.GetName();
//  LogF("[m] reading ranksystem: %s", cc_cast(_name));
  
  // reset all GameRank links to -1
  _gameRankIndex.Resize(NRanks);
  for(int i=0; i < NRanks; ++i)
    _gameRankIndex[i]=-1;

  for(int i=0; i< rankSystem.GetEntryCount(); ++i)
  {
    ParamEntryPar entry = rankSystem.GetEntry(i);
    if (!entry.IsClass()) continue;
    int rankIndex = _ranks.Add(VBS_Rank(entry));
    int gameRank = _ranks[rankIndex].gameRank;
    //if this gameRank isn't assigned yet
    if(gameRank != -1 && _gameRankIndex[gameRank] == -1)
      _gameRankIndex[gameRank] = rankIndex;
    else
      if(gameRank == -1)
        LogF("[m] GameRank(%d) invalid!", gameRank);
  } 
}

Texture* RankSystem::GetGameRankImage(Rank rank, bool officialVersion)
{
  if(rank <= RankUndefined || rank >= NRanks )
    return NULL;
  if(_gameRankIndex[rank] == -1)
    return NULL;

  if(officialVersion)
    return _ranks[_gameRankIndex[rank]]._image;
  else
    return _ranks[_gameRankIndex[rank]]._imageCombat;
}

Texture* VBSVisuals::GetRankImage(int rankSystem, Rank rank, bool officialVersion)
{
  if(rankSystem < 0 || rankSystem > _rankSystems.Size())
    return NULL;
  return _rankSystems[rankSystem].GetGameRankImage(rank, officialVersion);
}

void VBSVisuals::LoadRanks(ParamEntryPar ranks)
{
  _rankSystems.Clear();

  for (int j=0; j < ranks.GetEntryCount(); j++)
  {
    ParamEntryPar rankSystem = ranks.GetEntry(j);
    if (!rankSystem.IsClass()) continue;
    _rankSystems.Add(RankSystem(rankSystem));
  }
}
#endif

void VBSVisuals::SetHistoryLifeTime(float time)
{
  if(time <= 0) return;
  _historyLifeTime = time;

  RefreshLinesEntitys func;
  GWorld->ForEachAnimal(func);
  GWorld->ForEachVehicle(func);
}


void VBSVisuals::SetDrawHitLine(bool val)
{
  //currently drawn in AAR only!
  _drawHitLine = val;
}

void VBSVisuals::UpdateTrails()
{
  EnableDraw func(_drawTrail);
  GWorld->ForEachVehicle(func);
  GWorld->ForEachAnimal(func);
}

void VBSVisuals::SetDrawTrail(bool val)
{
  _drawTrail = val;
  UpdateTrails();
}


void VBSVisuals::DrawPausedScreen()
{
  FontID fontID(GetFontID("TahomaB"));

  Font *font=GEngine->LoadFont(fontID);
  const float yso=0.001,xso=0.001;
  PackedColor color(Color(0.83,0.83,0.83,1.0));
  const float size = 0.1;
  TextDrawAttr attrShadow(size,font,PackedBlack,false);
  TextDrawAttr attrText(size,font,color,false);

  Point2DAbs pos;
  float xs = toInt(GEngine->Width2D() * xso); saturateMax(xs, 1);
  float ys = toInt(GEngine->Height2D()* yso); saturateMax(ys, 1);

  float width = 0.5 * GEngine->GetTextWidthF(size,font,"Paused","");
  float height = 0.5 * size;
  GEngine->Convert(pos, Point2DFloat(0.5-width, 0.1-height));
  
  GEngine->DrawTextF(Point2DAbs((pos.x+xs),(pos.y+ys)),attrShadow,"Paused","");
  GEngine->DrawTextF(pos,attrText,"Paused","");
}

/*
\VBS_patch 1.00 Date [2/18/2008] by clion
- Fixed: Drawing of 3D soldiers names only enabled when interface is on and difficulty is set
*/
void VBSVisuals::Draw3DLines(bool postProccesing)
{

#if _ENABLE_CHEATS //ability to not clear 3DLines
  static bool clear = true;
  if (GInput.GetCheat1ToDo(DIK_PAUSE))
  {
    clear = !clear;
    DIAG_MESSAGE(500,"Clear " + clear ? "ON" : "OFF");
  }
#endif
  AutoArray<Line3d> &drawLines = postProccesing ? _draw3dLines : _draw3DLinesNoPP;
  if(drawLines.Size()>0)
  {
    for(int i=0; i< drawLines.Size(); ++i)
    {
      Line3d& line = drawLines[i];
      GEngine->DrawLine3D(-1, line._from, line._to, line._color, line._width, IsAlpha|IsAlphaFog|IsTransparent|DisableSun);
    }

#if _ENABLE_CHEATS //ability to not clear 3DLines
    if(clear)
#endif
    drawLines.Clear();
  }

  // We dont want to draw GUI element when post proccessing is on
  if(!postProccesing)
  {
  // Drawing code moved from Draw3D.
#if _AAR
    GAAR.Draw();
    if(rteVisible)
    {
      DrawLinesEntitys draw;
      GWorld->ForEachVehicle(draw);
    }
#endif

#if _VBS3_SOLDIERNAMES
    // Draw soldiers names on the battlefield from
    // Players persepective.
    if(Glob.config.IsEnabled(DTSoldierNames) && !GVBSVisuals.interfaceHidden)
      DrawSoldiersNames();
#endif

    // User defined text should be drawn above everthing else
    DrawDisplayTextEntitys drawText;
    GWorld->ForEachVehicle(drawText);

    if(GWorld->IsPaused() && !interfaceHidden )
#if _EXT_CTRL
      if(!GAAR.IsLoaded())
#endif
        DrawPausedScreen();
  }
}

float VBSVisuals::GetMinDist2Line() const
{
  const Vector3 &position = GScene->GetCamera()->Position();
  float minDist2 = FLT_MAX;
  for(int i = 0; i < _draw3dLines.Size(); i++)
  {
    const Line3d &l = _draw3dLines[i];
    saturateMin(minDist2, position.Distance2(l._from));
    saturateMin(minDist2, position.Distance2(l._to));
  }
  return minDist2;
}

void DrawSphere(Vector3 pos, float size, PackedColor color)
{
  Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
  obj->SetPosition(pos);
  obj->SetScale(size);
  obj->SetConstantColor(color);
  GScene->ShowObject(obj);
}

#include "../keyInput.hpp"
#include "../dikCodes.h"

void VBSVisuals::DrawHeightNodes()
{
  const float radius = 500;
  Camera* camera = GScene->GetCamera();
  if(!camera)
    return;

  Vector3 pos = camera->Transform().Position();
  Vector3 tl = pos + Vector3(-radius,0,radius);
  Vector3 br = pos + Vector3(radius,0,-radius);

  float invTerraindGrid = GLandscape->GetInvTerrainGrid();
  float terrainGrid = GLandscape->GetTerrainGrid();
  int subDiv = GLandscape->GetSubdiv();

  int l_xx = toInt(tl.X() * invTerraindGrid);
  int r_xx = toInt(br.X() * invTerraindGrid);

  int t_zz = toInt(tl.Z() * invTerraindGrid);
  int b_zz = toInt(br.Z() * invTerraindGrid);

  PackedColor colT = PackedColor(0,255,0,255);
  PackedColor colL = PackedColor(255,0,0,255);

  for(int x = l_xx; x <= r_xx; ++x)
    for(int z = b_zz; z <= t_zz; ++z)
    {
      if (TerrainInRange(z, x))
      {
        Vector3 p(float(x)*terrainGrid, GLandscape->GetHeight(z, x), float(z)*terrainGrid);
        if(camera->IsClipped(p,1.0f) == 0)
          DrawSphere(p, 0.5f, (x % subDiv ==0 && z % subDiv ==0) ? colL : colT);
      }
    }
}

//Draws all objects in 3D makes sure that clipping works
//Lines are drawn in Draw3DLines
#include "../ai/ai.hpp"
#include "../soldierold.hpp"
//Functor for drawing Lasers
class DrawLasers
{
protected:
  VisionMode _vm;
public:
  ::DrawLasers(VisionMode vm):_vm(vm){}
  bool DrawLasers::operator() (Object *obj) const
  {
    Man* man = dyn_cast<Man>(obj);
    if(!man) return false;
    if(!man->IsPilotLight()) return false;
        man->DrawLaserPointer(_vm, man->GetLightMode());
    return false;
  };
};

bool VBSVisuals::Draw3D()
{
  if(editor3DVisible
#if _AAR
    && !GAAR.IsReplayMode()
#endif
    )
  {
    //no draw for animals, since they don't need a bounding box
    DrawSelectedObjects drawSelect;
    GWorld->ForEachVehicle(drawSelect);
    //ForEachFastVehicle(drawSelect); // don't think fastvehicles need boundingboxes
    GWorld->ForEachSlowVehicle(drawSelect);
  } 
  
  DrawAttachedObjects drawAttached;
  GWorld->ForEachVehicle(drawAttached);
  GWorld->ForEachSlowVehicle(drawAttached);

#if _VBS3_SHAPEFILES
  if(!rteVisible || editor3DVisible) //don't draw in map mode
  {
    GShapeLayers.Draw();
  }
#endif

  //Draw Weapon trajectory helpers for soldiers + lasers
  AIBrain *brain = GWorld->FocusOn();
  if(Glob.config.IsEnabled(DTGrenadeTrajectory))
  if(brain)
  {
    if (!GWorld->HasOptions() && GWorld->UserDialog() == NULL)
    {
      if(brain && brain->IsPlayer() && !brain->GetVehicleIn())
        if(brain->GetPerson())
          brain->GetPerson()->ShowTrajectory();
    }

    if(brain->GetPerson())
    {
      DrawLasers draw(brain->GetPerson()->GetVisionMode());
      GWorld->ForEachVehicle(draw);
    }
  }

//  Draw3DLines(); //called separately now after Terrain is drawn

#if _VBS3_CHEAT_DIAG
  if (CHECK_DIAG(DETerrain))
    DrawHeightNodes();
#endif

  return true;
}

bool VBSVisuals::DrawExt(float alpha, CStaticMap& map)
{
  if(GWorld->IsPaused() && !interfaceHidden)
#if _EXT_CTRL
    if(!GAAR.IsLoaded())
#endif
    DrawPausedScreen();

#if _AAR
    // Call the AAR drawing routine
    GAAR.DrawExt(alpha,map);
    DrawLinesExt draw(alpha,map);
    GWorld->ForEachAnimal(draw);
    GWorld->ForEachVehicle(draw);
#endif
#if _VBS3_SHAPEFILES
    GShapeLayers.DrawExt(1, map);
#endif
  return true;
}

//////////////////////////////////////////////////////////////////////////
// HistoryLine 
//////////////////////////////////////////////////////////////////////////
void HistoryLine::Init(EntityAI *entity)
{
  _entity = entity; //rather be OLINK?
  _doDraw = GVBSVisuals.GetDrawTrail();
}

void HistoryLine::AddPath()
{
  Assert(_entity);

  // if we're attached to some object, dont draw
  // our history line
  if(_entity->IsAttached()) return;

  Vector3 cur = _entity->Position();

  if(!_path.Empty())
  {
    Vector3 pre = _path.Last()->pos;
    if(cur.Distance2(pre) < 4.0f) 
      return;
  }
  PathTimes* newItem = new PathTimes();
  _path.Add(newItem);

#if _AAR
  newItem->arriveTime = GAAR.IsPlaying() ? GAAR.CurrentTime() : Glob.time.toFloat();
#else
  newItem->arriveTime = Glob.time.toFloat();
#endif
  newItem->pos = cur; 
}

// Removes PathTime from linkList and delete it
void HistoryLine::DeletePath(PathTimes *item)
{
  Assert(item);
  item->Delete();
  delete item;
}

void HistoryLine::Clear()
{  
  //Clear doesn't delete the objects!
  while(_path.First())
    DeletePath(_path.First());
}

void HistoryLine::RemoveOldUpdates()
{
  if(_path.Empty()) return;

#if _AAR
  float currentTime = GAAR.IsPlaying() ? GAAR.CurrentTime() : Glob.time.toFloat();
#else
  float currentTime = Glob.time.toFloat();
#endif

  PathTimes* actItem = _path.First();
  float lifeTime = GVBSVisuals.GetHistoryLifeTime();

  while(actItem 
    && (actItem->arriveTime < currentTime - lifeTime
        || actItem->arriveTime > currentTime)
    )
  {
    DeletePath(actItem);
    actItem = _path.First();
  }

  actItem = _path.Last();
  while(actItem 
    && (actItem->arriveTime < currentTime - lifeTime
    || actItem->arriveTime > currentTime)
    )
  {
    DeletePath(actItem);
    actItem = _path.Last();
  }
}

void HistoryLine::Draw()
{
  if(!_doDraw) return;

  if(_path.Size() < 2) return;

  PathTimes* pathc = _path.First();
  PathTimes* pathn = _path.Next(pathc);

  Vector3 offset(0.0f,0.2f,0.0f);
  PackedColor color = GVBSVisuals.historyColor.GetColor(_entity->GetTargetSideAsIfAlive());

  while(pathn)
  {
    GVBSVisuals.Add3dLine(pathc->pos + offset, pathn->pos + offset, color,3.0,false);
    pathn = _path.Next(pathc = pathn);
  }
  
  // Draw the last link to the object
  GVBSVisuals.Add3dLine(pathc->pos + offset, _entity->Position() + offset, color,3.0,false); 
}

#include "../ui/uiMap.hpp"
void HistoryLine::DrawExt(float alpha,CStaticMap &map)
{
  if(!_doDraw) return;

  if(_path.Size() < 2) return;

  PathTimes* pathc = _path.First();
  PathTimes* pathn = _path.Next(pathc);

  Vector3 offset(0.0f,0.2f,0.0f);
  PackedColor color = GVBSVisuals.historyColor.GetColor(_entity->GetTargetSideAsIfAlive());

  while(pathn)
  {
    DrawCoord pt1 = map.WorldToScreen(pathc->pos + offset);
    DrawCoord pt2 = map.WorldToScreen(pathn->pos + offset);

    GLOB_ENGINE->DrawLine
      (
        Line2DPixel(
            pt1.x * map._wScreen, pt1.y * map._hScreen,
            pt2.x * map._wScreen, pt2.y * map._hScreen
            ),
          color, color, map._clipRect
      );
    pathn = _path.Next(pathc = pathn);
  }

  // Draw the last link to the object
  DrawCoord cpos = map.WorldToScreen(_entity->Position() + offset);
  DrawCoord pt1 = map.WorldToScreen(pathc->pos + offset);

  GLOB_ENGINE->DrawLine
  (
    Line2DPixel(
      cpos.x * map._wScreen, cpos.y * map._hScreen,
      pt1.x * map._wScreen, pt1.y * map._hScreen
     ),
    color, color, map._clipRect
  );
}
//////////////////////////////////////////////////////////////////////////
/// Functors for ForEach procedures
//////////////////////////////////////////////////////////////////////////
//Functor for updating lines
bool RefreshLinesEntitys::operator() (Entity *obj) const
{
  EntityAI *ent = dyn_cast<EntityAI>(obj);
  if(ent)
  {
    HistoryLine &hline = ent->GetHistoryLine();
    hline.RemoveOldUpdates();
  }

  return false;
};


inline Vector3 MinMaxCorner
(
 const Vector3 *minMax, int x, int y, int z
 )
{
  return Vector3(minMax[x][0],minMax[y][1],minMax[z][2]);
}

void DrawSelectBox(Object* obj, PackedColor color, int highlightFlags)
{
  Vector3 minMax[2];
  minMax[0] = obj->GetShape()->MinMax()[0];
  minMax[1] = obj->GetShape()->MinMax()[1];

  // Change thickness of the color to the usual 3 pixels
  color.SetA8(color.A8() * 30 / 255);

  // draw 12 lines
  static const int lines[12][2][3]=
  {
    {{0,0,0},{0,0,1}}, // diff in Z
    {{0,1,0},{0,1,1}},
    {{1,0,0},{1,0,1}},
    {{1,1,0},{1,1,1}},

    {{0,0,0},{0,1,0}}, // diff in Y
    {{0,0,1},{0,1,1}},
    {{1,0,0},{1,1,0}},
    {{1,0,1},{1,1,1}},

    {{0,0,0},{1,0,0}}, // diff in X
    {{0,0,1},{1,0,1}},
    {{0,1,0},{1,1,0}},
    {{0,1,1},{1,1,1}},
  };

  for (int l=0; l<12; l++)
  {
    // get from
    Vector3 from = MinMaxCorner(minMax,lines[l][0][0],lines[l][0][1],lines[l][0][2]);
    Vector3 to   = MinMaxCorner(minMax,lines[l][1][0],lines[l][1][1],lines[l][1][2]);
    GVBSVisuals.Add3dLine(obj->PositionModelToWorld(from), obj->PositionModelToWorld(to), color);
//    GEngine->DrawLine3D(obj->PositionModelToWorld(from), obj->PositionModelToWorld(to), color, 0);
  }

  if(highlightFlags & HIGHLIGHT_SNAP_TO_GROUND)
  {
    float size = obj->GetShape()->BoundingSphere() * 0.1;
    saturateMin(size, 1);
    DrawSphere(obj->PositionModelToWorld(MinMaxCorner(minMax,0,0,0)), size, color);
    DrawSphere(obj->PositionModelToWorld(MinMaxCorner(minMax,0,0,1)), size, color);
    DrawSphere(obj->PositionModelToWorld(MinMaxCorner(minMax,1,0,0)), size, color);
    DrawSphere(obj->PositionModelToWorld(MinMaxCorner(minMax,1,0,1)), size, color);
  }
};
//Functor for drawing attached objects in 3D, e.g. rope
bool DrawAttachedObjects::operator() (Object *obj) const
{
  if(obj && obj->IsAttached())
    obj->DrawAttached();
  return false;
};

//Functor for drawing selected objects in OME/ RTE
bool DrawSelectedObjects::operator() (Object *obj) const
{
  if(obj && obj->_highlighted  != 0 && obj->GetShape())
  {
      DrawSelectBox(obj,  obj->_highlightedColor, obj->_highlighted);
  }
  return false;
};

//Functor for drawing Line for every Vehicle in 3D
bool DrawLinesEntitys::operator() (Entity *obj) const
{
    EntityAI *ent = dyn_cast<EntityAI>(obj);
    if(ent)
    {
      HistoryLine &hline = ent->GetHistoryLine();
      hline.Draw();
    }
    return false;
};

//Functor for drawing Line for every Vehicle in 3D
bool DrawDisplayTextEntitys::operator() (Entity *obj) const
{
  const DisplayText &_displayText = obj->GetDisplayText();

#if _VBS3
  if(_displayText.text.GetLength()>0)
  {
    RString text = _displayText.text;
    //do we want to display a variable like URN
    if(text[0] == '#')
    {
      if(text.GetLength()-1 == 0)
        return false;

      text = text.Substring(1, text.GetLength());
      GameVarSpace *vars = obj->GetVars();
      if (vars)
      {
        GameValue var;
        if (vars->VarGet(text, var))
        {
          if (var.GetType()==GameString)
            text = var.GetData()->GetString();
          else
            text = var.GetText();
        }
      }
    }
    Camera *camera = GScene->GetCamera();
    FontID fontID(GetFontID("TahomaB"));
    Font *font = GEngine->LoadFont(fontID);

    if(font && camera)
    {
      Vector3 pos = obj->PositionModelToWorld(_displayText.comPosOffset + obj->GetCenterOfMass());

      if(_displayText.render3DText)
      {
        Vector3 &cPos = pos;

        Matrix3P mY;
        mY.SetRotationY(-90.0f*(H_PI/180.0f));
        Vector3 dir = mY * camera->Orientation().Direction();
        mY.SetUpAndDirection(VUp,dir);

        Vector3 textSize = GEngine->GetText3DWidth(mY.Direction() * _displayText.textSize,font,text.Data());
        float textWidth = (textSize*0.5f).Size();
        cPos -= (mY.Direction()*textWidth);

        GEngine->DrawText3D(-1, cPos,
          VUp * _displayText.textSize,
          mY.Direction() * _displayText.textSize,
          ClipAll,font,_displayText.color,0,text.Data());
      }
      else
      {
        Vector3 cPos = camera->GetInvTransform()*pos;

        TextDrawAttr attrText(_displayText.textSize,font,_displayText.color,false);
        float textWidth = GEngine->GetTextWidthF(_displayText.textSize,font,"%s",text.Data());

        if (cPos.Z() > 0.0)
        {
          float invZ = 1.0 / cPos.Z();
          float screenPosX = 0.5 * (1.0 + cPos.X() * invZ * camera->InvLeft());
          float screenPosY = 0.5 * (1.0 - cPos.Y() * invZ * camera->InvTop()); 

          screenPosX -= textWidth * 0.5;

          Point2DAbs textPos;
          GEngine->Convert(textPos, Point2DFloat(screenPosX, screenPosY));

          // Draw text using absolute position
          GEngine->DrawTextF(textPos,attrText,"%s",text.Data());
        }
      }
    }
  }
#endif

  return false;
};

//Functor for drawing Lines on the map
bool DrawLinesExt::operator() (Entity *obj) const
{
  EntityAI *ent = dyn_cast<EntityAI>(obj);
  if(ent)
  {
    HistoryLine &hline = ent->GetHistoryLine();
    hline.DrawExt(_alpha,_map);
  }
  return false;
};
//Functor to enable and disable trail drawing
bool EnableDraw::operator () (Entity *obj) const 
{
    EntityAI *ent = dyn_cast<EntityAI>(obj);
    if(ent)
    {
      HistoryLine &hline = ent->GetHistoryLine();
      hline.SetDraw(enable);
    }

    return false;
};

/** 
TODO LIST:
* Move color,size,position data into configuration file.
* Submit new configuration file, then move onto next part.
*/
/**
Leassons learned.
* Drawing 2D text then resizing it base on distance is not a good idea(Scaling issues)
* Drawing 3D text then relising that its not clear for the screen(Blury edges)
* Draw simply 2D over the character and place it above his head works pretty well
*/
void VBSVisuals::DrawSoldiersNames()
{
  Camera *camera = GScene->GetCamera();
  Person *player = GWorld->GetRealPlayer();
  bool inCutscene = (GWorld->GetCameraEffect() || GWorld->GetCameraScript());
  if(!camera) return;

  FontID fontID(GetFontID("TahomaB"));
  Font *font=GEngine->LoadFont(fontID);

  const Vector3 &camPos = camera->Position();

  int xMin, xMax, zMin, zMax;
  const float limit = _soldierNameCfg.maxDistance;
  float minDist2 = Square(limit);
  ObjRadiusRectangle(xMin, xMax, zMin, zMax, camPos, camPos, limit);


  for(int x=xMin; x<=xMax; x++) 
  {
    for(int z=zMin; z<=zMax; z++)
    {
      const ObjectListUsed &list = GLandscape->UseObjects(x,z);
      int n = list.Size();

      for(int i=0; i<n; i++)
      {
        Object *obj = list[i];
        Soldier *soldier = dyn_cast<Soldier>(obj);            
        if(!soldier) continue;// only show person types

        // Only show player side
        // Show all players when in cutscene
        // Dont show players id
        if(!inCutscene && soldier &&
          (soldier->GetTargetSide() != soldier->GetTargetSide()) ||
          (player == soldier))
          continue;

        SConfig &cfg = _soldierNameCfg.config[soldier->GetTargetSideAsIfAlive()];

        const float position3DOffset =  _soldierNameCfg.position3DOffset;
        const float screen2DOffset   =  _soldierNameCfg.screen2DOffset;
        const float textSize         =  _soldierNameCfg.textSize;

        float dist2 = obj->Position().Distance2(camPos);
        if (dist2 <= minDist2)
        {
          Matrix4Val camInvTransform = camera->GetInvTransform();

          Vector3 displayPos = soldier->COMPosition();
          Vector3 offsetPos(0,position3DOffset,0);
          Vector3 cpos = camInvTransform * (displayPos + offsetPos);

          RString playerName = soldier->GetInfo()._name;
          if(playerName.GetLength() == 0 )
          {
            GameVarSpace *vars = soldier->GetVars();
            if (vars)
            {
              GameValue var;
              if (vars->VarGet("URN", var))
                playerName = var.GetData()->GetString();
            }
          }

          // Linear interpolate alpha channel start would be 30, limiting being 50
          const float maxAlpha = 0.0f;
          const float minAlpha = 1.0f;
          const float minDist  = Square(_soldierNameCfg.alphaOutStart);
          const float maxDist  = Square(limit);

          float gAlpha = minAlpha + (dist2 - minDist)*(maxAlpha - minAlpha)/(maxDist - minDist);
          saturate(gAlpha,0.0f,1.0f);


          //////////////////////////////////////////////////////////////////////////
          // Draw the players name, dont draw AI names makes no sense
          PackedColor color = cfg.nameText.color;
          color.SetA8(PackColorComponent(gAlpha));

          TextDrawAttr attrText(textSize,font,color,false);
          float textWidth = GEngine->GetTextWidthF(textSize,font,"%s",playerName.Data());

          if (cpos.Z() <= 0) continue;
          float invZ = 1.0 / cpos.Z();
          float screenPosX = 0.5 * (1.0 + cpos.X() * invZ * camera->InvLeft());
          float screenPosY = 0.5 * (1.0 - cpos.Y() * invZ * camera->InvTop());         

          // Working in 2D viewport coordinates
          screenPosX += cfg.nameText.x;
          screenPosY += cfg.nameText.y + screen2DOffset;

          AIBrain *brain = soldier->Brain();
          AIGroup *group = brain ? brain->GetGroup() : NULL;
          bool    isAnyPlayer = brain ? brain->IsAnyPlayer() : false;
          bool    isLeader    = group ? group->Leader() == brain : false;
          if( isAnyPlayer || isLeader ) 
          {
            // Convert 2D viewport to absolute screen position, apply aspect ratio
            Point2DAbs pos;
            GEngine->Convert(pos, Point2DFloat(screenPosX, screenPosY));

            // Draw text using absolute position
            GEngine->DrawTextF(pos,attrText,"%s",playerName.Data());
          }
          //////////////////////////////////////////////////////////////////////////

          //////////////////////////////////////////////////////////////////////////
          // Draw the Rank
#if _VBS3_RANKS
          const float rankImageX = cfg.rankImage.x;
          const float rankImageY = cfg.rankImage.y;;
          const float rankImageWidth = cfg.rankImage.w;
          const float rankImageHight = cfg.rankImage.h;

          if( !(isAnyPlayer || isLeader) )
            textWidth = (screenPosX+rankImageX + rankImageWidth) - (screenPosX+cfg.healthBar.x);

          AIUnitInfo &unitInfo = soldier->GetInfo();
          Texture* texture = GVBSVisuals.GetRankImage(0,unitInfo._rank, true);
          if(texture)
          {
            PackedColor color = cfg.rankImage.color;
            color.SetA8(PackColorComponent(gAlpha));

            MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);

            Rect2DAbs rankRec;
            GEngine->Convert(rankRec,
              Rect2DFloat(screenPosX+rankImageX,screenPosY+rankImageY,rankImageWidth,rankImageHight));

            mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
            GEngine->Draw2D(mip, color,rankRec);
          }
#endif
          //////////////////////////////////////////////////////////////////////////

          //////////////////////////////////////////////////////////////////////////
          // Draw the health under the name
          const float healthBarWidth  = textWidth;
          const float healthBarX      = cfg.healthBar.x;
          const float healthBarY      = cfg.healthBar.y;
          const float healthBarHeight = cfg.healthBar.h;


          float health = 0;
          PackedColor healthBarcolor = PackedColor(Color(0,0,0,gAlpha));  
          health = 1 - soldier->GetTotalDamage();
          saturate(health, 0, 1);

          // Move into configuration files
          if (health > 0.5)
            healthBarcolor = cfg.healthBar.barGreen;
          else if (health > 0.3)
            healthBarcolor = cfg.healthBar.barYellow;
          else if (health > 0.15)
            healthBarcolor = cfg.healthBar.barRed;

          // set the global alpha channel
          healthBarcolor.SetA8(PackColorComponent(gAlpha));

          MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);

          Rect2DAbs healthBarPos;
          GEngine->Convert(healthBarPos,
            Rect2DFloat((screenPosX+healthBarX),(screenPosY+healthBarY),healthBarWidth*health,healthBarHeight));
          GEngine->Draw2D(mip, healthBarcolor,healthBarPos);
          //////////////////////////////////////////////////////////////////////////


          //////////////////////////////////////////////////////////////////////////
          // Draw the distance first from camera/person
          const float distancePosX = cfg.distanceText.x;
          const float distancePosY = cfg.distanceText.y;

          float distFrom = obj->Position().DistanceXZ(camPos);

          PackedColor  colorDist = cfg.distanceText.color;
          colorDist.SetA8(PackColorComponent(gAlpha));
          TextDrawAttr attrTextDist(textSize,font,colorDist,false);

          Point2DAbs posDist;
          GEngine->Convert(posDist,Point2DFloat(screenPosX+distancePosX, screenPosY+distancePosY));
          GEngine->DrawTextF(posDist,attrTextDist,"%2.0fm%",distFrom);
          //////////////////////////////////////////////////////////////////////////


          /*
          3D Version bellow, not used anymore. Keeping it for future 3D text rendering

          // Linear interpolate the font size depending on the distance
          const float maxFont = 1.16f;
          const float minFont = 0.17f;
          const float minDist = 0.0f;
          const float maxDist = Square(limit);

          float fontSize = minFont + (dist2 - minDist)*(maxFont - minFont)/(maxDist - minDist);


          PackedColor color(Color(0.0,0.0,1.0,1.0));

          Matrix3P mY;
          mY.SetRotationY(-90.0f*(H_PI/180.0f));
          Vector3 dir = mY * camera->Orientation().Direction();
          mY.SetUpAndDirection(VUp,dir);

          Vector3 textSize = GEngine->GetText3DWidth(mY.Direction() * fontSize,font,playerName.Data());
          float textWidth = (textSize*0.5f).Size();
          displayPos -= (mY.Direction()*textWidth);

          displayPos[1] += fontWithSize->Height() * fontSize;         

          GEngine->DrawText3D(displayPos,
          VUp * fontSize,
          mY.Direction() * fontSize,
          ClipAll | (MSShiningAdjustable * ClipUserStep),font,color,0,playerName.Data());
          */
        }     
      }
    }
  }
}

#endif //_VBS2
