//#ifdef _MSC_VER
//#pragma once
//#endif

#ifndef _MOTIONCONTROLLER_H
#define _MOTIONCONTROLLER_H
#include "../wpch.hpp"

#if _MOTIONCONTROLLER

/*
#include "../network/nettransport.hpp" //required here, otherwise we'll receive: winsock2.h(400) : error C2011: 'sockaddr_in' : 'struct' type redefinition
#include "../network/network.hpp"
#include "../Network/networkImpl.hpp"
*/
#include "../vehicleAI.hpp"


// *************************************************************************************
// Freedom Works, Inc. - cmenge@gmail.com
// The following format must be strickly followed. 
// *************************************************************************************
// packet sent to Titan
struct MotionPacket
{
  unsigned int	time;   //:time in milliseconds (to check order)
  float			AngVel[3];	// Angular Velocity     :3 floats
  float			HPR[3];		  // Orientation          :3 floats Heading, Pitch, Roll
  float			Accel[3];	  // Acceleration         :3 floats X, Y, Z - meters/sec2
  float			Vel[3];		  // Velocity             :3 floats X, Y, Z
  int				Pos[3];		  // Position             :3 ints X, Y, Z - metres
  BOOL			MOTION;		  // RUN = true / STOP = false
};
// *************************************************************************************

class MotionController //fixed for freedom motioncontroller atm
{
private:
//  struct in_addr _addr;
  struct sockaddr_in {  //we don't have to include winsock here then
    short   sin_family;
    unsigned short sin_port;
//    struct  in_addr sin_addr;
    unsigned long sin_addr;
    char    sin_zero[8];
  }_ControllerAddr;

  int sockfd;
  bool _inMotion;

  unsigned long _lastSent;

  void SendData(char *buf, int size);

  // Limit FPS
  int _maxRate; 

public:
  MotionController();
  ~MotionController();

  bool Init(RString IP, int Port, int maxRate);
  void Update();
  void SetInMotion(bool val = true) {_inMotion = val;}
  OLink(EntityAI) controlledVehicle;
};

extern MotionController GMotionController;

#endif //_MOTIONCONTROLLER
#endif //_MOTIONCONTROLLER_H


