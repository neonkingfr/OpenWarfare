#ifdef _MSC_VER
#pragma once
#endif

#include "..\wpch.hpp"

#if _VBS3_SHAPEFILES

#ifndef _SHAPE_LAYERS_HPP
#define _SHAPE_LAYERS_HPP

#include "Es\Strings\rString.hpp"
#include "El\Shapes\ShapeOrganizer.h"
#include "El\Pathname\Pathname.h"
#include "El\Color\colors.hpp"

#include "El/Shapes/ShapePolygon.h"

//#include "El\Shapes\ComputationalGeometry\CG_Polygon2.h"

#include "..\object.hpp"
using namespace ShapeFiles;

class CStaticMap;

class ShapeLayer
{
private:
  double _origin[2];
  ShapeFiles::ShapeOrganizer* _shapeOrg;
  PackedColor _color;
  
  RefArray<ObjectColored> _objects; //stores an objects to draw
  RefArray<LODShapeWithShadow> _drawShapes;
  RString _name;

protected:
//  bool AddRoadSegement(RoadLink *item, VertexArray *vertexes = NULL);
  void DrawRoads(int x, int z);

// prepares the shape for internal use
// e.g. orders Polygons to CCW
  bool Prepare();

//create an object for drawing the shape
  bool AddDrawShape(ShapePolygon* shp);

public:
  bool hidden;

  ShapeLayer();
  ~ShapeLayer();

  void SetOrigin(double x, double z);
  
  //load a new shapefile
  bool Load(RString name, bool missionDir = false);

  //Draw the Shape
  bool Draw();

  //Draw the shape on a map
  bool DrawExt(float alpha, CStaticMap &map);

  //projects the shape into VBS coords (pure substract of offset)
  bool Project();

  //missing export function of shape files
  bool ExportShapeLayer(RString fileName);

  PackedColor GetColor(){return _color;}
  bool SetColor(const PackedColor& color){_color = color; return true;}

  RString GetName() {return _name;}
};
TypeIsGeneric(ShapeLayer)

//extern ShapeLayer GShapeLayer;

class ShapeLayers
{

protected:
  AutoArray<ShapeLayer> _shapeLayers;

public:
  ShapeLayers();

  // loads and add a new shapefile, returns index or -1 if failed
  // Shapes can be loaded from mission or User Directory
  int AddShape(RString name, bool missionDir = false);

  //removes a shape
  bool RemoveShape(int index);

  //draw all visible shapefiles
  bool Draw();

  //draw all visible shapefiles
  bool DrawExt(float alpha, CStaticMap& map);

  // Get the Shaped based on his index
  ShapeLayer* GetShape(int index);
  // Get the shape based on it's name
  ShapeLayer* GetShape(RString name);

  int GetCount() { return _shapeLayers.Size();}

  void Clear();

  int GetShapeIndex(RString name);
};

extern ShapeLayers GShapeLayers;

#endif //_SHAPE_LAYERS_HPP
#endif //_VBS3_SHAPEFILES