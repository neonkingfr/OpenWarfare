#include "../wpch.hpp"

#if _AAR
#ifndef AAR_MODULE
#define AAR_MODULE

#include <Es/Types/pointers.hpp>
#include <El/QStream/qStream.hpp>
#include "AARTypes.hpp"

#include "../arcadeWaypoint.hpp"
#include "../object.hpp"

bool IsNonStaticEntity(const EntityId &Id);
bool ValidateData(const EntityId &id);
bool IsNullEntityId(const EntityId &id);

/**
Note implimentation of new HLA commands in version 7. Older structure needs to be converted
to the new structure for AAREntityUpdate.
*/

/*
Note all version changes

\VBS_patch_internal 1.00 Date [2/14/2008] by clion
  - Version 11 - Introduction of layered markers
*/
#define CURRENT_AAR_VERSION 14

enum AARNetworkMessage
{
  StartRecord,
  StopRecord,
  ClearRecord,
  SaveFile,
  SaveState,
  AddBookMK,
  RemBookMK
};

enum AARLoadError
{
  AAR_OK,
  AAR_EOF,
  AAR_FailedLoad
};

class BulletIcon
{
public:
  RString _icon;
  float   _size;
  PackedColor _color;
  BulletIcon();
  void Init(ParamEntryVal entry);
};

class HitLine
{
public:
  PackedColor _color;
  HitLine();
  void Init(ParamEntryVal entry);
};

struct Detonation
{
  EntityId FiringId;
  Vector3  sPos;
  EntityId TargetId;
  Vector3  tPos;
  float    time;
};
TypeIsSimple(Detonation);

struct LogEvent
{
  int   id;				// Event index number 
  float startTime;// start time in AAR time format 1 = a second
  float duration;	// duration   in AAR time format 1 = a second

  long    freq;			// Radio frequency the event was recorded at
  RString source;		// Source id the event was recorded at

  bool    played;
};
TypeIsGeneric(LogEvent);

class DynamicLoadMessage : public RequestableObject
{
public:
  Ref<AARMessageBase> _message;
  QFileSize    _startPos;
  QFileSize    _endPos;

  float        _time;
  AARDataType  _type;
  bool         _requested;
  bool         _requestCompleted;

  DynamicLoadMessage();
  DynamicLoadMessage( AARMessageBase *msg, QFileSize start, QFileSize end );

  virtual void RequestLoad();
  virtual bool IsRequestDone();

  virtual void Unload();
  bool IsRequested();
  virtual void RequestDone(RequestContext *context, RequestResult result);
};

class EntityKeyFrame
{
public:
  EntityId _entity;
  RefArray<AAREntityUpdateData> _messages;
};
TypeIsMovable(EntityKeyFrame);

class KeyFrames : public AutoArray<EntityKeyFrame>
{
  const float _keyFrameInterval;
  int   _keyframe;
  
  void AddKeyFrameMessage();
  void AddNeededKeyFrames(Ref<AAREntityUpdateData> message);
  void CheckConsistency();
public:
  void Init();
  KeyFrames();
  float GetKeyTime() const;
  void AddMessage(EntityId id, Ref<AAREntityUpdateData> message);
};


//! AAR class responsible for recording/replaying
class AAR
{
public:
  friend class NetworkClient;
  friend class AARUnitUpdateData;
  friend class AARUpdateGroupData;
  friend class AARGroupCreateData;
  friend class AARUnitCreateData;
  friend class AARFireWeaponData;
  friend class AAREntityDestroyed;

  // classes needing to know of previous existing messages
  friend class AARCreateMarker;
  friend class AARDeleteMarker;
  friend class AARAttachToUpdate;
  friend class AARWeatherUpdate;
  friend class AARAnimationPhase;

  friend class DynamicLoadMessage;
  friend class LifeSpan;
   
  friend class ObjectLifeSpan;
  friend class UnitLifeSpan;
  friend class GroupLifeSpan;
private:

  //! File handeling.
  SerializeBinStream *_stream;
	QIFStream *_fin;
	QOFStream *_fout;

  // file actual size.
  QFileSize _fileSize;
  int       _msgStreamInPS;
  int       _msgStreamOutPS;
  int       _msgInCount;
  int       _msgOutCount;
  DWORD     _lastKeyframe;

  // Absolute path to where the file was loaded
  RString   _absolutePathLoadedFile;
  //! Aboslute path where the temporary file was
  //! saved during the recording.
  RString   _tmpFileAbsolutePath;
  // was it loaded successfully?
  bool      _fileLoaded;
  bool      _dataChanged;

  float _currentTimePos;
  float _moveToTime;
  float _hitLineHistory; // deprecated
  float _timeOffset;
  float _duration; //Duration of the whole recorded session, use RefreshDuration() to update
  DWORD _previousTime;
  
	//! All the messages related to recording and playback
  //! Lifespan of all objects during mission
	RefArray<DynamicLoadMessage> _messages;
  KeyFrames                    _keyFrames;

  //! List of messages to append to the file
  //! after successfull loading. We can only
  //! read or write but not at the same time.
  RefArray<AARMessageBase>     _appendMessages;

  AutoArray<int>               _loadedMessageIndex;

  //! Backwards compatable with the old version. 
  //! This is now replaced with a new technique.
	RefArray<ObjectLifeSpan>     _entityLifeSpan;
  RefArray<CenterLifeSpan>     _centerLifeSpan; 
  RefArray<GroupLifeSpan>      _groupLifeSpan; 
  RefArray<UnitLifeSpan>       _unitLifeSpan; 


  AutoArray<Detonation>        _detonationBuffer;
  RefArray<BookMarkLifeSpan>   _bookMarks;

  // Used by realtime editor to draw fireevents
  AutoArray<float>    _fireEventsTimes[TSideUnknown];
  AutoArray<float>    _deathTimes[TSideUnknown];
  AutoArray<LogEvent> _cnrEvents;

  bool _inSimulation;
  bool _play;   
  bool _repeat; 
  bool _pause;  
  
  bool _record; 
  bool _remoteRecord; // Used when AAR is remote, not local to server
  int  _saveState;

  // max time lower and higher
  float _lookAheadTime;

  bool _centersSpawned; //helper variable to prevent centers to be spawned multiple times

  BulletIcon  _bulletIcon[TSideUnknown];
  HitLine     _hitLine[TSideUnknown];

  //! Version control system
  int   _version; 

  //! CNR log session file for AAR recording
  AARMessageCNRSession _CNRSession;

  //! current replay index of messages
  int _replayIndex;

  //! HLA interface, enable callback to hla.
  bool _enableCallbacks;
  
  // Mode is in Replay, used to handle UI properly
  bool _replayMode;  

  // briefing data that has been reloaded..
  RString _briefingData;

  // Functiion pointer callbacks
  EntityId (*_ptr2CreateEntity)(EntityCreateData data);
  int  (*_ptr2UpdateEntity)(EntityUpdateData data);
  void (*_ptr2TurretUpdate)(TurretUpdate data);
  void (*_ptr2DeleteEntity)(EntityDeleteData data);
  void (*_ptr2DeleteGroup)(GroupDeleteData data);
  void (*_ptr2FireWeapon)(FireWeaponData data);
  void (*_ptr2DetonateMunition)(DetonationData data);
  EntityId (*_ptr2CenterCreate)(CenterCreateData data);
  EntityId (*_ptr2GroupCreate)(GroupCreateData data);
  EntityId (*_ptr2UnitCreate)(UnitCreateData data);
  void (*_ptr2DoDamage)(DoDamageData data);
  void (*_ptr2VehicleDestroyed)(EntityDestroyed data);
  void (*_ptr2UpdateUnit)(UnitUpdateData data);
  void (*_ptr2UpdateGroup)(UpdateGroupData data);
  void (*_ptr2AttachTo)(AttachToData data);

  /*!
   Clears all the message ques, and life spans.
  */
  void ClearAllQues();
  /*!
   Chechs to see if its safe to clear the message que because
   of requested messages not being loaded
  */
  bool SafeToUnloadMessages();
  /*!
   Corrects the path specified to be absolute if realtive
   @pre valid string length > 0
   @return path that is absolute
  */
  RString CorrectPath(RString &relorabs); 
  /*!
   Creates time/date timestamp for AAR recordings.
  */
  RString GetSystemTime();
  /*! 
   Opens the necceary input stream and assign it to SerializeBin
   @param path absolute path to file
  */
  LSError OpenInputFileStream(RString &path);
  /*!
   Closes the input stream.
   @pre opened inputstream, and SerializeBin
  */
  void CloseInputFileStream();
  /*!
   Opens the output stream
   @param path path to file location
   @returns LSError LSOK if successfull or !LSOK if failed to load
  */
  LSError OpenOutputFileStream(RString &path);
  /*!
   Closes the file output stream
   @pre Stream and file output handel
   @post Close the files and assign null to necceary pointers
  */
  void CloseOutputFileStream();

  //! Clears all explosions or craters on the map, not concerned with currentTimepos
  void ClearExplosions();
  //! Clears all the craters on the map if they're bellow our currentTimePos
  void TimeShiftClearCraters();

  /*!
   Proccess the messages within the serialized bin stream
   @pre valid open bin stream
   @return true if successfull or false if invalid data incountered
   */
  AARLoadError ProccessMessage(SerializeBinStream &in);

  void SerializeMessage(SerializeBinStream &in,Ref<AARMessageBase> &msg);
  
  /*!
   Flushes buffered messages to hard disk.
   @pre Valid Stream handel, to output file
   @post Saves all messages to avaliable stream
   @todo Impliment thread for saving data to hard disk
  */
  SerializeBinStream::ErrorCode FlushMessages();

  //! Used to spawn entity,groups,centers in one function
  void RespawnLifeTimes(bool create);
  //! Respawns/Deletes entitys that have come into scope or left the scope of their lifespan
  void ReSpawnEntitys(bool create);
  void ReSpawnEntity(bool create,ObjectLifeSpan *lifespan );
  //! Respawns groups that have come into scope or left the scope of their lifespan
  void ReSpawnGroups(bool create);
  void ReSpawnGroup(bool create,GroupLifeSpan *lifespan );
  //! Respawns units that have come into scope or left the scope of their lifespan
  void ReSpawnUnits(bool create);
  void ReSpawnUnit(bool create,UnitLifeSpan *lifespan );
  //! Respawns all centers from the current AAR mission
  void SpawnAllCenters();

  //! Performs the neccearly callbacks depending on messages to the hla interface
  void RunMessages();
  //! Run an individual message
  void RunEachMessage
  (
    AARMessageBase* msg, 
    bool enableFireMsg = true,
    bool timeLineForward = true,
    bool moveToTime = false
  );

  int InsertMessage(DynamicLoadMessage *msg);

  //! Set all objects velocitys to zero!
  void SetAllVelocity();

  //! Remove any old detonation data
  void RemoveOldDetonationBuffer();

  //! Init all the internal variables
  void Init();

  void AddFireEventTime(TargetSide side,float time);
  void AddDeathTime(TargetSide side,float time);
  
  /*!
  Clears the _messages and lifespan.
  @post Unload all the messages and lifespan
  */
  void ErrorLoadingFile();
  
  /*!
  Saves all loaded messages, and lifespan to specified path
  */
  LSError SaveLoadedFile();
  
  //! Sets the briefing data
  void RecordBriefingData();
  void ProccessCNRLogEvent();
  void AddMessageToArray(AARMessageBase *msg);
  void AddMessageToAppendArray(AARMessageBase *msg);

  // Truncate the file that has the error
  void Truncate(RString &absPath, int toSize );

  //! Record CNR file, from record
  void StartCNRRecorder();
  void SaveWorldInfo();

  //! Request loading of dynamic messages
  void RequestMessageLoad(float timePos);

public:
	AAR();
	~AAR();

  EntityAI *GetEntityAI(EntityId id);

  //! AAR version number, used for messages processing
  int GetVersion(){ return _version; };

  void InitFields();
  bool IsReplayMode();
  void SetReplayMode(bool mode = true);

  /*! Used for map drawing */
  void DrawExt(float alpha,CStaticMap &map);

  //! Debugging commands
  void NextMessage();
  void PreviousMessage();
  int  MessageCount();
  int  CurrentMessageIndex();
  
  //  -2 is start recording
  //  -1 is saving
  //   0 is success
  // > 0 is failure.
  void SetSaveState(int state);
  void SetRemoteSaveState(int state);

  const AutoArray<float> &GetFireEventTimes(TargetSide side);
  const AutoArray<float> &GetDeathTimes(TargetSide side);

  //! Query AAR book marks during replay and also recording.
  void RemoteAddBookMark(float jumpTo,const RString &name,const RString &message);
  void RemoteRemoveBookMark(RString &name);

  int AddBookMark(float jumpTo,const RString &name,const RString &message);
  void RemoveBookMark(int pos);
  void RemoveBookMark(RString &name);

  //! Get the bookmarks in AAR
  BookMarkLifeSpan &GetBookMark(int i);
  int GetBookMarkSize();

  //! Get the cached CNR log events
  LogEvent &GetLogEvent(int i);
  int GetLogEventSize();

  /*!
   Loads the current file into memory. PreProccesses the messages  within the file.
   @param   absPath absolute path to the file to open 
   @pre     no open stream handles
   @post    closes input file, on success or failure to load
   @returns LSError
  */
  LSError LoadFile(RString &absPath); 
  /*!
   Checks to see if there is any file input stream 
   @returns true, file input stream is open, false if its closed
  */
  bool IsLoaded(){ return _fileLoaded; };
  /*
    Unloads the file, and clears all the message ques
  */
  void UnLoadFile();
  //! Local request to play file
  void Play();       
  /*!
   Stops playing.
   @post Delets all user made entity, but doest flush memory
  */
  void PlayStop();
  //! Pause replaying 
  void PlayPause();   
  //! Set playback to repeat itself when it reaches the end
  void SetRepeat(bool repeat);

  /*!
   Sets the drawing history, resolution in seconds
   @pre amount is greater or equal to zero
  */
  void SetPathHistory(float amount);
  /*!
   Sets the drawing of hit lines between units
   @pre amount is greater or equal to zero
  */
  void SetHitLineHistory(float amount);

  /*
   Query internal variables of AAR
  */
  bool IsPlayPaused();
  // When paused it is not playing
  bool IsPlaying();
  bool IsPlayRepeat();
  bool IsRecording();
  bool IsRemoteRecording();

  //! Returns the current time, within recorded mission
  float CurrentTime(); 

  // Return the recorded briefing description.
  const RString &GetBriefingData();

  /*!
   Move to the specified time.
   @pre pos >= 0.0f, pos <= Duration()
   @post Moves to specified time
  */
  void MoveToTime(float pos);

  //! Start recording and collecting hla messages
  void RemoteRecord();

  /*!
   Creates temporary file for recording. If file creation proccess
   failes then will report it to the user, and fail to record.
   */
  void Record();   

  /*! 
   Stop the recording, and set all the lifespan objects
   death time to GetRecordTimeOffset 
  */
  void RemoteRecordStop();
  void RecordStop();  

  /*!
   Saves all recorded messages from hla to the path specified.
   @pre path can be relative or absolute.
   @return Use GetSaveState for success or failure
  */
  void RecordSave(RString &path);
  void RemoteRecordSave(RString &path);

  int  GetSaveState();

  //! Returns the duration of the recorded mission in playback mode
  float GetDuration();

  void Draw();

  // Get the recorder time offset, since starting recording relative to mission start
  float GetRecordTimeOffset() const { return Glob.time.toFloat() - _timeOffset; };

  /*!
    Search lifeSpan for entity Id. If no match is found, index is not modified.
    If match found index is updated with the array index position.
    @pre EntityId
    @post if found index is updated with its position
    @returns true if found, false otherwise
  */
  bool CheckEntityId(EntityId id, int &index);
  bool CheckEntityId(EntityId id);

  /*!
    Gets the Entitys LifeSpan at current index.
    @pre index >= 0 and < ArraySize, valid index
    @post returns ObjectLifeSpan
  */
  ObjectLifeSpan &GetLifeSpan(int index);

  //! Returns LifeSpan Size.
  int GetLifeSpanSize();

  /**
  * Initialise LVC Game API.
  * This will bring everything up, join the RTI (if successful)
  * and be ready to receive / send information on HLA.
  *
  * @return true if initialisation was successful or false otherwise
  */
  bool init();
  /**
  * Initialise LVC Game API.
  * This will bring everything up, join the RTI (if successful)
  * and be ready to receive / send information on HLA.
  *
  * @param path the path (relative or absolute) to the LVC Game API 
  * configuration files
  *
  * @return true if initialisation was successful or false otherwise
  */
  bool init(const char* path);
  /**
  * Indicates whether LVC Game API has been initialised correctly.
  * If the LVC Game API has initialised, it will be connected to the
  * RTI and ready to go.
  *
  * @return true if initialised or false otherwise
  */
  bool isInitialised();
  /**
  * Tick LVC Game.
  *
  * This will give control to LVC Game allowing one event to 
  * be processed.
  *
  * @return 1 if error was encountered otherwise 0;
  */
  int tick(); // tick comes all the time...
  /**
  * Shutdown the LVC Game
  *
  */
  void shutdown();// shutdown is called when the system goes out of muliplayer mode
  /**
  * Determines if the LVC Game lifecycle has stopped 
  */
  bool isStopped(); // dont need that

  /**
  * An entity was created within VBS2
  *
  * @param data the available data on the entity.
  */
  void entityCreated(EntityCreateData data);
  
  /**
  * An entity was updated within VBS2
  * Function ignores the following:
  *  static objects
  *  allready deleted objects
  *
  *  @pre  data.Id  - cannot be null/is a EntityId
  *  @pre  data.Veh - can be null/is a EntityId
  *  @post data is timestamped, and saved
  */
  void entityUpdated(EntityUpdateData data);
  /**
  * An entity was removed from VBS2
  * @param id the VBS ID of the entity.
  */
  void entityDeleted(EntityDeleteData id);
  /**
  * The state of the simulation changed within VBS2
  * @param data the available data on the simulation state.
  */
  void simulationStateChanged(SimStateData data);

  /*!
  @pre
  Data.FireingID
  1. Cannot be null
  2. Is valid entity
  3. ObjectId has to be non-static

  Data TargetId
  1. Can be NULL
  2. If not null
    1. Is valid entity
    2. If ObjectId is nonstatic, defaults to NULL
  */
  void weaponFired(FireWeaponData data);

  /*!
  @pre
  Data.FireingID
  1. Can be NULL
  2. If not null
    1. Is valid entity
    2. If ObjectId is nonstatic, defaults to NULL

  Data TargetId
  1. Can be NULL
  2. If not null
    1. Is valid entity
    2. If ObjectId is nonstatic, defaults to NULL
  */
  void munitionDetonation(DetonationData data);

  void CenterCreated(CenterCreateData data);
  void GroupCreated(GroupCreateData data);
  void UnitCreated(UnitCreateData data);
  void DoDamage(DoDamageData data);
  void VehicleDestroyed(EntityDestroyed data);
  void UpdateUnit(UnitUpdateData data);
  void UpdateGroup(UpdateGroupData data);
  void AttachToUpdate(AttachToData data);
  void UpdateTurret(TurretUpdate data);

  // Split from HLA
  // These methods no longer loopback into hla
  // AAR takes it own necceary action
  void CreateMarker(ArcadeMarkerInfo *marker);
  void DeleteMarker(const RString &name); 

  void WeatherUpdate(ApplyWeatherMessage *weather);
  void RecordAnimationPhase(const EntityAnimationPhase &data);

  /**
  * Callback registration for entity and weapon events
  */
  void setCreateEntity(EntityId (*fPtr)(EntityCreateData data));
  void setTurretUpdate(void (*fPtr)(TurretUpdate data));
  void setUpdateEntity(int (*fPtr)(EntityUpdateData data));
  void setDeleteEntity(void (*fPtr)(EntityDeleteData data));
  void setDeleteGroup(void (*fPtr)(GroupDeleteData data));
  void setFireWeapon(void (*fPtr)(FireWeaponData data));
  void setDetonateMunition(void (*fPtr)(DetonationData data));

  void setCreateCenter(EntityId (*fPtr)(CenterCreateData data));
  void setCreateGroup(EntityId (*fPtr)(GroupCreateData data));
  void setCreateUnit(EntityId (*fPtr)(UnitCreateData data));
  void setDoDamage(void (*fPtr)(DoDamageData data));
  void setVehicleDestroyed(void (*fPtr)(EntityDestroyed data));
  void setUpdateUnit(void (*fPtr)(UnitUpdateData data));
  void setUpdateGroup(void (*fPtr)(UpdateGroupData data));
  void setAttachToUpdate(void (*fPtr)(AttachToData data));
  void setCreateMarker(void (*fPtr)(MarkerCreateData data));


  /**
  * Enables all callback methods
  *
  * Once enabled callbacks will be invoked as appropriate
  * Ensure that initialisation of the API is complete and
  * callbacks have been registered.
  */
  void enableCallbacks(); // actualy do the call backs.
  /**
  * Disables all callback methods
  */
  void disableCallbacks(); // disable all callbacks.
};

extern AAR GAAR;

#endif
#endif
