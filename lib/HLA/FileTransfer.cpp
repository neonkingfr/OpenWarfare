#include "../wpch.hpp"

#include "FileTransfer.hpp"

#include <Es\Files\fileContainer.hpp>
#include "AAR.hpp"
#include "..\Network\networkImpl.hpp"


ThreadBase::ThreadBase():_threadId(0),_threadHandle(0){};

// if we're still alive, kill the thread
ThreadBase::~ThreadBase()
{
  Terminate();
}

// Implement your own start
void ThreadBase::Start()
{
  _threadHandle = ::CreateThread(NULL,64*1024,RunThread,this,THREAD_PRIORITY_NORMAL,&_threadId);		
}

void ThreadBase::Terminate()
{
  if(IsAlive())
    CloseHandle(_threadHandle);
}

// returns if the thread is still active
bool ThreadBase::IsAlive()
{
  DWORD exitCode;
  ::GetExitCodeThread(_threadHandle,&exitCode);
  if(exitCode== STILL_ACTIVE)
    return true;

  return false;
}

ServiceClient::ServiceClient(SOCKET socket):_socket(socket)
{
  // we want the sending socket to block.
  // but not the listening socket
  unsigned long argp = 0;
  ioctlsocket(_socket, FIONBIO, &argp);

  // wait till all data is sent!
  //linger lin;
  //lin.l_onoff  = 1;
  //lin.l_linger = 30;
  //setsockopt(_socket, SOL_SOCKET, SO_LINGER, (char*)&lin, sizeof(lin) );
};

void ServiceClient::Run()
{
  char buff[1024*4];
  char path[MAXFILENAME] = {0};

  int n = recv(_socket,buff,sizeof(buff),0);

  if(n == sizeof(FileSegmentRequest))
  {
    FileSegmentRequest *msg = (FileSegmentRequest*)buff;

    if(msg->magic == MAGIC_FILE_REQUEST)
    {
      const char *userDir  = GMachineSessions.GetUserDir();
      const char *aarPath  = "AAR\\";
      const char *fileName = msg->fileName;

      if(strlen(userDir)+strlen(aarPath)+strlen(fileName) < MAX_PATH)
      {
        strncpy(path,userDir,strlen(userDir));
        strncpy(&path[strlen(path)],aarPath,strlen(aarPath));
        strncpy(&path[strlen(path)],fileName,strlen(fileName)+1);

        //LogF("Sending the current file:%s", path );
        // first check if file even exsists
        WIN32_FIND_DATA FindFileData;
        HANDLE hFind;

        // check if file exsists
        hFind = FindFirstFile(path, &FindFileData);
        if (hFind != INVALID_HANDLE_VALUE) 
        {
          FindClose(hFind);

          // file exsists, now let go and get it
          int offset   =  0;
          //int segment  =  0;
          while(true)
          {		
            FILE *file = NULL;

            // wait till we can open it!
            // another thread may have it open!
            while(!file)
            {
              //LogF("Opening file!");
              file = fopen(path, "rb");
              if(!file)
              {
                LogF("Failed to open file, sleeping");
                Sleep(10);
              }
            }
            fseek(file,(long) offset * FILEBUFFER,SEEK_SET);

            FileSegmentResponse response;
            response.magic = MAGIC_FILE_RESPONSE;                

            response.size = fread(response.buffer,1,FILEBUFFER,file);
            fclose(file);// close the file as soon as possible

            //LogF("segment num:%d", offset );
            //LogF("read:%d", response.size );
            //LogF("Total size sent:%d",(FILEBUFFER * offset)+response.size );
            // calculate the crc on the data read.
            response.fileSegment = offset;              
            response.crcCHECK = 0;

            // send the data out
            // this can block, but its okay we're in a thread
            //LogF("Sending data out!");
            if(send(_socket,(char*) &response,sizeof(FileSegmentResponse),0)== SOCKET_ERROR)
            {
              //LogF("Error occured well sending:%d",WSAGetLastError());
              break;
            }
            
            ++offset;

            if(response.size < FILEBUFFER)
            {
              //LogF("Successfully sent file to client!");
              break;// successfully sent all of the file!
            }
          }	
        }
      }
    }
  } 

  LogF("service closed");
  closesocket(_socket);
}

bool ConnectToServer::InitWSA()
{
  // Connect to the dll
  int wsaret = WSAStartup(0x101,&_wsaData);
  if(wsaret != 0 )
    return false;

  return true;
}

ConnectToServer::ConnectToServer(struct sockaddr_in server, int port, const char *fileName, int filesize)
{
  _server = server;
  _port = port;
  _fileSize = filesize;

  // copy the filename
  // that wants to be downloaded
  strcpy(_fileName,fileName);

  InitWSA();   

  _downloadFinished = false;
  _downloadedBytes  = 0;
  _error = 0;
  _abort = false;
}

// Connect to the server return response.
// uses blocking socket!
bool ConnectToServer::Connect()
{
  _socket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
  if(_socket==INVALID_SOCKET)
    return false;

  _server.sin_family = AF_INET;
  _server.sin_port   = htons(_port);

  // connect to the server, is everthing fine? this should time out
  if(connect(_socket,(struct sockaddr*)&_server,sizeof(_server)))
  {
    _error = WSAGetLastError();
    closesocket(_socket);
    return false;	
  }

  return true;
}

void ConnectToServer::Run()
{
  // okay send out the request for the specific file name.
  // wait for the response to come back.
  char  path[MAX_PATH] = {0};
  char  buff[1024*4]   = {0};
  FILE *file           = NULL;

  const char *aarPath = "AAR\\";
  const char* userDir = GMachineSessions.GetUserDir();

  // Need better error checking here!
  if(strlen(userDir)+strlen(aarPath)+strlen(_fileName)+1 < MAX_PATH)
  {
    strncpy(path,userDir,strlen(userDir));
    strncpy(&path[strlen(path)],aarPath,strlen(aarPath));

    // Create the directory if it doesn't exsist
    CreateDirectory(path,NULL);
    strncpy(&path[strlen(path)],_fileName,strlen(_fileName)+1);

    // open the file, clear the current one.
    // can fail opening the file!
    file = fopen(path, "wb" );
    if(file)
    {
      FileSegmentRequest request;
      strcpy(request.fileName,_fileName);
      request.fileSegment = 0;
      request.magic = MAGIC_FILE_REQUEST;

      // send file segment request out, we hope it gets there
      if(send(_socket,(char*) &request,sizeof(request),0) != SOCKET_ERROR)
        _error = WSAGetLastError();

      while(!_abort && (_error != SOCKET_ERROR))
      {
        DWORD readIn = 0;
        // keep reading until you fill up the allocated buffer
        do 
        {
          readIn += recv(_socket,buff + readIn,sizeof(FileSegmentResponse) - readIn,0);
          if((readIn == SOCKET_ERROR) || _abort )
            break;
        } while (readIn != sizeof(FileSegmentResponse));
        

        if((readIn == SOCKET_ERROR) || _abort)
        {
          _error = WSAGetLastError();
          _downloadFinished = false;
          break;
        }

        if(readIn == sizeof(FileSegmentResponse))
        {
          FileSegmentResponse *fileSegment = (FileSegmentResponse*)buff;

          if(fileSegment->magic == MAGIC_FILE_RESPONSE )
          {       
            DWORD offset = fileSegment->fileSegment*FILEBUFFER;

            fwrite(fileSegment->buffer,1,fileSegment->size,file);          

            _downloadedBytes = offset + fileSegment->size;

            //LogF("Writing out segment:%d",fileSegment->fileSegment);
            //LogF("Size of segment is:%d", fileSegment->size );
            //LogF("Total writen:%d", _downloadedBytes );

            if((int)(offset + fileSegment->size) >= _fileSize)
            {
              _downloadFinished = true;
              break;
            }
          }
        }
      }

      fclose(file);
    }
    else
    {
      //prompt error code, cannot open file! 
      _downloadFinished = false;
      _error = -1; // file error
    }     
  }
  else
  {
    _downloadFinished = false;
    _error = -1;// file error
  }

  closesocket(_socket);
}

bool ServerListen::InitWSA()
{
  // Connect to the dll
  int wsaret = WSAStartup(0x101,&_wsaData);
  if(wsaret != 0 )
    return false;

  return true;
}

ServerListen::ServerListen():_port(0)
{
  InitWSA();
  _listenSocket  = 0;
  _failedToStart = false;
}

// disconnect all active users
ServerListen::~ServerListen()
{
  for( int i = 0; i < _clients.Size(); ++i)
    if(_clients[i])
      _clients[i]->Terminate();


  _clients.Clear();
}


bool ServerListen::CreateListenSocket(int port)
{
  _port = port;

  _local.sin_family     =AF_INET;
  _local.sin_addr.s_addr=INADDR_ANY;
  _local.sin_port       =htons((u_short)_port);
  _listenSocket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);

  if(_listenSocket==INVALID_SOCKET)
  {
    _listenSocket = NULL;
    return false;
  }

  // Make listen port non blocking
  unsigned long argp = 1;
  ioctlsocket(_listenSocket, FIONBIO, &argp);


  return true;
}

bool ServerListen::BindAndListen()
{
  if(bind(_listenSocket,(sockaddr*)&_local,sizeof(_local))==SOCKET_ERROR)
    return false;

  if(listen(_listenSocket,SOMAXCONN)==SOCKET_ERROR)
    return false;

  return true;
}

void ServerListen::ListenForConnections()
{
  SOCKET      client;
  sockaddr_in from;
  int fromlen=sizeof(from);

  client = accept(_listenSocket,(struct sockaddr*)&from,&fromlen);
  if(client != INVALID_SOCKET)
  {
    int index = _clients.Add(new ServiceClient(client));
    _clients[index]->Start();
  } 
  else
  {
    int error = WSAGetLastError();
    if(error != WSAEWOULDBLOCK)
      LogF("Experienced error code in socket:%d",error);
  }

  // clean up of some theads
  for( int i = 0; i < _clients.Size(); ++i)
    if(_clients[i] && !_clients[i]->IsAlive())
    {
      _clients[i] = NULL;
      --i;
    }
}
//////////////////////////////////////////////////////////////////////////











Session::Session()
{
  _myFilesDetails.Resize(60);
  Init();
}

void Session::Init()
{
  _used = false;
  _addExtraFile = false;
  ZeroMemory(&_distant,sizeof(_distant));
  ZeroMemory(&_name,sizeof(_name));

  for( int i = 0; i < _myFilesDetails.Size(); ++i )
    _myFilesDetails[i].Init();
}

void Session::SetName(const char *name)
{
  strncpy(_name,name,sizeof(_name));
}

void Session::UpdateTick(DWORD time)
{
  _lastUpdate = time;
}

const char* Session::GetName(){ return _name; }

AutoArray<AARFileInfo> &Session::GetFileList(){ return _myFilesDetails; }

void Session::AddExtraFile(){ _addExtraFile = true; }

void Session::SetDist(struct sockaddr_in &dist ){ _distant = dist; }

struct sockaddr_in &Session::GetDist(){ return _distant; }

void Session::SetUsed(bool used){ _used = used; }

bool Session::GetUsed(){ return _used; }

void Session::Tick()
{
  if(!_used)
    return;

  if(_addExtraFile)
  {
    _addExtraFile = false;
    _myFilesDetails.Add();
  }

  // no update in 10 seconds, disconnected
  if(::GlobalTickCount()-_lastUpdate > 10000)
    Init();
}

RString &MachineSessions::GetFileName(){ return _downLoadFile; }
DWORD MachineSessions::GetFileSize(){ return _fileSize;}


float MachineSessions::GetCompleted()
{
  int fileSizeBytes   = GetFileSize();
  int downloadedBytes = _downloadedBytes;

  if(DownloadFinished())
    return 1.0;

  if(DownloadCanceled())
    return 0.0f;

  return (float)downloadedBytes/(float)fileSizeBytes;
}

void MachineSessions::CancelDownload()
{
  if(_downloadFrom)
  {
    _downloadFrom->Abort();// abort downloading

    RString path = GetDefaultUserRootDir() + RString("\\AAR\\") + _downLoadFile;
    QIFileFunctions::CleanUpFile(path);

    _downloadFinished = false;
    _downloadCanceled = true;
    _downloadStarted  = false;
  }
}

void MachineSessions::DownLoadFile(RString name,DWORD size, struct sockaddr_in server )
{
  // no other choice but to terminate it, and continue with life.
  if(_downloadFrom)
  {
    _downloadFrom->Terminate();
    _downloadFrom = NULL;
  }

  // We're allready downloading from somebody
  if(!_downloadFrom)
  {
    _downloadFrom = new ConnectToServer(server,SERVERTCPLISTEN,name.Data(),size);

    _server       = server;
    _fileSize     = size;
    _downLoadFile = name;

    _downloadFinished = false;
    _downloadedBytes  = 0;
    _downloadCanceled = false;
    _downloadStarted  = false;

    if(_downloadFrom->Connect())
    {
      _downloadStarted = true;
      _downloadFrom->Start();
    }
    else
    {
      WarningMessage("Failed to connect to server.");
      ResetErrors();
      
      // terminate thread
      _downloadFrom->Terminate();
      _downloadFrom = NULL;
    }
  }
}

// forward declaration
RString GetUserDirectory();

MachineSessions::MachineSessions():
_addSession(false),
_fileSize(1024)
{
  _downloadFinished = false;
  _downloadStarted  = false;
  _downloadCanceled = false;
  _downloadedBytes  = 0;

  ZeroMemory(_userDir,sizeof(_userDir));
  _remoteMachines.Resize(5);
}

Session &MachineSessions::Get(int index)
{
  Assert(index >= 0 && index < _remoteMachines.Size() );
  return _remoteMachines[index];
}

// index allways points to the one that
// is avaliable.
int  MachineSessions::GetSize() { return _remoteMachines.Size(); }
void MachineSessions::AddSession(){ _addSession = true; }

void MachineSessions::SetUserDirectoryForThread()
{
  RString userDir = GetUserDirectory();
  // on the first init, we need to set the user dir
  if(_userDir[0] == 0)
    strncpy(_userDir,userDir.Data(),userDir.GetLength());
}

// foward declaration
bool IsDedicatedServer();

// next tick pass, cannot do anything until next simulation
void MachineSessions::Tick()
{
  Enter();

  SetUserDirectoryForThread();

#if _EXT_CTRL
  if(!GAAR.IsReplayMode() &&
#else
  if(
#endif
    (GNetworkManager.IsServer() && IsDedicatedServer()))
  {
    // Create socket if one is not connected
    if(!_listenServer.SocketCreated())
    {
      if(_listenServer.CreateListenSocket(SERVERTCPLISTEN))
      {
        if(_listenServer.BindAndListen())
          LogF("Listening to port:%d",SERVERTCPLISTEN);
        else
          LogF("Failed to bind port:%d",SERVERTCPLISTEN);
      } 
      else
        LogF("Failed to Create listen port:%d",SERVERTCPLISTEN);
    }
    _listenServer.ListenForConnections();
  }
  
  if(_downloadFrom)
  {
    _downloadedBytes  = _downloadFrom->ByesDownloaded();
    _downloadFinished = _downloadFrom->IsDownloadFinished();

     if(_downloadFrom->GetError()!=0)
    {
      WarningMessage("Connection to server lost");
      ResetErrors();

      _downloadFrom = NULL;
      _downloadFinished = false;
      _downloadCanceled = true;
      _downloadStarted  = false;

    }
     // clean up if we've finished downloading
     if(_downloadFinished)
     {
       if(_downloadFrom && !_downloadFrom->IsAlive())
         _downloadFrom = NULL;// terminate it, cleanup
     }
  }

  /*
  if(_downLoadStarted && !_downLoadFinished )
  {
    if(_totalRequestTime > 10000)
    {
      WarningMessage("No response from server in 10 seconds,please try again");
      ResetErrors();
      CancelDownload();
    }
    else
    {
      if(::GlobalTickCount()-_requestTime > 300)
        RequestSegmentDownload(_segment);
    }
  }
  */

  if(_addSession)
  {
    _remoteMachines.Add();
    _addSession = false;
  }
  for( int i = 0; i < _remoteMachines.Size(); i++ )
    _remoteMachines[i].Tick(); // allow internal buffers to increase


  Leave();
}

MachineSessions GMachineSessions;