#include "../wpch.hpp"
#include "AARHLAExtraTypes.hpp"

#if _AAR

#if AARDebug
  #define DLogF LogF
#else
  #define DLogF
#endif

#if _VBS2_AAR_ASSERT
  #ifdef Assert
    #undef Assert
  #endif
  #ifdef Verify
    #undef Verify
  #endif

  #define Assert(CONDITION) if(!(CONDITION)) { _asm { int 3 } }
  #define Verify(CONDITION) if(!(CONDITION)) { _asm { int 3 } }
#endif

#endif
