#include "../wpch.hpp"

#if _VBS3_SHAPEFILES

#include "ShapeLayers.hpp"

#include "../engine.hpp"
#include "../landscape.hpp"
#include "El/Math/math3d.hpp"
//#include "El/Shapes/ShapePolygon.h"
#include "El/Shapes/ShapePolyLine.h"
#include "VBSVisuals.hpp"

using namespace ShapeFiles;

ShapeLayer::ShapeLayer()
{
  _origin[0] = _origin[1] = 0;
  hidden = false;
  _shapeOrg = NULL;
  _color = PackedColor(255,0,0,150);
}

ShapeLayer::~ShapeLayer()
{
  _objects.Clear();
  _drawShapes.Clear();
}

void ShapeLayer::SetOrigin(double x, double z)
{
  _origin[0] = x;
  _origin[1] = z;
}

extern RString GetMissionDirectory();
extern RString GetUserDirectory();

bool ShapeLayer::Load(RString name, bool missionDir)
{
  if(_shapeOrg)
    delete _shapeOrg;

  _shapeOrg = new ShapeOrganizer;
  
  RString fullName;
  if(missionDir)
    fullName = GetMissionDirectory() + name;
  else
    fullName = GetUserDirectory() + name;


  if(! _shapeOrg->ReadShapeFile(Pathname(fullName.Data()),0))
  {
    LogF("[m] loading shapefile: %s failed!", cc_cast(fullName));
    delete _shapeOrg;  _shapeOrg = NULL;
    return false;
  }

  _name = name;
  return true;
}

bool ShapeLayer::Prepare()
{
  if(! _shapeOrg)
    return false;

  ShapeList& sList = _shapeOrg->GetShapeList();
  if(sList.Size()) 
  {
    for(unsigned int i=0, cnt = sList.Size(); i < cnt; ++i)
    {
      IShape* shp = sList.GetShape(i);
      ShapePolygon* shapePolygon = dynamic_cast<ShapePolygon*>(shp);
      if(shapePolygon)
      {
        if(shapePolygon->GetVertexCount() > 2)
        {
          if(shapePolygon->GetAreaSigned() < 0.0)
          {
//            shapePolygon->ReverseVertexList();
            LogF("[m] Shape: %i needs reversing", i);
          }
          
          if(i==0) //just for debug
          {          
            AddDrawShape(shapePolygon);
            LogF("[m] Adding Draw Shape: %d, Vertexes: %d", i, shapePolygon->GetVertexCount());
          }
        }
        else
          LogF("[m] invalid ShapeLayer: VertexCount: %d", shapePolygon->GetVertexCount());
      }
    }
  }
  return true;
};

#include "../Shape/specLods.hpp"
#include "../txtPreload.hpp"
#include "../keyInput.hpp"
#include "../dikCodes.h"

bool ShapeLayer::AddDrawShape(ShapePolygon* shp)
{
  int sVtxCount = shp->GetVertexCount();
  
  int index= _drawShapes.Add(new LODShapeWithShadow());
  LODShapeWithShadow* _drawShape = _drawShapes[index];
  Shape *shape=new Shape; 

  _drawShape->AddShape(shape,0); //create LOD 0
  
  // set flags
  int special=IsAlpha|NoShadow|IsAlphaFog|IsColored;
  _drawShape->SetSpecial(special);

  float heightOffset = 2;

  // initalize lod level
  int nVertexes = 3 * (sVtxCount - 2);
  shape->ReallocTable(nVertexes);

  shape->SetClipAll(ClipAll | (MSShiningAdjustable * ClipUserStep));
  for(int j=0; j < sVtxCount; ++j)
  {
    shape->SetNorm(j) = VUp;
  }

  // change faces parameters
  Poly face;
  face.Init();
  face.SetN(3);

  for(int i = 0; i < sVtxCount - 2; i++)
  {    
    face.Set(0,3*i);
    face.Set(1,3*i+1);
    face.Set(2,3*i+2);
    shape->AddFace(face);
  }

  // precalculate hints for possible optimizations
  shape->CalculateHints();
  _drawShape->CalculateHints();

  //  lShape->OrSpecial(IsColored|OnSurface|IsOnSurface|IsAlpha|IsAlphaFog|BestMipmap);
  special = IsColored|IsAlpha|IsAlphaFog|BestMipmap;
  _drawShape->OrSpecial(special);

  index = _objects.Add(new ObjectColored(_drawShape,VISITOR_NO_ID));
  ObjectColored* _obj = _objects[index];
  _obj->SetOrientation(M3Identity);

  // use global object
//  LODShape *lodShape = obj->GetShape();
//  Shape *shape2 = lodShape->GetLevelLocked(0);

  AutoArray<Vector3> vectorBuffer;
  for(int i=0; i< sVtxCount; ++i)
  {
    float x = shp->GetVertex(0).x;
    float z = shp->GetVertex(0).y;
    float y = GLandscape->SurfaceY(x, z) + heightOffset;
    //    y = (shapePolygon->GetVertex(j).z == 0) ? GLandscape->SurfaceY(x, z) + heightOffset : shp->GetVertex(j).z; //3d data?
    vectorBuffer.Add(Vector3(x,y,z));
  }

//  LODShape *lodShape = obj->GetShape();
//  Shape *shape2 = lodShape->GetLevelLocked(0);

  Vector3 normal = VUp;
  for(int i = 0; i < sVtxCount - 2; i++)
  { 
    shape->SetPos(3*i) = VZero;
    shape->SetPos(3*i+2) = vectorBuffer[i + 2] - vectorBuffer[0];
    shape->SetPos(3*i+1) = vectorBuffer[i + 1] - vectorBuffer[0];

    shape->SetNorm(3*i) = normal;
    shape->SetNorm(3*i+1) = normal;
    shape->SetNorm(3*i+2) = normal;
  }

  //  shape2->SurfaceSplit(GScene->GetLandscape(),obj->Transform(),0.1,1);

//  _obj->SetConstantColor(color);

  _drawShape->SetAutoCenter(false);
  _drawShape->CalculateMinMax(true);

  ShapeSectionInfo prop;
  prop.Init();
  prop.SetTexture(GScene->Preloaded(TextureWhite));

  if(GInput.keys[DIK_LCONTROL] > 0.0f)
    prop.SetSpecial(special|_drawShape->Special()|OnSurface);
  else
    prop.SetSpecial(special|_drawShape->Special());

  shape->SetAsOneSection(prop);
  shape->RecalculateAreas();

  _obj->SetPosition(vectorBuffer[0]);

  return true;
}

bool ShapeLayer::Project()
{
  if(! _shapeOrg)
    return false;

  if(!_origin[0] && !_origin[1])
  {
    LogF("[m] no projection, since origin is Zero");
    return true;
  }

  ShapeList& sList = _shapeOrg->GetShapeList();
  LogF("[m] number of Shapes: %d", sList.Size());
  
  if(sList.Size()) 
  {
    IShape* shp = sList.GetShape(0);
    VertexArray* vtxArray = shp->GetVertexArray(); //vertex array contains all vertexes!


    const ShapePolygon* shapePolygon = dynamic_cast<const ShapePolygon*>(shp);
    if (shapePolygon)
      LogF("[m] Identified as Polygon");
    else
    {
      const ShapePolyLine* shapePolyLine = dynamic_cast<const ShapePolyLine*>(shp);
      if(shapePolyLine)
        LogF("[m] Identified as PolyLine");
      else
      {
        LogF("[m] Unknown shape");
      }
    }

    LogF("[m]    vtxArray->Size: %d", vtxArray->Size());
    for(unsigned int j=0; j < vtxArray->Size(); ++j)
    {
      //todo: map to terrain?
      double x = vtxArray->GetVertex(j).x;
      double y = vtxArray->GetVertex(j).y;
//      LogF("[m] %d: Project: (%f,%f) -> (%f,%f)", j, x,y, (x - _origin[0]), (y - _origin[1]));
      if(!vtxArray->SetVertex(j, DVertex(x - _origin[0], y - _origin[1], vtxArray->GetVertex(j).z)))
      {
        LogF("[m] Fail to modify Shp vertex!");
        return false;
      }
    }
    //TODO: restructure, not a very good place here, rather move all objects
    Prepare();

    return true;
  }
  return false;
}

//===================================================================================
//from Editor.cpp
//===================================================================================

static void DrawPolygon(const AutoArray<Vector3>& polygon, const PackedColor& color)
{ 

  Ref<LODShapeWithShadow> lShape=new LODShapeWithShadow();
  Shape *shape=new Shape;
  lShape->AddShape(shape,0);
  
  // set flags
  int special=IsAlpha|NoShadow|IsAlphaFog|IsColored;

//  if(GInput.keys[DIK_LSHIFT] > 0.0f) //DIK_LSHIFT
//    special|= OnSurface;

  lShape->SetSpecial(special);

  // initalize lod level
  int nVertexes = 3 * (polygon.Size() - 2);
  shape->ReallocTable(nVertexes);
  shape->SetClipAll(ClipAll | (MSShiningAdjustable * ClipUserStep));
  for (int i=0; i< nVertexes; i++)
  {
    //shape->SetPos(i) = VZero;
    shape->SetNorm(i) = VUp;
//    shape->SetSpecial(special);
  }
  /*shape->SetUV(0,0,0);
  shape->SetUV(1,1,0);
  shape->SetUV(2,0,1);
  shape->SetUV(3,1,0);
  shape->SetUV(4,1,1);
  shape->SetUV(5,0,1);*/

  // precalculate hints for possible optimizations
  shape->CalculateHints();
  lShape->CalculateHints();

  // change faces parameters
  Poly face;
  face.Init();
  face.SetN(3);
  for(int i = 0; i < polygon.Size() - 2; i++)
  {    
    face.Set(0,3*i);
    face.Set(1,3*i+1);
    face.Set(2,3*i+2);
    shape->AddFace(face);
  }

//  lShape->OrSpecial(IsColored|OnSurface|IsOnSurface|IsAlpha|IsAlphaFog|BestMipmap);
  special = IsColored|IsAlpha|IsAlphaFog|BestMipmap;
//  if(GInput.keys[DIK_LALT] > 0.0f)
//    special|= OnSurface;

  lShape->OrSpecial(special);

  Ref<ObjectColored> obj = new ObjectColored(lShape,VISITOR_NO_ID);
  obj->SetOrientation(M3Identity);
  obj->SetConstantColor(color);
  //}

  // use global object
  LODShape *lodShape = obj->GetShape();
  Shape *shape2 = lodShape->InitLevelLocked(0);

  Vector3 normal;
  for(int i = 0; i < polygon.Size() - 2; i++)
  { 
    shape2->SetPos(3*i) = VZero;
    shape2->SetPos(3*i+2) = polygon[i + 2] - polygon[0];
    shape2->SetPos(3*i+1) = polygon[i + 1] - polygon[0];

    normal = VUp;

    shape2->SetNorm(3*i) = normal;
    shape2->SetNorm(3*i+1) = normal;
    shape2->SetNorm(3*i+2) = normal;
  }

//  shape2->SurfaceSplit(GScene->GetLandscape(),obj->Transform(),0.1,1);

  obj->SetConstantColor(color);

  lodShape->SetAutoCenter(false);
  lodShape->CalculateMinMax(true);

  ShapeSectionInfo prop;
  prop.Init();
  prop.SetTexture(GScene->Preloaded(TextureWhite));

  if(GInput.keys[DIK_LCONTROL] > 0.0f)
     prop.SetSpecial(special|lShape->Special()|OnSurface);
  else
    prop.SetSpecial(special|lShape->Special());

  shape2->SetAsOneSection(prop);
  shape2->RecalculateAreas();

  obj->SetPosition(polygon[0]);


  //  if(GInput.keys[DIK_RCONTROL] > 0.0f)
//    obj->Draw(0, SectionMaterialLODsArray(), ClipAll | ClipLandOn , DrawParameters::_default, InstanceParameters::_default, 0.0f, *obj, obj);
//  else
    obj->Draw(-1, 0, SectionMaterialLODsArray(), ClipAll, DrawParameters::_default, InstanceParameters::_default, 0.0f, *obj, NULL);
//  obj->DrawLines(0,  ClipAll | (MSShiningAdjustable * ClipUserStep), *obj);
}
//===================================================================================
bool ShapeLayer::Draw()
{
  if(!_shapeOrg)
    return false;

  ShapeList& sList = _shapeOrg->GetShapeList();

  //TODO: Multiparts?
  //TODO: Clipping via BoundingBox?
 AutoArray<Vector3> vectorBuffer;
 
  for(unsigned int i=0; i < sList.Size(); ++i)
  {
    IShape* shp = sList.GetShape(i);
    ASSERT(shp != NULL);

    const ShapePolygon* shapePolygon = dynamic_cast<const ShapePolygon*>(shp);
    if (shapePolygon && shp->GetVertexCount() > 2) 
    {
//      if(_objects[)
      if(false)
      {
        _objects[i]->SetConstantColor(PackedColor(0,1,0,1)); //TODO: directly do in setprop code
        _objects[i]->Draw(-1, 0, SectionMaterialLODsArray(), ClipAll, DrawParameters::_default
                          , InstanceParameters::_default, 0.0f, *_objects[i], NULL);
        _objects[i]->DrawLines(-1, 0,  ClipAll | (MSShiningAdjustable * ClipUserStep), *_objects[i]);
      }

     vectorBuffer.Clear();
      float x = shp->GetVertex(0).x;
      float z = shp->GetVertex(0).y;
      float y = GLandscape->SurfaceY(x, z) + 1;
      Vector3 first(x,y,z);
      vectorBuffer.Add(first);
      for(unsigned int  j=1; j < shp->GetVertexCount(); ++j)
      {
        x = shp->GetVertex(j).x;
        z = shp->GetVertex(j).y;
        y = GLandscape->SurfaceY(x, z) + 1;

//        y = (shp->GetVertex(j).z == 0) ? GLandscape->SurfaceY(x, z) + 3 : shp->GetVertex(j).z;
        vectorBuffer.Add(Vector3(x,y,z));
      }  

      //there should be more efficient ways
      for(int i=1; i< vectorBuffer.Size(); ++i)
        GVBSVisuals.Add3dLine(vectorBuffer[i-1], vectorBuffer[i], _color);
//        GEngine->DrawLine3D(vectorBuffer[i-1], vectorBuffer[i], _color, 0);
    }
    else //not Polygon
    {
      // Point2DFloat
      if(shp->GetVertexCount())
      {
        vectorBuffer.Clear();
        float x = shp->GetVertex(0).x;
        float z = shp->GetVertex(0).y;
        float y = GLandscape->SurfaceY(x, z) + 3;
        Vector3 first(x,y,z);
        vectorBuffer.Add(first);
        for(unsigned int  j=1; j < shp->GetVertexCount(); ++j)
        {
          x = shp->GetVertex(j).x;
          z = shp->GetVertex(j).y;
          y = (shp->GetVertex(j).z == 0) ? GLandscape->SurfaceY(x, z) + 3 : shp->GetVertex(j).z;
          vectorBuffer.Add(Vector3(x,y,z));
        }  

        const ShapePolyLine* shapePolyLine = dynamic_cast<const ShapePolyLine*>(shp);
        if(shapePolyLine)
        {
          //there should be more efficient ways
          for(int i=1; i< vectorBuffer.Size(); ++i)
            GVBSVisuals.Add3dLine(vectorBuffer[i-1], vectorBuffer[i], _color);
//            GEngine->DrawLine3D(vectorBuffer[i-1], vectorBuffer[i], _color, 0);
        }
      }
    }
  }
  return true;
}

#include "../ui/uiMap.hpp"

TypeIsSimple(DrawCoord)
bool ShapeLayer::DrawExt(float alpha, CStaticMap &map)
{
  if(!_shapeOrg)
    return false;

  ShapeList& sList = _shapeOrg->GetShapeList();
  
  //todo: use foreach?

//todo: use alpha?
  AutoArray<DrawCoord> vectorBuffer;
  Vector3 offset(0.0f,0.2f,0.0f);

  Vector3 realLT = map.ScreenToWorld(Point2DFloat(map.X(), map.Y()));
  Vector3 realRB = map.ScreenToWorld(Point2DFloat(map.X() + map.W(), map.Y() + map.H()));

  const Rect2DAbs screenRect(map.X(),map.Y(),map.W(),map.H());

  for(unsigned int  i=0; i < sList.Size(); ++i)
  {
    IShape* shp = sList.GetShape(i);
    if(!shp) continue;
    
    //map is not part of the bounding box, continue
    DBox& boundingBox = shp->CalcBoundingBox();
    if(!boundingBox.IntersectionXY(
        DBox(DVector(realLT.X(), realRB.Z()), DVector(realRB.X(), realLT.Z())))
       ) continue;

    shp = shp->Crop(realLT.X(), realRB.Z(), realRB.X(), realLT.Z());

    ASSERT(shp != NULL);
    //TODO: Multiparts?
    //TODO: use origin rather here to enable quick change of origin? 
    // Point2DFloat
    if(shp && shp->GetVertexCount() > 1)
    {
      vectorBuffer.Clear();
      float x = shp->GetVertex(0).x;
      float z = shp->GetVertex(0).y;
      float y = GLandscape->SurfaceY(x, z) + 0.5;

      DrawCoord first = map.WorldToScreen(Vector3(x,y,z) + offset);
      vectorBuffer.Add(first);

      const char* name = _shapeOrg->GetShapeParameter(i,"name");
      if(name != 0)
      {
        float width = GEngine->GetTextWidth(map._sizeGrid, map._fontGrid, name);
        DrawCoord textPos = first;
        saturate(textPos.x, map.X(),map.X() + map.W() - width);
        saturate(textPos.y, map.Y(),map.Y() + map.H() - map._sizeGrid);
        GLOB_ENGINE->DrawText(textPos, map._sizeGrid, Rect2DFloat(map.X(), map.Y(), map.W(), map.H())
          , map._fontGrid, map._colorGrid, name
          );
      }
      for(unsigned int  j=1; j < shp->GetVertexCount(); ++j)
      // only use first part of every shapefile
      // TODO: ignore NULL Parts ?
//      for(int j=1; j < shp->GetPartSize(0); ++j)
      {
        x = shp->GetVertex(j).x;
        z = shp->GetVertex(j).y;
        y = GLandscape->SurfaceY(x, z) + 0.5;
        vectorBuffer.Add(map.WorldToScreen(Vector3(x,y,z) + offset));
      }  

      const ShapePolygon* shapePolygon = dynamic_cast<const ShapePolygon*>(shp);
      if (shapePolygon) //close polygon
      {
       if(shp->GetVertexCount() < 3) continue;

//       int n = vectorBuffer.Size() -1;
//       saturateMin(n, 32); //split shapes?
//   DrawPolygon(vectorBuffer, _color);
//       n = 3;
//       AutoArray<Vertex2DPixel> vs;
//       vs.Resize(n);

/*       static Vertex2DPixel vs[32];
//       AUTO_STATIC_ARRAY(Vertex2DAbs, vs, 16);
//       vs.Resize(n);

        for(int i=n-1; i>= 0; --i)
        {
          vs[i].u = 0;
          vs[i].v = 0;
          vs[i].x = vectorBuffer[i].x * map._wScreen;
          vs[i].y = vectorBuffer[i].y * map._hScreen;
          vs[i].color = _color;
        }

        int special=IsAlpha|NoShadow|IsAlphaFog|IsColored;

        MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
//        GLOB_ENGINE->DrawPoly(mip, vs, n, map._clipRect, special);
*/
        for(int i=1; i< vectorBuffer.Size(); ++i)
        {
          PackedColor color = _color;
          color.SetA8(255);

          GLOB_ENGINE->DrawLine
          (
          Line2DPixel(
            vectorBuffer[i-1].x * map._wScreen, vectorBuffer[i-1].y * map._hScreen,
            vectorBuffer[i].x * map._wScreen, vectorBuffer[i].y * map._hScreen
          ),
          color, color, map._clipRect
          );
        }
      }
      else
      {
        const ShapePolyLine* shapePolyLine = dynamic_cast<const ShapePolyLine*>(shp);
        if(shapePolyLine)
        {
          //there should be more efficient ways
          for(int i=1; i< vectorBuffer.Size(); ++i)
          {
            GLOB_ENGINE->DrawLine
              (
              Line2DPixel(
              vectorBuffer[i-1].x * map._wScreen, vectorBuffer[i-1].y * map._hScreen,
              vectorBuffer[i].x * map._wScreen, vectorBuffer[i].y * map._hScreen
              ),
              _color, _color, map._clipRect
              );
          }
        }
      }
    }
  }
  return true;
}
/*

bool ShapeLayer::AddRoadSegement(RoadLink *item, VertexArray *vertexes = NULL)
{
  if(!item)
    return false;

  Vector3 v = item->GetCenter();
  vertexes.AddVertex(DVertex(v.X(), v.Z()));

  for (int i=0; i<road->NConnections(); i++)
  {
    if(!AddRoadSegement(vertexes, item->PosConnections()[i]))
    {

      ShapePolyLine* currentLine = new ShapePolyLine();

    }
  }
}
*/

void ShapeLayer::DrawRoads(int x, int z)
{
/*  const RoadList &roadList = GLandscape->GetRoadNet()->GetRoadList(x, z);
  
  ShapePolyLine* currentLine = NULL;
  for (int i=0; i<roadList.Size(); i++)
  {
    RoadLink *item = roadList[i];
    
    AddRoadSegement(item);
  }
    /*
    MapObject *obj = list[o];
    if (!obj) continue;
    switch (obj->GetType())
    {
    case MapTrack:
      //set properties later
      break;
    case MapRoad:
      //set properties later
      break;
    case MapMainRoad:
      //set properties later
      break;
    default:
      // not a road
      continue;
    }


    Vector3 ptTL = obj->GetTLPosition();
    Vector3 ptTR = obj->GetTRPosition();
    Vector3 ptBL = obj->GetBLPosition();
    Vector3 ptBR = obj->GetBRPosition();

    const int n = 4;
    POINT vs[n];
    vs[0].x = ptTL.X();
    vs[0].y = ptTL.Z();
    vs[1].x = ptBL.X();
    vs[1].y = ptBL.Z();
    vs[2].x = ptBR.X();
    vs[2].y = ptBR.Z();
    vs[3].x = ptTR.X();
    vs[3].y = ptTR.Z();
    SelectObject(hDC, brush);
    SelectObject(hDC, GetStockObject(NULL_PEN));
    Polygon(hDC, vs, n);

    SelectObject(hDC, pen);
    MoveToEx(hDC, toInt(Coef * ptTL.X()), CONVY(Coef * ptTL.Z()), NULL);
    LineTo(hDC, toInt(Coef * ptBL.X()), CONVY(Coef * ptBL.Z()));
    MoveToEx(hDC, toInt(Coef * ptTR.X()), CONVY(Coef * ptTR.Z()), NULL);
    LineTo(hDC, toInt(Coef * ptBR.X()), CONVY(Coef * ptBR.Z()));
*/
}

bool ShapeLayer::ExportShapeLayer(RString fileName)
{
  if(_shapeOrg)
    delete _shapeOrg;

  _shapeOrg = new ShapeOrganizer;
  
  return true;
/*
  int landRange = GLandscape->GetLandRange();
  
  for (int z=0; z<LandRange; z++)
    for (int x=0; x<LandRange; x++)
    {
      DrawRoads(x, z);
    }
*/
}

ShapeLayers::ShapeLayers()
{

}

bool ShapeLayers::Draw()
{
  for(int i=0; i < _shapeLayers.Size(); ++i)
  {
    ShapeLayer &shape = _shapeLayers[i];
    if(!shape.hidden)
      shape.Draw();
  }
  return true;
}

bool ShapeLayers::DrawExt(float alpha, CStaticMap& map)
{
  for(int i=0; i < _shapeLayers.Size(); ++i)
  {
    ShapeLayer &shape = _shapeLayers[i];
    if(!shape.hidden)
      shape.DrawExt(alpha, map);
  }
  return true;
}

int ShapeLayers::AddShape(RString name, bool missionDir)
{
  int index = _shapeLayers.Add();
  ShapeLayer& shape = _shapeLayers[index];
  if(!shape.Load(name, missionDir))
  {
    _shapeLayers.Delete(index);
    return -1;
  }
  return index;
}

bool ShapeLayers::RemoveShape(int index)
{
  if(index <0 || index >=_shapeLayers.Size())
    return false;
  _shapeLayers.Delete(index);
  return true;
}

ShapeLayer *ShapeLayers::GetShape(int index)
{
  if(index <0 || index >=_shapeLayers.Size())
    return NULL;
  return &_shapeLayers[index];
}

ShapeLayer *ShapeLayers::GetShape(RString name)
{
  return GetShape(GetShapeIndex(name));
}

int ShapeLayers::GetShapeIndex(RString name)
{
  for(int i=0; i< _shapeLayers.Size(); ++i)
  {
    if(_shapeLayers[i].GetName() == name)
      return i;
  }
  return -1;
}

void ShapeLayers::Clear()
{
  _shapeLayers.Clear();
}


#endif //_VBS3_SHAPEFILES
