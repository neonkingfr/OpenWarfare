#include "../wpch.hpp"

#if _AAR
#include "AAR.hpp"
#include "AARDebug.hpp"

#include "HLA_base.hpp"
#include "../objLine.hpp"

#include <El/ParamFile/paramFile.hpp>
#include <El/Common/perfProf.hpp>
#include <El/elementpch.hpp>
#include <El/Debugging/debugTrap.hpp>

#include "../global.hpp"
#include "../progress.hpp"
#include "../stringtableExt.hpp"
#include "../Engine.hpp"
#include "../UI/uiMap.hpp"
#include "../shots.hpp"
#include "../engine.hpp"
#include "../Network/networkImpl.hpp"
#include "../scene.hpp"
#include "../camera.hpp"
#include "../soldierOld.hpp"
#include "../landscape.hpp"


/*
\VBS_patch 1.7  Date [7/09/2007] by clion
- Fixed: When loading config file loadConfig, will forcefully close the file now instead of leaving it open.
\VBS_patch 1.7  Date [5/09/2007] by clion
- Fixed: Char array was not null terminating in some recordings, leading to crash when saving
\VBS_patch 1.7  Date [5/09/2007] by clion
- Added: Error checking when saving AAR recording:
  AAR checks that recording was successfull before clearing internal buffers.
\VBS_patch 1.7  Date [4/09/2007] by clion
- Chanaged: Path history now is smoother, instead of being jumping when moving time line is moved.
\VBS_patch 1.7  Date [4/09/2007] by clion
- Fixed: Remote saving multiplayer commands made easier to work with, now ask and do request.
\VBS_patch 1.7  Date [4/09/2007] by clion
- Fixed: HitLines now disapear when using moving to different recording time.
\VBS_patch 1.00 Date [2/14/2008] by clion
- Added: Mission briefing is now avaliable inside AAR
\VBS_patch 1.00 Date [2/14/2008] by clion
- Added: Markers now show up correctly with new AAR recording, layered markers now supported
\VBS_patch 1.00 Date [2/19/2008] by clion
- Fixed: Slave objects now, unslave and slave to objects when moving time slider
\VBS_patch 1.00 Date [2/19/2008] by clion
- Fixed: Markers now delete/create correctly when moving time slider
*/
const char *AARDataTypeString[] = 
{
  MyEnumType(ENUMSTRING)
};

// Global declaration of AAR, and filelister
AAR GAAR;

bool IsNullEntityId(const EntityId &id)
{
  if(id.ApplicationNumber == 0 &&
    id.EntityNumber == 0)
    return true;

  return false;
}

bool IsSameEntityId(const EntityId &id1, const EntityId &id2)
{
  if(id1.ApplicationNumber == id2.ApplicationNumber)
    if(id1.EntityNumber == id2.EntityNumber)
      return true;

  return false;
}

bool IsNetworkObject(const EntityId &id)
{
  NetworkObject *nObj = _hla.GetNetObjFromHLAId(id);
  
  return (nObj != NULL);
}

bool IsEntity(const EntityId &id)
{
  NetworkObject *nObj = _hla.GetNetObjFromHLAId(id);
  Assert(nObj);
  Entity *entity = _hla.GetEntity(nObj);

  return (entity != NULL);
}

bool IsObjectIdAObject(const EntityId &Id)
{
  // We expect all objects passed are atleast Entity
  NetworkObject *nObj = _hla.GetNetObjFromHLAId(Id);
  Assert(nObj);
  Entity *entity = _hla.GetEntity(nObj);
  Assert(entity);
  if (!entity) return false;

  if(!entity->GetObjectId().IsObject())
    return true;

  return false;
}

// Returns true if id is valid as nullEntityId
// Returns true if id is a non static object
// Returns false if id is not null, and is a static object 
bool ValidateData(const EntityId &id)
{
  if(IsNullEntityId(id)) return true;
  if(IsObjectIdAObject(id)) return true;

  return false;
}

BulletIcon::BulletIcon()
{
  _icon = "ca\\ui\\data\\marker_dot_ca.paa";
  _size = 5.0;
  _color = PackedColor(Color(0,0,0,255));
}

void BulletIcon::Init(ParamEntryVal entry)
{
  _color = GetPackedColor(entry >> "color");
  _icon =  entry >> "icon";
  _size =  entry >> "size";
}

HitLine::HitLine():_color(PackedColor(Color(0,0,0,255))){};

void HitLine::Init(ParamEntryVal entry)
{
  _color = GetPackedColor(entry >> "color");
}

DynamicLoadMessage::DynamicLoadMessage( AARMessageBase *msg, QFileSize start, QFileSize end )
{
  Assert(msg);

  _message = msg;
  _startPos = start;
  _endPos   = end;
  _time = msg->_time;
  _type = msg->TypeIs();
  _requested = false;
  _requestCompleted = false;
}

DynamicLoadMessage::DynamicLoadMessage():
_startPos(0),
_endPos(0),
_time(0.0),
_type(TypeLimit),
_requested(false),
_requestCompleted(false)
{

}

void DynamicLoadMessage::RequestLoad()
{
  Assert(!_requested);
  Assert(_endPos >= _startPos );
  GAAR._fin->PreReadObject(this,NULL,FileRequestPriority(1),_startPos,_endPos-_startPos);
  _requested = true;
  _requestCompleted = false;
}

void DynamicLoadMessage::Unload()
{
  Assert(_requested);
  _requested = false;
  _requestCompleted = false;
  _message.Free();
}

bool DynamicLoadMessage::IsRequested(){ return _requested; }
bool DynamicLoadMessage::IsRequestDone(){ return _requestCompleted; }

void DynamicLoadMessage::RequestDone(RequestContext *context, RequestResult result) 
{
  if(result == RRSuccess)
  {
    IFileBuffer *source = GAAR._fin->GetSource();
    QIStreamDataSource input;
    input.AttachSource(source);
    input.seekg(_startPos);

    SerializeBinStream serializeIn(&input);

    // we create a dummy system, to load the data to do a test
    Assert(_type == enumEntityUpdateData);
    Assert(!_message);

    _message = new AAREntityUpdateData;
    _message->Serialize(serializeIn);   
    _message->PreProccess(0);

    // we tell aar that we've successfully loaded
    ++GAAR._msgInCount;
  }

  _requestCompleted = true;
  base::RequestDone(context,result);
}

void KeyFrames::AddKeyFrameMessage()
{
  ++_keyframe;
  for( int i = 0; i < Size(); ++i )
  { 
    int size  = Get(i)._messages.Size();
    Ref<AAREntityUpdateData> msg = Get(i)._messages[size-1];
    
    Set(i)._messages.Add(msg);
  }
}

void KeyFrames::AddNeededKeyFrames(Ref<AAREntityUpdateData> message)
{
  Assert(Size()>=1);

  // 2 * 30.0f = 60, everthing above 60 is considered
  // a new keyframe. 
  // >  0 new frame
  // > 30 new frame
  // > 60 new frame
  float maxKeyFrame = Get(0)._messages.Size()*_keyFrameInterval;

  if( message->_time > maxKeyFrame)
  {
    AddKeyFrameMessage();
    AddNeededKeyFrames(message);
  }
}


KeyFrames::KeyFrames():_keyFrameInterval(10.0f){ Init(); };

void KeyFrames::Init()
{
  _keyframe = 0;
}

float KeyFrames::GetKeyTime() const
{
  return _keyFrameInterval;
}

void KeyFrames::CheckConsistency()
{
  if(Size()>=1)
  {
    int baseSize = Get(0)._messages.Size();
    for(int i = 0; i < Size(); ++i )
    {
      Assert(Get(i)._messages.Size()==baseSize);
    }
  }
}

void KeyFrames::AddMessage(EntityId id, Ref<AAREntityUpdateData> message)
{
  if(Size()>=1)
    AddNeededKeyFrames(message);
  
  // do a consistency check
  CheckConsistency();

  bool found = false;
  for( int i = 0; i < Size(); ++i )
  {
    if( Get(i)._entity.ApplicationNumber == id.ApplicationNumber &&
      Get(i)._entity.EntityNumber      == id.EntityNumber )
    {
      // we over-write the message, with the newest if possible.
      Assert((Get(i)._messages.Size()-1)==_keyframe);
      Set(i)._messages.Set(_keyframe) = message;
      found = true;
      break;
    }
  }

  if(!found)
  {
    int index = Add();
    Set(index)._entity = id;

    if(Get(0)._messages.Size()>=1)
    {     
      int keyFramesSize = Get(0)._messages.Size();
      for( int x = 0; x < keyFramesSize; ++x )
        Set(index)._messages.Add();// we fill all its keyframes with null
    }
    else
    {
      Set(index)._messages.Add(message);
    }

    AddMessage(id,message);
  }

  // do a consistency check
  CheckConsistency();
}


AAR::AAR()
{
  Init();
}

void AAR::InitFields()
{
  if(!Pars.CheckIfEntryExists("cfgAAR"))
    return;
  ParamEntryVal cfgAAR = Pars.FindEntry("cfgAAR");

  ParamEntryVal bulletIcon = cfgAAR >> "BulletIcon";
  _bulletIcon[TEast].Init(bulletIcon >> "East");
  _bulletIcon[TWest].Init(bulletIcon >> "West");
  _bulletIcon[TGuerrila].Init(bulletIcon >> "Guerrila");
  _bulletIcon[TCivilian].Init(bulletIcon >> "Civilian");

  ParamEntryVal hitLine = cfgAAR >> "HitLine";
  _hitLine[TEast].Init(hitLine  >> "East");
  _hitLine[TWest].Init(hitLine  >> "West");
  _hitLine[TGuerrila].Init(hitLine  >> "Guerrila");
  _hitLine[TCivilian].Init(hitLine  >> "Civilian");
}

bool AAR::IsReplayMode() 
{
  return _replayMode;
}

void AAR::SetReplayMode(bool mode) 
{
  _replayMode = mode;
}

class DrawFastVehicles
{
  OLinkArray(Entity) &_objects;
public:
  DrawFastVehicles(OLinkArray(Entity) &objects):_objects(objects){};
  bool operator () (Entity *obj) const
  {
    _objects.Add(obj);

    return false;
  };
};

void AAR::DrawExt(float alpha,CStaticMap &map)
{
  if(!IsPlaying())
    return;

  float _wScreen = GLOB_ENGINE->Width2D();
  float _hScreen = GLOB_ENGINE->Height2D();

  OLinkArray(Entity) shots;
  DrawFastVehicles getShots(shots);
  GWorld->ForEachFastVehicle(getShots);

  // Draw all the fast vehicles on the map
  //
  for( int i = 0; i < shots.Size(); i++ )
  {
    Entity *tmp = shots[i];
    Shot *missle = dynamic_cast<Shot*>(tmp);
    if(!missle)
      continue;
    EntityAI* obj = missle->GetOwner();
    if(!obj)
      continue;

    Ref<Texture> text = GlobLoadTexture(_bulletIcon[obj->GetTargetSideAsIfAlive()]._icon);
    map.DrawSign
    (
      text,
      _bulletIcon[obj->GetTargetSideAsIfAlive()]._color,
      shots[i]->Position(),
      _bulletIcon[obj->GetTargetSideAsIfAlive()]._size,
      _bulletIcon[obj->GetTargetSideAsIfAlive()]._size,
      0
    );
  }
  if(GVBSVisuals.GetDrawHitLine())
  {
    //draw all the detonation data
    for(int i = 0; i < _detonationBuffer.Size();i++)
    {
      Detonation &detonation = _detonationBuffer[i];
      // Detected sombody got hit!
      if(detonation.TargetId.EntityNumber>0)
      {
        EntityAI *shoter = GetEntityAI(detonation.FiringId);
        if(!shoter)
          continue;

        if(detonation.sPos.X()==0.0f)
        {
          Man *man = dyn_cast<Man>(shoter);
          if(man)
          {
            detonation.sPos = man->AimingPosition();
          }
          else
          {
            detonation.sPos = shoter->Position();
          }
        }

        DrawCoord pt1 = map.WorldToScreen(detonation.sPos);
        DrawCoord pt2 = map.WorldToScreen(detonation.tPos);

        PackedColor color = _hitLine[shoter->GetTargetSideAsIfAlive()]._color;

        GLOB_ENGINE->DrawLine
        (
          Line2DPixel(
          pt1.x * _wScreen, pt1.y * _hScreen,
          pt2.x * _wScreen, pt2.y * _hScreen),
          color, color, map._clipRect
        );
      }
    }
  }
}

void AAR::Init()
{
  _fout   = NULL;
  _fin    = NULL;
  _stream = NULL;

  _fileLoaded  = false;
  _dataChanged = false;
  _absolutePathLoadedFile = RString();
  _tmpFileAbsolutePath    = RString();

  _replayMode   = false;
  _previousTime = GlobalTickCount();

  _version = CURRENT_AAR_VERSION; // Define the version
  _currentTimePos = 0.0f;
  _replayIndex    = 0;
  _timeOffset     = 0.0f;
  _hitLineHistory = 1.0f;
  _moveToTime     = -1.0f;
  _lookAheadTime  = 2.0f;

  _play         = false;
  _repeat       = false;
  _pause        = false;
  _inSimulation = false;

  _saveState    = 0;
  _record       = false;
  _remoteRecord = false;


  ClearAllQues();

  _ptr2CreateEntity = NULL;
  _ptr2UpdateEntity = NULL;
  _ptr2DeleteEntity = NULL;
  _ptr2DeleteGroup  = NULL;
  _ptr2FireWeapon   = NULL;
  _ptr2CenterCreate = NULL;
  _ptr2GroupCreate  = NULL;
  _ptr2UnitCreate   = NULL;
  _ptr2DoDamage     = NULL;
  _ptr2UpdateUnit   = NULL;
  _ptr2UpdateGroup  = NULL;
  _ptr2DetonateMunition = NULL;
  _ptr2VehicleDestroyed = NULL;
}

AAR::~AAR(){}

LSError AAR::OpenInputFileStream(RString &path)
{
   // open the file stream
  _fin = new QIFStream();
  _fin->open(path);
  if( _fin->fail() || _fin->eof() )
  {
    delete _fin;
    _fin = NULL;
    WarningMessage("Error loading file %s", (const char*) path );
    ResetErrors();

    return LSFileNotFound;
  }
  _stream = new SerializeBinStream(_fin);

  return LSOK;
}

void AAR::CloseInputFileStream()
{
  Assert(_stream);
  Assert(_fin);

  delete _stream;
  delete _fin;

  _fin = NULL;
  _stream = NULL;
}

void AAR::RemoteAddBookMark(float jumpTo,const RString &name,const RString &message)
{
  for( int i = 0; i < _bookMarks.Size(); ++i)
    if(name == _bookMarks[i]->GetName())
      return;// dont add duplicants
}

void AAR::RemoteRemoveBookMark(RString &name)
{
  for( int i = 0; i < _bookMarks.Size(); ++i)
  {
    if(name == _bookMarks[i]->GetName())
    {
        _bookMarks.Delete(i);
        break;
    }
  }
}

// Returns the index of the newly added bookmark
int AAR::AddBookMark(float jumpTo,const RString &name,const RString &message)
{
  if(!GetNetworkManager().IsServer()) return -1;

  Ref<BookMarkLifeSpan> bookMark = new BookMarkLifeSpan(name,jumpTo,message,enumBookMarkLifeSpan);
  _bookMarks.Add(bookMark);

  AARAddBookMark *msg = new AARAddBookMark(name,jumpTo,message);

  // Book marks can be added or appended to the file
  if(IsRecording())
    AddMessageToArray(msg);
  else
    AddMessageToAppendArray(msg);

  // notify we need to save file when unloading
  _dataChanged = true;

  // send network message to server. to add this bookmark
  GetNetworkManager().AARDoUpdate(AddBookMK,0,name,message,jumpTo);

  return _bookMarks.Size()-1;
}
 
void AAR::RemoveBookMark(RString &name)
{
  if(!GetNetworkManager().IsServer()) return;

  for( int i = 0; i < _bookMarks.Size(); ++i)
  {
    if(name == _bookMarks[i]->GetName())
    {
      AARDeleteBookMark *msg = new AARDeleteBookMark(name);

      if(IsRecording())
        AddMessageToArray(msg);
      else
        AddMessageToAppendArray(msg);

      _bookMarks.Delete(i);
      break;
    }
  }

  _dataChanged = true;

  // Send network message!
  GetNetworkManager().AARDoUpdate(RemBookMK,0,name);
}


void AAR::RemoveBookMark(int pos)
{
  Assert( pos >= 0 && pos < _bookMarks.Size());
  if(!GetNetworkManager().IsServer()) return;

  // send server to remove this bookmark
  GetNetworkManager().AARDoUpdate(RemBookMK,0,_bookMarks[pos]->GetName());

  RString name = _bookMarks[pos]->GetName();
  RemoveBookMark(name);
}

ObjectLifeSpan &AAR::GetLifeSpan(int index)
{
  Assert(index >= 0);
  Assert(index < _entityLifeSpan.Size());
  return *_entityLifeSpan[index].GetRef();
};

const AutoArray<float> &AAR::GetFireEventTimes(TargetSide side)
{ 
  Assert(side < TSideUnknown);
  return _fireEventsTimes[side];
}

const AutoArray<float> &AAR::GetDeathTimes(TargetSide side)
{
  Assert(side < TSideUnknown);
  return _deathTimes[side]; 
}

BookMarkLifeSpan &AAR::GetBookMark(int i)
{
  Assert( i >= 0 && i < _bookMarks.Size());
  return *_bookMarks[i].GetRef();
}

int AAR::GetLogEventSize()
{
  return _cnrEvents.Size();
}

//! Get the cached CNR log events
LogEvent &AAR::GetLogEvent(int i)
{
  Assert( i >= 0 && i < _cnrEvents.Size());
  return _cnrEvents[i];
}

void AAR::AddFireEventTime(TargetSide side,float time)
{
  Assert(side < TSideUnknown);
  Assert(time >= 0.0f);

  _fireEventsTimes[side].Add(time);
}


void AAR::AddDeathTime(TargetSide side,float time)
{
  Assert(side < TSideUnknown);
  Assert(time >= 0.0f);

  _deathTimes[side].Add(time);
}

int AAR::GetBookMarkSize()
{
  return _bookMarks.Size(); 
}

static int CmpDeaths(const float *c1, float *c2){ return sign(*c1 - *c2);}

int AAR::InsertMessage(DynamicLoadMessage *msg)
{
  Assert(msg);

  // All 0.0 message have to follow and order.
  // Other messages are just appended to the end.
  static int insertOrder = 0;
  if(msg->_time == 0.0f)
  {
    _messages.Insert(insertOrder,msg);
    ++insertOrder;
    return insertOrder;
  }

  for( int i = 0; i < _messages.Size(); ++i )
  {
    if(_messages[i]->_time > msg->_time)
    {
      _messages.Insert(i,msg);
      return i;
    }
  }

  return _messages.Add(msg);
}


void AAR::Truncate(RString &absPath, int toSize )
{
  RString newFile;
  int pos = absPath.ReverseFind('.');
  if(pos==-1)
    return;
  
  newFile = RString(absPath,pos) + RString("_BackUp.aar");
  QIFileFunctions::Copy(absPath, newFile);

  // Flish the read handels to this file before proccessing
  GFileServerFunctions->FlushReadHandle(absPath);

  // Open the file and seek to the position.
  // then truncate and close
  HANDLE truncateFile;
  truncateFile = CreateFile
  (
    absPath,
    GENERIC_WRITE,
    0,
    NULL,
    OPEN_EXISTING,
    FILE_ATTRIBUTE_NORMAL,
    NULL
  );

  if(truncateFile != INVALID_HANDLE_VALUE)
  {
    SetFilePointer(truncateFile,(long)toSize,NULL,0);
    SetEndOfFile(truncateFile);
    CloseHandle(truncateFile);
  }
}



LSError AAR::LoadFile(RString &absPath)
{
  Assert(!_fin);
  Assert(_messages.Size()==0);
  ResetErrors();

  // copy the path
  _absolutePathLoadedFile = absPath;
  _fileLoaded = false;

  LSError error = OpenInputFileStream(absPath);
  _currentTimePos = 0.0f;
  _replayIndex = 0;

  if(error != LSOK)
  {
    switch(error)
    {
      case LSAccessDenied:
        ErrorMessage(EMError,"Cannot access file, access denied");
      case LSDiskError:
        ErrorMessage(EMError,"Cannot access file, disk error");
      case LSFileNotFound:
        ErrorMessage(EMError,"File not found");
      default:
        ErrorMessage(EMError,"Unknown Error occurred well opening file");
    }
    ResetErrors();
    return error;
  }

  // Get the file size used for debugging
  _fileSize = QFBankQueryFunctions::GetFileSize(absPath);

  IProgressDisplay *CreateDisplayLoadIsland(RString text, RString date);
  Ref<ProgressHandle> p = ProgressStart(CreateDisplayLoadIsland("Loading AAR", RString()));

  ProgressReset();
  ProgressAdd(9);
  ProgressRefresh();

  _stream->TransferBinary(_version);
  if(_stream->GetError()!=SerializeBinStream::EOK)
  {
    ErrorLoadingFile();
    ErrorMessage(EMError,"Unexpected end of file");
    ResetErrors();

    CloseOutputFileStream();
    return LSUnknownError;
  }

  if(_version > CURRENT_AAR_VERSION)
  {
    ErrorLoadingFile();
    ProgressAdvance(9);
    ProgressRefresh();
    ProgressFinish(p); // finish it off

    WarningMessage("Unable to play AAR file, file version is newer. VBS2 AAR version:%d File Version:%d",CURRENT_AAR_VERSION,_version);
    ResetErrors();

    return LSUnknownError;    
  }

  // Versions bellow version 3 are not supported
  if(_version < 3)
  {
    ErrorLoadingFile();
    ProgressAdvance(9);
    ProgressRefresh();
    ProgressFinish(p); // finish it off

    WarningMessage("Unsupported old AAR file version, file version %d current version:%d",_version,CURRENT_AAR_VERSION);
    ResetErrors();

    return LSUnknownError;
  }

  // report loading file now to the console system,
  // used for debugging
  LstF("*******");
  LstF("AAR Report:");
  LstF("FileName:%s",(const char*)absPath);
  LstF("Version:%d",_version);

  AARLoadError loaderror;
  int safePoint = 0;
  while(true)
  {
    safePoint = _stream->GetLoadStream()->tellg();
    loaderror = ProccessMessage(*_stream);
    if(loaderror != AAR_OK)
      break;
  }

  // Panic!, failed AAR file. Need dialog for user to resave.
  // We cannot append to the file without knowings its all valid.
  if(loaderror == AAR_FailedLoad)
  { 
    ErrorLoadingFile();

    ProgressAdvance(9);
    ProgressRefresh();
    ProgressFinish(p);

    // Bellow version 14
    // we cannot recover, lifeTime is lost!
    if(_version < 14)
      return LSUnknownError;

    // we now just resize the file to the nearst safe point if one is avaliable.
    if(safePoint > 0)
      Truncate(absPath,safePoint);
    
    // return error back saying to reload the file again.
    return LSBadFile;
  }

  ProgressAdvance(3);
  ProgressRefresh();

  //////////////////////////////////////////////////////////////////////////
  /// Cirtical data secriont starts here. If an error occures here, all best are off
  bool critcalFailure = false;

  LstF("Processing GroupLifeSpans");
  for(int i=0; i < _groupLifeSpan.Size(); ++i)
  {
    GroupLifeSpan* Item = _groupLifeSpan[i];

    // Link to the centerID, this is critical because we 
    // centers are the main different sides of the map, and are not
    // really de-allocated during gameplay
    FindEntityID findcenter(Item->_data.CenterId);
    _centerLifeSpan.ForEachF(findcenter);
    Item->_centerLifeSpanId = findcenter.GetResult();
    if(Item->_centerLifeSpanId ==-1)
      critcalFailure = true;

    // Group can now spawn without the need of a leader of the group
    // This is updated inside groupUpdate. The first person to join
    // the group is the leader, unless otherwise stated by group update
    FindEntityID findleader(Item->_data.LeaderId);
    _unitLifeSpan.ForEachF(findleader);
    Item->_leaderLifeSpanId = findleader.GetResult();

    DLogF("[AAR] GroupLifeSpan (%d) new _centerLifeSpanId: %d, _leaderLifeSpanId: %d"
      , i, Item->_centerLifeSpanId, Item->_leaderLifeSpanId );

    if(critcalFailure)
      break;
  }

  // Update if we hit a critical error
  if(critcalFailure)
  {
    ErrorLoadingFile();

    ProgressAdvance(9);
    ProgressRefresh();
    ProgressFinish(p);

    ErrorMessage(EMError,"Failed to load AAR file");
    ResetErrors();

    LstF("Failed to proccess GroupLifeSpans");
    return LSUnknownError;
  }
  
  LstF("Processing UnitLifeSpan");
  for(int i=0; i < _unitLifeSpan.Size(); ++i)
  {
    UnitLifeSpan* Item = _unitLifeSpan[i];

    // Brain now does not belong to a group when it inialy spawns
    // This is no longer a critical error.
    FindEntityID findGroup(Item->_data.GroupId);
    _groupLifeSpan.ForEachF(findGroup);
    Item->_groupLifeSpanId = findGroup.GetResult();

    // Every Brain need to belong to a unit, this is critical
    FindEntityID findperson(Item->_data.PersonId);
    _entityLifeSpan.ForEachF(findperson);
    Item->_entityLifeSpanId = findperson.GetResult();
    if(Item->_entityLifeSpanId == -1)
      critcalFailure = true;

    DLogF("[AAR] UnitLifeSpan (%d) new _groupLifeSpanId: %d, _entityLifeSpanId: %d"
      ,i, Item->_groupLifeSpanId, Item->_entityLifeSpanId);

    if(critcalFailure)
      break;
  }

  if(critcalFailure)
  {
    ErrorLoadingFile();

    ProgressAdvance(9);
    ProgressRefresh();
    ProgressFinish(p);

    ErrorMessage(EMError,"Failed to load AAR file");
    ResetErrors();

    LstF("Failed to proccess UnitLifeSpan");
    return LSUnknownError;
  }

  LstF("Inserting messages into message bank");
  for( int i = 0; i < _entityLifeSpan.Size(); ++i )
  {
    if(_entityLifeSpan[i]->_death == 0.0) _entityLifeSpan[i]->_death = _duration;

    Ref<AAREntityLifeSpanIndex> createEnt = new AAREntityLifeSpanIndex(i,_entityLifeSpan[i]->_birth);
    Ref<AAREntityLifeSpanIndex> deleteEnt = new AAREntityLifeSpanIndex(i,_entityLifeSpan[i]->_death);
    int start = InsertMessage(new DynamicLoadMessage(createEnt,0,0)); 
    int end   = InsertMessage(new DynamicLoadMessage(deleteEnt,0,0));
    Assert(end > start);
  }
  for( int i = 0; i < _unitLifeSpan.Size(); ++i )
  {
    if(_unitLifeSpan[i]->_death == 0.0) _unitLifeSpan[i]->_death = _duration;

    Ref<AARUnitLifeSpanIndex> createEnt = new AARUnitLifeSpanIndex(i,_unitLifeSpan[i]->_birth);
    Ref<AARUnitLifeSpanIndex> deleteEnt = new AARUnitLifeSpanIndex(i,_unitLifeSpan[i]->_death);
    int start = InsertMessage(new DynamicLoadMessage(createEnt,0,0)); 
    int end   = InsertMessage(new DynamicLoadMessage(deleteEnt,0,0)); 
    Assert(end > start);
  }

  for( int i = 0; i < _groupLifeSpan.Size(); ++i )
  {
    if(_groupLifeSpan[i]->_death == 0.0) _groupLifeSpan[i]->_death = _duration;

    Ref<AARGroupLifeSpanIndex>  createEnt = new AARGroupLifeSpanIndex(i,_groupLifeSpan[i]->_birth);
    Ref<AARGroupLifeSpanIndex>  deleteEnt = new AARGroupLifeSpanIndex(i,_groupLifeSpan[i]->_death);
    int start = InsertMessage(new DynamicLoadMessage(createEnt,0,0)); 
    int end   = InsertMessage(new DynamicLoadMessage(deleteEnt,0,0)); 
    Assert(end > start);
  }
  /*
  for( int i = 0; i < _centerLifeSpan.Size(); ++i )
  {
    if(_centerLifeSpan[i]->_death == 0.0) _centerLifeSpan[i]->_death = _duration;

    Ref<AARCenterLifeSpanIndex> createEnt = new AARCenterLifeSpanIndex(i,_centerLifeSpan[i]->_birth);
    Ref<AARCenterLifeSpanIndex> deleteEnt = new AARCenterLifeSpanIndex(i,_centerLifeSpan[i]->_death);
    int start = InsertMessage(new DynamicLoadMessage(createEnt,0,0)); 
    int end   = InsertMessage(new DynamicLoadMessage(deleteEnt,0,0)); 
    Assert(end > start);
  }
  */

  LstF("Processing Messages");
  int   errorType[TypeLimit] = {0};
  int   countType[TypeLimit] = {0};
  int   sizeOfType[TypeLimit]= {0};
  for(int i = 0; i < _messages.Size();)
  {
    Ref<AARMessageBase> msg = _messages[i]->_message;
    if(msg)
    {
      // Pass the message index, used by some messages
      // to gather information about the timeline
      msg->PreProccess(i);

      if(!msg->_passPP)
      {
        errorType[msg->TypeIs()] += 1;
        _messages.DeleteAt(i);
      }
      else
      {
        countType[msg->TypeIs()] += 1;
        sizeOfType[msg->TypeIs()] = msg->SizeOf();
        i++;
      }
    }
    else
      i++;
  }
 


  // Print summary of network packets that
  // never made it through
  for( int i = 0; i < TypeLimit; i++ )
  {
    LogF("Type:%23s SuccessC:%8d ErrorC:%3d Size:%5d TotalSize:%5d TotalSize:%4.3f",
      AARDataTypeString[i],
      countType[i],
      errorType[i],
      sizeOfType[i],
      (countType[i]*sizeOfType[i])/1024/1024,
      (sizeOfType[i]*countType[i])/1024.0f/1024.0f );
  }

  int dynamicMsgSize = sizeof(DynamicLoadMessage);
  LogF("Dynamic Message Size:%d Complete overhead:%d",dynamicMsgSize,dynamicMsgSize*_messages.Size());

  if(critcalFailure)
  {
    ErrorLoadingFile();

    ProgressAdvance(9);
    ProgressRefresh();
    ProgressFinish(p);

    ErrorMessage(EMError,"Failed to load AAR file");
    ResetErrors();

    LstF("Failed pre-proccessing messages");
    return LSUnknownError;
  }
  /// Critcal section end
  //////////////////////////////////////////////////////////////////////////

  ProgressAdvance(6);
  ProgressRefresh();

  LstF("[AAR] PreProccessing lifespan critcal");
  PreprocessLifeSpan func;
  _entityLifeSpan.ForEachF(func);

  ProgressAdvance(9);
  ProgressRefresh();
  ProgressFinish(p); // finish it off

  if(func.GetError())
  {
    ErrorLoadingFile(); // unload the file, because of loading error.
    return LSNoEntry;
  }

  LstF("AAR file load successfull");


  LstF("Loading CNR logger system");
  // See if we can connect to the dummy interface
  if(_hla.ConnectDummyInterface())
    ProccessCNRLogEvent();
  LstF("Finished Loading CNR logger");


  _fileLoaded     = true;
  _dataChanged    = false;
  _centersSpawned = false;

  return error;
}

void AAR::ErrorLoadingFile()
{
  CloseInputFileStream();
  ClearAllQues();
}

void AAR::UnLoadFile()
{
  // here close the input file stream so we can save the file.
  CloseInputFileStream();

  if(IsPlaying()) 
    PlayStop();

  // write out any new appended messages
  if(_dataChanged)
  {
    QOFStream out;
    out.openForAppend(_absolutePathLoadedFile);
    if(out.error() == LSOK)
    {
      SerializeBinStream stream(&out);
        for( int i = 0; i < _appendMessages.Size(); ++i)
          _appendMessages[i]->Serialize(stream); 
    }
  }
  
  ClearAllQues();

  // Disconnect the dummy interface when we finished playing
  _hla.DisconnectDummyInterface();

  _fileLoaded  = false;
  _dataChanged = false;

  DLogF("[AAR] file unloaded!");
}

/*!
Saves all loaded messages, and lifespan
*/
LSError AAR::SaveLoadedFile()
{
  // removed for the time being
  return LSOK;
}


void AAR::Play()
{
  if(!IsLoaded())  return;
  if(!_inSimulation) return;

  if(_play&&_pause)
  {
    _pause = false;
    return;
  }

  _previousTime = GlobalTickCount();
  _currentTimePos = 0.0f;
  _replayIndex = 0;
  _play = true;
  DLogF("[AAR] start playing, length: %.2f", GetDuration());
}

class SetSpeedZero
{
public:
  SetSpeedZero(){};
  bool operator () (Entity *obj) const
  {
    obj->SetSpeed(VZero);

    return false;
  }
};

// Zero the speed of everthing in the world for paused.
void AAR::SetAllVelocity()
{
  SetSpeedZero zeroSpeed;
  GWorld->ForEachAnimal(zeroSpeed);
  GWorld->ForEachSlowVehicle(zeroSpeed);
  GWorld->ForEachVehicle(zeroSpeed);
}

void AAR::PlayPause(){ SetAllVelocity();_pause = !_pause;}

void AAR::PlayStop()
{
  //PlayStop didn't remove the group and unit markers in RTE ?!
  //have to investigate why it wasn't working without the following lines
  //which are copied from tick.
  //_currentTimePos = GetDuration() + 1;
  //RunMessages();
  _play = false;
  _pause = false;
  _currentTimePos = 0.0f;
  _replayIndex = 0;

  for( int i = 0; i < _entityLifeSpan.Size(); i++ )
  {
    if(_entityLifeSpan[i]->_newId.EntityNumber)
    {
      EntityDeleteData delMe = _entityLifeSpan[i]->_newId;
      _entityLifeSpan[i]->_newId = NullEntityId;

      DLogF("[AAR] deleteEntity (%d/%d)"
        , delMe.ApplicationNumber
        , delMe.EntityNumber
        );

      if(_ptr2DeleteEntity)
        _ptr2DeleteEntity(delMe);
    }
  }

  for( int i = 0; i < _unitLifeSpan.Size(); i++ )
    _unitLifeSpan[i]->_newId = NullEntityId;

  for( int i = 0; i < _groupLifeSpan.Size(); i++ )
  {
    if(_groupLifeSpan[i]->_newId.EntityNumber)
    {
      GroupDeleteData delMe = _groupLifeSpan[i]->_newId;
      _groupLifeSpan[i]->_newId = NullEntityId;

      DLogF("[AAR] deleteGroup (%d/%d)"
        , delMe.ApplicationNumber
        , delMe.EntityNumber
        );

      if(_ptr2DeleteGroup)
        _ptr2DeleteGroup(delMe);
    }
  }

  ClearExplosions();

  // reset all the played events
  for( int i = 0; i < _cnrEvents.Size(); ++i )
    _cnrEvents[i].played = false;
}



void AAR::RemoteRecord()
{
  _saveState = -2;
  _timeOffset = Glob.time.toFloat();
  _remoteRecord = true;
}

void AAR::SaveWorldInfo()
{
  AARMessageWorldLoad *world = new AARMessageWorldLoad;
  world->Init();
  AddMessageToArray(world);
}

void AAR::StartCNRRecorder()
{
  // If we cant find the mission name for this recording use
  // last mission
  RString name = RString("LastMission");
  const MissionHeader *header = GetNetworkManager().GetMissionHeader();
  if (header) 
    name = header->GetLocalizedMissionName();    

  // Append the system time for a complete unique time stamp
  name = name + RString("_") + GetSystemTime();

  // Check to see if CNR is active, if it isnt try to start it
  if (!_hla.IsCNRSessionStarted())
    _hla.CNRInitRemoteLogger();

  if(_hla.IsCNRSessionStarted())
  {
    // if successfull, connection then start recording
    if(_hla.CNROpenSession(name.Data()) == CNRSUCCESS)
    {
      if(_hla.CNRStartRecording() == CNRSUCCESS)
      {
        _CNRSession.Init();
        _CNRSession._sessionName = name;   // record the session name we're saving under
        ::_time32(&_CNRSession._sessionTime);

        // Add CNRSession into the buffered array
        AARMessageCNRSession *msg = new AARMessageCNRSession;
        *msg = _CNRSession;
        AddMessageToArray(msg);

        DLogF("[CNRLOG] Successfully started recording:%s",name.Data());
      }
      else
        DLogF("[CNRLOG] Failed to start recording");
    }
    else
    {
#if _HLA
      // Dont display warning message, unless we're the HLA build
      if(Glob.vbsEnableCNR)
      {
        WarningMessage("Failed to connect to CNR Log");
        ResetErrors();
      }
#endif

      DLogF("[CNRLOG] Failed to open session:%s",name.Data());
    }
  }
}

/*
\VBS_patch_internal 1.00 Date [2/19/2008] by clion
- Fixed: When loading older AAR files, then recording new missions would cause version miss match
*/
void AAR::Record()
{
  DLogF("Starting to record");

  // Check to make sure that you cannot play and
  // also at the same time play
  if(IsLoaded())
  {
    WarningMessage("Unable to record playing AAR");
    ResetErrors();
    return;
  }

  // Do not record unless we're the server
  // and our simulation is running
  if(!GetNetworkManager().IsServer()) return;
  if(!_inSimulation) return;

  // Reset AAR get ready to record
  Init();

  // Open a temp file we will use for recording.
  RString fileName("LastMission");
  RString fullePath = CorrectPath(fileName);
  LSError error = OpenOutputFileStream(fullePath);
  
  if(error != LSOK)
  {
    // notify the user we, cannot create temp file
    WarningMessage("Cannot create recording file:%s",fullePath.Data());
    ResetErrors();
    return;
  }

  // Save the path we're saving to.
  _tmpFileAbsolutePath = fullePath;

  // Save the header and prepare for recording. If we 
  // dont have write access the writer will fail anyway.
  _version = CURRENT_AAR_VERSION;
  _stream->TransferBinary(_version);

  // Set the save state to be -2, to indicate we're now recording.
  // set our relative time to now for recording
  _saveState    = -2;
  _timeOffset   = Glob.time.toFloat();
  _record       = true;
  _inSimulation = true;

  // sends all current centers, groups, units, objects  
  SaveWorldInfo();
  StartCNRRecorder();
  RecordBriefingData();
  _hla.SendSnapshot();

  GetNetworkManager().AARDoUpdate(StartRecord,0,RString(""));
}

void AAR::RemoteRecordStop()
{
  _remoteRecord = false;
}

void AAR::RecordStop()
{
  // Remote record stays active until success
  if(!GetNetworkManager().IsServer()) return;

  _record = false;

  if(_hla.IsCNRSessionStarted())
  {
    bool isRecording = false;
    _hla.CNRIsRecording(isRecording);

    if(isRecording)
      if(_hla.CNRStopRecording() == CNRSUCCESS)
        DLogF("[CNRLOG] Successfully stoped recording");
  }


  GetNetworkManager().AARDoUpdate(StopRecord,0,RString(""));
}

void AAR::RemoteRecordSave(RString &path)
{ 
  // Should never be called
}

void AAR::RecordSave(RString &path)
{
  if(!GetNetworkManager().IsServer()) return;

  // Set state to save
  _saveState = -1;
  GetNetworkManager().AARDoUpdate(SaveState,_saveState,RString(""));

  if(_record)
  {
    // First attempt to save
    RecordStop();

    // Flush memory, clear all ques, and close output stream.
    FlushMessages();
    ClearAllQues();
    CloseOutputFileStream();
  }
  
  // Try moving the file to the new path!
  RString newPath = CorrectPath(path);
  int moveOp = MoveFile(_tmpFileAbsolutePath,newPath);
  if(moveOp != 0)
    _saveState = (int) LSOK;
  else
    _saveState = (int) LSFileNotFound;
 
  // update everyone on the status of saving
  _remoteRecord = false;
  GetNetworkManager().AARDoUpdate(SaveState,_saveState,RString(""));
}

int AAR::GetSaveState()
{
  return _saveState;
}

bool AAR::SafeToUnloadMessages()
{
  for( int i = 0; i < _messages.Size(); ++i )
    if(_messages[i]->IsRequested() && !_messages[i]->IsRequestDone())
      return false;
  
  return true;
}

void AAR::ClearAllQues()
{
  _messages.Clear();
  _appendMessages.Clear();
  _keyFrames.Clear();
  _keyFrames.Init();
  _loadedMessageIndex.Clear();
  _entityLifeSpan.Clear();
  _centerLifeSpan.Clear();
  _groupLifeSpan.Clear();
  _unitLifeSpan.Clear();
  _bookMarks.Clear();

  // Clear the fire events
  // Clear the death events
  for( int i = 0; i < TSideUnknown; ++i)
  {
    _fireEventsTimes[i].Clear();
    _deathTimes[i].Clear();
  }

  // Empty the session data
  _CNRSession._sessionName = RString();
  _CNRSession._sessionTime = 0;
  _cnrEvents.Clear();
}

void AAR::AddMessageToArray(AARMessageBase *msg)
{
  Assert(msg);
  Assert(IsRecording());

  DynamicLoadMessage *newMessage = new DynamicLoadMessage;
  newMessage->_message = msg;

  _messages.Add(newMessage);
}

void AAR::AddMessageToAppendArray(AARMessageBase *msg)
{
  Assert(msg);
  Assert(IsLoaded());

  _appendMessages.Add(msg);
}

/*
\VBS_patch_internal 1.00 Date [2/19/2008] by clion
- Fixed: Saving modified AAR file now uses the latest version, instead of the version
  it was originaly recorded with
*/

// Possible problem that can occure, IO disk full.
// so, we have to just keep everthing in memory instead
// and notify the user at the end of the mission
SerializeBinStream::ErrorCode AAR::FlushMessages()
{
  Assert(_fout);
  Assert(_stream);

  DLogF("[AAR] save %d messages", _messages.Size());

  while(_messages.Size()>0)
  {
    _messages[0]->_message->Serialize(*_stream);
    if(_stream->GetError() != SerializeBinStream::EOK)
    {
      DLogF("[AAR] Error: Message serialization failed!");
      return _stream->GetError();
    }
    
    // delete the message we've just writent to disk
    _messages.Delete(0);
  }

  return _stream->GetError();
}

LSError AAR::OpenOutputFileStream(RString &path)
{
  LSError error = LSOK;

  // open the output stream
  _fout = new QOFStream();
  _fout->open(path);
  error = _fout->error();
  if(error==LSOK)
    _stream = new SerializeBinStream(_fout);
  else
  {
    delete _fout; // clean up the file output
    _fout = NULL;
  }

  return error;
}

void AAR::CloseOutputFileStream()
{
  Assert(_stream);
  Assert(_fout);

  delete _stream;
  delete _fout;

  _fout = NULL;
  _stream = NULL;
}

void AAR::SetHitLineHistory(float amount)
{
  Assert(amount>=0.0f);
  _hitLineHistory=amount;
}

bool AAR::IsPlayPaused()     { return _pause; };

// When paused it is not playing
bool AAR::IsPlaying()        { return _play; };
bool AAR::IsPlayRepeat()     { return _repeat;};
bool AAR::IsRecording()      { return _record;};
bool AAR::IsRemoteRecording(){ return _remoteRecord; };

float AAR::GetDuration()
{
  if(_record || _remoteRecord)
    return  Glob.time.toFloat() - _timeOffset;

  return _duration;
}

float AAR::CurrentTime(){ return _currentTimePos; }

//! Sets the briefing data, normaly done at mission start
void AAR::RecordBriefingData()
{
  DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
  if (map && map->_briefing)
  { 
    AARBriefingData *msg = new AARBriefingData(map->_briefing->GetMissionBriefing(),0.0);
    AddMessageToArray(msg);
  }
}

const RString &AAR::GetBriefingData()
{
  return _briefingData;
}

void AAR::NextMessage()
{
  /*
  GlobalShowMessage(1000,"Message count:%d Message index:%d",MessageCount(),CurrentMessageIndex());
  Assert(_replayIndex < _messages.Size());
  Assert(_replayIndex >= 0); 

  _pause = true;

  Ref<AARMessageBase> msg = _messages[_replayIndex];
  _currentTimePos = msg->_time; // apply the message time.

  RespawnLifeTimes(true);
  RunEachMessage(msg,true);
  RespawnLifeTimes(false);

  _replayIndex++;
  */
}

void AAR::PreviousMessage()
{
  /*
  GlobalShowMessage(1000,"Message count:%d Message index:%d",MessageCount(),CurrentMessageIndex());
  Assert(_replayIndex < _messages.Size());
  Assert(_replayIndex > 0); 

  _pause = true;

  Ref<AARMessageBase> msg = _messages[_replayIndex];
  _currentTimePos = msg->_time; // apply the message time.

  RespawnLifeTimes(true);
  RunEachMessage(msg,false);
  RespawnLifeTimes(false);

  _replayIndex--;
  */
}

int AAR::MessageCount(){ return _messages.Size(); };
int AAR::CurrentMessageIndex(){ return _replayIndex; };

//  -2 is start recording
//  -1 is saving
//   0 is succeess
// > 0 is failure.
void AAR::SetSaveState(int state)
{
  _saveState = state;
}

void AAR::SetRemoteSaveState(int state)
{
  _saveState = state;
}


void AAR::RespawnLifeTimes(bool create)
{
  ReSpawnEntitys(create);
  ReSpawnGroups(create);
  ReSpawnUnits(create);
}


void AAR::MoveToTime(float pos)
{
  PROFILE_SCOPE_EX(AARMoveT,*);
  Assert(pos >= 0.0f && pos <= GetDuration());

  // Because of the large volume of data, pause the
  // simulation.
  if(!_pause)
    PlayPause();

  _hla.CNRStopPlaying();

  //! request the area we're about to walk to.
  float oldTimePos = _currentTimePos;
  RequestMessageLoad(pos);

  if( pos > _currentTimePos ) // move forward
  {
    for( int i = _replayIndex; i < _messages.Size(); i++ )
    {
      _replayIndex = i;
      if(pos > _messages[i]->_time)
      {
        // apply the message time, as the current time.
        _currentTimePos = _messages[i]->_time;       

        // run the message in the index!
        Ref<AARMessageBase> msg = _messages[i]->_message;
        if(msg)
          RunEachMessage(msg,false,true,true);    
      }
      else
        break;        
    }
    // we now apply the moveToThisTime to the actual current time
    // and spawn or delete the necceary lifetimes
    _currentTimePos = pos;

    // reset all the current audio that has been played
    // when moving time position
    for( int i = 0; i < _cnrEvents.Size(); ++i )
    {
      if(_cnrEvents[i].startTime < _currentTimePos)
        _cnrEvents[i].played = true;
    }
  }
  else if( pos <= _currentTimePos ) // move back
  {  
    for( int i = (_replayIndex-1); (i >= 0) && (i < _messages.Size()); --i )
    {
      if( _messages[i]->_time >= pos )
      {
        _replayIndex = i;
        
        // apply the message time, as the current time
        _currentTimePos = _messages[i]->_time;

        // run the message in the index!
        Ref<AARMessageBase> msg = _messages[i]->_message;
        if(msg)
          RunEachMessage(msg,false,false,true);
      }
      else
        break;       
    }
   
    // here we, delete need objects after moving to the position.
    _currentTimePos = pos;
    TimeShiftClearCraters();

    // reset all the current audio that has been played
    // when moving time position
    for( int i = 0; i < _cnrEvents.Size(); ++i )
    {
      if(_cnrEvents[i].startTime > _currentTimePos)
        _cnrEvents[i].played = false;
    }
  }

  // remove any un-needed objects.
  RespawnLifeTimes(false);
  RemoveOldDetonationBuffer();
 
  // we only, use keyframing when our seek time is huge.
  if(abs(_currentTimePos-oldTimePos) > _lookAheadTime)
  {  
    // Get the keyframe to play.
    int keyFrame = (int)_currentTimePos/(int)_keyFrames.GetKeyTime();
    if(keyFrame >= _keyFrames[0]._messages.Size())
      keyFrame = _keyFrames[0]._messages.Size()-1;

    // Play all keyframes for all the entity at this position.
    for( int i = 0; i < _keyFrames.Size(); ++i )
    {
      Ref<AAREntityUpdateData> msg = _keyFrames.Get(i)._messages[keyFrame];
      if(msg)
      {
        msg->PreProccess(0);
        if(msg->_passPP && msg->KeyFrameCanPlay(_currentTimePos))
          RunEachMessage(msg);
      }
    }
  }
}

void AAR::ProccessCNRLogEvent()
{
  if(_CNRSession._sessionName.GetLength()==0) return;

  // If we're not connected see if its avaliable.
  if(!_hla.IsCNRSessionStarted())
    _hla.CNRInitRemoteLogger();
  

  if(_hla.IsCNRSessionStarted())
  {
    // Successfully opened session
    if(_hla.CNROpenSession(_CNRSession._sessionName.Data()) == CNRSUCCESS)
    {
      int size = _hla.CNRGetEventSize();

      for( int i = 0; i < size; ++i)
      {
        CNRLogEvent event;
        ZeroMemory(&event,sizeof(CNRLogEvent));

        if(_hla.CNRGetEvent(i,event) == TRUE)
        {       
          // end time is Duration and that resolution is in seconds
          int index = _cnrEvents.Add();
          _cnrEvents[index].id        = event.id;       
          _cnrEvents[index].duration  = fabs(event.duration) / 1000;
          _cnrEvents[index].startTime = fabs(event.time-_CNRSession._sessionTime);
          _cnrEvents[index].freq      = event.freq;
          _cnrEvents[index].source    = RString(event.source);
          _cnrEvents[index].played    = false;

          DLogF
          (
           "[CNRLOG] Id:%d StartTime:%f Duration:%f Freq:%d",
            _cnrEvents[index].id,
            _cnrEvents[index].startTime,
            _cnrEvents[index].duration,
            _cnrEvents[index].freq
          );
        }
      }
    }
    else
    {
#if _HLA
      if(Glob.vbsEnableCNR)
      {
        WarningMessage("Failed to connect to CNR Log");
        ResetErrors();
      }
#endif
      DLogF("[CNRLOG] Failed to connect to hla");
    }
  }
}


#define CHECKERROR(X) X; if(_fin->eof()) { return AAR_FailedLoad; }

void AAR::SerializeMessage(SerializeBinStream &in,Ref<AARMessageBase> &msg)
{
  Ref<DynamicLoadMessage> dynamicMsg = new DynamicLoadMessage;
  dynamicMsg->_startPos = in.TellG();

  msg->Serialize(in);
  if(in.GetLoadStream()->eof())
    return;

  // allways use the last message time for recording duration
  _duration = msg->_time;

  dynamicMsg->_message= msg;
  dynamicMsg->_endPos = in.TellG();
  dynamicMsg->_type   = msg->TypeIs();        
  dynamicMsg->_time   = msg->_time;

  if(msg->TypeIs() == enumEntityUpdateData)
  {
    Ref<AAREntityUpdateData> entiyUpdate = static_cast<AAREntityUpdateData*>(msg.GetRef());
    _keyFrames.AddMessage(entiyUpdate->GetRawData().Id,entiyUpdate);

    dynamicMsg->_message.Free();
  }

  _messages.Add(dynamicMsg);

  return;
}

void AAR::RunEachMessage(AARMessageBase *msg, bool enableFireMsg,bool timeLineForward, bool moveToTime)
{
  Assert(msg);

  switch(msg->TypeIs())
  {
  case enumEntityUpdateData:
    {
      Assert(msg->TypeIs()==enumEntityUpdateData);

      AAREntityUpdateData *update = static_cast<AAREntityUpdateData*>(msg);
      Ref<AAREntityUpdateData> tmp = update;
      EntityUpdateData data = update->GetDataUpdateIndex(_replayIndex,timeLineForward);

      // for moveing in moveTo, we need better smoothing
      if(moveToTime)
      {
        EntityAI *obj = GetEntityAI(data.Id);
        if(obj)
          obj->OnAARMoveTime();
      }

      _ptr2UpdateEntity(data);
    }
    break;
  case enumFireWeaponData:
    {
      Assert(msg->TypeIs()==enumFireWeaponData);

      if(enableFireMsg)
      {
        AARFireWeaponData *fire = static_cast<AARFireWeaponData*>(msg);
        _ptr2FireWeapon(fire->GetData());
      }
    }
    break;
  case enumTurretUpdate:
    {
      Assert(msg->TypeIs()==enumTurretUpdate);

      AARTurretUpdate *turret = static_cast<AARTurretUpdate*>(msg);
      _ptr2TurretUpdate(turret->GetData());
    }
    break;
  case enumDoDamage:
    {
      Assert(msg->TypeIs()==enumDoDamage);

      AARDoDamageData *damage = static_cast<AARDoDamageData*>(msg);
      _ptr2DoDamage(damage->GetData());
    }
    break;
  case enumEntityDestroyed:
    {
      Assert(msg->TypeIs()==enumEntityDestroyed);

      AAREntityDestroyed *destroyed = static_cast<AAREntityDestroyed*>(msg);
      _ptr2VehicleDestroyed(destroyed->GetData());
    }
    break;
  case enumUnitUpdateData:
    {
      Assert(msg->TypeIs()==enumUnitUpdateData);

      AARUnitUpdateData *updateUnit = static_cast<AARUnitUpdateData*>(msg);
      _ptr2UpdateUnit(updateUnit->GetData());
    }
    break;
  case enumUpdateGroupData:
    {
      Assert(msg->TypeIs()==enumUpdateGroupData);

      AARUpdateGroupData *updateGroup = static_cast<AARUpdateGroupData*>(msg);
      _ptr2UpdateGroup(updateGroup->GetData());
    }
    break;
  case enumAttachToUpdate:
    {
      Assert(msg->TypeIs()==enumAttachToUpdate);

      AARAttachToUpdate *attachToUpdate = static_cast<AARAttachToUpdate*>(msg);

      if(timeLineForward)
      {
        _ptr2AttachTo(attachToUpdate->GetData());
      }
      else
      {
        int index = attachToUpdate->PreviousMessage();
        // run the last message
        if(index != -1)
        {
          AARAttachToUpdate *preMsg = static_cast<AARAttachToUpdate*>(_messages[index]->_message.GetRef());

          _ptr2AttachTo(preMsg->GetData());
        }
      }     
    }
    break;
  case enumWeatherUpdate:
    {
      Assert(msg->TypeIs()==enumWeatherUpdate);

      AARWeatherUpdate *updateWeather = static_cast<AARWeatherUpdate*>(msg);

      if(timeLineForward)
        updateWeather->RunMessage();
      else
      {
        int index = updateWeather->PreviousMessage();
        if(index != -1)
        {
          AARWeatherUpdate *weatherUpdateBefore = static_cast<AARWeatherUpdate*>(_messages[index]->_message.GetRef());
          weatherUpdateBefore->RunMessage();
        }
        else
          updateWeather->RunMessage();
      }
    }
    break;
  case enumCreateMarker:
    {
      Assert(msg->TypeIs()==enumCreateMarker);

      AARCreateMarker *createMarker = static_cast<AARCreateMarker*>(msg);
      if(timeLineForward)
      {
        createMarker->RunMessage();
      }
      else
      {
        int index = createMarker->PreviousMessage();
        if(index == -1)// first create message, we delete our marker now.
        {
          for (int i=0; i< markersMap.Size(); i++)
          {
            ArcadeMarkerInfo &mInfo = markersMap[i];
            if (stricmp(createMarker->_data.name,mInfo.name) == 0)
            {
              void ProcessDeleteMarkerEH(RString name);
              ProcessDeleteMarkerEH(mInfo.name);

              markersMap.Delete(i,1);// going back in time.
              break;
            }
          }
        }
        else
          createMarker->RunMessage();
      }
    }
    break;
  case enumDeleteMarker:
    {
      Assert(msg->TypeIs()==enumDeleteMarker);

      AARDeleteMarker *delMarker = static_cast<AARDeleteMarker*>(msg);
      if(timeLineForward)
      {
        delMarker->RunMessage();
      }
      else
      {
        int index = delMarker->PreviousMessage();
        if(index!=-1)
        {
          AARCreateMarker *creatMarker = static_cast<AARCreateMarker*>(_messages[index]->_message.GetRef());
          creatMarker->RunMessage();
        }
      }    
    }
    break;
  case enumBriefingData: // dont run this message
    Assert(msg->TypeIs()==enumBriefingData);
    break;
  case enumDetonationData:
    {
      Assert(msg->TypeIs()==enumDetonationData);

      if(enableFireMsg)
      {      
        AARDetonationData *detonate = static_cast<AARDetonationData*>(msg);
        DetonationData data = detonate->GetData();

        // Note: the source position should be resolved here not within the dentation drawing routine
        int index = _detonationBuffer.Add();
        _detonationBuffer[index].time = CurrentTime();
        _detonationBuffer[index].sPos = VZeroP;
        _detonationBuffer[index].tPos = Vector3(data.Pos.x,data.Pos.y,data.Pos.z);
        _detonationBuffer[index].FiringId = data.FiringId;
        _detonationBuffer[index].TargetId = data.TargetId;

        _ptr2DetonateMunition(data);
      }
    }
    break;
  case enumAnimationPhase:
    {
      Assert(msg->TypeIs()==enumAnimationPhase);

      AARAnimationPhase *animation = static_cast<AARAnimationPhase*>(msg);

      // When replaying, we want to restore to the previous animation state.
      if(timeLineForward)
      {
        animation->RunMessage();
      }
      else
      {
        int index = animation->PreviousMessage();
        if(index != -1)
        {
          AARAnimationPhase *oldAnimation = static_cast<AARAnimationPhase*>(msg);
          oldAnimation->RunMessage();
        }
      }     
    }
    break;
  case enumEntityLifeSpanIndex:
    {
      Assert(msg->TypeIs()==enumEntityLifeSpanIndex);
      AAREntityLifeSpanIndex *msgIndex = static_cast<AAREntityLifeSpanIndex*>(msg);
      ReSpawnEntity(true,_entityLifeSpan[msgIndex->_index].GetRef());
      ReSpawnEntity(false,_entityLifeSpan[msgIndex->_index].GetRef());
    }
    break;
  case enumUnitLifeSpanIndex:
    {
      Assert(msg->TypeIs()==enumUnitLifeSpanIndex);
      AARUnitLifeSpanIndex *msgIndex = static_cast<AARUnitLifeSpanIndex*>(msg);
      ReSpawnUnit(true,_unitLifeSpan[msgIndex->_index].GetRef());
      ReSpawnUnit(false,_unitLifeSpan[msgIndex->_index].GetRef());
    }
    break;
  case enumGroupLifeSpanIndex:
    {
      Assert(msg->TypeIs()==enumGroupLifeSpanIndex);
      AARGroupLifeSpanIndex *msgIndex = static_cast<AARGroupLifeSpanIndex*>(msg);
      ReSpawnGroup(true,_groupLifeSpan[msgIndex->_index].GetRef());
      ReSpawnGroup(false,_groupLifeSpan[msgIndex->_index].GetRef());
    }
    break;
  // these messages should no longer be in the message que
  case enumEntityCreateData:
  case enumUnitCreateData:
  case enumGroupCreateData:
  case enumCenterCreateData:
  case enumEntityDeleteData: 
  case enumUnitDeleteData:
  case enumCenterDeleteData:
  case enumGroupDeleteData:
    Assert(false);
  default:
    Assert(false);// No message for this type found!
    break;
  } 
  // proccess unit sastistics
  msg->ProccessStatistics(timeLineForward); 
}

int allMsgSize[TypeLimit] = {0};

class Info
{
  int _startSize;
  int _type;

public:
  Info(AARDataType index)
  {
    _type = index;
    _startSize = GetMemoryUsedSize();
  }
  ~Info()
  {
    allMsgSize[_type] += GetMemoryUsedSize() - _startSize;
  }
};



AARLoadError AAR::ProccessMessage(SerializeBinStream &in)
{
    if(in.IsSaving()) // this should never happen
      return AAR_EOF;

    AARMessageHeader header;
    in.Load(&header,sizeof(AARMessageHeader));

    // hit the end, end of file reached no error
    if(_fin->eof()) 
      return AAR_EOF;

    switch(header._mtype)
    {
      case enumCNRSessionName:
        {
          _CNRSession.Init();
          CHECKERROR(_CNRSession.Serialize(in));
        }
        break;
      case enumLoadWorld:
        {
          AARMessageWorldLoad world;
          LstF("Start:World loaded");
          CHECKERROR(world.Serialize(in));
          LstF("Loaded:World loaded");
        }
        break;
      case enumEntityLifeSpan:
        {
          LstF("Start:Life Span");
          CHECKERROR(in.TransferRefArray(_entityLifeSpan));
          LstF("Loaded:Life Span");
        }
        break;
      case enumCenterLifeSpan:
        {
          LstF("Start:Center Life Span");
          CHECKERROR(in.TransferRefArray(_centerLifeSpan));
          LstF("Loaded:Center Life Span");
        }
        break;
      case enumGroupLifeSpan:
        {
          LstF("Start:Group Life Span");
          CHECKERROR(in.TransferRefArray(_groupLifeSpan));
          LstF("Loaded:Group Life Span");
        }
        break;
      case enumUnitLifeSpan:
        {
          LstF("Start:Unit Life Span");
          CHECKERROR(in.TransferRefArray(_unitLifeSpan));
          LstF("Loaded:Unit Life Span");
        }
        break;
      case enumBookMarkLifeSpan:
        {
          LstF("Start: BookMarks");
          CHECKERROR(in.TransferRefArray(_bookMarks));
          LstF("Loaded: BookMarks");
        }
        break;
      case enumEntityCreateData: 
        {
          // (Do not add message into _messages)
          //  Conversion is done after loading
          Ref<AAREntityCreateData> msg = new AAREntityCreateData;
          CHECKERROR(msg->Serialize(in));
          
          // reconstructed object life span on the fly.
          Ref<ObjectLifeSpan> newObjectLifeSpan = new ObjectLifeSpan(msg->GetData(),enumEntityCreateData,msg->_time,0.0f);
          _entityLifeSpan.Add(newObjectLifeSpan);
        }
        break;
      case enumEntityDeleteData:
        {
          // (Do not add message into _messages)
          //  Conversion is done after loading
          Ref<AAREntityDeleteData> msg = new AAREntityDeleteData;
          CHECKERROR(msg->Serialize(in));
          
          // search for the entity in lifespan and set its death to this message time.
          for( int i = 0; i < _entityLifeSpan.Size(); ++i )
          {
            if(_entityLifeSpan[i]->CheckEntityId(msg->GetData()))
            {
              _entityLifeSpan[i]->_death = msg->_time; 
              break;
            }
          }
        }
        break;
      case enumUnitCreateData:
        {
          Ref<AARUnitCreateData> msg = new AARUnitCreateData; //see entity create!
          CHECKERROR(msg->Serialize(in));

          Ref<UnitLifeSpan> newUnitLifeSpan = new UnitLifeSpan(msg->GetData(),enumUnitCreateData,msg->_time,0.0f);
          _unitLifeSpan.Add(newUnitLifeSpan);
        }
        break;
      case enumUnitDeleteData:
        {
          Ref<AARUnitDeleteData> msg = new AARUnitDeleteData;
          CHECKERROR(msg->Serialize(in));

          for( int i = 0; i < _unitLifeSpan.Size(); ++i )
          {
            if(_unitLifeSpan[i]->CheckEntityId(msg->GetData()))
            {
              _unitLifeSpan[i]->_death = msg->_time;
              break;
            }
          }
        }
        break;
      case enumGroupCreateData:
        {
          Ref<AARGroupCreateData> msg = new AARGroupCreateData; //see entity create!
          CHECKERROR(msg->Serialize(in));

          Ref<GroupLifeSpan> newGroupLifeSpan = new GroupLifeSpan(msg->GetData(),enumGroupCreateData,msg->_time,0.0f);
          _groupLifeSpan.Add(newGroupLifeSpan);
        }
        break;
      case enumGroupDeleteData:
        {
          Ref<AARGroupDeleteData> msg = new AARGroupDeleteData;
          CHECKERROR(msg->Serialize(in));

          for( int i = 0; i < _groupLifeSpan.Size(); ++i )
          {
            if(_groupLifeSpan[i]->CheckEntityId(msg->GetData()))
            {
              _groupLifeSpan[i]->_death = msg->_time;
              break;
            }
          }
        }
        break;
      case enumCenterCreateData:
        {
          Ref<AARCenterCreateData> msg = new AARCenterCreateData; //see entity create!
          CHECKERROR(msg->Serialize(in));

          Ref<CenterLifeSpan> nobj = new CenterLifeSpan(msg->GetData(),enumCenterCreateData,msg->_time,0.0f);
          _centerLifeSpan.Add(nobj);
        }
        break;
      case enumCenterDeleteData:
        {
          Ref<AARCenterDeleteData> msg = new AARCenterDeleteData;
          CHECKERROR(msg->Serialize(in));

          for( int i = 0; i < _centerLifeSpan.Size(); ++i )
          {
            if(_centerLifeSpan[i]->CheckEntityId(msg->GetData()))
            {
              _centerLifeSpan[i]->_death = msg->_time;
              break;
            }
          }
        }
        break;
      case enumAddBookMark:
        {
          Ref<AARAddBookMark> msg = new AARAddBookMark;
          CHECKERROR(msg->Serialize(in));

          // Add the bookmark
          Ref<BookMarkLifeSpan> bookMark = new BookMarkLifeSpan(msg->_name,msg->_jumpTo,msg->_message,enumBookMarkLifeSpan);
          _bookMarks.Add(bookMark);
        }
        break;
      case enumDeleteBookMark:
        {
          Ref<AARDeleteBookMark> msg = new AARDeleteBookMark;
          CHECKERROR(msg->Serialize(in));

          // Remove any of the bookmarks in the list
          for( int i = 0; i < _bookMarks.Size(); ++i)
          {
            if(msg->_name == _bookMarks[i]->GetName())
            {
              _bookMarks.Delete(i);
              break;
            }
          }
        }
        break;
      case enumEntityUpdateData:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AAREntityUpdateData;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumFireWeaponData:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AARFireWeaponData;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumDetonationData:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AARDetonationData;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumDoDamage:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AARDoDamageData;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumEntityDestroyed:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AAREntityDestroyed;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumUnitUpdateData:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> previousMsg = new AARUnitUpdateData(true);
          Ref<AARMessageBase> newMsg      = new AARUnitUpdateData;
          
          int cPos = in.TellG();
          CHECKERROR(SerializeMessage(in,previousMsg));
          in.SeekG(cPos); // rewind it back to the location..
          CHECKERROR(SerializeMessage(in,newMsg));
        }
        break;
      case enumUpdateGroupData:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AARUpdateGroupData;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumTurretUpdate:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AARTurretUpdate;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumAttachToUpdate:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AARAttachToUpdate;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumCreateMarker:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AARCreateMarker;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumWeatherUpdate:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AARWeatherUpdate;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumAnimationPhase:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AARAnimationPhase;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumDeleteMarker:
        {
          Info info(header._mtype);
          Ref<AARMessageBase> msg = new AARDeleteMarker;
          CHECKERROR(SerializeMessage(in,msg));
        }
        break;
      case enumBriefingData:
        {
          Ref<AARBriefingData> msg = new AARBriefingData;
          Ref<AARMessageBase>  mbase = msg.GetRef();
          CHECKERROR(SerializeMessage(in,mbase));

          // set the briefing data
          _briefingData = msg->GetData();
        }
        break;
      default:
          Assert(false);// normaly means you didnt handel a message
          return AAR_FailedLoad;
        break;
    }

    return AAR_OK;
}

bool AAR::init(){ return true; }
bool AAR::init(const char *path){ return true; }
bool AAR::isInitialised(){ return true;}


#define DRAW_TEXT(xi,yi,attrText,format,arg1) \
{ \
  Point2DAbs pos; \
  GEngine->Convert(pos, Point2DFloat(xi, yi)); \
  float xs = toInt(GEngine->Width2D() * xso); saturateMax(xs, 1); \
  float ys = toInt(GEngine->Height2D()* yso); saturateMax(ys, 1); \
  GEngine->DrawTextF(Point2DAbs(pos.x+xs,pos.y+ys),attrShadow,format,arg1); \
  GEngine->DrawTextF(pos,attrText,format,arg1); \
}

DrawCoord WorldToScreen(Vector3Val pt) 
{
  float invSizeLand = 1.0 / (GLandscape->GetLandGrid() *  GLandscape->GetLandGrid());
  float xMap = pt.X() * invSizeLand;
  float yMap = (1.0 - pt.Z() * invSizeLand);
  return DrawCoord(xMap, yMap);
}

void AAR::Draw()
{
  if(!IsPlaying())
    return;

#if GUIDebug
  Font *font=GEngine->GetDebugFont();
  const float yso=0.001,xso=0.001;
  PackedColor color(Color(1,0.8,0.5,0.8));
  const float size = 0.02;
  TextDrawAttr attrShadow(size,font,PackedBlack,true);
  TextDrawAttr attrText(size,font,color,true);

  for( int i = 0; i < _entityLifeSpan.Size(); i++ )
  {
    EntityAI *obj = GetEntityAI(_entityLifeSpan[i]->_newId);
    Person *person = dyn_cast<Person>(obj);
    
    if(!person) // dont show non person
      continue;

    RString group("");
    if(obj && obj->GetGroup())
    {
      group = obj->GetGroup()->GetDebugName();
      if(obj->GetGroup()->Leader())
        if(obj->GetGroup()->Leader()->GetPerson()==person)
          group = group + RString(" leader");
    }

    Vector3 pPos = person->Position();   
    Camera *sCamera = GScene->GetCamera();

    if(sCamera)
    {
      // check position
      Matrix4Val camInvTransform = sCamera->GetInvTransform();
      //Vector3 dir = pPos - sCamera->Position();
      Vector3 pos = camInvTransform * pPos;
      if( pos.SquareSize() > Square(100) )
        continue;


      if (pos.Z() <= 0) continue;
      float invZ = 1.0 / pos.Z();
      float x = 0.5 * (1.0 + pos.X() * invZ * sCamera->InvLeft());
      float y = 0.5 * (1.0 - pos.Y() * invZ * sCamera->InvTop());

      GameVarSpace *vars = person->GetVars();
      RString URN;
      if (vars)
      {
        GameValue var;
        if (vars->VarGet("URN", var))
          URN = var.GetData()->GetString();
      }

      RString vehInName; 
      Person *person = dyn_cast<Person>(obj);
      if(person && person->Brain() && person->Brain()->GetVehicleIn())
        vehInName = person->Brain()->GetVehicleIn()->GetDebugName();

      // Draw information about vehicles and groups
      DRAW_TEXT(x,y+(size*0),attrText,"URN  :%s",URN.Data())
      DRAW_TEXT(x,y+(size*1),attrText,"ID   :%d",_entityLifeSpan[i]->_newId.EntityNumber )
      DRAW_TEXT(x,y+(size*2),attrText,"GROUP:%s",group.Data())
      DRAW_TEXT(x,y+(size*3),attrText,"VEHIN:%s",vehInName.Data())
    }
  }

  // FileName
  // FileSize
  // Tottal number of messages
  // tottal number of messages in memory
  // streamed messages in per-second
  // streamed messages out per-second
  // number of keyframes
  // current keyframe
  RString fileName = RString("FileName:") + _absolutePathLoadedFile;
  RString fileSize = ::Format("FileSize:%d",_fileSize);
  RString messages = ::Format("Total number of messages:%d",_messages.Size());

  // calculate how much memory being used.
  // and how many messages are loaded
  int loadedSize = 0;
  int loadedMsgs = _loadedMessageIndex.Size();
  int inReqBuffer= 0;
  int inComBuffer= 0;
  int outBuffer  = 0;
  for( int i = 0; i < _loadedMessageIndex.Size(); ++i )
  { 
    const Ref<DynamicLoadMessage> msg = _messages[_loadedMessageIndex[i]];
    if(msg->_message)
    {
      loadedSize += msg->_endPos - msg->_startPos;
      if(_loadedMessageIndex[i]>_replayIndex)
        ++inComBuffer;
    }

    if(_loadedMessageIndex[i]>_replayIndex)
      ++inReqBuffer;
    else
      --outBuffer;
  }
  
  RString msgInMemory = ::Format("Total number of messages in memory:%d",loadedMsgs);
  RString msgSizeInMem= ::Format("Total size of messages in mem KB:%d",loadedSize/1024);
  RString streamIn    = ::Format("Streamed in msg per second:%d",_msgStreamInPS);
  RString streamOut   = ::Format("Streamed out msg per second:%d",_msgStreamOutPS);
  RString numKeyfram  = ::Format("Number of keyframes:%d",_keyFrames.Size() > 0 ? _keyFrames[0]._messages.Size() : 0 );

  int keyFrame = (int)_currentTimePos/(int)_keyFrames.GetKeyTime();
  RString keyFrameNum = ::Format("Current keyframe:%d", keyFrame );
  RString msgReqBuffer = ::Format("Buffer in request  msgs:%d",inReqBuffer);
  RString msgComBuffer = ::Format("Buffer in complete msgs:%d",inComBuffer);
  RString msgOutBuffer= ::Format("Buffer unloading msgs:%d",outBuffer);

  // New stats
  static const float x = 0.1;
  static const float y = 0.1;
  int counter = 0;

  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",fileName.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",fileSize.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",messages.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",msgInMemory.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",msgSizeInMem.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",streamIn.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",streamOut.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",msgReqBuffer.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",msgComBuffer.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",msgOutBuffer.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",numKeyfram.Data())
  DRAW_TEXT(x,y+(size*counter++),attrText,"%s",keyFrameNum.Data())

#endif

  //draw all the detonation data
  if(GVBSVisuals.GetDrawHitLine())
  {
    for(int i = 0; i < _detonationBuffer.Size();i++)
    {
      Detonation &detonation = _detonationBuffer[i];

      // Detected somebody got hit!
      if(detonation.TargetId.EntityNumber>0)
      {
        EntityAI *obj = GetEntityAI(detonation.FiringId);
        EntityAIFull *shoter = dyn_cast<EntityAIFull>(obj);

        if(!shoter)
          continue;

        // use the primary turret
        TurretContext context;
        shoter->GetPrimaryGunnerTurret(context);

        if(detonation.sPos.X()==0.0)
          detonation.sPos = shoter->PositionModelToWorld(shoter->GetWeaponPoint(context, context._weapons->ValidatedCurrentWeapon()));

        PackedColor color = _hitLine[shoter->GetTargetSideAsIfAlive()]._color;
        GEngine->DrawLine3D(-1, detonation.sPos,detonation.tPos,color,2,0);
      }
    }
  }
}

void AAR::RemoveOldDetonationBuffer()
{
  float startTime = _currentTimePos - _hitLineHistory;

  for(int i=0;i<_detonationBuffer.Size();i++)
  {
    if((_detonationBuffer[i].time < startTime)||
       (_detonationBuffer[i].time > _currentTimePos))
    {
      _detonationBuffer.Delete(i);
      i--;
    }
  }
}

void AAR::TimeShiftClearCraters()
{
  OLinkArray(Entity) objArray;

  LandScapeRemoveAnimals remove(objArray);
  GWorld->ForEachAnimal(remove);

  for( int i = 0; i < objArray.Size(); i++)
  {
    Entity *obj = objArray[i];
    if( _currentTimePos < obj->GetStartTime() )
      GWorld->DeleteAnimal(obj);
  }
}

void AAR::ClearExplosions()
{
  OLinkArray(Entity) objArray;
  LandScapeRemoveAnimals remove(objArray);
  GWorld->ForEachAnimal(remove);
  for( int i = 0; i < objArray.Size(); i++)
  {
    Entity *obj = objArray[i];
    GWorld->DeleteAnimal(obj);
  }
}

void AAR::SpawnAllCenters()
{
  if(!_ptr2CenterCreate)
    return;

  for(int i=0;i<_centerLifeSpan.Size();i++)
  {
    _centerLifeSpan[i]->_newId = _ptr2CenterCreate(_centerLifeSpan[i]->_data);
    DLogF("[AAR] spawn Center: old (%d/%d) new (%d/%d)"
      , _centerLifeSpan[i]->_data.Id.ApplicationNumber
      , _centerLifeSpan[i]->_data.Id.EntityNumber
      , _centerLifeSpan[i]->_newId.ApplicationNumber
      , _centerLifeSpan[i]->_newId.EntityNumber
      );
  }
  _centersSpawned = true;
}

/*!
\patch Date 02/07/2007 by Chad
- Fixed: Large AAR missions would delay loading, causing game to crash
*/
void AAR::ReSpawnEntitys(bool create)
{
  GDebugger.PauseCheckingAlive();

  // create, delete objects that are no longer in scope
  for(int i=0;i<_entityLifeSpan.Size();i++)
    ReSpawnEntity(create,_entityLifeSpan[i].GetRef());

  GDebugger.ResumeCheckingAlive();
}

void AAR::ReSpawnUnits(bool create)
{
  // create, delete units that are no longer in scope
  for(int i=0;i<_unitLifeSpan.Size();i++)
    ReSpawnUnit(create,_unitLifeSpan[i].GetRef());
}

void AAR::ReSpawnGroups(bool create)
{
  // create, delete groups that are no longer in scope
  for(int i=0; i < _groupLifeSpan.Size();i++)
    ReSpawnGroup(create,_groupLifeSpan[i].GetRef());
}

void AAR::ReSpawnEntity(bool create,ObjectLifeSpan *lifespan )
{
  bool betweenLifeAndDeath  = (lifespan->_birth <= _currentTimePos) && (lifespan->_death >=  _currentTimePos);

  if(betweenLifeAndDeath)
  {
    if(lifespan->IsNewIdNull())
    {
      EntityId id = _ptr2CreateEntity(lifespan->_data);
      Assert(!IsNullEntityId(id));

      //update the id number
      lifespan->_newId = id;

      for( int x = 0; x < TSideUnknown; ++x)
        lifespan->_stats[x].Init();

      DLogF("[AAR] Creating object:%s Old:%d/%d New:%d/%d",
        lifespan->_data.EntityType,
        lifespan->_data.Id.ApplicationNumber,
        lifespan->_data.Id.EntityNumber,
        lifespan->_newId.ApplicationNumber,
        lifespan->_newId.EntityNumber );
    }
  }
  else
  {
    // dont delete units if we're in create mode
    if(create) 
      return;

    if(!lifespan->IsNewIdNull())
    {
      EntityId Id = lifespan->GetNewId();
      if(_ptr2DeleteEntity) _ptr2DeleteEntity(Id);

      DLogF("[AAR] Entity delete: %d/%d",
        lifespan->_newId.ApplicationNumber,
        lifespan->_newId.EntityNumber);

      //no need to remove Units entry as well, since the unit just links the lifeSpan id
      lifespan->_newId = NullEntityId;
    }
  }
}

void AAR::ReSpawnGroup(bool create,GroupLifeSpan *lifespan)
{
  bool betweenLifeAndDeath  = (lifespan->_birth <= _currentTimePos) && (lifespan->_death >=  _currentTimePos);

  // Old recordings have no group death time, because of old bug.
  // New recordigns do have death time.
  if(betweenLifeAndDeath)
  {
    if(lifespan->IsNewIdNull())
    {
      Assert(lifespan->_centerLifeSpanId > -1)

        EntityId nullId = {0,0};
      GroupCreateData data = lifespan->_data;

      // we have to update EntityIds to current ids
      if(lifespan->_centerLifeSpanId == -1)
      {
        DLogF("[AAR] CTD: No centerID in respawn groups");
        data.CenterId = nullId;
      }
      else
        data.CenterId = _centerLifeSpan[lifespan->_centerLifeSpanId]->GetNewId();

      // For simplicity, we dont have a leader for the group. The leader is selected
      // when a groupUpdate comes along. Btw, leaderId refers to the BrainId of a unitcreateData.
      // And to get the person you need to get the Unit, then the person the brain is connected to.
      data.LeaderId = nullId;

      // Save the new groupId
      lifespan->_newId = _ptr2GroupCreate(data);

      DLogF("[AAR] spawn group: old (%d/%d) new (%d/%d)"
        , lifespan->_data.Id.ApplicationNumber, lifespan->_data.Id.EntityNumber
        , lifespan->_newId.ApplicationNumber, lifespan->_newId.EntityNumber
        );
    }
  }
  else
  {
    // dont delete units if we're in create mode
    if(create) 
      return;

    if(!lifespan->IsNewIdNull())
    {
      EntityId Id = lifespan->GetNewId();

      DLogF("[AAR] Delete Group:%d", lifespan->_newId.EntityNumber);
      if(_ptr2DeleteGroup) _ptr2DeleteGroup(Id);      

      lifespan->_newId = NullEntityId;
    }
  }
}

void AAR::ReSpawnUnit(bool create, UnitLifeSpan *lifespan)
{
  bool betweenLifeAndDeath  = ((lifespan->_birth <= _currentTimePos) && (lifespan->_death >= _currentTimePos));

  if(betweenLifeAndDeath)
  {
    if(lifespan->IsNewIdNull())
    {
      Assert(lifespan->_groupLifeSpanId > -1)
        Assert(lifespan->_entityLifeSpanId > -1)

        EntityId nullId = {0,0};

      UnitCreateData data = lifespan->_data;

      DLogF("[AAR] raw data spawn unit: old (%d/%d) new (%d/%d)"
        , lifespan->_data.Id.ApplicationNumber, lifespan->_data.Id.EntityNumber
        , lifespan->_newId.ApplicationNumber, lifespan->_newId.EntityNumber
        );

      // For simplicity, a unit brain does not have a group, this is updated 
      // in unitUpdateData.
      data.GroupId = nullId;

      // Person should allways be a 1-1 correspondence, should never chanage
      if(lifespan->_entityLifeSpanId == -1 )
      {
        DLogF("[AAR] CTD: No _entityLifeSpanID in respawn Unit");
        data.PersonId = nullId;
      }
      else
      {
        data.PersonId = _entityLifeSpan[lifespan->_entityLifeSpanId]->GetNewId();
      }

      // Store the new BrainId
      lifespan->_newId = _ptr2UnitCreate(data);

      DLogF("[AAR] new data spawn unit: old (%d/%d) new (%d/%d)"
        , lifespan->_data.Id.ApplicationNumber, lifespan->_data.Id.EntityNumber
        , lifespan->_newId.ApplicationNumber, lifespan->_newId.EntityNumber
        );

      DLogF("[AAR] group:%d/%d person:%d/%d", 
        data.GroupId.ApplicationNumber,
        data.GroupId.EntityNumber,
        data.PersonId.ApplicationNumber,
        data.PersonId.EntityNumber);     
    }
  }
  else
  {
    // Dont delete units if we're in create mode
    if(create)
      return;

    lifespan->_newId = NullEntityId;
  }
}


void AAR::RunMessages()
{
  float seekTo = _currentTimePos;
  for( int i=_replayIndex; i < _messages.Size();i++)
  {
    _replayIndex = i;
    Ref<DynamicLoadMessage> msg = _messages[i];
    if(seekTo > msg->_time)
    {
      // step through with exactly the same
      // time stamp as the message.
      _currentTimePos = msg->_time;
      
      if(msg->_message)
        RunEachMessage(msg->_message);
    }
    else
      break;
  }

  _currentTimePos =  seekTo;
}

void AAR::SetRepeat(bool repeat)
{
  _repeat = repeat;
};

// Forward dec, from world.cpp
bool IsAppFocused();

//////////////////////////////////////////////////////////////////////////
// Perform dynamicaly load messages
//////////////////////////////////////////////////////////////////////////
void AAR::RequestMessageLoad(float timePos)
{
  float maxTime = timePos + _lookAheadTime;
  float minTime = timePos - _lookAheadTime;

  saturate(maxTime,0.0,_duration);
  saturate(minTime,0.0,_duration);

  for( int i = 0; i < _loadedMessageIndex.Size();)
  { 
    float time = _messages[_loadedMessageIndex[i]]->_time;

    // we want to unload loaded message indexes that
    // are above or bellow min and max time load times
    if(time > maxTime || time < minTime)
    {
      // we only unload successfully loaded messages
      if(_messages[_loadedMessageIndex[i]]->_message)
      {
        _messages[_loadedMessageIndex[i]]->Unload();
        _loadedMessageIndex.Delete(i);

        // stats
        ++_msgOutCount;
      }
      else
        ++i;
    }
    else
      ++i;
  }

  int startIndex = _replayIndex;
  for( int i = _replayIndex; i >= 0; --i )
  {
    if(_messages[i]->_time < minTime)
      break;
    else
      startIndex = i;
  }

  Assert(startIndex >= 0);
  for( int i = startIndex; i < _messages.Size(); ++i )
  {
    // only messages that have not been loaded
    if(!_messages[i]->_message)
    {    
      float time = _messages[i]->_time;

      // Load the message into memory.
      if(!_messages[i]->IsRequested())
        if(time > minTime && time < maxTime )
        {
          //LogF("Requesting loading of message:%d",i);
          _messages[i]->RequestLoad();
          _loadedMessageIndex.Add(i);
        }
    }

    // reached above max time. break out of loop
    if(_messages[i]->_time > maxTime)
      break;
  }
}


int AAR::tick()
{
  PROFILE_SCOPE_EX(AARTick,*);

  // Calculate deltaT
  float deltaT = (GlobalTickCount()-_previousTime)/1000.0f;
  _previousTime = GlobalTickCount();

  if(_record)
  {
    // flush messages when we're recording.
    FlushMessages();

    return false;
  }

  if(!_play || !IsAppFocused()) return false;

  // max deltaT should be 0.200ms
  // thats 5fps.
  saturate(deltaT,0.0f,0.2f);

  if(_pause)
  {
    SetAllVelocity();
    return false;
  }

  //! Check if the playback has finished
  if((_currentTimePos + deltaT) >= GetDuration())
  {
    if(_repeat)
    {
      _currentTimePos = 0.0f;
      _replayIndex = 0;
      ClearExplosions();
    }
    else
    {
      PlayPause();
      return false;
    }
  }

  // Request messages to load at the current time pos
  RequestMessageLoad(_currentTimePos);

  // This is not a very fast way to acheive loading times.
  // but should suffice
  if(GlobalTickCount()-_lastKeyframe>1000)
  {
    _msgStreamInPS  = _msgInCount;
    _msgStreamOutPS = _msgOutCount;
    _msgInCount  = 0;
    _msgOutCount = 0;
    _lastKeyframe = GlobalTickCount();
  }
  
  // only during playback
  _currentTimePos += deltaT;
  RemoveOldDetonationBuffer();

  // scan the messages for packages that need to be updated
  if(_enableCallbacks)
  {
    if(!_centersSpawned)
      SpawnAllCenters();

    
    if(_hla.IsCNRSessionStarted())
    {
      for( int i = 0; i < _cnrEvents.Size(); ++i)
      {
        if(_cnrEvents[i].startTime < _currentTimePos && 
           _cnrEvents[i].played == false )
        {
          _cnrEvents[i].played = true;

          // play that single recording
          _hla.CNRPlayOneRecord(i);
          break;
        }
      }
    }

    RunMessages();
  }
  else
    DLogF("[AAR] callbacks disabled!");

  return false;
};

int AAR::GetLifeSpanSize(){ return _entityLifeSpan.Size();};

// Check the existance in _entityLifeSpan if this entity id existed
bool AAR::CheckEntityId(EntityId id,int &index)
{
  for( int i = 0; i < _entityLifeSpan.Size(); i++ )
  {
      if(_entityLifeSpan[i]->CheckEntityId(id))
      {
        index = i;
        return true;
      }
  }

  return false;
}

bool AAR::CheckEntityId(EntityId id)
{
  int index;
  return CheckEntityId(id,index);
}


void AAR::shutdown(){};

bool AAR::isStopped(){ return false; };

/*!
 @pre 
 Data.Id 
  is not null, is entity and is non
  static entity. 
 Data.VehicleId
  Can be null, if not null is entity
  and is non static 
*/
void AAR::entityCreated(EntityCreateData data)
{
  if(!IsRecording())  
    return;

  if(IsNullEntityId(data.Id)) return;

  Verify(!IsNullEntityId(data.Id));
  Verify(IsEntity(data.Id));
  Verify(IsObjectIdAObject(data.Id));

  if(!IsNullEntityId(data.VehicleId))
  {
    Verify(IsEntity(data.VehicleId));
    Verify(IsObjectIdAObject(data.VehicleId));   
  }

  DLogF("[AAR] Entity created msg called: Type:%s Name:%s Side:%d Id:%d/%d Vehicle:%d/%d CrewIndex:%d", 
    data.EntityType,
    data.Name, 
    data.Side,
    data.Id.ApplicationNumber, data.Id.EntityNumber,
    data.VehicleId.ApplicationNumber,
    data.VehicleId.EntityNumber,
    data.CrewIndex );

  // Still need to maintain a database of objects, for deletion purposes
  Ref<ObjectLifeSpan> nobj = new ObjectLifeSpan(data,enumEntityCreateData,GetRecordTimeOffset(),0.0f);
  _entityLifeSpan.Add(nobj);

  // New system is to still save the entityCreateData message, but reconstruct the entitylifespan
  // when serializing the file back, incase of ctd during server operation
  Ref<AAREntityCreateData> msg = new AAREntityCreateData(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
};

// Need explanation
void AAR::entityDeleted(EntityDeleteData id)
{
  if(!IsRecording())    return;

  DLogF("[AAR] Entity deleted msg called");
  Assert(!IsNullEntityId(id));
  
  bool wasDeleted = false;
  int index = 0;

  if(CheckEntityId(id,index))
  {
      DLogF("[AAR] entDelmsg:%d/%d time:%f",id.ApplicationNumber,id.EntityNumber,GetRecordTimeOffset());
      _entityLifeSpan[index]->_death = GetRecordTimeOffset();      

      Ref<AAREntityDeleteData> msg = new AAREntityDeleteData(id,GetRecordTimeOffset());
      AddMessageToArray(msg);
  }
  else
  {
    for(int i = 0; i < _unitLifeSpan.Size(); i++)
    {
      if( _unitLifeSpan[i]->_data.Id.ApplicationNumber == id.ApplicationNumber &&
         _unitLifeSpan[i]->_data.Id.EntityNumber == id.EntityNumber )
      {
        _unitLifeSpan[i]->_death = GetRecordTimeOffset();

        // Add the entity delete data
        Ref<AARUnitDeleteData> msg = new AARUnitDeleteData(id,GetRecordTimeOffset());
        AddMessageToArray(msg);

        break;
      }
    }

    for( int i = 0; i < _groupLifeSpan.Size(); ++i )
    {
      if( _groupLifeSpan[i]->_data.Id.ApplicationNumber == id.ApplicationNumber &&
        _groupLifeSpan[i]->_data.Id.EntityNumber == id.EntityNumber )
      {
        Assert(!wasDeleted);
        _groupLifeSpan[i]->_death = GetRecordTimeOffset();

        // Add the group delete data
        Ref<AARGroupDeleteData> msg = new AARGroupDeleteData(id,GetRecordTimeOffset());
        AddMessageToArray(msg);

        wasDeleted = true;
        break;
      }
    }

    for( int i = 0; i < _centerLifeSpan.Size(); ++i)
    {
      if(_centerLifeSpan[i]->_data.Id.ApplicationNumber == id.ApplicationNumber &&
        _centerLifeSpan[i]->_data.Id.EntityNumber == id.EntityNumber )
      {
        Assert(!wasDeleted);
        _centerLifeSpan[i]->_death = GetRecordTimeOffset();

        // Add the center delete data.
        Ref<AARCenterDeleteData> msg = new AARCenterDeleteData(id,GetRecordTimeOffset());
        AddMessageToArray(msg);

        wasDeleted = true;
        break;
      }
    } 
  }

  return;
};

EntityAI *AAR::GetEntityAI(EntityId id)
{
  return _hla.GetEntityAI(id);
}

// Forward declarations to hla interface
inline void CpyEntity(EntityId &dest, const NetworkObject *src);
inline void CpyEntity(Entity *dest, const EntityId &src);

inline void CpyCoord(Coordinate3DType &dest, const Vector3 &src)
{
  dest.x = src.X(); dest.y = src.Y(); dest.z = src.Z();
}
inline void CpyCoord(Vector3 &dest, Coordinate3DType &src)
{
  dest[0] = src.x; dest[1] = src.y; dest[2] = src.z;
}


//! Has Been validated and works correctly!
void AAR::entityUpdated(EntityUpdateData data)
{
  if(!IsRecording())              return;  
  if(!IsObjectIdAObject(data.Id)) return; 

  if(!IsNullEntityId(data.VehicleId))
  {
    Assert(IsEntity(data.VehicleId));
    Assert(IsObjectIdAObject(data.VehicleId));
  }

  // check if entity exsists in the lifespan, if it doesnt get the 
  // necceary information and call entityCreated instead of calling
  // a loopback to hla!
  int  index = 0;
  bool found = CheckEntityId(data.Id,index);

  if(!found)
  {
    NetworkId nwID = NetworkId(data.Id.ApplicationNumber,data.Id.EntityNumber);
    Entity *veh = _hla.GetEntity(_hla.GetNetObjFromHLAId(data.Id));
    
    // entityUpdate deals with only entitys. If we get something that
    // is not an entity then this is a serious bug
    Assert(veh);

    RString typeName = "";
    const EntityType *etype = veh->Type();
    if(etype)
      typeName = etype->GetName();

    EntityCreateData data;
    CpyEntity(data.VehicleId, (Object*)NULL);
    CpyEntity(data.Id, veh);
    CpyCoord(data.Pos, veh->WorldPosition(veh->FutureVisualState()));

    data.EPos = Pos_None;    
    data.Side = UnitSide(veh->GetTargetSide());
    data.Dimensions.x = 1.0f;
    data.Dimensions.y = 1.0f;
    data.Dimensions.z = 1.0f;

    data.Name[0] = 0;
    if(typeName.GetLength())
      strcpy(data.Name, cc_cast(typeName));

    const Person* person = dyn_cast<const Person>(veh);
    if(person)
      _hla.FillCrewInfo(person, data.VehicleId, data.CrewIndex, data.EPos);

    strcpy(data.EntityType,(const char*)typeName);

    LODShapeWithShadow * shape = etype->GetShape();
    if(shape)
    {
      Vector3 bounding = shape->Max() - shape->Min();
      CpyCoord(data.Dimensions, bounding);
    }

    entityCreated(data);
  }

  found = CheckEntityId(data.Id,index);
  
  //! This should never fire off, if it does then there is
  //! A major problem.
  Assert(found); 

  //! Because of a bug within RTE, we ignore updates from entitys that have allready
  //! Been deleted
  if(_entityLifeSpan[index]->_death != 0.0f)
  {
    DLogF("[AAR] Ignoring update message, unit allready been deleted");
    return;
  }


  // To reduce recording file size, we check to see if this update is really required.
  // we compare this message to its previous message
  AAREntityUpdateData *msg = new AAREntityUpdateData(data,GetRecordTimeOffset());
  if(_entityLifeSpan[index]->_lastmsg)
  {
    if(*_entityLifeSpan[index]->_lastmsg == *msg)
    {
      // Deconstruct message, its identical
      {
        Ref<AAREntityUpdateData> dref = msg;
      }
      return;
    }
  }

  _entityLifeSpan[index]->_lastmsg = msg;
  AddMessageToArray(msg);
};

// Forward declarations
RString GetUserDirectory();
void CreatePath(RString path);

RString AAR::GetSystemTime()
{
  SYSTEMTIME sysTime;
  GetLocalTime(&sysTime);

  char systemTime[MAX_PATH];
  sprintf(systemTime,"%d_%d_%d_%d_%d",sysTime.wYear,sysTime.wMonth,sysTime.wDay,sysTime.wHour,sysTime.wMinute);

  return RString(systemTime);
}

RString AAR::CorrectPath(RString &relorabs)
{
  RString name = relorabs;

  bool absolute = false;
  for( int i = 0; i < name.GetLength(); i++ )
    if( name[i] == '\\' )
    {
      absolute = true;
      break;
    }

    RString path;
    if(absolute)
      path = name;
    else
    {
      char buffer[MAX_PATH];
      RString Directory = GetUserDirectory() + RString("AAR\\");
      RString date = GetSystemTime();
      RString worldName = Glob.header.worldname;

      CreatePath(Directory);
      GetFilename(buffer,worldName);

      path = Directory + date + RString(".") + name + RString(".") + RString(buffer) + RString(".aar");
    }

    return path;
}

void AAR::simulationStateChanged(SimStateData data)
{
  if(data == SSimulation)
  {
    InitFields();
    _inSimulation = true;
  }

  if((data != SSimulation) && _inSimulation )
  {
    if(IsLoaded())
      UnLoadFile();
    if(_record)
      RecordSave(RString("LastMission"));

    _inSimulation = false;

    // check on exit to make sure gui is being simulated
    if(GWorld->IsPaused())
       GWorld->PauseSimulation(false);

    // once finished clear the bookmarks
    _bookMarks.Clear();
  }

  DLogF("[AAR] Simulation state chanaged");
};


void AAR::weaponFired(FireWeaponData data)
{
  if(!IsRecording())               
    return;

  // Verify that the person shotting is
  // infact an entity!
  Verify(!IsNullEntityId(data.FiringId)); // Allways require a person who shot the bullet
  Verify(IsEntity(data.FiringId));        // We expect to be shot by a entity.
  Verify(CheckEntityId(data.FiringId));   // Check if entity exsist in life span
  Verify(IsObjectIdAObject(data.FiringId));//Cannot be static object.

  if(!IsNullEntityId(data.TargetId))
  {
    if(IsEntity(data.TargetId))
    {
      if(!IsObjectIdAObject(data.TargetId))
        data.TargetId = NullEntityId; // we want to see bullets hitting buildings
      else
        Verify(CheckEntityId(data.TargetId));        
    }
    else
      Assert(false);
  }

  DLogF("[AAR] fireMsg: FiringId:%d TargetId:%d MunitionId:%d WeaponId:%d Type:%s",
    data.FiringId.EntityNumber,
    data.TargetId.EntityNumber,
    data.MunitionId.EntityNumber,
    data.WeaponId,
    data.MunitionType);

  AARFireWeaponData *msg = new AARFireWeaponData(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
};


void AAR::munitionDetonation(DetonationData data)
{
  if(!IsRecording()) return;

  if(!IsNullEntityId(data.FiringId))
  {
    if(IsEntity(data.FiringId))
    {
      if(!IsObjectIdAObject(data.FiringId))
        data.FiringId = NullEntityId; // we want to see bullets hitting buildings
      else
        Verify(CheckEntityId(data.FiringId));        
    }
    else
      Assert(false);
  }


  if(!IsNullEntityId(data.TargetId))
  {
    if(IsEntity(data.TargetId))
    {
      if(!IsObjectIdAObject(data.TargetId))
        data.TargetId = NullEntityId; // we want to see bullets hitting buildings
      else
        Verify(CheckEntityId(data.TargetId));        
    }
    else
      Assert(false);
  }

  DLogF("[AAR] munition detonation msg called");
  AARDetonationData *msg = new AARDetonationData(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
}

void AAR::CenterCreated(CenterCreateData data)
{
  if(!IsRecording()) return;
  Assert(!IsNullEntityId(data.Id));

  DLogF("[AAR] Center created msg called: Id:%d/%d Side:%d", 
    data.Id.ApplicationNumber,
    data.Id.EntityNumber,
    (int) data.Side );

  // Maintain the lifespan array.
  Ref<CenterLifeSpan> nobj = new CenterLifeSpan(data,enumCenterCreateData,GetRecordTimeOffset(),0.0f);
  _centerLifeSpan.Add(nobj);

  AARCenterCreateData *msg = new AARCenterCreateData(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
};

void AAR::GroupCreated(GroupCreateData data)
{
  if(!IsRecording()) return;
  Assert(!IsNullEntityId(data.Id));

  DLogF("[AAR] Group created msg called Id: %d/%d CenterId:%d/%d Num:%d LeaderId:%d/%d",
    data.Id.ApplicationNumber,
    data.Id.EntityNumber,
    data.CenterId.ApplicationNumber,
    data.CenterId.EntityNumber,
    data.Number,
    data.LeaderId.ApplicationNumber,
    data.LeaderId.EntityNumber );

  // Maintain the lifespan array.
  Ref<GroupLifeSpan> nobj = new GroupLifeSpan(data,enumGroupCreateData,GetRecordTimeOffset(),0.0f);
  _groupLifeSpan.Add(nobj);

  AARGroupCreateData *msg = new AARGroupCreateData(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
};

// Need fixing, we have no idea the correctness of this recording corectly!
void AAR::UnitCreated(UnitCreateData data)
{
  if(!IsRecording()) return;
  Assert(!IsNullEntityId(data.Id));
  Assert(!IsNullEntityId(data.PersonId));
  Assert(IsEntity(data.PersonId));
  Assert(CheckEntityId(data.PersonId)); // person need to exsist in lifespan!

  DLogF("[AAR] UnitCreated msg called: Id:%d/%d PersonId:%d/%d GroupId:%d/%d Num:%d",
    data.Id.ApplicationNumber,
    data.Id.EntityNumber,
    data.PersonId.ApplicationNumber,
    data.PersonId.EntityNumber,
    data.GroupId.ApplicationNumber,
    data.GroupId.EntityNumber,
    data.Number);

  // Maintain the lifespan array.
  Ref<UnitLifeSpan> nobj = new UnitLifeSpan(data,enumUnitCreateData,GetRecordTimeOffset(),0.0f);
  _unitLifeSpan.Add(nobj);

  AARUnitCreateData *msg = new AARUnitCreateData(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
};

/**
Problem here with UpdateUnit. The problem is that, the engine itself may
recieve messages in mixed order. So may recieve an update for the unit well
the unit doesnt exsist yet in AAR eyes, until we recieve an createUnit message.

Going to accept the data right here, and filter it out in the postproccessing stage
nothing else can really be done at this stage, because of pre-recorded aar files.
*/
void AAR::UpdateUnit(UnitUpdateData data)
{
  if(!IsRecording()) return;
  Assert(!IsNullEntityId(data.Id));
  
#if _VBS2_AAR_ASSERT
  FindEntityID findId(data.Id);
  GAAR._unitLifeSpan.ForEachF(findId);
  Assert(findId.GetResult() != -1);
#endif

  Assert(!IsNullEntityId(data.PersonId));
  Assert(IsEntity(data.PersonId));
  Assert(CheckEntityId(data.PersonId)); // person need to exsist in lifespan!

  DLogF("[AAR] UpdateUnitCreateData msg called: Id:%d/%d PersonId:%d/%d GroupId:%d/%d Num:%d",
    data.Id.ApplicationNumber,
    data.Id.EntityNumber,
    data.PersonId.ApplicationNumber,
    data.PersonId.EntityNumber,
    data.GroupId.ApplicationNumber,
    data.GroupId.EntityNumber,
    data.Number);

  AARUnitUpdateData *msg = new AARUnitUpdateData(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
}

 void AAR::AttachToUpdate(AttachToData data)
 {
  if(!IsRecording()) return;
  Assert(!IsNullEntityId(data.obj));

  DLogF("[AAR] AttachToUpdate msg called: Id:%d/%d AttachTo:%d/%d memoryPoint:%d pos:%f %f %f flat:%d",
      data.obj.ApplicationNumber,
      data.obj.EntityNumber,
      data.attachTo.ApplicationNumber,
      data.attachTo.EntityNumber,
      data.memoryIndex,
      data.com.x,
      data.com.y,
      data.com.z,
      data.flags);

    
  AARAttachToUpdate *msg = new AARAttachToUpdate(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
 }

 void AAR::UpdateTurret(TurretUpdate data)
 {
   if(!IsRecording()) return;
   Assert(!IsNullEntityId(data.Id));

   AARTurretUpdate *msg = new AARTurretUpdate(data,GetRecordTimeOffset());
   AddMessageToArray(msg);
 }


void AAR::CreateMarker(ArcadeMarkerInfo *marker)
{
  if(!IsRecording()) return;
    
  AARCreateMarker *msg = new AARCreateMarker(marker,GetRecordTimeOffset());
  AddMessageToArray(msg);
}

void AAR::DeleteMarker(const RString &name)
{
  if(!IsRecording()) return;

  AARDeleteMarker *msg = new AARDeleteMarker(name,GetRecordTimeOffset());
  AddMessageToArray(msg);
}

void AAR::WeatherUpdate(ApplyWeatherMessage *weather)
{
  if(!IsRecording()) return;

  AARWeatherUpdate *msg = new AARWeatherUpdate(weather,GetRecordTimeOffset());
  AddMessageToArray(msg);
}

void AAR::RecordAnimationPhase(const EntityAnimationPhase &data)
{
  if(!IsRecording()) return;

  AARAnimationPhase *msg = new AARAnimationPhase(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
}


/**
  Problem here with UpdateGroup. The problem is that, the engine itself may
  recieve messages in mixed order. So may recieve an update for the group well
  the group doesnt exsist yet in AAR eyes, until we recieve an createGroup message.

  Going to accept the data right here, and filter it out in the postproccessing stage
  nothing else can really be done at this stage, because of pre-recorded aar files.
 */
void AAR::UpdateGroup(UpdateGroupData data)
{
  if(!IsRecording()) return;
  Assert(!IsNullEntityId(data.Id));

#if _VBS2_AAR_ASSERT
  FindEntityID findId(data.Id);
  GAAR._groupLifeSpan.ForEachF(findId);
  Assert(findId.GetResult() != -1);
#endif


  DLogF("[AAR] UpdateGroupData msg called: Id:%d/%d LeaderId:%d/%d CenterId:%d/%d Num:%d",
    data.Id.ApplicationNumber,
    data.Id.EntityNumber,
    data.LeaderId.ApplicationNumber,
    data.LeaderId.EntityNumber,
    data.CenterId.ApplicationNumber,
    data.CenterId.EntityNumber,
    data.Number); 

  AARUpdateGroupData *msg = new AARUpdateGroupData(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
}

// Need fixing, we have no idea the correctness of this recording corectly!
void AAR::VehicleDestroyed(EntityDestroyed data)
{
  if(!IsRecording())             return;
  if(!ValidateData(data.Killer)) return;
  if(!ValidateData(data.Killed)) return; // you damaged nothing?
  
  DLogF("[AAR] VehicleDestoryed msg called Killed:%d/%d Killer:%d/%d",
    data.Killed.ApplicationNumber,
    data.Killed.EntityNumber,
    data.Killer.ApplicationNumber,
    data.Killer.EntityNumber);

  AAREntityDestroyed *msg = new AAREntityDestroyed(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
}

// Need fixing, we have no idea the correctness of this recording corectly!
void AAR::DoDamage(DoDamageData data)
{
  if(!IsRecording())             return;
  if(!ValidateData(data.Killer)) return;
  if(!ValidateData(data.Who))    return; // you damaged nothing?

  DLogF("[AAR] DoDamage msg called TargetId:%d Killer:%d Damage:%f Type:%s",
    data.Who.EntityNumber,
    data.Killer.EntityNumber,
    data.DamageAmount,
    data.Ammo);

  AARDoDamageData *msg = new AARDoDamageData(data,GetRecordTimeOffset());
  AddMessageToArray(msg);
}

void AAR::setCreateEntity(EntityId (*fPtr)(EntityCreateData data))
{
  Assert(fPtr);
  _ptr2CreateEntity = fPtr;
}

void AAR::setTurretUpdate(void (*fPtr)(TurretUpdate data))
{
  Assert(fPtr);
  _ptr2TurretUpdate = fPtr;
}

void AAR::setUpdateEntity(int (*fPtr)(EntityUpdateData data))
{
  Assert(fPtr);
  _ptr2UpdateEntity = fPtr;
}

void AAR::setDeleteEntity(void (*fPtr)(EntityDeleteData data))
{
  Assert(fPtr);
  _ptr2DeleteEntity = fPtr;
}

void AAR::setDeleteGroup(void (*fPtr)(GroupDeleteData data))
{
  Assert(fPtr);
  _ptr2DeleteGroup = fPtr;
}

void AAR::setFireWeapon(void (*fPtr)(FireWeaponData data))
{
  Assert(fPtr);
  _ptr2FireWeapon = fPtr;
}

void AAR::setDetonateMunition(void (*fPtr)(DetonationData data))
{
  Assert(fPtr);
  _ptr2DetonateMunition = fPtr;
}

void AAR::setCreateCenter(EntityId (*fPtr)(CenterCreateData data))
{
  Assert(fPtr);
  _ptr2CenterCreate = fPtr;

}

void AAR::setCreateGroup(EntityId (*fPtr)(GroupCreateData data))
{
  Assert(fPtr);
  _ptr2GroupCreate = fPtr;
}

void AAR::setCreateUnit(EntityId (*fPtr)(UnitCreateData data))
{
  Assert(fPtr);
  _ptr2UnitCreate = fPtr;
}

void AAR::setDoDamage(void (*fPtr)(DoDamageData data))
{
  Assert(fPtr);
  _ptr2DoDamage = fPtr;
}

void AAR::setVehicleDestroyed(void (*fPtr)(EntityDestroyed data))
{
  Assert(fPtr);
  _ptr2VehicleDestroyed = fPtr;
}

void AAR::setUpdateUnit(void (*fPtr)(UnitUpdateData data))
{
  Assert(fPtr);
  _ptr2UpdateUnit = fPtr;
}

void AAR::setUpdateGroup(void (*fPtr)(UpdateGroupData data))
{
  Assert(fPtr);
  _ptr2UpdateGroup = fPtr;
}

void AAR::setAttachToUpdate(void (*fPtr)(AttachToData data))
{
  Assert(fPtr);
  _ptr2AttachTo = fPtr;
}

void AAR::enableCallbacks(){_enableCallbacks = true;};
void AAR::disableCallbacks(){_enableCallbacks = false;};

#endif
