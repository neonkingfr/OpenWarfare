#ifndef __ExtraCallbacks___
#define __ExtraCallbacks___

#define LD_FUNCTION_CODE_MARKING 4

#if _AAR

#define AARDebug 0 && _ENABLE_CHEATS
#define GUIDebug 0 && _ENABLE_CHEATS
#define _VBS2_AAR_ASSERT 0 && _ENABLE_CHEATS

 struct UpdateGroupData
 {
   EntityId Id;                // this Id number used for identifications
   EntityId CenterId;          // Center Id that is created for each side!
   int Number;
   EntityId LeaderId;          // Is the UnitCreateData::Id for leader

 };

 struct AttachToData
 {
   EntityId obj;
   EntityId attachTo;         // if attachTo is null, then detach from the object
   Coordinate3DType com;
   int  memoryIndex;
   int flags;
 };

 struct UnitUpdateData
 {
   EntityId Id;                  // Id used for identification
   EntityId PersonId;            // Person representing the unit
   EntityId GroupId;             // The GroupCreateData::Id representing the unit
   int Number;
 };

 struct DoDamageData
 {
   EntityId Who;
   EntityId Killer;
   float    DamageAmount;
   char     Ammo[256];
 };

 struct EntityDestroyed
 {
   EntityId Killed;
   EntityId Killer;
 };

 struct EntityAnimationPhase
 {
    EntityId obj;
    RString  animation;
    float    phase;
 };

#endif

#endif