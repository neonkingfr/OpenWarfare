#include "../wpch.hpp"

#if _AAR
#include "AAR.hpp"
#include "AARTypes.hpp"
#include "AARDebug.hpp"
#include "HLA_base.hpp"

#include "../landscape.hpp"
#include "../world.hpp"

#include <Es/Common/win.h>
#include <El/QStream/qStream.hpp>
#include <El/QStream/serializeBin.hpp>
#include "../AI/ai.hpp"

#include "..\Network\networkImpl.hpp"

LifeSpan::LifeSpan()
{
  _newId = NullEntityId;
  _lastMsgIndex = 0;
  _dtype = TypeLimit;
  _birth = 0.0f;
  _death = 0.0f;
};

LifeSpan::LifeSpan(AARDataType dtype, float birth, float death)
{
  _dtype = dtype;
  _birth = birth; _death = death;

  _newId.EntityNumber = 0; _newId.ApplicationNumber = 0;
}

void LifeSpan::SerializeBin(SerializeBinStream &f)
{
  f.Transfer(_birth);
  f.Transfer(_death);
  f.TransferBinary(_dtype);
}

bool LifeSpan::CheckEntityNewId(EntityId id)
{
  const EntityId &newId = GetNewId();
  return (newId.EntityNumber == id.EntityNumber) && (newId.ApplicationNumber == id.ApplicationNumber);
}

bool LifeSpan::CheckEntityId(EntityId id) 
{
  const EntityId &ownId = GetDataId();
  return (ownId.EntityNumber == id.EntityNumber) && (ownId.ApplicationNumber == id.ApplicationNumber);
}

EntityId LifeSpan::GetNewId()
{
  // by this stage we should be perfectly fine to continue
  // we should of recreated the needed group/unit/entity
  Assert(!IsNewIdNull());
  return _newId;
}

EntityId UnitLifeSpan::GetNewId()
{
  if(IsNewIdNull())
  {
    GAAR.ReSpawnUnit(true,this);
    GAAR.ReSpawnUnit(false,this);
  }

  return LifeSpan::GetNewId();
}

EntityId GroupLifeSpan::GetNewId()
{
  if(IsNewIdNull())
  {
    GAAR.ReSpawnGroup(true,this);
    GAAR.ReSpawnGroup(false,this);
  }

  return LifeSpan::GetNewId();
}

EntityId ObjectLifeSpan::GetNewId()
{
  if(IsNewIdNull())
  {
    GAAR.ReSpawnEntity(true,this);
    GAAR.ReSpawnEntity(false,this);
  }

  return LifeSpan::GetNewId();
}



//////////////////////////////////////////////////////////////////////////
// Object LifeSpan
//////////////////////////////////////////////////////////////////////////
ObjectLifeSpan::ObjectLifeSpan(EntityCreateData &data, AARDataType dtype, float birth, float death)
               :LifeSpan(dtype, birth, death),_data(data)
{
  Assert(_data.Id.ApplicationNumber>=0);
  Assert(_data.Id.EntityNumber>=0);

  _simulation = RString("");
  _isInVeh = false;

  // When in recording mode, all objects passed by objectlifespan are valid network objects
  // if invalid objects are passed during multiplayer then there is nothing i can do about it
  // because this will never be called if an invalid addon is used and isnt on the server. No network
  // updates reach me, so all assumtion from this point on, is that a recording was done
  // but now the addon isn't avaliable on THIS comuter system thats trying to replay it. So all i can do
  // is prompt the user that the addon isnt present, and im using a generic addon instead.
  // Sorry, we encounted an error sir, here is the alternative or you can quit and get the addon
  if(_dtype==enumEntityCreateData)
  {
    EntityAI *entAi = GAAR.GetEntityAI(_data.Id);
    if(entAi)
    {
      const EntityAIType *type = entAi->GetType();
      if(type)
        _simulation = type->_simName;
    }
  }
}

/*
  Some issues come up here, regarding finding a replacement for the simulation
  firstly, lets take a helicopter. If we spawn a replacement for the helicopter
  that is perfectly fine, but when a person requests to move into a position 
  in the helicopter it will crash to desktop because that position is not avaliable. 
  Because of the different configuration of the helicopter and the positions avaliable.

  The only alternative is for every update, that interacts with that object is to disallow 
  movement into those position or usage of those guns or turrents because they're no longer avaliable.
  So this turns into a furball situation very quickly. You have to have very high error checking for allmost
  all possible conceviable data that is going into the system. 

  engine <-> hla <-> AAR < filter module > Old recording

  The easy solution would be not to load the AAR recording at all, because the addon is not present. 
  it would make the code cleaner. and less error prone if it went ahead.

 */
bool ObjectLifeSpan::PreProccess()
{
  Assert(_dtype==enumEntityCreateData);

  RString typeName(_data.EntityType);
  ConstParamEntryPtr findType =  (::Pars >> "CfgVehicles").FindEntry(typeName);
  if(findType) return true;

  findType = (::Pars >> "CfgAmmo").FindEntry(typeName);
  if(findType) return true;

  findType = (::Pars >> "CfgNonAIVehicles").FindEntry(typeName);
  if(findType) return true;

  // if the first char is a internal class then
  // return true, used for effects
  if(typeName.GetLength() >= 1 && typeName[0] == '#')
    return true;

  ErrorMessage(EMMissingData,"Addon:%s not found",typeName.Data());
  return false;

  /*
  RString simName = _simulation;
  ParamEntryVal clsDefault    =  ::Pars >> "DefaultSimulation";

  RString defaultAddon;
  switch(_data.Side)
  {
    case US_OpFor :
      defaultAddon = (*simNameptr) >> "East" >> "classEntry";
      break;
    case US_BlueFor :
      defaultAddon = (*simNameptr) >> "West" >> "classEntry";
      break;
    case US_Resistance :
      defaultAddon = (*simNameptr) >> "Guerrila" >> "classEntry";
      break;
    case US_NonCombatant :
      defaultAddon = (*simNameptr) >> "Civilian" >> "classEntry";
      break;
    default:
      Assert(false);// unknown side
      break;
  }

  if(defaultAddon.GetLength()==0)
    return;
  else
    ErrorMessage(EMMissingData,"Addon:%s not found,using core unit",typeName.Data());

  Assert(defaultAddon.GetLength()<255);
  strcpy(_data.EntityType,(const char*) defaultAddon);
  */
}

bool ObjectLifeSpan::IsSameEntityId(const Entity *obj)
{
  Assert(obj);
  // convert object to network ID.
  NetworkId nwId = obj->GetNetworkId();
  EntityId id;
  
  id.ApplicationNumber = nwId.creator;
  id.EntityNumber      = nwId.id;

  return CheckEntityNewId(id);
}


void ObjectLifeSpan::SerializeBin(SerializeBinStream &f)
{
  int deprecated = 0;
  
  SerializeEntityCreateData(_data,f);
  f.Transfer(deprecated);

  base::SerializeBin(f); 
  
  f.Transfer(_simulation); //maintain the order used by previous version of AAR

  DLogF("[AAR] EntityLifeSpan id:%2d/%3d Start:%5.3f End:%5.3f Type:%s",
    _data.Id.ApplicationNumber,
    _data.Id.EntityNumber,
    _birth,
    _death,
    &_data.EntityType[0]
    );
}

void CenterLifeSpan::SerializeBin(SerializeBinStream &f)
{
  SerializeCenterCreateData(_data,f);
  base::SerializeBin(f); 

  DLogF("[AAR] CenterLifeSpan id:%2d/%3d Start:%5.3f End:%5.3f",
    _data.Id.ApplicationNumber,
    _data.Id.EntityNumber,
    _birth,
    _death
  );
}

void GroupLifeSpan::SerializeBin(SerializeBinStream &f)
{
  SerializeGroupCreateData(_data,f);
  base::SerializeBin(f); 

  DLogF("[AAR] GroupLifeSpan id:%2d/%3d Start:%5.3f End:%5.3f",
    _data.Id.ApplicationNumber,
    _data.Id.EntityNumber,
    _birth,
    _death
  );
}


void UnitLifeSpan::SerializeBin(SerializeBinStream &f)
{
  SerializeUnitCreateData(_data,f);
  base::SerializeBin(f); 

  DLogF("[AAR] SerializeUnitCreateData id:%2d/%3d Start:%5.3f End:%5.3f",
    _data.Id.ApplicationNumber,
    _data.Id.EntityNumber,
    _birth,
    _death
  );
}


AARMessageBase::AARMessageBase():_time(0.0f),_passPP(true)
{
};

void AARMessageBase::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    f.Save(&_time,sizeof(float));
  }
  else
  {
    f.Load(&_time,sizeof(float));
  }
}

void AARMessageHeader::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    f.TransferBinary(_mtype);
    f.TransferBinary(_size);
  }
  else
  {
    f.TransferBinary(_mtype);
    f.TransferBinary(_size);
  }
}

void AARMessageWorldLoad::Init()
{
  _worldName = GLandscape->GetName();
  _addons.Clear();
}

void AARMessageCNRSession::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(TypeIs(),-1);
    header.Serialize(f);
  }

  AARMessageBase::Serialize(f);
  f.Transfer(_sessionName);
  f.TransferBinary(_sessionTime);
}

void AARMessageWorldLoad::Serialize(SerializeBinStream &f)
{ 
  if(f.IsSaving())
  {
    AARMessageHeader header(enumLoadWorld,sizeof(AARMessageWorldLoad));
    header.Serialize(f);
  }

  AARMessageBase::Serialize(f);
  f.Transfer(_worldName);
  f.Transfer(_addons);
}

// Apply the message to the world
void AARMessageWorldLoad::Apply()
{
  // Load the world
  char islandName[256];
  GetFilename(islandName,_worldName);
  GWorld->SwitchLandscape(islandName);

  FindArrayRStringCI addons;

  void CheckPatch(FindArrayRStringCI &addOns, FindArrayRStringCI &missing);
  FindArrayRStringCI missing;
  CheckPatch(addons, missing);
  if (missing.Size() > 0)
  {
    // Warning message
    Fail("Addon required");
  }

  // add addons from mission template
  GWorld->ActivateAddons(addons,_worldName);
}

void AAREntityCreateData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(enumEntityCreateData,-1);
    header.Serialize(f);

    AARMessageBase::Serialize(f);
    SerializeEntityCreateData(_data,f);
  }
  else
  {
    AARMessageBase::Serialize(f);
    SerializeEntityCreateData(_data,f);
  }
}

bool AAREntityUpdateData::KeyFrameCanPlay(float time)
{
  Assert(_passPP);

  ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_idIndex);
  if(time <= id_Objls._death && time >= id_Objls._birth )
    return true;
  
  return false;
}

/*
\VBS_patch_internal 1.00 Date [1/25/2008] by clion
- Fixed: EntityUpdate was not preproccesing its death and birth correctly.
*/
void AAREntityUpdateData::PreProccess(int messageIndex)
{
  if(!GAAR.CheckEntityId(_data.Id,_idIndex))
  {
    if(!IsNullEntityId(_data.Id))
    {
      // common this occures in old recordings.
      //DLogF("[AAR] Cannot find link in lifespan for update");
      _passPP = false;
    }
  }
  else
  {
    // check to see if this is an update for an entity that no longer exsists
    ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_idIndex);
    if(_time > id_Objls._death ||
       _time < id_Objls._birth )
    {
      //DLogF("[AAR] Message time:%f Object DeathTime:%f",_time, id_Objls._death);
      _passPP = false;
    }
  } 

  if(!IsNullEntityId(_data.VehicleId))
  {
    if(GAAR.CheckEntityId(_data.VehicleId,_vehicleIdIndex))
    {
      // check to see if this is an update for an entity that no longer exsists
      ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_vehicleIdIndex);
      if(_time > id_Objls._death ||
         _time < id_Objls._birth )
        {
          _passPP = false;
        }      
    }
    else
    {
      Assert(false);
      _passPP = false;
    }
  }
}

// Deprecated no longer required. What i used to do, what store the last message index 
// into the lifespan element so when it came to drawing it could jump quickly to the last
// message and draw it.

EntityUpdateData AAREntityUpdateData::GetDataUpdateIndex(int msgIndex,bool forwardTimeLine)
{
  if(_idIndex >= 0 )
  {
    ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_idIndex);

    // TimeLine management
    if(_lastMessageIndex == -1)
    {
      if(forwardTimeLine)
      {
        if(id_Objls._lastMsgIndex == 0)// was their every a previous message?
          _lastMessageIndex = msgIndex;
        else
          _lastMessageIndex = id_Objls._lastMsgIndex;
      }
    }

    if(forwardTimeLine )
      id_Objls._lastMsgIndex = msgIndex; 
    else
      id_Objls._lastMsgIndex = _lastMessageIndex;
  }

  return GetData();
}

EntityUpdateData AAREntityUpdateData::GetData()
{
  EntityUpdateData data = _data;

  // This should allways be true
  if(_idIndex >= 0)
  {
    ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_idIndex);
    data.Id = id_Objls.GetNewId();
  }
  // Bug i cannot be bother hunting down here.
  // when reversing the vehicleID is valid, but no longer
  // exsists. Don't know why!
  if(_vehicleIdIndex >= 0)
  {
    ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_vehicleIdIndex);
    data.VehicleId = id_Objls.GetNewIdCanBeNull();
  }

  return data;
}

#define ISDIFFERENCE(VALEONE) \
  if(myData.##VALEONE != otherData.##VALEONE ) return false

bool AAREntityUpdateData::operator==(const AAREntityUpdateData &other)
{
  const EntityUpdateData &myData = _data;
  const EntityUpdateData &otherData = other._data;

  ISDIFFERENCE(Id.ApplicationNumber);
  ISDIFFERENCE(Id.EntityNumber);
  ISDIFFERENCE(Pos.x);
  ISDIFFERENCE(Pos.y);
  ISDIFFERENCE(Pos.z);
  ISDIFFERENCE(Dir.x);
  ISDIFFERENCE(Dir.y);
  ISDIFFERENCE(Dir.z);
  ISDIFFERENCE(Up.x);
  ISDIFFERENCE(Up.y);
  ISDIFFERENCE(Up.z);
  ISDIFFERENCE(Velocity.x);
  ISDIFFERENCE(Velocity.y);
  ISDIFFERENCE(Velocity.z);
  ISDIFFERENCE(Acceleration.x);
  ISDIFFERENCE(Acceleration.y);
  ISDIFFERENCE(Acceleration.z);
  ISDIFFERENCE(Damage);
  ISDIFFERENCE(Appearance);
  ISDIFFERENCE(Lights);
  ISDIFFERENCE(HasTurret);
  ISDIFFERENCE(Azimuth);
  ISDIFFERENCE(Elevation);
  ISDIFFERENCE(AzimuthWanted);
  ISDIFFERENCE(ElevationWanted);
  ISDIFFERENCE(VehicleId.ApplicationNumber);
  ISDIFFERENCE(VehicleId.EntityNumber);
  ISDIFFERENCE(EPos);
  ISDIFFERENCE(Stance);
  ISDIFFERENCE(PrWeaponPosture);
  ISDIFFERENCE(CrewIndex);
  ISDIFFERENCE(SelectedWeapon);

  // they're the same
  return true;
}

void AARDetonationData::PreProccess(int messageIndex)
{
  if(!GAAR.CheckEntityId(_data.FiringId,_idFiringId))
  {
    if(!IsNullEntityId(_data.FiringId))
    {
      Assert(false);
      _passPP = false;
    }
  }
  if(!GAAR.CheckEntityId(_data.TargetId,_idTargetId))
  {
    if(!IsNullEntityId(_data.TargetId))
    {
      Assert(false);
      _passPP = false;
    }
  }
}

DetonationData AARDetonationData::GetData()
{ 
  DetonationData data = _data;
  if(_idFiringId >= 0)
  {
    ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_idFiringId);
    data.FiringId = id_Objls.GetNewId();
  }
  if(_idTargetId >= 0)
  {
    ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_idTargetId);
    data.TargetId = id_Objls.GetNewId();
  }

  return data; 
};


void AARFireWeaponData::PreProccess(int messageIndex)
{
  if(!GAAR.CheckEntityId(_data.FiringId,_fireIdIndex))
  {
    if(!IsNullEntityId(_data.FiringId))
    {
      Assert(false);
      _passPP = false;
    }
  }
  else
  {
    // Valid weapon fired data, we tell AAR about it
    ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_fireIdIndex);
    // Object side could possibly be completly incorrect.
    if(id_Objls._data.Side >= TEast && id_Objls._data.Side < TSideUnknown )
    {
      GAAR.AddFireEventTime((TargetSide) id_Objls._data.Side,_time);
    }
    else
    {
      // Should never occure
      Assert(false);
    }
  }
  if(!GAAR.CheckEntityId(_data.TargetId,_targetIdIndex))
  {
    if(!IsNullEntityId(_data.TargetId))
    {
      Assert(false)
      _passPP = false;
    }
  }
}
 
FireWeaponData AARFireWeaponData::GetData()
{
  FireWeaponData data = _data;
  if(_fireIdIndex >= 0)
  {
    ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_fireIdIndex);
    data.FiringId = id_Objls.GetNewId();
  }
  if(_targetIdIndex >= 0)
  {
    ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_targetIdIndex);
    data.TargetId = id_Objls.GetNewId();
  }

  return data;
}

/*
\VBS_patch_internal 1.00 Date [1/25/2008] by clion
- Fixed: Fix missed serializing the center create data
*/
void AARCenterCreateData::Serialize(SerializeBinStream &f)
{
  CenterCreateData &data = (CenterCreateData&)_data;

  if(f.IsSaving())
  {
    AARMessageHeader header(enumCenterCreateData,-1);
    header.Serialize(f);
    AARMessageBase::Serialize(f);

    SerializeEntityId(data.Id,f);
    f.TransferBinary(data.Side);
  }
  else
  {
    AARMessageBase::Serialize(f);

    SerializeEntityId(data.Id,f);
    f.TransferBinary(data.Side);
  }
}

void AARCenterCreateData::PreProccess(int messageIndex)
{
  /*
  if(!GAAR.CheckEntityId(_data.Id,_idIndex))
  {
    if(!IsNullEntityId(_data.Id))
    {
      Assert(false);
      _passPP = false;
    }
  }
  */
}

CenterCreateData AARCenterCreateData::GetData()
{
  CenterCreateData data = _data;
  if(_idIndex >= 0)
  {
    ObjectLifeSpan &objls = GAAR.GetLifeSpan(_idIndex);
    data.Id = objls.GetNewId();
  }
  
  return data;
}

void AARGroupCreateData::PreProccess(int messageIndex)
{
  //link the findGroup with the index in the lifeSpan
  FindEntityID findGroupId(_data.Id);
  GAAR._groupLifeSpan.ForEachF(findGroupId);
  _idIndex = findGroupId.GetResult();

  FindEntityID findCenterId(_data.CenterId);
  GAAR._centerLifeSpan.ForEachF(findCenterId);
  _centerIdIndex = findCenterId.GetResult();

  FindEntityID findLeaderId(_data.LeaderId);
  GAAR._unitLifeSpan.ForEachF(findLeaderId);
  _leaderIndex = findLeaderId.GetResult();

  if(_idIndex       == -1 ||
     _centerIdIndex == -1 )
  {
    // in developer build, this should never happen during recording
    Assert(false);
     _passPP = false;
  }
}

GroupCreateData AARGroupCreateData::GetData()
{
  GroupCreateData data = _data;

  if(_idIndex >= 0)
    data.Id = GAAR._groupLifeSpan[_idIndex]->GetNewId();

  if(_leaderIndex >= 0)
    data.LeaderId = GAAR._unitLifeSpan[_leaderIndex]->GetNewId();
  else
    data.LeaderId = NullEntityId;

  if(_centerIdIndex >= 0)
    data.CenterId = GAAR._centerLifeSpan[_centerIdIndex]->GetNewId();

  return data;
}

/*
\VBS_patch_internal 1.00 Date [1/25/2008] by clion
- Fixed: Forgott to serialize the group create data time
*/
void AARGroupCreateData::Serialize(SerializeBinStream &f)
{
  GroupCreateData &data = (GroupCreateData&)_data;
  if(f.IsSaving())
  {
    AARMessageHeader header(enumGroupCreateData,-1);
    header.Serialize(f);
    AARMessageBase::Serialize(f);

    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.CenterId,f);
    f.TransferBinary(data.Number);
    SerializeEntityId(data.LeaderId,f);
  }
  else
  {
    AARMessageBase::Serialize(f);

    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.CenterId,f);
    f.TransferBinary(data.Number);
    SerializeEntityId(data.LeaderId,f);
  }
}

/*
\VBS_patch_internal 1.00 Date [1/25/2008] by clion
- Fixed: Forgott to serialize the unitcreateData time
*/
void AARUnitCreateData::Serialize(SerializeBinStream &f)
{
  UnitCreateData &data = (UnitCreateData&)_data;

  if(f.IsSaving())
  {
    AARMessageHeader header(enumUnitCreateData,-1);
    header.Serialize(f);
    AARMessageBase::Serialize(f);

    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.PersonId,f);
    SerializeEntityId(data.GroupId,f);
    f.TransferBinary(data.Number);
  }
  else
  {
    AARMessageBase::Serialize(f);

    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.PersonId,f);
    SerializeEntityId(data.GroupId,f);
    f.TransferBinary(data.Number);
  }
}
void AARUnitCreateData::PreProccess(int messageIndex)
{
  FindEntityID findId(_data.Id);
  GAAR._unitLifeSpan.ForEachF(findId);
  _idIndex = findId.GetResult();

  FindEntityID findperson(_data.PersonId);
  GAAR._entityLifeSpan.ForEachF(findperson);
  _personIndex = findperson.GetResult();  

  //link the findGroup with the index in the lifeSpan
  FindEntityID findGroup(_data.GroupId);
  GAAR._groupLifeSpan.ForEachF(findGroup);
  _groupIndex = findGroup.GetResult();

  if(_idIndex     == -1 ||
     _personIndex == -1 ||
     _groupIndex  == -1 )
  {
    // in developer build, this should never happen during recording
    Assert(false);
     _passPP = false;
  }
}
UnitCreateData AARUnitCreateData::GetData()
{
  UnitCreateData data = _data;

  if(_idIndex >= 0)
    data.Id = GAAR._unitLifeSpan[_idIndex]->GetNewId();

  if(_personIndex >= 0)
    data.PersonId = GAAR._entityLifeSpan[_personIndex]->GetNewId(); 

  if(_groupIndex >= 0)
    data.GroupId  = GAAR._groupLifeSpan[_groupIndex]->GetNewId();
  
  return data;
}

AAREntityUpdateData::AAREntityUpdateData()
{
  _lastMessageIndex = -1;
  _idIndex = -1;
  _vehicleIdIndex = -1;

  EntityUpdateData &data = (EntityUpdateData&) _data;
  ZeroMemory(&data,sizeof(EntityUpdateData));
}

AAREntityUpdateData::~AAREntityUpdateData()
{
}

AAREntityUpdateData::AAREntityUpdateData(const EntityUpdateData &data,float time):_data(data)
{
  _lastMessageIndex = -1;
  _time = time;
  _idIndex = -1;
  _vehicleIdIndex = -1;
  Assert(_data.Id.ApplicationNumber>=0);
  Assert(_data.Id.EntityNumber>=0);
}

void AAREntityUpdateData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(enumEntityUpdateData,-1);
    header.Serialize(f);

    AARMessageBase::Serialize(f);
    SerializeEntityUpdateData(_data,f);
  }
  else
  {
    AARMessageBase::Serialize(f);
    SerializeEntityUpdateData(_data,f);
  }
}

void AARFireWeaponData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(enumFireWeaponData,-1);
    header.Serialize(f);

    AARMessageBase::Serialize(f);
    SerializeFireWeaponData(_data,f);
  }
  else
  {
    AARMessageBase::Serialize(f);
    SerializeFireWeaponData(_data,f);
  }
}

static void UpdateEntityStats(ObjectLifeSpan &person)
{
  EntityAI *entAi = GAAR.GetEntityAI(person.GetNewId());
  if(entAi)
  {
    TargetSide side = entAi->GetTargetSideAsIfAlive();
    AARStat myStat;

    person._stats[side].GetStats(myStat);
    entAi->SetStat(myStat,side);
  }
}

static void UpdateWoundedData(int ShoterId,
                              int TargetId,
                              bool moveForward,
                              ObjectLifeSpan &shoter,
                              ObjectLifeSpan &target, 
                              AutoArray<Wounded> &side)
{
  // we ignore people hitting themselves.
  if(ShoterId == TargetId) return;

  // check if i've hit this target before
  bool hitBefore = false;
  for(int i = 0; i < side.Size(); i++)
  {
    if( side[i].who == TargetId )
    {
      hitBefore = true;
      if(moveForward)
      {
        side[i].num += 1;
      }
      else
      {
        side[i].num -= 1;
        if(side[i].num <= 0)
        {
          side.Delete(i,1);
          break;
        }         
      }
    }
  }

  if(!hitBefore)
  {
    // error in frame count we ignore rewinding if this happens.
    if(!moveForward) return;

    int index = side.Add();

    Assert(side[index].num == 0)
    side[index].who = TargetId;
    side[index].num += 1;
  }
}

void AAREntityUpdateData::ProccessStatistics(bool moveForward)
{

}

void AARFireWeaponData::ProccessStatistics(bool moveForward)
{
  if(_fireIdIndex >= 0)
  {
    ObjectLifeSpan &shoter = GAAR.GetLifeSpan(_fireIdIndex);

    EntityAI *entAi = GAAR.GetEntityAI(shoter.GetNewId());
    if(entAi)
      shoter._stats[entAi->GetTargetSideAsIfAlive()].roundsFired += moveForward ? 1 : -1;
    
    UpdateEntityStats(shoter);

    /* Fired events do not contain who it shot, only usefull for rounds fired
    if(_targetIdIndex >= 0)
    {
      ObjectLifeSpan &target = GAAR.GetLifeSpan(_targetIdIndex);
      if(shoter._data.Side == target._data.Side)
        UpdateWoundedData(_fireIdIndex,moveForward,shoter,target,shoter._stats.friendlyWounded);

      if(shoter._data.Side != target._data.Side)
        UpdateWoundedData(_fireIdIndex,moveForward,shoter,target,shoter._stats.enemyWounded);
    } 
    */
  }
}

void AAREntityDestroyed::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(enumEntityDestroyed,-1);
    header.Serialize(f);

    AARMessageBase::Serialize(f);
    SerializeEntityId(_data.Killed,f);
    SerializeEntityId(_data.Killer,f);
  }
  else
  {
    AARMessageBase::Serialize(f);
    SerializeEntityId(_data.Killed,f);
    SerializeEntityId(_data.Killer,f);
  }
}

void AAREntityDestroyed::PreProccess(int messageIndex)
{
  if(!GAAR.CheckEntityId(_data.Killed,_killed))
  {
    if(!IsNullEntityId(_data.Killed))
    {
      Assert(false);
      _passPP = false;
    }
  }
  else
  {
    // Valid weapon fired data, we tell AAR about it
    ObjectLifeSpan &id_Objls = GAAR.GetLifeSpan(_killed);
    // Object side could possibly be completly incorrect.
    if(id_Objls._data.Side >= TEast && id_Objls._data.Side < TSideUnknown )
    {
      GAAR.AddDeathTime((TargetSide) id_Objls._data.Side,_time);
    }
    else
    {
      // Should never occure
      Assert(false);
    }
  }
  if(!GAAR.CheckEntityId(_data.Killer,_killer))
  {
    if(!IsNullEntityId(_data.Killer))
    {
      Assert(false);
      _passPP = false;
    }
  }
}

EntityDestroyed AAREntityDestroyed::GetData()
{
  EntityDestroyed data = _data;
  if(_killed >= 0)
    data.Killed = GAAR.GetLifeSpan(_killed).GetNewId();

  if(_killer >= 0)
    data.Killer = GAAR.GetLifeSpan(_killer).GetNewId();

  return data;
}

void AAREntityDestroyed::ProccessStatistics(bool moveForward)
{
  if(_killer >= 0)
  {
    ObjectLifeSpan &killer = GAAR.GetLifeSpan(_killer);
    // detonation data does not contain in-direct fire
    // shoter._stats.explosivesFired += moveForward ? 1 : -1;

    if(_killed >= 0)
    {
      ObjectLifeSpan &killed = GAAR.GetLifeSpan(_killed);

      EntityAI *killerEn = GAAR.GetEntityAI(killer.GetNewId());
      EntityAI *killedEn = GAAR.GetEntityAI(killed.GetNewId());

      if(killerEn && killedEn)
      {
        TargetSide killerSide = killerEn->GetTargetSideAsIfAlive();
        TargetSide killedSide = killedEn->GetTargetSideAsIfAlive();

        if(killerSide == killedSide)
          killer._stats[killerSide].friendlyKilled += moveForward ? 1 : -1;
        else
          killer._stats[killerSide].enemyKilled    += moveForward ? 1 : -1;

        UpdateEntityStats(killed);
        UpdateEntityStats(killer);
      }
      else
      {
        Assert(false);// Should never fire.
      }
    }
  }
  // do proccessing on the killer and who got killed!
}


void AARDoDamageData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(enumDoDamage,-1);
    header.Serialize(f);

    AARMessageBase::Serialize(f);

    SerializeDoDamage(_data,f);
  }
  else
  {
    AARMessageBase::Serialize(f);
    SerializeDoDamage(_data,f);
  }
}

void AARDoDamageData::PreProccess(int messageIndex)
{
  if(!GAAR.CheckEntityId(_data.Who,_who))
  {
    if(!IsNullEntityId(_data.Who))
    {
      Assert(false);
      _passPP = false;
    }
  }
  if(!GAAR.CheckEntityId(_data.Killer,_killer))
  {
    if(!IsNullEntityId(_data.Killer))
    {
      Assert(false);
      _passPP = false;
    }
  }
}

DoDamageData AARDoDamageData::GetData()
{
  DoDamageData data = _data;

  if(_who >= 0)
  {
    ObjectLifeSpan &who = GAAR.GetLifeSpan(_who);
    data.Who = who.GetNewId();
  }
  if(_killer >= 0)
  {
    ObjectLifeSpan &killer = GAAR.GetLifeSpan(_killer);
    data.Killer = killer.GetNewId();
  }

  return data;
}


void AARDoDamageData::ProccessStatistics(bool moveForward)
{
  if(_killer >= 0)
  {
    ObjectLifeSpan &shoter = GAAR.GetLifeSpan(_killer);
    // detonation data does not contain in-direct fire
    // shoter._stats.explosivesFired += moveForward ? 1 : -1;

    if(_who >= 0)
    {
      ObjectLifeSpan &target = GAAR.GetLifeSpan(_who);


      EntityAI *entAi = GAAR.GetEntityAI(shoter.GetNewId());
      if(entAi)
      {
        if(shoter._data.Side == target._data.Side)
          UpdateWoundedData(_killer,
                            _who,
                            moveForward,
                            shoter,
                            target,
                            shoter._stats[entAi->GetTargetSideAsIfAlive()].friendlyWounded);

        if(shoter._data.Side != target._data.Side)
          UpdateWoundedData(_killer,
                            _who,
                            moveForward,
                            shoter,
                            target,
                            shoter._stats[entAi->GetTargetSideAsIfAlive()].enemyWounded);
      }

      UpdateEntityStats(shoter);
      UpdateEntityStats(target);
    }
  }
}


void AARDetonationData::ProccessStatistics(bool moveForward){}

void AARDetonationData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(enumDetonationData,-1);
    header.Serialize(f);

    AARMessageBase::Serialize(f);
    SerializeDetonationData(_data,f);
  }
  else
  {
    AARMessageBase::Serialize(f);
    SerializeDetonationData(_data,f);
  }
}

void AARUnitUpdateData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(enumUnitUpdateData,-1);
    header.Serialize(f);

    AARMessageBase::Serialize(f);
    SerializeUnitUpdateData(_data,f);
  }
  else
  {
    AARMessageBase::Serialize(f);
    SerializeUnitUpdateData(_data,f);
  }
}

void AARUnitUpdateData::PreProccess(int messageIndex)
{
  FindEntityID findId(_data.Id);
  GAAR._unitLifeSpan.ForEachF(findId);
  _idIndex = findId.GetResult();

  //link the findGroup with the index in the lifeSpan
  FindEntityID findGroup(_data.GroupId);
  GAAR._groupLifeSpan.ForEachF(findGroup);
  _groupIndex = findGroup.GetResult();

  if(_insertedMessaged)
  {
    // here, we search through all the recorded messages to find. another with the same id as mine.
    // if we do, just simply append its groupId onto myself!
    bool found = false;
    // dont search ourself :) -1
    for(int i = (messageIndex - 1); i >= 0; --i)
    {
      Ref<AARMessageBase> msg = GAAR._messages[i]->_message;
      if(!msg) continue;
      if(msg->TypeIs() != TypeIs()) continue;

      AARUnitUpdateData *unitUpdate = static_cast<AARUnitUpdateData*>(GAAR._messages[i]->_message.GetRef());
      if(unitUpdate->_idIndex == _idIndex)
      {
        _groupIndex = unitUpdate->_groupIndex;
        found = true;
        break;
      }
    }

    if(_idIndex == -1 )
      DLogF("[AAR] CTD: Old recording, error with UnitUpdateData");

    // if we didnt find a message, we know we're the last one...
    // so, we need the original group that we were attached too.
    if(!found && (_idIndex != -1))
    {
      EntityId groupId = GAAR._unitLifeSpan[_idIndex]->_data.GroupId;
      //_data.GroupId = GAAR._unitLifeSpan[_idIndex]->_data.GroupId;
      
      //link the findGroup with the index in the lifeSpan
      FindEntityID findGroup(groupId);
      GAAR._groupLifeSpan.ForEachF(findGroup);
      _groupIndex = findGroup.GetResult();
    }
  }

  FindEntityID findperson(_data.PersonId);
  GAAR._entityLifeSpan.ForEachF(findperson);
  _personIndex = findperson.GetResult();  

  if(_idIndex     == -1 ||
     _personIndex == -1 ||
     _groupIndex  == -1 )
  {
    // in developer build, this should never happen during recording
    _passPP = false;
  }
}

UnitUpdateData AARUnitUpdateData::GetData()
{
  UnitUpdateData data = _data;

  // This is really really bad
  // should be refractured
  data.Id = GAAR._unitLifeSpan[_idIndex]->GetNewId();
  data.PersonId = GAAR._entityLifeSpan[_personIndex]->GetNewId();  

  // Group update, can be null.
  data.GroupId  = GAAR._groupLifeSpan[_groupIndex]->GetNewIdCanBeNull();

  DLogF("[AAR] UpdateUnitCreateData: Id:%d/%d PersonId:%d/%d GroupId:%d/%d Num:%d",
    data.Id.ApplicationNumber,
    data.Id.EntityNumber,
    data.PersonId.ApplicationNumber,
    data.PersonId.EntityNumber,
    data.GroupId.ApplicationNumber,
    data.GroupId.EntityNumber,
    data.Number);

  return data;
}

void AARUpdateGroupData::PreProccess(int messageIndex)
{
  FindEntityID findId(_data.Id);
  GAAR._groupLifeSpan.ForEachF(findId);
  _idIndex = findId.GetResult();

  FindEntityID findCenterId(_data.CenterId);
  GAAR._centerLifeSpan.ForEachF(findCenterId);
  _centerIdIndex = findCenterId.GetResult();

  // _unitLifeSpan is not a typo! group.leaderid 
  // references _unitLifeSpanId for the personid for the leaderID
  FindEntityID findLeaderId(_data.LeaderId);
  GAAR._unitLifeSpan.ForEachF(findLeaderId);
  _leaderId = findLeaderId.GetResult();

  if(_idIndex       == -1 ||
     _centerIdIndex == -1 )
  {
    // in developer build, this should never happen during recording
    Assert(false);
    _passPP = false;
  }
}

void AARUpdateGroupData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(enumUpdateGroupData,-1);
    header.Serialize(f);

    AARMessageBase::Serialize(f);
    SerializeUpdateGroupData(_data,f);
  }
  else
  {
    AARMessageBase::Serialize(f);
    SerializeUpdateGroupData(_data,f);
  }
}

UpdateGroupData AARUpdateGroupData::GetData()
{
  UpdateGroupData data = _data;
  // This is really really bad
  // should be refractured
  data.Id        = GAAR._groupLifeSpan[_idIndex]->GetNewId();
  data.CenterId  = GAAR._centerLifeSpan[_centerIdIndex]->GetNewId();
  
  if( _leaderId >= 0 )
    data.LeaderId  = GAAR._unitLifeSpan[_leaderId]->GetNewId();
  else
    data.LeaderId = NullEntityId;

  DLogF("[AAR] AARUpdateGroupData: Id:%d/%d UnitCreateDataId:%d/%d CenterId:%d/%d Num:%d",
    data.Id.ApplicationNumber,
    data.Id.EntityNumber,
    data.LeaderId.ApplicationNumber,
    data.LeaderId.EntityNumber,
    data.CenterId.ApplicationNumber,
    data.CenterId.EntityNumber,
    data.Number);

  return data;
}

void AAREntityDeleteData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(enumEntityDeleteData,-1);
    header.Serialize(f);

    AARMessageBase::Serialize(f);
    SerializeEntityId(_data,f);
  }
  else
  {
    AARMessageBase::Serialize(f);
    SerializeEntityId(_data,f);
  }
}

AARTurretUpdate::AARTurretUpdate():_idIndex(-1)
{
  TurretUpdate &data = (TurretUpdate&) _data;
  ZeroMemory(&data,sizeof(TurretUpdate));
};

/*
\VBS_patch_internal 1.00 Date [1/25/2008] by clion
- Fixed: Possible AAR crash, fogott to serialize the turret update correctly
*/
void AARTurretUpdate::Serialize(SerializeBinStream &f)
{
  TurretUpdate &data = (TurretUpdate&) _data;

  if(f.IsSaving())
  {
    AARMessageHeader header(enumTurretUpdate,-1);
    header.Serialize(f);
  }

  AARMessageBase::Serialize(f);
  SerializeEntityId(data.Id,f);
  f.TransferBinary(data.TurretNum);
  f.TransferBinary(data.Azimuth);
  f.TransferBinary(data.AzimuthWanted);
  f.TransferBinary(data.Elevation);
  f.TransferBinary(data.ElevationWanted);

}

void AARTurretUpdate::PreProccess(int messageIndex)
{
  if(!GAAR.CheckEntityId(_data.Id,_idIndex))
  {
    if(!IsNullEntityId(_data.Id))
    {
      _passPP = false;
      Assert(false);
    }
  }
}

TurretUpdate AARTurretUpdate::GetData()
{
  TurretUpdate data = _data;

  if(_idIndex >= 0)
    data.Id = GAAR.GetLifeSpan(_idIndex).GetNewId();

  return data;
}

AARAttachToUpdate::AARAttachToUpdate()
{
  AttachToData &data = (AttachToData&) _data;
  ZeroMemory(&data,sizeof(data));
  _objIndex = _attachToIndex = -1;
  _previousMessage = -1;
}

void AARAttachToUpdate::Serialize(SerializeBinStream &f)
{
  AttachToData &data = (AttachToData&) _data;
  
  if(f.IsSaving())
  {
    AARMessageHeader header(enumAttachToUpdate,-1);
    header.Serialize(f);
  }

    AARMessageBase::Serialize(f);
    SerializeEntityId(data.obj,f);
    SerializeEntityId(data.attachTo,f);
    SerializeGeneric3DType(data.com,f);
    f.TransferBinary(data.memoryIndex);

    //! Changed: flag from bool to index.
    if(GAAR.GetVersion() < 13 )
    {
      bool flag;
      f.TransferBinary(flag);
      data.flags = (int) flag;
    }
    else
    {
      f.TransferBinary(data.flags);
    }
    
}

/*
  With reversing in time we, have a tad bit of a problem.

  That problem is the following. When A connects to B, and B
  is disconnected because of A being deleted. When reversing
  B message to un-attach of A, is okay because A currently does
  not exsist. But the problem is, to reattach to A when it comes back 
  into exsistance like it originaly happened causes problems.

  So, nothing much we can do here without changing the whole system. 
  We just change attachToUpdate 
 */
void AARAttachToUpdate::PreProccess(int messageIndex)
{
  FindEntityID findId(_data.obj);
  GAAR._entityLifeSpan.ForEachF(findId);
  _objIndex = findId.GetResult();

  FindEntityID findAttach(_data.attachTo);
  GAAR._entityLifeSpan.ForEachF(findAttach);
  _attachToIndex = findAttach.GetResult();

  // we're not attached to anything
  // we need to find out, for reversing 
  // who we where attached to!
  if(IsNullEntityId(_data.attachTo))
  {
    for(int i = messageIndex; i >= 0; --i)
    {
      Ref<AARMessageBase> msg = GAAR._messages[i]->_message;
      if(!msg) continue;
      if(msg->TypeIs() == TypeIs())
      {
        _previousMessage = i;
        break;
      }
    }
  }

  // Check that this message is within both life spans
  if(_objIndex != -1)
  {
    // object no longer exsists!
    if(!(_time >= GAAR.GetLifeSpan(_objIndex)._birth &&
       _time <= GAAR.GetLifeSpan(_objIndex)._death))
    {
       Assert(false);
       _passPP = false;
    }   
  }

  // Check that this message is within both life spans
  if(_attachToIndex != -1)
  {
    // object no longer exsists!
    if(!(_time >= GAAR.GetLifeSpan(_attachToIndex)._birth &&
      _time <= GAAR.GetLifeSpan(_attachToIndex)._death))
    {
      Assert(false);
      _passPP = false;
    }   
  }

  // attach to index can be null indicating to detach
  // if its not null, then check to validate that we're
  // attaching to a person who actualy still exsists
  if(_objIndex == -1 || 
     (!IsNullEntityId(_data.attachTo) &&
      (_attachToIndex == -1)))
  {
    Assert(false);
    _passPP = false;
  }
}

AttachToData AARAttachToUpdate::GetData()
{
  AttachToData data = _data;

  if(_objIndex >= 0)
    data.obj = GAAR.GetLifeSpan(_objIndex).GetNewId();

  // Can be null, even when valid!
  if(_attachToIndex >= 0)
    data.attachTo = GAAR.GetLifeSpan(_attachToIndex).GetNewIdCanBeNull();
  
  return data;
}

AARCreateMarker::AARCreateMarker()
{
  _time = 0.0;
  _previousMessage = -1;
}

AARCreateMarker::AARCreateMarker(const ArcadeMarkerInfo *marker, float time)
{
  MarkerCreateData &data = (MarkerCreateData&) _data;

  data.pos  = marker->position;
  data.name = marker->name;
  data.text = marker->text;
  data.markerType = marker->markerType;
  data.type = marker->type;
  data.colorName = marker->colorName;
  data.color = marker->color;
  data.fillName = marker->fillName;
  data.size  = marker->size;
  data.a = marker->a;
  data.b = marker->b;
  data.angle = marker->angle;
  data.selected = marker->selected;
  data.autosize = marker->autosize;
  data.isHidden = marker->isHidden;

  for( int i = 0; i < marker->layeredIcons.Size(); ++i)
  {
    if(marker->layeredIcons[i] && marker->layeredIcons[i]->icon )
    {
      int index = data.layers.Add();
      Ref<LayeredMarkerInfo> info = marker->layeredIcons[i];
      data.layers[index].tName = info->icon->Name();
      data.layers[index].a     = info->a;
      data.layers[index].b     = info->b;
      data.layers[index].x     = info->x;
      data.layers[index].y     = info->y;
    }
  }

  _time = time;
  _previousMessage = -1;
}

// DEPRECRATED STRUCTURE!
struct DEPRECATED_MarkerCreateData
{
  Coordinate3DType pos;
  bool  deleteMarker; // delete the marker if true
  char  name[255];
  char  text[255];
  byte  markerType;
  char  type[255];
  char  colorName[255];
  ULONG color;
  char  fillName[255];
  float size;
  float a;
  float b;
  float angle;
  bool  selected;
  bool  autosize;
  bool  isHidden;
};

void AARCreateMarker::Serialize(SerializeBinStream &f)
{
  MarkerCreateData &data = (MarkerCreateData&) _data;

  // Save the message header
  if(f.IsSaving())
  {
    // write out what type this is, if saving
    AARMessageHeader header(enumCreateMarker,-1);
    header.Serialize(f);
  }

  // load the message
  AARMessageBase::Serialize(f);
  
  if(GAAR.GetVersion() <= 10)
  {
    DEPRECATED_MarkerCreateData oldData;

    SerializeGeneric3DType(oldData.pos,f);
    SerializeCharArray(oldData.name,sizeof(oldData.name),f);
    SerializeCharArray(oldData.text,sizeof(oldData.text),f);
    f.TransferBinary(oldData.markerType);
    SerializeCharArray(oldData.type,sizeof(oldData.type),f);
    SerializeCharArray(oldData.colorName,sizeof(oldData.colorName),f);
    f.TransferBinary(oldData.color);
    SerializeCharArray(oldData.fillName,sizeof(oldData.fillName),f);
    f.TransferBinary(oldData.size);
    f.TransferBinary(oldData.a);
    f.TransferBinary(oldData.b);
    f.TransferBinary(oldData.angle);
    f.TransferBinary(oldData.selected);
    f.TransferBinary(oldData.autosize);
    f.TransferBinary(oldData.isHidden);
    f.TransferBinary(oldData.deleteMarker);

    _passPP = false;// dont execute the old marker
  }
  else
  {
    f.TransferBinary(data.pos[0]);
    f.TransferBinary(data.pos[1]);
    f.TransferBinary(data.pos[2]);
    f.Transfer(data.name);
    f.Transfer(data.text);
    f.Transfer(data.markerType);
    f.Transfer(data.type);
    f.Transfer(data.colorName);
    f.TransferBinary(data.color);
    f.Transfer(data.fillName);
    f.Transfer(data.size);
    f.Transfer(data.a);
    f.Transfer(data.b);
    f.Transfer(data.angle);
    f.Transfer(data.selected);
    f.Transfer(data.autosize);
    f.Transfer(data.isHidden);
    f.TransferArray(data.layers);
  }
}

void AARCreateMarker::RunMessage()
{
  bool found = false;
  int index  = 0;
  RString name;
  for (int i=0; i< markersMap.Size(); i++)
  {
    ArcadeMarkerInfo &mInfo = markersMap[i];
    if (stricmp(mInfo.name,_data.name) == 0)
    {
      found = true;
      index = i;
      name = _data.name;
      break;
    }
  }

  // Add the extra marker
  if(!found)
    index = markersMap.Add();

  ArcadeMarkerInfo &mInfo = markersMap[index];
  mInfo.position    = _data.pos;
  mInfo.name        = _data.name;
  mInfo.text        = _data.text;
  mInfo.markerType  = (MarkerType) _data.markerType;
  mInfo.type        = _data.type;
  mInfo.colorName   = _data.colorName;
  mInfo.color       = PackedColor(_data.color);
  mInfo.fillName    = _data.fillName;
  mInfo.size        = _data.size;
  mInfo.a           = _data.a;
  mInfo.b           = _data.b;
  mInfo.angle       = _data.angle;
  mInfo.selected    = _data.selected;
  mInfo.autosize    = _data.autosize;
  mInfo.isHidden    = _data.isHidden;
  mInfo.condition   = RString("");

  //////////////////
  // Layered marker section
  if(_data.layers.Size()<mInfo.layeredIcons.Size())
    mInfo.layeredIcons.Clear();
 
  while(_data.layers.Size()!=mInfo.layeredIcons.Size())
    mInfo.layeredIcons.Add(new LayeredMarkerInfo);

  for(int i = 0; i < _data.layers.Size(); ++i)
  {
    // check if there is a texture
    if(mInfo.layeredIcons[i]->icon)
    {
      if(stricmp(mInfo.layeredIcons[i]->icon->Name(),_data.layers[i].tName)!=0)
        mInfo.layeredIcons[i]->icon = GlobLoadTexture(_data.layers[i].tName);
    }
    else
      mInfo.layeredIcons[i]->icon = GlobLoadTexture(_data.layers[i].tName);

    mInfo.layeredIcons[i]->a = _data.layers[i].a;
    mInfo.layeredIcons[i]->b = _data.layers[i].b;
    mInfo.layeredIcons[i]->x = _data.layers[i].x;
    mInfo.layeredIcons[i]->y = _data.layers[i].y;
  }
  //////////////////


  mInfo.OnTypeChanged();
  mInfo.OnColorChanged();
  mInfo.OnFillChanged();

  // if it was found, then its an update, otherwise its a create
  void ProcessCreateMarkerEH(RString name, bool updated = true);
  ProcessCreateMarkerEH(mInfo.name,found);
}

// Nothing to validate
void AARCreateMarker::PreProccess(int messageIndex)
{
  // problem here, dont scan yourself!
  for(int i = (messageIndex - 1); i >= 0; --i)
  {
    Ref<AARMessageBase> msg = GAAR._messages[i]->_message;
    if(!msg) continue;
    if(msg->TypeIs() == enumCreateMarker)
    {
      AARCreateMarker *cmker = static_cast<AARCreateMarker*>(msg.GetRef());
      if(cmker->_data.name == _data.name)
      {
        _previousMessage = i;
        break;
      }
    }
  }
}


AARDeleteMarker::AARDeleteMarker()
{
  _time = 0.0f;
  _previousMessage = -1;
}

AARDeleteMarker::AARDeleteMarker(const RString &name, float time)
{
  _name = name;
  _time = time;
  _previousMessage = -1;
}

// We need to find, the create message.
void AARDeleteMarker::PreProccess(int messageIndex)
{
  for(int i = (messageIndex - 1); i >= 0; --i)
  {
    Ref<AARMessageBase> msg = GAAR._messages[i]->_message;
    if(!msg) continue;
    if(msg->TypeIs() == enumCreateMarker)
    {
      AARCreateMarker *cmker = static_cast<AARCreateMarker*>(msg.GetRef());
      if(cmker->_data.name == _name)
      {
        _previousMessage = i;
        break;
      }
    }
  }
}

void AARDeleteMarker::Serialize(SerializeBinStream &f)
{
  // Save the message header
  if(f.IsSaving())
  {
    // write out what type this is, if saving
    AARMessageHeader header(TypeIs(),-1);
    header.Serialize(f);
  }

  // load the message
  AARMessageBase::Serialize(f);
  f << _name;
}


void AARDeleteMarker::RunMessage()
{
  for (int i=0; i< markersMap.Size(); i++)
  {
    ArcadeMarkerInfo &mInfo = markersMap[i];
    if (stricmp(mInfo.name,_name) == 0)
    {
      void ProcessDeleteMarkerEH(RString name);
      ProcessDeleteMarkerEH(_name);

      markersMap.Delete(i,1);// delete this marker      
      break;
    }
  }
}


AARWeatherUpdate::AARWeatherUpdate()
{
  _time = 0.0f;
  _previousMessage = -1;
}

AARWeatherUpdate::AARWeatherUpdate(const ApplyWeatherMessage *weather,float time)
{
  _time = time;
  _previousMessage = -1;

  WeatherUpdateStruct &data = (WeatherUpdateStruct&) _data;

  data._seaLevelOffset = weather->_seaLevelOffset;
  data._overcastSetSky = weather->_overcastSetSky;
  data._overcastSetClouds = weather->_overcastSetClouds;
  data._fogSet = weather->_fogSet;
  data._cloudsPos = weather->_cloudsPos;
  data._cloudsAlpha = weather->_cloudsAlpha;
  data._cloudsBrightness = weather->_cloudsBrightness;
  data._cloudsSpeed = weather->_cloudsSpeed;
  data._skyThrough = weather->_skyThrough;
  data._rainDensity = weather->_rainDensity;
  data._rainDensityWanted = weather->_rainDensityWanted;
  data._rainDensitySpeed = weather->_rainDensitySpeed;
  data._rainNextChange = weather->_rainNextChange;
  data._lastWindSpeedChange = weather->_lastWindSpeedChange;
  data._gustUntil = weather->_gustUntil;
  data._windSpeed = weather->_windSpeed;
  data._currentWindSpeed = weather->_currentWindSpeed;
  data._currentWindSpeedSlow = weather->_currentWindSpeedSlow;
  data._gust = weather->_gust;
  data._year = weather->_year;
  data._month = weather->_month;
  data._day = weather->_day;
  data._hour = weather->_hour;
  data._minute = weather->_minute;
}

void AARWeatherUpdate::Serialize(SerializeBinStream &f)
{
  WeatherUpdateStruct &data = (WeatherUpdateStruct&) _data;

  // Save the message header
  if(f.IsSaving())
  {
    // write out what type this is, if saving
    AARMessageHeader header(enumWeatherUpdate,-1);
    header.Serialize(f);
  }

  // load the message
  AARMessageBase::Serialize(f);

  f.TransferBinary(data._seaLevelOffset);
  f.TransferBinary(data._overcastSetSky);
  f.TransferBinary(data._overcastSetClouds);
  f.TransferBinary(data._fogSet);
  f.TransferBinary(data._cloudsPos);
  f.TransferBinary(data._cloudsAlpha);
  f.TransferBinary(data._cloudsBrightness);
  f.TransferBinary(data._cloudsSpeed);
  f.TransferBinary(data._skyThrough);
  f.TransferBinary(data._rainDensity);
  f.TransferBinary(data._rainDensityWanted);
  f.TransferBinary(data._rainDensitySpeed);
  f.TransferBinary(data._rainNextChange);
  f.TransferBinary(data._lastWindSpeedChange);
  f.TransferBinary(data._gustUntil); 
  for( int i = 0; i < 3; ++i) 
  {
    f.TransferBinary(data._windSpeed[i]);
    f.TransferBinary(data._currentWindSpeed[i]);
    f.TransferBinary(data._currentWindSpeedSlow[i]);
    f.TransferBinary(data._gust[i]);
  }
  f.TransferBinary(data._year);
  f.TransferBinary(data._month);
  f.TransferBinary(data._day);
  f.TransferBinary(data._hour);
  f.TransferBinary(data._minute);
}

// Allways pass, not much data checking needs to be done
void AARWeatherUpdate::PreProccess(int messageIndex)
{
  // search for the previous message
  for(int i = (messageIndex - 1); i >= 0; --i)
  {
    Ref<AARMessageBase> msg = GAAR._messages[i]->_message;
    if(!msg) continue;
    if(msg->TypeIs() == enumWeatherUpdate)
    {
      _previousMessage = i;
      break;
    }
  }

  _passPP = true;
}

// Apply the weather
void AARWeatherUpdate::RunMessage()
{
  ApplyWeatherMessage weather;

  weather._seaLevelOffset = _data._seaLevelOffset;
  weather._overcastSetSky = _data._overcastSetSky;
  weather._overcastSetClouds = _data._overcastSetClouds;
  weather._fogSet = _data._fogSet;
  weather._cloudsPos = _data._cloudsPos;
  weather._cloudsAlpha = _data._cloudsAlpha;
  weather._cloudsBrightness = _data._cloudsBrightness;
  weather._cloudsSpeed = _data._cloudsSpeed;
  weather._skyThrough = _data._skyThrough;
  weather._rainDensity = _data._rainDensity;
  weather._rainDensityWanted = _data._rainDensityWanted;
  weather._rainDensitySpeed = _data._rainDensitySpeed;
  weather._rainNextChange = _data._rainNextChange;
  weather._lastWindSpeedChange = _data._lastWindSpeedChange;
  weather._gustUntil = _data._gustUntil;
  weather._windSpeed = _data._windSpeed;
  weather._currentWindSpeed = _data._currentWindSpeed;
  weather._currentWindSpeedSlow = _data._currentWindSpeedSlow;
  weather._gust = _data._gust;
  weather._year = _data._year;
  weather._month = _data._month;
  weather._day = _data._day;
  weather._hour = _data._hour;
  weather._minute = _data._minute;

  // apply the weather to the world
  GLandscape->ApplyWeatherMsg(weather);
}

AARAnimationPhase::AARAnimationPhase()
{
  _time = 0.0f;
  _previousMessage = -1;
  _idIndex = -1;
}

AARAnimationPhase::AARAnimationPhase(const EntityAnimationPhase &data, float time ):
_data(data)
{
  _time = time;
  _previousMessage = -1;
  _idIndex = -1;
}

void AARAnimationPhase::Serialize(SerializeBinStream &f)
{
  EntityAnimationPhase &data = (EntityAnimationPhase&) _data;

  // Save the message header
  if(f.IsSaving())
  {
    // write out what type this is, if saving
    AARMessageHeader header(enumAnimationPhase,-1);
    header.Serialize(f);
  }

  // load the message
  AARMessageBase::Serialize(f);
  
  // Save the object
  f << data.animation;
  SerializeEntityId(data.obj,f);
  f.TransferBinary(data.phase);

}


void AARAddBookMark::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(TypeIs(),-1);
    header.Serialize(f);
  }

  // load the message
  AARMessageBase::Serialize(f);
  f << _name;
  f.TransferBinary(_jumpTo);
  f << _message;
}

void AARDeleteBookMark::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(TypeIs(),-1);
    header.Serialize(f);
  }

  // load the message
  AARMessageBase::Serialize(f);
  f << _name;
}


void AARAnimationPhase::PreProccess(int messageIndex)
{
  EntityAnimationPhase &data = (EntityAnimationPhase) _data;

  FindEntityID findId(data.obj);
  GAAR._entityLifeSpan.ForEachF(findId);
  _idIndex = findId.GetResult();

  if((_idIndex != -1) || (data.obj.ApplicationNumber == STATIC_OBJECT))
  {
    for(int i = (messageIndex - 1); i >= 0; --i)
    {
      Ref<AARMessageBase> msg = GAAR._messages[i]->_message;
      if(!msg) continue;
      if(msg->TypeIs() == TypeIs())
      {
        AARAnimationPhase *preAnim = static_cast<AARAnimationPhase*>(msg.GetRef());
        if(_idIndex == preAnim->_idIndex)
        {
          _previousMessage = i;
          break;
        }
      }
    }
  }
  else
  {
    // in developer build, this should never happen during recording
    Assert(false);
    _passPP = false;
  }
}

void AARAnimationPhase::RunMessage()
{
  if(_idIndex >= 0)
  {
    ObjectLifeSpan &entLifeSpan = GAAR.GetLifeSpan(_idIndex);
    EntityAI *entAI = GAAR.GetEntityAI(entLifeSpan.GetNewId());

    // set the animation phase for this object. 
    if(entAI)
      entAI->SetAnimationPhase(_data.animation,_data.phase);
  }
  else if(_data.obj.ApplicationNumber == STATIC_OBJECT)
  {
    NetworkObject *nObject = _hla.GetNetObjFromHLAId(_data.obj);
    Entity *obj = dyn_cast<Entity>(nObject);
    
    if(obj)
      obj->SetAnimationPhase(_data.animation,_data.phase);
  }
}

AARBriefingData::AARBriefingData(){};
AARBriefingData::AARBriefingData(const RString &data, float time){ _data = data;_time = time; };

void AARBriefingData::Serialize(SerializeBinStream &f)
{
  // Save the message header
  if(f.IsSaving())
  {
    // write out what type this is, if saving
    AARMessageHeader header(enumBriefingData,-1);
    header.Serialize(f);
  }

  // load the data
  AARMessageBase::Serialize(f);
  f << _data;
}

// no pre-proccessing required
void AARBriefingData::PreProccess(int messageIndex){}


bool LandScapeRemoveAnimals::operator () (Entity *obj) const 
{ 
  Crater *explosion = dyn_cast<Crater>(obj);
  if(explosion)
    _objects.Add(obj);

  return false;
};


void BookMarkLifeSpan::SerializeBin(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    f << _name;
    f.TransferBinary(_jumpTo);
    f << _message;

    base::SerializeBin(f);
  }
  else
  {
    f << _name;
    f.TransferBinary(_jumpTo);
    f << _message;

    base::SerializeBin(f);
  }
}

void AARUnitDeleteData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(TypeIs(),-1);
    header.Serialize(f);
  }
  SerializeEntityId(_data,f);
}

void AARGroupDeleteData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(TypeIs(),-1);
    header.Serialize(f);
  }
  SerializeEntityId(_data,f);
}

void AARCenterDeleteData::Serialize(SerializeBinStream &f)
{
  if(f.IsSaving())
  {
    AARMessageHeader header(TypeIs(),-1);
    header.Serialize(f);
  }
  SerializeEntityId(_data,f);
}

void SerializeEntityCreateData(const EntityCreateData &input,SerializeBinStream &f)
{
  EntityCreateData &data = (EntityCreateData&) input;

  if(f.IsSaving())
  {
    SerializeEntityId(data.Id,f);
    SerializeGeneric3DType(data.Pos,f);
    SerializeCharArray(data.EntityType,sizeof(data.EntityType),f);
    SerializeEntityId(data.VehicleId,f);
    f.TransferBinary(data.EPos);
    f.TransferBinary(data.Dimensions);
    f.TransferBinary(data.Side);
    SerializeCharArray(data.Name,sizeof(data.Name),f);
    SerializeCharArray(data.Profile,sizeof(data.Profile),f);
    f.TransferBinary(data.CrewIndex);
  }
  else
  {
    SerializeEntityId(data.Id,f);
    SerializeGeneric3DType(data.Pos,f);
    SerializeCharArray(data.EntityType,sizeof(data.EntityType),f);
    SerializeEntityId(data.VehicleId,f);
    f.TransferBinary(data.EPos);
    f.TransferBinary(data.Dimensions);
    f.TransferBinary(data.Side);
    SerializeCharArray(data.Name,sizeof(data.Name),f);

    if(GAAR.GetVersion() >= 8 )
      SerializeCharArray(data.Profile,sizeof(data.Profile),f);
    else
      data.Profile[0] = 0;

    f.TransferBinary(data.CrewIndex);
  }
}

void SerializeCenterCreateData(const CenterCreateData &input,SerializeBinStream &f)
{
  CenterCreateData &data = (CenterCreateData&) input;
  if(f.IsSaving())
  {
    SerializeEntityId(data.Id,f);
    f.TransferBinary(data.Side);
  }
  else
  {
    SerializeEntityId(data.Id,f);
    f.TransferBinary(data.Side);
  }
}

void SerializeUpdateGroupData(const UpdateGroupData &input,SerializeBinStream &f)
{
  UpdateGroupData &data = (UpdateGroupData&)input;

  if(f.IsSaving())
  {
    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.CenterId,f);
    f.TransferBinary(data.Number);
    SerializeEntityId(data.LeaderId,f);
  }
  else
  {
    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.CenterId,f);
    f.TransferBinary(data.Number);
    SerializeEntityId(data.LeaderId,f);
  }
}

void SerializeGroupCreateData(const GroupCreateData &input,SerializeBinStream &f)
{
  GroupCreateData &data = (GroupCreateData&)input;

  if(f.IsSaving())
  {
    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.CenterId,f);
    f.TransferBinary(data.Number);
    SerializeEntityId(data.LeaderId,f);
  }
  else
  {
    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.CenterId,f);
    f.TransferBinary(data.Number);
    SerializeEntityId(data.LeaderId,f);
  }
}

void SerializeUnitUpdateData(const UnitUpdateData &input,SerializeBinStream &f)
{
  UnitUpdateData &data = (UnitUpdateData&) input;

  if(f.IsSaving())
  {
    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.PersonId,f);
    SerializeEntityId(data.GroupId,f);
    f.TransferBinary(data.Number);
  }
  else
  {
    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.PersonId,f);
    SerializeEntityId(data.GroupId,f);
    f.TransferBinary(data.Number);
  }
}

void SerializeUnitCreateData(const UnitCreateData &input,SerializeBinStream &f)
{
  UnitCreateData &data = (UnitCreateData&) input;

  if(f.IsSaving())
  {
    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.PersonId,f);
    SerializeEntityId(data.GroupId,f);
    f.TransferBinary(data.Number);
  }
  else
  {
    SerializeEntityId(data.Id,f);
    SerializeEntityId(data.PersonId,f);
    SerializeEntityId(data.GroupId,f);
    f.TransferBinary(data.Number);
  }
}

void SerializeDoDamage(const DoDamageData& input,SerializeBinStream &f)
{
  DoDamageData &data = (DoDamageData&) input;

  if(f.IsSaving())
  {
    SerializeEntityId(data.Who,f);
    SerializeEntityId(data.Killer,f);
    f.TransferBinary(data.DamageAmount);
    SerializeCharArray(data.Ammo,sizeof(data.Ammo),f);
  }
  else
  {
    SerializeEntityId(data.Who,f);
    SerializeEntityId(data.Killer,f);
    f.TransferBinary(data.DamageAmount);
    SerializeCharArray(data.Ammo,sizeof(data.Ammo),f);
  }
}


void SerializeEntityUpdateData(const EntityUpdateData &input,SerializeBinStream &f)
{
  EntityUpdateData &data = (EntityUpdateData&) input;

  if(f.IsSaving())
  {
    // save the new format...
    SerializeEntityId(data.Id,f);
    SerializeGeneric3DType(data.Pos,f);
    SerializeGeneric3DType(data.Dir,f);
    SerializeGeneric3DType(data.Up,f);
    SerializeGeneric3DType(data.Velocity,f);
    SerializeGeneric3DType(data.Acceleration,f);
    f.TransferBinary(data.Damage);
    //f.TransferBinary(data.FirePowerDisabled); phased out version 7
    //f.TransferBinary(data.MobilityDisabled);  phased out version 7
    //f.TransferBinary(data.Destroyed); phased out version 7
    f.TransferBinary(data.HasTurret);
    f.TransferBinary(data.Azimuth);
    f.TransferBinary(data.Elevation);
    f.TransferBinary(data.AzimuthWanted);
    f.TransferBinary(data.ElevationWanted);
    //f.TransferBinary(data.LightOn); phased out version 7
    f.TransferBinary(data.Appearance);
    f.TransferBinary(data.Lights);
    //f.TransferBinary(data.PowerPlantOn);     phased out version 7
    //f.TransferBinary(data.FlamesPresent);    phased out version 7
    //f.TransferBinary(data.SmokePlumePresent);phased out version 7
    SerializeEntityId(data.VehicleId,f);
    f.TransferBinary(data.EPos);
    f.TransferBinary(data.Stance);
    f.TransferBinary(data.PrWeaponPosture);
    f.TransferBinary(data.CrewIndex);
    f.TransferBinary(data.SelectedWeapon);
    SerializeCharArray(data.URN,sizeof(data.URN), f);
  }
  else
  {
    SerializeEntityId(data.Id,f);
    SerializeGeneric3DType(data.Pos,f);
    SerializeGeneric3DType(data.Dir,f);
    SerializeGeneric3DType(data.Up,f);
    SerializeGeneric3DType(data.Velocity,f);
    if(GAAR.GetVersion() >= 7 )
      SerializeGeneric3DType(data.Acceleration,f);

    f.TransferBinary(data.Damage);
    if(GAAR.GetVersion() < 7 )
    {
      bool firePowerDis = false;
      bool mobilityDis  = false;
      bool destoryed    = false;

      f.TransferBinary(firePowerDis);
      f.TransferBinary(mobilityDis);
      f.TransferBinary(destoryed);

      if(firePowerDis) data.Appearance |= LVC_FIREPOWER_DISABLED;
      if(mobilityDis)  data.Appearance |= LVC_MOBILITY_DISABLED;
      if(destoryed)    data.Appearance |= LVC_DESTROYED;
    }   

    f.TransferBinary(data.HasTurret);
    f.TransferBinary(data.Azimuth);
    f.TransferBinary(data.Elevation);
    if(GAAR.GetVersion()>3)
    {
      f.TransferBinary(data.AzimuthWanted);
      f.TransferBinary(data.ElevationWanted);
    }
    else
    {
      data.AzimuthWanted = data.Azimuth;
      data.ElevationWanted = data.Elevation;
    }


    if(GAAR.GetVersion() < 7)
    {
      bool lightsOn          = false;
      bool powerPlantOn      = false;
      bool flamesPresent     = false;
      bool smokePresent = false;

      f.TransferBinary(lightsOn);
      f.TransferBinary(powerPlantOn);
      f.TransferBinary(flamesPresent);
      f.TransferBinary(smokePresent);
      
      if(lightsOn)     data.Lights     |= LVC_LIGHT_HEAD;
      if(powerPlantOn) data.Appearance |= LVC_POWER_PLANT_ON;
      if(flamesPresent)data.Appearance |= LVC_FLAMES_PRESENT;
      if(smokePresent) data.Appearance |= LVC_SMOKE_PLUME;
    }
    else
    {
      f.TransferBinary(data.Appearance);
      f.TransferBinary(data.Lights);
    }
    SerializeEntityId(data.VehicleId,f);
    f.TransferBinary(data.EPos);
    f.TransferBinary(data.Stance);
    f.TransferBinary(data.PrWeaponPosture);
    f.TransferBinary(data.CrewIndex);
    f.TransferBinary(data.SelectedWeapon);
    if(GAAR.GetVersion()>4)
      SerializeCharArray(data.URN,sizeof(data.URN), f);
  }
}

void SerializeFireWeaponData(const FireWeaponData &input,SerializeBinStream &f)
{
  FireWeaponData &data = (FireWeaponData&) input;

  if(f.IsSaving())
  {
    SerializeEntityId(data.FiringId,f);
    SerializeEntityId(data.TargetId,f);
    SerializeEntityId(data.MunitionId,f);
    f.TransferBinary(data.WeaponId);
    SerializeGeneric3DType(data.Pos,f);
    SerializeGeneric3DType(data.Velocity,f);
    SerializeCharArray(data.MunitionType,sizeof(data.MunitionType),f);
    f.TransferBinary(data.TimeToLive);
  }
  else
  {
    SerializeEntityId(data.FiringId,f);
    SerializeEntityId(data.TargetId,f);
    SerializeEntityId(data.MunitionId,f);
    f.TransferBinary(data.WeaponId);
    SerializeGeneric3DType(data.Pos,f);
    SerializeGeneric3DType(data.Velocity,f);
    SerializeCharArray(data.MunitionType,sizeof(data.MunitionType),f);
    f.TransferBinary(data.TimeToLive);
  }
}

void SerializeDetonationData(const DetonationData &input,SerializeBinStream &f)
{
  DetonationData &data = (DetonationData&) input;

  if(f.IsSaving())
  {
    SerializeEntityId(data.FiringId,f);
    SerializeEntityId(data.TargetId,f);
    SerializeEntityId(data.MunitionId,f);
    SerializeGeneric3DType(data.Velocity,f);
    SerializeGeneric3DType(data.Pos,f);
    SerializeGeneric3DType(data.RelPos,f);
    SerializeCharArray(data.MunitionType,sizeof(data.MunitionType),f);
  }
  else
  {
    SerializeEntityId(data.FiringId,f);
    SerializeEntityId(data.TargetId,f);
    SerializeEntityId(data.MunitionId,f);
    SerializeGeneric3DType(data.Velocity,f);
    SerializeGeneric3DType(data.Pos,f);
    SerializeGeneric3DType(data.RelPos,f);
    SerializeCharArray(data.MunitionType,sizeof(data.MunitionType),f);
  }
}

//! Added Entity - Serialization of primitive AAR types
//  requires to be updated when newer version comes out.
void SerializeEntityId(const EntityId &input,SerializeBinStream &f)
{
  EntityId &data = (EntityId&) input;

  if(f.IsSaving())
  {
    f.TransferBinary(data.ApplicationNumber);
    f.TransferBinary(data.EntityNumber);
  }
  else
  {
    f.TransferBinary(data.ApplicationNumber);
    f.TransferBinary(data.EntityNumber);
  }
}

/**
* copies the static sized array to SerializedBinStream. Will
* not write out full length if null terminating string is detected
* before the end.
*/
void SerializeCharArray(const char *input,int arraySize, SerializeBinStream &f)
{
  Assert(input);
  char *data = (char *) input;

  if(f.IsSaving())
  {
    for( int i = 0; i < arraySize; i++ )
    {
      if(data[i]=='\0')
      {
        f.SaveChar(data[i]);
        break; // break out of loop,
      }
      else
      {
        // we've reached the end, append null terminator
        if(i == arraySize-1)
        {
          if(data[i] != '\0')
          {
            Assert(false);// should really never be called
          }

          f.SaveChar('\0');
        }
        else
          f.SaveChar(data[i]);
      }
    }
  }
  else
  {
    for(int i = 0; i < arraySize; i++ )
    {
      char newChar = f.LoadChar();

      if(newChar == '\0')
      {
        data[i] = newChar;
        break;
      }
      else
        if(i == arraySize)
        {
          if(newChar != '\0')
          {
            Assert(false);
          }

          data[i] = '\0';
        }
        else
          data[i] = newChar;
    }
  }
}


void SerializeGeneric3DType(const Generic3DType &input,SerializeBinStream &f)
{
  Generic3DType &data = (Generic3DType&) input;

  if(f.IsSaving())
  {
    f.TransferBinary(data.x);
    f.TransferBinary(data.y);
    f.TransferBinary(data.z);
  }
  else
  {
    f.TransferBinary(data.x);
    f.TransferBinary(data.y);
    f.TransferBinary(data.z);
  }
}


#endif
