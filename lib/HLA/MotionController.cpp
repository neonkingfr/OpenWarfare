#include "../wpch.hpp"

#if _MOTIONCONTROLLER
#include "MotionController.hpp"

#include <winsock2.h>

void initNetworkUDP();
void doneNetworkUDP();

MotionController GMotionController; //create one instance

MotionController::MotionController()
{
  sockfd = -1;
  _inMotion = false;
  controlledVehicle = NULL;
}

MotionController::~MotionController()
{
  doneNetworkUDP();
}

bool MotionController::Init(RString IP, int Port, int maxRate)
{

  //TODO: Proper implementation using peerfactory?

  initNetworkUDP(); 

  // create the socket	
  if ((sockfd = socket(PF_INET, SOCK_DGRAM, 0)) == -1) {
    LogF("[MotionController] Error creating socket");
    return false;
  }

  // setup the port and address
  u_long nRemoteAddr = inet_addr(cc_cast(IP));// assign IP address, this address will only be used if the sim and app are running on the same machine
  _ControllerAddr.sin_addr = nRemoteAddr;
  _ControllerAddr.sin_family = AF_INET;			// host byte order
  _ControllerAddr.sin_port = htons(Port);		// short, network byte order
  memset(&(_ControllerAddr.sin_zero), '\0', 8);	// zero the rest of the struct

  LogF("[MotionController] initialized");

  _lastSent = 0;
  _maxRate = maxRate;
  return true;
}

void MotionController::SendData(char *buf, int size) 
{
  int numbytes;
  if ((numbytes = sendto(sockfd, buf, size, 0,
    (struct sockaddr *)&_ControllerAddr, sizeof(struct sockaddr))) == -1) {
      LogF("[motionController] Error sending update");
      return;
  }
}

void MotionController::Update()
{
  MotionPacket simData;                 //:time in milliseconds (to check order)

  if(controlledVehicle)
  {
    Matrix3 orient = controlledVehicle->Orientation();

    Vector3Val dir = orient.Direction();

    float heading = atan2(dir.X(), dir.Z());
    float pitch = atan2(dir.Y(),dir.SizeXZ());
    float roll = atan2(orient.DirectionAside().Y(),orient.DirectionAside().SizeXZ());

    simData.HPR[0] = heading *180./H_PI; //output in deg
    simData.HPR[1] = -pitch *180./H_PI;
    simData.HPR[2] = roll *180./H_PI;

    simData.Accel[0] = controlledVehicle->DirectionWorldToModel(controlledVehicle->Acceleration()).X();
    simData.Accel[1] = controlledVehicle->DirectionWorldToModel(controlledVehicle->Acceleration()).Y();
    simData.Accel[2] = controlledVehicle->DirectionWorldToModel(controlledVehicle->Acceleration()).Z();


    simData.AngVel[0] = controlledVehicle->AngVelocity().X();
    simData.AngVel[1] = controlledVehicle->AngVelocity().Y();
    simData.AngVel[2] = controlledVehicle->AngVelocity().Z();

    simData.Vel[0] = controlledVehicle->ModelSpeed().X();
    simData.Vel[1] = controlledVehicle->ModelSpeed().Y();
    simData.Vel[2] = controlledVehicle->ModelSpeed().Z();

    simData.Pos[0] = controlledVehicle->Position().X();
    simData.Pos[1] = controlledVehicle->Position().Y();
    simData.Pos[2] = controlledVehicle->Position().Z();

    simData.MOTION = _inMotion;           // RUN = true / STOP = false
  }
  else
  {
    memset(&simData, 0, sizeof(simData));
    simData.MOTION = false;
  }

  unsigned long GlobalTickCount();
  unsigned long actTick = GlobalTickCount();
  
  //Limit Rate
  if(_maxRate == 0 || actTick - _lastSent < 1000/_maxRate)
    return;
    
  simData.time = (_lastSent = actTick);    

  // send data to Titan (App)
  SendData((char *)&simData, sizeof(MotionPacket));
}

/*
TODO: place in thread?
HANDLE handle = CreateThread
(
 NULL,64*1024,DownloadToFileThread,context,0,&threadId
 );
 _thread = ::CreateThread(NULL,16*1024,DoWorkCallback,this,0,&threadId);

*/
#endif //_MOTIONCONTROLLER