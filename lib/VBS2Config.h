// Configuration parameters for VBS2

#define _VERIFY_KEY                   0
#define _VERIFY_CLIENT_KEY  			    0  // server is checking the CD key of clients on GameSpy
#define _VERIFY_BLACKLIST					    0

#define _ENABLE_EDITOR                1 // mission editor
#define _ENABLE_WIZARD                0

#define _ENABLE_ALL_MISSIONS          0

#define _ENABLE_MP                    1 // multiplayer
#define _ENABLE_AI                    1
#define _ENABLE_CAMPAIGN              0
#define _ENABLE_SINGLE_MISSION        1 // single missions
#define _ENABLE_UNSIGNED_MISSIONS	    1

//#define _ENABLE_BRIEF_EQ            1
#define _ENABLE_BRIEF_GRP             1
#define _ENABLE_PARAMS                1

#define _ENABLE_CHEATS                !_SUPER_RELEASE

#define _FORCE_DEMO_ISLAND            0
#define _FORCE_SINGLE_VOICE           0
#define _ENABLE_AIRPLANES             1
#define _ENABLE_HELICOPTERS           1
#define _ENABLE_SHIPS                 1
#define _ENABLE_CARS                  1
#define _ENABLE_TANKS                 1
#define _ENABLE_MOTORCYCLES           1
#define _ENABLE_PARACHUTES            1
#define _ENABLE_DATADISC              1
#define _ENABLE_VBS                   1
#define _FORCE_DS_CONTEXT             1
#define _ENABLE_GAMESPY               0
#define _USE_FCPHMANAGER              1
#define _ENABLE_DX10                  0
#define _USE_BATTL_EYE_SERVER         1
#define _USE_BATTL_EYE_CLIENT         1
#define _ENABLE_STEAM                 0

#define _ENABLE_NEWHEAD               0
#define _ENABLE_SKINNEDINSTANCING     0
#define _ENABLE_BB_TREES              0
#define _ENABLE_CONVERSATION          1
#define _ENABLE_SPEECH_RECOGNITION    0
#define _ENABLE_EDITOR2               1
#define _ENABLE_EDITOR2_MP            0
#define _ENABLE_IDENTITIES            1
#define _ENABLE_INVENTORY             0
#define _ENABLE_MISSION_CONFIG        1
#define _ENABLE_INDEPENDENT_AGENTS    1
#define _ENABLE_DIRECT_MESSAGES       0
#define _ENABLE_FILE_FUNCTIONS        1
#define _ENABLE_HAND_IK               1
#define _ENABLE_WALK_ON_GEOMETRY      0
#define _ENABLE_BULDOZER              1
#define _ENABLE_ADDONS                1
#define _ENABLE_COMPILED_SHADER_CACHE 1
#define _ENABLE_MAIN_MENU_TABS        1                         // support for main menu tabs
#define _ENABLE_DEVELOPER             _ENABLE_CHEATS            //give the ability to disable alpha content in a _ENABLE_CHEATS build
#define _MACRO_CDP                    0
#define _ENABLE_DISTRIBUTIONS         0

// -------------------------------------------------------------------------------------------------------------------------

#define _VBS2                                     1 // fix used because of visual studio intelliSene not recognizing 
#define _VBS3                                     1

#define _ENABLE_DEDICATED_SERVER                  !_VBS2_LITE
#define _AAR                                      !_VBS2_LITE
#define _DISABLE_CRC_PROTECTION                   !_VBS2_LITE
#define _HLA                                      !_VBS2_LITE
#define _EXT_CTRL                                 _AAR || _HLA

#define _HUD_DRAW_POLYGON                         1

#define _TIME_EXPIRY                              !_ENABLE_DEVELOPER // enables HASP testing, also checks for expiry, no harm to include it!

#define _VBS2_MODIFY_WORLD                        0                 // allow to change the world in 3D Editor

#define _VBS3_SHAPEFILES                          1                 //enable to load shapefiles into VBS

#define _VBS2_FORCE_RENDER                        1                 // continue rendering, even if application looses the focus

#define _VBS_WP_SYNCH                             1                 // improve scripted synchronisation of waypoints
#define _ENABLE_CARRY_BODY                        1                 //BattlefieldClearance
#define _VBS3_WEAPON_ON_BACK                      1                 //Weapon on Back action (action is defined with Carry body)

#define _VBS_TRACKING                             _VBS3_AVRS        //! Tracking module used for loadmaster project
#define _VBS3_ROPESIM                             1
#define _VBS3_BOATPHYSICS                         1

#define _VBS3_SOLDIERNAMES                        1
#define _VBS3_CONSTANT_CRATERS_LIFETIME           3*60*60           //force a constant lifetime for big craters
#define _VBS3_RANKS                               1                 //Config defined Ranks, displayed in HUD and UI
#define _VBS3_CRATERS_FIXEDPOLYGONSPLIT           1                 // This is essential part of craters - polygon split doesn't produce corrupted geometry anymore
#define _VBS3_CRATERS_DEFORM_TERRAIN              1                 //craters will change terrain heightfield
#define _VBS3_CRATERS_LATEINIT                    1
#define _VBS3_CRATERS_DEFORM_TERRAIN_SHADOWS      0
#define _VBS3_CRATERS_LIMITCOUNT                  1
#define _VBS3_CRATERS_BIAS                        1                 // Bias introduced to craters, solving z-fighting artefact
#define _VBS3_LODTRANSITIONCONTROL                1                 // Transition speed of tree LOD blending can be controlled by the core config value "lodTransitionSpeed" now
#define _VBS3_MODELCRATERDISTANCE                 1                 // Distance of crater on model drawing now depends on zoom (snipers can see craters in distance)
#define _VBS3_OBJECTSNOTRESETEDAFTERRESTARTFIX    1                 // TODO: Ondrej can use it in original project and remove this define
#define _VBS3_FIX_CLUTTERUPDATECALLEDTWICE        1                 // TODO: Ondrej can use it in original project and remove this define
#define _VBS3_FLAREFIX                            1
#define _VBS3_VEHICLESBURNINGUNDERWATERFIX        1
//#define _VBS3_TI                                  1
#define _VBS3_SM2                                 1             
#define _VBS3_NPFLT                               1                 // Near plane floating - this solves z-fighting in most scenarios
#define _VBS3_LASE_LEAD                           1
#define _VBS3_UDEFCHART                           1                 // User defined charts enabling
#define _VBS3_MAPGRIDIMPROVED                     1                 // Improved version of map grid drawing
#define _VBS3_SCREENSHOT                          1                 // Screenshot grabbing code
#define _VBS3_PERPIXELPSLIGHTS                    1                 // Per pixel P&S lights
#define _VBS3_CHEAT_DIAG                          !_VBS2_LITE       //enables diagdraw and diagenable commands 
#define _VBS3_DEADENEMYDISABLESVEHICLEFIX         1                 // Fixes problem that it was not possible to get into vehicle with dead body of the enemy
#define _VBS3_MAPRESETSGROUPSFIX                  1                 // Fixes problem - entering map during mission resets groups
#define _VBS3_DIFFICULTIESFLAGDEFAULT             1                 // Makes CfgDifficulties.Flags not mandatory
#define _VBS3_BIASFIX                             1                 // Bias multiplication by clipNear removed, now bias depends on z-buffer precision
#define _VBS3_INVENTORY                           1                 //replaced Arma2 inventory with scripted version
#define _VBS3_DISABLE_RENEGADE                    1                 //diable renegade mode

#define _MOTIONCONTROLLER                         _LASERSHOT

#define _TIME_ACC_MIN                             (1.0 / 128.0)
#define _TIME_ACC_MAX                             16.0

#define _SPACEMOUSE                               1


// -------------------------------------------------------------------------------------------------------------------------

// Registry key for saving player's name, IP address of server etc.
#undef ConfigApp
#define ConfigApp "Software\\BIS\\VBS2_VTK"

#define _MOD_PATH_DEFAULT "ca;vbs2;mycontent"

// CD Key parameters

const bool CDKeyRegistryUser = false;
const unsigned char CDKeyPublicKey[] =
{
  0x11, 0x00, 0x00, 0x00,
  0x2F, 0xAD, 0xE2, 0xFA, 0x1A, 0xA2, 0x60, 0x95, 0xEB, 0x81, 0x76, 0xDB, 0xCC, 0xE3, 0x50
};
const char CDKeyFixed[] = "BIS Internal.";

const int CDKeyIdLength = (sizeof(CDKeyPublicKey) / sizeof(*CDKeyPublicKey) - 4) - strlen(CDKeyFixed);
