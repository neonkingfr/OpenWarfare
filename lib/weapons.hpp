#ifdef _MSC_VER
#pragma once
#endif

#ifndef _WEAPONS_HPP
#define _WEAPONS_HPP

#include "wpch.hpp"
#include "paramFileExt.hpp"
#include "vehicle.hpp"
#include "animation.hpp"
#if _ENABLE_HAND_IK
#include "rtAnimation.hpp"
#endif
#include "camShake.hpp"

#if _VBS3 //added muzzle events
  #include <El/Enum/enumNames.hpp> 
  #include <El/Evaluator/expressImpl.hpp>
#endif

enum AmmoSimulation
{ // basic ammo types
  AmmoNone,
  AmmoShotShell,
  AmmoShotMissile,
  AmmoShotRocket,
  AmmoShotBullet,
  AmmoShotSpread,
  AmmoShotIlluminating,
  AmmoShotSmoke,
  AmmoShotTimeBomb,
  AmmoShotPipeBomb,
  AmmoShotMine,
  AmmoShotStroke,
  AmmoShotLaser, //!< laser designator
  AmmoShotCM, //!< AA counter Measures
  AmmoShotMarker
};

struct SoundProbab: public SoundPars
{
  float _probability;
};

TypeIsMovable(SoundProbab);

class RandomSound
{
  AutoArray<SoundProbab> _pars;

  public:
  void Load( ParamEntryPar base, const char *name );
  const SoundPars &SelectSound( float probab ) const;
  bool PreloadSounds() const;
};

// information about post effect used for optics
struct PPEffectType
{
  // 
  RString _effectType;
  int _priority;
  // parameters
  AutoArray<float> _pars;

  void Load(ParamEntryPar cfg);
};

TypeIsMovable(PPEffectType);

struct HitEffectsInfo
{
  RString _name;
  RString _effects;
};
TypeIsMovableZeroed(HitEffectsInfo)

/// params for camera shake - used for reading values from config
class CameraShakePars
{
public:
  CameraShakePars():_isValid(false), _power(0.0f), _duration(0.0f), _frequency(0), _distance(0.0f), _minSpeed(0.0f) {}

  /// reads values from given cfg
  void Load(ParamEntryPar cfg);

  bool IsValid() const {return _isValid;}
  float GetPower() const {return _power;}
  float GetDuration() const {return _duration;}
  float GetFrequency() const {return _frequency;}
  float GetDistance() const {return _distance;}
  float GetMinSpeed() const {return _minSpeed;}

  /// returns if power is non zero
  bool HasPower() const
  {
    static const float epsilon = 0.0001f; // just a very small number for testing
    return _power > epsilon;
  }
  /// returns if distance is non zero
  bool HasDistance() const
  {
    static const float epsilon = 0.0001f; // just a very small number for testing
    return _distance > epsilon;
  }

private:
  /// if this is valid (it was read from cfg)
  bool _isValid;
  /// max power of shake (is decreased based on distance of player from shake)
  float _power;
  /// max duration of shake (in s) (is decreased based on distance of player from shake)
  float _duration;
  /// frequency of camera changes in shakes (number of changes per second) - if 0, then default value from CameraShakeManager is used
  float _frequency;
  /// max distance from player to produce the shake (in m)
  float _distance;
  /// min speed of vehicle to produce shake effect
  float _minSpeed;
};


#if _VBS3
#define AMMO_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, AmmoHit) /*string:weapon,string:muzzle,string:mode,string:ammo*/ \
  XX(type, prefix, AmmoExplode) /*string:weapon,string:muzzle,string:mode,string:ammo*/ 

#ifndef DECL_ENUM_MUZZLE_EVENT_ENUM
# define DECL_ENUM_MUZZLE_EVENT_ENUM
DECL_ENUM(AmmoEvent)
#endif
DECLARE_ENUM(AmmoEvent,AE,AMMO_EVENT_ENUM)
#endif

/// ammunition information
class AmmoType: public EntityType
{
  typedef EntityType base;

#if _VBS3 //Ammo Destroyed event
  protected:
    RString _eventHandlers[NAmmoEvent];
  public:
    //! generic event handler with typical parameter sets
    void OnEvent(AmmoEvent event, const GameValue &pars) const;
    //! check if there is some event handler
    bool IsEventHandler(AmmoEvent event) const;
#endif

public:
  float hit,indirectHit,indirectHitRange;

  float maxControlRange; // missile parameters
  float maneuvrability;
  /// how much of the tracking is done using over-steering
  float trackOversteer;
  /// how much of the tracking is done using computed leading of the target
  float trackLead;
  
  float initTime; //!< time to ignition
  float thrustTime; //!< time of burning
  float thrust; //!< how strong the engine is
  float sideAirFriction; //!< air friction (used for missiles / bombs)
  float explosionTime;
  /// some shots (hand grenades) are fused until they travel a certain distance
  float fuseDistance;
  
  float cost;
  float maxSpeed; // max speed of missiles
  float simulationStep;
  bool irLock; // ir lock only
  bool laserLock; // laser lock only
  bool nvLock; // night vision marker lock only
  bool artilleryLock; // artillery lock only
  bool airLock; // air lock possible
  bool manualControl; // no control out of maxControlRange
  float explosive;
  float caliber;
  /// some bullets can be deflected after a hit
  float deflecting;
  /// slowdown coefficient for deflection (speed will be multiplied by this coef after the ammo deflects from some surface)
  float _deflectionSlowDown;
  float _timeToLive;
  float _airFriction;
  float _coefGravity;
  /// speed at which hit and indirectHit values are valid
  float _typicalSpeed2;

  int weaponLockSystem;
  float cmImmunity;
  
  Ref<Texture> _texture;
  AmmoSimulation _simulation;
  Ref<VehicleType> _cartridgeType; // what kind of cartridges to we use (effect)
  //! shape that should be used when drawing proxy on vehicle
  Ref<LODShapeWithShadow> _proxyShape;

  PackedColor _tracerColor;   //! tracer color with alpha
  PackedColor _tracerColorR;  //!< tracer color in realistic mode

  float audibleFire;
  float visibleFire;
  float visibleFireTime;
#if _VBS3
  float audibleFireTime;
#endif
  // samples group for specified group
  RandomSound _hit[SurfaceInfo::NSoundHitTypes];

  // fake bullets samples
  RandomSound _bulletFly;

  SoundPars _soundFly;
  SoundPars _soundEngine;
  //SoundPars _supersonicCrackNear;
  //SoundPars _supersonicCrackFar;
  
  RString _defaultMagazine;

  RString _explosionEffects;
  RString _craterEffects;
  AutoArray<HitEffectsInfo> _hitEffects;
  
  // smoke trails for rockets
  RString _effectsMissile;
  // smoke effects (only for smokeshell simulation)
  RString _effectsSmoke;

  //@{VBS style tracers
  float _tracerScale;
  float _tracerStartTime;
  float _tracerEndTime;
  bool _nvgOnly;
  //@}

  // whistling radius
  float _whistleDist;
  // 0 .. on fire, 1 .. on explosion, 2 .. both
  int _whistleOnFire;

  //@{Camera shake parameters
  /// on explosion
  CameraShakePars _camShakeExplode;
  /// on weapon fire (general)
  CameraShakePars _camShakeFire;
  /// effect on player, when he fires from the weapon with this ammo 
  CameraShakePars _camShakePlayerFire;
  /// effect on player, when he is hit with this ammo
  CameraShakePars _camShakeHit;
  //@}

  
  AmmoType(ParamEntryPar name);

  void InitShape();
  void DeinitShape();
  bool AbstractOnly() const { return false; }

  virtual void Load(ParamEntryPar par, const char *shape);

  ParamEntryVal ParClass() const {return *_par;}
  ParamEntryVal ParClass(const char *name) const { return (*_par)>>name; }

  RString GetHitEffects(RString name) const;

  const SoundPars& FakeBulletSound(float prob) const { return _bulletFly.SelectSound(prob); }
};

/// information about how explosion / damage caused by ammo
struct HitInfo
{
  float _hit;
  float _indirectHit;
  float _indirectHitRange;
  float _explosive;
#if _VBS2
  int _external; //enumeration is specified in hlabase. required to prevent sending hit events back to HLA
#endif
  /// some information is used from the ammo type
  const AmmoType *_ammoType;
  Vector3 _pos;
  Vector3 _surfNormal;
  Vector3 _inSpeed;
  Vector3 _outSpeed;

  HitInfo(const AmmoType *ammoType, Vector3Par pos, Vector3Par surfNormal, Vector3Par inSpeed, Vector3Par outSpeed=VZero);
};

typedef const HitInfo &HitInfoPar;


class WeaponModeType : public RefCount
{
public:
  ConstParamClassDeepPtr _parClass;
  RStringB _displayName;      // used in InGameUI
  int _mult;                  // take this from ammo store (bullets in small burst)
  int _burst;
  //SoundPars _sound;           // shot sound
  RandomSound _beginSound;
  float _soundDuration;
  float _reloadTime;          // how long it takes to reload
  float _dispersion;          // dispersion
  /// default recoil
  RStringB _recoilName;
  /// recoil when prone
  RStringB _recoilProneName;
  float _aiRateOfFire; // AI rate of fire at given distance
  float _aiRateOfFireDistance;

  bool _soundContinuous;
  /// when burst is fired, only a first bullet is making a sound
  bool _soundBurst;
  bool _autoFire;             // full auto weapons
  bool _useAction;
  bool _showToPlayer;
  RString _useActionTitle;

  float minRange,minRangeProbab;
  float midRange,midRangeProbab;
  float maxRange,maxRangeProbab;

  float invMidRangeMinusMinRange; // precalculated inverse
  float invMidRangeMinusMaxRange;

  //multiplier of dispersion, when 1, there is  1 meter dispersion with every second of flight
  float artilleryDispersion;
  float artilleryCharge;

public:
  WeaponModeType();
  void Init(ParamEntryPar cls);
  const RStringB &GetName() const {return _parClass->GetName();}
  RStringB GetDisplayName() const {return _displayName;}
};

DECL_ENUM(ManAction)

class MagazineSlot;
class WeaponType;
class EntityPlainType;

#include <Es/Memory/normalNew.hpp>

// used for drawing of weapon proxy
class WeaponObject : public ObjectTyped
{
  typedef ObjectTyped base;

protected:
  const WeaponType *_weapon;
  const MagazineSlot *_magazineSlot;
  const AnimationAnimatedTexture *_animFire;
  int _phaseFire;

  //! Heat factor of the gun (note this class doesn't store it, it's lifetime is in scope Init-Clear)
  float _gunHeatFactor;
  // TI params
  float _htMin;
  float _htMax;
  float _afMax;
  float _mfMax;
  float _mFact;
  float _tBody;

public:
  WeaponObject(LODShapeWithShadow *shape, const EntityPlainType *type, const CreateObjectId &id);
  void Init(const WeaponType *weapon, const MagazineSlot *magazineSlot, const AnimationAnimatedTexture *animFire, int phaseFire, float gunHeatFactor);
  void Clear();

  virtual bool PrepareShapeDrawMatrices(Matrix4Array &matrices, const ObjectVisualState &vs, LODShape *shape, int level);

  virtual bool CanBeInstanced(int level, float distance2, const DrawParameters &dp) const;
  virtual AnimationStyle IsAnimated(int level) const;
  virtual void Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2);
  virtual void Deanimate(int level, bool setEngineStuff);

  const EntityPlainType *Type() const
  {
    return static_cast<const EntityPlainType *>(GetEntityType());
  }

  //! Virtual method
  /*!
    Note it is not correct to use metabolism factor for heat caused by firing, because there is a lerp in the calculation instead of addition.
    But we have no choice, as there may be an object that uses movement and alive factors already. The cleanest solution would be to modify
    the pixel shader and decide upon a negative value of the PSC_MFact_TBody_X_HSDiffCoef.x to use addition instead of lerp.
  */
  virtual float GetMetabolismFactor() const;
  __forceinline void SetTIParams(float htMin, float htMax, float afMax, float mfMax, float mFact, float tBody) 
  {
    _htMin = htMin;
    _htMax = htMax;
    _afMax = afMax;
    _mfMax = mfMax;
    _mFact = mFact;
    _tBody = tBody;
  }
  virtual float HTMin() const {return _htMin;}
  virtual float HTMax() const {return _htMax;}
  virtual float AFMax() const {return _afMax;}
  virtual float MFMax() const {return _mfMax;}
  virtual float MFact() const {return _mFact;}
  virtual float TBody() const {return _tBody;}

  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

class MagazineType : public RefCount, public LODShapeLoadHandler
{
public:
  ConstParamClassDeepPtr _parClass;
  int _scope;                 // public / protected for briefing
  RStringB _displayName;      // used in action menu
  RStringB _displayNameShort;  // used in UI vehicle info

  RStringB _picName;
//  RStringB _shortName;        // used in briefing
  RStringB _nameSound;        // used for "LOAD <_nameSound>" sentence
  RStringB _description;      // description for overview / library
  int _magazineType;          // slots occupied
  int _maxAmmo;               // how much ammo is available
  float _maxLeadSpeed;        // max lead
  float _initSpeed;           // initial ammo speed
  float _invInitSpeed;        // initial ammo speed
  bool _quickReload;          //when magazine is changed, weapon is ready to fire
  //! used in MP game for decision which body to hide
  float _value;
#if _VBS2 // fatigue model
  //! mass of magazine
  float _mass;
#endif
  /// max. time of holding throw button
  float _maxThrowHoldTime;
  /// min. coef for throwing
  float _minThrowIntensityCoef;
  /// max. coef for throwing
  float _maxThrowIntensityCoef;

  float TravelSpeed() const
  {
    float travelSpeed = floatMax(_initSpeed,0.1f);
    if (_ammo) saturateMax(travelSpeed,_ammo->maxSpeed);
    return travelSpeed;
  }

  RString _reloadAction;
  RStringB _modelName;
  
  Ref<WeaponObject> _model; // model
  mutable AnimationAnimatedTexture _animFire;
  mutable int _modelRefCount;

  mutable Ref<LODShapeWithShadow> _modelMagazine; // weapon model
  mutable int _magazineShapeRef; // how many times is this type weapon actually used?

  bool _useAction;
  RString _useActionTitle;

  Ref<const AmmoType> _ammo;  // ammo

  //@{ tracers
  int _tracersEvery;
  //specifies if the last rounds are tracers
  int _lastRoundsTracer; 
  //@}

public:
  MagazineType();
  void Init(const char *name);
  const RStringB &GetName() const {return _parClass->GetName();}
  RStringB GetPictureName() const;
  RStringB GetDescription() const {return _description;}

  void InitShape(); // force all used ammo types to load shape
  void DeinitShape(); // allow all used ammo types to release shape

  //@{ LODShapeLoadHandler
  virtual void LODShapeLoaded(LODShape *shape);
  virtual void LODShapeUnloaded(LODShape *shape);
  virtual void ShapeLoaded(LODShape *shape, int level);
  virtual void ShapeUnloaded(LODShape *shape, int level);
  //@} LODShapeLoadHandler
  
  void AmmoAddRef() const; // force all used ammo types to load shape
  void AmmoRelease() const; // allow all used ammo types to release shape

  void InitMagazineShape() const; // force all used ammo types to load shape
  void DeinitMagazineShape() const; // allow all used ammo types to release shape

  void MagazineShapeAddRef() const; // force model to be loaded
  void MagazineShapeRelease() const; // allow model to be released

  LSError Serialize(ParamArchive &ar);
  static MagazineType *CreateObject(ParamArchive &ar);

  // TODO: network transfer

  RStringB GetDisplayName() const {return _displayName;}
  RStringB GetNameSound() const {return _nameSound;}
};
TypeIsMovable(MagazineType)

class WeaponType;

struct CursorTextureSection
{
  float uMin;
  float vMin;
  float uMax;
  float vMax;
  float xOffset;
  float yOffset;
};
TypeIsSimple(CursorTextureSection)

struct CursorTextureInfo
{
  Ref<Texture> texture;
  float fade;
  PackedColor color;
  int shadow;
  AutoArray<CursorTextureSection> sections;

  CursorTextureInfo() {fade = 1.0f; color = PackedWhite; shadow = 2;}
};

#if _VBS3
#define MUZZLE_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, Fired) /*string:weapon,string:muzzle,string:mode,string:ammo*/ 

  #ifndef DECL_ENUM_MUZZLE_EVENT_ENUM
  # define DECL_ENUM_MUZZLE_EVENT_ENUM
  DECL_ENUM(MuzzleEvent)
  #endif
  DECLARE_ENUM(MuzzleEvent,ME,MUZZLE_EVENT_ENUM)
#endif

  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#if _VBS3 //moved struct definition from vehicleAI

  enum CanSee
  {
    CanSeeRadar=1,
    CanSeeEye=2,
    CanSeeOptics=4,
    CanSeeEar=8,
    CanSeeCompass=16,
    CanSeePeripheral=32,
    //CanSeeAll=~0
  };

  enum VisionMode
  {
    VMDv,
    VMNv,
    VMTi,
    VMInvalid
  };

  // camera parameters
  struct ViewPars
  {
    float _initAngleY,_minAngleY,_maxAngleY;
    float _initAngleX,_minAngleX,_maxAngleX;
    float _initFov,_minFov,_maxFov;

    void Load( ParamEntryPar cfg );
    void InitVirtual
      (
      CameraType camType, float &heading, float &dive, float &fov
      ) const;
    void LimitVirtual
      (
      CameraType camType, float &heading, float &dive, float &fov
      ) 
      const;

# if _VBS3
  protected:
    //! Nightvision
    bool _hasNV;
    //! Normal Day Vision
    bool _hasDV;
    //! Class was properly loaded
    bool _initialized;
  public:

    AutoArray <float> _discreteFov;
    AutoArray <int> _thermalModes;

    RString _opticsModel; //stores just the name, also indication for "VBS2 supported viewoptics class"
    PackedColor _opticsColor;

    //specify a CanSee per weapon
    int _canSee;
    void DiscretizeValue(float fov, float &prev, float &closest, float &next) const;
    bool DiscreteValues() const { return _discreteFov.Size() > 0;}
    int GetNextThermalMode(int oldThermalmode) const;

    bool HasNV() const {return _hasNV;}
    bool HasDV() const {return _hasDV;}
    bool Initialized() const {return _initialized;}

    bool HasTI() const {return _thermalModes.Size() > 0;}

    void NextVisionMode(VisionMode& mode, int &tiModeIndex) const;

    bool ApplyVisionMode(VisionMode& mode, int& tiModus) const;

    ViewPars():_initialized(false),_canSee(0),_hasNV(true),_hasDV(true){};
# endif
  };
#endif //!_VBS3 //moved class to weapon.hpp

#if !_VBS3
  #ifndef DECL_ENUM_VISION_MODE
  #define DECL_ENUM_VISION_MODE
    DECL_ENUM(VisionMode)
  #endif

  DEFINE_ENUM_BEG(VisionMode)
    VMNormal, VMNvg, VMTi, VMCount, VMInvalid
  DEFINE_ENUM_END(VisionMode)

  // camera parameters
  struct ViewPars
  {
    float _initAngleY,_minAngleY,_maxAngleY;
    float _initAngleX,_minAngleX,_maxAngleX;
    float _initFov,_minFov,_maxFov;

    AutoArray<int> _visionModes;
    AutoArray<int> _thermalModes;

    bool _directionStabilized;

    RString _opticsDisplayName;

    //optics models
    Ref<LODShapeWithShadow> _gunnerOpticsModel;

    __forceinline bool HasVisionModes() const { return _visionModes.Size() > 0; }
    __forceinline bool HasThermalModes() const { return _thermalModes.Size() > 0; }
    __forceinline int VisionModesCount() const { return _visionModes.Size(); }
    void NextVisionMode(VisionMode& mode, int &tiModeIndex) const;
    void GetInitVisionMode(VisionMode& mode, int &tiModeIndex) const;

    void Load(ParamEntryPar cfg);
    void InitVirtual(CameraType camType, float &heading, float &dive, float &fov) const;
    void LimitVirtual(CameraType camType, float &heading, float &dive, float &fov) const;
  };
  TypeIsMovable(ViewPars);

#endif

/* Hold flash light params */
struct FlashLigthInfo: public RefCount
{
  Color _color;
  Color _colorAmbient;
  RString _positionMem;
  RString _directionMem;
  float _brightness;
  Vector3 _position;
  Vector3 _direction;
  Vector3 _scale;
  float _angle;

  bool Load(ParamEntryPar cls);
  void Init(const LODShapeWithShadow * shape);
};

/* Hold optics settings params */
struct OpticsInfo
{
  Vector3 _cameraDir, _cameraPos;
  bool _useModelOptics;

  AutoArray<PPEffectType> _ppEffectType; 

  bool _opticsFlare;
  float _opticsDisablePeripherialVision;
  float _opticsZoomMin,_opticsZoomMax,_opticsZoomInit;
  float _distanceZoomMin,_distanceZoomMax;
  int _id;

  float GetNormalZoom() const
  {
    // never return value above than 1 if the weapon is capable of 1
    return _opticsZoomMax>1 ? floatMax(_opticsZoomMin,1) : _opticsZoomMax;
  }

  RString _opticsDisplayName;

  //vision modes
  AutoArray<int> _visionModes;
  AutoArray<int> _thermalModes;

  //discrete fov
  AutoArray<float> _discreteFov;
  AutoArray<float> _discreteDistance;
  int _discreteInitIndex;
  int _discreteDistanceInitIndex;

  /// compensation to aim with the _cameraDir, not with the base weapon direction
  float _neutralXRot, _neutralYRot;

  OpticsInfo()
  {
    _cameraPos = VZero;
    _cameraDir = VForward;
    _id = -1;
    _neutralXRot = 0;
    _neutralYRot = 0;
    _discreteInitIndex = 0;
    _discreteDistanceInitIndex = 0;
  }

  __forceinline bool HasVisionModes() const { return _visionModes.Size() > 0; }
  __forceinline int VisionModesCount() const { return _visionModes.Size(); }
  void NextVisionMode(VisionMode& mode, int &tiModeIndex) const;
  void GetInitVisionMode(VisionMode& mode, int &tiModeIndex) const;
  bool HasVisionMode(VisionMode mode) const;
};
TypeIsMovable(OpticsInfo);

class MuzzleType : public RefCount, public LODShapeLoadHandler
{

#if _VBS3 //MuzzleEvent
protected:
  RString _eventHandlers[NMuzzleEvent];
public:
  //! generic event handler with typical parameter sets
  void OnEvent(MuzzleEvent event, const GameValue &pars);
  //! check if there is some event handler
  bool IsEventHandler(MuzzleEvent event) const;
  //! will show the trajectory
  bool _showTrajectory;

#endif

protected:
  CursorTextureInfo _cursorTexture;
  CursorTextureInfo _cursorAimTexture;
  // relative cursor size, 1 is normal
  float _cursorSize;

public:
  ConstParamClassDeepPtr _parClass;
  RStringB _displayName;      // used in InGameUI (when no magazine loaded)
  float _magazineReloadTime;
  SoundPars _sound;           // no shot sound
  SoundPars _reloadSound;
  SoundPars _reloadMagazineSound;
  RandomSound _bullets;
  float _soundDuration; 
  float _reloadSoundDuration; 
  float _reloadMagazineSoundDuration;
  float _aiDispersionCoefX;
  float _aiDispersionCoefY;

  /// shot spread when spread ammo used
  float _shotSpreadAngle;

  void GetDispersionCoefs(float &x, float &y, float invAimingAccuracy) const
  {
    // invAimingAccuracy is assumed to be in range 1..5
    float f = (invAimingAccuracy-1)*0.25f;
    x = f*_aiDispersionCoefX+1;
    y = f*_aiDispersionCoefY+1;
  }
  
  bool _soundContinuous; // weapon sound is continuous
  bool _enableAttack;
  bool _optics;
  bool _showEmpty;
  bool _autoReload;
  bool _backgroundReload;
  //bool _opticsFlare;
  /// 1 = force optics always, 2 = force optics for 1st person view only
  signed char _forceOptics;
  //float _opticsDisablePeripherialVision;
  //weapons also support ViewOptics
#if _VBS3 //weapons also support ViewOptics
  ViewPars  _viewOptics;
#endif

  bool _canAutoAim;
  bool _showAimCursorInternal;
  bool _showSwitchAction;
  
  int _canBeLocked;
  int _ballisticsComputer; // should ballistics computer (and eventually which one) be used?
  int _primary; // primary level

  /// what stance is needed to use the weapon
  bool _useAsBinocular;

//  float _opticsZoomMin,_opticsZoomMax,_opticsZoomInit;
//  float _distanceZoomMin,_distanceZoomMax;

  /// 2D optics model
  Ref<LODShapeWithShadow> _opticsModel;
  
  /// 3D optics enabled
  bool _opticsEnable;

  /// fire animation for 2D optics
  AnimationAnimatedTexture _animFire;

  /// camera position and direction
  //Vector3 _cameraPos, _cameraDir;
  //@{ position and direction of muzzle
  Vector3 _muzzlePos, _muzzleDir, _muzzleEnd;
  // infra red laser position and direction
  Vector3 _irLaserPos, _irLaserDir;

#if _VBS3
  //@{ position of a laser attachment
  Vector3 _laserPos;
#endif

  //@{ position and velocity of outgoing empty cartridge
  Vector3 _cartridgeOutPos, _cartridgeOutVel;
  int _cartridgeOutPosIndex, _cartridgeOutEndIndex;
  //@}


  RefArray<WeaponModeType> _modes;
  RefArray<MagazineType> _magazines;  // which ammo can be used
  Ref<MagazineType> _typicalMagazine; // for type (expected magazine)

  //AutoArray<PPEffectType> _ppEffectType;
  // flash light params
  Ref<FlashLigthInfo> _gunLightInfo;
  //optics settings
  AutoArray<OpticsInfo> _opticsInfo;
  // has any optics Ti
  bool _hasTiVision;

  float _irDistance; // 0 - no IR

public:
  MuzzleType();
  virtual ~MuzzleType();

  void Init(ParamEntryPar cls, const WeaponType *weapon);
  void InitOpticModes(ParamEntryPar cls, const WeaponType *weapon);
  
  virtual void InitShape(const WeaponType *weapon);
  virtual void InitShapeOpticsShape(const WeaponType *weapon);
  virtual void DeinitShape(const WeaponType *weapon);
  void InitShapeLevel(const WeaponType *weapon, int level);
  void DeinitShapeLevel(const WeaponType *weapon, int level);

  //@{ LODShapeLoadHandler
  virtual void LODShapeLoaded(LODShape *shape);
  virtual void LODShapeUnloaded(LODShape *shape);
  virtual void ShapeLoaded(LODShape *shape, int level);
  virtual void ShapeUnloaded(LODShape *shape, int level);
  //@} LODShapeLoadHandler

  bool CanUse(const MagazineType *type) const;

  const RStringB &GetName() const {return _parClass->GetName();}

  virtual float GetCursorSize() const {return _cursorSize;}
  virtual const CursorTextureInfo *GetCursorTexture() const {return &_cursorTexture;}
  virtual const CursorTextureInfo *GetCursorAimTexture(bool laserOn) const {return &_cursorAimTexture;}

  __forceinline bool HasTiVision() const { return _hasTiVision; }
  __forceinline bool HasVisionModes(int opticsMode) const {return _opticsInfo[opticsMode].HasVisionModes();}
  __forceinline int VisionModesCount(int opticsMode) const { return _opticsInfo[opticsMode].VisionModesCount(); }
  void NextVisionMode(VisionMode& mode, int &tiModeIndex, int opticsMode) const;
  void GetInitVisionMode(VisionMode& mode, int &tiModeIndex, int opticsMode) const;
};
TypeIsMovable(MuzzleType)

class MuzzleTypeLaser : public MuzzleType
{
  typedef MuzzleType base;

protected:
  CursorTextureInfo _cursorAimOnTexture;

public:
  virtual void InitShape(const WeaponType *weapon);
  virtual void DeinitShape(const WeaponType *weapon);

  virtual const CursorTextureInfo *GetCursorAimTexture(bool laserOn) const;
};

#define MaskSlotPrimary     0x00000001  // primary weapons
#define MaskSlotSecondary   0x00000004  // secondary weapons
#define MaskSlotItem        0x00000F00  // items
#define MaskSlotBinocular   0x00003000  // binocular
#define MaskHardMounted     0x00010000  // hard mounted
#define MaskSlotHandGun     0x00000002  // hand gun
#define MaskSlotHandGunItem 0x000000F0  // hand gun magazines
#define MaskSlotInventory   0x001E0000  // hand gun magazines

inline int GetItemSlotsCount(int slots)
{
  return (slots & MaskSlotItem) >> 8; // =  / 0x0100 
}

inline int GetHandGunItemSlotsCount(int slots)
{
  return (slots & MaskSlotHandGunItem) >> 4;  // =  / 0x0010
}

inline int GetInventorySlotsCount(int slots)
{
  return (slots & MaskSlotInventory) >> 17; // =  / 0x00020000 
}

/// Weapon simulation (special item type) enum
#define WEAPON_SIMULATION_ENUM(type, prefix, XX) \
  XX(type, prefix, Weapon) /* regular weapon */ \
  XX(type, prefix, Binocular) /* binocular */ \
  XX(type, prefix, NVGoggles) /* night vision goggles */ \
  XX(type, prefix, ItemWatch) \
  XX(type, prefix, ItemCompass) \
  XX(type, prefix, ItemGPS) \
  XX(type, prefix, ItemRadio) \
  XX(type, prefix, ItemMap) \
  XX(type, prefix, CMLauncher) \

#ifndef DECL_ENUM_WEAPON_SIMULATION
#define DECL_ENUM_WEAPON_SIMULATION
DECL_ENUM(WeaponSimulation)
#endif

DECLARE_ENUM(WeaponSimulation, WS, WEAPON_SIMULATION_ENUM)

/*!
\patch 1.22 Date 09/04/2001 by Jirka
- Added: Support for revolving drum for weapons.
*/
class WeaponType: public RefCount, public LODShapeLoadHandler, public AnimatedType
{
public:
  ConstParamClassDeepPtr _parClass;
  int _scope;                 // public / protected for briefing
  RStringB _displayName;      // used in briefing and radio protocol
  RStringB _nameSound;        // used in radio protocol
  RStringB _picName;          // used in briefing
  RStringB _description;      // description for overview / library
  RStringB _modelName;
  
  /// enable to find special items (see WEAPON_SIMULATION_ENUM for the list)
  WeaponSimulation _simulation;

  Ref<Texture> _picture;      // used in group info (InGameUI)
  int _weaponType;            // slots occupied - see MaskSlot
  bool _shotFromTurret;
  bool _canDrop;              // can be dropped
  //! infantry weapon have different mass and can influence dexterity
  float _dexterity;
  //! used in MP game for decision which body to hide
  float _value;
  /// duration of light flash
  float _fireLightDuration; 
  /// intensity of light flash
  float _fireLightIntensity;
#if _VBS2 // fatigue model
  //! mass of weapon
  float _mass;
  // make lightsources view mode dependent
  int _lightMode;
#endif

  //!{ TI properties
  float _htMin;
  float _htMax;
  float _afMax;
  float _mfMax;
  float _mFact;
  float _tBody;
  bool _loadedFromCfg;
  //!}

  RefArray<MuzzleType> _muzzles;

  Ref<WeaponObject> _model; // weapon model
  mutable AnimationAnimatedTexture _animFire;
  mutable int _shapeRef; // how many times is this type weapon actually used?

  /// source values for _animations
  AnimationSourceHolder _animSources;

  /// resources for weapon optics info in HUD 
  AutoArray<RStringB> _weaponInfoTypes;

  //time reguired to lock weapon
  float _weaponLockDelay;
  //weapon targeting system - used for CM
  int _weaponLockSystem;
  float _cmImmunity;

  //sounds played when weapon is locking/locked at target
  SoundPars _lockingTargetSound;
  SoundPars _lockedTargetSound;
  
public:
  WeaponType();
  void Init(const char *name);
  const RStringB &GetName() const {return _parClass->GetName();}
  RStringB GetPictureName() const;
  Texture *GetPicture() const {return _picture;}

  WeaponSimulation GetSimulation() const {return _simulation;}

  RStringB GetDisplayName() const {return _displayName;}
  RStringB GetNameSound() const {return _nameSound;}

  RStringB GetDescription() const {return _description;}

  bool IsMagazineUsableInWeapon(const MagazineType *type) const;

#if _VBS2 // fatigue model
  //! mass of weapon
  float GetMass() const {return _mass;}
#endif

  AnimationSource *CreateAnimationSource(const AnimationType *type, RStringB source);

  /// encapsulate access to animations
  const AnimationHolder &GetAnimations() const
  {
    // prefer shape based animations
    if (_model && _model->GetShape() && _model->GetShape()->GetAnimations())
    {
      return *_model->GetShape()->GetAnimations();
    }
    /// once _animations is removed, use AnimationHolder::Empty()
    return AnimationHolder::Empty();
  }
  
  LSError Serialize(ParamArchive &ar);
  static WeaponType *CreateObject(ParamArchive &ar);

  void ShapeAddRef(); // force model to be loaded
  void ShapeRelease(); // allow model to be released
  void InitShape();
  void DeinitShape();
  
  void InitShapeLevel(int level);
  void DeinitShapeLevel(int level);

  //@{ LODShapeLoadHandler
  virtual void LODShapeLoaded(LODShape *shape);
  virtual void LODShapeUnloaded(LODShape *shape);
  virtual void ShapeLoaded(LODShape *shape, int level);
  virtual void ShapeUnloaded(LODShape *shape, int level);
  //@}

  bool PrepareShapeDrawMatrices(Matrix4Array &matrices, int level, const ObjectVisualState &vs, const MagazineSlot *magazineSlot) const;

  __forceinline const AutoArray<RStringB> &GetWeaponInfoTypes() const {return _weaponInfoTypes;}

#if _ENABLE_HAND_IK
protected:
  /// soldier anim with weapon
  RefArray<AnimationRT> _handAnim;
public:
  Ref<AnimationRT> GetHandAnim(Skeleton * sk) const;  
#endif
};

inline RString ExtractActionName(RStringB name)
{
  const char *prefix = "ManAct";
  if (strnicmp(name, prefix, strlen(prefix)) == 0) return (const char *)name + strlen(prefix);
  return name;
}

#include <Es/Containers/bankInitArray.hpp>

typedef BankInitArray<MagazineType> MagazineTypeBank;
typedef BankInitArray<WeaponType> WeaponTypeBank;

extern MagazineTypeBank MagazineTypes;
extern WeaponTypeBank WeaponTypes;

#endif

