#ifdef _MSC_VER
#pragma once
#endif

#ifndef _LIVE_STATS_HPP
#define _LIVE_STATS_HPP

#include <El/Network/netXboxConfig.hpp>

#if _XBOX_SECURE && defined _XBOX && _XBOX_VER < 200 && _ENABLE_MP

#include <Es/Common/win.h>
#include <El/ParamFile/paramFile.hpp>

#ifndef XONLINE_STAT_IS_SIMPLE
  struct XONLINE_STAT {
    UINT wID;
    UINT  type;
    LONGLONG llValue;
  };
# define XONLINE_STAT_IS_SIMPLE
  TypeIsSimple(XONLINE_STAT)
#endif

struct XOnlineStats
{
  int user;
  AutoArray<XONLINE_STAT> stats;
  AutoArray<XONLINE_STAT> statsTotal;
};
TypeIsMovable(XOnlineStats)

enum LiveStatsAlgorithm
{
  LSANone = -1,
  LSASingleMission,
  LSACampaign,
  LSADeathmatch,
  LSATeam,
  LSAServer,
  LSABegin,
  LSAEnd
};

#define XONLINE_STAT_BEGIN  0x1000
#define XONLINE_STAT_END    0x1001
#define XONLINE_STAT_TIME   0x1002

class LiveStatsUpdate
{
  AutoArray<XONLINE_STAT_SPEC> _specs;
  AutoArray<XOnlineStats> _statPlayers;

  XONLINETASK_HANDLE _readStatsTask;
  XONLINETASK_HANDLE _writeStatsTask;
  int _updateOffset;
  int _updateSize;

  ConstParamEntryPtr _statsEntry;
  int _board;
  int _algorithm;

  ConstParamEntryPtr _statsEntryTotal;
  int _boardTotal;

public:
  LiveStatsUpdate(RString entryName = RString(), LiveStatsAlgorithm algorithm = LSANone);
  ~LiveStatsUpdate();

  void Init(RString entryName);
  void Process();
  void Close();

  bool IsDone() const {return !_readStatsTask && !_writeStatsTask;}
  RString GetName() const {return _statsEntry ? _statsEntry->GetName() : RString();}

protected:
  bool CreateStatsRows();
  void UpdateStatsRows();
};
#endif

#endif
