#include "../wpch.hpp"
#include "liveStats.hpp"

#if _XBOX_SECURE && defined _XBOX && _XBOX_VER < 200 && _ENABLE_MP

#include <Es/Common/global.hpp>
#include <Es/Strings/rString.hpp>
#include "../world.hpp"
#include "../global.hpp"
#include "../progress.hpp"
#include "../stringtableExt.hpp"
#include "network.hpp"
#include "../AI/ai.hpp"
#include "../UI/missionDirs.hpp"

#define LOG_LIVE_STATS 1

LiveStatsUpdate::LiveStatsUpdate(RString entryName, LiveStatsAlgorithm algorithm)
{
  _readStatsTask = NULL;
  _writeStatsTask = NULL;
  _board = -1;
  _algorithm = algorithm;
  _boardTotal = -1;
  _updateOffset = 0;
  _updateSize = 0;

#if LOG_LIVE_STATS
  RptF("<LiveStatsUpdate name=\"%s\" algorithm=\"%d\">", cc_cast(entryName), algorithm);
#endif

  Init(entryName);
}

LiveStatsUpdate::~LiveStatsUpdate()
{
  Close();
#if LOG_LIVE_STATS
  RptF("</LiveStatsUpdate>");
#endif
}


void LiveStatsUpdate::Init(RString entryName)
{
  if (entryName.GetLength() > 0)
  {
  }
  else
  {
    if (GetMissionParameters().GetLength() > 0)
    {
      // user missions do not change statistics
#if LOG_LIVE_STATS
      RptF(" - user mission don't change stats");
#endif
      return;
    }
    if (GWorld->GetMode() == GModeNetware)
    {
      if (!GetNetworkManager().GetMissionHeader()) return;
      RString gameType = GetNetworkManager().GetMissionHeader()->_gameType;
      ConstParamEntryPtr cls = (Pars >> "CfgMPGameTypes").FindEntry(gameType);
      if (!cls) return;
      entryName = (*cls) >> "stats";
    }
    else if (IsCampaign())
    {
      entryName = ExtParsCampaign >> "stats";
    }
    else
    {
      entryName = "SPSingle";
    }
  }

#if LOG_LIVE_STATS
  RptF(" - board name \"%s\"", cc_cast(entryName));
#endif
  _statsEntry = (Pars >> "CfgLiveStats").FindEntry(entryName);
  if (!_statsEntry) return;
  _board = (*_statsEntry) >> "board";
#if LOG_LIVE_STATS
  RptF(" - board #%d", _board);
#endif
  if (_algorithm < 0) _algorithm = (*_statsEntry) >> "algorithm";
#if LOG_LIVE_STATS
  RptF(" - algorithm #%d", _algorithm);
#endif
  if (_board < 0 || _algorithm < 0) return;

  ConstParamEntryPtr total = _statsEntry->FindEntry("total");
  if (total)
  {
    RString totalName = *total;
    _statsEntryTotal = (Pars >> "CfgLiveStats").FindEntry(totalName);
    if (_statsEntryTotal)
    {
      _boardTotal = (*_statsEntryTotal) >> "board";
#if LOG_LIVE_STATS
      RptF(" - total board #%d", _boardTotal);
#endif
    }
  }

  // prepare statistics structure
  if (!CreateStatsRows()) return;

  // start reading of statistics
  _updateOffset = 0;
  _updateSize = min(_specs.Size() - _updateOffset, XONLINE_STAT_MAX_SPECS_IN_WRITE_REQUEST);
  HRESULT result = XOnlineStatRead(_updateSize, _specs.Data() + _updateOffset, NULL, &_readStatsTask);
  if (FAILED(result))
  {
    // cannot read statistics
#if LOG_LIVE_STATS
    RptF(" - XOnlineStatRead failed with error 0x%x", result);
#endif
    _readStatsTask = NULL;
    return;
  }
#if LOG_LIVE_STATS
  RptF(" - Reading %d rows", _updateSize);
#endif
}

void LiveStatsUpdate::Process()
{
  if (_readStatsTask)
  {
    HRESULT result = XOnlineTaskContinue(_readStatsTask);
    if (result == XONLINETASK_S_RUNNING) return;

    if (FAILED(result))
    {
      // cannot read statistics
#if LOG_LIVE_STATS
      RptF(" - XOnlineStatRead update failed with error 0x%x", result);
#endif
      XOnlineTaskClose(_readStatsTask);
      _readStatsTask = NULL;
      return;
    }

    result = XOnlineStatReadGetResult(_readStatsTask, _updateSize, _specs.Data() + _updateOffset, 0, NULL); 
    XOnlineTaskClose(_readStatsTask);
    _readStatsTask = NULL;
    if (FAILED(result))
    {
      // cannot read statistics
#if LOG_LIVE_STATS
      RptF(" - XOnlineStatRead result failed with error 0x%x", result);
#endif
      return;
    }

#if LOG_LIVE_STATS
    RptF(" - Read %d rows", _updateSize);
#endif

    _updateOffset += _updateSize;
    if (_updateOffset < _specs.Size())
    {
      _updateSize = min(_specs.Size() - _updateOffset, XONLINE_STAT_MAX_SPECS_IN_WRITE_REQUEST);
      result = XOnlineStatRead(_updateSize, _specs.Data() + _updateOffset, NULL, &_readStatsTask);
      if (FAILED(result))
      {
        // cannot read statistics
#if LOG_LIVE_STATS
        RptF(" - XOnlineStatRead failed with error 0x%x", result);
#endif
        _readStatsTask = NULL;
        return;
      }
#if LOG_LIVE_STATS
      RptF(" - Reading %d rows", _updateSize);
#endif
      return;
    }

    // update statistics
#if LOG_LIVE_STATS
    RptF(" - Updating statistics");
#endif
    UpdateStatsRows();

    // FIX: send only statistics for local players, for remote players ask clients
    if
    (
      GWorld->GetMode() == GModeNetware &&
      _algorithm != LSAServer && // DS - only to host
      _algorithm != LSABegin && _algorithm != LSAEnd
    )
    {
      int player = GetNetworkManager().GetPlayer();
      XONLINE_USER *user = XOnlineGetLogonUsers();
      DoAssert(user);
      XUID xuid = {0, 0};
      if (user) xuid = user->xuid;

      // remove remote statistics
      for (int i=0; i<_specs.Size();)
      {
        if (XOnlineAreUsersIdentical(&_specs[i].xuidUser, &xuid))
          i++;
        else
          _specs.Delete(i);
      }

      // send remote statistics
      for (int i=0; i<_statPlayers.Size(); i++)
      {
        if (_statPlayers[i].user == player) continue; // local stats
        GetNetworkManager().AskForStatsWrite(
          _statPlayers[i].user,
          _board, _statPlayers[i].stats,
          _boardTotal, _statPlayers[i].statsTotal
          );
      }
    }

    if (_specs.Size() == 0) return;

    // start writing of statistics
    _updateOffset = 0;
    _updateSize = min(_specs.Size() - _updateOffset, XONLINE_STAT_MAX_SPECS_IN_WRITE_REQUEST);
    result = XOnlineStatWrite(_updateSize, _specs.Data() + _updateOffset, NULL, &_writeStatsTask);
    if (FAILED(result))
    {
      // cannot write statistics
#if LOG_LIVE_STATS
      RptF(" - XOnlineStatWrite failed with error 0x%x", result);
#endif
      _writeStatsTask = NULL;
      return;
    }
#if LOG_LIVE_STATS
    RptF(" - Writing %d rows", _updateSize);
#endif
  }
  else if (_writeStatsTask)
  {
    HRESULT result = XOnlineTaskContinue(_writeStatsTask);
    if (result == XONLINETASK_S_RUNNING) return;

    XOnlineTaskClose(_writeStatsTask);
    _writeStatsTask = NULL;

    if (FAILED(result))
    {
      // cannot write statistics
#if LOG_LIVE_STATS
      RptF(" - XOnlineStatWrite update failed with error 0x%x", result);
#endif
      return;
    }
#if LOG_LIVE_STATS
    RptF(" - Written %d rows", _updateSize);
#endif

    _updateOffset += _updateSize;
    if (_updateOffset < _specs.Size())
    {
      _updateSize = min(_specs.Size() - _updateOffset, XONLINE_STAT_MAX_SPECS_IN_WRITE_REQUEST);
      result = XOnlineStatWrite(_updateSize, _specs.Data() + _updateOffset, NULL, &_writeStatsTask);
      if (FAILED(result))
      {
        // cannot write statistics
#if LOG_LIVE_STATS
        RptF(" - XOnlineStatWrite failed with error 0x%x", result);
#endif
        _writeStatsTask = NULL;
        return;
      }
#if LOG_LIVE_STATS
      RptF(" - Writing %d rows", _updateSize);
#endif
      return;
    }
  }
}

void LiveStatsUpdate::Close()
{
  if (_readStatsTask || _writeStatsTask)
  {
    ProgressReset();
    RString message = GWorld->GetMode() == GModeNetware ? LocalizeString(IDS_NETWORK_UPLOADING_STATS_MP) : LocalizeString(IDS_NETWORK_UPLOADING_STATS);
    IProgressDisplay *CreateDisplayProgress(RString text, RString resource);
    Ref<ProgressHandle> p = ProgressStart(CreateDisplayProgress(message, "RscDisplayNotFreezeBig"));
    while (_readStatsTask || _writeStatsTask)
    {
      Process();
      ProgressRefresh();
    }
    ProgressFinish(p);
  }
}

bool LiveStatsUpdate::CreateStatsRows()
{
  AUTO_STATIC_ARRAY(int, players, 32);
  AUTO_STATIC_ARRAY(XUID, xuids, 32);
  int n = 1;
  if
  (
    GWorld->GetMode() == GModeNetware &&
    _algorithm != LSAServer && // DS - only to host
    _algorithm != LSABegin && _algorithm != LSAEnd
  ) 
  {
    const AutoArray<PlayerIdentity> *identities = GetNetworkManager().GetIdentities();
    if (!identities) return false;
    int m = identities->Size();
    if (m == 0) return false;

    // total time in the mission
    float totalTime = Glob.time - Time(0);
    const float atLeastCoef = 0.4f;
    float atLeastInMission = atLeastCoef * totalTime;

    for (int i=0; i<m; i++)
    {
      const PlayerIdentity &identity = identities->Get(i);
      float playerTime = Glob.time - GetNetworkManager().GetPlayerJoined(identity.dpnid);
      if (playerTime >= atLeastInMission)
      {
        players.Add(identity.dpnid);
        xuids.Add(identity.xuid);
//        LogF("STATS: Player %s presence: %.0f / %.0f", cc_cast(identity.name), playerTime, totalTime);
      }
    }
    n = players.Size();
  }
  else
  {
    XONLINE_USER *user = XOnlineGetLogonUsers();
    if (!user) return false;
    players.Resize(1);
    xuids.Resize(1);
    players[0] = 0;
    xuids[0] = user->xuid;
  }

  // fast check if update is needed
  switch (_algorithm)
  {
  case LSASingleMission:
    if (GWorld->GetEndMode() == EMContinue) return false;
    break;
  case LSACampaign:
  case LSAServer:
  case LSABegin:
  case LSAEnd:
    break;
  case LSADeathmatch:
  case LSATeam:
    if (n <= 1) return false;
    break;
  default:
    RptF("Unknown statistics algorithm %d", _algorithm);
    return false;
  }

  // create structures
  _statPlayers.Realloc(n);
  _statPlayers.Resize(n);
  _specs.Realloc(_boardTotal >= 0 ? 2 * n : n);
  _specs.Resize(_boardTotal >= 0 ? 2 * n : n);
  for (int i=0; i<n; i++)
    _statPlayers[i].user = players[i];

  // create partial statistics
  switch (_algorithm)
  {
  case LSASingleMission:
    {
      // read parameters
      int columns = (*_statsEntry) >> "columns";
      for (int i=0; i<n; i++)
      {
        AutoArray<XONLINE_STAT> &array = _statPlayers[i].stats;
        array.Realloc(columns + 1);
        array.Resize(columns + 1);
        for (int j=0; j<columns; j++)
        {
          XONLINE_STAT &stat = array[j];
          stat.wID = j;
          stat.type = XONLINE_STAT_LONGLONG;
          stat.llValue = 0; // default value
        }
        XONLINE_STAT &stat = array[columns];
        stat.wID = XONLINE_STAT_RATING;
        stat.type = XONLINE_STAT_LONGLONG;
        stat.llValue = 0; // default value
      }
    }
    break;
  case LSACampaign:
  case LSADeathmatch:
  case LSATeam:
  case LSAServer:
    // no parameters needed
    for (int i=0; i<n; i++)
    {
      AutoArray<XONLINE_STAT> &array = _statPlayers[i].stats;
      array.Realloc(2);
      array.Resize(2);
      {
        XONLINE_STAT &stat = array[0];
        stat.wID = XONLINE_STAT_RATING;
        stat.type = XONLINE_STAT_LONGLONG;
        stat.llValue = 0; // default value
      }
      {
        XONLINE_STAT &stat = array[1];
        stat.wID = XONLINE_STAT_TIME;
        stat.type = XONLINE_STAT_LONGLONG;
        stat.llValue = 0; // default value
      }
    }
    break;
  case LSABegin:
    // no parameters needed
    for (int i=0; i<n; i++)
    {
      AutoArray<XONLINE_STAT> &array = _statPlayers[i].stats;
      array.Realloc(2);
      array.Resize(2);
      {
        XONLINE_STAT &stat = array[0];
        stat.wID = XONLINE_STAT_BEGIN;
        stat.type = XONLINE_STAT_LONG;
        stat.lValue = 0; // default value
      }
      {
        // avoid line with no score
        XONLINE_STAT &stat = array[1];
        stat.wID = XONLINE_STAT_RATING;
        stat.type = XONLINE_STAT_LONGLONG;
        stat.llValue = 0; // default value
      }
    }
    break;
  case LSAEnd:
    // no parameters needed
    for (int i=0; i<n; i++)
    {
      AutoArray<XONLINE_STAT> &array = _statPlayers[i].stats;
      array.Realloc(1);
      array.Resize(1);
      XONLINE_STAT &stat = array[0];
      stat.wID = XONLINE_STAT_END;
      stat.type = XONLINE_STAT_LONG;
      stat.lValue = 0; // default value
    }
    break;
  default:
    Fail("Algorithm");
    break;
  }

  // create total statistics
  if (_boardTotal >= 0)
  {
    switch (_algorithm)
    {
    case LSASingleMission:
    case LSACampaign:
    case LSAServer:
    case LSADeathmatch:
    case LSATeam:
      {
        ParamEntryVal columns = (*_statsEntryTotal) >> "Columns";
        int m = columns.GetEntryCount();
        for (int i=0; i<n; i++)
        {
          AutoArray<XONLINE_STAT> &array = _statPlayers[i].statsTotal;
          array.Realloc(m + 1);
          array.Resize(m + 1);
          for (int j=0; j<m; j++)
          {
            ParamEntryVal entry = columns.GetEntry(j);
            XONLINE_STAT &stat = array[j];
            stat.wID = (entry >> "column").GetInt();
            stat.type = XONLINE_STAT_LONGLONG;
            stat.llValue = 0; // default value
          }
          XONLINE_STAT &stat = array[m];
          stat.wID = XONLINE_STAT_RATING;
          stat.type = XONLINE_STAT_LONGLONG;
          stat.llValue = 0; // default value
        }
      }
      break;
    case LSABegin:
      for (int i=0; i<n; i++)
      {
        AutoArray<XONLINE_STAT> &array = _statPlayers[i].statsTotal;
        array.Realloc(2);
        array.Resize(2);
        {
          XONLINE_STAT &stat = array[0];
          stat.wID = XONLINE_STAT_BEGIN;
          stat.type = XONLINE_STAT_LONG;
          stat.lValue = 0; // default value
        }
        {
          // avoid line with no score
          XONLINE_STAT &stat = array[1];
          stat.wID = XONLINE_STAT_RATING;
          stat.type = XONLINE_STAT_LONGLONG;
          stat.llValue = 0; // default value
        }
      }
      break;
    case LSAEnd:
      for (int i=0; i<n; i++)
      {
        AutoArray<XONLINE_STAT> &array = _statPlayers[i].statsTotal;
        array.Realloc(1);
        array.Resize(1);
        XONLINE_STAT &stat = array[0];
        stat.wID = XONLINE_STAT_END;
        stat.type = XONLINE_STAT_LONG;
        stat.lValue = 0; // default value
      }
      break;
    }
  }

  // connect partial statistics
  for (int i=0; i<n; i++)
  {
    XOnlineStats &stat = _statPlayers[i];
    XONLINE_STAT_SPEC &spec = _specs[i];
    spec.xuidUser = xuids[i];
    spec.dwLeaderBoardID = _board;
    spec.dwNumStats = stat.stats.Size();
    spec.pStats = stat.stats.Data();
  }

  // connect total statistics
  if (_boardTotal >= 0)
  {
    for (int i=0; i<n; i++)
    {
      XOnlineStats &stat = _statPlayers[i];
      XONLINE_STAT_SPEC &spec = _specs[n + i];
      spec.xuidUser = xuids[i];
      spec.dwLeaderBoardID = _boardTotal;
      spec.dwNumStats = stat.statsTotal.Size();
      spec.pStats = stat.statsTotal.Data();
    }
  }

  return true;
}

static XONLINE_STAT &FindStat(AutoArray<XONLINE_STAT> &array, WORD id)
{
  for (int i=0; i<array.Size(); i++)
  {
    if (array[i].wID == id) return array[i];
  }
  Fail("Column");
  return *(XONLINE_STAT *)NULL;
}

void LiveStatsUpdate::UpdateStatsRows()
{
  int n = _statPlayers.Size();

  // store partial rating for update of total statistics 
  AUTO_STATIC_ARRAY(int64, result, 32);
  result.Resize(n);

  // repair statistics (avoid type XONLINE_STAT_NONE)
  for (int i=0; i<n; i++)
  {
    // ensure uninitialized variable is not used
    result[i] = 0;

    // partial statistics
    {
      AutoArray<XONLINE_STAT> &array = _statPlayers[i].stats;
      for (int j=0; j<array.Size(); j++)
      {
        XONLINE_STAT &stat = array[j];
        if (stat.type == XONLINE_STAT_NONE)
        {
          switch (stat.wID)
          {
          case XONLINE_STAT_RANK: 
          case XONLINE_STAT_BEGIN: 
          case XONLINE_STAT_END:
            stat.type = XONLINE_STAT_LONG;
            stat.lValue = 0;
            break;
          default:
            stat.type = XONLINE_STAT_LONGLONG;
            stat.llValue = 0;
            break;
          }
        }
      }
    }
    // total statistics
    {
      AutoArray<XONLINE_STAT> &array = _statPlayers[i].statsTotal;
      for (int j=0; j<array.Size(); j++)
      {
        XONLINE_STAT &stat = array[j];
        if (stat.type == XONLINE_STAT_NONE)
        {
          switch (stat.wID)
          {
          case XONLINE_STAT_RANK: 
          case XONLINE_STAT_BEGIN: 
          case XONLINE_STAT_END:
            stat.type = XONLINE_STAT_LONG;
            stat.lValue = 0;
            break;
          default:
            stat.type = XONLINE_STAT_LONGLONG;
            stat.llValue = 0;
            break;
          }
        }
      }
    }
  }

  // update partial statistics
  switch (_algorithm)
  {
  case LSASingleMission:
    {
      // calculate "new high score"
      EndMode end = GWorld->GetEndMode();
      RString endName = FindEnumName(end);
      float coefEnd = ExtParsMission >> (RString("coef") + endName);

      int difficulty = Glob.config.difficulty;
      RString diffName = Glob.config.diffNames.GetName(difficulty);
      float coefDiff = (*_statsEntry) >> (RString("coef") + diffName);

      int64 newScore = toInt(coefEnd * coefDiff);
      int column = ExtParsMission >> "statsColumn";
      for (int i=0; i<n; i++)
      {
        // for each player (row)
        AutoArray<XONLINE_STAT> &array = _statPlayers[i].stats;
        // old state
        XONLINE_STAT &stat = FindStat(array, column);
        if (!&stat) break; // wrong mission description
        if (newScore <= stat.llValue)
        {
          // unchanged
          XONLINE_STAT &stat = FindStat(array, XONLINE_STAT_RATING);
          // sum (in result) also unchanged
          result[i] = stat.llValue;
        }
        else
        {
#if LOG_LIVE_STATS
          RptF(" ... player %d, column %d - score changed %I64d -> %I64d", i, column, stat.llValue, newScore);
#endif
          stat.llValue = newScore;

          // update rating
          int columns = (*_statsEntry) >> "columns";

          int64 sum = 0;
          for (int i=0; i<columns; i++)
          {
            XONLINE_STAT &stat = FindStat(array, i);
            sum += stat.llValue;
          }

          {
            XONLINE_STAT &stat = FindStat(array, XONLINE_STAT_RATING);
#if LOG_LIVE_STATS
            RptF(" ... player %d, rating changed %I64d -> %I64d", i, stat.llValue, sum);
#endif
            stat.llValue = result[i] = sum;
          }
        }
      }
    }
    break;
  case LSACampaign:
    for (int i=0; i<n; i++)
    {
      AutoArray<XONLINE_STAT> &array = _statPlayers[i].stats;
      XONLINE_STAT &stat = FindStat(array, XONLINE_STAT_RATING);
      if (GStats._campaign._liveScore > stat.llValue)
      {
#if LOG_LIVE_STATS
        RptF(" ... rating changed %I64d -> %I64d", stat.llValue, toInt(GStats._campaign._liveScore));
#endif
        stat.llValue = toInt(GStats._campaign._liveScore);
      }
      result[i] = stat.llValue;
    }
    break;
  case LSADeathmatch:
    {
      // current rating (for handicap calculations)
      AUTO_STATIC_ARRAY(int64, rating, 32);
      rating.Resize(n);
      for (int i=0; i<n; i++)
      {
        XONLINE_STAT &stat = FindStat(_statPlayers[i].stats, XONLINE_STAT_RATING);
        rating[i] = stat.llValue;
      }

      // score in this mission
      AUTO_STATIC_ARRAY(int, score, 32);
      score.Resize(n);
      for (int i=0; i<n; i++)
      {
        int dpnid = _statPlayers[i].user;
        const AIStatsMPRow *row = GStats._mission.GetRow(dpnid);
        score[i] = row ? row->_killsTotal : 0;
      }

      // read parameters
      float coefWin = (*_statsEntry) >> "coefWin";
      float coefLoose = (*_statsEntry) >> "coefLoose";
      float coefM1 = (*_statsEntry) >> "coefM1";
      float coefS1 = (*_statsEntry) >> "coefS1";
      float coefM2 = (*_statsEntry) >> "coefM2";
      float coefS2 = (*_statsEntry) >> "coefS2";
      float coefMin = (*_statsEntry) >> "coefMin";
      float coefMax = (*_statsEntry) >> "coefMax";

      for (int i=0; i<n; i++)
      {
        // calculate score for player i
        float sum = 0;
        for (int j=0; j<n; j++)
        {
          if (j == i) continue;
          if (score[j] == score[i]) continue;
          if (score[i] < score[j])
          {
            // j beats i
            float handicap = (coefM1 * rating[i] + coefS1) / (coefM2 * rating[j] + coefS2);
            saturate(handicap, coefMin, coefMax);
            sum += coefLoose * handicap;
          }
          else
          {
            // i beats j
            float handicap = (coefM1 * rating[j] + coefS1) / (coefM2 * rating[i] + coefS2);
            saturate(handicap, coefMin, coefMax);
            sum += coefWin * handicap;
          }
        }

        XONLINE_STAT &stat = FindStat(_statPlayers[i].stats, XONLINE_STAT_RATING);
#if LOG_LIVE_STATS
        RptF(" ... player %d, rating changed %I64d -> %I64d", i, stat.llValue, max(rating[i] + toInt(sum), 0));
#endif
        stat.llValue = result[i] = max(rating[i] + toInt(sum), 0);
      }
    }
    break;
  case LSATeam:
    {
      // current rating (for handicap calculations)
      AUTO_STATIC_ARRAY(int, sides, 32);
      sides.Resize(n);

      int64 rating[TSideUnknown];
      int teamResult[TSideUnknown];
      int players[TSideUnknown];
      int ai[TSideUnknown];
      int score[TSideUnknown];
      for (int i=0; i<TSideUnknown; i++)
      {
        rating[i] = 0;
        teamResult[i] = 0;
        players[i] = 0;
        ai[i] = 0;
        score[i] = 0;
      }

      for (int i=0; i<n; i++)
      {
        // player id
        int dpnid = _statPlayers[i].user;

        // player side
        const PlayerRole *role = GetNetworkManager().FindPlayerRole(dpnid);
        if (!role)
        {
          sides[i] = (TargetSide)-1;
          continue;
        }
        TargetSide side = role->side;

        sides[i] = side;
        players[side]++;

        const AIStatsMPRow *row = GStats._mission.GetRow(dpnid);
        if (row) saturateMax(score[side], row->_liveStats);

        XONLINE_STAT &stat = FindStat(_statPlayers[i].stats, XONLINE_STAT_RATING);
        rating[side] += stat.llValue;
      }

      // AI ratings
      for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
      {
        const PlayerRole *role = GetNetworkManager().GetPlayerRole(i);
        if (role->player == AI_PLAYER) ai[role->side]++;
      }
      float coefAI = (*_statsEntry) >> "coefAI";
      for (int i=0; i<TSideUnknown; i++)
      {
        if (players[i] > 0)
          rating[i] += coefAI * ai[i] * rating[i] / players[i];
      }

      // read parameters
      float coefWin = (*_statsEntry) >> "coefWin";
      float coefLoose = (*_statsEntry) >> "coefLoose";
      float coefM1 = (*_statsEntry) >> "coefM1";
      float coefS1 = (*_statsEntry) >> "coefS1";
      float coefM2 = (*_statsEntry) >> "coefM2";
      float coefS2 = (*_statsEntry) >> "coefS2";
      float coefMin = (*_statsEntry) >> "coefMin";
      float coefMax = (*_statsEntry) >> "coefMax";

      for (int i=0; i<TSideUnknown; i++)
      {
        // calculate score for team i
        if (players[i] == 0) continue;
        float sum = 0;
        for (int j=0; j<TSideUnknown; j++)
        {
          if (j == i) continue;
          if (players[j] == 0) continue;
          if (score[j] == score[i]) continue;
          if (score[i] < score[j])
          {
            // j beats i
            float handicap = (coefM1 * rating[i] + coefS1) / (coefM2 * rating[j] + coefS2);
            saturate(handicap, coefMin, coefMax);
            sum += coefLoose * handicap;
          }
          else
          {
            // i beats j
            float handicap = (coefM1 * rating[j] + coefS1) / (coefM2 * rating[i] + coefS2);
            saturate(handicap, coefMin, coefMax);
            sum += coefWin * handicap;
          }
        }
        teamResult[i] = sum;
      }

      for (int i=0; i<n; i++)
      {
        // calculate score for player i
        int side = sides[i];
        if (side < 0) continue;
        XONLINE_STAT &stat = FindStat(_statPlayers[i].stats, XONLINE_STAT_RATING);
#if LOG_LIVE_STATS
        RptF(" ... player %d, rating changed %I64d -> %I64d", i, stat.llValue, max(stat.llValue + toInt(teamResult[side]), 0));
#endif
        stat.llValue = result[i] = max(stat.llValue + toInt(teamResult[side]), 0);
      }
    }
    break;
  case LSAServer:
    // dedicated server time
    for (int i=0; i<n; i++)
    {
      AutoArray<XONLINE_STAT> &array = _statPlayers[i].stats;
      {
        XONLINE_STAT &stat = FindStat(array, XONLINE_STAT_TIME);
#if LOG_LIVE_STATS
        RptF(" ... player %d, rating changed %I64d -> %I64d", i, stat.llValue, stat.llValue + 1);
#endif
        stat.llValue++;
      }
      {
        XONLINE_STAT &stat = FindStat(array, XONLINE_STAT_RATING);
        const AutoArray<PlayerIdentity> *identities = GetNetworkManager().GetIdentities();
        if (identities) stat.llValue += identities->Size();
        result[i] = stat.llValue;
      }
    }
    break;
  case LSABegin:
    for (int i=0; i<n; i++)
    {
      AutoArray<XONLINE_STAT> &array = _statPlayers[i].stats;
      XONLINE_STAT &stat = FindStat(array, XONLINE_STAT_BEGIN);
#if LOG_LIVE_STATS
      RptF(" ... player %d, rating changed %d -> %d", i, stat.lValue, stat.lValue + 1);
#endif
      stat.lValue++;
    }
    break;
  case LSAEnd:
    for (int i=0; i<n; i++)
    {
      AutoArray<XONLINE_STAT> &array = _statPlayers[i].stats;
      XONLINE_STAT &stat = FindStat(array, XONLINE_STAT_END);
#if LOG_LIVE_STATS
      RptF(" ... player %d, rating changed %d -> %d", i, stat.lValue, stat.lValue + 1);
#endif
      stat.lValue++;
    }
    break;
  default:
    Fail("Algorithm");
    return;
  }

  // update total statistics
  if (_boardTotal >= 0)
  {
    switch (_algorithm)
    {
    case LSASingleMission:
    case LSACampaign:
    case LSAServer:
    case LSADeathmatch:
    case LSATeam:
      {
        RString name = _statsEntry->GetName();
        ParamEntryVal columns = (*_statsEntryTotal) >> "Columns";
        ParamEntryVal entry = columns >> name;
        int column = entry >> "column";

        for (int i=0; i<n; i++)
        {
          AutoArray<XONLINE_STAT> &array = _statPlayers[i].statsTotal;
          // update column
          {
            XONLINE_STAT &stat = FindStat(array, column);
            stat.llValue = result[i];
          }

          // recalculate sum
          float sum = 0;
          for (int i=0; i<columns.GetEntryCount(); i++)
          {
            ParamEntryVal entry = columns.GetEntry(i);
            int column = entry >> "column";
            float coef = entry >> "coef";
            XONLINE_STAT &stat = FindStat(array, column);
            sum += coef * stat.llValue;
          }

          // update total rating
          {
            XONLINE_STAT &stat = FindStat(array, XONLINE_STAT_RATING);
            stat.llValue = sum;
          }
        }
      }
      break;
    case LSABegin:
      for (int i=0; i<n; i++)
      {
        AutoArray<XONLINE_STAT> &array = _statPlayers[i].statsTotal;
        XONLINE_STAT &stat = FindStat(array, XONLINE_STAT_BEGIN);
        stat.lValue++;
      }
      break;
    case LSAEnd:
      for (int i=0; i<n; i++)
      {
        AutoArray<XONLINE_STAT> &array = _statPlayers[i].statsTotal;
        XONLINE_STAT &stat = FindStat(array, XONLINE_STAT_END);
        stat.lValue++;
      }
      break;
    }
  }
}

#endif

