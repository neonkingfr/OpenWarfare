#ifdef _WIN32
#include <crtdbg.h>
#endif
#include <El/Credits/credits.hpp>
#include "httpRequest.h"
#include <Es/Strings/bString.hpp>
#include <Es/Strings/rString.hpp>
#include <string>

#ifndef _CONSOLE
#include <libcurl/include/curl/curl.h>
#endif

REGISTER_CREDITS("rapidjson","2011 Milo Yip","")
REGISTER_CREDITS("libcurl","Copyright (c) 1996 - 2012, Daniel Stenberg","")

struct CurlInit
{
  CurlInit()
  {
    // CURL_GLOBAL_WIN32 initializes sockets (WSAStartup), this can be done multiple times
    // CURL_GLOBAL_SSL initializes SSL
#ifdef _WIN32
#ifndef _CONSOLE
    curl_global_init(CURL_GLOBAL_WIN32|CURL_GLOBAL_SSL);
#endif
#endif
  }
  ~CurlInit()
  {
#ifdef _WIN32
#ifndef _CONSOLE
    curl_global_cleanup();
#endif
#endif
  }
} GCurlInit;

struct write_callbackCtx
{
  RString out;
};

static size_t write_callback(void *buffer, size_t size, size_t nmemb, void *data)
{
  write_callbackCtx *ctx = reinterpret_cast<write_callbackCtx *>(data);
  ctx->out = ctx->out + RString(reinterpret_cast<const char *>(buffer),size*nmemb);
  return size*nmemb;
}


static rapidjson_binding::DocumentValue CreateErrorObject(const char *errorString)
{
  rapidjson_binding::DocumentValue doc;
  // construct a JSON object containing the error message
  doc.AddMemberString("error", errorString);
  return doc;
}


int GetResponse(RString &response, const char *site, const char *query, const char *postString)
{
  BString<1024> url;
  url.PrintF("%s%s",site,query);

#ifndef _CONSOLE
  try
  {
    CURL *curl = curl_easy_init();

    char myErrorBuffer[CURL_ERROR_SIZE];
    curl_easy_setopt(curl,CURLOPT_ERRORBUFFER,myErrorBuffer);
    myErrorBuffer[0] = 0;


    struct CurlDebug
    {
      std::string info;
      /*
      std::string headerIn;
      std::string headerOut;
      std::string dataIn;
      std::string dataOut;
      */
      void Record(CURL *curl, curl_infotype infotype, char *string, size_t size)
      {
        std::string *res = NULL;
        switch (infotype)
        {
          /*
          case CURLINFO_HEADER_IN: res = &headerIn; break;
          case CURLINFO_HEADER_OUT: res = &headerOut; break;
          case CURLINFO_DATA_IN: res = &dataIn; break;
          case CURLINFO_DATA_OUT: res = &dataOut; break;
          */
          case CURLINFO_TEXT: res = &info; break;
        }
        if (res) *res += std::string(string,size);
      }
      void Dump()
      {
        RptF("Info: ((\n%s)) Info",info.c_str());
        /*
        RptF("Header In:\n%s",headerIn);
        RptF("Header Out:\n%s",headerOut);
        RptF("Data In:\n%s",dataIn);
        RptF("Data Out:\n%s",dataOut);
        */
      }
      static int Callback(CURL *curl, curl_infotype info, char *string, size_t size, void *param)
      {
        ((CurlDebug *)param)->Record(curl,info,string,size);
        return 0;
      }

    } curlDebug;

#if 1 //_DEBUG
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1); 
    curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, CurlDebug::Callback); 
    curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &curlDebug); 
#endif
    curl_easy_setopt(curl, CURLOPT_URL, url.cstr());

    curl_easy_setopt(curl,  CURLOPT_TIMEOUT,60);
    curl_easy_setopt(curl,  CURLOPT_CONNECTTIMEOUT,20);
    /* no need to use CURLOPT_SSL_VERIFYPEER, CURLOPT_SSL_VERIFYHOST - defaults are good */

    struct curl_slist *slist=NULL;
    if (postString)
    {
      curl_easy_setopt(curl, CURLOPT_POST, 1);
      curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postString);
      curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(postString));

      slist = curl_slist_append(slist,"Content-type: application/json");

      curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
    }

    
    write_callbackCtx ctx;
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ctx);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);

    CURLcode res = curl_easy_perform(curl);

    long http_code = 0;
    curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
    
    if (slist) curl_slist_free_all(slist);
    curl_easy_cleanup(curl);

    // TODO: check response, handle HTTP error
    switch (res)
    {
      case CURLE_OK:
        if (http_code<400)
        {
          response = ctx.out;
          return http_code;
        }
        switch (http_code)
        {
          case 404: response = Format("HTTP error %d: '%s' not found (%s)",http_code,cc_cast(url),myErrorBuffer);break;
          case 401: response = Format("HTTP error %d: '%s' access denied (%s)",http_code,cc_cast(url),myErrorBuffer);break;
          default: response = Format("HTTP error %d: URL '%s'  (%s)",http_code,cc_cast(url),myErrorBuffer);break;
        }
        break;
      default:
        http_code = 500;
        response = Format("%s (%s)",curl_easy_strerror(res),myErrorBuffer);
        break;
    }
    RptF("HTTP error %d (res=%d)",http_code,res);
    curlDebug.Dump();
    return http_code;
  }
  catch (...)
  {
    // any failure: return a NULL
    response = "Unhandled exception accessing http";
    return 500;
  }
#else
  // any failure: return a NULL
  response = "Disabled for Binarize";
  return 500;
#endif
}

rapidjson_binding::DocumentValue GetResponseJSON(const char *site, const char *query, rapidjson_binding::DocumentValue &data)
{
  BString<1024> url;
  url.PrintF("%s%s",site,query);

  try
  {
    rapidjson::StringBuffer dataBuffer;
    rapidjson::Writer<rapidjson::StringBuffer> writer(dataBuffer);
    data->Accept(writer);
    const char *dataString = dataBuffer.GetString();

    RString responseStr;
    long http_code = GetResponse(responseStr,site,query,dataString);

    if (http_code<400)
    {
      rapidjson_binding::DocumentValue doc;
      doc->Parse<rapidjson::kParseDefaultFlags>(responseStr);
      return doc;
    }

    return CreateErrorObject(responseStr);
  }
  catch (...)
  {
    // any failure: return a NULL
    return CreateErrorObject("Unhandled exception while registration");
  }
  return rapidjson_binding::DocumentValue();
}

