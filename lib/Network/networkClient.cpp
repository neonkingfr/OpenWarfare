#include "../wpch.hpp"
#include "networkImpl.hpp"
#include "../global.hpp"
//#include "strIncl.hpp"
#include "../stringtableExt.hpp"

#include "../arcadeTemplate.hpp"
#include "../AI/ai.hpp"
#include "../world.hpp"
#include "../unitsOnLand.hpp"
#include "../landscape.hpp"
#include "../UI/chat.hpp"
#include <El/XML/xml.hpp>

#include <Es/Algorithms/qsort.hpp>
#include <Es/Files/fileContainer.hpp>

#include "../allAIVehicles.hpp"
#include "../seaGull.hpp"
#include "../detector.hpp"
#include "../shots.hpp"
#include "../Protect/selectProtection.h"
#include "../scene.hpp"

#include <El/Debugging/debugTrap.hpp>

#include <El/QStream/qbStream.hpp>
#include <El/QStream/packFiles.hpp>
#include <El/FileServer/fileServer.hpp>
#include <El/Evaluator/express.hpp>

#include <El/ParamArchive/paramArchiveDb.hpp>
#include "../saveVersion.hpp"
#include "../registration.hpp"

#include <El/Common/randomGen.hpp>
#include "../gameStateExt.hpp"
#include "../integrity.hpp"

#include "../progress.hpp"
#include <El/Common/perfLog.hpp>
#include <El/Common/perfProf.hpp>

#include "../keyInput.hpp"
#include "../dikCodes.h"
#include "../UI/uiActions.hpp"
#include "../UI/missionDirs.hpp"
#include "../fileLocator.hpp"
#include "../gameDirs.hpp"

#include <El/CRC/crc.hpp>

#if _VBS3
  #include "../HLA/AAR.hpp"
#endif

//#include <sys/types.h>
#include <sys/stat.h>
#ifdef _WIN32
  #include <io.h>
#endif

#include <Es/Strings/bString.hpp>
#ifdef _CPPRTTI
  #include "typeinfo.h"
#endif

#include "El/Speech/debugVoN.hpp"

#if _ENABLE_GAMESPY && _VERIFY_KEY
#include "../GameSpy/gcdkey/gcdkeyc.h"
# ifdef NOMINMAX
#  undef min
#  undef max
# endif
#endif

// include HLA base
#if _EXT_CTRL //ext. Controller like HLA, AAR
  #include "../HLA/HLA_base.hpp"
#endif

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
# include "../saveGame.hpp"
# include "../X360/ArmA2.spa.h"
#endif

extern int MaxGroups;

/*!
\patch 1.87 Date 10/17/2002 by Jirka
- Fixed: Too many dead bodies were kept on server in 1.85.
*/

//! Time after that body can disappear (in seconds)
#define OLD_BODY                    10

//! Number of bodies on bot client
#define BODIES_PER_CLIENT           10

//! Time after that wreck can disappear (in seconds)
#define OLD_WRECK                   10

//! Number of wrecks on bot client
#define WRECKS_PER_CLIENT           5

//! enable diagnostic logs of message errors on client
#define LOG_CLIENT_ERRORS       0
//! limit for diagnostic logs of message errors on client
static float LogClientErrorLimit = 50.0f;

#define LOG_LIVE_STATS 1

/*!
\file
Basic implementation file for network client
*/

#define LOG_SEND_PROCESS 0

#if _SUPER_RELEASE
#define LOG_TEAM_SWITCH 0
#else
#define LOG_TEAM_SWITCH 1
#endif

const char *GameStateNames[] =
{
  "None",
  "Creating",
  "Create",
  "Login",
  "Edit",
  "Mission Voted",
  "Prepare Side",
  "Prepare Role",
  "Prepare OK",
  "Debriefing",
  "Debriefing OK",
  "Transfer Mission",
  "Load Island",
  "Briefing",
  "Play"
};

RStringB GetExtensionWizardMission();

//! Single item of respawn queue (list of units waiting for respawn)
class RespawnQueueItem
{
protected:
  //! position where respawn
  Vector3 _position;
  //! time when respawn
  Time _time;

public:
  RespawnQueueItem(Vector3Par position, Time time)
  {
    _position = position;
    _time = time;
  }

  Vector3Val GetPosition() const {return _position;}
  Time GetTime() const {return _time;}
  void SetTime(Time time) {_time = time;}
  virtual bool IsVehicleRespawn() const = 0;
  virtual bool IsPlayer() const {return false;}
  virtual Person *GetPerson() const {return NULL;}
  virtual Transport *GetVehicle() const {return NULL;}
  virtual bool DeleteOldBody() const {return false;}
};

class RespawnPersonQueueItem : public RespawnQueueItem
{
protected:
  //! person to respawn
  OLinkPerm<Person> _person;
  //! is it player unit
  bool _player;
  //! delete old body?
  bool _deleteOldBody;

public:
  RespawnPersonQueueItem(Vector3Par position, Time time, Person *person, bool player, bool deleteOldBody = false)
  : RespawnQueueItem(position, time)
  {
    _person = person;
    _player = player;
    _deleteOldBody = deleteOldBody;
  }
  virtual bool DeleteOldBody() const {return _deleteOldBody;}
  virtual bool IsVehicleRespawn() const {return false;}
  virtual bool IsPlayer() const {return _player;}
  virtual Person *GetPerson() const {return _person;}
};

class RespawnVehicleQueueItem : public RespawnQueueItem
{
protected:
  //! vehicle to respawn
  OLinkPerm<Transport> _vehicle;

public:
  RespawnVehicleQueueItem(Vector3Par position, Time time, Transport *vehicle)
    : RespawnQueueItem(position, time)
  {
    _vehicle = vehicle;
  }

  virtual bool IsVehicleRespawn() const {return true;}
  virtual Transport *GetVehicle() const {return _vehicle;}
};

void ParseFlashpointCfg(ParamFile &file);

#if _USE_BATTL_EYE_CLIENT

NetworkClient::BattlEye::BattlEye()
{
  CleanUp();
}

NetworkClient::BattlEye::~BattlEye()
{
  Done();
}

bool GetBELaunchPath(char *launchPath);

int NetworkClient::BattlEye::GetBEVersion(HMODULE library) const
{
  if (library)
  {
    GetVerF *getVer = (GetVerF *)GetProcAddress(library, "GetVer");
    if (getVer) return (*getVer)();
  }
  return 0;
}
 
bool NetworkClient::BattlEye::Init(NetworkClient *client)
{
  _initialized = false;
  const char *installPath = "Expansion\\BattlEye\\BEClient.dll";

  char launchPath[MAX_PATH];
  if (!GetBELaunchPath(launchPath)) return false;

  int curBEVersion = 0;
  if (QIFileFunctions::FileExists(installPath))
  {
    GFileServerFunctions->FlushReadHandle(installPath);
    // Get the BEClient.dll version from installPath first
    HMODULE installLibrary = LoadLibrary(installPath);
    int instVersion = GetBEVersion(installLibrary);
    if (installLibrary)
      FreeLibrary(installLibrary);
    
    CreatePath(launchPath);
    strcat(launchPath, "BEClient.dll");
    // try to load the dll from the cache
    _library = LoadLibrary(launchPath);
    if (_library)
    { // check whether the BE is updated to the last version
      curBEVersion = GetBEVersion(_library);
      if (instVersion>curBEVersion)
      {
        FreeLibrary(_library);
        _library = NULL;  // so it will be repaired from the install path
      }
    }
    if (!_library)
    {
      // install / repair the live copy from the install path
      chmod(launchPath, S_IREAD | S_IWRITE);
      unlink(launchPath);
      QIFileFunctions::Copy(installPath, launchPath);
      _library = LoadLibrary(launchPath);
      if (!_library) return false;
      RptF(Format("BattlEye client updated to version: %d", instVersion));
      curBEVersion = instVersion;
    }
  }
  else
  {
    // BattlEye removed, remove also from the cache
    strcat(launchPath, "BEClient.dll");
    chmod(launchPath, S_IREAD | S_IWRITE);
    unlink(launchPath);
    return false;
  }

  InitF *init = (InitF *)GetProcAddress(_library, "Init");
  sockaddr_in address;
  client->_client->GetServerAddress(address);

  _gameParams._address = address.sin_addr.s_addr;
  _gameParams._port = address.sin_port;
  _gameParams._printMessage = &PrintMessage;

  const int minBEVerReq = 164;
  if ((curBEVersion<minBEVerReq) || !init || !(*init)(0, &_gameParams, &_callbacks))
  {
    FreeLibrary(_library);
    CleanUp();
    if (curBEVersion<minBEVerReq) RptF("Obsolete/unsupported BattlEye version, please reinstall BE!");
    return false;
  }

  _initialized = true;
  return true;
}

void NetworkClient::BattlEye::Done()
{
  if (_initialized && _callbacks._exit)
  {
    (*_callbacks._exit)(); //call the Exit function before whenever you unload the dll, but only when the previous Init() call succeeded
    _initialized = false;
  }
  if (_library)
  {
    FreeLibrary(_library);
    _library = NULL;
  }
  CleanUp();
}

bool NetworkClient::BattlEye::Run(NetworkClient *client)
{
  if (!_callbacks._run) return true;
  if (!(*_callbacks._run)())
  {
    // auto-update
    if (_initialized && _callbacks._exit) (*_callbacks._exit)();
    FreeLibrary(_library);
    CleanUp();

    char path[MAX_PATH];
    if (!GetBELaunchPath(path)) return false;
    strcat(path, "BEClient.dll");

    chmod(path, S_IREAD | S_IWRITE);
    unlink(path);
    rename(RString(path) + RString(".new"), path);

    if (!Init(client)) return false;
  }
  return true;
}

void NetworkClient::BattlEye::Command(const char *command)
{
  if (_callbacks._command) (*_callbacks._command)(command);
}

template<> RString ImplicitMapTraits<RString>::null;
template<> RString ImplicitMapTraits<RString>::zombie = RString("zombie"); // any string will do, we need a unique instance of RString

bool NetworkClient::BattlEye::OnScriptExec(RString script)
{
  #if _ENABLE_CHEATS
  if (script && *script) 
  #else
  if (_callbacks._onScriptExec && script && *script) 
  #endif
  {
    RString exists;
    intptr_t key = KeyFromString(script);
    if (_checked.get(key,exists))
    {
      return true;
    }
    // TODO: maintain LRU instead of FIFO?

    const int maxFIFO = 200;
    _checkedFIFO.Add(key);
    if (_checkedFIFO.Size()>maxFIFO)
    {
      intptr_t keyToRemove = _checkedFIFO[0];
      _checkedFIFO.Delete(0,1);
      _checked.removeKey(keyToRemove);
    }
    _checked.put(script);
    if (_callbacks._onScriptExec)
    {
      (*_callbacks._onScriptExec)(script);
    }
    #if 0
    static int count = 0;
    RptF("BE Script: no.%d", ++count);
    RptF(script);
    #endif
    return true;
  }
#if 0
  if (script && *script)
  {
    static int count = 0;
    RptF("BE Script: no.%d", ++count);
    RptF(script);
  }
#endif
  return (_callbacks._onScriptExec==NULL); //when no callback is present, consider it as done
}

void NetworkClient::BattlEye::PrintMessage(const char *message)
{
  bool firstLine = true;
  const char *nl = NULL;
  while (nl = strchr(message, '\n'))
  {
    RString line(message, nl - message);
    if (firstLine)
    {
      GChatList.Add(CCBattlEye, NULL, RString("BattlEye Client: ") + line, false, true);
      firstLine = false;
    }
    else
    {
      GChatList.Add(CCBattlEye, NULL, RString("  ") + line, false, true);
    }
    message = nl + 1;
  }
  if (firstLine)
  {
    GChatList.Add(CCBattlEye, NULL, RString("BattlEye Client: ") + message, false, true);
    firstLine = false;
  }
  else
  {
    GChatList.Add(CCBattlEye, NULL, RString("  ") + message, false, true);
  }
}

#endif

// Network client itself

#if _XBOX_SECURE
NetworkClient::NetworkClient(NetworkManager *parent, const XNADDR &addr, const XNKID &kid, const XNKEY &key, int port, bool privateSlot, int playerRating)
: NetworkComponent(parent)
{
  _playerId = 0;
  _creatorId = CreatorId(0);
#if _USE_BATTL_EYE_CLIENT
  _beWanted = false;
#endif
  _localPlayerName = Glob.header.GetPlayerName();
#if _ENABLE_GAMESPY
  _publicAddr = 0;
#endif

#if _ENABLE_MP
  _playerRating = playerRating;
# if _XBOX_VER >= 200
  // Statistics handled by the NetworkManager
# else
  _writeStatsTask = NULL;

  if (!parent->IsSystemLink())
  {
    XONLINE_USER *user = XOnlineGetLogonUsers();
    Assert(user);
    if (user) _localPlayerName = user->szGamertag;
  }
# endif // _XBOX_VER >= 200
#endif

  GStats.ClearMission();

  Verify(Init(addr, kid, key, port, privateSlot));
  _clientState = NCSCreated;
  _serverState = NSSSelectingMission;
  _serverTimeout = INT_MAX;

  _connectionQuality = CQGood;
  _connectionDesync = CDGood;

  _missionFileValid = false;

  _teamSwitchDisabledUntil = 0;

  _gameMaster = false;
  _admin = false;
  _selectMission = false;
  _voteMission = false;

  _controlsPaused = false;

  //  _rvReceived = 0;
  _soundId = 0; // incremented for each PlaySound

  GetValue(_chatSound, Pars >> "CfgInGameUI" >> "Chat" >> "sound");

  // ensure no content remain
  DeleteDirectoryStructure(GetClientTmpDir(), false);

  _clientInfo = new ClientInfoObject();
  _clientCameraPosition = new ClientCameraPositionObject();
  
  _hideBodies = 0;
  _hideWrecks = 0;

  _voiceChannel = CCGlobal;

  _totalToCreate = -1;
  _remainingToCreate = -1;
  _totalToUpdate = -1;
  _remainingToUpdate = -1;
  _vonServerConnectType = PlayerIdentity::VONCT_UNINITIALIZED;
  
  _serverTime = _serverTimeOfEstimatedEnd = 0;
}

bool NetworkClient::Init(const XNADDR &addr, const XNKID &kid, const XNKEY &key, int serverPort, bool privateSlot)
{
#if _ENABLE_MP
  // load some settings from Flashpoint.cfg
  ParamFile cfg;
  ParseFlashpointCfg(cfg);

  _client = CreateNetClient(cfg);

  if (!_client)
  {
    _connectResult = CRError;
    return false;
  }
  
  _client->SetNetworkParams(cfg);
  
  int port = GetNetworkPort();

# if _XBOX_VER < 200
  _sessionKid = kid;
  // _xnaddr = addr;
  // retrieve local address
  DWORD status;
  do
  {
    status = XNetGetTitleXnAddr(&_xnaddr);
  } while (status == XNET_GET_XNADDR_PENDING);


  int res = XNetRegisterKey(&kid, &key);
  if (res != 0)
  {
    RptF("Client: XNetRegisterKey failed with error 0x%x", res);
    _connectResult = CRError;
    return false;
  }
# endif // _XBOX_VER < 200

  IN_ADDR inaddr;
  INT err = XNetXnAddrToInAddr(&addr, &kid, &inaddr);
  if (err != 0)
  {
    RptF("Client: XNetXnAddrToInAddr failed with error 0x%x", err);
    _connectResult = CRError;
    return false;
  }
  
  RString ip = Format
  (
    "%d.%d.%d.%d:%d",
    inaddr.S_un.S_un_b.s_b1,
    inaddr.S_un.S_un_b.s_b2,
    inaddr.S_un.S_un_b.s_b3,
    inaddr.S_un.S_un_b.s_b4,
    serverPort
  );

  MPVersionInfo versionInfo;
  versionInfo.versionActual = MP_VERSION_ACTUAL;
  versionInfo.versionRequired = MP_VERSION_REQUIRED;
  versionInfo.mission[0] = 0;
  versionInfo.serverState = NSSSelectingMission;
#if USE_MOD_LIST
  strncpy(versionInfo.mod, GetModListNames(), MOD_LENGTH);
#else
  strncpy(versionInfo.mod, GLoadedContentHash, MOD_LENGTH);
#endif
  versionInfo.mod[MOD_LENGTH - 1] = 0;
  RString playerName = _localPlayerName;
  _connectResult = _client->Init
  (
    ip, RString(), false, privateSlot, port, playerName, versionInfo, MAGIC_APP
  );
  if (_connectResult != CROK)
  {
    _client = NULL;
    return false;
  }

  InitVoice();
  
  char buffer[256];
  sprintf(buffer, "Tmp%d", port);
  ServerTmpDir = buffer;

  return true;
  
  #else
  return false;
  #endif
}

#else
/*!
\patch 1.45 Date 3/1/2002 by Jirka
- Fixed: Sometimes client has problems connect to server
\patch 1.89 Date 10/22/2002 by Ondra
- Fixed: Chat sound more quiet.
\patch 5140 Date 3/15/2007 by Bebul
- Fixed: VoN issues with server behind firewall fixed
*/

NetworkClient::NetworkClient(NetworkManager *parent, RString address, RString password)
: NetworkComponent(parent)
{
#if DEBUG_CREATOR_ID
  _debugCreatorIdSet = false;
#endif
  _playerID = 0;
  _creatorId = CreatorId(0);
#if _VBS2 // VBS2 nickname
  _localPlayerName = Glob.header.playerHandle;
#else
  _localPlayerName = Glob.header.GetPlayerName();
#endif

#if _VBS3 // player color
  _sentPlayerColour = false;
#endif

#if _USE_BATTL_EYE_CLIENT
  _beWanted = false;
#endif

#if _ENABLE_GAMESPY
  _publicAddr = 0;
  const char *ptr = address;
  const char *p = strrchr(ptr, '#');
  if (p)
  {
    // NAT Negotiation is needed
    _natNegotiationNeeded = true;
  }
  else _natNegotiationNeeded = false;
#else
  _natNegotiationNeeded = false;
#endif

  GStats.ClearMission();

  Verify(Init(address, password));
  _clientState = NCSCreated;
  _serverState = NSSSelectingMission;
  _serverTimeout = INT_MAX;

  _connectionQuality = CQGood;
  _connectionDesync = CDGood;

  _missionFileValid = false;

  _teamSwitchDisabledUntil = 0;

  _gameMaster = false;
  _admin = false;
  _selectMission = false;
  _voteMission = false;

  _controlsPaused = false;

  //  _rvReceived = 0;
  _soundId = 0; // incremented for each PlaySound

  GetValue(_chatSound, Pars >> "CfgInGameUI" >> "Chat" >> "sound");

  // ensure no content remain
  DeleteDirectoryStructure(GetClientTmpDir(), false);

  _clientInfo = new ClientInfoObject(); // regular client
  _clientCameraPosition = new ClientCameraPositionObject();
  
  _hideBodies = 0;
  _hideWrecks = 0;

  memset(&_serverVoNAddr,  0, sizeof(_serverVoNAddr));

  _totalToCreate = -1;
  _remainingToCreate = -1;
  _totalToUpdate = -1;
  _remainingToUpdate = -1;

  _voiceChannel = CCGlobal;

  _serverTime = _serverTimeOfEstimatedEnd = 0;
}

bool NetworkClient::Init(RString address, RString password)
{
  // load some settings from Flashpoint.cfg
  ParamFile cfg;
  ParseFlashpointCfg(cfg);

  //Direct play has been eliminated from here
  _client = CreateNetClient(cfg);

  if (!_client)
  {
    _connectResult = CRError;
    return false;
  }
  
  _client->SetNetworkParams(cfg);
  
  int port = GetNetworkPort();

  MPVersionInfo versionInfo;
  versionInfo.versionActual = MP_VERSION_ACTUAL;
  versionInfo.versionRequired = MP_VERSION_REQUIRED;
  versionInfo.mission[0] = 0;
  versionInfo.serverState = NSSSelectingMission;
#if USE_MOD_LIST
  RString GetModListNames();
  strncpy(versionInfo.mod, GetModListNames(), MOD_LENGTH);
#else
  strncpy(versionInfo.mod, GLoadedContentHash, MOD_LENGTH);
#endif
  versionInfo.mod[MOD_LENGTH - 1] = 0;
#if _VBS2 // VBS2 nickname
  RString playerName = Glob.header.playerHandle;
#else
  RString playerName = Glob.header.GetPlayerName();
#endif
  extern bool KeepAppActive();
  _connectResult = _client->Init
  (
    address, password, false, port, playerName, versionInfo, MAGIC_APP, KeepAppActive
  );
  if (_connectResult != CROK)
  {
    _client = NULL;
    return false;
  }

  InitVoice();
  
  char buffer[256];
  sprintf(buffer, "Tmp%d", port);
  ServerTmpDir = buffer;

  return true;
}
#endif

NetworkLocalObjectInfo ImplicitMapTraits<NetworkLocalObjectInfo>::zombie = {NetworkId(CreatorId(-1),-1)};
NetworkLocalObjectInfo ImplicitMapTraits<NetworkLocalObjectInfo>::null= {NetworkId(CreatorId(0),0)};

NetworkRemoteObjectInfo ImplicitMapTraits<NetworkRemoteObjectInfo>::zombie = {NetworkId(CreatorId(-1),-1)};
NetworkRemoteObjectInfo ImplicitMapTraits<NetworkRemoteObjectInfo>::null = {NetworkId(CreatorId(0),0)};

NetworkClient::NetworkClient(NetworkManager *parent, NetworkServer *server, int playerRating)
: NetworkComponent(parent)
{
#if DEBUG_CREATOR_ID
  _debugCreatorIdSet = false;
#endif
  _playerID = 0;
  _creatorId = CreatorId(0);
#if _USE_BATTL_EYE_CLIENT
  _beWanted = false;
#endif

#if _ENABLE_GAMESPY
  _publicAddr = 0;
  _natNegotiationNeeded = false;
#endif

#if _VBS2 // VBS2 nickname
  _localPlayerName = Glob.header.playerHandle;
#else
  _localPlayerName = Glob.header.GetPlayerName();
#endif

#if _XBOX_SECURE && _ENABLE_MP
  _playerRating = playerRating;

# if _XBOX_VER >= 200
  // Statistics handled by the NetworkManager
# else
  _writeStatsTask = NULL;

  if (!parent->IsSystemLink())
  {
    XONLINE_USER *user = XOnlineGetLogonUsers();
    Assert(user);
    if (user) _localPlayerName = user->szGamertag;
  }
# endif // _XBOX_VER >= 200
  
#endif // _XBOX_SECURE && _ENABLE_MP

  GStats.ClearMission();

  Verify(Init(server));
  _clientState = NCSCreated;
  _serverState = NSSSelectingMission;
  _serverTimeout = INT_MAX;

  _connectionQuality = CQGood;
  _connectionDesync = CDGood;

  _missionFileValid = false;

  _teamSwitchDisabledUntil = 0;

  _gameMaster = false;
  _admin = false;
  _selectMission = false;
  _voteMission = false;

  _controlsPaused = false;

  //  _rvReceived = 0;
  _soundId = 0; // incremented for each PlaySound

  GetValue(_chatSound, Pars >> "CfgInGameUI" >> "Chat" >> "sound");

  // ensure no content remain
  DeleteDirectoryStructure(GetClientTmpDir(), false);

  _clientInfo = new ClientInfoObject();
  _clientCameraPosition = new ClientCameraPositionObject();
  
  _hideBodies = 0;
  _hideWrecks = 0;

  #if _EXT_CTRL //ext. Controller like HLA, AAR
    //connect to HLA, this is only called for BOTCLIENT
    _hla.Connect(this);
  #endif

  memset(&_serverVoNAddr,  0, sizeof(_serverVoNAddr));

  _totalToCreate = -1;
  _remainingToCreate = -1;
  _totalToUpdate = -1;
  _remainingToUpdate = -1;

  _voiceChannel = CCGlobal;

  _serverTime = _serverTimeOfEstimatedEnd = 0;
}

// Init with NetworkServer parameter is called to create BOT_CLIENT
bool NetworkClient::Init(NetworkServer *server)
{
  // load some settings from Flashpoint.cfg
  ParamFile cfg;
  ParseFlashpointCfg(cfg);

  _client = CreateNetClient(cfg);

  if (!_client)
  {
    _connectResult = CRError;
    return false;
  }
  
  _client->SetNetworkParams(cfg);
  
  int port = GetNetworkPort();

  // connect bot client directly
  _player = BOT_CLIENT;
  _connectResult = CROK;
  
#if _ENABLE_DEDICATED_SERVER
  //if (!IsDedicatedServer())  
#endif
    InitVoice(true);
  
  char buffer[256];
  sprintf(buffer, "Tmp%d", port);
  ServerTmpDir = buffer;

#if _XBOX_SECURE && _ENABLE_MP && _XBOX_VER < 200
  DoAssert(server);
  server->GetSessionInfo(_xnaddr, _sessionKid);
#endif
  return true;
}

void NetworkClient::DisableVoice(bool disable)
{
  #if _GAMES_FOR_WINDOWS || defined _XBOX
  _client->VoiceCapture(!disable);
  #endif
}

#if _ENABLE_GAMESPY

#include "../GameSpy/natneg/natneg.h"
# ifdef NOMINMAX
#  undef min
#  undef max
# endif

static bool CheckRawVoiceMessage(char *data, int len, struct sockaddr_in *from)
{
  if (len >= NATNEG_MAGIC_LEN && memcmp(data, NNMagicData, NATNEG_MAGIC_LEN) == 0)
  {
#if !_SUPER_RELEASE
    int port = from->sin_port;
    LogF("*** NAT Message arrived from %s:%d, length %d bytes", inet_ntoa(from->sin_addr), ntohs(port), len);
#endif
    // NAT Negotiation message - pass to GameSpy SDK
    enterNN();
    NNProcessData(data, len, from);
    leaveNN();
    return true; // processed
  }
  return false;
}
#else
static bool CheckRawVoiceMessage(char *data, int len, struct sockaddr_in *from)
{
  return false;
}
#endif

void NetworkClient::InitVoice(bool isBotClient)
{
#if _XBOX_SECURE && _ENABLE_MP
  if (!_parent->IsSystemLink())
  {
# if _XBOX_VER >= 200
    // TODOXNET: Voice enabled (check privileges)
# else
    XONLINE_USER *user = XOnlineGetLogonUsers();
    Assert(user);
    if (!user || user->xuid.dwUserFlags & XONLINE_USER_VOICE_NOT_ALLOWED) return;
# endif // _XBOX_VER >= 200
  }
#endif

  ConstParamEntryPtr entry = (Pars >> "CfgVoiceMask").FindEntry(Glob.header.voiceMask);
  if (entry)
  {
    VoiceMask mask;
    mask.specEnergyWeight = (*entry) >> "specEnergyWeight";
    mask.pitchScale = (*entry) >> "pitchScale";
    mask.whisperValue = (*entry) >> "whisperValue";
    mask.roboticValue = (*entry) >> "roboticValue";
    _client->InitVoice(&mask, &CheckRawVoiceMessage, isBotClient);
  }
  else _client->InitVoice(NULL, &CheckRawVoiceMessage, isBotClient);
#if _GAMES_FOR_WINDOWS || defined _XBOX
  _client->VoiceCapture(true);
#endif
}

static void TargetFromIdentity(TransmitTarget &tt, const PlayerIdentity &id, int extInfo)
{
  // add to transmit targets
  tt.addr = id._voiceAddress;
  tt.dpid = id.dpnid;

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  tt.xuid = identity->xuid;
#endif
  tt.extInfo = extInfo;
}


/*!
\patch 5108 Date 12/20/2006 by Bebul
- Fixed: Player hosting game was not able to hear voice over net
*/
void NetworkClient::ConnectVoice(int player, sockaddr_in &addr)
{
  // no else here - BOT_CLIENT may be both server and client
  PlayerIdentity *identity = FindIdentity(player);
  if (identity) identity->_voiceAddress = addr;
  if (player==BOT_CLIENT)
  {
    _serverVoNAddr = addr;
  }
#if _ENABLE_MP //binarize was not linkable
  VoiceStartKeepAlive(true, player); //keepAlive is constructed as inactive by default
#endif
}

void NetworkClient::ChangeVoiceMask(RString mask)
{
  if (!_client) return;
  ConstParamEntryPtr entry = (Pars >> "CfgVoiceMask").FindEntry(mask);
  if (entry)
  {
    VoiceMask mask;
    mask.specEnergyWeight = (*entry) >> "specEnergyWeight";
    mask.pitchScale = (*entry) >> "pitchScale";
    mask.whisperValue = (*entry) >> "whisperValue";
    mask.roboticValue = (*entry) >> "roboticValue";
    _client->ChangeVoiceMask(&mask);
  }
}

NetworkClient::~NetworkClient()
{
  #if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient())
  {
  _hla.Disconnect();
  }
  #endif

  RemoveSystemMessages();

  Done();
  DeleteDirectoryStructure(GetClientTmpDir(), true);

#if _ENABLE_CHEATS
  if (_debugWindow) _debugWindow->Close();
#endif
}

void NetworkClient::Done()
{
  // _dp->CancelAsyncOperation(NULL, DPNCANCEL_ALL_OPERATIONS);
  RemoveUserMessages();

  _soundBuffers.Clear();

#if _ENABLE_STEAM
  if (UseSteam)
  {
    if (SteamUser() && _client)
    {
      in_addr addr;
      int port;
      if (_player == BOT_CLIENT)
      {
        addr.s_addr = htonl(0x7F000001); // localhost
        port = GetServerPort();
      }
      else
      {
        _client->GetDistantAddress(addr, port);
      }
      SteamUser()->TerminateGameConnection(ntohl(addr.s_addr), ntohs(port));
    }
  }
#endif

  _client = NULL;

  GDebugVoNBank.Reset(true); // all VoN/Net diagnostics should be reset

#if _XBOX_SECURE && _ENABLE_MP

# if _XBOX_VER >= 200
  // Statistics handled by the NetworkManager
# else
  if (_writeStatsTask)
  {
    // finish the task
    HRESULT result;
    do
    {
      result = XOnlineTaskContinue(_writeStatsTask);
    } while(result == XONLINETASK_S_RUNNING);
    XOnlineTaskClose(_writeStatsTask);
    _writeStatsTask = NULL;
#if LOG_LIVE_STATS
    if (FAILED(result))
      RptF(" - XOnlineStatWrite update failed with error 0x%x", result);
    else
      RptF(" - Statistics written");
#endif
  }
  if (_player != BOT_CLIENT)
    XNetUnregisterKey(&_sessionKid);
# endif // _XBOX_VER >= 200
#endif
}

void NetworkClient::CreateDebugWindow()
{
#if _ENABLE_CHEATS
  _debugWindow = new DebugListWindow("Client debug");
#endif
}

int CmpBodies
(
  const BodyInfo *info1,
  const BodyInfo *info2
)
{
  float diff = info1->value - info2->value;
  return sign(diff);
}

/*!
\patch 1.56 Date 5/10/2002 by Jirka
- Improved: New algorithm for removing bodies in MP game
\patch 1.85 Date 9/23/2002 by Jirka
- Fixed: Algorithm for removing bodies in MP game improved
*/

void NetworkClient::DisposeBody(Person *body)
{
  DoAssert(body->IsLocal());

  // Add body to queue
  int index = _bodies.Add();
  BodyInfo &bodyInfo = _bodies[index];
  bodyInfo.body = body;
  bodyInfo.hideTime = Glob.time + OLD_BODY;
  bodyInfo.value = 0;

  // Recalculate body values
  for (int i=0; i<_bodies.Size(); i++)
  {
    BodyInfo &bodyInfo = _bodies[i];
    if (!bodyInfo.body)
    {
      _bodies.Delete(i);
      i--;
      continue;
    }
    bodyInfo.value = 0;
    TurretContext context;
    DoVerify(bodyInfo.body->GetPrimaryGunnerTurret(context));
    for (int w=0; w<context._weapons->_weapons.Size(); w++)
    {
      const WeaponType *weapon = context._weapons->_weapons[w];
      if (weapon) bodyInfo.value += weapon->_value;
    }
    for (int m=0; m<context._weapons->_magazines.Size(); m++)
    {
      const Magazine *magazine = context._weapons->_magazines[m];
      const MagazineType *type = magazine ? magazine->_type : NULL;
      if (type) bodyInfo.value += type->_value;
    }
    if (bodyInfo.body->GetFlagCarrier()) bodyInfo.value += 1000;
  }

  // Sort values
  QSort(_bodies.Data(), _bodies.Size(), CmpBodies);

  // Remove old and unvaluable bodies
  for (int i=0; i<_bodies.Size(); i++)
  {
    if (_hideBodies <= 0) break;
    
    BodyInfo &bodyInfo = _bodies[i];
    if (bodyInfo.value >= 1000) break;
    if (bodyInfo.hideTime > Glob.time) continue;

    DoAssert(bodyInfo.body->IsLocal());
    bodyInfo.body->HideBody();
    _bodies.Delete(i);
    _hideBodies--;
    i--;
  }
}

void NetworkClient::DisposeWreck(Transport *wreck)
{
  DoAssert(wreck->IsLocal());

  // Add wreck to queue
  int index = _wrecks.Add();
  WreckInfo &wreckInfo = _wrecks[index];
  wreckInfo.wreck = wreck;
  wreckInfo.hideTime = Glob.time + OLD_WRECK;

  // Remove old wrecks
  for (int i=0; i<_wrecks.Size(); i++)
  {
    if (_hideWrecks <= 0) break;

    WreckInfo &wreckInfo = _wrecks[i];
    if (wreckInfo.hideTime > Glob.time) continue;

    DoAssert(wreckInfo.wreck->IsLocal());
    wreckInfo.wreck->SetDelete();
    _wrecks.Delete(i);
    _hideWrecks--;
    i--;
  }
}

// Respawn soldier postpone EH wrapper
class EventRetVector3CallP: public IPostponedEvent
{
  NetworkClient *_client;
  OLinkO(Person) _soldier;
  OLinkO(Person) _body;
  bool _deleteOldBody;

public:
  EventRetVector3CallP(NetworkClient *client, Soldier *soldier, Person *body, bool deleteOldBody)
    : _client(client), _soldier(soldier), _body(body), _deleteOldBody(deleteOldBody) {}

  void DoEvent() const
  {
    DoAssert(_client);

    if (!_soldier || !_body) return;

    // respawn command
    Vector3 respawnPosition = _soldier->OnEventRetVector3(EERespawn, _body);

    const AutoArray<RString> &handlers = _soldier->GetMPEventHandlers(EEMPRespawn);
    if (handlers.Size()>0) //there are some event handlers => trigger the MP Event on all clients
    {
      GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
      GameArrayType &arguments = value;
      Person *soldier = dyn_cast<Person>(_soldier.GetLink());
      Person *body = dyn_cast<Person>(_body.GetLink());
      arguments.Add(GameValueExt(soldier));
      arguments.Add(GameValueExt(body));
      GetNetworkManager().SendMPEvent(EEMPRespawn, value);
      // and trigger the MP Event here
      respawnPosition = _soldier->OnMPEventRetVector3(EEMPRespawn, value);
    }

    // synchronize MPEventHandlers on other clients
    for (int i=0; i<NMPEntityEvent; i++)
    {
      const AutoArray<RString> &eventHandlers = _soldier->GetMPEventHandlers((MPEntityEvent)i);
      if (eventHandlers.Size())
      {
        GetNetworkManager().OnVehMPEventHandlersChanged(_soldier, i, eventHandlers);
      }
    }

    if (respawnPosition.SquareSize() > 10.0f) 
    {
      respawnPosition[1] = GLandscape->RoadSurfaceYAboveWater(respawnPosition + VUp * 0.5f);
      _soldier->Move(respawnPosition);
      _client->UpdateObject(_soldier, NMFGuaranteed);
    }

    _client->DeleteBody(_body, _deleteOldBody);
  }

  USE_FAST_ALLOCATOR
};

/*!
\patch 1.27 Date 10/11/2001 by Jirka
- Fixed: experience after respawn cannot be lower than initial experience.
This prevents AI from killing Team Killers after they respawn.
\patch 1.30 Date 11/06/2001 by Jirka
- Fixed: respawn when unit is inside remote vehicle
*/

Person *NetworkClient::DoRespawn(RespawnQueueItem *item)
{
  if (_clientState != NCSBriefingRead) return NULL;  // send event messages only when playing

  Person *body = item->GetPerson();
  if (!body)
  {
    Fail("Respawn failed - body disappeared");
    return NULL;
  }
  AIBrain *unit = body->Brain();
  if (!unit)
  {
    Fail("Respawn failed - unit disappeared");
    return NULL;
  }
  TargetSide side = body->Vehicle::GetTargetSide();
  int id = -1;
  for (int i=0; i<vehiclesMap.Size(); i++)
    if (vehiclesMap[i] == body)
    {
      id = i;
      break;
    }

  // new body
  Soldier *soldier = new Soldier(const_cast<VehicleType *>(body->GetType()));
  soldier->SetID(GLandscape->NewObjectID());
  
  // move brain
  soldier->SetBrain(unit);
  body->SetBrain(NULL);
  GLOB_WORLD->RemoveSensor(body);
  unit->SetPerson(soldier);
  //add ID of body to respawn histary
  unit->AddObjectIDHistory(body->GetObjectId().Encode());
  unit->SetVehicleIn(NULL);
  
  // other parameters
  soldier->SetTargetSide(side);
  unit->SetLifeState(LifeStateAlive);
  soldier->EntityAI::GetVars()->_vars = body->EntityAI::GetVars()->_vars; // keep variables (some scripting modules requires it)

  AIUnitInfo &info = body->GetInfo();
  saturateMax(info._experience, info._initExperience);
  soldier->SetInfo(info);
  body->SetFace();
  body->SetGlasses();

#if _ENABLE_IDENTITIES
  // transfer the identity to the new person
  soldier->CopyIdentity(body);
#endif

  RString var = body->GetVarName();
  if (var.GetLength() > 0)
  {
    soldier->SetVarName(var);
    GWorld->GetGameState()->VarSet(var, GameValueExt(soldier), true, false, GWorld->GetMissionNamespace()); // mission namespace
  }

  // event handlers
  for (int i=0; i<NEntityEvent; i++)
  {
    EntityEvent event = (EntityEvent)i;
    const EventHandlers &handlers = body->GetEventHandlers(event);
    soldier->ClearEventHandlers(event);
    soldier->SetEventHandlers(event, handlers);
  }

  // MP event handlers
  for (int i=0; i<NMPEntityEvent; i++)
  {
    MPEntityEvent event = (MPEntityEvent)i;
    const AutoArray<RString> &handlers = body->GetMPEventHandlers(event);
    soldier->ClearMPEventHandlers(event);
    for (int j=0; j<handlers.Size(); j++) soldier->AddMPEventHandler(event, handlers[j]);
  }

  // weapons
  soldier->RemoveAllWeapons(false); // keep the items from the type
  soldier->RemoveAllMagazines();
  soldier->AddDefaultWeapons();

  ParamEntryPtr entryWeapons;
  ParamEntryPtr entryMagazines;
  switch (side)
  {
  case TWest:
    entryWeapons = ExtParsMission.FindEntry("respawnWeaponsWest");
    entryMagazines = ExtParsMission.FindEntry("respawnMagazinesWest");
    break;
  case TEast:
    entryWeapons = ExtParsMission.FindEntry("respawnWeaponsEast");
    entryMagazines = ExtParsMission.FindEntry("respawnMagazinesEast");
    break;
  case TGuerrila:
    entryWeapons = ExtParsMission.FindEntry("respawnWeaponsGuer");
    entryMagazines = ExtParsMission.FindEntry("respawnMagazinesGuer");
    break;
  case TCivilian:
    entryWeapons = ExtParsMission.FindEntry("respawnWeaponsCiv");
    entryMagazines = ExtParsMission.FindEntry("respawnMagazinesCiv");
    break;
  }
  if (entryWeapons && entryMagazines)
  {
    for (int i=0; i<entryMagazines->GetSize(); i++)
    {
      RString name = (*entryMagazines)[i];
      soldier->AddMagazine(name);
    }
    for (int i=0; i<entryWeapons->GetSize(); i++)
    {
      RString name = (*entryWeapons)[i];
      soldier->AddWeapon(name);
    }
  }
  else soldier->AddRespawnWeapons();

  // add to world
  Vector3 position = item->GetPosition();
  unit->FindNearestEmpty(position);
  position[1] = GLandscape->RoadSurfaceYAboveWater(position + VUp * 0.5f);

  Matrix4 pos;
  pos.SetOrientation(Matrix3(MRotationY,-H_PI*2*GRandGen.RandomValue()));
  pos.SetPosition(position);
  soldier->SetTransform(pos);
  soldier->Init(pos, true);
  GLOB_WORLD->AddVehicle(soldier);
  if (id >= 0) vehiclesMap[id] = soldier;

  CreateVehicle(soldier, VLTVehicle, var, id);
  AttachPerson(soldier);

LogF
(
  "*** Identity %s transferred from %d:%d to %d:%d",
  (const char *)info._name,
  body->GetNetworkId().creator.CreatorInt(), body->GetNetworkId().id,
  soldier->GetNetworkId().creator.CreatorInt(), soldier->GetNetworkId().id
);

  // FIX: try to asure player is valid whenever unit is respawned
  if (item->IsPlayer())
  {
    SelectPlayer
    (
      _player, soldier, true
    );
#if _VBS2 // allow seagull difficulty option
    if (!Glob.config.IsEnabled(DTAllowSeagull) && GWorld->GetTitleEffect())
    {
      GWorld->GetTitleEffect()->Terminate();
      GWorld->LockTitleEffect(false);
    }
#endif
#if _VBS3 // player color
    RString FindPicture (RString name);
    Ref<Texture> texture = GlobLoadTexture(FindPicture(Glob.playerColor));
    if (texture && !texture->IsAlpha())
    {
      int index = soldier->GetType()->GetHiddenSelectionIndex("hide_teamcolor");
      if(index > -1)
      {
        soldier->SetObjectTexture(index, texture);
        GetNetworkManager().SetObjectTexture(soldier,index,FindPicture(Glob.playerColor),"side player == side _this");
      }
    }
#endif
  }

  /// Create backpack if needed
  if(soldier) 
    AICenter::CreateBackpack(soldier, true);
  //////////////////////////

  UpdateObject(soldier, NMFGuaranteed);
  UpdateObject(unit, NMFGuaranteed);

  unit->OnRespawnFinished();

  GWorld->AppendPostponedEvent(new EventRetVector3CallP(this, soldier, body, item->DeleteOldBody()));
  
  LogF
    (
    "Client: Respawn processed - unit %s (%d:%d), old body %d:%d, new body %d:%d",
    (const char *)unit->GetDebugName(),
    unit->GetNetworkId().creator.CreatorInt(), unit->GetNetworkId().id,
    body->GetNetworkId().creator.CreatorInt(), body->GetNetworkId().id,
    soldier->GetNetworkId().creator.CreatorInt(), soldier->GetNetworkId().id
    );

  return soldier;
}

void NetworkClient::DeleteBody(Person *body, bool deleteOldBody)
{
  if (!deleteOldBody)
  {
    // single body can disappear
    _hideBodies++;

    // Add body to queue
    DisposeBody(body);
  }
  else
  {
    NetworkId bodyId = body->GetNetworkId();
    GetNetworkManager().DeleteObject(bodyId);
    body->SetDelete();
  }
}

// Respawn transport postpone EH wrapper
class EventRetVector3CallT: public IPostponedEvent
{
  NetworkClient *_client;
  OLinkO(Transport) _vehicle;
  OLinkO(Transport) _wreck;

public:
  EventRetVector3CallT(NetworkClient *client, Transport *vehicle, Transport *wreck)
    : _client(client), _vehicle(vehicle), _wreck(wreck) {}

  void DoEvent() const
  {   
    DoAssert(_client);

    if (!_vehicle || !_wreck) return;

    // respawn vehicle at specific coordinates
    Vector3 respawnPosition = _vehicle->OnEventRetVector3(EERespawn, _wreck);

    const AutoArray<RString> &handlers = _vehicle->GetMPEventHandlers(EEMPRespawn);
    if (handlers.Size()>0) //there are some event handlers => trigger the MP Event on all clients
    {
      GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
      GameArrayType &arguments = value;
      Transport *veh = dyn_cast<Transport>(_vehicle.GetLink());
      Transport *wreck = dyn_cast<Transport>(_wreck.GetLink());
      arguments.Add(GameValueExt(veh));
      arguments.Add(GameValueExt(wreck));
      GetNetworkManager().SendMPEvent(EEMPRespawn, value);
      // and trigger the MP Event here
      respawnPosition = _vehicle->OnMPEventRetVector3(EEMPRespawn, value);
    }

    // synchronize MPEventHandlers on other clients
    for (int i=0; i<NMPEntityEvent; i++)
    {
      const AutoArray<RString> &eventHandlers = _vehicle->GetMPEventHandlers((MPEntityEvent)i);
      if (eventHandlers.Size())
      {
        GetNetworkManager().OnVehMPEventHandlersChanged(_vehicle, i, eventHandlers);
      }
    }

    if (respawnPosition.SquareSize() > 10.0f) 
    {
      Matrix4 pos = _vehicle->FutureVisualState().Transform();

      pos.SetPosition(respawnPosition);

      if (_vehicle->CanFloat())
        _vehicle->PlaceOnSurface(pos);
      else
        _vehicle->PlaceOnSurfaceUnderWater(pos);

      _vehicle->Move(pos);
      _client->UpdateObject(_vehicle, NMFGuaranteed);
    }

    _client->DeleteWreck(_wreck);
  }

  USE_FAST_ALLOCATOR
};

void NetworkClient::DeleteWreck(Transport *wreck)
{
  _hideWrecks++;
  DisposeWreck(wreck);
}

Transport *NetworkClient::DoVehicleRespawn(RespawnQueueItem *item)
{
  if (_clientState != NCSBriefingRead) return NULL;  // send event messages only when playing

  Transport *wreck = item->GetVehicle();
  if (!wreck)
  {
    Fail("Respawn failed - wreck disappeared");
    return NULL;
  }

  int id = -1;
  for (int i=0; i<vehiclesMap.Size(); i++)
  {
    if (vehiclesMap[i] == wreck)
    {
      id = i;
      break;
    }
  }

  // new vehicle
  EntityAI *veh = GWorld->NewVehicleWithID(unconst_cast(wreck->GetType()));
  Transport *vehicle = dyn_cast<Transport>(veh);
  Assert(vehicle);

  // parameters
  int count = wreck->GetRespawnCount();
  if (count != 1)
  {
    vehicle->SetRespawnSide(wreck->GetRespawnSide());
    vehicle->SetRespawnDelay(wreck->GetRespawnDelay());
    if (count > 1) count--;
    vehicle->SetRespawnCount(count);
  }
  wreck->SetRespawnSide(TSideUnknown); // do not respawn again
  vehicle->SetRespawnFlying(wreck->IsRespawnFlying());
  AIBrain *respawnUnit = wreck->GetRespawnUnit();
  if (respawnUnit)
  {
    vehicle->SetRespawnUnit(respawnUnit);
LogF
(
  "Client: new respawn vehicle %s (%d:%d) attached to unit %s (%d:%d)",
  (const char *)vehicle->GetDebugName(),
  vehicle->GetNetworkId().creator.CreatorInt(), vehicle->GetNetworkId().id,
  (const char *)respawnUnit->GetDebugName(),
  respawnUnit->GetNetworkId().creator.CreatorInt(), respawnUnit->GetNetworkId().id
);
  }
  wreck->SetRespawning(false);
  wreck->SetRespawnUnit(NULL);

  RString var = wreck->GetVarName();
  if (var.GetLength() > 0)
  {
    vehicle->SetVarName(var);
    GWorld->GetGameState()->VarSet(var, GameValueExt(vehicle), true, false, GWorld->GetMissionNamespace()); // mission namespace
  }

  // event handlers
  for (int i=0; i<NEntityEvent; i++)
  {
    EntityEvent event = (EntityEvent)i;
    const EventHandlers &handlers = wreck->GetEventHandlers(event);
    vehicle->ClearEventHandlers(event);
    vehicle->SetEventHandlers(event, handlers);
  }

  // MP event handlers
  for (int i=0; i<NMPEntityEvent; i++)
  {
    MPEntityEvent event = (MPEntityEvent)i;
    const AutoArray<RString> &handlers = wreck->GetMPEventHandlers(event);
    vehicle->ClearMPEventHandlers(event);
    for (int j=0; j<handlers.Size(); j++) vehicle->AddMPEventHandler(event, handlers[j]);
  }

  // add to world
  Vector3 position = item->GetPosition();
  AIUnit::FindNearestEmpty(position, false, vehicle);

  Matrix4 pos;
  pos.SetOrientation(Matrix3(MRotationY,-H_PI*2*GRandGen.RandomValue()));
  pos.SetPosition(position);
  if (vehicle->CanFloat())
  vehicle->PlaceOnSurface(pos);
  else
    vehicle->PlaceOnSurfaceUnderWater(pos);
  vehicle->SetTransform(pos);
  vehicle->Init(pos, true);
  GLOB_WORLD->AddVehicle(vehicle);
  if (id >= 0) vehiclesMap[id] = vehicle;

  UpdateObject(wreck, NMFGuaranteed);
  CreateVehicle(vehicle, VLTVehicle, var, id);

  UpdateObject(vehicle, NMFGuaranteed);
  if (respawnUnit) UpdateObject(respawnUnit, NMFGuaranteed);

  GWorld->AppendPostponedEvent(new EventRetVector3CallT(this, vehicle, wreck));

  LogF
  (
    "Client: Vehicle respawn processed - old vehicle %s %d:%d, new vehicle %s %d:%d",
    (const char *)wreck->GetDebugName(), wreck->GetNetworkId().creator.CreatorInt(), wreck->GetNetworkId().id,
    (const char *)vehicle->GetDebugName(), vehicle->GetNetworkId().creator.CreatorInt(), vehicle->GetNetworkId().id
  );

  return vehicle;
}

void NetworkClient::ProcessRespawns()
{
  for (int i=0; i<_respawnQueue.Size();)
  {
    RespawnQueueItem *item = _respawnQueue[i];
    if (item->GetTime() > Glob.time)
    {
      i++;
      continue;
    }

    if (item->IsVehicleRespawn())
    {
      Transport *vehicle = DoVehicleRespawn(item);
      if (vehicle && !vehicle->Driver())
      {
        AIBrain *unit = vehicle->GetRespawnUnit();
        if (unit)
        {
          DoAssert(unit->IsLocal());
          if (unit->GetLifeState() == LifeStateDeadInRespawn)
          {
            // respawn out of order
            for (int j=0; j<_respawnQueue.Size(); j++)
            {
              RespawnQueueItem *item = _respawnQueue[j];
              if (item->IsVehicleRespawn()) continue;
              Person *person = item->GetPerson();
              if (person && person->Brain() == unit)
              {
                DoRespawn(item);
                _respawnQueue.Delete(j);
                if (j < i) i--;
                break;
              }
            }
          }
          // get in directly
          vehicle->GetInDriver(unit->GetPerson(), false);
          unit->AssignAsDriver(vehicle);
          unit->OrderGetIn(true);
          if (unit == GWorld->FocusOn())
          {
            CameraType ValidateCamera(CameraType cam);
            GWorld->SwitchCameraTo(vehicle, ValidateCamera(GWorld->GetCameraType()), false);
          }
          // make it flying
          if (vehicle->IsRespawnFlying())
          {
            Vector3 pos = vehicle->FutureVisualState().Position();
            pos[1] += vehicle->MakeAirborne();
            vehicle->SetPosition(pos);
          }
        }
      }
    } // vehicle respawn
    else
    { // person respawn
      Person *person = DoRespawn(item);
      AIBrain *unit = person ? person->Brain() : NULL;
      if (unit)
      {
        Transport *vehicle = unit->FindRespawnVehicle();
        if (vehicle && !vehicle->Driver())
        {
          if (vehicle->IsRespawning())
          {
            // respawn out of order
            for (int j=0; j<_respawnQueue.Size(); j++)
            {
              RespawnQueueItem *item = _respawnQueue[j];
              if (!item->IsVehicleRespawn()) continue;
              Transport *v = item->GetVehicle();
              if (v && v == vehicle)
              {
                vehicle = DoVehicleRespawn(item);
                _respawnQueue.Delete(j);
                if (j < i) i--;
                break;
              }
            }
          }
          if (vehicle)
          {
            // get in directly
            if (vehicle->IsLocal())
            {
              vehicle->GetInDriver(person, false);
              if (unit == GWorld->FocusOn())
              {
                CameraType ValidateCamera(CameraType cam);
                GWorld->SwitchCameraTo(vehicle, ValidateCamera(GWorld->GetCameraType()), false);
              }
            }
            else
              AskForGetIn(person, vehicle, GIPDriver);

            unit->AssignAsDriver(vehicle);
            unit->OrderGetIn(true);
            // make it flying
            if (vehicle->IsRespawnFlying())
            {
              Vector3 pos = vehicle->FutureVisualState().Position();
              pos[1] += vehicle->MakeAirborne();
              vehicle->SetPosition(pos);
            }
          }
        }
      }
    } // person respawn

    _respawnQueue.Delete(i);
  }
}

/*!
\patch 1.47 Date 3/8/2002 by Ondra
- Improved: Chat message priority increased when mission is not running.
This should help when chatting during mission transfers.
*/

NetMsgFlags NetworkClient::GetChatPriority() const
{
  if (GetServerState() == NSSPlaying) return NMFNone;
  return NMFHighPriority;
}

void NetworkClient::SetParams(float param1, float param2, bool updateOnly)
{
  _param1 = param1;
  _param2 = param2;
  
  MissionParamsMessage msg;
  msg._param1 = param1;
  msg._param2 = param2;
  msg._paramsArray = _paramsArray;
  msg._updateOnly = updateOnly;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::SetParams(float param1, float param2, AutoArray<float> paramsArray, bool updateOnly)
{
  _paramsArray = paramsArray;
  _param1 = param1;
  _param2 = param2;

  MissionParamsMessage msg;
  msg._param1 = param1;
  msg._param2 = param2;
  msg._paramsArray = paramsArray;
  msg._updateOnly = updateOnly;
  SendMsg(&msg, NMFGuaranteed);
}

static void MergeTargets(AutoArray<TransmitTarget, MemAllocSA> &targets, AutoArray<TransmitTarget, MemAllocSA> &addTargets)
{
  // when merging all targets with direct speech targets, target should be always found
  // which means: if 'all' is sorted at the input, it remains sorted also after merging
  int tgSize = targets.Size();
  for (int i=0, size=addTargets.Size(); i<size; i++)
  {
    int dpid = addTargets[i].dpid;
    int j=0;
    for (; j<tgSize; j++)
      if (targets[j].dpid==dpid) break;
    if (j==tgSize) targets.Add(addTargets[i]);
    else targets[j].extInfo |= addTargets[i].extInfo;
  }
}

void NetworkClient::GetTargetsInDirectSpeakRange(AutoArray<TransmitTarget, MemAllocSA> &targets, int channel) const
{
  Person *player = GWorld->GetRealPlayer();
  if (!player) return;
  AutoArray<int> units;
#if _ENABLE_MP //making binarize project linkable
  float maxDist = ((channel==CCDirect) ? 80.0f : 50.0f);
  Vector3Val pos = player->WorldPosition(player->FutureVisualState());
  WhatUnitsVoNDirect(units, pos, maxDist);
#endif

  for (int i=0; i<units.Size(); i++)
  {
    int dpnid = units[i];
    const PlayerIdentity *identity = FindIdentity(dpnid);
    if (identity)
    {
#if _XBOX_SECURE && _ENABLE_MP
      // check mute lists
      if (_parent->IsPlayerInMuteList(identity->xuid)) continue;
#endif
      // check remote mute list
      if (IsPlayerInRemoteMuteList(identity->dpnid)) continue;
      TransmitTarget &tt = targets.Append();
      TargetFromIdentity(tt,*identity,TTChan3D);
    }
  }
}

class PlayersInVehicleFunc : public ICrewFunc
{
protected:
  Person *_player;
  AutoArray<int, MemAllocSA> &_players;

public:
  PlayersInVehicleFunc(AutoArray<int, MemAllocSA> &players, Person *player) : _players(players), _player(player) {}

  virtual bool operator () (Person *person)
  {
    if (person && person != _player && person->Brain() && person->Brain()->GetLifeState() != LifeStateDead)
    {
      int id = person->GetRemotePlayer();
      if (id != AI_PLAYER) _players.Add(id);
    }
    return false; // continue
  }
};

/*!
\patch 5259 Date 7/29/2008 by Bebul
- Fixed: Voice over Side channel could be transmitted to wrong listeners after respawn.
*/
void NetworkClient::GetPlayersOnChannel(AutoArray<int, MemAllocSA> &players, int channel) const
{
  // It differs from NetworkServer.GetPlayersOnChannel in not using playerRoles here.
  players.Resize(0);
  switch (channel)
  {
  case CCGlobal:
    // optimization
    Fail("Do not call for global channel");
    break;
  case CCNone:
  case CCDirect:
    break;
  case CCSide:
    {
      Person *player = GWorld->GetRealPlayer();
      if (!player) break;
      AIBrain *agent = player->Brain();
      if (!agent) break;
      AIGroup *group = agent->GetGroup();
      if (!group) break;
      AICenter *center = group->GetCenter();
      if (!center) break;
      for (int j=0; j<center->NGroups(); j++)
      {
        AIGroup *grp = center->GetGroup(j);
        if (!grp) continue;
        for (int i=0; i<grp->NUnits(); i++)
        {
          AIUnit *unit = grp->GetUnit(i);
          if (!unit || unit->GetLifeState() == LifeStateDead) continue;
#if HEAR_YOURSELF
          if (!hearYourself)
#endif
            if (unit == agent)
              continue;

          Person *person = unit->GetPerson();
          if (!person) continue;
          int id = person->GetRemotePlayer();
          if (id != AI_PLAYER) players.Add(id);
        }
      }
    }
    break;
  case CCCommand:
    {
      Person *player = GWorld->GetRealPlayer();
      if (!player) break;
      AIBrain *agent = player->Brain();
      if (!agent) break;
      AIGroup *group = agent->GetGroup();
      if (!group) break;
      AICenter *center = group->GetCenter();
      if (!center) break;
      for (int j=0; j<center->NGroups(); j++)
      {
        AIGroup *grp = center->GetGroup(j);
        if (!grp) continue;
        AIUnit *unit = grp->Leader();
        if (!unit || unit == agent || unit->GetLifeState() == LifeStateDead) continue;
        Person *person = unit->GetPerson();
        if (!person) continue;
        int id = person->GetRemotePlayer();
        if (id != AI_PLAYER) players.Add(id);
      }
    }
    break;
  case CCGroup:
    {
      Person *player = GWorld->GetRealPlayer();
      if (!player) break;
      AIBrain *agent = player->Brain();
      if (!agent) break;
      AIGroup *group = agent->GetGroup();
      if (!group) break;

      for (int i=0; i<group->NUnits(); i++)
      {
        AIUnit *unit = group->GetUnit(i);
        if (!unit || unit == agent || unit->GetLifeState() == LifeStateDead) continue;
        Person *person = unit->GetPerson();
        if (!person) continue;
        int id = person->GetRemotePlayer();
        if (id != AI_PLAYER) players.Add(id);
      }
    }
    break;
  case CCVehicle:
    {
      Person *player = GWorld->GetRealPlayer();
      if (!player) break;
      AIBrain *agent = player->Brain();
      if (!agent) break;
      Transport *vehicle = agent->GetVehicleIn();
      if (!vehicle) break;

      PlayersInVehicleFunc func(players, player);
      vehicle->ForEachCrew(func);
    }
    break;
  }
}

DEFINE_NET_MESSAGE(LocalizedChat,LOCALIZED_CHAT_MSG)

bool NetworkClient::IsPlayerInRemoteMuteList(int player) const
{
#if _ENABLE_MP
  for (int i=0; i<_remoteMuteList._list.Size(); i++)
    if (_remoteMuteList._list[i] == player) return true;
#endif
  return false;
}

#if _XBOX_SECURE && _XBOX_VER >= 200

bool NetworkClient::IsPlayerInMuteList(const XUID &xuid) const
{
  for (int i=0; i<_muteList._list.Size(); i++)
    if (XOnlineAreUsersIdentical(_muteList._list[i], xuid)) return true;
  return false;
}

void NetworkClient::UpdateMuteList()
{
  // rebuild the list of muted players
  _muteList._list.Resize(0);
  int userIndex = GSaveSystem.GetUserIndex();
#ifdef _XBOX
  if (GSaveSystem.IsUserSignedIn())
#else
  if (userIndex >= 0)
#endif
  {
    for (int i=0; i<_identities.Size(); i++)
    {
      const PlayerIdentity &identity = _identities[i];
      if (identity.dpnid == _player) continue;

      BOOL muted = false;
      if (FAILED(XUserMuteListQuery(userIndex, identity.xuid, &muted))) continue;

      if (muted) _muteList._list.Add(identity.xuid);
    }
  }
  _muteList._list.Compact();
  // send the list to the server
  if (_clientState >= NCSConnected) SendMsg(&_muteList, NMFGuaranteed);
}

#endif

static int CompareTransmitTargetId(TransmitTarget *t1, TransmitTarget *t2)
{
  return t1->dpid-t2->dpid;
}

void NetworkClient::OnDraw()
{
}

#if DEBUG_VON_DIAGS
  DWORD replayerCurPosition;
  DWORD VoNReplStatus;
#endif

#if HEAR_YOURSELF
bool hearYourself = true; //only for debugging purposes, no need to run two instances of OFP game
#endif

// when port address was changed in VoN layer, change it also in PlayerIdentity
void PeerChangedProc(int dpid, int port, void *context)
{
  AutoArray<PlayerIdentity> &identities = *((AutoArray<PlayerIdentity> *)(context));
  for (int i=0; i<identities.Size(); i++)
  {
    if (identities[i].dpnid == dpid)
    {
      identities[i]._voiceAddress.sin_port = port;
      break;
    }
  }
}

/*!
\patch 5164 Date 6/5/2007 by Ondra
- Fixed: Vehicle channel no longer transforms to a side channel when not in the vehicle.
*/
void NetworkClient::OnSimulate()
{
  PROFILE_SCOPE_EX(nwCli, network)

  // raw statistics
#if _ENABLE_CHEATS
  // remove statistics info
  static AutoArray<int> texts;
  for (int i=0; i<texts.Size(); i++)
  {
    if (texts[i] >= 0) GEngine->RemoveText(texts[i]);
  }
  texts.Resize(0);

  if (outputDiags == ODMRawStats)
  {
    DoAssert(_rawStatistics.Size() <= 1);
    if (_rawStatistics.Size() > 0)
    {
      char output[1024];
      sprintf
      (
        output, "To server: sent %d (%d), received %d (%d)",
        _rawStatistics[0].sizeSent, _rawStatistics[0].msgSent,
        _rawStatistics[0].sizeReceived, _rawStatistics[0].msgReceived
      );  
      texts.Add(GEngine->ShowTextF(1000, output));

      _rawStatisticsGraph.Resize(1);
      if (!_rawStatisticsGraph[0]) _rawStatisticsGraph[0] = new NetStatsGraph;
      _rawStatisticsGraph[0]->OnSimulate(_rawStatistics[0].sizeReceived,_rawStatistics[0].msgReceived);
      GEngine->ShowDiagGraph(_rawStatisticsGraph[0]);


    }
    else
    {
      texts.Add(GEngine->ShowTextF(1000, "To server: No raw messages"));
    }


  }
#endif

#if DIAGNOSE_SPEEX_ESTFRAMES
  static volatile bool doIt = false;
  if (doIt)
  { // step into this scope in debugger to increase the codec quality
    static volatile int quality = 1;
    GNetworkManager.SetVoNCodecQuality(quality);
    RptF("VoN quality set to %d", quality);
    quality++;
  }
#endif

  // update ClientCameraPosition
  const Camera *camera = GScene->GetCamera();
  if ( camera==NULL || ( GWorld->CameraOn()==GWorld->PlayerOn()  && GWorld->GetCameraEffect()==NULL) )
    _clientCameraPosition->_position = InvalidCamPos;
  else
    _clientCameraPosition->_position = camera->Position();

  Person *player = GWorld->GetRealPlayer();
  AIBrain *agent = player ? player->Brain() : NULL;

  #ifdef _XBOX
  // when out of vehicle, do not speak on vehicle channel
  if (_voiceChannel == CCVehicle && (!agent || !agent->GetVehicleIn()))
  {
    SetVoiceChannel(CCSide);
    GWorld->OnChannelChanged();
  }
  #endif

  // outside briefing and game speak on global channel
  //ChatChannel ccLobby = CCGlobal;
  ChatChannel ccLobby = CCGroup;
  
  bool forceLobbyChannel = (
    _clientState != NCSBriefingShown && _clientState != NCSBriefingRead ||
    !agent || agent->GetLifeState() == LifeStateDead
  );
  
  // if we are server owner or admin, we may always use the global channel
  if (_voiceChannel==CCGlobal && (GetNetworkManager().IsGameMaster() || GetNetworkManager().IsServer()))
  {
    forceLobbyChannel = false;
  }
  
  if (_voiceChannel != ccLobby && _voiceChannel!=CCNone && forceLobbyChannel && GetNetworkManager().IsClient() && GetNetworkManager().VoiceReady())
  {
    SetVoiceChannel(ccLobby);
    GWorld->OnChannelChanged();
  }

  bool canSetTransmitTargets = true;
#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  // on Xbox 360, set transmit targets when xuid is correctly set
  canSetTransmitTargets = GetClientState() >= NCSLoggedIn;
#endif


  BankList::ReadAccess banks(GFileBanks);
  //@{ Async bank signatures checking (as sigVer2 test with level=2 are slow)
  if (!_checkingSignature && _checkSignatureQueue.Size() > 0)
  {
    int index = _checkSignatureQueue[0]._bankIndex;
    int m = banks.Size();
    if (index < m)
    {
      int maxFrames = _checkSignatureQueue[0]._initialTest ? 3 : 20; // test the whole file in max three frames when initial check is processin
      _checkingSignature = new BankSignatureCheckAsync(index, _checkSignatureQueue[0]._sigVer, _checkSignatureQueue[0]._level, _checkSignatureQueue[0]._type, maxFrames);
      /*
      if (IsDedicatedClient())
      {
        ConsoleF(
          "Checking signature %d:'%s' level %d, %d pending", index, cc_cast(banks[_checkSignatureQueue[0]._bankIndex].GetOpenName()),
          _checkSignatureQueue[0]._level, _checkSignatureQueue.Size()
        );
      }
      */
    }
    else
    {
      LogF("Bank index out of range");
      _checkSignatureQueue.DeleteAt(0);
    }
  }
  if (_checkingSignature)
  {
    if (_checkingSignature->Process())
    {
      // TODO: protect _checkingSignature and _checkSignatureQueue against manipulation by hacker
      DoAssert(_checkSignatureQueue.Size() > 0);
      LogF("Hash level=%d of %s checked.", _checkSignatureQueue[0]._level, cc_cast(banks[_checkSignatureQueue[0]._bankIndex].GetOpenName()));
      // send the proper message
      switch (_checkSignatureQueue[0]._type)
      {
      case NMTDataSignatureAnswer:
        {
          DataSignatureAnswerMessage *answer = static_cast<DataSignatureAnswerMessage *>(_checkSignatureQueue[0]._msg);
          /*
          if (IsDedicatedClient())
          {
            ConsoleF("Data signature answer %d:'%s'. %d pending", answer->_index, cc_cast(answer->_filename), answer->_level);
          }
          */
          if ( _checkingSignature->GetHash(answer->_hash._content) )
          {
            GetNetworkManager().SendMsg(answer);
          }
          else
            GetNetworkManager().HackedDataDetected(banks[_checkSignatureQueue[0]._bankIndex].GetOpenName());
        }
        break;
      case NMTFileSignatureAnswer:
        {
          FileSignatureAnswerMessage *answer = static_cast<FileSignatureAnswerMessage *>(_checkSignatureQueue[0]._msg);
          /*
          if (IsDedicatedClient())
          {
            ConsoleF("File signature answer %d:'%s'. %d pending", answer->_index, cc_cast(answer->_fileName), answer->_level);
          }
          */
          if ( _checkingSignature->GetHash(answer->_dataToHash) )
            GetNetworkManager().SendMsg(answer);
          else
            GetNetworkManager().HackedDataDetected(banks[_checkSignatureQueue[0]._bankIndex].GetOpenName());
        }
        break;
      }
      _checkingSignature = NULL;
      _checkSignatureQueue.DeleteAt(0);
    }
  }
  //@} Async bank signatures checking 


  if (canSetTransmitTargets)
  {
    // list of all addresses, included muted players
    // used for re-translation
    AUTO_STATIC_ARRAY(TransmitTarget, targets, 32);
    AUTO_STATIC_ARRAY(TransmitTarget, all, 64);
    // TODO: this list needs to be create only when _identities is changed
    bool serverFound = false;
#if DEBUG_IDENTITIES
    static InitVal<int,-1> handles[30];
    int handle = 0;
#endif


#if _ENABLE_MP //making binarize project linkable
    // when port address was changed in VoN layer, change it also in PlayerIdentity
    ProcessPeerChanged(&PeerChangedProc, (void *)&_identities /*context*/);
#endif

    for (int i=0; i<_identities.Size(); i++)
    {
      const PlayerIdentity &identity = _identities[i];
      // nobody can hear himself
#if HEAR_YOURSELF
      if (!hearYourself)
#endif
        if (identity.dpnid == _player) continue;
      
      // add to transmit targets
      TransmitTarget &tt = all.Append();
      TargetFromIdentity(tt,identity,0);

      if (identity.dpnid==BOT_CLIENT)
      {
        serverFound = true;
      }
#if DEBUG_IDENTITIES
      if (netLogValid) //type -netlog as command line parameter
      {
        char *botstr = (identity.dpnid == BOT_CLIENT) ? "(bot client)" : "";
        char diagTxt[1024]; 
        sprintf(diagTxt, "Player %s IP: %d.%d.%d.%d:%d %s",
          cc_cast(identity.name),
          identity._voiceAddress.sin_addr.S_un.S_un_b.s_b1, identity._voiceAddress.sin_addr.S_un.S_un_b.s_b2, 
          identity._voiceAddress.sin_addr.S_un.S_un_b.s_b3, identity._voiceAddress.sin_addr.S_un.S_un_b.s_b4,
          ntohs(identity._voiceAddress.sin_port), botstr);
        GlobalDiagMessage(handles[handle++],0,500,diagTxt);
        //LogF(diagTxt.Data());
      }
#endif
    }

    // make sure bot-client is always there
    // this is important for dedicated server
    if (!serverFound && _serverVoNAddr.sin_port)
    {
      TransmitTarget &tt = all.Append();
      tt.addr = _serverVoNAddr;
      tt.dpid = BOT_CLIENT;
#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
      tt.xuid = INVALID_XUID;
#endif
      tt.extInfo = 0;
    }

    QSort(all,CompareTransmitTargetId);

    if (forceLobbyChannel)
    {
      typedef OLink(Person) helperType;
      AUTO_STATIC_ARRAY( helperType, players, 128);
      GWorld->ScanPlayers(players);
      // select dead players
      for (int i=0; i<players.Size(); i++)
      {
        Person *person = players[i];
        if (!person->Brain() || person->Brain()->GetLifeState()==LifeStateDead)
        {
          // he is dead - we want to add him into the list
          // we need to get his identity
          int dpnid = person->GetRemotePlayer();
          const PlayerIdentity *id = FindIdentity(dpnid);
          if (id)
          {
            const PlayerIdentity &identity = *id;
            // add only those which do not pass the handling in forceLobbyChannel below
            if (identity.clientState==NCSBriefingShown || identity.clientState==NCSBriefingRead)
            {
              TransmitTarget &tt = targets.Append();
              TargetFromIdentity(tt,identity,TTChan2D);
            }
          }
        }
      }
    }
    if (_voiceChannel == CCGlobal || forceLobbyChannel)
    {
      // need not to search - send to all players
      for (int i=0; i<_identities.Size(); i++)
      {
        const PlayerIdentity &identity = _identities[i];
        // nobody can hear himself
  #if HEAR_YOURSELF
        if (!hearYourself)
  #endif
          if (identity.dpnid == _player) continue;
  #if _XBOX_SECURE
        // check mute lists
        if (_parent->IsPlayerInMuteList(identity.xuid)) continue;
  #endif
        // check remote mute list
        if (IsPlayerInRemoteMuteList(identity.dpnid)) continue;
        
        if (forceLobbyChannel)
        {
          // check for players which are not in the game
          if (identity.clientState==NCSBriefingShown || identity.clientState==NCSBriefingRead)
          {
            // when in game and we are not, he cannot hear us
            continue;
          }
        }
        // add to transmit targets
        TransmitTarget &tt = targets.Append();
        TargetFromIdentity(tt,identity,TTChan2D);
      }
    }
    else
    {
      AUTO_STATIC_ARRAY(int, players, 32);
      GetPlayersOnChannel(players, _voiceChannel);
      for (int i=0; i<players.Size(); i++)
      {
        const PlayerIdentity *identity = FindIdentity(players[i]);
        if (identity)
        {
#if _XBOX_SECURE
          // check mute lists
          if (_parent->IsPlayerInMuteList(identity->xuid)) continue;
#endif
          // check remote mute list
          if (IsPlayerInRemoteMuteList(identity->dpnid)) continue;
          // add to transmit targets
          TransmitTarget &tt = targets.Append();
          TargetFromIdentity(tt,*identity,TTChan2D);
        }
      }
    }
#if _ENABLE_MP
    // only alive players can be speaking 3D
    if (agent && agent->GetLifeState() != LifeStateDead)
    {
      // get transmit targets of players in the "direct speak range"
      AUTO_STATIC_ARRAY(TransmitTarget, dsTargets, 32);
      GetTargetsInDirectSpeakRange(dsTargets, _voiceChannel);
      MergeTargets(targets, dsTargets); //targets = targets + dsTargets
    }
    MergeTargets(all, targets); //this will set extInfo to all targets

  // 'all' targets MUST be sorted (it is assumed in VoNClient::setAllTargets)
  // 'targets' need not be sorted, as these all will be used to send VoN message
  QSort(all,CompareTransmitTargetId);
  
  //final set of transmit targets
  VoiceTransmitTargets(targets.Data(), targets.Size());
  VoiceAllTargets(all.Data(), all.Size());
#endif
  }

#if _ENABLE_REPORT
  int VoiceClientGetReceivedDataCount();
  int VoiceClientGetReceivedDataSize();
  int VoiceClientGetReceivedCtrlCount();
  int VoiceClientGetReceivedCtrlSize();
  int VoiceClientGetSentDataCount();
  int VoiceClientGetSentDataSize();
  int VoiceClientGetSentCtrlCount();
  int VoiceClientGetSentCtrlSize();
  void VoiceClientResetStats();

#if _XBOX_SECURE && _ENABLE_MP
  if (!_parent->IsServer())
  {
    int rdc = VoiceClientGetReceivedDataCount();
    int rds = VoiceClientGetReceivedDataSize();
    int rcc = VoiceClientGetReceivedCtrlCount();
    int rcs = VoiceClientGetReceivedCtrlSize();
    int sdc = VoiceClientGetSentDataCount();
    int sds = VoiceClientGetSentDataSize();
    int scc = VoiceClientGetSentCtrlCount();
    int scs = VoiceClientGetSentCtrlSize();

    int foundData = -1;
    int foundCtrl = -1;
    for (int i=0; i<_statistics.Size(); i++)
    {
      if (_statistics[i].message == NMTN)
      {
        foundData = i;
        if (foundCtrl >= 0) break;
      }
      else if (_statistics[i].message == NMTN + 1)
      {
        foundCtrl = i;
        if (foundData >= 0) break;
      }
    }
    if (rdc > 0 || sdc > 0)
    {
      if (foundData < 0)
      {
        foundData = _statistics.Add();
        _statistics[foundData].message = NMTN;
      }
      _statistics[foundData].msgSent += sdc;
      _statistics[foundData].sizeSent += sds;
      _statistics[foundData].msgReceived += rdc;
      _statistics[foundData].sizeReceived += rds;
    }
    if (rcc > 0 || scc > 0)
    {
      if (foundCtrl < 0)
      {
        foundCtrl = _statistics.Add();
        _statistics[foundCtrl].message = NMTN + 1;
      }
      _statistics[foundCtrl].msgSent += scc;
      _statistics[foundCtrl].sizeSent += scs;
      _statistics[foundCtrl].msgReceived += rcc;
      _statistics[foundCtrl].sizeReceived += rcs;
    }
    VoiceClientResetStats();
  }
#endif
#if DEBUG_VON_DIAGS
  if (netLogValid) //type -netlog as command line parameter
  {
    char dgmessage[1024]; 
    extern PoCriticalSection lockRecvDiagMessage;
    extern char RecvDiagMessage[1024];
    lockRecvDiagMessage.enter();
    if (RecvDiagMessage[0]!=0) DIAG_MESSAGE(1000,RecvDiagMessage);
    RecvDiagMessage[0]=0;
    lockRecvDiagMessage.leave();
    extern PoCriticalSection lockSendDiagMessage;
    extern char SendDiagMessage[1024];
    lockSendDiagMessage.enter();
    if (SendDiagMessage[0]!=0) DIAG_MESSAGE(1000,SendDiagMessage);
    SendDiagMessage[0]=0;
    extern int SendDiagBps;
    sprintf(dgmessage, "sending rate: %.2f kbps", (SendDiagBps*8)/1000.0f);
    if (SendDiagBps!=0) DIAG_MESSAGE(1000,dgmessage);
    lockSendDiagMessage.leave();
    char dgChatChannel[1024];
    extern int diagChatChan;
    sprintf(dgChatChannel, " chan=%x", diagChatChan);
    if (diagChatChan&TTChan2D) strcat(dgChatChannel, " 2D");
    if (diagChatChan&TTChan3D) strcat(dgChatChannel, " 3D");
    if (diagChatChan&TTDirectChan) strcat(dgChatChannel, " Direct");
    extern float diagLastAmpl;
    extern DWORD diagAmplTimeStamp;
    DWORD sysTime = ::GetTickCount();
    if (sysTime - diagAmplTimeStamp > 1500) diagLastAmpl = 0;
    sprintf(dgmessage, "ampl=%.2f %% %s", diagLastAmpl*100.0f, dgChatChannel);
    if (diagLastAmpl!=0) DIAG_MESSAGE(1000, dgmessage);
#if _ENABLE_CHEATS
    extern char GVoNSoundDiag[];
    if (*GVoNSoundDiag) DIAG_MESSAGE(500, GVoNSoundDiag); 
#endif
#if 0 //this diagnostic section was handy only for DirectSound, not for OpenAl
    extern DWORD replayerCurPosition;
    sprintf(dgmessage, "replayerPos: %u", replayerCurPosition);
    DIAG_MESSAGE(1000, dgmessage);
#define DSBSTATUS_PLAYING           0x00000001
#define DSBSTATUS_BUFFERLOST        0x00000002
#define DSBSTATUS_LOOPING           0x00000004
#define DSBSTATUS_LOCHARDWARE       0x00000008
#define DSBSTATUS_LOCSOFTWARE       0x00000010
#define DSBSTATUS_TERMINATED        0x00000020
    if (VoNReplStatus==-1) sprintf(dgmessage, "VoNStatus: Error");
    else sprintf(dgmessage, "VoNStatus: %s %s %s %s %s %s", 
      ((VoNReplStatus & DSBSTATUS_BUFFERLOST) ? "LOST" : ""),
      ((VoNReplStatus & DSBSTATUS_LOOPING) ? "LOOPING" : ""),
      ((VoNReplStatus & DSBSTATUS_PLAYING) ? "PLAYING" : ""),
      ((VoNReplStatus & DSBSTATUS_LOCSOFTWARE) ? "LOCSOFTWARE" : ""),
      ((VoNReplStatus & DSBSTATUS_LOCHARDWARE) ? "LOCHARDWARE" : ""),
      ((VoNReplStatus & DSBSTATUS_TERMINATED) ? "TERMINATED" : "")
      );
    DIAG_MESSAGE(1000, dgmessage);
#endif
  }
#endif

#endif // _ENABLE_REPORT

#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER >= 200
  // Statistics handled by the NetworkManager
# else
  // statistics writing
  if (_writeStatsTask)
  {
    HRESULT result = XOnlineTaskContinue(_writeStatsTask);
    if (result != XONLINETASK_S_RUNNING)
    {
      XOnlineTaskClose(_writeStatsTask);
      _writeStatsTask = NULL;
#if LOG_LIVE_STATS
      if (FAILED(result))
        RptF(" - XOnlineStatWrite update failed with error 0x%x", result);
      else
        RptF(" - Statisctics written");
#endif
    }
  }
# endif // _XBOX_VER >= 200
#endif

//#endif // _XBOX_SECURE

  _connectionQuality = CQGood;
  _connectionDesync = CDGood;

#if _ENABLE_CHEATS
  if (GInput.GetCheat2ToDo(DIK_RETURN))
  {
    outputLogs = !outputLogs;
    if (outputLogs)
      GlobalShowMessage(500, "message logs on");
    else
      GlobalShowMessage(500, "message logs off");
  }
  if (GInput.GetCheat2ToDo(DIK_SEMICOLON))
  {
    outputDiags++;
    if (GNetworkManager.GetServer())
    {
      if (outputDiags >= NOutputDiags*2) outputDiags = -1;
    }
    else
    {
      if (outputDiags >= NOutputDiags) outputDiags = -1;
    }
  }
  if (GInput.GetCheat2ToDo(DIK_APOSTROPHE))
  {
    _statistics.Clear();
    if (_parent->GetServer()) _parent->GetServer()->GetStatistics().Clear();
  }
#endif

  if (_remoteObjects.card() > 0 && !_parent->IsServer())
  {
    float age = _client->GetLastMsgAge();

    if (age > 10) 
    {
      if (_connectionQuality < CQBad) _connectionQuality = CQBad;
#ifndef _XBOX
      float ageReported = _client->GetLastMsgAgeReported();
      if (ageReported >=5)
      {
        char message[256];
        sprintf(message, LocalizeString(IDS_MP_NO_MESSAGE), age);
        GChatList.Add(CCSystem, NULL, message, false, true);
        _client->LastMsgAgeReported();
      }
#endif
    }
    else if (age > 5)
    {
      if (_connectionQuality < CQPoor) _connectionQuality = CQPoor;
    }
    
    const PlayerIdentity *identity = FindIdentity(_player);
    if (identity)
    {
      if (identity->_desync>PlayerIdentity::DesyncLow)
      {
        if (identity->_desync>PlayerIdentity::DesyncHigh)
        {
          _connectionDesync = CDHigh;
        }
        else
        {
          _connectionDesync = CDLow;
        }
      }
    }
  }

  _controlsPaused = _client->GetLastMsgAgeReliable() > 10.0f;

  // receive all system and user messages
  {
    PROFILE_SCOPE_EX(nwCRv, network)
    ReceiveSystemMessages();
    ReceiveLocalMessages();
    ReceiveUserMessages();
  }
  if (!_client) return; // session lost

  #if _EXT_CTRL //ext. Controller like HLA, AAR
    if(IsBotClient())
    {
      _hla.OnSimulate();
    }
  #endif

  // statistics
  #if _ENABLE_MP
  if (DiagLevel >= 3)
  {
    int nMsg, nBytes, nMsgG, nBytesG;
    _client->GetSendQueueInfo(nMsg, nBytes, nMsgG, nBytesG);
        if (nMsg > 0 || nMsgG > 0)
        {
            if ( nMsgG < 0 )    // old style
          DiagLogF("Client: pending in SendQueue: %d messages, %d bytes", nMsg, nBytes);
            else                // new style
          DiagLogF("Client: pending in SendQueue: common - %d messages, %d bytes, guaranteed - %d messages, %d bytes",
                         nMsg, nBytes, nMsgG, nBytesG);
        }
  }
  #endif

  // respawning
  ProcessRespawns();

#if _GAMES_FOR_WINDOWS || defined _XBOX
  PlayerIdentity *identity = FindIdentity(GetPlayer());
  if (identity)
  {
    bool voiceOn = _parent->IsVoice();
    if (voiceOn != identity->voiceOn)
    {
      identity->voiceOn = voiceOn;
      VoiceOnMessage msg;
      msg._dpnid = GetPlayer();
      msg._voiceOn = voiceOn;
      SendMsg(&msg, NMFGuaranteed);
    }
  }
#endif

  // send update messages
  {
    PROFILE_SCOPE_EX(nwCSd, network)
    SendMessages();
  }

  // update synchronized _serverTime
  _serverTime = ::GetTickCount() - _serverTimeInitTickCount;

  // update speaker buffers
  for (int i=0; i<_soundBuffers.Size(); i++)
  {
    if (_soundBuffers[i].buffer && _soundBuffers[i].object)
    {
      Vector3 pos = _soundBuffers[i].object->GetSpeakerPosition();
      _soundBuffers[i].buffer->SetPosition(pos[0], pos[1], pos[2]);
      bool playing = _client->IsVoicePlaying(_soundBuffers[i].player);
      _soundBuffers[i].object->SetRandomLip(playing);
    }
  }

  // check if sent confirmed for all outgoing messages
  for (LocalObjectsContainer::Iterator it=_localObjects.begin(); it; _localObjects.next(it))
  {
    NetworkLocalObjectInfo &oInfo = *it;
    for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
    {
      NetworkUpdateInfo &info = oInfo.updates[j];
      if (info.lastCreatedMsg && ::GlobalTickCount() > info.lastCreatedMsgTime + 5000)
      {
        RptF("Client: Network message %x (update info %x) is pending", info.lastCreatedMsgId, &info);
        info.lastCreatedMsg = NULL;
        info.lastCreatedMsgId = 0xFFFFFFFF;
        info.lastCreatedMsgTime = 0;
      }
    }
  }

  // log all diagnostics
  WriteDiagOutput(false);

#if _ENABLE_CHEATS
  if (_debugWindow)
  {
    _debugWindow->ResetContent();
    for (LocalObjectsContainer::Iterator it=_localObjects.begin(); it; _localObjects.next(it))
    {
      NetworkLocalObjectInfo &oInfo = *it;
      NetworkId id = oInfo.id;
      NetworkObject *object = oInfo.object;
      RString typeName;
      if (object)
#ifdef _CPPRTTI
        typeName = typeid(*object).name();
#else
        typeName = "Unknown";
#endif
      else typeName = "<null>";

      BString<1024> text;
      AIUnit *unit = dynamic_cast<AIUnit *>(object);
      sprintf
      (
        text, "Local %s (%x) %d:%d %s",
        (const char *)typeName, object, id.creator.CreatorInt(), id.id,
        unit ? (const char *)FindEnumName(unit->GetLifeState()) : ""
      );
      _debugWindow->AddString(text);
    }
    for (RemoteObjectsContainer::Iterator it=_remoteObjects.begin(); it; _remoteObjects.next(it))
    {
      NetworkRemoteObjectInfo &oInfo = *it;
      NetworkId id = oInfo.id;
      NetworkObject *object = oInfo.object;
      RString typeName;
      if (object)
#ifdef _CPPRTTI
        typeName = typeid(*object).name();
#else
        typeName = "Unknown";
#endif
      else typeName = "<null>";

      BString<1024> text;
      AIUnit *unit = dynamic_cast<AIUnit *>(object);
      sprintf
      (
        text, "Remote %s (%x) %d:%d %s",
        (const char *)typeName, object, id.creator.CreatorInt(), id.id,
        unit ? (const char *)FindEnumName(unit->GetLifeState()) : ""
      );
      _debugWindow->AddString(text);
    }
  }
#endif

#if _USE_BATTL_EYE_CLIENT
  if (!_beIntegration.Run(this))
  {
    // reinitialization after auto-update failed, need to disconnect
    Disconnect("BattlEye initialization failed");
  }
#endif
}

BankSignatureCheckAsync::BankSignatureCheckAsync(int bankIndex, int sigVer, int level, int type, int maxFrames)
: _bankIndex(bankIndex), _sigVer(sigVer), _level(level), _type(type), _maxFrames(maxFrames)
{
  DoAssert(level==2 && sigVer==2); //async used only for level=2 checks
  BankList::ReadAccess banks(GFileBanks);
  _dummyPaddingFilePresent = banks[bankIndex].GetNonDataFileList(_nonDataFiles);
  _curFile = 0;
}

bool BankSignatureCheckAsync::Process()
{
  if (_curFile < _nonDataFiles.Size())
  {
    BankList::ReadAccess banks(GFileBanks);
    QIStreamDataSource in;
    QFBank &bank = banks[_bankIndex];
    bank.AttachSource(in);
    int range = (_nonDataFiles.Size()+_maxFrames-1)/_maxFrames;  //at least one
    int curStopIx = _curFile+range;
    saturateMin(curStopIx, _nonDataFiles.Size());
    int fileOld = in.tellg(); // store original position
    for (; _curFile < curStopIx; _curFile++)
    {
#if _ENABLE_PBO_PROTECTION 
      if (_dummyPaddingFilePresent)
      {
        _dummyPaddingFilePresent = false; // only once
        // get the original content of ___dummypadding___ file by encrypting pbo header using SETUP key
        QFBank::BankHeaderProtectionInfo info;
        bank.GetBankHeaderProtectionInfo(info);
        QOStrStream decryptedHeader;
        if (bank.DecryptHeaders(decryptedHeader, in, info.protHeaderStart, info.protHeaderStart+info.protHeaderSize))
        {
          QOStrStream setupEncryptedHeader;
          QFileSize encrSize = bank.EncryptHeadersSetup(setupEncryptedHeader, decryptedHeader);
          if (encrSize)
          {
            QIStrStream setupHeader(setupEncryptedHeader.str(), setupEncryptedHeader.pcount());
            bank.AddFileHash(_calculator, setupHeader,  _nonDataFiles[_curFile].startOffset-info.protHeaderStart, _nonDataFiles[_curFile].length);
          }
        }
        else bank.AddFileHash(_calculator, in, _nonDataFiles[_curFile].startOffset, _nonDataFiles[_curFile].length);
      }
      else
#endif
      { //after else!
        bank.AddFileHash(_calculator, in, _nonDataFiles[_curFile].startOffset, _nonDataFiles[_curFile].length);
      }
    }
    in.seekg(fileOld); // move read pointer to original position
    return false; // continue next simulation step; note: if we are over, the condition fails next simulation step
  }
  else if ( !_nonDataFiles.Size() )
  {
    static const char *dummyDataFiles = "nothing";
    _calculator.Add(dummyDataFiles, 7); //hash dummy content
  }
  return true;
}

bool BankSignatureCheckAsync::GetHash(Temp<char> &hash)
{
  BankList::ReadAccess banks(GFileBanks);
  QFBank &bank = banks[_bankIndex];
  // the final hash will be: HashOf(nonDataHash+fileListHash+pboPrefix)
  AutoArray<char> nonDataHash;
  if (!_calculator.GetResult(nonDataHash))
    return false;
  AutoArray<char> fileListHash;
  if (!bank.GetFileListHash<HashCalculator>(fileListHash))
    return false;
  switch (this->_type)
  {
  case NMTDataSignatureAnswer:
    {
      // and hash it finally
      HashCalculator calculator;
      calculator.Add(nonDataHash.Data(), nonDataHash.Size());
      calculator.Add(fileListHash.Data(), fileListHash.Size());
      calculator.Add(bank.GetPrefix().Data(), bank.GetPrefix().GetLength());
      AutoArray<char> finalHash;
      calculator.GetResult(finalHash);
      hash.Realloc(finalHash.Data(), finalHash.Size()); //Ver2 hash
      return true;
    }
    break;
  case NMTFileSignatureAnswer:
    {
      // concatenate it to create _dataToHash
      hash.Realloc( nonDataHash.Size() + fileListHash.Size() + bank.GetPrefix().GetLength() );
      char *data = hash.Data();
      memcpy(data, nonDataHash.Data(), nonDataHash.Size()); data += nonDataHash.Size();
      memcpy(data, fileListHash.Data(), fileListHash.Size()); data += fileListHash.Size();
      memcpy(data, bank.GetPrefix().Data(), bank.GetPrefix().GetLength());
      return true;
    }
    break;
  }
  return false;
}

void NetworkClient::FastUpdate()
{
}

void OnClientUserMessage(char *buffer, int bufferSize, void *context)
{
  PROFILE_SCOPE_EX(nwCUM, network)

  NetworkClient *client = (NetworkClient *)context;
  NetworkMessageRaw rawMsg(buffer, bufferSize);
  client->DecodeMessage(TO_SERVER, rawMsg);
}

void OnClientSendComplete(DWORD msgID, bool ok, void *context)
{
  PROFILE_SCOPE_EX(nwCSC, network)

  NetworkClient *client = (NetworkClient *)context;
  client->OnSendComplete(msgID, ok);
}

#if _VBS3
extern RString ClientIP;
#endif

/*!
\patch 1.50 Date 4/4/2002 by Jirka
- Fixed: When session was lost, client doesn't fully escape from game to basic multiplayer screen
*/
void NetworkClient::ReceiveSystemMessages()
{
  PROFILE_SCOPE_EX(nwCSM, network)

  if (!_client) return;
  if (_client->IsSessionTerminated() && _player != BOT_CLIENT && _serverState != NSSNone)
  {
    _clientState = NCSNone;
    _serverState = NSSNone;
    _serverTimeout = INT_MAX;

    NetTerminationReason reason = _client->GetWhySessionTerminated();
    RString format = LocalizeString(IDS_MP_SESSION_LOST);
    RString message = format;
    switch (reason)
    {
      case NTRTimeout: format = LocalizeString(IDS_MP_TIMEOUT_CLIENT); break;
      case NTRKicked:
        {
          RString reasonStr = _client->GetWhySessionTerminatedStr();
          if (reasonStr.IsEmpty())
            format = LocalizeString(IDS_MP_KICKED_CLIENT); 
          else
            format = LocalizeString(IDS_MP_KICKED_CLIENT) + RString(" (") + reasonStr + RString(")");
        }
        break;
      case NTRBanned: format = LocalizeString(IDS_MP_BANNED_CLIENT); break;
      case NTRBadCDKey: 
      case NTRCDKeyInUse:
      case NTRGSTimeOut:
        {
          if (reason == NTRBadCDKey) format = LocalizeString(IDS_MP_BAD_CDKEY);
          else if (reason == NTRGSTimeOut) format = "Authentication Timeout"; 
          else format = LocalizeString(IDS_MP_CDKEY_IN_USE); 
          RString reasonStr = _client->GetWhySessionTerminatedStr();
          if ( !reasonStr.IsEmpty() && stricmp(reasonStr, format)!=0 )
          { // reason and formated strings differ, we should show both
            format = format + RString(" - ") + reasonStr;
          }
          break;
        }
      case NTRSessionLocked: format = LocalizeString(IDS_MP_SESSION_LOCKED); break;
      case NTRBattlEye: 
        {
          RString reasonStr = _client->GetWhySessionTerminatedStr();
          format = LocalizeString(IDS_MP_KICKED_CLIENT) + RString(" (BattlEye: ") + reasonStr + RString(")");
          break;
        }
    }

#if _VBS3
    // if we dont have a client IP address, then
    // prompt like normal.
    if(ClientIP.GetLength() == 0)
#endif
    {
      GChatList.Add(CCSystem, NULL, format, false, true);
      GWorld->CreateWarningMessage(format);
    }
    Done();
    return;
  }
  _client->ProcessSendComplete(OnClientSendComplete, this);
}

void NetworkClient::ReceiveUserMessages()
{
  if (_client) _client->ProcessUserMessages(OnClientUserMessage, this);
}

void NetworkClient::RemoveSystemMessages()
{
  if (_client) _client->RemoveSendComplete();
}

void NetworkClient::RemoveUserMessages()
{
  if (_client) _client->RemoveUserMessages();
}

#if DEBUG_GIVEN_NETWORK_ID
extern bool DebugMeBoy(NetworkId id);
#endif

void NetworkClient::OnSendComplete(DWORD msgID, bool ok)
{
#if CHECK_MSG
  CheckLocalObjects();
#endif

#if LOG_SEND_PROCESS
  LogF("Client: Send %x completed: %s", msgID, ok ? "ok" : "failed");
#endif
  for (LocalObjectsContainer::Iterator it=_localObjects.begin(); it; _localObjects.next(it))
  {
    NetworkLocalObjectInfo &localObject = *it;
    for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
    {
      // NetworkLocalObjectInfo &info = _localObjects[i];
      NetworkUpdateInfo &info = localObject.updates[j];
      if (info.lastCreatedMsgId != msgID)
      {
        continue;
      }
      if (!info.lastCreatedMsg)
      {
        Fail("Message ID without message");
        info.lastCreatedMsgId = 0xFFFFFFFF;
        info.lastCreatedMsgTime = 0;
        continue;
      }
      if (ok)
      {
        ADD_COUNTER(netCN, 1);
        ADD_COUNTER(netCS, info.lastCreatedMsg->size);
        // message sent
        info.lastSentMsg = info.lastCreatedMsg;

#if CHECK_MSG
NetworkObject *object = localObject.object;
if (object)
{
NetworkDataTyped<int> *creator = static_cast<NetworkDataTyped<int> *>(info.lastSentMsg->values[0].GetRef());
NetworkDataTyped<int> *id = static_cast<NetworkDataTyped<int> *>(info.lastSentMsg->values[1].GetRef());
LogF
(
"Message %x (%d:%d) assigned to object %x (%d:%d)",
info.lastSentMsg.GetRef, creator->value, id->value,
object, object->GetNetworkId().creator.CreatorInt(), object->GetNetworkId().id
);
}
#endif

      }
      else
      {
        ADD_COUNTER(netFN, 1);
        ADD_COUNTER(netFS, info.lastCreatedMsg->size);
        if (DiagLevel >= 4)
        {
          DiagLogF("Client: sent of message %x failed", msgID);
        }
      }
      info.lastCreatedMsg = NULL;
      info.lastCreatedMsgId = 0xFFFFFFFF;
      info.lastCreatedMsgTime = 0;
#if LOG_SEND_PROCESS
      LogF("  - update info %x updated", &info);
#endif
    }
  }

#if CHECK_MSG
  CheckLocalObjects();
#endif
}

//! Text representation of NetworkClientState enum values
const EnumName NetworkClientStateNames[]=
{
  EnumName(NCSNone, "NONE"),
  EnumName(NCSCreated, "CREATED"),
  EnumName(NCSConnected, "CONNECTED"),
  EnumName(NCSLoggedIn, "LOGGED IN"),
  EnumName(NCSMissionSelected, "MISSION SELECTED"),
  EnumName(NCSMissionAsked, "MISSION ASKED"),
  EnumName(NCSRoleAssigned, "ROLE ASSIGNED"),
  EnumName(NCSMissionReceived, "MISSION RECEIVED"),
  EnumName(NCSGameLoaded, "GAME LOADED"),
  EnumName(NCSBriefingShown, "BRIEFING SHOWN"),
  EnumName(NCSBriefingRead, "BRIEFING READ"),
  EnumName(NCSGameFinished, "GAME FINISHED"),
  EnumName(NCSDebriefingRead, "DEBRIEFING READ"),
  EnumName()
};

//! Map NetworkClientState enum values to its text representation
template<>
const EnumName *GetEnumNames(NetworkClientState dummy) {return NetworkClientStateNames;}

void NetworkClient::SetClientState(NetworkClientState state)
{
  if (state == NCSRoleAssigned)
  {
    // Clear old storage of other client camera positions
    GWorld->ClearClientCameraPositions();
  }
  
  if (state == NCSBriefingRead && _clientState != NCSBriefingRead)
    OnMissionStarted();

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  // Xbox Live - start / end the session
  if (state == NCSBriefingRead)
  {
    if (_clientState != NCSBriefingRead)
    {
      _parent->StartSession();
    }
  }
  else if (_clientState == NCSBriefingRead)
  {
    _parent->EndSession();
  }
#endif

  //RptF("*** Client: SetClientState %s -> %s", (const char *)FindEnumName(_clientState), (const char *)FindEnumName(state));
  _clientState = state;
  PlayerIdentity *identity = FindIdentity(_player);
  if (identity) identity->clientState = state;

  if (state >= NCSConnected)
  {
    ClientStateMessage msg;
    msg._clientState = state;
    SendMsg(&msg, NMFGuaranteed);
  }

  // reset post processes
  if (state == NCSMissionAsked)
  {
    GEngine->ResetGameState();
  }

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  // reset the values which have not yet sense
  if (state < NCSMissionSelected)
  {
    // - MPType
    GSaveSystem.SetContext(CONTEXT_GAME_MPTYPE, 0);
    // - World
    GSaveSystem.SetContext(CONTEXT_GAME_WORLD, 0);
    // - Difficulty
    GSaveSystem.SetContext(CONTEXT_GAME_DIFFICULTY, 0);
    // - MPMission
    GSaveSystem.SetContext(CONTEXT_GAME_MPMISSION, 0);
  }
#endif
}

bool NetworkClient::GetTransferProgress(float &progress) const
{
  if (_totalToCreate < 0 || _totalToUpdate < 0) return false;
  if (_totalToCreate + _totalToUpdate == 0) progress = 1.0f;
  else progress = 1.0f - (float)(_remainingToCreate + _remainingToUpdate) / (float)(_totalToCreate + _totalToUpdate);

// DIAG_MESSAGE(500, Format("Created %d / %d, updated %d / %d", _remainingToCreate, _totalToCreate, _remainingToUpdate, _totalToUpdate));
  return true;
}

int NetworkClient::TimeElapsed() const
{
  return GlobalTickCount() - _missionHeader._start;
}

void NetworkClient::OnJoiningInProgress()
{
  int timeElapsed = TimeElapsed();
  // FIX: AdvanceTime should not be used here as NCMTMissionDate command is sent instead
  // Glob.clock.AdvanceTime(OneSecond * 0.001 * timeElapsed);
  Glob.time = Time(timeElapsed);
}

float NetworkClient::GetRespawnRemainingTime() const
{
  for (int i=0; i<_respawnQueue.Size(); i++)
  {
    if (_respawnQueue[i]->IsPlayer()) return _respawnQueue[i]->GetTime() - Glob.time;
  }
  return -1;
}

float NetworkClient::GetVehicleRespawnRemainingTime() const
{
  AIBrain *player = GWorld->FocusOn();
  if (!player) return -1;
  Transport *vehicle = player->FindRespawnVehicle();
  if (!vehicle) return -1;
  for (int i=0; i<_respawnQueue.Size(); i++)
  {
    if (_respawnQueue[i]->GetVehicle() == vehicle) return _respawnQueue[i]->GetTime() - Glob.time;
  }
  return -1;
}

void NetworkClient::OnClientStateChanged(NetworkClientState state)
{
// RptF("*** Client: Client state forced %s -> %s", (const char *)FindEnumName(_clientState), (const char *)FindEnumName(state));

  NetworkClientState origState = _clientState; 


#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  // Xbox Live - start / end the session
  if (state == NCSBriefingRead)
  {
    if (_clientState != NCSBriefingRead)
    {
      _parent->StartSession();
    }
  }
  else if (_clientState == NCSBriefingRead)
  {
    _parent->EndSession();
  }
#endif

  _clientState = state;
  PlayerIdentity *identity = FindIdentity(_player);
  if (identity) identity->clientState = state;

  switch (state)
  {
  case NCSMissionReceived:
    // after _clientState assigned - network object is created in PrepareGame
    if (origState != NCSRoleAssigned)
      RptF
      (
        "NetworkClient::OnClientStateChanged state %s, expected %s",
        (const char *)FindEnumName(origState),
        (const char *)FindEnumName(NCSRoleAssigned)
      );
    DoAssert(_missionFileValid || _missionHeader._hasMissionParams);
    _missionFileValid = true;
    if (_parent->IsServer())
    {
      // clear statistics now - ensure they will not be cleared after stats info from clients arrived
      GStats.ClearMission();
    }
    else
    {
      // do even more on non-BOT 
      DoVerify(PrepareGame());
    }
    // empty event handlers set in the previous mission
    _publicVariableEventHandlers.Clear();
    break;
  case NCSBriefingRead:
    if (origState != NCSBriefingRead)
      OnMissionStarted();
    break;
  }

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  // reset the values which have not yet sense
  if (state < NCSMissionSelected)
  {
    // - MPType
    GSaveSystem.SetContext(CONTEXT_GAME_MPTYPE, 0);
    // - World
    GSaveSystem.SetContext(CONTEXT_GAME_WORLD, 0);
    // - Difficulty
    GSaveSystem.SetContext(CONTEXT_GAME_DIFFICULTY, 0);
    // - MPMission
    GSaveSystem.SetContext(CONTEXT_GAME_MPMISSION, 0);
  }
#endif
}

void NetworkClient::OnMissionStarted()
{
#if _ENABLE_CHEATS
  extern int GNextPersonDpnid;
  GNextPersonDpnid = 100; //reinit
#endif
  // Register client info as network object
  LogF("Client: Game started, creating client info");
  CreateLocalObject(_clientInfo);
  UpdateObject(_clientInfo, NMFGuaranteed);

  // Register client camera position object
  _clientCameraPosition->_dpnid = GetPlayer();
  CreateObject(_clientCameraPosition);
  UpdateObject(_clientCameraPosition, NMFGuaranteed);

  // number of bodies we want to hide in mission
  _hideBodies = -BODIES_PER_CLIENT;
  ConstParamEntryPtr entry = Pars.FindEntry("maxBodiesAdded");
  if (entry)
  {
    int maxBodiesAdded = *entry;
    _hideBodies = -maxBodiesAdded;
  }
  _bodies.Clear();

  // number of wrecks we want to hide in mission
  _hideWrecks = -WRECKS_PER_CLIENT;
  entry = Pars.FindEntry("maxWrecksAdded");
  if (entry)
  {
    int maxWrecksAdded = *entry;
    _hideWrecks = -maxWrecksAdded;
  }
  _wrecks.Clear();

  _missionHeader._playerRespawnTime = _missionHeader._respawnDelay;

  //Check the selected chat channel (possibly disabled channel was selected in briefing)
  void SetChatChannel(ChatChannel channel);
  SetChatChannel(ActualChatChannel());
  GWorld->OnChannelChanged();

  BankList::ReadAccess banks(GFileBanks);
  //@{ Ant1HaX0R section
  // Send the Loaded Bank Count
  // hidden inside the NMTChat ... channel = ( #signed files xor (dpnid & 0x7fffffff) ) | 0x80000000
  int nSignedBanks = 0;
  for (int i=0; i<banks.Size(); i++) 
  {
    if (banks[i].VerifySignature()) nSignedBanks++;
  }
  int nFiles = _additionalSignedFiles.Size() + nSignedBanks;
  int fakeChannel = ((_player & 0x7fffffff) ^ nFiles) | 0x80000000;
  GetNetworkManager().Chat((ChatChannel)fakeChannel,"2"); // I know level 2
  //@}
}

void NetworkClient::ClearMissionVoting()
{
  _missionVoting._diff = RString();
}

void NetworkClient::OnServerStateChanged(NetworkServerState state)
{
//RptF("Client: OnServerStateChanged to %s", (const char *)FindEnumName(state));

#if _VBS3
  // hot fix, for VTK demonstration
  if(IsDedicatedServer())
  {
    if((_serverState == NSSPlaying) && (state <= NSSMissionAborted))
    {
      GStats.OnMPMissionEnd();// write everthing out
      GStats.ClearMission();// clear all the stats from previous mission
    }      
  }
#endif

  //LogF("Old state %s, received %s", GameStateNames[_state], GameStateNames[gs.gameState]);
  _serverState = state;
  _serverTimeout = INT_MAX;
  
  switch (state)
  {
  case NSSNone:
    SetClientState(NCSNone);
    break;
  case NSSSelectingMission:
  case NSSEditingMission:
    if (_clientState > NCSLoggedIn)
      SetClientState(NCSLoggedIn);
    break;
  case NSSAssigningRoles:
    if (_clientState > NCSMissionAsked)
      SetClientState(NCSMissionAsked);
    break;
  case NSSSendingMission:
    // if (IsDedicatedServer()) SetClientState(NCSRoleAssigned);
    break;
  case NSSLoadingGame:
    // used to inform server that transfer was finished
    if (IsDedicatedServer()) SetClientState(NCSMissionReceived);
    break;
  case NSSBriefing:
    if (GetMaxError() >= EMError)
    {
      // some error when loading mission
      RString message = GetMaxErrorMessage();
#if !_VBS2  // disable MP missing addon warning messages (temp)
      if (message.GetLength() <= 0)
      {
        RString ReportMissingAddons(FindArrayRStringCI missing);
        message = ReportMissingAddons(CurrentTemplate.missingAddOns);
      }
#endif
      const PlayerIdentity *id = FindIdentity(_player);
      if (id)
      {
        RString senderName = id->name;
        GChatList.Add(CCSystem, senderName, message, false, true);
        RefArray<NetworkObject> dummy;
        GNetworkManager.Chat(CCSystem,senderName,dummy,message);
      }
      // we might want to disconnect now
    }

    _respawnQueue.Clear();
    if (_clientState == NCSMissionReceived)
    {
      SetClientState(NCSGameLoaded);
      LogF("Enforced NCSGameLoaded state");
    }
    break;
  case NSSPlaying:
    {
    if (_clientState == NCSGameLoaded || _clientState == NCSBriefingShown)
      SetClientState(NCSBriefingRead);
    _missionHeader._start = GlobalTickCount();
#if _VBS3 // player color
      EntityAI *player = GWorld->GetRealPlayer();
      if (player && !_sentPlayerColour)
      {
        RString FindPicture (RString name);
        Ref<Texture> texture = GlobLoadTexture(FindPicture(Glob.playerColor));
        if (texture && !texture->IsAlpha())
        {
          int index = player->GetType()->GetHiddenSelectionIndex("hide_teamcolor");
          if(index > -1)
          {
            player->SetObjectTexture(index, texture);
            GetNetworkManager().SetObjectTexture(player,index,FindPicture(Glob.playerColor),"side player == side _this");
          }
        }
      }
#endif
    }
    break;
  case NSSMissionAborted:
    if (_clientState == NCSBriefingRead) SetClientState(NCSGameFinished);
    break;
  }
  
#if _VBS3 // player color
  _sentPlayerColour = state == NSSPlaying;
#endif

  if (state!=NSSSelectingMission)
  {
    // if server is not selecting a mission, voting results habe no sense
    ClearMissionVoting();
    _voteMission = false;

  }

  #if _EXT_CTRL //ext. Controller like HLA, AAR
    _hla.ChangeSimulationState(state);
  #endif

//LogF("New state %s", GameStateNames[_state]);
}

unsigned NetworkClient::CleanUpMemory()
{
  if (_client) return _client->FreeMemory();
  return 0;
}

void NetworkClient::OnClose()
{
#if _USE_BATTL_EYE_CLIENT
  if (_player != BOT_CLIENT || !IsDedicatedServer()) // DS bot client does not represent any player
  {
    _beIntegration.Done();
  }
#endif
}

bool NetworkClient::CheckLocalObjects() const
{
  for (LocalObjectsContainer::Iterator it=_localObjects.begin(); it; _localObjects.next(it))
  {
    NetworkLocalObjectInfo &localObject = *it;
    NetworkObject *object = localObject.object;
    if (object)
    {
      for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
      {
        const NetworkUpdateInfo &info = localObject.updates[j];
        if (info.lastCreatedMsg)
        {
          NetworkMessageNetworkObject *message = (NetworkMessageNetworkObject *)info.lastCreatedMsg.GetRef();
          if (message->_objectCreator != object->GetNetworkId().creator || message->_objectId != object->GetNetworkId().id)
          {
            Fail("lastCreatedMsg is bad");
            return false;
          }
        }
        else if (info.lastSentMsg)
        {
          NetworkMessageNetworkObject *message = (NetworkMessageNetworkObject *)info.lastSentMsg.GetRef();
          if (message->_objectCreator != object->GetNetworkId().creator || message->_objectId != object->GetNetworkId().id)
          {
            Fail("lastSentMsg is bad");
            return false;
          }
        }
      }
    }
  }
  return true;
}

//! Check if given unit is in list of units
/*!
\param soldier unit to find
\param units checked array of units
*/
static bool FindUnit(NetworkObject *soldier, RefArray<NetworkObject> &units)
{
  if (!soldier) return false;
  for (int i=0; i<units.Size(); i++)
  {
    if (units[i] == soldier) return true;
  }
  return false;
}

//! Find output radio channel for given unit and chat channel
/*!
\param unit unit
\param channel chat channel
*/
RadioChannel *FindChannel(AIBrain *unit, int channel)
{
  switch (channel)
  {
  case CCVehicle:
    {
      Transport *veh = unit->GetVehicleIn();
      return veh ? &veh->GetRadio() : NULL;
    }
  case CCGroup:
    {
      AIGroup *grp = unit->GetGroup();
      return grp ? &grp->GetRadio() : NULL;
    }
  case CCCommand:
    {
      AIGroup *grp = unit->GetGroup();
      if (!grp) return NULL;
      AICenter *center = grp->GetCenter();
      return center ? &center->GetCommandRadio() : NULL;
    }
  case CCSide:
    {
      AIGroup *grp = unit->GetGroup();
      if (!grp) return NULL;
      AICenter *center = grp->GetCenter();
      return center ? &center->GetSideRadio() : NULL;
    }
  case CCGlobal:
    return &GWorld->GetRadio();
  case CCDirect: 
    { // direct speaking
      Person *person = unit->GetPerson();
      return person ? person->GetRadio() : NULL;
    }
  default:
    RptF("Client: Bad radio channel %d", channel);
    return NULL;
  }
}

//! Check whole AI structute for consistency
static bool AssertAIValid()
{
  for (int side=0; side<TSideUnknown; side++)
  {
    AICenter *center = GWorld->GetCenter((TargetSide)side);
    if (!center) continue;
    if (!center->AssertValid()) return false;
  }
  return true;
}

RString MissionHeader::GetLocalizedMissionName() const
{
  return _name.GetLocalizedValue();
}
RString MissionHeader::GetLocalizedMissionDesc() const
{
  return _description.GetLocalizedValue();
}

#if _ENABLE_UNSIGNED_MISSIONS
/*!
\patch 1.78 Date 7/22/2002 by Jirka
- Fixed: Better usage of already transferred multiplayer missions (cache added)
*/
bool CheckMissionFile(RString fileName, MissionHeader &header)
{
#ifdef _XBOX
  char buffer[256];
  const char *FullXBoxName(const char *name, char *temp);
  fileName = FullXBoxName(fileName, buffer);
#endif
  RString fileNameExt = fileName + RString(".pbo");

#ifdef _WIN32

  QFileSize size = QIFileFunctions::GetFileSize(fileNameExt);

  if
  (
    header._fileSizeL != size ||
    header._fileSizeH != 0
  ) return false; 

#else   // !defined _WIN32

  LocalPath(fn,fileNameExt);
  struct stat st;
  if ( stat(fn,&st) ) return false;
#if 1       // (sizeof(off_t) <= 4)
  if ( header._fileSizeH ||
       st.st_size != header._fileSizeL )
    return false;
#else
  if ( st.st_size != header._fileSizeL + (((off_t)header._fileSizeH) << 32) )
    return false;
#endif

#endif

  QIFStream f;
  f.open(fileNameExt);
  CRCCalculator crc;
  crc.Reset();
  // use copy function - it does exactly what we need
  f.copy(crc);
  if (crc.GetResult() != header._fileCRC) return false;
  //if (crc.CRC(f.act(), f.rest()) != header.fileCRC) return false;

  CreateMPMissionBank(fileName, header._island);
  return true;
}
#endif

//! client-side settings:
//! custom face file bigger than given limit is ignored
int MaxCustomFaceSize = 100*1024;
//! custom sound file bigger than given limit is ignored
//int MaxCustomSoundSize = 40*1024;
int MaxCustomSoundSize = 50*1024;
//! server-side setting:
//! client attemping to transfer more than given limit will be kicked
/*!
\patch 1.87 Date 10/17/2002 by Ondra
- New: Server can determine max. size of any single custom file acceptable on the server.
To do this add line MaxCustomFileSize=size_in_bytes into Flashpoint.cfg file.
Users with custom face or custom sound larger than this size are kicked when trying to connect.
*/

int MaxCustomFileSize = 128*1024;

static int FileSize(const char *name)
{
  #ifdef _WIN32
  struct stat st;
  if ( stat(name,&st) ) return INT_MAX;
  #else
  LocalPath(fn,name);
  struct stat st;
  if ( stat(fn,&st) ) return INT_MAX;
  #endif
  if (st.st_size>INT_MAX) return INT_MAX;
  return st.st_size;
}

#define NMT_UPDATE_FORMAT(macro, class, name, description, group) \
  case NMT##name: \
  { \
    int index = 0; \
    void UpdateMessageFormat##name(NetworkMessageFormatBase *format, int &index); \
    UpdateMessageFormat##name(format, index); \
    break; \
  }

void UpdateMessageFormat(NetworkMessageFormatBase *format, int type)
{
  switch (type)
  {
  NETWORK_MESSAGE_TYPES(NMT_UPDATE_FORMAT)
#if _ENABLE_VBS
  NETWORK_MESSAGE_TYPES_VBS(NMT_UPDATE_FORMAT)
# if _VBS2
  NETWORK_MESSAGE_TYPES_VBS2(NMT_UPDATE_FORMAT)
# endif
#if _VBS3 // respawn command
  NETWORK_MESSAGE_TYPES_VBS3(NMT_UPDATE_FORMAT)
#endif
#endif
  default:
    Fail("Unknown message type");
    break;
  }
}

static RString GetMissionCacheDir()
{
  char dir[MAX_PATH];
  if (GetLocalSettingsDir(dir))
  {
    CreateDirectory(dir, NULL);
    TerminateBy(dir, '\\');
    strcat(dir, "MPMissionsCache\\");
    return dir;
  }

  return "MPMissionsCache\\";
}

RString GetClientPath(RString src)
{
  if (src.GetLength() == 0) return src;
  if (src[0] != '$') return src;

  // translate char 's' to Local Settings directory
  char dir[MAX_PATH];
  if (!GetLocalSettingsDir(dir)) return src.Substring(2, INT_MAX); // place in current directory

  TerminateBy(dir, '\\');
  return RString(dir) + src.Substring(2, INT_MAX);
}

RString NetworkClient::GetDebugVoNString()
{
  AutoArray<ConvPair, MemAllocLocal<ConvPair, 64> > convTable;
  convTable.Realloc(_identities.Size());
  convTable.Resize(_identities.Size());
  for (int i=0; i<_identities.Size(); i++)
  {
    convTable[i]._dpnid = _identities[i].dpnid;
    convTable[i]._id = _identities[i].GetDebugId();
    convTable[i]._name = _identities[i].name;
  }
  QOStrStream out;
  out << GDebugVoNBank.GetDebugString(convTable); //basic info (features collected by client)
  //add version info
  RString ver = Format("Version: %s.%d\n", APP_VERSION_TEXT, BUILD_NO);
  out << ver;
  //add IP:PORT info
  in_addr local;
  if (_client && _client->GetLocalAddress(local))
  {
    RString privateIP = Format("Private: %d.%d.%d.%d\n",
      (unsigned)(local.s_addr & 0xff), (unsigned)((local.s_addr>>8) & 0xff),
      (unsigned)((local.s_addr>>16) & 0xff), (unsigned)((local.s_addr>>24) & 0xff)
      );
    out << privateIP;
  }
#if _ENABLE_GAMESPY
  if (_publicAddr!=0)
  {
    RString publicIP = Format("Public: %d.%d.%d.%d\n",
      _publicAddr&0xff, (_publicAddr>>8)&0xff, (_publicAddr>>16)&0xff, (_publicAddr>>24)&0xff
      );
    out << publicIP;
  }
#endif
  if (GetNetworkManager().IsVoiceRecording())
  {
    out << "Note: I am speaking!\n";
  }
#if _ENABLE_MP //making binarize project linkable
  //add Connection type information for each player
  out << "Connections:\n";
  bool serverFound = false;
  for (int i=0; i<_identities.Size(); i++)
  {
    if (_identities[i].dpnid==_player) continue; //me
    if (_identities[i].dpnid==BOT_CLIENT) serverFound=true; //server
    // connection type (direct, NAT)
    RString cdid = _identities[i].GetDebugId();
    if (cdid.IsEmpty()) cdid = Format("#%d", _identities[i].dpnid);
    // find corresponding TransmitTarget
    RString diag = GetVoiceTargetDiagnostics(_identities[i].dpnid);
    // everything is ready for output
    out << Format("  %s: %s\n", cc_cast(cdid), cc_cast(diag));
  }
  //serve server at last (on standalone servers, BOT_CLIENT has no identity)
  if (!serverFound && _player!=BOT_CLIENT) //exclude connection server to server 
  {
    RString cdid = Format("#%d", BOT_CLIENT);
    // find corresponding TransmitTarget
    RString diag = GetVoiceTargetDiagnostics(BOT_CLIENT);
    out << Format("  %s: %s\n", cc_cast(cdid), cc_cast(diag));
  }
  // cliques diagnostics
  GetVoiceCliquesDiagnostics(out, convTable);
#endif

  return RString(out.str(), out.pcount());
}

/*!
\patch 5191 Date 11/27/2007 by Jirka
- New: publicVariable now works for all types of variables
*/

bool ReadValue(GameValue &value, QIStream &in, INetworkManager *netManager)
{    
  GameState *gstate = GWorld->GetGameState();
  GameDataNamespace *globalsExt = GWorld->GetMissionNamespace(); // global variables for compiling of GameDataCode

  // read the type name
  int typeSize;
  in.read(&typeSize, sizeof(int));
  if (typeSize==-1) return false;
  RString typeName(NULL, typeSize + 1);
  char *ptr = typeName.MutableData();
  in.read((char *)ptr, typeSize);
  ptr[typeSize] = 0;
  // find the type
  const GameTypeType *typeType = gstate->FindType(typeName);
  if (!typeType)
  {
    RptF("Unknown variable type %s", cc_cast(typeName));
    return false;
  }
  GameType type(typeType); // TODO: optimize - avoid GameType temporary

  if (type == GameScalar)
  {
    GameScalarType data;
    in.read(&data, sizeof(data));
    value = data;
  }
  else if (type == GameBool)
  {
    GameBoolType data;
    in.read(&data, sizeof(data));
    value = data;
  }
  else if (type == GameString)
  {
    int len = in.rest();
    RString data(NULL, len + 1);
    char *ptr = data.MutableData();
    in.read((char *)ptr, len);
    ptr[len] = 0;
    value = data;
  }
  else if (type == GameObject)
  {
    NetworkId id;
    in.read(&id, sizeof(id));
    Object *obj = netManager ? dynamic_cast<Object *>(netManager->GetObject(id)) : NULL;
    value = GameValueExt(obj);
  }
  else if (type == GameGroup)
  {
    NetworkId id;
    in.read(&id, sizeof(id));
    AIGroup *grp = netManager ? dynamic_cast<AIGroup *>(netManager->GetObject(id)) : NULL;
    value = GameValueExt(grp);
  }
#if _ENABLE_INDEPENDENT_AGENTS
  else if (type == GameTeamMember)
  {
    NetworkId id;
    in.read(&id, sizeof(id));
    AITeamMember *member = netManager ? dynamic_cast<AITeamMember *>(netManager->GetObject(id)) : NULL;
    value = GameValueExt(member);
  }
#endif
  else
  {
    Ref<GameData> data = gstate->CreateGameData(type, NULL);
    if (!data) return false;

    GameDataNamespace globals(NULL, RString(), false); // save - no global namespace needed

    // load content of variable from the stream (using its serialization)
    ParamArchiveLoad ar;
    ar.SetParams(gstate);
    ar.SetParams(netManager,1);
    ar.LoadBin(in, NULL, &globals);

    ar.FirstPass();

    GameDataNamespace *oldNamespace = gstate->SetGlobalVariables(globalsExt);
    LSError err = data->Serialize(ar);
    gstate->SetGlobalVariables(oldNamespace);
    if (err != LSOK) return false;
    
    ar.SecondPass();
    gstate->SetGlobalVariables(globalsExt);
    err = data->Serialize(ar);
    gstate->SetGlobalVariables(oldNamespace);
    if (err != LSOK) return false;

    value = GameValue(data.GetRef());
  }
  return true;
}

#if _VBS3 // handle clipLandKeep objects
	DEF_RSB(slope)
	DEF_RSB(placement)
	DEF_RSB(vertical)
#endif

/// delete all files in $/Player/name/*.*, which are not contained in the fileList
void DeleteObsoleteFiles(const char *name, const AutoArray<CustomFilePaths> &fileList, bool init=false)
{
  if (!name || *name == 0) return;
  RString path;
  if (init)
  {
    path = GetClientPath(RString("$\\players\\") + EncodeFileName(name));
    name = cc_cast(path);
  }

#ifdef _WIN32
  BString<1024> buffer;
  sprintf(buffer, "%s\\*.*", name);

  _finddata_t info;
  long h = X_findfirst(buffer, &info);
  if (h != -1)
  {
    do
    {
      if ((info.attrib & _A_SUBDIR) != 0)
      {
        // FIX - do not delete only current and parent directory
        if (strcmp(info.name, ".") != 0 && strcmp(info.name, "..") != 0)
        {
          sprintf(buffer, "%s\\%s", name, info.name);
          DeleteObsoleteFiles(buffer, fileList);
        }
      }
      else
      {
        sprintf(buffer, "%s\\%s", name, info.name);
        bool found = false;
        for (int i=0; i<fileList.Size(); i++)
        {
          RString path = GetClientPath(fileList[i]._dst);
          if (stricmp(path, buffer)==0) 
          {
            found = true;
            break;
          }
        }
        if (!found) 
        {
          //RptF("Unlinking: %s", cc_cast(buffer));
          QIFileFunctions::Unlink(buffer);
        }
      }
    }
    while (_findnext(h, &info)==0);
    _findclose(h);
  }
#endif
}

static void GetCustomFilesWanted(/*OUT*/CustomFilesWantedMessage &customFilesWantedMsg, /*IN*/const AutoArray<CustomFilePaths> &fileList)
{
  AutoArray<int> &wanted = customFilesWantedMsg._filesWanted;
  for (int i=0; i<fileList.Size(); i++)
  {
    RString path = GetClientPath(fileList[i]._dst);
    if (QIFileFunctions::FileExists(path) && FileSize(path)==fileList[i]._len)
    {
      unsigned int GetFileCRC(const char *fullname);
      unsigned int crc = GetFileCRC(path);
      if (crc == fileList[i]._crc) continue; //file is correct, no send is necessary
    }
    wanted.Add(i);
  }
}

void VehSetMPEventHandlers(EntityAI *ai, MPEntityEvent event, AutoArray<RString> handlers)
{
  if (ai && event<NMPEntityEvent)
  {
    ai->SetMPEventHandlers(event, handlers);
  }
}

/*!
\patch 1.01 Date 06/18/2001 by Jirka
- Fixed: bad experience increase in MP
- when killed unit on other machine no experience to player was added
\patch_internal 1.01 Date 06/18/2001 by Jirka
- fixed - added experience change to NMTVehicleDestroyed message
\patch_internal 1.26 Date 10/02/2001 by Ondra
- New: message class for updating dammage.
\patch 1.26 Date 10/02/2001 by Ondra
- Improved: decreased lag when hitting other units.
\patch_internal 1.28 Date 10/29/2001 by Jirka
- Fixed: do not update local objects from network
\patch 1.34 Date 12/03/2001 by Jirka
- Fixed: Join in multiplayer
\patch 1.34 Date 12/04/2001 by Jirka
- Fixed: Waypoint synchronization in multiplayer
\patch 1.34 Date 12/04/2001 by Jirka
- Fixed: Trigger activation through radio in multiplayer
\patch 1.43 Date 1/30/2002 by Jirka
- Fixed: Crashes in MP if player waits in lobby and game is in progress
\patch 1.43 Date 2/4/2002 by Jirka
- Fixed: Sometimes camera was on bad person if two players gets in vehicle simultaneously in MP
\patch 1.47 Date 3/8/2002 by Ondra
- Fixed: Config verification checks have increased priority
to avoid timeouts during mission transfer.
\patch 1.56 Date 5/13/2002 by Ondra
- Fixed: More robust handling of addons missing on client.
\patch 1.80 Date 8/5/2002 by Jirka
- Fixed: MP - when admin logged in during select mission screen, no missions was listed
\patch 5114 Date 1/7/2007 by Pete
- Fixed: setWPPos and setWaypointPosition now modifies waypoint position on all computers in MP
\patch_internal 5138 Date 3/9/2007 by Jirka
- Fixed: MP - signatures are not checked for files without config
*/

// note, cannot use TMCHECK inside NetworkClient::OnMessage, as it returns a value
#define TMSGCHECK(command) \
{TMError err = command; if (err != TMOK && err != TMNotFound) break;}

void NetworkClient::OnMessage(int from, NetworkMessage *msg, NetworkMessageType type)
{
  if (_serverState == NSSNone) return;
  
  FreeOnDemandGarbageCollect(1024*1024,256*1024);
  
  NetworkMessageFormatBase *format = GetFormat(/*from, */type);
  if (!format)
  {
    RptF("Client: Bad message %d(%s) format", (int)type, cc_cast(GetMsgTypeName(type)));
    return;
  }
  NetworkMessageContext ctx(msg, this, from, MSG_RECEIVE);

  #if _ENABLE_CHEATS
  int level = GetDiagLevel(type,!_parent->GetServer());
  if (level >= 2)
  {
    DiagLogF("Client: received message %s from %d", cc_cast(GetMsgTypeName(type)), from);   
    ctx.LogMessage(format, level, "\t");
  }
  #endif

  bool validBefore = true;
  if (_clientState == NCSBriefingRead) validBefore = AssertAIValid();

  switch (type)
  {
    case NMTMsgFormat:
      {
        NetworkMessageMsgFormat *message = (NetworkMessageMsgFormat *)msg;
        int index = message->_index;
        if (index >= NMTN) break; // unknown message
        if (index < NMTFirstVariant) break; 

        int pos = index - NMTFirstVariant;
        _formats.Access(pos);
        TMSGCHECK(_formats[pos].TransferMsg(ctx))
        ::UpdateMessageFormat(&_formats[pos], index);
      }
      break;
    case NMTIntegrityQuestion:
      {
        IntegrityQuestionMessage question;
        TMSGCHECK(question.TransferMsg(ctx))

        IntegrityAnswerMessage answer;
        answer._id = question._id;
        answer._type = question._type;
//((EXE_CRC_CHECK
        answer._answer = IntegrityCheckAnswer
        (
          (IntegrityQuestionType)question._type,
          IntegrityQuestion(question._name, question._offset, question._size),
          false // client
        );
//))EXE_CRC_CHECK
        SendMsg(&answer, NMFGuaranteed|NMFHighPriority);
      }
      break;
    case NMTPlayerClientState:
      {
        PlayerClientStateMessage state;
        TMSGCHECK(state.TransferMsg(ctx))
        PlayerIdentity *identity = FindIdentity(state._dpnid);
        if (identity) identity->clientState = (NetworkClientState)state._clientState;
      }
      break;
    case NMTClientState:
      {
        ClientStateMessage state;
        TMSGCHECK(state.TransferMsg(ctx))

        OnClientStateChanged((NetworkClientState)state._clientState);
      }
      break;
    case NMTNetworkCommand:
      {
        NetworkCommandMessage cmd;
        TMSGCHECK(cmd.TransferMsg(ctx))
        switch (cmd._type)
        {
        case NCMTLogged:
          {
            _gameMaster = true;
            cmd._content.Read(&_admin, sizeof(_admin));
            GChatList.Add(CCSystem, NULL, LocalizeString(IDS_MP_LOGGED), false, true);
          }
          break;
        case NCMTLoggedOut:
          {
            _gameMaster = false;
            _selectMission = false;
            GChatList.Add(CCSystem, NULL, LocalizeString(IDS_MP_LOGGED_OUT), false, true);
          }
          break;
        case NCMTExecResult:
          {
            RString result = cmd._content.ReadString();
            GChatList.Add(CCSystem, NULL, result, false, true);
          }
          break;
        case NCMTVoteMission:
          {
            _serverMissions.Clear();
            while (true)
            {
              RString mission = cmd._content.ReadString();
              if (mission.GetLength() == 0) break;
              int index = _serverMissions.Add();
              MPMissionInfo &info = _serverMissions[index];
              info.mission = mission;
              info.world = cmd._content.ReadString();
              info.gameType = cmd._content.ReadString();
              info.displayName = cmd._content.ReadLocalizedString();
              cmd._content.Read(&info.placement, sizeof(int));
              cmd._content.Read(&info.minPlayers, sizeof(int));
              cmd._content.Read(&info.maxPlayers, sizeof(int));
              cmd._content.Read(&info.perClient, sizeof(int));
            }
#if !_XBOX_SECURE
            if (_gameMaster) _selectMission = true;
#endif
            _voteMission = true;
          }
          break;
        case NCMTVoteMissionExt:
          {
            int count;
            cmd._content.Read(&count, sizeof(int));
            for (int i=0; i<count; i++)
            {
              MPMissionInfo &info = _serverMissions[i];
              info.description = cmd._content.ReadLocalizedString();
              cmd._content.Read(&info.respawn, sizeof(int));
            }
          }
          break;
        case NCMTMonitorAnswer:
          {
            float fps = 0;
            int memory = 0;
            float in = 0;
            float out = 0;
            cmd._content.Read(&fps, sizeof(fps));
            cmd._content.Read(&memory, sizeof(memory));
            cmd._content.Read(&in, sizeof(in));
            cmd._content.Read(&out, sizeof(out));
          
            int sizeNG = 0, sizeG = 0;
            cmd._content.Read(&sizeG, sizeof(sizeG));
            cmd._content.Read(&sizeNG, sizeof(sizeNG));
            
            char buffer[256];
            sprintf
            (
              buffer, LocalizeString(IDS_SERVER_MONITOR),
              fps, 1e-6 * memory, 8e-3 * out, 8e-3 * in,
              sizeNG, sizeG
            );
            GChatList.Add(CCSystem, NULL, buffer, false, true);
          }
          break;
        case NCMTDebugAnswer:
          LogDebugger(cmd._content.ReadString());
          break;
#if _VBS3
        case NCMTExitAll:
            // shut down the client
            #ifdef _XBOX
            #elif defined _WIN32
            ::PostMessage((HWND)hwndApp, WM_CLOSE, 0, 0);
            #endif
          break;
#endif
        case NCMTMissionTimeElapsed:
          {
            int timeElapsed = 0;
            cmd._content.Read(&timeElapsed, sizeof(timeElapsed));
            _missionHeader._start = GlobalTickCount() - timeElapsed;
          }
          break;
        case NCMTMissionDate:
          {
            // set current clock
            Glob.clock.Read(cmd._content);
            LightSun *sun = GWorld->GetScene()->MainLight();
            if (sun)
            {
              sun->Recalculate(GWorld);
              GWorld->GetScene()->MainLightChanged();
              GWorld->GetScene()->ResetLandShadows();
            }
          }
          break;
        case NCMTCustomFilesProgress:
          {
            int max = 0;
            int pos = 0;
            cmd._content.Read(&max, sizeof(max));
            cmd._content.Read(&pos, sizeof(pos));
            _customFilesProgressMax = max;
            _customFilesProgressPos = pos;
          }
          break;
        case NCMTAskVoNDiagnostics:
          {
            NetworkCommandMessage answer;
            answer._type = NCMTAskVoNDiagnostics;
            answer._content.WriteString(GetDebugVoNString());
            GDebugVoNBank.Reset(); // next #debug von should not contain old data
            SendMsg(&answer, NMFGuaranteed);
          }
          break;
        }
      }
      break;
    case NMTVotingMissionProgress:
      // update voting mission results
      TMSGCHECK(_missionVoting.TransferMsg(ctx))
      break;
    case NMTPlayer:
      {
        PlayerMessage playerMsg;
        TMSGCHECK(playerMsg.TransferMsg(ctx))
        _player = playerMsg._player;
//{ DEDICATED SERVER SUPPORT
        if (playerMsg._server)
        {
          break; // do not create identity for server
        }
//}

#if _USE_BATTL_EYE_CLIENT
        _beWanted = playerMsg._battlEye;
        if (_beWanted) // BattlEye initialization needed
        {
          if (_player != BOT_CLIENT || !IsDedicatedServer()) // DS bot client does not represent any player
          {
            if ( !_beIntegration.Init(this) )
            {
              // initialization failed, disconnect
              Disconnect("BattlEye initialization failed");
              return;
            }
          }
        }
#endif
        _sigVerRequired = playerMsg._sigVerRequired;
        _parent->SetVoNCodecQuality(playerMsg._vonCodecQuality);
        _parent->SetVoNDisabled(playerMsg._vonDisabled);
        if (playerMsg._vonDisabled)
        {
          void destroyVoice();
          destroyVoice();
        }

        {// send a few messages (random count) to break deterministic serials (and CRC) in packets
          int randMsg = GRandGen.RandomInt(1,20);
          for (int i=0; i<randMsg; i++)
          {
            // use CCVehicle to limit possible receivers of the message
            // in fact, at this stage there is no vehicle and no group, and server will not send the message
            // messages have NMFHighPriority at this stage, preventing aggregation (coalescing)
            Chat(CCVehicle,RString());
          }
        }

#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER >= 200

# else
        // send mute list
        SendMsg(&_parent->GetMuteList(), NMFGuaranteed);
# endif // _XBOX_VER >= 200
#endif
        // send identity
        PlayerIdentity identity;
        identity.dpnid = _player;
//        identity.inaddr.S_un.S_addr = playerMsg._inaddr;
#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER >= 200
        identity.xuid = 0; 
# else
        identity.xuid.qwUserID = 0; 
        identity.xuid.dwUserFlags = 0;
# endif
        ZeroMemory(&identity.xnaddr, sizeof(identity.xnaddr));
# if _XBOX_VER >= 200
        // xuid
        const XUSER_SIGNIN_INFO *signInInfo = GSaveSystem.GetUserInfo();
        if (signInInfo) identity.xuid = signInInfo->xuid;
        
        // xnaddr
        while (XNetGetTitleXnAddr(&identity.xnaddr) == XNET_GET_XNADDR_PENDING);
# else
        if (_parent->IsSignedIn())
        {
          XONLINE_USER *user = XOnlineGetLogonUsers();
          Assert(user);
          if (user) identity.xuid = user->xuid;
        }
        identity.xnaddr = _xnaddr;
        INT err = XNetXnAddrToInAddr(&_xnaddr, &_sessionKid, &identity.inaddr);
        if (err != 0)
        {
          RptF("Client: XNetXnAddrToInAddr failed with error 0x%x", err);
        }
# endif // _XBOX_VER >= 200
        identity.rating = _playerRating;
        identity.voiceOn = _parent->IsVoice();
#endif // _XBOX_SECURE && _ENABLE_MP
        identity.version = MP_VERSION_ACTUAL;
        identity.build = BUILD_NO;
        #if !INTERNAL_KEYS && !_SERVER && _VERIFY_CLIENT_KEY
        {
          if (playerMsg._steamServerId!=0)
          {
            RString playerId = GetPublicKey();
            RString productId = GetProductIdString();
            const Array<unsigned char> &cdKey = GetCDKeyBinary();
            UserCertificate cert = GetUserCertificate(playerMsg._regURL,productId,playerId,cdKey);
            identity.certificate = cert.certificate;
            if (identity.certificate.Size()>0)
            {
              AutoArray<unsigned char> signature = SignData(playerMsg._steamServerId,Array<unsigned char>(reinterpret_cast<unsigned char *>(cert.privateKey.Data()),cert.privateKey.Size()));
              identity.challengeSigned = AutoArray<char>(reinterpret_cast<char *>(signature.Data()),signature.Size());
              identity.blockedKeys = cert.blockedKeys.keys;
              identity.blockedKeysSignature = cert.blockedKeys.signature;
              identity.blockedKeysSignatureWithTime = cert.blockedKeys.signatureWithTime;
              identity.blockedKeysTimestamp = cert.blockedKeys.timestamp;
            }
            else
            {
              RptF("Registration failed: error %s",cc_cast(cert.errorMessage));
              RString message = Format("Registration failed: %s",cc_cast(cert.errorMessage));
              GChatList.Add(CCSystem, NULL, message, false, true);
            }
          }
        }
        #endif
#if BATTLEYE_INTERNAL_ID
// set BATTLEYE_INTERNAL_ID when you need to test BattlEye using internal DEV666 public keys
        RString fakeID = GetPublicKey();
        identity.id = fakeID.Substring(3,fakeID.GetLength());
#else
        identity.id = GetPublicKey();
#endif
        // include userId from the certificate here
        identity.name = playerMsg._name; // name is changed by server
        identity.face = Glob.header.playerFace;
        identity.glasses = Glob.header.playerGlasses;
        identity.speakerType = Glob.header.playerSpeakerType;
        identity.pitch = Glob.header.playerPitch;
        identity._minPing = 10;
        identity._avgPing = 100;
        identity._maxPing = 1000;
        identity._minBandwidth = 14;
        identity._avgBandwidth = 28;
        identity._maxBandwidth = 33;
        identity._headlessClient = IsDedicatedClient();

#if defined _XBOX && _XBOX_VER >= 200
        identity.localPlayerInfoList.Init();
#endif
        BankList::ReadAccess banks(GFileBanks);
        identity.filesCount = banks.Size() + _additionalSignedFiles.Size();

#if _ENABLE_STEAM
        if (UseSteam)
        {
          if (SteamUser())
          {
            identity._steamID = SteamUser()->GetSteamID().ConvertToUint64();

            in_addr addr;
            int port;
            uint64 steamId;
            if (_player == BOT_CLIENT)
            {
              addr.s_addr = htonl(0x7F000001); // localhost
              port = GetServerPort();
              steamId = (uint64)identity._steamID;
            }
            else
            {
              _client->GetDistantAddress(addr, port);
              steamId = (uint64)playerMsg._steamServerId;
            }

            char authBlob[2048];
            int authBlobSize = sizeof(authBlob);
            authBlobSize = SteamUser()->InitiateGameConnection(authBlob, authBlobSize, CSteamID(steamId),
              ntohl(addr.s_addr), ntohs(port), playerMsg._steamSecure);
            identity._steamSessionTicket.Copy(authBlob, authBlobSize);
          }
        }
#endif

        GameDataNamespace globals(NULL, RString(), false); // save - no global namespace
        ParamFile cfg;
        bool ParseUserParams(ParamFile &cfg, GameDataNamespace *globals);
        if (ParseUserParams(cfg, &globals))
        {
          ConstParamEntryPtr entry = cfg.FindEntry("Identity");
          if (entry)
          {
            if (entry->FindEntry("squad"))
              identity.squadId = (*entry) >> "squad";
          }
        }
#ifndef _XBOX
        // send face to server
        bool localServer = _parent->IsServer();
        RString GetUserDirectory();
        if (stricmp(identity.face, "custom") == 0)
        {
          RString srcDir = GetUserDirectory();
          RString dstDir = GetClientCustomFilesDir() + RString("\\players\\") + EncodeFileName(identity.name) + RString("\\");
          DeleteDirectoryStructure(dstDir, true); // clear first
          CreatePath(dstDir);
          RString serverDir;
          if (localServer)
          {
            serverDir = GetServerTmpDir() + RString("\\players\\") + EncodeFileName(identity.name) + RString("\\");
            CreatePath(serverDir);
          }
          else
          {
            // char '#' is used as an indication of Local Settings directory + ServerTmpDir (different on server than on client)
            serverDir = RString("#") + RString("\\players\\") + EncodeFileName(identity.name) + RString("\\");
          }
          RString src = srcDir + RString("face.paa");
          if (QIFileFunctions::FileExists(src) && FileSize(src)<=MaxCustomFaceSize && ValidDataFile(src))
          {
            RString dst = dstDir + RString("face.paa");
            QIFileFunctions::Copy(src, dst); // "send" the file to yourself
            RString server = serverDir + RString("face.paa");
            if (localServer) QIFileFunctions::Copy(src, server);
            else TransferFileToServer(server, src);
          }
          else
          {
            src = srcDir + RString("face.jpg");
            if (QIFileFunctions::FileExists(src) && FileSize(src)<=MaxCustomFaceSize && ValidDataFile(src))
            {
              RString dst = dstDir + RString("face.jpg");
              QIFileFunctions::Copy(src, dst); // "send" the file to yourself
              RString server = serverDir + RString("face.jpg");
              if (localServer) QIFileFunctions::Copy(src, server);
              else TransferFileToServer(server, src);
            }
          }
        }

        // send sounds
        const AutoArray<RString> &sounds = GWorld->UI()->GetCustomRadio();
        if (sounds.Size() > 0)
        {
          RString srcDir = GetUserDirectory() + RString("sound\\");
          RString dstDir = GetClientCustomFilesDir() + RString("\\players\\") + EncodeFileName(identity.name) + RString("\\sound\\");
          CreatePath(dstDir);
          RString serverDir;
          if (localServer)
          {
            serverDir = GetServerTmpDir() + RString("\\players\\") + EncodeFileName(identity.name) + RString("\\sound\\");
            CreatePath(serverDir);
          }
          else
          {
            // char '#' is used as an indication of Local Settings directory + ServerTmpDir (different on server than on client)
            serverDir = RString("#") + RString("\\players\\") + EncodeFileName(identity.name) + RString("\\sound\\");
          }
          for (int i=0; i<sounds.Size(); i++)
          {
            RString src = srcDir + sounds[i];
            RString dst = dstDir + sounds[i];
            // check file size
            // do not copy file if it is too large
            if (FileSize(src)<MaxCustomSoundSize && ValidDataFile(src))
            {
              QIFileFunctions::Copy(src, dst); // "send" the file to yourself
              RString server = serverDir + sounds[i];
              // send the file to server
              if (localServer) QIFileFunctions::Copy(src, server);
              else TransferFileToServer(server, src);
            }
          }
        }
#endif

        SendMsg(&identity, NMFGuaranteed);
      }
      break;
    case NMTRemoteMuteList:
#if _ENABLE_MP
      TMSGCHECK(_remoteMuteList.TransferMsg(ctx))
#endif
      break;
    case NMTLogin:
      {
        int index = _identities.Add();
        PlayerIdentity &identity = _identities[index];
        TMError err = identity.TransferMsg(ctx); 
        if (err != TMOK && err != TMNotFound) 
        {
          _identities.Delete(index,1);
          break;
        }
//        if (identity.dpnid == BOT_CLIENT && _client) _client->GetDistantAddress(identity.inaddr);
#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER >= 200
        // register the player in the session
        _parent->RegisterPlayer(identity);

        const XUSER_SIGNIN_INFO *signinInfo = GSaveSystem.GetUserInfo();
        // if the user is not local
        if (signinInfo && identity.xuid != signinInfo->xuid)
        {
          // register all users as remote talkers
          for (int userIndex = 0; userIndex < identity.localPlayerInfoList.localPlayerInfoList.Size(); ++userIndex)
          {
            const LocalPlayerInfo &info = identity.localPlayerInfoList.localPlayerInfoList[userIndex];
            if (info.signedToLive && info.xuid != INVALID_XUID)
            {
              void VoiceRegisterRemoteTalker(XUID xuid);
              VoiceRegisterRemoteTalker(info.xuid);
            }
          }
        }        

        // set the _voiceAddress
        if (_client)
        {
          identity._voiceAddress.sin_port = htons(_client->GetVoicePort());
          identity._voiceAddress.sin_family = AF_INET;
          INT err = XNetXnAddrToInAddr(&identity.xnaddr, _parent->GetSessionKey(), &identity._voiceAddress.sin_addr);
          if (err != 0)
          {
            RptF("Client: XNetXnAddrToInAddr failed with error 0x%x", err);
          }
        }
# else
        INT err = XNetXnAddrToInAddr(&identity.xnaddr, &_sessionKid, &identity.inaddr);
        if (err != 0)
        {
          RptF("Client: XNetXnAddrToInAddr failed with error 0x%x", err);
        }
# endif // _XBOX_VER >= 200
#endif
        if (identity.dpnid == _player)
        {
          // store playerId
          _playerID =  identity.playerid;
          SetCreatorId(CreatorId(identity.playerid));
          // login confirmed
          SetClientState(NCSLoggedIn);  
#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
          // voiceAddress was deduced from the xnaddr
          
          // if needed, update the mute list
          if (_identities.Size() > 1) UpdateMuteList();
#else
          // send info needed to establish peer to peer connection
          in_addr local;
          int port;
          if (_client && _client->GetLocalAddress(local,port))
          {
            sockaddr_in vaddr;
            vaddr.sin_port = htons(port);
            vaddr.sin_family = AF_INET;
            vaddr.sin_addr.s_addr = local.s_addr;
            memset(&(vaddr.sin_zero), '\0', 8); // zero the rest of the struct

            // fill my own address so that HEAR_YOURSELF works. If necessary, ConnectVoice can overwrite it later
            identity._voiceAddress = vaddr;

            // ask the server to connect us with other players
            AskConnectVoiceMessage ask;
            ask._privateAddr = local.s_addr; //S_un.S_addr;
            ask._verNo = BUILD_NO;
#if _ENABLE_GAMESPY
            if (_publicAddr != 0)
            {
              ask._publicAddr = _publicAddr;
            }
            else
#endif
            {
              ask._publicAddr = ask._privateAddr;
#             if BETA_TESTERS_DEBUG_VON && _ENABLE_GAMESPY
              RptF("VoNLog,Client: Warning! publicAddr was zero while Login. It was set to privateAddr.");
#             endif
            }
            ask._voicePort = port;
#ifdef _WIN32
            LogF("VoNLog: Client sent AskConnectVoiceMessage %d.%d.%d.%d:%d to server",local.S_un.S_un_b.s_b1,local.S_un.S_un_b.s_b2,local.S_un.S_un_b.s_b3,local.S_un.S_un_b.s_b4,port);
#endif
            SendMsg(&ask, NMFGuaranteed);
          }
#endif
        }
        else
        {
          DecFadeSemaphore(FadePlayerConnected);
#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
          // check if a new player is not muted, update mute list if so
          int userIndex = GSaveSystem.GetUserIndex();
          if (userIndex >= 0)
          {
            BOOL muted = false;
            if (SUCCEEDED(XUserMuteListQuery(userIndex, identity.xuid, &muted)) && muted)
            {
              // add to the list
              _muteList._list.Add(identity.xuid);
              // send the updated list to the server
              if (_clientState >= NCSConnected) SendMsg(&_muteList, NMFGuaranteed);
            }
          }
#endif // _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
        }
        // identity.state = NGSLogin;
        // find squad info
        if (identity.squadId.GetLength() > 0)
        {
          for (int i=0; i<_squads.Size(); i++)
            if (_squads[i]->_id == identity.squadId)
            {
              identity.squad = _squads[i];
              break;
            }
        }
      }
      break;
    case NMTLogout:
      {
        LogoutMessage logout;
        TMSGCHECK(logout.TransferMsg(ctx))
        for (int i=0; i<_identities.Size(); i++)
        {
          PlayerIdentity &identity = _identities[i];
          if (identity.dpnid == logout._dpnid)
          {
#if _XBOX_SECURE && _ENABLE_MP
            AddRecentPlayer(identity);
# if _XBOX_VER >= 200

            const XUSER_SIGNIN_INFO *signinInfo = GSaveSystem.GetUserInfo();
            // if the user is not local
            if (signinInfo && identity.xuid != signinInfo->xuid)
            {
              // unregister all users as remote talkers
              for (int userIndex = 0; userIndex < identity.localPlayerInfoList.localPlayerInfoList.Size(); ++userIndex)
              {
                const LocalPlayerInfo &info = identity.localPlayerInfoList.localPlayerInfoList[userIndex];
                if (info.signedToLive && info.xuid != INVALID_XUID)
                {
                  void VoiceUnregisterRemoteTalker(XUID xuid);
                  VoiceUnregisterRemoteTalker(info.xuid);
                }
              }
            }        

            // unregister the player from the session
            _parent->UnregisterPlayer(identity);
# endif
#endif
            _identities.Delete(i);
            break;
          }
        }
      }
      break;
    case NMTSquad:
      {
        PREPARE_TRANSFER(Squad)
          bool addIt = true;
        for (int i=0; i<_squads.Size(); i++)
        {
          if (_squads[i]->_id == message->_id)
          {
            addIt = false;
            break;
          }
        }
        if (addIt)
        {
          int index = _squads.Add(new SquadIdentity());
          SquadIdentity *squad = _squads[index];
          TMError err = squad->TransferMsg(ctx); 
          if (err != TMOK && err != TMNotFound) 
          {
            _squads.DeleteAt(index);
            break;
          }
        }
      }
      break;
    case NMTCustomFileList:
      {
        CustomFileListMessage fileListMsg;
        TMSGCHECK(fileListMsg.TransferMsg(ctx))
        CustomFilesWantedMessage customFilesWantedMsg;
        customFilesWantedMsg._player = fileListMsg._player;
        GetCustomFilesWanted(customFilesWantedMsg, fileListMsg._fileList);
        DeleteObsoleteFiles(fileListMsg._name, fileListMsg._fileList, true);
        SendMsg(&customFilesWantedMsg, NMFGuaranteed);
      }
      break;
    case NMTDeleteCustomFiles:
      {
        DeleteCustomFilesMessage msg;
        TMSGCHECK(msg.TransferMsg(ctx))
        for (int i=0; i<msg._list.Size(); i++)
        {
          if (!msg._list[i].IsEmpty())
          {
            RString dir = GetClientPath(RString("$\\players\\") + EncodeFileName(msg._list[i]));
            RString path = GetClientPath(dir);
            if (path.GetLength()>8) DeleteDirectoryStructure(path, true); // only to be more safe
          }
        }
      }
      break;
    case NMTMissionHeader:
      {
        TMSGCHECK(_missionHeader.TransferMsg(ctx))
        _missionHeader._playerRespawnTime = _missionHeader._respawnDelay;

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
        // set the properties and contexts for the rich presence
        // - MPType
        int gameType = 0;
        if (_missionHeader._gameType.GetLength() > 0)
        {
          ConstParamEntryPtr cls = (Pars >> "CfgMPGameTypes").FindEntry(_missionHeader._gameType);
          if (cls) gameType = *cls >> "id";
        }
        GSaveSystem.SetContext(CONTEXT_GAME_MPTYPE, gameType);
        // - World
        int worldId = 0;
        if (_missionHeader._island.GetLength() > 0)
        {
          ConstParamEntryPtr cls = (Pars >> "CfgWorlds").FindEntry(_missionHeader._island);
          if (cls) worldId = *cls >> "worldId";
        }
        GSaveSystem.SetContext(CONTEXT_GAME_WORLD, worldId);
        // - Difficulty
        int difficulty = Glob.config.diffNames.GetValue(_missionHeader._diffName);
        if (difficulty < 0) difficulty = Glob.config.diffDefault;
        GSaveSystem.SetContext(CONTEXT_GAME_DIFFICULTY, difficulty);
        // - MPMission
        int missionId = 0;
        // TODOXNET: mission identification
        GSaveSystem.SetContext(CONTEXT_GAME_MPMISSION, missionId);
#endif

#ifndef _XBOX
        _missionHeader._fileDir = GetClientPath(_missionHeader._fileDir);
#endif
        if (_missionHeader._updateOnly) break;
        {
          // ADDED - AddOns check
          void CheckPatch(FindArrayRStringCI &addOns, FindArrayRStringCI &missing);
          FindArrayRStringCI missing;
          CheckPatch(_missionHeader._addOns, missing);
#if !_VBS2  // disable MP missing addon warning messages (temp)
          if (missing.Size() > 0)
          {
            RString ReportMissingAddons(FindArrayRStringCI missing);
            RString message = ReportMissingAddons(missing);
            Disconnect(message);
            return;
          }
#endif
          GWorld->ActivateAddons(_missionHeader._addOns,_missionHeader._island);
        }

        Glob.config.difficulty = Glob.config.diffDefault;
        {
          int difficulty = Glob.config.diffNames.GetValue(_missionHeader._diffName);
          if (difficulty >= 0) Glob.config.difficulty = difficulty;
          else
          {
            RptF("Unknown difficulty: %s", (const char *)_missionHeader._diffName);
          }
        }
        _playerRoles.Resize(0);
        
#if _ENABLE_UNSIGNED_MISSIONS
        // check if mission file is valid
        if (_missionHeader._noCopy)
        {
          // if list of addons is OK, we already have the correct mission
          _missionFileValid = true;
        }
        else
        {
          NetworkServer *server = _parent->GetServer();
          if (server)
          {
            // copy file
            RString src = server->GetMissionFilename();
            RString dst = _missionHeader._fileDir + _missionHeader._fileName + RString(".pbo");
            CreatePath(dst);
            QIFileFunctions::Copy(src, dst);
            _missionFileValid = true;
          }
          else
          {
            RString fullname = _missionHeader._fileDir + _missionHeader._fileName;
            _missionFileValid = CheckMissionFile(fullname, _missionHeader);

            if (!_missionFileValid)
            {
              // check cache
#ifdef _XBOX
              WarningMessage("Server has different version of mission %s", (const char *)_missionHeader.GetLocalizedMissionName());
#else
              RString fullname = GetMissionCacheDir() + _missionHeader._fileName;
              _missionFileValid = CheckMissionFile(fullname, _missionHeader);
#endif
            }
          }
        }
#else
        _missionFileValid = true;
#endif // _ENABLE_UNSIGNED_MISSIONS

        SetClientState(NCSMissionSelected);
      }
      break;
    case NMTPlayerRole:
      {
        PREPARE_TRANSFER(PlayerRole)
        
        int index;
        TRANSF_BASE(index, index);
        if (index >= _playerRoles.Size())
          _playerRoles.Resize(index + 1);
        TMSGCHECK(_playerRoles[index].TransferMsg(ctx))
      }
      break;
    case NMTPlayerRoleUpdate:
      {
        PlayerRoleUpdate update;
        TMSGCHECK(update.TransferMsg(ctx))
        if (update._index>=0 && update._index < _playerRoles.Size() )
        {
          PlayerRole &role = _playerRoles[update._index];
          role.player = update._player;
          role.lifeState = (LifeState)update._lifeState;
        }
      }
      break;
    case NMTSelectPlayer:
      {
        SelectPlayerMessage pl;
        TMSGCHECK(pl.TransferMsg(ctx))
        Assert(pl._player == _player);
        NetworkId nid(pl._creator, pl._id);
        NetworkObject *object = GetObject(nid);
        Person *person = dynamic_cast<Person *>(object);
        ProcessSelectPlayer(person, pl._respawn, pl._position);
      }
      break;
    case NMTTransferFile:
      {
        TransferFileMessage transfer;
        TMSGCHECK(transfer.TransferMsg(ctx))
#ifdef _XBOX
        if (stricmp(transfer._path, ".") != 0)
        {
          char buffer[256];
          const char *FullXBoxName(const char *name, char *temp);
          transfer._path = FullXBoxName(transfer._path, buffer);
        }
#else
        transfer._path = GetClientPath(transfer._path);
#endif
        ReceiveFileSegment(transfer);
      }
      break;
#if _ENABLE_UNSIGNED_MISSIONS
    case NMTTransferMissionFile:
      {
        TransferMissionFileMessage transfer;
        TMSGCHECK(transfer.TransferMsg(ctx))
#ifdef _XBOX
        char buffer[256];
        const char *FullXBoxName(const char *name, char *temp);
        transfer._path = FullXBoxName(transfer._path, buffer);
#else
        transfer._path = GetClientPath(transfer._path);
#endif

        const char *prefix = "mpmissions\\__cur_mp.";
        GFileBanks.Remove(prefix);

        int ret = ReceiveFileSegment(transfer);
        if (ret > 0)
        {
// FIX: transfer mission file always into tmp directory (do not rewrite original file)
//          CreateMPMissionBank(_missionHeader.fileDir + _missionHeader.fileName, _missionHeader.island);
          const char *ptr = transfer._path;
          const char *ext = strrchr(ptr, '.');
          DoAssert(ext);
          DoAssert(stricmp(ext, ".pbo") == 0);
          RString path = transfer._path.Substring(0, ext - ptr);
          CreateMPMissionBank(path, _missionHeader._island);

          DoAssert(!_missionFileValid);
          _missionFileValid = true;
          // TODO: this is not bot client
          DoAssert(!_parent->IsServer());
/*
          DoAssert(GetClientState() == NCSRoleAssigned);
          if (GetClientState() == NCSRoleAssigned && PrepareGame())
            SetClientState(NCSMissionReceived);
*/
        }
        else if (ret<0)
        {
          RString format = LocalizeString(IDS_MP_VALIDERROR_2);
          RString message;
          const PlayerIdentity *id = FindIdentity(_player);
          if (id) message = Format(format, cc_cast(id->name)) + Format(" - %s", cc_cast(transfer._path));
          Disconnect(message);
        }
      }
      break;
#endif // _ENABLE_UNSIGNED_MISSIONS
    case NMTAskForDamage:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForDamageMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._who)
        {
          Object::DoDamageResult result;
          ask._who->DoDamage(
            result, ask._modelPos, ask._val, ask._valRange, ask._ammo, ask._originDir,false
          );
          // even if object is not local, we update it only locally
          // remote distribution was already done
          ask._who->ApplyDoDamageLocalHandleHitBy(result, ask._owner, ask._ammo);
        }
      }
      break;
    case NMTAskForEnableVMode:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForEnableVModeMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle) ask._vehicle->EnableVisionModes(ask._enable);
      }
      break;
    case NMTAskForAllowCrewInImmobile:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForAllowCrewInImmobileMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle) ask._vehicle->SetAllowCrewInImmobile(ask._enable);
      }
      break;
    case NMTAskForForceGunLight:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForForceGunLightMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle) ask._vehicle->ForceGunLight(ask._force);
      }
      break;
    case NMTAskForPilotLight:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForPilotLightMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle) ask._vehicle->SetPilotLight(ask._enable);
      }
      break;
    case NMTAskForIRLaser:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForIRLaserMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle) ask._vehicle->EnableIRLaser(ask._enable);
      }
      break;
    case NMTAskForSetDamage:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForSetDamageMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._who)
          ask._who->SetDamage(ask._dammage);
      }
      break;
    case NMTAskForSetMaxHitZoneDamage:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForSetDamageMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._who)
          ask._who->SetMaxHitZoneDamage(ask._dammage);
      }
      break;
    case NMTAskForApplyDoDamage:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForApplyDoDamageMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._who)
        {
          Object::DoDamageResult result;
          result.damage = ask._damage;
          result.hits = ask._hits;
          ask._who->ApplyDoDamageLocalHandleHitBy(result,ask._owner,ask._ammo);
          // TODO:
        }
      }
      break;
    case NMTAskForAddImpulse:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForAddImpulseMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle)
          ask._vehicle->AddImpulse(ask._force, ask._torque);
      }
      break;
    case NMTAskForMoveVector:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForMoveVectorMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle)
        {
          ask._vehicle->ResetRemoteState();

#if _VBS3 // handle clipLandKeep objects
          if (ask._vehicle->Static())
          {
            LODShape *shape = ask._vehicle->GetShape();
            Entity *objT = dyn_cast<Entity>((Object *)ask._vehicle);
            if (
              shape && (shape->GetOrHints()&(ClipLandKeep|ClipLandOn)
              || shape->PropertyValue(RSB(placement))==RSB(slope)
              || shape->PropertyValue(RSB(placement))==RSB(vertical)
              || (objT && objT->Type()->_placement==PlacementVertical))
            )
            {
              Matrix4 trans = ask._vehicle->Transform();
              trans.SetPosition(ask._pos);
              trans.SetUpAndDirection(VUp,trans.Direction());
              ask._vehicle->InitSkew(GLandscape,trans);
              ask._vehicle->Move(trans);
              break;
            }
          }
#endif
          ask._vehicle->Move(ask._pos);
        }
      }
      break;
    case NMTAskForMoveMatrix:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForMoveMatrixMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle)
        {
          ask._vehicle->ResetRemoteState();
          Matrix4 trans;
          trans.SetPosition(ask._pos);
          trans.SetOrientation(ask._orient);
#if _VBS3 // handle clipLandKeep objects
          if (ask._vehicle->Static())
          {
            LODShape *shape = ask._vehicle->GetShape();
            Entity *objT = dyn_cast<Entity>((Object *)ask._vehicle);
            if (
              shape && (shape->GetOrHints()&(ClipLandKeep|ClipLandOn)
              || shape->PropertyValue(RSB(placement))==RSB(slope)
              || shape->PropertyValue(RSB(placement))==RSB(vertical)
              || (objT && objT->Type()->_placement==PlacementVertical))
            )
            {
              trans.SetUpAndDirection(VUp,trans.Direction());
              ask._vehicle->InitSkew(GLandscape,trans);
            }
          }
#endif
          ask._vehicle->Move(trans);
        }
      }
      break;
    case NMTAskForJoinGroup:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForJoinGroupMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._group && ask._join) //crash hotfix
        {
          void ProcessJoinGroups(AIGroup *from, AIGroup *to, bool silent);
          ProcessJoinGroups(ask._group, ask._join, ask._silent);
        }
      }
      break;
    case NMTAskForJoinUnits:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForJoinUnitsMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        void ProcessJoinGroups(OLinkPermNOArray(AIUnit) &units, AIGroup *grp, bool silent, int forceId);
        ProcessJoinGroups(ask._units, ask._join, ask._silent, ask._id);
      }
      break;
    case NMTAskForChangeSide:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForChangeSideMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if(ask._vehicle) ask._vehicle->SetTargetSide((TargetSide)ask._side);
      }
      break;
    case NMTAskForHideBody:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForHideBodyMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle)
        {
          ask._vehicle->HideBody();
        }
      }
      break;
    case NMTExplosionDamageEffects:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        ExplosionDamageEffectsMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        Ref<AmmoType> ammo;
        if (ask._type.GetLength() > 0)
        {
          Ref<EntityType> type = VehicleTypes.New(ask._type);
          ammo = dynamic_cast<AmmoType *>(type.GetRef());
          if (ammo)
          {
            HitInfo hitInfo(ammo, ask._pos, ask._surfNormal, ask._inSpeed, ask._outSpeed);
            GLandscape->ExplosionDamageEffects(
              ask._owner, ask._directHit, hitInfo, ask._componentIndex,
              ask._enemyDamage, ask._energyFactor, ask._explosionFactor
            );
          #if _EXT_CTRL //ext. Controller like HLA, AAR
            if(IsBotClient())
            {
              // WIP:EXTEVARS:FIXME:EXTERNAL pass proper parameters
              _hla.ExplosionDamageEffects(ask._owner, ask._directHit, ask._pos
                , ask._dir, hitInfo, ask._enemyDamage, ask._energyFactor, ask._explosionFactor
              );
            }
          #endif
          }
        }
      }
      break;
    case NMTAskForGetIn:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForGetInMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle && ask._soldier)
        {
          bool ok = false;
          switch (ask._position)
          {
          case GIPCommander:
            if (ask._vehicle->Observer() && !ask._vehicle->Observer()->IsDamageDestroyed()) break;
            ask._vehicle->GetInCommander(ask._soldier);
            ok = true;
            break;
          case GIPDriver:
            if (ask._vehicle->Driver() && !ask._vehicle->Driver()->IsDamageDestroyed()) break;
            ask._vehicle->GetInDriver(ask._soldier);
            ok = true;
            break;
          case GIPGunner:
            if (ask._vehicle->Gunner() && !ask._vehicle->Gunner()->IsDamageDestroyed()) break;
            ask._vehicle->GetInGunner(ask._soldier);
            ok = true;
            break;
          case GIPTurret:
            if (ask._turret)
            {
              if (ask._turret->Gunner() && !ask._turret->Gunner()->IsDamageDestroyed()) break;
              ask._vehicle->GetInTurret(ask._turret, ask._soldier);
              ok = true;
            }
            break;
          case GIPCargo:
            {
              if(ask._cargoIndex >= 0)
              {
                if(ask._cargoIndex >= ask._vehicle->GetManCargo().Size())
                  RptF("Client: CargoIndex to high:%d max:%d", ask._position, ask._vehicle->GetManCargo().Size());
                else
                  if (ask._vehicle->GetManCargo()[ask._cargoIndex] == NULL || ask._vehicle->GetManCargo()[ask._cargoIndex]->IsDamageDestroyed())
                    ok = true;
              }
              else
                for (int i=0; i<ask._vehicle->GetManCargo().Size(); i++)
                {
                  if (ask._vehicle->GetManCargo()[i] == NULL || ask._vehicle->GetManCargo()[i]->IsDamageDestroyed())
                  {
                    ok = true;
                    break;
                  }
                }
            }
            if (ok) 
              ask._vehicle->GetInCargo(ask._soldier, true, ask._cargoIndex);
#if _VBS3
            else
            {
              if(ask._vehicle && ask._soldier)
              {
                int newCargoIndex = ask._vehicle->QCanIGetInCargo(ask._soldier,-1);
                if(newCargoIndex >= 0 && newCargoIndex <  ask._vehicle->GetManCargo().Size())
                  ask._vehicle->GetInCargo(ask._soldier, true, ask._cargoIndex);
              }
            }
#endif
            break;
          default:
            RptF("Client: Unknown get in position %d", ask._position);
            break;
          }
          if (ok && ask._soldier && ask._soldier->Brain())
          {
            ask._vehicle->GetInFinished(ask._soldier->Brain());
            if (GLOB_WORLD->FocusOn() == ask._soldier->Brain())
              GLOB_WORLD->SwitchCameraTo(ask._vehicle, GLOB_WORLD->GetCameraType(), false);
          }
        }
      }
      break;
    case NMTAskForGetOut:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForGetOutMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle && ask._soldier && ask._soldier->Brain())
        {
          /*
          LogF
          (
            "%s: get out from %s to %s",
            (const char *)ask.soldier->GetDebugName(),
            (const char *)ask.vehicle->GetDebugName(),
            ask.parachute ? "Parachute" : "nothing"
          );
          */
          ask._soldier->Brain()->DoGetOut(ask._vehicle,ask._eject,ask._parachute);
        }
      }
      break;
    case NMTAskWaitForGetOut:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskWaitForGetOutMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle && ask._unit)
        {
          ask._vehicle->WaitForGetOut(ask._unit);
        }
      }
      break;
    case NMTAskRemoteControlled:
      {
        AskRemoteControlledMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        AIBrain *whom = ask._whom;
        if ( whom->IsLocal() )
        {
          whom->SetRemoteControlled(ask._who);
        }
      }
      break;
    case NMTAskForChangePosition:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
#if DIAG_CREW
        LogF("### Ask for change position message arrived");
#endif
        AskForChangePositionMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle && ask._soldier)
        {
          if (ask._type == ATMoveToTurret)
          {
            if (ask._turret)
            {
              Ref<Action> action = new ActionTurret(ATMoveToTurret, ask._vehicle, ask._turret);
              ask._vehicle->ChangePosition(action, ask._soldier);
            }
          }
          else if (ask._type == ATMoveToCargo)
          {
            if (ask._cargoIndex >= 0 && ask._cargoIndex < ask._vehicle->GetManCargo().Size())
            {
              Ref<Action> action = new ActionIndex(ATMoveToCargo, ask._vehicle, ask._cargoIndex);
              ask._vehicle->ChangePosition(action, ask._soldier);
            }
          }
          else
          {
            Ref<Action> action = new ActionBasic((UIActionType)ask._type, ask._vehicle);
            ask._vehicle->ChangePosition(action, ask._soldier);
          }
        }
      }
      break;
    case NMTAskForSelectWeapon:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForSelectWeaponMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._vehicle)
        {
          if (ask._turret)
          {
            ask._turret->GetWeapons().SelectWeapon(ask._vehicle, ask._weapon);
          }
          else
          {
            ask._vehicle->GetWeapons().SelectWeapon(ask._vehicle, ask._weapon);
          }
        }
      }
      break;
    case NMTAskForAmmo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForAmmoMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (!ask._vehicle) break;
        WeaponsState &weapons = ask._turret ? ask._turret->GetWeapons() : ask._vehicle->GetWeapons();
        Magazine *state = weapons._magazineSlots[ask._weapon]._magazine;
        if (!state) break;
        saturateMin(ask._burst, state->GetAmmo());
        ADD_ENCRYPTED(state->_ammo,state->_ammoSupport, -ask._burst);
        if (state->GetAmmo() < 0) state->SetAmmo(0);
      }
      break;
    case NMTAskForFireWeapon:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AskForFireWeaponMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (!ask._vehicle) break;
        TurretContextV context;
        if (ask._vehicle->FindTurret(unconst_cast(ask._vehicle->FutureVisualState()), ask._turret, context))
        {
          ask._vehicle->FireWeapon(context, ask._weapon, ask._target, ask._forceLock);
        }
      }
      break;
    case NMTFireWeapon:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        FireWeaponMessage fire;
        TMSGCHECK(fire.TransferMsg(ctx))
        EntityAIFull *veh = fire._vehicle;
        TurretContext context;
        if (veh && veh->FindTurret(fire._gunner, context))
        {
          Magazine *magazine = veh->FindMagazine(fire._magazineCreator, fire._magazineId);
          RemoteFireWeaponInfo remoteInfo;
          remoteInfo._position = fire._pos;
          remoteInfo._direction = fire._dir;
          remoteInfo._visible = fire._visible;
#if _VBS3
          remoteInfo._projectile = fire._projectile;
#endif
          if (veh->FireWeaponEffects(context, fire._weapon, magazine, fire._target,&remoteInfo))
          {
            const MagazineSlot &slot = context._weapons->_magazineSlots[fire._weapon];
            veh->RemoveAmmoAfterShot(slot._weaponMode,magazine);
          }

#if _VBS3
          // set the local client who is shooting the laserTarget.
          LaserTarget *lstg = dyn_cast<LaserTarget>(fire._projectile.GetLink());
          if(lstg)
            lstg->SetOwner(veh);
#endif

#if _EXT_CTRL //ext. Controller like HLA, AAR
          if(IsBotClient())
            _hla.FireWeapon(veh, fire._gunner, fire._weapon, magazine, fire._target, remoteInfo);
#endif
        }
      }
      break;
    case NMTUpdateWeapons:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        UpdateWeaponsMessage update;
        TMSGCHECK(update.TransferMsg(ctx))
      }
      break;
    case NMTAddWeaponCargo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AddWeaponCargoMessage update;
        TMSGCHECK(update.TransferMsg(ctx))
        if (update._vehicle)
        {
          Ref<WeaponType> weapon = WeaponTypes.New(update._weapon);
          update._vehicle->AddWeaponCargo(weapon, 1, true);
        }
      }
      break;
    case NMTRemoveWeaponCargo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        RemoveWeaponCargoMessage update;
        TMSGCHECK(update.TransferMsg(ctx))
        if (update._vehicle)
        {
          Ref<WeaponType> weapon = WeaponTypes.New(update._weapon);
          update._vehicle->RemoveWeaponCargo(weapon);
        }
      }
      break;
    case NMTAddMagazineCargo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AddMagazineCargoMessage update;
        TMSGCHECK(update.TransferMsg(ctx))
        if (update._vehicle && update._magazine)
          update._vehicle->AddMagazineCargo(update._magazine, true);
      }
      break;
    case NMTRemoveMagazineCargo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        RemoveMagazineCargoMessage update;
        TMSGCHECK(update.TransferMsg(ctx))
        if (update._vehicle)
          update._vehicle->RemoveMagazineCargo(update._type, update._ammo);
      }
      break;
    case NMTAddBackpackCargo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AddBackpackCargoMessage update;
        TMSGCHECK(update.TransferMsg(ctx))
        if (update._vehicle && update._backpack)
          update._vehicle->AddBackpackCargo(update._backpack);
      }
      break;
    case NMTRemoveBackpackCargo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        RemoveBackpackCargoMessage update;
        TMSGCHECK(update.TransferMsg(ctx))
        if (update._vehicle)
          update._vehicle->RemoveBackpackCargo(update._backpack);
      }
      break;
    case NMTClearWeaponCargo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        ClearWeaponCargoMessage update;
        TMSGCHECK(update.TransferMsg(ctx))
        if (update._vehicle)
          update._vehicle->ClearWeaponCargo(true);
      }
      break;
    case NMTClearMagazineCargo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        ClearMagazineCargoMessage update;
        TMSGCHECK(update.TransferMsg(ctx))
        if (update._vehicle)
          update._vehicle->ClearMagazineCargo(true);
      }
      break;
    case NMTClearBackpackCargo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        ClearBackpackCargoMessage update;
        TMSGCHECK(update.TransferMsg(ctx))
        if (update._vehicle)
          update._vehicle->ClearBackpackCargo(true);
      }
      break;
    case NMTVehicleInit:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        
        VehicleInitMessage init;
        TMSGCHECK(init.TransferMsg(ctx))

        // postpone execution, to be sure we do not execute before init events
        if (init._vehicle)
        {
          const EntityType *type = init._vehicle->GetEntityType();
          RString typeName = type ? type->GetName() : RString();
          init._vehicle->OnVehicleInit(init._init);
        }
/*
LogF
(
  "Vehicle %s (0x%x)",
  init.vehicle ? (const char *)init.vehicle->GetDebugName() : "<null>"
);
LogF
(
  " - command %s", (const char *)init.init
);
LogF("");
*/
      }
      break;
    case NMTVehicleDestroyed:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        VehicleDestroyedMessage info;
        TMSGCHECK(info.TransferMsg(ctx))

        GStats.OnVehicleDestroyed(info._killed, info._killer);

#if _EXT_CTRL //ext. Controller like HLA, AAR
        if(IsBotClient())
          _hla.VehicleDestroyed(info._killed,info._killer);
#endif

        // FIX - add experience
        if (info._killer && info._killed)
        {
          // use Entity member to get original target side
          // all dead bodies are considered civilian
          TargetSide origSide = info._killed->Entity::GetTargetSide();
          const VehicleType &type = *info._killed->GetType();

          // increase killer's experience
          AIBrain *kBrain = info._killer->CommanderUnit();
          if (kBrain && kBrain->IsLocal())
          {
            kBrain->IncreaseExperience(type, origSide);
            // send radio message
            if (kBrain->IsEnemy(origSide))
            {
              // find corresponding target
              Target *tar = kBrain->FindTargetAll(info._killed);
              if (tar)
              {
                // mark killer
                // when destroyed will be set, it will be marked for reporting
                tar->MarkKiller(info._killer);
              }
            }
          }
          if
          (
            info._killer->GunnerUnit() &&
            info._killer->GunnerUnit()->IsLocal() &&
            info._killer->GunnerUnit() != kBrain
          )
            info._killer->GunnerUnit()->IncreaseExperience(type, origSide);
          if
          (
            info._killer->PilotUnit() &&
            info._killer->PilotUnit()->IsLocal() &&
            info._killer->PilotUnit() != kBrain
          )
            info._killer->PilotUnit()->IncreaseExperience(type, origSide);
        }
      }
      break;
    case NMTVehicleDamaged:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
#if _ENABLE_VBS
        VehicleDamagedMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        if (info._damaged) GStats.OnVehicleDamaged(info._damaged, info._killer, info._damage, info._ammo);

        #if _EXT_CTRL //ext. Controller like HLA, AAR
        if(IsBotClient())
          _hla.VehicleDamaged(info._damaged,info._killer, info._damage,info._ammo);
        #endif
#endif
      }
      break;
    case NMTIncomingMissile:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        IncomingMissileMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        if(info._shot)
        {
          Entity *eshot = info._shot;
          Shot *shot = dyn_cast<Shot>(eshot);
          if (info._target) info._target->OnIncomingMissile(info._target, shot, info._owner);
        }
      }
      break;
    case NMTLaunchedCounterMeasures:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        LaunchedCounterMeasuresMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        if(info._testedSystem && info._testedSystem->IsLocal())
        {
          Entity *testedSystem = info._testedSystem;
          Entity *cshot = info._cm;
          Missile *missile = dyn_cast<Missile>(testedSystem);
          Shot *cm = dyn_cast<Shot>(cshot);
          if(missile) 
            missile->TestCounterMeasures(cm, info._count);
          else
          {
            EntityAIFull *entity = dyn_cast<EntityAIFull>(testedSystem);
            if(entity && entity->CommanderUnit() && entity->CommanderUnit()->GetPerson())
            {
              TurretContext context;
              bool valid = entity->FindTurret(entity->CommanderUnit()->GetPerson(),context);
              if(valid && cm) context._weapons->TestCounterMeasures(cm, info._count);
            }
          }
        }
      }
      break;
    case NMTWeaponLocked:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        WeaponLockedMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        if(info._target && info._target->CommanderUnit() && info._target->CommanderUnit()->GetVehicleIn())
          info._target->CommanderUnit()->GetVehicleIn()->OnWeaponLock(info._target, info._gunner, info._locked);
      }
      break;
    case NMTMarkerCreate:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        MarkerCreateMessage marker;
        TMSGCHECK(marker.TransferMsg(ctx))
#if _EXT_CTRL //ext. Controller like HLA, AAR
        if(IsBotClient()) _hla.CreateMarkerMsg(&marker._marker);
#endif
        RString name = marker._marker.name;
        int index = -1;
        for (int i=0; i<markersMap.Size(); i++)
        {
          if (stricmp(markersMap[i].name, name) == 0)
          {
            index = i;
            break;
          }
        }
#if _VBS3 // marker EH
        bool updated = index > -1;
#endif
        if (index < 0) index = markersMap.Add();
        markersMap[index] = marker._marker;
#if _VBS3 // marker EH
        void ProcessCreateMarkerEH(RString name, bool updated = true);
        ProcessCreateMarkerEH(name,updated);
#endif
      }
      break;
    case NMTMarkerDelete:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        MarkerDeleteMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
#if _EXT_CTRL //ext. Controller like HLA, AAR
        if(IsBotClient()) _hla.DeleteMarkerMsg(info._name);
#endif
        for (int i=0; i<markersMap.Size(); i++)
        {
          ArcadeMarkerInfo &marker = markersMap[i];
          if (marker.name == info._name)
          {
#if _VBS3 // marker EH
            void ProcessDeleteMarkerEH(RString name);
            ProcessDeleteMarkerEH(info._name);
#endif
            markersMap.Delete(i);
            break;
          }
        }
      }
      break;
    case NMTWaypointCreate:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        WaypointCreateMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        AIGroup *group = info._group;
        if (group)
        {
          // the created waypoint should be at most the last one
          DoAssert(info._index <= group->NWaypoints());
          int index = -1;
          if (info._index == group->NWaypoints())
            index = group->AddWaypoint();
          else if (info._index < group->NWaypoints())
          {
            index = info._index;
            group->InsertWaypoint(index);
          }
          if (index >= 0)
          {
            // save the breakpoint
            WaypointInfo &wp = group->GetWaypoint(index);
            wp = info._waypoint;

            // reset the waypoint if necessary
            if (group->GetCurrent())
            {
              AIGroupContext context(group);
              context._task = group->GetCurrent()->_task;
              context._fsm = group->GetCurrent()->_fsm;
              void OnWaypointUpdated(AIGroupContext *context, int index);
              OnWaypointUpdated(&context, index);
            }
          }
        }
      }
      break;
    case NMTWaypointUpdate:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        WaypointUpdateMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        AIGroup *group = info._group;
        if (group && info._index >= 0 && info._index < group->NWaypoints())
        {
          // save the breakpoint
          WaypointInfo &wp = group->GetWaypoint(info._index);          
          wp = info._waypoint;

          // reset the waypoint if necessary
          if (group->GetCurrent())
          {
            AIGroupContext context(group);
            context._task = group->GetCurrent()->_task;
            context._fsm = group->GetCurrent()->_fsm;
            void OnWaypointUpdated(AIGroupContext *context, int index);
            OnWaypointUpdated(&context, info._index);
          }
        }
      }
      break;
    case NMTWaypointDelete:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        WaypointDeleteMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        if (info._group && info._index >= 0 && info._index < info._group->NWaypoints())
        {
          info._group->DeleteWaypoint(info._index);

          if (info._group->GetCurrent() && info._group->GetCurrent()->_fsm)
          {
            int &currentIndex = info._group->GetCurrent()->_fsm->Var(0);

            if (info._index < currentIndex)
            {
              // keep the processing of current waypoint
              currentIndex--;
            }
            else if (info._index == currentIndex)
            {
              // reset the new current waypoint
              AIGroupContext context(info._group);
              context._task = info._group->GetCurrent()->_task;
              context._fsm = info._group->GetCurrent()->_fsm;
              void ResetWaypoint(AIGroupContext *context);
              ResetWaypoint(&context);
            }
          }
        }
      }
      break;
    case NMTWaypointsCopy:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        WaypointsCopyMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        if (info._to && info._from)
        {
          info._to->CopyWaypoints(info._from);
        }
      }
      break;
    case NMTHCSetGroup:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        HCSetGroupMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        AIUnit *unit = info._unit;
        AIGroup *group = info._group;
        if (unit && group)
        {
          AIHCGroup hcGroup;
          hcGroup.SetGroup(group);
          hcGroup.SetTeam(info._team);
          hcGroup.SetName(info._name);

          unit->AddHCGroup(hcGroup);
        }
      }
      break;
    case NMTHCRemoveGroup:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        HCRemoveGroupMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        AIUnit *unit = info._unit;
        AIGroup *group = info._group;
        if (unit && group)
        {
           unit->RemoveHCGroup(group);
        }
      }
      break;
    case NMTHCClearGroups:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        HCClearGroupsMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        AIUnit *unit = info._unit;
        if (unit)
        {
          unit->ClearHCGroups();
        }
      }
      break;
    case NMTGroupSetUnconsciousLeader:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        GroupSetUnconsciousLeaderMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        AIUnit *unit = info._unit;
        AIGroup *group = info._group;
        if (group)
        {
          group->SetUnconsciousLeader(unit);
        }
      }
      break;
    case NMTGroupSelectLeader:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        GroupSelectLeaderMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        AIUnit *unit = info._unit;
        AIGroup *group = info._group;
        if (group && unit)
        {
          group->SelectLeader(unit);
        }
      }
      break;
    case NMTDropBackpack:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        DropBackpackMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        EntityAI *backpack = info._backpack;
        Matrix4 trans;
        trans.SetPosition(info._pos);
        trans.SetOrientation(info._orient);
        if (backpack)
        { 
          backpack->SetTransform(trans);
          backpack->Init(trans, false); // do not reset movement

          backpack->ResetMoveOut(); // mark it is in landscape
          GLOB_WORLD->AddSlowVehicle(backpack);
          GLOB_WORLD->RemoveOutVehicle(backpack);
                // we need other entities nearby to be notified of our presence
         backpack->OnMoved();
         GWorld->OnEntityMovedFar(backpack);
        }
      }
      break;
    case NMTTakeBackpack:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        TakeBackpackMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        Person *soldier = info._soldier; 
        EntityAI *backpack = info._backpack;
        if (backpack)
        {
          if(soldier->GetBackpack()) soldier->DropBackpack();
          backpack->MoveOut(soldier);
          soldier->SetBackpack(backpack);
        }
      }
      break;
    case NMTAssemble:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        AssembleMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        EntityAI *assembled = info._weapon;
        if(assembled)
        {
          Matrix4 transform;
          transform.SetOrientation(info._transform);
          transform.SetPosition(info._position);

          assembled->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
          assembled->SetTransform(transform);

          assembled->Init(transform, true); // do not reset movement
          assembled->OnEvent(EEInit);
          // if (_backpack->IsLocal()) 
          {
            assembled->ResetMoveOut(); // mark it is in landscape
            GLOB_WORLD->AddVehicle(assembled);
            GLOB_WORLD->RemoveOutVehicle(assembled);
            // we need other entities nearby to be notified of our presence
            assembled->OnMoved();
            GWorld->OnEntityMovedFar(assembled);
          }
        }
      }
      break;
    case NMTDisAssemble:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        DisAssembleMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        EntityAI *assembled = info._weapon;
        if (assembled && info._backpack)
        {
          assembled->UnlockPosition();
          if (assembled->IsInLandscape())
          {
            info._backpack->SetAseembleTo(assembled);
            assembled->SetMoveOut(info._backpack); // remove reference from the world    
          }
        }
      }
      break;
#if _ENABLE_VBS
    case NMTSetObjectTexture:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        SetObjectTextureMessage msg;
        TMSGCHECK(msg.TransferMsg(ctx))
        EntityAI* veh = msg._veh;   
        if (veh)
        {
          bool setTexture = true;
          if (msg._eval.GetLength() > 0)
          {
            GameState *state = GWorld->GetGameState();
            GameVarSpace vars(false);
            state->BeginContext(&vars);
            state->VarSetLocal("_this", GameValueExt(veh), true);
            setTexture = state->EvaluateMultiple(msg._eval, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
            state->EndContext();
          }
          if (setTexture)
          {
#if _VBS3
            if(stricmp(msg._name, "#reset") == 0)
              veh->ResetObjectTexture(msg._index);
            else  
#endif
            {
              Ref<Texture> texture = GlobLoadTexture(msg._name);
              veh->SetObjectTexture(msg._index, texture);
            }
          }
        }
      }
      break;
    case NMTPublicExec:
      {
        PublicExecMessage msg;
        TMSGCHECK(msg.TransferMsg(ctx))
        if (msg._condition.GetLength() > 0)
        {
          GameState *state = GWorld->GetGameState();
          GameVarSpace vars(false);
          state->BeginContext(&vars);
          if (msg._obj) 
            state->VarSetLocal("_this", GameValueExt(msg._obj,GameValExtObject), true);
          else
          {
            GameValue value = state->CreateGameValue(GameArray);
            GameArrayType &array = value;
            array.Resize(msg._objs.Size());
            for (int i=0; i<msg._objs.Size(); i++)
              array[i] = GameValueExt(msg._objs[i]);
            state->VarSetLocal("_this", value, true);
          }
          bool doCommand = state->EvaluateMultiple(msg._condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
          if (doCommand && msg._command.GetLength() > 0)
            state->EvaluateMultiple(msg._command, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
          state->EndContext();
        }
      }
      break;
#endif // _ENABLE_VBS
/*
    case NMTRespawn:
      {
        RespawnQueueItem item;
        TMSGCHECK(item.TransferMsg(ctx))
        DoRespawn(item);
      }
      break;
*/
    case NMTSetFlagOwner:
      {
        SetFlagOwnerMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (!ask._carrier) break;
        ask._carrier->SetFlagOwner(ask._owner);
      }
      break;
    case NMTSetFlagCarrier:
      {
        SetFlagCarrierMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (!ask._owner) break;
        ask._owner->SetFlagCarrier(ask._carrier);
      }
      break;
    case NMTCancelTakeFlag:
      {
        CancelTakeFlagMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (!ask._flag) break;
        ask._flag->CancelTakeFlag();
      }
      break;
    case NMTMsgVTarget:
      {
        RadioMessageVTarget radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVFire:
      {
        RadioMessageVFire radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVMove:
      {
        RadioMessageVMove radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVWatchTgt:
      {
        RadioMessageVWatchTgt radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVWatchPos:
      {
        RadioMessageVWatchPos radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVFormation:
      {
        RadioMessageVFormation radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVSimpleCommand:
      {
        RadioMessageVSimpleCommand radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVLoad:
      {
        RadioMessageVLoad radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVLoadMagazine:
      {
        RadioMessageVLoadMagazine radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVAzimut:
      {
        RadioMessageVAzimut radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVStopTurning:
      {
        RadioMessageVStopTurning radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTMsgVFireFailed:
      {
        RadioMessageVFireFailed radioMsg;
        TMSGCHECK(radioMsg.TransferMsg(ctx))
        radioMsg.Send();
      }
      break;
    case NMTChangeOwner:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        ChangeOwnerMessage co;
        TMSGCHECK(co.TransferMsg(ctx))
        NetworkId id(co._creator, co._id);
        if (co._owner == _player)
        {
          // object becomes local
          NetworkRemoteObjectInfo info;
          if (_remoteObjects.removeKey(id,&info) && info.object)
          {
            NetworkObject *object = info.object;
            if ( object->IsObsolete() )
            {
              ForceDeleteObject(object);
              object->DestroyObject();
              break;
            }
            object->SetLocal(true);
#if CHECK_MSG
  CheckLocalObjects();
#endif
            { // add to local objects
              NetworkLocalObjectInfo localObject;
              localObject.id = id;
              localObject.object = object;
              for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
              {
                NetworkUpdateInfo &info = localObject.updates[j];
                info.lastCreatedMsg = NULL;
                info.lastCreatedMsgId = 0xFFFFFFFF;
                info.lastCreatedMsgTime = 0;
              }
              _localObjects.put(localObject);
              if (DiagLevel >= 1)
                DiagLogF("Client: object %d:%d is now local", localObject.id.creator.CreatorInt(), localObject.id.id);
            }
#if CHECK_MSG
  CheckLocalObjects();
#endif

            Person *person = dynamic_cast<Person *>(object);
            if (person)
            {
              bool player = person == GWorld->GetRealPlayer();
              if (person == GWorld->GetRealPlayer())
              {
#if LOG_TEAM_SWITCH
                RptF("Remote player of %s set to %d (owner change reaction)", cc_cast(person->GetDebugName()), _player);
#endif
                person->SetRemotePlayer(_player);
              }
              else if (co._disconnected)
              {
#if LOG_TEAM_SWITCH
                RptF("Remote player of %s set to %d (owner change reaction)", cc_cast(person->GetDebugName()), AI_PLAYER);
#endif
                person->SetRemotePlayer(AI_PLAYER);
              }
              AIBrain *agent = person->Brain();
              if (agent && agent->IsPlayable())
              {
                const PlayerRole *role = GetPlayerRole(agent->GetRoleIndex());
                if (role)
                {
                  if (!agent->IsAnyPlayer() && !role->GetFlag(PRFEnabledAI))
                  {
                    // avoid asserts
                    agent->SetLocal(true);

                    // Get out from vehicle
                    Transport *vehicle = agent->GetVehicleIn();
                    if (vehicle)
                    {
                      agent->DoGetOut(vehicle, true, false);
                    }

                    // playable person with AI disabled - destroy unit
                    Ref<AIBrain> lockedAgent = agent;
                    AIUnit *unit = lockedAgent->GetUnit();
                    Ref<AISubgroup> subgrp = unit ? unit->GetSubgroup() : NULL;
                    Ref<AIGroup> grp = subgrp ? subgrp->GetGroup() : NULL;

                    // detach brain (to avoid respawn during SetDamage)
                    person->SetBrain(NULL);

                    // destroy person
                    person->SetDamage(1);
                    person->HideBody();
                    Assert(person->IsLocal());
                    // DeleteObject(person->GetNetworkId(), true);

                    if (lockedAgent)
                    {
                      // destroy unit
                      ForceDeleteObject(lockedAgent);
                      lockedAgent->DestroyObject();
                    }

                    if (grp)
                    {
                      if (subgrp->NUnits() == 0 && (subgrp != grp->MainSubgroup() || grp->UnitsCount() == 0))
                      {
                        // destroy subgroup
                        ForceDeleteObject(subgrp);
                        subgrp->DestroyObject();
                      }
                      if (grp->UnitsCount() == 0)
                      {
                        // destroy group
                        ForceDeleteObject(grp);
                        grp->DestroyObject();
                      }
                    }
                  }
                  else if (agent->GetLifeState() == LifeStateDeadInRespawn)
                  {
                    // playable person in respawn, add to respawn queue
                    LogF
                      (
                      "Owner changed. Respawning %s (%s)",
                      (const char *)person->GetDebugName(),
                      player ? "player" : "AI"
                      );
                    Vector3 pos = VZero;
                    switch (GetRespawnMode())
                    {
                    case RespawnAtPlace:
                      pos = person->WorldPosition(person->FutureVisualState());
                      break;
                    case RespawnInBase:
                      Vector3 RespawnInBasePosition(Person *person);
                      pos = RespawnInBasePosition(person);
                      break;
                    default:
                      Fail("Respawn type");
                      break;
                    }
                    Respawn(person, pos, 0.5);
                  }
                }
              }
            } // person
            else
            {
              Transport *vehicle = dynamic_cast<Transport *>(object);
              if (vehicle)
              {
                if (vehicle->IsRespawning())
                {
                  Vector3 pos = VZero;
                  switch (GetRespawnMode())
                  {
                  case RespawnAtPlace:
                    pos = vehicle->WorldPosition(vehicle->FutureVisualState());
                    break;
                  case RespawnInBase:
                    Vector3 VehicleRespawnInBasePosition(Transport *vehicle);
                    pos = VehicleRespawnInBasePosition(vehicle);
                    break;
                  default:
                    Fail("Respawn type");
                    break;
                  }
                  Respawn(vehicle, pos, 0.5);
                }
              }
            }
          }
          else
          {
            RptF("Client: Remote object %d:%d not found", co._creator.CreatorInt(), co._id);
          }
        }
        else
        {
          // object becomes remote
#if CHECK_MSG
          CheckLocalObjects();
#endif
          // send an acknowledgment to server
          OwnerChangedMessage ackMsg;
          ackMsg._creator = co._creator;
          ackMsg._id = co._id;
          SendMsg(&ackMsg, NMFGuaranteed);

#if CHECK_MSG
          CheckLocalObjects();
#endif
          NetworkLocalObjectInfo info;
          if (_localObjects.removeKey(id,&info) && info.object)
          {
            NetworkObject *object = info.object;

            object->SetLocal(false);
            // add to remote objects
            NetworkRemoteObjectInfo info;
            info.id = id;
            info.object = object;
            if (DiagLevel >= 1)
              DiagLogF("Client: object %d:%d is now remote", info.id.creator.CreatorInt(), info.id.id);
            _remoteObjects.put(info);

            // if playable person in respawn, remove from respawn queue
            Person *person = dynamic_cast<Person *>(object);
            AIBrain *unit = person ? person->Brain() : NULL;
            if (unit && unit->GetLifeState() == LifeStateDeadInRespawn)
            {
              // remove from respawn queue
              for (int i=0; i<_respawnQueue.Size(); i++)
              {
                if (_respawnQueue[i]->GetPerson() == person)
                {
                  _respawnQueue.Delete(i);
                  break;
                }
              }
            }
          }
          else
          {
            RptF("Client: Local object %d:%d not found", co._creator.CreatorInt(), co._id);
          }
        }
      }
      break;
    case NMTPlaySound:
      {
        PlaySoundMessage sound;
        TMSGCHECK(sound.TransferMsg(ctx))

        AbstractWave *wave = GSoundScene->OpenAndPlayOnce(
          sound._name,
          NULL,false,
          sound._position, sound._speed,
          sound._volume, sound._frequency
        );
        if (wave)
        {
          GSoundScene->AddSound(wave);

          for (int i=0; i<_receivedSounds.Size();)
          {
            PlaySoundInfo &info = _receivedSounds[i];
            if (info.wave) i++;
            else _receivedSounds.Delete(i);
          }

          int index = _receivedSounds.Add();
          _receivedSounds[index].creator = sound._creator;
          _receivedSounds[index].id = sound._soundId;
          _receivedSounds[index].wave = wave;
        }
      }
      break;
    case NMTSoundState:
      {
        SoundStateMessage sound;
        TMSGCHECK(sound.TransferMsg(ctx))

        for (int i=0; i<_receivedSounds.Size();)
        {
          PlaySoundInfo &info = _receivedSounds[i];
          if (!info.wave)
          {
            _receivedSounds.Delete(i);
            continue;
          }
          if (info.creator == sound._creator && info.id == sound._soundId)
          {
            switch (sound._state)
            {
            case SSRestart:
              info.wave->Restart();
              break;
            case SSStop:
              GSoundScene->DeleteSound(info.wave);
              // remove wave - it may never be started again
              // info.wave is link and should be NULL now
              Assert( !info.wave);
              break;
            case SSRepeat:
              info.wave->Repeat(1000);
              break;
            case SSLastLoop:
              info.wave->LastLoop();
              // remove wave - it may never be started again
              info.wave = NULL;
              break;
            default:
              Fail("Sound state");
              break;
            }
          }
          i++;
        }
      }
      break;
#if _ENABLE_INDEPENDENT_AGENTS
    case NMTTeamMemberSetVariable:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        TeamMemberSetVariableMessage varMsg;
        TMSGCHECK(varMsg.TransferMsg(ctx))

        // get owner
        NetworkId nid(varMsg._creator, varMsg._id);
        NetworkObject *object = GetObject(nid);
        if (!object) break;

        AITeamMember *teamMember = dynamic_cast<AITeamMember *>(object);
        if (!teamMember) break;

        RString name = varMsg._name;
        QIStrStream in(varMsg._value.Data(), varMsg._value.Size());

        GameValue value;
        if (ReadValue(value, in, _parent))
        {
          // set the variable
          teamMember->GetVars()->VarSet(name, value);
        }
      }
      break;
#endif
    case NMTPublicVariable:
    case NMTPublicVariableTo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        PublicVariableMessage varMsg;
        TMSGCHECK(varMsg.TransferMsg(ctx))

        RString name = varMsg._name;
        QIStrStream in(varMsg._value.Data(), varMsg._value.Size());

        GameState *gstate = GWorld->GetGameState();

        GameValue value;
        if (ReadValue(value, in, _parent))
        {
          // set the variable
          GWorld->GetGameState()->VarSet(name, value, false, false, GWorld->GetMissionNamespace()); // mission namespace

          // invoke the event handler
          const PublicVariableEventHandler &handler = _publicVariableEventHandlers[name];
          if (_publicVariableEventHandlers.NotNull(handler))
          {
            GameValue arguments = gstate->CreateGameValue(GameArray);
            GameArrayType &array = arguments;
            array.Add(GameValue(name));
            array.Add(value);

            GameVarSpace local(gstate->GetContext(), false);
            gstate->BeginContext(&local);
            gstate->VarSetLocal("_this", arguments, true);
#if USE_PRECOMPILATION
            gstate->Evaluate(handler._code->GetString(), handler._code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#else
            gstate->EvaluateMultiple(handler._code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#endif
            gstate->EndContext();
          }
        }
      }
      break;
    case NMTObjectSetVariable:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        ObjectSetVariableMessage varMsg;
        TMSGCHECK(varMsg.TransferMsg(ctx))

        if (varMsg._object)
        {
          GameVarSpace *vars = varMsg._object->GetVars();
          if (vars)
          {
            RString name = varMsg._name;
            GameValue value;
            if(varMsg._value.Size() == 0)
            {
              vars->VarSet(name, value);
            }
            else
            {
              QIStrStream in(varMsg._value.Data(), varMsg._value.Size());
              if (ReadValue(value, in, _parent))
              {
                vars->VarSet(name, value);
              }
            }
          }
        }
      }
      break;
    case NMTGroupSetVariable:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        GroupSetVariableMessage varMsg;
        TMSGCHECK(varMsg.TransferMsg(ctx))

        if (varMsg._group)
        {
          GameVarSpace *vars = varMsg._group->GetVars();
          if (vars)
          {
            RString name = varMsg._name;
            GameValue value;
            if(varMsg._value.Size() == 0)
            {
              vars->VarSet(name, value);
            }
            else
            {
              QIStrStream in(varMsg._value.Data(), varMsg._value.Size());
              if (ReadValue(value, in, _parent))
              {
                vars->VarSet(name, value);
              }
            }
          }
        }
      }
      break;
    case NMTGroupSynchronization:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        GroupSynchronizationMessage sync;
        TMSGCHECK(sync.TransferMsg(ctx))
        //hotfixed, sync._synchronization was out of array size
        if (sync._group && sync._synchronization >=0 && sync._synchronization < synchronized.Size())
          synchronized[sync._synchronization].SetActive(sync._group, sync._active);
      }
      break;
    case NMTDetectorActivation:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
        DetectorActivationMessage act;
        TMSGCHECK(act.TransferMsg(ctx))
        if (act._detector)
          act._detector->DoActivate();
      }
      break;
#if _VBS2 //Convoy trainer
    case NMTEnablePersonalItems:
      {
        EnablePersonalItemsMessage act;
        TMSGCHECK(act.TransferMsg(ctx))
        if(act._object)
          act._object->SetEnablePersonalItems(act._active, act._animAction, act._personalItemsOffset);
      }
      break;
    case NMTLoadIsland:
      {
        LoadIslandMessage act;
        TMSGCHECK(act.TransferMsg(ctx))

        // get the client to load the current island
        GWorld->SwitchLandscape(act._islandName);
      }
      break;
    case NMTAddMPReport:
      {
        AddMPReportMessage act;
        TMSGCHECK(act.TransferMsg(ctx))

        switch(act._type)
        {
        case AddHeader:
          GStats.VBSAddHeader(act._myString);
          break;
        case AddEvent:
          GStats.VBSAddEvent(act._myString);
          break;
        case AddFooter:
          GStats.VBSAddFooter(act._myString);
          break;
        }
      }
      break;
 #if _EXT_CTRL
    case NMTAARDoUpdate:
      {
        AARDoUpdateMessage act;
        TMSGCHECK(act.TransferMsg(ctx))
        
        switch(act._type)
        {
          case StartRecord:
            GAAR.RemoteRecord();
            break;
          case StopRecord:
            GAAR.RemoteRecordStop();
            // so something regarding stoping the record
            break;
          case SaveFile:
            GAAR.RemoteRecordSave(act._stringOne); // not used on remote clients
            break;
          case SaveState:
            GAAR.SetRemoteSaveState(act._intType);
            break;
          case AddBookMK:
            GAAR.RemoteAddBookMark(act._floatType,act._stringOne,act._stringTwo);
            break;
          case RemBookMK:
            GAAR.RemoteRemoveBookMark(act._stringOne);
            break;
          default:
            Assert(false);// message type not handeled, development bug
        }
      }
      break;
  #endif
#endif
    case NMTAskForCreateUnit:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskForCreateUnitMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        void CreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank);
        if (ask._group)
          CreateUnit(ask._group, ask._type, ask._position, ask._init, ask._skill, (Rank)ask._rank);
      }
      break;
    case NMTAskForDeleteVehicle:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskForDeleteVehicleMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        void DeleteVehicle(Entity *veh);
        if (ask._vehicle) DeleteVehicle(ask._vehicle);
      }
      break;
    case NMTAskForReceiveUnitAnswer:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskForReceiveUnitAnswerMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._to) ask._to->ReceiveAnswer(ask._from,(AI::Answer)ask._answer);
      }
      break;
    case NMTAskForOrderGetIn:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskForOrderGetInMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._unit) ask._unit->OrderGetInLocal(ask._flag);
      }
      break;
    case NMTAskForAllowGetIn:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskForAllowGetInMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._unit) ask._unit->AllowGetInLocal(ask._flag);
      }
      break;
#if _ENABLE_CONVERSATION
    case NMTKBReact:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
 
        KBReactMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._to)
        {
          const KBCenter *kbCenter = ask._to->GetKBCenter();
          if (!kbCenter) break;
          const KBCenter *kbParent = ask._to->GetKBParent();

          KBTopic *topic = kbCenter->GetTopic(ask._topic, kbParent);
          if (topic)
          {
            const KBMessageTemplate *type = topic->FindMessageTemplate(ask._message);
            if (type)
            {
              Ref<KBMessage> message = new KBMessage(type);
              // arguments
              int n = ask._arguments.Size();
              for (int i=0; i<n; i++)
              {
                const PublicVariableMessage &var = ask._arguments[i];
                RString name = var._name;
                QIStrStream in(var._value.Data(), var._value.Size());
                GameValue value;
                if (ReadValue(value, in, _parent))
                {
                  // text and speech contain no info, need not to transfer them
                  RString text;
                  AutoArray<RString> speech;
                  message->SetArgument(name, value, text, speech);
                }
              }
              Ref<KBMessageInfo> info = new KBMessageInfo(ask._topic, message);
#if _ENABLE_IDENTITIES
              // register sentence on this client as well
              if (ask._from && ask._from->GetPerson() && ask._from->GetKBCenter())
              {
                ask._from->GetPerson()->GetIdentity().AddSaid(ask._to, info);
                const KBCenter *kbParent = ask._from->GetKBParent();
                RString message = ask._from->GetKBCenter()->GetText(info, kbParent);
                if (message.GetLength() > 0)
                {
                  // as unit use the person we are speaking with
                  // as target use the speaker
                  ask._from->GetPerson()->GetIdentity().AddDiaryRecord("Conversation", RString(), message, NULL, TSNone, ask._to, ask._from, true);
                  ask._to->GetPerson()->GetIdentity().AddDiaryRecord("Conversation", RString(), message, NULL, TSNone, ask._from, ask._from, true);
                }
              }
#endif
              // process reaction
              ask._to->KBReact(ask._from, info);
            }
          }
        }
      }
      break;
#endif
    case NMTSendAUMsg:
      {
        SendAUMsgMessage varMsg;
        TMSGCHECK(varMsg.TransferMsg(ctx))

        QIStrStream in(varMsg._value.Data(), varMsg._value.Size());

        GameValue value;
        if (ReadValue(value, in, _parent))
        {
          const GameArrayType &array=value;
          if ( array.Size() == 2 )
          {
            RString command = array[1];
            extern void ProcessAUCommand(RString command);
            ProcessAUCommand(command);
          }
        }
      }
      break;

    case NMTAskForGroupRespawn:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskForGroupRespawnMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
LogF
(
  "Group respawn: ask received - person %s, group %s, from %d",
  ask._person ? (const char *)ask._person->GetDebugName() : NULL,
  ask._group ? (const char *)ask._group->GetDebugName() : NULL,
  ask._from
);
        if (ask._person && ask._group && ask._person->Brain() && ask._person->Brain()->GetGroup() == ask._group)
        {
          Person *ProcessGroupRespawn(Person *person, int player);
          Person *respawn = ProcessGroupRespawn(ask._person, ask._from);

          GroupRespawnDoneMessage answer;
          answer._person = ask._person;
          answer._killer = ask._killer;
          answer._respawn = respawn;
          answer._to = ask._from;
LogF
(
  "Group respawn: sending answer - person %s, respawn %s, to %d",
  answer._person ? (const char *)answer._person->GetDebugName() : NULL,
  answer._respawn ? (const char *)answer._respawn->GetDebugName() : NULL,
  answer._to
);
          SendMsg(&answer, NMFGuaranteed);
        }
      }
      break;
#if _VBS3 // respawn command
    case NMTApplyWeather:
      {
        ApplyWeatherMessage act;
        TMSGCHECK(act.TransferMsg(ctx))

        GLandscape->ApplyWeatherMsg(act);
#if _EXT_CTRL
        if(IsBotClient()) _hla.WeatherUpdate(&act);
#endif
      }
      break;
    case NMTPauseSimulation:
      {
        PauseSimulationMessage msg;
        TMSGCHECK(msg.TransferMsg(ctx))
      
        // Finaly pause the local simulation.
        GWorld->PauseSimulation(msg._pause);
      }
      break;
    case NMTAskForRespawn:
      {
		    AskForRespawnMessage ask;
		    TMSGCHECK(ask.TransferMsg(ctx))
		    if (ask._person)
		    {
			    if (ask._person->IsLocal())
			    {
				    // kill off any running camera scripts
				    GWorld->TerminateCameraScript();
				    GWorld->SetCameraEffect(NULL);

				    // kill off seagulls
            //DestroySeagull build();
            //GWorld->ForEachAnimal(build);

            if (GWorld->CameraOn())
              LogF("[p] camera is on %s",GWorld->CameraOn()->GetDebugName().Data());

				    Respawn(ask._person,ask._person->Position());
			    }
		    }
      }
      break;
    case NMTAskCommanderOverride:
      {
        AskCommanderOverrideMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._turret)
        {
          Assert(ask._turret->IsLocal());
          Turret* overriddenByTurret = ask._overriddenByTurret;
          ask._turret->_overriddenByTurret = overriddenByTurret;
        }
        else
        {
          Fail("Error: ask._turret was NULL");
        }
      }
      break;
#endif
#if _ENABLE_ATTACHED_OBJECTS
    case NMTAttachObject:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AttachObjectMessage msg;
        TMSGCHECK(msg.TransferMsg(ctx))
        if (msg._obj) msg._obj->AttachTo(msg._attachTo, msg._pos, msg._memIndex, msg._flags);
# if _EXT_CTRL //ext. Controller like HLA, AAR
        if(IsBotClient()) _hla.AttachToMsg(msg._obj,msg._attachTo, msg._memIndex,msg._pos,msg._flags);
# endif
      }
      break;
    case NMTDetachObject:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        DetachObjectMessage msg;
        TMSGCHECK(msg.TransferMsg(ctx))
        if (msg._obj) msg._obj->Detach();
# if _EXT_CTRL //ext. Controller like HLA, AAR
        if(IsBotClient()) _hla.AttachToMsg(msg._obj);
# endif
      }
      break;
#endif
    case NMTAskForActivateMine:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskForActivateMineMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._mine) ask._mine->SetActive(ask._activate);
      }
      break;
    case NMTAskForInflameFire:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskForInflameFireMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._fireplace) ask._fireplace->Inflame(ask._fire);
      }
      break;
    case NMTAskForAnimationPhase:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskForAnimationPhaseMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
#if _VBS3
        float preValue = 0.0f;
        if(ask._vehicle)
          preValue = ask._vehicle->GetAnimationPhase(ask._vehicle->FutureVisualState(), ask._animation);
#endif
        if (ask._vehicle) ask._vehicle->SetAnimationPhase(ask._animation, ask._phase);
#if _EXT_CTRL //ext. Controller like HLA, AAR
        if(IsBotClient())
        {
          _hla.RecordAnimationPhase(ask._vehicle,ask._animation,preValue);
          _hla.RecordAnimationPhase(ask._vehicle,ask._animation,ask._phase);
        }
#endif
      }
      break;
    case NMTAskWeapon:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskWeaponMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._from)
        {
          Ref<WeaponType> weapon = WeaponTypes.New(ask._weapon);
          ask._from->OfferWeapon(ask._to, weapon, ask._slots, ask._useBackpack);
        }
      }
      break;
    case NMTAskMagazine:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskMagazineMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._from)
        {
          Ref<MagazineType> type = MagazineTypes.New(ask._type);
          ask._from->OfferMagazine(ask._to, type, ask._useBackpack);
        }
      }
      break;
    case NMTAskBackpack:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskBackpackMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._from)
        {
          ask._from->OfferBackpack(ask._to, ask._name);
        }
      }
      break;
    case NMTReplaceWeapon:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        ReplaceWeaponMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._to && ask._from)
        {
          Ref<WeaponType> weapon = WeaponTypes.New(ask._weapon);
          ask._to->ReplaceWeapon(ask._from, weapon, ask._useBackpack);
        }
      }
      break;
    case NMTReplaceMagazine:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        ReplaceMagazineMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._to)
        {
          ask._to->ReplaceMagazine(ask._from, ask._magazine, ask._reload, ask._useBackpack);
        }
      }
      break;
    case NMTReplaceBackpack:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        ReplaceBackpackMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._to)
        {
          ask._to->ReplaceBackpack(ask._from, ask._backpack);
        }
      }
      break;
    case NMTReturnWeapon:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        ReturnWeaponMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._from)
        {
          Ref<WeaponType> weapon = WeaponTypes.New(ask._weapon);
          ask._from->ReturnWeapon(weapon);
        }
      }
      break;
    case NMTReturnMagazine:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        ReturnMagazineMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._from) ask._from->ReturnMagazine(ask._magazine);
      }
      break;
    case NMTReturnBackpack:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        ReturnBackpackMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._from)
        {
          ask._from->ReturnBackpack(ask._backpack);
        }
      }
      break;
    case NMTLockSupply:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        LockSupplyMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
          if (ask._supply)
          {
            ask._supply->LockResources(ask._user);
          }
      }
      break;
    case NMTUnlockSupply:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        UnlockSupplyMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
          if (ask._supply)
          {
            ask._supply->UnlockResources(ask._user);
          }
      }
      break;
    case NMTReturnEquipment:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        ReturnEquipmentMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))

          if (!ask._to || !ask._to->IsLocal())
          { 
            // !ask._to             >> ammo box was deleted
            // !ask._to->IsLocal()  >> ammo box is deleted on server and ReturnEquipmentMessage has returned 
            if(ask._from)
            {
              EntityAI *vehicle = ask._from;
              Vector3 pos = vehicle->FutureVisualState().Position() + 0.5f * vehicle->FutureVisualState().Direction() + VUp*0.5f;
              Matrix3 dir;
              dir.SetUpAndDirection(VUp, vehicle->FutureVisualState().Direction());
              Matrix4 transform;
              transform.SetPosition(pos);
              transform.SetOrientation(dir);

              // create container
              Ref<EntityAI> veh = GWorld->NewVehicleWithID("WeaponHolder");
              if (veh) 
              {
                veh->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
                veh->SetTransform(transform);
                veh->Init(transform, true);
                veh->OnEvent(EEInit);

                GWorld->AddSlowVehicle(veh);
                if (GWorld->GetMode() == GModeNetware)
                  GetNetworkManager().CreateVehicle(veh, VLTSlowVehicle, "", -1);
                GWorld->OnEntityMovedFar(veh);

                veh->ReturnEquipment(ask._weapons,ask._magazines, ask._backpack);
              }
            }
          }
          else
          {
            ask._to->ReturnEquipment(ask._weapons,ask._magazines, ask._backpack);
          }
      }
      break;
    case NMTPoolReplaceWeapon:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        PoolReplaceWeaponMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        
        NetworkId id;
        id.creator = ask._creator;
        id.id = ask._id;
        NetworkObject *obj = GetObject(id);
        if (!obj) break;
        Assert(dynamic_cast<AIUnit *>(obj));
        AIUnit *unit = static_cast<AIUnit *>(obj);

        Ref<WeaponType> weapon = WeaponTypes.New(ask._weapon);

        void PoolReplaceWeapon(AIUnit *unit, WeaponType *weapon, int slot, bool useBackpack);
        PoolReplaceWeapon(unit, weapon, ask._slot, ask._useBackpack);
      }
      break;
    case NMTPoolReplaceMagazine:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        PoolReplaceMagazineMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))

        NetworkId id;
        id.creator = ask._creator;
        id.id = ask._id;
        NetworkObject *obj = GetObject(id);
        if (!obj) break;
        Assert(dynamic_cast<AIUnit *>(obj));
        AIUnit *unit = static_cast<AIUnit *>(obj);

        void PoolReplaceMagazine(AIUnit *unit, Magazine *magazine, int slot, bool useBackpack);
        PoolReplaceMagazine(unit, ask._magazine, ask._slot, ask._useBackpack);
      }
      break;
    case NMTPoolReplaceBackpack:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        PoolReplaceBackpackMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))

        NetworkId id;
        id.creator = ask._creator;
        id.id = ask._id;
        NetworkObject *obj = GetObject(id);
        if (!obj) break;
        Assert(dynamic_cast<AIUnit *>(obj));
        AIUnit *unit = static_cast<AIUnit *>(obj);

        NetworkId idBag;
        idBag.creator = ask._bagCreator;
        idBag.id = ask._bagId;
        NetworkObject *objBag = GetObject(idBag);
        if (!objBag) break;
        Assert(dynamic_cast<EntityAI *>(objBag));
        EntityAI *backpack = static_cast<EntityAI *>(objBag);

        void PoolReplaceBackpack(AIUnit *unit, EntityAI *backpack);
        PoolReplaceBackpack(unit, backpack);
      }
      break;
    case NMTUpdateWeaponsInfo:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        void PoolUpdateWeapons(NetworkMessageContext &ctx);
        PoolUpdateWeapons(ctx);
      }
      break;
    case NMTUpdateWeaponsPool:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        void PoolUpdateWeaponsPool(NetworkMessageContext &ctx);
        PoolUpdateWeaponsPool(ctx);
      }
      break;
    case NMTUpdateMagazinesPool:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        void PoolUpdateMagazinesPool(NetworkMessageContext &ctx);
        PoolUpdateMagazinesPool(ctx);
      }
      break;
    case NMTUpdateBackpacksPool:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        void PoolUpdateBackpacksPool(NetworkMessageContext &ctx);
        PoolUpdateBackpacksPool(ctx);
      }
      break;
    case NMTGroupRespawnDone:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        GroupRespawnDoneMessage answer;
        TMSGCHECK(answer.TransferMsg(ctx))
LogF
(
  "Group respawn: answer received - person %s, respawn %s",
  answer._person ? (const char *)answer._person->GetDebugName() : NULL,
  answer._respawn ? (const char *)answer._respawn->GetDebugName() : NULL
);
        if (answer._person)
        {
          if (answer._respawn)
          {
            void GroupRespawnDone(Person *person, EntityAI *killer, Person *respawn);
            GroupRespawnDone(answer._person, answer._killer, answer._respawn);
          }
          else
          {
            void ProcessSeagullRespawn(Person *person, EntityAI *killer);
            ProcessSeagullRespawn(answer._person, answer._killer);
          }
        }
      }
      break;
    case NMTChat:
      {
        ChatMessage chat;
        TMSGCHECK(chat.TransferMsg(ctx))
        if (chat._channel<0 || chat._channel>=CCN)
        {
          RptF("NMTChat received bad channel %d",chat._channel);
          break;
        }
        chat._text = chat._text.Substring(0, MaxChatLength);

        if (chat._sender)
          GChatList.Add((ChatChannel)chat._channel, chat._sender, chat._text, false, true);
        else
          GChatList.Add((ChatChannel)chat._channel, chat._name, chat._text, false, true);
        if (_chatSound.name.GetLength() > 0)
        {
          AbstractWave *wave = GSoundScene->OpenAndPlayOnce2D
          (
            _chatSound.name, _chatSound.vol, _chatSound.freq, false
          );
          if (wave)
          {
            wave->SetKind(WaveMusic); // UI sounds considered music???
            wave->SetSticky(true); // enable chat sounds in the lobby
            GSoundScene->AddSound(wave);
          }
        }
      }
      break;
    case NMTLocalizedChat:
      {
        LocalizedChatMessage lChat;
        TMSGCHECK(lChat.TransferMsg(ctx))
        if (lChat._channel<0 || lChat._channel>=CCN)
        {
          RptF("NMTLocalizedChat received bad channel %d",lChat._channel);
          break;
        }
        RString text = LocalizedFormatedStringTemp(lChat._args).GetLocalizedValue();
        if (lChat._sender)
          GChatList.Add((ChatChannel)lChat._channel, lChat._sender, text, false, true);
        else
          GChatList.Add((ChatChannel)lChat._channel, lChat._name, text, false, true);
        if (_chatSound.name.GetLength() > 0)
        {
          AbstractWave *wave = GSoundScene->OpenAndPlayOnce2D
          (
            _chatSound.name, _chatSound.vol, _chatSound.freq, false
          );
          if (wave)
          {
            wave->SetKind(WaveMusic); // UI sounds considered music???
            wave->SetSticky(true); // enable chat sounds in the lobby
            GSoundScene->AddSound(wave);
          }
        }
      }
      break;
    case NMTRadioChat:
      {
        RadioChatMessage rChat;
        TMSGCHECK(rChat.TransferMsg(ctx))
        if (rChat._channel<0 || rChat._channel>=CCN)
        {
          RptF("NMTRadioChat received bad channel %d",rChat._channel);
          break;
        }
        // if (!chat.sender) break;
        Person *soldier = GWorld->GetRealPlayer();
        if (!soldier) break;
        if (rChat._channel != CCGlobal && !FindUnit(GWorld->GetRealPlayer(), rChat._units)) break;
        // TODO: playerMsg
        GChatList.Add((ChatChannel)rChat._channel, rChat._sender, rChat._text, false, false);
        if (rChat._sender && rChat._sentence.Size() > 0)
        {
          AIBrain *unit = soldier->Brain();
          if (!unit) break;
          RadioChannel *channel = FindChannel(unit, rChat._channel);
          if (channel)
          {
            Person *who = NULL;
            if (channel->GetParams().sound3D) who = rChat._sender->GetPerson();
            rChat._sentence.wordsClass = rChat._wordsClass;
            rChat._sentence.Say(who, channel, rChat._sender->GetSpeaker());
          }
        }
      }
      break;
    case NMTRadioChatWave:
      {
        RadioChatWaveMessage chat;
        TMSGCHECK(chat.TransferMsg(ctx))
        if (chat._channel<0 || chat._channel>=CCN)
        {
          RptF("NMTRadioChatWave received bad channel %d",chat._channel);
          break;
        }
        Person *soldier = GWorld->GetRealPlayer();
        if (!soldier) break;
        if (chat._channel != CCGlobal && !FindUnit(GWorld->GetRealPlayer(), chat._units)) break;
        if (chat._wave.GetLength() > 0)
        {
          AIBrain *unit = soldier->Brain();
          if (!unit) break;
          RadioChannel *channel = FindChannel(unit, chat._channel);
          if (channel)
          {
            RString player;
            if (chat._wave[0] == '#' && chat._sender)
            {
              player = chat._sender->GetPerson()->GetPersonName();
            }
            channel->Say(chat._wave, chat._sender, chat._senderName, player, 2.0);
          }
        }
      }
      break;
    case NMTSetSpeaker:
      {
        Fail("Network message 'SetSpeaker' not implemented!");
      }
      break;
    case NMTServerState:
      {
        ServerStateMessage state;
        TMSGCHECK(state.TransferMsg(ctx))
        OnServerStateChanged((NetworkServerState)state._serverState);
      }
      break;
    case NMTServerTimeout:
      {
        ServerTimeoutMessage timeout;
        TMSGCHECK(timeout.TransferMsg(ctx))
        _serverTimeout = timeout._timeout;
      }
      break;
    case NMTServerInfo:
      {
        ServerInfoMessage serverInfo;
        TMSGCHECK(serverInfo.TransferMsg(ctx))
        _serverName = serverInfo._hostname;
      }
      break;
    case NMTDeleteObject:
      if ( !CanReceiveUpdateMessages() ) 
        break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
      {
        DeleteObjectMessage dom;
        TMSGCHECK(dom.TransferMsg(ctx))
        NetworkId id(dom._creator, dom._id);
        DestroyRemoteObject(id);
      }
      break;
    case NMTDeleteCommand:
      if ( !CanReceiveUpdateMessages() ) 
        break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
      {
        DeleteCommandMessage dc;
        TMSGCHECK(dc.TransferMsg(ctx))
        NetworkId id(dc._creator, dc._id);
        if (dc._subgrp) dc._subgrp->DeleteCommand(id);
        if (_remoteObjects.removeKey(id))
        {
          if (DiagLevel >= 1)
            DiagLogF("Client: remote command destroyed %d:%d", dc._creator.CreatorInt(), dc._id);
          break;
        }
      }
      break;
    case NMTCreateObject:
      CreateRemoteObject<Object>(ctx);
      break;
    case NMTCreateVehicle:
      CreateRemoteObject<Vehicle>(ctx);
      break;
    case NMTCreateDetector:
      CreateRemoteObject<Detector>(ctx);
      break;
    case NMTCreateShot:
      CreateRemoteObject<Shot>(ctx);
      break;
    case NMTCreateCrater:
      CreateRemoteObject<Crater>(ctx);
      break;
    case NMTCreateCraterOnVehicle:
      CreateRemoteObject<CraterOnVehicle>(ctx);
      break;
    case NMTCreateObjectDestructed:
      CreateRemoteObject<ObjectDestructed>(ctx);
      break;
    case NMTCreateDynSoundSource:
      CreateRemoteObject<DynSoundSource>(ctx);
      break;
    case NMTCreateAICenter:
      CreateRemoteObject<AICenter>(ctx);
      break;
    case NMTCreateAIGroup:
      CreateRemoteObject<AIGroup>(ctx);
      break;
    case NMTCreateAISubgroup:
      CreateRemoteObject<AISubgroup>(ctx);
      break;
/*
    case NMTCreateAIBrain:
      CreateRemoteObject<AIBrain>(ctx);
      break;
*/
    case NMTCreateAIUnit:
      CreateRemoteObject<AIUnit>(ctx);
      break;
#if _ENABLE_INDEPENDENT_AGENTS
    case NMTCreateAIAgent:
      CreateRemoteObject<AIAgent>(ctx);
      break;
    case NMTCreateAITeam:
      CreateRemoteObject<AITeam>(ctx);
      break;
#endif
    case NMTCreateCommand:
      CreateRemoteObject<Command>(ctx);
      break;
    case NMTCreateHelicopter:
      CreateRemoteObject<HelicopterAuto>(ctx);
      break;
    case NMTCreateSeagull:
      CreateRemoteObject<SeaGullAuto>(ctx);
      break;
    case NMTCreateTurret:
      CreateRemoteObject<Turret>(ctx);
      break;
    case NMTAIStatsMPRowCreate:
      CreateRemoteObject<AIStatsMPRow>(ctx);
      break;
    case NMTCreateDestructionEffects:
      CreateRemoteObject<DestructionEffects>(ctx);
      break;
    case NMTCreateClientCameraPosition:
      CreateRemoteObject<ClientCameraPositionObject>(ctx);
      break;
    case NMTUpdateDamageObject:
    case NMTUpdateDamageVehicle:
    case NMTUpdateDamageVehicleAI:
      ctx.SetClass(NMCUpdateDamage);
      goto Update;
    case NMTUpdatePositionVehicle:
    case NMTUpdatePositionMan:
    case NMTUpdatePositionTank:
    case NMTUpdatePositionCar:
    case NMTUpdatePositionAirplane:
    case NMTUpdatePositionHelicopter:
    case NMTUpdatePositionShip:
    case NMTUpdatePositionDoor:
    case NMTUpdatePositionSeagull:
    case NMTUpdatePositionMotorcycle:
    case NMTUpdatePositionTurret:
      ctx.SetClass(NMCUpdatePosition);
      // continue with update
    case NMTUpdateObject:
    case NMTUpdateVehicle:
    case NMTUpdateDetector:
    case NMTUpdateFlag:
    case NMTUpdateShot:
    case NMTUpdateMine:
    case NMTUpdateVehicleAI:
    case NMTUpdateVehicleAIFull:
    case NMTUpdateVehicleBrain:
    case NMTUpdateTransport:
    case NMTUpdateMan:
    case NMTUpdateTankOrCar:
    case NMTUpdateTank:
    case NMTUpdateCar:
    case NMTUpdateAirplane:
    case NMTUpdateHelicopter:
    case NMTUpdateParachute:
    case NMTUpdateShip:
    case NMTUpdateDoor:
    case NMTUpdateSeagull:
    case NMTUpdateAICenter:
    case NMTUpdateAIGroup:
    case NMTUpdateAISubgroup:
    case NMTUpdateAIBrain:
    case NMTUpdateAIUnit:
    case NMTUpdateAIAgent:
    case NMTUpdateAITeam:
    case NMTUpdateCommand:
    case NMTUpdateMotorcycle:
    case NMTUpdateFireplace:
    case NMTAIStatsMPRowUpdate:
    case NMTUpdateTurret:
    case NMTUpdateClientCameraPosition:
    Update:
      {
        // find IndicesNetworkObject level
        PREPARE_TRANSFER(NetworkObject)

        NetworkId id;
        TRANSF_BASE(objectCreator, id.creator);
        TRANSF_BASE(objectId, id.id);

        if (_clientState < NCSMissionReceived)
        {
          LogF("Update of %d:%d ignored, state %s", id.creator.CreatorInt(), id.id, (const char *)FindEnumName(_clientState));
          break; // updates from the last session
        }

        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        // update the transfer progress
        if (_clientState == NCSMissionReceived && ctx.GetClass() == NMCUpdateGeneric && _remainingToUpdate > 0) _remainingToUpdate--;

        NetworkObject *object = GetObject(id);
        if (!object)
        {
          RptF
          (
            "Client: Object %d:%d (type %s) not found.",
            id.creator.CreatorInt(), id.id,
            cc_cast(GetMsgTypeName(type))
          );
          break;
        }
        if (object->IsLocal())
        {
          RptF
          (
            "Client: Object (id %d:%d, type %s) is local - update is ignored.",
            id.creator.CreatorInt(), id.id,
            cc_cast(GetMsgTypeName(type))
          );
          break;
        }
        TMSGCHECK(object->TransferMsg(ctx))
        if (DiagLevel >= 4)
          DiagLogF("Client: object %d:%d updated", id.creator.CreatorInt(), id.id);

#if _EXT_CTRL //ext. Controller like HLA, AAR 
        if(IsBotClient())
          _hla.UpdateObject(object);
#endif

      }
      break;
    case NMTUpdateClientInfo:
      Fail("Unexpected on client");
      break;
    case NMTUpdateClientInfoDpid:
    {
      // camera position is necessary for dead players VoN Direct Speech
      ctx.SetClass(NMCUpdatePosition);
      ClientInfoObject clientInfo;
      TMSGCHECK(clientInfo.TransferMsg(ctx))

      GetNetworkManager().SetVoNPosition(clientInfo._dpnid, clientInfo._cameraPosition, VZero);
      break;
    }
    case NMTPlayerUpdate:
    {
      ctx.SetClass(NMCUpdatePosition);
      //Assert(ctx.GetClass()==NMCUpdatePosition);
      PREPARE_TRANSFER(PlayerUpdate)
      // check which identity is this message affecting
      int dpnid;
      if (TRANSF_BASE(dpnid, dpnid)==TMOK)
      {
        // find corresponding identity
        PlayerIdentity *pi = FindIdentity(dpnid);
        if (pi)
        {
          // update identity
          TMSGCHECK(pi->TransferMsg(ctx))
        }
      }
      break;
    }
    case NMTShowTarget:
      { // used only to show targets to players, never to assign targets to AI
        ShowTargetMessage show;
        TMSGCHECK(show.TransferMsg(ctx))
        Person *person = show._vehicle;
        AIBrain *unit = person ? person->Brain() : NULL;
        if (unit && show._target)
        {
          Target *target = unit->FindTarget(show._target);
          unit->AssignTarget(TargetToFire(target,TargetAlive));
          GWorld->UI()->ShowTarget();
        }
      }
      break;
    case NMTShowGroupDir:
      {
        ShowGroupDirMessage show;
        TMSGCHECK(show.TransferMsg(ctx))
        Person *person = show._vehicle;
        AIBrain *unit = person ? person->Brain() : NULL;
        AIGroup *grp = unit ? unit->GetGroup() : NULL;
        AISubgroup *subgrp = grp ? grp->MainSubgroup() : NULL;
        if (subgrp)
        {
          subgrp->SetDirection(show._dir);
          GWorld->UI()->ShowGroupDir();
        }
      }
      break;
    case NMTMissionParams:
      {
        MissionParamsMessage params;
        TMSGCHECK(params.TransferMsg(ctx))

        _param1 = params._param1;
        _param2 = params._param2;
        _paramsArray = params._paramsArray;

        if (!params._updateOnly)
        {
          // whole mission info received, ask for mission file
          AskMissionFileMessage ask;
          ask._valid = _missionFileValid;
          SendMsg(&ask, NMFGuaranteed);

          SetClientState(NCSMissionAsked);
        }
        // else - update of mission params in Multiplayer Setup Display
      }
      break;
    case NMTJoinIntoUnit:
      {
        JoinIntoUnitMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        JoinIntoUnit(ask._dpnid, ask._roleIndex, ask._unit, ask._group);
      }
      break;
    case NMTForceDeleteObject:
      {
        ForceDeleteObjectMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._object)
        {
          NetworkId nid=ask._object->GetNetworkId();
          DeleteObject(nid, false); // do not resend message
          ask._object->DestroyObject();
        }
      }
      break;
    case NMTVoiceOn:
#if _GAMES_FOR_WINDOWS || defined _XBOX
      {
        VoiceOnMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        PlayerIdentity *identity = FindIdentity(ask._dpnid);
        if (identity) identity->voiceOn = ask._voiceOn;
      }
#endif
      break;
    case NMTCleanupPlayer:
      {
        CleanupPlayerMessage state;
        TMSGCHECK(state.TransferMsg(ctx))

        NetworkId person(state._creator, state._id);
        CleanupPlayer(state._dpnid, person);
      }
      break;
    case NMTSetRoleIndex:
      {
        SetRoleIndexMessage state;
        TMSGCHECK(state.TransferMsg(ctx))

        if (state._unit) state._unit->SetRoleIndex(state._roleIndex);
      }
      break;
    case NMTRevealTarget:
      {
        RevealTargetMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        LogF("Reveal Target received");
        if (ask._group && ask._group->Leader() && ask._target)
        {
          ask._group->Leader()->AddTarget(ask._target, 4.0f, 4.0f, 0);
          LogF(" - target %s added for group %s", (const char *)ask._target->GetDebugName(), (const char *)ask._group->GetDebugName());
        }
      }
      break;
    case NMTAskForStatsWrite:
#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER >= 200
      // Statistics handled by the NetworkManager
# else
      {
        XONLINE_USER *user = XOnlineGetLogonUsers();
        DoAssert(user);
        if (!user) break;
        
        AskForStatsWriteMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        // TODO: Make task member of client and don't wait here
        int rows = 1;
        XONLINE_STAT_SPEC specs[2];
        specs[0].xuidUser = user->xuid;
        specs[0].dwLeaderBoardID = ask._board;
        specs[0].dwNumStats = ask._stats.Size();
        specs[0].pStats = ask._stats.Data();
        if (ask._boardTotal >= 0)
        {
          specs[1].xuidUser = user->xuid;
          specs[1].dwLeaderBoardID = ask._boardTotal;
          specs[1].dwNumStats = ask._statsTotal.Size();
          specs[1].pStats = ask._statsTotal.Data();
          rows = 2;
        }

        if (_writeStatsTask)
        {
          // finish the task
          HRESULT result;
          do
          {
            result = XOnlineTaskContinue(_writeStatsTask);
          } while(result == XONLINETASK_S_RUNNING);
          XOnlineTaskClose(_writeStatsTask);
          _writeStatsTask = NULL;
#if LOG_LIVE_STATS
          if (FAILED(result))
            RptF(" - XOnlineStatWrite update failed with error 0x%x", result);
          else
            RptF(" - Statistics written");
#endif
        }

        HRESULT result = XOnlineStatWrite(rows, specs, NULL, &_writeStatsTask);
        if (FAILED(result))
        {
          // cannot write statistics
#if LOG_LIVE_STATS
          RptF(" - XOnlineStatWrite failed with error 0x%x", result);
#endif
          break;
        }
#if LOG_LIVE_STATS
        RptF(" - Writing %d rows", rows);
#endif
      }
# endif // _XBOX_VER >= 200
#endif // _XBOX_SECURE && _ENABLE_MP
      break;
    case NMTAcceptedKey:
      {
        int index = _acceptedKeys.Add();
        AcceptedKeyMessage ask(_acceptedKeys[index]);
        TMError err = ask.TransferMsg(ctx); 
        if (err != TMOK && err != TMNotFound) 
        {
          _acceptedKeys.Delete(index, 1);
          break;
        }
      }
      break;
    case NMTAdditionalSignedFiles:
      {
        // TODO: protect _additionalSignedFiles against manipulation by hacker
        AdditionalSignedFilesMessage files(_additionalSignedFiles);
        TMSGCHECK(files.TransferMsg(ctx))
      }
      break;
    case NMTDataSignatureAsk:
      {
        DataSignatureAskMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        /*
        if (IsDedicatedClient())
        {
          ConsoleF("Data signature ask %d level %d",ask._index, ask._level);
        }
        */
        // indices of banks with configs (only these need to be checked)
        AUTO_STATIC_ARRAY(int, indices, 32);
        BankList::ReadAccess banks(GFileBanks);
        for (int i=0; i<banks.Size(); i++)
        {
          if (banks[i].VerifySignature()) indices.Add(i);
        }
        int nBanks = indices.Size(); // banks
        int nFiles = _additionalSignedFiles.Size(); // additional files

        if (ask._index < 0)
        {
          // GDebugger.PauseCheckingAlive();
          Ref<ProgressHandle> p = ProgressStartExt(true, LocalizeString(IDS_CREATE_CLIENT));
          // check all files
          AutoArray<RString> wrongFiles;
          // check the banks
          for (int i=0; i<nBanks; i++)
          {
            ProgressRefresh();
            QFBank &bank = banks[indices[i]];
            if (!CheckDataSignature(ask._index, ask._level, bank, indices[i]))
            {
              wrongFiles.Add(bank.GetOpenName());
            }
          }
          // check the additional file
          for (int i=0; i<nFiles; i++)
          {
            ProgressRefresh();
            RString filename = _additionalSignedFiles[i];
            if (!CheckDataSignature(ask._index, ask._level, filename))
            {
              wrongFiles.Add(filename);
            }
          }
          ProgressFinish(p);
          // GDebugger.ResumeCheckingAlive();
          if (wrongFiles.Size() > 0)
          {
            RString format = LocalizeString(IDS_SIGNATURE_MISSING);
            const int maxFiles = 6;
            RString files = RString("\"") + wrongFiles[0];
            int n = std::min(wrongFiles.Size(), maxFiles);
            for (int i=1; i<n; i++)
            {
              files = files + RString("\", \"") + wrongFiles[i];
            }
            files = files + RString("\"");
            if (wrongFiles.Size() > maxFiles) files = files + RString(", ...");
            WarningMessage(format, cc_cast(files));

            // let server decide if need to disconnect
            for (int i=0; i<wrongFiles.Size(); i++)
            {
              HackedDataMessage msg;
              msg._type = HTUnsignedData;
              msg._filename = wrongFiles[i];
              SendMsg(&msg, NMFGuaranteed);
            }

            ResetErrors();
          }
          else if (ask._level == 1)
          {
            GWorld->CheckHash(-1, CHRMultiplayer);
          }
          // if it is data signatures collecting phase, so send the end packet
          if ( ask._index == -2 )
          {
            //@{ Ant1HaX0R section
            // Send the Loaded Bank Count
            // hidden inside the NMTChat ... channel = ( #signed files xor (dpnid & 0x7fffffff) ) | 0x80000000
            int nSignedBanks = 0;
            for (int i=0; i<banks.Size(); i++) 
            {
              if (banks[i].VerifySignature()) nSignedBanks++;
            }
            int nFiles = _additionalSignedFiles.Size() + nSignedBanks;
            int fakeChannel = ((_player & 0x7fffffff) ^ nFiles) | 0x80000000;
            GetNetworkManager().Chat((ChatChannel)fakeChannel,"2"); // I know level 2
            //@}

            ask._index = CollectedHashes::CollectEndIndex;
            ProgressFinish(p);
            goto CheckSingleSignature;
          }
        }
        else
        {
CheckSingleSignature:
          // random check
          int i = ask._index % (nBanks + nFiles);
          if (i < nBanks)
          {
            // check the bank
            QFBank &bank = banks[indices[i]];
            if (!CheckDataSignature(ask._index, ask._level, bank, indices[i]))
            {
              HackedDataMessage msg;
              msg._type = HTHackedData;
              msg._filename = bank.GetOpenName();
              SendMsg(&msg, NMFGuaranteed);
            }
            else if (ask._level == 1)
            {
              GWorld->CheckHash(indices[i], CHRMultiplayer);
            }
          }
          else
          {
            // check the additional file
            RString filename = _additionalSignedFiles[i - nBanks];
            if (!CheckDataSignature(ask._index, ask._level, filename))
            {
              HackedDataMessage msg;
              msg._type = HTHackedData;
              msg._filename = filename;
              SendMsg(&msg, NMFGuaranteed);
            }
          }
        }
      }
      break;
    case NMTFileSignatureAsk:
      {
        FileSignatureAskMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        /*
        if (IsDedicatedClient())
        {
          ConsoleF("File signature ask '%s' level %d",cc_cast(ask._file), ask._level);
        }
        */
        // check the bank
#if _ENABLE_REPORT
        LogF("FileSignatureCheck: %s", cc_cast(ask._file));
#endif
        int bankIndex = BankIndex(ask._file);
        if ( bankIndex>=0 )
        {
          if (!CheckFileSignature( bankIndex, ask._level, ask._file))
          {
            HackedDataMessage msg;
            msg._type = HTHackedData;
            msg._filename = ask._file;
            SendMsg(&msg, NMFGuaranteed);
          }
          else if (ask._level == 1)
          {
            GWorld->CheckHash(bankIndex, CHRMultiplayer);
          }
        }
        else
        { 
          HackedDataMessage msg;
          msg._type = HTHackedData;
          msg._filename = ask._file;
          SendMsg(&msg, NMFGuaranteed);
        }
      }
      break;
    case NMTStaticObjectDestructed:
      {
        StaticObjectDestructedMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        if (ask._object)
        {
          // similar to calling Destroy
          const EntityType *destructionType = ask._object->CanDestroyUsingEffects();
          if (destructionType)
          {
            ask._object->DestroyUsingEffects(NULL,ask._immediate,false);
            ask._object->ExplodeSmoke();
          }
          if (ask._immediate)
          {
            ask._object->SetDestroyed(1.0f);
          }
          else
          {
            // initiate the explosion
            // need to set the damage later somehow?
          }
          ask._object->SetTotalDamage(1,false);
        }
      }
      break;
    case NMTCDKeyCheckAsk:
#if _ENABLE_GAMESPY && _VERIFY_KEY
      {
        CDKeyCheckAskMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        
        RString GetCDKey();
        RString cdKey = GetCDKey();
        char response[RESPONSE_SIZE];
        gcd_compute_response((char *)cc_cast(cdKey), (char *)cc_cast(ask._challenge), response, CDResponseMethod_NEWAUTH);

        CDKeyCheckAnswerMessage answer;
        answer._response = response;
        SendMsg(&answer, NMFGuaranteed);
      }
#endif
      break;
    case NMTCDKeyRecheckAsk:
#if _ENABLE_GAMESPY && _VERIFY_KEY
      {
        CDKeyRecheckAskMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))

        RString GetCDKey();
        RString cdKey = GetCDKey();
        char response[RESPONSE_SIZE];
        gcd_compute_response((char *)cc_cast(cdKey), (char *)cc_cast(ask._challenge), response, CDResponseMethod_REAUTH);

        CDKeyRecheckAnswerMessage answer;
        answer._hint = ask._hint;
        answer._response = response;
        SendMsg(&answer, NMFGuaranteed);
      }
#endif
      break;
    case NMTConnectVoiceDirect:
      {
        ConnectVoiceDirectMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        // fill in some diagnostics first
#if _ENABLE_CHEATS
        if (GInput.cheatVoNServer && ask._dpnid != BOT_CLIENT) break; //force retranslation through server
        if (GInput.cheatVoNRetranslate && ask._dpnid == BOT_CLIENT) break; //force retranslation through gameSocket
#endif
        // Connect voice
        sockaddr_in addr;
        addr.sin_port = htons(ask._port);
        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = ask._addr;
        memset(&(addr.sin_zero), '\0', 8); // zero the rest of the struct
#     if BETA_TESTERS_DEBUG_VON
        RptF("VoNLog,Client: directMsg addr=%d.%d.%d.%d port=%d id=%x", 
          (unsigned)IP4(addr),(unsigned)IP3(addr),(unsigned)IP2(addr),(unsigned)IP1(addr),
          ask._port, ask._dpnid
          );
#     endif
        ConnectVoice(ask._dpnid, addr);
      }
      break;
    case NMTConnectVoiceNatNeg:
      break;
    case NMTDisconnect:
      {
        DisconnectMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        GChatList.Add(CCSystem, NULL, ask._message, false, true);
        WarningMessage(ask._message);

        RemoveUserMessages();
        _clientState = NCSNone;
        _serverState = NSSNone;
      }
      break;
    case NMTMissionStats:
      {
        MissionStatsMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        _totalToCreate = _remainingToCreate = info._toCreate;
        _totalToUpdate = _remainingToUpdate = info._toUpdate;
      }
      break;
#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
    case NMTAskForArbitration:
      {
        AskForArbitrationMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        _parent->ArbitrationRegister(ask._sessionNonce);
      }
      break;
#endif
    case NMTServerTime:
      {
        ServerTimeMessage stmsg;
        TMSGCHECK(stmsg.TransferMsg(ctx))
        _serverTime = stmsg._time;
        _serverTimeOfEstimatedEnd = stmsg._endTime;
        _serverTimeInitTickCount = ::GetTickCount()-_serverTime; //we want _serverTime to match ::GetTickCount()-_serverTimeInitTickCount
      }
      break;
    case NMTServerAdmin:
      {
        ServerAdminMessage samsg;
        TMSGCHECK(samsg.TransferMsg(ctx))
        _serverAdmin = samsg._admin;
        _serverGameMaster = samsg._gameMaster;
      }
      break;

    case NMTAskForTeamSwitch:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        AskForTeamSwitchMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        
        bool result = ProcessTeamSwitch(ask._player, ask._from, ask._to, ask._killer, ask._respawn);
        if (result)
        {
          FinishTeamSwitchMessage answer;
          answer._player = ask._player;
          answer._from = ask._from;
          answer._fromGroup = answer._from ? answer._from->GetGroup() : NULL;
          answer._fromLeader = answer._fromGroup ? answer._fromGroup->Leader() : NULL;
          answer._to = ask._to;
          answer._killer = ask._killer;
          answer._respawn = ask._respawn;
          SendMsg(&answer, NMFGuaranteed);
          // TeamSwitchResult will be handled in reaction
        }
        else
        {
          if (ask._player == _player)
          {
            HandleTeamSwitchResult(ask._player, ask._from, ask._to, ask._killer, ask._respawn, false);
          }
          else
          {
            TeamSwitchResultMessage answer;
            answer._player = ask._player;
            answer._from = ask._from;
            answer._to = ask._to;
            answer._killer = ask._killer;
            answer._respawn = ask._respawn;
            answer._result = false;
            SendMsg(&answer, NMFGuaranteed);
          }
        }
      }
      break;
    case NMTTeamSwitchResult:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        TeamSwitchResultMessage answer;
        TMSGCHECK(answer.TransferMsg(ctx))
        HandleTeamSwitchResult(answer._player, answer._from, answer._to, answer._killer, answer._respawn, answer._result);
      }
      break;
    case NMTFinishTeamSwitch:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        FinishTeamSwitchMessage ask;
        TMSGCHECK(ask.TransferMsg(ctx))
        FinishTeamSwitch(ask._from, ask._respawn);

        // all finished, report results
        if (ask._player == _player)
        {
          HandleTeamSwitchResult(ask._player, ask._from, ask._to, ask._killer, ask._respawn, true);
        }
        else
        {
          TeamSwitchResultMessage answer;
          answer._player = ask._player;
          answer._from = ask._from;
          answer._to = ask._to;
          answer._killer = ask._killer;
          answer._respawn = ask._respawn;
          answer._result = true;
          SendMsg(&answer, NMFGuaranteed);
        }
      }
      break;
    case NMTVehMPEventHandlers:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        VehMPEventHandlersMessage info;
        TMSGCHECK(info.TransferMsg(ctx))
        VehSetMPEventHandlers(info._vehicle, (MPEntityEvent)info._eventNum, info._handlers);
      }
      break;
    case NMTVehMPEvent:
      {
        if ( !CanReceiveUpdateMessages() ) 
          break;  // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect

        VehMPEventMessage eventmsg;
        TMSGCHECK(eventmsg.TransferMsg(ctx))
        if (eventmsg._eventNum<NMPEntityEvent)
        {
          QIStrStream in(eventmsg._pars.Data(), eventmsg._pars.Size());
          GameValue value;
          if (ReadValue(value, in, _parent))
          {
            if (value.GetType() == GameArray)
            {
              const GameArrayType &array = value;
              extern Object *GetObject( GameValuePar oper );
              Object *obj = GetObject(array[0]);
              if( !obj ) break;
              EntityAI *ai=dyn_cast<EntityAI>(obj);
              if( !ai ) break;
              ai->OnMPEvent((MPEntityEvent)eventmsg._eventNum, value);
            }
          }
        }
      }
      break;
    case NMTChangeCreatorId:
      {
#if DEBUG_CREATOR_ID
        _debugCreatorIdSet = true;
#endif
        ChangeCreatorIdMessage msg;
        TMSGCHECK(msg.TransferMsg(ctx))
        SetCreatorId(msg._creator);
      }
      break;

#if defined _XBOX && _XBOX_VER >= 200
    case NMTLocPlayerInfoList:
      {
        // sign in of some user changed
        LocalPlayerInfoList localPlayerInfoList;
        TMSGCHECK(localPlayerInfoList.TransferMsg(ctx))
        UpdateLocalPlayerInfoList(localPlayerInfoList);
      }
      break;
#endif //#if defined _XBOX && _XBOX_VER >= 200
    default:
      RptF("Client: Unhandled user message %s", cc_cast(GetMsgTypeName(type)));
      break;
  }

  if (_clientState == NCSBriefingRead && validBefore)
  {
    bool validAfter = AssertAIValid();
    if (!validAfter)
    {
      DiagLogF("Client: error in structure after message %s", cc_cast(GetMsgTypeName(type)));   
      ctx.LogMessage(format, 4, "\t");
    }
  }
}

#if defined _XBOX && _XBOX_VER >= 200

void NetworkClient::UpdateLocalPlayerInfoList(const LocalPlayerInfoList &localPlayerInfoList)
{
  // current player XUID
  XUID localPlayerXUID = INVALID_XUID;
  const XUSER_SIGNIN_INFO *info = GSaveSystem.GetUserInfo();
  if (info)
    localPlayerXUID = info->xuid;

  // lets find out if the message is about local user or remote user
  bool isLocal = false;
  for (int i=0; i<localPlayerInfoList.localPlayerInfoList.Size(); ++i)
  {
    const LocalPlayerInfo &info = localPlayerInfoList.localPlayerInfoList[i];
    if (XOnlineAreUsersIdentical(info.xuid, localPlayerXUID))
    {
      isLocal = true;
      break;
    }
  }

  // lets find the identity we are updating
  PlayerIdentity *identity = NULL;

  for (int i=0; i<_identities.Size(); i++)
  {
    PlayerIdentity &id = _identities[i];
    for (int j=0; j<localPlayerInfoList.localPlayerInfoList.Size(); ++j)
    {
      const LocalPlayerInfo &info = localPlayerInfoList.localPlayerInfoList[j];
      if (XOnlineAreUsersIdentical(info.xuid,id.xuid))
      {
        identity = &id;
        break;
      }
    }

  }

  if (!identity)
    return;   // identity was not found

  void VoiceRegisterLocalTalker(int userIndex);
  void VoiceUnregisterLocalTalker(int userIndex);
  void VoiceRegisterRemoteTalker(XUID xuid);
  void VoiceUnregisterRemoteTalker(XUID xuid);


  // we have to compare new player info list with player info list in identity and add/remove talkers if needed
  for (int j=0; j<identity->localPlayerInfoList.localPlayerInfoList.Size(); ++j)
  {
    const LocalPlayerInfo &oldInfo = identity->localPlayerInfoList.localPlayerInfoList[j];
    if (INVALID_XUID == oldInfo.xuid || !oldInfo.signedToLive)
      continue;
    bool found = false;
    for (int i=0; i<localPlayerInfoList.localPlayerInfoList.Size(); ++i)
    {
      const LocalPlayerInfo &newInfo = localPlayerInfoList.localPlayerInfoList[i];
      if (INVALID_XUID == newInfo.xuid || !newInfo.signedToLive)
        continue;

      if (XOnlineAreUsersIdentical(oldInfo.xuid,newInfo.xuid))
      {
        found = true;
        break;
      }      
    }

    if (!found)
    {
      // remove talker
      if (isLocal)
        VoiceUnregisterLocalTalker(j);
      else
        VoiceUnregisterRemoteTalker(oldInfo.xuid);
    }
  }

  for (int i=0; i<localPlayerInfoList.localPlayerInfoList.Size(); ++i)
  {
    const LocalPlayerInfo &newInfo = localPlayerInfoList.localPlayerInfoList[i];
    if (INVALID_XUID == newInfo.xuid || !newInfo.signedToLive)
      continue;

    bool found = false;
    for (int j=0; j<identity->localPlayerInfoList.localPlayerInfoList.Size(); ++j)
    {
      const LocalPlayerInfo &oldInfo = identity->localPlayerInfoList.localPlayerInfoList[j];
      if (INVALID_XUID == oldInfo.xuid || !oldInfo.signedToLive)
        continue;
      if (XOnlineAreUsersIdentical(oldInfo.xuid,newInfo.xuid))
      {
        found = true;
        break;
      }      
    }

    if (!found)
    {
      // add talker
      if (isLocal)
        VoiceRegisterLocalTalker(i);
      else
        VoiceRegisterRemoteTalker(newInfo.xuid);
    }
  }

  // update identity data
  identity->localPlayerInfoList = localPlayerInfoList;
  
}
#endif

bool NetworkClient::CheckDataSignature(int askedIndex, int level, QFBank &bank, int gBankIx)
{
#if _ENABLE_CHEATS
  static bool doIt = true;
  if (doIt && bank.IsPatched()) return false;
#else
  if (bank.IsPatched()) return false;
#endif

  DataSignatureAnswerMessage answer;
  answer._index = askedIndex;
  answer._filename = bank.GetOpenName();
  answer._level = level;
  if (DataSignatures::FindSignature(answer._signature, answer._filename, _acceptedKeys.Data(), _acceptedKeys.Size(), _sigVerRequired))
  {
    if (answer._signature.Version()>1 && level==2)
    { // use async hash computation for level=2 asks
      // Add ask to be completed async
      int ix = _checkSignatureQueue.Add();
      CheckSignatureItem &check = _checkSignatureQueue[ix];
      check._sigVer = answer._signature.Version();
      check._level = level;
      check._bankIndex = gBankIx;
      check._initialTest = (askedIndex == -2);
      // prepare answer msg
      DataSignatureAnswerMessage *answer2 = new DataSignatureAnswerMessage();
      answer2->_signature = answer._signature;
      answer2->_index = askedIndex;
      answer2->_filename = bank.GetOpenName();
      answer2->_level = level;
      check._msg = answer2;
      check._type = NMTDataSignatureAnswer;
      /*
      if (IsDedicatedClient())
      {
        ConsoleF(
          "Queueing signature %d:'%s' level %d, %d pending", check._bankIndex, cc_cast(bank.GetOpenName()),
          check._level, _checkSignatureQueue.Size()
        );
      }
      */

    }
    else
    { // for other asks compute the hash directly
      DoVerify(bank.GetHash<HashCalculator>(answer._hash._content, answer._signature.Version(), level));
      SendMsg(&answer, NMFGuaranteed);
    }
    return true;
  }
  return false;
}

bool NetworkClient::CheckDataSignature(int askedIndex, int level, RString filename)
{
  DataSignatureAnswerMessage answer;
  answer._index = askedIndex;
  answer._filename = filename;
  answer._level = level;
  if (DataSignatures::FindSignature(answer._signature, answer._filename, _acceptedKeys.Data(), _acceptedKeys.Size(), _sigVerRequired))
  {
    DoVerify(DataSignatures::GetHash(answer._hash, answer._filename));
    SendMsg(&answer, NMFGuaranteed);
    return true;
  }
  return false;
}

bool NetworkClient::CheckFileSignature(int askedIndex, int level, RString filename)
{
  BankList::ReadAccess banks(GFileBanks);
  FileSignatureAnswerMessage answer;
  QFBank &bank = banks[askedIndex];
  //RptF("CheckFileSignature: %s-->(%s)", cc_cast(filename), cc_cast(bank.GetPrefix()));
  answer._fileName = filename;
  answer._level = level;
  // get the index matching the DataSignatureAnswers
  int sigIndex = -1;
  for (int i=0; i<=askedIndex; i++)
  {
    if (banks[i].VerifySignature()) sigIndex++;
  }
  answer._index = sigIndex;
  if ( DataSignatures::FindSignature(answer._signature, bank.GetOpenName(), _acceptedKeys.Data(), _acceptedKeys.Size(), _sigVerRequired) )
  {
    Temp<char> dataHash;
    if (!bank.GetHash(dataHash)) 
      return false;
    switch (answer._signature.Version())
    {
    case 1:
      {
        answer._dataToHash.Realloc( dataHash.Size() + bank.GetPrefix().GetLength() );
        char *data = answer._dataToHash.Data();
        memcpy(data, dataHash.Data(), dataHash.Size()); data += dataHash.Size();
        memcpy(data, bank.GetPrefix().Data(), bank.GetPrefix().GetLength());
      }
      break;
    case 2:
      {
        switch (level)
        {
        case 0:
        case 1:
          {
            // combine content hash with hash of pbo file list and pbo prefix
            // the final hash will be: HashOf(storedHash+fileListHash+pboPrefix)
            AutoArray<char> fileListHash;
            if (!bank.GetFileListHash<HashCalculator>(fileListHash))
              return false;
            // concatenate it to create _dataToHash
            answer._dataToHash.Realloc( dataHash.Size() + fileListHash.Size() + bank.GetPrefix().GetLength() );
            char *data = answer._dataToHash.Data();
            memcpy(data, dataHash.Data(), dataHash.Size()); data += dataHash.Size();
            memcpy(data, fileListHash.Data(), fileListHash.Size()); data += fileListHash.Size();
            memcpy(data, bank.GetPrefix().Data(), bank.GetPrefix().GetLength());
          }
          break;
        case 2:
          {
            // use async hash computation for level=2 asks
            // Add ask to be completed async
            int ix = _checkSignatureQueue.Add();
            CheckSignatureItem &check = _checkSignatureQueue[ix];
            check._sigVer = answer._signature.Version();
            check._level = level;
            check._bankIndex = askedIndex;
            check._initialTest = false;
            // prepare answer msg
            FileSignatureAnswerMessage *answer2 = new FileSignatureAnswerMessage();
            answer2->_signature = answer._signature;
            answer2->_index = answer._index;
            answer2->_fileName = answer._fileName;
            answer2->_level = answer._level;
            check._msg = answer2;
            check._type = NMTFileSignatureAnswer;
            return true;
          }
        default:
          return false; //fail
        }
      }
      break;
    default:
      return false; //fail
    }
    SendMsg(&answer, NMFGuaranteed);
    return true;
  }
  return false;
}

/*!
\patch_internal 1.80 Date 8/6/2002 by Jirka
- Fixed: Glob.header.filenameReal now always contain original name of mission (even in MP)
*/

bool NetworkClient::PrepareGame()
{
LogF("Mission %s received", (const char *)_missionHeader.GetLocalizedMissionName());

  _totalToCreate = -1;
  _remainingToCreate = -1;
  _totalToUpdate = -1;
  _remainingToUpdate = -1;

  if (_missionHeader._island.GetLength() == 0)
  {
    RptF("NetworkClient::PrepareGame - no island");
    return true;
  }
  if (!GetMyPlayerRole())
  {
    RptF("NetworkClient::PrepareGame - no role assigned");
    return true;
  }

  RString fullname = GetWorldName(_missionHeader._island);
  if (!QFBankQueryFunctions::FileExists(fullname))
  {
    // TODO: Warning and error processing
    WarningMessage(LocalizeString(IDS_MSG_NO_WORLD), (const char *)fullname);
    return false;
  }

  SetBaseDirectory(false, "");

  if (_missionHeader._noCopy)
  {
    const char *ext = strrchr(_missionHeader._fileName, '.');
    Assert(ext);
    RString mission(_missionHeader._fileName, ext - cc_cast(_missionHeader._fileName));
    if (_missionHeader._hasMissionParams)
    {
#ifdef _XBOX
      RString filename = "."; // stored in memory
#else
      RString filename = GetMissionCacheDir() + RString("__cur_mp.") + GetExtensionWizardMission();
#endif
      SetMission(_missionHeader._island, mission, _missionHeader._fileDir, filename, _missionHeader.GetLocalizedMissionName());
    }
    else
    {
      SetMission(_missionHeader._island, mission, _missionHeader._fileDir);
    }
  }
  else
  {
    if (_missionHeader._hasMissionParams)
    {
#ifdef _XBOX
      RString filename = "."; // stored in memory
#else
      RString filename = GetMissionCacheDir() + RString("__cur_mp.") + GetExtensionWizardMission();
#endif

      SetMission(_missionHeader._island, "__cur_mp", MPMissionsDir, filename, _missionHeader.GetLocalizedMissionName());
    }
    else
    {
      SetMission(_missionHeader._island, "__CUR_MP", MPMissionsDir);
      const char *ext = strrchr(_missionHeader._fileName, '.');
      if (ext) Glob.header.filenameReal = _missionHeader._fileName.Substring(0, ext - (const char *)_missionHeader._fileName);
      else Glob.header.filenameReal = _missionHeader._fileName;
    }
  }

  CurrentTemplate.Clear();
  ParseMission(true, true);
  LogF("  - parsed");

  Ref<ProgressHandle> p = GWorld->BegLoadMissionProgress(Glob.header.worldname, CurrentTemplate, GModeNetware);

  GWorld->SwitchLandscape(_missionHeader._island);
  if (!CurrentTemplate.CheckObjectIds())
  {
    WarningMessage(LocalizeString(IDS_BAD_OBJECT_ID),(const char *)Localize(CurrentTemplate.intel.briefingName));
  }

  if (GWorld->UI()) GWorld->UI()->Init();

  GStats.ClearMission();
  GWorld->ActivateAddons(CurrentTemplate.addOns,_missionHeader._island);
  GWorld->InitGeneral(CurrentTemplate.intel);
  GWorld->SetViewDistanceMin(CurrentTemplate.intel.viewDistance);
  GWorld->SetViewDistanceHard(0);
  GWorld->SetTerrainGridHard(0,false);
  GWorld->AdjustSubdivision(GModeNetware);
  GWorld->InitClient();

  // enable team switch based on mission settings
  GWorld->EnableTeamSwitch(_missionHeader._teamSwitchEnabled);

  ConstParamEntryPtr enableItemsDropping = ExtParsMission.FindEntry("enableItemsDropping");
  if (enableItemsDropping) GWorld->EnableItemsDropping(*enableItemsDropping);

  // create markers
  markersMap.Clear();
  void ResetMarkerID();
  ResetMarkerID();
  int n = CurrentTemplate.markers.Size();
  for (int i=0; i<n; i++) markersMap.Add(CurrentTemplate.markers[i]);

  ProgressFinish(p);

  return true;
}

NetworkId NetworkClient::CreateLocalObject(NetworkObject *object, bool setNetworkId)
{
  Assert(object);

  if (setNetworkId)
  {
    // set network id
    NetworkId id(GetCreatorId(), _nextId++);
    object->SetNetworkId(id);
  }
  NetworkId id = object->GetNetworkId();
  object->SetLocal(true);

  // add to local objects
#if CHECK_MSG
  CheckLocalObjects();
#endif

  NetworkLocalObjectInfo localObject;
  localObject.id = id;
  localObject.object = object;
  for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
  {
    NetworkUpdateInfo &info = localObject.updates[j];
    info.lastCreatedMsg = NULL;
    info.lastCreatedMsgId = 0xFFFFFFFF;
    info.lastCreatedMsgTime = 0;
  }
  _localObjects.put(localObject);

  if (DiagLevel >= 1)
  {
    NetworkMessageType type = object->GetNMType();
    DiagLogF("Client: local object created %d:%d (type %s)", id.creator.CreatorInt(), id.id, type == NMTNone ? "None" : cc_cast(GetMsgTypeName(type)));
  }

#if CHECK_MSG
  CheckLocalObjects();
#endif

  return id;
}

bool NetworkClient::CreateObject(NetworkObject *object, bool setNetworkId)
{
  if (!object) return false;
  if (_clientState < NCSMissionReceived)
  {
    RptF("Creation of object %s failed, state %s", (const char *)object->GetDebugName(), (const char *)FindEnumName(_clientState));
    return false;
  }

  NetworkId id = CreateLocalObject(object, setNetworkId);

  // send message to center
  NetworkMessageType msgType = object->GetNMType(NMCCreate);
/*
  NetworkMessageFormatBase *format = GetFormat(msgType);
*/

/*
if (msgType == NMTCreateAISubgroup)
{
  ErrF("Subgroup %d:%d (0x%x) created", id.creator.CreatorInt(), id.id, object);
}
*/

  // create message
  Ref<NetworkMessage> msg = ::CreateNetworkMessage(msgType);
  msg->time = Glob.time;
  NetworkMessageContext ctx(msg, this, TO_SERVER, MSG_SEND);
  ctx.SetClass(NMCCreate);
  if (object->TransferMsg(ctx) != TMOK) return false;

  // avoid uninitialized message member
  PREPARE_TRANSFER(NetworkObject)
  bool guaranteed = true;
  TRANSF_BASE(guaranteed, guaranteed);

  // send message
  DWORD dwMsgId = SendMsg
  (
  TO_SERVER, msg, msgType, NMFGuaranteed, object
  );

#if _EXT_CTRL //ext. Controller like HLA, AAR 
  if(IsBotClient())
    _hla.CreateObject(object);
#endif

  return dwMsgId != 0xFFFFFFFF;
}

/*!
\patch_internal 1.30 Date 11/5/2001 by Jirka
- Fixed: More robust transfer of guaranteed update messages
\patch 1.35 Date 12/11/2001 by Jirka
- Fixed: Error in updates on dedicated server
*/

int NetworkClient::UpdateObject(NetworkObject *object, NetworkMessageClass cls, NetMsgFlags dwFlags)
{
  if (!object) return -1;
  if (_clientState < NCSMissionReceived) return -1;

  // find local objects
  NetworkId id = object->GetNetworkId();
  if (id.IsNull())
  {
    RptF("Client: Nonnetwork object %x.", object);
    return -1;
  }
  if (!object->IsLocal())
  {
    RptF("Update of nonlocal object %d:%d called", id.creator.CreatorInt(), id.id);
    return -1;
  }
#if CHECK_MSG
  CheckLocalObjects();
#endif
  NetworkLocalObjectInfo *localObject = GetLocalObjectInfo(id);
  if (!localObject) return -1;

  NetworkUpdateInfo &info = localObject->updates[cls];

  // send message to center
  NetworkMessageType msgType = object->GetNMType(cls);
  if (msgType == NMTNone) return -1;
  
  // FIX
#if _ENABLE_DEDICATED_SERVER
  if (((msgType == NMTUpdateClientInfo) || (msgType == NMTUpdateClientInfoDpid)) && _parent->GetServer() && IsDedicatedServer()) return -1;
#endif

  // create message
  Ref<NetworkMessage> msg = ::CreateNetworkMessage(msgType);
  // must be Ref - msg is stored
//  msg->id = id;
  msg->time = Glob.time;
//  msg->position = object->GetCurrentPosition();
  NetworkMessageContext ctx(msg, this, TO_SERVER, MSG_SEND);
  ctx.SetClass(cls);
  if ((dwFlags & NMFGuaranteed) != 0) ctx.SetInitialUpdate();
  if (object->TransferMsg(ctx) != TMOK) return -1;

  PREPARE_TRANSFER(NetworkObject)
  bool guaranteed = (dwFlags & NMFGuaranteed) != 0;
  if (TRANSF_BASE(guaranteed, guaranteed) != TMOK)
  {
    Fail("Cannot transfer");
    return -1;
  }

#if CHECK_MSG
  DoAssert(localObject->object == object);
  DoAssert(localObject->id == id);
  if (object)
  {
    NetworkDataTyped<int> *creator = static_cast<NetworkDataTyped<int> *>(msg->values[0].GetRef());
    NetworkDataTyped<int> *id = static_cast<NetworkDataTyped<int> *>(msg->values[1].GetRef());
    LogF
    (
      "Message %x (%d:%d) assigned to object %x (%d:%d)",
      msg.GetRef(), creator->value, id->value,
      object, object->GetNetworkId().creator.CreatorInt(), object->GetNetworkId().id
    );
  }
#endif

  // send message
  DWORD dwMsgId = SendMsg
  (
    TO_SERVER, msg, msgType, dwFlags, object
  );
  msg->objectUpdateInfo = NULL;

  if (dwMsgId == 0xFFFFFFFF) return -1;
  // update local objects info
  else if (dwMsgId == 0)
  {
    // sent as sync message
    info.lastCreatedMsg = NULL;
    info.lastCreatedMsgId = 0xFFFFFFFF;
    info.lastCreatedMsgTime = 0;
    info.lastSentMsg = msg;
  }
  else
  {
    DoAssert(dwMsgId == 1);
    // !!! Pointer to static structure - must be process before structure changed
    msg->objectUpdateInfo = &info;
    msg->objectServerInfo = NULL;

    info.lastCreatedMsg = msg;
    info.lastCreatedMsgId = MSGID_REPLACE;
    info.lastCreatedMsgTime = ::GlobalTickCount();
    info.canCancel = (dwFlags & NMFGuaranteed) == 0;
#if LOG_SEND_PROCESS
    LogF("Client: Update info %x marked for send", &info);
#endif
  }
#if CHECK_MSG
  CheckLocalObjects();
#endif

#if _EXT_CTRL //ext. Controller like HLA, AAR 
  if(IsBotClient())
    _hla.UpdateObject(object);
#endif

  return msg->size;
}

void NetworkClient::UpdateObject(NetworkObject *object, NetMsgFlags dwFlags)
{
  if (!object) return;

  // find local objects
  NetworkId id = object->GetNetworkId();
  if (id.IsNull())
  {
    RptF("Client: Nonnetwork object %x.", object);
    return;
  }
#if CHECK_MSG
  CheckLocalObjects();

  if (id.id == 0x24)
    LogF("Here");
#endif

  NetworkLocalObjectInfo *localObject = GetLocalObjectInfo(id);
  if (!localObject) return;

  for (int cls=NMCUpdateFirst; cls<NMCUpdateN; cls++)
  {
    NetworkUpdateInfo &info = localObject->updates[cls];

    // send message to center
    NetworkMessageType msgType = object->GetNMType((NetworkMessageClass)cls);
    if (msgType == NMTNone) continue;
    if (msgType == NMTUpdateClientInfoDpid) continue;

    // create message
    Ref<NetworkMessage> msg = ::CreateNetworkMessage(msgType);
    // must be Ref - msg is stored
    msg->time = Glob.time;
    NetworkMessageContext ctx(msg, this, TO_SERVER, MSG_SEND);
    ctx.SetClass((NetworkMessageClass)cls);
    if ((dwFlags & NMFGuaranteed) != 0) ctx.SetInitialUpdate();
    if (object->TransferMsg(ctx) != TMOK) return;

    PREPARE_TRANSFER(NetworkObject)
    bool guaranteed = (dwFlags & NMFGuaranteed) != 0;
    if (TRANSF_BASE(guaranteed, guaranteed) != TMOK)
    {
      Fail("Cannot transfer");
      return;
    }

#if CHECK_MSG
  DoAssert(localObject->object == object);
  DoAssert(localObject->id == id);
  if (object)
  {
    NetworkDataTyped<int> *creator = static_cast<NetworkDataTyped<int> *>(msg->values[0].GetRef());
    NetworkDataTyped<int> *id = static_cast<NetworkDataTyped<int> *>(msg->values[1].GetRef());
    LogF
    (
      "Message %x (%d:%d) assigned to object %x (%d:%d)",
      msg.GetRef(), creator->value, id->value,
      object, object->GetNetworkId().creator.CreatorInt(), object->GetNetworkId().id
    );
  }
#endif

    // send message
    DWORD dwMsgId = SendMsg
    (
      TO_SERVER, msg, msgType, dwFlags, object
    );
    if (dwMsgId == 0xFFFFFFFF) return;
    // update local objects info
    else if (dwMsgId == 0)
    {
      // sent as sync message
      info.lastCreatedMsg = NULL;
      info.lastCreatedMsgId = 0xFFFFFFFF;
      info.lastCreatedMsgTime = 0;
      info.lastSentMsg = msg;
    }
    else
    {
      DoAssert(dwMsgId == 1);
      // !!! Pointer to static structure - must be process before structure changed
      msg->objectUpdateInfo = &info;
      msg->objectServerInfo = NULL;

      info.lastCreatedMsg = msg;
      info.lastCreatedMsgId = MSGID_REPLACE;
      info.lastCreatedMsgTime = ::GlobalTickCount();
      info.canCancel = (dwFlags & NMFGuaranteed) == 0;
#if LOG_SEND_PROCESS
      LogF("Client: Update info %x marked for send", &info);
#endif
    }
  }
#if CHECK_MSG
  CheckLocalObjects();
#endif

}

Time NetworkClient::GetEstimatedEndTime() const
{
  return _missionHeader._estimatedEndTime;
}

void NetworkClient::DestroyRemoteObject(NetworkId id)
{
#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient()) _hla.DeleteObject(id);
#endif

  NetworkRemoteObjectInfo info;
  if (_remoteObjects.removeKey(id,&info))
  {
    if (info.object) info.object->DestroyObject();
    if (DiagLevel >= 1)
      DiagLogF("Client: remote object destroyed %d:%d", id.creator.CreatorInt(), id.id);
  }
}

/// helper for SelectPlayer
class SelectPosition
{
  Vector3 &_pos;
  
  public:
  explicit SelectPosition(Vector3 &pos):_pos(pos){}
  
  __forceinline bool operator () (Entity *obj) const
  {
    _pos = obj->WorldPosition(obj->FutureVisualState());
    return true; /// break on first vehicle
  }
};

/*!
\patch_internal 1.31 Date 11/08/2001 by Jirka
- Fixed: player's vehicle and group ownership is updated immediately when player is selected
*/
void NetworkClient::SelectPlayer(int player, Person *person, bool respawn)
{
LogF("Client: SelectPlayer %x, person %s", player, person ? (const char *)person->GetDebugName() : "<null>");

  Vector3 pos = VZero;

  // send message to server
  if (person)
  {
    // synchronize information on server (ChangeOwner will be performed - so send actual information last time)
    if (person->IsLocal())
    {
      // FIX: wrong mission end detection on DS
      if (player == _player && person->Brain() && person->Brain()->GetLifeState() != LifeStateDead)
      {
        GWorld->SetRealPlayer(person);
      }

      UpdateObject(person, NMFGuaranteed);
      AIBrain *agent = person->Brain();
      if (agent)
      {
        DoAssert(agent->IsLocal());
        UpdateObject(agent, NMFGuaranteed);

        // update roles
        int roleIx = agent->GetRoleIndex();
        if (roleIx>=0 && roleIx<_playerRoles.Size())
        {
          AssignPlayer(roleIx, player, _playerRoles[roleIx].flags, true); //only update
        }

        Transport *veh = agent->GetVehicleIn();
        if (veh)
        {
          if (veh->IsLocal())
            UpdateObject(veh, NMFGuaranteed);
          else
            LogF("SelectPlayer - local player in remote vehicle");
        }
        
        AIUnit *unit = agent->GetUnit();
        if (unit)
        {
          AISubgroup *subgrp = unit->GetSubgroup();
          if (subgrp)
          {
            if (subgrp->IsLocal())
              UpdateObject(subgrp, NMFGuaranteed);
            else
              LogF("SelectPlayer - local player in remote subgroup");
          }

          AIGroup *grp = subgrp ? subgrp->GetGroup() : NULL;
          if (grp)
          {
            if (grp->IsLocal())
              UpdateObject(grp, NMFGuaranteed);
            else
              LogF("SelectPlayer - local player in remote group");
          }
        }
      }
    }
    else
    {
      RptF("SelectPlayer - on remote person");
    }

    pos = person->FutureVisualState().Position();

    NetworkId id = person->GetNetworkId();
    SelectPlayerMessage msg;
    msg._player = player;
    msg._creator = id.creator;
    msg._id = id.id;
    msg._position = pos;
    msg._respawn = respawn;
    SendMsg(&msg, NMFGuaranteed);
  }
  else
  {
    GWorld->ForEachVehicle(SelectPosition(pos));

    SelectPlayerMessage msg;
    msg._player = player;
    msg._creator = CreatorId(0);
    msg._id = 0;
    msg._position = pos;
    msg._respawn = respawn;
    SendMsg(&msg, NMFGuaranteed);
  }

  if (player == _player) ProcessSelectPlayer(person, respawn, pos);
}

void NetworkClient::SendSelectPlayer(int player, Person *person)
{
  // send message to server
  if (person)
  {
    Vector3 pos = person->FutureVisualState().Position();
    NetworkId id = person->GetNetworkId();
    SelectPlayerMessage msg;
    msg._player = player;
    msg._creator = id.creator;
    msg._id = id.id;
    msg._position = pos;
    msg._respawn = false;
    SendMsg(&msg, NMFGuaranteed);
  }
}

void NetworkClient::ProcessSelectPlayer(Person *person, bool respawn, Vector3Par pos)
{
LogF
(
  "Client: ProcessSelectPlayer %s (%d:%d)",
  person ? (const char *)person->GetDebugName() : "<null>",
  person ? person->GetNetworkId().creator.CreatorInt() : 0,
  person ? person->GetNetworkId().id : 0
);

  if (person && person->Brain() && person->Brain()->GetLifeState() != LifeStateDead) // alive or in respawn
  {
    AIBrain *unit = person->Brain();

    GWorld->SwitchCameraTo(unit->GetVehicle(), CamInternal, true);
    GWorld->SetPlayerManual(true);
    // old player is now in _playerOn
    GWorld->SwitchPlayerTo(person);
    GWorld->SetRealPlayer(person);
    GWorld->UI()->ResetVehicle(unit->GetVehicle());

    // set playerSide (for function playerSide)
    AIGroup *grp = unit->GetGroup();
    AICenter *center = grp ? grp->GetCenter() : NULL;
    if (center) Glob.header.playerSide = center->GetSide();

    // ??? respawn animation script
    if (respawn)
    {
      RStringB GetScriptsPath();
      RString name = Pars >> "playerResurrectScript";
      if (QFBankQueryFunctions::FileExists(GetScriptsPath() + name))
      {
        GameArrayType arguments;
        arguments.Add(GameValueExt(person));
        Script *script = new Script(name, GameValue(arguments), GWorld->GetMissionNamespace()); // mission namespace
        GWorld->StartCameraScript(script);
      }
    }
LogF("  selecting %s person", (const char *)FindEnumName(person->Brain()->GetLifeState()));
  }
  else
  {
    if (person)
    {
      if (!person->Brain())
      {
RptF("  error - person has no brain");
      }
      else
      {
RptF("  error - brain in state %s", (const char *)FindEnumName(person->Brain()->GetLifeState()));
      }
    }
    // create seagull
    SeaGullAuto *seagull = dyn_cast<SeaGullAuto>
    (
      GWorld->NewNonAIVehicleWithID("seagull", RString())
    );
    if (seagull)
    {
      Vector3 position = pos + 20.0f * VUp;
      DoAssert(position.IsFinite());
      seagull->SetPosition(position);
      seagull->SetManual(true);
      seagull->MakeAirborne(20);
      seagull->SetPlayer(GetPlayer());
      GWorld->AddAnimal(seagull); // insert vehicle to both landscape and world
      GetNetworkManager().CreateVehicle(seagull, VLTAnimal, "", -1);
      GWorld->SwitchCameraTo(seagull, CamExternal, true);
    }
    if (person)
      LogF("  creating seagull (brain %x, life state %s)", person->Brain(), person->Brain() ? (const char *)FindEnumName(person->Brain()->GetLifeState()) : "none");
    else
      RptF("  creating seagull (no person)");
  }

  if (!respawn)
  {
    // center map
    AbstractOptionsUI *map = GWorld->Map();
    if (map) map->InitHUD();
  }
}

void NetworkClient::AttachPerson(Person *person)
{
  AttachPersonMessage msg;
  msg._person = person;
  msg._unit = person->Brain();
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::PlaySound
(
  RString name, Vector3Par position, Vector3Par speed,
  float volume, float freq, AbstractWave *wave
)
{
  if (_clientState != NCSBriefingRead) return;  // send event messages only when playing

  PlaySoundMessage msg;
  msg._name = name;
  msg._position = position;
  msg._speed = speed;
  msg._volume = volume;
  msg._frequency = freq;
  msg._soundId = _soundId++;
  msg._creator = GetCreatorId();
  SendMsg(&msg, NMFNone);

  // compact _sentSounds
  for (int i=0; i<_sentSounds.Size();)
  {
    PlaySoundInfo &info = _sentSounds[i];
    if (info.wave) i++;
    else _sentSounds.Delete(i);
  }

  int index = _sentSounds.Add();
  _sentSounds[index].creator = msg._creator;
  _sentSounds[index].id = msg._soundId;
  _sentSounds[index].wave = wave;
}

void NetworkClient::SoundState
(
  AbstractWave *wave, SoundStateType state
)
{
  if (_clientState != NCSBriefingRead) return;  // send event messages only when playing

  CreatorId creator = CreatorId(-1);
  int id = -1;
  for (int i=0; i<_sentSounds.Size();)
  {
    PlaySoundInfo &info = _sentSounds[i];
    if (!info.wave)
    {
      _sentSounds.Delete(i);
      continue;
    }
    if (info.wave == wave)
    {
      creator = info.creator;
      id = info.id;
    }
    i++;
  }
  
  if (id < 0) return;

  SoundStateMessage msg;
  msg._state = state;
  msg._creator = creator;
  msg._soundId = id;
  SendMsg(&msg, NMFNone);
}

/*!
\patch 5234 Date 2/28/2008 by Jirka
- Fixed: For players in driver and gunner position of the vehicle, obsolete network transfer was generated
*/

/// functor for update object traversal
class NetworkClientUpdateTurret : public ITurretFunc
{
protected:
  NetworkClient *_client;

public:
  NetworkClientUpdateTurret(NetworkClient *client) : _client(client) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turret) _client->UpdateObject(context._turret, NMFGuaranteed);
    return false; // continue
  }
};

/// functor for update object traversal
class NetworkClientUpdateObject
{
protected:
  NetworkClient *_client;

public:
  NetworkClientUpdateObject(NetworkClient *client) : _client(client) {}
  bool operator () (Entity *veh) const
  {
    if (veh->GetNetworkId().IsNull()) return false; // there can be craters,smokes,etc. after MP Load, skip them
    _client->UpdateObject(veh, NMFGuaranteed);
    EntityAIFull *vehAI = dyn_cast<EntityAIFull>(veh);
    if (vehAI)
    {
      NetworkClientUpdateTurret func(_client);
      vehAI->ForEachTurret(func);
    }
    return false;
  }
};

void NetworkClient::CreateAllObjects()
{
  // send the info about network objects need to be created
  MissionStatsMessage stats;
  stats._toCreate = 0;
  stats._toUpdate = _localObjects.card();
  SendMsg(&stats, NMFGuaranteed);

  // pass 2
  GWorld->ForEachVehicle(NetworkClientUpdateObject(this));
  GWorld->ForEachAnimal(NetworkClientUpdateObject(this));
  GWorld->ForEachSlowVehicle(NetworkClientUpdateObject(this));
  GWorld->ForEachCloudlet(NetworkClientUpdateObject(this));
  GWorld->ForEachFastVehicle(NetworkClientUpdateObject(this));
  GWorld->ForEachOutVehicle(NetworkClientUpdateObject(this));
  
  // AI structure
  AICenter *center = GWorld->GetEastCenter();
  if (center) UpdateCenter(center);
  center = GWorld->GetWestCenter();
  if (center) UpdateCenter(center);
  center = GWorld->GetGuerrilaCenter();
  if (center) UpdateCenter(center);
  center = GWorld->GetCivilianCenter();
  if (center) UpdateCenter(center);
  center = GWorld->GetLogicCenter();
  if (center) UpdateCenter(center);

#if _ENABLE_INDEPENDENT_AGENTS
  GWorld->UpdateTeamMembers();
#endif

  ClientStateMessage msg;
  msg._clientState = NCSGameLoaded;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::DestroyAllObjects()
{
LogF("Client: Destroying network objects database");
/*
  RefArray<AIStatsMPRow> &table = GStats._mission._tableMP;
  for (int i=0; i<table.Size(); i++)
    if (table[i]->_dpnid == GetPlayer())
    {
      NetworkId nid=table[i]->GetNetworkId();
      DeleteObject(nid);
      break;
    }
*/

  // avoid respawn after mission finished
  _respawnQueue.Clear();

  // declare all objects as remote
  for (LocalObjectsContainer::Iterator it=_localObjects.begin(); it; _localObjects.next(it))
  {
    NetworkLocalObjectInfo &oInfo = *it;
    if (oInfo.object) oInfo.object->SetLocal(false);
  }
  _localObjects.reset();
  _remoteObjects.reset();
  if (DiagLevel >= 1)
    DiagLogF("Client: all objects destroyed");

  // avoid id duplicity at next missions
  // _nextId = 0;
}

bool NetworkClient::CreateVehicle(Vehicle *veh, VehicleListType list, RString name, int idVeh)
{
  if (!veh) return false;
  if (_clientState < NCSMissionReceived) return false;

  NetworkId id = CreateLocalObject(veh);

  // send message to center
  NetworkMessageType msgType = veh->GetNMType(NMCCreate);

  // create message
  Ref<NetworkMessage> msg = ::CreateNetworkMessage(msgType);
  msg->time = Glob.time;
  NetworkMessageContext ctx(msg, this, TO_SERVER, MSG_SEND);
  ctx.SetClass(NMCCreate);

  // difference from CreateObject - vehicle does known its list, name, id:
  DoAssert(dynamic_cast<NetworkMessageCreateVehicle *>(ctx.GetNetworkMessage()));
  PREPARE_TRANSFER(CreateVehicle)

  if (TRANSF_BASE(list, (int &)list) != TMOK) return false;
  if (TRANSF_BASE(name, name) != TMOK) return false;
  if (TRANSF_BASE(idVehicle, idVeh) != TMOK) return false;

  // avoid uninitialized message member
  bool guaranteed = true;
  TRANSF_BASE(guaranteed, guaranteed);

  if (veh->TransferMsg(ctx) != TMOK) return false;

  // send message
  DWORD dwMsgId = SendMsg
  (
    TO_SERVER, msg, msgType, NMFGuaranteed, veh
  );

  // create subobjects (turrets etc.)
  veh->CreateSubobjects();

  #if _EXT_CTRL //ext. Controller like HLA, AAR
    if(IsBotClient())
      _hla.CreateVehicle(id, veh, name, idVeh);
  #endif

  return dwMsgId != 0xFFFFFFFF;
}

bool NetworkClient::CreateSubobject(Vehicle *veh, TurretPath &path, NetworkObject *object, bool setNetworkId)
{
  Assert(veh);
  Assert(object);
  if (_clientState < NCSMissionReceived) return false;

  NetworkId id = CreateLocalObject(object, setNetworkId);

  // send message to center
  NetworkMessageType msgType = object->GetNMType(NMCCreate);

  // create message
  Ref<NetworkMessage> msg = ::CreateNetworkMessage(msgType);
  msg->time = Glob.time;
  NetworkMessageContext ctx(msg, this, TO_SERVER, MSG_SEND);
  ctx.SetClass(NMCCreate);

  // difference from CreateObject - object does not known his owner
  // TODO: generalization - other subobjects
  PREPARE_TRANSFER(CreateTurret)

  Ref<Entity> vehicle = veh;
  if (TRANSF_REF_BASE(vehicle, vehicle) != TMOK) return false;
  if (TRANSF_BASE(path, path) != TMOK) return false;
  if (object->TransferMsg(ctx) != TMOK) return false;

  // avoid uninitialized message member
  bool guaranteed = true;
  TRANSF_BASE(guaranteed, guaranteed);

  // send message
  DWORD dwMsgId = SendMsg
  (
    TO_SERVER, msg, msgType, NMFGuaranteed, object
  );

  return dwMsgId != 0xFFFFFFFF;
}

bool NetworkClient::CreateCommand(AISubgroup *subgrp, Command *cmd, bool enqueued, bool setNetworkId)
{
  if (_clientState < NCSMissionReceived) return false;

  NetworkId id = CreateLocalObject(cmd, setNetworkId);

  // send message to center
  NetworkMessageType msgType = cmd->GetNMType(NMCCreate);

  // create message
  Ref<NetworkMessage> msg = ::CreateNetworkMessage(msgType);
  msg->time = Glob.time;
  NetworkMessageContext ctx(msg, this, TO_SERVER, MSG_SEND);
  ctx.SetClass(NMCCreate);

  // difference from CreateObject - command doesnot known its subgrp, index
  PREPARE_TRANSFER(CreateCommand)

  if (TRANSF_PTR_REF_BASE(subgroup, subgrp) != TMOK) return false;
  if (TRANSF_BASE(enqueued, enqueued) != TMOK) return false;
  if (cmd->TransferMsg(ctx) != TMOK) return false;

  // avoid uninitialized message member
  bool guaranteed = true;
  TRANSF_BASE(guaranteed, guaranteed);

  // send message
  DWORD dwMsgId = SendMsg
  (
    TO_SERVER, msg, msgType, NMFGuaranteed, cmd
  );
  return dwMsgId != 0xFFFFFFFF;
}

void NetworkClient::DeleteCommand(AISubgroup *subgrp, Command *cmd)
{
  const NetworkId &id = cmd->GetNetworkId();
  if (id.IsNull()) return;

  DeleteCommandMessage msg;
  msg._creator = id.creator;
  msg._id = id.id;
  msg._subgrp = subgrp;
  SendMsg(&msg, NMFGuaranteed);

#if CHECK_MSG
  CheckLocalObjects();
#endif

  if (_localObjects.removeKey(id))
  {
    if (DiagLevel >= 1)
      DiagLogF("Client: command deleted %d:%d", id.creator.CreatorInt(), id.id);
    return;
  }
  LogF("Warning: Client: Command info %d:%d not found.", id.creator.CreatorInt(), id.id);

#if CHECK_MSG
  CheckLocalObjects();
#endif
}

bool NetworkClient::CreateCenter(AICenter *center)
{
  Assert(center);
  if (!CreateObject(center)) return false;

  for (int g=0; g<center->NGroups(); g++)
  {
    AIGroup *grp = center->GetGroup(g);
    if (!grp) continue;
    if (!CreateObject(grp)) return false;
    for (int s=0; s<grp->NSubgroups(); s++)
    {
      AISubgroup *subgrp = grp->GetSubgroup(s);
      if (!subgrp) continue;
      if (!CreateObject(subgrp)) return false;
      for (int u=0; u<subgrp->NUnits(); u++)
      {
        AIUnit *unit = subgrp->GetUnit(u);
        if (!unit) continue;
        if (!CreateObject(unit)) return false;
      }
    }
  }
  return true;
}

bool NetworkClient::UpdateCenter(AICenter *center)
{
  // order - units, subgroups, groups, center
  // because of ownership changes
  Assert(center);
  for (int g=0; g<center->NGroups(); g++)
  {
    AIGroup *grp = center->GetGroup(g);
    if (!grp) continue;
    for (int s=0; s<grp->NSubgroups(); s++)
    {
      AISubgroup *subgrp = grp->GetSubgroup(s);
      if (!subgrp) continue;
      for (int u=0; u<subgrp->NUnits(); u++)
      {
        AIUnit *unit = subgrp->GetUnit(u);
        if (!unit) continue;
//        if (UpdateObject(unit, NMFGuaranteed) < 0) return false;
        UpdateObject(unit, NMFGuaranteed);
      }
//      if (UpdateObject(subgrp, NMFGuaranteed) < 0) return false;
      UpdateObject(subgrp, NMFGuaranteed);
    }
//    if (UpdateObject(grp, NMFGuaranteed) < 0) return false;
    UpdateObject(grp, NMFGuaranteed);
  }
//  if (UpdateObject(center, NMFGuaranteed) < 0) return false;
  UpdateObject(center, NMFGuaranteed);

  for (int g=0; g<center->NGroups(); g++)
  {
    AIGroup *grp = center->GetGroup(g);
    if (!grp) continue;
    for (int s=0; s<grp->NSubgroups(); s++)
    {
      AISubgroup *subgrp = grp->GetSubgroup(s);
      if (!subgrp) continue;
      for (int u=0; u<subgrp->NUnits(); u++)
      {
        AIUnit *unit = subgrp->GetUnit(u);
        if (!unit) continue;

        int player = unit->GetPerson()->GetRemotePlayer();
        if (player != 1) SelectPlayer(player, unit->GetPerson());
      }
    }
  }
  
  return true;
}

//! Maps text form of Network Command to Network Command Type
struct NetworkCommand
{
  //! text form
  const char *command;
  //! Network Command Type 
  NetworkCommandType type;
};

//! Maps text form of Network Commands to Network Command Types
/*!
\patch 1.16 Date 08/10/2001 by Jirka
- Added: Dedicated server command #shutdown
- Added: Dedicated server command #reassign
\patch 1.22 Date 8/24/2001 by Jirka
- Added: Dedicated server command #monitor
\patch 1.24 Date 9/19/2001 by Jirka
- Added: Dedicated server command #userlist
- Added: Dedicated server command #kick <number = player id>
\patch 1.24 Date 9/20/2001 by Jirka
- Added: Voting system (command #vote ...)
Possible vote commands: kick, restart, reassign, mission, missions
\patch 1.33 Date 11/29/2001 by Jirka
- Added: Dedicated server command #vote admin
\patch 1.34 Date 12/06/2001 by Jirka
- Added: Dedicated server command #init (reload server config file)
\patch 1.87 Date 10/17/2002 by Jirka
- Added: Dedicated server command #debug (console, totalSent, userSent)
(Requires 1.87 client, client output sent to OS using OuputDebugString)
*/

static NetworkCommand NetworkCommands[] =
{
  {"login", CMDLogin},
  {"logout", CMDLogout},
  {"kick", CMDKick},
  {"restart", CMDRestart},
  {"mission", CMDMission},
  {"missions", CMDMissions},
  {"shutdown", CMDShutdown},
  {"reassign", CMDReassign},
  {"monitor", CMDMonitor},
  {"userlist", CMDUserlist},
  {"vote", CMDVote},
  {"admin", CMDAdmin},
  {"init", CMDInit},
  {"debug", CMDDebug},
  {"exec", CMDExec},
  {"lock", CMDLock},
  {"unlock", CMDUnlock},  
  {"beserver", CMDBEServer},
  {"beclient", CMDBEClient},
#if _VBS3
  {"exitall", CMDExitAll},
#endif
};

//! Find Network Command Type for text form of Network Command
/*!
\param beg text form of Command
\param len length of text
*/
static NetworkCommandType FindCommandType(const char *beg, int len)
{
  for (int i=0; i<sizeof(NetworkCommands)/sizeof(NetworkCommand); i++)
  {
    NetworkCommand &item = NetworkCommands[i];
    if (strlen(item.command) == len && strnicmp(beg, item.command, len) == 0)
    {
      return item.type;
    }
  }
  return CMDNone;
}

//! Find DirectPlay ID of player for text form of player
/*!
\param name text form of player - player name or (simple) player id
\param identities player identities to search in
*/
static int FindPlayerId(const char *name, AutoArray<PlayerIdentity> &identities)
{
  for (int i=0; i<identities.Size(); i++)
  {
    if (stricmp(name, identities[i].name) == 0)
      return identities[i].dpnid;
  }
  int playerid = atoi(name);
  for (int i=0; i<identities.Size(); i++)
  {
    if (identities[i].playerid == playerid)
      return identities[i].dpnid;
  }
  return AI_PLAYER; // not found
}
/*!
\patch 1.33 Date 12/03/2001 by Ondra
- New: Admin / Server indication in list of players.
*/
RString GetIdentityText(const PlayerIdentity &identity)
{
  RString spec;
#ifndef _XBOX
  spec = Format("%d ms",identity._avgPing);
#endif
  if (identity._rights&PRServer)
  {
    spec = LocalizeString(IDS_MP_SERVER);
  }
  else if (identity._rights&(PRAdmin|PRVotedAdmin))
  {
    if (identity._rights&PRVotedAdmin)
    {
#ifdef _XBOX
      spec = LocalizeString(IDS_MP_MASTER);
#else
      spec = spec + RString(", ")+LocalizeString(IDS_MP_MASTER);
#endif
    }
    else
    {
#ifdef _XBOX
      spec = RString("*")+LocalizeString(IDS_MP_MASTER)+RString("*");
#else
      spec = spec + RString(", *")+LocalizeString(IDS_MP_MASTER)+RString("*");
#endif
    }
  }
  if (spec.GetLength() > 0)
    return Format("%s (%s)",(const char *)identity.name,(const char *)spec);
  else
    return identity.name;
}

/*!
\patch 1.25 Date 10/01/2001 by Jirka
- Added: Enable kick off player from MPPlayers display also by gamemaster on dedicated server.
*/
void NetworkClient::SendKick(int player)
{
  NetworkCommandMessage msg;
  msg._type = NCMTKick;
  msg._content.Write(&player, sizeof(int));
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::SendLockSession(bool lock)
{
  NetworkCommandMessage msg;
  msg._type = NCMTLockSession;
  msg._content.Write(&lock, sizeof(bool));
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::Disconnect(RString message)
{
  RemoveUserMessages();

  _clientState = NCSNone;
  _serverState = NSSNone;

  const PlayerIdentity *id = FindIdentity(_player);
  if (id)
  {
    RString senderName = id->name;
    GChatList.Add(CCSystem, senderName, message, false, true);
    RefArray<NetworkObject> dummy;
    if (!ChannelDisabled(CCSystem))
      GNetworkManager.Chat(CCSystem,senderName,dummy,message);
  }
  else
  {
    GChatList.Add(CCSystem, NULL, message, false, true);
    WarningMessage(message);
  }
  // BattlEye integration
#if _USE_BATTL_EYE_CLIENT
  if (_player != BOT_CLIENT || !IsDedicatedServer()) // DS bot client does not represent any player
  {
    _beIntegration.Done();
  }
#endif
}

bool NetworkClient::IsCommandAvailable(RString command)
{
  const char *beg = command;
  if (*beg != '#') return false;
  beg++;

  const char *end = strchr(beg, ' ');
  int len = end ? end - beg : strlen(beg);
  NetworkCommandType type = FindCommandType(beg, len);

  return IsCommandAvailable(type);
}

bool NetworkClient::IsCommandAvailable(NetworkCommandType type)
{
  switch (type)
  {
  // NOT AVAILABLE
  case CMDNone:  
  case CMDAdmin: 
    return false;
    break;
  // ALWAYS AVAILABLE
  case CMDLogin: 
  case CMDUserlist:
#if _USE_BATTL_EYE_CLIENT
  case CMDBEClient:
#endif
#if _VBS3
  case CMDExitAll:
#endif
    return true;
    break;
  // There is no admin on the server, so voting is possible
  case CMDVote:
    return _serverGameMaster == AI_PLAYER;
    break;
  // ADMIN OR SERVER
  case CMDKick:
  case CMDDebug:
    return (_gameMaster || GetNetworkManager().IsServer());
  // Logged in ADMINs or server
  case CMDLock: 
  case CMDUnlock:
    return ((_gameMaster && !_admin)|| GetNetworkManager().IsServer());
  // ADMINS ONLY
  // Voted
  case CMDLogout:
  case CMDRestart: 
  case CMDMission:
  case CMDMissions:
  case CMDReassign:
  case CMDMonitor: 
  case CMDInit: 
    return _gameMaster;
  // Logged in admins
  case CMDShutdown:
  case CMDExec:
  case CMDBEServer:
    return (_gameMaster && !_admin);
  }
  return false;
}

/*!
\patch 5180 Date 12/21/2007 by Ondra
- Fixed: Disabled player ID in non-admin #userlist.
*/

bool NetworkClient::ProcessCommand(RString command)
{
  const char *beg = command;
  if (*beg != '#') return false;
  beg++;

  const char *end = strchr(beg, ' ');
  int len = end ? end - beg : strlen(beg);
  NetworkCommandType type = FindCommandType(beg, len);
  beg += len;
  while (*beg == ' ') beg++;

  if ( IsCommandAvailable(type) || IsBotClient() )
  {
  switch (type)
  {
    case CMDNone:
    case CMDAdmin:
      break;
    case CMDLogin:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTLogin;
        msg._content.WriteString(beg);
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDLogout:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTLogout;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDKick:
      {
        int id = FindPlayerId(beg, _identities);
        if (id != AI_PLAYER) SendKick(id);
      }
      break;
    case CMDLock:
      SendLockSession(true);
      break;
    case CMDUnlock:
      SendLockSession(false);
      break;
    case CMDRestart:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTRestart;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDMission:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTMission;
        msg._content.WriteString(beg);
        msg._content.WriteString(Glob.config.diffNames.GetName(Glob.config.diffDefault));
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDMissions:
      {
        // _selectMission = true;
        NetworkCommandMessage msg;
        msg._type = NCMTMissions;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDShutdown:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTShutdown;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDReassign:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTReassign;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDMonitor:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTMonitorAsk;
        float value = 10.0f;
        if (*beg) value = atof(beg);
        msg._content.Write(&value, sizeof(value));
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDUserlist:
      for (int i=0; i<_identities.Size(); i++)
      {
        const PlayerIdentity &identity = _identities[i];
        BString<256> buffer;
        if (GetNetworkManager().IsGameMaster() && !GetNetworkManager().IsAdmin() || GetNetworkManager().IsServer())
        {
          sprintf(
            buffer, "%d: %s (id = %s%s), version %d.%d.%d",
            identity.playerid,cc_cast(GetIdentityText(identity)),cc_cast(identity.id),identity.certificate.Size()>0 ? "" : " ?",
            identity.version/100,identity.version%100,identity.build
          );
        }
        else
        {
          sprintf(
            buffer, "%d: %s",
            identity.playerid,(const char *)GetIdentityText(identity)
          );
        }
        GChatList.Add(CCSystem, NULL, buffer.cstr(), false, true);
      }
      break;
    case CMDInit:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTInit;
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDDebug:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTDebugAsk;
        msg._content.WriteString(beg);
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDVote:
      {
        end = strchr(beg, ' ');
        len = end ? end - beg : strlen(beg);
        type = FindCommandType(beg, len);
        beg += len;
        while (*beg == ' ') beg++;

        switch (type)
        {
          case CMDKick:
            {
              int id = FindPlayerId(beg, _identities);
              if (id != AI_PLAYER)
              {
                NetworkCommandMessage msg;
                msg._type = NCMTVote;
                int subtype = NCMTKick;
                msg._content.Write(&subtype, sizeof(int));
                msg._content.Write(&id, sizeof(int));
                SendMsg(&msg, NMFGuaranteed);
              }
            }
            break;
          case CMDRestart:
            {
              NetworkCommandMessage msg;
              msg._type = NCMTVote;
              int subtype = NCMTRestart;
              msg._content.Write(&subtype, sizeof(int));
              SendMsg(&msg, NMFGuaranteed);
            }
            break;
          case CMDReassign:
            {
              NetworkCommandMessage msg;
              msg._type = NCMTVote;
              int subtype = NCMTReassign;
              msg._content.Write(&subtype, sizeof(int));
              SendMsg(&msg, NMFGuaranteed);
            }
            break;
          case CMDMission:
            {
              VoteMissionMessage msg;
              RString name = beg;
              name.Lower();
              msg._name = name;
              msg._diff = Glob.config.diffNames.GetName(Glob.config.diffDefault);
              SendMsg(&msg, NMFGuaranteed);
            }
            break;
          case CMDMissions:
            {
              NetworkCommandMessage msg;
              msg._type = NCMTVote;
              int subtype = NCMTMissions;
              msg._content.Write(&subtype, sizeof(int));
              SendMsg(&msg, NMFGuaranteed);
            }
            break;
          case CMDAdmin:
            {
              int id = FindPlayerId(beg, _identities);
              if (id != AI_PLAYER)
              {
                NetworkCommandMessage msg;
                msg._type = NCMTVote;
                int subtype = NCMTAdmin;
                msg._content.Write(&subtype, sizeof(int));
                msg._content.Write(&id, sizeof(int));
                SendMsg(&msg, NMFGuaranteed);
              }
            }
            break;
        }
      }
      break;
    case CMDExec:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTExec;
        msg._content.WriteString(beg);
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
    case CMDBEServer:
      {
        NetworkCommandMessage msg;
        msg._type = NCMTBattlEye;
        msg._content.WriteString(beg);
        SendMsg(&msg, NMFGuaranteed);
      }
      break;
#if _USE_BATTL_EYE_CLIENT
    case CMDBEClient:
      _beIntegration.Command(beg);
      break;
#endif
#if _VBS3
    case CMDExitAll:
      NetworkCommandMessage msg;
      msg._type = NCMTExitAll;
      SendMsg(&msg, NMFGuaranteed);
      break;
#endif
  }
  }
  return true;
}

void NetworkClient::SelectMission(RString mission, int difficulty)
{
  if (_gameMaster)
  {
    NetworkCommandMessage msg;
    msg._type = NCMTMission;
    msg._content.WriteString(mission);
    msg._content.WriteString(Glob.config.diffNames.GetName(difficulty));
    SendMsg(&msg, NMFGuaranteed);
  }
  _selectMission = false;
  _voteMission = false;
}

void NetworkClient::VoteMission(RString mission, int difficulty)
{
  VoteMissionMessage msg;
  msg._name = mission;
  msg._diff = Glob.config.diffNames.GetName(difficulty);
  SendMsg(&msg, NMFGuaranteed);
}

DWORD NetworkClient::SendMsg
(
  NetworkSimpleObject *object, NetMsgFlags dwFlags
)
{
  if (_clientState<NCSConnected) return ~0;
  return NetworkComponent::SendMsg(TO_SERVER, object, dwFlags);
}

void NetworkClient::EnqueueMsg
(
  int to, NetworkMessage *msg,
  NetworkMessageType type,
  NetworkObject *object
)
{
  ClientQueueItem *item = new ClientQueueItem();
  item->type = type;
  item->msg = msg;
  item->object = object;
  _messageQueue.Add(item);
}

/*!
\patch 1.26 Date 10/05/2001 by Jirka
- Optimized: MP: small update messages are sent packed
  to form optimum size UDP frames.
*/

void NetworkClient::EnqueueMsgNonGuaranteed
(
  int to, NetworkMessage *msg,
  NetworkMessageType type,
  const RefR<NetMessage, RefCountSafe> &dependOn
)
{
  ClientQueueItem *item = new ClientQueueItem();
  item->type = type;
  item->msg = msg;
  item->dependOn = dependOn;
  _messageQueueNonGuaranteed.Add(item);
}

NetworkMessageFormatBase *NetworkClient::GetFormat(/*int client, */int type)
{
  Assert(type >= 0);
  if (type < NMTFirstVariant)
  {
    // static format
    return GMsgFormats[type];
  }
  else if (_clientState>=NCSConnected || type==NMTClientState) // client may receive packets from a previous session. It should not attempt processing them if not ready.
  {
    // dynamic format
    int index = type - NMTFirstVariant;
    if (index >= _formats.Size()) return NULL;
    return &_formats[index];
  }
  else return NULL;
}

const NetworkMessageFormatBase *NetworkClient::GetFormat(/*int client, */int type) const
{
  Assert(type >= 0);
  if (type < NMTFirstVariant)
  {
    // static format
    return GMsgFormats[type];
  }
  else if (_clientState>=NCSConnected || type==NMTClientState) // client may receive packets from a previous session. It should not attempt processing them if not ready.
  {
    // dynamic format
    int index = type - NMTFirstVariant;
    if (index >= _formats.Size()) return NULL;
    return &_formats[index];
  }
  else return NULL;
}

void NetworkClient::GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players)
{
  players.Resize(0);
  for (int i=0; i<_identities.Size(); i++)
  {
    int index = players.Add();
    players[index].dpid = _identities[i].dpnid;
    players[index].name = _identities[i].GetName();
  }
}

void NetworkClient::AssignPlayer(int role, int player, int flags, bool update)
{
  PlayerRole info;

  if (role==-1)
  { 
    role = _playerRoles.Size(); //AutoAssign by server
  }
  else
  {
  if (role < 0 || role >= _playerRoles.Size()) return;
  if (!update && _playerRoles[role].player == player && _playerRoles[role].flags == flags) return;
    info = _playerRoles[role];
  }
  info.player = player;
  info.flags = flags;
  DoAssert(!info.GetFlag(PRFLocked));

  // create message
  if (update)
  { // use NetworkMessagePlayerRoleUpdate
    PlayerRoleUpdate msg;
    msg._index = role;
    msg._player = info.player;
    msg._lifeState = (int)info.lifeState;
    SendMsg(&msg, NMFGuaranteed);
  }
  else
  { // use NetworkMessagePlayerRole
    NetworkMessageType type = info.GetNMType(NMCCreate);
    Ref<NetworkMessage> msg = ::CreateNetworkMessage(type);
    msg->time = Glob.time;
    NetworkMessageContext ctx(msg, this, TO_SERVER, MSG_SEND);

    PREPARE_TRANSFER(PlayerRole)

    TMError err;
    err = TRANSF_BASE(index, role);
    if (err != TMOK) return;
    err = info.TransferMsg(ctx);
    if (err != TMOK) return;

    // send message
    SendMsg(TO_SERVER, msg, type, NMFGuaranteed, NULL);
  }
}

void NetworkClient::AskForSetDamage
(
  Object *who, float dammage
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForSetDamageMessage msg;
  msg._who = who;
  msg._dammage = dammage;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForSetMaxHitZoneDamage
(
 Object *who, float dammage
 )
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForSetMaxHitZoneDamageMessage msg;
  msg._who = who;
  msg._dammage = dammage;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForEnableVisionModes(Transport *vehicle, bool enable)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForEnableVModeMessage msg;
  msg._vehicle = vehicle;
  msg._enable = enable;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForForceGunLight(Person *person, bool force)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForForceGunLightMessage msg;
  msg._vehicle = person;
  msg._force = force;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForPilotLight(EntityAI *vehicle, bool enable)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForPilotLightMessage msg;
  msg._vehicle = vehicle;
  msg._enable = enable;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForIRLaser(Person *person, bool enable)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForIRLaserMessage msg;
  msg._vehicle = person;
  msg._enable = enable;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForAllowCrewInImmobile(Transport *vehicle, bool enable)
{
  AskForAllowCrewInImmobileMessage msg;
  msg._vehicle = vehicle;
  msg._enable = enable;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForApplyDoDamage(
  Object *who, EntityAI *owner, const Object::DoDamageResult &result, RString ammo
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForApplyDoDamageMessage msg;
  msg._who = who;
  msg._owner = owner;
  msg._damage = result.damage;
  msg._hits = result.hits;
  msg._ammo = ammo;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::StaticObjectDestructed(Object *object, bool now, bool immediate)
{
  StaticObjectDestructedMessage msg;
  msg._object = object;
  msg._now = now;
  msg._immediate = immediate;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForGetIn
(
  Person *soldier, Transport *vehicle,
  GetInPosition position, Turret *turret,
  int cargoIndex
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing
/*
RptF
(
  "Asking for get in: %s (%s, id %d:%d) into %s (%s, id %d:%d)",
  (const char *)soldier->GetDebugName(), soldier->IsLocal() ? "LOCAL" : "REMOTE",
  soldier->GetNetworkId().creator.CreatorInt(), soldier->GetNetworkId().id,
  (const char *)vehicle->GetDebugName(), vehicle->IsLocal() ? "LOCAL" : "REMOTE",
  vehicle->GetNetworkId().creator.CreatorInt(), vehicle->GetNetworkId().id
);
*/
  AskForGetInMessage msg;
  msg._soldier = soldier;
  msg._vehicle = vehicle;
  msg._position = position;
  msg._turret = turret;
  msg._cargoIndex = cargoIndex;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForGetOut(
  Person *soldier, Transport *vehicle, Turret *turret, bool eject, bool parachute
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing
/*
RptF
(
  "Asking for get out: %s (%s, id %d:%d) from %s (%s, id %d:%d)",
  (const char *)soldier->GetDebugName(), soldier->IsLocal() ? "LOCAL" : "REMOTE",
  soldier->GetNetworkId().creator.CreatorInt(), soldier->GetNetworkId().id,
  (const char *)vehicle->GetDebugName(), vehicle->IsLocal() ? "LOCAL" : "REMOTE",
  vehicle->GetNetworkId().creator.CreatorInt(), vehicle->GetNetworkId().id
);
*/

  AskForGetOutMessage msg;
  msg._soldier = soldier;
  msg._vehicle = vehicle;
  msg._turret = turret;
  msg._parachute = parachute;
  msg._eject = eject;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskWaitForGetOut(Transport *vehicle, AIUnit *unit)
{
  if (_clientState != NCSBriefingRead) return;  // send event messages only when playing

  AskWaitForGetOutMessage msg;
  msg._vehicle = vehicle;
  msg._unit = unit;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForChangePosition
(
  Person *soldier, Transport *vehicle, UIActionType type, Turret *turret, int cargoIndex
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForChangePositionMessage msg;
  msg._soldier = soldier;
  msg._vehicle = vehicle;
  msg._type = type;
  msg._turret = turret;
  msg._cargoIndex = cargoIndex;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForSelectWeapon
(
  EntityAIFull *vehicle, Turret *turret, int weapon
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForSelectWeaponMessage msg;
  msg._vehicle = vehicle;
  msg._turret = turret;
  msg._weapon = weapon;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForAmmo
(
  EntityAIFull *vehicle, Turret *turret, int weapon, int burst
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForAmmoMessage msg;
  msg._vehicle = vehicle;
  msg._turret = turret;
  msg._weapon = weapon;
  msg._burst = burst;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForFireWeapon(EntityAIFull *vehicle, Turret *turret, int weapon, TargetType *target, bool forceLock)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForFireWeaponMessage msg;
  msg._vehicle = vehicle;
  msg._turret = turret;
  msg._weapon = weapon;
  msg._target = target;
  msg._forceLock = forceLock;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForAirportSetSide(int index, TargetSide side)
{
  AskForAirportSetSideMessage msg;
  msg._airportId = index;
  msg._side = side;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForAddImpulse
(
  Vehicle *vehicle, Vector3Par force, Vector3Par torque
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForAddImpulseMessage msg;
  msg._vehicle = vehicle;
  msg._force = force;
  msg._torque = torque;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForMove
(
  Object *vehicle, Vector3Par pos
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForMoveVectorMessage msg;
  msg._vehicle = vehicle;
  msg._pos = pos;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForMove
(
  Object *vehicle, Matrix4Par trans
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForMoveMatrixMessage msg;
  msg._vehicle = vehicle;
  msg._pos = trans.Position();
  msg._orient = trans.Orientation();
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForJoin
(
  AIGroup *join, AIGroup *group, bool silent
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForJoinGroupMessage msg;
  msg._join = join;
  msg._group = group;
  msg._silent = silent;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForJoin
(
  AIGroup *join, OLinkPermNOArray(AIUnit) &units, bool silent, int id
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  AskForJoinUnitsMessage msg;
  msg._join = join;
  msg._units = units;
  msg._silent = silent;
  msg._id = id;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForChangeSide
(
 Entity *vehicle, TargetSide side
 )
{
  AskForChangeSideMessage msg;
  msg._vehicle = vehicle;
  msg._side = side;

  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskRemoteControlled
(
 Person *who, AIBrain *whom
 )
{
  AskRemoteControlledMessage msg;
  msg._whom = whom;
  msg._who = who;

  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForHideBody(Person *vehicle)
{
  if (_clientState != NCSBriefingRead) return;  // send event messages only when playing

  AskForHideBodyMessage msg;
  msg._vehicle = vehicle;
  SendMsg(&msg, NMFGuaranteed); 
}

void NetworkClient::ExplosionDamageEffects(
  EntityAI *owner, Object *directHit, HitInfoPar hitInfo, int componentIndex,
  bool enemyDamage, float energyFactor, float explosionFactor
)
{
  if (_clientState != NCSBriefingRead) return;  // send event messages only when playing

  if (hitInfo._ammoType == NULL) return;

  ExplosionDamageEffectsMessage msg;
  msg._owner = owner;
  // FIX: Ref to nonnetwork object 2787f900# 93702: protectionzone.p3d (objects created through script command "createVehicleLocal" can be hit by shot but has NO NetworkId)
  msg._directHit = (!directHit || directHit->GetNetworkId().IsNull()) ? NULL : directHit;
  msg._componentIndex = componentIndex;
  msg._type = hitInfo._ammoType->GetName();
  msg._pos = hitInfo._pos;
  msg._surfNormal = hitInfo._surfNormal;
  msg._inSpeed = hitInfo._inSpeed;
  msg._outSpeed = hitInfo._outSpeed;
  msg._enemyDamage = enemyDamage;
  msg._energyFactor = energyFactor;
  msg._explosionFactor = explosionFactor;
  SendMsg(&msg, NMFGuaranteed);

  #if _EXT_CTRL //ext. Controller like HLA, AAR
    if(IsBotClient())
    {
      // WIP:EXTEVARS:FIXME:EXTERNAL pass proper parameters
      _hla.ExplosionDamageEffects(owner, directHit, pos, dir, hitInfo, enemyDamage, energyFactor, explosionFactor);
    }
  #endif
}

void NetworkClient::FireWeapon(
  EntityAIFull *vehicle, Person *gunner, int weapon, const Magazine *magazine, EntityAI *target,
  const RemoteFireWeaponInfo &remoteInfo
)
{
  if (_clientState != NCSBriefingRead) return;  // send event messages only when playing

  if (!magazine) return;

  FireWeaponMessage msg;
  msg._vehicle = vehicle;
  // TODO: for now, gunner is used as a turret identification
  // maybe some better turret id should be transferred
  msg._gunner = gunner;
  msg._target = target;
  msg._weapon = weapon;
  msg._magazineCreator = magazine->_creator;
  msg._magazineId = magazine->_id;
  DoAssert(remoteInfo._position.IsFinite());
  msg._pos = remoteInfo._position;
  msg._dir = remoteInfo._direction;
  msg._visible = remoteInfo._visible;
#if _VBS3
  msg._projectile = remoteInfo._projectile;
#endif
  SendMsg(&msg, NMFGuaranteed);

#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient())
    _hla.FireWeapon(vehicle, gunner, weapon, magazine, target,remoteInfo);
#endif
}

void NetworkClient::UpdateWeapons(EntityAIFull *vehicle, Turret *turret, Person *gunner)
{
  UpdateWeaponsMessage msg;
  msg.vehicle = vehicle;
  msg.turret = turret;
  msg.gunner = gunner;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AddWeaponCargo(EntityAI *vehicle, RString weapon)
{
  AddWeaponCargoMessage msg;
  msg._vehicle = vehicle;
  msg._weapon = weapon;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::RemoveWeaponCargo(EntityAI *vehicle, RString weapon)
{
  RemoveWeaponCargoMessage msg;
  msg._vehicle = vehicle;
  msg._weapon = weapon;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AddMagazineCargo(EntityAI *vehicle, const Magazine *magazine)
{
  AddMagazineCargoMessage msg;
  msg._vehicle = vehicle;
  msg._magazine = const_cast<Magazine *>(magazine);
  SendMsg(&msg, NMFGuaranteed);
}


void NetworkClient::RemoveMagazineCargo(EntityAI *vehicle, RString type, int ammo)
{
  RemoveMagazineCargoMessage msg;
  msg._vehicle = vehicle;
  msg._type = type;
  msg._ammo = ammo;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AddBackpackCargo(EntityAI *vehicle, EntityAI *backpack)
{
  AddBackpackCargoMessage msg;
  msg._vehicle = vehicle;
  msg._backpack = backpack;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::RemoveBackpackCargo(EntityAI *vehicle, EntityAI *backpack)
{
  RemoveBackpackCargoMessage msg;
  msg._vehicle = vehicle;
  msg._backpack = backpack;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::ClearWeaponCargo(EntityAI *vehicle)
{
  ClearWeaponCargoMessage msg;
  msg._vehicle = vehicle;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::ClearMagazineCargo(EntityAI *vehicle)
{
  ClearMagazineCargoMessage msg;
  msg._vehicle = vehicle;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::ClearBackpackCargo(EntityAI *vehicle)
{
  ClearBackpackCargoMessage msg;
  msg._vehicle = vehicle;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::VehicleInit(VehicleInitMessage &init)
{
  SendMsg(&init, NMFGuaranteed);
}

void NetworkClient::OnVehicleDestroyed
(
  EntityAI *killed, EntityAI *killer
)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  if (!killed->GetNetworkId().IsNull())
  {
    VehicleDestroyedMessage msg;
    msg._killed = killed;
    if (killer && !killer->GetNetworkId().IsNull())
      msg._killer = killer;
    SendMsg(&msg, NMFGuaranteed);
  }

#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient())
    _hla.VehicleDestroyed(killed,killer);
#endif
}

void NetworkClient::OnVehicleDamaged(EntityAI *damaged, EntityAI *killer, float damage, RString ammo)
{
  if (_clientState != NCSBriefingRead
#if _VBS3 //enable messages also during briefing screen to enable scripted missions
    && (_clientState < NCSGameLoaded || _clientState > NCSBriefingRead)
#endif
    ) return;  // send event messages only when playing

  VehicleDamagedMessage msg;
  msg._damaged = damaged;
  msg._killer = killer;
  msg._damage = damage;
  msg._ammo = ammo;
  SendMsg(&msg, NMFGuaranteed);

#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient())
  {
    _hla.VehicleDamaged(damaged, killer, damage, ammo);
  }
#endif
}

void NetworkClient::OnVehMPEventHandlersChanged(EntityAI *ai, int event, const AutoArray<RString> &handlers)
{
  VehMPEventHandlersMessage msg;
  msg._vehicle = ai;
  msg._eventNum = event;
  msg._handlers = handlers;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::OnIncomingMissile(EntityAI *target, Entity *shot, EntityAI *owner)
{
  if (_clientState != NCSBriefingRead) return;  // send event messages only when playing

  IncomingMissileMessage msg;
  msg._target = target;
  msg._shot = shot;
  msg._owner = owner;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::OnLaunchedCounterMeasures(Entity *cm, Entity *testedSystem, int count)
{
  if (_clientState != NCSBriefingRead) return;  // send event messages only when playing

  LaunchedCounterMeasuresMessage msg;
  msg._cm = cm;
  msg._testedSystem = testedSystem;
  msg._count = count;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::OnWeaponLocked(EntityAI *target, Person *gunner, bool locked)
{
  if (_clientState != NCSBriefingRead) return;  // send event messages only when playing

  WeaponLockedMessage msg;
  msg._target = target;
  msg._gunner = gunner;
  msg._locked = locked;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::MarkerCreate(int channel, AIBrain *sender, RefArray<NetworkObject> &units, ArcadeMarkerInfo &info)
{
  if (_clientState < NCSMissionReceived) return;

  MarkerCreateMessage msg;
  msg._marker = info;
  msg._channel = channel;
  msg._sender = sender;
  msg._units = units;
  SendMsg(&msg, NMFGuaranteed);
#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient()) _hla.CreateMarkerMsg(&info);
#endif

}

void NetworkClient::MarkerDelete(RString name)
{
  if (_clientState < NCSMissionReceived) return;

  MarkerDeleteMessage msg;
  msg._name = name;
#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient()) _hla.DeleteMarkerMsg(name);
#endif

  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::WaypointCreate(AIGroup *group, int index, const WaypointInfo &wp)
{
  if (_clientState < NCSMissionReceived) return;
  
  //LogF("==== WaypointCreate %s (id=%d:%d), index %d, id %d",cc_cast(group->GetDebugName()),group->GetNetworkId().creator.CreatorInt(),group->GetNetworkId().id,index,wp.idWP);
  WaypointCreateMessage msg;
  msg._group = group;
  msg._index = index;
  msg._waypoint = wp;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::HCClearGroups(AIUnit *unit)
{
  if (_clientState < NCSMissionReceived) return;

  HCClearGroupsMessage msg;
  msg._unit = unit;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::HCRemoveGroup(AIUnit *unit,AIGroup *group)
{
  if (_clientState < NCSMissionReceived) return;

  HCRemoveGroupMessage msg;
  msg._group = group;
  msg._unit = unit;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::HCSetGroup(AIUnit *unit,AIHCGroup hcGroup)
{
  if (_clientState < NCSMissionReceived) return;

  HCSetGroupMessage msg;
  msg._group = hcGroup.GetGroup();
  msg._unit = unit;
  msg._team = hcGroup.Team();
  msg._name= hcGroup.Name();
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::GroupSetUnconsciousLeader(AIUnit *unit,AIGroup *group)
{
  if (_clientState < NCSMissionReceived) return;

  GroupSetUnconsciousLeaderMessage msg;
  msg._group = group;
  msg._unit = unit;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::GroupSelectLeader(AIUnit *unit,AIGroup *group)
{
  if (_clientState < NCSMissionReceived) return;

  GroupSelectLeaderMessage msg;
  msg._group = group;
  msg._unit = unit;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::DropBackpack(EntityAI *backpack, Matrix4Par trans)
{
  if (_clientState < NCSMissionReceived) return;

  DropBackpackMessage msg;
  msg._backpack = backpack;
  msg._pos = trans.Position();
  msg._orient = trans.Orientation();
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::TakeBackpack(Person *soldier, EntityAI *backpack)
{
  if (_clientState < NCSMissionReceived) return;

  TakeBackpackMessage msg;
  msg._backpack = backpack;
  msg._soldier = soldier;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::Assemble(EntityAI *weapon, Matrix4Par transform)
{
  if (_clientState < NCSMissionReceived) return;

  AssembleMessage msg;
  msg._weapon = weapon;
  msg._transform = transform.Orientation();
  msg._position = transform.Position();
  SendMsg(&msg, NMFGuaranteed);
}


void NetworkClient::DisAssemble(EntityAI *weapon, EntityAI *backpack)
{
  if (_clientState < NCSMissionReceived) return;

  DisAssembleMessage msg;
  msg._weapon = weapon;
  msg._backpack = backpack;
  SendMsg(&msg, NMFGuaranteed);
}


void NetworkClient::WaypointUpdate(AIGroup *group, int index, const WaypointInfo &wp)
{
  if (_clientState < NCSMissionReceived) return;

  //LogF("==== WaypointUpdate %s (id=%d:%d), index %d, id %d",cc_cast(group->GetDebugName()),group->GetNetworkId().creator.CreatorInt(),group->GetNetworkId().id,index,wp.idWP);
  WaypointUpdateMessage msg;
  msg._group = group;
  msg._index = index;
  msg._waypoint = wp;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::WaypointDelete(AIGroup *group, int index, const WaypointInfo &wp)
{
  if (_clientState < NCSMissionReceived) return;
  //LogF("==== WaypointDelete %s (id=%d:%d), index %d, id %d",cc_cast(group->GetDebugName()),group->GetNetworkId().creator.CreatorInt(),group->GetNetworkId().id,index,wp.idWP);
  WaypointDeleteMessage msg;
  msg._group = group;
  msg._index = index;
  msg._idWP = wp.idWP;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::WaypointsCopy(AIGroup *to, AIGroup *from)
{
  if (_clientState < NCSMissionReceived) return;

  //LogF("==== WaypointsCopy %s (id=%d:%d), index %d, id %d",cc_cast(to->GetDebugName()),to->GetNetworkId().creator.CreatorInt(),to->GetNetworkId().id);
  WaypointsCopyMessage msg;
  msg._to = to;
  msg._from = from;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::SetObjectTexture(EntityAI *veh, int index, RString name, RString eval)
{
#if _ENABLE_VBS
  if (_clientState < NCSMissionReceived) return;

  SetObjectTextureMessage msg;
  msg._veh = veh;
  msg._index = index;
  msg._name = name;
  msg._eval = eval;
  SendMsg(&msg, NMFGuaranteed);
#endif
}

void NetworkClient::PublicExec(RString condition, RString command, Object* obj, OLinkArray(Object) &objs)
{
#if _ENABLE_VBS
  if (_clientState < NCSMissionReceived) return;

  PublicExecMessage msg;
  msg._condition = condition;
  msg._command = command;
  msg._obj = obj;
  msg._objs = objs;
  SendMsg(&msg, NMFGuaranteed);
#endif
}

void NetworkClient::SetFlagOwner
(
  Person *owner, EntityAI *carrier
)
{
  SetFlagOwnerMessage msg;
  msg._owner = owner;
  msg._carrier = carrier;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::SetFlagCarrier
(
  Person *owner, EntityAI *carrier
)
{
  SetFlagCarrierMessage msg;
  msg._owner = owner;
  msg._carrier = carrier;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::CancelTakeFlag(EntityAI *carrier)
{
  CancelTakeFlagMessage msg;
  msg._flag = carrier;
  SendMsg(&msg, NMFGuaranteed);
}

static Object *GetValObject( GameValuePar oper )
{
  if( oper.GetType()==GameObject )
  {
    return static_cast<GameDataObject *>(oper.GetData())->GetObject();
  }
  return NULL;
}

static AIGroup *GetValGroup( GameValuePar oper )
{
  if( oper.GetType()==GameGroup )
  {
    return static_cast<GameDataGroup *>(oper.GetData())->GetGroup();
  }
  return NULL;
}

#if _ENABLE_INDEPENDENT_AGENTS
static AITeamMember *GetValTeamMember( GameValuePar oper )
{
  if( oper.GetType()==GameTeamMember )
  {
    return static_cast<GameDataTeamMember *>(oper.GetData())->GetTeamMember();
  }
  return NULL;
}
#endif

static bool WriteValue(GameValuePar var, QOStrStream &out)
{    
  GameType type = var.GetType();
  if (type.Size() != 1)
  {
    RptF("Transfer of uninitialized variables is not supported");
    return false;
  }

  // write the type
  RString typeName = type[0]->_name;
  int typeSize = typeName.GetLength();
  out.write(&typeSize, sizeof(int));
  out.write(cc_cast(typeName), typeSize);

  if (type == GameScalar)
  {
    GameScalarType value = var.GetData()->GetScalar();
    out.write(&value, sizeof(value));
  }
  else if (type==GameBool)
  {
    GameBoolType value = var.GetData()->GetBool();
    out.write(&value, sizeof(value));
  }
  else if (type == GameString)
  {
    GameStringType value = var.GetData()->GetString();
    out.write((const char *)value, value.GetLength());
  }
  else if (type==GameObject)
  {
    GameObjectType value = GetValObject(var);
    NetworkId id = value ? value->GetNetworkId() : NetworkId::Null();
    out.write(&id, sizeof(id));
  }
  else if (type==GameGroup)
  {
    GameGroupType value = GetValGroup(var);
    NetworkId id = value ? value->GetNetworkId() : NetworkId::Null();
    out.write(&id, sizeof(id));
  }
#if _ENABLE_INDEPENDENT_AGENTS
  else if (type==GameTeamMember)
  {
    GameTeamMemberType value = GetValTeamMember(var);
    NetworkId id = value ? value->GetNetworkId() : NetworkId::Null();
    out.write(&id, sizeof(id));
  }
#endif
  else
  {
    // common solution - store content using its serialization
    ParamArchiveSave ar(PublicVariableVersion);
    ar.SetParams(GWorld->GetGameState());
    LSError err = var.GetData()->Serialize(ar);
    ar.SaveBin(out);
    return err == LSOK;
  }
  return true;
}

/*!
Implemented only for scalar variables.
\todo Implement for other variable types.
\patch 1.34 Date 12/7/2001 by Ondra
- New: publicVariable now works with scalar, bool, Object and Group types.
\patch 2.04 Date 6/4/2003 by Jirka
- New: publicVariable now works with string type.
*/
void NetworkClient::PublicVariable(RString name, int dpnid)
{
#define DEBUG_PUBLIC_VARIABLES 0
#if DEBUG_PUBLIC_VARIABLES
  if (stricmp(cc_cast(name),"BIS_missionScope")==0)
    _asm nop;
#endif
  GameState *gstate = GWorld->GetGameState();
  GameValue var = gstate->VarGet(name, false, GWorld->GetMissionNamespace()); // mission namespace
  if (var.GetNil()) return;

  QOStrStream out;
  if (!WriteValue(var, out))
    return;

  // Store publicVariables containing objects without NetworkId to the delayed list
  bool addToDelayedList = false;
  const GameType &type = var.GetType();
  if (type==GameObject)
  {
    Object *value = GetValObject(var);
    if ( value && value->GetNetworkId().IsNull() ) addToDelayedList = true; // it exists but CreateAllObjects was possibly not called yet
  }
  else if (type==GameGroup)
  {
    GameGroupType value = GetValGroup(var);
    if ( value && value->GetNetworkId().IsNull() ) addToDelayedList = true;
  }
#if _ENABLE_INDEPENDENT_AGENTS
  else if (type==GameTeamMember)
  {
    GameTeamMemberType value = GetValTeamMember(var);
    if ( value && value->GetNetworkId().IsNull() ) addToDelayedList = true;
  }
#endif

  if (addToDelayedList)
  { 
    AddDelayedPublicVariable(name, dpnid); //store for later processing
    if ( _clientState < NCSGameLoaded )
      return; // Hack: we hope the delayed PublicVariableMessage would contain object with NetworkId present
  }

  if (dpnid == 0)
  {
    PublicVariableMessage msg;
    msg._name = name;
    int size = out.pcount();
    msg._value.Resize(size);
    memcpy(msg._value.Data(), out.str(), size);
    SendMsg(&msg, NMFGuaranteed);
  }
  else
  {
    PublicVariableToMessage msg;
    msg._name = name;
    int size = out.pcount();
    msg._value.Resize(size);
    memcpy(msg._value.Data(), out.str(), size);
    msg._player = dpnid;
    SendMsg(&msg, NMFGuaranteed);
  }
}

void NetworkClient::SendAUMessage(GameValuePar oper1)
{
  QOStrStream out;
  if (!WriteValue(oper1, out))
    return;

  SendAUMsgMessage msg;
  int size = out.pcount();
  msg._value.Resize(size);
  memcpy(msg._value.Data(), out.str(), size);
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AddDelayedPublicVariable(RString name, int dpnid)
{
  DelayedPublicVariable item;
  item._name = name;
  item._player = dpnid;
  _delayedPublicVariables.Add(item);
}

void NetworkClient::ClearDelayedPublicVariables()
{
  _delayedPublicVariables.Clear();
}

void NetworkClient::ProcessDelayedPublicVariables()
{
  //note: the for cycle cannot loop for ever even when PublicVariable add something into _delayedPublicVariables array
  for (int i=0, siz=_delayedPublicVariables.Size(); i<siz; i++)
  {
    const DelayedPublicVariable &item = _delayedPublicVariables[i];
    PublicVariable(item._name, item._player);
  }
  _delayedPublicVariables.Clear();
}

void NetworkClient::TeamMemberSetVariable(AITeamMember *teamMember,RString name)
{
#if _ENABLE_INDEPENDENT_AGENTS
  GameValue var;
  if (!teamMember->GetVars()->VarGet(name, var)) return;

  QOStrStream out;
  if (!WriteValue(var, out))
    return;

  const NetworkId &id = teamMember->GetNetworkId();

  TeamMemberSetVariableMessage msg;
  msg._id = id.id;
  msg._creator = id.creator;
  msg._name = name;
  int size = out.pcount();
  msg._value.Resize(size);
  memcpy(msg._value.Data(), out.str(), size);
  SendMsg(&msg, NMFGuaranteed);
#endif
}

void NetworkClient::ObjectSetVariable(Object *obj, RString name)
{
  if (!obj) return;
  GameVarSpace *vars = obj->GetVars();
  if (!vars) return;

  GameValue var;
  if (!vars->VarGet(name, var)) return;

  QOStrStream out;
  if(!var.GetNil())
    if (!WriteValue(var, out)) return;

  ObjectSetVariableMessage msg;
  msg._object = obj;
  msg._name = name;
  int size = out.pcount();
  msg._value.Resize(size);
  memcpy(msg._value.Data(), out.str(), size);
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::GroupSetVariable(AIGroup *group, RString name)
{
  if (!group) return;
  GameVarSpace *vars = group->GetVars();
  if (!vars) return;

  GameValue var;
  if (!vars->VarGet(name, var)) return;

  QOStrStream out;
  if(!var.GetNil())
    if (!WriteValue(var, out)) return;

  GroupSetVariableMessage msg;
  msg._group = group;
  msg._name = name;
  int size = out.pcount();
  msg._value.Resize(size);
  memcpy(msg._value.Data(), out.str(), size);
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::SendMPEvent(MPEntityEvent event, const GameValue &pars)
{
  QOStrStream out;
  if (!WriteValue(pars, out))
    return;
  VehMPEventMessage msg;
  msg._eventNum = event;
  int size = out.pcount();
  msg._pars.Resize(size);
  memcpy(msg._pars.Data(), out.str(), size);
  SendMsg(&msg, NMFGuaranteed);    
}

void NetworkClient::GroupSynchronization(AIGroup *grp, int synchronization, bool active)
{
  GroupSynchronizationMessage msg;
  msg._group = grp;
  msg._synchronization = synchronization;
  msg._active = active;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::DetectorActivation(Detector *det, bool active)
{
  DetectorActivationMessage msg;
  msg._detector = det;
  msg._active = active;
  SendMsg(&msg, NMFGuaranteed);
}

#if _VBS3
void NetworkClient::EnablePersonalItems(Person *object,bool active, RString animAction, Vector3 personalItemsOffset)
{
  EnablePersonalItemsMessage msg;
  msg._object = object;
  msg._active = active;
  msg._animAction = animAction;
  msg._personalItemsOffset = personalItemsOffset;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AARAskUpdate(int type ,int intType,RString stringOne, RString stringTwo, float floatType)
{
  AARAskUpdateMessage msg;
  msg._type = type;
  msg._intType = intType;
  msg._stringOne = stringOne;
  msg._stringTwo = stringTwo;
  msg._floatType = floatType;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AddMPReport(int type, RString myString)
{
  AddMPReportMessage msg;
  msg._type = type;
  msg._myString = myString;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AARDoUpdate(int type ,int intType,RString stringOne, RString stringTwo, float floatType)
{
  AARDoUpdateMessage msg;
  msg._type = type;
  msg._intType = intType;
  msg._stringOne = stringOne;
  msg._stringTwo = stringTwo;
  msg._floatType = floatType;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::LoadIsland(RString islandName)
{
  LoadIslandMessage msg;
  msg._islandName = islandName;

  SendMsg(&msg,NMFGuaranteed);
}

void NetworkClient::ApplyWeather()
{
  ApplyWeatherMessage msg;
  GLandscape->TranferMsg(msg);

#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient()) _hla.WeatherUpdate(&msg);
#endif

  SendMsg(&msg,NMFGuaranteed);
}

void NetworkClient::AskCommanderOverride(Turret *turret, Turret *overriddenByTurret)
{
  AskCommanderOverrideMessage msg;
  msg._turret = turret;
  msg._overriddenByTurret = overriddenByTurret;
  SendMsg(&msg, NMFGuaranteed);
}

#endif //_VBS3

void NetworkClient::AskForCreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank)
{
  AskForCreateUnitMessage msg;
  msg._group = group;
  msg._type = type;
  msg._position = position;
  msg._init = init;
  msg._skill = skill;
  msg._rank = rank;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForDeleteVehicle(Entity *veh)
{
  AskForDeleteVehicleMessage msg;
  msg._vehicle = veh;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForReceiveUnitAnswer
(
  AIUnit *from, AISubgroup *to, int answer
)
{
  AskForReceiveUnitAnswerMessage msg;
  msg._from = from;
  msg._to = to;
  msg._answer = answer;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForOrderGetIn(AIBrain *unit, bool flag)
{
  AskForOrderGetInMessage msg;
  msg._unit = unit;
  msg._flag = flag;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForAllowGetIn(AIBrain *unit, bool flag)
{
  AskForAllowGetInMessage msg;
  msg._unit = unit;
  msg._flag = flag;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AskForGroupRespawn(Person *person, EntityAI *killer)
{
  AskForGroupRespawnMessage msg;
  msg._person = person;
  msg._killer = killer;
  msg._group = person->Brain()->GetGroup();
  msg._from = GetPlayer();
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::GetSwitchableUnits(OLinkPermNOArray(AIBrain) &units, const AIBrain *player)
{
  switch (GetRespawnMode())
  {
  case RespawnToGroup:
    {
      AIGroup *grp = player->GetGroup();
      if (!grp) break;
      // from my group
      for (int i=0; i<grp->NUnits(); i++)
      {
        AIBrain *unit = grp->GetUnit(i);
        // alive
        if (!unit || !unit->LSIsAlive()) continue;
        // playable
        if (!unit->IsPlayable()) continue;
        Person *person = unit->GetPerson();
        if (!person) continue;
        // alive
        if (person->IsDamageDestroyed()) continue;
        // free or current player
        if (person->GetRemotePlayer() != 1 &&  person->GetRemotePlayer() != _player) continue;

        units.Add(unit);
      }
    }
    break;
  case RespawnToFriendly:
    {
      AIGroup *grp = player->GetGroup();
      if (!grp) break;
      AICenter *center = grp->GetCenter();
      if (!center) break;
      // my side
      for (int j=0; j<center->NGroups(); j++)
      {
        AIGroup *grp = center->GetGroup(j);
        if (!grp) continue;
        for (int i=0; i<grp->NUnits(); i++)
        {
          AIBrain *unit = grp->GetUnit(i);
          // alive
          if (!unit || !unit->LSIsAlive()) continue;
          // playable
          if (!unit->IsPlayable()) continue;
          Person *person = unit->GetPerson();
          if (!person) continue;
          // alive
          if (person->IsDamageDestroyed()) continue;
          // free or current player
          if (person->GetRemotePlayer() != AI_PLAYER && person->GetRemotePlayer() != _player) continue;

          units.Add(unit);
        }
      }
    }
    break;
  }
}

void NetworkClient::TeamSwitch(Person *from, Person *to, EntityAI *killer, bool respawn)
{
  if (!respawn && ::GlobalTickCount() < _teamSwitchDisabledUntil)
  {
#if LOG_TEAM_SWITCH
    RptF("Team switch %s -> %s disabled", cc_cast(from->GetDebugName()), cc_cast(to->GetDebugName()));
#endif
    return;
  }

#if LOG_TEAM_SWITCH
  RptF("Team switch %s -> %s wanted", cc_cast(from->GetDebugName()), cc_cast(to->GetDebugName()));
#endif
  if (to->IsLocal())
  {
    bool result = ProcessTeamSwitch(_player, from, to, killer, respawn);
    Assert(result);
    if (result)
    {
      FinishTeamSwitchMessage msg;
      msg._player = _player;
      msg._from = from;
      msg._fromGroup = msg._from ? msg._from->GetGroup() : NULL;
      msg._fromLeader = msg._fromGroup ? msg._fromGroup->Leader() : NULL;
      msg._to = to;
      msg._killer = killer;
      msg._respawn = respawn;
      SendMsg(&msg, NMFGuaranteed);

      _teamSwitchDisabledUntil = ::GlobalTickCount() + 5000;
      // TeamSwitchResult will be handled in reaction
    }
    else
    {
      HandleTeamSwitchResult(_player, from, to, killer, respawn, false);
    }
  }
  else
  {
#if LOG_TEAM_SWITCH
    RptF(" - owner of %s asked", cc_cast(to->GetDebugName()));
#endif
    // ask the owner to handle team switch
    AskForTeamSwitchMessage msg;
    msg._player = _player;
    msg._from = from;
    msg._to = to;
    msg._killer = killer;
    msg._respawn = respawn;
    SendMsg(&msg, NMFGuaranteed);

    _teamSwitchDisabledUntil = ::GlobalTickCount() + 5000;
  }
}

#if _ENABLE_CONVERSATION
void NetworkClient::KBReact(AIBrain *from, AIBrain *to, const KBMessageInfo *message)
{
  KBReactMessage msg;
  msg._from = from;
  msg._to = to;
  msg._topic = message->GetTopic();
  msg._message = message->GetMessage()->GetType()->GetName();
  int n = message->GetMessage()->NArguments();
  msg._arguments.Realloc(n);
  msg._arguments.Resize(n);
  for (int i=0; i<n; i++)
  {
    const KBArgument &argument = message->GetMessage()->GetArgument(i);
    PublicVariableMessage &var = msg._arguments[i];

    var._name = argument._name;
#if DEBUG_PUBLIC_VARIABLES
    if (stricmp(cc_cast(var._name),"BIS_missionScope")==0)
      _asm nop;
#endif
    QOStrStream out;
    if (WriteValue(argument._value, out))
    {
      int size = out.pcount();
      var._value.Resize(size);
      memcpy(var._value.Data(), out.str(), size);
    }
  }
  SendMsg(&msg, NMFGuaranteed);
}
#endif

bool NetworkClient::ProcessTeamSwitch(int player, Person *from, Person *to, EntityAI *killer, bool respawn)
{
#if LOG_TEAM_SWITCH
  RptF("Team switch %s -> %s starting", from ? cc_cast(from->GetDebugName()) : "null", to ? cc_cast(to->GetDebugName()) : "null");
#endif
  // check if respawn can be proceed
  if (!from)
  {
#if LOG_TEAM_SWITCH
    RptF(" - from is null");
#endif
    return false;
  }
  if (!from->Brain())
  {
#if LOG_TEAM_SWITCH
    RptF(" - from has no brain");
#endif
    return false;
  }
  if (!to)
  {
#if LOG_TEAM_SWITCH
    RptF(" - to is null");
#endif
    return false;
  }
  if (!to->IsLocal())
  {
#if LOG_TEAM_SWITCH
    RptF(" - to is remote");
#endif
    return false;
  }
  AIBrain *unit = to->Brain();
  if (!unit)
  {
#if LOG_TEAM_SWITCH
    RptF(" - to has no brain");
#endif
    return false;
  }
  if (!unit->LSIsAlive())
  {
#if LOG_TEAM_SWITCH
    RptF(" - to is dead");
#endif
    return false;
  }
  if (to->IsDamageDestroyed())
  {
#if LOG_TEAM_SWITCH
    RptF(" - to is damaged");
#endif
    return false;
  }
  if (to->IsNetworkPlayer())
  {
#if LOG_TEAM_SWITCH
    RptF(" - to is player (%d)", to->GetRemotePlayer());
#endif
    return false;
  }

  // move the indication of player
  to->SetRemotePlayer(player);
#if LOG_TEAM_SWITCH
  RptF("Remote player of %s set to %d (process team switch)", cc_cast(to->GetDebugName()), player);
#endif
  // enable AI for the previously frozen unit
  int state = unit->GetAIDisabled();
  unit->SetAIDisabled(state & ~AIBrain::DATeamSwitch);

  // copy the identity
  // TODO: better solution needed - avoid multiplying of player identities
  float exp = from->GetExperience();
  AIUnitInfo &info = from->GetInfo();
  saturateMax(exp, info._initExperience);
  to->SetExperience(exp);
  
  UpdateObject(to, NMFGuaranteed);
  UpdateObject(unit, NMFGuaranteed);

  // make this person a player (avoid the respawn script)
  SelectPlayer(player, to, false);

#if LOG_TEAM_SWITCH
  RptF(" - OK");
#endif

  return true;
}


void NetworkClient::FinishTeamSwitch(Person *from, bool respawn)
{
#if LOG_TEAM_SWITCH
  RptF("Team switch from %s finishing", from ? cc_cast(from->GetDebugName()) : "null");
#endif

  DoAssert(from->IsLocal());
  AIBrain *unit = from->Brain();
  DoAssert(unit);

  from->SetRemotePlayer(AI_PLAYER);
#if LOG_TEAM_SWITCH
  RptF("Remote player of %s set to %d (finish team switch)", cc_cast(from->GetDebugName()), AI_PLAYER);
#endif
  if (respawn)
  {
    unit->SetLifeState(LifeStateDead);
    DisposeBody(from);
  }
  else
  {
    // freeze the old player to do not leave the position
    // TODO: enable disable movement / fire based on the dialog controls
    int state = unit->GetAIDisabled();
    unit->SetAIDisabled(state | AIBrain::DATeamSwitch);
  }

  UpdateObject(from, NMFGuaranteed);
#if LOG_TEAM_SWITCH
  RptF("- update of %d:%d forced, remote player after update %d", from->GetNetworkId().creator.CreatorInt(), from->GetNetworkId().id, from->GetRemotePlayer());
#endif
  UpdateObject(unit, NMFGuaranteed);
}

extern GameValue GOnTeamSwitch;

void NetworkClient::HandleTeamSwitchResult(int player, Person *from, Person *to, EntityAI *killer, bool respawn, bool result)
{
#if LOG_TEAM_SWITCH
  RptF("Team switch %s -> %s handling result (%s)", from ? cc_cast(from->GetDebugName()) : "null", to ? cc_cast(to->GetDebugName()) : "null", result ? "succeeded" : "failed");
#endif

  // enable team switch again
  _teamSwitchDisabledUntil = 0;

  if (result)
  {
    // succeeded
    // animation script
    RStringB GetScriptsPath();
    if (respawn && killer)
    {
      // respawn script
      RString name = Pars >> "playerRespawnOtherUnitScript";
      if (QFBankQueryFunctions::FileExists(GetScriptsPath() + name))
      {
        GWorld->TerminateCameraScript();

        GameArrayType arguments;
        arguments.Add(GameValueExt(from));
        arguments.Add(GameValueExt(killer));
        arguments.Add(GameValueExt(to));
        Script *script = new Script(name, GameValue(arguments), GWorld->GetMissionNamespace()); // mission namespace
        GWorld->StartCameraScript(script);
      }
    }
    else
    {
      // team switch script
      RString name = Pars >> "teamSwitchScript";
      if (QFBankQueryFunctions::FileExists(GetScriptsPath() + name))
      {
        GWorld->TerminateCameraScript();

        GameArrayType arguments;
        arguments.Add(GameValueExt(from));
        arguments.Add(GameValueExt(to));
        Script *script = new Script(name, GameValue(arguments), GWorld->GetMissionNamespace()); // mission namespace
        GWorld->StartCameraScript(script);
      }
    }

    // event handler
    if (!GOnTeamSwitch.GetNil())
    {
      GameState *state = GWorld->GetGameState();
      GameVarSpace vars(false);
      state->BeginContext(&vars);
      state->VarSetLocal("_from", GameValueExt(from), true);
      state->VarSetLocal("_to", GameValueExt(to), true);
#if USE_PRECOMPILATION
      if (GOnTeamSwitch.GetType() == GameCode)
      {
        GameDataCode *code = static_cast<GameDataCode *>(GOnTeamSwitch.GetData());
        if (code->IsCompiled() && code->GetCode().Size() > 0)
          state->Evaluate(code->GetString(), code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
      }
      else
#endif
        if (GOnTeamSwitch.GetType() == GameString)
        {
          // make sure string is not destructed while being evaluated
          RString code = GOnTeamSwitch;
          if (code.GetLength() > 0)
            state->EvaluateMultiple(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
        }
        state->EndContext();
    }
  }
  else
  {
    if (respawn)
    {
      // try to select different person
      AbstractOptionsUI *CreateTeamSwitchDialog(Person *player, EntityAI *killer, bool respawn, bool userDialog);
      GWorld->DestroyUserDialog();
      GWorld->SetUserDialog(CreateTeamSwitchDialog(from, killer, true, true));
    }
  }
}

#if _VBS3 // respawn command
void NetworkClient::PauseSimulation(bool pause)
{
  PauseSimulationMessage msg;
  msg._pause = pause;
  SendMsg(&msg,NMFGuaranteed);
}

void NetworkClient::AskForRespawn(Person *person)
{
  AskForRespawnMessage msg;
  msg._person = person;
  SendMsg(&msg, NMFGuaranteed);
}
#endif

#if _ENABLE_ATTACHED_OBJECTS
void NetworkClient::AttachObject(Object *obj, Object *attachTo, int memIndex, Vector3 pos, int flags)
{
  AttachObjectMessage msg;
  msg._obj = obj;
  msg._attachTo = attachTo;
  msg._memIndex = memIndex;
  msg._pos = pos;
  msg._flags = flags;
  SendMsg(&msg, NMFGuaranteed);
#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient()) _hla.AttachToMsg(msg._obj,msg._attachTo, msg._memIndex,msg._pos,msg._flags);
#endif
}

void NetworkClient::DetachObject(Object *obj)
{
  DetachObjectMessage msg;
  msg._obj = obj;
  SendMsg(&msg, NMFGuaranteed);
#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient()) _hla.AttachToMsg(msg._obj);
#endif
}
#endif

void NetworkClient::AskForActivateMine(Mine *mine, bool activate)
{
  AskForActivateMineMessage msg;
  msg._mine = mine;
  msg._activate = activate;
  SendMsg(&msg, NMFGuaranteed);
}

/*!
\patch 1.82 Date 8/26/2002 by Jirka
- Fixed: Fire burning is synchronized in MP
*/

void NetworkClient::AskForInflameFire(Fireplace *fireplace, bool fire)
{
  AskForInflameFireMessage msg;
  msg._fireplace = fireplace;
  msg._fire = fire;
  SendMsg(&msg, NMFGuaranteed);
}

/*!
\patch 1.82 Date 8/26/2002 by Jirka
- Fixed: Transfer of user defined animations through network in MP
*/

void NetworkClient::AskForAnimationPhase(Entity *vehicle, RString animation, float phase)
{
  AskForAnimationPhaseMessage msg;
  msg._vehicle = vehicle;
  msg._animation = animation;
  msg._phase = phase;
  SendMsg(&msg, NMFGuaranteed);

#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient())
  {
    _hla.RecordAnimationPhase(vehicle,animation,vehicle->GetAnimationPhase(vehicle->FutureVisualState(), animation));
    _hla.RecordAnimationPhase(vehicle,animation,phase);
  }
#endif
}

void NetworkClient::OfferWeapon(EntityAI *from, EntityAIFull *to, const WeaponType *weapon, int slots, bool useBackpack)
{
  AskWeaponMessage msg;
  msg._from = from;
  msg._to = to;
  msg._weapon = weapon->GetName();
  msg._slots = slots;
  msg._useBackpack = useBackpack;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::OfferMagazine(EntityAI *from, EntityAIFull *to, const MagazineType *type, bool useBackpack)
{
  AskMagazineMessage msg;
  msg._from = from;
  msg._to = to;
  msg._useBackpack = useBackpack;
  msg._type = type->GetName();
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::OfferBackpack(EntityAI *from, EntityAIFull *to, RString name)
{
  AskBackpackMessage msg;
  msg._from = from;
  msg._to = to;
  msg._name = name;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::ReplaceWeapon(EntityAI *from, EntityAIFull *to, const WeaponType *weapon, bool useBackpack)
{
  ReplaceWeaponMessage msg;
  msg._from = from;
  msg._to = to;
  msg._weapon = weapon->GetName();
  msg._useBackpack = useBackpack;
  SendMsg(&msg, NMFGuaranteed | NMFHighPriority);
}

void NetworkClient::ReplaceMagazine(EntityAI *from, EntityAIFull *to, Magazine *magazine, bool reload, bool useBackpack)
{
  ReplaceMagazineMessage msg;
  msg._from = from;
  msg._to = to;
  msg._magazine = magazine;
  msg._reload = reload;
  msg._useBackpack = useBackpack;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::ReplaceBackpack(EntityAI *from, EntityAIFull *to, EntityAI *backpack)
{
  ReplaceBackpackMessage msg;
  msg._from = from;
  msg._to = to;
  msg._backpack = backpack;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::ReturnWeapon(EntityAI *from, const WeaponType *weapon)
{
  ReturnWeaponMessage msg;
  msg._from = from;
  msg._weapon = weapon->GetName();
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::ReturnMagazine(EntityAI *from, Magazine *magazine)
{
  ReturnMagazineMessage msg;
  msg._from = from;
  msg._magazine = magazine;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::ReturnBackpack(EntityAI *from, EntityAI *backpack)
{
  ReturnBackpackMessage msg;
  msg._from = from;
  msg._backpack = backpack;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::LockSupply(EntityAI *supply, AIBrain *user)
{
  LockSupplyMessage msg;
  msg._supply = supply;
  msg._user = user;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::UnlockSupply(EntityAI *supply, AIBrain *user)
{
  if (_client) //Hotfix: there were crashes when Gear was closed after session lost, see news:jubm6p$g9f$1@new-server.localdomain
  {
    UnlockSupplyMessage msg;
    msg._supply = supply;
    msg._user = user;
    SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
  }
}

void NetworkClient::ReturnEquipment(EntityAI *to, EntityAI *from, AutoArray<RString> weapons, RefArray<Magazine> magazines, EntityAI *backpack)
{
  ReturnEquipmentMessage msg;
  msg._to = to;
  msg._from = from;
  msg._weapons = weapons;
  msg._magazines = magazines;
  msg._backpack = backpack;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::PoolAskWeapon(AIBrain *unit, const WeaponType *weapon, int slot, bool useBackpack)
{
  PoolAskWeaponMessage msg;
  NetworkId id;
  if (unit) id = unit->GetNetworkId();
  msg._creator = id.creator;
  msg._id = id.id;
  msg._weapon = weapon->GetName();
  msg._slot = slot;
  msg._useBackpack = useBackpack;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::PoolAskMagazine(AIBrain *unit, const MagazineType *type, int slot, bool useBackpack)
{
  PoolAskMagazineMessage msg;
  NetworkId id;
  if (unit) id = unit->GetNetworkId();
  msg._creator = id.creator;
  msg._id = id.id;
  msg._type = type->GetName();
  msg._slot = slot;
  msg._useBackpack = useBackpack;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::PoolAskBackpack(AIBrain *unit, RString name)
{
  PoolAskBackpackMessage msg;
  NetworkId id;
  if (unit) id = unit->GetNetworkId();
  msg._creator = id.creator;
  msg._id = id.id;
  msg._name = name;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}


void NetworkClient::PoolReturnWeapon(const WeaponType *weapon)
{
  PoolReturnWeaponMessage msg;
  msg._weapon = weapon->GetName();
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::PoolReturnMagazine(Magazine *magazine)
{
  PoolReturnMagazineMessage msg;
  msg._magazine = magazine;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}

void NetworkClient::PoolReturnBackpack(CreatorId creator, int id, RString typeName)
{
  PoolReturnBackpackMessage msg;
  msg._creator = creator;
  msg._id = id;
  msg._typeName = typeName;
  SendMsg(&msg, NMFGuaranteed| NMFHighPriority);
}


int NetworkClient::ConsoleF(const char *format, ...)
{
  #if _ENABLE_DEDICATED_SERVER
  if (!IsDedicatedClient()) return 0;
  int VConsoleF(const char *format, va_list argptr);

  va_list arglist;
  va_start(arglist, format);

  int result = VConsoleF(format, arglist);
  
  va_end( arglist );
  return result;
  #else
  return 0;
  #endif
}

/*!
\patch 5175 Date 10/16/2007 by Jirka
- Fixed: Chat message length limited to 150 characters 
*/

void NetworkClient::Chat(int channel, RString text)
{
  ChatMessage msg;
  msg._channel = channel;
  msg._name = GetLocalPlayerName();
  msg._text = text.Substring(0, MaxChatLength);
  SendMsg(&msg, NMFGuaranteed|GetChatPriority());
}

void NetworkClient::Chat(int channel, AIBrain *sender, RefArray<NetworkObject> &units, RString text)
{
  ChatMessage msg;
  msg._channel = channel;
  msg._sender = sender;
  msg._units = units;
  msg._name = GetLocalPlayerName();
  msg._text = text.Substring(0, MaxChatLength);
  SendMsg(&msg, NMFGuaranteed|GetChatPriority());
}

void NetworkClient::Chat(int channel, RString sender, RefArray<NetworkObject> &units, RString text)
{
  ChatMessage msg;
  msg._channel = channel;
  msg._sender = NULL;
  msg._units = units;
  msg._name = sender;
  msg._text = text.Substring(0, MaxChatLength);
  SendMsg(&msg, NMFGuaranteed|GetChatPriority());
}

void NetworkClient::LocalizedChat(int channel, const LocalizedFormatedString &text)
{
  LocalizedChatMessage msg;
  msg._channel = channel;
  msg._name = GetLocalPlayerName();
  msg._args = text._args;
  SendMsg(&msg, NMFGuaranteed|GetChatPriority());
}


void NetworkClient::LocalizedChat(int channel, AIBrain *sender, RefArray<NetworkObject> &units, const LocalizedFormatedString &text)
{
  LocalizedChatMessage msg;
  msg._channel = channel;
  msg._sender = sender;
  msg._units = units;
  msg._name = GetLocalPlayerName();
  msg._args = text._args;
  SendMsg(&msg, NMFGuaranteed|GetChatPriority());
}

void NetworkClient::LocalizedChat(int channel, RString sender, RefArray<NetworkObject> &units, const LocalizedFormatedString &text)
{
  LocalizedChatMessage msg;
  msg._channel = channel;
  msg._sender = NULL;
  msg._units = units;
  msg._name = sender;
  msg._args = text._args;
  SendMsg(&msg, NMFGuaranteed|GetChatPriority());
}


void NetworkClient::RadioChat(int channel, AIBrain *sender, RefArray<NetworkObject> &units, RString text, RadioSentence &sentence)
{
  RadioChatMessage msg;
  msg._channel = channel;
  msg._sender = sender;
  msg._units = units;
  msg._text = text;
  msg._sentence = sentence;
  msg._wordsClass = sentence.wordsClass;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::RadioChatWave(int channel, RefArray<NetworkObject> &units, RString wave, AIBrain *sender, RString senderName)
{
  RadioChatWaveMessage msg;
  msg._channel = channel;
  msg._units = units;
  msg._wave = wave;
  msg._sender = sender;
  msg._senderName = senderName;
  SendMsg(&msg, NMFGuaranteed);
}

/// Notice VoNChatChannel has CCDirect on zero position (zero value is swapped with CCDirect)
/// this is in order to keep vonApp.hpp without knowledge of ChatChannels which are engine specific
VoNChatChannel ChatChannel2VoNChatChannel(int channel)
{
  if (channel == 0) return (VoNChatChannel)CCDirect;
  if (channel == CCDirect) return (VoNChatChannel)0;
  return (VoNChatChannel)channel;
}

/// inverse function to ChatChannel2VoNChatChannel
ChatChannel VoNChatChannel2ChatChannel(int channel)
{
  if (channel == 0) return CCDirect;
  if (channel == CCDirect) return (ChatChannel)0;
  return (ChatChannel)channel;

}

void NetworkClient::SetVoiceChannel(int channel)
{
#if !_GAMES_FOR_WINDOWS && !defined _XBOX
 if (_client)  _client->VoiceCapture(channel!=CCNone,_parent->VoNCodecQuality());
  /// reset disable voice request
  GNetworkManager.SetDisableVoiceRequest(NetworkManager::DVRNone);
#endif
#if VOICE_OVER_NET && _ENABLE_MP
  if (_voiceChannel!=channel)
    VoiceSetChatChannel(ChatChannel2VoNChatChannel(channel));
#endif
  _voiceChannel = channel;
}

void NetworkClient::SetVoNPosition(int dpnid, Vector3Par pos, Vector3Par speed)
{
  if (_client) _client->SetVoNPosition(dpnid, pos, speed);
}

void NetworkClient::SetVoNPlayerPosition(Vector3Par pos, Vector3Par speed)
{
  if (_client) _client->SetVoNPlayerPosition(pos, speed);
}

void NetworkClient::SetVoNObstruction(int dpnid, float obstruction, float occlusion, float deltaT)
{
  if (_client) _client->SetVoNObstruction(dpnid, obstruction, occlusion, deltaT);
}

void NetworkClient::UpdateVoNVolumeAndAccomodation(int dpnid, float volume, float accomodation)
{
  if (_client) _client->UpdateVoNVolumeAndAccomodation(dpnid, volume, accomodation);
}

void NetworkClient::AdvanceAllVoNSounds(float deltaT, bool paused, bool quiet)
{
  if (_client) _client->AdvanceAllVoNSounds(deltaT, paused, quiet);
}

void NetworkClient::SetRoleIndex(AIUnit *unit, int roleIndex)
{
  SetRoleIndexMessage msg;
  msg._unit = unit;
  msg._roleIndex = roleIndex;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::RevealTarget(int to, AIGroup *grp, EntityAI *target)
{
  RevealTargetMessage msg;
  msg._to = to;
  msg._group = grp;
  msg._target = target;
  SendMsg(&msg, NMFGuaranteed);
}

/*!
Note: Files can be transferred only to players temporary folder on server -
path dest format must be "tmp\\players\\" + playername + "\\relativepath".
Transferred file size must not be bigger than MaxCustomFileSize.
All of this is enforced server side and clients violating this are kicked.
This is done to provide some reasonable level of security.
Otherwise fake clients would be able to write anywhere on the server disc.
\patch_internal 1.82 Date 8/19/2002 by Ondra
- Improved: Server-side check of tranferred file path and size.
*/

void NetworkClient::TransferFileToServer(RString dest, RString source)
{
  int maxSegmentSize = 512 - 50;

  QIFStreamB f;
  f.AutoOpen(source);

  TransferFileToServerMessage msg;
  msg._path = dest;
  msg._totSize = f.rest();
  msg._totSegments = (msg._totSize + maxSegmentSize - 1) / maxSegmentSize;
  msg._offset = 0;
  for (int i=0; i<msg._totSegments; i++)
  {
    msg._curSegment = i;
    int size = std::min(maxSegmentSize, msg._totSize - msg._offset);
    msg._data.Resize(size);
    f.read(msg._data.Data(), size);
    SendMsg(&msg, NMFGuaranteed);
    msg._offset += size;
  }
}

void NetworkClient::GetTransferStats(int &curBytes, int &totBytes)
{
  curBytes = 0; totBytes = 0;
  for (int i=0; i<_files.Size(); i++)
  {
    curBytes = _files[i].received;
    totBytes = _files[i].fileData.Size();
  }
}

NetworkLocalObjectInfo *NetworkClient::GetLocalObjectInfo(const NetworkId &id)
{
  const NetworkLocalObjectInfo *info = _localObjects.get(id);
  return unconst_cast(info);
}

NetworkRemoteObjectInfo *NetworkClient::GetRemoteObjectInfo(const NetworkId &id)
{
  const NetworkRemoteObjectInfo *info = _remoteObjects.get(id);
  return unconst_cast(info);
}

NetworkObject *NetworkClient::GetObject(const NetworkId &id)
{
  if ( id.creator == CreatorId(0) ) return NULL;
  else if ( id.creator == CreatorId(STATIC_OBJECT) )
  {
    ObjectId oid;
    oid.Decode(id.id);
    return GLandscape->GetObject(oid);
  }
  else
  {
    NetworkRemoteObjectInfo *infoR = GetRemoteObjectInfo(id);
    if (infoR) return infoR->object;
    NetworkLocalObjectInfo *infoL = GetLocalObjectInfo(id);
    if (infoL) return infoL->object;
    return NULL;
  }
}

void NetworkClient::DeleteObject(const NetworkId &id, bool sendMsg)
{
  if (id.IsNull()) return;

  if (sendMsg)
  {
    DeleteObjectMessage msg;
    msg._creator = id.creator;
    msg._id = id.id;
    SendMsg(&msg, NMFGuaranteed);
  }

#if CHECK_MSG
  CheckLocalObjects();
#endif

#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsBotClient()) _hla.DeleteObject(id);
#endif

  if (_localObjects.removeKey(id))
  {
    if (DiagLevel >= 1)
      DiagLogF("Client: local object destroyed (DeleteObject function) %d:%d", id.creator.CreatorInt(), id.id);
    return;
  }
  LogF("Warning: Client: Object info %d:%d not found.", id.creator.CreatorInt(), id.id);

#if CHECK_MSG
  CheckLocalObjects();
#endif
}

void NetworkClient::ForceDeleteObject(NetworkObject *object)
{
  if (object->IsLocal())
  {
    NetworkId nid=object->GetNetworkId();
    DeleteObject(nid, true);
  }
  else
  {
    DestroyRemoteObject(object->GetNetworkId());
    ForceDeleteObjectMessage msg;
    msg._object = object;
    SendMsg(&msg, NMFGuaranteed);
  }
}

void NetworkClient::ShowTarget(Person *vehicle, TargetType *target)
{
  if (_clientState < NCSBriefingRead) return; // send event messages only when playing

  ShowTargetMessage msg;
  msg._vehicle = vehicle;
  msg._target = target;
  SendMsg(&msg, NMFGuaranteed); 
}

void NetworkClient::ShowGroupDir(Person *vehicle, Vector3Par dir)
{
  if (_clientState < NCSBriefingRead) return; // send event messages only when playing

  ShowGroupDirMessage msg;
  msg._vehicle = vehicle;
  msg._dir = dir;
  SendMsg(&msg, NMFGuaranteed); 
}

//! Subsidiary structure for sorting messages by error
struct UpdateLocalObjectInfo
{
  //! Local object info
  NetworkLocalObjectInfo *oInfo;
  //! Message class type
  NetworkMessageClass cls;
  //! Error (difference)
  float error;
  //! Critical update
  bool criticalUpdate;
};
TypeIsSimple(UpdateLocalObjectInfo)

//! Compare local object infos by error
int CmpLocalObjects
(
  const UpdateLocalObjectInfo *info1,
  const UpdateLocalObjectInfo *info2
)
{
  float diff = info2->error - info1->error;
  return sign(diff);
}

//! Format value of error for output
/*!
\param val value to format
\param buffer output buffer
\return pointer to output buffer
*/
const char *FormatVal(float val, char *buffer)
{
  if (val == FLT_MAX)
    strcpy(buffer, "MAX");
  else
    sprintf(buffer, "%.0f", val);
  return buffer;
}

void NetworkClient::EstimateBandwidth(int &nMsgMax, int &nBytesMax)
{
  if (IsBotClient())
  {
    // unlimited bandwidth
    nMsgMax = INT_MAX;
    nBytesMax = INT_MAX;
  }
  else
  {
  #if _ENABLE_MP
    int nMsg = 0, nBytes = 0, nMsgG = 0, nBytesG = 0;
    _client->GetSendQueueInfo(nMsg, nBytes, nMsgG, nBytesG);
    if ( nMsgG > 0 )    // new style?
    {
        nMsg += nMsgG;
        nBytes += nBytesG;
    }
  #endif
/*
  #if _ENABLE_CHEATS
    if (outputDiags%NOutputDiags == ODMsgQueue && nMsg > 0) sprintf(output, "Waiting (%d, %d); ", nBytes, nMsg);
  #endif
*/
    nMsgMax = MaxMsgSend;
    nBytesMax = 1024;

    #if _ENABLE_MP
    int latencyMS, throughputBPS;
    if (_client->GetConnectionInfo(latencyMS, throughputBPS))
    {
      // nBytesMax += 1.25 * info.dwThroughputBPS;
      nBytesMax += throughputBPS;
      nBytesMax += throughputBPS >> 2; // info.dwThroughputBPS / 4
/*
  #if _ENABLE_CHEATS
      if (outputDiags%NOutputDiags == ODMsgQueue) sprintf(output + strlen(output), "%d ms, %d bps; ", latencyMS, throughputBPS*8);
  #endif
*/
      if (_connectionQuality < CQPoor)
      {
        if (latencyMS >= 500) _connectionQuality = CQPoor;
        // if (throughputBPS < 3000) _connectionQuality = CQPoor;
      }
    }
    int nPlayers = _identities.Size() - 1;
  //  if (nPlayers > 0) saturateMax(nBytesMax, 14400 / nPlayers);
    if (nPlayers > 0) saturate(nBytesMax, MinBandwidth/8, MaxBandwidth/8);

    nBytesMax = toInt(0.001 * GEngine->GetAvgFrameDuration() * nBytesMax);

    nBytesMax -= nBytes;
    nMsgMax -= nMsg;
    #endif

  #if _ENABLE_CHEATS
    if (outputLogs) LogF("Client: Limit (%d, %d)", nBytesMax, nMsgMax);
/*
    if (outputDiags%NOutputDiags == ODMsgQueue) sprintf(output + strlen(output), "Limit (%d, %d); ", nBytesMax, nMsgMax);
*/
  #endif
  }
}

void NetworkClient::CreateObjectsList(AutoArray<UpdateLocalObjectInfo, MemAllocSA> &objects)
{
  PROFILE_SCOPE_EX(crOLC, network)
  float maxError = 0, sumError = 0;
  for (LocalObjectsContainer::Iterator it=_localObjects.begin(); it; _localObjects.next(it))
  {
    NetworkLocalObjectInfo &oInfo = *it;
    DoAssert(oInfo.object.IsNull() || oInfo.id == oInfo.object->GetNetworkId());
    for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
    {
      NetworkUpdateInfo &info = oInfo.updates[j];

      float error;
      if (info.lastCreatedMsg)
      {
        // message on the way
        continue;
      }
      else
      {
        NetworkMessageType type = oInfo.object->GetNMType((NetworkMessageClass)j);
        if (type == NMTNone) continue;
        if (type == NMTUpdateClientInfoDpid) continue;
        
        if (!info.lastSentMsg)
        {
          // error = FLT_MAX;
          error = oInfo.object->CalculateErrorInitial((NetworkMessageClass)j);
#if LOG_CLIENT_ERRORS
        if (!(error < LogClientErrorLimit)) // handle NaN well
        {
          DiagLogF("Initial error for %s %d:%d (Client)", cc_cast(GetMsgTypeName(type)), oInfo.id.creator.CreatorInt(), oInfo.id.id);
          DiagLogF("  - error %.3f", error);
        }
#endif
        }
        else
        {
        /*
          NetworkMessageFormatBase *format = GetFormat(type);
          if (!format)
          {
            RptF("Client: Bad message %d(%s) format", (int)type, cc_cast(GetMsgTypeName(type)));
            continue;
          }
          */
          NetworkMessageError errorList;
          NetworkMessageContextWithError ctx(info.lastSentMsg, this, TO_SERVER, MSG_RECEIVE, errorList);
          ctx.SetClass((NetworkMessageClass)j);
          error = oInfo.object->CalculateError(ctx);
#if LOG_CLIENT_ERRORS
          if (!(error < LogClientErrorLimit))
          {
            DiagLogF
            (
              "Object %s %d:%d (Client)",
              cc_cast(GetMsgTypeName(type)), oInfo.id.creator.CreatorInt(), oInfo.id.id
            );
            DiagLogF
            (
              "  - error %.3f", error
            );
            float dummy = oInfo.object->CalculateError(ctx);
            DoAssert(dummy == error);
          }
#endif
          #if _ENABLE_REPORT
            if (!_finite(error))
            {
              RptF(
                "Client: Infinite error %g detected on %s",
                error,(const char *)oInfo.object->GetDebugName()
              );
              // recalculate: debuggin opportunity
              oInfo.object->CalculateError(ctx);
            }
          #endif
        }
        // note: following condition seems to be identical to error>MinErrorToSendNear
        // but it is different when error is NaN
        if (!(error <= MinErrorToSendNear))
        {
          int index = objects.Add();
          objects[index].oInfo = &oInfo;
          objects[index].cls = (NetworkMessageClass)j;
          objects[index].error = error;
          objects[index].criticalUpdate = false;
          #if _ENABLE_REPORT
            if (!_finite(error))
            {
              RptF(
                "Client: Infinite error error %g, caught on %s",
                error,(const char *)oInfo.object->GetDebugName()
              );
            }
          #endif
        }
        saturateMax(maxError, error);
        sumError += error;
      }
    }
  }
/*
#if _ENABLE_CHEATS
  if (outputDiags%NOutputDiags == ODMsgQueue && sumError > 0)
  {
    char buf1[32], buf2[32];
    sprintf
    (
      output + strlen(output),
      "Err %s / %s",
      FormatVal(maxError, buf1),
      FormatVal(sumError, buf2)
    );
    GlobalShowMessage(1000, output);
  }
#endif
*/

  // sort local objects by errors
  QSort(objects.Data(), objects.Size(), CmpLocalObjects);
}

static void OnUpdateSent(NetworkMessageQueueItem *item, DWORD dwMsgId)
{
  if (item->msg->objectUpdateInfo)
  {
#if LOG_SEND_PROCESS
    LogF("  - update info %x updated", item.msg->objectUpdateInfo);
#endif
    if (item->msg->objectUpdateInfo->lastCreatedMsg != item->msg)
    {
      RptF("Last created message is bad:");
      RptF("  sending message: %x (type %s), id = %x", item->msg.GetRef(), cc_cast(GetMsgTypeName(item->type)), dwMsgId);
      RptF("  attached object info: %x", item->msg->objectUpdateInfo);
      RptF("  last created message: %x, id = %x", item->msg->objectUpdateInfo->lastCreatedMsg.GetRef(), item->msg->objectUpdateInfo->lastCreatedMsgId);
      RptF("  last sent message: %x", item->msg->objectUpdateInfo->lastSentMsg.GetRef());

      item->msg->objectUpdateInfo->lastCreatedMsg = item->msg;
    }
    item->msg->objectUpdateInfo->lastCreatedMsgId = dwMsgId;
    if (dwMsgId == 0xffffffff)
    {
      item->msg->objectUpdateInfo->lastCreatedMsg = NULL;
      item->msg->objectUpdateInfo->lastCreatedMsgTime = 0;
    }
  }
}

void NetworkClient::SendMessages()
{
  // calculate limits
  int nMsg = 0, nBytes = 0;
  int nMsgMax, nBytesMax;
  
  EstimateBandwidth(nMsgMax, nBytesMax);

  // send enqueued guaranteed messages

  const int nMsgMaxGuaranteed = nMsgMax;
  const int nBytesMaxGuaranteed = nBytesMax;

  int maxSize = MaxSizeGuaranteed;
  while (_messageQueue.Size() > 0)
  {
    if (nMsg >= nMsgMaxGuaranteed || nBytes >= nBytesMaxGuaranteed)
    {
/*
#if _ENABLE_CHEATS
      if (outputDiags%NOutputDiags == ODMsgQueue)
      {
        strcat(output, "Limit reached");
        GlobalShowMessage(1000, output);
      }
#endif
*/
      break;
    }

    NetworkMessage *msg = _messageQueue[0]->msg;
    int size = msg->size;
    if (size >= maxSize)
    {
      RefR<NetMessage, RefCountSafe> resultMsg;
      DWORD dwMsgId = SendMsgRemote(TO_SERVER, msg, _messageQueue[0]->type, NMFGuaranteed|NMFStatsAlreadyDone, &resultMsg, NULL);
      _messageQueue[0]->SetFence(resultMsg);
      (void)dwMsgId; // not used, but may be usefull for debugging
      _messageQueue.Delete(0);
      nMsg++;
      nBytes += size;
    }
    else
    {
      int totalSize = 0;
      int i;
      for (i=0; i<_messageQueue.Size(); i++)
      {
        NetworkMessage *msg = _messageQueue[i]->msg;
        int size = msg->size;
        if (totalSize + size > maxSize) break;
        totalSize += size;
      }
      DWORD dwMsgId = SendMsgQueue(TO_SERVER, _messageQueue, 0, i, NMFGuaranteed);
      (void)dwMsgId; // not used, but may be usefull for debugging
      _messageQueue.Delete(0, i);
      nMsg++;
      nBytes += totalSize;
    }
  }

  AUTO_STATIC_ARRAY(UpdateLocalObjectInfo, objects, 256);

#define ENABLE_CRITICAL_UPDATES 1

#if !ENABLE_CRITICAL_UPDATES
  if (nMsg >= nMsgMax || nBytes >= nBytesMax)
  {
  #if _ENABLE_CHEATS
    if (outputLogs) LogF("  Client: limit reached (guaranteed)");
  #endif
    goto DoNotSendUpdates;
  }
#endif

  if (_clientState == NCSBriefingRead)
  {
  #if CHECK_MSG
    CheckLocalObjects();
  #endif

    // delete destroyed objects - keep order
    for (LocalObjectsContainer::Iterator it=_localObjects.begin(); it; _localObjects.next(it))
    {
      NetworkLocalObjectInfo &info = *it;
      if (!info.object)
      {
#if _VBS3
      //! Added - No other way to tell client localy to delete unit.
      #if _EXT_CTRL //ext. Controller like HLA, AAR
              if(IsBotClient()) _hla.DeleteObject(info.id);
      #endif
#endif

        DeleteObjectMessage msg;
        msg._creator = info.id.creator;
        msg._id = info.id.id;
        SendMsg(&msg, NMFGuaranteed);
        if (DiagLevel >= 1)
          DiagLogF("Client: local object destroyed (object not found) %d:%d", info.id.creator.CreatorInt(), info.id.id);
        _localObjects.removeKey(info.id); // safe - value replaced by a zombie
        continue;
      }
    }

  #if CHECK_MSG
    CheckLocalObjects();
  #endif

    // update errors of local objects
    CreateObjectsList(objects);

#if DEBUG_GIVEN_NETWORK_ID
  for (int i=0; i<objects.Size(); i++)
  {
    NetworkLocalObjectInfo &info = *objects[i].oInfo;
    float error = objects[i].error;
    if (DebugMeBoy(info.id))
    {
      float times[NMCUpdateN];
      for (int j=0; j<NMCUpdateN; j++) times[j] = info.updates[j].lastSentMsg ? info.updates[j].lastSentMsg->time.toFloat() : 0;
      LogF("*** DebugMeBoy: {%d:%d} has error %.2f, position in _objects = %d, lastMsgSent: %.02f, %.02f, %.02f", 
          info.id.creator.CreatorInt(), info.id.id, error, i, times[NMCUpdateGeneric], times[NMCUpdatePosition], times[NMCUpdateDamage]);
    }
  }
#endif

    // create and send critical updates     
  #if ENABLE_CRITICAL_UPDATES
    for (int i=0; i<objects.Size(); i++)
    {
      NetworkLocalObjectInfo &info = *objects[i].oInfo;
      NetworkMessageClass cls = objects[i].cls;
      DoAssert(info.id==info.object->GetNetworkId());
      float error = objects[i].error;

      // any message about dammage that transmit change
      // that could mean dead should be considered high priority
      if
      (
        (cls != NMCUpdateDamage || error < ERR_COEF_STRUCTURE) && error<ERR_COEF_IMMEDIATE
      ) continue;

      objects[i].criticalUpdate = true;
      int bytes;
      if (!info.object || !info.object->_fence || info.object->_fence->WasReceived())
      {
        bytes = UpdateObject(info.object, cls, NMFGuaranteed | NMFHighPriority);
      }
      else
      {
        bytes = UpdateObject(info.object, cls, NMFGuaranteed);
      }

    #if _ENABLE_CHEATS
      if (outputLogs)
      {
        LogF
        (
          "  Client: Object %d:%d updated - size %d bytes, critical",
          info.id.creator.CreatorInt(), info.id.id, bytes
        );
      }
    #endif

      if (bytes >= 0)
      {
        // no error
        nMsg++;
        nBytes += bytes;
      }
    }

    if (nMsg >= nMsgMax || nBytes >= nBytesMax)
    {
    #if _ENABLE_CHEATS
      if (outputLogs) LogF("  Client: limit reached (guaranteed)");
    #endif
      goto DoNotSendUpdates;
    }

  #endif
  }

DoNotSendUpdates:
  // send nonguaranteed enqueued messages
  maxSize = MaxSizeNonguaranteed;
  int next = 0; // index of next object to update
  bool empty = false;
  while (true)
  {
    // enforce send last message
    if (!empty || _messageQueueNonGuaranteed.Size() <= 0)
    {
      if (nMsg >= nMsgMax || nBytes >= nBytesMax) break;
    }

    if (_messageQueueNonGuaranteed.Size() <= 0)
    {
      empty = true;
      // starving, try to create further message
int oldNext = next;
      if (!PrepareNextUpdate(objects, next)) break;
      // local communication - message was already sent
      if (_parent->IsServer())
      {
        DoAssert(_messageQueueNonGuaranteed.Size() == 0);
        nMsg++;
        continue;
      }

      // DoAssert(_messageQueueNonGuaranteed.Size() > 0);
      // Temporary FIX:
      if (_messageQueueNonGuaranteed.Size() == 0)
      {
RptF("PrepareNextUpdate - no message was prepared - next %d, object 0x%x", oldNext, &objects[oldNext]);
PrepareNextUpdate(objects, oldNext);        
        nMsg++;
        continue;
      }
    }
    
    NetworkMessageQueueItem *item = _messageQueueNonGuaranteed[0];
    int size = item->msg->size;
    // enforce send last message
    if (size >= maxSize || nMsg >= nMsgMax || nBytes >= nBytesMax)
    {
      RefR<NetMessage, RefCountSafe> resultMsg;
      DWORD dwMsgId = SendMsgRemote(TO_SERVER, item->msg, item->type, NMFStatsAlreadyDone, &resultMsg, item->dependOn);
      //item->SetFence(resultMsg); //nonguaranteed messages could not raise a fence
#if LOG_SEND_PROCESS
      LogF("Client: Message %x sent", dwMsgId);
#endif
      OnUpdateSent(item, dwMsgId);
      _messageQueueNonGuaranteed.Delete(0);
      nMsg++;
      nBytes += size;
    }
    else
    {
      int totalSize = 0; //controls the cumulative size of agregated message
      int i;
      for (i=0; true; i++)
      {
        if (i >= _messageQueueNonGuaranteed.Size())
        {
          empty = true;
          // starving, try to create further message
          if (!PrepareNextUpdate(objects, next)) break;
          DoAssert(!_parent->IsServer());
          DoAssert(i < _messageQueueNonGuaranteed.Size());
        }
        
        NetworkMessageQueueItem &item = *_messageQueueNonGuaranteed[i];
        int size = item.msg->size;
        if (totalSize + size > maxSize) break;
        totalSize += size;
      }

      for (int j=0; j<i; j++)
      {
        NetworkMessageQueueItem &item = *_messageQueueNonGuaranteed[j];

        if (item.dependOn)
        { //it depends on previous guaranteed update and cannot be agregated. We sent it immediately by setting begin
          RefR<NetMessage, RefCountSafe> resultMsg;
          DWORD dwMsgId = SendMsgRemote(TO_SERVER, item.msg, item.type, NMFStatsAlreadyDone, &resultMsg, item.dependOn);
          //item.SetFence(resultMsg);  //nonguaranteed messages could not raise a fence
#if LOG_SEND_PROCESS
      LogF("Client: Message %x sent", dwMsgId);
#endif
          OnUpdateSent(&item, dwMsgId);
          _messageQueueNonGuaranteed.Delete(j);
          i--; //one less items to process
          j--; //the same index contains new data
          nMsg++;
        }
      }

      if (i>0) //it may happen there were only messages with dependOn set
      {
        DWORD dwMsgId = SendMsgQueue(TO_SERVER, _messageQueueNonGuaranteed, 0, i, NMFNone);
#if LOG_SEND_PROCESS
        LogF("Client: Message %x sent", dwMsgId);
#endif
        for (int j=0; j<i; j++)
          {
          NetworkMessageQueueItem &item = *_messageQueueNonGuaranteed[j];
          OnUpdateSent(&item, dwMsgId);
          }
      _messageQueueNonGuaranteed.Delete(0, i);
      nMsg++;
      }
      nBytes += totalSize;
    }
  }
  DoAssert(!empty || _messageQueueNonGuaranteed.Size() == 0);

//  int sumError = 0; // messages not sent total error
  if (next < objects.Size())
  {
  #if _ENABLE_CHEATS
    if (outputLogs) LogF("  Client: limit reached");
  #endif
/*
    for (int i=next; i<objects.Size(); i++)
    {
      if (objects[i].error < FLT_MAX)
      {
        sumError += objects[i].error;
      }
    }
*/
  }

#if CHECK_MSG
  CheckLocalObjects();
#endif

#if _ENABLE_CHEATS
  // remove statistics info
  static AutoArray<int> texts;
  for (int i=0; i<texts.Size(); i++)
  {
    if (texts[i] >= 0) GEngine->RemoveText(texts[i]);
  }
  texts.Resize(0);

  // create statistics
  if (outputDiags == ODMsgQueue)
  {
    int count = _messageQueue.Size(), size = 0;
    for (int i=0; i<count; i++) size += _messageQueue[i]->msg->size;

    RString output = Format("Server: HLWait%3d(%5dB)", count, size);
    texts.Add(GEngine->ShowTextF(1000, output));
    texts.Add(GEngine->ShowTextF(1000, _client->GetStatistics()));
  }
#endif
}

bool NetworkClient::PrepareNextUpdate(AutoArray<UpdateLocalObjectInfo, MemAllocSA> &objects, int &next)
{
  while (next < objects.Size())
  {
    UpdateLocalObjectInfo &info = objects[next++];
    if (info.criticalUpdate) continue;

/*
    #if _ENABLE_REPORT
    int oldCount = _messageQueueNonGuaranteed.Size();
    #endif
*/

    NetworkLocalObjectInfo &oInfo = *info.oInfo;
    NetworkMessageClass cls = info.cls;
    int bytes = UpdateObject(oInfo.object, cls, NMFNone);
    if (bytes < 0) continue;

/*
    #if _ENABLE_REPORT
    if (oldCount == _messageQueueNonGuaranteed.Size())
    {
      Fail("No message added during NetworkClient::PrepareNextUpdate");
      continue;
    }
    #endif
*/
  #if _ENABLE_CHEATS
    if (outputLogs) LogF("  Client: Object updated - size %d bytes", bytes);
  #endif
    return true;
  }
  return false;
}

void NetworkClient::SetPlayerRespawnTime(float timeInterval)
{
  _missionHeader._playerRespawnTime = timeInterval;
  // if the wait on respawn is currently running, change its time
  for (int i=0; i<_respawnQueue.Size(); i++)
  {
    if (_respawnQueue[i]->IsPlayer()) 
    {
      _respawnQueue[i]->SetTime(Glob.time + timeInterval);
      break;
    }
  }
}

void NetworkClient::Respawn(Person *soldier, Vector3Par pos, float timeCoef)
{
  SECUROM_MARKER_SECURITY_ON(11)
  Time respawnTime = Glob.time + timeCoef * ( (soldier==GWorld->GetRealPlayer()) ? _missionHeader._playerRespawnTime : GetRespawnDelay() ); 
  DoAssert(soldier->IsLocal());
  AIBrain *unit = soldier->Brain();
  AIGroup *grp = unit->GetGroup();
  Assert(grp);
  if (!grp) Fail("Respawning unit with no group");

  Transport *vehicle = unit->FindRespawnVehicle();
  if (vehicle && vehicle->IsRespawning())
  {
    // delay respawn of vehicle to respawnTime
    for (int i=0; i<_respawnQueue.Size(); i++)
    {
      RespawnQueueItem *item = _respawnQueue[i];
      if (!item->IsVehicleRespawn()) continue;
      Transport *v = item->GetVehicle();
      if (v && v == vehicle)
      {
        if (respawnTime <= item->GetTime())
          respawnTime = item->GetTime();
        else
          item->SetTime(respawnTime);
        break;
      }
    }
  }

  // add to respawn queue
  _respawnQueue.Add(new RespawnPersonQueueItem(
    pos, respawnTime,
    soldier, soldier == GWorld->GetRealPlayer(), false));
LogF("*** Adding %s (%d:%d) to respawn queue", (const char *)soldier->GetPersonName(), soldier->GetNetworkId().creator.CreatorInt(), soldier->GetNetworkId().id);

  SECUROM_MARKER_SECURITY_OFF(11)
}

void NetworkClient::Respawn(Transport *vehicle, Vector3Par pos, float timeCoef)
{
  Time respawnTime = Glob.time + timeCoef * vehicle->GetRespawnDelay(); 

  AIBrain *unit = vehicle->GetRespawnUnit();
  if (unit && unit->GetLifeState() == LifeStateDeadInRespawn)
  {
    // delay respawn of vehicle to respawnTime
    for (int j=0; j<_respawnQueue.Size(); j++)
    {
      RespawnQueueItem *item = _respawnQueue[j];
      if (item->IsVehicleRespawn()) continue;
      Person *person = item->GetPerson();
      if (person && person->Brain() == unit)
      {
        if (respawnTime <= item->GetTime())
          respawnTime = item->GetTime();
        else
          item->SetTime(respawnTime);
        break;
      }
    }
  }

  // add to respawn queue
  _respawnQueue.Add(new RespawnVehicleQueueItem(
    pos, respawnTime,
    vehicle));
  LogF("*** Adding vehicle %s (%d:%d) to respawn queue", (const char *)vehicle->GetDebugName(), vehicle->GetNetworkId().creator.CreatorInt(), vehicle->GetNetworkId().id);
}

int NetworkClient::NLocalObjectsNULL() const
{
  int n = 0;
  for (LocalObjectsContainer::Iterator it=_localObjects.begin(); it; _localObjects.next(it))
  {
    NetworkLocalObjectInfo &info = *it;
    if (!info.object) n++;
  }
  return n;
}

float NetworkClient::GetLastMsgAgeReliable()
{
  if (_client) return _client->GetLastMsgAgeReliable();
  return 0;
}

void NetworkClient::JIPSelectPlayer(int dpnid)
{
  AIGroup *playerGroup = NULL;
  AIBrain *playerUnit = NULL;

LogF("Choosing player candidate");      
  int roleIndex = -1;
  for (int i=0; i<_playerRoles.Size(); i++)
    if (_playerRoles[i].player == dpnid)
    {
      roleIndex = i;
      break;
    }

LogF("  Role index %d", roleIndex);     
  if (roleIndex >= 0)
  {
    const PlayerRole &role = _playerRoles[roleIndex];

    AICenter *center = GWorld->GetCenter(role.side);
    if (center)
    {
LogF("  Center %s (%x), %d groups", (const char *)center->GetDebugName(), center, center->NGroups());     
      // find group
      for (int g=0; g<center->NGroups(); g++)
      {
        AIGroup *grp = center->GetGroup(g);
        if (grp && grp->GetRoleIndex() == role.group)
        {
          playerGroup = grp;
          break;
        }
      }
      LogF("  Group %s (%x), %d units", playerGroup ? (const char *)playerGroup->GetDebugName() : "<null>", playerGroup, playerGroup ? playerGroup->UnitsCount() : 0);      

      // find unit
      if (playerGroup)
      {
        for (int u=0; u<playerGroup->NUnits(); u++)
        {
          AIUnit *unit = playerGroup->GetUnit(u);
          if (!unit) continue;
          if (unit->GetRoleIndex() == roleIndex)
          {
            if (unit->IsAnyPlayer()) continue; // prevent spawning two players into one avatar

            // found
            playerUnit = unit;
            break;
          }
        }
LogF("  Unit %s (%x)", playerUnit ? (const char *)playerUnit->GetDebugName() : "<null>", playerUnit);     
        if (playerUnit)
        {
LogF("  Life state %s", (const char *)FindEnumName(playerUnit->GetLifeState()));
          if (playerUnit->GetLifeState() == LifeStateDead) playerUnit = NULL;
        }
        else
        {
LogF("  List of units in group:");      
          for (int u=0; u<playerGroup->NUnits(); u++)
          {
            AIUnit *unit = playerGroup->GetUnit(u);
            if (unit)
LogF("    %s (%x): roleIndex %d", (const char *)unit->GetDebugName(), unit, unit->GetRoleIndex());      
          }
        }
      }
    }
#if _ENABLE_INDEPENDENT_AGENTS
    else // animal or other independent AIAgent
    {
      AIAgent *agent = GWorld->FindAgent(roleIndex);
      if (agent && agent->GetLifeState()!=LifeStateDead) playerUnit = agent;
    }
#endif
  }

  bool local = true;
  if (playerUnit) local = playerUnit->IsLocal();
  else if (playerGroup) local = playerGroup->IsLocal();

  if (local) JoinIntoUnit(dpnid, roleIndex, playerUnit, playerGroup);
  else
  {
    JoinIntoUnitMessage msg;
    msg._dpnid = dpnid;
    msg._roleIndex = roleIndex;
    msg._unit = playerUnit;
    msg._group = playerGroup;
    SendMsg(&msg, NMFGuaranteed);
  }
}

static const ArcadeGroupInfo &FindGroupInfo(const ArcadeTemplate &t, TargetSide side, int group)
{
  for (int i=0; i<t.groups.Size(); i++)
  {
    if (t.groups[i].side != side) continue;
    if (group == 0) return t.groups[i];
    group--;
  }
  Fail("Unaccessible");
  return *(const ArcadeGroupInfo *)NULL;
}

AIBrain *NetworkClient::CreateUnit(AIGroup *grp, int roleIndex)
{
LogF("Current mission: %s", (const char *)CurrentTemplate.intel.briefingName);

  const PlayerRole *role = GetPlayerRole(roleIndex);
  if (!role)
  {
    LogF(" - role not found");
    return NULL;
  }

  bool newGroup = grp == NULL;
  AICenter *center = GWorld->GetCenter(role->side);
  if (!center)
  {
    LogF(" - center not found");
    return NULL;
  }
  if (newGroup)
  {
    if (center->NGroups() >= MaxGroups)
    {
      LogF(" - cannot create group - maximal number of groups reached");
      return NULL;
    }
    grp = new AIGroup();
    center->AddGroup(grp);
    grp->SetRoleIndex(role->group);
  }
  
  const ArcadeGroupInfo &gInfo = FindGroupInfo(CurrentTemplate, role->side, role->group);
  if (!(&gInfo)) return NULL;
  DoAssert(role->unit < gInfo.units.Size());
  const ArcadeUnitInfo &uInfo = gInfo.units[role->unit];

  Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
  if (!type) return NULL;

  AIBrain *agent = NULL;
  bool soldier = type->IsPerson();
  if (soldier)
  { 
    agent = center->CreateUnit(uInfo, CurrentTemplate, true, grp);
  }
  else
  {
    TransportType *transportType = dynamic_cast<TransportType *>(type.GetRef());
    if (!transportType) return NULL;

    RString nameType = transportType->GetCrew();
    Person *soldier = dyn_cast<Person>(GWorld->NewVehicleWithID(nameType));
    // FIX: better handling of invalid crew specification
    if (!soldier)
    {
      RptF("Invalid crew %s", (const char*)nameType);
      return NULL;
    }
    soldier->SetTargetSide(role->side);

    agent = new AIUnit(soldier);
    soldier->SetBrain(agent);

    // Add into World
    // maintain correct position in hierarchy
    Vector3 pos = uInfo.position;
    agent->FindNearestEmpty(pos);
    pos[1] = GLandscape->RoadSurfaceYAboveWater(pos + VUp * 0.5f);
    soldier->SetPosition(pos);

    // we are creating the soldier inside of some vehicle
    GWorld->AddVehicle(soldier);
    CreateVehicle(soldier, VLTVehicle, uInfo.name, uInfo.id);

    AIUnitInfo &info = soldier->GetInfo();
    info._rank = uInfo.rank;
    info._initExperience = info._experience = AI::ExpForRank(uInfo.rank);
    AIUnit *unit = agent->GetUnit();
    if (unit) grp->AddUnit(unit);
  }
  agent->SetRoleIndex(roleIndex);

  AIUnit *unit = agent->GetUnit();
  if (unit)
  {
    if (newGroup)
    {
      grp->SelectLeader(unit);
      grp->PrepareGroup(gInfo, true, false);

      CreateObject(grp);
    }

    AISubgroup *subgrp = unit->GetSubgroup();
    DoAssert(subgrp->GetGroup() == grp);

    if (grp->IsLocal())
    {
      grp->CalculateMaximalStrength();

      if (role->leader && grp->Leader() != unit)
      {
        grp->SelectLeader(unit);
        LogF("  Creating unit %s - marked as leader", (const char *)unit->GetDebugName());
      }
      else
      {
        center->SelectLeader(grp);
        subgrp->SelectLeader();
        LogF("  Creating unit %s - leader is now %s", (const char *)unit->GetDebugName(), (const char *)grp->Leader()->GetDebugName());
      }
    }
    else
    {
      LogF("  Creating unit %s - group is remote - cannot change leader", (const char *)unit->GetDebugName());
    }
  }

  if (soldier && uInfo.init.GetLength() > 0)
  {
    agent->GetVehicle()->OnVehicleInit(uInfo.init);
  }

  AIUnitInfo &info = agent->GetPerson()->GetInfo();
  info._initExperience = info._experience;

// RptF("  Unit %s created - experience %.0f (init %.0f)", (const char *)unit->GetDebugName(), unit->GetPerson()->GetExperience(), unit->GetPerson()->GetInfo()._initExperience);
  
  if (unit)
  {
    AISubgroup *subgrp = unit->GetSubgroup();
    if (subgrp->GetNetworkId().IsNull())
    {
      // new subgroup
      CreateObject(subgrp);
  // RptF("* Subgroup %d:%d created", subgrp->GetNetworkId().creator.CreatorInt(), subgrp->GetNetworkId().id);
    }
    else
    {
  // RptF("* Subgroup %d:%d already exists", subgrp->GetNetworkId().creator.CreatorInt(), subgrp->GetNetworkId().id);
    }
  }
  CreateObject(agent);
// RptF("* Unit %d:%d created", unit->GetNetworkId().creator.CreatorInt(), unit->GetNetworkId().id);
  AttachPerson(agent->GetPerson());

  if (soldier && uInfo.init.GetLength() > 0)
  {
    VehicleInitMessage cmd;
    cmd._vehicle = agent->GetVehicle();
    cmd._init = uInfo.init;
    VehicleInit(cmd);
  }

  UpdateObject(unit->GetPerson(), NMFGuaranteed);
  UpdateObject(unit, NMFGuaranteed);
  if (unit)
  {
    UpdateObject(unit->GetSubgroup(), NMFGuaranteed);
    UpdateObject(grp, NMFGuaranteed);
  }

/*
RptF
(
  "* Unit %d:%d has subgroup %d:%d",
  unit->GetNetworkId().creator.CreatorInt(), unit->GetNetworkId().id,
  subgrp->GetNetworkId().creator.CreatorInt(), subgrp->GetNetworkId().id
);
*/
  return agent;
}

void NetworkClient::JoinIntoUnit(int dpnid, int roleIndex, AIBrain *playerUnit, AIGroup *playerGroup)
{
  if (playerUnit)
  {
    DoAssert(playerUnit->IsLocal());
  }
  else if (playerGroup)
  {
    DoAssert(playerGroup->IsLocal());
  }

  if (!playerUnit && roleIndex >= 0)
  {
    // unit with given role not found, find another candidate depending on respawn type
    switch (_missionHeader._respawn)
    {
    case RespawnNone:
    case RespawnSeaGull:
      // nothing to do, seagull will be created
      break;
    case RespawnAtPlace:
    case RespawnInBase:
      // create new unit
      playerUnit = CreateUnit(playerGroup, roleIndex);
      break;
    case RespawnToGroup:
      // find other unit in group
      {
        const PlayerRole *role = GetPlayerRole(roleIndex);
        if (playerGroup && role->GetFlag(PRFEnabledAI))
        {
LogF("  Respawn in group - searching for respawn candidate");   
          for (int i=0; i<playerGroup->NUnits(); i++)
          {
            AIUnit *unit = playerGroup->GetUnit(i);
            if (!unit || !unit->LSIsAlive()) continue;
            if (!unit->IsPlayable()) continue;
            Person *person = unit->GetPerson();
            if (!person->IsLocal()) continue;
            if (person->IsNetworkPlayer()) continue;
            if (person->IsDamageDestroyed()) continue;
            playerUnit = unit;
LogF("  Respawn in group into %s (%x)", (const char *)playerUnit->GetDebugName(), playerUnit);      
            break;
          }
        }
        else playerUnit = CreateUnit(playerGroup, roleIndex);
      }
      break;
    case RespawnToFriendly:
      // maybe we can find other playable unit to join into
      break;
    }
  }

  // sign as player
  Person *person = playerUnit ? playerUnit->GetPerson() : NULL;
LogF("  Person %s", person ? (const char *)person->GetDebugName() : "<null>");      
    
  // switch player
  SelectPlayer(dpnid, person, false);

/*
  // join into game
  // transfer message from bot client to ensure it arrive after all other messages from bot client
  // (for example SelectPlayerMessage)

  PlayerClientStateMessage msg;
  msg._dpnid = dpnid;
  msg._clientState = NCSGameLoaded;
  SendMsg(&msg, NMFGuaranteed);
*/
}

class NetworkClient::RemoveSeaGull
{
  NetworkClient *_client;
  int _dpnid;
  
  public:
  RemoveSeaGull(NetworkClient *client, int dpnid):_client(client),_dpnid(dpnid)
  {
  }
  bool operator () (Entity *animal) const
  {
    SeaGullAuto *seagull = dyn_cast<SeaGullAuto>(animal);
    if (seagull && seagull->GetPlayer() == _dpnid)
    {
      _client->ForceDeleteObject(seagull);
      seagull->DestroyObject();
      return true;
    }
    return false;
  }
};

void NetworkClient::CleanupPlayer(int dpnid, NetworkId id)
{
  NetworkObject *obj = NULL;
  if (!id.IsNull())
  {
    obj = GetObject(id);
    if (!obj)
    {
      RptF("Warning: Cleanup player - person %d:%d not found", id.creator.CreatorInt(), id.id);
    }
  }

  // untouch from multiplayer statistics
  RefArray<AIStatsMPRow> &table = GStats._mission._tableMP;
  for (int i=0; i<table.Size(); i++)
    if (table[i]->_dpnid == dpnid)
    {
/*
      DeleteObject(table[i]->GetNetworkId(), true);
      table.Delete(i);
*/
      table[i]->_role = -1;
      UpdateObject(table[i], NMFGuaranteed);
      break;
    }

  // Changed: there is no need to add a new row to multiplayer statistics (it is added after the AI scores)

  // remove attached seagull
  GWorld->ForEachAnimal(RemoveSeaGull(this,dpnid));
}

#if _XBOX_SECURE && _ENABLE_MP
void NetworkClient::AddRecentPlayer(PlayerIdentity &identity)
{
  const int maxRecentPlayers = 10;

  if (identity.xuid == INVALID_XUID) return;

  // Check if player is already in list
  int n = _recentPlayers.Size();
  for (int i=0; i<n; i++)
    if (XOnlineAreUsersIdentical(_recentPlayers[i].xuid, identity.xuid)) return;

  if (n >= maxRecentPlayers)
  {
    _recentPlayers.Realloc(maxRecentPlayers);
    _recentPlayers.Resize(maxRecentPlayers - 1);
  }
  _recentPlayers.Insert(0);
  _recentPlayers[0].xuid = identity.xuid;
  _recentPlayers[0].name = identity.GetName();
}
#endif

void NetworkClient::HackedDataDetected(RString filename)
{
  HackedDataMessage msg;
  msg._type = HTHackedData;
  msg._filename = filename;
  SendMsg(&msg, NMFGuaranteed);
}

void NetworkClient::AddPublicVariableEventHandler(RString name, GameValuePar code)
{
  DoAssert(code.GetType() == GameCode);
  PublicVariableEventHandler item;
  item._name = name;
#if USE_PRECOMPILATION
  item._code = static_cast<GameDataCode *>(code.GetData());
#else
  item._code = code;
#endif
  _publicVariableEventHandlers.Add(item);
}

void NetworkClient::RememberSerializedObject(NetworkObject *obj)
{
  DoAssert(obj);
  _serializedObjects.Add(obj);
}

void ListPublicVariableEHFunc(const PublicVariableEventHandler &eh, const PublicVariableEventHandlers *dummy1, void *pubVarEH)
{
  AutoArray<PublicVariableEventHandler> &publicVarEH = *static_cast<AutoArray<PublicVariableEventHandler> *>(pubVarEH);
  publicVarEH.AddFast(eh);
}

/// save/load the MP game
LSError NetworkClient::SerializeMP(ParamArchive &ar)
{
  //TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
  CHECK(ar.Serialize("nextId", _nextId, 1))
  // publicVariableEventHandlers after Load
  //   a) on clients not on server are created by placing logic vehicle with initializing init script
  //   b) on server client it should be serialized here
  void *oldParams = ar.GetParams();
  ar.SetParams(&GGameState); // needed to create variables of registered types
  if (ar.IsSaving())
  {
    AutoArray<PublicVariableEventHandler> pubVarEH;
    int size = _publicVariableEventHandlers.NItems();
    pubVarEH.Realloc(size);
    _publicVariableEventHandlers.ForEach(ListPublicVariableEHFunc, &pubVarEH);
    CHECK(ar.Serialize("PublicVariableEventHandlers", pubVarEH, 1))
  }
  else if (ar.GetPass()==ParamArchive::PassFirst)
  {
    AutoArray<PublicVariableEventHandler> pubVarEH;
    CHECK(ar.Serialize("PublicVariableEventHandlers", pubVarEH, 1))
    // add collected EH into _publicVariableEventHandlers
    for (int i=0; i<pubVarEH.Size(); i++)
    {
      _publicVariableEventHandlers.Add(pubVarEH[i]);
    }
  }
  ar.SetParams(oldParams);
  return LSOK;
}

/// functor for create object traversal
class NetworkClientCreateTurret : public ITurretFunc
{
protected:
  NetworkClient *_client;

public:
  NetworkClientCreateTurret(NetworkClient *client) : _client(client) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._turret && !context._turret->GetNetworkId().IsNull())
    {
      Transport *veh = dyn_cast<Transport>(entity);
      if (veh)
      {
        TurretPath path;
        bool ok = veh->GetTurretPath(path, context._turret);
        DoAssert(ok);
        _client->CreateSubobject(veh, path, context._turret, false); //do not create NetworkId
      }
    }
    return false; // continue
  }
};

/// functor for update object traversal
class NetworkClientCreateObject
{
protected:
  NetworkClient *_client;
  VehicleListType _list;

public:
  NetworkClientCreateObject(NetworkClient *client, VehicleListType list) : _client(client), _list(list) {}
  bool operator () (Entity *veh) const
  {
    if (!veh->GetNetworkId().IsNull())
    {
      int idVeh = -1;
      for (int i=0; i<vehiclesMap.Size(); i++)
      {
        if (vehiclesMap[i] == veh)
        {
          idVeh = i;
          break;
        }
      }
      // CreateVehicle now (similar to NetworkClient::CreateVehicle
      // FIX: veh->GetVarName() is necessary, fixing news:h4s733$jgb$1@new-server.localdomain
      _client->CreateVehicleFromLoad(veh, _list, veh->GetVarName(), idVeh);

      // CreateSubobjects
      EntityAIFull *vehAI = dyn_cast<EntityAIFull>(veh);
      if (vehAI)
      {
        NetworkClientCreateTurret func(_client);
        vehAI->ForEachTurret(func);
      }
    }
    return false;
  }
};

void NetworkClient::CreateAllObjectsFromLoad()
{
  GWorld->ForEachVehicle(NetworkClientCreateObject(this, VLTVehicle));
  GWorld->ForEachAnimal(NetworkClientCreateObject(this, VLTAnimal));
  GWorld->ForEachSlowVehicle(NetworkClientCreateObject(this, VLTSlowVehicle));
  GWorld->ForEachCloudlet(NetworkClientCreateObject(this, VLTCloudlet));
  GWorld->ForEachFastVehicle(NetworkClientCreateObject(this, VLTFast));
  //FIX: For EachOutVehicle the VLTVehicle list should be used (see news:h4v0k5$qjk$1@new-server.localdomain)
  GWorld->ForEachOutVehicle(NetworkClientCreateObject(this, VLTVehicle));

  // AI structure
  AICenter *center = GWorld->GetEastCenter();
  if (center) CreateCenterFromLoad(center);
  center = GWorld->GetWestCenter();
  if (center) CreateCenterFromLoad(center);
  center = GWorld->GetGuerrilaCenter();
  if (center) CreateCenterFromLoad(center);
  center = GWorld->GetCivilianCenter();
  if (center) CreateCenterFromLoad(center);
  center = GWorld->GetLogicCenter();
  if (center) CreateCenterFromLoad(center);

#if _ENABLE_INDEPENDENT_AGENTS
  GWorld->CreateTeamMembersFromLoad();
#endif
}

void NetworkClient::CreateCenterFromLoad(AICenter *center)
{
  // because of ownership the order should be: center, groups, subgroups, units
  Assert(center);
  if (!center->GetNetworkId().IsNull()) CreateObject(center, false); //Center
  for (int g=0; g<center->NGroups(); g++)
  {
    AIGroup *grp = center->GetGroup(g);
    if (!grp) continue;
    if (!grp->GetNetworkId().IsNull()) CreateObject(grp, false); //Group
    for (int s=0; s<grp->NSubgroups(); s++)
    {
      AISubgroup *subgrp = grp->GetSubgroup(s);
      if (!subgrp) continue;
      if (!subgrp->GetNetworkId().IsNull()) CreateObject(subgrp, false); //Subgroup
      for (int u=0; u<subgrp->NUnits(); u++)
      {
        AIUnit *unit = subgrp->GetUnit(u);
        if (!unit) continue;
        if (!unit->GetNetworkId().IsNull()) CreateObject(unit, false); //Unit
      }
      // Create also Commands (stored inside Subgroup)
      subgrp->CreateCommandsFromLoad();
    }
  }
}

bool NetworkClient::CreateVehicleFromLoad(Vehicle *veh, VehicleListType list, RString name, int idVeh)
{
  if (!veh) return false;
  if (_clientState < NCSMissionReceived) return false;

  NetworkId netwId = veh->GetNetworkId();
  if (!netwId.IsNull() && netwId.creator!=CreatorId(STATIC_OBJECT))
  {
    NetworkId id = CreateLocalObject(veh, false); // use existing NetworkId

    // send message to center
    NetworkMessageType msgType = veh->GetNMType(NMCCreate);

    // create message
    Ref<NetworkMessage> msg = ::CreateNetworkMessage(msgType);
    msg->time = Glob.time;
    NetworkMessageContext ctx(msg, this, TO_SERVER, MSG_SEND);
    ctx.SetClass(NMCCreate);

    switch (msgType)
    {
    //@{ The following cannot happen
    //case NMTCreateAICenter:
    //case NMTCreateAIGroup:
    //case NMTCreateAISubgroup:
    //case NMTCreateAIBrain:
    //case NMTCreateCommand:
    //case NMTCreateAITeam:
    //case NMTAIStatsMPRowCreate:
    //@}
    case NMTCreateTurret:
      DoAssert(false); //Could not happen! Turrets should not be created by CreateVehicleFromLoad call
      return false;
      // break; //unreachable code
    case NMTCreateShot:
      // NetworkMessageCreateShot is known only inside shot.cpp file scope
      // but veh->TransferMsg(ctx) should work
      // DoAssert(dynamic_cast<NetworkMessageCreateShot *>(ctx.GetNetworkMessage()));
      break;
    //@{ The following are children of CreateVehicle message
    case NMTCreateDetector: // DECLARE_NET_INDICES_EX(CreateDetector, CreateVehicle, CREATE_DETECTOR_MSG)
    case NMTCreateCrater:   // DECLARE_NET_INDICES_EX(CreateCrater, CreateVehicle, CREATE_CRATER_MSG)
    case NMTCreateCraterOnVehicle: // DECLARE_NET_INDICES_EX(CreateCraterOnVehicle, CreateCrater, CREATE_CRATER_ON_VEHICLE_MSG)
    case NMTCreateObjectDestructed: // DECLARE_NET_INDICES_EX(CreateObjectDestructed, CreateVehicle, CREATE_OBJECT_DESTRUCTED_MSG)
    case NMTCreateDynSoundSource: // DECLARE_NET_INDICES_EX(CreateDynSoundSource, CreateVehicle, CREATE_DYN_SOUND_SOURCE_MSG)
    case NMTCreateHelicopter: // DECLARE_NET_INDICES_EX(CreateHelicopter, CreateVehicle, CREATE_HELICOPTER_MSG)
    case NMTCreateSeagull: // DECLARE_NET_INDICES_EX(CreateSeagull, CreateVehicle, CREATE_SEAGULL_MSG)
    case NMTCreateDestructionEffects: // DECLARE_NET_INDICES_EX(CreateDestructionEffects, CreateVehicle, CREATE_DESTRUCTION_EFFECTS_MSG)
    //@}
    case NMTCreateVehicle:
      { // difference from CreateObject - vehicle does known its list, name, id:
        DoAssert(dynamic_cast<NetworkMessageCreateVehicle *>(ctx.GetNetworkMessage()));
        PREPARE_TRANSFER(CreateVehicle)

        if (TRANSF_BASE(list, (int &)list) != TMOK) return false;
        if (TRANSF_BASE(name, name) != TMOK) return false;
        if (TRANSF_BASE(idVehicle, idVeh) != TMOK) return false;
      }
      break;
    case NMTCreateObject:
    case NMTCreateAIUnit:
    case NMTCreateAIAgent:
    default:
      DoAssert(false); // ??? possible or not ???
      return false;
      // break; //unreachable code
    }

    { // avoid uninitialized message member
      PREPARE_TRANSFER(NetworkObject)
      bool guaranteed = true;
      TRANSF_BASE(guaranteed, guaranteed);
    }

    if (veh->TransferMsg(ctx) != TMOK) return false;

    // send message
    DWORD dwMsgId = SendMsg
      (
      TO_SERVER, msg, msgType, NMFGuaranteed, veh
      );

#if _EXT_CTRL //ext. Controller like HLA, AAR
    if(IsBotClient())
      _hla.CreateVehicle(id, veh, name, idVeh);
#endif

    return dwMsgId != 0xFFFFFFFF;
  }
  return false;
}

void NetworkClient::SendGameLoadedFromSave()
{
  LoadedFromSaveMessage msg;
  SendMsg(&msg, NMFGuaranteed);
}

DWORD NetworkClient::SendMsg
(
  int to, NetworkMessage *msg,
  NetworkMessageType type, NetMsgFlags dwFlags,
  NetworkObject *object
)
{
#if _ENABLE_MP
  if (!_client) return -1;
//  NetworkMessageFormatBase *format = GetFormat(type);
//  if (!format) return 0xffffffff;

  NetworkServer *server = _parent->GetServer();
  if (server)
  {
    msg->size = 0;
#if _ENABLE_CHEATS
    StatMsgSent(type, CalculateMessageSize(msg, type), dwFlags);
#endif
    server->AddLocalMessage(BOT_CLIENT, type, msg);
    return 0;
  }
  else if (dwFlags & NMFGuaranteed)
  {
    msg->size = CalculateMessageSize(msg, type);
#if _ENABLE_CHEATS
StatMsgSent(type, msg->size, dwFlags);
#endif
    if (dwFlags&NMFHighPriority)
    {
      DWORD err = SendMsgRemote(to,msg,type,dwFlags|NMFStatsAlreadyDone, object ? &object->_fence : NULL, NULL);
      if (err==0xffffffff) return err;
      return 0;
    }
    else
    {
      EnqueueMsg(to, msg, type, object);
      return 0;
    }
  }
  else
  {
    msg->size = CalculateMessageSize(msg, type);
#if _ENABLE_CHEATS
StatMsgSent(type, msg->size, dwFlags);
#endif
    //invalidate fence when message is acknowledged
    RefR<NetMessage, RefCountSafe> fence;
    if (object && object->_fence)
    {
      if ( object->_fence->WasReceived() ) object->_fence = NULL;
      else fence = object->_fence;
    }
    EnqueueMsgNonGuaranteed(to, msg, type, fence);
    return 1;
  }
#else
    return E_FAIL;
#endif
}

