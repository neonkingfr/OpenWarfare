#ifdef _MSC_VER
#pragma once
#endif

#ifndef geo_loc_hpp
#define geo_loc_hpp

void GeoLocationFromIP(float &longitude, float &latitude, const char *ip);

#endif
