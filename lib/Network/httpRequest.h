#ifdef _MSC_VER
#pragma once
#endif

#ifndef http_request_hpp
#define http_request_hpp

#include "../jsonBinding.h"


int NOTHROW GetResponse(RString &response, const char *site, const char *query, const char *method);

rapidjson_binding::DocumentValue NOTHROW GetResponseJSON(const char *site, const char *query, rapidjson_binding::DocumentValue &data);

#endif
