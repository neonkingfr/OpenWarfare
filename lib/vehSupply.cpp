#include "wpch.hpp"
#include "vehSupply.hpp"
#include "UI/uiActions.hpp"
#include "paramArchiveExt.hpp"
#include "Network/network.hpp"
#include "AI/ai.hpp"
#include "move_actions.hpp"
#include "landscape.hpp"

// config identifiers
#define TitleTxt 0 // down fades text
#define TitleTxtDown 1 // down fades text
#define TitleRsc 2 // resource text
#define TitleObj 3

/*!
\patch 1.47 Date 3/7/2002 by Jirka
- Fixed: Taking weapon from cargo space could crash dedicated server.
*/

void CutScene( const char *name )
{
  if (!GWorld->CameraOn()) return;

  static const char *lastName=NULL;
  static Link<TitleEffect> lastEffect;
  static Link<AbstractWave> lastSound;

  TitleEffect *effect=GWorld->GetTitleEffect();
  if( effect && effect==lastEffect )
  {
    if( !name )
    {
      effect->Terminate();
      return;
    }
    else if( name==lastName )
    {
      effect->Prolong(1);
      // TODO: prolong sound effect?
    }
  }
  if( effect || !name ) return;

  if( GWorld->GetCameraEffect() ) return;
  
  ParamEntryVal entry=Pars>>"CfgCutScenes">>name;
  // set-up title
  int titName=entry>>"titleType";
  RString titText=entry>>"title";

  TitleEffect *titEff=NULL;
  switch( titName )
  {
    case TitleTxt: titEff=CreateTitleEffect(TitPlain,titText);break;
    case TitleTxtDown: titEff=CreateTitleEffect(TitPlainDown,titText);break;
    //case TitleRsc: titEff=CreateTitleEffectRsc(TitPlainDown,titText);break;
  }
  if( titEff )
  {
    GWorld->SetTitleEffect(titEff);
    lastEffect=titEff;
    lastName=name;
  }

  // set-up sound effect
  SoundPars pars;
  GetValue(pars, entry>>"sound");
  if( pars.name.GetLength()>0 )
  {
    AbstractWave *wave=GSoundScene->OpenAndPlayOnce
    (
      pars.name,NULL, false, 
      GWorld->CameraOn()->FutureVisualState().Position(),GWorld->CameraOn()->ObjectSpeed(),
      pars.vol,pars.freq, pars.distance
    );
    if( wave )
    {
      GSoundScene->AddSound(wave);
      lastSound=wave;
    }
  }
}



ResourceSupply::ResourceSupply( EntityAI *vehicle, bool fullCreate)
:_parent(vehicle)
{
  const VehicleType *type=vehicle->GetType();
  _fuelCargo = type->GetMaxFuelCargo();
  _repairCargo = type->GetMaxRepairCargo();
  _ammoCargo = type->GetMaxAmmoCargo();
  _wasRefuelingPlayer = false;

  _resourceLock.Clear(); // not locked

  for (int i=0; i<type->_weaponCargo.Size(); i++)
  {
    const WeaponCargoItem &item = type->_weaponCargo[i];
    AddWeaponCargo(vehicle,item.weapon, item.count, true);
  }
  for (int i=0; i<type->_magazineCargo.Size(); i++)
  {
    const MagazineCargoItem &item = type->_magazineCargo[i];
    AddMagazineCargoCount(vehicle,item.magazine, item.count, true);
  }
}

// used for serialization only 
ResourceSupply::ResourceSupply():
_parent(NULL)
{
  _fuelCargo = 0;
  _repairCargo = 0;
  _ammoCargo = 0;
  _wasRefuelingPlayer = false;
}

ResourceSupply::~ResourceSupply()
{
  DoAssert(_weaponCargo.Size() == 0);
  DoAssert(_magazineCargo.Size() == 0);
  DoAssert(_backpacks.Size() == 0);
}

void ResourceSupply::Destroy(EntityAI *parent)
{
  // clear weapons and magazines shapes 
  if (parent->GetType()->_showWeaponCargo)
  {
    for (int i=0; i<_weaponCargo.Size(); i++)
      if (_weaponCargo[i]) _weaponCargo[i]->ShapeRelease();

    for (int i=0; i<_magazineCargo.Size(); i++)
      if (_magazineCargo[i]) _magazineCargo[i]->_type->MagazineShapeRelease();
  }
  _weaponCargo.Clear();
  _magazineCargo.Clear();


  for (int i=0; i< _backpacks.Size(); i++)
  {
    if(GWorld->IsOutVehicle(_backpacks[i]))
    {    
      GWorld->RemoveOutVehicle(_backpacks[i]);
      _backpacks[i]->SetDelete();
    }
  }
  _backpacks.Clear();
}

void ResourceSupply::InitCargoBackpacks()
{
  const VehicleType *type=_parent->GetType();
  if(!type) return;


  if(free>0) for (int i=0; i<type->_backpackCargo.Size(); i++)
  {
    const BackpackCargoItem &item = type->_backpackCargo[i];
    for (int j=0; j< item.count; j++)
    {

      Ref<EntityAI> backpack = GWorld->NewVehicleWithID(item.backpack);
      if(!backpack) continue;

      //Add into World
      GWorld->AddOutVehicle(backpack);
      backpack->SetMoveOutDone(_parent);

      if (GWorld->GetMode() == GModeNetware)
        GetNetworkManager().CreateVehicle(backpack, VLTOut, "", -1);

      // TODO: make it working correctly in MP
      _parent->AddBackpackCargo(backpack, false);

    }
  }
}

bool CheckSupply(EntityAI *vehicle, EntityAI *parent, SupportCheckF check, float limit, bool now)
{
  if (parent == vehicle) return false;

  if (now)
  {
    // check distance
    Vector3 supplyPos = parent->FutureVisualState().PositionModelToWorld
    (
      parent->GetType()->GetSupplyPoint()
    );
    float supplyRadius = parent->GetType()->GetSupplyRadius();

    float dist2 = vehicle->FutureVisualState().Position().Distance2(supplyPos);
    if (dist2 > Square(40)) return false;
    float maxDistance = vehicle->CollisionSize() * 1.2 + 1 + supplyRadius;
    if (dist2 > Square(maxDistance)) return false;
  //  if (vehicle->Speed().Distance2(parent->Speed()) > Square(0.5)) return false;
  }

  if (check)
    return (vehicle->*check)() > limit;
  else
    return true;
}

bool ResourceSupply::Check(EntityAI *parent, EntityAI *vehicle, SupportCheckF check, float limit, bool now) const
{
  if (!vehicle) return false;
  if (!vehicle->GetShape()) return false;

  return CheckSupply(vehicle, parent, check, limit, now);
}

#define ACTIONS_ENUM_NAME(name, type) EnumName(AT##name, #name),

static const EnumName UIActionNames[]=
{
  ACTIONS_DEFAULT(ACTIONS_ENUM_NAME)
#if _ENABLE_CONVERSATION
  ACTIONS_CONVERSATION(ACTIONS_ENUM_NAME)
#endif 
#if _VBS3 && _ENABLE_CARRY_BODY //BattlefieldClearance
  ACTIONS_CARRY_BODY(ACTIONS_ENUM_NAME)
#endif
#if _VBS2
  ACTIONS_VBS2(ACTIONS_ENUM_NAME)
#endif   
  EnumName()
};
template<>
const EnumName *GetEnumNames(UIActionType dummy)
{
  return UIActionNames;
}

/*!
\patch 5130 Date 2/21/2007 by Ondra
- Fixed: After save/load weapons might become invisible.
*/
LSError ResourceSupply::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("fuelCargo", _fuelCargo, 1, 0))
  CHECK(ar.Serialize("repairCargo", _repairCargo, 1, 0))
  CHECK(ar.Serialize("ammoCargo", _ammoCargo, 1, 0))
  CHECK(ar.Serialize("wasSupplyingPlayer", _wasRefuelingPlayer, 1, false))
  
  // no need to serialize parent - sets in EntityAI::Serialize
  if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
  {
    CHECK(ar.SerializeRef("Supplying", _supplying, 1))
    CHECK(ar.SerializeRef("Alloc", _alloc, 1))
    CHECK(ar.Serialize("action", _action, 1))
  }

  CHECK(ar.Serialize("WeaponCargo", _weaponCargo, 1));
  CHECK(ar.Serialize("MagazineCargo", _magazineCargo, 1));
  CHECK(ar.SerializeRefs("backpacks", _backpacks, 1));

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond)
  {
    EntityAI *parent = reinterpret_cast<EntityAI *>(ar.GetParams());
    // if the container is showing its content, it needs to AddRef the content properly
    if (parent->GetType()->_showWeaponCargo)
    {
      for (int i=0; i<_weaponCargo.Size(); i++)
      {

        WeaponType *weapon = _weaponCargo[i];
        if (weapon) weapon->ShapeAddRef();
      }
      for (int i=0; i<_magazineCargo.Size(); i++)
      {

        Magazine *mag = _magazineCargo[i];
        if (mag) mag->_type->MagazineShapeAddRef();
      }
    }
  }

//  CHECK(ar.Serialize("lastSupportTime", _lastSupportTime, 1))

  return LSOK;
}

DEFINE_NETWORK_OBJECT_SIMPLE(ResourceSupply, UpdateSupply)

#define UPDATE_SUPPLY_MSG(MessageName, XX) \
  XX(MessageName, float, fuelCargo, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Transported fuel"), TRANSF, ET_ABS_DIF, 0.01 * ERR_COEF_VALUE_MINOR) \
  XX(MessageName, float, repairCargo, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Transported repair material"), TRANSF, ET_ABS_DIF, 0.01 * ERR_COEF_VALUE_MINOR) \
  XX(MessageName, float, ammoCargo, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Transported ammunition (for vehicles)"), TRANSF, ET_ABS_DIF, 0.01 * ERR_COEF_VALUE_MINOR) \
  XX(MessageName, OLinkO(EntityAIFull), alloc, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Unit allocated for supplying"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_MODE) \

//  XX(MessageName, OLinkO(EntityAIFull), supplying, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Currently supplying unit"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_MODE) \
// TODO:
/*
  XX(MessageName, int, action, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, ATNone), DOC_MSG("Currently processing action"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, actionParam, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Action parameter"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, actionParam2, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Action parameter"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, RString, actionParam3, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Action parameter"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE)
*/

DECLARE_NET_INDICES_ERR(UpdateSupply, UPDATE_SUPPLY_MSG)
DEFINE_NET_INDICES_ERR(UpdateSupply, UPDATE_SUPPLY_MSG, NoErrorInitialFunc)

NetworkMessageFormat &ResourceSupply::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  UPDATE_SUPPLY_MSG(UpdateSupply, MSG_FORMAT_ERR)
  return format;
}

TMError ResourceSupply::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(UpdateSupply)

  TRANSF(fuelCargo)
  TRANSF(repairCargo)
  TRANSF(ammoCargo)
//  ITRANSF(infantryAmmoCargo)
 // TRANSF_REF(supplying)
  TRANSF_REF(alloc)
//  ITRANSF(lastSupportTime)

// TODO:
/*
  TRANSF_ENUM(action)
  TRANSF(actionParam)
  TRANSF(actionParam2)
  TRANSF(actionParam3)
*/
  return TMOK;
}

float ResourceSupply::CalculateError(NetworkMessageContextWithError &ctx)
{
  PREPARE_TRANSFER(UpdateSupply)

  float error = 0;
  ICALCERR_NEQREF_SOFT(EntityAI, alloc, ERR_COEF_MODE)
 // ICALCERR_NEQREF_PERM(EntityAI, supplying, ERR_COEF_MODE)
// TODO:
/*
  ICALCERR_NEQ(int, action, ERR_COEF_MODE)
  ICALCERR_NEQ(int, actionParam, ERR_COEF_MODE)
  ICALCERR_NEQ(int, actionParam2, ERR_COEF_MODE)
  ICALCERR_NEQSTR(actionParam3, ERR_COEF_MODE)
*/
  ICALCERR_ABSDIF(float, fuelCargo, 0.01 * ERR_COEF_VALUE_MINOR)
  ICALCERR_ABSDIF(float, repairCargo, 0.01 * ERR_COEF_VALUE_MINOR)
  ICALCERR_ABSDIF(float, ammoCargo, 0.01 * ERR_COEF_VALUE_MINOR)
  //ICALCERR_ABSDIF(float, infantryAmmoCargo, 0.01 * ERR_COEF_VALUE_MINOR)
  return error;
}

bool ResourceSupply::Supply(EntityAIFull *vehicle, const Action *action)
{
  if (vehicle)
  {
    if (!vehicle->IsLocal())
    {
      RptF("Remote entity want to supply: %s", cc_cast(vehicle->GetDebugName()));
    }
    AIBrain *brain = vehicle->CommanderUnit();
    if (_alloc && _alloc != vehicle && brain && !brain->IsPlayer()) return false;
    // do not allocate - you would unallocate immediatelly
    /*
    {
      _alloc = vehicle;
      LogF
      (
        "%s: Late alloc to %s",
        (const char *)_parent->GetDebugName(),
        (const char *)vehicle->GetDebugName()
      );
    }
    else if (_alloc != vehicle) return false;
    */
  }
  _supplying = vehicle;
  _action = unconst_cast(action);
  return true;
}

void ResourceSupply::CancelSupply()
{
  if (_alloc && _alloc == _supplying) _alloc = NULL;
  _supplying = NULL;
  _action = NULL;
}

struct MagazineInWeapon
{
  Ref<MuzzleType> muzzle;
  Ref<Magazine> magazine;
};
TypeIsMovableZeroed(MagazineInWeapon)

void ResourceSupply::PutMagazine(EntityAI *parent)
{
  EntityAIFull *target = _supplying;
  Person *man = dyn_cast<Person>(target);
  const ActionMagazineType *actionTyped = static_cast<const ActionMagazineType *>(_action.GetRef());
  if (man)
  {

    Ref<MagazineType> type = MagazineTypes.New(actionTyped->GetMagazineType());
    if (!type) return;

    if(!actionTyped->UseBackpack())
    {
      Ref<const Magazine> magazine;
      int minCount = INT_MAX;
      // find in nonused magazines
      for (int i=0; i<man->NMagazines(); i++)
      {
        const Magazine *m = man->GetMagazine(i);
        if (!m) continue;
        if (m->_type != type) continue;
        if (man->IsMagazineUsed(m)) continue;
        if (m->GetAmmo() < minCount)
        {
          magazine = m;
          minCount = m->GetAmmo(); 
        }
      }
      // find in all magazines
      if (!magazine) for (int i=0; i<man->NMagazines(); i++)
      {
        const Magazine *m = man->GetMagazine(i);
        if (!m) continue;
        if (m->_type != type) continue;
        if (m->GetAmmo() < minCount)
        {
          magazine = m;
          minCount = m->GetAmmo(); 
        }
      }

      if (magazine)
      {
        man->RemoveMagazine(magazine);
        if (minCount > 0) AddMagazineCargo(parent, const_cast<Magazine *>(magazine.GetRef()), false);
      }
    }
    else
    {
      if(man->GetBackpack() && man->GetBackpack()->GetSupply())
      {
        ResourceSupply *supply = man->GetBackpack()->GetSupply();

        Ref<const Magazine> mag = supply->FindMagazine(type);
        if(mag)
        {
          Magazine *magazine = const_cast<Magazine *>(mag.GetRef());
          // remove weapon
          supply->RemoveMagazineCargo(man->GetBackpack(),magazine);
          GetNetworkManager().RemoveMagazineCargo(man->GetBackpack(), magazine->_type->GetName(), magazine->GetAmmo());

          if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == man)
            GWorld->UI()->ResetVehicle(man);

          AddMagazineCargo(parent, magazine, false);
        }
      }
    }

    void UpdateWeaponsInBriefing();
    UpdateWeaponsInBriefing();
  }
}

void ResourceSupply::PutWeapon(EntityAI *parent)
{
  EntityAIFull *target = _supplying;
  Person *man = dyn_cast<Person>(target);
  const ActionWeapon *actionTyped = static_cast<const ActionWeapon *>(_action.GetRef());

  if (man)
  {
    // find weapon    
    Ref<WeaponType> weapon = WeaponTypes.New(actionTyped->GetWeapon());
    if (!weapon || !weapon->_canDrop) return;

    if(!actionTyped->UseBackpack())
    {
      if (man->FindWeapon(weapon))
      {
        // remove weapon
        man->RemoveWeapon(weapon);
        if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == man)
          GWorld->UI()->ResetVehicle(man);

        AddWeaponCargo(parent, weapon, 1, false);

        // remove unusable magazines
        for (int i=0; i<man->NMagazines();)
        {
          Ref<Magazine> magazine = man->GetMagazine(i);
          if (!magazine || !weapon->IsMagazineUsableInWeapon(magazine->_type))
          {
            i++;
            continue;
          }
          man->RemoveMagazine(magazine);
          if (magazine->GetAmmo() > 0) AddMagazineCargo(parent, magazine, false);
        }
      }
    }
    else
    {
      if(man->GetBackpack() && man->GetBackpack()->GetSupply())
      {
        ResourceSupply *supply = man->GetBackpack()->GetSupply();
        
        if ( supply->FindWeapon(weapon))
        {
          // remove weapon
          supply->RemoveWeaponCargo(man->GetBackpack(),weapon);
          GetNetworkManager().RemoveWeaponCargo(man->GetBackpack(), weapon->GetName());
          if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == man)
            GWorld->UI()->ResetVehicle(man);

          AddWeaponCargo(parent, weapon, 1, false);
        }
      }
    }
    void UpdateWeaponsInBriefing();
    UpdateWeaponsInBriefing();
  }
}

void ResourceSupply::PutBackpack(EntityAI *parent)
{
  EntityAIFull *target = _supplying;
  Person *man = dyn_cast<Person>(target);
  if (man && man->GetBackpack())
  {
    // find weapon    
    const ActionWeapon *actionTyped = static_cast<const ActionWeapon *>(_action.GetRef());
    RString name = actionTyped->GetWeapon();
    if (strcmpi(name, man->GetBackpack()->Type()->GetName())!=0) return;

    // remove weapon
    Ref<EntityAI> backpack = man->GetBackpack();
    man->RemoveBackpack();
    /*man->RemoveWeapon(weapon);*/
    if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == man)
      GWorld->UI()->ResetVehicle(man);

    AddBackpackCargo(parent, backpack, false, man);

    void UpdateWeaponsInBriefing();
    UpdateWeaponsInBriefing();
  }
}

/*!
\patch 1.82 Date 8/28/2002 by Jirka
- Fixed: Unit in cargo space of vehicle can take / drop items even when vehicle moves
\patch 5163 Date 6/4/2007 by Jirka
- Fixed: In some situations, Rearm / Repair / Refuel did not fill all
\patch 5255 Date 5/13/2008 by Bebul
- Fixed: Player could not use grenades added by Rearm
*/

void ResourceSupply::Simulate( EntityAI *parent, float deltaT, SimulationImportance prec )
{
  DoAssert(parent);

  if (_alloc)
  {
    // we are allocated to some unit
    float limit2 = _alloc->QIsManual() ? Square(30) : Square(100);
    if
    (
      _alloc->FutureVisualState().Position().Distance2(parent->FutureVisualState().Position()) > limit2
      // TODO: check if units needs us
    )
    {
      // allocated vehicle is very far - unallocate it
      if (_supplying == _alloc)
      {
        _supplying = NULL;
        _action = NULL;
      }
      _alloc = NULL;
    }
  }
  if (_supplying)
  {
    if (_supplying->FutureVisualState().Speed().Distance2(parent->FutureVisualState().Speed())>Square(2))
    {
      // FIX: Check if unit is not in cargo
      AIBrain *unit = _supplying->CommanderUnit();
      if (unit && unit->GetPerson() == _supplying && unit->GetVehicleIn() == parent)
      {
        // unit in cargo space of vehicle can take / drop items even when vehicle moves
      }
      else
      {
        _supplying = NULL;
        _action = NULL;
      }
    }
  }
  //Assert(_alloc || !_supplying);
  
  const float thold = 0;
  const float fuelThold = 0;

  // check last frame state and reset it
  bool wasRefuelingPlayer = _wasRefuelingPlayer;
  _wasRefuelingPlayer = false;
  
  // support unit - perform support if necessary
  //if (_supplying && _supplying->IsLocal())
  if (_supplying)
  {
    EntityAIFull *target = _supplying;
    if (!_action)
    {
      RptF("Supply action on %s - missing action", cc_cast(_supplying->GetDebugName()));
      _supplying = NULL;
      return;
    }
    switch (_action->GetType())
    {
    case ATHeal:
      if (parent->GetType()->IsAttendant() && target->CommanderUnit() && target->NeedsAmbulance() > 0)
      {
        // TODO: scan also driver, gunner and cargo soldiers
        EntityAI *person = target->CommanderUnit()->GetPerson();
        person->Repair(1);
        _supplying = NULL; // done
        _action = NULL;
        _alloc = NULL;
        return;
      }
      break;
    case ATHealSoldier:
      if (parent->GetType()->IsAttendant() && target->CommanderUnit() && target->NeedsAmbulance() > 0)
      {
        // TODO: scan also driver, gunner and cargo soldiers
        EntityAI *person = target->CommanderUnit()->GetPerson();
        AIBrain *aibrain = person->CommanderUnit();
        person->SetDamageNetAware(0); 
        if(aibrain)
          aibrain->SetLifeState(LifeStateAlive);
        _supplying = NULL; // done
        _action = NULL;
        _alloc = NULL;

        AIGroup *group = person->GetGroup();
        if(aibrain && group)
        {
          AIUnit *aiunit = aibrain->GetUnit();
          if(group && aiunit) group->SetHealthStateReported(aiunit,AIUnit::RSNormal);
        }

        return;
      }
      break;
    case ATRepairVehicle:
      if (parent->GetType()->IsEngineer() && target->GetMaxHitCont() > 0.6f)
      {
        // TODO: scan also driver, gunner and cargo soldiers
        target->SetMaxHitZoneDamageNetAware(0.6f); 

        _supplying = NULL; // done
        _action = NULL;
        _alloc = NULL;

        return;
      }
      break;
    case ATRepair:
      if (GetRepairCargo() > 0 && target->NeedsRepair() > 0)
      {
        float dammage = target->GetTotalDamage();
        float total = target->GetType()->GetCost();
        float howMuch = total * dammage;
        if (howMuch <= total * thold)
        {
          _supplying = NULL; // done
          _action = NULL;
          break;
        }
        saturateMin(howMuch, _repairCargo);
        const float maxFlow = total * 0.1f;
        saturateMin(howMuch, maxFlow * deltaT);     
        target->Repair(howMuch / total);
        _repairCargo -= howMuch;
        if (target == GWorld->CameraOn()) CutScene("Repair");
        return;
      }
      break;
    case ATRefuel:
      if (GetFuelCargo() > 0 && target->NeedsRefuel() > 0)
      {
        float total = target->GetType()->GetFuelCapacity();
        float howMuch = total - target->GetFuel();
        if (howMuch <= total * fuelThold)
        {
          _supplying = NULL; // done
          _action = NULL;
          break;
        }
        saturateMin(howMuch, _fuelCargo);
        const float maxFlow = total * 0.1;
        saturateMin(howMuch, maxFlow * deltaT);
        target->Refuel(howMuch);
        _fuelCargo -= howMuch;
        if (target == GWorld->CameraOn()) CutScene("Refuel");
        return;
      }
      break;
    case ATRearm:
      {
        Person *man = dyn_cast<Person>(target);
        if (man)
        {
          // men rearm
          AIBrain *unit = man->Brain();
          if (!unit) return;
          // which magazines we needed
          CheckAmmoInfo info;
          unit->CheckAmmo(info);
          for (int i=0; i<GetMagazineCargoSize();)
          {
            Ref<const Magazine> magazine = GetMagazineCargo(i);
            if (!magazine || magazine->GetAmmo() == 0)
            {
              i++;
              continue;
            }
            // check if magazine can be used
            const MagazineType *type = magazine->_type;
            int slots = GetItemSlotsCount(type->_magazineType);
            int hgslots = GetHandGunItemSlotsCount(type->_magazineType);

            bool add = false;
            if (slots > 0)
            {
              if (info.muzzle1 && info.muzzle1->CanUse(type))
              {
                if (slots <= info.slots1)
                {
                  info.slots1 -= slots;
                  add = true;
                }
              }
              else if (info.muzzle2 && info.muzzle2->CanUse(type))
              {
                if (slots <= info.slots2)
                {
                  info.slots2 -= slots;
                  add = true;
                }
              }
              else if (man->IsMagazineUsable(type))
              {
                if (slots <= info.slots3)
                {
                  info.slots3 -= slots;
                  add = true;
                }
              }
            }
            else if (hgslots > 0)
            {
              if (info.muzzleHG && info.muzzleHG->CanUse(type))
              {
                if (hgslots <= info.slotsHG)
                {
                  info.slotsHG -= hgslots;
                  add = true;
                }
              }
            }
            if (add)
            {
              // add magazine
              AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
              if (man->CheckMagazine(magazine, conflict))
              {
                for (int j=0; j<conflict.Size(); j++)
                {
                  const Magazine *m = conflict[j];
                  man->RemoveMagazine(m);
                  AddMagazineCargo(parent,const_cast<Magazine *>(m), false);
                }
                RemoveMagazineCargo(parent,const_cast<Magazine *>(magazine.GetRef()));
                GetNetworkManager().RemoveMagazineCargo(parent, magazine->_type->GetName(), magazine->GetAmmo());
                DoVerify(man->AddMagazine(const_cast<Magazine *>(magazine.GetRef())) >= 0);
              } else i++;
            }
            else i++;
          }
          _supplying = NULL;  // done
          _action = NULL;

          man->AutoReloadAll(false);

          void UpdateWeaponsInBriefing();
          UpdateWeaponsInBriefing();

          return;
        }
        else
        {
          // vehicle rearm
          if (GetAmmoCargo() > 0 && target->NeedsRearm() > 0)
          {
            float total = target->GetMaxAmmoCost();
            float howMuch = total - target->GetAmmoCost();
            if (howMuch <= total * thold)
            {
              _supplying = NULL; // done
              _action = NULL;
              break;
            }
            saturateMin(howMuch, _ammoCargo);
            const float maxFlow1 = total * 0.05;
            const float maxFlow2 = 3000;
            saturateMin(howMuch, maxFlow1 * deltaT);
            saturateMin(howMuch, maxFlow2 * deltaT);
            target->Rearm(howMuch);
            _ammoCargo -= howMuch;
            if (target == GWorld->CameraOn()) CutScene("Rearm");
          }
          return;
        }
      }
      break;
    case ATTakeWeapon:
    case ATTakeDropWeapon:
      {
        if (target->IsLocal())
        {
          // Check if action can be processed
          const ActionWeapon *actionTyped = static_cast<const ActionWeapon *>(_action.GetRef());
          Ref<WeaponType> weapon = WeaponTypes.New(actionTyped->GetWeapon());
          if (weapon && FindWeapon(weapon))
          {
            target->TakeWeapon(parent, weapon, actionTyped->UseBackpack());
            void UpdateWeaponsInBriefing();
            UpdateWeaponsInBriefing();
          }
          _supplying = NULL;
          _action = NULL;
        }
        return;
      }
      break;
    case ATTakeMagazine:
    case ATTakeDropMagazine:
      {
        if (target->IsLocal())
        {
          // Check if action can be processed
          const ActionMagazineType *actionTyped = static_cast<const ActionMagazineType *>(_action.GetRef());
          Ref<const Magazine> magazine = FindMagazine(actionTyped->GetMagazineType());
          if (magazine)
          {
            target->TakeMagazine(parent, magazine->_type, actionTyped->UseBackpack());   
            void UpdateWeaponsInBriefing();
            UpdateWeaponsInBriefing();
          }
          _supplying = NULL;
          _action = NULL;
        }
        return;
      }
      break;
    case ATDropWeapon:
    case ATPutWeapon:
      {
        PutWeapon(parent);
        _supplying = NULL;  // done
        _action = NULL;
      }
      return;
    case ATDropMagazine:
    case ATPutMagazine:
      {
        PutMagazine(parent);
        _supplying = NULL;  // done
        _action = NULL;
      }
      return;
    case ATDropBag:
      {
        PutBackpack(parent);
        _supplying = NULL;  // done
        _action = NULL;
      }
      return;
    case ATTakeBag:
    case ATAddBag:
      {
        {
          if (target->IsLocal())
          {
            // Check if action can be processed
            const ActionWeapon *actionTyped = static_cast<const ActionWeapon *>(_action.GetRef());
            RString name = actionTyped->GetWeapon();
            if (name.GetLength()>0)
            {
              target->TakeBackpack(parent, name);
              void UpdateWeaponsInBriefing();
              UpdateWeaponsInBriefing();
            }
            _supplying = NULL;
            _action = NULL;
          }
          return;
        }
        break;
      }
    return;
    default:
      Fail("Unsupported action");
      break;
    }
    _supplying = NULL;
    _action = NULL;
  }
  else
  {
    // handle player vehicle automatically
    AIBrain *unit = GWorld->FocusOn();
    if (unit)
    {
      Transport *vehicle = unit->GetVehicleIn();
      if (vehicle && vehicle->FutureVisualState().Speed().SquareSize() < Square(0.1f) && Check(parent, vehicle, NULL, 0, true))
      {
        // repair
        if (GetRepairCargo() > 0 && vehicle->NeedsRepair() > 0)
        {
          float dammage = vehicle->GetTotalDamage();
          float total = vehicle->GetType()->GetCost();
          float howMuch = total * dammage;
          saturateMin(howMuch, _repairCargo);
          if (howMuch > 0)
          {
            const float maxFlow = total * 0.1;
            saturateMin(howMuch, maxFlow * deltaT);
            vehicle->Repair(howMuch / total);
            _repairCargo -= howMuch;
            CutScene("Repair");
            return;
          }
        }
        // refuel
        if (
          GetFuelCargo() > 0 &&
          // when engine is running, we refuel only when there is a substantial amount missing
          // or if we have already started before
          vehicle->NeedsRefuel() > 0 && (!vehicle->EngineIsOn() || vehicle->NeedsRefuel()>0.02f || wasRefuelingPlayer)
        )
        {
          float total = vehicle->GetType()->GetFuelCapacity();
          float howMuch = total - vehicle->GetFuel();
          saturateMin(howMuch, _fuelCargo);
          if (howMuch > 0)
          {
            const float maxFlow = total * 0.1;
            saturateMin(howMuch, maxFlow * deltaT);
            vehicle->Refuel(howMuch);
            _fuelCargo -= howMuch;
            _wasRefuelingPlayer = true;
            CutScene("Refuel");
            return;
          }
        }
        // rearm
        if (GetAmmoCargo() > 0 && vehicle->NeedsRearm() > 0)
        {
          float total = vehicle->GetMaxAmmoCost();
          float howMuch = total - vehicle->GetAmmoCost();
          saturateMin(howMuch, _ammoCargo);
          if (howMuch > 0)
          {
            const float maxFlow1 = total * 0.05;
            const float maxFlow2 = 3000;
            saturateMin(howMuch, maxFlow1 * deltaT);
            saturateMin(howMuch, maxFlow2 * deltaT);
            vehicle->Rearm(howMuch);
            _ammoCargo -= howMuch;
            CutScene("Rearm");
            return;
          }
        }
      }
    }
  }
  CutScene(NULL);

  //check if any MP player have locked this ammocrate 
  //and if that player is still playing
  Vector3 supplyPosition =  parent ? parent->FutureVisualState().Position() : VZero;
  for(int i = _resourceLock.Size() -1 ; i>= 0; i--)
  {
    //remove Lock if unit was deleted or moved away from supply (MP connection interrupted ??)
    if(!_resourceLock[i] || !_resourceLock[i]->GetVehicle() 
      || _resourceLock[i]->GetVehicle()->RenderVisualState().Position().Distance2(supplyPosition) > 100) _resourceLock.DeleteAt(i);
  }
  if(_resourceLock.Size() == 0) DeleteIfNotNeeded(parent);

}

#if _VBS3
float ResourceSupply::GetMagazineWeightKg()
{
  float weightKg = 0.0f;
  for( int i = 0; i < _magazineCargo.Size(); ++i )
  {
    const Magazine *m = _magazineCargo[i];
    if (!m) continue;
    const MagazineType *mType = m->_type;
    if (!mType || mType->_maxAmmo == 0) continue;      
    float ammoRatio = (float)m->GetAmmo() / (float)mType->_maxAmmo;

    if(mType->_mass == 0.5)

      weightKg += ammoRatio * mType->_mass;
  }
  return weightKg;
}

float ResourceSupply::GetWeaponWeightKg()
{
  float weightKg = 0.0f;
  for( int i = 0; i < _weaponCargo.Size(); ++i)
  {
    const WeaponType *w = _weaponCargo[i];
    if (!w) continue;
    weightKg += w->GetMass();
  }
  return weightKg;
}
#endif

/*!
\patch 1.21 Date 8/24/2001 by Jirka
- Fixed: Allow Repair, Refuel, Rearm, Heal, TakeMagazine and TakeWeapon actions in MP only to vehicle owner (driver)
\patch 1.75 Date 2/5/2002 by Jirka
- Added: Player can drop actual weapon and magazine
\patch 1.75 Date 2/13/2002 by Jirka
- Added: Player can take weapons and magazines from vehicle if sitting in cargo 
\patch 5097 Date 12/7/2006 by Jirka
- Fixed: Stealing gear from medics was possible
*/

void ResourceSupply::GetActions(EntityAI *parent, UIActions &actions, AIBrain *unit, bool now, bool strict)
{
  if (!unit) return;
  
  EntityAIFull *veh = unit->GetVehicle();
  if (now && !veh->IsLocal()) return;

/*
if (unit == GWorld->FocusOn() && veh->Position().Distance2(parent->Position()) < Square(3))
{
  bool check = Check(parent, veh, NULL, 0, now); // check distance
  GlobalShowMessage
  (
    500,
    "%d weapons, %d magazines, allocated to %s, check %s",
    _weaponCargo.Size(), _magazineCargo.Size(),
    _alloc ? (const char *)_alloc->GetDebugName() : "nobody",
    check ? "true" : "false"
  );
}
*/
  if (_alloc && _alloc != veh && !unit->IsPlayer()) return;  // wait


  if(!parent->GetSupply()) return;

/*
  if (_supplying)
  {
    DoAssert(_supplying == veh);
    actions.Add(ATCancelAction, parent, 20, 0, true, true);
    return;
  }
*/

#if _ENABLE_DATADISC
  bool inCargo = unit->GetVehicleIn() == parent && unit->IsInCargo();
#else
  bool inCargo = false;
#endif
  bool check = Check(parent, veh, NULL, 0, now); // check distance

  bool soldier = unit->IsFreeSoldier();
  bool iAmSoldier = dyn_cast<Person>(parent) != NULL;

  AIBrain *parentBrain =  parent->CommanderUnit();
  bool isParentSoldier = false;
  if(parentBrain) isParentSoldier = parentBrain->IsFreeSoldier();
  
  const EntityAIType *type = parent->GetType();

  if (!iAmSoldier && check)
  {
    if (soldier)
    {
      if (_magazineCargo.Size() > 0)
      {
        if (Check(parent, veh, &EntityAI::NeedsInfantryRearm, 0, now) && parent->GetDisplayName().GetLength() > 0)
        {
          UIAction action(new ActionBasic(ATRearm, parent));
          action.position = parent->RenderVisualState().PositionModelToWorld(type->GetSupplyPoint());
          actions.Add(action);
        }
      }
    }
    else
    {
      if (GetAmmoCargo() > 0)
      {
        if (Check(parent, veh, &EntityAI::NeedsRearm, 0, now))
        {
          UIAction action(new ActionBasic(ATRearm, parent));
          action.position = parent->RenderVisualState().PositionModelToWorld(type->GetSupplyPoint());
          actions.Add(action);
        }
      }
    }
    if (GetRepairCargo() > 0)
    {
      if (Check(parent, veh, &EntityAI::NeedsRepair, 0, now))
      {
        UIAction action(new ActionBasic(ATRepair, parent));
        action.position = parent->RenderVisualState().PositionModelToWorld(type->GetSupplyPoint());
        actions.Add(action);
      }
    }
    if (GetFuelCargo() > 0)
    {
      if (Check(parent, veh, &EntityAI::NeedsRefuel, 0, now))
      {
        UIAction action(new ActionBasic(ATRefuel, parent));
        action.position = parent->RenderVisualState().PositionModelToWorld(type->GetSupplyPoint());
        actions.Add(action);
      }
    }
  }
  if (check)
  {
    if (type->IsAttendant() && soldier && !isParentSoldier)
    {
      if (veh->NeedsAmbulance() > 0)
      {
        UIAction action(new ActionBasic(ATHeal, parent));
        action.position = parent->RenderVisualState().PositionModelToWorld(type->GetSupplyPoint());
        actions.Add(action);
      }
    }
  }

  if (!check && !inCargo) return;
  if (iAmSoldier) return;


  if (type->CanStoreWeapons() && (inCargo || (soldier && !unit->GetPerson()->IsActionInProgress(MFReload))))
  {
    bool EnableMarkEntities();
    bool CampaignIsEntityMarked(EntityAI *entity);
    if (
      EnableMarkEntities() && !CampaignIsEntityMarked(parent) &&
      (_weaponCargo.Size() > 0 || _magazineCargo.Size() > 0))
    {
      UIActionType actionType = parent->GetShortName().GetLength() == 0 ? ATMarkWeapon : ATMarkEntity;
      UIAction action(new ActionBasic(actionType, parent));
      action.position = parent->RenderVisualState().PositionModelToWorld(type->GetSupplyPoint());
      actions.Add(action);
    }

    if(!type->IsBackpack() )
    {
      UIAction action(new ActionBasic(ATGear, parent));
      action.position = parent->RenderVisualState().PositionModelToWorld(type->GetSupplyPoint());
      if (inCargo) action.showWindow = false;
      actions.Add(action);
    }
    else if(soldier)
    {
      UIAction action(new ActionBasic(ATOpenBag, parent));
      action.position = parent->RenderVisualState().PositionModelToWorld(type->GetSupplyPoint());
      if (inCargo) action.showWindow = false;
      actions.Add(action);
    }

    // CHANGED: Show Take action only when single one is available
    bool canCollect = true;
    Ref<Action> takeAction;
    float takePriority = 0;

    Person *person = unit->GetPerson();

    AUTO_STATIC_ARRAY(bool, checked, 32);
    // weapons
    int n = _weaponCargo.Size();
    checked.Resize(n); for (int i=0; i<n; i++) checked[i] = false;
    for (int i=0; i<n; i++)
    {
      if (checked[i]) continue;
      checked[i] = true;
      const WeaponType *weapon = _weaponCargo[i];
      Assert(weapon->_scope == 2);
      AUTO_STATIC_ARRAY(Ref<const WeaponType>, conflict, 16);
      if (!person->CheckWeapon(weapon, conflict)) continue;
      // can take this weapon
      if (takeAction)
      {
        // >1 actions available
        canCollect = false;
        break;
      }
      // assign priority by weapon type
      float priority = 0;
      if (weapon->_weaponType & MaskSlotPrimary) priority += 0.002f;
      else if (weapon->_weaponType & MaskSlotSecondary) priority += 0.003f;
      else if (weapon->_weaponType & MaskSlotHandGun) priority += 0.001f;
      float value = weapon->_value;
      saturate(value, 0, 99.0f);
      priority += 0.001f * 0.01f * value;
      takeAction = new ActionWeapon(conflict.Size() > 0 ? ATTakeDropWeapon : ATTakeWeapon, parent, weapon->GetName(), false);
      takePriority = priority;

      for (int j=i+1; j<n; j++)
        if (_weaponCargo[j] == weapon) checked[j] = true;
    }
    if (canCollect)
    {
      // magazines
      n = _magazineCargo.Size();
      checked.Resize(n); for (int i=0; i<n; i++) checked[i] = false;
      for (int i=0; i<n; i++)
      {
        if (checked[i]) continue;
        checked[i] = true;
        const Magazine *magazine = _magazineCargo[i];
        Assert(magazine->_type->_scope == 2);
        if (magazine->GetAmmo() == 0) continue;
        if (!person->IsMagazineUsable(magazine->_type)) continue;
        for (int j=i+1; j<n; j++)
        {
          const Magazine *magazineJ = _magazineCargo[j];
          if (magazineJ->_type == magazine->_type)
          {
            checked[j] = true;
            if (magazineJ->GetAmmo() > magazine->GetAmmo()) magazine = magazineJ;
          }
        }
        AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
        if (!person->CheckMagazine(magazine, conflict)) continue;
        // assign priority by weapon type
        float priority = 0;
        for (int j=0; j<person->NWeaponSystems(); j++)
        {
          const WeaponType *weapon = person->GetWeaponSystem(j);
          if (!weapon) continue;
          bool found = false;
          for (int k=0; k<weapon->_muzzles.Size(); k++)
          {
            const MuzzleType *muzzle = weapon->_muzzles[k];
            if (muzzle && muzzle->CanUse(magazine->_type))
            {
              if (weapon->_weaponType & MaskSlotPrimary) priority += 0.002f;
              else if (weapon->_weaponType & MaskSlotSecondary) priority += 0.003f;
              else if (weapon->_weaponType & MaskSlotHandGun) priority += 0.001f;
              float value = magazine->_type->_value;
              saturate(value, 0, 99.0f);
              priority += 0.001f * 0.01f * value;
              found = true;
              break;
            }
          }
          if (found) break;
        }
        // can take this magazine
        if (takeAction)
        {
          // >1 actions available
          canCollect = false;
          break;
        }
        takeAction = new ActionMagazineType(conflict.Size() > 0 ? ATTakeDropMagazine : ATTakeMagazine, parent, magazine->_type->GetName(), false);
        takePriority = priority;
      }
    }

    if (canCollect && takeAction)
    {
      // right single take action
      UIAction action(takeAction);
      action.position = parent->RenderVisualState().PositionModelToWorld(type->GetSupplyPoint());
      action.priority += takePriority;
      actions.Add(action);
    }
  }
}

int ResourceSupply::GetFreeWeaponCargo(const EntityAI *parent) const
{
  return parent->GetType()->_maxWeaponsCargo - _weaponCargo.Size();
}

int ResourceSupply::GetFreeMagazineCargo(const EntityAI *parent) const
{
  return parent->GetType()->_maxMagazinesCargo - _magazineCargo.Size();
}

int ResourceSupply::GetFreeBackpackCargo(const EntityAI *parent) const
{
  int free = parent->GetType()->_maxBackpacksCargo - _backpacks.Size();
  if(parent->HaveBackpack()) free--;
  return free;
}

#define WEAPON_SIZE  10
#define HAND_GUN_SIZE  5
#define ITEM_SIZE  1

int ResourceSupply::GetFreeWeaponCargoBackpack(const EntityAI *parent, WeaponType *type) const
{
  //number of rifles/launchers
  int weaponsCount = 0;
  //number of taken slots
  int takenSpace = 0;
  // primary/secondary/handgun weapon takes 10 ammo slots, weapon items 1
  for(int i=0; i< _weaponCargo.Size(); i++)
  {
    if((_weaponCargo[i]->_weaponType & MaskSlotPrimary) != 0
      || (_weaponCargo[i]->_weaponType & MaskSlotSecondary) != 0)
    {
      takenSpace+=WEAPON_SIZE;
      weaponsCount++;
    }
    else if ((_weaponCargo[i]->_weaponType & MaskSlotHandGun) != 0)
    {
      takenSpace+=HAND_GUN_SIZE;
      weaponsCount++;
    }
    else
      takenSpace+=ITEM_SIZE;
  }

  //space taken by magazines
  for(int i=0; i< _magazineCargo.Size(); i++)
  {
    if((_magazineCargo[i]->_type->_magazineType & MaskSlotItem) !=0 )
      takenSpace+=(_magazineCargo[i]->_type->_magazineType & MaskSlotItem) >> 8; // =  / 0x0100 
    else if((_magazineCargo[i]->_type->_magazineType & MaskSlotHandGunItem) !=0 )
      takenSpace+=(_magazineCargo[i]->_type->_magazineType & MaskSlotHandGunItem) >> 4;  // =  / 0x0010
  }

  //space required for new item
  int itemSize = ITEM_SIZE;
  if((type->_weaponType & MaskSlotPrimary) != 0
    || (type->_weaponType  & MaskSlotSecondary) != 0
    || (type->_weaponType  & MaskSlotHandGun) != 0)
  {
    if((type->_weaponType  & MaskSlotHandGun) != 0) itemSize = HAND_GUN_SIZE;
    else  itemSize = WEAPON_SIZE;
    // min (space for rifles, free space / space required)
    return std::min(parent->GetType()->_maxWeaponsCargo - weaponsCount,(parent->GetType()->_maxMagazinesCargo - takenSpace) / itemSize);
  }
  //free space / space required
  return (parent->GetType()->_maxMagazinesCargo - takenSpace) / itemSize;
}

int ResourceSupply::GetFreeMagazineCargoBackpack(const EntityAI *parent, MagazineType *type) const
{
  //number of taken slots
  int takenSpace = 0;
  // primary/secondary/handgun weapon takes WEAPON_SIZE ammo slots, weapon items ITEM_SIZE slota
  for(int i=0; i< _weaponCargo.Size(); i++)
  {
    if((_weaponCargo[i]->_weaponType & MaskSlotPrimary) != 0
      || (_weaponCargo[i]->_weaponType & MaskSlotSecondary) != 0) takenSpace+=WEAPON_SIZE;
    else if ((_weaponCargo[i]->_weaponType & MaskSlotHandGun) != 0)  takenSpace+=HAND_GUN_SIZE;
    else  takenSpace+=ITEM_SIZE;
  }
  //space taken by magazines
  for(int i=0; i< _magazineCargo.Size(); i++)
  {
    if((_magazineCargo[i]->_type->_magazineType & MaskSlotItem) !=0 )
      takenSpace+=(_magazineCargo[i]->_type->_magazineType & MaskSlotItem) >> 8; // =  / 0x0100 
    else if((_magazineCargo[i]->_type->_magazineType & MaskSlotHandGunItem) !=0 )
      takenSpace+=(_magazineCargo[i]->_type->_magazineType & MaskSlotHandGunItem) >> 4;  // =  / 0x0010
  }

  //size of magazine we are trying to drop
  int itemSize = 1;
  if((type->_magazineType & MaskSlotItem) !=0 )
    itemSize= std::max(1,(type->_magazineType & MaskSlotItem) >> 8); // =  / 0x0100 
  else if((type->_magazineType & MaskSlotHandGunItem) !=0 )
    itemSize= std::max(1,(type->_magazineType & MaskSlotHandGunItem) >> 4);  // =  / 0x0010

  //free space / space required
  return (parent->GetType()->_maxMagazinesCargo - takenSpace) / itemSize;
}

int ResourceSupply::GetFreeCargoBackpack(const EntityAI *parent) const
{
  //number of taken slots
  int takenSpace = 0;
  // primary/secondary/handgun weapon takes WEAPON_SIZE ammo slots, weapon items ITEM_SIZE slota
  for(int i=0; i< _weaponCargo.Size(); i++)
  {
    if((_weaponCargo[i]->_weaponType & MaskSlotPrimary) != 0
      || (_weaponCargo[i]->_weaponType & MaskSlotSecondary) != 0) takenSpace+=WEAPON_SIZE;
    else if ((_weaponCargo[i]->_weaponType & MaskSlotHandGun) != 0)  takenSpace+=HAND_GUN_SIZE;
    else  takenSpace+=ITEM_SIZE;
  }
  //space taken by magazines
  for(int i=0; i< _magazineCargo.Size(); i++)
  {
    if((_magazineCargo[i]->_type->_magazineType & MaskSlotItem) !=0 )
      takenSpace+=(_magazineCargo[i]->_type->_magazineType & MaskSlotItem) >> 8; // =  / 0x0100 
    else if((_magazineCargo[i]->_type->_magazineType & MaskSlotHandGunItem) !=0 )
      takenSpace+=(_magazineCargo[i]->_type->_magazineType & MaskSlotHandGunItem) >> 4;  // =  / 0x0010
  }

  //size of magazine we are trying to drop
  int itemSize = 1;
  //free space / space required
  return (parent->GetType()->_maxMagazinesCargo - takenSpace) / itemSize;
}


bool ResourceSupply::FindWeapon(const WeaponType *weapon, int *count) const
{
  if (count)
  {
    *count = 0;
    for (int i=0; i<_weaponCargo.Size(); i++)
      if (_weaponCargo[i] == weapon) (*count)++;
    return *count > 0;
  }
  else
  {
    for (int i=0; i<_weaponCargo.Size(); i++)
      if (_weaponCargo[i] == weapon) return true;
    return false;
  }
}

bool ResourceSupply::FindMagazine(const Magazine *magazine) const
{
  for (int i=0; i<_magazineCargo.Size(); i++)
    if (_magazineCargo[i] == magazine) return true;
  return false;
}

const Magazine *ResourceSupply::FindMagazine(RString name, int *count) const
{
  Ref<MagazineType> type = MagazineTypes.New(name);
  return FindMagazine(type, count);
}

const Magazine *ResourceSupply::FindMagazine(const MagazineType *type, int *count) const
{
  const Magazine *magazine = NULL;
  int ammo = 0;
  if (count) *count = 0;
  for (int i=0; i<_magazineCargo.Size(); i++)
  {
    Magazine *m = _magazineCargo[i];
    if (!m) continue;
    if (m->_type != type) continue;
    if (count && m->GetAmmo() > 0) (*count)++;
    if (m->GetAmmo() > ammo)
    {
      magazine = m;
      ammo = m->GetAmmo();
    }
  }
  return magazine;
}

Magazine *ResourceSupply::FindMagazine(CreatorId creator, int id) const
{
  for (int i=0; i<_magazineCargo.Size(); i++)
  {
    Magazine *magazine = _magazineCargo[i];
    if (!magazine) continue;
    if (magazine->_creator == creator && magazine->_id == id) return magazine;
  }
  return NULL;
}

EntityAI *ResourceSupply::FindBackpack(RString name)
{
  for (int i=0; i<_backpacks.Size(); i++)
  {
    if(!_backpacks[i]) continue;
    EntityAI *backpack = _backpacks[i];
    if (strcmpi(name,backpack->GetType()->GetName())==0) return backpack;

  }
  return NULL;
}

void ResourceSupply::OfferWeapon(EntityAI *parent, EntityAIFull *to, const WeaponType *weapon, int slots, bool useBackpack)
{
  DoAssert(parent->IsLocal());

#if LOG_WEAPONS_CHANGES
RptF
(
  "ResourceSupply::OfferWeapon from %s, to %s, weapon %s, slots 0x%x",
  (const char *)parent->GetDebugName(),
  (const char *)to->GetDebugName(),
  (const char *)weapon->GetName(),
  slots
);
#endif

  if (!to) return;

  if (!FindWeapon(weapon))
  {
#if LOG_WEAPONS_CHANGES
RptF(" - weapon not found");
#endif
    return;
  }

  if(parent->Type()->IsBackpack())
  {
    // 1. bags are not deleted if not need
    // 2. need to make space for weapon before removing soldiers's weapon 

    RemoveWeaponCargo(parent, weapon);
    GetNetworkManager().RemoveWeaponCargo(parent, weapon->GetName());

  // send asked weapon
  if (to->IsLocal())
    to->ReplaceWeapon(parent, weapon, useBackpack);
  else
    GetNetworkManager().ReplaceWeapon(parent, to, weapon, useBackpack);
  }
  else
  {
    RemoveWeaponCargo(parent, weapon);
    GetNetworkManager().RemoveWeaponCargo(parent, weapon->GetName());

    // send asked weapon
    if (to->IsLocal())
      to->ReplaceWeapon(parent, weapon, useBackpack);
    else
      GetNetworkManager().ReplaceWeapon(parent, to, weapon, useBackpack);
  }


  if(!useBackpack)
  {
    // send asked magazine
    int freeSlots = slots & MaskSlotItem;
    int freeSlotsHG = slots & MaskSlotHandGunItem;
    for (int i=0; i<weapon->_muzzles.Size(); i++)
    {
      MuzzleType *muzzle = weapon->_muzzles[i];
      for (int j=0; j<muzzle->_magazines.Size(); j++)
      {
        MagazineType *type = muzzle->_magazines[j];
        int used = type->_magazineType;
        int usedSlots = used & MaskSlotItem;
        int usedSlotsHG = used & MaskSlotHandGunItem;
        if (usedSlots > freeSlots || usedSlotsHG > freeSlotsHG) continue; // not enough space

        for (int k=0; k<_magazineCargo.Size();)
        {
          Ref<Magazine> magazine = _magazineCargo[k];
          if (!magazine || magazine->_type != type)
          {
            k++;
            continue;
          }
          // offer magazine
          freeSlots -= usedSlots;
          freeSlotsHG -= usedSlotsHG;

          RemoveMagazineCargo(parent, magazine);
          GetNetworkManager().RemoveMagazineCargo(parent, magazine->_type->GetName(), magazine->GetAmmo());

          if (to->IsLocal())
            to->ReplaceMagazine(parent, magazine, true, useBackpack); // reload
          else
            GetNetworkManager().ReplaceMagazine(parent, to, magazine, true, useBackpack);

          if (usedSlots > freeSlots || usedSlotsHG > freeSlotsHG) break; // not enough space for next one
        }
      }
    }
  }
}

void ResourceSupply::OfferMagazine(EntityAI *parent, EntityAIFull *to, const MagazineType *type, bool useBackpack)
{
  DoAssert(parent->IsLocal());
#if LOG_WEAPONS_CHANGES
RptF
(
  "ResourceSupply::OfferMagazine from %s, to %s, magazine %s",
  (const char *)parent->GetDebugName(),
  (const char *)to->GetDebugName(),
  (const char *)type->GetName()
);
#endif

  if (!to) return;

  Ref<Magazine> magazine = const_cast<Magazine *>(FindMagazine(type));
  if (!magazine)
  {
#if LOG_WEAPONS_CHANGES
RptF(" - magazine not found");
#endif
    return;
  }
  //first remove, so there is space to return items
  RemoveMagazineCargo(parent, magazine);
  GetNetworkManager().RemoveMagazineCargo(parent, magazine->_type->GetName(), magazine->GetAmmo());

  if (to->IsLocal())
    to->ReplaceMagazine(parent, magazine, false, useBackpack); // do not reload
  else
    GetNetworkManager().ReplaceMagazine(parent, to, magazine, false, useBackpack);
}

void ResourceSupply::OfferBackpack(EntityAI *parent, EntityAIFull *to, RString name)
{
  DoAssert(parent->IsLocal());

  if (!to) return;

  Ref<EntityAI> backpack = FindBackpack(name);
  if (!backpack)
  {
#if LOG_WEAPONS_CHANGES
    RptF(" - backpack not found");
#endif
    return;
  }

  //first remove, so there is space to return items
  RemoveBackpackCargo(parent, backpack);
  GetNetworkManager().RemoveBackpackCargo(parent, backpack);

  // send asked weapon
  if (to->IsLocal())
    to->ReplaceBackpack(parent, backpack);
  else
    GetNetworkManager().ReplaceBackpack(parent, to, backpack);
}

void ResourceSupply::ReturnWeapon(EntityAI *parent, const WeaponType *weapon)
{
  DoAssert(parent->IsLocal());

#if LOG_WEAPONS_CHANGES
RptF
(
  "ResourceSupply::ReturnWeapon from %s, weapon %s",
  (const char *)parent->GetDebugName(),
  (const char *)weapon->GetName()
);
#endif

  AddWeaponCargo(parent, const_cast<WeaponType *>(weapon), 1, false);
}

void ResourceSupply::ReturnMagazine(EntityAI *parent, Magazine *magazine)
{
  DoAssert(parent->IsLocal());

#if LOG_WEAPONS_CHANGES
RptF
(
  "ResourceSupply::ReturnMagazine from %s, magazine %s 0x%x:0x%x",
  (const char *)parent->GetDebugName(),
  (const char *)magazine->_type->GetName(),
  magazine->_creator, magazine->_id
);
#endif

  if (magazine && magazine->GetAmmo() > 0)
  {
    AddMagazineCargo(parent, magazine, false);
  }
}

void ResourceSupply::ReturnBackpack(EntityAI *parent, EntityAI *backpack)
{
  DoAssert(parent->IsLocal());

  if (backpack)
  {
    AddBackpackCargo(parent, backpack, false, parent);
  }
}

void ResourceSupply::ReturnEquipment(EntityAI *parent, AutoArray<RString> weapons, RefArray<Magazine> magazines, EntityAI *backpack)
{
  DoAssert(parent->IsLocal());

  for( int i = 0; i< weapons.Size(); i++) 
  {
    Ref<WeaponType> weaponType = WeaponTypes.New(weapons[i]);
    if(weaponType) AddWeaponCargo(parent, weaponType, 1, false);
  }

  for (int i = 0; i < magazines.Size(); i++)
  {
    if (magazines[i] && magazines[i]->GetAmmo() > 0)
    {
      AddMagazineCargo(parent, magazines[i], false);
    }
  }

  if (backpack) AddBackpackCargo(parent, backpack, false, parent);
}

static bool WeaponCanBeDropped(Vector3Par p)
{
  //LogF("WeaponCanBeDropped test at %.1f,%.1f",p.X(),p.Z());
  // check if we are inside some object
  int lx = (int)(p.X()*InvLandGrid), lz = (int)(p.Z()*InvLandGrid);
  for (int lxx=lx-1; lxx<=lx+1; lxx++) for (int lzz=lz-1; lzz<=lz+1; lzz++)
  {
    if (!InRange(lxx,lzz)) continue;
    const ObjectListUsed &list = GLandscape->UseObjects(lxx,lzz);
    for (int o=0; o<list.Size(); o++)
    {
      const Object *oo = list[o];
      if (oo->GetType()!=Primary && oo->GetType()!=TypeVehicle) continue;
      if (!oo->GetShape()) continue;
      // check if we are inside
      float colSize = oo->CollisionSize();
      saturateMax(colSize,1);
      float distXZ2 = p.DistanceXZ2(oo->FutureVisualState().Position());
      if (distXZ2>=Square(colSize)) continue;
      return false;
    }
  }
  //LogF("  WeaponCanBeDropped passed");
  return true;
}


const int WeaponCountInOperGrid = 3;

static bool OperGridWeaponCanBeDropped(Vector3Par pos, void *context)
{
  //LogF("OperGridWeaponCanBeDropped test at %.1f,%.1f",pos.X(),pos.Z());
  // one square is big enough to contain several weapons (3x3 m)
  const int weaponMax = (WeaponCountInOperGrid-1)/2;
  const float weaponDropGrid = OperItemGrid*(1.0f/WeaponCountInOperGrid);
  for (int x=-weaponMax; x<=weaponMax; x++)
  for (int z=-weaponMax; z<=weaponMax; z++)
  {
    Vector3 p = pos+Vector3(x*weaponDropGrid,0,z*weaponDropGrid);
    if (WeaponCanBeDropped(p))
    {
      //LogF("  OperGridWeaponCanBeDropped passed");
      return true;
    }
  }
  return false;
}

bool FindDropPos(Matrix4 &transform)
{
  Vector3 pos = transform.Position(), normal = VUp;

  //LogF("FindFreePosition drop pos %.1f,%.1f",pos.X(),pos.Z());
  if
  (
    !AIUnit::FindFreePosition
    (
      pos, normal, true, GWorld->Preloaded(VTypePaperCar), OperGridWeaponCanBeDropped
    )
  )
  {
    // no free position - we will rather destroy the container
    // that place it somewhere where it cannot be reached
    return false;
  }
  //LogF("Find drop pos %.1f,%.1f",pos.X(),pos.Z());
  // we have to find some position where is no weapon lying yet
  // search max. 50 m from this position

  if (WeaponCanBeDropped(pos))
  {
    transform.SetPosition(pos);
    Matrix3 dir(MRotationY,H_PI*2*GRandGen.RandomValue());
    transform.SetUpAndDirection(normal,dir.Direction());
    return true;
  }
  // one square is big enough to contain several weapons (3x3 m)
  const int weaponMax = (WeaponCountInOperGrid-1)/2;
  const float weaponDropGrid = OperItemGrid*(1.0f/WeaponCountInOperGrid);
  for (int x=-weaponMax; x<=weaponMax; x++)
  for (int z=-weaponMax; z<=weaponMax; z++)
  {
    Vector3 p = pos+Vector3(x*weaponDropGrid,0,z*weaponDropGrid);
    if (WeaponCanBeDropped(p))
    {
      transform.SetPosition(p);
      Matrix3 dir(MRotationY,H_PI*2*GRandGen.RandomValue());
      transform.SetUpAndDirection(normal,dir.Direction());
      return true;
    }
    
  }
  return false;
}

static bool DropWeapon(WeaponType *weapon, const Matrix4 &trans)
{
  Matrix4 transform = trans;
  if (!FindDropPos(transform)) return false;

  // create container
  Ref<EntityAI> veh = GWorld->NewVehicleWithID("WeaponHolder");
  if (!veh) return false;

  veh->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
  veh->SetTransform(transform);
  veh->Init(transform, true);
  veh->OnEvent(EEInit);

  // add weapon to veh
#if _VBS3 //changed handling of cargo space
  if (true)
#else
  int free = SUPPLY(veh,GetFreeWeaponCargo, ());
  if (free>0)
#endif
  {
    GWorld->AddSlowVehicle(veh);

    if (GWorld->GetMode() == GModeNetware)
      GetNetworkManager().CreateVehicle(veh, VLTSlowVehicle, "", -1);

    veh->AddWeaponCargo(weapon, 1, false);

    return true;
  }
  else
  {
    ErrF("WeaponHolder has no space for weapon");
    return false;
  }
}

static bool DropMagazine(Magazine *magazine, Matrix4 &transform)
{
  if (!FindDropPos(transform)) return false;

  // create container
  Ref<EntityAI> veh = GWorld->NewVehicleWithID("WeaponHolder");
  if (!veh) return false;
  
  veh->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
  veh->SetTransform(transform);
  veh->Init(transform, true);
  veh->OnEvent(EEInit);

  // add weapon to veh
#if _VBS3 //changed handling of cargo space
  if(true)
#else
  int free = SUPPLY(veh,GetFreeMagazineCargo, ());
  if(free>0)
#endif
  {
    GWorld->AddSlowVehicle(veh);
    if (GWorld->GetMode() == GModeNetware)
      GetNetworkManager().CreateVehicle(veh, VLTSlowVehicle, "", -1);

    veh->AddMagazineCargo(magazine, false);
    return true;
  }
  else
  {
    ErrF("WeaponHolder has no space for magazine");
    return false;
  }
}

void ResourceSupply::DeleteIfNotNeeded(EntityAI *parent)
{
  if (parent->GetType()->_forceSupply && !parent->AreResourcesLocked() && parent->IsLocal() && _magazineCargo.Size() == 0 && _weaponCargo.Size() == 0)
    parent->SetDelete();
}

int ResourceSupply::AddWeaponCargo(EntityAI *parent, WeaponType *weapon, int count, bool localOnly)
{
  int free = 0;
  if(parent->Type()->IsBackpack())
    free = GetFreeWeaponCargoBackpack(parent,weapon);
  else 
    free = GetFreeWeaponCargo(parent);

#if _VBS3 //changed handling of cargo space
  if(false)
#else
  if(count > free && !localOnly)
#endif
  {
    Vector3 pos = parent->FutureVisualState().PositionModelToWorld
    (
      parent->GetType()->GetSupplyPoint()
    );
    Matrix3 dir = parent->FutureVisualState().Orientation();
    Matrix4 transform;
    transform.SetPosition(pos);
    transform.SetOrientation(dir);
    while (count > free && count>0)
    {
      DropWeapon(weapon, transform);
      count--;
    }
  }

  if(localOnly && count > free) count = std::max(free,0); 

  int index = -1;
  for (int j=0; j<count; j++)
  {
    if (parent->GetType()->_showWeaponCargo) unconst_cast(weapon)->ShapeAddRef();
    index = _weaponCargo.Add(weapon);
    if (!localOnly && GWorld->GetMode() == GModeNetware)
      GetNetworkManager().AddWeaponCargo(parent, weapon->GetName());
  }

  void UpdateWeaponsInBriefing(EntityAI *owner);
  UpdateWeaponsInBriefing(parent);

#if _VBS3
  parent->OnEvent(EECargoChanged);
#endif

  return index;
}

bool ResourceSupply::RemoveWeaponCargo(EntityAI *parent, const WeaponType *weapon)
{
  for (int i=0; i<_weaponCargo.Size(); i++)
  {
    if (_weaponCargo[i] == weapon)
    {
      if (parent->GetType()->_showWeaponCargo) unconst_cast(weapon)->ShapeRelease();
      _weaponCargo.Delete(i);

      void UpdateWeaponsInBriefing(EntityAI *owner);
      UpdateWeaponsInBriefing(parent);

#if _VBS3
      parent->OnEvent(EECargoChanged);
#endif
      DeleteIfNotNeeded(parent);
      return true;
    }
  }
  return false;
}

void ResourceSupply::ClearWeaponCargo(EntityAI *parent, bool localOnly)
{
  if (parent->GetType()->_showWeaponCargo)
  {
    for (int i=0; i<_weaponCargo.Size(); i++)
      if (_weaponCargo[i]) _weaponCargo[i]->ShapeRelease();
  }
  _weaponCargo.Clear();
#if _VBS3
  parent->OnEvent(EECargoChanged);
#endif

  if (!localOnly && GWorld->GetMode() == GModeNetware)
    GetNetworkManager().ClearWeaponCargo(parent);

  if(localOnly || parent->IsLocal()) DeleteIfNotNeeded(parent);
}

/*!
\patch 5153 Date 4/3/2007 by Jirka
- Fixed: MP - multiplication of magazines when putting them into a full crate
*/

int ResourceSupply::AddMagazineCargo(EntityAI *parent, Magazine *magazine, bool localOnly)
{
#if _VBS3 //changed handling of cargo space
  if(false)
#else
  int free = 0;
  if(parent->Type()->IsBackpack())
    free = GetFreeMagazineCargoBackpack(parent, magazine->_type);
  else 
    free = GetFreeMagazineCargo(parent);

  if(free < 1 && !localOnly)
#endif
  {
    Vector3 pos;
    if(parent->Type()->IsBackpack() && parent->GetHierachyParent())
      pos = parent->GetHierachyParent()->FutureVisualState().Position();
    else
      pos = parent->FutureVisualState().PositionModelToWorld
      (
      parent->GetType()->GetSupplyPoint()
      );

    Matrix3 dir = parent->FutureVisualState().Orientation();
    Matrix4 transform;
    transform.SetPosition(pos);
    transform.SetOrientation(dir);
    DropMagazine(magazine, transform);

    void UpdateWeaponsInBriefing(EntityAI *owner);
    UpdateWeaponsInBriefing(parent);

    return -1;
  }
  
  if (parent->GetType()->_showWeaponCargo) magazine->_type->MagazineShapeAddRef();
  int index = _magazineCargo.Add(magazine);
  if (!localOnly && GWorld->GetMode() == GModeNetware)
    GetNetworkManager().AddMagazineCargo(parent, magazine);
#if _VBS3
  parent->OnEvent(EECargoChanged);
#endif

  void UpdateWeaponsInBriefing(EntityAI *owner);
  UpdateWeaponsInBriefing(parent);

  return index;
}

int ResourceSupply::AddMagazineCargoCount(EntityAI *parent, MagazineType *type, int count, bool localOnly)
{
  int free = 0;
  if(parent->Type()->IsBackpack())
    free = GetFreeMagazineCargoBackpack(parent,type);
  else 
    free = GetFreeMagazineCargo(parent);

  if (!type) return -1;

#if _VBS3 //changed handling of cargo space
  if(false)    
#else
  if(count > free && !localOnly)
#endif
  {
    Vector3 pos = parent->FutureVisualState().PositionModelToWorld
    (
      parent->GetType()->GetSupplyPoint()
    );
    Matrix3 dir = parent->FutureVisualState().Orientation();
    Matrix4 transform;
    transform.SetPosition(pos);
    transform.SetOrientation(dir);
    while (count > free && count>0)
    {
      Ref<Magazine> magazine = new Magazine(type);
      magazine->SetAmmo(type->_maxAmmo);
      magazine->_reload = 0;
      magazine->_reloadDuration = 1;
      magazine->_reloadMagazine = 0;
      DropMagazine(magazine, transform);
      count--;
    }
  }

  if(localOnly && count > free) count = std::max(free,0); 

  int index = -1;
  for (int j=0; j<count; j++)
  {
    Ref<Magazine> magazine = new Magazine(type);
    magazine->SetAmmo(type->_maxAmmo);
    magazine->_reload = 0;
    magazine->_reloadDuration = 1;
    magazine->_reloadMagazine = 0;
    if (parent->GetType()->_showWeaponCargo) magazine->_type->MagazineShapeAddRef();
    index = _magazineCargo.Add(magazine);
    if (!localOnly && GWorld->GetMode() == GModeNetware)
      GetNetworkManager().AddMagazineCargo(parent, magazine);
  }
#if _VBS3
  parent->OnEvent(EECargoChanged);
#endif

  void UpdateWeaponsInBriefing(EntityAI *owner);
  UpdateWeaponsInBriefing(parent);

  return index;
}

bool ResourceSupply::RemoveMagazineCargo(EntityAI *parent, Magazine *magazine)
{
  for (int i=0; i<_magazineCargo.Size(); i++)
  {
    if (_magazineCargo[i] == magazine)
    {
      if (parent->GetType()->_showWeaponCargo) magazine->_type->MagazineShapeRelease();
      _magazineCargo.Delete(i);

      void UpdateWeaponsInBriefing(EntityAI *owner);
      UpdateWeaponsInBriefing(parent);

#if _VBS3
      parent->OnEvent(EECargoChanged);
#endif
      DeleteIfNotNeeded(parent);
      return true;
    }
  }
  return false;
}

bool ResourceSupply::RemoveMagazineCargo(EntityAI *parent, RString type, int ammo)
{
  Ref<MagazineType> magazineType = MagazineTypes.New(type);
  if (!magazineType)
  {
    RptF("Wrong magazine type %s", (const char *)type);
  }
  for (int i=0; i<_magazineCargo.Size(); i++)
  {
    Magazine *magazine = _magazineCargo[i];
    if (magazine->_type == magazineType && magazine->GetAmmo() == ammo)
    {
      // found
      if (parent->GetType()->_showWeaponCargo) magazineType->MagazineShapeRelease();
      _magazineCargo.Delete(i);

      void UpdateWeaponsInBriefing(EntityAI *owner);
      UpdateWeaponsInBriefing(parent);

#if _VBS3
      parent->OnEvent(EECargoChanged);
#endif
      DeleteIfNotNeeded(parent);
      return true;
    }
  }
  RptF("Magazine %s (%d ammo left) not found", (const char *)type, ammo);
  return false;
}

void ResourceSupply::ClearMagazineCargo(EntityAI *parent, bool localOnly)
{
  if (parent->GetType()->_showWeaponCargo)
  {
    for (int i=0; i<_magazineCargo.Size(); i++)
      if (_magazineCargo[i]) _magazineCargo[i]->_type->MagazineShapeRelease();
  }
  _magazineCargo.Clear();

  if (!localOnly && GWorld->GetMode() == GModeNetware)
    GetNetworkManager().ClearMagazineCargo(parent);

  if(localOnly || parent->IsLocal()) DeleteIfNotNeeded(parent);
}


void ResourceSupply::ClearBackpackCargo(EntityAI *parent, bool localOnly)
{
  //if (parent->GetType()->_showWeaponCargo)
  {
    for (int i=0; i<_backpacks.Size(); i++)
      if (_backpacks[i])
      {
        if (_backpacks[i]->IsLocal() && GWorld->IsOutVehicle(_backpacks[i]))
        {    
          GWorld->RemoveOutVehicle(_backpacks[i]);
          _backpacks[i]->SetDelete();
        }
      }
  }

  _backpacks.Clear();
  if (!localOnly && GWorld->GetMode() == GModeNetware)
    GetNetworkManager().ClearBackpackCargo(parent);

 if(localOnly || parent->IsLocal()) DeleteIfNotNeeded(parent);
}

int ResourceSupply::AddBackpackCargo(EntityAI *parent, EntityAI *backpack, bool localOnly, EntityAI *man)
{
  int free = 0;
  if(parent->Type()->IsBackpack())free = 0;
  else free = GetFreeBackpackCargo(parent);

  if( free < 1 && !localOnly)
  {
    Vector3 dirtmp = man? man->FutureVisualState().Direction():parent->FutureVisualState().Direction();
    Vector3 dirXZ = (Vector3(dirtmp.X(),0,dirtmp.Z()));
    dirXZ.Normalize();
    Vector3 position = (man? man->FutureVisualState().Position():parent->FutureVisualState().Position()) + 0.5f*dirXZ + VUp*0.5f;
    
    Matrix3 dir;
    dir.SetUpAndDirection(VUp, dirtmp);

    Matrix4 transform;
    transform.SetPosition(position);
    transform.SetOrientation(dir);

    FindDropPos(transform);

    backpack->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
    backpack->SetTransform(transform);

    backpack->Init(transform, true); // do not reset movement
    backpack->OnEvent(EEInit);
    backpack->Init(transform, false); // do not reset movement

   // if (_backpack->IsLocal()) 
    {
      backpack->ResetMoveOut();
      GLOB_WORLD->AddSlowVehicle(backpack);
      GLOB_WORLD->RemoveOutVehicle(backpack);
      // we need other entities nearby to be notified of our presence
      backpack->OnMoved();
      GWorld->OnEntityMovedFar(backpack);
    }
    if(GWorld->GetMode()  == GModeNetware)
    {      
      GetNetworkManager().DropBackpack(backpack, transform);
    }
    void UpdateWeaponsInBriefing(EntityAI *owner);
    UpdateWeaponsInBriefing(parent);

    return -1;
  }
  else
  {
    if(backpack)
    {
      backpack->MoveOut(parent);
    }
    int index = _backpacks.Add(backpack);
    if (!localOnly && GWorld->GetMode() == GModeNetware)
      GetNetworkManager().AddBackpackCargo(parent, backpack);

    void UpdateWeaponsInBriefing(EntityAI *owner);
    UpdateWeaponsInBriefing(parent);

    return index;
  }
}

bool ResourceSupply::RemoveBackpackCargo(EntityAI *parent, EntityAI *backpack)
{
  for (int i=0; i<_backpacks.Size(); i++)
  {
    if (_backpacks[i] == backpack)
    {
   //   _backpacks[i]->ResetMoveOut(); // mark it is in landscape   
      _backpacks.Delete(i);

      void UpdateWeaponsInBriefing(EntityAI *owner);
      UpdateWeaponsInBriefing(parent);

      DeleteIfNotNeeded(parent);
      return true;
    }
  }
  return false;
}

void EntityAI::SupplyStarted( AIUnit *unit )
{
  LogF
  (
    "%s SupplyStarted for %s",
    (const char *)GetDebugName(),(const char *)unit->GetDebugName()
  );

  if (!_supply) return;
  SupplyUnitList &list = GetSupplyUnits();

  Assert( list.Find(unit)<0 );
  list.AddUnique(unit);
}

void EntityAI::SupplyFinished( AIUnit *unit )
{
  if (!_supply) return;

  // done/canceled
  LogF
  (
    "%s SupplyFinished for %s",
    (const char *)GetDebugName(),(const char *)unit->GetDebugName()
  );
  if (GetAllocSupply()==unit->GetVehicle())
  {
    LogF("  patch unalloc");
    SetAllocSupply(NULL);
  }
  
  SupplyUnitList &list = GetSupplyUnits();
  for (int i=0; i<list.Size(); i++)
  {
    if( unit == list[i] )
    {
      list.Delete(i);
      break;
    }
  }

  if (list.Size()==0)
  {
    if (GetAllocSupply())
    {
      LogF("  %s: hard patch unalloc",(const char *)GetDebugName());
    }
  }
  UpdateStop();
}

void EntityAI::WaitForSupply(AIUnit *unit)
{
  // add to queue only when commander is alive
  AIBrain *commander = CommanderUnit();
  if (!commander || !commander->LSIsAlive()) return;
  SupplyStarted(unit);  
}

bool EntityAI::CanCancelStop() const
{
/*
  if (_supplyUnits.Count()<=0)
  {
    _supplyUnits.Clear();
    return true;
  }
*/
  return true;
}

void EntityAI::SetAllocSupply( EntityAIFull *vehicle )
{
  if( _supply )
  {
    if (_supply->GetAlloc()!=vehicle)
    {
      LogF
      (
        "%s Allocated for %s",
        (const char *)GetDebugName(),
        vehicle ? (const char *)vehicle->GetDebugName() : "NULL"
      );
      _supply->SetAlloc(vehicle);
    }
  }
}

EntityAIFull *EntityAI::GetAllocSupply() const
{
  return _supply ? _supply->GetAlloc() : NULL;
}

bool EntityAI::Supply(EntityAIFull *vehicle, const Action *action)
{
  //Operator ? shouldn't be used in .NET2003 due to compiler error
  //return _supply? _supply->Supply(vehicle, action) : false;
  if (_supply)
    return _supply->Supply(vehicle, action);
  else 
    return false;
}

bool EntityAI::SupplyInverse(EntityAIFull *vehicle, const Action *action)
{
  //Operator ? shouldn't be used in .NET2003 due to compiler error
  //return _supply? _supply->Supply(vehicle, action) : false;
  if (vehicle->GetSupply())
  {
    EntityAIFull *entity  = dyn_cast<EntityAIFull>(this);
    if (entity) // FIX: CommanderUnit returns NULL sometimes
      return vehicle->GetSupply()->Supply(entity, action);
  }
  return false;
}

void EntityAI::CancelSupply()
{
  if (_supply) _supply->CancelSupply();
}

EntityAIFull *EntityAI::GetSupplying() const
{
  return _supply ? _supply->GetSupplying() : NULL;
}

void EntityAI::UpdateStop()
{
  // if vehicle can be moving, stop waiting
  if( CanCancelStop() )
  {
    AIBrain *brain = PilotUnit();
    if( brain )
    {
      if( brain && (brain->GetState()==AIUnit::Stopping || brain->GetState()==AIUnit::Stopped) )
      {
        brain->SetState(AIUnit::Wait);  // valid pass Stopped -> Wait
      }
    }
  }
}

const SupplyUnitList &EntityAI::GetSupplyUnits() const
{
  return _supply->GetSupplyUnits();
}
SupplyUnitList &EntityAI::GetSupplyUnits()
{
  return _supply->GetSupplyUnits();
}
