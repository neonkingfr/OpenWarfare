#define ENCODE_CHAR(x,i,c) (char)((c-(i))^x)
#define DECODE_CHAR(x,i,d) (char)((d^x)+(i))

#ifdef _MSC_VER

// TODO: try to share more code between GCC / MSC
#define VA_TOKENIZE_(x) x // workaround for VS2005..VS2010 variadic template bug (https://connect.microsoft.com/VisualStudio/feedback/details/380090/variadic-macro-replacement)
#define VA_TOKENIZE(...) VA_TOKENIZE_((__VA_ARGS__))

#define ENCODE_STR_1(x,i,c) ENCODE_CHAR(x,i,c)
#define ENCODE_STR_2(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_1 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_3(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_2 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_4(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_3 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_5(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_4 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_6(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_5 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_7(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_6 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_8(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_7 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_9(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_8 VA_TOKENIZE(x,i+1,__VA_ARGS__)

#define ENCODE_STR_10(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_9 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_11(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_10 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_12(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_11 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_13(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_12 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_14(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_13 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_15(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_14 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_16(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_15 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_17(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_16 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_18(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_17 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_19(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_18 VA_TOKENIZE(x,i+1,__VA_ARGS__)

#define ENCODE_STR_20(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_19 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_21(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_20 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_22(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_21 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_23(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_22 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_24(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_23 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_25(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_24 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_26(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_25 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_27(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_26 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_28(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_27 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_29(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_28 VA_TOKENIZE(x,i+1,__VA_ARGS__)

#define ENCODE_STR_30(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_29 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_31(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_30 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_32(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_31 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_33(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_32 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_34(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_33 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_35(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_34 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_36(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_35 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_37(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_36 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_38(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_37 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_39(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_38 VA_TOKENIZE(x,i+1,__VA_ARGS__)
                                                                
#define ENCODE_STR_40(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_39 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_41(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_40 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_42(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_41 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_43(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_42 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_44(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_43 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_45(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_44 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_46(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_45 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_47(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_46 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_48(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_47 VA_TOKENIZE(x,i+1,__VA_ARGS__)
#define ENCODE_STR_49(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_48 VA_TOKENIZE(x,i+1,__VA_ARGS__)

#define PP_NARG(...) PP_NARG_ VA_TOKENIZE(__VA_ARGS__,PP_RSEQ_N()) 
#define PP_NARG_(...) PP_ARG_N VA_TOKENIZE(__VA_ARGS__) 

#else

#define ENCODE_STR_1(x,i,c) ENCODE_CHAR(x,i,c)
#define ENCODE_STR_2(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_1(x,i+1,__VA_ARGS__)
#define ENCODE_STR_3(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_2(x,i+1,__VA_ARGS__)
#define ENCODE_STR_4(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_3(x,i+1,__VA_ARGS__)
#define ENCODE_STR_5(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_4(x,i+1,__VA_ARGS__)
#define ENCODE_STR_6(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_5(x,i+1,__VA_ARGS__)
#define ENCODE_STR_7(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_6(x,i+1,__VA_ARGS__)
#define ENCODE_STR_8(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_7(x,i+1,__VA_ARGS__)
#define ENCODE_STR_9(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_8(x,i+1,__VA_ARGS__)

#define ENCODE_STR_10(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_9(x,i+1,__VA_ARGS__)
#define ENCODE_STR_11(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_10(x,i+1,__VA_ARGS__)
#define ENCODE_STR_12(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_11(x,i+1,__VA_ARGS__)
#define ENCODE_STR_13(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_12(x,i+1,__VA_ARGS__)
#define ENCODE_STR_14(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_13(x,i+1,__VA_ARGS__)
#define ENCODE_STR_15(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_14(x,i+1,__VA_ARGS__)
#define ENCODE_STR_16(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_15(x,i+1,__VA_ARGS__)
#define ENCODE_STR_17(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_16(x,i+1,__VA_ARGS__)
#define ENCODE_STR_18(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_17(x,i+1,__VA_ARGS__)
#define ENCODE_STR_19(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_18(x,i+1,__VA_ARGS__)

#define ENCODE_STR_20(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_19(x,i+1,__VA_ARGS__)
#define ENCODE_STR_21(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_20(x,i+1,__VA_ARGS__)
#define ENCODE_STR_22(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_21(x,i+1,__VA_ARGS__)
#define ENCODE_STR_23(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_22(x,i+1,__VA_ARGS__)
#define ENCODE_STR_24(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_23(x,i+1,__VA_ARGS__)
#define ENCODE_STR_25(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_24(x,i+1,__VA_ARGS__)
#define ENCODE_STR_26(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_25(x,i+1,__VA_ARGS__)
#define ENCODE_STR_27(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_26(x,i+1,__VA_ARGS__)
#define ENCODE_STR_28(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_27(x,i+1,__VA_ARGS__)
#define ENCODE_STR_29(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_28(x,i+1,__VA_ARGS__)

#define ENCODE_STR_30(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_29(x,i+1,__VA_ARGS__)
#define ENCODE_STR_31(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_30(x,i+1,__VA_ARGS__)
#define ENCODE_STR_32(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_31(x,i+1,__VA_ARGS__)
#define ENCODE_STR_33(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_32(x,i+1,__VA_ARGS__)
#define ENCODE_STR_34(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_33(x,i+1,__VA_ARGS__)
#define ENCODE_STR_35(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_34(x,i+1,__VA_ARGS__)
#define ENCODE_STR_36(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_35(x,i+1,__VA_ARGS__)
#define ENCODE_STR_37(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_36(x,i+1,__VA_ARGS__)
#define ENCODE_STR_38(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_37(x,i+1,__VA_ARGS__)
#define ENCODE_STR_39(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_38(x,i+1,__VA_ARGS__)
                                                                
#define ENCODE_STR_40(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_39(x,i+1,__VA_ARGS__)
#define ENCODE_STR_41(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_40(x,i+1,__VA_ARGS__)
#define ENCODE_STR_42(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_41(x,i+1,__VA_ARGS__)
#define ENCODE_STR_43(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_42(x,i+1,__VA_ARGS__)
#define ENCODE_STR_44(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_43(x,i+1,__VA_ARGS__)
#define ENCODE_STR_45(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_44(x,i+1,__VA_ARGS__)
#define ENCODE_STR_46(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_45(x,i+1,__VA_ARGS__)
#define ENCODE_STR_47(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_46(x,i+1,__VA_ARGS__)
#define ENCODE_STR_48(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_47(x,i+1,__VA_ARGS__)
#define ENCODE_STR_49(x,i,c,...) ENCODE_CHAR(x,i,c),ENCODE_STR_48(x,i+1,__VA_ARGS__)

#define PP_NARG(...) PP_NARG_(__VA_ARGS__,PP_RSEQ_N())
#define PP_NARG_(...) PP_ARG_N(__VA_ARGS__) 

#endif

#define CAT_STR_DO(a,b) a ## b
#define CAT_STR_(a,b) CAT_STR_DO(a,b)
#define CAT_STR(a,b) CAT_STR_(a,b)

/* http://groups.google.com/group/comp.std.c/browse_thread/thread/77ee8c8f92e4a3fb/346fc464319b1ee5?pli=1 */
/* The PP_NARG macro returns the number of arguments that have been passed to it. */ 
#define PP_ARG_N( _1, _2, _3, _4, _5, _6, _7, _8, _9,_10, \
         _11,_12,_13,_14,_15,_16,_17,_18,_19,_20, \
         _21,_22,_23,_24,_25,_26,_27,_28,_29,_30, \
         _31,_32,_33,_34,_35,_36,_37,_38,_39,_40, \
         _41,_42,_43,_44,_45,_46,_47,_48,_49,_50, \
         _51,_52,_53,_54,_55,_56,_57,_58,_59,_60, \
         _61,_62,_63,N,...) N 
#define PP_RSEQ_N() \
         63,62,61,60,                   \
         59,58,57,56,55,54,53,52,51,50, \
         49,48,47,46,45,44,43,42,41,40, \
         39,38,37,36,35,34,33,32,31,30, \
         29,28,27,26,25,24,23,22,21,20, \
         19,18,17,16,15,14,13,12,11,10, \
         9,8,7,6,5,4,3,2,1,0

/// note: following macro does not work as expected. resulting tokens are ENCODE_STR_PP_ARG_N (...) instead of expected ENCODE_STR_xxx(...)
#define ENCODE_STR_LEN(x,...) CAT_STR(ENCODE_STR_,PP_NARG(__VA_ARGS__)) (x,0,__VA_ARGS__)
