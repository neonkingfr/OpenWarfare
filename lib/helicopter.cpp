// Poseidon - vehicle class
// (C) 1997, Suma
#include "wpch.hpp"

#include "helicopter.hpp"
#include "AI/ai.hpp"
#include "shots.hpp"
#include "scene.hpp"
#include "world.hpp"
#include "landscape.hpp"
#include "keyInput.hpp"
#include "lights.hpp"
#include "stringtableExt.hpp"
#include "paramArchiveExt.hpp"
#include "txtPreload.hpp"
#include "diagModes.hpp"
#include "dikCodes.h"

#include "tlVertex.hpp"

#include "UI/uiActions.hpp"

#include <El/Common/randomGen.hpp>
#include <El/Common/perfProf.hpp>

#include "Network/network.hpp"

#include "engine.hpp"
#include "Shape/specLods.hpp"

#include <El/Enum/enumNames.hpp>

#include "mbcs.hpp"
#include "progress.hpp"

#if _ENABLE_CHEATS
  #define ARROWS 1
#else
  #define ARROWS 0
#endif

#if _PROFILE // _ENABLE_CHEATS
#pragma optimize("",off)
#endif

static float HeliSimStepInactive = 0.100f;
static float HeliSimStepInactiveAI = 0.100f;

static float HeliSimStepActive = 0.050f;
static float HeliSimStepActiveAI = 0.050f;

static float HeliSimStepPriority = 0.025f;
static float HeliSimStepPriorityAI = 0.050f;

DEFINE_CASTING(Helicopter)
Helicopter::Helicopter( const EntityAIType *name, Person *pilot, bool fullCreate)
:base(name,pilot, fullCreate),

_rotorSpeedWanted(0), // turning motor on/off
_mainRotorWanted(0),
_backRotor(0),_backRotorWanted(0),

_pilotGear(true),

_cyclicAsideWanted(0),
_cyclicForwardWanted(0),
_rotorDiveWanted(0),

_lastAngVelocity(VZero),
_turbulence(VZero),
_lastTurbulenceTime(Glob.time),

_rndFrequency(1-GRandGen.RandomValue()*0.05), // do not use same sound frequency
_missileLRToggle(false),_rocketLRToggle(false),
_gunPosIndexToogle(0)
{
  if (pilot && QIsManual(pilot->Brain()))
    EngineOn();
  SetSimulationPrecision(HeliSimStepActive,1.0/10);
  RandomizeSimulationTime();
  SetTimeOffset(floatMax(floatMax(HeliSimStepActive, HeliSimStepInactive), HeliSimStepPriority));

  _dustEffect.Init(Pars >> Type()->_dustEffect, this, EVarSet_Effect);
  _waterEffect.Init(Pars >> Type()->_waterEffect, this, EVarSet_Effect);
  _damageEffect.Init(Pars >> Type()->_damageEffect, this, EVarSet_Damage);
  // by default we are suspended - MakeAirborne or impulse will wake up us as needed
  _isStopped = true;
  _cmIndexToggle = 0;
}

HelicopterVisualState::HelicopterVisualState(HelicopterType const& type)
:base(type),
_cyclicAside(0),
_cyclicForward(0),
_rotorPosition(0),
_rotorPositionDelta(0),
_backRotorPosition(0),
_rotorDive(0),
_gearsUp(0),
_rotorSpeed(0),
_mainRotor(0)
{
}


void HelicopterVisualState::Interpolate(Object *obj, ObjectVisualState const& t1state, float t, ObjectVisualState& interpolatedResult) const
{
  base::Interpolate(obj, t1state, t, interpolatedResult);

  HelicopterVisualState const& s = static_cast<HelicopterVisualState const&>(t1state);
  HelicopterVisualState& res = static_cast<HelicopterVisualState&>(interpolatedResult);

  #define INTERPOLATE(x) res.x = x + t*(s.x - x)

  INTERPOLATE(_cyclicAside);  // -1..1 (actually -cyclicAsideMaxVal..-cyclicAsideMaxVal, but it's 1)
  INTERPOLATE(_cyclicForward);  // -1..1
  INTERPOLATE(_rotorDive);  // -1..1
  INTERPOLATE(_gearsUp);  // -1..1
  INTERPOLATE(_backRotorPosition);  // continuous (maybe should be mod k*pi)
  res._rotorPosition = fastFmod(_rotorPosition + t * s._rotorPositionDelta, 60*H_PI);  // mod 60pi: assume that `t1state` was created by Simulate() from `*this`
  INTERPOLATE(_rotorSpeed);  // 0..1
  INTERPOLATE(_mainRotor);  // ?

  #undef INTERPOLATE
}


Helicopter::~Helicopter()
{
}

HelicopterType::HelicopterType(ParamEntryPar param)
:base(param)
{
  _pilotPos=VZero;  

  _hullHit = -1;
  _engineHit = -1;
  _rotorHHit = -1;
  _rotorVHit = -1;
  _avionicsHit = -1;
  _missiles = -1;
  _glassRHit = -1;
  _glassLHit = -1;
}

HelicopterType::~HelicopterType()
{
}

void HelicopterType::Load(ParamEntryPar par, const char *shape)
{
  base::Load(par,shape);

  _enableSweep = par>>"enableSweep";
  _gearRetracting = par>>"gearRetracting";

  float t = par >> "gearDownTime";
  _gearDownSpeed = t > 0 ? 1.0f / t : 0;
  t = par >> "gearUpTime";
  _gearUpSpeed = t > 0 ? 1.0f / t : 0;

  _mainRotorSpeed = par>>"mainRotorSpeed";
  _backRotorSpeed = par>>"backRotorSpeed";

  _altFullForce = par.ReadValue("altFullForce",1000);
  _altNoForce = par.ReadValue("altNoForce",3000);

  _minMainRotorDive = HDegree(par>>"minMainRotorDive");
  _maxMainRotorDive = HDegree(par>>"maxMainRotorDive");
  _neutralMainRotorDive = HDegree(par>>"neutralMainRotorDive");


  _minBackRotorDive = HDegree(par>>"minBackRotorDive");
  _maxBackRotorDive = HDegree(par>>"maxBackRotorDive");
  _neutralBackRotorDive = HDegree(par>>"neutralBackRotorDive");

  _dustEffect =  par >> "dustEffect";
  _waterEffect =  par >> "waterEffect";
  _damageEffect = par >> "damageEffect";

  // load Envelope from config
  ParamEntryVal envelope = par>>"envelope";
  _envelope.Realloc(envelope.GetSize());
  _envelope.Resize(envelope.GetSize());
  for (int i=0; i<_envelope.Size(); i++)
    _envelope[i] = envelope[i];

  SetHardwareAnimation(par);
}

#include <Es/Common/delegate.hpp>


AnimationSource *HelicopterType::CreateAnimationSource(const AnimationType *type, RStringB source)
{
  static const struct AnimSourceDesc
  {
    const char *name;
    float (Helicopter::*func)(ObjectVisualState const&) const;
  } desc[]=
  {
    {"rotorH",&Helicopter::GetCtrlRotorH},
    {"rotorV",&Helicopter::GetCtrlRotorV},
    {"rotorHDive",&Helicopter::GetCtrlRotorHDive},
    {"rotorVDive",&Helicopter::GetCtrlRotorVDive},
    {"gear",&Helicopter::GetCtrlGear},
    {"cyclicForward",&Helicopter::GetCtrlCyclicFwd},
    {"cyclicAside",&Helicopter::GetCtrlCyclicAside},
  };
  for (int i=0; i<sizeof(desc)/sizeof(desc[0]); i++)
  {
    if (!strcmpi(source,desc[i].name))
      return _animSources.CreateAnimationSource(desc[i].func);
  }
  return base::CreateAnimationSource(type,source);
}

//-----------------------------------------------------------------------------
void HelicopterType::InitDummyPosAndDir()
{
  _gunPosArray.Resize(1);
  _gunDirArray.Resize(1);
  
  _gunPosArray[0] = VZero;
  _gunDirArray[0] = VForward;
}

//-----------------------------------------------------------------------------
Vector3Val HelicopterType::GetGunPos(int index) const
{
  bool checkRange = index<_gunPosArray.Size() && index>=0;
  DoAssert(checkRange);  

  return checkRange? _gunPosArray[index] : VZero;
}

//-----------------------------------------------------------------------------
Vector3Val HelicopterType::GetGunDir(int index) const
{
  bool checkRange = index<_gunDirArray.Size() && index>=0;
  DoAssert(checkRange);  

  return checkRange? _gunDirArray[index] : VForward;
}

//-----------------------------------------------------------------------------
bool HelicopterType::ComputePosAndDirFromBegEnd
                        (const RString &gunBeg,const RString &gunEnd, int index)
{
  bool checkPosSize = index<_gunPosArray.Size() && index>=0;
  bool checkDirSize = index<_gunDirArray.Size() && index>=0;
  DoAssert(checkPosSize);
  DoAssert(checkDirSize);
  DoAssert (_shape);
  
  if (!checkPosSize || !checkDirSize)
    return false;
    
  if (_shape &&
      _shape->MemoryPointExists(gunBeg) &&
      _shape->MemoryPointExists(gunEnd))
  {
    Vector3Val beg  = _shape->MemoryPoint(gunBeg);
    Vector3Val end  = _shape->MemoryPoint(gunEnd);
    Vector3 pos = (beg+end)*0.5f;
    _gunPosArray[index] = pos;
    Vector3 dir = beg-end;
    dir.Normalize();
    if (dir == VZero)
      dir = VForward;
    _gunDirArray[index] = dir;  
  }
  else
  {
    _gunPosArray[index] = VZero;
    _gunDirArray[index] = VForward;
    
    return false;
  }

  return true;    
}                                        

//-----------------------------------------------------------------------------
void HelicopterType::InitTurretsPosAndDir()
{
  const ParamEntry &par=*_par;

  DoAssert (_shape);

  // init pos and dir to default VZero values
  InitDummyPosAndDir();

  // first try to load memoryPoint directly
  ParamEntryPtr memoryPointGun = par.FindEntry("memoryPointGun");
  if(memoryPointGun)
  {
    // we have to split single value and array
    if (memoryPointGun->IsArray())
    {
      int n = memoryPointGun->GetSize();
      _gunPosArray.Resize(n);
      _gunDirArray.Resize(n);
      for (int i=0; i<n; i++)
      {
        RString pointPos = memoryPointGun->operator [](i);
        _gunPosArray[i] = _shape ? _shape->MemoryPoint(pointPos) : VZero;
        _gunDirArray[i] = VForward;
      }
    }
    else
    {
      // single value
      _gunPosArray[0] = _shape ? 
        _shape->MemoryPoint(memoryPointGun->operator RString()) :
        VZero;    
    }  
  }
  
  // try to load pos and dir from gunBeg and gunEnd
  ParamEntryPtr gunBegArr = par.FindEntry("gunBeg");
  ParamEntryPtr gunEndArr = par.FindEntry("gunEnd");

  if (!gunBegArr || !gunEndArr)
    return;
  
  // single values only  
  if (!gunBegArr->IsArray() && !gunEndArr->IsArray())  
  {
    ComputePosAndDirFromBegEnd(
      gunBegArr->operator RString(),gunEndArr->operator RString(),0);
  }  
  else if (gunBegArr->GetSize() == gunEndArr->GetSize())
  {
    // both are arrays.. compare pos and dir arrays size
    int n = gunBegArr->GetSize();
    _gunPosArray.Resize(n);
    _gunDirArray.Resize(n);
    
    // iterate array and compute pos&dir
    for (int i=0; i<n; i++)
    {
      RString gunBegStr = gunBegArr->operator [](i);
      RString gunEndStr = gunEndArr->operator [](i);
      ComputePosAndDirFromBegEnd(gunBegStr,gunEndStr,i);      
    }    
  }  
}

void HelicopterType::InitShape()
{
  const ParamEntry &par=*_par;
  base::InitShape();

  DoAssert (_shape);

  _hRotorStill.Init(_shape,(par>>"selectionHRotorStill").operator RString(),NULL);
  _hRotorMove.Init(_shape,(par>>"selectionHRotorMove").operator RString(),NULL);
  _vRotorStill.Init(_shape,(par>>"selectionVRotorStill").operator RString(),NULL);
  _vRotorMove.Init(_shape,(par>>"selectionVRotorMove").operator RString(),NULL);
  _missileLPos=_shape->MemoryPoint((par>>"memoryPointLMissile").operator RString());
  _missileRPos=_shape->MemoryPoint((par>>"memoryPointRMissile").operator RString());
  _rocketLPos=_shape->MemoryPoint((par>>"memoryPointLRocket").operator RString());
  _rocketRPos=_shape->MemoryPoint((par>>"memoryPointRRocket").operator RString());
  

  InitTurretsPosAndDir();

  ProgressRefresh();
  
  if(par.FindEntry("memoryPointCM"))
  {
    int n = (par >> "memoryPointCM").GetSize();
    _cmPos.Resize(n);
    for (int i=0; i<n; i++)
    {
      RString cmPos = (par >> "memoryPointCM")[i];
      _cmPos[i] = _shape->MemoryPoint(cmPos);
    }
  }
  if (_cmPos.Size() == 0) _cmPos.Add(Vector3(0,0,0));

  if(par.FindEntry("memoryPointCMDir"))
  {
    int n = (par >> "memoryPointCMDir").GetSize();
    _cmDir.Resize(n);
    for (int i=0; i<n; i++)
    {
      RString cmDir = (par >> "memoryPointCMDir")[i];
      _cmDir[i] = _shape->MemoryPoint(cmDir);
    }
  }
  if (_cmDir.Size() == 0) _cmDir.Add(Vector3(1,0,0));


  ProgressRefresh();
  
  int level=_shape->FindLevel(VIEW_GUNNER);
  if (level >= 0)
    _shape->Level(level)->MakeCockpit();
  ProgressRefresh();

  RString namePilotPos = par >> "memoryPointPilot";

  level=_shape->FindLevel(VIEW_PILOT);
  if( level>=0 )
  {
    _shape->Level(level)->MakeCockpit();
    _pilotPos=_shape->NamedPoint(level,namePilotPos);
  }
  ProgressRefresh();
  
  level=_shape->FindLevel(VIEW_CARGO);
  if( level<0 )
    level=_shape->FindLevel(VIEW_PILOT);
  if( level>=0 )
    _shape->Level(level)->MakeCockpit();
  ProgressRefresh();

  if( _pilotPos.SquareSize()<0.1 )
    _pilotPos=_shape->MemoryPoint(namePilotPos);

  _hullHit = FindHitPoint("HitHull");
  _engineHit = FindHitPoint("HitEngine");
  _avionicsHit = FindHitPoint("HitAvionics");
  _rotorVHit = FindHitPoint("HitVRotor");
  _rotorHHit = FindHitPoint("HitHRotor");
  _missiles = FindHitPoint("HitMissiles");
  _glassRHit = FindHitPoint("HitRGlass");
  _glassLHit = FindHitPoint("HitLGlass");

  _hitZoneTextureUV.Add(7);_hitZoneTextureUV.Add(8);
  _hitZoneTextureUV.Add(11);_hitZoneTextureUV.Add(12);
  _hitZoneTextureUV.Add(10);

  {
    Ref<WoundInfo> damageInfo = new WoundInfo;
    damageInfo->Load(_shape,par>>"dammageHalf");
    _glassDamageHalf.Init(_shape,damageInfo,NULL,NULL);
  }
  {
    Ref<WoundInfo> damageInfo = new WoundInfo;
    damageInfo->Load(_shape,par>>"dammageFull");
    _glassDamageFull.Init(_shape,damageInfo,NULL,NULL);
  }
  ProgressRefresh();
}

void HelicopterType::DeinitShape()
{
  base::DeinitShape();
}

void HelicopterType::InitShapeLevel(int level)
{
  base::InitShapeLevel(level);

  _glassDamageHalf.InitLevel(_shape,level);
	_glassDamageFull.InitLevel(_shape,level);
	
	_hRotorStill.InitLevel(_shape,level);
	_hRotorMove.InitLevel(_shape,level);
	_vRotorStill.InitLevel(_shape,level);
	_vRotorMove.InitLevel(_shape,level);
}

void HelicopterType::DeinitShapeLevel(int level)
{
  _glassDamageHalf.DeinitLevel(_shape,level);
	_glassDamageFull.DeinitLevel(_shape,level);

	_hRotorStill.DeinitLevel(_shape,level);
	_hRotorMove.DeinitLevel(_shape,level);
	_vRotorStill.DeinitLevel(_shape,level);
	_vRotorMove.DeinitLevel(_shape,level);

  base::DeinitShapeLevel(level);
}


Object* HelicopterType::CreateObject(bool unused) const
{ 
  return new HelicopterAuto(this, NULL, unused);
}


static Vector3 BodyFriction(Vector3Val speed, float rotorEff, bool large)
{
  Vector3 friction;
  friction.Init();
  float stabXY=rotorEff*0.6f+0.4f;
  static float coefX2 = 3.0f;
  static float coefX1 = 50.0f;
  static float coefX0 = 2.0f;
  friction[0] = speed[0]*fabs(speed[0])*coefX2+speed[0]*coefX1+fSign(speed[0])*coefX0;
  if (!large)
  {
    static float coefZ2 = 2.05f;
    static float coefZ1 = 50.0f;
    static float coefZ0 = 2.0f;

    static float coefY2 = 3.00f;
    static float coefY1 = 500.0f;
    static float coefY0 = 2.0f;
    friction[1] = speed[1]*fabs(speed[1])*coefY2+speed[1]*coefY1+fSign(speed[1])*coefY0;
    friction[2] = speed[2]*fabs(speed[2])*coefZ2+speed[2]*coefZ1+fSign(speed[2])*coefZ0;
  }
  else
  {
    friction[1] = speed[1]*fabs(speed[1])*4+speed[1]*500+fSign(speed[1])*15;
    friction[2] = speed[2]*fabs(speed[2])*1 + speed[2]*100+fSign(speed[2])*6 + speed[2]*speed[2]*speed[2]*0.05f;
  }
  friction[0] *= stabXY;
  friction[1] *= stabXY;
  return friction;
}

/// limit rotor forces in high altitude
static float AltLimit(float alt, float altFullForce, float altNoForce)
{
  if (alt>altFullForce)
  {
    if (alt>altNoForce)
      return 0;
    else
      return 1-(alt-altFullForce)*(1/(altNoForce-altFullForce));
  }
  return 1;
}

float HelicopterType::GetStopDistance() const
{
  return base::GetStopDistance();
}


Vector3 HelicopterType::RotorUpForce(Vector3Val speed, float coef, float alt, float groundEff, bool diag) const
{
  Vector3 force(VZero);
  float spd1=speed[1]+3-18*coef*(1+groundEff);
  saturateMax(spd1,-5);
  // rotor lift envelope
  
  float speedRel = speed.SizeXZ()/(GetMaxSpeedMs()*1.4f);

  const int maxI=_envelope.Size()-1;
  float fSpeed=maxI*speedRel;

  // 
  float liftFactor = 0;
  int iLow=toIntFloor(fSpeed);
  float frac=fSpeed-iLow;
  if( iLow>=maxI )
    liftFactor = _envelope[maxI-1];
  else if( iLow<0 )
    liftFactor = 0; // no up force
  else
  {
    float fLow=_envelope[iLow];
    float fHigh=_envelope[iLow+1];
    liftFactor = fLow+(fHigh-fLow)*frac;
  }
  
  static float liftAdjustMove = 4000.0f;
  static float liftAdjustStat1 = 400.0f;
  static float liftAdjustStat2 = 6000.0f;
  force[1] += liftAdjustMove*liftFactor;

  force[1] -= spd1*fabs(spd1)*liftAdjustStat1+spd1*liftAdjustStat2;
  // avoid too strong negative force
  saturateMax(force[1],-10000);
  #if _ENABLE_CHEATS
    if (diag)
    {
      DIAG_MESSAGE(
        500,"Speed %.1f (rel %.3f), Lift factor %.2f, rotor %.2f, force %5g, ge %.2f",
        speed.SizeXZ()*3.6f, fSpeed, liftFactor, coef, force[1], groundEff
      );
    }
  #endif
  
  /*
  float fwdCoef=speed[2]*(1.0/20);
  saturate(fwdCoef,-1,1);
  force[2]+=force[1]*0.1*fwdCoef;
  */
  return force*AltLimit(alt,_altFullForce,_altNoForce);
}

#define FAST_COEF (1.0f/25) // use fast/slow simulation mode

float Helicopter::GetBackRotorMalfunction() const
{
  // Square(Square(...)) is here to make sure small damage causes little trouble
  // maps 0.5 to 0.0625
  return Square(Square(floatMin(1,1.3f*GetHitCont(Type()->_rotorVHit))));
}

float Helicopter::FuelLeakRate() const
{
#if _VBS3 //only leak fuel if Hull is severely damaged
  if(GetHitCont(Type()->_hullHit) < 0.7) return 0.0f;
#endif
  // typical heli fuel is about 1000 l
  // we want this to last about 30 sec with 0.8 damage
  const float factor = 1000.0f/30/0.64f;
  return Square(GetHitCont(Type()->_hullHit))*factor;
}

/// assume typical heli has 1000 l, and is able to fly about 2-3 hours
const float HeliFuelConsumption = 0.1f;

float Helicopter::FuelTimeLeft() const
{
  float fuelConsumeRate = FuelLeakRate()+HeliFuelConsumption;
  return FutureVisualState()._fuel/fuelConsumeRate; 
}

/// acquire current bank based on transformation matrix
static inline float BankFromOrientation(Matrix3Par orient)
{
  float invDive = orient.Direction().InvSizeXZ();
  float asideUp = orient.DirectionAside().Y();
  // if bottom-up, return 1 (nonsense)
  if (invDive<=0)
    return fSign(asideUp);
  return asideUp*invDive;
}

static inline float TailRotateFactor(Vector3Par speed)
{
  return Square(speed[2])*Square(FAST_COEF)*0.3f+fabs(speed[2])*FAST_COEF*0.7f;
}

/// speed at which auto-rotation starts to be seen
const float MinAutoRotateSpeed = 2.0f;
/// speed at which auto-rotation is at max
const float MaxAutoRotateSpeed = 10.0f;

/*
Auto-rotation is expected to be about 7.6 m/s - 1500 ft/min - http://helicopterflight.net/autorotation.htm:
 
while in autorotation, your sink rate will vary between 1,300 and 1,800 feet-per-minute.
Most commonly between 1,500 and 1,800 feet-per-minute.

See also http://www.cybercom.net/~copters/pilot/autorotation.html
*/

void Helicopter::Simulate(float deltaT, SimulationImportance prec)
{
  VisualState &vs = FutureVisualState();

  _isDead = IsDamageDestroyed();

  if( vs._rotorSpeed>0 )
    ConsumeFuel(deltaT*HeliFuelConsumption);

  ConsumeFuel(deltaT*FuelLeakRate());
  
  //Vector3Val position=Position();

  // calculate all forces, frictions and torques
  Vector3Val speed=vs.ModelSpeed();
  Vector3 force(VZero),friction(VZero);
  Vector3 torque(VZero),torqueFriction(VZero);
  
  // world space center of mass
  Vector3 wCenter(VFastTransform,vs.ModelToWorld(),GetCenterOfMass());

#if _EXT_CTRL //ext. Controller like HLA, AAR
  if(IsExternalControlled())
{
    bool engineOn = speed.Size() > 0.1;
     if(engineOn)
     {
       if(vs._rotorSpeed < 0.9)
       {
        vs._rotorSpeed += 0.3f * deltaT;
        saturateMin(vs._rotorSpeed, 1);
       }
     }else
       if(vs._rotorSpeed > 0)
       {
         vs._rotorSpeed -= 0.1f * deltaT;
         saturateMax(vs._rotorSpeed, 0);
       }
  }
#endif
  // we consider following forces
  // gravity
  // main rotor (in direction of rotor axis, changed by cyclic, controlled by thrust)
  // back rotor (in direction of rotor axis - controlled together with cyclic)
  // body aerodynamics (air friction)
  // main rotor aerodynamics (air friction and "up" force)
  // body and rotor torque friction (air friction)

  Vector3 pForce(VZero); // partial force
  Vector3 pCenter(VZero); // partial force application point
  
  float delta;

  if( _isDead )
  {
    if( _landContact || _waterContact || _objectContact )
      StopRotor();
  }
  if( vs._fuel<=0 )
  {
    // warn about engine malfunction
    if (_rotorSpeedWanted>0 && !IsDamageDestroyed())
    {
      _showDmg = true;
      _showDmgValid = Glob.time + 5.0f;
    }
    StopRotor();
  }

  vs._rotorPositionDelta = vs._rotorSpeed*deltaT*20;

  float backWorking = 1-GetBackRotorMalfunction();

  vs._backRotorPosition += vs._rotorSpeed*backWorking*deltaT*20;
  // keep number small, is equivalent mod 2 (see usage of _rotorPosition)
  vs._rotorPosition=fastFmod(vs._rotorPosition + vs._rotorPositionDelta, 4*3*5*H_PI);

  bool doServo=false;

  // change gears
  float gearWanted = 1-_pilotGear;
  //if( _gearDamage ) gearWanted=-0.66f;
  if (!Type()->_gearRetracting)
    gearWanted = 0;
  float oldGears = vs._gearsUp;
  delta = gearWanted - oldGears;
  if( fabs(delta)>0.01 )
    doServo=true;

  Limit(delta,-Type()->_gearDownSpeed * deltaT,+Type()->_gearUpSpeed * deltaT);
  vs._gearsUp+=delta; 
  Limit(vs._gearsUp,-1,1);

  const float tholdUp = 0.95f;
  const float tholdDown = 0.05f;
  float gears = vs._gearsUp;
  if (gears >= tholdUp && oldGears < tholdUp) OnEvent(EEGear,false);
  if (gears <= tholdDown && oldGears > tholdDown) OnEvent(EEGear,true);

  // wake up the simulation as needed
  if (vs._rotorSpeed>0 || _rotorSpeedWanted>0.1f || vs._speed.SquareSize() > 0.1f || _angVelocity.SquareSize() > 0.1f)
    OnMoved();
  if( _impulseForce.SquareSize() + _impulseTorque.SquareSize()>0 )
    OnMoved();

  if (GetStopped())
  {
    // reset impulse - avoid cumulation
    _impulseForce = VZero;
    _impulseTorque = VZero;
    vs._modelSpeed = VZero;
    vs._speed = VZero;
    _angVelocity = VZero;
    _angMomentum = VZero;
  }
  
  if( !GetStopped() && !CheckPredictionFrozen() && !IsAttached())
  {
    saturateMin(_rotorSpeedWanted,1-GetHit(Type()->_engineHit));    

    if (_isDead)
      vs._rotorSpeed -= 0.2f * deltaT;
    else
    {
      // main engine
      delta=_rotorSpeedWanted-vs._rotorSpeed;
      // if we apply rotor up, rotor will stop somewhat faster
      // rotor has significant inertia and it does not stop fast
      float brakeRotor = floatMax(vs._mainRotor*0.2f,0);
      // auto-rotate - when going down, rotor will move faster
      float goingDown = (-vs.ModelSpeed().Y()-MinAutoRotateSpeed)*1.0f/(MaxAutoRotateSpeed-MinAutoRotateSpeed);
      saturate(goingDown,0,1);
      saturateMax(brakeRotor,-goingDown);
      // when airborne, we can assume the pilot wants to make the rotor moving fast
      if (goingDown>0 && !_landContact && _rotorSpeedWanted<=0.1f)
      {
        // auto-rotation may make rotor even moving faster, depending on how fast it moves now
        float tgtSpeed = floatMin(goingDown,1.0f);
        delta = (tgtSpeed-vs._rotorSpeed)*0.04f*deltaT;
#if 0 //_DEBUG || _PROFILE
        DIAG_MESSAGE(1000,Format("Rotor %.3f->%.3f, going down %.2f, MR %.2f->%.2f",vs._rotorSpeed,tgtSpeed,goingDown,vs._mainRotor,_mainRotorWanted));
#endif
      }
      float maxDown = -0.025f - brakeRotor * 0.37f;
      Limit(delta,maxDown*deltaT,+0.05f*deltaT);      
      vs._rotorSpeed+=delta;
    }

    Limit(vs._rotorSpeed,0,1);

    if (IsLocal() && vs._rotorSpeed>0.1f && (_landContact || _objectContact || _waterContact) && vs.DirectionUp()[1] < -0.7f)
    {
      // landed upside down - we need to explode
      float damage = 10.0f;
      LocalDamageMyself(VZero,damage*deltaT,GetRadius());
      _isDead = true;
    }

    // main rotor thrust
    // change main rotor force
    float mrw = floatMin(_mainRotorWanted,vs._rotorSpeed);
    delta = mrw-vs._mainRotor;
    Limit(delta,-0.25f*deltaT,+0.25f*deltaT);
    vs._mainRotor+=delta;
    Limit(vs._mainRotor,-0.2f,vs._rotorSpeed);

    float brw = _backRotorWanted;
    // change back rotor force
    delta = brw-_backRotor;
    Limit(delta,-10*deltaT,+10*deltaT);
    _backRotor+=delta;
    Limit(_backRotor,-1,+1);
    
    // change cyclic
    Limit(_cyclicAsideWanted,-2,+2);
#define DIAG_HELI_CYCLIC_ASIDE 0
#if DIAG_HELI_CYCLIC_ASIDE
    char diagTxt[1024]; sprintf(diagTxt,"_cyclicAsideWanted = %.2f", _cyclicAsideWanted);
    DIAG_MESSAGE(100, diagTxt);
#endif
    delta=_cyclicAsideWanted-vs._cyclicAside;
    //static float cyclicAsideSpeed = 10;
    static float cyclicAsideSpeed = 4;
    static float cyclicForwardSpeed = 10;
    Limit(delta,-cyclicAsideSpeed*deltaT,cyclicAsideSpeed*deltaT);
    static float cyclicAsideMaxVal = 1.0f; //1.0f //TODO - check it
    vs._cyclicAside+=delta;
    Limit(vs._cyclicAside,-cyclicAsideMaxVal,cyclicAsideMaxVal);

    delta=_cyclicForwardWanted-vs._cyclicForward;
    Limit(delta,-cyclicForwardSpeed*deltaT,cyclicForwardSpeed*deltaT);
    vs._cyclicForward+=delta;
    Limit(vs._cyclicForward,-1,1);

    Limit(_rotorDiveWanted,-1,+1);
    delta=_rotorDiveWanted-vs._rotorDive;
    Limit(delta,-0.15f*deltaT,0.15f*deltaT);
    vs._rotorDive+=delta;
    Limit(vs._rotorDive,-1,1);

    // apply aerodynamics of the main rotor
    float rotorSpeed=vs._rotorSpeed;
    if( _isDead )
      // dirty hack - when heli is destroyed, we want it to fall down
      rotorSpeed *= 0.1;
    if (rotorSpeed>0.01f)
    {
      float massCoef=GetMass()*(1.0f/3000);
      bool diag = CHECK_DIAG(DEPath) && GWorld->CameraOn()==this;

      float rotorDiameter = _shape->BoundingSphere()*1.5f; // assume rotor diameter is approximately the heli radius
      float groundEff = 0;
      if (rotorDiameter>0)
      {
        float height = vs.Position().Y()-GLandscape->SurfaceYAboveWater(vs.Position()[0],vs.Position()[2]);
        float relHeight = height/rotorDiameter;
        // approximation of function from http://www.cybercom.net/~copters/aero/ground_effect.html
        groundEff = Square(floatMinMax(1.2f-relHeight,0,1))*0.25f;
      }

      pForce = (Type()->RotorUpForce(speed,vs._mainRotor*rotorSpeed,vs.Position().Y(),groundEff,diag)*(vs._rotorSpeed*vs._rotorSpeed)) * massCoef;
      // rotate force by main rotor orientation
      if (fabs(vs._rotorDive)>1e-3)
      {
        Matrix3 rotDive(MRotationX,vs._rotorDive);
        pForce = rotDive * pForce;
      }
    }
    else
      pForce=VZero;

    float tailRotateFactor = TailRotateFactor(speed);
    
    //Vector3 sideOffset(VZero);
    pCenter.Init();
    
    // debugging back-rotor causing yaw
    //static bool checkNoTorque = true;
    const bool checkNoTorque = false;

    {// main rotor force - "lift"
      // force applied to the center of mass
      pCenter = VZero;
      
      // instead of modifying the force center, we apply a separate torque instead
      // this way we can control the helicopter even when no or negative rotor force is applied
      
#if ARROWS
        AddForce(vs.DirectionModelToWorld(pCenter)+wCenter, vs.DirectionModelToWorld(pForce)*InvMass(), Color(1,1,0));
#endif
      force+=pForce;
    }

    { // cyclic torque simulation
      // instead of modifying the force center, we apply a separate torque instead
      // this way we can control the helicopter even when no or negative rotor force is applied

      float size = _shape->BoundingSphere();

      static float cyclicTorque = 2.11f;
      pForce = Vector3(0,cyclicTorque*GetMass(),0)*Square(rotorSpeed)*AltLimit(vs.Position().Y(),Type()->_altFullForce,Type()->_altNoForce);

      // force applied to the center of the rotor
      static float cAsideFactor = 0.6f;
      static float cForwardFactor = -1.6f;
      pCenter[0] = vs._cyclicAside*cAsideFactor*size;
      pCenter[2] = vs._cyclicForward*cForwardFactor*size;
      pCenter[1] = 0;
      
#if ARROWS
        AddForce(vs.DirectionModelToWorld(pCenter)+wCenter, vs.DirectionModelToWorld(pForce)*InvMass(), Color(0,1,0));
#endif
      //force+=pForce;
      if (!checkNoTorque) torque+=pCenter.CrossProduct(pForce);
      
#if 0
      // apply a stabilizing force
      // we want rotor center to stay approximately fixed
      float maxY = _shape->MinMax()[1].Y();
      float comY = GetCenterOfMass().Y();
      static float maxComFactor = +0.82f;
      float rotCenterY = (maxY-comY)*maxComFactor;
      // for each torque we need to build a force which will fix the point
      static float cAsideFix = +0.1f;
      static float cForwardFix = +0.1f;

      // with high speeds the cyclic is counteracted by y-force and rotational friction
      // without the correction it was possible to reach very high speed
      pForce[0] = pForce[1]*pCenter[0]*rotCenterY*cAsideFix*floatMax(1-tailRotateFactor,0);
      pForce[1] = 0;
      pForce[2] = pForce[1]*pCenter[2]*rotCenterY*cForwardFix*floatMax(1-tailRotateFactor,0);
      
      #if ARROWS
        AddForce(
          DirectionModelToWorld(pCenter)+wCenter,
          DirectionModelToWorld(pForce)*InvMass(),
          Color(0,1,1)
        );
      #endif
      force+=pForce;
#endif
    }
    
    // rotate helicopter when in free fall mode
    if( !_landContact && vs._rotorSpeed<0.1 )
    {
      if (!checkNoTorque)
        torque+=Vector3(0.5,3.2,1.3).CrossProduct(Vector3(0,GetMass(),0));
    }
    
    float backRadius=_shape->BoundingSphere()*0.95;
    // when moving fast, side bank causes torque
    float bank=BankFromOrientation(vs.Orientation());
    
    // without following line the response to back rotor was too strong in mid-speed flight (80 km/h)
    float sizeFactor=backRadius*(1.0/12);
    
    float backMalfunc = GetBackRotorMalfunction();
    float backMalfuncTorque = 5.0f * backMalfunc *vs._rotorSpeed*floatMax(vs._mainRotor+0.1,0.05);
    // avoid applying back rotor in high speed to avoid instability
    float fastAtten = 1-floatMin(fabs(speed[2])*(1.0f/80),1);

    // bank turn should correspond to asin(bank)
    
    float asinBank = bank + (1.0/6)*bank*bank*bank + (3.0/40)*bank*bank*bank*bank*bank;
    //x+(x^3)/6+3/40*(x^5)
    
//    if (this==GWorld->CameraOn())
//      DIAG_MESSAGE(100,Format("TailFactor %.3f, asinBank %.3f",tailRotateFactor,asinBank));
    static float bankTurnCoef = 0.7f;
    pForce=Vector3(
      (asinBank*bankTurnCoef*tailRotateFactor +
        // when flying fast, back rotor has almost zero influence
        (_backRotor*8*(1-backMalfunc) - backMalfuncTorque*4)*fastAtten)
      *(vs._rotorSpeed*vs._rotorSpeed*GetMass()*sizeFactor),
      0,0
    );

    // back rotor should be causing torque only, no force
    //pCenter=Vector3(0,+backRadius*0.08,-backRadius);
    //static float backROffset = +0.11f;
    static float backROffset = +0.008f;
    pCenter=Vector3(0,+backRadius*backROffset,-backRadius);
    torque+=pCenter.CrossProduct(pForce);
    
#if ARROWS
      AddForce(vs.DirectionModelToWorld(pCenter)+wCenter, vs.DirectionModelToWorld(pForce)*InvMass());
#endif
    
    // tail causes turning the chopper into the direction of the flight
    // the force depends only on model-speed X/Y and on forward speed
    static float tailRotX1 = -0.500f;
    static float tailRotX2 = -0.010f;
    static float tailRotY1 = -0.005f;
    static float tailRotY2 = -0.0003f;
    pForce = Vector3(
      tailRotateFactor*(speed[0]*tailRotX1+speed[0]*fabs(speed[0])*tailRotX2)*GetMass(),
      tailRotateFactor*(speed[1]*tailRotY1+speed[1]*fabs(speed[1])*tailRotY2)*GetMass(),
      0
    );
    pCenter=Vector3(0,0,-backRadius);
    if (!checkNoTorque)
      torque+=pCenter.CrossProduct(pForce);
    
#if ARROWS
      AddForce(vs.DirectionModelToWorld(pCenter)+wCenter, vs.DirectionModelToWorld(pForce)*InvMass());
#endif
    
    // convert forces to world coordinates

    vs.DirectionModelToWorld(torque,torque);
    vs.DirectionModelToWorld(force,force);
    
    // angular velocity causes also some angular friction
    // this should be simulated as torque
    //static float damp = 1.0f;
    //static float dampSpeed  = 2.0f;
    static float damp = 2.5f;
    static float dampSpeed  = 0.5f;
    // we want the torque friction to be simulated by real aerodynamic
    // we should do this by sampling a few points and let them generate torques
    if (!checkNoTorque)
      torqueFriction = _angMomentum * ((0.2f+vs._rotorSpeed)*(damp+dampSpeed*tailRotateFactor));
    
    // calculate new position
    VisualState moveTrans = PredictPos<VisualState>(deltaT);

    ApplyRemoteStateAdjustSpeed(deltaT,moveTrans);

    // model space turbulence calculation

    if( Glob.time>_lastTurbulenceTime+2 )
    {
      _lastTurbulenceTime=Glob.time;
      const float maxXT=1;
      const float maxYT=1;
      const float maxZT=1;
      float tx=(GRandGen.RandomValue()-0.5)*(maxXT*2);
      float ty=(GRandGen.RandomValue()-0.5)*(maxYT*2);
      float tz=(GRandGen.RandomValue()-0.5)*(maxZT*2);
      _turbulence=Vector3(tx,ty,tz);
    }

    // body air friction
    Vector3 wind = GLandscape->GetWind()+_turbulence;
    Vector3 airSpeed = speed + DirectionWorldToModel(wind);
    friction=BodyFriction(airSpeed, vs._rotorSpeed*vs._rotorSpeed, Type()->_maxMainRotorDive>0.01);
    vs.DirectionModelToWorld(friction,friction);

#if ARROWS
      AddForce(wCenter, -friction*InvMass(), Color(0.5,0,0));
#endif
    
    // gravity - no torque
    pForce=Vector3(0,-1,0)*(GetMass()*G_CONST);
    force+=pForce;
#if ARROWS
      AddForce(wCenter,pForce*InvMass());
#endif

    
    // recalculate COM to reflect change of position
    wCenter.SetFastTransform(moveTrans.ModelToWorld(),GetCenterOfMass());
    if( deltaT>0 )
    {
      bool wasLandContact = _landContact;
      _objectContact=false;
      _landContact=false;
      _waterContact=false;

      Vector3 totForce(VZero);

      float crash=0;
      Vector3 crashPos;

      if( prec<=SimulateVisibleFar && IsLocal())
      {
        CollisionBuffer collision;
        GLOB_LAND->ObjectCollision(collision,this,moveTrans);
        #define MAX_IN 0.4
        #define MAX_IN_FORCE 0.1
        #define MAX_IN_FRICTION 0.4

        for( int i=0; i<collision.Size(); i++ )
        {
          // info.pos is relative to object
          CollisionInfo &info=collision[i];
          if (info.object && !info.object->IsPassable())
          {
            info.object->SimulationNeeded(GetVisualStateAge(FutureVisualState()));
            _objectContact=true;
            float cFactor=1;
            if( info.object->GetMass()<60 )
              continue;
            if (dyn_cast<Person, Object>(info.object)) continue; // avoid collision with person
            if( info.object->GetType()==Primary )
              cFactor=1;
            else
            {
              cFactor=info.object->GetMass()*GetInvMass();
              saturate(cFactor,0,5);
            }
            if( cFactor>0.05 )
            {
              Vector3Val pos=info.pos;
              Vector3Val dirOut=info.dirOut;
              // create a force pushing "out" of the collision
              float forceIn=floatMin(info.under,MAX_IN_FORCE);
              Vector3 pForce=dirOut*GetMass()*40*forceIn;
              // apply proportional part of force in place of impact
              pCenter=pos-wCenter;
              totForce+=pForce;
              torque+=pCenter.CrossProduct(pForce);
              
              Vector3Val objSpeed=info.object->ObjectSpeed();
              Vector3 colSpeed=vs._speed-objSpeed;

              // if info.under is bigger than MAX_IN, move out
              if( info.under>MAX_IN )
              {
                Vector3 newPos=moveTrans.Position();
                float moveOut=info.under-MAX_IN;
                if( moveOut>0.1 ) 
                {
                  float newCrash = 0.1 * Type()->GetArmor();
                  if (crash < newCrash)
                  {
                    crash = newCrash;
                    crashPos = pos;
                  }
                }
                newPos+=dirOut*moveOut*0.1;
                moveTrans.SetPosition(newPos);
              }

              float newCrash;
              if (moveTrans.DirectionUp()[1] > 0)
                newCrash = (colSpeed.SquareSize()-25.0f)*2.0f;
              else
                // If helicopter is upside down destroy it more
                newCrash = Type()->GetArmor() * 10.0f;

              if (crash < newCrash)
              {
                crash = newCrash;
                crashPos = pos;
              }
              //saturateMax(crash, (colSpeed.SquareSize()-25)*2);

              const float maxRelSpeed=5;
              if( colSpeed.SquareSize()>Square(maxRelSpeed) )
              {
                // adapt _speed to match criterion
                colSpeed.Normalize();
                colSpeed*=maxRelSpeed;
                // only slow down
                float oldSize = vs._speed.Size();
                vs._speed = colSpeed+objSpeed;
                if (vs._speed.SquareSize()>Square(oldSize))
                  vs._speed = vs._speed.Normalized()*oldSize;
              }


              // second is "land friction" - causing no momentum

              float frictionIn=floatMin(info.under,MAX_IN_FRICTION);
              pForce[0]=fSign(speed[0])*20000;
              pForce[1]=speed[1]*fabs(speed[1])*1000+speed[1]*8000+fSign(speed[1])*10000;
              pForce[2]=speed[2]*fabs(speed[2])*150+speed[2]*250+fSign(speed[2])*2000;

              pForce=vs.DirectionModelToWorld(pForce)*GetMass()*(4.0/10000)*frictionIn;
              //saturateMin(pForce[1],0);
              //torque-=pCenter.CrossProduct(pForce);
#if ARROWS
                AddForce(wCenter+pCenter,-pForce*InvMass());
#endif
              friction+=pForce;
              torqueFriction+=_angMomentum*0.15;
            }
          }
        }
      } // if( object collisions enabled )
      
      GroundCollisionBuffer gCollision;
      bool enableLandcontact = true;
      if (wasLandContact && vs.DirectionUp().Y()<0.985f) // cos 10 deg
        enableLandcontact = false;
      int nContactPoint = GLOB_LAND->GroundCollision(gCollision, this, moveTrans, 0.05f, 0, enableLandcontact);

      if( gCollision.Size()>0 )
      {
        // it is hard to have more than 3 contact points
        saturateMin(nContactPoint,5);
        //saturateMax(nContactPoint,gCollision.Size());

        Vector3 gFriction(VZero);
        float maxUnder=0;
        float maxUnderWater = 0;
        #define MAX_UNDER 0.1f
        #define MAX_UNDER_FORCE 0.1f
        
        const float nPointCoef = 3.0f/nContactPoint;
        for( int i=0; i<gCollision.Size(); i++ )
        {
          // info.pos is world space
          UndergroundInfo &info=gCollision[i];
          if( info.under<0 ) continue;
          // we consider two forces
          //ReportFloat("land",info.under);
          float under;
          if( info.type==GroundWater )
          {
            under=floatMin(info.under,3)*0.001f;
            if( maxUnderWater<info.under ) maxUnderWater=info.under;
            _waterContact=true;
            if( vs._speed.SquareSize()>Square(8) ) 
            {              
              float newCrash = 1 * Type()->GetArmor();
              if (crash < newCrash)
              {
                crash = newCrash;
                crashPos = info.pos;
              }
            }
          }
          else
          {
            _landContact=true;
            // we consider two forces
            //ReportFloat("land",info.under);
            if( maxUnder<info.under )
              maxUnder=info.under;

            under=floatMin(info.under,MAX_UNDER_FORCE);
          }
          // one is ground "pushing" everything out - causing some momentum
          Vector3 dirOut=Vector3(0,info.dZ,1).CrossProduct(Vector3(1,info.dX,0)).Normalized();
          pForce=dirOut*GetMass()*40.0f*nPointCoef*under;
          pCenter=info.pos-wCenter;
          torque+=pCenter.CrossProduct(pForce);
          // to do: analyze ground reaction force
          totForce+=pForce;

#if ARROWS
          AddForce(wCenter+pCenter,pForce*under*InvMass());
#endif
          
          // second is "land friction" - causing momentum
          pForce[0]=speed[0]*5000+fSign(speed[0])*10000;
          pForce[1]=speed[1]*fabs(speed[1])*1000+speed[1]*8000+fSign(speed[1])*10000;
          pForce[2]=speed[2]*fabs(speed[2])*150+speed[2]*250+fSign(speed[2])*5000;
          
          pForce=vs.DirectionModelToWorld(pForce)*GetMass()*nPointCoef*(1.0f/10000);
#if ARROWS
          AddForce(wCenter+pCenter,-pForce*InvMass());
#endif
          friction+=pForce;

          // torque applied if speed is big enough
          if( fabs(speed[0])<1 ) pForce[0]=0;
          if( fabs(speed[1])<1 ) pForce[1]=0;
          if( fabs(speed[2])<1 ) pForce[2]=0;
          torque-=pCenter.CrossProduct(pForce); // sub: it is friction

          torqueFriction+=_angMomentum*info.under*15;
        }
        if (maxUnder > 0 || maxUnderWater>0)
        {
          //torqueFriction=_angMomentum*1.0;   
          float newCrash;
          float dX,dZ;
#if _ENABLE_WALK_ON_GEOMETRY
          GLandscape->RoadSurfaceYAboveWater(vs.Position(),Landscape::FilterIgnoreOne(this), 1, &dX,&dZ);
#else
          GLandscape->RoadSurfaceYAboveWater(vs.Position(),&dX,&dZ);
#endif
          Vector3 normal(-dX,1,-dZ);
          if (vs.DirectionUp()[1] > 0.0f)
          {
            if ( vs.DirectionUp()*normal.Normalized() > 0.866) // (cos 30)
            {
              // we can assume heli is able to slide forward
              // TODO: handle rotating gear?
              Vector3 slide = vs.ModelSpeed();
              // with gears up we take much more damage
              slide[2] *= vs._gearsUp + (1-vs._gearsUp)*0.2f;
              newCrash = (slide.SquareSize()-25.0f);
            }
            else
            {
              newCrash = vs._speed.SquareSize();
            }
          }
          else
          {
            // If helicopter is upside down destroy it more
            newCrash = Type()->GetArmor() * 10.0f;
          }

          if (crash < newCrash)
          {
            crash = newCrash;            
            crashPos = vs.PositionModelToWorld(GetCenterOfMass());
          }

          if( maxUnder>MAX_UNDER )
          {
            // it is necessary to move object immediately
            Vector3 newPos=moveTrans.Position();
            float moveUp=maxUnder-MAX_UNDER;
            newPos[1]+=moveUp;
            moveTrans.SetPosition(newPos);

            if (vs._speed.SquareSize()>Square(10))
            {
              vs._speed.Normalize();
              vs._speed *= 10;
            }
            saturateMax(vs._speed[1],-0.1f);
          }
        }

        const float maxFord=1.1f;
        if( maxUnderWater>maxFord && IsLocal())
        {
          float damage=(maxUnderWater-maxFord)*0.5f;
          saturateMin(damage,0.2f);
          LocalDamageMyself(VZero,damage*deltaT,GetRadius());
        }
        // if too much submerged, we are dead
        float tooMuchY = (_shape->Max().Y()-_shape->Min().Y())*0.5f;
        if (maxUnderWater>tooMuchY && IsLocal())
          LocalDamageMyself(VZero,5.0f*deltaT,GetRadius());
      }
      force+=totForce;
      if (crash>0)
      {
        if( Glob.time>_disableDamageUntil )
        {
          _disableDamageUntil = Glob.time+0.5f;
          // crash boom bang state - impact speed too high
          _doCrash=CrashLand;
          if( _objectContact )
            _doCrash=CrashObject;
          if( _waterContact )
            _doCrash=CrashWater;
          _crashVolume=crash;
          //
          
          // Do crash damage around crashPos
          PositionWorldToModel(crashPos,crashPos);
          CrashDammage(crash, 0.7 * crashPos + 0.3 * GetCenterOfMass(), 2);
          // If chopper is upside down rather explode... 
          if (IsLocal() && (crash * GetType()->GetInvArmor() >5))           
          {
            // set some explosion
            float maxTime = 5-crash * GetType()->GetInvArmor()*0.2f;
            maxTime *=0.2; //make helicopter explode earlier 
            saturate(maxTime,0.5f,3);
            if (_explosionTime>Glob.time+maxTime)
              _explosionTime=Glob.time+GRandGen.Gauss(0,maxTime*0.3f,maxTime);
          }
        }
      }
    }

    bool stopCondition=false;
    if( _landContact && !_waterContact && !_objectContact )
    {
      // apply static friction
      float maxSpeed=Square(0.7f);
      if( !Driver() ) maxSpeed=Square(1.2f);
      if( vs._speed.SquareSize()<maxSpeed && _angVelocity.SquareSize()<maxSpeed*0.3f )
        stopCondition=true;
    }
    if( stopCondition)
      StopDetected();
    else
      OnMoved();

    // apply all forces

    _lastAngVelocity=_angVelocity; // helper for prediction
    ApplyForces(deltaT,force,torque,friction,torqueFriction);

    //LogF("ang vel %.3f,%.3f,%.3f",_angVelocity[0],_angVelocity[1],_angVelocity[2]);
    //LogF("ang mom %.3f,%.3f,%.3f",_angMomentum[0],_angMomentum[1],_angMomentum[2]);

    StabilizeTurrets(vs.Transform().Orientation(), moveTrans.Orientation());

    Move(moveTrans);
    vs.DirectionWorldToModel(vs._modelSpeed, vs._speed);
  }
  if (_isDead && ( _landContact || _objectContact))
  {
    NeverDestroy();
    IExplosion *smoke = GetSmoke();
    if (smoke)
    {
      smoke->SetExplosion(100,100);
      smoke->Explode();
    }
    // note: high speed during contact causes explosion
  }

  if (EnableVisualEffects(prec))
  {
    // effects simulation
    float surfaceY = GLandscape->SurfaceY(vs.Position().X(), vs.Position().Z());
    bool water = surfaceY < GLandscape->GetSeaLevel();
    float height = vs.Position().Y() - (water ? GLandscape->GetSeaLevel() : surfaceY);
    const float maxHeight = 30.0f;
    saturate(height, 0, maxHeight);
    float factor = vs._rotorSpeed * (maxHeight - height) / maxHeight;
    const float invMaxSpeed = 1.0f / (200.0f / 3.6f);
    float alpha = factor - fabs(vs.ModelSpeed().Z()) * invMaxSpeed;
    if (alpha > 0.01)
    {
      float speed = 20.0f * factor + 6.0f;
      float effectValues[] = {speed, alpha, 0, 0, 0};
      if (water)
        _waterEffect.Simulate(deltaT, prec, effectValues, lenof(effectValues));
      else
        _dustEffect.Simulate(deltaT, prec, effectValues, lenof(effectValues));
    }

    float damage = GetTotalDamage();
    if (vs._rotorSpeed>0 && damage >= 0.4)
    {
      float damageValues[] = {damage, vs._rotorSpeed, 0, 0, 0};
      _damageEffect.Simulate(deltaT, prec, damageValues, lenof(damageValues));
    }
  }
  base::Simulate(deltaT,prec);
  SimulatePost(deltaT,prec);
}

bool HelicopterAuto::EngineIsOn() const
{
  return _rotorSpeedWanted>0.5f;
}

bool HelicopterAuto::IRSignatureOn() const
{
  return _rotorSpeedWanted>0.05f || FutureVisualState()._rotorSpeed>0.05f;
}

void HelicopterAuto::EngineOn()
{
  if (FutureVisualState()._fuel>0)
  {
    _rotorSpeedWanted=1;
    base::EngineOn();
  }
}
void HelicopterAuto::EngineOff()
{
  //base::EngineOff();
  //_rotorSpeedWanted=0;
}
void HelicopterAuto::EngineOffAction()
{
  StopRotor();
}

float HelicopterAuto::MakeAirborne()
{
  EngineOn();
  FutureVisualState()._rotorSpeed=_rotorSpeedWanted=1.0;
  FutureVisualState()._mainRotor=_mainRotorWanted=0.7;
  _pilotHeight=_defPilotHeight;
#if _VBS3
  if(!_pilotHeightAGL)
  {
    float surfaceY = GLandscape->SurfaceYAboveWater(Position()[0],Position()[2]);
    _pilotHeight += surfaceY;
  }
#endif
  _landContact=false;
  _objectContact=false;
  FutureVisualState()._gearsUp = 1;
  _pilotGear = false;
  CancelStop();
  return _defPilotHeight;
}

void HelicopterAuto::SetFlyingHeight(float val)
{
  _defPilotHeight = val;
#if _VBS3
  if(Glob.config.IsEnabled(DTSimplifiedHeliModel))
  {
    if(!_pilotHeightAGL)
    {
      _pilotHeight=_defPilotHeight;
      float surfaceY = GLandscape->SurfaceYAboveWater(Position()[0],Position()[2]);
      _pilotHeight += surfaceY;
    }
  }
#endif
}

//returns the relative wanted height from the current height if Contour is off
float HelicopterAuto::GetFlyingHeight()
{
#if _VBS3
  bool enabled = Glob.config.IsEnabled(DTSimplifiedHeliModel);
#else
  bool enabled = true;
#endif
  if (enabled && !_pilotHeightAGL)
    return _pilotHeight - FutureVisualState().Position()[1];
  else
    return _defPilotHeight;
}

float Helicopter::GetGearPos() const
{
  const HelicopterType *type = Type();
  if (!type->_gearRetracting)
    return 0;
  return FutureVisualState()._gearsUp;
}


bool Helicopter::Airborne() const
{
  return !_landContact;
}

Vector3 Helicopter::GetLockPreferDir() const
{
  // for helicopter we want to somewhat prefer targets in front of us
  return FutureVisualState().Direction()*0.5f;
}

bool Helicopter::AllowLockDir(Vector3Par dir) const
{
  // helicopter is able to turn quite fast, esp. when hovering
  return true;
  //return dir*Direction()>0;
}

void Helicopter::StopRotor()
{
  _rotorSpeedWanted=0;
  base::EngineOff();
}

float Helicopter::GetEngineVol(float &freq) const
{
  freq=_rndFrequency*FutureVisualState()._rotorSpeed*(1-FutureVisualState()._mainRotor*0.1);
  return 1;
}

float Helicopter::GetEnvironVol(float &freq) const
{
  freq=1;
  return FutureVisualState()._speed.Size()/Type()->GetMaxSpeedMs();
}

const CursorTextureInfo *Helicopter::GetCursorTexture(Person *person)
{
  return base::GetCursorTexture(person);
}

const CursorTextureInfo *Helicopter::GetCursorAimTexture(Person *person)
{
  // check if target designator is active
  /*
  if (_laserTargetOn)
  {
    return GPreloadedTextures.New(CursorLocked);
  }
  */
  return base::GetCursorAimTexture(person);
}

void Helicopter::Sound(bool inside, float deltaT)
{
  PROFILE_SCOPE_EX(snHel,sound);
  base::Sound(inside,deltaT);
}

void Helicopter::UnloadSound()
{
  base::UnloadSound();
}

bool Helicopter::HasFlares(CameraType camType) const
{
  return camType!=CamInternal && camType!=CamGunner;
}

float Helicopter::GetHitForDisplay(int kind) const
{
  // see InGameUI::DrawTankDirection
  switch (kind)
  {
  case 0: return GetHitCont(Type()->_hullHit);
  case 1: return GetHitCont(Type()->_engineHit);
  case 2:return GetHitCont(Type()->_rotorVHit);
  case 3:return GetHitCont(Type()->_rotorHHit);
  case 4:return GetHitCont(Type()->_avionicsHit);

  default: return 0;
  }
}

Vector3 Helicopter::CameraPointRel(CameraType camType) const
{
  return GetCenterOfMass();
  /*
  // this code binds camera into the rotor mast - the result is pendulum-like impression
  float maxY = _shape->MinMax()[1].Y();
  Vector3 com = GetCenterOfMass();
  static float maxFactor = 0.82f;
  return com + Vector3(0,(maxY-com.Y())*maxFactor,0);
  */
  //return _shape->AimingCenter();
}

Vector3 Helicopter::CameraPosition() const
{
  return RenderVisualState().PositionModelToWorld(CameraPointRel(CamExternal));
}

const float WindEmitRadius = 40;

bool Helicopter::EmittingWind(Vector3Par pos, float radius) const
{
  float dist2 = pos.Distance2(RenderVisualState().Position());
  return dist2<Square(radius+WindEmitRadius) && RenderVisualState()._rotorSpeed>1e-3f;
}

Vector3 Helicopter::WindEmit(Vector3Par pos, Vector3Par wind) const
{
  VisualState const& vs = RenderVisualState();

  // calculate how much can we effect the wind at given point
  // assume the rotor is quite high above the model center
  Vector3 relPos = vs.PositionWorldToModel(pos)-Vector3(0,2,0);

  // wind is not affects on the sides
  if (fabs(relPos.Y())<1e-2)
    return wind;
  
  if (relPos.SquareSize()>Square(WindEmitRadius))
    return wind;
  
  // how much is helicopter above the point
  float above = -relPos.Y()*0.5;
  //float aside = relPos.SizeXZ();
  float dist = relPos.Size();
  
  float vDist = relPos.Y()*0.05f;
  saturate(vDist,-1,+1);
  // effect depends on how much the rotor is rotating
  float strenght = Square(vs._rotorSpeed)*(1-fabs(vDist))*40*(1-dist/WindEmitRadius);
  // it depends also on rotor blades position
  float bladeCoef = vs._mainRotor*5;
  saturate(bladeCoef,0.5,1.5);
  strenght *= bladeCoef;

  // the more outside, the more we will turn the direction outside
  Vector3 outDir = relPos;
  outDir.Normalize();
  
  // reaching end of the influence area, the effect needs to be weaker
  
  // the more we are under, the more the wind is going outside
  float aboveSign = above;
  saturate(aboveSign,0,+1);
  // TODO: check the relation to the surface as well - surfaces acts as a deflector
  return wind + vs.DirectionModelToWorld(outDir)*(strenght*aboveSign);
}


AnimationStyle Helicopter::IsAnimated(int level) const {return AnimatedGeometry;}
bool Helicopter::IsAnimatedShadow(int level) const {return true;}

// TODO: move broken glass to Transport class
inline float Helicopter::GetGlassBroken() const
{
  const HelicopterType *type = Type();
  float glassDamage = GetHitCont(type->_hullHit);
  saturateMax(glassDamage,GetTotalDamage());
  saturateMax(glassDamage,GetHitCont(type->_glassLHit));
  saturateMax(glassDamage,GetHitCont(type->_glassRHit));
  return glassDamage;
}

void Helicopter::DamageAnimation(AnimationContext &animContext, int level, float dist2 )
{
  const HelicopterType *type = Type();
  // scan corresponding wound

  float glassDamage = GetGlassBroken();
  if (glassDamage>=0.6)
    // TODO: convert to 3-step wounds
    type->_glassDamageFull.Apply(animContext, _shape, level, 2, dist2);
  else if (glassDamage>=0.3)
    type->_glassDamageHalf.Apply(animContext, _shape, level, 1, dist2);
}

void Helicopter::DamageDeanimation(int level)
{
}

float HelicopterVisualState::GetRotorH(const HelicopterType* type) const
{
  return fastFmod((_rotorPosition * type->_mainRotorSpeed)/(H_PI*2),1);
}

float HelicopterVisualState::GetGear(const HelicopterType* type) const
{
  if (!type->_gearRetracting)
    return 0;
  return _gearsUp;
}


float HelicopterVisualState::GetRotorV(const HelicopterType* type) const
{
  return fastFmod((_backRotorPosition * type->_backRotorSpeed)/(H_PI*2),1);
}

float HelicopterVisualState::GetRotorHDive(const HelicopterType* type) const
{
  return -_rotorDive - type->_neutralMainRotorDive;
}

float HelicopterVisualState::GetRotorVDive(const HelicopterType* type) const
{
  return -_rotorDive - type->_neutralBackRotorDive;
}

void Helicopter::Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2)
{
  if (animContext.GetVisualState().cast<Helicopter>()._rotorSpeed < 0.6f)
    Type()->_hRotorMove.Hide(animContext, _shape, level);
  else
    Type()->_hRotorStill.Hide(animContext, _shape, level);
  if (animContext.GetVisualState().cast<Helicopter>()._rotorSpeed < 0.6f || GetBackRotorMalfunction()>0.5f)
    Type()->_vRotorMove.Hide(animContext, _shape, level);
  else
    Type()->_vRotorStill.Hide(animContext, _shape, level);
  
  // if drawing shadows, old style animation is required?

  if (_weaponsState.ShowFire())
  {
    Type()->_weapons._animFire.Unhide(animContext, _shape, level);
    Type()->_weapons._animFire.SetPhase(animContext, _shape, level, _weaponsState._mGunFirePhase);
  }
  else
    Type()->_weapons._animFire.Hide(animContext, _shape, level);

  // set gun and turret to correct position
  // calculate animation transformation
  // turret transformation

  DamageAnimation(animContext, level, dist2);

  base::Animate(animContext, level, setEngineStuff, parentObject, dist2);
//  SelectTexture(level,FutureVisualState()._rotorSpeed);
  // TODO: animate min-max box
  // set minmax box and sphere
  ShapeUsed shape = GetShape()->Level(level);
  float factor = 1.1;
  Vector3 min = shape->Min();
  Vector3 max = shape->Max();
  Vector3 bCenter = shape->BSphereCenter();
  float bRadius = shape->BSphereRadius();
  // enlarge twice to be sure soldier will fit it
  min = bCenter+(min-bCenter)*factor;
  max = bCenter+(max-bCenter)*factor;
  bRadius *=factor;
  animContext.SetMinMax(min, max, bCenter, bRadius);
}

void Helicopter::Deanimate(int level, bool setEngineStuff)
{
  DamageDeanimation(level);
  base::Deanimate(level,setEngineStuff);
}

bool Helicopter::IsPossibleToGetIn() const
{
  if (GetHit(Type()->_engineHit)>=0.7f)
    return false;
  return base::IsPossibleToGetIn();
}

bool Helicopter::EjectNeeded() const
{
  // prefer emergency landing over ejecting
  //if (GetHitCont(Type()->_engineHit)>=0.8f) return true;
  if (GetTotalDamage()>0.8)
    return true;
  return false;
}

bool Helicopter::IsWorking() const
{
  // FIX: Enable player get into the helicopter even when out of fuel
  // For AI, IsAbleToMove() is checked

  // if (GetHit(Type()->_engineHit)>=0.7f) return false;
  // if (FuelTimeLeft()<30) return false;
  
  return base::IsWorking();
}

float Helicopter::GetFleeDamage() const
{
  float flee = base::GetFleeDamage();
  // if we are low on fuel, we need to retreat
  saturateMax(flee,InterpolativC(FuelTimeLeft(),120,240,1,0));
  // if the back rotor is too damaged, we need to retreat
  saturateMax(flee,GetBackRotorMalfunction());
  return flee;
}

bool Helicopter::IsAbleToMove() const
{
  if (GetHit(Type()->_engineHit)>=0.7f)
    return false;

  // without working backrotor we cannot fly far
  // TODO: implement escape flying with broken rotor for AI 
  if (GetBackRotorMalfunction()>=0.7f)
    return false;
  // we are nearly out of fuel, we need to eject / get out
  if (FuelTimeLeft()<30)
    return false;
  
  // landed upside down heli is not able to move
  if ((_landContact || _objectContact) && FutureVisualState().DirectionUp()[1] < -0.7)
    return false;
  return base::IsAbleToMove();
}

const char **Helicopter::HUDMode(int &mask) const
{
  mask = 0;
  if (!EngineIsOn())
    return NULL;
  float malfunction = GetHitCont(Type()->_avionicsHit);
  if (malfunction>0)
  {
    // assume 10 phases per second
    int phase = Glob.time.ModMs(10000)/100;
    float random = GRandGen.RandomValue(phase);
    // HUD may malfunction more or less
    if (random<=malfunction) return NULL;
  }
  static const char *conditions[]=
  {
    "on","ils","mgun","missile","aamissile","atmissile","rocket","bomb",NULL
  };
  mask |= 1<<0; // on is always on
  if (_pilotGear) mask |= 1<<1; // ILS is on when gear is down

  // TODO: which turret?
  TurretContext context;
  if (GetPrimaryGunnerTurret(context))
  {
    int weapon = context._weapons->ValidatedCurrentWeapon();
    if (weapon>=0)
    {
      const AmmoType *ammo = context._weapons->GetAmmoType(weapon);
      if (ammo)
      {
        if (ammo->_simulation == AmmoShotBullet || ammo->_simulation == AmmoShotSpread || ammo->_simulation == AmmoShotShell)
        {
          mask |= 1<<2; //mgun
        }
        else
        {
          mask |= 1<<3; //missile // back compatibility
          if (ammo->_simulation == AmmoShotMissile) 
          {
            if  (ammo->maxSpeed<150) //its not nice, but here is no other option, LGBs are also bombs but maneuvrability > 10 and maxControlRange > 10
            {
              mask |= 1<<7; //bomb
      }
            else
            {
              if  (ammo->maxControlRange>10) // guided missile
              {
                if (ammo->irLock)
                {
                  if (ammo->airLock)
                    mask |= 1<<4; //aamissile
                  else
                    mask |= 1<<5; //atmissile
    }
  }
              else
              {
                mask |= 1<<6; //rocket
              }
            }
          }
        }
      }
    }
  }
  return conditions;
}

DEFINE_CASTING(HelicopterAuto)

HelicopterAuto::HelicopterAuto( const EntityAIType *name, Person *pilot, bool fullCreate)
:Helicopter(name,pilot,fullCreate),_pilotHeading(0),_pilotSpeed(VZero),_pilotHeight(2.5),
_pilotHeadingSet(false),_pilotDiveSet(false),_pilotBankSet(false),
_hoveringAutopilot(false),
_dirCompensate(0.5),
_defPilotHeight(50),
_bankWanted(0),_diveWanted(0), _minAttackDive(0),
_forceDive(10),_forceBank(10),

_pilotAvoidHigh(TIME_MIN),
_pilotAvoidHighHeight(0),
_pilotAvoidLow(TIME_MIN),
_pilotAvoidLowHeight(100000),

_pilotDive(0), // dive set by pilot
_pilotHeightHelper(true),
_pilotSpeedHelper(true),
_pilotDirHelper(true),
_pilotVSpeedHelper(false),
_pilotCyclicDirect(false),
_pilotHeightAGL(true),
_state(AutopilotFar),
_analogueThrottleTime(TIME_MIN),
_thrustHelperTime(TIME_MIN),
_lastForward(FLT_MAX),
_lastThrotleDirect(FLT_MAX),
_pressedUp(false),_pressedDown(false),
#if _VBS3_AVRS
_heliVector(VZero),
#endif
_sweepDelay(Glob.time),
_sweepState(SweepDisengage)
{
}

/**
@return above ground level
@param minAboveObstacle accumulate height above "most risky" obstacle
*/
static void MinSafeAltitude(const ObjectVisualState &vs, Vector3Val estPos, Vector3Val oldPos, float surfaceY, float &minAboveObstacle)
{
  // first approximation: objects 40 m tall everywhere
  
  // check the object list
  // if the object is not loaded yet, we do not care about it
  int xMin,xMax,zMin,zMax;
  ObjRadiusRectangle(xMin,xMax,zMin,zMax,estPos,oldPos,0);
  // never fly closer than a certain limit, to stay safe of unexpected objects
  for (int z=zMin; z<=zMax; z++) for (int x=xMin; x<=xMax; x++)
  {
    // TODO: quick reject objects which lie too far from our trajectory
    const ObjectList &list = GLandscape->GetObjects(z, x);
    int n = list.Size();
    for (int i=0; i<n; i++)
    {
      const Object *obj = list[i];
      // dynamic object avoidance not done here
      if (!obj->Static())
        continue;
      // assume the object is more or less erected
      const LODShape *shape = obj->GetShape();
      if (!shape)
        continue;
      const Shape *geom = shape->GeometryLevel();
      if (!geom)
        continue;
      // assume the object is whole above the ground
      float topObjY = obj->FutureVisualState().Position().Y()+geom->Max().Y();
      // TODO: find nearest in 2D sense only
      // measure the object height as viewed from our current position
      // find a nearest point on the oldPos ... estPos line
      Vector3 nearest = NearestPoint(estPos,oldPos,obj->FutureVisualState().Position());
      float dist2 = nearest.DistanceXZ2(obj->FutureVisualState().Position());
      // if the object is far from our trajectory, no need to care about it
      if (dist2>Square(obj->GetCollisionRadius(vs)*1.2f))
        continue;
      // we are interested in the "lowest above"
      // compare against the "nearest point on the trajectory"
      saturateMin(minAboveObstacle,nearest.Y()-topObjY);
    }
  }
}

const float MaxBank = 0.90f;

void HelicopterAuto::Move(Matrix4Par transform)
{
  base::Move(transform);
  Matrix3 modelToWorld(MUpAndDirection,VUp,transform.Direction());
  _worldToRelative = modelToWorld.InverseRotation();
}

void HelicopterAuto::Init( Matrix4Par pos, bool init )
{
  base::Init(pos, init);
  Matrix3 modelToWorld(MUpAndDirection,VUp,pos.Direction());
  _worldToRelative = modelToWorld.InverseRotation();
}

void HelicopterAuto::Simulate(float deltaT, SimulationImportance prec)
{
  // switch to wreck simulation and drawing if needed
  if (!_isWreck)
    _isWreck = GetDestructType() == DestructWreck && _isDead && _landContact;

  VisualState &vs = FutureVisualState();
  if (_isWreck)
  {
    // switch off the engine - very fast
    _rotorSpeedWanted = 0;
    vs._rotorSpeed -= 2.0f * deltaT;
    saturateMax(vs._rotorSpeed,0);
    SimulateWreck(deltaT, prec);
    return;
  }

  PROFILE_SCOPE_EX(simHe,sim);
  //if( !_driver && !_isDead ) StopRotor();

  // get simple approximations of bank and dive
  // we must consider current angular velocity
  float massCoef=GetMass()*(1.0/3000);
  saturate(massCoef,1,3);

  static float minEstM = 0.5f,maxEstM = 0.5f;
  static float minEstT = 1.0f,maxEstT = 1.0f;
  
  float dirEstT = Interpolativ(massCoef,minEstM,maxEstM,minEstT,maxEstT);
  
  const Matrix3 &orientation=vs.Orientation();

  //Vector3Val angAcceleration=(_angVelocity-_lastAngVelocity)/deltaT;
  //Vector3Val avgAngVelocity = _angVelocity+angAcceleration*0.5*dirEstT;
  Vector3Val avgAngVelocity = _angVelocity;
  Matrix3Val derOrientation = avgAngVelocity.Tilda()*orientation;

  Matrix3Val estOrientation=orientation+derOrientation*dirEstT;
  
  Vector3Val estDirection=estOrientation.Direction().Normalized();
  Vector3Val estDirectionUp=estOrientation.DirectionUp().Normalized();
  Vector3Val estDirectionAside=estOrientation.DirectionAside().Normalized();
  
  Matrix3 estOrientationNorm;
  estOrientationNorm.SetDirection(estDirection);
  estOrientationNorm.SetDirectionUp(estDirectionUp);
  estOrientationNorm.SetDirectionAside(estDirectionAside);

  float bank=BankFromOrientation(estOrientationNorm);
  float dive=estDirection.Y();

#if _ENABLE_CHEATS
  if (this==GWorld->CameraOn() && CHECK_DIAG(DEPath))
  {
    const char *state = FindEnumName(_state);
    float lSpeed = 0;
    if (PilotUnit() && PilotUnit()->GetUnit())
    {
      AIUnit *leader = PilotUnit()->GetUnit()->GetSubgroup()->Leader();
      lSpeed = leader->GetVehicle()->FutureVisualState().Speed().SizeXZ();
    }
    DIAG_MESSAGE(500,"PHeight %.1f, PSpeed %.1f, lSpeed %.1f, Bank %.2f->%.2f, Dive %.3f->%.3f, APState %s",
      _pilotHeight,_pilotSpeed[2]*3.6f,lSpeed*3.6,bank,_bankWanted,dive,_diveWanted,state);
  }
#endif
  float avoidGroundMRW=_mainRotorWanted;
  bool driverAlive = _driver && _driver->Brain() && _driver && _driver->Brain()->LSIsAlive();
  bool autorotating = false;
  float belowFlightLevel = 0;
  if (driverAlive)
  {
    if( _pilotHeightHelper)
    {
      Vector3Val speed = vs._speed;
      // autopilot: convert simple _pilot control
      // to advanced simulation model
      
      // use acceleration to estimate change of position
      // estimate vertical acceleration
      //float estY=Position.Y()+_pilotSpeed*EST_DELTA+_acceleration*EST_DELTA*EST_DELTA*0.5;
      // estimate position after 
      const float estT=2;
      const float accPredict=0.2; // estimate acceleration only partially
      Vector3 estPos=vs.Position()+speed*estT+0.5*accPredict*estT*estT*vs._acceleration;
      
      float minEstHeight=estPos.Y();
      float minAboveObstacle = FLT_MAX;
      float estSurfaceY = GLandscape->SurfaceYAboveWaterNoWaves(estPos.X(),estPos.Z());
      // we may need to consider nearby objects as well
      float estHeight=estPos.Y()-estSurfaceY;
      float targetHeight = _pilotHeight;
      if (_pilotHeightAGL)
      {
        // first AGL sample
        minEstHeight = estHeight;

        if( speed.SquareSizeXZ()>Square(5) )
        {
          // predict terrain
          Vector3 oldPos = estPos;
          for( int t=1; t<3; t++ )
          {
            float estT=t*0.8f;
            Vector3 estPos=vs.Position()+speed*estT+0.5f*estT*estT*vs._acceleration;
            float estSurfaceY = GLandscape->SurfaceYAboveWaterNoWaves(estPos.X(),estPos.Z());
            MinSafeAltitude(vs,estPos,oldPos,estSurfaceY,minAboveObstacle);
            oldPos = estPos;
            float estHeight=estPos.Y()-estSurfaceY;
            saturateMin(minEstHeight,estHeight);
          }
        }
        else
          MinSafeAltitude(vs,estPos,estPos,estSurfaceY,minAboveObstacle);

        // we need to keep minEstHeight above zero at all cost
        // otherwise we fly into a crash
        // if we are ordered to fly low, we need to obey, even if we risk a collision?
        const float minHeight=10;
        float flightLevelWarning = floatMax(targetHeight-5.0f,minHeight);
        if( _pilotHeight>=minHeight && (_stopMode==SMNone || _state!=AutopilotReached))
        {
          // always assume obstacles minHeight tall may exist
          saturateMin(minAboveObstacle,estPos.Y()-(estSurfaceY+minHeight));
          // no need to be afraid until we could be too near to the ground
          // slight flying under assigned level should not matter
          const float separationNeeded = GetCollisionRadius(vs)*0.66f;
          if (minAboveObstacle<separationNeeded)
          {
            float avoidGroundCAY=(separationNeeded-minAboveObstacle)/separationNeeded;
            avoidGroundMRW = vs._mainRotor+floatMax(avoidGroundCAY,0);
          }
          saturateMax(targetHeight,estHeight+separationNeeded-minAboveObstacle);
          saturateMax(flightLevelWarning,estHeight+separationNeeded-minAboveObstacle);
          //DIAG_MESSAGE(1000,Format("TgtH %.1f EstH %.1f MinAb %.1f",targetHeight,estHeight,minAboveObstacle));
        }
        belowFlightLevel = flightLevelWarning-estHeight;
      }

      float changeAY = (targetHeight-minEstHeight)*0.1f;
      
      // when flying low, avoid diving too fast
      const float maxSpeed = Type()->GetMaxSpeedMs();
      float minYSpeed = Interpolativ(minEstHeight,maxSpeed*0.2f,maxSpeed*0.4f,-maxSpeed*0.06f,-maxSpeed*0.25f);
      
      float minChangeAY = minYSpeed-speed[1];
      saturateMax(changeAY,minChangeAY);
      
      // it has no sense to change _mainRotorWanted faster than _mainRotor can be changed
      // it would only cause excessive traffic
      _mainRotorWanted=floatMinMax(changeAY,-0.25f*deltaT,+0.25f*deltaT)+vs._mainRotor;
      // if rotor is off, we need to auto-rotate
      if (!QIsManual(PilotUnit()) && !_landContact && !_objectContact && !_waterContact)
      {
        {
          // when flying fast, never let rotor wanted drop too much - we will need it soon
          float fast = (fabs(_pilotSpeed[2])+fabs(vs.ModelSpeed()[2]))*0.5f;
          float minRotor = InterpolativC(fast,0,Type()->GetMaxSpeedMs()*0.7f,-1,0.5);
          saturateMax(_mainRotorWanted,minRotor);
        }

        // AI auto-rotation
        if (_rotorSpeedWanted<0.1f ||
          // autorotate when we need landing and are close enough to the suitable landing position
          GetBackRotorMalfunction()>=0.7f && fabs(vs.Speed().SizeXZ())<Type()->GetMaxSpeedMs()*0.2f &&
          _stopMode!=SMNone && _stopPosition.DistanceXZ2(vs.Position())<Square(50.0f)
        )
        {
          // for optimal auto-rotation we keep steady descend speed
          // if _rotorSpeed goes down, we should dive faster
          float descend = MinAutoRotateSpeed+(MaxAutoRotateSpeed-MinAutoRotateSpeed)*(1-vs._rotorSpeed);

          autorotating = true;

          const float autoRotateLandHeight = 10.0f;
          if (estHeight<autoRotateLandHeight)
          {
            _rotorSpeedWanted = 0;
            // once we are close to ground, we want to slow down the descent
            descend = InterpolativC(estHeight,0,autoRotateLandHeight,0.5,descend);
            _mainRotorWanted = vs._mainRotor + -vs.ModelSpeed().Y()-descend;
            _pilotGear = true;
          }
          else
            _mainRotorWanted = floatMin(vs._mainRotor + -vs.ModelSpeed().Y()-descend,0);
#if 0 // _DEBUG || _PROFILE
          DIAG_MESSAGE(500,Format("AI autorotate descend %.2f, MR %.2f->%.2f",descend,vs._mainRotor,_mainRotorWanted));
#endif
        }
      }
      // we need to be able to pass _mainRotorWanted higher than 1 to other parts of the autopilot
      // we will apply saturation during rotor simulation anyway
      saturate(_mainRotorWanted,-1,2.0f);
    }
    else if (_pilotVSpeedHelper)
    {
      // autopilot: convert simple _pilot control
      // to advanced simulation model
      
      // use acceleration to estimate change of position
      // estimate vertical acceleration
      //float estY=Position.Y()+_pilotSpeed*EST_DELTA+_acceleration*EST_DELTA*EST_DELTA*0.5;
      // estimate position after 
      const float estT=0.5f;
      float estSpeedY=vs._speed.Y()+estT*vs._acceleration.Y();
      float changeAY=(_pilotSpeed[1]-estSpeedY)*0.1f;
      // this may mean descending even faster than _pilotSpeed[1] dictates us
      _mainRotorWanted=changeAY+vs._mainRotor;

      // if helicopter is reversed, we need to apply reversed rotor as well
      // if helicopter is banked, we may need to apply more thrust
      // but this we ignore for now
      // we want to avoid abrupt changes, therefore we apply a smooth transition
      float reversedRotor = InterpolativC(vs.DirectionUp().Y(),-0.2f,-0.0f,-1,+1);
      _mainRotorWanted *= reversedRotor;
      saturate(_mainRotorWanted,-1,1);
    }
  }

  const Vector3 relSpeed=vs.ModelSpeed();
  float zPilotSpeed=_pilotSpeed[2];

  // changeAccel is required for speed helper and dirhelper
  float estAccT=3.0;
  // the speed we want is given in the horizontal plane
  // we want to rotate it azimut-wise only
  // if there is singularity here, we cannot control anything, the helicopter is aiming up or down
  Matrix3 rotSpeed(MUpAndDirection,VUp,vs.Direction());
  Vector3 absSpeedWanted = rotSpeed * _pilotSpeed;

  if (_rotorSpeedWanted<0.1f || autorotating)
  {
    // prevent moving too fast on landing
    const float maxAutorotateSpeedWanted = 6.0f;
    if (absSpeedWanted.SquareSizeXZ()>Square(maxAutorotateSpeedWanted))
    {
      absSpeedWanted = absSpeedWanted*(absSpeedWanted.InvSizeXZ()*maxAutorotateSpeedWanted);
    }
  }

  static float speedF = 1.0f;
  static float accF = 0.2f;
  Vector3 speedDif = DirectionWorldToRelative(absSpeedWanted-vs._speed);
  Vector3 changeAccel=(absSpeedWanted-vs._speed)*(1/estAccT)*speedF-vs._acceleration*accF;
  float bankLimit=0.80f;

  if( _pilotSpeedHelper && driverAlive)
  {
    // if we need to accelerate forward, move cyclic forward
    changeAccel = DirectionWorldToRelative(changeAccel);
    
    // depending on the change desired, we will limit max. angles as well
    static float maxDiveF = 0.08f;
    // note: some dive is also needed to maintain current speed
    //float curSpeedDive = Interpolativ(_pilotSpeed[2],0,Type()->GetMaxSpeedMs(),0,0.1f);
    // max allowable dive at max speed is ~ 0.15
    // max allowable dive at max speed*0.9 is ~ 0.2
    
    static const float speedLint[]={0, 0.33, 0.8,  1.0};
    static const float diveLint []={1, 0.3,  0.19, 0.13};
    COMPILETIME_COMPARE(lenof(speedLint),lenof(diveLint));
    
    float curSpeedMaxDive = Lint(speedLint,diveLint,lenof(speedLint),relSpeed[2]/Type()->GetMaxSpeedMs());
    
    float maxDive = fabs(speedDif[2])*maxDiveF+0.05f; // max. nose up
    float minDive = -curSpeedMaxDive; // max. nose down
    // if we need to apply more rotor power that we have
    // we have to limit cyclic movement
    if( avoidGroundMRW>=1.0f )
    { // limit dive - we need to climb to avoid ground collision
      // if we are moving forward, we may want to climb up using dive
      float avoidW=floatMin(avoidGroundMRW-1,1);
      float speedDL=fabs(relSpeed[2])*0.025f;
      // negative dive is accelerating forward
      if( relSpeed[2]>5 )
        minDive=Lerp(minDive,floatMin(+speedDL,+0.25f),avoidW);
      else if( relSpeed[2]<-5 )
        maxDive=Lerp(maxDive,floatMax(-speedDL,-0.25f),avoidW);
      else
      {
        // low speed climb - disable dive
        minDive = Lerp(minDive,-0.2f,avoidW);
        maxDive = Lerp(maxDive,+0.2f,avoidW);
      }
      bankLimit=1.0f-0.8f*avoidW;
    }

    // if we are under the assigned flight level, we may need to avoid diving more when moving fast
    if (_pilotHeightHelper)
    {
      static float climbByDiveMul = +0.05f;
      static float climbByDiveAdd = -0.10f;
      float flightLevelDive = floatMin(belowFlightLevel*climbByDiveMul+climbByDiveAdd,0.7f);
      
      // limiting depends on speed - when moving slow we need not limit diving that much
      flightLevelDive = Interpolativ(relSpeed.Z(),0,20,0,flightLevelDive);
      
      // once at flight level or only slightly below, apply no limitations
      // if we are not at full rotor power, relax limitation
      float relaxPowerReserve = 1-_mainRotorWanted*1.5f;
      float relaxHighEnough = InterpolativC(belowFlightLevel,0,-2,1,-0.5f);
      
      float minFlightLevelDive = floatMin(-relaxPowerReserve,-relaxHighEnough,flightLevelDive);


      if (minDive<minFlightLevelDive)
        minDive = minFlightLevelDive;
    }

    // when moving slow, dive corresponds to acceleration
    // when moving fast, dive corresponds to speed
    if( fabs(_forceDive)<0.9f )
    {
      _diveWanted=_forceDive;
      if (CHECK_DIAG(DEPath) && GWorld->CameraOn()==this)
        DIAG_MESSAGE(100,"dive forced %.2f (min %.2f, max %.2f, speed %.2f), below %.1f",
          _diveWanted,minDive,maxDive,-curSpeedMaxDive,belowFlightLevel);
      saturate(_diveWanted,minDive,maxDive);
    }
    else
    {
      static float fastDiveFactor = 1.0f;
      float fastDive=fabs(zPilotSpeed)*(fastDiveFactor/Type()->GetMaxSpeedMs());
      //  when moving fast, we want the change to be slower
      Limit(fastDive,0,0.9f);
      _diveWanted = dive + changeAccel[2]*-0.15f*(1-fastDive);

      if (CHECK_DIAG(DEPath) && GWorld->CameraOn()==this)
        DIAG_MESSAGE(
          100,"_diveWanted %.2f (min %.2f, max %.2f, speed %.2f, ca %.2f), fastDive %.2f, below %.1f",
          _diveWanted,minDive,maxDive,-curSpeedMaxDive,
          changeAccel[2],fastDive,belowFlightLevel
        );
      saturate(_diveWanted,minDive,maxDive);
    }

    if (!QIsManual() && !_landContact && !_objectContact && !_waterContact)
    {
      // AI auto-rotation - make sure there is some wind into the rotor
      if (_rotorSpeedWanted<0.1f)
      {
        // we may need to slow down a little bit
        // landing autopilot is active at this time, we only want to correct it, not to disable it
        saturate(_diveWanted,-0.05f,0.2f);
      }
    }
  }

  // when moving slow, do side slip
  float bankInTurn=fabs(zPilotSpeed)*FAST_COEF; // probably in turn
  saturateMin(bankInTurn,1);

  float turnByBank=(fabs(relSpeed[2])+fabs(_pilotSpeed[2])-4)*0.05f;
  saturate(turnByBank,0,1);
  saturateMax(turnByBank,bankInTurn);
  
  if (_pilotDirHelper && driverAlive)
  {
    _bankWanted = 0;
    //float dirC = _dirCompensate;
    static float dirC = 1.0f;
    //float dirC = 0.9f;
    //float dirC = 0.0f;
    //0.5f;
    Vector3 direction=vs.Direction()*(1-dirC)+estDirection*dirC;
    float curHeading=atan2(direction[0],direction[2]);
    static float chCoef = 4.0f;
    float changeHeadingAng=AngleDifference(_pilotHeading,curHeading)*chCoef;
    Limit(changeHeadingAng,-1,1);
    // when heading change is low, we want to make it even lower
    float slowChH = changeHeadingAng * fabs(changeHeadingAng);

    // we want more of the slow (quadratic) change
    float changeHeading = Lerp(changeHeadingAng,slowChH,0.8f);

    if (CHECK_DIAG(DEPath) && GWorld->CameraOn()==this)
    {
      #if _ENABLE_REPORT
        float curHeadingNoC = atan2(vs.Direction()[0],vs.Direction()[2]);
        float changeHeadingNoC = AngleDifference(_pilotHeading,curHeadingNoC)*chCoef;
      #endif
      DIAG_MESSAGE(
        100,
        "changeHeading %.2f (ang %.2f, slow %.2f, noC %.2f)",
        changeHeading*(180/H_PI),changeHeadingAng*(180/H_PI),slowChH*(180/H_PI),changeHeadingNoC*(180/H_PI)
      );
    }

    // when slow, use back rotor
    float fastTurn=floatMax(fabs(relSpeed[2])-6,0)*FAST_COEF;
    Limit(fastTurn,0,1);
    _backRotorWanted=(-changeHeading*2)*(1-fastTurn);
    saturate(_backRotorWanted,-1,+1);

    // when moving fast, turn by banking
    float bankWantedFast=-changeHeading*turnByBank;

    _bankWanted += bankWantedFast;
  }
  
  if (_pilotSpeedHelper && driverAlive)
  {
    float bankWantedSlow = bank+changeAccel[0]*(-1.0/20);

    static float maxBankF = 0.05f;
    float maxSpeedBank = fabs(speedDif[0])*maxBankF;
    saturate(bankWantedSlow,-maxSpeedBank,+maxSpeedBank);
    // avoid using extreme bank for side-slips
    saturate(bankWantedSlow,-0.5f,0.5f);
    _bankWanted = Lerp(bankWantedSlow,_bankWanted,turnByBank);
  }

  if( fabs(_forceBank)<1 )
    _bankWanted=_forceBank;
  saturate(_bankWanted,-MaxBank,MaxBank);

  if (_pilotSpeedHelper && driverAlive)
  {
    // bank is limited 
    //if( fabs(_diveWanted)>0.3 ) 
    // if diving, limit bank
    saturate(_diveWanted,-0.7f,0.6f);
    float diveLimitBank=MaxBank-floatMax(fabs(_diveWanted),fabs(dive))*1.3f;
    saturateMax(diveLimitBank,0.1f);
    saturateMin(bankLimit,diveLimitBank);
    saturate(_bankWanted,-bankLimit,+bankLimit);
  }

  if (!_driver || _driver->IsDamageDestroyed())
  {
    if (!_landContact)
    {
      _cyclicAsideWanted=-0.1;
      _cyclicForwardWanted=0.1;
      _backRotorWanted=-0.1;
      _mainRotorWanted=0.1;
    }
    else
    {
      _cyclicAsideWanted=0;
      _cyclicForwardWanted=0;
      _backRotorWanted=0;
      _mainRotorWanted=0;
    }
  }
  else
  {
    if (_pilotSpeedHelper)
#define DIAG_HELI_BANK 0
#if DIAG_HELI_BANK
      char diagtxt[1024]; sprintf(diagtxt, "heli _bankWanted = %.2f", _bankWanted);
      DIAG_MESSAGE(100, diagtxt);
#endif
    if( vs._rotorSpeed>=0.3 )
    {
      if( !_landContact )
      {
        static float cFactorSlow = 1.0f;
        static float cFactorFast = 3.0f;
        
        // when moving fast, we need to apply cyclic faster as well
        
        float tailRotateFactor = TailRotateFactor(relSpeed);
        
        float cFactor = tailRotateFactor*cFactorFast+(1-tailRotateFactor)*cFactorSlow;

        float fastTurn = floatMax(fabs(relSpeed[2])-6,0)*FAST_COEF;
        Limit(fastTurn,0,1);
        
        _cyclicAsideWanted = +(_bankWanted-bank)*cFactor;

        float minRotorDive = floatMin(Type()->_minMainRotorDive,Type()->_minBackRotorDive);
        float maxRotorDive = floatMax(Type()->_maxMainRotorDive,Type()->_maxBackRotorDive);

        float diveByRotor = _diveWanted;
        
        float adjustedDive = 0;
        if(_sweepState==SweepEngage && fabs(_minAttackDive) > 0)
        {
          //limit attack dive
          _minAttackDive = fabs(_minAttackDive) > fabs(_diveWanted) ? _diveWanted:_minAttackDive;
          // _rotorDiveWanted should be opposite to _diveWanted or _minAttackDive
          // as in _rotorDiveWanted = -_diveWanted;
          // and so before any actual adjustedDive can happen, we should set _rotorDiveWanted to min/max
          _rotorDiveWanted = (_minAttackDive > 0) ? minRotorDive: maxRotorDive;
          adjustedDive = _minAttackDive;
          _minAttackDive = 0;
        }
        else
        {
          saturate(diveByRotor,minRotorDive,maxRotorDive);
          _rotorDiveWanted = -diveByRotor;
          adjustedDive = _diveWanted - diveByRotor;
        }

        _cyclicForwardWanted =- (adjustedDive-dive)*16.0f*cFactor;
        Limit(_cyclicForwardWanted,-1,+1);
      }
      else
      {
        _cyclicAsideWanted=0;
        _cyclicForwardWanted=0;
        _backRotorWanted=0;
        _rotorDiveWanted = 0;
      }
    }
    else
    {
      // no controls available - no engine power
      _cyclicAsideWanted=0;
      _cyclicForwardWanted=0;
      _backRotorWanted=0;
      _mainRotorWanted=0.3;
      _rotorDiveWanted = 0;
    }

    if( _angVelocity.SquareSize()>=10*10 )
    { // if we are rotating fast, leave all controls in neutral position
      _cyclicAsideWanted=0;
      _cyclicForwardWanted=0;
      _backRotorWanted=0;
      _mainRotorWanted=0.3;
      _rotorDiveWanted = 0;
    }
  }

  // perform advanced simulation
  MoveWeapons(deltaT);
  base::Simulate(deltaT,prec);

  // set simulation precision for a next step
  if(GetStopped())
    // when suspended, no need for frequent simulation
    SetSimulationPrecision(HeliSimStepInactive,HeliSimStepInactiveAI);
  else if (QIsManual() || _state>=AutopilotAlign)
    // sometimes we want extra stability, this is always true for player, and sometimes for AI as well (landing, precise maneuvring)
    SetSimulationPrecision(HeliSimStepPriority,HeliSimStepPriorityAI);
  else
    SetSimulationPrecision(HeliSimStepActive,HeliSimStepActiveAI);
}

bool HelicopterAuto::AimWeapon(const TurretContextEx &context, int weapon, Vector3Par direction)
{
  if (weapon < 0)
  {
    if (context._weapons->_magazineSlots.Size() <= 0)
      return false;
    weapon = 0;
  }
  context._weapons->SelectWeapon(this, weapon);
  // move turret/gun accordingly to direction
  Vector3 relDir(VMultiply, context._parentTrans.InverseRotation(), direction);
  // calculate current gun direction
  // compensate for neutral gun position

  if (context._turret && context._turret->Aim(*context._turretVisualState, *context._turretType, relDir, context._parentTrans))
    CancelStop();
  return true;
}

bool HelicopterAuto::CalculateAimWeaponPos(const TurretContext &context, int weapon, Vector3 &pos, float &timeToLead, Target *target, bool exact) const
{
  if (weapon < 0)
  {
    if (context._weapons->_magazineSlots.Size() <= 0)
      return false;
    weapon = 0;
  }
  if ( target->idExact.IsNull())
    return false;
  if (!CheckTargetKnown(context,target))
    return false;

  Vector3 tgtPos, tgtSpd;
  if (exact)
  {
    tgtPos = target->idExact->AimingPosition(target->idExact->FutureVisualState());
    tgtSpd = target->idExact->FutureVisualState().Speed();
  }
  else
  {
    AIBrain *gunnerBrain = context._gunner ? context._gunner->Brain() : NULL;
    tgtPos = target->LandAimingPosition(gunnerBrain);
    tgtSpd = target->GetSpeed(gunnerBrain);
  }

  const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
  const MagazineType *aInfo = magazine ? magazine->_type : NULL;
  const AmmoType *ammo = aInfo ? aInfo->_ammo : NULL;
  float dist = tgtPos.Distance(FutureVisualState().Position());
  float time = aInfo ? dist * aInfo->_invInitSpeed : 0.0f;
  float fall = 0.0f;
  bool rockets = false;
  if (ammo)
  {
    switch (ammo->_simulation)
    {
    case AmmoShotMissile: case AmmoShotRocket:
      if (ammo->maxControlRange<10)
      {
        Vector3 pos = FutureVisualState().PositionModelToWorld((Type()->_missileLPos + Type()->_missileRPos) / 2.0f);
        time = PreviewRocketBallistics(tgtPos, FutureVisualState().PositionModelToWorld(Type()->_missileLPos), aInfo, ammo, fall);
        rockets = true;
        if (time>=FLT_MAX) return false;
      }
      else
      {
        time = 0.0f;
    }
      break;
    default:
      time = PreviewBallistics(tgtPos, FutureVisualState().Position(), aInfo, ammo, fall);
      if (time>=FLT_MAX) return false;
      break;
  }

  }
  
  timeToLead = time;
  
  tgtPos[1] += fall; // consider ballistics
  if (aInfo)
  {
    // for rockets this speed correction aprox. is not working properly
    Vector3 speedEst = rockets? tgtSpd : tgtSpd - FutureVisualState().Speed();
    float maxSpeedEst = aInfo->_maxLeadSpeed;
    if (speedEst.SquareSize() > Square(maxSpeedEst))
      speedEst = speedEst.Normalized() * maxSpeedEst;
    float predTime = floatMax(time + 0.1f, 0.25f);
    tgtPos += speedEst * predTime;
  }

  pos = tgtPos;
  return true;
}

bool HelicopterAuto::CalculateAimWeapon(const TurretContext &context, int weapon, Vector3 &dir, float &timeToLead, Target *target, bool exact ) const
{
  Vector3 tgtPos;
  bool ret = CalculateAimWeaponPos(context, weapon,tgtPos,timeToLead,target,exact);
  if (ret)
  {
    const TurretType* turretType = context._turretType;
    Vector3 weaponPos = VZero;

    // hotfix: news:hs16tn$u2q$1@new-server.localdomain
    bool gunPosSet = false;

    if (turretType)
    {
      weaponPos = turretType->GetTurretAimCenter(Type());
      gunPosSet = true;
    }
    else if (context._turret) 
    {
      weaponPos = Type()->GetGunPos(context._turret->_gunPosIndexToogle);
      gunPosSet = true;
    }

    // try to set first position from turretType positionArray
    if (turretType && !gunPosSet) 
    {
      int PosArrSize = turretType->_gunPosArray.Size();
      
      if (PosArrSize>0)
        weaponPos = Type()->GetGunPos(0);
    }
    
    Vector3 myPos=FutureVisualState().PositionModelToWorld(weaponPos);
    dir = tgtPos-myPos;
  }
  return ret;
}

bool HelicopterAuto::AimWeaponTgt(const TurretContextEx &context, int weapon, Target *target)
{
  Vector3 dir;
  float timeToLead;
  if (!CalculateAimWeapon(context, weapon,dir,timeToLead,target,false)) return false;

  return AimWeapon(context, weapon, dir);
}

Vector3 HelicopterAuto::GetWeaponDirection(ObjectVisualState const& vs, const TurretContext &context, int weapon) const
{
  if (weapon < 0 || weapon >= context._weapons->_magazineSlots.Size())
    return vs.Direction();
  if (!context._turretType)
    return vs.Direction();
  const AmmoType *ammo = context._weapons->GetAmmoType(weapon);
  if (!ammo)
    return vs.Direction();

#if _VBS3
  // return the turret rotation, when laser target is on.
  if(context._weapons->_laserTargetOn)
  {
    return vs.Transform().Rotate(context._turretType->GetTurretDir(GunTurretTransform(vs, *context._turretType)));
  }
#endif

  if (ammo->_simulation == AmmoShotMissile)
    return vs.Direction();
  else if (ammo->_simulation == AmmoShotRocket)
    return vs.Direction();
  else
    return vs.Transform().Rotate(context._turretType->GetTurretDir(GunTurretTransform(vs, *context._turretType)));
}

Vector3 HelicopterAuto::GetWeaponManualControlDirection(ObjectVisualState const& vs, const TurretContext &context, int weapon) const
{
  if (!context._turretType)
    return vs.Direction();
  return vs.Transform().Rotate(context._turretType->GetTurretDir(GunTurretTransform(vs, *context._turretType)));
}

Vector3 HelicopterAuto::GetWeaponManualControlPoint(ObjectVisualState const& vs, const TurretContext &context, int weapon) const
{
  if (!context._turretType)
    return vs.Direction();
  return context._turretType->GetTurretPos(GunTurretTransform(vs, *context._turretType));
}

Vector3 HelicopterAuto::GetWeaponDirectionWanted(const TurretContextEx &context, int weapon) const
{
  if (weapon < 0 || weapon >= context._weapons->_magazineSlots.Size() || !context._turret)
    return FutureVisualState().Direction();
  if (!context._turretType)
    return FutureVisualState().Direction();
  const AmmoType *ammo = context._weapons->GetAmmoType(weapon);
  if (!ammo )
    return FutureVisualState().Direction();
  if (ammo->_simulation == AmmoShotMissile)
    return FutureVisualState().Direction();
  else if (ammo->_simulation == AmmoShotRocket)
    return FutureVisualState().Direction();
  else
  {
    Matrix3Val aim = context._turret->GetAimWantedWorld(context._parentTrans);
    Vector3 dir = context._turretType->GetTurretDir(aim);
    return dir;
  }
}

Vector3 HelicopterAuto::GetWeaponPoint(ObjectVisualState const& vs, const TurretContext &context, int weapon) const
{
  if (weapon < 0 || weapon >= context._weapons->_magazineSlots.Size())
    return VZero;
  //const Magazine *magazine = GetMagazineSlot(weapon)._magazine;
  //if (!magazine) return VZero;
  const AmmoType *ammo = context._weapons->GetAmmoType(weapon);
  if (!ammo)
    return VZero;

#if _VBS3
  // return the turret rotation, when laser target is on.
  if(context._weapons->_laserTargetOn)
  {
    if (!context._turretType) return VZero;
    return context._turretType->GetTurretPos(GunTurretTransform(vs, *context._turretType));
  }
#endif

  switch (ammo->_simulation )
  {
    case AmmoShotRocket:
      return !_rocketLRToggle ? Type()->_rocketLPos : Type()->_rocketRPos;
    case AmmoShotMissile:
    {
      int count = GetLastMissileIndex(context,weapon);
      bool found;
      Vector3 pos = FindMissilePos(count,found).Position();
      if (!found)
        pos = !_missileLRToggle ? Type()->_missileLPos : Type()->_missileRPos;
      return pos;
    }
    case AmmoShotBullet:
    case AmmoShotSpread:
    case AmmoShotLaser:
      if (!context._turretType)
        return VZero;
      return context._turretType->GetTurretPos(GunTurretTransform(vs, *context._turretType));
    case AmmoShotShell:
      if (!context._turretType)
        return VZero;
      return context._turretType->GetTurretPos(GunTurretTransform(vs, *context._turretType));
    case AmmoShotCM:
      {
        return Type()->_cmPos[_cmIndexToggle];
      }
  }
  return VZero;
}

Vector3 HelicopterAuto::GetWeaponCenter(ObjectVisualState const& vs, const TurretContext &context, int weapon) const
{
  const AmmoType *ammo = context._weapons->GetAmmoType(weapon);
  if (!ammo || !context._turret)
    return VZero;
  if (ammo->_simulation == AmmoShotMissile || ammo->_simulation == AmmoShotRocket)
    return base::GetWeaponCenter(vs, context, weapon);
  else
    return context._turret->GetCenter(Type(), *context._turretType);
}

float HelicopterAuto::GetAimed(const TurretContext &context, int weapon, Target *target, bool exact, bool checkLockDelay) const
{
  if( !target )
    return 0;
  if( !target->idExact )
    return 0;
  // check if weapon is aimed
  if (weapon < 0 || weapon >= context._weapons->_magazineSlots.Size())
    return 0;

  const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
  if (!magazine || !magazine->_type)
    return 0;
  const MagazineType *aInfo = magazine->_type;
  if (!aInfo)
    return 0;
  const AmmoType *ammo = aInfo->_ammo;
  if (!ammo)
    return 0;
  if (!CheckTargetKnown(context,target))
    return 0;
  if (!context._gunner)
    return 0;

  Vector3 speed;
  Vector3 ap;
  float seenBefore;
  float visible = GetAimingPosition(ap, speed, seenBefore, exact, context, target, ammo);
  
  float lock = 1;
  if (ammo->_simulation == AmmoShotMissile)
  {
    if (ammo->maxControlRange>10)
    {
      // guided missile 
      Vector3 relPos=PositionWorldToModel(ap);
      // check if target is in front of us
      if( relPos.Z()<=50 ) return 0; // missile fire impossible
      // check if target position is withing missile lock cone
      if( fabs(relPos.X())>relPos.Z() ) return 0;
      if( fabs(relPos.Y())>relPos.Z() ) return 0;
      // the nearer we are, the more precise lock required
      float invRZ=1.0/relPos.Z();
      float lockX=1-fabs(relPos.X())*invRZ;
      float lockY=1-fabs(relPos.Y())*invRZ;
      lock=floatMin(lockX,lockY);
      saturate(lock,0,1);
      // we can fire
#if _ENABLE_CHEATS
      if (CHECK_DIAG(DECombat) && this==GWorld->CameraOn() )
        DIAG_MESSAGE(100,Format("Lock precision %.3f"));
#endif
      if( lock<0.5f )
        lock=0;

    }
    else
    {
      return CheckAimed(ap, speed, context, weapon, target, exact, ammo) *visible;
    }
  }
  else
  {
    if (ammo->_simulation == AmmoShotBullet || ammo->_simulation == AmmoShotSpread || ammo->_simulation == AmmoShotShell)
    {
      lock = CheckAimed(ap, speed, context, weapon, target, exact, ammo);
    }
    else
    {
      // predict shot result
      float dist=ap.Distance(FutureVisualState().Position());
      float time=aInfo ? dist*aInfo->_invInitSpeed : 0;
      Vector3 targetSpeed = exact ? target->idExact->FutureVisualState().Speed() : target->GetSpeed(context._gunner->Brain());
      Vector3 leadSpeed = targetSpeed-FutureVisualState().Speed();
      Vector3 estPos = ap+leadSpeed*time;
      Vector3 wDir=GetWeaponDirection(FutureVisualState(), context, weapon);
      Vector3 wPos=FutureVisualState().PositionModelToWorld(GetWeaponCenter(FutureVisualState(), context, weapon));
      float eDist=wPos.Distance(estPos);
      Vector3 hit=wPos+wDir*eDist;
      hit[1]-=G_CONST*time*time*0.5f;
      Vector3 hError=hit-estPos;
#if _ENABLE_CHEATS
      float distHit = hit.Distance(wPos);
#endif

      hError[1]*=2;
      float error=hError.Size()*0.5f;

      float tgtSize=target->idExact->AimingSize();
      float maxError = tgtSize+ammo->indirectHitRange*0.3f;
      const WeaponModeType *mode = context._weapons->_magazineSlots[weapon]._weaponMode;
      maxError+=dist*mode->_dispersion;

      if (ammo->_simulation != AmmoShotBullet && ammo->_simulation != AmmoShotSpread )
        maxError *=2;

      if (leadSpeed.SquareSize()>Square(10)) maxError *= 2;

#if _ENABLE_CHEATS
      if (CHECK_DIAG(DECombat) && this==GWorld->CameraOn())
        DIAG_MESSAGE(2000,Format("Error %.1f, tgtSize %.1f, time %.2f, ePos %.1f,%.1f,%.1f, distErr %.1f",
        error,tgtSize,time, hError[0],hError[1],hError[2], distHit-eDist));
#endif

      lock = error < maxError;
    }
  }
  if (lock<0.1f)
    return 0;
  if (exact)
    return lock;

  // 0.6 visibility means 0.8 unaimed
  visible=1-(1-visible)*0.5f;
  return lock*visible;
}

// manual control
void HelicopterAuto::DamageCrew( EntityAI *killer, float howMuch, RString ammo)
{
  AIBrain *commander = CommanderUnit();
  if (commander)
  {
    if (GetRawTotalDamage()>=0.7f && commander->GetCombatModeLowLevel()>=CMCombat)
    {
      Time goTime = Glob.time + GRandGen.PlusMinus(2.0f,1.0f);
      if (goTime<_getOutAfterDamage)
        _getOutAfterDamage = goTime;
    }
  }
  base::DamageCrew(killer,howMuch,ammo);
}

void HelicopterAuto::Eject(AIBrain *unit, Vector3Val diff)
{
  // check height
  float surfaceY=GLOB_LAND->SurfaceYAboveWaterNoWaves(FutureVisualState().Position()[0],FutureVisualState().Position()[2]);
  float height = FutureVisualState().Position().Y() - surfaceY;
  bool parachute = (height>30);
  unit->ProcessGetOut(true, parachute, diff);
}

void HelicopterAuto::FakePilot(float deltaT)
{
  _forceDive=1;
}

void HelicopterAuto::AutohoverOn()
{
  //if (_hoveringAutopilot) return;
  _hoveringAutopilot = true;
  //_pilotHeading = atan2(Direction()[0],Direction()[2]);
}

void HelicopterAuto::SuspendedPilot(AIBrain *unit, float deltaT )
{
#if _VBS3 //with simplified flight model, don't change the values
  if(Glob.config.IsEnabled(DTSimplifiedHeliModel))
    return;
#endif

  float surfaceY=GLOB_LAND->SurfaceYAboveWaterNoWaves(FutureVisualState().Position()[0],FutureVisualState().Position()[2]);
  _pilotHeight=FutureVisualState().Position().Y()-surfaceY;
  _pilotSpeed=VZero;
}


void HelicopterAuto::SwitchPilotHeightAGL(bool agl)
{
  if (_pilotHeightAGL==agl)
    return;
  float surfaceY = GLandscape->SurfaceYAboveWaterNoWaves(FutureVisualState().Position()[0],FutureVisualState().Position()[2]);
  if (agl)
    // switch from ASL to AGL
    _pilotHeight -= surfaceY;
  else
    // switch from AGL to ASL
    _pilotHeight += surfaceY;
  _pilotHeightAGL = agl; // manual controls mean no automatic terrain following
}

void HelicopterAuto::KeyboardPilot(AIBrain *unit, float deltaT )
{ 
  Vector3Val position=FutureVisualState().Position();
  float surfaceY=GLOB_LAND->SurfaceYAboveWaterNoWaves(position[0],position[2]);
  float curHeightAGL=position.Y()-surfaceY;

  float curBottomHeight = curHeightAGL + _shape->GeometryLevel()->Min().Y();
  
  _dirCompensate=0; // low heading compensation
  _forceDive=10;
  _forceBank=10;
  // quick commanding forces freeLook
  if (GInput.GetAction(UAForceCommandingMode)>0.5f)
    GInput.lookAroundEnabled = true;

  // automatic gear control
  if (curBottomHeight>30 || curBottomHeight>20 && FutureVisualState()._speed.SquareSizeXZ()>Square(25))
    _pilotGear = false;
  if (FutureVisualState()._speed.SquareSizeXZ()<Square(23) || (FutureVisualState()._rotorSpeed < 0.9)) // (_rotorSpeed < 0.9)) - when main rotor failure open gear earlier
  {
    if (curBottomHeight<5 || curBottomHeight<25 && (FutureVisualState()._speed.SquareSizeXZ()<Square(8) || (FutureVisualState()._rotorSpeed < 0.9)))
      _pilotGear = true;
  }

  //Helicopter thrust pilot
#if _VBS3
  // enable a simplified Control mode, where the speed and altitude are automatically set
  // following the contour is a seperate difficulty setting
  // enable a simplified Control mode, where the speed and altitude are automatically set
  // following the contour is a separate difficulty setting
    // enable a simplified Control mode, where the speed and altitude are automatically set
    // following the contour is a seperate difficulty setting

    SwitchPilotHeightAGL(Glob.config.IsEnabled(DTFollowContour));

    if(Glob.config.IsEnabled(DTSimplifiedHeliModel))
    {
      _pilotHeightHelper = true; // keyboard helper activated
      _pilotVSpeedHelper = true; // keyboard helper activated
//      _pilotSpeedHelper = true; // keyboard helper activated
//      _pilotDirHelper = true;

      if(FutureVisualState()._rotorSpeed > 0.5)
      {
        if(_pilotHeightAGL)
          _defPilotHeight += (GInput.GetAction(UAHeliUp)-GInput.GetAction(UAHeliDown)) * .3;
        else
          _pilotHeight += (GInput.GetAction(UAHeliUp)-GInput.GetAction(UAHeliDown)) * .3;
      }

      if(_pilotHeightAGL) _pilotHeight=_defPilotHeight;

      //prevent to go under ground
      float surfaceY = GLandscape->SurfaceYAboveWater(Position()[0],Position()[2]);
      if(!_pilotHeightAGL) _pilotHeight -= surfaceY;

      if(_pilotSpeed[2] > 11) // don't allow to go below 11m unless speed is below 11m/s
        saturateMax(_pilotHeight, 11);
      else
        saturateMax(_pilotHeight, -.2); //don't go under ground

      if(!_pilotHeightAGL) _pilotHeight += surfaceY;
    }
    else
    {
    _pilotHeightHelper = false; // keyboard helper activated
    _pilotVSpeedHelper = true; // keyboard helper activated
    }
    
#else
    _pilotHeightHelper = false; // keyboard helper activated
    _pilotVSpeedHelper = true; // keyboard helper activated
    SwitchPilotHeightAGL(false);
#endif

  // when using analogue throttle, do not use thrust autopilot
  // switch between analogue / autopilot based on last input
  
  float forward= GInput.GetAction(UAHeliUp)-GInput.GetAction(UAHeliDown);

  float throttleDirect = GInput.GetAction(UAHeliThrottlePos)-GInput.GetAction(UAHeliThrottleNeg);
  
  if (fabs(forward-_lastForward)>0.1f)
    {
    if (_lastForward<FLT_MAX) _thrustHelperTime = Glob.time; // ignore initial settings
    _lastForward = forward;
  }
  if (fabs(throttleDirect-_lastThrotleDirect)>0.1f)
  {
    if (_lastThrotleDirect<FLT_MAX) _analogueThrottleTime = Glob.time; // ignore initial settings
    _lastThrotleDirect = throttleDirect;
  }
  // reset the older input - input methods are not compatible, only one must be used at any given time
  _pilotVSpeedHelper = _analogueThrottleTime<=_thrustHelperTime;

  if (forward>0 || throttleDirect>0)
  {
      if (!EngineIsOn())
        EngineOn();
    }


  if (!_pilotVSpeedHelper)
    {
    _mainRotorWanted = throttleDirect;
    }
  else
  {
    throttleDirect = 0;
#if _VBS3 //increase 
    _pilotSpeed[1]=forward*20.0f;
#else
    _pilotSpeed[1]=forward*10.0f;
#endif
  }
  

  _pilotSpeed[0]=0; // no side slip

  //Helicopter direction pilot
  {
    // TODO: when speed is low and controls was not touched for a long time
    // activate hovering autopilot
    {

      float turn = 0;
      float backRotor = 0;

      //if (!GWorld->HasMap())
      {
        // aim and turn with aiming actions
        ValueWithCurve aimX(0, NULL), aimY(0, NULL);
        if (GWorld->LookAroundEnabled())
        {
          aimX = GInput.GetActionWithCurveExclusive(UAAimRight, UAAimLeft)*Input::MouseRangeFactor;
          aimY = GInput.GetActionWithCurveExclusive(UAAimUp, UAAimDown)*Input::MouseRangeFactor;
        }
        LookAround(aimX, aimY, deltaT, &Type()->_viewPilot, unit);
                  
        // turn keys control bank
        backRotor = GInput.GetAction(UAHeliRudderLeft)-GInput.GetAction(UAHeliRudderRight);
        //joystick was too sensitive - cyclicLRcoef fixes it
        //but keyboard sensitivity affected too: GetAction third parameter fixes it
        const float cyclicLRcoef = 0.7;
        static float cyclicKeyboard = 1.0f;
        turn = (GInput.GetAction(UAHeliCyclicLeft,true,cyclicKeyboard)-GInput.GetAction(UAHeliCyclicRight,true,cyclicKeyboard))*Input::MouseRangeFactor*cyclicLRcoef;
        float mouseLR = (GInput.GetAction(UAHeliLeft)-GInput.GetAction(UAHeliRight))*Input::MouseRangeFactor;
        static float coefLR = 1.0f;
#if _ENABLE_CHEATS
        if (CHECK_DIAG(DEMouseSensitivity))
        {
          if (GInput.GetKeyToDo(KEYBOARD_COMBO_OFFSET*DIK_RCONTROL + DIK_NUMPADPLUS))
            coefLR -= 0.1;
          else if (GInput.GetKeyToDo(KEYBOARD_COMBO_OFFSET*DIK_RCONTROL + DIK_NUMPADMINUS))
            coefLR += 0.1;
          saturate(coefLR,0.1, 50);
          DIAG_MESSAGE(500, Format("Helicopter: mouseSensitivity = %.1f", coefLR));
        }
#endif
        mouseLR *= coefLR;
        float fastFactor = floatMax(0,fabs(FutureVisualState().ModelSpeed().Z())-4)*FAST_COEF;
        saturate(fastFactor,0,1);
        // if we are already banked, allow banking as well
        // otherwise we cannot bank back
        // allow banking back only using mouse
        static float bankBackFactor = 6.0f;
        float bank = floatMin(FutureVisualState().DirectionAside().Y()*bankBackFactor,1);
        if (bank*mouseLR>0) bank = 0;
        float bankFactor = floatMax(fabs(bank),fastFactor);
        backRotor += mouseLR*(1-bankFactor);
        turn += mouseLR*bankFactor;
        forward = GInput.GetAction(UAHeliForward)*Input::MouseRangeFactor + GInput.GetActionHeliFastForward()*2 - GInput.GetAction(UAHeliBack)*Input::MouseRangeFactor;
      }

      // always turn using back rotor
      _backRotorWanted = backRotor;
      saturate(_backRotorWanted,-1,+1);

      if (!_hoveringAutopilot)
      {
        bool realistic = true;
#if _VBS3 //simplified helicopter flight model (set speed and altitude)
        if(Glob.config.IsEnabled(DTSimplifiedHeliModel))
        {
          _pilotSpeedHelper = true; // keyboard helper activated
          _pilotSpeed[2] += forward * 0.1f; 
//          if(_defPilotHeight < 10) // reduce speed close to the ground
//            saturateMin(_pilotSpeed[2], ceil(_defPilotHeight));

          realistic = false;
        }
        else
#endif
        _pilotSpeedHelper = false; // keyboard helper activated

        _pilotDirHelper = false;

        if (realistic)
        {
          static float turnCyclicFactor=1.0f;
          static float diveCyclicFactor=1.0f;
          _pilotCyclicDirect = true;
          _cyclicAsideWanted = turn*turnCyclicFactor;
          _cyclicForwardWanted = forward*diveCyclicFactor;
          //
        }
        else
        {
          _pilotCyclicDirect = false;
          static float turnFactor=1.0f;
          static float diveFactor=1.0f;
          // avoid too fast bank controls

          float massCoef=GetMass()*(1.0/3000);
          saturate(massCoef,1,3);
          float dirEstT = massCoef*0.3f;
          static float minEstT = 0.1f;
          static float maxEstT = 0.5f;
          saturate(dirEstT,minEstT,maxEstT);
          
          const Matrix3 &orientation=FutureVisualState().Orientation();
          Matrix3Val derOrientation=_angVelocity.Tilda()*orientation;
          Matrix3Val estOrientation=orientation+derOrientation*dirEstT;
          
          // remember bank, only adjust it slightly
          float dive = FutureVisualState().Direction().Y() - FutureVisualState()._rotorDive;
          float bank = BankFromOrientation(FutureVisualState().Orientation());
          float estBank = BankFromOrientation(estOrientation);
          float estDive = estOrientation.Direction().Y() - FutureVisualState()._rotorDive;
          (void)dive,(void)estBank;
          
          if (fabs(turn)>0.1f)
          {
            _bankWanted = bank+turn*turnFactor;
            _pilotBankSet = false;
          }
          else if (!_pilotBankSet)
          {
            _pilotBankSet = true;
            _bankWanted = bank;
          }
          saturate(_bankWanted,-MaxBank,+MaxBank);
          
#if _VBS3
          if(!_pilotSpeedHelper)
          {
#endif
          if (fabs(forward)>0.1f)
          {
            _pilotDive = estDive-forward*diveFactor;
            _pilotDiveSet = false;
          }
          else if (!_pilotDiveSet)
          {
            _pilotDiveSet = true;
            _pilotDive = estDive;
          }
          saturate(_pilotDive,-0.7f,+0.7f);

          _diveWanted = _pilotDive;
        }
#if _VBS3
        }
#endif
      }
      else
      {
        _pilotDirHelper = false;

        // control hovering with keyboard
        _pilotSpeedHelper = true; // keyboard helper activated
        _pilotCyclicDirect = false;
        
        // strafe
        _pilotSpeed[0] = (GInput.GetAction(UAHeliCyclicRight)-GInput.GetAction(UAHeliCyclicLeft))*6*Input::MouseRangeFactor;
        float mouseLR = (GInput.GetAction(UAHeliLeft)-GInput.GetAction(UAHeliRight))*Input::MouseRangeFactor;
        static float coefLR = 6.0f;
#if _ENABLE_CHEATS
        if (CHECK_DIAG(DEMouseSensitivity))
        {
          if (GInput.GetKeyToDo(KEYBOARD_COMBO_OFFSET*DIK_RCONTROL + DIK_NUMPADPLUS))
            coefLR -= 0.1;
          else if (GInput.GetKeyToDo(KEYBOARD_COMBO_OFFSET*DIK_RCONTROL + DIK_NUMPADMINUS))
            coefLR += 0.1;
          saturate(coefLR,0.1, 50);
          DIAG_MESSAGE(500, Format("HeliAutoHovering: mouseSensitivity = %.1f", coefLR));
        }
#endif
        _pilotSpeed[0] += coefLR*mouseLR;
        static float SpeedFactor=1.0f; //1.0f //TODO - check it
        _pilotSpeed[0] *= SpeedFactor;
        _pilotSpeed[2] = forward*3.5f;
        _pilotDive = 0;
        _bankWanted = 0;
        
#ifdef _XBOX
          if (forward>=1.5f || fabs(turn)>=0.9f)
            _hoveringAutopilot = false;
#endif
      }
    }
    Limit(_pilotSpeed[2],-10,+Type()->GetMaxSpeedMs());
  }
}

void HelicopterAuto::AvoidGround(float minHeight)
{
  Point3 estimate=FutureVisualState().Position();
  float maxUnder=0;
  for (int i=0; i<2; i++)
  {
    float estY=GLOB_LAND->SurfaceYAboveWaterNoWaves(estimate.X(),estimate.Z());
    estY+=minHeight;
    float estUnder=estY-estimate.Y();
    saturateMax(maxUnder,estUnder);
    estimate+=FutureVisualState()._speed*1.5f;
  }
  static float limitSpeedUnder = 5.0f;
  if (maxUnder>limitSpeedUnder)
  {
    float maxSpeed=Interpolativ(maxUnder,limitSpeedUnder,10,Type()->GetMaxSpeedMs(),0);
    Limit(_pilotSpeed[2],-maxSpeed,maxSpeed);
  }
}

void HelicopterAuto::AvoidCollision()
{
  if (!Airborne())
    return;
  AIBrain *agent = PilotUnit();
  if( !agent )
    return;

  if (agent->GetPlanningMode() == AIBrain::LeaderDirect)
    return;
  // if we are stopped, do not try to avoid
  if( FutureVisualState()._rotorSpeed<=0.1 )
    return;

  // avoid collisions
  float mySize=floatMax(15,CollisionSize()*2); // assume vehicle is not round
  //float mySpeedSize=fabs(ModelSpeed()[2]);
  float gap = mySize*4;
  //float frontGap = mySize*8;
  // check if we are on collision course
  VehicleCollisionBuffer ret;

  const float maxSpeed = GetType()->GetMaxSpeedMs();
  const float mySpeed = FutureVisualState().ModelSpeed().Z();
  const float maxTime = 4;
  const float maxDist = floatMax(mySize*4,floatMax(mySpeed,maxSpeed*0.2)*maxTime);
  GLOB_LAND->PredictCollision(ret,this,maxTime,gap,maxDist);
  if( ret.Size()<=0 ) return;

  // some collision predicted

  //AIGroup *myGroup = unit->GetGroup();

  // precalculate
  //float invMaxSpeed=1/maxSpeed;

  //Vector3 mySpeed=Speed();

  for( int i=0; i<ret.Size(); i++ )
  {
    const VehicleCollision &info=ret[i];
    const EntityAI *who=info.who;

    if (!who->Airborne()) continue;
    // something is near
    // some vehicle
    // determine who should slow down
    // if who is in front of us, slow down to his speed
    // if we are heave and he is enemy soldier, ignore him

#if _ENABLE_CHEATS
    if( CHECK_DIAG(DECombat) )
    {
      Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
      obj->SetPosition(info.pos);
      Color color(1,1,0,0.3);
      //color=Color(1,-1,0)*danger+Color(0,1,0);
      obj->SetScale(info.distance<gap ? 1 : 0.3);
      obj->SetConstantColor(PackedColor(color));
      GScene->ShowObject(obj);
    }
#endif


#if DIAG_COL
    //if( this==GWorld->CameraOn() )
    {
      LogF("%s vs %s",(const char *)GetDebugName(),(const char *)who->GetDebugName());
    }
#endif

    bool sameSubgroup=false;
    AIBrain *myUnit = PilotUnit();
    AIBrain *whoUnit = who->CommanderUnit();
    if (whoUnit && whoUnit->GetUnit() && myUnit && myUnit->GetUnit())
    {
      AISubgroup *whoSubgroup=whoUnit->GetUnit()->GetSubgroup();
      AISubgroup *mySubgrp = myUnit->GetUnit() ? myUnit->GetUnit()->GetSubgroup() : NULL;
      sameSubgroup = whoSubgroup == mySubgrp;
    }

    Vector3 relPos = PositionWorldToModel(who->FutureVisualState().Position());

    // if we are sharing a formation, relax avoidamce
    if (sameSubgroup)
    {
      // collision is still distant, no avoidance needed
      if (info.time>2.0f && who->FutureVisualState().Position().DistanceXZ2(FutureVisualState().Position())>Square(mySize))
        continue;
      // collision not sure, no avoidance needed
      if (info.distance>mySize*2)
        continue;
    }


    bool iAmInFrontOfHim=who->PositionWorldToModel(FutureVisualState().Position()).Z()>0;
    bool heIsInFrontOfMe=relPos.Z()>0;


    if (heIsInFrontOfMe && iAmInFrontOfHim)
    {
      // who is higher should climb
      float heIsHigher = who->FutureVisualState().Position().Y()-FutureVisualState().Position().Y();
      if (heIsHigher<0)
      {
        float surfaceY = GLandscape->SurfaceYAboveWaterNoWaves(FutureVisualState().Position().X(),FutureVisualState().Position().Z());
        saturateMax(_pilotAvoidHighHeight,who->FutureVisualState().Position().Y()-surfaceY+mySize);
        Time until = Glob.time+10;
        if (_pilotAvoidHigh<until)
          _pilotAvoidHigh=until;

#if DIAG_COL
        //if( this==GWorld->CameraOn() )
        {
          LogF("  avoid hi %.2f",_pilotAvoidHighHeight);
        }
#endif
      }
      else if (heIsHigher>0)
      {
        float surfaceY = GLandscape->SurfaceYAboveWaterNoWaves(FutureVisualState().Position().X(),FutureVisualState().Position().Z());
        saturateMin(_pilotAvoidLowHeight,who->FutureVisualState().Position().Y()-surfaceY-mySize);
        Time until = Glob.time+10;
        if (_pilotAvoidLow<until) _pilotAvoidLow=until;

#if DIAG_COL
        //if( this==GWorld->CameraOn() )
        {
          LogF("  avoid lo %.2f",_pilotAvoidLowHeight);
        }
#endif
      }
    }

    if( info.distance>gap )
      continue;

    if (info.distance<mySize)
    {
      // collision imminent - who is back needs to brake
      // note: this will make the heli climbing at the same time
      // check if he is in front of me
      if (heIsInFrontOfMe)
      {

        {
          Vector3Val hisRelSpeed = DirectionWorldToRelative(who->FutureVisualState().Speed());
          // slow down, but it has no sense to slow down much more than his relative speed
          // - we still may want to be a little slower to increase the gap
          _pilotSpeed = Vector3(0,0,floatMax(0,hisRelSpeed.Z()-2.0f));
        }
      }
    }
  } // for(i)
}

// AI autopilot

const float MaxBrakeSpeed = 20;

void HelicopterAuto::BrakingManeuver()
{
  // before we start braking, we need to slow down so that we can turn by back rotor
  // maintain forward dive
  _pilotHeading = atan2(-FutureVisualState().Speed().X(),-FutureVisualState().Speed().Z());
  float curHeading = atan2(FutureVisualState().Direction().X(),FutureVisualState().Direction().Z());
  float headChange = AngleDifference(_pilotHeading,curHeading);

  // once chopper is turned is desired direction, it may start braking
  float brakeDive = floatMax(0,1-headChange*(0.5f/H_PI));
  _forceDive = brakeDive;
  saturate(_forceDive,-0.6f,-0.1f);
}

Vector3 HelicopterAuto::GetStopPosition() const
{
  // FIX: return _stopPosition only when valid
  if (_stopMode == SMNone)
    return base::GetStopPosition();
  return _stopPosition;
}

void HelicopterAuto::Autopilot(float deltaT, Vector3Par targetCur, Vector3Par targetEst, Vector3Par tgtSpeed, Vector3Par direction)
{
  float maxSpeed = Type()->GetMaxSpeedMs();
  // point we would like to reach
  float avoidGround=0;
  Vector3Val position=FutureVisualState().Position();
  Vector3 absDistance=targetCur-position;
  Vector3 distance=DirectionWorldToRelative(absDistance);

  float bank=BankFromOrientation(FutureVisualState().Orientation());
  float dive=FutureVisualState().Direction().Y();

  float sizeXZ2=distance.SquareSizeXZ();
  switch( _state )
  {
    default: //case AutopilotFar:
    {
      Vector3 relTgtPos = DirectionWorldToRelative(absDistance);
      float timeToReach = absDistance.SizeXZ()/maxSpeed;
      
      // "lead target" - est. target position - based on estimated time to reach, limited to max 30 seconds
      Vector3 absDirection = absDistance + floatMin(30, timeToReach)*tgtSpeed;
      _pilotHeading=atan2(absDirection.X(),absDirection.Z());
      avoidGround=30;
      _pilotHeight=avoidGround;
      Vector3 relSpeed=DirectionWorldToRelative(FutureVisualState()._speed-tgtSpeed);
      // use special maneuvre for fast braking
      // before using it we need to slow down, though
      #define BRAKE_SEC 3.0
      if
      (
        tgtSpeed.SquareSize()<Square(10) // target is slow
        && (FutureVisualState()._speed.SquareSize()-tgtSpeed.SquareSize())>Square(MaxBrakeSpeed*0.5f) // we will need to brake
      )
      {
        if( distance[2]-relSpeed[2]*BRAKE_SEC<0 && sizeXZ2<Square(450) && FutureVisualState().ModelSpeed().Z()<MaxBrakeSpeed)
          // we are approaching the target, start braking
          _state=GetBackRotorMalfunction()<0.6f ? AutopilotBrake : AutopilotNear;
      }
      else if( sizeXZ2<200*200 )
        _state=AutopilotNear;
      
      _pilotSpeed[0]=0; // no side slips
      _pilotSpeed[1]=0; // vertical speed is ignored anyway
      if (tgtSpeed.SquareSize()<Square(5))
      {
        float fast=sqrt(sizeXZ2)*(1.0f/1000);
        Limit(fast,0,0.8f);

        _pilotSpeed[2]=maxSpeed*fast+1;
      }
      else
      {
        static float timeToReach = 10.0f;
        // adjust speed so that you reach target at given time
        _pilotSpeed[2] = relTgtPos.Z()*(1.0/timeToReach);
      }
      // target height
    }
    break;
    case AutopilotBrake:
    {
      Vector3 relSpeed=DirectionWorldToRelative(FutureVisualState()._speed-tgtSpeed);
      // wait until loosing speed
      _pilotSpeed[0]=0; // no side slips
      _pilotSpeed[1]=0; // vertical speed is ignored anyway
      avoidGround=30;
      _pilotHeight=avoidGround;
      BrakingManeuver();
      
      if (sizeXZ2>Square(550))
        _state = AutopilotFar;
      else if (tgtSpeed.SquareSize()>40*40) // target is too fast
        _state=AutopilotNear;
      else if (FutureVisualState().Speed().SquareSize()<Square(maxSpeed*0.25f))
        // if we are moving slow, we may stop braking
        _state = AutopilotNear;
    }
    break;
    case AutopilotNear:
    {
      Vector3 absDirection = targetEst-FutureVisualState().Position(); // "lead target" - est. target position
      
      
      _pilotHeading=atan2(absDirection.X(),absDirection.Z());
      if (tgtSpeed.SquareSize()>Square(1))
      { // to avoid overshooting with moving target, we limit the angle from the target velocity
        float aside = NearestPointInfiniteDistance(targetCur,targetCur+tgtSpeed,position);
        // limit less when far aside from the target line
        float maxAngle = Interpolativ(aside,maxSpeed*0.01f,maxSpeed*0.2f,H_PI*0.05f,H_PI);
        // limit more when target is moving fast
        float maxDiff = Interpolativ(tgtSpeed.SizeXZ(),0,maxSpeed*0.5f,maxAngle,maxAngle*0.25f);
        float tgtHeading = atan2(tgtSpeed.X(),tgtSpeed.Z());
        float headingDiff = AngleDifference(_pilotHeading,tgtHeading);
        #if _ENABLE_CHEATS
          if (this==GWorld->CameraOn() && CHECK_DIAG(DEPath))
          {
            DIAG_MESSAGE(
              500,"Aside %.1f, maxAngle %.1f, maxDiff %.1f",aside,maxAngle*(180/H_PI),maxDiff*(180/H_PI)
            );
          }
        #endif
        saturate(headingDiff,-maxDiff,+maxDiff);
        _pilotHeading = tgtHeading + headingDiff;
      }
      
      float speedSize=fabs(FutureVisualState().ModelSpeed().Z());
      
      float avoidGroundStatic = floatMinMax(_defPilotHeight,8,30);
      avoidGround = floatMax(avoidGroundStatic,speedSize * 0.35f);

      
      float targetAbove=targetCur.Y()-GLOB_LAND->SurfaceYAboveWaterNoWaves(position[0],position[2]);
      // autopilot is used to reach some place (e.g. for landing) or to keep in formation
      // when keeping in formation it makes no sense to avoid ground depending on the target height
      if (tgtSpeed.SquareSize()<Square(1))
      {
        saturateMax(avoidGround,  targetAbove);
      }

      _pilotHeight = floatMax(avoidGround,targetAbove);
      _pilotSpeed[0]=0; // no side slips
      _pilotSpeed[1]=0; // vertical speed is ignored anyway
      // slow down near the target
      // select speed so that you will reach target in 10 sec
      if (tgtSpeed.SquareSize()<Square(1))
      {
        // target is static
        // if we are moving very fast, we need to brake
        float fast=sqrt(sizeXZ2)*(1.0/500);
        Limit(fast,0,1);
        float wantedSpeed = maxSpeed*fast*0.5+1;
        float currentSpeed = FutureVisualState().ModelSpeed()[2];
        _pilotSpeed[2] = wantedSpeed;
        if
        (
          sizeXZ2<Square(50) // near enough
          // never assume aligned when target is moving fast
          && FutureVisualState()._speed.Distance2(tgtSpeed)<Square(10) // speed difference is not too high
        )
          _state=AutopilotAlign;
        else if (currentSpeed>wantedSpeed*1.5 && currentSpeed>45)
          // moving too fast - initiate braking maneuver
          _state=GetBackRotorMalfunction()<0.6f ? AutopilotBrake : AutopilotNear;
      }
      else
      {
        // moving target
        // target is the place we should be right now
        Vector3 relTgtPos = DirectionWorldToRelative(targetCur-FutureVisualState().Position());
        //if (fabs(relTgtPos[2])<1.0f) relTgtPos[2] = 0; // avoid oscillation with small offsets
        static float tgtSpeed1 = 0.2f;
        // adjust speed so that you reach target at given time
        static float tgtSpeed2 = 0.005f;
        _pilotSpeed[2] = relTgtPos.Z()*tgtSpeed1 + relTgtPos.Z()*floatMin(fabs(relTgtPos.Z()),30)*tgtSpeed2;
        // note: small adjustments need to be done more carefully
        
        #if _ENABLE_CHEATS
          if (this==GWorld->CameraOn() && CHECK_DIAG(DEPath))
          {
            DIAG_MESSAGE(
              500,"relTgtPos %.2f, relSpd %.2f, spd1 %.2f, spd2 %.2f",
              relTgtPos[2],_pilotSpeed[2],relTgtPos.Z()*tgtSpeed1,relTgtPos.Z()*fabs(relTgtPos.Z())*tgtSpeed2
            );
          }
        #endif
        if (distance[2]>fabs(distance[0])*2 && sizeXZ2>300*300)
          // far away, heading to target, target is moving slow
          _state=AutopilotFar;
      }
    }
    break;
    case AutopilotAlign: case AutopilotReached:
    {
      _pilotHeading=atan2(direction.X(),direction.Z());
      float sizeXZ=sqrt(sizeXZ2);
      
      float speedSize=fabs(FutureVisualState().ModelSpeed().Z());
      // never fly higher than AutopilotNear has forced us
      float avoidGroundStatic = floatMinMax(_defPilotHeight,8,30);
      float avoidGroundNear = floatMax(avoidGroundStatic,speedSize * 0.35f);
      
      float highX=Interpolativ(sizeXZ,5,50,2,avoidGroundNear);
      float highZ=highX;
      // control to be there in estT sec
      const float estT=4.0;
      Vector3Val estSpeed=DirectionWorldToRelative(FutureVisualState()._speed+2.0*FutureVisualState()._acceleration);
      float estSpeedZ = estSpeed.Z();
      float estSpeedX = estSpeed.X();
      Vector3Val estPosA = FutureVisualState().Position()+FutureVisualState()._speed*estT+0.5*estT*estT*FutureVisualState()._acceleration;
      Vector3Val estTgt = targetCur+tgtSpeed*estT;
      
      Vector3 tgtPos=DirectionWorldToRelative(estTgt-estPosA);

#if _ENABLE_CHEATS
      if( CHECK_DIAG(DEPath) )
      {
        Vector3Val estPos=FutureVisualState().Position()+FutureVisualState()._speed*estT; //+0.5*estT*estT*_acceleration;
        {
          Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
          obj->SetPosition(estPos);
          obj->SetScale(1);
          obj->SetConstantColor(PackedColor(Color(1,0,0)));
          GScene->ShowObject(obj);
        }
        {
          Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
          obj->SetPosition(estPosA);
          obj->SetScale(0.75);
          obj->SetConstantColor(PackedColor(Color(1,1,0)));
          GScene->ShowObject(obj);
        }
      }
#endif

      // if we are nearly aligned and stable, tolerate some inaccuracy
      if( FutureVisualState()._speed.SquareSizeXZ()<2 && fabs(dive)<0.1 && fabs(bank)<0.1 )
      {
        if( fabs(tgtPos[2])<2 ) tgtPos[2]=0,highZ=0;
        if( fabs(tgtPos[0])<2 ) tgtPos[0]=0,highX=0;
      }
      

      float high=floatMax(highX,highZ);
      if( high<4.0f && FutureVisualState()._speed.Distance2(tgtSpeed)<Square(2.5f)) _state=AutopilotReached;


      if( tgtSpeed.SquareSize()<Square(1) )
      {
        float high=floatMax(highX,highZ);
        float highSpeed=Interpolativ(FutureVisualState()._speed.Distance(tgtSpeed),0.2,20,2,30);
        saturateMax(high,highSpeed);
      }
      else
        _state = AutopilotNear;

      // once we are reached, stay reached, stay low

      float targetAbove=targetCur.Y()-GLOB_LAND->SurfaceYAboveWaterNoWaves(position[0],position[2]);

      if( _state==AutopilotReached )
      {
        avoidGround=-0.5;
        _pilotHeight = targetAbove;
      }
      else
      {
        avoidGround=high;
        _pilotHeight=floatMax(high,targetAbove);
      }
      
      float apDive=dive-tgtPos.Z()*0.05;
      float apBank=bank-tgtPos.X()*0.05;

      // if speed is high enough, do not accelerate
      const float maxZSpd=1;
      const float maxXSpd=1;
      if( FutureVisualState().ModelSpeed().Z()>+maxZSpd ) saturateMax(apDive,0); // no negative
      if( FutureVisualState().ModelSpeed().Z()<-maxZSpd ) saturateMin(apDive,0); // no positive
      if( FutureVisualState().ModelSpeed().X()>+maxXSpd ) saturateMax(apBank,0);
      if( FutureVisualState().ModelSpeed().X()<-maxXSpd ) saturateMin(apBank,0);

      if( estSpeedZ>0 ) saturateMax(apDive,-0.1); // little negative
      if( estSpeedZ<0 ) saturateMin(apDive,+0.1); // little positive
      if( estSpeedX>0 ) saturateMax(apBank,-0.1);
      if( estSpeedX<0 ) saturateMin(apBank,+0.1);

      float posLimit=floatMax(tgtPos.SizeXZ()*(1.0f/4)-0.2f,0);
      float spdLimit=floatMax(tgtSpeed.SizeXZ()*(1.0f/4)-0.2f,0);
      float maxDive=spdLimit*0.3f+posLimit*0.2f;
      saturateMin(maxDive,0.5f);

      saturate(apDive,-maxDive,+maxDive);
      saturate(apBank,-maxDive,+maxDive);

      _forceDive=apDive;
      _forceBank=apBank;

      // pilotSpeed is not used (forceDive and forceBank is used instead)
      // but it may be used in MP prediction
      // it should be low: setting it to zero should give good results
      _pilotSpeed = VZero;

#if _ENABLE_CHEATS
      if( CHECK_DIAG(DEPath) && GLOB_WORLD->CameraOn()==this )
        DIAG_MESSAGE(500,Format("spd %.1f,%.1f, mspd %.1f,%.1f espd %.1f,%.1f, tPos %.1f,%.1f d %.2f->ad %.2f, b %.2f->ab %.2f",
          _pilotSpeed[0],_pilotSpeed[2],FutureVisualState().ModelSpeed()[0],FutureVisualState().ModelSpeed()[2],estSpeed[0],estSpeed[2],tgtPos.X(),tgtPos.Z(),dive,apDive,bank,apBank));
#endif
      
      if( sizeXZ2>Square(90))
        _state=AutopilotNear;
    }
    break;
  }
  // if target is flying a turn, we still want to fly at its full speed, not its speed relative to us
  // if he is fast, we need to consider his horizontal speed instead instead of DirectionWorldToRelative
  // because once we lose forward speed, we will hardly recover it
  
  Vector3 addSpeed = DirectionWorldToRelative(tgtSpeed);
  
  float wantedSpeed = tgtSpeed.SizeXZ();
  float fastTarget = Interpolativ(wantedSpeed,maxSpeed*0.1f,maxSpeed*0.5f,0,1);
  // do this only when our and wanted speed direction agree (more or less)
  // caution: avoid factor 1, as this could make formation flying unstable (overrunning the leader)
  float directionAgrees = InterpolativC(addSpeed.CosAngle(VForward),0.5f,0.7f,0,0.99f);
  saturateMax(addSpeed[2],fastTarget*directionAgrees*wantedSpeed);
  
  _pilotSpeed += addSpeed;
  Limit(_pilotSpeed[2],-10,maxSpeed);
  if( avoidGround>0 )
    AvoidGround(avoidGround);
}

void HelicopterAuto::ResetAutopilot()
{
  // We set state to near. It will go to far automatically (if necessary).
  _state=AutopilotFar;
  //_apDive=0,_apBank=0;
}

bool HelicopterAuto::ProcessFireWeapon(const TurretContext &context, int weapon, const Magazine *magazine, EntityAI *target,
  const RemoteFireWeaponInfo *remoteInfo, RemoteFireWeaponInfo *localInfo, bool forceLock)
{
  const MagazineType *aInfo = magazine ? magazine->_type : NULL;
  const AmmoType *ammo = context._weapons->GetAmmoType(weapon);
  VisualState &vs = FutureVisualState();
  if (!ammo)
    return false;
  if (localInfo)
  {
    localInfo->_position = vs.Position();
    localInfo->_visible=floatMax(ammo->visibleFire, ammo->audibleFire);
  }
  switch (ammo->_simulation )
  {
  case AmmoShotRocket:
    if (remoteInfo)
      return false;
    {
      _rocketLRToggle=!_rocketLRToggle;
      Vector3Val pos=( _rocketLRToggle ? Type()->_rocketLPos : Type()->_rocketRPos );
      localInfo->_direction = vs.Direction();
      return FireMissile(context, weapon,pos,localInfo->_direction,Vector3(0,0,aInfo->_initSpeed),target, true, false, forceLock);
    }
  case AmmoShotMissile:
    if (remoteInfo) return false;
    {
      int count = GetLastMissileIndex(context,weapon);
      bool found;
      Vector3 pos = FindMissilePos(count,found).Position();
      if (!found)
      {
        pos=( _missileLRToggle ? Type()->_missileLPos : Type()->_missileRPos );
        _missileLRToggle=!_missileLRToggle;
      }
      localInfo->_direction = vs.Direction();
      return FireMissile(context, weapon,pos,localInfo->_direction,Vector3(0,0,aInfo->_initSpeed),target, true, false, forceLock);
    }
  case AmmoShotShell:
    {
      Matrix4Val shootTrans = context._turretType ? GunTurretTransform(vs, *context._turretType) : M4Identity;
      Vector3Val pos = context._turretType ? context._turretType->GetTurretPos(shootTrans) : VZero;
      bool fired;
      Vector3 dir;
      if (remoteInfo)
      {
        dir = remoteInfo->_direction;
        fired = FireShell(context, weapon, pos, dir, target, false, true);
      }
      else
      {
        Vector3 aimtDirection =  context._turretType ? context._turretType->GetTurretDir(shootTrans) : VForward;
        vs.DirectionModelToWorld(dir, aimtDirection);
        if (GIsManual(context._gunner))
        {
          // get fov from the current camera settings
          //CameraType camType = GWorld->GetCameraType();
          //float fov = GScene->GetCamera()->Left();
          float fov = GetCameraFOV();
          // let vehicle adjust the weapon depending on fov
          dir = AdjustWeapon(context, weapon,fov,dir);
        }

        localInfo->_direction = dir;
        fired = FireShell(context, weapon, pos, dir, target, false, false);
      }
      // compute the momentum change, assume a projectile mass 0.25kg

      // make the recoil proportional to muzzle velocity - this is done to reduce recoil for grenade launchers
      float massEst = ammo->hit*(0.25f/13.0f); // assume hit 13 equals to 0.5 kg, hit 650 -> 12 kg
      float forceSize = massEst*aInfo->_initSpeed;
      Vector3 force=-dir*forceSize;
      Vector3 forcePos=vs.DirectionModelToWorld(pos-_shape->CenterOfMass());
      // simulated both remote and local - do not transfer
      AddImpulse(force,forcePos.CrossProduct(force));
      return fired;
    }
  case AmmoShotBullet:
  case AmmoShotSpread:
    {
      Matrix4Val shootTrans = context._turretType ? GunTurretTransform(vs, *context._turretType) : M4Identity;
      Vector3 pos;
      Vector3 dir;
      if (remoteInfo) dir = remoteInfo->_direction;
      if (context._weapons->_magazineSlots[weapon]._weapon->_shotFromTurret)
      {
        if (context._turretType)
          pos = context._turretType->
            GetTurretPos(shootTrans);
        else if (context._turret)
          pos = Type()->GetGunPos(context._turret->_gunPosIndexToogle);
        else
          pos = VZero;        
          
        if (!remoteInfo)
        {
          Vector3 aimtDirection = context._turretType ? 
            context._turretType->
              GetTurretDir(shootTrans) : 
            VForward;
            
          vs.DirectionModelToWorld(dir, aimtDirection);
          if (GIsManual(context._gunner))
          {
            // get fov from the current camera settings
            //CameraType camType = GWorld->GetCameraType();
            //float fov = GScene->GetCamera()->Left();
            float fov = GetCameraFOV();
            // let vehicle adjust the weapon depending on fov
            dir = AdjustWeapon(context, weapon,fov,dir);
          }
          localInfo->_direction = dir;
        }
      }
      else
      { 
        size_t pointsCnt = 0;
        if (Type()) 
          pointsCnt = Type()->_gunPosArray.Size();                       
        
        if (context._turretType)
          pos = shootTrans.FastTransform(context._turretType->_gunPos);
        else
        {
          size_t pointsCnt = Type()->_gunPosArray.Size();         
          pos = pointsCnt>0 ? 
            shootTrans.FastTransform(Type()->_gunPosArray[_gunPosIndexToogle]) :
            shootTrans.FastTransform(VZero);          
        }        
             
        //pos = shootTrans.FastTransform(context._turretType ? context._turretType->_gunPos : Type()->_gunPos);
        if (!remoteInfo)
        {
          Vector3Val gunDir = Type()->_gunDirArray[_gunPosIndexToogle];
          Vector3 aimtDirection = shootTrans.Rotate(context._turretType ? context._turretType->_gunDir : gunDir);
          vs.DirectionModelToWorld(dir , aimtDirection);
          if (GIsManual(context._gunner))
          {
            // get fov from the current camera settings
            //CameraType camType = GWorld->GetCameraType();
            //float fov = GScene->GetCamera()->Left();
            float fov = GetCameraFOV();
            // let vehicle adjust the weapon depending on fov
            dir = AdjustWeapon(context, weapon,fov,dir);
          }
          localInfo->_direction = dir;
        }
        
        // update toggle
        if (pointsCnt>0)
          _gunPosIndexToogle = (++_gunPosIndexToogle)%pointsCnt;
      }

      return FireMGun(context, weapon, pos, dir, target, false, remoteInfo!=NULL);
    }
    break;
  case AmmoNone:
    if (localInfo) localInfo->_direction = VForward;
    break;
  case AmmoShotLaser:
    if (remoteInfo) return false;
    localInfo->_direction = VForward;
    FireLaser(*context._weapons, weapon, target);
    break;
  case AmmoShotCM:
    if (remoteInfo) return false;
    {
      bool fired = false;
      for(int i=0; i< std::min(magazine->GetAmmo(),Type()->_cmPos.Size()); i++)
      {
        _cmIndexToggle = (_cmIndexToggle+1)%Type()->_cmPos.Size();
        Vector3Val pos=( Type()->_cmPos[_cmIndexToggle] );
        Vector3 dir = (Type()->_cmDir[_cmIndexToggle]) - pos;
        dir.Normalize();
        localInfo->_direction = dir;
        if(FireCM(context, weapon,pos,localInfo->_direction,aInfo->_initSpeed*dir,target, true, false)) fired = true;
      }
      return fired;
    }
  default:
    if (localInfo) localInfo->_direction = VForward;
    Fail("Unknown ammo used.");
    break;
  }
  return false;
}

bool HelicopterAuto::FireWeapon(const TurretContextV &context, int weapon, TargetType *target, bool forceLock)
{
  if (GetNetworkManager().IsControlsPaused())
    return false;

  if (weapon < 0 || weapon >= context._weapons->_magazineSlots.Size())
    return false;
  if( !GetWeaponLoaded(*context._weapons, weapon) )
    return false;
  if( !IsFireEnabled(context) )
    return false;

  const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
  if (!magazine)
    return false;
 
  RemoteFireWeaponInfo local;
  bool fired = ProcessFireWeapon(context, weapon, magazine, target, NULL, &local, forceLock);
  if( fired )
  {
    base::PostFireWeapon(context, weapon, target, local);
    return true;
  }
  return false;
}

bool HelicopterAuto::FireWeaponEffects(const TurretContext &context, int weapon, const Magazine *magazine,EntityAI *target, const RemoteFireWeaponInfo *remoteInfo)
{
  const AmmoType *ammo = ValidateFireWeaponWithAmmo(weapon, context, magazine);
  if (!ammo) return false;
  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];

  if (EnableVisualEffects(SimulateVisibleNear)) switch (ammo->_simulation)
  {
    case AmmoShotRocket:
    case AmmoShotMissile:
    case AmmoShotCM:
    case AmmoNone:
      break;
    case AmmoShotShell:
      {
        context._weapons->_gunClouds.Start(1.5f);
        context._weapons->_gunFire.Start(0.1f,1,false);
      }
      break;
    case AmmoShotBullet:
    case AmmoShotSpread:
      context._weapons->_mGunClouds.Start(0.1);
      {
        float duration = 0.1f;
        float intensity = 0.06f;
        if (slot._weapon)
        {
          duration = slot._weapon->_fireLightDuration;
          intensity = slot._weapon->_fireLightIntensity;
        }
        if (duration > 0 && intensity > 0)
          context._weapons->_mGunFire.Start(duration, intensity, true);
      }
      context._weapons->_mGunFireUntilFrame = Glob.frameID+1;
      context._weapons->_mGunFireTime = Glob.time;
      int newPhase;
      while ((newPhase = toIntFloor(GRandGen.RandomValue() * 3)) == context._weapons->_mGunFirePhase)
        ;
      context._weapons->_mGunFirePhase = newPhase;
      break;
  }
  return base::FireWeaponEffects(context, weapon, magazine,target,remoteInfo);
}

// AI interface

float HelicopterType::GetFieldCost(const GeographyInfo &info, CombatMode mode) const
{
  return 1;
}

float HelicopterType::GetBaseCost(const GeographyInfo &geogr, bool operative, bool includeGradient) const
{
  float cost=GetMinCost(); // basic speed
  // water is low and therefore following water is usually safer and quicker
  if (geogr.u.minWaterDepth>0) cost*=0.90f; // water everywhere
  else if (geogr.u.maxWaterDepth>0) cost*=0.95f; // water somewhere
  // penalty for objects
  //cost *= 1 + geogr.howManyObjects*0.1;
  if (includeGradient)
  {
    // avoid steep hills
    // penalty for hills
    int grad = geogr.u.gradient;
    Assert(grad <= 7);
    static const float gradPenalty[8]={1.0,1.02,1.05,1.2,1.3,1.5,1.7,2.0};
    cost *= gradPenalty[grad];
  }
  return cost;
}

float HelicopterType::GetGradientPenalty(float gradient) const
{
  // TODO: continuous function, sign of gradient
  gradient = fabs(gradient);

  if (gradient >= 0.95f) return 2.0f; // level 7
  else if (gradient >= 0.60f) return 1.7f; // level 6
  else if (gradient >= 0.40f) return 1.5f; // level 5
  else if (gradient >= 0.25f) return 1.3f; // level 4
  else if (gradient >= 0.15f) return 1.2f; // level 3
  else if (gradient >= 0.10f) return 1.05f; // level 2
  else if (gradient >= 0.05f) return 1.02f; // level 1
  return 1.0f; // level 0
}

float HelicopterType::GetCostTurn(int difDir, int &debet, bool forceFullTurn) const
{ // in sec
  if( difDir==0 )
    return 0;
  float aDir=fabs(difDir);
  float cost=aDir*0.15+aDir*aDir*0.02;
  if( difDir<0 )
    return cost*0.8;
  return cost;
}


float HelicopterAuto::FireInRange(const TurretContext &context, int weapon, float &timeToAim, const Target &target) const
{
  if (weapon!=context._weapons->ValidatedCurrentWeapon())
    // assume switching mode takes a little time
    timeToAim = 0.25f;
  //return GetAimed(weapon,target.idExact);
  return 1;
}

float HelicopterAuto::FireAngleInRange(const TurretContext &context, int weapon, Vector3Par rel) const
{
  // helicopter cannot fire high, can fire slight low
  if( rel.Y()>0 )
    return 0;
  float size2=rel.SquareSizeXZ();
  float y2=Square(rel.Y());
  const float maxY=0.25;
  if( y2>size2*Square(maxY) )
    return 0;
  // nearly same level
  float invSize=InvSqrt(size2);
  return 1-rel.Y()*invSize*(1/maxY);
}

#if _ENABLE_AI

void HelicopterAuto::AIGunner(TurretContextEx &context, float deltaT)
{
  AIBrain *unit = context._gunner->Brain();
  Assert(unit);

  int selected = context._weapons->ValidatedCurrentWeapon();
  if (!context._weapons->_fire._fireTarget || context._weapons->_fire.GetTargetFinished(unit))
  {
    context._weapons->_fire._gunner = NULL;
    context._weapons->_fire._weapon = -1;
    context._weapons->_fire._fireTarget = NULL;
    
    // make sure turret is in its default position
    if (context._turret)
      context._turret->LockForward(*context._turretType);
    return;
  }
  else
    AimWeaponTgt(context, selected, context._weapons->_fire._fireTarget);
  
  if (selected < 0)
    return;
  if (context._weapons->_fire._firePrepareOnly)
    return;
  
  if (CommanderUnit() && CommanderUnit()->IsPlayer() && !context._weapons->_fire._fireCommanded)
  {
    // fire individually only with autoFire weapons
    const WeaponModeType *mode = context._weapons->_magazineSlots[selected]._weaponMode;
    if (!mode || !mode->_autoFire)
      return;
  }

#if 0
  DIAG_MESSAGE(1000, Format("%s fires to %s", cc_cast(GetDebugName()), cc_cast(context._weapons->_fire._fireTarget.IdExact()->GetDebugName())));
  DIAG_MESSAGE(1000, Format("- weapon effects %s", PreloadFireWeaponEffects(*context._weapons, selected) ? "preloaded" : "not preloaded"));
  DIAG_MESSAGE(1000, Format("- weapon %s", GetWeaponLoaded(*context._weapons, selected) ? "loaded" : "not loaded"));
  DIAG_MESSAGE(1000, Format("- weapon aimed: %.3f", GetAimed(context, selected, context._weapons->_fire._fireTarget)));
  DIAG_MESSAGE(1000, Format("- weapon %s", GetWeaponReady(*context._weapons, selected, context._weapons->_fire._fireTarget) ? "ready" : "not ready"));
#endif

  // manual fire disable gunner only in the primary turret
  bool manualFire = false;
  if (context._turretType && context._turretType->_primaryGunner)
    manualFire = IsManualFire();

  if (GetWeaponLoaded(*context._weapons, selected) && !manualFire)
  {
    // check if weapon is aimed
    if (context._weapons->_fire._fireTarget &&
        (PreloadFireWeaponEffects(*context._weapons, selected),
        GetWeaponReady(*context._weapons, selected, context._weapons->_fire._fireTarget)))
    {
      float aimed = GetAimed(context, selected, context._weapons->_fire._fireTarget, false, true);
      if(aimed >= 0.75)
      {
        //weapon is not locked
        if(!GetWeaponLockReady(context, context._weapons->_fire._fireTarget, selected, aimed)) return;

        if (!GetAIFireEnabled(context._weapons->_fire._fireTarget))
          ReportFireReady();
        else
        {
          FireWeapon(context, selected, context._weapons->_fire._fireTarget->idExact, false);
          _fireState = FireDone;
          context._weapons->_fire._fireCommanded = false;
          _fireStateDelay = Glob.time + 5; // leave some time to recover
        }
      }
    }
    else
    {
      // check other targets as well
      float timeToLive = unit->GetTimeToLive();
      const TargetList *list = unit->AccessTargetList();
      if (list)
      {
        for (int i=0; i<list->EnemyCount(); i++)
        {
          TargetNormal *tgt = list->GetEnemy(i);
          if (!tgt || !unit->IsEnemy(tgt->side))
            continue;
          if (tgt->State(unit) < TargetEnemy)
            continue;
          if (!GetWeaponReady(*context._weapons, selected, tgt))
            continue;
          if (!GetAIFireEnabled(tgt))
            continue;
          if (GetAimed(context, selected, tgt)<0.75)
            continue;
          FireResult result;
          if (WhatFireResult(result, *tgt, context, selected, timeToLive)==FPCan && result.CleanSurplus())
          {
            FireWeapon(context, selected, tgt->idExact, false);
            _fireState = FireDone;
            _fireStateDelay = Glob.time+5; // leave some time to recover
            break;
          }
        }
      }
    }
  }
}

#endif //_ENABLE_AI

void HelicopterAuto::MoveWeapons(float deltaT)
{
  MoveCrewHead();
}

#if _ENABLE_AI

void HelicopterAuto::AIPilot(AIBrain *unit, float deltaT )
{
  Assert(unit);
  bool isLeader = unit->GetFormationLeader() == unit;

  // AI: activate all helpers
  _pilotSpeedHelper=true;
  _pilotCyclicDirect = false;
  _pilotDirHelper=true;
  _pilotHeightHelper=true;
  _pilotVSpeedHelper= false;
  SwitchPilotHeightAGL(true);

  _dirCompensate=1;
  // if aiming for fire, we need quick reactions
  /*
  if( _fireMode>=0 )
  {
    if( !_firePrepareOnly ) _dirCompensate=0.1;
    else _dirCompensate=0.3;
  }
  */

  Vector3 steerPos=SteerPoint(2.0,4.0);

  VisualState &vs = FutureVisualState();

  Vector3 steerWant=PositionWorldToModel(steerPos);
  
  float headChange=atan2(steerWant.X(),steerWant.Z());
  float speedWanted=0;
  
  float inCombat=2-_nearestEnemy*(1.0/400);
  saturate(inCombat,0,1);
  if (unit->GetCombatModeLowLevel() <= CMSafe)
    inCombat = 0;

  speedWanted=Type()->GetMaxSpeedMs()*inCombat*0.5;
  
  Target *assigned = unit->GetTargetAssigned();
  if (assigned && _sweepTarget!=assigned && Type()->_enableSweep)
  {
    _sweepTarget=assigned;
    _sweepState=SweepDisengage;
    _sweepDelay=Glob.time+10;
  }

  if (_sweepTarget)
  {
    EntityAI *swAI = _sweepTarget->idExact;
    if (unit->GetCombatModeLowLevel()<=CMSafe || !unit->IsFireEnabled(_sweepTarget))
      _sweepTarget = NULL;
    // check if target is alive
    else if
    (
      // destroyed
      !swAI || swAI->IsDamageDestroyed()
      // or not enemy and not ordered to fire
      || _sweepTarget->State(unit)<TargetEnemy && unit->GetEnableFireTarget()!=_sweepTarget
    )
      _sweepTarget=NULL;
  }

  _forceDive=10;
  _forceBank=10;
  _minAttackDive = 0;

  UpdateStopMode(unit);

  bool autopilot=false;
  if (_pilotGear && !_landContact)
  {
#if _ENABLE_WALK_ON_GEOMETRY
    float curSurfaceY=GLOB_LAND->RoadSurfaceYAboveWater(vs.Position().X(),vs.Position().Z(), Landscape::FilterIgnoreOne(this));
#else
    float curSurfaceY=GLOB_LAND->RoadSurfaceYAboveWater(vs.Position().X(),vs.Position().Z());
#endif

    if (vs.Position().Y()>curSurfaceY+5 && _rotorSpeedWanted>0.5f && vs._speed.Y()>=0)
    {
      // once we are high enough, climbing and not auto-rotating, retract gear
      _pilotGear = false;
    }
  }
  // safety measure - when on the ground, we need the gear down
  if (_landContact) _pilotGear = true;
  
  if( unit->GetState()==AIUnit::Stopping || unit->GetState()==AIUnit::Stopped )
  {
    // special handling of stop state
    // landing position is in _stopPositon

    Vector3 sPos = _stopPosition;
    if (_stopMode==SMGetIn)
      sPos[1] += 1.0f;
    else if (_stopMode==SMGetOut)
      sPos[1] += 1.5f;

    // direction - opposite to wind
    Vector3Val windDir = GLandscape->GetWind();

    float windSize = windDir.Size();

    Vector3 landDir = FutureVisualState().Direction();
    if (windSize>1.5f)
    {
      float windFactor = windSize*0.2f;
      saturate(windFactor,0,0.5f);

      landDir = -windDir*windFactor+FutureVisualState().Direction()*(1-windFactor);
      landDir[1] = 0;
      landDir.Normalize();
    }

  
    Autopilot(deltaT,sPos,sPos,VZero,landDir);
    
    if (_state>=AutopilotAlign)
    {
      _pilotGear = true;

    }
    
    speedWanted=0;

    // check if heli is already landed
    if (_landContact)
    {
      if (_stopMode==SMLand)
      {
        StopRotor();
        if( FutureVisualState()._rotorSpeed<0.7 )
        {
          UpdateStopTimeout();
          // note: Pilot may get out - Brain may be NULL
          unit->OnStepCompleted();
          if( unit->IsFreeSoldier() )
            return;
        }
      }
      if (unit->GetState()==AIUnit::Stopping && vs.Speed().SquareSize()<Square(10))
      {
        UpdateStopTimeout();
        // note: Pilot may get out - Brain may be NULL
        unit->OnStepCompleted();
        if( unit->IsFreeSoldier() )
          return;
      }
    }

    float bottomY = FutureVisualState().Position().Y()+_shape->GeometryLevel()->Min().Y();
    switch (_stopMode)
    {
      case SMLand:
        autopilot=true;
        if( _state==AutopilotReached )
        {
#if _ENABLE_WALK_ON_GEOMETRY
          float curSurfaceY=GLOB_LAND->RoadSurfaceYAboveWater(FutureVisualState().Position().X(),FutureVisualState().Position().Z(), Landscape::FilterIgnoreOne(this));          
#else
          float curSurfaceY=GLOB_LAND->RoadSurfaceYAboveWater(FutureVisualState().Position().X(),FutureVisualState().Position().Z());
#endif
          if( FutureVisualState().Position().Y()<=curSurfaceY+2.5f || bottomY<curSurfaceY+0.5f)
            StopRotor();
        }
        break;
      case SMGetIn:
      case SMGetOut:
        if (_state == AutopilotReached)
        {
          if (_landContact || FutureVisualState().Position().Y()<=sPos.Y()+0.5f)
          {
            if (unit->GetState() == AIUnit::Stopping)
            {
              UpdateStopTimeout();
              unit->OnStepCompleted();
              Assert(!unit->IsFreeSoldier());
            }
          }
        }
        autopilot=true;
        break;
    }
  }
  else if( !_landContact && _sweepTarget && Type()->_enableSweep && IsAbleToMove())
  {
    bool laserTarget = _sweepTarget->idExact->GetType()->GetNvTarget() || _sweepTarget->idExact->GetType()->GetLaserTarget();
    speedWanted=laserTarget ? 0 : Type()->GetMaxSpeedMs()*0.5f;
    Vector3 tgtPos;

    // use exact weapon aiming calculation
    // TODO: which weapons?
    TurretContext context;
    bool valid = GetPrimaryGunnerTurret(context);
    DoAssert(valid);
    float timeToLead;
    int weapon = context._weapons->ValidatedCurrentWeapon();
    if (!CalculateAimWeaponPos(context, weapon,tgtPos,timeToLead,_sweepTarget,false))
    {
      // if exact calculation failed, used approximate calculation
      float tgtDist = _sweepTarget->AimingPosition().Distance(FutureVisualState().Position());
      float tgtTime = tgtDist*(1.0f/100);
      tgtPos = _sweepTarget->AimingPosition()+_sweepTarget->GetSpeed(NULL)*tgtTime;
    }

    // when the position is not known accurately enough, search the target by circling its assumed position
    Vector3 flyToPos = tgtPos;
    
    float posAccuracy = _sweepTarget->FadingPosAccuracy();
    if (posAccuracy>25.0f)
    {
      flyToPos += unit->ScanTargetOffset(_sweepTarget);
    }
    
    Vector3 relPos = tgtPos-FutureVisualState().Position();
    float distXZ=relPos.SizeXZ();
    //const float safeDistance = laserTarget ? 400 : 250;
    const float safeDistance = laserTarget ? 600 : 400;
    if( _sweepState==SweepDisengage )
    {
      Vector3 aimDir=PositionWorldToModel(tgtPos);
      headChange=atan2(aimDir.X(),aimDir.Z());

      // move - to be hard target
      if( distXZ>safeDistance || fabs(headChange)<0.5f )
      {
        _sweepState=SweepEngage;
        _sweepDelay=Glob.time+25;
      }
      headChange=0;
    }
    else if( _sweepState==SweepEngage )
    {
      Vector3 aimDir=PositionWorldToModel(flyToPos);
      headChange=atan2(aimDir.X(),aimDir.Z());
      // reduce speed while turning - we will gain it during the sweep anyway
      // avoid slowing down too much - this would cause us climbing
      float minSpeed = FutureVisualState().ModelSpeed().Z()*0.96f;
      speedWanted = floatMax(minSpeed,speedWanted*0.5f);
      
      // move - to be hard target
      if( distXZ<100 )
      {
        _sweepState=SweepFire;
        _sweepDelay=Glob.time+5; // start immediately
        _sweepDir=relPos;
      }

      if( aimDir.Z()>20 && fabs(headChange)<0.2f )
      { // if the target is horizontally aimed, make vertical adjust
        float minSpeed=laserTarget ? Type()->GetMaxSpeedMs()*0.3f : 0;
        float speed=FutureVisualState().ModelSpeed()[2];
        if( speed>minSpeed )
        { // we are flying fast enough
          // actual aiming (using speed)
          _forceDive=(relPos.Y()-3)*relPos.InvSizeXZ();
          saturate(_forceDive,-0.7f,0);
          _minAttackDive = _forceDive;
        }
      }
    }
    else if( _sweepState==SweepFire )
    {
      // we are very close - continue flying in the last direction
      Vector3 aimDir=PositionWorldToModel(flyToPos);
      Vector3 relSweepDir=DirectionWorldToRelative(_sweepDir);
      headChange=atan2(relSweepDir.X(),relSweepDir.Z());
      if( fabs(headChange)>0.6f && ( distXZ<50 || distXZ>150 ) )
      {
        _sweepState=SweepDisengage;
        _sweepDelay=Glob.time+10; // start immediately
      } 
      else
      {
        float minSpeed=laserTarget ? Type()->GetMaxSpeedMs()*0.3f : 0;
        float speed=FutureVisualState().ModelSpeed()[2];
        if( speed>minSpeed && aimDir.Z()>20)
        { // we are flying fast enough
          // actual aiming (using speed)
          _forceDive=(relPos.Y()-3)*relPos.InvSizeXZ();
          // limit diving
          // we want to stay at least 25 m above the target
          // i.e. -relPos.Y()>=25
          float above = -relPos.Y()-25;
          float limitDive = InterpolativC(above,0,10,-0.1f,-0.7f);
          saturate(_forceDive,limitDive,0);
          _minAttackDive = _forceDive;
        }
      }
    }

    // sweep target should be slightly below
    float wantAbove=distXZ*0.1f;
    saturate(wantAbove,30,50);
    float wantY=tgtPos.Y()+wantAbove;

    float curSurfaceY=GLOB_LAND->SurfaceYAboveWaterNoWaves(FutureVisualState().Position().X(),FutureVisualState().Position().Z());
    _pilotHeight=wantY-curSurfaceY;
    
    // when firing at laser target, do not fly too high
    saturate(_pilotHeight,30,laserTarget ? 60 : 200);

    if( Glob.time>_sweepDelay )
      _sweepTarget=NULL;
  }
  else if( !isLeader )
  {
    AIBrain *leader = unit->GetFormationLeader();
    if (!EngineIsOn() && leader)
    {
      // check if we should take off
      // check if leader is airborne
      EntityAI *veh = leader->GetVehicle();
      if (veh->Airborne() || veh->FutureVisualState().Position().Distance2(FutureVisualState().Position())>Square(200))
        EngineOn();
    }
    // trivial solution always works
    unit->ForceReplan();
    _limitSpeed=GetType()->GetMaxSpeedMs()*1.5f;

    if (leader)
    {
      Assert(unit->GetUnit()); // independent unit is always formation leader
      Vector3Val relFormWanted = unit->GetUnit()->GetFormationRelative(true);

      EntityAIFull *leaderVeh = leader->GetVehicle();

      static float estT = 2.0f;
      float distance = leaderVeh->FutureVisualState().Speed().SizeXZ()*estT;
      // we need to predict position and orientation as well
      Vector3 predictedDirection = leaderVeh->FutureVisualState().Direction();
      predictedDirection[1] = 0;
      predictedDirection.Normalize();
      Vector3 predictedPosition = leaderVeh->DesiredFormationPosition()+predictedDirection*distance;
      Matrix3 formOrientEst(MUpAndDirection,VUp,predictedDirection);
      Vector3Val absFormWanted = formOrientEst*relFormWanted;
      
      Vector3 formPosEst = absFormWanted + predictedPosition;
      // formation at current position, but with estimated orientation - this helps maintaining proper speed in turns
      Vector3 formPosCur = absFormWanted + leaderVeh->FutureVisualState().Position();

      if (CHECK_DIAG(DECombat))
      {
        {
          static int handle;
          GDiagsShown.ShowArrow(handle,ID().Encode(),10,"Est",formPosEst,VUp,5.0,Color(0.8,0.25,1));
        }
        {
          static int handle;
          GDiagsShown.ShowArrow(handle,ID().Encode(),10,"Cur",formPosCur,VUp,3,Color(0,0.5,1));
        }
      }

      // use autopilot to stay in position
      Autopilot(deltaT,formPosCur,formPosEst,leaderVeh->FutureVisualState().Speed(),predictedDirection);
      autopilot=true;

      // predict leader's position
      Vector3Val leaderPos=leaderVeh->FutureVisualState().Position();
      float leaderSurfY=GLandscape->SurfaceYAboveWaterNoWaves(leaderPos.X(),leaderPos.Z());
      _pilotHeight=leaderPos.Y()-leaderSurfY;
      saturateMax(_pilotHeight,_defPilotHeight);
    }
    else
      Autopilot(deltaT,FutureVisualState().Position(),FutureVisualState().Position()+FutureVisualState().Direction(),VZero,FutureVisualState().Direction());
  }
  else
  {
    // 

    speedWanted=_limitSpeed; // go faster
    
    #if DIAG_SPEED
    if( this==GWorld->CameraOn() )
      LogF("Basic speed %.1f",speedWanted*3.6f);
    #endif

#if 1 // VBS change ported to ArmA 2
    AIGroup* grp = unit->GetGroup();
    bool isStopWaypoint = false;
    bool isLastWaypoint = false;
    int currentWaypoint = 0;
    int lastWaypoint = -1;
    if (grp)
    {
      currentWaypoint = grp->GetCurrent()->_fsm->Var(0);
      lastWaypoint = grp->NWaypoints() - 1;

      // when no more waypoints, enable navigation using move / doMove
      // when last waypoint is a cycle, there are always more waypoints
      isLastWaypoint = currentWaypoint >= lastWaypoint && (lastWaypoint<0 || grp->GetWaypoint(lastWaypoint).type!=ACCYCLE);
      isStopWaypoint = isLastWaypoint;
      if (currentWaypoint>=0 && currentWaypoint<grp->NWaypoints())
      {
        const WaypointInfo &wp = grp->GetWaypoint(currentWaypoint);
        if (wp.RequiresStop()) isStopWaypoint = true;
      }
    }
#endif

    // check path position
    const Path &path=unit->GetPath();
    if( path.Size()>=2 )
    {
      // wait until path is old enough, this prevents starting engine because of path which disappears quickly again
      // Cf. news:hf99a3$slb$2@new-server.localdomain
      if (path.GetSearchTime()<Glob.time-2)
      EngineOn();

      if (unit->GetPlanningMode() != AIBrain::LeaderDirect && 
        unit->GetPlanningMode() != AIBrain::FormationPlanned )
      {
        float precision=GetPrecision();
        //_moveMode=gotoNormal;
        float cost=path.CostAtPos(FutureVisualState().Position());
        Vector3 pos=path.PosAtCost(cost,0,FutureVisualState().Position(),cost);

        float distEnd2=FutureVisualState().Position().DistanceXZ2(path.End());
        float dist2=FutureVisualState().Position().DistanceXZ2(pos);
        // check if we have first point of Plan complete

#if _VBS3_AVRS
        if(_heliVector == Vector3(0.0f,0.0f,0.0f) || _wPPos != path.End())
        {
          _heliVector = path.End()-FutureVisualState().FutureVisualState().Position();
          _wPPos = path.End();
        }

        // this should give about 40 degree error rate when
        // approaching the waypoint. if it fails then we continue to 
        // the next one.
        if( _heliVector.CosAngle(path.End()-FutureVisualState().FutureVisualState().Position()) < 0.2f && _stopMode == SMNone && _landing == LMNone)
        {
          int pathSize = path.Size();         
          unit->OnStepCompleted();
                    
          // re-do calculation only, when path changes
          if(pathSize != path.Size())
            _heliVector = Vector3(0.0,0.0,0.0);
        }
        else
        {
#endif
          // when the waypoint will not slow us down, we may want to decrease its precision
          if(!isStopWaypoint)
          {
            //higher speed -> higher WP tolerance
            float factor = FutureVisualState().Speed().Size()/2.0f;
            saturate(factor,1.0f,10.0f);
            precision *= factor;
          }

          if( distEnd2 < Square(precision) || cost>path.EndCost() )
          {
#if _VBS3_AVRS
            // we've reached the waypoint
            _heliVector = Vector3(0.0,0.0,0.0);
#endif
            unit->OnStepCompleted();
          //        unit->ForceReplan();
          }
          if( dist2>Square(precision*3) )
            unit->OnStepTimedOut();
#if _VBS3_AVRS
        }
#endif
      }
      _pilotHeight=_defPilotHeight;
    }
    else
    {
      //_moveMode=gotoWait;
      speedWanted=0;
      saturateMin(_pilotHeight,_defPilotHeight);

      /*
      Autopilot(deltaT,_stopPosition,VZero,Direction());
      speedWanted=0;
      */
      // do not turn engine on
    }

    if (lastWaypoint >= 0 && grp)
    {
      // try to estimate position where to stop
      bool noStop = true;
      float distToStop = FLT_MAX;
      if (_stopMode != SMNone)
      {
        distToStop = _stopPosition.DistanceXZ(FutureVisualState().Position());
        noStop = false;
      }
      else if ((isLastWaypoint || _landing != LMNone) && _stratGoToPos.SquareSize() > 0.1f)
      {
        distToStop = _stratGoToPos.DistanceXZ(FutureVisualState().Position());
        noStop = false;
      }
      else if (lastWaypoint>0)
      {
        // find the next waypoint we need to stop at
        int index = lastWaypoint;
        float dist = 0;
        Vector3 curPos = FutureVisualState().Position();
        // maxIters is to handle infinite CYCLE with no stop gracefully
        for (int i=currentWaypoint,maxIters=lastWaypoint*2; i<=lastWaypoint && maxIters>0; i++,maxIters--)
        {
          Vector3 wpPos = grp->GetWaypointPosition(i);
          dist += wpPos.DistanceXZ(curPos);
          curPos = wpPos;

          const WaypointInfo &wp = grp->GetWaypoint(i);
          if (wp.type==ACCYCLE)
          {
            // start again
            i = grp->FindNearestPreviousWaypoint(i-1,wp.position)-1; // i++ will be done by the for loop
            continue;
          }
          else if (wp.RequiresStop())
          {
            index = i;
            break;
          }
        }
        noStop = grp->GetWaypoint(index).type==ACCYCLE; // we have selected the last waypoint and it is a cycle
        // TODO: compute distance along the path
        distToStop = dist;
      }

      if (!noStop)
      {
        // limit speed based on the distance of stop position
        static float timeToReach = 15.0f;
        float maxSpd = distToStop / timeToReach;
        if (CHECK_DIAG(DEPath) && GWorld->CameraOn()==this && maxSpd<Type()->GetMaxSpeedMs())
        {
          DIAG_MESSAGE(500,Format("Dist to stop: %f, maxSpeed: %.0f",distToStop, maxSpd*3.6f))
        }
        saturateMax(maxSpd, 5);
        saturate(speedWanted, -maxSpd, maxSpd);
      }
    }

    saturate(speedWanted,-_limitSpeed,+_limitSpeed); // move max. by given speed

    // when we want to rotate, avoid accelerating

    float limSpeed = Interpolativ(fabs(headChange),0,H_PI/2,Type()->GetMaxSpeedMs(),FutureVisualState().ModelSpeed().Z());

    saturateMin(speedWanted,limSpeed);
    
    if( inCombat>=0.3f && _pilotHeight>=25)
    { // when flying low, do not speed in combat
      float combatSpeed=Type()->GetMaxSpeedMs()*inCombat*0.5f;
      saturateMax(speedWanted,combatSpeed);
    }
  }

  Assert(unit);

  AvoidCollision();

  if (_stopMode==SMNone)
  {
    if (Glob.time<_pilotAvoidHigh)
      saturateMax(_pilotHeight,_pilotAvoidHighHeight);
    else
      _pilotAvoidHighHeight = 0;
    if (Glob.time<_pilotAvoidLow)
      saturateMin(_pilotHeight,floatMax(_pilotAvoidLowHeight,30));
    else
      _pilotAvoidLowHeight = 100000;
  }
  /*
  if( unit->GetState()!=AIUnit::Stopping && unit->GetState()!=AIUnit::Stopped )
  {
    EngineOn();
  }
  */
  
  if( !autopilot )
  {
    float speedSize = fabs(FutureVisualState()._modelSpeed.Z());
    float avoidGround = floatMax(0.5f, speedSize * 0.35f);
    if (avoidGround>_pilotHeight) avoidGround = _pilotHeight;

    _pilotSpeed=Vector3(0,0,speedWanted);

    if( avoidGround > 0.0f)
      AvoidGround(avoidGround);

    // if we need to decrease speed significantly, we may want to use braking maneuver

    if (FutureVisualState().ModelSpeed().Z()-speedWanted>Type()->GetMaxSpeedMs()*0.5f && speedWanted>=0 && FutureVisualState().ModelSpeed().Z()<MaxBrakeSpeed)
      BrakingManeuver();
    else
    {
      float maxTurn=H_PI;
      if( inCombat>0.1f )
      {
        const float maxTurnInCombat=H_PI*0.4f;
        const float maxTurnNoCombat=H_PI;
        maxTurn=(maxTurnInCombat-maxTurnNoCombat)*inCombat+maxTurnNoCombat;
      }
      saturate(headChange,-maxTurn,+maxTurn);
      float curHeading=atan2(FutureVisualState().Direction()[0],FutureVisualState().Direction()[2]);
      _pilotHeading=curHeading+headChange;
    }

    if (_rotorSpeedWanted<0.1f)
    {
      // when the engine is off, we may be either sitting on the ground, or the engine is malfunctioning
      // in the 2nd case we need to auto-rotate to avoid crash
      _pilotHeight = 0;
      if (!_pilotGear)
      {
        float surfY=GLOB_LAND->RoadSurfaceYAboveWater(FutureVisualState().Position());
        // once close to the ground, lower the gear
        _pilotGear = surfY+30>FutureVisualState().Position().Y();
      }
    }
  }

#if _VBS3
  AIBrain *leader = unit->GetFormationLeader();
  if (unit == leader && unit->GetForceSpeed()>=0)
    saturateMin(_pilotSpeed[2], unit->GetForceSpeed()/3.6);
#endif

#if 0
  if( this==GWorld->CameraOn() )
    LogF("InCombat %.2f wSpd %.1f pSpd %.1f spd %.1f",inCombat,speedWanted*3.6,_pilotSpeed[2]*3.6,ModelSpeed()[2]*3.6);
#endif

#if 0
    if( this==GWorld->CameraOn() && _fireMode>=0 )
      GEngine->ShowMessage(100,"Heli mode %d state %d, delay %.1f spd %.1f",_fireMode,_fireState,_fireStateDelay-Glob.time,speedWanted);
#endif

  // aim to current target
  // if( !_isDead ) AIFire(unit, deltaT);
}
#endif // _ENABLE_AI

void HelicopterAuto::DrawDiags()
{
  if (CHECK_DIAG(DECombat))
  {
  #define DRAW_OBJ(obj) GScene->DrawObject(obj)
    if (CommanderUnit())
    {
      LODShapeWithShadow *forceArrow=GScene->ForceArrow();

      if( _stopPosition.SquareSize()>0.5 )
      {
        Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
        obj->SetPosition(_stopPosition);
        obj->SetScale(2);
        obj->SetConstantColor(PackedColor(Color(1,0,1)));
        GScene->ShowObject(obj);
      }

  #if 1
        // draw pilot diags
        {
          Matrix3 rotY(MRotationY,-_pilotHeading);
          Vector3 pilotDir=rotY.Direction();
          Ref<ObjectColored> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);
          Point3 pos=RenderVisualState().Position()+Vector3(0,5,0);

          float size=0.6;
          arrow->SetPosition(pos);
          arrow->SetOrient(pilotDir,VUp);
          arrow->SetPosition
          (
            arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size)
          );
          arrow->SetScale(size);
          arrow->SetConstantColor(PackedColor(Color(1,1,0)));

          DRAW_OBJ(arrow);
        }
  #endif
    }
  }
  base::DrawDiags();
}

RString HelicopterAuto::DiagText() const
{
  RString text=base::DiagText();
  char buf[256];
  sprintf(buf," pSpd=%.1f, MRW=%.2f, ",_pilotSpeed[2]*3.6,_mainRotorWanted);
  if (_state!=AutopilotNear)
    text = text + FindEnumName(_state);
  return text+RString(buf);
}

void Helicopter::GetAimActions(ValueWithCurve &aimX, ValueWithCurve &aimY) const
{
  aimX = GInput.GetActionWithCurve(UAAimRight, UAAimLeft)*Input::MouseRangeFactor;
  aimY = GInput.GetActionWithCurve(UAAimUp, UAAimDown)*Input::MouseRangeFactor;
}

UserAction Helicopter::GetManualFireAction() const
{
  return UAHeliManualFire;
}


#define SERIAL_DEF_VISUALSTATE(name, value) CHECK(ar.Serialize(#name, FutureVisualState()._##name, 1, value))

LSError Helicopter::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  // TODO: serialize other members

  if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
  {
    SERIAL_DEF_VISUALSTATE(cyclicForward, 0);
    SERIAL_DEF_VISUALSTATE(cyclicAside, 0);
    SERIAL_DEF_VISUALSTATE(rotorPosition, 0);
    SERIAL_DEF_VISUALSTATE(rotorDive, 0);
    SERIAL_DEF_VISUALSTATE(gearsUp, 0);
    SERIAL_DEF_VISUALSTATE(rotorSpeed,1);SERIAL_DEF(rotorSpeedWanted,1);
    SERIAL_DEF(backRotor,0);SERIAL_DEF(backRotorWanted,0);
    SERIAL_DEF_VISUALSTATE(mainRotor,0);SERIAL_DEF(mainRotorWanted,0);
    SERIAL_DEF(cyclicForwardWanted,0);
    SERIAL_DEF(cyclicAsideWanted,0);
    SERIAL_DEF(rotorDiveWanted,0);
    SERIAL_DEF(pilotGear, true);
  }
/*
  SERIAL_DEF(gunYRot,0);SERIAL_DEF(gunYRotWanted,0);
  SERIAL_DEF(gunXRot,0);SERIAL_DEF(gunXRotWanted,0);
  SERIAL_DEF(gunXSpeed,0);SERIAL_DEF(gunYSpeed,0);
*/

  return LSOK;
}

#undef SERIAL_DEF_VISUALSTATE

static const EnumName AutopilotStateNames[]=
{
  EnumName(AutopilotFar, "FAR"),
  EnumName(AutopilotBrake, "BRAKE"),
  EnumName(AutopilotNear, "NEAR"),
  EnumName(AutopilotAlign, "ALIGN"),
  EnumName(AutopilotReached, "REACHED"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AutopilotState dummy)
{
  return AutopilotStateNames;
}

#pragma warning(disable:4060)

bool HelicopterAuto::GetActionParams(UIActionParams &params, const Action *action, AIBrain *unit) const
{
  switch (action->GetType())
  {
    #ifndef _XBOX
    case ATAutoHover:
    case ATAutoHoverCancel:
      // No parameters
      return true;
    #endif
  }
  return base::GetActionParams(params, action, unit);
}

void HelicopterAuto::PerformAction(const Action *action, AIBrain *unit)
{
  switch (action->GetType())
  {
    #ifndef _XBOX
    case ATAutoHover:
      AutohoverOn();
      return;
    case ATAutoHoverCancel:
      _hoveringAutopilot = false;
      return;
    #endif
    default:
      base::PerformAction(action,unit);
      break;
  }
}

void HelicopterAuto::GetActions(UIActions &actions, AIBrain *unit, bool now, bool strict)
{
  #ifndef _XBOX
  if (unit && unit==DriverBrain() && QIsManual())
  {
    UIAction action(new ActionBasic(_hoveringAutopilot ? ATAutoHoverCancel : ATAutoHover, this));
    actions.Add(action);
  }
  #endif

  base::GetActions(actions, unit, now, strict);
}

bool HelicopterAuto::IsStopped() const
{
  // check manual heli stopped
  // check height above surface
  // caution: not every object contact means we are stopped
  if (_landContact)
  {
    return base::IsStopped();
  }

  // high flying heli cannot be stopped
#if _ENABLE_WALK_ON_GEOMETRY
  float surfaceY=GLOB_LAND->RoadSurfaceYAboveWater(FutureVisualState().Position(), Landscape::FilterIgnoreOne(this));
#else
  float surfaceY=GLOB_LAND->RoadSurfaceYAboveWater(FutureVisualState().Position());
#endif
  float height = FutureVisualState().Position().Y()-surfaceY;
  if (height>5) return false;

  return base::IsStopped();
}

LSError HelicopterAuto::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
  {
    CHECK(ar.Serialize("defPilotHeight", _defPilotHeight, 1, 50))
    CHECK(ar.Serialize("_pilotHeight", _pilotHeight, 1, 2.5))
    CHECK(::Serialize(ar, "_pilotSpeed", _pilotSpeed, 1, VZero))
    CHECK(::Serialize(ar, "_stopPosition", _stopPosition, 1, VZero))
    CHECK(ar.SerializeEnum("stopMode", _stopMode, 1, SMNone))
    CHECK(ar.SerializeEnum("stopResult", _stopResult, 1, SPNotReady))
    CHECK(ar.SerializeEnum("state", _state, 1, AutopilotNear))
    CHECK(ar.Serialize("hoveringAutopilot", _hoveringAutopilot, 1, false))
  }
  // TODO: serialize other members

  return LSOK;
}

#define HELICOPTER_MSG_LIST(XX) \
  XX(Create, CreateHelicopter) \
  XX(UpdateGeneric, UpdateHelicopter) \
  XX(UpdatePosition, UpdatePositionHelicopter)

DEFINE_NETWORK_OBJECT(HelicopterAuto, base, HELICOPTER_MSG_LIST)

#define CREATE_HELICOPTER_MSG(MessageName, XX) \
  XX(MessageName, float, rotorSpeed, NDTFloat, float, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("Initial rotor speed"), TRANSF)

DECLARE_NET_INDICES_EX(CreateHelicopter, CreateVehicle, CREATE_HELICOPTER_MSG)
DEFINE_NET_INDICES_EX(CreateHelicopter, CreateVehicle, CREATE_HELICOPTER_MSG)

#define UPDATE_HELICOPTER_MSG(MessageName, XX) \
  XX(MessageName, float, rotorSpeedWanted, NDTFloat, float, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("Wanted rotor speed"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, int, state, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Autopilot state"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, float, pilotHeight, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Height, wanted by pilot"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, Vector3, pilotSpeed, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Speed, wanted by pilot"), TRANSF, ET_ABS_DIF, 1) \
  XX(MessageName, int, stopMode, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Landing type"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, stopResult, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, PlaneOrHeli::SPNotReady), DOC_MSG("Landing result"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, Vector3, stopPosition, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Landing position"), TRANSF, ET_ABS_DIF, 0.1) \
  XX(MessageName, bool, pilotSpeedHelper, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("pilotSpeed is valid"), TRANSF, ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, bool, pilotHeightHelper, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("pilotHeight is valid"), TRANSF, ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, bool, pilotDirHelper, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("pilotHeading is valid"), TRANSF, ET_NOT_EQUAL, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, bool, pilotGear, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Position of gear, wanted by pilot"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, bool, hoveringAutopilot, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Hovering autopilot on/off toggle"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE)

DECLARE_NET_INDICES_EX_ERR(UpdateHelicopter, UpdateTransport, UPDATE_HELICOPTER_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateHelicopter, UpdateTransport, UPDATE_HELICOPTER_MSG, NoErrorInitialFunc)

#define UPDATE_POSITION_HELICOPTER_MSG(MessageName, XX) \
  XX(MessageName, float, backRotorWanted, NDTFloat, float, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted back rotor control"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, float, mainRotorWanted, NDTFloat, float, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted main rotor control"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, float, cyclicForwardWanted, NDTFloat, float, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted forward cyclic position"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, float, cyclicAsideWanted, NDTFloat, float, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted aside cyclic position"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, float, rotorDiveWanted, NDTFloat, float, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted rotor dive"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, float, bankWanted, NDTFloat, float, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted bank position"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, float, diveWanted, NDTFloat, float, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Wanted dive"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, float, pilotHeading, NDTFloat, float, NCTFloatAngle, DEFVALUE(float, 0), DOC_MSG("Heading, wanted by pilot"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, float, pilotDive, NDTFloat, float, NCTFloatM1ToP1, DEFVALUE(float, 0), DOC_MSG("Dive, wanted by pilot"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR)

DECLARE_NET_INDICES_EX_ERR(UpdatePositionHelicopter, UpdatePositionVehicle, UPDATE_POSITION_HELICOPTER_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdatePositionHelicopter, UpdatePositionVehicle, UPDATE_POSITION_HELICOPTER_MSG, NoErrorInitialFunc)

NetworkMessageFormat &HelicopterAuto::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  switch (cls)
  {
  case NMCCreate:
    base::CreateFormat(cls, format);
    CREATE_HELICOPTER_MSG(CreateHelicopter, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    base::CreateFormat(cls, format);
    UPDATE_HELICOPTER_MSG(UpdateHelicopter, MSG_FORMAT_ERR)
    break;
  case NMCUpdatePosition:
    base::CreateFormat(cls, format);
    UPDATE_POSITION_HELICOPTER_MSG(UpdatePositionHelicopter, MSG_FORMAT_ERR)
    break;
  default:
    base::CreateFormat(cls, format);
    break;
  }
  return format;
}

HelicopterAuto *HelicopterAuto::CreateObject(NetworkMessageContext &ctx)
{
  Entity *veh = Entity::CreateObject(ctx);
  HelicopterAuto *heli = dyn_cast<HelicopterAuto>(veh);
  if (!heli) return NULL;
  heli->TransferMsg(ctx);
  return heli;
}

TMError HelicopterAuto::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    if (ctx.IsSending())
    {
      TMCHECK(base::TransferMsg(ctx))
    }
    {
      PREPARE_TRANSFER(CreateHelicopter)
      TRANSF_EX(rotorSpeed, FutureVisualState()._rotorSpeed)
    }
    break;
  case NMCUpdateGeneric:
    TMCHECK(base::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateHelicopter)

      TRANSF(rotorSpeedWanted)

      TRANSF_ENUM(state)
      TRANSF(pilotHeight)
      TRANSF(pilotSpeed)
      TRANSF_ENUM(stopMode)
      TRANSF_ENUM(stopResult)
      TRANSF(stopPosition)
      TRANSF(pilotSpeedHelper)
      TRANSF(pilotHeightHelper)
      TRANSF(pilotDirHelper)
      TRANSF(pilotGear)
      TRANSF(hoveringAutopilot)
      
      if (!ctx.IsSending() && (ctx.GetInitialUpdate() || GetNetworkManager().GetClientState()<NCSGameLoaded))
        FutureVisualState()._rotorSpeed = _rotorSpeedWanted;
    }
    break;
  case NMCUpdatePosition:
    {
      PREPARE_TRANSFER(UpdatePositionHelicopter)

      Matrix3 oldTrans = FutureVisualState().Orientation();
      TMCHECK(base::TransferMsg(ctx))
      StabilizeTurrets(oldTrans, FutureVisualState().Orientation());
      TRANSF(backRotorWanted)
      TRANSF(mainRotorWanted)
      TRANSF(cyclicForwardWanted)
      TRANSF(cyclicAsideWanted)
      TRANSF(rotorDiveWanted)
      TRANSF(bankWanted)
      TRANSF(diveWanted)
      TRANSF(pilotHeading)
      TRANSF(pilotDive)
      if (!ctx.IsSending() && (ctx.GetInitialUpdate() || GetNetworkManager().GetClientState()<NCSGameLoaded))
        FutureVisualState()._mainRotor = _mainRotorWanted;
    }
    break;
  default:
    return base::TransferMsg(ctx);
  }
  return TMOK;
}

#define MAKE_FINITE(x) \
  if (!_finite(x)) \
  { \
    RptF("%s(%d) : Infinite value %s==%g",__FILE__,__LINE__,#x,x); \
    x = 1000; \
  }


float HelicopterAuto::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = 0;
  switch (ctx.GetClass())
  {
  case NMCUpdateGeneric:
    error += base::CalculateError(ctx);
    {
      PREPARE_TRANSFER(UpdateHelicopter)

      ICALCERR_ABSDIF(float, rotorSpeedWanted, ERR_COEF_VALUE_MAJOR)
      ICALCERR_NEQ(int, state, ERR_COEF_MODE)
      ICALCERR_ABSDIF(float, pilotHeight, ERR_COEF_VALUE_MAJOR)
      ICALCERR_DIST(pilotSpeed, 1)
      ICALCERR_NEQ(int, stopMode, ERR_COEF_MODE)
      ICALCERR_NEQ(int, stopResult, ERR_COEF_MODE)
      ICALCERR_DIST(stopPosition, 0.1)

      ICALCERR_NEQ(bool, pilotSpeedHelper, ERR_COEF_VALUE_MAJOR)
      ICALCERR_NEQ(bool, pilotHeightHelper, ERR_COEF_VALUE_MAJOR)
      ICALCERR_NEQ(bool, pilotDirHelper, ERR_COEF_VALUE_MAJOR)
      ICALCERR_NEQ(bool, pilotGear, ERR_COEF_MODE)

      ICALCERR_NEQ(bool, hoveringAutopilot, ERR_COEF_MODE)
    }
    break;
  case NMCUpdatePosition:
    {
      error +=  base::CalculateError(ctx);
      PREPARE_TRANSFER(UpdatePositionHelicopter)

      MAKE_FINITE(_backRotorWanted)
      MAKE_FINITE(_mainRotorWanted)
      MAKE_FINITE(_cyclicForwardWanted)
      MAKE_FINITE(_cyclicAsideWanted)
      MAKE_FINITE(_bankWanted)
      MAKE_FINITE(_diveWanted)
      MAKE_FINITE(_pilotHeading)
      MAKE_FINITE(_pilotDive)

      ICALCERR_ABSDIF(float, backRotorWanted, ERR_COEF_VALUE_MAJOR)
      ICALCERR_ABSDIF(float, mainRotorWanted, ERR_COEF_VALUE_MAJOR)
      ICALCERR_ABSDIF(float, cyclicForwardWanted, ERR_COEF_VALUE_MAJOR)
      ICALCERR_ABSDIF(float, cyclicAsideWanted, ERR_COEF_VALUE_MAJOR)
      ICALCERR_ABSDIF(float, rotorDiveWanted, ERR_COEF_VALUE_MAJOR)
      ICALCERR_ABSDIF(float, bankWanted, ERR_COEF_VALUE_MAJOR)
      ICALCERR_ABSDIF(float, diveWanted, ERR_COEF_VALUE_MAJOR)

      ICALCERR_ABSDIF(float, pilotHeading, ERR_COEF_VALUE_MAJOR)
      ICALCERR_ABSDIF(float, pilotDive, ERR_COEF_VALUE_MAJOR)
    }
    break;
  default:
    error += base::CalculateError(ctx);
    break;
  }
  DoAssert(_finite(error));
  return error;
}
