#include <macros.h>
#include <string.h>
#include <stdlib.h>
#include <stdc\mempool.h>
#include <stdc\hledstrm.h>
#include "cache.h"

#define TEST 0

#define BPBVec ( *(BPBProc **)0x472 )
#define RWAVec ( *(RWAProc **)0x476 )
#define MChVec ( *(MChProc **)0x47e )

#define _drvbits ( *(long *)0x4c2 )

#ifdef __DEBUG
	void DebugError( int Line, const char *File )
	{
		(void)Line,(void)File;
		for(;;);
	}
#endif

static void InstVec( void )
{
	void *SSP=(void *)Super(0);
	obpblk=BPBVec,BPBVec=bpblk;
	orwabs=RWAVec,RWAVec=rwabs;
	omchng=MChVec,MChVec=mchng;
	Super(SSP);
}

static void DeinstVec( void )
{
	void *SSP=(void *)Super(0);
	BPBVec=obpblk;
	RWAVec=orwabs;
	MChVec=omchng;
	Super(SSP);
}

#if TEST
static void TestFunc( void )
{
	Dsetdrv(4);
	Dsetpath("E:\\PURE_C");
	{
		static char Env[]={0,0};
		static COMMAND Com={0,""};
		Pexec(0,"E:\\PURE_C\\PC.PRG",&Com,Env);
	}
}
#endif

CInfo CIS[MaxDrv];
long CMaxVel;
int PreRead;

static CaVazT CaVaz;

static void ZacLog( const char *EDrvs, long KB )
{ /* v EDrv seznam cachovanych drivu */
	int ND;
	long DM=Drvmap();
	CMaxVel=KB<<10;
	PreRead=16;
	CaVaz.Magic=CMagic;
	CaVaz.Buff=&Buff;
	CaVaz.Stat=&Stat;
	CaVaz.Pool=&Pool;
	for( ND=0; ND<MaxDrv; ND++ )
	{
		CInfo *c=&CIS[ND];
		c->CaVaz=&CaVaz;
		c->Cachuj=( DM&(1<<ND) ) && strchr(EDrvs,'A'+ND);
		c->NoMedia=True;
	}
}

static void ZacFyz( void )
{
	int ND;
	for( ND=0; ND<MaxDrv; ND++ )
	{
		CInfo *c=&CIS[ND];
		if( c->Cachuj )
		{
			long r;
			do
			{
				if( ND!=1 ) r=Mediach(ND);
				if( ND<2 ) r=0; /* diskety nezkousej znovu */
			} while( r<0 );
		}
	}
}

int main()
{
	DTA d;
	char EDrvs[32];
	long KB;
	long Fre=(long)Malloc(-1);
	DTA *od=Fgetdta();
	Fsetdta(&d);
	if
	(
		Fsfirst("C_*.PRG",FA_READONLY|FA_HIDDEN)>=0 ||
		Fsfirst("C_*.TOS",FA_READONLY|FA_HIDDEN)>=0 ||
		Fsfirst("AUTO\\C_*.PRG",FA_READONLY|FA_HIDDEN)>=0 ||
		Fsfirst("\\AUTO\\C_*.PRG",FA_READONLY|FA_HIDDEN)>=0
	)
	{
		char f[16];
		char *e;
		strcpy(f,&d.d_fname[2]);
		e=strchr(f,'.');
		if( e ) *e=0;
		KB=strtoul(f,&e,10)*1024/100;
		if( e ) strcpy(EDrvs,e);
		else strcpy(EDrvs,"A");
		Fre-=64*1024;
		Fre>>=4;
		Fre*=15;
		Fre>>=10;
		if( KB>Fre ) KB=Fre;
	}
	else
	{
		strcpy(EDrvs,"A");
		KB=Fre>>12;
		if( KB>1024 ) KB=(KB-1024)/4+1024;
		if( KB>2*1024 ) KB=(KB-2*1024)/4+2*1024;
	}
	Fsetdta(od);
	Cconws("Cache, � 1995, SUMA\r\n");
	Cconws("Cache drives ");
	Cconws(EDrvs);
	{
		char Buf[128];
		ultoa(KB,Buf,10);
		Cconws("; ");
		Cconws(Buf);
		Cconws(" KB\r\n\r\n");
	}
	ZacLog(EDrvs,KB);
	if( ZacStromy(CMaxVel)>=0 )
	{
		if( ZacStat()>=0 )
		{
			if( ZacBuff()>=0 )
			{
				InstVec();
				ZacFyz(); /* inic. co jde inic. */
				#if TEST
					TestFunc();
				#else
					Ptermres(_PgmSize,0);
				#endif
				/* sem normalne vubec nedojde */
				DeinstVec();
				KonBuff();
			}
			KonStat();
		}
		KonStromy();
	}
	return 0;
}
