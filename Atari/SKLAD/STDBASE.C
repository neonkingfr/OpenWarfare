/* DEPOZIT - operace spec. pro Atari ST */
/* SUMA 7/1993-5/1994 */

#include <macros.h>
#include <stdio.h>
#ifdef __MSDOS__
	#include <sys\stat.h>
	#include <io.h>
	#include <fcntl.h>
	#include <share.h>
#endif
#include <string.h>
#include <stdc\fileutil.h>
#include <sttos\narod.h>
#include "dbase\d4all.h"

void LogF( const char *f, ... );

/* impl. SWAPINT fci. */

#if defined __TOS__ && !defined __MSDOS__
void SwapInt( void *b )
{
	byte *B=b;
	byte p;
	p=B[0],B[0]=B[1],B[1]=p;
}
void SwapLong( void *b )
{
	byte *B=b;
	byte p;
	p=B[0],B[0]=B[3],B[3]=p;
	p=B[1],B[1]=B[2],B[2]=p;
}

void WriteDateSwap( char *Buf, const char *yy, unsigned int len )
{
	Plati( len==7 );
	memcpy(Buf,yy,len);
	SwapLong(&Buf[3]);
}

typedef  struct  base_header_st
{
   char version ;
   char yy ;
   char mm ;
   char dd ;
   long num_recs ;
   unsigned short  header_len ;
   unsigned short  record_width ;
   char filler[20] ;
}  BASE_HEADER ;

int ReadHeaderSwap( int Handle, char *head, unsigned int h_len )
{
	BASE_HEADER *h=(void *)head;
	Plati( h_len==4+4+2+2 );
	Plati( h->version==3 );
	if( h->header_len%0x20==1 && h->header_len/0x20<0x80 )
	{
		/* netreba swapovat - tzn. - na disku brajgl */
		BASE_HEADER H;
		memcpy(&H,h,h_len);
		SwapInt(&H.header_len);
		SwapInt(&H.record_width);
		SwapLong(&H.num_recs);
		lseek(Handle,0,0);
		return (int)write(Handle,&H,h_len);
	}
	SwapInt(&h->header_len);
	SwapInt(&h->record_width);
	SwapLong(&h->num_recs);
	Plati( h->header_len%0x20==1 && h->header_len/0x20<0x80 );
	return h_len;
}

#pragma warn -par
void ReadFieldsSwap( char *buffer, unsigned int h_len)
{
	/* ve fieldu je vsechno po bytech */
	/* jen C - fields maji v decimal horni byte delky - to nam ovsem nevadi */
}
#pragma warn .par

void WriteHeadSwap( char *head, unsigned int h_len )
{
	BASE_HEADER *h=(void *)head;
	Plati( h_len>sizeof(*h) );
	SwapInt(&h->header_len);
	SwapInt(&h->record_width);
	SwapLong(&h->num_recs);
}

#endif

/* ------- */

/* implementace jaz. zavislych operaci */

extern CmpPar CesCmp;

int u4memcmp( char *S, char *s, size_t len )
{
	return WordCmp(S,s,len,len,&CesCmp,True);
	/* lexikograficke usporadani - nemohou prebyvat mezery ani zavorky ... */
}
char *u4upper( char *s )
{
	char *S;
	for( S=s; *S; S++ ) *S=ToUpper(*S);
	return s;
}
char *u4lower( char *s )
{
	char *S;
	for( S=s; *S; S++ ) *S=ToLower(*S);
	return s;
}
int u4toupper( int c ) {return ToUpper(c);}

int u4remove( const char *N )
{
	return remove(N);
}

#ifndef __MSDOS__
long filelength( int handle )
{
	size_t APos=lseek(handle,0,SEEK_CUR);
	size_t LPos=lseek(handle,0,SEEK_END);
	lseek(handle,APos,SEEK_SET);
	return LPos;
}
#endif

int iexist( const char *S )
{
	PathName P;
	int h;
	strcpy(P,S);
	#ifndef __MSDOS__
	  u4name_full( P, S, ".DIX" ) ;  
	#else
	  u4name_full( P, S, ".NDX" ) ;  
	#endif
  h=u4open(P,0);
	if( h>=0 )
	{
		close(h);
		return 0;
	}
	return -1;
}

Flag CmpFields( int N, FIELD *nf )
{
	int numf=f4num_fields();
	int n;
	FIELD *f=d4fields();
	if( numf!=N ) return True;
	while( --N>=0 )
	{
		FIELD *A=&nf[N];
		Flag JeF=False;
		for( n=0; n<numf; n++ )
		{
			FIELD *a=&f[N];
			if
			(
				!strcmp(A->name,a->name) && A->type==a->type &&
				A->width==a->width && A->decimals==a->decimals
			) JeF=True;
		}
		if( !JeF ) return True;
	}
	return False;
}

void CopyRecord( int nh, int oh )
{
	FIELD *of;
	int numf;
	int indf;
	d4select(oh);
	of=d4fields();
	numf=f4num_fields();
	for( indf=0; indf<numf; indf++ )
	{
		const char *NamF=of[indf].name;
		const char *OS;
		/* stary i novy ma urcite stejnou delku */
		long nr,or;
		d4select(nh);nr=f4ref(NamF);
		d4select(oh);or=f4ref(NamF);
		if( nr>=0 && or>=0 )
		{
			OS=f4str(or);
			f4r_str(nr,OS);
		}
	}
}

static const char PracSoub[]="DTB\\DEP_PRAC.DBF";

int ReformatDat( const char *ON, int N, FIELD *nf )
{
	int ret=-1;
	int oh;
	#if __DEBUG
	LogF("ReformatDat: %s",ON);
	#endif
	oh=d4use_excl(ON);
	#if __DEBUG
	LogF("ReformatDat: Opened");
	#endif
	if( oh>=0 )
	{
		if( CmpFields(N,nf) )
		{ /* je nutno preformatovat */
			int nh;
			#if __DEBUG
			LogF("ReformatDat: Different");
			#endif
			nh=d4create(PracSoub,N,nf,1);
			if( nh>=0 )
			{
				if( !d4lock(-1,1) )
				{
					long nr;
					long ni;
					d4select(oh);nr=d4reccount();
					for( ni=1; ni<=nr; ni++ )
					{
						d4select(oh);if( d4go(ni)<0 ) goto Error;
						d4select(nh);if( d4go(-1)<0 ) goto Error;
						CopyRecord(nh,oh);
						d4select(nh);if( d4append()<0 ) goto Error;
					}
					ret=0;
				}
				Error:
				d4select(nh);
				d4close();
			}
		}
		else ret=+1;
		d4select(oh);
		d4close();
		if( ret==0 ) /* kladny - zadna zmena */
		{
			PathName PON;
			u4name_full(PON,ON,".DBF");
			if( remove(PON)<0 || rename(PracSoub,PON)<0 ) ret=-1;
		}
		ef( ret<0 ) remove(PracSoub);
	}
	#if __DEBUG
	LogF("ReformatDat: Kon");
	#endif
	return ret;
}

int DPack( const char *N, const char *Indy[], const char *Sort, int (*filter)( void ) )
{
	int Dtb;
	int ret=-1;
	#if __DEBUG
	LogF("DPack: %s",N);
	#endif
	Dtb=d4use_excl(N); /* pracuj na tom sam */
	if( Dtb>=0 )
	{
		int nb;
		while( *Indy )
		{
			PathName I;
			#ifndef __MSDOS__
				u4name_full(I,*Indy,".DIX");
			#else
				u4name_full(I,*Indy,".NDX");
			#endif
			remove(I);
			Indy++;
		}
		x4filter(filter);
		nb=x4sort(PracSoub,Sort,1,1);
		if( nb>=0 )
		{
			d4select(nb);
			if( d4close()==0 ) ret=0;
		}
		d4select(Dtb);
		if( d4close()!=0 ) ret=-1;
		#if __DEBUG
		LogF("DPack: DBase sorted");
		#endif
		if( ret==0 )
		{
			PathName Name;
			char *NE;
			strcpy(Name,N);
			NE=NajdiExt(NajdiNazev(Name));
			if( *NE!='.' ) strcpy(NE,".DBF");
			if( remove(Name)<0 || rename(PracSoub,Name)<0 ) ret=-1;
		}
		else
		{
			remove(PracSoub);
		}
	}
	#if __DEBUG
	LogF("DPack: %s ret=%d",N,ret);
	#endif
	return ret;
}
