
/* (c)Copyright Sequiter Software Inc., 1987-1990.  All rights reserved.
	u4huge_a.c
	u4huge.c
*/

#include "p4misc.h"
#include "d4all.h"
#include "u4error.h"

#include <string.h>
#ifndef UNIX
#ifdef STTOS
#include <stdio.h>
#else
#include <io.h>
#endif
#endif

#ifdef STTOS
int  u4huge_write( int hand, char H_PTR ptr, long len_ptr )
{
	return write(hand,ptr,len_ptr)==len_ptr ? 0 : -1;
}
#else

#ifdef NO_HUGE
#include <stdlib.h>
#else
#ifdef TURBO
#include <dos.h>
#include <alloc.h>
#else
#include <malloc.h>
#endif
#endif

#ifdef UNIX
/* The following switch can be used only if an integer is 4 bytes */
/* #define MAX_ALLOC 0x7FFFFFFFL */
#endif
#ifdef STTOS
	#define MAX_ALLOC 0x7FFFFFFFL
#endif

#ifndef MAX_ALLOC
#ifdef IS_386
#define MAX_ALLOC 0x7FFFFFFFL
#else
#ifdef NO_HUGE
#define MAX_ALLOC  0xFFE0
#endif
#endif
#endif

void  u4huge_set( char H_PTR ptr, int val, long len )
{
   long  i ;

   for ( i=0; i< len; i++)
      ptr[i] = (char) val ;
}

void  u4huge_cpy( void H_PTR dest, void H_PTR sou, long bytes )
{
   for ( ;; )
   {
      if ( bytes <= 0xFFFF )
      {
	 memcpy( (char *) dest, (char *) sou, (unsigned) bytes ) ;
         return ;
      }

      memcpy( (char *) dest, (char *) sou, (size_t) 0xFFFF ) ;
      dest   =  (char H_PTR) dest +  0xFFFF ;
      sou    =  (char H_PTR) sou  +  0xFFFF ;
      bytes -=  0xFFFF ;
   }
}

int  u4huge_cmp( unsigned char H_PTR a, unsigned char H_PTR b, unsigned len )
{
	 #ifdef LANGUAGE
			return u4memcmp((unsigned char far *)a,(unsigned char far *)b,len);
	 #else
      return( memcmp( (char *) a, (char *) b, (size_t) len ) ) ;
   #endif
}

#ifdef OS2

void H_PTR u4huge_norm( void H_PTR ptr )
{
   return( ptr ) ;
}

/* u4huge_read returns the number of bytes read. 
   CLH Memory Models only. */

long  u4huge_read( int hand, char H_PTR ptr, long len_ptr )  
{
   long          tot, left_seg ;
   unsigned int  u_rc ;

   tot = 0L ;

   for(;;)
   {
      left_seg =  0x10000L - *((unsigned *)&(ptr)) ;
      if ( (long) left_seg > len_ptr )  left_seg =  len_ptr ;
      if ( left_seg == 0L )   return( tot ) ;
      if ( left_seg > 0xFFFEL )  left_seg =  0xFC00L ;

      u_rc =  read(hand, ptr, (unsigned) left_seg ) ;
      if ( u_rc == 0xFFFF )
      {
         u4error( E_READ, (char *) 0 ) ;
	 return -1L ;
      }

      tot +=  u_rc ;
      if ( (long) u_rc < left_seg )  return( tot ) ;

      ptr     +=  u_rc ;
      len_ptr -=  u_rc ;
   }
}

/* CLH Memory Models only. */
int  u4huge_write( int hand, char H_PTR ptr, long len_ptr ) 
{
   unsigned int  u_rc ;
   long          left_seg ;

   for(;;)
   {
      left_seg = 0x10000L - (long) *((unsigned *)&(ptr)) ;
      if ( (long) left_seg > len_ptr )  left_seg =  (unsigned) len_ptr ;
      if ( left_seg == 0 )  return 0 ;

      if ( left_seg > 0xFFFE )  left_seg =  0xFC00 ;

      u_rc =  write(hand, ptr, (unsigned) left_seg ) ;
      if ( u_rc != (unsigned) left_seg )
      {
         u4error( E_READ, (char *) 0 ) ;
	 return -1 ;
      }

      ptr     +=  u_rc ;
      len_ptr -=  u_rc ;
   }
}

#else

/* Normalize the pointer so that the offset is as small as possible. */

void H_PTR u4huge_norm( void H_PTR ptr )
{
   #ifdef IS_386
      return( ptr )  ;
   #else
      #ifdef NO_HUGE
	 return( ptr ) ;
      #else
	 static   union
	 {
	    void H_PTR far_ptr ;
	    unsigned  off_seg[2] ; /* off_seg[0] is offset, off_seg[1] is segment */
	 }  memory_ptr ;

	 memory_ptr.far_ptr =  ptr ;
	 memory_ptr.off_seg[1] +=  memory_ptr.off_seg[0] >> 4 ;
	 memory_ptr.off_seg[0] &=  0xF ;

	 return( memory_ptr.far_ptr ) ;
      #endif
   #endif
}

/* DOS  u4huge_read and u4huge_write.
   CLH Memory Models only. */

long  u4huge_read( int hand, char H_PTR ptr, long len_ptr )  
{
   long          tot ;
   unsigned int  u_rc ;

   tot = 0L ;

   while ( len_ptr > 0xFFF0L )
   {
      ptr =  (char H_PTR) u4huge_norm( ptr ) ;
			u_rc =  (int)read(hand, (char *) ptr, 0xFFF0) ;
      tot  += u_rc ;

      if ( u_rc != 0xFFF0 )
      {
	 if ( u_rc == 0xFFFF )
	 {
  	    u4error( E_READ, (char *) 0 ) ;
	    return -1 ;
	 }
	 return( tot ) ;
      }

			ptr      = (char *) ptr + (unsigned)0xFFF0L ;
      len_ptr -= 0xFFF0L ;
   }

   ptr =  (char H_PTR) u4huge_norm( ptr ) ;
   u_rc = (int)read(hand, (char *) ptr, (unsigned int) len_ptr) ;
   if ( u_rc == 0xFFFF )
   {
      u4error( E_READ, (char *) 0 ) ;
      return -1L ;
   }

   return( tot+u_rc ) ;
}

/* CLH Memory Models only. */
int  u4huge_write( int hand, char H_PTR ptr, long len_ptr )  
{
   while ( len_ptr > 0xFFF0L)
   {
      ptr =  (char H_PTR) u4huge_norm( ptr ) ;
      if ( (unsigned int) write( hand, (char *) ptr, 0xFFF0) != 0xFFF0)
      {
	 u4error( E_WRITE, (char *) 0 ) ;
	 return -1 ;
      }

			ptr      = (char *) ptr + (unsigned)0xFFF0L ;
      len_ptr -= 0xFFF0L ;
   }

   ptr =  (char H_PTR) u4huge_norm( ptr ) ;
   if ( (unsigned int) write(hand, (char *) ptr, (unsigned int) len_ptr) !=
                                        (unsigned int) len_ptr)
   {
      u4error( E_WRITE, (char *) 0 ) ;
      return -1 ;
   }

   return 0 ;
}


void H_PTR  u4huge_alloc( long num )
{
   #ifdef NO_HUGE
      if ( num > MAX_ALLOC )
	 return( (void H_PTR) 0 ) ;

      return( h4alloc_try( (unsigned) num) ) ;
   #else
      #ifdef TURBO
	 if ( num >= 700000L )
	    return( (void H_PTR) 0 ) ;
	 return( (void H_PTR) farmalloc((unsigned long) num) ) ;
      #else
	 return( halloc( num, 1 ) ) ;
      #endif
   #endif
}


void u4huge_free( void H_PTR ptr )
{
   #ifdef NO_HUGE
      h4free_memory( ptr ) ;
   #else
      #ifdef TURBO
	 farfree( (void far *) ptr ) ;
      #else
	 hfree(ptr) ;
      #endif
   #endif
}

#endif
#endif