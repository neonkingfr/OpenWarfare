/* skladove hospodarstvi */
/* spojeni - komplety */
/* SUMA 5/1994-5/1994 */

#include <macros.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <ext.h>
#include <stdc\fileutil.h>
#include "dbase\d4all.h"
#include "ramsklad.h"

/* databaze spojeni */

static Flag SpojOtevrene=False;
static int SpojDtb,SpojIdx,SKdoIdx;
#define NSpoj "KOMPLETY"
static char SpojeniN[]="DTB\\"NSpoj;
static char KdoSpN[]="DTB\\KOMPLKDO";

static FIELD SpojForm[]=
{
	"KEKOMU",  'C',DelNazvu,0,0, /* k jakemu zbozi */
	"KDO",     'C',DelNazvu,0,0, /* kdo */
};

static long RKeKomu;
static long RKdo;

static void InitSpoj( void )
{
	d4select(SpojDtb);
	#if SINGLE_USER
		PlatiProc( d4lock(-1,1), ==0 ); /* locknout cely soubor */
	#endif
	RKeKomu=f4ref("KEKOMU");
	RKdo=f4ref("KDO");
}

static void GetSpoj( Spojeni *D )
{
	ZFilterstrcpy(D->KeKomu,f4str(RKeKomu),DataNorma);
	StrNormal(D->KeKomu);
	ZFilterstrcpy(D->Kdo,f4str(RKdo),DataNorma);
	StrNormal(D->Kdo);
}
static void SetSpoj( const Spojeni *D )
{
	*(char *)f4record()=' ';
	DFilterf4r_str(RKeKomu,D->KeKomu,DataNorma);
	DFilterf4r_str(RKdo,D->Kdo,DataNorma);
}
static Flag RuzneSpoj( const Spojeni *D, const Spojeni *d )
{
	return strcmp(D->KeKomu,d->KeKomu) || strcmp(D->Kdo,d->Kdo);
}

static Err ZalozSpIndex( void )
{
	int idx;
	idx=i4index_filter(SpojeniN,"KEKOMU",0,0,d4deleted);if( idx<0 ) return EDTB;
	idx=i4close(idx);if( idx<0 ) return EDTB;
	idx=IndOpen(SpojeniN,False);if( idx<0 ) return EDTB;
	SpojIdx=idx;
	idx=i4index_filter(KdoSpN,"KDO",0,0,d4deleted);if( idx<0 ) return EDTB;
	idx=i4close(idx);if( idx<0 ) return EDTB;
	idx=IndOpen(KdoSpN,False);if( idx<0 ) return EDTB;
	SKdoIdx=idx;
	return EOK;
}
static Err OtevriSpIndex( Flag Test )
{
	int idx;
	idx=IndOpen(SpojeniN,Test);if( idx<0 ) return EDTB;
	SpojIdx=idx;
	idx=IndOpen(KdoSpN,Test);if( idx<0 ) return EDTB;
	SKdoIdx=idx;
	return EOK;
}

Err ZalozSpojeni( void )
{
	int ret;
	Plati( !SpojOtevrene );
	if( !Privileg(2) ) return ECAN;
	ret=d4create(SpojeniN,(int)lenof(SpojForm),SpojForm,0);
	if( ret<0 ) return EDTB;
	SpojDtb=ret;
	ret=ZalozSpIndex();if( ret<EOK ) return EDTB;
	InitSpoj();
	d4select(SpojDtb);d4close();
	return EOK;
}

static Err OtevriSpojeni( Flag Test )
{
	int r;
	r=FieldsUse(SpojeniN,(int)lenof(SpojForm),SpojForm);
	if( r<0 ) {Chyba((Err)r);return ECAN;}
	SpojDtb=r;
	d4lock(-1,1); /* s fakturami smi pracovat jen jeden clovek */
	if( OtevriSpIndex(Test)<EOK )
	{
		if( Test )
		{
			if( ZalozSpIndex()<EOK ) return EDTB;
		}
		else return EDTB;
	}
	InitSpoj();
	SpojOtevrene=True;
	return EOK;
}

static Err UlozSSpojeni( void )
{
	Err ret;
	int r;
	Plati( SpojOtevrene );
	r=DtbSave(SpojDtb);if( r ) {ret=EDTB;goto Konec;}
	r=IndexFlush(&SpojIdx,SpojDtb);if( r ) {ret=EDTB;goto Konec;}
	ret=EOK;
	Konec:
	return ret;
}

static Err ZavriSpojeni( void )
{
	Err ret;
	int r;
	Plati( SpojOtevrene );
	ret=UlozSSpojeni();if( ret<EOK ) return ret;
	SpojOtevrene=False;
	d4select(SpojDtb);
	r=d4close();
	if( r ) return EDTB;
	return EOK;
}

static Err FInitSSpojeni( void )
{
	Err ret=OtevriSpojeni(True);
	if( ret<EOK ) return ZalozSpojeni();
	ZavriSpojeni();
	return ret;
}

static Err FZacSpojeni( void ){return OtevriSpojeni(False);}
static Err FKonSpojeni( void ){return ZavriSpojeni();}

Err ZacSpojeni( void ){return FInitSSpojeni();}
Err KonSpojeni( void ){return EOK;}

static int SelDeg=0;

Err SelectSpojeni( void )
{
	Plati( SelDeg>=0 );
	if( SelDeg++==0 ) return FZacSpojeni();
	else return EOK;
}
Err UnselectSpojeni( void )
{
	Plati( SelDeg>0 );
	if( --SelDeg==0 ) return FKonSpojeni();
	else return EOK;
}

#ifndef __MSDOS__
void OdlokniSpojeni( void ){Plati( !SpojOtevrene );}
#endif

long SpojeniBack( const char *DPath, const char *SP, Flag Est )
{
	PathName S;
	sprintf(S,"%s\\%s",SP,NSpoj".DBF");
	return FileBack(DPath,S,Est);
}

Err SpojeniPack( void ){return EOK;}
Err FormatSpojeni( void ){return EOK;}

/* pristup k jednotlivym fakturam */

Err ZacSpoj( Spojeni *FH ) /* vyhleda spojeni podle klice Kdo */
{
	char OKey[DelNazvu+1];
	sprintf(OKey,"%-*s",DelNazvu,FH->Kdo);
	d4select(SpojDtb);
	if( IndSelect(SKdoIdx)<0 ) return EDTB;
	if( Filterd4seek_str(OKey,DataNorma) ) return EDTB;
	GetSpoj(FH);
	return EOK; /* zustane lokly */
}
Err KonSpoj( Spojeni *FH ) /* je-li treba, ulozi spojeni */
{
	Spojeni O;
	KursorBusy();
	Plati( SpojOtevrene );
	d4select(SpojDtb);
	GetSpoj(&O);
	if( RuzneSpoj(FH,&O) )
	{
		long rn=d4recno();
		int r;
		if( rn==0 ) return EDTB;
		SetSpoj(FH);
		GetSpoj(FH);
		r=d4write(rn);
		if( r<0 ) goto Error;
	}
	#if !SINGLE_USER
		d4unlock(1);
		IndUnselect();
	#endif
	return EOK;
	Error:
	*FH=O;
	return EDTB;
}

Err NajdiSpojeni( Spojeni *FH, const Zaznam *Z, int i )
{
	char OKey[DelNazvu+1];
	Plati( SpojOtevrene );
	strlncpy(FH->KeKomu,Z->Nazev,DelNazvu+1);
	d4select(SpojDtb);
	if( IndSelect(SpojIdx)<0 ) return EDTB;
	sprintf(OKey,"%-*s",DelNazvu,Z->Nazev);
	if( Filterd4seek_str(OKey,DataNorma) ) return EDTB;
	while( i>0 )
	{
		if( d4skip(+1) ) return ERR;
		i--;
	}
	GetSpoj(FH);
	d4select(SpojDtb);
	#if !SINGLE_USER
		d4unlock(1);
		IndUnselect();
	#endif
	if( strcmp(FH->KeKomu,Z->Nazev) ) return ERR; /* uz jsme jinde */
	return EOK;
}

Err UlozSpojeni( Spojeni *FH )
{ /* ukladame jen nove faktury */
	int r;
	Plati( SpojOtevrene );
	d4select(SpojDtb);
	i4select(SpojIdx);
	/* novy zaznam */
	r=d4go(-1);if( r<0 ) return EDTB;
	/* prirad mu hodnotu */
	SetSpoj(FH);
	/* a uloz ho */
	r=d4append();if( r<0 ) return EDTB;
	return EOK;
}

Err PrejmenujSpoj( const Zaznam *N, const Zaznam *O )
{
	Spojeni FH;
	char NNazev[DelNazvu+1],ONazev[DelNazvu+1];
 	strcpy(NNazev,N->Nazev);StrNormal(NNazev);
	strcpy(ONazev,O->Nazev);StrNormal(ONazev);
	if( strcmp(NNazev,ONazev) )
	{
		char OKey[DelNazvu+1];
		sprintf(OKey,"%-*s",DelNazvu,ONazev);
		Plati( SpojOtevrene );
		d4select(SpojDtb);
		if( IndSelect(SpojIdx)<0 ) return EDTB;
		while( Filterd4seek_str(OKey,DataNorma)==0 )
		{
			GetSpoj(&FH);
			strlncpy(FH.KeKomu,NNazev,DelNazvu+1);
			Chyba(KonSpoj(&FH));
		}
		if( IndSelect(SKdoIdx)<0 ) return EDTB;
		while( Filterd4seek_str(OKey,DataNorma)==0 )
		{
			GetSpoj(&FH);
			strlncpy(FH.Kdo,NNazev,DelNazvu+1);
			Chyba(KonSpoj(&FH));
		}
	}
	return EOK;
}

Err VymazSpojeni( const char *N )
{
	char OKey[DelNazvu+1];
	sprintf(OKey,"%-*s",DelNazvu,N);
	Plati( SpojOtevrene );
	d4select(SpojDtb);
	if( IndSelect(SpojIdx)<0 ) return EDTB;
	while( Filterd4seek_str(OKey,DataNorma)==0 )
	{
		long rn=d4recno();
		if( !rn ) return EDTB;
		if( d4delete(rn) ) return EDTB;
	}
	return EOK;
}

Err SoupisSpojeni( enum vystup Vystup )
{
	Err ret;
	ret=SelectSpojeni();
	if( ret<EOK ) return ret;
	else
	{
		char Buf[DelRadku+1];
		char PoslS[DelNazvu+1];
		ret=EDTB;
		d4select(SpojDtb);
		if( IndSelect(SpojIdx)<0 ) goto Error;
		PoslS[0]=0;
		if( ZacVystup(Vystup)>=EOK )
		{
			ret=EOK;
			if( d4top()==0 )
			{
				Spojeni S;
				while( ret>=EOK )
				{
					GetSpoj(&S);
					if( strcmp(PoslS,S.KeKomu) )
					{
						TiskPodtr(Buf,28*2+1),ret=TiskVystup(Vystup,Buf);
						strcpy(PoslS,S.KeKomu);
						if( ret>=EOK ) sprintf(Buf,"%-28s %-28s",S.KeKomu,S.Kdo),ret=TiskVystup(Vystup,Buf);
					}
					else
					{
						sprintf(Buf,"%-28s %-28s","",S.Kdo),ret=TiskVystup(Vystup,Buf);
					}
					if( d4skip(+1) ) break;
				}
			}
			KonVystup(Vystup);
			if( Vystup==Okno || Vystup==ScOk )
			{
				StiskRet();
				ZrusOkno();
			}
		}
		Error:
		Chyba(UnselectSpojeni());
	}
	return EOK;
}
