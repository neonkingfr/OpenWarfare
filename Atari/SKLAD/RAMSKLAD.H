/* skladove hospodarstvi */
/* definice procedur nezavislych na prostredi */

#ifndef __MSDOS__
	#define SINGLE_USER 1 /* v SINGLE_USER verzi je vsechno porad locknute */
	#define N_USERS 1
#else
	#define SINGLE_USER 0 /* na PC sitova verze */
	#define N_USERS 4
#endif

#define DAVKY ( !SINGLE_USER && N_USERS<8 )

#define SkladNazev "DTB\\DEPOZIT"

enum vystup {Tisk,Soub,Okno,ScOk};
enum norma {Latin2,InterF,KeybCs,Cestin};

enum
{
	DelNazvu=24, /* nazev polozky */
	DelFNazvu=32, /* nazev firmy */
	DelFAdr=24, /* adresa firmy */
	DelIco=12,
	DelDIco=18,
	NAdr=3, /* pocet rad. adresy */
	DelRadku=160, /* delka tiskoveho radku */
	DelKoment=16, /* delka komentare v dokladu - inventura */
	DelEsc=31, /* max. delka ridicich sekv. na tiskarne */
	NSkup=10,
	PocetObdobi=3, /* obraty -  denni, tydenni, mesicni */
};

typedef enum
{
	ERR=-256,
	ERAM,EFIL,EDTB,EPRN,EFRD,EFWR,
	ECAN,
	EOK=0,
} Err;

typedef long vindex;

enum styl {Normal=0,Tucne=1,Kurziva=2,Uzke=4,Strankovani=0x200,Strana=0x400};
enum
{
	Uctenka,Prijemka,Dodak,Inventura,
	VUctenka,VPrijemka,VDodak,_VInventura,
	OUctenka,_OPrijemka,_ODodak,_OInventura,
	BUctenka,_BPrijemka,_BDodak,_BInventura,
	PresPrij,PresVyd,VPresPrij,VPresVyd,
	/* jiz zavedene typy se nesmi menit */
	/* ty s podtrzitkem by nemely existovat */
	NTypu,
	/* prodej bez uctenek - i doklad 0.0.0. - 0/0 (postaru) */ 
	/*
	TypVrat=VUctenka-Uctenka,
	TypOprava=OUctenka-Uctenka,
	TypBez=BUctenka-Uctenka,
	*/
};

#define MAXLONG ((long)0x7fffffffL)
#define MINLONG ((long)0x80000000L)
#define MININT ((int)0x8000)
#define MAXINT ((int)0x7fff)

typedef double Castka;

typedef struct
{
	Castka Dan; /* soucet dani */
	Castka Obrat; /* Obrat s dani */
	Castka Zisk; /* Zisk bez dane */
	long Ks;
} RObrat;
typedef struct
{
	char ONazev[DelNazvu+1];
	int OSkup;
	int ODan; /* ruzne dane - ruzne polozky */
	RObrat P,V,D,R; /* obrat pro prijem, vydej, dodavky, presuny */
} OObrat;
typedef struct
{
	Flag Vymaz;
	int Skupina;
	char Nazev[DelNazvu+1]; /* nazev zbozi */
	int Dan; /* dan z prid. hodnoty */
	long Ks; /* kusu na sklade */
	Castka Cena,PorCena; /* prodejni, porizovaci cena */
	Castka OCena1,OCena2; /* obchodni ceny */
	long OKs1,OKs2; /* poz. mnozstvi pro obch. ceny */
	Flag CenaDph;
	long MinKs,DopKs;
} Zaznam;

typedef struct
{
	char Nazev[DelNazvu+1];
	int Skup;
	char Koment[DelKoment+1];
	Castka CCena,CZisk; /* nic na jednotku, vse nasobene Ks */
	Castka JCena; /* cena na jednotku - jen cteci (pomocna) */
	Castka DZaklad; /* zaklad dane - celkem - nikoliv na jednotku */
	Flag DSDani; /* zda je DZaklad s dani nebo bez */
	long Ks;
	int Dan;
	Flag JeDph; /* CCena,JCena je vcetne dph? */
	int Znam; /* smer pohybu - vydej -1, prijem +1 */
	Flag Ukladej;
} Radek;
typedef struct
{
	Radek *Buf;
	size_t Vyhrazeno,Uzito;
	Castka CDan,CCena; /* celk. dan */
	vindex Idtf;
	long Typ;
	Flag SDani;
	char FNazev[DelFNazvu+1];
	int Den,Mesic,Rok;
	Flag VObratu;
	Flag Odlozen;
} Doklad;

typedef struct
{
	char FNazev[DelFNazvu+1];
	char FAdr[NAdr][DelFAdr+1];
	char Ico[DelIco+1];
	char DIco[DelDIco+1];
	long MnoD1,MnoD2;
} Firma;

enum {DelkaFPol=32};
enum {MaxFPol=32};

typedef struct
{
	char Nazev[16+1];
	char Text[20+1];
} FakPolozka;

typedef struct
{
	char Cislo[DelkaFPol+1];
	char Datum[DelkaFPol+1];
	char Splatnost[DelkaFPol+1];
	FakPolozka Pol[MaxFPol]; /* posledni ma *Nazev== 0 */
	vindex Idtf; /* jaky dodaci list */
	long Typ; /* pripadne i jiny doklad - zatim se nepouziva */
} FakturaHlav;

typedef struct
{
	char KeKomu[DelNazvu+1];
	char Kdo[DelNazvu+1];
} Spojeni;

/* BAK */

long VolnoNaDisku( int N );

long FileBack( const char *DPath, const char *Name, Flag Est );

long DBackup( int Doba, Flag Est ); /* back. na hard disk */
long Backup( int Doba, Flag Est ); /* z HD na A: */

Err DenniRutina( void );
Err TydenniRutina( void );
Err MesicniRutina( void );

/* DTB */

extern PathName TiskRPath;

/* obecna nadstavba d4 */

#ifdef __D4BASE
	#include "stdbase.h"
	void StrNormal( char *Z );
	void DFilterf4r_str( long ref, const char *S, enum norma DNorma );
	int Filterd4seek_str( const char *S, enum norma DNorma );
	long FRef( const char *Name );
	Err DtbSave( int ref ); /* ulozit databazi */
	int FieldsUse( const char *Name, int N, FIELD *F ); /* use s testem struktury */
	int IndOpen( const char *Nam, Flag Test ); /* otevrit index - filter deleted - testovat korekt. */
	int IndexFlush( int *_iref, int bref ); /* ulozit index */
	int IndSelect( int iref ); /* zvolit index */
	int IndUnselect( void ); /* unlocknout index */
#endif

#ifdef __DEBUG
	void LogF( const char *f, ... );
#else
	#define LogF (void)
#endif

/* konkr. pouziti - sklad, obrat, doklady */

Err ZacSklad( void ); /* virtualizace otevirani databaze */
Err KonSklad( void );

Err SelectSklad( void );
Err UnselectSklad( void );
void OdlokniSklad( void );

Err UlozSklad( void );

Flag ZacDtb( const char *Naz ); /* vsechny databaze */
void KonDtb( void );
Flag JeDtb( void );
Err NovaDtb( void );
Err CtiDtb( void );
Err ZavriDtb( void );

void VseOdlokni( void );

long SkladBack( const char *DPath, const char *SP, Flag Est );
Err SkladPack( void );
Err FormatSklad( void );

Zaznam **SeznamZbozi( int Skup ); /* stav skladu */
void UvolniZbozi( Zaznam **ZZ );

Zaznam *FZalozZaznam( int Skup, const char *Text ); /* alokuje a inic. novy zaznam */

Err ZacZaznam( const char *Naz, Zaznam *Z ); /* najde zaznam, lockne ho a precte */
Err KonZaznam( Zaznam *N ); /* zapise a odlokne zaznam - nejde-li zapsat, uvede ho v N do puvodniho stavu */
Err AktualZaznam( const char *Naz, Zaznam *Z ); /* nacte do zaznamu z databaze */

Err PrvniZaznam( Zaznam *Z, Flag Spesne ); /* dela ZacZanam */
Err DalsiZaznam( Zaznam *Z, Flag Spesne ); /* dela ZacZaznam - spesne urcuje odemykani indexu */

Err DNovyZaznam( Zaznam *N ); /* vlozi zaznam do dtb */
Err VymazZaznam( const char *Naz ); /* vymaze zaznam z dtb */
Err DZmenStav( const char *Naz, long ZmenaKs ); /* zmeni pocet kusu v zaznamu */
Err DZmenStavP( const char *Naz, long ZmenaKs, Castka PCena, Flag SDani ); /* zmeni pocet kusu v zaznamu a PCenu */

void MazSklad( void );

Err ZalozObrat( int Doba ); /* stav obratu */
Err ZalozObraty( void );
Err UlozObrat( int Doba );

long ObratyBack( const char *DPath, const char *SP, Flag Est );
Err ObratyPack( void );
Err FormatObraty( void );

Err ZacSObrat( void ); /* prace s obratem na zac. a kon. programu */
Err KonSObrat( void );

Err SelectObrat( void ); /* aktivace a deaktivace obratu */
Err UnselectObrat( void );

Err ZacObrat( const char *Naz, int Skup, int Dan, OObrat *O, int Doba, Flag Zaloz ); /* najde a nacte obrat, muze i zalozit */
Err KonObrat( OObrat *O, int Doba ); /* zapise a odlokne zaznam */
Err PrejmenujObrat( const Zaznam *N, const Zaznam *O, int Doba ); /* pri zmene zaznamu nutno upravit obrat */

Err PrvniObrat( OObrat *O, int Doba ); /* precte a odlokne prvni zaznam */
Err DalsiObrat( OObrat *O, int Doba ); /* precte a odlokne dalsi zaznam */

void DefRadek( char *Buf );

Err CtiSkupiny( void ); /* skupiny */
Err PisSkupiny( void );
const char *NazevSkupiny( int Skup ); /* prida cislo skupiny apod. */

const char *SkutNazevSkupiny( int Skup );
const char *SkutKodSkupiny( int Skup );
void SetNazevSkupiny( int Skup, const char *Naz, const char *Kod );

Err NactiInfo( void ); /* config. soub. */
Err UlozInfo( void );

void KontrolaDat( void );

/* DOK */

Err ZalozDoklady( void );

long DokladyBack( const char *DPath, const char *SP, Flag Est );
Err DokladyPack( void );
Err FormatDoklady( void );

Err OdlozDoklady( enum vystup Vystup, Flag NoQuery );

Err ZacSDoklady( void );
Err KonSDoklady( void );

Err SelectDoklady( void ); /* virtualizace otevirani */
Err UnselectDoklady( void );

void OdlokniDoklady( void );

long FyzIdtf( vindex Idtf, long Typ );
void LogIdtf( long FI, vindex *Idtf, long *Typ );

vindex CisloDokladu( long Typ ); /* zjisteni cisla dokladu */

Err UlozDokladNaDisk( const char *Nazev, const Doklad *D );
Err NactiDokladZDisku( const char *Nazev, Doklad *D );

Err UlozDoklad( const Doklad *D ); /* zavedeni do databaze */
Err NactiDoklad( Doklad *D, vindex Idtf, long Typ ); /* pro ucely dalsi editace nebo vraceni */
Err InfoDoklad( Doklad *D, vindex Idtf, long Typ ); /* pro ucely dalsi editace nebo vraceni - jen hlavicka */
vindex DalsiDoklad( long Typ, vindex Idtf, const char *FNaz, Flag Spesne ); /* zjisti cislo dalsiho dokladu (pro Idtf<0 prvniho dokladu) */
void DalsiVDoklad( long *PTyp, vindex *PIdtf, const int *Typy, const char *FNaz, Flag Spesne ); /* dalsi doklad, jeden z povol. typu, Typ a Idtf jsou I/O param. */

Err DokladBezObratu( Doklad *D, vindex *I, long *T );

Err VymazDoklad( vindex Idtf, long Typ );
Err PridejDoklad( const Doklad *D ); /* prida do dokladu se stejnym typem a I */
Err ZmenDoklad( Doklad *D ); /* editace dokladu - je destruktivni - maze to, co v .DBF prebyva */
/* v dokladech s Idtf 0 jsou ulozeny vsechny ty operace, ktere nemely tisteny doklad */

Doklad **SoupisDokladu( const char *FNaz );
Doklad **SoupisDokladuDruhu( const char *FNaz, Flag PovDruhy[NTypu] );
void UvolniDoklady( Doklad **MB );

/* LNK */

Err ZalozSpojeni( void );

long SpojeniBack( const char *DPath, const char *SP, Flag Est );
Err SpojeniPack( void );
Err FormatSpojeni( void );

Err ZacSpojeni( void );
Err KonSpojeni( void );

Err SelectSpojeni( void ); /* virtualizace otevirani */
Err UnselectSpojeni( void );

Err ZacSpoj( Spojeni *FH ); /* vyhleda spojeni podle klice Kdo */
Err KonSpoj( Spojeni *FH ); /* je-li treba, ulozi, vzdy odlokne */

Err PrejmenujSpoj( const Zaznam *N, const Zaznam *O );
Err UlozSpojeni( Spojeni *FH );
Err NajdiSpojeni( Spojeni *S, const Zaznam *Z, int i );
Err VymazSpojeni( const char *N ); /* zrusi cely komplet */

Err SoupisSpojeni( enum vystup Vystup );

/* FAD */

Err ZalozFaktury( void );

long FakturyBack( const char *DPath, const char *SP, Flag Est );
Err FakturyPack( void );
Err FormatFaktury( void );

Err ZacSFaktury( void );
Err KonSFaktury( void );

Err SelectFaktury( void ); /* virtualizace otevirani */
Err UnselectFaktury( void );

void PrvniFaktura( char *buf, size_t len );
void NasledujiciFaktura( char *buf, size_t len );

void PoslFaktura( char *buf, size_t len );
void DalsiFaktura( char *buf, size_t len );

Err NactiFakturu( FakturaHlav *FH );
Err UlozFakturu( FakturaHlav *FH );

/* FIR */

Err ZalozFirmy( void );
Err UlozFirmy( void );
Err ZavriFirmy( void );

long FirmyBack( const char *DPath, const char *SP, Flag Est );
Err FirmyPack( void );
Err FormatFirmy( void );

Err ZacSFirmy( void );
Err KonSFirmy( void );

Err SelectFirmy( void ); /* virtualizace otevirani */
Err UnselectFirmy( void );

void OdlokniFirmy( void );

Err DNovaFirma( Firma *F );
Firma *FZalozFirma( const char *Naz );
Err VymazFirmu( const char *Naz );

Err ZacFirma( const char *Naz, Firma *F );
Err KonFirma( Firma *F );
Firma **SeznamFirem( void );
void UvolniFirmy( Firma **S );

/* OPC */

#ifdef __NAROD
	extern CmpPar CesCmp;
#endif

void ZacCestina( void );

Castka SpoctiDan( Castka Cena, int Dan, Flag SDani );

Err Zacni( Doklad *D, Flag InfoOnly );
Radek *Pridej( Doklad *D, const Radek *R );
Radek *NajdiRadek( Doklad *D, const char *N, int Skup, Flag Zaloz );
Err AktualDoklad( Doklad *D, const Doklad *P );
Err PrictiDoklad( Doklad *D, const Doklad *P ); /* obratovy doklad */
Err ZacniKopii( Doklad *D, const Doklad *P );
void Uvolni( Doklad *D );

Flag PrazdnyDoklad( const Doklad *D );

Castka UrciVCenu( const Zaznam *Z, long Ks, const Firma *F ); /* nektere firmy maji spec. podminky */
Castka PorCenaSDph( const Zaznam *Z );
Castka PorCenaBDph( const Zaznam *Z );
Castka CenaSDph( const Radek *R );

void TiskPodtr( char *Buf, int Len );
void TiskHlava( char *Buf, const char *Nazev, long Idtf, int Den, int Mesic, int Rok, int DelkaR );
/* tisk jmena firmy - nutno nejak chranit */

Err TiskTelaDokladu( enum vystup Vyst, Doklad *D, Flag SDani, Flag JePlatce, int MaxLines );
/* telo dokladu se tiskne i pri fakturach */
/* SDani a JePlatce se uplatnuje jen pri prijmu - jinak by melo byt oboji True */

void NulaText( char *B );

Castka StanovBDph( Castka C, int Dan ); /* z Dph se pokusi udelat bez Dph */
Castka StanovSDph( Castka C, int Dan ); /* z Dph se pokusi udelat bez Dph */

Err TypTiskDokladu( enum vystup Vyst, Doklad *D ); /* tisk dokladu vsech typu */
Err TypObratDokladu( Doklad *D );
Err DaneDokladu( OObrat *Dane, Doklad *D );

Err PrejmenujDoklad( Doklad *D, const Zaznam *N, const Zaznam *O );

void InitOpce( void );

void Vydej( int Skup ); /* skupina, ve ktere se zacina */
void VydejFir( void );
void Dodavka( void );
void PresunVyd( void );
void PresunPrij( void );
void Prijem( void );
void Editace( void );
void OpravDoklad( void ); /* dodatecne opravy dokladu */
void PrevratDoklad( Doklad *D ); /* dualni */
void ProvedDoklad( Doklad *D ); /* dodatecne provedeni dokladu */
void KopieDokladu( void ); /* kopie z dokladu */

void VraceniDokladu( void );
void VratVydej( void );
void VratVydejFir( void );
void VratPrijem( void );
void VratDodavku( void );

/* FAK */

void Faktura( void ); /* faktura z dokladu */
void FakturaZDokladu( Doklad *D );
void KopieFaktury( void );
Err TiskFaktury( enum vystup Vyst, FakturaHlav *F, Flag Edit );

/* ODO */

Err VyberDoklad( const char *N, long *TD, vindex *TI );
Err VyberDokladDruhu( const char *N, long *TD, vindex *TI, const int *Druhy );
void DokladNaDisk( Doklad *D );
void DokladZDisku( void );

/* OFI */

void PrehledFirem( void );
const char *ProFirmu( const char *Titul );

/* PRH */

Err PridejObrat( const char *Naz, int Skup, int Dan, const OObrat *O );

void NovObrat( OObrat *O );

void VymazObrat( void );

Err PrehledObratu( int Doba, enum vystup Vyst, Flag NoQuery );
Err PrevedObrat( int Doba );

void PrehledDokladu( const char *FNaz ); /* Fir==NULL => Vsechny doklady */
#if defined __EXT || defined __DOS_H
	Err FPrehledDokladu( const char *FNaz, enum vystup Vystup, struct date *o, struct date *d, Flag Uct, Flag Dod, Flag Prij, Flag Inv, Flag BUct, Flag Faktury, Flag NoQuery, Flag Odlozene );
#endif

void PrictiDane( OObrat *Dane, const OObrat *P );

Err PrehledSkladu( enum vystup Vyst, Flag NoQuery );
Err Objednavka( void );
Err SoupisFirem( void );

/* SOU */

extern char TucneOn[DelEsc+1],TucneOff[DelEsc+1];
extern char KurzivOn[DelEsc+1],KurzivOff[DelEsc+1];
extern char UzkeOff[DelEsc+1],UzkeOn[DelEsc+1];

extern int OdradTisk;
extern Flag HezkyTisk;

extern PathName SoubNaz;

Err CtiTiskarnu( const char *FName );

void InitNormy( void );

int ConvertZNormy( int c, enum norma N );
int ConvertDNormy( int c, enum norma N );
void FilterStrncpy( char *D, const char *S, long l, enum norma N );
void ZFilterstrcpy( char *D, const char *S, enum norma N );
void DFilterstrcpy( char *D, const char *S, enum norma N );

Err ZacTisk( void );
Err KonTisk( void );
Err TiskTisk( const char *Buf, enum styl Styl );
Err ZacSoub( void );
Err KonSoub( void );
Err TiskSoub( const char *Buf, enum styl Styl );

Err ZacVystup( enum vystup o );
Err ZacVystupNQ( enum vystup o, Flag NoQuery );
Err KonVystup( enum vystup o );
Err TiskSVystup( enum vystup o, const char *Buf, enum styl Styl );
Err TiskVystup( enum vystup o, const char *Buf );

/* VYS */

int OAlert( const char *A, int Def, int Canc ); /* cislo Def (Return) a Canc (Esc) (Canc muze byt i mensi nez nula apod.) */
int OAlertRF( int AI, int Def, int Canc, ... );

void ClsOkno( void );

Err ZacOkno( void );
Err KonOkno( void );
Err ZrusOkno( void );
Err TiskOkno( const char *Buf, enum styl Styl );

/* VYB */

Err ZacVyber( void );
void KonVyber( void );

/* RAM */

extern Flag OdlozTydne;
extern int SplatnostFaktur;
extern int SuseniDokladu; /* tak dlouho se nechavaji v databazi */

typedef const char *cstr;
typedef struct
{
	cstr Vydej,Prijem,Dodavka,Editace,Presun,Inventura,Dodavky;
	cstr Nazev,Ks,CenaKs,BezDph,VceDph;
	cstr Uctenka,Prijemka,Dodak;
	cstr VUctenka,VPrijemka,VDodak,VEditace;
	cstr SouhrnDph,CelkemDph,CelkemCena;
	cstr Celkem,Zisk,Obrat,MesObrat,TydObrat,DenObrat;
	cstr VseBezDph,Dph,Mezisoucet,VsechnySkup;
	cstr Cena,Mazani;
	cstr Vystup,Vstup,Firma,ProFirmu,OdFirmy;
	cstr NovyStav,ZvlaCena;
	cstr OpravaDok;
	cstr NoveZbozi,EditaceZbozi;
	cstr Vystavil,Pro;
	cstr Ico,DIco;
	cstr PCena;
	cstr VratVydej,VratDodej,VratPrijem;
	cstr VrUctenka,VrPrijemka,VrDodak;
	cstr BUctenka,Doklady;
	cstr StavSkl;
	cstr KopieDokl,Faktura;
	cstr SDani,Prevodem,Osobni;
	cstr Objednavka;
	cstr PresPrij,PresVyd;
	cstr VPresPrij,VPresVyd;
	cstr UlozDok,NactiDok;
} FSS;

extern FSS FS;

extern enum norma TiskNorma,SoubNorma,KlavNorma,DataNorma;

extern const char *AdresaS;

int ZvolSkup( const char *Naz ); /* volba skupiny */

long VydejPocet( Zaznam *Z, long Max, long Init, Castka *ExCena );
long DodejPocet( Zaznam *Z, long Max, long Init, Castka *ExCena );
long PrijemPocet( Zaznam *Z, long Init, Flag *Dph, Flag Novy ); /* Cena, dph - obousmerny parametr */
long StavPocet( Zaznam *Z, long Init, char *Koment ); /* Koment - obousmerny - delka DelKoment */
void DoMeziopce( Flag Draw, Flag Force ); /* nekdo chce zobrazit drive, nez zapise stav */

Err EditZaznam( Zaznam *Z );
Err EditFirmu( Firma *Z );
Err ZadejSpojeni( char *Nazev, char *Kdo );
Err VymazLinkD( char *Nazev ); /* zrusi cely komplet */

enum zdatisk {TNic,TTisk,TEdit};
Flag ZdaOpravitDoklad( void );
Flag ZdaNovyObrat( void );
Flag ZdaSpocObrat( void );
Flag ZdaPrevedObrat( int Doba, enum vystup O );
Flag ZdaNovaDtb( void );
Flag ZdaNoveFirmy( void );

Flag ZdaMazatZbozi( void );
Flag ZdaRusitZbozi( void );
Flag ZdaMazatFirmu( void );

void NejdeEditVO( void );
Flag Privileg( int Level );
Flag VolnyDisk( void );

enum zdatisk ZdaTisk( const char *Naz, Flag Def );
enum zdatisk OZdaTisk( const char *Naz ); /* jen Tisk/Oprava */
enum zdatisk ZdaKopie( const char *Naz, Flag Def );
Flag ZdaFaktura( void );
Flag PriprTisk( void );

void ChybaAdresy( void );
Flag NeniMisto( long Del ); /* kolik je treba */
void MocVelke( void ); /* moc velke, vetsi nez disketa */

Flag TestTiskarny( void );
void GetDatum( int *Den, int *Mesic, int *Rok );
int TestShift( void );

Flag CislaDokl( int *Uct, int *Dod, int *Pri, int *OUct, int *ODod, int *OPri );
Flag Nulovat( Flag *PCen, Flag *Cen, Flag *CD1, Flag *CD2, Flag *Ks );
Err JVyberDok( const char *N, long *TD, vindex *TI, const int *Povol );

Err VyberFakturu( char *Cislo );
Err FakturaD( Doklad *D, FakturaHlav *F );

Flag ZdaZrusitDoklad( const char *Naz );
enum vystup KamVystup( void );
enum vystup KamVystupT( const char *N );
void AutomNazev( const char *Uv );
Flag ZdaNuly( void );
Flag ZrusDoklad( void );

void StiskRet( void );
void ChybaPol( const char *N );

Flag ZjistiFSel( PathName SoubNaz );
void UzVracen( void );
void HlasKonflikt( void );

void Chyba( Err ret, ... );
void Panika( int Num, const char *Msg );
void KursorBusy( void );
void KursorFree( void );

void SetTitul( const char *N, ... );
void ResetTitul( void );

#if defined __EXT || defined __EXT_H || defined __DOS_H
	Flag DoklOdDo( struct date *o, struct date *d, Flag *Uct, Flag *Dod, Flag *Prij, Flag *Inv, Flag *BUct, Flag *Faktury );
#endif

/* reimport */

void lenstr( char *S, size_t l, char c );

