/* editor sumu - ramec programu */
/* SUMA 5/1994-6/1994 */

#include <stdlib.h>
#include <string.h>
#include <macros.h>
#include <gem\multiitf.h>
#include <sttos\cszvaz.h>
#include "ramsum.h"
#include "syntsum.h"

char NazevRsc[]="SYNTSUM.RSC";

PathName NastrP;

void DebugError( int Line, const char *File )
{
	if( 1==AlertRF(PANIKA,2,NajdiNazev(File),Line) )
	{
		if( Falcon ) wind_new();
		exit(0);
	}
}
void NejsouOkna( void ){AlertRF(NOKNAA,1);}
void ChybaRAM( void ){AlertRF(RAMEA,1);}

void WChybaA( const char *FP ){AlertRF(WERRA,1,SizeNazev(FP,20));}
void RChybaA( const char *FP ){AlertRF(RERRA,1,SizeNazev(FP,20));}

void ZmenVizaz( Flag Je3D )
{
	static const int Ds[]=
	{
		ABOUT,
		SNDD,
		0
	};
	const int *D;
	for( D=Ds; *D; D++ ) Dial3D(RscTree(*D),Je3D);
}

static void ZmenCode( void (*CS)( char *S, int N ), int N )
{
	int i;
	for( i=0; i<=SNDD; i++ ) DialCode(RscTree(i),CS,N);
	FSCode(SNDLOCKA,CS,N);
}

void ZmenNormu( int NTab, int OTab )
{
	if( OTab ) ZmenCode(CSStrDecode,OTab);
	if( NTab ) ZmenCode(CSStrCode,NTab);
	ZmenNormuOkna(NTab,OTab);
}

Flag ZacRsc( void )
{
	TestFalcon();
	Menu=RscTree(MENU);
	if( AutoMenuKey()<0 ) return False;
	{
		WindRect W;
		GetWind(0,WF_FULLXYWH,&W);
		if( Menu[0].ob_width>W.w ) Menu[0].ob_width=W.w;
		if( Menu[1].ob_width>W.w ) Menu[1].ob_width=W.w;
	}
	{
		OBJECT *O=RscTree(ABOUT);
		char Buf[40];
		AutoDatum(Buf,"",__DATE__);
		SetStrTed(TI(O,ABODATT),Buf);
	}
	ZmenVizaz(Falcon);
	return True;
}

int Kursor( Flag Inside, Flag Obrys ){return KursorOkno(Inside,Obrys);}
void DoCas( void ){DoCasOkna();}

static void Odveseni( void )
{
	if( CSGVazba() )
	{
		CSOdves(ApId);
	}
}

Flag ZacMulti( void )
{
	if( CSGVazba() )
	{
		ZmenCode(CSStrCode,CSTestNorm());
		if( atexit(Odveseni)>=0 )
		{
			CSZaves(ApId);
		}
	}
	GetPath(NastrP);
	EndChar(NastrP,'\\');
	strcat(NastrP,"*.HNS");
	if( SZacDsp()<0 ) return False;
	return ZacOsc()>=0;
}

Flag KonMulti( void )
{
	SKonDsp();
	KonOsc();
	return True;
}
Flag DesktopStisk( int ObIndex, Flag Dvojite, Flag Dlouhe, int MX, int MY ){(void)ObIndex,(void)Dvojite,(void)MX,(void)MY;return !Dlouhe;}
Flag DesktopStisk2( int ObIndex, Flag Dvojite, Flag Dlouhe, int MX, int MY ){(void)ObIndex,(void)Dvojite,(void)MX,(void)MY;return !Dlouhe;}
void DeskZnak( int Asc, int Scan, int Shf, Flag Buf ){(void)Asc,(void)Scan,(void)Shf,(void)Buf;}

void ZmenaVrchniho( int LHan ){(void)LHan;}
void ZmenNormu( int NTab, int OTab );

int main()
{
	return MenuMain();
}