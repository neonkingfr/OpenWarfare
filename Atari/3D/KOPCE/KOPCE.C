/* Visitor - obsluha scen (krajiny) */
/* SUMA 11/1993-9/1994 */

#include <macros.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include "kopce.h"
#include "STDC/IMGLOAD.H"

#define DRAFT 0 /* jen maly vyrez - na zakladni testy */

#define S_CACHE 1 /* cachovani vygenerovane sceny */
#define PULENI_T 0 /* interpolace na prechodech */
#define RANDOMIZ 1 /* jemne vlneni na povrchu */
#define HLADINA 4
#define POHYBVLN 0 /* od jakeho stupne gridu uz nenuti prekreslovani */

#define SGRID0 4 /* nejhrubsi uroven krajiny */
#define SGRIDP 3 /* nejjemnejsi uroven - aspon 1 */
#define SGRIDR 4 /* zaokrouhlovaci uroven */

#define MapLog 3 /* zvetseni mapy - dohani se interpolaci */

#if !DRAFT
static int RozmerPlatu[]=
{ /* rozmery platu pro jednotlive stupne SGRID */
	/* musi byt sude, navic RP[i+1]>RP[i]/2 */
	8,8,8,8,8,6,6,6,4,4,4,4
};
#else
static int RozmerPlatu[]=
{ /* rozmery platu pro jednotlive stupne SGRID */
	/* musi byt sude, navic RP[i+1]>RP[i]/2 */
	4,4,4,4,4,4,4,4,4,4,4,4
};
#endif

long CasSimulace; /* cas, podle ktereho se provadeji animace v krajine */

static void FyzKoor( long *X, long *Y, long x, long y )
{
	*X=x*T_Z-y*(T_Z/2);
	*Y=y*T_V;
}

static void Log0RoundXY( long *cx, long *cy, long x, long y )
{ /* na �rovni MapLog, cx a cy vrac� >>MapLog */
	long XX;
	#define tv ( T_V<<MapLog )
	#define tz ( T_Z<<MapLog )
	XX=x+MF(y,fx(D_Z/(2*D_V))); /* zkoseni souradnicoveho systemu  */
	/* potrebujeme zaokrouhlovat vzdy dolu! */
	if( XX>=0 ) *cx=XX/tz;
	else *cx=-((-XX+tz-1)/tz);
	if( y>=0 ) *cy=y/tv;
	else *cy=-((-y+tv-1)/tv);
	#undef tv
	#undef tz
}

static void LogRoundXY( long *cx, long *cy, long x, long y, int SegH, int SGrid )
{
	static const int AR[7][2]={{ 0, 0},{+1, 0},{+1,+1},{+1,+1},{ 0,+1},{-1, 0},{ 0,-1},};
	/* nutny posuv v sekci A/B */
	long XX;
	long tv=T_V<<SGrid,tz=T_Z<<SGrid;
	XX=x+MF(y,fx(D_Z/(2*D_V))); /* zkoseni souradnicoveho systemu  */
	/* potrebujeme zaokrouhlovat vzdy dolu! */
	if( XX>=0 ) *cx=XX/tz;
	else *cx=-((-XX+tz-1)/tz);
	if( y>=0 ) *cy=y/tv;
	else *cy=-((-y+tv-1)/tv);
	(*cx)+=AR[SegH][0],(*cy)+=AR[SegH][1];
	(*cx)<<=SGrid;
	(*cy)<<=SGrid;
}

#define VyskaLog ( PosLog-MerLog+MapLog-4 )
	
#if VyskaLog>=0
#define KVyska(l) ( (long)(l)<<VyskaLog )
#else
#define KVyska(l) ( (long)(l)>>-VyskaLog )
#endif

static int SurovyKraj( const KrajDef *KD, long xc, long yc, int SGrid )
{ /* primo hodnota z mapy */
	if( SGrid>KDefLog-1 ) SGrid=KDefLog-1;
	xc+=KDefRng/2,yc+=KDefRng/2;
	if( xc<0 || yc<0 || xc>=KDefRng || yc>=KDefRng )
	{
		unsigned long seed=((long)((word)xc^0xa7ea)<<16)+((word)yc^0x5865);
		return KRand(&seed)%12;
	}
	else
	{
		/* KDefRng>>SGrid == (1<<KDefLog)>>SGrid) == 1<<(KDefLog-SGrid) */
		/*return KD->KrajDef[SGrid][(yc>>SGrid)*(KDefRng>>SGrid)+((int)xc>>SGrid)];*/
		return KD->KrajDef[SGrid][((yc>>SGrid)<<(KDefLog-SGrid))+((int)xc>>SGrid)];
	}
}

#if MapLog==SGRIDP
	#define UrovenKraj(KD,xc,yc,SGrid) KVyska(SurovyKraj(KD,(xc)>>MapLog,(yc)>>MapLog,(SGrid)-MapLog))
#else /* mapa prilis hruba */

/* v logickem prostoru je ctvercova sit
   pro interplolaci je nutno ctverce rozdelit zase na trojuhelniky
      +-----------+
      |          /|
      |  A     /  |
      |      /    |
      |    /      |
      |  /   B    |
      |/          |
      +-----------+
*/

static long UrovenKraj( const KrajDef *KD, long xc, long yc, int SGrid )
{
	/* v x a y opravdove souradnice - fixed */
	int v;
	#define MCTV (1L<<MapLog)
	long xr=xc&(MCTV-1),yr=yc&(MCTV-1); /* pro interpolaci */
	xc>>=MapLog,yc>>=MapLog;
	SGrid-=MapLog;if( SGrid<0 ) SGrid=0;
	v=SurovyKraj(KD,xc,yc,SGrid);
	if( xr>0 || yr>0 )
	{
		int vx,vy;
		if( xr>yr ) /* sekce B */
		{
			vx=SurovyKraj(KD,xc+1,yc,SGrid);
			vy=SurovyKraj(KD,xc+1,yc+1,SGrid);
			v+=(int)( ( (vx-v)*xr+(vy-vx)*yr )>>MapLog );
		}
		else /* sekce A */
		{
			vx=SurovyKraj(KD,xc+1,yc+1,SGrid);
			vy=SurovyKraj(KD,xc,yc+1,SGrid);
			v+=(int)( ( (vx-vy)*xr+(vy-v)*yr )>>MapLog );
		}
	}
	return KVyska(v);
}
#endif

Flag Mezi( long r, long v, long vx, long vy )
{
	/* r nesmi byt extrem */
	long max=v,min=v;
	if( vx>max ) max=vx;
	else min=vx;
	if( vy>max ) max=vy;
	ef( vy<min ) min=vy;
	return r>=min && r<=max;
}

long HrubaUrovenVBode( KrajDef *KD, long x, long y )
{ /* rychla varianta - bez interpolace */
	long xc,yc;
	Log0RoundXY(&xc,&yc,x,y); /* vypocet log. souradnic */
	return KVyska(SurovyKraj(KD,xc,yc,0));
}

#define CACHE_UROVEN 0 /* nulovy efekt */

#if CACHE_UROVEN
	static Flag CUEmpty=True;
#endif

long UrovenVBode( KrajDef *KD, long x, long y, long *dx, long *dy )
{
	long vx,vy;
	long xc,yc;
	long sx,sy;
	long v,r;
	long dX,dY;
	#define tv ( T_V<<MapLog )
	#define tz ( T_Z<<MapLog )
	Log0RoundXY(&xc,&yc,x,y); /* vypocet log. souradnic */

	sy=yc*tv; /* vypocti fyz. souradnice rohu ctverecku */
	sx=xc*tz-yc*((1<<MapLog)*(T_Z/2));
	
	sx=x-sx; /* pozice ve ctverecku */
	sy=y-sy;

	sx+=MF(sy,fx(D_Z/2/D_V)); /* korekce geometrie */
	/* prevedli jsme na obdelnik o stranach D_V, D_Z */

	#if __DEBUG>=2
	if( sx<-fxmetr(0.05) || sx>tz+fxmetr(0.05) )
	{
		RepMetry("x",x);RepMetry("y",y);
		RepMetry("sx",sx);RepMetry("sy",sy);
	}
	#endif

	Plati( sy>=0 );
	Plati( sy<=tv );
	
	v=KVyska(SurovyKraj(KD,xc,yc,0));
	/* podminka pro hranici sx/D_Z==sy/D_V */
	if( sx>MF(sy,fx(D_Z/D_V)) ) /* sekce B */
	{
		vx=KVyska(SurovyKraj(KD,xc+1,yc,0));
		vy=KVyska(SurovyKraj(KD,xc+1,yc+1,0));
		dX=MKDK(vx-v,fx(1),tz),dY=MKDK(vy-vx,fx(1),tv);
		if( dx ) *dx=dX,*dy=dY; /* v brzy prestane platit */
		r=v+MF(sx,dX)+MF(sy,dY);
	}
	else /* sekce A */
	{
		vx=KVyska(SurovyKraj(KD,xc+1,yc+1,0));
		vy=KVyska(SurovyKraj(KD,xc,yc+1,0));
		dX=MKDK(vx-vy,fx(1),tz),dY=MKDK(vy-v,fx(1),tv); /* v brzy prestane platit */
		if( dx ) *dx=dX,*dy=dY; /* v brzy prestane platit */
		r=v+MF(dX,sx)+MF(dY,sy);
	}
	return r;
}

static Barva KrivkaRGB[1024*4];

#define BarvaLog ( PosLog-MerLog+MapLog-3 )

static const BarvaInfo *Akt=NULL;

void ZacBarvyTab( const BarvaInfo *BI )
{ /* max. kopce jsou nekolik tis. kilometru */
	const BarvaInfo *B;
	int i;
	Barva o={0};
	if( Akt==BI ) return;
	Akt=BI;
	#if CACHE_UROVEN
		CUEmpty=True; /* inicializuje se krajina */
	#endif
	#define lb ( (int)lenof(KrivkaRGB) )
	for( i=0,B=BI; i<lb; B++ )
	{
		Barva b=B->b;
		int n;
		n=(int)( (long)B->Rng*lb/1000 );
		if( n<=0 || n>lb-i ) n=lb-i;
		if( !B->IPol )
		{
			for( ; n>0; n--,i++ ) KrivkaRGB[i]=b;
			o=b;
		}
		else
		{
			int s=23-_bbitu;
			#define ap(f) ( ( (long)(f)<<s )/n )
			long sr=ap(TR(b)-TR(o)),sg=ap(TG(b)-TG(o)),sb=ap(TB(b)-TB(o));
			long ar=(long)TR(o)<<s,ag=(long)TG(o)<<s,ab=(long)TB(o)<<s;
			for( ; n>0; n--,i++ )
			{
				Barva *S=&KrivkaRGB[i];
				ar+=sr,ag+=sg,ab+=sb;
				SRGBA(S,(BSlozka)(ar>>s),(BSlozka)(ag>>s),(BSlozka)(ab>>s),0);
			}
			o=b;
		}
	}
}

const BarvaInfo *AktKlima( void )
{
	return Akt;
}

Barva BarvaVyska( long z ) /* z je trojnasobek vysky */
{
	int i=(int)( ( z+(1L<<BarvaLog)-1 )>>BarvaLog );
	if( i<0 ) i=0;
	ef( i>(int)lenof(KrivkaRGB)-1 ) i=(int)lenof(KrivkaRGB)-1;
	return KrivkaRGB[i];
}

static void SpoctiPlochy( Plocha *GP, Plocha *EP )
{
	for( ; GP<EP; GP++ )
	{
		long y,Y,a;
		#if __DEBUG>=2
			Plati( GP->Body[0] );
			Plati( GP->Body[1] );
			Plati( GP->Body[2] );
			Plati( GP->Hrany[0] );
			Plati( GP->Hrany[1] );
			Plati( GP->Hrany[2] );
			Plati( GP->Hrany[0]->L==GP || GP->Hrany[0]->P==GP );
			Plati( GP->Hrany[1]->L==GP || GP->Hrany[1]->P==GP );
			Plati( GP->Hrany[2]->L==GP || GP->Hrany[2]->P==GP );
		#endif
		/*
		GP->B=BarvaVyska(GP->Body[0]->Pos.y+GP->Body[1]->Pos.y+GP->Body[2]->Pos.y);
		*/
		y=Y=GP->Body[0]->Pos.y;
		a=GP->Body[1]->Pos.y;
		if( y>a ) y=a;
		else Y=a;
		a=GP->Body[2]->Pos.y;
		if( y>a ) y=a;
		ef( Y<a ) Y=a;
		/* aby nezalezelo na orientaci trojuhelniku, zachazime podle (max+min)/2 */
		a=(y+Y)>>1;
		GP->B=BarvaVyska(a+a+a);
	}
}

typedef long SmerKrokT[2][2];
typedef long SmeLKrokT[2][2]; /* long - aby nemusel rozsirovat znamenko */

static const SmerKrokT Smer[7]=
{
	/* posun x, posun y */
	{{  T_Z,    0  },{ 0,         T_V  }},
	{{ (T_Z/2), T_V},{-(T_Z*3/4), (T_V/2)}},
	{{-(T_Z/2), T_V},{-(T_Z*3/4),-(T_V/2)}},
	{{ -T_Z,    0  },{ 0,        -T_V  }}, /* [3]==-[0] */
	{{-(T_Z/2),-T_V},{ (T_Z*3/4),-(T_V/2)}}, /* [4]==-[1] */
	{{ (T_Z/2),-T_V},{ (T_Z*3/4), (T_V/2)}}, /* [5]==-[2] */
	{{  T_Z,    0  },{ 0,         T_V  }}, /* [6]== [0] */
};
static const SmeLKrokT SmeL[7]=
{
	{{+1, 0},{ 0,+1}},
	{{+1,+1},{-1, 0}},
	{{ 0,+1},{-1,-1}},
	{{-1, 0},{ 0,-1}}, /* [3]==-[0] */
	{{-1,-1},{+1, 0}}, /* [4]==-[1] */
	{{ 0,-1},{+1,+1}}, /* [5]==-[2] */
	{{+1, 0},{ 0,+1}}, /* [6]== [0] */
};

/*
    ----------
    \        /\
     \  A   /  \
      \    /    \
       \  /  B   \
        \/        \
         ----------
*/

#define RPMAX 32

/* ilustrace - postaveni bodu v trojuhelniku :
  GB+b(i)----HD----GB+b(i)+1-----------GB+b(i)+2
     \                / \                /
      \              /   \              /
       V            V     \            /
        \          /       \          /
         HA      HB         \        /
          \      /           \      /
           V    V             \    /
            \  /               \  /
             \/                 \/
             GB----->--HC-->----GB+1
                  (bezny dilec)
(sipky znaci orientaci hran - dulezite pri imp/exp)
  GB+b(i)----HD----GB+b(i)+1-----------GB+b(i)+2
     \                / \                /
      \              / | \              /
       V            V  V  \            /
        \          /   |   \          /
         HA      HB    HF   \        /
          \      /     |     \      /
           V    V      V      \    /
            \  /       |       \  /
             \/        |        \/
             GB->HC->GB+1->HE->GB+2
              (navazovaci dilec)
*/

/* HA odpovida v poli Hrany vzdy index 0 */
/* HB odpovida v poli Hrany vzdy index 2 */
/* HC odpovida v poli Hrany vzdy index 1 */
/* HE odpovida v poli Hrany vzdy index 1 */
/* HF odpovida v poli Hrany index 0 nebo 2 */

static int GenKrajSeg( int Seg, KresliScena *KS, const KrajDef *KD, Hrana *Imp1[], Hrana *Exp1[], Hrana *Imp2[], Hrana *Exp2[], long cx, long cy, int SGrid, int DelZ, int DelD )
{ /* Imp1 a Exp1 - navazovani na sousedni segmenty */
	/* Imp2 a Exp2 - navazovani na jemnejsi a hrubsi segment */
	int k,i; /* index v urovni, index urovne */
	long rx,ry; /* v souradnicich */
	long ax,ay;
	long cax,cay;
	const long (*S)[2]; /* krok v obou smerech - vlastne urcuje segment */
	const long (*C)[2]; /* krok v obou smerech - logic. souradnice */
	#define p(i) ( 2*(i)+1 ) /* pocet ploch i-te urovne */
	#define b(i) (i) /* pocet bodu i-te urovne */
	Bod *GB; /* prave akt. bod */
	Plocha *GP,*FP; /* prave akt. plocha - nad bodem */
	Hrana *GH; /* postupne pridavane hrany */
	GH=AlokHrany(KS,3*DelD*(DelD+1)/2-DelD-(3*DelZ*(DelZ+1)/2-DelZ)+DelZ);
	if( !GH ) return -1;
	GB=AlokBody(KS,DelD*(DelD+1)/2-DelZ*(DelZ+1)/2);
	if( !GB ) return -1;
	FP=GP=AlokPlochy(KS,DelD*DelD-DelZ*DelZ+DelZ);
	if( !GP ) return -1;
	#if __DEBUG>=2
		for( k=0; k<DelZ*3+1; k++ ) GP++->B=RGB(255,255,0),GP->Body[0]=GP->Body[1]=GP->Body[2]=NULL;
		for( i=DelZ+1; i<DelD; i++ ) for( k=0; k<p(i); k++ ) GP++->B=RGB(255,255,0),GP->Body[0]=GP->Body[1]=GP->Body[2]=NULL;
		Plati( GP==&KS->Plochy[KS->NPloch] );
		GP=FP;
	#endif
	FyzKoor(&rx,&ry,cx,cy);
	S=Smer[Seg];
	C=SmeL[Seg];
	/* preskoc vynechany zacatek */
	rx+=DelZ*( (S[1][0]<<SGrid)-((S[0][0]<<SGrid)>>1) );
	ry+=DelZ*( (S[1][1]<<SGrid)-((S[0][1]<<SGrid)>>1) );
	cx+=DelZ*(C[1][0]<<SGrid);
	cy+=DelZ*(C[1][1]<<SGrid);
	for( cax=cx,cay=cy,ax=rx,ay=ry,k=0; k<=DelZ; k++,ax+=S[0][0]<<SGrid,ay+=S[0][1]<<SGrid,cax+=C[0][0]<<SGrid,cay+=C[0][1]<<SGrid )
	{ /* cyklus pro nultou uroven - zjemnovaci dilec */
		/* vsude jakoby i==DelZ */
		Bod *GB0,*GB1,*GB2;
		Bod *GBbi,*GBbi1;
		if( DelZ>0 )
		{
			if( k<DelZ ) GB0=Imp2[k*2]->b1,GB1=Imp2[k*2]->b2,GB2=Imp2[k*2+1]->b2;
			else GB0=Imp2[DelZ*2-1]->b2,GB1=GB2=NULL;
		}
		else
		{ /* k==DelZ==0 */
			GB0=(Bod *)Imp2[0];
			GB1=GB2=NULL;
		}
		if( k>0 ) GBbi=GB+k-1;
		else GBbi=Imp1[DelZ]->b1;
		GBbi1=GB+k;
		#if __DEBUG>=2
			Plati( labs(GB0->Pos.x-ax)<fx(0.01) );
			Plati( labs(GB0->Pos.z-ay)<fx(0.01) );
		#endif
		/* nejsme na konci segmentu - dalsi hrany */
		if( k>0 )
		{ /* bezna hrana */
			GH->b1=GBbi,GH->b2=GB0; /* v obr. HA */
			GH->L=GP;
			GH->P=GP-1;
			GP[0].Hrany[2]=GP[-1].Hrany[2]=GH;
			GH++;
		}
		else /* import - navazani na predch. segment - HA */
		{
			Plati( !Imp1[DelZ]->L );
			Imp1[DelZ]->L=GP;
			GP[0].Hrany[2]=Imp1[DelZ];
		}
		GH->b1=GBbi1,GH->b2=GB0; /* v obr. HB */
		GH->P=GP,GP[0].Hrany[0]=GH;
		if( k<DelZ ) GH->L=GP+1,GP[1].Hrany[0]=GH;
		else GH->L=NULL,Exp1[DelZ]=GH;
		/* export - navazani na nasled. segmentu */
		GH++;
		if( k<DelZ ) /* nejsme na konci urovne */
		{
			/* urcite DelZ>0 */
			Imp2[k*2]->L=GP+1,GP[1].Hrany[1]=Imp2[k*2]; /* HC */
			Imp2[k*2+1]->L=GP+2,GP[2].Hrany[1]=Imp2[k*2+1]; /* HE */
			#if __DEBUG>=2
				Plati( GB0 );
				Plati( GB1 );
				Plati( GB2 );
				Plati( GBbi1 );
			#endif
			GH->b1=GBbi1,GH->b2=GB1; /* v obr HF */
			GH->L=GP+2,GH->P=GP+1;
			GP[2].Hrany[0]=GP[1].Hrany[2]=GH;
			GH++;
			/* horni plocha GP+1*/
			GP[1].Body[0]=GB0,GP[1].Body[1]=GB1,GP[1].Body[2]=GBbi1;
			/* horni plocha GP+2*/
			GP[2].Body[0]=GB1,GP[2].Body[1]=GB2,GP[2].Body[2]=GBbi1;
			/* GP bude inic. az jako dolni */
			GP+=3;
		}
		else GP++; /* k==DelZ */
	}
	/* dalsi - pravidelne urovne */
	rx+=S[1][0]<<SGrid,ry+=S[1][1]<<SGrid,rx-=(S[0][0]<<SGrid)>>1,ry-=(S[0][1]<<SGrid)>>1,cx+=C[1][0]<<SGrid,cy+=C[1][1]<<SGrid;
	for( i=DelZ+1; i<=DelD; i++,rx+=S[1][0]<<SGrid,ry+=S[1][1]<<SGrid,rx-=(S[0][0]<<SGrid)>>1,ry-=(S[0][1]<<SGrid)>>1,cx+=C[1][0]<<SGrid,cy+=C[1][1]<<SGrid )
	for( cax=cx,cay=cy,ax=rx,ay=ry,k=0; k<=i; k++,ax+=S[0][0]<<SGrid,ay+=S[0][1]<<SGrid,cax+=C[0][0]<<SGrid,cay+=C[0][1]<<SGrid )
	{ /* cyklus pro vsechny body segmentu */
		Bod *GB0,*GB1;
		Bod *GBbi,*GBbi1,*GB0_bi_1;
		if( k==0 ) /* nulty bod se musi importovat */
		{ /* import od souseda */
			if( i<DelD ) GB0=Imp1[i]->b2,GBbi=Imp1[i]->b1;
			else GB0=Imp1[i-1]->b1,GBbi=NULL;
			GB1=GB;
			GB0_bi_1=Imp1[i-1]->b2;
			GBbi1=GB+b(i);
			#if __DEBUG>=2
				Plati( labs(GB0->Pos.x-ax)<fx(0.01) );
				Plati( labs(GB0->Pos.z-ay)<fx(0.01) );
			#endif
		}
		else /* regulerni bod */
		{
			GB->Pos.x=ax;
			GB->Pos.z=ay;
			#if PULENI_T
			if( i<DelD || !(k&1) )
			{
			#endif
				GB->Pos.y=UrovenKraj(KD,cax,cay,SGrid);
			#if PULENI_T
			}
			else /* v posl. urovni liche body interpolaci */
			{
				GB->Pos.y=
				(
					UrovenKraj(KD,cax-(C[0][0]<<SGrid),cay-(C[0][1]<<SGrid),SGrid+1)+
					UrovenKraj(KD,cax+(C[0][0]<<SGrid),cay+(C[0][1]<<SGrid),SGrid+1)
				)
				>>1;
			}
			#endif
			GB0=GB;
			GB1=GB+1;
			GBbi=GB+b(i);
			GBbi1=GBbi+1;
			if( i>DelZ+1 ) GB0_bi_1=GB-b(i)+1;
			else /* dolni plocha se dotyka ciziho bodu */
			{ /* k>0 */
				if( k<=DelZ ) GB0_bi_1=Imp2[2*k-1]->b2;
				else GB0_bi_1=NULL;
			}
			GB++;
		}
		if( i<DelD ) /* nejsme na konci segmentu - dalsi hrany */
		{
			if( k>0 )
			{ /* bezna hrana */
				GH->b1=GBbi,GH->b2=GB0; /* v obr. HA */
				GH->L=GP;
				GH->P=GP-1;
				GP[0].Hrany[2]=GP[-1].Hrany[2]=GH;
				GH++;
			}
			else /* import - navazani na predch. segmentu */
			{
				Plati( !Imp1[i]->L );
				Imp1[i]->L=GP;
				GP[0].Hrany[2]=Imp1[i];
			}
			GH->b1=GBbi1,GH->b2=GB0; /* v obr. HB */
			GH->P=GP,GP[0].Hrany[0]=GH;
			if( k<i ) GH->L=GP+1,GP[1].Hrany[0]=GH;
			else GH->L=NULL,Exp1[i]=GH; /* i<DelD */
			/* export - navazani na nasled. segmentu */
			GH++;
		}
		if( k<i ) /* nejsme na konci urovne */
		{
			Plocha *PD;
			if( i==DelZ+1 ) /* jine indexy ploch v nulte urovni */
			{ /* prvni plocha v radku je GP-2*k */
				/* od ni minuly radek je -3*DelZ-1 */
				PD=GP-2*k-(3*DelZ+1)+3*k;
			}
			else PD=GP-p(i-1);
			/* urcite i>DelZ */
			GH->b1=GB0,GH->b2=GB1; /* v obr. HC */
			GH->P=PD,PD->Hrany[1]=GH;
			if( i<DelD ) GH->L=GP+1,GP[1].Hrany[1]=GH;
			else GH->L=NULL,Exp2[k]=GH; /* export pro hrubsi segment */
			GH++;
			#if __DEBUG>=2
				Plati( GB0 );
				Plati( GB1 );
			#endif
			if( i<DelD )
			{ /* horni plocha */
				#if __DEBUG>=2
					Plati( GBbi1 );
				#endif
				GP[1].Body[0]=GB0,GP[1].Body[1]=GB1,GP[1].Body[2]=GBbi1;
			}
			/* dolni plocha */
			#if __DEBUG>=2
				Plati( GB0_bi_1 );
			#endif
			PD->Body[0]=GB1,PD->Body[1]=GB0,PD->Body[2]=GB0_bi_1;
			GP+=2;
		}
		else GP++;
	}
	Plati( GH==&KS->Hrany[KS->NHran] );
	Plati( GB==&KS->Body[KS->NBodu] );
	SpoctiPlochy(FP,&KS->Plochy[KS->NPloch]);
	return 0;
}

static int GenOkrSeg( int Seg, KresliScena *KS, const KrajDef *KD, Hrana *Exp1[], Hrana *Imp2[], long cx, long cy, int SGrid, int DelZ, int DelD )
{ /* Exp1 - navazovani na sousedni segmenty */
	int i; /* index v urovni, index urovne */
	long rx,ry; /* v souradnicich */
	const long (*S)[2]; /* krok v obou smerech - vlastne urcuje segment */
	const long (*C)[2]; /* krok v obou smerech - logic. souradnice */
	Bod *GB; /* prave akt. bod */
	Hrana *GH; /* postupne pridavane hrany */
	GH=AlokHrany(KS,DelD-DelZ);
	if( !GH ) return -1;
	GB=AlokBody(KS,DelD-DelZ);
	if( !GB ) return -1;
	FyzKoor(&rx,&ry,cx,cy);
	S=Smer[Seg];
	C=SmeL[Seg];
	rx+=DelZ*( (S[1][0]<<SGrid)-((S[0][0]<<SGrid)>>1) );
	ry+=DelZ*( (S[1][1]<<SGrid)-((S[0][1]<<SGrid)>>1) );
	cx+=DelZ*(C[1][0]<<SGrid);
	cy+=DelZ*(C[1][1]<<SGrid);
	for( i=DelZ; i<=DelD; i++,rx+=S[1][0]<<SGrid,ry+=S[1][1]<<SGrid,rx-=(S[0][0]<<SGrid)>>1,ry-=(S[0][1]<<SGrid)>>1,cx+=C[1][0]<<SGrid,cy+=C[1][1]<<SGrid )
	{ /* cyklus pro okraj segmentu */
		/* vsechny body exportujeme */
		/* jen prvni importujeme z predch. hrubosti */
		Bod *GB0,*GB1;
		if( i>DelZ )
		{
			GB->Pos.x=rx;
			GB->Pos.z=ry;
			GB->Pos.y=UrovenKraj(KD,cx,cy,SGrid);
			GB0=GB;
			GB1=GB+1;
			GB++;
		}
		else
		{
			if( DelZ>0 ) GB0=Imp2[0]->b1;
			else GB0=(Bod *)Imp2[0];
			GB1=GB;
		}
		if( i<DelD ) /* nejsme na konci segmentu - dalsi hrany */
		{
			GH->b1=GB1,GH->b2=GB0; /* v obr. HA */
			GH->L=NULL;
			GH->P=NULL;
			Exp1[i]=GH;
			GH++;
		}
	}
	Plati( GH==&KS->Hrany[KS->NHran] );
	Plati( GB==&KS->Body[KS->NBodu] );
	return 0;
}

static int GenZacSeg( int Seg, KresliScena *KS, const KrajDef *KD, Hrana *Exp1[], long cx, long cy, int SGrid, int DelZ )
{ /* Exp1 - navazovani na sousedni segmenty */
	int i; /* index v urovni, index urovne */
	long rx,ry; /* v souradnicich */
	const long (*S)[2]; /* krok v obou smerech - vlastne urcuje segment */
	const long (*C)[2]; /* krok v obou smerech - logic. souradnice */
	Bod *GB; /* prave akt. bod */
	Hrana *GH; /* postupne pridavane hrany */
	GH=AlokHrany(KS,DelZ);
	if( !GH ) return -1;
	GB=AlokBody(KS,DelZ+1);
	if( !GB ) return -1;
	FyzKoor(&rx,&ry,cx,cy);
	S=Smer[Seg];
	C=SmeL[Seg];
	/* posun na pocatecni bod */
	rx+=DelZ*( (S[1][0]<<SGrid)-((S[0][0]<<SGrid)>>1) );
	ry+=DelZ*( (S[1][1]<<SGrid)-((S[0][1]<<SGrid)>>1) );
	cx+=DelZ*(C[1][0]<<SGrid);
	cy+=DelZ*(C[1][1]<<SGrid);
	for( i=0; i<=DelZ; i++,rx+=S[0][0]<<SGrid,ry+=S[0][1]<<SGrid,cx+=C[0][0]<<SGrid,cy+=C[0][1]<<SGrid )
	{ /* cyklus pro zacatek segmentu */
		/* vsechny body exportujeme */
		GB->Pos.x=rx;
		GB->Pos.z=ry;
		GB->Pos.y=UrovenKraj(KD,cx,cy,SGrid);
		if( i<DelZ ) /* nejsme na konci segmentu - dalsi hrany */
		{
			GH->b2=GB+1,GH->b1=GB; /* v obr. HA */
			GH->L=NULL;
			GH->P=NULL;
			Exp1[i]=GH;
			GH++;
		}
		GB++;
	}
	Plati( GH==&KS->Hrany[KS->NHran] );
	Plati( GB==&KS->Body[KS->NBodu] );
	return 0;
}
static Bod *GenZacBod( KresliScena *KS, const KrajDef *KD, long cx, long cy, int SGrid )
{ /* Exp1 - navazovani na sousedni segmenty */
	/* singularni pripad GenZacSeg */
	long rx,ry; /* v souradnicich */
	Bod *GB; /* prave akt. bod */
	GB=AlokBody(KS,1);
	if( !GB ) return GB;
	FyzKoor(&rx,&ry,cx,cy);
	/* bod exportujeme */
	GB->Pos.x=rx;
	GB->Pos.z=ry;
	GB->Pos.y=UrovenKraj(KD,cx,cy,SGrid);
	return GB;
}

#define P_ZUHEL 45 /* pol. zorny uhel */
#define U_REZ 15 /* rezerva v uhlu na okrajich */
/* celk. zorny uhel musi byt 2*60 stupnu - vzdy prave 3 segmenty */

#define MAX_SEG 6
/* ve skutecnosti se zobrazi nejvyse 3 segmenty */
/* ale indexy nabyvaji vsech 6 hodnot */

/* cachovani vygenerovane sceny */

typedef Hrana *HrubIE[RPMAX*2];

void ZacCache( KCache *CS )
{
	CS->SegZ=-1,CS->SegK=-1;
	CS->cx=-1,CS->cy=-1;
}

UKresliScena GenKraj( KCache *CS, KresliScena *KS, const KrajDef *KD, const StatickaKamera *P, int Detail )
{ /* Detail je po�et general. �rovn� - obvykle  0..1 */
	/* prenosy hran mezi segmenty */
	Hrana *Imp1[RPMAX*2],*Exp1[RPMAX*2];
	HrubIE Imp2[MAX_SEG],Exp2[MAX_SEG];
	Hrana **I1,**E1;
	HrubIE *I2,*E2;
	int SGrid0=SGRIDP+Detail;
	int SegZ,SegK; /* cisla segmentu proti smeru hod. rucicek! */
	int Seg,SegH;
	int ret;
	int SGrid,SGridZ;
	long cx,cy;
	int uy;
	if( P->Pos.y>fxmetr(4000) )
	{ /* z vysky neni videt vubec nic */
    UKresliScena uks;
    uks.vboIndex = 0;
		return uks;
	}
	uy=NormUhel(P->Uhel.y+P->PoUhel.y);
	/* neni uplne korektni - zanedbava otaceni v osach x a z */
	if( uy<30*UhelPom ) uy+=360*UhelPom; /* jen kladne! */
	SegZ=(int)((uy+(30-P_ZUHEL-U_REZ)*UhelPom)/(60*UhelPom));
	SegK=(int)((uy+(30+P_ZUHEL+U_REZ)*UhelPom)/(60*UhelPom));
	SegH=(int)((uy+30*UhelPom)/(60*UhelPom));
	if( SegZ>=6 ) SegZ-=6;
	if( SegK>=6 ) SegK-=6;
	if( SegH>=6 ) SegH-=6;
	LogRoundXY(&cx,&cy,P->Pos.x,P->Pos.z,SegH,SGrid0);
	#if S_CACHE
		if( cx==CS->cx && cy==CS->cy )
		{
			int CSK=CS->SegK,SZ=SegZ,SK=SegK;
			if( CSK<CS->SegZ ) CSK+=6;
			if( SZ<CS->SegZ ) SZ+=6;
			if( SK<CS->SegZ ) SK+=6;
			if( SZ>=CS->SegZ && SZ<=CSK && SK>=CS->SegZ && SK<=CSK )
			{
				return CS->KS;
			}
		}
		CS->cx=CS->cy=-1;
		CS->SegZ=CS->SegK=-1;
	#endif
	ZacHrany(KS);
	{
		Bod *GB=GenZacBod(KS,KD,cx,cy,SGRIDP);
    if( !GB )
    {
      UKresliScena uks;
      uks.vboIndex = 0;
	    return uks;
    }
		for( Seg=SegK;;)
		{
			Exp2[Seg][0]=(void *)GB;
			if( Seg==SegZ ) break;
			Seg--;if( Seg<0 ) Seg+=6;
		}
	}
	I2=Imp2,E2=Exp2;
	SGridZ=SGRIDP;
	for( SGrid=SGridZ; SGrid<=SGridZ+Detail; SGrid++ )
	{
		HrubIE *p;
		int dz;
		p=I2,I2=E2,E2=p;
		if( SGrid==SGridZ ) dz=0;
		else dz=RozmerPlatu[SGrid-1]/2;
		ret=GenOkrSeg(SegK,KS,KD,Exp1,I2[SegK],cx,cy,SGrid,dz,RozmerPlatu[SGrid]);
    if( ret<0 )
    {
      UKresliScena uks;
      uks.vboIndex = 0;
		  return uks;
    }
		for( Seg=SegK,I1=Imp1,E1=Exp1;;)
		{
			Hrana **p;
			p=I1,I1=E1,E1=p;
			ret=GenKrajSeg(Seg,KS,KD,I1,E1,I2[Seg],E2[Seg],cx,cy,SGrid,dz,RozmerPlatu[SGrid]);
      if( ret<0 )
      {
        UKresliScena uks;
        uks.vboIndex = 0;
		    return uks;
      }
			if( Seg==SegZ ) break;
			Seg--;if( Seg<0 ) Seg+=6;
		}
	}
	/* preved z bufferu */
	#if S_CACHE
		UlozKS(&CS->KS,KS);
		CS->cx=cx,CS->cy=cy;
		CS->SegZ=SegZ,CS->SegK=SegK;
	  return CS->KS;
  #else
    UKresliScena uks;
		UlozKS(&uks,KS);
    return uks;
	#endif
}

#define MAP_R fx(0.90)
#define MAP_G fx(0.95)
#define MAP_B fx(1.00)

typedef struct fyz_rgb
{
	unsigned r: 5;
	unsigned g: 6;
	unsigned b: 5;
};

Flag MapaKrajiny( VRAMPixel *VidAd, ClipRect *C, KrajDef *KD, const PosT *P, int MSGrid, long LPix, int (*AbFunc)( void ) )
{
	long x0,z,x;
	int xg,yg;
	int cnt=0;
	VRAMPixel *VAd=VidAd+(long)C->y1*LPix+C->x1;
	x0=P->x,z=P->z;
	MSGrid-=MapLog;if( MSGrid<0 ) MSGrid=0;
	x0-=(C->x2+1-C->x1)/2*(T_Z<<(MSGrid+MapLog));
	z+=(C->y2+1-C->y1)/2*(T_Z<<(MSGrid+MapLog));
	for( yg=C->y1; yg<=C->y2; yg++,z-=T_Z<<(MSGrid+MapLog) )
	{
		VRAMPixel *VA=VAd;
		VAd+=LPix;
		for( x=x0,xg=C->x1; xg<=C->x2; xg++,x+=T_Z<<(MSGrid+MapLog) )
		{
			int Sg=COL_G(*VA);
			//if( Sg )
			{
				long dx,dz;
				long v=UrovenVBode(KD,x/FACTOR_2D,z/FACTOR_2D,&dx,&dz);
				Barva B=BarvaVyska(v+v+v);
				BSlozka r,g,b;
				long Stin=(dx+dz);
				if( Stin>fx(+2.0) ) Stin=fx(+2.0);
				ef( Stin<fx(-2.0) ) Stin=fx(-2.0);
				/* z <-2.0,+2.0> */
				Stin+=fx(2.0);
				Stin>>=1;
				if( Sg!=56 ) Stin*=Sg,Stin/=56;
				r=(BSlozka)MF(TR(B),Stin),g=(BSlozka)MF(TG(B),Stin),b=(BSlozka)MF(TB(B),Stin);
				r=(BSlozka)MF(r,MAP_R),g=(BSlozka)MF(g,MAP_G),b=(BSlozka)MF(b,MAP_B);
				if( r>MAX_BSLOZKA ) r=MAX_BSLOZKA;
				if( g>MAX_BSLOZKA ) g=MAX_BSLOZKA;
				if( b>MAX_BSLOZKA ) b=MAX_BSLOZKA;
				SRGBA(&B,r,g,b,0);
				*VA=KonvBarva(&B);
			}
			VA++;
		}
		if( --cnt<0 ) CountDown(-1),cnt=16;
		if( AbFunc() ) return True;
	}
	return False;
}

