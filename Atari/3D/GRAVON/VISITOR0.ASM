; VISITOR.ASM
; SUMA 2/1994-4/1994

; vektorove vypocty
; pridano ozvuceni (viz syntsum)
; nocni videni
; uz neobsahuje supersampling

visitor  ident   1,0

	nolist
	include "intequ.asm"
	include "ioequ.asm"
	include "macros.asm"
	list

SAFE equ 0 ; jako __DEBUG v C - delej neco pro jistotu

BufferOutput: equ 0 ; komprimovany vystup z-bufferu najednou?

; Cast promennych se vejde do internal RAM
	org l:0
; internal L: pro grafiku
; obecne registry

Var0: ds 1
Var1: ds 1
Var2: ds 1
; pouzivame Var0..Var2 pro kodovani barev

; spec. registry
EAET: ds 1

	swspc x

NAET: ds 1

Svetlo: ds 3 ; smer svetla
Osvetleni: ds 3 ; barva svetla (i intensita)
Rozptyl: ds 3 ; rozptylene svetlo
Pozadi: ds 3 ; barva pozadi (pro mlhu)
NocniEfekt: ds 1 ; prispevek r,g,b k celk. intensite
KonSvetla: equ *

NLamp: ds 1

NPloch: ds 1
ROut: ds 1
PoloFlag: ds 1
Clipw: ds 1 ; v X fyz. pocet pixelu, Y log. (antialiasing)

	swspc l

Nebe: ds 3 ; stejne usporadani, jako maji plochy

; nutne parametry pro zvuk
CRA_BITS	EQU	$4100
CRB_BITS	EQU	$F800
; stereo transmit+interrupt+receive+interrupt
; receive je 8-bitu stereo
; internal L: pro zvuky

; definice aktualnich parametru zvuku v kanale
	
; X00: citac          Y00: perioda
;                     Y01: difer. periody
; L02: posl. data
; X03: krok           Y03: offset
; X04: ukaz. na obalku Y04 pointer na proceduru
; X05: seed pro random Y05 ukazatel do samplu
; X06: hlasitost L    Y06: hlasitost P
; X07: dif. hlas. L   Y07: dif. hlasitosti P

K_Delka: equ 8

	swspc l

SavePR: ds 1
WordIn: ds 1

ZvukBufPtr: ds 1 ; X: je �ten� (Ptr), Y: je z�pis (Fil)
	swspc x
SndLock: ds 1
SampleEnd: ds 1

	swspc l
NKanalu equ 6
Kanaly: ds K_Delka*NKanalu

	swspc l

MaxLamp equ 8
Lampy: ds MaxLamp*6
; rozlozeni lampy:
;      X       Y
; 00   Pos.x   Smer.x
; 01   Pos.y   Smer.y
; 02   Pos.z   Smer.z
; 03   r
; 04   g
; 05   b

; pouziva se behem vypoctu osvetleni:
	swspc x
LampyUhrn: ds 3 ; kolik davaji vsechny lampy dohromady pro danou plochu
RelPos: ds 3 ; rel. pozice plochy 
NocniKoef:  ; koeficienty citlivosti cipku na r,g,b
	dc 0.150 ; r
	dc 0.999 ; g
	dc 0.700 ; b

; sekce vektoru

	org p:$0
	jmp start

	org p:SSIRD
	jsr datain
	org p:SSIRDE
	jsr datain

	org p:SSITD
	jsr dataout
	org p:SSITDE
	jsr dataout
 
; sekce hlavniho kodu
; nejprve grafika

	org p:$40

; prevzato z DSP_SMPL

; Full 23 bit precision square root routine
; y  = double precision (48 bit) positive input number
; b  = 24 bit output root
; uses a,x0,x1
 
Sqrt48_24:
	clr     b    #<$40,x0           ; init root and guess
	move                    x0,x1   ; init bit to test
	do      #23,_sqendl
		mpy     -x0,x0,a                ; square and negate the guess
		add     y,a                     ; compare to double precision input
		tge     x0,b                    ; update root if input >= guess
		tfr     x1,a                    ; get bit to test
		asr     a                               ; shift to next bit to test
		add     b,a             a,x1            ; form new guess
		move                    a,x0            ; save new guess
	_sqendl:
	rts

diva: macro rg
	; 4-Quadrant division, 24-bit signed quotient
	; result in A0
	; uses A,B
	ABS A	A,B				;make dividend positive, copy A1 to B1
	EOR	rg,B				;save quo. sign in N
	AND	#$FE,CCR		;clear carry bit C (quotient sign bit)
	REP	#$18				;form a 24-bit quotient
	DIV	rg,A				;form quotient in A0, remainder in A1
	JPL	<_SAVEQUO		;go to _SAVEQUO if quotient is positive
		NEG	A					;complement quotient if N bit set
	_SAVEQUO:				;
	endm

NormujR0:
; znormuje z X:R0 do Y:R0, posune R0 o 3
; vytvari normovany vektor k zadanemu vektoru
; slozky v X:R0 by mely byt zhruba 23 bitu
; nici R2
	move R0,R2
	move X:(R0)+,X0
	mpy X0,X0,A X:(R0)+,X0
	mac X0,X0,A X:(R0)+,X0
	mac X0,X0,A
	move A1,Y1
	move A0,Y0
	jsr <Sqrt48_24
	move R2,R0
	move B,Y0
	do #3,_divveclop
		move X:(R0),A
		diva X0 
		move A0,Y:(R0)+
	_divveclop:
	rts

NormalaR0doR1:
; ze dvou vektoru z X:R0, Y:R0,
; vytvori normalu do X:R1, normalizovnou do Y:R1
; posune R0 o 3, R1 o 3
; nici R2-R5, N1, N4

	lua (R0)+,R3 ; R3=slozka y
	move R0,R4   ; R0=R4=slozka x
	lua (R3)+,R5 ; R5=slozka z
	nop
	
;	P->Normala.x=IMF(va.y,vb.z)-IMF(va.z,vb.y);
	move X:(R3),X0 Y:(R5),Y0
	mpy X0,Y0,A X:(R5),X0 Y:(R3),Y0
	mac -X0,Y0,A
	abs A A,L:(R1)+
	tfr A,B ; v B vytvarej maximum abs. h. slozek
		
;	P->Normala.y=IMF(va.z,vb.x)-IMF(va.x,vb.z);
	move X:(R5),X0 Y:(R0),Y0
	mpy X0,Y0,A X:(R0),X0 Y:(R5),Y0
	mac -X0,Y0,A
	abs A A,L:(R1)+
	
	cmp B,A
	tgt A,B ;  B=max(A,B)
	
;	P->Normala.z=IMF(va.x,vb.y)-IMF(va.y,vb.x);
	move X:(R4),X0 Y:(R3),Y0
	mpy X0,Y0,A X:(R3),X0 Y:(R4),Y0
	mac -X0,Y0,A
	abs A A,L:(R1)+

	cmp B,A
	tgt A,B ;  B=max(A,B)

	; v L:(R1) to mas - ted to priblizne normalizuj - v B mas velikost
	move #-3,N1 ; N1 dopredu optim.
	move #0.2,X1
	do #22,_nploop
		cmp X1,B
		jlt <_donorm
			enddo ; term. loop
			jmp <_nploop
		_donorm:
		move (R1)+N1
		asl B L:(R1),A
		asl A
		move A,L:(R1)+
		move L:(R1),A
		asl A
		move A,L:(R1)+
		move L:(R1),A
		asl A
		move A,L:(R1)+
	_nploop:

	lua (R1)+N1,R0
	move #3,N4 ; N4 dopredu
	jsr <NormujR0 ; pozor - nici R2
	; v R1 jiz je konec vystupu
	lua (R4)+N4,R0 ; preskoc vektor na vstupu
	rts

start:
	InitGeneral
	InitStack
	
	jsr <InitZvuky
	InitSSI
;cekame na povel
; v tomto LODu se nikde M-ka nepouzivaji - toho muzes vyuzit v preruseni

main:
	; centralni vstup povelu
	ComRcv:
		move L:ZvukBufPtr,AB ; pokus se zaplnit kus bufferu do z�soby
		sub A,B #>DelZvukBuf-1,X0
		and X0,B1 #>DelZvukBuf-16,X0
		move B1,B ; B=(ZvukFil-ZvukPtr)%DelZvukBuf
		cmp X0,B
		jgt <_Enough
			jsr <datafill
			move #-1,M0
		_Enough:
	jclr #0,X:<<HSR,ComRcv
	movep X:<<HRX,X0
	
	jset #4,X0,DSvetlo
	jset #5,X0,DOsvetleni
	jset #6,X0,DExpInit
	jset #7,X0,DKresli
	jset #8,X0,DPlochy

	jset #23,X0,DefZvuku
	jset #22,X0,ZniZvuk
	jset #21,X0,ZmenZvuk
	jset #20,X0,LoadSample
	
	if SAFE
	; nesmyslny povel ukonci zpracovani
	; dava ven prazdny vystup - same -1.0
	_End:
		_dummyr: jclr #0,X:<<HSR,_dummys
		movep X:<<HRX,X0
		_dummys: jclr #1,X:<<HSR,_End
		movep #-1.0,X:<<HTX
		jmp <_End
	else
		movep X:SavePR,X:<<IPR ; vrat IPR
		movep #0,X:<<CRB ; disable interr.
		Lop: jmp <Lop
	endif

DPlochy:
; inic. ploch - pro pozdejsi kresleni a stinovani
	move #Plochy,R0
	_rcv1: jclr #0,X:<<HSR,_rcv1
	movep X:<<HRX,X0
	do X0,_InPlochy
		; cteme barevne slozky
		_rcvbar: jclr #0,X:<<HSR,_rcvbar
		movep X:<<HRX,Y0 ; barva
		jclr #16,Y0,_BarOK ; barva ma 16 bitu - vice je konec
			enddo
			jmp <_InPlochy
		_BarOK:
		move Y0,Y:(R0)+
		move (R0)+ ; vynech misto
	_InPlochy:
	jmp <main

ReadRGB macro ColSpace,ColReg
; vysledek je v A1, nici B
	clr B #0,A ; nutno zamezit saturaci v B
	move ColSpace:(ColReg)+,B0 ; slozka R - 23 bitu
	asl B
	move B0,A0
	rep #5 ; bitu ve slozce
	asl A
	move ColSpace:(ColReg)+,B0 ; slozka G - 23 bitu
	asl B ; odstran znamenko z B0
	move B0,A0
	rep #6
	asl A
	move ColSpace:(ColReg)+,B0 ; slozka B - 23 bitu
	asl B ; odstran znamenko z B0
	move B0,A0
	rep #5
	asl A
	endm

PloPtr: macro Ac
		move #>-1,Ac
		jset #15,X0,_NoPlo
			bclr #14,X0
			move #>Plochy,Y1
			jcc <_NeniPolo
				bset #14,Y1
			_NeniPolo:
			move X0,Ac
			asl Ac ; vynasob - plocha ma vel. 3
			add X0,Ac
			add Y1,Ac
		_NoPlo:
	endm

ZBInLine:
	; hrany se prijimaji na Y:EAET
	; prijmi pocty hran
	
	_rcv2: jclr #0,X:<<HSR,_rcv2
	movep X:<<HRX,X:Clipw

	_rcv3: jclr #0,X:<<HSR,_rcv3
	movep X:<<HRX,R3 ; pocet novych hran

	move R3,A
	tst A
	jeq <NoDataIn
	
		; prijmi jednu radku pro z-buffer
		; P/L mysleno pri pohledu zhora dolu
		move Y:EAET,R0 ; hrany ukladame za konec
		
		do R3,ZBinput
	
			_rcvx: jclr #0,X:<<HSR,_rcvx ; x
			movep X:<<HRX,X:(R0)
			move #0.5,X0
			move X0,Y:(R0)+
			
			_rcvz: jclr #0,X:<<HSR,_rcvz ; z
			movep X:<<HRX,X:(R0)
			move #0,X0
			move X0,Y:(R0)+
	
			_rcvL: jclr #0,X:<<HSR,_rcvL ; index L-plochy
			movep X:<<HRX,X:(R0)
			
			_rcvP: jclr #0,X:<<HSR,_rcvP ; index P-plochy
			movep X:<<HRX,Y:(R0)+
	
			_rcdxh: jclr #0,X:<<HSR,_rcdxh ; dx hi 16b
			movep X:<<HRX,X:(R0)
			_rcdxl: jclr #0,X:<<HSR,_rcdxl ; dx low 16b
			movep X:<<HRX,Y:(R0)+ ; pozdeji zkonvertuj!
	
			_rcdzh: jclr #0,X:<<HSR,_rcdzh ; dz hi 16b
			movep X:<<HRX,X:(R0)
			_rcdzl: jclr #0,X:<<HSR,_rcdzl ; dz low 16b
			movep X:<<HRX,Y:(R0)+ ; pozdeji zkonvert.
	
			move (R4)+ ; zapocti hranu do poctu

			_rchh: jclr #0,X:<<HSR,_rchh ; hh
			movep X:<<HRX,X:(R0)+

		ZBinput:
		move R0,Y:EAET ; prijmovy citac
	NoDataIn:
	; pocet vrat v R3
	nop
	rts

ZBKonvLine:
	; konvertuje od X:EAET, pocet R3
	move R3,A
	tst A
	jeq <NoDataKonv

		move #>(1<<7),X1 ; p�iprav rotaci <<8
		move #2,N0
		move X:EAET,R0
		
		do R3,ZBkonvert
			move (R0)+N0 ; preskoc x a z
			move X:(R0),X0 ; spocti ukaz. na plochu L
			PloPtr A ; ni�� A, Y1
			move Y:(R0),X0 ; spocti ukaz. na plochu P
			PloPtr B ; ni�� B, Y1
			move AB,L:(R0)+ ; uloz ukazatele
			
			move Y:(R0),X0 ; konvertuj dx lo
			mpy X1,X0,A
			move A0,Y:(R0)+
	
			move L:(R0),Y ; zpracuj 16.16
			mpy X1,Y0,A Y1,B
			move A0,B0
			rep #8 ; konvertuj z 16.16 na 24.8
			asl B
			move B,L:(R0)+N0 ; posun a preskoc hh
		ZBkonvert:
		move R0,X:EAET ; zapocti je z prijatych do celkovych
		
	NoDataKonv:
	nop
	rts

ZBuffer:
	; provede vykresleni jednoho radku na adresu X:ROut
	; v PoloFlag zachazeni s polopruheldnymi plochami	
	move X:ROut,R2
	move R2,R1 ; na adr. R2 zapisujeme pri z-bufferu
	move #$7fffff,Y0
	move #>Nebe,Y1 ; predstirej, ze je to plocha
	do X:Clipw,InitLoop
		move Y,L:(R1)+
		nop
	InitLoop:
	
	; rep X:Clipw ; rep nejde prerusit - to pusobilo ztratu sync. u zvuku
	; move Y,L:(R1)+
	; X:(X:ROut) jsou indexy plochy
	; Y:(X:ROut) je z-buffer

	move X:NAET,A ; jen aktivni
	tst A
	jeq <NoData21
	
	move #AET,R0
	move #3,N0
	do X:NAET,PPruchod ; vyhledej konce ploch
		move X:(R0)+,A ; sour.x
		move X:(R0)+,B ; sour.z
		move Y:(R0)+,R3 ; ktera plocha je P
		jset #15,R3,_NoP
			move A,X:(R3)+ ; zapamatuj posici konce
			move B,X:(R3) ; zapamatuj posici konce
		_NoP:
		move (R0)+N0 ; preskoc dz,dx a h
	PPruchod:
	
	move #AET,R0
	do X:NAET,TPruchod
		move #1,N0
		move X:(R0)+,A ; sour. x - zacatek (L)
		move X:(R0+N0),R3 ; kdo je L
		jset #15,R3,NoDraw
			bclr #14,R3
			jcc <_NeniPolo
				jset #0,X:PoloFlag,NoDraw ; radek, na kterem se nekresli polop. plochy
			_NeniPolo:
			move X:(R3)+,B ; x - konec
			move X:Clipw,X1
			move AB,L:Var1 ; uloz zac. a kon
			cmp X1,B
			tgt X1,B ; B=min(Clipw,B)
			tst A #0,X1
			tlt X1,A ; A=max(0,A);
			sub A,B A,N4 ; spocitej sirku useku
			jle <Orient
				; vypocitej si dzdx do X1
				move B,X:Var0 ; R3 ukazuje na z
				
				move L:Var1,BA ; A = x kon, B = x zac
				sub B,A R2,R4
				move X:(R0),X0 A,Y0 ; X0 = z zac, Y0 = delta X
				move X:(R3)-,B ; z kon
				sub X0,B ; B = delta z
				; z jsou od ( 0.0 do 1.0 ), proto |dz|<1.0
				move B,X1 ; X1 = delta z

				move X:Var1,A
				tst A A,Y1 ; Y1 = x zac
				jlt <_ARez
					move #>1,X0
					mpy X1,X0,A ; A = dz>>23
					diva Y0 ; A0=dz/dx, nici B, predpoklada |dz|<dx (vime, ze dx>=1, |dz|<1 )
					move A0,X1 ; X1 je dzdx

					move X:(R0),B ; B = z zac
					; je-li Y0 moc velke, dojde ke ztrate presnosti
					; ta je ovsem v principu interpolace, a na obrazovce nemuze zpusobit velke potize
				jmp <_ADone

				_ARez:
					mpy -X1,Y1,A ; (X1*-Y1)/Y0+X:(R0)
					diva Y0
					move A0,B
					move X:(R0),A
					add A,B ; B = Z(0)
					move B,Y1
					
					; deleni stejne musim udelat! (nech Y0 a X1)
					move #>1,X0 ; vypocet dzdx
					mpy X1,X0,A ; A = dz>>23
					diva Y0 ; A0 = dz/dx, nici B, predpoklada |dz|<dx (vime, ze dx>=1, |dz|<1 )
					move A0,X1 ; X1 = dzdx
					move Y1,B

				_ADone:
				move (R4)+N4 ; kde je zacatek
				move R3,A ; p�iprav si L: move
				do X:Var0,_ZLoop
					move Y:(R4),Y0
					cmp Y0,B
					jge <_NotZ
						move AB,L:(R4) ; uloz adresu plochy a z-souradnici
					_NotZ:
					add X1,B (R4)+ ; interpoluj z, posun na dalsi pixel
				_ZLoop:
			Orient: ; zadni strany vubec nekresli
		NoDraw:

		; pri vypnutem antialiasingu Y nebude 2. pruchod
		; musis vsechny interpolace provest 2x
		move #3,N0
		move L:-(R0),A ; x
		move L:(R0+N0),B ; dx
		add B,A
		move A,L:(R0)+ ; x+dx
		
		move L:(R0),A ; z
		move L:(R0+N0),B ; dz
		add B,A
		move A,L:(R0)+ ; z+dz

		move X:(R0+N0),R3 ; hh
		move (R0)+N0 ; preskoc LP, dx, dz
		move (R3)-
		move R3,X:(R0)+ ; hh-1
		
	TPruchod:

	NoData21:

	move X:NAET,A ; i neaktivni pripravene hrany
	tst A
	jeq <NoData22
	
	move #5,N0
	move #AET,R0
	move R0,R1
	move #0,R2
	do X:NAET,VyhodZAET
		move X:(R0+N0),A
		tst A
		jle <_VynechHo
			do #6,_MoveItem ; zkopiruj hrany
				move L:(R0)+,A
				move A,L:(R1)+
			_MoveItem:
			move (R2)+
			jmp <_Continue
		_VynechHo: ; vynechej hranu v seznamu
			move (R0)+N0
			move (R0)+
		_Continue:
		nop
	VyhodZAET:

	move R2,X:NAET ; odeber zrusene hrany

	NoData22:
	nop
	rts
	
DKresli:
; z-buffer, sam si pocita dzdx

	; prijmi nove hrany	pro oba radky antialiasingu
	; prijima se na pozici Y:EAET
	
	move X:EAET,X0
	move X0,Y:EAET ; prijmovy ukazatel na celkovy

	move X:NAET,R4 ; v R4 pocitame hrany
	
	jsr <ZBInLine ; prijem 1. radky, cita se v Y:EAET

	move R4,X:NAET ; NAET - celkovy pocet hran

	move X:Clipw,A ; spocti rozmer pro antialiasing
	; v R3 se predava pocet prijatych hran
	
	jsr <ZBKonvLine ; konvertuj nove prijate, cita se v X:EAET

	move #Rad1Out,R0
	move R0,X:ROut ; zpracuj 1. radku
	bchg #0,X:PoloFlag
	jsr <ZBuffer
	
	move X:NAET,A ; najdi &AET[NAET]
	
	asl A
	asl A A,B
	add B,A #AET,X0 ; A=A*6

	add X0,A  ; A=A+AET
	move A,X:EAET ; uloz konec AET do EAET pro dalsi radek
	
	; zpracuj vysledky z obou radku
	
	move #Rad1Out,R1
	move R1,R0 ; vystupni buffer
	move #2,N3
	move N3,N5
	do X:Clipw,PloKonv ; preved z ploch do 16-ti bit. barev
		move X:(R1)+,R3
		nop
		move X:(R3+N3),A1 ; barva plochy
		move A1,X:(R0)+ ; uloz ji
	PloKonv:
	
	move #0,R2 ; pocitadlo opak.
	if BufferOutput
		move R2,R3 ; pocitadlo delky
	endif
	move #Rad1Out,R0 ; zdroj
	move R0,R1 ; cil
	move X:(R0),X0 ; posl. hodnota - nastav na prvni
	move #>250,Y0	; max. opakov�n�
	move #>(1<<15),Y1 ; p�iprav si posouv�n�
	do X:Clipw,KompLop ; komprese dat - pro snizeni zatizeni prenosem
		move X:(R0)+,A
		cmp X0,A
		jne <_SKomp
		move R2,B
		cmp Y0,B
		jlt <_Komp
			_SKomp:
			SaveKomp: macro
				move R2,B
				tst B B,X1
				jeq <_EndSav
					move X0,B0
					if BufferOutput
						mac X1,Y1,B (R3)+
						move B0,X:(R1)+
					else
						mac X1,Y1,B
						_send: jclr #1,X:<<HSR,_send
						movep B0,X:<<HTX
					endif
				_EndSav:
			endm
			SaveKomp
			move #0,R2 ; nove pocitadlo
			move A,X0 ; nova hodnota
		_Komp:
		move (R2)+
	KompLop:

	SaveKomp

	if BufferOutput	
		move #Rad1Out,R0
		do R3,KSendLop
			_snd: jclr #1,X:<<HSR,_snd
			movep X:(R0)+,X:<<HTX
		KSendLop:
	endif
	
	jmp <main


SpoctiNocni:
	; vstup/vystup Y:(R1) barva
	; nici A,B,X
	; R1 posune o 3
	
	move #NocniKoef,R2 ; koeficienty citlivosti cipku na r,g,b
	clr A
	do #3,LSpoctiNocni
		; v B je vysledna barva
		; jeste ji prozeneme nocnim videnim
		move X:NocniEfekt,X0
		move X:(R2)+,X1
		mpy X0,X1,B
		move B,X1
		move Y:(R1)+,B
		asl B
		asl B
		move B,X0
		mac X0,X1,A
	LSpoctiNocni:
	move (R1)-
	move (R1)-
	move (R1)-
	do #3,PridejNocni
		move Y:(R1),B
		add A,B
		move B,Y:(R1)+
	PridejNocni:
	rts

DSvetlo:
	move #Svetlo,R0
	do #KonSvetla-Svetlo,InSveLop
		_rcv: jclr #0,X:<<HSR,_rcv
		movep X:<<HRX,X:(R0)+
	InSveLop:


	_rcvNL: jclr #0,X:<<HSR,_rcvNL
	movep X:<<HRX,A
	move A,X:NLamp
	tst A
	jle <NoLamp ; nejsou lampy
		move #Lampy,R0
		move #3,N0
		; ted jeste prijmi lampy!
		do X:NLamp,InLampLop
			do #3,LampaPos
				_rcv: jclr #0,X:<<HSR,_rcv
				movep X:<<HRX,X:(R0)+
			LampaPos:
	
			move (R0)-N0
			do #3,LampaSmer
				_rcv: jclr #0,X:<<HSR,_rcv
				movep X:<<HRX,Y:(R0)+
			LampaSmer:
	
			do #3,LampaBar
				_rcv: jclr #0,X:<<HSR,_rcv
				movep X:<<HRX,X:(R0)+
			LampaBar:
	
			nop
		InLampLop:

	NoLamp:
	move #Pozadi,R0
	move #Nebe,R4
	move #2,N4
	move X:(R0)+,A ; zkopiruj slozky do Nebe
	move A,Y:(R4)+ X:(R0)+,A
	move A,Y:(R4)+ X:(R0)+,A
	move A,Y:(R4)-N4

	move R4,R1
	jsr SpoctiNocni
	ReadRGB Y,R4 ; zjisti 16 bit. hodnotu
	move A1,X:-(R4)
	
	jmp <main

LampaPlocha:
; spocita vliv lampy na plochu
; vstup: R6 = lampa, X:RelPos = pos, Y:(R4) = normala
; vysledek v X0
; nici A,B,X,Y, R4

	; spocti B= R.s (to by melo vyradit hodne ploch)
	move X:(R2)+,X0 Y:(R6)+,Y0
	mpy X0,Y0,B X:(R2)+,X0 Y:(R6)+,Y0
	mac X0,Y0,B X:(R2)-,X0 Y:(R6)-,Y0
	mac X0,Y0,B (R6)- ; R6 je na zacatku lampy
	; B=R.s
	move (R2)- ; X:(R2) je RelPos
	jle <IgnLampu ; mimo kuzel
		; v Y:(R4) je (inverzni?) normala plochy
		move X:(R2)+,X0 Y:(R4)+,Y0
		mpy -X0,Y0,A X:(R2)+,X0 Y:(R4)+,Y0
		mac -X0,Y0,A X:(R2)-,X0 Y:(R4)+,Y0
		mac -X0,Y0,A (R2)-
		; R6 ukazuje na lampu
		; A=R.n
		jle <IgnLampu ; odvracena plocha
		
		move B,X0 ; R.s
		move A,Y0 ; R.n
		mpy X0,Y0,A ; A=R.s * R.n
		
		; spocti R.R
		; move #RelPos,R2 ; - uz je, neni treba
		move X:(R2)+,X0
		mpy X0,X0,B X:(R2)+,X0
		mac X0,X0,B X:(R2)+,X0
		mac X0,X0,B ; B = R.R
		
		rep #6 ; nesmi byt vic jak 6 - mohlo by dojit k Overflow
		asl B ; vliv vzdal. se nejak neprojevuje!
		
		move B,X0
		mpy X0,X0,B ; B = (R.R)^2
		; urcite je B>=0, A>=0

		asl B

		move B,X0 ; A je mozna dost velke?
		cmp X0,A ; musi byt X0>A, jinak je vysledek vetsi nez 1.0
		jge <_Overflow
			AND	#$FE,CCR		;clear carry bit C (quotient sign bit)
			REP	#$18				;form a 24-bit quotient
			DIV	X0,A				;form quotient in A0, remainder in A1
			move A0,B
			move #0.5,X0
			cmp X0,B
			tgt X0,B
			move B,X0
		rts
		_Overflow:
			move #0.5,X0 ; max. magn.
		rts
	IgnLampu:
		move #0,X0
	rts
		
; jednotliva plocha je

;     X:      Y:    po Delta fazi  X:      Y:
; 00  a.x     b.x                (a-c).x (c-b).x
; 01  a.y     b.y                (a-c).y (c-b).y
; 02  a.z     b.c                (a-c).z (c-b).z

; 03  r       c.x                  r  char(a.x,b.x,c.x)
; 04  g       c.y                  r  char(a.x,b.x,c.x)
; 05  b       c.z                  r  char(a.x,b.x,c.x)

; kde char(x,y,z) je (max(x,y,z)+min(x,y,z))/2

DOsvetleni:
	move #PlochyIn,R0
	move #3,N0
	_rcvN: jclr #0,X:<<HSR,_rcvN
	movep X:<<HRX,A
	move A,X:NPloch
	; format vstupu je
	; X:NPloch *( vektory a,b, rgb. vektor, z-sour) -> ( rgb565 )
	tst A
	jeq <NoInPlochy
	do X:NPloch,InOsvLop
		do #3,_inalop
			_rcvdosa: jclr #0,X:<<HSR,_rcvdosa
			movep X:<<HRX,X:(R0)+
		_inalop:
		move (R0)-N0
		do #6,_inbclop
			_rcvdosb: jclr #0,X:<<HSR,_rcvdosb
			movep X:<<HRX,Y:(R0)+
		_inbclop:
		move (R0)-N0
		do #3,_inrgblop
			_rcvdosrgb: jclr #0,X:<<HSR,_rcvdosrgb
			movep X:<<HRX,X:(R0)+
		_inrgblop:
		nop
	InOsvLop:

	move #PlochyIn,R0
	move #Plochy,R1 ; barvy pocitej rovnou do ploch
	do X:NPloch,SvetloBlok

		move #3,N0
		move (R0)+
		move (R0)+ ; R0 posun na z - sour
		
		; - do X:Var1 spocitej charakter. z - souradnici
		
		if 0
		; v A sestavime maximum, v B minimum
		move X:(R0),A ; A=a.x
		tfr A,B Y:(R0),X1 ; B=a.x, X1=b.x
		cmp X1,A Y:(R0+N0),Y1 ; Y1=c.x
		tlt X1,A ; if( A<X1 ) A=X1
		tgt X1,B ; else B=X1
		cmp Y1,A
		tlt Y1,A ; if( A<Y1 ) A=Y1;
		cmp Y1,B
		tgt Y1,B ; if( B>Y1 ) B=Y1
		add B,A ; A=max+min
		asr A ; A=(max+min)/2
		else
		
		move L:(R0),AB ; pocitej skutecny prumer
		add B,A Y:(R0+N0),B
		add B,A #0.333333,X1
		move A,Y1
		mpy X1,Y1,A ; X0 = (a.x+b.x+c.x)/3
		
		endif
		
		move A,X:Var1
		move (R0)-
		move (R0)-
		
		; - a a b preved na relat. vyjadreni vuci c

		do #3,_DeltaC
			move X:(R0),B ; B=a.x
			move Y:(R0+N0),A ; A=c.x
			sub A,B Y:(R0),Y1 ; B=a.x-c.x, Y1=b.x
			move B,X:(R0) ; a.x-=c.x
			sub Y1,A
			move A,Y:(R0)+ ; b.x= c.x-b.x
		_DeltaC:
		move (R0)-N0 ; vrat se na zac. plochy

		jsr <NormalaR0doR1 ; nici R2-R5,N1,N4

		move #3,N1 ; dulezity offset - udrz si ho behem celeho cyklu
		move #LampyUhrn,R5 ; spocti pridavek ze vsech lamp
	
		; - spocitej vliv lamp

		clr A (R1)-N1 ; vrat se na normalu, Y:(R1) je normala
		move A,X:(R5)+
		move A,X:(R5)+
		move A,X:(R5)+
		move X:NLamp,A
		tst A
		jle <NoLampLop
			; vypocet osvetleni lampami - viz DSP_REM ****** LAMP
			; pridavky se hromadi v LampyUhrn
			
			move #3,N6 ; dulezity offset - udrz si ho behem celeho cyklu
			move #Lampy,R6 ; R6 ukazuje prave pocitanou lampu
			do X:NLamp,LampyLop
				; R0 je na 3. posici prave zpracovavane plochou

				; - kazda lampa se pocita ve vsech trech bodech
				
				move R0,R2 ; Y:(R2) je c.x
				move #RelPos,R5
				; spocti RelPos (R)
				do #3,_Rel
					move X:(R6)+,X0 Y:(R2)+,A
					sub X0,A
					move A,Y:(R5)
					move A,X:(R5)+
				_Rel:
				move (R6)-N6 ; v X:RelPos a Y:RelPos je rel. pozice c
				
				move #RelPos,R2 ; X:(R2) = rel. posice
				move R1,R4 ; Y:(R4) = normala
				jsr <LampaPlocha
				move #0.333333,X1
				mpy X0,X1,A
				move A,X:Var0
				
				move #RelPos,R5
				lua (R0)-N0,R2 ; zac. plochy
				do #3,_AddLop1
					move X:(R2)+,X0 Y:(R5),A ; X0=a0.x-c0.x, A=c.x
					add X0,A
					move A,X:(R5)+ ; RelPos.x=a0.x+(c.x-c0.x)
				_AddLop1:
				
				move #RelPos,R2 ; X:(R2) = rel. posice
				move R1,R4 ; Y:(R4) = normala
				jsr <LampaPlocha
				move X:Var0,A ; X:Var0+=X0/3
				move #0.333333,X1
				mac X0,X1,A
				move A,X:Var0

				move #RelPos,R5
				lua (R0)-N0,R2 ; zac. plochy
				do #3,_AddLop2
					move Y:(R2)+,X0
					move Y:(R5),A ; X0=c0.x-b0.x, A=c.x
					sub X0,A ; A= c.x-c0.x+b0.x
					move A,X:(R5)+ ; RelPos.x=b0.x+c.x-c0.x
				_AddLop2:
				
				move #RelPos,R2 ; X:(R2) = rel. posice
				move R1,R4 ; Y:(R4) = normala
				jsr <LampaPlocha
				move X:Var0,A ; X:Var0+=X0/3
				move #0.333333,X1
				mac X0,X1,A
				move A,X0 ; celk. vysledek
				
				move #LampyUhrn,R2 ; pricti pridavek z tehle lampy
				; barva lampy je na X:(R6)+N6
				move (R6)+N6 ; skoc na barvu
				do #3,_AddLop
					move X:(R2),A
					move X:(R6)+,X1
					mac X0,X1,A
					move A,X:(R2)+
				_AddLop:
				nop
			LampyLop:
	
		NoLampLop:
		
		; do c.z je nutno spocitat prumer - bud jako prumer;
		; nebo jako (min+max)/2
		
		move #Svetlo,R4 ; spocti. odchylku svetla a normaly
		move #2,N0 ; priprav si dopredu
		move X:(R4)+,X1 Y:(R1)+,Y1
		mpy X1,Y1,A X:(R4)+,X1 Y:(R1)+,Y1
		mac X1,Y1,A X:(R4)+,X1 Y:(R1)-,Y1
		mac X1,Y1,A (R1)- ; v A cos(fi), R1 vrat na normalu
		tst A #0,X0
		tlt X0,A ; A=max(A,0)
		move A,Y1 ; do Y1 dej koef. podle odchylky
		move #ExpTab,R2 ; hodne dopredu priprav R2
		move X:Var1,A ; nacti z-sour. do A
		; tab. exponentu ma delku 8bitu
		; z je urcite kladne - 23 bitu
		; my chceme, aby tabulka pokryvala 12.8 bitu
		rep #7+6 ; udava strmost mlhy a meritko (za + je REZ_BITU)
		asr A ; cim vetsi, tim slabsi mlha
		move #0,X0 ; X0 udava zacatek mlhy
		cmp X0,A
		tlt X0,A ; A=max(A,16)
		sub X0,A #DelExp-1,X0
		cmp X0,A
		tgt X0,A ; A=min(A,DelExp)
		move A1,N2 ; horni cast dava index
		; dolni cast interpolaci mezi hodnotami v tabulce
		move #0,A1
		move (R2)+N2 ; priprav adr. v tabulce
		asr A
		move X:(R2)+,X1
		; A=f(0)-f(0)*x+f(1)*x
		move A0,X0 ; X0=frac
		move X1,A
		mac -X1,X0,A X:(R2),X1
		mac X1,X0,A
		move A,Y0 ; v Y0 vytvor koeficient mlhy

		move #Pozadi,R2
		move #Osvetleni,R3
		move #Rozptyl,R4
		move #LampyUhrn,R5
		bclr #0,X:Var0 
		do #3,Barsvlop
			; v Y1 mas cos odchylky
			move X:(R0)+,X1 ; barva plochy
			jset #23,X1,Aktivni
				; nejprve spocitej pridavek zpusobeny svetlem slunce
				; rozptylenym svetlem
				mpy Y1,X1,A X:(R3)+,X0 ; na R3 mas svetlo slunce
				move X1,B ; B=skutecna barva plochy
				move A,X1
				; na R4 mas barvu pozadi
				mpy X1,X0,A X:(R4)+,X0 ; A=cos(fi)*barva(plocha)*barva(slunce)
				move B,X1
				mac X1,X0,A X:(R5)+,X0 ; v A mas slozku po osvetleni sluncem a pozadim
				mac X1,X0,A ; pricti vyznam lamp
			jmp <AktPas
			Aktivni:
				bset #0,X:Var0
				move (R3)+ ; ignoruj slunce
				move (R4)+ ; ignoruj pozadi
				move (R5)+ ; ignoruj lampy
				move X1,A
				abs A
			AktPas:
			; v Y0 mas hodnotu mlhy
			move X:(R2)+,X0 ; na R2 je barva mlhy
			move X0,B
			mac -X0,Y0,B A,X0
			mac X0,Y0,B
			move B,Y:(R1)+
		Barsvlop:

		jset #0,X:Var0,_NeNocni
			move (R1)-N1 ; na zacatek plochy
			jsr <SpoctiNocni
		_NeNocni:

		nop ; kvuli pipelinu - (do - loop)
	SvetloBlok:

	move #Plochy,R0
	move #-1,N0
	do X:NPloch,_outpaklop
		ReadRGB Y,R0
		move A1,X:(R0+N0) ; uloz barvu 16-bitu
	_outpaklop:
	
	move #Plochy,R0
	move #3,N0
	do X:NPloch,_outsvlop
		_sndos1: jclr #1,X:<<HSR,_sndos1
		movep X:(R0)+N0,X:<<HTX ; vysli barvy
	_outsvlop:
	NoInPlochy:
	move #0,X0
	move X0,X:NAET ; inic. AET
	move #AET,R0
	move R0,X:EAET
	bclr #0,X:PoloFlag
	jmp <main

DExpInit:
	move #ExpTab,R0
	do #DelExp,_inloclop
		_rcvei3: jclr #0,X:<<HSR,_rcvei3
		movep X:<<HRX,X:(R0)+
	_inloclop:
	jmp <main

InitZvuky:
	move #0,X0
	move #0,X1

	move #ZvukyBuf,R0
	move #ZvukyBuf+16,R1
	move R0,X:ZvukBufPtr ; nech Filu kus n�skok
	move R1,Y:ZvukBufPtr ; na prvni word

	move X0,X:SndLock

	move #Kanaly,R0
	move #>Random24,A
	do #NKanalu,InitKanaly
		move X,L:(R0)+
		move X,L:(R0)+
		move X,L:(R0)+
		move X,L:(R0)+
		move X0,X:(R0)
		move A,Y:(R0)+ ; ukazatel na proceduru
		move X,L:(R0)+
		move X,L:(R0)+
		move X,L:(R0)+
	InitKanaly:

	move #Zvuky,R0
	rep #C_Delka*NZvuku
	move X,L:(R0)+

	move #ZvukyBuf,R0
	rep #DelZvukBuf
	move X,L:(R0)+
	rts

	
; RandC ma byt $15a5235
; my to musime roszsekat po 16-ti bitech

SaveM equ 0 ; zda je potreba si inicializovat M-ka
; (tj - pouzivaji-li se kruhove buffery)

datain:
	jclr #RFS,X:<<SSR,dinP
	movep X:<<RX,X:WordIn
	rti
dinP:
	movep X:<<RX,Y:WordIn
	rti

dataout:
	jclr #TFS,X:<<SSR,doutPravy
		pha
		phrr X0,R0
		move X:ZvukBufPtr,R0
		move X:WordIn,A ; p�evezmi sample
		asl A Y:(R0),X0
		add X0,A
		movep A,X:<<TX ; vydej v�sledek na CODEC
		plrr X0,R0
		pla
	rti
	doutPravy:
		phregs
		phrr R0,M0
		move X:ZvukBufPtr,R0
		move #DelZvukBuf-1,M0

		move Y:WordIn,A ; p�evezmi sample
		asl A X:(R0)+,B
		add B,A
		movep A,X:<<TX ; vydej v�sledek na CODEC
		move R0,X:ZvukBufPtr

		move L:ZvukBufPtr,AB
		sub A,B #>DelZvukBuf-1,X0
		and X0,B1 #>16,X0
		move B1,B ; B=(ZvukFil-ZvukPtr)%DelZvukBuf
		cmp X0,B
		jgt <Enough
			phr N0
			jsr <datafill
			plr N0
		Enough:
	
		plrr R0,M0
		plregs
	rti
	

; na X:(R0) je seed, na Y:(R0) ukazatel do samplu
; sm� zni�it X,B
; mus� posunout (R0)+
; v�sledek d�v� v X0

; pseudon�hodn� gener�tor
Random24:
	move X:(R0),X0
	clr B #>$5a5235,X1
	move #>2,B0
	mac X0,X1,B
	asr B
	move B0,X:(R0)+
	move B0,X0 ; vydej v�sledek
	rts
; p�ehr�v�n� samplu
Sampler:
	move Y:(R0),R3 ; bere jen 16 bit�
	jclr #23,Y:(R0),Sude ; byl-li nastaven, bez druhou polovinu samplu
		move Y:(R3),X0 ; p�e�ti vzorek ze samplu
		move #1<<11,X1
		mpy X0,X1,B (R3)+ ; vysu� horn� polovinu z B0 pry�
		move R3,X1
		move X:SampleEnd,A
		cmp X1,A B0,X0
		jgt <NotLoop
			move #>SampleBufZac,R3
		NotLoop:
		move R3,Y:(R0)+ ; zm�� posici
	rts
	Sude:
		move Y:(R3),X0 ; p�e�ti vzorek ze samplu
		bset #23,Y:(R0)+ ; nem�� posici
	rts
	
datafill: ; ni�� M0,R0,N0,A,B,X,Y
	bset #0,X:SndLock
	jcs <NoFill ; n�kdo u� filuje, nejde p�eru�ovat
	
	phr R3
	phrr R2,N2
	phrr R1,N1
	
	if SaveM
		phrr M0,M1
		phr M2
		move #-1,M0
		move M0,M1
		move M0,M2
	endif
	
	; pos��tej kan�ly
	move #Kanaly,R1
	; move #0,N1
	move #K_Delka,N1
	do #NKanalu,KanalyLop
		; ** postupn� popisuji strukturu
		move R1,R0
		move #1,N0
		move L:(R0),AB ; ��ta� a krok
		; ** X00: citac, Y00: krok
		move Y:(R0+N0),X0 ; zm�na kroku
		; ** Y01: zm�na kroku
		add X0,B
		move B,Y:(R0) ; zm�nen� krok ulo�
		
		add B,A #-1.0,X0
		jlt <KanalNezni
			tst B
			jeq <KanalNeni ; nemuze znit - ma krok 0
			add X0,A
			move A,X:(R0)+ ; znovu nastav citac
			move (R0)+ ; preskoc zmenu kroku
			move (R0)+ ; preskoc posl. data
			; ** L02: posl. data
			move L:(R0),BA ; nacti krok v obalce a offset
			; ** X03: krok v datech, Y03: offset
			add B,A #>C_Delka*256-1,X0
			and X0,A ; cykli v delce obalky
			move A,Y:(R0)+ ; uloz offset
			rep #8
			asr A
			move A,N2
			; ** X04: ukaz. na obalku
			move X:(R0),R2 ; nacti ukazatel na obalku
			move Y:(R0)+,R3 ; ukazatel na proceduru
			move (R2)+N2 ; spocti si, kde jsi v ramci bufferu
			; ** X05: seed pro random
			jsr (R3) ; prove� synt�zu
			move X:(R2),X1 ; vezmi hlasitost z obalky
			mpy X0,X1,A L:(R0),Y ;  Y1 - hlasitost vlevo, Y0 - vpravo
			; ** X06: hlasitost L, Y06: hlasitost P
			move #-4,N0
			move A,X1 ; X1 - obalena uroven
			mpy X1,Y1,A
			mpy X1,Y0,B
			move AB,L:(R0+N0) ; uloz, jaka byla posledni data
			move #1,N0
			nop
			move L:(R0+N0),AB
			; ** X07: zmena hlasitosti L, Y06: zmena hlasitosti P
			add Y1,A
			add Y0,B
			move AB,L:(R0)
			jmp <KanalDone
		KanalNeni: ; normalne nech posl. data
			move (R0)+
			move (R0)+
			clr A
			move A,L:(R0)+
			jmp <KanalDone
		KanalNezni:
			move A,X:(R0)+ ; uloz citac
			move (R0)+ ; preskoc zmenu periody
		KanalDone:
		move (R1)+N1
	KanalyLop:
	
	clr A #0,B ; zesil speech - je jen 8-bitov�
	; oddelene scitani omezi orezavani
	move #Kanaly,R0
	move #K_Delka-2,N0
	move #-1,M0
	do #NKanalu,KanalyAdd
		move (R0)+ ; preskoc periodu
		move (R0)+ ; preskoc zmenu per.
		move L:(R0)+N0,Y ; nacti data, preskoc zbytek
		add Y1,A ; pricti
		add Y0,B
	KanalyAdd:

	move Y:ZvukBufPtr,R0
	move #DelZvukBuf-1,M0
	nop
	move AB,L:(R0)+
	move R0,Y:ZvukBufPtr

	if SaveM
		plr M2
		plrr M0,M1
	endif
	plrr R1,N1
	plrr R2,N2
	plr R3
	bclr #0,X:SndLock
	NoFill:
	nop
	rts

DefZvuku:
	_rcvN: jclr #0,X:<<HSR,_rcvN
	movep X:<<HRX,X0
	move #>C_Delka/2,X1 ; C_Delka je sude
	mpy X0,X1,A ; int. nasobeni do A0
	move #Zvuky,R0
	move A0,N0
	nop
	move (R0)+N0
	do #C_Delka,PrijmiZvuk
		_rcvD: jclr #0,X:<<HSR,_rcvD
		movep X:<<HRX,X:(R0)+
	PrijmiZvuk:
	jmp <main

ZniZvuk:
	bset #0,X:SndLock
	move #>K_Delka,X1
	move #Kanaly,R0
	_rcvN: jclr #0,X:<<HSR,_rcvN
	movep X:<<HRX,X0
	mpy X0,X1,A
	asr A ; int. nasobeni do A0
	move A0,N0
	move #0,X0
	move (R0)+N0

	move X0,X:(R0) ; citac
	_rcvD0: jclr #0,X:<<HSR,_rcvD0
	movep X:<<HRX,Y:(R0)+ ; rizeni frekvence
	move #0,X1
	_rcvD0b: jclr #0,X:<<HSR,_rcvD0b
	movep X:<<HRX,Y:(R0)+ ; zmena frekvence
	move X,L:(R0)+ ; akust. data
	_rcvD1: jclr #0,X:<<HSR,_rcvD1
	movep X:<<HRX,X:(R0) ; krok
	move X0,Y:(R0)+ ; offset
	_rcvD2: jclr #0,X:<<HSR,_rcvD2
	movep X:<<HRX,X:(R0) ; ukaz na obalku
	_rcvD2b: jclr #0,X:<<HSR,_rcvD2b
	movep X:<<HRX,N2
	move #>ZvukProcedury,R2
	nop
	move X:(R2+N2),R2
	move R2,Y:(R0)+ ; ukazatel na proceduru

	move X0,X:(R0) ; seed pro random
	move #>SampleBufZac,R2 ; ukazatel na sample
	move R2,Y:(R0)+
	_rcvD3: jclr #0,X:<<HSR,_rcvD3
	movep X:<<HRX,X:(R0) ; hlasitost L
	_rcvD4: jclr #0,X:<<HSR,_rcvD4
	movep X:<<HRX,Y:(R0)+ ; hlasitost P
	_rcvD5: jclr #0,X:<<HSR,_rcvD5
	movep X:<<HRX,X:(R0) ; zmena hlasitosti L
	_rcvD6: jclr #0,X:<<HSR,_rcvD6
	movep X:<<HRX,Y:(R0)+ ; zmena hlasitosti P
	move #-4,N0
	move #>C_Delka,X1
	move X:(R0+N0),X0 ; posun se na ind. char.
	mpy X0,X1,A
	asr A ; int. nasobeni do A0
	move #Zvuky,R1
	move A0,N1
	nop
	move (R1)+N1
	move R1,X:(R0+N0)
	bclr #0,X:SndLock
	jmp <main

ZmenZvuk:
	bset #0,X:SndLock
	move #>K_Delka,X1
	move #Kanaly,R0
	_rcvN: jclr #0,X:<<HSR,_rcvN
	movep X:<<HRX,X0
	mpy X0,X1,A
	asr A ; int. nasobeni do A0
	move A0,N0
	move #0,X0
	move (R0)+N0

	_rcvD0: jclr #0,X:<<HSR,_rcvD0
	movep X:<<HRX,Y:(R0)+ ; ��zen� frekvence
	_rcvD0b: jclr #0,X:<<HSR,_rcvD0b
	movep X:<<HRX,Y:(R0)+ ; zm�na frekvence
	move (R0)+ ; akust. data
	_rcvD1: jclr #0,X:<<HSR,_rcvD1
	movep X:<<HRX,X:(R0) ; krok
	move (R0)+ ; offset
	move (R0)+ ; ukazatel na ob�lku a proceduru je stejn�
	move (R0)+ ; seed pro random, sample
	_rcvD3: jclr #0,X:<<HSR,_rcvD3
	movep X:<<HRX,X:(R0) ; hlasitost L
	_rcvD4: jclr #0,X:<<HSR,_rcvD4
	movep X:<<HRX,Y:(R0)+ ; hlasitost P
	_rcvD5: jclr #0,X:<<HSR,_rcvD5
	movep X:<<HRX,X:(R0) ; zm�na hlasitosti L
	_rcvD6: jclr #0,X:<<HSR,_rcvD6
	movep X:<<HRX,Y:(R0)+ ; zm�na hlasitosti P
	bclr #0,X:SndLock
	jmp <main

LoadSample:
	bset #0,X:SndLock
	move #SampleBufZac,R0
	_rcvD0: jclr #0,X:<<HSR,_rcvD0
	movep X:<<HRX,X0 ; d�lka samplu
	do X0,LoadSamLoop
		_rcvD1: jclr #0,X:<<HSR,_rcvD1
		movep X:<<HRX,Y:(R0)+ ; data samplu
	LoadSamLoop:
	nop
	move R0,X:SampleEnd ; cyklen� samplu - nejde pou��t mod. aritmetiku!
	bclr #0,X:SndLock
	jmp <main

EndProgSeg: equ *

ZacDataSeg: equ *

MaxPloch equ 600
MaxHran equ 640

DelExp equ $101

; globalni data, trvalejsiho charakteru

	swspc x
ExpTab: ds DelExp
	swspc l

Stack_len equ 128
Stack: ds Stack_len

	swspc l ; zakladni charakteristiky

Plochy: ds MaxPloch*3 ;
; rozlozeni:
; Y:0-2 rgb
; X:0 x konce, X:1 z konce (pri z-bufferu) X:2 barva 16 bitu

	swspc x
ZvukProcedury: ; r�zn� druhy gener�tor� zvuku
	dc Random24
	dc Sampler
	dc Random24
	dc Random24
	dc Random24
	dc Random24
	dc Random24
	dc Random24

; L:ZvukyBuf pou�ijeme jako buffer pro v�stup

NZvuku equ 10
C_Delka equ 256

SampleBufZac: ; v y m�me prostor pro docela dlouh� sample
Zvuky: ds C_Delka*NZvuku


ZacSpare: equ *

DelZvukBuf equ 2048 ; sta�� p�i 16 kHz na 0.25 sec
ZvukyBuf: dsm DelZvukBuf
SampleBufKon: equ ZvukyBuf ; kon�� n�m prostor pro sample

	swspc l

AET: ds MaxHran*6 ; toto je AET
; obsahuje:
; L:0 =  x
; L:1 =  z
; X:2 =  L, Y:2 = P
; L:3 = dx
; L:4 = dz
; X:5 = hh

ModuleBss equ *

; pracovni data pro jednotlive sluzby
	org L:ModuleBss
	Rad1Out: ds 640
; schema vzorku je vzdy X: index plochy, Y:0 z-souradnice
EndRadekBss equ *
	org L:ModuleBss
PlochyIn: ds 6*MaxPloch
EndPlochyBss equ *
