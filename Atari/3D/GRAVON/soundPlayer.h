#ifndef soundPlayer_h__
#define soundPlayer_h__

#ifdef __cplusplus
extern "C"
{
#endif

#define PLAY 0

#define STEREO8    0
#define STEREO16   1
#define MONO8      2

/// nomimal frequency
/**
Even when playing on a lot higher frequency, API assumes SND_FRQ as a basic frequency,
and many values are relative to it.
*/
#define SND_FRQ 16390

/// similar to Atari XBIOS functionality - set one PCM wave (voice) for a playback (fixed rate)
void setbuffer(int playRec, int mode, int freq, const void *beg, const void *end);

/// set one PCM wave (engine) for a playback (variable rate)
void SendSample(void *data, size_t len);

void DefSyntSound( enum indzvuku Index, const int *Prubeh );

void SyntSoundStart( int Handle, enum indzvuku Index, long HlasL, long HlasP, long Frekvence, long Rychlost );

void SyntSoundChange( int Handle, long HlasL, long HlasP, long Frekvence, long Rychlost, long DFr, long DHL, long DHP );

void StartSound();
void EndSound();

#ifdef __cplusplus
};
#endif
#endif // soundPlayer_h__
