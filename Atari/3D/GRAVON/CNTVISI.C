/* Gravon - Nepratele - ridici centrum */
/* SUMA 7/1994-7/1994 */

#include <limits.h>
#include <macros.h>
#include "letvisi.h"

#if __DEBUG
	#define CNT_PROTOCOL 0
#endif

#if CNT_PROTOCOL
	#include <stdio.h>
#endif

#define PROC_USPES 95
#define SEC_INTERVAL 30

typedef struct
{
	enum druh Druh;
	PosT KdeSeHlasil;
	long KdySeHlasil; /* podle toho odhadujeme zniceni a tankovani, nekdy spojeni vubec neuskutecnime */
	Flag BylPokus;
	Flag Zije; /* nema cenu mu davat povely (mozna jen tankuje) */
	enum kategorie {KStihac,KBachar,KHlidacBonusu,KMax} Kat;
	long MaxRychl; /* pomaha nam zvolit reprezentanty */
	int CisloBonusu; /* pokud hlida, tak ktery */
} VuzInfo;

static VuzInfo CntVozy[NMaxVozidel];

static PosT VidenG,VidenV;
static Flag FVidenG,FVidenV; /* kdy jsi vid�l Gravona a v�zn� */
static long KdyVidenG,KdyVidenV;

typedef struct
{
	int HlidaBon; /* jak� ��st vy�len�na na bonusy */
	int Prior[KMax];
} Strategie;

const static Strategie StratStd={3,30,25,50,};
const static Strategie StratNul={0,0,0,0,};
static Strategie StratChci; /* jake chci rozlozeni sil */

static int NVozidel; /* po�. stav */
static int JeVozidel; /* aktu�l. stav */

static int HlidaBonus[MaxPos];
static int HlidaTajne[MaxPos];

static enum kategorie PridelKat( Strategie *S, const Strategie *W )
{
	long pmax=LONG_MAX;
	enum kategorie kmax=KHlidacBonusu,ki;
	if( VidBonusy.N<=0 || (S->Prior[KHlidacBonusu]+JeVozidel)*W->HlidaBon>NVozidel ) /* je�t� m� kdo hl�dat, nebo nen� co hl�dat */
	for( ki=0; ki<KMax; ki++ )
	{
		int WP=W->Prior[ki];
		if( WP>0 )
		{
			long prior=DF(S->Prior[ki],WP);
			if( pmax>prior ) pmax=prior,kmax=ki;
		}
	}
	S->Prior[kmax]++;
	JeVozidel--;
	return kmax;
}

static void InitVI( KrajInfo *KI, VuzInfo *VI, Vozidlo *V )
{
	VI->KdeSeHlasil=V->NPos;
	VI->KdySeHlasil=CasSimulace+(KRand(&KI->KD.Seed)%SEC_INTERVAL)*1000L;
	VI->Zije=True;
	VI->Kat=KHlidacBonusu;
}

void ZacCentrum( KrajInfo *KI, int *Vozidla, int *Vrtulniky, int *Kanony, int *Tanky, int *Letadla )
{
	int I;
	NVozidel=0;
	StratChci=StratStd;
	for( I=0; I<NMaxVozidel; I++ ) CntVozy[I].Zije=False;
	while( (I=*Vozidla++)>=0 )
	{
		VuzInfo *VI=&CntVozy[I];
		Vozidlo *V=KI->Vozy[I];
		VI->Druh=DVznasedlo;
		VI->MaxRychl=fxmetr(30); /* bezna prumerna cestovni rychlost */ 
		InitVI(KI,VI,V);
		NVozidel++;
	}
	while( (I=*Vrtulniky++)>=0 )
	{
		VuzInfo *VI=&CntVozy[I];
		Vozidlo *V=KI->Vozy[I];
		VI->Druh=DVrtulnik;
		VI->MaxRychl=fxmetr(90); /* muze letat primo */
		InitVI(KI,VI,V);
		NVozidel++;
	}
	while( (I=*Letadla++)>=0 )
	{
		VuzInfo *VI=&CntVozy[I];
		Vozidlo *V=KI->Vozy[I];
		VI->Druh=DVrtulnik;
		VI->MaxRychl=fxmetr(200); /* muze letat primo */
		InitVI(KI,VI,V);
	}
	while( (I=*Kanony++)>=0 )
	{
		VuzInfo *VI=&CntVozy[I];
		Vozidlo *V=KI->Vozy[I];
		VI->Druh=DDelo;
		VI->MaxRychl=fxmetr(5); /* vubec se nehybe */
		InitVI(KI,VI,V);
	}
	while( (I=*Tanky++)>=0 )
	{
		VuzInfo *VI=&CntVozy[I];
		Vozidlo *V=KI->Vozy[I];
		VI->Druh=DVznasedlo;
		VI->MaxRychl=fxmetr(20); /* jezdi pomalu, ale jiste */
		InitVI(KI,VI,V);
		NVozidel++;
	}
	FVidenG=FVidenV=False;
}

void KonCentrum( KrajInfo *KI )
{
	(void)KI;
}


static long MaxVzdalenost( const PosT *B, const PosT *b )
{ /* odhad vzdalenosti */
	long d,D;
	d=B->x-b->x;if( d<0 ) d=-d;
	D=B->x-b->x;if( D<0 ) D=-D;
	if( D<d ) D=d;
	return D;
}

static void VypracujStrategii( KrajInfo *KI )
{
	Strategie StratS;
	int I,i;
	/* po chvili uz nema cenu si pamatovat posici Gravona */
	if( CasSimulace-KdyVidenG>120000L ) FVidenG=False;
	if( CasSimulace-KdyVidenV>120000L ) FVidenV=False;
	JeVozidel=0;
	for( I=0; I<NMaxVozidel; I++ )
	{
		VuzInfo *VI=&CntVozy[I];
		if( VI->Zije )
		{
			if( CasSimulace-VI->KdySeHlasil>SEC_INTERVAL*3500L )
			{ /* odepis ho, nehlasi se */
				VI->Zije=False;
				continue;
			}
			else JeVozidel++;
			VI->Kat=KMax; /* musis pridelit */
		}
	}
	/* inic. priority */
	StratS=StratNul;
	for( i=0; i<VidBonusy.N; i++ ) HlidaBonus[i]=0;
	for( i=0; i<Tajne.N; i++ ) HlidaTajne[i]=0;
	for(;;)
	{
		enum kategorie k=PridelKat(&StratS,&StratChci);
		/* koho ted potrebujeme nejvic */
		int BestI=-1;
		int Mi=0;
		int Mv=INT_MAX;
		long BestP=LONG_MAX;
		const PosT *M;
		switch( k )
		{
			case KBachar:
				if( FVidenV ) {M=&VidenV;break;}
				/* hlid� tajn� bonus */
				if( Tajne.N>0 )
				{
					for( i=0; i<Tajne.N; i++ )
					{
						if( Mv>HlidaTajne[i] ) Mv=HlidaTajne[i],Mi=i;
					}
					M=&Tajne.Pos[Mi];
					break;
				}
			/* cont! */
			case KStihac:
				if( FVidenG ) {M=&VidenG;break;}
			/* cont! */
			default:
			{
				for( i=0; i<VidBonusy.N; i++ )
				{
					if( Mv>HlidaBonus[i] ) Mv=HlidaBonus[i],Mi=i;
				}
				M=&VidBonusy.Pos[Mi];
				HlidaBonus[Mi]++;
			}
			break;
		}
		/* pridelovani kategorii - ber ohled na to, kde kdo je! */
		for( I=0; I<NMaxVozidel; I++ )
		{
			VuzInfo *VI=&CntVozy[I];
			if( VI->Zije && VI->Kat==KMax && VI->MaxRychl>0 )
			{
				long P=DF(MaxVzdalenost(M,&VI->KdeSeHlasil),VI->MaxRychl);
				if( BestP>P ) BestP=P,BestI=I;
			}
			/* VI->MaxRychl==0 -> nemuzeme s nim vubec pocitat */
		}
		if( BestI>=0 )
		{
			VuzInfo *VI=&CntVozy[BestI];
			VI->Kat=k;
			VI->CisloBonusu=Mi;
		}
		else break;
	}
	/* vycerpame vsechna vozidla s KMax */
	(void)KI;
}

void PredejZpravy( KrajInfo *KI, int Vuz, ParVozu *Vozu )
{ /* ziva netankujici vozidla se snazi stale vysilat sve udaje */
	VuzInfo *VI=&CntVozy[Vuz];
	int sec=(int)((CasSimulace-VI->KdySeHlasil)/1000);
	sec%=SEC_INTERVAL;
	if( sec!=0 ) VI->BylPokus=False;
	ef( !VI->BylPokus )
	{ /* jednou zkus uskutecnit spojeni */
		VI->BylPokus=True;
		if( KRand(&KI->KD.Seed)%100<PROC_USPES )
		{ /* spojeni navazano */
			#if CNT_PROTOCOL
				char Buf[160];
				sprintf(Buf,"Msg from #%d, >%d",Vuz,Vozu->Centru);
				RepPos(Buf,&Vozu->Misto);
			#endif
			if( !VI->Zije ) /* uz jsme ho odepsali */
			{
				VI->Zije=True;
				VI->Kat=KHlidacBonusu;
			}
			if( Vozu->Centru&CZiji ) VI->KdeSeHlasil=Vozu->Misto,VI->KdySeHlasil=CasSimulace;
			if( Vozu->Centru&CSpatrenGravon ) VidenG=Vozu->Misto,FVidenG=True,KdyVidenG=CasSimulace;
			if( Vozu->Centru&CSpatrenVezen ) VidenV=Vozu->Misto,FVidenV=True,KdyVidenV=CasSimulace;
			/* pri spatreni je sice poloha nepresna, ale lepsi nez zadna */
			/* jinak bychom vubec nevedeli, kde stihac je */
			/* nove zpravy - zmen strategi */
			VypracujStrategii(KI);
			/* podle kategorie vozu posli povel */
			switch( VI->Kat )
			{
				#define MAX_DOBA_VID (120000L)
				case KBachar:
					if( FVidenV )
					{
						long T=CasSimulace-KdyVidenV;
						Vozu->HlidejKolem=VidenV;
						Vozu->HlidejAzDo=MKDK(T,fxmetr(5000),MAX_DOBA_VID)+fxmetr(100);
						Vozu->StihejAzDo=Vozu->HlidejAzDo+fxmetr(5000);
						Vozu->IhnedNaMisto=CasSimulace-KdyVidenV<20000;
						break;
					}
					if( Tajne.N>0 && VI->CisloBonusu<Tajne.N )
					{
						Vozu->HlidejKolem=Tajne.Pos[VI->CisloBonusu];
						Vozu->StihejAzDo=fxmetr(4000);
						Vozu->HlidejAzDo=fxmetr(1000);
						Vozu->IhnedNaMisto=False;
						break;
					}
				case KStihac:
					if( FVidenG )
					{
						long T=CasSimulace-KdyVidenG;
						if( T>MAX_DOBA_VID ) T=MAX_DOBA_VID;
						Vozu->HlidejKolem=VidenG;
						Vozu->HlidejAzDo=MKDK(T,fxmetr(5000),MAX_DOBA_VID)+fxmetr(100);
						Vozu->StihejAzDo=Vozu->HlidejAzDo+fxmetr(5000);
						Vozu->IhnedNaMisto=CasSimulace-KdyVidenG<20000;
						break;
					}
				goto NicLab;
				/* cont! */
				/* cont! */
				case KHlidacBonusu:
					if( VidBonusy.N>0 && VI->CisloBonusu<VidBonusy.N )
					{
						Vozu->HlidejKolem=VidBonusy.Pos[VI->CisloBonusu];
						Vozu->StihejAzDo=fxmetr(4000);
						Vozu->HlidejAzDo=fxmetr(1000);
						Vozu->IhnedNaMisto=False;
						break;
					}
				/* cont! */
				NicLab:
					Vozu->HlidejKolem.x=Vozu->HlidejKolem.y=Vozu->HlidejKolem.z=0;
					Vozu->StihejAzDo=fxmetr(8000);
					Vozu->HlidejAzDo=fxmetr(6000);
					Vozu->IhnedNaMisto=False;
				break;
			}
			if( VI->Druh==DVrtulnik )
			{
				#define MAX_RNG fxmetr(10000)
				Vozu->StihejAzDo*=2; /* lita rychleji, olita toho vic */
				Vozu->HlidejAzDo*=2;
				if( Vozu->StihejAzDo>MAX_RNG ) Vozu->StihejAzDo=MAX_RNG;
				if( Vozu->HlidejAzDo>MAX_RNG ) Vozu->HlidejAzDo=MAX_RNG;
			}
			#if CNT_PROTOCOL
			{
				char Buf[160];
				sprintf(Buf,"Msg to   #%d, <%d",Vuz,VI->Kat);
				RepPos(Buf,&Vozu->HlidejKolem);
			}
			#endif
		} /* uspesny pokus */
		Vozu->Centru=CNic;
		/* zrus zpravu */
	}
}
