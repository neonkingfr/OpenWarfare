#include "sndGiGOut.h"

SoundCallbackFunction *SoundCallback;
HANDLE SoundReset;

typedef size_t SoundCallbackFunction(void *buffer, size_t bufferSize, int bufferFreq);

void StartSoundWaveOut(int freq, SoundCallbackFunction *callback)
{
  SoundCallback = callback;
}


void EndSoundWaveOut()
{
  SoundReset = CreateEvent(NULL,TRUE,FALSE,NULL);
  SoundCallback = nullptr;
  // wait until sound callback change is confirmed
  WaitForSingleObject(SoundReset,INFINITE);
  CloseHandle(SoundReset), SoundReset = NULL;
}
