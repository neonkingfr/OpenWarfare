#ifndef sndWaveOut_h__
#define sndWaveOut_h__

#include <windows.h>

#ifdef __cplusplus
extern "C"
{
#endif

typedef size_t SoundCallbackFunction(void *buffer, size_t bufferSize, int bufferFreq);

extern SoundCallbackFunction *SoundCallback;
extern HANDLE SoundReset;

void StartSoundWaveOut(int freq, SoundCallbackFunction *callback);
void EndSoundWaveOut();

  
#ifdef __cplusplus
};
#endif

#endif // sndWaveOut_h__
