#define _BC(n) (n)
static IBod BRejnok[]=
{
	{fxmetr( -1.9316),fxmetr(  0.2273),fxmetr( -1.8179),},
	{fxmetr(  2.0454),fxmetr(  0.2070),fxmetr( -1.7065),},
	{fxmetr( -4.8860),fxmetr(  0.3523),fxmetr(  0.7427),},
	{fxmetr(  4.6589),fxmetr(  0.3518),fxmetr(  0.7424),},
	{fxmetr(  0.0000),fxmetr(  1.1086),fxmetr( -0.2781),},
	{fxmetr( -0.1135),fxmetr( -0.2075),fxmetr(  4.5664),},
	{fxmetr( -1.1362),fxmetr(  0.6016),fxmetr( -3.9443),},
	{fxmetr(  1.0229),fxmetr(  0.6018),fxmetr( -3.9446),},
	{fxmetr(  0.0000),fxmetr( -0.7014),fxmetr( -2.4436),},
	{fxmetr( -0.7959),fxmetr( -0.8889),fxmetr(  0.5806),},
	{fxmetr( -0.7959),fxmetr(  0.3423),fxmetr(  0.7979),},
	{fxmetr(  0.7383),fxmetr(  0.3423),fxmetr(  0.7979),},
	{fxmetr(  0.7383),fxmetr( -0.8889),fxmetr(  0.5806),},
	{fxmetr( -1.7048),fxmetr( -0.9604),fxmetr(  2.2983),},
	{fxmetr( -1.7048),fxmetr(  0.2703),fxmetr(  2.5154),},
	{fxmetr(  1.4199),fxmetr(  0.2703),fxmetr(  2.5154),},
	{fxmetr(  1.4199),fxmetr( -0.9604),fxmetr(  2.2983),},
};

static IPlocha PRejnok[]=
{
	{Gray60,_BC(4),_BC(6),_BC(7),},
	{Gray60,_BC(1),_BC(4),_BC(7),},
	{Gray60,_BC(4),_BC(0),_BC(6),},
	{Gray60,_BC(2),_BC(0),_BC(4),},
	{Gray60,_BC(2),_BC(4),_BC(5),},
	{Gray60,_BC(4),_BC(3),_BC(5),},
	{Gray60,_BC(1),_BC(3),_BC(4),},
	{Gray45,_BC(1),_BC(7),_BC(8),},
	{CadetBlue,_BC(7),_BC(6),_BC(8),},
	{Gray45,_BC(6),_BC(0),_BC(8),},
	{Gray45,_BC(0),_BC(5),_BC(8),},
	{Gray45,_BC(0),_BC(2),_BC(5),},
	{Gray45,_BC(5),_BC(1),_BC(8),},
	{Gray45,_BC(3),_BC(1),_BC(5),},
	{Gray05,_BC(10),_BC(9),_BC(11),},
	{Gray05,_BC(11),_BC(9),_BC(12),},
	{Firebrick,_BC(13),_BC(14),_BC(15),},
	{Firebrick,_BC(13),_BC(15),_BC(16),},
	{Firebrick,_BC(10),_BC(11),_BC(15),},
	{Firebrick,_BC(14),_BC(10),_BC(15),},
	{Firebrick,_BC(12),_BC(13),_BC(16),},
	{Firebrick,_BC(12),_BC(9),_BC(13),},
	{Firebrick,_BC(15),_BC(11),_BC(16),},
	{Firebrick,_BC(11),_BC(12),_BC(16),},
	{Firebrick,_BC(9),_BC(10),_BC(14),},
	{Firebrick,_BC(13),_BC(9),_BC(14),},
};

#undef _BC

IKresliScena Rejnok={
	NULL,BRejnok,PRejnok,
	39,(bindex)lenof(BRejnok),(bindex)lenof(PRejnok)
};
