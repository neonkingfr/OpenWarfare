#include "predmety.h"
#include "colors.h"
static IBod BDrak1[]=
{
	{fxmetr(  0.6775),fxmetr(  1.1951),fxmetr(  1.9070),},
	{fxmetr( -0.6367),fxmetr(  1.1951),fxmetr(  1.9070),},
	{fxmetr(  0.0115),fxmetr(  0.4446),fxmetr(  3.1458),},
	{fxmetr(  1.4485),fxmetr(  1.4922),fxmetr( -1.4231),},
	{fxmetr( -1.4143),fxmetr(  1.4922),fxmetr( -1.4231),},
	{fxmetr(  0.0530),fxmetr(  0.2388),fxmetr( -0.6819),},
	{fxmetr(  0.0186),fxmetr(  0.9185),fxmetr(  5.9993),},
	{fxmetr(  0.0579),fxmetr(  1.4839),fxmetr(  2.8782),},
	{fxmetr(  0.9878),fxmetr(  2.4683),fxmetr( -0.1670),},
	{fxmetr( -0.8557),fxmetr(  2.4683),fxmetr( -0.1670),},
	{fxmetr( -6.0371),fxmetr(  1.6851),fxmetr(  2.2393),},
	{fxmetr(  0.2017),fxmetr(  1.0796),fxmetr( -1.2104),},
	{fxmetr( -6.0051),fxmetr(  1.1785),fxmetr( -2.4775),},
	{fxmetr(  0.2764),fxmetr(  1.1851),fxmetr( -1.2324),},
	{fxmetr(  5.9546),fxmetr(  1.6934),fxmetr(  2.2632),},
	{fxmetr(  5.7896),fxmetr(  1.1785),fxmetr( -2.4729),},
	{fxmetr(  0.0867),fxmetr(  4.3511),fxmetr( -0.7048),},
	{fxmetr(  0.0884),fxmetr(  3.7034),fxmetr( -2.4133),},
	{fxmetr(  0.0867),fxmetr(  3.4617),fxmetr( -0.7600),},
	{fxmetr(  0.0730),fxmetr(  3.8816),fxmetr( -2.8352),},
	{fxmetr(  0.0730),fxmetr(  4.3328),fxmetr( -3.0083),},
	{fxmetr(  0.8062),fxmetr(  1.5374),fxmetr( -1.2927),},
	{fxmetr(  0.0901),fxmetr(  2.3223),fxmetr( -0.1880),},
	{fxmetr( -0.7209),fxmetr(  1.5811),fxmetr( -1.2927),},
	{fxmetr(  0.0635),fxmetr(  4.4993),fxmetr( -0.7019),},
};

static IPlocha PDrak1[]=
{
	{DrakZelena,(2),(1),(6),},
	{DrakZelena,(0),(6),(7),},
	{DrakZelena,(0),(7),(8),},
	{DrakZelena,(3),(0),(8),},
	{DrakZelena,(3),(8),(9),},
	{DrakZelena,(4),(3),(9),},
	{DrakZelena,(1),(4),(9),},
	{DrakZelena,(4),(2),(5),},
	{DrakZelena,(1),(2),(4),},
	{DrakKridla,(11),(10),(12),},
	{DrakKridla,(10),(11),(12),},
	{DrakKridla,(14),(13),(15),},
	{DrakKridla,(13),(14),(15),},
	{DrakHlava,(17),(16),(18),},
	{DrakHlava,(16),(17),(18),},
	{DrakHlava,(19),(16),(20),},
	{DrakHlava,(16),(19),(20),},
	{DrakKridla,(22),(23),(24),},
	{DrakKridla,(23),(21),(24),},
	{DrakKridla,(21),(22),(24),},
	{DrakZelena,(2),(3),(5),},
	{DrakZelena,(2),(0),(3),},
	{DrakZelena,(3),(4),(5),},
	{DrakZelena,(0),(2),(6),},
	{DrakZelena,(6),(1),(7),},
	{DrakZelena,(7),(1),(9),},
	{DrakZelena,(8),(7),(9),},
};

IKresliScena Drak1={
	NULL,BDrak1,PDrak1,
	42,(bindex)lenof(BDrak1),(bindex)lenof(PDrak1)
};
