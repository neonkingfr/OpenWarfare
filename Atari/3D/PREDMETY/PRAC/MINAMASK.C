#include "predmety.h"
#include "colors.h"
static IBod BMinamask[]=
{
	{fxmetr(  0.0571),fxmetr(  0.2827),fxmetr(  0.1016),},
	{fxmetr( -0.6699),fxmetr(  0.0327),fxmetr( -0.3638),},
	{fxmetr(  0.7388),fxmetr(  0.0327),fxmetr( -0.3638),},
	{fxmetr(  0.0674),fxmetr(  0.0327),fxmetr(  0.5674),},
};

static IPlocha PMinamask[]=
{
	{STROM,(0),(1),(2),},
	{STROM,(0),(2),(3),},
	{STROM,(0),(3),(1),},
};

KKresliScena Minamask={
	NULL,BMinamask,PMinamask,
	6,(bindex)lenof(BMinamask),(bindex)lenof(PMinamask)
};
