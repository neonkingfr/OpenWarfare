#include "predmety.h"
#include "colors.h"
static IBod BSouska[]=
{
	{fxmetr(  0.0000),fxmetr(  0.0000),fxmetr(  0.0000),},
	{fxmetr( -1.0000),fxmetr(  2.0000),fxmetr(  0.0000),},
	{fxmetr(  0.0000),fxmetr(  2.0000),fxmetr( -1.0000),},
	{fxmetr(  1.0000),fxmetr(  2.6819),fxmetr(  0.0000),},
	{fxmetr(  0.0000),fxmetr(  2.0000),fxmetr(  1.0000),},
	{fxmetr(  0.6814),fxmetr(  7.0452),fxmetr(  0.0000),},
};

static IPlocha PSouska[]=
{
	{ClovekKuze,(0),(2),(1),},
	{ClovekKuze,(0),(3),(2),},
	{ClovekKuze,(0),(4),(3),},
	{ClovekKuze,(0),(1),(4),},
	{ClovekKuze,(1),(2),(5),},
	{ClovekKuze,(2),(3),(5),},
	{ClovekKuze,(3),(4),(5),},
	{ClovekKuze,(1),(5),(4),},
};

KKresliScena Souska={
	NULL,BSouska,PSouska,
	12,(bindex)lenof(BSouska),(bindex)lenof(PSouska)
};
