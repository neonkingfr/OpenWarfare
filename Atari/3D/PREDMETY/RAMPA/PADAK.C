#include "predmety.h"
#include "colors.h"
static IBod BPadak[]=
{
	{fxmetr(  3.5181),fxmetr( -0.0073),fxmetr( -0.0271),},
	{fxmetr(  2.8428),fxmetr(  0.0000),fxmetr(  2.3533),},
	{fxmetr( -0.0522),fxmetr(  0.0000),fxmetr(  3.4126),},
	{fxmetr(  3.1199),fxmetr(  1.1421),fxmetr( -0.0217),},
	{fxmetr(  2.3667),fxmetr(  1.1421),fxmetr(  1.9932),},
	{fxmetr( -0.0039),fxmetr(  1.1421),fxmetr(  2.7876),},
	{fxmetr(  0.0000),fxmetr(  1.8833),fxmetr( -0.1389),},
	{fxmetr(  2.9751),fxmetr(  0.0000),fxmetr( -2.4199),},
	{fxmetr(  0.0137),fxmetr(  0.0000),fxmetr( -3.3469),},
	{fxmetr(  2.4988),fxmetr(  1.1421),fxmetr( -2.1257),},
	{fxmetr( -0.0039),fxmetr(  1.1421),fxmetr( -2.7881),},
	{fxmetr( -3.5183),fxmetr( -0.0073),fxmetr( -0.0271),},
	{fxmetr( -2.8430),fxmetr(  0.0000),fxmetr(  2.3533),},
	{fxmetr(  0.0520),fxmetr(  0.0000),fxmetr(  3.4126),},
	{fxmetr( -3.1201),fxmetr(  1.1421),fxmetr( -0.0217),},
	{fxmetr( -2.3667),fxmetr(  1.1421),fxmetr(  1.9932),},
	{fxmetr( -2.9753),fxmetr(  0.0000),fxmetr( -2.4199),},
	{fxmetr( -2.4990),fxmetr(  1.1421),fxmetr( -2.1257),},
};

static IPlocha PPadak[]=
{
	{White,(0),(1),(3),},
	{White,(1),(4),(3),},
	{Red,(1),(5),(4),},
	{Red,(1),(2),(5),},
	{White,(3),(4),(6),},
	{Red,(4),(5),(6),},
	{White,(0),(3),(1),},
	{White,(1),(3),(4),},
	{Red,(1),(4),(5),},
	{Red,(1),(5),(2),},
	{White,(3),(6),(4),},
	{Red,(4),(6),(5),},
	{Red,(0),(7),(3),},
	{Red,(3),(7),(9),},
	{White,(7),(10),(9),},
	{White,(7),(8),(10),},
	{Red,(3),(9),(6),},
	{White,(6),(9),(10),},
	{Red,(0),(3),(7),},
	{Red,(3),(9),(7),},
	{White,(7),(9),(10),},
	{White,(7),(10),(8),},
	{Red,(3),(6),(9),},
	{White,(6),(10),(9),},
	{Red,(11),(12),(14),},
	{Red,(12),(15),(14),},
	{White,(5),(15),(12),},
	{White,(5),(12),(13),},
	{Red,(6),(14),(15),},
	{White,(5),(6),(15),},
	{Red,(11),(14),(12),},
	{Red,(12),(14),(15),},
	{White,(5),(12),(15),},
	{White,(5),(13),(12),},
	{Red,(6),(15),(14),},
	{White,(5),(15),(6),},
	{White,(11),(16),(14),},
	{White,(14),(16),(17),},
	{Red,(10),(17),(16),},
	{Red,(8),(10),(16),},
	{Red,(6),(17),(10),},
	{White,(11),(14),(16),},
	{White,(14),(17),(16),},
	{Red,(10),(16),(17),},
	{Red,(8),(16),(10),},
	{Red,(6),(10),(17),},
	{White,(6),(17),(14),},
	{White,(6),(14),(17),},
};

KKresliScena Padak={
	NULL,BPadak,PPadak,
	103,(bindex)lenof(BPadak),(bindex)lenof(PPadak)
};
