#include "predmety.h"
#include "colors.h"
static IBod BTopol[]=
{
	{fxmetr(  0.0000),fxmetr(  0.0000),fxmetr(  0.0000),},
	{fxmetr( -2.0000),fxmetr(  4.0000),fxmetr(  0.0000),},
	{fxmetr(  0.0000),fxmetr(  4.0000),fxmetr( -2.0000),},
	{fxmetr(  2.0000),fxmetr(  4.0000),fxmetr(  0.0000),},
	{fxmetr(  0.0000),fxmetr(  4.0000),fxmetr(  2.0000),},
	{fxmetr(  0.0000),fxmetr( 20.0000),fxmetr(  0.0000),},
};

static IPlocha PTopol[]=
{
	{STROM,(0),(2),(1),},
	{STROM,(0),(3),(2),},
	{STROM,(0),(4),(3),},
	{STROM,(0),(1),(4),},
	{STROM,(1),(2),(5),},
	{STROM,(2),(3),(5),},
	{STROM,(3),(4),(5),},
	{STROM,(1),(5),(4),},
};

KKresliScena Topol={
	NULL,BTopol,PTopol,
	12,(bindex)lenof(BTopol),(bindex)lenof(PTopol)
};
