#include "predmety.h"
#include "colors.h"
static IBod _BHrad_d5[]=
{
	{fxmetr(-32.6709),fxmetr( -1.0938),fxmetr(  3.1094),},
	{fxmetr( -8.1846),fxmetr( -1.0938),fxmetr(  6.5508),},
	{fxmetr(-10.2227),fxmetr( -1.0938),fxmetr( 21.0547),},
	{fxmetr(-34.7100),fxmetr( -1.0938),fxmetr( 17.6143),},
	{fxmetr(-32.6709),fxmetr( 27.4043),fxmetr(  3.1094),},
	{fxmetr( -8.1846),fxmetr( 27.4043),fxmetr(  6.5508),},
	{fxmetr(-10.2227),fxmetr( 27.4043),fxmetr( 21.0547),},
	{fxmetr(-34.7100),fxmetr( 27.4043),fxmetr( 17.6143),},
	{fxmetr(-21.3223),fxmetr( 32.0098),fxmetr( 11.1914),},
	{fxmetr(-27.1475),fxmetr( 27.9502),fxmetr(  6.9160),},
	{fxmetr(-14.9043),fxmetr( 27.9502),fxmetr(  8.6367),},
	{fxmetr(-15.9229),fxmetr( 27.9502),fxmetr( 15.8887),},
	{fxmetr(-28.1670),fxmetr( 27.9502),fxmetr( 14.1680),},
	{fxmetr(-21.4727),fxmetr( 34.7979),fxmetr( 10.9570),},
	{fxmetr(-21.2959),fxmetr( 35.9365),fxmetr( 10.9092),},
	{fxmetr(-17.6602),fxmetr( 35.1592),fxmetr( 12.2725),},
	{fxmetr(-21.2959),fxmetr( 38.6055),fxmetr( 10.9092),},
	{fxmetr(-17.5000),fxmetr( 38.4082),fxmetr( 12.2725),},
};

static IPlocha _PHrad_d5[]=
{
	{Gray80,(0),(1),(5),},
	{Gray80,(1),(2),(5),},
	{Gray80,(2),(6),(5),},
	{Gray80,(2),(7),(6),},
	{Gray80,(2),(3),(7),},
	{Gray80,(0),(7),(3),},
	{Gray80,(0),(4),(7),},
	{Gray80,(0),(5),(4),},
	{Gray35,(4),(5),(8),},
	{Gray35,(5),(6),(8),},
	{Gray35,(6),(7),(8),},
	{Gray35,(4),(8),(7),},
	{Gray35,(9),(10),(13),},
	{Gray35,(10),(11),(13),},
	{Gray35,(11),(12),(13),},
	{Gray35,(9),(13),(12),},
	{Green,(14),(16),(15),},
	{Green,(14),(15),(16),},
	{Yellow,(15),(17),(16),},
	{Yellow,(15),(16),(17),},
};

KKresliScena Hrad_d5={
	NULL,_BHrad_d5,_PHrad_d5,
	35,(bindex)lenof(_BHrad_d5),(bindex)lenof(_PHrad_d5)
};
