#version 410

in vec3 position;
in vec3 normal;
in vec4 color;

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 pMatrix;
uniform vec3 lightDirection;
uniform vec3 ambient;
uniform vec3 diffuse;

uniform vec3 hemiGround = vec3(0.6,0.7,0.5); 

out vec3 lit;
out vec3 emit;
out vec3 vColor;
out float material;

out float dist;

out vec3 worldNormal;
out vec4 worldPos;

void main()
{
  // transformation to the world space
  // diffuse 
  worldNormal = normalize(mat3(modelMatrix) * normal);
  worldPos = modelMatrix * vec4(position, 1);

  if (color.a<0.5 || color.a>1.5)
  {
    float diffuseF = dot(worldNormal, lightDirection);
    if (diffuseF<0) diffuseF = -diffuseF*0.1; // very weak back-side (reflected light) lighting

    float upDown = worldNormal.y*0.5+0.5;
    lit = diffuse*diffuseF + ambient*mix(hemiGround,vec3(1),upDown);
    vColor = color.rgb;
    emit = vec3(0);
  }
  else
  {
    lit = vec3(0);
    vColor = vec3(0);
    emit = color.rgb;
  }
  material = color.a;

  vec4 viewPos = viewMatrix * worldPos;
  dist = sqrt(dot(viewPos.xyz,viewPos.xyz));
  gl_Position = pMatrix * viewPos;
}

