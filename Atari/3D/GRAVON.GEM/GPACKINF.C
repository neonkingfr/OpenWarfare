/* kopirovani rizene souborem */
/* SUMA 11/1992 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <tos.h>
#include <macros.h>
#include <stdc\fileutil.h>

#include "gpackinf.h"
#include "packers.h"

static int MkAllDir( char *path )
{
	PathName P;
	char *BS;
	
	strcpy(P,path);
	BS=strchr(P,'\\');
	if( BS ) for(;;)
	{
		BS=strchr(&BS[1],'\\');
		if( BS )
		{
			*BS=0;
			Dcreate(P);
			*BS='\\';
		}
		else break;
	}
	return 0;
}

static Flag InExt( const char **In, const char *T )
{
	if( In ) while( *In )
	{
		if( !strcmpi(*In,T) ) return True;
		In++;
	}
	return False;
}

static PathName LastCD;

int PackRad( const char *Do, const char *Z, const char *Wild, Flag Safety, const char **NoExts, Flag Recurse )
{
	DTA F,*ODTA;
	PathName CestaZ,CestaD,CestaF,CestaC,CestaFF;
	int FNew;
	int ret=0;
	int f;
	strcpy(CestaZ,Z);
	strcpy(CestaD,Do);
	if( CestaZ[strlen(CestaZ)-1]!='\\' ) strcat(CestaZ,"\\");
	if( CestaD[strlen(CestaD)-1]!='\\' ) strcat(CestaD,"\\");
	strcpy(CestaF,CestaD);
	CestaF[strlen(CestaF)-1]=0;
	if( strcmpi(LastCD,CestaD) )
	{
		MkAllDir(CestaD);
		strcpy(LastCD,CestaD);
	}
	MCSetZ(CestaZ);
	MCSetDo(CestaD);
	strcpy(CestaF,CestaZ);
	strcat(CestaF,Wild);
	MCEvents();
	ODTA=Fgetdta();
	Fsetdta(&F);
	ret=0;
	if( Recurse ) for( f=Fsfirst(CestaF,FA_SUBDIR); f==0; f=Fsnext() ) if( (F.d_attrib&FA_SUBDIR) && F.d_fname[0]!='.' )
	{
		PathName NewZ,NewD;
		strcpy(NewZ,CestaZ),strcpy(NewD,CestaD);
		strcat(NewZ,F.d_fname),strcat(NewD,F.d_fname);
		if( PackRad(NewD,NewZ,Wild,Safety,NoExts,Recurse)<0 )
		{
			ret=-1;
			break;
		}
	}
	if( ret>=0 ) for( f=Fsfirst(CestaF,FA_READONLY|FA_HIDDEN|FA_SYSTEM); f==0; f=Fsnext() ) if( !InExt(NoExts,NajdiExt(F.d_fname)) )
	{
		Flag Copy=False;
		strcpy(CestaF,CestaD);
		strcpy(CestaFF,CestaD);
		strcat(CestaF,F.d_fname);
		strcat(CestaFF,"MEGACOPY.$$$");
		FNew=Fopen(CestaF,0);
		if( FNew>=0 )
		{
			DOSTIME D;
			Fdatime(&D,FNew,False);
			Copy=D.date<F.d_date || D.date==F.d_date && D.time<F.d_time;
			Fclose(FNew);
			if( Copy ) Fattrib(CestaF,True,0);
		}
		else Copy=True;
		if( !Copy ) MCSetName(""),MCEvents();
		else
		{
			Flag Err=False;
			strcpy(CestaC,CestaZ);
			strcat(CestaC,F.d_fname);
			MCSetName(CestaF),MCEvents();
			if( !Safety ) /* bal�me */
			{
				FILE *out=fopen(CestaF,"wb");
				if( out>=0 )
				{
					FILE *in=fopen(CestaC,"rb");
					if( in>=0 )
					{
						if( F.d_length>0 )
						{
							if( setvbuf(in,NULL,_IOFBF,64*1024L)!=0 ) Err=True;
							if( !Err && setvbuf(out,NULL,_IOFBF,64*1024L)!=0 ) Err=True;
							if( !Err && fwrite(&F.d_length,4,1,out)!=1 ) Err=True;
							if( !Err )
							{
								long PackI='PACK';
								const char *E=NajdiPExt(F.d_fname);
								if( !strcmp(E,".SSS") ) PackI='COPY';
								if( fwrite(&PackI,4,1,out)!=1 ) Err=True;
								if( PackI=='PACK' )
								{
									long CS=EncodeSS(F.d_length,in,out);
									if( CS<0 ) Err=True;
									else
									{
										if( F.d_length>0 ) printf("Spakov�no na %ld %%\n",CS*100/F.d_length);
										else printf("Pr�zdn� soubor\n");
									}
								}
								else
								{
									long CS=CopyFile(F.d_length,in,out);
									if( CS<0 ) Err=True;
									else printf("Zkop�rov�no\n");
								}
							}
						}
						fclose(in);
					}
					fclose(out);
				}
				else Err=True;
				if( Err ) DChyba(WFileErr,SizeNazev(CestaF,24));
			} /* konec balen� */
			else
			{ /* rozbalujeme */
				FILE *out=fopen(CestaF,"wb");
				if( out>=0 )
				{
					FILE *in=fopen(CestaC,"rb");
					if( in>=0 )
					{
						long Len;
						long PackI;
						if( setvbuf(in,NULL,_IOFBF,64*1024L)!=0 ) Err=True;
						if( !Err && setvbuf(out,NULL,_IOFBF,64*1024L)!=0 ) Err=True;
						if( !Err && fread(&Len,4,1,in)!=1 ) Err=True;
						if( !Err && fread(&PackI,4,1,in)!=1 ) Err=True;
						if( !Err )
						{
							if( PackI=='PACK' )
							{
								if( DecodeSS(Len,in,out)<0 ) DChyba(WFileErr,SizeNazev(CestaF,24));
							}
							else
							{
								if( CopyFile(Len,in,out)<0 ) DChyba(WFileErr,SizeNazev(CestaF,24));
							}
						}
						fclose(in);
					}
					fclose(out);
				}
				else Err=True;
				if( Err ) DChyba(WFileErr,SizeNazev(CestaF,24));
			} /* konec rozbalov�n� */
			if( !Err )
			{
				DOSTIME DNTime;
				int FNew=Fopen(CestaF,FO_READ);
				if( FNew>=0 )
				{
					DNTime.time=F.d_time;
					DNTime.date=F.d_date;
					Fdatime(&DNTime,FNew,True);
					Err=Fclose(FNew)<0;
				}
			}
			else
			{
				Fattrib(CestaF,True,0);
				Fdelete(CestaF);
			}
			if( Err ) {ret=-1;break;}
		}
	} /* for all files */
	MCSetName("");
	Fsetdta(ODTA); /* zajisti rekurzi */
	return ret;
}


