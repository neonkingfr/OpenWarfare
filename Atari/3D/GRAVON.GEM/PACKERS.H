long EncodeSS( long lensb, FILE *in, FILE *out );
int DecodeSS( long lensb, FILE *in, FILE *out );
int CopyFile( long lensb, FILE *in, FILE *out );

int IDecodeSS( long lensb, long lenf, int in, int out );
int ICopyFile( long lensb, int in, int out );

void *UnpackSS( long lensb, void *in, void *out );

void PrintCount( long a, int b );
