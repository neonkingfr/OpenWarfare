/* kopirovani rizene souborem */
/* SUMA 11/1992 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <tos.h>
#include <macros.h>
#include <stdc\fileutil.h>

#include "gpackinf.h"
#include "packers.h"

static int MkAllDir( char *path )
{
	PathName P;
	char *BS;
	
	strcpy(P,path);
	BS=strchr(P,'\\');
	if( BS ) for(;;)
	{
		BS=strchr(&BS[1],'\\');
		if( BS )
		{
			*BS=0;
			Dcreate(P);
			*BS='\\';
		}
		else break;
	}
	return 0;
}

static Flag InExt( const char **In, const char *T )
{
	if( In ) while( *In )
	{
		if( !strcmpi(*In,T) ) return True;
		In++;
	}
	return False;
}

static PathName LastCD;

int PackRad( const char *Do, const char *Z, const char *Wild, Flag Safety, const char **NoExts, Flag Recurse )
{
	DTA F,*ODTA;
	PathName CestaZ,CestaD,CestaF,CestaC;
	int FNew;
	int ret=0;
	int f;
	strcpy(CestaZ,Z);
	strcpy(CestaD,Do);
	if( CestaZ[strlen(CestaZ)-1]!='\\' ) strcat(CestaZ,"\\");
	if( CestaD[strlen(CestaD)-1]!='\\' ) strcat(CestaD,"\\");
	strcpy(CestaF,CestaD);
	CestaF[strlen(CestaF)-1]=0;
	if( strcmpi(LastCD,CestaD) )
	{
		MkAllDir(CestaD);
		strcpy(LastCD,CestaD);
	}
	MCSetZ(CestaZ);
	MCSetDo(CestaD);
	strcpy(CestaF,CestaZ);
	strcat(CestaF,Wild);
	MCEvents();
	ODTA=Fgetdta();
	Fsetdta(&F);
	ret=0;
	if( Recurse ) for( f=Fsfirst(CestaF,FA_SUBDIR); f==0; f=Fsnext() ) if( (F.d_attrib&FA_SUBDIR) && F.d_fname[0]!='.' )
	{
		PathName NewZ,NewD;
		strcpy(NewZ,CestaZ),strcpy(NewD,CestaD);
		strcat(NewZ,F.d_fname),strcat(NewD,F.d_fname);
		if( PackRad(NewD,NewZ,Wild,Safety,NoExts,Recurse)<0 )
		{
			ret=-1;
			break;
		}
	}
	if( ret>=0 ) for( f=Fsfirst(CestaF,FA_READONLY|FA_HIDDEN|FA_SYSTEM); f==0; f=Fsnext() ) if( !InExt(NoExts,NajdiExt(F.d_fname)) )
	{
		Flag Copy=False;
		strcpy(CestaF,CestaD);
		strcat(CestaF,F.d_fname);
		FNew=Fopen(CestaF,0);
		if( FNew>=0 )
		{
			DOSTIME D;
			Fdatime(&D,FNew,False);
			Copy=D.date<F.d_date || D.date==F.d_date && D.time<F.d_time;
			Fclose(FNew);
			if( Copy ) Fattrib(CestaF,True,0);
		}
		else Copy=True;
		if( !Copy ) MCSetName(""),MCEvents();
		else
		{
			Flag Err=False;
			strcpy(CestaC,CestaZ);
			strcat(CestaC,F.d_fname);
			MCSetName(CestaF),MCEvents();
			if( !Safety ) /* bal�me */
			{
				Err=True; /* neum�me balit */
			}
			else
			{ /* rozbalujeme */
				int out=Fcreate(CestaF,F.d_attrib);
				if( out>=0 )
				{
					int in=Fopen(CestaC,FO_READ);
					if( in>=0 )
					{
						struct head
						{
							long Len;
							long PackI;
						} Head;
						if( Fread(in,sizeof(Head),&Head)!=sizeof(Head) ) Err=True;
						if( !Err )
						{
							if( Head.PackI=='PACK' )
							{
								if( IDecodeSS(Head.Len,F.d_length-sizeof(Head),in,out)<0 ) DChyba(WFileErr,SizeNazev(CestaF,24));
							}
							else
							{
								if( ICopyFile(Head.Len,in,out)<0 ) DChyba(WFileErr,SizeNazev(CestaF,24));
							}
						}
						Fclose(in);
					}
					if( !Err )
					{
						DOSTIME DNTime;
						DNTime.time=F.d_time;
						DNTime.date=F.d_date;
						Fdatime(&DNTime,out,True);
						Err=Fclose(out)<0;
					}
					else
					{
						Fclose(out);
						Fattrib(CestaF,True,0);
						Fdelete(CestaF);
					}
				}
				else Err=True;
				if( Err ) DChyba(WFileErr,SizeNazev(CestaF,24));
			} /* konec rozbalov�n� */
			if( Err ) {ret=-1;break;}
		}
	} /* for all files */
	MCSetName("");
	Fsetdta(ODTA); /* zajisti rekurzi */
	return ret;
}


