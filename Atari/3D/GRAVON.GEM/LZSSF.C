/**************************************************************
	LZSS.C -- A Data Compression Program
	upravena verze - pro potreby SUMY - MEGACOPY
**************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <macros.h>
#include "packers.h"

#define N		 4096
#define F		   18
#define THRESHOLD	2

byte text_buf[N + F - 1];

#define _TWICE 1

#if !_TWICE
	#define lson(n) (lsons[n])
	#define rson(n) (rsons[n])
	#define dad(n)  (dads[n])
	#define NIL N
#else
	#define lson(n) ( *(int*)&( ((byte *)lsons)[n] ) )
	#define rson(n) ( *(int*)&( ((byte *)rsons)[n] ) )
	#define dad(n)  ( *(int*)&( ((byte *)dads)[n] ) )
	#define NIL (2*N)
#endif

int match_position,match_len;
int lsons[N+1],rsons[N+257],dads[N+1];


void CInitTree(void)  /* initialize trees */
{
	int  i;

	for (i = N + 1; i <= N + 256; i++) rsons[i] = NIL;
	for (i = 0; i < N; i++) dads[i] = NIL;
}

void CInsertNode(int r)
{
	int  i, p, cmp;
	byte *key;

	cmp = 1;  key = &text_buf[r];  p = (N + 1 + key[0]);
	#if _TWICE
		r<<=1;
		p<<=1;
	#endif
	rson(r) = lson(r) = NIL;  match_len = 0;
	for ( ; ; ) {
		if ( cmp ) {
			if (rson(p) != NIL) p = rson(p);
			else {  rson(p) = r;  dad(r) = p;  return;  }
		} else {
			if (lson(p) != NIL) p = lson(p);
			else {  lson(p) = r;  dad(r) = p;  return;  }
		}
		{
			#if _TWICE
				int ppul=p>>1;
			#else
				#define ppul p
			#endif
			byte *tbp=&text_buf[ppul+1];
			byte *kp=&key[1];
			for (i = 1; i < F; i++)
				if( *kp++!=*tbp++ ) { cmp=kp[-1]>=tbp[-1];break; }
			if (i > match_len) {
				match_position = ppul;
				if ((match_len = i) >= F)  break;
			}
		}
	}
	dad(r) = dad(p);  lson(r) = lson(p);  rson(r) = rson(p);
	dad(lson(p)) = r;  dad(rson(p)) = r;
	if (rson(dad(p)) == p) rson(dad(p)) = r;
	else                   lson(dad(p)) = r;
	dad(p) = NIL;  /* remove p */
}

void CDeleteNode(int p)  /* deletes node p from tree */
{
	int  q;
	#if _TWICE
		p<<=1;
	#endif
	if (dad(p) == NIL) return;  /* not in tree */
	if (rson(p) == NIL) q = lson(p);
	else if (lson(p) == NIL) q = rson(p);
	else {
		q = lson(p);
		if (rson(q) != NIL) {
			do {  q = rson(q);  } while (rson(q) != NIL);
			rson(dad(q)) = lson(q);  dad(lson(q)) = dad(q);
			lson(q) = lson(p);  dad(lson(p)) = q;
		}
		rson(q) = rson(p);  dad(rson(p)) = q;
	}
	dad(q) = dad(p);
	if (rson(dad(p)) == p) rson(dad(p)) = q;  else lson(dad(p)) = q;
	dad(p) = NIL;
}

void AInsertNode( int r );

#define InsNode AInsertNode
#define DelNode CDeleteNode
#define IniTree CInitTree

long EncodeSS( long lensb, FILE *in, FILE *out )
{
	int  i,c,len,r,s,last_match_len,CPtr;
	byte CBuf[17],mask;
	lword textsize,codesize,printcount;
	int csum;
	enum {pcstep=8*1024};
	if( lensb==0 ) return 0;
	printcount=pcstep;
	codesize=textsize=0;
	IniTree();  /* initialize trees */
	csum=0;
	CBuf[0]=0;
	CPtr=mask=1;
	s=0;  r=N-F;
	for( i=s; i<r; i++ ) text_buf[i]=' ';
	for( len=0; len<F && (c=getc(in))!=EOF; len++ )
	{
		text_buf[r+len]=c;
		csum+=(char)c;
	}
	if( (textsize=len)==0 ) goto Error; /* lensb>0 */
	for( i = 1; i<=F; i++ ) InsNode(r-i);
	InsNode(r);
	do
	{
		if( match_len>len ) match_len=len;
		if( match_len<=THRESHOLD )
		{
			match_len=1;
			CBuf[0]|=mask;
			CBuf[CPtr++]=text_buf[r];
		}
		else
		{
			int mp=(r-match_position)&(N-1);
			CBuf[CPtr++]=(byte)mp;
			CBuf[CPtr++]=(byte)(((mp>>4)&0xf0)|(match_len-(THRESHOLD+1)));
		}
		if( (mask<<=1)==0 )
		{
			if( fwrite(CBuf,CPtr,1,out)!=1 ) goto Error; 
			codesize+=CPtr;
			CBuf[0]=0; CPtr=mask=1;
		}
		last_match_len=match_len;
		for ( i=0; i<last_match_len && (c=getc(in))!= EOF; i++ )
		{
			DelNode(s);		/* Delete old strings and read new bytes */
			text_buf[s]=c;
			csum+=(char)c;
			if ( s<F-1 ) text_buf[s+N]=c; /* beg. of buf. */
			s++;s&=N-1;
			r++;r&=N-1;
			InsNode(r);
		}
		if( (textsize+=i)>printcount )
		{
			PrintCount(pcstep,0);
			printcount+=pcstep;
		}
		while( i++<last_match_len )
		{
			DelNode(s);					/* EOF => no need to read, but */
			s++;s&=N-1;
			r++;r&=N-1;
			if( --len ) InsNode(r);		/* buffer may not be empty. */
		}
	} while( len>0 );
	if( textsize<lensb ) goto Error;
	if( CPtr>1 )
	{
		if( fwrite(CBuf,CPtr,1,out)!=1 ) goto Error; 
		codesize+=CPtr;
	}
	if( fwrite(&csum,sizeof(int),1,out)!=1 ) goto Error;
	codesize+=sizeof(int);
	PrintCount(textsize+pcstep-printcount,1);
	return codesize;
	Error:
	return -1;
}

#define BufSize ( 640L*1024 )
static char Buf[BufSize];

int DecodeSS( long lensb, FILE *in, FILE *out )
{
	int  i,j,r,c,csum=0,csr;
	word flags;
	if( lensb<=0 ) return 0;
	for( i=0; i<N-F; i++ ) text_buf[i] = ' ';
	r=N-F; flags=0;
	while( lensb>0 )
	{
		if( ((flags>>= 1)&256)==0 )
		{
			if( (c=fgetc(in))==EOF ) goto Error;
			flags=c|0xff00;
		}
		if( flags&1 )
		{
			if( (c=fgetc(in))==EOF ) goto Error;
			csum+=(char)c;
			if( errno=0,fputc(c,out),errno ) goto Error;
			lensb--;
			text_buf[r]=c;
			r++;r&=(N-1);
		}
		else
		{
			if( (i=fgetc(in))==EOF ) goto Error;
			if( (j=fgetc(in))==EOF ) goto Error;
			i|=(j&0xf0)<<4; j&=0x0f; j+=THRESHOLD;
			lensb-=j;
			errno=0;
			for( i=r-i,j+=i; i<=j; i++ )
			{
				c=text_buf[i&(N-1)];
				csum+=(char)c;
				if( fputc(c,out),errno ) goto Error;
				text_buf[r]=c;
				r++;r&=(N-1);
			}
			lensb--;
		}
	}
	if( fread(&csr,sizeof(int),1,in)!=1 || csr!=csum ) goto Error;
	return 0;
	Error:
	return -1;
}

int CopyFile( long lensb, FILE *in, FILE *out )
{
	long Rd,Wr;
	while( lensb>0 )
	{
		Rd=fread(Buf,1,BufSize,in);
		if( Rd<=0 ) return -1;
		Wr=fwrite(Buf,1,Rd,out);
		if( Wr!=Rd ) return -1;
		lensb-=Wr;
	}
	return 0;
}

int ICopyFile( long lensb, int h_in, int h_out )
{
	long Rd;
	if( lensb<=BufSize )
	{
		Rd=read(h_in,Buf,lensb);
		if( Rd!=lensb ) return -1; /* neum�me */
		if( write(h_out,Buf,lensb)!=lensb ) return -1;
		return 0;
	}
	return -1;
}

int IDecodeSS( long lensb, long LenF, int h_in, int h_out )
{
	long Rd;
	void *U;
	if( LenF>=BufSize-32*1024L ) return -1; /* neum�me */
	Rd=read(h_in,Buf+BufSize-LenF,LenF);
	if( Rd!=LenF ) return -1;
	U=UnpackSS(lensb,Buf+BufSize-LenF,Buf);
	if( !U ) return -1;
	if( write(h_out,U,lensb)!=lensb ) return -1;
	return 0;
}
