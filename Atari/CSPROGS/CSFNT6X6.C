#include <macros.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef byte FontLine[256];
static FontLine F8[8];

typedef byte Line6[(256*6+5)/8];
static Line6 F6[6];

static void CH86( int c )
{ /* transformace jednoho znaku 8x8 -> 6x6 */
	int l;
	for( l=0; l<6; l++ )
	{
		word s=F8[l][c];
		word *d;
		word m=0xfc;
		int x=6*c;
		int xm=x&7;
		int xd=x>>3;
		xm=8-xm;
		s<<=xm;
		m<<=xm;
		d=(word *)&F6[l][xd];
		*d&=~m;
		*d|=s;
	}
}

static void Tform86( void )
{
	int i;
	for( i=0; i<256; i++ ) CH86(i);
}

int main()
{
	PathName FntS="CS6x6S.FNT";
	PathName FntD="C:\\CESTINA\\CSZOBRAZ.6x6";
	int s,d;
	int ret=0;
	s=open(FntS,O_RDONLY);
	if( s>=0 )
	{
		lseek(s,0x800,SEEK_SET);
		read(s,F8,sizeof(F8)); /* prvni pulka 16*8 */
		close(s);
	}
	else ret=s;
	if( ret<0 ) return ret;
	Tform86();
	d=creat(FntD,0);
	if( d>=0 )
	{
		write(s,F6,sizeof(F6));
		close(d);
	}
	else ret=d;
	return ret;
}
