#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <tos.h>
#include <aes.h>
#include <macros.h>
#include <gem\interf.h>
#include <sttos\cszvaz.h>

typedef struct cfile
{
	char *Buf;
	long Len;
	char PathName[64];
	struct cfile *next;
} CFile;

static char *Str[]=
{
	/* 0 */ "[3][   M�lo pam�ti!   ][  Konec  ]",
	/* 1 */ "[3][Chyba p�i �ten� souboru |'%s'.][  Konec  ]",
	/* 2 */ "[3][Chyba p�i z�pisu souboru|'%s'.][  Konec  ]",
	/* 3 */ "[3][  Nemohu nal�zt soubor  |'%s'.][  Konec  ]",
	/* 4 */ "[0]["
					"  Instalace syst�mu  |"
					"   ST �e�tina 2.6    |"
					"   � 1994, � SUMA    | |"
					"Nevym��ujte disketu! ]"
					"[OK| Zru�it ]",
	/* 5 */ "[2]["
					"Instalovat �adi� pro|"
					"dev�t�jehli�kov� tisk�rny?]"
					"[ Ano | Ne ]",
	/* 6 */ "[2]["
					" Instalovat PRINTER.CFG |"
					"    pro 1st WordPlus?    |"
					"][ Ano | Ne ]",
	/* 7 */ "[2]["
					"    Typ znak� na tisk�rn�    ]"
					"[Roman|Sans ser.|LQ Roman]",
	/* 8 */ "[2]["
					"Jak� chcete ovl�dac� panel?|"
					"][ ACC | CPX | ��dn� ]",
	/* 9 */ "[1]["
					"   Instalovat na:   "
					"][ Disketu | Pevn� disk ]",
	/*10 */ "[1]["
					"ST �e�tina bude zavedena|"
					"po resetu po��ta�e."
					"][   OK   ]",
	/*11 */ "[1]["
					"Vlo�te c�lovou disketu"
					"][   OK   ]",
	/*12 */ "Konfig. soubor pro 1st Word+",
};

static void Zkrat( char *N )
{
	strupr(N);
	if( strlen(N)>28 ) N[28]=0;
}

static void MemLo( void )
{
	form_alert(1,Str[0]);
	appl_exit();exit(0);
}
static void RdErr( char *Name )
{
	char a[256];
	Zkrat(Name);sprintf(a,Str[1],Name);form_alert(1,a);
	appl_exit();exit(0);
}

static void WrErr( char *Name )
{
	char a[256];
	Zkrat(Name);sprintf(a,Str[2],Name);form_alert(1,a);
	appl_exit();exit(0);
}
static void MisErr( char *Name )
{
	char a[256];
	Zkrat(Name);sprintf(a,Str[3],Name);form_alert(1,a);
	appl_exit();exit(0);
}

static CFile *ReadF( char *wild )
{
	int f;
	DTA FFnd,*odta;
	CFile Beg;
	CFile *NF=&Beg,*NFO;
	char Path[64],*PP;
	int FIn;
	strcpy(Path,wild);
	strupr(Path);
	PP=strrchr(Path,'\\');
	if( PP ) *PP=0;
	Beg.next=NULL;
	odta=Fgetdta();
	Fsetdta(&FFnd);
	if( *wild=='\\' ) wild++;
	for( f=Fsfirst(wild,1); f==0; f=Fsnext() )
	{
		NFO=NF;
		NF=malloc(sizeof(CFile));
		if( NF==NULL ) MemLo();
		NFO->next=NF;
		NF->next=NULL;
		strcpy(NF->PathName,Path);
		strcat(NF->PathName,"\\");
		strcat(NF->PathName,FFnd.d_fname);
		NF->Len=FFnd.d_length;
		NF->Buf=malloc(NF->Len);
		if( NF->Buf==NULL ) MemLo();
		if( *NF->PathName=='\\' ) FIn=Fopen(NF->PathName+1,0);
		else FIn=Fopen(NF->PathName,0);
		if( FIn<0 ) RdErr(NF->PathName);
		if( Fread(FIn,NF->Len,NF->Buf)!=NF->Len ) {Fclose(FIn);RdErr(NF->PathName);}
		Fclose(FIn);
	}
	Fsetdta(odta);
	if( Beg.next==NULL ) MisErr(wild);
	return Beg.next;
}
static void WriteF( CFile *writ )
{
	if( writ!=NULL )
	{
		CFile *W,*WN;
		int FOut;
		char Path[64];
		strcpy(Path,writ->PathName);
		*strrchr(Path,'\\')=0;
		Dcreate(Path);
		for( W=writ; W!=NULL; )
		{
			FOut=Fcreate(W->PathName,0);
			if( FOut<0 ) WrErr(W->PathName);
			if( Fwrite(FOut,W->Len,W->Buf)!=W->Len ) {Fclose(FOut);WrErr(W->PathName);}
			if( Fclose(FOut)<0 ) WrErr(W->PathName);
			WN=W->next;
			free(W->Buf);
			free(W);
			W=WN;
		}
	}
}

static PathName CPXCS="\\CPX\\CSZOBRAZ.CPX";

int main()
{
	CFile *CsTisk=NULL;
	CFile *CsZobraz=NULL;
	CFile *CsPanel=NULL;
	CFile *CsFonts=NULL;
	CFile *TFont=NULL;
	CFile *WpCfg=NULL;
	CFile *Cs6x6=NULL;
	if( appl_init()>=0 )
	{
		Mouse(HOURGLASS);
		{
			char **i;
			int c;
			int N;
			CSGVazba();
			N=CSTestNorm();
			for( c=0,i=Str; c<(int)lenof(Str); c++,i++ )
			{
				CSStrCode(*i,N);
			}
		}
		if( 1==form_alert(1,Str[4]) )
		{
			int DO,DN;
			int TiskP=0,WordP=0,FontT=0,PanelP=0;
			Flag HardD=(Drvmap()&4)!=0;
			if( HardD )
			{
				FILE *F=fopen("C:\\CONTROL.INF","r");
				char Buf[128];
				if( F )
				{
					char *eb;
					fgets(Buf,(int)sizeof(Buf),F);
					fclose(F);
					eb=Buf+strlen(Buf);
					if( eb[-1]=='\n' ) eb[-1]=0;
					eb=strrchr(Buf,'\\');
					if( eb ) *eb=0;
					strcat(Buf,"\\CSZOBRAZ.CPX");
					strcpy(CPXCS,Buf+2);
				}
			}
			TiskP=2-form_alert(1,Str[5]);
			if( TiskP )
			{
				WordP=2-form_alert(HardD ? 2 : 1,Str[6]);
				FontT= form_alert(1,Str[7]);
			}
			PanelP=form_alert(HardD ? 2 : 1,Str[8]);
			
			CsZobraz=ReadF("\\auto\\cszobraz.prg");
			Cs6x6=ReadF("\\cestina\\*.6x6");
			if( TiskP )
			{
				CsTisk=ReadF("\\auto\\cstisk.prg");
				if( WordP ) WpCfg=ReadF("\\printer.cfg");
				switch( FontT )
				{
					case 1: TFont=ReadF("\\cestina\\roman.fnp");break;
					case 2: TFont=ReadF("\\cestina\\sansser.fnp");break;
					case 3: TFont=ReadF("\\cestina\\lqroman.fnp");break;
				}
				if( TFont!=NULL )
				{
					strcpy(TFont->PathName,"\\cestina\\cstisk.fnp");
				}
			}
			if( PanelP<3 )
			{
				CsFonts=ReadF("\\cestina\\*.fnt");
				if( PanelP==1 ) CsPanel=ReadF("\\cspanel.acc");
				else
				{
					CsPanel=ReadF("\\cpx\\cszobraz.cpx");
					if( CsPanel ) strcpy(CsPanel->PathName,CPXCS);
				}
			}
			else
			{
				CsFonts=ReadF("\\cestina\\systemcs.fnt");
			}
			DO=Dgetdrv();
			DN=2==form_alert(HardD ? 2 : 1,Str[9]) ? 2 : 0;
			if( DN<2 ) form_alert(1,Str[11]);
			Dsetdrv(DN);
			if( WpCfg )
			{
				PathName FW,FP,FN;
				strcpy(FW,"A:\\PRINTER.CFG");
				FW[0]=DN+'A';
				VyrobCesty(FP,FN,FW);
				if( EFSel(Str[12],FP,FN,FW) )
				{
					strcpy(WpCfg->PathName,FW);
				}
				else
				{
					free(WpCfg->Buf);
					free(WpCfg);
					WpCfg=NULL;
				}
			}
			Fdelete("\\auto\\cszobraz.prx");
			WriteF(CsZobraz);
			Fdelete("\\auto\\cstisk.prx");
			if( CsTisk==NULL ) Fdelete("\\auto\\cstisk.prg");
			else WriteF(CsTisk);
			Fdelete("\\cspanel.acx");
			{
				char *e=CPXCS+strlen(CPXCS);
				if( e>CPXCS )
				{
					e[-1]='Z';
					Fdelete(CPXCS);
					e[-1]='X';
				}
			}
			if( PanelP!=1 ) Fdelete("\\cspanel.acc");
			if( PanelP!=2 ) Fdelete(CPXCS);
			if( PanelP<3 ) WriteF(CsPanel);
			WriteF(Cs6x6);
			WriteF(CsFonts);
			WriteF(TFont);
			WriteF(WpCfg);
			if( Dgetdrv()>0 )
			{
				form_alert(1,Str[10]);
			}
			Dsetdrv(DO);
		}
		appl_exit();
	}
	return 0;
}
