/* program pro preklad do/z cestiny */
/* SUMA 2/1994-8/1994 */

#include <aes.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <tos.h>
#include <stdlib.h>
#include <macros.h>
#include <stdc\dynpole.h>
#include <gem\multiitf.h>
#include "transl.h"

typedef struct ph
{
	int ph_branch;
	long ph_tlen;
	long ph_dlen;
	long ph_blen;
	long ph_slen;
	long ph_res1; /* must be 0 */
	long ph_res2; /* must be 0 */
	int ph_flag; /* DR Flag - should be 0 */
} PH;

enum typsoub {Rsc,Prg,Asc,Dta};

typedef struct
{
	char *Orig;
	char *Prek;
	int JazykZ;
	int JazykD;
} Dvojice;

static DynPole Prekl={sizeof(Dvojice),256,malloc,free};
static Flag PreklZmena;

char *Duplicate( const char *S )
{
	char *s=malloc(strlen(S)+1);
	if( !s ) return s;
	strcpy(s,S);
	return s;
}
char *NDuplicate( const char *S, const char *es )
{
	char *s=malloc(es-S+1);
	if( !s ) return s;
	strlncpy(s,S,es-S+1);
	return s;
}

static void PustJazyk( DynPole *Jaz )
{
	long NS;
	for( NS=0; NS<NDyn(Jaz); NS++ )
	{
		Dvojice *Dvoj=AccDyn(Jaz,NS);
		if( Dvoj->Orig ) free(Dvoj->Orig),Dvoj->Orig=NULL;
		if( Dvoj->Prek ) free(Dvoj->Prek),Dvoj->Prek=NULL;
	}
}

static int NactiJazyk( DynPole *Jaz, const char *Soub )
{
	int ret=0;
	FILE *f=fopen(Soub,"r");
	if( f )
	{
		enum {SL=4*1024}; /* max. delka radku */
		char Z[SL],D[SL],J[SL];
		while( fgetl(J,SL,f) && fgetl(Z,SL,f) && fgetl(D,SL,f) )
		{
			if( strcmp(Z,D) )
			{ /* stejne dvojice vypoustej */
				long NS=NewDyn(Jaz);
				if( NS>=0 )
				{
					Dvojice *Dvoj=AccDyn(Jaz,NS);
					Dvoj->Orig=Duplicate(Z);
					Dvoj->Prek=Duplicate(D);
					Dvoj->JazykZ=toupper(J[0]);
					Dvoj->JazykD=toupper(J[1]);
					if( Dvoj->JazykZ=='0' ) Dvoj->JazykZ=0;
					if( Dvoj->JazykD==' ' ) Dvoj->JazykD='C';
					ef( Dvoj->JazykD=='0' ) Dvoj->JazykD=0;
					if( !Dvoj->Orig || !Dvoj->Prek ) {ChybaRAM();DelDyn(Jaz,NS);ret=-1;break;}
				}
				else {ChybaRAM();ret=-1;break;}
			}
		}
		fclose(f);
	}
	else AlertRF(IERRA,1,SizeNazev(Soub,25));
	return ret;
}
static int UlozJazyk( DynPole *Jaz, const char *Soub )
{
	int ret=-1;
	FILE *f=fopen(Soub,"w");
	if( f )
	{
		long NS;
		for( NS=0; NS<NDyn(Jaz); NS++ )
		{
			Dvojice *Dvoj=AccDyn(Jaz,NS);
			int JZ=Dvoj->JazykZ;
			int JD=Dvoj->JazykD;
			if( JZ==0 ) JZ='0';
			if( JD==0 ) JD='0';
			if( EOF==fprintf(f,"%c%c <#\n%s\n%s\n",toupper(JZ),toupper(JD),Dvoj->Orig,Dvoj->Prek) ) goto Error;
		}
		ret=0;
		Error:
		fclose(f);
	}
	if( ret<0 ) AlertRF(WERRA,1,SizeNazev(Soub,25));
	return ret;
}

static int ZacJazyky( void )
{
	int r;
	ZacDyn(&Prekl);
	r=NactiJazyk(&Prekl,"TRANSL.SLO");
	PreklZmena=False;
	if( r<0 ) PustJazyk(&Prekl),KonDyn(&Prekl);
	return r;
}

static void KonJazyky( void )
{
	if( PreklZmena ) UlozJazyk(&Prekl,"TRANSL.SLO");
	PustJazyk(&Prekl);
	KonDyn(&Prekl);
}

static const char *NajdiSlovo( DynPole *Jaz, const char *s, const char *es, int JazZ, int JazD )
{
	long NS;
	Dvojice *D;
	for( D=PrvniDyn(Jaz,&NS); D; D=DalsiDyn(Jaz,D,&NS) )
	{
		if
		(
			( !JazZ || !D->JazykZ || JazZ==D->JazykZ ) &&
			( !JazD || !D->JazykD || JazD==D->JazykD )
		)
		{
			if( !memcmp(s,D->Orig,es-s) ) return D->Prek;
		}
	}
	for( D=PrvniDyn(Jaz,&NS); D; D=DalsiDyn(Jaz,D,&NS) )
	{
		if
		(
			( !JazZ || !D->JazykD || JazZ==D->JazykD ) &&
			( !JazD || !D->JazykZ || JazD==D->JazykZ )
		)
		{
			if( !memcmp(s,D->Prek,es-s) ) return D->Orig;
		}
	}
	return NULL;
}

static char *CountS( const char *S, char *LP )
{
	const char *s;
	for( s=S; *s && LP[*s]; s++ );
	return (char *)s;
}

static Flag NutnoPrekladat( const char *s, const char *es, char *JeZavisle )
{
	for( ; s<es; s++ ) if( JeZavisle[*s] ) return True;
	return False;
}

static void Nahrad( char *s, char *es, const char *t )
{
	while( s<es && *s ) /* nesmime prodluzovat */
	{
		*s=*t;
		if( !*t ) break; /* smime zkracovat */
		s++,t++;
	}
}

#define MAX_STRING 8000

static word OldString[MAX_STRING]; /* offsety v puv. resourcu */
static word NewString[MAX_STRING]; /* offsety v novem resourcu */
static int NStr;
static int ZmenaOff; /* o tolik se meni */
static int KonStr; /* odsud se uplatunje zmena */

static void *SeznamStrPreloz( char *K, char *AnBuf, long *_Size, int JazZ, int JazD, Flag Analysa, int OffS )
{ /* nulami oddelene retezce */
	char *e;
	long Size=*_Size;
	char *BD;
	char *BE,*KD;
	char *Buf=K;
	if( !Analysa )
	{
		BD=malloc(Size*4+4*1024);
		if( !BD ) {ChybaRAM();return K;}
		BE=BD+Size*4;
	}
	else BD=AnBuf,BE=AnBuf+0xffff; /* neomezuj */
	NStr=0;
	for( KD=BD,e=K+Size; K<e && KD<=BE; )
	{
		char *es;
		const char *OS;
		OldString[NStr]=(word)(K-Buf)+OffS;
		NewString[NStr]=(word)(KD-BD)+OffS;
		NStr++;
		for( es=K; es<e && *es; es++ ); /* najdi konec retezce */
		OS=NajdiSlovo(&Prekl,K,es+1,JazZ,JazD);
		if( !Analysa ) /* zameny */
		{
			if( !OS )
			{
				strcpy(KD,K);
				KD+=strlen(K)+1;
			}
			else /* slovo nalezeno */
			{
				strcpy(KD,OS);
				KD+=strlen(OS)+1;
			}
			K=es+1; /* posun se za konec */
		}
		else /* analyza */
		{
			if( !OS )
			{
				long NS=NewDyn(&Prekl);
				if( NS>=0 )
				{
					Dvojice *Dvoj=AccDyn(&Prekl,NS);
					Dvoj->Orig=Duplicate(K);
					Dvoj->Prek=Duplicate(KD);
					Dvoj->JazykZ=JazZ;
					Dvoj->JazykD=JazD;
					PreklZmena=True;
				}
				else {ChybaRAM();break;}
				K+=strlen(K)+1;
				KD+=strlen(KD)+1;
			}
			else
			{
				K+=strlen(K)+1;
				KD+=strlen(KD)+1;
			}
		}
	}
	if( (KD-BD)&1 ) *KD++=0; /* word align */
	*_Size=KD-BD;
	return BD;
}

static void ViceStrPreloz( char *K, long Size, int JazZ, int JazD, Flag ZeroT, Flag Analysa, Flag Zkoumej )
{
	char JePismeno[256];
	char JeZavisle[256];
	char *e;
	char *Pis=" กขฃ"; /* Ctrl-G je casto v menu */
	char *Zav=" กขฃ";
	int i;
	if( Analysa && !ZeroT ) return;
	memset(JeZavisle,0,sizeof(JeZavisle));
	if( Zkoumej )
	{
		memset(JePismeno,0,sizeof(JePismeno));
		for( i=' '; i<0x80; i++ ) JePismeno[i]=True;
		for( ; *Pis; Pis++ ) JePismeno[*Pis]=True;
	}
	else
	{
		memset(JePismeno,True,sizeof(JePismeno));
		JePismeno[0]=False;
	}
	for( i='A'; i<'Z'; i++ ) JeZavisle[i]=True;
	for( i='a'; i<'z'; i++ ) JeZavisle[i]=True;
	for( ; *Zav; Zav++ ) JeZavisle[*Zav]=True;
	for( e=K+Size; K<e; )
	{
		char *es=CountS(K,JePismeno);
		if( es>=K+(Analysa ? 6 : 1)  && ( !ZeroT || !*es ) )
		{
			if( NutnoPrekladat(K,es,JeZavisle) )
			{
				const char *OS=NajdiSlovo(&Prekl,K,es+ZeroT,JazZ,JazD);
				if( !OS )
				{
					if( Analysa )
					{
						long NS=NewDyn(&Prekl);
						if( NS>=0 )
						{
							Dvojice *Dvoj=AccDyn(&Prekl,NS);
							Dvoj->Orig=NDuplicate(K,es);
							Dvoj->Prek=NDuplicate(K,es);
							Dvoj->JazykZ=JazZ;
							Dvoj->JazykD=JazD;
							PreklZmena=True;
						}
						else {ChybaRAM();break;}
					}
				}
				else /* slovo nalezeno */
				{
					Nahrad(K,es,OS);
				}
			}
			K=es;
		}
		else K++;
	}
}

static void ConvertAsc( char *Buf, long Len, int JazZ, int JazD, Flag Anal )
{
	ViceStrPreloz(Buf,Len,JazZ,JazD,False,Anal,True);
}

static void ConvertDta( char *Buf, long Len, int JazZ, int JazD, Flag Anal )
{
	ViceStrPreloz(Buf,Len,JazZ,JazD,True,Anal,True);
}

static void ConvertPrg( char *Buf, long Len, int JazZ, int JazD, Flag Anal )
{
	#define HS sizeof(PH)
	char *dbeg=Buf+HS+((PH *)Buf)->ph_tlen;
	if( Len>((PH *)Buf)->ph_dlen ) Len=((PH *)Buf)->ph_dlen;
	if( Len<0 ) Len=0;
	ViceStrPreloz(dbeg,Len,JazZ,JazD,True,Anal,True);
	#undef HS
}

static int UrciIndex( word off )
{
	int i;
	for( i=0; i<NStr; i++ )
	{
		if( off==OldString[i] ) return i;
	}
	Plati( False );
	return 0;
}
static int ZmenOff( word off )
{
	if( off<KonStr ) return off;
	else return off+ZmenaOff;
}

static word StringEnd( const RSHDR *RH )
{
	word r=RH->rsh_rssize;
	word B=RH->rsh_string;
	#define eqm(b) if( r>b && b>=B ) r=b
	if( RH->rsh_nstring ) eqm(RH->rsh_frstr);
	if( RH->rsh_nobs ) eqm(RH->rsh_object);
	if( RH->rsh_nted ) eqm(RH->rsh_tedinfo);
	if( RH->rsh_nib ) eqm(RH->rsh_iconblk);
	if( RH->rsh_nbb ) eqm(RH->rsh_bitblk);
	if( RH->rsh_nbb ) eqm(RH->rsh_imdata);
	if( RH->rsh_nimages ) eqm(RH->rsh_frimg);
	if( RH->rsh_ntree ) eqm(RH->rsh_trindex);
	return r;
}

static char *ConvertRsc( char *Buf, char *CBuf, unsigned long *_Len, int JazZ, int JazD, Flag Anal )
{
	RSHDR *RH=(RSHDR *)Buf;
	RSHDR *RC=(RSHDR *)CBuf;
	char *SE;
	char *BufO=Buf;
	word r;
	long Len=*_Len;
	long ln,lo;
	r=StringEnd(RH);
	SE=Buf+r;
	Buf+=RH->rsh_string;
	lo=ln=SE-Buf;
	if( Anal ) CBuf+=RC->rsh_string;
	SE=SeznamStrPreloz(Buf,CBuf,&ln,JazZ,JazD,Anal,RH->rsh_string);
	if( !Anal )
	{
		ln-=lo;
		if( ln>0x3fff || Len+ln>0xffff ) {ChybaRAM();return BufO;}
		Buf=malloc(Len+ln);
		if( !Buf ) {ChybaRAM();return BufO;}
		ZmenaOff=(int)ln;
		KonStr=(word)(RH->rsh_string+lo);
		#define sm(b) if( (b)>RH->rsh_string ) (b)+=(int)ln; /* posun, co je za retezci */
		/* presun tree-index */
		{
			long *fs;
			int c;
			fs=(void *)(BufO+RH->rsh_trindex);
			for( c=0; c<RH->rsh_ntree; c++ )
			{
				sm(*fs);
				fs++;
			}
			fs=(void *)(BufO+RH->rsh_frstr);
			for( c=0; c<RH->rsh_nstring; c++ )
			{
				*fs=NewString[UrciIndex((word)(*fs))];
				fs++;
			}
			fs=(void *)(BufO+RH->rsh_iconblk);
			for( c=0; c<RH->rsh_nib; c++ )
			{
				sm(*fs);fs++;
				sm(*fs);fs++;
				*fs=NewString[UrciIndex((word)(*fs))];
				fs=(void *)( (char*)fs+sizeof(ICONBLK)-3*sizeof(long) );
				fs++;
			}
			fs=(void *)(BufO+RH->rsh_frimg);
			for( c=0; c<RH->rsh_nimages; c++ )
			{
				sm(*fs);
				fs++;
			}
			{
				/* bitblks */
				BITBLK *ei,*i;
				for( i=(void *)(BufO+RH->rsh_bitblk),ei=i+RH->rsh_nbb; i<ei; i++ )
				{
					i->bi_pdata=(void *)ZmenOff((word)i->bi_pdata);
				}
				/* tedinfos */
			}
			{
				TEDINFO *et,*t;
				for( t=(void *)(BufO+RH->rsh_tedinfo),et=t+RH->rsh_nted; t<et; t++ )
				{
					word tp;
					long ls;
					tp=NewString[UrciIndex((word)t->te_ptext)];
					t->te_ptext=(void *)tp;
					ls=strlen(SE-RH->rsh_string+tp)+1;
					if( t->te_txtlen<ls ) t->te_txtlen=(int)ls;
					tp=NewString[UrciIndex((word)t->te_ptmplt)];
					t->te_ptmplt=(void *)tp;
					ls=strlen(SE-RH->rsh_string+tp)+1;
					if( t->te_tmplen<ls ) t->te_tmplen=(int)ls;
					t->te_pvalid=(void *)NewString[UrciIndex((word)t->te_pvalid)];
				}
			}
			/* jeste projdi skrz cely strom objektu */
			{ /* meni offsety retezcu */
				OBJECT *o,*eo;
				/* ob_spec pointers */
				for( o=(void *)(BufO+RH->rsh_object),eo=o+RH->rsh_nobs; o<eo; o++ )
				{
					switch( o->ob_type )
					{
						case G_TITLE: case G_BUTTON: case G_STRING:
						{
							word s=NewString[UrciIndex((word)o->ob_spec.index)];
							long ls;
							o->ob_spec.index=s;
							ls=strlen(SE-RH->rsh_string+s);
							if( o->ob_type==G_TITLE || o->ob_width<ls ) o->ob_width=(int)ls;
						}
						break;
						case G_FTEXT: case G_FBOXTEXT:
						{ /* tedinfa jsou jeste tam co byly */
							TEDINFO *t=(void *)(BufO+o->ob_spec.index);
							o->ob_spec.index=ZmenOff((word)o->ob_spec.index);
							if( t->te_font==IBM && o->ob_width<t->te_tmplen-1 ) o->ob_width=t->te_tmplen-1;
						}
						break;
						case G_TEXT: case G_BOXTEXT:
						{ /* tedinfa jsou jeste tam co byly */
							TEDINFO *t=(void *)(BufO+o->ob_spec.index);
							o->ob_spec.index=ZmenOff((word)o->ob_spec.index);
							if( t->te_font==IBM && o->ob_width<t->te_txtlen-1 ) o->ob_width=t->te_txtlen-1;
						}
						break;
						case G_IMAGE:
							o->ob_spec.index=ZmenOff((word)o->ob_spec.index);
						break;
						case G_ICON:
							o->ob_spec.index=ZmenOff((word)o->ob_spec.index);
						break;
						default: break;
					}
				}
			}
		}
		/* prekoduj hlavicku */
		sm(RH->rsh_object);
		sm(RH->rsh_tedinfo);
		sm(RH->rsh_iconblk);
		sm(RH->rsh_bitblk);
		sm(RH->rsh_frstr);
		sm(RH->rsh_imdata);
		sm(RH->rsh_frimg);
		sm(RH->rsh_trindex);
		sm(RH->rsh_rssize);
		memcpy(Buf,BufO,RH->rsh_string); /* pred retezcema */
		memcpy(Buf+RH->rsh_string,SE,lo+ln); /* retezce */
		memcpy(Buf+RH->rsh_string+lo+ln,BufO+RH->rsh_string+lo,Len-RH->rsh_string-lo); /* za retezcema */
		*_Len=Len+ln;
		free(SE);
		free(BufO);
		return Buf;
	}
	return BufO;
}

static PathName FileP,ZFileW,DFileW;
static FileName FileN;

typedef char ExtT[5];

static Flag StrCmp( const char *Ext, ExtT *Exts )
{
	ExtT *E;
	for( E=Exts; *E[0]!=0; E++ ) if( !strcmp(*E,Ext) ) return False;
	return True;
}

static void Convert( const char *FZ, const char *FDo, enum typsoub TS, int JazZ, int JazD, Flag Anal )
{ /* FDo==NULL-> jen statistika */
	int FIn;
	DTA Dt,*OD;
	Mouse(HOURGLASS);
	OD=Fgetdta();
	Fsetdta(&Dt);
	if( !Anal && !strcmp(FZ,FDo) ) {AlertRF(NEJISTA,1);return;}
	if( Fsfirst(FZ,1)==0 )
	{
		/* na disketach neoveruj moc dlouhe soubory! */		
		{
			if( (FIn=Fopen(FZ,0))>=0 )
			{
				char *Buf=malloc(Dt.d_length);
				long ld=Fsize(FDo);
				char *DBuf=NULL;
				if( Anal ) DBuf=malloc(ld);
				if( Buf && ( DBuf || !Anal ) )
				{
					if( Fread(FIn,Dt.d_length,Buf)==Dt.d_length )
					{
						int FOut;
						switch( TS )
						{
							case Rsc:
							{
								if( Anal )
								{
									FOut=Fopen(FDo,0);
									if( FOut>=0 )
									{
										long rd=Fread(FOut,ld,DBuf);
										Fclose(FOut);
										if( rd!=ld )
										{
											AlertRF(IERRA,1,SizeNazev(FZ,25));
											break;
										}
									}
								}
								Buf=ConvertRsc(Buf,DBuf,&Dt.d_length,JazZ,JazD,Anal);
							}
							break;
							case Prg: ConvertPrg(Buf,Dt.d_length,JazZ,JazD,Anal);break;
							case Asc: ConvertAsc(Buf,Dt.d_length,JazZ,JazD,Anal);break;
							default: ConvertDta(Buf,Dt.d_length,JazZ,JazD,Anal);break;
						}
						if( !Anal )
						{
							if( (FOut=Fcreate(FDo,0))>=0 )
							{
								if( Fwrite(FOut,Dt.d_length,Buf)!=Dt.d_length )
								{
									AlertRF(WERRA,1,SizeNazev(FZ,25));
								}
								Fclose(FOut);
							}
							else AlertRF(WERRA,1,SizeNazev(FZ,25));
						}
					}
					else AlertRF(IERRA,1,SizeNazev(FZ,25));
				}
				else ChybaRAM();
				Fclose(FIn);
				if( Buf ) free(Buf);
				if( DBuf ) free(DBuf);
			}
			else AlertRF(IERRA,1,SizeNazev(FZ,25));
		}
	}
	else AlertRF(IERRA,1,SizeNazev(FZ,25));
	Fsetdta(OD);
}

void DebugError( int Line, const char *File ) {(void)Line,(void)File;}

static void SetTypTed( TEDINFO *T, enum typsoub TS )
{
	const char *FS;
	OBJECT *PD=RscTree(TYPYPD);
	switch( TS )
	{
		case Asc: FS=PD[TASCI].ob_spec.free_string;break;
		case Rsc: FS=PD[TRSCI].ob_spec.free_string;break;
		case Prg: FS=PD[TPRGI].ob_spec.free_string;break;
		default: FS=PD[TDATAI].ob_spec.free_string;break;
	}
	SetStrTed(T,FS);
}

static enum typsoub TypTed( TEDINFO *T )
{
	enum typsoub TS;
	const char *FS=StrTed(T);
	OBJECT *PD=RscTree(TYPYPD);
	if( !strcmp(FS,PD[TASCI].ob_spec.free_string) ) TS=Asc;
	ef( !strcmp(FS,PD[TRSCI].ob_spec.free_string) ) TS=Rsc;
	ef( !strcmp(FS,PD[TPRGI].ob_spec.free_string) ) TS=Prg;
	else TS=Dta;
	return TS;
}

static Flag Konvert( OBJECT *Bt, Flag Dvoj, int AscK )
{
	OBJECT *F=RscTree(KONVERTD);
	Convert(ZFileW,DFileW,TypTed(TI(F,STYPPD)),toupper(*StrTed(TI(F,KONJAZET))),toupper(*StrTed(TI(F,KONJAZDT))),False);
	Unselect(Bt,Dvoj,AscK);
	ObjcRedraw(F,KONKONVB,BPHandle,NULL);
	(void)Bt,(void)Dvoj,(void)AscK;
	return False;
}
static Flag Analyz( OBJECT *Bt, Flag Dvoj, int AscK )
{
	OBJECT *F=RscTree(KONVERTD);
	Convert(ZFileW,DFileW,TypTed(TI(F,STYPPD)),toupper(*StrTed(TI(F,KONJAZET))),toupper(*StrTed(TI(F,KONJAZDT))),True);
	Unselect(Bt,Dvoj,AscK);
	ObjcRedraw(F,KONANALB,BPHandle,NULL);
	(void)Bt,(void)Dvoj,(void)AscK;
	return False;
}

static enum typsoub UrciTyp( const char *Naz )
{
	const static ExtT Txts[]=
	{
		".ASC",".C",".S",".H",".RSH",".PRJ",".ASM",".LOD",".I",
		".PAS",".HEX",".INF",".TXT",".CPS",".ATV",".MDV",""
	};
	const static ExtT Dats[]=
	{
		".STR",".DEF",".DFN",".DAT",".BIN",
		".LIB",".O",".HRD",".HDR",
		".CFG",".LDW",".BAS",".LD_",
		".1WP",".1ST",".DOC",""
	};
	const static ExtT Prgs[]=
	{
		".PRG",".ACC",".TOS",".TTP",".APP",
		".ACX",".PRX",".GTP",""
	};
	enum typsoub TS=Dta;
	const char *Ext=strrchr(NajdiNazev(Naz),'.');
	if( Ext )
	{
		if( !strcmp(Ext,".RSC") ) TS=Rsc;
		ef( !StrCmp(Ext,Prgs) ) TS=Prg;
		ef( !StrCmp(Ext,Txts) ) TS=Asc;
		ef( !StrCmp(Ext,Dats) ) TS=Dta;
	}
	return TS;
}

static Flag NazSSel( OBJECT *Bt, Flag Dvoj, int AscK )
{
	VyrobCesty(FileP,FileN,ZFileW);
	VsePrekresli();
	if( EFSel("Konvertovat soubor:",FileP,FileN,ZFileW) )
	{
		OBJECT *F=RscTree(KONVERTD);
		enum typsoub TS=UrciTyp(ZFileW);
		Mouse(HOURGLASS);
		VsePrekresli();
		SetStrTed(TI(F,SNAMT),SizeNazev(ZFileW,TI(F,SNAMT)->te_txtlen-1));
		strcpy(DFileW,ZFileW);
		SetStrTed(TI(F,DNAMT),StrTed(TI(F,SNAMT)));
		SetTypTed(TI(F,STYPPD),TS);
		ObjcRedraw(F,SNAMT,BPHandle,NULL);
		ObjcRedraw(F,DNAMT,BPHandle,NULL);
		ObjcRedraw(F,STYPPD,BPHandle,NULL);
	}
	(void)Bt,(void)Dvoj,(void)AscK;
	return False;
}
static Flag NazDSel( OBJECT *Bt, Flag Dvoj, int AscK )
{
	VyrobCesty(FileP,FileN,DFileW);
	VsePrekresli();
	if( EFSel("Do souboru:",FileP,FileN,DFileW) )
	{
		OBJECT *F=RscTree(KONVERTD);
		SetStrTed(TI(F,DNAMT),SizeNazev(DFileW,TI(F,SNAMT)->te_txtlen-1));
		ObjcRedraw(F,DNAMT,BPHandle,NULL);
	}
	(void)Bt,(void)Dvoj,(void)AscK;
	return False;
}
static Flag TypSSel( OBJECT *Bt, Flag Dvoj, int AscK )
{
	OBJECT *F=RscTree(KONVERTD);
	OBJECT *PD=RscTree(TYPYPD);
	int I=(int)(Bt-F);
	int Df;
	WindRect W;
	ObjcRect(F,I,&W);
	switch( TypTed(TI(F,I)) )
	{
		case Asc: Df=TASCI;break;
		case Rsc: Df=TRSCI;break;
		case Prg: Df=TPRGI;break;
		default: Df=TDATAI;break;
	}
	switch( DPopUp(PD,Df-1,&W) )
	{
		case TASCI-1: Df=Asc;break;
		case TRSCI-1: Df=Rsc;break;
		case TPRGI-1: Df=Prg;break;
		case TDATAI-1: Df=Dta;break;
		default: Df=-1;break;
	}
	if( Df>=0 )
	{
		SetTypTed(TI(F,STYPPD),Df);
		ObjcRedraw(F,STYPPD,BPHandle,NULL);
	}
	(void)Bt,(void)Dvoj,(void)AscK;
	return False;
}
static Flag TypDSel( OBJECT *Bt, Flag Dvoj, int AscK )
{
	(void)Bt,(void)Dvoj,(void)AscK;
	return False;
}

static Button KonvTlac[]=
{
	NoButProc,-1,
	Unselect,KONKONB,
	Konvert,KONKONVB,
	Analyz,KONANALB,
	TypSSel,STYPPD,
	NazSSel,SNAMT,
	NazDSel,DNAMT,
	NULL,
};

static void TestD( void )
{
	OBJECT *F=RscTree(KONVERTD);
	enum typsoub TS;
	SetStrTed(TI(F,SNAMT),SizeNazev(ZFileW,TI(F,SNAMT)->te_txtlen-1));
	strcpy(DFileW,ZFileW);
	SetStrTed(TI(F,DNAMT),StrTed(TI(F,SNAMT)));
	TS=UrciTyp(ZFileW);
	SetTypTed(TI(F,STYPPD),TS);
	SetStrTed(TI(F,KONJAZET),"e");
	SetStrTed(TI(F,KONJAZDT),"_");
	EFForm(F,KonvTlac,NULL,KONJAZDT,NULL);
}

static void ZmenCode( void (*CS)( char *S, int N ), int N )
{
	int i;
	for( i=0; i<=TYPYPD; i++ ) DialCode(RscTree(i),CS,N);
	FSCode(NEJISTA,CS,N);
}

void ZmenNormu( int NTab, int OTab )
{
	(void)NTab,(void)OTab;
}

int main( int argc, const char *argv[] )
{
	int ApId;
	if( (ApId=appl_init())>=0 )
	{
		(void)ApId;
		Mouse(HOURGLASS);
		if( rsrc_load("TRANSL.RSC") )
		{
			TestFalcon();
			Dial3D(RscTree(KONVERTD),Falcon);
			GetPath(FileP);
			EndChar(FileP,'\\');
			strcat(FileP,"*.RSC");
			if( ZacJazyky()>=0 )
			{
				if( argc>1 )
				{
					const char *N=argv[1];
					if( N[1]!=':' && N[0]!='\\' ) /* bez cesty */
					{
						GetPath(ZFileW);
						EndChar(ZFileW,'\\');
						strcat(ZFileW,N);
					}
					else strcpy(ZFileW,N);
					argc--,argv++;
					strupr(ZFileW);
				}
				else strcpy(ZFileW,FileP);
				Mouse(ARROW);
				TestD();
				KonJazyky();
			}
			rsrc_free();
		}
		else form_alert(1,"[1][TRANSL.RSC?][   OK   ]");
		appl_exit();
	}
	return 0;
}	

void DoZprava( int M[8] ){(void)M;}
void NejsouOkna( void ){}
void ChybaRAM( void ){AlertRF(RAMEA,1);}
