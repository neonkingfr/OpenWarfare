/* Debugger - pomuci por hledani mista, kde program spadnul */
/* SUMA 5/1994 */
/* vyuzity casti profileru */

#include <tos.h>
#include <linea.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <macros.h>
#include <sttos\sysvar.h>

#include "debint.h"

static char Header[]=
"DEBUG Direct Color Debugger  SUMA, Praha\n"
"(C) 1994                     All Rights Reserved\n"
"Release 3-5-94               Version for ATARI Falcon\n"
"\n";
static char Wait[]=
"Hit any key to continue ...\n";

#define Ost1Nam "other_1"
#define Ost2Nam "other_2"

typedef struct
{
	int branch;
	long tlen,dlen,blen,slen;
	long res1,res2;
	int flag;
} PH;

typedef struct
{
	char name[8];
	int type;
	long value;
} Symbol;

TextS *TSym;
long TSN;

#define OTHER 1

static int ComparAdr( const TextS *T, const TextS *t )
{
	long dif=T->value-t->value;
	if( dif>0 ) return +1;
	if( dif<0 ) return -1;
	return 0;
}

static void Sort( void )
{
	qsort(TSym,TSN-1,sizeof(*TSym),ComparAdr);
}

static void NewSym( Symbol *Sym, long nsym )
{
	const static long P2[]={1,2,4,8,0x10,0x20,0x40,0x80,0x100,0x200,0x400,0x800,0x1000,0x2000,0x4000,};
	long i;
	long nsymo;
	int log2;
	for( i=0,nsymo=0; i<nsym; i++ ) if( Sym[i].type&0x200 ) nsymo++;
	nsymo+=3;
	for( log2=0; log2<lenof(P2); log2++ ) if( P2[log2]>=nsymo ) break;
	TSN=nsymo=P2[log2];
	TSym=malloc(nsymo*sizeof(*TSym));
}

static TextS *ZacSym( Symbol *Sym, long nsym, long Beg, BASPAG *BP )
{
	long i,is;
	long nsymo=TSN;
	if( TSym )
	{
		for( i=0,is=0; i<nsym; i++ )
		{
			Symbol *S=&Sym[i];
			if( S->type&0x200 )
			{
				TextS *TS=&TSym[is++];
				memcpy(TS->name,S->name,8);
				TS->value=S->value+Beg;
				if( !strncmp(TS->name,"exit",8) ) TS->stat=2;
				else TS->stat=0;
			}
		}
		{
			static TextS TSZer={Ost1Nam,0,OTHER};
			static TextS TSEnd={Ost2Nam,0,OTHER};
			static TextS TSROM={"ROM",0xE00000L,0};
			TSEnd.value=(long)BP->p_tbase+BP->p_tlen;
			TSym[is++]=TSZer;
			TSym[is++]=TSEnd;
			TSym[is++]=TSROM;
		}
		{
			long St=(0x1000000L-TSym[is-1].value)/(nsymo-is);
			for( i=is; i<nsymo; i++ )
			{
				TSym[i]=TSym[i-1];
				TSym[i].value+=St;
			}
			TSym[is-1].stat=0;
			is=nsymo;
			TSym[nsymo-1].value=0x1000000L;
		}
		Sort();
	}
	return TSym;
}

#define read(File,Co) ( sizeof(Co)==Fread(FIn,sizeof(Co),&(Co)) )

extern BASPAG *_BasPag;

static void *osd;

byte Font[0x1800];
int BPL;

void LoadFont( void )
{
	#ifdef __TURBOC__
		linea_init();
		memcpy(Font,Fonts->font[1]->fnt_dta,0x800);
		memcpy(&Font[0x800],Fonts->font[2]->fnt_dta,0x1000);
	#else
		linea0();
		memcpy(Font,la_init.li_a1[1]->font_data,0x800);
		memcpy(&Font[0x800],la_init.li_a1[2]->font_data,0x1000);
	#endif
}

void Init( void )
{
	void *SSP=(void *)Super(NULL);
	LoadFont();
	osd=scr_dump;
	scr_dump=MySD;
	Super(SSP);
	BPL=320*2;
}

void Deinit( void )
{
	void *SSP=(void *)Super(NULL);
	scr_dump=osd;
	Super(SSP);
}

int main( int argc, char *argv[] )
{
	int FIn;
	int ret=0;
	PH Head;
	char *PrgNam;
	int argp=1;
	if( argc<=1 )
	{
		Usage:
		printf(Header);
		printf(Wait);bios(2,2);
		return -1;
	}
	if( argp>=argc ) goto Usage;
	PrgNam=argv[argp++];
	
	FIn=Fopen(PrgNam,0);
	if( FIn>=0 )
	{
		Symbol *Sym;
		static char Env[]={0,0};
		static COMMAND Com={0,""};
		BASPAG *BP;
		long Err;
		if( !read(FIn,Head) ) goto Error;
		Fseek(Head.tlen+Head.dlen,FIn,1);
		Sym=malloc(Head.slen);
		if( Sym )
		{
			long NSym;
			if( Fread(FIn,Head.slen,Sym)!=Head.slen ) goto Error2;
			NSym=Head.slen/sizeof(*Sym);
			Fclose(FIn);
			NewSym(Sym,NSym);
			if( !TSym ) ret=-39;
			else
			{
				BP=(BASPAG *)Pexec(3,PrgNam,&Com,Env);
				Err=(long)BP;
				if( Err<0 ) {free(Sym);return (int)Err;}
				ZacSym(Sym,NSym,(long)BP->p_tbase,BP);
				if( TSym )
				{
					Init();
					Pexec(4,NULL,(COMMAND *)BP,NULL);
					Deinit();
				}
				else ret=-39;
			}
			free(Sym);
			return ret;
			Error2:
			free(Sym);
		}
		else ret=-39;
		Error:
		Fclose(FIn);
		return ret;
	}
	return FIn;
}
