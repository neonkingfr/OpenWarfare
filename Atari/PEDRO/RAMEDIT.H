/* spole�n� deklarace IDE Pedro */
/* SUMA 1/1995 */

#define APP_NAME "PEDRO"
#define RSC_H "PEDRO.H"

#define SDOkno 0x100

#ifndef __UTLEDIT
#include "utledit.h"
#endif

typedef struct
{
	int LHan;
	int Handle; /* VDI workstation */
	TextBuf *Text; /* s ��m pracujeme */
	TextKursor Zac; /* za��tek okna */
	Flag JeBlok;
	Flag BylBlok; /* hnuli jsme kursorem a zru�ili blok - mus�� p�ekreslovat */
	TextKursor ZBlok,KBlok; /* vymezen� bloku */
	TextKursor Kurs; /* kursor */
	ClipRect KRect; /* ��ra kursoru */
	ClipRect ZBRect,KBRect; /* kde je za��tek, a kde konec bloku */
	long OZBPos,OKBPos; /* jak je nakreslen blok? */
	Flag ZmenaObrazu; /* n�kdo v sousedn�m okn� zapracoval */
	long PosZac,PosKon; /* oblast zobrazen� oknem */
	long RadZac,RadKon; /* oblast zobrazen� oknem - ��sla ��dk� */
	int NLines; /* spo��t�me ��dky */
	int HSPos,HSVel; /* odlo�en� ovl�d�n� */
	int VSPos,VSVel; /* odlo�en� ovl�d�n� */
	int TabSize;
	Flag AutoIndent;
	int XOffset; /* posun H-sliderem */
	int XWidth; /* nejdel� ��dek - zobrazen� ��st */
	int XWidthMax; /* nejdel� ��dek */
	Flag VNazvuZmena;
	int FKlav; /* ��slo F .. kl�vesy */
} OknoInfo;

/* OKN */

#ifdef __LINEA__
	extern FONT_HDR *FastFont; /* pro vlastn� rutiny */
#endif
extern Flag MonoSpaced; /* v�echn znaky maj� stejn� rozm�r */
extern int MonoX,MonoY; /* jak� je ten rozm�r? */

extern PathName TextP,TextW;
extern FileName TextN;

extern PathName ProjectFile;

#ifdef __MULTI
	extern WindRect ObrysyTextu[NMaxOken];
	extern int MaxTextu;
#endif

void LAutoRozmer( WindRect *D, int LHan, int AsHan );
Flag OknoJeText( int LHan );
int VrchniText( void );
int NajdiText( const char *Cesta );
int PocetOken( void );
void Kresli( int LHan );
void KresliBlok( int LHan );
Flag ChciKursorZB( int LHan );
Flag ChciKursor( int LHan, const char *InsS, long InsL, Flag Blok );
Flag PosunKursor( int LHan, Flag Hor, Flag Dol, Flag Neskroluj );
void KresliKursorPos( int LHan, Flag Hor, Flag Dol );
void KresliKursor( int LHan );
void KresliZBlok( int LHan );

void OtevriOkno( Flag Novy, Flag Ask );
Flag OtevriText( TextBuf *T, const WindRect *Obr );

void DalsiChyba( int LHan );

const char *TvorNazev( const TextBuf *S, int LHan, Flag Zmena );
void NastavNazvy( TextBuf *T );

void ConfigOken( void );

Flag ZnakSwitchO( int Scan, Flag Buf ); /* chceme okno F.. */
int NajdiFText( int FN );
int MaskaFKlav( void ); /* jak� F jsou obsazena */

/* GRF */

#ifdef __LINEA__
void ZacText( FONT_HDR *Font );
#endif
void KonText( void );

/* x ud�van� v bytech */

void PrintText( int x, int y, int w, const char *S, Flag Invert );
void InvertRect( int x, int y, int w, int h );
void InvertKursor( int x, int y, int h );

void ClearRect( int x, int y, int w, int h, Flag Invert );
void ClearRHalf( int x, int y, int h, Flag Invert );
void ClearLHalf( int x, int y, int h, Flag Invert );

/* BLK */

Flag ZacBlok( int LHan ); /* vyznaci zac. resp. konec na posici kursoru. */
Flag KonBlok( int LHan );
void ProzatimZacBlok( int LHan );
void ProzatimKonBlok( int LHan ); /* zmena konce behem vyznacovani */
void ZrusBlok( int LHan );

Flag HideSelect( int LHan );
Flag SelectAll( int LHan );
Flag SelectLine( int LHan );
Flag SelectWord( int LHan );

Err CutBlok( int LHan );
Err CopyBlok( int LHan );
Err PasteBlok( int LHan );
Err DeleteBlok( int LHan );

Flag LzePaste( void );

Flag IndentBlok( int LHan );
Flag UnindentBlok( int LHan );

/* ENV */

void ReadEnv( const char *FName ); /* nastav "Env" */
const char *GetEnv( const char *Var ); /* hodnoty prom�nn�ch */
const char *Env( void ); /* pro vol�n� program� */

int AddEnv( const char *Var, const char *Val );
void StripEnv( const char *Var );

/* RED */

TextBuf *LoadMessages( const char *FName );

void NormalDirect( const char *In, const char *Out, const char *Err );
int Redirect( const char *In, const char *Out, const char *Err );
Err CallMake( const char *MakeFile, const char *Target );
Err CallDebug( const char *MakeFile, const char *Format, Flag Exec );

void ZacRedir( void );
void KonRedir( void );

/* SHL */

void ZacShell( void );
void KonShell( void );

/* FND */

enum {FICase=1,FWords=2,FBack=4,FAll=8};

Flag Search( TextKursor *s, const char *Str, int Flags );

/* MNU */

void ZacSearch( const char *SS, int SF, const char *RS, const char *RR, int RF );
void ZacMenu( void );

long OIZacBlok( OknoInfo *OI );
long OIKonBlok( OknoInfo *OI );

TextBuf *LoadFile( const char *TextW );
Flag SaveFile( TextBuf *T );
Flag ESaveFile( TextBuf *T ); /* je-li t�eba, p�e�te environment ... */

void ProjectChanged( void ); /* nastav� cesty podle projektu */

Flag ZavriOknaTextu( TextBuf *T, int KromeHan );

void CutOkno( int LHan );
void PasteOkno( int LHan );
void OknoCutRadek( int LHan );

void RevertOkno( int LHan );

/* RAM */

extern int ArgC;
extern const char **ArgV;
extern const char **EnvP;

extern PathName PedroPWD;
extern PathName EnvPath;
extern PathName FormPath;

void SetSearchInfo( const char *SS, int SF );
void SetReplaceInfo( const char *RS, const char *RR, int RF );
void SetProjectInfo( const char *P );
void SetEnvInfo( const char *P );

void SetOknoInfo( const WindRect *W, const char *N, int Han );

void GetConfig( void );
void UlozConfig( void );

void WChybaA( const char *F ); /* pro pot�eby ne-GEM modul� */
void RChybaA( const char *F );
void ChybaRAM( void );
