#include "sndWaveOut.h"
#include <windows.h>
#include <mmsystem.h>
#include <assert.h>

#pragma comment(lib,"winmm.lib")

extern HINSTANCE g_hInstance;
extern BOOL AppActive;

DWORD SoundThreadId;
DWORD SoundFreq;
HANDLE SoundThreadHandle;
HANDLE SoundThreadTerminate;
SoundCallbackFunction *SoundCallback;

HANDLE notifyEvent;
/**
thread processing audio buffers
*/
DWORD WINAPI SoundThread(void *context)
{
  /*HANDLE */notifyEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  MMRESULT mm;
  HWAVEOUT device;
  WAVEFORMATEX format;
  memset(&format, 0, sizeof(format));

  format.wFormatTag = WAVE_FORMAT_PCM;
  format.nChannels = 2;
  format.nSamplesPerSec = SoundFreq;
  format.wBitsPerSample = 16;
  format.nBlockAlign = format.wBitsPerSample/8*format.nChannels;
  format.nAvgBytesPerSec = format.nBlockAlign*format.nSamplesPerSec;
  format.cbSize = 0;
  // format.pExtraBytes;

  waveOutOpen(&device, WAVE_MAPPER, &format, (DWORD_PTR)notifyEvent, 0, CALLBACK_EVENT);

  // call the callback to initiate the sound
  const int buffers = 4;
  char buffer[buffers][4*1024]; // ~ 100 ms of data. 1 sec is ~ 176 KB

  WAVEHDR hdrs[buffers];

  int buffersInProgress = 0;

  for (int i=0; i<buffers; i++)
  {
    WAVEHDR &hdr = hdrs[i];
    memset(&hdr, 0 , sizeof(hdr));
    hdr.lpData = buffer[i];
    hdr.dwBufferLength = SoundCallback(buffer[i],sizeof(buffer[i]),format.nSamplesPerSec);
    hdr.dwLoops = 1;
    hdr.dwUser = i;
    mm  = waveOutPrepareHeader(device, &hdr, sizeof(hdr));

    mm = waveOutWrite(device, &hdr, sizeof(hdr));

    buffersInProgress++;
  }

  int bufferDone = 0;
  bool finish = false;
  for (;;)
  {
    HANDLE handles[2] = {SoundThreadTerminate, notifyEvent};
    DWORD res = WaitForMultipleObjects(2,handles,FALSE,INFINITE);
    if (res==WAIT_OBJECT_0+1)
    {
      // when we are processing slow, one event may signalize multiple buffers
      while (hdrs[bufferDone].dwFlags&WHDR_DONE)
      {
        WAVEHDR &hdr = hdrs[bufferDone];
        assert(hdr.dwUser==bufferDone);
        // finish the buffer processing
        waveOutUnprepareHeader(device, &hdr, sizeof(hdr));

        if (!finish)
        {
          // prepare a new buffer
          memset(&hdr, 0 , sizeof(hdr));
          hdr.lpData = buffer[bufferDone];
          hdr.dwUser = bufferDone;
          hdr.dwBufferLength = SoundCallback(buffer[bufferDone],sizeof(buffer[bufferDone]),format.nSamplesPerSec);
          hdr.dwLoops = 1;
          if (!AppActive)
          {
            // force silence when running in the background
            memset(buffer[bufferDone],0,hdr.dwBufferLength);
          }
          mm  = waveOutPrepareHeader(device, &hdr, sizeof(hdr));

          // submit the new buffer
          mm = waveOutWrite(device, &hdr, sizeof(hdr));
        }
        else
        {
          if (--buffersInProgress<=0)
            goto Break;
        }

        bufferDone = (bufferDone+1)%buffers;
      }
    }
    else
    {
      // we need to wait until all buffers finish playing (or we could perhaps stop them forcefully)
      finish = true;
    }
  }
  Break:
  waveOutClose(device);

  CloseHandle(notifyEvent);

  // flush all unprocessed messages
  SoundCallback(NULL,0,format.nSamplesPerSec);
  return 0;
}

void StartSoundWaveOut(int freq, SoundCallbackFunction *callback)
{
  SoundFreq = freq;
  SoundCallback = callback;
  SoundThreadHandle = CreateThread(NULL,128*1024,SoundThread,NULL,CREATE_SUSPENDED,&SoundThreadId);
  ResumeThread(SoundThreadHandle);
  SoundThreadTerminate = CreateEvent(NULL,FALSE,FALSE,NULL);
}

void EndSoundWaveOut()
{
  SetEvent(SoundThreadTerminate);
  WaitForSingleObject(SoundThreadHandle,INFINITE);
  CloseHandle(SoundThreadHandle),SoundThreadHandle = NULL;
  CloseHandle(SoundThreadTerminate), SoundThreadTerminate = NULL;
}
