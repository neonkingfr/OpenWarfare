/* Gravon- akcni 3D hra */
/* SUMA 9/1994-2/1995 */

#include <windows.h>
#include <functional>

//#include "resource.h"

#include <float.h>
#include <math.h>
#include <io.h>
#include <assert.h>
#include "sndWaveOut.h"

#include "../GiG/gig.h"
#include <mmsystem.h>
#include <XInput.h>

HINSTANCE g_hInstance;
DEVMODE g_oldDevMode;
DEVMODE g_devModeBest;
bool g_fullscreen;
bool g_active;
int g_FullscreenW;
int g_FullscreenH;
HWND  g_hWnd = NULL;
static HANDLE renderEvent;
static HANDLE renderDone;
static HANDLE terminating;
DWORD WinW;
DWORD WinH;

BOOL AppActive;
static BOOL screenSaverEnabledBackup;

#define GIG_DECL_FUNC(ret,name,args) ret __stdcall name args;
#define GIG_DEFINE_FUNC_PTR(ret,name,args) ret (__stdcall *name) args;

GIG_HOST(GIG_DECL_FUNC)

GIG_GAME(GIG_DEFINE_FUNC_PTR)

template <class Type>
inline void GetFunctionAddress(Type *&res, HMODULE mod, const char *name)
{
  res = (Type *)GetProcAddress(mod,name);
};

void GIG_Bind(const char *name)
{
  // load the dll and bind to it
  HMODULE lib = LoadLibrary(name);
  GetFunctionAddress(init_gig,lib,"_init_gig@12");
  GetFunctionAddress(name_gig,lib,"_name_gig@8");
  GetFunctionAddress(shutDown_gig,lib,"_shutDown_gig@0");
  GetFunctionAddress(onKeyDown_gig,lib,"_onKeyDown_gig@4");
  GetFunctionAddress(onKeyUp_gig,lib,"_onKeyUp_gig@4");
  GetFunctionAddress(sound_gig,lib,"_sound_gig@12");
}

static void SelectFullscreenMode()
{

  //
  // Enumerate Device modes...
  //

  int nMode = 0;
  float bestDist = FLT_MAX;
  DEVMODE devMode;
  BOOL bDesiredDevModeFound = FALSE;
  unsigned bitsPerPixelWanted = 32;
  unsigned bitsPerPixelMin = 16;
  unsigned bitsPerPixelMax = 32;
  int refreshWanted = 75;
  float screenAspect;

  // Cache the current display mode so we can switch back when done.
  EnumDisplaySettings( NULL, ENUM_CURRENT_SETTINGS, &g_oldDevMode );

  // assume desktop is set to a mode which tells us about a screen aspect
  screenAspect = (float)g_oldDevMode.dmPelsWidth/g_oldDevMode.dmPelsHeight;

  // find a mode with the same aspect, at least 640x480
  // prefer the desktop resolution

  while( EnumDisplaySettings( NULL, nMode++, &devMode ) )
  {
    float dist = 0;

    // Does this device mode support a 640 x 480 setting?
    if( devMode.dmPelsWidth  < 640 || devMode.dmPelsHeight < 480)
      continue;

//     if (devMode.dmPelsWidth>=1900)
//       continue;

    // avoid modes too large
    //dist += (devMode.dmPelsWidth*devMode.dmPelsHeight-640*480)*0.001f;

    // prefer current resolution
    dist += fabs(float((int)(devMode.dmPelsWidth*devMode.dmPelsHeight-g_oldDevMode.dmPelsWidth*g_oldDevMode.dmPelsHeight)))*0.001f;

    // Does this device mode support 32-bit color?
    if( devMode.dmBitsPerPel < bitsPerPixelMin || devMode.dmBitsPerPel >bitsPerPixelMax)
    {
      continue;
    }
    // prefer keeping the aspect
    dist += (float)fabs(screenAspect-(float)devMode.dmPelsWidth/devMode.dmPelsHeight)*1000;
    dist += 10*(float)fabs((float)(int)(devMode.dmBitsPerPel - bitsPerPixelWanted));

    dist += (float)fabs((float)(int)(devMode.dmDisplayFrequency - g_oldDevMode.dmDisplayFrequency));

    if (dist<bestDist)
    {
      g_devModeBest = devMode;
      bestDist = dist;
    }
  }

  // We found a match, but can it be set without rebooting?
  if( ChangeDisplaySettings( &g_devModeBest, CDS_TEST ) == DISP_CHANGE_SUCCESSFUL )
  {
    g_fullscreen = true;
    g_FullscreenW = g_devModeBest.dmPelsWidth;
    g_FullscreenH = g_devModeBest.dmPelsHeight;
  }

}

void setScreenMode()
{
  if (g_fullscreen)
  {
    ChangeDisplaySettings( &g_devModeBest, CDS_FULLSCREEN );
  }
}

void restoreScreenMode()
{
  if (g_fullscreen)
  {
    ChangeDisplaySettings( &g_oldDevMode, CDS_FULLSCREEN );
  }
}

void shutDown( void )	
{
  SetEvent(terminating); // TODO: more robust solution
  if (g_fullscreen)
  {
    ChangeDisplaySettings( &g_oldDevMode, CDS_FULLSCREEN );
  }

  if (shutDown_gig) shutDown_gig();
  if (renderEvent) CloseHandle(renderEvent), renderEvent = NULL;
  if (renderDone) CloseHandle(renderDone), renderDone = NULL;
  if (terminating) CloseHandle(terminating), terminating = NULL;
}

size_t SoundCallbackFunc(void *buffer, size_t bufferSize, int bufferFreq)
{
  return sound_gig(buffer,bufferSize,bufferFreq);
};


HBITMAP m_hBitmap;
HANDLE gigTerminated;
static HWND g_hostWindow;

static bool GameInit(std::function<bool(const char *appName)> createWindow)
{

  renderDone = CreateEvent(NULL, FALSE, FALSE, NULL);
  renderEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
  terminating = CreateEvent(NULL, TRUE, FALSE, NULL);

  if (g_fullscreen)
  {
    ChangeDisplaySettings( &g_devModeBest, CDS_FULLSCREEN );
  }

  static const char *paths[] = {"GameInGame",".",NULL};

  for (const char **path=paths; *path; path++)
  {
    char fullPath[1024];
    char fullMask[1024];
    strcpy(fullMask,*path);
    strcat(fullMask,"\\*.dll");
    strcpy(fullPath,*path);
    strcat(fullPath,"\\");

    _finddata_t find;
    intptr_t hFile = _findfirst(fullMask,&find);
    if (hFile>=0)
    {
      strcat(fullPath,find.name);
      _findclose(hFile);
    }
    else
    {
      continue;
    }

    GIG_Bind(fullPath);
    if (init_gig)
    {
      char name[64] = "Game In Game";
      if (name_gig)
      {
        name_gig(name,sizeof(name));
      }
      if (!createWindow(name)) return false;

      if (sound_gig) StartSoundWaveOut(44100,SoundCallbackFunc);

      GiG_Interface gig;
      gig.version = GIG_VERSION;
      gig.mouseState_gig = mouseState_gig;
      gig.present_gig = present_gig;
      gigTerminated = init_gig(g_hWnd,&gig,*path);
      if (gigTerminated)
      {
        return true;
      }
      if (shutDown_gig) shutDown_gig();
      if (sound_gig) EndSoundWaveOut();
    }
  }

  return false;
}

/// multi threaded synchronization functionality
class CriticalSection
{
private:
  bool _initialized;
  mutable CRITICAL_SECTION _handle;
  
public:
  /// create initialized object
  CriticalSection() {InitializeCriticalSection(&_handle);}
  /// create initialized object
  explicit CriticalSection(DWORD spinCount) {InitializeCriticalSectionAndSpinCount(&_handle, spinCount);}
  ~CriticalSection() {DeleteCriticalSection(&_handle);}

  bool Lock() const
  {
    EnterCriticalSection(&_handle);
    return true;
  }
  void Unlock() const
  {
    LeaveCriticalSection(&_handle);
  }

private:
  void operator = (const CriticalSection &src);
  CriticalSection(const CriticalSection &src);
};

struct ScreenDesc
{
  unsigned long *shotObr;
  int scrWidth;
  int scrHeight;
  int scrBpp;
  unsigned int *shotPalette;
};

CriticalSection ScreenDescCS_Gig;
ScreenDesc ScreenDesc_Gig;

static int limitFrameMs = 20;

static DWORD lastFrameTime;

static void __stdcall present_gig( unsigned long *shotObr, int scrWidth, int scrHeight, int scrBpp, unsigned int *shotPalette )
{
  DWORD now = timeGetTime();
  int delta = now-lastFrameTime;
  if (delta<limitFrameMs)
  {
    Sleep(limitFrameMs-delta);
  }
  lastFrameTime = now;
  ScreenDescCS_Gig.Lock();
  ScreenDesc_Gig.shotObr = shotObr;
  ScreenDesc_Gig.scrWidth = scrWidth;
  ScreenDesc_Gig.scrHeight = scrHeight;
  ScreenDesc_Gig.scrBpp = scrBpp;
  ScreenDesc_Gig.shotPalette = shotPalette;

  SetEvent(renderEvent);
  // wait until the event is reset (the shot was done)
  HANDLE handles[]={renderDone,terminating};
  WaitForMultipleObjects(sizeof(handles)/sizeof(*handles),handles,FALSE,INFINITE);
  ScreenDescCS_Gig.Unlock();
}

static byte Swap[640*480*3]; // 640*480 should be enough for any mode
static int SwapWidth;
static int SwapHeight;
static int SwapBpp;

static void DoRenderSwap(HWND hwnd)
{
  if (SwapWidth<=0 || SwapHeight<=0) return;

  HDC dc = GetDC(hwnd);

  BITMAPINFO swapBMI;
  memset(&swapBMI, 0, sizeof(swapBMI));
  swapBMI.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
  swapBMI.bmiHeader.biWidth = SwapWidth;
  swapBMI.bmiHeader.biHeight = SwapHeight;
  swapBMI.bmiHeader.biPlanes = 1;
  swapBMI.bmiHeader.biBitCount = 24;
  swapBMI.bmiHeader.biCompression = BI_RGB;
  swapBMI.bmiHeader.biSizeImage = swapBMI.bmiHeader.biWidth * swapBMI.bmiHeader.biHeight * 3; 
  // place the image on the screen
  // assume 1:1 pixels
  float SwapAspect = float(SwapWidth)/SwapHeight;


  unsigned dstH = unsigned(WinW/SwapAspect+0.5f);
  unsigned dstW = unsigned(WinH*SwapAspect+0.5f);
  if (dstW>WinW)
  { // dstW too large, use dstH (borders up and down)
    //SetStretchBltMode( dc, HALFTONE );
    StretchDIBits(dc,0,(WinH-dstH)/2,WinW,dstH,0,0,SwapWidth,SwapHeight,Swap,&swapBMI,DIB_RGB_COLORS,SRCCOPY);
  }
  else
  { // dstW fits, borders left and right
    //SetStretchBltMode( dc, HALFTONE );
    StretchDIBits(dc,(WinW-dstW)/2,0,dstW,WinH,0,0,SwapWidth,SwapHeight,Swap,&swapBMI,DIB_RGB_COLORS,SRCCOPY);
  }
  DeleteDC(dc);
}

static int GIG_MysX,GIG_MysY,GIG_MB,GIG_MysW;

static int XInputX, XInputY, XInputMB, XInputW;

static struct XInputDll
{
  HMODULE hmod_;

  DWORD (WINAPI *getStateFunc_)( DWORD dwUserIndex, XINPUT_STATE* pState);
   
  XInputDll()
  {
    // first attempt to load 1.3 version
    hmod_ = LoadLibrary("XInput1_3.dll");
    // when not available, try to fallback to legacy 1.0 - http://msdn.microsoft.com/en-us/library/windows/desktop/hh405051(v=vs.85).aspx
    if (!hmod_) hmod_ = LoadLibrary("XInput9_1_0.dll");

    GetFunctionAddress(getStateFunc_,hmod_,"XInputGetState");
  }

  DWORD GetState(DWORD dwUserIndex, XINPUT_STATE* pState)
  {
    if (!getStateFunc_) return ERROR_DEVICE_NOT_CONNECTED;
    return getStateFunc_(dwUserIndex,pState);
  }
} XInputDll;

static float PadSensitivity(SHORT x, float sens)
{
  const int maxVal = 32767;
  float x01 = x*(1.0f/maxVal);
  if (x01>0)
  {
    return pow(x01,sens)*maxVal;
  }
  else
  {
    return -pow(-x01,sens)*maxVal;
  }
}
void UpdateXInput()
{
  static XINPUT_STATE lastState[4];
  static DWORD lastWTime[4];
  static bool lastWActive[4];
  DWORD now = timeGetTime();
  for (DWORD i=0; i< 4; i++ )
  {
    XINPUT_STATE state;
    XINPUT_STATE &old = lastState[i];
    ZeroMemory( &state, sizeof(XINPUT_STATE) );

    // Simply get the state of the controller from XInput.
    DWORD dwResult = XInputDll.GetState( i, &state );

    if( dwResult == ERROR_SUCCESS )
    { 
      // Controller is connected 
      static float xScale = 0.015f, yScale = -0.005f, wScale = +0.0001f;
      XInputMB = 0;
      if (state.Gamepad.bRightTrigger>128) XInputMB |= 1;
      if (state.Gamepad.wButtons&XINPUT_GAMEPAD_A) XInputMB |= 1;
      if (state.Gamepad.wButtons&XINPUT_GAMEPAD_RIGHT_SHOULDER) XInputMB |= 1;

      if (state.Gamepad.bLeftTrigger>128) XInputMB |= 2;
      if (state.Gamepad.wButtons&XINPUT_GAMEPAD_B) XInputMB |= 2;
      if (state.Gamepad.wButtons&XINPUT_GAMEPAD_LEFT_SHOULDER) XInputMB |= 2;

      int newButtons = state.Gamepad.wButtons & ~ old.Gamepad.wButtons;

      if (newButtons&XINPUT_GAMEPAD_DPAD_UP) XInputW += 1;
      if (newButtons&XINPUT_GAMEPAD_DPAD_DOWN) XInputW -= 1;

      static float xSens = 3.0f, ySens = 3.0f;
      XInputX = int(PadSensitivity(state.Gamepad.sThumbRX,xSens)*xScale);
      XInputY = int(PadSensitivity(state.Gamepad.sThumbRY,ySens)*yScale);


      int wState = int(state.Gamepad.sThumbLY*wScale);
      if (wState>+1) wState = +1;
      if (wState<-1) wState = -1;
      if (wState!=0)
      {
        if (!lastWActive[i] || now-lastWTime[i]>1000)
        {
          XInputW += wState;
          lastWTime[i] = now;
        }
      }
      lastWActive[i] = wState!=0;

    }
    old = state;
  }
}



static void __stdcall mouseState_gig(int *x, int *y, int *buttons, int *wheel)
{
  UpdateXInput();
  *x = GIG_MysX + XInputX;
  *y = GIG_MysY + XInputY;
  *buttons= GIG_MB | XInputMB;
  if (wheel)
  {
    *wheel = InterlockedExchange((long *)&GIG_MysW,0);
    *wheel += InterlockedExchange((long *)&XInputW,0);
  }
}

static void AppActivate(bool active)
{
  g_active = active;
}
LRESULT CALLBACK WindowProc( HWND   hWnd,  UINT   msg,  WPARAM wParam, LPARAM lParam )
{
  switch( msg )
  {
  case WM_ACTIVATEAPP:
    if (g_fullscreen)
    {
      AppActivate(wParam!=0);
    }
    break;
  case WM_KEYDOWN:
    if (onKeyDown_gig) onKeyDown_gig(wParam);
    break;
  case WM_KEYUP:
    if (onKeyUp_gig) onKeyUp_gig(wParam);
    break;
  case WM_MOUSEMOVE:
    // TODO: clip mouse in the allowable control area
    GIG_MysX = (int)(short)lParam;
    GIG_MysY = (int)(short)(lParam>>16);
    break;
  case WM_LBUTTONDOWN: GIG_MB |= 1; break;
  case WM_RBUTTONDOWN: GIG_MB |= 2; break;
  case WM_LBUTTONUP: GIG_MB &= ~1; break;
  case WM_RBUTTONUP:GIG_MB &= ~2; break;
  case WM_MOUSEWHEEL: GIG_MysW += GET_WHEEL_DELTA_WPARAM(wParam)/WHEEL_DELTA;
//   case WM_ERASEBKGND:
//     return 0;
  case WM_PAINT:
    //if (!g_fullscreen)
    {
      DoRenderSwap(hWnd);
      //SwapBuffers(g_hDC);
    }
    return 0;
  case WM_SETCURSOR:
    SetCursor(NULL);
    break;
  case WM_ACTIVATE:
    {
      BOOL active = LOWORD(wParam)!=WA_INACTIVE;
      BOOL minimized = HIWORD(wParam)!=0;

      if (active && !minimized)
      {	
        AppActive = TRUE;
        SystemParametersInfo(SPI_GETSCREENSAVEACTIVE, 0, &screenSaverEnabledBackup, 0);
        SystemParametersInfo(SPI_SETSCREENSAVEACTIVE, FALSE, 0, 0);
        if (g_fullscreen)
        {
          setScreenMode();
          SetForegroundWindow( hWnd );				
          ShowWindow(hWnd, SW_RESTORE );
        }
      }			
      else			
      {
        AppActive = FALSE;				
        if (g_fullscreen)
        {
          ShowWindow(hWnd,SW_MINIMIZE);				
          restoreScreenMode();
        }
        if (g_fullscreen)
        {
          if (onKeyDown_gig) onKeyDown_gig(VK_CUSTOM_PAUSE);
        }
        SystemParametersInfo(SPI_SETSCREENSAVEACTIVE, screenSaverEnabledBackup, 0, 0);
      }		
    }

    return DefWindowProc( hWnd, msg, wParam, lParam );
  case WM_SIZE:
    if (!g_fullscreen)
    {
      WinW = LOWORD(lParam);
      WinH = HIWORD(lParam);
    }
    break;

  case WM_CLOSE:
    PostMessage(hWnd,WM_DESTROY,0,0);
    return TRUE;
  case WM_DESTROY:
    shutDown();
    PostQuitMessage(0);
    g_hWnd = NULL;
    break;

  default:
    return DefWindowProc( hWnd, msg, wParam, lParam );
  }

  return 0;
}

#define COL_R(color) ( (int)((color>>0)&0xff) )
#define COL_G(color) ( (int)((color>>8)&0xff) )
#define COL_B(color) ( (int)((color>>16)&0xff) )
#define COL_A(color) ( (int)((color>>24)&0xff) )


static void Bilint(byte *tgt, int tgtLine, const unsigned long *src, int srcW, int srcH, int x, int y)
{
  int x1 = x+1<srcW ? x+1: srcW-1;
  int y1 = y+1<srcH ? y+1: srcH-1;
  int color00 = src[srcW*y+x];
  int color01 = src[srcW*y1+x];
  int color10 = src[srcW*y+x1];
  int color11 = src[srcW*y1+x1];

  int r00 = COL_R(color00), g00 = COL_G(color00), b00 = COL_B(color00);
  int r01 = COL_R(color01), g01 = COL_G(color01), b01 = COL_B(color01);
  int r10 = COL_R(color10), g10 = COL_G(color10), b10 = COL_B(color10);
  int r11 = COL_R(color11), g11 = COL_G(color11), b11 = COL_B(color11);

  tgt[0] = (r01+r00)/2, tgt[1] = (g01+g00)/2, tgt[2] = (b01+b00)/2;
  tgt[3] = (r11+r00+r10+r01)/4, tgt[3+1] = (g11+g00+g10+g01)/4, tgt[3+2] = (b11+b00+b10+b01)/4;
  tgt[tgtLine*3] = r00, tgt[tgtLine*3+1] = g00, tgt[tgtLine*3+2] = b00;
  tgt[tgtLine*3+3] = (r10+r00)/2, tgt[tgtLine*3+3+1] = (g10+g00)/2, tgt[tgtLine*3+3+2] = (b10+b00)/2;
}


void BilintImage(byte *tgt, const unsigned long *scr, int scrWidth, int scrHeight, bool invert)
{
  for(int y=0; y<scrHeight; y++)
  {
    int baseY = (invert ? scrHeight-1-y : y)*2*scrWidth*2;
    for(int x=0; x<scrWidth; x++)
    {
      int base = (baseY+x*2)*3;
      Bilint(tgt+base,scrWidth*2,scr,scrWidth,scrHeight,x,y);
    }
  }
}

void DoRender(HWND hwnd, ScreenDesc &desc)
{
  // copy from VRam to framebuffer, adjust format as necessary, manipulate the palette
  // swap top-down

  {
    if (desc.scrBpp!=8)
    {
      if (desc.scrWidth==640/2 && desc.scrHeight==480/2)
      {
        BilintImage(Swap,desc.shotObr,desc.scrWidth,desc.scrHeight,true);
      }
      else if (desc.scrWidth==640 && desc.scrHeight==480)
      {
        for(int y=0; y<desc.scrHeight; y++)
        for(int x=0; x<desc.scrWidth; x++)
        {
          int base = ((desc.scrHeight-1-y)*640+x)*3;

          int c = desc.shotObr[desc.scrWidth*y+x];
          Swap[base+0] = c>>16, Swap[base+1] = c>>8, Swap[base+2] = c>>0;
        }
      }
      else
      {
        assert(false); // general rescale not supported yet
      }

      SwapWidth = 640;
      SwapHeight = 480;
      SwapBpp = 24;
    }
    else
    {
      if (desc.scrWidth<=320 && desc.scrHeight<=240)
      {
        static unsigned long temp[320*240];
        for(int y=0; y<desc.scrHeight; y++)
        for(int x=0; x<desc.scrWidth; x++)
        {
          int base = y*desc.scrWidth+x;

          int i = ((unsigned char *)desc.shotObr)[desc.scrWidth*y+x];
          int c = desc.shotPalette[i];
          temp[base] = c;
        }
        BilintImage(Swap,temp,desc.scrWidth,desc.scrHeight,true);
        SwapWidth = desc.scrWidth*2;
        SwapHeight = desc.scrHeight*2;
      }
      else
      {
        for(int y=0; y<desc.scrHeight; y++)
        for(int x=0; x<desc.scrWidth; x++)
        {
          int base = ((desc.scrHeight-1-y)*desc.scrWidth+x)*3;

          int i = ((unsigned char *)desc.shotObr)[desc.scrWidth*y+x];
          int c = desc.shotPalette[i];
          Swap[base+0] = c>>0, Swap[base+1] = c>>8, Swap[base+2] = c>>16;
        }
        SwapWidth = desc.scrWidth;
        SwapHeight = desc.scrHeight;
      }
      SwapBpp = 24;
    }
  }
  DoRenderSwap(hwnd);
}

int WINAPI WinMain( __in HINSTANCE hInstance, __in_opt HINSTANCE hPrevInstance, __in LPSTR lpCmdLine, __in int nShowCmd )
{
  WNDCLASSEX winClass; 
  const char WindowClass[]="Game In Game";

  //int b[1];
  MSG        uMsg;

  std::function<bool(const char *appName)> createWindow = [&] (const char *appName) -> bool
  {
    RECT rect;
    #if NDEBUG
    bool fullscreen = strstr(lpCmdLine,"-window")==NULL;
    #else
    bool fullscreen = strstr(lpCmdLine,"-fullscreen")!=NULL;
    #endif

    static const char fpsString[] = "-fps=";
    const char *fpsLimit = strstr(lpCmdLine,fpsString);
    if (fpsLimit)
    {
      int fps = atoi(fpsLimit+sizeof(fpsString)-1);
      limitFrameMs = 1000/fps;
    }
    memset(&uMsg,0,sizeof(uMsg));

    g_hInstance = hInstance;

    winClass.lpszClassName = WindowClass;
    winClass.cbSize        = sizeof(WNDCLASSEX);
    winClass.style         = CS_HREDRAW | CS_VREDRAW;
    winClass.lpfnWndProc   = WindowProc;
    winClass.hInstance     = hInstance;
     winClass.hIcon	       = NULL;
     winClass.hIconSm	     = NULL;
  //   winClass.hIcon	       = LoadIcon(hInstance, (LPCTSTR)IDI_ICON1);
  //   winClass.hIconSm	     = LoadIcon(hInstance, (LPCTSTR)IDI_ICON1);
    winClass.hCursor       = LoadCursor(NULL, IDC_ARROW);
    winClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    winClass.lpszMenuName  = NULL;
    winClass.cbClsExtra    = 0;
    winClass.cbWndExtra    = 0;

    if( !RegisterClassEx(&winClass) )
      return false;

    if (fullscreen)
    {
      SelectFullscreenMode();
      rect.top = 0;
      rect.left = 0;
      rect.right = g_FullscreenW;
      rect.bottom = g_FullscreenH;
      WinW =  g_FullscreenW;
      WinH =  g_FullscreenH;
    }
    else
    {
      DEVMODE devMode;
      EnumDisplaySettings( NULL, ENUM_CURRENT_SETTINGS, &devMode );

      rect.top = 0;
      rect.left = 0;
      int maxW = devMode.dmPelsWidth*3/4;
      int maxH = devMode.dmPelsHeight*3/4;
      if (maxW*480>640*maxH)
      {
        maxW = maxH*640/480;
      }
      else
      {
        maxH = maxW*480/640;
      }
      rect.right = maxW;
      rect.bottom = maxH;

      WinW =  rect.right;
      WinH =  rect.bottom;

      AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW | WS_VISIBLE, FALSE);
    }

    g_hWnd = CreateWindowEx( 0, WindowClass,  appName,
      fullscreen ? WS_POPUP | WS_VISIBLE : WS_OVERLAPPEDWINDOW | WS_VISIBLE,
      0, 0, rect.right-rect.left, rect.bottom-rect.top, NULL, NULL, hInstance, NULL );

    if (g_hWnd)
    {
      ShowWindow( g_hWnd, nShowCmd );
      UpdateWindow( g_hWnd );
    }

    return g_hWnd!=NULL;
  };



  timeBeginPeriod(1);
  if (GameInit(createWindow))
  {
    bool closed = false;
    while( uMsg.message != WM_QUIT )
    {
      HANDLE handles[2];
      DWORD wait;
      handles[0] = renderEvent;
      handles[1] = gigTerminated; // TODO: detect the game has terminated itself
      wait = MsgWaitForMultipleObjects(sizeof(handles)/sizeof(*handles),handles,FALSE,INFINITE,QS_ALLEVENTS);
      if( PeekMessage( &uMsg, NULL, 0, 0, PM_REMOVE ) )
      {
        TranslateMessage( &uMsg );
        DispatchMessage( &uMsg );
      }
      if (wait==WAIT_OBJECT_0 || WaitForSingleObject(renderEvent,0)==WAIT_OBJECT_0)
      {
        DoRender(g_hWnd,ScreenDesc_Gig);
        SetEvent(renderDone);
        //SwapBuffers( g_hDC );
      }
      if (wait==WAIT_OBJECT_0+1 || WaitForSingleObject(gigTerminated,0)==WAIT_OBJECT_0)
      {
        // also pretend the rendering was done
        if (!closed)
        {
          PostMessage(g_hWnd,WM_CLOSE,0,0);
          closed = true;
        }
      }
    }
    if (sound_gig) EndSoundWaveOut();
    return true;
  }

  UnregisterClass( WindowClass, winClass.hInstance );
  timeEndPeriod(1);

  return uMsg.wParam;
}
