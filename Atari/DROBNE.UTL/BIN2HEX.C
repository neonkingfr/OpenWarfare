#include <stdio.h>
#include <string.h>

int main( int argc, const char *argv[] )
{
	int ret=1;
	if( argc!=2 ) printf("USAGE: BIN2HEX source\n");
	else
	{
		FILE *f;
		char out[128],*e;
		strcpy(out,argv[1]);
		e=strrchr(out,'.');
		if( !e ) {printf("source must have an extension\n");return 1;}
		strcpy(e,".HEX");
		f=fopen(argv[1],"rb");
		if( f )
		{
			FILE *o=fopen(out,"w");
			if( o )
			{
				int cl=0,cs=0,c;
				const char *p=strrchr(argv[1],'\\');
				if( !p ) p=argv[1];
				else ++p;
				fprintf(o,"#%s\n",p);
				while( (c=fgetc(f))!=EOF )
				{
					fprintf(o,"%02x",c);
					cs+=c;
					if( ++cl>=32 ) cl=0,fprintf(o,"\n");
				}
				if( cl ) fprintf(o,"\n");
				fprintf(o,"#%04x",cs);
				fclose(o);
				ret=0;
			}
			fclose(f);
		}
	}
	return ret;
}
