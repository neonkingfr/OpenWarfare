/* automaticke ukladani a cteni NEWDESK.INF souboru */

#include <macros.h>
#include <tos.h>
#include <string.h>
#include <vidxbind.h>
#include <interf.h>
#include <sysvar.h>
#include "testauto.h"

int ApId;

static int BootDev( void )
{
	void *SSP=(void *)Super(NULL);
	int bd=_bootdev+'A';
	Super(SSP);
	return bd;
}

static char SBuf[16L*1024];

int main()
{
	PathName Inf;
	int i,handle;
	witype wi;
	wotype wo;
	for( i=0; i<10; i++ ) wi[i]=1;
	wi[7]=0;
	wi[10]=2;
	v_opnvwk(wi,&handle,wo);
	if( handle )
	{
		int RngX = wo[0]+1;
		int RngY = wo[1]+1;
		int NPla;
		int bd=BootDev();
		vq_extnd(handle,1,wo);
		NPla = wo[4];
		v_clsvwk(handle);
		if( !TestAuto() )
		{
			if( (ApId=appl_init())==0 )
			{
				if( shel_get(SBuf,(int)sizeof(SBuf)) )
				{
					char *e;
					int h;
					if( strncmpi(SBuf,"#a",2) ) {appl_exit();goto NoAuto;}
					if( NPla>9 ) NPla=9;
					if( NPla<0 ) NPla=0;
					sprintf(Inf,"%c:\\%dx%dx%d.INF",bd,(RngX+5)/10,(RngY+5)/10,NPla);
					e=SBuf+strlen(SBuf);
					h=creat(Inf);
					if( h>=0 )
					{
						write(h,SBuf,e-SBuf);
						close(h);
					}
				}
				appl_exit();
			}
		}
		else /* je AUTO */
		{
			NoAuto:
			Cconws("\r\nSelecon, � 1994, � SUMA, Praha\r\n");
			Cconws(Inf);
			Cconws("\r\n->NEWDESK.INF\r\n");
			{
				int h=open(Inf,0);
				if( h>=0 )
				{
					static PathName Nam="A:\\NEWDESK.INF";
					int w;
					Nam[0]=bd;
					w=creat(Nam);
					if( w>=0 )
					{
						long r;
						do
						{
							r=read(h,SBuf,sizeof(SBuf));
							if( r>0 ) write(w,SBuf,r);
						}
						while( r==sizeof(SBuf) );
						close(w);
					}
				}
			}
			/* jeste je nutno pridat dalsi nastavovani konfigurace */
			/* podle XBOOT.DAT */
		}
	}
	return 0;
}
