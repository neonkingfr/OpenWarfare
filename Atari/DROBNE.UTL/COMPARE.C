/* SUMA 12/1993 */
/* deleni souboru na mensi (a opetne spojovani) */

#include <macros.h>
#include <stdio.h>
#include <stdlib.h>
#include <tos.h>
#include <ctype.h>
#include <string.h>
#include <fileutil.h>

#define DBuf (256*1024L)

static int Compar( const char *C, const char *c )
{
	void *B;
	void *b=malloc(2*DBuf);
	int ret=-1;
	int r,R;
	if( !b ) {printf("No RAM\n");Cnecin();return -1;}
	B=(char *)b+DBuf;
	r=open(c,O_RDONLY);
	if( r>=0 )
	{
		R=open(C,O_RDONLY);
		if( R>=0 )
		{
			long rd;
			do
			{
				rd=read(r,b,DBuf);if( rd<0 ) goto CError;
				if( rd!=read(R,B,DBuf) ) goto CError;
				if( memcmp(b,B,rd) ) goto CError;
			}
			while( rd==DBuf );
			ret=0;
			CError:
			if( ret!=0 )
			{
				printf("Diff: %ld\n",rd);
				Cnecin();
			}
			close(R);
		}
		close(r);
	}
	return ret;
}

int main( int argc, const char *argv[] )
{
	if( argc==3 ) /* deleni */
	{
		return Compar(argv[1],argv[2]);
	}
	else
	{
		printf("compare filename.ext filename.ext\n");
		Cnecin();
		return 0;
	}
}