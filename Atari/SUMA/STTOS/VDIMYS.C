/* virtualni obsluha mysi */
/* SUMA 03/1994 - varianta VDI */

#include <macros.h>
#include <sttos\irqmys.h>
#include <tos.h>
#include <vdi.h>
#include <aes.h>

typedef int witype[11];
typedef int wotype[57];

static int MOpenGraf( void )
{
	int i,handle;
	witype wi;
	wotype wo;
	int d;
	handle=graf_handle(&d,&d,&d,&d);
	for( i=0; i<10; i++ ) wi[i]=1;
	wi[7]=0;
	wi[10]=2;
	v_opnvwk(wi,&handle,wo);
	return handle;
}

static int MHandle;
void KonMys( void )
{
	v_clsvwk(MHandle);
}

int ZacMys( void )
{
	MHandle=MOpenGraf();
	return MHandle>0 ? 0 : -1;
}

void ReqMys( void ) {}

void InqMys( int *MB, int *MX, int *MY )
{
	vq_mouse(MHandle,MB,MX,MY);
}

