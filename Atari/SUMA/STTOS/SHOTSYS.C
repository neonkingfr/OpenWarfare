/* ShotSystem (TM) */
/* SUMA 3/1994-3/1994 */

#include <windows.h>
#include <macros.h>
#include <sttos\shotsys.h>
#include <stdc\memspc.h>
#include <assert.h>

int ShotInd=0;
unsigned long *ShotObr;
unsigned long *VRam; /* LogAdr - sem vsichni kresli */
long ScrSize;
int ScrWidth;
int ScrHeight;
int ScrBpp;
unsigned int ShotPalette[256];

CRITICAL_SECTION ScrParameters;

/* Last a Akt se mohou lisit o jeden snimek ! */

long *LocalVRam; // simulated framebuffer

int ZacShot( void )
{
	int ret=-1;
	Vsync();
	ShotInd=0;

  ShotObr = mallocSpc(ScrSize,'Shot');
  memset(ShotObr,0,ScrSize);

	VRam=ShotObr;
	/* 0 se pr�v� zobrazuje */
	Vsync();
	ret=0;
	return ret;
}

void KonShot( void )
{
  if (LocalVRam) freeSpc(LocalVRam),LocalVRam = NULL;
  VRam = NULL;
  if (ShotObr) freeSpc(ShotObr),ShotObr=NULL;

	Vsync();
}

void present_shot( unsigned long *shotObr, int scrWidth, int scrHeight, int scrBpp, unsigned int *shotPalette );

void DoShot( void )
{
  present_shot(VRam,ScrWidth,ScrHeight,ScrBpp,ShotPalette);
	VRam=ShotObr;
}

void ShotPresent()
{
  present_shot(VRam,ScrWidth,ScrHeight,ScrBpp,ShotPalette);
}

void *Physbase()
{
  return LocalVRam;
}

void ShotResolution(int w, int h, int bpp)
{
  int newSize = w*h*bpp/8;
  assert(bpp==8 || bpp==32);
  //assert(bpp>=4 && bpp<=16);
  if (LocalVRam) freeSpc(LocalVRam);
  LocalVRam = mallocSpc(newSize,'VRAM');
  memset(LocalVRam,0,newSize);
  VRam = LocalVRam;
  ScrSize = newSize;
  ScrWidth = w;
  ScrHeight = h;
  ScrBpp = bpp;
}

