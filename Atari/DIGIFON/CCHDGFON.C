/* cachovani dlouhych pro AVR samplu */
/* virtualizace pristupu k souboru */
/* SUMA 12/1993 */

#include <macros.h>
#include <stdlib.h>
#include <stdio.h>
#include <tos.h>
#include <time.h>
#include <limits.h>
#include "ramdgfon.h"
#include "digifon.h"

/* pristup jen ke stereo 16 bitu samplum! */
long SizeAlok=0;

static Flag FVyhodCAVR( OknoSpec *OI )
{
	stat_t Max=MAX_STAT;
	long i,MaxI=-1;
	for( i=0; i<NMaxBloku; i++ )
	{
		stat_t s;
		s=OI->Stat[i];
		if( s>0 && s<Max ) Max=s,MaxI=i;
	}
	if( MaxI>=0 )
	{
		OI->Stat[MaxI]=0;
		free(OI->Mapa[MaxI]),OI->Mapa[MaxI]=NULL;
		SizeAlok-=VelAVRBloku;
		return True;
	}
	return False;
}

static Flag VyhodCAVR( OknoSpec *OI )
{
	int LHan;
	/* pokus se vyhodit z jine cache */
	for( LHan=0; LHan<NMaxOken; LHan++ ) if( LHan!=OI->WD.LHan )
	{
		Okno *O=Okna[LHan];
		if( O && O->SDruh==SDSample )
		{
			OknoSpec *SI=OInfa[LHan];
			if( FVyhodCAVR(SI) ) return True;
		}
	}
	return FVyhodCAVR(OI);
}

enum cchyba CAVRChyba=C_OK;
int CCLHan=-1;

static stat_t StatVal=1;
static int div_stat=0;

int GetCAVR( OknoSpec *OI, long pos, int tr )
{ /* podle potreby nacita do pameti dalsi bloky */
	long ind=pos>>(LogAVRBloku-2);
	long off=pos&(VelAVRBloku/4-1);
	int *B=OI->Mapa[ind];
	if( OI->FHandle<0 ) return 0;
	if( !B )
	{ /* zaved do cache */
		long Fre=(long)Malloc(-1);
		while( SizeAlok>=Fre*2 ) /* max. velikost cache (1/3 volne pameti) */
		{
			if( !VyhodCAVR(OI) ) break;
		}
		B=malloc(VelAVRBloku);
		if( !B ) {CCLHan=OI->WD.LHan;CAVRChyba=C_RAM;return 0;}
		{
			long FPos=ind*VelAVRBloku;
			long len=OI->H.avr_length-FPos;
			FPos+=sizeof(avr_t);
			if( len>VelAVRBloku ) len=VelAVRBloku;
			if( lseek(OI->FHandle,FPos,SEEK_SET)!=FPos || read(OI->FHandle,B,len)!=len )
			{
				free(B);
				CCLHan=OI->WD.LHan;
				CAVRChyba=C_FILE;
				return 0;
			}
		}
		OI->Mapa[ind]=B;
		OI->Dirty[ind]=False;
		SizeAlok+=VelAVRBloku;
	}
	if( --div_stat<=0 )
	{
		div_stat=0x1000;
		if( StatVal<MAX_STAT ) StatVal++;
	}
	OI->Stat[ind]=StatVal;
	return B[off*2+tr];
}

void *ZavedBlokCAVR( OknoSpec *OI, long pos, stat_t Stat )
{ /* podle potreby nacita do pameti dalsi bloky */
	long ind=pos>>(LogAVRBloku-2);
	int *B=OI->Mapa[ind];
	if( OI->FHandle<0 ) return NULL;
	if( !B )
	{ /* zaved do cache */
		long Fre=(long)Malloc(-1);
		while( SizeAlok>=Fre*2 ) /* max. velikost cache (1/3 volne pameti) */
		{
			if( !VyhodCAVR(OI) ) break;
		}
		B=malloc(VelAVRBloku);
		if( !B ) {CCLHan=OI->WD.LHan;CAVRChyba=C_RAM;return 0;}
		{
			long FPos=ind*VelAVRBloku;
			long len=OI->H.avr_length-FPos;
			FPos+=sizeof(avr_t);
			if( len>VelAVRBloku ) len=VelAVRBloku;
			if( lseek(OI->FHandle,FPos,SEEK_SET)!=FPos || read(OI->FHandle,B,len)!=len )
			{
				free(B);
				CCLHan=OI->WD.LHan;
				CAVRChyba=C_FILE;
				return 0;
			}
		}
		OI->Mapa[ind]=B;
		OI->Dirty[ind]=False;
		SizeAlok+=VelAVRBloku;
	}
	Stat>>=12;
	div_stat=0x1000;
	if( StatVal<MAX_STAT-Stat ) StatVal+=Stat;
	OI->Stat[ind]=StatVal;
	return B;
}

int ZacCAVR( OknoSpec *OI )
{
	long i;
	for( i=0; i<NMaxBloku; i++ )
	{
		OI->Mapa[i]=NULL,OI->Stat[i]=0;
		OI->Dirty[i]=False;
	}
	{
		avr_t *H=&OI->H;
		int F=open(OI->P,O_RDONLY);
		if( F<0 ) {RChybaA(OI->P);return -1;}
		if( sizeof(avr_t)!=read(F,H,sizeof(avr_t)) )
		{
			ErrHlava: close(F);AlertRF(FORMSA,1);OI->FHandle=-1;return -1;
		}
		if( H->avr_magic!=AVR_MAGIC ) goto ErrHlava;
		if( H->avr_resolution!=16 ) goto ErrHlava;
		if( H->avr_mode==AVR_MONO ) goto ErrHlava;
		if( H->avr_signed==AVR_UNSIGNED ) goto ErrHlava;
		OI->FHandle=F;
	}
	return 0;
}

void KonCAVR( OknoSpec *OI )
{
	long i;
	if( OI->FHandle<0 ) return;
	for( i=0; i<NMaxBloku; i++ )
	{
		if( OI->Mapa[i] )
		{
			Plati( !OI->Dirty[i] );
			free(OI->Mapa[i]);
			OI->Mapa[i]=NULL;
			SizeAlok-=VelAVRBloku;
		}
	}
	close(OI->FHandle);
}
