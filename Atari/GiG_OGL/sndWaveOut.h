#ifndef sndWaveOut_h__
#define sndWaveOut_h__

typedef size_t SoundCallbackFunction(void *buffer, size_t bufferSize, int bufferFreq);

void StartSoundWaveOut(int freq, SoundCallbackFunction *callback);
void EndSoundWaveOut();

#endif // sndWaveOut_h__
