/* Gravon- akcni 3D hra */
/* SUMA 9/1994-2/1995 */

#include <windows.h>
#include <intrin.h>

//#include "resource.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <float.h>
#include <math.h>
#include <assert.h>
#include "sndWaveOut.h"

#include "GiG/gig.h"

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

HINSTANCE g_hInstance;
DEVMODE g_oldDevMode;
DEVMODE g_devModeBest;
bool g_fullscreen;
int g_FullscreenW;
int g_FullscreenH;
HGLRC g_hRC  = NULL;
HWND  g_hWnd = NULL;
HDC	  g_hDC  = NULL;
HANDLE renderEvent;
HANDLE renderDone;


static BOOL GLInitDone;
static BOOL AppActive;
static BOOL screenSaverEnabledBackup;

#define GIG_DECL_FUNC(ret,name,args) ret __stdcall name args;
#define GIG_DEFINE_FUNC_PTR(ret,name,args) ret (__stdcall *name) args;

GIG_HOST(GIG_DECL_FUNC)

extern "C" {
  struct GiG_Interface
  {
    // implemented on the host side
    GIG_HOST(GIG_DEFINE_FUNC_PTR)
  };

  GIG_GAME(GIG_DEFINE_FUNC_PTR)
};


template <class Type>
inline void GetFunctionAddress(Type *&res, HMODULE mod, const char *name)
{
  res = (Type *)GetProcAddress(mod,name);
};

void GIG_Bind(const char *name)
{
  // load the dll and bind to it
  HMODULE lib = LoadLibrary(name);
  GetFunctionAddress(init_gig,lib,"_init_gig@8");
  GetFunctionAddress(shutDown_gig,lib,"_shutDown_gig@0");
  GetFunctionAddress(onKeyDown_gig,lib,"_onKeyDown_gig@4");
  GetFunctionAddress(sound_gig,lib,"_sound_gig@12");
}

static void SelectFullscreenMode()
{

  //
  // Enumerate Device modes...
  //

  int nMode = 0;
  float bestDist = FLT_MAX;
  DEVMODE devMode;
  BOOL bDesiredDevModeFound = FALSE;
  unsigned bitsPerPixelWanted = 32;
  unsigned bitsPerPixelMin = 16;
  unsigned bitsPerPixelMax = 32;
  int refreshWanted = 75;
  float screenAspect;

  // Cache the current display mode so we can switch back when done.
  EnumDisplaySettings( NULL, ENUM_CURRENT_SETTINGS, &g_oldDevMode );

  // assume desktop is set to a mode which tells us about a screen aspect
  screenAspect = (float)g_oldDevMode.dmPelsWidth/g_oldDevMode.dmPelsHeight;

  // find a mode with the same aspect, at least 640x480
  // prefer the desktop resolution

  while( EnumDisplaySettings( NULL, nMode++, &devMode ) )
  {
    float dist = 0;

    // Does this device mode support a 640 x 480 setting?
    if( devMode.dmPelsWidth  < 640 || devMode.dmPelsHeight < 480)
      continue;

    // avoid modes too different from the target one - no resizing for OGL now
    dist += int(devMode.dmPelsWidth*devMode.dmPelsHeight-640*480)*0.001f;

    // Does this device mode support 32-bit color?
    if( devMode.dmBitsPerPel < bitsPerPixelMin || devMode.dmBitsPerPel >bitsPerPixelMax)
    {
      continue;
    }

    // prefer keeping the aspect
    dist += (float)fabs(screenAspect-(float)devMode.dmPelsWidth/devMode.dmPelsHeight)*1000;
    dist += 10*(float)fabs((float)(int)(devMode.dmBitsPerPel - bitsPerPixelWanted));

    dist += (float)fabs((float)(int)(devMode.dmDisplayFrequency - g_oldDevMode.dmDisplayFrequency));

    if (dist<bestDist)
    {
      g_devModeBest = devMode;
      bestDist = dist;
    }
  }

  // We found a match, but can it be set without rebooting?
  if( ChangeDisplaySettings( &g_devModeBest, CDS_TEST ) == DISP_CHANGE_SUCCESSFUL )
  {
    g_fullscreen = true;
    g_FullscreenW = g_devModeBest.dmPelsWidth;
    g_FullscreenH = g_devModeBest.dmPelsHeight;
  }

}

void setScreenMode()
{
  if (g_fullscreen)
  {
    ChangeDisplaySettings( &g_devModeBest, CDS_FULLSCREEN );
  }
}

void restoreScreenMode()
{
  if (g_fullscreen)
  {
    ChangeDisplaySettings( &g_oldDevMode, CDS_FULLSCREEN );
  }
}

void shutDown( void )	
{
  if (!GLInitDone) return;
  GLInitDone = FALSE;
  if (g_fullscreen)
  {
    ChangeDisplaySettings( &g_oldDevMode, CDS_FULLSCREEN );
  }

	if( g_hRC != NULL )
	{
		wglMakeCurrent( NULL, NULL );
		wglDeleteContext( g_hRC );
		g_hRC = NULL;
	}

	if( g_hDC != NULL )
	{
		ReleaseDC( g_hWnd, g_hDC );
		g_hDC = NULL;
	}

  if (renderEvent) CloseHandle(renderEvent), renderEvent = NULL;
  if (renderDone) CloseHandle(renderDone), renderDone = NULL;
  if (shutDown_gig) shutDown_gig();
}



static bool GameInit()
{
  PIXELFORMATDESCRIPTOR pfd;
  GLuint iPixelFormat;
  if (GLInitDone) return true;
  GLInitDone = TRUE;
  memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));

  pfd.nSize      = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion   = 1;
  pfd.dwFlags    = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
  pfd.iPixelType = PFD_TYPE_RGBA;
  pfd.cColorBits = 32;
  pfd.cDepthBits = 24;

  g_hDC = GetDC( g_hWnd );
  iPixelFormat = ChoosePixelFormat( g_hDC, &pfd );

  if( iPixelFormat != 0 )
  {
    PIXELFORMATDESCRIPTOR bestMatch_pfd;
    DescribePixelFormat( g_hDC, iPixelFormat, sizeof(pfd), &bestMatch_pfd );

    // TO DO: Double-check  the closest match pfd for anything unacceptable...

    if( bestMatch_pfd.cDepthBits < pfd.cDepthBits )
    {
      // POTENTIAL PROBLEM: We need at least a 16-bit z-buffer!
      return false;
    }

    if( SetPixelFormat( g_hDC, iPixelFormat, &pfd) == FALSE )
    {
      DWORD dwErrorCode = GetLastError();
      // TO DO: Report cause of failure here...
      return false;
    }
  }
  else
  {
    DWORD dwErrorCode = GetLastError();
    // TO DO: Report cause of failure here...
    return false;
  }

  g_hRC = wglCreateContext( g_hDC );
  wglMakeCurrent( g_hDC, g_hRC );

  glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );


  renderDone = CreateEvent(NULL, FALSE, FALSE, NULL);
  renderEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

  if (g_fullscreen)
  {
    ChangeDisplaySettings( &g_devModeBest, CDS_FULLSCREEN );
  }

  GIG_Bind("gravon.dll");
  if (init_gig)
  {
    GiG_Interface gig;
    gig.mouseState_gig = mouseState_gig;
    gig.present_gig = present_gig;
    init_gig(g_hWnd,&gig);
    return true;
  }
  return false;
}

/// multi threaded synchronization functionality
class CriticalSection
{
private:
  bool _initialized;
  mutable CRITICAL_SECTION _handle;
  
public:
  /// create initialized object
  CriticalSection() {InitializeCriticalSection(&_handle);}
  /// create initialized object
  explicit CriticalSection(DWORD spinCount) {InitializeCriticalSectionAndSpinCount(&_handle, spinCount);}
  ~CriticalSection() {DeleteCriticalSection(&_handle);}

  bool Lock() const
  {
    EnterCriticalSection(&_handle);
    return true;
  }
  void Unlock() const
  {
    LeaveCriticalSection(&_handle);
  }

private:
  void operator = (const CriticalSection &src);
  CriticalSection(const CriticalSection &src);
};

struct ScreenDesc
{
  unsigned long *shotObr;
  int scrWidth;
  int scrHeight;
  int scrBpp;
  unsigned int *shotPalette;
};

CriticalSection ScreenDescCS_Gig;
ScreenDesc ScreenDesc_Gig;

static void __stdcall present_gig( unsigned long *shotObr, int scrWidth, int scrHeight, int scrBpp, unsigned int *shotPalette )
{
  ScreenDescCS_Gig.Lock();
  ScreenDesc_Gig.shotObr = shotObr;
  ScreenDesc_Gig.scrWidth = scrWidth;
  ScreenDesc_Gig.scrHeight = scrHeight;
  ScreenDesc_Gig.scrBpp = scrBpp;
  ScreenDesc_Gig.shotPalette = shotPalette;

  SetEvent(renderEvent);
  // wait until the event is reset (the shot was done)
  WaitForSingleObject(renderDone,INFINITE);
  ScreenDescCS_Gig.Unlock();
}


static byte Swap[640*480*3]; // 640*480 should be enough for any mode
static int SwapWidth;
static int SwapHeight;
static int SwapBpp;
static unsigned int SwapPalette[256];

void DoRenderSwap()
{
  // zoom 640x480 into g_FullscreenW x g_FullscreeH
  if (g_FullscreenW>640 || g_FullscreenH>480)
  {
    float xPos = 640.0f/g_FullscreenW;
    float yPos = 480.0f/g_FullscreenH;
    glRasterPos4f(-xPos,-yPos,1,1);
    glClear(GL_COLOR_BUFFER_BIT);
    {
      //       float zoomX = g_FullscreenW/640.0f;
      //       float zoomY = g_FullscreenH/480.0f;
      //       float zoom = zoomX<zoomY ? zoomX : zoomY;
      //       glPixelZoom(zoom,zoom);
    }
  }
  else
  {
    glRasterPos4f(-1,-1,1,1);
  }
  if (SwapBpp!=8)
  {
    glDrawPixels(640,480,GL_RGB,GL_UNSIGNED_BYTE,Swap);
  }
  else
  {
    float r[256],g[256],b[256],a[256];
    for (int i=0; i<256; i++)
    {
      const float f = 1.0f/255;
      r[i] = ((SwapPalette[i]>>16)&0xff)*f;
      g[i] = ((SwapPalette[i]>>8)&0xff)*f;
      b[i] = ((SwapPalette[i]>>0)&0xff)*f;
      a[i] = 1.0f;
    }
    glPixelMapfv(GL_PIXEL_MAP_I_TO_R,256,r);
    glPixelMapfv(GL_PIXEL_MAP_I_TO_G,256,g);
    glPixelMapfv(GL_PIXEL_MAP_I_TO_B,256,b);
    glPixelMapfv(GL_PIXEL_MAP_I_TO_A,256,a);
    glDrawPixels(SwapWidth,SwapHeight,GL_COLOR_INDEX,GL_UNSIGNED_BYTE,Swap);
  }
}

static int GIG_MysX,GIG_MysY,GIG_MB,GIG_MysW;

static void __stdcall mouseState_gig(int *x, int *y, int *buttons, int *wheel)
{
  *x = GIG_MysX;
  *y = GIG_MysY;
  *buttons= GIG_MB;
  if (wheel)
  {
    *wheel = InterlockedExchange((long *)&GIG_MysW,0);
  }
}


LRESULT CALLBACK WindowProc( HWND   hWnd,  UINT   msg,  WPARAM wParam, LPARAM lParam )
{
  switch( msg )
  {
  case WM_KEYDOWN:
    if (onKeyDown_gig) onKeyDown_gig(wParam);
    break;
  case WM_MOUSEMOVE:
    // TODO: clip mouse in the allowable control area
    GIG_MysX = (int)(short)lParam;
    GIG_MysY = (int)(short)(lParam>>16);
    break;
  case WM_LBUTTONDOWN: GIG_MB |= 1; break;
  case WM_RBUTTONDOWN: GIG_MB |= 2; break;
  case WM_LBUTTONUP: GIG_MB &= ~1; break;
  case WM_RBUTTONUP:GIG_MB &= ~2; break;
  case WM_MOUSEWHEEL: GIG_MysW += GET_WHEEL_DELTA_WPARAM(wParam)/WHEEL_DELTA;
  case WM_PAINT:
    if (!g_fullscreen)
    {
      DoRenderSwap();
      SwapBuffers(g_hDC);
    }
    return 0;
  case WM_SETCURSOR:
    SetCursor(NULL);
    break;
  case WM_ACTIVATE:
    {
      BOOL active = LOWORD(wParam)!=WA_INACTIVE;
      BOOL minimized = HIWORD(wParam)!=0;

      if (active && !minimized)
      {	
        AppActive = TRUE;
        SystemParametersInfo(SPI_GETSCREENSAVEACTIVE, 0, &screenSaverEnabledBackup, 0);
        SystemParametersInfo(SPI_SETSCREENSAVEACTIVE, FALSE, 0, 0);
        if (g_fullscreen)
        {
          setScreenMode();
          SetForegroundWindow( hWnd );				
          ShowWindow(hWnd, SW_RESTORE );				
        }
      }			
      else			
      {
        AppActive = FALSE;				
        if (g_fullscreen)
        {
          ShowWindow(hWnd,SW_MINIMIZE);				
          restoreScreenMode();
        }
        if (g_fullscreen)
        {
          if (onKeyDown_gig) onKeyDown_gig(VK_CUSTOM_PAUSE);
        }
        SystemParametersInfo(SPI_SETSCREENSAVEACTIVE, screenSaverEnabledBackup, 0, 0);
      }		
    }

    return DefWindowProc( hWnd, msg, wParam, lParam );
  case WM_SIZE:
    if (!g_fullscreen)
    {
      int nWidth  = LOWORD(lParam); 
      int nHeight = HIWORD(lParam);
      glViewport(0, 0, nWidth, nHeight);
    }
    break;

  case WM_CLOSE:
    PostMessage(hWnd,WM_DESTROY,0,0);
    return TRUE;
  case WM_DESTROY:
    shutDown();
    PostQuitMessage(0);
    g_hWnd = NULL;
    break;

  default:
    return DefWindowProc( hWnd, msg, wParam, lParam );
  }

  return 0;
}

#define COL_R(color) ( (int)((color>>0)&0xff) )
#define COL_G(color) ( (int)((color>>8)&0xff) )
#define COL_B(color) ( (int)((color>>16)&0xff) )
#define COL_A(color) ( (int)((color>>24)&0xff) )


static void Bilint(byte *tgt, int tgtLine, const unsigned long *src, int srcW, int srcH, int x, int y)
{
  int x1 = x+1<srcW ? x+1: srcW-1;
  int y1 = y+1<srcH ? y+1: srcH-1;
  int color00 = src[srcW*y+x];
  int color01 = src[srcW*y1+x];
  int color10 = src[srcW*y+x1];
  int color11 = src[srcW*y1+x1];

  int r00 = COL_R(color00), g00 = COL_G(color00), b00 = COL_B(color00);
  int r01 = COL_R(color01), g01 = COL_G(color01), b01 = COL_B(color01);
  int r10 = COL_R(color10), g10 = COL_G(color10), b10 = COL_B(color10);
  int r11 = COL_R(color11), g11 = COL_G(color11), b11 = COL_B(color11);

  tgt[0] = (r01+r00)/2, tgt[1] = (g01+g00)/2, tgt[2] = (b01+b00)/2;
  tgt[3] = (r11+r00+r10+r01)/4, tgt[3+1] = (g11+g00+g10+g01)/4, tgt[3+2] = (b11+b00+b10+b01)/4;
  tgt[tgtLine*3] = r00, tgt[tgtLine*3+1] = g00, tgt[tgtLine*3+2] = b00;
  tgt[tgtLine*3+3] = (r10+r00)/2, tgt[tgtLine*3+3+1] = (g10+g00)/2, tgt[tgtLine*3+3+2] = (b10+b00)/2;
}



void DoRender(ScreenDesc &desc)
{
  // copy from VRam to framebuffer, adjust format as necessary, manipulate the palette
  // swap top-down

  {
    //float zoom;
    
    if (desc.scrBpp!=8)
    {
      int x,y;
      if (desc.scrWidth==640/2 && desc.scrHeight==480/2)
      {
        for(y=0; y<desc.scrHeight; y++)
        for(x=0; x<desc.scrWidth; x++)
        {
          int base = ((desc.scrHeight-1-y)*2*640+x*2)*3;
          Bilint(Swap+base,640,desc.shotObr,desc.scrWidth,desc.scrHeight,x,y);
        }
      }
      else if (desc.scrWidth==640 && desc.scrHeight==480)
      {
        for(y=0; y<desc.scrHeight; y++)
        for(x=0; x<desc.scrWidth; x++)
        {
          int base = ((desc.scrHeight-1-y)*640+x)*3;

          int c = desc.shotObr[desc.scrWidth*y+x];
          Swap[base+0] = c>>0, Swap[base+1] = c>>8, Swap[base+2] = c>>16;
        }
      }
      else
      {
        assert(false); // general rescale not supported yet
      }
    }
    else
    {
      memcpy(SwapPalette,desc.shotPalette,sizeof(SwapPalette));
      for(int y=0; y<desc.scrHeight; y++)
      {
        memcpy(Swap+desc.scrWidth*y,(char *)desc.shotObr+desc.scrWidth*(desc.scrHeight-1-y),desc.scrWidth*desc.scrBpp/8);
      }
    }
  }
  SwapWidth = desc.scrWidth;
  SwapHeight = desc.scrHeight;
  SwapBpp = desc.scrBpp;
  DoRenderSwap();
}

size_t SoundCallbackFunc(void *buffer, size_t bufferSize, int bufferFreq)
{
  return sound_gig(buffer,bufferSize,bufferFreq);
};

int WINAPI WinMain( __in HINSTANCE hInstance, __in_opt HINSTANCE hPrevInstance, __in LPSTR lpCmdLine, __in int nShowCmd )
{
  WNDCLASSEX winClass; 
  //int b[1];
  MSG        uMsg;
  const char WindowClass[]="Gravon";
  const char AppName[]="Gravon";
  RECT rect;

  #if NDEBUG
  bool fullscreen = strstr(lpCmdLine,"-window")==NULL;
  #else
  bool fullscreen = strstr(lpCmdLine,"-fullscreen")!=NULL;
  #endif

  memset(&uMsg,0,sizeof(uMsg));

  g_hInstance = hInstance;

  winClass.lpszClassName = WindowClass;
  winClass.cbSize        = sizeof(WNDCLASSEX);
  winClass.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  winClass.lpfnWndProc   = WindowProc;
  winClass.hInstance     = hInstance;
   winClass.hIcon	       = NULL;
   winClass.hIconSm	     = NULL;
//   winClass.hIcon	       = LoadIcon(hInstance, (LPCTSTR)IDI_ICON1);
//   winClass.hIconSm	     = LoadIcon(hInstance, (LPCTSTR)IDI_ICON1);
  winClass.hCursor       = LoadCursor(NULL, IDC_ARROW);
  winClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  winClass.lpszMenuName  = NULL;
  winClass.cbClsExtra    = 0;
  winClass.cbWndExtra    = 0;

  if( !RegisterClassEx(&winClass) )
    return E_FAIL;

  if (fullscreen)
  {
    SelectFullscreenMode();
    rect.top = 0;
    rect.left = 0;
    rect.right = g_FullscreenW;
    rect.bottom = g_FullscreenH;
  }
  else
  {

    rect.top = 0;
    rect.left = 0;
    rect.right = 640;
    rect.bottom = 480;

    AdjustWindowRect(&rect, WS_OVERLAPPEDWINDOW | WS_VISIBLE, FALSE);
  }

  g_hWnd = CreateWindowEx( 0, WindowClass,  AppName,
    fullscreen ? WS_POPUP | WS_VISIBLE : WS_OVERLAPPEDWINDOW | WS_VISIBLE,
    0, 0, rect.right-rect.left, rect.bottom-rect.top, NULL, NULL, hInstance, NULL );

  if( g_hWnd == NULL )
    return E_FAIL;

  ShowWindow( g_hWnd, nShowCmd );
  UpdateWindow( g_hWnd );


  if (GameInit())
  {
    if (sound_gig) StartSoundWaveOut(44100,SoundCallbackFunc);

    while( uMsg.message != WM_QUIT )
    {
      HANDLE handles[1];
      DWORD wait;
      handles[0] = renderEvent;
      //handles[1] = gameThread; // TODO: detect the game has termiated itself
      wait = MsgWaitForMultipleObjects(sizeof(handles)/sizeof(*handles),handles,FALSE,INFINITE,QS_ALLEVENTS);
      if( PeekMessage( &uMsg, NULL, 0, 0, PM_REMOVE ) )
      {
        TranslateMessage( &uMsg );
        DispatchMessage( &uMsg );
      }
      if (wait==WAIT_OBJECT_0 || WaitForSingleObject(renderEvent,0)==WAIT_OBJECT_0)
      {
        DoRender(ScreenDesc_Gig);
        SetEvent(renderDone);
        SwapBuffers( g_hDC );
      }
//       if (wait==WAIT_OBJECT_0+1)
//       {
//         PostMessage(g_hWnd,WM_DESTROY,0,0);
//       }
    }
    if (sound_gig) EndSoundWaveOut();
  }

  UnregisterClass( WindowClass, winClass.hInstance );

  return uMsg.wParam;
}
