#define MENU 0  	/* TREE */
#define DESKT 3  	/* OBJECT in TREE #0 */
#define SOUBORT 4  	/* OBJECT in TREE #0 */
#define KONECI 29  	/* OBJECT in TREE #0 */
#define OTEVRI 20  	/* OBJECT in TREE #0 */
#define ULOZI 22  	/* OBJECT in TREE #0 */
#define ULOZJAKI 23  	/* OBJECT in TREE #0 */
#define EDITT 6  	/* OBJECT in TREE #0 */
#define ROZSAHI 41  	/* OBJECT in TREE #0 */
#define ZESILI 39  	/* OBJECT in TREE #0 */
#define ZESLABI 40  	/* OBJECT in TREE #0 */
#define SAMPLED 2  	/* TREE */
#define SMPSOUBT 1  	/* OBJECT in TREE #2 */
#define SMPFRQT 21  	/* OBJECT in TREE #2 */
#define SMPDELT 23  	/* OBJECT in TREE #2 */
#define SMPRODT 24  	/* OBJECT in TREE #2 */
#define SMPRDOT 25  	/* OBJECT in TREE #2 */
#define SMPNOTAT 20  	/* OBJECT in TREE #2 */
#define ABOUTI 10  	/* OBJECT in TREE #0 */
#define ABOUTD 1  	/* TREE */
#define NOKNAA 9  	/* STRING */
#define SMPNAZT 19  	/* OBJECT in TREE #2 */
#define ZHLASD 3  	/* TREE */
#define ZESNAZT 1  	/* OBJECT in TREE #3 */
#define ZESOKB 5  	/* OBJECT in TREE #3 */
#define ZESCANB 6  	/* OBJECT in TREE #3 */
#define ZESILFS 0		/* FREE STRING */
#define ZESLABFS 1		/* FREE STRING */
#define IERRA 11  	/* STRING */
#define OERRA 10  	/* STRING */
#define RAMEA 12  	/* STRING */
#define SMPCBOXB 26  	/* OBJECT in TREE #2 */
#define ZESOT 3  	/* OBJECT in TREE #3 */
#define SMPSTEPD 34  	/* OBJECT in TREE #2 */
#define SMPBITPD 35  	/* OBJECT in TREE #2 */
#define SMPPLYB 28  	/* OBJECT in TREE #2 */
#define SMPPAUB 29  	/* OBJECT in TREE #2 */
#define STEREOFS 2		/* FREE STRING */
#define MONOFS 3		/* FREE STRING */
#define BIT16FS 4		/* FREE STRING */
#define BIT8FS 5		/* FREE STRING */
#define INFOD 4  	/* TREE */
#define INFROZT 4  	/* OBJECT in TREE #4 */
#define INFLEVT 7  	/* OBJECT in TREE #4 */
#define INFPRAT 10  	/* OBJECT in TREE #4 */
#define STATUSI 27  	/* OBJECT in TREE #0 */
#define INFONAZT 1  	/* OBJECT in TREE #4 */
#define VERZET 6  	/* OBJECT in TREE #1 */
#define DATUMT 7  	/* OBJECT in TREE #1 */
#define SMPOSCUD 3  	/* OBJECT in TREE #2 */
#define OKNAT 7  	/* OBJECT in TREE #0 */
#define USPORI 53  	/* OBJECT in TREE #0 */
#define PROHODI 52  	/* OBJECT in TREE #0 */
#define ZAVRII 25  	/* OBJECT in TREE #0 */
#define VYBNASFS 6		/* FREE STRING */
#define ULONASFS 7		/* FREE STRING */
#define SMPOSCBB 2  	/* OBJECT in TREE #2 */
#define SCALEPD 5  	/* TREE */
#define SCA100I 4  	/* OBJECT in TREE #5 */
#define SCAPOSI 10  	/* OBJECT in TREE #5 */
#define SMPMERPD 31  	/* OBJECT in TREE #2 */
#define SCAPRVI 1  	/* OBJECT in TREE #5 */
#define SELALLI 31  	/* OBJECT in TREE #0 */
#define UNSALLI 32  	/* OBJECT in TREE #0 */
#define BLOKT 5  	/* OBJECT in TREE #0 */
#define FILTRI 46  	/* OBJECT in TREE #0 */
#define PANIKA 13  	/* STRING */
#define INFSLEVT 13  	/* OBJECT in TREE #4 */
#define INFSPRAT 16  	/* OBJECT in TREE #4 */
#define STREDI 43  	/* OBJECT in TREE #0 */
#define FILTRD 6  	/* TREE */
#define FILNT 3  	/* OBJECT in TREE #6 */
#define FILOKB 4  	/* OBJECT in TREE #6 */
#define KONVCOMI 50  	/* OBJECT in TREE #0 */
#define NABEHI 45  	/* OBJECT in TREE #0 */
#define CLZVUKA 14  	/* STRING */
#define REPBLOKI 35  	/* OBJECT in TREE #0 */
#define REZBLOKI 34  	/* OBJECT in TREE #0 */
#define FREKVPD 7  	/* TREE */
#define FREKV0I 1  	/* OBJECT in TREE #7 */
#define FREKVPI 8  	/* OBJECT in TREE #7 */
#define SMPYZOPD 33  	/* OBJECT in TREE #2 */
#define SMPRECB 27  	/* OBJECT in TREE #2 */
#define SMPFRQPD 17  	/* OBJECT in TREE #2 */
#define NOVYI 19  	/* OBJECT in TREE #0 */
#define SNDLOCKA 8  	/* STRING */
#define SMPRECBX 4  	/* OBJECT in TREE #2 */
#define SMPRECLB 6  	/* OBJECT in TREE #2 */
#define SMPRECPB 8  	/* OBJECT in TREE #2 */
#define SMPRECLS 7  	/* OBJECT in TREE #2 */
#define SMPRECPS 9  	/* OBJECT in TREE #2 */
#define SMPDELLB 12  	/* OBJECT in TREE #2 */
#define SMPDELPB 13  	/* OBJECT in TREE #2 */
#define SMPMDELT 14  	/* OBJECT in TREE #2 */
#define NAPOJI 37  	/* OBJECT in TREE #0 */
#define NULYI 36  	/* OBJECT in TREE #0 */
#define NAPOJA 15  	/* STRING */
#define SNIZFRI 48  	/* OBJECT in TREE #0 */
