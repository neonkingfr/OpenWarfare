#include <string.h>
#include <aes.h>
#include <ext.h>
#include <macros.h>
#include <gem\interf.h>
#include "aesglobl.h"

#if !defined __MSDOS__
#define TN(i) ( (long)&FS_T[i] )
#else
#define TN(i) ( (long)&FS_T[(int)i] )
#endif

static TEDINFO FS_T[] =
{
"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx","","",3, 6, 2, 0x1180, 0x0, -1, 41,1,
"____________________________________________","____________________________________________","PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP", 3, 6, 0, 0x1180, 0x0, -1, 45,45,
"xxxxxxxxxxx","N�zev: ________.___","FFFFFFFFFFF", 3, 6, 0, 0x1180, 0x0, -1, 12,20,
"01xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"02xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"03xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"04xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"05xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"06xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"07xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"08xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"09xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"10xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"11xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"12xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"13xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"14xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1,
"15xxxxxx.xxx","","", 3, 6, 2, 0x1180, 0x0, 0, 13,1
};

static OBJECT FS_O[] =
{
-1, 1, 22, G_BOX, NONE, OUTLINED, 0x21141L, 0,0, 52,16,
2, -1, -1, G_BOXTEXT, NONE, NORMAL, TN(0x0L), 2,1, 48,1,
21, 3, 5, G_BOX, NONE, NORMAL, 0xFF1100L, 2,3, 48,10,
4, -1, -1, G_FTEXT, EDITABLE, NORMAL, TN(0x1L), 2,1, 44,1,
5, -1, -1, G_FTEXT, EDITABLE, NORMAL, TN(0x2L), 2,2, 19,1,
2, 6, 20, G_BOX, NONE, NORMAL, 0xFF1100L, 2,4, 44,5,
7, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0x3L), 2,0, 12,1,
8, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0x4L), 2,1, 12,1,
9, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0x5L), 2,2, 12,1,
10, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0x6L), 2,3, 12,1,
11, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0x7L), 2,4, 12,1,
12, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0x8L), 16,0, 12,1,
13, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0x9L), 16,1, 12,1,
14, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0xAL), 16,2, 12,1,
15, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0xBL), 16,3, 12,1,
16, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0xCL), 16,4, 12,1,
17, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0xDL), 30,0, 12,1,
18, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0xEL), 30,1, 12,1,
19, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0xFL), 30,2, 12,1,
20, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0x10L), 30,3, 12,1,
5, -1, -1, G_BOXTEXT, 0x51, NORMAL, TN(0x11L), 30,4, 12,1,
22, -1, -1, G_BUTTON, 0x7, NORMAL, (long)"OK", 2,14, 27,1,
0, -1, -1, G_BUTTON, 0x25, NORMAL, (long)"Zru�it", 31,14, 19,1
};

#define FSELD 0  	/* TREE */
#define FSCESTT 3  	/* OBJECT in TREE #0 */
#define FSNAZT 4  	/* OBJECT in TREE #0 */
#define FSFILEST 6  	/* OBJECT in TREE #0 */
#define FSFILEET 20  	/* OBJECT in TREE #0 */
#define FSOKB 21  	/* OBJECT in TREE #0 */
#define FSCANB 22  	/* OBJECT in TREE #0 */

/* fsel */

static void FixFSel( void )
{
	static Flag Fixed=False;
	int i;
	if( Fixed ) return;
	Fixed=True;
	for( i=0; i<(int)lenof(FS_O); i++ )
	{
		rsrc_obfix(FS_O,i);
		#if __MSDOS__
		switch( FS_O[i].ob_type )
		{
			case G_BOX: case G_IBOX: case G_BOXCHAR:
				SwapObSpec(&FS_O[i].ob_spec);
			break;
		}
		#endif
	}
}

#define NFS ( FSFILEET+1-FSFILEST )

static Flag FSSelect( OBJECT *Bt, Flag Dvoj, int Asc )
{
	char Buf[20],Buf2[20],*E;
	strcpy(Buf,StrTed(Bt->ob_spec.tedinfo));
	E=strchr(Buf,'.');
	if( E ) *E++=0;
	else E="";
	sprintf(Buf2,"%-8s%-3s",Buf,E);
	SetStrTed(TI(FS_O,FSNAZT),Buf2);
	RedrawI(FS_O,FSNAZT);
	(void)Asc;
	return Dvoj;
}

static Button FSBut[]=
{
	{Unselect,FSOKB},
	{Unselect,FSCANB},
	{FSSelect,-1},
	NULL
};

#ifdef SYMANTEC /* Symantec */
int fsel_input( char *P, char *N, int *Ret )
{
	char Buf[20],Buf2[20];
	char *E;
	struct FIND *F;
	int i,r;
	FixFSel();
	SetStrTed(TI(FS_O,1),"Vyberte soubor:");
	SetStrTed(TI(FS_O,FSCESTT),P);
	strcpy(Buf,N);
	E=strchr(Buf,'.');
	if( E ) *E++=0;
	else E="";
	sprintf(Buf2,"%-8s%-3s",Buf,E);
	SetStrTed(TI(FS_O,FSNAZT),Buf2);
	for( F=findfirst(P,0),i=0; i<NFS && F; i++,F=findnext() )
	{
    OBJECT *A=&FS_O[FSFILEST+i];
		SetStrTed(A->ob_spec.tedinfo,F->name);
		Set0(A,DISABLED);
	}
	for( ; i<NFS; i++ )
	{
    OBJECT *A=&FS_O[FSFILEST+i];
		SetStrTed(A->ob_spec.tedinfo,"");
		Set1(A,DISABLED);
	}
	r=EFForm(FS_O,FSBut,NULL,FSNAZT,NULL);
	if( r==FSOKB || r>=FSFILEST && r<=FSFILEET )
	{
		strcpy(P,StrTed(TI(FS_O,FSCESTT)));
		strcpy(Buf,StrTed(TI(FS_O,FSNAZT)));
		if( strlen(Buf)>8 )
		{
			size_t l;
			strlncpy(N,Buf,9);
			for( l=7; l>=0; l++ )
			{
				if( N[l]==' ' ) N[l]=0;
				else break;
			}
			strcat(N,".");
			strcpy(N+strlen(N),Buf+8);
		}
		else strcpy(N,Buf);
		*Ret=True;
	}
	else *Ret=False;
	return 1;
}

#else

int fsel_input( char *P, char *N, int *Ret )
{
	char Buf[20],Buf2[20];
	char *E;
	struct ffblk ff;
	int i,r;
	FixFSel();
	SetStrTed(TI(FS_O,1),"Vyberte soubor:");
	SetStrTed(TI(FS_O,FSCESTT),P);
	strcpy(Buf,N);
	E=strchr(Buf,'.');
	if( E ) *E++=0;
	else E="";
	sprintf(Buf2,"%-8s%-3s",Buf,E);
	SetStrTed(TI(FS_O,FSNAZT),Buf2);
	for( r=findfirst(P,&ff,0),i=0; i<NFS && r==0; i++,r=findnext(&ff) )
	{
    OBJECT *A=&FS_O[FSFILEST+i];
		SetStrTed(A->ob_spec.tedinfo,ff.ff_name);
		Set0(A,DISABLED);
	}
	for( ; i<NFS; i++ )
	{
    OBJECT *A=&FS_O[FSFILEST+i];
		SetStrTed(A->ob_spec.tedinfo,"");
		Set1(A,DISABLED);
	}
	r=EFForm(FS_O,FSBut,NULL,FSNAZT,NULL);
	if( r==FSOKB || r>=FSFILEST && r<=FSFILEET )
	{
		strcpy(P,StrTed(TI(FS_O,FSCESTT)));
		strcpy(Buf,StrTed(TI(FS_O,FSNAZT)));
		if( strlen(Buf)>8 )
		{
			size_t l;
			strlncpy(N,Buf,9);
			for( l=7; l>=0; l++ )
			{
				if( N[l]==' ' ) N[l]=0;
				else break;
			}
			strcat(N,".");
			strcpy(N+strlen(N),Buf+8);
		}
		else strcpy(N,Buf);
		*Ret=True;
	}
	else *Ret=False;
	return 1;
}

#endif
