typedef long PackProc( long lensb, FILE *in, FILE *out );
typedef int UnpackProc( long lensb, FILE *in, FILE *out );

extern PackProc *PackProcs[];
extern UnpackProc *UnpackProcs[];

PackProc EncodeSS;
UnpackProc DecodeSS,TestSS;
