#ifndef macros_h_included
#define macros_h_included

typedef unsigned short word;
typedef unsigned char byte;
typedef unsigned int lword;

typedef enum flag {False,True} Flag;

#define lenof(a) sizeof(a)/sizeof(*a)

typedef char PathName[1024];

#endif
