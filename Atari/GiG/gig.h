#ifndef gig_h__
#define gig_h__


/**@fn present_gig
@param shotObr screen data, 32b RGBA or 8b indexed color (palette)
@param scrBpp should be 32b for RGBA data, or 8b for palette data
@param scrWidth screen buffer width
@param scrHeight screen buffer height
*/

/** @fn sound_gig
@param buffer destination buffer to fill. Data are expected as stereo (2 channels) 16b signed integer
@param bufferSize size of the destination buffer
@param bufferFreq frequency which will be used for playback
@return number of bytes filled. Game should fill as much as possible, buf does not have to fill the complete buffer. Game must always fill complete 2 channels samples.
*/

#define GIG_HOST(XXXXX) \
  XXXXX(void,present_gig,( unsigned long *shotObr, int scrWidth, int scrHeight, int scrBpp, unsigned int *shotPalette ))\
  XXXXX(void,mouseState_gig,(int *x, int *y, int *buttons, int *wheel))

#define GIG_GAME(XXXXX) \
  XXXXX(HANDLE,init_gig, (HWND hwnd, struct GiG_Interface *gig, const char *currentDirectory)) \
  XXXXX(void,name_gig, (char *buf, size_t bufSize)) \
  XXXXX(void,shutDown_gig, ()) \
  XXXXX(void,onKeyDown_gig, (WPARAM wParam)) \
  XXXXX(void,onKeyUp_gig, (WPARAM wParam)) \
  XXXXX(size_t,sound_gig, (void *buffer, size_t bufferSize, int bufferFreq))


#define VK_CUSTOM_PAUSE (VK_ESCAPE|(0x80000000))
#define VK_CUSTOM_CONTINUE (VK_SPACE|(0x80000000))
#define VK_CUSTOM_QUIT (VK_PAUSE|(0x80000000))

#define GIG_VERSION 2

#define GIG_DEFINE_FUNC_PTR(ret,name,args) ret (__stdcall *name) args;
struct GiG_Interface
{
  int version;
  // implemented on the host side
  GIG_HOST(GIG_DEFINE_FUNC_PTR)
};

#endif // gig_h__
