/* konvertor obrazku */
/* SUMA 3/1993 */

#include <macros.h>
#include <string.h>
#include <stdlib.h>
#include <stdc\fileutil.h>
#include <stdc\imgload.h>
#include <stdc\memspc.h>
#include "obrazky.h"

#define JAG 1 /* organizace pixelu */

static void JagRGB555( Obrazek *Obr )
{
	long Cnt=(long)Obr->W*Obr->H;
	word *B=Obr->Obr;
	while( --Cnt>=0 )
	{
		word s=*B;
		word r=s>>11;
		word g=(s>>6)&31;
		word b=(s)&31;
		word d=(r<<11)|(b<<6)|(g<<1)|1;
		*B++=d;
	}
}

int main( int argc, const char *argv[] )
{
	PathName S,D;
	if( argc==2 )
	{
		Obrazek Obr;
		argv++;
		strcpy(S,*argv);
		strupr(S);
		strcpy(D,S);
		#if JAG
			if( NactiTC16(S,&Obr)>=0 )
			{
				JagRGB555(&Obr);
				strcpy(NajdiPExt(NajdiNazev(D)),".PJC");
				printf("Destination: %s %dx%dxRGB15.\n",D,Obr.W,Obr.H);
				if( SaveRAWPCC(D,Obr.Obr,Obr.W,Obr.H)>=0 )
				{
					return 0;
				}
			}
		#else
			if( NactiTC16(S,&Obr)>=0 )
			{
				strcpy(NajdiPExt(NajdiNazev(D)),".PCC");
				printf("Destination: %s %dx%dx16b.\n",D,Obr.W,Obr.H);
				if( SavePCC(D,Obr.Obr,Obr.W,Obr.H)>=0 )
				{
					return 0;
				}
			}
		#endif
	}
	return -1;
}

