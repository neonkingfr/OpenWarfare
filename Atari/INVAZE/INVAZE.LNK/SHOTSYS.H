extern void *VRam[3];
extern byte *VRamFree;
extern void *NxVRam;

void *InitVRam( void );
void RestVRam( void );
void *Shot( void );
