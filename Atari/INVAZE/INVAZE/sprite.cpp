#include "sprite.h"
#include <memory.h>

static void TiskPis(int x, word *VA, int c)
{
  byte *fontStart = Font+0x800+c;
  int i;

  int xw = x>>4;
  int xAlign = 15-(x&15);
  int of = 0;
  int ov = xw*4;

  for (i=16; --i>=0; )
  {
    int fontData = fontStart[of];

    int fontH = (fontData<<xAlign)&0xffff;
    int fontL = ((fontData<<xAlign)>>16)&0xffff;
    VA[ov+0] &= ~fontL;
    VA[ov+1] &= ~fontL;
    VA[ov+2] &= ~fontL;
    VA[ov+3] &= ~fontL;
    VA[ov+4+0] &= ~fontH;
    VA[ov+4+1] &= ~fontH;
    VA[ov+4+2] &= ~fontH;
    VA[ov+4+3] &= ~fontH;
    of += 256;
    ov += 80;
  }
}

void TiskS( word *VAdr, int x, char *Str ) // TODO: pass x into the function
{
  x *= 16;

  while (*Str)
  {
    TiskPis(x, VAdr, *Str++);
    x+= 8;
  }
}

static void TiskPisG(int x, word *VA, int c)
{
  byte *fontStart = Font+0x800+c;
  int i;

  int xw = x>>4;
  int xAlign = 15-(x&15);
  int of = 0;
  int ov = xw*4;

  for (i=16; --i>=0; )
  {
    int fontData = fontStart[of];

    int fontH = (fontData<<xAlign)&0xffff;
    int fontL = ((fontData<<xAlign)>>16)&0xffff;
    VA[ov+0] |= fontL;
    VA[ov+1] |= fontL;
    VA[ov+2] |= fontL;
    VA[ov+3] |= fontL;
    VA[ov+4+0] |= fontH;
    VA[ov+4+1] |= fontH;
    VA[ov+4+2] |= fontH;
    VA[ov+4+3] |= fontH;
    of += 256;
    ov += 80;
  }
}

void TiskSG( int x, word *VAdr, char *Str )
{
  x >>= 1;
  x -= 8;

  while (*Str)
  {
    TiskPisG(x, VAdr, *Str++);
    x+= 8;
  }
}

static void TiskPisGO(int x, word *VA, int c)
{
  byte *fontStart = Font+0x800+c;
  int i;

  int xw = x>>4;
  int xAlign = 15-(x&15);
  int of = 0;
  int ov = xw*4;

  for (i=16; --i>=0; )
  {
    int fontData = fontStart[of];
    int backData = 0xff;

    int fontH = (fontData<<xAlign)&0xffff;
    int fontL = ((fontData<<xAlign)>>16)&0xffff;
    int backH = (backData<<xAlign)&0xffff;
    int backL = ((backData<<xAlign)>>16)&0xffff;
    VA[ov+0] |= backL;
    VA[ov+1] |= backL;
    VA[ov+2] |= backL;
    VA[ov+3] |= backL;
    VA[ov+4+0] |= backH;
    VA[ov+4+1] |= backH;
    VA[ov+4+2] |= backH;
    VA[ov+4+3] |= backH;
    VA[ov+0] &= ~fontL;
    VA[ov+1] &= ~fontL;
    VA[ov+2] &= ~fontL;
    VA[ov+3] &= ~fontL;
    VA[ov+4+0] &= ~fontH;
    VA[ov+4+1] &= ~fontH;
    VA[ov+4+2] &= ~fontH;
    VA[ov+4+3] &= ~fontH;
    of += 256;
    ov += 80;
  }
}

void TiskSGO( int x, word *VAdr, char *Str )
{
  x >>= 1;
  x -= 8;

  while (*Str)
  {
    TiskPisGO(x, VAdr, *Str++);
    x+= 8;
  }
}


void Sups( void *Obsah )
{
  memcpy(VRA,Obsah,VRAMSize);
}

void Prolni( void *T, void *s, void *S, word *R0 )
{
  word *TW = reinterpret_cast<word *>(T);
  word *sW = reinterpret_cast<word *>(s);
  word *SW = reinterpret_cast<word *>(S);
  // s old source
  // S new source
  // R0 raster
  int x,y;
  int lineWordPitch = 320/16*4;
  for (y=0; y<200; y++)
  {
    int yr = y&15;
    word my = R0[yr];
    for (x=0; x<320/16*4; x++)
    {
      int offset = y*lineWordPitch+x;
      TW[offset] = (sW[offset]&my)|(SW[offset]&~my);
    }
  }
}

void Recolor( word *BP, lword *SP )
{
}

word RozsirB[256];

word *VRA;
byte Font[0x1800+0x80*3];


void present_shot( unsigned long *shotObr, int scrWidth, int scrHeight, int scrBpp, unsigned int *shotPalette );

void SetVsync( int n, void *VA )
{
  // convert to a GiG formats (i.e. 8bpp + 256 color palette)
  static byte shotObr[320*200];
  static unsigned int shotPalette[256];
  // input is 320x200 in 4 interleaved 8b bitplanes
  // take all bitplanes, compose 8 pixels
  word *VAB = reinterpret_cast<word *>(VA);
  byte *SOB = shotObr;
  int h,w,i;
  for (h=0; h<200; h++) for (w=0; w<320/16; w++)
  {
    word b3 = *VAB++;
    word b2 = *VAB++;
    word b1 = *VAB++;
    word b0 = *VAB++;
    for (i=0; i<16; i++)
    {
      int p = 0;
      p |= (b0&0x8000)!=0;
      p <<=1;
      p |= (b1&0x8000)!=0;
      p <<=1;
      p |= (b2&0x8000)!=0;
      p <<=1;
      p |= (b3&0x8000)!=0;
      b0 <<= 1;
      b1 <<= 1;
      b2 <<= 1;
      b3 <<= 1;
      *SOB++ = p;
    }
  }

  // convert palette from 16 b (rgb333?) to 24 b (argb8888)
  for (i=0; i<16; i++)
  {
    extern word SetPaletteData[16];
    word rgb333 = SetPaletteData[i];
    int r3 = (rgb333>>8)&7;
    int g3 = (rgb333>>4)&7;
    int b3 = (rgb333>>0)&7;
    int r8 = (int)((float)r3*255/7+0.5f);
    int g8 = (int)((float)g3*255/7+0.5f);
    int b8 = (int)((float)b3*255/7+0.5f);
    shotPalette[i] = (r8<<16)|(g8<<8)|b8;
  }
  present_shot((unsigned long *)shotObr,320,200,8,shotPalette);
}


void Save16( int x, int h, Flag Write, word *VAdr, word *SAdr )
{
  VAdr += (x>>5)<<2;

  h>>=1;

  if (!Write)
  {
    while (--h>=0)
    {
      int i;
      for (i=0; i<8; i++) *SAdr++ = *VAdr++;
      VAdr += (BPL*2-16)/2;
    }
  }
  else
  {
    while (--h>=0)
    {
      int i;
      for (i=0; i<8; i++) *VAdr++ = *SAdr++;
      VAdr += (BPL*2-16)/2;
    }
  }
}

static int InRange(int x)
{
  if (x<0)
  {
    return 0;
  }
  if (x>=BPL)
  {
    return 0;
  }
  return 1;
}
void Draw16( int x, word *VAdr, Sprite *Def )
{
  int H = Def->s_h>>1;
  int W = Def->s_w;
  int i,l;

  int xOff = (x>>5)<<2;
  x = (x&31)>>1;

  {
    word *FAdr = (word *)Def->Image[0];
    word *MAdr = (word *)Def->Mask[0];

    x = 16-x;
    while (H-->0)
    {
      int wCount = W;
      //int wCount = W-1;
      for (l=0; l<wCount; l++)
      {
        for (i=0; i<4; i++)
        {
          lword Mask = ~(MAdr[l*4+i]<<x);
          word g0 = InRange(xOff+l*4) ? VAdr[l*4+xOff+i] : 0;
          word g1 = InRange(xOff+l*4+4) ? VAdr[l*4+xOff+i+4] : 0;
          lword w1 = ((lword)g0<<16)|g1;
          w1 &= Mask;
          w1 |= FAdr[l*4+i]<<x;
          if (InRange(xOff+l*4)) VAdr[l*4+xOff+i] = w1>>16;
          if (InRange(xOff+l*4+4)) VAdr[l*4+xOff+i+4] = (word)w1;
        }
      }
      MAdr += wCount*4;
      FAdr += wCount*4;
      VAdr += BPL;
    }
  }
}

void Draw( int x, word *VAdr, Sprite *Def )
{
  Draw16(x,VAdr,Def);
}


void Draw128( int x, word *VAdr, Sprite *Def )
{
  Draw16(x,VAdr,Def);
}

void Bar8( int x, word *VAdr, word *EAdr, lword Obsah, lword Maska )
{
  word *tgt = VAdr+((x>>5)<<2);
  int align = (x>>1)&15;
  Obsah <<= align;
  Maska = ~(~Maska>>align);
  while (tgt<EAdr)
  {
    int i;
    for (i=0; i<4; i++)
    {
      lword td = (tgt[i]<<16)|tgt[i+4];
      td = td&Maska|Obsah;
      tgt[i] = td>>16;
      tgt[i+4] = td&0xffff;
    }
    tgt += BPL;
  }

}

