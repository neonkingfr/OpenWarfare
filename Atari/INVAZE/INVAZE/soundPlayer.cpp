#include <string.h>
#include <assert.h>
#include <limits.h>
#include "msgPass.h"
#include "soundPlayer.h"
#include "zvuky.h"
#include "NASTROJE.H"
#include "HUDBA.H"


/* DSP-like sound engine for Gravon */


/// DSP-like interface

enum MessageType
{
  MT_setbuffer,
  MT_setloop,
};

struct MessageBase
{
  MessageType type;
  MessageBase(MessageType type):type(type){}
};

struct MessageSetbuffer: MessageBase
{
  const struct SoundWave *sound;
  int channel;
  float freq;
  int loop;
  MessageSetbuffer(const struct SoundWave *sound, int channel, float freq, int loop):MessageBase(MT_setbuffer),sound(sound),channel(channel),freq(freq),loop(loop){}
};

struct MessageSetLoop: MessageBase
{
  int channel;
  int loop;
  MessageSetLoop(int channel, int loop):MessageBase(MT_setloop),channel(channel),loop(loop){}
};

void setbuffer(int channel, float freq, const struct SoundWave *sound, int loop)
{
  // changing the buffer needs to be atomic
  MessageSetbuffer msg(sound,channel,freq,loop);
  SendMsg(&msg,sizeof(msg));
}

void setloop(int channel, int looped)
{
  MessageSetLoop msg(channel,looped);
  SendMsg(&msg,sizeof(msg));
}


int IsBufferDone(int channel)
{
  return !kanaly[channel].busy;
}

static int GameSound()
{
  int accum = 0;
  // Game sound effects
  for (int i=0; i<NKanaly; i++)
  {
    Kanal &kan = kanaly[i];
    if (!kan.snd || kan.Krk==0) continue;
    size_t bufferOffset = 0;
    int sSize = kan.snd->format.wBitsPerSample/8;

    int sample = int(kan.Pos>>32);
    int offset = sample*sSize;

    long long nPos = kan.Pos + kan.Krk;
    if (kan.loop && (nPos>>32)>=kan.snd->LoopEnd)
    {
      nPos -= long long(kan.snd->LoopEnd-kan.snd->LoopBeg)<<32;
    }
    if (offset+sSize<=(int)kan.snd->dataSize)
    {
      if (kan.snd->format.nChannels==1)
      {
        short sampleData;
        switch (kan.snd->format.wBitsPerSample)
        {
          case 16:
            sampleData = *(short *)((char *)kan.snd->data+offset);
            break;
          case 8:
            sampleData = (*((byte *)kan.snd->data+offset)-128)<<8;
            break;
        }
        accum += sampleData;
      }
      kan.Pos = nPos;
    }
    else
    {
      kan.busy = false;
    }

  }
  return accum;
}

static inline short Sat16b( int x ) 
{
  if (x<SHRT_MIN) return SHRT_MIN;
  if (x>SHRT_MAX) return SHRT_MAX;
  return x;
}

const int sampleSizeTgt = sizeof(short)*2;

// function called by the low-level sound engine
static size_t SoundPlayerCallback(void *buffer, size_t bufferSize, int bufferFreq)
{
  char message[512];
  while(size_t rd = GetMsg(message,sizeof(message)))
  {
    // process the message
    const MessageBase &msg = *(const MessageBase*)message;
    switch (msg.type)
    {
      case MT_setbuffer:
      {
        const MessageSetbuffer &m = static_cast<const MessageSetbuffer &>(msg);
        kanaly[m.channel].Pos = 0;
        kanaly[m.channel].Krk = m.sound ? long long((m.freq*m.sound->format.nSamplesPerSec)*(1ULL<<32)/bufferFreq) : 0;
        kanaly[m.channel].snd = m.sound;
        kanaly[m.channel].loop = m.loop;
        break;
      }
      case MT_setloop:
      {
        const MessageSetLoop &m = static_cast<const MessageSetLoop &>(msg);
        kanaly[m.channel].loop = m.loop;
        break;
      }
      default:
        assert(false); // unexpected message type
    }
  }
  
  {
    size_t bufferOffset = 0;
    while (bufferOffset<bufferSize)
    {
      int accum = PlayMusicSample(bufferFreq);
      accum += GameSound();
      // Music
      short *bufTgt = (short *)((char *)buffer+bufferOffset);
      bufTgt[0] = Sat16b(accum);
      bufTgt[1] = Sat16b(accum);
      bufferOffset += sampleSizeTgt;
    }
  }
  return bufferSize;
}

void StartSound()
{
}

void EndSound()
{
}

extern "C" __declspec(dllexport) size_t __stdcall sound_gig(void *buffer, size_t bufferSize, int bufferFreq)
{
  return SoundPlayerCallback(buffer,bufferSize,bufferFreq);
}
