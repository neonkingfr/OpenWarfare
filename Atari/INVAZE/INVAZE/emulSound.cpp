#include "macros.h"
#include "st_input.h"
#include "zvuky.h"
#include "soundPlayer.h"

void InitPlay(void)
{
  // expect InitKan / EndKan
}

void LastLoopP()
{
  setloop(0,0);
}

void InitP( struct SoundWave *M )
{
  if (!M)
  {
    // end playing
  }
  else
  {
    // always use channel 0
    setbuffer(0,1.0f,M,True);
    kanaly[0].busy = 1;
  }
}

void EndPlay( void )
{
  int i;
  for (i=0; i<NKanaly; i++)
  {
    setbuffer(i,1,NULL,False);
    kanaly[i].busy = 0;
  }
}

Kanal kanaly[NKanaly];

int InitKanF(struct SoundWave *Co, Flag looped)
{
  int i;
  if (!Co->data) return -1;
  // find a free channel
  for (i=0; i<NKanaly; i++)
  {
    if (!kanaly[i].busy)
    {
      setbuffer(i,1.0f,Co,looped);
      kanaly[i].busy = 1;
      return i;
    }
  }
  return -1;
}

int InitKanLooped(struct SoundWave *Co)
{
  return InitKanF(Co,True);
}

int InitKan(struct SoundWave *Co)
{
  return InitKanF(Co,False);
}

void EndKan( int K )
{
  if (K<0) return;
  kanaly[K].snd = NULL;
  kanaly[K].Pos = 0;
  kanaly[K].Krk = 0;
  kanaly[K].busy = 0;
}


int *NstrV[4], *NstrF[4];
int Fr[4], FVb[4],Vo[4], Plac[4], Bub[4], PlacT[4];

struct SoundWave Dup1;
struct SoundWave Dup2;
struct SoundWave DelVyst;
struct SoundWave UFOVyst;
struct SoundWave Vybuch;
struct SoundWave Pulec;
struct SoundWave BuchS;
struct SoundWave BuchUF;
struct SoundWave Ml, Bon, Let;

struct RiffChunk
{
  lword id;
  lword len;
};

#define RIFF_FOURCC_C(ch0, ch1, ch2, ch3)  \
    ((lword)(byte)(ch0) | ((lword)(byte)(ch1) << 8) |  \
    ((lword)(byte)(ch2) << 16) | ((lword)(byte)(ch3) << 24 ))


#define RIFF_FOURCC RIFF_FOURCC_C('R','I','F','F')
#define RIFF_WAVE RIFF_FOURCC_C('W','A','V','E')
#define RIFF_fmt RIFF_FOURCC_C('f','m','t',' ')
#define RIFF_data RIFF_FOURCC_C('d','a','t','a')
#define RIFF_fact RIFF_FOURCC_C('f','a','c','t')
#define RIFF_smpl RIFF_FOURCC_C('s','m','p','l')

#define WAVE_FORMAT_PCM     1

struct sampler_loop
{
  lword CuePointID;
  lword Type;
  lword Start;
  lword End;
  lword Fraction;
  lword PlayCount;
};

struct sampler_chunk
{ // size = 36 + (Num Sample Loops * 24) + Sampler Data - see http://www.sonicspot.com/guide/wavefiles.html
  lword	Manufacturer;
  lword	Product;
  lword	SamplePeriod;
  lword	MIDIUnityNote;
  lword	MIDIPitchFraction;
  lword	SMPTEFormat;
  lword	SMPTEOffset;
  lword	NumSampleLoops;
  lword	SamplerData;
};

static int ProcessLoopChunk(struct RiffChunk *chunk, long *LoopBeg,long *LoopEnd, FILE *f)
{
  if (chunk->id==RIFF_smpl && chunk->len>=sizeof(struct sampler_chunk))
  {
    struct sampler_chunk smpl;
    struct sampler_loop loop;
    if (fread(&smpl,sizeof(smpl),1,f)!=1) return -1;
    if (smpl.NumSampleLoops*sizeof(struct sampler_loop)+sizeof(struct sampler_chunk)>chunk->len) return -1;
    // we expect one loop only
    if (smpl.NumSampleLoops<1) return 1;
    if (fread(&loop,sizeof(loop),1,f)!=1) return -1;
    *LoopBeg = loop.Start;
    *LoopEnd = loop.End;
    fseek(f,chunk->len-sizeof(struct sampler_loop)-sizeof(struct sampler_chunk),SEEK_CUR);

    return 1;
  }
  return 0;
}


void LoadSound(struct SoundWave *snd, const char *filename)
{
  // check file header
  FILE *f = fopen(filename,"rb");
  struct RiffChunk chunk;
  struct wave_WAVEFORMATEX fmt;
  long loopBeg = 0, loopEnd = 0;
  lword fformat;
  if (!f) return;
  if (fread(&chunk,sizeof(chunk),1,f)!=1) goto Error;
  if (chunk.id!=RIFF_FOURCC) goto Error;
  if (fread(&fformat,sizeof(fformat),1,f)!=1) goto Error;
  if (fformat!=RIFF_WAVE) goto Error;

  // process chunks
  // search for fmt chunk
    
  // Search the input file for for the 'fmt ' chunk.

  for(;;)
  {
    if (fread(&chunk,sizeof(chunk),1,f)!=1) goto Error;
    if (chunk.id!=RIFF_fmt)
    {
      // skip chunk
      fseek(f,chunk.len,SEEK_CUR);
      continue;
    }
    // check if it is valid
    if (chunk.len<sizeof(struct wave_WAVEFORMATEX))
    {
      if (chunk.len<sizeof(struct wave_WAVEFORMAT))
      {
        goto Error;
      }
      else
      {
        // try to read old PCM format
        struct wave_WAVEFORMAT fmt_old;
        if (fread(&fmt_old,sizeof(fmt_old),1,f)!=1) goto Error;
        fseek(f,chunk.len-sizeof(fmt_old),SEEK_CUR); // skip rest of chunk
        if (fmt_old.wFormatTag!=WAVE_FORMAT_PCM) goto Error;
        fmt.wFormatTag = fmt_old.wFormatTag;
        fmt.nAvgBytesPerSec = fmt_old.nAvgBytesPerSec;
        fmt.nBlockAlign = fmt_old.nBlockAlign;
        fmt.nChannels = fmt_old.nChannels;
        fmt.nSamplesPerSec = fmt_old.nSamplesPerSec;
        fmt.wBitsPerSample = fmt_old.wBitsPerSample;
        fmt.cbSize = 0;
        break;
      }
    }
    else
    {
      // read format
      if (!fread(&fmt,sizeof(fmt),1,f)!=1) goto Error;
      if (fmt.wFormatTag!=WAVE_FORMAT_PCM ) goto Error;
      if (chunk.len!=sizeof(struct wave_WAVEFORMATEX))
      {
        goto Error;
      }
      fmt.cbSize=0;
      break;
    }
  }

  // TODO: process LOOPs

  // now read the data
  for(;;)
  {
    if (fread(&chunk,sizeof(chunk),1,f)!=1) goto Error;
    else if (chunk.id==RIFF_data)
    {
      snd->data = malloc(chunk.len);
      if (!snd->data) goto Error;
      if (fread(snd->data,chunk.len,1,f)!=1) goto Error;
      snd->format = fmt;
      snd->dataSize = chunk.len;
      snd->LoopBeg = loopBeg;
      snd->LoopEnd = loopEnd;
      break;
    }
    else
    {
      // skip chunk
      int ret = ProcessLoopChunk(&chunk,&loopBeg,&loopEnd,f);
      if (ret<0) goto Error;
      if (ret==0) fseek(f,chunk.len,SEEK_CUR);
      continue;
    }
  }

  // skip the rest of the data, search for loop markers
  for(;;)
  {
    int ret;
    if (fread(&chunk,sizeof(chunk),1,f)!=1) break;
    ret = ProcessLoopChunk(&chunk,&snd->LoopBeg,&snd->LoopEnd,f);
    if (ret<0) goto Error;
    if (ret==0) fseek(f,chunk.len,SEEK_CUR);
  }
  Error:
  fclose(f);
}


#define WD "Invaze.DTA\\"

Flag LoadSounds()
{
  LoadSound(&Dup1,WD "dup1.wav");
  LoadSound(&Dup2,WD "dup2.wav");
  LoadSound(&DelVyst,WD "fire.wav");
  LoadSound(&UFOVyst,WD "ufo-fire.wav");
  LoadSound(&Vybuch,WD "hit-delo.wav");
  LoadSound(&Let,WD "launch.wav");
  LoadSound(&Bon,WD "win.wav");
  LoadSound(&Ml,WD "jozue.wav");
  LoadSound(&Pulec,WD "pulec.wav");
  LoadSound(&BuchUF,WD "ufo-hit.wav");
// Pulec;
// BuchS;
// BuchUF;
// Ml, Bon, Let;
  StartSound();
  return True;
}
