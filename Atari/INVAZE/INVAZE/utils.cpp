#include <string.h>
#include <stdlib.h>
#include <io.h>
#include "macros.h"
#include "st_input.h"

#include "sprite.h"
#include "utils.h"

Flag Mono;

static word _Rozsir( word w )
{
  #define b(i) (1<<i)
  #define r(a,i) ((a&b(i))<<i)
  return r(w,0)|r(w,1)|r(w,2)|r(w,3)|r(w,4)|r(w,5)|r(w,6)|r(w,7);
  #undef b
  #undef r
}

void __inline SwapEndianL(lword *xx)
{
  int x = *xx;
  *xx = (((x)>>24)&0xff) | (((x)>>8)&0xff00) | (((x)<<8)&0xff0000) | (((x)<<24)&0xff000000);
}
void __inline SwapEndianW(word *xx)
{
  int x = *xx;
  *xx = (((x)>>8)&0xff) | (((x)<<8)&0xff00);
}

void SwapEndianWBuffer(word *x, size_t size)
{
  while (size>0)
  {
    SwapEndianW(x++);
    size-= sizeof(word);
  }
}

int LoadPc( word *Scr, word *Pal, const char *name )
{
  static byte Buf[VRAMSize];
  int FIn = Fopen(name,0);
  int Rez=Mono ? 2 : 0;
  int Offset=(1<<Rez)*20;
  int NPl=1<<(2-Rez);
  int ScanLine;
  byte Buffer[160];
  byte *FBuf;
  long Rd;
  
  if (FIn<0) return -1;
  Rd=Fread(FIn,2,Pal);
  // ignored (Degas header)
  if( Rd<2 ) goto Error;
  Rd=Fread(FIn,32,Pal);
  if( Rd<32 ) goto Error;
  SwapEndianWBuffer(Pal,Rd);
  Rd=Fread(FIn,sizeof(Buf),Buf);
  if( Rd<0 ) goto Error;
  FBuf=Buf;
  for( ScanLine=0; ScanLine<200; ScanLine++ )
  {
    byte *BP;
    int Dat;
    int off,i;
    
    for( BP=Buffer; BP<&Buffer[160]; )
    {
      Dat=*FBuf++;
      if( Dat<128 )
      {
        for( ; Dat>=0; Dat-- ) *BP++=*FBuf++;
      }
      else
      {
        byte Mem=*FBuf++;
        
        for( Dat=256-Dat; Dat>=0; Dat-- ) *BP++=Mem;
      }
      if( FBuf>Buf+Rd ) goto Error;
    }
    // swizzle bitplanes to interleaved byte order
    for( off=0; off<Offset; off++,Scr+=NPl )
    for( i=0; i<NPl; i++ )
    {
      word scrI = ((word *)Buffer)[i*Offset+off];
      SwapEndianW(&scrI);
      Scr[i]=scrI;
    }
  }
  Fclose(FIn);
  return 0;
  Error:
  Fclose(FIn);
  return -1;
  #undef Rez
}

static void GetImage( lword *I, lword *M, int w, int h, lword *SAdr )
{
  int xx,yy;
  #define IW ((word *)I)
  #define MW ((word *)M)
  #define SW ((word *)SAdr)
  
  if( !Mono )
  {
    w*=4;
    h/=2;
    for( yy=0; yy<h; yy++ )
    {
      for( xx=0; xx<w-4; xx++ )
      {
        IW[yy*w+xx]=SW[yy*80+xx];
      }
      IW[yy*w+w-3]=IW[yy*w+w-2]=0;
      IW[yy*w+w-2]=IW[yy*w+w-1]=0;
    }
    for( yy=0; yy<h; yy++ ) for( xx=0; xx<w; xx+=4 )
    {
      int Off=yy*w+xx;
      word p=IW[Off]|IW[Off+1]|IW[Off+2]|IW[Off+3];
      
      MW[Off]=MW[Off+1]=MW[Off+2]=MW[Off+3]=p;
    }
  }
  else
  {
    for( yy=0; yy<h; yy++ ) 
    {
      for( xx=0; xx<w-1; xx++ )
      {
        I[yy*w+xx]=SAdr[yy*20+xx];
      }
      I[yy*w+w-1]=0;
    }
  }
}

static void GetMask( lword *M, int w, int h, lword *SAdr )
{ /* jen Mono */
  int xx,yy;
  
  for( yy=0; yy<h; yy++ ) 
  {
    for( xx=0; xx<w-1; xx++ )
    {
      M[yy*w+xx]=SAdr[yy*20+xx];
    }
    M[yy*w+w-1]=0L;
  }
}

void SetSpr( Sprite *S, SInfo *SI, word *SAdr )
{
  int i;
    
  if( Mono )
  {
    GetImage
    (
      S->Image[0], S->Mask[0], S->s_w, S->s_h,
      (lword *)(ycoor(SAdr,SI->FY*2)+SI->FX)
    );
    if( S->faze ) for( i=1; i<NFaz; i++ )
    {
      memcpy(S->Image[i],S->Image[0],S->s_w*S->s_h*sizeof(long));
    }
  }
  else
  {
    GetImage
    (
      S->Image[0],S->Mask[0],
      S->s_w, S->s_h,
      (lword *)(ycoor(SAdr,SI->FY*2)+SI->FX*2)
    );
    if( S->faze ) for( i=1; i<NFaz; i++ )
    {
      memcpy(S->Image[i],S->Image[0],S->s_w*S->s_h*sizeof(long));
      memcpy(S->Mask [i],S->Mask [0],S->s_w*S->s_h*sizeof(long));
    }
  }
}
void SetMask( Sprite *S, SInfo *SI, word *SAdr )
{
  int i;
  
  GetMask
  (
    S->Mask[0], S->s_w, S->s_h,
    (lword *)(ycoor(SAdr,SI->FY*2)+SI->FX)
  );
  if( S->faze ) for( i=1; i<NFaz; i++ )
  {
    memcpy(S->Image[i],S->Image[0],S->s_w*S->s_h*sizeof(long));
    memcpy(S->Mask [i],S->Mask [0],S->s_w*S->s_h*sizeof(long));
  }
}

static void _RotR( lword *I, int n, int w, int h )
{
  if( n>0 )
  {
    int j;
    lword Mask=(2L<<n)-1;
    
    for( j=0; j<h; j++ )
    {
      lword Zb=0;
      
      for( lword *W = &I[j*w]; W<&I[j*w+w]; W++ )
      {
        lword NZb=Zb<<(32-n);
        
        Zb=*W&Mask;
        *W=(*W>>n)|NZb;
      }
    }
  }
}
static void _RotRC( word *I, int n, int w, int h )
{
  if( n>0 )
  {
    int j;
    word Mask=(2<<n)-1;
    
    for( j=0; j<h; j++ )
    {
      int pl;
      for( pl=0; pl<4; pl++ )
      {
        word Zb=0;
        for( word *W = &I[j*w+pl]; W<&I[j*w+w+pl]; W+=4 )
        {
          word NZb=Zb<<(16-n);
          Zb=*W&Mask;
          *W=(*W>>n)|NZb;
        }
      }
    }
  }
}

static void Tilda( lword *l, long s )
{
  for( ; s>0; s-- ) *l++=~*l;
}
void InitSpr( Sprite *U )
{
  /*
  int i;
  
  if( Mono )
  {
    if( U->faze ) for( i=0; i<NFaz; i++ )
    {
      _RotR(U->Image[i],i,U->s_w,U->s_h);
      _RotR(U->Mask [i],i,U->s_w,U->s_h);
      Tilda(U->Mask [i],U->s_w*U->s_h);
    }
  }
  else
  {
    if( U->faze ) for( i=0; i<NFaz; i++ )
    {
      _RotRC((word *)U->Image[i],i,U->s_w*4,U->s_h/2);
      _RotRC((word *)U->Mask [i],i,U->s_w*4,U->s_h/2);
      Tilda(U->Mask [i],U->s_w*U->s_h);
    }
  }
  */
}

word Rnd( word Rng )
{
  int ret;
  float r=rand()/(float)RAND_MAX;
  
  Assert( Rng<=0, "Rnd domain error");
  ret=(int)( r*Rng );
  if( ret>=Rng ) ret=Rng-1;
  return ret;
}

