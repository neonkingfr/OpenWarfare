extern byte VRamFree[0x10000L+0x100];
extern void *VRam[2];
extern void *NxVRam;
extern int NumVRam;

word *InitVRam( void );
void RestVRam( void );
word *Shot( void );
