#include <string.h>
#include <stdlib.h>
#include "macros.h"
#include "st_input.h"

#include "zvuky.h"
#include "sprite.h"
#include "shotsys.h"
#include "utils.h"
#include "invaze.h"
#include "soundPlayer.h"

#define SL sizeof(long)

enum
{
  UfoW=3,
  UfoHX=32,
  Ufo6HX=16,
  Ufo1H=25*2,
  Ufo2H=22*2,
  Ufo3H=19*2,
  Ufo4H=17*2,
  Ufo5H=15*2,
  Ufo6H=13*2,
  Ufo7H=10*2,
  Ufo8H=8*2,
  Ufo1HY=Ufo1H/2,
  Ufo2HY=Ufo2H/2,
  Ufo3HY=Ufo3H/2,
  Ufo4HY=Ufo4H/2,
  Ufo5HY=Ufo5H/2,
  Ufo6HY=Ufo6H/2,
  Ufo7HY=Ufo7H/2,
  Ufo8HY=Ufo8H/2,
  Ufo1S=Ufo1H*UfoW*sizeof(long),
  Ufo2S=Ufo2H*UfoW*sizeof(long),
  Ufo3S=Ufo3H*UfoW*sizeof(long),
  Ufo4S=Ufo4H*UfoW*sizeof(long),
  Ufo5S=Ufo5H*UfoW*sizeof(long),
  Ufo6S=Ufo6H*UfoW*sizeof(long),
  Ufo7S=Ufo7H*UfoW*sizeof(long),
  Ufo8S=Ufo8H*UfoW*sizeof(long),
  
  RakW=2,
  RakH=8*2,
  RakEW=11,
  RakHX=16,
  RakHY=RakH/2,
  RakS=RakW*RakH*sizeof(long),

  LodW=3,
  LodH=20*2,
  LodS=LodW*LodH*SL,
  LEW=64,
  LodHX=LEW/2,
  LodHY=LodH/2,
};

static lword Ufo1I[NFaz][Ufo1S/SL],Ufo1M[NFaz][Ufo1S/SL];
static lword Ufo2I[NFaz][Ufo2S/SL],Ufo2M[NFaz][Ufo2S/SL];
static lword Ufo3I[NFaz][Ufo3S/SL],Ufo3M[NFaz][Ufo3S/SL];
static lword Ufo4I[NFaz][Ufo4S/SL],Ufo4M[NFaz][Ufo4S/SL];
static lword Ufo5I[NFaz][Ufo5S/SL],Ufo5M[NFaz][Ufo5S/SL];
static lword Ufo6I[NFaz][Ufo6S/SL],Ufo6M[NFaz][Ufo6S/SL];
static lword Ufo7I[NFaz][Ufo7S/SL],Ufo7M[NFaz][Ufo7S/SL];
static lword Ufo8I[NFaz][Ufo8S/SL],Ufo8M[NFaz][Ufo8S/SL];

enum { NFazPul=8 };

static lword PulI[NFazPul][PulS/SL],PulM[NFazPul][PulS/SL];

static lword StrI[4][StrS/SL],StrM[4][StrS/SL];

static lword DelI[NFaz][DelS/SL],DelM[NFaz][DelS/SL];

static lword SudI[SudS/SL],SudM[SudS/SL];
static lword LodI[LodS/SL],LodM[LodS/SL];

enum {RUh=5};

static lword Rak1I[RUh][RakS/SL],Rak1M[RUh][RakS/SL];
static lword Rak2I[RUh][RakS/SL],Rak2M[RUh][RakS/SL];
static lword Rak3I[RUh][RakS/SL],Rak3M[RUh][RakS/SL];
static lword Rak4I[RUh][RakS/SL],Rak4M[RUh][RakS/SL];

static lword Vyb1I[VybS/SL],Vyb1M[VybS/SL];
static lword Vyb2I[VybS/SL],Vyb2M[VybS/SL];
static lword Vyb3I[VybS/SL],Vyb3M[VybS/SL];
static lword Vyb4I[VybS/SL],Vyb4M[VybS/SL];

word Pozadi[VRAMSize/2];

#define SprImage(Ufon,UfoW,UfoH,UFOI,UFOM) \
Sprite Ufon= \
{ \
  UfoH,UfoW,1, \
  {UFOI[0],UFOI[1],UFOI[2],UFOI[3], \
  UFOI[4],UFOI[5],UFOI[6],UFOI[7], \
  UFOI[8],UFOI[9],UFOI[10],UFOI[11], \
  UFOI[12],UFOI[13],UFOI[14],UFOI[15]}, \
  {UFOM[0],UFOM[1],UFOM[2],UFOM[3], \
  UFOM[4],UFOM[5],UFOM[6],UFOM[7], \
  UFOM[8],UFOM[9],UFOM[10],UFOM[11], \
  UFOM[12],UFOM[13],UFOM[14],UFOM[15]}, \
};
#define ShfImage(Ufon,UfoW,UfoH,IUfo,IUfoM) \
Sprite Ufon= \
{ \
  UfoH,UfoW,0, \
  {IUfo}, \
  {IUfoM}, \
};

static SprImage(Ufon1,UfoW,Ufo1H,Ufo1I,Ufo1M);
static SprImage(Ufon2,UfoW,Ufo2H,Ufo2I,Ufo2M);
static SprImage(Ufon3,UfoW,Ufo3H,Ufo3I,Ufo3M);
static SprImage(Ufon4,UfoW,Ufo4H,Ufo4I,Ufo4M);
static SprImage(Ufon5,UfoW,Ufo5H,Ufo5I,Ufo5M);
static SprImage(Ufon6,UfoW,Ufo6H,Ufo6I,Ufo6M);
static SprImage(Ufon7,UfoW,Ufo7H,Ufo7I,Ufo7M);
static SprImage(Ufon8,UfoW,Ufo8H,Ufo8I,Ufo8M);

static ShfImage(PulSpr0,PulW,PulH,PulI[0],PulM[0]);
static ShfImage(PulSpr1,PulW,PulH,PulI[1],PulM[1]);
static ShfImage(PulSpr2,PulW,PulH,PulI[2],PulM[2]);
static ShfImage(PulSpr3,PulW,PulH,PulI[3],PulM[3]);
static ShfImage(PulSpr4,PulW,PulH,PulI[4],PulM[4]);
static ShfImage(PulSpr5,PulW,PulH,PulI[5],PulM[5]);
static ShfImage(PulSpr6,PulW,PulH,PulI[6],PulM[6]);
static ShfImage(PulSpr7,PulW,PulH,PulI[7],PulM[7]);

static SprImage(Kano,DelW,DelH,DelI,DelM);
static ShfImage(Lod,LodW,LodH,LodI,LodM);

ShfImage(Strela1,StrW,StrH,StrI[0],StrM[0]);
ShfImage(Strela2,StrW,StrH,StrI[1],StrM[1]);
ShfImage(Strela3,StrW,StrH,StrI[2],StrM[2]);
ShfImage(Strela4,StrW,StrH,StrI[3],StrM[3]);

static ShfImage(Rak10,RakW,RakH,Rak1I[0],Rak1M[0]);
static ShfImage(Rak11,RakW,RakH,Rak1I[1],Rak1M[1]);
static ShfImage(Rak12,RakW,RakH,Rak1I[2],Rak1M[2]);
static ShfImage(Rak13,RakW,RakH,Rak1I[3],Rak1M[3]);
static ShfImage(Rak14,RakW,RakH,Rak1I[4],Rak1M[4]);

static ShfImage(Rak20,RakW,RakH,Rak2I[0],Rak2M[0]);
static ShfImage(Rak21,RakW,RakH,Rak2I[1],Rak2M[1]);
static ShfImage(Rak22,RakW,RakH,Rak2I[2],Rak2M[2]);
static ShfImage(Rak23,RakW,RakH,Rak2I[3],Rak2M[3]);
static ShfImage(Rak24,RakW,RakH,Rak2I[4],Rak2M[4]);

static ShfImage(Rak30,RakW,RakH,Rak3I[0],Rak3M[0]);
static ShfImage(Rak31,RakW,RakH,Rak3I[1],Rak3M[1]);
static ShfImage(Rak32,RakW,RakH,Rak3I[2],Rak3M[2]);
static ShfImage(Rak33,RakW,RakH,Rak3I[3],Rak3M[3]);
static ShfImage(Rak34,RakW,RakH,Rak3I[4],Rak3M[4]);

static ShfImage(Rak40,RakW,RakH,Rak4I[0],Rak4M[0]);
static ShfImage(Rak41,RakW,RakH,Rak4I[1],Rak4M[1]);
static ShfImage(Rak42,RakW,RakH,Rak4I[2],Rak4M[2]);
static ShfImage(Rak43,RakW,RakH,Rak4I[3],Rak4M[3]);
static ShfImage(Rak44,RakW,RakH,Rak4I[4],Rak4M[4]);

static ShfImage(Sud,SudW,SudH,SudI,SudM);

static ShfImage(Vybuch1,VybW,VybH,Vyb1I,Vyb1M);
static ShfImage(Vybuch2,VybW,VybH,Vyb2I,Vyb2M);
static ShfImage(Vybuch3,VybW,VybH,Vyb3I,Vyb3M);
static ShfImage(Vybuch4,VybW,VybH,Vyb4I,Vyb4M);

AnimSpr PulSpr=
{
  8,0,0,
  {
    {8,&PulSpr0},
    {8,&PulSpr1},
    {8,&PulSpr2},
    {8,&PulSpr3},
    {8,&PulSpr4},
    {10,&PulSpr5},
    {12,&PulSpr6},
    {10,&PulSpr7},
  }
};

int ntakt = 1;
int ntaktVirt = 1;

volatile int JoyStat;

Flag Exit;
Flag Demo;
static Flag ExitP;
long Body;
HiRec Hi[Levels][LenHi];
int HiPtr;

Flag Kbhit(void);

static void TestKeys( Flag any )
{
  if (Kbhit())
  {
    int c = Getch(true);
    if (any && c>=0 || c=='P' || c==0x1b)
    {
      ExitP=True;
    }
  }
}

void ResetVsync( void )
{
  SetVsync(NTAKT,VRA);
  ntakt=1;
}

static char MOn[]={0x8};
static char MOff[]={0x14};

static word Pal[16];
Flag LoadObr(const char *name)
{
  static SInfo ToSet[]=
  {
    {0,0},{0,25},{0,47},{0,66},{0,83},{0,98},{0,111},{0,121}, /* inv */
    {0,131}, /* kanon */
    {8,131},{10,131},{12,131},{14,131}, /* koule */
    
    { 0,170},{ 2,170},{ 4,170},{ 6,170}, /* rak 0 */
    { 8,170},{10,170},{12,170},{14,170}, /* rak 1 */
    {16,170},{18,170},{20,170},{22,170}, /* rak 2 */
    {24,170},{26,170},{28,170},{30,170}, /* rak 3 */
    {32,170},{34,170},{36,170},{38,170}, /* rak 4 */
    {32,131}, /* sud */
    {16,131},{20,131},{24,131},{28,131}, /* vybuch */
    { 8,154},{12,155},{16,156},{20,157},
    {24,157},{28,156},{32,155},{36,154}, /* pulec */
    {36,131}, /* lod */
    {-1,-1},
  };
  
  static Sprite *ToInit[]=
  {
    &Ufon1,&Ufon2,&Ufon3,&Ufon4,&Ufon5,&Ufon6,&Ufon7,&Ufon8,&Kano,
    &Strela1,&Strela2,&Strela3,&Strela4,
    &Rak10,&Rak20,&Rak30,&Rak40,&Rak11,&Rak21,&Rak31,&Rak41,
    &Rak12,&Rak22,&Rak32,&Rak42,&Rak13,&Rak23,&Rak33,&Rak43,
    &Rak14,&Rak24,&Rak34,&Rak44,&Sud,
    &Vybuch1,&Vybuch2,&Vybuch3,&Vybuch4,
    &PulSpr0,&PulSpr1,&PulSpr2,&PulSpr3,
    &PulSpr4,&PulSpr5,&PulSpr6,&PulSpr7,&Lod,
    NULL
  };
  Sprite **S;
  SInfo *SI;
  word temp[VRAMSize/2];
  if( LoadPc(temp,Pal,name)<0 ) return False;
  for( S=ToInit,SI=ToSet; *S; SI++,S++ ) SetSpr(*S,SI,temp);
  
  for( S=ToInit; *S!=NULL; S++ ) InitSpr(*S);
  return True;
}

static void **MVQ;

Flag Zac( void )
{
  VRA=InitVRam();
  if( VRA )
  {
  }
  return True;
}

void Kon( void )
{
  EndSound();
}

void GD( CooG *G, CooD *D )
{
  G->x=gdx(D->X,D->D);
  G->y=h0+(int)(D->H/D->D);
};
void DG( CooD *D, CooG *G )
{
  D->X=dgx(G->x,D->D);
  D->H=(G->y-h0)*D->D;
};

enum
{
  dsm=2,
};

int ZvukC;
int Dup12;

Formace F;
CooD CI;
Flag PulecL;
int px;
VybuchI VI;

static int lx;
static int dlx;

enum {lxdsm=4,Maxdlx=1<<(lxdsm-1),Minlx=100<<lxdsm,Maxlx=400<<lxdsm};
enum {ly=(int)( (CIH+(long)D*UW) / ICID )+h0-LodHY,xls=SW2-LodHX};

int xd,xdi;

int tx;
int Stit;
int sx;
long sd;

static VybuchI SV;

StrelaI SI[2];
int Odraz;

void IncBody( int Am )
{
  long BN=Body+Am;
  
  if( BN>=1000000L ) BN=999999L;
  
  if( BN/1000>Body/1000 )
  {
    Stit+=StitKrok;
    if( Stit>StitMax ) Stit=StitMax;
  }
  Body=BN;
  while( HiPtr>0 && BN > Hi[Lvl][HiPtr-1].Body ) HiPtr--;
}

static Sprite *Strs[]=
{
  &Strela1,&Strela1,&Strela1,&Strela1,
  &Strela1,&Strela1,&Strela1,&Strela1,
  &Strela1,&Strela1,&Strela1,&Strela1,
  &Strela1,&Strela1,&Strela2,&Strela2,
  &Strela2,&Strela2,&Strela3,&Strela3,
  &Strela3,&Strela3,&Strela4,&Strela4,
  &Strela4,&Strela4,
};

#define coors(xx,yy) xx,ycoor(VRA,yy)

static void KrStr( void )
{
  int dd=(int)( sd/(Delta/4) );
  int y;
  int x;
    
  if( dd>=(int)lenof(Strs) ) dd=(int)lenof(Strs)-1;
  y=h0+(int)(th/sd)-StrHY;
  x=sx-StrHX;
  Draw16(coors(x,y),Strs[dd]);
}


typedef Sprite *UhSprite[RUh];

static UhSprite Rak1={&Rak10,&Rak11,&Rak12,&Rak13,&Rak14};
static UhSprite Rak2={&Rak20,&Rak21,&Rak22,&Rak23,&Rak24};
static UhSprite Rak3={&Rak30,&Rak31,&Rak32,&Rak33,&Rak34};
static UhSprite Rak4={&Rak40,&Rak41,&Rak42,&Rak43,&Rak44};

static UhSprite *Raks[]=
{
  &Rak1,&Rak1,&Rak1,&Rak1,
  &Rak1,&Rak1,&Rak1,&Rak1,
  &Rak1,&Rak1,&Rak1,&Rak1,
  &Rak2,&Rak2,&Rak2,&Rak2,
  &Rak3,&Rak3,&Rak3,&Rak3,
  &Rak4,&Rak4,&Rak4,&Rak4,
};

static void KrRak( StrelaI *S )
{
  CooG G;
  int dd=(int)( S->C.D/(Delta/4) );
  int x,y;
  int h;

    
  GD(&G,&S->C);
  x=G.x-RakHX;
  y=G.y-RakHY;
  if( y<ScrH && x>=0 && x<ScrW-RakEW )
  {
    int si;
    Sprite *RS;
    
    if( dd>=(int)lenof(Raks) ) dd=(int)lenof(Raks)-1;
    h=ScrH-y;
    if( h>StrH ) h=StrH;
    
    si=x*5/ScrW+S->skm;

    if( si<0 ) si=0;
    if( si>=RUh ) si=RUh-1;
    RS=(*Raks[dd])[si];
    
    RS->s_h=h;
    Draw16(coors(x,y),RS);
  }
}

static Sprite *Vybs[]={&Vybuch4,&Vybuch3,&Vybuch2,&Vybuch1};

int VybVel( long d )
{
  int r;
  
  r=(int)( lenof(Vybs)*D/d );
  if( r>(int)lenof(Vybs) ) return (int)lenof(Vybs);
  return r;
}

void KrVyb( VybuchI *V )
{
  int x=V->x-VybHX;
  int y=V->y-VybHY;
  
  Draw(coors(x,y),Vybs[V->Cnt>>VybDsm]);
  V->Cnt+=V->Step*ntakt;
  if( V->Cnt>=V->Max ) V->v_i=False;
}

static void VybUfo( void )
{
  if( VI.v_i )
  {
    CooD VC;
    CooG VG;
    
    VC.X=CI.X+(long)VI.xi*UW*D;
    VC.H=CI.H+(long)(HN-1)*UH*D;
    VC.D=CI.D+VI.di*Delta;
    GD(&VG,&VC);
    VI.x=VG.x;
    VI.y=VG.y;
    KrVyb(&VI);
  }
}

static void KrLod( void )
{
  Draw(coors((lx>>lxdsm),ly),&Lod);
}

static Sprite *Ufos[]=
{
  &Ufon1,&Ufon1,&Ufon1,&Ufon1, /*0..1 */
  &Ufon1,&Ufon1,&Ufon1,&Ufon1, /* ..2 */
  &Ufon1,&Ufon1,&Ufon1,&Ufon1, /* ..3 */
  &Ufon1,&Ufon1,&Ufon1,&Ufon1, /* ..4 */
  &Ufon1,&Ufon2,&Ufon3,&Ufon3, /* ..5 */
  &Ufon3,&Ufon4,&Ufon4,&Ufon4, /* ..6 */
  &Ufon5,&Ufon5,&Ufon5,&Ufon5, /* ..7 */
  &Ufon6,&Ufon6,&Ufon6,&Ufon6, /* ..8 */
  &Ufon6,&Ufon7,&Ufon7,&Ufon7, /* ..9 */
  &Ufon7,&Ufon7,&Ufon7,&Ufon7, /* ..10*/
  &Ufon7,&Ufon8,&Ufon8,&Ufon8, /* ..11*/
  &Ufon8,&Ufon8,&Ufon8,&Ufon8, /* ..12*/
  &Ufon8,&Ufon8,&Ufon8,&Ufon8, /* ..13*/
};

static UhSprite *UhRaks[]=
{
  &Rak1,&Rak1,&Rak1,&Rak1, /* ..17*/
  &Rak2,&Rak2,&Rak2,&Rak2, /* ..21*/
  &Rak2,&Rak3,&Rak3,&Rak3, /* ..25*/
  &Rak3,&Rak3,&Rak3,&Rak3, /* ..29*/
  &Rak3,&Rak4,&Rak4,&Rak4, /* ..33*/
};

static void KrLetku( void )
{
  CooG CG;
  CooD CD;
  int dn;
  int dir0,dir1,dis;
  int dvyb;
  
  if( VI.v_i )
  {
    dvyb=VI.di;
  }
  else dvyb=DN;
  
  if( SI[0].s_i )
  {
    dir0=(int)( (SI[0].C.D-CI.D)/Delta );
    if( dir0>=DN ) dir0=DN-1;
  }
  else dir0=DN;
  if( SI[1].s_i )
  {
    dir1=(int)( (SI[1].C.D-CI.D)/Delta );
    if( dir1>=DN ) dir1=DN-1;
  }
  else dir1=DN;
  if( StrelaL )
  {
    dis=(int)( (sd-CI.D)/Delta );
    if( dis>=DN ) dis=DN-1;
  }
  else dis=DN;
  
  CD=CI;
  for( dn=DN-1; dn>=0; dn-- )
  {
    int hn;
    int dd;
    Sprite *UfoS = nullptr;
    UhSprite *RakS = nullptr;
    Flag RakF;
    int xx0;
    
    int yy;
    int w,h;
    
    if( dn==dir0 ) KrRak(&SI[0]);
    if( dn==dir1 ) KrRak(&SI[1]);
    if( dn==dis ) KrStr();
    CD.D=CI.D+dn*Delta;
    w=(int)((UW<<dsm)*(long)D/CD.D);
    h=(int)((UH<<dsm)*(long)D/CD.D);

    dd=(int)((CD.D+Delta/8)/(Delta/4));
    RakF=( dd>=(int)lenof(Ufos) );
    if( RakF )
    {
      dd=(dd-(int)lenof(Ufos))/4;
      if( dd>=(int)lenof(UhRaks) ) dd=(int)lenof(UhRaks)-1;
    }
    
    GD(&CG,&CD);
    
    if( !RakF )
    {
      UfoS=Ufos[dd];
      yy=(CG.y-UfoS->s_h/2)<<dsm;
    }
    else
    {
      RakS=UhRaks[dd];
      yy=(CG.y-StrH/2)<<dsm;
    }
    
    xx0=( ( (CG.x-(RakF ? RakHX : UfoHX)) & ~1 )<<dsm ) + F.LM*w;
    for( hn=0; hn<HN; hn++,yy+=h )
    {
      int xx,xn;
      byte *LetkaX=F.Letka[dn][hn];
      /* stejn� D i H */
      if( RakF )
      {
        for( xx=xx0,xn=F.LM; xn<=F.RM; xn++,xx+=w ) if( LetkaX[xn] )
        {
          #if XN>RUh
            int xxn=xn; if( xxn>=RUh ) xxn=RUh-1;
          #else 
            #define xxn xn
          #endif
          Draw16(coors(xx>>dsm,yy>>dsm),(*RakS)[xxn]);
          #undef xxn
        }
      }
      else
      {
        for( xn=F.LM,xx=xx0; xn<=F.RM; xn++,xx+=w ) if( LetkaX[xn] )
        {
          Draw(coors(xx>>dsm,yy>>dsm),UfoS);
        }
      }
    }
    if( dn==dvyb ) VybUfo();
  } /* for dn */
  if( dir0<0 ) KrRak(&SI[0]);
  if( dir1<0 ) KrRak(&SI[1]);
  if( dis<0 ) KrStr();
  #undef dsm
}

static void Ukazatele( void )
{
  int xx=tx+(Mono ? 60 : 58);
  
  Bar8
  (
    coors(xx,DelPH-smin-Stit),ycoor(VRA,DelPH-smin+1),
    0x00000000L,0x9fffffffL
  );
}

static char BodyS[40];

static void TBody( void )
{
  strcpy(BodyS,"Score: ");
  _ultoa(Body,&BodyS[7],10);
  TiskS(ycoor(VRA,4),1,BodyS);
  if( HiPtr<LenHi )
  {
    _ultoa(HiPtr+1,BodyS,10);
    strcat(BodyS,".");
    TiskS(ycoor(VRA,30),5,BodyS);
  }
}

static void KrTank( void )
{
  Draw128(coors(tx,UDelH),&Kano);
}

byte Sudy[NSud];
int Sudyx[NSud]=
{
  Sud1X,Sud1X+32,Sud1X+64,Sud1X+16,Sud1X+48,
  Sud2X,Sud2X+32,Sud2X+64,Sud2X+16,Sud2X+48,
};
int Sudyy[NSud]=
{
  SudY,SudY,SudY,Sud2Y,Sud2Y,
  SudPY,SudPY,SudPY,SudP2Y,SudP2Y,
};

static void KrSudy( void )
{
  int i;

  if( SV.v_i ) KrVyb(&SV);
  for( i=0; i<NSud; i++ ) if( Sudy[i] )
  {
    int x=Sudyx[i];
    
    Draw16(coors(x,Sudyy[i]),&Sud);
  }
}

#define dds 32

void RanSud( int si )
{
  switch( si )
  {
    case 0:
      if( Sudy[3] ) si=3;
      break;
    case 1:
      if( Sudy[4] ) si=4;
      else if( Sudy[3] ) si=3;
      break;
    case 2:
      if( Sudy[4] ) si=4;
      break;
      
    case 5:
      if( Sudy[8] ) si=8;
      break;
    case 6:
      if( Sudy[8]) si=8;
      else if( Sudy[9]) si=9;
      break;
    case 7:
      if( Sudy[9] ) si=9;
      break;
  }
  Sudy[si]=False;
  
  SV.x=Sudyx[si]+SudHX;
  SV.y=Sudyy[si]+SudHY;
  SV.Max=4<<VybDsm;
  SV.Step=4<<(VybDsm-3);
  SV.Cnt=0;
  SV.v_i=True;
  InitKan(&BuchS);
}

void InAnim( AnimSpr *AS )
{
  AS->FCnt=AS->TCnt=0;
}

void KrAnim( int x, int y, AnimSpr *AS )
{
  AS->TCnt+=ntakt;
  if( AS->TCnt >= AS->Faze[AS->FCnt].Doba )
  {
    AS->TCnt=0;
    if( ++AS->FCnt >= AS->NF ) AS->FCnt=0;
  }
  Draw(coors(x,y),AS->Faze[AS->FCnt].Obr);
}

void KonHra0( void )
{
  #ifdef __TESTSPEED
    DebugError
    (
      __LINE__,__FILE__,
      "Pomal� %ld, Rychl� %ld Celkem %ld\xd\xa",
      TSum,PSum,_frclock0-FrclockI
    );
  #endif
  EndPlay(); /* vypnout norm player */
  Exit=False;
  ntakt = 1;
}

static void Pau( void )
{
  EndPlay(); /* vypnout norm player */
  if( !Demo ) Pauzuj();
  else Exit=True;
  ExitP=False;
  InitPlay(); /* norm player on */
}

static void MinPohyb( Flag L )
{
  SetVsync(ntakt,VRA);
  Sups(Pozadi);
  KrLod();
  if( L ) KrLetku();
  KrSudy();
  KrTank();
  Ukazatele();
  TBody();
  Rizeni(L);
  VRA=Shot();
  MyVsync();
  TestKeys(Demo);
  if( ExitP ) Pau();
}

static const StrelaI LSI[Levels][2]=
{
  {{ 6, 0, 6},{ 0, 0, 0}},
  {{10, 0, 8},{ 0, 0, 0}},
  {{10,20,12},{ 0, 0, 0}},
  {{10, 0, 8},{10,75,12}},
  {{10, 0,16},{10,75,12}},
  {{10, 40,14},{10,75,16}},
};

void ZacHra0( void )
{
  int i;
  
  InitPlay(); /* zapnout norm player */
  SI[0]=LSI[Lvl][0];
  SI[1]=LSI[Lvl][1];
  
  for( i=0; i<NSud; i++ ) Sudy[i]=False;
  
  Stit=StitMax;
  ExitP=Exit=False;
  Body=0;
  tx=SW2-TEW/2;
  HiPtr=LenHi;
  
  #ifdef __TESTSPEED
  {
    void *SSP=(void *)Super(0);
    TSum=PSum=0;
    FrclockI=_frclock;
    Super(SSP);
  }
  #endif
}

static void ZacHra( Formace *f, long Dist )
{
  int dd,i;
  
  F=*f;
  
  InitP(&Let);
  LogInit();
  ExitP=Exit=False;

  ZvukC=ZvukS;
  Dup12=0;

  SV.v_i=VI.v_i=
  SI[0].s_i=SI[1].s_i=False;
  
  {
    long MaxX=(long)(SW2-UW-UW*F.RM)*D;
    long MinX=(long)(-SW2+UW-UW*F.LM)*D;
    
    CI.X=(MinX+MaxX)/2;
  }
  
  for( i=0; i<NSud; i++ ) Sudy[i]=True;
  
  CI.D=ICID;
  CI.H=CIH;
  ntakt=1;
  ntaktVirt=NTAKT;
  dd=Delta*2+Delta/3;
  for( ; CI.D > Dist+Delta; CI.D-=dd*ntakt/ntaktVirt )
  {
    MinPohyb(True);
    if( Exit ) goto Konec;
    if( dd>50*ntakt/ntaktVirt ) dd-=15*ntakt/ntaktVirt;
  }
  LastLoopP();
  dlx=0;
  xd=+1;
  xdi=4;
  Odraz=0;
  while( !IsBufferDone(0) )
  {
    MinPohyb(True);
    if( Exit ) goto Konec;
    CI.D-=5;
  }
  
  Konec:
  ntakt=1;
  InitP(0);
  return;
}

static void Jezdi( void )
{
  if( Rnd(100)<5 )
  {
    dlx+=(Rnd(200)-100)*Maxdlx/100;
    if( dlx>Maxdlx ) dlx=Maxdlx;
    if( dlx<-Maxdlx ) dlx=-Maxdlx;
  }
  lx+=dlx*ntakt;
  if( lx<Minlx+4*Maxdlx && dlx<=0 ) dlx+=Maxdlx/4;
  if( lx>Maxlx-4*Maxdlx && dlx>=0 ) dlx-=Maxdlx/4;
  if( lx<Minlx )
  {
    lx=Minlx;
    dlx=0;
  }
  if( lx>Maxlx )
  {
    lx=Maxlx;
    dlx=0;
  }
}

void Hra( Formace *f, long Dist, int formi )
{
  ZacHra(f,Dist);
  if( Exit ) goto Konec;
  while
  (
    !Exit
    &&
    ( F.NI>0 || PulecL || SI[0].s_i || SI[1].s_i )
    ||
    ( PV.v_i || VI.v_i || VD.v_i || SV.v_i )
  )
  {
    SetVsync(ntakt,VRA);
    Sups(Pozadi);
    Strileni();
    Zasahy();
    KrLod();
    Jezdi();
    PulecD();
    KrLetku();
    KrSudy();
    VybDelo();
    KrTank();
    Ukazatele();
    TBody();
    Odrazy();
    
    Strely(&SI[0]);
    Strely(&SI[1]);

    Rizeni(True);
    VRA=Shot();
    MyVsync();
    TestKeys(Demo);
    if( ExitP )
    {
      Pau();
      if( Exit ) goto Konec;
    }
    if( Stit<=0 || CI.D+(long)F.FM*Delta<=Dmin ) Exit=True;
  }
  if( !Exit )
  {
    int d;
    long Bonus=(CI.D+(long)F.FM*Delta-Dmin)*(formi+3)/3;
    long i;
    Flag B=Bonus>=4*Delta;
    
    if( B ) InitP(&Bon);
    for( i=0; d=(lx>>lxdsm)-xls, i<Bonus && ((i+=20),True) || d!= 0; )
    {
      IncBody(1);
      if( B && i>=Bonus-4*Delta )
      {
        LastLoopP();
      }
      MinPohyb(0);
      if( d<=-ntakt ) lx+=ntakt<<lxdsm;
      else if( d>=+ntakt ) lx-=ntakt<<lxdsm;
      else if( d<0 ) lx+=1<<lxdsm;
      else if( d>0 ) lx-=1<<lxdsm;
      if( Exit ) goto Konec;
    }
    if( B )
    {
      while( !IsBufferDone(0) )
      {
        MinPohyb(0);
        if( Exit ) goto Konec;
      }
    }
  }
  Konec:
  InitP(0);
}

void ZacForm( void )
{
  ntaktVirt=NTAKT-2;
  for( lx=-(LEW<<lxdsm); lx<(xls<<lxdsm); lx+=ntakt*(2<<lxdsm) )
  {
    MinPohyb(False);
    if( Exit ) goto Konec;
  }
  Konec:
  return;
}

void VyhrajForm( void )
{
  VybuchI LV;
  Flag Lod=True,LVyb=False;
  int i;
  
  if( !Demo ) InitP(&Ml);
  else InitP(0);
  ntaktVirt=NTAKT-2;
  LV.v_i=False;
  while( Lod || LV.v_i )
  {
    SetVsync(ntakt,VRA);
    Sups(Pozadi);
    if( Demo || IsBufferDone(0) ) LVyb=True;
    if( Lod || LVyb && LV.v_i && LV.Cnt<(2<<VybDsm) ) KrLod();
    if( Lod )
    {
      if( !LV.v_i && Rnd(100)<5 )
      {
        if( LVyb )
        {
          Lod=False;
          LV.Max=(4<<VybDsm);
          LV.x=SW2;
          LV.y=ly+LodHY;
          InitP(0);
          InitKan(&Vybuch);
        }
        else
        {
          LV.Max=(2<<VybDsm)+(2<<VybDsm)*Rnd(100)/100;
          LV.x=xls+LodHX/2+Rnd(LodHX);
          LV.y=ly+LodHY/2+Rnd(LodHY);
        }
        LV.Cnt=0;
        LV.Step=4<<(VybDsm-5);
        LV.v_i=True;
      }
    }
    if( LV.v_i ) KrVyb(&LV);
  
    KrSudy();
    KrTank();
    Ukazatele();
    TBody();
    Rizeni(False);
    VRA=Shot();
    MyVsync();
    TestKeys(Demo);
    if( ExitP ) Pau();
    if( Exit ) goto Konec;
  }
  for( i=0; i<20; i++ )
  {
    SetVsync(ntakt,VRA);
    Sups(Pozadi);
    KrSudy();
    KrTank();
    Ukazatele();
    TBody();
    Rizeni(False);
    VRA=Shot();
    MyVsync();
    TestKeys(Demo);
    if( ExitP ) Pau();
    if( Exit ) goto Konec;
  }
  Konec:
  InitP(0);
}
