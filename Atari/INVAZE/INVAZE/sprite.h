#ifndef sprite_h_included
#define sprite_h_included

#include "macros.h"

#define BPL 80

typedef lword Image[];
typedef lword *ImageP;

#define NFaz 16
typedef struct
{
	int s_h,s_w;
	int faze;
	ImageP Image[NFaz],Mask[NFaz];
} Sprite;

typedef void DrawProc( int x, word *VAdr, Sprite *Def );
DrawProc Draw,Draw16,Draw128;
void Save16( int x, int h, Flag Write, word *VAdr, word *SAdr );

void Bar8( int x, word *VAdr, word *EAdr, lword Obsah, lword Maska );
void TiskS( word *VAdr, int x, char *Str );
void TiskSG( int x, word *VAdr, char *Str );
void TiskSGO( int x, word *VAdr, char *Str );

void Sups( void *Obsah );
void Prolni( void *T, void *s, void *S, word *R0 );
void Recolor( word *BP, lword *SP );
extern word RozsirB[256];

#define VRAMSize (320*200*4/8)

extern word *VRA;
extern byte Font[0x1800+0x80*3];

void linea_init( void );
void show_mouse( int flag );
void hide_mouse( void );

#endif
