#include <io.h>
#include <stdio.h>
#include <string.h>

#undef fopen
#undef open
#undef _open

extern char GiGDirectory[];

FILE *my_fopen(const char *filename, const char *mode)
{
  char buf[2048];
  strcpy(buf,GiGDirectory);
  strcat(buf,"\\");
  strcat(buf,filename);
  return fopen(buf,mode);
}

int my_open(const char *filename, int mode)
{
  char buf[2048];
  strcpy(buf,GiGDirectory);
  strcat(buf,"\\");
  strcat(buf,filename);
  return _open(buf,mode);
}
