#ifndef st_input_h_included
#define st_input_h_included

#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

Flag Kbhit();
int Getch(bool downOnly=false);

#ifdef __cplusplus
extern "C"
{
#endif

#define Fopen(name,mode) my_open(name,mode|_O_BINARY)
#define Fread(h,s,b) _read(h,b,s)
#define Fwrite(h,s,b) _write(h,b,s)
#define Fseek(offset,fd,origin) _lseek(fd,offset,origin)
#define Fclose(h) _close(h)
#define Fcreate(name,mode) _creat(name,mode)
#define Malloc(a) malloc(a)
#define Mfree(a) free(a)

FILE *my_fopen(const char *filename, const char *mode);
int my_open(const char *filename, int mode);
#define fopen my_fopen
#define open my_open
#define _open my_open

#ifdef __cplusplus
};
#endif

#endif
