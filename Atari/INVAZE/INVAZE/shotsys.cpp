#include "macros.h"

#include "shotsys.h"

byte VRamFree[0x10000L+0x100];

void *VRam[2];
int NumVRam;

word *InitVRam( void )
{
	VRam[0]=(void *)( ((long)VRamFree+0xff)&~0xffL );
	VRam[1]=(void *)( (long)VRam[0]+ 0x8000L );
	NumVRam=0;
	return reinterpret_cast<word *>(VRam[NumVRam]);
}

word *Shot( void )
{
	NumVRam^=1;
	return reinterpret_cast<word *>(VRam[NumVRam]);
}
