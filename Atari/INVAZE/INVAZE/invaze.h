enum {Levels=6,LenHi=7};
enum
{
	ScrW=640,
	ScrH=400,
	SW2=ScrW/2,
	h0=16,
};

enum
{
	Delta=256,d=Delta,D=4*Delta,
	XN=5,
	HN=3,
	DN=3,
	NNI=HN*XN*DN
};

typedef struct
{
	long X,H,D;
} CooD;
typedef struct
{
	int x,y;
} CooG;
typedef struct
{
	byte Letka[DN][HN][XN];
	int cidd;
	int NI;
	int NIX[XN];
	int NIH[HN];
	int NID[DN];
	int LM,RM;
	int FM;
} Formace;
typedef struct
{
	int StrelaP;
	int SikmaP;
	int ssp;
	Flag s_i;
	Flag s_sud;
	CooD C;
	CooD D;
	int skm;
} StrelaI;
typedef struct
{
	Flag v_i;
	int x,y,xi,di;
	int Max,Step;
	int Cnt;
} VybuchI;
enum { MaxFaz=8 };
typedef struct
{
	int Doba;
	Sprite *Obr;
} FazeT;
typedef struct
{
	int NF;
	int FCnt;
	int TCnt;
	FazeT Faze[MaxFaz];
} AnimSpr;
enum {HJL=16}; /* delka jmena */
typedef struct
{
	char Jmeno[HJL];
	long Body;
} HiRec;

#define CIH (140L*D) /* vyska invaderu */
#define ICID (50L*Delta) /* zac. vzdal inva.=vzdal. lode */

#define th (CIH+(long)(HN-1)*UH*D) /* vyska kanonu */
#define td (th/(DelHH-h0)) /* vzdalenost kanonu */
#define Sudd (td+100) /* vzdalenost sudu */

enum /* parametry hry */
{
	StrW=2, /* sprite del. koule */
	StrH=8*2,
	StrHX=16,
	StrHY=8,
	StrS=StrH*StrW*sizeof(long),

	DelHY=0, /* kanon sprite */
	DelHX=64,
	DelW=5,
	DelH=34*2,
	DelS=DelW*DelH*sizeof(long),
	TEW=128,

	PulH=12*2, /* sprite pulec */
	PulW=3,
	PEW=64,
	PulHX=PEW/2,
	PulHY=PulH/2,
	PulS=PulH*PulW*sizeof(long),
	
	SudH=18*2, /* sprite sud */
	SEW=31,
	SudHX=SEW/2,
	SudHY=SudH/2,
	SudW=2,
	SudS=SudW*SudH*sizeof(long),
	
	VybW=3, /* sprite vybuch */
	VybH=19*2,
	VybS=VybW*VybH*sizeof(long),
	VybHX=32,
	VybHY=VybH/2,
		
	UW=96, /* sirka inva. */
	UH=64, /* vyska inva. */

	UWS=UW/3,
	UHS=UH/2,

	/* rychlosti */
	NTAKT=6, /* zakl. taktovani ( v monoch. snimcich ) */

	dds=32, /* strel */
	dxt=2, /* dela */
	dxp=1, /* pulce */
	ZvukS=35, /* dupot */
	TaktOdr=4, /* doba odrazu */

	DelPH=ScrH, /* spodek kanonu - gr */
	UDelH=DelPH-DelH, /* vrsek kanonu */
	DelHH=DelPH-DelH/3, /* hlaven */

	pd=13*Delta, /* vzd. pulce */
	Maxsd=pd, /* dolet strely */
	Dmin=D-Delta/2, /* min. vzd. invaderu */

	py=th/pd+h0, /* gr. sour. strely */
	tds=td+130, /* vzdalenost strely pri vysrtelu */

	Sud1X=160, /* sour. sudu */
	Sud2X=ScrW-260,
	SudY=ScrH-SudH-32,
	Sud2Y=SudY-SudH+8,
	SudPY=ScrH-SudH-28,
	SudP2Y=SudPY-SudH+8,

	VybDsm=8, /* des. mista ve VybuchI */

	smin=6,smax=37, /* sour. uk. stitu */
	StitMax=smax-smin-1,StitKrok=StitMax/10, /* odolnost stitu */
};

#define gdx(X,D) ( SW2+(int)((long)(X)/(D)) )
#define dgx(x,D) ( (long)((x)-SW2)*(D) )

void GD( CooG *G, CooD *D );
void DG( CooD *D, CooG *G );

/* OMACKA.C */
extern int Lvl;

/* INVAZE.C */
extern int ntakt; /* pocet dob (70Hz) na jeden snimek */
extern int ntaktVirt; /* pocet dob (70Hz) na jeden snimek */
extern volatile int JoyStat;

extern Flag Exit;
extern Flag Demo;
extern word Pozadi[]; /* krajina */

extern long Body;
extern int Stit;
extern HiRec Hi[Levels][LenHi]; /* tabulka rekordu */
extern int HiPtr;

extern Flag PulecL;
extern Flag Fire; /* demo - strileni */
extern int Odraz; /* citac odrazu inv. */

extern int ZvukC; /* citac dupotu */
extern int Dup12; /* 2 druhy dupotu */

extern int xd,xdi; /* smer, ryhlost invaderu */
extern int px,tx; /* x pulce, kanonu */

extern int sx; /* x strely */
extern long sd; /* vzd. */

extern StrelaI SI[2]; /* strely invaderu */
extern VybuchI VI; /* vybuch inv. */

extern Formace F;
extern CooD CI; /* sour. invad. */

enum {NSud=10};
extern byte Sudy[NSud];
extern int Sudyx[NSud],Sudyy[NSud];

extern Sprite Strela1,Strela2,Strela3,Strela4;
extern AnimSpr PulSpr;

/* LOGHRA.C */
extern Flag StrelaL;
extern VybuchI PV,VD;

/* OMACKA.C */
void Pauzuj( void );

/* INVAZE.C */

void MyVsync( void );
void SetVsync( int n, void *VA );
void ResetVsync( void );

void InAnim( AnimSpr *AS );
void KrAnim( int x, int y, AnimSpr *AS );

Flag Zac( void );
void Kon( void );
Flag LoadObr( const char *name );

void ZacHra0( void );
void KonHra0( void );

void ZacForm( void );
void VyhrajForm( void );

void Hra( Formace *f, long Dist, int formi );

void RanSud( int si );
int VybVel( long d );
void KrVyb( VybuchI *V );

void IncBody( int Am );

/* LOGHRA.C */

void Rizeni( Flag L );
void LogInit( void );
void ZrusPulce( void );
void PulecD( void );
void Zasahy( void );
void Strileni( void );
void Strely( StrelaI *S );
void Odrazy( void );
void VybDelo( void );


/* ----------*/

