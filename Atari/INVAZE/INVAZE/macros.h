#ifndef macros_h_included
#define macros_h_included

#include <stdlib.h>

typedef unsigned short word;
typedef unsigned char byte;
typedef unsigned int lword;

#ifdef __cplusplus
typedef bool Flag;
const bool True = true;
const bool False = false;
#else
typedef enum flag {False,True} Flag;
#endif

#define lenof(a) sizeof(a)/sizeof(*a)

typedef char PathName[256];

#ifndef max
#define max(a,b) ((a)<(b) ? (b) : (a))
#endif

#ifndef min
#define min(a,b) ((a)>(b) ? (b) : (a))
#endif

#define Assert(cond,text)

#ifndef __cplusplus
#define For(Type,Var,Init,End,Step) \
{ \
	Type Var;Type EF=End; \
	for( Var=Init; Var<EF; Step ) \
	{
#define Next } }
#endif

#endif
