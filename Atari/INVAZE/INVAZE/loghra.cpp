#include "macros.h"
#include <string.h>
#include <stdio.h>

#include "zvuky.h"
#include "sprite.h"
#include "shotsys.h"
#include "utils.h"

#include "invaze.h"

static int Klesej;
static int BLM=0,BRM=0,WXI=-1; /* demo */

int KeyFire();
int KeyMoveX();
int KeyMoveY();


Flag StrelaL;
static int StrelaC;

VybuchI VD,PV; /* vybuch kanonu, pulce */

static int PK; /* kanal pulce - zvuk */
static int PulecC; /* citac pulce */
enum {PulecS=1200}; /* rychl.pulce */

void LogInit( void )
{
  StrelaC=
  Klesej=0;
  PulecL=
  StrelaL=False;
  VD.v_i=False;
  PulecC=PulecS;
}
static Flag Predp( int txx )
{
  int xn,dn;
  Flag r=False;
  
  BLM=F.RM;
  BRM=F.LM;
  WXI=-1;
  for( xn=F.LM; xn<=F.RM; xn++ ) for( dn=F.FM; dn<DN; dn++ )
  {
    if( F.Letka[dn][HN-1][xn] )
    {
      long di=CI.D+Delta*dn;
      int dd=(int)( (di-tds)/(dds*ntakt) );
      long cix=CI.X+(dd*xd*xdi)*(long)D;
      long MaxX=(long)(SW2-UW-UW*F.RM)*D;
      long MinX=(long)(-SW2+UW-UW*F.LM)*D;
      long xi;
      long odrx=(TaktOdr*xdi)*(long)D;
      
      if( dn==F.FM ) WXI=xn;
      if( xn<BLM ) BLM=xn;
      if( xn>BRM ) BRM=xn;
      #define odr              \
      if( cix>MaxX )           \
      {                        \
        if( cix>MaxX+odrx )    \
        {                      \
          cix-=odrx;           \
          cix=MaxX-(cix-MaxX); \
        }                      \
        else cix=MaxX;         \
      }                        \
      if( cix<MinX )           \
      {                        \
        if( cix<MinX-odrx )    \
        {                      \
          cix+=odrx;           \
          cix=(MinX-cix)+MinX; \
        }                      \
        else cix=MinX;         \
      }
      
      odr
      odr

      xi=cix+(long)xn*UW*D;
      {
        int gxi=gdx(xi,di);
        int gxd=gxi-(txx+TEW/2);
        
        if( gxd<0 ) gxd=-gxd;
        if( gxd<UW/8 ) r=True;
      }
    }
  }
  if( r ) return True;
  if( PulecL )
  {
    #define pxh ( px+(int)( (Maxsd-tds)/dds )*dxp )
    
    int dp=pxh-(txx+TEW/2);
    
    if( dp<0 ) dp=-dp;
    
    if( dp<ntakt*2*dxp ) return True;
    #undef pxh
  }
  return False;
}

static Flag Fire=False;

void Rizeni( Flag L )
{
  enum { Mintx=10,Maxtx=ScrW-4*32-10};
  if( !Demo )
  {
    int x = KeyMoveX();
    
    if( x>0 ) tx+=ntakt*dxt;
    if( x<0 ) tx-=ntakt*dxt;
  }
  else if( L )
  {
    static int txw=SW2;
    static enum {TXW,LMX,RMX,HALT} Mode=HALT;
    int lmx;
    int rmx;
    int lmi,rmi;
    int txww;
    word r=Rnd(1000);
    
    int i;
    
    lmi=rmi=-1;
    for( i=0; i<DN; i++ ) if( F.Letka[i][HN-1][BLM] )
    {
      lmi=i;
      break;
    }
    for( i=0; i<DN; i++ ) if( F.Letka[i][HN-1][BRM] )
    {
      rmi=i;
      break;
    }
    
    #define MaxX ((long)(SW2-UW)*D)
      lmx=
      SW2+(int)
      (
        ( -MaxX+(long)(BLM-F.LM)*UW*D )
        /
        (CI.D+lmi*Delta)
      )-TEW/2;
      rmx=
      SW2+(int)
      (
        ( MaxX-(long)(F.RM-BRM)*UW*D )
        /
        (CI.D+rmi*Delta)
      )-TEW/2;
    #undef MaxX
    {
      enum {P1=2,P2=P1+30,P3=P2+5,P4=P3+3,P5=P4+2 };
      int mf=BRM-BLM;
      
      if( r<P1 )
      {
        Mode=TXW;
        txw=Rnd(Maxtx-Mintx)+Mintx;
      }
      
      else if( mf>=XN-2 && r<P2 && lmi>=0 ) Mode=LMX;
      else if( mf>=XN-2 && r<P3 && rmi>=0 ) Mode=RMX;
      else if( r<P4 )
      {
        Mode=TXW;
        txw=Rnd(rmx-lmx)+lmx;
      }
      else if( r<P5 ) Mode=HALT;
    }
    
    switch( Mode )
    {
      case TXW: txww=txw;break;
      case LMX: txww=lmx;break;
      case RMX: txww=rmx;break;
      default:  txww=tx;break;
    }
    if( SI[0].s_i )
    {
      if( SI[0].C.D<td+3*Delta )
      {
        #define tew ( (long)TEW*td/2 )
        long TX=dgx(tx+TEW/2,td);
        long sx=SI[0].C.X;
        long dt=sx-TX;
        
        if( dt<0 ) dt=-dt;
        if( dt<=tew )
        {
          if
          (
            sx<TX && sx<dgx(ScrW-TEW*5/2,td) ||
            sx<dgx(TEW*3/2,td)
          ) txww=tx+TEW;
          else txww=tx-TEW;
          txw=txww;
          Mode=TXW;
          #if 0 // _DEBUG
            {
              char diag[256];
              sprintf(diag,"TX %3d, sx %3d, dt %3d",tx,sx,dt);
              TiskSG(10,ycoor(VRA,340),diag);
            }
          #endif
        }
        #undef tew
      }
    }
    if( CI.D+Delta*F.FM<Dmin+Delta && WXI>=F.LM )
    {
      if( BRM==BLM ) txww=(rmx+lmx)/2;
      else txww=(rmx-lmx)*(WXI-BLM)/(BRM-BLM)+lmx;
    }
    #if 0 // _DEBUG
      {
        char diag[256];
        sprintf(diag,"TX %3d, TXWW %3d, LMX %3d, RMX %3d, M=%d",tx,txww,lmx,rmx,Mode);
        TiskSG(10,ycoor(VRA,300),diag);
      }
    #endif
    if( tx>txww ) {tx-=dxt*ntakt;if (tx<txww) tx = txww;}
    if( tx<txww ) {tx+=dxt*ntakt;if (tx>txww) tx = txww;}
    Fire=Predp(tx);
  }
  if( tx<Mintx ) tx=Mintx;
  if( tx>Maxtx ) tx=Maxtx;
}

void Odrazy( void )
{
  if( F.NI>0 )
  {
    if( Odraz>0 )
    {
      CI.D-=F.cidd;
      Odraz--;
    }
    else
    {
      long X=CI.X;
      long MaxX=(long)(SW2-UW-UW*F.RM)*D;
      long MinX=(long)(-SW2+UW-UW*F.LM)*D;
      
      X+=xd*xdi*D*ntakt/ntaktVirt;
      if( X>=MaxX )
      {
        xd=-1;
        if( !Exit ) Odraz=TaktOdr;
        X=MaxX;
      }
      if( X<=MinX )
      {
        xd=+1;
        if( !Exit ) Odraz=TaktOdr;
        X=MinX;
      }
      CI.X=X;
    }
    ZvukC-=xdi*100*ntakt/ntaktVirt;
    if( ZvukC<=0 )
    {
      ZvukC=ZvukS*100;
      InitKan( Dup12 ? &Dup1 : &Dup2);
      Dup12^=1;
    }
  }
}

void ZrusPulce( void )
{
  PulecL=False;
  if( PK>=0 && kanaly[PK].snd==&Pulec ) EndKan(PK);
}

static void RanPul( void )
{
  PV.Max=2<<VybDsm;
  PV.Step=2<<(VybDsm-3);
  PV.Cnt=0;
  PV.x=px;
  PV.y=py;
  PV.v_i=True;
  ZrusPulce();
  InitKan(&Vybuch);
  IncBody(50);
}

#define StrelSp ( Mono ? 40 : 40*5/7 )

void Strileni( void )
{
  if( StrelaL )
  {
    sd+=dds*ntakt;
    if( sd>Maxsd )
    {
      StrelaL=False;
      if( PulecL && sx>=px-PulHX+16 && sx<px+PEW-PulHX ) RanPul();
    }
  }
  else if( StrelaC<=0 )
  {
    if( !Demo ? KeyFire() : Fire )
    {
      InitKan(&DelVyst);
      sx=tx+TEW/2;
      sd=tds;
      if( sx>=Sud1X+16 && sx<Sud1X+16+2*SEW )
      {
        int si=(sx-Sud1X-16)/32+3;
        
        if( Sudy[si] )
        {
          StrelaC=StrelSp;
          RanSud(si);
          return;
        }
      }
      else if( sx>=Sud2X+16 && sx<Sud2X+16+2*SEW )
      {
        int si=(sx-Sud2X-16)/32+8;
        
        if( Sudy[si] )
        {
          StrelaC=StrelSp;
          RanSud(si);
          return;
        }
      }
      StrelaL=True;
      StrelaC=StrelSp;
    } /* trigger */
  } /* strelac<=0 */
  else StrelaC-=ntakt;
}

static void Zasah( int xi, int dis, byte *l )
{
  *l=False;
  InitKan(&BuchUF);
  F.NIX[xi]--;
  F.NID[dis]--;
  F.NIH[HN-1]--;
  F.NI--;
  while( F.NIX[F.LM]==0 && F.LM<F.RM ) F.LM++;
  while( F.NIX[F.RM]==0 && F.LM<F.RM ) F.RM--;
  while( F.NID[F.FM]==0 && F.FM<DN ) F.FM++;
  VI.xi=xi;
  VI.di=dis;
  VI.Max=VybVel(CI.D+Delta*(long)dis)<<VybDsm;
  VI.Step=VI.Max/16;
  VI.Cnt=0;
  VI.v_i=True;

  IncBody(dis*2+3);

  switch( F.NI )
  {
    case 1:
      xdi=20;
      break;
    case 2:
      xdi=12;
      break;
    case 6:
      xdi=8;
      ntaktVirt=NTAKT-2;
      break;
    case 12:
      xdi=8;
      break;
  }
}

void Zasahy( void )
{
  if( StrelaL )
  {
    int maxdm=ntakt*dds*3/5;
    
    if( !Klesej )
    {
      int dis=(int)( (sd-CI.D+Delta/2)/Delta );
      
      if( dis>=F.FM && dis<DN )
      {
        int dism=(int)( (CI.D+Delta*(long)dis-sd) );
        
        if( dism>-maxdm && dism<maxdm )
        {
          long SX=(sx-SW2)*sd;
          int xi=(int)( (SX-CI.X+(long)UWS*D)/((long)D*UW) );
            
          if( xi>=F.LM && xi<=F.RM )
          {
            int xim=(int)((SX-CI.X)/D-xi*UW);
            
            if( xim>-UWS && xim<UWS ) 
            {
              byte *l=&F.Letka[dis][HN-1][xi];
              if( *l )
              {
                Zasah(xi,dis,l);
                StrelaL=False;
              }
            } /* if xim */
          } /* if xi */
        } /* if dism */
      } /* if dis */
    }
  } /* if StrelaL */
  if( F.NIH[HN-1]==0 && !VI.v_i && F.NI>0 ) /* sklesávání */
  {
    const int NKles = 32;
    if( Klesej==0 ) Klesej=NKles;
    else
    {
      Klesej-=ntakt;
      CI.H+=(long)UH*D/NKles*ntakt;
      if( Klesej<=0 )
      {
        int xn,dn,hn;
        
        CI.H=CIH;
        for( hn=HN-1; hn>0; hn-- )
        {
          for( xn=0; xn<XN; xn++ )
          for( dn=0; dn<DN; dn++ )
          {
            F.Letka[dn][hn][xn]=F.Letka[dn][hn-1][xn];
          }
          F.NIH[hn]=F.NIH[hn-1];
        }
        for( xn=0; xn<XN; xn++ ) for( dn=0; dn<DN; dn++ )
        {
          F.Letka[dn][0][xn]=False;
        }
        F.NIH[0]=0;
        Klesej=0;
      }
    }
  }
}

static void Rana( int xf )
{
  if( Stit>0 )
  {
    Stit-=StitKrok;
    if( Stit<0 ) Stit=0;
  }
  VD.v_i=True;
  VD.xi=xf-tx;
  VD.y=UDelH+VybHY;
  VD.Max=4<<VybDsm;
  VD.Cnt=0;
  VD.Step=1<<(VybDsm-3);
  InitKan(&Vybuch);
}

void Strely( StrelaI *S )
{
  if( S->s_i )
  {
    S->C.X+=S->D.X*ntakt;
    S->C.D+=S->D.D*ntakt;
    if( S->s_sud && S->C.D<=Sudd )
    {
      CooG G;
      int i;
      
      S->s_sud=False;
      GD(&G,&S->C);
      for( i=0; i<NSud; i++ )
      {
        int sxi=Sudyx[i];
        
        if( G.x>=sxi && G.x<sxi+SEW )
        {
          if( Sudy[i] )
          {
            RanSud(i);
            S->s_i=False;
          }
          break;
        }
      }
    }
    if( S->C.D<=td )
    {
      CooG G;
      
      GD(&G,&S->C);
      
      if( G.x>=tx && G.x<tx+TEW ) Rana(G.x);
      S->s_i=False;
    }
  }
  else if( F.NI>0 && S->StrelaP>Rnd(100) )
  {
    int xi,di,yi;
    long TDX;
    bool Sikma;
    Flag Fnd=False;
    
    TDX=(tx+TEW/2-SW2)*td;
    
    Sikma=S->SikmaP>Rnd(100);
    
    xi=(int)( (TDX-CI.X+(long)UWS*D)/((long)UW*D) );
    
    if( Sikma )
    {	
      xi=xi+Rnd(5)-2;
      if( xi<F.LM ) xi=F.LM;
      else if( xi>F.RM ) xi=F.RM;
    }
    if( xi>=F.LM && xi<=F.RM )
    {
      yi=HN-1;
      for( di=Rnd(DN*2/3); di<DN; di++ )
      {
        if( F.Letka[di][yi][xi] )
        {
          Fnd=True;
          break;
        }
      }
    }
    if( Fnd )
    {
      S->C.X=CI.X+((long)xi*UW)*D;
      S->C.H=CI.H+((long)yi*UH+StrH)*D;
      S->C.D=CI.D+((long)di*Delta);
      S->D.D=-S->ssp;
      if( Sikma )
      {
        int ns=(int)( (td-S->C.D)/S->D.D );
        long dx=(TDX-S->C.X)/ns;
        long mxd=S->ssp*(long)D/6;
        
        if( dx>+mxd ) dx=+mxd;
        if( dx<-mxd ) dx=-mxd;
        S->D.X=dx;
        S->skm=(int)( dx*2/mxd );
      }
      else
      {
        S->D.X=0;
        S->skm=0;
      }
      S->s_i=S->s_sud=True;
      InitKan(&UFOVyst);
    }
  }
}

void PulecD( void )
{
  if( PulecL )
  {
    KrAnim(px-PulHX,py-PulHY,&PulSpr);
    px+=ntakt*dxp;
    if( px>=ScrW+PulHX ) ZrusPulce();
  }
  else if( PV.v_i ) KrVyb(&PV);
  else if( PulecC<=0 )
  {
    InAnim(&PulSpr);
    PulecL=True;
    PK=InitKanLooped(&Pulec);
    px=PulHX-PEW;
    PulecC=PulecS;
  }
  else PulecC-=ntakt;
}

void VybDelo( void )
{
  if( VD.v_i )
  {
    VD.x=tx+VD.xi;
    KrVyb(&VD);
  }
}

