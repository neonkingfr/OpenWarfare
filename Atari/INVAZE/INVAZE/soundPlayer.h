#ifndef soundPlayer_h__
#define soundPlayer_h__

#ifdef __cplusplus
extern "C"
{
#endif

void setbuffer(int channel, float freq, const struct SoundWave *sound, int loop);
void setloop(int channel, int looped);
int IsBufferDone(int channel);

void StartSound();
void EndSound();

#ifdef __cplusplus
};
#endif
#endif // soundPlayer_h__
