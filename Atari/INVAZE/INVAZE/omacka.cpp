#include <windows.h>
#include "macros.h"
#include <string.h>
#include <sys/stat.h>
#include "st_input.h"

#include "dighraj.h"
#include "sprite.h"
#include "utils.h"
#include "shotsys.h"
#include "invaze.h"
#include "zvuky.h"

#include "../../GiG/gig.h"

#pragma comment(lib,"winmm.lib")

#define WD "Invaze.DTA\\"

/// terminate requested by the host
static Flag TerminatedFlag;

int KeyFire();
int KeyMoveX();
int KeyMoveY();

/// quit requested by the player
Flag QuitFlag;

#define Keybd 2

#define RLen 16
typedef word Raster[RLen][16];

static Formace Fs[]=
{
  {
    {
      {{0},{0},{1,0,0,0,1},},
      {{0},{0},{1,1,1,1,1},},
      {{0},{0},{1,0,0,0,1},},
    },
    Delta/32,
  }, /* vodor. H */
  {
    {
      {{0},{0},{1,1,1,1,1},},
      {{0},{0},{1,1,1,1,1},},
      {{0},{0},{1,1,1,1,1},},
    },
    Delta/32,
  }, /* vodor. p s */
  {
    {
      {{0,0,0,0,0},{0,0,1,0,0},{0,0,0,0,0},},
      {{0,0,1,0,0},{1,1,1,1,1},{0,0,1,0,0},},
      {{0,0,0,0,0},{0,0,1,0,0},{0,0,0,0,0},}
    },
    Delta/32,
  }, /* 3D kriz 5*3*3 */
  {
    {
      {{0},{0},{1,1,1,1,1},},
      {{0,0,1,0,0},{0,0,1,0,0},{1,1,1,1,1},},
      {{0},{0},{1,1,1,1,1},},
    },
    Delta/32,
  }, /* vodor. T */
  {
    {
      {{1,1,1,1},{1,1,1,1},{1,1,1,1},},
      {{1,1,1,1},{1,1,1,1},{1,1,1,1},},
      {{1,1,1,1},{1,1,1,1},{1,1,1,1},},
    },
    Delta/32,
  }, /* Hranol 4*3*3 */
  {
    {
      {{1,1,1,1,1},{0},{1,1,1,1,1},},
      {{1,1,1,1,1},{0,0,1,0,0},{1,1,1,1,1},},
      {{1,1,1,1,1},{0},{1,1,1,1,1},},
    },
    Delta/32,
  }, /* vodor. H */
  {
    {
      {{0,0,0,0,0},{1,0,0,0,1},{0,0,0,0,0},},
      {{1,0,0,0,1},{1,1,1,1,1},{1,0,0,0,1},},
      {{0,0,0,0,0},{1,0,0,0,1},{0,0,0,0,0},}
    },
    Delta/32,
  }, /* 2 kriz 5*3*3 */
};

static void SetFormace( Formace *f )
{
  int xn,dn,hn;
  
  for( xn=0; xn<XN; xn++ ) for( dn=0; dn<DN; dn++ )
  for( hn=0; hn<HN; hn++ ) if( f->Letka[dn][hn][xn] )
  {
    f->NI++;
    f->NIX[xn]++;
    f->NIH[hn]++;
    f->NID[dn]++;
  }
  f->LM=0;
  f->RM=XN-1;
  f->FM=0;
  while( f->NIX[f->LM]==0 ) f->LM++;
  while( f->NIX[f->RM]==0 ) f->RM--;
  while( f->NID[f->FM]==0 ) f->FM++;
}

static char TabNam[]="rekordy.tab";

static word VolVRam[VRAMSize/2];
static word PauVRam[VRAMSize/2];
static word *RekScr=NULL;

#define VV(x,y) x,ycoor(VolVRam,y)
#define VP(x,y) x,ycoor(PauVRam,y)

static int xk,yk,xd,yd;
static Flag ExitV;
static Flag ExitB,StartB,DemoB,LevelB,ReklB;
static Flag Prask;
static int DemoCnt;
enum {DemoSp=10000};

int Lvl=1;

enum
{
  TX=46,
  BX=436,
  BY1=74,BY2=136,BY3=200,BY4=264,
  BW=164,BH=44,
  PBX=68,
  PBY1=188,PBY2=284,
  PBW=346,PBH=44,
  SBX=446,SBY=188,
  SBW=154,SBH=140,
};

static void lenstr( char *S, size_t s ) /* nezakonci 0 */
{
  size_t ss=strlen(S);
  size_t sss;
  
  for( sss=ss; sss<s; sss++ ) S[sss]=' ';
}
static void TiskRec( int i )
{
  HiRec *H=&Hi[Lvl][i];
  char S[40],N[10];
  int y=BY1+4+i*32;
  
  strcpy(S,H->Jmeno);
  _ultoa(H->Body,N,10);
  {
    size_t ES=HJL+6-strlen(N);
    lenstr(S,ES);
    strcpy(&S[ES],N);
  }
  TiskSGO(VV(TX,y),S);
}

static void TiskTab( void )
{
  int i;
  
  for( i=0; i<LenHi; i++ ) TiskRec(i);
}
enum {LBX=BX+24};

static void TiskLvl( void )
{
  enum {x=LBX+7*16,y=BY3+6};
  
  char LS[10];
  
  LS[0]=Lvl+'0';
  LS[1]=0;
  TiskSG(x,ycoor(VRA,y),LS);
}

static Sprite *StrPuls[]=
{
  &Strela1,&Strela2,&Strela3,&Strela4,&Strela3,&Strela2,&Strela1
};

typedef struct
{
  Flag SavV;
  word SavKur[StrS/sizeof(word)];
  int xc0,yc0;
  Sprite *SC0;
} SavInfo;

static SavInfo CSav[2];

static void ResetSav( void )
{
  CSav[0].SavV=CSav[1].SavV=False;
}
#define coors(xx,yy) xx,ycoor(VRA,yy)

static void SupsVolVRam( void )
{
  Sups(VolVRam);
  TiskLvl();
  VRA=Shot();
  Sups(VolVRam);
  TiskLvl();
  ResetSav();
}
static void SupsPauVRam( void )
{
  Sups(PauVRam);
  VRA=Shot();
  Sups(PauVRam);
  ResetSav();
}

static void KrKur( int x, int y, Sprite *S )
{
  SavInfo *SV=&CSav[NumVRam];
  
  Save16(x,StrH,False,ycoor(VRA,y),SV->SavKur);
  Draw16(coors(x,y),S);
  SV->xc0=x;SV->yc0=y;
  SV->SC0=S;
  SV->SavV=True;
}

static void MazKur( void )
{
  SavInfo *SV=&CSav[NumVRam];
  
  if( SV->SavV )
  {
    Save16(SV->xc0,StrH,True,ycoor(VRA,SV->yc0),SV->SavKur);
    SV->SavV=False;
  }
} 

static void MovKur( int x, int y, Sprite *S )
{
  MazKur();
  KrKur(x,y,S);
}

static Flag JmenoP=False;

static char Jm[sizeof(Hi[0][0].Jmeno)];


static void EdJmeno( void )
{
  int CX=0;
  HiRec *H=&Hi[Lvl][HiPtr];
  Flag EndEd=False;
  int y=BY1+6+HiPtr*32;
  int pi=0;
  
  if( !JmenoP ) H->Jmeno[0]=0;
  else strcpy(H->Jmeno,Jm);
  while( !EndEd )
  {
    SetVsync(4,VRA);
    pi++;
    if( pi>=(int)lenof(StrPuls) ) pi=0;
    TiskRec(HiPtr);
    Sups(VolVRam);
    TiskLvl();
    Draw16(coors(TX+CX*16-8,y+8),StrPuls[pi]);
    VRA=Shot();
    if( Kbhit() )
    {
      long K=Getch();
      pi=0;
      if( (int)K==0 ) K=(int)(K>>8);
      switch( (int)K )
      {
        case 75<<8: if( CX>0 ) --CX;break;
        case 77<<8: if( CX<HJL-1 ) CX++; break;
        case 0x7f: /* delete */
          memcpy
          (
            &H->Jmeno[CX],&H->Jmeno[CX+1],
            HJL-(CX+1)
          );
          H->Jmeno[HJL-1]=0;
          break;
        case 8: /* back space */
          if( CX>0 )
          {
            CX--;
            memcpy
            (
              &H->Jmeno[CX],&H->Jmeno[CX+1],
              HJL-(CX+1)
            );
            H->Jmeno[HJL-1]=0;
          }
          break;
        case 13: EndEd=True; break;
        default:
          if( (byte)K>=' ' && CX<HJL-1 )
          {
            memcpy
            (
              &H->Jmeno[CX+1],&H->Jmeno[CX],
              HJL-(CX+1)
            );
            H->Jmeno[CX++]=(char)K;
            H->Jmeno[HJL-1]=0;
            pi=0;
          }
          break;
      }
    }
    MyVsync();
  }
  JmenoP=True;
  strcpy(Jm,H->Jmeno);
}

static Raster RovRast=
{
  {0xffff,},
  {0xffff,0xffff,},
  {0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
};

static Raster KosoRast=
{
  {0}, /* 0 */
  {0x8001,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x8001,},
  {0xc003,0x8001,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x8001,0xc003,},
  {0xe007,0xc003,0x8001,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x8001,0xc003,0xe007,},
  {0xf00f,0xe007,0xc003,0x8001,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x8001,0xc003,0xe007,0xf00f,}, /* 4 */
  {0xf81f,0xf00f,0xe007,0xc003,0x8001,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x8001,0xc003,0xe007,0xf00f,0xf81f,},
  {0xfc3f,0xf81f,0xf00f,0xe007,0xc003,0x8001,0x0000,0x0000,0x0000,0x0000,0x8001,0xc003,0xe007,0xf00f,0xf81f,0xfc3f,},
  {0xfe7f,0xfc3f,0xf81f,0xf00f,0xe007,0xc003,0x8001,0x0000,0x0000,0x8001,0xc003,0xe007,0xf00f,0xf81f,0xfc3f,0xfe7f,},
  {0xffff,0xfe7f,0xfc3f,0xf81f,0xf00f,0xe007,0xc003,0x8001,0x8001,0xc003,0xe007,0xf00f,0xf81f,0xfc3f,0xfe7f,0xffff,}, /* 8 */
  {0xffff,0xffff,0xfe7f,0xfc3f,0xf81f,0xf00f,0xe007,0xc003,0xc003,0xe007,0xf00f,0xf81f,0xfc3f,0xfe7f,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xfe7f,0xfc3f,0xf81f,0xf00f,0xe007,0xe007,0xf00f,0xf81f,0xfc3f,0xfe7f,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xfe7f,0xfc3f,0xf81f,0xf00f,0xf00f,0xf81f,0xfc3f,0xfe7f,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xfe7f,0xfc3f,0xf81f,0xf81f,0xfc3f,0xfe7f,0xffff,0xffff,0xffff,0xffff,0xffff,}, /* 12 */
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xfe7f,0xfc3f,0xfc3f,0xfe7f,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xfe7f,0xfe7f,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,}, /* 15 */
};

static Raster KrizRast=
{
  {0xffff,0x0001,0x0001,0x0001,0x0001,0x0001,0x0001,0x0001,0x0001,0x0001,0x0001,0x0001,0x0001,0x0001,0x0001,0x0001,},
  {0xffff,0xffff,0x0003,0x0003,0x0003,0x0003,0x0003,0x0003,0x0003,0x0003,0x0003,0x0003,0x0003,0x0003,0x0003,0x0003,},
  {0xffff,0xffff,0xffff,0x0007,0x0007,0x0007,0x0007,0x0007,0x0007,0x0007,0x0007,0x0007,0x0007,0x0007,0x0007,0x0007,},
  {0xffff,0xffff,0xffff,0xffff,0x000f,0x000f,0x000f,0x000f,0x000f,0x000f,0x000f,0x000f,0x000f,0x000f,0x000f,0x000f,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0x001f,0x001f,0x001f,0x001f,0x001f,0x001f,0x001f,0x001f,0x001f,0x001f,0x001f,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0x003f,0x003f,0x003f,0x003f,0x003f,0x003f,0x003f,0x003f,0x003f,0x003f,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0x007f,0x007f,0x007f,0x007f,0x007f,0x007f,0x007f,0x007f,0x007f,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0x00ff,0x00ff,0x00ff,0x00ff,0x00ff,0x00ff,0x00ff,0x00ff,},

  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0x01ff,0x01ff,0x01ff,0x01ff,0x01ff,0x01ff,0x01ff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0x03ff,0x03ff,0x03ff,0x03ff,0x03ff,0x03ff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0x07ff,0x07ff,0x07ff,0x07ff,0x07ff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0x0fff,0x0fff,0x0fff,0x0fff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0x1fff,0x1fff,0x1fff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0x3fff,0x3fff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0x7fff,},
  {0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,0xffff,},
};
static Raster SumaRast=
{
  {0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  {0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0180,0x0180,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  {0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x03C0,0x03C0,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  {0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x07E0,0x0300,0x0300,0x07E0,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000},
  {0x0000,0x0000,0x0000,0x0000,0x0000,0x0FF0,0x0630,0x0300,0x0300,0x0630,0x0FF0,0x0000,0x0000,0x0000,0x0000,0x0000},
  {0x0000,0x0000,0x0000,0x0000,0x1FF0,0x0C30,0x0600,0x0300,0x0300,0x0600,0x0C30,0x1FF0,0x0000,0x0000,0x0000,0x0000},
  {0x0000,0x0000,0x0000,0x7FF8,0x3818,0x1C00,0x0E00,0x0700,0x0700,0x0E00,0x1C00,0x3818,0x7FF8,0x0000,0x0000,0x0000},
  {0x0000,0x0000,0x7FFC,0x381C,0x1C00,0x0E00,0x0700,0x0380,0x0380,0x0700,0x0E00,0x1C00,0x381C,0x7FFC,0x0000,0x0000},
  {0x0000,0x7FFE,0x700E,0x3800,0x1C00,0x0E00,0x0700,0x0380,0x0380,0x0700,0x0E00,0x1C00,0x3800,0x700E,0x7FFE,0x0000},
  {0xFFFF,0xFFFF,0xFFFF,0x7C1F,0x3E00,0x1F00,0x0F80,0x07C0,0x07C0,0x0F80,0x1F00,0x3E00,0x7C0F,0xFFFF,0xFFFF,0xFFFF},
  {0xFFFF,0xFFFF,0xFFFF,0xFFFF,0x7F0F,0x3F80,0x1FC0,0x0FE0,0x0FE0,0x1FC0,0x3F80,0x7F0F,0xFFFF,0xFFFF,0xFFFF,0xFFFF},
  {0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0x7FCF,0x3FE0,0x1FF0,0x1FF0,0x3FE0,0x7FCF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF},
  {0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0x7FFF,0x3FF0,0x3FF0,0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF},
  {0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0x7FFF,0x7FFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF},
  {0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF},
  {0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF,0xFFFF},
};

static void Prolinej( void *S, void *s, Raster R )
{
  int i;
  
  for( i=0; i<RLen; i++ )
  {
    ResetVsync();
    Prolni(VRA,S,s,R[i]);
    VRA=Shot();
    MyVsync();
  }
}
static void Odlinej( void *S, void *s, Raster R )
{
  int i;
  
  for( i=RLen-1; i>=0; i-- )
  {
    ResetVsync();
    Prolni(VRA,S,s,R[i]);
    VRA=Shot();
    MyVsync();
  }
}

static void ObjOma( void )
{
  Prolinej(VolVRam,Pozadi,RovRast);
}
static void MizOma( void )
{
  Odlinej(VolVRam,Pozadi,RovRast);
}

static word VolPal[16];

static Flag ZacOma( void )
{
  int i,l;
  int FIn;
  
  for( i=0; i<(int)lenof(Fs); i++ ) SetFormace(&Fs[i]);
  
  for( l=0; l<Levels; l++ )
  {
    int Max=(Levels-l)*500+2000;
    int Min=500;
    int Step=(Max-Min)/(LenHi-1)/50*50;
    
    for( i=0; i<LenHi; i++ )
    {
      HiRec *H=&Hi[l][i];
      
      H->Body=Max-Step*i;
      strcpy(H->Jmeno,"SUMA");
    }
  }
  if( (FIn=Fopen(TabNam,0))>=0 )
  {
    Fread(FIn,sizeof(Hi),Hi);
    Fclose(FIn);
  }
  
  TiskSG(VV(BX+42,BY1+6),"Start");
  TiskSG(VV(BX+50,BY2+6),"Demo");
  TiskSG(VV(BX+24,BY3+6),"Level");
  TiskSG(VV(BX+42,BY4+6),"Quit");
  TiskSG(VP(PBX+142,PBY1+6),"Quit");
  TiskSG(VP(PBX+92,PBY2+6),"Continue");
  xk=SW2;
  yk=ScrH/2;
  xd=yd=0;
  Prask=ExitV=False;
  return True;
}

static void IRiz( void )
{
  xd=yd=0;
  ExitB=StartB=LevelB=DemoB=ReklB=False;
}

static void LevSel( void )
{
  Lvl++;
  if( Lvl>=Levels ) Lvl=0;
  TiskTab();
  SupsVolVRam();
}
static void Reklama( void *VF, void *VT, int Wait )
{
  if( RekScr )
  {
    int i;
    
    Prolinej(RekScr,VF,SumaRast);
    Sups(RekScr);
    SetVsync(3,VRA);
    MyVsync();
    for( i=0; Wait ? i<Wait : !JoyStat; i++ )
    {
      SetVsync(5,VRA);
      MyVsync();
      if( Kbhit() && Getch(true)>=0)
      {
        break;
      }
    }
    Prolinej(VT,RekScr,KrizRast);
    Sups(VT);
    SetVsync(3,VRA);
  }
}


static void Hraj( void )
{
  int i;
  long Dist;
  Flag Rec=False;
  enum
  {
    MinDist=Delta*6,MaxDist=8*Delta,StepDist=(MaxDist-MinDist)/5
  };
  const static int LvlStart[]={0,1,1,1,2,2};
  
  KonHraj(); /* vypnout digi player */
  MizOma();
  ZacHra0();
  #ifndef __TESTSPEED
    if( !Demo )
    {
      for( i=LvlStart[Lvl]; i<(int)lenof(Fs); i++ )
      {
        ZacForm();
        if( Exit ) goto Konec;
        for( Dist=MaxDist; Dist>=MinDist; Dist-=StepDist )
        {
          Hra(&Fs[i],Dist,i);
          if( Exit ) goto Konec;
          if( Demo ) goto VyhF;
        }
        VyhF:
        VyhrajForm();
        if( Exit ) goto Konec;
      }
      while( 1 )
      {
        ZacForm();
        if( Exit ) goto Konec;
        Hra(&Fs[lenof(Fs)-1],MinDist-StepDist,(int)lenof(Fs)-1);
        if( Exit ) goto Konec;
      }
    }
    else
    {
      ZacForm();
      if( Exit ) goto Konec;
      for( i=LvlStart[Lvl]; i<(int)lenof(Fs); i++ )
      {
        Hra(&Fs[i],MaxDist,i);
        if( Exit ) goto Konec;
      }
      VyhrajForm();
    }
  #else
    ZacForm();
    if( Exit ) goto Konec;
    Hra(&Fs[5],MaxDist,5);
  #endif
  Konec:
  KonHra0();
  
  if( Demo && RekScr ) Reklama(Pozadi,VolVRam,200);
  else ObjOma();
  IRiz();
  /* name edit testing
  if (HiPtr>=LenHi)
  {
    HiPtr = LenHi-1;
  }
  */ 
  if( !Demo && HiPtr<LenHi )
  {
    for( i=LenHi-1; i>HiPtr; i-- ) Hi[Lvl][i]=Hi[Lvl][i-1];
    Hi[Lvl][HiPtr].Body=Body;
    TiskTab();
    EdJmeno();
    {
      int FOut=_open(TabNam,_O_CREAT|_O_TRUNC|_O_WRONLY|_O_BINARY);
      
      if( FOut>=0 )
      {
        _write(FOut,Hi,sizeof(Hi));
        _close(FOut);
      }
    }
    Rec=True;
  }
  TiskTab();
  SupsVolVRam();
  IRiz();
  if (Rec) HrajSmrt(); /* zapnout digi player */
  else HrajPrelud();
}

enum {Maxxd=12,Maxyd=10};

static bool InBut( int x, int y, int wb, int hb )
{
  return xk>=x && xk<x+wb && yk>=y &&yk<y+hb;
}

static Flag BCtrl( int xb, int yb, int wb, int hb, Flag *Ctrl )
{
  if( *Ctrl )
  {
    if( xk<=xb && xd<Maxxd ) xd++;
    else if( xk>=xb+wb && xd>-Maxxd ) xd--;
    else
    {
      if( xd>0 ) xd--;
      else if( xd<0 ) xd++;
    }
    if( yk<=yb && yd<Maxyd*3/4 ) yd++;
    else if( yk>=yb+hb && yd>-Maxyd*3/4 ) yd--;
    else
    {
      if( yd>0 ) yd--;
      else if( yd<0 ) yd++;
    }
    if( InBut(xb,yb,wb,hb) )
    {
      *Ctrl=False;
      Prask=True;
    }
    return True;
  }
  return False;
}

static void Riz( void )
{
  int x = KeyMoveX();
  int y = KeyMoveY();

  if( x>0 && xd<Maxxd ) xd++;
  else if( x<0 && xd>-Maxxd ) xd--;
  else
  {
    if( xd>0 ) xd--;
    else if( xd<0 ) xd++;
  }
  if( y>0 && yd<Maxyd ) yd++;
  else if( y<0 && yd>-Maxyd ) yd--;
  else
  {
    if( yd>0 ) yd--;
    else if( yd<0 ) yd++;
  }
}

enum {StrOkr=4};

static void CRiz( void )
{
  xk+=xd;
  if( xk<StrHX-StrOkr ) {xk=StrHX-StrOkr; xd=-xd*4/5;}
  if( xk>ScrW-StrHX+StrOkr ) {xk=ScrW-StrHX+StrOkr; xd=-xd*4/5;}
  yk+=yd;
  if( yk<StrHY ) {yk=StrHY; yd=-yd*4/5;}
  if( yk>ScrH-StrHY ) {yk=ScrH-StrHY; yd=-yd*4/5;}
}

static void TestKV( void )
{
  if( Kbhit() )
  {
    int K=Getch(true);
    switch( K )
    {
      case 'Q': case 'q': ExitB=True; break;
      case 's': case 'S': StartB=True; break;
      case 'd': case 'D': DemoB=True; break;
      case 'l': case 'L': LevelB=True; break;
    }
  }
}
static void TestKP( void )
{
  if( Kbhit() )
  {
    int K=Getch(true);
    switch( K )
    {
      case 'q': case 'Q': ExitB=True; break;
      case 'p': case 'P': StartB=True; break;
      case 'c': case 'C': StartB=True; break;
    }
  }
}

static void VRiz( void )
{
  if
  (
    !BCtrl(BX,BY4,BW,BH,&ExitB) && 
    !BCtrl(BX,BY3,BW,BH,&LevelB) &&
    !BCtrl(BX,BY2,BW,BH,&DemoB) &&
    !BCtrl(BX,BY1,BW,BH,&StartB)
  ) Riz();
  CRiz();
  TestKV();
}

static void PRiz( void )
{
  if
  (
    !BCtrl(PBX,PBY1,PBW,PBH,&ExitB) && 
    !BCtrl(SBX,SBY,SBW,SBH,&ReklB) && 
    !BCtrl(PBX,PBY2,PBW,PBH,&StartB)
  ) Riz();
  CRiz();
  TestKP();
}

static Sprite *SBum[]={&Strela1,&Strela2,&Strela2,&Strela3,&Strela4};
static Sprite *SHor[]={&Strela4,&Strela3,&Strela2,&Strela2,&Strela1};

static void Bum( void )
{
  int i;
  
  for( i=0; i<(int)lenof(SBum); i++ )
  {
    SetVsync(3,VRA);
    MovKur(xk-StrHX,yk-StrHY,SBum[i]);
    Riz();
    CRiz();
    VRA=Shot();
    MyVsync();
  }
}
static void Hor( void )
{
  int i;
  
  for( i=0; i<(int)lenof(SHor); i++ )
  {
    SetVsync(3,VRA);
    MovKur(xk-StrHX,yk-StrHY,SHor[i]);
    Riz();
    CRiz();
    VRA=Shot();
    MyVsync();
  }
}

void Pauzuj( void )
{	
  Flag PExit=False;
  Flag DPlay;
  
  IRiz();
  Prolinej(PauVRam,Pozadi,KosoRast);
  SupsPauVRam();
  HrajRondo(); /* digi play on */
  DPlay=True;
  Hor();
  while( !PExit )
  {
    void mouseState(int *x, int *y, int *buttons, int *wheel);
    int x,y,buttons,wheel;
    mouseState(&x,&y,&buttons,&wheel);
    SetVsync(3,VRA);
    MovKur(xk-StrHX,yk-StrHY,&Strela1);
    PRiz();
    VRA=Shot();
    MyVsync();
    if( Prask || buttons || KeyFire())
    {
      Prask=False;
      Bum();
      if( InBut(PBX,PBY1,PBW,PBH) ) PExit=Exit=True;
      else if( InBut(PBX,PBY2,PBW,PBH) ) PExit=True;
      else if( InBut(SBX,SBY,SBW,SBH) )
      {
        if( DPlay )
        {
          KonHraj();
          DPlay=False;
        }
        Reklama(PauVRam,PauVRam,0);
        Hor();
      }
      else Hor();
    }
  }
  if( DPlay )
  {
    KonHraj(); /* digi play off */
    DPlay=False;
  }
  MazKur();
  Odlinej(PauVRam,Pozadi,KosoRast);
}

static void Volby( void )
{
  VRiz();
  if( xd || yd ) DemoCnt=DemoSp;
  if( Prask || JoyStat&0x80 || KeyFire())
  {
    DemoCnt=DemoSp;
    Prask=False;
    Bum();
    if( InBut(BX,BY4,BW,BH) ) ExitV=True;
    else
    {
      if( InBut(BX,BY1,BW,BH) )
      {
        Demo=False;Hraj();
      }
      else if( InBut(BX,BY2,BW,BH) )
      {
        Demo=True;Hraj();
      }
      else if( InBut(BX,BY3,BW,BH) ) LevSel();
      Hor();
    }
  }
}

static word Pal1[16],Pal2[16];


word SetPaletteData[16];

static void SetPalette( word *P )
{
  int i;
  
  for( i=0; i<16; i++ ) SetPaletteData[i] = P[i];
}

static void WaitObr()
{
  DWORD startTime = timeGetTime();
  while (timeGetTime()<startTime+2000)
  {
    Sleep(50);
    if (Kbhit())
    {
      Getch();
      break;
    }
  }
}

static Flag LoadDPc( const char *name, Raster R1, Raster R2, word *Pal )
{
  if( LoadPc(PauVRam,Pal,name)<0 ) return False;
  Odlinej(Pozadi,VolVRam,R1);
  SetVsync(4,VolVRam);
  MyVsync();
  SetPalette(Pal);
  memcpy(Pozadi,PauVRam,VRAMSize);
  Prolinej(Pozadi,VolVRam,R2);
  SetVsync(4,Pozadi);
  MyVsync();
  WaitObr();
  return True;
}

static Flag LoadSPc( const char *name, Raster Rast )
{
  if( LoadPc(PauVRam,Pal2,name)<0 ) return False;
  Prolinej(PauVRam,Pozadi,Rast);
  memcpy(Pozadi,PauVRam,VRAMSize);
  SetPalette(Pal2);
  SetVsync(4,Pozadi);
  MyVsync();
  WaitObr();
  return True;
}	

static Flag LoadVV()
{
  return LoadPc(VolVRam,VolPal,WD "VOLBY.PC1")>=0;
}

static Flag LoadPau()
{
  return LoadPc(PauVRam,VolPal,WD "PAUZA.PC1")>=0;
}

static Flag LoadRek()
{
  RekScr=(word *)Malloc(VRAMSize);
  return LoadPc(RekScr,VolPal,WD "REKL.PC1")>=0;
}
static void RemoRek( void )
{
  if( RekScr ) Mfree(RekScr);
}

static Flag LoadFont()
{
  long Rd;
  int MFIn = Fopen(WD "INVAZE.FNT",0);
  if (MFIn<0 ) return False;
  
  Rd=Fread(MFIn,sizeof(Font),Font);
  Fclose(MFIn);
  if( Rd!=sizeof(Font) ) return False;
  
  for( byte *b=Font; b<&Font[0x800]; b++ )
  {
    *b=~*b;
  }
  return True;
}


void InvazeMain()
{
  if( Zac() )
  {
    memset(VolVRam,0,VRAMSize);
    HrajZnelku();
    LoadDPc(WD "UVOD.PC1",RovRast,SumaRast,Pal1);
    Sleep(1000);
    #if NDEBUG
      LoadSPc(WD "INVZALD.PC1",RovRast);
    #endif
    if
    (
      #if NDEBUG
        LoadDPc(WD "TIT1.PC1",RovRast,SumaRast,Pal1) &&
      #endif
      LoadObr(WD "OBRAZKY.PC1") &&
      LoadSounds() && LoadRek() &&
      #if NDEBUG
        LoadSPc(WD "TIT2.PC1",KosoRast) && 
        LoadSPc(WD "TIT3.PC1",RovRast) &&
      #endif
      LoadFont() &&
      LoadDPc(WD "KRAJINA.PC1",KrizRast,KosoRast,Pal2) &&
      LoadVV() && LoadPau()
    )
    {
      //SetPalette(VolPal);
      if( ZacOma() )
      {
        TiskTab();
        Prolinej(VolVRam,Pozadi,SumaRast);
        SupsVolVRam();
        HrajPrelud();
        DemoCnt=DemoSp;
        while( !ExitV ) 
        {
          SetVsync(3,VRA);
          MovKur(xk-StrHX,yk-StrHY,&Strela1);
          VRA=Shot();
          MyVsync();
          Volby();
          if( Dohrano() )
          {
            if( DemoCnt<0 ) DemoB=True;
            else --DemoCnt;
          }
        }
        KonHraj();
        MazKur();
        memset(Pozadi,0,VRAMSize);
        if( RekScr ) Reklama(VolVRam,Pozadi,200);
        else Prolinej(Pozadi,VolVRam,KrizRast);
        while( JoyStat&0x80 || KeyFire());
      }
    } /* loaded obrs */
    RemoRek();
    Kon();
  } /* setrez done */
}


HANDLE KeySemaphore;

struct KeyEvent
{
  WPARAM key;
  int upDown; // 0 == up, >0 = down
};

struct KeyEvent Keys[1024];
int KeysBuffered=0;
int KeysDownBuffered=0;

CRITICAL_SECTION KeysCS;

void LockKeys()
{
  EnterCriticalSection(&KeysCS);
}

void UnlockKeys()
{
  LeaveCriticalSection(&KeysCS);
}

extern "C" char GiGDirectory[];


char GiGDirectory[1024];
static HWND g_hostWindow;

static HINSTANCE g_hInstanceGiG;

static HANDLE gameThread;
static DWORD gameThreadId;


#define GIG_DLL_FUNC(ret,name,args) extern "C" ret __declspec(dllexport) __stdcall name args;

GIG_GAME(GIG_DLL_FUNC)



BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD reason, LPVOID Reserved)
{
  if (reason==DLL_PROCESS_ATTACH)
  {
    g_hInstanceGiG = hInstance;
  }
  return TRUE;
}

DWORD WINAPI GameThread(void *context)
{
  timeBeginPeriod(1);

  InvazeMain();

  if (!TerminatedFlag)
  { // we have terminated on our own - we need to notify the owner
    __asm nop;
  }
  timeEndPeriod(1);
  return 0;
}

struct GiG_Interface GIG;

void __stdcall name_gig(char *buf, size_t bufSize)
{
  strncpy(buf,"Invaze",bufSize-1);
  buf[bufSize-1]=0;
}

HANDLE __stdcall init_gig(HWND hwnd, struct GiG_Interface *gig, const char *currentDirectory)
{
  HANDLE ret = NULL;

  g_hostWindow = hwnd;

  if (gig->version!=GIG_VERSION) return ret;

  strcpy(GiGDirectory,currentDirectory);

  GIG = *gig;
  KeySemaphore = CreateSemaphore(NULL,0,INT_MAX,NULL);

  InitializeCriticalSection(&KeysCS);

  // dbmain spawned as a thread?
  gameThread = CreateThread(NULL, 128*1024, GameThread, NULL, CREATE_SUSPENDED, &gameThreadId);

  ResumeThread(gameThread);

  DuplicateHandle(GetCurrentProcess(),gameThread,GetCurrentProcess(),&ret,0,FALSE,DUPLICATE_SAME_ACCESS);

  return ret;
}

void present_shot( unsigned long *shotObr, int scrWidth, int scrHeight, int scrBpp, unsigned int *shotPalette )
{
  GIG.present_gig(shotObr,scrWidth,scrHeight,scrBpp,shotPalette);
}

void mouseState(int *x, int *y, int *buttons, int *wheel)
{
  GIG.mouseState_gig(x,y,buttons,wheel);
}

static int MouseXReported,MouseYReported;
static int firstMouse = 1;

void mouseMovement(int *x, int *y)
{
  int buttons;
  int wheel;
  int mxCur, myCur;
  mouseState(&mxCur,&myCur,&buttons,&wheel);
  if (firstMouse)
  {
    *x = 0;
    *y = 0;
    firstMouse = 0;
  }
  else
  {
    *x = mxCur-MouseXReported;
    *y = myCur-MouseYReported;
  }
  MouseXReported = mxCur;
  MouseYReported = myCur;
}

Flag Terminated(void)
{
  return TerminatedFlag || QuitFlag;
}

int leftArrow,rightArrow,upArrow,downArrow,fire;

int KeyFire()
{
  if (fire>0)
  {
    fire = -1; // mark the value as consumed
    return 1;
  }
  return 0;
}
int KeyMoveX()
{
  return rightArrow-leftArrow;
}
int KeyMoveY()
{
  return downArrow-upArrow;
}

static int *KeyVar(int wParam)
{
  if (wParam==VK_LEFT) return &leftArrow;
  if (wParam==VK_RIGHT) return &rightArrow;
  if (wParam==VK_UP) return &upArrow;
  if (wParam==VK_DOWN) return &downArrow;
  if (wParam==VK_SPACE) return &fire;
  return NULL;
}

void __stdcall onKeyDown_gig(WPARAM wParam)
{
  int *var = KeyVar(wParam);
  if (var && *var==0) *var = 1;
  if (KeysBuffered<lenof(Keys))
  {
    LockKeys();
    Keys[KeysBuffered].key = wParam;
    Keys[KeysBuffered].upDown = 1;
    KeysBuffered++;
    KeysDownBuffered++;
    UnlockKeys();
    ReleaseSemaphore(KeySemaphore,1,NULL);
  }
}

void __stdcall onKeyUp_gig(WPARAM wParam)
{
  int *var = KeyVar(wParam);
  if (var) *var = 0;
  if (KeysBuffered<lenof(Keys))
  {
    LockKeys();
    Keys[KeysBuffered].key = wParam;
    Keys[KeysBuffered].upDown = 0;
    KeysBuffered++;
    UnlockKeys();
    ReleaseSemaphore(KeySemaphore,1,NULL);
  }
}

void __stdcall shutDown_gig()
{
  // any waiting for the key should terminate
  if (gameThread)
  {
    TerminatedFlag = True;
    // pretend a few keys to avoid getting stuck at waiting to them
    onKeyDown_gig(0);
    // caution: gameThread might request the rendering to be done
    WaitForSingleObject(gameThread,INFINITE);
    CloseHandle(gameThread),gameThread = NULL;
  }
  if (KeySemaphore) CloseHandle(KeySemaphore),KeySemaphore = NULL;

}



Flag Kbhit()
{
  return KeysDownBuffered>0 || TerminatedFlag ? True : False;
}


#include <set>

std::set<int> pressed;

int Getch(bool downonly)
{
  // we need to wait for a key
  WaitForSingleObject(KeySemaphore,TerminatedFlag ? 0 : INFINITE);
  if (KeysDownBuffered<=0)
  {
    if (TerminatedFlag)
    {
      return 0;
    }
    return -1;
  }
  else
  {
    int c = -1;
    LockKeys();
    while (KeysBuffered>0)
    {
      int cc = Keys[0].key;
      int upDown = Keys[0].upDown;
      memmove(Keys,Keys+1,(KeysBuffered-1)*sizeof(*Keys));
      KeysBuffered--;
      if (upDown)
      {
        KeysDownBuffered--;
        if (pressed.insert(cc).second || !downonly)
        {
          c = cc;
          break;
        }
      }
      else
      {
        pressed.erase(cc);
      }
    }
    UnlockKeys();
    return c;
  }
}

void MyVsync( void )
{
  // TODO: confirm: wait for vsync, which will Present - 
  Sleep(1000*ntakt/70);
}

