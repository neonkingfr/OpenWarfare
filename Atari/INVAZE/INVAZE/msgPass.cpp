#include "msgPass.h"
#include <windows.h>
#include <assert.h>

struct MessageToPass: SLIST_ENTRY
{
  size_t size;
  int id;
  #pragma warning(suppress:4200)
  char data[];
};

SLIST_HEADER Messages;
SLIST_HEADER MessagesReverse;

static struct Init
{
  Init()
  {
    InitializeSListHead(&Messages);
    InitializeSListHead(&MessagesReverse);
  }
} SInit;
void SendMsg(void *data, size_t size)
{
  MessageToPass *copy = (MessageToPass *)new char[sizeof(MessageToPass)+size];
  copy->size = size;
  static int sendId;
  copy->id = ++sendId;
  memcpy(copy->data,data,size);
  InterlockedPushEntrySList(&Messages,copy);
}

size_t GetMsg(void *data, size_t maxSize)
{
  // TODO: need to reverse order - first check the reversed list
  SLIST_ENTRY *entry = InterlockedPopEntrySList(&MessagesReverse);
  if (!entry)
  {
    SLIST_ENTRY *reverse = InterlockedFlushSList(&Messages);
    // reverse into MessagesReverse
    if (!reverse) return 0;
/*
    SLIST_HEADER temp;
    InitializeSListHead(&temp);
    while (reverse)
    {
      SLIST_ENTRY *next = reverse->Next;
      InterlockedPushEntrySList(&temp,reverse);
      reverse = next;
    }
    reverse = InterlockedFlushSList(&temp);
*/
    while (reverse)
    {
      SLIST_ENTRY *next = reverse->Next;
      InterlockedPushEntrySList(&MessagesReverse,reverse);
      reverse = next;
    }
    entry = InterlockedPopEntrySList(&MessagesReverse);
  }
  MessageToPass *msg = static_cast<MessageToPass *>(entry);
  static int getId;
  assert(msg->id = ++getId);
  size_t size = msg->size;
  assert (size<=maxSize);
  memcpy(data,msg->data,size);
  delete[] (char *)msg;
  return size;
}
