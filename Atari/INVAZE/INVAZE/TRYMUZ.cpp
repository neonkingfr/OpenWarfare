#include "macros.h"
#include "digimus.h"
#include "nastroje.h"
#include "hudba.h"

static unsigned EMel[]=
{
  end
};

Nastroj *NTab[]=
{
  &Pauza, &Turbo5, &Turbo16, &Turbo17, &Turbo2, &Turbo11, &Turbo12
};
enum {ppp,Piano, SoloSynt, SoloSynt1, Sinus, Ddur, Adur};

#define td 4
#define s td*2
#define o td*4
#define q	td*8
#define pl td*16

static unsigned Ron1[]=
{
  #define np 0,ins,Piano
  np,
  G,o,G,o,G,o,
  e,q,c,q,H,q,d,q,
  c,q,e,q,d,o,d,o,d,o,d,o,
  e,pl,p,o,np,f,o,e,o,d,o,
  p,o,np,c,o,c,o,c,o,H,o,e,o,e,o,e,o, /* 1.r */
  A,o,e,o,c,o,e,o,G_,o,e,o,d,o,e,o,
  A,o,e,o,c,o,e,o,c,pl,
  h,o,d1,o,c1,o,h,o,a,o,c1,o,h,o,a,o,
  g,q,g,q,g,o,g,o,g,o,g,o, /* 2.r */
  F,s,G,s,H,s,G,s,E,s,G,s,c,s,G,s,D,s,G,s,d,s,G,s,C,s,G,s,e,s,G,s,
  D,s,G,s,f,s,G,s,C_,s,G,s,e,s,G,s,f,o,g,o,g,o,g,o,
  H,s,d,s,g,s,d,s,c,s,e,s,g,s,e,s,f,s,g,s,e,s,g,s,d,s,g,s,c,s,g,s,/* 3.r */
  H,s,d,s,g,s,d,s,A,s,c,s,g,s,c,s,H,o,g,o,g,o,g,o,
  F,s,G,s,H,s,G,s,E,s,G,s,c,s,G,s,D,s,G,s,d,s,G,s,C,s,G,s,e,s,G,s,
  D,s,G,s,f,s,G,s,C_,s,G,s,e,s,G,s,f,o,g,o,g,o,g,o,/*4.r*/
  H,s,d,s,g,s,d,s,c,s,e,s,g,s,e,s,f,s,g,s,e,s,g,s,d,s,g,s,c,s,g,s,
  g,o,g,o,g,o,g,o,g,o,G,o,G,o,G,o,
  e,q,c,q,H,q,d,q,
  c,q,e,q,d,o,d,o,d,o,d,o,/*5.r*/
  e,pl,p,o,np,f,o,e,o,d,o,
  p,o,np,c,o,c,o,c,o,H,o,G,o,G,o,G,o,
  e,q,c,q,H,q,d,q,
  c,q,e,q,d,o,d_,o,d_,o,d_,o,/*6.r*/	
  G1,o,e,o,G1,o,c,o,G1,o,d,o,G1,o,H,o,
  c,q,e,q,c,o,d,o,d,o,d,o,
  g,o,d,o,d,o,d,o,a,o,d,o,d,o,d,o,
  h,o,c1,o,d1,o,h,o,g,o,d1,o,d1,o,d1,o,/*7.r*/
  c1,o,c1,o,a,o,a,o,h,o,h,o,g,o,g,o,
  a,q,c1,q,c1,o,d,o,d,o,d,o,
  g,o,d,o,d,o,d,o,a,o,d,o,d,o,d,o,
  h,o,c1,o,d1,o,h,o,g,o,h,o,h,o,h,o,/*8.r*/
  h,o,g,o,e,o,g,o,h,o,a,o,g,o,f_,o,
  e,o,g,o,h,o,g,o,e,o,d1,o,d1,o,d1,o,
  c1,o,a,o,f_,o,d,o,c,o,A,o,F_,o,A,o,
  G,s,H,s,d,s,H,s,H,s,d,s,g,s,d,s,d,s,g,s,h,s,g,s,d1,o,d1,o,/*9.r*/
  c1,o,a,o,f_,o,d,o,c,o,A,o,F_,o,A,o,
  G,s,H,s,d,s,H,s,H,s,d,s,g,s,d,s,d,s,g,s,h,s,g,s,d1,o,d1,o,
  E,s,g,s,c1,s,g,s,c1,o,c1,o,D,s,f_,s,h,s,f_,s,h,o,h,o,
  C,s,e,s,a,s,e,s,a,o,a,o,H1,s,d,s,g,s,d,s,g,o,g,o,/*10.r*/
  f_,o,e,o,d,o,c,o,H,o,d,o,c,o,A,o,
  G,o,H,o,d,o,g,o,h,o,d1,o,d1,o,d1,o,
  c1,o,h,o,f_,o,d,o,c,o,A,o,F_,o,A,o,
  G,s,H,s,d,s,H,s,H,s,d,s,g,s,d,s,d,s,g,s,h,s,g,s,d1,o,d1,o,/*11.r*/
  c1,o,a,o,f_,o,d,o,c,o,A,o,F_,o,A,o,
  G,s,H,s,d,s,H,s,H,s,d,s,g,s,d,s,d,s,g,s,h,s,g,s,d1,o,d1,o,
  E,s,g,s,c1,s,g,s,c1,o,c1,o,D,s,f_,s,h,s,f_,s,h,o,h,o,
  C,s,e,s,a,s,e,s,a,o,a,o,H1,s,d,s,g,s,d,s,g,o,g,o,/*12.r*/
  f_,o,e,o,d,o,c,o,H,o,d,o,c,o,A,o,
  G,o,H,o,A,o,c,o,H,o,d,o,c,o,A,o,
  G,o,H,o,A,o,c,o,H,o,d,o,c,o,A,o,
  G,o,G,o,G,o,G,o,G,o,H,o,H,o,H,o,
  H,o,d,o,d,o,d,o,d,o,f,o,f,o,f,o,/*13.r*/
  f,o,d,o,d,o,d,o,d,o,H,o,H,o,H,o,
  H,o,G,o,G,o,G,o,G,o,G,o,G,o,G,o,
  e,q,c,q,H,q,d,q,
  c,q,e,q,d,o,f,o,f,o,f,o,
  e,pl,p,o,np,f,o,e,o,d,o,/*14.r*/
  p,o,np,c,o,c,o,c,o,H,o,G,o,G,o,G,o,
  e,q,c,q,H,q,d,q,
  c,q,e,q,d,o,d_,o,d_,o,d_,o,
  G1,o,e,o,G1,o,c,o,G1,o,d,o,G1,o,H,o,
  c,q,e,q,c,o,e,o,e,o,e,o,/*15.r*/
  a,o,e,o,e,o,e,o,h,o,e,o,e,o,e,o,
  c1,q+o,h,o,a,o,c1,o,h,o,a,o,
  h,o,g_,o,e,o,d,o,H,o,G_,o,e,o,d,o,
  c,o,e,o,a,o,h,o,c1,o,e,o,e,o,e,o,
  a,o,e,o,e,o,e,o,h,o,e,o,e,o,e,o,/*16.r*/
  c1,q+o,h,o,a,o,a,o,a,o,g,o,
  f,o,f,o,f,o,e,o,d_,o,d_,o,d_,o,d_,o,
  e,pl,p,o,np,e1,o,e1,o,e1,o,	

  d1,o,h,o,g_,o,e,o,d,o,H,o,G_,o,H,o,
  A1,s,c_,s,e,s,c_,s,c_,s,e,s,a,s,e,s,e,s,a,s,c_1,s,a,s,e1,o,e1,o,/*17.r*/
  d1,o,h,o,g_,o,e,o,d,o,H,o,G_,o,H,o,
  A1,s,c_,s,e,s,c_,s,c_,s,e,s,a,s,e,s,e,s,a,s,c_1,s,a,s,e1,o,e1,o,
  F_,s,a,s,d1,s,a,s,d1,o,d1,o,
  E,s,g_,s,c_1,s,g_,s,c_1,o,c_1,o,
  D,s,f_,s,h,s,f_,s,h,o,h,o,
  C_,s,e,s,a,s,e,s,a,o,a,o,/*18.r*/
  g_,o,f_,o,e,o,d,o,c_,o,e,o,d,o,H,o,
  A,o,c_,o,e,o,a,o,c_1,o,e1,o,e1,o,e1,o,
  d1,o,h,o,g_,o,e,o,d,o,H,o,G_,o,H,o,
  A1,s,c_,s,e,s,c_,s,c_,s,e,s,a,s,e,s,e,s,a,s,c_1,s,a,s,e1,o,e1,o,/*19.r*/
  d1,o,h,o,g_,o,e,o,d,o,H,o,G_,o,H,o,
  A1,s,c_,s,e,s,c_,s,c_,s,e,s,a,s,e,s,e,s,a,s,c_1,s,a,s,e1,o,e1,o,
  F_,s,a,s,d1,s,a,s,d1,o,d1,o,
  E,s,g_,s,c_1,s,g_,s,c_1,o,c_1,o,
  D,s,f_,s,h,s,f_,s,h,o,h,o,
  C_,s,e,s,a,s,e,s,a,o,a,o,/*20.r*/
  g_,o,f_,o,e,o,d,o,c_,o,H,o,A,o,G_,o,
  e,q+o,e,o,d,o,c,o,H,o,A,o,
  e,q+o,e,o,d,o,c,o,H,o,A,o,
  e,o,e,o,e,o,e,o,e,o,H,o,H,o,H,o,
  H,o,G,o,G,o,G,o,G,o,E,o,E,o,E,o,/*21.r*/
  f,o,f,o,f,o,f,o,f,o,d,o,d,o,d,o,
  d,o,H,o,H,o,H,o,H,o,G,o,G,o,G,o,
  e,q,c,q,H,q,d,q,
  c,q,e,q,d,o,f,o,f,o,f,o,/*22.r*/
  e,pl,p,o,np,f,o,e,o,d,o,
  p,o,np,c,o,c,o,c,o,H,o,e,o,e,o,e,o,
  a,q,a,q,h,q,h,q,
  c1,q,c1,q,e,o,c1,o,h,o,a,o,/*23.r*/
  h,o,d1,o,c1,o,h,o,a,o,c1,o,h,o,a,o,
  g,q,g,q,g,o,g,o,g,o,g,o,
  F,s,G,s,H,s,G,s,E,s,G,s,c,s,G,s,D,s,G,s,d,s,G,s,C,s,G,s,e,s,G,s,/*24.r*/
  D,s,G,s,f,s,G,s,C_,s,G,s,e,s,G,s,f,o,g,o,g,o,g,o,
  H,s,d,s,g,s,d,s,c,s,e,s,g,s,e,s,f,s,g,s,e,s,g,s,d,s,g,s,c,s,g,s,
  H,s,d,s,g,s,d,s,A,s,c,s,g,s,c,s,H,o,g,o,g,o,g,o,/*25.r*/
  F,s,G,s,H,s,G,s,E,s,G,s,c,s,G,s,D,s,G,s,d,s,G,s,C,s,G,s,e,s,G,s,
  D,s,G,s,f,s,G,s,C_,s,G,s,e,s,G,s,f,o,g,o,g,o,g,o,/*26.r*/
  H,s,d,s,g,s,d,s,c,s,e,s,g,s,e,s,f,s,g,s,e,s,g,s,d,s,g,s,c,s,g,s,
  g,o,g,o,g,o,g,o,g,o,G,o,G,o,G,o,
  e,q,c,q,H,q,d,q,/*27.r*/
  c,q,e,q,d,o,d,o,d,o,d,o,
  e,pl,p,o,np,f,o,e,o,d,o,
  p,o,np,c,o,c,o,c,o,H,o,G,o,G,o,G,o,
  e,q,c,q,H,q,d,q,/*28.r*/
  c,q,e,q,d,o,d_,o,d_,o,d_,o,
  G1,o,e,o,G1,o,c,o,G1,o,d,o,G1,o,H,o,
  C,s,G,s,E,s,G,s,c,s,G,s,E,s,G,s,C,s,G_,s,F,s,G_,s,H,s,G_,s,F,s,G_,s,/*29.r*/
  C,s,G,s,E,s,G,s,c,s,G,s,E,s,G,s,C,s,G_,s,F,s,G_,s,H,s,G_,s,F,s,G_,s,
  c,s,g,s,e,s,g,s,c1,s,g,s,e,s,g,s,c,s,g_,s,f,s,g_,s,h,s,g_,s,f,s,g_,s,/*30.r*/
  c,s,g,s,e,s,g,s,c1,s,g,s,e,s,g,s,c,s,g_,s,f,s,g_,s,h,s,g_,s,f,s,g_,s,
  c1,q+o,c1,o,c1,q,c1,q,
  c1,pl,c,q+o,c,o,
  c,pl,p,pl,/*31.r*/
  
  end,
};

static unsigned Ron2[]=
{
  np,
  p,q+o,
  C,pl+pl,
  C,pl,C,o,np,f,o,f,o,f,o,
  c,pl,p,o,np,A,o,G,o,F,o,
  p,o,np,E,o,E,o,E,o,D,o,p,3*o,np, /* 1.r */
  a,q,a,q,h,q,h,q,
  c1,q,c1,q,e,o,c1,o,h,o,a,o,
  p,pl+pl,np,
  H,q,H,q,H,q,p,q, /* 2.r */
  p,pl+pl,C,pl,np,G,o,p,3*o,C,pl+pl,/*3.r*/
  C,2*(pl+pl),C,pl,np,G,o,p,3*o,/*4.r*/
  C,pl+pl,np,d,q,p,pl+q,C,pl+pl,
  C,pl+o,np,f,o,f,o,f,o,/*5.r*/
  c,pl,p,o,np,A,o,G,o,F,o,
  p,o,np,E,o,E,o,E,o,D,o,p,o+q,
  p,pl+pl+pl+o,np,c,o,c,o,c,o,/*6.r*/
  p,o,np,G,o,p,o,np,E,o,p,o,np,F,o,p,o,np,D,o,
  E,q,G,q,E,o,p,o+q,
  np,d,o,p,q+o,np,d,o,p,q+o,
  C,pl+pl,/*7.r*/
  np,a,o,a,o,d,o,d,o,g,o,g,o,H,o,H,o,
  p,q,np,f_,q,f_,o,p,q+o,
  np,d,o,p,o+q,np,d,o,p,o+q,
  C,pl+pl,/*8.r*/
  np,g,o,p,q+o,np,f_,o,p,q+o,
  p,pl+pl,
  np,f_,o,p,o+q+pl,
  C,pl+pl,/*9.r*/
  np,f_,o,p,o+q+pl,
  C,pl+pl,C,pl-o,np,g,o,p,pl-o,np,f_,o,
  p,pl-o,np,a,o,p,pl-o,np,g,o,/*10.r*/
  p,(pl+pl)*2,np,f_,o,p,pl+q+o,p,pl+pl,/*11.r*/
  np,f_,o,p,o+q+pl,
  C,pl+pl,C,pl-o,np,g,o,p,pl-o,np,f_,o,
  p,pl-o,np,a,o,p,pl-o,np,g,o,/*12.r*/
  p,(pl+pl)*5,/*13.r.*/
  p,(pl+pl)*3,C,pl+o,np,d,o,d,o,d,o,
  c,pl,p,o,np,A,o,G,o,F,o,/*14.r*/
  p,o,np,E,o,E,o,E,o,D,o,p,q+o,
  C,pl+pl+pl+o,np,c,o,c,o,c,o,
  p,o,np,G,o,p,o,np,E,o,p,o,np,F,o,p,o,np,D,o,
  E,q,G,q,E,o,p,q+o,/*15.r*/
  np,e,o,p,o+q,np,d,o,p,o+q,
  np,e,q+o,p,o+pl,
  C,(pl+pl)*2,
  np,e,o,p,o+q,np,d,o,p,o+q,/*16.r*/
  np,e,q+o,p,pl+o+pl+o,A,o,A,o,A,o,
  H,pl,p,pl,

  np,g_,o,p,pl+q+o,C,pl+pl,/*17.r*/
  np,g_,o,p,pl+q+o,C,pl+pl,
  C,q+o,np,a,o,p,q+o,np,g_,o,
  p,q+o,np,f_,o,p,q+o,np,e,o,/*18.r*/
  p,(pl+pl)*2,
  np,g_,o,p,pl+q+o,C,pl+pl,/*19.r*/
  np,g_,o,p,pl+q+o,C,pl+pl,
  C,q+o,np,a,o,p,q+o,np,g_,o,
  p,q+o,np,f_,o,p,q+o,np,e,o,/*20.r*/
  p,pl+pl,np,H,q+o,p,pl+o,
  np,H,q+o,p,pl+o,
  np,H,o,p,pl+o+q,C,pl+pl,/*21.r*/

  np,H,o,p,pl+q+o,C,(pl+pl)*2,
  C,pl+o,np,d,o,d,o,d,o,/*22.r*/
  c,pl,p,o,np,A,o,G,o,F,o,
  p,o,np,E,o,E,o,E,o,D,o,p,q+o,
  C,pl+pl+pl,c,o,p,q+o,/*23.r*/
  C,pl+pl,np,
  H,q,H,q,H,q,p,q,
  p,pl+pl,C,pl,np,G,o,p,3*o,C,pl+pl,
  C,2*(pl+pl),C,pl,np,G,o,p,3*o,
  C,pl+pl,np,d,q,p,pl+q,C,pl+pl,
  C,pl+o,np,f,o,f,o,f,o,
  c,pl,p,o,np,A,o,G,o,F,o,
  p,o,np,E,o,E,o,E,o,D,o,p,o+q,
  p,pl+pl+pl+o,np,c,o,c,o,c,o,/*28.r*/
  p,o,np,G,o,p,o,np,E,o,p,o,np,F,o,p,o,np,D,o,
  p,(pl+pl)*4,
  np,g,q+o,g,o,g,q,g,q,g,pl,G,q+o,G,o,
  G,pl,p,pl,

  end,
};

static unsigned Ron3[]=
{
  p,	
  p,q+o,
  C,pl+pl,C,pl+pl,C,pl+pl,C,pl+pl, /* 1.r */
  C,pl+pl,C,pl,np,A,o,p,3*o,C,pl+pl,C,pl+pl, /* 2.r */
  C,(pl+pl)*3,/*3.r*/
  C,(pl+pl)*3,/*4.r*/
  C,pl+pl,np,h,q,p,pl+q,C,pl+pl,C,pl+pl, /* 5.r */
  C,(pl+pl)*3,C,pl+o,np,F_,o,F_,o,F_,o,/*6.r*/
  p,(pl+pl)*2,np,H,o,p,o+q,np,c,o,p,o+q,C,pl+pl,/*7.r*/
  C,(pl+pl)*2,np,H,o,p,o+q,np,c,o,p,o+q,C,pl+pl,/*8.r*/
  C,pl,np,d_,o,p,o+q,C,pl+pl,np,d,o,p,o+q+pl,C,pl+pl,/*9.r*/
  np,d,o,p,o+q+pl,C,(pl+pl)*3,/*10.r*/
  C,(pl+pl)*2,np,d,o,p,pl+q+o,p,pl+pl,/*11.r*/
  np,d,o,p,o+q+pl,C,(pl+pl)*3,/*12.r*/
  C,(pl+pl)*5,/*13.r.*/
  C,(pl+pl)*5,/*14.r.*/
  C,(pl+pl)*2,C,pl+o,F_,o,F_,o,F_,o,p,(pl+pl)*2,/*15.r.*/
  np,c,o,p,o+q+pl,
  np,c,o+q,p,pl+o,C,(pl+pl)*2,
  np,c,o,p,o+q+pl,/*16.r*/
  np,c,o+q,p,pl+o+pl+pl,np,G_,pl,
  p,pl,

  np,e,o,p,pl+q+o,C,pl+pl,/*17.r*/
  np,e,o,p,pl+q+o,C,pl+pl,C,(pl+pl)*2,/*18.r*/
  C,(pl+pl)*2,np,e,o,p,pl+q+o,C,pl+pl,/*19.r*/
  np,e,o,p,pl+q+o,C,pl+pl,C,(pl+pl)*2,/*20.r*/
  C,pl+pl,np,G_,q+o,p,pl+o,np,G_,q+o,p,pl+o,
  np,G_,o,p,pl+o+q,C,pl+pl,/*21.r*/

  np,G,o,p,pl+q+o,C,(pl+pl)*3,/*22.r*/
  C,(pl+pl)*3+pl,np,A,o,p,q+o,/*23.r*/
  C,(pl+pl)*8,
  C,pl+pl,np,h,q,p,pl+q,C,pl+pl,C,pl+pl,
  C,(pl+pl)*3,C,pl+o,np,F_,o,F_,o,F_,o,
  p,(pl+pl)*5,
  np,e,q+o,e,o,e,q,e,q,e,pl,E,q+o,E,o,
  E,pl,p,pl,
  end,
};
static unsigned Ron4[]=
{
  #undef np
  #define np 0,ins,SoloSynt

  np,
  p,q+o,np,
  c,o,g,o,e,o,g,o,d,o,g,o,f,o,g,o,
  e,o,g,o,c,o,g,o,H,q,G,q,
  c,o,H,o,A,o,G,o,F,pl,
  G,pl,G,o,p,3*o, /* 1.r */
  C,pl+pl,
  C,pl+pl,
  np,g,q,g,q,d,q,d,q,
  g,o,d,o,H,o,d,o,G,o,p,3*o, /* 2.r */
  np,f,q,e,q,d,q,c,q,
  d,q,c_,q,d,o,p,q+o,
  np,h,q,c1,q,f1,o,e1,o,d1,o,c1,o,/*3.r*/
  h,q,a,q,g,o,p,q+o,
  np,f,q,e,q,d,q,c,q,
  d,q,c_,q,d,o,p,q+o,/*4.r*/
  np,h,q,c1,q,f1,o,e1,o,d1,o,c1,o,
  g,q,p,q,C,pl,
  np,c,o,g,o,e,o,g,o,d,o,g,o,f,o,g,o,
  e,o,g,o,c,o,g,o,H,q,G,q, /*5.r*/
  c,o,H,o,A,o,G,o,F,pl,
  G,pl,G,o,p,o+q,
  np,c,o,g,o,e,o,g,o,d,o,g,o,f,o,g,o,
  e,o,g,o,c,o,g,o,H,o,A,o,A,o,A,o,/*6.r*/
  G,q,G,q,G,q,G,q,
  c,q,c,q,c,o,p,q+o,
  np,g,q,p,q,np,f_,q,p,q,
  np,g,pl,g,pl,/*7.r*/
  p,pl+pl,np,d1,q,d,q,d,o,p,q+o,
  np,g,q,p,q,np,f_,q,p,q,
  np,g,pl,g,o,p,o+q,/*8.r*/
  np,e1,q,p,q,np,h,q,p,q,
  np,e1,q,p,q,np,e1,o,p,o+q,
  np,d,q,p,pl,np,f_,q,
  g,q,h,q,d1,q,p,q,/*9.r*/
  np,d,q,p,pl,np,f_,q,
  g,q,h,q,d1,q,p,q,
  np,e,q+o,e,o,d,q+o,d,o,c,q+o,c,o,H,q+o,H,o,/*10.r*/
  c,q,A,q,d,q,d,q,
  G,q,p,pl+q,np,d,q,p,pl,np,f_,q,
  g,q,h,q,d1,q,p,q,/*11.r*/
  np,d,q,p,pl,np,f_,q,
  g,q,h,q,d1,q,p,q,
  np,e,q+o,e,o,d,q+o,d,o,c,q+o,c,o,H,q+o,H,o,/*12.r*/
  c,q,A,q,d,q,d,q,
  G,q,p,pl,np,d,q,G,q,p,pl,np,d,q,
  G,o,p,pl+q+o,C,pl+pl,/*13.r*/
  p,(pl+pl)*2,np,c,o,g,o,e,o,g,o,d,o,g,o,f,o,g,o,
  e,o,g,o,c,o,g,o,H,q,G,q,
  c,o,H,o,A,o,G,o,F,pl,/*14.r*/
  G,pl,G,o,p,o+q,
  np,c,o,g,o,e,o,g,o,d,o,g,o,f,o,g,o,
  e,o,g,o,c,o,g,o,H,o,A,o,A,o,A,o,
  G,q,G,q,G,q,G,q,
  c,q,c,q,c,o,p,o+q,/*15.r*/
  np,a,q,p,q,np,g_,q,p,q,
  np,a,q+o,p,pl+o,
  np,E,pl,E,pl,
  A,pl,A,o,p,q+o,
  np,a,q,p,q,np,g_,q,p,q,/*16.r*/
  np,a,q+o,p,pl+o,
  C,pl+o,np,f,o,f,o,f,o,
  e,pl,p,pl,

  np,E,pl,p,pl,np,A,q,c_1,q,e1,q,p,q,/*17.r*/
  np,E,pl,p,pl,np,A,q,c_1,q,e1,q,p,q,
  np,f_,q+o,f_,o,e,q+o,e,o,
  d,q+o,d,o,c_,q+o,c_,o,/*18.r*/
  d,pl,e,q,E,q,A,q,p,pl+q,
  np,E,pl,p,pl,np,A,q,c_1,q,e1,q,p,q,/*19.r*/
  np,E,pl,p,pl,np,A,q,c_1,q,e1,q,p,q,
  np,f_,q+o,f_,o,e,q+o,e,o,
  d,q+o,d,o,c_,q+o,c_,o,/*20.r*/
  d,pl,d,q,d_,q,
  e,pl,f,pl,e,pl,f,pl,
  e,o,p,pl+q+o,C,pl+pl,/*21.r*/

  np,d,o,p,pl+q+o,C,pl+pl,
  np,c,o,g,o,e,o,g,o,d,o,g,o,f,o,g,o,
  e,o,g,o,c,o,g,o,H,q,G,q,/*22.r*/
  c,o,H,o,A,o,G,o,F,pl,
  G,pl,G,o,p,o+q,
  np,a,o,e1,o,c1,o,e1,o,g_,o,e1,o,d1,o,e1,o,
  a,o,e1,o,c1,o,e1,o,a,o,p,q+o,/*23.r*/
  np,g,q,g,q,d,q,d,q,
  g,o,d,o,H,o,d,o,G,o,p,3*o,
  np,f,q,e,q,d,q,c,q,
  d,q,c_,q,d,o,p,q+o,
  np,h,q,c1,q,f1,o,e1,o,d1,o,c1,o,/*3.r*/
  h,q,a,q,g,o,p,q+o,
  np,f,q,e,q,d,q,c,q,
  d,q,c_,q,d,o,p,q+o,/*4.r*/
  np,h,q,c1,q,f1,o,e1,o,d1,o,c1,o,
  g,q,p,q,C,pl,
  np,c,o,g,o,e,o,g,o,d,o,g,o,f,o,g,o,
  e,o,g,o,c,o,g,o,H,q,G,q, /*5.r*/
  c,o,H,o,A,o,G,o,F,pl,
  G,pl,G,o,p,o+q,
  np,c,o,g,o,e,o,g,o,d,o,g,o,f,o,g,o,
  e,o,g,o,c,o,g,o,H,o,A,o,A,o,A,o,/*6.r*/
  G,q,G,q,G,q,G,q,
  c,o,e,q,e,o,c,o,f,q,f,o,
  c,o,e,q,e,o,c,o,f,q,f,o,
  c1,o,e1,q,e1,o,c1,o,f1,q,f1,o,
  c1,o,e1,q,e1,o,c1,o,f1,q,f1,o,
  np,c,q+o,c,o,c,q,c,q,c,pl,C,q+o,C,o,
  C,pl,p,pl,
  end,
};

void *Rondo[]={Ron1,Ron2,Ron3,Ron4};

#undef td
#define td 4
#define s td*2
#define o td*4
#define q	td*8
#define pl td*16

static unsigned int Prel1[]=
{
  0,ins,SoloSynt,
  e,q,g,q,e,q,g,q,
  d_,q,a,q,d_,q,a,q,
  f_,q,h,q,f_,q,h,q,
  e,q,g,q,e,q,g,q,
  E,q,g,q,E,q,g,q,
  A,q,c1,q,A,q,c1,q,
  H,q,h,q,H,q,h,q,
  e,q,g,q,e,q,g,q,

  C,q,G,q,C,q,G,q,
  D,q,G,q,D,q,G,q,
  H1,q,D,q,H1,q,D,q,
  E,q,G,q,E,q,G,q,
  0,ins,SoloSynt1,
  E,q,g,q,E,q,g,q,
  F,q,f,q,F,q,f,q,
  F_,q,d,q,F_,q,d,q,
  0,ins,SoloSynt,
  G1,q,D,q,G1,q,D,q,

  d_,q,a,q,d_,q,a,q,
  e,q,g,q,e,q,g,q,
  d_,q,a,q,d_,q,a,q,
  e,q,g,q,e,q,g,q,
  G,q,g,q,G,q,g,q,
  A,q,a,q,A,q,a,q,
  H,q,f_,q,H,q,f_,q,
  e,q,g,q,e,q,g,q,
  E,q,g,q,E,q,g,q,
  A,q,c1,q,A,q,c1,q,
  H,q,h,q,H,q,h,q,

  E,pl,p,pl,
  end
};
static unsigned int Prel2[]=
{
  0,ins,Piano,
  0,rpt,4,E,o,H1,o,0,erp,
  0,rpt,4,F_,o,H1,o,0,erp,
  0,rpt,4,A,o,D_,o,0,erp,
  0,rpt,4,G,o,H1,o,0,erp,
  0,rpt,4,H,o,H1,o,0,erp,
  0,rpt,4,A,o,E,o,0,erp,
  0,rpt,4,F_,o,D_,o,0,erp,
  0,rpt,4,E,o,H1,o,0,erp,
  0,ins,Sinus,
  0,rpt,4,E,o,C,o,0,erp,
  0,rpt,4,F,o,H1,o,0,erp,
  0,rpt,4,D,o,G1,o,0,erp,
  0,rpt,4,G,o,C,o,0,erp,
  0,rpt,4,C_,o,B1,o,0,erp,
  0,rpt,4,D,o,A1,o,0,erp,
  0,rpt,4,C,o,A1,o,0,erp,
  0,rpt,4,H1,o,G1,o,0,erp,
  0,ins,Piano,
  0,rpt,4,F_,o,H1,o,0,erp,
  0,rpt,4,G,o,H1,o,0,erp,
  0,rpt,4,F_,o,H1,o,0,erp,
  0,rpt,4,G,o,H1,o,0,erp,
  0,rpt,4,E,o,H1,o,0,erp,
  0,rpt,4,F_,o,C,o,0,erp,
  0,rpt,4,D_,o,A1,o,0,erp,
  0,rpt,4,G,o,H1,o,0,erp,
  0,rpt,4,H,o,H1,o,0,erp,
  0,rpt,4,A,o,E,o,0,erp,
  0,rpt,4,F_,o,D_,o,0,erp,
  0,ins,SoloSynt,
  e,pl,p,pl,
  end
};
void *Pl[]={Prel1, Prel2, 0, 0};

#undef td
#define td 6

static unsigned int Smrt1[]=
{
  0,rpt,2,
    0,ins,Piano,
    A1,o,
    A1,s,D,s,F_,o,D,o,
    D,o,C_,o,A1,o,
    A1,s,C_,s,G,o,E,o,
    E,o,D,o,A1,o,
    A1,s,D,s,F_,o,D,o,
    A1,s,C_,s,G,o,E,o,
    F_,s,D,s,A,s,F_,s,D,s,C_,s,
    D,o,p,o,
  0,erp,
  0,rpt,2,
    0,ins,Piano,
    A1,o,
    A1,s,C_,s,E,o,E,o,
    A1,s,D,s,F_,o,F_,o,
    A1,s,C_,s,G,o,G,o,
    F_,o,E,o,D,o,
    A1,s,C_,s,E,o,E,o,
    A1,s,D,s,F_,o,F_,o,
    A1,s,C_,s,G,o,F_,o,
    E,o,p,o,
    0,ins,Piano,
    A1,o,
    A1,s,D,s,F_,o,D,o,
    D,o,C_,o,A1,o,
    A1,s,C_,s,G,o,E,o,
    E,o,D,o,A1,o,
    A1,s,D,s,F_,o,D,o,
    A1,s,C_,s,G,o,E,o,
    F_,s,D,s,A1,s,F_,s,D,s,C_,s,
    D,o,p,o,
  0,erp,

  end
};
static unsigned int Smrt2[]=
{
  0,rpt,2,
    p,o,
    0,ins,SoloSynt,
    d,q+o,
    A,q+o,
    A,q+o,
    d,q+o,
    d,q+o,
    A,q+o,
    d,q,A,o,
    d,o,p,o,
  0,erp,
  0,rpt,2,
    p,o,
    0,ins,Adur,
    C,q,C,o,
    0,ins,Ddur,
    C,q,C,o,
    0,ins,Adur,
    C,q,C,o,
    0,ins,Ddur,
    C,q+o,
    0,ins,Adur,
    C,q,C,o,
    0,ins,Ddur,
    C,q,C,o,
    0,ins,Adur,
    C,q,
    0,ins,Ddur,
    C,o,
    0,ins,Adur,
    C,o,p,q,
    0,ins,SoloSynt,
    d,q+o,
    A,q+o,
    A,q+o,
    d,q+o,
    d,q+o,
    A,q+o,
    0,ins,Ddur,
    C,q,
    0,ins,Adur,
    C,o,
    0,ins,Ddur,
    C,o,p,o,
  0,erp,
  end
};

void *Sm[]={Smrt2,Smrt1,0, 0};

#undef td
#define td 5
#define s td*2
#define o td*4
#define q	td*8
#define pl td*16

static unsigned Zne1[]=
{
  #define np 0,ins,SoloSynt
  

  0,ins,Piano,
  g_,o,h,s,h,s,h,o,h,pl+o,
  h,o,a,s,a,s,g_,o,a,o,g_,s,g_,s,f,o,g_,o,f,o,
  e,pl,
  end,
};

static unsigned Zne2[]=
{
  np,
  H,o,H,s,H,s,H,o,H,q,H,o,H,o,H,o,
  c,o,c,s,c,s,c,o,c,q,c,o,d,o,c,o,
  H,pl,
  end,
};
static unsigned Zne4[]=
{


  np,
  0,rpt,2,
    E,o,E,s,E,s,E,o,E,q,E,o,E,o,E,o,
  0,erp,
  E,pl,
  end,
};
static unsigned Bub[]=
{
  0,ins,Piano,
  G_,o,G_,s,G_,s,G_,o,G_,q,G_,o,G_,o,G_,o,
  A,o,A,s,A,s,A,o,A,q,A,o,A,o,A,o,
  G_,pl,
  
  end,
};


void *Znelka[]={Zne1,Bub,Zne2,Zne4};

void HrajRondo( void )
{
  DInitPlay(Rondo);
}
void HrajPrelud( void )
{
  DInitPlay(Pl);
}
void HrajSmrt( void )
{
  DInitPlay(Sm);
}

void HrajZnelku( void )
{
  DInitPlay(Znelka);
}


void KonHraj( void )
{
  DEndPlay();
}

Flag Dohrano( void )
{
  return True;
}

static KanalD kanalyD[4];

/*
static short NextSample(Nastroj *nastr)
{
  long long pos64 = (((long long)nastr->RPos)<<32)|nastr->RPosF;
  long long nxt64 = pos64 + (1ULL<<32);
  if (nxt64>=0)
  {
    long long rep64 = (((long long)nastr->RRep)<<32)|nastr->RRepF;
    nxt64 -= rep64;
    if (nxt64>=0) return nastr->RNas[nastr->RPos]; // no loop, return the same sample
  }
  return nastr->RNas[(long)(nxt64>>32)];
}
*/

static int MixChannels()
{
  int ch;
  int vol = 0;
  for (ch=0; ch<4; ch++)
  {
    KanalD *nastr = &kanalyD[ch];
    Nastroj *ns = nastr->Ns;
    if (ns)
    {
      unsigned off = (unsigned)nastr->Off;
      int sampleP = off<ns->Kon ? ns->Nas[off] : 0;
      //short sampleN = NextSample(nastr);
      //float factorNP = nastr->RPosF*(1.0f/(1ULL<<32));
      int sample = sampleP;
      if (off<ns->Kon)
      {
        nastr->Off += nastr->Step;
      }

      vol += sample<<7;
    }
  }
  return vol;
}

static void **ToPlay;
static void **Playing;

int PlayMusicSample(int freq)
{
  float deltaT = 1.0f/freq;
  int Kn;
  KanalD *kan;
  if (ToPlay!=Playing)
  {
    Playing = ToPlay;
    // change music
    for( Kn=0; Kn<4; Kn++ )
    {
      kan=&kanalyD[Kn];
      kan->Mel0 = kan->Mel = ToPlay ? (unsigned *)ToPlay[Kn] : EMel;
      kan->BR = 0;
      kan->Ns = NULL;
      kan->RpF = 0;
      kan->Del = 0;
      kan->Off = 0;
      kan->Step = 0;
    }

  }
  for( Kn=0; Kn<4; Kn++ )
  {
    kan=&kanalyD[Kn];
    if (!kan->Mel) continue;
    if( kan->Del<=0 )
    {
      while(  *kan->Mel == 0 )
      {
        kan->Mel++;
        switch( *kan->Mel++ )
        {
          case endhlas:
            kan->EFg=-1;
            break;
          case ins:
          {
            Nastroj *N=NTab[*kan->Mel++];
            kan->Ns = N;
            break;	
          }
          case rphlas:
            if( !kan->RpF ) kan->Mel=kan->Mel0;
            break;
          case beg:
            kan->Mel0=kan->Mel;
            break;
          case rpt:
            kan->BR++;
            kan->Rep[kan->BR]=*kan->Mel++;
            kan->Buf[kan->BR]=kan->Mel;
            break;
          case erp:
            if( --kan->Rep[kan->BR] <= 0 )
            {
              if( --kan->BR<0 )
              {
                kan->BR=0;
                kan->Buf[kan->BR]=kan->Mel0;
              }
              break;
            }
            kan->Mel=kan->Buf[kan->BR];
            break;
        }
      } /* while */
      
      {
        int tone = *kan->Mel++;

        kan->Step = (float)BASE_FRQ/100/tone/freq*36.603f; // tuned experimentally based on comparison to Steem emulator output
        kan->Off=0;
        kan->Del=*kan->Mel++*0.0130f; // tempo control - experimentally based on comparison to Steem emulator output
      }
    } /* if del */
    kan->Del -= deltaT;
  }
  // mix channels
  return MixChannels();

}

void DInitPlay(void *mel[])
{
  // init all channels
  ToPlay = mel;
  // TODO: atomic or lock

}

void DEndPlay()
{
  ToPlay = NULL;
}


