typedef struct
{
	int FX[NFaz],FY[NFaz];
} SInfo;
typedef word (*Coors)[400][BPL/2];

int LoadPc1( byte *Buf, word *Scr, char *Name, int mode, word *UIP );
void InitSpr( Sprite *Ufon );
void SetSpr( Sprite *S, SInfo *SI, word *SAdr );
int Rnd( word Rng );

enum {Sort,User,Linear,Bright};
