typedef struct
{
	int FX,FY;
} SInfo;
typedef word (*Coors)[400][BPL/2];

int LoadPc(word *Scr, word *Pal, const char *name);
void InitSpr( Sprite *Ufon );
void SetSpr( Sprite *S, SInfo *SI, word *SAdr );
void SetMask( Sprite *S, SInfo *SI, word *SAdr );
word Rnd( word Rng );

extern Flag Mono;

#define ycoor(VRA,yy) ( (word *)& ((byte *)VRA) [((yy)>>1)*160] )

int MFopen( int FIn );
long MFread( int FIn, long Sz, void *Buf );
int MFclose( int FIn );
