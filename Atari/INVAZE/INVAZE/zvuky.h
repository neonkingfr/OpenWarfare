#include "macros.h"

#ifdef __cplusplus
extern "C"
{
#endif


struct wave_WAVEFORMAT // copied from Windows header files
{
  word        wFormatTag;         /* format type */
  word        nChannels;          /* number of channels (i.e. mono, stereo...) */
  lword       nSamplesPerSec;     /* sample rate */
  lword       nAvgBytesPerSec;    /* for buffer estimation */
  word        nBlockAlign;        /* block size of data */
  word        wBitsPerSample;     /* number of bits per sample of mono data */
};

struct wave_WAVEFORMATEX // copied from Windows header files
{
  word        wFormatTag;         /* format type */
  word        nChannels;          /* number of channels (i.e. mono, stereo...) */
  lword       nSamplesPerSec;     /* sample rate */
  lword       nAvgBytesPerSec;    /* for buffer estimation */
  word        nBlockAlign;        /* block size of data */
  word        wBitsPerSample;     /* number of bits per sample of mono data */
  word        cbSize;             /* the count in bytes of the size of */
};

struct SoundWave
{
  struct wave_WAVEFORMATEX format;
  void *data;
  size_t dataSize;
  long LoopBeg,LoopEnd;
};

typedef struct
{
  long long Pos;
  long long Krk;
  const struct SoundWave *snd;
  int busy;
  int loop;
} Kanal;

void InitPlay(void);
void InitP(struct SoundWave *M);
void LastLoopP();

void EndPlay( void );
int InitKan( struct SoundWave *Co );
int InitKanLooped( struct SoundWave *Co );
void EndKan( int K );

void LoadSound(struct SoundWave *snd, const char *filename);

Flag LoadSounds();


#define NKanaly 3

extern struct SoundWave Dup1;
extern struct SoundWave Dup2;
extern struct SoundWave DelVyst;
extern struct SoundWave UFOVyst;
extern struct SoundWave Vybuch;
extern struct SoundWave Pulec;
extern struct SoundWave BuchS;
extern struct SoundWave BuchUF;
extern struct SoundWave Ml, Bon, Let;

extern Kanal kanaly[NKanaly];

#ifdef __cplusplus
};
#endif
