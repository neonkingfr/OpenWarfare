#if !defined( __STDLIB )
#define __STDLIB


#define EXIT_FAILURE    !0
#define EXIT_SUCCESS    0
#define RAND_MAX        32767


typedef unsigned long   size_t;

int     atoi( const char *str );
long    atol( const char *str );

char    *itoa( int value, char *string, int radix );
char    *ltoa( long value, char *string, int radix );
char    *ultoa( unsigned long value, char *string, int radix );

void    *malloc( size_t size );
void    *calloc( size_t elt_count, size_t elt_size );
void    free( void *ptr );
void    *realloc( void *ptr, size_t size );

int     abs( int x );
long    labs( long x );

#ifndef __ACC
	void    exit( int status );
	void    abort( void );
	int     atexit( void (*func)( void ) );
#endif

#endif

