#ifdef _MSC_VER
#pragma once
#endif

#ifndef _FILE_SERVER_MT_HPP
#define _FILE_SERVER_MT_HPP

#include "fileServer.hpp"
#include <Es/Memory/fastAlloc.hpp>
#include <Es/Containers/staticArray.hpp>
#include <Es/Containers/boolArray.hpp>

#include <El/QStream/qbStream.hpp>

#include "El/FreeOnDemand/memFreeReq.hpp"

#include "Es/Threads/pocritical.hpp"

#include "Es/Threads/multisync.hpp"

#include <Es/Memory/normalNew.hpp>

#include "El/FileServer/multithread.hpp"

#if /*defined _XBOX ||*/ !defined _WIN32
// no memory store on Linux - no file mapping available
# define USE_MEM_STORE 0
#else
# define USE_MEM_STORE 1
#endif

#if USE_MEM_STORE
#include <El/MemoryStore/memStore.hpp>
  class QIStreamBufferMappedTgt;
  typedef QIStreamBufferMappedTgt TgtBuffer;
  typedef RefR<QIStreamBufferMappedTgt> RefTgtBuffer;
#else
  typedef QIStreamBufferPage TgtBuffer;
  typedef Ref<QIStreamBufferPage> RefTgtBuffer;
#endif

// In this implementation, FileServerHandle is interpreted as ReadHandleInfo *

class FileServerWorkerThread;
class FileRequest;

/// information about position where I/O completed
struct DiskPositionDesc
{
  /** handle may be invalid, but this does not matter - it may only cause
  seek detection to report false negative, but the handle is not really used
  */
  HANDLE _handle;
  QFileSize _offset;

  bool CheckSeek(HANDLE handle, QFileSize offset) const
  {
    return handle!=_handle || offset!=_offset;
  }
  
  bool operator == (const DiskPositionDesc &src) const
  {
    return src._handle==_handle && src._offset==_offset;
  }
  
  DiskPositionDesc():_handle(0),_offset(0)
  {
  }
};


/// overlapped operation interface
class IOverlapped: public FileOpRef
{
protected:
  FileServerWorkerThread *_owner;

public:
  IOverlapped() {_owner = NULL;}
  virtual ~IOverlapped(){}
  virtual HANDLE GetHandle() const = 0;
  virtual const RString &GetName() const = 0;
  FileServerWorkerThread *GetOwner() {return _owner;}
  virtual FileRequest *GetFileRequest() const {return NULL;}
  virtual int GetMemNeeded() const {return 0;}

  void SetOwner(FileServerWorkerThread *owner) {_owner = owner;}
  virtual bool IsDone() const = 0;
  virtual bool DependsOn(IOverlapped *req) const {return req == this;}
  /// return false when restart is needed (after some memory released)
  virtual bool Perform(DiskPositionDesc &pos) = 0;
  virtual void OnDone() {}
  // ask the op if it will cause the seek
  /** by default we suppose any op will cause a seek*/
  virtual bool DetectSeek(const DiskPositionDesc &pos) const {return true;}
  void Wait();
};

/// contains information needed to call the request done handler
struct RequestableObjectRequestInfo
{
  LLink<RequestableObject> _object;
  SRef<RequestableObject::RequestContext> _context;
};

TypeIsMovableZeroed(RequestableObjectRequestInfo)

//Forward declarations
class FileServerST;
class ReadHandleCache;
struct ReadHandleInfoNormal;
struct ReadHandleInfoDVD;
class OverlappedReadScatter;
class FileServerWorkerThread;

//! file request - waiting in queue to
class FileRequest: public FileReqRef
{
  friend class FileServerST;
  friend class ReadHandleCache;
  friend struct ReadHandleInfoNormal;
  friend struct ReadHandleInfoDVD;
  friend class OverlappedReadScatter;
  friend class FileServerWorkerThread; // debugging

  //! real source file name
  FileServerHandle _filePhysical;
  //@{ real source file region we are interested in
  QFileSize _fromPhysical;
  QFileSize _sizePhysical;
  //@}

  //! logical file name
  FileServerHandle _fileLogical;
  //@{ logical source file region we are interested in
  QFileSize _fromLogical;
  QFileSize _sizeLogical;
  //@}
  /// how many times _fromLogical,_sizeLogical region was locked
  int _lockCount;
  /// debugging - how many pages were locked
  /** used to verify lock/unlock pairing */
  int _lockPages;

  /// list of pending requests which we depend upon
  /**
  Requests may be removed from here once they complete.
  */
  RefArray<FileRequest> _depends;
  
  //@ which part of the logical file was needed by the caller
  QFileSize _neededBeg;
  QFileSize _neededEnd;
  //@}
  
  //! request priority
  FileRequestPriority _priority;
  /// time when the request was submitted
  DWORD _timeStarted;

  /// handlers to be performed when the request is done
  AutoArray<RequestableObjectRequestInfo> _handlers;
  
  //! which server is this request from
  /*!
  This is usually GFileServer, but it might be unsafe to assume it.
  */
  FileServerST *_server;
  //! result will be stored here
  AutoArray<RefTgtBuffer> _target;
  /// how many target buffers are still not done
  int _targetsNotDone;

  //! which pages store to cache
  PackedBoolAutoArray _store;

  //! set true when request has not started yet and is waiting in the queue
  bool _waiting;

  //! _target is filled with correct (decompressed) data
  bool _targetValid;

  //! processed overlapped operation (where Ref to request is stored)
  InitPtr<IOverlapped> _operation;

  public:
  FileRequest(){_targetValid = false;}
  FileRequest
  (
    FileServerHandle fileLogical, FileServerHandle filePhysical,
    FileRequestPriority priority,
    QFileSize fromLogical=0, QFileSize fromPhysical=0,
    QFileSize sizeLogical=QFileSizeMax, QFileSize sizePhysical=QFileSizeMax
  );
  ~FileRequest();

  void CloseHandles();
  
  bool IsMoreUrgent(const FileRequest &req) const;

  void OnDone(QFileSize read, HRESULT result, OverlappedReadScatter *op);

  void Decode();

  bool IsTargetValid() const {return _targetValid;}
  void SetTargetValid() {_targetValid = true;}
  
  private:
  bool operator == (const FileRequest &with) const;
  bool Contains(const FileRequest &what) const;
  bool Contains(FileServerHandle file, QFileSize from, QFileSize size) const;
  bool Overlaps(FileServerHandle file, QFileSize from, QFileSize size) const;

  void AddHandler(
    RequestableObject *object,
    RequestableObject::RequestContext *context
  );
  /// notification called when any of the buffers belonging to the request is done
  void OnBufferCompleted(bool executeHandlers, bool success);
  
  /// called once when all buffers are known to be loaded
  void ExecuteHandlers(bool executeHandlers, bool success);
  
  //! check if given request has finished
  bool IsDone();
  
  RString GetDebugName() const;
  
  void WaitUntilDone();

#if DEBUG_PRELOADING
  bool DebugMe() const
  {
    for (int i=0; i<_handlers.Size(); i++)
    {
      if (_handlers[i]._object && _handlers[i]._object->DebugMe()) return true;
    }
    return false;
  }
#endif
  
  USE_FAST_ALLOCATOR
};

// opening file can be very slow on XBox
// it is therefore a good idea to keep cache of recently opened files in case they are used again
struct ReadHandleInfo: public TLinkBidirRef, public RequestableObject
{
  RStringI _name;
  QFileSize _size;
  /// how many times this item was opened
  int _open;
  /// do not discard file when server is stopped (Alt-Tab)
  bool _permanent;
  
  ReadHandleInfo();
  ~ReadHandleInfo();

  virtual void *GetWinHandle() const = 0;
  virtual void GetFileTime(FILETIME *time) const = 0;
  virtual bool Check() const = 0;
  virtual bool Read(FileRequest *req, bool urgent) = 0;
  virtual void CheckRequest(FileRequest *req) = 0;
  virtual void UpdateRequest(FileRequest *req) = 0;
  virtual bool IsCached(DWORD start, DWORD size) = 0;
  virtual Ref<IOverlapped> FindHandle(FileServerST *server, QFileSize from, QFileSize size) = 0;

  USE_FAST_ALLOCATOR
};

struct ReadHandleInfoNormal : ReadHandleInfo
{
  Ref<IOverlapped> _handle;

  ReadHandleInfoNormal(IOverlapped *handle) {_handle = handle;}
  ~ReadHandleInfoNormal();

  virtual void *GetWinHandle() const;
  virtual void GetFileTime(FILETIME *time) const;
  virtual bool Check() const;
  virtual bool Read(FileRequest *req, bool urgent);
  virtual void CheckRequest(FileRequest *req);
  virtual void UpdateRequest(FileRequest *req);
  virtual bool IsCached(DWORD start, DWORD size) {return true;}
  virtual Ref<IOverlapped> FindHandle(FileServerST *server, QFileSize from, QFileSize size) {return _handle;}

  USE_FAST_ALLOCATOR
};

#ifdef _XBOX

struct ReadHandleInfoDVD : ReadHandleInfo
{
  Ref<IOverlapped> _handleDVD;
  Ref<IOverlapped> _handleCache;
  Ref<IOverlapped> _handleFlags;

  PackedBoolAutoArray _pages;
  static const int _dvdPage;

  class DVDCopyRequestContext: public RequestContext
  {
  public:
    Ref<FileRequest> _req;
    DVDCopyRequestContext(FileRequest *req) {_req = req;}
  };

  ReadHandleInfoDVD();
  ~ReadHandleInfoDVD();

  virtual void *GetWinHandle() const;
  virtual void GetFileTime(FILETIME *time) const;
  virtual bool Check() const;
  virtual bool Read(FileRequest *req, bool urgent);
  virtual void CheckRequest(FileRequest *req);
  virtual void UpdateRequest(FileRequest *req);
  virtual void RequestDone(RequestContext *context, RequestResult reason);
  virtual bool ProcessImmediately() const {return true;}
  virtual bool IsCached(DWORD start, DWORD size);
  virtual Ref<IOverlapped> FindHandle(FileServerST *server, QFileSize from, QFileSize size);

  // implementation
  void InitCacheFlags();
  bool ReadCacheFlags();
  bool WriteCacheFlags(FileServerST *server);

  USE_FAST_ALLOCATOR
};

#endif

//! Merge several following requests together

#if _ENABLE_REPORT && _ENABLE_PERFLOG
#include <El/Common/perfLog.hpp>

class LogFileServerActivity
{
  bool _closed;
  //@{ information about the last file requested
  RString _file;
  int _start;
  int _size;
  //@}
  LogFile _log;

public:
  LogFileServerActivity();
  ~LogFileServerActivity();
  void Flush();
  void DoLog(RString file, int start, int size);
  void Open(const char *name);
  void Close();
};
#endif

#ifdef _XBOX
struct DVDCacheList
{
  int _preloading;
  AutoArray<RStringI> _list;
  MapStringToClass<RStringI, AutoArray<RStringI> > _table;

  DVDCacheList() {_preloading = 0;}
  void Add(RStringI name) {_list.Add(name); _table.Add(name);}
  bool Find(RStringI name) const {return _table.NotNull(_table[name]);}
};
#endif

class ReadHandleCache
{
  friend class FileServerST;
  
  int _count;
  int _countCached;
  TListBidirRef<ReadHandleInfo> _cache;

#ifdef _XBOX
  DVDCacheList _dvdCache;
#endif

  public:
  ReadHandleCache();

  FileServerHandle Open(FileServerST *server, const char *name, bool onlyIfOpen=false);
  bool Close(const char *name, FileServerHandle handle);

  //! increase opened count of given handle
  void Open(FileServerHandle handle);
  //! decrease opened count of given handle
  bool Close(FileServerHandle handle);
  //! read from opened handle
  bool Read(FileRequest *req, bool urgent);

  RString GetName(FileServerHandle handle) const;
  QFileSize GetSize(FileServerHandle handle) const;
  void CheckRequest(FileRequest *req);
  void UpdateRequest(FileRequest *req);

  bool Flush(const char *name);
  void FlushAll();
  bool CheckIntegrity() const;
  void Clear();
  void Maintain();

#ifdef _XBOX
  void InitDVDCache();
  void AddPreloadRequests(FileServerST *server, RString name);
#endif

protected:
#ifdef _XBOX
  // opening file on the scratch volume
  ReadHandleInfo *OpenCached(FileServerST *server, const char *name, DWORD attr);
#endif
};

template <>
struct HeapTraits< Ref<FileRequest> >
{
  typedef Ref<FileRequest> Type;
  // default traits: Type is pointer to actual value
  static bool IsLess(const Type &a, const Type &b)
  {
    return a->IsMoreUrgent(*b);
  }
};

struct RequestableObjectRequestResultInfo: public RequestableObjectRequestInfo
{
  RequestableObject::RequestResult _result;
};
TypeIsMovableZeroed(RequestableObjectRequestResultInfo)

#ifdef _XBOX

struct WriteRequest : public RefCount
{
  // data to write
  LLink<ReadHandleInfoDVD> _info;
  int _start;
  QFileSize _size;
  AutoArray<RefTgtBuffer> _source;

  WriteRequest(ReadHandleInfoDVD *info, int start, QFileSize size, AutoArray<RefTgtBuffer> &source)
  {
    _info = info;
    _start = start;
    _size = size;
    _source = source;
  }
  bool IsMoreUrgent(const WriteRequest &req) const;

  USE_FAST_ALLOCATOR
};

template <>
struct HeapTraits< Ref<WriteRequest> >
{
  typedef Ref<WriteRequest> Type;

  // default traits: Type is pointer to actual value
  static bool IsLess(const Type &a, const Type &b)
  {
    return a->IsMoreUrgent(*b);
  }
};

struct WriteRequests : MemoryFreeOnDemandHelper
{
  FileServerST *_server;

  HeapArray< Ref<WriteRequest> > _dataRequests;
  LLinkArray<ReadHandleInfoDVD> _flagsRequests;

  int _pendingDataRequests;
  int _pendingFlagsRequests;
  QFileSize _pendingDataSize;

  WriteRequests()
  {
    _server = NULL;
    _pendingDataRequests = 0;
    _pendingFlagsRequests = 0;
    _pendingDataSize = 0;
    RegisterFreeOnDemandMemory(this);
  }

  // implementation of MemoryFreeOnDemandHelper abstract interface
  virtual size_t FreeOneItem();
  virtual float Priority() const;
  virtual size_t MemoryControlled() const;
  virtual RString GetDebugName() const;
};

#endif

#ifdef _XBOX
class OverlappedWriteData;
class OverlappedWriteFlags;
#endif

//! file server worker thread
class FileServerWorkerThread
{
  friend class FileServerST;
  bool _cleanedUp;

  class ThrRunnable;
  friend class ThrRunnable;
  class ThrRunnable: public MultiThread::ThreadBase
  {
    FileServerWorkerThread *_outer;
  public:
    ThrRunnable(FileServerWorkerThread *outer) 
      : MultiThread::ThreadBase(MultiThread::ThreadBase::PriorityAboveNormal, 16*1024), 
        _outer(outer) 
    {}
    unsigned long Run()
    {
      _outer->DoWork();
      return 0;
    }
  };

protected:
  ThrRunnable _thread;
  /// item can never be desrtroyed unless already processed
  RefArray<IOverlapped> _queue;
  /// items which are already processed
  /**
  worker thread cannot manipulate the queue - it only increases processed count
  */
  int _queueProcessed;
  /// items which already started processing or are processed
  int _queueInProcess;
  PoCriticalSection _queueLock;
  /// count submitted request
  MultiThread::SemaphoreBlocker _submitSemaphore;
  MultiThread::EventBlocker _terminateEvent;
  MultiThread::EventBlocker _errorEvent;
  MultiThread::EventBlocker _errorEventHandled;
#if !POSIX_FILES_COMMON
  HANDLE _overlapEvent;
#endif

  /// position on the disc where last I/O completed
  DiskPositionDesc _lastPos;
  
  FileServerST *_server;

  //@{ _queueLock access
  void EnterLock(){_queueLock.Lock();}
  void LeaveLock(){_queueLock.Unlock();}
  //@}
public:
  FileServerWorkerThread(FileServerST *server);
  ~FileServerWorkerThread();
  void Prepare();
  void CleanUp();

  int Size() const {return _queue.Size();}

  QFileSize GetReadScatterPageSize() const;
#ifdef _XBOX
  void OnWriteDataDone(OverlappedWriteData *req);
  void OnWriteFlagsDone(OverlappedWriteFlags *req);
#endif

#if !POSIX_FILES_COMMON
  HANDLE GetOverlapEvent() const {return _overlapEvent;}
#endif

  bool CheckIntegrity() const;
  void DoWork();
  static DWORD WINAPI DoWorkCallback(void *context);

  /// submit to the queue edn
  IOverlapped *Submit(IOverlapped *req, bool urgent=false);
  /// make sure it is executed as soon as possible
  void MakeRequestUrgent(FileRequest *req);
  /// make sure it is executed as soon as possible
  void MakeItemUrgent(int index);
  
  void Wait(IOverlapped *op);
  bool IsDone(RString name) const;
  bool IsWaiting(IOverlapped *op) const;
  void Wait(RString name);

  void Maintain();
  void Update();
  
  private:
  //@{ helpers for MakeRequestUrgent
  int FindForward(IOverlapped *item, int from, int to);
  void Forward(IOverlapped *item, int moveTo, int moveFrom);
  //@}
};


/// class which can be used to construct QIStreamBuffer
class QIStreamBufferSource: private NoCopy
{
  #if USE_MEM_STORE
  // store based implementation - memory is allocated only as needed
  /// index of the memory store location
  int _memIndex;
  
  public:
  QIStreamBufferSource():_memIndex(-1){}
  ~QIStreamBufferSource(){Free();}
  
  explicit QIStreamBufferSource(int memIndex):_memIndex(memIndex){}
  
  QIStreamBufferSource(const QIStreamBufferMappedTgt *tgt);
  void operator = (const QIStreamBufferMappedTgt *tgt);
  
  void Free();
  Ref<QIStreamBuffer> GetBuffer() const;
  
  size_t GetSize() const {return _memIndex>=0 ? GetPageRecommendedSize() : 0;}
  bool IsNull() const {return _memIndex<0;}
  
  #else

  // simple implementation - memory held directly
  Ref<QIStreamBuffer> _ref;
  
  public:
  QIStreamBufferSource(){}
  QIStreamBufferSource(QIStreamBuffer *buf):_ref(buf){}
  void operator = (QIStreamBuffer *buf){_ref = buf;}
  
  Ref<QIStreamBuffer> GetBuffer() const {return _ref.GetRef();}
  
  size_t GetSize() const {return _ref ? _ref->GetSize() : 0;}
  bool IsNull() const {return _ref.IsNull();}
  
  #endif

  private:
  QIStreamBufferSource & operator =(const QIStreamBufferSource &);
};

#if USE_MEM_STORE

/// buffer mapped into the memory store
class QIStreamBufferMapped: public QIStreamBuffer
{
  /// index of the page in the store
  int _index;
  
  public:
  QIStreamBufferMapped(int page);
  ~QIStreamBufferMapped();

  USE_FAST_ALLOCATOR
};

extern MemoryStore GMemStore;

/// buffer mapped into the memory store, used a target for file reads
class QIStreamBufferMappedTgt: public RefCount, private NoCopy
{
  friend class QIStreamBufferSource;
  friend class QIStreamBufferMappedTgtLocked;
  
  protected:
  int _index; /// index in the memory store
  char *_buffer; //! data buffer

  public:
  QIStreamBufferMappedTgt()
  {
    _index = GMemStore.Alloc();
    _buffer = NULL;
  }

  ~QIStreamBufferMappedTgt()
  {
    if (_index>=0)
    {
      GMemStore.Free(_index);
    }
  }

  void *DataLock(QFileSize pos, QFileSize size)
  {
    // only one lock per buffer possible
    Assert(!_buffer);
    _buffer = (char *)GMemStore.MapView(_index,true);
    // only whole buffer locked possible
    Assert(pos==0);
    Assert(GMemStore.GetPageSize()>=size);
    //_bufLen = size;
    return _buffer+pos;
  }
  void DataUnlock(QFileSize pos, QFileSize size)
  {
    Assert(_buffer);
    Assert(pos==0);
    Assert(GMemStore.GetPageSize()>=size);
    GMemStore.UnmapView(_index,_buffer);
    _buffer = 0;
  }
  
  Ref<QIStreamBuffer> GetBuffer() const;
  
  bool IsLocked() const {return _buffer!=NULL;}
  
  __forceinline QFileSize GetSize() const {return _index>=0 ? GMemStore.GetPageSize() : 0;}

  USE_FAST_ALLOCATOR
};

inline QIStreamBufferSource::QIStreamBufferSource(const QIStreamBufferMappedTgt *tgt)
{
  // multiple buffers owning the same allocation at the same time
  _memIndex = tgt->_index;
  GMemStore.ShareAlloc(tgt->_index);
}

inline void QIStreamBufferSource::operator = (const QIStreamBufferMappedTgt *tgt)
{
  Assert(_memIndex<0);
  // multiple buffers owning the same allocation at the same time
  _memIndex = tgt->_index;
  GMemStore.ShareAlloc(tgt->_index);
}

#endif

// for ArmA 1 we do not need any thread safety, but we need it for ArmA 2
#define FILE_SERVER_MT_SAFE 1

//! file server
class FileServerST: public FileServer, public QIFileServerFunctions, public MemoryFreeOnDemandHelper
{
  friend class FileRequest;
  friend class FileRequestLock;
  friend struct ReadHandleInfoDVD;
  
  //! convert name to handle
  ReadHandleCache _handles;
  
  #if FILE_SERVER_MT_SAFE
  mutable CriticalSection _lock;
  
  /// lock access to file cache (used in external functions)
  #define FS_SCOPE_LOCK_EXT(server) ScopeLock<FileServerST> lock(server)
  /// lock access to file cache (used in members)
  #define FS_SCOPE_LOCK() FS_SCOPE_LOCK_EXT(*this)
  
  public:
  void Lock() const
  {
    // CS hierarchy: always lock memory first
    GMemFunctions->Lock();
    _lock.Lock();
  }
  void Unlock() const
  {
    _lock.Unlock();
    GMemFunctions->Unlock();
  }
  
  private:
  #else
  #define FS_SCOPE_LOCK() 
  #define FS_SCOPE_LOCK_EXT(server) 
  
  #endif

  //@{ request throttling and statistics
  //! number of requests that were already passed to the OS
  int _nRequestsLoading;
  //! total size of requests that were already passed to the OS
  int _sizeRequestsLoading;
  //@}

  /// worker thread
  FileServerWorkerThread _workerThread;

  /// true when FileServer was manually destructed
  bool _serverFinalized;
  
  /// handlers for request that are already finished
  AutoArray<RequestableObjectRequestResultInfo> _handlers;

#ifdef _XBOX
  // write requests processing
  WriteRequests _writeRequests;
#endif

  //! make sure server is started before using it and it is started only once
  bool _initDone;

  #if _ENABLE_REPORT && _ENABLE_PERFLOG
    //! log any file operations - good for preloading
    LogFileServerActivity _logFiles;
  #endif
  
  //! cached item
public:
  class FileInCache: public RefCount, public LinkBidirWithFrameStore
  {
    public:
    FileServerHandle _handle; //!< OS handle used to access the file
    //RString _fileName; //!< when we cannot keep handle, keep name
    QFileSize _start; //!< region start
    QFileSize _size; //! region size
    /// associated data
    QIStreamBufferSource _buffer;
    /// if the file is not loaded yet, store only the associated request here
    /** note: there may be several request associated with the same cache item*/
    RefArray<FileRequest> _request;
    //@{ item cannot be released while there is an active request to it
    int _locked;

    void Lock(){Assert(_locked>=0);_locked++;}
    void Unlock(){_locked--;Assert(_locked>=0);}
    //@}
    
    FileInCache():_locked(0){} // empty name for and empty entry
    ~FileInCache(){Assert(_request.Size()==0);} // no name for destructed entry

    /// access the buffer
    PosQIStreamBuffer GetBufferRef() const;
    
    size_t GetMemoryControlled() const;

    USE_FAST_ALLOCATOR
  };

  struct RefFileInCache : public Ref<FileInCache>
  {
    RefFileInCache() {}
    RefFileInCache(FileInCache *source) : Ref<FileInCache>(source) {}
    const FileInCache *GetKey() const {return GetRef();}
  };

  struct MapFileInCacheTraits: public DefMapClassTraits<RefFileInCache>
  {
    typedef const FileInCache *KeyType;

    //! calculate hash value
    static unsigned int CalculateHashValue(KeyType key)
    {
      return key->_start + (unsigned int)key->_handle;
    }

    //! compare keys, return negative when k1<k2, positive when k1>k2, zero when equal
    static int CmpKey(KeyType k1, KeyType k2)
    {
      int diff = k1->_start - k2->_start;
      if (diff != 0) return diff;
      diff = k1->_handle - k2->_handle;
      if (diff != 0) return diff;
      diff = k1->_size - k2->_size;
      return diff;
    }
    
    static KeyType GetKey(const RefFileInCache &item) {return item;}
  };

  struct FileCache
  {
    FramedMemoryControlledList<
      FileInCache,
      FramedMemoryControlledListTraitsRef<FileInCache>
    > _list;
    typedef MapStringToClass<
      RefFileInCache,
      AutoArray<RefFileInCache>,
      MapFileInCacheTraits
    > CacheTableType;
    CacheTableType _table;
  };
private:

  //! page size required for ReadScatter operation
  int _readScatterPageSize;
  //! max. allowed memory usage
  QFileSize _maxSize;

  //! all data that is read and cached
  FileCache _cache;

  //! heap of waiting requests
  HeapArray< Ref<FileRequest> > _queue;
  //! total size of all requests waiting in the queue
  int _queueByteSize;

  protected:
  FileInCache *Find(FileServerHandle handle, QFileSize start, QFileSize size);
  PosQIStreamBuffer New(FileServerHandle handle, QFileSize start, QFileSize size);
  /// store a buffer into the file cache
  bool Store(
    TgtBuffer *buf, FileServerHandle handle, QFileSize start, QFileSize size,
    bool executeHandlers, bool success, bool discardSoon, FileRequest *completedBy
  );
  /// store an empty slot into the file cache
  FileInCache *StoreEmpty(
    FileServerHandle handle, QFileSize start, QFileSize size, bool discardSoon
  );
  
  void MoveToFront(FileInCache *item);
  void DeleteItem(FileInCache *item);
  void Maintain();

  /// finish requested operation - called when transfer is completed
  void RequestDone(FileRequest *handle, int rd, bool executeHandlers=false);
  
  /// request must be repeated due to memory failure or similiar condition
  void RequestRepeat(FileRequest *req);

  void UpdateRequestStored(FileRequest *req, QFileSize start, QFileSize size);

  public:
  FileServerST();
  ~FileServerST();

  void SetMaxSize(int maxCacheSize);
  void Clear();
  void Finalize();

  /// submit request for operation
  IOverlapped *Submit(IOverlapped *req) {return _workerThread.Submit(req);}

  /// submit request for operation - urgent (place before all other requests)
  /** this call is typically used for blocking request. */
  IOverlapped *SubmitUrgent(IOverlapped *req) {return _workerThread.Submit(req,true);}

  //! preload using given logfile
  void Preload(RString logFile);
  
  /// preload - inform object when done
	bool Preload(
	  RequestableObject *obj, RequestableObject::RequestContext *ctx,
	  FileRequestPriority priority, FileServerHandle handle, QFileSize start, QFileSize size
	);

  //! preload into the cache, request handle is owned by the server
  bool Preload(
    FileRequestPriority priority, const char *name, QFileSize start=0, QFileSize size=QFileSizeMax
  );
  //! preload into the cache, request handle is owned by the server
  bool Preload(
    FileRequestPriority priority, FileServerHandle handle, QFileSize start=0, QFileSize size=QFileSizeMax
  );

  //! request, store result in cache, caller is sure no part is currently cached
  FileRequestHandle RequestUncached(
    RequestableObject *obj, RequestableObject::RequestContext *ctx,
    FileRequestPriority priority, FileServerHandle handle, QFileSize start, QFileSize size
  );
  //! request, store result in cache
  FileRequestHandle Request(
    FileRequestPriority priority, const char *name, QFileSize start=0, QFileSize size=QFileSizeMax
  );
  //! request, store result in cache
  FileRequestHandle Request(
    RequestableObject *obj, RequestableObject::RequestContext *ctx,
    FileRequestPriority priority, FileServerHandle handle, QFileSize start=0, QFileSize size=QFileSizeMax
  );
  //! check if given request is done
  void WaitUntilDone(FileRequestHandle handle);
  //! check if given request is done
  bool RequestIsDone(FileRequestHandle handle);
  //! cancel request
  void CancelRequest(FileRequestHandle handle);
  /// add a handler to be called once the request is finished
  void AddHandler(
    FileRequestHandle handle,
    RequestableObject *object,
    RequestableObject::RequestContext *context
  );

  Ref<FileRequest> AddRequest
  (
    FileServerHandle fileLogical, FileServerHandle filePhysical,
    FileRequestPriority priority,
    QFileSize fromLogical, QFileSize fromPhysical,
    QFileSize sizeLogical, QFileSize sizePhysical,
    RequestableObject *obj = NULL, RequestableObject::RequestContext *ctx = NULL,
    const PackedBoolAutoArray &store = PackedBoolAutoArray()
  );
  void LockRequest(FileRequest *req);
  /// used in pair with AddRequest to clean-up
  void UnlockRequest(FileRequest *req);
  void UnlockRequest(FileRequestHandle req)
  {
    UnlockRequest(static_cast<FileRequest *>(req.GetRef()));
  }

  virtual bool CheckIntegrity() const {return _workerThread.CheckIntegrity();}

#ifdef _XBOX
  void AddWriteRequest(ReadHandleInfoDVD *info, int start, QFileSize size, AutoArray<RefTgtBuffer> &source);
  bool AddWriteFlagsRequest(ReadHandleInfoDVD *handle);

  bool AddCopyRequests();

  void OnWriteDataDone(OverlappedWriteData *req);
  void OnWriteFlagsDone(OverlappedWriteFlags *req);

  bool CopyFromDVD(int &requests);
#endif
  
  //! open file - this can be used as a hint to pre-read it
  void Open(QIFStream &in, const char *name, QFileSize from=0, QFileSize to=QFileSizeMax);

  // say we need the file NOW - load it
  void FlushBank(QFBank *bank);
  void OpenPreloadLog(const char *name); //!< start preload monitoring
  void SavePreloadLog(); //!< called when preload monitoring is finished
  void OpenDVDLog(const char *name); //!< start preload monitoring
  void SaveDVDLog(); //!< called when preload monitoring is finished
  void PreloadDVD(RString name);
  void WaitForRequestsDone();
  void Init();
  void Start();
  void Stop();

  //! make sure pending part is not staying idle if there are any queued requests
  void SubmitQueuedRequests(
    FileRequestPriority maxPriority = FileRequestPriority(INT_MAX),
    int maxCount=INT_MAX, bool submitAll=false
  );
  //! handle any pending request competition
  void CheckPendingRequests();

  //! submit a particular request for processing
  void SubmitRequest(FileRequest *req, bool urgent=false);
  /// make sure given request is processes as soon as possible
  void MakeRequestUrgent(FileRequest *req);
  //! process request queue
  void ProcessRequests(int maxCount=INT_MAX, bool submitAll=false);
#ifdef _XBOX
  //! process write request queue
  void ProcessWriteRequests(bool blocking);
#endif

  /// remove any priorities to make sure the seeks are minimized
  void OptimizeQueueForSeek();
  
  /// submit all requests - used for batch preloading
  void SubmitRequests();
  //! main body of file-server - perform regular maintenance
  void Update();
  //! process handlers which are ready
  void ProcessHandlers();
  //! update worker thread
  void UpdateWorkerThread() {_workerThread.Update();}
	void LogWaitingRequests(FileRequestHandle req);

  virtual void WaitForOneRequestDone();
  virtual float PreloadQueueStatus() const;
  virtual int PreloadQueueRequests(int *size) const;
  
  // implementation of MemoryFreeOnDemandHelper abstract interface
  virtual size_t FreeOneItem();
  virtual float Priority() const;
  virtual size_t MemoryControlled() const;
  virtual size_t MemoryControlledRecent() const;
	virtual void MemoryControlledFrame();
  virtual RString GetDebugName() const;
  
  //@{ FileServer interface
  
  virtual int GetPageSize() {return GetPageRecommendedSize();}
  //@}
  QFileSize GetReadScatterPageSize() const {return _readScatterPageSize;}

  //@{ QIFileServerFunctions interface
  virtual bool FlushReadHandle(const char *name);
  virtual PosQIStreamBuffer Read(FileServerHandle handle, QFileSize start, QFileSize size);
  virtual FileServerHandle OpenReadHandle(const char *name);
  virtual FileServerHandle OpenWriteHandle(const char *name);
  virtual void MakeReadHandlePermanent(FileServerHandle handle);
  virtual void CloseReadHandle(const char *name, FileServerHandle handle);
  virtual bool CheckReadHandle(FileServerHandle handle);
  virtual RString GetHandleName(FileServerHandle handle) const;
  virtual void *GetWinHandle(FileServerHandle handle) const;
  virtual QFileSize GetFileSize(FileServerHandle handle) const;
  virtual QFileTime TimeStamp(FileServerHandle handle) const;
  //@}
  
  #if _ENABLE_REPORT
  /// check if we are interested in debugging given handle
  bool InterestedIn(FileServerHandle handle) const;
  /// check if we are interested in debugging given region
  bool InterestedIn(FileServerHandle handle, QFileSize start, QFileSize size) const;
  /// check if we are interested in debugging given file
  bool InterestedIn(const char *name) const;
  #else
  bool InterestedIn(FileServerHandle handle) const {return false;}
  /// check if we are interested in debugging given region
  bool InterestedIn(FileServerHandle handle, QFileSize start, QFileSize size) const {return false;}
  /// check if we are interested in debugging given file
  bool InterestedIn(const char *name) const {return false;}
  #endif

  virtual FileOpHandle RequestFileTime(RString name);
  virtual bool IsCompleted(FileOpHandle req) const;
#ifdef _WIN32
  virtual bool IsValidFileTime(FileOpHandle req) const;
  virtual RString FormatFileTime(FileOpHandle req, RString format) const;
#endif

#ifdef _WIN32
  virtual FileServerHandle CreateFileLongPath(
    const char *lpFileName,
    DWORD dwDesiredAccess,
    DWORD dwShareMode,
    _SECURITY_ATTRIBUTES *lpSecurityAttributes,
    DWORD dwCreationDisposition,
    DWORD dwFlagsAndAttributes,
    FileServerHandle hTemplateFile
  );
#endif
#if POSIX_FILES_COMMON
  virtual FileServerHandle CreateFileLongPath(const char *lpFileName, int oflag, int pmode);
#endif
};

inline void IOverlapped::Wait() {if (_owner) _owner->Wait(this);}
inline QFileSize FileServerWorkerThread::GetReadScatterPageSize() const {return _server->GetReadScatterPageSize();}

#include <Es/Memory/debugNew.hpp>

#endif
