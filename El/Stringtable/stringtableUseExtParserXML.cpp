#include <El/elementpch.hpp>
#include "iExtParser.hpp"

#include <El/QStream/qbStream.hpp>
#include <El/XML/xml.hpp>
#include "stringtableImpl.hpp"

/// Implementation of extended parser enabling stringtable stored in the XML file
class StringtableXMLParserFunctions : public StringtableExtParserFunctions
{
public:
  virtual bool Open(const char *filename, QIFStreamB &in);
  virtual bool Open(QFBank &bank, const char *filename, QIFStreamB &in);
  virtual bool Parse(QIStream &in, StringTableDynamic &stringtable, RString language);
};

bool StringtableXMLParserFunctions::Open(const char *filename, QIFStreamB &in)
{
  const char *ext = strrchr(filename, '.');
  if (ext && stricmp(ext, ".csv") == 0)
  {
    RString filenameXML = RString(filename, ext - filename) + RString(".xml");
    if (QFBankQueryFunctions::FileExists(filenameXML))
    {
      in.AutoOpen(filenameXML);
      return true;
    }
  }
  return false;
}

bool StringtableXMLParserFunctions::Open(QFBank &bank, const char *filename, QIFStreamB &in)
{
  const char *ext = strrchr(filename, '.');
  if (ext && stricmp(ext, ".csv") == 0)
  {
    RString filenameXML = RString(filename, ext - filename) + RString(".xml");
    if (bank.FileExists(filename))
    {
      in.open(bank, filenameXML);
      return true;
    }
  }
  return false;
}

/// what column was assigned to the selected language
enum LanguageMatch
{
  LMNone,
  LMFirst,
  LMEnglish,
  LMOriginal,
  LMCorrect
};

/// XML parser used to process stringtable
class StringtableParser : public SAXParser
{
protected:
  StringTableDynamic &_stringtable;
  /// Language we want to load
  RString _currentLanguage;
  /// Currently parsing string id tag (<Text> or <Key>)
  RString _idTag;
  /// Currently parsing string id (<Text ID="..."> or <Key ID="...">)
  RString _id;
  /// Currently parsing language (<English>, ...)
  RString _language;
  /// Best text found for the current id (corresponding to _lineBest)
  RString _text;
  /// The last text inside the language tag
  RString _lastText;

  /// The best language matching for the current id
  LanguageMatch _lineBest;
  /// The worst language matching for the whole table
  LanguageMatch _tableWorst;

public:
  StringtableParser(StringTableDynamic &stringtable, RString language);
  LanguageMatch GetResult() const {return _tableWorst;}
  void OnStartElement(RString name, XMLAttributes &attributes);
  void OnEndElement(RString name);
  void OnCharacters(RString chars);

protected:
  /// Check if _lastText can be used as _text
  void HandleLastText();
};

StringtableParser::StringtableParser(StringTableDynamic &stringtable, RString language)
: _stringtable(stringtable), _currentLanguage(language)
{
  _lineBest = LMNone;
  _tableWorst = LMCorrect;
}

void StringtableParser::OnStartElement(RString name, XMLAttributes &attributes)
{
  if (_id.GetLength() > 0)
  {
    // inside <Text> ... <Text/>
    if (_language.GetLength() > 0)
    {
      RptF("Unexpected stringtable format inside <Text ID=\"%s\"><%s>", cc_cast(_id), cc_cast(_language));
    }
    _language = name;
    _lastText = RString();
  }
  else if (strcmp(name, "Text") == 0 || strcmp(name, "Key") == 0)
  {
    _idTag = name;
    const XMLAttribute *attr = attributes.Find("ID");
    if (attr)
    {
      _id = attr->value;
      _language = RString();
      _text = RString();
      _lineBest = LMNone;
    }
  }
}

void StringtableParser::OnEndElement(RString name)
{
  if (strcmp(name, _idTag) == 0)
  {
    if (_language.GetLength() > 0)
    {
      RptF("Unexpected stringtable format inside <Text ID=\"%s\"><%s>", cc_cast(_id), cc_cast(_language));
      _language = RString();
    }
    if (_id.GetLength() > 0)
    {
      if (_lineBest < _tableWorst)
      {
        _tableWorst = _lineBest;
      }
      _stringtable.Add(_id, _text);
      _id = RString();
    }
    _idTag = RString();
  }
  else if (_language.GetLength() > 0)
  {
    HandleLastText();
    _language = RString();
  }
}

void StringtableParser::OnCharacters(RString chars)
{
  if (_language.GetLength() > 0) _lastText = chars;
}

void StringtableParser::HandleLastText()
{
  // check the languages match by priority
  if (stricmp(_language, _currentLanguage) == 0)
  {
    _text = _lastText;
    _lineBest = LMCorrect;
    return;
  }
  if (_lineBest == LMCorrect) return;

  if (stricmp(_language, "Original") == 0)
  {
    _text = _lastText;
    _lineBest = LMOriginal;
    return;
  }
  if (_lineBest == LMOriginal) return;

  if (stricmp(_language, "English") == 0)
  {
    _text = _lastText;
    _lineBest = LMEnglish;
    return;
  }
  if (_lineBest == LMEnglish) return;

  if (_lineBest == LMNone)
  {
    _text = _lastText;
    _lineBest = LMFirst;
  }
}

bool StringtableXMLParserFunctions::Parse(QIStream &in, StringTableDynamic &stringtable, RString language)
{
  StringtableParser parser(stringtable, language);
  parser.Parse(in);
  return parser.GetResult() == LMCorrect;
}

static StringtableXMLParserFunctions GStringtableXMLParserFunctions;
extern StringtableExtParserFunctions *GStringtableExtParserFunctions = &GStringtableXMLParserFunctions;
