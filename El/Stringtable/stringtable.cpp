#include <El/elementpch.hpp>
#include <El/QStream/qbStream.hpp>
#include <ctype.h>
#include "stringtable.hpp"
#include "stringtableImpl.hpp"

#include "iExtParser.hpp"
#include <El/Interfaces/iPreproc.hpp>

void StringTableDynamic::Create(RString name, float priority)
{
  _priority = priority;
  _name = name;
}

void StringTableDynamic::Init()
{
  base::Clear();
}

void StringTableDynamic::Add(RString name, RString value)
{
  // avoid duplicity in names
  const StringTableItem &check = Get(name);
  if (!IsNull(check))
  {
#if _VBS2
    // allow addons to overwrite registered strings
    StringTableItem &item = Set(name);
    item.value = value;
    return;
#else
    RptF("Item %s listed twice", (const char *)name);
    return;
#endif
  }

  base::Add(StringTableItem(name, value));
}


/* replaced by   QIStream::nextLine()
8/27/2002 Pepca

void NextLine(QIStream &f)
{
while (!f.eof() && f.get() != 0x0D);
if (!f.eof() && f.get() != 0x0A) f.unget();
}
*/

RString ReadColumn(QIStream &f)
{
  int c = f.get();
  if (c < 0) return "";
  // skip leading spaces:
  while (c != 0x0D && c != 0x0A && isspace(c))
  {
    c = f.get();
    if (c < 0) return "";
  }
  char buffer[4096];
  int i = 0;
  if (c == '\"')
  {
    c = f.get();
    if (c < 0) return "";
    while (true)
    {
      if (c == '\"')
      {
        c = f.get();
        if (c < 0)
        {
          buffer[i] = 0;
          return buffer;
        }
        if (c != '"')
        {
          while (c != 0x0D && c != 0x0A && c != ',')		
          {
            c = f.get();
            if (c < 0)
            {
              buffer[i] = 0;
              return buffer;
            }
          }
          if (c == 0x0D || c == 0x0A) f.unget();
          if (i == 0) return "";
          buffer[i] = 0;
          return buffer;
        }
      }
      if (i < sizeof(buffer) - 1) buffer[i++] = c;
      c = f.get();
      if (c < 0)
      {
        Assert(i > 0);
        buffer[i] = 0;
        return buffer;
      }
    }
    // Unaccessible
  }
  else
  {
    while (c != 0x0D && c != 0x0A && c != ',')		
    {
      if (i < sizeof(buffer) - 1) buffer[i++] = c;
      c = f.get();
      if (c < 0)
      {
        buffer[i] = 0;
        return buffer;
      }
    }
    if (c == 0x0D || c == 0x0A) f.unget();
    if (i == 0) return "";
    buffer[i] = 0;
    return buffer;
  }
}

void StringTableDynamic::Load(QIStream &f, bool useExtParser)
{
  if (useExtParser)
  {
    if (!GStringtableExtParserFunctions->Parse(f, *this, GLanguage))
    {
      RptF("Unsupported language %s in stringtable", cc_cast(GLanguage));
    }
  }
  else
  {
    int column = -1;
    while (true)
    {
      if (f.eof()) return;
      RString name = ReadColumn(f);
      if (name && stricmp(name, "LANGUAGE") == 0)
      {
        RString value = ReadColumn(f);
        int c = 0;
        while (value.GetLength() > 0)
        {
          if (stricmp(value, GLanguage) == 0)
          {
            column = c;
            break;
          }
          value = ReadColumn(f);
          c++;
        }
        f.nextLine();
        break;
      }
      else
        f.nextLine();
    }
    if (column < 0)
    {
      RptF("Unsupported language %s in stringtable",(const char *)GLanguage);
      column = 0;
    }

    while (!f.eof())
    {
      RString name = ReadColumn(f);
      if (name.GetLength()<=0 || stricmp(name, "COMMENT") == 0)
      {
        f.nextLine();
        continue;
      }
      for (int c=0; c<column; c++) ReadColumn(f);
      RString value = ReadColumn(f);
      if (value) Add(name, value);
      f.nextLine();
    }
  }
}

/// localization support
class StringTable
{
protected:
  AutoArray<StringTableDynamic> _tables;
  AutoArray<StringTableItem> _registered;

public:
  StringTable() {}
  void Clear();

  void Load(RString type, QIStream &f, float priority, bool init, bool useExtParser);
  void Unload(RString type);

  int Register(RString name);

  /// localize based on numerical ID
  RString Localize(int ids);

  /// localize based on string ID (starting with STR_)
  RString Localize(const char *str);

  /// check if localization is possible, if not, return empty string
  RString TryLocalize(const char *str);
  /// check if localization is possible, and by what stringtable
  RString TryLocalize(const char *str, RString &tableName);

protected:
  StringTableDynamic *FindStringtable(RString type);
};

void StringTable::Clear()
{
  _tables.Clear();
  _registered.Clear();
}

StringTableDynamic *StringTable::FindStringtable(RString type)
{
  for (int i=0; i<_tables.Size(); i++)
  {
    StringTableDynamic &table = _tables[i];
    if (stricmp(table.GetName(), type) == 0) return &table;
  }
  return NULL;
}

void StringTable::Load(RString type, QIStream &f, float priority, bool init, bool useExtParser)
{
  for (int i=0; i<_tables.Size(); i++)
  {
    StringTableDynamic &table = _tables[i];
    if (stricmp(table.GetName(), type) == 0)
    {
      DoAssert(table.GetPriority() == priority);
      if (init) table.Init();
      table.Load(f, useExtParser);
      return;
    }
  }

  // Create new stringtable
  int index = -1;
  for (int i=0; i<_tables.Size(); i++)
  {
    if (priority > _tables[i].GetPriority())
    {
      _tables.Insert(i);
      index = i;
      break;
    }
  }

  if (index < 0) index = _tables.Add();

  StringTableDynamic &table = _tables[index];
  table.Create(type, priority);
  table.Load(f, useExtParser);
}

void StringTable::Unload(RString type)
{
  for (int i=0; i<_tables.Size(); i++)
  {
    if (stricmp(_tables[i].GetName(), type) == 0)
    {
      _tables.Delete(i);
      return;
    }
  }
}

int StringTable::Register(RString name)
{
  // only strings from global stringtable can be registered
  StringTableDynamic *table = FindStringtable("Global");
  if (!table)
  {
    RptF("Cannot register string %s - global stringtable not found", (const char *)name);
    return -1;
  }

  const StringTableItem &item = (*table)[name];
  if (table->IsNull(item))
  {
    RptF("Cannot register unknown string %s", (const char *)name);
    StringTableItem noValue(name, Format("Missing string %s", cc_cast(name)));
    return _registered.Add(noValue);
  }
  return _registered.Add(item);
}

RString StringTable::Localize(int ids)
{
  if (ids < 0 || ids >= _registered.Size())
  {
    RptF("String id %d is not registered", ids);
#if _ENABLE_REPORT
    return "!!! UNREGISTERED STRING";
#else
    return "";
#endif
  }
#if _VBS2
  // load string from hashmap, as it may have been overwritten by an addon
  return Localize(_registered[ids].name);
#else
  return _registered[ids].value;
#endif
}

RString StringTable::Localize(const char *str)
{
  for (int i=0; i<_tables.Size(); i++)
  {
    const StringTableDynamic &table = _tables[i];
    const StringTableItem &item1 = table[str];
    if (!table.IsNull(item1)) return item1.value;
  }

  RptF("String %s not found", (const char *)str);
#if _ENABLE_REPORT
  return "!!! MISSING STRING";
#else
  return "";
#endif
}

RString StringTable::TryLocalize(const char *str)
{
  for (int i=0; i<_tables.Size(); i++)
  {
    const StringTableDynamic &table = _tables[i];
    const StringTableItem &item1 = table[str];
    if (!table.IsNull(item1)) return item1.value;
  }
  return RString();
}

RString StringTable::TryLocalize(const char *str, RString &tableName)
{
  tableName = RString();
  for (int i=0; i<_tables.Size(); i++)
  {
    const StringTableDynamic &table = _tables[i];
    const StringTableItem &item1 = table[str];
    if (!table.IsNull(item1))
    {
      tableName = table.GetName();
      return item1.value;
    }
  }
  return RString();
}

RString GLanguage;
StringTable GStringTable;

bool LoadStringtable(RString type, RString filename, float priority, bool init, const PreprocessorFunctions::Define *defs)
{
  // check if the extended parser can handle it first
  {
    QIFStreamB in;
    if (GStringtableExtParserFunctions->Open(filename, in))
    {
      GStringTable.Load(type, in, priority, init, true);
      return true;
    }
  }

  // process csv file now
  if (!QFBankQueryFunctions::FileExists(filename)) return false;

  QOStrStream out;
  if (!GStringtablePreprocFunctions->Preprocess(out, filename,PreprocessorFunctions::Params(false,defs)))
  {
    ErrF("Stringtable %s: preprocessor error", (const char *)filename);
    return false;
  }
  QIStrStream in;
  in.init(out.str(), out.pcount());
  GStringTable.Load(type, in, priority, init, false);
  return true;
}

bool LoadStringtable(RString type, QFBank &bank, RString filename, float priority, bool init, const PreprocessorFunctions::Define *defs)
{
  // check if the extended parser can handle it first
  {
    QIFStreamB in;
    if (GStringtableExtParserFunctions->Open(bank, filename, in))
    {
      GStringTable.Load(type, in, priority, init, true);
      return true;
    }
  }

  // process csv file now
  if (!bank.FileExists(filename)) return false;

  QOStrStream out;
  if (!GStringtablePreprocFunctions->Preprocess(out, bank, filename, PreprocessorFunctions::Params(false,defs)))
  {
    ErrF("Stringtable %s: preprocessor error", (const char *)filename);
    return false;
  }
  QIStrStream in;
  in.init(out.str(), out.pcount());
  GStringTable.Load(type, in, priority, init, false);
  return true;
}

void UnloadStringtable(RString type)
{
  GStringTable.Unload(type);
}

int RegisterString(RString name)
{
  return GStringTable.Register(name);
}

RString LocalizeString(int ids)
{
  return GStringTable.Localize(ids);
}

RString LocalizeString(const char *str)
{
  return GStringTable.Localize(str);
}

RString TryLocalizeString(const char *str)
{
  return GStringTable.TryLocalize(str);
}

RString TryLocalizeString(const char *str, RString &tableName)
{
  return GStringTable.TryLocalize(str,tableName);
}

RString Localize(RString str)
{
  if (str[0] == '@') return LocalizeString((const char *)str + 1);
  return str;
}

RString TryLocalize(RString str, RString &tableName)
{
  if (str[0] == '@')
  {
    return TryLocalizeString((const char *)str + 1,tableName);
  }
  else
  {
    tableName = RString();
    return str;
  }
}


RString LocalizedString::GetLocalizedValue() const
{
  switch (_type)
  {
  case Stringtable:
    return LocalizeString(_id);
  default: // case PlainText
    return _id;
  }
}

typedef AutoArray<RString, MemAllocLocal<RString,32> > ReplaceStringArgs;

/// functor will replace %s by the strings given in the input array
class ReplaceStringFunc
{
protected:
  int _index;
  ReplaceStringArgs &_args;

public:
  ReplaceStringFunc(ReplaceStringArgs &args) : _args(args) {_index = 0;}

  RString operator ()(const char *&ptr)
  {
    // only %s are recognized
    if (*ptr != 's') return RString();
    ptr++;
    if (_index >= _args.Size()) return RString();
    return _args[_index++];
  }
};

RString LocalizedFormatedStringTemp::GetLocalizedValue() const
{
  // first argument is format
  if (_args.Size()<=0)
  {
    Fail("No localization format string");
    return RString();
  }
  RString format = _args[0].GetLocalizedValue();
  // localize arguments
  ReplaceStringArgs args;
  args.Resize(_args.Size()-1);
  for (int i=0; i<args.Size(); i++)
  {
    const LocalizedString &arg = _args[i+1];
    args[i] = arg.GetLocalizedValue();
  }
  
  ReplaceStringFunc func(args);
  return format.ParseFormat(func);
}

/**
SetFormat can be used only once
*/

void LocalizedFormatedString::SetFormat(LocalizedString::Type type, RString id)
{
  Assert(_args.Size()==0);
  _args.Access(0);
  _args[0] = LocalizedString(type,id);
}
/**
SetFormat must be used before AddArg
*/
void LocalizedFormatedString::AddArg(LocalizedString::Type type, RString id)
{
  Assert(_args.Size()>0);
  _args.Add(LocalizedString(type,id));
}

RString LocalizedFormatedString::GetLocalizedValue() const
{
  return LocalizedFormatedStringTemp(_args).GetLocalizedValue();
}


void ClearStringtable()
{
  GStringTable.Clear();
}
