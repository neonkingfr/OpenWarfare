#ifdef _MSC_VER
#pragma once
#endif

#ifndef _DEBUG_ENGINE_INTERFACE_HPP
#define _DEBUG_ENGINE_INTERFACE_HPP

// Interface with BIDebugEngine.dll

#include "El/Interfaces/iDebugEngineInterface.hpp"

class IToDll
{
public:
  IToDll() {}
  virtual ~IToDll() {}

  virtual void Startup() = NULL;
  virtual void Shutdown() = NULL;

  virtual void ScriptLoaded(IDebugScript *script, const char *name) = NULL;
  virtual void ScriptEntered(IDebugScript *script) = NULL;
  virtual void ScriptTerminated(IDebugScript *script) = NULL;

  virtual void FireBreakpoint(IDebugScript *script, unsigned int bp) = NULL;
  virtual void Breaked(IDebugScript *script) = NULL;

  virtual void DebugEngineLog(const char *str) = NULL;
};

class IToApp
{
public:
  IToApp() {}
  virtual ~IToApp() {}

  virtual void DebugLogF(const char *str) = NULL;
};

#endif
