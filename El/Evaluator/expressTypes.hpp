#ifndef _EXPRESS_TYPES_HPP
#define _EXPRESS_TYPES_HPP

/// list of basic types
#define TYPES_DEFAULT(XX, Category) \
  XX("SCALAR",GameScalar, CreateGameDataScalar, "@STR_EVAL_TYPESCALAR", "Number", "A real number.", Category) \
  XX("BOOL",GameBool, CreateGameDataBool, "@STR_EVAL_TYPEBOOL", "Boolean", "Boolean (<f>true</f> or <f>false</f>).", Category) \
  XX("ARRAY",GameArray, CreateGameDataArray, "@STR_EVAL_TYPEARRAY", "Array", "An array of items, each may be of any type.", Category) \
  XX("STRING",GameString, CreateGameDataString, "@STR_EVAL_TYPESTRING", "String", "An ASCII string.", Category) \
  XX("NOTHING",GameNothing, CreateGameDataNothing, "@STR_EVAL_TYPENOTHING", "Nothing", "Nothing - no value.", Category) \
  XX("ANY",GameAny, CreateGameDataAny, "@STR_EVAL_TYPEANY", "Any", "Anyhing - any value.", Category) \
  XX("NAMESPACE",GameNamespace, CreateGameDataNamespace, "@STR_EVAL_TYPENAMESPACE", "Namespace", "Namespace - set of variables.", Category) \

/// list of combined types
#define TYPES_DEFAULT_COMB(XX, Category) \
  XX("",GameAny, NULL, "", "Anything", "Anything, including nothing.", Category) \
  XX("",GameVoid, NULL, "", "Any Value", "Any value.", Category) \
  XX("",GameString | GameArray, NULL, "", "String or Array", "<t>String</t> or <t>Array</t>.", Category) \
  XX("",GameScalar | GameNothing, NULL, "", "Number or Nothing", "<t>Number</t> or <t>Nothing</t>.", Category) \

#if USE_PRECOMPILATION
/// list of basic types
# define TYPES_CODE(XX, Category) \
  XX("CODE", GameCode, CreateGameDataCode, "code", "Code", "Part of code (compiled).", Category)

/// list of combined types
# define TYPES_CODE_COMB(XX, Category) \
  XX("",GameString | GameCode, NULL, "", "String or Code", "<t>String</t> or <t>Code</t>.", Category)
#endif

TYPES_DEFAULT(DECLARE_TYPE, "Default")
#if USE_PRECOMPILATION
TYPES_CODE(DECLARE_TYPE, "Default")
#endif

#define GameVoid GameAny // any value type

#if !USE_PRECOMPILATION
const GameType GameCode(GameString);
#endif

#endif // _EXPRESS_TYPES_HPP

