// ParamArchiveTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/ParamArchive/paramArchive.hpp>

int main(int argc, char* argv[])
{
	float x = 100;

	ParamArchiveSave ars(1);
	ars.Serialize("x", x, 1);
	ars.Save("test.ar");

	float y;
	ParamArchiveLoad arl("test.ar");
	arl.Serialize("x", y, 1);

	return 0;
}
