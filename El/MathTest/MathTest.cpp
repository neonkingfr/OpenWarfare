// MathTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/Math/math3D.hpp>

int main(int argc, char* argv[])
{
	Matrix4 transf;
	Matrix3 orient = M3Identity;
	Vector3 pos(10, 10, 10);
	transf.SetPosition(pos);
	transf.SetOrientation(orient);
	transf = transf * transf;
	return 0;
}
