#include <El/elementpch.hpp>

#pragma optimize ("t",on)

#include "colorsFloat.hpp"

#pragma warning(disable:4073)
#pragma init_seg(lib) // we need the constants to be available before other parts start running

extern const ColorP HBlackP(0,0,0);
extern const ColorP HWhiteP(1,1,1);
