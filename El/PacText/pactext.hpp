#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PACTEXT_HPP
#define _PACTEXT_HPP

// basic structure for storing bitmap data
#include <Es/Types/memtype.h>
#include "pactexttypes.hpp"
#include <El/QStream/qStream.hpp>
#include <El/Color/colors.hpp>
#include <El/Enum/enumNames.hpp>

#include <Es/Containers/boolArray.hpp>



#define PAC_FORMAT_ENUM(type,prefix,XX) \
  XX(type, prefix, P8) \
  XX(type, prefix, AI88) \
  XX(type, prefix, RGB565) \
  XX(type, prefix, ARGB1555) \
  XX(type, prefix, ARGB4444) \
  XX(type, prefix, ARGB8888) \
  XX(type, prefix, DXT1) \
  XX(type, prefix, DXT2) \
  XX(type, prefix, DXT3) \
  XX(type, prefix, DXT4) \
  XX(type, prefix, DXT5) \
  XX(type, prefix, Unknown)

/// surface format
#ifndef DECL_ENUM_PAC_FORMAT
#define DECL_ENUM_PAC_FORMAT
DECL_ENUM(PacFormat)
#endif
DECLARE_ENUM(PacFormat, Pac, PAC_FORMAT_ENUM)

static inline bool PacFormatIsDXT(PacFormat format)
{
  return format>=PacDXT1;
}

class PacPalette;

#if _RELEASE
  #define CHECKSUMS 0
#else
  #define CHECKSUMS 0
#endif

class PacLevelMem
{
  public:
  // short for texture dimensions and pitch should be enough
  short _w,_h; // note: 
  short _pitch; // DDraw requires DWORD (or QWORD) alignement

  SizedEnum<PacFormat,char> _sFormat; // source (file) format
  SizedEnum<PacFormat,char> _dFormat; // destination (memory) format

  int _start; // where in the file this mipmap level start

  // note: _memData is often used as temporary pointer
  //void *_memData; // _data for one mipmap levels


  public:
  PacLevelMem();

  PackedColor GetPixel(const void *data, float u, float v) const;
  PackedColor GetPixelInt(const void *data, int u, int v) const;

  static void DecompressDXT1(void *dst, const void *src, int w, int h);
  static void DecompressDXT2(void *dst, const void *src, int w, int h);
  static void DecompressDXT4(void *dst, const void *src, int w, int h);

  protected:

  // paa 16b formats loading
  int LoadPaaBin16( QIStream &in, void *mem, const PacPalette *pal ) const;
  int LoadPaaDXT( QIStream &in, void *mem, const PacPalette *pal, int texSize ) const;


  // pac formats loading
  int LoadPacARGB1555( QIStream &in, void *mem, const PacPalette *pal ) const;
  int LoadPacRGB565( QIStream &in, void *mem, const PacPalette *pal ) const;
  
  public:
  void Interpolate
  (
    void *data, void *withData,
    const PacLevelMem &with, float factor
  );

  //! fill data with a color
  bool FillMipmap(void *dta, PackedColor color) const;
  
  int LoadPac( QIStream &in, void *mem, const PacPalette *pal, int texSize ) const; // format set by init

  int LoadPaa( QIStream &in, void *mem, const PacPalette *pal, int texSize ) const; // format set by init

  void SetStart(int offset){_start=offset;}
  int Init(PacFormat sFormat); // no file init - used for JPG files
  int Init(QIStream &in, PacFormat sFormat);
  void SetDestFormat(PacFormat dFormat, int align);
  //int Skip( QIStream &in );
  PacFormat SrcFormat() const {return _sFormat.GetEnumValue();}
  PacFormat DstFormat() const {return _dFormat.GetEnumValue();}
  int Pitch() const {return _pitch;}
  int Size() const {Assert(_pitch > 0); return _pitch*_h;}
  bool TooLarge( int max=256 ) const {return _w>max || _h>max;}
  /// size estimation - no alignment considered
  /** Usefull when allocating memory for GetMipmapData */
  static int MipmapSize(PacFormat format, int w, int h);
};

const char *FormatName( PacFormat format );
PacFormat PacFormatFromDesc( int desc, bool &alpha );

class MipInfo
{
  public:
  Texture *_texture;
  int _level;

  MipInfo( Texture *texture, int level )
  :_texture(texture),_level(level)
  {}
  MipInfo()
  :_texture(NULL),_level(-1)
  {}

  bool IsOK() const {return _level>=0;}
  void SetNoTexture()
  {
    _texture = NULL;
    _level = 0;
  }
};

TypeIsBinary(MipInfo);


#define TRANSPARENT1_RGB 0xff00ff
#define TRANSPARENT2_RGB 0x00ffff

/// flags used to indicate clamping in UV directions
enum {TexClampU=1,TexClampV=2};

/// information about colors and attributes
class PacPalette: private NoCopy
{
  //friend class PacLevel;

  public:
  //MemoryHeap *_heap;
  int _nColors; // palette size
  DWORD *_palette; // 24-bit RGB format
  //Pixel *_palPixel; // pixel-format palette (suitable for copy mode)

  Color _averageColor;
  PackedColor _averageColor32;
  //! color used to maximize dynamic range
  PackedColor _maxColor32;

  /// forced clamping in any given direction
  int _clampFlags;
  // this information is same for all mipmaps, but is also copied in mipmap header
  int _transparentColor;
  //! check if _maxColor32 was loaded
  bool _maxColorSet;
  bool _isAlpha,_isTransparent,_isAlphaNonOpaque;

  #if CHECKSUMS
    mutable bool _checksumValid;
    mutable int _checksum;
  #endif

  public:
  PacPalette();
  ~PacPalette();

  int Load( QIStream &in, int *startOffsets, int maxStartOffsets, PacFormat format );
  static int Skip( QIStream &in );

  ColorVal AverageColor() const {return _averageColor;}
  PackedColor AverageColor32() const {return _averageColor32;}
  PackedColor MaxColor32() const {return _maxColor32;}
};

//! Set of texture types
enum TextureType
{
  /// diffuse color, SRGB color space
  TT_Diffuse,
  /// diffuse color, linear color space
  TT_Diffuse_Linear,
  /// detail texture, linear color space
  TT_Detail,
  /// normal map
  TT_Normal,
  /// irradiance map
  TT_Irradiance,
  //! Map with random values from interval <0.5, 1>
  TT_RandomTest,
  /// tree crown calculation texture
  TT_TreeCrown,
  /// diffuse "macro" object color, SRGB color space
  TT_Macro,
  /// ambient shadow layer
  TT_AmbientShadow,
  /// amount of specular layer
  TT_Specular,
  /// dithering texture
  TT_Dither,
  /// amount of detail specular layer
  TT_DTSpecular,
  //! Number of texture types
  NTextureType
};


/** TFAnizotropic means auto-detect anisotropy level based on PixelShaderID while loading the material **/

#define TEXFILTER_ENUM(type,prefix,XX) \
  XX(type, prefix, Point) \
  XX(type, prefix, Linear) \
  XX(type, prefix, Trilinear) \
  XX(type, prefix, Anizotropic) \
  XX(type, prefix, Anizotropic2) \
  XX(type, prefix, Anizotropic4) \
  XX(type, prefix, Anizotropic8) \
  XX(type, prefix, Anizotropic16) \

//! texture sampling
#ifndef DECL_ENUM_TEX_FILTER
#define DECL_ENUM_TEX_FILTER
DECL_ENUM(TexFilter)
#endif
DECLARE_ENUM(TexFilter,TF,TEXFILTER_ENUM)

//! Texture source interface
class ITextureSource
{
public:
  virtual ~ITextureSource() {}

  //! Init data source
  /*! Must be called before any other methods.*/
  virtual bool Init(PacLevelMem *mips, int maxMips) = 0;
  //! Get mipmap count.
  virtual int GetMipmapCount() const = 0;
  //! Get source data format.
  virtual PacFormat GetFormat() const = 0;
  //! Get the type of the texture
  virtual TextureType GetTextureType() const = 0;
  //! Get max. filter having sense for given texture
  virtual TexFilter GetMaxFilter() const = 0;

  //! Check if texture should be alpha blended
  virtual bool IsAlpha() const = 0;
  //! Check if texture should be alpha blended and overall opacity is very low
  virtual bool IsAlphaNonOpaque() const = 0;
  //! Check if texture should be alpha tested
  virtual bool IsTransparent() const = 0;
  
  bool IsPlatformLittleEndian() const
  {
    #ifdef _M_PPC
    return false;
    #else
    return true;
    #endif
  }
  /// on some platforms (X360) we may have dual data - some little endian, some big endian
  virtual bool IsLittleEndian() const = 0;
  //! Get mipmap data.
  virtual bool GetMipmapData(void *mem, const PacLevelMem &mip, int level, int texSize) const = 0; 

  //! Request background loading of the data.
  virtual bool RequestMipmapData(
    const PacLevelMem *mips, int levelMin, int levelMax, FileRequestPriority prior
  ) const = 0;  

  //! Get average color (packed format)
  virtual PackedColor GetAverageColor() const = 0;
  //! Get max. dynamic range color (packed format)
  virtual PackedColor GetMaxColor() const = 0;

  //! We know from some reason texture is alpha blended
  virtual void ForceAlpha() = 0;

  /// modification time, 0 if not applicaple/not available
  virtual QFileTime GetTimestamp() const = 0;
  virtual void FlushHandles() = 0;

  // @{ clamping interface
  virtual int GetClamp() const = 0;
  virtual void SetClamp(int clampFlags) = 0;
  //@}
};

//! all routines required to create texture
class ITextureSourceFactory
{
  public:
  //! quick check if texture source can exist
  virtual bool Check(const char *name) = 0;
  //! preload any files as necessary so that Create will be quick.
  /*!
  In typical implementation data source asks file server to preload any files as necessary.
  Calling this function before Create is optional.
  */
  virtual bool PreInit(
    const char *name,
    RequestableObject *obj=NULL, RequestableObject::RequestContext *ctx=NULL
  ) = 0;
  //! create texture source
  virtual ITextureSource *Create(const char *name, PacLevelMem *mips, int maxMips) = 0;
  
  /// determine texture type based on name only, no file access
  virtual TextureType GetTextureType(const char *name) = 0;
  /// determine max. filter based on name only, no file access
  virtual TexFilter GetMaxFilter(const char *name) = 0;
};

/// texture containing a simple procedural texture
/**
Helper class for easier implementation of simple procedural textures
*/
class TextureSourceSimple: public ITextureSource
{
  protected:
  int _nMipmaps; //!< source file mipmap count
  int _w,_h; //!< texture dimensions
  PacFormat _format; //!< source file pixel format

  public:
  TextureSourceSimple(
    PacFormat format, int w, int h, int nMipmaps
  );
  ~TextureSourceSimple();

  bool Init(PacLevelMem *mips, int maxMips);
  
  int GetMipmapCount() const {return _nMipmaps;}
  PacFormat GetFormat() const {return _format;}
  
  //@{ functions which still need to be implemented
  bool GetMipmapData(void *mem, const PacLevelMem &mip, int level, int texSize) const = 0;
  PackedColor GetAverageColor() const = 0;
  bool IsAlpha() const = 0;
  //@}
  
  /// procedural data are always native
  bool IsLittleEndian() const {return IsPlatformLittleEndian();}
  
  bool RequestMipmapData(
    const PacLevelMem *mips, int levelMin, int levelMax, FileRequestPriority prior
  ) const
  {
    // data are always ready
    return true;
  }

  /// no dynamic range compression
  PackedColor GetMaxColor() const {return PackedColor(0xffffffff);}

  // no timestamps
  virtual QFileTime GetTimestamp() const {return 0;}
  virtual void FlushHandles() {}

  //bool IsAlpha() const {return _color.A()<1.0f;}
  bool IsTransparent() const {return false;}
  /// hack used to fix textures that have alpha flag missing
  void ForceAlpha(){}
};


//! create texture source based on identifier (file name)
ITextureSourceFactory *SelectTextureSourceFactory(const char *name);

/// create single pixel colored ARGB8888 texture
ITextureSource *CreateColorTextureSource(ColorVal color);
//! Create random texture for alpha testing purposes
ITextureSource *CreateRandomTestTextureSource();
/// load Pac texture from a stream
ITextureSource *CreatePacMemTextureSource(QIStream *stream, RString name);
/// create a dithered pattern texture
ITextureSource *CreateDitherTextureSource(int dim, int min, int max);
/// create a perlin noise texture
ITextureSource *CreatePerlinNoise(int x, int y, int nMipmaps, float xScale, float yScale, float min, float max);


#endif

