// implementation of performance monitor + logging

#include <El/elementpch.hpp>
#include <El/Common/perfLog.hpp>
#include <El/Common/perfProf.hpp>

#if _MSC_VER && !defined INIT_SEG_COMPILER
  // we know no memory allocation is done here
  // we want to be constructed as early as possible - sooner than RStringB, which does allocation
  // otherwise allocation scopes are not working
  #pragma warning(disable:4074)
  #pragma init_seg(compiler)
  #define INIT_SEG_COMPILER
#endif

#include <Es/Common/win.h>

//#include "psapi.h"
// search for "psapi"
// Structure for GetProcessMemoryInfo()

typedef struct _PROCESS_MEMORY_COUNTERS {
    DWORD cb;
    DWORD PageFaultCount;
    SIZE_T PeakWorkingSetSize;
    SIZE_T WorkingSetSize;
    SIZE_T QuotaPeakPagedPoolUsage;
    SIZE_T QuotaPagedPoolUsage;
    SIZE_T QuotaPeakNonPagedPoolUsage;
    SIZE_T QuotaNonPagedPoolUsage;
    SIZE_T PagefileUsage;
     SIZE_T PeakPagefileUsage;
} PROCESS_MEMORY_COUNTERS;
typedef PROCESS_MEMORY_COUNTERS *PPROCESS_MEMORY_COUNTERS;

#ifdef _WIN32

typedef BOOL WINAPI GetProcessMemoryInfoF
(
  HANDLE Process,
  PPROCESS_MEMORY_COUNTERS ppsmemCounters,
  DWORD cb
);

static GetProcessMemoryInfoF *GetProcessMemoryInfo;

#endif

size_t GetMemoryUsedSize()
{
#ifdef _WIN32
  if (!GetProcessMemoryInfo) return 0;
  PROCESS_MEMORY_COUNTERS pc;
  pc.cb = sizeof(pc);
  if (GetProcessMemoryInfo(GetCurrentProcess(),&pc,sizeof(pc)))
  {
    return pc.WorkingSetSize;
  }
#endif
  return 0;
}

size_t GetMemoryCommitedSize()
{
#ifdef _WIN32
  if (!GetProcessMemoryInfo) return 0;
  PROCESS_MEMORY_COUNTERS pc;
  pc.cb = sizeof(pc);
  if (GetProcessMemoryInfo(GetCurrentProcess(),&pc,sizeof(pc)))
  {
    return pc.PagefileUsage;
  }
#endif
  return 0;
}

static struct InitPSAPI
{
  InitPSAPI()
  {
    static bool once = true;
    if (!once) return;
    once = false;
    #if defined _WIN32 && !defined _XBOX
    HINSTANCE module = LoadLibraryA("psapi.dll");
    if (!module) return;
    void *pa = GetProcAddress(module,"GetProcessMemoryInfo");
    if (pa)
    {
      GetProcessMemoryInfo = (GetProcessMemoryInfoF *)pa;
    }
    #endif
  }
} SInitPSAPI; 


#if _ENABLE_PERFLOG

#include <Es/Strings/bString.hpp>
#include <Es/Common/fltopts.hpp>
#include <Es/Containers/staticArray.hpp>

void PerfCounters::Enable( bool value )
{
  if( !_enabled && value )
  {
    Reset();
    Reinit();
  }
  _enabled=value;
  if( !_enabled ) _first=true;
}


void PerfCounters::SavePCHeaders()
{
  _logFile <<"  wset";
  //_logFile <<"  mcom";

}

void PerfCounters::SavePCValues()
{
  _logFile << (int)(GetMemoryUsedSize()/(1024*1024));
  //_logFile << (int)(GetMemoryCommitedSize()/(1024*1024));
  // note: works only for Win9x
  // TODO: WinNT performance counters, see HKEY_PERFORMANCE_DATA
}

static int CompareCounters(const int *c1, const int *c2, const PerfCounters *context)
{
  const PerfCounterSlot *slot1=context->Slot(*c1);
  const PerfCounterSlot *slot2=context->Slot(*c2);
  return slot2->value-slot1->value;
}

struct CounterSlotInfo
{
  int index;
  int value;
  int count;
};
TypeIsSimple(CounterSlotInfo)

static int CompareCountersAvg(const CounterSlotInfo *c1, const CounterSlotInfo *c2)
{
  return c2->value - c1->value;
}

#include <Es/Algorithms/qsort.hpp>

/// format numeric value so that it fits in 5 characters
template <class String>
static void FormatValue(String &value, int slotValue)
{
  if (slotValue<100000)
  {
    // time in 1/100 ms
    sprintf(value,"%6d",slotValue);
  }
  else if (slotValue<1000000)
  {
    // time in sec
    sprintf(value,"%5.3gk",float(slotValue)/1000);
  }
  else
  {
    // time in sec
    sprintf(value,"%5.3gM",float(slotValue)/1000000);
  }
}

void PerfCounters::Diagnose(int maxCounters)
{
  if( !_enabled ) return;

  // avoid increasing the size during the function
  int slotsCount = _countersFreeSlot;
  saturateMin(slotsCount, _maxSlots);

  // auto-enable counters
  BString<512> header;
  BString<512> line;
  BString<512> count;
  AUTO_STATIC_ARRAY(int,sorted,64);
  for( int i=0; i<slotsCount; i++ )
  {
    const PerfCounterSlot &slot=_counters[i];
    if( slot.disabled ) continue;
    if( slot.value==0 ) continue;
    sorted.Add(i);
  }
  QSort(sorted.Data(),sorted.Size(),this,CompareCounters);
  for( int i=0; i<sorted.Size() && maxCounters>0; i++ )
  {
    int slotI = sorted[i];
    const PerfCounterSlot &slot=_counters[slotI];
    if( slot.disabled ) continue;
    if( slot.value==0 || slot.scale==0) continue;
    BString<7> value;
    BString<7> name;
    sprintf(name,"%6s",cc_cast(slot._name));
    int slotValue = slot.value/slot.scale;
    // TODO: formatting time value
    FormatValue(value,slotValue);
    strcat(header,name);
    strcat(line,value);
    if (_profilers)
    {
      FormatValue(value,slot.count);
      strcat(count,value);
    }
    --maxCounters;
  }
  LogF("%s",header.cstr());
  LogF("%s",line.cstr());
  if (_profilers)
  {
    LogF("%s",count.cstr());
  }
}

void PerfCounters::WriteCounters(StaticArrayAuto<CounterSlotInfo> &sorted, int maxCounters)
{
  BString<512> header;
  BString<512> line;
  BString<512> count;
  for( int i=0; i<sorted.Size() && maxCounters>0; i++ )
  {
    int slotI = sorted[i].index;
    const PerfCounterSlot &slot=_counters[slotI];
    if( slot.disabled ) continue;
    int slotValue = sorted[i].value;
    if (slotValue == 0) continue;
    BString<7> value;
    BString<7> name;
    sprintf(name,"%6s",cc_cast(slot._name));
    // TODO: formatting time value
    FormatValue(value,slotValue);
    strcat(header,name);
    strcat(line,value);
    if (_profilers)
    {
      FormatValue(value,sorted[i].count);
      strcat(count,value);
    }
    --maxCounters;
  }
#if _SUPER_RELEASE
  bool IsDedicatedServer();
  if (IsDedicatedServer())
  {
    RptF("%s",header.cstr());
    RptF("%s",line.cstr());
    RptF("%s",count.cstr());
  }
#else
  LogF("%s",header.cstr());
  LogF("%s",line.cstr());
  LogF("%s",count.cstr());
#endif
}

/**
@param lastFrame Report last frame instead of the slowest one.
*/
void PerfCounters::DiagnoseAvg(int maxCounters, bool lastFrame)
{
  if( !_enabled && !lastFrame ) return;
  _avgDiagnosed = true;

  // avoid increasing the size during the function
  int slotsCount = _countersFreeSlot;
  saturateMin(slotsCount, _maxSlots);

  // select nonempty frame and frame with minimal fps
  int frameMaxIndex = -1;
  int frames = 0;
  int frameMaxValue = 0;
  for (int frame=0; frame<PerfCounterSlot::NLastValues; frame++)
  {
    // select max. value from all counters - this should closely correspond to lowest fps
    // there is some scope which holds whole frame
    int frameMax = 0;
    for( int i=0; i<slotsCount; i++ )
    {
      const PerfCounterSlot &slot = _counters[i];
      if (slot.disabled) continue;
      saturateMax(frameMax, slot.LastValue(frame));
    }
    if (frameMax > 0) frames++;
    if (frameMax >= frameMaxValue) // latest value is more interesting for us
    {
      frameMaxValue = frameMax;
      frameMaxIndex = frame;
    }
  }
  if (frames == 0) return;
  Assert(frameMaxIndex >= 0);
  if (lastFrame)
  {
    frameMaxIndex = PerfCounterSlot::NLastValues-1;
  }

  // auto-enable counters
  AUTO_STATIC_ARRAY(CounterSlotInfo,sorted,64);

  // min fps
  for( int i=0; i<slotsCount; i++ )
  {
    const PerfCounterSlot &slot = _counters[i];
    if (slot.disabled) continue;
    int value = toLargeInt((float)slot.LastValue(frameMaxIndex) / (float)slot.scale);
    if (value > 0)
    {
      int index = sorted.Add();
      sorted[index].index = i;
      sorted[index].value = value;
      sorted[index].count = slot.LastCount(frameMaxIndex);
    }
  }
  QSort(sorted.Data(),sorted.Size(),CompareCountersAvg);
  WriteCounters(sorted, maxCounters);

  sorted.Clear();

  // avg fps
  for( int i=0; i<slotsCount; i++ )
  {
    const PerfCounterSlot &slot = _counters[i];
    if (slot.disabled) continue;
    int value = toLargeInt((float)slot.SumValue() / (float)(slot.scale * frames));
    if (value > 0)
    {
      int index = sorted.Add();
      sorted[index].index = i;
      sorted[index].value = value;
      sorted[index].count = slot.SumCount()/frames;
    }
  }
  QSort(sorted.Data(),sorted.Size(),CompareCountersAvg);
  WriteCounters(sorted, maxCounters);
}

void PerfCounters::Save( float fps )
{
  _lastFrameStart = _frameStart;

  // avoid increasing the size during the function
  int slotsCount = _countersFreeSlot;
  saturateMin(slotsCount, _maxSlots);

  for (int i=0; i<slotsCount; i++)
  {
    PerfCounterSlot &slot=_counters[i];
    slot.savedValue = slot.value;
    slot.savedCount = slot.count;
  }
  // auto-enable counters
  for( int i=0; i<slotsCount; i++ )
  {
    PerfCounterSlot &slot=_counters[i];
    if( slot.disabled ) continue;
    if( !slot.enabled )
    {
      if( slot.value==0 ) continue;
      slot.enabled=true;
    }
  }
  if( !_enabled ) return;
  if( _skip>0 )
  {
    --_skip;
    return;
  }
  // check if save is enabled
  // repeat headers
  if( _lines++>=30 ) _first=true;
  if( _first )
  {
    _first=false;
    _lines=0;
    if (_logFile.IsOpen())
    {
      _logFile << "   fps";
      SavePCHeaders();
      _logFile << " Alloc";
      _logFile << " NFree";
      //_logFile << " NB100";
      for( int i=0; i<slotsCount; i++ )
      {
        const PerfCounterSlot &slot=_counters[i];
        if( slot.disabled || !slot.enabled ) continue;
        char name[256];
        sprintf(name,"%6s",cc_cast(slot._name));
        name[6]=0;
        _logFile<<name;
      }
      _logFile << "\n";
    }
  }
  if( _line )
  {
    _line=false;
    _logFile << "----------------------------------------------------";
    _logFile << "-----------------------------------------\n";
  }
  if (_logFile.IsOpen())
  {
    _logFile << floatMin(fps,999);
    SavePCValues();
    _logFile << (int)MemoryUsed()/(1024*1024);
    _logFile << (int)MemoryFreeBlocks();
    //_logFile << MemoryAllocatedBlocks()/100;
    for( int i=0; i<slotsCount; i++ )
    {
      const PerfCounterSlot &slot=_counters[i];
      if( slot.disabled || !slot.enabled || slot.scale==0) continue;
      _logFile<<(slot.value/slot.scale);
    }

    _logFile << "\n";
  }
}

static PerfCounterSlot GPerfCountersCounters[150] INIT_PRIORITY_URGENT;
PerfCounters GPerfCounters(false, lenof(GPerfCountersCounters), GPerfCountersCounters, 0, NULL) INIT_PRIORITY_URGENT;

static PerfCounterSlot GPerfProfilersCounters[450] INIT_PRIORITY_URGENT;
// should store all ScopeProfiler instances in a single (in-game) frame
static CaptureBufferItem GCaptureBuffer[100000];
PerfCounters GPerfProfilers(true,
  lenof(GPerfProfilersCounters), GPerfProfilersCounters,
  lenof(GCaptureBuffer), GCaptureBuffer) INIT_PRIORITY_URGENT;

void OpenPerfCounters()
{
  #ifdef _XBOX
  GPerfCounters.Open("u:\\events.spf");
  GPerfProfilers.Open("u:\\timing.spf");
  #else
  GPerfCounters.Open("events.spf");
  GPerfProfilers.Open("timing.spf");
  #endif
}
void ClosePerfCounters()
{
  GPerfCounters.Close();
  GPerfProfilers.Close();
}

void PerfCounter::operator +=( int value )
{
  if( _slotIndex<0 ) _slotIndex=_bank->New(_name, _nameCategory, _scale);
  if( _slotIndex>=0 )
  {
    PerfCounterSlot *slot = Slot();
    if (slot)
    {
      slot->value+=value;
      slot->count++;
    }
  }
}
int PerfCounter::GetValue() const
{
  if( _slotIndex>=0 )
  {
    const PerfCounterSlot *slot = Slot();
    if (slot) return slot->value;
  }
  return 0;
}

bool PerfCounter::TimingEnabled() const
{
  if (_slotIndex >= 0)
  {
    const PerfCounterSlot *slot = Slot();
    return slot && slot->_timingEnabled;
  }
  return false;
}

PerfCounters::PerfCounters(bool profilers,
  const int maxSlots, PerfCounterSlot *counters,
  const int captureBufferSize, CaptureBufferItem *captureBuffer
  )
  : _maxSlots(maxSlots), _counters(counters), _captureBufferSize(captureBufferSize), _captureBuffer(captureBuffer)
{
  _constructed=true;
  _enabled=false;
  _skip=5; // skip some frames - to reach stable environment
  _profilers = profilers;
  _countersFreeSlot = 0; // all slots unused now
  _frameStart = 0;
  _lastFrameStart = 0;

  _captureBufferIndex = 0;
  _captureSingleFrame = 0;
  _captureFrameThreshold = 0;
  _capturePermanent = false;
  _capture = false;
  _captureReady = DoNotCapture;

  Reinit();
  // some default setting
  if (!_profilers)
  {
    DEF_COUNTER(mSize, 1024);
    DEF_COUNTER(tHeap, 1024);
    DEF_COUNTER(tGRAM, 1024);
  }
}

void PerfCounters::Open(const char *name)
{
  _logFile.Open(name);
  _skip=3; // skip some frames - to reach stable environment
}

void PerfCounters::Close()
{
  _logFile.Close();
}

PerfCounters::~PerfCounters()
{
  // make sure all columns have corresponding header
  _first=true;
  Save(0);
  _constructed = false;
}

void PerfCounters::Reinit()
{
  _first=true;
  _line=true;
  _lines=0;
  _frameCount = 0;
  _avgDiagnosed = false;

  // avoid increasing the size during the function
  int slotsCount = _countersFreeSlot;
  saturateMin(slotsCount, _maxSlots);

  for( int i=0; i<slotsCount; i++ )
  {
    _counters[i].Reset();
  }
}

void PerfCounters::Reset()
{
  _frameCount = 0;
  _avgDiagnosed = false;

  // avoid increasing the size during the function
  int slotsCount = _countersFreeSlot;
  saturateMin(slotsCount, _maxSlots);

  for( int i=0; i<slotsCount; i++ )
  {
    _counters[i].Reset();
  }
}

void PerfCounters::AddCapture(__int64 start, __int64 end, const RString &moreInfo, PerfCounter *counter)
{
  // new item need to be created
  LONG index = InterlockedIncrement(&_captureBufferIndex);
  if (index > _captureBufferSize)
  {
    // the capture does not fit to the buffer
    __asm nop;
    return;
  }
  // index is now first free slot -> fill last used slot

  if (_captureReady==CaptureTail)
  {
    //LogF("%s: start %lld, end %lld (of %lld)", counter->GetName(), start-_capturedFrameStart,end-_capturedFrameStart,_capturedFrameEnd-_capturedFrameStart);
  }
  CaptureBufferItem &item = _captureBuffer[index-1];
  // when item is closed which was never open, start is zero - saturate to frame
  if (start<_capturedFrameStart) start = _capturedFrameStart;
  item._start = start - _capturedFrameStart;
  item._duration = end - start;
  item._threadId = GetCurrentThreadId();
  item._moreInfo = moreInfo;
  item._slotIndex = counter->SlotNumber(GPerfProfilers);
}

void PerfCounters::Frame(float fps, float frameTime, int nCongregate)
{
  // handle the capture buffer
  bool captured = _capture;
  _capture = false; // stop capturing now

  // Analysis - aggregate frame capture to counters
  // note: this is not 100 % thread safe. Some thread may be still writing
  // as a result nonsense results are sometimes possible
  int n = _captureBufferIndex; // ignore the next scopes to simplify analysis
  // the pointer may have overflown the buffer - read only the valid part
  if (n>_captureBufferSize) n = _captureBufferSize;
  for (int i=0; i<n; i++)
  {
    const CaptureBufferItem &item = _captureBuffer[i];
    PerfCounterSlot *slot = Slot(item._slotIndex);
    slot->value += item._duration >> PROF_ACQ_SCALE_LOG;
    slot->count++;
  }
  // values and counters are valid now, store them to backup values
  Save(fps);

  // decide if deep capture is needed next frame
  if (_captureSingleFrame > 0)
  {
    if (--_captureSingleFrame == 0) // counter reached
    {
      _capture = true;
    }
  }
  else if (_capturePermanent)
  {
    if (_captureFrameScope.GetLength() > 0)
    {
      // check whether the last frame is the one we want to store
      int index = Find(_captureFrameScope);
      if (index >= 0)
      {
        int value = CurrentValue(index);
        if (0.00001f * value >= _captureFrameThreshold)
        {
          // the scope exceeds the limit
          _capturePermanent = false;
        }
      }
    }
    _capture = _capturePermanent;
  }
  __int64 now = ProfileTime();

  if (_captureReady==CaptureTail)
  { // tail open - process it
    // backup the buffer
    _capturedFrame.Copy(_captureBuffer, n);
    // advise the data are ready
    _captureReady = CaptureReady;
  }
  else if (captured && !_capture)
  {
    _captureReady = CaptureTail;
    _capturedFrameStart = _frameStart;
    _capturedFrameEnd = now;
  }
  
  if (_captureReady!=CaptureTail)
  {
    // we need to call RString destructors here, to prevent other threads calling them
    // as we know no thread other than main should be using moreInfo, this is safe
    for (int i=0; i<n; i++)
    {
      _captureBuffer[i]._moreInfo = RString();
    }
    
    // note: not 100 % thread safe. Some thread may be writing at this time
    // as a result nonsense results are sometimes possible
    _captureBufferIndex = 0; // erase the buffer
  }

  _frameCount++;
  _frameStart = now;
  if (_captureReady==DoNotCapture) _capturedFrameStart = _frameStart;

  // if the slowest frame is too old or the current frame is slower, reset it
  _slowFrameAge++;

  // avoid increasing the size during the function
  int slotsCount = _countersFreeSlot;
  saturateMin(slotsCount, _maxSlots);

  /* The following code allows to catch pikes in oPas1 counter... . 
  int j = 0;
  for(;j < _counters.Size(); j++)
  {
    if (strcmp(_counters[j].name,"oPas1") == 0)
      break;
  }
  static float  frameTimeGlobal = 0;
  frameTimeGlobal += frameTime;
  //LogF("frameTimeGlobal %lf", frameTimeGlobal);
  if (frameTimeGlobal > 100 && j != _counters.Size() && _counters[j].slowValue < _counters[j].value)
  */
  if (_slowFrameTime<frameTime || _slowFrameAge>200)

  {
    for( int i=0; i<slotsCount; i++ )
    {
      _counters[i].slowValue=_counters[i].value;
      _counters[i].slowCount=_counters[i].count;
    }
    _slowFrameTime = frameTime;
    _slowFrameAge = 0;
  }
  bool doCongregate = _frameCount>=nCongregate;
  int currentTime = GlobalTickCount();
  for( int i=0; i<slotsCount; i++ )
  {
    PerfCounterSlot &slot = _counters[i];

    slot.Frame(frameTime, _frameCount, doCongregate,currentTime);
    slot.value = 0;
    slot.count = 0;
  }
  if (doCongregate)
  {
    _frameCount = 0;
    _avgDiagnosed = false;
  }
}

int PerfCounters::N() const
{
  return intMin(_countersFreeSlot, _maxSlots);
}

const char *PerfCounters::Name( int i ) const
{
  return _counters[i]._name;
}

void PerfCounters::FrameBegEnd(__int64 &beg, __int64 &end) const
{
  beg = _lastFrameStart;
  end = _frameStart;
}

int PerfCounters::LastValue( int i ) const
{
  if (_counters[i].scale==0) return 0;
  return _counters[i].savedValue/_counters[i].scale;
}

int PerfCounters::LastValueRaw( int i ) const
{
  return _counters[i].savedValue;
}

int PerfCounters::LastCount(int i) const
{
  return _counters[i].savedCount;
}


int PerfCounters::CurrentValue( int i ) const
{
  if (_counters[i].scale==0) return 0;
  return _counters[i].value/_counters[i].scale;
}

int PerfCounters::SlowValue( int i ) const
{
  if (_counters[i].scale==0) return 0;
  return _counters[i].slowValue/_counters[i].scale;
}

int PerfCounters::SlowCount( int i ) const
{
  return _counters[i].slowCount;
}

int PerfCounters::SmoothValue( int i ) const
{
  return _counters[i].smoothValue;
}

int PerfCounters::SumValue(int i) const
{
  return _counters[i].SumValue();
}

float PerfCounters::Correlation(int i) const
{
  #if ENABLE_COREL_ANALIS
  return _counters[i]._regresResult;
  #else
  return 1;
  #endif
}



bool PerfCounters::Show( int i ) const
{
  return !_counters[i].disabled && _counters[i].enabled;
}

bool PerfCounters::WasNonZero( int i, DWORD time ) const
{
  return _counters[i].lastNonZero>time;
}


int PerfCounters::Find(const char *name)
{
  // avoid increasing the size during the function
  int slotsCount = _countersFreeSlot;
  saturateMin(slotsCount, _maxSlots);

  for( int i=0; i<slotsCount; i++ )
  {
    if (!strcmp(_counters[i]._name, name)) return i;
  }
  return -1;
}

PerfCounterSlot::PerfCounterSlot()
{
  strcpy(_name,"");
  strcpy(_nameCategory,"");
  value=0;
  count=0;
  scale=1;
  enabled=true;
  disabled=false;

  _timingEnabled = false;

  congregateValue = 0;
  smoothValue = 0;
  for (int i=0; i<NLastValues; i++)
  {
    lastValues[i] = 0;
    lastCounts[i] = 0;
  }

  slowValue = 0;
  slowCount = 0;
  #if ENABLE_COREL_ANALIS
  _regresResult = 0;
  #endif
}

/**
@param frameTime frame time in seconds - needed for regression analysis
*/

void PerfCounterSlot::Frame(float frameTime, int nCongregate, bool doCongregate, DWORD currentTime)
{
  if (value) lastNonZero = currentTime;

  for (int i=0; i<NLastValues-1; i++)
  {
    lastValues[i] = lastValues[i+1];
    lastCounts[i] = lastCounts[i+1];
  }
  lastValues[NLastValues-1] = value;
  lastCounts[NLastValues-1] = count;

  if (scale!=0)
  {
    congregateValue += value/(float)scale;
  }
  #if ENABLE_COREL_ANALIS
    _regres.Sample(frameTime,value);
  #endif
  if (doCongregate)
  {
    smoothValue = toLargeInt(congregateValue / nCongregate);
    congregateValue = 0;        
    #if ENABLE_COREL_ANALIS
      _regresResult = _regres.Result();
      _regres.Reset();
    #endif
  }
}

int PerfCounterSlot::SumValue() const
{
  int sum = 0;
  for (int i=0; i<NLastValues; i++) sum += lastValues[i];
  return sum;
  //return toLargeInt(sum / (float)(NLastValues * scale));
}

int PerfCounterSlot::SumCount() const
{
  int sum = 0;
  for (int i=0; i<NLastValues; i++) sum += lastCounts[i];
  return sum;
}

int PerfCounterSlot::LastValue(int frame) const
{
  if (frame >= NLastValues) return 0;
  return lastValues[frame];
}

int PerfCounterSlot::LastCount(int frame) const
{
  if (frame >= NLastValues) return 0;
  return lastCounts[frame];
}

void PerfCounterSlot::Reset()
{
  lastNonZero = 0;
  value = 0;
  count = 0;
  smoothValue = 0;
  slowValue = 0;
  slowCount = 0;
  savedValue = 0;
  savedCount = 0;
  congregateValue = 0;
  for (int i=0; i<NLastValues; i++)
  {
    lastValues[i] = 0;
    lastCounts[i] = 0;
  }

  #if ENABLE_COREL_ANALIS
    _regres.Reset();
  #endif
}

int PerfCounters::New(const char *name, const char *nameCategory, int scale)
{
  if( !_constructed )
  {
    return -1;
  }
  int index = Find(name);
  if (index >= 0) return index;

  // new slot need to be created
  index = InterlockedIncrement(&_countersFreeSlot);
  if (index > _maxSlots)
  {
    RptF("No more counters enabled %d/%d, %s ignored", index, _maxSlots, name);
    return -1;
  }
  index--; // first free slot -> last used slot

  PerfCounterSlot &slot = _counters[index];
  slot._name =  name;
  slot._nameCategory = nameCategory;
  #ifdef _XBOX
    // measurement is fast on Xbox - turn everything on by default
    slot._timingEnabled = true;
  #else
    // QueryPerformanceCounter is quite slow on PC - avoid using it too often
    slot._timingEnabled = slot._nameCategory[0] == '*';
  #endif
  slot.scale = scale;
  slot.lastNonZero = 0;
  //LogF("New perf slot %s",slot.name);
  return index;
}

void PerfCounters::Enable(const char *name, const char *nameCategory)
{
  int index = Find(name);
  if( index>=0 ) _counters[index].enabled=true;
}

void PerfCounters::SetScale(const char *name, const char *nameCategory, int scale)
{
  int index=Find(name);
  if( index>=0 ) _counters[index].scale=scale;
}

void PerfCounters::Disable(const char *name, const char *nameCategory)
{
  int index=Find(name);
  if( index>=0 ) _counters[index].disabled=true;
}

void PerfCounters::EnableCategory(const char *nameCategory, bool enable)
{
  // avoid increasing the size during the function
  int slotsCount = _countersFreeSlot;
  saturateMin(slotsCount, _maxSlots);

  for (int i = 0; i < slotsCount; i++)
  {
    // Skip in case the counter doesn't belong to the category
    // empty category means the counter is always enabled
    if (_counters[i]._nameCategory[0]!=0)
    {
      //if (strnicmp(_counters[i]._nameCategory, nameCategory, nameCategoryLength)) continue;
      // any substring match enables the counter
      // this is done to allow for multiple categories, like *sim
      if (!strstr(_counters[i]._nameCategory, nameCategory)) continue;
    }

    // Enable / disable timing
    _counters[i]._timingEnabled = enable;
  }
}

void LogFile::AttachToDebugger()
{
  _attachedToDebugger = true;
}

void LogFile::Open( const char *name )
{
  Close();
  if (!name || !*name) return; // empty name - no logging
#ifdef _XBOX
  BString<256> fullname;
  if (!strchr(name,':'))
  {
    strcpy(fullname,"U:\\");
    strcat(fullname,name);
    name = fullname;
  }
#endif
  _file=fopen(name,"w");
  _counter = 0;
  if( !_file ) return;
  setvbuf(_file,NULL,_IOFBF,32*1024);
}
void LogFile::Close()
{
  if( !_file ) return;
  fclose(_file);
  _file=NULL;
  _attachedToDebugger = false;
}

LogFile::LogFile()
{
  _file=NULL;
  _attachedToDebugger = false;
}

LogFile::LogFile( const char *name )
{
  Open(name);
}

LogFile::~LogFile()
{
  Close();
}


void LogFile::Append( const char *text )
{
  if (_attachedToDebugger)
  {
    OutputDebugStringA(text);
  }
  if( !_file ) return;
  fwrite(text,strlen(text),1,_file);
  if ( !(++_counter & 0xff) )
  fflush(_file);
}

void LogFile::Flush ()
{
  if ( !_file ) return;
  fflush(_file);
  _counter = 0;
}

void LogFile::PrintF( const char *text, ... )
{
  if (!IsOpen()) return;
  BString<1024> buf;
  va_list arglist;
  va_start( arglist, text );
  vsprintf( buf, text, arglist );
  va_end( arglist );
  Append(buf);
}

LogFile &LogFile::operator << ( int i )
{
  if (!IsOpen()) return *this;
  char buf[80];
  sprintf(buf,"%6d",i);
  Append(buf);
  return *this;
}


LogFile &LogFile::operator << ( float f )
{
  if (!IsOpen()) return *this;
  char buf[80];
  sprintf(buf,"%6.2f",f);
  Append(buf);
  return *this;
}

LogFile &LogFile::operator << ( const char *txt )
{
  Append(txt);
  return *this;
}

#if (!defined(_XBOX) || _XBOX_VER>=2 ) && _ENABLE_PERFLOG
static int ProfileScaleCalc()
{
  // TODOX360: ProfileScaleCalc may always return 1
  LARGE_INTEGER frequency;
  if (::QueryPerformanceFrequency(&frequency)==FALSE || frequency.QuadPart==0)
  {
    return 0;
  }
  __int64 freq = frequency.QuadPart;
  __int64 scale = freq/100000/(1<<PROF_ACQ_SCALE_LOG);
  #if 0
    // check what time can we represent with given ACQ_SCALE_LOG
    __int64 acqFrequency = freq/(1<<PROF_ACQ_SCALE_LOG);
    // max. time we are able to represent safely is 31b
    float time = float(1U<<31)/acqFrequency;
    LogF("Max PROFILE_SCOPE time %.2f ms",time*1000);
  #endif
  // make sure we can represent 1 sec
  return int(scale);
}

/// profiler counter scale - should be calculated only once
/**
If any static variable initialization in init_seg(compiler)
needs to use profiling, it needs to make sure InitProfileScale is called first.
*/

static int ProfileScaleVal = ProfileScaleCalc() INIT_PRIORITY_URGENT;

#ifndef _XBOX
__int64 ProfileTime()
{
  // on some CPUs using rdtsc may be much faster than using QPC
  // how to check?

  // TODOX360: we might use __mftb or __mftb32 instead of QueryPerformanceCounter
  /** docs seem to be somewhat contradictory, see: 
  QueryPerformanceFrequency: For the Xbox 360, QueryPerformanceFrequency returns 50 MHz
  QueryPerformanceFrequency: ... the QueryPerformanceCounter function equates to the mftb instruction. ...
  mftb erratum: A four-cycle lag between update .. can result in a ... time base register that is incorrect. 
  __mftb32 should wrap about each 40 seconds, which should be enough
  */

  LARGE_INTEGER count;
  ::QueryPerformanceCounter(&count);
  return count.QuadPart;
}  
#endif

void InitProfileScale()
{
  if (ProfileScaleVal!=0) return;
  ProfileScaleVal = ProfileScaleCalc();
}

int ProfileScale()
{
  return ProfileScaleVal;
}

DWORD ScopeProfiler::_mainThread = GetCurrentThreadId();

#endif

#endif
