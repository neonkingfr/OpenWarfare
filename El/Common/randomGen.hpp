#ifndef _RANDOMGEN_HPP
#define _RANDOMGEN_HPP

class RandomGenerator
{
	mutable int _seed;
	
	public:
	RandomGenerator(int seed1, int seed2);
	RandomGenerator();
	/// random value from a sequence
	float RandomValue() const;
	
	/// set sequence start
	void SetSeed( int seed ) {_seed=seed;}
	
	/// random value based on explicit seed
	float RandomValue(int seed) const;
	/// random value based on explicit 2D seed
	float RandomValue(int x, int z) const {return RandomValue(GetSeed(x,z));}
	/// random value based on explicit 3D seed
	float RandomValue(int x, int z, int y) const {return RandomValue(GetSeed(x,z,y));}
	
	float Gauss(float min, float mid, float max) const;
	float PlusMinus(float a, float b) const;

 	int GetSeed(int x, int z) const;
 	int GetSeed(int x, int z, int y) const;
};

extern RandomGenerator GRandGen;

#endif
