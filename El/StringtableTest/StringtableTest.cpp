// StringtableTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/Stringtable/stringtable.hpp>

int main(int argc, char* argv[])
{
	GLanguage = "English";
	LoadStringtable("global", "stringtable.csv");
	RString result1 = LocalizeString("STR_XXX");
	int IDS_YYY = RegisterString("STR_YYY");
	RString result2 = LocalizeString(IDS_YYY);
	return 0;
}
