// EnumTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/Enum/enumNames.hpp>
#include <El/Enum/dynEnum.hpp>
#include <El/Enum/idString.hpp>

// enum names declaration and definition

#define TEST_ENUM(type,prefix,XX) \
	XX(type, prefix, Value1) \
	XX(type, prefix, Value2) \
	XX(type, prefix, Value3) \
	XX(type, prefix, Value4) \
	XX(type, prefix, Value5)

DECLARE_DEFINE_ENUM(TestEnum,Test,TEST_ENUM)

// idStrings definitions

static RStringB TestStrings[]=
{
	// weapons and magazines
	"M16",
	"AK74",
	"HandGrenade",
	"M60",
	"M4",
	"Put",
	"Throw",
	"NVGoggles",
	"CarHorn",
	"TruckHorn",

	"AK47",
	"KozliceBall",
	"KozliceShell",
	"M21",
	"GrenadeLauncher",
	"AK47CZ",
	"Kozlice",
	"AK47GrenadeLauncher"
};

static IdStringTable TestIdStrings(TestStrings,sizeof(TestStrings)/sizeof(*TestStrings));

int main(int argc, char* argv[])
{
	// enum names test
	RString name = FindEnumName(TestValue3);
	TestEnum value = GetEnumValue<TestEnum>("Value4");

	// dynamic enum test
	DynEnum state;
	int state1 = state.AddValue("State1");
	int state2 = state.AddValue("State2");
	int state3 = state.AddValue("State3");
	int state4 = state.AddValue("State4");
	int state5 = state.AddValue("State5");

	int s2 = state.GetValue("State2");
	int s3 = state.GetValue("state3");
	RString n = state.GetName(state1);

	// idString test
	int id = TestIdStrings.GetId("M60");
	RString str = TestIdStrings.GetString(id);

	return 0;
}
