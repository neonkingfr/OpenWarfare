#ifdef _MSC_VER
#pragma once
#endif

/**
 * @file   obb.hpp
 * @brief  Abstract OBB hierarchy.
 *
 * Copyright &copy; 2004 by BIStudio (www.bistudio.com)
 * @author PE
 * @since  8.6.2004
 * @date   17.7.2004
 */

#ifndef _OBB_HPP
#define _OBB_HPP

//#include "boundingbox.hpp"

#include "El/elementpch.hpp"
#include "El/Math/math3d.hpp"
#include <El/Color/colors.hpp>
#include <Es/Algorithms/qsort.hpp>
class Frame;

#define PI 3.14159265f

/* 12.4.2006 Fehy I removed an obsolete code (class OBBNode). If you need this code get it from SS */

//---------------------------
// OBBNodeS
//---------------------------


/// special implementation of OBB tree used in ConvexComponnet groups
// Currently only OBBNodeS is used. It is not inherited from BBNodeS, because I want to avoid all virtual functions. (to get max speed).
// Later if there will be used several types of bounding volumes take BBNodeS like a base class so use version befor 9.5.2006
//class OBBNodeS : public BBNodeS
class ConvexComponent;
class Shape;

class OBBNodeS :  public RefCount, public CountInstances<OBBNodeS> 
{  
protected:

  //-----------------------------------------------------------------------
  //  Core data:
  //  Core data:
  Ref<ConvexComponent> _cc; 

  /// Center of the box.
  Vector3 _c;

  /// [Outer] radius (< 0.0 if invalid).
  Coord _r;   

  /// Three unit vectors of the box segments.
  Vector3 _u, _v, _w;

  /// Half-sizes of the box.
  Coord _hu, _hv, _hw; 

  /// Center of Obb
  Vector3 _obbC;

  /**
  * Accuracy level (balance between intersection accuracy and speed).
  * Should be set to 0 for exact intersection computation.
  * values:  0 .. exact computation (slowest),
  *         10 .. fastest computation (pessimistic)
  */
  int _accuracyLevel;

  /// Linked-list of tree-brothers.
  Ref<OBBNodeS> _brother;

  /// Link to the 1st son (NULL for leaf OBBs).
  Ref<OBBNodeS> _son;

  /**
  * Sets OBB from already defined orientation vectors and set of (hull) points.
  *  @param  v Set of (hull) points.
  *  @param  d Array of three orientation vectors 
  */
  void SetFromVectors ( const AutoArray<Vector3> &v, Vector3 *d);

public:

  OBBNodeS(ConvexComponent * cc);

  OBBNodeS(ConvexComponent * cc, const  Shape * shape);
  OBBNodeS(Ref<OBBNodeS> node1, Ref<OBBNodeS> node2,const Shape * shape,float accuracy = 0);
  OBBNodeS(AutoArray<Vector3>& v) {SetFromVertices(v);};

  const Vector3 &GetCenter () const { return _c;};

  Coord GetOutRadius () const { return _r;};

  /// True if node has son
  bool HasSon() const { return _son.NotNull(); };

  OBBNodeS * Son() const {return _son;};
  OBBNodeS * Brother() const {return _brother;};

  /// Actual accuracy level: 0 .. exact (slowest), 10 .. pessimistic (fastest)
  int GetAccuracyLevel () const
  { return _accuracyLevel; }

  /// Sets actual accuracy level: 0 .. exact (slowest), 10 .. pessimistic (fastest)
  void SetAccuracyLevel ( int accuracyLevel )
  { _accuracyLevel = accuracyLevel; }

  ConvexComponent *GetComponnent () const
  { return _cc; }

  Coord GetVolume() const
  { return _hu * _hv * _hw * 8;};    

  void GetVertices(AutoArray<Vector3>& v,const Shape * shape) const;

  /// Sets BB from the given vertices.
  void SetFromVertices ( const AutoArray<Vector3> &v);  

  /// Sets BB shape (and tree pointers) from two ancestors.
  void SetFromTwoNodes ( OBBNodeS * node1, OBBNodeS * node2,const Shape * shape );
    
  /**
  * Does the given ray intersect the BB?
  * Ray from P0 to P1
  */
  bool Ray ( Vector3Par P0, Vector3Par P1 );
  /**
  * Test this subtree (deep) against the given ray.
  * Ray from P0 to P1
  * 'who' is appended with interfering leaf-nodes.
  */
  bool RayDeep (Vector3Par P0, Vector3Par P1, RefArray<OBBNodeS> &who);


  /**
  * Test only this BB (shallow) against the other one (shallow).
  */ 
  bool MeVsYou ( OBBNodeS * you, Matrix4Par transMeToYou ) const; 

  /**
  * Test only this BB (shallow) against the other one (shallow).
  */ 
  bool MeVsYou ( OBBNodeS * you, Matrix4Par transMeToYou, float epsilon ) const; 


  /**
  * Test only this BB (shallow) against the other one (shallow).
  */
  bool MeStrechedVsYou ( Vector3Val offsetMe, OBBNodeS * you, Matrix4Par transMeToYou );

  /**
  * Test this subtree (deep) against the other subtree (deep).
  * 'who' is appended with interfering leaf-node tuples.
  */
  bool DeepVsDeep ( OBBNodeS * root, Matrix4Par transMeToYou, AutoArray<const OBBNodeS*, MemAllocSA> &who, float epsilon ) const; 


  /**
  * Test this subtree (deep) against the other subtree (deep).
  * 'who' is appended with interfering leaf-node tuples.
  */
  bool DeepVsDeep ( OBBNodeS * root, Matrix4Par transMeToYou, AutoArray<const OBBNodeS*, MemAllocSA> &who ) const; 


  /**
  * Test this subtree (deep) against the other subtree (deep). 
  * This subtree is localized anywhere between current position and position offseted by offset
  * 'who' is appended with interfering leaf-node tuples.
  */
  bool DeepStrechedVsDeep (Vector3Val offsetMe, OBBNodeS * root, Matrix4Par transMeToYou, RefArray<OBBNodeS> &who ); 


#if _ENABLE_CHEATS
  void DrawDiag(Color color1,Color color2, float t, float step, const Frame &pos) const;
#endif
};
TypeIsSimple(OBBNodeS *);
TypeIsSimple(const OBBNodeS *);

#endif
