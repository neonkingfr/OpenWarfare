/**
 * @file   boundingbox.hpp
 * @brief  Abstract bounding-box hierarchy.
 *
 * Copyright &copy; 2004 by BIStudio (www.bistudio.com)
 * @author PE
 * @since  9.6.2004
 * @date   17.7.2004
 */


#include "boundingbox.hpp"
#include "obb.hpp"
#include <Es/Containers/array2D.hpp>
#include <Es/Containers/bitmask.hpp>
#include <El/Common/perfProf.hpp>
#include <Poseidon/lib/Shape/shape.hpp>


/* 12.4.2006 Fehy I removed an obsolete code (class BBNode. class BBTreeBuilder). If you need this code get it from SS */

//--------------------------------
// BBNodeS
//--------------------------------

BBNodeS::BBNodeS(ConvexComponent * cc) : _cc(cc), _accuracyLevel(0)
{
  _c = cc->GetCenter();
  _r = cc->GetRadius();
}

BBNodeS::BBNodeS(ConvexComponent * cc, const Shape * shape) : _cc(cc), _accuracyLevel(0)
{  
  AutoArray<Vector3> v;
  GetVertices(v, shape);
  SetFromVertices(v);
}

BBNodeS::BBNodeS(Ref<BBNodeS> node1, Ref<BBNodeS> node2,const Shape * shape) : _accuracyLevel(0)
{
  SetFromTwoNodes(node1, node2, shape);
}

void BBNodeS::GetVertices(AutoArray<Vector3>& v,const Shape * shape) const
{
  if (_cc)
  {
    v.Reserve(v.Size() + _cc->Size());
    for(int i = 0; i < _cc->Size(); i++)
    {
      v.AddFast(shape->Pos((*_cc)[i]));
    }
  }
 
  BBNodeS * mySons = _son;
  while ( mySons )
  {
    mySons->GetVertices(v,shape);
    mySons = mySons->_brother;
  };
}

Coord BBNodeS::GetVolume() const
{
  return 4 * PI / 3 * _r * _r * _r;
};

/// Sets BB from the given vertices.
void BBNodeS::SetFromVertices ( const AutoArray<Vector3> &v)
{
  Assert(v.Size() > 0);
  _c = v[0];

  for(int i = 1; i < v.Size(); i++)
  {
    _c += v[i];
  }
  _c /= v.Size();

  _r = 0; 

  for(int i = 0; i < v.Size(); i++)
  {
    float d = _c.Distance2(v[i]);
    saturateMax(_r,d);
  }

  _r = sqrt(_r);
}

/// Sets BB shape (and tree pointers) from two ancestors.
void BBNodeS::SetFromTwoNodes ( BBNodeS * node1, BBNodeS * node2,const Shape * shape )
{
  AutoArray<Vector3> v; 
  node1->GetVertices(v, shape);
  if (node2)
    node2->GetVertices(v, shape);
  SetFromVertices(v);

  Assert(!_son);
  _son = node1;
  node1->_brother = node2;
}

bool BBNodeS::Ray ( Vector3Par P0, Vector3Par P1 )
{
  // geometric solution a la Glassner:
  Vector3 V = P1 - P0;
  float t = V.DotProduct( _c - P0 );
  Vector3 T = P0 + t * V / V.SquareSize();
  float r2 = _r * _r;
  if ( _c.Distance2(T) >= r2)
     return false;

  // maybe some point is closer... (if t is not between P0 and P1
  if (t < 0)  
    return _c.Distance2(P0) < r2;  
  else if (t > V.SquareSize())
    return _c.Distance2(P1) < r2;
  else
    return true; // is between points
}

/**
* Test this subtree (deep) against the given ray.
* Ray from P0 to P1
* 'who' is appended with interfering leaf-nodes.
*/
bool BBNodeS::RayDeep(Vector3Par P0, Vector3Par P1, RefArray<BBNodeS> &who)
{
  if ( !Ray( P0, P1) ) return false;

  if ( _son.IsNull())
  {                                         
    who.Add(this);    
    return true;
  }

  bool was = false;

  BBNodeS * mySons = _son;
  do
  {
    was = mySons->RayDeep( P0, P1, who) || was;
    mySons = mySons->_brother;
  } while ( mySons );

  return was;
}

/**
* Test this subtree (deep) against the other subtree (deep).
* 'who' is appended with interfering leaf-node tuples.
*/
bool BBNodeS::DeepVsDeep ( BBNodeS * root, Matrix4Par transMeToYou, AutoArray<const BBNodeS*, MemAllocSA> &who ) const
{
  if ( !MeVsYou(root, transMeToYou) ) return false;

  if ( _son.IsNull() && root->_son.IsNull() )
  {                                         // two leaf nodes
    who.Add(this);
    who.Add(root);
    return true;
  }

  bool was = false;

  if ( root->_son.IsNull()/* ||
                          m_son.NotNull() && m_r > root->m_r */) // divide myself
  {
    BBNodeS * mySons = _son;
    do
    {
      was = mySons->DeepVsDeep(root,transMeToYou,who) || was;
      mySons = mySons->_brother;
    } while ( mySons );
  }

  else                                      // divide the other node
  {
    BBNodeS * rootSons = root->_son;
    do
    {
      was = DeepVsDeep(rootSons,transMeToYou,who) || was;
      rootSons = rootSons->_brother;
    } while ( rootSons );
  }

  return was;
}

/**
* Test this subtree (deep) against the other subtree (deep).
* 'who' is appended with interfering leaf-node tuples.
*/
bool BBNodeS::DeepVsDeep ( BBNodeS * root, Matrix4Par transMeToYou, AutoArray<const BBNodeS*, MemAllocSA> &who, float epsilon ) const
{
  if ( !MeVsYou(root, transMeToYou, epsilon) ) return false;

  if ( _son.IsNull() && root->_son.IsNull() )
  {                                         // two leaf nodes
    who.Add(this);
    who.Add(root);
    return true;
  }

  bool was = false;

  if ( root->_son.IsNull() ||
                          (_son.NotNull() && _r > root->_r)) // divide myself
  {
    BBNodeS * mySons = _son;
    do
    {
      was = mySons->DeepVsDeep(root,transMeToYou,who, epsilon) || was;
      mySons = mySons->_brother;
    } while ( mySons );
  }

  else                                      // divide the other node
  {
    BBNodeS * rootSons = root->_son;
    do
    {
      was = DeepVsDeep(rootSons,transMeToYou,who, epsilon) || was;
      rootSons = rootSons->_brother;
    } while ( rootSons );
  }

  return was;
}

/**
* Test this subtree (deep) against the other subtree (deep). 
* This subtree is localized anywhere between current position and position moved by offset
* 'who' is appended with interfering leaf-node tuples.
*/
bool BBNodeS::DeepStrechedVsDeep (Vector3Val offsetMe, BBNodeS * root, Matrix4Par transMeToYou, RefArray<BBNodeS> &who )
{
  if ( !MeStrechedVsYou(offsetMe,root, transMeToYou) ) return false;

  if ( _son.IsNull() && root->_son.IsNull() )
  {                                         // two leaf nodes
    who.Add(this);
    who.Add(root);
    return true;
  }

  bool was = false;

  if ( root->_son.IsNull()/* ||
                          m_son.NotNull() && m_r > root->m_r */) // divide myself
  {
    BBNodeS * mySons = _son;
    do
    {
      was = mySons->DeepStrechedVsDeep(offsetMe,root,transMeToYou,who) || was;
      mySons = mySons->_brother;
    } while ( mySons );
  }

  else                                      // divide the other node
  {
    BBNodeS * rootSons = root->_son;
    do
    {
      was = DeepStrechedVsDeep(offsetMe,rootSons,transMeToYou,who) || was;
      rootSons = rootSons->_brother;
    } while ( rootSons );
  }

  return was;
}

/**
* Test only this BB (shallow) against the other one (shallow).
*/
bool BBNodeS::MeVsYou ( BBNodeS * you, Matrix4Par transMeToYou ) const
{
  float dist = you->_c.Distance2(transMeToYou * _c);
  float rMax = you->_r + _r;
  rMax *= rMax;

  return dist <= rMax;
}

/**
* Test only this BB (shallow) against the other one (shallow).
*/
bool BBNodeS::MeVsYou ( BBNodeS * you, Matrix4Par transMeToYou, float epsilon ) const
{
  float dist = you->_c.Distance2(transMeToYou * _c);
  float rMax = you->_r + _r + epsilon;
  rMax *= rMax;

  return dist <= rMax;
}


/**
* Test only this BB (shallow) against the other one (shallow).
*/
bool BBNodeS::MeStrechedVsYou ( Vector3Val offsetMe, BBNodeS * you, Matrix4Par transMeToYou )
{
  // geometric solution a la Glassner:
  Vector3 V = transMeToYou.Rotate(offsetMe);
  Vector3 P0 = transMeToYou * _c;
  float t = V.DotProduct( you->_c - P0 );
  Vector3 T = P0 + t * V / V.SquareSize();
  float r2 = _r  + you->_r;
  r2 *= r2;
  if ( you->_c.Distance2(T) >= r2)
    return false;

  // maybe some point is closer... (if t is not between P0 and P1
  if (t < 0)  
    return you->_c.Distance2(P0) < r2;  
  else if (t > V.SquareSize())
    return you->_c.Distance2(P0 + V) < r2;
  else
    return true; // is between points
}




