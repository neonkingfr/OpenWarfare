#ifdef _MSC_VER
#pragma once
#endif

/**
 * @file   boundingbox.hpp
 * @brief  Abstract bounding-box hierarchy.
 *
 * Copyright &copy; 2004 by BIStudio (www.bistudio.com)
 * @author PE
 * @since  9.6.2004
 * @date   17.7.2004
 */

#ifndef _BOUNDINGBOX_HPP
#define _BOUNDINGBOX_HPP

#include "El/elementpch.hpp"
#include "El/Math/math3d.hpp"
#include <El/Color/colors.hpp>
#include <Es/Algorithms/qsort.hpp>


//#define COORD_EPS  (1e-30f)
//#define AbsZero(x) (x == 0)
//#define AbsZero(x) (x < COORD_EPS)



#define BB_SPHERE   1
#define BB_OBB      2


class Color;
class Frame;

/* 12.4.2006 Fehy I removed an obsolete code (class BBNode. class BBTreeBuilder). If you need this code get it from SS */

//-----------------------------------------
// BBNodeS
//-----------------------------------------

class ConvexComponent;
class Shape;

/**
* Bounding-box hierarchy node (default - bounding sphere). Used in Convex Component Group
*/
class BBNodeS : public RefCount, public CountInstances<BBNodeS> 
{
protected:  
  //-----------------------------------------------------------------------
  //  Core data:
  Ref<ConvexComponent> _cc; 

  /// Center of the box.
  Vector3 _c;
  
  /// [Outer] radius (< 0.0 if invalid).
  Coord _r;    

  /**
  * Accuracy level (balance between intersection accuracy and speed).
  * Should be set to 0 for exact intersection computation.
  * values:  0 .. exact computation (slowest),
  *         10 .. fastest computation (pessimistic)
  */
  int _accuracyLevel;

  /// Linked-list of tree-brothers.
  Ref<BBNodeS> _brother;

  /// Link to the 1st son (NULL for leaf OBBs).
  Ref<BBNodeS> _son;

  //-----------------------------------------------------------------------
  //  Helpers:

  /// [Re-]sets all helper variables.
  virtual void setHelpers () {};

public:
  BBNodeS(ConvexComponent * cc);
  BBNodeS(ConvexComponent * cc, const Shape * shape);
  BBNodeS(Ref<BBNodeS> node1, Ref<BBNodeS> node2,const Shape * shape);  
  BBNodeS(AutoArray<Vector3>& v) : _accuracyLevel(0) {SetFromVertices(v);};  
  BBNodeS() : _accuracyLevel(0) {};

  
  const Vector3 &GetCenter () const { return _c;};
  Coord GetOutRadius () const { return _r;};

  virtual Coord GetVolume() const;

  /// True if node has son
  bool HasSon() const { return _son.NotNull(); };

  BBNodeS * Son() const {return _son;};
  BBNodeS * Brother() const {return _brother;};


  /// Actual accuracy level: 0 .. exact (slowest), 10 .. pessimistic (fastest)
  int GetAccuracyLevel () const
  { return _accuracyLevel; }

  /// Sets actual accuracy level: 0 .. exact (slowest), 10 .. pessimistic (fastest)
  void SetAccuracyLevel ( int accuracyLevel )
  { _accuracyLevel = accuracyLevel; }

  ConvexComponent *GetComponnent () const
  { return _cc; }

  void GetVertices(AutoArray<Vector3>& v,const Shape * shape) const;

  /// Sets BB from the given vertices.
  virtual void SetFromVertices ( const AutoArray<Vector3> &v);  

  /// Sets BB shape (and tree pointers) from two ancestors.
  virtual void SetFromTwoNodes ( BBNodeS * node1, BBNodeS * node2, const Shape * shape);   

  /**
  * Does the given ray intersect the BB?
  * Ray from P0 to P1
  */
  virtual bool Ray ( Vector3Par P0, Vector3Par P1 );

  /**
  * Test this subtree (deep) against the given ray.
  * Ray from P0 to P1
  * 'who' is appended with interfering leaf-nodes.
  */
  bool RayDeep (Vector3Par P0, Vector3Par P1, RefArray<BBNodeS> &who);

  /**
  * Test this subtree (deep) against the other subtree (deep).
  * 'who' is appended with interfering leaf-node tuples.
  */
  bool DeepVsDeep ( BBNodeS * root, Matrix4Par transMeToYou, AutoArray<const BBNodeS*, MemAllocSA> &who ) const; 

  /**
  * Test this subtree (deep) against the other subtree (deep).
  * 'who' is appended with interfering leaf-node tuples.
  */
  bool DeepVsDeep ( BBNodeS * root, Matrix4Par transMeToYou, AutoArray<const BBNodeS*, MemAllocSA> &who, float epsilon ) const; 

  /**
  * Test this subtree (deep) against the other subtree (deep). 
  * This subtree is localized anywhere between current position and position offseted by offset
  * 'who' is appended with interfering leaf-node tuples.
  */
  bool DeepStrechedVsDeep (Vector3Val offsetMe, BBNodeS * root, Matrix4Par transMeToYou, RefArray<BBNodeS> &who );  
 
  /**
  * Test only this BB (shallow) against the other one (shallow).
  */
  virtual bool MeVsYou ( BBNodeS * you, Matrix4Par transMeToYou ) const;

  /**
  * Test only this BB (shallow) against the other one (shallow).
  */
  virtual bool MeVsYou ( BBNodeS * you, Matrix4Par transMeToYou, float epsilon ) const;

  /**
  * Test only this BB (shallow) against the other one (shallow).
  */
  bool MeStrechedVsYou ( Vector3Val offsetMe, BBNodeS * you, Matrix4Par transMeToYou );
  

  /**
  * Appends new node to the tree.
  */
  //virtual void Append ( Ref<BBNode> you);  
#if _ENABLE_CHEATS
  virtual void DrawDiag(Color color1,Color color2, float t, float step, const Frame &pos) const;  
#endif
};

TypeIsSimple(BBNodeS *);
TypeIsSimple(const BBNodeS *);


//-----------------------------------------
// BBTreeBuilderS
//-----------------------------------------

/**
* Bounding-box tree builder.
*  Builds BB-tree from convex components (ShapeReference referenced).
*  Default implementation: uses OBB-tree nodes, builds binary tree.
*/
template<class BBNodeType>
class BBTreeBuilderS
{
protected:

  /// Container of BB-tree nodes (temporary roots).   
  static Ref<BBNodeType> CreateBBNode ( RefArray<BBNodeType> nodes, const Shape * shape);
  static  float NodeVolume( RefArray<BBNodeType> nodes, const Shape * shape);
public:
  /**
  *  Finishes the building process, returns the BB-tree.
  *    The created tree (root) remains in m_nodes[0]
  *   (and can be part of the next building process).
  */
  static Ref<BBNodeType> BuildTree (const Shape * shape, const RefArray<ConvexComponent>& ccs);
};

template<class BBNodeType>
Ref<BBNodeType> BBTreeBuilderS<BBNodeType>::BuildTree(const Shape * shape, const RefArray<ConvexComponent>& ccs )
{
  RefArray<BBNodeType> nodes;
  nodes.Realloc(ccs.Size());

  for(int i = 0; i < ccs.Size(); i++)
  {
    nodes.AddFast(new BBNodeType(ccs[i], shape));
  }

  if (nodes.Size() == 0)
    return NULL;

  return CreateBBNode(nodes,shape);
}



template<class BBNodeType>
float BBTreeBuilderS<BBNodeType>::NodeVolume( RefArray<BBNodeType> nodes, const Shape * shape)
{
  if (nodes.Size() == 0)
    return 0;

  if (nodes.Size() == 1)
    return nodes[0]->GetVolume();

  AutoArray<Vector3> v;

  
  for(int i = 0; i < nodes.Size(); i++)
  {
    nodes[i]->GetVertices(v, shape);    
  }

  BBNodeType bb(v);

  return bb.GetVolume();
}

template<class BBNodeType, int i>
class CmpBy
{
public:
  int operator()( const Ref<BBNodeType>*s1, const  Ref<BBNodeType> *s2 ) const
  {
    return ((*s2)->GetCenter()[i]-(*s1)->GetCenter()[i]) > 0 ? 1: -1;
  }
};

template<class BBNodeType>
Ref<BBNodeType> BBTreeBuilderS<BBNodeType>::CreateBBNode ( RefArray<BBNodeType> nodes, const Shape * shape)
{
  if (nodes.Size() == 0)
    return NULL;

  if (nodes.Size() == 1)
    return nodes[0];

  if (nodes.Size() == 2)
    return new BBNodeType(nodes[0].GetRef(), nodes[1].GetRef(),shape,5);

  // try several separations by plane and choose one with smallest result volume
  CmpBy<BBNodeType,0> f0;
  QSort<Ref<BBNodeType> , CmpBy<BBNodeType,0> > (nodes.Data(), nodes.Size(), f0);

  RefArray<BBNodeType> leftSide0; 
  RefArray<BBNodeType> rightSide0;

  for(int i = 0; i < nodes.Size()/2; i++)
  {   
    leftSide0.Add(nodes[i]);
  }

  for(int i = nodes.Size()/2; i < nodes.Size(); i++)
  {   
    rightSide0.Add(nodes[i]);
  }

  float volume0 = NodeVolume(rightSide0, shape) + NodeVolume(leftSide0, shape);
  CmpBy<BBNodeType,1>  f1;
  QSort<Ref<BBNodeType> , CmpBy<BBNodeType,1> > (nodes.Data(), nodes.Size(), f1);

  RefArray<BBNodeType> leftSide1; 
  RefArray<BBNodeType> rightSide1;

  for(int i = 0; i < nodes.Size()/2; i++)
  {   
    leftSide1.Add(nodes[i]);
  }

  for(int i = nodes.Size()/2; i < nodes.Size(); i++)
  {   
    rightSide1.Add(nodes[i]);
  }  

  float volume1 = NodeVolume(rightSide1, shape) + NodeVolume(leftSide1, shape);

  CmpBy<BBNodeType,2> f2;
  QSort<Ref<BBNodeType> , CmpBy<BBNodeType,2> > (nodes.Data(), nodes.Size(), f2);

  RefArray<BBNodeType> leftSide2; 
  RefArray<BBNodeType> rightSide2;

  for(int i = 0; i < nodes.Size()/2; i++)
  {   
    leftSide2.Add(nodes[i]);
  }

  for(int i = nodes.Size()/2; i < nodes.Size(); i++)
  {   
    rightSide2.Add(nodes[i]);
  }

  float volume2 = NodeVolume(rightSide2, shape) + NodeVolume(leftSide2, shape);

  if (volume0 < volume1 && volume0 < volume2)
  {
    Ref<BBNodeType> nodeRight0 = CreateBBNode(rightSide0,shape);  
    Ref<BBNodeType> nodeLef0 = CreateBBNode(leftSide0,shape);

    return  new BBNodeType(nodeRight0, nodeLef0, shape,5);
  }

  if (volume2 < volume1)
  {
    Ref<BBNodeType> nodeRight2 = CreateBBNode(rightSide2,shape);  
    Ref<BBNodeType> nodeLef2 = CreateBBNode(leftSide2,shape);

    return  new BBNodeType(nodeRight2, nodeLef2,shape,5);
  }

  Ref<BBNodeType> nodeRight1 = CreateBBNode(rightSide1,shape);  
  Ref<BBNodeType> nodeLef1 = CreateBBNode(leftSide1,shape);

  return  new BBNodeType(nodeRight1, nodeLef1,shape,5);
}




#endif
