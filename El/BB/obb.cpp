/**
 * @file   obb.hpp
 * @brief  Abstract OBB hierarchy.
 *
 * Copyright &copy; 2004 by BIStudio (www.bistudio.com)
 * @author PE
 * @since  8.6.2004
 * @date   17.7.2004
 */

#include "obb.hpp"
//#ifdef USE_BB
#include <Poseidon/lib/global.hpp>
#include <Es/Containers/array2D.hpp>
#include <Es/Containers/bitmask.hpp>
#include <El/Common/perfProf.hpp>
#include <Poseidon/lib/Shape/shape.hpp>


/* 12.4.2006 Fehy I removed an obsolete code (class OBBNode). If you need this code get it from SS */

/// condition for the most coarse intersection test (lev1 and lev2 are accuracy levels of the tested BBs).
#define BB_TEST_MOST_COARSE(lev1,lev2) ( (lev1) > 7 || (lev2) > 7 )

/// condition for coarse intersection test (lev1 and lev2 are accuracy levels of the tested BBs).
#define BB_TEST_COARSE(lev1,lev2)      ( (lev1) > 4 || (lev2) > 4 )

/// condition for the most accurate intersection test (lev1 and lev2 are accuracy levels of the tested BBs).
#define BB_TEST_ACCURATE(lev1,lev2)    ( (lev1) <= 4 && (lev2) <= 4 )


//---------------------------
// OBBNodeS
//---------------------------

OBBNodeS::OBBNodeS(ConvexComponent * cc) :_cc(cc), _accuracyLevel(0)
{
  _c = cc->GetCenter();
  _r = cc->GetRadius();

  _u = Vector3(1,0,0);
  _v = Vector3(0,1,0);
  _w = Vector3(0,0,1);

  Vector3 h = (cc->Max() - cc->Min()) * 0.5f;
  _hu = h[0];
  _hv = h[1];
  _hw = h[2];

  _obbC = (cc->Max() + cc->Min()) * 0.5f;
};

OBBNodeS::OBBNodeS(ConvexComponent * cc,const  Shape * shape) :_cc(cc), _accuracyLevel(0)
{  
  AutoArray<Vector3> v;
  GetVertices(v, shape);
  SetFromVertices(v);
};

OBBNodeS::OBBNodeS(Ref<OBBNodeS> node1, Ref<OBBNodeS> node2,const Shape * shape, float accuracy) :  _accuracyLevel(0) 
{
   SetFromTwoNodes(node1, node2, shape);
   _accuracyLevel = accuracy;
};

void OBBNodeS::GetVertices(AutoArray<Vector3>& v,const Shape * shape) const
{
  if (_cc)
  {
    v.Reserve(v.Size() + _cc->Size());
    for(int i = 0; i < _cc->Size(); i++)
    {
      v.AddFast(shape->Pos((*_cc)[i]));
    }
  }

  OBBNodeS * mySons = _son;
  while ( mySons )
  {
    mySons->GetVertices(v,shape);
    mySons = mySons->_brother;
  };
}

void computeMinMax ( Vector3Par vec, const AutoArray<Vector3> &v,
                             float &min, float &max)
{
  Vector3 act;
 
  act = v[0];
  min = max = act.DotProduct(vec);
  int i;
  int len = v.Size();
  Coord d;
  for ( i = 1; i < len; i++ )
  {   
    act = v[i];
    d = act.DotProduct(vec);
    if ( d < min ) min = d;
    if ( d > max ) max = d;
  }
}


void OBBNodeS::SetFromVectors ( const AutoArray<Vector3> &v,
                              Vector3 *d)
{ 
  _u = d[0];
  _v = d[1];
  _w = d[2];
  Coord mu, mv, mw;
  Coord min, max;
  computeMinMax(_u,v,min,max);
  _hu = 0.5f * (max - min);
  mu   = 0.5f * (max + min);
  computeMinMax(_v,v,min,max);
  _hv = 0.5f * (max - min);
  mv   = 0.5f * (max + min);
  computeMinMax(_w,v,min,max);
  _hw = 0.5f * (max - min);
  mw   = 0.5f * (max + min);
 
  _obbC = mu * _u + mv * _v + mw * _w;
    
}

/// Sets BB from the given vertices.
void OBBNodeS::SetFromVertices ( const AutoArray<Vector3> &v)
{
  // find bounding sphere
  Assert(v.Size() > 0);
  _c = v[0];

  for(int i = 1; i < v.Size(); i++)
  {
    _c += v[i];
  }
  _c /= v.Size();
  _r = 0; 
  for(int i = 0; i < v.Size(); i++)
  {
    float d = _c.Distance2(v[i]);
    saturateMax(_r,d);
  }
  _r = sqrt(_r);

  // check OBB and AABB and choose better option
  Matrix3 cov;
  cov.SetCovarianceCenter(v);

  Matrix3 eigV;
  eigV.SetEigenStandard(cov,NULL);

  Vector3 d[3] =
  {
    eigV.DirectionAside().Normalized(),
    eigV.DirectionUp().Normalized(),
    eigV.Direction().Normalized()
  };

  SetFromVectors(v,d);
  float volumeOBB = GetVolume();

  Vector3 da[3] = {VAside, VUp, VForward};    
  SetFromVectors(v,da);
  if (volumeOBB < GetVolume())
  {
    SetFromVectors(v,d); // better option is OBB
  }  
}

/// Sets BB shape (and tree pointers) from two ancestors.
void OBBNodeS::SetFromTwoNodes ( OBBNodeS * node1, OBBNodeS * node2,const Shape * shape )
{
  AutoArray<Vector3> v; 
  node1->GetVertices(v, shape);
  if (node2)
    node2->GetVertices(v, shape);
  SetFromVertices(v);

  Assert(!_son);
  _son = node1;
  node1->_brother = node2;
}

/**
* Does the given ray intersect the BB?
* Ray from P0 to P1
*/  
bool OBBNodeS::Ray ( Vector3Par P0, Vector3Par P1 )
{
  // first check against bounding sphere
  // geometric solution a la Glassner:
  Vector3 V = P1 - P0;
  float t = V.DotProduct( _c - P0 );
  Vector3 T = P0 + t * V / V.SquareSize();
  float r2 = _r * _r;
  if ( _c.Distance2(T) >= r2)
    return false;

  // maybe some point is closer... (if t is not between P0 and P1
  if (t < 0)  
  {
    if (_c.Distance2(P0) > r2)
      return false;
  }
  else if (t > V.SquareSize())
  {
    if (_c.Distance2(P1) > r2)
      return false;
  }

  // first check against bounding box

  Vector3 POC = P0 - _obbC;
  Vector3 P1C = P1 - _obbC;  

  float uPOC = POC * _u;
  float uP1C = P1C * _u; 

  if (uP1C > _hu)
  {
    float vu = V * _u;
    if (vu == 0) return false; // both points are outside (uPOC == uP1C)

    // restrict to region
    P1C = ((_hu - uPOC ) / vu) * V + POC;
  }
  else if (uP1C < -_hu)
  {          
    float vu = V * _u;
    if (vu == 0) return false; // both points are outside (uPOC == uP1C)

    // restrict to region
    P1C = - ((uPOC + _hu) / vu) * V + POC;
  }      

  if (uPOC > _hu)
  {    
    if ( uP1C > _hu) return false; // out of region

    // restrict to region
    POC += (_hu - uPOC ) / (V * _u) * V;
  }
  else if (uPOC < -_hu)
  {    
    if ( uP1C < -_hu) return false; // out of region

    // restrict to region
    POC -= (uPOC + _hu) / (V * _u) * V;
  }      

  // v axis: 
  uPOC = POC * _v;
  uP1C = P1C * _v; 

  if (uP1C > _hv)
  {          
    float vv = V * _v;
    if (vv == 0) return false; // both points are outside (uPOC == uP1C)

    // restrict to region
    P1C = (_hv - uPOC) / vv * V + POC;
  }
  else if (uP1C < -_hv)
  {          
    float vv = V * _v;
    if (vv == 0) return false; // both points are outside (uPOC == uP1C)

    // restrict to region
    P1C = -(uPOC + _hv) / vv * V + POC;
  }      

  if (uPOC > _hv)
  {    
    if ( uP1C > _hv) return false; // out of region

    // restrict to region
    POC += (_hv - uPOC) / (V * _v) * V;
  }
  else if (uPOC < -_hv)
  {    
    if ( uP1C < -_hv) return false; // out of region

    // restrict to region
    POC -= (uPOC + _hv) / (V * _v) * V;
  }    

  // w axis:  
  uPOC = POC * _w;
  uP1C = P1C * _w; 

  /*if (uP1C > _hw)
  {          
    // restrict to region
    P1C = (_hw - uPOC) / (V * _w) * V + POC;
  }
  else if (uP1C < -_hw)
  {          
    // restrict to region
    P1C = -(uPOC + _hw) / (V * _w) * V + POC;
  } */     

  if (uPOC > _hw)
  {    
    if ( uP1C > _hw) return false; // out of region

    // restrict to region
    //POC += (_hw - uPOC) / (V * _w) * V;
  }
  else if (uPOC < -_hw)
  {    
    if ( uP1C < -_hw) return false; // out of region

    // restrict to region
    //POC -= (uPOC + _hw) / (V * _w) * V;
  }    

  // otherwise intersection exists..
  return true;
}

/**
* Test this subtree (deep) against the given ray.
* Ray from P0 to P1
* 'who' is appended with interfering leaf-nodes.
*/
bool OBBNodeS::RayDeep(Vector3Par P0, Vector3Par P1, RefArray<OBBNodeS> &who)
{
  if ( !Ray( P0, P1) ) return false;

  if ( _son.IsNull())
  {                                         
    who.Add(this);    
    return true;
  }

  bool was = false;

  OBBNodeS * mySons = _son;
  do
  {
    was = mySons->RayDeep( P0, P1, who) || was;
    mySons = mySons->_brother;
  } while ( mySons );

  return was;
}

/**
* Test only this BB (shallow) against the other one (shallow).
* It is considered that transMeToYou is rotation or scaled matrix. If it is skew test for
* separating axes perpendicular to edges is not correct. So do not do it in that case. 
*/
bool OBBNodeS::MeVsYou ( OBBNodeS * b, Matrix4Par transMeToYou ) const
{
  // bounding spheres
  float dist = b->_c.Distance2(transMeToYou * _c);
  float rMax = b->_r + _r;
  rMax *= rMax;

  if (dist > rMax)
    return false;

  // bounding boxes  
  Vector3 vD = b->_obbC - transMeToYou * _obbC;  

  int bAccLevel = b->GetAccuracyLevel();

  // most coarse test:
  if ( BB_TEST_MOST_COARSE(_accuracyLevel,bAccLevel) )
    return true;

  Vector3 uInYou = transMeToYou.Rotate(_u);
  Vector3 vInYou = transMeToYou.Rotate(_v);
  Vector3 wInYou = transMeToYou.Rotate(_w);
  float R, Rab; 

  // Separation this.u:
  float uu = uInYou * b->_u;
  float uv = uInYou * b->_v;
  float uw = uInYou * b->_w;

  float auu = abs(uu);
  float auv = abs(uv);
  float auw = abs(uw);

  R = abs(uInYou * vD);
  Rab = b->_hu * auu + b->_hv * auv + b->_hw * auw;
  if ( R > _hu + Rab ) return false;

  // Separation this.v:
  float vu = vInYou * b->_u;
  float vv = vInYou * b->_v;
  float vw = vInYou * b->_w;

  float avu = abs(vu);
  float avv = abs(vv);
  float avw = abs(vw);

  R = abs(vInYou * vD);
  Rab = b->_hu * avu + b->_hv * avv + b->_hw * avw;
  if ( R > _hv + Rab ) return false;

  // Separation this.w:
  float wu = wInYou * b->_u;
  float wv = wInYou * b->_v;
  float ww = wInYou * b->_w;

  float awu = abs(wu);
  float awv = abs(wv);
  float aww = abs(ww);

  R = abs(wInYou * vD);
  Rab = b->_hu * awu + b->_hv * awv + b->_hw * aww;
  if ( R > _hw + Rab ) return false;

  // Separation b.u:
  R = abs(b->_u * vD);
  Rab = _hu * auu + _hv * avu + _hw * awu;
  if ( R > b->_hu + Rab ) return false;

  // Separation b.v:
  R = abs(b->_v * vD);
  Rab = _hu * auv + _hv * avv + _hw * awv;
  if ( R > b->_hv + Rab ) return false;

  // Separation b.w:
  R = abs(b->_w * vD);
  Rab = _hu * auw + _hv * avw + _hw * aww;
  if ( R > b->_hw + Rab ) return false;

  if ( BB_TEST_COARSE(_accuracyLevel,bAccLevel) )
    return true;

  // BEWARE the following part is not true if transMeToYou is skew

  // b->u -> b->v -> b->w -> b->u

  // Separating this.u x b.u:

  R = abs(vD * uInYou.CrossProduct(b->_u));
  Rab = _hv * awu + _hw * avu + b->_hv * auw + b->_hw * auv;  
  if ( R > Rab ) return false;

  // Separating this.u x b.v:
  R = abs(vD * uInYou.CrossProduct(b->_v));
  Rab = _hv * awv + _hw * avv + b->_hu * auw + b->_hw * auu;  
  if ( R > Rab ) return false;

  // Separating this.u x b.w:
  R = abs(vD * uInYou.CrossProduct(b->_w));
  Rab = _hv * aww + _hw * avw + b->_hu * auv + b->_hv * auu;  
  if ( R > Rab ) return false;

  // Separating this.v x b.u:
  R = abs(vD * vInYou.CrossProduct(b->_u));
  Rab = _hu * awu + _hw * auu + b->_hv * avw + b->_hw * avv;  
  if ( R > Rab ) return false;

  // Separating this.v x b.v:
  R = abs(vD * vInYou.CrossProduct(b->_v));
  Rab = _hu * awv + _hw * auv + b->_hu * avw + b->_hw * avu;  
  if ( R > Rab ) return false;

  // Separating this.v x b.w:
  R = abs(vD * vInYou.CrossProduct(b->_w));
  Rab = _hu * aww + _hw * auw + b->_hu * avv + b->_hv * avu;  
  if ( R > Rab ) return false;

  // Separating this.w x b.u:
  R = abs(vD * wInYou.CrossProduct(b->_u));
  Rab = _hu * avu + _hv * auu + b->_hv * aww + b->_hw * awv;  
  if ( R > Rab ) return false;

  // Separating this.w x b.v:
  R = abs(vD * wInYou.CrossProduct(b->_v));
  Rab = _hu * avv + _hv * auv + b->_hu * aww + b->_hw * awu;  
  if ( R > Rab ) return false;

  // Separating this.w x b.w:
  R = abs(vD * wInYou.CrossProduct(b->_w));
  Rab = _hu * avw + _hv * auw + b->_hu * awv + b->_hv * awu;  
  if ( R > Rab ) return false;
  return true;
};

/**
* Test only this BB (shallow) against the other one (shallow).
* It is considered that transMeToYou is rotation or scaled matrix. If it is skew test for
* separating axes perpendicular to edges is not correct. So do not do it in that case. 
*/
bool OBBNodeS::MeVsYou ( OBBNodeS * b, Matrix4Par transMeToYou, float epsilon) const
{
  // bounding spheres
  float dist = b->_c.Distance2(transMeToYou * _c);
  float rMax = b->_r + _r + epsilon;
  rMax *= rMax;

  if (dist > rMax)
    return false;

  // bounding boxes

  int bAccLevel = b->GetAccuracyLevel();

  // most coarse test:
  /*if ( BB_TEST_MOST_COARSE(_accuracyLevel,bAccLevel) )
    return true;*/

  // enlarge this size by epsilon
  float hu = _hu + epsilon;
  float hv = _hv + epsilon;
  float hw = _hw + epsilon;

  Vector3 vD = b->_obbC - transMeToYou * _obbC;  

  Vector3 uInYou = transMeToYou.Rotate(_u);
  Vector3 vInYou = transMeToYou.Rotate(_v);
  Vector3 wInYou = transMeToYou.Rotate(_w);
  float R, Rab; 

  // Separation this.u:
  float uu = uInYou * b->_u;
  float uv = uInYou * b->_v;
  float uw = uInYou * b->_w;

  float auu = abs(uu);
  float auv = abs(uv);
  float auw = abs(uw);

  R = abs(uInYou * vD);
  Rab = b->_hu * auu + b->_hv * auv + b->_hw * auw;
  if ( R > hu + Rab ) return false;

  // Separation this.v:
  float vu = vInYou * b->_u;
  float vv = vInYou * b->_v;
  float vw = vInYou * b->_w;

  float avu = abs(vu);
  float avv = abs(vv);
  float avw = abs(vw);

  R = abs(vInYou * vD);
  Rab = b->_hu * avu + b->_hv * avv + b->_hw * avw;
  if ( R > hv + Rab ) return false;

  // Separation this.w:
  float wu = wInYou * b->_u;
  float wv = wInYou * b->_v;
  float ww = wInYou * b->_w;

  float awu = abs(wu);
  float awv = abs(wv);
  float aww = abs(ww);

  R = abs(wInYou * vD);
  Rab = b->_hu * awu + b->_hv * awv + b->_hw * aww;
  if ( R > hw + Rab ) return false;

  // Separation b.u:
  R = abs(b->_u * vD);
  Rab = hu * auu + hv * avu + hw * awu;
  if ( R > b->_hu + Rab ) return false;

  // Separation b.v:
  R = abs(b->_v * vD);
  Rab = hu * auv + hv * avv + hw * awv;
  if ( R > b->_hv + Rab ) return false;

  // Separation b.w:
  R = abs(b->_w * vD);
  Rab = hu * auw + hv * avw + hw * aww;
  if ( R > b->_hw + Rab ) return false;

  if ( BB_TEST_COARSE(_accuracyLevel,bAccLevel) )
    return true;

  // BEWARE the following part is not true if transMeToYou is skew

  // b->u -> b->v -> b->w -> b->u

  // Separating this.u x b.u:

  R = abs(vD * uInYou.CrossProduct(b->_u));
  Rab = hv * awu + hw * avu + b->_hv * auw + b->_hw * auv;  
  if ( R > Rab ) return false;

  // Separating this.u x b.v:
  R = abs(vD * uInYou.CrossProduct(b->_v));
  Rab = _hv * awv + _hw * avv + b->_hu * auw + b->_hw * auu;  
  if ( R > Rab ) return false;

  // Separating this.u x b.w:
  R = abs(vD * uInYou.CrossProduct(b->_w));
  Rab = hv * aww + hw * avw + b->_hu * auv + b->_hv * auu;  
  if ( R > Rab ) return false;

  // Separating this.v x b.u:
  R = abs(vD * vInYou.CrossProduct(b->_u));
  Rab = hu * awu + hw * auu + b->_hv * avw + b->_hw * avv;  
  if ( R > Rab ) return false;

  // Separating this.v x b.v:
  R = abs(vD * vInYou.CrossProduct(b->_v));
  Rab = hu * awv + hw * auv + b->_hu * avw + b->_hw * avu;  
  if ( R > Rab ) return false;

  // Separating this.v x b.w:
  R = abs(vD * vInYou.CrossProduct(b->_w));
  Rab = hu * aww + hw * auw + b->_hu * avv + b->_hv * avu;  
  if ( R > Rab ) return false;

  // Separating this.w x b.u:
  R = abs(vD * wInYou.CrossProduct(b->_u));
  Rab = hu * avu + hv * auu + b->_hv * aww + b->_hw * awv;  
  if ( R > Rab ) return false;

  // Separating this.w x b.v:
  R = abs(vD * wInYou.CrossProduct(b->_v));
  Rab = hu * avv + hv * auv + b->_hu * aww + b->_hw * awu;  
  if ( R > Rab ) return false;

  // Separating this.w x b.w:
  R = abs(vD * wInYou.CrossProduct(b->_w));
  Rab = hu * avw + hv * auw + b->_hu * awv + b->_hv * awu;  
  if ( R > Rab ) return false;
  return true;
};


/**
* Test this subtree (deep) against the other subtree (deep).
* 'who' is appended with interfering leaf-node tuples.
*/
bool OBBNodeS::DeepVsDeep ( OBBNodeS * root, Matrix4Par transMeToYou, AutoArray<const OBBNodeS*, MemAllocSA> &who ) const
{
  if ( !MeVsYou(root, transMeToYou) ) return false;

  if ( _son.IsNull() && root->_son.IsNull() )
  {                                         // two leaf nodes
    who.Add(this);
    who.Add(root);
    return true;
  }

  bool was = false;

  if ( root->_son.IsNull()/* ||
                          m_son.NotNull() && m_r > root->m_r */) // divide myself
  {
    OBBNodeS * mySons = _son;
    do
    {
      was = mySons->DeepVsDeep(root,transMeToYou,who) || was;
      mySons = mySons->_brother;
    } while ( mySons );
  }

  else                                      // divide the other node
  {
    OBBNodeS * rootSons = root->_son;
    do
    {
      was = DeepVsDeep(rootSons,transMeToYou,who) || was;
      rootSons = rootSons->_brother;
    } while ( rootSons );
  }

  return was;
}

bool OBBNodeS::DeepVsDeep ( OBBNodeS * root, Matrix4Par transMeToYou, AutoArray<const OBBNodeS *, MemAllocSA> &who, float epsilon ) const
{
  if ( !MeVsYou(root, transMeToYou, epsilon) ) return false;

  if ( _son.IsNull() && root->_son.IsNull() )
  {                                         // two leaf nodes
    who.Add(this);
    who.Add(root);
    return true;
  }

  bool was = false;

  if ( root->_son.IsNull() ||
    (_son.NotNull() && _r > root->_r)) // divide myself
  {
    OBBNodeS * mySons = static_cast<OBBNodeS *>(_son.GetRef());
    do
    {
      was = mySons->DeepVsDeep(root,transMeToYou,who, epsilon) || was;
      mySons = mySons->_brother;
    } while ( mySons );
  }

  else                                      // divide the other node
  {
    OBBNodeS * rootSons = root->_son;
    do
    {
      was = DeepVsDeep(rootSons,transMeToYou,who, epsilon) || was;
      rootSons = rootSons->_brother;
    } while ( rootSons );
  }

  return was;
}

/**
* Test only this BB (shallow) against the other one (shallow).
*/
bool OBBNodeS::MeStrechedVsYou ( Vector3Val offsetMe, OBBNodeS * you, Matrix4Par transMeToYou )
{
  // bounding spheres
  // geometric solution a la Glassner:
  Vector3 V = transMeToYou.Rotate(offsetMe);
  Vector3 P0 = transMeToYou * _c;
  float t = V.DotProduct( you->_c - P0 );
  Vector3 T = P0 + t * V / V.SquareSize();
  float r2 = _r  + you->_r;
  r2 *= r2;
  if ( you->_c.Distance2(T) >= r2)
    return false;

  // maybe some point is closer... (if t is not between P0 and P1
  if (t < 0)  
    return you->_c.Distance2(P0) < r2;  
  else if (t > V.SquareSize())
    return you->_c.Distance2(P0 + V) < r2;
  else
    return true; // is between points

  // bounding boxes
  // hmmm it will be too complicated
}

/**
* Test this subtree (deep) against the other subtree (deep). 
* This subtree is localized anywhere between current position and position moved by offset
* 'who' is appended with interfering leaf-node tuples.
*/
bool OBBNodeS::DeepStrechedVsDeep (Vector3Val offsetMe, OBBNodeS * root, Matrix4Par transMeToYou, RefArray<OBBNodeS> &who )
{
  if ( !MeStrechedVsYou(offsetMe,root, transMeToYou) ) return false;

  if ( _son.IsNull() && root->_son.IsNull() )
  {                                         // two leaf nodes
    who.Add(this);
    who.Add(root);
    return true;
  }

  bool was = false;

  if ( root->_son.IsNull()/* ||
                          m_son.NotNull() && m_r > root->m_r */) // divide myself
  {
    OBBNodeS * mySons = _son;
    do
    {
      was = mySons->DeepStrechedVsDeep(offsetMe,root,transMeToYou,who) || was;
      mySons = mySons->_brother;
    } while ( mySons );
  }

  else                                      // divide the other node
  {
    OBBNodeS * rootSons = root->_son;
    do
    {
      was = DeepStrechedVsDeep(offsetMe,rootSons,transMeToYou,who) || was;
      rootSons = rootSons->_brother;
    } while ( rootSons );
  }

  return was;
}
