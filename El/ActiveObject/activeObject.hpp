#ifdef _MSC_VER
#pragma once
#endif

#ifndef _EL_COMMAND_QUEUE
#define _EL_COMMAND_QUEUE

#include <El/elementpch.hpp>
#include <El/Common/perfProf.hpp>
#include <El/Multicore/multicore.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Threads/multisync.hpp>
//#include <boost/tuple/tuple.hpp>

#define SUPPORT_RETURN_VALUES 0

#if SUPPORT_RETURN_VALUES
// we are unable to pass Void values, therefore we create an artificial Void type
struct Void
{
};
#else
typedef void Void;
#endif

/// provide a handle to a future result
template <class Value>
class FutureValue: private NoCopy
{
  Value _value;
  Event _done;
  
  public:
  FutureValue()
  :_done(true) // manual reset - once set, it stays set forever
  {
  }
  void SetResult(const Value &value)
  {
    _value = value;
    _done.Set();
  }
  const Value &GetResult() const
  {
    _done.Wait();
    return _value;
  }
  
  operator const Value &() const {return GetResult();}
};

// note: following does not work, we are unable to make Void out of void.
template <>
class FutureValue<void>
{
  public:
  void GetResult() const {}
  void SetResult() {}
};

#if SUPPORT_RETURN_VALUES
template <>
class FutureValue<Void>
{
  public:
  void GetResult() const {}
  void SetResult(Void x) {}
};
#endif

template <class Type>
class Future: public SRef< FutureValue<Type> >
{
  typedef SRef< FutureValue<Type> > base;
  
  public:
  Future():base(new FutureValue<Type>){}
};

/// specialization - void needs no allocation
template <>
class Future<Void>
{
  public:
  void *GetRef() const {return NULL;}
};

/// make sure each command is aligned so that subsequent commands can be aligned too
/**
Note: cleaner alternative would be to introduce before read/before write alignment via a new function of SizeOf
However defining it globally here is a lot simpler (and results in better performance too)
*/
static inline size_t AlignStoreSize(size_t size)
{
  #ifdef _XBOX
  const int alignment = sizeof(__vector4);
  #else
  const int alignment = sizeof(int);
  #endif
  return (size+alignment-1)&~(alignment-1);
}

/// some structures might contain a dynamic information in them
template <class Type>
struct StoredSize
{
  static size_t SizeOf(const Type &val) {return AlignStoreSize(sizeof(val));}
};
/// default implementation for storing into SerialArray - allows external overloading for individual types
/** implemented because of ComRef specific needs */

template <class DstType, class SrcType>
struct SerialStoreTraits
{
  static void Store(DstType &dst, const SrcType &src)
  {
    dst = src;
  }
};

/// easy way to call SerialStoreTraits::Store without having to provide template parameters
template <class DstType, class SrcType>
inline void SerialStore(DstType &dst, const SrcType &src)
{
  SerialStoreTraits<DstType,SrcType>::Store(dst,src);
}

/// command storage used for CommandBuffers - used for recording
template < int bufferSize=1024, class Container=AutoArray<char, MemAllocLocal<char, bufferSize> > >
class SerialArraySimple
{
  Container _data;
  int _writeTo;

  public:
  
  SerialArraySimple()
  {
    _writeTo = 0;
  }
  ~SerialArraySimple()
  {
  }
  
  /**
  avoid using _writeTo member, because this leads to compiler assuming aliasing
  */
  template <class Type>
  void Set(Type *&val, int writeTo)
  {
    // reserve maximum possible space for the item
    _data.Access(writeTo+AlignStoreSize(sizeof(Type))-1);
    val = (Type *)&_data[writeTo];
    // construct the target area, so that it is ready for an initialization (will be done member by member)
    ConstructAt(*val);
  }
  template <class Type>
  int Put(const Type &val, int writeTo)
  {
    // construct the target area, so that it is ready for an initialization
    int writeEnd = writeTo+StoredSize<const Type>::SizeOf(val);
    _data.Access(writeEnd-1);
    Type *dst = (Type *)&_data[writeTo];
    ConstructAt(*dst);
    // assign the value
    // and advance
    *dst = val;
    // no need to wrap here - wrap is done on FinishWrite only
    return writeEnd;
  }
  
  __forceinline int StartWrite() {return _writeTo;}
  
  template <class Type>
  __forceinline void FinishWrite(const Type *val,int writeTo)
  {
    // we know the size only after the value is written to
    int written = StoredSize<const Type>::SizeOf(*val);
    // verify there is no misalignment caused by the data written
    Assert((written&(sizeof(int)-1))==0);
    //LogF("W %x (%d)",writeTo,written);
    Assert(writeTo+written<=_data.Size());
    _writeTo = writeTo+written;
  }

  /// access to allow reading the command buffer
  const void *GetData(int &size) const
  {
    // verify nobody else is reading the data, and the buffer is large enough to contain the data
    Assert(_data.Size()>=_writeTo);
    size = _writeTo;
    return _data.Data();
  }
  
  int GetCurrentOffset() const {return _writeTo;}
  
  /// reset the command buffer - start recording again
  void Reset()
  {
    _data.Resize(0);
    _writeTo = 0;  
  }
};

/// lightweight counterpart of SerialArrayCircular - read only data
class SerialArrayData
{
  const Array<char> &_data;
  
  public:
  
  SerialArrayData(const Array<char> &data):_data(data)
  {
  }
  ~SerialArrayData()
  {
  }
  
  
  template <class Type>
  __forceinline RESTRICT_RETURN const Type *Get(int &pos)
  {
    #if _DEBUG
    int storedSize = StoredSize<const Type>::SizeOf(*(Type *)(_data.Data()+pos));
    Assert(pos+storedSize<=_data.Size());
    #endif
    // it is quite possible the target may not fit inside - it may be a truncated variables sized item
    //Assert(_readFrom+(int)sizeof(Type)<=_data.Size());
    // target is already constructed - we need to destruct the data first, so that we can move on
    // simply return a pointer to a data we already have - no copy needed
    return (Type *)(_data.Data()+pos);
  }
  template <bool destruct, class Type>
  __forceinline void Advance(Type * __restrict val, int &pos)
  {
    // we are done with the data - we may destruct them now
    Assert(val==(Type *)(_data.Data()+pos));
    // before destructing them we need to read their size
    // note: we always pass a "const Type" to the StoredSize to avoid passing sometimes const Type and sometimes Type
    size_t size = StoredSize<const Type>::SizeOf(*val);
    if (destruct) val->~Type();
    // and advance over them - this will also enable writing into them
    //LogF("R %x (%d)",_readFrom,size);
    pos += size;
    
  }
  
  __forceinline bool IsEmpty(int pos) const {return pos>=_data.Size();}
    
};

/// lightweight counterpart of active object - command buffer recording
template <class ServantClass,int size>
class CommandBufferRec
{
  protected:
  typedef ServantClass Servant;
  
  /// a list of functions to be called, including their arguments
  SerialArraySimple<size> _invocationList;
  
  #if _DEBUG
  // for debugging purposes we record everything in text form as well
  CharArray<> _debug;
  
  #endif
  
  public:
  
  CommandBufferRec()
  {
    #if _DEBUG
    _debug.Reset();
    #endif
  }
  
  const void *GetData(int &size) const
  {
    return _invocationList.GetData(size);
  }

  int GetDataSize() const
  {
    int size;
    _invocationList.GetData(size);
    return size;
  }
  
  int GetCurrentOffset() const {return _invocationList.GetCurrentOffset();}
  
  void Reset()
  {
    _invocationList.Reset();
    #if _DEBUG
    _debug.Reset();
    #endif
  }

  #if _DEBUG
  const char *GetDebugText() const {return _debug;}
  void AppendDebugText(const char *text){_debug+=text;}
  #else
  const char *GetDebugText() const {return "";}
  void AppendDebugText(const char *text){}
  #endif
};

/// lightweight counterpart of active object - command buffer playback
template <class ServantClass>
class CommandBufferPlay: protected ServantClass
{
  protected:
  typedef ServantClass Servant;
  
  public:
  
  CommandBufferPlay(){}
  
  template <bool destruct, class InvocationContainer>
  void Execute(InvocationContainer &invocationList);

  template <class InvocationContainer>
  void Destruct(InvocationContainer &invocationList);
};

template <class ServantClass>
template <bool destruct, class InvocationContainer>
void CommandBufferPlay<ServantClass>::Execute(InvocationContainer &invocationList)
{
  // adapted from ActiveObject::Update
  int pos = 0;
  while (!invocationList.IsEmpty(pos))
  {  
    // some message is there - check it
    // first get message ID
    const int *idPtr = invocationList.Get<int>(pos);
    int id = *idPtr;
    invocationList.Advance<destruct>(idPtr,pos);
    Servant::ProcessItem<destruct>((typename Servant::ActObjIds)id,invocationList,pos);
  }
}

template <class ServantClass>
template <class InvocationContainer>
void CommandBufferPlay<ServantClass>::Destruct(InvocationContainer &invocationList)
{
  // adapted from ActiveObject::Update

  int pos = 0;
  while (!invocationList.IsEmpty(pos))
  {  
    // some message is there - check it
    // first get message ID
    const int *idPtr = invocationList.Get<int>(pos);
    int id = *idPtr;
    invocationList.Advance<true>(idPtr,pos);
    Servant::DestructItem((typename Servant::ActObjIds)id,invocationList,pos);
  }
}

/// used to define argument types
#define ACTOBJ_DECLARE_ARGS(ctx,Ret,Name,Args,NumArgs,DefArgs) \
  struct Name##AllArgs \
  { \
    /*FutureValue<Ret> *retVal;*/ /* retVal before argument values - arguments may be truncated */ \
    DefArgs; \
  };

/// used to declare functions
#define ACTOBJ_DECLARE_FUNCTION(ctx,Ret,Name,Args,NumArgs,DefArgs) \
  Ret Name(const Name##AllArgs &args);

/// used to generate enum identifying messages
#define ACTOBJ_ID_FUNCTION(Type,Ret,Name,Args,NumArgs,DefArgs) AO##Name,

/// used to implement method invocation
#define ACTOBJ_SWITCH_FUNC_PROCESS(Type,Ret,Name,Args,NumArgs,DefArgs) \
  case AO##Name: \
    {  \
      /* now we need each argument to be read from the invocation list */ \
      const Name##AllArgs *args = invocationList.Get<Name##AllArgs>(pos); \
      Name(*args); \
      /*args->retVal->SetResult(Name(*args));*/ \
      /* we must not advance the pointer until we are done with the data */ \
      invocationList.Advance<destruct>(args,pos); \
    } \
    break;

/// used to implement method invocation for destruction
#define ACTOBJ_SWITCH_FUNC_DESTRUCT(Type,Ret,Name,Args,NumArgs,DefArgs) \
  case AO##Name: \
    {  \
      /* now we need each argument to be read from the invocation list */ \
      const Name##AllArgs *args = invocationList.Get<Name##AllArgs>(pos); \
      /* we must not advance the pointer until we are done with the data */ \
      invocationList.Advance<true>(args,pos); \
    } \
    break;

/// define a function processing individual messages in the Servant class
#define IS_ACTIVE_OBJECT(Type,XXX) \
  template <bool destruct,class SerialArrayType> \
  __forceinline void ProcessItem(ActObjIds id, SerialArrayType &invocationList, int &pos) \
  { \
    switch (id) \
    { \
      XXX(Type,ACTOBJ_SWITCH_FUNC_PROCESS) \
    } \
  } \
  template <class SerialArrayType> \
  __forceinline void DestructItem(ActObjIds id, SerialArrayType &invocationList, int &pos) \
  { \
    switch (id) \
    { \
      XXX(Type,ACTOBJ_SWITCH_FUNC_DESTRUCT) \
    } \
  }


#define READARGS0 
#define READARGS1            SerialStore(buf->arg1,arg1)
#define READARGS2  READARGS1;SerialStore(buf->arg2,arg2)
#define READARGS3  READARGS2;SerialStore(buf->arg3,arg3)
#define READARGS4  READARGS3;SerialStore(buf->arg4,arg4)
#define READARGS5  READARGS4;SerialStore(buf->arg5,arg5)
#define READARGS6  READARGS5;SerialStore(buf->arg6,arg6)
#define READARGS7  READARGS6;SerialStore(buf->arg7,arg7)
#define READARGS8  READARGS7;SerialStore(buf->arg8,arg8)
#define READARGS9  READARGS8;SerialStore(buf->arg9,arg9)
#define READARGS10 READARGS9;SerialStore(buf->arg10,arg10)

/// trait determining for each input type a string type to convert to, and performing the conversion
template <class Type>
struct ArgumentToPrint
{
  typedef const char *RetType;
  
  static RetType FormatArg(const Type &value);
};

/// creative function for ArgumentToPrint
template <class Type>
ArgumentToPrint<Type> ArgumentToPrintF(const Type &value)
{
  return ArgumentToPrint<Type>();
}

template <class ArgType>
void DebugArg(CharArray<> &log, const ArgType &arg)
{
  // by default assume some conversion from argument to string exists
  log += " ";
  log += ArgumentToPrintF(arg).FormatArg(arg);
}

/***/

#define DEBUGARGS0 
#define DEBUGARGS1             DebugArg(_debug,arg1)
#define DEBUGARGS2  DEBUGARGS1;DebugArg(_debug,arg2)
#define DEBUGARGS3  DEBUGARGS2;DebugArg(_debug,arg3)
#define DEBUGARGS4  DEBUGARGS3;DebugArg(_debug,arg4)
#define DEBUGARGS5  DEBUGARGS4;DebugArg(_debug,arg5)
#define DEBUGARGS6  DEBUGARGS5;DebugArg(_debug,arg6)
#define DEBUGARGS7  DEBUGARGS6;DebugArg(_debug,arg7)
#define DEBUGARGS8  DEBUGARGS7;DebugArg(_debug,arg8)
#define DEBUGARGS9  DEBUGARGS8;DebugArg(_debug,arg9)
#define DEBUGARGS10 DEBUGARGS9;DebugArg(_debug,arg10)

#define PASSARGS0 
#define PASSARGS1 arg1
#define PASSARGS2 PASSARGS1,arg2
#define PASSARGS3 PASSARGS2,arg3
#define PASSARGS4 PASSARGS3,arg4
#define PASSARGS5 PASSARGS4,arg5
#define PASSARGS6 PASSARGS5,arg6
#define PASSARGS7 PASSARGS6,arg7
#define PASSARGS8 PASSARGS7,arg8
#define PASSARGS9 PASSARGS8,arg9
#define PASSARGS10 PASSARGS9,arg10

#if SUPPORT_RETURN_VALUES
# define SET_RET_VAL buf->retVal = (FutureValue<Ret> *)futureRet.GetRef();
#else
# define SET_RET_VAL
#endif

#if 0 //_DEBUG
# define LOG_DEBUG_REC(Name,NumArgs) AppendDebugText(#Name);DEBUGARGS##NumArgs;AppendDebugText("\n")
#else
# define LOG_DEBUG_REC(Name,NumArgs)
#endif


/// used inside a wrapper around the active object, which allows us to use normal function calls to invoke methods
/**
we are forcing inline here, as what we do here is not much more complicated than normal function parameter passing
and we want to avoid the parameter copying overhead
*/

#define CMDBUF_WRAP_FUNC(Type,Ret,Name,Args,NumArgs,DefArgs) \
  __forceinline Future<Ret> Name Args \
  { \
    int writeTo = _invocationList.StartWrite(); \
    writeTo = _invocationList.Put(Servant::AO##Name,writeTo); \
    Name##AllArgs *buf; \
    _invocationList.Set(buf,writeTo); \
    READARGS##NumArgs; \
    Future<Ret> futureRet; \
    SET_RET_VAL \
    _invocationList.FinishWrite(buf,writeTo); \
    LOG_DEBUG_REC(Name,NumArgs); \
    return futureRet; \
  }

#endif
