// Clipboard.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/Clipboard/clipboard.hpp>

int main(int argc, char* argv[])
{
	RString in = "Test 001";
	ExportToClipboardAndFile(in, in.GetLength(), "test.txt");

	RString out = ImportFromClipboard();

	return 0;
}
