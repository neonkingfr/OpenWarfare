#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MATH_GEOM_3D_HPP
#define _MATH_GEOM_3D_HPP

#include "math3d.hpp"

/// point on line beg .. end nearest to pos, t gives limits (0,1 represent beg, end endpoints)
static inline float NearestPointDistance( Vector3Par beg, Vector3Par end, Vector3Par pos, float minT, float maxT )
{
  Vector3Val eb = end - beg;
  Vector3Val pb = pos - beg;
  float t = (eb*pb) / eb.SquareSize();
  saturate(t,minT,maxT);
  Vector3 nearest = beg+eb*t;
  return nearest.Distance(pos);

}

/// point on line beg .. end nearest to pos, line without any limits

static inline float NearestPointT( Vector3Par beg, Vector3Par end, Vector3Par pos )
{
  // point on line beg .. end nearest to pos
  Vector3Val eb = end - beg;
  Vector3Val pb = pos - beg;
  float denom2 = eb.SquareSize();
  if (denom2<=0) return 0;
  return (eb*pb) / denom2;
}

static inline Vector3 NearestPoint( Vector3Par beg, Vector3Par end, Vector3Par pos )
{
  // point on line beg .. end nearest to pos
  Vector3Val eb = end - beg;
  Vector3Val pb = pos - beg;
  float denom2 = eb.SquareSize();
  if (denom2<=0) return beg;
  float t = (eb*pb) / denom2;
  saturate(t,0,1);
  return beg+eb*t;
}

static inline Vector3 NearestPointInfinite( Vector3Par beg, Vector3Par end, Vector3Par pos )
{
  // point on line beg .. end nearest to pos
  Vector3Val eb = end - beg;
  Vector3Val pb = pos - beg;
  float denom2 = eb.SquareSize();
  // when line is ill-defined, return anything
  if (denom2<=0) return beg;
  float t = (eb*pb) / denom2;
  return beg+eb*t;
}

static inline float NearestPointDistance( Vector3Par beg, Vector3Par end, Vector3Par pos )
{
  Vector3Val nearest = NearestPoint(beg,end,pos);
  return nearest.Distance(pos);
}

static inline float NearestPointInfiniteDistance( Vector3Par beg, Vector3Par end, Vector3Par pos )
{
  Vector3Val nearest = NearestPointInfinite(beg,end,pos);
  return nearest.Distance(pos);
}

/// interpolation based on source values, saturated by limits
float Interpolativ( float control, float cMin, float cMax, float vMin, float vMax );

/// Returns x + s(y - x) 
/**
This linearly interpolates between x and y, such that the return value is x when s is 0, and y when s is 1.
*/
static inline float Lerp(float x, float y, float s)
{
  return x + s*(y - x); 
}

float AngleDifference( float a, float b );

/// check line against box intersection
bool IsIntersectionBoxWithLine(Vector3Val boxMin, Vector3Val boxMax, Vector3Val lineBeg, Vector3Val lineEnd);

#endif
