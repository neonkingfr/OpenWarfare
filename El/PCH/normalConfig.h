// Configuration parameters for internal release / debug version

#ifdef _WIN32
  #if _PROFILE || _DEBUG
    // Profile configuration - enable more counters
    #define _ENABLE_PERFLOG 2
  #else
    // Basic configuration - enable basic counters
    #define _ENABLE_PERFLOG 1
  #endif
#else
  #define _ENABLE_PERFLOG           0
#endif
#if defined _XBOX && _SUPER_RELEASE
  #define _ENABLE_PATCHING					0
  #define _ENABLE_INJECTING					0
#else
  #define _ENABLE_PATCHING					1
  #define _ENABLE_INJECTING					1
#endif

