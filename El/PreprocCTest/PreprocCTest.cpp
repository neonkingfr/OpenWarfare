// PreprocCTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/preprocC/preprocC.hpp>

int main(int argc, char* argv[])
{
	CPreprocessorFunctions functions;
	QOFStream out;
	out.open("test.i");
	functions.Preprocess(out, "test.txt");
	out.close();
	return 0;
}
