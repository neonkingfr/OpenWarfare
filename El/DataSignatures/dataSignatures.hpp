#ifdef _MSC_VER
#pragma once
#endif

#ifndef _EL_DATA_SIGNATURES_HPP
#define _EL_DATA_SIGNATURES_HPP

#ifndef _WIN32
#define USE_HASH_LIB
#endif

#include <Es/Strings/rString.hpp>
#include <Es/Types/pointers.hpp>
#ifdef USE_HASH_LIB
#include "hashwrapper.h"
#include "sha1wrapper.h"
#endif

#define SIGNATURES_TEST_ONLY 0

// BIS_SIGNATURE_VERSION review
// 1 ... old style signature signs hash of data content and there is no version number stored after the serialized data _content
// 2 ... signature signs hash of (data content hash)+(pbo file list hash)+(pbo prefix) and there is version number stored
#define BIS_SIGNATURE_VERSION 2

// Prior raising the version (causing MP incompatibility), only partial signature 2 checking is used
const bool UseSigVer2Full = false;

class SerializeBinStream;

struct DSKey
{
  RString _name;
  Temp<char> _content;

  bool operator ==(const DSKey &with) const;

  void SerializeBin(SerializeBinStream &stream);
  bool Load(RString filename);
};
TypeIsMovable(DSKey)

struct DSHash
{
  Temp<char> _content;
};

struct DSSignature
{
  DSKey _key;
  int      _version;
  Temp<char> _content1;  // sigVer1
  Temp<char> _content2;  // sigVer2

  void SerializeBin(SerializeBinStream &stream);
  bool Load(RString filename);
  int  Version() const { return _version; }
};

class DataSignatures
{
public:
  // in - filename
  // in - acceptedKeys, acceptedKeysCount
  // out - signature
  // return - found
  static bool FindSignature(
    DSSignature &signature,
    RString filename,
    const DSKey *acceptedKeys, int acceptedKeysCount);

  // in - filename
  // out - hash
  // return - processed without errors
  static bool GetHash(DSHash &hash, RString filename);

  // in - hash
  // in - signature
  // return - signature is valid
  static bool VerifySignature(const DSHash &hash, const DSSignature &signature);
};

class HashCalculator
{
protected:
  void *_provider;
  void *_hash;
  mutable bool _ok;
#ifdef USE_HASH_LIB
  Ref<hashwrapper> shaWrapper;
#endif

public:
  //! Constructor
  HashCalculator();
  //! Destructor
  ~HashCalculator();
  //! initialize
  void Reset();
  //! add memory block
  void Add(const void *data, int len);
  //! add memory block - OStream like interface (useful for QIStream::copy)
  __forceinline void write(const void *data, int len) {Add(data,len);}
  //! add single character
  void Add(char c) {Add(&c, sizeof(char));}
  //! get result of all Add operations since Reset()
  bool GetResult(AutoArray<char> &result) const;
};

class QIStream;
class QFBank;

class BankHashCalculatorAsync
{
protected:
  void *_provider;
  void *_hash;
  bool _ok;
  bool _cancel;
  RString _bankPrefix;
#ifdef USE_HASH_LIB
  Ref<hashwrapper> shaWrapper;
#endif

  int _bankIndex;

  struct BankRange
  {
    int _offsetBeg;
    int _offsetEnd;
    ClassIsSimple(BankRange)
  };

  AutoArray<BankRange> _ranges;
  int _index;
  int _pos;

public:
  //! Constructor
  BankHashCalculatorAsync(int bankIndex);
  //! Destructor
  ~BankHashCalculatorAsync();
  //! Update function
  // returns true if done
  bool Process();
  //! the result of testing
  bool IsValid();

  // Callback functions
  //! Add a range for testing
  void AddRange(int dataBeg, int dataEnd);
  //! Calculate hash from stream if data are ready
  bool Process(QIStream &in);
};

#endif
