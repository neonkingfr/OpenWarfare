// ColorTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/Color/colors.hpp>

int main(int argc, char* argv[])
{
	Color color(0, 1, 0, 1);
	PackedColor pcolor(color);
	color = color * color;

	return 0;
}
