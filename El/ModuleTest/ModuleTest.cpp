// ModuleTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/Modules/modules.hpp>

int main(int argc, char* argv[])
{
	InitModules();
	return 0;
}

INIT_MODULE(Test, 1)
{
	printf("Hello, world");
}

