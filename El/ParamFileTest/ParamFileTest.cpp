// ParamFileTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/ParamFile/paramFile.hpp>

int main(int argc, char* argv[])
{
	ParamFile out;
	out.Add("Test", 3.14f);
	out.Save("test.cfg");

	ParamFile in;
	in.Parse("test.cfg");
	float pi = in >> "Test";
	return 0;
}
