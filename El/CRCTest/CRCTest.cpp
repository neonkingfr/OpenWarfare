// CRCTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/CRC/crc.hpp>

int main(int argc, char* argv[])
{
	CRCCalculator crc;

	crc.Reset();
	crc.Reset();

	crc.Add('C');
	const char *text = "Ahoj";
	crc.Add(text, strlen(text));

	int result = crc.GetResult();

	return 0;
}
