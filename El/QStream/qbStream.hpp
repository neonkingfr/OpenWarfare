#ifdef _MSC_VER
#pragma once
#endif

#ifndef _QBSTREAM_HPP
#define _QBSTREAM_HPP

#include "qStream.hpp"

#include <Es/Containers/array.hpp>
#include <Es/Strings/bString.hpp>
#include <Es/Algorithms/qsort.hpp>

class QFBank;
class QFBankHandle;

//! check if given bank/file is accessible in given context
/*!
  In future this will probably be used also to open the file using given access.
*/

class IQFBankContext
{
  public:
  //! check if file can be accessed
  virtual bool IsAccessible(const QFBank *bank) const = 0;
};

class QFBankQueryFunctions
{
  public:
  static QFBank *AutoBank( const char *name );
  //! check timestamp of the file in the bank or outside of it
  static QFileTime TimeStamp( const char *name, IQFBankContext *context=NULL );
  //! check if file exists in the bank or outside of it
  static bool FileExist( const char *name, IQFBankContext *context=NULL );
  //! query file size
  static QFileSize GetFileSize(const char *name);
  // overload FileExists of QIFStream to avoid any confusion
  static bool FileExists( const char *name, IQFBankContext *context=NULL )
  {
    return FileExist(name,context);
  }
  /// check recommended loading order
  static int FileOrder(const char *nameA, const char *nameB);
  /// delete all banks
  static void ClearBanks();
  
  static QFBankHandle GetHandle( const char *name, QFileSize offset=0, QFileSize size=QFileSizeMax); // autoselect bank/file
};

//! stream that is opened from some file bank
class QIFStreamB: public QIFStream //, public QFBankQueryFunctions
{
  const QFBank *_bank;

  public:
  QIFStreamB();

  void AutoOpen( const char *name, QFileSize offset=0, QFileSize size=QFileSizeMax ); // autoselect bank/file
  bool IsAccessible(IQFBankContext *context) const;

  void open(const QFBankHandle &handle); // open and preload file

  void open( const QFBank &bank, const char *name, QFileSize offset=0, QFileSize size=QFileSizeMax ); // open and preload file
  bool IsFromBank(const QFBank *bank) const;
};

class FindBank
{
#ifdef _WIN32
  BString<1024> _wild;
  // check all "pb?" file banks
  void *_info;
  long _handle;

#else
    DIR *dir;
    struct dirent *entry;
#endif
  public:
  FindBank();
  ~FindBank();

  bool First(const char *path);
  bool Next();
  void Close();

  const char *GetName() const;
};

class BankContextBase: public RefCount
{
};

typedef bool (*OpenCallback)(QFBank *bank, BankContextBase *context);

/// list of all active file banks
class BankList: public AutoArray<QFBank>
{
  public: 
  /// load a new bank into the list
  RString Load(
    const RString &path,
    const RString &bankPrefix,const RString &bName, bool emptyPrefix,
    OpenCallback beforeOpen=NULL, OpenCallback afterOpen=NULL,
    BankContextBase *context=NULL
  );
  void Unload
  (
    const RString & bankPrefix,const RString &bName, bool emptyPrefix
  );
  void Lock(const RString &prefix);
  void Unlock(const RString &prefix);
  void SetLockable(const RString &prefix, bool lockable);

  //! unload all unused bank files
  bool UnloadUnused();
};

extern BankList GFileBanks;

int CmpStartStr( const char *str, const char *start );

#include "fileinfo.h"

#include <Es/Memory/normalNew.hpp>

struct FileInfoO
{
  //char name[NAME_LEN];
  RStringB name;

  #ifndef _XBOX
    unsigned int compressedMagic;
    unsigned int uncompressedSize;
  #endif
  unsigned long startOffset;
  #ifndef _XBOX
    unsigned long time;
  #endif
  unsigned long length;
  #if _ENABLE_PATCHING
    bool loadFromFile; // fast patching enabled
  #endif

  FileInfoO()
  {
    #if _ENABLE_PATCHING
    loadFromFile=false;
    #endif
  }

  const char *GetKey() const {return name;}
};

#include <Es/Memory/debugNew.hpp>


TypeIsMovable(FileInfoO);

typedef MapStringToClass<FileInfoO, AutoArray<FileInfoO> > FileBankType;

typedef void *WINHANDLE;

// interface for creating file log

class IBankLog: public RefCount
{
  public:
  virtual void Init(const char *bankName) = 0;
  virtual void LogFileOp(const char *name) = 0;
  virtual void Flush(const char *bankName) = 0;
};

class FileBufferMapped;

//! any bank may have several properties attached
struct QFProperty
{
  RString name;
  RString value;
};

TypeIsMovableZeroed(QFProperty)

//! virtual read-only file system
/*!
Access to files in QFBank is much faster and
they are stored more efficiently than when using OS services.
*/

class QFBank;

//! filebank compression/encryption interface
/*!
Instance of IFilebankEncryption already should contain any information
required to encrypt/decrypt data
*/

class IFilebankEncryption: public RefCount
{
  public:
#if _VBS2
  // should never be called, directly here!
  virtual bool Decode(char *dst,long uncompressedSize,QIStream &inBuf,unsigned char initVec[16] ){ DoAssert(false); return false; };
  virtual void Encode(QOStream &out, QIStream &in,unsigned char intVec[16]){ DoAssert(false); };
  virtual void Encode( QOStream &out, const char *dst, long lensb,unsigned char intVec[16] )
  {
    QIStrStream in(dst,lensb);
    Encode(out,in,intVec);
  }
#endif

  //! decode block of data
  /*!
  \param dst pointer to data destination
  \param lensb destination length
  \param in encoded data stored in stream
  */
  virtual bool Decode( char *dst, long lensb, QIStream &in ) = 0;
  //! encode block of data
  /*!
  \param out stream into which encoding is done
  \param dst pointer to source data
  \param lensb length of source data
  */
  virtual void Encode( QOStream &out, const char *dst, long lensb )
  {
    QIStrStream in(dst,lensb);
    Encode(out,in);
  }
  //! encode block of data
  /*!
  \param out stream into which encoding is done
  \param in input stream
  */
  virtual void Encode( QOStream &out, QIStream &in) = 0;
};

//! create encryptor that is able to encrypt/decrypt given bank
Ref<IFilebankEncryption> CreateFilebankEncryption(const char *name, const void *context);

//! register another encryptor
void RegisterFilebankEncryption
(
  const char *name, IFilebankEncryption *(*createFunction)(const void *context)
);

//! interface - using this interface you can quickly open file
class QFBankPointer: public RefCount
{
  public:
  virtual void open(QIFStreamB &f) = 0;
  virtual void open(QIFStreamB &f, QFileSize offset, QFileSize size) = 0;
  virtual RString GetDebugName() const = 0;
  virtual Ref<QFBankPointer> Partial(QFileSize offset, QFileSize size) const = 0;
};

//! encapsulation of Ref<QFBankPointer> - hiding implementation from the user

class QFBankHandle
{
  Ref<QFBankPointer> _ref;
  friend class QFBank;
  //friend class QIFStreamB;
  friend class QFBankQueryFunctions;

  QFBankHandle(QFBankPointer *ref):_ref(ref){}

  public:
  QFBankHandle(){}
  QFBankHandle(const QFBankHandle &src, QFileSize offset, QFileSize size)
  {
    _ref = src._ref->Partial(offset,size);
  }
  //! open, only part of the file used
  void open(QIFStreamB &f, QFileSize offset, QFileSize size) const
  {
    Assert(_ref);
    if (_ref) _ref->open(f,offset,size);
  }
  //! open, default file position
  void open(QIFStreamB &f) const
  {
    Assert(_ref);
    if (_ref) _ref->open(f);
  }
  bool IsNull() const {return _ref==NULL;}
  RString GetDebugName() const
  {
    return _ref ? _ref->GetDebugName() : "<null>";
  }
};


#define MT_SAFE 0
class QFBank
{
#if _VBS2 
  friend class FileBankManager;
#endif
  #if MT_SAFE
  mutable CriticalSection _lock;
  #endif
  #if !_RELASE
    mutable bool _serialize; // help finding bugs
  #endif
  Ref<IBankLog> _log;

  friend class QIFStream;
  private:

  // remember parameters necessary for opening
  //! name provided by open call
  RString _openName;
  //! callback provided by open call
  OpenCallback _openBeforeOpenCallback;
  //! context provided by open call
  Ref<BankContextBase> _openContext;

  //! time of last open - can be used for automated unloading of unused banks
  //DWORD _lastOpen;
  //! during some operations (file from bank being used) unload is not possible
  //int _openCount;
  // ! lock bank - it cannot be unloaded while locked
  mutable bool _locked;
  //! only lockable bank can be locked/unlocked
  bool _lockable;
  /// mark if the bank contain any config file
  bool _hasConfig;
#if _ENABLE_PATCHING
  /// some file is patched
  bool _patched;
#endif
  RString _prefix;
  FileBankType _files;
  AutoArray<QFProperty> _properties;

  //int _handle;
  Ref<IFileBuffer> _fileAccess;

  //! set true once Load was sucessfull
  bool _loaded;
  //! set true when attempt to open (DoOpen) failed
  bool _error;

  /// init file access
  bool InitFileAccess();
  /// load what needs to be loaded during open
  bool Init();
  void ScanPatchFiles(RString prefix, RString subdir);
  
  public:
  QFBank();
  ~QFBank();
  RString GetPrefix() const {return _prefix;}
  void SetPrefix(RString prefix);
  RString GetOpenName() const {return _openName;}
  bool HasConfig() const {return _hasConfig;}
  void SetHasConfig() {_hasConfig = true;}
  bool IsPatched() const
  {
#if _ENABLE_PATCHING
    return _patched;
#else
    return false;
#endif
  }
  /// access to properties
  int NProperties() const {return _properties.Size();}
  /// access to properties
  const QFProperty &GetProperty(int index) const {return _properties[index];}

  //! open bank from file
  bool open
    (
    RString name, OpenCallback beforeOpen=NULL,
    BankContextBase *context=NULL
    );
  //! open bank from memory
  bool open
  (
    IFileBuffer *buffer,
    OpenCallback beforeOpen=NULL, BankContextBase *context=NULL
  );
  //! open bank from file, not adding ".pbo" at the end of name
  bool openFromFile
    (
    RString name, OpenCallback beforeOpen=NULL,
    BankContextBase *context=NULL,
    QFileSize start=0, QFileSize size=0
    );
  //! load bank - physically performs action
  bool Load();
  //! open bank and mark is as locked (cannot be un-opened)
  void Lock() const;
  //! set lockable starus
  void SetLockable(bool lockable) {_lockable=lockable;}
  //! get lockable starus
  bool GetLockable() const {return _lockable;}
  //! mark bank as unlocked and unload if possible
  void Unlock() const;
  //! check if it can be unloaded
  bool IsLocked() const {return _locked;}
  //! check if it can be unloaded (if it is used, it cannot be unloaded)
  bool CanBeUnloaded() const;
  //! load bank - enable modification of necessary fields
  bool Load() const
  {
    return const_cast<QFBank *>(this)->Load();
  }
  /// no files from the bank should be flushed on alt-tab, no need to check for modification
  void MakeHandlePermanent();
  //! unload bank
  void Unload();
  //! unload bank - enable modification of necessary fields
  void Unload() const
  {
    if (!_loaded) return;
    const_cast<QFBank *>(this)->Unload();
  }
  void Clear(); // release all files
  void close() {Clear();}

  bool error() const;

  const RString &GetProperty(const RString &name) const;

  const FileInfoO &FindFileInfo( const char *name ) const; // seek to beginning of some file
  bool FileExists( const char *name ) const; // check if file exists
  bool Contains(const char *path) const; // check if file exists
  /// get OS timestamp of the pbo file
  QFileTime TimeStamp() const;
  // low level access to bank
  //int AlignedRead( char *buf, long size ) const; // read from bank
  //int AlignSize() const {return _align;}
  //int InSectorOffset() const {return _wantPos&(_align-1);}
  void Read( QFileSize startOffset, void *buf, long size, const char *name ) const; // read from bank

  //! prepare handle so that file can be opened quickly
  QFBankHandle GetHandle(const char *fullName, const char *filename, QFileSize offset, QFileSize size);

  //! read file - uncompress if necessary
  Ref<IFileBuffer> Read( const char *file, QFileSize offset=0, QFileSize size=QFileSizeMax ) const;
  //! get file order in the bank
  int GetFileOrder(const char *file);

  void ForEach
  (
    void (*Func)(const FileInfoO &fi, const FileBankType *files, void *context),
    void *context
  ) const; // call Func for all files
  static bool IsNull(const FileInfoO &value) {return FileBankType::IsNull(value);}
  static bool NotNull(const FileInfoO &value) {return FileBankType::NotNull(value);}
  static FileInfoO &Null() {return FileBankType::Null();}

  bool BufferOwned(const FileBufferMapped *buffer) const; // check if file belongs to this bank
  bool HandleOwned(const FileServerHandle &handle) const; // check if file belongs to this bank

  /// check which file resides on given offset
  RStringB FileOnOffset(QFileSize offsetBeg, QFileSize offsetEnd) const;

  /// check which file corresponds to given handle and offset
  static RString FileOnOffset(const FileServerHandle &handle, QFileSize offsetBeg, QFileSize offsetEnd);

  static bool FreeUnusedBanks(size_t sizeNeeded);

  /// number of files in bank
  int NFiles() const {return _files.NItems();}
  /// get the stored hash
  bool GetHash(Temp<char> &hash) const;
  /// get the hash of file list
  static int CmpFileNames(const RString *a, const RString *b)
  {
    return strcmp(*a,*b);
  }

  template <class Calculator>
  bool GetFileListHash(AutoArray<char> &hash) const
  {
    struct AddFileName
    {
      AutoArray<RString> &_fileNames;

      AddFileName(AutoArray<RString> &fileNames) :_fileNames(fileNames) {}    

      bool operator ()(const FileInfoO &info, const FileBankType *container)
      {
        if (info.length>0)
        {
          RString lowerName = info.name; lowerName.Lower();
          _fileNames.Add(lowerName);
        }
        return false;
      }
    };
    AutoArray<RString> fileNames;
    fileNames.Realloc(_files.NItems());
    _files.ForEachF(AddFileName(fileNames));
    // QSort
    QSort(fileNames.Data(), fileNames.Size(), QFBank::CmpFileNames);
    // hash it
    Calculator calculator;
    for (int i=0; i<fileNames.Size(); i++) calculator.Add(fileNames[i].Data(), fileNames[i].GetLength());
    return calculator.GetResult(hash);
  }
  /// calculate hash from data
  template <typename Calculator>
  bool CalculateHash(Calculator &calculator) const
  {
    int dataBeg, dataEnd;
    if (!FindData(dataBeg, dataEnd)) return false;

    QIStreamDataSource in;
    in.AttachSource(_fileAccess);
    int fileOld = in.tellg(); // store original position

    bool ok = _fileAccess->GetSize() - (dataEnd + sizeof(bool)) > 0;
    if (ok)
    {
      in.seekg(dataEnd);
      bool reversed;
      in.read(&reversed, sizeof(bool));
      if (reversed)
      {
        calculator.Init(dataBeg, dataEnd);
        in.seekg(0);
        in.Process(calculator);
        calculator.Init(0, dataBeg);
        in.seekg(0);
        in.Process(calculator);
      }
      else
      {
        calculator.Init(0, dataEnd);
        in.seekg(0);
        in.Process(calculator);
      }
    }

    in.seekg(fileOld); // move read pointer to original position
    return ok;
  }
  /// asynchronous calculation of hash - initialization
  template <typename Calculator>
  bool CalculateInit(Calculator &calculator) const
  {
    int dataBeg, dataEnd;
    if (!FindData(dataBeg, dataEnd)) return false;

    QIStreamDataSource in;
    in.AttachSource(_fileAccess);
    int fileOld = in.tellg(); // store original position

    bool ok = _fileAccess->GetSize() - (dataEnd + sizeof(bool)) > 0;
    if (ok)
    {
      in.seekg(dataEnd);
      bool reversed;
      in.read(&reversed, sizeof(bool));
      if (reversed)
      {
        calculator.AddRange(dataBeg, dataEnd);
        calculator.AddRange(0, dataBeg);
      }
      else
      {
        calculator.AddRange(0, dataEnd);
      }
    }

    in.seekg(fileOld); // move read pointer to original position
    return ok;
  }

  /// asynchronous calculation of hash - processing
  template <typename Calculator>
  bool CalculateProcess(Calculator &calculator) const
  {
    QIStreamDataSource in;
    in.AttachSource(_fileAccess);
    int fileOld = in.tellg(); // store original position
    bool done = calculator.Process(in);
    in.seekg(fileOld); // move read pointer to original position
    return done;
  }

protected:
  // implementation
  // search for data area in bank
  bool FindData(int &dataBeg, int &dataEnd) const;
};
TypeIsMovable(QFBank)

//! determine if files in the banks can be "patched"
extern bool GEnablePatching;
//! determine if file banks can be used
extern bool GUseFileBanks;
//! determine if file access should be logged
extern bool GLogFileOps;

#endif


