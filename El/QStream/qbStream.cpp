#include <El/elementpch.hpp>
#include "qbStream.hpp"
#ifdef _WIN32
  #include <direct.h>
  #include <io.h>
#endif
#include <Es/Strings/bString.hpp>
#include <Es/Files/fileContainer.hpp>

#include <Es/Common/win.h>

#if MT_SAFE
#define EXCLUSIVE() ScopeLockSection lock(_lock)
#else
#define EXCLUSIVE()
#endif

QFBank::QFBank()
{
  EXCLUSIVE();
  #if !_RELEASE
    _serialize=false;
  #endif
  _error = true; // no open called yet
  _locked = true;
  _loaded = false;
  _lockable = false;
  _hasConfig = false;
#if _ENABLE_PATCHING
  _patched = false;
#endif
}

#define WIN_DIR '\\'
#define UNIX_DIR '/'

#if !_RELEASE
#define BEG_SERIALIZE {Assert(!_serialize);_serialize=true;}
#define END_SERIALIZE {Assert(_serialize);_serialize=false;}
#else
#define BEG_SERIALIZE {}
#define END_SERIALIZE {}
#endif

#if USE_MAPPING
  #include "fileMapping.hpp"
#endif


#if defined _M_PPC 
// PowerPC is big endian
static int LoadInt(HANDLE f )
{
  Fail("Big endian from handle not implemented");
  int i = 0;
  DWORD rd = 0;
  ReadFile(f,&i,sizeof(i),&rd,NULL);
  if (rd!=sizeof(i)) i=0;
  return i;
}
#else
static int LoadInt(HANDLE f )
{
  int i = 0;
  DWORD rd = 0;
#if POSIX_FILES_COMMON
  ReadFilePosix(f,&i,sizeof(i),&rd,NULL);
#else
  ReadFile(f,&i,sizeof(i),&rd,NULL);
#endif
  if (rd!=sizeof(i)) i=0;
  return i;
}
#endif
static inline int LoadInt(QIStream &f)
{
  return f.getil();
}

// see also FileBank.cpp

static bool LoadFileInfo( FileInfoO &i, QIStream &f)
{
  // read zero terminated name
  char name[1024];
  char *n=name;
  int maxLen=sizeof(name)-1;
  for (int l=0; l<maxLen; l++)
  {
    int c = f.get();
    if (c<0)
    {
      // error during file reading
      i.name = "";
      i.startOffset = 0;
      i.length = 0;
      return false;
    }
    if (!c) break;
    *n++=c;
  }
  *n=0; // zero terminate in any case
  strlwr(name);
  i.name=name;
  #ifndef _XBOX
    int compressedMagic = i.compressedMagic = LoadInt(f);
    i.uncompressedSize = LoadInt(f);
  #else
    int compressedMagic = LoadInt(f);
    int uncompressedSize = LoadInt(f);
    if (compressedMagic==CompMagic || uncompressedSize!=0)
    {
      RptF("File compression used on %s",name);
      Fail("File compression not supported on Xbox");
      // pretend the file is not there and there are no more files
      i.name = "";
      i.startOffset = 0;
      i.length = 0;
      return false;
    }
  #endif
  i.startOffset = LoadInt(f);
  #ifndef _XBOX
    int time = i.time = LoadInt(f);
  #else
    int time = LoadInt(f); // skip timestamp on Xbox
  #endif
  i.length = LoadInt(f);

  return compressedMagic==VersionMagic && i.length==0 && time==0;
}

bool GLogFileOps=false;

// simple logging - log what is used, do not check for double files

class QFBankLog: public IBankLog
{
  FILE *_file;
  FindArray<RString> _files; // files aready there

  public:
  void Init(const char *bankName);
  void LogFileOp(const char *name);
  void Flush(const char *bankName);

  QFBankLog();
  ~QFBankLog();

  private:
  void Close();
};

//! create path to given directory / file
static void CreatePath(const char *path)
{
  // string will be changed temporary
  char *end = unconst_cast(path);
  if (end[0] != 0 && end[1] == ':' && end[2] == '\\') end += 3;
  while (end = strchr(end, '\\'))
  {
    *end = 0;
#ifdef _WIN32
    ::CreateDirectoryA(path, NULL);
#else
    ::CreateDirectory(path, NULL);
#endif
    *end = '\\';
    end++;
  }
}

/*!
\patch_internal 5095 Date 12/5/2006 by Jirka
- Fixed: option -logfiles fixed to support long prefixes
*/

void QFBankLog::Init(const char *bankName)
{
  Close();

  BString<1024> logName;
#ifdef _XBOX
  strcpy(logName,"d:\\FilesUsed");
#else
  strcpy(logName,"FilesUsed");
#endif
  strcat(logName,"\\");
  strcat(logName,bankName);
  if ( logName.BString<1024>::operator[](strlen(logName)-1)=='\\') logName.BString<1024>::operator[](strlen(logName)-1) = 0;
  strcat(logName,".log");

  ::CreatePath(logName);

#ifndef _WIN32
  logName.UnixPath();
#endif
  FILE *file = fopen(logName,"r");
  if (file)
  {
    // read filenames already there
    for(;;)
    {
      char buf[1024];
      *buf = 0;
      fgets(buf,sizeof(buf),file);
      if (!*buf) break;
      if (buf[strlen(buf)-1]=='\n') buf[strlen(buf)-1]=0;
      _files.Add(buf);
    }
    fclose(file),file = NULL;
  }
  _file = fopen(logName,"a+");
}

void QFBankLog::LogFileOp(const char *name)
{
  if (!_file) return;
  // check if file is already there
  if (_files.Find(name)>=0) return;
  _files.Add(name);
  fprintf(_file,"%s\n",name);

  fflush(_file);
}

void QFBankLog::Flush(const char *bankName)
{
  if (_file) fflush(_file);
}

QFBankLog::QFBankLog()
{
  _file = NULL;
}

void QFBankLog::Close()
{
  if (_file)
  {
    fclose(_file);
    _file = NULL;
  }
}

QFBankLog::~QFBankLog()
{
  Close();
}

RString LoadFromFile(QIStream &in)
{
  BString<1024> buf;
  for(;;)
  {
    char c[2];
    int c0 = in.get();
    if (c0==EOF) break;
    if (c0==0) break;
    c[0] = c0;
    c[1] = 0;
    buf += c;
  }
  return RString(buf);
}

bool StrIsLower(const char *str)
{
  if ( !str ) return true;
  do {
      if ( isupper(*str) ) return false;
  } while ( *(++str) );
  return true;
}

//bool SaveToFile(HANDLE file, RString value)
//{
//  int len = value.GetLength();
//  DWORD size = sizeof(len);
//  BOOL ok = WriteFile(file,&len,size,&size,NULL);
//  return ok!=FALSE;
//}
/*!
\param name filename of the bank
\param callback function called after header is loaded,
used to determine if bank should be loaded
\param context context passed to callback
\return true when bank was succesfully opened
Note: bank opening may be deferred.
Bank may only remmeber all necessary parameter when this function is called
and actual "opening" may be performed later. This should be almost transparent to user.
*/

bool QFBank::open
(
  RString name, OpenCallback beforeOpen, BankContextBase *context
)
{
  BString<1024> fullName;

  strcpy(fullName,name);
  strcat(fullName,".pbo");

  // check if the file exists
  if (!QIFileFunctions::FileExists(fullName))
#if _VBS2 //also accept .ebo files (encrypted pbo)
  {
    bool fileExists = false;
    strcpy(fullName,name);
    strcat(fullName,".ebo");
    if (QIFileFunctions::FileExists(fullName))
      fileExists = true;
    else
    {
      strcpy(fullName,name);
      strcat(fullName,".xbo");   
      if(QIFileFunctions::FileExists(fullName))
        fileExists = true;
    }
    if(!fileExists)
#endif
    {
      ErrorMessage("Cannot open file '%s'",(const char *)fullName);
#ifndef _WIN32
      if (!StrIsLower(cc_cast(fullName))) 
        ErrorMessage("The filename '%s' is not lowercase. Run ./install to convert it!", cc_cast(fullName));
#endif
      return false;
    }
#if _VBS2
  }
#endif

  _openName = fullName.cstr();

  _openBeforeOpenCallback = beforeOpen;
  _openContext = context;
  _files.Clear();
  _error = false; // no open called yet
  //_openCount = 0;
  return Init();
}

bool QFBank::openFromFile
(
 RString name, OpenCallback beforeOpen, BankContextBase *context, QFileSize start, QFileSize size
)
{
  // check if the file exists
  if (!QIFileFunctions::FileExists(name))
  {
    ErrorMessage("Cannot open file '%s'",cc_cast(name));
#ifndef _WIN32
    if (!StrIsLower(cc_cast(name))) 
      ErrorMessage("The filename '%s' is not lowercase. Run ./install to convert it!", cc_cast(name));
#endif
    return false;
  }

  _openName = name;

  _openBeforeOpenCallback = beforeOpen;
  _openContext = context;
  _files.Clear();
  _error = false; // no open called yet
  //_openCount = 0;
  if (size)
  {
    LogF("Load bank %s",cc_cast(name));
    Ref<FileBufferLoading> wholeBuf = new FileBufferLoading(cc_cast(name));
    _fileAccess = new FileBufferSub(wholeBuf, start, size);
    if (_fileAccess->GetError())
    {
      ErrorMessage("Error opening bank %s",cc_cast(name));
      _error = true;
      return false;
    }
  }
  return Init();
}

bool QFBank::open
(
 IFileBuffer *buffer,
 OpenCallback beforeOpen, BankContextBase *context
)
{
  _openBeforeOpenCallback = beforeOpen;
  _openContext = context;
  _fileAccess = buffer;
  _files.Clear();
  _error = false; // no open called yet
  return Init();
}

bool QFBank::InitFileAccess()
{
  if (_fileAccess) return true;
  LogF("Load bank %s",(const char *)_openName);

#ifdef _XBOX
  const char *FullXBoxName(const char *name, char *temp);
  char temp[1024];
  RString openNameF = FullXBoxName(_openName,temp);
#else
  RString openNameF = _openName;
#endif

  // get file mapping access
#if USE_MAPPING
  if (GUseFileMapping)
  {
Retry:
    // do not map headers, only file content
    // open OS specific

    _fileAccess = new FileBufferMapped(openNameF);
    if (_fileAccess->GetError())
    {
      // try to close any bank that may be closed
      _fileAccess.Free();

      int size = QIFileFunctions::GetFileSize(openNameF);
      bool ok = FreeUnusedBanks(size);
      if (ok) goto Retry;
      // nothing can be freed now, we have to terminate
      ErrorMessage("Cannot memory-map file '%s'",(const char *)openNameF);
      _error = true;
      return false;
    }
  }
  else
#endif
  {
    // map whole file
    _fileAccess = new FileBufferLoading(openNameF);
    if (_fileAccess->GetError())
    {
      // TODO: localize
      ErrorMessage("Error opening bank %s",(const char *)openNameF);
      _error = true;
      return false;
    }
  }
  return true;
}

bool QFBank::Init()
{
  if (!InitFileAccess())
  {
    return false;
  }

  // open a stream to get headers
  QIStreamDataSource in;
  in.AttachSource(_fileAccess);

  // first count how many file infos we have, and count start offset
  int startPos = in.tellg();
  FileInfoO info;
  bool version = LoadFileInfo(info,in);
  if( info.name.GetLength()>0 )
  {
    return true; // no properties
  }

  // if bank is empty, it may be "new bank" with product identification
  // check if there is normal terminator, or special
  if (version)
  {
    // read properties
    for(;;)
    {
      RString name = LoadFromFile(in);
      if (name.GetLength()==0)
      {
        break;
      }
      RString value = LoadFromFile(in);
      // some properties are handled differently
      if (!strcmpi(name,"prefix"))
      {
        if (_prefix.GetLength()==0)
        {
          SetPrefix(value+"\\");
        }
      }
      else
      {
        QFProperty &prop = _properties.Append();
        prop.name = name;
        prop.value = value;
      }
    }
  }

  in.seekg(startPos);
  return true;
}

/*!
All banks are locked by default. If application bank can be unloaded, it must unlock it.
*/

void QFBank::Lock() const
{
  _locked = true;
  Load();
}

void QFBank::Unlock() const
{
  _locked = false;
  if (_fileAccess && _fileAccess->RefCounter()==1)
  {
    Unload();
  }
}

bool QFBank::CanBeUnloaded() const
{
  return _fileAccess && _fileAccess->RefCounter()==1 && _loaded;
}

static inline RStringB ConvertDirSlash(RStringB name)
{
  const char *change=strchr(name,UNIX_DIR);
  if (!change) return name;
  // make sure name is mutable
  RString mutableName = name;
  char *mutName = mutableName.MutableData();
  for(;;)
  {
    char *change=strchr(mutName,UNIX_DIR);
    if (!change) break;
    *change=WIN_DIR;
  }
  return mutableName;
}

bool QFBank::Load()
{
  if (!_locked)
  {
    RptF("Cannot open bank %s that is not locked",(const char *)_prefix);
    return false;
  }
  if (_error) return false;
  // TODO: implement some timing. GlobalTickCount is not available here.
  //_lastOpen = GlobalTickCount();
  if (_loaded) return true;
  _files.Clear();
  EXCLUSIVE();
  // note: name should not contain extension
  // automatic optimal bank type is performed with different extensions
  // like .pbf and .pbo

  if (!InitFileAccess())
  {
    return false;
  }
  // open a stream to get headers
  QIStreamDataSource in;
  in.AttachSource(_fileAccess);

  int fileCount = 0;
  FileInfoO info;

  // first count how many file infos we have, and count start offset
  int startPos = in.tellg();
  for(;;)
  {
    LoadFileInfo(info,in);
    if( info.name.GetLength()<=0 ) break;
    fileCount++;
  }
#if !_VBS2_LITE
  _files.Reserve(fileCount);
#endif

  int startOffset=0;
  in.seekg(startPos);
  bool lastIsVersion = false;
  for(;;)
  {
    // from optimized bank different loading
    lastIsVersion = LoadFileInfo(info,in);
    
    info.startOffset = startOffset;
    startOffset += info.length;
    if( info.name.GetLength()<=0 ) break;

    info.name = ConvertDirSlash(info.name);
    
#if !_VBS2_LITE
    _files.Add(info);
#endif
  }
  // if bank is empty, it may be "new bank" with product identification
  // check if there is normal terminator, or special
  if (_files.NItems()==0 && lastIsVersion)
  {
    // read properties
    for(;;)
    {
      RString name = LoadFromFile(in);
      if (name.GetLength()==0)
      {
        break;
      }
      RString value = LoadFromFile(in);
      // properties are already read during open/Init
      // we can skip them now
    }

    // call callback function
    if(_openBeforeOpenCallback)
    {
      bool ok = _openBeforeOpenCallback(this,_openContext);
      if (!ok)
      {
        _error = true;
        return false;
      }
    }

    // read normal file headers
    RString encryption = GetProperty("encryption");
    if (encryption.GetLength()>0)
    {
      // we need to load encrypted headers
      // for this we need to know headers encrypted size
      // load decoded size
      int headersSize = LoadInt(in);
      int headersEncodedSize = LoadInt(in);
      // read encoded headers into memory
      Temp<char> headers(headersEncodedSize);
      int rd = in.read(headers.Data(),headers.Size());
      if (rd==headers.Size())
      {
        Ref<IFilebankEncryption> ss = CreateFilebankEncryption(encryption,NULL);
        if (ss)
        {
          QIStrStream headersEncoded(headers.Data(),headers.Size());
          Temp<char> headerDecodedData(headersSize);
#if _VBS2 
          if( strcmpi((const char*) encryption,"vbs2")==0 ||
              strcmpi((const char*) encryption,"vbs2m")==0 )
          {
            // read in 16 bytes from the header, this should contain the files initvec
            unsigned char inVec[16] = { 
              0x4b,0x64,0x6b,0x41,0x4c,0x40,0x61,0x4f,
              0x6f,0x79,0x30,0x3a,0x7b,0x65,0x20,0x6c 
            };
            
            int startPos = in.tellg();
            int invcHeader = in.read(inVec,16);
            in.seekg(startPos); // move back to previous position

            if(invcHeader != 16)
            {
              LogF("Failed to get invector on bank:%s", (const char*)_openName );
              _files.Clear();
              _error = true;
              return false;
            }
            else
              ss->Decode(headerDecodedData.Data(),headerDecodedData.Size(),headersEncoded,inVec);
          }
          else
          ss->Decode(headerDecodedData.Data(),headerDecodedData.Size(),headersEncoded);
#else
          ss->Decode(headerDecodedData.Data(),headerDecodedData.Size(),headersEncoded);
#endif
          QIStrStream headersDecoded(headerDecodedData.Data(),headerDecodedData.Size());
          for(;;)
          {
            // from optimized bank different loading
            LoadFileInfo(info,headersDecoded);
            
            info.startOffset = startOffset;
            startOffset+=info.length;
            if( info.name.GetLength()<=0 ) break;

            info.name = ConvertDirSlash(info.name);
            _files.Add(info);
          }
        }
      }
    }
    else
    {
#if _VBS2_LITE
      ErrorMessage("Cannot load file:%s",(const char *)_prefix);
      _files.Clear();
      _error = true;
      return false;
#else
      int fileCount = 0;
      int startPos = in.tellg();
      for(;;)
      {
        // from optimized bank different loading
        LoadFileInfo(info,in);
        
        if( info.name.GetLength()<=0 ) break;
        fileCount++;
      }
      _files.Reserve(fileCount);
      in.seekg(startPos);
      for(;;)
      {
        // from optimized bank different loading
        LoadFileInfo(info,in);
        
        info.startOffset = startOffset;
        startOffset+=info.length;
        if( info.name.GetLength()<=0 ) break;

        info.name = ConvertDirSlash(info.name);
        _files.Add(info);
      }
#endif
    }
  }
  else
  {
    // call callback function
    if(_openBeforeOpenCallback)
    {
      bool ok = _openBeforeOpenCallback(this,_openContext);
      if (!ok)
      {
        _files.Clear();
        _error = true;
        return false;
      }
    }
  }


  int headerSize = in.tellg();
  BEG_SERIALIZE
  // filemapping uses offset from end of header
  // direct access uses file offset
  if (_files.NItems() > 0)
  {
    // !!! avoid GetTable when NItems == 0
    for (int i=0; i<_files.NTables(); i++)
    {
      AutoArray<FileInfoO> &container = _files.GetTable(i);
      for (int j=0; j<container.Size(); j++)
      {
        FileInfoO &info = container[j];
        info.startOffset += headerSize;
      }
      //container.Compact();
    }
  }
  startOffset+=headerSize;
  // check integrity - try to seek end of file
  in.seekg(startOffset);
  int checkEof = in.tellg();
  if( checkEof!=startOffset )
  {
    ErrorMessage("Data file too short '%s'. Expected %d B, got %d B",(const char *)_openName,startOffset,checkEof);
  }

  #if _ENABLE_PATCHING
  if (GEnablePatching)
  {
    ScanPatchFiles(_prefix,RString());
  }
  #endif

  _files.Compact();

  _loaded = true;
  END_SERIALIZE
  return true;
}

void QFBank::MakeHandlePermanent()
{
  if (_fileAccess)
  {
    FileServerHandle handle = _fileAccess->GetHandle();
    GFileServerFunctions->MakeReadHandlePermanent(handle);
  }
  else
  {
    LogF("Permanent ignored for %s, not loaded",cc_cast(_openName));
  }
}

void QFBank::Unload()
{
  // if error, there is nothing to undo
  if (_error) return;
  // if there is no handle, there is nothing to undo
  if (!_loaded) return;
  // unload this bank
  if (_fileAccess && _fileAccess->RefCounter()>1)
  {
    // if some file from bank is still used, we cannot unload it
    LogF
    (
      "Cannot unload bank %s, %d files are still open",
      (const char *)_openName,_fileAccess->RefCounter()-1
    );
    return;
  }
  LogF("Unload bank %s",(const char *)_openName);
  _fileAccess.Free();
  _loaded = false;
  Clear();
}

bool QFBank::error() const
{
  // if bank was not opened yet, it cannot have any fatal errors
  if (!_error) return false;
  // if there is no handle, there is some fatal error
  if (!_loaded) return true;
  return false;
}

const RString &QFBank::GetProperty(const RString &name) const
{
  for (int i=0; i<_properties.Size(); i++)
  {
    if (!strcmpi(_properties[i].name,name))
    {
      return _properties[i].value;
    }
  }
  const static RString empty;
  return empty;
}

void QFBank::ScanPatchFiles(RString prefix, RString subdir)
{
#if _ENABLE_PATCHING
  // check if there is folder containing patch files
  BString<1024> wildname;
  strcpy(wildname,prefix);
  //TerminateBy(wildname,"\\");
  strcat(wildname,subdir);

  if (prefix.GetLength()<=0)
  {
    // patching bank with no prefix is nonsense
    // most likely cause is temporary bank (like in single missions)
    return;
  } 
#ifdef _WIN32

  strcat(wildname,"*.*");
  _finddata_t find;
  long hf = X_findfirst(wildname,&find);  // not used on Xbox
  if (hf!=-1)
  {
    do
    {
      BString<1024> lowName;
      strcpy(lowName,find.name);
      strlwr(lowName);
      RString name = lowName;
      //LogF("Checking file %s",find.name);
      if (find.attrib&_A_SUBDIR)
      {
        if (!strcmp(find.name,".") || !strcmp(find.name,"..")) continue;
        ScanPatchFiles(prefix,subdir + name + RString("\\"));
      }
      else
      {
        RString subname = subdir+name;
        const FileInfoO &file = _files[subname];
        if (_files.NotNull(file))
        {
          LogF("Plain file version of %s used",(const char *)(subname));
          FileInfoO fileSet = file;
          fileSet.loadFromFile = true;
          _files.Add(fileSet);
          _patched = true;
        }
      }


    } while (_findnext(hf,&find)==0);
    
    _findclose(hf);
  }

#else

    wildname.UnixPath();
    int len = strlen(wildname);
    if ( len > 0 && wildname[len-1] == UNIX_DIR )
        wildname[--len] = (char)0;
    DIR *dir = OpenDir(wildname);
    if ( !dir ) return;
    struct dirent *entry;
    while ( (entry = readdir(dir)) ) {  // process one directory entry
        if ( entry->d_name[0] == '.' &&
             (!entry->d_name[1] ||
              entry->d_name[1] == '.' &&
              !entry->d_name[2]) )
            continue;
        // stat the entry <= subdirectories must be handled differently
        wildname += "/";
        wildname += entry->d_name;
        struct stat st;
        if ( !stat(wildname,&st) ) {
            if ( S_ISDIR(st.st_mode) ) {// directory
                ScanPatchFiles(prefix,subdir + entry->d_name);
                }
            else {                      // regular file
                RString subname = subdir + entry->d_name;
                const FileInfoO &file = _files[subname];
                if (_files.NotNull(file)) {
                    LogF("Plain file version of %s used",(const char *)(subname));
                    FileInfoO fileSet = file;
                    fileSet.loadFromFile = true;
                    _files.Add(fileSet);
                    _patched = true;
                    }
                }
            }
        wildname[len] = (char)0;
        }
    closedir(dir);

#endif

#endif
}

void QFBank::SetPrefix(RString prefix)
{
  _prefix = prefix;
  // create log
  if (GLogFileOps)
  {
    _log = new QFBankLog();
    _log->Init(prefix);
  }

}

const FileInfoO &QFBank::FindFileInfo( const char *name ) const // check if file exists
{
  EXCLUSIVE();
  BString<1024> lowName;
  strcpy(lowName,name);
  strlwr(lowName);
  return _files[lowName];
}

QFileTime QFBank::TimeStamp() const
{
  if (!_fileAccess) return 0;
  return GFileServerFunctions->TimeStamp(_fileAccess->GetHandle());
}

bool QFBank::FileExists( const char *name ) const // check if file exists
{
  if (!Load()) return false;
  const FileInfoO &info = FindFileInfo(name);
  return NotNull(info);
}

bool QFBank::Contains(const char *path) const
{
  int n = GetPrefix().GetLength();
  if (strnicmp(path, GetPrefix(), n) != 0) return false;
  return FileExists(path + n);
}

void QFBank::Read( QFileSize offset, void *buf, long size, const char *name ) const
{
  Assert(!_error);
  BEG_SERIALIZE
  EXCLUSIVE();  // seek to wanted position
  Log("Do not use - slow");
  QIFStream in;
  in.open(_openName);
  in.seekg(offset);
  int rd = in.read(buf,size);
  if (rd!=size)
  {
    ErrorMessage("Data file read error (%s, %s).",name,(const char *)_openName);
  }
  END_SERIALIZE
}

#include "fileCompress.hpp"

Ref<IFileBuffer> GetFileBufferMappedPart(const char *fullName, QFileSize offset, QFileSize size);

/*!
Function result can be used to determine relative order of two files in bank.
When two files return same result, their order is unknown.
When one file returns higher smaller than another one,
it is nearer the beginning of the bank.

Note: Function results need not be continuous for any bank. When i is returned
for some file, there is no guranntee i+1 or i-1 will be returned for any other file.

The purpose of this function is to give anyone loading many files opportunity
to optimize access order to achieve as sequential access as possible.
*/

int QFBank::GetFileOrder(const char *file)
{
  const FileInfoO &info = FindFileInfo(file);
  if (IsNull(info)) return 0;
  return info.startOffset;
}

//! anything can be opened based on path
class QFBankPointerToAny: public QFBankPointer
{
  RString _path;
  QFileSize _offset;
  QFileSize _size;

  public:
  QFBankPointerToAny(const RString &path, QFileSize offset, QFileSize size);
  void open(QIFStreamB &f, QFileSize offset, QFileSize size);
  void open(QIFStreamB &f);
  RString GetDebugName() const {return _path;}
  Ref<QFBankPointer> Partial(QFileSize offset, QFileSize size) const;
};

QFBankPointerToAny::QFBankPointerToAny(const RString &path, QFileSize offset, QFileSize size)
:_path(path)
{
  _offset = offset;
  _size = size;
}

Ref<QFBankPointer> QFBankPointerToAny::Partial(QFileSize offset, QFileSize size) const
{
  if (offset>_size) offset = _size;
  QFileSize maxSize = _size-offset;
  if (size>maxSize) size = maxSize;
  return new QFBankPointerToAny(_path,_offset+offset,size);
}

void QFBankPointerToAny::open(QIFStreamB &f, QFileSize offset, QFileSize size)
{
  Fail("Not tested");
  QFileSize maxSize = _size-offset;
  if (size>maxSize) size = maxSize;
  f.AutoOpen(_path,offset+offset,size);
}

void QFBankPointerToAny::open(QIFStreamB &f)
{
  f.AutoOpen(_path,_offset,_size);
}

QFBankHandle QFBank::GetHandle(const char *fullName, const char *filename, QFileSize offset, QFileSize size)
{
  // handle must include
  // bank identification 
  // file identification
  return new QFBankPointerToAny(fullName,offset,size);
}

/*!
  \patch 1.05 Date 07/17/2001 by Ondra
  - Improved: Better diagnostics of corrupted datafiles.
*/

Ref<IFileBuffer> QFBank::Read( const char *name, QFileSize offset, QFileSize size ) const
{
  if (!Load()) return NULL;
  // log information about file being opened
  // even when operation is not sucessful, file is logged
  if (_log)
  {
    // log name
    _log->LogFileOp(name);
  }

  const FileInfoO &info = FindFileInfo(name);
  if (IsNull(info))
  {
    return NULL;
  }
  #if _ENABLE_PATCHING
  if (info.loadFromFile)
  {
    // patch file provided - use it
    BString<1024> fullName;
    strcpy(fullName,GetPrefix());
    strcat(fullName,name);
#ifdef _XBOX  
    char temp[1024];
    fullName = FullXBoxName(fullName, temp);
#endif
    #if USE_MAPPING
    if (GUseFileMapping)
    {
      Ref<IFileBuffer> data = GetFileBufferMappedPart(fullName,offset,size);
      if (!data->GetError()) return data.GetRef();
    }
    else
    #endif
    {
      Ref<IFileBuffer> whole = new FileBufferLoading(fullName);
      if (!whole->GetError())
      {
        QFileSize wholeSize = whole->GetSize();
        if (offset>0 || size<wholeSize)
        {
          return new FileBufferSub(whole,offset,size);
        }
        else
        {
          return whole;
        }
      }
    }
  }
  #endif

  #ifndef _XBOX
  if( info.compressedMagic==CompMagic ) // some compression
  {
    QIStrStream inBuf;
    // read compressed data into temporary buffer
    Ref<QIStreamBuffer> cData = new QIStreamBuffer(info.length);

    //Temp<char> cData(info.length);
    Read(info.startOffset,cData->DataLock(0,info.length),info.length,name);
    cData->DataUnlock(0,info.length);
    inBuf.init(cData);
    // uncompress
    Ref<FileBufferMemory> data=new FileBufferMemory(info.uncompressedSize);
    SSCompress ss;
    if (!ss.Decode(data->GetWritableData(),info.uncompressedSize,inBuf))
    {
      RptF("Error decoding %s from %s",name,(const char *)_prefix);
      return NULL;
    }
    if (offset>0 || size<info.length)
    {
      return new FileBufferSub(data,offset,size);
    }
    return data.GetRef();
  }
  else if (info.compressedMagic==EncrMagic)
  {
#if _VBS2
      RString encypt = GetProperty("encryption");
      if( strcmpi((const char*)encypt,"VBS2")==0 ||
          strcmpi((const char*)encypt,"VBS2M")==0 )
      {
        if (size>info.length-offset) 
          size = info.length-offset;
        
        // check to see if offset is moduls of 16.
        QFileSize nOffset = offset;
        QFileSize nEndPos = nOffset + size;
        
        // grab the inital inVec from the file header information
        unsigned char inVec[16] = { 
          0x4b,0x64,0x6b,0x41,0x4c,0x40,0x61,0x4f,
          0x6f,0x79,0x30,0x3a,0x7b,0x65,0x20,0x6c 
        };

        // Load the inilisation vector
        int *ptr = (int*)inVec;
        for( int x = 0; x < 16; x += sizeof(int),++ptr)
          *ptr = info.uncompressedSize; // keep appending length to InVec

        // move back to the 16 byte boundary
        int remainder = nOffset % 16;
        nOffset -= remainder;

        // Read in the inVector, previous data chunk
        if(nOffset>0)
          Read(info.startOffset+(nOffset-16),inVec,16,name);

        // check that end poisition is a modules of 16
        remainder = nEndPos % 16;
        if(remainder>0)
          nEndPos += 16 - remainder; // move forward

        QFileSize newSize = nEndPos-nOffset;
        QIStrStream inBuf;
        Ref<QIStreamBuffer> cData = new QIStreamBuffer(newSize);

        Read(info.startOffset+nOffset,cData->DataLock(0,newSize),newSize,name);
        cData->DataUnlock(0,newSize);
        inBuf.init(cData);

        Ref<FileBufferMemory> data=new FileBufferMemory(newSize); 
        Ref<IFilebankEncryption> ss = CreateFilebankEncryption(encypt,NULL);     
        if (!ss || !ss->Decode(data->GetWritableData(),newSize,inBuf,inVec))
        {
          RptF("Error decoding %s from %s",name,(const char *)_prefix);
          return NULL;
        }
        
        if (offset > 0 || size < info.length)
        {
          QFileSize moveToOffset = offset - nOffset;
          return new FileBufferSub(data,moveToOffset,newSize);
        }
        
        return data.GetRef();
      }
#endif 
    QIStrStream inBuf;
    // read compressed data into temporary buffer
    Ref<QIStreamBuffer> cData = new QIStreamBuffer(info.length);

    Read(info.startOffset,cData->DataLock(0,info.length),info.length,name);
    cData->DataUnlock(0,info.length);

    inBuf.init(cData);
    // ask compression manager to create encryptor/decryptor
    // uncompress
    Ref<FileBufferMemory> data=new FileBufferMemory(info.uncompressedSize);
    Ref<IFilebankEncryption> ss = CreateFilebankEncryption(GetProperty("encryption"),NULL);
    if (!ss || !ss->Decode(data->GetWritableData(),info.uncompressedSize,inBuf))
    {
      RptF("Error decoding %s from %s",name,(const char *)_prefix);
      return NULL;
    }
    if (offset>0 || size<info.length)
    {
      return new FileBufferSub(data,offset,size);
    }
    return data.GetRef();
  }
  else if (info.compressedMagic==0)
  #endif
  {
    // no compression
    if (size>info.length-offset) size = info.length-offset;


    Ref<IFileBuffer> data = new FileBufferSub
    (
      _fileAccess,info.startOffset+offset,size
    );
    return data;
  }
  #ifndef _XBOX
  else
  {
    // some unknown compression manager
    Fail("Unknown compression manager");
    return NULL;
  }
  #endif
}

#if USE_MAPPING
bool QFBank::BufferOwned(const FileBufferMapped *buffer) const
{
  return (const IFileBuffer *)_fileAccess.GetRef() == buffer;
  //return buffer->GetFileHandle()==(HANDLE)_handle;
}
#endif

bool QFBank::HandleOwned(const FileServerHandle &handle) const
{
  if (!_fileAccess) return false;
  return handle == _fileAccess->GetHandle();
}

//typedef MapStringToClass<FileInfoO, AutoArray<FileInfoO> > FileBankType;
//ForEach(void (*Func)(const Type &, const MapStringToClass *, void *), void *context=NULL) const;

void QFBank::ForEach
(
  void (*Func)(const FileInfoO &fi, const FileBankType *files, void *context),
  void *context
) const
{
  if (!Load()) return;
  // call Func for all files
  EXCLUSIVE();
  _files.ForEach(Func,context);
}

struct CheckOffsetContext
{
  RStringB name;
  QFileSize offsetBeg,offsetEnd;
  
};
static void CheckOffset(const FileInfoO &fi, const FileBankType *files, void *context)
{
  CheckOffsetContext *ctx = (CheckOffsetContext *)context;
  // check if offset interval intersection is non empry
  unsigned int fiEnd = fi.startOffset+fi.length;
  if (ctx->offsetBeg>=fiEnd) return;
  if (ctx->offsetEnd<=fi.startOffset) return;
  if (ctx->name.GetLength()>0)
  {
    ctx->name = ctx->name+","+fi.name;
  }
  else
  {
    ctx->name = fi.name;
  }
}

RStringB QFBank::FileOnOffset(QFileSize offsetBeg,QFileSize offsetEnd) const
{
  if (!_loaded) return RStringB();
  CheckOffsetContext ctx;
  ctx.offsetBeg = offsetBeg;
  ctx.offsetEnd = offsetEnd;
  ForEach(CheckOffset,&ctx);
  return ctx.name;
}

RString QFBank::FileOnOffset(
  const FileServerHandle &handle, QFileSize offsetBeg, QFileSize offsetEnd
)
{
  for (int i=0; i<GFileBanks.Size(); i++)
  {
    if (GFileBanks[i].HandleOwned(handle))
    {
      return GFileBanks[i].FileOnOffset(offsetBeg,offsetEnd);
    }
  }
  return RString();
  
}

void QFBank::Clear()
{
  EXCLUSIVE();
  // clear variables that are not part of Global structure
  _files.Clear();
  _fileAccess.Free();
}

QFBank::~QFBank()
{
  EXCLUSIVE();
  Clear();
}

void QIFStreamB::open( const QFBank &bank, const char *name, QFileSize offset, QFileSize size )
{
  _error=LSUnknownError;
  InvalidateBuffer();
  _buf._start = 0;
  _readFromBuf = 0;
  Ref<IFileBuffer> data=bank.Read(name,offset,size);
  if( !data )
  {
    // create an empty filebuffer (always reporting an error)
    AttachSource(new FileBufferError);
    _fail = true;
    _bank = NULL;
    return;
  }
  AttachSource(data);
  _bank = &bank;
  _fail = false;
  _eof = false;
}

BankList GFileBanks;
bool GUseFileBanks;

#if _ENABLE_PATCHING
bool GEnablePatching = true;
#endif

bool QFBank::FreeUnusedBanks(size_t sizeNeeded)
{
  return GFileBanks.UnloadUnused();
}

struct FindDataContext
{
  QFileSize offsetBeg;
  QFileSize offsetEnd;
  FindDataContext()
  {
    offsetBeg = INT_MAX;
    offsetEnd = 0;
  }
};

static void FindData(const FileInfoO &fi, const FileBankType *files, void *context)
{
  FindDataContext *ctx = (FindDataContext *)context;

  if (fi.startOffset < ctx->offsetBeg) ctx->offsetBeg = fi.startOffset;
  if (fi.startOffset + fi.length > ctx->offsetEnd) ctx->offsetEnd = fi.startOffset + fi.length;
}

bool QFBank::FindData(int &dataBeg, int &dataEnd) const
{
  // find data start and end
  FindDataContext ctx;
  ForEach(::FindData, &ctx);
  dataBeg = ctx.offsetBeg;
  dataEnd = ctx.offsetEnd;
  return dataEnd > 0;
}

bool QFBank::GetHash(Temp<char> &hash) const
{
  if (!_loaded) return false;

  int dataBeg, dataEnd;
  if (!FindData(dataBeg, dataEnd)) return false;

  QIStreamDataSource in;
  in.AttachSource(_fileAccess);
  int fileOld = in.tellg(); // store original position

  int size = _fileAccess->GetSize() - dataEnd - sizeof(bool);

  if (size > 0)
  {
    hash.Realloc(size);
    in.seekg(dataEnd + sizeof(bool));
    in.read(hash.Data(), size);
  }

  in.seekg(fileOld); // move read pointer to original position

  return size > 0;
}

#ifdef _WIN32

FindBank::FindBank()
{
  _handle = -1;
}

FindBank::~FindBank()
{
  Close();
}

bool FindBank::First(const char *path)
{
  #ifdef _XBOX
    if (IsPathAbsolute(path)) // full path
    {
      DoAssert(path[0] != '#');
      strcpy(_wild,path);
    }
    else
    {
      strcpy(_wild,"d:\\");
      strcat(_wild,path);
    }
  #else
    strcpy(_wild,path);
  #endif
#if _VBS2
  strcat(_wild,"\\*.*bo");
#else
  strcat(_wild,"\\*.pbo");
#endif
  _info = new _finddata_t;
  _handle = _findfirst(_wild, (_finddata_t *)_info);
  return _handle != -1;
}

bool FindBank::Next()
{
  if (!_info || _handle==-1) return false;
  return _findnext(_handle, (_finddata_t *)_info)==0;
}

void FindBank::Close()
{
  if (_info)
  {
    delete (_finddata_t *)_info;
    _info = NULL;
  }
  if (_handle!=-1)
  {
    _findclose(_handle);
    _handle = -1;
  }
}

const char *FindBank::GetName() const
{
  if (!_info) return "";
  return ((_finddata_t *)_info)->name;
}

#else

FindBank::FindBank()
{
  dir = NULL;
  entry = NULL;
}

FindBank::~FindBank()
{
  Close();
}

bool FindBank::First(const char *path)
{
    Close();
    LocalPath(dirName,path);
    dir = OpenDir(dirName);
    return Next();
}

bool FindBank::Next()
{
    if ( !dir ) return false;
    while ( (entry = readdir(dir)) ) {
        int len = strlen(entry->d_name);
        if ( len <= 4 ) continue;
        if ( !strcmp(entry->d_name+len-4,".pbo") )
            return true;
        }
    closedir(dir);
    dir = NULL;
    return false;
}

void FindBank::Close()
{
    if ( dir ) {
        closedir(dir);
        dir = NULL;
        entry = NULL;
        }
}

const char *FindBank::GetName() const
{
    if ( !dir || !entry ) return "";
  return entry->d_name;
}

#endif

/*!
  @return real bank prefix
  Note: callback may be called even after Load returns
  Context is stored meanwhile inside the bank.
*/

RString BankList::Load
(
  const RString &path,
  const RString &bankPrefix,const RString &bName,
  bool emptyPrefix,
  OpenCallback beforeOpen, OpenCallback afterOpen, BankContextBase *context
)
{
  int oldSize = Size();
  int index = Add();
  QFBank &bank = Set(index);
  // path can differ from bankPrefix (for example if Mod is used)
  if (!bank.open(path+bName,beforeOpen,context))
  {
    Resize(oldSize);
    return RString();
  }
  Log("Open bank %s",(const char *)(bankPrefix+bName));

  if (bank.error())
  {
    Resize(oldSize);
    return RString();
  }

  if (bank.GetPrefix().GetLength()==0)
  {
    // if no prefix is stored in the bank, use automatic prefixing
    RString prefix;
    if (emptyPrefix) prefix = bName;
    else prefix = bankPrefix+bName;

    RString prefixPath = prefix+"\\";
    bank.SetPrefix(prefixPath);
  }

  if (afterOpen && !afterOpen(&bank,context))
  {
    Resize(oldSize);
    return RString();
  }
  return bank.GetPrefix();
}

void BankList::Unload
(
  const RString & bankPrefix,const RString &bName,
  bool emptyPrefix
)
{
  // find corresponding bank
  RString prefix;
  if (emptyPrefix) prefix = bName;
  else prefix = bankPrefix+bName;

  RString prefixPath = prefix+"\\";

  for (int i=0; i<Size(); i++)
  {
    const QFBank &b = Get(i);
    if(b.GetPrefix()==prefixPath)
    {
      Delete(i);
      return;
    }
  }
}

void BankList::Lock(const RString &prefix)
{
  for (int i=0; i<Size(); i++)
  {
    const QFBank &b = Get(i);
    if(b.GetPrefix()==prefix)
    {
      b.Lock();
      return;
    }
  }
  LogF("Lock: Bank %s not found",(const char *)prefix);
}
void BankList::Unlock(const RString &prefix)
{
  for (int i=0; i<Size(); i++)
  {
    const QFBank &b = Get(i);
    if(b.GetPrefix()==prefix)
    {
      b.Unlock();
      return;
    }
  }
  LogF("Unlock: Bank %s not found",(const char *)prefix);
}

void BankList::SetLockable(const RString &prefix, bool lockable)
{
  for (int i=0; i<Size(); i++)
  {
    QFBank &b = Set(i);
    if(b.GetPrefix()==prefix)
    {
      b.SetLockable(lockable);
      return;
    }
  }
  LogF("MakeLockable: Bank %s not found",(const char *)prefix);
}

/*!
\param before unload all banks that have been last used before this time
*/

bool BankList::UnloadUnused()
{
  // if it is not locked, is is probably already unloaded
  // but we still may want to try it first
  for (int i=0; i<GFileBanks.Size(); i++)
  {
    QFBank &bank = GFileBanks[i];
    if (bank.IsLocked()) continue;
    if (!bank.CanBeUnloaded()) continue;
    LogF("Unloading bank %s",(const char *)bank.GetPrefix());
    bank.Unload();
    return true;
  }
  // if no unlocked bank is available for unloading, try locked banks
  for (int i=0; i<GFileBanks.Size(); i++)
  {
    QFBank &bank = GFileBanks[i];
    //if (bank.IsLocked()) continue;
    if (!bank.CanBeUnloaded()) continue;
    LogF("Unloading locked bank %s",(const char *)bank.GetPrefix());
    bank.Unload();
    return true;
  }
  return false;
}

void QFBankQueryFunctions::ClearBanks()
{
  GFileBanks.Clear();
}


QFBank *QFBankQueryFunctions::AutoBank( const char *name )
{
  if( !*name ) return NULL;
  if( name[1]==':' ) return NULL;
  // select bank with the longest prefix
  QFBank *bestBank = NULL;
  int maxLen = 0;
  for (int i=0; i<GFileBanks.Size(); i++)
  {
    QFBank &bank = GFileBanks[i];
    RString prefix = bank.GetPrefix();
    if (!CmpStartStr(name, prefix))
    {
      int len = prefix.GetLength();
      if (len > maxLen)
      {
        bestBank = &bank;
        maxLen = len;
      }
    }
  }
  return bestBank;
}

QIFStreamB::QIFStreamB()
:_bank(NULL)
{
}

QFBankHandle QFBankQueryFunctions::GetHandle( const char *name, QFileSize offset, QFileSize size)
{
  const char *name0=name;
  if( GUseFileBanks )
  {
    QFBank *bank=AutoBank(name);
    if( bank )
    {
      // check if we should use bank version of the file
      // skip bank name
      name += bank->GetPrefix().GetLength();
      QFBankHandle handle = bank->GetHandle(name0,name,offset,size);
      #if !_ENABLE_INJECTING
      return handle;
      #else
      if (!GEnablePatching) return handle;
      if( !handle.IsNull())
      {
        return handle;
      }
      // if file does not exist in bank, try to open it from file
      LogF("File %s not in bank",name0);
      #endif
    }
  }
  return new QFBankPointerToAny(name0,offset,size);

  //QIFStream::open(name0,offset,size);
}

void QIFStreamB::open(const QFBankHandle &handle)
{
  handle.open(*this);
}

void QIFStreamB::AutoOpen( const char *name, QFileSize offset, QFileSize size)
{
  _fail = true;
  const char *name0=name;
  if( GUseFileBanks )
  {
    QFBank *bank=QFBankQueryFunctions::AutoBank(name);
    if( bank )
    {
      // check if we should use bank version of the file
      // skip bank name
      name += bank->GetPrefix().GetLength();
      open(*bank,name,offset,size);
      // how can we check if file is OK?
      if (!fail())
      {
        _bank = bank;
        _fail = false;
        return;
      }
      AttachSource(NULL);
      // if file does not exist in bank, try to open it from file
      #if !_ENABLE_INJECTING
      return;
      #else
      if (!GEnablePatching) return;
      LogF("File %s not in bank",name0);
      #endif
    }
  }
  QIFStream::open(name0,offset,size);
}

bool QIFStreamB::IsAccessible(IQFBankContext *context) const
{
  if (context && _bank && !context->IsAccessible(_bank))
  {
    RptF("AutoOpen %s: access denied",(const char *)_bank->GetPrefix());
    return false;
  }
  return true;
}

bool QIFStreamB::IsFromBank(const QFBank *bank) const
{
  // check request to flush all banks
  if (!bank) return true;
  return bank==_bank;
}

QFileTime QFBankQueryFunctions::TimeStamp( const char *name, IQFBankContext *context )
{
  if( GUseFileBanks )
  {
    QFBank *bank=AutoBank(name);
    if( bank )
    {
      #ifdef _XBOX
        return 0;
      #else
        const char *rName = name + bank->GetPrefix().GetLength();
        if (context && !context->IsAccessible(bank))
        {
          if( bank->FileExists(rName) ) return 0;
          LogF("FileExist %s: access denied",name);
          return 0;
        }
        const FileInfoO &info = bank->FindFileInfo(rName);
        if (!QFBank::IsNull(info))
        {
          #if _ENABLE_PATCHING
          if (!info.loadFromFile)
          #endif
          {
            return info.time;
          }
        }
        else
        {
          #if !_ENABLE_INJECTING
          return 0;
          #endif
          if (!GEnablePatching) return 0;
        }
      #endif
    }
  }
  return QIFileFunctions::TimeStamp(name);
}

/**
@return
negative when A should be loaded sooner then B,
positive when A should be loaded later then B
*/
int QFBankQueryFunctions::FileOrder(const char *nameA, const char *nameB)
{
  QFBank *bA = QFBankQueryFunctions::AutoBank(nameA);
  QFBank *bB = QFBankQueryFunctions::AutoBank(nameB);
  if (bA<bB) return -1;
  if (bA>bB) return +1;
  Assert(bA==bB);
  if (!bA) return 0;
  int oA = bA->GetFileOrder(nameA+strlen(bA->GetPrefix()));
  int oB = bB->GetFileOrder(nameB+strlen(bB->GetPrefix()));
  return oA-oB;
}

bool QFBankQueryFunctions::FileExist( const char *name, IQFBankContext *context)
{
  if( GUseFileBanks )
  {
    QFBank *bank=AutoBank(name);
    if( bank )
    {
      if (context && !context->IsAccessible(bank))
      {
        const char *rName = name + bank->GetPrefix().GetLength();
        if( bank->FileExists(rName) ) return false;
        LogF("FileExist %s: access denied",name);
        return false;
      }
      const char *rName = name + bank->GetPrefix().GetLength();
      if( bank->FileExists(rName) ) return true;
      #if !_ENABLE_INJECTING
      return false;
      #else
      if (!GEnablePatching) return false;
      #endif
    }
  }
  return QIFileFunctions::FileExists(name);
}

QFileSize QFBankQueryFunctions::GetFileSize(const char *name)
{
  if( GUseFileBanks )
  {
    QFBank *bank=AutoBank(name);
    if( bank )
    {
      /*
      if (context && !context->IsAccessible(bank))
      {
        const char *rName = name + bank->GetPrefix().GetLength();
        if( bank->FileExists(rName) ) return false;
        LogF("FileExist %s: access denied",name);
        return false;
      }
      */
      const char *rName = name + bank->GetPrefix().GetLength();
      const FileInfoO &info = bank->FindFileInfo(rName);
      if (bank->NotNull(info))
      {
        #if _ENABLE_PATCHING
        if (!info.loadFromFile)
        #endif
        {
          #ifndef _XBOX
            return info.compressedMagic!=0 ? info.uncompressedSize : info.length;
          #else
            return info.length;
          #endif
        }
      }
      else
      {
        #if !_ENABLE_INJECTING
        return 0;
        #else
        if (!GEnablePatching) return 0;
        #endif
      }
    }
  }
  return QIFileFunctions::GetFileSize(name);
}

struct EncryptorInformation
{
  RString name;
  IFilebankEncryption *(*createFunction)(const void *context);
};

TypeIsMovableZeroed(EncryptorInformation)

template <>
struct FindArrayKeyTraits<EncryptorInformation>
{
  typedef const char *KeyType;
  static bool IsEqual(const char * a, const char *b)
  {
    return !strcmpi(a,b);
  }
  static const char *GetKey(const EncryptorInformation &a) {return a.name;}
};

static FindArrayKey<EncryptorInformation> GEncryptors;

void RegisterFilebankEncryption
(
  const char *name, IFilebankEncryption *(*createFunction)(const void *context)
)
{
  // check if given encyption already exists
  int index = GEncryptors.FindKey(name);
  if (index>=0)
  {
    ErrF("Ecryption %s already registered",name);
    return;
  }
  EncryptorInformation &ei = GEncryptors.Append();
  ei.name = name;
  ei.createFunction = createFunction;
}

Ref<IFilebankEncryption> CreateFilebankEncryption(const char *name, const void *context)
{
  int index = GEncryptors.FindKey(name);
  if (index<0)
  {
    ErrF("Unknown encryption %s",name);
    return NULL;
  }
  return GEncryptors[index].createFunction(context);
}
