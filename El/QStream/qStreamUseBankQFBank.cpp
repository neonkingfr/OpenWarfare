#include <El/elementpch.hpp>

#include "iQFBank.hpp"
#include "qbStream.hpp"

class QFBankFunctionsQFBank: public QFBankFunctions
{
public:
  /// if possible, flush the bank handle so that the file can be deleted/written
  virtual bool FlushBankHandle(const char *filename);
  #if USE_MAPPING
	virtual bool BufferOwned(const QFBank *bank, const FileBufferMapped *buffer)
  {
    return bank->BufferOwned(buffer);
  }
	#endif
};

bool QFBankFunctionsQFBank::FlushBankHandle(const char *filename)
{
  // scan GFileBanks
  for (int i=0; i<GFileBanks.Size(); i++)
  {
    QFBank &bank = GFileBanks[i];
    if (strcmpi(bank.GetOpenName(),filename)) continue;
    if (!bank.CanBeUnloaded()) return false;
    // we do not care if unloading locked bank - we know it can be unloaded
    bank.Unload();
    return true;
  }
  return true;
}

static QFBankFunctionsQFBank GQStreamQFBankBankFunctions;
QFBankFunctions *FileBufferBankFunctions::_defaultQFBankFunctions = &GQStreamQFBankBankFunctions;
