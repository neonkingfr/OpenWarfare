// Quick file stream implementation

#include <El/elementpch.hpp>
//#include "winpch.hpp"

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#ifdef _WIN32
	#include <io.h>
	#include <stdio.h>
#else
	#include <stdio.h> 
	#include <unistd.h> 
	#define POSIX_FILES 1
#endif

#include <Es/Files/filenames.hpp>

#include "qStream.hpp"

#if POSIX_FILES_COMMON
#define POSIX_FILES 1

#endif

#if !defined INVALID_SET_FILE_POINTER && _MSC_VER<1300
#define INVALID_SET_FILE_POINTER ((DWORD)-1)
#endif //INVALID_SET_FILE_POINTER not defined in MSVC 6.0 headers

#ifdef POSIX_FILES
	#define NO_FILE(file) ( file<0 )
	#define NO_FILE_SET -1
#else
	#define NO_FILE(file) ( file==NULL )
	#define NO_FILE_SET NULL
#endif

#if USE_MAPPING
// by default file mapping is enabled, when supported on given platform
bool GUseFileMapping = true;
#else
bool GUseFileMapping = false;
#endif


const char *GetErrorName(LSError err)
{
	switch (err)
	{
	case LSOK:
		return "No error";
	case LSFileNotFound:
		return "No such file";
	case LSBadFile:
		return "Bad file (CRC, ...)";
	case LSStructure:
		return "Bad file structure";
	case LSUnsupportedFormat:
		return "Unsupported format";
	case LSVersionTooNew:
		return "Version is too new";
	case LSVersionTooOld:
		return "Version is too old";
	case LSDiskFull:
		return "No such file";
	case LSAccessDenied:
		return "Access denied";
	case LSDiskError:
		return "Disk error";
	case LSNoEntry:
		return "No entry";
	default:
		Fail("LSError");
	case LSUnknownError:
		return "Unknown error";
	}
}

void QIStreamSimple::Close()
{
	_buf=NULL;
	_len=0;
	_readFrom=0;
	_fail=true,_eof=false;
}

void QIStreamSimple::Assign( const QIStreamSimple &src )
{
	// this points to same data as from
	_buf=src._buf;
	_len=src._len;
	_readFrom=src._readFrom;
	_fail=src._fail,_eof=src._eof;
}

bool QIStreamSimple::nextLine ()
{
    int c1;
    while ( !eof() )
        if ( (c1 = get()) == 0x0D || c1 == 0x0A ) {
            if ( !eof() && c1 == 0x0D ) {
                int c2 = get();
                if ( c2 != 0x0A ) unget();
                }
            return true;
            }
    return false;
}

bool QIStreamSimple::readLine ( char *buf, int bufLen )
{
    Assert( buf );
    int left = bufLen - 1;                  // regular chars to read
    int c1;
    while ( !eof() ) {
        if ( (c1 = get()) == 0x0D || c1 == 0x0A ) {
                // EOLN:
            if ( !eof() && c1 == 0x0D ) {
                int c2 = get();
                if ( c2 != 0x0A ) unget();
                }
            *buf = (char)0;
            return true;
            }
            // regular char:
        if ( bufLen > 0 && left == 0 ) {    // buffer overflow
            *buf = (char)0;
            return nextLine();
            }
        *buf++ = c1;
        left--;
        }
    *buf = (char)0;
    return false;                           // EOF reached before EOLN
}

#ifdef POSIX_FILES
int FileSize ( int handle )
{
	struct stat buf;
	fstat(handle,&buf);
	return buf.st_size;
}
#endif

#include "fileMapping.hpp"

#ifndef POSIX_FILES
  #include "fileCompress.hpp"
#endif

/*!
Default implementation - assume you can access whole file.
In older implementations of IFileBuffer GetData() and GetSize() were the only way to get data
This is 
*/
PosQIStreamBuffer IFileBuffer::ReadBuffer(PosQIStreamBuffer &buf, QFileSize pos)
{
  buf.Free();
  const char *wholeData = GetData();
  QFileSize wholeSize = GetSize();
  if (pos>=wholeSize) return PosQIStreamBuffer(new QIStreamBuffer,pos,0);

  // optimal buffer size is system dependent variable
  // on XBox and PC (Intel 32b) this is 4 KB
  QFileSize size = 64*1024;
  QFileSize maxSize = wholeSize-pos;
  if (size>maxSize) size=maxSize;

  QIStreamBuffer *nBuf = new QIStreamBuffer(size);

  nBuf->FillWithData(wholeData+pos,size);

  return PosQIStreamBuffer(nBuf,pos,size);
}

/*!
Read given data region using ReadBuffer
*/
QFileSize IFileBuffer::Read(void *buf, QFileSize pos, QFileSize size)
{
  QFileSize rdTotal = 0;
  char *dta = (char *)buf;
  while (size>0)
  {
    PosQIStreamBuffer dummy(NULL,0,0);
    PosQIStreamBuffer buffer = ReadBuffer(dummy,pos);

    int bufferPos = Convert64ToQFileSize(pos-buffer._start);
    QFileSize readSize = buffer._len-bufferPos;
    if (readSize>size) readSize = size;
    QFileSize rd = buffer->ReadData(dta,bufferPos,readSize);

    if (rd<=0) break;
    size -= rd;
    dta += rd;
    pos += rd;
    rdTotal += rd;
  }
  return rdTotal;
}

FileBufferLoaded::FileBufferLoaded( const char *name, QFileSize offset, QFileSize size)
{
	#ifdef POSIX_FILES
	#ifndef _WIN32
	LocalPath(fn,name);
	int file=::open(fn,O_RDONLY);
	#else
	int file=::open(name,O_RDONLY|O_BINARY);
	#endif
	if( file>=0 )
	{
		int sizeMax=FileSize(file);
		if( sizeMax!=-1 )
		{
			if (size>sizeMax-offset) size = sizeMax-offset;
			_data.Init(size);
			::lseek(file,offset,SEEK_CUR);
			int sizeRead=::read(file,_data.Data(),size);
			if( sizeRead!=size )
			{
				_data.Delete();
				::WarningMessage("File '%s' read error",name);
			}
		}
		else
		{
			//::WarningMessage("File '%s' not found.",name);
		}
		::close(file);
	}
	#else
	FileServerHandle file = GFileServerFunctions->OpenReadHandle(name);
	if( GFileServerFunctions->CheckReadHandle(file))
	{
    HANDLE handle = GFileServerFunctions->GetWinHandle(file);
		DWORD sizeMax=::GetFileSize(handle,NULL);
		if( sizeMax>0 && sizeMax!=0xffffffff )
		{
			if (size>sizeMax-offset) size = sizeMax-offset;
			_data.Init(size);
			DWORD sizeRead;
    	// note: following call fill fail if file was opended with FILE_FLAG_NO_BUFFERING
			::SetFilePointer(handle,offset,NULL,FILE_CURRENT);
			::ReadFile(handle,_data.Data(),size,&sizeRead,NULL);
			if( sizeRead!=size )
			{
				_data.Delete();
				HRESULT hr = GetLastError();
				::WarningMessage("File '%s' read error %x",name,hr);
			}
		}
		GFileServerFunctions->CloseReadHandle(name,file);
	}	
	#endif
}

FileBufferLoaded::~FileBufferLoaded()
{
	_data.Delete();
}

FileBufferSub::FileBufferSub( IFileBuffer *buf, QFileSize start, QFileSize size )
:_whole(buf)
{
	QFileSize bufSize = buf->GetSize();
	if (start>bufSize) start = bufSize;
  QFileSize maxSize = bufSize-start;
  if (size>maxSize) size = maxSize;
  
	_start = start;
	_size = size;
}

bool FileBufferSub::PreloadBuffer(
  RequestableObject *obj, RequestableObject::RequestContext *ctx,
  FileRequestPriority priority, QFileSize pos, QFileSize size
) const
{
  if (pos>_size) pos = _size;
  QFileSize maxSize = _size-pos;
  if (size>maxSize) size=maxSize;
  return _whole->PreloadBuffer(obj,ctx,priority,_start+pos,size);
}

/*!
  \patch 5175 Date 10/11/2007 by Ondra
  - Fixed: Reduced loading time for large textures.
*/

void FileBufferSub::PreloadSequential(QFileSize pos, QFileSize size) const
{
  if (pos>_size) pos = _size;
  QFileSize maxSize = _size-pos;
  if (size>maxSize) size=maxSize;
  _whole->PreloadSequential(_start+pos,size);
}


PosQIStreamBuffer FileBufferSub::ReadBuffer(PosQIStreamBuffer &buf, QFileSize pos)
{
  if (pos>_size) pos = _size;
  PosQIStreamBuffer nBuf = _whole->ReadBuffer(buf,_start+pos);
  // we may need to shrink data a little bit
  Assert(nBuf._start<=_start+_size)
  QFileSize maxEnd = Convert64ToQFileSize(_start+_size-nBuf._start);
  QFileSize end = nBuf._len;
  if (end>maxEnd)
  {
    end = maxEnd;
  }

  return PosQIStreamBuffer(nBuf,nBuf._start-_start,end);
}

FileBufferLoading::FileBufferLoading( const char *name)
{
  _name = name;

  FileServerHandle handle = GFileServerFunctions->OpenReadHandle(name);
  if( GFileServerFunctions->CheckReadHandle(handle))
  {
    QFileSize fileSize = GFileServerFunctions->GetFileSize(handle);

    // adjust offset and size based on actual file size
    _handle = handle;
    _size = fileSize;
  }
  else
  {
    _handle = NULL;
    _size = 0;
  } 
}

FileBufferLoading::~FileBufferLoading()
{
  if (GFileServerFunctions->CheckReadHandle(_handle))
  {
    GFileServerFunctions->CloseReadHandle(_name,_handle);
  }
  _handle = NULL;
}

const char *FileBufferLoading::GetData() const
{
  Fail("Cannot return data");
  return NULL;
}
int FileBufferLoading::GetSize() const
{
  return _size;
}


DEFINE_FAST_ALLOCATOR(QIStreamBufferPage)

QIStreamBufferPage::QIStreamBufferPage()
{
  // allocate one page
	int pageSize = GFileServerFunctions->GetPageSize();
  _buffer = (char *)NewPage(pageSize,pageSize);
  if (!_buffer)
  {
    _bufferSize = 0;
    // error - no memory
  }
  else
  {
    _bufferSize = pageSize;
  }
}
QIStreamBufferPage::~QIStreamBufferPage()
{
  // release one page
  if (_buffer)
  {
    DeletePage(_buffer,GFileServerFunctions->GetPageSize());
	  _buffer = NULL;
  }
  _bufferSize = 0;
}

PosQIStreamBuffer QIFileServerFunctions::Read(FileServerHandle handle, QFileSize start, QFileSize size)
{
  QIStreamBuffer *buffer = new QIStreamBufferPage();
  Assert(size<=buffer->GetSize());

  void *data = buffer->DataLock(0,size);
  if (!data)
  {
    //! error - out of memory
    return PosQIStreamBuffer(buffer,start,0);
  }

  HANDLE filehandle = (HANDLE)handle;
  LONG zero = NULL;
  SetFilePointer(filehandle,start,&zero,FILE_BEGIN); //so we are currently limited to 4GB
  DWORD rd = 0;
  ReadFile(filehandle,data,size,&rd,NULL);
  buffer->DataUnlock(0,rd);

  return PosQIStreamBuffer(buffer,start,rd);
}

int QIFileServerFunctions::GetPageSize()
{
  // unless some file server is used, there any page size will do
  // ask memory manager what page size it likes
  return GetPageRecommendedSize();
}

void QIFileServerFunctions::Copy(const char *srcName, const char *dstName)
{
  if (_stricmp(srcName, dstName) == 0) return; // do not copy to itself

  QIFStream in;
  in.open(srcName);
  QOFStream out;
  out.open(dstName);
  in.copy(out);
  out.close();
  in.close();
}

/**
Default handling - assume all requests are satisfied and call handler immediately.
*/
bool QIFileServerFunctions::Preload(
  RequestableObject *obj, RequestableObject::RequestContext *ctx, FileRequestPriority priority, FileServerHandle handle, QFileSize start, QFileSize size
)
{
  if (obj)
    obj->RequestDone(ctx,RequestableObject::RRSuccess);
  return true;
}

bool QIFileServerFunctions::FlushReadHandle(const char *name)
{
  return true;
}

FileServerHandle QIFileServerFunctions::OpenReadHandle(const char *name)
{
#ifdef POSIX_FILES
  LocalPath(fn,name);
  //if testing POSIX_FILES in _WIN32, must be O_BINARY, because read function replaces
  //each carriage return�linefeed (CR-LF) pair with a single linefeed character
  //In FileBufferLoading::ReadBuffer, the pageSize aligning then fails: pos &= ~(pageSize-1);
  //see "read function" msdn documentation
  #ifdef _WIN32
  return (FileServerHandle)::open(fn,O_RDONLY|O_BINARY);
  #else
  return (FileServerHandle)::open(fn,O_RDONLY);
  #endif
#else
  return (FileServerHandle)::CreateFileA
    (
    name,GENERIC_READ,FILE_SHARE_READ,
    NULL, // security
    OPEN_EXISTING,
    FILE_ATTRIBUTE_NORMAL,
    NULL // template
    );
#endif
}

FileServerHandle QIFileServerFunctions::OpenWriteHandle(const char *name)
{
#ifdef POSIX_FILES

  #ifndef _WIN32
  LocalPath(fn, (const char*)name);
  FileServerHandle retVal = (FileServerHandle)::open(fn,O_CREAT|O_WRONLY|O_TRUNC,S_IREAD|S_IWRITE);
  if (retVal) chmod(fn,S_IREAD|S_IWRITE);
  return retVal;
  #else
  return (FileServerHandle)::open(name,O_CREAT|O_BINARY|O_WRONLY|O_TRUNC,S_IREAD|S_IWRITE);
  #endif

#else
  return (FileServerHandle)::CreateFileA(
    name, GENERIC_WRITE, 0,
    NULL, // security
    CREATE_ALWAYS,
    FILE_ATTRIBUTE_NORMAL,
    NULL); // template
#endif
}

FileServerHandle QIFileServerFunctions::OpenWriteHandleForAppend(const char *name)
{
#ifdef POSIX_FILES

#ifndef _WIN32
  LocalPath(fn, (const char*)name);
  FileServerHandle retVal = (FileServerHandle)::open(fn,O_CREAT|O_APPEND|O_WRONLY,S_IREAD|S_IWRITE);
  if (retVal) chmod(fn,S_IREAD|S_IWRITE);
  return retVal;
#else
  return (FileServerHandle)::open(name,O_CREAT|_O_APPEND|O_BINARY|O_WRONLY,S_IREAD|S_IWRITE);
#endif

#else
  HANDLE fileHandle = ::CreateFileA(
    name, GENERIC_WRITE, 0,
    NULL, // security
    OPEN_ALWAYS,
    FILE_ATTRIBUTE_NORMAL,
    NULL); // template
  SetFilePointer(fileHandle, 0, 0, FILE_END);
  return (FileServerHandle)fileHandle;
#endif
}

void QIFileServerFunctions::MakeReadHandlePermanent(FileServerHandle handle)
{
}

void QIFileServerFunctions::CloseReadHandle(const char *name, FileServerHandle handle)
{
#if !POSIX_FILES_COMMON
  CloseHandle((FileServerHandle)handle);
#else
  ::close((int)handle);
#endif
}

bool QIFileServerFunctions::CheckReadHandle(FileServerHandle handle)
{
  HANDLE winHandle = (HANDLE)handle;
  return winHandle!=NULL && winHandle!=INVALID_HANDLE_VALUE;
}

RString QIFileServerFunctions::GetHandleName(FileServerHandle handle) const
{
  // is it not possible to get a name of the file when only handle is known
  return Format("Handle %x",(int)handle);  
}

QFileSize QIFileServerFunctions::GetFileSize(FileServerHandle handle) const
{
#ifndef POSIX_FILES
  return ::GetFileSize((HANDLE)handle, NULL);
#else
  return FileSize((int)handle);
#endif
}

QFileTime QIFileServerFunctions::TimeStamp(FileServerHandle handle) const
{
#if defined POSIX_FILES
  struct stat st;
  fstat((int)handle,&st);
  return st.st_mtime;
#else
  FILETIME filetime;
  ::GetFileTime((HANDLE)handle, NULL, NULL, &filetime);

  /*
  The FILETIME structure is a 64-bit value representing
  the number of 100-nanosecond intervals since January 1, 1601 (UTC).
  
  The time function returns the number of seconds elapsed since midnight (00:00:00),
  January 1, 1970, coordinated universal time (UTC), according to the system clock.
  */

  /*
  SYSTEMTIME startTime;
  startTime.wYear = 1970; startTime.wMonth = 1; startTime.wDay = 1;
  startTime.wDayOfWeek = 0;
  startTime.wHour = 0; startTime.wMinute = 0; startTime.wSecond = 0;
  startTime.wMilliseconds = 0;
  FILETIME jan1970;
  SystemTimeToFileTime(&startTime, &jan1970);
  __int64 tJan1970 = *((__int64*)&jan1970);
  */

  // the code above leads to a constant like below:  
  // FILETIME of 1 Jan 1970 (base of Unix timestamps)
#if _MSC_VER>1200
  ULONGLONG tJan1970 = 0x019db1ded53e8000ULL;
  ULARGE_INTEGER t;
  t.HighPart = filetime.dwHighDateTime;
  t.LowPart = filetime.dwLowDateTime;

  // convert 100 ns to 1 s (nano = 10e-9)
  ULONGLONG sysTime = (t.QuadPart-tJan1970)/10000000ULL;
  Assert(sysTime<0x100000000ULL);
  return QFileTime(sysTime);
#else
  ULONGLONG tJan1970 = 0x019db1ded53e8000;
  ULARGE_INTEGER t;
  t.HighPart = filetime.dwHighDateTime;
  t.LowPart = filetime.dwLowDateTime;

  // convert 100 ns to 1 s (nano = 10e-9)
  ULONGLONG sysTime = (t.QuadPart-tJan1970)/10000000;
  Assert(sysTime<0x100000000);
  return QFileTime(sysTime);
#endif
#endif
}

bool FileBufferLoading::GetError() const
{
  return !GFileServerFunctions->CheckReadHandle(_handle);
}

bool FileBufferLoading::PreloadBuffer(
  RequestableObject *obj, RequestableObject::RequestContext *ctx,
  FileRequestPriority priority, QFileSize pos, QFileSize size
) const
{
  if (pos>_size) pos = _size;
  QFileSize maxSize = _size - pos;
  if (size>maxSize) size = maxSize;
  return GFileServerFunctions->Preload(obj,ctx,priority,_handle,pos,size);
}

void FileBufferLoading::PreloadSequential(QFileSize pos, QFileSize size) const
{
  // we know actual read will follow immediately
  FileRequestPriority priority = FileRequestPriority(1);
  if (pos>_size) pos = _size;
  QFileSize maxSize = _size - pos;
  if (size>maxSize) size = maxSize;
  
  // will split the request into a lot of small ones
  // this way first part will be ready much sooner than the whole request would be
  // what granularity is best is hard to tell
  QFileSize granularity = GFileServerFunctions->GetPageSize();
  // avoid generating too small requests
  // avoid generating too many requests
  // expected request latency:
  // hard-disk: seek rate 5-30 ms, sustained data rate 12-80 MB/s
  // DVD: seek rate 100-300 ms, sustained data rate 2-15 MB/s
  // expected time for sustained reading 32 KB file from hard-disk is around 1 ms 
  
  while (granularity<32*1024) granularity = granularity+granularity;

  // we do not care about results of the requests  
  while (size>=granularity)
  {
    GFileServerFunctions->Preload(NULL,NULL,priority,_handle,pos,granularity);
    pos += granularity;
    size -= granularity;
  }
  if (size>0)
  {
    GFileServerFunctions->Preload(NULL,NULL,priority,_handle,pos,size);
  }
}

PosQIStreamBuffer FileBufferLoading::ReadBuffer(PosQIStreamBuffer &buf, QFileSize pos)
{
  buf.Free();
  if (pos>_size) pos = _size;

  int pageSize = GFileServerFunctions->GetPageSize();

  pos &= ~(pageSize-1); //for POSIX_FILES, reading should be O_BINARY, otherwise pos often set to zero

  Assert(_size>=pos);
  QFileSize maxSize = _size - pos;

  QFileSize size = pageSize;
  if (size>maxSize) size = maxSize;
  
  if (pos>=_size)
  {
    return PosQIStreamBuffer(new QIStreamBuffer,pos,0);
  }

  return PosQIStreamBuffer(GFileServerFunctions->Read(_handle,pos,size));
}

#ifdef _XBOX

RString FullXBoxName(RString name)
{
  if (IsPathAbsolute(name)) return name;
  // first skip any letters
  // check if they are followed by double colon
  // check if there is any double colon before the first backslash
  return RString("#:\\")+name;
}
const char *FullXBoxName(const char *name, char *temp)
{
  if (IsPathAbsolute(name)) return name;
  strcpy(temp,"#:\\");
	strcat(temp,name);
	return temp;
}
#endif

#if USE_MAPPING
Ref<IFileBuffer> GetFileBufferMappedPart(const char *fullName, QFileSize offset, QFileSize size)
{
	// round offset, cr
	if (offset==0)
	{
		return new FileBufferMapped(fullName,0,size);
	}
	SYSTEM_INFO info;
	GetSystemInfo(&info);
	int page = info.dwAllocationGranularity;
	int align = offset % page;
	Ref<IFileBuffer> aligned = new FileBufferMapped(fullName,offset-align,size+align);
	return new FileBufferSub(aligned,align,size);
}
#endif


int CmpStartStr( const char *str, const char *start )
{
	while( *start )
	{
		if( myLower(*str++)!=myLower(*start++) ) return 1;
	}
	return 0;
}

bool QIFileFunctions::FileReadOnly( const char *name )
{
	// file exists and is read only
#if defined POSIX_FILES
	LocalPath(fn,name);
	struct stat st;
	if ( stat(fn,&st) ) return false;
  return( (st.st_mode & S_IWUSR) == 0 );
#else
	DWORD attrib=::GetFileAttributesA(name);
	// check for cases where write would fail
	if( attrib&FILE_ATTRIBUTE_READONLY ) return true;
	if( attrib&FILE_ATTRIBUTE_DIRECTORY ) return true;
	if( attrib&FILE_ATTRIBUTE_SYSTEM ) return true;
	return false; // no file
#endif
}

QFileTime QIFileFunctions::TimeStamp(const char *name)
{
  // on X360 we require file timestamps because of shader cache
#if 0 // defined _XBOX
	return 0;
#else
	FileServerHandle check = GFileServerFunctions->OpenReadHandle(name);
	if (!GFileServerFunctions->CheckReadHandle(check)) return 0;
  QFileTime time = GFileServerFunctions->TimeStamp(check);
	GFileServerFunctions->CloseReadHandle(name, check);
  return time;
#endif
}

QFileSize QIFileFunctions::GetFileSize(const char *name)
{
  FileServerHandle check = GFileServerFunctions->OpenReadHandle(name);
  if (!GFileServerFunctions->CheckReadHandle(check)) return 0;
  QFileSize size = GFileServerFunctions->GetFileSize(check);
  GFileServerFunctions->CloseReadHandle(name,check);
  if (size == 0xffffffff) return 0;
  return size;
}

bool QIFileFunctions::FileExists( const char *name )
{
	// check normal file existence
#ifdef _XBOX
		char temp[1024];
		name = FullXBoxName(name,temp);
#endif
	FileServerHandle check = GFileServerFunctions->OpenReadHandle(name);
  if (!GFileServerFunctions->CheckReadHandle(check)) return false;
	GFileServerFunctions->CloseReadHandle(name,check);
	return true;
}

#include <Es/Strings/bString.hpp>

bool QIFileFunctions::Unlink(const char *name)
{
#ifdef POSIX_FILES
	LocalPath(fn, name);
	name = fn;
#endif
	GFileServerFunctions->FlushReadHandle(name);
#if defined _XBOX && _ENABLE_REPORT
  BString<512> fn;
  if (name[0]=='#')
  {
    strcpy(fn,name);
    LogF("Deleting 'DVD' file %s",name);
    fn[0]='D';
    name = fn;
  }
#endif
  chmod(name, S_IREAD | S_IWRITE);
	return unlink(name) == 0;
}

bool QIFileFunctions::CleanUpFile(const char *name)
{
	if (!FileExists(name)) return true;
	return Unlink(name);
}

bool QIFileFunctions::DirectoryExists(const char *name)
{
  RString realName;
#ifdef _XBOX
  // Caching DVD on Scratch Volume
  if (name[0] == '#' && name[1] == ':')
  {
    // cached access to DVD
    realName = RString("d") + RString(name + 1);
  }
  else
  {
    realName = name;
  }
#else
  realName = name;
#endif
	return access(realName, 6) != -1;
}

#define WIN_DIR '\\'
#define UNIX_DIR '/'

#if __GNUC__
	#define INVAL_DIR WIN_DIR
	#define VAL_DIR UNIX_DIR
#else
	#define INVAL_DIR UNIX_DIR
	#define VAL_DIR WIN_DIR
#endif

// helper: open in file or in file bank
static RString ConvertFileName( const char *name, int inval, int valid )
{
	if( !strchr(name,inval) ) return name; // no conversion required
	// convert directory characters depending on platform
	char cname[512];
	strcpy(cname,name);
	for( char *cc=cname; *cc; cc++ )
	{
		if( *cc==inval ) *cc=valid;
	}
	return cname;
}

inline RString PlatformFileName( const char *name )
{
	return ConvertFileName(name,INVAL_DIR,VAL_DIR);
}

inline RString UniversalFileName( const char *name )
{ // filenames are normally stored with backslash '\\'
	return ConvertFileName(name,UNIX_DIR,WIN_DIR);
}

#ifndef POSIX_FILES
static LSError LSErrorCode( DWORD eCode )
{
  switch( eCode )
  {
  case ERROR_HANDLE_DISK_FULL: return LSDiskFull;
  case ERROR_NETWORK_ACCESS_DENIED:
  case ERROR_LOCK_VIOLATION:
  case ERROR_SHARING_VIOLATION:
  case ERROR_WRITE_PROTECT:
  case ERROR_ACCESS_DENIED: return LSAccessDenied;
  case ERROR_FILE_NOT_FOUND: return LSFileNotFound;
  case ERROR_READ_FAULT:
  case ERROR_WRITE_FAULT:
  case ERROR_CRC: return LSDiskError;
  default:
    {
#ifndef _XBOX
      char buffer[256];
      FormatMessageA
        (
        FORMAT_MESSAGE_FROM_SYSTEM,
        NULL, // source
        eCode, // requested message identifier 
        0, // languague
        buffer,sizeof(buffer),
        NULL
        );
      Log("Unknown error %d %s",eCode,buffer);
#else
      Log("Unknown error %d",eCode);
#endif

    }
    return LSUnknownError;
  }
}
#endif

void QOFStream::open( const char *file )
{
  _fail = false;
  _error = LSOK;
  
  RString name = PlatformFileName(file);

  // check if file is not open for reading
  GFileServerFunctions->FlushReadHandle(name);

#if POSIX_FILES
  _file = (int)GFileServerFunctions->OpenWriteHandle(name);
#else
  _file = GFileServerFunctions->OpenWriteHandle(name);

  if (_file == INVALID_HANDLE_VALUE)
  {
    _file = NULL;
    _fail = true;
    DWORD eCode = ::GetLastError();
    if (eCode) _error = LSErrorCode(eCode);
  }
#endif

  rewind();
}

void QOFStream::openForAppend( const char *file )
{
  _fail = false;
  _error = LSOK;

  RString name = PlatformFileName(file);

  // check if file is not open for reading
  GFileServerFunctions->FlushReadHandle(name);

#if POSIX_FILES
  _file = (int)GFileServerFunctions->OpenWriteHandleForAppend(name);
#else
  _file = GFileServerFunctions->OpenWriteHandleForAppend(name);

  if (_file == INVALID_HANDLE_VALUE)
  {
    _file = NULL;
    _fail = true;
    DWORD eCode = ::GetLastError();
    if (eCode) _error = LSErrorCode(eCode);
  }
#endif

  rewind();
}

/*!
\patch 1.85 Date 9/9/2002 by Jirka
- Fixed: Writing into big file sometimes failed with error #1450:
  "Insufficient system resources exist to complete the requested service."
*/

/*
void QOFStream::close( const void *header, int headerSize )
{
}
*/

void QOFStream::close()
{
  if (!_file) return; // no file

  Flush(0);

#ifdef POSIX_FILES
  int success = ::close(_file);
  if (success < 0)
  {
    _fail = true;
    if (_error == LSOK)
      _error = LSUnknownError;
  }
#else
  BOOL success = ::CloseHandle(_file);
  if (!success)
  {
    _fail = true;
    if (_error == LSOK)
    {
      DWORD eCode = ::GetLastError();
      _error = LSErrorCode(eCode);
    }
  }
#endif
  // forget any data
  rewind();
  _file = NULL;
}

QOFStream::~QOFStream()
{
  close();
}

void QOFStream::Flush(int size)
{
  if (_file && !_fail && _bufferSize > 0)
  {
    // open and preload file
#ifdef POSIX_FILES
    const char *data = (const char *)_buf->DataLock(0, _bufferSize);
    int written = ::write(_file, data, _bufferSize);
    _buf->DataUnlock(0, _bufferSize);
    if (written != _bufferSize)
    {
      _fail = true;
      _error = LSUnknownError;
    }
#else
    const char *data = (const char *)_buf->DataLock(0, _bufferSize);
    DWORD written;
    ::WriteFile(_file, data, _bufferSize, &written, NULL);
    _buf->DataUnlock(0, _bufferSize);
    if (written != _bufferSize)
    {
      _fail = true;
      DWORD eCode = ::GetLastError();
      _error=LSErrorCode(eCode);
    }
#endif

    _bufferStart += _bufferOffset;
    if (_bufferSize != _bufferOffset)
    {
      // set pointer to right place
#if POSIX_FILES_COMMON
      if (SetFilePointerPosix(_file, _bufferStart, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
#else
      if (::SetFilePointer(_file, _bufferStart, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
#endif
      {
        _fail = true;
#ifndef POSIX_FILES
        DWORD eCode = ::GetLastError();
        _error=LSErrorCode(eCode);
#endif
      }
    }
  }

  _bufferSize = 0;
  _bufferOffset = 0;
}

void QOFStream::SetFilePos(int pos)
{
  if (pos >= (int)_bufferStart && pos <= (int)(_bufferStart + _bufferSize))
  {
    _bufferOffset = pos - _bufferStart;
  }
  else
  {
    Flush(0);
    Assert(_bufferOffset == 0)
    _bufferStart = pos;
    if (_file && !_fail)
    {
#if POSIX_FILES_COMMON
      if (SetFilePointerPosix(_file, _bufferStart, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
#else
      if (::SetFilePointer(_file, _bufferStart, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
#endif
      {
        _fail = true;
#if defined(_WIN32) && !POSIX_FILES
        DWORD eCode = ::GetLastError();
        _error=LSErrorCode(eCode);
#endif
      }
    }
  }
}

DEFINE_FAST_ALLOCATOR(QIStreamBuffer)

QIStreamBuffer::QIStreamBuffer(int size)
{
	_bufferSize = size;
  _buffer = new char[size];
}

/*
QIStreamBuffer::QIStreamBuffer(const void *data, int size)
{
	_bufferSize = size;
  _buffer = new char[size];
  memcpy(_buffer,data,size);
}
*/

void QIStreamBuffer::Realloc(int size, int used)
{
  if (_bufferSize==size) return;
  if (used>0)
  {
    Assert(_bufferSize>=(QFileSize)used);
    char *oldBuffer = _buffer;
    _buffer = new char[size];
    memcpy(_buffer,oldBuffer,used);
    delete oldBuffer;
  }
  else
  {
    if (_buffer) delete _buffer;
    _buffer = new char[size];
  }
  _bufferSize = size;
}

QIStreamBuffer::QIStreamBuffer()
{
  // create an empty buffer
  _bufferSize = 0; // default buffer size - one XBox/PCx86 system page
  _buffer = NULL;
}
QIStreamBuffer::~QIStreamBuffer()
{
  if (_buffer)
  {
    delete[] _buffer;
    _buffer = NULL;
  }
}


QIStream::QIStream()
//_buf(new QIStreamBuffer,0,0)
:_buf(NULL,0,0)
{
  // I do not see any reason why buffer should be allocated before reading starts
  // it would be discarded in ReadBuffer anyway
	_readFromBuf = 0;

	_fail = false;
  _eof = false;
	_error = LSOK;
}

QIStream::QIStream(enum _noBuffer)
:_buf(NULL,0,0)
{
  _readFromBuf = 0;

  _fail = false;
  _eof = false;
  _error = LSOK;
}

QIStream::~QIStream()
{
  // make sure buffer was released by the implementation
  Assert(_buf==NULL);
}


bool QIStream::nextLine ()
{
  for(;;)
  {
    int c1 = get();
    if (c1==EOF) return false;
    if ( c1== 0x0D || c1 == 0x0A )
    {
      if (c1 == 0x0D )
      {
        int c2 = get();
        if ( c2 != 0x0A && c2!=EOF ) unget();
      }
      return true;
    }
  }
}

bool QIStream::readLine ( char *buf, int bufLen )
{
  Assert( buf );
  int left = bufLen - 1;                  // regular chars to read
  for(;;)
  {
    int c1 = get();
    if (c1==EOF)
    {
      *buf = (char)0;
      return false;                           // EOF reached before EOLN
    }
    if ( c1== 0x0D || c1 == 0x0A )
    {
      // EOLN:
      if ( c1 == 0x0D )
      {
        int c2 = get();
        if ( c2 != 0x0A && c2!=EOF ) unget();
      }
      *buf = (char)0;
      return true;
    }
    // regular char:
    if ( bufLen > 0 && left == 0 )
    {    // buffer overflow
      *buf = (char)0;
      return nextLine();
    }
    *buf++ = c1;
    left--;
  }
}

#include <Es/Memory/normalNew.hpp>
/// a buffer style refrence to existing data
class QIStreamBufferTemp: public QIStreamBuffer
{
  public:
  QIStreamBufferTemp(const void *data, int size);
  ~QIStreamBufferTemp();

  USE_FAST_ALLOCATOR
};
#include <Es/Memory/debugNew.hpp>

DEFINE_FAST_ALLOCATOR(QIStreamBufferTemp)

QIStreamBufferTemp::QIStreamBufferTemp(const void *data, int size)
{
  _buffer = (char *)data;
  _bufferSize = size;
}
QIStreamBufferTemp::~QIStreamBufferTemp()
{
  _buffer = NULL;
  _bufferSize = 0;
}

/**
caution: data are not copied, only referenced - they must exist in proper scope
*/

void QIStrStream::init(const void *data, int size)
{
  // data are touched by ReadData, but our ReadData will not touch them
  _buf = PosQIStreamBuffer(new QIStreamBufferTemp(data,size),0,size);
  _readFromBuf = 0;
}

void QIStrStream::init(QOStrStream &stream)
{
  _buf = stream.GetBuffer();
}


QIStrStream::~QIStrStream()
{
  _buf = PosQIStreamBuffer(NULL,0,0);
}

void QIStreamDataSource::Copy(const QIStreamDataSource &src)
{
  _buf = src._buf;
  _source = src._source;

	_readFromBuf = src._readFromBuf;

	_fail = src._fail;
  _eof = src._eof;
	_error = src._error;
}

QIStreamDataSource &QIStreamDataSource::operator = ( const QIStreamDataSource &src )
{
  Copy(src);
  return *this;
}
QIStreamDataSource::QIStreamDataSource( const QIStreamDataSource &src )
{
  Copy(src);
}


QIStreamDataSource::QIStreamDataSource()
:QIStream(NoBuffer) // no need to allocate buffer until reading will start
{
}
QIStreamDataSource::~QIStreamDataSource()
{
  _buf = PosQIStreamBuffer(NULL,0,0);
}

PosQIStreamBuffer QIStreamDataSource::ReadBuffer(PosQIStreamBuffer &buf, QFileSize pos)
{
  return _source->ReadBuffer(buf,pos);
}

bool QIStreamDataSource::PreloadBuffer(
  RequestableObject *obj, RequestableObject::RequestContext *ctx,
  FileRequestPriority priority, QFileSize pos, QFileSize size
) const
{
  if (!_source) return true;
  return _source->PreloadBuffer(obj,ctx,priority,pos,size);
}

void QIStreamDataSource::PreloadSequential(QFileSize size) const
{
  if (!_source) return;
  QFileSize pos = tellg();
  _source->PreloadSequential(pos,size);
  
}


void QIFStream::open(const char *dta, QFileSize offset, QFileSize size)
{
	Assert(!GetSource());
	_fail = true;
	InvalidateBuffer();
	_buf._start = 0;
	_readFromBuf = 0;

	#ifdef _XBOX
	char temp[1024];
	dta = FullXBoxName(dta,temp);
	#endif
	// open file handle

	Ref<IFileBuffer> whole = new FileBufferLoading(dta);
	if (whole->GetError())
	{
		InvalidateBuffer();
		AttachSource(new FileBufferError);
		_fail = true;
	}
	else
	{
		QFileSize wholeSize = whole->GetSize();
		if (offset>0 || size<wholeSize)
		{
			Ref<IFileBuffer> buffer = new FileBufferSub(whole,offset,size);
			Assert (!buffer->GetError());
			InvalidateBuffer();
			AttachSource(buffer);
			_fail = false;
		}
		else
		{
			InvalidateBuffer();
			AttachSource(whole);
			_fail = false;
		}
	}
}

void QIFStream::close()
{
  InvalidateBuffer();
  AttachSource(NULL);
}

/// Functor converting Unicode stream to UTF-8 stream
class FileWideCharToMultiByte
{
protected:
  QOStream &_out;
  bool _revert;

public:
  FileWideCharToMultiByte(QOStream &out, bool revert) : _out(out), _revert(revert) {}

  void operator ()(char *buf, int size)
  {
    if (_revert)
    {
      // fix the endians
      for (int i=0; i<size; i+=2) {char c = buf[i]; buf[i] = buf[i + 1]; buf[i + 1] = c;}
    }
    WCHAR *src = (WCHAR *)(buf);
    int srcLen = size / sizeof(WCHAR);
    // check the size of the needed buffer
    int dstLen = WideCharToMultiByte(CP_UTF8, 0, src, srcLen, NULL, 0, NULL, NULL);
    // allocate the buffer, convert and write to the stream
    Buffer<char> dst(dstLen);
    WideCharToMultiByte(CP_UTF8, 0, src, srcLen, dst.Data(), dstLen, NULL, NULL);
    _out.write(dst.Data(), dstLen);
  }
};

//TODO: Linux changes in archived ArmA
// [Look into IsUnicode implamentation and usage]
QIStream *HandleUnicode::SelectStream(QIStrStream *stream)
{
  int c1 = _source.get();
  if (_source.eof())
  {
    _source.seekg(0);
    return &_source; // too short source
  }
  int c2 = _source.get();
  if (_source.eof())
  {
    _source.seekg(0);
    return &_source; // too short source
  }

  if (c1 == 0xff && c2 == 0xfe)
  {
    // standard Unicode
#if _M_PPC
    bool revert = true;
#else
    bool revert = false;
#endif
    // convert file to UTF-8
    QOStrStream out;
    FileWideCharToMultiByte func(out, revert);
    _source.Process(func);

    // use the output as an input stream
    if (!stream) stream = new QIStrStream();
    stream->init(out);
    return stream;
  }
  if (c1 == 0xfe && c2 == 0xff)
  {
    // reverted Unicode
#if _M_PPC
    bool revert = false;
#else
    bool revert = true;
#endif
    // convert file to UTF-8
    QOStrStream out;
    FileWideCharToMultiByte func(out, revert);
    _source.Process(func);

    // use the output as an input stream
    if (!stream) stream = new QIStrStream();
    stream->init(out);
    return stream;
  }
  if (c1 == 0xef && c2 == 0xbb)
  {
    int c3 = _source.get();
    if (!_source.eof() && c3 == 0xbf) return &_source; // skip UTF-8 prefix
  }
  _source.seekg(0);
  return &_source; // no known prefix
}

QIPreprocessedStream::QIPreprocessedStream(QIStream *stream)
: _sourceBuf(NULL, 0, 0)
{
  _stream = stream;
  if (stream) _sourceBuf = stream->_buf;
  _nlRequest = false;
}

QIPreprocessedStream::~QIPreprocessedStream()
{
  _buf = PosQIStreamBuffer(NULL, 0, 0);
  _sourceBuf = PosQIStreamBuffer(NULL, 0, 0);
}

PosQIStreamBuffer QIPreprocessedStream::ReadBuffer(PosQIStreamBuffer &buf, QFileSize pos)
{
  if (!_stream) return PosQIStreamBuffer(new QIStreamBuffer, pos, 0);
  if (pos != buf._start + buf._len)
  {
    // only sequential access is enabled
    return PosQIStreamBuffer(new QIStreamBuffer, pos, 0);
  }

  if (_nlRequest)
  {
    _location.line++;
    _nlRequest = false;
  }

  int len = Convert64ToQFileSize(_sourceBuf._start + _sourceBuf._len - pos);
  if (len <= 0)
  {
    // we are out of source buffer, read next page
    _sourceBuf = _stream->ReadBuffer(_sourceBuf, pos);
    len = Convert64ToQFileSize(_sourceBuf._start + _sourceBuf._len - pos);
  }
  saturateMax(len, 0);

  int offset = Convert64ToQFileSize(pos - _sourceBuf._start);
  for (int i=0; i<len; i++)
  {
    if (_sourceBuf->_buffer[offset + i] == '\n')
    {
      // return single line only
      _nlRequest = true;
      return PosQIStreamBuffer(_sourceBuf, _sourceBuf._start, offset + i + 1);
    }
  }

  // return the rest of buffer
  return PosQIStreamBuffer(_sourceBuf, _sourceBuf._start, offset + len);
}

bool QIPreprocessedStream::PreloadBuffer(
  RequestableObject *obj, RequestableObject::RequestContext *ctx,
  FileRequestPriority priority, QFileSize pos, QFileSize size
  ) const
{
  if (!_stream) return true;
  return _stream->PreloadBuffer(obj, ctx, priority, pos, size);
}

QFileSize QIPreprocessedStream::GetDataSize() const
{
  if (!_stream) return 0;
  return _stream->GetDataSize();
}
