// DebugWindowTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/DebugWindow/debugWin.hpp>
#include <El/Color/colors.hpp>

#include <Es/Common/win.h>

LRESULT CALLBACK DebugWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

int WINAPI WinMain
(
  HINSTANCE hInstance,      // handle to current instance
  HINSTANCE hPrevInstance,  // handle to previous instance
  LPSTR lpCmdLine,          // command line
  int nCmdShow              // show state
)
{
	WNDCLASS clsDebug;
	clsDebug.hCursor        = LoadCursor(0,IDC_ARROW);
	clsDebug.hIcon          = LoadIcon(hInstance, "APPICON");
	clsDebug.lpszMenuName   = NULL;
	clsDebug.lpszClassName  = "DEBUG WINDOW";
	clsDebug.hbrBackground  = (HBRUSH)GetStockObject(BLACK_BRUSH);
	clsDebug.hInstance      = hInstance;
	clsDebug.style          = CS_VREDRAW | CS_HREDRAW;
	clsDebug.lpfnWndProc    = (WNDPROC)DebugWndProc;
	clsDebug.cbClsExtra     = 0;
	clsDebug.cbWndExtra     = 0;
	if( !RegisterClass(&clsDebug) ) return NULL;

	const int size = 1024;
	
	Link<DebugMemWindow> debugWin = new DebugMemWindow("Debug Window Test", size, size);

	MSG msg;
	while (true)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			if (!GetMessage(&msg, NULL, 0, 0)) break; 
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (!debugWin)
		{
			PostQuitMessage(0);
			while (GetMessage(&msg, NULL, 0, 0))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			break;
		}

		for (int i=0; i<100; i++)
		{
			int x = rand() % size;
			int y = rand() % size;
			float r = (float)rand() / RAND_MAX;
			float g = (float)rand() / RAND_MAX;
			float b = (float)rand() / RAND_MAX;
			Color color(r, g, b, 1);
			debugWin->Set(x, y) = debugWin->DColor(color);
		}
		debugWin->Update();
	}

	return msg.wParam;
}
