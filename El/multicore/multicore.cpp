#include <El/elementpch.hpp>
#include "multicore.hpp"

static int ForcedCPUCount = 0;

void ForceCPUCount(int count)
{
  ForcedCPUCount = count;

  #if !defined _XBOX && !_SUPER_RELEASE
  // when used in non-retail builds, we want to use cpuCount to simulate less-core CPUs
  // therefore we set affinity mask so that no more CPUs are used
  // in Retail builds we use it only to override CPU count detection to determine optimum threading patterns,
  // but we still allow the system to run more threads when possible
  DWORD processAffinityMask = 0;
  DWORD systemAffinityMask = 0;
  if (GetProcessAffinityMask(GetCurrentProcess(), &processAffinityMask, &systemAffinityMask))
  {
    int left = count;
    DWORD wantedAffinityMask = 0;
    for (int i=0; i<32; i++)
    {
      if (processAffinityMask&(1<<i))
      {
        wantedAffinityMask |= 1<<i;
        if (--left<=0) break;
      }
    }
    LogF("Process afinity forced to %x",wantedAffinityMask);
    SetProcessAffinityMask(GetCurrentProcess(),wantedAffinityMask);
  }
  #endif
}

int GetCPUCount()
{
  // when CPU count is given from command line, we want to respect it even on Xbox retail version
  // such command lines are used during the build process
  if (ForcedCPUCount > 0) return ForcedCPUCount;
#if defined _XBOX
  return 6;
#else
  int count = 0;
  DWORD processAffinityMask = 0;
  DWORD systemAffinityMask = 0;
  if (GetProcessAffinityMask(GetCurrentProcess(), &processAffinityMask, &systemAffinityMask))
  {
    while (processAffinityMask != 0)
    {
      if (processAffinityMask & 1) count++;
      processAffinityMask >>= 1;
    }
  }
  return count;
#endif
}

#if !_SUPER_RELEASE

#define MS_VC_EXCEPTION 0x406D1388

struct THREADNAME_INFO
{
  DWORD dwType;     // Must be 0x1000
  LPCSTR szName;    // Pointer to name (in user address space)
  DWORD dwThreadID; // Thread ID (-1 for caller thread)
  DWORD dwFlags;    // Reserved for future use; must be zero
};

void SetThreadName(DWORD dwThreadID, LPCSTR szThreadName)
{
  THREADNAME_INFO info;
  info.dwType = 0x1000;
  info.szName = szThreadName;
  info.dwThreadID = dwThreadID;
  info.dwFlags = 0;

  __try
  {
    RaiseException(MS_VC_EXCEPTION, 0, sizeof(info)/sizeof(DWORD), (DWORD *)&info);
  }
  __except (GetExceptionCode() == MS_VC_EXCEPTION ? EXCEPTION_CONTINUE_EXECUTION : EXCEPTION_EXECUTE_HANDLER)
  {
  }
}

#endif

struct ThreadInfo
{
  DWORD _threadId;
  int _cpu;
  RString _name;

  ThreadInfo(DWORD threadId, int cpu, RString name)
    : _threadId(threadId), _cpu(cpu), _name(name) {}
};
TypeIsMovableZeroed(ThreadInfo)

struct ThreadInfoTraits
{
  typedef DWORD KeyType;
  /// check if two keys are equal
  static bool IsEqual(DWORD a, DWORD b) {return a == b;}
  /// get a key from an item
  static DWORD GetKey(const ThreadInfo &a) {return a._threadId;}
};

static FindArrayKey<ThreadInfo, ThreadInfoTraits> ThreadsMap;

void RegisterThread(DWORD threadId, int cpu, const char *name)
{
  ThreadsMap.AddUnique(ThreadInfo(threadId, cpu, name));
}

RString GetThreadName(DWORD threadId)
{
  int index = ThreadsMap.FindKey(threadId);
  if (index < 0) return RString();
  return ThreadsMap[index]._name;
}

int GetThreadCPU(DWORD threadId)
{
  int index = ThreadsMap.FindKey(threadId);
  if (index < 0) return -1;
  return ThreadsMap[index]._cpu;
}

struct InheritFPUSettingsContext
{
  LPTHREAD_START_ROUTINE threadProc;
  void *context;
  
  // x87 control word to inherit
  int fpuControl;
  #if USING_SSE
    // SSE control word to inherit
    int mmCSR;
  #endif
};

#ifdef _XBOX
// On X360 the _control87 seems to be supported by the headers, but not by the library
// We replace it with _controlfp, which does not handle denormals, but otherwise is identical
#define _control87 _controlfp
#endif

static DWORD WINAPI InheritFPUSettings(void *context)
{
  InheritFPUSettingsContext *fpuContext = (InheritFPUSettingsContext *)context;
  _control87(fpuContext->fpuControl,~0);
  // the main purpose of this is to inherit precision - check if we have been sucessfull
  Assert((_controlfp(0, 0)&_MCW_PC)==(fpuContext->fpuControl&_PC_24));
  #if USING_SSE
     _mm_setcsr(fpuContext->mmCSR);
  #endif
  
  void *threadContext = fpuContext->context;
  LPTHREAD_START_ROUTINE start = fpuContext->threadProc;
  delete fpuContext;
  return start(threadContext);
}

/**
@param threadId [out] [optional] return thread ID value
*/
HANDLE CreateThreadOnCPU(
  DWORD stackSize, LPTHREAD_START_ROUTINE startAddress, LPVOID parameter, int cpu, int priority, const char *name,
  DWORD *threadIdRet
)
{
  DWORD threadId;
  // dynamic allocation needed here, we cannot use stack storage
  // this function can terminate (and stack vars are destroyed) before the new thread processed the data
  InheritFPUSettingsContext *fpuContext = new InheritFPUSettingsContext;
  fpuContext->context = parameter;
  fpuContext->threadProc = startAddress;

  // store the FPU control word
  fpuContext->fpuControl = _control87(0,0);
  #if USING_SSE
    fpuContext->mmCSR = _mm_getcsr();
  #endif

  HANDLE handle = CreateThread(NULL, stackSize, InheritFPUSettings, fpuContext, CREATE_SUSPENDED, &threadId);
  if (handle)
  {
#ifdef _XBOX
    XSetThreadProcessor(handle, cpu);
#else
    // TODO: ??? place thread on given CPU using SetThreadAffinityMask or SetThreadIdealProcessor
#endif

# if !_SUPER_RELEASE
    if (name) SetThreadName(threadId, name);
# endif
    RegisterThread(threadId, cpu, name);

    SetThreadPriority(handle, priority);

    ResumeThread(handle);
    
    if (threadIdRet) *threadIdRet = threadId;
  }
  return handle;
}
