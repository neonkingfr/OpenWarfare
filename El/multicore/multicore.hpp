/// Helper functions and classes for environment with multiple CPUs

#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MULTICORE_HPP
#define _MULTICORE_HPP

#include <Es/Common/win.h>
#include <Es/Strings/rString.hpp>

/// force the number of CPUs GetCPUCount() will return
void ForceCPUCount(int count);

#ifndef _XBOX
#include <intrin.h>

#if _DEBUG // without debug intrinsics are allowed globally
  #pragma intrinsic ( _InterlockedAnd ) 
  #pragma intrinsic ( _InterlockedOr ) 
  #pragma intrinsic ( _InterlockedXor ) 
#endif

#else

extern "C" void _ReadWriteBarrier ();
extern "C" void _ReadBarrier ();
extern "C" void _WriteBarrier ();

#pragma intrinsic ( _ReadWriteBarrier ) 
#pragma intrinsic ( _ReadBarrier ) 
#pragma intrinsic ( _WriteBarrier ) 

#endif

/// return the current number of CPUs
int GetCPUCount();
/// create the thread on given CPU
HANDLE CreateThreadOnCPU(
  DWORD stackSize, LPTHREAD_START_ROUTINE startAddress, LPVOID parameter, int cpu, int priority, const char *name,
  DWORD *threadId=NULL
);

/// register thread to be able receive its name and cpu later
void RegisterThread(DWORD threadId, int cpu, const char *name);
/// find the name of the registered thread
RString GetThreadName(DWORD threadId);
/// find the cpu of the registered thread
int GetThreadCPU(DWORD threadId);

/// int with atomic (interlocked) operations
class AtomicInt
{
  volatile LONG _value;

  public:  
  operator int() const volatile {return _value;}
  //operator AtomicInt &() {return _value;}
  explicit AtomicInt(int val) {_value = val;}
  /// having default constructor as uninitialized is a little bit unsafe, but performant
  AtomicInt(){}
  
  void operator = (const AtomicInt &src) volatile {_value = src._value;}
  
  //@{ note: prefix implementation not native, but we can derive original value from the resulting one
  int operator ++(int postfix) volatile {return InterlockedIncrement(&_value)-1;}
  int operator --(int postfix) volatile {return InterlockedDecrement(&_value)+1;}
  //@}
  int operator ++() volatile {return InterlockedIncrement(&_value);}
  int operator --() volatile {return InterlockedDecrement(&_value);}
  /** note: native implementation returns original value, but we can derive the new one */
  int operator += (int x) volatile {return InterlockedExchangeAdd(&_value,x)+x;}
  int operator -= (int x) volatile {return InterlockedExchangeAdd(&_value,-x)-x;}
  // caution: X360 and PC returns a different value from _InterlockedOr/_InterlockedAnd - do not rely upon it
  void operator |= (int x) volatile {_InterlockedOr(&_value,x);}
  void operator &= (int x) volatile {_InterlockedAnd(&_value,x);}
  
};

#ifdef _XBOX

  /** following technique comes from:
  Subject: Re: X360 equivalent of mm_pause
  From: "Jason Denton" <jason.denton@bizarrecreations.com>
  References: <4Rd01AGaIHA.404@TK2ATGFSA01.phx.gbl> <pIv#piGaIHA.6012@TK2ATGFSA01.phx.gbl>
  Date: Thu, 7 Feb 2008 17:22:47 -0000
  Message-ID: <TCOvW4aaIHA.5816@TK2ATGFSA01.phx.gbl>
  Newsgroups: xds360.cpu
  Path: TK2ATGFSA01.phx.gbl
  Xref: TK2ATGFSA01.phx.gbl xds360.cpu:1867


  I noticed that our own idle loop (for a job processor thread when the queue 
  is empty) implemented with YieldProcessor() calls was actually causing the 
  other hyperthread to run slightly slower (+5%) than with the OS idle thread.
  I tracked it down to the instruction mix in the idle loop, the naive 
  approach of just throwing in a load (say 32) of YieldProcessor() calls 
  appears to cause stalls that have an impact on the execution of the other 
  hyperthread. The best mix I've found so far is:

  YieldProcessor(); __asm { or r0,r0,r0 } YieldProcessor(); __asm { or 
  r1,r1,r1 } // x8 in a loop

  Which gives the same performance as the OS idle thread.
  */

  // source: Re: X360 equivalent of mm_pause
  #define YieldProcessorNice() \
        YieldProcessor(); __asm { or r0,r0,r0 } YieldProcessor(); __asm { or r1,r1,r1 }; \
        YieldProcessor(); __asm { or r0,r0,r0 } YieldProcessor(); __asm { or r1,r1,r1 }; \
        YieldProcessor(); __asm { or r0,r0,r0 } YieldProcessor(); __asm { or r1,r1,r1 }; \
        YieldProcessor(); __asm { or r0,r0,r0 } YieldProcessor(); __asm { or r1,r1,r1 }; \
        YieldProcessor(); __asm { or r0,r0,r0 } YieldProcessor(); __asm { or r1,r1,r1 }; \
        YieldProcessor(); __asm { or r0,r0,r0 } YieldProcessor(); __asm { or r1,r1,r1 }; \
        YieldProcessor(); __asm { or r0,r0,r0 } YieldProcessor(); __asm { or r1,r1,r1 }; \
        YieldProcessor(); __asm { or r0,r0,r0 } YieldProcessor(); __asm { or r1,r1,r1 } \

#elif _M_IX86_FP>0 // when SSE enabled, use it

  // TODO: check YieldProcessor vs _mm_pause efficiency
  #define YieldProcessorNice() YieldProcessor();YieldProcessor();YieldProcessor();YieldProcessor()

#else

  // YieldProcessor() is portable
  #define YieldProcessorNice() YieldProcessor()

#endif

/// publish any data modified so far (Release semantics, done before advertising)
/**
See Lockless Programming Considerations for Xbox 360 and Microsoft Windows
*/
static __forceinline void MemoryPublish()
{
  #ifndef _DEBUG
    // cannot use in debug: linker errors because of missing function
    // not a real problem: with debug no optimizations done, no compiler barriers needed
    _WriteBarrier();
  #endif
  #if _XBOX
    // note: docs say this is both CPU and compiler barrier
    ReleaseLockBarrier();
  #else
    // make sure write complete before any reads are started (on x86 reads can be reordered ahead of writes)
    #if _M_IX86_FP>0
      // MemoryBarrier not using SSE on Win32, only xchng, explicit SSE seems more efficient
      _mm_sfence();
    #else
      MemoryBarrier();
    #endif
  #endif
}

/// subsribe to data (Acquire semantics, done after checking)
static __forceinline void MemorySubscribe()
{
  #ifndef _DEBUG
    _ReadBarrier();
  #endif
  #if _XBOX
    // note: docs say this is both CPU and compiler barrier
    AcquireLockBarrier();
  #else
    // no CPU memory barrier required on x86 - reads not reordered by CPU
  #endif

}

#endif
