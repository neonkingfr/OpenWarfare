/// Job base class, Job Manager class implementation

#include <El/elementpch.hpp>
#include "jobs.hpp"
#include <Es/Containers/staticArray.hpp>
#include "multicore.hpp"
# include <El/Common/perfProf.hpp>

// switch on to measure overhead cost of micro-jobs 
#define MICRO_TASKS_PERF 0

Job::Job()
{
  _jobState = JSNone;
  _jobCommand = JCNone;
  _threadNumber = -1;
  _jobFinishUntil = 0;
}

void JobsQueue::Insert(Job *job)
{
  Assert(job);
  ScopeLockSection lock(_queueLock);
  _queue.HeapInsert(job);
  _submitSemaphore.Unlock();
}

bool JobsQueue::RemoveFirst(Job *&result)
{
  ScopeLockSection lock(_queueLock);
  return _queue.HeapRemoveFirst(result);
}

bool JobsQueue::Cancel(Job *job)
{
  Assert(job);
  ScopeLockSection lock(_queueLock);
  int index = _queue.Find(job);
  if (index < 0) return false;
  // try to recover the semaphore
  if (!_submitSemaphore.TryLock())
  {
    LogF("Cancel job did not lock the submit semaphore, lock count broken");
  }
  // we can remove it now
  _queue[index]->_jobState = Job::JSCanceled;
  _queue.HeapDeleteAt(index);
  return true;
}

void JobsQueue::Clear()
{
  ScopeLockSection lock(_queueLock);
  // recover the semaphore
  for (int i=0; i<_queue.Size(); i++)
  {
    Verify(_submitSemaphore.TryLock());
    _queue[i]->_jobState = Job::JSCanceled;
  }
  _queue.Clear();
}

bool JobManagerThread::Init(int threadNumber, JobsQueue *queue)
{
  DoAssert(!_threadHandle); // avoid multiple initialization

  _threadNumber = threadNumber;
  _queue = queue;

  int cpuNumber = threadNumber + 1; // begin from CPU #1
  #if _XBOX
  // on Xbox use CPUs in following order for jobs:
  // we want to prevent sharing the physical CPU 0 between the main thread and the jobs as much as possible
  static const int cpuOrder[]={2,5,1,3,4};
  if (threadNumber<lenof(cpuOrder))
  {
    cpuNumber = cpuOrder[threadNumber]; // begin from CPU #1
  }
  #endif
  // do not limit the main application and micro-jobs threads
  HANDLE handle = CreateThreadOnCPU(64 * 1024, JobManagerThread::ThreadProcedureCallback, this,
    cpuNumber, THREAD_PRIORITY_LOWEST, Format("Job#%d", threadNumber+1));
  if (handle)
  {
    _threadHandle.Init(handle);
    return true;
  }
  else
  {
    RptF("Cannot create job manager thread");
    return false;
  }
}

void JobManagerThread::Done()
{
  if (_threadHandle)
  {
    // tell the thread to finish
    _terminateEvent.Set();
    // tell the current job to finish immediately
    Cancel();
    // wait until it is done
    _threadHandle.Wait();
    // finish
    _threadHandle.Done();
  }
}

void JobManagerThread::Cancel()
{
  {
    // make sure nobody will set _currentJob to NULL
    ScopeLockSection lock(_currentJobLock);
    if (!_currentJob) return;

    // cancel and wait until done
    _currentJob->CancelJob();
  }
  _cancelEvent.Wait();
}

bool JobManagerThread::Cancel(Job *job)
{
  {
    // make sure nobody will set _currentJob to NULL
    ScopeLockSection lock(_currentJobLock);
    if (_currentJob != job) return false;

    // cancel and wait until done
    _currentJob->CancelJob();
  }
  _cancelEvent.Wait();
  return true;
}

DWORD JobManagerThread::ThreadProcedureCallback(void *context)
{
  // redirect to the member function
  JobManagerThread *instance = reinterpret_cast<JobManagerThread *>(context);
  return instance->ThreadProcedure();
}

DWORD JobManagerThread::ThreadProcedure()
{
  #ifndef _M_PPC
  // there is no precision control on PPC, precision is controlled by instructions instead
  Assert((_controlfp(0, 0)&_MCW_PC)==_PC_24);
  #endif
  while (true)
  {
    // wait until some job is ready or termination
    SignaledObject *handles[] = {&_terminateEvent, _queue->GetSubmitSemaphore()};
    int result = SignaledObject::WaitForMultiple(handles, lenof(handles));
    if (result == 0) break; // done

    // get the job
    if (!_queue->RemoveFirst(_currentJob) || _currentJob == NULL)
    {
      LogF("Announced job not found in the queue, lock count recovered after Cancel job.");
      continue;
    }
    // set the properties
    _currentJob->_jobState = Job::JSRunning;
    _currentJob->_threadNumber = _threadNumber;
    // work
    bool done = _currentJob->Process();

    Job *currentJob = _currentJob;
    {
      ScopeLockSection lock(_currentJobLock);
      _currentJob = NULL;
      // currentJob cannot be canceled from now
    }

    // ensure Process finished with some reason
    Assert(done || currentJob->_jobCommand != Job::JCNone);
    // fix the state - do not suspend finished task
    if (done && currentJob->_jobCommand == Job::JCSuspend) currentJob->_jobCommand = Job::JCNone;

    // we are done with the job, set the properties
    currentJob->_threadNumber = -1;
    switch (currentJob->_jobCommand)
    {
    case Job::JCNone:
      currentJob->_jobState = Job::JSDone;
      break;
    case Job::JCSuspend:
      currentJob->_jobState = Job::JSSuspended;
      currentJob->_jobCommand = Job::JCNone;
      break;
    case Job::JCCancel:
      currentJob->_jobState = Job::JSCanceled;
      currentJob->_jobCommand = Job::JCNone;
      // let the canceling thread known it is done
      _cancelEvent.Set();
      break;
    }
  }
  return 0;
}

/**
@param threadNumber 1 .. CPUCount()-1
*/
bool MicroJobsThread::Init(int threadNumber, MicroJobsList *jobs)
{
  DoAssert(!_threadHandle); // avoid multiple initialization

  _jobs = jobs;

  int cpuNumber = threadNumber+1; // begin from CPU #1

  #if _XBOX
  // on Xbox use CPUs in following order for jobs:
  // we want to prevent sharing the physical CPU 0 between the main thread and the jobs as much as possible
  static const int cpuOrder[]={2,5,1,3,4};
  if (threadNumber<lenof(cpuOrder))
  {
    cpuNumber = cpuOrder[threadNumber]; // begin from CPU #1
  }
  #endif

  // do not limit the main application and micro-jobs threads
  HANDLE handle = CreateThreadOnCPU(512 * 1024, MicroJobsThread::ThreadProcedureCallback,
    this, cpuNumber, THREAD_PRIORITY_NORMAL, Format("MJob#%d", threadNumber+1));
  if (handle)
  {
    _threadHandle.Init(handle);
    return true;
  }
  else
  {
    RptF("Cannot create micro jobs thread");
    return false;
  }
}

void MicroJobsThread::Done()
{
  if (_threadHandle)
  {
    // tell the thread to finish
    _terminateEvent.Set();
    // wait until it is done
    _threadHandle.Wait();
    // finish
    _threadHandle.Done();
  }
}

DWORD MicroJobsThread::ThreadProcedureCallback(void *context)
{
  // redirect to the member function
  MicroJobsThread *instance = reinterpret_cast<MicroJobsThread *>(context);
  return instance->ThreadProcedure();
}

DWORD MicroJobsThread::ThreadProcedure()
{
  #ifndef _M_PPC
  // there is no precision control on PPC, precision is controlled by instructions instead
  Assert((_controlfp(0, 0)&_MCW_PC)==_PC_24);
  #endif
    
  while (true)
  {
    // wait until some task is ready or termination
    SignaledObject *handles[] = {&_terminateEvent, &_jobs->_readyEvent};
    int result = SignaledObject::WaitForMultiple(handles, lenof(handles));
    if (result == 0) break; // done

    int tMask = 1<<_thrIndex;
    // check false start: if the corresponding bit in the wake up mask was already reset, ignore the request
    // it means we are in process of resetting the event
    if ((_jobs->_wakeUpMask&tMask)==0)
    {
      // WIP: PRED check why mtWai sometimes wait for nothing
      //PROFILE_SCOPE_DETAIL_EX(mJobW,*);
      // let jobs execute - important to prevent priority inversion on memory allocator CS
      SetThreadPriority(GetCurrentThread(),THREAD_PRIORITY_LOWEST); // THREAD_PRIORITY_IDLE might work as well?
      YieldProcessorNice();
      continue;
    }
    
    _jobs->_wakeUpMask &= ~tMask;
    
    PROFILE_SCOPE_EX(mJob,*);

    // reset would be nice here, but it causes a race condition (Reset may be done after Set for the next data)
    // if (_jobs->_wakeUpMask==0) _jobs->_readyEvent.Reset()
    
    // some jobs are ready, process all of them
    MicroJobsList::Schedule *schedule = &_jobs->_schedule[_jobs->_actSchedule[_thrIndex]];
    while (true)
    {
      // get the task
      int index = InterlockedIncrementAcquire(&schedule->_started);
      if (index >= schedule->_end)
      {
        // no more jobs in our schedule, try to switch to another schedule to steal
        for (int so = 1; so<_jobs->_threadCount; so++)
        {
          int s = _thrIndex+so;
          if (s>=_jobs->_threadCount) s -= _jobs->_threadCount;
          // try stealing
          MicroJobsList::Schedule *stealFrom = &_jobs->_schedule[s];
          index = InterlockedIncrementAcquire(&stealFrom->_started);
          if (index >= stealFrom->_end) continue;
          // next iteration attempt the same stealing
          _jobs->_actSchedule[_thrIndex] = s;
          goto DoTask;
        }
        // if there is nobody to steal from, terminate
        break;
      }
      DoTask:
      // execute the task
      IMicroJob *task = _jobs->_jobsData[index];
      (*task)(_context,_thrIndex);
    }
    // signal thread finish
    InterlockedDecrementRelease(&_jobs->_threadsPending);
  }
  return 0;
}

JobManager::JobManager()
{
  #ifdef _XBOX
  // CPU #4 is used for audio driver, which would pre-empt us often.
  _microJobsCountLimit=5;
  #else
  // we need to be able to represent CPUs using a 32b bitmap
  _microJobsCountLimit=31;
  #endif
}
JobManager::~JobManager()
{
  Done();
}

void JobManager::Init()
{
  // decide what number of threads we will manage
  int cpuCount = GetCPUCount();
  if (cpuCount <2) return; // no available CPU
  // to stay future proof, ignore more CPUs than we can handle
  if (cpuCount>MaxMicroJobThreads) cpuCount = MaxMicroJobThreads;
  #ifdef _XBOX
  // leave one core for service tasks + audio driver on X360 - experiments shown 5 core performance is the best
  int threadsCount = cpuCount - 2;
  #else
  // we could consider leaving one CPU for service threads when available on multi-core systems 
  // TODO: benchmark on QuadCore PC
  //int threadsCount = intMax(1, cpuCount - 2);
  int threadsCount = cpuCount - 1;
  #endif

  _microJobsThreads.Realloc(threadsCount);
  for (int i=0; i<threadsCount; i++)
  {
    // index as i+1, because 0 is used for the main thread
    Ref<MicroJobsThread> thread = new MicroJobsThread(i+1);
    if (thread->Init(i, &_microJobsList)) _microJobsThreads.Add(thread);
  }

  #ifdef _XBOX
    // allow at most 2 job threads
    // use physical CPUs only, do not use the primary CPU
    if (threadsCount>2) threadsCount = 2;
  #endif
  _threads.Realloc(threadsCount);
  for (int i=0; i<threadsCount; i++)
  {
    Ref<JobManagerThread> thread = new JobManagerThread();
    if (thread->Init(i, &_queue)) _threads.Add(thread);
  }
}

void JobManager::Done()
{
  // TODO: optimization - finish all threads at once instead one by one
  _queue.Clear();
  _threads.Clear();

  _microJobsThreads.Clear();
}

void JobManager::CancelAllJobs()
{
  _queue.Clear();
  // let threads running, only cancel the running jobs
  for (int i=0; i<_threads.Size(); i++) _threads[i]->Cancel();
}

void JobManager::CreateJob(Job *job, DWORD timeMs)
{
  Assert(job->_jobState == Job::JSNone || job->_jobState == Job::JSCanceled || job->_jobState == Job::JSDone);
  Assert(job->_jobCommand == Job::JCNone);
  Assert(job->_threadNumber == -1);

  job->_jobState = Job::JSWaiting;
  job->_jobFinishUntil = GlobalTickCount() + timeMs;
  _queue.Insert(job);
}

void JobManager::ResumeJob(Job *job)
{
  Assert(job->_jobState == Job::JSSuspended);
  Assert(job->_jobCommand == Job::JCNone);
  Assert(job->_threadNumber == -1);

  job->_jobState = Job::JSWaiting;
  // keep the _jobFinishUntil unchanged
  _queue.Insert(job);
}

void JobManager::CancelJob(Job *job)
{
  if (job->_jobState != Job::JSWaiting && job->_jobState != Job::JSRunning)
  {
    if (job->_jobState == Job::JSSuspended) job->_jobState = Job::JSCanceled;
    return; // out of our control already
  }
  if (job->_jobState == Job::JSWaiting)
  {
    // could be in the queue
    if (_queue.Cancel(job))
    {
      Assert(job->_jobState == Job::JSCanceled);
      return; // found
    }
    // launched meanwhile
  }
  for (int i=0; i<_threads.Size(); i++)
  {
    if (_threads[i]->Cancel(job))
    {
      Assert(job->_jobState == Job::JSCanceled);
      return; // found
    }
  }
  Assert(job->_jobState == Job::JSNone || job->_jobState == Job::JSCanceled || job->_jobState == Job::JSDone);
}

/// Yield CPU, automatically lower priority as needed to handle when CPU is not available
static inline void YieldProcessorNiceAutoPriority(int &spinCount, int countToIdle)
{
  #ifndef _XBOX // no reason to do this on Xbox, nobody can use CPUs without us knowing
  if (--spinCount<=0)
  {
    // when spinning too long with no result, lower priority
    // this should help when no CPU is ready
    // priority of the background thread is Below normal, we want to wait for it
    // once it finishes its work, it will wait using WaitForMultipleObjects
//     if (spinCount==0)
//     {
//       SetThreadPriority(GetCurrentThread(),THREAD_PRIORITY_LOWEST);
//     }
    #if 0 // it seems going to Idle does more harm then good, too many processes can prevent us schedulling then
    else if (spinCount==countToIdle)
    {
      // done to workaround nVidia priority inversion driver bug (IDLE thread holding RtlHeap critical section)
      //LogF("Background thread posible deadlock detected (%d ms), going IDLE to recover",GlobalTickCount()-time);
      SetThreadPriority(GetCurrentThread(),THREAD_PRIORITY_IDLE);
    }
    #endif
  }
  #endif
  YieldProcessorNice();
}

static inline void YieldProcessorNiceAutoPriorityCleanup(int spinCount)
{
  if (spinCount<=0)
  {
    SetThreadPriority(GetCurrentThread(),THREAD_PRIORITY_NORMAL);
  }
}

void JobManager::ProcessMicroJobs(IMTTContext *context, int contextSize, IMicroJob *const*jobsData, int jobsCount)
{
  // if there is zero jobs, no thread would start any task and no _doneEvent would be set
  // to prevent this, we are calling serial implementation
  // calling it when there is only one task is most likely smart as well,
  // as it removes the synchronization overhead in such case
  // perhaps the limit should be even higher (depending on the task duration)
  if (_microJobsThreads.Size()<=0 || jobsCount<=1)
  {
    // special case - no MT safety or synchronization needed
    ProcessMicroJobsSerial(context,contextSize,jobsData,jobsCount);
    return;
  }
  // we will never spawn more than allowed by a limit
  int spawnThreads = intMin(_microJobsThreads.Size(),_microJobsCountLimit-1);
  int threadCount = spawnThreads+1;
  // prepare the thread contexts
  AutoArray< char, MemAllocLocal<char, 1024> > contexts; // avoid allocation
  if (context)
  {
#if MICRO_TASKS_PERF
    PROFILE_SCOPE_GRF_EX(mtCtx, *, 0xffc000c0);
#endif

    contexts.Resize(spawnThreads * contextSize);
    char *ptr = contexts.Data();
    for (int i=0; i<spawnThreads; i++)
    {
      MicroJobsThread *thread = _microJobsThreads[i];
      thread->SetContext(context->CreateContext(ptr));
      ptr += contextSize;
    }
  }
  else
  {
    // make sure the variable is initialized
    for (int i=0; i<spawnThreads; i++)
    {
      MicroJobsThread *thread = _microJobsThreads[i];
      thread->SetContext(NULL);
    }
  }

  // natural mask would be (1<<spawnThreads)-1, but we shift the result <<1 one more
  // so that we have the bits numbered as CPUs, with #0 skipped
  int tMask = (2<<spawnThreads)-2;
  
  // initialize the task list
  {
#if MICRO_TASKS_PERF
    PROFILE_SCOPE_GRF_EX(mtIni, *, 0xffc000c0);
#endif
    _microJobsList._jobsData = jobsData;
    _microJobsList._jobsCount = jobsCount;
    _microJobsList._threadsPending = spawnThreads; // how many --jobs-- threads we need to wait for
    _microJobsList._threadCount = threadCount;
    // for debugging purposes we may want to avoid any microjobs on the main thread
    #define NO_MAIN_THREAD_JOBS 0
    #if !NO_MAIN_THREAD_JOBS
    // we need to setup schedule limits first
    int current = 0;
    // compute quotient and remainder
    // first remainder thread get quotient+1, all other get quotient
    // this makes sure thread 0 is always the most saturated one (reduces waiting on it)
    // at the same time the distribution is as even as possible
    int perThread = jobsCount/threadCount;
    int perThreadRem = jobsCount-threadCount*perThread;
    for (int i=0; i<threadCount; i++)
    {
      _microJobsList._schedule[i]._beg = current;
      current += i<perThreadRem ? perThread+1 : perThread;
      _microJobsList._schedule[i]._end = current;
      // start with no stealing (each thread processing its own schedule)
      _microJobsList._actSchedule[i] = i;
    }
    DoAssert(current==jobsCount);
    #else
    // debugging - setup everything into thread #1
    for (int i=0; i<threadCount; i++)
    {
      _microJobsList._schedule[i]._beg = 0;
      _microJobsList._schedule[i]._end = i==1 ? jobsCount : 0;
      // start with no stealing (each thread processing its own schedule)
      _microJobsList._actSchedule[i] = i;
    }
    #endif
    MemoryPublish(); // make sure all above is visible before we start the task
    // now indicate each schedule is ready to run
    for (int i=0; i<threadCount; i++)
    {
      // no task started yet - point before the beginning
      _microJobsList._schedule[i]._started = _microJobsList._schedule[i]._beg-1;
      // reset all thread priorities back to normal, so that they take precedence over jobs
    }

    
    // wake up worker threads
    // as a handshake from threads that they received we expect each thread to reset its bit
    MemoryPublish(); // make sure schedule[i]._started is set before _wakeUpMask
    _microJobsList._wakeUpMask = AtomicInt(tMask);
    MemoryPublish();
    // set priority after wake up mask to prevent race conditions (thread could lower it again)
    for (int i=0; i<spawnThreads; i++)
    {
      SetThreadPriority(_microJobsThreads[i]->GetThreadHandle(),THREAD_PRIORITY_NORMAL);
    }
    // we use manual reset event as an "wakeup" advisory
    _microJobsList._readyEvent.Set();
  }

  // the main thread will work simultaneously with the working threads
  MicroJobsList::Schedule *schedule = &_microJobsList._schedule[_microJobsList._actSchedule[0]];
  while (true)
  {
    // get the task
    int index = InterlockedIncrementAcquire(&schedule->_started);
    if (index >= schedule->_end)
    {
      #if !NO_MAIN_THREAD_JOBS
      // no more jobs in our schedule, try to switch to another schedule to steal
      for (int s = 1; s<threadCount; s++)
      {
        // try stealing
        MicroJobsList::Schedule *stealFrom = &_microJobsList._schedule[s];
        index = InterlockedIncrementAcquire(&stealFrom->_started);
        if (index >= stealFrom->_end) continue;
        // next iteration attempt the same stealing
        _microJobsList._actSchedule[0] = s;
        goto DoTask;
      }
      #endif
      // if there is nobody to steal from, terminate
      break;
    }
    DoTask:

    // execute the task
    IMicroJob *task = _microJobsList._jobsData[index];
    // calling from the main thread - ID is always zero
    (*task)(context,0);

  }

  {
    PROFILE_SCOPE_GRF_EX(mtWai, *, 0xffc000c0);


    // spin-wait is efficient on the main thread, because we are not scheduling any other work on it
    // wait for: all threads reporting started, all threads reporting completed
    //   while (_microJobsList._wakeUpMask!=0 || _microJobsList._threadsPending!=0) YieldProcessorNice();
    // wait for conditions one by one - this provides lower overhead and better reaction time
    // in a typical case threads wake up first, complete later
    // spinCount = 1000 corresponds approx. to 0.1 ms on 2.4 GHz Intel Core CPU
    int spinCount = 1000;
    #ifndef _XBOX
      int countToIdle = GetCPUCount()==1 ? -20000 : -200000 ; // cca 1 ms : cca 10 ms
      #if _ENABLE_REPORT
        //DWORD time = GlobalTickCount();
      #endif
    #else
      int countToIdle = spinCount;
    #endif
    
    while (_microJobsList._wakeUpMask!=0) YieldProcessorNiceAutoPriority(spinCount,countToIdle);
    while (_microJobsList._threadsPending!=0) YieldProcessorNiceAutoPriority(spinCount,countToIdle);
    _microJobsList._readyEvent.Reset();
    YieldProcessorNiceAutoPriorityCleanup(spinCount);
  }
  
  // clean-up
#if MICRO_TASKS_PERF
  PROFILE_SCOPE_GRF_EX(mtDon, *, 0xffc000c0);
#endif
  _microJobsList._jobsData = NULL;
  _microJobsList._jobsCount = 0;
  for (int i=0; i<_microJobsThreads.Size()+1; i++)
  {
    // values selected so that writing sensible values into _end / _started in any order does not start processing
    _microJobsList._schedule[i]._beg = 0;
    _microJobsList._schedule[i]._end = 0;
    _microJobsList._schedule[i]._started = INT_MAX / 2;
  }
  MemoryPublish(); // make sure the _jobsCount is visible before we submit new jobs
  
  // collect contexts from threads
  if (context)
  {
    for (int i=0; i<_microJobsThreads.Size(); i++)
    {
      MicroJobsThread *thread = _microJobsThreads[i];
      context->AggregateContext(thread->GetContext());
      thread->SetContext(NULL);
    }
  }
}

void JobManager::ProcessMicroJobsSerial(IMTTContext *context, int contextSize, IMicroJob *const*jobsData, int jobsCount)
{
  // if there is a context, there needs to be exactly one
  Assert(!context || contextSize==1);

  // the main thread is the only one working here
  for (int index=0; index<jobsCount; index++)
  {
    // execute the task
    IMicroJob *task = jobsData[index];
    (*task)(context,0);
  }
}

JobManager GJobManager;
