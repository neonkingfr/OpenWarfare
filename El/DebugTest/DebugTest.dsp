# Microsoft Developer Studio Project File - Name="DebugTest" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=DebugTest - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DebugTest.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DebugTest.mak" CFG="DebugTest - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DebugTest - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "DebugTest - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Modularization/DebugTest", PSMAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DebugTest - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /W3 /GR /O2 /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "MFC_NEW" /FD /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /map /machine:I386

!ELSEIF  "$(CFG)" == "DebugTest - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GR /ZI /Od /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "MFC_NEW" /FD /GZ /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "DebugTest - Win32 Release"
# Name "DebugTest - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\DebugTest.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Group "El"

# PROP Default_Filter ""
# Begin Group "Debugging"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Debugging\debugTrap.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\debugTrap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\imexhnd.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\imexhnd.h
# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\mapFile.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Debugging\mapFile.hpp
# End Source File
# End Group
# Begin Group "CRC"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\CRC\crc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Crc\Crc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\CRC\sumCalcCrc.hpp
# End Source File
# End Group
# Begin Group "Interfaces"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Interfaces\iAppInfo.hpp
# End Source File
# End Group
# Begin Group "QStream"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\QStream\fileCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\Fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileMapping.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileOverlapped.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\fileOverlapped.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\qbstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\qstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\serializeBin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\QStream\ssCompress.cpp
# End Source File
# End Group
# Begin Group "Element Common"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\Common\globalAlive.cpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Common\perfLog.hpp
# End Source File
# End Group
# Begin Group "PCH"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\PCH\afxConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\El\Pch\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Pch\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=..\..\El\Pch\normalConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\El\PCH\sReleaseConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\El\Pch\stdIncludes.h
# End Source File
# End Group
# Begin Group "AppInfo"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\El\AppInfo\useAppInfoDefault.cpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\El\elementpch.hpp
# End Source File
# End Group
# Begin Group "Es"

# PROP Default_Filter ""
# Begin Group "Containers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Containers\Array.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\cachelist.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\List.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\smallArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Containers\typeOpts.hpp
# End Source File
# End Group
# Begin Group "Memory"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Memory\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Memory\normalNew.hpp
# End Source File
# End Group
# Begin Group "Framework"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Framework\useAppFrameDefault.cpp
# End Source File
# End Group
# Begin Group "Types"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\Memtype.h
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\Pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Types\scopeLock.hpp
# End Source File
# End Group
# Begin Group "Common"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Common\Fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Common\Win.h
# End Source File
# End Group
# Begin Group "Strings"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Strings\Bstring.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Strings\rString.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Strings\rString.hpp
# End Source File
# End Group
# Begin Group "Threads"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Threads\multisync.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\threadSync.cpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Threads\threadSync.hpp
# End Source File
# End Group
# Begin Group "Algorithms"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\Es\Algorithms\Qsort.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\Es\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\Es\Platform.hpp
# End Source File
# End Group
# Begin Source File

SOURCE=..\..\El\Debugging\module.xml
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
