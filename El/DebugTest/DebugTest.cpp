// DebugTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <El/elementpch.hpp>
#include <El/Debugging/debugTrap.hpp>

int main(int argc, char* argv[])
{
	GDebugger.ForceLogging();

	GDebugger.ResumeCheckingAlive();
	GDebugger.NextAliveExpected(5000);

	while (true) {}
	return 0;
}

