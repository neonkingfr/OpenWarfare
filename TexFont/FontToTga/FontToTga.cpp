// FontToTga.cpp : Defines the entry point for the console application.
//

#include <Es/essencepch.hpp>

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#include <afxwin.h>         // MFC core and standard components

#include <Es/Strings/rString.hpp>

#include <stdio.h>

#include <El/ParamFile/paramFile.hpp>

#include "IntLetter.h"

static void PrintUsage()
{
  printf("Usage:\n");
  printf("   FontToTga {options} font [destination]\n");
  printf("\n");
  printf("   Options:\n");
  printf("   -bold\n");
  printf("   -italic\n");
  printf("   -size=size\n");
}

static RString SkipSpaces(RString src)
{
  RString dst;
  const char *ptr = src;
  for (const char *p=strchr(ptr,' '); p; ptr=p+1, p=strchr(ptr,' '))
  {
    dst = dst + RString(ptr, p - ptr);
  }
  return dst + RString(ptr);
}

#define PREVIEW 0

static void FlushSet(LetterSet &set, const char *path, int setNum)
{
  char setName[1024];
  strcpy(setName, path);
  strcat(setName, "-");
  sprintf(setName + strlen(setName), "%02d", setNum);
  strcat(setName, ".tga");
  set.Save(setName, false);
#if PREVIEW
  strcpy(setName, path);
  strcat(setName, "#");
  sprintf(setName + strlen(setName), "%02d", setNum);
  strcat(setName, ".tga");
  set.Save(setName, true);
#endif
}

static RString GetModuleDirectory()
{
  char exeName[MAX_PATH];
  GetModuleFileName(NULL, exeName, MAX_PATH);
  char *ext = strrchr(exeName, '\\');
  if (ext) return RString(exeName, ext - exeName);
  return RString();
}

int main(int argc, char* argv[])
{
  // skip the exe name
  argv++;
  argc--;
  if (argc<=0)
  {
    PrintUsage();
    return 1;		
  }

  // parse options
  bool bold = false;
  bool italic = false;
  int size = 0;
  while (argc > 0)
  {
    const char *arg = *argv;
    if (!arg) {argc--; argv++; continue;}
#if __GNUC__
    if (*arg != '-') break;
#else
    if (*arg != '-' && *arg != '/') break;
#endif
    // some option
    argv++; // skip argument
    argc--;
    arg++; // skip dash
    if (stricmp(arg, "bold") == 0)
    {
      bold = true;
      continue;
    }
    if (stricmp(arg, "italic") == 0)
    {
      italic = true;
      continue;
    }
    if (strnicmp(arg, "size=", strlen("size=")) == 0)
    {
      arg += strlen("size=");
      size = atoi(arg);
      continue;
    }
    printf("Unknown option %s\n\n", arg);
    PrintUsage();
    return 1;
  }

  if (size == 0)
  {
    printf("-size=... is required\n\n");
    PrintUsage();
    return 1;
  }
  if (argc < 0 || !*argv)
  {
    printf("Font name is required\n\n");
    PrintUsage();
    return 1;
  }

  // source name
  RString srcName = *argv;
  argv++; // skip argument
  argc--;

  // destination name
  RString dstName;
  if (argc > 0)
  {
    dstName = *argv;
    argv++; // skip argument
    argc--;
  }

  if (argc > 0)
  {
    printf("Too much arguments\n\n");
    PrintUsage();
    return 1;
  }

  if (dstName.GetLength() == 0)
  {
    dstName = SkipSpaces(srcName);
    if (bold) dstName = dstName + RString("B");
    if (italic) dstName = dstName + RString("I");
    dstName = dstName + Format("%d", size);
  }

  // create memory canvas
  CDC cdc;
  cdc.CreateCompatibleDC(NULL);

  // fill the font info
  LOGFONT logFont;
  memset(&logFont, 0, sizeof(logFont));

  logFont.lfHeight = -MulDiv(size, GetDeviceCaps(cdc.GetSafeHdc(), LOGPIXELSY), 72);
  logFont.lfWidth = 0; 
  logFont.lfEscapement = 0; 
  logFont.lfOrientation = 0; 
  logFont.lfWeight = bold ? 700 : 400; 
  logFont.lfItalic = italic ? TRUE : FALSE; 
  logFont.lfUnderline = FALSE; 
  logFont.lfStrikeOut = FALSE; 
  logFont.lfCharSet = ANSI_CHARSET; 
  logFont.lfOutPrecision = OUT_TT_PRECIS; // OUT_STROKE_PRECIS not supported on Win2K/WinXP
  logFont.lfClipPrecision = CLIP_STROKE_PRECIS; 
  logFont.lfQuality = CLEARTYPE_QUALITY; // ANTIALIASED_QUALITY did not antialias some fonts
  logFont.lfPitchAndFamily = FF_DONTCARE | DEFAULT_PITCH; 
  strncpy(logFont.lfFaceName, srcName, LF_FACESIZE); 
  logFont.lfFaceName[LF_FACESIZE - 1] = 0;

  // create the font
  CFont baseFont;
  baseFont.CreateFontIndirect(&logFont);

  // set canvas dimensions and format
  CBitmap bitmap;
  bitmap.CreateBitmap(LET_X, LET_Y, 1, 32, NULL);
  cdc.SelectObject(&bitmap);

  CBrush brush(RGB(255,255,255));

  // use selected font to draw into the memory canvas

  LetterSet set;
  int setNum = 1;

  RString descName = dstName + RString(".fxy");
  QOFStream desc;
  desc.open(descName);
  int version = 0x101;
  static const char magic[]="BIFo";
  desc.write(&magic,sizeof(magic)-1);
  desc.write(&version,sizeof(version));

  // Configuration file, controlling letters present in the font
  ParamFile cfg;
  // prefer current directory
  if (!cfg.Parse("FontToTga.cfg"))
  {
    cfg.Parse(GetModuleDirectory() + RString("\\FontToTga.cfg"));
  }
  
  for (int j=0; j<cfg.GetEntryCount(); j++)
  {
    // single font page
    ParamEntryPar cls = cfg.GetEntry(j);
    if (!cls.IsClass()) continue;
    int start = (cls.CheckIfEntryExists("start")) ? cls >> "start" : 0;
    int end = (cls.CheckIfEntryExists("end")) ? cls >> "end" : 0;

    LOGFONT logFont;
    CFont overrideFont;
    if (cls.CheckIfEntryExists("overrideFont")) {
      RString overrideFontName = cls >> "overrideFont";
      memset(&logFont, 0, sizeof(logFont));
      logFont.lfHeight = -MulDiv(size, GetDeviceCaps(cdc.GetSafeHdc(), LOGPIXELSY), 72);
      logFont.lfWidth = 0; 
      logFont.lfEscapement = 0; 
      logFont.lfOrientation = 0; 
      logFont.lfWeight = 400; 
      logFont.lfItalic = FALSE; 
      logFont.lfUnderline = FALSE; 
      logFont.lfStrikeOut = FALSE; 
      logFont.lfCharSet = ANSI_CHARSET; 
      logFont.lfOutPrecision = OUT_TT_PRECIS; // OUT_STROKE_PRECIS not supported on Win2K/WinXP
      logFont.lfClipPrecision = CLIP_STROKE_PRECIS; 
      logFont.lfQuality = CLEARTYPE_QUALITY; // ANTIALIASED_QUALITY did not antialias some fonts
      logFont.lfPitchAndFamily = FF_DONTCARE | DEFAULT_PITCH; 
      strncpy(logFont.lfFaceName, overrideFontName, LF_FACESIZE); 
      logFont.lfFaceName[LF_FACESIZE - 1] = 0;

      overrideFont.CreateFontIndirect(&logFont);
      cdc.SelectObject(&overrideFont);
      printf("\n\"%s\" font override ", overrideFontName.Data());
    }
    else {
      cdc.SelectObject(&baseFont);
      printf("\n");
    }
    if (start || end) printf("%04X..%04X", start,end);

    // convert each letter into single-plane bitmap
    for (int i=start; i<end; i++)
    {
      // init the bitmap
      cdc.FillRect(CRect(0, 0, LET_X, LET_Y), &brush);

      // write down the character
      wchar_t text[2];
      text[0] = i;
      text[1] = 0;
      // leave 1 pixel border
      ::TextOutW(cdc.GetSafeHdc(), 1, 1, text, 1);

      // convert bitmap into internal font format
      Buffer<char> letter;
      BITMAP size;
      bitmap.GetBitmap(&size);
      letter.Init(size.bmWidthBytes * size.bmHeight);

      bitmap.GetBitmapBits(letter.Size(),letter.Data());

      IntLetter letterPic;
      letterPic.Load(size, letter);

      // use TGA32 format to save data
      if (!set.AddLetter(i, letterPic))
      {
        // next texture
        set.AddDesc(desc, descName, setNum);
        FlushSet(set, dstName, setNum++);
        set.Clear();
        set.AddLetter(i, letterPic);
      }
    }

    if (cls.CheckIfEntryExists("list")) {
      ParamEntryPar list = cls >> "list";
      if (list.IsArray()) {
        if (start || end) printf(" + %d chars", list.GetSize());
        else printf("%d chars", list.GetSize());

        for (int j=0; j<list.GetSize(); j++) {
          int i = list[j];

          // init the bitmap
          cdc.FillRect(CRect(0, 0, LET_X, LET_Y), &brush);

          // write down the character
          wchar_t text[2];
          text[0] = i;
          text[1] = 0;
          // leave 1 pixel border
          ::TextOutW(cdc.GetSafeHdc(), 1, 1, text, 1);

          // convert bitmap into internal font format
          Buffer<char> letter;
          BITMAP size;
          bitmap.GetBitmap(&size);
          letter.Init(size.bmWidthBytes * size.bmHeight);

          bitmap.GetBitmapBits(letter.Size(),letter.Data());

          IntLetter letterPic;
          letterPic.Load(size, letter);

          // use TGA32 format to save data
          if (!set.AddLetter(i, letterPic))
          {
            // next texture
            set.AddDesc(desc, descName, setNum);
            FlushSet(set, dstName, setNum++);
            set.Clear();
            set.AddLetter(i, letterPic);
          }
        }
      }
    }

    // force move to the next texture before the next page
    set.AddDesc(desc, descName, setNum);
    FlushSet(set, dstName, setNum++);
    set.Clear();
  }

  desc.close();

	return 0;
}

