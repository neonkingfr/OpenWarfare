// IntLetter.cpp: implementation of the IntLetter class.
//
//////////////////////////////////////////////////////////////////////

#include "IntLetter.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Code added here will be included at the top of the .CPP file
static void fputiw( int W, QOStream &f )
{
	f.put((char)(W&0xff));
	f.put((char)(W>>8));
}


IntLetter::IntLetter()
{
	Clear();
}

IntLetter::~IntLetter()
{
	
}

//#define ENABLE_SS	0

void IntLetter::Load( BITMAP &header, const Buffer<char> &buffer )
{
	// load letter from memory buffer
	// memory buffer is single-plane (one bit per pixel)
	// starts with bitmap header
	// bmp file starts with ...
	// data contains: BITMAPFILEINFO followed by BITMAPINFO
	
	int size=buffer.Size();
  const char *data=buffer.Data();
  
  //const struct bmp_info *bInfo=(const struct bmp_info *)data;
  const char *bits=data;
  
  // we should assert that the bitmap is really 1-bit
  if( header.bmBitsPixel==8 )
	{
    Clear();
    
    int x,y;
    for( y=0; y<LET_Y; y++ )
	  {
		  if( y>=header.bmHeight ) break;
		  const char *line=&bits[y*header.bmWidthBytes];
		  for( x=0; x<LET_X; x++ )
		  {
			  if( x>=header.bmWidth ) break;
			  _pixel8[y][x]= line[x];
		  }
	  }
	}
  else if( header.bmBitsPixel==32 )
  {
    Clear();
    
    int x,y;
    for( y=0; y<LET_Y; y++ )
	  {
		  if( y>=header.bmHeight ) break;
		  const DWORD *line=(const DWORD *)&bits[y*header.bmWidthBytes];
		  for( x=0; x<LET_X; x++ )
		  {
			  if( x>=header.bmWidth ) break;
			  DWORD pixel = line[x];
			  BYTE pix = 0xff-BYTE(pixel&0xff);
			  _pixel8[y][x]= pix;
		  }
	  }
  }
	else
	{
		Fail("Wrong bitmap format");
		return;
  }
  
	CalculateWidth();
}

void IntLetter::Load( BITMAP &header, const Buffer<char> &buffer, int left, int top, int right, int bottom )
{
	int size = buffer.Size();
  const char *data = buffer.Data();
  
  //const struct bmp_info *bInfo=(const struct bmp_info *)data;
  const char *bits = data;
  
  // we should assert that the bitmap is really 1-bit
  if( header.bmBitsPixel!=1 )
	{
		//Fail("Wrong bitmap format");
		return;
	}
  
  Clear();
  
  int x,y;
  for( y=top; y<LET_Y; y++ )
	{
		if( y>=header.bmHeight ) break;
		const char *line=&bits[y*header.bmWidthBytes];
		for( x=left; x<LET_X; x++ )
		{
		   if( x>=header.bmWidth ) break;
			_pixel8[y - top][x - left]= line[x];
		}
	}

	_width = right - left;
	_height = bottom - top;
}

void IntLetter::Clear()
{
	int x,y;
	for( y=0; y<LET_Y; y++ ) for( x=0; x<LET_X; x++ )
  {
		_pixel8[y][x]=0;
	}
	_width=0,_height=0;
}

void IntLetter::CalculateWidth()
{
  const int aaTholdOne = 0x80;
  const int aaTholdFour = 0x40;
	int x,y;
	// calculate width
	for( x=LET_X; --x>=0; )
    for( y=LET_Y; --y>=0; )
		{
			if( _pixel8[y][x] ) goto WBreak;
		}
	WBreak:
	_width=x+1;
	// calculate height
	for( y=LET_Y; --y>=0; )
		for( x=LET_X; --x>=0; )
		{
			if( _pixel8[y][x] ) goto HBreak;
		}
	HBreak:
	_height=y+1;

  // if first column is empty and 2nd is antialiasing only, delete the first one
  if (_width>=2)
  {
    int aaSum = 0;
    for (int yy=0; yy<_height; yy++)
    {
      if (_pixel8[yy][0]>0 || _pixel8[yy][1]>=aaTholdOne)
      {
        goto NotTwoEmpty;
      }
      aaSum += _pixel8[yy][1];
      if (aaSum>=aaTholdFour*4) goto AABreak;
    }

    // delete the first column
    for (int yy=0; yy<_height; yy++)
    {
      for (int xx=0; xx<_width-1; xx++)
      {
        _pixel8[yy][xx] = _pixel8[yy][xx+1];
      }
      _pixel8[yy][_width-1]=0;
    }
    _width--;
    NotTwoEmpty:;

  }


  // scan for the last pixel which is opaque enough
  for (x=_width; --x>=0; )
  {
    int aaSum = 0;
    for (y=_height; --y>=0; )
    {
      aaSum += _pixel8[y][x];
      if (_pixel8[y][x]>=aaTholdOne) goto AABreak;
    }
    if (aaSum>=aaTholdFour*4) goto AABreak;
  }
AABreak:
  _widthKerning = x+1;
  // there should be no more that one pixel dedicated to width AA
  if (_widthKerning<_width-1) _widthKerning = _width-1;
}

void IntLetter::Save( const char *name, bool preview )
{
	//adapted from  TGALSave
  /* TGA 32b uncompressed */
	QOFStream f;
	f.open(name);
	
	// find smallest power of two bigger or equal to W and H
	int W=1,H=1;
	while( W<_width ) W<<=1;
	while( H<_height ) H<<=1;
	long L=(long)W*H;
	f.put((char)0); /*  Number of Characters in Identification Field. */
	f.put((char)0); /*  Color Map Type. */
	f.put((char)2); /*  Image Type Code. */ /* RGB uncompress. */
	/*   3   : Color Map Specification. */
	fputiw(0,f);
	fputiw(0,f);
	f.put((char)0);
	/*   8   : Image Specification. */
	fputiw(0,f); /*  X Origin of Image. */
	fputiw(0,f); /*  Y Origin of Image. */
	fputiw(W,f); /*  Width Image. */
	fputiw(H,f); /*  Height Image. */
	//f.put((char)24); /*  Image Pixel Size. */
	f.put((char)32); /*  Image Pixel Size. */
	f.put((char)0x28); /*  Image Descriptor Byte. */
	/* 18 */
	/* pouze pro N>16 */
	for( int y=0; y<H; y++ )
		for( int x=0; x<W; x++ )
		{
			BYTE a=_pixel8[y][x];
			BYTE i=0xff;
			if( preview )
			{
				i=a;
				a=0xff;
			}
		  f.put(i); // only alpha channel is used
		  f.put(i);
		  f.put(i);
		  f.put(a);
			L--;
		}

  f.close();
}

bool LetterSet::IsFree( const LetterItem *exclude, int xMin, int yMin, int xMax, int yMax ) const
{
	for( int i=0; i<Size(); i++ )
	{
		const LetterItem &item=Get(i);
		if( &item==exclude ) continue;
		if
		(
			max(item.x,xMin)<min(item.x+item.w,xMax) &&
			max(item.y,yMin)<min(item.y+item.h,yMax)
		)
		{
			return false;
		}
	}
	return true;
}

LetterSet::LetterSet()
{
	Clear();
}

LetterSet::~LetterSet()
{
}

void LetterSet::FindPosition( int &x, int &y, int w, int h ) const
{
	for (int mx=0; mx<=max(_height-h,_width-w); mx+=32)
	{
		int mxy=min(mx,_height-h);
		int mxx=min(mx,_width-w);
		for( int yy=0; yy<=mxy; yy+=4 ) for( int xx=0; xx<=mxx; xx+=4 )
		{
			// check if this position is free
      // -2 -> we need to keep one pixel border for the already inserted character
			if( IsFree(NULL,xx-2,yy-2,xx+w,yy+h) )
			{
        // the character is rendered with one pixel border
				x=xx;
				y=yy;
				return;
			}
		}
	}
	// no free position
	x=-1;
	y=-1;
}

void LetterSet::Clear()
{
	base::Clear();
	memset(_pixel8,0,sizeof(_pixel8));
	_width = MinWidth;
	_height = MinHeight;
	_widthPow2 = MinWidth;
	_heightPow2 = MinHeight;
}

/// conversion table SRGB to linear space (byte to byte)
struct LinearizeFontAA
{
  unsigned char _table[256];

  struct XY
  {
    unsigned char x,y;
  };

  static const XY _func[17];

  LinearizeFontAA()
  {
    for (int i=0; i<lenof(_table); i++)
    {
      // perform linear interpolation for source values
      unsigned char y = 0;
      for (int xx=1; xx<lenof(_func); xx++)
      {
        // find neighbouring nodes
        if (_func[xx-1].x<=i && _func[xx].x>=i)
        {
          float factor = float(i-_func[xx-1].x)/(_func[xx].x-_func[xx-1].x);
          y = toInt(_func[xx-1].y*(1-factor) + _func[xx].y*factor);
          break;
        }
      }
      _table[i] = y;
    }
  }
  unsigned char operator () (unsigned char x) const
  {
    Assert(x>=0 && x<lenof(_table));
    return _table[x];
  }
} FixFontAA;

const LinearizeFontAA::XY LinearizeFontAA::_func[]=
{
  {0,0},{15,17},{22,34},{30,51},{38,68},{47,85},{56,102},{66,119},{77,136},{88,153},
  {101,170},{115,187},{131,204},{151,221},{178,238},{255,255}
};

static inline void ForceBorder(unsigned char &val)
{
  const int borderValue = 20;
  if (val<borderValue) val = borderValue;
}

bool LetterSet::AddLetter( int code, const IntLetter &letter )
{
	// find some place to place this letter
	int x,y;
	int w = letter.Width();
	int h = letter.Height();
  // already includes one pixel border for AA

  while (true)
  {
    // we want the trailing pixel to be included
    FindPosition(x, y, w+2, h+2);
    if (x >= 0 && y >= 0) break; // found
    // check if we can to extend
    if (_width >= MaxWidth && _height >= MaxHeight) return false; // cannot extend
    if (_width > _height)
    {
      _height *= 2;
      _heightPow2 *= 2;
    }
    else
    {
      _width *= 2;
      _widthPow2 *= 2;
    }
  }
	// place item to busy position
	LetterItem item;
  // there is one pixel border for left/top anti-aliasing left
	item.x = x+1, item.y = y+1;
	item.w = w, item.h = h;
  item.kw = letter.KerningWidth();
	item.char_code = code;
	Add(item);
	// copy letter data to the set
	for( int yy=0; yy<h; yy++ ) for( int xx=0; xx<w; xx++ )
	{
    // convert from SRGB to linear space
    // letter are rendered 0 = white, however we assume 0=black for the SRGB conversion
		_pixel8[y+yy][x+xx] = FixFontAA(letter.Pixel8(xx,yy));
		//_pixel8[y+yy][x+xx] = letter.Pixel8(xx,yy);
	}
  // debugging - mark borders 
  #if _DEBUG
  if (letter.Height()>0 && letter.KerningWidth()>0)
  {
    for (int xx=0; xx<letter.KerningWidth(); xx++)
    {
      ForceBorder(_pixel8[y+0][x+xx]);
      ForceBorder(_pixel8[y+letter.Height()-1][x+xx]);
    }
    for (int yy=0; yy<letter.Height(); yy++)
    {
      ForceBorder(_pixel8[y+yy][x+0]);
      ForceBorder(_pixel8[y+yy][x+letter.KerningWidth()]);
    }
  }
  
  #endif

	return true;
}

void LetterSet::RefreshPars()
{
	// scan for max. used w,h
	int maxW = 1, maxH = 1;
	for( int y=0; y<MaxHeight; y++ ) for( int x=0; x<MaxWidth; x++ )
	{
		if (!_pixel8[y][x]) continue;
		if (maxW<x+1) maxW = x+1;
		if (maxH<y+1) maxH = y+1;
	}
	int maxW2 = 1, maxH2= 1;
	while ( maxW2<maxW ) maxW2<<=1;
	while ( maxH2<maxH ) maxH2<<=1;
	_width = maxW;
	_height = maxH;
	_widthPow2 = maxW2;
	_heightPow2 = maxH2;
}

void LetterSet::Save( const char *name, bool preview )
{
  QOFStream f;
	f.open(name);
	
	RefreshPars();

	// find smallest power of two bigger or equal to W and H
	long L=(long)_widthPow2*_heightPow2;
	f.put((char)0); /*  Number of Characters in Identification Field. */
	f.put((char)0); /*  Color Map Type. */
	f.put((char)2); /*  Image Type Code. */ /* RGB uncompress. */
	//   3   : Color Map Specification.
	fputiw(0,f);
	fputiw(0,f);
	f.put((char)0);
	//   8   : Image Specification.
	fputiw(0,f); //  X Origin of Image.
	fputiw(0,f); /*  Y Origin of Image. */
	fputiw(_widthPow2,f); /*  Width Image. */
	fputiw(_heightPow2,f); /*  Height Image. */
	//f.put((char)24); /*  Image Pixel Size. */
	f.put((char)32); /*  Image Pixel Size. */
	f.put((char)0x28); /*  Image Descriptor Byte. */
	/* 18 */
	for( int y=0; y<_heightPow2; y++ ) for( int x=0; x<_widthPow2; x++ )
	{
		BYTE a=_pixel8[y][x];
		BYTE i=0xff;
		if( preview )
		{
			i=a;
			a=0xff;
		}
	  f.put(i); // only alpha channel is used
	  f.put(i);
	  f.put(i);
	  f.put(a);
		L--;
	}

  f.close();
}

void LetterSet::AddDesc( QOStream &o, const char *name, int index )
{
	// save descriptions for all letters
	for (int i=0; i<Size();i++)
	{
		const LetterItem &item = Get(i);
		fputiw(item.char_code-32,o); // save character code
		fputiw(index,o); // save set number
		fputiw(item.x,o);
		fputiw(item.y,o);
		fputiw(item.w,o);
		fputiw(item.h,o);
		fputiw(item.kw,o);
	}
}
