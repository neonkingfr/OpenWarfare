// IntLetter.h: interface for the IntLetter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INTLETTER_H__57375574_1BA5_4FA9_B14A_D09BB7F14BA0__INCLUDED_)
#define AFX_INTLETTER_H__57375574_1BA5_4FA9_B14A_D09BB7F14BA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define LET_X 256 // bitmap width and height
#define LET_Y 256

#define ENABLE_SS	0

#if ENABLE_SS
#define SS_FACTORX 4
#define SS_FACTORY 4
#else
#define SS_FACTORX 1
#define SS_FACTORY 1
#endif

#define LET_SSX (LET_X/SS_FACTORX) // supersampled width and height
#define LET_SSY (LET_Y/SS_FACTORY)

#include <Es/essencepch.hpp>
#include <Es/Types/pointers.hpp>
#include <El/QStream/qStream.hpp>
#include <Es/Common/win.h>

class IntLetter  
{
  
private:
  /// actual bitmap dimensions
  int _width,_height;
  /// width used for kerning (ignore transparent pixels which are only as as result of AA)
  int _widthKerning;
  BYTE _pixel8[LET_Y][LET_X];
  
public:
  int Width() const {return _width;}
  int Height() const {return _height;}
  int KerningWidth() const {return _widthKerning;}
  //int SSWidth() const {return _width;}
  //int SSHeight() const {return _height;}

  BYTE Pixel8( int x, int y ) const {return _pixel8[y][x];}
  
public:
  IntLetter();
  
public:
  ~IntLetter();
  
public:
  void Load( BITMAP &header, const Buffer<char> &buffer );
  void Load( BITMAP &header, const Buffer<char> &buffer, int left, int top, int right, int bottom );
  
public:
  void Clear();
  
public:
  void CalculateWidth();
  
public:
  void Save( const char *name, bool preview=false );
  
};

#include <Es/Containers/array.hpp>

struct LetterItem
{
  /// Unicode character code
  int char_code;
  //@{ location in the texture
  int x,y,w,h;
  //@}
  /// width for kerning purposes
  int kw;
};

TypeIsSimple(LetterItem)

class LetterSet: public AutoArray<LetterItem>
{
  typedef AutoArray<LetterItem> base;

  enum {MinWidth=128, MinHeight=128, MaxWidth=2048, MaxHeight=2048};
  BYTE _pixel8[MaxHeight][MaxWidth];
  int _width,_height;
  int _widthPow2,_heightPow2;

  public:

  LetterSet();
  ~LetterSet();

  void FindPosition( int &x, int &y, int w, int h ) const;
  /// check if given position is free
  bool IsFree( const LetterItem *exclude, int xMin, int yMin, int xMax, int yMax ) const;

  void Clear();
  bool AddLetter( int code, const IntLetter &letter );

  void RefreshPars();
  void Save( const char *name, bool preview );
  void AddDesc( QOStream &o, const char *name, int index );


};

#endif // !defined(AFX_INTLETTER_H__57375574_1BA5_4FA9_B14A_D09BB7F14BA0__INCLUDED_)
