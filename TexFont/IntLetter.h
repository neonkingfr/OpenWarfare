// IntLetter.h: interface for the IntLetter class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INTLETTER_H__57375574_1BA5_4FA9_B14A_D09BB7F14BA0__INCLUDED_)
#define AFX_INTLETTER_H__57375574_1BA5_4FA9_B14A_D09BB7F14BA0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define LET_X 256 // bitmap width and height
#define LET_Y 256

#define ENABLE_SS	0

#if ENABLE_SS
#define SS_FACTORX 4
#define SS_FACTORY 4
#else
#define SS_FACTORX 1
#define SS_FACTORY 1
#endif

#define LET_SSX (LET_X/SS_FACTORX) // supersampled width and height
#define LET_SSY (LET_Y/SS_FACTORY)

#include <Es/Types/pointers.hpp>

#include <fstream>

using namespace std;

class IntLetter  
{
	
private:
	int _width,_height; // actual bitmap dimensions
	BYTE _pixel8[LET_Y][LET_X];
	
public:
	int Width() const {return _width;}
	int Height() const {return _height;}
	int SSWidth() const {return _width;}
	int SSHeight() const {return _height;}

	BYTE Pixel8( int x, int y ) const {return _pixel8[y][x];}
	
public:
	IntLetter();
	
public:
	~IntLetter();
	
public:
	void Load( BITMAP &header, const Buffer<char> &buffer );
	void Load( BITMAP &header, const Buffer<char> &buffer, int left, int top, int right, int bottom );
	
public:
	void Clear();
	
public:
	void CalculateWidth();
	
public:
	void Save( const char *name, bool preview=false );
	
};

#include <Es/Containers/array.hpp>

struct LetterItem
{
	int char_code;
	int x,y,w,h;
};

TypeIsSimple(LetterItem)

class LetterSet: public AutoArray<LetterItem>
{
	typedef AutoArray<LetterItem> base;

	enum {Width=256,Height=256};
	BYTE _pixel8[Height][Width];
	int _width,_height;
	int _widthPow2,_heightPow2;

	public:

	LetterSet();
	~LetterSet();

	void FindPosition( int &x, int &y, int w, int h ) const;
	bool IsFree( const LetterItem *exclude, int x, int y, int w, int h ) const;

	void Clear();
	bool AddLetter( int code, const IntLetter &letter );

	void RefreshPars();
	void Save( const char *name, bool preview );
	void AddDesc( ostream &o, const char *name, int index );


};

#endif // !defined(AFX_INTLETTER_H__57375574_1BA5_4FA9_B14A_D09BB7F14BA0__INCLUDED_)
