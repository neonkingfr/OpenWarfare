// TexFont.h : main header file for the TEXFONT application
//

#if !defined(AFX_TEXFONT_H__0F6F617F_39B3_4176_8C26_02BA7C4945E8__INCLUDED_)
#define AFX_TEXFONT_H__0F6F617F_39B3_4176_8C26_02BA7C4945E8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CTexFontApp:
// See TexFont.cpp for the implementation of this class
//

class CTexFontApp : public CWinApp
{
public:
	CTexFontApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTexFontApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTexFontApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXFONT_H__0F6F617F_39B3_4176_8C26_02BA7C4945E8__INCLUDED_)
