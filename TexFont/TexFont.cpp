// TexFont.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TexFont.h"

#include "intLetter.h"
#include <Es/Strings/rstring.hpp>
#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define PREVIEW 0

/////////////////////////////////////////////////////////////////////////////
// CTexFontApp

BEGIN_MESSAGE_MAP(CTexFontApp, CWinApp)
	//{{AFX_MSG_MAP(CTexFontApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTexFontApp construction

CTexFontApp::CTexFontApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTexFontApp object

CTexFontApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CTexFontApp initialization

static void FlushSet( LetterSet &set, const char *path, int setNum )
{
	char setName[1024];
	strcpy(setName,path);
	strcat(setName,"-");
	sprintf(setName+strlen(setName),"%02d",setNum);
	strcat(setName,".tga");
	set.Save(setName,false);
	#if PREVIEW
		strcpy(setName,path);
		strcat(setName,"#");
		sprintf(setName+strlen(setName),"%02d",setNum);
		strcat(setName,".tga");
		set.Save(setName,true);
	#endif
}

struct CharInfo
{
	wchar_t c;
	double l;
	double t;
	double r;
	double b;

	double el;
	double et;
	double er;
	double eb;
};

// Gulim (Gulim Che) font
static CharInfo HangulChars[] =
{
	{0x115f, 0.00, 0.00, 1.00, 1.00},	// 0x60
			
	{0xac00, 0.08, 0.08, 0.58, 0.92},	// 0x61
	{0xae4c, 0.08, 0.08, 0.60, 0.92},	// 0x62
	{0xb098, 0.08, 0.08, 0.70, 0.92},	// 0x63
	{0xb2e4, 0.08, 0.08, 0.70, 0.92},	// 0x64
	{0xb530, 0.08, 0.08, 0.70, 0.92},	// 0x65
	{0xb77c, 0.08, 0.08, 0.70, 0.92},	// 0x66
	{0xb9c8, 0.08, 0.08, 0.60, 0.92},	// 0x67
	{0xbc14, 0.08, 0.08, 0.60, 0.92},	// 0x68
	{0xbe60, 0.08, 0.08, 0.68, 0.92},	// 0x69
	{0xc0ac, 0.08, 0.08, 0.64, 0.92},	// 0x6a
	{0xc2f8, 0.08, 0.08, 0.68, 0.92},	// 0x6b
	{0xc544, 0.08, 0.08, 0.60, 0.92},	// 0x6c
	{0xc790, 0.08, 0.08, 0.68, 0.92},	// 0x6d
	{0xc9dc, 0.08, 0.08, 0.70, 0.92},	// 0x6e
	{0xcc28, 0.08, 0.08, 0.68, 0.92},	// 0x6f
	{0xce74, 0.08, 0.08, 0.58, 0.92},	// 0x70
	{0xd0c0, 0.08, 0.08, 0.70, 0.92},	// 0x71
	{0xd30c, 0.08, 0.08, 0.68, 0.92},	// 0x72
	{0xd558, 0.08, 0.08, 0.64, 0.92},	// 0x73
			
	{0xad6c, 0.08, 0.08, 0.92, 0.51},	// 0x74
	{0xafb8, 0.08, 0.08, 0.92, 0.51},	// 0x75
	{0xb204, 0.08, 0.08, 0.92, 0.51},	// 0x76
	{0xb450, 0.08, 0.08, 0.92, 0.51},	// 0x77
	{0xb69c, 0.08, 0.08, 0.92, 0.51},	// 0x78
	{0xb8e8, 0.08, 0.08, 0.92, 0.54},	// 0x79
	{0xbb34, 0.08, 0.08, 0.92, 0.51},	// 0x7a
	{0xbd80, 0.08, 0.08, 0.92, 0.51},	// 0x7b
	{0xbfcc, 0.08, 0.08, 0.92, 0.51},	// 0x7c
	{0xc218, 0.08, 0.08, 0.92, 0.51},	// 0x7d
	{0xc464, 0.08, 0.08, 0.92, 0.51},	// 0x7e
	{0xc6b0, 0.08, 0.08, 0.92, 0.51},	// 0x7f
	{0xc8fc, 0.08, 0.08, 0.92, 0.51},	// 0x80
	{0xcb48, 0.08, 0.08, 0.92, 0.51},	// 0x81
	{0xcd94, 0.08, 0.08, 0.92, 0.54},	// 0x82
	{0xcfe0, 0.08, 0.08, 0.92, 0.53},	// 0x83
	{0xd22c, 0.08, 0.08, 0.92, 0.51},	// 0x84
	{0xd478, 0.08, 0.08, 0.92, 0.51},	// 0x85
	{0xd6c4, 0.08, 0.08, 0.92, 0.58},	// 0x86
			
	{0xac01, 0.08, 0.08, 0.58, 0.58},	// 0x87
	{0xae4d, 0.08, 0.08, 0.62, 0.58},	// 0x88
	{0xb099, 0.08, 0.08, 0.70, 0.54},	// 0x89
	{0xb2e5, 0.08, 0.08, 0.70, 0.54},	// 0x8a
	{0xb531, 0.08, 0.08, 0.72, 0.54},	// 0x8b
	{0xb77d, 0.08, 0.08, 0.70, 0.54},	// 0x8c
	{0xb9c9, 0.08, 0.08, 0.60, 0.54},	// 0x8d
	{0xbc15, 0.08, 0.08, 0.60, 0.54},	// 0x8e
	{0xbe61, 0.08, 0.08, 0.68, 0.54},	// 0x8f
	{0xc0ad, 0.08, 0.08, 0.62, 0.58},	// 0x90
	{0xc2f9, 0.08, 0.08, 0.68, 0.58},	// 0x91
	{0xc545, 0.08, 0.08, 0.60, 0.54},	// 0x92
	{0xc791, 0.08, 0.08, 0.68, 0.54},	// 0x93
	{0xc9dd, 0.08, 0.08, 0.70, 0.58},	// 0x94
	{0xcc29, 0.08, 0.08, 0.68, 0.59},	// 0x95
	{0xce75, 0.08, 0.08, 0.58, 0.59},	// 0x96
	{0xd0c1, 0.08, 0.08, 0.70, 0.56},	// 0x97
	{0xd30d, 0.08, 0.08, 0.68, 0.56},	// 0x98
	{0xd559, 0.08, 0.08, 0.64, 0.60},	// 0x99
			
	{0xb9c8, 0.60, 0.08, 0.92, 0.92},	// 0x9a
	{0xb9e4, 0.51, 0.08, 0.92, 0.92},	// 0x9b
	{0xba00, 0.60, 0.08, 0.92, 0.92},	// 0x9c
	{0xba1c, 0.51, 0.08, 0.92, 0.92},	// 0x9d
	{0xba38, 0.55, 0.08, 0.92, 0.92},	// 0x9e
	{0xba54, 0.46, 0.08, 0.92, 0.92},	// 0x9f
	{0xba70, 0.55, 0.08, 0.92, 0.92},	// 0xa0
	{0xba8c, 0.46, 0.08, 0.92, 0.92},	// 0xa1
	{0xbaa8, 0.08, 0.55, 0.92, 0.92},	// 0xa2
	{0xbac4, 0.08, 0.08, 0.92, 0.92, 0.08, 0.08, 0.68, 0.53},	// 0xa3
	{0xbae0, 0.08, 0.08, 0.92, 0.92, 0.08, 0.08, 0.58, 0.50},	// 0xa4
	{0xbafc, 0.08, 0.08, 0.92, 0.92, 0.08, 0.08, 0.68, 0.53},	// 0xa5
	{0xbb18, 0.08, 0.55, 0.92, 0.92},// 0xa6
	{0xbb34, 0.08, 0.50, 0.92, 0.92},// 0xa7
	{0xbb50, 0.08, 0.08, 0.92, 0.92, 0.08, 0.08, 0.68, 0.48},	// 0xa8
	{0xbb6c, 0.08, 0.08, 0.92, 0.92, 0.08, 0.08, 0.58, 0.48},	// 0xa9
	{0xbb88, 0.08, 0.08, 0.92, 0.92, 0.08, 0.08, 0.68, 0.48},	// 0xaa
	{0xbba4, 0.08, 0.50, 0.92, 0.92},// 0xab
	{0xbbc0, 0.08, 0.58, 0.92, 0.92},// 0xac
	{0xbbdc, 0.08, 0.08, 0.92, 0.92, 0.08, 0.08, 0.68, 0.58},	// 0xad
	{0xbbf8, 0.60, 0.08, 0.92, 0.92},// 0xae
			
	{0xb9c9, 0.60, 0.08, 0.92, 0.58},// 0xaf
	{0xb9e5, 0.51, 0.08, 0.92, 0.58},// 0xb0
	{0xba01, 0.60, 0.08, 0.92, 0.58},// 0xb1
	{0xba1d, 0.51, 0.08, 0.92, 0.58},// 0xb2
	{0xba39, 0.55, 0.08, 0.92, 0.58},// 0xb3
	{0xba55, 0.46, 0.08, 0.92, 0.58},// 0xb4
	{0xba71, 0.55, 0.08, 0.92, 0.58},// 0xb5
	{0xba8d, 0.46, 0.08, 0.92, 0.58},// 0xb6
	{0xbaa9, 0.08, 0.39, 0.92, 0.58},// 0xb7
	{0xbac5, 0.08, 0.08, 0.92, 0.58, 0.08, 0.08, 0.68, 0.39},	// 0xb8
	{0xbae1, 0.08, 0.08, 0.92, 0.58, 0.08, 0.08, 0.58, 0.40},	// 0xb9
	{0xbafd, 0.08, 0.08, 0.92, 0.58, 0.08, 0.08, 0.68, 0.39},	// 0xba
	{0xbb19, 0.08, 0.39, 0.92, 0.58},// 0xbb
	{0xbb35, 0.08, 0.39, 0.92, 0.63},// 0xbc
	{0xbb51, 0.08, 0.08, 0.92, 0.63, 0.08, 0.08, 0.68, 0.39},	// 0xbd
	{0xbb6d, 0.08, 0.08, 0.92, 0.63, 0.08, 0.08, 0.58, 0.39},	// 0xbe
	{0xbb89, 0.08, 0.08, 0.92, 0.63, 0.08, 0.08, 0.68, 0.39},	// 0xbf
	{0xbba5, 0.08, 0.39, 0.92, 0.64},// 0xc0
	{0xbbc1, 0.08, 0.42, 0.92, 0.58},// 0xc1
	{0xbbdd, 0.08, 0.08, 0.92, 0.58, 0.08, 0.08, 0.68, 0.40},	// 0xc2
	{0xbbf9, 0.60, 0.08, 0.92, 0.58},	// 0xc3
			
	{0xadf9, 0.08, 0.56, 0.92, 0.92},	// 0xc4
	{0xadfa, 0.08, 0.56, 0.92, 0.92},	// 0xc5
	{0xadfb, 0.08, 0.56, 0.92, 0.92},	// 0xc6
	{0xadfc, 0.08, 0.60, 0.92, 0.92},	// 0xc7
	{0xadfd, 0.08, 0.56, 0.92, 0.92},	// 0xc8
	{0xadfe, 0.08, 0.54, 0.92, 0.92},	// 0xc9
	{0xadff, 0.08, 0.56, 0.92, 0.92},	// 0xca
	{0xae00, 0.08, 0.54, 0.92, 0.92},	// 0xcb
	{0xae01, 0.08, 0.54, 0.92, 0.92},	// 0xcc
	{0xae02, 0.08, 0.54, 0.92, 0.92},	// 0xcd
	{0xae03, 0.08, 0.54, 0.92, 0.92},	// 0xce
	{0xae04, 0.08, 0.54, 0.92, 0.92},	// 0xcf
	{0xae05, 0.08, 0.54, 0.92, 0.92},	// 0xd0
	{0xae06, 0.08, 0.54, 0.92, 0.92},	// 0xd1
	{0xae07, 0.08, 0.54, 0.92, 0.92},	// 0xd2
	{0xae08, 0.08, 0.58, 0.92, 0.92},	// 0xd3
	{0xae09, 0.08, 0.56, 0.92, 0.92},	// 0xd4
	{0xae0a, 0.08, 0.54, 0.92, 0.92},	// 0xd5
	{0xae0b, 0.08, 0.58, 0.92, 0.92},	// 0xd6
	{0xae0c, 0.08, 0.58, 0.92, 0.92},	// 0xd7
	{0xae0d, 0.08, 0.60, 0.92, 0.92},	// 0xd8
	{0xae0e, 0.08, 0.60, 0.92, 0.92},	// 0xd9
	{0xae0f, 0.08, 0.54, 0.92, 0.92},	// 0xda
	{0xae10, 0.08, 0.58, 0.92, 0.92},	// 0xdb
	{0xae11, 0.08, 0.54, 0.92, 0.92},	// 0xdc
	{0xae12, 0.08, 0.58, 0.92, 0.92},	// 0xdd
	{0xae13, 0.08, 0.52, 0.92, 0.92},	// 0xde
			
	{0xb8f1, 0.00, 0.00, 1.00, 1.00},	// 0xdf
};

#include <Es/Files/filenames.hpp>

BOOL CTexFontApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#if _MFC_VER<0x700
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
#endif

	CRegKey key;
	char path[1024]="";
	DWORD size = 0;
	CFont font;

	LOGFONT logFont;
	memset(&logFont,0,sizeof(logFont));

	{
		ifstream cfg;
		cfg.open("fontDraw.par",ios::in|ios::binary);
		cfg.read((char *)&logFont,sizeof(logFont));
	}
	if( key.Open(HKEY_LOCAL_MACHINE,"Software\\Suma\\FontDraw")>=0 )
	{
		ULONG pathSize=sizeof(path);
		key.QueryValue("FilePath",NULL,path,&pathSize);
		/*
		DWORD nameSize=sizeof(logFont.lfFaceName);
		DWORD styleSize=sizeof(logFont.lfStyle);
		key.QueryValue(logFont.lfFaceName,"FontName",&nameSize);
		key.QueryValue(logFont.lfStyle,"FontStyle",&nameSize);
		DWORD height;
		key.QueryValue(height,"FontSize");
		logFont.lfHeight = height;
		*/
	}
	if( strlen(path)<=0 )
	{
		::GetCurrentDirectory(sizeof(path),path);
		strcat(path,"\\default.fxy");
	}
	
	CFontDialog fontSel;
	fontSel.m_cf.Flags |= CF_INITTOLOGFONTSTRUCT;
	fontSel.m_cf.Flags &= ~CF_EFFECTS;
	fontSel.m_cf.lpLogFont = &logFont;
	//fontSel.m_cf.lpszStyle=name;
	if( fontSel.DoModal()==IDOK )
	{
		enum {CCount=256-32};
		//IntLetter *letters=new IntLetter[CCount];
		
		fontSel.GetCurrentFont(&logFont);
		logFont.lfQuality = ANTIALIASED_QUALITY;
		CFont font;
		font.CreateFontIndirect(&logFont);

		RString faceName=fontSel.GetFaceName();
		BOOL bold=fontSel.IsBold();
		BOOL italic=fontSel.IsItalic();
		int size=fontSel.GetSize();
		
		char name[1024];
		strcpy(name,faceName);
		char *dn,*sn;
		for (dn=name,sn=name; *sn; )
		{
			if (*sn!=' ') *dn++=*sn++;
			else sn++;
		}
		*dn=0;
		if( bold ) strcat(name,"B");
		if( italic ) strcat(name,"I");
		sprintf(name+strlen(name),"%d",size/10);

		{
			ofstream cfg;
			cfg.open("fontDraw.par",ios::out|ios::binary);
			cfg.write((char *)&logFont,sizeof(logFont));
		}

		CFileDialog fileSel
		(
			FALSE,".fxy",name,0,
			"Font files (*.fxy)|*.fxy|"
			"|",NULL
		);
		
		if( fileSel.DoModal()==IDOK )
		{
			strcpy(path,fileSel.GetPathName());
			char *ext = strrchr(path,'.');
			if( ext) *ext = 0;

			if( key.Create(HKEY_LOCAL_MACHINE,"Software\\Suma\\FontDraw")>=0 )
			{
				key.SetValue("FilePath",REG_SZ,path,strlen(path)+1);
				//key.SetValue(logFont.lfFaceName,"FontName");
				//key.SetValue(logFont.lfHeight,"FontSize");
				//key.SetValue(logFont.lfStyle,"FontStyle");
				// save LogFont as binary to the registry
			}

			// create memory canvas
			CDC cdc;
			cdc.CreateCompatibleDC(NULL);

			// set canvas dimensions and format
			 
			CBitmap bitmap;
			bitmap.CreateBitmap(LET_X,LET_Y,1,32,NULL);
			//bitmap.CreateCompatibleBitmap(&cdc,LET_X,LET_Y);
			cdc.SelectObject(&bitmap);
			
			// use selected font to draw into the memory canvas
			cdc.SelectObject(&font);

			CBrush brush(RGB(255,255,255));

			LetterSet set;
			int setNum = 1;

			ofstream desc;
			char descname[1024];
			strcpy(descname,path);
			strcat(descname,".fxy");
			desc.open(descname,ios::binary|ios::out);

      int maxH = 0;
			int i;
			// convert each letter into single-plane bitmap
			for( i=32; i<CCount+32; i++ )
			{
				CharInfo *ci = NULL;
				if (i >= 0x80 && logFont.lfCharSet == HANGUL_CHARSET)
				{
					ci = &HangulChars[i - 0x80];
				}

				cdc.FillRect(CRect(0,0,LET_X,LET_Y),&brush);
				if (ci)
				{
				  wchar_t text[2];
				  text[1]=0;
					text[0] = ci->c;
  				::TextOutW(cdc.GetSafeHdc(),0,0,text,1);
				}
				else
				{
				  char text[2];
				  text[1]=0;
					text[0] = i;
  				::TextOutA(cdc.GetSafeHdc(),0,0,text,1);
				}
				
				int h = 0;
				int w = 0;

				if (ci)
				{
					TEXTMETRIC tm;
					GetTextMetrics(cdc.GetSafeHdc(), &tm);
					h = tm.tmHeight;

					::GetCharWidth32W(cdc.GetSafeHdc(), ci->c, ci->c, &w);

					if (ci->er > ci->el && ci->eb > ci->et)
					{
						int left = ci->el * w;
						int top = ci->et * h;
						int right = ci->er * w;
						int bottom = ci->eb * h;
						cdc.FillSolidRect(left, top, right - left, bottom - top, RGB(255, 255, 255));
					}
				}

				// convert bitmap into internal font format
				Buffer<char> letter;
				BITMAP size;
				bitmap.GetObject(sizeof(size),&size);
				letter.Init(size.bmWidthBytes*size.bmHeight);
			
				bitmap.GetBitmapBits(letter.Size(),letter.Data());
				
				IntLetter letterPic;
				if (ci)
				{
					int left = ci->l * w;
					int top = ci->t * h;
					int right = ci->r * w;
					int bottom = ci->b * h;
					letterPic.Load(size, letter, left, top, right, bottom);
				}
				else
					letterPic.Load(size,letter);
				// save each letter into the separate file
				
				// use TGA32 format to save data

				if (!set.AddLetter(i,letterPic))
				{
					set.AddDesc(desc,descname,setNum);
					FlushSet(set,path,setNum++);
					set.Clear();
					set.AddLetter(i,letterPic);
				}
				
				if (maxH<letterPic.SSHeight()) maxH = letterPic.SSHeight();
			}

			set.AddDesc(desc,descname,setNum);
			FlushSet(set,path,setNum);
			
			char text[256];
			sprintf(text,"Font size %d",maxH+1);
			MessageBox(NULL,text,"FontDraw",MB_OK|MB_ICONINFORMATION);
		}

	}
	
	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

void LogF(char const *format,...)
{

}

void ErrF(char const *format,...)
{

}

void LstF(char const *format,...)
{

}

