// IntLetter.cpp: implementation of the IntLetter class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "IntLetter.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Code added here will be included at the top of the .CPP file
#include <fstream>

static void fputiw( int W, ostream &f )
{
	f.put((char)(W&0xff));
	f.put((char)(W>>8));
}


IntLetter::IntLetter()
{
	Clear();
}

IntLetter::~IntLetter()
{
	
}

//#define ENABLE_SS	0

void IntLetter::Load( BITMAP &header, const Buffer<char> &buffer )
{
	// load letter from memory buffer
	// memory buffer is single-plane (one bit per pixel)
	// starts with bitmap header
	// bmp file starts with ...
	// data contains: BITMAPFILEINFO followed by BITMAPINFO
	
	int size=buffer.Size();
  const char *data=buffer.Data();
  
  //const struct bmp_info *bInfo=(const struct bmp_info *)data;
  const char *bits=data;
  
  // we should assert that the bitmap is really 1-bit
  if( header.bmBitsPixel==8 )
	{
    Clear();
    
    int x,y;
    for( y=0; y<LET_Y; y++ )
	  {
		  if( y>=header.bmHeight ) break;
		  const char *line=&bits[y*header.bmWidthBytes];
		  for( x=0; x<LET_X; x++ )
		  {
			  if( x>=header.bmWidth ) break;
			  _pixel8[y][x]= line[x];
		  }
	  }
	}
  else if( header.bmBitsPixel==32 )
  {
    Clear();
    
    int x,y;
    for( y=0; y<LET_Y; y++ )
	  {
		  if( y>=header.bmHeight ) break;
		  const DWORD *line=(const DWORD *)&bits[y*header.bmWidthBytes];
		  for( x=0; x<LET_X; x++ )
		  {
			  if( x>=header.bmWidth ) break;
			  DWORD pixel = line[x];
			  BYTE pix = 0xff-BYTE(pixel&0xff);
			  _pixel8[y][x]= pix;
		  }
	  }
  }
	else
	{
		Fail("Wrong bitmap format");
		return;
  }
  
	CalculateWidth();
}

void IntLetter::Load( BITMAP &header, const Buffer<char> &buffer, int left, int top, int right, int bottom )
{
	int size = buffer.Size();
  const char *data = buffer.Data();
  
  //const struct bmp_info *bInfo=(const struct bmp_info *)data;
  const char *bits = data;
  
  // we should assert that the bitmap is really 1-bit
  if( header.bmBitsPixel!=1 )
	{
		//Fail("Wrong bitmap format");
		return;
	}
  
  Clear();
  
  int x,y;
  for( y=top; y<LET_Y; y++ )
	{
		if( y>=header.bmHeight ) break;
		const char *line=&bits[y*header.bmWidthBytes];
		for( x=left; x<LET_X; x++ )
		{
		   if( x>=header.bmWidth ) break;
			_pixel8[y - top][x - left]= line[x];
		}
	}

	_width = right - left;
	_height = bottom - top;
}

void IntLetter::Clear()
{
	int x,y;
	for( y=0; y<LET_Y; y++ ) for( x=0; x<LET_X; x++ )
  {
		_pixel8[y][x]=0;
	}
	_width=0,_height=0;
}

void IntLetter::CalculateWidth()
{
	int x,y;
	// calculate width
	for( x=LET_X; --x>=0; )
    for( y=LET_Y; --y>=0; )
		{
			if( _pixel8[y][x] ) goto WBreak;
		}
	WBreak:
	_width=x+1;
	// calculate height
	for( y=LET_Y; --y>=0; )
		for( x=LET_X; --x>=0; )
		{
			if( _pixel8[y][x] ) goto HBreak;
		}
	HBreak:
	_height=y+1;
}

void IntLetter::Save( const char *name, bool preview )
{
	//static void TGALSave( const char *N, int W, int H, BYTE *Buf ) /* TC s hloubkou Resol */
  /* TGA 32b uncompressed */
	ofstream f;
	f.open(name,ios::binary|ios::out);
	
	// find smallest power of two bigger or equal to W and H
	int W=1,H=1;
	while( W<_width ) W<<=1;
	while( H<_height ) H<<=1;
	long L=(long)W*H;
	f.put((char)0); /*  Number of Characters in Identification Field. */
	f.put((char)0); /*  Color Map Type. */
	f.put((char)2); /*  Image Type Code. */ /* RGB uncompress. */
	/*   3   : Color Map Specification. */
	fputiw(0,f);
	fputiw(0,f);
	f.put((char)0);
	/*   8   : Image Specification. */
	fputiw(0,f); /*  X Origin of Image. */
	fputiw(0,f); /*  Y Origin of Image. */
	fputiw(W,f); /*  Width Image. */
	fputiw(H,f); /*  Height Image. */
	//f.put((char)24); /*  Image Pixel Size. */
	f.put((char)32); /*  Image Pixel Size. */
	f.put((char)0x28); /*  Image Descriptor Byte. */
	/* 18 */
	/* pouze pro N>16 */
	for( int y=0; y<H; y++ )
		for( int x=0; x<W; x++ )
		{
			BYTE a=_pixel8[y][x];
			BYTE i=0xff;
			if( preview )
			{
				i=a;
				a=0xff;
			}
			f.put(i); // only alpha channel is used
			f.put(i);
			f.put(i);
			f.put(a);
			L--;
		}
}

bool LetterSet::IsFree( const LetterItem *exclude, int x, int y, int w, int h ) const
{
	for( int i=0; i<Size(); i++ )
	{
		const LetterItem &item=Get(i);
		if( &item==exclude ) continue;
		if
		(
			max(item.x,x)<min(item.x+item.w,x+w) &&
			max(item.y,y)<min(item.y+item.h,y+h)
		)
		{
			return false;
		}
	}
	return true;
}

LetterSet::LetterSet()
{
	Clear();
}

LetterSet::~LetterSet()
{
}

void LetterSet::FindPosition( int &x, int &y, int w, int h ) const
{
	for( int mx=0; mx<=max(Height-h,Width-w); mx+=32 )
	{
		int mxy=min(mx,Height-h);
		int mxx=min(mx,Width-w);
		for( int yy=0; yy<=mxy; yy+=4 ) for( int xx=0; xx<=mxx; xx+=4 )
		{
			// check if this position is free
			if( IsFree(NULL,xx,yy,w,h) )
			{
				x=xx;
				y=yy;
				return;
			}
		}
	}
	// no free position
	x=-1;
	y=-1;
}

void LetterSet::Clear()
{
	base::Clear();
	memset(_pixel8,0,sizeof(_pixel8));
	_width = Width;
	_height = Height;
	_widthPow2 = Width;
	_heightPow2 = Height;
}

bool LetterSet::AddLetter( int code, const IntLetter &letter )
{
	// find some place to place this letter
	int x,y;
	int w = letter.SSWidth();
	int h = letter.SSHeight();
	FindPosition(x,y,w+1,h+1);
	if (x<0 || y<0) return false;
	// place item to busy position
	LetterItem item;
	item.x = x, item.y = y;
	item.w = w+1, item.h = h+1;
	item.char_code = code;
	Add(item);
	// copy letter data to the set
	for( int yy=0; yy<h; yy++ ) for( int xx=0; xx<w; xx++ )
	{
		_pixel8[y+yy][x+xx] = letter.Pixel8(xx,yy);
	}

	return true;
}

void LetterSet::RefreshPars()
{
	// scan for max. used w,h
	int maxW = 1, maxH = 1;
	for( int y=0; y<Height; y++ ) for( int x=0; x<Width; x++ )
	{
		if (!_pixel8[y][x]) continue;
		if (maxW<x+1) maxW = x+1;
		if (maxH<y+1) maxH = y+1;
	}
	int maxW2 = 1, maxH2= 1;
	while ( maxW2<maxW ) maxW2<<=1;
	while ( maxH2<maxH ) maxH2<<=1;
	_width = maxW;
	_height = maxH;
	_widthPow2 = maxW2;
	_heightPow2 = maxH2;
}

void LetterSet::Save( const char *name, bool preview )
{
	ofstream f;
	f.open(name,ios::binary|ios::out);
	
	RefreshPars();

	// find smallest power of two bigger or equal to W and H
	long L=(long)_widthPow2*_heightPow2;
	f.put((char)0); /*  Number of Characters in Identification Field. */
	f.put((char)0); /*  Color Map Type. */
	f.put((char)2); /*  Image Type Code. */ /* RGB uncompress. */
	//   3   : Color Map Specification.
	fputiw(0,f);
	fputiw(0,f);
	f.put((char)0);
	//   8   : Image Specification.
	fputiw(0,f); //  X Origin of Image.
	fputiw(0,f); /*  Y Origin of Image. */
	fputiw(_widthPow2,f); /*  Width Image. */
	fputiw(_heightPow2,f); /*  Height Image. */
	//f.put((char)24); /*  Image Pixel Size. */
	f.put((char)32); /*  Image Pixel Size. */
	f.put((char)0x28); /*  Image Descriptor Byte. */
	/* 18 */
	for( int y=0; y<_heightPow2; y++ ) for( int x=0; x<_widthPow2; x++ )
	{
		BYTE a=_pixel8[y][x];
		BYTE i=0xff;
		if( preview )
		{
			i=a;
			a=0xff;
		}
		f.put(i); // only alpha channel is used
		f.put(i);
		f.put(i);
		f.put(a);
		L--;
	}
}

void LetterSet::AddDesc( ostream &o, const char *name, int index )
{
	// save descriptions for all letters
	for (int i=0; i<Size();i++)
	{
		const LetterItem &item = Get(i);
		fputiw(item.char_code-32,o); // save character code
		fputiw(index,o); // save set number
		fputiw(item.x,o);
		fputiw(item.y,o);
		fputiw(item.w,o);
		fputiw(item.h,o);
	}
}
