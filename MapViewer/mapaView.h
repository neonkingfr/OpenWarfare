// mapaView.h : interface of the CMapaView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAPAVIEW_H__E9CA47D4_E310_4ADC_BA45_31BCA2D51815__INCLUDED_)
#define AFX_MAPAVIEW_H__E9CA47D4_E310_4ADC_BA45_31BCA2D51815__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMapaView : public CView
{
protected: // create from serialization only
	CMapaView();
	DECLARE_DYNCREATE(CMapaView)

// Attributes
public:
	CMapaDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMapaView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMapaView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
  bool scrollBarsInitialized;

// Generated message map functions
protected:
	//{{AFX_MSG(CMapaView)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  void UpdateScrollbars();

  afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
  afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};

#ifndef _DEBUG  // debug version in mapaView.cpp
inline CMapaDoc* CMapaView::GetDocument()
   { return (CMapaDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPAVIEW_H__E9CA47D4_E310_4ADC_BA45_31BCA2D51815__INCLUDED_)
