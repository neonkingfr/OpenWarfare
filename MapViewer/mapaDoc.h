// mapaDoc.h : interface of the CMapaDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAPADOC_H__92AD4EB3_9934_4F3C_BB6F_184E4C6DAAE7__INCLUDED_)
#define AFX_MAPADOC_H__92AD4EB3_9934_4F3C_BB6F_184E4C6DAAE7__INCLUDED_

#include "VIEWER.H"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMapaDoc : public CDocument
{
protected: // create from serialization only
	CMapaDoc();
	DECLARE_DYNCREATE(CMapaDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMapaDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	CScreen screen;
	
	virtual ~CMapaDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMapaDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPADOC_H__92AD4EB3_9934_4F3C_BB6F_184E4C6DAAE7__INCLUDED_)
