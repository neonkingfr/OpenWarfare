// mapaDoc.cpp : implementation of the CMapaDoc class
//

#include "stdafx.h"
#include "mapa.h"

#include "mapaDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMapaDoc

IMPLEMENT_DYNCREATE(CMapaDoc, CDocument)

BEGIN_MESSAGE_MAP(CMapaDoc, CDocument)
	//{{AFX_MSG_MAP(CMapaDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMapaDoc construction/destruction

CMapaDoc::CMapaDoc()
{
	// TODO: add one-time construction code here
	//naprdam neco do screenu, testovani
	CScreen::SetInactive();
	screen.Init(0.2,0,0);

  const int POCET_HRAN=2+3*15 + 12*3;
  const int array[4*POCET_HRAN] = 
  {
    90,135, 80,145,  90,135, 100,140,
    70,140, 60,130,  85,125, 60,130,  80,110, 85,125,
    50,50,  10,60,   10,60,  25,100,  35,95,  25,100,
    40,90,  35,95,   45,95,  40,90,   50,90,  45,95,
    35,70,  50,90,   25,100, 40,105,  45,115, 40,105,
    95,125, 100,115, 60,125, 55,120,  55,120, 50,110,
    50,110, 45,105,  40,95,  45,105,  60,125, 60,110,
    60,110, 55,100,  55,100, 55,85,   55,85,  40,60,
    40,60,  20,70,   20,70,  30,90,   35,100,  50,100,
    70,120, 75,100,  75,100, 90,110,  65,115, 70,105,
    65,90,  70,105,  85,100, 85,95,   85,95,  70,95,
    70,95,  60,100,  60,85,  75,80,   75,90,  60,75,
    50,60,  80,70,   75,75,  95,75,   95,75,  90,85,
    90,85,  95,95,   80,85,  90,65,   100,75, 95,85,
    95,85,  100,95,  100,95, 90,105,  90,105, 105,100,
    105,100,100,105, 100,105,105,105, 105,105,95,95,

    //Recke bludiste:
    55,165, 55,155,  50,180, 50,185,  50,185, 65,185,   //1
    65,175, 75,175,  75,175, 75,195,  75,195, 40,195,   //2
    40,195, 40,170,  40,170, 80,170,  80,170, 80,200,   //3
    80,200, 35,200,  35,200, 35,165,  35,165, 55,165,   //4
    55,155, 25,155,  25,155, 25,210,  65,175, 65,185,   //5
    25,210, 90,210,  90,210, 90,160,  90,160, 70,160,   //6
    65,165, 65,155,  50,160, 30,160,  30,160, 30,205,   //7
    30,205, 85,205,  65,155, 95,155,  95,155, 95,215,   //8
    95,215, 20,215,  20,215, 20,150,  20,150, 60,150,   //9
    60,150, 60,180,  60,180, 55,180,  55,180, 55,175,   //10
    55,175, 45,175,  45,175, 45,190,  45,190, 70,190,   //11
    70,190, 70,180,  85,205, 85,165,  85,165, 65,165    //12
  };
  screen.SetColor(RGB(128,0,0));
  screen.SetLineStyle(PS_SOLID, 1);
  for (int i=0; i<POCET_HRAN; i++)
  {
    screen.Line(array[4*i],array[4*i+1],array[4*i+2],array[4*i+3]);
  }
  
/*
  screen.Moveto(0,0);
  srand((int)time(NULL));
	screen.SetColor(RGB(128,0,0));
	screen.SetLineStyle(PS_SOLID, 1);
	int lastX=0, lastY=0;
	for (int i=1; i<=50; i++)
	{
		screen.Lineto(lastX+=rand()%500-250,lastY+=rand()%500-250); //screen neni aktivni, proto se nekresli

	}
	screen.SetColor(RGB(0,0,0));
	screen.SetLineStyle(PS_SOLID, 3);
	screen.Line(-80,-30,50,80);
*/

}

CMapaDoc::~CMapaDoc()
{
	screen.closeGraph();
}

BOOL CMapaDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return false;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return true;
}



/////////////////////////////////////////////////////////////////////////////
// CMapaDoc serialization

void CMapaDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMapaDoc diagnostics

#ifdef _DEBUG
void CMapaDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMapaDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMapaDoc commands
