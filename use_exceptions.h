#ifdef _WIN32
#include <crtdbg.h> // needed for exception handling to compile with Visual Studio C++
#endif

#ifdef _MSC_VER
# ifndef _CPPUNWIND
#   define EXCEPTIONS_MISSING 1
# endif
#elif __GNUC__
# ifndef __EXCEPTIONS
#   define EXCEPTIONS_MISSING 1
# endif
#endif

#if EXCEPTIONS_MISSING
# error "Exception handling needed"
#endif

#ifdef _WIN32
#define NOTHROW __declspec(nothrow)
#else
#define NOTHROW __attribute__((nothrow))
#endif

