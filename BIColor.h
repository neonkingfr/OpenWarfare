#ifndef _BI_COLOR_H_
#define _BI_COLOR_H_

#include "BISDKCommon.h"

class BI_Color
{
  float _color[4];

public:
  void Set(const BI_Color& color)
  {
    for (int i = 0; i < 4; i++)
      _color[i] = color._color[i];
  }
  
  void Set(float r, float g, float b, float a = 1.0f)
  {
    _color[0] = r;
    _color[1] = g;
    _color[2] = b;
    _color[3] = a;
  }

  BI_Color()
  {
    Set(0,0,0,1);
  }

  BI_Color(const BI_Color& color)
  {
    Set(color);
  }

  BI_Color(float r, float g, float b, float a = 1.0f)
  {
    Set(r, g, b, a);
  }

  float& operator[](unsigned int i)
  {
    return _color[i];
  }

  float operator[](unsigned int i) const
  {
    return _color[i];
  }

  BI_Color& operator=(const BI_Color& color)
  {
    Set(color);
    return *this;
  }

  BI_Color& operator*(const BI_Color& color)
  {
    for (int i = 0; i < 4; i++)
      _color[i] *= color[i];
    return *this;
  }

  BI_Color& operator*(float m)
  {
    for (int i = 0; i < 4; i++)
      _color[i] *= m;
    return *this;
  }

  BI_Color& operator*=(const BI_Color& color)
  {
    *this = *this * color;
    return *this;
  }

  BI_Color& operator*=(float m)
  {
    *this = *this * m;
    return *this;
  }

  float R() const {return _color[0];}
  float G() const {return _color[1];}
  float B() const {return _color[2];}
  float A() const {return _color[3];}
};

#endif
