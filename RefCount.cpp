#include "RefCount.hpp"

//! Main implementation of the refcount
//! We're going to use intrusive reference counting technique
RefCount::RefCount():_count(0)
{

};

RefCount::~RefCount()
{

};

void RefCount::AddRef()
{
  ++_count;
}

void RefCount::SubRef()
{
  --_count;
  if (_count == 0) delete this;
}

