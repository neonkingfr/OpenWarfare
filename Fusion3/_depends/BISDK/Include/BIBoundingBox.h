#ifndef __BI_BOUDINGBOX__
#define __BI_BOUDINGBOX__

#include "BISDKCommon.h"
#include "BIVector.h"

#include <float.h>
#include <stdio.h> 
#include <string> 
#include <algorithm>
#include <cmath>

/** \brief Class representing bounding box to some object */
 /** Edges of the box are parallel to X, Y, Z axis 
 */
class BI_BoundingBox3
{
	/** \brief Two coordinates of two vertices. */
	/*  No more are needed, as edges are parallel to X, Y, Z axis 
	*/
	BI_Vector3 _coords[2]; 

public:
	/** \brief Contructor */
	/** Sets coordinates of two vertices defining the box 
 	 */
	BI_BoundingBox3()
	{
		_coords[0].SetX(FLT_MAX);
		_coords[0].SetY(FLT_MAX);
		_coords[0].SetZ(FLT_MAX);
		_coords[1].SetX(-FLT_MAX);
		_coords[1].SetY(-FLT_MAX);
		_coords[1].SetZ(-FLT_MAX);
	}

	/** \brief Contructor */
	/** Sets coordinates of two vertices defining the box 
 	 */
	BI_BoundingBox3( float x1, float y1, float z1,
					 float x2, float y2, float z2)
	{
		_coords[0].SetX(BI_Min(x1,x2));
		_coords[0].SetY(BI_Min(y1,y2));
		_coords[0].SetZ(BI_Min(z1,z2));
		_coords[1].SetX(BI_Max(x1,x2));
		_coords[1].SetY(BI_Max(y1,y2));
		_coords[1].SetZ(BI_Max(z1,z2));
	}

	/**\brief Copy Contructor */
	/**	Copies information about position of bounding box 
	*/
	BI_BoundingBox3( const BI_BoundingBox3 & src)
	{
		memcpy(_coords, src._coords, sizeof(BI_Vector3)*2);
	}

	/** \brief Contructor */
	/** Sets coordinates of two vertices defining the box */
	BI_BoundingBox3( const BI_Vector3 &downleft, const BI_Vector3 & upRight)
	{
		_coords[0] = downleft;
		_coords[1] = upRight;
	}

	/** \brief Lower vertex of box */
	/** \return BI_Vector3 representing coordinates of lower vertex of the box
	 */
	BI_Vector3 GetLower() const { return _coords[0]; }

	/** \brief Upper vertex of box 
	 * \return upper vertex of the box
	 */
	BI_Vector3 GetUpper() const { return _coords[1]; }

	/** \brief Assogment operator */
	/** Sets new positions of vertices defining bounding box */
	BI_BoundingBox3 & operator=( const BI_BoundingBox3 & src)
	{
		memcpy(_coords, src._coords, sizeof(BI_Vector3)*2);
		return(*this);
	}

	/** \brief Scales the box */
	/** Enlarges or reduces the box according to parameter
	 * One coordinate remainss the same,
	 * the other will change its  position in such a way that distance from static point
	 * in every direction will be multiplied by f 
	 * \param f scaling parameter
	 */
	void Scale(float f)
	{
		_coords[1] = _coords[0] +(_coords[1] - _coords[0])*f;
	}

	/** \brief Check point container */
	/** Checks if box contains given position 
	 * \return true if position is in box, otherwise false
	 */
	bool Contains(const BI_Vector3 &b)
	{
		return ( Distance(b,BI_Vector3::_X )<=0 ) 
			&& ( Distance(b,BI_Vector3::_Y) <=0 ) 
			&& ( Distance(b,BI_Vector3::_Z) <=0 );
	}

	/** \brief Computes point from box */ 
	/** \returns the smallest distance from this box, -1 if the box contains the point 
	 */
	float Distance(const BI_Vector3 & point)
	{
		BI_Vector3 v(Distance(point,BI_Vector3::_X),Distance(point,BI_Vector3::_Y), Distance(point,BI_Vector3::_Z));
		int i;
		return v.GetMinimum(i);
	}

	/** \brief Computes distance in X axis */
	/** \returns the distance in X direction, -1 if box contains the point in projection to X plane 
	 */
	float Distance( const BI_Vector3 &p, int axis )
	{
		float a = p[axis] - _coords[0][axis];
		float b = p[axis] - _coords[1][axis];
		return(a < b)?a:b;
	}

	/** \brief Computes volume of the box */
	/** \return positive float, volume of the whole box 
	 */
	float Volume()
	{
		BI_Vector3 p(_coords[0]-_coords[1]);
		float volume = p.X()*p.Y()*p.Z();
		if(volume < 0)
			volume *= -1;
		return volume;
	}

  void Shift(BI_Vector3 vector);

	/** \brief Computes intersection between two boxes */
	/** Acoording to character od the boxes, result will be again BoundingBox
	 * \return box that represent box common to two BoundingBoxes. If volume of this box is 0,
	 * the boxes do not intersects 
	 */
	BI_BoundingBox3 Intersection(const BI_BoundingBox3& box)const
	{
		BI_BoundingBox3 out(0,0,0,0,0,0);
		out._coords[0].SetX( std::max<float>(std::min<float>(_coords[0].X(),_coords[1].X()), std::min<float>(box._coords[0].X(),box._coords[1].X())));
		out._coords[0].SetY( std::max<float>(std::min<float>(_coords[0].Y(),_coords[1].Y()), std::min<float>(box._coords[0].Y(),box._coords[1].Y())));
		out._coords[0].SetZ( std::max<float>(std::min<float>(_coords[0].Z(),_coords[1].Z()), std::min<float>(box._coords[0].Z(),box._coords[1].Z())));

		out._coords[1].SetX( std::min<float>(std::max<float>(_coords[0].X(),_coords[1].X()),std::max<float>(box._coords[0].X(),box._coords[1].X())));
		out._coords[1].SetY( std::min<float>(std::max<float>(_coords[0].Y(),_coords[1].Y()),std::max<float>(box._coords[0].Y(),box._coords[1].Y())));
		out._coords[1].SetZ( std::min<float>(std::max<float>(_coords[0].Z(),_coords[1].Z()),std::max<float>(box._coords[0].Z(),box._coords[1].Z())));
		return out;
	}

	/** \brief Enlarge box according to point boxes */
	/** Enlarges the box in such way that box will contains also point defined by point
	 * \param point BI_Vector( or BI_Vector3, as BI_Vector3 is base for BI_Vector3 ). 
	 * If point was not contained in box, it will lie on one side of the box after performin this method
	 */
	BI_BoundingBox3& operator+=( const BI_Vector3 & point)
	{
		BI_BoundingBox3 b(point,point);
		return operator+=(b);
	}
	/** \brief Unifies boxes */
	/** Enlarges the box in such way that box will contains also box defined by src 
	 * \param box is box that will be contained in this instance
	 */
	BI_BoundingBox3& operator+=( const BI_BoundingBox3 & box)
	{
		BI_BoundingBox3 hlp(*this);
		_coords[0].SetX( BI_Min(hlp._coords[0].X(),box._coords[0].X()));
		_coords[0].SetY( BI_Min(hlp._coords[0].Y(),box._coords[0].Y()));
		_coords[0].SetZ( BI_Min(hlp._coords[0].Z(),box._coords[0].Z()));

		_coords[1].SetX( BI_Max(hlp._coords[1].X(),box._coords[1].X()));
		_coords[1].SetY( BI_Max(hlp._coords[1].Y(),box._coords[1].Y()));
		_coords[1].SetZ( BI_Max(hlp._coords[1].Z(),box._coords[1].Z()));
		return *this;
	}

	/** \brief Comparatory operator */
	/** Checks if the positions of the box are the same 
	 * \return true if the boxes are on  the same position, false otherwise 
	 */
	bool operator==( const BI_BoundingBox3 & src)
	{
		return _coords[0]==src._coords[0];
		if(( _coords[0] == src._coords[0] ) 
			&&(_coords[1] == src._coords[1]))
			return true;
		return(_coords[0] == src._coords[1]) &&(_coords[1] == src._coords[0]) ; //TODO check!
	}
	/** \brief Comparatory operator */
	/** checks if the boxes differ in at least one point */
	bool operator!=( const BI_BoundingBox3 & src) { return !((*this)==src); }
};
#endif //__BI_BOUDINGBOX__