#ifndef __BI_Model__
#define __BI_Model__

#include "..\BISdkCommon.h"
#include "..\BIBoundingBox.h"
#include "BIMesh.h"
#include "BIPreprocessFile.h"
#include "..\BIErrorListener.h"

/** \addtogroup modelSDK */
/** \{ */

/** \brief Class representing whole object */
/** Manager of meshes.
 */
class BISDK_DLLEXTERN BI_Model
{
  bool _dirty;
	/** P3d object representation 
	 * this structure stores all data that defines object
	 */
	BI_InternalLODObject * _p3dObject;
	
	/** \brief Load data */
	/** Replaces content of object with data readed from the file
	 * \param f std stream with prepared data
	 * \param callback Interface with function that will be called to read user 
	 * specific data associated with the p3d
	 */
	BI_ErrCode Load(FILE * f, BI_ModelPreprocessLoadCallback *callback = 0);

  size_t ClipFormat();

public:
	/** \brief Create new, empty model, with no mesh and no resolution */
	/** This is completely empty mesh. It contains nothing, not even default resolution. It is suppossed that user should fill it
	 * If not so, mesh is not valid and cannot be saved. There can be called BI_Mesh::Load method to fill it or fill it manually by adding meshes
	 */
	BI_Model();

	/** \brief Model created from valid file */
	/** If file is not valid, the result is not defined */
	BI_Model(const char * nameOfFile);

	/** \brief Creates BI_Model from existing BI_Mesh and assigns the mesh resolution 0.0f */
	BI_Model(BI_Mesh &firstlod, float resolution=0.0f);

	/** \brief Destructor */
	~BI_Model();

  /** \brief save model to clipboard */
  BI_ErrCode SaveToClipboard();

  /** \brief Load model to clipboard */
  BI_ErrCode LoadFromClipboard( );

  /** \brief register listener */
  /* This listener will be use when reporting some errors */
	void RegisterListener(BI_ErrorListener * listener);

  /* \brief remove listener from the model */
  /* The listener won't be deleted */
  void RemoveListener(BI_ErrorListener * listener);

	/** \name BI_Mesh handling */
	/** \{ */
    /** \brief Returns active BI_Mesh */
	/** \note rememeber, that always must be some mesh active. When calling on completely empty mesh, return 0, 
	 * even though no mesh with 0 id is present. However, when working with model, most likely will be present 
	 * at least one mesh, so this behaviour is correct 
	 */
    BI_Mesh GetActiveMesh() const;

	/** \brief Binds mesh to this model */
	/** From now on, changes made on this instance will affect model as well. This does not inserts bes to model, it is used only to retrieve
	 * \return BIEC_OK if success
	 * \return BIEC_UnableToBind if there is no such resolution
	 */
	BI_ErrCode Bind(BI_Mesh& mesh, float resolution);
  BI_ErrCode Bind(BI_Mesh& mesh, int level) /*rebind */;
	/** \brief Unbinds the mesh from model */
	/** Removes mesh from model but do not destroys it. */
	BI_ErrCode Unbind(BI_Mesh& mesh);

	/** \} */

	/** \name level managing */
	/** \{ */

    /** \brief Retrieves index of active level */
    int GetActiveLevel() const;

    /** \brief BI_Mesh at level specified by level */
    BI_Mesh GetMeshAtLevel(int level) const;   
        
	/**	\brief Sets object with this level a active */
    void SelectLevel( int level ); 
    
	/** \brief Retrieves number of meshes bounded to this model */
    int NumberOfMeshes() const;
       
	/** \brief Finds level */
	/**	\param resolution resolution to search 
	 \return index of mesh with nearest resolution
	*/
	int FindLevel( float resolution ) const;

	/** \brief Set mesh at position */
	/** If there was previously any mesh, it will be destroyed. If there is on this place 
   * any other BI_Mesh, it will be destroyed. BI_Meshes that were bouded to this mesh, cannot be used after result. 
   */
	int SetLevel(const BI_Mesh & mesh, float resolution);

	///Finds exact level
	/**
	 * \param resolution resolution to search
	 * \return index of mesh with specified resolution, 
	 * or -1 if mesh with specified resolution not exists
	 */
	int FindLevelExact( float resolution ) const;

	///Retrieves resolution of the mesh
	float GetResolution( int level ) const;
	
	///Changes resolution of mesh specified by its current resolution
	bool ChangeResolution( float oldRes, float newRes );

	///Sets resolution at level
	int SetResolution( int level, float newRes );

	///Removes level        
	bool DeleteLevel( int level );

	///Adds new level to the LODMesh
	/**
	 * \param mesh source mesh, it will used as template of that level. 
	 * Function adds copy of the mesh, so later changes in the source object
	 * will not affect added level.
	 * \param resolution resolution
	 * \retval index of the new level
	 * \retval -1 if it was impossible to add level
	 */
	int AddLevel(const BI_Mesh & mesh, float resolution );
    
	/** \} */

	/** \name Load & Save */
	/** \{ */
	/** \brief Load data */
	/** Replaces content of object with data read from the file
	 * \param filename name of P3D file. if extension is not 'p3d', it will be added. (case sensitive)
	 * \param callback Interface with function that will be called to read user 
	 * specific data associated with the p3d
	 * \retval BIEC_OK indicates success
	 * \retval BIEC_UnableOpen indicates file not allowed for reading
	 */
	BI_ErrCode Load( const char * filename, bool part = false,  BI_ModelPreprocessLoadCallback *callback = 0 );

	/** \brief Save data */
	/** Stores content of object into the external file
	\param filename name of P3D file
	\param callback Interface with function that will be called to write user 
	specific data associated with the p3d
	\retval BI_OK success
	\retval BI_UnableToSave if something prevented from saving to disk
	\retval BIEC_UnableToSaveCallbackData if callback data were not saved
	*/
  BI_ErrCode Save(const char *filename, BI_ModelPreprocessSaveCallback *callback = 0 , const char * fbxDataPath="P:\\" ) const;

	/** \} */

	/** \brief Merge two models*/
	/** Merges one LODMesh with another Processes all resolutions and merges meshes with the same resolution
	\param src source LODMesh
	\param createlods specify true, if you wish to enable creation of the new levels. 
	If this flag is false, function will not merge levels that
	is not exists in target object, until fillLods is specified
	\param createSel if specified, function will create selection with this name 
	for all faces and points added to the levels
	\param fillLods for not existing levels function will use nearest unused levels
	\retval true success
	\retval false fail
	*/
	bool Merge(const BI_Model &src , bool createlods, const char *createSel=NULL, bool fillLods = false);
        
	///Centers all objects 
	/** \return the actual center. all meshes are resize to have common bounding box */
	BI_Vector3 CenterAll();

	/// Clear model
	/** removes all meshes. Such a model is unable to save */
	void Clear();

	///Calculates axis aligned bounding box -> bounding box
	/** \return bounding box */
	BI_BoundingBox3 GetAABB()const;

	/** \name Operators */
	/** \{ */
	/** \brief  Assign operator */
	/** This destruct all the data that were previously allocated and fill it new data. All data that 
	 * were bounded to this model ( or sets, faces, selections bounded to BI_Mesh in this model will be invalidated 
	 */
	BI_Model & operator=( const BI_Model& src );

	///access operator
	/** Since model consists only from meshes, access operator returns one mesh that is identified by idx. 
	 * This is the only way to change somthing inside the Model when not using BI_Mesh::Bind, except for all sorts of Get methods.
	 * When inserting, you cannot rely on the index not changing
	 */
	BI_Mesh operator[]( int idx );

	/** \} */

  void SetChanged(bool changed){ _dirty = changed; }
  bool Changed()const { return _dirty; }
  BI_ErrCode LoadRTM(const char * path, bool allLods);
};
/** \} */
#endif //__BI_Model __