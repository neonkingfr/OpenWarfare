#ifndef __OXYGENESDKV_2___
#define __OXYGENESDKV_2___


#include "..\BISdkCommon.h"
#include "..\BIError.h"
#include "..\BIString.h"
#include "..\BIVector.h"
#include "..\BIVertex.h"
#include "BIModelVersion.h"
#include "BIMesh.h"
#include "BIModel.h"
#include "BIFace.h"
#include "BISpecialLods.h"
#include "BISpecialVertexFlags.h"
#include "BIFaceFlags.h"
#include "BISelection.h"

#endif