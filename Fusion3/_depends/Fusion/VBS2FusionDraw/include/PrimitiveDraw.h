
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:

Purpose:

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			00-00/2011	YFP: Original Implementation
	1.1			00-00/2012	KAJ: Extended for 2D drawings
/*****************************************************************************/

#ifndef VBS2FUSION_DRAW_PRIMITIVEDRAW_H
#define VBS2FUSION_DRAW_PRIMITIVEDRAW_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <vector>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "VBSFusionDraw.h"
#include "position3D.h"
#include "math/Vector3f.h"
#include "math/spline.h"
#include "VBSFusionDefinitions.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{
	namespace Draw
	{
		class VBS2FUSION_DRAW_API PrimitiveDraw
		{
		public:

		 /*!
			@description 
			Used to draw a Filled PolygonBox with given parameters within the VBS2 environment.

			@locality

			Locally Effected 
			
			@version [VBS2Fusion v3.02]

			@param positions - A vector consisting of vertices of the type position3D.
			@param height - Height of the PolygonBox in meters.
			@param color - Color of the PolygonBox (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the PolygonBox.

			@return Nothing.

			@example

			@code

			//Define the Position

			vector<position3D> posList1;
			posList1.push_back(endPos1);	
			posList1.push_back(endPos3);
			posList1.push_back(endPos2);
			posList1.push_back(endPos4);

			//Define the Color 

			VBSFusion::Draw::ColorRGBA_FD col2(1,0,0,1);

			//Draw a filled polygon box

			VBSFusion::Draw::PrimitiveDraw::DrawPolygonBoxFill(posList1,0,col2,"FilledPolygonBox");

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawPolygonBoxFill(const std::vector<position3D>& positions, float height, const ColorRGBA_FD& color, const std::string& name);
		
		 /*!
			@description 
			Used to draw a Wired PolygonBox(outline) with given parameters within the VBS2 environment.
	 
			@locality

			Locally Effected 

			@version [VBS2Fusion v3.02]

			@param positions - A vector consisting of vertices of the type position3D.
			@param height - Height of the PolygonBox in meters.
			@param color - Color of the PolygonBox (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the PolygonBox.

			@return Nothing.

			@example

			@code

			//Define the Position, (endPos1, endPos2, endPos3, endPos4 should have been defined before)

			vector<position3D> posList1;
			posList1.push_back(endPos1);	
			posList1.push_back(endPos3);
			posList1.push_back(endPos2);
			posList1.push_back(endPos4);

			//Define the Color 

			VBSFusion::Draw::ColorRGBA_FD col(0,0,1,1);

			//Draw a polygon box out line

			VBSFusion::Draw::PrimitiveDraw::DrawPolygonBoxOutline(posList1,10,col,"PolygonBoxOutline");

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawPolygonBoxOutline(const std::vector<position3D>& positions, float height, const ColorRGBA_FD& color, const std::string& name);					
		 
		 /*!
			@description 
			Used to draw a Filled Polygon with given parameters within the VBS2 environment.

			@locality

			Locally Effected 

			@version  [VBS2Fusion v3.02]

			@param positions - A vector consisting of vertices of the type position3D.
			@param color - Color of the polygon (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the polygon.

			@return Nothing.

			@example

			@code

			//Define the Position, (endPos1, endPos2, endPos3, endPos4 should have been defined before)

			vector<position3D> posList1;
			posList1.push_back(endPos1);
			posList1.push_back(endPos3);
			posList1.push_back(endPos2);
			posList1.push_back(endPos4);
			posList1.push_back(endPos5);

			//Define the Color 

			VBSFusion::Draw::ColorRGBA_FD col2 (1,0,0,1);

			//Draw a filled polygon

			VBSFusion::Draw::PrimitiveDraw::DrawPolygonFill(posList1,col2,"FilledPolygon");

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawPolygonFill(const std::vector<position3D>& positions, const ColorRGBA_FD& color, const std::string& name);

		 /*!
			@description 
			Used to draw a Filled Triangle with given parameters within the VBS2 environment.

			@locality

			Locally Effected 

			@version  [VBS2Fusion v3.02]

			@param pointA - First vertex of the triangle.
			@param pointB - Second vertex of the triangle.
			@param pointC - Third vertex of the triangle.
			@param color - Color of the triangle (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the triangle.

			@return Nothing.

			@example

			@code

			//Define the Color 

			VBSFusion::Draw::ColorRGBA_FD col2(1,0,0,1);

			//Draw a filled triangle(endPos1, endPos2, endPos3, endPos4 should have been defined before)

			VBSFusion::Draw::PrimitiveDraw::DrawTriangleFill(endPos4,endPos3,endPos2,col2,"FilledTriangle");

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawTriangleFill(const position3D& pointA, const position3D& pointB, const position3D& pointC, const ColorRGBA_FD& color, const std::string& name);

		 /*!
			@description 

			Used to draw a Filled Rectangle with given parameters within the VBS2 environment.

			@locality

			Locally Effected 

			@version  [VBS2Fusion v3.02]

			@param pointA - Starting point.
			@param normalVec - The vector perpendicular to the rectangle (The direction (side) in which the filled rectangle is facing).
			@param height - Height or length of the Rectangle in meters.
			@param width - Width of the Rectangle in meters.
			@param color - Color of the Rectangle (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the Rectangle.

			@return Nothing.

			@example

			@code

			//Define the Color 

			VBSFusion::Draw::ColorRGBA_FD col2;
			col2.b=1;
			col2.a=1;

			vector3D normVec(0,0,1);

			VBSFusion::Draw::PrimitiveDraw::DrawRectangleFill(centerPos, normVec,20,20,col2, "FilledRectangle");

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawRectangleFill(const position3D& pointA, const vector3D& normalVec, float height, float width, const ColorRGBA_FD& color, const std::string& name);

		 /*!
			@description 
			Used to draw a Wired Rectangle with given parameters within the VBS2 environment.

			@locality

			Locally Effected

			@version  [VBS2Fusion v3.02]

			@param pointA - Starting point.
			@param normalVec -The vector perpendicular to the rectangle (The direction (side) in which the rectangle is facing).
			@param height - Height or length of the Rectangle in meters.
			@param width - Width of the Rectangle in meters.
			@param color - Color of the Rectangle (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the Rectangle.

			@return Nothing.

			@example

			@code

			//Define the Color 
			VBSFusion::Draw::ColorRGBA_FD col(0,0,1,1);
			vector3D normVec(0,0,1);
			VBSFusion::Draw::PrimitiveDraw::DrawRectangle(centerPos, normVec,20,20,col, "Rectangle")

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawRectangle(const position3D& pointA,  const vector3D& normalVec, float height, float width, const ColorRGBA_FD& color, const std::string& name);

		 /*!
			@description 
			Used to draw Line Strip. (i.e the function draws straight lines, that connects the given positions within the VBS2 environment).

			@locality

			Locally Effected

			@version [VBS2Fusion v3.02] 

			@param points -  A container consisting of vertices of the type position3D.
			@param color - Color of the LineStrip (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the LineStrip.

			@return Nothing.

			@example

			@code

			//Define the Color 

			VBSFusion::Draw::ColorRGBA_FD col2 (0,1,0,1);

			//Define the Position, (endPos1, endPos2, endPos3, endPos4 should have been defined before)

			vector<position3D> posList1;
			posList1.push_back(endPos1);
			posList1.push_back(endPos6);
			posList1.push_back(endPos2);
			posList1.push_back(endPos5);
			posList1.push_back(endPos1);

			VBSFusion::Draw::PrimitiveDraw::DrawLineStrip(posList1,col2,"LineStrip");

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawLineStrip(const std::vector<position3D>& points, const ColorRGBA_FD& color, const std::string& name);

		 /*!
			@description 

			Used to draw a Solid Sphere with given parameters within the VBS2 environment.

			@locality

			Locally Effected

			@version  [VBS2Fusion v3.02]

			@param center - The center position of the sphere.
			@param radius - Radius of the sphere in meters. This value should be greater than or equal to 0.0f.
			@param color - Color of the sphere (each member's range is 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the sphere.
			@param sliceCount - Number of slices about the main axis.
			@param stackCount - Number of stacks along the main axis.

			@return Nothing.

			@example

			@code

			//Define the Color 

			VBSFusion::Draw::ColorRGBA_FD col (0,0,1,1);

			//Define the Position

			position3D endPos1 = position3D(2380,2650,20);

			VBSFusion::Draw::PrimitiveDraw::DrawSphere(endPos1,20,col,"Sphere",20,10);

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawSphere(const position3D& center, float radius, const ColorRGBA_FD& color, const std::string& name, int sliceCount, int stackCount );

		 /*!
			@description 

			Used to draw a filled cylinder with given parameters within the VBS2 environment.

			@locality

			Locally Effected

			@version  

			@param center - The center position of the cylinder.
			@param radius1 - Radius of face one (one end)in meters.
			@param radius2 - Radius of face two (the other end) in meters.
			@param lenght - Length between two faces (circular ends) in meters.
			@param color - Color of the Cylinder (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the Cylinder.
			@param sliceCount - Number of slices about the main axis.
			@param stackCount - Number of stacks along the main axis.

			@return Nothing.

			@example

			@code

			//Define the Color 

			VBSFusion::Draw::ColorRGBA_FD col2 (0,0,1,1);
			
			//endPos should have been defined before 

			VBSFusion::Draw::PrimitiveDraw::DrawCylinderFill(endPos2, 5,5,10,col2,"FilledCylinder",30,40 ); 

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawCylinderFill(const position3D& center, float radius1, float radius2, float lenght, const ColorRGBA_FD& color, const std::string& name, int sliceCount, int stackCount);

		 /*!
			@description 
			Used to draw a Triangle with given parameters within the VBS2 environment.

			@locality

			Locally Effected 

			@version  [VBS2Fusion v3.02]

			@param pointA - The first vertex of the triangle.
			@param pointB - The second vertex of the triangle.
			@param pointC - The third vertex of the triangle.
			@param color - The color of the triangle (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the triangle.

			@return Nothing.

			@example

			@code

			//Define the Color
			VBSFusion::Draw::ColorRGBA_FD col(0,0,1,1);

			//The vertices endPos1, endPos2, endPos3, endPos4 should have been defined before
			VBSFusion::Draw::PrimitiveDraw::DrawTriangle(endPos1,endPos3,endPos4,col,"Triangle");

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawTriangle(const position3D& pointA, const position3D& pointB, const position3D& pointC, const ColorRGBA_FD& color, const std::string& name);

		 /*!
			@description 
			Used to draw a circle with given parameters within the VBS2 environment.

			@locality

			Locally Effected 

			@version [VBS2Fusion v3.02] 

			@param centre - The center position of the circle.
			@param radius - The radius of the circle.
			@param normalVector - The vector perpendicular to the circle (The direction (side) in which the circle is facing).
			@param color - The color of the circle (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the circle.

			@return Nothing.

			@example

			@code

			// Define the Color
			VBSFusion::Draw::ColorRGBA_FD col(0,1,0,0.5);

			Vector3f normVec(0.0f,0.0f,1.0f);

			VBSFusion::Draw::PrimitiveDraw::DrawCircle(centerPos,10,normVec,col,"Circle");

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawCircle(const position3D& centre, float radius, const Vector3f& normalVector, const ColorRGBA_FD& color, const std::string& name);

		 /*!
			@description 
			Used to draw a line with given parameters in 3D space within the VBS2 environment. (In the RTE view mode a 2D projection of the particular 3D line can be seen).

			@locality

			Locally Effected 

			@version  [VBS2Fusion v3.02] 

			@param start - The starting point of a line in position3D format.
			@param end - The end point of line in position3D format.
			@param color - The color of the line (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name - Name of the line.

			@return Nothing.

			@example

			@code

			//Define the Color 

			VBSFusion::Draw::ColorRGBA_FD col (0,1,0,1);
			
			//endPos1 and endPos4 should have been defined before
			VBSFusion::Draw::PrimitiveDraw::DrawLine(endPos1,endPos4,col,"Line");

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void DrawLine(const position3D& start, const position3D& end, const ColorRGBA_FD& color, const std::string& name);

		 /*!
			@description 
			Used to set the visibility of the drawn object within the VBS2 environment

			@locality

			Locally Effected

			@version  [VBS2Fusion v3.02] 

			@param name - The name of the object, through which the visibility status is set.
			@param isVisible - Visibility Status.

			@return Nothing.

			@example

			@code

			VBSFusion::Draw::PrimitiveDraw::SetVisible("Sphere",false);

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void SetVisible(const std::string& name, bool isVisible);

		 /*!
			@description 
			Used to set the visibility status of the Drawn Object  according to the given APPLICATIONSTATE. (i.e. the function
			hides the drawn object if the given application state and the VBS application state are same).

			@locality

			Locally Effected

			@version  [VBS2Fusion v3.03] 

			@param name - The name of the object.
			@param appState - The APPLICATIONSTATE enum holds the application state.

			@return Nothing.

			@example

			@code

			VBSFusion::Draw::PrimitiveDraw::SetDisableAppState("Triangle",MISSION);

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
			static void SetDisableAppState(const std::string& name, APPLICATIONSTATE appState);
		 
		 /*!
			@description 
			Used to delete a drawn object within the VBS2 environment.

			@locality

			Locally Effected 

			@version  [VBS2Fusion v3.02]

			@param name - The name of the object to be deleted.

			@return Nothing.

			@example

			@code

			VBSFusion::Draw::PrimitiveDraw::DeletePrimitive("Sphere");

			@endcode

			@overloaded

			None.

			@related

			Draw::DrawPolygonBoxOutline(vector<position3D>& positions, float height, ColorRGBA_FD color,string name);

			@remarks
		 */
			static void DeletePrimitive(const std::string& name);
		 
		 /*!
			@description 
			Used to rotate a specified drawn object  about a position along the X axis at any given angle within the VBS2 environment.

			@locality

			Locally Effected 

			@version  [VBS2Fusion v3.09]

			@param name - The name of the object that needs to be rotated. 
			@param angle - The angle(degrees) in which the rotation should take place along the X-Axis.
			@param pos - The position at which the rotation should take place.

			@return bool - Returns true if rotation was applied on the object. 

			@example

			@code

			//endpos2 should have been defined

			bool isRotatePX = VBSFusion::Draw::PrimitiveDraw::RotatePrimitiveX("FilledCylinder" , 10, endPos2);

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::RotatePrimitiveZ(const string& name , float angle, const position3D& pos)

			@remarks
		 */
		 static bool RotatePrimitiveX(const std::string& name , float angle, const position3D& pos);

		 /*!
			@description 
			Used to rotate a specified drawn object  about a position along the Z axis at any given angle within the VBS2 environment.

			@locality

			Locally Effected 

			@version  [VBS2Fusion v3.09]

			@param name - The name of the object that needs to be rotated. 
			@param angle - The angle(degrees) in which the rotation should take place along the Z-Axis.
			@param pos - The position at which the rotation should take place.

			@return bool - Returns true if rotation was applied on the object. 

			@example

			@code

			//endpos2 should have been defined

			bool isRotatePZ = VBSFusion::Draw::PrimitiveDraw::RotatePrimitiveZ("FilledCylinder" , 10, endPos2);	

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::RotatePrimitiveY(const string& name , float angle, const position3D& pos)

			@remarks
		 */
		 static bool RotatePrimitiveZ(const std::string& name , float angle, const position3D& pos);

		 /*!
			@description 
			Used to rotate a specified drawn object  about a position along the Y axis at any given angle within the VBS2 environment.

			@locality

			Locally Effected 

			@version  [VBS2Fusion v3.09]

			@param name - The name of the object that needs to be rotated. 
			@param angle - The angle(degrees) in which the rotation should take place along the Y-Axis.
			@param pos - The position at which the rotation should take place.

			@return bool - Returns true if rotation was applied on the object. 

			@example

			@code

			//endpos2 should have been defined

			bool isRotatePY = VBSFusion::Draw::PrimitiveDraw::RotatePrimitiveY("FilledCylinder" , 10, endPos2);	

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::RotatePrimitiveX(const string& name , float angle, const position3D& pos)

			@remarks
		 */
		 static bool RotatePrimitiveY(const std::string& name , float angle, const position3D& pos);
		 
		 /*!
		 @description 
		 Used to draw draw a Filled Circle with given parameters within the VBS2 environment.

		 @locality

		 Locally Effected 

		 @version  [VBS2Fusion v3.02]

		 @param centre - The center position of the filled circle.
		 @param radius - The radius of the filled circle.
		 @param normalVector - The vector perpendicular to the filled circle (The direction (side) in which the circle is facing).
		 @param color - The color of the circle (each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
		 @param name - Name of the filled circle.

		 @return Nothing.

		 @example

		 @code

		 // Define the Color
		 VBSFusion::Draw::ColorRGBA_FD col2 (1,0,0,1);

		 Vector3f normVec(0.0f,0.0f,1.0f);

		 //centerPos should have been defined before
		 VBSFusion::Draw::PrimitiveDraw::DrawCircleFill(centerPos,10,normVec,col2,"FilledCircle");

		 @endcode

		 @overloaded

		 None. 

		 @related

		 PrimitiveDraw::DeletePrimitive(string name);

		 @remarks
		 */
		 static void DrawCircleFill(const position3D& centre, float radius, const Vector3f& normalVector, const ColorRGBA_FD& color, const std::string& name);

		 /*!
			@description 
			Used to transform a given object by a given offset position within the VBS2 environment.

			@locality

			Locally Effected 

			@version [VBS2Fusion v3.02]

			@param name - The name of the object which should be transformed. 
			
			@param position - Offset  = Decided position - current position (Refer example)

			@return bool - Returns true if successful.

			@example

			@code

			//The object would be transformed (translated) up 30 meters (about the y axis) 
			position3D ofset = position3D(0,0,30);

			VBSFusion::Draw::PrimitiveDraw::SetPrimitiveTransform("Sphere",ofset);

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
		 static bool SetPrimitiveTransform(const std::string& name, const position3D& position);

		 /*!
			@description 

			Used to change the color of the object within the VBS2 environment. 

			@locality

			Locally Effected 

			@version  [VBS2Fusion v3.02]

			@param name - The name of the Draw object for which the color is changed. 
			@param color - The color applied to the object(each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).

			@return bool - Returns true if the change of color was successful.

			@example

			@code

			//Define the Color
			VBSFusion::Draw::ColorRGBA_FD col1(0, 128, 128, 0.90f);
		
			VBSFusion::Draw::PrimitiveDraw::SetPrimitiveColor("Sphere",col1);

			@endcode

			@overloaded

			None.

			@related

			PrimitiveDraw::DeletePrimitive(string name);

			@remarks
		 */
		 static bool SetPrimitiveColor(const std::string& name, const ColorRGBA_FD& color);

		 /*!
			@description 
		 
			Used to draw a filled Box with given parameters in 3D space within the VBS2 environment. (In the RTE map view mode a 2D projection of the center plane of that filled box can be seen).

			@locality

			Locally Effected

			@version  [VBS2Fusion v3.02]
		 
			@param startPosition - The center position of the Box in position3D format.
			@param length	     - The length of the box (along the X axis before rotating at a specified angle).
			@param width	     - The width of the box (along the Z axis before rotating at a specified angle).
			@param height		 - The height of the box (along the Y axis before rotating at a specified angle).
			@param angleRotX	 - The angle (degrees) in which the object should be rotated about the X axis.
			@param angleRotY	 - The angle (degrees) in which the object should be rotated about the Y axis..
			@param angleRotZ	 - The angle (degrees) in which the object should be rotated about the Z axis.
			@param color		 - The color applied to the object(each member's range is within 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name			 - The name of the filled box object.

		 @return	Nothing.

			@example

			@code

			//Define the Color
			VBSFusion::Draw::ColorRGBA_FD col(1,0,0,1);

			//startPos1 should have been defined before
			VBSFusion::Draw::PrimitiveDraw::DrawBoxFill(startPos1,20,20,20,0,0,0,col, "FilledBox");

			@endcode

			@overloaded
			None.

			@related

			@remarks If the angleRotX=0 ,angleRotY=0 & angleRotZ=0. The Box will be drawn  parallel to the x,y,z axis.
		 */
		 static void DrawBoxFill(const position3D& startPosition, float length, float width, float height, float angleRotX, float angleRotY, float angleRotZ, const ColorRGBA_FD& color, const std::string& name);

			/*!
			@description 
			Draw a Curvely Tube with given parameters.

			@locality

			Locally effected.

			@version  [VBS2Fusion v3.15]

			@param controlPoints - At least 4 points should be in this controlPoints. The control points are middle points of the tube.
								   According to the control points function generate a curve line.	
								   Then the tube will be drawn in the path of curve line from first control point to last control point with the given radius.
			@param radius		 - Radius of the tube cross section.
			@param color		 - Color of CurveTube (each member's range is 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name			 - Draw object name that needs to accessed. 
			@param sliceCount    - No of slices between two control points. In side the function, required intermediate point(slices) 
								   in between two given controlPoints will be calculated. If given 'sliceCount' is greater than
								   the calculated value then calculated value will be used. This calculation uses distance between two intermediate points as 2m.
			@param stackCount    - No of points laid on circumference of the circle. If given 'radius is less than '3' given 'stackCount' ignored and default value (3) will be used. 
								   If not so number of required points will be calculated depend on the circumference. If given 'stackCount' is greater than the calculated value then calculated value will be used. 

			@return Nothing.

			@example

			@code

			//Declare a position3D vector.
			vector<position3D> projList;

			//Push the positions to the vector.
			projList.push_back(position3D(8257.5,8559.1,307.9));
			projList.push_back(position3D(8248.1,8561,306.9));
			projList.push_back(position3D(8229.4,8564.9,304.7));
			projList.push_back(position3D(8220,8566.8,303.5));
			projList.push_back(position3D(8210.6,8568.7,302.3));
			projList.push_back(position3D(8201.3,8570.6,301.1));
			projList.push_back(position3D(8191.9,8572.5,299.8));

			//Define a color.
			VBSFusion::Draw::ColorRGBA_FD colT1;

			colT1.r = 0;
			colT1.g = 1;
			colT1.b = 0;
			colT1.a = 0.5;

			//Draw the curve tube.
			PrimitiveDraw::DrawCurveTube(projList, 10, colT1, "Curve", 50, 50);

			@endcode

			@overloaded

			@related

			@remarks
			*/
		 static void DrawCurveTube(const std::vector<position3D>& controlPoints, float radius, const ColorRGBA_FD& color, const std::string& name, int sliceCount, int stackCount);

			/*!
			@description 
		 Draw a Brush line on the terrain with the given parameters.

			@locality

			Locally applied.

			@version  [VBS2Fusion v3.15]

			@param controlPoints - At least 2 points should be in this controlPoints. controlPoints are the center points in the brush line. 
								   These values are interpolated to draw a smooth line. Only the x,z components are processed in the controlPoints vector.
			@param radius		 - Radius of the brush.
			@param color		 - Color(each member's range is 0.0f - 0.1f of ColorRGBA_FD struct).
			@param name			 - Draw object name that needs to accessed. 
			@param sliceCount    - No of slices between two control points. This value is used to interpolate the line to appear smoothly. 
						           Increasing this value makes the line appear smoother. 
			@param stackCount    - No of points laid on circumference of the circle on the brush ends.
			@param heightOffset  - The distance gap between the brush line and the terrain. The optimal values for better aesthetic appearance lies around 0.1

			@return Nothing.

			@example

			@code

			//Declare a position3D vector.
			vector<position3D> projList;

			//Push the positions to the vector.
			projList.push_back(position3D(8257.5,8559.1,307.9));
			projList.push_back(position3D(8248.1,8561,306.9));
			projList.push_back(position3D(8229.4,8564.9,304.7));
			projList.push_back(position3D(8220,8566.8,303.5));
			projList.push_back(position3D(8210.6,8568.7,302.3));
			projList.push_back(position3D(8201.3,8570.6,301.1));
			projList.push_back(position3D(8191.9,8572.5,299.8));

			//Define a color.
			VBSFusion::Draw::ColorRGBA_FD colT1;

			colT1.r = 0;
			colT1.g = 1;
			colT1.b = 0;
			colT1.a = 0.5;

			//Draw the brush line.
			PrimitiveDraw::DrawBrush(projList, 10, colT1, "BrushLine", 50, 50);

			@endcode

			@overloaded

			@related

			@remarks
		 */
		 static void DrawBrush(const std::vector<position3D>& controlPoints, float radius, const ColorRGBA_FD& color,  const std::string& name, int sliceCount, int stackCount, float heightOffset = 0.1f);

		 /*!
			@description 
		 Specify the object to use ambient light, i.e in the night time, the object appears darker. Set this value to false to enable the visibility of the object 
		 at night time. When an object is created, this value is set to false by default.

			@locality

			Locally effected.

			@version  [VBS2Fusion v3.15]
			
			@param name - String identifier of the Draw object 

			@param isEnabled - setting this 'value' to true calculates the color using the ambient light. By setting 'false' will make object visible at night.

			@return bool - Return true on succeed.

			@example

			@code

			VBSFusion::Draw::PrimitiveDraw::UseAmbientLight("triangle",false);

			@endcode

			@overloaded

			@related

			@remarks
		 */
		 static bool UseAmbientLight(const std::string& name, bool isEnabled);

		 /*!
		 @description 

		 Sets the visibility of all Draw objects in the 2D Map view. By default this is set to true.

		 @locality

		 Locally Applied, Locally Effected.

		 @version [VBS2Fusion v3.15]

		 @param	show - Turns the visibility on/off of the PrimitiveDraw objects in the 2D map. 

		 @return Nothing

		 @example

		 @code

		 //To turn off visibility of PrimitiveDraw objects in the 2D map view
		 PrimitiveDraw::ShowObjectsIn2DMap(false);

		 @endcode

		 @overloaded

		 None

		 @related

		 None

		 @remarks
		 */
		 static void ShowObjectsIn2DMap(bool show);


		 /*!
		 @description 
		 Sets the radius of the already drawn SPHERE, CURVE TUBE and BRUSH objects.

		 @locality

		 Locally effected.

		 @version [VBS2Fusion 3.15]

		 @param name	-	String identifier of the Draw object

		 @param radius	-	New radius of the Draw object

		 @return Nothing

		 @example

		 @code

		 //Set the radius of the sphere to 30
		 VBSFusion::Draw::PrimitiveDraw::SetRadius("sphr",30);

		 @endcode

		 @overloaded

		 @related

		 @remarks
		 */
		 static bool SetRadius(const std::string& name, float radius);

		 /*!
		 @description 
		 Sets the control points of the already drawn CURVE TUBE and BRUSH objects.

		 @locality

		 Locally effected.

		 @version [VBS2Fusion v3.15]

		 @param name - String identifier of the Draw object

		 @param controlPoints -	A vector consisting of control points of the desired draw object

		 @return Nothing

		 @example

		 @code

		 //Define position3D vector.
		 vector<position3D> cPoints;

		 //push the positions to the vector.
		 cPoints.push_back(position3D(10692.6,8825.79,320));
		 cPoints.push_back(position3D(10686.8,8822,309.6));
		 cPoints.push_back(position3D(10681,8818.2,298.5));
		 cPoints.push_back(position3D(10675.2,8814.29,286.8));
		 cPoints.push_back(position3D(10669.4,8810.5,274.5));
		 cPoints.push_back(position3D(10663.6,8806.6,261.7));
		 cPoints.push_back(position3D(10657.7,8802.79,248.1));

		 //Set the control points draw CURVE TUBE.
		 PrimitiveDraw::SetControlPoints("Curve", cPoints);

		 @endcode

		 @overloaded

		 @related

		 @remarks
		 */
		 static bool SetControlPoints(const std::string& name, const std::vector<position3D>& controlPoints);

		 /*!
		 @description 
		 Adds a control point to the already drawn CURVE TUBE and BRUSH objects. The new point will be appended to the 
		 object's control points, and the new object will be drawn. 

		 @locality

		 Locally effected.

		 @version [VBS2Fusion 3.15]

		 @param name			-	String identifier of the Draw object

		 @param controlPoint	-	A Position3D value that needs to be appended to the existing control points.

		 @return true if successful

		 @example

		 @code

		 //Add a control point to drawn curve.
		 PrimitiveDraw::AddControlPoint("Curve", position3D(10692.6,8825.79,320));

		 @endcode

		 @overloaded

		 @related

		 @remarks
		 */
		 static bool AddControlPoint(const std::string& name, const position3D& controlPoint);


		};

	}
}


#endif //VBS2FUSION_DRAW_PRIMITIVEDRAW_H