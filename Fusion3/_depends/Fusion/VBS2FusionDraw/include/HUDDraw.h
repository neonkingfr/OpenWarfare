 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:

Purpose:

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			00-00/2011	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_DRAW_HUDDRAW_H
#define VBS2FUSION_DRAW_HUDDRAW_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <vector>
// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "VBSFusionDraw.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{
	namespace Draw
	{
		class VBS2FUSION_DRAW_API HUDDraw
		{
		public:

			/*!
			@description

			Used to draw a line of given "color" from "start" point to "end" point. A unique name must provided for the line to be drawn.
			The width is the line width in pixels.
			The centre point of VBS2 screen is considered as (0,0),left-bottom as (-1,-1) and top-right as (1,1),thus the 
			range of X and Y axes varies from (-1) to 1.
			This function will stretch and skew based on the VBS2 window dimensions. 
			
			@locality

			Locally applied, Locally effected

			@version v3.15

			@param start	- The starting position of the line in VBS2 HUD Coordinates.

			@param end		- The ending position of the line in VBS2 HUD Coordinates.

			@param color	- Color of the Line

			@param name		- A unique string identifier of the drawn HUD line object. 

			@param width	- Width (thickness) of the line.

			@return	Nothing.	

			@example

			@code
			
			VBS2Fusion::Draw::Point2D line_start = { 0.6f, -0.6f};
			VBS2Fusion::Draw::Point2D line_end = {0.9f, -0.9f};
			VBS2Fusion::Draw::ColorRGBA_FD line_color;	
			line_color.r = 0;
			line_color.g = 255;
			line_color.b = 255;
			line_color.a = 1;
			HUDDraw::DrawLine(line_start,line_end,line_color,"Line",5.0f );

			@endcode

			@overloaded
			
			None.

			@related

			@remarks
					
			*/
			static void DrawLine(const Point2D& start, const Point2D& end, const ColorRGBA_FD& color,  const std::string& name, float width = 1.0f);

			/*!
			
			@description

			Used to draw a rectangle of given "color" from "leftBottom" point with the given "height" and "width".
			A unique name must provided for the rectangle to be drawn.
			The "height" and "width" are considered as ratios to VBS2 window dimensions
			The centre point of VBS2 screen is considered as (0,0),left-bottom as (-1,-1) and top-right as (1,1),thus the 
			range of X and Y axes varies from (-1) to 1.
			This function will stretch and skew based on the VBS2 window dimensions.

			@locality

			Locally applied, Locally effected

			@version v3.15

			@param leftBottom	- The left bottom corner of the rectangle in VBS2 HUD Coordinates.

			@param height		- The height of the rectangle.

			@param width		- The width of the rectangle.

			@param color		- Color of the rectangle

			@param name			- A unique string identifier of the drawn HUD rectangle object. 

			@param lineWidth	- Width (thickness) of the line.

			@return Nothing.

			@example

			@code

			VBS2Fusion::Draw::Point2D rectangle_left_b = { -0.8f, -0.8f};
			VBS2Fusion::Draw::ColorRGBA_FD rectangle_color;	
			rectangle_color.r = 128;
			rectangle_color.g = 0;
			rectangle_color.b = 128;
			rectangle_color.a = 1;
			HUDDraw::DrawRectangle(rectangle_left_b,0.2,0.5,rectangle_color,"Rectangle",5.0f );

			@endcode

			@overloaded

			None.

			@related

			@remarks

			*/
			static void DrawRectangle(const Point2D& leftBottom, float height, float width, const ColorRGBA_FD& color, const std::string& name, float lineWidth = 1.0f);

			/*!
			@description 
			Draw a filled rectangle of given "color" from "leftBottom" point with the given "height" and "width".
			"height" and "width" are considered as a ratios to VBS2 window dimensions.
			Thus drawn rectangle will stretch and skew based on the VBS2 window dimensions.
			Given "name" should be unique, unless the rectangle will not draw.
			The centre point of VBS2 screen is considered as (0,0),
			left-bottom as (-1,-1) and top-right as (1,1).
			Thus range of X and Y axes varies from (-1) to 1.	
			NOTE: The scale of the object may not be accurate in the mission selection screen.

			@locality

			Locally applied, Locally effected

			@version v3.15

			@param leftBottom	- Left bottom position of the rectangle to be drawn in VBS2 HUD Coordinates.
			
			@param height		- Height of the rectangle in VBS2 HUD Units
			
			@param width		- Width of the rectangle in VBS2 HUD Units
			
			@param color		- Color of the rectangle
			
			@param name			- A unique string identifier of the drawn HUD object. 
	
			@return Nothing.

			@example

			@code

			Point2D leftbottom;
			leftbottom.x=0;
			leftbottom.y=0;

			ColorRGBA_FD color;
			color.r = 0;
			color.g = 1;
			color.b = 1;
			color.a = 0.8; 

			HUDDraw::DrawFilledRectangle(leftbottom, 0.5,0.1,color, string("RECT1"));

			@endcode

			@overloaded

			@related

			@remarks
			*/

			static void DrawFilledRectangle(const Point2D& leftBottom, float height, float width, const ColorRGBA_FD& color, const std::string& name);


			/*!
			
			@description

			Used to draw a circle of given "color" around the "centre" with the given "radius".
			The "radius" is the initial radius (in pixels) for the drawn circle. 
			When "keepShape" is false the circle that has been drawn will reshape when the VBS2 window is resized and otherwise.
			(i.e. If keepShape parameter is set to false, once the circle is drawn, it will stretch and skew based on the VBS2 window dimensions.)
			A unique name must provided for the circle to be drawn.The centre point of VBS2 screen is considered as (0,0),
			left-bottom as (-1,-1) and top-right as (1,1).thus range of X and Y axes varies from (-1) to 1.
			The lineWidth is the circle line width in pixels.

			@locality

			Locally applied, Locally effected

			@version v3.15

			@param centre		- The center point of the circle in VBS2 HUD Coordinates.

			@param radius		- The radius of the circle.

			@param color		- Color of the circle.

			@param name			- A unique string identifier of the drawn HUD circle object. 

			@param keepShape	- If keepShape parameter is false, the circle will reshape when the VBS2 window is resized 

			@param lineWidth	- Width (thickness) of the line.

			@return Nothing.

			@example

			@code

			VBS2Fusion::Draw::Point2D rectangle_left_b = { -0.8f, -0.8f};
			VBS2Fusion::Draw::ColorRGBA_FD rectangle_color;	
			rectangle_color.r = 128;
			rectangle_color.g = 0;
			rectangle_color.b = 128;
			rectangle_color.a = 1;
			HUDDraw::DrawRectangle(rectangle_left_b,0.2,0.5,rectangle_color,"Rectangle",5.0f );

			@endcode

			@overloaded

			None.

			@related

			@remarks

			*/
			static void DrawCircle(const Point2D& centre, float radius, const ColorRGBA_FD& color, const std::string& name, bool keepShape = false, float lineWidth = 1.0f);
			
			/*!
			@description 
			Draw a circle of given "color" around the "centre" with the given "radius".
			"radius" is the initial radius (in pixels) for the drawn circle. 
			Once the circle has been drawn, it will reshape when the VBS2 window is resized. 
			Given "name" should be unique, unless the circle will not draw.
			The centre point of VBS2 screen is considered as (0,0),
			left-bottom as (-1,-1) and top-right as (1,1).
			Thus range of X and Y axes varies from (-1) to 1.
			NOTE: The scale of the object may not be accurate in the mission selection screen.

			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param centre	- Centre of the circle to be drawn.

			@param radius	- Radius of the circle, in pixels

			@param color	- Color of the circle

			@param name		- A unique string identifier of the drawn HUD object. 

	
			@return Nothing.

			@example

			@code

			Point2D center;
			center.x=0;
			center.y=0;

			ColorRGBA_FD color;
			color.r = 0;
			color.g = 1;
			color.b = 1;
			color.a = 0.8; 

			HUDDraw::DrawFilledCircle(center, 40, color, string("CIRCLE1"));

			@endcode

			@overloaded

			@related

			@remarks
			*/

			static void DrawFilledCircle(const Point2D& centre, float radius, const ColorRGBA_FD& color, const std::string& name);

			/*!
			
			@description

			Used to draw an image from "leftBottom" point with the given "height" and "width". 
			The "height" and "width" are considered as ratios to VBS2 window dimensions, thus the drawn image will stretch and 
			skew based on the VBS2 window dimensions.A unique name must provided for the image to be drawn.The centre point of VBS2 screen is considered as (0,0),
			left-bottom as (-1,-1) and top-right as (1,1), thus range of X and Y axes varies from (-1) to 1.
		
			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param leftBottom - Left bottom position of the image to be drawn in VBS2 HUD Coordinates.(i.e starting position of image )

			@param height - Height of the image.

			@param width - Width of the image.

			@param filePath -  The file path to the image.

			@param name - A unique string identifier of the drawn image HUD object. 

			@return Nothing.

			@example

			@code

			VBS2Fusion::Draw::Point2D jd_po = { 0.0f, 0.0f};
			HUDDraw::DrawImage(jd_po,0.75,0.75,"C:\\Users\\kohilan.p\\Pictures\\jd.jpg","JD");	

			@endcode

			@overloaded

			None

			@related

			@remarks 

			*/
			static void DrawImage(const Point2D& leftBottom, float height, float width, const std::string& filePath, const std::string& name);

			/*!
			@description

			Used to draw a text of type "fontType" and color "color" on a horizontal line 
			which is placed "fontSize" pixels below the "start" point and towards the right side of the screen.
			A unique name must provided for the text to be drawn.
			The centre point of VBS2 screen is considered as (0,0),
			left-bottom as (-1,-1) and top-right as (1,1), thus range of X and Y axes varies from (-1) to 1.

			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param start - Start position of the text to be drawn in VBS2 HUD Coordinates.(i.e starting position of text on the screen)

			@param strText - The content of the text object.

			@param fontType - Font type of the text. 

			@param fontSize - Font size of the text. 

			@param color - Color of the text. 

			@param name - A unique string identifier of the drawn text HUD object. 

			@return Nothing.

			@example

			@code

			displayString ="HUD EXAMPLE";
			VBS2Fusion::Draw::Point2D text_start = { -0.1f, 0.9f};
			VBS2Fusion::Draw::ColorRGBA_FD text_color;	
			text_color.r = 0;
			text_color.g = 0;
			text_color.b = 1;
			text_color.a = 1;
			HUDDraw::DrawTextObject(text_start, displayString, "Arial", 20, text_color, "Text" );	

			@endcode

			@overloaded

			None.

			@related

			@remarks - Do not use the starting point of x coordinate as 1 and y as -1. It wont show any text on the screen.

			*/
			static void DrawTextObject(const Point2D& start, const std::string& strText, const std::string& fontType, int fontSize, const ColorRGBA_FD& color, const std::string& name);

			/*!
			@description

			Deletes the HUD object by the given identifier name.
			
			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param name - Name of the HUD object to be deleted.

			@return Nothing.

			@example

			@code

			HUDDraw::DeleteObject("Circle");

			@endcode

			@overloaded

			None.

			@related

			@remarks 
	
			*/
			static void DeleteObject(const std::string& name);

			/*!
			@description

			Used to change color of a previously drawn HUD object.

			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param name - Name of the HUD object for which the color is changed.

			@param color - Color to be changed. 

			@return Boolean.

			@example

			@code

			ColorRGBA_FD change_col1 = { 0.0, 128, 0.0, 0.90f};
			bool change1;
			change1 = HUDDraw::ChangeObjectColor("Text",change_col1);

			@endcode

			@overloaded

			None.

			@related

			@remarks 

			*/
			static bool ChangeObjectColor(const std::string& name, const ColorRGBA_FD& color);

			/*!
			@description

			Used to translate the previously drawn HUD objects by the given offset position. The HUD object will be identified 
			through the name.

			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param name - Name of the HUD object to be  translated.

			@param offset - The point in VBS2 HUD Coordinates to which the HUD object is translated to.

			@return Boolean.

			@example

			@code

			VBS2Fusion::Draw::Point2D rect_rotate_to = {0.3f,0.0f};
			HUDDraw::TransformObject("Rectangle",rect_rotate_to);

			@endcode

			@overloaded

			None.

			@related

			@remarks 

			*/
			static bool TransformObject(const std::string& name, const Point2D& offset);

			/*!
			@description

			Used to rotate a drawn HUD Objects by an angle in degrees. The angles are measured 
			counter clockwise.

			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param name - Name of the HUD object to be rotated.
			
			@param origin - The origin of VBS HUD Coordinates to which the HUD object is moved to. 

			@param angle - The angle in which the HUD object is rotated. 

			@return Boolean.

			@example

			@code

			VBS2Fusion::Draw::Point2D rotation_origin = {0.0f,0.0f};
			HUDDraw::RotateObject("Rectangle",rotation_origin,90);

			@endcode

			@overloaded

			None.

			@related

			@remarks 

			*/
			static bool RotateObject(const std::string& name, const Point2D& origin, float angle);

			/*!
			@description
			Used to scale a previously  drawn Object by the scaleFactor.  
			The Scaling origin is the top left of the HUD Object.

			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param name - Name of the HUD object to be scaled.

			@param scaleFactor - The scaling factor.

			@return Boolean.

			@example

			@code

			float rect_scale_to = 0.45f;
			HUDDraw::ScaleObject("Rectangle",rect_scale_to);

			@endcode

			@overloaded

			None.

			@related

			@remarks 
			*/
			static bool ScaleObject(const std::string& name, float scaleFactor);

			/*!
			@description

			Used to change the position of a HUD object to a given position.
			
			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param name - Name of the HUD object for which the position is changed.

			@param position - The point in VBS2 HUD Coordinates to which the HUD object is moved to.

			@return Boolean.

			@example

			@code

			VBS2Fusion::Draw::Point2D text_down = { -0.1f, -0.55f};

			HUDDraw::SetPosition("Text",text_down);

			@endcode

			@overloaded

			None.

			@related

			@remarks 

			*/
			static bool SetPosition(const std::string& name, const Point2D& position);

			/*!
			@description

			Used to change the text of the  previously drawn HUD_Text object.The HUD_Text object will be 
			identified through the name.
			
			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param name -Name of the HUD Text object for which the text is changed.

			@param strText - The new text to be changed (displayed by the text object)

			@return Boolean.

			@example

			@code

			HUDDraw::ChangeTextObjectString("string", displayString);

			@endcode

			@overloaded

			None.

			@related

			@remarks 

			
			*/
			static bool ChangeTextObjectString(const std::string& name, const std::string& strText);

			/*!
			@description
			
			Used to change the font type of the  previously drawn HUD_Text object. 
			The HUD_Text object will be identified through the name.

			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param name -Name of the HUD Text object for which the font type is changed.

			@param fontType - The new font type to be changed (displayed by the text object)

			@return Boolean.

			@example

			@code

			HUDDraw::ChangeTextObjectFontType("Text","Lucida Handwriting");

			@endcode

			@overloaded

			None.

			@related

			@remarks 

			*/
			static bool ChangeTextObjectFontType(const std::string& name, const std::string& fontType);

			/*!
			@description

			Used to change the font size of the previously drawn HUD_Text object.
			The HUD_Text object will be identified through the name.
			
			@locality

			Locally applied, Locally effected

			@version [VBS2Fusion v3.15]

			@param name -Name of the HUD Text object for which the font size is changed.

			@param fontSize - The new font size to be changed

			@return Boolean.

			@example

			@code

			HUDDraw::ChangeTextObjectFontSize("Text",15);

			@endcode

			@overloaded

			None.

			@related

			@remarks 

			*/
			static bool ChangeTextObjectFontSize(const std::string& name, int fontSize);

		};
	}
}


#endif //VBS2FUSION_DRAW_PRIMITIVEDRAW_H