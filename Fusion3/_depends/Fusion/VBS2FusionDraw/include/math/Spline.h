
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:

Purpose:

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			04-10/2011	NDB: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_SPLINE_H
#define VBS2FUSION_SPLINE_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <vector>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "math/Vector3f.h"
#include "position3D.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/


	class spline
	{
	public:
		/*!
		Main constructor for this class.
		*/
		spline();

		/*!
		Main destructor for this class. 
		*/
		~spline();

		/*!
		Main constructor for this class.
		- It creates spline with given values
		Atleast 4 points should be given to construct spline
		*/
		spline(std::vector<VBSFusion::Vector3f> points);

		/*!
		Copy constructor for the spline class. 
		*/
		spline(const spline &spl);

		/*!
		set 3d points for this spline
		Atleast 4 points should be given
		*/
		void set3DControlPoints(std::vector<VBSFusion::Vector3f> points);

		/*!
		returns 3d points of the spline
		*/
		std::vector<VBSFusion::Vector3f> get3DControlPoints() const;

		/*!
		returns 3D point in the curve for given x value
		*/
		VBSFusion::Vector3f get3DPointInLine(float x);

	private:
		/*!
		Calculate and returns the 3d line equation.
		y = A1x^3+B1x^2+C1x+D1 , z = A2x^3+B2x^2+C2x+D2
		returns values are <<A1,B1,C1,D1>,<A2,B2,C2,D2>>
		So it is return as vector<vector<float>>
		*/
		//vector<vector<float>> get3DLineEquation();

		//void Calculate3DLineEquation();

		/*!
		sorting in ascending order of value x
		*/
		void Sort3DControlPoints();

	private:
		//vector<Vector3f> _points3D;
		VBSFusion::Vector3f* _ArrayPoints3D;
		int _arraySize;
		/*!
		y = A1x^3+B1x^2+C1x+D1 , z = A2x^3+B2x^2+C2x+D2
		<<A1,B1,C1,D1>,<A2,B2,C2,D2>>
		*/
		//float A1,B1,C1,D1;
		//float A2,B2,C2,D2;
	};



#endif