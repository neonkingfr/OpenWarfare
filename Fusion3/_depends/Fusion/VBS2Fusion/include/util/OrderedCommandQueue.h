/*
* Copyright 2010 SimCentric Technologies, Pty. Ltd.  All Rights Reserved.
*
* Permission to use, copy, modify, and distribute this software in object
* code form for any purpose and without fee is hereby granted, provided
* that the above copyright notice appears in all copies and that both
* that copyright notice and the limited warranty and restricted rights
* notice below appear in all supporting documentation.
*
* SIMCENTRIC PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
* SIMCENTRIC SPECIFICALLY DISCLAIMS ANY AND ALL WARRANTIES, WHETHER EXPRESS
* OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTY
* OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE OR NON-INFRINGEMENT
* OF THIRD PARTY RIGHTS.  SIMCENTRIC DOES NOT WARRANT THAT THE OPERATION
* OF THE PROGRAM WILL BE UNINTERRUPTED OR ERROR FREE.
*
* In no event shall SimCentric Technologies, Pty. Ltd. be liable for any direct, indirect,
* incidental, special, exemplary, or consequential damages (including,
* but not limited to, procurement of substitute goods or services;
* loss of use, data, or profits; or business interruption) however caused
* and on any theory of liability, whether in contract, strict liability,
* or tort (including negligence or otherwise) arising in any way out
* of such code.
*/

/*************************************************************************

Name:

	OrderedQueue.h

Purpose:

	This file contains the declaration of the OrderedQueue class.
	which contains CommandElements ordered by execution time.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			11-09/2009	SLA: Original Implementation
	1.01        01-06/2011  YFP: Methods Added:
										applyCamDive
										applyCamBank
										applyCamDirection
										applyCamFOVRange
										applyCamFrustum

************************************************************************/

#ifndef ORDEREDQUEUE_H
#define ORDEREDQUEUE_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push) 
#pragma warning (disable: 4251)


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <list>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBSFusion.h"
#include "data/ControllableObject.h"
#include "data/Camera.h"
#include "data/EnvironmentState.h"
#include "util/ControllableObjectUtilities.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{
	enum CAMCONDITION{CAMCONDITION_COMMITTED, CAMCONDITION_TIMESINCE, CAMCONDITION_GLOBALTIME, CAMCONDITION_NOCONDITION};

	typedef void (*type1Function)(Camera&);
	typedef void (*type2Function)(Camera&, bool);
	typedef void (*type3Function)(Camera&, position3D);
	typedef void (*type4Function)(Camera&, std::string);
	typedef void (*type5Function)(Camera&, CAMEFFECTTYPE, CAMEFFECTPOS);
	typedef void (*type6Function)(Camera&, double);
	typedef void (*type7Function)(Camera&, double, double);	
	typedef void (*type8Function)(Camera&, position3D, double, double);
	typedef void (*type9Function)(Camera&, ControllableObject&);
	typedef void (*type10Function)(ControllableObject&, ControllableObject&, position3D);
	typedef void (*type11Function)(ControllableObject&, ControllableObject&, position3D, std::string);
	typedef void (*type12Function)(ControllableObject&, CAMMODETYPE);
	typedef void (*type13Function)(double);
	typedef	void (*type14Function)(ControllableObject&);


	typedef bool		(*type15Function)(Camera&);
	typedef position3D	(*type16Function)(Camera&);
	typedef double		(*type17Function)(Camera&);
	typedef void		(*type18Function)(ControllableObject& , double);

	typedef NetworkID	(*type19Function)(void);
	typedef double		(*type20Function)(void);
	typedef std::vector<float>	(*type21Function)(Camera&); 
	typedef void	(*type22Function)(Camera& , float ,float);
	typedef bool	(*type23Function)(bool,float,float,float,float);
	typedef void	(*type24Function)(double);
	typedef void	(*type25Function)(Camera&, CAMEFFECTMODE);
	typedef bool    (*type26Function)(bool,double,double,double,double);


	

	struct CommandQueueItem
	{
		//CamUtilityTypes _camUtilType;
		type1Function _type1Func;
		type2Function _type2Func;
		type3Function _type3Func;
		type4Function _type4Func;
		type5Function _type5Func;
		type6Function _type6Func;
		type7Function _type7Func;
		type8Function _type8Func;
		type9Function _type9Func;
		type10Function _type10Func;
		type11Function _type11Func;
		type12Function _type12Func;
		type13Function _type13Func;
		type14Function _type14Func;
		type15Function _type15Func;
		type16Function _type16Func;
		type17Function _type17Func;
		type18Function _type18Func;
		type19Function _type19Func;
		type20Function _type20Func;
		type21Function _type21Func;
		type22Function _type22Func;
		type23Function _type23Func;
		type24Function _type24Func;
		type25Function _type25Func;
		type26Function _type26Func;

		std::string _sCamAlias;
		std::string _scoAlias;
		bool _bValue;
		position3D _position3DVal;
		std::string _sVal1;
		//string _sVal2;
		double _dVal1;
		double _dVal2;
		double _dVal3;
		double _dVal4;
		CAMCONDITION _camCondition;
		int _dtimeToExecute;
		int _parameterType;
		CAMEFFECTTYPE _camEffectType;
		CAMEFFECTPOS _camEffectPos;
		CAMMODETYPE _camModeType;

		float _fval1;
		float _fval2;
		float _fval3;
		float _fval4;

		CAMEFFECTMODE _camEffectMode;
	};	

	class VBSFUSION_API OrderedCommandQueue
	{
	public:	

		OrderedCommandQueue();

		~OrderedCommandQueue();

		/*!
		Typedef for a list containing command queue items. 
		*/
		typedef std::list<CommandQueueItem> CommandQueueList;

		/*!
		Typedef for an iterator to a list containing command queue items. 
		*/
		typedef std::list<CommandQueueItem>::iterator commandQueueListIterator;

		/*!
		Typedef for a const iterator to a list containing command queue items. 
		*/
		typedef std::list<CommandQueueItem>::const_iterator commandQueueListConst_Iterator;

		//********************************************************

		/*!
		Begin iterator to browse through the list of commands in CommandQueue list.
		*/
		commandQueueListIterator commands_begin();

		/*!
		Begin const iterator to browse through the list of commands in CommandQueue.
		*/
		commandQueueListConst_Iterator commands_begin() const;

		/*!
		End iterator to browse through the list of commands in CommandQueue. 
		*/
		commandQueueListIterator commands_end();

		/*!
		End const iterator to browse through the list of commands in CommandQueue. 
		*/
		commandQueueListConst_Iterator commands_end() const;	

		//********************************************************

		/*!
		Typedef for a list containing command queue items. 
		*/
		typedef std::list<CommandQueueItem> GlobalTimeCommandQueue;

		/*!
		Typedef for an iterator to a list containing command queue items. 
		*/
		typedef std::list<CommandQueueItem>::iterator g_t_commandQueueIterator;

		/*!
		Typedef for a const iterator to a list containing command queue items. 
		*/
		typedef std::list<CommandQueueItem>::const_iterator g_t_commandQueueConst_Iterator;

		//********************************************************

		/*!
		Begin iterator to browse through the list of commands in CommandQueue list.
		*/
		g_t_commandQueueIterator g_t_commands_begin();

		/*!
		Begin const iterator to browse through the list of commands in CommandQueue.
		*/
		g_t_commandQueueConst_Iterator g_t_commands_begin() const;

		/*!
		End iterator to browse through the list of commands in CommandQueue. 
		*/
		g_t_commandQueueIterator g_t_commands_end();

		/*!
		End const iterator to browse through the list of commands in CommandQueue. 
		*/
		g_t_commandQueueConst_Iterator g_t_commands_end() const;	

		//********************************************************



		/*!
		Typedef for a list containing pointers to Camera objects. 
		*/
		typedef std::list<Camera*> CList;

		/*!
		Typedef for an iterator to a list containing pointers to Camera objects. 
		*/
		typedef std::list<Camera*>::iterator c_iterator;

		/*!
		Typedef for a const iterator to a list containing pointers to Camera objects. 
		*/
		typedef std::list<Camera*>::const_iterator c_const_iterator;

		//********************************************************

		/*!
		Begin iterator to browse through the list of Camera pointers which 
		are added automatically to the object everytime call OrderedCommandQueue. 
		*/
		c_iterator c_begin();

		/*!
		Begin const iterator to browse through the list of Camera pointers which 
		are added automatically to the object everytime call OrderedCommandQueue.
		*/
		c_const_iterator c_begin() const;

		/*!
		End iterator to browse through the list of Camera pointers which 
		are added automatically to the object everytime call OrderedCommandQueue. 
		*/
		c_iterator c_end();

		/*!
		End const iterator to browse through the list of Camera pointers which 
		are added automatically to the object everytime call OrderedCommandQueue.
		*/
		c_const_iterator c_end() const;		

		//********************************************************

		/*!
		Typedef for a list containing pointers to Controllable objects. 
		*/
		typedef std::list<ControllableObject*> COList;

		/*!
		Typedef for an iterator to a list containing pointers to Controllable objects. 
		*/
		typedef std::list<ControllableObject*>::iterator co_iterator;

		/*!
		Typedef for a const iterator to a list containing pointers to Controllable objects. 
		*/
		typedef std::list<ControllableObject*>::const_iterator co_const_iterator;;

		//********************************************************

		/*!
		Begin iterator to browse through the list of Controllable Object pointers which 
		are added automatically to the controllable objects list when ever store in OrderedCommandQueue. 
		*/
		co_iterator co_begin();

		/*!
		Begin const iterator to browse through the list of Controllable Object pointers which 
		are added automatically to the controllable objects list when ever store in OrderedCommandQueue. 
		*/
		co_const_iterator co_begin() const;

		/*!
		End iterator to browse through the list of Controllable Object pointers which 
		are added automatically to the controllable objects list when ever store in OrderedCommandQueue. 
		*/
		co_iterator co_end();

		/*!
		End const iterator to browse through the list of Controllable Object pointers which 
		are added automatically to the controllable objects list when ever store in OrderedCommandQueue. 
		*/
		co_const_iterator co_end() const;		
		
		/*!
		Applies the effect type and effect position, specified by user to the camera.
		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		*/
		void applyCamEffect(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, int delayTime, CAMCONDITION condition);

		/*!
		Applies the target position specified in camTarget to the camera.

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		*/
		void applyCamTarget(Camera& c, const position3D& camTarget, int delayTime, CAMCONDITION condition);

		/*!
		Applies the target position specified in camTargetCO object position to the camera. 

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		*/
		void applyCamTarget(Camera& c, ControllableObject& camTargetCO, int delayTime, CAMCONDITION condition);

		/*!
		Applies the camera position specified in camPos to the camera. 

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		*/
		void applyCamPos(Camera& c, const position3D& camPos, int delayTime, CAMCONDITION condition);

		/*!
		Applies the camera relative position specified in camRelPos to the camera. 

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		*/
		void applyCamRelPos(Camera& c, const position3D& camRelPos, int delayTime, CAMCONDITION condition);

		/*!
		Applies the camera commit time specified in camCommit to the camera. 

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		*/
		void applyCamCommit(Camera& c, double camCommit, int delayTime, CAMCONDITION condition);

		/*!
		Applies the camera mode specified in camMode to the camera/Object. 

		Error checking utility validates the following:

		- ObjectAlias (outputs an error if the object alias is invalid)

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		*/
		void applyCamMode(ControllableObject& co, CAMMODETYPE camMode, int delayTime, CAMCONDITION condition);

		/*!

		Applies the camera focus specified in camFocusDist and camFocusBlur to the camera. 

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		*/
		void applyCamFocus(Camera& c, double camFocusDist, double camFocusBlur, int delayTime, CAMCONDITION condition);

		/*!
		Applies the camera Fov specified in camFov to the camera. 

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		*/
		void applyCamFov(Camera& c, double camFov, int delayTime, CAMCONDITION condition);

		/*!
		Destroy the camera from the scene. 

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		Deprecated.Use void applyDestroyCamera(Camera& c, int delayTime, CAMCONDITION condition)
		*/
		VBSFUSION_DPR(UOCQ004) void destroyCamera(Camera& c, int delayTime, CAMCONDITION condition);

		/*!
		Executes a command on the given camera / actor object.
		The "manual on" and "manual off" commands are recognized for all types.
		For the "camera" type, the following commands can be used: "inertia on" and "inertia off".
		For the "seagull" type it's one of: "landed" and "airborne" these control if they land or fly. 
		When you execute camCommand "landed" on a flying seagull, it will land and sit on the floor until you call camCommand "airborne". 
		The camCommand changes are conducted immediately, the command doesn't wait for camCommit.

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		Deprecated.Use void applyCamCommand(Camera& c, string camCommand, int delayTime, CAMCONDITION condition)
		*/
		VBSFUSION_DPR(UOCQ003) void camCommand(Camera& c, std::string camCommand, int delayTime, CAMCONDITION condition);

		/*!
		Attaches an object to another object. The offset is applied to the object, memory point.
		Memory point is optional, it specifies location where the attaching should happen (it's name of the memory point in the model)

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		Deprecated.Use void applyCamAttachTo(Camera& c, ControllableObject& tObject, position3D offSet, string memoryPoint, int delayTime, CAMCONDITION condition)
		*/
		VBSFUSION_DPR(UOCQ002) void camAttachTo(Camera& object, ControllableObject& tObject, position3D offSet, std::string memoryPoint, int delayTime, CAMCONDITION condition);

		/*!
		Attaches an object to another object. The offset is applied to the object center. 

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		Deprecated.Use void applyCamAttachTo(Camera& c, ControllableObject& tObject, position3D offSet, int delayTime, CAMCONDITION condition)
		*/
		VBSFUSION_DPR(UOCQ001) void camAttachTo(Camera& object, ControllableObject& tObject, position3D offSet, int delayTime, CAMCONDITION condition);

		/*!
		  Set time acceleration coefficient. May be also used to slow time in cutscenes. 
		  This command does NOT work in multiplayer.

		  delay time can be used to make some delay to this command relative to the last executed command.
		  or delay time can be used as the actual time in VBS to execute the command.

		  condition is used to check whether this command should be executed after relevant check.
		  condition can be one of following.
		  NOCONDITION - State no condition so command will be executed immediately.
		  CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
			  if not, this command will be waited until commit is applied.
		  TIMESINCE - State to make a given delay to execute command since last executed command.
		  GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		  Deprecated.Use void applyAccTime(double accFactor, int delayTime, CAMCONDITION condition)
		*/
		VBSFUSION_DPR(UOCQ007) void setAccTime(double accFactor, int delayTime, CAMCONDITION condition);
		
		/*!
		Detaches an object was being attached by another object.

		delay time can be used to make some delay to this command relative to the last executed command.
		or delay time can be used as the actual time in VBS to execute the command.

		condition is used to check whether this command should be executed after relevant check.
		condition can be one of following.
		NOCONDITION - State no condition so command will be executed immediately.
		CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
						if not, this command will be waited until commit is applied.
		TIMESINCE - State to make a given delay to execute command since last executed command.
		GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		Deprecated.Use void applyObjectDetach(ControllableObject& co, int delayTime, CAMCONDITION condition)
		*/
		VBSFUSION_DPR(UOCQ005) void detachObject(ControllableObject& c, int delayTime, CAMCONDITION condition);

		/*!
		Start the processing of created OrderedCommandQueue object.
		This should be called from onSimulation step and deltaT value should be derive from the deltaT value from onSimulation step.

		Deprecated.Use void applyProcessQueueCommands(float deltaT)
		*/
		VBSFUSION_DPR(UOCQ006) void processQueueCommands(float deltaT);

		/*!
		@description
		Attaches camera object to a target controllable object.

		@locality

		Globally applied, Locally effected.

		@version [VBSFusion v3.20]

		@param c  -     A reference to a currently implemented camera object.

		@param tObject -    Target controllable object that camera will attach.

		@param offSet -     Offset is applied to the object center.Set relative position with respect to the object.

		@param delayTime -  Delay time can be used to make some delay to this command relative to the last executed command.
							or delay time can be used as the actual time in VBS to execute the command.

		@param condition -  Condition is used to check whether this command should be executed after relevant check.
							condition can be one of following.
							NOCONDITION - State no condition so command will be executed immediately.
							CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
							if not, this command will be waited until commit is applied.
							TIMESINCE - State to make a given delay to execute command since last executed command.
							GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		@return Nothing.

		@example

		@code

		// Defines a Ordered Command Queue Object.
		OrderedCommandQueue* testQueue;

		// Initialize the testQueue.
		testQueue = new OrderedCommandQueue();

		// Attach Camera to Unit. 
		testQueue->applyCamAttachTo(cam,newUnit1,position3D(1,1,1),5,CAMCONDITION::CAMCONDITION_NOCONDITION);

		// Apply Effect for Camera. 
		CameraUtilities::applyCamEffect(cam, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTPOS_TOP);

		@endcode

		@overloaded 
		
		applyCamAttachTo(Camera& c, ControllableObject& tObject, position3D offSet, string memoryPoint, int delayTime, CAMCONDITION condition)

		@related

		@remarks This is a replication of void camAttachTo(Camera& object, ControllableObject& tObject, position3D offSet, int delayTime, CAMCONDITION condition)

		*/
		void applyCamAttachTo(Camera& c, ControllableObject& tObject, const position3D& offSet, int delayTime, CAMCONDITION condition);

		/*!
		@description
		Attaches camera object to a target controllable object.

		@locality

		Globally applied, Locally effected.

		@version [VBSFusion v3.15]

		@param c  -     A reference to a currently implemented camera object.

		@param tObject -    Target controllable object that camera will attach.

		@param offSet -     Offset is applied to the object center.Set relative position with respect to the object.

		@param memoryPoint - Memory point is optional, it specifies location where the attaching should happen (it's name of the memory point in the model).
		                     Memory point of the controllable object.

		@param delayTime -  Delay time can be used to make some delay to this command relative to the last executed command.
							or delay time can be used as the actual time in VBS to execute the command.

		@param condition -  Condition is used to check whether this command should be executed after relevant check.
							condition can be one of following.
							NOCONDITION - State no condition so command will be executed immediately.
							CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
							if not, this command will be waited until commit is applied.
							TIMESINCE - State to make a given delay to execute command since last executed command.
							GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		@return Nothing.

		@example

		@code

		// Defines a Ordered Command Queue Object.
		OrderedCommandQueue* testQueue;

		// Initialize the testQueue.
		testQueue = new OrderedCommandQueue();

		// Attach Camera to Unit. 
		testQueue->applyCamAttachTo(cam,newUnit1,position3D(1,1,1),string("Head"),5,CAMCONDITION::CAMCONDITION_NOCONDITION);

		// Apply Effect for Camera. 
		CameraUtilities::applyCamEffect(cam, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTPOS_TOP);

		@endcode

		@overloaded void applyCamAttachTo(Camera& c, ControllableObject& tObject, position3D offSet, int delayTime, CAMCONDITION condition)

		@related

		@remarks This is a replication of void camAttachTo(Camera& c, ControllableObject& tObject, position3D offSet, string memoryPoint, int delayTime, CAMCONDITION condition).
		void applyCamEffect(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, int delayTime, CAMCONDITION condition) function should be implemented successively in order to achieve proper perspective view.
		*/
		void applyCamAttachTo(Camera& c, ControllableObject& tObject, const position3D& offSet, const std::string& memoryPoint, int delayTime, CAMCONDITION condition);

		/*!
		@description
		Executes a command on the given camera / object.

		@locality

		Globally applied, Locally effected.

		@version [VBSFusion v3.15]

		@param c  -     A reference to a currently implemented camera object.

		@param camCommand -   The "manual on" and "manual off" commands are recognized for all types.
							  For the "camera" type, the following commands can be used: "inertia on" and "inertia off".
							  For the "seagull" type it's one of: "landed" and "airborne" these control if they land or fly. 
							  When you execute camCommand "landed" on a flying seagull, it will land and sit on the floor until you call camCommand "airborne". 
							  The camCommand changes are conducted immediately, the command doesn't wait for camCommit.

		@param delayTime -  Delay time can be used to make some delay to this command relative to the last executed command.
							or delay time can be used as the actual time in VBS to execute the command.

		@param condition -  Condition is used to check whether this command should be executed after relevant check.
							condition can be one of following.
							NOCONDITION - State no condition so command will be executed immediately.
							CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
							if not, this command will be waited until commit is applied.
							TIMESINCE - State to make a given delay to execute command since last executed command.
							GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		@return Nothing.

		@example

		@code

		testQueue->applyCamCommand(cam,"manual on",2,CAMCONDITION::CAMCONDITION_NOCONDITION);

		@endcode

		@overloaded

		None.

		@related

		@remarks This is a replication of void camCommand(Camera& c, string camCommand, int delayTime, CAMCONDITION condition)

		*/
		void applyCamCommand(Camera& c, const std::string& camCommand, int delayTime, CAMCONDITION condition);
		
		/*!
		@description
		Destroy the camera from the scene. 
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid).

		@locality

		Globally applied, Locally effected.

		@version [VBSFusion v3.15]

		@param c  -     A reference to a currently implemented camera object.

		@param delayTime -  Delay time can be used to make some delay to this command relative to the last executed command.
							or delay time can be used as the actual time in VBS to execute the command.

		@param condition -  Condition is used to check whether this command should be executed after relevant check.
							condition can be one of following.
							NOCONDITION - State no condition so command will be executed immediately.
							CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
							if not, this command will be waited until commit is applied.
							TIMESINCE - State to make a given delay to execute command since last executed command.
							GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		@return Nothing.

		@example

		@code

		testQueue->applyDestroyCamera(cam,2,CAMCONDITION::CAMCONDITION_NOCONDITION);

		@endcode

		@overloaded 

		@related

		@remarks This is a replication of void destroyCamera(Camera& c, int delayTime, CAMCONDITION condition)

		*/
		void applyDestroyCamera(Camera& c, int delayTime, CAMCONDITION condition);

		/*!
		@description
		Detaches an object was being attached by another object

		@locality

		Globally applied, Locally effected.

		@version [VBSFusion v3.10]

		@param co  -     A reference to the current object that will be detached.

		@param delayTime -  Delay time can be used to make some delay to this command relative to the last executed command.
							or delay time can be used as the actual time in VBS to execute the command.

		@param condition -  Condition is used to check whether this command should be executed after relevant check.
							condition can be one of following.
							NOCONDITION - State no condition so command will be executed immediately.
							CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
							if not, this command will be waited until commit is applied.
							TIMESINCE - State to make a given delay to execute command since last executed command.
							GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		@return Nothing.

		@example

		@code

		testQueue->applyObjectDetach(cam,2,CAMCONDITION::CAMCONDITION_TIMESINCE);

		@endcode

		@overloaded 

		@related

		@remarks This is a replication of void detachObject(ControllableObject& c, int delayTime, CAMCONDITION condition)

		*/
		void applyObjectDetach(ControllableObject& co, int delayTime, CAMCONDITION condition);

		/*!
		@description
		Start the processing of created OrderedCommandQueue object.
		This should be called from onSimulation step and deltaT value should be derive from the deltaT value from onSimulation step.

		@locality

		Locally applied.

		@version [VBSFusion v3.20]

		@param deltaT  -  Current Simulation Time(Total expired time since simulation start)

		@return Nothing.

		@example

		@code

		@endcode

		testQueue->applyProcessQueueCommands(deltaT);

		@overloaded 

		@related

		@remarks This is a replication of void processQueueCommands(float deltaT)

		*/
		void applyProcessQueueCommands(float deltaT);

		/*!
		@description
		Set time acceleration coefficient. May be also used to slow time in cut scenes. 
		This command does NOT work in multi player.

		@locality

		Locally applied.

		@version [VBSFusion v3.10]

		@param accFactor - Current time acceleration factor.

		@param delayTime -  Delay time can be used to make some delay to this command relative to the last executed command.
							or delay time can be used as the actual time in VBS to execute the command.

		@param condition -  Condition is used to check whether this command should be executed after relevant check.
							condition can be one of following.
							NOCONDITION - State no condition so command will be executed immediately.
							CAMCOMMITTED - check whether last commit has been applied if so command will be executed immediately.
							if not, this command will be waited until commit is applied.
							TIMESINCE - State to make a given delay to execute command since last executed command.
							GLOBALTIME - State to wait until VBS time to become given time. At that time this command will be executed.

		@return Nothing.

		@example

		@code

		testQueue->applyAccTime(0.8,2,CAMCONDITION::CAMCONDITION_TIMESINCE);

		@endcode

		@overloaded 

		@related

		@remarks This is a replication of void setAccTime(double accFactor, int delayTime, CAMCONDITION condition)

		*/
		void applyAccTime(double accFactor, int delayTime, CAMCONDITION condition);

	private:
		CommandQueueList _commandQueueList;
		GlobalTimeCommandQueue _g_timeQueueList;
		CList _cameraList;
		COList _coList;

		float _timeSinceLastExecution;
		Camera* lastExecutedCam;

		//VBSTime _vbsCurrentTime;
		Time _vbsCurrentTime;

	private:
		ControllableObject* getControllableObject(const std::string& coAlias);

		void addControllableObject(ControllableObject& co);

		Camera* getCamera(const std::string& camAlias);
		
		void addCamera(Camera& c);

		

		//*******************************************************************
		//CommandQueueItem front();

		//CommandQueueItem pop();

		void pushInOrder(CommandQueueItem& commandElement);

		int noOfCommandElements();

		//*******************************************************************

		double getTimeSince();

		void resetTimeSince();

		void setTimeSince(double deltaT);

		//*******************************************************************
		void updateVBSstate();

		void setVBSCurrentTime(EnvironmentState& status);

		Time getVBSCurrentTime();
		
		//bool m_dirty;
	};
}

#pragma warning(pop) // Enable warnings [C:4251]

#endif //COMMANDELEMENT_H