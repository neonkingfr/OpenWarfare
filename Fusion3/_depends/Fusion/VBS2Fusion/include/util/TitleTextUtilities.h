/**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	TitleTextUtilities.h

Purpose: This file contains Text Related function

/*****************************************************************************/
#ifndef VBS2FUSION_TITLETEXTUTILITIES_H
#define VBS2FUSION_TITLETEXTUTILITIES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <string>
#include <vector>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "data/ControllableObject.h"
#include "data/Group.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion{

	class VBSFUSION_API TitleTextUtilities
	{
	public:

		/*!
		@description 

		Prolongs the active title effect.
		
		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.15]

		@param time - Extended time in seconds.

		@return Nothing.

		@example

		@code

		TitleTextUtilities::applyTitleEffectProlong(100.2);

		@endcode

		@overloaded  
		
		None
		
		@related

		None

		@remarks This is a replication of void prolongTitleEffect(double time) function.
		*/
		static void applyTitleEffectProlong(const double time);

		/*!
		@description 

		Displays a text message in the center of the screen.
		
		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.15]

		@param text - The text which is needs to appear.
		@param type - Types of effects (PLAIN, PLAIN_DOWN, BLACK, BLACK_FADED, BLACK_OUT, BLACK_IN, WHITE_OUT, WHITE_IN ).
		@param speed - Time in seconds to fade in resource.

		@return Nothing.

		@example

		@code

		TitleTextUtilities::applyCutText(string("Hello WOrld"),TITLE_EFFECT_TYPE::PLAIN, 2);

		@endcode

		@overloaded  
		
		void applyCutText(const double layer, string& text, const TITLE_EFFECT_TYPE type, const double speed = 1)
		
		@related

		None

		@remarks This is a replication of void cutText(string text,TITLE_EFFECT_TYPE type,double speed)
		*/
		static void applyCutText(const std::string& text, const TITLE_EFFECT_TYPE type, const double speed = 1);

		/*!
		@description 

		Displays a text message in the center of the screen with additional parameter layer
		
		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.15]

		@param layer - Defines the layer in which the effect is shown, where 0 is the back most one. 
		@param text - The text which is needs to appear.
		@param type - Types of effects (PLAIN, PLAIN_DOWN, BLACK, BLACK_FADED, BLACK_OUT, BLACK_IN, WHITE_OUT, WHITE_IN ).
		@param speed - Time in seconds to fade in resource.

		@return Nothing.

		@example

		@code

		TitleTextUtilities::applyCutText(2.2,string("Hello WOrld"),TITLE_EFFECT_TYPE::BLACK_OUT, 2);

		@endcode

		@overloaded  
		
		void applyCutText(string& text, const TITLE_EFFECT_TYPE type, const double speed = 1)
		
		@related

		None

		@remarks This is a replication of void cutText(double layer,string text,TITLE_EFFECT_TYPE type,double speed)
		*/
		static void applyCutText(const double layer, std::string& text, const TITLE_EFFECT_TYPE type, const double speed = 1);

		/*!
		@description 

		Display a resource defined in the mission's description.ext, the campaign's 
		description.ext or the global resource.cpp. 
		
		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.15]

		@param resource - The class name of the resource. ("Default" will remove the current resource.)
		@param type - Types of effects (PLAIN, PLAIN_DOWN, BLACK, BLACK_FADED, BLACK_OUT, BLACK_IN, WHITE_OUT, WHITE_IN ).
		@param speed - Time in seconds to fade in resource.

		@return Nothing.

		@example

		@code

		TitleTextUtilities::applyCutRsc(string("binocular"),TITLE_EFFECT_TYPE::BLACK_IN, 5);

		@endcode

		@overloaded  
		
		None
		
		@related

		None

		@remarks This is a replication of void cutRsc(string resource,TITLE_EFFECT_TYPE type,double speed) function.
		*/
		static void applyCutRsc(const std::string& resource, const TITLE_EFFECT_TYPE type, const double speed = 1);

		/*!
		@description 

		Displays text across the screen.If used along with cutText two different texts (in different type styles) can be shown at once.
		
		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.15]

		@param text - The text which is needs to appear.
		@param type - Types of effects (PLAIN, PLAIN_DOWN, BLACK, BLACK_FADED, BLACK_OUT, BLACK_IN, WHITE_OUT, WHITE_IN ).
		@param speed - Time in seconds to fade in resource.

		@return Nothing.

		@example

		@code

		TitleTextUtilities::applyTitleText(string("Hello World"),TITLE_EFFECT_TYPE::BLACK_IN, 5);

		@endcode

		@overloaded  
		
		None
		
		@related

		None

		@remarks This is a replication of void titleText(string text,TITLE_EFFECT_TYPE type,double speed) function.
		*/
		static void applyTitleText(const std::string& text, const TITLE_EFFECT_TYPE type, const double speed = 1);

		/*!
		@description 

		Terminate the effect in the given layer and set duration of the fade out phase to the given time. 
		
		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.15]

		@param layer - Defines the layer in which the effect is shown, where 0 is the back most one. 
		@param delay - Delay time in seconds.

		@return Nothing.

		@example

		@code

		TitleTextUtilities::applyCutFadeOut(1.1,5.1);

		@endcode

		@overloaded  
		
		None
		
		@related

		None

		@remarks This is a replication of void cutFadeOut(double layer,double delay) function.
		*/
		static void applyCutFadeOut(const double layer, const double delay);

		/*!
		@description 

		Resource title - Resource can be defined in Description.ext Also see cutRsc, with these two commands you can show two different resources at once. 
		
		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.15]

		@param resource - The class name of the resource. ("Default" will remove the current resource.)
		@param type - Types of effects (PLAIN, PLAIN_DOWN, BLACK, BLACK_FADED, BLACK_OUT, BLACK_IN, WHITE_OUT, WHITE_IN ).
		@param speed - Time in seconds to fade in resource.

		@return Nothing.

		@example

		@code

		TitleTextUtilities::applyTitleRsc(string("BIS"),TITLE_EFFECT_TYPE::WHITE_OUT, 5);

		@endcode

		@overloaded  
		
		None
		
		@related

		None

		@remarks This is a replication of void titleRsc(string resource, TITLE_EFFECT_TYPE type,double speed) function.
		*/
		static void applyTitleRsc(const std::string& resource, const TITLE_EFFECT_TYPE type, const double speed = 1);

		/*!
		@description 

		Terminate the title effect and set duration of the fade out phase to the given time. 
		
		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.15]

		@param duration - Given time in seconds.
		
		@return Nothing.

		@example

		@code

		TitleTextUtilities::applyTitleFadeOut(2.5);

		@endcode

		@overloaded  
		
		None
		
		@related

		None

		@remarks This is a replication of void titleFadeOut(double duration) function.
		*/
		static void applyTitleFadeOut(const double duration);

		/*!
		@description 

		Resource title - Preload data. The resource can be defined in the Description.ext file. 
		
		@locality

		Locally applied, Globally effected

		@version  [VBSFusion v3.15]

		@param resource - Resource name (as defined in RscTitles).
		@param type - Types of effects (PLAIN, PLAIN_DOWN, BLACK, BLACK_FADED, BLACK_OUT, BLACK_IN, WHITE_OUT, WHITE_IN ).
		
		@return bool - Returns true once resource was loaded successfully.

		@example

		@code

		bool b=TitleTextUtilities::applyTitleRscPreload(string("BIS"), TITLE_EFFECT_TYPE::PLAIN);

		@endcode

		@overloaded  
		
		None
		
		@related

		None

		@remarks This is a replication of bool preloadTitleRsc(string& resource, TITLE_EFFECT_TYPE type) function.
		*/
		static bool applyTitleRscPreload(const std::string& resource, const TITLE_EFFECT_TYPE type);

		/*!
		Prolongs the active title effect.
		Deprecated. Use void applyTitleEffectProlong(const double time)
		*/
		VBSFUSION_DPR(UTTX001) static void prolongTitleEffect(double time);

		/*!
		Displays a text message in the center of the screen.
		Deprecated. Use void applyCutText(string& text, const TITLE_EFFECT_TYPE type, const double speed)
		*/
		VBSFUSION_DPR(UTTX002) static void cutText(std::string text,TITLE_EFFECT_TYPE type,double speed);

		/*!
		Displays a text message in the center of the screen with additonal parameter layer
		layer: Number - Defines the layer in which the effect is shown, where 0 is the backmost one. 
		Deprecated. Use void applyCutText(const double layer, string& text, const TITLE_EFFECT_TYPE type, const double speed)
		*/
		VBSFUSION_DPR(UTTX003) static void cutText(double layer,std::string text,TITLE_EFFECT_TYPE type,double speed);

		/*!
		Display a resource defined in the mission's description.ext, the campaign's 
		description.ext or the global resource.cpp. 
		Deprecated. Use void applyCutRsc(string& resource, const TITLE_EFFECT_TYPE type, const double speed = 1)
		*/
		VBSFUSION_DPR(UTTX004) static void cutRsc(std::string resource,TITLE_EFFECT_TYPE type,double speed);

		/*!
		Displays text across the screen.
		If used along with cutText two different texts (in different type styles) can be shown at once. 
		Deprecated. Use void applyTitleText(string& text, const TITLE_EFFECT_TYPE type, const double speed = 1)
		*/
		VBSFUSION_DPR(UTTX005) static void titleText(std::string text,TITLE_EFFECT_TYPE type,double speed);

		/*!
		Terminate the effect in the given layer and set duration of the fade out phase to the given time. 
		Deprecated. Use void applyCutFadeOut(const double layer, const double delay)
		*/
		VBSFUSION_DPR(UTTX006) static void cutFadeOut(double layer,double delay);

		/*!
		Resource title - Resource can be defined in Description.ext Also see cutRsc, with these two commands you can show two different resources at once. 
		Deprecated. Use void applyTitleRsc(string& resource, const TITLE_EFFECT_TYPE type, const double speed = 1)
		*/
		VBSFUSION_DPR(UTTX007) static void titleRsc(std::string resource, TITLE_EFFECT_TYPE type,double speed);

		/*!
		Terminate the title effect and set duration of the fade out phase to the given time. 
		Deprecated. Use void applyTitleFadeOut(const double duration)
		*/
		VBSFUSION_DPR(UTTX008) static void titleFadeOut(double duration);

		/*!
		Resource title - Preload data. The resource can be defined in the Description.ext file. 
		Deprecated. Use bool applyTitleRscPreload(string& resource, const TITLE_EFFECT_TYPE type)
		*/
		VBSFUSION_DPR(UTTX009) static bool preloadTitleRsc(std::string& resource, TITLE_EFFECT_TYPE type);

	};

};
#endif