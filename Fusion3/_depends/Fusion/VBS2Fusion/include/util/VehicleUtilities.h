/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

VehicleUtilities.h

Purpose:

This file contains the declaration of the VehicleUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			02-04/2009	RMR: Original Implementation

2.0			10-02/2010  YFP: Added	void animateVehicle( Vehicle& vehicle ,string animationName,double phase)
									void createVehicleLocal(Vehicle& vehicle)
									Unit getEffectiveCommander(Vehicle& vehicle)
									void animateVehicle( Vehicle& vehicle ,string animationName,double phase)
2.01		10-02/2010	MHA: Comments Checked
2.02		14-06/2010  YFP: Added void createVehicle(Vehicle& , string, string)

2.03		02-07/2010  YFP: Methods added,
									void setManualControl(Vehicle&,bool)
									void setTurnWanted(Vehicle&,double)
									void setThrustWanted(Vehicle&,double)
2.04		05-07/2010  YFP: Methods added,
									void startEngine(Vehicle&,bool);
2.05		10-01/2011  CGS: Methods added getVehicleGroup(Vehicle& vehicle)
							Modified All createVehicle
								createAndAddWaypoint(Vehicle& vehicle, Waypoint& wp)
2.06		14-09/2011	NDB: Methods Added,
								vector<CrewPos> getCrewPosition(Vehicle& vehicle)
								vector<string> getMuzzles(Vehicle& vehicle, Turret::TurretPath& turretPath)
								bool isEngineDisabled(Vehicle& vehicle)
								void applyEngineDisable(Vehicle& vehicle, bool state)
								void applyTowParent(Vehicle& vehicle1, Vehicle& vehicle2)
								Vehicle getTowParent(Vehicle& vehicle)
								void applyBlockSeat(Vehicle& vehicle, VEHICLEASSIGNMENTTYPE type, int index = 0, bool isblock = true)
								void applyBlockSeat(Vehicle& vehicle, vector<int> index, bool isblock = true)
								vector<BLOCKEDSEAT> getBlockedSeats(Vehicle& vehicle)
								int getCargoIndex(Vehicle& vehicle, Unit& unit)
								Unit getUnitAtCargoIndex(Vehicle& vehicle, int index)
								void applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath turretPath, position3D& position,bool trackHidden = false)
								position3D getTurretLockedOn(Vehicle& vehicle, Turret::TurretPath turretPath)
								double getLaserRange(Vehicle& vehicle, Unit& gunner)
								void applyLaserRange(Vehicle& vehicle, double range)
								void applyLaserRange(Vehicle& vehicle, Unit& gunner, double range)
								int getLasingStatus(Vehicle& vehicle, Unit& gunner)
								void applyOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath, double azimuth, double elevation, bool transition = true)
								vector<double> getOpticsOffset(Vehicle& vehicle, Unit& gunner)
								vector<double> getOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath)
								void applyIndicators(Vehicle& vehicle, bool left, bool hazard, bool right)
								void applyMaxFordingDepth(Vehicle& vehicle, double depth)
								double getMaxFordingDepth(Vehicle& vehicle)
								Unit getTurretUnit(Vehicle& vehicle, Turret::TurretPath turretPath)
2.07		19-09/2011	NDB: Added Methods
								void applyThrustLeftWanted(Vehicle& vehicle, double factor)
								void applyThrustRightWanted(Vehicle& vehicle, double factor)
								void applyFireArc(Vehicle& vehicle, Turret::TurretPath& turretPath, vector3D& direction, double sideRange, double verticalRange, bool relative)
								void applyCommanderOverride(Vehicle& vehicle, Unit& unit, Turret::TurretPath turretPath)
								void land(Vehicle& vehicle, LANDMODE mode)
								void sendToVehicleRadio(Vehicle& vehicle, string name)
								void vehicleChat(Vehicle& vehicle, string message)
								bool isEngineOn(Vehicle& vehicle)
								void commandGetOut(Unit& unit)
								void doGetOut(Unit& unit)
								void applyVehicleId(Vehicle& vehicle, int id)
								void respawnVehicle(Vehicle& vehicle, double delay = -1, int count = 0)
								bool isTurnedOut(Unit& unit)
								void autoAssignVehicle(Unit& unit)
2.08		26-09/2011	NDB: Added Methods
								void assignAsCommander(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsDriver(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsGunner(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsCargo(Vehicle& vehicle, Unit& unit, double delay)
								void orderGetIn(vector<Unit> unitVec, bool order)
								void orderGetIn(Unit& unit, bool order)
								vector<WEAPONCARGO> getWeaponFromCargo(Vehicle& vehicle)
								vector<MAGAZINECARGO> getMagazineFromCargo(Vehicle& vehicle)
								vector<string> getMuzzles(Vehicle& vehicle)
								position3D getAimingPosition(Vehicle& vehicle, ControllableObject& target)
2.09		26-09/2011	NDB:	Added Method void commandMove(Vehicle& vehicle, position3D position)
2.10		04-11/2011	NDB: Added Mehthods
								bool isKindOf(Vehicle& vehicle, string typeName)
								float knowsAbout(Vehicle& vehicle, ControllableObject& target)
2.11		08-11-2011	SSD		void applyName(Vehicle& vehicle)
2.12        13-01-2012  RDJ     Add Methods
								static void landTo(Vehicle& vehicle, int port)
								static void assignToAirport(Vehicle& vehicle, int port)
2.12		18-01-2012	RDJ		Add the method islocked(Vehicle& vehicle)
2.13		18-01-2012	RDJ		Add the method void moveInCargoWithPosition(Unit& unit,Vehicle& vehicle, int cargoIndex)
2.14		19-01-2012  RDJ		Add the Method void groupLeaveVehicle(Group& group, Vehicle& vehicle);
2.15		19-01-2012	RDJ		Add the Method void groupLeaveVehicle(Unit& unit, Vehicle& vehicle);



/************************************************************************/ 

#ifndef VBS2FUSION_VEHICLE_UTILITIES_H
#define VBS2FUSION_VEHICLE_UTILITIES_H
/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBSFusion.h"
#include "data/Vehicle.h"
#include "data/Unit.h"
#include "util/ExecutionUtilities.h"
#include "VBS2FusionAppContext.h"
#include "WaypointUtilities.h"
#include "ControllableObjectUtilities.h"
#include "data/VBSTypes.h"
#include "data/VehicleArmedModule.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBSFusion
{
	struct CrewPos
	{
		std::string type;
		position3D pos;
		int cargoID;
		Unit unit;
		Turret::TurretPath turret;
// 		CrewPos(){}
// 		~CrewPos(){}
// 		void operator=(const CrewPos& obj)
// 		{
// 			this->type = obj.type;
// 			this->pos = obj.pos;
// 			this->cargoID = obj.cargoID;
// 			this->unit = obj.unit;
// 		}

	};

	class VBSFUSION_API VehicleUtilities : public ControllableObjectUtilities
	{
	public:	

		//********************************************************
		// Load utilities
		//********************************************************

		/*!
		
		@description

		Used to load all units belonging to a vehicle onto the vehicle list. Random aliases are assigned to each loaded unit.
		The network ID and the name of each unit are also loaded along with its role (Driver, Cargo, Turret) within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the units are loaded. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::loadUnits(v1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks remarks To load the units of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: loadUnits(Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void loadUnits(Vehicle& vehicle);		

		/*!
		
		@description

		Used to load all waypoints belonging to a vehicle onto the waypoint list. Random aliases are assigned to each waypoint within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the waypoints are loaded. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::loadWaypoints(v1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To load the waypoints of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::loadWaypoints (Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void loadWaypoints(Vehicle& vehicle);		

		////**************************************************************		

		/*!
		
		@description

		Used to load all units and waypoints belonging to a vehicle within the VBS2 environment. This function is a combination of VehicleUtilities::loadUnits(Vehicle& vehicle) and VehicleUtilities::loadWaypoints(Vehicle& vehicle).

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the units and waypoints are loaded. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::loadVehicleUnitsAndWaypoints(v1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To load the units and waypoints of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: loadVehicleUnitsAndWaypoints(Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void loadVehicleUnitsAndWaypoints(Vehicle& vehicle);		

		/*!
		
		@description

		Used to update the properties of a vehicle created within the VBS2 Environment in the fusion end. The properties that are updated are:
			- The vehicle members list
			- The waypoint list. 
			- position
			- direction
			- isAlive
			- group name
			- damage
			- fuel
			- type
			- positionASL
			- The current waypoint.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the properties are updated in the fusion end. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::updateVehicle(v1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the properties of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::updateVehicle (Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/

		static void updateVehicle(Vehicle& vehicle);

		
		//********************************************************
		// position utilities
		//********************************************************


		/*!
		
		@description

		Used to obtain the position of a vehicle within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param object - The vehicle for which the position is obtained. The vehicle should be created before passing it as a parameter.

		@return position3D

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		displayString+=" The position of v1 is "+VehicleUtilities::getPosition(v1).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities::updatePosition(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the position of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getPosition(Vehicle& object).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getPosition(const Vehicle& object);

		/*!
		
		@description

		Used to update the position of a vehicle created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the position is updated in the fusion end. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::updatePosition (v1);
		
		displayString+=" The position of v1 in the fusion end is "+v1.getPosition().getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities::getPosition(Vehicle& object);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the position of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updatePosition(Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information) 

		*/
		static void updatePosition(Vehicle& vehicle);

		/*!
		
		@description

		Used to apply a position to a vehicle created within the VBS2 Environment. The position value is not updated on the vehicle variable. Thus use the function VehicleUtilities::updateVehicle(Vehicle& vehicle)to update the vehicle variable and verify the changes that have been made.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the position is applied. The vehicle should be created before passing it as a parameter.

		@param position - The position applied to the vehicle. If the positional information is invalid, the vehicle object would be moved to map location [0,0,0].

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::applyPosition(veh1, position3D (position3D(300,300,0)));

		@endcode

		@overloaded

		VehicleUtilities::applyPosition(Vehicle& vehicle);

		@related

		VehicleUtilities::getPosition(Vehicle& object);

		VehicleUtilities::updatePosition(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply a position to a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyPosition(Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information) 

		*/
		static void applyPosition(Vehicle& vehicle, const position3D &position);

		/*!
		
		@description

		Used to apply a position specified by the function setPosition to a vehicle created within the VBS2 Environment. The position value is not updated on the vehicle variable. Thus use the function VehicleUtilities::updateVehicle(Vehicle& vehicle)to update the vehicle variable and verify the changes that have been made.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the position is applied. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		veh1.setPosition(player.getPosition()+position3D(300,300,0));
		
		VehicleUtilities::applyPosition(v1);

		@endcode

		@overloaded

		VehicleUtilities::applyPosition(Vehicle& vehicle, position3D position);

		@related

		VehicleUtilities::getPosition(Vehicle& object);

		VehicleUtilities::updatePosition(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To apply a position to a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyPosition(Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further.

		*/
		static void applyPosition(Vehicle& vehicle);


		//********************************************************
		// Direction utilities
		//********************************************************

		/*!
		
		@description

		Used to obtain the direction of a vehicle within the VBS2 Environment ranging from 0 - 360.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the direction is obtained. The vehicle should be created before passing it as a parameter.

		@return double

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		// To display the double as a string use the function DoubleToString() as shown below
		displayString+="The direction of v1 is "+conversions::DoubleToString(VehicleUtilities::getDirection(v1));

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities:: updateDirection(Vehicle& unit);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the direction of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getDirection(Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getDirection(const Vehicle& vehicle);

		/*!
		
		@description

		Used to update the direction of a vehicle created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit - The unit for which the direction is updated in the fusion end. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::updateDirection(v1);
		
		displayString+="The direction of v1 is "+conversions::DoubleToString(v1.getDirection());

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities:: applyDirection(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the direction of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updateDirection(Vehicle& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateDirection(Vehicle& unit);

		/*!
		
		@description

		Used to apply a direction to a vehicle created within the VBS2 Environment. 

		@locality

		Locally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the direction is applied. The vehicle should be created before passing it as a parameter. 

		@param direction - The direction applied to the vehicle.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::applyDirection(v1,289.5);

		@endcode

		@overloaded

		VehicleUtilities:: applyDirection(Vehicle& vehicle);

		@related

		VehicleUtilities:: getDirection(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 
		
		*/
		static void applyDirection(Vehicle& vehicle, double direction);

		/*!
		
		@description

		Used to apply a direction specified by the function setDirection to a vehicle created within the VBS2 Environment.

		@locality

		Locally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the direction is applied. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		v1.setDirection(189.5);
		
		VehicleUtilities::applyDirection(v1);

		@endcode

		@overloaded

		VehicleUtilities:: applyDirection(Vehicle& vehicle, double direction);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyDirection(Vehicle& vehicle);


	
		//********************************************************
		// Group utilities
		//********************************************************	
		
		/*!
		
		@description

		This function displays the group of the vehicle created within the VBS3 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The name of the vehicle for which its group name is required. 

		@return Group

		@example

		@code

		Vehicle veh1;
		
		Group grp;
		
		grp= VehicleUtilities::getVehicleGroup(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the group of the  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getVehicleGroup(Vehicle& vehicle). 

		*/
		static Group getVehicleGroup(const Vehicle& vehicle);

		/*!
		
		@description

		Used to obtain the name of the group the vehicle belongs to, within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the group name is obtained. The vehicle should be created before passing it as a parameter.

		@return double

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		displayString+="The group name is "+VehicleUtilities::getGroup(v1);

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities::updateGroup(Vehicle& unit);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the name of the group of a vehicle created in a client machine within a network, use the function 
		MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getGroup(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::string getGroup(Vehicle& vehicle);		

		/*!
		
		@description

		Used to update the name of the group of a vehicle created within the VBS2 Environment in the fusion end. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit - The vehicle for which the name is updated in the fusion end. The vehicle should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::updateGroup(v1);

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities::getGroup(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the name of the group of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updateGroup (Vehicle& unit). 
		
		*/
		static void updateGroup(Vehicle& unit);


		//********************************************************
		// Vehicle alive utilities
		//********************************************************	
	
		/*!
		
		@description

		Used to update the alive status of a vehicle created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit - The vehicle for which the status is updated in the fusion end. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::updateAlive(v1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the status of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::updateAlive (Vehicle& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information) 

		*/
		static void updateAlive(Vehicle& unit);


		//********************************************************
		// Damage utilities
		//********************************************************	
			
		/*!
		
		@description

		Used to obtain the current damage level of a vehicle within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the damage is obtained. The vehicle should be created before passing it as a parameter.

		@return double

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		// To display the double as a string use the function DoubleToString() as shown below
		displayString+="The damage level of veh1 is "+conversions::DoubleToString(VehicleUtilities::getDamage(v1));

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities::updateDamage(Vehicle& unit);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the damage level of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getDamage(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getDamage(const Vehicle& vehicle);

		/*!
		
		@description

		Used to update the current damage level of a vehicle created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit - The vehicle for which the current damage level is updated in the fusion end. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::updateDamage(v1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the damage level of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updateDamage(Vehicle& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void updateDamage(Vehicle& unit);

		/*!
		
		@description

		Used to apply damage to a vehicle created within the VBS2 Environment.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the damage is applied. The vehicle should be created before passing it as a parameter.

		@param damage - The damage level applied to the vehicle.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::applyDamage(v1,0.4562);

		@endcode

		@overloaded

		VehicleUtilities::applyDamage(Vehicle& vehicle);

		@related

		VehicleUtilities::getDamage(Vehicle& vehicle)

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply damage to a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyDamage(Vehicle& vehicle, double damage).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyDamage(Vehicle& vehicle, double damage);

		/*!
		
		@description

		Used to apply a damage level specified by the function setDamage to a vehicle created within the VBS2 Environment. 

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the damage is applied. The vehicle should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		v1.setDamage(0.4562);
		
		VehicleUtilities::applyDamage(v1);

		@endcode

		@overloaded

		VehicleUtilities::applyDamage(Vehicle& vehicle, double damage);

		@related

		VehicleUtilities::getDamage(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply damage to a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyDamage(Vehicle& vehicle)).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyDamage(Vehicle& vehicle);

	

		//********************************************************
		// Fuel utilities
		//********************************************************		

		/*!
		
		@description

		Used to obtain the current fuel level of a vehicle within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the fuel is obtained. The vehicle should be created before passing it as a parameter.

		@return double

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		// To display the double as a string use the function DoubleToString() as shown below
		displayString+="The fuel level of veh1 is "+conversions::DoubleToString(VehicleUtilities::getFuel(veh1));

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities::updateFuel(Vehicle& vehicle);

		@remarks  It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the fuel level of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getFuel(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getFuel(const Vehicle& vehicle);

		/*!
		
		@description

		Used to update the current fuel level of a vehicle created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit - The vehicle for which the current fuel level is updated in the fusion end. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities:: updateFuel(v1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the fuel level of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::updateFuel (Vehicle& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateFuel(Vehicle& unit);

		/*!
		
		@description

		Used to apply the fuel level to a vehicle created within the VBS2 Environment.

		@locality

		Locally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the fuel level is applied. The vehicle should be created before passing it as a parameter.

		@param fuel - The fuel level applied to the vehicle.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::applyFuel(v1, 0.4562);

		@endcode

		@overloaded

		VehicleUtilities::applyFuel(Vehicle& vehicle);

		@related

		VehicleUtilities:: getFuel(Vehicle& vehicle);

		@remarks  It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyFuel(Vehicle& vehicle, double fuel);		

		/*!
		
		@description

		Used to apply a fuel level specified by the function setFuel to a vehicle created within the VBS2 Environment.

		@locality

		Locally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the fuel is applied. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		v1. setFuel(0.4562);
		
		VehicleUtilities::applyFuel(v1);

		@endcode

		@overloaded

		VehicleUtilities::applyFuel(Vehicle& vehicle, double fuel);

		@related

		VehicleUtilities::getFuel(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyFuel(Vehicle& vehicle);

		//********************************************************
		// Type utilities
		//********************************************************		

		/*!
		Returns the type of the vehicle. 
		*/
		//static string getType(Vehicle& vehicle);

		/*!
		Updates the type of the vehicle. 
		*/
		//static void updateType(Vehicle& unit);


		//********************************************************
		// Waypoint utilities
		//********************************************************		

		/*!
		
		@description

		Used to create a new way point within the VBS2 Environment and adds it to the vehicle.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the waypoint is created and applied. The vehicle should be created before passing it as a parameter.

		@param  wp - The waypoint to which the vehicle is direcyted to move.
		
		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		//The waypoint to be assigned
		Waypoint w1;
		
		//The position assigned to the waypoint
		position3D wpposition (100,120,0);
		
		w1.setPosition(wpposition+veh1.getPosition());
		
		VehicleUtilities::createAndAddWaypoint(v1,w1);

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities::getCurrentWaypoint(Vehicle& vehicle)

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To create and add way points for a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: createAndAddWaypoint(Vehicle& vehicle, Waypoint& wp). 

		*/
		static void createAndAddWaypoint(Vehicle& vehicle, Waypoint& wp);

		/*!
		
		@description

		Used to return the index of the current way point of a vehicle within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the index of the current way point is obtained. The vehicle should be created before passing it as a parameter.

		@return int

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		displayString+="The waypoint is "+conversions::IntToString(VehicleUtilities::getCurrentWaypoint(v1));

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities::updateCurrentWaypoint(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the index of the current way point of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function getCurrentWaypoint(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static int getCurrentWaypoint(const Vehicle& vehicle);			

		/*!
		
		@description

		Used to update the index of the current way point of a vehicle within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the index of the current way point is updated in the fusion end. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		VehicleUtilities::updateCurrentWaypoint(v1);
		
		displayString+="The waypoint in the fusion end is "+conversions::IntToString(v1.getCurrentWaypoint());

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities::getCurrentWaypoint(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To update the index of the current way point of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function updateCurrentWaypoint(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void updateCurrentWaypoint(Vehicle& vehicle);


		//********************************************************
		// Vehicle create utilities
		//********************************************************	

		/*!
		
		@description

		Used to create a vehicle within the VBS2 Environment. The type, position, and other details of the object must be defined before calling the function, or else the default parameters would be used for the creation of the vehicle.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle to be created.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		v1.setName(string ("veh1"));
		
		v1.setType(string("VBS2_US_ARMY_M1A1_W_X"));
		
		v1.setPosition(player.getPosition()+position3D(11,11,0));
		
		ControllableObjectUtilities::applyPosition(v1);
		
		VehicleUtilities::createVehicle(v1);
		
		@endcode

		@overloaded

		VehicleUtilities::createVehicle(Vehicle& vehicle, string markerNames, string specialProperties);

		@related

		None

		@remarks Vehicles loaded with invalid or no positional information are created at map location [0,0,0] 

		@remarks If an alias has not been set to the vehicle, a random alias would be assigned to it.

		*/
		static void createVehicle(Vehicle& vehicle);

		/*!
		
		@description

		Used to create a vehicle within the VBS2 Environment.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle to be created.

		@param markerNames - The name of the marker.
		
		@param specialProperties - Options include "NONE", "FLY" and "FORM".

		@return Nothing

		@example

		@code

		Vehicle v1;
		
		Marker m1;
		
		m1.setName("mark");
		
		MarkerUtilities::createMarker(m1);
		
		position3D markerpos = player.getPosition()+position3D(40,40,0);
		
		MarkerUtilities::applyType(m1,"Flag");
		
		MarkerUtilities::applySize(m1, 9.5, 9.5);
		
		MarkerUtilities::applyColor(m1, "ColorRed");
		
		MarkerUtilities::applyPosition(m1,markerpos);
		
		MarkerUtilities::updateMarker(m1);
		
		VehicleUtilities::createVehicle(v1,"mark","FLY");

		@endcode

		@overloaded

		VehicleUtilities::createVehicle(Vehicle& vehicle);

		@related

		None

		@remarks Vehicles loaded with invalid marker name are created at map location [0,0,0]. 

		@remarks If an alias has not been set to the vehicle, a random alias would be assigned to it. 

		*/
		static void createVehicle(Vehicle& vehicle, const std::string& markerNames, const std::string& specialProperties);

		/*!
		
		@description

		Used to create a vehicle locally within the VBS2 Environment. 

		@locality

		Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle to be created. 

		@return Nothing 

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		v1.setName(string ("veh1"));
		
		v1.setType(string("VBS2_US_ARMY_M1A1_W_X"));
		
		v1.setPosition(player.getPosition()+position3D(11,11,0));
		
		VehicleUtilities::applyPosition(v1);
		
		VehicleUtilities::updateDynamicProperties(v1);
		
		VehicleUtilities::updateStaticProperties(v1);
		
		VehicleUtilities::createVehicleLocal(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Vehicles loaded with invalid marker name are created at map location [0,0,0]. 

		@remarks If an alias has not been set to the vehicle, a random alias would be assigned to it.

		*/
		static void createVehicleLocal(Vehicle& vehicle);

		//********************************************************
		// Vehicle delete utilities
		//********************************************************	

		/*!
		
		@description

		Used to delete a vehicle within the VBS2 Environment.

		@locality

		Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle to be deleted.

		@return Nothing

		@example

		@code

		VehicleUtilities::deleteVehicle(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks

		*/
		static void deleteVehicle(Vehicle& vehicle);

		//********************************************************
		// Vehicle group utilities
		//********************************************************	

		/*!
		
		@description

		Used to add a vehicle to a group within the VBS2 Environment.

		@locality

		Locally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle to be added.

		@param group - The name of the group for which the vehicle is assigned to.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;
		
		//The group to be created
		Group gr1;
		
		VehicleUtilities::addToGroup(v1,gr1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks 

		*/
		static void addToGroup(Vehicle& vehicle, Group& group);		


		/*!
		
		@description

		Used to obtain the commander of the vehicle within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the commander is obtained. The vehicle should be created before passing it as a parameter.

		@return unit

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		commander_veh1 = VehicleUtilities::getEffectiveCommander(veh1);
		
		displayString += "The effective commander for veh1 is "+commander_veh1.getName();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the commander of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getEffectiveCommander(Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static Unit getEffectiveCommander(const Vehicle& vehicle);

		//********************************************************
		// Vehicle move in utilities
		//********************************************************

		/*!
		
		@description

		Used to create a unit and move the unit into the vehicle as a driver.

		@locality

		Globally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle in which the unit is moved in. The vehicle should be created before passing it as a parameter.   

		@param unit - The unit that is moved into vehicle as a driver.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		Unit driver;
		
		VehicleUtilities::createAndMoveInAndAsDriver(veh1,driver);


		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To create a unit and move it into  a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: createAndMoveInAndAsDriver(Vehicle& vehicle, Unit& unit). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		*/
		static void createAndMoveInAndAsDriver(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as driver. 

		This function uses the network ID of the object to access it within the game environment, so please
		ensure that either a registered alias is assigned to vehicle. 

		Deprecated. Use void applyMoveInAsDriver(Vehicle& vehicle, Unit& unit)
		*/
		VBSFUSION_DPR(UVHC001) static void moveInAndAsDriver(Vehicle& vehicle, Unit& unit);	

		/*!
		
		@description

		Used to create a unit and move the unit into the default turret [1,0] of a vehicle.

		@locality

		Globally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle in which the unit is moved in. The vehicle should be created before passing it as a parameter. 

		@param unit - The unit that is moved into the turret.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		Unit tu;
		
		tu.setName(string ("testunit"));
		
		VehicleUtilities::createAndMoveInAndAsTurret(veh1,tu);

		@endcode

		@overloaded

		VehicleUtilities::createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turrntPath);

		VehicleUtilities::createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To create a unit and move it into  a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit);

		
		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		It take default turret
		[0,1]: second sub-turret of first main turret.

		Deprecated. Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit)
		*/
		VBSFUSION_DPR(UVHC004) static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit);

		/*!
		
		@description

		Used to create a unit and move the unit into the turret   of a vehicle. 

		@locality

		Globally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle in which the unit is moved in. The vehicle should be created before passing it as a parameter.

		@param unit - The unit that is moved into the turret.

		@param turrntPath - The turret in which the unit is placed. Option include 
			[0]: first turret (no sub-turrets exist).
			[0,0]: first sub-turret of first main turret.
			[1,0]: second sub-turret of first main turret.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		Unit tu;
		
		tu.setName(string ("testunit"));

		VehicleUtilities::createAndMoveInAndAsTurret(tu,driver,"[0,1]");	

		@endcode

		@overloaded

		VehicleUtilities::createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit);

		VehicleUtilities::createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To create a unit and move it into  a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turrntPath).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, const std::string& turrntPath);

		
		/*!
		
		@description

		Used to create a unit and move the unit into the turret   of a vehicle. 

		@locality

		Globally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle in which the unit is moved in. The vehicle should be created before passing it as a parameter. 

		@param unit - The unit that is moved into the turret.

		@param turretPath - The turret in which the unit is placed. Option include 
			[0]: first turret (no sub-turrets exist).
			[0,0]: first sub-turret of first main turret.
			[1,0]: second sub-turret of first main turret.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		Unit tu;
		tu.setName(string ("testunit"));

		Turret::TurretPath tpath;
		tpath.push_back(0);
		tpath.push_back(1);

		Turret t;
		t.setTurretPath(tpath);

		VehicleUtilities::createAndMoveInAndAsTurret(veh1,tu,t);

		@endcode

		@overloaded

		VehicleUtilities::createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit);

		VehicleUtilities::createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turrntPath);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To create a unit and move it into  a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, const Turret& turretPath);

		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: first sub-turret of second main turret.

		Deprecated. Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, string turretPath)
		*/
		VBSFUSION_DPR(UVHC002) static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, std::string turretPath);

		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: first sub-turret of second main turret.

		Deprecated. Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath).
		*/
		VBSFUSION_DPR(UVHC003) static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath);

		/*!
		
		@description

		Used to create a unit and move the unit into the vehicle as cargo. 

		@locality

		Globally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle in which the unit is moved in. The vehicle should be created before passing it as a parameter.

		@param unit - The unit that is moved as cargo.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		Unit tu;
		
		tu.setName(string ("testunit"));
		
		VehicleUtilities::createAndMoveInAndAsCargo(veh2,tu);


		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To create a unit and move it into  a vehicle created in a client machine within a network as cargo, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function createAndMoveInAndAsCargo(Vehicle& vehicle, Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void createAndMoveInAndAsCargo(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as cargo. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyMoveInAsCargo(Vehicle& vehicle, Unit& unit)
		*/
		VBSFUSION_DPR(UVHC005) static void moveInAndAsCargo(Vehicle& vehicle, Unit& unit);		

		/*!
		
		@description

		Used to create a unit and move the unit into the vehicle as commander. 

		@locality

		Globally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle in which the unit is moved in. The vehicle should be created before passing it as a parameter.

		@param unit - The unit that is moved in as commander.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		Unit tu;
		
		tu.setName(string ("testunit"));
		
		VehicleUtilities::createAndMoveInAndAsCommander(veh1,tu);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To create a unit and move it into  a vehicle created in a client machine within a network as commander, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function createAndMoveInAndAsCommander(Vehicle& vehicle, Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void createAndMoveInAndAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as commander. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 		

		Deprecated. Use void applyMoveInAsCommander(Vehicle& vehicle, Unit& unit)
		*/
		VBSFUSION_DPR(UVHC007) static void moveInAndAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		
		@description

		Used to create a unit and move the unit into the vehicle as gunner

		@locality

		Globally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle in which the unit is moved in. The vehicle should be created before passing it as a parameter.

		@param unit - The unit that is moved in as gunner.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		Unit u
		
		u.setName(string ("testunit"));
		
		VehicleUtilities::createAndMoveInAndAsGunner(veh1,u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To create a unit and move it into  a vehicle created in a client machine within a network as gunner, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function createAndMoveInAndAsGunner(Vehicle& vehicle, Unit& unit). 

		*/
		static void createAndMoveInAndAsGunner(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as gunner. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyMoveInAsGunner(Vehicle& vehicle, Unit& unit)
		*/
		VBSFUSION_DPR(UVHC006) static void moveInAndAsGunner(Vehicle& vehicle, Unit& unit);



		//********************************************************
		// Vehicle leave utilities
		//********************************************************

		/*!
		Orders unit to leave vehicle. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		VBSFUSION_DPR(UVHC009) static void leaveVehicle(Vehicle& vehicle, Unit& unit);


		/*!
		Orders unit with ID defined by unit.getID() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		VBSFUSION_DPR(UVHC009) static void leaveVehicleByID(Vehicle& vehicle, Unit& unit); 

		/*!
		Orders unit with Name defined by unit.getName() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		VBSFUSION_DPR(UVHC009) static void leaveVehicleByName(Vehicle& vehicle, Unit& unit); 


		/*!
		Orders unit with Alias defined by unit.getAlias() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		VBSFUSION_DPR(UVHC009) static void leaveVehicleByAlias(Vehicle& vehicle, Unit& unit);		

		//*******************************************************/

		/*!
		Activate and deactivate the engine of the vehicle. If state is true, 
		engine will be on and it will stop when state is false.

		Deprecated. Use void applyEngineStart(Vehicle& vehicle , bool state)
		*/
		VBSFUSION_DPR(UVHC008) static void startEngine(Vehicle& vehicle , bool state);

		/*!
		Enable/ disable manual control of the vehicle.
		if control is TRUE  vehicle AI disabled and it can be controlled 
		by setTurnWanted & setThrustWanted functions.  

		Deprecated. Use void applyManualControl(Vehicle& vehicle , bool control)
		*/
		VBSFUSION_DPR(UVHC010) static void setManualControl(Vehicle& vehicle , bool control);

		/*!
		set the turn wanted for the vehicle specified by the factor value.
		manual control of the vehicle should be enabled first. 

		Deprecated. Use void applyTurnWanted(Vehicle& vehicle, double factor)
		*/
		VBSFUSION_DPR(UVHC011) static void setTurnWanted(Vehicle& vehicle, double factor);

		/*!
		set the thrust wanted for the vehicle to passed in. 
		manual control of the vehicle should be enabled first.

		Deprecated. Use void applyThrustWanted(Vehicle& vehicle , double factor)
		*/
		VBSFUSION_DPR(UVHC012) static void setThrustWanted(Vehicle& vehicle , double factor);

		//********************************************************
		// Animate utilities
		//********************************************************

		/*!
		Animate Vehicle. 
		animationName - The name of the animation
		phase - the phase value is between 0 and 1.  

		Deprecated. Use void applyAnimation(Vehicle& vehicle, string animationName, double phase)
		*/
		VBSFUSION_DPR(UVHC020) static void animateVehicle( Vehicle& vehicle ,std::string animationName,double phase);

		/*!
		
		@description

		Used to obtain the current gear of a vehicle within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the current gear is obtained. The vehicle should be created before passing it as a parameter.

		@return int

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		// To display the double as a string use the function IntToString() as shown below
		displayString += "The current gear of veh1 is "+conversions::IntToString(VehicleUtilities::getCurrentGear(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the  current gear  of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getCurrentGear(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static int getCurrentGear(const Vehicle& vehicle);

		/*!
		
		@description

		Used to obtain the current RPM of a vehicle within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the current RPM is obtained. The vehicle should be created before passing it as a parameter.

		@return float

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		// To display the double as a string use the function FloatToString() as shown below
		displayString += "The current rpm of veh1 "+conversions::FloatToString(VehicleUtilities::getCurrentRPM(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the  current RPM  of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getCurrentRPM(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static float getCurrentRPM(const Vehicle& vehicle);
		
		/*!
		
		@description

		Used to obtain the current RPM of a vehicle within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the current RPM is obtained. The vehicle should be created before passing it as a parameter.

		@return double

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		// To display the double as a string use the function DoubleToString() as shown below
		displayString += "The current rpmex of veh1 "+conversions::DoubleToString(VehicleUtilities::getCurrentRPMEx(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the  current RPM  of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getCurrentRPMEx(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getCurrentRPMEx(const Vehicle& vehicle);
		
		/*!
		
		@description

		Used to obtain the engine strength coefficient of a vehicle within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the engine strength coefficient is obtained. The vehicle should be created before passing it as a parameter.

		@return double

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		// To display the double as a string use the function DoubleToString() as shown below
		displayString += "The engine strength coefficient of  veh1 "+conversions::DoubleToString(VehicleUtilities::getEngineStrengthCoefficient(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the  engine strength coefficient  of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getEngineStrengthCoefficient(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getEngineStrengthCoefficient(const Vehicle& vehicle);

		/*!
		
		@description

		Used to apply the vehicle engine strength coefficient of a vehicle within the VBS2 Environment.
		It multiplies the engine strength and divides the max speed of the vehicle by the specified coefficient (i.e it either makes the vehicle faster and weaker or slower and stronger.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the engine strength coefficient is applied to. The vehicle should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		veh1.setEngineStrength(0.6292);
		
		VehicleUtilities::applyEngineStrengthCoefficient(veh1);

		@endcode

		@overloaded

		VehicleUtilities::applyEngineStrengthCoefficient(Vehicle& vehicle , double strengthCoeff);

		@related

		None

		@remarks This function is mainly used in vehicles in connection with towing actions. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 
		
		@remarks To apply a engine strength coefficient to a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyEngineStrengthCoefficient (Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyEngineStrengthCoefficient(Vehicle& vehicle);

		/*!
		
		@description

		Used to apply a specified  engine strength coefficient to a vehicle within the VBS2 Environment.
		It multiplies the engine strength and divides the max speed of the vehicle by the specified coefficient (i.e it either makes the vehicle faster and weaker or slower and stronger.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the engine strength coefficient is applied to. The vehicle should be created before passing it as a parameter.

		@param strengthCoeff - The value of the engine strength coefficient.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		VehicleUtilities::applyEngineStrengthCoefficient(veh1,0.456);

		@endcode

		@overloaded

		VehicleUtilities::applyEngineStrengthCoefficient(Vehicle& vehicle);

		@related

		None

		@remarks This function is mainly used in vehicles in connection with towing actions. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 
		
		@remarks To apply a specified  engine strength coefficient to a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyEngineStrengthCoefficient (Vehicle& vehicle, double strengthCoeff). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyEngineStrengthCoefficient(Vehicle& vehicle , double strengthCoeff);

		/*!
		
		@description

		Used to obtain the maximum speed limit a vehicle within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the maximum speed limit is obtained. The vehicle should be created before passing it as a parameter.

		@return double

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		displayString += "The max speed limit is  "+conversions::DoubleToString(VehicleUtilities::getMaximumSpeedLimit(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the  maximum speed limit  of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getMaximumSpeedLimit (Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getMaximumSpeedLimit(const Vehicle& vehicle);

		/*!
		
		@description

		Used to set a specified value as the maximums speed of a vehicle. The speed limit should be in km/h. If the value of maxSpeedLimit is less than 0, then the vehicle�s maximum speed limit sets to its default value.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the maximum speed. The vehicle should be created before passing it as a parameter.  

		@param maxSpeedLimit - The maximum speed limit of the vehicle.		

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		VehicleUtilities::applyMaximumSpeedLimit(veh1,0.45);

		@endcode

		@overloaded

		VehicleUtilities::applyMaximumSpeedLimit(Vehicle& vehicle);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply the  maximum speed limit to a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyMaximumSpeedLimit(Vehicle& vehicle , double maxSpeedLimit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyMaximumSpeedLimit(Vehicle& vehicle , double maxSpeedLimit);
		
		/*!
		
		@description

		Used to obtain the name of the primary weapon of a vehicle within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the primary weapon name is obtained. The vehicle should be created before passing it as a parameter. 

		@return string

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		displayString+="The primary weapon of veh1 is "+VehicleUtilities::getPrimaryWeapon(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function returns an empty string if a primary weapon is not available. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks To obtain the  primary weapon name  of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getPrimaryWeapon(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::string getPrimaryWeapon(const Vehicle& vehicle);

		/*!
		
		@description

		Used to update the primary weapon of a vehicle  created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle  for which the primary weapon is updated in the fusion end. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		VehicleUtilities::updatePrimaryWeapon(veh1);
		
		displayString+="The primary weapon of veh1 is "+veh1.getPrimaryWeapon();

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities:: getPrimaryWeapon(Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the primary weapon  of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updatePrimaryWeapon(Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updatePrimaryWeapon(Vehicle& vehicle);

		/*
		
		@description

		Used to update the list of weapons assigned to a vehicle  created within the VBS2 Environment in the fusion end. 

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle  for which the list of weapon is updated in the fusion end. The vehicle should be created before passing it as a parameter.   

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		VehicleUtilities::updateWeaponTypes(veh1);

		@endcode

		@overloaded

		None

		@related

		VehicleUtilities::updateWeaponAmmo (Vehicle& vehicle);

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This function assigns each weapon with an ammo value of 0 and does not update the ammo amounts on the weapons magazines. Use the function VehicleUtilities:: updateWeaponAmmo to update the weapon types with ammo.
		
		@remarks To update the  weapon list of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updateWeaponTypes (Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void updateWeaponTypes(Vehicle& vehicle);

		/*!
		Select the Weapon given by the muzzle Name.
		Deprecated. Use void applyWeaponSelection(Vehicle& vehicle, string muzzleName)
		*/
		VBSFUSION_DPR(UVHC023) static void selectWeapon(Vehicle& vehicle, std::string muzzleName);

		/*
		
		@description

		Used to update the amount of ammo left in the primary magazine of each assigned weapon type of a vehicle  created within the VBS2 Environment in the fusion end.   

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle  for which the ammo  is updated in the fusion end. The vehicle should be created before passing it as a parameter.   

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		VehicleUtilities::updateWeaponAmmo(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This function uses the list defined by the weapons list within VehicleArmedModule, and therefore will only load ammo information for each weapon on that list, this function should be used after the function updateWeaponTypes is called. 
		 	
		@remarks To update the  ammo  of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updateWeaponAmmo (Vehicle& vehicle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateWeaponAmmo(Vehicle& vehicle);

		/*!
		Add magazine to the Vehicle.
		Deprecated. Use void applyMagazineAddition(Vehicle& vehicle, string magName)
		*/
		VBSFUSION_DPR(UVHC025) static void addMagazine(Vehicle& vehicle, std::string name);

		/*!
		Count number of magazines available in given Vehicle object.

		Deprecated. Use int getMagazineCount(Vehicle& vehicle)
		*/
		VBSFUSION_DPR(UVHC026) static int countMagazines(Vehicle& vehicle);

		/*!
		
		@description

		Used to update the list of magazines assigned to a vehicle�s armed module. This function loads all magazines assigned to the vehicle from VBS2 and adds a new Magazine object to the Magazine list of a vehicle  created within the VBS2 Environment in the fusion end.   

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle � The vehicle  for which the list of magazines  is updated in the fusion end. The vehicle should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		VehicleUtilities::updateMagazineTypes(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This function does not update the ammo amount within Magazine list. 
		So the ammo count within the MagazineList would be zero, in order to update this list Use the function VehicleUtilities::updateMagazineAmmo.

		@remarks To update the  list of magazines assigned to a  a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updateMagazineTypes (Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void updateMagazineTypes(Vehicle& vehicle);

		/*!
		
		@description

		Used to updates the magazine ammo count & magazine name inside the vehicleArmedModule's MagazineList therefore updates the magazine list with the required details of a vehicle  created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle  for which the magazine ammo count  is updated in the fusion end. The vehicle should be created before passing it as a parameter.   

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		VehicleUtilities::updateMagazineAmmo(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This function is adequate to update the Magazine List correctly, thus the function updateMagazineTypes need not be used before this function.
		
		@remarks To update the  magazine ammo count and magazine name of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updateMagazineAmmo (Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void updateMagazineAmmo(Vehicle& vehicle);

		/*!
		
		@description

		Used to load turrets available in armed module  vehicle  created within the VBS2 Environment in the fusion end. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle  for which the  turrets are updated in the fusion end. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle v1;

		VehicleUtilities::updateTurrets(v1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To load the turrets of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updateTurrets (Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void updateTurrets(Vehicle& vehicle);

		//----------------------------------------------------------------------------

		/*!
		
		@description

		Used to obtain the elevation of the weapon in a specified turret of the Armed Vehicle within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the elevation of the weapon is obtained. The vehicle should be created before passing it as a parameter.   

		@param turret - The turret object in which the weapon is located.
		
		@return double

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		Turret::TurretPath tpath;
		tpath.push_back(0);
		tpath.push_back(1);

		Turret t;
		t.setTurretPath(tpath);

		displayString+="The elevation is "+conversions::DoubleToString(VehicleUtilities::getElevation(veh1,t));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks Ensure that the correct turret path is given.
		
		@remarks To obtain the  elevation   of  the weapon of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getElevation(Vehicle& vehicle, Turret& turret).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static double getElevation(const Vehicle& vehicle, const Turret& turret);

		/*! 
		
		@description

		Used to obtain the azimuth of the weapon in a specified turret of the Armed Vehicle within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the azimuth of the weapon is obtained. The vehicle should be created before passing it as a parameter.

		@param turret - The turret object in which the weapon is located.

		@return double

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		Turret::TurretPath tpath;
		tpath.push_back(0);
		tpath.push_back(1);

		Turret t;
		t.setTurretPath(tpath);

		// To display the double as a string use the function DoubleToString() as shown below
		displayString+="The azimuth is "+conversions::DoubleToString(VehicleUtilities::getElevation(veh1,t));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Ensure that the correct turret path is given.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the  azimuth  of  the weapon of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getAzimuth (Vehicle& vehicle, Turret& turret).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getAzimuth(const Vehicle& vehicle, const Turret& turret);
	
		/*!
		
		@description

		Used to obtain the position of the weapon in model coordinates within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle that contains the weapon for which the position is obtained. The vehicle should be created before passing it as a parameter. 

		@param weapon - The weapon object for which the position is obtained.

		@return position3D

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		
		//The weapon to be set
		Weapon w1;

		w1.setWeaponName("vbs2_m256_120mm");

		// To display the position3D as a string use the function getVBSPosition() as shown below
		displayString+="The weapon position is "+VehicleUtilities::getWeaponPosition(veh1,w1).getVBSPosition();

		@endcode

		@overloaded

		VehicleUtilities::getWeaponPosition(Vehicle& vehicle, Turret& turret);

		@related

		None

		@remarks Ensure that the name of the weapon is correct. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the  position  of  a weapon  in a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getWeaponPosition(Vehicle& vehicle, Weapon& weapon).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static position3D getWeaponPosition(const Vehicle& vehicle, const Weapon& weapon);

		/*!
		
		@description

		Used to obtain weapon  position  on a turret  in model coordinates within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle that contains the turret for which the position is obtained. The vehicle should be created before passing it as a parameter.

		@param turret - The turret object in which the weapon is located.

		@return position3D

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		Turret::TurretPath tpath1;
		tpath1.push_back(0);
		tpath1.push_back(0);

		Turret t1;
		t1.setTurretPath(tpath1);

		// To display the position3D as a string use the function getVBSPosition() as shown below
		displayString+="The weapon position on the turret is "+VehicleUtilities::getWeaponPosition(veh1,t1).getVBSPosition();

		@endcode

		@overloaded

		VehicleUtilities::getWeaponPosition(Vehicle& vehicle, Weapon& weapon);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks Ensure that the turret path is correct. 

		@remarks To obtain the weapon  position  on a turret  in a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getWeaponPosition(Vehicle& vehicle, Turret& turret).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		*/
		static position3D getWeaponPosition(const Vehicle& vehicle, const Turret& turret);

		/*!
		
		@description

		Used to obtain the weapon firing point of a selected muzzle in model coordinates within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle that contains the weapon for which the point is obtained. The vehicle should be created before passing it as a parameter.

		@param weapon - The  weapon object for which the firing point is obtained.

		@return position3D

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		//The weapon to be set
		Weapon w1;

		w1.setWeaponName("vbs2_m256_120mm");

		// To display the position3D as a string use the function getVBSPosition() as shown below
		displayString+="The weapon point is "+VehicleUtilities::getWeaponPosition(veh1,w1).getVBSPosition();

		@endcode

		@overloaded

		getWeaponPoint(Vehicle& vehicle, Turret& turret, Weapon& weapon);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks Ensure that the name of the weapon is correct. 

		@remark To obtain the  firing point  of  a weapon  in a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getWeaponPoint (Vehicle& vehicle, Weapon& weapon).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		*/
		static position3D getWeaponPoint(const Vehicle& vehicle, const Weapon& weapon);

		/*!
		
		@description

		Used to obtain the firing point of a  weapon on a turret in model coordinates within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle that contains the weapon for which the point is obtained. The vehicle should be created before passing it as a parameter.

		@param turret - The turret in which the weapon is located.
		
		@param weapon - The weapon located in the turret, for which the firing point is obtained.

		@return position3D;

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		//The turret to be set

		Turret::TurretPath tpath2;
		tpath2.push_back(0);
		tpath2.push_back(1);

		Turret t2;
		t2.setTurretPath(tpath2);

		//The weapon to be set

		Weapon w1;
		w1.setWeaponName("vbs2_m256_120mm");

		displayString+="The weapon point is "+VehicleUtilities::getWeaponPoint(veh1,t2,w1).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks Ensure that the turret path is correct.
		
		@remarks To obtain the firing  point  of  a weapon on a turret in a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getWeaponPoint(Vehicle& vehicle, Turret& turret, Weapon& weapon);
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getWeaponPoint(const Vehicle& vehicle, const Turret& turret, const Weapon& weapon);

		/*! 
		
		@description

		Used to obtain the aiming direction of a weapon at a turret in an armed vehicle  within the VBS2 Environment.

		@locality

		Globally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle that contains the weapon for which the direction is obtained. The vehicle should be created before passing it as a parameter.   

		@param turret - The turret object.

		@return position3D

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		Turret::TurretPath tpath1;
		tpath1.push_back(0);
		tpath1.push_back(0);

		Turret t1;
		t1.setTurretPath(tpath1);

		displayString+="The weapon direction vector is "+VehicleUtilities::getWeaponDirectionVector(veh1,t1).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks Ensure that the turret path is correct.
		
		@remarks To obtain the aiming  direction  of  a weapon  in a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getWeaponDirection(Vehicle& vehicle, Turret& turret).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static position3D getWeaponDirectionVector(const Vehicle& vehicle, const Turret& turret);

		/*! 
		set reload state of the weapon.
		Deprecated. Use void applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime)
		*/
		VBSFUSION_DPR(UVHC028) static void setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime);

		/*!
		
		@description

		used to return the current weapon details of the vehicle within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle which contains the weapon for which the current details are obtained .The vehicle should be created before passing it as a parameter.   

		@param tPath - The turret path.

		@return Weapon

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		//The turret to be set
		Turret::TurretPath tpath2;
		tpath2.push_back(0);
		tpath2.push_back(1);

		Turret t2;
		t2.setTurretPath(tpath2);
		
		//The weapon to be set
		Weapon w3;
		
		w3 = VehicleUtilities::getWeaponState(veh1,tpath2);
		displayString+="The weapon state  is"+conversions::IntToString(w3.getAmmoCount());

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks Ensure that the turret path is correct.
		
		@remarks To obtain the current details of the weapon  in a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::getWeaponState(Vehicle& vehicle, Turret::TurretPath& tPath).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static Weapon getWeaponState(const Vehicle& vehicle, const Turret::TurretPath& tPath);

		/*!
		
		@description

		Used to return the list of objects the weapon in the vehicle is aiming at within the VBS2 Environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle which contains the weapon for which the objects it�s aiming are listed. The vehicle should be created before passing it as a parameter.

		@param weaponName - Name of the weapon

		@return list of Controllable Object

		@example

		@code

		//The vehicle to be created
		Vehicle veh2;

		list<ControllableObject> l1;
		l1 = VehicleUtilities::getWeaponAimingTargets(veh2,"VBS2_MK19_GMG");
		
		for (list<ControllableObject>::iterator itr = l1.begin(), itr_end = l1.end(); itr != itr_end; ++itr){
			displayString="The list of targets for l1 are "+itr->getName();
		}

		@endcode

		@overloaded

		list<ControllableObject> getWeaponAimingTargets(Vehicle& vehicle, Weapon& weapon);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static std::list<ControllableObject> getWeaponAimingTargets(const Vehicle& vehicle, const std::string& weaponName);

		/*!
		
		@description

		Used to return the list of objects the weapon in the vehicle is aiming at within the VBS2 Environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle which contains the weapon for which the objects it�s aiming are listed. The vehicle should be created before passing it as a parameter. 

		@param weapon - The weapon object.

		@return list of Controllable Objects

		@example

		@code

		//The vehicle to be created
		Vehicle veh2;
		
		//The weapon to be defined
		Weapon w1;
		w1.setWeaponName("VBS2_MK19_GMG");
		
		list<ControllableObject> l1;
		
		l1 = VehicleUtilities::getWeaponAimingTargets(veh2,w1);
		for (list<ControllableObject>::iterator itr = l1.begin(), itr_end = l1.end(); itr != itr_end; ++itr){
			displayString="The list of targets for l1 are "+itr->getName();
		}

		@endcode

		@overloaded

		list<ControllableObject> getWeaponAimingTargets(Vehicle& vehicle, string weaponName);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static std::list<ControllableObject> getWeaponAimingTargets(const Vehicle& vehicle, const Weapon& weapon);

		/*!
		Remove the specified magazine from the Vehicle.	
		Deprecated. Use void applyMagazineRemove(Vehicle& vehicle, string magName)
		*/
		VBSFUSION_DPR(UVHC031) static void removeMagazine(Vehicle& vehicle, std::string name);

		/*!
		Remove all magazine from the vehicle.	
		Deprecated. Use void applyMagazineRemoveAll(Vehicle& vehicle, string magName)
		*/
		VBSFUSION_DPR(UVHC032) static void removeallMagazines(Vehicle& vehicle, std::string name);

		/*!
		Add Weapon to the Vehicle. weapon slots are filled, any further addWeapon commands are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should 
		be added, before adding the weapon.

		Deprecated. Use void applyWeaponAddition(Vehicle& vehicle, string name)
		*/
		VBSFUSION_DPR(UVHC027) static void addWeapon(Vehicle& vehicle, std::string name);

		/*!
		Remove the specified Weapon from the unit.	

		Deprecated. Use void applyWeaponRemove(Vehicle& vehicle, string name)
		*/
		VBSFUSION_DPR(UVHC029) static void removeWeapon(Vehicle& vehicle, std::string name);

		/*!
		Tells if a unit has the given weapon.	

		Deprecated. Use void isWeaponAvailable(Vehicle& vehicle, string name)
		*/
		VBSFUSION_DPR(UVHC030) static bool hasWeapon(Vehicle& vehicle, std::string name);

		/*!
		Sets the direction of the Turret Weapon according to azimuth& elevation. 
		Deprecated. Use void applyWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition)
		*/
		VBSFUSION_DPR(UVHC033) static void setWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition);

		/*!
		@description

		used to return between 0-1 to indicate the quantity of ammunition the vehicle has compared to the full state defined by the unit type within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle which contains the ammunition. The vehicle should be created before passing it as a parameter. 

		@return float 

		@example

		@code

		//The vehicle to be created
		Vehicle veh2;
		
		displayString+="The ammo ratio of veh2 "+conversions::FloatToString(VehicleUtilities::getAmmoRatio(veh2));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the ammo ratio of  a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getAmmoRatio(Vehicle& vehicle). 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static float getAmmoRatio(const Vehicle& vehicle);

		/*!
		Sets how much ammunition (compared to a full state defined by the unit type) the vehicle has. 
		The value ranges from 0 to 1.	
		Deprecated. Use void applyVehicleAmmo(Vehicle& vehicle, double ammovalue)
		*/
		VBSFUSION_DPR(UVHC035) static void setVehicleAmmo(Vehicle& vehicle, float ammovalue);

		/*!
		Sets how much ammunition (compared to a full state defined by the unit type) the vehicle has. 
		The value ranges from 0 to 1.	
		Deprecated. Use void applyVehicleAmmo(Vehicle& vehicle, double ammovalue)
		*/
		VBSFUSION_DPR(UVHC035) static void setVehicleAmmo(Vehicle& vehicle, double ammovalue);

		/*!
		Disables gunner input at the turret.
		Deprecated. Use void applyGunnerInputDisable(Vehicle& vehicle, Turret& turret, bool status = true)
		*/
		VBSFUSION_DPR(UVHC034) static void disableGunnerInput(Vehicle& vehicle, Turret& turret, bool status = true);

		/*!
		Add weapons to the cargo space of vehicles, which can be taken out by infantry units. 
		Once the weapon cargo space is filled up, any further addWeaponCargo commands are ignored.

		Deprecated. Use void applyWeaponCargo(Vehicle& vehicle, string weaponName, int count)
		*/
		VBSFUSION_DPR(UVHC036) static void addWeaponToCargo(Vehicle& vehicle, std::string weaponName, int count);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units. 
		Once the magazine cargo space is filled up, any further addMagazineCargo commands are ignored.

		Deprecated. Use void applyMagazineCargo(Vehicle& vehicle, string magName, int count)
		*/
		VBSFUSION_DPR(UVHC037) static void addMagazineToCargo(Vehicle& vehicle, std::string magName, int count);

		/*!
		Remove all weapons from the given vehicle's weapon cargo space.
		Deprecated. Use void applyWeaponCargoRemoveAll(Vehicle& vehicle)
		*/
		VBSFUSION_DPR(UVHC038) static void clearAllWeaponFromCargo(Vehicle& vehicle);

		/*!
		Remove all magazines from the given vehicle's magazine cargo space.
		Deprecated. Use void applyMagazineCargoRemoveAll(Vehicle& vehicle)
		*/
		VBSFUSION_DPR(UVHC039) static void clearAllMagazineFromCargo(Vehicle& vehicle);

		/*!
		Enables or Disables firing on current weapon in the selected turret. If true, the unit's current 
		weapon cannot be fired.
		Deprecated. Use void applyWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe)
		*/
		VBSFUSION_DPR(UVHC040) static void setWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe);

		/*!
		
		@description

		used to return the safety of weapon in a selected turret within the VBS2 environment.

		@locality

		Locally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle which contains the weapon. The vehicle should be created before passing it as a parameter. 
		
		@param turret - The turret which enables or disables firing on current weapon.

		@return bool

		@example

		@code

		//The vehicle to be created
		Vehicle veh2;

		//The turret to be set
		Turret::TurretPath tpath2;  
		tpath2.push_back(0);
		
		Turret t2;
		
		t2.setTurretPath(tpath2);
		displayString+="The weapon safety  is "+conversions::BoolToString(VehicleUtilities::getWeaponSafety(veh2,t2));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the safety of a weapon within a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getWeaponSafety(Vehicle& vehicle, Turret& turret);
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static bool getWeaponSafety(const Vehicle& vehicle, const Turret& turret);

		/*!
		
		@description

		Used to update the armed properties of the unit within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the armed properties  of the  unit needs to be updated. The vehicle should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		VehicleUtilities::updateVehicleArmedProperties(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the armed properties of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updateVehicleArmedProperties updateVehicleArmedProperties(Vehicle& vehicle););
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateVehicleArmedProperties(Vehicle& vehicle);

		/*!
		
		@description

		Commands the vehicle to fire a given target via radio message within the VBS2 environment.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle which fires the target. The vehicle should be created before passing it as a parameter.

		@param co - the target object which is to be fired.

		@return Nothing

		@example

		@code

		//The vehicle and controllable object to be created
		Vehicle veh1;
		
		ControllableObject co;
		
		VehicleUtilities::commandFire(veh1, co);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void commandFire(Vehicle& vehicle, ControllableObject& co);

		/*!
		
		@description

		Commands the vehicle to fire a given target without radio message within the VBS2 environment.

		@locality

		Locally Applied and Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle which fires the target. The vehicle should be created before passing it as a parameter.

		@param co - the target object which is to be fired.

		@return Nothing

		@example

		@code

		//The vehicle and controllable object to be created
		Vehicle veh1;
		
		ControllableObject co;
		
		VehicleUtilities::doFire(veh1, co);	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void doFire(Vehicle& vehicle, ControllableObject& co);

		/*!
		Forces the Armed Vehicle to fire from the specifically named weapon.   
		Deprecated. Use void applyFire(Vehicle& vehicle, string weaponName)
		*/
		VBSFUSION_DPR(UVHC072) static void fire(Vehicle& vehicle, std::string weaponName);

		/*!
		Forces the Armed Vehicle to fire according to the parameters given.  
		Deprecated. Use void applyFire(Vehicle& vehicle, string muzzleName, string modeName)
		*/
		VBSFUSION_DPR(UVHC073) static void fire(Vehicle& vehicle, std::string muzzleName, std::string modeName);

		/*!
		@description

		Used to order the vehicle to fire according to the parameters within the VBS2 environment.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle which fires the target. The vehicle should be created before passing it as a parameter.  

		@param muzzleName - Name of the muzzle
		
		@param modeName - Name of the mode
		
		@param magazineName - Name of the magazine
		
		@param pos - position of the target
		
		@param aim - To aim at target before firing or not

		@return Nothing

		@example

		@code

		Vehicle v1;

		position3D pos(player.getPosition()+position3D(100,50,0));
		
		VehicleUtilities::fire(v1, "M16", "throw", "SmokeShell", pos, true);	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks  To make a   vehicle created in a client machine within a network to fire, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: fire(Vehicle& vehicle, string muzzleName, string modeName, string magazineName, position3D& pos, bool aim) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
 
		*/
		static void fire(Vehicle& vehicle, const std::string& muzzleName, const std::string& modeName, const std::string& magazineName, const position3D& pos, bool aim);

		/*!
		Apply the network shell mode. Normally shells are locally simulated following a fired event.
		When network shell mode is set, the simulation is broadcast to the network. 
		Deprecated. Use void applyNetworkShellMode(Vehicle& vehicle, bool mode)
		*/
		VBSFUSION_DPR(UVHC041) static void setNetworkShellMode(Vehicle& vehicle,bool mode);

	/*!
	
	@description

	Used to return the gunner in the vehicle within the VBS2 environment.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the gunner is present. The vehicle should be created before passing it as a parameter. 

	@return unit

	@example

	@code

	//The vehicle to be created
	Vehicle veh1
	
	//The unit object to be set
	Unit u ;
	u = VehicleUtilities::getGunner(veh1);

	displayString = "The gunner in veh1  is"+u.getName();

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

	@remarks To obtain the gunner of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getGunner(Vehicle& vehicle).
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	*/
	static Unit getGunner(const Vehicle& vehicle);

	/*!
	
	@description

	Used to return the commander in the vehicle within the VBS2 environment.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the  presence of commander is returned. The vehicle should be created before passing it as a parameter. 

	@return unit

	@example

	@code

	//The vehicle to be created
	Vehicle veh1

	//The unit object to be set 
	Unit u ;
	u = VehicleUtilities::getCommander(veh1);
	
	displayString = "The commander in veh1  is"+u.getName();

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the commander of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getCommander (Vehicle& vehicle). 
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	*/
	static Unit getCommander(const Vehicle& vehicle);

	/*!
	
	@description

	Used to return the driver of the vehicle within the VBS2 environment.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the  driver is present. The vehicle should be created before passing it as a parameter.   

	@return unit

	@example

	@code

	//The vehicle to be created
	Vehicle veh1

	//The unit object to be set 
	Unit u ;
	u = VehicleUtilities::getDriver(veh1);
	
	displayString = "The driver in veh1  is"+u.getName();

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the driver of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getDriver (Vehicle& vehicle). 
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

	*/
	static Unit getDriver(const Vehicle& vehicle);

	/*!
	
	@description

	Used to return the array which contains the detailed position of the crew in the vehicle within the VBS2 environment.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the  crew is present. The vehicle should be created before passing it as a parameter.

	@return vector<CrewPos>

	@example

	@code

	//The vehicle to be created
	Vehicle veh2
	
	vector<CrewPos> vcrewpos;
	vcrewpos = VehicleUtilities::getCrewPosition(veh2);
	
	for (vector<CrewPos>::iterator itr = vcrewpos.begin(), itr_end = vcrewpos.end(); itr != itr_end; ++itr){
		displayString+="The type of crew in veh2 are "+itr->type;
	}

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks An array is returned which contains the list of detailed crew position.The details are 
		type - (Proxy type. Can be "driver", "gunner", "commander" or "cargo".), 
		position - (Seat position in model space (i.e. relative to the vehicle)). 
		cargoID - (Index of cargo position.).unit � (Unit occupying that seat.) 
		turret - (Turret path. For non-gunner positions this element is not returned.)
 
	@remarks To obtain the positions of the crew of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getCrewPosition(Vehicle& vehicle). 
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

	*/

	static std::vector<CrewPos> getCrewPosition(const Vehicle& vehicle);


	/*!
	
	@description

	Used to return the muzzles of the turret in the vehicle  within the VBS2 environment.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the  muzzles are available. The vehicle should be created before passing it as a parameter.

	@param turretPath - The turret path in which the muzzle is located

	@return vector<string>

	@example

	@code

	//The vehicle to be created
	Vehicle veh2

	Turret::TurretPath tpath;
	tpath.push_back(0);
	tpath.push_back(1);

	Turret t;
	t.setTurretPath(tpath);

	vector<string> vmuzzles;
	vmuzzles = VehicleUtilities::getMuzzles(veh2,tpath);
	
	for (vector<string>::iterator itr = vmuzzles.begin(), itr_end = vmuzzles.end(); itr != itr_end; ++itr){
		displayString+="The muzzles position in veh2 are "+*itr+"\n";
	}

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the muzzles of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::getMuzzles(Vehicle& vehicle, Turret::TurretPath& turretPath); 
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

	*/
	static std::vector<std::string> getMuzzles(const Vehicle& vehicle, const Turret::TurretPath& turretPath);

	/*!
	
	@description

	Used to return whether the engine of the vehicle is disabled within the VBS2 environment.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the disability of engine is checked . The vehicle should be created before passing it as a parameter.

	@return bool

	@example

	@code

	//The vehicle to be created
	Vehicle veh1

	//To display the bool as a string use the function BoolToString() as shown below
	displayString="The engine is disabled in veh1"+conversions::BoolToString(VehicleUtilities::isEngineDisabled(veh1));

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To check if an engine  of a  vehicle created in a client machine within a network is diabled, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::isEngineDisabled(Vehicle& vehicle) 
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	*/
	static bool isEngineDisabled(const Vehicle& vehicle);


	/*!
	
	@description

	Used to Disable/enable the engine of a land vehicle.A disabled engine will keep the vehicle from moving, but the demage status will not be affected.Engines that have been destroyed by in-game actions (e.g. by driving through water), can also be re-enabled with this command.Disabled engines can be fixed by a repair truck. 

	@locality

	Globally Applied Globally effected

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the  engine is disabled or enabled . The vehicle should be created before passing it as a parameter. 

	@param state - true to disable the engine , false to enable it.

	@return bool

	@example

	@code

	//The vehicle to be created
	Vehicle veh1;

	VehicleUtilities::applyEngineDisable(veh1,true);

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To disable an engine  of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::applyEngineDisable(Vehicle& vehicle, bool state) (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	*/
	static void applyEngineDisable(Vehicle& vehicle, bool state);

	/*!
	
	@description

	Marks a vehicle seat as blocked, so that neither AI nor players can use it.
	Blocking used seats will not remove the occupying unit. A third optional boolean parameter can be used to unblock the seat.

	@locality

	Locally Applied Globally effected

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the seat needs to be blocked . The vehicle should be created before passing it as a parameter. 

	@param type - Type of seat (proxy): Either "Driver", "Turret", "Cargo" or "Commander"

	@return index = 0 - Index of cargo position or turret definition.
	
	@param isblock = true - Seat can be blocked or unblocked using in this parameter.

	@return Nothing
	
	@example

	@code

	//The vehicle to be created
	Vehicle veh1

	VehicleUtilities::applyBlockSeat(veh1,VEHICLEASSIGNMENTTYPE::DRIVER,0,true);

	@endcode

	@overloaded

	VehicleUtilities::applyBlockSeat(Vehicle& vehicle, vector<int> index, bool isblock = true);

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	*/
	static void applyBlockSeat(Vehicle& vehicle, VEHICLEASSIGNMENTTYPE type, int index = 0, bool isblock = true);

	/*!
	
	@description

	Marks a vehicle seat as blocked, so that neither AI nor players can use it.
	Blocking used seats will not remove the occupying unit.

	@locality

	Locally Applied Globally effected

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle � The vehicle in which the seat needs to be blocked . The vehicle should be created before passing it as a parameter. 

	@return index - Set of indexes of cargo positions or turret definition.

	@param isblock = true - Seat can be blocked or unblocked using in this parameter. 

	@return Nothing

	@example

	@code

	//The vehicle to be created
	Vehicle veh1;

	vector<int> vint;
	vint.push_back(0);
	vint.push_back(1);

	VehicleUtilities::applyBlockSeat(veh1,vint,true);

	@endcode

	@overloaded

	VehicleUtilities::applyBlockSeat(Vehicle& vehicle, VEHICLEASSIGNMENTTYPE type, int index = 0, bool isblock = true);

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	*/
	static void applyBlockSeat(Vehicle& vehicle, const std::vector<int>& index, bool isblock = true);

	/*!
	
	@description

	Used to return the list of blocked seats in the vehicle within the VBS2 environment in the form of a vector.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the seats are  blocked. The vehicle should be created before passing it as a parameter.

	@return vector<BLOCKEDSEAT>

	@example

	@code

	//The vehicle to be created
	Vehicle veh1

	vector<BLOCKEDSEAT> vector_getblocked;
	vector_getblocked= VehicleUtilities::getBlockedSeats(veh1);
	
	for (vector<BLOCKEDSEAT>::iterator itr = vector_getblocked.begin(),itr_end = vector_getblocked.end();itr!=itr_end;itr++){
		displayString = "The type of seats blocked "+itr->type;
	}

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the list of blocked seats for a vehicle  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities ::getBlockedSeats(Vehicle& vehicle). 

	*/
	static std::vector<BLOCKEDSEAT> getBlockedSeats(const Vehicle& vehicle);

	/*!
	
	@description

	Used to return the position of unit in the vehicle within the VBS2 environment. If a unit is not present in the cargo a value of -1 is returned.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param unit - The unit for which the position is required.

	@param vehicle - The vehicle in which the unit is available . The vehicle should be created before passing it as a parameter.

	@return int

	@example

	@code

	//The vehicle and unit to be created
	Vehicle veh1
	
	Unit u1;
	
	int x = VehicleUtilities::getCargoIndex(veh1,u1);
	
	displayString = "The position of the unit is"+conversions::IntToString(x);

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the position of a unit within a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getCargoIndex(Vehicle& vehicle, Unit& unit)
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	*/
	static int getCargoIndex(const Vehicle& vehicle, const Unit& unit);

	/*!
	
	@description

	Used to return the unit in the vehicle within the VBS2 environment for a specified cargo index.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the unit is available . The vehicle should be created before passing it as a parameter.

	@param index - Index value of the cargo.

	@return unit

	@example

	@code

	//The vehicle to be created
	Vehicle veh1

	Unit x = VehicleUtilities::getUnitAtCargoIndex(veh1,0);
	
	displayString = "The name of the unit  is"+x.getName();

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the unit in a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getUnitAtCargoIndex(Vehicle& vehicle, int index) 
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	*/
	static Unit getUnitAtCargoIndex(const Vehicle& vehicle, int index);

	/*!
	
	@description

	Used to Locks a turret onto an object or ASL position within the VBS2 environment. 

	@locality

	Locally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.03]

	@param vehicle - The vehicle to be locked. The vehicle should be created before passing it as a parameter.

	@param turretPath - turret to be locked.
	
	@param co - What to lock on to.

	@param trackHidden - (optional, default: false)Continue tracking unit after entering a vehicle.

	@return Nothing

	@example

	@code

	//The vehicle to be created
	Vehicle veh1;
	ControlableObject co;

	Turret::TurretPath tpath;
	tpath.push_back(0);
	tpath.push_back(1);

	Turret t;
	t.setTurretPath(tpath);

	VehicleUtilities::applyTurretLockOn(veh1,tpath,co,false);

	@endcode

	@overloaded

	VehicleUtilities::applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath turretPath, position3D& position)

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 
	
	*/
	static void applyTurretLockOn(Vehicle& vehicle, const Turret::TurretPath& turretPath, ControllableObject& co,bool trackHidden = false);

	/*!
	
	@description

	Used to Locks a turret onto an object or ASL position within the VBS2 environment. 

	@locality

	Locally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.03]

	@param vehicle - The vehicle to be locked. The vehicle should be created before passing it as a parameter. 

	@param turretPath - turret to be locked.

	@param position - What to lock on to. Locking can be disabled by passing [0,0,0].
	
	@return Nothing

	@example

	@code

	Vehicle veh1;

	Turret::TurretPath tpath;
	tpath.push_back(0);
	tpath.push_back(1);

	Turret t;
	t.setTurretPath(tpath);

	VehicleUtilities::applyTurretLockOn(veh1,tpath,position3D (0,0,0));

	@endcode

	@overloaded

	VehicleUtilities::applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath turretPath, ControllableObject& co,bool trackHidden = false);

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	*/
	static void applyTurretLockOn(Vehicle& vehicle, const Turret::TurretPath& turretPath, const position3D& position);


	/*!
	
	@description

	Used to return the position of the object if the turret is locked on an object. [0,0,0] if it is not locked within the VBS2 environment.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.04]

	@param vehicle - The vehicle in which the turret is locked. The vehicle should be created before passing it as a parameter. 

	@param turretPath - turret to be locked

	@return position3D

	@example

	@code

	//The vehicle be created
	Vehicle veh1
	
	//To display the position as a string use the function getVBSPosition () as shown below
	displayString += "The turret locked on is "+VehicleUtilities::getTurretLockedOn(veh1,tpath).getVBSPosition();

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the above position in a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getTurretLockedOn(Vehicle& vehicle, Turret::TurretPath turretPath) (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	*/
	static position3D getTurretLockedOn(const Vehicle& vehicle, const Turret::TurretPath& turretPath);

	/*!
	
	@description

	Used to return the current laser range of the gunner within the VBS2 environment. -1 is returned if the gunner is not operating a range finder weapon.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.03]

	@param vehicle - The vehicle in which the gunner is present. The vehicle should be created before passing it as a parameter.

	@param gunner - the gunner whose operating the weapon. 

	@return double

	@example

	@code

	//The vehicle and unit to be created
	Vehicle veh1
	Unit gunner
	
	displayString = "the gunner range is "+conversions::DoubleToString(VehicleUtilities::getLaserRange(veh1,gunner));

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the laser range of a gunner within a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities ::getLaserRange(Vehicle& vehicle, Unit& gunner); 
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	*/
	static double getLaserRange(const Vehicle& vehicle, const Unit& gunner);

	/*!
	
	@description

	This function overrides the actual laser measurements (which are used to calculate the ballistic calculations) with a value provided by this function. This value will be shown in the gunner's optics view, until he does another manual lasing.

	@locality

	Globally applied Locally effected

	@version [VBS2 2.02 / VBS2Fusion v3.03]

	@param vehicle - The vehicle in which the gunner is present. The vehicle should be created before passing it as a parameter. 

	@param gunner - Unit that is manning the affected weapon. 

	@param range - Distance (in meters) to be used in ballistics calculations.
	
	@return Nothing

	@example

	@code

	//The vehicle  and unit to be created
	Vehicle veh2;
	Unit gunner;
	
	VehicleUtilities::applyLaserRange(veh2,gunner,5000);	

	@endcode

	@overloaded

	VehicleUtilities::applyLaserRange(Vehicle& vehicle, Unit& gunner);

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To apply the laser range for  a  vehicle created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyLaserRange(Vehicle& vehicle, Unit& gunner, double range) (Refer (MissionUtilities::loadMission(Mission& mission)for further information. 

	*/
	static void applyLaserRange(Vehicle& vehicle, Unit& gunner, double range);

	/*!
	
	@description

	Sets a new offset between weapon orientation and optics. Lasing will change the elevation offset.This function works only if the gunner is the player.

	@locality

	Locally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.03]

	@param vehicle - The vehicle for which the optic offset is applied. The vehicle should be created before passing it as a parameter. 
	
	@param turretPath - Turret path

	@param azimuth -  Angular measurement
	
	@param elevation -  The elevation in number

	@param transition -  Boolean (optional, default: true)

	@return Nothing

	@example

	@code

	//The vehicle  to be created
	Vehicle veh1;
	
	Turret::TurretPath tpath;
	tpath.push_back(0);
	tpath.push_back(1);

	Turret t;
	t.setTurretPath(tpath);

	VehicleUtilities::applyOpticsOffset(veh1,tpath, 0.4,0.5,true);

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	*/
	static void applyOpticsOffset(Vehicle& vehicle, const Turret::TurretPath& turretPath, double azimuth, double elevation, bool transition = true);

	/*!
	
	@description

	used to return the offset between weapon orientation and optics within the VBS2 environment.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.03]

	@param vehicle - The vehicle in which the gunner is present. The vehicle should be created before passing it as a parameter.

	@param gunner - The gunner who is in the vehicle.

	@return vector<double>

	@example

	@code

	//The vehicle  and unit to be created
	Vehicle veh1;
	Unit gunner;
	
	vector<double> optic_offset;
	optic_offset = VehicleUtilities::getOpticsOffset(veh1,gunner);

	for (vector<double>::iterator itr = optic_offset.begin(),itr_end = optic_offset.end();itr!=itr_end;itr++){
		displayString += "The optic offset is"+conversions::DoubleToString(*itr)+"\n";
	}

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the offset between weapon orientation and optics of a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function getOpticsOffset(Vehicle& vehicle, Unit& gunner) (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	*/
	static std::vector<double> getOpticsOffset(const Vehicle& vehicle, const Unit& gunner);

	/*!
	
	@description

	Used to return the current status of laser range finder (LRF) (corresponds with the in-game status color):
		0 - lasing not available (unit or vehicle position not equipped with LRF)
		1 - [red]: not lased (distance cannot be determined, e.g. water, air)
		2 - [yellow]: lasing in progress (distance is being determined)
		3 - [green]: lased (distance has been determined and displayed) 

	@locality

	Globally Applied 

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the gunner is present. The vehicle should be created before passing it as a parameter. 

	@param gunner - The gunner who is in the vehicle.

	@return int

	@example

	@code

	//The vehicle to be created
	Vehicle Veh1;
	
	//The gunner to be created
	Unit gunner;
	
	displayString += "The status of laser range is"+conversions::IntToString(VehicleUtilities::getLasingStatus(veh1,gunner));

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks remarks  To obtain the current laser of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getLasingStatus(Vehicle& vehicle, Unit& gunner) 
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
	
	*/
	static int getLasingStatus(const Vehicle& vehicle, const Unit& gunner);

	/*!
	
	@description

	Used to control the  turn indicators and hazard lights on vehicles (true=on, false=off) within the VBS2 environment.

	@locality

	Locally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle for which the indicators needs to be changed. The vehicle should be created before passing it as a parameter. 

	@param hazard -  Turns hazard lights on and off. (If true, indicator light will be turned off.)

	@param left - Turns left indicator lights on and off. (If true, hazard light will be turned off.)
	
	@param right -  Turns right indicator lights on and off. (If true, hazard light will be turned off.)
	
	@return Nothing

	@example

	@code

	//Veh1 to be created
	Vehicle Veh1;
	
	VehicleUtilities::applyIndicators(veh1,false,true,false);

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).  
	
	*/
	static void applyIndicators(Vehicle& vehicle, bool left, bool hazard, bool right);

	/*!
	
	@description
	
	Used to set the new fording depth in meters for objects of the classes motorcycle, helicopter, tank and car. Setting the max fording depth to -1 will cause the engine to revert to the default configuration value.
	
	@locality
	
	Locally Applied
	
	@version [VBS2 2.02 / VBS2Fusion v3.02]
	
	@param vehicle - The vehicle for which the new fording depth in meters is set. The vehicle should be created before passing it as a parameter.
	
	@param depth - fording depth in meters for the vehicle
	
	@return Nothing
	
	@example
	
	@code
	
	VehicleUtilities::applyMaxFordingDepth(veh1,120 );
	
	@endcode
	
	@overloaded
	
	None
	
	@related
	
	None
	
	@remarks It is highly recommended to create objects using its respective classes for @localityive results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

	*/
	static void applyMaxFordingDepth(Vehicle& vehicle, double depth);

	/*!
	
	@description

	Used to return the maximum depth of the vehicle within the VBS2 environment.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle for which the maximum fording depth is returned. The vehicle should be created before passing it as a parameter. 

	@return double

	@example

	@code

	//The vehicle  to be created
	Vehicle veh1;

	//To display the double as a string use the function DoubleToString() as shown below
	displayString += "The fording depth of a vehicle is"+conversions::DoubleToString(VehicleUtilities::getMaxFordingDepth(veh1));

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the maximum depth of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getMaxFordingDepth(Vehicle& vehicle); 
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	*/
	static double getMaxFordingDepth(const Vehicle& vehicle);

	/*!
	
	@description

	Used to return the gunner in the turret path specified within the VBS2 environment.

	@locality

	Globally Applied

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param vehicle - The vehicle in which the gunner is present. The vehicle should be created before passing it as a parameter. 

	@param turretPath - Turret path

	@return unit

	@example

	@code

	//The vehicle to be created
	Vehicle veh1
	
	//The unit to be set
	Unit u1;
	Turret::TurretPath tpath;
	tpath.push_back(0);
	tpath.push_back(1);
	
	Turret t;
	t.setTurretPath(tpath);
	
	u1 = VehicleUtilities::getTurretUnit(veh1,tpath);
	displayString ="The unit in the turret is "+u1.getName();

	@endcode

	@overloaded

	None

	@related

	None

	@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To obtain the gunner in the turret  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getTurretUnit(Vehicle& vehicle, Turret::TurretPath turretPath) 
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	*/
	static Unit getTurretUnit(const Vehicle& vehicle, const Turret::TurretPath& turretPath);

	/*!
	Brings an aircraft to a flying altitude. Either planes or helicopters will take off realistically to reach this height
	Deprecated. Use double applyAirborne(Vehicle& vehicle)
	*/
    VBSFUSION_DPR(UVHC042) static double makeAirborne(Vehicle& vehicle);
 
	 /*!
	 
	 @description

	 Used to return a boolean value to check  whether the aircraft  is currently airborne or not within the VBS2 environment.

	 @locality

	 Globally Applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param vehicle - The vehicle for which the above function needs to be worked. The vehicle should be created before passing it as a parameter.

	 @return bool

	 @example

	 @code

	 //The vehicle to be created 
	 Vehicle veh1;
	 
	 //To display the bool as a string use the function BoolToString() as shown below
	 displayString="The vehicle1 is airborne"+conversions::BoolToString(VehicleUtilities::isAirborne(veh1));

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To check if a  vehicle created in a client machine within a network is airborne, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::isAirborne(Vehicle& vehicle); 
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

	 */
	 static bool isAirborne(const Vehicle& vehicle);


	 /*!
	 
	 @description

	 Used to return a boolean value to check  whether the light of the vehicle is on or off within the VBS2 environment.

	 @locality

	 Globally Applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param vehicle - The vehicle for which the status of light in the vehicle is checked. The vehicle should be created before passing it as a parameter.

	 @return bool

	 @example

	 @code

	 //The vehicle to be created 
	 Vehicle veh1;
	
	 //To display the bool as a string use the function BoolToString() as shown below
	 displayString+="The vehicle1 light is on"+conversions::BoolToString(VehicleUtilities::isLightOn(veh1));

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To check the status of a light of a   vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::isLightOn(Vehicle& vehicle) (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	 */
	 static bool isLightOn(const Vehicle& vehicle);

	 /*!
	 
	 @description

	 Used to return either the default fly-in height of aircraft (independently of its current altitude), or the value that was set via flyInHeight within the VBS2 environment.

	 @locality

	 Globally Applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param vehicle - The vehicle for which the fly in height is obtained. The vehicle should be created before passing it as a parameter. 

	 @return bool

	 @example

	 @code

	 //The vehicle  to be created
	 Vehicle veh1
	 
	 //To display the double as a string use the function DoubleToString() as shown below
	 displayString+="The fly in height of vehicle1"+conversions::DoubleToString(VehicleUtilities::getFlyInHeight(veh1));

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To obtain the fly in height of a    vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getFlyInHeight(Vehicle& vehicle); 
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	 */
	 static double getFlyInHeight(const Vehicle& vehicle);


	 /*!
	 Sets the flying altitude for aircraft.
	 Avoid too low altitudes, as helicopters and planes won't evade trees and obstacles on the ground.
	 The default flying altitude depends on the "flyInHeight" definition in the vehicle's configuration. 

	 Deprecated. Use void applyHeightFlyIn(Vehicle& vehicle, double height)
	 */
	 VBSFUSION_DPR(UVHC043) static void flyInHeight(Vehicle& vehicle, double height);

	 /*!
	 Set object's orientation (given as direction and up vector).
	 Since version +1.30 accepts additional parameter which, when object is attached, 
	 specifies if vectors applied are relative to parent object's orientation.  

	 Deprecated. Use void applyVectorDirAndUp(Vehicle veh, vector3D vectorDir, vector3D vectorUp, bool relative)
	 */
	 VBSFUSION_DPR(UVHC044) static void setVectorDirAndUp(Vehicle veh,vector3D vectorDir, vector3D vectorUp, bool relative);

	 /*!
	
	 @description

	 Used to set the passed thrust wanted for the left track of entity for only tracked vehicles within the VBS2 environment . Vehicle needs to have the controller enabled via applyManualControl().

	 @locality

	 Locally Applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param vehicle - The vehicle which is controlled. The vehicle should be created before passing it as a parameter. 

	 @param factor - Factor to change the thrust of the vehicle.

	 @return Nothing

	 @example

	 @code

	 //The vehicle to be created
	 Vehicle veh1
	 
	 VehicleUtilities::setManualControl(veh1,true);
	 
	 VehicleUtilities::applyThrustLeftWanted(veh1,1);

	 @endcode

	 @overloaded

	 None

	 @related

	 VehicleUtilities::setManualControl(Vehicle& vehicle, bool control);

	 @remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 */
	 static void applyThrustLeftWanted(Vehicle& vehicle, double factor);

	 /*!
	
	 @description

	 Used to set the passed thrust wanted for the Right track of entity for only tracked vehicles within the VBS2 environment . Vehicle needs to have the controller enabled via applyManualControl().

	 @locality

	 Locally Applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param vehicle - The vehicle which is controlled. The vehicle should be created before passing it as a parameter. 

	 @param factor - Factor to change the thrust of the vehicle.

	 @return Nothing

	 @example

	 @code

	 //The vehicle to be created
	 Vehicle veh2
	 
	 VehicleUtilities::setManualControl(veh2,true);
	 
	 VehicleUtilities::applyThrustRightWanted(veh2,1);

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 */
	 static void applyThrustRightWanted(Vehicle& vehicle, double factor);

	 /*!
	
	 @description

	 Used to set the watch direction and firing arc. In the scenario the AI gunner will only watch and engage targets within the allocated arc. It sets for a turrets gunner specified in parameters.

	 @locality

	 Globally Applied Globally effected

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param vehicle - The vehicle for which the fire arc  function is applied. The vehicle should be created before passing it as a parameter. 

	 @param turretPath - Turret path
	 
	 @param direction - Direction vector, acts as center of current arc

	 @param sideRange - Horizontal width of arc (left/right)

	 @param verticalRange - Vertical width of arc (up/down)

	 @param relative - Sets if direction vector is relative to vehicle orientation
	 
	 @return Nothing

	 @example

	 @code

	 //The vehicle  to be created
	 Vehicle veh1
	 
	 //The turret to be set
	 Turret::TurretPath tpath;
	 tpath.push_back(0);
	 tpath.push_back(1);

	 Turret t;
	 t.setTurretPath(tpath);

	 VehicleUtilities::applyFireArc(veh1, tpath, vector3D (0,1,0),45, 62, true);

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To set a firing arc  for a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyFireArc(Vehicle& vehicle, Turret::TurretPath& turretPath, vector3D& direction, double sideRange, double verticalRange, bool relative)
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

	 */
	 static void applyFireArc(Vehicle& vehicle, const Turret::TurretPath& turretPath, const vector3D& direction, double sideRange, double verticalRange, bool relative);

	 /*!
	 Force aircraft landing.

	 Planes will use the airport defined by assignToAirport, or the one closest to them (in this priority).
	 Helicopters will use either the nearest editor-placed helipad, or the nearest flat location (in this priority). assignToAirport has no effect on helicopters, and either of them have to be within 500m, or it will not land.
	 
	 Deprecated. Use void applyCustomizedLanding(Vehicle& vehicle, LANDMODE mode)
	 */
	 VBSFUSION_DPR(UVHC045) static void land(Vehicle& vehicle, LANDMODE mode);

	 /*!
	 Send message to vehicle radio channel. 
	 Deprecated. Use void applyVehicleRadioBroadcast(Vehicle& vehicle, string message)
	 */
	 VBSFUSION_DPR(UVHC048) static void sendToVehicleRadio(Vehicle& vehicle, std::string name);

	 /*!
	 Type text to vehicle radio channel.

	 This function only types text to the list, it does not broadcast the message. If you want the message to show on all computers, you have to execute it on them.

	 Object parameter must be a vehicle, not a player.
	 If you are in a crew seat (i.e. driver, gunner or commander), then it will include that role in the chat name output (Eg: Driver (you_name): "Message"). 
	
	 Deprecated. Use void applyVehicleChat(Vehicle& vehicle, string message)
	 */
	 VBSFUSION_DPR(UVHC049) static void vehicleChat(Vehicle& vehicle, std::string message);

	 /*!
	 
	 @description

	 Used to return a boolean value true if the vehicle engine is on or false when it is is off.

	 @locality

	 Globally Applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param vehicle - The vehicle for which the status of engine is required. The vehicle should be created before passing it as a parameter. 

	 @return bool 

	 @example

	 @code

	 //The vehicle to be created
	 Vehicle veh1
	 
	 //To display the bool as a string use the function BoolToString() as shown below
	 displayString+="The vehicle1 is engine on"+conversions::BoolToString(VehicleUtilities::isEngineOn(veh1));

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To check if the status of an  engine  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: isEngineOn(Vehicle& vehicle)
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

	 */
	 static bool isEngineOn(const Vehicle& vehicle);
	 
	 /*!
	 
	 @description

	 Used to return a boolean value true if the given unit is turned  out from a vehicle hatch within the VBS2 environment.

	 @locality

	 Globally Applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit - The unit for which the above function needs to be applied.

	 @return bool

	 @example

	 @code

	 //The unit to be created 
	 Unit u ;
	
	 displayString+="vehicle1 is turned out"+conversions::BoolToString(VehicleUtilities::isTurnedOut(u));	

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To check if a unit is out of the hatch of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: isTurnedOut (Vehicle& vehicle,  ) 
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	 */
	 static bool isTurnedOut(const Unit& unit);

	 /*!
	 Assigns a unit as commander of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsCommander(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 VBSFUSION_DPR(UVHC019) static void assignAsCommander(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as driver of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsDriver(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 VBSFUSION_DPR(UVHC021) static void assignAsDriver(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as gunner of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsGunner(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 VBSFUSION_DPR(UVHC022) static void assignAsGunner(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsCargo(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 VBSFUSION_DPR(UVHC024) static void assignAsCargo(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as commander of a vehicle. Used together with orderGetIn to order a unit to get in as commander.	 

	 Deprecated. Use void applyAssignAsCommander(Vehicle& vehicle, Unit& unit)
	 */
	 VBSFUSION_DPR(UVHC015) static void assignAsCommander(Vehicle& vehicle, Unit& unit);

	 /*!
	 Assigns a unit as driver of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 Deprecated. Use void applyAssignAsDriver(Vehicle& vehicle, Unit& unit)
	 */
	 VBSFUSION_DPR(UVHC016) static void assignAsDriver(Vehicle& vehicle, Unit& unit);

	 /*!
	 Assigns a unit as gunner of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 Deprecated. Use void applyAssignAsGunner(Vehicle& vehicle, Unit& unit)
	 */
	 VBSFUSION_DPR(UVHC017) static void assignAsGunner(Vehicle& vehicle, Unit& unit);

	 /*!
	 Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 Deprecated. Use void applyAssignAsCargo(Vehicle& vehicle, Unit& unit)
	 */
	 VBSFUSION_DPR(UVHC018) static void assignAsCargo(Vehicle& vehicle, Unit& unit);

	 /*!
	 Force all units in the array to get in or out of their assigned vehicles. Units must be assigned to a vehicle before this command will do anything. 

	 Deprecated. Use void applyGetIn(vector<Unit> unitVec, bool order)
	 */
	 VBSFUSION_DPR(UVHC013) static void orderGetIn(std::vector<Unit> unitVec, bool order);

	 /*!
	 Force unit to get in or out of their assigned vehicles. Unit must be assigned to a vehicle before this command will do anything. 

	 Deprecated. Use void applyGetIn(Unit& unit, bool order)
	 */
	 VBSFUSION_DPR(UVHC014) static void orderGetIn(Unit& unit, bool order);

		/*!
		
		@description

		Used to return a list of weapons in the cargo of the vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - the vehicle in which the cargo is present. The vehicle should be created before passing it as a parameter. 

		@return vector<WEAPONCARGO> 

		@example

		@code

		//Vehicle to be created
		Vehicle veh1
		
		vector<WEAPONCARGO> weapon_cargo = VehicleUtilities::getWeaponFromCargo(veh1);
		
		for (vector<WEAPONCARGO>::iterator itr = weapon_cargo.begin(),itr_end = weapon_cargo.end();itr!=itr_end;itr++){
			displayString += "The name of the weapons are"+itr->weaponName;
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the list of weapons in the cargo of a vehicle created in a client machine within a network, use the function  
		MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getWeaponFromCargo(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::vector<WEAPONCARGO> getWeaponFromCargo(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return a list of magazines in the cargo of the vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - the vehicle in which the cargo is present. The vehicle should be created before passing it as a parameter.

		@return vector<MAGAZINECARGO>

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		vector<MAGAZINECARGO> mag_cargo = VehicleUtilities::getMagazineFromCargo(veh1);
		
		for (vector<MAGAZINECARGO>::iterator mitr = mag_cargo.begin(),mitr_end = mag_cargo.end();mitr!=mitr_end;mitr++){
			displayString += "The name of the magazines are"+mitr->magazineName;
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To return a list of magazines in the cargo  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  
		clients in the network before calling the function VehicleUtilities:: getMagazineFromCargo(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
 
		*/
		static std::vector<MAGAZINECARGO> getMagazineFromCargo(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return the muzzle of the vehicle  within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle in which the muzzle is present. The vehicle should be created before passing it as a parameter. 

		@return vector<string>

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		vector<string> muz = VehicleUtilities::getMuzzles(veh1);
		
		for (vector<string>::iterator itr = muz.begin(),itr_end = muz.end();itr!=itr_end;++itr){
			displayString += "The name of the muzzles are"+(*itr)+"\n";
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To return a list of muzzles of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getMuzzles(Vehicle& vehicle)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::vector<std::string> getMuzzles(const Vehicle& vehicle);

		
		/*!
		
		@description

		Used to return the position the shooter  has to aim in order to hit the target with the selected weapon  within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle in which the unit aims the target. The vehicle should be created before passing it as a parameter.

		@param target - The target object to be shot.

		@return position3D 

		@example

		@code

		displayString+="The aiming position is "+VehicleUtilities::getAimingPosition(veh1, co).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the shooting position  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getMuzzles(Vehicle& vehicle)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getAimingPosition(const Vehicle& vehicle, const ControllableObject& target);


		/*!
		It is only for local objects
		Order the given unit(s) to move to the given location (via the radio). 
		Exactly the same as doMove, except this command displays a radio message. 

		Deprecated. Use void applyCommandMove(Vehicle& vehicle, position3D position) 
		*/
		VBSFUSION_DPR(UVHC071) static void commandMove(Vehicle& vehicle, position3D position);

		/*!
		
		@description

		Used to return the force speed of within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the force speed is required. The vehicle should be created before passing it as a parameter. 

		@return double 

		@example

		@code

		//Vehicle to be created 
		Vehicle veh1;
		
		displayString+="The force speed of veh1"+conversions::DoubleToString(VehicleUtilities::getForceSpeed(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the force speed  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getForceSpeed(Vehicle& vehicle)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
 
		*/
		static double getForceSpeed(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return the parameters of the fire arc of the vehicle  for the specified turret path within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the fire arc parameters are obtained. The vehicle should be created before passing it as a parameter. 

		@param turretPath - The turret path for which the fire arc parameters are obtained

		@return FIREARC 

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1

		Turret::TurretPath tpath;
		tpath.push_back(0);
		tpath.push_back(1);

		Turret t;
		t.setTurretPath(tpath);

		FIREARC fire_arc = VehicleUtilities::getFireArcParameters(veh1,tpath);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the fire arc parameters of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getFireArcParameters(Vehicle& vehicle, Turret::TurretPath& turretPath); 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static FIREARC getFireArcParameters(const Vehicle& vehicle, const Turret::TurretPath& turretPath);

		/*!
		
		@description

		This function enables the player to steer the vehicle even if it is not a driver. A true value set satisfies this.

		@locality

		Locally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param overRide - It is a Boolean value

		@return Nothing

		@example

		@code

		VehicleUtilities::applyDriverOverride(true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks An AI driver has to be switched off manually with disable AIMOVE(); as this is not working for planes, but make sure that the pilot is local to the player's client. 

		*/
		static void applyDriverOverride(bool overRide);

		/*!
		Deletes the specified weapon from the cargo of a vehicle. Does nothing if the weapon isn't actually in vehicle's cargo. 
		Deprecated. Use void applyWeaponCargoRemove(Vehicle& vehicle, string weapon)
		*/
		VBSFUSION_DPR(UVHC050) static void removeWeaponFromCargo(Vehicle& vehicle, std::string weapon);

		/*!
		Deletes the specified magazine from the cargo of a vehicle. Does nothing if the magazine isn't actually in vehicle's cargo. Magazines are removed in order from fullest to emptiest.
		Deprecated. Use void applyMagazineCargoRemove(Vehicle& vehicle, string magazine)
		*/
		VBSFUSION_DPR(UVHC051) static void removeMagazineFromCargo(Vehicle& vehicle, std::string magazine);

		/*!
		Removes the magazine from vehicles turret
		Deprecated. Use void applyMagazineTurretRemove(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath)
		*/
		VBSFUSION_DPR(UVHC052) static void removeMagazineFromTurret(Vehicle& vehicle, std::string magazine, Turret::TurretPath& turretPath);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units.
		Deprecated. Use void applySingleMagazineCargo(Vehicle& vehicle, string magazineName, int bulletCount)
		*/
		VBSFUSION_DPR(UVHC053) static void addMagazineCargoEx(Vehicle& vehicle, std::string magazineName, int bulletCount);

		/*!
		
		@description

		This function sets the armor state of the vehicle within the VBS2 environment (a value from 0 to 1).

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - the vehicle to which the armor is applied. The vehicle should be created before it is passed as a parameter.

		@param armor - The state of the armor.

		@return Nothing

		@example

		@code

		//vehicle to be created 
		Vehicle veh1;
		
		VehicleUtilities::applyVehicleArmor(veh1,0.45);	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyVehicleArmor(Vehicle& vehicle, double armor);

		/*!
		Sets the vehicle formation direction
		*/
		static void applyFormationDirection(Vehicle& vehicle, double direction);
		//---------------------------------------------------------------------------

		/*!
		
		@description

		Used to check if the vehicle object within the VBS2 environment  is a subtype  of  the given type. It returns a boolean value true if this is satisfied.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the subtype is checked. The vehicle object should be created before the parameter is passed.

		@param typeName - Name of the type.

		@return bool

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the bool as a string use the function BoolToString() as shown below
		displayString+="Is kind"+conversions::BoolToString(VehicleUtilities::isKindOf(veh1,"tank"));	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check the kind  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::applyEngineDisable(Vehicle& vehicle,  bool state) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool isKindOf(const Vehicle& vehicle, const std::string& typeName);

		/*!
		Check if (and by how much) vehicle commander knows about target.
		And returns a number from 0 to 4.
		Deprecated. Use double getKnowsAbout(Vehicle& vehicle, ControllableObject& target)
		*/		
		VBSFUSION_DPR(UVHC054) static float knowsAbout(Vehicle& vehicle, ControllableObject& target);


		/*!
		Check if (and by how much) vehicle commander knows about target.
		And returns a number from 0 to 4.
		Deprecated. Use double getKnowsAbout(Vehicle& vehicle, ControllableObject& target)
		*/		
		VBSFUSION_DPR(UVHC054) static double knowsAboutEx(Vehicle& vehicle, ControllableObject& target);


		/*!
		
		@description

		This function is to apply a valid name for the vehicle in the VBS2 environment. Prior to applying the function verifies that the name is  valid according to following criteria:
			- Name has to start with letter or underscore.
			- The only special character allowed is underscore.
			- Name should not contain spaces in between.
			- Existing names in the mission should not be used.

		@locality

		Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the name is applied. The vehicle object should be created before the parameter is passed.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		string name = "veh1";
		veh1.setName(name);
		
		VehicleUtilities::applyName(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyName(Vehicle& vehicle);

		/*!
		
		@description

		Used to return the settings defined by  the setNetworkShell of the vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the network shell is required. The vehicle object should be created before the parameter is passed.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1
		
		displayString+="The networks shell for veh1 is"+conversions::BoolToString(VehicleUtilities::getNetworkShell(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obatin the network shell  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getNetworkShell (Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool getNetworkShell (const Vehicle& vehicle);

		/*!
		Lock or Unlock the vehicle for player. 
		Deprecated. Use void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock)
		*/
		VBSFUSION_DPR(UVHC055) static void lockVehicle(Vehicle& vehicle, bool playerLock);

		/*!
		Lock or Unlock the vehicle for player. 
		Deprecated. Use void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock, bool allLock)
		*/
		VBSFUSION_DPR(UVHC056) static void lockVehicle(Vehicle& vehicle, bool playerLock, bool allLock);

		/*!
		
		@description

		This function is used to check if the unit is mounted to the vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle which is to be checked, if the unit is mounted to it. The vehicle object should be created before the parameter is passed.

		@param unit - The unit which is checked for its presence in the vehicle.
		
		@return bool

		@example

		@code

		//The vehicle  to be created: 
		Vehicle Veh1;
		
		//The unit to be created
		Unit u;
		displayString+="The unit is in veh1 is"+conversions::BoolToString(VehicleUtilities::isInVehicle(veh1,u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check if a unit is in a   vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: isInVehicle(Vehicle& vehicle, Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool isInVehicle(const Vehicle& vehicle, const Unit& unit);

		/*!
		
		@description

		This function returns the formation leader of the vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the formation leader is obtained. The vehicle object should be created before the parameter is passed.

		@return unit

		@example

		@code

		//Vehicle to be created
		Vehicle veh1;

		//Unit to be set
		u2 = VehicleUtilities::getFormLeader(veh1);
		displayString +="The formation leader is "+u2.getName();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the formation of leader in a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: getFormLeader(Vehicle& vehicle); 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static Unit getFormLeader(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return the precision of the vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the precision is required. The vehicle object should be created before the parameter is passed.

		@return float

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the float as a string use the function FloatToString() as shown below
		displayString+="The precision of the entity is"+conversions::FloatToString(VehicleUtilities::getPrecision(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the precision  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getPrecision(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static float getPrecision(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return the precision of the vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the precision is required. The vehicle object should be created before the parameter is passed.

		@return double

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the double as a string use the function DoubleToString () as shown below
		displayString+="The precision of the entity is"+conversions:: DoubleToString (VehicleUtilities::getPrecision(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the precision  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getPrecisionEx (Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getPrecisionEx(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return the assigned target for the vehicle within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the assigned target object is required. The vehicle object should be created before the parameter is passed.

		@return ControllableObject

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1 
		
		//The Controllable object to be set
		ControllableObject co;

		co = VehicleUtilities::getAssignedTarget(veh1);	
		displayString="The target is"+coe.getName();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static ControllableObject getAssignedTarget(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return the current position of a selection in model space.If a selection does not exist [0,0,0] is returned.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the selection position is obtained. The vehicle object should be created before the parameter is passed.

		@param selectName - Possible values for cars include'
			door_1_1, door_1_2, door_2_1,door_2_2, driveWheel,
			hatch_1_1,hatch_1_2,hull,light_1_1,light_1_2, light_back, light_brake, light_reverse, light_turnL,
			light_turnR, main_trav,swap_hmmwv_body, swap_hmmwv_hood, swap_hmmwv_parts, swap_hmmwv_regular, tailgate,
			trunk, unit_callsign, unit_logo, wheel_1_1, wheel_1_2, wheel_2_1, wheel_2_2

			Possible values for Tanks include,
			cmdr_elev, cmdr_trav, hatch_commander, hatch_driver, hatch_gunner, hull, light_1_1, light_cmdr,
			light_turret, main_elev, main_elev_recoil, main_muzzleflash, main_trav, swap_1, swap_2, swap_3,
			track_1,track_2,unit_callsign,unit_Iogo, wheel_1_1, wheel_1_2, wheel_2_1, wheel_2_1_damper,
			wheel_2_2, wheel_2_2_damper, wheel_3_1, wheel_3_1_damper, wheel_3_2, wheel_3_2_damper,wheel_4_1,
			wheel_4_1_damper, wheel_4_2, wheel_4_2_damper, wheel_5_1, wheel_5_1_damper, wheel_5_2, wheel_5_2_damper,
			wheel_6_1, wheel_6_1_damper, wheel_6_2, wheel_6_2_damper, wheel_7_1, wheel_7_1_damper, wheel_7_2,
			wheel_7_2_damper, wheel_8_1, wheel_8_2

			Possible values for Helicopters include,
			damageHide, damper_rear, dampers, dashboard, door_1_1, door_1_2, door_2_1, door_2_2, doorgun_1_barrels,
			doorgun_1_elev, doorgun_1_trav, doorgun_2_barrels, doorgun_2_elev, doorgun_2_trav, elevator, hull, mainRotor,
			mainRotorBlurred, mainRotorStatic, rotorShaft, taiIRotor, taiIRotorBlurred, taiIRotorStatic, unit_callsign,
			unit_logo

			Possible values for Planes include, 
			aileron_1, aileron_2, canopy, elevator_1, elevator_2, engine_1_glow_1, engine_1_glow_2, engine_1_glow_3,
			engine_1_nozzle_1, engine_1_nozzle_2, engine_1_nozzle_3, engine_1_nozzle_4,	engine_1_nozzle_5, engine_1_nozzle_6,
			engine_1_nozzle_7, engine_1_nozzle_8, engine_2_glow_1, engine_2_glow_2, engine_2_glow_3, engine_2_nozzle_1,
			engine_2_nozzle_2, engine_2_nozzle_3, engine_2_nozzle_4, engine_2_nozzle_5, engine_2_nozzle_6, engine_2_nozzle_7,
			engine_2_nozzle_8,	engine_afterburner_O engine_afterburner_1, engine_afterburner_2, engine_afterburner_3,
			engine_afterburner_4, flap_1, flap_2, gear_1, gear_1_hatch_1_1, gear_1_hatch_1_2, gear_1_hatch_2, gear_2_1,
			gear_2_1_hatch_1, gear_2_1_hatch_2, gear_2_2, gear_2_2_hatch_1, gear_2_2_hatch_2, hull, PropBlurred, rudder_1,
			rudder_2, Trail_1, Trail_2, Trail_3, Trails, unit_callsign, unit_logo, wheel_1, wheel_2_1, wheel_2_2

		@return position3D

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the position3D as a string use the function getVBSPosition () as shown below
		displayString+="The current position of a selection"+VehicleUtilities::getSelectionPosition(veh1,"hull").getVBSPosition();

		@endcode

		@overloaded

		VehicleUtilities::getSelectionPosition(Vehicle& vehicle, string selectName, string method);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the selection poisition  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getSelectionPosition(Vehicle& vehicle, string selectName) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static position3D getSelectionPosition(const Vehicle& vehicle, const std::string& selectName);

		/*!
		
		@description

		Used to return the current position of a selection in model space.If a selection does not exist [0,0,0] is returned.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the selection position is obtained. The vehicle object should be created before the parameter is passed.

		@param selectName - Possible values for cars include'
			door_1_1, door_1_2, door_2_1,door_2_2, driveWheel,
			hatch_1_1,hatch_1_2,hull,light_1_1,light_1_2, light_back, light_brake, light_reverse, light_turnL,
			light_turnR, main_trav,swap_hmmwv_body, swap_hmmwv_hood, swap_hmmwv_parts, swap_hmmwv_regular, tailgate,
			trunk, unit_callsign, unit_logo, wheel_1_1, wheel_1_2, wheel_2_1, wheel_2_2

			Possible values for Tanks include,
			cmdr_elev, cmdr_trav, hatch_commander, hatch_driver, hatch_gunner, hull, light_1_1, light_cmdr,
			light_turret, main_elev, main_elev_recoil, main_muzzleflash, main_trav, swap_1, swap_2, swap_3,
			track_1,track_2,unit_callsign,unit_Iogo, wheel_1_1, wheel_1_2, wheel_2_1, wheel_2_1_damper,
			wheel_2_2, wheel_2_2_damper, wheel_3_1, wheel_3_1_damper, wheel_3_2, wheel_3_2_damper,wheel_4_1,
			wheel_4_1_damper, wheel_4_2, wheel_4_2_damper, wheel_5_1, wheel_5_1_damper, wheel_5_2, wheel_5_2_damper,
			wheel_6_1, wheel_6_1_damper, wheel_6_2, wheel_6_2_damper, wheel_7_1, wheel_7_1_damper, wheel_7_2,
			wheel_7_2_damper, wheel_8_1, wheel_8_2

			Possible values for Helicopters include,
			damageHide, damper_rear, dampers, dashboard, door_1_1, door_1_2, door_2_1, door_2_2, doorgun_1_barrels,
			doorgun_1_elev, doorgun_1_trav, doorgun_2_barrels, doorgun_2_elev, doorgun_2_trav, elevator, hull, mainRotor,
			mainRotorBlurred, mainRotorStatic, rotorShaft, taiIRotor, taiIRotorBlurred, taiIRotorStatic, unit_callsign,
			unit_logo

			Possible values for Planes include, 
			aileron_1, aileron_2, canopy, elevator_1, elevator_2, engine_1_glow_1, engine_1_glow_2, engine_1_glow_3,
			engine_1_nozzle_1, engine_1_nozzle_2, engine_1_nozzle_3, engine_1_nozzle_4,	engine_1_nozzle_5, engine_1_nozzle_6,
			engine_1_nozzle_7, engine_1_nozzle_8, engine_2_glow_1, engine_2_glow_2, engine_2_glow_3, engine_2_nozzle_1,
			engine_2_nozzle_2, engine_2_nozzle_3, engine_2_nozzle_4, engine_2_nozzle_5, engine_2_nozzle_6, engine_2_nozzle_7,
			engine_2_nozzle_8,	engine_afterburner_O engine_afterburner_1, engine_afterburner_2, engine_afterburner_3,
			engine_afterburner_4, flap_1, flap_2, gear_1, gear_1_hatch_1_1, gear_1_hatch_1_2, gear_1_hatch_2, gear_2_1,
			gear_2_1_hatch_1, gear_2_1_hatch_2, gear_2_2, gear_2_2_hatch_1, gear_2_2_hatch_2, hull, PropBlurred, rudder_1,
			rudder_2, Trail_1, Trail_2, Trail_3, Trails, unit_callsign, unit_logo, wheel_1, wheel_2_1, wheel_2_2
		
		@param method - The values for method are,
			"average": Arithmetic average of all vertices in selection
			"bbox_center": Geometry center of the axis-aligned bounding box containing all vertices from selection.
			"allpoints": All points contained in the specific selection. 

		@return position3D

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the position3D as a string use the function getVBSPosition () as shown below
		displayString+="The current position of a selection"+VehicleUtilities::getSelectionPosition(veh1,"hull","allpoints").getVBSPosition();

		@endcode

		@overloaded

		VehicleUtilities::getSelectionPosition(Vehicle& vehicle, string selectName)

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the selection poisition  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: position3D getSelectionPosition(Vehicle& vehicle, string selectName, string method) (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getSelectionPosition(const Vehicle& vehicle, const std::string& selectName, const std::string& method);

		/*!
		
		@description

		Used to check if the vehicle within the VBS2 environment can be reloaded with magazines when it is emptied.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the magazines are reloaded. The vehicle object should be created before the parameter is passed.

		@return bool

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the bool as a string use the function BoolToString() as shown below
		displayString+="The magazine is reloaded"+conversions::BoolToString(VehicleUtilities::isReloadEnabled(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check the reload ability of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::isReloadEnabled (Vehicle& vehicle,  bool state) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool isReloadEnabled(const Vehicle& vehicle);


		/*!
		Attach a statement to a Vehicle. The statement is propagated over the network
		in MP games, it can be executed by invoking processInitCommands. 
		Deprecated. Use void applyInitStatement(Vehicle& vehicle, string statement)
		*/
		VBSFUSION_DPR(UVHC065) static void setInitStatement(Vehicle& vehicle, std::string& statement) ;

		/*!
		Clear the vehicle's init statement.
		Deprecated. Use void applyInitStatementClear(Vehicle& vehicle)
		*/
		VBSFUSION_DPR(UVHC066) static void clearInitStatement(Vehicle& vehicle) ;

		/*!
		
		@description

		Used to return the name of the first crew in the vehicle in order within the VBS2 environment. 

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the units name is required. The vehicle object should be created before the parameter is passed.

		@return string

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		displayString+="The name of the first member"+VehicleUtilities::getName(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the name of the first crew of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getName(Vehicle& vehicle) (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::string getName(const Vehicle& vehicle);

		/*!
		
		@description

		Used to check if the vehicle is captive within the VBS2 environment. If a vehicle is captive enemies will not shoot the vehicle.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle to be checked. The vehicle object should be created before the parameter is passed.

		@return bool

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the bool as a string use the function BoolToString() as shown below
		displayString+="The vehicle is captive "+conversions::BoolToString(VehicleUtilities::isCaptive(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check if a   vehicle created in a client machine within a network is captive, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: isCaptive(Vehicle& vehicle) (Vehicle& vehicle) (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool isCaptive(const Vehicle& vehicle);

		/*!
		Orders an AI-controlled aircraft to land at a given airport.
		The available airport codes are listed on the respective map pages.

		Deprecated. Use void applyLanding(Vehicle& vehicle, int port)
		*/
		VBSFUSION_DPR(UVHC047) static void landTo(Vehicle& vehicle, int port);

		/*!
		Assigns the airport that will be used when issuing either a land command to an AI-controlled aircraft, 
		or selecting the "Landing autopilot" action, if the aircraft is player-controlled.
		The available airport codes are listed on the respective map pages. 

		Deprecated. Use void applyAirportAssignment(Vehicle& vehicle, int port)
		*/
		VBSFUSION_DPR(UVHC046) static void assignToAirport(Vehicle& vehicle, int port);

		/* !
		
		@description

		Used to return a Boolean value indicating whether vehicle is locked or not. If the vehicle is locked units cannot mount or dismount into the vehicle without order.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle which is to be checked. The vehicle object should be created before the parameter is passed.

		@return bool

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the bool as a string use the function BoolToString() as shown below
		displayString+="The vehicle is locked  "+conversions::BoolToString(VehicleUtilities::islocked(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check if a   vehicle created in a client machine within a network is locked, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: islocked (Vehicle& vehicle) (Vehicle& vehicle) (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool islocked(const Vehicle& vehicle);

		/*!
		
		@description

		Used to Assigns cargo role to unit, and places him into the vehicle within the VBS2 environment.

		@locality

		Locally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit - The unit which needs to be moved into the vehicle.

		@param vehicle - The vehicle to which the unit is moved. The vehicle object should be created before the parameter is passed.

		@param cargoIndex - Index of cargo position
		
		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//The unit  to be created
		Unit u1
		
		VehicleUtilities::moveInCargoWithPosition(u1,veh1,3);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void moveInCargoWithPosition(Unit& unit,Vehicle& vehicle, int cargoIndex);

		/*!
		Ceases the using of the vehicle by  a group. It unassigns them from the vehicle. 
		Deprecated. Use void applyGroupLeave(Vehicle& vehicle, Group& group)
		*/
		VBSFUSION_DPR(UVHC057) static void groupLeaveVehicle(Group& group, Vehicle& vehicle);

		/*!
		Ceases the using of the vehicle by  a unit. It unassigns them from the vehicle. 
		Deprecated. Use void applyGroupLeave(Vehicle& vehicle, Unit& unit)
		*/
		VBSFUSION_DPR(UVHC058) static void groupLeaveVehicle(Unit& unit, Vehicle& vehicle);

		/*!
		
		@description

		Used to return the type name of Binocular weapon which is in the WeaponSlotBinocular. Vehicle's side cannot be a civilian.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for  which the binocular weapon name is required. The vehicle object should be created before the parameter is passed.

		@return string

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		displayString =" The bino's name is "+VehicleUtilities::getBinocularWeaponName(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the name of the binocular of a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getBinocularWeaponName(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::string getBinocularWeaponName(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return the expected destination position of the Vehicle within the VBS2 environment. 

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the position is required. The vehicle object should be created before the parameter is passed.

		@return position3D

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the position3D as a string use the function getVBSPosition () as shown below
		displayString +=" The expected destination position is  "+VehicleUtilities::getExpectedDestinationPos(veh1).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the expected destination position  of a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getExpectedDestinationPos(Vehicle& vehicle)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static position3D getExpectedDestinationPos(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return the expected destination plan mode of the Vehicle within the VBS2 environment. 

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the plan mode is required. The vehicle object should be created before the parameter is passed.

		@return string

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		displayString +=" The expected destination plan mode is  "+VehicleUtilities::getExpectedDestinationPlanMode(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the expected destination plan mode  of a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getExpectedDestinationPlanMode(Vehicle& vehicle)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::string getExpectedDestinationPlanMode(const Vehicle& vehicle);

		/*!
		
		@description

		Used to check if the destination's replanning of the path was forced within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which DestinationForceReplan is required. The vehicle object should be created before the parameter is passed.

		@return bool

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1

		//To display the bool as a string use the function BoolToString() as shown below
		displayString +=" The destination re panning is "+conversions::BoolToString(VehicleUtilities::getExpectedDestinationForceReplan(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the expected destination force replan  of a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getExpectedDestinationForceReplan (Vehicle& vehicle)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
 
		*/
		static bool getExpectedDestinationForceReplan(const Vehicle& vehicle);

		/*!
		
		@description

		Used to get the position of the vehicle formation within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the position of formation is obtained. The vehicle object should be created before the parameter is passed.

		@return position3D

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the position3D as a string use the function getVBSPosition () as shown below
		displayString +=" The formation position is "+VehicleUtilities::getFormationPosition(veh1).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the formation position  of a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getFormationPosition(Vehicle& vehicle)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getFormationPosition(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return the current command type of the commander for the specified vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the command type of the commander is  obtained. The vehicle object should be created before the parameter is passed.

		@return string

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
	
		displayString +=" The current command is "+VehicleUtilities::getCurrentCommand(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain current command type of the commander  of a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getCurrentCommand(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::string getCurrentCommand(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return the group leader of the vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the group leader is  required. The vehicle object should be created before the parameter is passed. For dead units ObjNull is returned.

		@return unit

		@example

		@code

		//The unit to be set
		Unit u1;
		
		u1 = VehicleUtilities::getGroupLeader(veh1);
		
		displayString +="The leader is"+u1.getName();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the group leader of a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getGroupLeader(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static Unit getGroupLeader(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return a vector which contains a  list of units in the vehicle  within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the list of units are required. The vehicle object should be created before the parameter is passed.

		@return vector<Unit>

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1

		vector<Unit> g1 = VehicleUtilities::getGroupUnits(veh1);

		for(vector<Unit>::iterator itr = g1.begin(), itr_end = g1.end();itr!=itr_end;++itr ){
			displayString ="The name of the unit is"+itr->getName();
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the units of a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getGroupUnits(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::vector<Unit> getGroupUnits(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return a boolean value checking if the vehicle is stopped or not using a stop command within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle  which is to be checked whether it is stopped or not. The vehicle object should be created before the parameter is passed.

		@return bool

		@example

		@code

		displayString +=" The vehicle is stopped by the stop command"+conversions::BoolToString(VehicleUtilities::isStopped(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check if a vehicle created in a client machine within a network is stopped by the stop function, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::isStopped(Vehicle& vehicle)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool isStopped(const Vehicle& vehicle);

		/*!
		
		@description

		Used to return a boolean value checking if the vehicle is ready within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle to be checked if it is ready. The vehicle object should be created before the parameter is passed.

		@return bool

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the bool as a string use the function BoolToString() as shown below
		displayString +=" The vehicle is ready"+conversions::BoolToString(VehicleUtilities::isReady(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check if a vehicle created in a client machine within a network is ready , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::isReady(Vehicle& vehicle)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool isReady(const Vehicle& vehicle);
		
		/*!
		Removes all weapons from the vehicle cargo space. MP synchronized.
		Deprecated. Use void applyWeaponCargoRemoveGlobal(Vehicle& vehicle)
		*/
		VBSFUSION_DPR(UVHC069) static void clearWeaponCargoGlobal(Vehicle& vehicle);

		/*!
		Removes all magazines from the vehicle cargo space. MP synchronized.
		Deprecated. Use void applyMagazineCargoRemoveGlobal(Vehicle& vehicle)
		*/
		VBSFUSION_DPR(UVHC070) static void clearMagazineCargoGlobal(Vehicle& vehicle);

		/*!

		@description

		Used to locate a position for landing an aircraft after a search within the VBS2 environment. The returned values can be:
			"Found": Position found (within about 250m)
			"NotFound": No position was found within the searched range
			"NotReady": No land command received yet, or still searching for a landing position

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle to be landed. The vehicle object should be created before the parameter is passed.

		@return string

		@example

		@code

		//The vehicle  to be created
		Vehicle veh3
		
		displayString +=" The result is "+VehicleUtilities::getLandResult(veh3);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To locate a position of a vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getLandResult (Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static  std::string getLandResult(const Vehicle& vehicle);

		/*!
		Sets vehicle's lights states. (-2 forced off, -1 off (user controlled), 0 user controlled, 1 on (user controlled), 2 forced on)
		Deprecated. Use void applyVehicleLights(Vehicle& vehicle, int main, int side, int convoy, int left, int right)
		*/
		VBSFUSION_DPR(UVHC061) static void setVehicleLights(Vehicle& vehicle,int main, int side, int convoy, int left, int right);

		/*!
		
		@description

		Used to return the status of different vehicle lights, as set by either the user's input (via action menu or control keys), or the function VehicleUtilities ::applyVehicleLights(Vehicle& vehicle, int main, int side, int convoy, int left, int right).Each light element can have one of the following states:
			-2: Forced off (user cannot override it, either via action menu or control buttons)
			-1: off (user can turn it on again)
			1: on (user can turn it off again)
			2: forced on (user cannot override it, either via action menu or control buttons)

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param vehicle - The vehicle for which the status of lights are required. The vehicle object should be created before the parameter is passed.

		@return vector<int>

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1

		vector<int> li= VehicleUtilities::getVehicleLights(veh1);

		for (vector<int>::iterator mitr = li.begin(),mitr_end = li.end();mitr!= mitr_end;mitr++)
		{
			displayString += "The lights are"+conversions::IntToString(*mitr)+"\n";
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the status of the lights of  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getVehicleLights(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::vector<int> getVehicleLights(const Vehicle& vehicle);

		/*!
		Adds weapons to the weapon cargo space. This is used for infantry weapons.
		MP synchronized
		Deprecated. Use void applyWeaponCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count)
		*/
		VBSFUSION_DPR(UVHC067) static void addWeaponToCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count);

		/*!
		Adds magazines to the weapon cargo space. This is used for infantry weapons.
		MP synchronized.
		magName - Class name of the magazine.
		Deprecated. Use void applyMagazineCargoGlobal(Vehicle& vehicle, string magName, int count)
		*/
		VBSFUSION_DPR(UVHC068) static void addMagazineToCargoGlobal(Vehicle& vehicle, std::string magName, int count);

		/*!
		Set if leader can issue attack commands to the soldiers in his group
		Deprecated. Use void applyAttackEnable(Vehicle& vehicle, bool boolVal)
		*/
		VBSFUSION_DPR(UVHC059) static void setEnableAttack(Vehicle& vehicle, bool boolVal);

		/*!
		
		@description

		Used to return a boolean value indicating if a group leader can issue commands for the soldiers under his command within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.03]

		@param vehicle - The vehicle in which the group leader is present. The vehicle object should be created before the parameter is passed.

		@return bool

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//To display the bool as a string use the function BoolToString() as shown below
		displayString +=" Attack enable "+conversions::BoolToString(VehicleUtilities::isAttackEnabled(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check if a leader within a  vehicle created in a client machine within a network can issue orders to his soldiers, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: isAttackEnabled(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool isAttackEnabled(const Vehicle& vehicle);

		/*!
		Sets the rotor wash effect of helicopters (applies to all helicopters in mission).

		This affects the dust created by the helicopter, as well as the wind effect on objects nearby.
		While it is not possible to turn off the dust effect altogether, it is possible to make it somewhat 
		invisible by using very high values (e.g. [100,100]) in the parameters. This only works though, 
		if there are no objects nearby that might be affected by the generated wind (e.g. trees), as they would then show unrealistic wind strain.
		
		Deprecated. Use bool applyRotorWash(double strength, double diameter)
		*/
		VBSFUSION_DPR(UVHC060) static bool setRotorWash(double strength, double diameter);

		/*!
		Enables or disables specific sound types for a vehicle:

		"SOUNDENGINE": Engine
		"SOUNDENVIRON": Movement sounds (e.g. tank tracks)
		"SOUNDCRASH": Impact with another object
		"SOUNDDAMMAGE": Alarm sound if vehicle was hit
		"SOUNDLANDCRASH": Impact with land (e.g. aircraft crash)
		"SOUNDWATERCRASH": Impact with water (e.g. aircraft crash)
		"SOUNDGETIN": Entering vehicle
		"SOUNDGETOUT": Exiting vehicle
		"SOUNDSERVO": Vehicle attachments (e.g. tank turrets)

		Be aware that not all of these sounds are defined in every vehicle.

		Deprecated. Use void applySoundEnable(Vehicle& vehicle, VEHICLE_SOUND_TYPES types, bool value)
		*/
		VBSFUSION_DPR(UVHC074) static void allowSound(Vehicle& vehicle,VEHICLE_SOUND_TYPES types,bool value);

		/*
		Controls screen blanking during certain actions. 
		Action with an associated screen blanking. Supported actions: "GETOUT", "TURNOUT", "TURNIN" 
		Enum :VA_GETOUT,VA_TURNOUT,VA_TURNIN

		Deprecated. Use void applyBlackFadeEnable(Vehicle& vehicle, VEHICLE_ACTION actions, bool value)
		*/
		VBSFUSION_DPR(UVHC062) static void allowBlackFade(Vehicle& vehicle, VEHICLE_ACTION actions, bool value);

		/*!
		
		@description

		Used to return a vector listing the sounds which are disabled in the vehicle within the VBS2 environment via allow sound.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.03]

		@param vehicle - The vehicle for which the disabled sounds are obtained. The vehicle object should be created before the parameter is passed.

		@return vector<string>

		@example

		@code

		vector<string> sound= VehicleUtilities::getDisallowedSounds(veh1);

		for (vector<string>::iterator nitr = sound.begin(),nitr_end = sound.end();nitr!= nitr_end;nitr++){
			displayString += "The sounds are"+(*nitr)+"\n";
		} 

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the disabled sounds of a  vehicle created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getDisallowedSounds(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)). 

		*/
		static std::vector<std::string> getDisallowedSounds(const Vehicle& vehicle);

	    /*!
		
		@description

		Used to return the position of the center of vehicle�s named selection within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.10.1]

		@param vehicle - The vehicle for which the above function is applied. The vehicle object should be created before the parameter is passed.

		@param selectionName - Name of the selection for which the center of vehicle is obtained.

		@return position3D

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1

		string selcen= "hull";
		displayString +=" The selection center is "+VehicleUtilities::getSelectionCenter(veh1,selcen).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks obtain the centre  of a  vehicle created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getSelectionCenter(Vehicle& vehicle, string& selectionName) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information. 

		*/
		static position3D getSelectionCenter(const Vehicle& vehicle, const std::string& selectionName);

		/*!
		
		@description

		Used to return number of empty positions in the vehicle for specified position in the vehicle within the VBS2 environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.03]

		@param vehicle - The vehicle for which the above function is applied. The vehicle object should be created before the parameter is passed.

		@param vehPosition - Possible options for positions include (Commander, Driver, Gunner, Cargo)

		@return int

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;

		//To display the integer as a string use the function IntToString () as shown below
		displayString +=" The empty positions are "+conversions::IntToString(VehicleUtilities::getEmptyPositions(veh1,VEHICLEASSIGNMENTTYPE::GUNNER));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the number of empty  positions of a  vehicle created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getEmptyPositions(Vehicle& vehicle,VEHICLEASSIGNMENTTYPE vehPosition);
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)).

		*/
		static int getEmptyPositions(const Vehicle& vehicle, VEHICLEASSIGNMENTTYPE vehPosition);

		/*!

		@description

		Used to return the combat mode (engagement rules) of the vehicle within the VBS2 environment.
		Modes returned may be:
			COMBAT_BLUE (Never fire)
			COMBAT_GREEN (Hold fire - defend only)
			COMBAT_WHITE (Hold fire, engage at will)
			COMBAT_YELLOW (Fire at will)
			COMBAT_RED (Fire at will, engage at will) 
			or
			COMBAT_ERROR ) 

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.05]

		@param vehicle - The vehicle for which the combat mode is required. The vehicle object should be created before the parameter is passed.

		@return COMBATMODE

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1

		COMBATMODE cm = VehicleUtilities::getVehicleCombatMode(veh1);
		
		if(cm =COMBATMODE::COMBAT_BLUE){
			displayString+="The combat mode is blue";}
		else if (cm =COMBATMODE::COMBAT_ERROR){
			displayString+="The combat mode is error";}
		else if (cm =COMBATMODE::COMBAT_GREEN){
			displayString+="The combat mode is green";}
		else if (cm =COMBATMODE::COMBAT_RED){
			displayString+="The combat mode is red";}
		else if (cm =COMBATMODE::COMBAT_WHITE){
			displayString+="The combat mode is white";}
		else if (cm =COMBATMODE::COMBAT_YELLOW){
			displayString+="The combat mode is yellow";
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the combat mode of a  vehicle created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getVehicleCombatMode (Vehicle& vehicle,VEHICLEASSIGNMENTTYPE vehPosition); 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)).

		*/
		static COMBATMODE getVehicleCombatMode(const Vehicle& vehicle);

		/*!
		
		@description

		Used to lock the vehicle in the preferred lock state within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.05]

		@param vehicle - The vehicle which needs to be locked. The vehicle object should be created before the parameter is passed.

		@param state - The lock state of the vehicle. 
		
		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		VehicleUtilities::applyVehicleLock(veh1,LOCK_STATE::UNLOCKED);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyVehicleLock(Vehicle& vehicle, LOCK_STATE state);

		/*!
		Orders the unit to get out from the vehicle (via the radio). 
		This command is only effective when used on local units. When used on a remote unit, this command will create radio 
		dialog on the machine the unit is local to, but the unit will not leave the vehicle.

		Deprecated. Use void applyCommandGetOut(Unit& unit)
		*/
		VBSFUSION_DPR(UVHC064) static void commandGetOut(Unit& unit);

		/*!
		Orders a unit or units to get out from the vehicle (silently).
		Deprecated. Use void applyGetOut(Unit& unit)
		*/
		VBSFUSION_DPR(UVHC063) static void doGetOut(Unit& unit);

		/*!
		
		@description

		Used to return a vector which contains a list of the offset between weapon orientation and optics within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.05]

		@param vehicle - The vehicle  to which the above function is applied. The vehicle object should be created before the parameter is passed.

		@param turretPath - Path of the turret
		
		@return vector<double>

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		Turret::TurretPath tpath;
		tpath.push_back(0);
		tpath.push_back(1);

		Turret t;
		t.setTurretPath(tpath);

		vector<double> optic_offset;
		optic_offset = VehicleUtilities::getOpticsOffset(veh1,tpath);

		for (vector<double>::iterator itr = optic_offset.begin(),itr_end = optic_offset.end();itr!=itr_end;itr++){
			displayString +="The optic offset are "+conversions::DoubleToString(*itr)+"\n";
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obatin the offset for  a  vehicle created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)). 

		*/
		static std::vector<double> getOpticsOffset(const Vehicle& vehicle, const Turret::TurretPath& turretPath);

		/*!
		
		@description

		Used to return the laser range which Forces  the gunner in vehicle to recalculate the current lasing distance.
		Overrides the actual laser measurements (which are used to calculate the ballistic calculations) with a value provided by this command.This value will be shown in the gunner's optics view, until he does another manual lasing within the VBS2 environment.

		@locality

		Globally applied Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle  for  which the laser range is required. The vehicle object should be created before the parameter is passed.

		@param gunner - the gunner who calculates the current lasing distance.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//The unit  to be created
		Unit u1
		
		VehicleUtilities::applyLaserRange(veh1,u1);

		@endcode

		@overloaded

		VehicleUtilities::applyLaserRange(Vehicle& vehicle, Unit& gunner, double range);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks apply the laser range for  a  vehicle created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyLaserRange(Vehicle& vehicle, Unit& gunner) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)); 
		
		*/
		static void applyLaserRange(Vehicle& vehicle, Unit& gunner);

		/*!
		
		@description

		Used to apply a created unit to the vehicle as a driver within the VBS2 environment.

		@locality

		Locally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle for  which the unit is applied as a driver. The vehicle object should be created before the parameter is passed.

		@param unit - The driver of the vehicle

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//The unit  to be created
		Unit u1
		
		VehicleUtilities::applyMoveInAsDriver(veh1,u1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks This is a replication of void moveInAndAsDriver(Vehicle& vehicle, Unit& unit) function.

		*/
		static void applyMoveInAsDriver(Vehicle& vehicle, Unit& unit);

		/*!
		
		@description

		Used to assign the turret role to a created unit, and places him into the vehicle immediately within the VBS2 environment.

		@locality

		Locally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle which assigns the turret. The vehicle object should be created before the parameter is passed.

		@param unit - A created unit which is assigned to the turret role and placed into the vehicle.

		@param turretPath - TurretPath which the unit is moved into.
		
		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//The unit  to be created
		Unit u1;

		VehicleUtilities::applyMoveInAsTurret(veh1,u1,"[0,1]");

		@endcode

		@overloaded

		VehicleUtilities::applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, Turret turret);
		
		VehicleUtilities::applyMoveInAsTurret(Vehicle& vehicle, Unit& unit);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks This is a replication of void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turretPath) function.

		*/
		static void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, const std::string& turretPath);

		/*!
		
		@description

		Used to assign the turret role to a created unit, and places him into the vehicle immediately within the VBS2 environment.

		@locality

		Locally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle which assigns the turret. The vehicle object should be created before the parameter is passed.

		@param unit - A created unit which is assigned to the turret role and placed into the vehicle.

		@param turret - TurretPath which the unit is moved into.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//The unit  to be created
		Unit u1;

		Turret::TurretPath tpath;
		tpath.push_back(0);
		tpath.push_back(1);

		Turret t;
		t.setTurretPath(tpath);

		VehicleUtilities::applyMoveInAsTurret(veh1,u1,t);

		@endcode

		@overloaded

		VehicleUtilities::applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, string turretPath);
		
		VehicleUtilities::applyMoveInAsTurret(Vehicle& vehicle, Unit& unit);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks This is a replication of void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turret) function. 

		*/
		static void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, const Turret& turret);

		/*!
		
		@description

		Used to assign the turret role to a created unit, and places him into the vehicle immediately within the VBS2 environment.

		@locality

		Locally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle which assigns the turret. The vehicle object should be created before the parameter is passed.

		@param  unit - A created unit which is assigned to the turret role and placed into the vehicle.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//The unit  to be created
		Unit u1;
		
		VehicleUtilities::applyMoveInAsTurret(veh1,u1);

		@endcode

		@overloaded

		VehicleUtilities::applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, string turretPath);
		
		VehicleUtilities::applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, Turret turret);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		*/
		static void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit);


		/*!

		@description

		Used to assign a created unit as a gunner and places him into the vehicle within the VBS2 environment.

		@locality

		Locally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle which assigns the unit as a gunner. The vehicle object should be created before the parameter is passed.

		@param unit - A created unit which is assigned as a gunner.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1

		//The unit to be created
		Unit u1;
		VehicleUtilities::applyMoveInAsGunner(veh1,u1);

		@endcode

		@overloaded

		void applyAssignAsGunner(Vehicle& vehicle, Unit& unit);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks This is a replication of void moveInAndAsGunner(Vehicle& vehicle, Unit& unit); function. 

		*/
		static void applyMoveInAsGunner(Vehicle& vehicle, Unit& unit);

		/*!

		@description

		Used to force the Armed Vehicle to fire on the given position from the specified turret path within the VBS2 environment.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle to which the above function is applied. The vehicle object should be created before the parameter is passed.

		@param pos - The position which is needed to be fire.

		@param turretPath - path of the turret.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1

		Turret::TurretPath tpath;
		tpath.push_back(0);
		tpath.push_back(1);

		Turret t;
		t.setTurretPath(tpath);

		VehicleUtilities::applyFire(veh1,player.getPosition()+position3D(30,10,0),tpath);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To make a vehicle created in a client machine within a network to fire, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyFire(Vehicle& vehicle, position3D& pos, Turret::TurretPath turretPath); 
		
		*/
		static void applyFire(Vehicle& vehicle, const position3D& pos, const Turret::TurretPath& turretPath);

		/*!

		@description

		Used to assign the cargo role to a created unit within the VBS2 environment.

		@locality

		Locally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle to which the unit is assigned . The vehicle object should be created before the parameter is passed.

		@param unit - The unit which needs to be moved in as cargo.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		//The unit to be created
		Unit u1;

		VehicleUtilities::applyMoveInAsCargo(veh1,u1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of void moveInAndAsCargo(Vehicle& vehicle, Unit& unit); function.

		*/
		static void applyMoveInAsCargo(Vehicle& vehicle, Unit& unit);

		/*!

		@description

		Used to assign a created unit as commander to the vehicle within the VBS2 environment.

		@locality

		Locally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle to which the above function is applied. The vehicle object should be created before the parameter is passed.

		@param unit - The unit which needs to be moved in as a commander.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		//The unit to be created
		Unit u1;

		VehicleUtilities::applyMoveInAsCommander(veh1,u1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks his is a replication of void moveInAndAsCommander(Vehicle& vehicle, Unit& unit); function.
		
		*/
		static void applyMoveInAsCommander(Vehicle& vehicle, Unit& unit);


		/*!

		@description

		Used to activate or deactivate an engine of the vehicle within the VBS2 environment.

		@locality

		Globally Applied Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle which needs to be started or stopped . The vehicle object should be created before the parameter is passed.

		@param state - boolean value true or false. A true starts the engine and a false value stops it.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		VehicleUtilities::applyEngineStart(veh1,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of void startEngine(Vehicle& vehicle , bool state) function.
		
		@remarks To activate the engine of a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyEngineStart(Vehicle& vehicle , bool state)

		*/
		static void applyEngineStart(Vehicle& vehicle , bool state);

		/*!

		@description

		This function is applied for a unit in the vehicle within the VBS2 environment to leave.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle in which the unit is present . The vehicle object should be created before the parameter is passed.

		@param unit - The unit which needs to be left from the vehicle.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		//The unit to be created
		Vehicle u1;

		VehicleUtilities::applyUnitLeave(veh1,u1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of following functions., VehicleUtilities::leaveVehicle(Vehicle& vehicle, Unit& unit), VehicleUtilities:: leaveVehicleByID(Vehicle& vehicle, Unit& unit), VehicleUtilities::leaveVehicleByName(Vehicle& vehicle, Unit& unit), VehicleUtilities:: leaveVehicleByAlias(Vehicle& vehicle, Unit& unit)
		
		@remarks To force a unit to leave vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::applyUnitLeave(Vehicle& vehicle, Unit& unit)

		*/
		static void applyUnitLeave(Vehicle& vehicle, Unit& unit);

		/*!

		@description

		This function enables/ disables the manual control of the vehicle. If the control is TRUE vehicle AI is disabled and it can be controlled by applyTurnWanted & applyThrustWanted functions.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle which sets the AI control. The vehicle object should be created before the parameter is passed.

		@param control - Its a boolean value, if the control is "true" AI control enabled and false value disables it.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		VehicleUtilities::applyManualControl(veh1,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of void setManualControl(Vehicle& vehicle , bool control) Function.

		*/
		static void applyManualControl(Vehicle& vehicle , bool control);

		/*!

		@description

		This function sets the turn wanted for the vehicle specified by the factor value. The manual control of the vehicle should be enabled in prior.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle which is needs to be controlled. The vehicle object should be created before the parameter is passed.

		@param factor - value that needs to be turned, it ranges between -1.0 to 1.0.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		VehicleUtilities::applyTurnWanted(veh1,1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of void setTurnWanted(Vehicle& vehicle, double factor) Function.

		*/
		static void applyTurnWanted(Vehicle& vehicle, double factor);

		/*!

		@description

		This function sets set the thrust wanted for the vehicle to passed in. Manual control of the vehicle should be enabled in prior.

		@locality

		LOcally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle which controls the thrust. The vehicle object should be created before the parameter is passed.
		
		@param factor - The value of the thrust, it ranges between -1.0 to 1.0.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyThrustWanted(veh1, 1.0);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of void setThrustWanted(Vehicle& vehicle , double factor) function

		*/
		static void applyThrustWanted(Vehicle& vehicle , double factor);

		/*!

		@description

		This function applies the maximum speed to the vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle for which the maximum speed is applied. The vehicle object should be created before the parameter is passed.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;
		veh1.setMaxSpeedLimit(0.5);
		VehicleUtilities::applyMaximumSpeedLimit(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks To apply the maximum speed limit to a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function applyMaximumSpeedLimit(Vehicle& vehicle)

		*/
		static void applyMaximumSpeedLimit(Vehicle& vehicle);

		/*!

		@description

		This function updates the maximum speed limit of the vehicle within the VBS2 environment on the fusion end.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle for which the maximum speed is updated. The vehicle object should be created before the parameter is passed.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1
		
		VehicleUtilities::updateMaximumSpeedLimit(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To update the maximum speed limit  of a  vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: updateMaximumSpeedLimit(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void updateMaximumSpeedLimit(Vehicle& vehicle);

		/*!
		
		@description

		This function applies animation process to the vehicle within the VBS2 environment.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle for which the animation is applied. The vehicle object should be created before the parameter is passed.

		@param animationName - Name of the animation.
		
		@param phase - Phase value is between 0(start point of the animation) and 1(end point of the animation).  

		@return Nothing

		@example

		@code

		Vehicle  veh1;

		VehicleUtilities::applyAnimation(vehi1, "hood", 1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks This is a replication of void applyAnimation(Vehicle& vehicle, string animationName, double phase) function

		*/
		static void applyAnimation(Vehicle& vehicle, const std::string& animationName, double phase);

		/*!
		
		@description

		This function assigns a driver to the vehicle within the VBS2 environment.

		@locality

		Globally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle for which the driver is assigned. The vehicle object should be created before the parameter is passed.

		@param unit - The driver
		
		@param delay - The amount of time the driver has to be in the newly assigned position and moves back to his old position after the delay time is over if he is assigned with another vehicle role. The unit remains in his new position if the delay value is negative.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		//The unit  to be created
		Unit u1;
		
		VehicleUtilities::applyAssignAsDriver(veh1,u1,1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
			- This is a replication of assignAsDriver(Vehicle& vehicle, Unit& unit, double delay) function.
			- To assign a driver for a  vehicle created in a client machine within a network, use the function  

		@remarks MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyAssignAsDriver(Vehicle& vehicle, Unit& unit, double delay) (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyAssignAsDriver(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		
		@description

		This function assigns a driver to the vehicle within the VBS2 environment.

		@locality

		Globally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle for which the gunner is assigned. The vehicle object should be created before the parameter is passed.

		@param unit - The gunner

		@param delay - The amount of time the gunner has to be in the newly assigned position and moves back to his old position after the delay time is over if he is assigned with another vehicle role. The unit remains in his new position if the delay value is negative.
		
		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		VehicleUtilities::applyAssignAsGunner(veh1,gunner,1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of assignAsGunner(Vehicle& vehicle, Unit& unit, double delay) function.

		@remarks To assign a gunner for a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyAssignAsGunner(Vehicle& vehicle, Unit& unit, double delay); 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void applyAssignAsGunner(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		
		@description

		This function forces the units assigned to a  vehicle within the VBS2 environment to move out or to move in.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit - The unit which moves in or out of the assigned vehicle.

		@param order - If the Boolean value is true, the unit is moved into the assigned vehicle and vice versa if the Boolean value is false.

		@return Nothing

		@example

		@code

		//The unit  to be created
		Unit u1;
		
		VehicleUtilities::applyGetIn(u1,false);

		@endcode

		@overloaded

		VehicleUtilities::applyGetIn(vector<Unit> unitVec, bool order);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To force a unit to get into  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyGetIn(Unit& unit, bool order); 

		*/
		static void applyGetIn(Unit& unit, bool order);

		/*!
		
		@description

		This function forces the array of units assigned to a  vehicle within the VBS2 environment to move out or to move into the vehicle.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unitVec - The array of units which moves in or out of the assigned vehicle.

		@param Order - If the Boolean value is true, the array of units are moved into the assigned vehicle and vice versa if the Boolean value is false.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1
		
		vector<Unit> vu;
		vu.push_back(u1);
		vu.push_back(u2);
		vu.push_back(u3);
		
		VehicleUtilities::applyGetIn(vu,false); 

		@endcode

		@overloaded

		VehicleUtilities::applyGetIn(Unit& unit, bool order);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of void orderGetIn(vector<Unit> unitVec, bool order) function .
		
		@remarks To force a list units to get into/out of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyGetIn(vector<Unit> unitVec, bool order).
 
		*/
		static void applyGetIn(std::vector<Unit> unitVec, bool order);

		/*!
		
		@description

		This function assigns a unit as  cargo to the vehicle within the VBS2 environment.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle for which the cargo is assigned. The vehicle object should be created before the parameter is passed.

		@param unit - The which is assigned as a cargo.

		@return delay - The amount of time the cargo has to be in the newly assigned position and moves back to his old position after the delay time is over if he is assigned with another vehicle role. The unit remains in his new position if the delay value is negative.

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		//The unit to be created 
		Unit u1
		
		VehicleUtilities::applyAssignAsCargo(veh1,u1,1);

		@endcode

		@overloaded

		VehicleUtilities::applyAssignAsCargo(Vehicle& vehicle, Unit& unit);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of assignAsCargo(Vehicle& vehicle, Unit& unit, double delay) .
 
		@remarks To apply  a unit as cargo to a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyAssignAsCargo(Vehicle& vehicle, Unit& unit, double delay). 

		*/
		static void applyAssignAsCargo(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		
		@description

		This function assigns a commander to the vehicle within the VBS2 environment.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle for which the commander  is assigned. The vehicle object should be created before the parameter is passed.

		@param unit - The unit which is assigned as a commander.
		
		@param delay - The amount of time the commander has to be in the newly assigned position and moves back to his old position after the delay time is over if he is assigned with another vehicle role. The unit remains in his new position if the delay value is negative.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		//The unit to be created
		Unit u1;
		
		VehicleUtilities::applyAssignAsCommander(veh1,u1,1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of assignAsCommander(Vehicle& vehicle, Unit& unit, double delay) .
		
		@remarks To apply  a unit as commander to a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyAssignAsCommander(Vehicle& vehicle, Unit& unit, double delay)
 
		*/
		static void applyAssignAsCommander(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		
		@description

		This function selects weapons in the vehicle by the muzzle name within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle which holds the weapon. The vehicle object should be created before the parameter is passed.

		@param muzzleName - Name of the muzzle. This should be given as an input if the weapon has more than one muzzle. The only weapons that have muzzleNames are rifles with grenade launchers, handgrenades, smokeshells and satchels. In all the other cases the weapon name must be used.

		@return Nothing

		@example

		@code

		Vehicle veh1;

		vector<string> muzzleList = VehicleUtilities::getMuzzles(veh1);
		string firstMuzzle = muzzleList.at(0);

		VehicleUtilities::applyWeaponSelection(veh1, firstMuzzle);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void selectWeapon(Vehicle& vehicle, string muzzleName) function. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyWeaponSelection(Vehicle& vehicle, const std::string& muzzleName);

		/*!
		
		@description

		This function enables to add many magazines to the vehicle within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle to which the magazines are added. The vehicle object should be created before the parameter is passed.  Any number of magazines can be added to the vehicle as a fixed value to be added is not allotted.

		@param magName - Name of the magazine to be added.

		@return Nothing

		@example

		@code

		Vehicle veh1;

		string magName = "vbs2_mag_100rnd_127x99_Trace_browning_m2";

		VehicleUtilities::applyMagazineAddition(veh1, magName);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void addMagazine(Vehicle& vehicle, string name) function.

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyMagazineAddition(Vehicle& vehicle, const std::string &magName);

		/*!
		
		@description

		Used to return the count of the magazines available in the vehicle within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle  which contains the magazines. The vehicle object should be created before the parameter is passed.  

		@return int

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		//To display the integer as a string use the function IntToString () as shown below
		displayString+="The number is "+conversions::IntToString(VehicleUtilities::getMagazineCount(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks This is a replication of int countMagazines(Vehicle& vehicle)function.

		@remarks To obtain the magazine count of  a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: getMagazineCount(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static int getMagazineCount(const Vehicle& vehicle);

		/*!
		
		@description

		Used to add weapon to the vehicle within the VBS2 environment. When the space alloted for weapon is full, application of the above  function is ignored. To ensure that the weapon is loaded at the start of the mission, at least one magazine should be added  before the addition of weapon.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle which adds the weapon. The vehicle object should be created before the parameter is passed.  

		@param name - Name of the weapon to be added.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyWeaponAddition(vehi1, "VBS2_2A42_30mm");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void addWeapon(Vehicle& vehicle, string name)function.

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyWeaponAddition(Vehicle& vehicle, const std::string& name);

		/*!
		
		@description

		This function assigns a commander to the vehicle within the VBS2 environment.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle for which the commander is assigned. The vehicle object should be created before the parameter is passed.

		@param unit - The unit  which is assigned as a commander.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		//The unit  to be created
		Unit u1;
		VehicleUtilities::applyAssignAsCommander(veh,u1);

		@endcode

		@overloaded

		VehicleUtilities::applyAssignAsCommander(Vehicle& vehicle, Unit& unit, double delay);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		This is a replication of void assignAsCommander(Vehicle& vehicle, Unit& unit) function.

		@remarks To set a  unit as a commander for a   vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyAssignAsCommander(Vehicle& vehicle, Unit& unit); 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyAssignAsCommander(Vehicle& vehicle, Unit& unit);

		/*!

		@description

		This function assigns a unit as  cargo to the vehicle within the VBS2 environment.

		@locality

		Globally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle for which the cargo is assigned. The vehicle object should be created before the parameter is passed.

		@param unit - The which is assigned as a cargo.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		//The unit to be created 
		Unit u1
		
		VehicleUtilities::applyAssignAsCargo(veh1,u1);

		VehicleUtilities::applyGetIn(u1,true);

		@endcode

		@overloaded

		VehicleUtilities::applyAssignAsCargo(Vehicle& vehicle, Unit& unit, double delay);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of assignAsCargo(Vehicle& vehicle, Unit& unit). 
		
		@remarks To apply  a unit as cargo to a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyAssignAsCargo(Vehicle& vehicle, Unit& unit).

		@remarks The function applyGetIn(Unit& unit, bool order) should be used to move the unit as a cargo into the vehicle.

		*/
		static void applyAssignAsCargo(Vehicle& vehicle, Unit& unit);

		/*!
		
		@description

		This function assigns a driver to the vehicle within the VBS2 environment.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle for which the driver is assigned. The vehicle object should be created before the parameter is passed.

		@param unit - The unit which is assigned as a driver.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		//The unit  to be created
		Unit u1;
		
		VehicleUtilities::applyAssignAsDriver(veh1,u1);

		@endcode

		@overloaded

		VehicleUtilities::applyAssignAsDriver(Vehicle& vehicle, Unit& unit, double delay);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void assignAsDriver(Vehicle& vehicle, Unit& unit) function.

		@remarks  To set a  unit as a Driver for a   vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyAssignAsDriver(Vehicle& vehicle, Unit& unit); 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyAssignAsDriver(Vehicle& vehicle, Unit& unit);

		/*!

		@description

		This function assigns a gunner to the vehicle within the VBS2 environment.

		@locality

		Globally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle for which the driver is assigned. The vehicle object should be created before the parameter is passed.

		@param unit - The unit which is assigned as a gunner.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		//The unit  to be created
		Unit u1;
		
		VehicleUtilities::applyAssignAsGunner (veh1,u1);

		@endcode

		@overloaded

		VehicleUtilities::applyAssignAsGunner(Vehicle& vehicle, Unit& unit, double delay);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks This is a replication of void assignAsGunner(Vehicle& vehicle, Unit& unit) function.
		
		@remarks To set a  unit as a Gunner for a   vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: applyAssignAsGunner(Vehicle& vehicle, Unit& unit); 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
 
		*/
		static void applyAssignAsGunner(Vehicle& vehicle, Unit& unit);

		/*!
		
		@description

		This function used to apply the reload status of the weapon in the vehicle within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle which contains the weapon. The vehicle object should be created before the parameter is passed.

		@param weapon - Name of the weapon.
		
		@param reloadStatus - Reload status of the weapon.

		@param reloadTime - Reloads time in seconds.

		@return Nothing

		@example

		@code

		Weapon w1;
		
		w1.setWeaponClassName("VBS2_2A42_30mm");
		
		VehicleUtilities::applyWeaponState(veh1, w1, true, 7);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime) function. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime);

		/*!
		
		@description

		This function is used to remove the weapon in the vehicle within the VBS2 environment.

		@locality

		Locally Applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The Vehicle which contains the weapon. The vehicle object should be created before the parameter is passed.

		@param name - Name of the weapon which needs to be removed.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		VehicleUtilities::applyWeaponRemove(veh1 ,"VBS2_M2_HMG");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks This is a replication of void removeWeapon(Vehicle& vehicle, string name) function. 

		*/
		static void applyWeaponRemove(Vehicle& vehicle, const std::string& name);

		/*!
		
		@description

		This function used if a particular weapon in the vehicle is available within the VBS2 environment. It returns a true value if the weapon is present in the vehicle.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param vehicle - The vehicle which contains the weapon. The vehicle object should be created before the parameter is passed.

		@param name - Name of the weapon to check its availability. 

		@return bool

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
	
		VehicleUtilities:: isWeaponAvailable (veh1 ,"VBS2_M2_HMG");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks @remarks This is a replication of bool hasWeapon(Vehicle& vehicle, string name).

		@remarks To check if a weapon is available in a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function isWeaponAvailable(Vehicle& vehicle, string name) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool isWeaponAvailable(const Vehicle& vehicle, const std::string& name);

		/*!
		
		@description

		This function is used remove the specified magazine in the vehicle within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle which contains the magazine. The vehicle object should be created before the parameter is passed.

		@param magName - Name of the magazine to be removed. 

		@return Nothing

		@example

		@code

		applyMagazineRemove(Veh1, "vbs2_mag_100rnd_127x99_Trace_browning_m2");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void removeMagazine(Vehicle& vehicle, string name) function. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyMagazineRemove(Vehicle& vehicle, const std::string& magName);

		/*!
		
		@description

		This function is used remove all the magazines of given type in the vehicle within the VBS2 environment. 

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The Vehicle which contain the magazines. The vehicle object should be created before the parameter is passed.

		@param magName - Type of magazine to be removed. 

		@return Nothing

		@example

		@code

		applyMagazineRemove(Veh1, "vbs2_mag_100rnd_127x99_Trace_browning_m2");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void removeallMagazines(Vehicle& vehicle, string name)function.

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyMagazineRemoveAll(Vehicle& vehicle, const std::string& magName);

		/*!

		@description

		This function is used to apply the direction of the Turret weapon according to azimuth and elevation within the VBS2 environment.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle with turret weapon which have a gunner, whose input has been disabled via void applyGunnerInputDisable(Vehicle& vehicle, Turret& turret, bool status = true) function.

		@param turret - Turret which weapon belongs to.

		@param azimuth - Azimuth value in degrees.

		@param elevation - Elevation value in degrees.

		@param transition - If the boolean value transition is false, the turret 'jumps' to it's destination.

		@return Nothing

		@example

		@code

		//Create a Turret path
		Turret::TurretPath turret_path;
		turret_path.push_back(0);

		//Create a Turret
		Turret turret1;
		turret1.setTurretPath(turret_path);

		VehicleUtilities::applyWeaponDirection(vehi1, turret1, 8, 10, true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarksThis is a replication of void setWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition) function..
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		*/
		static void applyWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition);

		/*!
		@description 
		
		Sets world space azimuth and elevation for optics on turret.If direction of optics on turret is changed, weapon on the turret also changes direction to look where the optics points.

		@locality

		Locally Applied Globally Effected

		@version [VBS2Fusion v2.70.2]

		@param vehicle - A vehicle with turrets.

		@param turret - Considered turret.

		@param azimuth - azimuth in degrees.

		@param elevation - elevation in degrees.

		@return Nothing

		@example

		@code

		//Vehicle created
		Vehicle v;

		//Add turret path of the weapon to the Turret
		Turret::TurretPath tupath;
		tupath.resize(1);
		tupath.at(0) = 0;
		Turret turret
		turret.setTurretPath(tupath);

		// set turret azimuth to 90 degrees and 25 degrees elevation
		VehicleUtilities::applyOpticsDirection(tank, turret, 90, 25);

		@endcode

		@overloaded 

		@related

		@remarks To apply the optics direction for an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities::applyPositionASL(ControllableObject& co, position3D aslpos).
		*/
    #if _BISIM_DEV_APPLY_TURRET_FUNCTION
		static void applyOpticsDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation);
		
    /*!
		@description 
		
		Sets speed of turret azimuth and elevation changes in degrees per second.
		The higher speed the faster the turret changes it's orientation.
		Negative number for azimuthSpeed or elevationSpeed set the speed back to config defaults.

		@locality

		@version [VBS2Fusion v2.70.2]

		@param vehicle - A vehicle with turrets.

		@param turret - Considered turret.

		@param azimuthSpeed - azimuth speed in degrees/s.

		@param elevationSpeed - elevation speed in degrees/s.

		@return Nothing

		@example

		@code

		//Vehicle to be created.
		Vehicle v;
		//Setting up turrets and turret path.

		Turret::TurretPath tupath;
		tupath.resize(1);
		tupath.at(0) = 0;
		VBS2Fusion::Turret turret;
		turret.setTurretPath(tupath);

		//Apply azimuth and elevation speed to 1,1
		VehicleUtilities::applyTurretSpeed(v, turret, 1, 1);

		//Set direction of the turret
		VehicleUtilities::applyOpticsDirection(v, turret, 90, 25);

		@endcode

		@overloaded 

		@related

		@remarks
		*/
		static void applyTurretSpeed(Vehicle& vehicle, Turret& turret, double azimuthSpeed, double elevationSpeed);

		/*!
		@description 
		
		Switches stabilization of turret in azimuth and elevation axes on/off. 
		If set stabilization in both axes off That means that after this is applied the turret will not try to point in the world direction it did 
		but will turn together with tank as the tank turns as if it was sealed with the tank. 
		If the stabilization is on then the turret tries to always point in the same world direction which results
		in the turret staying on place when tank under it turns.

		@locality

		@version [VBS2Fusion v2.70.2]

		@param vehicle - A vehicle with turrets.

		@param turret - Considered turret.

		@param azimuthStabil - If true azimuth stabilization on.

		@param elevationStabil - If true elevation stabilization on.

		@return Nothing

		@example

		@code

		//Vehicle to be created.
		Vehicle v;
		//Setting up turrets and turret path.

		Turret::TurretPath tupath;
		tupath.resize(1);
		tupath.at(0) = 0;
		VBS2Fusion::Turret turret;
		turret.setTurretPath(tupath);

		//Set direction of the turret
		VehicleUtilities::applyOpticsDirection(v, turret, 90, 25);

		//Set the stabilization off
		VehicleUtilities::applyTurretStabilizaton(tank, turret, false, false);

		@endcode

		@overloaded 

		@related

		@remarks
		*/
		static void applyTurretStabilizaton(Vehicle& vehicle, Turret& turret, bool azimuthStabil, bool elevationStabil);

		/*!
		@description 
		Makes turret to change it's direction using given speed. If the turret is stabilized then the direction change 
		happens around world axes. When the turret is not stabilized the direction change happens around the local axes of the vehicle.
		When the turret is changing it's direction the user or AI can't interact with the turret.
		There is no specific direction to change to given so once this command is used the turret changes it's direction 
		using given speed until the speed is set to 0.

		@locality
		
		Globally Applied Globally Effected

		@version [VBS2Fusion v2.71]

		@param vehicle - A vehicle with turrets.

		@param turret - Considered turret.

		@param azimuthSpeed - speed in degrees per second of the azimuth direction change
			negative numbers means turning clockwise
			positive numbers means turning counter-clockwise

		@param elevationSpeed - speed in degrees per second of the elevation direction change
			negative numbers means turning down
			positive numbers means turning up

		@param useAimuthAcceleration - if true then the turret uses its configured acceleration to reach the given azimuth change speed.
		if false then the turret reaches given azimuth change speed immediately without accelerating.

		@param useElevationAcceleration - if true then the turret uses it's configured acceleration to reach the given elevation change speed
		if false then the turret reaches given elevation change speed immediately without accelerating.

		@return Nothing

		@example

		@code

		//Vehicle to be created.
		Vehicle v;
		//Setting up turrets and turret path.

		Turret::TurretPath tupath;
		tupath.resize(1);
		tupath.at(0) = 0;
		VBS2Fusion::Turret turret;
		turret.setTurretPath(tupath);

		//Apply the change of direction
		VehicleUtilities::applyTurretDirectionCahnge(tank, turret, 10, 5, false, false);

		@endcode

		@overloaded 

		@related

		@remarks
		*/
		static void applyTurretDirectionChange(Vehicle& vehicle, Turret& turret, float azimuthSpeed, float elevationSpeed, bool useAimuthAcceleration = false, bool useElevationAcceleration = false);

#endif
    /*!

		@description

		This function is used to disable the gunner�s input at the turret within the VBS2 environment.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - Vehicle with turret.

		@param turret - Turret to disabled the gunner input.

		@param status - If default value 'true' is set then AI will not control turret else if it sets to 'false', AI will control turret.

		@return Nothing

		@example

		@code

		//Create Turret path
		Turret::TurretPath turret_path;
		turret_path.push_back(0);

		//Create Turret
		Turret turret1;
		turret1.setTurretPath(turret_path);

		VehicleUtilities::applyGunnerInputDisable(vehi1, turret1, false);

		@endcode

		@overloaded

		None

		@related

		None

		@remarksThis is a replication of void disableGunnerInput(Vehicle& vehicle, Turret& turret, bool status = true) function.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyGunnerInputDisable(Vehicle& vehicle, Turret& turret, bool status = true);

		/*!

		@description

		This function is used to apply a specified amount (compared to a full state defined by the unit type)of ammunition to the vehicle within the VBS2 environment.

		@locality

		Globally Applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to which the ammunition is applied. The vehivle should be created before passing it as a parameter.

		@param ammovalue - The amount of ammunition to be added to the vehicle which range from 0 to 1.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyVehicleAmmo(veh1,0.5);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void setVehicleAmmo(Vehicle& vehicle, float ammovalue) and void setVehicleAmmo(Vehicle& vehicle, double ammovalue) functions.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyVehicleAmmo(Vehicle& vehicle, double ammovalue);

		/*!

		@description

		This function is used to add weapons to the cargo space of vehicle, which can be taken out by infantry units within the VBS2 environment. Once the weapon cargo space is filled usage of further applyWeaponCargo function is ignored.

		@locality

		Globally Applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle for which the weapons are added to it's cargo.

		@param weaponName - Name of the weapon.

		@param count - The number of weapons to be added.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyWeaponCargo(veh1, "VBS2_2A42_30mm", 5);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void addWeaponToCargo(Vehicle& vehicle, string weaponName, int count)functions.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyWeaponCargo(Vehicle& vehicle, const std::string& weaponName, int count);

		/*!

		@description

		This function is used to add magazines to the cargo space of vehicle, which can be taken out by infantry units within the VBS2 environment. Once the magazine cargo space is filled usage of further applyMagazineCargo function is ignored.

		@locality

		Globally Applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - Vehicle for which the weapons are added to it's cargo.

		@param magName - Name of the magazine.

		@param count - Number of magazines to be added.

		@return Nothing

		@example

		@code

		applyMagazineCargo(veh1, "vbs2_mag_100rnd_127x99_Trace_browning_m2", 5);

		@endcode

		@overloaded

		None

		@related

		None

		@remarksThis is a replication of void addMagazineToCargo(Vehicle& vehicle, string weaponName, int count)functions.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyMagazineCargo(Vehicle& vehicle, const std::string& magName, int count);

		/*!

		@description

		This function is used to remove all the weapons from the cargo space of vehicle within the VBS2 environment.

		@locality

		Globally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle for which the weapons from the cargo are removed.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyWeaponCargoRemoveAll(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarksThis is a replication of void clearAllWeaponFromCargo(Vehicle& vehicle) function.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyWeaponCargoRemoveAll(Vehicle& vehicle);

		/*!

		@description

		This function is used to remove all the magazines from the cargo space of vehicle within the VBS2 environment.

		@locality

		Globally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle for which the magazines from the cargo are removed.

		@return Nothing

		@example

		@code

		applyMagazineCargoRemoveAll(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void clearAllMagazineFromCargo(Vehicle& vehicle) function.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyMagazineCargoRemoveAll(Vehicle& vehicle);

		/*!

		@description

		This function is used enable or Disable firing of current weapon in the selected turret of the vehicle within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle with the turret.
		
		@param turret - The turret which enables or disables firing on current weapon.
		
		@param safe - If the value is true, none of the turret's weapon can be fired, and a "Safety On" status message will be visible in the player�s HUD. If the value is false, weapon can be fired normally.

		@return Nothing

		@example

		@code

		//Create Turret Path
		Turret::TurretPath turret_path;
		turret_path.push_back(0);

		//Create Turret
		Turret turret1;
		turret1.setTurretPath(turret_path);

		VehicleUtilities::applyWeaponSafety(vehi1, turret1, false);

		@endcode

		@overloaded

		None

		@related

		None

		@remarksThis is a replication of void setWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe) function.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe);

		/*!

		@description

		Used to apply the network shell mode. This function when creating shells enables the vehicle to create them as network objects.Normally shells are treated like bullets and locally simulated following a fired event. When network shell mode is set,the simulation is broadcasted to the network.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle which create shells. . The vehicle should be created before passing it as a parameter.

		@param mode - If the value is true, creates shells as network objects and the simulation is broadcaster to the network. If the value is false it is locally simulated.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyNetworkShellMode(veh1,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarksThis is a replication of void setNetworkShellMode(Vehicle& vehicle, bool mode) function.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyNetworkShellMode(Vehicle& vehicle,bool mode);

		/*!

		@description

		Used to bring an aircraft to a flying altitude within the VBS2 environment and returns the flying height that will be reached. Planes or helicopters will take off realistically to reach this height.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - Vehicle to be brought to a flying altitude. The vehicle should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyAirborne(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarksThis is a replication of double makeAirborne(Vehicle& vehicle) function.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static double applyAirborne(Vehicle& vehicle);

		/*!
		
		@description

		Used to apply the target flying altitude for aircraft. This cannot be used to start an aircraft at the given height.For this purpose setPos combined with flyInHeight in the init line is used.Avoid too low altitudes, as helicopters and planes will not evade trees and obstacles on the ground.The default flying altitude depends on the "flyInHeight" definition in the vehicle's configuration. 

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle for which the flying altitude is applied. The vehicle should be created before passing it as a parameter. 

		@param height - Flying altitude in meters (Above Ground Level).
		
		@return Nothing

		@example

		@code

		VehicleUtilities::applyHeightFlyIn(veh1, 80);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyHeightFlyIn(Vehicle& vehicle, double height);

		/*!
		
		@description

		Used to apply  the object's orientation (given as direction and up vector)within the VBS2 environment.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param veh - The vehicle for which the orientation is applied. The vehicle should be created before passing it as a parameter. 

		@param vectorDir - The direction vector applied to vehicle.

		@param vectorUp - The up vector applied to vehicle.

		@param relative - Parameter specifying if vectors are relative to parent or not.

		@return Nothing

		@example

		@code

		vector3D vec1;
		vector3D vec2;

		vec1.setX(0);
		vec1.setY(1);
		vec1.setZ(0);
		vec2.setX(0);
		vec2.setY(0);
		vec2.setZ(1);

		VehicleUtilities::applyVectorDirAndUp(veh1, vec1, vec2, true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void setVectorDirAndUp(Vehicle veh, vector3D vectorDir, vector3D vectorUp, bool relative) function. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyVectorDirAndUp(Vehicle& veh, const vector3D& vectorDir, const vector3D& vectorUp, bool relative);

		/*!
		
		@description

		Planes will use the airport defined by applyAirportAssignment function, or the one closest to them (in this priority).Helicopters will use either the nearest editor-placed helipad or the nearest flat location (in this priority). 
		applyAirportAssignment function has no effect on helicopters, and either of them have to be within 500m or it will not be landed.

		@locality

		Locally Applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to be landed. The vehicle should be created before passing it as a parameter. 

		@param mode - Landing Modes. Available modes are:
			LAND - A complete stop.
			GET_IN - Hovering very low for another unit to get in.
			GET_OUT - Hovering low for another unit to get out.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyCustomizedLanding(veh1, LANDMODE::LAND)

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void land(Vehicle& vehicle, LANDMODE mode) function. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyCustomizedLanding(Vehicle& vehicle, LANDMODE mode);

		/*!
		
		@description

		Used to assign the airport which will be used when applying either a applyCustomizedLanding function to an AI-controlled vehicle or selecting the "Landing autopilot" action if the vehicle is player-controlled.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to be assigned to the airport. The vehicle should be created before passing it as a parameter. 

		@param port - The airport code.   The available airport codes are listed on the respective map pages.

		@return Nothing

		@example

		@code

		Vehicle plane;

		VehicleUtilities::applyAirportAssignment(plane, 0);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void assignToAirport(Vehicle& vehicle, int port) function.	

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyAirportAssignment(Vehicle& vehicle, int port);

		/*!
		
		@description

		Used to apply the airport place for the vehicle to land within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to be landed in the airport. The vehicle should be created before passing it as a parameter. 

		@param port - The Air port code.   The available airport codes are listed on the respective map pages.

		@return Nothing

		@example

		@code

		Vehicle plane;

		VehicleUtilities::applyLanding(plane, 0);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void landTo(Vehicle& vehicle, int port) function. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyLanding(Vehicle& vehicle, int port);

		/*!
	
		@description
		
		Used to send message to vehicle radio channels within the VBS2 environment.  

		@locality
		
		Globally Applied Locally Effective

		@version [VBS2 2.02 / VBS2Fusion v3.08]
		
		@param vehicle - The vehicle which sends the message . The vehicle should be created before passing it as a parameter. 
		
		@param message - The message to be sent.

		@return Nothing
		
		@example
		
		@code
		
		VehicleUtilities::applyVehicleRadioBroadcast(veh1, "CEASE FIRE");

		@endcode
		
		@overloaded

		None

		@related
		
		None

		@remarks This is a replication of void sendToVehicleRadio(Vehicle& vehicle, string name) function. 
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyVehicleRadioBroadcast(Vehicle& vehicle, const std::string& message);

		/*!
		
		@description

		Used to type text to vehicle radio channel.This function only types text to the list, it does not broadcast the message. If the message needs to be shown on all computers then it should be executed on them.If theunit is in the crew seat (i.e. driver, gunner or commander), then it will include the unit�s role in the chat name output.
		E.g: Driver (you_name): "Message" 

		@locality

		Globally Applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle which sends the message . The vehicle should be created before passing it as a parameter. 

		@param message - The message to be sent.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyVehicleChat(veh1, "CEASE FIRE");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void vehicleChat(Vehicle& vehicle, string message) function.	 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyVehicleChat(Vehicle& vehicle, const std::string& message);

		/*
		
		@description

		This function is used to remove the weapon in the cargo of the vehicle within the VBS2 environment.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle which contains the weapon in the cargo. The vehicle should be created before passing it as a parameter. 

		@param weapon - The name of the  weapon which needs to be removed. 

		@return Nothing

		@example

		@code

		VehicleUtilities::applyWeaponCargoRemove(veh1, "vbs2_M2_HMG");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void removeWeaponFromCargo(Vehicle& vehicle, string weapon) function. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyWeaponCargoRemove(Vehicle& vehicle, const std::string& weapon);

		/*!
	
		@description
		
		This function is used to remove the magazine in the cargo of the vehicle within the VBS2 environment.

		@locality
		
		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]
		
		@param vehicle - The vehicle which contains the magazine in the cargo. The vehicle should be created before passing it as a parameter. 
		
		@param magazine - The name of the magazine which needs to be removed.

		@return Nothing
		
		@example
		
		@code
		
		VehicleUtilities::applyMagazineCargoRemove(veh1, "vbs2_mag_100rnd_127x99_Trace_browning_m2");

		@endcode
		
		@overloaded

		None

		@related
		
		None

		@remarks This is a replication of void remove Magazine FromCargo(Vehicle& vehicle, string magazine) function. 
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyMagazineCargoRemove(Vehicle& vehicle, const std::string& magazine);

		/*!
		
		@description

		This function is used to remove the magazine in the turret of the vehicle within the VBS2 environment.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle which contains the magazine in the turret. The vehicle should be created before passing it as a parameter. 

		@param turretPath -  The turret from which the magazine is removed.

		@param magazine - The name of the magazine which needs to be removed from the turret.
		
		@return Nothing

		@example

		@code
		
		Vehicle veh1;
		
		Turret::TurretPath tpath;
		tpath.push_back(0);
		tpath.push_back(1);

		Turret t;
		t.setTurretPath(tpath);

		applyMagazineTurretRemove(veh1, "vbs2_mag_100rnd_127x99_Trace_browning_m2", tpath);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void removeMagazineFromTurret(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath) function.

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyMagazineTurretRemove(Vehicle& vehicle, const std::string& magazine, Turret::TurretPath& turretPath);

		/*!

		@description

		This function is used to add the magazine to the cargo space of the vehicle within the VBS2 environment.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to which the magazine is added. The vehicle should be created before passing it as a parameter.
		
		@param turretPath - The turret from which the magazine is removed.
		
		@param magazineName - The name of the magazine which needs to be added.
		
		@param bulletCount - The Number of bullets.

		@return Nothing

		@example

		@code

		VehicleUtilities::applySingleMagazineCargo(vehi1, "vbs2_mag_100rnd_127x99_Trace_browning_m2", 10);
		
		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void removeMagazineFromTurret(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath) function.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applySingleMagazineCargo(Vehicle& vehicle, const std::string& magazineName, int bulletCount);

		/*!

		@description

		This function checks the knowledge the vehicle commander has about the target within the VBS2 environment.

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to be checked. The vehicle should be created before passing it as a parameter.
		
		@param target - The target object.

		@return double

		@example

		@code

		Vehicle Veh1;
		ControllableObject co;

		VehicleUtilities::getKnowsAbout(veh1, co);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void removeMagazineFromTurret(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath) function.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/		
		static double getKnowsAbout(const Vehicle& vehicle, const ControllableObject& target);

		/*!

		@description

		This function locks or unlocks the vehicle for the player unit within the VBS2 environment.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to which the above function is applied. The vehicle should be created before passing it as a parameter.
		
		@param playerLock - true or false value depending the vehicle needs to be locked or unlocked.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyVehicleLockEnable(veh1, true);

		@endcode

		@overloaded
		
		void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock);

		@related

		None

		@remarks This is a replication of void lockVehicle(Vehicle& vehicle, bool playerLock, bool allLock) function.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock);

		/*!

		@description

		This function locks or unlocks the vehicle for the player unit within the VBS2 environment.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to which the above function is applied. The vehicle should be created before passing it as a parameter. 
		
		@param playerLock - true or false value depending the vehicle needs to be locked or unlocked.
		
		@param allLock - true or false value to indicate whether or not the vehicle is locked for all the units including the player.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyVehicleLockEnable(Veh1, false, false)

		@endcode

		@overloaded

		void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock);

		@related

		None

		@remarks This is a replication of void lockVehicle(Vehicle& vehicle, bool playerLock, bool allLock) function.
		
		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock, bool allLock);

		/*!

		@description

		This function enables the group to leave the vehicle within the VBS2 environment.

		@locality

		Locally Applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle -The vehicle to which the above function is applied. The vehicle should be created before passing it as a parameter. 
		
		@param group - The group which is assigned to leave the vehicle.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		//The group to be created
		Group gr1;

		VehicleUtilities::applyGroupLeave(veh1, gr1);

		@endcode

		@overloaded

		applyGroupLeave(Vehicle& vehicle, Unit& unit);

		@related

		None

		@remarks This is a replication of void groupLeaveVehicle(Group& group, Vehicle& vehicle) function. 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyGroupLeave(Vehicle& vehicle, Group& group);

		/*!

		@description

		This function enables the unit in the group to leave the vehicle within the VBS2 environment.

		@locality

		Locally Applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to which the above function is applied. The vehicle should be created before passing it as a parameter. 
		
		@param unit - The unit which is assigned to leave vehicle.

		@return Nothing

		@example

		@code

		//The vehicle to be created
		Vehicle veh1;

		//The unit to be created
		Unit u;

		VehicleUtilities::applyGroupLeave(veh1, u);

		@endcode

		@overloaded

		void applyGroupLeave(Vehicle& vehicle, Group& group);

		@related

		None

		@remarks This is a replication of void groupLeaveVehicle(Unit& unit, Vehicle& vehicle) function.

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyGroupLeave(Vehicle& vehicle, Unit& unit);

		/*!

		@description

		This function sets the leader to issue attack commands to the soldiers within the VBS2 environment if the boolVal is true.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to which the above function is applied. The vehicle should be created before passing it as a parameter. 
		
		@param boolVal - If the value is true the leader can issue commands to the soldiers.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyAttackEnable(veh1, true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void setEnableAttack(Vehicle& vehicle, bool boolVal) function.

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyAttackEnable(Vehicle& vehicle, bool boolVal);

		/*!

		@description

		This function sets the rotor wash effect of helicopters (applies to all helicopters in mission).This affects the dust created by the helicopter, as well as the wind effect on objects nearby.While it is not possible to turn off the dust effect altogether, it is possible to make it invisible by using very high values (e.g. [100,100]) in the parameters. This only works if there are no objects nearby which might be affected by the wind generated(e.g. trees), as it would show unrealistic wind strain.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param strength - The force of rotor wash.
		
		@param diameter - The maximum range of the diameter.

		@return Boolean

		@example

		@code

		VehicleUtilities::::applyRotorWash(45, 62);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of bool setRotorWash(double strength, double diameter) function. 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static bool applyRotorWash(double strength, double diameter);

		/*!

		@description

		This function applies vehicle's lights states for vehicle within the VBS2 environment(-2 forced off, -1 off (user controlled), 0 user controlled, 1 on (user controlled), 2 forced on)

		@locality

		Globally Applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - Vehicle that lights states are applied to. The vehicle should be created before passing it as a parameter. 
		
		@param main	- Driving lights (head lights, rear lights and brake lights).
		
		@param side	- Side lights (small edge indicator lights - only available on some vehicles).
		
		@param convoy - White rear light for convoy driving (only available on some vehicles). Activating the convoy light will disable driving and indicator lights while on.
		
		@param left	- Left turn signal.
		
		@param right - Right turn signal.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyVehicleLights(veh1,1,0,0,1,1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void setVehicleLights(Vehicle& vehicle,int main, int side, int convoy, int left, int right) function.

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyVehicleLights(Vehicle& vehicle, int main, int side, int convoy, int left, int right);

		/*!

		@description

		This function controls the screen blanking during certain actions within the VBS2 environment.

		@locality

		Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle for which fading is applied. The vehicle should be created before passing it as a parameter. 
		
		@param actions	- Action with an associated screen blanking. Supported actions are VA_GETOUT - GETOUT,VA_TURNOUT - TURNOUT,VA_TURNIN - TURNIN,
		
		@param value - If the value is �true' fading is enabled else disabled.
		
		@return Nothing

		@example

		@code

		VehicleUtilities::applyBlackFadeEnable(veh1, VA_GETOUT, false);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void allowBlackFade(Vehicle& vehicle, VEHICLE_ACTION actions, bool value) function. 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyBlackFadeEnable(Vehicle& vehicle, VEHICLE_ACTION actions, bool value);

		/*!

		@description

		This function orders the unit to leave the vehicle (silently) within the VBS2 environment. 

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit - The unit which needs to be moved out of the vehicle.

		@return Nothing

		@example

		@code

		Unit u;

		VehicleUtilities::applyGetOut(u);

		@endcode

		@overloaded

		VehicleUtilities::applyGetOut(vector<Unit> unitVec);

		@related

		None

		@remarks This is a replication of void doGetOut(Unit& unit) function. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyGetOut(Unit& unit);

		/*!
		
		@description

		This function orders the array of units  to leave the vehicle (silently) within the VBS2 environment. 

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unitVec - The units  which needs to be moved out of the vehicle.

		@return Nothing

		@example

		@code

		vector<Unit> vu;
		vu.push_back(u1);
		vu.push_back(u2);
		vu.push_back(u3);

		VehicleUtilities::applyGetOut(vu);

		@endcode

		@overloaded

		VehicleUtilities::applyGetOut(Unit& unit);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyGetOut(std::vector<Unit> unitVec);

		/*!
		
		@description

		This function orders the unit to get out from the vehicle (via the radio). This command is only when used on local units. When used on a remote unit, this command will create radio dialog on the machine the unit is local to, but the unit will not leave the vehicle.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit - The units  which needs to be moved out of the vehicle.

		@return Nothing

		@example

		@code

		Unit u;

		VehicleUtilities::applyCommandGetOut(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void commandGetOut(Unit& unit) function. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyCommandGetOut(Unit& unit);

		/*!
		
		@description

		This function attaches a statement to a vehicle within the VBS2 environment. The statement is propagated over the network in MP games, it can be executed by invoking processInitCommands. 

		@locality

		Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to which the statement is attached.  The vehicle should be created before passing it as a parameter. 

		@param statement - The statement to be applied.

		@return Nothing

		@example

		@code

		VehicleUtilities::applyInitStatement(veh1,"hint hi");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void setInitStatement(Vehicle& vehicle, string& statement) function. 

		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyInitStatement(Vehicle& vehicle, const std::string& statement);

		/*!
		
		@description

		This function clears the initial statement to a vehicle within the VBS2 environment.

		@locality

		Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle in which the statement is cleared.  The vehicle should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		VehicleUtilities::applyInitStatementClear(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void clearInitStatement(Vehicle& vehicle) function. 

		*/
		static void applyInitStatementClear(Vehicle& vehicle);

		/*!
		
		@description

		This function adds weapons to the weapon cargo space within the VBS2 environment. This is used for infantry weapons. MP synchronized. 

		@locality

		Globally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to which the weapons are added.  The vehicle should be created before passing it as a parameter. 

		@param weapon - The name of the weapons to be added.  Class name of the weapon should be stated.

		@param count - Number of weapons to be added to the cargo. 
		
		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		//The weapon to be added
		Weapon w1;
		w1.setWeaponName("M16")

		VehicleUtilities::applyWeaponCargoGlobal(veh1,w1,1 );

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void addWeaponToCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count) function. 

		@remarks To add a weapon to a   vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyWeaponCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count )
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		*/
		static void applyWeaponCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count);

		/*!
		
		@description

		This function adds magazine to the cargo space within the VBS2 environment. This is used for infantry magazine. MP synchronized. 

		@locality

		Globally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to which the magazine is added.  The vehicle should be created before passing it as a parameter. 

		@param magName - The name of the magazine to be added.  Class name of the weapon should be stated.
		
		@param count - Number of magazines to be added to the cargo. 

		@return Nothing

		@example

		@code

		VehicleUtilities::applyMagazineCargoGlobal(veh1,"M16",1 );

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks This is a replication of void addMagazineToCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count) function. 
		
		@remarks To add a magazine  to a   vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyMagazineCargoGlobal(Vehicle& vehicle, string magName, int count) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void applyMagazineCargoGlobal(Vehicle& vehicle, const std::string& magName, int count);

		/*!
		
		@description

		This function removes the weapon from the cargo space within the VBS2 environment. MP synchronized. 

		@locality

		Globally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle from which the weapon  is removed.  The vehicle should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
	
		VehicleUtilities::applyWeaponCargoRemoveGlobal(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To remove weapons from a   vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyWeaponCargoRemoveGlobal(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
 
		@remarks This is a replication of void clearWeaponCargoGlobal(Vehicle& vehicle) function.

		*/
		static void applyWeaponCargoRemoveGlobal(Vehicle& vehicle);

		/*!
		
		@description

		This function removes magazine from the cargo space within the VBS2 environment. MP synchronized. 

		@locality

		Globally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle from which the magazine are removed.  The vehicle should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		VehicleUtilities::applyMagazineCargoRemoveGlobal(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To remove magazines from a   vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyMagazineCargoRemoveGlobal (Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks This is a replication of void clearMagazineCargoGlobal(Vehicle& vehicle) function.
		
		*/
		static void applyMagazineCargoRemoveGlobal(Vehicle& vehicle);

		/*!
		
		@description

		This function moves the vehicle to move to the specified position within the VBS2 environment.

		@locality

		Locally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle to be moved.  The vehicle should be created before passing it as a parameter. 

		@param position - The position where the vehicle is to be moved.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		VehicleUtilities::applyCommandMove(veh1, player.getPosition()+position3D(50,50,0));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void commandMove(Vehicle& vehicle, position3D position) function. 

		*/
		static void applyCommandMove(Vehicle& vehicle, const position3D& position);

		/*!
		
		@description

		This function enables the vehicle to fire using a specified weapon within the VBS2 environment.

		@locality

		Globally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle which fires.  The vehicle should be created before passing it as a parameter. 

		@param weaponName - The name of the weapon which is fired from.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		VehicleUtilities::applyFire(veh1, "VBS2_M2_HMG");

		@endcode

		@overloaded

		VehicleUtilities::applyFire(Vehicle& vehicle, string muzzleName, string modeName);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void fire(Vehicle& vehicle, string weaponName) function. 

		@remarks To make a  vehicle created in a client machine within a network fire, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyFire(Vehicle& vehicle, string weaponName) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		*/
		static void applyFire(Vehicle& vehicle, const std::string& weaponName);

		/*!
		
		@description

		This function enables the vehicle to fire using a specified weapon within the VBS2 environment.

		@locality

		Globally Applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle which fires.  The vehicle should be created before passing it as a parameter. 

		@param muzzleName - The name of the muzzle which is fired from.

		@param modeName - The mode of the weapon.

		@return Nothing
		
		@example

		@code

		VehicleUtilities::applyFire(veh1, "VBS2_M2_HMG", "FullAuto");

		@endcode

		@overloaded

		VehicleUtilities::applyFire(Vehicle& vehicle, string weaponName);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void fire(Vehicle& vehicle, string muzzleName, string modeName)function 
		
		@remarks To make a  vehicle created in a client machine within a network fire, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applyFire(Vehicle& vehicle, string muzzleName, string modeName) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)


		*/
		static void applyFire(Vehicle& vehicle, const std::string& muzzleName, const std::string& modeName);

		/*!
		
		@description

		This function enables  or disables specific sound types for a vehicle within the VBS2 environment.

		@locality

		Globally Applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle which is to be enabled or disabled for sound type.  The vehicle should be created before passing it as a parameter. 

		@param types - Sound types. Available sound types are;
			- SOUNDENGINE - Engine
			- SOUNDENVIRON - Movement sounds (e.g. tank tracks)
			- SOUNDCRASH - Impact with another object
			- SOUNDDAMMAGE - Alarm sound if vehicle was hit
			- SOUNDLANDCRASH - Impact with land (e.g. aircraft crash)
			- SOUNDWATERCRASH - Impact with water (e.g. aircraft crash)
			- SOUNDGETIN - Entering vehicle
			- SOUNDGETOUT - Exiting vehicle
			- SOUNDSERVO - Vehicle attachments (e.g. tank turrets)

		@param value - Whether sound type should be played or not. If 'true' sound type will be played.

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		VehicleUtilities::applySoundEnable(veh1, VEHICLE_SOUND_TYPES::SOUNDLANDCRASH, true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void allowSound(Vehicle& vehicle,VEHICLE_SOUND_TYPES types,bool value) function. 

		@remarks To enable/disable sounds for a  vehicle created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities:: applySoundEnable(Vehicle& vehicle, VEHICLE_SOUND_TYPES types, bool value) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		*/
		static void applySoundEnable(Vehicle& vehicle, VEHICLE_SOUND_TYPES types, bool value);

		/*!
		
		@description

		This function returns the path to the turret (as a TurretPath object) the unit is in, on the given vehicle.
		VehicleUtilities::updateVehicle' and 'VehicleUtilities::updateTurrets' must be called on the vehicle object,before calling this function.
		The Turret Path of the turret which the unit is mounted on.
		If the unit is not mounted on the given vehicle OR the unit is mounted on some other crew position other than a turret, an empty Turret Path will be returned.
		The function Returns an empty TurretPath, If the Vehicle is not active or if the Unit is not active or if the Unit is not in the Vehicle 
		or if the Vehicle does not have any VehicleArmedModule or if the Unit is not in a GUNNER/COMMANDER position.

		@locality

		Locally applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle for which the above function is applied.  The vehicle should be created before passing it as a parameter. 

		@param unit - The unit which is mounted inside the vehicle

		@return Nothing

		@example

		@code

		Vehicle veh1;

		Unit u;

		Turret::TurretPath tpath = VehicleUtilities::getTurretPathOfUnit(veh1, u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static Turret::TurretPath getTurretPathOfUnit(Vehicle& vehicle, Unit& unit);

		/*!
		
		@description

		This function returns the absolute speed of the vehicle calculated using the velocity of the vehicle. 

		@locality

		Globally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle for which the above function is applied.  The vehicle should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The vehicle  to be created
		Vehicle veh1;
		
		displayString+="the speed is "+conversions::DoubleToString(VehicleUtilities::getAbsoluteSpeed(veh1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the absolute speed  of a  vehicle created in a client machine within a network, use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function VehicleUtilities::getAbsoluteSpeed(Vehicle& vehicle) 
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getAbsoluteSpeed(const Vehicle& vehicle);

		/*!
		
		@description

		This function returns the absolute speed of the vehicle calculated using the velocity of the vehicle. 

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicleType - The type of the vehicle.
		
		@param turretName - The name of the turret.

		@return Nothing

		@example

		@code

		Turret::TurretPath t;

		t = VehicleUtilities::getTurretPathByName("vbs2_us_army_m109a6_d_x","maingun");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static Turret::TurretPath getTurretPathByName(const std::string& vehicleType, const std::string& turretName);

		/*!
		@description

		"Respawns" the vehicle passed as a parameter. The initial vehicle is deleted and a new vehicle is created. Units may lose their seat in some vehicles.

		@locality  

		Globally Applied Globally Effected

		@version [VBS2Fusion v3.10.1]

		@param vehicle - Vehicle that is suppose to Respawn.

		@return bool - If false some unit or units might have lost their seats. (ex: If vehicle has two gunner's position one gunner might be not in vehicle after respawns)

		@example

		@code

		//Vehicle to be created
		Vehicle v;

		//Apply the applyVehicleRespawn function to the vehicle v
		VehicleUtilities::applyVehicleRespawn(v);

		@endcode

		@relates to 

		@overloaded

		@remarks To obtain a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network.
		*/
		static bool applyVehicleRespawn(Vehicle& vehicle);
		
		/*!
		@description 

		Checks whether the specified vehicle is currently on top of another vehicle (e.g. a trailer). Only works with physX vehicles.
		
		@locality

		Locally Effected

		@version  [VBS2Fusion v3.12]

		@param vehicle - Specified vehicle that suppose to check whether top of another vehicle or not.

		@return bool - If true then on top of another vehicle.

		@example

		@code

		//The vehicle to be created
		Vehicle vehi;

		VehicleUtilities::isDrivingOn(vehi);

		@endcode

		@overloaded  

		None
		
		@related

		None

		@remarks This checks if a vehicle is on top of a Trailer vehicle type. Would work accordingly for object typ "VBS2_CA_ARMY_HET_TRAILER_D_X"
		*/
		static bool isDrivingOn(const Vehicle& vehicle);

		/*!
		@description 
		Returns a list of groups that are in cargo of the passed vehicle. Each group in the return list need to be updated.

		@locality
		
		Locally effected, locally applied

		@version  [VBS2Fusion v3.12.1]

		@param vehicle - A passed vehicle.

		@return list<Group> - list of groups.

		@example

		@code
				
		VehicleUtilities::updateVehicle(tank);
		list<Group> myList =VehicleUtilities::getCargoGroups(tank);
		for (list<Group>::iterator itr = myList.begin(), itr_end = myList.end(); itr != itr_end; ++itr)
		{
			displayString+="\\nThe name is "+itr->getName();
		}

		@endcode

		@overloaded

		@related
		void GroupUtilities::updateGroup(Group& group)

		@remarks
		This function works for most of the cargo vehicles. It will not work for non cargo vehicles.

		*/
		static std::list<Group> getCargoGroups(const Vehicle& vehicle);

		/*!
		@description 
		Returns the vehicle position occupied by the passed unit.

		@locality
		
		Globally effected

		@version  [VBS2Fusion v3.15]

		@param vehicle - A passed vehicle.
		@param unit - Unit that suppose to find crew position.

		@return CrewPos - This is the struct that holds crew position description. If instance is crewDesc
					crewDesc.type - Proxy type. Can be "driver", "gunner", "commander" or "cargo".
					crewDesc.position - Seat position in model space (i.e. relative to the vehicle).
					crewDesc.cargoID - Index of cargo position.
					crewDesc.unit - Unit occupying that seat. objNull if empty.
					crewDesc.turret - Turret path. For non-gunner positions this element is not returned. 

		@example

		@code
		
		CrewPos driverDetails = VehicleUtilities::getCrewPosition(lorry,driver);
				
		Turret::TurretPath TPDriver = driverDetails.turret;
		
		for (vector<int> :: iterator itrD = TPDriver.begin();itrD != TPDriver.end(); itrD++)
		{
			displayString +="\\nDriver TurretPath: "+ conversions::IntToString(*itrD);
		}
		
		@endcode

		@overloaded
		vector<CrewPos> getCrewPosition(Vehicle& vehicle)

		@related

		@remarks
		*/
		static CrewPos getCrewPosition(const Vehicle& vehicle, const Unit& unit);

	private:

		/*!
		
		@description

		This function checks for the existence of a group when creating a vehicle within the VBS2 environment. If there are no groups a random group is created.

		@locality

		Locally Applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param vehicle - The vehicle for which a group is created.  The vehicle should be created before passing it as a parameter. 

		@return group

		@example

		@code

		VehicleUtilities::createGroup(veh1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static Group createGroup(Vehicle& vehicle);


	};
};
#endif //VEHICLE_UTILITIES_H