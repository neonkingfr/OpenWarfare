/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

WaypointUtilities.h

Purpose:

This file contains the declaration of the WaypointUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			27-03/2009	RMR: Original Implementation
1.01		27-08/2009  SDS: Added updateIsLocal(Waypoint& wp);			
							 updated updateWaypoint(Waypoint& wp);
2.0			11-01/2009	UDW: Version 2 Implementation
2.01		05-02/2010	MHA: Comments Checked
2.02		10-01/2011  CGS: Added updateNumber(Waypoint& wp)
								deleteWaypoint(Waypoint& wp )
2.03		08-09/2011	CGS: Added getAttachedObject(Waypoint& wp)
								setHousePosition(Waypoint& wp, int pos)
								getHousePosition(Waypoint& wp)
								WaypointUtilities::setWaypointVariable(Waypoint& wp, string name, VBS2Variable& val, bool publ)
								WaypointUtilities::getWaypointVariable(Waypoint& wp, string name)
2.04		04-11/2011	CGS: Added void applyRandomPosition(Waypoint& wp, position3D center, double radius)

/************************************************************************/

#ifndef VBS2FUSION_WAYPOINT_UTILITIES_H
#define VBS2FUSION_WAYPOINT_UTILITIES_H

#include <string>
#include <list>

#include "position3D.h"
#include "data/Waypoint.h"
#include "data/VBSVariable.h"

namespace VBSFusion
{

	class VBSFUSION_API WaypointUtilities
	{
	public:

		/*!
		@description  This function returns the position of the selected waypoint created within the VBS Environment. The returned Z-value is always 0, even if an elevation is defined. To read the aircraft waypoint properties, use the object variable "WAYPOINT_AIR". 
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint to which the position is required.
		
		@return position3D
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		displayString = "The position of waypoint is:" +(WaypointUtilities::getPosition(wp1).getVBSPosition());

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyPosition(const Waypoint& wp)
		
		@remarks - Error checking utility validates the following:- Alias (outputs an error if the alias is invalid)	
		
		*/
		static position3D getPosition(const Waypoint& wp);

		/*!
		@description  This function sets the waypoint to the position given by wp.getPosition() to a group or to a Group created within the VBS environment.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected Waypoint to which the position is applied.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The Waypoint to be created
		Waypoint wp1;
		//The group to be created
		Group grp;
		//Waypoint added to the Group
		GroupUtilities::createAndAddUnit(grp,u1)
		
		//set the position of the Waypoint
		position3D wpOffset(100, 120, 0);
		wp1.setPosition(wpOffset + player.getPosition());

		WaypointUtilities::applyPosition(wp1);

		@end code
	
		@overloaded
		
		WaypointUtilities::applyPosition(const Waypoint& wp, const position3D& position);	

		@related
		
		WaypointUtilities::updatePosition(Waypoint& wp)
		
		@remarks The position of the Waypoint should be set using the function setPosition(const position3D& position) before the position is applied.

		*/
		static void applyPosition(const Waypoint& wp);

		/*!
		@description  This function sets the waypoint to the position given by position to a group or to a Group created within the VBS3 environment. Use update to check if changes have been applied.  

		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint to which the position is applied.
		
		@param position  The position to be applied to the waypoint of the Group or group. 
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		//The group to be created
		Group grp;
		//Waypoint added to the Group
		GroupUtilities::createAndAddWaypoint( grp,wp1);
		WaypointUtilities::applyPosition(wp1,player.getPosition()+position3D(0,400,0));	
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyPosition(const Waypoint& wp, const position3D& position);	

		@related
		
		WaypointUtilities::updatePosition(Waypoint& wp)
		
		@remarks Error checking utility validates the following:- Alias (outputs an error if the alias is invalid)	
		*/

		static void applyPosition(const Waypoint& wp, const position3D& position);	

		/*!
		@description  This function updates the position of the waypoint of an object created within the VBS Environment in the VBSFusion end and save using wp.setPosition(). 

		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the position is updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::updateWaypoint(wp1);
		displayString+="The position of waypoint1 is: " +(wp1.getPosition().getVBSPosition());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyPosition(const Waypoint& wp, const position3D& position);
		WaypointUtilities::applyPosition(const Waypoint& wp)
		
		@remarks

		*/

		static void updatePosition(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		@description  This function returns the direction for a waypoint (used for aircraft path optimization) applied to a group or a Group created within the VBS Environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the direction is obtained.
		
		@return double
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString+="The direction is:  "+conversions::DoubleToString(WaypointUtilities::getDirection(wp1));

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyDirection(const Waypoint& wp);
		
		WaypointUtilities::updateDirection(Waypoint& wp)	
		
		@remarks 
		
		*/
		static double getDirection(Waypoint& wp);

		/*!
		@description  This function applies the waypoint pointing direction given by wp.getDirection(). Used for aircraft path optimization.		 
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint to which the direction is applied.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//The group to be created
		Group grp;

		//Waypoint added to the Group
		GroupUtilities::createAndAddWaypoint( grp,wp1);

		//set the position of the waypoint
		position3D wpOffset(100, 120, 0);
		wp1.setPosition(wpOffset + player.getPosition());
		WaypointUtilities::applyPosition(wp1);

		//set the direction of the waypoint
		Wp1.setDirection(98.6);
		WaypointUtilities::applyDirection(wp1);
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyDirection(const Waypoint& wp, double direction);

		@related
		
		WaypointUtilities::updateDirection(Waypoint& wp);

		@remarks The direction should be set using the function below before applying the direction
		
		setDirection(double direction);

		@remarks The function WaypointUtilities::applyDirection(const Waypoint& wp) is used along with creating Waypoints.
		
		*/
		static void applyDirection(const Waypoint& wp);		

		/*!
		@description  This function applies the waypoint pointing direction given by given by direction. Used for aircraft path optimization.	
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint to which the direction is applied.
		
		@param direction  The direction to be applied to the selected waypoint .
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//The group to be created
		Group grp;

		//Waypoint added to the Group
		GroupUtilities::createAndAddWaypoint( grp,wp1);

		//set the position of the waypoint
		position3D wpOffset(100, 120, 0);
		wp1.setPosition(wpOffset + player.getPosition());
		WaypointUtilities::applyPosition(wp1);

		//apply the direction
		WaypointUtilities::applyDirection(wp1,185.0);

		@end code
		
		@overloaded
		
		WaypointUtilities::applyDirection(const Waypoint& wp);	
		
		@related
		
		WaypointUtilities::updateDirection(Waypoint& wp)
		
		WaypointUtilities::getDirection(Waypoint& wp);

		@remarks The function WaypointUtilities::applyDirection(const Waypoint& wp) is used along with creating Waypoints.
		
		@remarks Error checking utility validates the following:- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyDirection(const Waypoint& wp, double direction);

		/*!
		@description  This function updates the direction of the waypoint in the VBSFusion end and save using wp.setDirection().
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint which needs the direction to be updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		// The waypoint updated on the fusion end
		WaypointUtilities::updateDirection(wp1);

		displayString+="The direction is:  "+conversions::DoubleToString(wp1.getDirection());

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getDirection(Waypoint& wp);
		
		@remarks 
		
		*/
		static void updateDirection(Waypoint& wp);

	//	//-------------------------------------------------------------------------

		/*!
		@description  This function obtains the description of the waypoint created within the VBS Environment.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the description is required.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString+="The description of waypoint:  "+(WaypointUtilities::getDescription(wp1));

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyDescription(const Waypoint& wp);
		
		WaypointUtilities::updateDescription(Waypoint& wp);	
		
		@remarks Error checking utility validates the following:- Alias (outputs an error if the alias is invalid)		
		
		*/
		static std::string getDescription(Waypoint& wp);

		/*!
		@description This function applies the description to the waypoint created within the VBS Environment. Description is shown in the HUD while the waypoint is active.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint to which the description is applied. 
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//The group to be created
		Group grp;

		//Waypoint added to the Group
		GroupUtilities::createAndAddWaypoint( grp,wp1);

		Wp1.setDescription(string("waypoint1"));
		WaypointUtilities::applyDescription(wp1);

		@end code
		
		@overloaded
		
		WaypointUtilities::applyDescription(const Waypoint& wp, const string& description);
		
		@related
		
		WaypointUtilities:: updateDescription(Waypoint& wp)

		@remarks The description should be set using the function below before applying WaypointUtilities::applyDescription(const Waypoint& wp).
		setDescription(const string& strDescription)
		
		*/
		static void applyDescription(const Waypoint& wp);		

		/*!
		@description This function applies the description to the waypoint created within the VBS Environment. Description is shown in the HUD while the waypoint is active.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint to which the description is applied. 
		
		@param description - The description to be applied to the selected waypoint of the Group or group.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//The group to be created
		Group grp;

		//Waypoint added to the Group
		GroupUtilities::createAndAddWaypoint( grp,wp1);

		// apply the description
		WaypointUtilities::applyDescription(wp1,string("Waypoint"));

		@end code
		
		@overloaded
		
		WaypointUtilities::applyDescription(const Waypoint& wp);
		
		@related
		
		WaypointUtilities::updateDescription(Waypoint& wp);
		
		@remarks 
		
		*/
		static void applyDescription(const Waypoint& wp, const std::string& description);		

		/*!
		@description This function updates the direction of the waypoint in the fusion end and save using wp.setDirection(). 
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the description is updated in the fusion end.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::updateDescription(wp1);
		displayString="The description of waypoint 1 is:  "+(wp1.getDescription());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyDescription(const Waypoint& wp, const string& description);
		
		@remarks 
		
		*/
		static void updateDescription(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		@description  This function obtains the ShowMode of the waypoint in string format. This is the waypoints show/hide status. Possible values are: "NEVER", "EASY" and "ALWAYS".
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the show mode is obtained.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString= "show mode is: "+(WaypointUtilities::getShowMode(wp1));
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyShowMode(const Waypoint& wp)
		
		WaypointUtilities::updateShowMode(Waypoint& wp);

		@remarks 

		*/
		static std::string getShowMode(Waypoint& wp);

		/*!
		@description  This function applies the condition determining when the waypoint created within the VBS Environment is shown. Apply the ShowMode given by wp.getWaypointShowMode() to the waypoint.	
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the show mode is applied. 
		
		@return Nothing
		
		@example 
		
		@code
		// The waypoint to be created
		Waypoint wp1;

		//The group to be created
		Group grp;

		//Waypoint added to the Group
		GroupUtilities::createAndAddWaypoint( grp,wp1);

		// Set the show mode value
		wp1.setWaypointShowMode(string("ALWAYS"));

		//apply the show mode
		WaypointUtilities::applyShowMode(wp1);

		@end code
		
		@overloaded
		
		WaypointUtilities::applyShowMode(const Waypoint& wp, const string& showMode);	
		
		@related
		
		WaypointUtilities::updateShowMode(Waypoint& wp);

		@remarks The show mode of the waypoint should be set using the function below before applying WaypointUtilities:: applyShowMode(const Waypoint& wp).
		
		//Set the waypoint show mode mode using a WAYPOINTSHOWMODE variable. The possible values are: NEVER, EASY or ALWAYS.
		setWaypointShowMode(WAYPOINTSHOWMODE _type);
		
		Or

		//Set the waypoint show mode mode using a string variable The possible values are: NEVER, EASY or ALWAYS.
		setWaypointShowMode(const string& strShowMode);
		
		*/
		static void applyShowMode(const Waypoint& wp);		

		/*!
		@description  This function applies the condition determining when the waypoint created within the VBS Environment is shown. 
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the show mode is applied. 
		
		@param showMode  The showmode to be applied on the selected waypoint.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//The group to be created
		Group grp;

		//Waypoint added to the Group
		GroupUtilities::createAndAddWaypoint( grp,wp1);

		WaypointUtilities::applyShowMode(wp1,string ("ALWAYS"));
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyShowMode(const Waypoint& wp);	
		
		@related
		
		WaypointUtilities::updateShowMode(Waypoint& wp);

		@remarks 
		
		*/
		static void applyShowMode(const Waypoint& wp, const std::string& showMode);		

		/*!
		@description  This function update the show mode of the waypoint created within the VBS Environment in the fusion end .
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the show mode is updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		
		//updates the showmode in the Fusion end
		WaypointUtilities::updateShowMode(wp1);
		displayString="The show mode is: "+(wp1.getWaypointShowModeString());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getShowMode(Waypoint& wp);

		@remarks 
		
		*/
		static void updateShowMode(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		@description  This function returns the waypoint type of the waypoint created within the VBS environment in string format.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp - The selected waypoint for which the type is obtained.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString="the type is"+(WaypointUtilities::getType(wp1));

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyType(const Waypoint& wp)
		
		@remarks Error checking utility validates the following: - Alias (outputs an error if the alias is invalid)		
		
		*/
		static std::string getType(Waypoint& wp);

		/*!
		@description  This function sets the type of the waypoint using wp.getWaypointType() for the waypoint created within the VBS environment.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp -  The selected waypoint for which the type is applied.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//sets the type of waypoint
		wp1.setWaypointType(string("GETOUT"));
		WaypointUtilities::applyType(wp1);
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyType(const Waypoint& wp, const string& type);
		
		@related
		
		WaypointUtilities::getType(Waypoint& wp);
		
		@remarks The type of the waypoint should be set using the function below before applying WaypointUtilities::applyType(const Waypoint& wp).
		Set the waypoint type using string variable where strType values are:
		"MOVE", "DESTROY", "GETIN", "SAD", "JOIN", "LEADER", "GETOUT", "CYCLE", "LOAD", "UNLOAD", "TR_UNLOAD", "HOLD", "SENTRY", "GUARD", "TALK", "SCRIPTED", "SUPPORT", "GETIN_NEAREST", "AND" or "OR". 		
		setWaypointType(const string& _strType);

		*/
		static void applyType(const Waypoint& wp);		

		/*!
		@description - This function sets the type of the waypoint for the waypoint created within the VBS environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp -  The selected waypoint for which the type is applied.
		
		@param type  The type to be applied to the waypoint. 
		
		The possible values are:
		"AND": WP is only completed if all synchronized GameLogic WPs return true.
		"CYCLE": The nearest of the group's WP will become the current WP.
		"DESTROY": Group will attempt to destroy the object near the WP.
		"DISMISS": Groups will split and move into random directions.
		"GETIN NEAREST": Group will board a vehicle that's within 50m.
		"GETIN": Group will board vehicle.
		"GETOUT": Group will unboard vehicle.
		"GUARD": Used in conjunction with "Guarded by" triggers, to protect a specified area.
		"HOLD": Group will stay at this position until the WP type is switched via script.
		"JOIN": Group will merge with other group (which will become the leader).
		"LAND": Aircraft will land at nearest possible position.
		"LEADER": Group will merge with other group (and become the leader).
		"LOAD": Vehicle will load nearby units.
		"LOITER": Aircraft will fly circles around this WP (requires setWaypointLoiterType definition):
		"MOVE": Group will move to the specified location.
		"OR": WP is completed if any of the synchronized GameLogic WPs returns true.
		"SAD" (Search and Destroy): Group will search area for enemies.
		"SCRIPTED": Defined script will be executed, and group will skip to the next WP.
		"SENTRY": Group will stay at WP until it has identified any present enemies.
		"SUPPORT": Group will move to WP, and wait for a "Call Support" request.
		"TALK": Group leader will move its lips and voice any phrases selected in the "Effects" section.
		"TAXI": Maneuver aircraft to/from runway/helipad.
		"TR UNLOAD": Units in cargo (which are of a different group) will be unloaded from vehicle.
		"UNLOAD": Group members in cargo will be unloaded from vehicle.

		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//applies the type for the waypoint
		WaypointUtilities::applyType(wp1,string("JOIN"));
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyType(const Waypoint& wp)
		
		@related
		
		WaypointUtilities::getType(Waypoint& wp)
		
		@remarks 	
		
		*/
		static void applyType(const Waypoint& wp, const std::string& type);		

		/*!
		@description  This function update the type of the waypoint created within the VBS Environment in the fusion end save using wp.setWaypointType().
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp -  The selected waypoint for which the type is updated.
		
		@return  Nothing 
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::updateType(wp1);

		displayString="The type of waypoint is:"+(wp1.getWaypointTypeString());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyType(const Waypoint& wp, const string& type)
		
		@remarks 
		
		*/
		
		static void updateType(Waypoint& wp);

		//	//-------------------------------------------------------------------------

		/*!
		@description This function returns the combat mode of the waypoint created within the VBS environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp - The selected waypoint for which the combat mode is obtained.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString="The combat mode is: "+WaypointUtilities::getCombatMode(wp1);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyCombatMode(const Waypoint& wp);	
		
		@remarks Error checking utility validates the following:- Alias (outputs an error if the alias is invalid)	
		
		*/
		static std::string getCombatMode(Waypoint& wp);

		/*!
		@description  This function applies the combat mode obtained from wp.getWaypointCombatMode to the waypoint created within the VBS environment.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp - The selected waypoint for which the combat mode is applied.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		// sets the combat mode
		wp1.setWaypointCombatMode(BLUE);

		WaypointUtilities::applyCombatMode(wp1);
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyCombatMode(const Waypoint& wp, const string& combatMode);	
		
		@related
		
		WaypointUtilities::getCombatMode(Waypoint& wp)
		
		@remarks The type of the waypoint should be set using the function below before applying 
		WaypointUtilities::applyCombatMode(const Waypoint& wp).
		
		Set the waypoint combat mode using a string variable.  The values are:
		NO CHANGE" (No change)
		"BLUE" (Never fire)
		"GREEN" (Hold fire - defend only)
		"WHITE" (Hold fire, engage at will)
		"YELLOW" (Fire at will)
		"RED" (Fire at will, engage at will)
		setWaypointCombatMode(const string& _strType);	
		
		*/
		static void applyCombatMode(const Waypoint& wp);		

		/*!
		@description  This function applies the combat mode to the waypoint created within the VBS environment. The group combat mode is switched when the waypoint becomes active. 
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp - The selected waypoint for which the combat mode is applied.
		
		@param combatMode - The combat mode to be applied to the waypoint. Possible mode values are:
		"NO CHANGE" (No change)
		"BLUE" (Never fire)
		"GREEN" (Hold fire - defend only)
		"WHITE" (Hold fire, engage at will)
		"YELLOW" (Fire at will)
		"RED" (Fire at will, engage at will)

		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applyCombatMode(wp1,("YELLOW"));
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyCombatMode(const Waypoint& wp);	

		@remarks Error checking utility validates the following: Alias (outputs an error if the alias is invalid)	
		
		*/
		static void applyCombatMode(const Waypoint& wp, const std::string& combatMode);	

		/*!
		@description  This function updates the combat mode of the waypoint in fusion end.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp - The selected waypoint for which the combat mode is updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::updateCombatMode(wp1);

		displayString="The combat mode is: "+ wp1.getWaypointCombatModeString();

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyCombatMode(const Waypoint& wp, const string& combatMode);
		
		@remarks 
		
		*/
		static void updateCombatMode(Waypoint& wp);

		//	//-------------------------------------------------------------------------

		/*!

		@description  This function returns the behavior of the waypoint created within the VBS environment in string format. 

		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the behavior is obtained.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString="The behaviour of waypoint is: "+WaypointUtilities::getBehaviour(wp1);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyBehaviour(const Waypoint& wp);

		@remarks Error checking utility validates the following:- Alias (outputs an error if the alias is invalid)
		
		*/
		static std::string getBehaviour(Waypoint& wp);

		/*!
		@description  This function applies the behavior to the waypoint created within the VBS environment using wp.getWaypointBehaviour(). Switches the unit behavior when the waypoint becomes active.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the behavior is applied.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//sets the behaviour of the waypoint
		wp1.setWaypointBehaviour(string("COMBAT"));
		
		WaypointUtilities::applyBehaviour(wp1);
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyBehaviour(const Waypoint& wp, const string& behaviour)
		
		@related
		
		WaypointUtilities::getBehaviour(Waypoint& wp);
		
		@remarks The behavior of the waypoint should be set using the function below 
		setWaypointBehaviour(const string& _strType);

		The waypoint behavior mode can be set using  string variable. The possible values are:
		UNCHANGED
		CARELESS
		SAFE
		AWARE
		COMBAT 
		STEALTH

		*/
		static void applyBehaviour(const Waypoint& wp);

		/*!
		@description This function applies the behavior for the waypoint created within the VBS environment. Switches the unit behavior when the waypoint becomes active.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the behavior is applied.
		
		@param behavior  The behavior to be applied to the waypoint. The possible values are:
		UNCHANGED
		CARELESS
		SAFE
		AWARE
		COMBAT 
		STEALTH

		@return  Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applyBehaviour(wp1,string ("COMBAT"));

		@end code
		
		@overloaded
		
		WaypointUtilities::applyBehaviour(const Waypoint& wp);
		
		@related
		
		WaypointUtilities::updateBehaviour(Waypoint& wp);
		
		@remarks Error checking utility validates the following:- Alias (outputs an error if the alias is invalid)	
		
		*/
		static void applyBehaviour(const Waypoint& wp, const std::string& behaviour);

		/*!
		@description  This function updates the behavior of the waypoint created within the VBS environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the behavior is updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		
		Waypoint wp1;

		WaypointUtilities::updateBehaviour(wp1);

		displayString="The behaviour of waypoint is: "+ wp1.getWaypointBehaviourString();
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyBehaviour(const Waypoint& wp, const string& behaviour);
		
		@remarks 
		
		*/
		static void updateBehaviour(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		@description  This function returns the formation of the waypoint created within the VBS environment in string format.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the formation is obtained.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		
		Waypoint wp1;
		
		displayString="The formation is:"+WaypointUtilities::getFormation(wp1);

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyFormation(const Waypoint& wp);

		@remarks 	
		
		*/
		static std::string getFormation(Waypoint& wp);

		/*!
		@description  This function applies the formation to the waypoint using wp.getWaypointFormation().Switches the group formation when the waypoint becomes active.
		
		@locality 
		
		Locally Applied 
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the formation is applied.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		wp1.setWaypointFormation(DIAMOND);
		WaypointUtilities::applyFormation(wp1);
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyFormation(const Waypoint& wp, const string& formation);
		
		@related
		
		WaypointUtilities::getFormation(Waypoint& wp);
		
		@remarks The formation of the waypoint should be set using the function below 
		setWaypointFormation(const string& strFormation);
		
		The waypoint formation can be set using  the string variable. The possible values are:
		"COLUMN"
		"STAG_COLUMN"
		"WEDGE"
		"ECH_LEFT"
		"ECH_RIGHT"
		"VEE"
		"LINE" 
		"NONE"

		*/
		static void applyFormation(const Waypoint& wp);

		/*!
		@description - This function applies the formation to the waypoint created within the VBS environment. Switches the group formation when the waypoint becomes active. 
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the formation is applied.
		
		@param formation  the formation to be applied to the selected waypoint. 
		The possible values are:
		"COLUMN"
		"STAG_COLUMN"
		"WEDGE"
		"ECH_LEFT"
		"ECH_RIGHT"
		"VEE", "LINE" 
		"NONE"

		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applyFormation(wp1,string("LINE"))

		@end code
		
		@overloaded
		
		WaypointUtilities::applyFormation(const Waypoint& wp);
		
		@related
		
		WaypointUtilities::getFormation(Waypoint& wp)
		
		@remarks Error checking utility validates the following:- Alias (outputs an error if the alias is invalid)	
		
		*/
		static void applyFormation(const Waypoint& wp, const std::string& formation);

		/*!
		@description  This function updates the formation of the waypoint in the fusion end created within the VBS environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the formation is updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		
		Waypoint wp1;

		WaypointUtilities::updateFormation(wp1);
		
		displayString="The formation is:"+wp1.getWaypointFormationString();

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getFormation(Waypoint& wp);
		
		@remarks 
		
		*/
		static void updateFormation(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		@description This function returns the speed mode of the waypoint created within the VBS environment in string format.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param  wp  The selected waypoint for which the speed mode is obtained.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString= "The speedmode is:"+ WaypointUtilities::getSpeedMode(wp1);

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applySpeedMode(const Waypoint& wp);
		
		@remarks Error checking utility validates the following: - Alias (output an error if the alias is invalid)	
		
		*/
		static std::string getSpeedMode(Waypoint& wp);

		/*!
		@description  This function applies the speed mode to the waypoint created within VBS environment using wp.getWaypointSpeedMode().Switches the group speed mode when the waypoint becomes active. 
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the speed mode is applied.
		
		@return nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		
		//sets the speed mode
		wp1.setWaypointSpeedMode(string("NORMAL")); 
		WaypointUtilities::applySpeedMode(wp1);
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applySpeedMode(const Waypoint& wp, const string& speedMode);
		
		@related
		
		WaypointUtilities::getSpeedMode(Waypoint& wp);
		
		@remarks The speed mode of the waypoint should be set using the function below 
		setWaypointSpeedMode(const string& _strType);

		The possible values are:
		UNCHANGED
		LIMITED 
		NORMAL 
		FULL

		@remarks 
		*/
		static void applySpeedMode(const Waypoint& wp);

		/*!
		@description  This function applies the speed mode to the waypoint created within VBS environment using wp.getWaypointSpeedMode(). Switches the group speed mode when the waypoint becomes active. 
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the speed mode is applied.
		
		@param speedMode  The speed mode to be applied to the selected waypoint. 
		
		The possible values are:
		UNCHANGED
		LIMITED 
		NORMAL 
		FULL

		@return  nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applySpeedMode(wp1,string("FULL"));

		@end code
		
		@overloaded
		
		WaypointUtilities::applySpeedMode(const Waypoint& wp)
		
		@related
		
		WaypointUtilities::updateSpeedMode(Waypoint& wp);
		
		@remarks Error checking utility validates the following: - Alias (outputs an error if the alias is invalid)	
		
		*/
		static void applySpeedMode(const Waypoint& wp, const std::string& speedMode);

		/*!
		@description  This function updates the speed mode of the waypoint in the fusion end. 
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the speed mode is updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::updateSpeedMode(wp1); 

		displayString="The speedmode of wp is: "+ wp1.getWaypointSpeedModeString();

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getSpeedMode(Waypoint& wp);
		
		@remarks 
		
		*/
		static void updateSpeedMode(Waypoint& wp);


		//-------------------------------------------------------------------------

		/*!
		@description  This function returns the timeout values of the waypoint created within the VBS environment.
		return parameter 1 : Min
		return parameter 1 : Mid
		return parameter 1 : Max

		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the time out is obtained.
		
		@return vector<double>
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		
		// The timeout values are obtained through a vector
		vector<double> time = WaypointUtilities::getTimeout(wp1);
		for (vector<double>::iterator itr = time.begin(),itr_end =time.end();itr!=itr_end;++itr)
		{ 
		displayString+="The values are"+conversions::DoubleToString(*itr);
		}
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyTimeout(const Waypoint& wp);
		
		@remarks Error checking utility validates the following:- Alias (outputs an error if the alias is invalid)	
		
		*/
		static std::vector<double> getTimeout(Waypoint& wp);

		/*!
		@description  This function applies the timeout values of the waypoint created within the VBS environment. Defines the time between condition satisfaction and waypoint finish (randomly from min to max, with an average value mid).

		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the time out is applied.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//sets the values for timeout
		wp1.setTimeoutMax(4.5);
		wp1.setTimeoutMid(3);
		wp1.setTimeoutMin(1);
		WaypointUtilities::applyTimeout(wp1);

		@end code
		
		@overloaded
		
		WaypointUtilities::applyTimeout(const Waypoint& wp, const vector<double>& timeOut);
		
		@related
		
		WaypointUtilities::getTimeout(Waypoint& wp);
		
		@remarks The TimeOut values of the waypoint should be set using the functions
		setTimeoutMax(double timeout);
		setTimeoutMid(double timeout);
		setTimeoutMin(double timeout);

		@remarks 
		
		*/		
		static void applyTimeout(const Waypoint& wp);

		/*!
		@description - This function applies the timeout values of the waypoint created within the VBS environment. Defines the time between condition satisfaction and waypoint finish (randomly from min to max, with an average value mid).
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the time out is applied.
		
		@param timeout  The timeout values to be applied to the waypoint.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//The values are pushed into a vector
		vector<double> time
		time.push_back(2);
		time.push_back(3);
		time.push_back(5);

		WaypointUtilities::applyTimeout(wp1,time);

		@code
		
		@end code
		
		WaypointUtilities::applyTimeout(const Waypoint& wp)
		
		@related
		
		WaypointUtilities::getTimeout(Waypoint& wp);
		
		@remarks 
		
		*/
		static void applyTimeout(const Waypoint& wp, const std::vector<double>& timeOut);

		/*!
		@description  This function updates the timeout value of the waypoint in fusion end.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the time out is updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		WaypointUtilities::updateTimeout(wp1);

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getTimeout(Waypoint& wp);
		
		@remarks Error checking utility validates the following:- Alias (outputs an error if the alias is invalid).


		*/
		static void updateTimeout(Waypoint& wp);


		//-------------------------------------------------------------------------

		/*!
		@description  This function returns the statements of the waypoint created within the VBS environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the statement is obtained.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString="The statement is:" +WaypointUtilities::getWaypointStatements(wp1);

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyStatements(const Waypoint& wp);
		
		@remarks Error checking utility validates the following: - Alias (outputs an error if the alias is invalid)	

		*/
		static std::string getWaypointStatements(Waypoint& wp);

		/*!
		
		@description  This function applies the waypoint's completion condition, and the statement to be executed upon the completion of the waypoint created within the VBS environment.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the statement is applied.
		
		@return Nothing 
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		
		// sets the waypoint statement
		wp1.setStatements(string("hintHI")); 
		WaypointUtilities::applyStatements(wp1);
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyStatements(const Waypoint& wp, const string& statements);
		
		@related
		
		WaypointUtilities::getWaypointStatements(Waypoint& wp);
		
		@remarks The statement of the waypoint should be set using the functions
		setStatements(const string& statements);
		
		@remarks 
		
		*/
		static void applyStatements(const Waypoint& wp);

		/*!
		@description - This function applies the waypoint's completion condition, and the statement to be executed upon the completion of the waypoint created within the VBS environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the statement is applied.
		
		@param statement - The statement to be executed upon the completion of the waypoint.
		
		@return Nothing 
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//The script will be executed on the completion of the waypoint
		WaypointUtilities::applyStatements(wp1,string("hint 'Hi'"));
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyStatements(const Waypoint& wp);
		
		WaypointUtilities::applyStatements(const Waypoint& wp, const string& condition, const string& statements);

		@related
		
		WaypointUtilities::getWaypointStatements(Waypoint& wp);
		
		@remarks Error checking utility validates the following:- Alias (outputs an error if the alias is invalid).

		*/
		static void applyStatements(const Waypoint& wp, const std::string& statements);

		/*!
		@description - This function applies the waypoint's completion condition, and the statement to be executed upon the completion of the waypoint created within the VBS environment.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the statement is applied.
		
		@param condition - If true then group will execute the defined statement and continue to the next waypoint; if false then group will wait at this waypoint until its set to true.
		
		@param statement - The statement to be executed upon the completion of the waypoint ("this" is available, and contains the group leader). Set to an emtpty string ("") if no special action should be performed.
		
		@return Nothing 
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//The script will be executed on the completion of the waypoint
		WaypointUtilities::applyStatements(wp1,"true",string("hint 'Hi'"))
		
		@end code
		
		@overloaded
		
		WaypointUtilities::applyStatements(const Waypoint& wp);
		
		WaypointUtilities::applyStatements(const Waypoint& wp,const string& statements);

		@related
		
		WaypointUtilities::getWaypointStatements(Waypoint& wp);
		
		@remarks Error checking utility validates the following: - Alias (outputs an error if the alias is invalid)	
		
		*/
		static void applyStatements(const Waypoint& wp, const std::string& condition, const std::string& statements);

		/*!
		@description  This function updates the waypoint statements in the fusion end.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the statement to be updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::updateStatements(wp1);

		displayString="The statement is: "+wp1.getStatements();
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getWaypointStatements(Waypoint& wp);
		
		@remarks 

		*/
		static void updateStatements(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		@description  This function returns the script of the waypoint created within the VBS environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the script is obtained.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString="The script is:"+WaypointUtilities::getScript(wp1);

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyScript(const Waypoint& wp);
		
		@remarks Error checking utility validates the following: - Alias (outputs an error if the alias is invalid)	
		
		*/
		static std::string getScript(Waypoint& wp);

		/*!
		@description - This function attaches a script to a scripted waypoint created within the VBS environment. Command consists of a script name and additional script arguments. The Script has to use SQS-Syntax.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the script is applied.
		
		@return Nothing 
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		@end code
		
		@overloaded
		
		WaypointUtilities::applyScript(const Waypoint& wp, const string& script);
		
		@related
		
		WaypointUtilities::getScript(Waypoint& wp);
		
		@remarks The script of the waypoint should be set using the functions
		setScript(const string& script);
		script  The script to be executed.
		
		@remarks  
		*/
		static void applyScript(const Waypoint& wp);

		/*!
		Apply the script obtained from script.
		
		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyScript(const Waypoint& wp, const std::string& script);

		/*!
		@description  This function updates the Waypoint script in the fusion end.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the waypoint script is updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::updateScript(wp1);

		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks 
		
		*/
		static void updateScript(Waypoint& wp);

		/*!
		@description  This function updates the group Waypoint number for the waypoint in the fusion end.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the waypoint number is updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::updateNumber(wp1);

		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks 
		
		*/
		static void updateNumber(Waypoint& wp);

		/*!
		@description  This function compound updates the following characteristics in the fusion end:
		behavior
		combat mode
		description
		direction
		formation
		position
		script
		show mode
		speed mode
		statements
		timeout
		type

		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the above characteristics are updated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::updateWaypoint(wp1);

		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks 
		
		*/
		static void updateWaypoint(Waypoint& wp);

		/*!
		@description  This function apply changes to the waypoint for the following characteristics:		 
		behavior
		combat mode
		description
		direction
		formation
		position
		script
		show mode
		speed mode
		statements
		timeout
		type
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the above characteristic changes are applied.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		wp1.setWaypointBehaviour(string("COMBAT"));
		wp1.setTimeoutMax(25.2);

		//applies the changes for the above characteristics
		WaypointUtilities::applyChanges(wp1);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks 
		
		*/
		static void applyChanges(const Waypoint& wp);

		//-------------------------------------------------------
		// Network id Utility
		//-------------------------------------------------------

		/*!
		@description  This function updates the "IsLocal" property of the waypoint created within the VBS environment to know whether it has a valid network id in the Fusion end.

		@locality
		
		Locally Applied

		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint for which the above function is applied.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::updateIsLocal(wp1);
		displayString="is local"+conversions::BoolToString(wp1.isLocal());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks 	
		
		*/
		static void updateIsLocal(Waypoint& wp);

		/*!
		@description  This function creates a Waypoint within the VBS environment.	
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The waypoint to be created.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//sets the position of the waypoint
		wp1.setPosition(player.getPosition()+position3D(100,20,0));

		//sets the group name to the assigned waypoint
		wp1.setGroup(grp.getName());

		//creates the waypoint
		WaypointUtilities::createWaypoint(wp1);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::deleteWaypoint(Waypoint& wp); 
		
		@remarks It is mandatory to set the group name for which the waypoint is assigned to, using the function
		setGroup(const std::string& group); 	
		
		*/
		static void createWaypoint(Waypoint& wp);

		/*!
		@description  This function deletes the waypoints created within the VBS environment.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.20]
		
		@param wp  The selected waypoint to be deleted.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::deleteWaypoint(wp1);

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::createWaypoint(Waypoint &wp);
		
		@remarks	
		
		*/
		static void deleteWaypoint(Waypoint& wp);

		/*!
		Attaches a Object to the given Waypoint.
		Deprecated. Use void applyObjectAttach(Waypoint& wp, ControllableObject& co)
		*/
		VBSFUSION_DPR(UWPT001) static void WaypointAttachObject(Waypoint& wp, ControllableObject& co);

		/*!
		@description  This function synchronizes a waypoint with other waypoint.  Each waypoint is given as an array [group, index].	
		
		@locality
		
		Locally Applied 
		
		@version [VBS2Fusion v2.50]
		
		@param wp  The selected waypoint to be synchronized.
		
		@param sywp  The waypoint to synchronize with.
		
		@return Nothing
		
		@example 
		
		@code
		
		//Waypoints to be created
		Waypoint wp1,wp2;

		//Groups for which the waypoints are assigned to
		Group grp,group;

		//Waypoints created and assigned to groups
		wp1.setPosition(wpOffset + player.getPosition());
		WaypointUtilities::applyPosition(wp1);
		GroupUtilities::createAndAddWaypoint(grp,wp1);

		wp2.setPosition(wpOffset1 + player.getPosition());
		WaypointUtilities::applyPosition(wp2);
		GroupUtilities::createAndAddWaypoint(group,wp2);

		WaypointUtilities::synchronizeWaypoint(wp1,wp2);

		@end code
		
		@overloaded
		
		WaypointUtilities::synchronizeWaypoints(Waypoint& wp, list<Waypoint> wplist);

		@related
		
		WaypointUtilities::getSynchronizedWaypoints(Waypoint& wp);

		@remarks The waypoints of different groups are synchronized.	
		
		*/
		static void synchronizeWaypoint(Waypoint& wp, Waypoint& synwp);

		/*!
		@description  This function synchronizes a waypoint with other list of waypoint.  	

		@locality

		Locally Applied 

		@version [VBS2Fusion v2.50]

		@param wp  The selected waypoint to be synchronized.

		@param wplist  The waypoint list to be synchronize with.

		@return Nothing

		@example 

		@code

		//Waypoints to be created
		Waypoint wp1,wp2;

		//Groups for which the waypoints are assigned to
		Group grp,group;

		//Waypoints created and assigned to groups
		wp1.setPosition(wpOffset + player.getPosition());
		WaypointUtilities::applyPosition(wp1);
		GroupUtilities::createAndAddWaypoint(grp,wp1);

		wp2.setPosition(wpOffset1 + player.getPosition());
		WaypointUtilities::applyPosition(wp2);
		GroupUtilities::createAndAddWaypoint(group,wp2);

		list<Waypoint> inLst;
		inLst.push_back(wp1);
		inLst.push_back(wp2);
		WaypointUtilities::synchronizeWaypoints(wp1,inLst);

		@end code

		@overloaded

		WaypointUtilities::synchronizeWaypoint(Waypoint& wp, Waypoint& synwp);
		
		@related

		WaypointUtilities::getSynchronizedWaypoints(Waypoint& wp);

		@remarks The waypoints of different groups are synchronized.	
		
		*/
		static void synchronizeWaypoints(Waypoint& wp, std::list<Waypoint> wplist);

		/*!
		@description - This function returns a list of waypoints to which the specified waypoints are synchronized with.
		
		@locality 
		
		Locally Applied
		
		@version [VBS2Fusion v2.50]
		
		@param wp  The selected waypoint for which its synchronized waypoints are obtained.
		
		@return list<Waypoint>
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1,wp2,wp3;

		list<Waypoint> inLst = WaypointUtilities::getSynchronizedWaypoints(wp1);
		for (list<Waypoint>::iterator it = inLst.begin(); it != inLst.end(); it++)
		{
			displayString += "The waypoint names are:"+it->getName()+"\\n";
		}
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::synchronizeWaypoints(Waypoint& wp, list<Waypoint> wplist);

		@remarks
		
		*/
		static std::list<Waypoint> getSynchronizedWaypoints(Waypoint& wp);

		/*!
		Sets the radius for a loiter waypoint. Waypoint should be in Loiter type.	
		Deprecated. Use void applyLoiterRadius(Waypoint& wp, double radius)
		*/
		VBSFUSION_DPR(UWPT002) static void setLoiterRadius(Waypoint& wp, double radius);

		/*!
		@description - This function returns the radius of a loiter waypoint created within the VBS environment. Waypoints with other types will return -1.		
		
		@locality 
		
		Locally Applied
		
		@version [VBS2Fusion v2.50]
		
		@param wp  The selected waypoint for which the loiter radius is obtained.
		
		@return double
		
		@example 
		
		@code
		
		// The waypoint to be created
		
		Waypoint wp1;
		displayString="the loiter radius is: "+conversions::DoubleToString(WaypointUtilities::getLoiterRadius(wp1));

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyLoiterRadius(Waypoint& wp, double radius);
		
		@remarks
		
		None	
		
		*/
		static double getLoiterRadius(Waypoint& wp);

		/*!
		Sets the radius for a loiter waypoint. Waypoint should be in Loiter type.
		Deprecated. Use void applyLoiterType(Waypoint& wp, string type)
		*/
		VBSFUSION_DPR(UWPT003) static void setLoiterType(Waypoint& wp, std::string type);

		/*!
		@description - This function returns the type of a loiter waypoint created within the VBS environment. 
		
		@locality
		
		Locally Applied
		
		@version [VBS2Fusion v2.50]
		
		@param wp  The selected waypoint for which the loiter type is obtained.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		displayString="The loiter type is:" +WaypointUtilities::getLoiterType(wp1);	
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyLoiterType(Waypoint& wp, const string& type);

		@remarks
			
		*/
		static std::string getLoiterType(Waypoint& wp);

		/*!
		@description - 	This function returns the Controllable object attached to the waypoint created within the VBS environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBS2Fusion v2.54]
		
		@param wp  The selected waypoint for which the attached objects are obtained.
		
		@return ControllableObject
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		co = WaypointUtilities::getAttachedObject(wp1);
		displayString="The attached object is:"+ co.getName();
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyObjectAttach(Waypoint& wp, const ControllableObject& co)
		
		@remarks	
		
		*/
		static ControllableObject getAttachedObject(Waypoint& wp);

		/*!
		Sets the target house position for waypoints attached to a house,.	
		Deprecated. Use void applyHousePosition(Waypoint& wp, int pos)
		*/
		VBSFUSION_DPR(UWPT004) static void setHousePosition(Waypoint& wp, int pos);

		/*!
		@description  This function returns the house position assigned to the waypoint created within the VBS environment.
		
		@locality
		
		Locally Applied
		
		@version [VBS2Fusion v2.54]
		
		@param wp  The selected waypoint for which the assigned house position is obtained.
		
		@return int
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		displayString= "The House position is:"+WaypointUtilities::getHousePosition(wp1);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyHousePosition(Waypoint& wp, int pos)
		
		@remarks
		
		*/
		static int getHousePosition(Waypoint& wp);

		/*!
		Sets variable to given value in the variable space of given Waypoint.
		Deprecated. Use void applyVariable(Waypoint& wp, string& name, VBS2Variable& val, bool isPublic)
		*/
		VBSFUSION_DPR(UWPT005) static void setWaypointVariable(Waypoint& wp, std::string name, VBS2Variable& val, bool pub);

		/*!
		@description - 	This function returns the value of the variable in variable space of the Waypoint created within the VBS environment. The command is case sensitive (i.e. getVariable "Status" is not the same as getVariable "status").
		
		@locality
		
		Locally Applied
		
		@version [VBS2Fusion v2.54]
		
		@param wp  The selected waypoint for which the waypoint variable is obtained.
		
		@return VBSVariable
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		string s;

		VBSVariable var2 =WaypointUtilities::getWaypointVariable(wp1,string("A"));
		string svar=var2.getData().s;
		s=svar+"\n	";
		displayString="The variable is: "+s;
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyVariable(Waypoint& wp, const string& name, const VBSVariable& val, bool isPublic)
		
		@remarks
		
		*/
		static VBSVariable getWaypointVariable(Waypoint& wp, const std::string& name);

		/*!
		@description - This function applies the waypoint to a random position in a circle with the given center and radius. The position is not updated on the wp object. Use update to check if changes have been applied.  	
		
		@locality 
		
		Locally Applied
		
		@version [VBS2Fusion v2.60]
		
		@param wp  The selected waypoint for which a random position is applied.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		WaypointUtilities::applyRandomPosition(wp1,position3D(10,5,0),9.5);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks

		*/
		static void applyRandomPosition(const Waypoint& wp, const position3D& center, double radius);

		/*!
		Set the radius around the waypoint where is the waypoint completed.
		Deprecated. Use void applyCompletionRadius(Waypoint& wp, double radius)
		*/
		VBSFUSION_DPR(UWPT010) static void setWaypointCompletionRadius(Waypoint& wp, double radius);

		/*!
		@description  This function returns the radius around the waypoint which is considered completed.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.02]
		
		@param wp  The selected waypoint for which the completion radius is obtained.
		
		@return double
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		displayString="The waypoint Completion radius is:"+conversions::DoubleToString(WaypointUtilities::getWaypointCompletionRadius(wp1));

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyCompletionRadius(Waypoint& wp, double radius)
		
		@remarks
		
		*/
		static double getWaypointCompletionRadius(Waypoint& wp);

		/*!
		@description - This function returns the visibility of the waypoint created within the VBS environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.02]
		
		@param wp  The selected waypoint for which the completion radius is obtained.
		
		@return bool 
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString+="Is waypoint visible" +conversions::BoolToString(WaypointUtilities::isWaypointVisible(wp1));

		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks
		
		*/
		static bool isWaypointVisible(Waypoint& wp);

		/*!
		Set waypoint's visibility.
		Deprecated. Use void applyVisibility(Waypoint& wp, bool visible)
		*/
		VBSFUSION_DPR(UWPT011) static void setWaypointVisible(Waypoint& wp, bool visible);

		/*!
		@description  This function returns the statement that is executed when a waypoint is reached.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.02]
		
		@param wp  The selected waypoint for which effect condition is obtained.
		
		@return string 
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		displayString="The effect condition is:"+ WaypointUtilities::getEffectCondition(wp1);

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyEffectCondition(Waypoint& wp, string& statement)
		
		@remarks
		
		*/
		static std::string getEffectCondition(Waypoint& wp);
	
		/*!
		The statement is executed when the waypoint is activated and the effects are launched
		depending on the result.
			- If the result is a boolean and true, the effect is launched.
			- If the result is an object, the effect is launched if the result is the player or
			  the player vehicle.
			- If the result is an array, the effect is launched if the result contains the player
			  or the player vehicle. 
		
		Deprecated. Use void applyEffectCondition(Waypoint& wp, string& statement)
		*/
		VBSFUSION_DPR(UWPT009) static void setEffectCondition(Waypoint& wp, std::string& statement);

		/*!
		Defines the music track played on activation. Track is a subclass name of CfgMusic. 
		In addition, "$NONE$" (no change) or "$STOP$" (stops the current music track). 		
		Deprecated. Use void applyMusicEffect(Waypoint& wp, string& trackName)
		*/
		VBSFUSION_DPR(UWPT006) static void setMusicEffect(Waypoint& wp, std::string& trackName);

		/*!
		@description -   This function returns the defined music track played on activation on waypoint created within the VBS environment. Track is a subclass name of CfgMusic or "$NONE$" or "$STOP$".  		

		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.03]
		
		@param wp  The selected waypoint for which the music effect  is obtained.
		
		@return string
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		displayString="The musical effect: "+WaypointUtilities::getMusicEffect(wp1);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyMusicEffect(Waypoint& wp, string& trackName)
		
		@remarks
		
		*/
		static std::string getMusicEffect(Waypoint& wp);

		/*!
		Defines the different sound effects.
		Sound / voice plays a 2D / 3D sound from CfgSounds.
		SoundEnv plays an enviromental sound from CfgEnvSounds.
		SoundDet (only for triggers) creates a dynamic sound object attached to a trigger defined in CfgSFX. 

		Deprecated. Use void applySoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet)
		*/
		VBSFUSION_DPR(UWPT007) static void setSoundEffect(Waypoint& wp, std::string& sound, std::string& voice, std::string& soundEnv, std::string& soundDet);
		
		/*!
		@description  This function returns a vector of string which contains the sound, voice, soundEnv, and soundDet of the waypoin created within the VBS environmnet.

		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.03]
		
		@param wp  The selected waypoint for which sound effect is obtained.
		
		@return vector<string>
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//converts a vector to string
		string st;
		vector<string> vec;
		vec = WaypointUtilities::getSoundEffect(wp1);
		for (vector<string>::iterator itr=vec.begin();itr!=vec.end();itr++)
		{
		st=*itr;
		displayString+=st;
		}
		string s = "";
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applySoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet)

		@remarks
		
		*/
		static std::vector<std::string> getSoundEffect(Waypoint& wp);

		/*!
		Defines the title effect via [Type, Effect, Text] where

		'Type' can be,
			- "NONE",
			- "OBJECT",
				'Text' defines the shown object , a subclass of CfgTitles. 
			- "RES"
				'Text' defines a resource class, a subclass of RscTitles. 
			- "TEXT"
				The 'Text' is shown as text itself.
				
		'Effect' defines a subtype: 
			"PLAIN", "PLAIN DOWN", "BLACK", "BLACK FADED", "BLACK OUT", 
			"BLACK IN", "WHITE OUT" or "WHITE IN". 
		
		Deprecated. Use void applyTitleEffect(Waypoint& wp, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text)
		*/
		VBSFUSION_DPR(UWPT008) static void setTitleEffect(Waypoint& wp, std::string& type, std::string& effect, std::string& text);

		/*!
		@description - This function returns the defined title effect for a waypoint. Return vector of string contains type, effect and text
		'type' can be "NONE", "OBJECT", "RES" or "TEXT".
		'effect' returns the selected Title Effect Type for type "TEXT".
		'text' returns the shown object (a subclass of CfgTitles) for type "OBJECT", the resource class (a subclass of RscTitles) for type "RES", and the plain text string for type "TEXT"

		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.03]
		
		@param wp  The selected waypoint for which title effect is obtained.
		
		@return vector<string>
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//Coverts a vector to string
		string stt;
		vector<string> vect;
		vect = WaypointUtilities::getTitleEffect(wp1);
		for (vector<string>::iterator itr=vect.begin();itr!=vect.end();itr++)
		{
		stt=*itr;
		displayString+=stt;
		}
		string s = "";
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::applyTitleEffect(Waypoint& wp, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text)

		@remarks
		
		*/
		static std::vector<std::string> getTitleEffect(Waypoint& wp);

		/*!
		Attaches a Object to the given Waypoint.

		@param wp - Waypoint that object is attached.
		@param co - ControllableObject to be attached.

		@return Nothing.

		@remarks This is a replication of void WaypointAttachObject(Waypoint& wp, ControllableObject& co) function. 
		*/
		static void applyObjectAttach(const Waypoint& wp, const ControllableObject& co);


		/*!
		@description  This function is executed when the waypoint is activated and the effects are launched depending on the result.
		If the result is a boolean and true, the effect is launched.
		If the result is an object, the effect is launched if the result is the player or the player vehicle.
		If the result is an array, the effect is launched if the result contains the player or the player vehicle.

		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.09]
		
		@param wp  The selected Waypoint to which the music track is defined to.
		
		@param trackName - Name of the music track that will be played on waypoint activation. Track is a subclass name of CfgMusic (http://resources.bisimulations.com/content/classes/CfgMusic.html) defined by VBS. In addition, "$NONE$" (no change) or "$STOP$" (stops the current music track).   
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applyMusicEffect(wp1,string("track2"));
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getMusicEffect(Waypoint& wp);

		@remarks
		
		*/
		static void applyMusicEffect(Waypoint& wp, std::string& trackName);
		/*!
		@description  This function defines the radius for a loiter waypoint.  If no loiter radius is defined, a default radius is used and the size depending on the aircraft's maneuverability.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.09]
		
		@param wp  The selected Waypoint to which the loiter radius is applied.
		
		@param radius  The loiter radius to be applied on the waypoint (minimal executable radius depends on the speed and maneuverability of the aircraft) created within the VBS environment.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;
		WaypointUtilities::applyLoiterRadius(wp1,500.0);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getLoiterRadius(Waypoint& wp);

		@remarks This function have an effect on the waypoints of type Loiter which is set using WaypointUtilities::applyType(const Waypoint& wp, const string& type);		

		*/
		static void applyLoiterRadius(Waypoint& wp, double radius);

		/*!
		@description  This function defines the type of loitering mode for a loiter waypoint. To create a loitering waypoint, its type must first be set to "LOITER" (via setWaypointType), and then have its loiter type defined by WaypointLoiterType. Setting the setWaypointLoiterRadius is optional. The default loiter radius depends on the aircraft's maneuverability.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.09]
		
		@param wp  The selected Waypoint to which the loiter type is applied.
		
		@param type  - Loitering type to be applied to the waypoint.  It can be 
		"CIRCLE" (move in a circle around loiter waypoint, clockwise)
		"CIRCLE_L" (move in a circle around loiter waypoint, anticlockwise) 

		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		//waypoint type set to LOITER
		wp1.setWaypointType(LOITER);

		//applies the loiter type to the waypoint
		WaypointUtilities::applyLoiterType(wp1,string("L_CIRCLE"))

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getLoiterType(Waypoint& wp);

		@remarks Waypoint can be set to "LOITER" mode by using void applyType(Waypoint& wp, string type) function.

		@remarks This is a replication of void setLoiterType(Waypoint& wp, string type) function. 
		
		*/
		static void applyLoiterType(Waypoint& wp, const std::string& type);

		/*!
		@description  This function defines different sound effects to the waypoint created within the VBS environment.

		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.09]
		
		@param wp  The waypoint to which sound effects are defined to.
		
		@param sound - Name of the 2D / 3D sound from CfgSounds.
		
		@param voice - Name of the 2D / 3D sound from CfgSounds.
		
		@param soundEnv-  Name of the environmental sound from CfgEnvSounds.
		
		@param soundDet - Creates a dynamic sound object attached to a waypoint defined in CfgSFX.
		
		@return Nothing
		
		@example
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applySoundEffect(wp1,string("sound"),string("voice"),string("env"),string("det"));

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getSoundEffect(Waypoint& wp);

		@remarks This is a replication of void setSoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet function.
		
		*/
		static void applySoundEffect(Waypoint& wp, const std::string& sound, const std::string& voice, const std::string& soundEnv, const std::string& soundDet);

		/*!
		@description  This function defines the target house position for waypoints attached to a house.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.09]
		
		@param wp  The waypoint that is attached to a house.
		
		@param pos -  The target house position.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applyHousePosition(wp1,10);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getHousePosition(Waypoint& wp);

		@remarks This is a replication of void setHousePosition(Waypoint& wp, int pos) function. 
		
		*/
		static void applyHousePosition(Waypoint& wp, int pos);

		/*!
		@description -  This function applies variable to given value in the variable space of given Waypoint. To remove a variable, set it to nil (e.g. player setVariable ["varname", nil]).  Once a waypoint is removed, all associated variables will disappear.
		
		@locality
		
		Locally Applied
		
		@version [VBSFusion v3.09]
		
		@param wp  The waypoint to which the variable is assigned to.
		
		@param name -  Name of variable. Variable name is case-sensitive.
		
		@param val - Value to assign to the variable.
		
		@param isPublic -  If true, then the value is broadcast to all computers.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		VBS2Variable var = VBS2Variable(string("u1"));
		WaypointUtilities::applyVariable(wp1, string("A"), var, true);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getWaypointVariable(Waypoint& wp, const string& name);
		
		@remarks This is a replication of void setWaypointVariable(Waypoint& wp, string name, VBSVariable& val, bool pub) function.  
		
		*/
		static void applyVariable(Waypoint& wp, const std::string& name, const VBSVariable& val, bool isPublic);

		/*!
		@description - This function defines the title effect for the waypoint created within the VBS environmnet.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.09]
		
		@param wp  The waypoint To which the  title effect is defined to.
		
		@param classType - Type defines in TITLE_EFFECT_CLASS enum and it can be,
		
		TEC_NONE - "NONE",
		TEC_OBJECT - "OBJECT" : 'Text' defines the shown object , a subclass of CfgTitles. 
		TEC_RES - "RES" : 'Text' defines a resource class, a subclass of RscTitles. TEC_TEXT  
		"TEXT" : -  The 'Text' is shown as text itself. 'Effect' defines a subtype: "PLAIN", "PLAIN DOWN", "BLACK", "BLACK FADED", "BLACK OUT", "BLACK IN", "WHITE OUT" or "WHITE IN".

		@param effect - Effect defines in TITLE_EFFECT_TYPE enum.

		@param text - Text to be shown.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applyTitleEffect(wp1, TITLE_EFFECT_CLASS::TEC_TEXT, TITLE_EFFECT_TYPE::BLACK, string("Hello"));

		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getTitleEffect(Waypoint& wp);
		
		@remarks Waypoint can be set to "LOITER" mode by using void applyType(Waypoint& wp, string type) function.

		@remarks This is a replication of void setTitleEffect(Waypoint& wp, string& type, string& effect, string& text) function.
		
		*/
		static void applyTitleEffect(Waypoint& wp, const TITLE_EFFECT_CLASS& classType, const TITLE_EFFECT_TYPE& effect, std::string& text);

		/*!
		@description  This function is executed when the waypoint is activated and the effects are launched depending on the result of the statement.
		If the result is a boolean and true, the effect is launched.
		If the result is an object, the effect is launched if the result is the player or the player vehicle.
		If the result is an array, the effect is launched if the result contains the playeror the player vehicle.

		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.09]
		
		@param wp  The selected waypoint to which the effect condition is applied. 
		
		@param statement - The statement which is executed when the waypoint is activated.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applyEffectCondition(wp1,string("Waypoint completed"));
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getEffectCondition(Waypoint& wp);
		
		@remarks  Waypoint can be set to "LOITER" mode by using void applyType(Waypoint& wp, string type) function.
		
		*/
		static void applyEffectCondition(Waypoint& wp, const std::string& statement);

		/*!
		@description - This function applies the radius around the waypoint. The completion radius allows units to call the waypoint completed once they are inside of the given circle. IT has an effect only on units in behavior mode "COMBAT", and if the completion radius is larger than 25m. The distance at which a waypoint is considered completed may vary by about 10m from the specified value. 
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.09]
		
		@param wp  The selected waypoint to which the completion radius is applied.
		
		@param radius - Radius value in meters to be applied to the waypoint.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applyCompletionRadius(wp1,30.5);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		WaypointUtilities::getWaypointCompletionRadius(Waypoint& wp);
		
		@remarks This is a replication of void setWaypointCompletionRadius(Waypoint& wp, double radius) function.
		
		*/
		static void applyCompletionRadius(Waypoint& wp, double radius);

		/*!
		@description  This function applies the visibility of the waypoint created within the VBS environment.
		
		@locality 
		
		Locally Applied
		
		@version [VBSFusion v3.09]
		
		@param wp  The selected waypoint for which the visibility is applied.
		
		@param visible - If 'true', waypoint will be visible.
		
		@return Nothing
		
		@example 
		
		@code
		
		// The waypoint to be created
		Waypoint wp1;

		WaypointUtilities::applyVisibility(wp1,true);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks This is a replication of void setWaypointVisible(Waypoint& wp, bool visible) function.
		
		*/
		static void applyVisibility(Waypoint& wp, bool visible);
	};	
};

#endif //WAYPOINT_UTILITIES_H