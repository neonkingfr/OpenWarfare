
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:
	ExplosiveUtilities.h

Purpose:
	This file contains the declaration of the ExplosiveUtilities class.
	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			05-04/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_EXPLOSIVEUTILITIES_H
#define VBS2FUSION_EXPLOSIVEUTILITIES_H




/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "ControllableObjectUtilities.h"
#include "WorldUtilities.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{
	class VBSFUSION_API ExplosiveUtilities : public ControllableObjectUtilities
	{
		public:

		/*!
		Makes an explosion at specified controllable object.
		Deprecated. Use void applyDetonation(ControllableObject& co)
		*/
			VBSFUSION_DPR(UEXP001)static void detonate(ControllableObject& co);

		/*!
		@description 
		Makes an explosion at specified controllable object.

		@locality

		Globally applied, globally effected.

		@version [VBS2Fusion v3.15] 
		
		@param co - The object for which the position is obtained. The object should be created before passing it as a parameter.

		@return Nothing.

		@example 

		@code

		//Apply Detonation to controllable object co.
		ExplosiveUtilities::applyDetonation(co);

		@endcode

		@overloaded Nothing.

		@related  

		@remarks This is a replication of  void applyDetonation(ControllableObject& co).
		*/
			static void applyDetonation(const ControllableObject& co);
	};
}

#endif //VBS2FUSION_EXPLOSIVEUTILITIES_H