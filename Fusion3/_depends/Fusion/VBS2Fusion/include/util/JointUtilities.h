 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.;
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	JointUtilities.h

Purpose: utility class for joint.

/*****************************************************************************/

#ifndef VBS2FUSION_JOINTUTILITES_H
#define VBS2FUSION_JOINTUTILITES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "VBSFusionDefinitions.h"
#include "data/ControllableObject.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{
	
	class VBSFUSION_API JointUtilities
	{
	public:
		/*!
		@description 
		Creates a spherical joint between two objects with specified parameters. Also known as a Ball and Socket joint. 
		(Forward/Back,Up/Down, and Left/Right movement locked, Yaw, Pitch, and Roll movement unlocked.)Any truck and 
		trailer can be used for a quick test.

		@locality

		Locally Applied Globally Effected

		@version  [VBS2Fusion v3.12]

		@param fromObj - Joint created from this object(trailer).
		@param toObj - Joint created to this object(truck).
		@param SPHERE_JOINT - This is the struct that holds sphere joint description. If instance is SphereDesc

			SphereDesc.fromPose.paramArray[0] - This is vector3D holds joint connection point in model coordinates of fromObj ([0,0,0] if not defined).
			SphereDesc.fromPose.paramArray[1] - This is vector3D holds joint connection axis in model coordinates of fromObj (up vector if not defined).

			SphereDesc.toPose.paramArray[0]  - This is vector3D holds joint connection point in model coordinates of toObj ([0,0,0] if not defined). 			
			
			Miscelaneous parameters
			SphereDesc.jointMiscelaneous.paramFlag - Flag to determine if we want to have enabled collisions between connected objects. (default = false, range = [true,false]).
			SphereDesc.jointMiscelaneous.paramArray[0] - Force needed to break a joint connectio n in Newtons (default = -1 ... disabled, range = <0,+DBL_MAX)).
			SphereDesc.jointMiscelaneous.paramArray[1] - Max projection distance. (default = -1 ... disabled, range = <0,+DBL_MAX)).
			
			Joint limits
			SphereDesc.jointLimitsArray[0] - Twist low limit
			SphereDesc.jointLimitsArray[1] - Twist high limit
			SphereDesc.jointLimitsArray[2] - Swing limit.
				Above each and every joint limits parameters have following joint limit parameters.
				paramArray[0] - limit bounce (default = -1 ... joint limit disabled, range = <0,1>)).
				paramArray[1] - The angle beyond which the limit is active (default = 0, range = angular (-pi,+pi>).
			
			Joint springs
			SphereDesc.jointSpringsArray[0] - Twist spring.
			SphereDesc.jointSpringsArray[1] - Swing spring.
			SphereDesc.jointSpringsArray[2] - Joint spring.
				Above each and every joint spring parameters have following joint spring parameters.
				paramArray[0] - Flag to determine if we want to enable that spring (default = 0, range = [0,1]).
				paramArray[1] - spring coefficient (default = 0, range = <0,+DBL_MAX)).
				paramArray[2] - damper coefficient (default = 0, range = <0,+DBL_MAX)).
				paramArray[3] - target angle of spring where the spring force is zero. (default = 0, range = angular (-pi,+pi>).

		@return int - joint id (range >= 0 , returns 0 on unsucces joint creation).

		@example

		@code

		VBSFusion::SPHERE_JOINT s;
		s.fromPose.paramArray[0] = vector3D(0,0, 2);
		s.fromPose.paramArray[1] = vector3D(0,-2,0);

		s.toPose.paramArray[0] = vector3D(0,4,2);

		s.jointMiscelaneous.paramFlag=true;
		s.jointMiscelaneous.paramArray[0]=-1;
		s.jointMiscelaneous.paramArray[1]=-1;

		s.jointLimitsArray[0].paramArray[0]=0;
		s.jointLimitsArray[0].paramArray[1]=-0.5;

		s.jointLimitsArray[1].paramArray[0]=0;
		s.jointLimitsArray[1].paramArray[1]=0.5;

		s.jointLimitsArray[2].paramArray[0]=0.5;
		s.jointLimitsArray[2].paramArray[1]=1;

		s.jointSpringsArray[0].paramArray[0]=1;
		s.jointSpringsArray[0].paramArray[1]=200;
		s.jointSpringsArray[0].paramArray[2]=0;
		s.jointSpringsArray[0].paramArray[3]=0.5;

		s.jointSpringsArray[1].paramArray[0]=1;
		s.jointSpringsArray[1].paramArray[1]=200;
		s.jointSpringsArray[1].paramArray[2]=0;
		s.jointSpringsArray[1].paramArray[3]=0.5;

		s.jointSpringsArray[2].paramArray[0]=1;
		s.jointSpringsArray[2].paramArray[1]=200;
		s.jointSpringsArray[2].paramArray[2]=0;
		s.jointSpringsArray[2].paramArray[3]=0.5;

		int ret=JointUtilities::createSphereJoint(vehicle1, vehicle2, s);

		@overloaded  
		int createSphereJoint(ControllableObject& fromObj, ControllableObject& toObj)
		
		@related

		@overloaded  
		int createSphereJoint(ControllableObject& fromObj, ControllableObject& toObj)
		
		@related

		@remarks 
		*/
		static int createSphereJoint(const ControllableObject& fromObj, const ControllableObject& toObj, const SPHERE_JOINT& desc);

		/*!
		@description 
		Creates default spherical joint between two objects. Also known as a Ball and Socket joint. 
		(Forward/Back,Up/Down, and Left/Right movement locked, Yaw, Pitch, and Roll movement unlocked.)Any truck and 
		trailer can be used for a quick test.

		@locality

		Locally applied, Globally effected.

		@version  [VBS2Fusion v3.12]

		@param fromObj - Joint created from this object(trailer).
		@param toObj - Joint created to this object(truck).

		@return int - joint id (range >= 0 , returns 0 on unsucces joint creation).

		@example

		@code

		int ret=JointUtilities::createSphereJoint(vehicle1, vehicle2);

		@overloaded  
		int createSphereJoint(ControllableObject& fromObj, ControllableObject& toObj, SPHERE_JOINT& desc)

		@related

		@remarks 
		*/
		static int createSphereJoint(const ControllableObject& fromObj, const ControllableObject& toObj);

		/*!
		@description 
		Creates a revolute joint between two objects with specified parameters.Two hummers can be used for a quick test.
		(Forward/Back,Up/Down, and Left/Right movement locked, and Yaw, Pitch, OR Roll unlocked.)

		@locality

		Locally applied, Globally effected.

		@version  [VBS2Fusion v3.12]

		@param fromObj - Joint created from this object.
		@param toObj - Joint created to this object.
		@param REVOLUTE_JOINT - This is the struct that holds revolute joint description. If instance is RevoluteDesc

			RevoluteDesc.fromPose.paramArray[0] - This is vector3D holds joint connection point in model coordinates of fromObj ([0,0,0] if not defined).
			RevoluteDesc.fromPose.paramArray[1] - This is vector3D holds joint connection axis in model coordinates of fromObj (up vector if not defined).
			RevoluteDesc.fromPose.paramArray[2] - This is vector3D holds joint connection direction in model coordinates of fromObj (forward vector if not defined).

			RevoluteDesc.toPose.paramArray[0] - This is vector3D holds joint connection point in model coordinates of toObj ([0,0,0] if not defined).
			RevoluteDesc.toPose.paramArray[1] - This is vector3D holds joint connection axis in model coordinates of toObj (up vector if not defined).
			RevoluteDesc.toPose.paramArray[2] - This is vector3D holds joint connection direction in model coordinates of toObj (forward vector if not defined).
			
			Miscelaneous parameters
			RevoluteDesc.jointMiscelaneous.paramFlag - Flag to determine if we want to have enabled collisions between connected objects. (default = false, range = [true,false]).
			RevoluteDesc.jointMiscelaneous.paramArray[0] - Force needed to break a joint connectio n in Newtons (default = -1 ... disabled, range = <0,+DBL_MAX)).
			RevoluteDesc.jointMiscelaneous.paramArray[1] - Max projection distance. If one side of the joint is outside this range then the other side will be setpos'd to the other without excessive velocity(default = -1 ... disabled, range = <0,+DBL_MAX)).
			
			Joint limits
			RevoluteDesc.jointLimitsArray[0] - Twist left limit
			RevoluteDesc.jointLimitsArray[1] - Twist right limit
				Above each and every joint limits parameters has following joint limit parameters.
				paramArray[0] - limit bounce (default = -1 ... joint limit disabled, range = <0,1>)).
				paramArray[1] - The angle beyond which the limit is active (default = 0, range = angular (-pi,+pi>)
			
			Joint drive and springs
			RevoluteDesc.jointDrives.paramArray[0] - Flag to determine if we want to enable that drive (default = 0, range = [0,1]).
			RevoluteDesc.jointDrives.paramArray[1] - The relative velocity the motor is trying to achieve. In radians per second. (default = 3e+020, range = <0,3e+020)).	
			RevoluteDesc.jointDrives.paramArray[2] - If 1, motor will not brake when it spins faster than value. (default = 0, range = [0,1]).
			RevoluteDesc.jointDrives.paramArray[3] - The maximum force (or torque) the motor can exert. In Newtons. (default = 3e+020, range = <0,3e+020)).
			
			RevoluteDesc.jointSprings.paramArray[0] - Flag to determine if we want to enable that spring (default = 0, range = [0,1])
			RevoluteDesc.jointSprings.paramArray[1] - Spring coefficient. How much to force to push back to value (target angle) position. (default = 0, range = <0,3e+020))
			RevoluteDesc.jointSprings.paramArray[2] - Damper coefficient. How hard it is to move, stiffness of the joint. (default = 0, range = <0,3e+020))
			RevoluteDesc.jointSprings.paramArray[3] - Target angle of spring where the spring force is zero. Angle where the spring force pushes the joint to. In radians. (default = 0, range = angular (-pi,+pi>)

		@return int - joint id (range >= 0 , returns 0 on unsucces joint creation).

		@example

		@code

		VBSFusion::REVOLUTE_JOINT des;

		des.fromPose.paramArray[0] = vector3D(0,-3,0);
		des.fromPose.paramArray[1] = vector3D(0,-2,0);
		des.fromPose.paramArray[2] = vector3D(0,-3,1);

		des.toPose.paramArray[0] = vector3D(0,3,0);
		des.toPose.paramArray[1] = vector3D(0,4,0);
		des.toPose.paramArray[2] = vector3D(0,3,1);

		des.jointMiscelaneous.paramFlag=false;
		des.jointMiscelaneous.paramArray[0]=1;
		des.jointMiscelaneous.paramArray[1]=1;

		des.jointLimitsArray[0].paramArray[0] = 0;
		des.jointLimitsArray[0].paramArray[1] = -1;

		des.jointLimitsArray[1].paramArray[0] = 0;
		des.jointLimitsArray[1].paramArray[1] = 1;

		des.jointDrives.paramArray[0] = 1;
		des.jointDrives.paramArray[1] = 3;
		des.jointDrives.paramArray[2] = 1;
		des.jointDrives.paramArray[3]=0;

		des.jointSprings.paramArray[0]=1;
		des.jointSprings.paramArray[1]=200;
		des.jointSprings.paramArray[2]=0;
		des.jointSprings.paramArray[3]=0.5;

		int ret=JointUtilities::createRevoluteJoint(vehicle1, vehicle2, des);

		@overloaded  
		
		@related

		@remarks 
		*/
		static int createRevoluteJoint(const ControllableObject& fromObj, const ControllableObject& toObj, const REVOLUTE_JOINT& desc);

		/*!
		@description 

		Default non-breakable fixed joint(all 6 DOFs locked).Any truck and trailer can be used for a quick test.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.12]

		@param fromObj - The object to which the 'toObj' is joined to(i.e truck).
		@param toObj - The object joined to the 'fromObj' (i.e trailer).
			
		@return int - joint id (range >= 0 , returns 0 on unsucces joint creation).

		@example

		@code

		//The vehicle to be created and joined
		veh1 ,veh2;
		int joint;
		joint = JointUtilities::createFixedJoint(veh1,veh2);
		displayString += conversions::IntToString(joint)+"\\n";

		@endcode

		@overloaded  
		
		JointUtilities::createFixedJoint(ControllableObject& fromObj, ControllableObject& toObj, FIXED_JOINT& desc)
		
		@related

		To create a fixed joint for objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::createFixedJoint(ControllableObject& fromObj, ControllableObject& toObj).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static int createFixedJoint(const ControllableObject& fromObj, const ControllableObject& toObj);

		/*!
		@description 

		Creates a fixed joint between two objects with specified parameters (all 6 DOFs locked).

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.12]

		@param fromObj - Joint created from this object(trailer).
		@param toObj - Joint created to this object(truck).
		@param desc - This is the struct that holds sphere joint description. If instance is FixedDesc

			FixedDesc.fromPose.paramArray[0] - This is vector3D holds joint connection point in model coordinates of fromObj ([0,0,0] if not defined).

			FixedDesc.toPose.paramArray[0]  - This is vector3D holds joint connection point in model coordinates of toObj ([0,0,0] if not defined). 			
			
			Miscellaneous parameters
			FixedDesc.jointMiscelaneous.paramFlag - Flag to determine if we want to have enabled collisions between connected objects. (default = false, range = [true,false]).
			FixedDesc.jointMiscelaneous.paramArray[0] - Force needed to break a joint connectio n in Newtons (default = -1 ... disabled, range = <0,+DBL_MAX)).
			FixedDesc.jointMiscelaneous.paramArray[1] - Max projection distance. (default = -1 ... disabled, range = <0,+DBL_MAX)).
			
		@return int - joint id (range >= 0 , returns 0 on unsuccessful joint creation).

		@example

		@code

		FIXED_JOINT desc2;

		//The vehicle to be created and joined
		veh1 ,veh2;

		desc2.fromPose.paramArray[0] = vector3D(0,-3, 0);
		desc2.toPose.paramArray[0] = vector3D(0,3,0);
		desc2.jointMiscelaneous.paramFlag = false;
		desc2.jointMiscelaneous.paramArray[0] = 10;
		desc2.jointMiscelaneous.paramArray[1] = 0;

		joint = JointUtilities::createFixedJoint(veh1,veh2,desc2);
		displayString += conversions::IntToString(joint)+"\\n";

		@endcode

		@overloaded  

		JointUtilities::createFixedJoint(ControllableObject& fromObj, ControllableObject& toObj)
		
		@related

		@remarks To create a fixed joint for objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::createFixedJoint(ControllableObject& fromObj, ControllableObject& toObj, FIXED_JOINT& desc).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		*/
		static int createFixedJoint(const ControllableObject& fromObj, const ControllableObject& toObj, const FIXED_JOINT& desc);

		/*!
		@description 
		Creates a distance joint between two objects with specified parameters.Two hummers can be used for a quick test.
		(Forward/Back,Up/Down, and Left/Right movement unlocked, Yaw, Pitch, and Roll movement locked.)

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.12]

		@param fromObj - Joint created from this object.
		@param toObj - Joint created to this object.
		@param desc - This is the struct that holds sphere joint description. If instance is DistanceDesc

			DistanceDesc.fromPose.paramArray[0] - This is vector3D holds joint connection point in model coordinates of fromObj ([0,0,0] if not defined).

			DistanceDesc.toPose.paramArray[0]  - This is vector3D holds joint connection point in model coordinates of toObj ([0,0,0] if not defined). 			
			
			Miscelaneous parameters
			DistanceDesc.jointMiscelaneous.paramFlag - Flag to determine if we want to have enabled collisions between connected objects. (default = false, range = [true,false]).
			DistanceDesc.jointMiscelaneous.paramArray[0] - Force needed to break a joint connectio n in Newtons (default = -1 ... disabled, range = <0,+DBL_MAX)).
			DistanceDesc.jointMiscelaneous.paramArray[1] - Max projection distance. (default = -1 ... disabled, range = <0,+DBL_MAX)).
			
			Joint limit parameters
			DistanceDesc.jointLimitsArray[0].paramArray[0] - Min distance (default = -1 ... joint limit disabled, rangeMinDist = (0,maxDist>, rangeMaxDist = <minDist,+DBL_MAX))
			DistanceDesc.jointLimitsArray[1].paramArray[0] - Max distance (default = -1 ... joint limit disabled, rangeMinDist = (0,maxDist>, rangeMaxDist = <minDist,+DBL_MAX))

			Joint spring parameters
			DistanceDesc.jointSprings.paramArray[0] - Flag to determine if we want to enable that spring (default = 0, range = [0,1])
			DistanceDesc.jointSprings.paramArray[1] - Spring coefficient (default = 0, range = <0,+DBL_MAX))
			DistanceDesc.jointSprings.paramArray[2] - Damper coefficient (default = 0, range = <0,+DBL_MAX))
			DistanceDesc.jointSprings.paramArray[3] - Target position of spring where the spring force is zero. (default = 0, range = linear (-DBL_MAX,+DBL_MAX))

		@return int - joint id (range >= 0 , returns 0 on unsucces joint creation).

		@example

		@code

		DISTANCE_JOINT desc3;

		//The vehicles to be created and joined
		veh1 ,veh2;

		desc3.fromPose.paramArray[0] = vector3D(0,-3, 0);
		desc3.toPose.paramArray[0] = vector3D(0,3,0);

		desc3.jointSprings.paramArray[0] = 1;
		desc3.jointSprings.paramArray[1] = 20;
		desc3.jointSprings.paramArray[2] = 0;
		desc3.jointSprings.paramArray[3] = 3;

		desc3.jointLimitsArray[0].paramArray[0] = 2;
		desc3.jointLimitsArray[1].paramArray[0] = 5;

		joint = JointUtilities::createDistanceJoint(veh1,veh2, desc3);
		displayString += conversions::IntToString(joint)+"\\n";   

		@endcode

		@overloaded 
		
		None. 
		
		@related

		@remarks To create a distance joint for objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::createDistanceJoint(ControllableObject& fromObj, ControllableObject& toObj, DISTANCE_JOINT& desc).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static int createDistanceJoint(const ControllableObject& fromObj, const ControllableObject& toObj, const DISTANCE_JOINT& desc);

		/*!
		@description 
		Creates a 6 degrees of freedom configurable joint between two objects with specified parameters (all 6 DOFs can be locked/unlocked/limited by user). 
		Drives are used with the setDrive commands.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.12]

		@param fromObj - Joint created from this object.
		@param toObj - Joint created to this object.
		@param desc - This is the struct that holds revolute joint description. If instance is SixDofDesc

			SixDofDesc.fromPose.paramArray[0] - This is vector3D holds joint connection point in model coordinates of fromObj ([0,0,0] if not defined).
			SixDofDesc.fromPose.paramArray[1] - This is vector3D holds joint connection axis in model coordinates of fromObj (up vector if not defined).
			SixDofDesc.fromPose.paramArray[2] - This is vector3D holds joint connection direction in model coordinates of fromObj (forward vector if not defined).

			SixDofDesc.toPose.paramArray[0] - This is vector3D holds joint connection point in model coordinates of toObj ([0,0,0] if not defined).
			SixDofDesc.toPose.paramArray[1] - This is vector3D holds joint connection axis in model coordinates of toObj (up vector if not defined).
			SixDofDesc.toPose.paramArray[2] - This is vector3D holds joint connection direction in model coordinates of toObj (forward vector if not defined).
			
			Miscelaneous parameters
			SixDofDesc.jointMiscelaneous.paramFlag - Flag to determine if we want to have enabled collisions between connected objects. (default = false, range = [true,false]).
			SixDofDesc.jointMiscelaneous.paramArray[0] - Force needed to break a joint connectio n in Newtons (default = -1 ... disabled, range = <0,+DBL_MAX)).
			SixDofDesc.jointMiscelaneous.paramArray[1] - Max projection distance. If one side of the joint is outside this range then the other side will be setpos'd to the other without excessive velocity(default = -1 ... disabled, range = <0,+DBL_MAX)).
			SixDofDesc.jointMiscelaneous.paramArray[2] - Drive limits for joint drive if we want to drive by steering. How much to steer the joint, linked with the steering controls of the vehicle. (In Viking, for example.) In radians.(default = -1 ... disabled, range = (0,+pi>) (note: can be used only with attachChild command and only turnDrive can be driven this way)
			
			Joint limits parameters
			SixDofDesc.jointLimitsArray[0] - Yaw left in picture
			SixDofDesc.jointLimitsArray[1] - Yaw right in picture
			SixDofDesc.jointLimitsArray[2] - Roll in picture, mirrored (used for both sides). In radians. Rolls according to child object's joint direction (toPose) or parent object's model coordinates.
			SixDofDesc.jointLimitsArray[3] - Pitch in picture, mirrored (used for both sides). In radians. Pitches according to child object's joint direction (toPose) or parent object's model coordinates.
			SixDofDesc.jointLimitsArray[4] - XYZ movement limit, limited to distance from center, in meters.
				Above each and every joint limits parameters has following joint limit parameters.
				paramArray[0] - limit bounce (default = -1 ... joint limit disabled, range = <0,1>)).
				paramArray[1] - The angle beyond which the limit is active (default = 0, range = angular (-pi,+pi>)
				paramArray[2] - Pitch in picture, mirrored (used for both sides). In radians. Pitches according to child object's joint direction (toPose) or parent object's model coordinates.
				paramArray[3] - if spring is greater than zero, this is the damping of the spring. How hard it is to move the joint when it is pushed over the limit angle. (default = 0, range = <0,+DBL_MAX)).
			
			Joint drives parameters
			SixDofDesc.jointDrivesArray[0] - Turn drive
			SixDofDesc.jointDrivesArray[1] - Swing drive
			SixDofDesc.jointDrivesArray[2] - Slerp drive.
			SixDofDesc.jointDrivesArray[3] - X drive
			SixDofDesc.jointDrivesArray[4] - Y drive
			SixDofDesc.jointDrivesArray[5] - Z drive
				Above each and every joint drives parameters has following joint drive parameters.
				paramArray[0] - Drive type (default = -1 ... disabled, range [0,1], 0 - spring, 1 - velocity drive)
				paramArray[1] - If spring type: Number - Spring force coefficient. In Newtons. How much to force back to initial joint position. (default = 0, range (-DBL_MAX,+DBL_MAX))
				paramArray[2] - Damper coefficient. How hard it is to move the joint. (default = 0, range = <0,+DBL_MAX))
				paramArray[3] - Target angle of spring where the spring force is zero. How hard it is to move the joint. (default = +DBL_MAX, range = <0,+DBL_MAX))

			Joint degrees of freedom parameters
			SixDofDesc.jointDofs.paramArray[0] - Yaw in picture 
			SixDofDesc.jointDofs.paramArray[1] - Roll in picture 
			SixDofDesc.jointDofs.paramArray[2] - Pitch in picture 
			SixDofDesc.jointDofs.paramArray[3] - linear movement for X 
			SixDofDesc.jointDofs.paramArray[4] - linear movement for Y
			SixDofDesc.jointDofs.paramArray[5] - linear movement for Z
			
		@return int - joint id (range >= 0 , returns 0 on unsucces joint creation).

		@example

		@code

		SIX_DOF_JOINT desc4;

		//The vehicles to be created and joined
		veh1 ,veh2;

		desc4.fromPose.paramArray[0] = vector3D(0,-3, 0);
		desc4.fromPose.paramArray[1] = vector3D(0,-2, 0);
		desc4.fromPose.paramArray[2] = vector3D(0,-3, 1);

		desc4.toPose.paramArray[0] = vector3D(0,3, 0);
		desc4.toPose.paramArray[1] = vector3D(0,4, 0);
		desc4.toPose.paramArray[2] = vector3D(0,3, 1);

		desc4.jointMiscelaneous.paramFlag = true;
		desc4.jointMiscelaneous.paramArray[0] = -1;
		desc4.jointMiscelaneous.paramArray[1] = -1;
		desc4.jointMiscelaneous.paramArray[2] = 0.6;

		desc4.jointDofs.paramArray[0] = 2;
		desc4.jointDofs.paramArray[0] = 2;
		desc4.jointDofs.paramArray[1] = 1;
		desc4.jointDofs.paramArray[2] = 1;
		desc4.jointDofs.paramArray[3] = 0;
		desc4.jointDofs.paramArray[4] = 0;
		desc4.jointDofs.paramArray[5] = 0;

		desc4.jointLimitsArray[2].paramArray[0] = 0;
		desc4.jointLimitsArray[2].paramArray[1] = 1;

		desc4.jointLimitsArray[3].paramArray[0] = 0;
		desc4.jointLimitsArray[3].paramArray[1] = 1;

		desc4.jointDrivesArray[0].paramArray[0] = 0;
		desc4.jointDrivesArray[0].paramArray[1] = 10000;

		joint = JointUtilities::create6DOFJoint(veh1,veh2, desc4);
		displayString += conversions::IntToString(joint)+"\\n";    

		@endcode

		@overloaded  
		
		@related

		@remarks To create a 6 degrees joint for objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::create6DOFJoint(ControllableObject& fromObj, ControllableObject& toObj, SIX_DOF_JOINT& desc).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static int create6DOFJoint(const ControllableObject& fromObj, const ControllableObject& toObj, const SIX_DOF_JOINT& desc);

		/*!
		@description 
		Deletes a physical joint between two objects.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.12]

		@param jointId - Joint id return by create joint functions.

		@return Nothing.

		@example

		@code

		int joint = -1;
		JointUtilities::deleteJoint(joint);
		displayString += conversions::IntToString(joint)+"\\n";

		@endcode

		@overloaded  
		
		@related

		@remarks To delete the joint between objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::deleteJoint(int jointId).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static void deleteJoint(int jointId);
		
		/*!
		@description 
		
		Used to attach a PhysX child element. The applyChildAttach method creates a default spherical joint between the two vehicles passed in the function. 
		It also creates a logical coupling between them which is useful for things such as brakes on trailers etc.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param parent - The object the 'child' is being attached to.

		@param child - The object that is being attached to 'parent'.

		@return Nothing.

		@example

		@code

		//veh1 and veh2 should be created before being passed as a parameter
		JointUtilities::applyChildAttach (veh1, veh2);

		@endcode

		@overloaded  

		JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, SIX_DOF_JOINT& desc);
	
		@related

		JointUtilities::getAttachedChild(ControllableObject& parent)

		@remarks To create an attachment (joint) between objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static void applyChildAttach(const ControllableObject& parent, const ControllableObject& child);

		/*!
		@description 

		Used to detach a PhysX child from specified parent object within the VBS2 environment.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param parent - The parent object to detach from.

		@return Nothing.

		@example

		@code

		//veh1 to be created
		JointUtilities::applyChildDetach(veh1);

		@endcode

		@overloaded
		
		None.
		
		@related

		JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child)

		@remarks To detach an attachment between objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::applyChildDetach(ControllableObject& parent).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static void applyChildDetach(const ControllableObject& parent);

		/*!
		@description 

		Used to return the attached child object for a specified parent object within the VBS2 environment.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param parent - The parent object.

		@return ControllableObject - The child object.

		@example

		@code

		ControllableObject co;
		//veh1 to be created
		co = JointUtilities::getAttachedChild(veh1);

		@endcode

		@overloaded
		
		None.
		
		@related

		JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child)

		@remarks To obtain the details of a child object in a joint (attachment) created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::getAttachedChild(ControllableObject& parent).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static ControllableObject getAttachedChild(const ControllableObject& parent);

		/*!
		@description 

		Used to return the parent object of an attached child object within the VBS2 environment.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param child - The child object.

		@return ControllableObject - The parent object.

		@example

		@code

		ControllableObject co;
		//veh1 to be created
		co = JointUtilities::getAttachedParent(veh1);
		displayString +="The parent of veh1 is"+co.getName();
		
		@endcode

		@overloaded
		
		None.
		
		@related

		JointUtilities::getAttachedChild(ControllableObject& parent)

		@remarks To obtain the details of a parent object in a joint (attachment) created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::getAttachedParent(ControllableObject& child).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static ControllableObject getAttachedParent(const ControllableObject& child);

		/*!
		@description 

		Used to attach a child to a parent by 6 degrees of freedom configurable joint with specified parameters within the VBS2 environment.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param parent - The parent ControllableObject.

		@param child - The child ControllableObject.

		@param desc - SIX_DOF_JOINT joint description.

		@return Nothing.

		@example

		@code

		SIX_DOF_JOINT desc4;

		//The vehicles to be created and attached
		veh1 ,veh2;

		desc4.fromPose.paramArray[0] = vector3D(0,-3, 0);
		desc4.fromPose.paramArray[1] = vector3D(0,-2, 0);
		desc4.fromPose.paramArray[2] = vector3D(0,-3, 1);

		desc4.toPose.paramArray[0] = vector3D(0,3, 0);
		desc4.toPose.paramArray[1] = vector3D(0,4, 0);
		desc4.toPose.paramArray[2] = vector3D(0,3, 1);

		desc4.jointMiscelaneous.paramFlag = true;
		desc4.jointMiscelaneous.paramArray[0] = -1;
		desc4.jointMiscelaneous.paramArray[1] = -1;
		desc4.jointMiscelaneous.paramArray[2] = 0.6;

		desc4.jointDofs.paramArray[0] = 2;
		desc4.jointDofs.paramArray[0] = 2;
		desc4.jointDofs.paramArray[1] = 1;
		desc4.jointDofs.paramArray[2] = 1;
		desc4.jointDofs.paramArray[3] = 0;
		desc4.jointDofs.paramArray[4] = 0;
		desc4.jointDofs.paramArray[5] = 0;

		desc4.jointLimitsArray[2].paramArray[0] = 0;
		desc4.jointLimitsArray[2].paramArray[1] = 1;

		desc4.jointLimitsArray[3].paramArray[0] = 0;
		desc4.jointLimitsArray[3].paramArray[1] = 1;

		desc4.jointDrivesArray[0].paramArray[0] = 0;
		desc4.jointDrivesArray[0].paramArray[1] = 10000;

		JointUtilities::applyChildAttach(veh1,veh2,desc4);
		
		@endcode

		@overloaded
		
		JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, SPHERE_JOINT& desc)
		
		@related

		JointUtilities::getAttachedParent(ControllableObject& child)

		@remarks To create an attachment (6 degrees joint) between objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, SIX_DOF_JOINT& desc).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void applyChildAttach(const ControllableObject& parent, const ControllableObject& child, const SIX_DOF_JOINT& desc);

		/*!
		@description 

		Used to attach a child to a parent by a spherical joint with specified parameters within the VBS2 environment.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param parent - The parent ControllableObject.

		@param child - The child ControllableObject.

		@param desc - SPHERE_JOINT joint description.

		@return Nothing.

		@example

		@code

		SPHERE_JOINT desc5;

		//The vehicles to be created and attached
		veh1 ,veh2;

		desc5.fromPose.paramArray[0] = vector3D(0,-8, 2);
		desc5.fromPose.paramArray[1] = vector3D(0,-2,0);

		desc5.toPose.paramArray[0] = vector3D(0,4,2);

		desc5.jointLimitsArray[0].paramArray[0] = 0;
		desc5.jointLimitsArray[0].paramArray[1] = -0.5;

		desc5.jointLimitsArray[1].paramArray[0] = 0;
		desc5.jointLimitsArray[1].paramArray[1] = 0.5;

		desc5.jointLimitsArray[2].paramArray[0] = 0.5;
		desc5.jointLimitsArray[2].paramArray[1] = 1;

		JointUtilities::applyChildAttach(veh1,veh2,desc5);

		@endcode

		@overloaded
		
		JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, SIX_DOF_JOINT& desc)
		
		@related

		JointUtilities::getAttachedChild(ControllableObject& parent);

		@remarks To create an attachment (spherical joint) between objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, SPHERE_JOINT& desc).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void applyChildAttach(const ControllableObject& parent, const ControllableObject& child, const SPHERE_JOINT& desc);

		/*!
		@description 
		Used to attach a child to a parent by revolute joint with specified parameters within the VBS2 environment.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param parent - The parent ControllableObject.

		@param child - The child ControllableObject.

		@param desc - REVOLUTE_JOINT joint description.

		@return Nothing.

		@example

		@code

		//The objects to be created and attached

		veh,co;

		REVOLUTE_JOINT desc6;

		desc6.fromPose.paramArray[0] = vector3D(0,-3,0);
		desc6.fromPose.paramArray[1] = vector3D(0,-2,0);
		desc6.fromPose.paramArray[2] = vector3D(0,-3,1);

		desc6.toPose.paramArray[0] = vector3D(0,3,0);
		desc6.toPose.paramArray[1] = vector3D(0,4,0);
		desc6.toPose.paramArray[2] = vector3D(0,3,1);

		desc6.jointLimitsArray[0].paramArray[0] = 1;
		desc6.jointLimitsArray[0].paramArray[1] = -2;

		desc6.jointLimitsArray[1].paramArray[0] = 1;
		desc6.jointLimitsArray[1].paramArray[1] = 1;

		desc6.jointDrives.paramArray[0] = 1;
		desc6.jointDrives.paramArray[1] = 3;
		desc6.jointDrives.paramArray[2] = 1;
		desc6.jointDrives.paramArray[3]=0;

		desc6.jointSprings.paramArray[0]=1;
		desc6.jointSprings.paramArray[1]=200;
		desc6.jointSprings.paramArray[2]=0;
		desc6.jointSprings.paramArray[3]=1.5;

		JointUtilities::applyChildAttach(veh,co,desc6);
		
		@endcode

		@overloaded
		
		JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, FIXED_JOINT& desc);
		
		@related

		JointUtilities::getAttachedChild(ControllableObject& parent);

		@remarks To create an attachment (revolute joint) between objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, REVOLUTE_JOINT& desc).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void applyChildAttach(const ControllableObject& parent, const ControllableObject& child, const REVOLUTE_JOINT& desc);

		/*!
		@description 

		Used to attach a child to a parent by fixed joint with specified parameters within the VBS2 environment.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param parent - The parent ControllableObject.

		@param child - The child ControllableObject.

		@param desc - FIXED_JOINT joint description.

		@return Nothing.

		@example

		@code

		FIXED_JOINT desc2;

		desc2.fromPose.paramArray[0] = vector3D(0,-9, 0);
		desc2.toPose.paramArray[0] = vector3D(0,9,0);
		desc2.jointMiscelaneous.paramFlag = false;
		desc2.jointMiscelaneous.paramArray[0] = 0;
		desc2.jointMiscelaneous.paramArray[1] = 0;

		//veh1 and veh2 to be created

		JointUtilities::applyChildAttach(veh1,veh2,desc2);

		@endcode

		@overloaded
		
		JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, REVOLUTE_JOINT& desc);

		@related

		JointUtilities::getAttachedParent(ControllableObject& child)

		@remarks To create an attachment (fixed joint) between objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, FIXED_JOINT& desc).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void applyChildAttach(const ControllableObject& parent, const ControllableObject& child, const FIXED_JOINT& desc);

		/*!
		@description 
		Used to attach a child to a parent by distance joint with specified parameters within the VBS2 environment.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param parent - The parent ControllableObject.

		@param child - The child ControllableObject.

		@param desc - DISTANCE_JOINT joint description.

		@return Nothing.

		@example

		@code

		DISTANCE_JOINT desc3;

		desc3.fromPose.paramArray[0] = vector3D(0,-3, 0);
		desc3.toPose.paramArray[0] = vector3D(0,3,0);

		desc3.jointSprings.paramArray[0] = 1;
		desc3.jointSprings.paramArray[1] = 20;
		desc3.jointSprings.paramArray[2] = 0;
		desc3.jointSprings.paramArray[3] = 3;

		desc3.jointLimitsArray[0].paramArray[0] = 2;
		desc3.jointLimitsArray[1].paramArray[0] = 5;

		//veh1 and veh2 to be created

		JointUtilities::applyChildAttach(veh1,veh2,desc3);

		@endcode

		@overloaded
		
		JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, FIXED_JOINT& desc)

		@related

		@remarks To create an attachment (Distant joint) between objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::applyChildAttach(ControllableObject& parent, ControllableObject& child, DISTANCE_JOINT& desc).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void applyChildAttach(const ControllableObject& parent, const ControllableObject& child, const DISTANCE_JOINT& desc);

		/*!
		@description 
		Used to create a list of chained physX objects where all neighbors are connected via joint within the VBS2 environment.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param fromObj - ControllableObject to start creating a rope from.

		@param fromPoint - Specifies where on the object the rope should start.

		@param toObj - ControllableObject to attach last segment of rope to.

		@param toPoint - Specifies where on the object the rope should finish.

		@param rope - attributes of the rope to create in ROPE_ATTRIBUTES format. (Select link for detailed description)

		@return ROPE_ELEMENTS - Two containers with created rope segments and joints. (Select link for detailed description)

		@example

		@code

		
		ROPE_ATTRIBUTES rope_at(ROPE_SEGMENT,10);

		rope_at.swingSpring = 10;
		rope_at.twistSpring = 10;
		rope_at.damping = 0.5;

		//veh1 and veh2 to be created
		JointUtilities::createRope(veh1,vector3D(0,0,-1),veh2,vector3D(0,0,2),rope_at);


		@endcode

		@overloaded
		
		None.

		@related

		JointUtilities::deleteRope(ControllableObject& connectedObj, ROPE_CLASS ropeCls)

		@remarks To create a joint between objects created in a client machine within a network using ropes, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::createRope(ControllableObject& fromObj, vector3D& fromPoint,ControllableObject& toObj, vector3D& toPoint, ROPE_ATTRIBUTES& rope).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)


		*/
		static ROPE_ELEMENTS createRope(const ControllableObject& fromObj, const vector3D& fromPoint,
			const ControllableObject& toObj, const vector3D& toPoint, const ROPE_ATTRIBUTES& rope);

		/*!
		@description 
		Used to destroy a PhysX rope collection within the VBS2 environment.
		This function considers all objects that are connected via some joint with specified object (even mediately), considers those that are having the class name "ropeCls" and removes them from the scene.

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param connectedObj - ControllableObject connected to rope to be destroyed.

		@param ropeCls - ROPE_CLASS that represents a rope segment.

		@return Nothing.

		@example

		@code

		JointUtilities::deleteRope(veh2,ROPE_CLASS::ROPE_SEGMENT);

		@endcode

		@overloaded
		
		None.
		
		@related

		JointUtilities::createRope(ControllableObject& fromObj, vector3D& fromPoint,ControllableObject& toObj, vector3D& toPoint, ROPE_ATTRIBUTES& rope);

		@remarks To delete a rope (joint) between objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::deleteRope(ControllableObject& connectedObj, ROPE_CLASS ropeCls).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void deleteRope(const ControllableObject& connectedObj, const ROPE_CLASS ropeCls);

		/*!
		@description 
		Returns a container with all chained objects (which were generated by a createRope command).

		@locality

		Globally Applied and Globally Effected

		@version  [VBS2Fusion v3.15]

		@param fromObj - ControllableObject.

		@return ROPE_CHAIN - All chained objects, as well as the parent object itself.

		@example

		@code

		ROPE_CHAIN chain = JointUtilities::getObjectsChained(veh2);
		for(ROPE_CHAIN::iterator itr = chain.begin(),itr_end = chain.end();itr!=itr_end;++itr)
		{
		displayString+="The objects attached are"+itr->getName();

		}

		@endcode

		@overloaded
		
		None.
		
		@related

		JointUtilities::deleteRope(ControllableObject& connectedObj, ROPE_CLASS ropeCls)

		@remarks To obtain all chained objects in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function JointUtilities::getObjectsChained(ControllableObject& fromObj).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static ROPE_CHAIN getObjectsChained(const ControllableObject& fromObj);
	};
};

#endif //VBS2FUSION_JOINTUTILITES_H