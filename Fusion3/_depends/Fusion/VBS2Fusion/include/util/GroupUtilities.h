/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

GroupUtilities.h

Purpose:

This file contains the declaration of the GroupUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			31-03/2009	RMR: Original Implementation
1.01		27-08/2009  SDS: Added updateIsLocal(Group& grp);	
							 Updated updateGroupStaticProperties(Group& grp)
2.01		10-02/2010	MHA: Comments Checked
2.02        15-06/2010  YFP: Methods added,
							   void applyCustomFormation(Group&)
							   void updateCustomFormation(Group&)

2.03		04-01/2011	CGS: Methods added,
								createGroup(Group& _group, list<string> typelist,position3D pos)
								createGroup(Group & _group, list<Unit> unitlist,position3D pos)
								deleteGroup(Group& _group)
								setLeader(Group& group,Unit& u)
							  Modified,
								loadGroupMembers(Group& group)
								createAndAddWaypoint(Group& group, Waypoint& wp)
								updateGroupDynamicProperties(Group& group)
								getFormation(Group& group)
								updateFormation(Group& group)
								applyFormation(Group& group, string formation)
								getCurrentWaypoint(Group& group)
								updateCurrentWaypoint(Group& group)
								updateSide(Group& group)
								loadWaypoints(Group& group)
								updateGroupID(Group& group)
								updateIsLocal(Group& group)
								applyAlias(Group& group)
								getIDUsingAlias(Group& grp)
								applyCustomFormation(Group& group)	
2.04		28-09/2011	NDB: Added Method void allowFleeing(Group& grp, double cowardice)
2.05		04-11/2011	NDB: Added Method float knowsAbout(Group& grp, ControllableObject& target)

/************************************************************************/

#ifndef VBS2FUSION_GROUP_UTILITIES_H
#define VBS2FUSION_GROUP_UTILITIES_H

#include <sstream>
#include <string>

#include "position3D.h"
#include "VBSFusion.h"
#include "data/ControllableObject.h"

namespace VBSFusion
{
	class VBSFUSION_API GroupUtilities
	{
	public:

		/*!
		Creates a new group using the side obtained from group.getSide(). 

		Error checking utility validates the following:

		- side(outputs an error if the side is invalid)		 		
		*/
		static void createGroup(Group& group);

		/*!
		Creates a new group using side. 

		If not alias is assigned to the group, a random alias is generated 
		and assigned. 

		Error checking utility validates the following:

		- side(outputs an error if the side is invalid)	
		*/
		static void createGroup(Group& group, const std::string& side);

		/*!
		Creates a new group using the units specified in unitlist and position. 
	 		
		*/
		static void createGroup(Group& group, std::list<Unit>& unitlist, const position3D& pos);

		/*!
		Creates a new group using the units specified in type list and position. 
	 	For each entry in the type list, a unit is added to the group. 
		*/
		static void createGroup(Group& group, const std::list<std::string>& typelist, const position3D& pos);

		/*!
		Deletes a group in VBS side if there is no units inside the Group. 
	 		
		*/
		static void deleteGroup(Group& _group);

		//*******************************************************

		/*!
		Switch the unit into different Group	
		Deprecated. Use void applyGroupSwitch(Group& group, Unit& u).
		*/
	
		VBSFUSION_DPR(UGRP007) static void switchGroup(Group& group, Unit& u);
		
		/*!
		Assign a Leader to the group.
		Deprecated. Use void applyLeader(Group& group,Unit& u)
		*/
		
		VBSFUSION_DPR(UGRP001) static void setLeader(Group& group,Unit& u);

		/*!
		Returns the formation of the group in string format.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- formation (outputs an error if empty formation returned)	
		*/
		static std::string getFormation(const Group& group);

		/*!
		Updates and saves the formation of the group.
		*/
		static void updateFormation(Group& group);	

		/*!
		Applies the formation obtained from group.getFormation() to the group. 
		*/
		static void applyFormation(Group& group);

		/*!
		Applies the formation defined in formation to the group.

		Error checking utility validates the following:

		- formation (outputs an error if empty string passed)	
		*/
		static void applyFormation(Group& group,const std::string& formation);

		/*!
		@description 
		
		Applies a custom formation to a group. Formations can only be selected in behaviour mode 'AWARE'.
		
		@locality
		
		Locally applied, Globally effected

		@version  [VBS2Fusion v3.2.5]

		@param group - A Group object which consist the necessary Unit objects.

		@return Nothing

		@example

		@code
		
		GroupUtilities::applyGroupBehaviour(sGroup,B_AWARE);

		position3D pos (4,0,0);

		for (Group::iterator itr = sGroup.begin(); itr != sGroup.end(); itr++)
		{
		
			CUSTOMFORMATION formation;
			formation.offset = pos;
			formation.angle = 90;

			itr->setCustomFormation(formation);
			pos.setX(pos.getX()+4);

		}

		GroupUtilities::applyCustomFormation(sGroup);

		@endcode

		@overloaded

		None

		@related
		
		None

		@remarks Formations can only be selected in behaviour mode "AWARE" and higher (i.e. weapon raised). For "CARELESS" and "SAFE" modes, AI will always move in column formation.
		
		@remarks If the group leader changes directions, it will take a while for the group to "catch up" with him, and for everyone to be in the correct formation positioned again.
	
		@remarks New formation is always in relation to the group leader. 
		
		@remarks If no view direction is specified, units will look into formation direction.
		
		@remarks If no formation definition exists for a group member he will stay in the original formation.

		@remarks An empty formation array will reset the formation for the whole group, and is required to select default formations again.


		*/
		static void applyCustomFormation(Group& group);

		/*!
		Update custom formation of members in the group.
		*/
		static void updateCustomFormation(Group& group);

		//*******************************************************

		/*!
		Return the index of the current waypoint of the group. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		*/
		static int getCurrentWaypoint(const Group& group);		

		/*!
		Update the index of the current waypoint of the group. 
		*/
		static void updateCurrentWaypoint(Group& group);	

		/*!
		Apply the active Waypoint as a current Waypoint for the specified group.
		*/
		static void applyCurrentWaypoint(Group& group, const Waypoint& wp);

		/*!
		Apply the active Waypoint as a current Waypoint for the specified group by 
		passing the correct waypoint index.
		*/
		static void applyCurrentWaypoint(Group& group);


		//*******************************************************				

		/*!
		Update the side of the group.
		*/
		static void updateSide(Group& group);

		/*!
		Return the side of the group.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		*/
		static std::string getSide(const Group& group);

		//*******************************************************

		/*!
		Return the name of the leader of group. 
		*/
		static std::string getLeader(const Group& group);

		/*!
		Update the name of the group leader. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)

		Warning utility warns the following

		- output an warning if group size is zero
		*/

		static void updateLeader(Group& group);

		//*******************************************************

		/*!
		Compound Update for the group. Updates the following:

		- Group Dynamic properties: formation, leader, current waypoint. 
		- Group static properties: side.
		- Dynamic properties of all members.
		- Static properties of all members.
		- All waypoint properties. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)
		*/
		static void updateGroup(Group& group);		

		//*******************************************************

		/*!
		Updates dynamic properties of the group: formation, leader and current waypoint index.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)
		*/
		static void updateGroupDynamicProperties(Group& group);
		
		/*!
		Updates static properties of the group: side. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)

		Warning utility warns the following

		- output an warning if group size is zero
		*/
		static void updateGroupStaticProperties(Group& group);

		/*!
		Updates the dynamic properties of all members using UnitUtilities::updateDynamicProperties. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)
		*/
		static void updateMembersDynamicProperties(Group& group);

		/*!
		Updates the static properties of all members using UnitUtilities::updateStaticProperties.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)
		*/
		static void updateMembersStaticProperties(Group& group);

		/*!
		Updates all waypoint properties.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)
		- group (outputs an error if group is invalid)
		*/
		static void updateWaypointProperties(Group& group);

		//********************************************************

		/*!
		Loads the group from the game environment into the group variable. 

		- clears the unit and waypoint lists. 
		- loads the group name from VBS2
		- updates group static properties
		- updates group dynamic properties
		- loads group members
		- loads Waypoints
		- updates members dynamic and static properties
		- updates Waypoints dynamic and static properties

		Note: Displays an error on the screen if a group of the given name/alias (i.e. group.getAlias())
		does not exist in the VBS2 environment. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 


		Error checking utility validates the following:

		- group (outputs an error if group is invalid)
		- NetworkID (outputs an error if the id returned is invalid)
		*/
		static void loadGroup(Group& group);

		//********************************************************

		/*!
		Loads all Waypoints belonging to the group. 

		Error checking utility validates the following:

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		- group (outputs an error if group is invalid)
		- NetworkID (outputs an error if the id returned is invalid)
		*/
		static void loadWaypoints(Group& group);	

		/*!
		Updates the Waypoints belonging to the group.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		- group (outputs an error if group is invalid)
		- NetworkID (outputs an error if the id returned is invalid)
		*/

		static void updateWaypointList(Group& group);


		//********************************************************

		/*!
		Loads all units belonging to the group. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to group. 

		- group (outputs an error if group is invalid)
		- NetworkID (outputs an error if the id returned is invalid)
		*/
		static void loadGroupMembers(Group& group);

		/*!
		Updates all units belonging to the group. 
		*/
		static void updateGroupMembership(Group& group);

	//	//********************************************************

		/*!
		Creates a new unit specified by unit and adds as a member of the group. The new unit is added
		to the end of the unit list. If no alias is assigned to the unit, a random alias is generated. 
		*/
		static void createAndAddUnit(Group& group, Unit& unit);	

	//	//********************************************************

		/*!
		Creates a new waypoint specified by waypoint and adds as a member of the group. The new waypoint is added
		to the end of the waypoint list. If no alias is assigned to the waypoint, a random alias is generated. 
		*/

		static void createAndAddWaypoint(Group& group, Waypoint& wp);			

		//-------------------------------------------------------
		// NetworkId utility
		//-------------------------------------------------------

		/*!
		Update "isLocal" property in group to know whether co has valid
		network id or not		
		*/
		static void updateIsLocal(Group& group);	

		/*!
		updates the Waypoints belonging to the group.

		error checking utility validates the following:

		- NetworkId (outputs an error if the network id is invalid)
		*/
		static void updateGroupID(Group& grp);

		/*!
		get the NetworkId belonging to the group.

		Error checking utility validates the following:

		- alias (outputs an error if alias is invalid)
		*/
		static std::string getIDUsingAlias(const Group& grp);

		/*!
		Return whether this group is in loaded by VBS2Fusion
		Deprecated. Use bool isAvailable(string groupName)
		*/
		VBSFUSION_DPR(UGRP010) static bool groupExists(std::string groupName);

		/*!
		Applies alias to the group, if the applied alias has not 
		already been assigned to another group in the mission.
		*/
		static bool applyAlias(Group& group);

		/*!
		Gets the total number of waypoints belonging to the group.
		*/
		static int getNoOfWaypoints(const Group& group);

		/*!
		Disable switching to next waypoints.
		Deprecated. Use void applyWaypointsLock(Group& group)
		*/
		VBSFUSION_DPR(UGRP002) static void lockWaypoints(Group& group);
		
		/*!
		Enables switching to next waypoints.
		Deprecated. Use void applyWaypointsUnlock(Group& group)
		*/
		VBSFUSION_DPR(UGRP003) static void unLockWaypoints(Group& group);

		/*!
		Sets the cowardice level (the lack of courage or bravery) of a group.

		By setting fleeing to 0.75, units have a 75% chance of fleeing (and 25% of not fleeing). Once they have decided not to flee, they will never flee.

		0 means maximum courage, while 1 means always fleeing.
		V1.50+: If allowFleeing is set to 0 for a unit that is already fleeing, that unit will stop. (In previous versions changing allowFleeing in a situation like this would not have had any effect.)
		Deprecated. Use void applyFleeing(Group& grp, double cowardice)
		*/
		VBSFUSION_DPR(UGRP004) static void allowFleeing(Group& grp, double cowardice);

		/*!
		Gets the total number of waypoints belonging to the group. 
		*/
		static int getTotalWaypoints(const Group& grp);

		/*!
		Returns whether smart formation re-ordering is enabled for a group. 
		*/
		static bool isEnabledSmartOrder(const Group& grp);

		/*!
		Controls behavior of group members when changing formations. Default is on. 
		enable - If true, then group members might be re-ordered when assuming a new formation 
		(i.e. each unit will move to the position in the new formation that is closest to them). If false, 
		then groups will not be re-ordered.
		Deprecated. Use void applySmartOrderEnable(Group& group, bool enable).
		*/
		VBSFUSION_DPR(UGRP008) static void EnableSmartOrder(Group& grp, bool enable);

		/*!
		Returns the path post-processing mode for an AI group 
		returns Which group members will post-process their pathfinding ("ALL" or "ONLYLEADER")
		*/
		static std::string getPathPostProcessMode(const Group& grp);

		/*!
		Sets the pathfinding post-processing mode for an AI group (e.g. walking on side of road).

		mode - Which group members should post-process their path:
		"ONLYLEADER" - only group leader (default)
		"ALL" - all units in group
		"NONE" - no unit in group (V1.40+)
		*/
		static void applyPathPostProcessMode(Group& grp, PATHPOSTPROCESSMODE mode);

		/*!
		Returns an array with all the units in the group or unit. For a destroyed object an empty array is returned
		*/
		static std::vector<Unit> getUnits(const Group& grp);


		/*!
		Sets the group formation direction
		*/
		static void applyFormationDirection(Group& grp, double direction);

		/*!
		Set group formation heading. Accepted heading range is 0 to 360. 
		Formation is facing this direction unless enemy is seen. 
		When group is moving, this value is overriden by movement direction.
		*/
		static void applyFormDirection(Group& group, double direction);

		/*!
		Creates a move waypoint on given position and makes it an actual group waypoint.
		Deprecated. Use void applyMoveDestination(Group& group, position3D pos).
		*/
		VBSFUSION_DPR(UGRP009) static void move(Group& grp, position3D pos);

		/*!
			set the behaviour of the Group. 
			  CARELESS,SAFE,AWARE,COMBAT,STEALTH
		*/
		static void applyGroupBehaviour(Group& grp, BEHAVIOUR behaviour);

		 /*!
		 Sets unit combat mode (engagement rules). Does not work for groups.

		 Mode may be one of:
		 "BLUE" (Never fire)
		 "GREEN" (Hold fire - defend only)
		 "WHITE" (Hold fire, engage at will)
		 "YELLOW" (Open fire)
		 "RED" (Open fire, engage at will) 

		 returns True if combat mode was applied sucessfully, otherwise false. 
		 */
		 static void applyGroupCombatMode(Group& grp, WAYPOINTCOMBATMODE mode);

		/*!
		Returns the combat mode of the Group.

		Modes returned may be:

		"BLUE" (Never fire)
		"GREEN" (Hold fire - defend only)
		"WHITE" (Hold fire, engage at will)
		"YELLOW" (Fire at will)
		"RED" (Fire at will, engage at will) 

		or

		"UNIT ERROR" (combat mode couldn't be retrieved) 
		*/
		static std::string getGroupCombatMode(const Group& grp);

		/*!
		Set group speed mode. Mode may be one of:

		"LIMITED" (half speed)
		"NORMAL" (full speed, maintain formation)
		"FULL" (do not wait for any other units in formation) 
		*/
		static void applySpeedMode(Group& grp, WAYPOINTSPEEDMODE speedMode);

		/*!
		Return the speed mode of the group in string format.
		*/
		static std::string getSpeedMode(const Group& grp);

		/*!
		Checks whether the Group object is Null or not. It will be false, only that group object is available
		in VBS as a group. It will return true, if grp exists as grpNull that means as a non existing group or if it
		is not linked with VBS group.
		*/
		static bool isNull(const Group& grp);

		/*!
		Set variable to given value in the variable space of given element. To remove a variable, set it to nil
		_public - If true, then the value is broadcast to all computers.
		*/
		static void applyVariable(Group& grp, const std::string& name, const std::string& val, bool _public);

		/*!
		Return the value of variable in the variable space of given element.
		The command is case sensitive
		*/
		static std::string getVariable(const Group& grp, const std::string& name);

		/*!
		Set a group's identity.

		noOfString - It should be the no of the string arguments in the function for calling

		Group ID format should be [letter, color, picture] or [letter, color].

		Letter is one of:
		"Alpha"
		"Bravo"
		"Charlie"
		"Delta"
		"Echo"
		"Foxtrot"
		"Golf"
		"Hotel"
		"Kilo"
		"Yankee" 

		"Zulu"
		"Buffalo"
		"Convoy"
		"Guardian"
		"November"
		"Two"
		"Three"
		"Fox" 

		Colour may be one of the following:
		"GroupColor0" - (Nothing)
		"GroupColor1" - Black
		"GroupColor2" - Red
		"GroupColor3" - Green
		"GroupColor4" - Blue
		"GroupColor5" - Yellow
		"GroupColor6" - Orange
		"GroupColor7" - Pink
		"Six" - Six 
		*/
		static void applyGroupID(Group& grp, int noOfString, const std::string& nameFormat1, ...);

		/*!
		Check if (and by how much) unit knows about target.
		And returns a number from 0 to 4.
		Deprecated. Use double getKnowsAbout(Group& group, ControllableObject& target).
		*/
		VBSFUSION_DPR(UGRP006) static float knowsAbout(Group& grp, ControllableObject& target);


		/*!
		Check if (and by how much) unit knows about target.
		And returns a number from 0 to 4.
		*/
		static double knowsAboutEx(const Group& grp, const ControllableObject& target);

		/*!
		Returns the unlocalized text value of a group's side. 
		As: East, West, Resistance, Civilian, or Unknown.
		*/
		static std::string getSideTextString(const Group& grp);

		/*!
		Set if leader can issue attack commands to the soldiers in his group
		Deprecated. Use void applyAttackEnable(Group& group, bool boolVal).
		*/
		VBSFUSION_DPR(UGRP005) static void setEnableAttack(Group& group, bool boolVal);

		/*!
		Return whether a group's leader can issue attack commands to soldiers under his command. 
		*/
		static bool isAttackEnabled(const Group& group);

		/*!
		Checks whether two groups are the same.
		*/
		static bool isSame(const Group& group1, const Group& group2);

		/*!
		Checks whether two groups are different. If either of them is grpNull, true is returned.
		*/
		static bool isDifferent(const Group& group1, const Group& group2);

		/*!
		Assign leader to the group. The Unit which is going to be the leader should be in the same
		group then only particular unit will be assigned as a leader of that group.
		@param group - Group object to which leader is set.
		@param u - Unit object which is going to be the leader.
		@return Nothing.
		@remarks This is a replication of void setLeader(Group& group,Unit& u)
		*/
		static void applyLeader(Group& group, const Unit& u);

		/*!
		Locks the waypoints for the group. This will disable switching to next waypoints. Current 
		waypoint will never be completed while applyWaypointsLock is used.
		@param group - Group object to which waypoints needs to be locked.
		@return Nothing.
		@remarks This is a replication of void lockWaypoints(Group& group)
		*/
		static void applyWaypointsLock(Group& group);

		/*!
		Unlocks the waypoints for the group. This will again enable switching to next waypoints, if applyWaypointsLock
		was already used.
		@param group - Group object to which waypoints needs to be unlocked.
		@return Nothing.
		@remarks This is a replication of void unLockWaypoints(Group& group)
		*/
		static void applyWaypointsUnlock(Group& group);

		/*!
		Sets the cowardice level (the lack of courage or bravery) of a group.By setting fleeing to 0.75, 
		units have a 75% chance of fleeing (and 25% of not fleeing). Once they have decided not to flee, 
		they will never flee.		
		@param grp - Group object to which fleeing value is applied.
		@param cowardice - double values between 0 and 1 can be passed. 0 means maximum courage, while 1 means always fleeing.
		@return Nothing.
		@remarks If allowFleeing is set to 0 for a group, the Units which are already fleeing in the group will stop fleeing.
		@remarks This is a replication of void allowFleeing(Group& grp, double cowardice)
		*/
		static void applyFleeing(Group& grp, double cowardice);

		/*!
		Sets whether leader can issue attack commands to the soldiers in his group. 
		@param group - Group object to which command is applied.
		@param boolVal - If boolVal is true leader can issue attack command otherwise he can't.
		@return Nothing.
		@remarks This is a replication of void setEnableAttack(Group& group, bool boolVal)
		*/
		static void applyAttackEnable(Group& group, bool boolVal);

		/*!
		Returns how much group knows about target object.AI unit will not fire on an enemy soldier until his knowsAbout 
		level of that enemy has reached its knows about value. (Every Units have their own 'knowsAbout' number).		 
		All units in a group have equal knowsAbout value for any given target.
		@param group - Group Object to which knowsabout value is checked.
		@param target - Object on which knowsabout value is checked. 
		@return double value will be returned. If function returns correct value from a valid unit 
		then the value would be in a range 0 to 4. Otherwise value will be -1.
		@remarks Knowsabout level must reach the threshold before a unit knows if another unit is an enemy or a friendly unit.
		@remarks This is a replication of double knowsAbout(Group& grp, ControllableObject& target)
		*/
		static double getKnowsAbout(const Group& group, const ControllableObject& target);

		/*!
		Silently Joins the unit to different Group (without radio message).
		@param group - Group Object to where unit is switched.
		@param u - Unit object which is going to join into group.
		@return Nothing
		@remarks This is a replication of void switchGroup(Group& group, Unit& u)
		*/
		static void applyGroupSwitch(Group& group, Unit& u);

		/*!
		Controls behavior of group members when changing formations. By default it should be in enabled mode.
		@param group - Group Object to which function is applied.
		@param enable - Boolean value if it is true, then group members might be re-ordered when assuming a 
		new formation (i.e. each unit will move to the position in the new formation that is closest to them). 
		If false, then groups will not be re-ordered.
		@return Nothing
		@remarks This is a replication of void EnableSmartOrder(Group& grp, bool enable)
		*/
		static void applySmartOrderEnable(Group& group, bool enable);

		/*!
		Moves the Group to the destinate position that we specify.
		@param group - Group Object to which function is applied.
		@param pos - position where the group needs to move
		@return Nothing
		@remarks This is a replication of void move(Group& grp, position3D pos)
		*/
		static void applyMoveDestination(Group& group, const position3D& pos);

		/*!
		Check whether the group is a local object or not. Local object means the object which is available only in 
		the VBS2 instance it is created.
		@param group - Group Object to which the check to be made.
		@return Boolean value is returned from the function. If group object is local, function will return 
		true otherwise false will be returned.
		@remarks This is determined by checking the first two parts of NetworkID. For local group both should be zero.
		*/
		static bool isLocal(const Group& group);

		/*!
		Return whether this group is available in VBS environment. groupName should be the correct VBS group name.
		@param groupName - Group Object to which the check to be made.
		@return Boolean value is returned from the function. If the group is available, function will return 
		true otherwise false will be returned.
		@remarks This is a replication of bool groupExists(string groupName)
		*/
		static bool isAvailable(const std::string& groupName);

		/*!
		@description 
		
		Used to check if a group uses new convoy capabilities within the VBS2 environment.
		
		@locality

		Globally Applied

		@version  [VBS2Fusion v3.11]

		@param group - The group object that is checked for convoy capabilities.
		
		@return bool - If true the group uses new convoy capabilities and otherwise.

		@example

		@code

		//The group to be created
		Group gr1

		displayString+="The value is"+conversions::BoolToString(GroupUtilities::isGroupNewConvoy(gr1));

		@endcode

		@overloaded

		None.

		@related
		
		@remarks To check the convoy capabilities of a group created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function GroupUtilities::isGroupNewConvoy(Group& group).
		*/
		static bool isGroupNewConvoy(const Group& group);

		/*!
		@description 
		Used to select a unit from the player's group (in the UI command bar : similar to pressing the function keys, to select subordinates).
		This function only works if the player is a group leader.
		
		@locality

		Globally Applied, Locally Effected

		@version  [VBS2Fusion v3.11]

		@param member - A member of the group, from  which a unit is selected ( it doesn't have to be the group leader).
		@param unitToSelect - Unit to be selected/unselected.
		@param select - If true, the unit will be selected and otherwise.

		@return Nothing.

		@example

		@code

		//The units to be created
		Unit u1,u2;

		GroupUtilities::applySelectUnit(u1,u2,true);

		@endcode

		@overloaded 

		@related

		@remarks To select a unit from the player group which was initially created in a client machine within  a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function  GroupUtilities::applySelectUnit(Unit& member, Unit& unitToSelect, bool select) 	
	
		*/
		static void applySelectUnit(const Unit& member, Unit& unitToSelect, bool select);
	};
}
#endif //GROUP_UTILITIES_H