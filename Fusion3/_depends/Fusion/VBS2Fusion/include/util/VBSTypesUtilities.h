/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name: VBSTypesUtilities.h

Purpose:

This file contains the declaration of the vbs2Types class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			19-07-2013	MNG: Created in order to deprecate VBS2TypesUtilities.h
/************************************************************************/

#ifndef VBSTYPES_UTILITIES_H
#define VBSTYPES_UTILITIES_H

#include <iostream>
#include <fstream>
#include <list>

#include "VBSFusion.h"
#include "ExecutionUtilities.h"
#include "conversions.h"

namespace VBSFusion
{
	class VBSFUSION_API VBSTypesUtilities
	{
	public: 

		/*!
		Return all the soldier type objects in VBS engine as
		a vector of strings
		*/
		static std::vector<std::string> getSoldierTypes();

		/*!
		Return all the Airplane type objects in VBS engine as
		a vector of strings
		*/
		static std::vector<std::string> getAirplaneTypes();

		/*!
		Return all the Tank type objects in VBS engine as
		a vector of strings
		*/
		static std::vector<std::string> getTankTypes();

		/*!
		Return all the Car type objects in VBS engine as
		a vector of strings
		*/
		static std::vector<std::string> getCarTypes();

		/*!
		Return all the Helicopter type objects in VBS engine as
		a vector of strings
		*/
		static std::vector<std::string> getHelicopterTypes();

		/*!
		Return all the Vasi type objects in VBS engine as
		a vector of strings
		*/
		static std::vector<std::string> getVasiTypes();

		/*!
		Return all the All type objects in VBS engine as
		a vector of strings
		*/
		static std::vector<std::string> getAllTypes();

		static std::vector<std::string> getGrenadeTypes();

		/*!
		This method implement the logic to get different types of objects in VBS engine. object type passed
		as a parameter to method. type can be one of following. [SOLDIER, AIRPLANE, TANK, CAR, Helicopter, 
		VASI, ALL] etc.	Result returns as a vector of strings
		*/
		static std::vector<std::string> getTypesEx(const std::vector<std::string>& types);

		/*!
		Return all the ship type objects in VBS engine as a vector of strings
		*/
		static std::vector<std::string> getShipTypes();

		/*!
		Count how many units in the array are of given type. 
		type - This can be a specific class or parent class		
		*/
		static int countType(const std::string& type, const std::list<ControllableObject>& ControllableObjectList);

	private:
		/*!
		This method implement the logic to get different types
		of objects in VBS engine. object type passed as a parameter
		to method. type can be one of following.
		[SOLDIER, AIRPLANE, TANK, CAR, Helicopter, VASI, ALL] 
		result returns as a vector of strings

		*/
		static std::vector<std::string> getTypeLogic(const std::string& type);

	};
};

#endif //VBSTYPES_UTILITIES_H