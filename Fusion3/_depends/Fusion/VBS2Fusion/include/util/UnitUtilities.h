
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/
/*************************************************************************

Name:

	UnitUtilities.h

Purpose:

	This file contains the declaration of the UnitUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			26-03/2009	RMR: Original Implementation
	1.0			26-09/2009	SDS: Update create Unit to support ENUM structure to define unit type

	2.0			10-02/2010  YFP: Added  void CreateUnit(Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group)
										void setUnitBehaviour(Unit& unit, BEHAVIOUR behavior)
										void commandStop(Unit& u)
										void globalChat(Unit& u,string txtMessage)
										void reloadWeapons(Unit& u)	
	2.01		10-02/2010	MHA: Comments Checked
	2.02		25-02/2010	YFP: Added void applyAlias(Unit& u)
	2.03		24/05/2010  YFP: Added Methods, 
										void performSalute(Unit&, Unit&);
										void setHeight(Unit&, double);
										double getHeight(Unit&);
										void setBMI(Unit&, double);
										double getBMI(Unit&);
	2.04		10-01/2011  CGS: Added CreateUnit(Unit& u,Group& _group)
								 Modified applyEndurance(Unit& u, double endurance)
									doWatch(Unit& unit, position3D targetPosition)
									CreateUnit(Unit& u, position3D position)
									CreateUnit(Unit& u,string strAlias)
									CreateUnit(Unit& u)
									doFire(Unit& firingUnit, position3D pos)
									setUnitBehaviour(Unit& unit, BEHAVIOUR behaviour)
									doWatch(Unit& unit, position3D targetPosition)
									applyExperience(Unit& u, double experience)

	2.05		11-03/2011 YFP: Added Methods
									disableGunnerInput(Unit&,GUNNERINPUT_MODE)
									void applyHeadDirection(Unit&,double,double,double, bool);
	2.06		11-03/2011 SSD: Added Method isPlayerControlled(Unit);
	2.07		07-09/2011 NDB: Added Methods
									double getAerobicFatigue(Unit& unit)
									double getAnaerobicFatigue(Unit& unit)
									double getMoraleLevel(Unit& unit)
									double getMoraleLongTerm(Unit& unit)
									double getTraining(Unit& unit)
									int getUpDegree(Unit& unit)
									void EnablePersonalItems(Unit& unit, bool enable)
									bool isLookingAt(Unit& unit, ControllableObject& co, double angle)
									bool isSpeakable(Unit& unit)
									void allowSpeech(Unit& unit, bool speech)
									void applyAmputation(Unit& unit, BODYPART bodyPart)
									void applyAmputation(Unit& unit, MAIN_BODYPART bodyPart, BODY_SEGMENT segment)
									string getAmputation(Unit& unit, BODYPART bodyPart)
									void addToMorale(Unit& unit, double moralChange)
									void addToAnaerobicFatigue(Unit& unit, double AnaerobicFatigueChange)
									void addToAerobicFatigue(Unit& unit, double AerobicFatigueChange)
									Vehicle getAssignedVehicle(Unit& unit)
									void CreateUnitLocal(string type, position3D& position,Group& group, string initString = "", double skill = 0.5, string rank = "PRIVATE")
	2.08		19-09/2011	NDB: Added Method
									void StopUnit(Unit& unit)
	2.09		20-09/2011	CGS: Added Method
									void reviveUnit(Unit& unit)
	2.10		26-09/2011	NDB: Added Methods
									bool isWearingNVG(Unit& unit)
									bool isPersonalItemsEnabled(Unit& unit)
									string getWatchMode(Unit& unit)
									void applyAnimationSpeed(Unit& unit, double speed)
									double getAnimationSpeed(Unit& unit)
									ControllableObject getLockedTarget(Unit& unit)
									void applyDrawFrustrum(Unit& unit, int mode, double size, Color_RGBA color)
									void applyDrawFrustrum(Unit& unit, int mode, double size)
									DRAWFRUSTRUM getDrawFrustrum(Unit& unit)
									void applyDrawStationary(Unit& unit, int drawType, double maxSize, double growRate, Color_RGBA color, double offset = 0.1)
									void applyDrawStationary(Unit& unit, int drawType, double maxSize = 2, double growRate = 0.03)
									vector<double> getHeadDirection(Unit& unit)
									double getAzimuthHeadDirection(Unit& unit)
									double getElevationHeadDirection(Unit& unit)
									double getRollHeadDirection(Unit& unit)
									void applyLightMode(Unit& unit, int mode)
									bool isDisableAnimationMove(Unit& unit)
									void disableAnimationMove(Unit& unit, bool disable)
									void applyMinAnimationSpeed(Unit& unit, double percent)
									void applyFaceAnimation(Unit& unit, double blink)
									void applyFace(Unit& unit, FACETYPE face)
									void applyMimic(Unit& unit, MIMIC mimic)
									void ignoreRoads(Unit& unit, bool mode)
									void applyUnitCombatMode(Unit& unit, WAYPOINTCOMBATMODE mode)
									void commandWatch(Unit& unit, ControllableObject& target)
									void commandWatch(Unit& unit, position3D& target)
									void doFollow(ControllableObject& co, Group& grp)
									void moveToPos(Unit& unit, position3D& position)
									void glanceAt(Unit& unit, position3D& position)
									void LookAt(Unit& unit, position3D& position)
									void addWeaponToCargo(Unit& unit, string weaponName, int count)
									vector<WEAPONCARGO> getWeaponFromCargo(Unit& unit)
									void addMagazineToCargo(Unit& unit, string magName, int count)
									vector<MAGAZINECARGO> getMagazineFromCargo(Unit& unit)
									vector<string> getMuzzles(Unit& unit)
									position3D getAimingPosition(Unit& unit, ControllableObject& target)
	2.11		26-09/2011	NDB:	Added Method void commandMove(Unit& unit, position3D position)
	2.12		28-09/2011	NDB:  Added Methods,
									void addRating(Unit& unit, double rating)
									double getRating(Unit& unit)
									void addScore(Unit& unit, int score)
									int getScore(Unit& unit)
									void addLiveStats(Unit& unit, double score)
									bool canStand(Unit& unit)
									double getHandsHit(Unit& unit)
									bool isFleeing(Unit& unit)
									void allowFleeing(Unit& unit, double cowardice)
									bool hasSomeAmmo(Unit& unit)
									string getUnitBehaviour(Unit& unit)
									string getUnitCombatMode(Unit& unit)
	2.13		04-11/2011	NDB: Added Methods,
									bool isKindOf(Unit& unit, string typeName)
									void applyCaptive(Unit& unit, bool captive)
									void applyCaptive(Unit& unit, int captiveNum)
									int getCaptiveNumber(Unit& unit)
									float knowsAbout(Unit& unit, ControllableObject& target)
									void sideChat(Unit& unit, string chatText)
									void groupChat(Unit& unit, string chatText)
	2.13		07-11-2011	SSD		void applyName(Unit& unit)
	2.14		21-11-2011	DMB		void stop(Unit& unit, bool stop)
	2.15		19-01-2012  RDJ		void allowGetIn(vector<Unit> unitList, bool val);
	2.16		24-01-2012	RDJ		void unAssignVehicle(Unit& unit)
	2.17		24-01-2012	RDJ		void hideBody(Unit& unit)
	3.35		06-02-2014	GCR		void applyScoreIncrease(Unit& unit, int score)


/************************************************************************/

#ifndef VBS2FUSION_UNIT_UTILITIES_H
#define VBS2FUSION_UNIT_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <time.h>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBSFusion.h"
#include "data/Unit.h"
#include "util/ExecutionUtilities.h"
#include "util/ControllableObjectUtilities.h"
#include "VBS2FusionAppContext.h"
#include "data/VBSTypes.h"
#include "data/UnitArmedModule.h"
#include "data/Target.h"


/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBSFusion
{	

	class VBSFUSION_API UnitUtilities : public ControllableObjectUtilities
	{
	public:

		//********************************************************
		// DisableAI utilities
		//********************************************************

		/*!
		Disables "MOVE" AI for the unit
		deprecated. Use void applyAIMovementDisable(Unit& u).
		*/
		VBSFUSION_DPR(UUNT001) static void disableAIMOVE(Unit& u);
		
		/*!
		Disables "PATHPLAN" AI for the unit
		deprecated. Use void applyAIPathPlanningDisable(Unit& u).
		*/
		VBSFUSION_DPR(UUNT002) static void disableAIPATHPLAN(Unit& u);

		/*!
		Enables "MOVE" AI for the unit
		deprecated. Use void applyAIMovementEnable(Unit& u).
		*/
		VBSFUSION_DPR(UUNT003) static void enableAIMOVE(Unit& u);
		
		/*!
		Enables "PATHPLAN" AI for the unit
		deprecated. Use void applyAIPathPlanningEnable(Unit& u).
		*/
		VBSFUSION_DPR(UUNT004) static void enableAIPATHPLAN(Unit& u);

		//********************************************************
		// Unit weapon utilities
		//********************************************************
		/*!
		Reload weapons of the unit. Reload happens when the current magazine of the weapon is empty.
		deprecated. Use void applyWeaponReload(Unit& u).
		*/
		VBSFUSION_DPR(UUNT005) static void reloadWeapons(Unit& u);


		//********************************************************
		// chat utilities
		//********************************************************

		/*!
		Send the text message over the global radio channel.
		deprecated. Use void applyGlobalChat(Unit& u , string txtMessage).
		*/
		VBSFUSION_DPR(UUNT006) static void globalChat(Unit& u,std::string txtMessage);


		//********************************************************
		// Firing utilities
		//********************************************************

		/*!
		Orders a unit to commence firing on the given target (silently). 
		If the target is objNull, the unit is ordered to commence firing on its current target (using doTarget).
		Deprecated. Use void applyFire(Unit& firingUnit, ControllableObject& target).
		*/
		VBSFUSION_DPR(UUNT031) static void doFire(Unit& firingUnit, ControllableObject& target);

		/*!
		Orders the unit (silently) to fire at the given position. Please note that the 
		position needs to be given in PositionASL format. 
		Deprecated. Use void applyFire(Unit& firingUnit, position3D pos).
		*/
		VBSFUSION_DPR(UUNT032) static void doFire(Unit& firingUnit, position3D pos);


		//********************************************************
		// unit behavior utilities
		//********************************************************

		/*!
		Set the behavior of the unit. 
			  CARELESS,SAFE,AWARE,COMBAT,STEALTH
		Deprecated. Use void applyBehaviour(Unit& unit, BEHAVIOUR behaviour)
		*/
		VBSFUSION_DPR(UUNT007) static void setUnitBehaviour(Unit& unit, BEHAVIOUR behaviour);

		//********************************************************
		// Rank utilities
		//********************************************************

		/*!
		
		@description
		
		Used to obtain the rank of a unit within the VBS2 Environment. Any VBS2 unit�s rank can be obtained using this command. Some rank examples are as follows: PRIVATE, CORPORAL, SERGENT, LIEUTENANT, CAPTAIN, MAJOR, COLONEL, etc.

		@locality
		
		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]
		
		@param u � The unit for which the rank is obtained. The unit should be created before passing it as a parameter. 
		
		@return String
		
		@example
		
		@code
		
		//The unit to be created
		Unit u;

		displayString+="The rank of u is "+UnitUtilities::getRank(u);

		@endcode
		
		@overloaded

		None

		@related
		
		UnitUtilities::updateRank(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks To obtain the rank of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getRank(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::string getRank(const Unit& u);

		/*!
		 
		@description

		Used to update the rank of a unit created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the rank is updated in the fusion end. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::updateRank(u);
		displayString+="The rank of u in the fusion end is "+u.getRankString();

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::getRank((Unit& u)

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To update the rank of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::updateRank(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		
		*/
		static void updateRank(Unit& u);

		/*!
		 
		@description

		Used to apply the rank specified by the functions setRank(string strRank)or setRank(VBSRANK rank)to a unit created within the VBS2 Environment.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the rank is applied. The unit should be created before passing it as a parameter.
		
		@return Nothing
		
		@example

		@code

		//The unit to be created
		Unit u;
		
		u.setRank("COLONEL");
		UnitUtilities::applyRank(u);

		@endcode

		@overloaded

		UnitUtilities:: applyRank(Unit& u, string rank);

		@related

		UnitUtilities::getRank(Unit& u);
		
		UnitUtilities::updateRank(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To apply a rank to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::applyRank (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static void applyRank(Unit& u);

		/*!
		
		@description

		Used to apply a rank to a unit created within the VBS2 Environment.  

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the rank is applied. The unit should be created before passing it as a parameter. 
		
		@param rank- The rank applied to the object.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyRank(u,"COLONEL");

		@endcode

		@overloaded

		UnitUtilities:: applyRank(Unit& u); 

		@related

		UnitUtilities::getRank(Unit& u);
		
		UnitUtilities::updateRank(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To apply a rank to  a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::applyRank (Unit& u,string rank).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyRank(Unit& u, const std::string& rank);


		//********************************************************
		// Skill utilities
		//********************************************************

		/*!
		
		@description

		Used to obtain the skill level of a unit within the VBS2 Environment. Any VBS2 unit�s skill can be obtained using this command. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the skill level is obtained. The unit should be created before passing it as a parameter.

		@return Double

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The skill of u is "+conversions::DoubleToString(UnitUtilities::getSkill(u));

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: updateSkill(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the skill level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getSkill(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getSkill(const Unit& u);

		/*!
		
		@description

		Used to update the skill level of a unit created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the skill level is updated in the fusion end. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::updateSkill(u);
		displayString+="The skill of u in the fusion end is "+conversions::DoubleToString(u.getSkill());

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::getSkill((Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To update the skill level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::updateSkill(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateSkill(Unit& u);

		/*!
		
		@description

		Used to apply the skill level specified by the functions setSkill(double skill) to a unit created within the VBS2 Environment. 

		@locality

		Locally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the skill level is applied. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		u.setSkill(0.4562);
		UnitUtilities::applySkill(u);

		@endcode

		@overloaded

		UnitUtilities::applySkill(Unit& u, double skill);

		@related

		UnitUtilities:: getSkill(Unit& u); 
		
		UnitUtilities:: updateSkill(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).  

		*/
		static void applySkill(Unit& u);

		/*!
		
		@description

		Used to apply a skill level to a unit created within the VBS2 Environment. 

		@locality

		Locally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the skill level is applied. The unit should be created before passing it as a parameter. 
		
		@param skill � The skill level applied to the object.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applySkill(u,0.6292)

		@endcode

		@overloaded

		UnitUtilities::applySkill(Unit& u);

		@related

		UnitUtilities::getSkill(Unit& u); 
		
		UnitUtilities::updateSkill(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		*/
		static void applySkill(Unit& u, double skill);


		//********************************************************
		// Group utilities
		//********************************************************

		/*!
		
		@description

		Used to obtain the group of a unit within the VBS2 Environment. Any VBS2 unit�s group can be obtained using this command.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the group is obtained. The unit should be created before passing it as a parameter.

		@return String

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+=" The group of u is"+UnitUtilities::getGroup(u);

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: updateGroup(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the group of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getGroup(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static std::string getGroup(const Unit& u);

		/*!
		 
		@description

		Used to update the group of a unit created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the group is updated in the fusion end. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::updateGroup(u);
		displayString+="The group of u in the fusion end is"+u.getGroup();

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: getGroup(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To update the group of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::updateGroup(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static void updateGroup(Unit& u);		


		//********************************************************
		// Endurance utilities
		//********************************************************

		/*!
		
		@description

		Used to obtain the endurance rating of a unit within the VBS2 Environment. Any VBS2 unit�s endurance can be obtained using this command.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the endurance rating is obtained. The unit should be created before passing it as a parameter. 

		@return Double

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The endurance of u is"+conversions::DoubleToString(UnitUtilities::getEndurance(u));

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::updateEndurance(Unit& u);

		UnitUtilities::applyEndurance(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the endurance rating of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getEndurance (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static double getEndurance(const Unit& u);

		/*!
		
		@description

		Used to update the endurance rating of a unit created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the endurance rating is updated in the fusion end. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::updateEndurance(u);
		
		displayString+="The endurance of u in the fusion end is"+conversions::DoubleToString(u.getEndurance());

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: getEndurance(Unit& u);

		UnitUtilities:: applyEndurance(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To update the endurance rating of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: updateEndurance(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateEndurance(Unit& u);

		/*!
		
		@description

		Used to apply the endurance specified by the functions setEndurance(double endurance) to a unit created within the VBS2 Environment.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the endurance rating is applied. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		u.setEndurance(0.4562);
		
		UnitUtilities::applyEndurance(u);

		@endcode

		@overloaded

		UnitUtilities::applyEndurance(Unit& u, double endurance);

		@related

		UnitUtilities::getEndurance(Unit& u);

		UnitUtilities::updateEndurance(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To apply the endurance rating to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyEndurance (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyEndurance(Unit& u);

		/*!
		
		@description

		Used to apply the endurance rating to a unit created within the VBS2 Environment. 

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the endurance rating is applied. The unit should be created before passing it as a parameter. 
		
		@param endurance � The endurance rating applied to the object.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyEndurance (u,0.6292);

		@endcode

		@overloaded

		UnitUtilities:: applyEndurance (Unit& u);

		@related

		UnitUtilities::getEndurance(Unit& u);

		UnitUtilities:: updateEndurance(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To apply an endurance rating to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::applyEndurance (Unit& u, double endurance)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static void applyEndurance(Unit& u, double endurance);

		//********************************************************
		// Experience utilities
		//********************************************************

		/*!
		
		@description

		Used to obtain the experience level of a unit within the VBS2 Environment. Any VBS2 unit�s experience can be obtained using this command.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param  u � The unit for which the experience level is obtained. The unit should be created before passing it as a parameter.  

		@return Double

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The experience of u is"+conversions::DoubleToString(UnitUtilities::getExperience(u));

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::updateExperience(Unit& u);

		UnitUtilities::applyExperience(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the experience level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getExperience (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information);

		*/
		static double getExperience(const Unit& u);

		/*!
		
		@description

		Used to update the experience level of a unit created within the VBS2 Environment in the fusion end. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the experience level is updated in the fusion end. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::updateExperience(u);
		
		displayString+="The experience of u in the fusion end is"+conversions::DoubleToString(u.getExperience());

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: getExperience(Unit& u);

		UnitUtilities:: applyExperience (Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To update the experience level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: updateExperience(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateExperience(Unit& u);

		/*!
		
		@description

		Used to apply the experience specified by the functions setExperience(double experience) to a unit created within the VBS2 Environment. 

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the experience level is applied. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		u.setExperience(0.4562);

		UnitUtilities::applyExperience(u);

		@endcode

		@overloaded

		UnitUtilities:: applyExperience(Unit& u, double experience);

		@related

		UnitUtilities::getExperience(Unit& u);

		UnitUtilities::updateExperience(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply the experience level to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::applyExperience (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyExperience(Unit& u);

		/*!
		
		@description

		Used to apply an experience level to a unit created within the VBS2 Environment. 

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the experience level is applied. The unit should be created before passing it as a parameter.
		
		@param experience � The experience level applied to the object.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyExperience(u,0.6292)

		@endcode

		@overloaded

		UnitUtilities:: applyExperience (Unit& u)

		@related

		UnitUtilities:: getExperience(Unit& u);

		UnitUtilities:: updateExperience (Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply an experience level to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::applyExperience (Unit& u, double experience)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyExperience(Unit& u, double experience);


		//********************************************************
		// Fatigue utilities
		//********************************************************

		/*!
		
		@description

		Used to obtain the fatigue level of a unit within the VBS2 Environment. Any VBS2 unit�s fatigue level can be obtained using this command.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the fatigue level is obtained. The unit should be created before passing it as a parameter.  

		@return Double

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The fatigue level of u is"+conversions::DoubleToString(UnitUtilities::getFatigue(u));

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::updateFatigue(Unit& u);

		UnitUtilities::applyFatigue(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the fatigue level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getFatigue (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getFatigue(const Unit& u);

		/*!
		
		@description

		Used to update the fatigue level of a unit created within the VBS2 Environment in the fusion end.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the fatigue level is updated in the fusion end. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities:: updateFatigue(u);
		
		displayString+="The fatigue level of u in the fusion end is"+conversions::DoubleToString(u.getFatigue());

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: getFatigue(Unit& u);

		UnitUtilities:: applyFatigue(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the fatigue level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: updateFatigue (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void updateFatigue(Unit& u);

		/*!
		
		@description

		Used to apply the fatigue level specified by the functions setFatigue (double fatigue) to a unit created within the VBS2 Environment.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the fatigue level is applied. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		u. setFatigue(0.4562);

		UnitUtilities::applyFatigue(u);

		@endcode

		@overloaded

		UnitUtilities:: applyFatigue(Unit& u, double fatigue);

		@related

		UnitUtilities:: getFatigue(Unit& u);

		UnitUtilities:: updateFatigue (Unit& u);

		@remarks This function only has a temporary effect as the unit recovers within few seconds after performing an action (i.e. the unit would not be tired) 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply the fatigue level to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::applyFatigue (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		*/
		static void applyFatigue(Unit& u);

		/*!
		
		@description

		Used to apply a fatigue level to a unit created within the VBS2 Environment. 

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param  u � The unit for which the fatigue level is applied. The unit should be created before passing it as a parameter. 
		
		@param fatigue � The fatigue level applied to the object

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyFatigue(u,0.6292) 

		@endcode

		@overloaded

		UnitUtilities:: applyFatigue(Unit& u);

		@related

		UnitUtilities:: getFatigue(Unit& u);

		UnitUtilities:: updateFatigue(Unit& u);

		@remarks This function only has a temporary effect as the unit recovers within few seconds after performing an action (i.e. the unit would not be tired). 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks To apply a fatigue level to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyFatigue(Unit& u, double fatigue)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static void applyFatigue(Unit& u, double fatigue);



		//********************************************************
		// Leadership utilities
		//********************************************************

		/*!
		
		@description

		Used to obtain the leadership level of a unit within the VBS2 Environment. Any VBS2 unit�s leadership level can be obtained using this command. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the leadership level is obtained. The unit should be created before passing it as a parameter. 

		@return Double

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The leadership level of u is"+conversions::DoubleToString(UnitUtilities::getLeadership(u));

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::updateLeadership(Unit& u);

		UnitUtilities::applyLeadership (Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the leadership level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getLeadership (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getLeadership(const Unit& u);

		/*!
		
		@description

		Used to update the leadership level of a unit created within the VBS2 Environment in the fusion end. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the leadership level is updated in the fusion end. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities:: updateLeadership (u);
		
		displayString+="The leadership level of u in the fusion end is"+conversions::DoubleToString(u.getLeadership ());

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::getLeadership(Unit& u);

		UnitUtilities::applyLeadership(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the leadership level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::updateLeadership (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void updateLeadership(Unit& u);

		/*!
		
		@description

		Used to apply the leadership level specified by the functions setLeadership(double leadership) to a unit created within the VBS2 Environment. 

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param  u � The unit for which the leadership level is applied. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		u. setLeadership(0.4562);
		
		UnitUtilities:: applyLeadership(u);

		@endcode

		@overloaded

		UnitUtilities:: applyLeadership(Unit& u, double leadership);

		@related

		UnitUtilities:: getLeadership (Unit& u);

		UnitUtilities:: updateLeadership (Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply the leadership level to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyLeadership(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).  
		
		*/
		static void applyLeadership(Unit& u);

		/*!
		
		@description

		Used to apply a leadership level to a unit created within the VBS2 Environment.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the leadership level is applied. The unit should be created before passing it as a parameter. 

		@param leadership � The leadership level applied to the object.
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities:: applyLeadership(u,0.6292) 

		@endcode

		@overloaded

		UnitUtilities:: applyLeadership(Unit& u);

		@related

		UnitUtilities:: getLeadership (Unit& u);

		UnitUtilities:: updateLeadership (Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply a leadership level to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyLeadership(Unit& u, double leadership)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyLeadership(Unit& u, double leadership);

		//********************************************************
		// Create Unit
		//********************************************************
		/*!

		@description

		Used to create a unit within the VBS2 Environment. The type, position, initialization statements, skill, rank and other essential details of the object must be defined before calling the function, or else the default parameters would be used for the creation of the unit.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit to be created. The unit should be loaded with relevant parameters before using with the createUnit command.u � The unit to be created. The unit should be loaded with relevant parameters before using with the createUnit command. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		//Set parameters before calling the function
		u.setType(string("VBS2_us_mc_ATsoldier_D_SMAW"));

		u.setPosition(player.getPosition()+position3D(1,1,0))
		
		//Create the unit
		UnitUtilities::CreateUnit(u);
		
		//Update common properties of unit from VBS2
		UnitUtilities::updateStaticProperties(co);

		UnitUtilities::updateDynamicProperties(co);

		@endcode

		@overloaded

		UnitUtilities:: CreateUnit(Unit& u, position3D position);

		UnitUtilities:: CreateUnit(Unit& u, string strAlias);

		UnitUtilities:: CreateUnit(Unit& u, position3D position, string strAlias);

		UnitUtilities:: CreateUnit(Unit& u, string strAlias, string type);

		UnitUtilities:: CreateUnit(Unit& u, string strAlias, string type, position3D position, string initStatements, double skill, string rank, string group);

		UnitUtilities:: CreateUnit(	Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group);
		
		UnitUtilities::CreateUnit(Unit& u,Group& _group);

		@related

		UnitUtilities::deleteUnit(Unit& u);

		@remarks Units loaded with invalid or no positional information are created at map location [0,0,0]. 

		*/
		static void CreateUnit(Unit& u);

		/*!

		@description

		Used to create a unit within the VBS2 Environment. 

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit to be created. 

		@param position � The position where the unit should be created.
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		//Create the unit
		UnitUtilities::CreateUnit(u, player.getPosition()+position3D(1,1,0));

		//Update common properties of the unit from VBS2
		UnitUtilities::updateDynamicProperties(u);

		UnitUtilities::updateStaticProperties(u);

		@endcode

		@overloaded

		UnitUtilities::CreateUnit(Unit& u);

		UnitUtilities::CreateUnit (Unit& u, string strAlias);

		UnitUtilities::CreateUnit (Unit& u, position3D position, string strAlias);

		UnitUtilities::CreateUnit (Unit& u, string strAlias, string type);

		UnitUtilities:: CreateUnit(Unit& u, string strAlias, string type, position3D position, string initStatements, double skill, string rank, string group);

		UnitUtilities::CreateUnit (Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group);
		
		UnitUtilities::CreateUnit(Unit& u,Group& _group);

		@related

		UnitUtilities::deleteUnit(Unit& u);

		@remarks Objects loaded with invalid or no positional information are created at map location [0,0,0]. 

		*/

		static void CreateUnit(Unit& u, const position3D& position);

		/*!
		
		@description

		 Used to create a unit within the VBS2 Environment. 

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit to be created. 

		@param strAlias � The name/alias set to the unit.
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		//Create the unit and set the alias as alias
		UnitUtilities::CreateUnit(u,�alias�);

		//Update common properties of the unit from VBS2
		UnitUtilities::updateDynamicProperties(u);

		UnitUtilities::updateStaticProperties(u);

		@endcode

		@overloaded

		UnitUtilities::CreateUnit(Unit& u);

		UnitUtilities::CreateUnit(Unit& u, position3D position);

		UnitUtilities::CreateUnit (Unit& u, position3D position, string strAlias);

		UnitUtilities::CreateUnit (Unit& u, string strAlias, string type);

		UnitUtilities:: CreateUnit(Unit& u, string strAlias, string type, position3D position, string initStatements, double skill, string rank, string group);

		UnitUtilities::CreateUnit (Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group);
		
		UnitUtilities::CreateUnit(Unit& u,Group& _group);

		@related

		UnitUtilities::deleteUnit(Unit& u);

		@remarks Objects loaded with invalid or no positional information are created at map location [0,0,0]. 

		*/
		static void CreateUnit(Unit& u, const std::string& strAlias);

		/*!

		@description

		Used to create a unit within the VBS2 Environment. 

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit to be created.
		
		@param position - The position where the unit should be created.
		
		@param strAlias � The name/alias set to the unit.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		//Create the unit and set the alias as alias
		UnitUtilities::CreateUnit(u, player.getPosition()+position3D(1,1,0), �alias�);

		//Update common properties of the unit from VBS2
		UnitUtilities::updateDynamicProperties(u);

		UnitUtilities::updateStaticProperties(u);

		@endcode

		@overloaded

		UnitUtilities::CreateUnit(Unit& u);

		UnitUtilities::CreateUnit(Unit& u, position3D position);

		UnitUtilities::CreateUnit (Unit& u, string strAlias);

		UnitUtilities::CreateUnit (Unit& u, string strAlias, string type);

		UnitUtilities:: CreateUnit(Unit& u, string strAlias, string type, position3D position, string initStatements, double skill, string rank, string group);

		UnitUtilities::CreateUnit (Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group);

		UnitUtilities::CreateUnit(Unit& u,Group& _group);

		@related

		UnitUtilities::deleteUnit(Unit& u);

		@remarks Objects loaded with invalid or no positional information are created at map location [0,0,0]. 

		*/

		static void CreateUnit(Unit& u, const position3D& position, const std::string& strAlias);

		/*!
		
		@description

		Used to create a unit within the VBS2 Environment.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit to be created.  

		@param strAlias � The name/alias set to the unit

		@param type � The type of the unit to be created
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		//Create the unit,set the alias as alias and set the type
		UnitUtilities::CreateUnit(u,"alias","VBS2_us_mc_ATsoldier_D_SMAW" );

		//Update common properties of the unit from VBS2
		UnitUtilities::updateDynamicProperties(u);

		UnitUtilities::updateStaticProperties(u);

		@endcode

		@overloaded

		UnitUtilities::CreateUnit(Unit& u);

		UnitUtilities::CreateUnit(Unit& u, position3D position);

		UnitUtilities::CreateUnit (Unit& u, string strAlias);

		UnitUtilities::CreateUnit(Unit& u, position3D position, string strAlias);

		UnitUtilities:: CreateUnit(Unit& u, string strAlias, string type, position3D position, string initStatements, double skill, string rank, string group);
		
		UnitUtilities::CreateUnit (Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group);

		UnitUtilities::CreateUnit(Unit& u,Group& _group);

		@related

		UnitUtilities::deleteUnit(Unit& u);

		@remarks Objects loaded with invalid or no positional information are created at map location [0,0,0]. 

		*/
		static void CreateUnit(Unit& u, const std::string& strAlias, const std::string& type);

		/*!

		@description

		 Used to create a unit within the VBS2 Environment.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit to be created. 

		@param strAlias � The name/alias set to the unit.
		
		@param type � The type of the unit to be created.
		
		@param position - The position where the unit should be created.
		
		@param initStatements � Command to be executed upon the creation of the unit.

		@param skill � The skill level of the unit to be created.

		@param rank � The rank level of the unit to be created.
		
		@param group � The group to which the unit would be joining.
		
		@return Nothing

		@example

		@code

		Sample Code: 
		//The unit to be created
		Unit u;
		
		Group g1;
		
		UnitUtilities::CreateUnit(u,string("u1"),"VBS2_us_mc_ATsoldier_D_SMAW",(player.getPosition()+position3D(1,1,0)),"0 setRain 1",0.4562,"COLONEL","g1" );
		
		//Update common properties of the unit from VBS2
		UnitUtilities::updateDynamicProperties(u);
		
		UnitUtilities::updateStaticProperties(u);

		@endcode

		@overloaded

		UnitUtilities::CreateUnit(Unit& u);

		UnitUtilities::CreateUnit(Unit& u, position3D position);

		UnitUtilities::CreateUnit (Unit& u, string strAlias);

		UnitUtilities::CreateUnit(Unit& u, position3D position, string strAlias);

		UnitUtilities::CreateUnit (Unit& u, string strAlias, string type);

		UnitUtilities::CreateUnit (Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group);

		UnitUtilities::CreateUnit(Unit& u,Group& _group);

		@related

		UnitUtilities::deleteUnit(Unit& u);

		@remarks Objects loaded with invalid or no positional information are created at map location [0,0,0]. 

		*/

		static void CreateUnit(Unit& u, const std::string& strAlias, const std::string& type, const position3D& position, const std::string& initStatements, double skill, const std::string& rank, const std::string& group);

		/*!

		@description

		Used to create a unit within the VBS2 Environment.

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit to be created. 

		@param type � The type of the unit to be created.
		
		@param position - The position where the unit should be created.
		
		@param markers - Name of the marker, of which a random one is used to define the position.
		
		@param placementRadius � Radius around the specified position, within which the unit will be created.
		
		@param specialProperties � The Types of special properties are,
			"NONE" - no special effects.

			"CAN_COLLIDE" - If unit is created close to an existing one, the initial position is not corrected. If used with "asl" position option, then unit can be created above the ground.
		
		@param group � The group to which the unit would be joining.
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		Group g1;

		Marker m1;

		UnitUtilities::CreateUnit(u,"VBS2_us_mc_ATsoldier_D_SMAW",(player.getPosition()+position3D(1,1,0)),"m1",0.4562,"NONE","g1" );

		//Update common properties of the unit from VBS2
		UnitUtilities::updateDynamicProperties(u);

		UnitUtilities::updateStaticProperties(u);

		@endcode

		@overloaded

		UnitUtilities::CreateUnit(Unit& u);

		UnitUtilities::CreateUnit(Unit& u, position3D position);

		UnitUtilities::CreateUnit (Unit& u, string strAlias);

		UnitUtilities::CreateUnit(Unit& u, position3D position, string strAlias);

		UnitUtilities::CreateUnit (Unit& u, string strAlias, string type);

		UnitUtilities::CreateUnit(Unit& u, string strAlias, string type, position3D position, string initStatements, double skill, string rank, string group);

		UnitUtilities::CreateUnit(Unit& u,Group& _group);

		@related

		UnitUtilities::deleteUnit(Unit& u);

		@remarks Objects loaded with invalid or no positional information are created at map location [0,0,0]. 

		*/
		static void CreateUnit(	Unit& u, const std::string& type, const position3D& position, const std::string& markers, double placementRadius, const std::string& specialProperties, const std::string& group);

		/*

		@description

		Used to create a unit within the VBS2 Environment. 

		@locality

		 Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit to be created.
		
		@param group � The group to which the unit would be joining
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		Group grp;

		GroupUtilities::createGroup(grp);

		u.setposition(player.getPosition());

		UnitUtilities::CreateUnit(u,grp);

		//Update common properties of the unit from VBS2
		UnitUtilities::updateDynamicProperties(u);

		UnitUtilities::updateStaticProperties(u);

		@endcode

		@overloaded

		UnitUtilities::CreateUnit(Unit& u);

		UnitUtilities::CreateUnit(Unit& u, position3D position);

		UnitUtilities::CreateUnit (Unit& u, string strAlias);

		UnitUtilities::CreateUnit(Unit& u, position3D position, string strAlias);

		UnitUtilities::CreateUnit (Unit& u, string strAlias, string type);

		UnitUtilities::CreateUnit(Unit& u, string strAlias, string type, position3D position, string initStatements, double skill, string rank, string group);

		UnitUtilities::CreateUnit (Unit& u,string type,position3D position,string markers,double placementRadius,string specialProperties,string group);

		@related

		UnitUtilities::deleteUnit(Unit& u);

		@remarks Objects loaded with invalid or no positional information are created at map location [0,0,0]. 

		*/
		
		static void UnitUtilities::CreateUnit(Unit& u, Group& _group);
 


		//********************************************************
		// Update Utilities
		//********************************************************

		/*!
		@description

		Used to update the properties of a unit created within the VBS2 Environment in the fusion end e.g. endurance, experience, group etc (properties that do not change frequently).

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the properties are updated in the fusion end. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		UnitUtilities::updateStaticProperties(u);

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::updateDynamicProperties(Unit& u);

		@remarks To update the static properties of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: updateStaticProperties (Unit& u)	
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		
		*/
		static void updateStaticProperties(Unit& u);

		/*!
		
		@description
		
		Used to update the properties of a unit created within the VBS2 Environment in the fusion end e.g. fatigue, damage, direction etc. (properties that change frequently).

		@locality
		
		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]
		
		@param u � The unit for which the properties are updated in the fusion end. The unit should be created before passing it as a parameter.
		
		@return Nothing
		
		@example
		
		@code
		
		UnitUtilities::updateDynamicProperties(u);

		@endcode
		
		@overloaded

		None

		@related
		
		UnitUtilities::updateStaticProperties(Unit& u);

		@remarks To update the dynamic properties of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: updateDynamicProperties (Unit& u)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void updateDynamicProperties(Unit& u);

		//********************************************************
		// Group existence check Utility
		//********************************************************		 

		/*!
		Returns true if a group with the name/alias exists. 
		Deprecated. Use bool isGroupExists(string groupName)
		*/
		VBSFUSION_DPR(UUNT008) static bool groupExists(std::string groupName);

		//********************************************************
		// Group creation Utility
		//********************************************************

		/*!
		Creates a new group with the name/alias specified. Default side of "WEST" is used. 
		*/
		static void createGroup(const std::string& groupName, SIDE side = WEST);

		//********************************************************
		// Movement Utility
		//********************************************************

		/*!
		order unit to stop. 
		Deprecated. Use void applyCommandStop(Unit& u)
		*/
		VBSFUSION_DPR(UUNT009) static void commandStop(Unit& u);
		
		/*!
		Orders the unit to move to movePosition. 
		Deprecated. Use void applyMove(Unit& u, position3D movePosition)
		*/
		VBSFUSION_DPR(UUNT010) static void doMove(Unit& u, position3D movePosition);

		//********************************************************
		// forceSpeed Utility
		//********************************************************

		/*!
		Forces the speed of the unit. 
		*/
		//static void forceSpeed(Unit& unit, double forceSpeedVal);			

		//********************************************************
		// Targeting utilities
		//********************************************************

		/*!
		Orders the unit to target the given target (silently).
		Deprecated. Use void applyTarget(Unit& unit, ControllableObject& co)
		*/
		VBSFUSION_DPR(UUNT011) static void doTarget(Unit& unit, ControllableObject& co);

		//********************************************************
		// Watch utilities
		//********************************************************

		/*!
		Orders the unit to watch the given position (format position3D) (silently).
		Deprecated. Use void applyWatch(Unit& u, position3D targetPosition)
		*/
		VBSFUSION_DPR(UUNT012) static void doWatch(Unit& unit, position3D targetPosition);


		//********************************************************
		// UnitPos (i.e. UP, DOWN or AUTO) utilities
		//********************************************************

		/*!
		
		@description

		Used to obtain the current position mode of a unit within the VBS2 Environment in UNITPOS (i.e. AUTO, UP or DOWN). 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the position mode is obtained. The unit should be created before passing it as a parameter. 

		@return UNITPOS

		@example

		@code

		//The unit to be created
		Unit u;
		
		unitpos = UnitUtilities::getUnitPos(u);
		
		switch(unitpos)
		{ 
			case VBS2Fusion::UP: displayString+= "UP"; break; 

			case VBS2Fusion::DOWN: displayString+= "DOWN"; break; 
			
			case VBS2Fusion::AUTO: displayString+= "AUTO"; break; 
			
		}

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::updateUnitPos(Unit& u);

		UnitUtilities::applyUnitPos(Unit& u, UNITPOS pos) ;

		UnitUtilities::applyUnitPos(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the position mode of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getUnitPos(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static UNITPOS getUnitPos(const Unit& u) ;

		/*!
		
		@description

		Used to update the position mode of a unit created within the VBS2 Environment in the fusion end. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the position mode is updated in the fusion end. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities:: updateUnitPos (u);

		//To display the position as a string use the function getUnitPosString
		displayString+="The pos of u in the fusion end is "+u.getUnitPosString();

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::getUnitPos(Unit& u);

		UnitUtilities::applyUnitPos(Unit& u, UNITPOS pos);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the position mode of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::updateUnitPos (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void updateUnitPos(Unit& u);

		/*!
		
		@description

		Used to apply a position mode to a unit created within the VBS2 Environment. 

		@locality

		Locally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the position mode is applied. The unit should be created before passing it as a parameter. 

		@param pos - The position mode applied to the object. There are 3 types of modes �DOWN�, �UP� and �AUTO�.
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyUnitPos(u,UNITPOS::UP);	

		@endcode

		@overloaded

		applyUnitPos(Unit& u);

		@related

		UnitUtilities:: getUnitPos(Unit& u);

		UnitUtilities:: updateUnitPos(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyUnitPos(Unit& u, UNITPOS pos);

		/*!
		
		@description

		Used to apply a position mode specified by the function UP(), DOWN() or AUTO() to a unit created within the VBS2 Environment. 

		@locality

		 Locally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the position mode is applied. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		u.DOWN();
		
		UnitUtilities::applyUnitPos(u);	

		@endcode

		@overloaded

		applyUnitPos(Unit& u, UNITPOS pos);

		@related

		UnitUtilities:: getUnitPos(Unit& u);

		UnitUtilities:: updateUnitPos(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyUnitPos(Unit& u);

		/*!
		Applies the AUTO position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		Deprecated. Use void applyUnitPosAUTO(Unit& u)
		*/
		VBSFUSION_DPR(UUNT013) static void unitPosAUTO(Unit& u);

		/*!
		Applies the DOWN position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		Deprecated. Use void applyUnitPosDOWN(Unit& u)
		*/
		VBSFUSION_DPR(UUNT014) static void unitPosDOWN(Unit& u);

		/*!
		Applies the UP position to the unit within the game. Does not update the 
		UnitPos property of the unit. Use updateUnitPos to reload and check for successful execution. 
		Deprecated. Use void applyUnitPosUP(Unit& u)
		*/
		VBSFUSION_DPR(UUNT015) static void unitPosUP(Unit& u);

		/*!
		
		@description

		Used to apply an alias to a unit created within the VBS2 Environment. 

		@locality

		Globally applied and Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the alias is applied. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		string alias ="u";

		u.setAlias(u);

		UnitUtilities::applyAlias(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply the alias to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::applyAlias (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyAlias(Unit& u);

		/*!
		Enable/Disable precision movement mode for specified unit. 
		Deprecated. Use void applyUnitExactMovementMode(Unit &u, bool precisionMode)
		*/
		VBSFUSION_DPR(UUNT016) static void setUnitExactMovementMode(Unit& u, bool precisionMode);

		/*!
		
		@description

		Used to obtain the precision/exact movement mode of a unit within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the precision/exact movement mode is obtained. The unit should be created before passing it as a parameter. 

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The precision movement mode is"+conversions::BoolToString(UnitUtilities::getUnitExactMovementMode(u));

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::applyUnitExactMovementMode(Unit& u, bool precisionMode);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the exact movement mode of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getUnitExactMovementMode (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool getUnitExactMovementMode(const Unit& u);

		/*!
		
		@description

		Used to delete a unit created within the VBS2 Environment. 

		@locality

		Globally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit deleted. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::deleteUnit(u);	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To delete a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::deleteUnit (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void deleteUnit(Unit& u);

		/*!
		Causes a unit to salute to a specified target unit.
		Deprecated. Use void applySalutePerform(Unit& unit, Unit& targetUnit)
		*/
		VBSFUSION_DPR(UUNT017) static void performSalute(Unit& unit, Unit& targetUnit);

		/*!
		set body height of the unit. height should be in meters.
		Deprecated. Use void applyHeight(Unit& u, double height)
		*/
		VBSFUSION_DPR(UUNT018) static void setHeight(Unit& u , double height);

		/*!
		
		@description

		Used to obtain the height of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the height is obtained. The unit should be created before passing it as a parameter.  

		@return double

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The height of the unit is "+conversions::DoubleToString(UnitUtilities::getHeight(u));	

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::applyHeight(Unit& u , double height);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the height of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getHeight (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getHeight(const Unit& u);

		/*!
		set Body Mass Index (BMI) of the unit. 
		Deprecated. Use void applyBMI(Unit& u, double index)
		*/
		VBSFUSION_DPR(UUNT019) static void setBMI(Unit& u, double index );

		/*!
		
		@description

		Used to obtain the body mass index of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the body mass index is obtained. The unit should be created before passing it as a parameter.  

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The body mass index of the unit is "+conversions::DoubleToString(UnitUtilities::getBMI(u));	

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::applyBMI(Unit& u, double index);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the body mass index of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getBMI(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getBMI(const Unit& u);

		/*!
		Disable gunner input of the unit.
		input Modes are
			NOTHING         - Enable disabled modes.
			WEAPON			- Disable weapon.
			HEAD			- Disable head.
			WEAPONANDHEAD	- Disable weapon and head.
		Deprecated. Use void applyGunnerInputDisable(Unit& unit, GUNNERINPUT_MODE mode)
		*/
		VBSFUSION_DPR(UUNT020) static void disableGunnerInput(Unit& unit,GUNNERINPUT_MODE mode);


		/*!
		
		@description

		Used to set he head direction of a unit within the VBS2 Environment. 

		@locality

		Locally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the head direction is turned . The unit should be created before passing it as a parameter.  

		@param azimuth - The direction in degree in which head is turned. (Positive value turns the unit left and negative value turns the unit right).
		
		@param elevation - The angle in which the head should be pointed up.
		
		@param roll - The angle in which the head should be tilted.( Positive value turns the unit right and negative value turns the unit left).
		
		@param transition - The default value is true.
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyHeadDirection(u,90,5,2,true);	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyHeadDirection(Unit& unit, double azimuth, double elevation,double roll, bool transition = true);

		/*!
		
		@description

		Used to obtain the primary weapon of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the primary weapon is obtained. The unit should be created before passing it as a parameter. 

		@return String

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The primary weapon of the unit is "+(UnitUtilities::getPrimaryWeapon(u));

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::updatePrimaryWeapon(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks If the unit has no primary weapon, an empty string is returned.
		
		@remarks To obtain the primary weapon of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getPrimaryWeapon (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::string getPrimaryWeapon(const Unit& u);

		/*!
		
		@description

		Used to update the primary weapon of a unit created within the VBS2 Environment in the fusion end.   

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the primary weapon is updated in the fusion end. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::updatePrimaryWeapon(u);
		
		displayString+="The primary weapon of the unit in the fusion end is "+(u.getPrimaryWeapon());

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: getPrimaryWeapon (Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To update the primary weapon of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::updatePrimaryWeapon (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void updatePrimaryWeapon(Unit& u);

		/*

		@description

		Used to update the list of weapon types assigned to a unit created within the VBS2 Environment in the fusion end. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the list of weapon types are updated in the fusion end. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::updateWeaponTypes(u);

		UnitUtilities::updateWeaponAmmo(u);	

		for (Unit::weapons_iterator itr = u.weapons_begin();itr != u.weapons_end(); itr++)
			{	
				displayString += "WeaponName: " + itr->getWeaponName() ; 
			
				displayString += "\\nAmmo: " + conversions::IntToString(itr->getAmmoCount());
			}

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: getPrimaryWeapon(Unit& u);

		@remarks It is highly recommended to create objects 

		@remarks To obtain the rank of a unit created in a client 
		
		*/
		static void updateWeaponTypes(Unit& u);

		/*!
		Select the Weapon given by the muzzle Name.
		Deprecated. Use void applyWeaponSelect(Unit& u, string muzzleName)
		*/
		VBSFUSION_DPR(UUNT021) static void selectWeapon(Unit& u, std::string muzzleName);

		/*
		
		@description

		Updates the amount of ammo left in the primary magazine of each assigned weapon type. It uses the list defined by the weapons list within the UnitArmedModule, and thereby only the ammo information for each weapon on that list would be loaded.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the amount of ammo is updated in the fusion end. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::updateWeaponTypes(u);
		
		UnitUtilities::updateWeaponAmmo(u);	
		
		for (Unit::weapons_iterator itr = u.weapons_begin();itr != u.weapons_end(); itr++)
			{ 
				displayString += "WeaponName: " + itr->getWeaponName() ; 
				displayString += "\\nAmmo: " + conversions::IntToString(itr->getAmmoCount()); 
			}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function should be used after the function UnitUtilities::updateWeaponTypes (Unit& u)is called.

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks To update the amount of ammo left in the primary magazine of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::updateWeaponAmmo ((Unit& u)).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateWeaponAmmo(Unit& u);

		/*!
		Add magazine to the Unit.
		Deprecated. Use void applyMagazineAdd(Unit& u,string name)
		*/
		VBSFUSION_DPR(UUNT022) static void addMagazine(Unit& u, std::string name);

		/*!
		Count number of magazines available in given unit object.
		Deprecated. Use int getMagazinesCount(Unit& u)
		*/
		VBSFUSION_DPR(UUNT023) static int countMagazines(Unit& u);

		/*!

		@description

		Updates the list of magazines assigned to the unit within the VBS2 environment in the Armed Module. This function loads all magazines assigned to the unit from VBS2 and adds a new magazine object to the magazines list.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the list of magazines is updated in the fusion end. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		// The unit to be created
		Unit u; 

		UnitUtilities::updateMagazineTypes(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function does not update the ammo amount within the magazine list. An empty list is created for each newly created entry on the list. Use the function UnitUtilities::updateMagazineAmmo to update this list. 

		@remarks To update the list of magazines assigned to the unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: updateMagazineTypes(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void updateMagazineTypes(Unit& u);

		/*!
		
		@description

		Used to update the amount of ammo left in each magazine assigned to the object within the VBS2 environment and thereby updates the ammo amount list. The function uses the magazine types defined in the existing magazine list to search for available ammo, and therefore will not load ammo information for any magazines which are not on this list.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the amount of ammo is updated in the fusion end. The object should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::updateMagazineAmmo(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function should be used after the function updateMagazineTypes is called. 

		@remarks To update the amount of ammo left in each magazine of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateMagazineAmmo(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void updateMagazineAmmo(Unit& u);
		
		/*!
		
		@description

		Used to obtain the current weapon details of a unit within the VBS2 Environment by returning through a weapon object. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the weapon details is obtained. The unit should be created before passing it as a parameter. 

		@return Weapon

		@example

		@code

		//The unit to be created
		Unit u;
		
		//The weapon object to be created
		Weapon w;
		
		w = UnitUtilities::getCurrentWeapon(u);
		
		//Apart from the name, other details such as magazine name, muzzle name, ammo count and etc can be obtained 
		displayString+="The name of the current weapon of the unit is"+(w.getWeaponName());

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: updateCurrentWeapon(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the current weapon details of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getCurrentWeapon (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static Weapon getCurrentWeapon(const Unit& u);
		
		/*!
		
		@description

		Used to update the current weapon of a unit created within the VBS2 Environment in the fusion end. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the current weapon is updated in the fusion end. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		//The weapon object to be created
		Weapon w;

		UnitUtilities::updateCurrentWeapon(u);

		displayString+="The current weapon of the unit in the fusion end is "+(w.getWeaponName());

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: getCurrentWeapon(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To update the current weapon of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::updateCurrentWeapon (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void updateCurrentWeapon(Unit& u);

		//----------------------------------------------------------------------------
		/*!
		
		@description

		Used to obtain the pistol name of a unit within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit for which the pistol name is obtained. The unit should be created before passing it as a parameter.

		@return String

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The pistol weapon of the unit is "+(UnitUtilities::getPistolWeapon(u)); 

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the pistol name of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getPistolWeapon (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::string getPistolWeapon(const Unit& u); 

		/*!
		
		@description

		Used to obtain the weapons position of a unit within the VBS2 Environment in model space.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapons position is obtained. The unit should be created before passing it as a parameter. 

		@param weaponName � The name of the weapon, held by the unit. 
		
		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The vbs2_javelin weapon position is " + UnitUtilities::getWeaponPosition(commander,"vbs2_javelin").getVBSPosition();

		@endcode

		@overloaded

		UnitUtilities:: getWeaponPosition(Unit& unit, Weapon& weapon);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc); 

		@remarks To obtain the position of weapons of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponPosition(Unit& u,string weaponName).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static position3D getWeaponPosition(const Unit& unit, const std::string& weaponName);

		/*!
		
		@description

		Used to obtain the weapons position of a unit within the VBS2 Environment in model space. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapons position is obtained. The unit should be created before passing it as a parameter.  

		@param weapon- The weapon object in which the weapon�s name is passed. 
		
		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;

		Weapon w;

		w.setWeaponName("vbs2_javelin");

		displayString="The vbs2_javelin weapon position is " +UnitUtilities::getWeaponPosition(u,w).getVBSPosition();

		@endcode

		@overloaded

		UnitUtilities::getWeaponPosition(Unit& unit, string weaponName);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the position of weapons of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponPosition(Unit& u,string weaponName).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getWeaponPosition(const Unit& unit, const Weapon& weapon);

		/*!
		
		@description

		Used to obtain the weapons firing point for a particular muzzle in model space within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapon�s firing point is obtained. The unit should be created before passing it as a parameter. 

		@param muzzleName � The name of the muzzle, held by the unit. 
		
		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;

		displayString="The vbs2_javelin firing point is " + UnitUtilities::getWeaponPoint(u,"vbs2_javelin").getVBSPosition();

		@endcode

		@overloaded

		UnitUtilities:: getWeaponPoint(Unit& unit, Weapon& weapon);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the firing point of weapons of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponPoint(Unit& unit, string muzzleName).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getWeaponPoint(const Unit& unit, const std::string& muzzleName);

		/*!
		
		@description

		Used to obtain the weapons firing point for a particular muzzle in model space within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapon�s firing point is obtained. The unit should be created before passing it as a parameter. 

		@param weapon - The weapon object in which the weapon�s name is passed.
		
		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;

		Weapon w;

		w.setWeaponName("vbs2_javelin");

		displayString+="The vbs2_javelin firing point is " + UnitUtilities::getWeaponPoint(u,w).getVBSPosition();

		@endcode

		@overloaded

		UnitUtilities:: getWeaponPoint(Unit& unit, string muzzleName);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the firing point of weapons of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponPoint(Unit& unit, Weapon& weapon).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getWeaponPoint(const Unit& unit, const Weapon& weapon);

		/*!
		
		@description

		Used to obtain the direction in which the weapon is being pointed by the unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapon�s direction is obtained. The unit should be created before passing it as a parameter. 

		@param weaponName � The name of the weapon, held by the unit. 

		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;

		displayString="The vbs2_javelin weapon direction is "+UnitUtilities::getWeaponDirection(commander,"vbs2_javelin").getVBSPosition();

		@endcode

		@overloaded

		UnitUtilities::getWeaponDirection(Unit& unit, Weapon& weapon);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the direction of weapons of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponDirection (Unit& unit, string weaponName).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static position3D getWeaponDirection(const Unit& unit, const std::string& weaponName);

		/*!
		
		@description

		Used to obtain the direction in which the weapon is being pointed by the unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapon�s direction is obtained. The unit should be created before passing it as a parameter.

		@param weapon - The weapon object in which the weapon�s name is passed.

		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;

		Weapon w;

		w.setWeaponName("vbs2_javelin");

		displayString+="The VBS2_AU_F88A1_IRON_NONE weapon direction is "+UnitUtilities::getWeaponDirection(u,w).getVBSPosition();

		@endcode

		@overloaded

		UnitUtilities:: getWeaponDirection(Unit& unit, string weaponName);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the direction of weapons of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponDirection(Unit& unit, Weapon& weapon).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static position3D getWeaponDirection(const Unit& unit, const Weapon& weapon);

		/*! 
		
		@description

		Used to obtain the elevation of the weapon held by the unit within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapon�s elevation is obtained. The unit should be created before passing it as a parameter. 

		@param weaponName � The name of the weapon, held by the unit. 

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		displayString="The vbs2_javelin weapon elevation is " + conversions::DoubleToString(UnitUtilities::getWeaponElevation(u,"vbs2_javelin"));

		@endcode

		@overloaded

		UnitUtilities::getWeaponElevation(Unit& unit, Weapon& weapon);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the elevation of a weapon of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponElevation (Unit& unit, string weaponName).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getWeaponElevation(const Unit& unit, const std::string& weaponName);

		/*! 
		
		@description

		Used to obtain the elevation of the weapon held by the unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapon�s elevation is obtained. The unit should be created before passing it as a parameter. 

		@param weapon - The weapon object in which the weapon�s name is passed.

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		Weapon w;

		w.setWeaponName("vbs2_javelin");

		displayString="The vbs2_javelin weapon elevation is " + conversions::DoubleToString(UnitUtilities::getWeaponElevation(u,w));

		@endcode

		@overloaded

		UnitUtilities:: getWeaponElevation(Unit& unit, string weaponName);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the elevation of a weapon of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponElevation (Unit& unit, Weapon& weapon).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getWeaponElevation(const Unit& unit, const Weapon& weapon);

		/*! 
		
		@description

		Used to obtain the azimuth of the weapon held by the unit within the VBS2 Environment. Azimuth value will not be relative to the unit's body direction.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapon�s azimuth is obtained. The unit should be created before passing it as a parameter. 

		@param weaponName � The name of the weapon, held by the unit. 

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		displayString="The vbs2_javelin weapon azimuth is " + conversions::DoubleToString(UnitUtilities::getWeaponAzimuth(commander,"vbs2_javelin"));

		@endcode

		@overloaded

		UnitUtilities::getWeaponAzimuth (Unit& unit, Weapon& weapon);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the azimuth of a weapon of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponAzimuth (Unit& unit, string weaponName).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/		
		static double getWeaponAzimuth(const Unit& unit, const std::string& weaponName);

		/*! 
		
		@description

		Used to obtain the azimuth of the weapon held by the unit within the VBS2 Environment. Azimuth value will not be relative to the unit's body direction.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapon�s azimuth is obtained. The unit should be created before passing it as a parameter 

		@param weapon - The weapon object in which the weapon�s name is passed.

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		Weapon w;

		w.setWeaponName("vbs2_javelin");

		displayString="The vbs2_javelin weapon azimuth is "+ conversions::DoubleToString(UnitUtilities::getWeaponAzimuth(u,dx));

		@endcode

		@overloaded

		UnitUtilities:: getWeaponAzimuth (Unit& unit, string weaponName);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the azimuth of a weapon of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponAzimuth (Unit& unit, Weapon& weapon).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/		
		static double getWeaponAzimuth(const Unit& unit, const Weapon& weapon);
		
		/*!
		
		@description

		Used to obtain the list of objects a unit�s weapon is aiming at within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit �The unit holding the weapon. The unit should be created before passing it as a parameter. 

		@param weaponName - The name of the weapon, held by the unit.

		@return list

		@example

		@code

		//create a list as shown to obtain the values
		list<ControllableObject> myList = UnitUtilities::getWeaponAimingTargets(player, "vbs2_us_m16a4_iron_none");

		for (list<ControllableObject>::iterator itr = myList.begin(), itr_end = myList.end(); itr != itr_end; ++itr)
		{ 
			displayString+="The list of objects are"+itr->getName();
		}

		@endcode

		@overloaded

		UnitUtilities:: getWeaponAimingTargets(Unit& unit, Weapon& weapon);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc); 

		@remarks To obtain the list of target objects of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponAimingTargets (Unit& unit, string weaponName)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static std::list<ControllableObject> getWeaponAimingTargets(const Unit& unit, const std::string& weaponName);

		/*!
		
		@description

		Used to obtain the list of objects a unit�s weapon is aiming at within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit �The unit holding the weapon. The unit should be created before passing it as a parameter. 

		@param weapon - The weapon object in which the weapon name is passed. The weapon object should have a correct weapon name.

		@return list

		@example

		@code

		Weapon w;

		w = UnitUtilities::getCurrentWeapon(player);

		UnitUtilities::updateStaticProperties(player);

		UnitUtilities::updateDynamicProperties(player);

		UnitUtilities::updateCurrentWeapon(player);

		UnitUtilities::updateWeaponTypes(player);

		UnitUtilities::updateWeaponAmmo(player);

		list<ControllableObject> myList = UnitUtilities::getWeaponAimingTargets(player, w);

		for (list<ControllableObject>::iterator itr = myList.begin(), itr_end = myList.end(); itr != itr_end; ++itr)
		{ 
			displayString+="The list of objects are"+(*itr).getName();
		}

		@endcode

		@overloaded

		UnitUtilities:: getWeaponAimingTargets(Unit& unit, string weaponName);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the list of target objects of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponAimingTargets(Unit& unit, Weapon& weapon)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static std::list<ControllableObject> getWeaponAimingTargets(const Unit& unit, const Weapon& weapon);

		/*!
		Remove the one specified magazine from the unit.	
		Deprecated. Use void applyMagazineRemove(Unit& unit, string name)
		*/
		VBSFUSION_DPR(UUNT024) static void removeMagazine(Unit& unit, std::string name);

		/*!
		Remove all specified magazines from the unit.	
		Deprecated. Use void applyMagazineRemoveAll(Unit& unit, string name)
		*/
		VBSFUSION_DPR(UUNT025) static void removeallMagazines(Unit& unit, std::string name);

		/*!
		Add Weapon to the Unit. weapon slots are filled, any further addWeapon commands are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should 
		be added, before adding threse weapon.
		Deprecated. Use void applyWeaponAdd(Unit& unit,string name)
		*/
		VBSFUSION_DPR(UUNT026) static void addWeapon(Unit& unit, std::string name);

		/*!
		Remove the specified Weapon from the unit.	
		Deprecated. Use void applyWeaponRemove(Unit& unit, string name)
		*/
		VBSFUSION_DPR(UUNT027) static void removeWeapon(Unit& unit, std::string name);

		/*!
		Tells if a unit has the given weapon.	
		Deprecated. Use bool isWeaponHaving(Unit& unit, string name)
		*/
		VBSFUSION_DPR(UUNT028) static bool hasWeapon(Unit& unit, std::string name);
		
		/*!
		Sets the direction of the Weapon according to azimuth (relative to the unit's body direction)& elevation. 
		Must use the disableGunnerInput function before using this function.

		Deprecated. Use void applyWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition)
		*/
		VBSFUSION_DPR(UUNT076) static void setWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition);

		/*!
		
		@description

		Used to obtain the ammunition level of a unit within the VBS2 Environment. The value would be between 0 and 1.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the ammunition level is obtained. The unit should be created before passing it as a parameter.

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;

		displayString="Ammunition level of unit is "+conversions::FloatToString(UnitUtilities::getAmmoRatio(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the ammunition level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAmmoRatio(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static float getAmmoRatio(const Unit& unit);

		/*!
		@description

		This function retrieves and displays  the amount of ammunition the unit has compared to a full state defined by the unit type.  
		The value ranges between 0 to 1.

		@locality

		Globally applied 

		@version [VBS3Fusion v3.15]

		@param unit - The unit for which the amount of ammunition is checked.

		@return double

		@example

		@code

		//The unit to be created
		Unit soldier1;
		displayString = "The Ammo:  "+ conversions::DoubleToString(UnitUtilities::getAmmoRatioEx(soldier1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		@remarks - To obtain the ammunition of the unit  created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getAmmoRatioEx(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static double getAmmoRatioEx(const Unit& unit);

		/*! 
		Sets how much ammunition (compared to a full state defined by the unit type) the unit has. 
		The value ranges from 0 to 1.	
		Deprecated. Use void applyVehicleAmmo(Unit& unit, double ammovalue)
		*/
		VBSFUSION_DPR(UUNT029) static void setVehicleAmmo(Unit& unit, float ammovalue);

		/*!
		Sets how much ammunition (compared to a full state defined by the unit type) the unit has. 
		The value ranges from 0 to 1.	
		Deprecated. Use void applyVehicleAmmo(Unit& unit, double ammovalue)
		*/
		VBSFUSION_DPR(UUNT029) static void setVehicleAmmo(Unit& unit, double ammovalue);

		/*!

		@description

		Used to obtain the recoil impulse vectors of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the recoil impulse vector is obtained. The unit should be created before passing it as a parameter. 

		@return vector3D

		@example

		@code

		//The unit to be created
		Unit u;

		//To display the vector 3D value as a string use the function getVBSPosition
		displayString+="The impulse vector for the commander's weapon is"+UnitUtilities::getRecoilImpulse(u).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks The vector value can be obtained only when the weapon is actually firing.
		
		@remarks To obtain the recoil impulse vector of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getRecoilImpulse (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static vector3D getRecoilImpulse(const Unit& unit);

		/*!
		Remove all weapons and magazines from the unit.
		Deprecated. Use void applyWeaponRemoveAll(Unit& unit)
		*/
		VBSFUSION_DPR(UUNT030) static void removeallWeapons(Unit& unit);

		/*!
		Enables or Disables firing on current weapon. If true, the unit's current weapon cannot be fired.
		Deprecated. Use void applyWeaponSafety(Unit& unit, bool safe)
		*/
		VBSFUSION_DPR(UUNT033) static void setWeaponSafety(Unit& unit, bool safe);

		/*!
		
		@description

		Used to obtain the current status of a unit�s weapon within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapon�s safety is checked. The unit should be created before passing it as a parameter.  

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The safety of commander's current weapon is"+conversions::BoolToString(UnitUtilities::getWeaponSafety(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the safety level of a weapon held by a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponSafety (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool getWeaponSafety(const Unit& unit);

		/*!
		
		@description

		Used to update the armed properties of a unit created within the VBS2 Environment in the fusion end e.g. primary weapon, weapon types, weapon ammo, magazine types, magazine ammo etc.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the properties are updated in the fusion end. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		UnitUtilities::updateUnitArmedProperties(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To update the armed properties of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: updateUnitArmedProperties (Unit& u)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateUnitArmedProperties(Unit& unit);

		/*!
		
		@description

		Used to obtain the hiding position of a unit within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � A unit object. The unit should be created before passing it as a parameter.  

		@param enemy - Another unit object. The unit should be created before passing it as a parameter.

		@return position3D

		@example

		@code

		//The unit to be created
		Unit u1, u2;

		//To display the vector 3D value as a string use the function getVBSPosition
		displayString="The hiding position of commander"+UnitUtilities::getHideFrom(u1,u2).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This function is returns the hiding position of the enemy. If the enemy is hidden the position is returned as [0,0,0].

		@remarks To obtain the hidden position of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getHideFrom (Unit& unit, Unit& enemy).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information);
		
		*/
		static position3D getHideFrom(const Unit& unit, const Unit& enemy);

		/*!
		Returns a list of targets within the defined range.
		"Targets" are not restricted to enemy units.
		Deprecated. Use vector<Target> getNearTargets(Unit& unit, int range)
		*/
		VBSFUSION_DPR(UUNT034) static std::vector<Target> nearTargets(Unit& unit, int range);
	
		/*!
		Returns a list of targets within the defined range.
		"Targets" are not restricted to enemy units. 
		Deprecated. Use vector<Target> getNearTargets(Unit& unit, int range)
		*/
		VBSFUSION_DPR(UUNT034) static std::string* nearTarget(Unit& unit, int range);	

		/*!
		
		@description

		Used to check if a unit within the VBS2 Environment is controlled by the player or not. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param u � The unit checked. The unit should be created before passing it as a parameter. 

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;

		displayString="The object is controlled by the player - "+conversions::BoolToString(UnitUtilities::isPlayerControlled(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check whether a unit created in a client machine within a network is controlled by the player, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isPlayerControlled (Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/		
		static bool isPlayerControlled(const Unit& u);

		/*!
		
		@description

		Used to obtain the aerobic fatigue level of a unit within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the aerobic fatigue level is checked. The unit should be created before passing it as a parameter.  

		@return double

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The aerobic fatigue level is - "+conversions::DoubleToString(UnitUtilities::getAerobicFatigue(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the aerobic fatigue level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAerobicFatigue (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getAerobicFatigue(const Unit& unit);

		/*!
		
		@description

		Used to obtain the anaerobic fatigue level of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the anaerobic fatigue level is checked. The unit should be created before passing it as a parameter.  

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The anaerobic fatigue level is - "+conversions::DoubleToString(UnitUtilities::getAnaerobicFatigue(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the anaerobic fatigue level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAnaerobicFatigue (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getAnaerobicFatigue(const Unit& unit);

		/*!
		
		@description

		Used to obtain the morale level of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the morale level is obtained. The unit should be created before passing it as a parameter.  

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The morale level is - "+conversions::DoubleToString(UnitUtilities::getMoraleLevel(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the morale level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: get getMoraleLevel (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getMoraleLevel(const Unit& unit);

		/*!
		
		@description

		Used to obtain the long term morale level of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the long term morale level is obtained. The unit should be created before passing it as a parameter. 

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The long term morale level is - "+conversions::DoubleToString(UnitUtilities::getMoraleLongTerm(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the long term morale level of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: get getMoraleLevel (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double getMoraleLongTerm(const Unit& unit);

		/*!
		
		@description

		Used to obtain the training rating of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the training rating is obtained. The unit should be created before passing it as a parameter.  

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The training rate of a soldier is"+conversions::DoubleToString(UnitUtilities::getTraining(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the training rating of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getTraining (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static double getTraining(const Unit& unit);

		/*!

		@description

		Used to obtain the �up degree� of a unit within the VBS2 Environment. �Up degree� shows whether a unit is prone, kneeling, or standing, along with the type of weapon held by the unit at that instance.
		The following is a list of stances and their return values:
			
		rifle, lowered - standing/sitting: 10
		rifle, raised - standing: 8
		rifle - crouched: 6  
		rifle - prone: 4

		pistol - standing: 9 
		pistol - crouched: 7  
		pistol - prone: 5 

		launcher - standing: 1
		launcher - crouched: 1 
		launcher - prone: 3

		unarmed - standing/sitting: 12
		unarmed - prone: 3

		vehicles / static objects / non people: -1

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the up degree is obtained. The unit should be created before passing it as a parameter.  

		@return int

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString="The up degree is "+conversions::IntToString(UnitUtilities::getUpDegree(u));	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the up degree of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getUpDegree (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information) 

		*/
		static int getUpDegree(const Unit& unit);

		/*!
		Enables personal items. Used for units inside vehicles to enable their personal equipment.
		Deprecated. Use void applyPersonalItemsEnable(Unit& unit, bool enable)
		*/
		VBSFUSION_DPR(UUNT035) static void EnablePersonalItems(Unit& unit, bool enable);

		/*!

		@description

		Used to check if an object is within the line of sight of another object within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit that is looking. The unit should be created before passing it as a parameter. 

		@param co- The object within the line of sight of the unit. 

		@param angle- The angle in which the object is located inside unit�s vision.
		
		@return bool

		@example

		@code

		//The unit and object to be created
		
		ControllableObject co;
		Unit u;
		
		displayString+= "The value is "+conversions::BoolToString(UnitUtilities::isLookingAt(u, co, 90));	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To check for objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isLookingAt (Unit& unit, ControllableObject& co, double angle).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static bool isLookingAt(const Unit& unit, const ControllableObject& co, double angle);

		/*!
		
		@description

		Used to check if a unit within the VBS2 Environment is able to speak audibly or not. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit checked. The unit should be created before passing it as a parameter. 

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The audibility is"+conversions::BoolToString(UnitUtilities::isSpeakable(u));	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check the audibility of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isSpeakable (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool isSpeakable(const Unit& unit);
		
		/*!
		Turns the ability of a unit to audibly speak on or off.

		Does not affect orders from being issues or received.
		Units will still scream when being shot at.
		Deprecated. Use void applySpeechEnable(Unit& unit, bool speech)
		*/
		VBSFUSION_DPR(UUNT037) static void allowSpeech(Unit& unit, bool speech);

		/*!

		@description

		Used to remove a person's body parts and apply blood stains to the unit in proportion to the removed part within the VBS2 environment.

		@locality

		Globally applied and Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the amputation is done. The unit should be created before passing it as a parameter.  

		@param bodyPart � The part to be removed. Possible values are,
			"Arm_Left_Half": To remove left hand
			"Arm_Right_Half": To remove right hand
			"Leg_Left_Half": To remove lower left leg
			"Leg_Right_Half": To remove lower right leg
			"Head": To remove head
			"Arm_Left_Full": To remove left arm
			"Arm_Right_Full": To remove right arm
			"Leg_Left_Full": To remove left leg
			"Leg_Right_Full": To remove right leg
			"Head_Reset": To re-attach head
			"Arm_Left_Reset": To re-attach left arm
			"Arm_Right_Reset": To re-attach right arm
			"Leg_Left_Reset": To re-attach left leg
			"Leg_Right_Reset": To re-attach right leg

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyAmputation(u, BODYPART::B_LEG_LEFT_HALF); 

		@endcode

		@overloaded

		UnitUtilities::applyAmputation(Unit& unit, MAIN_BODYPART bodyPart, BODY_SEGMENT segment);

		@related

		None

		@remarks This function is available from version VBS2 1.50. In VBS2 1.50 amputations are present for all US Army and USMC soldiers.

		@remarks Removal of body parts has no effect on the health status of a unit (i.e. a person without a head would still be alive, and a person without legs can still walk), thereby it's up to the mission designer to ensure that these effects are applied in a realistic manner.
		
		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks To amputate a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities applyAmputation(Unit& unit, BODYPART bodyPart)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static void applyAmputation(Unit& unit, BODYPART bodyPart);

		/*!
		
		@description

		Used to remove a person's body parts and apply blood stains to the unit in proportion to the removed part within the VBS2 environment.

		@locality

		Globally applied and Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit�The unit for which the amputation is done. The unit should be created before passing it as a parameter.

		@param bodyPart � The part to be removed. Possible values are,
			"leftarm": To remove left arm
			"rightarm ": To remove right arm
			"leftleg ": To remove left leg
			"rightleg": To remove right leg

		@param segment � The segment of the specified body part.Possible values are,
			"head"
			"lefthand"
			"leftforearm"
			"righthand"
			"rightforearm"
			"leftfoot"
			"leftuplegroll"
			"rightfoot"	
			"rightuplegroll"
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyAmputation(u, MAIN_BODYPART::MB_LEFT_ARM, BODY_SEGMENT::BS_LEFT_HAND);

		@endcode

		@overloaded

		UnitUtilities::applyAmputation(Unit& unit, BODYPART bodyPart);

		@related

		None

		@remarks This function is available from version VBS2 1.50. In VBS2 1.50 amputations are present for all US Army and USMC soldiers. 

		@remarks Removal of body parts has no effect on the health status of a unit (i.e. a person without a head would still be alive, and a person without legs can still walk), thereby it's up to the mission designer to ensure that these effects are applied in a realistic manner.
		
		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks To amputate a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities applyAmputation(Unit& unit, BODYPART bodyPart)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static void applyAmputation(Unit& unit, MAIN_BODYPART bodyPart, BODY_SEGMENT segment);

		/*!
		
		@description

		Used to obtain which body part has been removed a unit within the VBS2 Environment. 

		@locality

		Locally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the vehicle is assigned. The unit should be created before passing it as a parameter.  

		@param bodyPart- The body part which is amputated.

		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		string body=UnitUtilities::getAmputation(u,B_LEG_LEFT_HALF);
		displayString+=" The amputated body part is"+body; 

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static std::string getAmputation(const Unit& unit, BODYPART bodyPart);


		/*!
		Adds the passed value to the unit's morale (morale is between 0=exhausted and 1=fit). 
		moralChange - It is the adding value. use (-)ve to reduce the morale
		Deprecated. Use void applyMoralAddition(Unit& unit, double moralChange)
		*/
		VBSFUSION_DPR(UUNT036) static void addToMorale(Unit& unit, double moralChange);

		/*!
		Adds the passed value to the unit's anaerobic fatigue (fatigue is between 0 and 1). 
		moralChange - It is the adding value. use (-)ve to reduce the morale
		Deprecated. Use void applyAnaerobicFatigueAddition(Unit& unit, double AnaerobicFatigueChange)
		*/
		VBSFUSION_DPR(UUNT038) static void addToAnaerobicFatigue(Unit& unit, double AnaerobicFatigueChange);

		/*!
		Adds the passed value to the unit's aerobic fatigue (fatigue is between 0 and 1). 
		moralChange - It is the adding value. use (-)ve to reduce the morale
		Deprecated. Use void applyAerobicFatigueAddition(Unit& unit, double AerobicFatigueChange)
		*/
		VBSFUSION_DPR(UUNT039) static void addToAerobicFatigue(Unit& unit, double AerobicFatigueChange);

		/*!
		
		@description

		Used to obtain a vehicle which is assigned to particular a unit within the VBS2 Environment. 

		@locality

		Locally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the vehicle is assigned. The unit should be created before passing it as a parameter.  

		@return Vehicle

		@example

		@code

		//The unit to be created
		Unit u;
		Vehicle veh3;
		
		veh3 =UnitUtilities::getAssignedVehicle(player);
		displayString+="The name of the vehicle is "+veh3.getName();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static Vehicle getAssignedVehicle(const Unit& unit);

		/*!
		@description 
		
		Creates a unit of the given type. Unit is only visible on the local machine.

		@locality

		Locally Applied Locally Effected

		@version [VBS3Fusion v3.15]

		@param type - The type of unit to be  created.

		@param position - Start position

		@param group - Group to be joined.  Units created this way cannot be grouped via the join command.

		@param init - initialization string (optional)

		@param skill-  skill level (optional, default: 0.5)

		@param rank -(optional, default: "PRIVATE")

		@return Nothing

		@example 

		@code

		Group group1;
		string type = "VBS32_AU_SOC_TAG_K_Mp5sd6_Gasmask";
		position3D pos = (playerUnit.getPosition()+position3D(40,40,0));
		UnitUtilities::CreateUnitLocal(type,position3D(pos),group1,string("Hello"),0.5,"PRIVATE");

		@end code
		
		@related
		
		None
		
		@overloaded
		
		None
		
		@remarks- It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).  
		@remarks- The group should be created before using the function CreateUnitLocal(string type, position3D& position,Group& group, string initString , double skill, string rank ).
		*/
		static void CreateUnitLocal(const std::string& type, const position3D& position, Group& group, const std::string& initString = "", double skill = 0.5, const std::string& rank = "PRIVATE");

	 /*!
	 Order the given unit(s) to stop (without radio messages).
	 DoStop'ed units leave the groups formation. It will prevent the unit from moving around with their group 
	 (or formation leader), while still beeing able to turn around and even move to a new position if they see fit. 
	 They will still respond to orders from their group leader (like engage, rearm, board a vehicle), but all of their 
	 actions will be seperate from the group formation (unless ordered to return, which AI leaders don't do unless a script tells them to).
	 Deprecated. Use void applyStop(Unit& unit)
	 */
	VBSFUSION_DPR(UUNT040) static void StopUnit(Unit& unit);

	 /*!
	 
	 @description

	 Used to check if a unit is wearing NVG equipment within the VBS2 Environment.

	 @locality

	 Globally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit checked. The unit should be created before passing it as a parameter. 

	 @return bool

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 displayString = "The unit is wearing NVG - "+conversions::BoolToString(UnitUtilities::isWearingNVG(u));

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To check if a unit created in a client machine within a network is wearing NVG equipment, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isWearingNVG (Unit& unit).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	 */
	static bool isWearingNVG(const Unit& unit);

	 /*!
	 
	 @description

	 Used to check if personal items are enabled for a unit within the VBS2 Environment. 

	 @locality

	 Globally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit checked. The unit should be created before passing it as a parameter. 

	 @return bool

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 displayString+="Personal items enabled - "+conversions::BoolToString(UnitUtilities::isPersonalItemsEnabled(u));

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To check for a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isPersonalItemsEnabled (Unit& unit).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	 */
	static bool isPersonalItemsEnabled(const Unit& unit);

	 /*!
	 
	 @description

	 Used to obtain the watch mode of a unit within the VBS2 Environment. 

	 @locality

	 Globally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the watch mode is obtained. The unit should be created before passing it as a parameter. 

	 @return double

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 displayString ="The watch mode is"+UnitUtilities::getWatchMode(U);	

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To obtain the watch mode of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getTraining (Unit& unit).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
	 
	 */
	static std::string getWatchMode(const Unit& unit);
	
	 /*!
	 
	 @description

	 Used to set the speed multiplier that defines the speed at which an animation is seen however the unit can still change from walking to running and so forth. 

	 @locality

	 Globally applied and Locally effected

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the speed is applied. The unit should be created before passing it as a parameter. 

	 @param speed � Possible values of speed are,
		0 -	frozen
		1 � Normal speed
		2 - Twice the normal speed

	 @return Nothing

	 @example

	 @code

	 //The unit to be created
	 Unit u;

	 UnitUtilities::applyAnimationSpeed(u,2);

	 @endcode

	 @overloaded

	 None

	 @related

	 UnitUtilities:: getAnimationSpeed(Unit& unit);

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

	 @remarks This function depends on the applyMinAnimationSpeed(Unit& unit, double percent); 
	 
	 @remarks To apply the animation speed to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyAnimationSpeed (Unit& u, double speed).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	 */
	 static void applyAnimationSpeed(Unit& unit, double speed);

	 /*!
	 @description

	 Used to obtain the animation speed multiplier of a unit within the VBS2 Environment. 

	 @locality

	 Locally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the animation speed multi player is obtained. The unit should be created before passing it as a parameter.  

	 @return double

	 @example

	 @code

	 //The unit to be created
	 Unit u;

	 displayString="The animation speed of u is"+conversions::DoubleToString(UnitUtilities::getAnimationSpeed(u));

	 @endcode

	 @overloaded

	 None

	 @related

	 UnitUtilities::applyAnimationSpeed(Unit& unit, double speed);

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To obtain the animation speed of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAnimationSpeed (Unit& unit), however as the function applyanimationSpeed�s effect is local the value would be difficult obtain and therby the getAnimationSpeed function�s locality is taken as local as mentioned above.
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information).  
	 */
	 static double getAnimationSpeed(const Unit& unit);

	 /*!
	 
	 @description

	 Used to draw a visualization editor, indicating a unit�s view cone within the VBS2 environment. 

	 @locality

	 Globally applied and Locally effected

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the frustum is drawn . The unit should be created before passing it as a parameter.  

	 @param mode- The mode where the frustum should be drawn. Possible values are,
	 	 0 - none
		 1 - 2D map
		 2 - 3D map
		 3 - both 
	 @param size � Size of cone in meters
	 
	 @param color - Color of cone (Optional).

	 @return Nothing

	 @example

	 @code

	 //The unit to be created
	 Unit u;

	 Color_RGBA colu;
	 colu.r=1;
	 colu.g=1;
	 colu.b=1;
	 colu.a=.5;

	 UnitUtilities::applyDrawFrustrum(u,3,20, colu);

	 @endcode

	 @overloaded

	 UnitUtilities::applyDrawFrustrum(Unit& unit, int mode, double size);

	 @related

	 UnitUtilities::getDrawFrustrum(Unit& unit);

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To draw the frustum for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyDrawFrustrum(Unit& unit, int mode, double size, Color_RGBA color).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	 */
	 static void applyDrawFrustrum(Unit& unit, int mode, double size, const Color_RGBA& color);

	 /*!
	
	@description

	Used to draw a visualization editor, indicating a unit�s view cone within the VBS2 environment.

	@locality

	Globally applied and Locally effected

	@version [VBS2 2.02 / VBS2Fusion v3.02]

	@param unit � The unit for which the frustum is drawn . The unit should be created before passing it as a parameter.

	@param mode - The mode where the frustum should be drawn. Possible values are,
		0-	none
		1-	2D map
		2-	3D map
		3-	both 

	@param size �Size of cone in meters
	
	@return Nothing

	@example

	@code

	//The unit to be created
	Unit u;
	
	UnitUtilities::applyDrawFrustrum(u,3,20);

	@endcode

	@overloaded

	UnitUtilities::applyDrawFrustrum(Unit& unit, int mode, double size, Color_RGBA color);

	@related

	UnitUtilities::getDrawFrustrum(Unit& unit);

	@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	@remarks To draw the frustum for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyDrawFrustrum(Unit& unit, int mode, double size).
	(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	 */
	 static void applyDrawFrustrum(Unit& unit, int mode, double size);

	 /*!
	 
	 @description

	 Used to obtain the drawn frustum setting of a unit within the VBS2 Environment. 

	 @locality

	 Globally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the frustum setting is obtained. The unit should be created before passing it as a parameter.

	 @return double

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 displayString+="The frustum setting mode is"+conversions::IntToString(UnitUtilities::getDrawFrustrum(u).mode);
	 displayString+="The frustum setting size is"+conversions::DoubleToString(UnitUtilities::getDrawFrustrum(u).size);

	 @endcode

	 @overloaded

	 None

	 @related

	 UnitUtilities:: applyDrawFrustrum(Unit& unit, int mode, double size, Color_RGBA color);

	 UnitUtilities:: applyDrawFrustrum(Unit& unit, int mode, double size);

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To obtain the frustum settings of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getDrawFrustrum(Unit& unit).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	 */
	 static DRAWFRUSTRUM getDrawFrustrum(const Unit& unit);

	 /*!

	 @description

	 Used to draw ink-spots in AAR to indicate the time a unit is stationary within the VBS2 environment. 

	 @locality

	 Globally applied and Locally effected

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the ink spot is drawn . The unit should be created before passing it as a parameter.  

	 @param drawType - The type of marker to draw, options can be added together ex: ( 1 + 2 + 4 ). Possible values are,
	 0: no drawing
	 +1: 2D drawing ON
	 +2: 3D drawing ON (disc by default)
	 +4: switch 3D drawing to a cloud (instead of disc, 2 must be set to be visible)

	 @param maxSize �Maximum size of the mark in meters.
	 
	 @param growRate- Growth rate of the mark in meters per second.
	 
	 @param color- 2D and 3D Marker color.
	 
	 @param offset- Offsets the default 3D disc representation above ground by the given value in meters (optional, default: 0.1).
	 
	 @return Nothing

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 Color_RGBA colunit;
	 colunit.r=1;
	 colunit.g=1;
	 colunit.b=1;
	 colunit.a=.5;
	 
	 UnitUtilities::applyDrawStationary(u, 1+2, 15, 15, colunit,0.1);

	 @endcode

	 @overloaded

	 UnitUtilities::applyDrawStationary(Unit& unit, int drawType, double maxSize = 2, double growRate = 0.03);

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To draw the ink spot for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyDrawStationary(Unit& unit, int drawType, double maxSize, double growRate, Color_RGBA color, double offset = 0.1)
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information).
	 
	 */
	 static void applyDrawStationary(Unit& unit, int drawType, double maxSize, double growRate, const Color_RGBA& color, double offset = 0.1);

	 /*!

	 @description

	 Used to draw ink-spots in AAR to indicate the time a unit is stationary within the VBS2 environment.  

	 @locality

	 Globally applied and Locally effected

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the ink spot is drawn . The unit should be created before passing it as a parameter. 

	 @param drawType - The type of marker to draw, options can be added together ex: ( 1 + 2 + 4 ). Possible values are,
	 0: no drawing
	 +1: 2D drawing ON
	 +2: 3D drawing ON (disc by default)
	 +4: switch 3D drawing to a cloud (instead of disc, 2 must be set to be visible)

	 @param maxSize � Maximum size of the mark in meters (optional, default is 2)
	 
	 @param growRate - Growth rate of the mark in meters per second (optional default is .03)
	 
	 @return Nothing

	 @example

	 @code

	 //The unit to be created
	 Unit u;

	 Color_RGBA colunit2;
	 colunit2.r=0;
	 colunit2.g=1;
	 colunit2.b=1;
	 colunit2.a=.5;
	 
	 UnitUtilities::applyDrawStationary(u, 1+2, 15, 0.03);

	 @endcode

	 @overloaded

	 applyDrawStationary(Unit& unit, int drawType, double maxSize, double growRate, Color_RGBA color, double offset = 0.1);

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To draw the ink spot for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyDrawStationary(Unit& unit, int drawType, double maxSize = 2, double growRate = 0.03)
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information) 
	 
	 */
	 static void applyDrawStationary(Unit& unit, int drawType, double maxSize = 2, double growRate = 0.03);

	 /*!
	 
	 @description

	 Used to return the direction of a unit�s head within the VBS2 Environment. 

	 @locality

	 Globally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the direction is obtained. The unit should be created before passing it as a parameter.  

	 @return vector<double>

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 vector<double> headdirection = UnitUtilities::getHeadDirection(u);
	 for (vector<double>::iterator itr = headdirection.begin(),itr_end =headdirection.end();itr!=itr_end;++itr)
	 { 
		displayString+="The values are"+conversions::DoubleToString(*itr);
	 }

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To obtain the direction of a unit�s head created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getHeadDirection(Unit& unit).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 		
	 
	 */
	 static std::vector<double> getHeadDirection(const Unit& unit);

	 /*!
	 
	 @description

	 Used to return the azimuth of a unit�s head within the VBS2 Environment. 

	 @locality

	 Globally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the azimuth is obtained. The unit should be created before passing it as a parameter.  

	 @return double

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 displayString+="the azimuth is "+conversions::DoubleToString(UnitUtilities::getAzimuthHeadDirection(u));

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To obtain the direction of a unit�s head created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: get getAzimuthHeadDirection(Unit& unit).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	 */
	 static double getAzimuthHeadDirection(const Unit& unit);

	 /*!
	 
	 @description

	 Used to return the elevation of a unit�s head within the VBS2 Environment.

	 @locality

	 Globally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the elevation is obtained. The unit should be created before passing it as a parameter. 

	 @return double

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 displayString+="the elevation is "+conversions::DoubleToString(UnitUtilities::getElevationHeadDirection(u));

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To obtain the elevation of a unit�s head created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: get getAzimuthHeadDirection(Unit& unit).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	 */
	 static double getElevationHeadDirection(const Unit& unit);

	 /*!
	 
	 @description

	 Used to return the roll of a unit�s head within the VBS2 Environment. 

	 @locality

	 Globally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the roll is obtained. The unit should be created before passing it as a parameter. 

	 @return double

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 displayString+="the elevation is "+conversions::DoubleToString(UnitUtilities::getRollHeadDirection(u));

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

	 @remarks To obtain the roll of a unit�s head created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: get getRollHeadDirection(Unit& unit).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	 */
	 static double getRollHeadDirection(const Unit& unit);


	 /*!
	 
	 @description

	 Used to check if a unit�s movement has been disabled within the VBS2 Environment. 

	 @locality

	 Globally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit checked. The unit should be created before passing it as a parameter. 

	 @return bool

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 displayString+=" The animation movement disablity is "+conversions::BoolToString(UnitUtilities::isDisableAnimationMove(u));

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

	 @remarks To check the movement disability of a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isDisableAnimationMove (Unit& unit).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

	 */
	 static bool isDisableAnimationMove(const Unit& unit);


	 /*!

	 @description

	 Used to set the minimum control input that will be used for movement speed.
	 When using a joystick or the setAction command, movement speed is analog (meaning, it can be in a range from no movement to full movement speed). This command puts a minimum on that analog range, so that the input can not be lower than the specified percent.
	 When using a keyboard, control input is always 100%, so this command is not relevant in this situation. 

	 @locality

	 Globally applied

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the input is set. The unit should be created before passing it as a parameter.

	 @param percent � The minimum analog input that will be accepted for movement .The value would be between 0 and 1

	 @return Nothing

	 @example

	 @code

	 //The unit to be created
	 Unit u;

	 UnitUtilities:: applyMinAnimationSpeed(u,0.3);

	 @endcode

	 @overloaded

	 None

	 @related

	 UnitUtilities:: getAnimationSpeed(Unit& unit);

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To apply the minimum  animation speed to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyMinAnimationSpeed (Unit& u, double percent).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
	 
	 */
	 static void applyMinAnimationSpeed(Unit& unit, double percent);

	 /*!
	 
	 @description

	 Used to set a person�s facial animation (blinking) within the VBS2 environment.

	 @locality

	 Globally applied and Locally effected

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the facial animation is set. The unit should be created before passing it as a parameter.  

	 @param blink �Number which is set for blinking.(The blink is in the range from 0 to 1).

	 @return Nothing

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 UnitUtilities::applyFaceAnimation(u,0);

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To set a facial blinking animation to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities applyFaceAnimation(Unit& unit, double blink).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
	 
	 */
	 static void applyFaceAnimation(Unit& unit, double blink);

	 /*!
	 
	 @description

	 Used to set a person�s face within the VBS2 environment.

	 @locality

	 Globally applied and Locally effected

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the face is set. The unit should be created before passing it as a parameter. 

	 @param face � The face to be set. Possible values are,
		F_MAN, F_CHILD_AFGHAN, F_CHILD_ASIAN,F_CHILD_EUROPEAN, F_CHILDREN,
		F_MAN_AFRICAN, F_MAN_AMERICAN, F_MAN_AMERICAN_MILITARY, F_MAN_ASIAN,
		F_BEARED_MIDDLE_EAST, F_MAN_EUROPEAN, F_EUROPEAN_MILITARY, F_MAN_INDIAN, F_MAN_MIDDLE_EAST 

	 @return Nothing

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 UnitUtilities::applyFace(u,FACETYPE::F_MAN_AFRICAN);

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

	 @remarks To set a face to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyFace (Unit& unit, FACETYPE face).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
	 
	 */	
	 static void applyFace(Unit& unit, FACETYPE face);

	 /*!
	 
	 @description

	 Used to set a person�s facial expression within the VBS2 environment.

	 @locality

	 Globally applied and Locally effected

	 @version [VBS2 2.02 / VBS2Fusion v3.02]

	 @param unit � The unit for which the facial expressionis set. The unit should be created before passing it as a parameter. 

	 @param mimic � The facial expression to be set. Possible values are,
		FE_DEFAULT, FE_NORMAL, FE_SMILE, FE_HURT, FE_IRONIC,
		FE_SAD, FE_CYNIC, FE_SURPRISED, FE_AGRESIVE, FE_ANGRY

	 @return Nothing

	 @example

	 @code

	 //The unit to be created
	 Unit u;
	 
	 UnitUtilities::applyMimic(u,MIMIC::FE_SMILE);

	 @endcode

	 @overloaded

	 None

	 @related

	 None

	 @remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

	 @remarks To set a facial expression to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities applyMimic(Unit& unit, MIMIC mimic).
	 (Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

	 */
	 static void applyMimic(Unit& unit, MIMIC mimic);

	 /*!
	 Revives an Unit immediately, preserving his name & equipment.
	 Deprecated. Use void applyRevive(Unit& unit)
	 */
	 VBSFUSION_DPR(UUNT041) static void reviveUnit(Unit& unit);


	 /*!
	 @description
	 
	 Sets unit combat mode (engagement rules). Does not work for groups.

	 @locality

	 Locally Applied Globally Effected

	 @version [VBS2Fusion v3.10.1]
 
	 @param unit - A unit that suppose to assign combat mode.

	 @param mode - Mode may be one of:
	 "BLUE" (Never fire)
	 "GREEN" (Hold fire - defend only)
	 "WHITE" (Hold fire, engage at will)
	 "YELLOW" (Open fire)
	 "RED" (Open fire, engage at will) 

	 @return Nothing.

	 @example

	 @code

	 //Unit to be created
	 Unit u;

	 //Applied CombatMode GREEN to the unit.
	 UnitUtilities::applyUnitCombatMode(unit1,WAYPOINTCOMBATMODE::GREEN);

	 @endcode

	 @overloaded

	 @related

	 @remarks

	 */
	 static void applyUnitCombatMode(Unit& unit, WAYPOINTCOMBATMODE mode);

	 /*!
	 Orders the unit to watch the given target (via the radio). 
	 Use objNull as the target to order a unit to stop watching a target 

	 deprecated. Use void applyCommandWatch(Unit& unit, ControllableObject& target)
	 */
	 VBSFUSION_DPR(UUNT077) static void commandWatch(Unit& unit, ControllableObject& target);

	 /*!
	 Orders the unit to watch the given position (via the radio) 

	 deprecated. Use void applyCommandWatch(Unit& unit, position3D& target)
	 */
	 VBSFUSION_DPR(UUNT078) static void commandWatch(Unit& unit, position3D& target);

	 /*!
	 Control what the unit is glancing at (target or Position). How frequently the unit is glancing there depends on behaviour. 

	 Deprecated. Use void applyGlanceAt(Unit& unit, position3D& position)
	 */
	 VBSFUSION_DPR(UUNT079) static void glanceAt(Unit& unit, position3D& position);

	 /*!
	 Control what the unit is looking at position.

	 Deprecated. Use void applyLookAt(Unit& unit, position3D& position)
	 */
	 VBSFUSION_DPR(UUNT080) static void LookAt(Unit& unit, position3D& position);

		/*!
		Add weapons to the cargo space of objects, which can be taken out by infantry units. 
		Once the weapon cargo space is filled up, any further addWeaponCargo commands are ignored.
		Deprecated. Use void applyWeaponCargo(Unit& unit, string weaponName, int count)
		*/
		VBSFUSION_DPR(UUNT042) static void addWeaponToCargo(Unit& unit, std::string weaponName, int count);

		/*!
		
		@description

		Used to obtain the weapons in the cargo within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the weapons is obtained. The unit should be created before passing it as a parameter. 

		@return vector<MAGAZINECARGO>

		@example

		@code

		//The unit to be created
		Unit u;

		vector<WEAPONCARGO> myList2 = UnitUtilities::getWeaponFromCargo(u);
		for (vector< WEAPONCARGO>::iterator itr = myList2.begin(); itr!= myList2.end(); ++itr)
		{ 
			displayString+="The name is "+itr->weaponName;
			displayString+="The name is "+ conversions::IntToString(itr->count);
		}

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: applyWeaponCargo(Unit& unit, string weaponName, int count);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc), 

		@remarks To obtain the weapons in the cargo of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getWeaponFromCargo(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::vector<WEAPONCARGO> getWeaponFromCargo(const Unit& unit);

		/*!
		Add magazines to the cargo space of Objects, which can be taken out by infantry units. 
		Once the magazine cargo space is filled up, any further addMagazineCargo commands are ignored.
		Deprecated. Use void applyMagazineCargo(Unit& unit, string magName, int count)
		*/
		VBSFUSION_DPR(UUNT043) static void addMagazineToCargo(Unit& unit, std::string magName, int count);

		/*!
		
		@description

		Used to obtain the magazines in the cargo within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the magazine is obtained. The unit should be created before passing it as a parameter. 

		@return vector<MAGAZINECARGO>

		@example

		@code

		//The unit to be created
		Unit u;
		
		vector<MAGAZINECARGO> myList2 = UnitUtilities::getMagazineFromCargo(u);
		for (vector<MAGAZINECARGO>::iterator itr = myList2.begin(); itr!= myList2.end(); ++itr)
		{
			displayString+="The name is "+itr->magazineName;
		}

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: applyMagazineCargo(Unit& unit, string magName, int count);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc),

		@remarks To obtain the magazines in the cargo of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getMagazineFromCargo(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::vector<MAGAZINECARGO> getMagazineFromCargo(const Unit& unit);

		/*!
		
		@description

		Used to obtain the muzzles of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the muzzles are obtained. The unit should be created before passing it as a parameter. 

		@return vector<string>

		@example

		@code

		//The unit to be created
		Unit u;
		
		vector<string> vstring = UnitUtilities::getMuzzles(u);
		for (vector<string>::iterator itr = vstring.begin(); itr!= vstring.end(); ++itr)
		{
			displayString +="The muzzle name is" + *itr;
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the muzzles of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getMuzzles(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::vector<std::string> getMuzzles(const Unit& unit);

		
		/*!
		
		@description

		Used to obtain the position a unit/shooter has to aim at to hit a target with the current selected weapon within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit/shooter aiming a target. The unit should be created before passing it as a parameter. 
		
		@param target � The object being aimed at by the unit. 

		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;
		
		ControllableObject co;
		
		displayString+="The position to aim is "+UnitUtilities::getAimingPosition(u, co).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the aiming position for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAimingPosition(Unit& unit, ControllableObject& target).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getAimingPosition(const Unit& unit, const ControllableObject& target);

		/*!
		It is only for local objects
		Order the given unit(s) to move to the given location (via the radio). 
		Exactly the same as doMove, except this command displays a radio message. 
		Deprecated. Use void applyCommandMove(Unit& unit, position3D position)
		*/
		VBSFUSION_DPR(UUNT044) static void commandMove(Unit& unit, position3D position);

		/*!
		Add a number to the rating of a unit. Negative values can be used to reduce the rating. This command is 
		usually used to reward for completed mission objectives. The rating is given at the end of the mission 
		and is automatically adjusted when killing enemies or friendlies. When the rating gets below zero, the 
		unit is considered "renegade" and is an enemy to everyone.
		Deprecated. Use void applyRatingIncrease(Unit& unit, double rating)
		*/
		VBSFUSION_DPR(UUNT045) static void addRating(Unit& unit, double rating);

		/*!
		
		@description

		Used to obtain the rating of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the rating is obtained. The unit should be created before passing it as a parameter. 

		@return double

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The unit rating is "+conversions::DoubleToString(UnitUtilities::getRating(u));	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks The rating is increased for killing enemies and decreased for killing friends. The rating can be changed via the function UnitUtilities:: applyRatingIncrease(Unit& unit, double rating) by the mission designer. The rating of the player is displayed as the "score" at the end of the mission.

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the rating of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getRating(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static double getRating(const Unit& unit);

		/*!
		Add a number to the score of a unit. This score is shown in multiplayer in the "I" screen. Negative values will remove from the score
		Deprecated. Use void applyScoreIncrease(Unit& unit, int score)
		*/
		VBSFUSION_DPR(UUNT046)static void addScore(Unit& unit, int score);

		/*!
		
		@description

		Used to obtain the score of a unit within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the score is obtained. The unit should be created before passing it as a parameter.  

		@return int

		@example

		@code

		//The unit to be created
		Unit player;
		
		displayString+=" The person's score is "+conversions::IntToString(UnitUtilities::getScore(player));	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This function is applicable to player units only.
		
		@remarks To obtain the score of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getScore(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static int getScore(Unit& unit);

		/*!
		
		@description

		Used to check if unit can stand within the VBS2 Environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit that is checked. The unit should be created before passing it as a parameter. 

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The unit can stand - "+conversions::BoolToString(UnitUtilities::canStand(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check if a unit created in a client machine within a network can stand, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: canStand(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		VBSFUSION_DPR(UUNT047) static bool canStand(Unit& unit);

		/*!
		
		@description

		Used to check if a unit�s hand is hit which would result in inaccurate aiming. 
		If the hands aren't damaged, the returned value is 0. If the hands are hit or the unit is dead, the returned value is 1. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit that is checked. The unit should be created before passing it as a parameter.    

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+=" The unit's hand is hit - "+conversions::DoubleToString(UnitUtilities::getHandsHit(u));	

		@endcode

		@overloaded

		None

		@related

		None 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To check for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getHandsHit(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		
		*/
		static double getHandsHit(const Unit& unit);

		/*!
		Check if unit has some ammo. 
		Deprecated. Use bool isAmmoAvailable(Unit& unit)
		*/
		VBSFUSION_DPR(UUNT048) static bool hasSomeAmmo(Unit& unit);

		/*!
		
		@description

		Used to check whether the unit within the VBS2 Environment is (a subtype) of the given type.This function works with classes contained in CfgVehicles or CfgAmmo, but not with others (e.g. CfgMagazines or CfgWeapons).

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit that is checked. The unit should be created before passing it as a parameter.  

		@param typeName- The subtype that used for the checking.

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString ="is Kind Of Soldier: " conversions::BoolToString(ControllableObjectUtilities::isKindOf(u,"Soldier"));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isKindOf(Unit& unit, string typeName).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		
		*/
		static bool isKindOf(const Unit& unit, const std::string& typeName);

		/*!

		@description

		Used to mark a unit as captive within the VBS2 Environment.

		@locality

		Locally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit imprisoned/confined. The unit should be created before passing it as a parameter. 

		@param captive - The value to be set,
			True = captive
			False = Not captive.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyCaptive(u,true);

		@endcode

		@overloaded

		UnitUtilities::applyCaptive(Unit& unit, int captiveNum);

		@related

		None

		@remarks If unit is a vehicle, the commander of the vehicle is marked. 

		@remarks A captive is neutral to everyone, and will not trigger "detected by" conditions for its original side. Using a number instead of a boolean has no further effect on the engine's behavior, but can be used by scripting commands to keep track of the captivity status at a finer resolution (e.g. handcuffed, grouped, etc.).
		
		@remarks The function UnitUtilities::applyCaptive(Unit& unit, int captiveNum)was introduced in VBS2 v1.19. Boolean syntax existed since v1.0.

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		*/
		static void applyCaptive(Unit& unit, bool captive);

		/*!

		@description

		Used to mark a unit as captive within the VBS2 Environment.

		@locality

		Locally applied and Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit imprisoned/confined. The unit should be created before passing it as a parameter. 
 
		@param captiveNum- The value to be set. Possible values are,
		2 - Capture: Unit receives action to be captured, i.e. to become part of the player's group
		4 - Release: Unit receives action to be released, i.e. to leave the player's group. If the AI was part of a different group before capture, he will not rejoin that group upon release.
		8 - Cuff: Unit receives action to be handcuffed. Adults will enter an animation that shows their hands on their backs, children will not display any special animation.
		16 - Uncuff: Unit receives action to be uncuffed. 

		The combination of these modes are possible by adding the base numbers:
		3 - Unit set captive & Capture action available (mode 1 + 2)
		10 - Capture & cuff actions available (mode 2 + 8)
		12 - Release & cuff actions available (mode 4 + 8)
		18 - Capture & uncuff actions available (mode 2 + 16)
		20 - Release & uncuff actions available (mode 4 + 16) 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		nitUtilities::applyCaptive(gunner,2+8));

		@endcode

		@overloaded

		UnitUtilities::applyCaptive(Unit& unit, bool captive);

		@related

		UnitUtilities::updateRank(Unit& u);

		@remarks If unit is a vehicle, the commander of the vehicle is marked.
			- Once an action is activated, the captive value may change automatically, i.e. after the "Capture" action is executed the captiveNum value for that unit changes to 4, indicating that the unit can now be released.
			- A captive is neutral to everyone, and will not trigger "detected by" conditions for its original side. Using a number instead of a boolean has no further effect on the engine's behavior, but can be used by scripting commands to keep track of the captivity status at a finer resolution (e.g. handcuffed, grouped, etc.) 

		@remarks The function UnitUtilities::applyCaptive(Unit& unit, int captiveNum)was introduced in VBS2 v1.19. Boolean syntax existed since v1.0. 
		
		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		*/
		static void applyCaptive(Unit& unit, int captiveNum);

		/*!
		
		@description

		Used to obtain the captivity status of a unit within the VBS2 environment.
		If a unit's captivity level was set as a Boolean, then the returned number is either 0 (for false) or 1 (for true).

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the captivity status is obtained. The unit should be created before passing it as a parameter.  

		@return int

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The unit's captive number - "+conversions::IntToString(UnitUtilities::getCaptiveNumber(u));	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the captivity status of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getCaptiveNumber(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static int getCaptiveNumber(const Unit& unit);

		/*!
		Check if (and by how much) unit knows about target.
		And returns a number from 0 to 4.
		Deprecated. Use double getKnowsAbout(Unit& unit, ControllableObject& target)
		*/
		VBSFUSION_DPR(UUNT049) static float knowsAbout(Unit& unit, ControllableObject& target);

		/*!
		Types text to the side radio channel.
		Deprecated. Use void applySideChat(Unit& unit, string chatText)
		*/
		VBSFUSION_DPR(UUNT050) static void sideChat(Unit& unit, std::string chatText);

		/*!
		Make a unit send a text message over the group radio channel. 
		Deprecated. Use void applyGroupChat(Unit& unit, string chatText)
		*/
		VBSFUSION_DPR(UUNT051) static void groupChat(Unit& unit, std::string chatText);

		/*!
		
		@description

		Used to apply a name for a unit within the VBS2 Environment.
		Prior to applying the function, it checks whether the name is valid according to following criteria.
			Name has to start with letter or underscore.
			Only special characters allowed is underscore.
			Name should not contain spaces in between.
			Name should not already exist in the mission.

		@locality

		Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the name is set. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		string uname = "u1";

		commander.setName(uname);

		UnitUtilities::applyName(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyName(Unit& unit);


		/*!
		Add an action to an object. The action is usable by the object itself, as well as anyone who is within proximity of the object and is looking at it.

		If an action is added to a unit, the action will only be visible when the unit is on foot. If the unit enters a vehicle, the action will not be visible, not to the unit, or to anyone in / near his vehicle.
		unit - Object to assign the action to
		title - The action name which is displayed in the action menu
		filename - File and Path relative to the mission folder. Called when the action is activated. Depending on the file extension (*.sqf/*.sqs) the file is executed with SQF or SQS Syntax

		It returns Index of the added action. Used to remove the action with removeActionMenuItem()
		Deprecated. Use int createActionMenuItem(Unit &unit, string title, string fileName)
		*/
		VBSFUSION_DPR(UUNT052) static int addActionMenuItem(Unit& unit, std::string title, std::string fileName);

		/*!
		Remove action with given id index. 
		Deprecated. Use void deleteActionMenuItem(Unit& unit, int index)
		*/
		VBSFUSION_DPR(UUNT053) static void removeActionMenuItem(Unit& unit, int index);

		/*!
		
		@description

		Used to obtain the formation leader of a unit within the VBS2 environment. This is most likely to be the group leader, but in some cases the leader would be fifferent for example when a unit is ordered to follow another unit.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the formation leader is obtained. The unit should be created before passing it as a parameter. 

		@return Unit

		@example

		@code

		//The unit to be created
		Unit commander ;
		Unit u1;
		
		u1 = UnitUtilities::getFormLeader(commander);
		displayString+="The name of the form leader is"+u1.getName();	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc) 

		@remarks To obtain the formation leader of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getFormLeader(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		
		*/
		static Unit getFormLeader(const Unit& unit);

		/*!
		
		@description

		Used to obtain the name of a unit�s current primary animation within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the primary animation name is obtained. The unit should be created before passing it as a parameter.  

		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+= " The name of the gunner's primary animation is "UnitUtilities::getAnimationState(u)"\n";

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the primary animation name of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAnimationState(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::string getAnimationState(const Unit& unit);

		/*!
		
		@description

		Used to obtain the leader of the formation within the VBS2 environment.	

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param  unit � The unit for which the formation leader is obtained. The unit should be created before passing it as a parameter. 

		@return Unit

		@example

		@code

		//The unit to be created
		Unit u;
		Unit u2;
		
		u2 = UnitUtilities::getFormationLeader(u);
		displayString+="The name of the formation leader (gunner)is"+u2.getName();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain leader of the formation of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getFormationLeader(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static Unit getFormationLeader(const Unit& unit);

		/*!
		
		@description

		Used to check whether a unit is subgroup leader within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit that is checked. The unit should be created before passing it as a parameter. 

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" Is formation leader - "conversions::BoolToString(UnitUtilities::isFormationLeader(u))"\n";

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isFormationLeader(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool isFormationLeader(const Unit& unit);

		/*!
		
		@description

		Used to return the list of units in the formation within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � A member (unit) in the formation. The unit should be created before passing it as a parameter. 

		@return vector<Unit>

		@example

		@code

		//The unit to be created
		Unit u;
		
		vector<Unit> vu = UnitUtilities::getFormationMembers(u);
		for (vector<Unit>::iterator itr = vu.begin(); itr!= vu.end(); ++itr)
		{ 
			displayString+="The name is "+itr->getName();
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks If a unit created in a client machine within a network is passed as a parameter, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getFormationMembers(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::vector<Unit> getFormationMembers(const Unit& unit);

		/*!
		
		@description

		Used to obtain the direction in degrees of a unit watching in formation within the VBS2 environment. 

		@locality

		Locally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit in the formation. The unit should be created before passing it as a parameter. 

		@return float

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The formation direction is "+conversions::DoubleToString(UnitUtilities::getFormationDirection(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static float getFormationDirection(const Unit& unit);

		/*!
		@description

		This function displays the direction in degrees of the unit watching in formation.

		@locality

		Globally applied 

		@version[VBS3Fusion v3.15]

		@param unit- The unit for which the formation direction is required

		@return float

		@example

		@code

		Unit soldier1;
		displayString+="FormationDirectionEx: "+conversions::FloatToString(UnitUtilities::getFormationDirectionEx(soldier1))

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::getFormationDirection(Unit& unit)

		None

		@remarks- It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		
		@remarks- To obtain the formation Direction of the unit  created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getFormationDirectionEx (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static double getFormationDirectionEx(const Unit& unit);

		/*!
		Reload primary weapon of given unit
		Deprecated. Use void applyPrimaryWeaponReload(Unit& unit)
		*/
		VBSFUSION_DPR(UUNT054) static void reloadPrimaryWeapon(Unit& unit);

		/*!
		
		@description

		Used to return the fill ratio for the current weapon�s magazine i.e. used to find out whether a unit needs to reload its weapons.
		The value ranges from 0 (fully loaded) to 1 (empty). 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit which is checked. The unit should be created before passing it as a parameter.  

		@return float

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The ratio of the unit is "+conversions::FloatToString(UnitUtilities::getNeedReload(u));	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the fill ratio of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getNeedReload (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		
		*/
		static float getNeedReload(const Unit& unit);


		/*!

		@description

		Used to search for a selection in the object model and return the current position of the selection in model space (takes into account any animations).If a selection does not exist, the value [0,0,0] is returned.
		The model is searched in the following order,
		Memory
		Geometry
		FireGeometry
		LandContact
		Hitpoints
		Paths
		SubParts
		ViewGeometry

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit which is passed as a parameter. The unit should be created before passing it as a parameter.  

		@param selectionName � The selection which is searched ,the Possible values for selection are:
			aiming_axis
			aimPoint
			camera
			footstepL
			footstepR
			granat
			granat2
			Head
			head_axis
			lankle
			launcher
			leaning_axis
			LeftFoot
			LeftHand
			LeftHandMiddlel
			lelbow
			lelbow_axis
			lfemur
			lknee
			lknee_axis
			lshoulder
			lwrist
			Neck
			NVG
			Pelvis
			pilot
			rankle
			rearm
			rearm2
			relbow
			relbow_axis
			rfemur
			RightFoot
			RightHand
			RightHandMiddlel
			rknee
			rknee_axis
			rshoulder
			rwrist
			Spine3
			Weapon 

		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The selection position is"+UnitUtilities::getSelectionPosition(u,"LeftFoot").getVBSPosition();

		@endcode

		@overloaded

		UnitUtilities::getSelectionPosition(Unit& unit, string selectionName, string method);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the position of a selection for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getSelectionPosition(Unit& unit, string selectionName)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getSelectionPosition(const Unit& unit, const std::string& selectionName);

		/*!

		@description

		Used to search for a selection in the object model and return the current position of the selection in model space (takes into account any animations).If a selection does not exist, the value [0,0,0] is returned.
		The model is searched in the following order:
		Memory
		Geometry
		FireGeometry
		LandContact
		Hitpoints
		Paths
		SubParts
		ViewGeometry

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit which is passed as a parameter. The unit should be created before passing it as a parameter. 

		@param selectionName � The Possible values for selection are,
			aiming_axis
			aimPoint
			camera
			footstepL
			footstepR
			granat
			granat2
			Head
			head_axis
			lankle
			launcher
			leaning_axis
			LeftFoot
			LeftHand
			LeftHandMiddlel
			lelbow
			lelbow_axis
			lfemur
			lknee
			lknee_axis
			lshoulder
			lwrist
			Neck
			NVG
			Pelvis
			pilot
			rankle
			rearm
			rearm2
			relbow
			relbow_axis
			rfemur
			RightFoot
			RightHand
			RightHandMiddlel
			rknee
			rknee_axis
			rshoulder
			rwrist
			Spine3
			Weapon 

		@param 	method � The method to calculate the selection center. Possible values are,
		"average": Arithmetic average of all vertices in selection
		"bbox_center": Geometry center of the axis-aligned bounding box containing all vertices from selection.
		"allpoints": All points contained in the specific selection.
		
		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="The selection position is"+UnitUtilities::getSelectionPosition(u,"LeftFoot","average").getVBSPosition();

		@endcode

		@overloaded

		UnitUtilities::getSelectionPosition(Unit& unit, string selectionName);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the position of a selection for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getSelectionPosition(Unit& unit, string selectionName, string method).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getSelectionPosition(const Unit& unit, const std::string& selectionName, const std::string& method);


		/*!

		@description

		Used to obtain the speed for a given speed mode of a unit within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the speed is obtained. The unit should be created before passing it as a parameter. 

		@param speedMode � The possible values of speed mode are 
		"AUTO"
		"SLOW"
		"NORMAL"
		"FAST" 

		@return float

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The speed is"+conversions::FloatToString(UnitUtilities::getSpeed(commander,"AUTO"))

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc); 

		@remarks To obtain the speed of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getSpeed(Unit& unit, string speedMode)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static float getSpeed(const Unit& unit, const std::string& speedMode);

		/*!

		@description
		
		Used to obtain the speed for the given speed mode.

		@locality

		Globally applied 

		@version[VBS3Fusion v3.15]

		@param  unit- The unit for which the speed is obtained. 
		
		@param speedMode- The speed modes are :"AUTO" , �SLOW", "NORMAL", "FAST"

		@return double

		@example 

		@code

		Unit soldier1;
		displayString+="SpeedEx: "+ conversions::DoubleToString(UnitUtilities::getSpeedEx(soldier1,("FAST")));
		
		@end code

		@related

		None

		@overloaded

		None
		
		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		@remarks - To obtain the speedEx of the unit  created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getSpeedEx(Unit& unit, string speedMode).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static double getSpeedEx(const Unit& unit, const std::string& speedMode);

		/*!
		
		@description

		Used to obtain the target assigned to a unit within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the target is obtained. The unit should be created before passing it as a parameter.  

		@return ControllableObject

		@example

		@code

		//The unit and object to be created
		Unit u;
		ControllableObject co;
		
		co= UnitUtilities::getAssignedTarget(commander);
		displayString+="The name of the assigned target is "+co.getName();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the target of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAssignedTarget(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static ControllableObject getAssignedTarget(const Unit& unit);

		/*!
		Stop AI unit.Stopped unit will not be able to move, fire, or change
		its orientation to follow a watched object.

		Deprecated. Use void applyStop(Unit& unit, bool stop)
		*/
		VBSFUSION_DPR(UUNT055) static void UnitUtilities::stop(Unit& unit, bool stop);

		/*!
		void ControllableObjectUtilities::applyExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on person, smooth transition to given move will be done.

		Deprecated. Use void applyMovement(Unit &unit, UNITMOVE move) 
		*/
		VBSFUSION_DPR(UUNT057) static void playMove(Unit& unit, UNITMOVE move);

		/*!
		void ControllableObjectUtilities::applyExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on a person, the given move is started immediately (there is no transition). Use parameter 'moveName' as "" to switch back to the default movement if there is no transition back, otherwise the person may be stuck. 

		Deprecated. Use void applySwitchMove(Unit &unit, UNITMOVE move) 
		*/
		VBSFUSION_DPR(UUNT059) static void switchMove(Unit& unit, UNITMOVE move);

		/*!
		void ControllableObjectUtilities::applyExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on person, smooth transition to given move will be done. 
		moveName : It can be one of the following
		AmovPercMstpSnonWnonDnon_coughing_v1
		AmovPercMstpSnonWnonDnon_coughing_v2
		AmovPercMstpSnonWnonDnon_coughing_v3
		AmovPercMstpSnonWnonDnon_coughing_v4
		AmovPercMstpSnonWnonDnon_coughing_v5
		AmovPercMstpSnonWnonDnon_CheckSix_v1
		AmovPercMstpSnonWnonDnon_CheckSix_v2
		AmovPercMstpSnonWnonDnon_CheckSix_v3
		AmovPercMstpSnonWnonDnon_CheckSix_v4
		AmovPercMstpSnonWnonDnon_ComeHere
		AmovPercMstpSnonWnonDnon_Contempt
		AmovPercMstpSnonWnonDnon_ContemptSlap
		AmovPercMstpSnonWnonDnon_GoAway
		AmovPercMstpSnonWnonDnon_Greeting
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v1
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v2
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v3
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v4
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v5
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_short
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_1
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_2
		AmovPercMstpSnonWnonDnon_teargas_v1
		AmovPercMstpSnonWnonDnon_teargas_v2
		AmovPercMstpSnonWnonDnon_teargas_v3
		AmovPercMstpSnonWnonDnon_Wait
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v1
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v2
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v3
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v4
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v5
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v1
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v2

		Deprecated. Use void applyMovement(Unit& unit, string moveName) 
		*/
		VBSFUSION_DPR(UUNT056) static void playMove(Unit& unit, std::string moveName);

		/*!
		void ControllableObjectUtilities::applyExternalControl(ControllableObject& co, bool control) should be call with parameter control as 'true'.
		When used on a person, the given move is started immediately (there is no transition). Use parameter 'moveName' as "" to switch back to the default movement if there is no transition back, otherwise the person may be stuck. 

		moveName : It can be one of the following
		AmovPercMstpSnonWnonDnon_coughing_v1
		AmovPercMstpSnonWnonDnon_coughing_v2
		AmovPercMstpSnonWnonDnon_coughing_v3
		AmovPercMstpSnonWnonDnon_coughing_v4
		AmovPercMstpSnonWnonDnon_coughing_v5
		AmovPercMstpSnonWnonDnon_CheckSix_v1
		AmovPercMstpSnonWnonDnon_CheckSix_v2
		AmovPercMstpSnonWnonDnon_CheckSix_v3
		AmovPercMstpSnonWnonDnon_CheckSix_v4
		AmovPercMstpSnonWnonDnon_ComeHere
		AmovPercMstpSnonWnonDnon_Contempt
		AmovPercMstpSnonWnonDnon_ContemptSlap
		AmovPercMstpSnonWnonDnon_GoAway
		AmovPercMstpSnonWnonDnon_Greeting
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v1
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v2
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v3
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v4
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v5
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_short
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_1
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_2
		AmovPercMstpSnonWnonDnon_teargas_v1
		AmovPercMstpSnonWnonDnon_teargas_v2
		AmovPercMstpSnonWnonDnon_teargas_v3
		AmovPercMstpSnonWnonDnon_Wait
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v1
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v2
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v3
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v4
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v5
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v1
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v2

		Deprecated. Use void applySwitchMove(Unit& unit, string moveName)
		*/
		VBSFUSION_DPR(UUNT058) static void switchMove(Unit& unit, std::string moveName);

		/*!
		
		@description

		Used to check if an AI unit is allowed to reload its weapon within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the ability to reload is checked. The unit should be created before passing it as a parameter. 

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+= "The values is "+conversions::BoolToString(UnitUtilities::isReloadEnabled(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check the ability of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isReloadEnabled(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool isReloadEnabled(const Unit& unit);


		/*!
		
		@description

		Used to attach a statement to a unit within the VBS2 Environment. The statement is propagated over the network in MP games, it can be executed by invoking processInitCommands.

		@locality

		Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the statement is attached. The unit should be created before passing it as a parameter.  

		@param statement - The statement attached to the unit.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		String message ="hi";
		
		UnitUtilities::setInitStatement(u, message);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To attach a statement to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: setInitStatement(Unit& unit, string& statement).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void setInitStatement(Unit& unit, const std::string& statement) ;

		/*!
		
		@description

		Used to clear the unit�s init statement of a unit within the VBS2 Environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the init statement is cleared. The unit should be created before passing it as a parameter.  

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::clearInitStatement(u);

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::setInitStatement(Unit& unit, string& statement);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc); 

		@remarks To clear the statement of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: clearInitStatement(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void clearInitStatement(Unit& unit);


		/*!
		
		@description

		Used to obtain the name of a unit that was applied by the function applyIdentity() or that was set randomly by the game engine.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the name is obtained. The unit should be created before passing it as a parameter.  

		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The name is "+UnitUtilities::getName(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the name of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getName(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::string getName(const Unit& unit);

		/*!
		
		@description

		Used to check if a unit is captive within the VBS2 environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit that is checked. The unit should be created before passing it as a parameter.  

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The unit is captive - "+conversions::BoolToString(UnitUtilities::isCaptive(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks If a unit is captive, the enemies will not shoot at the unit. 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 
		
		@remarks To check if a unit created in a client machine within a network is captive, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isCaptive(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		*/
		static bool isCaptive(const Unit& unit);

		/*!
		Set if the units given in the list are allowed to enter vehicles. 

		Deprecated. Use void applyAllowGetIn(vector<Unit> unitList, bool val)
		*/
		VBSFUSION_DPR(UUNT060) static void allowGetIn(std::vector<Unit> unitList, bool val);

		/*!
		
		@description

		Used to obtain the binocular weapon name of a unit within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the binocular weapon name is obtained. The unit should be created before passing it as a parameter. 

		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The Binocular name is "+UnitUtilities::getBinocularWeaponName(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks All units do not possess binoculars. If the function was applied to a unit that does not have a binocular, an empty string would be returned.

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 
		
		@remarks To obtain the name of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getName(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)  
		
		*/
		static std::string getBinocularWeaponName(const Unit& unit);

		/*!
		
		@description

		Used to obtain the expected destination position of  a unit within the VBS2 environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param  unit � The unit for which the destination position is obtained. The unit should be created before passing it as a parameter. 

		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The expected destination position is "+UnitUtilities::getExpectedDestinationPos(u).getVBSPosition()

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the expected destination position of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getExpectedDestinationPos(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information) 
		
		*/
		static position3D getExpectedDestinationPos(const Unit& unit);

		/*!
		
		@description

		Used to obtain the expected destination planning mode of a unit within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the destination planning mode is obtained. The unit should be created before passing it as a parameter. 

		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The expected destination planning mode"+UnitUtilities::getExpectedDestinationPlanMode(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the expected destination planning mode of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getExpectedDestinationPlanMode(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::string getExpectedDestinationPlanMode(const Unit& unit);

		/*!
		
		@description

		Used to check whether a unit�s destination was forcefully changed.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit that is checked for forceful destination changes. The unit should be created before passing it as a parameter.  

		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The destination re planning was forced is "+conversions::BoolToString(UnitUtilities::getExpectedDestinationForceReplan(u));	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getExpectedDestinationForceReplan(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool getExpectedDestinationForceReplan(const Unit& unit);

		/*!
		
		@description

		Used to obtain the position of a unit in the formation within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the position is obtained. The unit should be created before passing it as a parameter. 

		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The position of the unit in the formation is - "+UnitUtilities::getFormationPosition(gunner).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the position of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getFormationPosition(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static position3D getFormationPosition(const Unit& unit);

		/*!
		
		@description

		Used to obtain the current command mode of a unit within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the current command mode is obtained. The unit should be created before passing it as a parameter.  

		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The current command type is - "+UnitUtilities::getCurrentCommand(u);	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Possible return values are "WAIT","ATTACK","HIDE","MOVE","HEAL","REPAIR","REFUEL","REARM","SUPPORT","JOIN","GET IN","FIRE","GET OUT","STOP","EXPECT","ACTION","ATTACKFIRE" and if there is no command applied to the unit, an empty string is returned. 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks To obtain the current command mode of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getCurrentCommand(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static std::string getCurrentCommand(const Unit& unit);

		/*!
		Unassigns a unit from whichever vehicle that unit is currently assigned to. 
		If the unit is currently in that vehicle, the group leader will issue an order to disembark. 

		Deprecated. Use void applyVehicleUnassign(Unit& unit)
		*/
		VBSFUSION_DPR(UUNT061) static void unAssignVehicle(Unit& unit);

		/*!
		 Hides the body of the given person. 

		Deprecated. Use void applyBodyHiding(Unit& unit)
		*/
		VBSFUSION_DPR(UUNT062)static void hideBody(Unit& unit);
		
		/*!
		
		@description

		Used to obtain the group leader  of  a unit within the VBS2 environment. If applied to dead units objNull is returned.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit for which the group leader is obtained. The unit should be created before passing it as a parameter.

		@return Unit

		@example

		@code

		//The units to be created
		Unit commander, u2;
		
		u2= UnitUtilities::getGroupLeader(commander);

		displayString+="The leader is "+u2.getName();	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks If the function is applied to an individual unit (i.e has no leader, not in a group), the function would return the unit�s name/alias. 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the group leader of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getGroupLeader(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static Unit getGroupLeader(const Unit& unit);

		/*!
		
		@description

		Used to obtain all the units in the group of a given unit within the VBS2 environment. If applied to a dead unit an empty array is returned.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit through which the fellow members are obtained. The unit should be created before passing it as a parameter. 

		@return vector<Unit>

		@example

		@code

		//The unit to be created
		Unit u;

		vector<Unit> vu = UnitUtilities::getGroupUnits(u);
		for (vector<Unit>::iterator itr = vu.begin(); itr!= vu.end(); ++itr)
		{
			displayString+="The name is "+itr->getName();
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain all the units in the group of a given unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getGroupUnits(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static std::vector<Unit> getGroupUnits(const Unit& unit);

		/*!
		
		@description

		Used to check if a unit is stopped by the stop function within the VBS2 environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit that is checked. The unit should be created before passing it as a parameter.  

		@return bool

		@example

		@code

		//The unit to be created
		Unit u; 
		
		displayString+=" The unit is stopped - "+conversions::BoolToString(UnitUtilities::isStopped(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check if a unit created in a client machine within a network is stopped, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isStopped(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool isStopped(const Unit& unit);

		/*!
		
		@description

		Used to check if a unit is ready. A unit is busy when it is given some command like move, until the command is finished within the VBS2 environment. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.02]

		@param unit � The unit that is checked. The unit should be created before passing it as a parameter.  

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" The unit is ready - "+conversions::BoolToString(UnitUtilities::isReady(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check if a unit created in a client machine within a network is ready, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isReady(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		
		*/
		static bool isReady(const Unit& unit);

		/*!
		@description

		This function returns true if a unit is playable within the VBS2 environment and otherwise.

		@locality

		Globally applied 

		@version [VBS3Fusion v3.15]

		@param unit- The unit which is checked whether it is playable or not.  

		@return bool

		@example

		@code

		//The unit to be created

		Unit soldier1;
		displayString+="is playable: "+conversions::BoolToString(UnitUtilities::isPlayable(soldier1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  

		@remarks - To obtain the playable status of the unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::isPlayable((Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static bool isPlayable(const Unit& unit);

		/*!
		@description

		This function enables a smooth transition of the unit for a given action.

		@locality

		Locally applied Globally effected

		@version [VBS3Fusion v3.15]

		@param unit - The unit for which the action is applied.

		@param action � The action to be applied for the unit.

		@return Nothing

		@example

		@code

		//The unit to be created

		Unit soldier1;
		UnitUtilities::playAction(soldier1,string("SitDown"));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks - To apply action for the unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::playAction (Unit& unit, string& action).
		
		*/
		static void playAction(Unit& unit, const std::string& action);

		/*!
		@description
		
		This function enables an immediate action when used on an unit (there is no transition). 

		@locality

		Globally effected

		@version [VBS3Fusion v3.15]

		@param unit- The unit for which the action is applied.

		@param action- The immediate action which is applied on the unit.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit soldier1;
		UnitUtilities::switchAction(soldier1,string("MoveForward"));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  

		@remarks - To switch an action for the unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::switchAction(Unit& unit, string& action).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void switchAction(Unit& unit, const std::string& action);

		/*!
		@description

		It sets a forced walk to the unit within the VBS3 Environment.

		@locality

		Locally effected

		@version[VBS3Fusion v3.15]

		@param unit - The unit for which the above function is applied.

		@param boolVal � A Boolean value (true - does not allow the player to run and false -  allows the player to run).

		@return

		@example

		@code

		//The unit to be created
		Unit soldier1;
		
		UnitUtilities::setForceWalk(playerUnit,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - The above function can be used only on the playerUnit.

		*/
		static void setForceWalk(Unit& unit,bool boolVal);

		/*!

		@description

		It returns a true or false value indicating whether a unit  is forced to Walk or not.

		@locality

		Globally applied

		@version [VBS3Fusion v3.15]

		@param unit- The unit for which the above function is applied.

		@return bool

		@example

		@code

		//The unit to be created
		Unit soldier1;
		displayString+="forced walk: "+conversions::BoolToString(UnitUtilities::isForcedWalk(soldier1));

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::setForceWalk(Unit& unit,bool boolVal) 

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  

		@remarks - To check if the unit created in a client machine within a network is ForcedWalk, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::isForcedWalk(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static bool isForcedWalk(const Unit& unit);

		/*!
		@description 
		
		This function displays the muzzle of unit's weapon within the VBS3 environment.

		@locality

		Globally applied

		@version [VBS3Fusion v3.15]

		@param unit - The unit for which the current muzzle is required.

		@return string

		@example

		@code

		//The unit to be created
		Unit soldier1;
		displayString+="muzzle:  "+ UnitUtilities::getCurrentMuzzle(soldier1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		
		@remarks - To obtain the muzzle of the unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getCurrentMuzzle(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static std::string getCurrentMuzzle(const Unit& unit);

		/*!
		@description

		This function displays the current weapon mode of the unit within the VBS3 Environment.
		
		@locality

		Globally applied

		@version [VBS3Fusion v3.15]

		@param unit- The unit for which the current weapon mode is required.

		@return string

		@example

		@code

		//The unit to be created
		Unit soldier1;
		displayString+="weapon mode"+ UnitUtilities::getCurrentWeaponMode(soldier1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		
		@remarks - To obtain the weapon mode of the unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getCurrentWeaponMode(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static std::string getCurrentWeaponMode(const Unit& unit);

		/*!
		@description

		Displays the life state of a unit within the VBS3 Environment.

		@locality

		Globally applied

		@version [VBS3Fusion v3.15]

		@param unit- The unit for which the life state is obtained.

		@return string

		@example

		@code

		//The unit to be created
		Unit soldier1;
		displayString+="life state: "+ UnitUtilities::getLifeState(soldier1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		
		@remarks - To obtain the life status of the unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getLifeState(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static std::string getLifeState(const Unit& unit);

		/*!
		@description

		Removes all weapons from the unit cargo space.

		@locality

		Gobally applied Globally effected

		@version [VBS3Fusion v3.15]

		@param unit- The unit for which the above function is applied.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit soldier1;
		UnitUtilities::clearWeaponCargoGlobal(soldier1);

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::addWeaponToCargoGlobal(Unit& unit, Weapon& weapon, int count)

		None

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		@remarks - To clear the weaponCargoGlobal  of the unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::clearWeaponCargoGlobal(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void clearWeaponCargoGlobal(Unit& unit);

		/*!
		@description

		Adds weapons to the weapon cargo space. This is used for infantry weapons.
		
		@locality

		Globally applied Globally effected

		@version [VBS3Fusion v3.15]

		@param unit- The unit for which the weapons are added.
		
		@param weapon- The name of the weapon to be added.

		@param count- Number of weapons to be added.

		@return Nothing.

		@example

		@code

		//The unit to be created
		Unit soldier1;
		Weapon wp;
		wp.setWeaponName("M9");
		UnitUtilities::addWeaponToCargoGlobal(soldier1,wp,5);

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::clearWeaponCargoGlobal(Unit& unit).

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		@remarks - To add weaponToCargoGlobal of  a unit  created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::addWeaponToCargoGlobal(Unit& unit, Weapon& weapon, int count).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void addWeaponToCargoGlobal(Unit& unit, const Weapon& weapon, int count);

		/*!
		@description

		Used to add magazines to the weapon cargo space. This is used for infantry weapons.

		@locality

		Globally applied Globally effected

		@version [VBS3Fusion v3.15]

		@param unit- The unit for which the magazine is added.

		@param magName- The class name of the magazine.

		@param count- The number of magazines to be added.

		@return Nothing.

		@example

		@code

		//The unit to be created
		Unit soldier1;
		UnitUtilities::addMagazineToCargoGlobal(soldier1,("VBS32_mag_30rnd_556x45_Ball_steyr_aug"),5);

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::clearMagazineCargoGlobal(Unit& unit);

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  

		@remarks - To add magazine to cargoGlobal of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::addMagazineToCargoGlobal(Unit& unit, string magName, int count).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void addMagazineToCargoGlobal(Unit& unit, const std::string& magName, int count);

		/*!
		@description
		
		Used to remove all magazines from the unit cargo space. 

		@locality

		Globally applied Globally effected

		@version [VBS3Fusion v3.15]

		@param unit- The unit for which the above function is applied.  

		@return Nothing.

		@example

		@code

		//The unit to be created
		Unit soldier1;
		UnitUtilities::clearMagazineCargoGlobal(soldier1);

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::addMagazineToCargoGlobal(Unit& unit, string magName, int count);

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		@remarks - To clear the magazines in the cargo of the unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::clearMagazineCargoGlobal(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void clearMagazineCargoGlobal(Unit& unit);

		/*!
		Set if leader can issue attack commands to the soldiers in his group

		Deprecated. Use void applyAttackEnable(Unit& unit, bool boolVal)
		*/
		VBSFUSION_DPR(UUNT064) static void setEnableAttack(Unit& unit, bool boolVal);

		/*!
		
		@description

		Used to return a list of all the kills caused by a player unit during a MP session. The return value is a Nested array, with one sub-array for each death, containing the following data:
		Time of kill (seconds from mission start)
		[name unit that was killed, position, side, unit type]
		[name of unit which caused the death, position, side, unit type] (identical to dead unit, if self-inflicted - e.g. fall or crash)
		Distance between dead unit and killer 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.03]

		@param unit � The unit for which the list is obtained. The unit should be created before passing it as a parameter.  

		@return vector<KILLS_INFO>

		@example

		@code

		//The unit to be created
		Unit u; 

		vector<KILLS_INFO> vki = UnitUtilities::getAllKilledByInfo(u);
		for (vector<KILLS_INFO>::iterator itr = vki.begin(); itr!= vki.end(); ++itr)
		{ 
			displayString+="The name is "+itr->killedUnitInfo.name; 
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the list for a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAllKilledByInfo(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information) 

		*/
		static std::vector<KILLS_INFO> getAllKilledByInfo(const Unit& unit);

		/*!
		
		@description

		Used to check whether a group's leader can issue attack commands to soldiers under his command within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.03]

		@param unit � The unit that is checked. The unit should be created before passing it as a parameter.  

		@return bool

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+=" attack is enabled is- "+conversions::BoolToString(UnitUtilities::isAttackEnabled(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To check for a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isAttackEnabled(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool isAttackEnabled(const Unit& unit);

		/*!

		@description

		Used to return a list of all the deaths that occurred to a player unit during a MP session.The return value is a nested array, with one sub-array for each death, containing the following data:
		Time of death (seconds from mission start)
		[name of unit that died, position, side, unit type]
		[name of unit which caused the death, position, side, unit type] (identical to dead unit, if self-inflicted - e.g. fall or crash)
		Distance between dead unit and killer

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.03]

		@param unit � The unit for which the list is obtained. The unit should be created before passing it as a parameter.  

		@return vector<KILLS_INFO>

		@example

		@code

		//The unit to be created
		Unit u,player;

		vector<KILLS_INFO> myList2 = UnitUtilities::getAllDeathInfo(player); 
		for (vector<KILLS_INFO>::iterator itr = myList2.begin(); itr!= myList2.end(); ++itr)
		{
			displayString+="The name is "+itr->killedUnitInfo.name;
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the list for a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAllDeathInfo(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::vector<KILLS_INFO> getAllDeathInfo(const Unit& unit);

		/*!

		@description

		Used to return a list of all injuries a player unit has received during a MP session.The return value is a Nested array, with one sub-array for each individual injury containing the following data:
		Time of injury (seconds from mission start)
		[name of injured unit, position, side, unit type]
		[name of unit which inflicted the injury, position, side, unit type] (identical to injured unit, if self-inflicted - e.g. fall or crash)
		Amount of damage inflicted
		Ammo type that inflicted the damage (empty, in case of fall or crash)

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.03]

		@param unit � The unit for which the list is obtained. The unit should be created before passing it as a parameter.  

		@return vector<INJURY_INFO>

		@example

		@code

		//The unit to be created
		Unit u,player;
		
		vector<INJURY_INFO> VII = UnitUtilities::getAllInjuriesInfo(player);
		for (vector<INJURY_INFO>::iterator itr = VII.begin(); itr!= VII.end(); ++itr)
		{
			displayString+="The name is "+itr->attackUnitInfo.name;
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the list for a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAllInjuriesInfo(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		
		*/
		static std::vector<INJURY_INFO> getAllInjuriesInfo(const Unit& unit);

		/*!
		The unit will play the given sound. If the unit is a person, it will also perform the corresponding
		lipsync effect, provided an appropriate .lip file has been created for this sound. The sound is 
		defined in the description.ext file.
		
		Deprecated. Use void applySpeech(Unit& unit, string& speechName)
		*/
		VBSFUSION_DPR(UUNT065) static void say(Unit& unit, std::string& speechName);

		/*!
		The unit will play the given sound. If the unit is a person, it will also perform the corresponding 
		lipsync effect. If the camera is not withing the given range, the title is not shown. The sound is 
		defined in the description.ext file.

		Deprecated. Use void applySpeech(Unit& unit, string& speechName, double maxTitlesDistance)
		*/
		VBSFUSION_DPR(UUNT066) static void say(Unit& unit, std::string& speechName, double maxTitlesDistance);

		/*!
		Locks unit into current location. Player can still change stance, aim and perform other actions, but not move.
		If applied to AI units, they will still go through movement animations, but without actually moving. 

		Deprecated. Use void applyAnimationMoveDisable(Unit& unit, bool disable)
		*/
		VBSFUSION_DPR(UUNT067) static void setDisableAnimationMove(Unit& unit, bool disable);

		/*!
		
		@description

		Used to check whether a movement has been disabled for a unit within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.03]

		@param unit � The unit for which the movement disability is checked. The unit should be created before passing it as a parameter. 

		@return bool

		@example

		@code

		//The unit to be created
		Unit u; 
		
		displayString+=" disabled animation move is- "+conversions::BoolToString(UnitUtilities::getDisableAnimationMove(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check for a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getDisableAnimationMove(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool getDisableAnimationMove(const Unit& unit);

		/*!
		Send the message to the side radio channel.
		Message is defined in description.ext or CfgRadio. 

		Deprecated. Use void applySideRadioMessage(Unit& unit, string& messageName)
		*/
		VBSFUSION_DPR(UUNT075) static void sendSideRadioMessage(Unit& unit, std::string& messageName);

		/*!
		Make a unit send a message over the group radio channel.
		Message is defined in description.ext or CfgRadio.
		
		Deprecated. Use void applyGroupRadioMessage(Unit& unit, string& messageName)
		*/
		VBSFUSION_DPR(UUNT068) static void sendGroupRadioMessage(Unit& unit, std::string& messageName);

		/*!
		Make a unit send a message over the global radio channel.
		Message is defined in description.ext or CfgRadio.

		Deprecated. Use void applyGlobalRadioMessage(Unit& unit, string& radioName)
		*/
		VBSFUSION_DPR(UUNT069) static void sendGlobalRadioMessage(Unit& unit, std::string& radioName);

		/*!
		Order the given unit to target the given target (via the radio). 

		Deprecated. Use void applyCommandTarget(vector<Unit>& unitList, Unit& target)
		*/
		VBSFUSION_DPR(UUNT070) static void commandTarget(std::vector<Unit>& unitList, Unit& target);

		/*!
		@description

		Orders the given unit(s) to follow the given other unit (via the radio).All involved units must belong to the same group.

		@locality

		Globally applied 

		@version [VBS3Fusion v3.15]

		@param followerList- The list of units to be followed.

		@param leader- The unit which directs the other units.

		@return Nothing

		@example

		@code

		//The units to be created
		Unit u1,u2;
		vector<Unit> vu;
		vu.push_back(u1);
		vu.push_back(u2);
		UnitUtilities::commandFollow(vu,player);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		
		@remarks - To order units to follow the leader created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function unitUtilities:: createAndMoveInAndAsDriver(Vehicle& vehicle, Unit& unit).(Refer (MissionUtilities::loadMission(Mission& mission)for further information
		*/
		static void commandFollow(const std::vector<Unit>& followerList, Unit& leader);

		/*!
		Sets the preference of AI to cross through buildings, when trying to reach their destination.
		The higher the number (0-1), the less likely AI will use buildings during their pathplanning.
		At 1 it will never use buildings.

		Deprecated. Use void applyAIPathwayCost(Unit& AIunit, double cost)
		*/
		VBSFUSION_DPR(UUNT071) static void setAIPathwayCost(Unit& AIunit, double cost);

		/*!
		
		@description

		Used to obtain AI's avoidance of buildings during pathfinding.The return values are between 0 and 1 (1: never use buildings).

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.03]

		@param AIunit � The unit for which the value is obtained. The unit should be created before passing it as a parameter.  

		@return double

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" pathway cost is- "+conversions::DoubleToString(UnitUtilities::getAIPathwayCost(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the valuefor a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getAIPathwayCost (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getAIPathwayCost(const Unit& AIunit);

		/*!
		Sets rotation rate when unit is colliding.
		*/
		static void setSlideOnPush(Unit& unit, double rotationRate);

		/*!
		
		@description

		Used to search for a selection in the object model (Fire Geo only) and return the center in model space.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.03]

		@param unit � The unit for which the center is obtained. The unit should be created before passing it as a parameter.

		@param selectionName - The selection which is searched ,the Possible values for selection are,
			aiming_axis
			aimPoint
			camera
			footstepL
			footstepR
			granat
			granat2
			Head
			head_axis
			lankle
			launcher
			leaning_axis
			LeftFoot
			LeftHand
			LeftHandMiddlel
			lelbow
			lelbow_axis
			lfemur
			lknee
			lknee_axis
			lshoulder
			lwrist
			Neck
			NVG
			Pelvis
			pilot
			rankle
			rearm
			rearm2
			relbow
			relbow_axis
			rfemur
			RightFoot
			RightHand
			RightHandMiddlel
			rknee
			rknee_axis
			rshoulder
			rwrist
			Spine3
			Weapon

		@return position3D

		@example

		@code

		//The unit to be created
		Unit u;
		string selection ="LeftFoot";
		
		displayString+=" The center in model space is - "+UnitUtilities::getSelectionCenter(u,selection).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the centre for a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getSelectionCenter(Unit& unit,string& selectionName).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getSelectionCenter(const Unit& unit, const std::string& selectionName);

		/*!
		Unselect a unit in the players group (in the UI command bar). Only valid if the player is commander of a group. 

		Deprecated. Use void applyUnselectFromGroup(Unit& unit)
		*/
		VBSFUSION_DPR(UUNT063) static void unselectUnitInGroup(Unit& unit);

		/*!

		@description

		It displays the vision mode of unit's weapon within the VBS3 Environment. 0-dayTime, 1-night vision, 2-FLIR.
		
		@locality

		Globally applied

		@version

		@param unit- The unit for which the current vision mode is obtained.

		@return int

		@example

		@code

		//The unit to be created

		Unit soldier1;
		displayString="vision mode"+ conversions::IntToString(UnitUtilities::getCurrentVisionMode(soldier1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		@remarks - To obtain the vision mode of the unit  created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getCurrentVisionMode(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
	
		*/
		static int getCurrentVisionMode (const Unit& unit);


		/*!
		
		@description

		Used to apply the training rating of a unit within the VBS2 environment.

		@locality

		Globally applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.04]

		@param unit � The unit for which the training rating is applied. The unit should be created before passing it as a parameter.
		
		@param rating - Rating level to be applied.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyTraining(u,45);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To set the training rating of a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyTraining(Unit& unit, double rating)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		*/
		static void applyTraining(Unit& unit, double rating);

		/*!
		
		@description

		Used to obtain the fleeing mode of the unit within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.04]

		@param unit � The unit for which the fleeing mode is obtained. The unit should be created before passing it as a parameter. 

		@return double

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" Fleeing is - "+conversions::DoubleToString(UnitUtilities::getFleeing(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the fleeing mode of a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getFleeing(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getFleeing(const Unit& unit);

		/*!

		@description

		Used to obtain the combat mode of the unit within the VBS2 environment.The possible values for the modes are:
		"BLUE" (Never fire)
		"GREEN" (Hold fire - defend only)
		"WHITE" (Hold fire, engage at will)
		"YELLOW" (Fire at will)
		"RED" (Fire at will, engage at will) 
		"UNIT ERROR" (if a group was passed or the combat mode couldn't be retrieved)

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.04]

		@param unit � The unit for which the combat mode is obtained. The unit should be created before passing it as a parameter.

		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+=" unit combat mode is- "+UnitUtilities::getUnitCombatMode(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the combat mode of a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getUnitCombatMode(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::string getUnitCombatMode(const Unit& unit);

		/*!

		@description

		Used to reveal a unit to a group or another unit within the VBS2 environment.
		The knowledge value will be set to the highest level any unit of the revealing side has about the revealed unit. If the revealing side has no knowledge about the revealed unit, the value will be set to 1. 

		@locality

		Globally applied and Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.04]

		@param revealingUnit - The unit which receives revealing information.

		@param revealedUnit - The unit which is revealed.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u,u2; 

		UnitUtilities::applyReveal(u,u2);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks  It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To reveal a  unit created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyReveal(Unit& revealingUnit, Unit& revealedUnit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyReveal(const Unit& revealingUnit, const Unit& revealedUnit);

		/*!

		@description

		Used to set the skill of a unit within the VBS2 environment. The skill may vary from 0.2 to 1.0.

		@locality

		Locally applied and Globally effected 

		@version [VBS2 2.02 / VBS2Fusion v3.04]

		@param unit � The unit for which the skill is applied. The unit should be created before passing it as a parameter. 

		@param skill - Skill level to be applied.
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyUnitAbility(u,0.6);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To set the skill of a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyUnitAbility(Unit& unit, double skill)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyUnitAbility(Unit& unit, double skill);

		/*!

		@description

		Used to obtain the behaviour of a unit within the VBS2 environment. The possible return values are "CARELESS", "SAFE", "AWARE", "COMBAT" and "STEALTH".

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.04]

		@param unit � The unit for which the behavior is obtained. The unit should be created before passing it as a parameter.

		@return String

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+=" unit's behavior is- "+UnitUtilities::getUnitBehaviour(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To obtain the behavior of a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getUnitBehaviour(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static std::string getUnitBehaviour(const Unit& unit);

		/*!
		Find the nearest enemy to the specified position. 
		Returns a null object if the object's group does not know about any enemies.

		unit - The unit which is used to select the side of the enemy.
		positionUnit - This unit's position is used to select the nearest enemy unit from that position

		Deprecated. Use Unit getNearestEnemy(Unit& unit, Unit& positionUnit)
		*/
		VBSFUSION_DPR(UUNT073) static Unit findNearestEnemy(Unit& unit, Unit& positionUnit);

		/*!
		Find the nearest enemy to the specified position. 
		Returns a null object if the object's group does not know about any enemies.

		unit - The unit which is used to select the side of the enemy.
		position - This position is used to select the nearest enemy unit from that position

		Deprecated. Use Unit getNearestEnemy(Unit& unit, position3D position)
		*/
		VBSFUSION_DPR(UUNT074) static Unit findNearestEnemy(Unit& unit, position3D position);

		/*!

		@description

		Used to obtain the precision of the unit within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.05]

		@param  unit � The unit for which the precision is obtained. The unit should be created before passing it as a parameter.

		@return double

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+=" Precision is - "+conversions::DoubleToString(UnitUtilities::getPrecision(u));

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::updateRank(Unit& u);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the precision of a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getPrecision(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static double getPrecision(const Unit& unit);

		/*!

		@description

		Used to check if a unit is fleeing within the VBS2 environment. Returns true if a unit is fleeing, false otherwise. Dead or empty units will also return false.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.04]

		@param unit � The unit which is checked. The unit should be created before passing it as a parameter.

		@return bool

		@example

		@code

		//The unit to be created
		Unit u; 

		displayString+=" Unit is fleeing - "+conversions::BoolToString(UnitUtilities::isFleeing(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check for a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isFleeing(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static bool isFleeing(const Unit& unit);

		/*!
		Sets the cowardice level (the lack of courage or bravery) of a unit.By setting fleeing to 0.75, 
		units have a 75% chance of fleeing (and 25% of not fleeing). Once they have decided not to flee, they will never flee.
		0 means maximum courage, while 1 means always fleeing.V1.50+: If allowFleeing is set to 0 for a unit that is already fleeing, 
		that unit will stop. (In previous versions changing allowFleeing in a situation like this would not have had any effect.)

		Deprecated. Use void applyFleeing(Unit& unit, double cowardice)
		*/
		VBSFUSION_DPR(UUNT072) static void allowFleeing(Unit& unit, double cowardice);

		/*!
		
		@description

		Description Used to play the given sound for a unit within the VBS2 environment. If the unit is a person, it will also perform the corresponding lipsync effect. If the camera is not withing the given range, the title is not shown. The sound is 
		defined in the description.ext file. 

		@locality

		Globally applied Locally effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit for which the sound is played. The unit should be created before passing it as a parameter. 

		@param speechName- The name of the speech/sound.
		
		@param maxTitlesDistance- Maximum distance at which subtitles are shown.
		
		@param speed- It determines the speed of the subtitle lines provided that the relevant sound has subtitles.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		string speech ="M11v02";
		
		UnitUtilities::applySpeech(u,speech,1,3);

		@endcode

		@overloaded

		UnitUtilities::applySpeech(Unit& unit, string& speechName);

		UnitUtilities::applySpeech(Unit& unit, string& speechName, double maxTitlesDistance); 

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applySpeech(Unit& unit, const std::string& speechName, double maxTitlesDistance, double speed);

		/*!
		
		@description

		Used to turn on or off the forced collision of AI units within the VBS2 environment. 

		@locality

		Locally applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.06]

		@param unit � The unit for which the forced collision is changed. The unit should be created before passing it as a parameter.

		@param val � The value to be set, it decides whether the forced collision of AI units should be turned on or off. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyCollisionEnable(gunner,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyCollisionEnable(Unit& unit,bool val);

    /*! 
		Return the vehicle which given unit is mounted.
		
		@param unit - Unit that will be returned it's mounted vehicle.

		@return Vehicle - Vehicle object will return if unit is mounted to a vehicle, if it is not NULL object will return.
		*/
		static Vehicle getMountedVehicle(const Unit& unit);


		/*!

		@description

		Used to disable �MOVE� AI for a unit within the VBS2 environment.

		@locality

		Globally applied Locally Effected 

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the �MOVE� AI is disabled. The unit should be created before passing it as a parameter 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyAIMovementDisable(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To disable the MOVE AI for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyAIMovementDisable(Unit& u)).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		Deprecated. Use void ControllableObjectUtilities::applyAIBehaviourDisable(ControllableObject& co, AIBEHAVIOUR behaviour)
		*/
		VBSFUSION_DPR(UUNT082) static void applyAIMovementDisable(Unit& u);

		/*!

		@description

		Used to disable the AI path plan of a unit within the VBS2 environment. 

		@locality

		Globally applied Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the AI path plan is disabled. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyAIPATHPLANINGDisable(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To disable the AI path plan of a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyAIPATHPLANINGDisable(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		Deprecated. Use void ControllableObjectUtilities::applyAIBehaviourDisable(ControllableObject& co, AIBEHAVIOUR behaviour)
		*/
		VBSFUSION_DPR(UUNT084)static void applyAIPathPlanningDisable(Unit& u);

		/*!

		@description

		Used to enable the �Move� AI of a unit within the VBS2 environment. 

		@locality

		Globally applied Effected Locally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the �Move� AI is enabled. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyAIMovementEnable(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To enable the �Move� AI of a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyAIMovementEnable(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		Deprecated. Use void ControllableObjectUtilities::applyAIBehaviourEnable(ControllableObject& co, AIBEHAVIOUR behaviour)
		*/
		VBSFUSION_DPR(UUNT081) static void applyAIMovementEnable(Unit& u);

		/*!
		
		@description

		Used to enable the �PATHPLAN�  AI of a unit within the VBS2 environment.

		@locality

		Globally applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the �PATHPLAN� AI path plan is enabled. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyAIPathPlanningEnable(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To enable the �PATHPLAN� AI of a  unit created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyAIMovementEnable(Unit& u).
		Deprecated. Use void ControllableObjectUtilities::applyAIBehaviourEnable(ControllableObject& co, AIBEHAVIOUR behaviour)
		*/
		VBSFUSION_DPR(UUNT083) static void applyAIPathPlanningEnable(Unit& u);

		/*!

		@description

		Used to reload weapons of the unit within the VBS2 environment. The reload would take place when the current magazine of the weapon is empty.

		@locality

		Locally applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the reload is performed. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyWeaponReload(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyWeaponReload(Unit& u);

		/*!
		
		@description

		Used to send a text message over the global radio channel within the VBS2 environment.

		@locality

		Globally applied Effected Locally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit which sends the message. The unit should be created before passing it as a parameter.
		
		@param txtMessage -  The message that should be sent. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyGlobalChat(u,"hey");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To send a message from a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyGlobalChat(Unit& u,string txtMessage ).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyGlobalChat(Unit& u, const std::string& txtMessage);

		/*!
		
		@description

		Used to set the behavior of a unit within the VBS2 environment.

		@locality

		Globally applied Effected Locally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit for which the behavior is set. The unit should be created before passing it as a parameter. 

		@param behaviour - The type of behavior set to the unit. Possible values are CARELESS,SAFE,AWARE,COMBAT,STEALTH. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyBehaviour(commander, BEHAVIOUR::B_COMBAT);
		
		displayString+=" unit's behavior is- "+UnitUtilities::getUnitBehaviour(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply the behavior to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyBehaviour(Unit& unit, BEHAVIOUR behaviour)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyBehaviour(Unit& unit, BEHAVIOUR behaviour);

		/*!
		
		@description

		Used to check if a group with the name/alias exists within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param groupName � The name of the group that is checked. 

		@return bool

		@example

		@code

		//The unit to be created
		Unit u; 
		
		string name =�TeamA�;
		displayString+="The value is "+conversions::BoolToString(UnitUtilities:: isGroupExists(name));
		
		//or
		
		string isExists = conversions::BoolToString(UnitUtilities::isGroupExists(group1.getName()));
		displayString = isExists;

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply the behavior to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyBehaviour(Unit& unit, BEHAVIOUR behaviour)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool isGroupExists(const std::string& groupName);

		/*!
		
		@description

		Used to order a unit to stop within the VBS2 environment.

		@locality

		Globally applied Effected Globally.

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the order is given. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyCommandStop(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To stop a unit unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyCommandStop(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyCommandStop(Unit& u);

		/*!
		
		@description

		Used to order a unit to move to a new position within the VBS2 environment.

		@locality

		Locally applied Globally effected 

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the order is given. The unit should be created before passing it as a parameter.
		
		@param movePosition - The position to be moved

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u,player;
		
		UnitUtilities::applyMove(u, player.getPosition()+position3D(150,200,0));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To order a unit created in a client machine within a network to move to a new position, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyMove(Unit& u, position3D movePosition).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyMove(Unit& u, const position3D& movePosition);

		/*!
	
		@description

		Used to order a unit to target an object within the VBS2 environment.

		@locality

		Globally applied Effected Locally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the order is given. The unit should be created before passing it as a parameter. 

		@param co - The object targeted 

		@return String

		@example

		@code

		//The unit to be created
		Unit u,u1; 
		
		UnitUtilities::applyTarget(u, u1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To order a unit created in a client machine within a network to target an object, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyTarget(Unit& unit, ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyTarget(Unit& u, const ControllableObject& co);

		/*!

		@description

		Used to order a unit to watch a position within the VBS2 environment.

		@locality

		Globally applied Effected Locally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the order is given. The unit should be created before passing it as a parameter.

		@param targetPosition - The position to be watched.

		@return String

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyWatch(commander,player.getPosition()+position3D (150,170,0));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To order a unit created in a client machine within a network to watch a position, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyWatch(Unit& unit, position3D targetPosition).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyWatch(Unit& u, const position3D& targetPosition);

		/*!

		@description

		Used to apply the AUTO position to a unit within the VBS2 environment.

		@locality

		Locally Applied Effected Locally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the position is applied. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyUnitPosAUTO(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function does not update the UnitPos property of the unit. Use UnitUtilities:: updateUnitPos to reload and check for successful execution. 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To apply the AUTO position to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyUnitPosAUTO(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static void applyUnitPosAUTO(Unit& u);

		/*!

		@description

		Used to apply the DOWN position to a unit within the VBS2 environment.

		@locality

		Locally applied Effected Locally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the position is applied. The unit should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyUnitPosDOWN(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To apply the DOWN position to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyUnitPos DOWN(Unit& u).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information) 


		*/
		static void applyUnitPosDOWN(Unit& u);

		/*!
		Applies the UP position to the unit within the game.

		@param u - The Unit to apply the UP position.
		@return	Nothing.
		@remarks Does not update the UnitPos property of the unit.
		Use updateUnitPos to reload and check for successful execution.
		@remarks This is a replication of void unitPosUP(Unit& u) function.
		*/
		static void applyUnitPosUP(Unit& u);

		/*!
		
		@description

		Used to Enable/Disable precision movement mode for specified unit.
		This enables unit to move to exact position (e.g. as specified by the function UnitUtilities:: applyMove(Unit& u, position3D movePosition)) requested. By enabling precision movement, units precision is forced to 0. 
		This function only modifies movement in final steps, thereby path-finding and animation are not affected.

		@locality

		Globally applied Effected Locally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the precision movement is enabled/disabled . The unit should be created before passing it as a parameter.

		@param precisionMode - Mode of the precision.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyUnitExactMovementMode(u,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To change the precision mode of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyUnitExactMovementMode(Unit& u, bool precisionMode).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyUnitExactMovementMode(Unit& u, bool precisionMode);

		/*!
		
		@description

		Used to make a unit salute to a specified target unit within the VBS2 environment.

		@locality

		Locally applied Effected Globally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit which is made to salute . The unit should be created before passing it as a parameter. . 
 
		@param targetUnit � The unit which receives the salute.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applySalutePerform(commander,gunner);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applySalutePerform(Unit& unit, Unit& targetUnit);

		/*!
		
		@description

		Used to set the height of the unit within the VBS2 environment. The height should be in metres.

		@locality

		Globally applied Effected Locally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the height is set. The unit should be created before passing it as a parameter.  

		@param height � The height to be set.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyHeight(u,1.2);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To set the height for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyHeight(Unit& u , double height).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyHeight(Unit& u , double height);

		/*!

		@description

		Respawn the unit. Care should be taken when using this function as VBS2 automatically assigns a new name to the respawned unit if a name was not previously assigned to the unit.
		This command should therefore be used in conjunction with a respawn event handler to obtain the new name and assign an alias to the object. If the function is not used in conjunction with a respawn event, the respawning will prevent the unit from being handled and therefore could cause errors during the update cycle.

		@locality

		Globally Applied and Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit to be respawn.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 

		UnitUtilities::applyRespawn(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To respawn a unit unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyRespawn(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		 */
		static void applyRespawn(Unit& unit);

		/*!

		@description

		Used to set the body mass index of the unit within the VBS2 environment.

		@locality

		Globally applied Locally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the mass is set. The unit should be created before passing it as a parameter. 

		@param index � The body mass to be set.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 

		UnitUtilities::applyBMI(u,3);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To set the body mass for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyBMI(Unit& u, double index ).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyBMI(Unit& u, double index);
				
		/*!
		
		@description

		Used to disable gunner input of the unit within the VBS2 environment.

		@locality

		Globally applied Effected Locally

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit for which the gunner input is disabled. The unit should be created before passing it as a parameter. 

		@param  mode � The input modes are, 
			NOTHING� Enables disabled modes
			WEAPON- Disable weapon
			HEAD- Disable head
			WEAPONANDHEAD-Disable weapon and head

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u; 
		
		UnitUtilities::applyGunnerInputDisable(commander,GUNNERINPUT_MODE::GUNNERINPUT_WEAPON);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To disable the gunner input for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyGunnerInputDisable(Unit& unit,GUNNERINPUT_MODE mode).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyGunnerInputDisable(Unit& unit, GUNNERINPUT_MODE mode);

		/*!

		@description

		Used to select the weapon name by a given muzzle name.

		@locality

		Applied Locally Effective Globally 

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the weapon is selected. The unit should be created before passing it as a parameter. 

		@param muzzleName - Name of the muzzle.

		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyWeaponSelect(u,"vbs2_us_m249_iron");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyWeaponSelect(Unit& u, const std::string& muzzleName);

		/*!

		@description

		Used to add magazine to a Unit within the VBS2 environment.

		@locality

		Locally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the magazine is added. The unit should be created before passing it as a parameter 

		@param name- The name of the magazine added to the unit. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyMagazineAdd(u,"vbs2_Batteries");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyMagazineAdd(Unit& u, const std::string& name);

		/*!

		@description

		Used to Count the number of magazines available in given Unit within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param u � The unit for which the number of magazines is counted. The unit should be created before passing it as a parameter.

		@return int

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The count for u is "+conversions::IntToString(UnitUtilities::getMagazinesCount(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To count the number of magazines available in a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getMagazinesCount(Unit& u)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static int getMagazinesCount(const Unit& u);

		/*!
		
		@description

		Used to remove a specified magazine from the unit within the VBS2 Environment. 

		@locality

		Locally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit for which the magazine is removed. The unit should be created before passing it as a parameter. 

		@param name - The magazine which should be removed from the unit.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyMagazineRemove(u,"vbs2_mag_30rnd_556x45_Ball_steyr_aug" );

		@endcode

		@overloaded

		None

		@related

		None

		@remarks While using this function, there is a possibility to create invalid combinations that could result in undefined application behavior. 

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To remove a specified magazine from a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyMagazineRemove(Unit& unit, string name)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void applyMagazineRemove(Unit& unit, const std::string& name);

		/*!
		
		@description

		Used to remove all the specified magazines from the unit within the VBS2 Environment. 

		@locality

		Locally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit for which the magazines are removed. The unit should be created before passing it as a parameter. 

		@param name - The magazine which should be removed from the unit.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyMagazineRemoveAll(u,"vbs2_mag_200rnd_556x45_Trace_m249");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To remove a specified magazine from a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyMagazineRemoveAll(Unit& unit, string name)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyMagazineRemoveAll(Unit& unit, const std::string& name);

		/*!
		
		@description

		Used to add Weapons to the unit within the VBS2 Environment. Once the weapon slots are filled, any further applyWeaponAdd commands are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should be added, before adding these weapon to the unit.

		@locality

		Locally applied Globally effected.

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit for which the weapon is added. The unit should be created before passing it as a parameter.  

		@param name -The weapon added to the unit.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyWeaponAdd(u,"vbs2_soflam");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyWeaponAdd(Unit& unit, const std::string& name);
		
		/*!
		
		@description

		Used to remove a specified weapon from the unit within the VBS2 Environment. 

		@locality

		Locally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit for which the weapon is removed. The unit should be created before passing it as a parameter. 

		@param name - The magazine which should be removed from the unit.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyWeaponRemove(u,"vbs2_soflam");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyWeaponRemove(Unit& unit, const std::string& name);

		/*!

		@description

		Used to check if a unit within the VBS2 Environment has a given weapon. 

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit which is checked. The unit should be created before passing it as a parameter.  

		@param name - The weapon that is checked with the unit�s inventory.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The value is"+conversions::BoolToString(UnitUtilities::isWeaponHaving(u,"vbs2_soflam"));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To check the weapon for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isWeaponHaving(Unit& unit, string name)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool isWeaponHaving(Unit& unit, const std::string& name);

		/*!
		
		@description

		Used to sets how much ammunition (compared to a full state defined by the unit) the unit has. The value ranges from 0 to 1.

		@locality

		Locally applied 

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit for which the value is set. The unit should be created before passing it as a parameter.

		@param ammovalue- The ammo amount set to the unit.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyVehicleAmmo(commander, 0.4);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyVehicleAmmo(Unit& unit, double ammovalue);

		/*!

		@description

		Used to remove all weapons and magazines from the unit.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit for which the magazines and weapons are removed. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyWeaponRemoveAll(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To remove all weapons and magazines of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyWeaponRemoveAll(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyWeaponRemoveAll(Unit& unit);

		/*!
		
		@description

		Used to order a unit to commence firing on the given target (silently). If the target is objNull, the unit is ordered to commence firing on its current target (using UnitUtilities::applyTarget(Unit& unit, ControllableObject& co)).

		@locality

		Locally applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param firingUnit � The unit that starts firing. The unit should be created before passing it as a parameter.

		@param target- The object which is aimed at by the firing unit. The target can be a unit or a vehicle, but not an object.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u, u2; 
		
		UnitUtilities::applyFire(u1,u2);

		@endcode

		@overloaded

		UnitUtilities:: applyFire(Unit& firingUnit, position3D pos);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		*/
		static void applyFire(Unit& firingUnit, ControllableObject& target);

		/*!
		
		@description

		Used to order a unit (silently) to fire at the given position. The position must be given in PositionASL format.

		@locality

		Locally applied Globally Effected 

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param firingUnit � The unit that starts firing. The unit should be created before passing it as a parameter. 

		@param pos - The position where the unit would fire at. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyFire(u,player.getPosition()+position3D(10,10,0));

		@endcode

		@overloaded

		UnitUtilities:: applyFire(Unit& firingUnit, ControllableObject& target);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc); 

		*/
		static void applyFire(Unit& firingUnit, const position3D& pos);

		/*!

		@description

		Used to enable or disable firing on current weapon. If true, the unit's current weapon cannot be fired.

		@locality

		Locally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.07]

		@param unit � The unit for which the current weapon�s firing is enabled or not. The unit should be created before passing it as a parameter 

		@param safe - True to enable firing and false to disable

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyWeaponSafety(u,false);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To enable or disable the firing on the current weapon for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyWeaponSafety(Unit& unit, bool safe).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyWeaponSafety(Unit& unit, bool safe);

		/*!
		
		@description

		Used to return a list of targets within the defined range within the VBS2 environment."Targets" are not restricted to enemy units.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which the targets are obtained. The unit should be created before passing it as a parameter 

		@param range- The range within which the targets are obtained.

		@return vector<Target>

		@example

		@code

		//The unit to be created
		Unit u;

		vector<Target> vt = UnitUtilities::getNearTargets(commander,100 );
		
		for (vector<Target>::iterator itr = vt.begin(); itr!= vt.end(); ++itr)
		{
			displayString+="The name is "+itr->getType();
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To return a list of targets for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getNearTargets(Unit& unit, double range).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::vector<Target> getNearTargets(Unit& unit, int range);

		/*!

		@description

		Used to enable personal items for a unit within the VBS2 environment. Used for units inside vehicles to enable their personal equipment.

		@locality

		Locally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which the personal items are enabled. The unit should be created before passing it as a parameter

		@param enable - True to enable and false to disable.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyPersonalItemsEnable(u,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyPersonalItemsEnable(Unit& unit, bool enable);

		/*!

		@description

		Used to change the unit's morale level.The value of morale is between 0 and 1, where 0 = exhausted and 1 = fit).Use (-) values to reduce the morale for a unit within the VBS2 environment.

		@locality

		Locally applied Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit - The Unit to add morale.

		@param moralChange - The value to add. Use (-) to reduce the morale.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyMoralAddition(u,-0.45);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void addToMorale(Unit& unit, double moralChange) function.
		
		*/
		static void applyMoralAddition(Unit& unit, double moralChange);

		/*!

		@description

		Used to turn on or off the ability for a unit to speak audibly.This function does not affect orders from being issued or received.
		Units will still shout when being shot at.

		@locality

		Globally applied and Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which the audibility is turned on or off. The unit should be created before passing it as a parameter.

		@param speech - true to enable and false to disable.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applySpeechEnable(u,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To turn on/off the audibility of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applySpeechEnable(Unit& unit, bool speech).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		@remarks Does not affect orders from being issues or received. Also Units will still scream when being shot at.
		
		@remarks This is a replication of void allowSpeech(Unit& unit, bool speech) function.
		*/
		static void applySpeechEnable(Unit& unit, bool speech);

		/*!

		@description

		Used to change the unit's anaerobic fatigue level (The level is between 0 and 1). Use (-)ve values to reduce the AnaerobicFatigue level of a unit within the VBS2 environment.

		@locality

		Locally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which the anaerobic fatigue level is changed. The unit should be created before passing it as a parameter
		
		@param AnaerobicFatigueChange - The value to add. Use ( - ) to reduce.

		@return String

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyAnaerobicFatigueAddition(u,0.62);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).  

		@remarks This is a replication of void addToAnaerobicFatigue(Unit& unit, double AnaerobicFatigueChange) function.
		*/
		static void applyAnaerobicFatigueAddition(Unit& unit, double AnaerobicFatigueChange);

		/*!

		@description

		Used to change the unit's aerobic fatigue level (The level is between 0 and 1). Use (-)ve values to reduce the aerobicFatigue level of a unit within the VBS2 environment.

		@locality

		Locally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which the aerobic fatigue level is changed. The unit should be created before passing it as a parameter

		@param AerobicFatigueChange - The value to add the aerobic fatigue. Use ( - ) to reduce.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyAerobicFatigueAddition(gunner, 0.629);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void addToAerobicFatigue(Unit& unit, double AerobicFatigueChange) function.
		
		*/
		static void applyAerobicFatigueAddition(Unit& unit, double AerobicFatigueChange);

		/*!

		@description

		Used to stop a unit without using radio messages .A unit stopped using this function can leave the groups formation. It will prevent the unit from moving around with its group (or formation leader), while still being able to turn around and even move to a new position if they see fit. 
		They will still respond to orders from their group leader (like engage, rearm, board a vehicle), but all of their actions will be separate from the group formation (unless ordered to return, which AI leaders don't do unless a function tells them to).

		@locality

		Globally applied Effective Globally

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit that is stopped. The unit should be created before passing it as a parameter
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyStop(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To stop a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyStop(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		@remarks This is a replication of void StopUnit(Unit& unit) function.
		
		*/
		static void applyStop(Unit& unit);

		/*!

		@description

		Used to revive a unit immediately, preserving his name & equipment within the VBS2 environment.

		@locality

		Globally applied Globally affected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit that is revived. The unit should be created before passing it as a parameter 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyRevive(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To revive a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyRevive(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		@remarks This is a replication of void reviveUnit(Unit& unit) function.
		
		*/
		static void applyRevive(Unit& unit);

		/*!
		
		@description

		Used to add weapons to the cargo space of objects, which can be taken out by infantry units. Once the weapon cargo space is filled up, all new applyWeaponCargo functions are ignored.

		@locality

		Globally applied Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param  unit � The unit for which the weapon is added to the cargo. The unit should be created before passing it as a parameter

		@param weaponName - The weapon which is added.
		
		@param count - The number of weapons added.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyWeaponCargo(commander,"vbs2_soflam",10);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To add the weapon for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyWeaponCargo(Unit& unit, string weaponName, int count).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		@remarks Once the weapon cargo space is filled up, any further applyWeaponCargo commands are ignored.
		
		@remarks This is a replication of void addWeaponToCargo(Unit& unit, string weaponName, int count) function.
		
		*/
		static void applyWeaponCargo(Unit& unit, const std::string& weaponName, int count);

		/*!

		@description

		Used to add magazines to the cargo space of Objects, which can be taken out by infantry units. Once the magazine cargo space is filled up, all new addMagazineCargo commands are ignored.

		@locality

		Globally applied Locally effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which the magazine is added to the cargo. The unit should be created before passing it as a parameter

		@param magName - The magazine which is added.

		@param count - The number of magazines added.
		
		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyMagazineCargo(u,"vbs2_Batteries",10 );

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To add the magazine for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyMagazineCargo(Unit& unit, string magName, int count)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		@remarks Once the magazine cargo space is filled up, any further addMagazineCargo commands are ignored.

		@remarks This is a replication of void addMagazineToCargo(Unit& co, string magName, int count) function.
	
		*/
		static void applyMagazineCargo(Unit& unit, const std::string& magName, int count);

		/*!

		@description

		Used to order a unit to move to a given position (via the radio). This function is similar to the function UnitUtilities::applyMove(Unit& unit, position3D position), except this command displays a radio message. within the VBS2 environment.

		@locality

		Locally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit which is commanded to move. The unit should be created before passing it as a parameter

		@param position � The position to which the unit should move.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyCommandMove(u, player.getPosition()+position3D(150,200,0));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks This is a replication of void commandMove(Unit& unit, position3D position) function.
		
		*/
		static void applyCommandMove(Unit& unit, const position3D& position);

		/*!

		@description

		Used to add a number to the rating of a unit. Negative values can be used to reduce the rating. This command is 
		usually used to reward for completed mission objectives. The rating is given at the end of the mission 
		and is automatically adjusted when killing enemies or friendlies. When the rating gets below zero, the 
		unit is considered "renegade" and is an enemy to everyone.

		@locality

		Locally applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which the rating is applied. The unit should be created before passing it as a parameter

		@param rating � The rating to be applied. Use ( - ) to decrees the rating.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyRatingIncrease(u,45);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To apply the rating to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyRatingIncrease(Unit& unit, double rating).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyRatingIncrease(Unit& unit, double rating);

		/*!
		@description
		
		This function returns  True if the unit is able to stand else returns false.


		@locality

		Globally applied

		@version [VBS3Fusion v3.15]

		@param   unit- The unit for which the above function is applied.
		
		@return   bool
		
		@example 
		
		@code

		//The unit to be created

		Unit soldier1;
		displayString= "is Standable: "+ conversions::BoolToString(UnitUtilities::isStandable(soldier1));
		
		@end code
		
		@related
		
		None
		
		@overloaded
		
		None

		@remarks - This is a replication of bool canStand(Unit& unit) function.
		@remarks - It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		@remarks - To obtain the standable property of the unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function VehicleUtilities:: createAndMoveInAndAsDriver(Vehicle& vehicle, Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static bool isStandable(const Unit& unit);

		/*!

		@description

		Used to check if the unit has some ammo within the VBS2 environment.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param u � The unit which is checked. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		displayString+="The unit's ammo"+conversions::BoolToString(UnitUtilities::isAmmoAvailable(u));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check the availability of ammo for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: isAmmoAvailable(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		@remarks This is a replication of bool hasSomeAmmo(Unit& unit) function.
		
		*/
		static bool isAmmoAvailable(const Unit& unit);

		/*!
		
		@description

		Used to check if (and by how much)a unit knows about its target.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit which is checked. The unit should be created before passing it as a parameter. 

		@param target - The target object.

		@return double

		@example

		@code

		//The unit to be created
		Unit u1, u2;
		
		displayString+="The value is "+conversions::FloatToString(UnitUtilities::getKnowsAbout(u1,u2));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To check the knowledge of targets for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getKnowsAbout(Unit& unit, ControllableObject& target)).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		
		@remarks This is a replication of float knowsAbout(Unit& unit, ControllableObject& target) function.
		
		*/
		static double getKnowsAbout(Unit& unit, const ControllableObject& target);

		/*!

		@description

		Used to type text to the side radio channel.

		@locality

		Globally Applied Locally Effective

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit that types the text. The unit should be created before passing it as a parameter

		@param chatText - The text to type.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applySideChat(u,"hello world");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To type the text for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applySideChat(Unit& unit, string chatText)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks This is a replication of void sideChat(Unit& unit, string chatText) function.

		*/
		static void applySideChat(Unit& unit, const std::string& chatText);

		/*!

		@description

		Used to make a unit send a text message over the group radio channel within the VBS2 environment.

		@locality

		Locally applied Locally Effective

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit which sends the text. The unit should be created before passing it as a parameter

		@param chatText - The text to type.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u,player;
		
		UnitUtilities::applyGroupChat(player,"ROFL");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This function is applicable only for player units.

		@remarks This is a replication of void groupChat(Unit& unit, string chatText) function.
		
		*/
		static void applyGroupChat(Unit& unit, const std::string& chatText);

		/*!

		@description

		 Used to add an action to an object. The action is usable by the object itself, as well as anyone who is within proximity of the object and is looking at it.If an action is added to a unit, the action will only be visible when the unit is on foot. If the unit enters a vehicle, the action will not be visible, not to the unit, or to anyone in / near the unit�s vehicle.

		@locality

		Applied Locally Effective Locally 

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param  unit � The unit/object for which the action is assigned to. The unit should be created before passing it as a parameter 

		@param title - The action name which is displayed in the action menu

		@param fileName - File and Path relative to the mission folder. Called when the action is activated. Depending on the file extension (.sqf/.sqs) the file is executed with SQF or SQS Syntax.
		
		@return int (Returns the index of the added action. Used to delete the action with deleteActionMenuItem(Unit& unit, int index))

		@example

		@code

		//The unit to be created
		Unit player;
		int menuItem;
		
		menuItem = UnitUtilities::createActionMenuItem(player,"Test","test.sqf");

		@endcode

		@overloaded

		None

		@related

		UnitUtilities:: deleteActionMenuItem(Unit& unit, int index);

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)

		@remarks This is a replication of int addActionMenuItem(Unit& unit, string title, string fileName) function.
		
		*/
		static int createActionMenuItem(Unit& unit, const std::string& title, const std::string& fileName);

		/*!
		
		@description

		Used to remove an action with a given id index within the VBS2 environment. 

		@locality

		Applied Locally Effective Locally 

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit from which the action is removed. The unit should be created before passing it as a parameter

		@param index - Index of the action to delete.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit player;
		
		UnitUtilities::deleteActionMenuItem(player,menuItem);

		@endcode

		@overloaded

		None

		@related

		UnitUtilities::createActionMenuItem(Unit& unit, string title, string fileName);

		@remarks t is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).  
		
		@remarks This is a replication of void removeActionMenuItem(Unit& unit, int index) function.
		
		*/
		static void deleteActionMenuItem(Unit& unit, int index);

		/*!

		@description

		Used to reload the primary weapon of a given unit within the VBS2 environment.

		@locality

		Locally applied Globally Effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which the primary weapon is reloaded. The unit should be created before passing it as a parameter

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyPrimaryWeaponReload(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks This is a replication of void reloadPrimaryWeapon(Unit& unit)
		
		*/
		static void applyPrimaryWeaponReload(Unit& unit);

		/*!

		@description

		Used to stop an AI unit.The stopped unit will not be able to move, fire, or change its orientation to follow a watched object.

		@locality

		Locally applied Globally affected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which is stopped. The unit should be created before passing it as a parameter.

		@param stop - If this is "true" , unit will not able and else unit will able to move, fire, change.
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyStop(u,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To stop an AI unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyStop(Unit& unit, bool stop).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 
		
		@remarks This is a replication of void stop(Unit& unit, bool stop)
		
		*/
		static void applyStop(Unit& unit, bool stop);

		/*!

		@description

		Use to provide a smooth transition to a given move of a unit. Possible movements are,
		AmovPercMstpSnonWnonDnon_coughing_v1
		AmovPercMstpSnonWnonDnon_coughing_v2
		AmovPercMstpSnonWnonDnon_coughing_v3
		AmovPercMstpSnonWnonDnon_coughing_v4
		AmovPercMstpSnonWnonDnon_coughing_v5
		AmovPercMstpSnonWnonDnon_CheckSix_v1
		AmovPercMstpSnonWnonDnon_CheckSix_v2
		AmovPercMstpSnonWnonDnon_CheckSix_v3
		AmovPercMstpSnonWnonDnon_CheckSix_v4
		AmovPercMstpSnonWnonDnon_ComeHere
		AmovPercMstpSnonWnonDnon_Contempt
		AmovPercMstpSnonWnonDnon_ContemptSlap
		AmovPercMstpSnonWnonDnon_GoAway
		AmovPercMstpSnonWnonDnon_Greeting
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v1
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v2
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v3
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v4
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v5
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_short
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_1
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_2
		AmovPercMstpSnonWnonDnon_teargas_v1
		AmovPercMstpSnonWnonDnon_teargas_v2
		AmovPercMstpSnonWnonDnon_teargas_v3
		AmovPercMstpSnonWnonDnon_Wait
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v1
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v2
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v3
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v4
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v5
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v1
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v2

		@locality

		Locally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which the AI property is stopped. The unit should be created before passing it as a parameter

		@param moveName - The macro that defined movements.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applyMovement(u,string ("AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v5"));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).  
		
		@remarks This is a replication of void playMove(Unit& unit, string moveName)
		
		*/
		static void applyMovement(Unit& unit, const std::string& moveName);

		/*!
		
		@description

		Use to provide a smooth transition to a given move of a unit.

		@locality

		Locally applied Globally effected

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit � The unit for which the AI property is stopped. The unit should be created before passing it as a parameter

		@param move - The macro that defined movements and previously mentioned in void applyMovement(Unit& unit, string moveName) function definition comment.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyMovement(u,UNITMOVE::AMOVPERCMSTPSNONWNONDNON_WALK_FORWARD_CHECKSIX_V5);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void playMove(Unit& unit, UNITMOVE move)
		
		*/
		static void applyMovement(Unit& unit, UNITMOVE move);

		/*!

		@description

		When used on a person, the given move is started immediately (there is no transition). Use switchmove "" to switch back to the default movement if there is no transition back, otherwise the person may be stuck.

		Possible movements are,
		AmovPercMstpSnonWnonDnon_coughing_v1
		AmovPercMstpSnonWnonDnon_coughing_v2
		AmovPercMstpSnonWnonDnon_coughing_v3
		AmovPercMstpSnonWnonDnon_coughing_v4
		AmovPercMstpSnonWnonDnon_coughing_v5
		AmovPercMstpSnonWnonDnon_CheckSix_v1
		AmovPercMstpSnonWnonDnon_CheckSix_v2
		AmovPercMstpSnonWnonDnon_CheckSix_v3
		AmovPercMstpSnonWnonDnon_CheckSix_v4
		AmovPercMstpSnonWnonDnon_ComeHere
		AmovPercMstpSnonWnonDnon_Contempt
		AmovPercMstpSnonWnonDnon_ContemptSlap
		AmovPercMstpSnonWnonDnon_GoAway
		AmovPercMstpSnonWnonDnon_Greeting
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v1
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v2
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v3
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v4
		AmovPercMstpSnonWnonDnon_Rubbing_Hands_Fast_v5
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v1_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v2_short
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_long
		AmovPercMstpSnonWnonDnon_SmokingCasual_v3_short
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_1
		AmovPercMstpSnonWnonDnon_SmokingTalkingCasual_v1_complex_2
		AmovPercMstpSnonWnonDnon_teargas_v1
		AmovPercMstpSnonWnonDnon_teargas_v2
		AmovPercMstpSnonWnonDnon_teargas_v3
		AmovPercMstpSnonWnonDnon_Wait
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v1
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v2
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v3
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v4
		AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v5
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v1
		AmovPercMstpSnonWnonDnon_Wringing_Hands_Slow_v2

		@locality

		Globally applied Locally effected 

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit - The unit that suppose to switch moving.
		
		@param moveName - It can be one of the above things.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applySwitchMove(u, string ("AmovPercMstpSnonWnonDnon_Walk_Forward_CheckSix_v5"));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void switchMove(Unit& unit, string moveName)
		
		*/
		static void applySwitchMove(Unit& unit, const std::string& moveName);

		/*!

		@description

		When used on a person, the given move is started immediately (there is no transition). Use switchmove "" to switch back to the default movement if there is no transition back, otherwise the person may be stuck.

		@locality

		Globally applied Locally effected 

		@version [VBS2 2.02 / VBS2Fusion v3.08]

		@param unit - The unit that suppose to switch moving.

		@param move -  The macro that defined movements.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;

		UnitUtilities::applySwitchMove(u,UNITMOVE::AMOVPERCMSTPSNONWNONDNON_WALK_FORWARD_CHECKSIX_V5);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void switchMove(Unit& unit, UNITMOVE move)
	
		*/
		static void applySwitchMove(Unit& unit, UNITMOVE move);

		/*!
	
		@description

		Used to allow a unit(s) to enter vehicles within the VBS2 environment.	

		@locality

		Globally applied Globally effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unitList � The unit(s) which are allowed to enter vehicles. The unit should be created before passing it as a parameter 

		@param val - If this it "true" units allowed and else units not allowed enter vehicle.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u1,u2;

		vector<Unit> vu;
		vu.push_back(u1);
		vu.push_back(u2);

		UnitUtilities::applyAllowGetIn(vu,false);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 
	
		@remarks To allow a unit created in a client machine within a network to enter vehicles, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyAllowGetIn(vector<Unit> unitList, bool val).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		@remarks This is a replication of void allowGetIn(vector<Unit> unitList, bool val) function.
		
		*/
		static void applyAllowGetIn(const std::vector<Unit>& unitList, bool val);

		/*!

		@description

		Used to unassign a unit from whichever vehicle that unit is currently assigned to. If the unit is currently in that vehicle, the group leader will issue an order to disembark. 

		@locality

		Locally applied 

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit which is unassigned from the vehicle. The unit should be created before passing it as a parameter

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyVehicleUnassign(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To un assign a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyStop(Unit& unit, bool stop).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		@remarks This is a replication of void unAssignVehicle(Unit& unit)

		*/
		static void applyVehicleUnassign(Unit& unit);

		/*!
		
		@description

		Description Used to hide the body (corpse) of the given person within the VBS2 environment.

		@locality

		Locally applied Globally effective

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit for which the body is hidden. The unit should be created before passing it as a parameter

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyBodyHiding(gunner);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void hideBody(Unit& unit) function.
		
		*/
		static void applyBodyHiding(Unit& unit);

		/*!

		@description

		Used to unselect a unit in the players group (in the UI command bar). Only valid if the player is a commander of a group.

		@locality

		Globally applied

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit which is unselected from the player group. The unit should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyUnselectFromGroup(u);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To unselect a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyUnselectFromGroup(Unit& unit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		@remarks This is a replication of void unselectUnitInGroup(Unit& unit)

		*/
		static void applyUnselectFromGroup(Unit& unit);

		/*!
		
		@description

		Description Used to set if a leader can issue attack commands to the soldiers in his group

		@locality

		Locally applied Globally effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit for which the attack command is given. The unit should be created before passing it as a parameter. 

		@param boolVal - If this it "true" units allowed and else units not allow to attack. 

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyAttackEnable(u, false);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void setEnableAttack(Unit& unit, bool boolVal) function.
		
		*/
		static void applyAttackEnable(Unit& unit, bool boolVal);

		/*!
		
		@description

		Used to play the given sound for a unit within the VBS2 environment. If the unit is a person, it will also perform the corresponding lipsync effect. If the camera is not withing the given range, the title is not shown. The sound is 
		defined in the description.ext file.

		@locality

		Globally applied Locally effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit for which the sound is played. The unit should be created before passing it as a parameter.

		@param speechName � The name of the speech/sound.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		string speech ="M11v02";
		
		UnitUtilities::applySpeech(commander,speech);

		@endcode

		@overloaded

		applySpeech(Unit& unit, string& speechName, double maxTitlesDistance);

		applySpeech(Unit& unit, string& speechName, double maxTitlesDistance, double speed);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void say(Unit& unit, string& speechName) 
		
		*/
		static void applySpeech(Unit& unit, const std::string& speechName);

		/*!
		
		@description

		Description Used to play the given sound for a unit within the VBS2 environment. If the unit is a person, it will also perform the corresponding lipsync effect. If the camera is not withing the given range, the title is not shown. The sound is 
		defined in the description.ext file.

		@locality

		Globally applied Locally effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit for which the sound is played. The unit should be created before passing it as a parameter.

		@param speechName- The name of the speech/sound.

		@param maxTitlesDistance- Maximum distance at which subtitles are shown.
		
		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		string speech ="M11v02";

		UnitUtilities::applySpeech(commander,speech,1);

		@endcode

		@overloaded

		static void applySpeech(Unit& unit, string& speechName);
		
		UnitUtilities::applySpeech(Unit& unit, string& speechName, double maxTitlesDistance, double speed);

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void say(Unit& unit, string& speechName, double maxTitlesDistance)
		
		*/
		static void applySpeech(Unit& unit, const std::string& speechName, double maxTitlesDistance);

		/*!

		@description

		Used to disable the movement of a unit within the VBS2 environment. If the function is applied to a player unit, the player would not be able to move but the player unit can change its stance , aim and other actions. Wheras on the other hand, when the fucntion is applied to an AI unit ,the will perform movement animations,but it will not move.

		@locality

		Locally applied Globally effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit for which the movement is disabled. The unit should be created before passing it as a parameter.

		@param disable- The value to be set. If the value is true, the movement would be disabled and otherwise.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyAnimationMoveDisable(u,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		*/
		static void applyAnimationMoveDisable(Unit& unit, bool disable);

		/*!
		
		@description

		Used to make a unit send a message over the group radio channel. The message is defined in the file description.ext or CfgRadio.

		@locality

		Globally applied Locally effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit which sends the message. The unit should be created before passing it as a parameter. 

		@param messageName � The message to be sent.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		//message one is a class name and it is described in the file description.ext
		string message ="messageOne"; 
		
		UnitUtilities:: applyGroupRadioMessage (u,message);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To send the message for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyGroupRadioMessage(Unit& unit, string& messageName).

		*/
		static void applyGroupRadioMessage(Unit& unit, const std::string& messageName);

		/*!
		
		@description

		Used to send a message over the group radio channel. The message is defined in the file description.ext or CfgRadio.

		@locality

		Globally applied Locally effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit which sends the message. The unit should be created before passing it as a parameter

		@param radioName � The message to be sent.

		@return String

		@example

		@code

		//The unit to be created
		Unit u;
		
		//message one is a class name and it is described in the file description.ext
		string msg ="messageOne"; 
		
		UnitUtilities::applyGlobalRadioMessage(u,msg);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To send the message for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyGlobalRadioMessage(Unit& unit, string& radioName);
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applyGlobalRadioMessage(Unit& unit, const std::string& radioName);

		/*!

		@description

		Used to order a unit(s) to target a given target (via the radio) within the VBS2 environment. 

		@locality

		Locally applied Globally effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unitList � The unit(s) for which the order is given. The unit should be created before passing it as a parameter. 

		@param target � The target of the unit(s).

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u,u1;
		
		vector<Unit> unitlist;
		unitlist.push_back(u);
		
		UnitUtilities::applyCommandTarget(unitlist,u1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks This is a replication of void commandTarget(vector<Unit>& unitList, Unit& target) function.
		
		*/
		static void applyCommandTarget(std::vector<Unit>& unitList, Unit& target);

		/*!

		@description

		Used to set the preference of AI to cross through buildings, when trying to reach their destination within the VBS2 environment.The higher the number (0-1), the less likely the AI will use buildings during their pathplanning.At 1 it will never use buildings.

		@locality

		Globally applied

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param AIunit � The unit for which the AI preference is set. The unit should be created before passing it as a parameter.

		@param cost � The value (between (0-1)) which determines whether the AI should use buildings during the path planning.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyAIPathwayCost(u,1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To set the AI preference for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyAIPathwayCost(Unit& AIunit, double cost)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		@remarks This is a replication of void setAIPathwayCost(Unit& AIunit, double cost) function.
		
		*/
		static void applyAIPathwayCost(Unit& AIunit, double cost);

		/*!

		@description

		Used to set the cowardice level (the lack of courage or bravery) of a unit within the VBS2 environment. By setting fleeing to 0.75,the units have a 75% chance of fleeing (and 25% of not fleeing).

		@locality

		Locally applied Globally effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit for which the cowardice level is set. The unit should be created before passing it as a parameter.

		@param cowardice � The rate of cowardice. The value is between 0 and 1 (0 means maximum courage, while 1 means always fleeing)

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		UnitUtilities::applyFleeing(u,1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks In V1.50 and after: If allowFleeing is set to 0 for a unit that is already fleeing, that unit will stop.
		In previous versions changing allowFleeing in a situation like this would not have had any effect. 

		*/
		static void applyFleeing(Unit& unit, double cowardice);

		/*!
		
		@description

		Used to find the nearest enemy of a given position. The function returns a null object if the object's group does not know about any enemies.

		@locality

		Globally applied

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit for which the nearest enemy is obtained. The unit should be created before passing it as a parameter.

		@param positionUnit � This unit's position that is used to select the nearest enemy unit from that position

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u, u1;
		
		u1 = UnitUtilities::getNearestEnemy(u,u.getPosition());
		displayString+="The name is"+u1.getName();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To find the nearest enemy of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getNearestEnemy(Unit& unit, Unit& positionUnit)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		@remarks This is a replication of Unit findNearestEnemy(Unit& unit, Unit& positionUnit) function.
		
		*/
		static Unit getNearestEnemy(Unit& unit, Unit& positionUnit);

		/*!
		
		@description

		Used to find the nearest enemy to the specified position. The function returns a null object if the object's group does not know about any enemies.

		@locality

		Globally applied

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit for which the nearest enemy is obtained. The unit should be created before passing it as a parameter passing it as a parameter.	

		@param position� The position in which the nearest enemy unit is selected.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		u1 = UnitUtilities::getNearestEnemy(u,u.getPosition());
		displayString+="The name is"+u1.getName();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To find the nearest enemy of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getNearestEnemy(Unit& unit, position3D position)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		@remarks This is a replication of Unit findNearestEnemy(Unit& unit, position3D position) function.

		*/
		static Unit getNearestEnemy(Unit& unit, const position3D& position);

		/*!

		@description

		Used to send a message to the side radio channel.The message is defined in the file description.ext or CfgRadio. 

		@locality

		Globally applied Locally Effected

		@version [VBS2 2.09 / VBS2Fusion v3.09]

		@param unit � The unit which sends the message. The unit should be created before passing it as a parameter

		@param messageName � The message to be sent.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit u;
		
		//message one is a class name and it is described in the file description.ext
		string message ="messageOne"; 
		
		UnitUtilities::applySideRadioMessage(u,message);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To send the message for a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applySideRadioMessage(Unit& unit, string& messageName)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static void applySideRadioMessage(Unit& unit, const std::string& messageName);

		/*!
		
		@description

		 Used to obtain the position in front of the unit where an object could be dropped.

		@locality

		Globally applied

		@version [VBS2 2.02 / VBS2Fusion v3.10]

		@param unit � The unit that is passed. The unit should be created before passing it as a parameter.

		@return position3D

		@example

		@code

		//The unit to be created
		Unit u; 
		
		displayString+=" The drop position is - "+UnitUtilities::getDropPos(u).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc). 

		@remarks To obtain the drop position of a unit created in a client machine within a network , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getDropPos (Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information). 

		*/
		static position3D getDropPos(const Unit& unit);

		/*!
		@description 

		Sets the direction of the Weapon according to azimuth (relative to the unit's body direction)& elevation. 
		One must use the disableGunnerInput function before using this function.
		
		@locality 

		Locally Applied, Globally Effected

		@version [VBS2Fusion v3.15]

		@param unit  - The Unit to set the weapon direction.

		@param azimuth -  Azimuth in degrees.

		@param elevation - Elevation in degrees.

		@param transition - If false the change will apply immediately without any transition.

		@return Nothing

		@example

		@code

		//The unit to be created
		Unit player;
		
		UnitUtilities::applyExternalControl(player,true);
		
		UnitUtilities::applyGunnerInputDisable(player,GUNNERINPUT_MODE::GUNNERINPUT_WEAPON);
		
		UnitUtilities::applyWeaponDirection(player,60,45,true);

		@endcode

		@overloaded 

		None

		@related

		UnitUtilities::applyGunnerInputDisable(Unit& unit, GUNNERINPUT_MODE mode)

		@remarks This is a replication of setWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition).

		@remarks Use ControllableObjectUtilities::applyExternalControl(ControllableObject& co, bool control) and UnitUtilities::applyGunnerInputDisable(Unit& unit, GUNNERINPUT_MODE mode) before using this function.

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).
		
		@remarks To set the direction of a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition);

		/*!		
		@description 
		
		Orders the unit to watch the given position (via the radio).

		@locality

		Locally Applied Globally Effected

		@version [VBS2Fusion v3.10.1]
	
		@param unit - A unit who get the command to watch at some target position.

		@param target - The position which unit watch.

		@return Nothing.

		@example

		@code

		//Unit to be created
		Unit u;

		//Apply the applyCommandWatch function to unit u on the player units position.
		UnitUtilities::applyCommandWatch(u,player.getPosition());

		@endcode

		@overloaded
		
		void UnitUtilities::applyCommandWatch(Unit& unit, ControllableObject& target)

		@related

		void UnitUtilities::commandWatch(Unit& unit, position3D& target)

		@remarks This is a replication of UnitUtilities::commandWatch(Unit& unit, position3D& target)
		*/
		static void applyCommandWatch(Unit& unit, const position3D& target);

		/*!
		@description 

		Control what the unit is glancing at (target or Position). How frequently the unit is glancing there depends on behaviour. 

		@locality

		Locally Applied Globally Effected

		@version [VBS2Fusion v3.10.1]

		@param unit - A unit who get the command to glance at some target position.

		@param position - The position which unit glance at.

		@return Nothing.

		@example

		@code

		//Unit to be created
		Unit u;

		//Apply the applyGlanceAt function to unit u on the player units position.
		UnitUtilities::applyGlanceAt(u,player.getPosition());

		@endcode

		@overloaded

		None

		@related

		void UnitUtilities::glanceAt(Unit& unit, position3D& position)

		@remarks This is a replication of UnitUtilities::glanceAt(Unit& unit, position3D& position)

		*/
		static void applyGlanceAt(Unit& unit, const position3D& position);

		/*!
		@description 

		Orders the unit to watch the given target (via the radio).Use objNull as the target to order a unit to stop watching a target 

		@locality 

		Locally Applied Globally Effected

		@version [VBS2Fusion v3.10.1]

		@param unit - A unit who get the command to watch at some ControllableObject.

		@param target - The ControllableObject which unit watch.

		@return Nothing.

		@example

		@code

		//Unit to be created
		Unit u;

		//Apply the applyCommandWatch function to unit u on the player unit
		UnitUtilities::applyCommandWatch(u,player);

		@endcode

		@overloaded 

		void UnitUtilities::applyCommandWatch(Unit& unit, position3D& target)

		@related

		void UnitUtilities::commandWatch(Unit& unit, ControllableObject& target)	

		@remarks This is a replication of UnitUtilities::commandWatch(Unit& unit, ControllableObject& target)
		*/
		static void applyCommandWatch(Unit& unit, const ControllableObject& target);

		/*!
		@description 

		Control what the unit is looking at (position).

		@locality  

		Locally Applied Globally Effected

		@version [VBS2Fusion v3.10.1]

		@param unit - A unit who get the command to look at some target position.

		@param position - The position which unit look at.

		@return Nothing.

		@example

		@code

		//Unit to be created
		Unit u;

		//Apply the applyLookAt function to unit u on the player units position.
		UnitUtilities::applyLookAt(u,player.getPosition());

		@endcode

		@overloaded

		None

		@related

		void UnitUtilities::LookAt(Unit& unit, position3D& position)

		@remarks This is a replication of UnitUtilities::LookAt(Unit& unit, position3D& position)

		*/
		static void applyLookAt(Unit& unit, const position3D& position);

		/*!
		@description 

		Returns formation FSM name for given unit.

		@locality
		
		Globally applied

		@version [VBS2Fusion v3.10]

		@param unit - The formation of this unit will return.

		@return string - Formation of given unit.

		@example

		@code

		//The unit to be created
		Unit u;

		displayString+="\\nThe FormationFSM name is  "+ UnitUtilities::getFormationFSM(unit);
		
		@endcode
		
		@overloaded
		
		None

		@related
		
		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).  

		@remarks To obtain the formationFSM for a unit created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities::getFormationFSM(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static std::string getFormationFSM(const Unit& unit);

		/*!
		@description

		Returns the stance that the unit is currently in. 
		
		@locality
		
		Globally applied

		@version [VBS2 2.15 / VBS2Fusion v2.15.0.1]

		@param unit - The unit that suppose to get stance. The unit should be created before passing it as a parameter. 

		@return string - Return value can be one of these,"UP" "MIDDLE" "DOWN" or empty string if undefined.

		@example

		@code
		
		//The unit to be created
		Unit u;

		displayString+="The stance is  "+ UnitUtilities::getUnitStance(unit));

		@endcode

		@overloaded
		
		None

		@related
		
		None

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).  

		@remarks To obtain the stance for a unit created in a client machine within a network , use the function  MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: getUnitStance(Unit& unit).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::string getUnitStance(const Unit& unit);

		/*!
		@description 
		Returns true if unit is on some ladder

		@locality

		Globally applied.

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@return bool - true if unit is on some ladder.

		@example

		@code

		//Check if unit is on some ladder.
		bool b = UnitUtilities::isOnSomeLadder(unit1);

		//Display the return value as a string.
		displayString = "On Some ladder : "+conversions::BoolToString(b);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static bool isOnSomeLadder(const Unit& unit);

		/*!
		@description 
		Returns true if unit is seated.

		@locality

		Locally applied.

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@return bool - true if unit is seated.

		@example

		@code

		//Check if unit is seated or not.
		bool b = UnitUtilities::isSeated(unit1);

		//Display the return value as a string.
		displayString = "Is Seated : "+conversions::BoolToString(b);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static bool isSeated(const Unit& unit);

		/*!
		@description 
		Returns true if unit have weapon on back.

		@locality

		Locally applied.

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@return bool - true if unit have weapon on back.

		@example

		@code

		//Check if unit have weapon on back
		bool b =UnitUtilities::isWeaponOnBack(unit1);

		//Display the return value as a string.
		displayString = "Is Weapon On Back : "+conversions::BoolToString(b);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static bool isWeaponOnBack(const Unit& unit);

		/*!
		@description 
		Returns position of pelvis

		@locality

		Globally applied.

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@return Vector3f - position of pelvis.

		@example

		@code

		//Define a Vector3f.
		Vector3f v;

		//Get the Pelvis Position.
		v = UnitUtilities::getPelvisPosition(unit1);

		//Display the return value as a string.
		displayString = "Pelvis Position x : "+conversions::FloatToString(v.x());
		displayString += "\\nPelvis Position y : "+conversions::FloatToString(v.y());
		displayString += "\\nPelvis Position z : "+conversions::FloatToString(v.z());

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static Vector3f getPelvisPosition(const Unit& unit);

		/*!
		@description 
		Returns FOV value for given unit

		@locality

		Globally applied.

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@return float - FOV value for unit.

		@example

		@code

		//Get the Field of Vision.
		float fov = UnitUtilities::getCameraFOV(unit1);

		//Display the return value as a string.
		displayString = "Field of Vision : "+conversions::FloatToString(fov);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static float getCameraFOV(const Unit& unit);

		/*!
		@description 
		Forces the unit to fire using currently selected Weapon.

		@locality

		Locally applied, locally effected

		@version  [VBSFusion v3.15]

		@param u - The unit that suppose to force for firing.

		@param fireSingleShot - If unit is supposed to fire single shot regardless to weapon setting

		@return Nothing.

		@example

		@code

		UnitUtilities::applyFiringForce(unit1,true);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static void applyFiringForce(Unit& u, bool fireSingleShot = false);

		/*!
		@description 
		Returns center of head moves.

		@locality

		Globally applied.

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@return Vector3f - center of head moves

		@example

		@code

		//Define a Vector3f.
		Vector3f v;

		//Get the position around which unit's head moves.
		v = UnitUtilities::getHeadCenterMoves(unit1);

		//Display the return value as a string.
		displayString = "Head Center Move x : "+conversions::FloatToString(v.x());
		displayString += "\\nHead Center Move y : "+conversions::FloatToString(v.y());
		displayString += "\\nHead Center Move z : "+conversions::FloatToString(v.z());

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static Vector3f getHeadCenterMoves(Unit& unit);

#if _BISIM_DEV_EXTERNAL_POSE2
		/*!
		@description 
		Returns center of leaning moves.

		@locality

		Globally applied.

		@version  

		@param unit - passed unit.

		@return Vector3f - center of head moves

		@example

		@code

		//Define a Vector3f.
		Vector3f v;

		//Get the position around which unit is leaning.
		v = UnitUtilities::getLeaningCenterMoves(unit1);

		//Display the return value as a string.
		displayString = "Leaning Center Move x : "+conversions::FloatToString(v.x());
		displayString += "\\nLeaning Center Move y : "+conversions::FloatToString(v.y());
		displayString += "\\nLeaning Center Move z : "+conversions::FloatToString(v.z());

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static Vector3f getLeaningCenterMoves(Unit& unit);


    /*!
		@description 
		Returns center of weapon moves.

		@locality

		Globally applied.

		@version  

		@param unit - passed unit.

		@return Vector3f - center of head moves

		@example

		@code

		//Define a Vector3f.
		Vector3f v;

		//Get the position around which unit's weapon moves.
		v = UnitUtilities::getWeaponCenterMoves(unit1);

		//Display the return value as a string.
		displayString = "Leaning Center Move x : "+conversions::FloatToString(v.x());
		displayString += "\\nLeaning Center Move y : "+conversions::FloatToString(v.y());
		displayString += "\\nLeaning Center Move z : "+conversions::FloatToString(v.z());

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static Vector3f getWeaponCenterMoves(Unit& unit);
#endif

		/*!
		@description 
		Returns recoil coefficient

		@locality

		Globally applied.

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@return double - recoil coefficient

		@example

		@code

		//Get the Recoil coefficient.
		double ce = UnitUtilities::getRecoilCoefficient(unit1);

		//Display the return value as a string.
		displayString = "Recoil Coefficient : "+conversions::DoubleToString(ce);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static double getRecoilCoefficient(Unit& unit);

		/*!
		@description 
		Set recoil coefficient

		@locality

		Globally applied, locally effected.

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@param recoilCoefficient - recoil coefficient

		@return Nothing.

		@example

		@code

		UnitUtilities::applyRecoilCoefficient(unit1,10);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static void applyRecoilCoefficient(Unit& unit, double recoilCoefficient);

		/*!
		@description 
		Used to return a list of units available for the player to switch.

		@locality

		Local

		@version  [VBSFusion v3.15]

		@param None.

		@return vector<Unit> - list of switchable units

		@example

		@code

		vector<Unit> vu = UnitUtilities::getSwitchableUnits();

		for (vector<Unit>::iterator itr = vu.begin(), itr_end = vu.end(); itr != itr_end; ++itr)
		{
		displayString+="The list of units are"+itr->getName();
		}	

		@endcode

		@overloaded

		None.

		@related

		@remarks This function works only on the OME (On Mission Editor) mode.
		*/
		static std::vector<Unit> getSwitchableUnits();

		/*!
		@description

		Used to join a unit to a given group.
				
		@locality

		Locally Applied Globally Effected
		
		@version [VBSFusion v3.2.0]

		@param id - Group position id. If position id is available this one is used. 

		@param grp - The group to which the unit needs to be added.

		@param unit - The unit that is added to the given group.

		@return Nothing.

		@example

		@code
		//Group to be created
		Group g1;
		//Unit to be created
		Unit u1;
		UnitUtilities::applyJoinToGroup(3,u1,g1); 
		GroupUtilities::updateGroup(g1);
		GroupUtilities::updateGroupDynamicProperties(g1);
		
		@endcode

		@overloaded

		None.
		
		@related

		UnitUtilities::applyJoinToGroupSilently(int id, Unit& unit, Group& grp)
		
		@remarks 
		*/
		static void applyJoinToGroup(int id, Unit& unit, Group& grp);

		/*!
		@description

		Used to join a unit to a given group without any radio messages.
		
		@locality

		Locally Applied Globally Effected
		
		@version [VBSFusion v3.2.0]

		@param id - Group position id. If position id is available this one is used. 

		@param grp - The group to which the unit needs to be added.

		@param unit - The unit that is added to the given group.

		@return Nothing.

		@example

		@code
		//Group to be created
		Group g1;
		//Unit to be created
		Unit u1;
		UnitUtilities::applyJoinToGroupSilently(3,u1,g1); 
		GroupUtilities::updateGroup(g1);
		GroupUtilities::updateGroupDynamicProperties(g1);

		@endcode

		@overloaded
		
		@related void UnitUtilities::applyJoinToGroup(int id, Unit& unit, Group& grp)
		
		@remarks 
		*/
		static void applyJoinToGroupSilently(int id, Unit& unit, Group& grp);

		/*!
		@description

		Used to return the units in a group below a given height within the VBS2 environment.

		@locality

		Local
		
		@version [VBSFusion v3.2.0]

		@param grp - The group for which the units belong to.

		@param height - The height in kilometers.

		@return vector<unit> - Vector of units belonging to the group.

		@example
		
		@code

		vector<Unit> vu;
		vu = UnitUtilities::getUnitsBelowHeight(g1, 1);

		for (vector<Unit>::iterator itr = vu.begin(), itr_end = vu.end(); itr != itr_end; ++itr)
		{
		UnitUtilities::updateStaticProperties(*itr);
		UnitUtilities::updateDynamicProperties(*itr);
		displayString+="The name of the units are"+itr->getAlias();
		}
		
		@endcode
		
		@overloaded vector<Unit> getUnitsBelowHeight(vector<Unit>& unitList, double height)
		
		@related
		
		@remarks 
		*/
		static std::vector<Unit> UnitUtilities::getUnitsBelowHeight(const Group& grp, double height);

		/*!
		@description

		Returns units in group below the input height. Height should be given in kilo meters.

		@locality
		
		@version [VBSFusion v3.15]

		@param unitList - The input list of units.

		@param height - The input height in meters.

		@return vector<unit> - Vector of units who are below the given height.

		@example

		@code

		@endcode

		@overloaded vector<Unit> getUnitsBelowHeight(Group& grp, double height)
		
		@related
		
		@remarks 
		*/
		static std::vector<Unit> getUnitsBelowHeight(const std::vector<Unit>& unitList, double height);

		/*!
		@description

		Return the unit's diving depth.

		@locality

		Locally applied, Locally effected

		@version [VBSFusion v3.20]

		@param unit - Unit that need to get the diving depth.

		@return Return the coresponding integer value of unit's diving depth. 

		@example

		@code

		Unit unit1;

		int depth=UnitUtilities::getDivingDepth(unit1);

		@endcode

		@overloaded 

		@related

		@remarks 
		*/
		static int getDivingDepth(const Unit& unit);

		/*!
		@description 

		Damage or repair part of a unit.

		@locality 

		Globally applied, locally effected.

		@version [VBSFusion v3.15]

		@param unit - The unit that is hit.

		@param part - The part of the unit which is hit. Part can be "body", "hands" or "legs". 

		@param damage - Damage level between 0 and 1. Damage 0 means fully functional, damage 1 means completely dead.

		@return Nothing.

		@example

		@code

		UnitUtilities::applyHit(unit,string("body"),1);
		
		@endcode

		@overloaded

		None

		@related
		
		None

		@remarks This function will trigger the EventHandlers for "Damaged", "DamagedHitPart" & "Killed". 
		
		*/
		static void applyHit(Unit& unit, const std::string& part, double damage);

		/*
		@description 

		When used on a person,a smooth transition to the given move will be initiated, but all previous playMove are discarded.

		@locality 		

		@version [VBSFusion v3.3.0]

		@param unit - unit need to apply the movement.
		@param moveName - movement name.
		
		@return Nothing.

		@example

		@code

		@endcode

		@overloaded

		@related

		@remarks  
		*/
		static void applyPlayMoveNow(Unit& unit, const std::string& moveName);


		/*!
		
		@description

		Used to add a number to the score of the unit. This score is shown on the debriefing page. Negative values will reduce the score.

		@locality
		
		Globally applied and globally effected

		@version [VBSFusion v3.3.1]

		@param unit - The unit for which the score is applied. The unit should be created before passing it as a parameter
		
		@param Score - The score applied.

		@return Nothing

		@example

		@code
		
		UnitUtilities::applyScoreIncrease(player,50);
		int s2 = UnitUtilities::getScore(player);
		displayString += "\\n Score 2: "+conversions::IntToString(s2);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function is only applicable to the player unit.
		
		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To apply the score to a unit created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function UnitUtilities:: applyScoreIncrease(Unit& unit, int score).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information) 

		@remarks This score is shown in multiplayer in the "I" screen.
		
		@remarks This is a replication of void addScore(Unit& unit, double rating) function.
		
		*/
		static void applyScoreIncrease(Unit& unit, int score);

		/*
		@description 

		Sets the "name" of a person.

		@locality 	

		Globally applied and locally effected.

		@version [VBSFusion v3.3.1]

		@param unit - unit need to set playable.

		@param name - unit name. 
		
		@return Nothing.

		@example

		@code

		// create playable unit
		Unit unit1;

		UnitUtilities::setUnitName(unit1, string("test player"));

		@endcode

		@overloaded

		@related

		@remarks  
		*/
		static void setUnitName(Unit& unit, const std::string& name);

		/*!
		@description 
		
		Set a flag to some unit.

		@locality
		
		Locally Applied and Globally Effected

		@version  [VBSFusion v3.3.2]

		@param unit - The unit will be assigned as flag owner.If set inactive unit, flag will be returned to flagpole.

		@param flag - A flag object.

		@return None.

		@example

		@code	
		
		Unit flagOwner;
		ControllableObject flag;
		
		UnitUtilities::applyFlagOwner(flagOwner, flag);

		@endcode

		@overloaded 
				
		@related

		@remarks
		
		*/
		static void applyFlagOwner(Unit& unit, ControllableObject& flag);

		/*!
		@description 
		Returns the owner of a flag.

		@locality		

		@version  [VBSFusion v3.3.2]

		@param flag - A flag object.

		@return The unit who assigned as flag owner.

		@example

		@code		

		@endcode

		@overloaded 
				
		@related

		@remarks
		
		*/
		static Unit getFlagOwner(const ControllableObject& flag);

		/*!
		@description 
		Returns the flag the unit is carrying.

		@locality		

		@version  [VBSFusion v3.3.2]

		@param unit - The unit that wants to check for flag.

		@return the flag. If the unit carries no flag, unassigned ControllableObject is returned.

		@example

		@code		

		@endcode

		@overloaded 
				
		@related

		@remarks
		
		*/
		static ControllableObject getFlag(const Unit& unit);

	private:
		friend class Unit;

	};
};


#endif