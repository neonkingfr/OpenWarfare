
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 ****************************************************************************/

/*****************************************************************************

Name:
	MineUtilities.h
Purpose:
	This file contains the declaration of the MineUtilities class.
	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			05-04/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_MINEUTILITIES_H
#define VBS2FUSION_MINEUTILITIES_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "data/Mine.h"
#include "ExplosiveUtilities.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{
	class VBSFUSION_API MineUtilities : public ExplosiveUtilities
	{
	public:
		/*!
		Creates a new mine as defined by the parameters present in Mine. 

		- If no alias is set or if the alias already exists in the mission a random alias is assigned. 
		- The type of mine defined by mine.getMineTypeString() is used.		 
		- mine is placed inside a circle with mine position as its center and placement as its radius.	

		*/
		static void createMine(Mine &mine);

		/*!
		Applies the name of the mine to the VBS2 Environment.
		Prior to applying, function verifies that the name is a valid name
		according to following criteria.
		- Name has to start with letter or underscore.
		- Only special characters allowed is underscore.
		- Name should not contain spaces in between.
		- Name should not already exist in the mission.
		If the given name is already available in the mission, name will NOT be set in
		the VBS2 Environment and the object name will be set to previous valid instance.
		*/
		static void applyName(Mine& mine);
	};
}

#endif //VBS2FUSION_MINEUTILITIES_H