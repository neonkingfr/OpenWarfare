/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	ExecutionUtilties.h

Purpose:

	This file contains the declaration of the ExecutionUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			26-11/2009	RMR: Original Implementation
	2.01		10-02/2010	MHA: Comments Checked	
	2.02        18-07/2011  YFP: Method added,
									ExecuteStringPublic(string&, string&);
	2.03		29-09/2011	SSD: Added Methods;
									void ExecuteStringPublic(string, string, ControllableObject, bool);
									void ExecuteFusionFunctionPublic(string, string, bool);

									
/************************************************************************/

#ifndef VBS2FUSION_EXECUTION_UTILITIES_H
#define VBS2FUSION_EXECUTION_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBSFusion
{
	class VBSFUSION_API ExecutionUtilities
	{
	public:

		/*!
		Executes the string specified by executionString as a VBS2 script in the game environment. 
		No value is returned. 
		*/
		static void ExecuteStringAndForget(const std::string& executionString);		

		/*!
		Executes the string specified by executionString as a VBS2 script in the game environment, 
		and returns the result in the form of a string.  

		Warning: If result is greater than 2048 characters use the overloaded method.
		*/
		static std::string ExecuteStringAndReturn(const std::string& executionString);

		/*!
		Executes the string specified by executionString as a VBS2 script in the game environment, 
		and returns the result in the form of a string. 
		*/
		static std::string ExecuteStringAndReturn(const std::string& executionString, int bufferSize);


		/*!
		Executes the string specified by executionString on all computers as a VBS2 script where condition 
		given by conditionString is true. 
		*/
		static void ExecuteStringPublic(const std::string& executionString, const std::string& conditionString);
	
		/*!
		Return true if the VBS2 output given by output is a valid VBS2 output. 
		Deprecated. Use bool isOutputValid(string output)
		*/
		VBSFUSION_DPR(UEXC003) static bool validOutput(std::string output);

		/*!
		Executes a set of script commands on all the clients logged in to the current mission if the condition is met.

		executionString - Set of script commands to execute.
		conditionString - The script commands will be executed only if this condition is met.
		object - Object to execute the commands on. This object can be referred as "_this" with the script commands.
		executeSelf (optional) - Default is true. The set of commands will execute on all the clients logged 
			into the callers mission. If set to false, the commands will execute on all the clients logged 
			into the current mission, except on the caller.
		*/
		static void ExecuteStringPublic(const std::string& executionString, const std::string& conditionString, const VBSFusion::ControllableObject& object, bool executeSelf = true);


		/*!
		Calls the FusionFunction of the given plugin with the given parameters on all the clients logged 
		into the current mission.

		pluginName - Name of the plugin to be called. Note that full plugin name should be passed.
			Example: HelloWorld.dll, HelloWorld.fusion
		parameters - Parameters to be passed to the plugin. Ex: s, k, load, unload, loaded. Note that the
			user should not try to unload the plugin using this function within the plugin.
		executeSelf (optional) - Default is true. The FusionFunction will be called on all the clients logged 
			into the callers mission with the given parameters. If set to false, the FusionFunction will be 
			called on all the clients logged into the current mission, except on the caller.
		*/
		static void ExecuteFusionFunctionPublic(const std::string& pluginName, const std::string& parameters, bool executeSelf = true);
			
		/*!
		Causes all loaded plugins in the "../plugins" folder to be requested to be unloaded.
		On this call, its important for all DLLs to respond and to stop any currently executing threads. 
		*/
		static void UnLoadAllPlugins();

		/*!
		Loads all plugins in the "../plugins" folder. 
		*/
		static void LoadAllPlugins();

		/*!
		Checks whether a specific plugin is currently loaded.
		The plugin should be in "../plugins" folder
		*/
		static bool isPluginLoaded(const std::string& pluginName);

		/*!
		Calls plugin PluginName and uses Value as it's parameter
		pluginName - name of the plugin
		parameter - input parameter of call back function "PluginFunction()" in the given plugin
		*/
		static std::vector<std::string> ExecuteFusionFunction(const std::string& pluginName, const std::string& parameter);

		/*!
		Returns content of given filename (from the mission folder).
		The file can either contain data, which can then be interpreted as a string, or it can contain code
		*/
		static std::string loadFile(const std::string& filename);

		/*!
		Reads and processes the content of the specified file. Preprocessor is C-like, supports comments using
		\/\/ or \/\* and \*\/ and macros defined with #define.
		Due to the hard-drive access this command executes (and the lack of caching) this command should not be
		used in time-critical script loops.
		Deprecated. Use string applyFilePreprocess(string& filename)
		*/
		VBSFUSION_DPR(UEXC001) static std::string preprocessFile(std::string& filename);

		/*!
		Returns the preprocessed content of the given file.
		Deprecated. Use string applyFileLineNumbersPreprocess(string& filename)
		*/
		VBSFUSION_DPR(UEXC002) static std::string preprocessFileLineNumbers(std::string& filename);

		/*!
		Check if a script is finished running using the Script_(Handle) returned by execVM or spawn.
		scriptHandler - the return value from ExecuteVM
		*/
		static bool isScriptDone(int scriptHandler);

		/*!
		Terminate (abort) the script. 
		scriptHandler - the return value from ExecuteVM
		*/
		static void terminate(int scriptHandler);

		/*!
		Executes the given plugin function in accordance with the given parameter.
		The plugin should be in "../plugins" folder
		*/
		static void pluginFunction(const std::string& pluginName, const std::string& parameter);

		/*!
		Compile and execute SQF Script.

		The optional argument is passed to the script as local variable _this.

		Script is compiled every time you use this command.
		The Script is first searched for in the mission folder, then in the campaign scripts folder and finally in the global scripts folder. 
		It returns the script handler reference number				
		*/
		static int ExecuteVM(const std::string& argument, const std::string& fileName);

		/*!
		@description 

		Reads and processes the content of the specified file. Preprocessor is C-like, supports comments using
		\/\/ or \/\* and \*\/ and macros defined with #define.
		Due to the hard-drive access this command executes (and the lack of caching) this command should not be
		used in time-critical script loops.
				
		@locality
		
		Locally Applied and Locally Effected
		
		@version [VBSFusion v3.10]
		
		@param filename - Name of the file which need to be read.
		
		@return string - Preprocessed content of the given file.
		
		@example
		
		@code
		
		string fileOutPut = ExecutionUtilities::applyFilePreprocess(string ("chkDamage.sqf"));
		displayString = "\\n File Output: "+fileOutPut;

		@endcode
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks This is a replication of string preprocessFile(string& filename)
		*/
		static std::string applyFilePreprocess(const std::string& filename);

		/*!
		@description 

		Returns the preprocessed content of the given file. 
		The preprocessor is C-like, it supports comments using \/ or \/* and *\/ and macros defined with #define.
				
		@locality
		
		Locally Applied and Locally Effected
		
		@version  [VBSFusion v3.10]
		
		@param filename: Name of the file which need to be read.
		
		@return string: Preprocessed content of the given file.
		
		@example
		
		@code
		
		string fileLineOutPut = ExecutionUtilities::applyFileLineNumbersPreprocess(string ("chkDamage.sqf"));
		displayString ="\\n File Output: "+fileLineOutPut;

		@endcode
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks This is a replication of string preprocessFileLineNumbers(string& filename)
		*/
		static std::string applyFileLineNumbersPreprocess(const std::string& filename);

		/*!
		@description 

		Return true if the VBS2 output given by output is a valid VBS2 output. 
				
		@locality
		
		Locally Applied and Locally Effected
		
		@version  [VBSFusion v3.10]
		
		@param output: VBS2 output string.
		
		@return bool: Whether the output is valid output or not.
		
		@example
		
		@code
		
		outPut = ExecutionUtilities::applyFilePreprocess(string ("chkDamage.sqf"));
		
		bool retOutPut = ExecutionUtilities::isOutputValid(outPut);
		
		displayString ="Is Output valid : "+conversions::BoolToString(retOutPut);

		@endcode
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks This is a replication of bool validOutput(string output)
		*/
		static bool ExecutionUtilities::isOutputValid(const std::string& output);
	};
};

#endif //EXECUTION_UTILITIES_H