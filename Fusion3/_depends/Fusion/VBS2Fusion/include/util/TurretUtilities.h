
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

TurretUtilities.h

Purpose:

This file contains the declaration of the TurretUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			30-01/2010	YFP: Original Implementation\
2.01		12-02/2010	MHA: Comments Checked

/************************************************************************/


#ifndef TURRETUTILITIES_H
#define TURRETUTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "data/Magazine.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBSFusion
{

	class VBSFUSION_API VBSFUSION_DPR(UTUR) TurretUtilities
	{
	public:

		/*!
			Load all Magazines available in the specified turret of the vehicle. 
		*/
		static void loadMagazines(const Vehicle& vehicle , Turret& turret);

		/*!
			Load all weapons available in the specified turret of the vehicle. 
		*/
		static void loadWeapons(const Vehicle& vehicle, const Turret& turret);

	};

};


#endif //TURRETUTILITIES_H