
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/


/*************************************************************************

Name:

	ProfilingUtilities.h

Purpose:

	This file contains support for profiling

*/

#ifndef PROFILING_UTILITIES_H
#define PROFILING_UTILITIES_H

#include "GeneralUtilities.h"

#if _BISIM_DEV_PROFILING
// Profilinig ////////////////////////////////////////////

#define PROFILER_EXPORT __declspec(dllexport)

typedef unsigned __int64 uint64;

enum PluginProfilerType
{
  PPT_None,
  PPT_Scope,
  PPT_Counter
};

struct PluginProfilerScopeInfo
{
  char *_name;
  PluginProfilerType _type;
  uint64 _id;
  char *_file;
  int _line;

  __forceinline PluginProfilerScopeInfo()
  {
    memset(this, 0, sizeof(*this));
  }
};

typedef void (*SetPluginProfilerCallback_fPtr)(PluginProfilerScopeInfo *info);

class Profiler
{
public:
  Profiler() : _profilerCallback(NULL) {}

  SetPluginProfilerCallback_fPtr _profilerCallback;

  __forceinline void PerformAction(PluginProfilerScopeInfo *info)
  {
    if (_profilerCallback) _profilerCallback(info);
  }
};

static Profiler GProfiler;

PROFILER_EXPORT void WINAPI SetPluginProfilerCallback(void *func) 
{
  SetPluginProfilerCallback_fPtr fPtr = (SetPluginProfilerCallback_fPtr)func;
  GProfiler._profilerCallback = fPtr;
}

class ProfileCounter
{
protected:
  int _frameId;
public:
  PluginProfilerScopeInfo _info;

  __forceinline ProfileCounter()
  {
    _info._type = PPT_Counter;
  }

  __forceinline void Init(char *name)
  {
    _info._name = name;
    _frameId = VBS2Fusion::GeneralUtilities::getFrameId();
  }

  __forceinline void operator += (int value)
  {
    int currentFrameId = VBS2Fusion::GeneralUtilities::getFrameId();
    if (_frameId != currentFrameId)
    {
      GProfiler.PerformAction(&_info);
      _frameId = currentFrameId;
      _info._line = 0;
    }
    _info._line += value;
  }
};

class ProfileScope 
{
public:
  PluginProfilerScopeInfo _info;

  __forceinline ProfileScope(char *name, char *file, int line)
  {
    _info._type = PPT_Scope;
    _info._name = name;
    _info._file = file;
    _info._line = line;
  }

  __forceinline ~ProfileScope()
  {
    GProfiler.PerformAction(&_info);
  }
};

#define PROFILE_SCOPE_NAME(name) \
  Scope_##name \

#define PROFILE_SCOPE(name) \
  ProfileScope PROFILE_SCOPE_NAME(name) (#name, __FILE__, __LINE__); \
  GProfiler.PerformAction(&PROFILE_SCOPE_NAME(name)._info); \

#define PROFILE_COUNTER_NAME(name) \
  Counter_##name \

#define PROFILE_COUNTER_INIT_NAME(name) \
  CounterInit_##name \

#define PROFILE_COUNTER_ADD(name,value) \
  static bool PROFILE_COUNTER_INIT_NAME(name) = false; \
  static ProfileCounter PROFILE_COUNTER_NAME(name); \
  if (!PROFILE_COUNTER_INIT_NAME(name)) \
  { \
    PROFILE_COUNTER_NAME(name).Init(#name); \
    PROFILE_COUNTER_INIT_NAME(name) = true; \
  } \
  PROFILE_COUNTER_NAME(name) += (value); \
  
// End of Profiling ///////
#endif

#endif

