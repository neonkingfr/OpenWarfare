
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name: PoseControlUtilities.h

Purpose: External pose control functions are declared in this file. 

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			09-11/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_POSECONTROLUTILITIES_H
#define VBS2FUSION_POSECONTROLUTILITIES_H



/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "data/Unit.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{

	class VBSFUSION_API PoseControlUtilities
	{
	public:
		/************************************************************************/
		/* EXTERNAL POSE CONTROL                                                */
		/************************************************************************/

		/*!
		Enable/Disable the ability to modify pose of specified unit externally.

		Deprecated. Use void applyExternalPose(Unit& unit, bool status)
		*/
		VBSFUSION_DPR(UPCN001) static void setExternalPose(Unit& unit, bool status);

		/*!
		Returns true if pose of given unit is controlled externally. 
		*/
		static bool getExternalPose( const Unit& unit);

		/************************************************************************/
		/* UPPER BODY CONTROL                                                   */
		/************************************************************************/

		/*!
		Enable/Disable upper body pose control of specified unit.
		Upper Body pose control of unit is disabled by default.

		Deprecated. Use void applyExternalPoseUpBody(Unit& unit ,bool status)
		*/
		VBSFUSION_DPR(UPCN003) static void setExternalPoseUpBody(Unit& unit ,bool status);

		/*!
		Returns true if upper body pose of given unit is Enabled.
		*/
		static bool getExternalPoseUpBody( const Unit& unit);


		/************************************************************************/
		/* SKELETON CONTROL                                                    */
		/************************************************************************/

		/*!
		Enable/Disable skeleton control of a given unit.
		Skeleton control is disabled by default.

		Deprecated. Use void applyExternalPoseSkeleton(Unit& unit, bool status)
		*/
		VBSFUSION_DPR(UPCN002) static void setExternalPoseSkeleton(Unit& unit, bool status);

		/*!
		Return true if skeleton control is enabled.
		*/
		static bool getExternalPoseSkeleton( const Unit& unit);

		/*!
		Disable Pose controlling of the unit. 
		If user unload the fusion plug in during the mission. 
		This should be called in onUnload callback.
		*/
		static bool DisablePoseControl( const Unit& unit);

		/*!
		@description 

		Enable/Disable the ability to modify pose of specified unit externally.

		@locality

		Locally applied, Locally effected

		@version  [VBS2Fusion v3.15]

		@param unit - unit that going to modify the pose.
		@param status - True or false. Enable or desable the ability to modify the pose externally.

		@return None

		@example

		@code

		PoseControlUtilities::applyExternalPose(unit1, true);

		@endcode

		@overloaded None

		@related 

		@remarks This is a replication of void setExternalPose(Unit& unit, bool status)
		*/
		static void applyExternalPose(Unit& unit, bool status);
		
		/*!
		@description 

		Enable/Disable skeleton control of a given unit.
		Skeleton control is disabled by default.

		@locality
		
		Locally applied, Locally effected

		@version  [VBS2Fusion v3.15]

		@param unit - unit that going to modify the pose.
		@param status - True or false. Enable or disable the ability to modify the skeleton externally.

		@return None

		@example

		@code
		
		//Create the pose control unit
		PoseControlUnit pcUnit;
		playerUnit = MissionUtilities::getPlayer();

		position3D pos(playerUnit.getPosition().getX()+3,
		playerUnit.getPosition().getZ(),
		playerUnit.getPosition().getY());

		UnitUtilities::CreateUnit(pcUnit, pos);

		//Activate applyExternalPoseSkeleton
		PoseControlUtilities::applyExternalPose(pcUnit, true);
		PoseControlUtilities::applyExternalPoseSkeleton(pcUnit, true);

		//Deactivate applyExternalPoseSkeleton
		PoseControlUtilities::applyExternalPoseSkeleton(pcUnit, false);

		@endcode

		@overloaded None

		@related 
		
		PoseControlUtilities::applyExternalPose(Unit& unit, bool status)

		@remarks This is a replication of void setExternalPoseSkeleton(Unit& unit, bool status)
		*/
		static void applyExternalPoseSkeleton( const Unit& unit, bool status);

		/*!
		@description 

		Enable/Disable upper body pose control of specified unit.
		Upper Body pose control of unit is disabled by default.

		@locality
		
		Locally applied, Locally effected

		@version  [VBS2Fusion v3.15]

		@param unit - unit that need to have torso/view/aim matrices externally controlled.
		@param status - True or false. Enable or disable the ability to modify the torso/view/aim matrices externally controlled.

		@return None

		@example

		@code

		//Create the pose control unit
		PoseControlUnit pcUnit;
		playerUnit = MissionUtilities::getPlayer();

		position3D pos(playerUnit.getPosition().getX()+3,
		playerUnit.getPosition().getZ(),
		playerUnit.getPosition().getY());

		UnitUtilities::CreateUnit(pcUnit, pos);

		//Apply external pose to unit's upper body
		PoseControlUtilities::applyExternalPose(pcUnit, true);
		PoseControlUtilities::applyExternalPoseUpBody(pcUnit, true);

		//Deactivate external pose to unit's upper body
		PoseControlUtilities::applyExternalPoseUpBody(pcUnit, false);

		@endcode

		@overloaded None

		@related 
		
		PoseControlUtilities::applyExternalPose(Unit& unit, bool status)

		@remarks This is a replication of void setExternalPoseUpBody(Unit& unit ,bool status)
		*/
		static void applyExternalPoseUpBody( const Unit& unit ,bool status);

		/*!
		@description 
		Enable/Disable skeleton movement of a given unit.
		Skeleton movement control is disabled by default.

		@locality

		Locally applied, locally effected.

		@version  [VBSFusion v3.15]

		@param unit - unit to set external movement controlled.

		@param status - true/false.

		@return Nothing.

		@example

		@code

		PoseControlUtilities::setExternalPose(unit1,true);
		PoseControlUtilities::applyExternalMovementControlled(unit1,true);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static void applyExternalMovementControlled( const Unit& unit, bool status);

		/*!
		@description 
		Return true if skeleton movement control is enabled.

		@locality

		Locally applied.

		@version  [VBSFusion v3.15]

		@param unit - unit to get external movement controlled.

		@return status of skeleton movement control enabled/disabled.

		@example

		@code

		//Get the movement control is enabled or not.
		bool b = PoseControlUtilities::getExternalMovementControlled(unit1);

		//Display the return value as string.
		displayString = "External Movement Controlled : "+conversions::BoolToString(b);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static bool getExternalMovementControlled( const Unit& unit);

		/*!
		@description 
		Enable/Disable camera control of a given unit.
		Camera control is disabled by default.

		@locality

		Locally applied, locally effected.

		@version  [VBSFusion v3.15]

		@param unit - passed unit

		@param status - enable/disable camera control

		@return Nothing.

		@example

		@code

		PoseControlUtilities::setExternalPose(unit1,true);

		PoseControlUtilities::applyExternalCameraControlled(unit1,true);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static void applyExternalCameraControlled( const Unit& unit, bool status);

		/*!
		@description 
		Return true if camera control is enabled.

		@locality

		Locally applied.

		@version  [VBSFusion v3.15]

		@param unit - passed unit

		@return bool - camera control status enabled/disabled

		@example

		@code

		//Get the camera control is enabled or not.
		bool b = PoseControlUtilities::getExternalCameraControlled(unit1);

		//Display the return value as string.
		displayString = "External Camera Controlled : "+conversions::BoolToString(b);

		@endcode

		@overloaded

		@related

		@remarks
		*/    
		static bool getExternalCameraControlled( const Unit& unit);

		/*!
		@description 
		Enable/Disable lower body skeleton control of a given unit.
		Lower body skeleton control is disabled by default.

		@locality

		Locally applied, locally effected.

		@version  [VBSFusion v3.15]

		@param unit - passed unit

		@param status - enable/disable control

		@return Nothing.

		@example

		@code

		PoseControlUtilities::setExternalPose(unit1,true);

		PoseControlUtilities::applyExternalPoseLowerBody(unit1,true);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static void applyExternalPoseLowerBody( const Unit& unit, bool status);

		/*!
		@description 
		Return true if lower body skeleton control is enabled.

		@locality

		Locally applied.

		@version  [VBSFusion v3.15]

		@param unit - passed unit

		@return bool - control status enabled/disabled

		@example

		@code

		//Get the lower bode skeleton is enabled or not.
		bool b = PoseControlUtilities::getExternalPoseLowerBody(unit1);

		//Display the return value as string.
		displayString = "External Pose Lower Body : "+conversions::BoolToString(b);

		@endcode

		@overloaded

		@related

		@remarks
		*/    
		static bool getExternalPoseLowerBody( const Unit& unit);

		/*!
		@description 
		Return true if view is aligned with head.

		@locality

		Locally applied.

		@version  [VBSFusion v3.15]

		@param 

		@return bool - view aligned with head or not

		@example

		@code

		bool b = PoseControlUtilities::isViewAlignedWithHead();

		displayString = "View Aligned With Head : "+conversions::BoolToString(b);

		@endcode

		@overloaded

		@related

		@remarks
		*/    
		static bool isViewAlignedWithHead();

		/*!
		@description 
		Return Id of bone for given unit

		@locality

		Globally applied.

		@version  [VBSFusion v3.15]

		@param unit - desired unit.

		@param boneName - bone name.

		@return - bone Id

		@example

		@code

		//Get the Id of bone for a given unit.
		int Id = PoseControlUtilities::getBoneID(unit1,string("head"));

		//Display the return value as an Integer.
		displayString = "Skeleton ID : " +conversions::IntToString(Id);

		@endcode

		@overloaded

		@related

		@remarks
		*/   
		static int getBoneID( const Unit& unit,  const std::string& boneName);

	};	
}

#endif //VBS2FUSION_POSECONTROLUTILITIES_H