
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	WorldUtilities.h

Purpose: utility class for world.

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			09-07/2010	YFP: Original Implementation
	1.01		20-09/2010	YFP: methods added, getOrigin()
	1.02		03-08-2011	NDB: Added void onMapSignleClick(string command)
								 void onDoubleClick(string Map, string command)
	1.03		22-09-2011	CGS: Added double getDistance(position3D stPos, position3D enPos)
	1.04		23-09/2011	NDB: Added Methods
									double getDeclination()
									void applyDeclination(double declination)
	1.05		26-09/2011	NDB: Added Methods
									vector<string> getShapeFileList()
									ControllableObject getAttachedObject(ControllableObject& co)
									vector<ControllableObject> getAllAttachedObjects(ControllableObject& co)
									string getWorldName()
									void applyOrigin(double easting, double northing, double zone, string hemisphere)
									void switchAllLights(LIGHT_MODE lightMode)
									void initAmbientLife()
	1.06		18-11-2011	SSD: Added Methods
									Unit getNearestUnit(position3D pos)
									Unit getNearestUnit(position3D pos, string type)
	1.07		08-12-2011	DMB: Added Methods
									void drawGeometry(int lowLX, int lowLY, int upperRX, int upperRY)

/*****************************************************************************/

#ifndef VBS2FUSION_WORLDUTILITES_H
#define VBS2FUSION_WORLDUTILITES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <list>
// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "VBSFusionDefinitions.h"
#include "position2D.h"
#include "data/Vehicle.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{
	
	class VBSFUSION_API WorldUtilities
	{
	public:

		/*!
		Returns cost in the cost map on a given position.
		type can be 0 for vehicles and 1 for unit.
		*/
		static COST_TYPE getPositionCost(const position3D& pos , int type );

		/*!
		Returns center position of nearest cost field starting the search position specified by pos.
		list of cost types can query the search field. Maximum radius for search specified by radius.

		
		*/
		static position3D getNearestCostPosition(const position3D& pos, std::list<COST_TYPE> costTypes,double radius, const ControllableObject& co) ;
		
		/*!
		returns list of network IDs of given types within a radius of a given position.
		*/
		static std::list<NetworkID> getNearestObjects(const position3D& pos, std::list<std::string> types , int radius);

		/*!
		Get the current Universal Transverse Mercator (UTM) origin of the Map.
-		This returns UTM_POSITION. 	If -1 returned for east,north or zone
		*/
		static UTM_POSITION getOrigin();

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given from damage
		and range of indirect damage given from range.
		*/
		static void createIndirectDamage(const position3D & position, int damage, int range);
		
		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles.
		*/
		static void createIndirectDamage(const position3D& position, int damage, int range, bool disableParticles);

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles ,
		disable/enable creation of crates specified by disableCrates.
		*/
		static void createIndirectDamage(const position3D& position, int damage, int range, bool disableParticles, bool disableCrates);

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles ,
		disable/enable creation of crates specified by disableCrates and disable/enable damage on all objects by disableDamage.
		*/
		static void createIndirectDamage(const position3D& position, int damage, int range, bool disableParticles, bool disableCrates, bool disableDamage);

		/*!
		Creates an indirect damage at the specified position according to the strength of damage given by damage,
		damage range given by range , disable/enable particle effects specified by disableParticles ,
		disable/enable creation of crates specified by disableCrates and disable/enable damage on all objects by disableDamage and disable/enable damage effects on units and vehicles.
		*/
		static void createIndirectDamage(const position3D& position, int damage, int range, bool disableParticles, bool disableCrates, bool disableDamage, bool disableDamageEntities);

		/*!
		Converts position in world space into screen space.
		Deprecated. Use applyWorldToScreen(position3D pos)
		*/
		VBSFUSION_DPR(UWLD001) static position2D worldToScreen(position3D pos);

		/*!
		Returns the position on VBS world corresponding to the given point on screen. 
		This function ignores any VBS objects and only return a point on VBS terrain.
		Deprecated. Use applyScreenToWorld(position2D pos)
		*/
		VBSFUSION_DPR(UWLD002) static position3D screenToWorld(position2D pos);
		

		/*!
		Returns the distance in meters between two positions. 
		*/
		static double getDistance(const position3D& stPos, const position3D& enPos) ;
		
		/*!
		Returns the magnetic declination from VBS
		*/
		static double getDeclination() ;

		/*!
		Sets the magnetic declination in VBS
		*/
		static void applyDeclination(double declination);

		/*!
		Returns the object to which an object or location is attached to. If it is unattached, then objNull is returned.
		*/
		static ControllableObject getAttachedObject(const ControllableObject& co);

		/*!
		Returns the object that is attached to this location or object. If nothing is attached, then an empty array is returned.
		*/
		static std::vector<ControllableObject> getAllAttachedObjects(const ControllableObject& co);

		/*!
		Return the class name of the currently loaded map (e.g. "Intro" for Rahmadi). 
		*/
		static std::string getWorldName();

		/*!
		Sets the origin for LVCGame
		*/
		static void applyOrigin(double easting, double northing, double zone, const  std::string& hemisphere);

		/*!
		Returns the center position (i.e. X and Z coorinates);; of the currently loaded world. 
		*/
		static std::vector<float> GetWorldCenter();

		/*!
		Returns the center position (i.e. X and Z coorinates);; of the currently loaded world. 
		*/
		static std::vector<double> GetWorldCenterEx();

		/*!	
		Returns the nearest unit object to the given position within a range of 
		50 meters.
		*/
		static Unit getNearestUnit(const position3D& pos);

		/*!
		Returns the nearest unit object to the given position of the given type within 
		a range of 50 meters. An invalid unit object will be returned if a proper unit
		type is not passed or there is no object within the range.
		*/
		static Unit getNearestUnit(const position3D& pos, const std::string& type);

		/*!
		According to the parameters crater will be created and particle effects of an explosion at the location specified 
		and no damage will be done by this function.
		Currently crater type not accommodated with VBS. So use empty string for type.  
		*/
		static ControllableObject createCrater(const std::string& type, const position3D& position, std::list<Marker> markerList, float randDist, 
			bool asl, float duartion, float size, const std::string& effectsType, float effectTTL);

		/*!
		According to the parameters crater will be created and particle effects of an explosion at the location specified 
		and no damage will be done by this function.
		Currently crater type not accommodated with VBS. So use empty string for type.  
		*/
		static ControllableObject createCrater(const std::string& type, const position3D& position, std::list<Marker> markerList, double randDist, 
			bool asl, double duartion, double size, const std::string& effectsType, double effectTTL);

		/*!
		Draws geometry of the objects and landscape in given rectangle.

		lowLX	- X value of the lower-left coordinates
		lowLY	- Y value of the lower-left coordinates
		upperRX	- X value of the upper-right coordinates
		upperRY	- Y value of the upper-right coordinates

		Deprecated. Use void applyGeometricDrawing(int lowLX, int lowLY, int upperRX, int upperRY);
		*/
		VBSFUSION_DPR(UWLD003) static void drawGeometry(int lowLX, int lowLY, int upperRX, int upperRY);

		/*!
		Returns the nearest object (vehicle or unit) to the given position within 
		a range of 50 meters. An invalid object will be returned if there is no object
		within the range. 
		*/
		static ControllableObject getNearestObject(const position3D& pos) ;

		/*!
		Returns the nearest object (vehicle or unit) to the given position of the 
		given type within a range of 50 meters.An invalid object will be returned 
		if there is no object within the range or a proper type is not passed.
		*/
		static ControllableObject getNearestObject(const position3D& pos, const std::string& type);

		/*!
		Returns objects (vehicles and units) in the circle with given radius.
		*/
		static std::vector<ControllableObject> getNearObjects(const position3D& pos, int radius);

		/*!
		Returns objects (vehicles and units) of given type (or its subtype) in the
		circle with given radius.
		*/
		static std::vector<ControllableObject> getNearObjects(const position3D& pos, int radius, const std::string& type);

		/*!

		@description - Returns the squared value of the straight distance between the two objects( height/Y-axis is ignored ).
		Calculation ignores the elevation and calculates the distance using Pythagoras theory.

		@locality

		Globally Applied

		@version - [VBS2Fusion v3.10.1]

		@param objA - Start controllable Object.
		@param objB - End  controllable Object.

		@return double.

		@example

		@code

		//The Object to be created
		ControllableObjectUtilities::createObject(co1,"vbs2_us_army_rifleman_w_m4_assmg",position3D(2100,2150,0));

		//The Object to be created
		ControllableObjectUtilities::createObject(co2,"vbs2_us_army_rifleman_w_m4_assmg",position3D(2300,2350,0));

		//To display the Distane as a String, use the Function conversions::DoubleToString as shown below.
		displayString= "Squared 2D Displacement - "+conversions::DoubleToString(WorldUtilities::getSquared2DDisplacement(co1, co2));

		@endcode

		@overloaded  

		getSquared2DDisplacement(position2D positionA, position2D positionB)
		getSquared2DDisplacement(position3D positionA, position3D positionB)

		@relates to

		@remarks 

		*/
		static double getSquared2DDisplacement(const ControllableObject& objA, const ControllableObject& objB);

		/*!
		Initializes the Ambient Life. 
		"Ambient Life" are things like bugs and birds moving randomly on map. Not all maps may have these defined.
		Should enable the environmental effects to see the effects. 

		Deprecated. Use void applyAmbientLifeInitialization();
		*/
		VBSFUSION_DPR(UWLD004) static void initAmbientLife();

		/*!
		 Preload all textures and models around given Position to avoid visual artifacts after camera is moved.
		 Should be used before any abrupt camera change/cut.
		 Returns true once all data is ready.
		*/
		static bool applyCameraPreload( const position3D& position);

		/*!
		Returns the compass direction from one position to another. 
		@param pivot - A 3D position.
		@param target - Another 3D position.
		@return double - direction from pivot to target.
		*/
		static double getRelativeDirection(const position3D& pivot, const position3D& target);

		/*!
		Returns the compass direction from one object to another. 
		@param pivot - A controllable object.
		@param target - Another controllable object.
		@return double - direction from pivot to target.
		*/
		static double getCompassDirection(const ControllableObject& pivot, const ControllableObject& target);
		
		/*!
		Returns the compass direction from a given object to a given position. 
		@param pivot - A controllable object.
		@param target - A 3D position.
		@return double - direction from pivot to target.
		*/
		static double getRelativeDirection(const ControllableObject& pivot, const position3D& target);

		/*!
		Returns the compass direction from a given position to a given object. 
		@param pivot - A 3D position.
		@param target - A controllable object.
		@return double - direction from pivot to target.
		*/
		static double getRelativeDirection(const position3D& pivot, const ControllableObject& target);
		/*!
		Use this function to determine if a position lies within a certain angle from another position. 
		@param pivot - A 3D position.
		@param direction - Checked direction.
		@param angWidth - Checked angle. Angle sector split in half by the direction.
		@param target - Checked 3D position.
		@return bool - Returns true if the target position lies within the sector defined by pivot position, direction and angle width.
		*/
		static bool isInsideAngleSector(const position3D& pivot, double direction, double angWidth, const position3D& target);


		/*! 
		Return squared value of the straight distance between the two positions( height/Y-axis is ignored ).
		Calculation ignores the elevation and calculates the distance using pythagoras theory.
		@param positionA - Start 3D position.
		@param positionB - End 3D position.
		@return - double.
		*/
		static double getSquared2DDisplacement(const position3D& positionA, const position3D& positionB);

		/*! 
		Return squared value of the straight distance between the two positions.
		@param positionA - Start 2D position.
		@param positionB - End 2D position.
		@return - double.
		*/
		static double getSquared2DDisplacement(const position2D& positionA, const position2D& positionB);

		/*!

		@description 
		
		Rotates the 'targetPoint' around the 'centerPoint' clockwise in the given angle.

		@locality 
		
		Globally Applied

		@version [VBS2Fusion v3.10.1]

		@param centerPoint - Centered 2D position.
		@param angle - Angle is in degrees.
		@param targetPoint - The 2D Position which used to rotate.

		@return The position after rotate targetPoint around centerPoint by given angle.

		@example 

		@code
				
		//To display the position as a String, use the Function getVBSPosition as shown below.
		displayString = "Rotated point - " +WorldUtilities::getRotatedPoint(position2D(15,30),60,position2D(40,50)).getVBSPosition2D();

		@endcode

		@overloaded 
		
		WorldUtilites::getRotatedPoint(position3D centerPoint, double angle, position3D targetPoint)
		WorldUtilites::getRotatedPoint(ControllableObject& pivotObj, double angle, ControllableObject& targetObj)

		@related

		@remarks 

		*/
		static position2D getRotatedPoint(const position2D& centerPoint,double angle, const position2D& targetPoint);

		/*!

		@description

		Rotates the 'targetPoint' around the 'centerPoint' clockwise in the given angle.Rotation is done around 
		the Y-axis (from top-down view).Therefore Y components of both positions are ignored. After the rotation, 
		Y component will remain the same and only x and z components will be changed in the returned value.
		
		@locality 

		Globally Applied

		@version [VBS2Fusion v3.10.1]

		@param centerPoint - Centered 3D position.
		@param angle - Angle is in degrees.
		@param targetPoint - The 3D Position which used to rotate.

		@return position3D - The position after rotate targetPoint around centerPoint by given angle.

		@example 

		@code

		//To display the position as a String, use the Function getVBSPosition as shown below.
		displayString = "Rotated point - " +WorldUtilities::getRotatedPoint(position3D(15,30,50),60,position3D(40,50,30)).getVBSPosition();

		@endcode

		@overloaded

		WorldUtilites::getRotatedPoint(position2D centerPoint, double angle, position2D targetPoint)
		WorldUtilites::getRotatedPoint(ControllableObject& pivotObj, double angle, ControllableObject& targetObj)

		@related

		@remarks 

		*/
		static position3D getRotatedPoint(const position3D& centerPoint, double angle, const position3D& targetPoint);

		/*!

		@description

		Rotates the 'targetObj' around the 'pivotObj' clockwise in the given angle.Rotation is done around the Y-axis (from top-down view).
		Therefore Y components (height) of both object positions are ignored.After the rotation, Y component will remain the same and 
		only x and z components will be changed in the returned position value.

		@locality 

		Globally Applied

		@version [VBS2Fusion v3.10.1]

		@param pivotObj - Centered controllable object.
		@param angle - Angle is in degrees.
		@param targetObj - The controllable object which used to rotate.

		@return position3D - The position after rotate targetObj around pivotObj by given angle.
		
		@example 

		@code

		//The Object to be created
		ControllableObjectUtilities::createObject(co1,"vbs2_us_army_rifleman_w_m4_assmg",position3D(2100,2150,0));

		//The Object to be created
		ControllableObjectUtilities::createObject(co2,"vbs2_us_army_rifleman_w_m4_assmg",position3D(2300,2350,0));

		//To display the position as a String, use the Function getVBSPosition as shown below.
		displayString = "Rotated point - " +WorldUtilities::getRotatedPoint(co1,45,co2).getVBSPosition();

		@endcode

		@overloaded

		WorldUtilites::getRotatedPoint(position2D centerPoint, double angle, position2D targetPoint)
		WorldUtilites::getRotatedPoint(position3D centerPoint, double angle, position3D targetPoint)

		@related

		@remarks 

		*/
		static position3D getRotatedPoint(const ControllableObject& pivotObj, double angle, const ControllableObject& targetObj);

		/*!

		@description

		Returns the position that has specified distance and compass direction from the passed position.

		@locality 

		Globally Applied

		@version [VBS2Fusion v3.10.1]

		@param pos - A 2D position.
		@param distance - considered distance from the pos in meters.
		@param direction - compass direction from the pos in degrees.

		@return position2D.

		@example 

		@code

		//To display the position as a String, use the Function getVBSPosition as shown below.
		displayString = "Relative position - "+WorldUtilities::getRelativePosition(position2D(100,150),40,60).getVBSPosition2D();

		@endcode

		@overloaded

		WorldUtilites::getRelativePosition(position2D pos, double distance, double direction, double offsetDistance)
		WorldUtilites::getRelativePosition(position3D pos, double distance, double direction, double offsetDistance)
		WorldUtilites::getRelativePosition(position3D pos, double distance, double direction)

		@related

		@remarks  
		*/
		static position2D getRelativePosition(const position2D& pos, double distance, double direction);

		/*!
		@description

		Calculates the Position that has specified distance and compass direction from the passed(input) position and 
		adds an offset to it at a right-angle, by the given offsetDistance.

		@locality 

		Globally Applied

		@version [VBS2Fusion v3.10.1]

		@param pos - A 2D position.
		@param distance - Considered distance from the pos in meters.
		@param direction - Compass direction from the pos in degrees.
		@param offsetDistance - Distance offset, at a 90 degree angle, added to the relative position which 
				given by along the compass direction with distance in meters.

		@return position2D.

		@example 

		@code

		//To display the position as a String, use the Function getVBSPosition as shown below.
		displayString = "Relative position - "+WorldUtilities::getRelativePosition(position2D(100,150),40,60,10).getVBSPosition2D();

		@endcode

		@overloaded

		WorldUtilites::getRelativePosition(position2D pos, double distance, double direction)
		WorldUtilites::getRelativePosition(position3D pos, double distance, double direction, double offsetDistance)
		WorldUtilites::getRelativePosition(position3D pos, double distance, double direction)

		@related

		@remarks  
		*/
		static position2D getRelativePosition(const position2D& pos, double distance, double direction, double offsetDistance);

		/*!

		@description
		Returns the position that has specified distance and compass direction from the passed position.

		@locality 

		Globally Applied

		@version [VBS2Fusion v3.10.1]

		@param pos - A 3D position.
		@param distance - considered distance from the pos in meters.
		@param direction - compass direction from the pos in degrees.

		@return position3D.

		@example 

		@code

		//To display the position as a String, use the Function getVBSPosition as shown below.
		displayString = "Relative position - "+WorldUtilities::getRelativePosition(position2D(130,170),40,60).getVBSPosition2D();

		@endcode

		@overloaded

		WorldUtilites::getRelativePosition(position2D pos, double distance, double direction)
		WorldUtilites::getRelativePosition(position2D pos, double distance, double direction, double offsetDistance)
		WorldUtilites::getRelativePosition(position3D pos, double distance, double direction, double offsetDistance)

		@related

		@remarks 

		*/
		static position3D getRelativePosition(const position3D& pos, double distance, double direction);

		/*!

		@description
		Calculates the Position that has specified distance and compass direction from the passed(input) position and adds an offset to it at 
		a right-angle, by the given offsetDistance.	All the calculations are done in the XZ plane (Y axis is not used or changed).

		@locality 

		Globally Applied

		@version [VBS2Fusion v3.10.1]

		@param pos - A 3D position.
		@param distance - Considered distance from the pos in meters.
		@param direction - Compass direction from the pos in degrees.
		@param offsetDistance - Distance offset, at a 90 degree angle, added to the relative position which 
				given by along the compass direction with distance in meters.

		@return position3D.

		@example 

		@code
		//To display the position as a String, use the Function getVBSPosition as shown below.
		displayString = "Relative position - "+WorldUtilities::getRelativePosition(position3D(130,170,30),40,60,10).getVBSPosition();

		@endcode

		@overloaded

		WorldUtilites::getRelativePosition(position2D pos, double distance, double direction)
		WorldUtilites::getRelativePosition(position2D pos, double distance, double direction, double offsetDistance)
		WorldUtilites::getRelativePosition(position3D pos, double distance, double direction)

		@related

		@remarks 
		*/
		static position3D getRelativePosition(const position3D& pos, double distance, double direction, double offsetDistance);

		/*!

		@description
		Returns the relative direction from one object to another object. Put another way, this function returns 
		the number of degrees an object would have to rotate in order to face the wanted object.

		@locality 

		Globally Applied

		@version [VBS2Fusion v3.10.1]

		@param pivot - A controllable object.
		@param target - Another controllable object.

		@return double - Direction from pivot to target in degrees.

		@example 

		@code

		//The Object to be created
		ControllableObjectUtilities::createObject(co1,"vbs2_us_army_rifleman_w_m4_assmg",position3D(2100,2150,0));

		//The Object to be created
		ControllableObjectUtilities::createObject(co2,"vbs2_us_army_rifleman_w_m4_assmg",position3D(2300,2350,0));

		//To display the direction as a String use conversions::DoubleToString as shown below.
		displayString = "Relative direction - "+conversions::DoubleToString(WorldUtilities::getRelativeDirection(co1,co2));

		@endcode

		@overloaded

		WorldUtilites::getRelativeDirection(ControllableObject& pivot, position3D target)

		@related

		@remarks

		*/
		static double getRelativeDirection(const ControllableObject& pivot, const ControllableObject& target);

		/*!

		@description
		This function rotates a point in object's model space and returns it in world coordinates. 
		If last parameter is not specified (center of rotation) then objects center will be used.

		@locality 

		Globally Applied

		@version [VBS2Fusion v3.10.1]

		@param target - Point being rotated.
		@param object - Object used to return world coordinates.
		@param angle - Angle(degrees) by which the point will be rotated.
		@param center(optional) - Position around which to rotate target, default is Object center.

		@return position3D - Rotated position in world coordinates.

		@example 

		@code

		//The Object to be created
		ControllableObjectUtilities::createObject(co1,"vbs2_us_army_rifleman_w_m4_assmg",position3D(2100,2150,0));

		//To display the position as a String, use the Function getVBSPosition as shown below.
		displayString = "Relative model position - "+WorldUtilities::getRelativeModelPosition(position3D(1500,1350,0),co1,45,position3D(1650,1600,0)).getVBSPosition();

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static position3D getRelativeModelPosition(const position3D& target, const ControllableObject& object, double angle, const position3D& center = position3D());

		/*!

		@description 
		Invokes danger FSM for given units. If danger FSM is invoked for a unit, It may start to flee.

		@locality

		Globally applied globally effected

		@version [VBS2Fusion v3.10.1]

		@param radius - define a circular area of danger (in meters) .
		@param cause -  reason for fleeing.
		@param position - position where danger is located.
		@param duration - time duration in seconds.

		@return None.

		@example

		@code

		//Danger FSM is invoked within the range of 50 meters for time duration 60 seconds.
		WorldUtilities::applyDanger(50,string("FIRE"),player.getPosition(),60);

		@encode

		@overloaded

		WorldUtilities::applyDanger(vector<Unit> unitList, string& cause, position3D& position, double duration)

		@related

		@remarks Since it depends on present state of FSM, there is no requirement to show the same effect always.

		*/
		static void applyDanger( double radius, std::string& cause, position3D& position, double duration);

		/*!

		@description

		Invokes danger FSM for given units. If danger FSM is invoked for a unit, It may start to flee.

		@locality

		Globally applied globally effected.
		
		@version [VBS2Fusion v3.10.1]
		
		@param unitList - List of units that should start to flee.
		@param cause -  reason for fleeing.
		@param position - position where danger is located.
		@param duration - time duration in seconds.
		
		@return Nothing.
		
		@example

		@code
		
		//Danger FSM is invoked for time duration 60 seconds for given units in the vector "vec".
		WorldUtilities::applyDanger(vec,string("FIRE"),player.getPosition(),60);

		@endcode

		@overloaded 

		WorldUtilities::applyDanger(double radius, string& cause, position3D& position, double duration)

		@related

		@remarks Since it depends on present state of FSM, there is no requirement to show the same effect always.

		*/
		static void applyDanger(std::vector<Unit> unitList, std::string& cause, position3D& position, double duration);

		/*!
		@description  

		Switches lighting mode for all street lamps.  

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v3.12] 

		@param lightMode - LIGHT_MODE. Available modes are ( LIGHT_ON : Switch on all streetlamps, LIGHT_OFF : Switch off all streetlamps, LIGHT_AUTO : Switch on all streetlamps automatically during the nighttime )

		@return nothing 

		@example

		@code

		// Switch on all street lamps.
		WorldUtilities::applySwitchOn(LIGHT_ON);

		@endcode

		@overloaded 

		@related  

		@remarks This function works only for the street lamps that come by default for each respective terrain.
		*/
		static void applySwitchOn(LIGHT_MODE lightMode);

		/*!

		@description

		Used to convert positions in world space into screen (UI) space within the VBS2 environment.
		If a specified position is not within the current map view, an empty array is returned.

		@locality

		Globally Applied, Globally Effected

		@version [VBS2Fusion v3.12]

		@param pos - position3D Which need to be converted.

		@return position2D - Converted position of the Screen.

		@example

		@code

		//The vehicle to be created
		Vehicle v1;

		position2D pos;

		pos = WorldUtilities::applyWorldToScreen(v1.getPosition());

		displayString+="The screen position is"+pos.getVBSPosition2D();

		@endcode

		@overloaded 

		Nothing.

		@related

		@remarks This is a replication of position2D worldToScreen(position3D pos);

		@remarks For accurate values execute the function in 3rd person mode.

		@remarks To obtain the screen size position of an object created in a client machine within a network, use the function 
		MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function WorldUtilities::applyWorldToScreen(position3D pos).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static position2D applyWorldToScreen(const position3D& pos);

		/*!

		@description

		Used to convert positions in world space into screen (UI) space within the VBS2 environment.
		If a specified position is not within the current map view, an empty array is returned.

		@locality

		Globally Applied, Globally Effected

		@version [VBS2Fusion v3.12]

		@param pos - position2D Which need to be converted.

		@return position3D - Rotated position in the VBS2 Environment.

		@example

		@code

		//The vehicle to be created
		Vehicle v1;

		position2D pos2;
		position3D pos3;

		pos2 = WorldUtilities::applyWorldToScreen(v1.getPosition());
		pos3 = WorldUtilities::applyScreenToWorld(pos2);

		displayString+="The world position is"+pos3.getVBSPosition();

		@endcode

		@overloaded 

		Nothing.

		@related

		@remarks This is a replication of position3D screenToWorld(position2D pos);

		@remarks For accurate values execute the function in 3rd person mode.

		@remarks To obtain the world position from the screen position of an object created in a client machine within a network, use the function 
		MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function WorldUtilities::applyScreenToWorld(position2D pos).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static position3D applyScreenToWorld(position2D pos);

		/*!
		@description

		Draws geometry of the objects and landscape in given rectangle.

		@locality

		Locally Effected

		@version [VBS2Fusion v3.12]

		@param lowLX	- X value of the lower-left coordinates
		@param lowLY	- Y value of the lower-left coordinates
		@param upperRX	- X value of the upper-right coordinates
		@param upperRY	- Y value of the upper-right coordinates

		@return None.

		@example

		@code

		WorldUtilities::applyGeometricDrawing(2400.25463577542,2280.12866227765,2301.77099999417,2179.34106001203);

		@endcode

		@overloaded 

		Nothing.

		@related

		@remarks This is a replication of void drawGeometry(int lowLX, int lowLY, int upperRX, int upperRY);
		*/
		static void applyGeometricDrawing(int lowLX, int lowLY, int upperRX, int upperRY);

		/*!
		@description

		Used to initialize the Ambient Life within the VBS2 environment. 
		"Ambient Life" is a feature which controls the random movement and sounds of bugs and birds in the map. Not all maps may have these defined.
		Should enable the environmental effects to see the effects. 

		@locality

		Globally Applied, Globally Effected

		@version [VBS2Fusion v3.12]

		@param None.
	
		@return None.

		@example

		@code

		WorldUtilities::applyAmbientLifeInitialization();

		@endcode

		@overloaded 

		Nothing.

		@related

		@remarks This is a replication of void initAmbientLife();
		*/
		static void applyAmbientLifeInitialization();

	};

}


#endif //VBS2FUSION_WORLDUTILITES_H