/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	EnvironmentStateUtilities.h

Purpose:

	This file contains the declaration of the EnvironmentStateUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			12-11/2009	RMR: Original Implementation

	2.0			10-02/2010	RMR: Added	vector<float> loadWind()
										void applyWind(vector<float> _value)
										void applyWind(EnvironmentState& state)
										applyWind(EnvironmentState& state, vector<float> _value)
	2.01		10-02/2010	MHA: Comments Checked
	2.02		03-05/2011	SSD: Added	void applySurfaceMoisture(EnvironmentState& state);
										void applySurfaceMoisture(double value);
										void updateSurfaceMoisture(EnvironmentState& state);
										void applyMaxRainDensity(EnvironmentState& state);
										void applyMaxRainDensity(double value);
										void updateMaxRainDensity(EnvironmentState& state);
										void applyMaxEvaporation(EnvironmentState& state);
										void applyMaxEvaporation(double value);
										void updateMaxEvaporation(EnvironmentState& state);
										void applySurfaceDrainageSpeed(EnvironmentState& state);
										void applySurfaceDrainageSpeed(double value);
										void updateSurfaceDrainageSpeed(EnvironmentState& state);
										void applyWaterLevel(EnvironmentState& state);
										void applyWaterLevel(double value);
										vector<int> getSunRise(int year, int month, int date, int hours = 0, int minutes = 0);
										vector<int> getSunsSt(int year, int month, int date, int hours = 0, int minutes = 0);
										void setRandomWeather(bool mode);
										bool getRandomWeather();

/************************************************************************/

#ifndef VBS2FUSION_ENVIRONMENT_STATE_UTILITIES_H
#define VBS2FUSION_ENVIRONMENT_STATE_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "data/EnvironmentState.h"
#include "conversions.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBSFusion
{
	class VBSFUSION_API EnvironmentStateUtilities
	{
	public:

		/*!
		
		@description 
		
		This function applies the parameters specified in state to the game environment. The value specified by state.getFog(), state.getRain(), state.getOvercast(), state.getTimeYear(), state.getTimeMonth(),  state.getTimeDay(), state.getTimeHours() and state.getTimeMinutes are applied to the game environment. No delay is used when applying these changes. Changes are therefore registered immediately.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		state.setFog(0.96);
		state.setRain(0.56);
		state.setOvercast(0.1);
		EnvironmentStateUtilities::applyEnvironmentState(state);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyEnvironmentState( const EnvironmentState& state, int delay);
		
		@related
		
		EnvironmentStateUtilities::loadEnvironmentState(EnvironmentState& state);
		
		@remarks To apply the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::applyEnvironmentState( const EnvironmentState& state) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		*/
		static void applyEnvironmentState( const EnvironmentState& state);
		

		/*!
		
		@description 
		
		This function applies the parameters specified in state to the game environment. The value specified by state.getFog(), state.getRain(), state.getOvercast(), state.getTimeYear(), state.getTimeMonth(), state.getTimeDay(), state.getTimeHours() and state.getTimeMinutes are applied to the game environment. The delay specified by delay is used when applying these changes. The value of delay is given in seconds. Changes are therefore registered not registered immediately. 

		@locality 

		Locally Applied and Globally Effected

		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@param delay - The value of delay.

		@return Nothing
		
		@example 
		
		@code
		
		state.setTimeDay(12);
		state.setRain(0.78);
		state.setOvercast(0.3);
		state.setFog(state.getFog());
		EnvironmentStateUtilities::applyEnvironmentState(state,10);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyEnvironmentState( const EnvironmentState& state);
		
		@related
		
		None
		
		@remarks To apply the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::applyEnvironmentState( const EnvironmentState& state, int delay)) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void applyEnvironmentState( const EnvironmentState& state, int delay);

		/*!
		
		@description 
		
		This function loads current environment state onto the state variable. The variables overcast, rain, fog, TimeYear, TimeMonth, TimeDay, TimeHours and TimeMinutes are updated from the game environment. Updates the amount of time elapsed as well.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@return  
		
		Nothing
		
		@example 
		
		@code
		
		state.setRain(0.5);
		state.setTimeElapsed(2.35);
		EnvironmentStateUtilities::loadEnvironmentState(state);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks To load the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: loadEnvironmentState(EnvironmentState& state); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/

		static void loadEnvironmentState(EnvironmentState& state);
		
		//----------------------------------------------------------

		/*!
		
		@description  
		
		This function applies the fog value specified by state.getFog()to the game without delay .

		@locality 

		Locally Applied and Globally Effected

		@version [VBSFusion v3.10] 

		@param state �  The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		state.setFog(0.122);
		EnvironmentStateUtilities::applyFog(state);

		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyFog( const EnvironmentState& state, int Delay);
		
		EnvironmentStateUtilities::applyFog(float& value);
		
		EnvironmentStateUtilities::applyFog(double& value);
		
		@related
		
		None
		
		@remarks To apply fog to environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyFog( const EnvironmentState& state); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/		
		
		static void applyFog( const EnvironmentState& state);	

		/*!
		
		@description 
		
		This function applies the value specified by state.getFog()to the fog value in the game using the delay specified.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@param delay � The delaying time in seconds.

		@return  
		
		@example 
		
		@code
		
		state.setFog(0.692);
		EnvironmentStateUtilities::applyFog(state,2);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyFog( const EnvironmentState& state)
		
		EnvironmentStateUtilities::applyFog(double& value);
		
		@related
		
		None
		
		@remarks To apply fog to environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyFog( const EnvironmentState& state, int Delay); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/

		static void applyFog( const EnvironmentState& state, int Delay);	

		/*!
		
		description 
		
		This function applies the value specified by state.getFog()to the fog value in the game without a delay.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		float f= 0.13;
		EnvironmentStateUtilities::applyFog(f);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyFog( const EnvironmentState& state)
		
		EnvironmentStateUtilities::applyFog(double& value);
		
		@related
		
		None
		
		@remarks To apply fog to environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyFog(float& value);(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyFog(float& value);	

		/*!
		
		description 
		
		This function applies the value specified by state.getFog()to the fog value in the game without a delay.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		double d=0.53669874588;
		EnvironmentStateUtilities::applyFog(d);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyFog( const EnvironmentState& state)
		
		EnvironmentStateUtilities:: applyFog(float& value);
		
		@related
		
		None
		
		@remarks To apply fog to environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyFog(double& value);(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyFog(double& value);	

		/*!
		
		@description 
		
		This function applies the value specified by state.getFog()to the fog value in the game with a delay specified.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.15]
		
		@param state � The environment for which the above function is applied.

		@param delay � The delaying time in seconds.

		@return Nothing
		
		@example 
		
		@code
		
		float fog =0.25;
		EnvironmentStateUtilities::applyFog(fog,1);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyFog( const EnvironmentState& state)
		
		EnvironmentStateUtilities::applyFog(float& value);
		
		@related
		
		None
		
		@remarks To apply fog to environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyFog(float& value, int Delay); (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyFog(float& value, int Delay);	

		/*!
		
		@description 
		
		This function applies the value specified by state.getFog()to the fog value in the game with a delay specified.
		
		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.15]
		
		@param state � The environment for which the above function is applied.

		@param delay � The delaying time in seconds.

		@return Nothing
		
		@example 
		
		@code
		
		float fog =0.25;
		EnvironmentStateUtilities::applyFog(fog,1);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyFog( const EnvironmentState& state)
		
		EnvironmentStateUtilities:: applyFog(float& value);
		
		@related
		
		None
		
		@remarks To apply fog to environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyFog(double& value, int Delay); (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyFog(double& value, int Delay);

		/*! 
		@description 
		
		This function returns the current fog setting value of the VBS3 environment.

		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param Nothing.

		@return Double
		
		@example 
		
		@code
		
		displayString="fog level: "+conversions::DoubleToString(EnvironmentStateUtilities::loadFog());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		EnvironmentStateUtilities::applyFog(double& value);	

		@remarks 
		
		*/
		static double loadFog();

		/*!
		@description 

		Return the expanded settings for the current fog.
		
		@locality

		Globally effected

		@version  [VBS2Fusion v3.10]

		@param Nothing.

		@return vector - Element 0 is intensity, element 1 is decay and element 2 is altitude of the fog. returns (-1) for three element on error.

		@example

		@code

		vector<double> v=EnvironmentStateUtilities::loadFogEx();

		@endcode

		@overloaded  

		double EnvironmentStateUtilities::loadFog();
		
		@related

		None

		@remarks
		*/
		static std::vector<double> loadFogEx();
		//----------------------------------------------------------

		/*!
		@description
		
		The value specified by state.getOvercast() is used to apply the overcast value in the game with no delay.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		state.setOvercast(state.getOvercast());	
		EnvironmentStateUtilities::applyOvercast(state);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyOvercast( const EnvironmentState& state, int Delay);
		
		EnvironmentStateUtilities::applyOvercast(float& value);	
		
		EnvironmentStateUtilities::applyOvercast(double& value, int Delay);
		
		@related
		
		EnvironmentStateUtilities::loadOvercast()
		
		@remarks To apply overcast to the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyOvercast( const EnvironmentState& state); (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	

		static void applyOvercast( const EnvironmentState& state);

		/*!

		@description 
		
		The value specified by state.getOvercast() is used to apply the overcast value in the game with a delay specified.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@param delay � The delaying time in seconds.

		@return Nothing
		
		@example 
		
		@code
		
		state.setOvercast(state.getOvercast());	
		EnvironmentStateUtilities::applyOvercast(state,1);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities:: applyOvercast( const EnvironmentState& state);
		
		EnvironmentStateUtilities::applyOvercast(float& value);	
		
		EnvironmentStateUtilities::applyOvercast(double& value, int Delay);
		
		@related
		
		EnvironmentStateUtilities::loadOvercast()
		
		@remarks To apply overcast to the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyOvercast( const EnvironmentState& state, int Delay) (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	

		static void applyOvercast( const EnvironmentState& state, int Delay);
		
		/*!
		
		@description The value specified by state.getOvercast() is used to apply the overcast value in the game without specifying a delay.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		float f= 0.53;
		EnvironmentStateUtilities::applyOvercast(f,1);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities:: applyOvercast( const EnvironmentState& state);
		
		EnvironmentStateUtilities:: applyOvercast(double& value);		
		
		EnvironmentStateUtilities::applyOvercast(double& value, int Delay);
		
		@related
		
		EnvironmentStateUtilities::loadOvercast()
		
		@remarks To apply overcast to the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyOvercast(float& value); (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyOvercast(float& value);	

		/*!
		@description 
		
		The value specified by state.getOvercast() is used to apply the overcast value in the game with a delay specfied.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@param delay � The delaying time in seconds.

		@return Nothing
		
		@example 
		
		@code
		
		double d= 0.95236236;
		EnvironmentStateUtilities::applyOvercast(d,1); 
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyOvercast( const EnvironmentState& state)
		
		EnvironmentStateUtilities::applyOvercast(double& value)		
		
		EnvironmentStateUtilities::applyOvercast(float& value)
		
		@related
		
		EnvironmentStateUtilities::loadOvercast()
		
		@remarks To apply overcast to the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyOvercast(double& value, int Delay). (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyOvercast(double& value, int Delay);

		/*!
		
		@description  
		
		The value specified by state.getOvercast() is used to apply the overcast value in the game without specifying a delay.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		double d= 0.5236236;
		EnvironmentStateUtilities::applyOvercast(d);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyOvercast( const EnvironmentState& state);
		
		EnvironmentStateUtilities::applyOvercast(float& value);		
		
		EnvironmentStateUtilities::applyOvercast(double& value, int Delay);
		
		@related
		
		EnvironmentStateUtilities::loadOvercast()
		
		@remarks To apply overcast to the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyOvercast(double& value). (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyOvercast(double& value);	

		/*!
		
		@description  
		
		The value specified by state.getOvercast() is used to apply the overcast value in the game with a delay specified.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@param delay � The delaying time in seconds.

		@return Nothing
		
		@example 
		
		@code
		
		state.setOvercast(state.getOvercast());	
		EnvironmentStateUtilities::applyOvercast(state,1);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities:: applyOvercast( const EnvironmentState& state);
		
		EnvironmentStateUtilities::applyOvercast(float& value);	
		
		EnvironmentStateUtilities::applyOvercast(double& value, int Delay);
		
		@related
		
		EnvironmentStateUtilities::loadOvercast()
		
		@remarks To apply overcast to the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::applyOvercast(float& value, int Delay). (Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyOvercast(float& value, int Delay);
		
		/*!
		
		@description  
		
		This function returns the current overcast setting in the VBS3 environment.
		
		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 

		@param  None

		@return double
		
		@example 
		
		@code
		
		displayString = "overcast"+conversions::DoubleToString(EnvironmentStateUtilities::loadOvercast());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		EnvironmentStateUtilities:: applyOvercast( const EnvironmentState& state);
		
		EnvironmentStateUtilities::applyOvercast(float& value);	
		
		EnvironmentStateUtilities::applyOvercast(double& value, int Delay);
		
		@remarks  To obtain the  overcast of the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: loadOvercast().(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	

		static double loadOvercast();
		

		//----------------------------------------------------------

		/*!
		@description  
		
		The value specified by state.getRain() is used to apply the rain value to the VBS3 environmnent with no delay.
		
		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@return  Nothing
		
		@example 
		
		@code
		
		state.setRain(10);
		EnvironmentStateUtilities::applyRain(state);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state, int Delay)
		
		EnvironmentStateUtilities::applyRain(double& value)
		
		@related
		
		EnvironmentStateUtilities::loadRain();
		
		@remarks To apply rain to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyRain( const EnvironmentState& state). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyRain( const EnvironmentState& state);	

		/*!
		@description  
		
		The value specified by state.getRain() is used to apply the rain value to the VBS3 environmnent with a delay specified.
		
		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@param delay � The delaying time in seconds.

		@return  Nothing
		
		@example 
		
		@code
		
		state.setRain(0.9656);
		EnvironmentStateUtilities::applyRain(state,2);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state)
		
		EnvironmentStateUtilities::applyRain(double& value)
		
		@related
		
		EnvironmentStateUtilities::loadRain();
		
		@remarks To apply rain to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyRain( const EnvironmentState& state, int Delay). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyRain( const EnvironmentState& state, int Delay);	

		/*!
		
		@description  
		
		The value specified by state.getRain() is used to apply the rain value to the VBS3 environmnent with no delay.
		
		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return  Nothing
		
		@example 
		
		@code
		
		double r= 0.23668;
		EnvironmentStateUtilities::applyRain(r);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state)
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state, int Delay);	
		
		@related
		
		EnvironmentStateUtilities::loadRain();
		
		@remarks To apply rain to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyRain(double& value). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyRain(double& value);	

		/*!
		
		@description 
		
		The value specified by state.getRain() is used to apply the rain value to the VBS3 environmnent with a delay specified.
		
		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@param delay � The delaying time in seconds.

		@return Nothing
		
		@example 
		
		@code
		double r= 0.78366998;
		EnvironmentStateUtilities::applyRain(r,3);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state)
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state, int Delay);	
		
		@related
		
		EnvironmentStateUtilities::loadRain();
		
		@remarks To apply rain to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::applyRain(double& value, int Delay). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyRain(double& value, int Delay);	

		/*!
		
		@description 
		
		The value specified by state.getRain() is used to apply the rain value to the VBS3 environmnent without a delay specified.
		
		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10]
		
		@param state � The environment for which the above function is applied.

		@return  Nothing
		
		@example 
		
		@code
		
		float rain = 0.653;
		EnvironmentStateUtilities::applyRain(rain);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state)
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state, int Delay);	
		
		@related
		
		EnvironmentStateUtilities::loadRain();
		
		@remarks To apply rain to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyRain(float& value). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyRain(float& value);	

		/*!
		
		@description  
		
		The value specified by state.getRain() is used to apply the rain value to the VBS3 environmnent with a delay specified.
		
		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@param delay � The delaying time in seconds.

		@return  Nothing
		
		@example 
		
		@code
		
		float f = 0.12;
		EnvironmentStateUtilities::applyRain(f,3);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state)
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state, int Delay);	
		
		@related
		
		EnvironmentStateUtilities::loadRain();
		
		@remarks To apply rain to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyRain(float& value, int Delay). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	

		static void applyRain(float& value, int Delay);	

		/*!
		
		@description 
		
		This function returns the current rain setting of the VBS3 environment.

		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 

		@param  None

		@return double
		
		@example 
		
		@code
		
		displayString ="The rain level"+conversions::DoubleToString(EnvironmentStateUtilities::loadRain());	

		@end code
		
		@overloaded
		
		None
		
		@related
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state)
		
		EnvironmentStateUtilities::applyRain( const EnvironmentState& state, int Delay);	
		
		@remarks To obtain the rain value of the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: loadRain().(Refer (MissionUtilities::loadMission(Mission& mission)for further information).


		*/	
		static double loadRain();
		

		//----------------------------------------------------------

		/*!
		@description  
		
		This function applies the time variables (i.e. TimeYear, TimeMonth,TimeDay, TimeHours and TimeMinutes to the VBS3 environment without a delay.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@return  Nothing
		
		@example 
		
		@code
		
		//sets the time
		state.setTimeHours(2);
		state.setTimeMinutes(25);
		state.setTimeSeconds(54);
		state.setTimeYear(2014);
		EnvironmentStateUtilities::applyTime(state);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyTime( const EnvironmentState& state, int Delay);
		
		@related
		
		None
		
		@remarks To apply the time to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::applyTime( const EnvironmentState& state).(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyTime( const EnvironmentState& state);	

		/*!
		
		@description  
		
		This function applies the time variables (i.e. TimeYear, TimeMonth,TimeDay, TimeHours and TimeMinutes to the VBS3 environment with a delay specified.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@param delay � The delaying time in seconds.

		@return Nothing
		
		@example 
		
		@code
		
		//sets the time
		state.setTimeHours(5);
		state.setTimeMinutes(55);
		state.setTimeSeconds(50);
		state.setTimeYear(2013);
		EnvironmentStateUtilities::applyTime(state,5);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyTime( const EnvironmentState& state);
		
		@related
		
		None
		
		@remarks To apply the time to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyTime( const EnvironmentState& state, int Delay).(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyTime( const EnvironmentState& state, int Delay);
		
		//--------------------------------------------------------------------

		/*! 
		
		@description 
		
		This function returns the year of the VBS3 environment.

		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param None

		@return int
		
		@example 
		
		@code
		
		displayString+="the year is:"+conversions::IntToString(EnvironmentStateUtilities::loadYear());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None	
		
		@remarks To obtain the  year of the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: loadYear().(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static int loadYear();	

		/*! 
		@description 
		
		This function returns the current month of the VBS3 environment.

		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param   None

		@return  int
		
		@example 
		
		@code
		
		displayString+="the month is:"+conversions::IntToString(EnvironmentStateUtilities::loadMonth());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None	
		
		@remarks To obtain the  month of the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::loadMonth().(Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static int loadMonth();	

		/*! 
		
		@description 
		
		This function returns the current day of the VBS3 environment.

		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 

		@param   None

		@return  int
		
		@example 
		
		@code
		
		displayString+="the day is:"+conversions::IntToString(EnvironmentStateUtilities::loadDay());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None	
		
		@remarks To obtain the  day of the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::loadDay(). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static int loadDay();	

		/*! 
		
		@description 
		
		This function returns the current hour of the VBS3 environment.

		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param   None

		@return  int
		
		@example 
		
		@code
		
		displayString+="the hour is:"+conversions::IntToString(EnvironmentStateUtilities::loadHour());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None	
		
		@remarks To obtain the  hour of the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: loadHour();	. Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static int loadHour();	

		/*! 
		
		@description 
		
		This function returns the current minute of the VBS3 environment.
		
		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param   None

		@return  int
		
		@example 
		
		@code
		
		displayString+="the mins is:"+conversions::IntToString(EnvironmentStateUtilities::loadMinutes());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None	
		
		@remarks To obtain the  minute of the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: loadMinutes(. Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static int loadMinutes();

		//--------------------------------------------------------------------

		/*! 
		
		@description 
		
		This function returns the amount of time elapsed (in seconds) in the current mission.

		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param   None

		@return  double
		
		@example 
		
		@code
		
		displayString+="the time elasped is:"+conversions::IntToString(EnvironmentStateUtilities::loadTimeElapsed());
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None	
		
		@remarks To obtain the  minute of the environment state to the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::loadTimeElapsed (. Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static double loadTimeElapsed();

		//--------------------------------------------------------------------

		/*!
		
		@description 
		
		This function returns the rain setting in the current mission of VBS3 environment.

		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 

		@param   None

		@return  vector<float>
		
		@example 
		
		@code
		
		//create a vector to store the values
		vector<float> wind = EnvironmentStateUtilities::loadWind();
		for (vector<float>::iterator itr = wind.begin(),itr_end =wind.end();itr!=itr_end;++itr)
		{ 
		displayString+="The wind values are"+conversions::FloatToString(*itr);
		}
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		EnvironmentStateUtilities::applyWind(std::vector<double> _value);	
		
		@remarks To obtain the  wind setting of the environment state in the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::loadWind(). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::vector<float> loadWind();

		/*!
		
		@description 
		
		This function returns the rain setting in the current mission of VBS3 environment.
		
		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param   None

		@return  vector<double>
		
		@example 
		
		@code
		
		//create a vector to store the values
		vector<double> wind = EnvironmentStateUtilities::loadWindEx();
		for (vector<double>::iterator itr = wind.begin(),itr_end =wind.end();itr!=itr_end;++itr)
		{ 
		displayString+="The wind values are"+conversions::DoubleToString(*itr);
		}
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		EnvironmentStateUtilities::applyWind(std::vector<double> _value);	
		
		@remarks To obtain the  wind setting of the environment state in the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::loadWindEx(). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::vector<double> loadWindEx();

		/*!
		
		@description 
		
		This function uses the value specified by vector to apply the wind value to the VBS3 environment.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param value � The values of the wind.

		@return Nothing
		
		@example 
		
		@code
		
		vector<double> wd;
		wd.push_back(50);
		wd.push_back(200);
		wd.push_back(300);

		state.setWind(wd);
		EnvironmentStateUtilities::applyWind(wd);

		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyWind(std::vector<float> _value)
		
		EnvironmentStateUtilities::applyWind( const EnvironmentState& state);
		
		@related
		
		EnvironmentStateUtilities::loadWind();
		
		@remarks To apply the  wind setting to the environment state in the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyWind(std::vector<double> _value). Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		@remarks Smoke is created using the function CreateEffectWithReference(const ControllableObject& object, EFFECTS_TYPE effectsType) and the change in wind speed can be observered.

		*/	
		static void applyWind(std::vector<double> _value);

		/*!
		
		@description 
		
		This function uses the value specified by vector to apply the wind value to the VBS3 environment.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param value � The values of the wind.

		@return
		
		Nothing
		
		@example 
	
		@code
		
		vector<float> wd;
		wd.push_back(50.23);
		wd.push_back(200.34);
		wd.push_back(300.67);

		state.setWind(wd);
		EnvironmentStateUtilities::applyWind(wd);

		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyWind(std::vector<double> _value);
		
		EnvironmentStateUtilities::applyWind( const EnvironmentState& state);
		
		@related
		
		EnvironmentStateUtilities::loadWind();
		
		@remarks To apply the wind setting to the environment state in the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyWind(std::vector<float> _value);. Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyWind(std::vector<float> _value);

		/*!
		
		@description  
		
		The value specified by vector is used to apply the wind value in the VBS3 environment.
		
		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@param value � The wind value to be applied.

		@return Nothing 
		
		@example 
		
		@code
		
		vector<float> wd;
		wd.push_back(50.23);
		wd.push_back(200.34);
		wd.push_back(300.67);

		state.setWind(wd);
		EnvironmentStateUtilities::applyWind(state,wd);

		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyWind(std::vector<double> _value);
		
		EnvironmentStateUtilities::applyWind(std::vector<float> _value);
		
		@related
		
		EnvironmentStateUtilities::loadWind();
		
		@remarks To apply the  wind setting to the environment state in the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the ob
		
		*/	
		static void applyWind( const EnvironmentState& state);

		/*!
		
		@description  
		
		The value specified by vector is used to apply the wind value in the VBS3 environment.
		
		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 

		@param state � The environment for which the above function is applied.

		@param value � The wind value to be applied.

		@return 
		
		Nothing 
		
		@example 
		
		@code
		
		vector<float> wd;
		wd.push_back(50.23);
		wd.push_back(200.34);
		wd.push_back(300.67);

		state.setWind(wd);
		EnvironmentStateUtilities::applyWind(state,wd);

		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyWind(std::vector<double> _value);
		
		EnvironmentStateUtilities::applyWind(std::vector<float> _value);
		
		@related
		
		EnvironmentStateUtilities::loadWind();
		
		@remarks To apply the  wind setting to the environment state in the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyWind( const EnvironmentState& state, std::vector<float> _value). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/	
		static void applyWind( const EnvironmentState& state, std::vector<float> _value);

		/*!
		
		@description 
		
		This function applies the value specified by state.getSurfaceMoisture() to the current surface moisture value within the VBS3 environment.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example
		
		@code
		
		//sets the surface moisture
		state.setSurfaceMoisture(2.789);
		EnvironmentStateUtilities::applySurfaceMoisture(state);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applySurfaceMoisture(double value);
		
		@related
		
		EnvironmentStateUtilities::updateSurfaceMoisture(EnvironmentState& state);

		@remarks To apply the  SurfaceMoisture to the environment state in the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applySurfaceMoisture( const EnvironmentState& state). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applySurfaceMoisture( const EnvironmentState& state);
		/*!
		
		@description
		
		This function applies the specified value to the current surface moisture value within the VBS3 environment.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param value � The value of surface moisture.

		@return  
		
		Nothing
		
		@example 
		
		@code
		
		double d = 5.639;
		EnvironmentStateUtilities::applySurfaceMoisture(d);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applySurfaceMoisture( const EnvironmentState& state);
		
		@related
		
		EnvironmentStateUtilities::updateSurfaceMoisture(EnvironmentState& state);

		@remarks To apply the SurfaceMoisture to the environment state in the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applySurfaceMoisture(double value). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applySurfaceMoisture(double value);
		/*!
		
		@description 
		
		This function Updates the current surface moisture of the terrain within the VBS3 environment.

		@locality 
		
		Globally Applied
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		EnvironmentStateUtilities::updateSurfaceMoisture(state);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		EnvironmentStateUtilities::applySurfaceMoisture( const EnvironmentState& state);
		
		@remarks To update the  SurfaceMoisture in the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::updateSurfaceMoisture(EnvironmentState& state). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateSurfaceMoisture(EnvironmentState& state);


		/*!
		
		@description 
		
		This function applies the value specified by state.getMaxRainDensity() to the current surface moisture value within the VBS3 environment.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return  
		
		Nothing
		
		@example 
		
		@code

		state.setMaxRainDensity(31.678);
		EnvironmentStateUtilities::applyMaxRainDensity(state); 
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyMaxRainDensity(double value);
		
		@related
		
		EnvironmentStateUtilities:: updateMaxRainDensity(EnvironmentState& state);
		
		@remarks To apply the maximum rain density to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyMaxRainDensity(const EnvironmentState& state).Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyMaxRainDensity( const EnvironmentState& state);
		/*!
		
		@description 
		
		This function applies the maximum rain density to the current surface moisture value within the VBS3 environment.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param value � The value of maximum rain density.

		@return Nothing
	
		@example 
		
		@code
		
		EnvironmentStateUtilities::applyMaxRainDensity(23.789);@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyMaxRainDensity( const EnvironmentState& state);
		
		@related
		
		EnvironmentStateUtilities:: updateMaxRainDensity(EnvironmentState& state);
		
		@remarks To apply the maximum rain density to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyMaxRainDensity(double value).Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyMaxRainDensity(double value);
		/*!
		
		@description 
		
		This function Updates the maximal rain density of the terrain within the VBS3 environment.

		@locality
		
		Globally Applied
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return  
		
		Nothing
		
		@example 
		
		@code
		
		EnvironmentStateUtilities::updateMaxRainDensity(state);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		EnvironmentStateUtilities::applyMaxRainDensity(double value);
		
		@remarks To update the MaxRainDensity in the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::updateMaxRainDensity(EnvironmentState& state);
		Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateMaxRainDensity(EnvironmentState& state);


		/*!
		
		@description 
		
		This function applies the value specified by tate.getMaxEvaporation()as the maximal evaporation value for best weather conditions within the VBS3 environment.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		//sets the maximum evaporation
		state.setMaxEvaporation(12.3);
		EnvironmentStateUtilities::applyMaxEvaporation(state);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities:: applyMaxEvaporation(double value);
		
		@related
		
		EnvironmentStateUtilities:: updateMaxEvaporation(EnvironmentState& state);
		
		@remarks To apply the maximum evaporation to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::applyMaxRainDensity(const EnvironmentState& state).Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyMaxEvaporation( const EnvironmentState& state);
		/*!
		
		@description
		
		This function applies the maximal evaporation value for best weather conditions within the VBS3 environment.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param value � The maximum evaporation value

		@return  
		
		Nothing
		
		@example 
		
		@code
		
		double e = 12.365;
		EnvironmentStateUtilities::applyMaxEvaporation(e);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities::applyMaxEvaporation( const EnvironmentState& state);
		
		@related
		
		EnvironmentStateUtilities::updateMaxEvaporation(EnvironmentState& state);
		
		@remarks To apply the maximum evaporation to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyMaxEvaporation(double value).
		Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyMaxEvaporation(double value);
		/*!
		
		@description 
		
		This function Updates the maximal evaporation value of the terrain within the VBS3 environment for best weather conditions

		@locality 
		
		Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		EnvironmentStateUtilities::updateMaxEvaporation(state);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		EnvironmentStateUtilities:: applyMaxEvaporation( const EnvironmentState& state);
		
		@remarks To update the maximal evaporation value in the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: updateMaxEvaporation(EnvironmentState& state). Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void updateMaxEvaporation(EnvironmentState& state);


		/*!
		
		@description 
		
		This function applies the value specified by state.getSurfaceDrainageSpeed()to the surface drainage speed value within the VBS3 environment.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		//sets the surface drainage speed
		state.setSurfaceDrainageSpeed(45.788);
		
		EnvironmentStateUtilities::applySurfaceDrainageSpeed(state);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities:: applySurfaceDrainageSpeed(double value);
		
		@related
		
		EnvironmentStateUtilities::updateSurfaceDrainageSpeed(EnvironmentState& state);
		
		@remarks To apply the surface drainage to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applySurfaceDrainageSpeed( const EnvironmentState& state).Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applySurfaceDrainageSpeed( const EnvironmentState& state);
		/*!
		
		@description 
		
		This function applies the surface drainage speed within the VBS3 environment.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10]]
		
		@param value � The surface drainage speed value

		@return  
		
		Nothing
		
		@example 
		
		@code
		
		EnvironmentStateUtilities::applySurfaceDrainageSpeed(6.456); 
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities:: applySurfaceDrainageSpeed( const EnvironmentState& state);
		
		@related
		
		EnvironmentStateUtilities:: updateSurfaceDrainageSpeed(EnvironmentState& state);
		
		@remarks To apply the surface drainage speed to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applySurfaceDrainageSpeed(double value).Refer (MissionUtilities::loadMission(Mission& mission)for further information).


		*/
		static void applySurfaceDrainageSpeed(double value);
		/*!

		@description 
		
		This function Updates the surface drainage speed of the terrain within the VBS3 environment.

		@locality 
		
		Globally Applied
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return  Nothing
		
		@example 
		
		@code
		
		EnvironmentStateUtilities::updateSurfaceDrainageSpeed(state);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		EnvironmentStateUtilities:: applySurfaceDrainageSpeed(double value);
		
		@remarks To update the surface drainage speed in the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::updateSurfaceDrainageSpeed(EnvironmentState& state). Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static void updateSurfaceDrainageSpeed(EnvironmentState& state);

		/*!
		
		@description 
		
		This function applies the value specified by state.getWaterLevel() to the sea level value within the VBS3 environment.

		@locality 
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10] 
		
		@param state � The environment for which the above function is applied.

		@return Nothing
		
		@example 
		
		@code
		
		//sets the water level
		state.setWaterLevel(12.56);
		EnvironmentStateUtilities::applyWaterLevel(state); @end code
		
		@overloaded
		
		EnvironmentStateUtilities:: applyWaterLevel(double value); 
		
		@related
		
		None
		
		@remarks To apply the sea level value to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: applyWaterLevel( const EnvironmentState& state).Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyWaterLevel( const EnvironmentState& state);
		/*!
		
		@description 
		
		This function applies the sea level Value to the VBS3 environment.

		@locality
		
		Locally Applied and Globally Effected
		
		@version [VBSFusion v3.10]
		
		@param value � The surface drainage speed value.

		@return Nothing
		
		@example 
		
		@code
		
		EnvironmentStateUtilities::applyWaterLevel(54.2635);
		
		@end code
		
		@overloaded
		
		EnvironmentStateUtilities:: applyWaterLevel( const EnvironmentState& state);
		
		@related
		
		None
		
		@remarks To apply the sea level to the environment state of the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities::applyWaterLevel(double value);
		Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void applyWaterLevel(double value);


		/*!
		
		@description  
		
		This function returns the time the sun will rise on the specified date, at the current world location. The return vector contains the sun rise in the order [year,month,date,hour,minute].

		@locality
		
		Globally Applied
		
		@version [VBSFusion v3.10] 

		@param year � The year of sun rise.
		
		@param month � The month of sun rise.
		
		@param  date � The date of sun rise.
		
		@param hour � The hour of sun rise.
		
		@param minutes � The minute of sun rise.

		@return  vector<int> 
		
		@example 
		
		@code
		
		EnvironmentState ss;
		int sw;
		
		//create a vector to store the values
		vector<int> vec1;
		vec = EnvironmentStateUtilities::getSunRise(2012,6,12,12,45);
		for (vector<int>::iterator itr=vec.begin();itr!=vec.end();itr++)
		{
		sw=*itr;
		displayString+= +conversions::IntToString (+sw);
		}
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks To obtain the sun rise timeof the environment state in the client machine within a network, use This function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: getSunRise(int year, int month, int date, int hours = 0, int minutes = 0).Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::vector<int> getSunRise(int year, int month, int date, int hours = 0, int minutes = 0);

		/*!
		
		description 
		
		This function returns the time the sun will set on the specified date, at the current world location. The return vector contains the sun rise in the order [year,month,date,hour,minute].

		@locality 
		
		Globally Applied
		
		@version [VBSFusion v3.10] 

		@param   year � The year
		
		@param month � The month
		
		@param date � The date
		
		@param hour � The hour
		
		@param mintes � The minutes

		@return  vector<int> 
		
		@example 
		
		@code
		
		EnvironmentState ss;
		int st;
		
		//create a vector to store the values
		vector<int> vec;
		vec1 = EnvironmentStateUtilities::getSunSet(2013,6,12,12,45);
		for (vector<int>::iterator itr=vec.begin();itr!=vec.end();itr++)
		{
		st=*itr;
		displayString+= +conversions::IntToString (+st);
		}
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks To obtain the sun set time of the environment state in the client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: getSunRise(int year, int month, int date, int hours = 0, int minutes = 0).Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static std::vector<int> getSunSet(int year, int month, int date, int hours = 0, int minutes = 0);

		/*!
		Switches random weather on and off.
		Deprecated. Use static void applyRandomWeather(bool mode).
		*/
		VBSFUSION_DPR(UENV001) static void setRandomWeather(bool mode);

		/*!
		
		description  
		
		This function returns the current state of random weather flag.

		@locality 
		
		Globally Applied
		
		@version [VBSFusion v3.10] 

		@param   None

		@return bool
		
		@example 
		
		@code
		
		displayString="the current weather"+ conversions::BoolToString(EnvironmentStateUtilities::getRandomWeather());

		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks To obtain the current state of random whether of the environment state in the client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: getRandomWeather().Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static bool getRandomWeather();

		/*!
		
		@description  
		
		This function Enables/disables environmental effects (ambient life + sound). 

		@locality

		Locally Applied and Globally Effected

		@version [VBSFusion v3.03 ]

		@param enabled � The Boolean value. 

		@return Nothing
		
		@example 
		
		@code
		
		EnvironmentStateUtilities::enableEnvironment(false);
		
		@end code
		
		@overloaded
		
		None
		
		@related
		
		None
		
		@remarks To enable environment in the client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling This function EnvironmentStateUtilities:: enableEnvironment(bool enabled).Refer (MissionUtilities::loadMission(Mission& mission)for further information).

		*/
		static void enableEnvironment(bool enabled);
		
		/*!
		@description  

        /*!
		Switches random weather on and off.  

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v3.12] 

		@param mode - If true, random weather is created.

		@return nothing.

		@example

		@code

		EnvironmentStateUtilities::applyRandomWeather(true);
		
		@endcode

		@overloaded 

		@related  

		@remarks This is a replication of void setRandomWeather(bool mode)
		*/
		static void applyRandomWeather(bool mode);

		/*!
		Description
		Returns the expanded forecast setting for fog.

		@locality

		Globally effected

		@version [VBS2Fusion v3.10] 

		@param Nothing.

		@return vector - Element 0 is intensity, element 1 is decay and element 2 is altitude of the fog. returns (-1) for three element on error.

		@example

		@code
		
		vector<double> v=EnvironmentStateUtilities::getFogForecast();

		@endcode

		@overloaded 

		@related  

		@remarks
		*/
		static std::vector<double> getFogForecast();

	};
};

#endif