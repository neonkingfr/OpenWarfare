
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/


/*************************************************************************

Name:

	ControllableObjectUtilities.h

Purpose:

	This file contains the declaration of the ControllableObjectUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			25-03/2009	RMR: Original Implementation
	1.01		27-08/2009  SDS: Added updateIsLocal(ControllableObject& co);	
								 Updated updateStaticProperties(co);
								 Updated updateStaticProperties_A(co);

	2.0			10-02/2010  YFP: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		27-04/2010  YFP: Added Methods,
									void Suspend(ControllableObject&, bool);
									void Reactivate(ControllableObject&);
									int getSimulationMode(ControllableObject&);
									void updateSimulationMode(ControllableObject&);
									void applySimulationMode(ControllableObject& , SIMULATION_MODE);
	2.03		25-05/2010  YFP: Added Methods,
									void performAction(ControllableObject&,ControllableObject&,string)
									void limitSpeed(ControllableObject&, double)
									void forceSpeed(ControllableObject&, double)

	2.04		09-06/2010  YFP: Added Methods,
									void applyVelocity(ControllableObject&)
									void applyVelocity(ControllableObject&, double)

	2.05        10-06/2010  YFP: Added Methods,
									string getType(ControllableObject&)
	2.05		02-07/2010  YFP: Added Methods,
									void applyURNCallSign(ControllableObject&);
									string getURNCallSign(ControllableObject&);
									void updateURNCallSign(ControllableObject&);
	2.06		06-10/2010  YFP: Added Methods,
									void createObject(ControllableObject&);
									void createObject(ControllableObject&, string, position3D);
	2.07		10-01/2011  CGS: Modified updateDynamicProperties(ControllableObject& co)
									createObject(ControllableObject& co, string typeName, position3D pos)
									getIDUsingAlias(ControllableObject& co)
									updateWeaponAmmo(ControllableObject& co)
									doRespawn(ControllableObject &co)
									getExternalControl(ControllableObject& co)
									setKeyAction(ControllableObject& co, string keyStroke, float amount)
									setUnitAction(ControllableObject& co,string keyStroke, float amount)

	2.08		11-08-2011	SSD: Added Methods,
									applyPositionASL(ControllableObject& co)
									applyPositionASL(ControllableObject& co, position3D aslPos)
	2.09		20-09/2011 CGS:	Added Methods
									bool isExternally(ControllableObject& co)
									bool canFloat(ControllableObject& co)
									bool isAttached(ControllableObject& co)
									double getArmsTotalWeight(ControllableObject& co)
									bool isTouchingObject(ControllableObject& co)
									vector3D getAngularVelocity(ControllableObject& co)
									vector3D getAcceleration(ControllableObject& co)
									void disableActions(ControllableObject& co, bool action=true)
									bool isActionsDisabled(ControllableObject& co)
									void applyDrawMode(ControllableObject& co, DRAWMODE mode)
									string getDrawMode(ControllableObject& co)
				21-09/2011			bool applyDrawMode(ControllableObject& co, DRAWMODE mode, Color_RGBA color, string lod)
									bool applyDrawMode(ControllableObject& co, DRAWMODE mode, Color_RGB color, double transMin, double transMax, string lod)
									bool applyDisplayText (ControllableObject& co, string message)
									bool applyDisplayText (ControllableObject& co, string message, vector3D offset, bool mode, Color_RGBA color, double size)
									bool applyScale(ControllableObject& co, double width, double height, double length)
									bool applyForce(ControllableObject& co, vector3D force, position3D position)
				22-09/2011			bool isEnabledAI(ControllableObject& co, AIBEHAVIOUR type)
									string getAnimationBone(ControllableObject& co, string animation)
									string getVarName(ControllableObject& co)
									double getDistance(ControllableObject& co, ControllableObject& tco)
				28-09/2011			void applyPositionASLEx(ControllableObject& co, position3D aslPosEx)
									bool canMove(ControllableObject& co)
									bool canFire(ControllableObject& co)
									vector3D getBoundingBoxMinExtremePoints(ControllableObject& co, ControllableObject& tco)
									vector3D getBoundingBoxMaxExtremePoints(ControllableObject& co, ControllableObject& tco)
									bool isHidden(ControllableObject& co)
									double getLength(ControllableObject& co)
									double getWeight(ControllableObject& co)
									double getVolume(ControllableObject& co)

/************************************************************************/

#ifndef VBS2FUSION_CONTROLLABLE_OBJECT_UTILITIES_H
#define VBS2FUSION_CONTROLLABLE_OBJECT_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <math.h>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBSFusion.h"
#include "conversions.h"
#include "data/ControllableObject.h"
#include "util/ExecutionUtilities.h"
#include "data/Turret.h"


/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBSFusion
{	

	class VBSFUSION_API ControllableObjectUtilities
	{
	public:

		/*!

		@description

		Used to create an object within the VBS2 Environment. The type and position of the object to be created has to be defined before calling the function. Any VBS2 object can be created using this command. E.g. Units, Vehicles, Cameras etc.

		@locality

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object to be created. The object should be set with relevant parameters before using with the createObject function.

		@return Nothing

		@example

		@code

		//The object to be created
		ControllableObject co;

		//Set parameters before calling the function
		co.setType(string("Anchorman"));
		co.setPosition(position3D(3412.23, 2145.34, 25.45);

		//Create the object
		ControllableObjectUtilities::createObject(co);

		//Update common properties of objects from VBS2
		ControllableObjectUtilities::updateStaticProperties(co);
		ControllableObjectUtilities::updateDynamicProperties(co);

		@endcode

		@overloaded 

		ControllableObjectUtilities::createObject(ControllableObject& co string typeName, position3D pos)

		@related

		ControllableObjectUtilities::deleteObject(ControllableObject& co)

		@remarks The position and the type of object should be specified in the controllableObject data class before calling the createObject function.

		@remarks Objects loaded with invalid or no positional information are created at map location [0,0,0]

		@remarks Should be used for generic object creation, whereas specialized objects like Units and Vehicles can be created using the specialized createUnit and createVehicle commands.

		*/
		static void createObject(ControllableObject& co);

		/*!
		@description 

		Used to create an object within the VBS2 Environment. The type and position of the object to be created has to be passed as parameters while calling the function. Any VBS2 object can be created using this command. E.g. Units, Vehicles, Cameras etc.

		@locality 

		Global

		@version [VBS2Fusion v3.02] 

		@param co - The object to be created.

		@param typeName � The type of the object.

		@param pos � The position where the object should be created

		@return Nothing

		@example

		@code

		//The object to be created
		ControllableObject co;

		//Create the object
		ControllableObjectUtilities::createObject(co,�Anchorman�, position3D(3412.23, 2145.34, 25.45);

		//Update common properties of objects from VBS2
		ControllableObjectUtilities::updateStaticProperties(co);
		ControllableObjectUtilities::updateDynamicProperties(co);

		@endcode

		@overloaded

		ControllableObjectUtilities::createObject(ControllableObject& co) 

		@related

		ControllableObjectUtilities::deleteObject(ControllableObject& co)

		@remarks Objects loaded with invalid or no positional information are created at map location [0,0,0]

		@remarks Should be used for generic object creation, whereas specialized objects like Units and Vehicles can be created using the specialized createUnit and createVehicle commands.

		*/
		static void createObject(ControllableObject& co, const std::string& typeName, const position3D& pos);


		//-------------------------------------------------------
		// Side utilities
		//-------------------------------------------------------

		/*!
		@description

		Gets the side of an object within the VBS2 Environment. Any VBS2 objects side can be obtained using this command. Some side examples are as follows: WEST � for Westside, EAST- for East side, CIV- for civilian, GUER � for Resistance, etc.

		@locality 

		Global

		@version [VBS2Fusion v3.02] 

		@param co - The object for which the side is obtained. The object should be created before passing it as a parameter.

		@return String

		@example

		@code

		//The object to be created
		ControllableObject co;

		string side = ControllableObjectUtilities::getSide(co);

		@endcode

		@overloaded 

		None

		@related

		ControllableObjectUtilities::updateSide(ControllableObject& co)

		@remarks Object should be created before using this function.(Refer create object).

		@remarks The side of the object is predefined and cannot be changed. The side depends on the type applied to the object during its creation. (Refer ControllableObjectUtilities::createObject(ControllableObject& co)to set the type)

		@remarks To obtain the side property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getSide(ControllableObject& co). 
		*/
		static std::string getSide(const ControllableObject& co);

		/*!
		@description

		It updates the side property of an object created within the VBS2 Environment in the fusion end.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the side property is updated in the fusion end. The object should be created before passing it as a parameter.
		
		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;
		co.setName(string("U1"));

		// The side property of the
		//co object created in real time (Alias/Name as U1)is stored in Fusion

		ControllableObjectUtilities::updateSide(co);

		string side =co.getside();

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::getSide(ControllableObject& co)

		@remarks The side property can also be updated by calling the function updateStaticProperties(ControllableObject& co)

		@remarks Update does not imply changing of sides (e.g. west to east), the side of an object is predefined and cannot be changed. The side depends on the type applied to the object during its creation. (Refer ControllableObjectUtilities::createObject(ControllableObject& co) or ControllableObjectUtilities::createObject(ControllableObject& co string typeName, position3D pos) to set the type)

		@remarks To update the side property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::updateSide(ControllableObject& co).

		*/
		static void updateSide(ControllableObject& co);

		//-----------------------------------------------------

		/*!
		@description

		It checks if an object is locally owned or in a network etc. and returns true if the object is created in the running machine.

		@locality 

		Global

		@version [VBS2Fusion v3.02] 

		@param co - The object which is checked. The object should be created before passing it as a parameter.

		@return boolean

		@example

		@code

		// The object to be created
		ControllableObject co;

		if(ControllableObjectUtilities::isLocallyOwned(co)==true)
		{ disStr += "isLocallyOwned : true"; }

		else
		{ disStr += "isLocallyOwned : false"; }

		DisplayFunctions::DisplayString(disStr);

		@endcode

		@overloaded

		None

		@related ControllableObjectUtilities::updateLocallyOwned(ControllableObject& co)

		@remarks To check the ownership property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isLocallyOwned(ControllableObject& co). 
		*/
		static bool isLocallyOwned(const ControllableObject& co);

		/*!
		@description

		Used to update the locally owned property of an object created within the VBS2 Environment in the VBS2Fusion end.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the locally owned property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;

		// The object updated on the fusion end
		ControllableObjectUtilities::updateLocallyOwned(co);

		if(co.isLocallyOwned()==true)
		{ displayString += "updated"; }

		else
		{ displayString += "not updated"; } 

		@endcode

		@overloaded

		@related 

		ControllableObjectUtilities::isLocallyOwned(ControllableObject& co)

		@remarks To update the ownership property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateLocallyOwned(ControllableObject& co)
		*/
		static void updateLocallyOwned(ControllableObject& co);
			
		//------------------------------------------------------

		/*!
		@description

		Gets the network id of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the NetworkID is obtained. The object should be created before passing it as a parameter.

		@return NetworkID

		@example

		@code

		// The object to be created
		ControllableObject co;

		//To display the network id as a string use getNetworkIDString() as shown below
		displayString=(ControllableObjectUtilities::getNetworkID(co)).getNetworkIDString();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the network id of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getNetworkID(ControllableObject& co)	
		*/
		static NetworkID getNetworkID(const ControllableObject& co);

		//-------------------------------------------------------
		// Position utilities
		//-------------------------------------------------------

		/*!
		@description

		Used to get the position of an object in 3D space within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the position is obtained. The object should be created before passing it as a parameter.

		@return position3D

		@example

		@code

		// The object to be created
		ControllableObject co;

		//To display the position as a string use getVBSPosition() as shown below
		displayString = ControllableObjectUtilities::getPos(co).getVBSPosition(); 

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function is similar to the function ControllableObjectUtilities::getPosition(ControllableObject& co)

		@remarks Positions of objects created with no positional information or invalid positions can also be obtained using this function, although they are created at map location [0, 0, 0]

		@remarks To obtain the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getPos(ControllableObject& co).
		*/
		static position3D getPos(ControllableObject& co);

		/*!
		@description

		Gets the position of an object in 3D space within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the position is obtained. The object should be created before passing it as a parameter.

		@return position3D

		@example

		@code

		// The object to be created
		ControllableObject co;

		//To display the position as a string use getVBSPosition() as shown below
		displayString = ControllableObjectUtilities::getPosition(co).getVBSPosition(); 

		@endcode

		@overloaded

		None

		@related

		ontrollableObjectUtilities::updatePosition(ControllableObject& co);
		ControllableObjectUtilities::applyPosition(ControllableObject& co);

		@remarks This function is similar to the function ControllableObjectUtilities::getPos(ControllableObject& co)
		@remarks Positions of objects created with no positional information or invalid positions can also be obtained using this function although they are created at map location [0, 0, 0].

		@remarks To obtain the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getPosition(ControllableObject& co).
 
		*/
		static position3D getPosition(const ControllableObject& co);

		/*!
		@description

		Used to update the position of an object in 3D space within the VBS2 Environment in the VBS2Fusion end.
		
		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the position property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;

		ControllableObjectUtilities::updatePosition(co);

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities:: getPosition(ControllableObject& co)
		ControllableObjectUtilities::applyPosition(ControllableObject& co)

		@remarks To update the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updatePosition(ControllableObject& co).
		
		*/
		static void updatePosition(ControllableObject& co);

		/*!
		@description

		Used to apply the position defined by the function co.setPosition(position3D& position) to an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the position is applied. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;

		//position must be set before applying
		co.setPosition(position3D(40,60,0));
		ControllableObjectUtilities::applyPosition(co);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyPosition(ControllableObject& co, position3D pos)

		@related 

		ControllableObjectUtilities:: getPosition(ControllableObject& co)
		ControllableObjectUtilities:: updatePosition(ControllableObject& co)

		@remarks If the positional information is invalid or not set, the object would be moved to map location [0,0,0].

		@remarks To apply the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyPosition(ControllableObject& co).		
		*/
		static void applyPosition(ControllableObject& co);

		/*!
		@description

		Used to apply a position to an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the position is applied. The object should be created before passing it as a parameter.

		@param pos - The position where the object would be moved.	

		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co; 

		ControllableObjectUtilities::applyPosition(co, player.getPosition()+position3D(40,60,0));

		@endcode

		@remarks To apply the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyPosition(ControllableObject& co, position3D pos).

		@remarks If the positional information is not provided or is invalid, the object would be moved to map location [0,0,0]
		*/
		static void applyPosition(ControllableObject& co, const position3D& pos);

		/*!
		Order the object to move to the position specified by pos. The speed of 
		movement can be changed using forceSpeed. 

		Deprecated Function.
		Use void applyCommandMove(ControllableObject& co, position3D pos) instead.
		*/
		VBSFUSION_DPR(UCOB012) static void moveTo(ControllableObject& co, position3D pos);

		/*!
		Returns a world position (above ground level), relative to an object's origin, 
		and taking into account its direction.

		Deprecated Function.
		Use position3D getModelToWorld(ControllableObject& co, position3D& pos) instead.
		*/
		VBSFUSION_DPR(UCOB013) static position3D ControllableObjectUtilities::modelToWorld(ControllableObject& co, position3D& pos);

		/*!
		Returns an object's offset from the specified world position, taking into account 
		the object's origin and direction.

		Deprecated Function.
		Use position3D getWorldToModel(ControllableObject& co, position3D& pos) instead.
		*/
		VBSFUSION_DPR(UCOB014) static position3D ControllableObjectUtilities::worldToModel(ControllableObject& co, position3D& pos);

		/*!
		Returns a world position (above sea level), relative to an object's origin, 
		and taking into account its direction.

		Deprecated function.
		Use position3D getModelToWorldASL(ControllableObject& co, position3D& pos) instead.
		*/
		VBSFUSION_DPR(UCOB015) static position3D ControllableObjectUtilities::modelToWorldASL(ControllableObject& co, position3D& pos);

		/*!
		Returns an object's offset from the specified world position, taking into account 
		the object's origin and direction.

		Deprecated function.
		Use position3D getWorldToModelASL(ControllableObject& co, position3D& pos) instead.
		*/
		VBSFUSION_DPR(UCOB016) static position3D ControllableObjectUtilities::worldToModelASL(ControllableObject& co, position3D& pos);

		//-------------------------------------------------------
		// PositionASL utilities
		//-------------------------------------------------------

		/*!
		@description

		Used to get the position of an object above sea level in 3D format within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the position is obtained. The object should be created before passing it as a parameter.

		@return position3D

		@example

		@code

		/To display the position as a string use getVBSPosition() as shown below
		displayString = ControllableObjectUtilities::getPositionASL(co).getVBSPosition(); 

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::updatePositionASL(ControllableObject& co)
		ControllableObjectUtilities::applyPositionASL(ControllableObject& co)

		@remarks Positions of objects created with no positional information or invalid positions can also be obtained using this function.

		@remarks To obtain the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getPositionASL(ControllableObject& co).
		*/
		static position3D getPositionASL(const ControllableObject& co);

		/*!
		@description
		
		Used to update the position of an object above sea level in 3D format within the VBS2 Environment in the VBS2Fusion end.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the PositionASL property is updated in the fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;

		ControllableObjectUtilities::updatePositionASL(co);
		displayString += co.getPositionASL().getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::applyPositionASL(ControllableObject& co)
		ControllableObjectUtilities::getPositionASL(ControllableObject& co)

		@remarks To update the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updatePositionASL(ControllableObject& co).
		
		*/
		static void updatePositionASL(ControllableObject& co);


		//-------------------------------------------------------
		// Speed & Velocity utilities
		//-------------------------------------------------------

		/*!
		@description

		Used to get the speed of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the speed is obtained. The object should be created before passing it as a parameter.
		
		@returndouble

		@example

		@code

		//To display the speed of an object at each instant use get speed in the �OnSimulationStep� loop e.g. speed veh;
		string speed = conversions::DoubleToString(ControllableObjectUtilities::getSpeed(veh));
		displayString ="SPEED IS: "+speed;

		@endcode

		@overloaded

		@related

		ControllableObjectUtilities::updateSpeed(ControllableObject& co)

		@remarks To obtain the speed of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getSpeed(ControllableObject& co).

		@remarks To display the speed of the object at each instant use get speed in the �OnSimulationStep�
		*/
		static double getSpeed(const ControllableObject& co);

		/*!
		@description

		Used to update the speed of an object within the VBS2 Environment in the VBS2Fusion end.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the speed property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@returnNothing

		@example

		@code

		//Before passing an object as a parameter, always update its properties

		ControllableObjectUtilities::updateStaticProperties(veh);
		ControllableObjectUtilities::updateDynamicProperties(veh);

		ControllableObjectUtilities::updateSpeed(veh);

		//display the speed value stored in the fusion end
		string displayspeed = conversions::DoubleToString(veh.getSpeed());
		displayString += "SPEED IN DATA CLASS" +displayspeed;

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::getSpeed(ControllableObject& co)

		@remarks To update the speed of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateSpeed(ControllableObject& co).
		*/
		static void updateSpeed(ControllableObject& co);
		
		//-------------------------------------------------------

		/*!
		@description

		Used to apply the velocity defined by the function co.setVelocity(vector3D velocity) to an object within the VBS2 Environment. (I.e. the position the object will be within one second)

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the velocity is applied. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//set the velocity value before applying

		veh.setVelocity(-100,0,-100);
		ControllableObjectUtilities::applyVelocity(veh);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyVelocity(ControllableObject& co, vector3D velocity);

		@related

		ControllableObjectUtilities::getVelocity(ControllableObject& co)

		@remarks Velocity does not imply speed in this function, the object is moved to the position passed as a parameter and it does not abide by the laws of physics. 

		@remarks To apply the velocity to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyVelocity(ControllableObject& co).
		*/
		static void applyVelocity(ControllableObject& co);

		/*!
		@description

		Used to apply a velocity to an object within the VBS2 Environment.(I.e. the position the object will be within one second)

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the velocity is applied. The object should be created before passing it as a parameter.

		@param velocity - The velocity which is applied to the object

		@return Nothing

		@example

		@code

		//x=100,z=200,y=0
		//x- moves the object along the x axis
		//z- moves the object along the z axis
		//y- moves the object along the y axis(axis perpendicular to the x axis and z axis)
		ControllableObjectUtilities::applyVelocity(veh, vector3D(100,200,0));

		@endcode

		@overloaded

		ControllableObjectUtilities::applyVelocity(ControllableObject& co)

		@related

		ControllableObjectUtilities::getVelocity(ControllableObject& co)

		@remarks Velocity does not imply speed in this function, the object is moved to the position passed as a parameter and it does not abide by the laws of physics.

		@remarks To apply the velocity to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyVelocity(ControllableObject& co, vector3D velocity).
		*/
		static void applyVelocity(ControllableObject& co, const vector3D& velocity);

		/*!
		@description

		Used to get the velocity of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the velocity is obtained. The object should be created before passing it as a parameter.

		@return  vector3D

		@example

		@code

		//To convert the vector into string use getVBSPosition
		string displayvelocity= ControllableObjectUtilities::getVelocity(co).getVBSPosition();

		displayString += "VELOCITY IS"+displayvelocity;

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::applyVelocity(ControllableObject& co)

		@remarks To get the velocity of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getVelocity(ControllableObject& co).
		*/

		static vector3D getVelocity(const ControllableObject& co);

		/*!
		@description

		Used to update the velocity of an object within the VBS2 Environment in the VBS2Fusion end.
		 
		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the velocity property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		example

		@code

		//Before passing an object as a parameter, always update its properties

		ControllableObjectUtilities::updateStaticProperties(veh);
		ControllableObjectUtilities::updateDynamicProperties(veh);

		ControllableObjectUtilities::updateVelocity(veh);

		//display the velocity value stored in the fusion end
		string displayvelocity = conversions::DoubleToString(veh.getVelocity());
		displayString += "Velocity IN DATA CLASS" +displayvelocity;

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::getVelocity(ControllableObject& co)
		ControllableObjectUtilities::applyVelocity(ControllableObject& co)

		@remarks To update the velocity of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateVelocity(ControllableObject& co).
		 
		*/
		static void updateVelocity(ControllableObject& co);
		

		//-------------------------------------------------------
		// Direction utilities
		//-------------------------------------------------------

		/*!
		@description

		Used to get the direction in which an object is facing towards within the VBS2 Environment. (Number between 0 � 360 is returned).

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the direction is obtained. The object should be created before passing it as a parameter.

		@return double

		example

		@code

		//To convert the double into string use conversions::DoubleToString
		string displayDirection =conversions::DoubleToString(ControllableObjectUtilities::getDirection(veh));
		displayString += "The direction is"+displayDirection;

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::updateDirection(ControllableObject& co);
		ControllableObjectUtilities::applyDirection(ControllableObject& co);

		@remarks To get the direction of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getDirection(ControllableObject& co).  

		*/
		static double getDirection(const ControllableObject& co);

		/*!
		@description

		Used to update the direction of an object within the VBS2 Environment in the VBS2Fusion end.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the direction property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter

		@return Nothing

		example

		@code

		//Before passing an object as a parameter, always update its properties

		ControllableObjectUtilities::updateStaticProperties(veh);
		ControllableObjectUtilities::updateDynamicProperties(veh);

		ControllableObjectUtilities:: updateDirection(player);

		//display the direction stored in the fusion end as a string
		string displayDirection = conversions::DoubleToString(player.getDirection());

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::getDirection(ControllableObject& co)
		ControllableObjectUtilities::applyDirection(ControllableObject& co);

		@remarks To update the direction of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateDirection(ControllableObject& co).

		*/
		static void updateDirection(ControllableObject& co);

		/*!
		@description

		Used to apply the direction defined by the function co.setDirection(double direction) to an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the direction is applied. The object should be created before passing it as a parameter.

		@returnNothing

		example

		@code

		//set the direction value before applying
		veh.setDirection(289.5);
		ControllableObjectUtilities::applyDirection(veh);

		@endcode

		@overloaded

		ControllableObjectUtilities:: applyDirection(ControllableObject& co, double dir)

		@related

		ControllableObjectUtilities::getDirection(ControllableObject& co)
		ControllableObjectUtilities::updateDirection(ControllableObject& co)

		@remarks To apply the direction to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyDirection(ControllableObject& co).
		*/
		static void applyDirection(ControllableObject& co);

		/*!
		@description

		Used to apply a direction to an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the direction is applied. The object should be created before passing it as a parameter.
		
		@param dir - The direction in which the object should turn after applying the function. (The value should be between 0 and 360 )

		@return Nothing

		example

		@code

		ControllableObjectUtilities::applyDirection(veh,289.5);

		@endcode

		@overloaded

		ControllableObjectUtilities:: applyDirection(ControllableObject& co)

		@related

		ControllableObjectUtilities::getDirection(ControllableObject& co)
		ControllableObjectUtilities::updateDirection(ControllableObject& co)

		@remarks- To apply the direction to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::applyDirection(ControllableObject& co, double dir).	
		*/
		static void applyDirection(ControllableObject& co, double dir);

		//-------------------------------------------------------
		// Type utilities
		//-------------------------------------------------------

		/*!
		@description

		Used to get the type of an object within the VBS2 Environment.
		
		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the type is found. The object should be created before passing it as a parameter.

		@return String

		@example

		@code

		string DisplayType= ControllableObjectUtilities::getType(veh);
		displayString += "Vehicle is "+DisplayType;

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities:: updateType(ControllableObject& co)

		@remarks To get the type of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getType(ControllableObject& co).
		*/
		static std::string getType(const ControllableObject& co);

		/*!
		@description

		Used to update the type of an object within the VBS2 Environment in the VBS2Fusion end.
		
		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the type property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Before passing an object as a parameter, always update its properties

		ControllableObjectUtilities::updateStaticProperties(veh);
		ControllableObjectUtilities::updateDynamicProperties(veh);

		ControllableObjectUtilities::updateType(veh);

		string Type = veh.getType();
		displayString +="Vehicle type in Data class is"+Type;

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::getType(ControllableObject& co)

		@remarks Update does not imply changing of types (e.g. US army to Taliban), the type set to the object during its creation is fixed and remains throughout the mission. (Refer ControllableObjectUtilities::createObject(ControllableObject& co) or ControllableObjectUtilities::createObject(ControllableObject& co string typeName, position3D pos) to set the type)
		@remarks To update the type of an object created in a client machine within a network use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateType(ControllableObject& co).
		*/
		static void updateType(ControllableObject& co);

		//-------------------------------------------------------
		// Player status utilities
		//-------------------------------------------------------

		/*!
		@description

		Used to check if an object is controlled by the player and returns true or otherwise.
		
		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object which is checked. The object should be created before passing it as a parameter.

		@return boolean

		@example

		@code

		// The object to be created
		ControllableObject co;

		if(ControllableObjectUtilities::isPlayer(co)==true)
		{ displayString += " isPlayer : true";// True block called }

		else
		{ displayString += "isPlayer : false";// False block called } 

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::updateIsPlayer(ControllableObject& co) 

		@remarks Only the object that is controlled by the player during the function call is considered as the player object i.e. once the player switches to another unit, the original player unit during the mission creation would return false
		@remarks To check the isPlayer property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isPlayer(ControllableObject& co).
		*/
		static bool isPlayer(const ControllableObject& co);

		/*!
		@description

		Used to update the IsPlayer property of an object created within the VBS2 Environment in the VBS2Fusion end.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the isPlayer property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;

		// The object updated in the fusion end
		ControllableObjectUtilities::updateIsPlayer(co);

		//Use the getPlayableStatus()to check if the property has been updated in the fusion end

		if((co.getPlayableStatus())==true)
		{ displayString += "is played by player"; }

		else
		{ displayString += "not played by player"; } 

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::isPlayer(ControllableObject& co)

		@remarks To update the IsPlayer property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateIsPlayer(ControllableObject& co)  
		*/
		static void updateIsPlayer(ControllableObject& co);

		//-------------------------------------------------------
		// Alive status utilities
		//-------------------------------------------------------

		/*!
		@description

		Used to check if an object is alive and return true or otherwise.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object which is checked. The object should be created before passing it as a parameter.

		@return boolean

		@example

		@code

		// The object to be created
		ControllableObject co;

		if(ControllableObjectUtilities::isAlive(co)==true)
		{ displayString += " isAlive : true"; }

		else
		{ displayString += "isAlive : false"; } 

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::updateIsAlive(ControllableObject& co) 

		@remarks To check whether an object created in a client machine within a network is alive, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isAlive(ControllableObject& co). 
		*/
		static bool isAlive(const ControllableObject& co);

		/*!
		@description

		Used to update the IsAlive property of an object created within the VBS2 Environment in the VBS2Fusion end.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the isAlive property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.
		
		@return  Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;

		// The object updated on the fusion end
		ControllableObjectUtilities:: updateIsAlive(co);

		if(co.getALive()==true)
		{ displayString += "IsAlive updated"; }

		else
		{ displayString += "IsAlive not updated"; } 

		@endcode

		@overloaded

		None 

		@related

		ControllableObjectUtilities::isAlive(ControllableObject& co)

		@remarks To update the isAlive property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateIsAlive(ControllableObject& co) 
		*/
		static void updateIsAlive(ControllableObject& co);

		/*!
		@description

		Used to apply damage or repair the object defined by the function co.setAlive(bool boolAlive) created within the VBS2 Environment.
		
		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the damage or repair is done. The object should be created before passing it as a parameter.
		
		@return Nothing

		@example

		@code

		co.setAlive(false);
		ControllableObjectUtilities::applyIsAlive(co);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyIsAlive(ControllableObject& co, bool alive)

		@related

		ControllableObjectUtilities::isAlive(ControllableObject& co)
		ControllableObjectUtilities::updateIsAlive(ControllableObject& co)

		@remarks To apply the IsAlive property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::applyIsAlive(ControllableObject& co) 
		*/
		static void applyIsAlive(ControllableObject& co);

		/*!
		@description

		Used to damage or repair an object created within the VBS2 Environment. 

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the damage or repair is done. The object should be created before passing it as a parameter. 

		@param alive - If alive = false the object would be damaged/destroyed. If the value is true the object will be repaired.  

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyIsAlive(co, false);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyIsAlive(ControllableObject& co)

		None

		@related

		ControllableObjectUtilities:: updateDamage(ControllableObject& co)
		ControllableObjectUtilities:: applyDamage(ControllableObject& co)

		@remarks To apply the IsAlive property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyIsAlive(ControllableObject& co, bool alive) 
		*/
		static void applyIsAlive(ControllableObject& co, bool alive);


		//-------------------------------------------------------
		// Damage utilities
		//-------------------------------------------------------

		/*!
		@description

		Used to get the damage level of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the damage level is found.
		
		@return Double

		@example

		@code

		if(ControllableObjectUtilities::isAlive(co)==true)
		{ displayString += "isALIVE : true"; displayString+="Damage is" + // To display the double as a string use the function DoubleToString() as shown below 

		conversions::DoubleToString(ControllableObjectUtilities::getDamage(co)); } 

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities:: updateDamage(ControllableObject& co)
		ControllableObjectUtilities:: applyDamage(ControllableObject& co)

		@remarks To get the damage level of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getDamage(ControllableObject& co). 
		*/
		static double getDamage(const ControllableObject& co);

		/*!
		@description

		Used to update the Damage property of an object created within the VBS2 Environment in the VBS2Fusion end.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the Damage property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.
		
		@return Nothing

		@example

		@code

		// The object updated on the fusion end
		ControllableObjectUtilities::updateDamage(co);
		//To display the double as a string use the function DoubleToString() as shown below

		displayString = "after update damage is" conversions::DoubleToString(co.getDamage());

		@endcode

		@overloaded

		@related

		ControllableObjectUtilities::getDamage(ControllableObject& co)
		ControllableObjectUtilities:applyDamage(ControllableObject& co)

		@remarks To update the damage property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::updateDamage(ControllableObject& co)   
		*/
		static void updateDamage(ControllableObject& co);

		/*!
		@description

		Used to apply the damage level defined by the function co.setDamage(double damage) to the object created within the VBS2 Environment.
		
		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the damage is applied. The object should be created before passing it as a parameter.
		
		@return Nothing

		@example

		@code

		//Set the damage before applying the the function

		co.setDamage(0.4562);
		ControllableObjectUtilities::applyDamage(co);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyDamage(ControllableObject& co, double damage)

		@related

		ControllableObjectUtilities::updateDamage(ControllableObject& co)

		@remarks To apply the damage level to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyDamage(ControllableObject& co) 
		*/
		static void applyDamage(ControllableObject& co);

		/*!
		@description

		Used to apply damage to the object created within the VBS2 Environment .

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the damage is applied. The object should be created before passing it as a parameter.
		@param damage � The damage level to be applied to the object

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyDamage(co,0.4562);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyDamage(ControllableObject& co)

		@related

		ControllableObjectUtilities::updateDamage(ControllableObject& co)

		@remarks To apply the damage level to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyDamage(ControllableObject& co, double damage)
		*/
		static void applyDamage(ControllableObject& co, double damage);

		//-------------------------------------------------------
		// Object Update utilities
		//-------------------------------------------------------
		
		/*!
		@description

		Used to update the properties of an object created within the VBS2 Environment in the VBS2Fusion end. E.g. network ID, Type, etc. (properties that do not change frequently)

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the properties are updated in the VBS2Fusion end. The object should be created before passing it as a parameter
		
		@return Nothing

		@example

		@code

		ControllableObjectUtilities::updateStaticProperties(co); 

		@endcode

		@overloaded

		None 

		@related

		ControllableObjectUtilities::updateDynamicProperties(ControllableObject& co)

		@remarks To update the static properties of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateStaticProperties(ControllableObject& co) 
		*/
		static void updateStaticProperties(ControllableObject& co);

		/*!
		@description

		Used to update the properties of an object created within the VBS2 Environment in the VBS2Fusion end e.g. damage, direction, alive, isPlayer, position, positionASL (properties that change frequently)
		
		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the properties are updated in the VBS2Fusion end. The object should be created before passing it as a parameter 

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::updateDynamicProperties(co); 

		@endcode

		@overloaded

		None 

		@related

		ControllableObjectUtilities::updateStaticProperties(ControllableObject& co)

		@remarks To update the dynamic properties of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateDynamicProperties(ControllableObject& co)  
		*/
		static void updateDynamicProperties(ControllableObject& co);


		//-------------------------------------------------------
		// AttachTo controls
		//-------------------------------------------------------

		/*!
		Attaches an object to another object. The offset is applied to the object, memory point.
		Memory point is optional, it specifies location where the attaching should happen (it's name of the memory point in the model)

		Deprecated Function.
		Use void applyObjectAttach(ControllableObject& co, ControllableObject& tObject, position3D offSet, string memoryPoint) instead.
		*/
		VBSFUSION_DPR(UCOB017) static void attachTo(ControllableObject& object, ControllableObject& tObject, position3D offSet, std::string memoryPoint);

		/*!
		Attaches an object to another object. The offset is applied to the object center. 

		Deprecated Function.
		Use void applyObjectAttach(ControllableObject& co, ControllableObject& tObject, position3D offSet) instead.
		*/
		VBSFUSION_DPR(UCOB019) static void attachTo(ControllableObject& object, ControllableObject& tObject, position3D offSet);

		/*!
		detach the given object if it is attached to some object. 

		Deprecated Function.
		Use void applyObjectDetach(ControllableObject &co) instead.
		*/
		VBSFUSION_DPR(UCOB020) static void detach(ControllableObject &object);

		/*!
		Attaches an object to another object. The offset is applied to the object, memory point.
		Memory point specifies location where the attaching should happen (it's name of the memory point in the model)
		rope: 
		0 or false: The attachment will be rigid, meaning obj1 will be fixed in position relative to obj2.
		1 or true: The attachment will be done via a flexible rope, allowing obj1 to swing about.
		2: The object will be attached via an inflexible link (like a tow bar, with pivots on each end).
		3: The object will be attached via a horizontal rope (to simulate towing). If the model has a memory point "attachRope" then the rope will be attached to this point. 

		bone- Name of bone to be attached to on target object. The referenced bone must be of LOD "Memory", "Geometry" or "Fire".
		collisions- Whether collisions for attached object should be enabled 

		Deprecated Function.
		Use void applyObjectAttach(ControllableObject& co, ControllableObject& tObject, position3D offSet, string memoryPoint, int rope, string bone, bool collisions = false) instead.
		*/
		VBSFUSION_DPR(UCOB018) static void attachTo(ControllableObject& object, ControllableObject& tObject, position3D offSet, std::string memoryPoint, int rope, std::string bone, bool collisions = false);


		//-------------------------------------------------------
		// Object AI utilities
		//-------------------------------------------------------
		
		/*!
		Disable the AI behavior of the controllable object.

		Deprecated Function.
		void applyAIBehaviourDisable(ControllableObject& co, AIBEHAVIOUR behaviour) instead.
		*/
		VBSFUSION_DPR(UCOB021) static void disableAIbehaviour(ControllableObject& co, AIBEHAVIOUR behaviour);

		/*!
		Disable the AI behavior of the controllable object.

		Deprecated Function.
		void applyAIBehaviourEnable(ControllableObject& co, AIBEHAVIOUR behaviour) instead.
		*/
		VBSFUSION_DPR(UCOB022) static void enableAIbehaviour(ControllableObject& co, AIBEHAVIOUR behaviour);


		//-------------------------------------------------------
		// Existence check utilities
		//-------------------------------------------------------

		/*!
		return true if the object specified by co exists in the VBS2 game environment. 	
		Deprecated Function.
		Use bool isAvailable(ControllableObject& co) instead.
		*/
		VBSFUSION_DPR(UCOB001) static bool ControllableObjectExists(ControllableObject& co);

		/*!
		@description

		Used to get the network id of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]


		@param co - The object for which the NetworkID is obtained. The object should be created before passing it as a parameter.

		@return string

		@example

		@code

		displayString += "Id using getIDUsingalias by passing object"+ControllableObjectUtilities::getIDUsingAlias(co); 

		@endcode

		@overloaded

		None 

		@related

		ControllableObjectUtilities::getNameUsingAlias(ControllableObject& co)

		@remarks Although ControllableObjectUtilities::getIDUsingAlias(ControllableObject& co)is similar to the function ControllableObjectUtilities::getNetworkID(ControllableObject& co), their return types are different. The function getIDUsingAlias () returns the network id as a string whereas the function getNetworkID () returns the NetworkId itself, and thus should be converted using getNetworkIDString(), for it to be used as a string.
		(Refer (ControllableObjectUtilities::getNetworkID(ControllableObject& co)) for further information)

		@remarks - To obtain the network id of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getIDUsingAlias(ControllableObject& co)	
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)  
		*/
		static std::string getIDUsingAlias(const ControllableObject& co);

		/*!
		@description

		Used to get the name or alias of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the Name or Alias is obtained. The object should be created before passing it as a parameter.

		@return string

		@example

		@code

		co.setName(string("unit"));
		displayString += " getNameUsingalias � The name is "+ControllableObjectUtilities::getNameUsingAlias(co);

		@endcode

		@overloaded

		None 

		@related

		ControllableObjectUtilities:: getIDUsingAlias(ControllableObject& co)

		@remarks To obtain the name/alias of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getNameUsingAlias(ControllableObject& co)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)  
		*/
		static std::string getNameUsingAlias(const ControllableObject& co);

		/*!
		@description

		Used to check if an object exists and return true or otherwise.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object which is checked. The object should be created before passing it as a parameter.

		@return bool

		@example

		@code

		if(ControllableObjectUtilities::isNULL(co)==true)
		{ displayString += " NULL : true"; }

		else
		{ displayString += " NULL : false"; } 

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To check the NULL property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isNULL(ControllableObject& co).
		*/
		static bool isNULL(const ControllableObject& co);

		//-------------------------------------------------------
		// Weapons List Utilities 
		//-------------------------------------------------------
		
		/*!
		@description

		Used to get the primary weapon of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the weapon is obtained. The object should be created before passing it as a parameter.

		@return string

		@example

		@code

		displayString+="Weapons are" +(ControllableObjectUtilities::getPrimaryWeapon(co));

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::updatePrimaryWeapon(ControllableObject& co)

		@remarks To obtain the weapons of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getPrimaryWeapon(ControllableObject& co).	
		*/
		static std::string getPrimaryWeapon(const ControllableObject& co);

		/*!
		@description

		Used to get the primary weapons of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the primary weapon property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::updatePrimaryWeapon(co); 

		displayString += "Updated primary weapon "+co.getPrimaryWeapon();

		@endcode

		@overloaded

		None 

		@related

		ControllableObjectUtilities::getPrimaryWeapon(ControllableObject& co)

		@remarks To update the primary weapon property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::updatePrimaryWeapon (ControllableObject& co)  
		*/
		static void updatePrimaryWeapon(ControllableObject& co);

		/*!
		@description
		Used to update the list of weapon types assigned to the object created within the VBS2 Environment in the fusion end. A new WeaponAmmo is structure and is created for each recognized weapon type and the new structure is added to the weapons list. Each new weapon
		is assigned an ammo value of 0. 

		Used to update the list of weapon types assigned to the object created within the VBS2 Environment in the VBS2Fusion end. A new WeaponAmmo is structure and is created for each recognized weapon type and the new structure is added to the weapons list. Each new weapon is assigned an ammo value of 0. 

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		Used to update the list of weapon types assigned to the object created within the VBS2 Environment in the VBS2Fusion end. A new WeaponAmmo structure is created for each recognized weapon type and the new structure is added to the weapons list. Each new weapon is assigned an ammo value of 0.

		@param co - The object for which the list of weapons is updated in the fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;

		ControllableObjectUtilities::updateWeaponTypes(co)

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::updateWeaponAmmo(ControllableObject& co)

		@remarks This function does not update the ammo amounts on the weapons magazines. Use updateWeaponAmmo to update the ammo type

		@remarks To update the list of weapon types assigned to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateWeaponTypes(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static void updateWeaponTypes(ControllableObject& co);

		/*!
		Select the Weapon given by the muzzle Name.
		
		Deprecated Function.
		Use void applyWeaponSelection(ControllableObject& co, string muzzleName) instead.
		*/
		VBSFUSION_DPR(UCOB036) static void selectWeapon(ControllableObject& co, std::string muzzleName);

		/*!
		@description

		Updates the amount of ammo left in the primary magazine of each assigned weapon type. It uses the list defined by the weapons list, and thus only the ammo information for each weapon on that list would be loaded.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the amount of ammo is updated in the fusion end. The object should be created before passing it as a parameter.
		
		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;

		ControllableObjectUtilities::updateWeaponAmmo(co)

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function should be used after the function updateWeaponTypes is called.

		@remarks To update the amount of ammo left in the primary weapon of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::updateWeaponAmmo(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static void updateWeaponAmmo(ControllableObject& co);



		//-------------------------------------------------------
		//Magazine Utilities 
		//-------------------------------------------------------

		/*!
		Add magazine to the controllable object.
		*/
		static void addMagazine(ControllableObject& co, const std::string& name);

		/*!
		Count number of magazines available in given controllable object.
		*/
		static int countMagazines(const ControllableObject& co);

		//-------------------------------------------------------
		// Magazines List Utilities 
		//-------------------------------------------------------

		/*!
		@description

		Updates the list of magazines assigned to the object within the VBS2 environment. This function loads all magazines assigned to the object from VBS2 and adds a new MagazineAmmo structure to the magazines list whenever a distinct magazine type is found. If a magazine
		type which is already assigned is found, it increments the 'number' variable for the representative list item to indicate that there are multiple assignments of the same magazine.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the list of magazines is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;

		ControllableObjectUtilities::updateMagazineTypes(co);

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks This function does not update the ammo amount list. An empty list is created for each newly created entry on the list. (Use the function updateMagazineAmmo to update this list).

		@remarks This function uses the network ID to access the object within the VBS2 environment, so please ensure that a registered network ID is assigned to co.

		@remarks To update the list of magazines assigned to the object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: updateMagazineTypes(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void updateMagazineTypes(ControllableObject& co);

		/*!
		@description

		Used to update the amount of ammo left in each magazine assigned to the object within the VBS2 environment and thereby updates the ammo amount list. The function uses the magazine types defined in the existing magazine list to search for available ammo, and therefore will not load ammo information for any magazines which are not on this list.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the amount of ammo is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.
		
		@return Nothing

		@example

		@code

		// The object to be created
		ControllableObject co;
		ControllableObjectUtilities::updateMagazineAmmo(co);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function should be used after the function updateMagazineTypes(ControllableObject& co) is called.

		@remarks This function uses the network ID to access the object within the VBS2 environment, so please ensure that a registered network ID is assigned to co.

		@remarks To update the amount of ammo left in each magazine of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::updateMagazineAmmo(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)	
		*/

		static void updateMagazineAmmo(ControllableObject& co);


		//-------------------------------------------------------
		// Respawn function  
		//-------------------------------------------------------

		/*!
		Respawns the given object immediately.  
		When object is respawn, respawn event handler only fires where the respawned unit is local.
		
		Deprecated function.
		Use the functions related with particular type of object if needed.
		Eg: If you want to respawn a unit, use UnitUtilities::applyRespawn(Unit& unit ) function 
		*/
		VBSFUSION_DPR(UCOB026) static void doRespawn(ControllableObject& co);

		/*!
		Respawns the unit. Care should be taken when using this command as VBS2 automatically
		assigns a new name to the respawned unit (this does not happen if the unit is a named unit). 

		This command should therefore be used in conjunction with a respawn event handler to 
		obtain the new name and to assign an alias to the object. If not, respawning can result in
		the loss of the handle to the unit and therefore could cause errors during the update cycle. 	
		
		Deprecated function.
		Use the functions related with particular type of object if needed.
		Eg: If you want to respawn a unit, use UnitUtilities::applyRespawn(Unit& unit ) function 
		*/
		VBSFUSION_DPR(UCOB026) static void RespawnObject(ControllableObject& co);

		//-------------------------------------------------------
		// Turn function
		//-------------------------------------------------------


		/*!
		 Turning the unit. This is different from using the applyDirection command
		 which immediately sets the direction of the unit to the direction specified. The
		 Turn command simulates the object actually turning in a more realistic manner. 
		 The direction is specified in relation to world coordinates (i.e. 0 along positive
		 x axis, 90 along positive z axie etc. )
		Direction should be specified in degrees. 	

		Deprecated Function.
		Use void applyTurn(ControllableObject& co, double direction) instead.
		*/
		VBSFUSION_DPR(UCOB034) static void Turn(ControllableObject& co, double direction);

		/*!
		 Turning the unit. This is different from using the applyDirection command
		 which immediately sets the direction of the unit to the direction specified. The
		 Turn command simulates the object actually turning in a more realistic manner. 
		 The direction is specified in relation to the objects current direction.
		Direction should be specified in degrees. 

		Deprecated Function.
		Use void applyTurnRelative(ControllableObject& co, double relativeDir) instead.
		*/
		VBSFUSION_DPR(UCOB035) static void TurnRelative(ControllableObject& co, double direction);


		//************************************************************
		// Movement controls
		//************************************************************

		/*!
		The moveForward function is a special function to use when a programmer want to control the movement of 
		a unit. This command should be used in conjunction with the get/set functions for MoveDir
		and Speed. A single call to this method will order the unit to move in a straight line towards the
		direction pointed to by getMoveDir() at the speed specified by getSpeed(). Use setMoveDir() to control the direction
		of movement and setSpeed() to control the speed of movement. This command can be called continuously 
		within the OnSimulationStep loop to control and manipulate the movement of the unit. Set speed to 0 to stop 
		the unit from moving. 

		The unit will keep walking forward until an obstacle is reached. When an obstacle is reached, the unit will stop. 

		Is a crude method for controlling direction and speed (i.e. steering) of an object. 

		NOTE: this function will not work if used on the player unit. Call periodically instead of at every
		OnSimulationStep call as the unit walks 20 meters ahead at each call of this function. A call every 2-3 seconds 
		will provide the same functionality as a call at every OnSimulation step and will also improve performance.

		You should however make a call to this function soon after making any changes to the moveDirection
		or speed properties of an object for them to register immediately. 

		Disable 'PATHPLAN' AI for the unit if you do not want it to perform path planning and stop at obstacles. 

		Deprecated Function.
		Use void applyMoveForward(ControllableObject& co) instead.
		*/
		VBSFUSION_DPR(UCOB041) static void moveForward(ControllableObject& co);



		//-------------------------------------------------------
		// NetworkId utility
		//-------------------------------------------------------
		
		/*!
		@description

		Used to update the isLocal property of an object within the VBS2 Environment in the VBS2Fusion end.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the isLocal property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::updateIsLocal(co);
		displayString = " Update Is Local: " conversions::BoolToString(co.isLocal()); 

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To update the isLocal property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::updateIsLocal(ControllableObject& co) 		
		*/
		static void updateIsLocal(ControllableObject& co);		


		//-------------------------------------------------------
		// Camera Mode utilities
		//-------------------------------------------------------
		/*!
		@description

		Used to apply a camera mode to the object created within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param c � The object for which the camera mode is applied. The object should be created before passing it as a parameter.
		@param camMode � The camera mode which is applied to the object . There are 4 types
		CAMMODE_INTERNAL, CAMMODE_EXTERNAL, CAMMODE_GUNNER, CAMMODE_GROUP

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyCamMode(co,CAMMODE_EXTERNAL);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To apply a camera mode to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyCamMode(ControllableObject& co, CAMMODETYPE camMode) 
		*/
		static void applyCamMode(ControllableObject& c, CAMMODETYPE camMode);

		/*! 
			Check whether the object movement is completed.   

		Deprecated Function.
		Use bool isCommandMoveCompleted(ControllableObject& co) instead.
		*/
		VBSFUSION_DPR(UCOB025) static bool isMoveToCompleted(ControllableObject& co);

		/*!
		Set object to be control externally.	
		Deprecated Function. 
		Use void ControllableObjectUtilities::applyExternalControl(ControllableObject& co, bool control) instead.
		*/
		VBSFUSION_DPR(UCOB002) static void setExternalControl(ControllableObject& co, bool control);

		/*!
		@description

		Used to check whether an object within the VBS2 Environment is externally controlled.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the external control option is checked. The object should be created before passing it as a parameter.
		
		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyExternalControl(co, true)

		if(ControllableObjectUtilities::getExternalControl(co)==true)
		{ displayString +="externally controlled"; }

		else
		{ displayString += "not externally controlled"; } 

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::applyExternalControl(ControllableObject& co)

		@remarks To perform the function getExternalControl(ControllableObject& co)on an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getExternalControl(ControllableObject& co).
		*/
		static bool getExternalControl(const ControllableObject& co);

		/*!
		Simulate user input. Amount should be 0 to 1 for digital actions, -1 to 1 for analog.
		Deprecated Function.
		Use applyKeyAction(string keyStroke, double amount) function.
		*/
		VBSFUSION_DPR(UCOB003) static void setKeyAction(ControllableObject& co,std::string keyStroke, float amount);

		/*!
		Simulate user input for specified object. Amount should be 0 to 1 for digital actions, -1 to 1 for analog.
		This method works for externally controlled objects.
		Deprecated Function.
		Use applyKeyAction(ControllableObject& co, string keyStroke, double amount);
		*/
		VBSFUSION_DPR(UCOB004) static void setUnitAction(ControllableObject& co,std::string keyStroke, double amount);

		/*!
		Simulate user input. Amount should be 0 to 1 for digital actions, -1 to 1 for analog.
		Deprecated Function.
		Use applyKeyAction(string keyStroke, double amount) function.
		*/
		VBSFUSION_DPR(UCOB003) static void setKeyAction(ControllableObject& co,std::string keyStroke, double amount);

		/*!
		Simulate user input for specified object. Amount should be 0 to 1 for digital actions, -1 to 1 for analog.
		This method works for externally controlled objects.
		Deprecated Function.
		Use applyKeyAction(ControllableObject& co, string keyStroke, double amount);
		*/
		VBSFUSION_DPR(UCOB004) static void setUnitAction(ControllableObject& co,std::string keyStroke, float amount);

		/*!
		Suspend the simulation of the object. 
		If collidable is true,then object can collide else object is disappeared form VBS2. 

		Deprecated Function.
		Use void applySuspend(ControllableObject& co , bool collidable) instead
		*/
		VBSFUSION_DPR(UCOB032) static void Suspend(ControllableObject& co , bool collidable = true );

		/*!
		Reactivate a suspended object to its normal status.

		Deprecated Function.
		Use void applyReactivate(ControllableObject& co) instead
		*/
		VBSFUSION_DPR(UCOB033) static void Reactivate(ControllableObject& co);

		/*!
		@description

		Used to obtain the simulation mode of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the simulation mode is obtained. The object should be created before passing it as a parameter.
		
		@return int

		@example

		@code

		if (ControllableObjectUtilities::getSimulationMode(co)==0)
		{ displayString += "Simulation mode is zero"; }

		else if (ControllableObjectUtilities::getSimulationMode(co)==0)
		{ displayString += "Simulation mode is one"; }

		else
		{ displayString += "Simulation mode is two"; } 

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::updateSimulationMode(ControllableObject& co)

		@remarks To get the simulation mode of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getSimulationMode(ControllableObject& co).
		*/
		static int getSimulationMode(const ControllableObject& co);

		/*!
		@description

		Used to update the simulation mode of an object created within the VBS2 Environment in the VBS2Fusion end.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the simulation mode property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::updateSimulationMode(co);
		if(co.getSimulationMode()==0)
		{ displayString += "Updated -Simulation mode is zero"; }

		else if (co.getSimulationMode()==0)
		{ displayString += "Updated - Simulation mode is one"; }

		else
		{ displayString += "Updated -Simulation mode is two"; } 

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::getSimulationMode(ControllableObject& co)

		@remarks To update the simulation mode property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::updateSimulationMode(ControllableObject& co) 
		*/
		static void updateSimulationMode(ControllableObject& co);

		/*!
		@description
		
		Used to apply a simulation mode to the object created within VBS2.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the simulation mode is applied. The object should be created before passing it as a parameter.

		@param simmode � The camera mode which is applied to the object . There are 3types
		SIM_NORMAL => [Normal  - 0 ] Object is simulated and drawn.
		SIM_FROZEN => [Frozen  - 1 ] Object is NOT simulated, but is still drawn and still can collide.
		SIM_HIDDEN => [Hidden - 2 ] Object is not simulated and not drawn. Object can't collide with others.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applySimulationMode(co,SIM_FROZEN);

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::getSimulationMode(ControllableObject& co)
		ControllableObjectUtilities::updateSimulationMode(ControllableObject& co)

		@remarks To apply a simulation mode to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applySimulationMode(ControllableObject& co, SIMULATION_MODE simmode) 
		*/
		static void applySimulationMode(ControllableObject& co , SIMULATION_MODE simmode);

		/*!
		Perform specified action by the controllable object. 
		- if the action is done by the object itself, same object should be passed as "co" and "targetCo"
		- if the action is done by or on a different object, objects should be passed accordingly.
		
		Deprecated function.
		Use void applyAction(ControllableObject& co, string type, ControllableObject& targetCo) instead.
		*/
		VBSFUSION_DPR(UCOB040) static void performAction(ControllableObject& co, std::string type, ControllableObject& targetCo);

		/*!
		Perform specified action by the controllable object. 
		- if the action is done by the object itself, same object should be passed as "co" and "targetCo"
		- if the action is done by or on a different object, objects should be passed accordingly.

		Use this function if the action is defined in the form of [type, param1, param2, param3]

		Deprecated function.
		Use correct overloaded version of applyAction function instead.
		1. void applyAction(ControllableObject& co,string actionName ,ControllableObject& targetCo, ControllableObject& secondTargetCo)
		2. void applyAction(ControllableObject& co,string actionName ,ControllableObject& targetCo, int intFirst)
			3. void applyAction(ControllableObject& co,string actionName ,ControllableObject& targetCo, Turret::TurretPath& turretPath);
		*/
		VBSFUSION_DPR(UCOB042) static void performAction(ControllableObject& co, std::list<std::string> strpara, ControllableObject& targetCo );

		/*!
		Limit speed of a object to a defined speed value.
		Deprecated function.
		Use applySpeedLimit(ControllableObject& co, double speedLimit) instead.
		*/
		VBSFUSION_DPR(UCOB005) static void limitSpeed(ControllableObject& co , double speedValue);

		/*!
		Forces the speed of the unit. object will never attempt to move faster than given by forceSpeed.

		Deprecated function.
		Use void applySpeed(ControllableObject& co, double speed) instead.
		*/
		VBSFUSION_DPR(UCOB024) static void forceSpeed(ControllableObject& co, double forceSpeedVal);

		/*!
		@description

		Used to apply the alias defined by the function co.setAlias(string& strName) to the object created within the VBS2.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the alias is applied. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//set the alias before applying the alias to the object
		co.setAlias(string("c"));
		ControllableObjectUtilities::applyAlias(co);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To apply an alias to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyAlias(ControllableObject& co) 
		*/
		static void applyAlias(ControllableObject& co);

		/*!
		@description

		Used to apply the URN call sign defined by the function co.setURLCallSign(string& strName) to the object created within the VBS2.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the URN call sign is applied to. The object should be created before passing it as a parameter

		@return Nothing

		@example

		@code

		string URLCS ="urlcs";
		co.setURLCallSign(URLCS);
		ControllableObjectUtilities::applyURNCallSign(co);
		displayString +="Name is "+ControllableObjectUtilities::getURNCallSign(co);

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::updateURNCallSign(ControllableObject& co)
		ControllableObjectUtilities::getURNCallSign(ControllableObject& co)

		@remarks To apply a URN call sign to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyURNCallSign(ControllableObject& co)
		*/
		static void applyURNCallSign(ControllableObject& co);

		/*!
		@description

		Used to obtain the URN call sign of the object created within the VBS2.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the URN call sign is obtained. The object should be created before passing it as a parameter.

		@return String

		@example

		@code

		displayString +="Name is "+ControllableObjectUtilities::getURNCallSign(co);

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::applyURNCallSign(ControllableObject& co)
		ControllableObjectUtilities::updateURNCallSign(ControllableObject& co)

		@remarks To obtain the URN call sign of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getURNCallSign(ControllableObject& co)
		*/
		static std::string getURNCallSign(const ControllableObject& co);

		/*!
		@description

		Used to update the URN call sign of an object created within the VBS2 Environment in the VBS2Fusion end .

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the URN call sign property is updated in the VBS2Fusion end. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::updateURNCallSign(co);
		displayString +="Updated URNCallSign is "+co.getURLCallSign();

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::applyURNCallSign(ControllableObject& co)
		ControllableObjectUtilities::getURNCallSign(ControllableObject& co)

		@remarks To update the URN call sign property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::updateURNCallSign(ControllableObject& co)
		*/
		static void updateURNCallSign(ControllableObject& co);

		/*!
		Allow or prevent an object being damaged(injured or killed). 

		Deprecated Function.
		Use void applyDamageEnable(ControllableObject& co , bool allow);
		*/
		VBSFUSION_DPR(UCOB023) static void allowDamage(ControllableObject& co , bool allow);

		/*!
		@description

		Used to delete an object created within the VBS2..

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co- The object which is deleted. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::deleteObject(co);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - To delete an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::deleteObject(ControllableObject& co)
		*/
		static void deleteObject(ControllableObject& co);		

		/*!
		Turns different geometry settings on and off.
		A true argument turns the specific setting OFF

		Deprecated Function.
		Use void applyGeometrySettingsDisable(ControllableObject& co, bool visible, bool fire, bool view, bool collision, bool roadways, bool shadows) instead
		*/
		VBSFUSION_DPR(UCOB039) static void disableGeo(ControllableObject& co, bool visible, bool fire, bool view, bool collision);


		/*!
		@description

		Used to apply the position defined by the function co.setPositionASL(position3D& position) to an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the position is applied. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		co.setPositionASL(player.getPosition()+position3D(50,50,0));
		ControllableObjectUtilities::applyPositionASL(co); 

		@endcode

		@overloaded

		ControllableObjectUtilities::applyPositionASL(ControllableObject& co, position3D aslPos)

		@related

		None

		@remarks If there are elevated surfaces at the specified position (e.g. rooftops) their elevation will not be added to the entered Z-value (e.g. the roof of a building is 3m AGL, and 21m ASL. If setPosASL of the player is set to a height of 21, he will not end up at at elevation of 24m! but at 21m!)To prevent this situation, use the function ControllableObjectUtilities::applyPositionASLEX(ControllableObject& co, position3D aslPosEx).

		@remarks If the positional information is invalid or not set, the object would be moved to map location [0,0,0].

		@remarks To apply the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::applyPositionASL(ControllableObject& co).
		*/
		static void applyPositionASL(ControllableObject& co);

		/*!
		@description

		Used to apply a position above sea level to an object within the VBS2 Environment. 

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the position is applied. The object should be created before passing it as a parameter.
		@param aslPos - The position where the object would be moved.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyPositionASL(co,position3D(100,100,0));

		@endcode

		@overloaded

		ControllableObjectUtilities::applyPositionASL(ControllableObject& co)

		@related

		None

		@remarks If there are elevated surfaces at the specified position (e.g. rooftops) their elevation will not be added to the entered Z-value (e.g. the roof of a building is 3m AGL, and 21m ASL. . If setPosASL of the player is set to a height of 21, he will not end up at at elevation of 24m! but at 21m!)To prevent this situation, use the function  ControllableObjectUtilities::applyPositionASLEX(ControllableObject& co, position3D aslPosEx). 

		@remarks If the positional information is invalid or not set, the object would be moved to map location [0,0,0].

		@remarks To apply the position for an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::applyPositionASL(ControllableObject& co, position3D aslpos).
		*/
		static void applyPositionASL(ControllableObject& co, position3D aslPos);

		/*!
		Tells whether object can float or not. Returns true if the object can float.

		Deprecated Function.
		Use bool isFloatable(ControllableObject& co) instead.
		*/
		VBSFUSION_DPR(UCOB037) static bool canFloat(ControllableObject& co);

		/*!
		@description

		Used to check if an object is attached to another object within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object which is checked. The object should be created before passing it as a parameter. 

		@return bool 

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString+="is Attached"+conversions::BoolToString(ControllableObjectUtilities::isAttached(co));

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To check whether an object created in a client machine within a network is attached to another object, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isAttached(ControllableObject& co).	
		*/
		static bool isAttached(const ControllableObject& co);

		/*!
		@description

		Used to obtain the total weight of the weapons and magazines carried by the object created within the VBS2.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the total arms weight is obtained. The object should be created before passing it as a parameter.

		@return Double

		@example

		@code

		//To display the double as a string use the function DoubleToString() as shown below
		displayString +="Answer is "+conversions::DoubleToString (ControllableObjectUtilities::getArmsTotalWeight(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the total arms weight of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getArmsTotalWeight(ControllableObject& co)	
		*/
		static double getArmsTotalWeight(const ControllableObject& co);

		/*!
		@description

		Used to check if an object is touching another object.(Only objects that would lead to collisions are checked e.g. a car with another car, car with a tree, etc.)

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co � The object which is checked. The object should be created before passing it as a parameter.

		@return bool

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString ="Touching Object: " conversions::BoolToString(ControllableObjectUtilities::isTouchingObject (co));

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To check whether an object created in a client machine within a network is touching another object, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isTouchingObject(ControllableObject& co).
		*/
		static bool isTouchingObject(const ControllableObject& co);

		/*!
		@description

		Used to obtain the angular velocity of an object within the VBS2 Environment. 

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co - The object for which the angular velocity is obtained. The object should be created before passing
		it as a parameter. 

		@return vector3D - The return value represents an angular change, based on the x,y & z axes. An object going
		straight will return zero for all axes.

		@example

		@code

		//To convert the vector into string use getVBSPosition
		string displayvelocity= ControllableObjectUtilities::getAngularVelocity(co).getVBSPosition();

		displayString += "ANGULAR VELOCITY IS"+displayvelocity;

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To obtain the angular velocity of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getAngularVelocity(ControllableObject& co).
		*/
		static vector3D getAngularVelocity(const ControllableObject& co);

		/*!
		@description

		Used to get the acceleration of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the acceleration is obtained. The object should be created before passing it as a parameter.

		@return vector3D

		@example

		@code

		/To display the acceleration of an object at each instant use getAcceleration in the 'OnSimulationStep' callback
		//To display the acceleration as a string use the function getVBSPosition() as shown below

		string acceleration= ControllableObjectUtilities::getAcceleration(veh1).getVBSPosition();
		displayString +="Answer is "+acceleration;
		DisplayFunctions::DisplayString(displayString);

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To obtain the acceleration of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getAcceleration(ControllableObject& co).

		@remarks To display the acceleration of an object at each instant use getAcceleration in the 'OnSimulationStep'.
		*/
		static vector3D getAcceleration(const ControllableObject& co);

		/*!
		Disables or enables user actions for a specific object. Default value is set as true. It will disable 
		the user actions on to that object. Users can't interact with objects that have actions disabled.

		Deprecated Function.
		Use void applyActionsDisable(ControllableObject& co, bool disable=true) instead.
		*/
		VBSFUSION_DPR(UCOB038) static void disableActions(ControllableObject& co, bool action=true);

		/*!
		@description

		Used to check if an object can interact with other objects and return true or otherwise.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co � The object for which the actions are disabled. (I.e. Other objects wouldn�t be able to interact with this object).

		@return bool

		@example

		@code

		if (ControllableObjectUtilities::isActionsDisabled(co)==true)
		{ displayString+="actions disabled"; }

		else
		{ displayString+="actions not disabled"; } 

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To check the ActionsDisabled property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isActionsDisabled(ControllableObject& co).
		*/
		static bool isActionsDisabled(const ControllableObject& co);

		/*!
		@description

		Used to apply the draw mode for an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co � The object for which the draw mode is applied to. The object should be created before passing it as a parameter.

		@param mode � The mode applied to the object, there are 3 types (DM_NORMAL, DM_TRANSPARENT, DM_WIREFRAME)

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyDrawMode(co, DRAWMODE::DM_TRANSPARENT);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyDrawMode(ControllableObject& co, DRAWMODE mode, Color_RGBA color, string lod)
		ControllableObjectUtilities::applyDrawMode(ControllableObject& co, DRAWMODE mode, Color_RGB color, double transMin, double transMax, string lod);

		@related

		ControllableObjectUtilities::getDrawMode(ControllableObject& co)
		*/
		static void applyDrawMode(ControllableObject& co, DRAWMODE mode);

		/*!
		@description

		Used to apply the draw mode for an object within the VBS2 Environment. 

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co � The object for which the draw mode is applied to. The object should be created before passing it as a parameter.

		@param mode � The mode applied to the object, there are 3 types (DM_NORMAL, DM_TRANSPARENT, DM_WIREFRAME)

		@param color - The amount of red, green, and alpha applied to the object. (Alpha determines the transparency level)

		@param lod - (Level of Detail). It defines the viewable quality of the model and how it would interact with the environment. Refer appendix in the VBS2Fusion Product Manual.

		@return Nothing

		@example

		@code

		/defining the color values
		Color_RGBA colveh;
		col2.r=0;
		col2.g=1;
		col2.b=1;
		col2.a=1;

		ControllableObjectUtilities::applyDrawMode(veh,DRAWMODE::DM_TRANSPARENT,colveh,"pilot");

		@endcode

		@overloaded

		ControllableObjectUtilities::applyDrawMode(ControllableObject& co, DRAWMODE mode, Color_RGB color, double transMin, double transMax, string lod)
		ControllableObjectUtilities::applyDrawMode(ControllableObject& co, DRAWMODE mode)

		@related

		ControllableObjectUtilities::getDrawMode(ControllableObject& co)
		*/
		static void applyDrawMode(ControllableObject& co, DRAWMODE mode, const Color_RGBA& color, const std::string& lod);

		/*!
		@description

		Used to apply the draw mode for an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the draw mode is applied to.

		@param mode � The mode applied to the object, there are 3 types (DM_NORMAL, DM_TRANSPARENT, DM_WIREFRAME)

		@param color - The amount of red, green, and alpha applied to the object.(alpha determines the transparency level)

		@param transMin - The minimum alpha value , it specifies the minimum transparency

		@param transMax - The maximum alpha value, it specifies the maximum transparency

		@param lod - (Level of Detail). It defines the viewable quality of the model and how it would interact with the environment. Refer appendix in the VBS2Fusion Product Manual.

		@return Nothing

		@example

		@code

		//defining the color values
		Color_RGB col2;

		col2.r=0;
		col2.g=1;
		col2.b=1;
		ControllableObjectUtilities::applyDrawMode(veh,DRAWMODE::DM_TRANSPARENT,col2,0.45,0.62,"Pilot");

		@endcode

		@overloaded

		ControllableObjectUtilities::applyDrawMode(ControllableObject& co, DRAWMODE mode, Color_RGBA color, string lod)
		ControllableObjectUtilities::applyDrawMode(ControllableObject& co, DRAWMODE mode)

		@related

		ControllableObjectUtilities::getDrawMode(ControllableObject& co)
 
		*/
		static void applyDrawMode(ControllableObject& co, DRAWMODE mode, const Color_RGB& color, double transMin, double transMax, const std::string& lod);

		/*!
		@description

		Used to get the draw mode of an object within the VBS2 Environment

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the DrawMode is obtained. The object should be created before passing it as a parameter.

		@return string

		@example

		@code

		displayString+= ControllableObjectUtilities::getDrawMode(veh);

		@endcode

		@overloaded

		None

		@related

		None
		*/
		static std::string getDrawMode(const ControllableObject& co);

		/*!
		@description

		Used to attach a message to an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the message is attached. 

		@param message - The message attached to the object

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyDisplayText(co,"message");

		@endcode

		@overloaded

		ControllableObjectUtilities::applyDisplayText (ControllableObject& co, string message, vector3D offset, bool mode, Color_RGBA color, double size);

		@related

		None

		@remarks To attach a message to an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyDisplayText(ControllableObject& co, string message)
		*/
		static void applyDisplayText (ControllableObject& co, const std::string& message);

		/*!
		@description

		Displays message that is attached to the object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.08]


		@param co - The object for which the message is attached. 

		@param message - Text to be displayed

		@param offset - The positional offset to the object

		@param mode - Whether the message should be displayed in 3D or 2D. In 2D mode text will keep its size, no matter
		the distance to the player, and will be visible through obstructions.  In 3D mode the text size changes
		depending on the distance to the player, and will be hidden by obstructions (True means 2D and otherwise)

		@param color - The color of the text.

		@param size - The size of the message.

		@return Nothing.

		@example

		@code

		string message = "message";
		Color_RGBA col = {1,0,0,1};

		ControllableObjectUtilities::applyDisplayText(co, message, vector3D(0,0,5), true, col, 1.5); 

		@endcode

		@overloaded

		ControllableObjectUtilities::applyDisplayText (ControllableObject& co, string message)

		@related

		None
		*/
		static void applyDisplayText (ControllableObject& co, const std::string& message, const vector3D& offset, bool mode, const Color_RGBA& color, double size);

		/*!
		@description

		Used to change the size of an object in all three scales within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the size is changed. The object should be created before passing it as a parameter. 

		@param width - The width applied to the object.

		@param height - The height applied to the object.

		@param length - The length applied to the object.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyScale(co,50.5,200.5,100.5)

		@endcode

		@overloaded

		None

		@related

		None

		@remarks For most objects the scale will reset itself at each game cycle (i.e. several times a second), especially if any shooting or explosions occur near the object.

		@remarks Scale of units is also reset when entering and leaving vehicles, thus this command should be executed wisely in controlled circumstances.
		*/
		static void applyScale(ControllableObject& co, double width, double height, double length);


		/*!
		@description

		Used to apply a force on an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the force is applied. The object should be created before passing it as a parameter. 

		@param force - The force vector is in global coordinates and the magnitude of the force corresponds to Newtons. 

		@param position - The position to which the force is applied(in model coordinates)

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyForce(co,vector3D(0,0,990), position3D(100,100,0));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks
		*/
		static void applyForce(ControllableObject& co, const vector3D& force, const position3D& position);


		/*!
		@description

		Used to check if a particular AI of an object within the VBS2 environment is enabled or disabled.	.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the AI is checked. The object should be created before passing it as a parameter. 

		@param type - The type of AI behavior checked.

		@return bool

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString+="AI enabled"+conversions::BoolToString(ControllableObjectUtilities::isEnabledAI(co,AIBEHAVIOUR::AIMOVE));

		@endcode

		@overloaded

		None 

		@related

		None 
		*/
		static bool isEnabledAI(const ControllableObject& co, AIBEHAVIOUR type);

		/*!
		@description

		Used to obtain the name of the object created within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the name is obtained. The object should be created before passing it as a parameter.

		@return double

		@example

		@code

		displayString+="Answer is "+ControllableObjectUtilities::getVarName(co); 

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain name/alias of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getVarName(ControllableObject& co)	

		@remarks If a name or alias has not been assigned to the object. The default name of the object is displayed.
		*/
		static std::string getVarName(const ControllableObject& co);

		/*!
		@description

		Used to get the distance between two object created within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - One of the two objects, for which the in-between distance is obtained. The objects should be created before passing it as a parameter.
		@param tco - One of the two objects, for which the in-between distance is obtained. The objects should be created before passing it as a parameter.

		@return double

		@example

		@code

		//To display the double as a string use the function DoubleToString() as shown below

		displayString+="Distance is "+conversions::DoubleToString(ControllableObjectUtilities::getDistance(co,co1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the distance between the objects created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getDistance(ControllableObject& co, ControllableObject& tco)
		*/
		static double getDistance(const ControllableObject& co, const ControllableObject& tco);
		
		/*!
		@description

		Used to apply a position above sea level to an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the position is applied. The object should be created before passing it as a parameter.

		@param aslPosEx -The position where the object would be moved.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyPositionASLEx(co,player.getPosition()+position3D(200,100,0));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks If there are elevated surfaces at the specified position (e.g. rooftops) their elevation will be added to the entered Z-value (e.g. the roof of a building is 3m AGL, and 21m ASL. If I setPosASL the player to a height of 21, he will end up at at elevation of 24m!) To prevent this situation, use the function ControllableObjectUtilities::applyPositionASL

		@remarks If the positional information is invalid or not set the object would be moved to map location [0,0,0].

		@remarks To apply the position for an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::applyPositionASLEx(ControllableObject& co, position3D aslposEx).
		*/
		VBSFUSION_DPR(UCOB048) static void applyPositionASLEx(ControllableObject& co, position3D aslPosEx);

		/*!
		Returns if the given object is still able to move. This command checks only the damage value, 
		not the amount of fuel. 

		Deprecated Function.
		Use bool isAbleToMove(ControllableObject& co) instead.
		*/
		VBSFUSION_DPR(UCOB029) static bool canMove(ControllableObject& co);

		/*!
		Returns if the given object is still able to fire. This command checks only the damage value, not the ammo. 

		Deprecated Function.
		Use bool isAbleToFire(ControllableObject& co) instead.
		*/
		VBSFUSION_DPR(UCOB030) static bool canFire(ControllableObject& co);

		/*!
		Returns a given object's bounding box's minimum extreme points in model coordinate space.
		Deprecated. Use vector3D getBoundingBoxMinExtremePoints(ControllableObject& co).
		*/
		VBSFUSION_DPR(UCOB043) static vector3D getBoundingBoxMinExtremePoints(ControllableObject& co, ControllableObject& tco);

		/*!
		Returns a given object's bounding box's maximum extreme points in model coordinate space. 
		Deprecated. Use vector3D getBoundingBoxMaxExtremePoints(ControllableObject& co).
		*/
		VBSFUSION_DPR(UCOB044) static vector3D getBoundingBoxMaxExtremePoints(ControllableObject& co, ControllableObject& tco);

		/*!
		@description

		Used to check if an object is hidden within the VBS2 environment

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object which is checked. The object should be created before passing it as a parameter.

		@return bool

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString ="Hidden: " conversions::BoolToString(ControllableObjectUtilities::isHidden(co));

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To check whether an object created in a client machine within a network is hidden, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isHidden(ControllableObject& co).
		*/
		static bool isHidden(const ControllableObject& co);

		/*!
		@description

		Used to get the length of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the length is obtained. The object should be created before passing it as a parameter.

		@return double

		@example

		@code

		//To display the double as a string use the function DoubleToString() as shown below

		displayString+="length is"+conversions::DoubleToString(ControllableObjectUtilities::getLength(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the length of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getLength(ControllableObject& co)
		*/
		static double getLength(const ControllableObject& co);

		/*!
		@description

		Used to get the weight of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the weight is obtained. The object should be created before passing it as a parameter.

		@return double

		@example

		@code

		//To display the double as a string use the function DoubleToString() as shown below

		displayString+="weight is"+conversions::DoubleToString(ControllableObjectUtilities::getWeight(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the weight of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getWeight(ControllableObject& co).
		*/
		static double getWeight(const ControllableObject& co);

		/*!
		@description

		Used to get the volume of an object within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object for which the volume is obtained. The object should be created before passing it as a parameter.

		@return double

		@example

		@code

		//To display the double as a string use the function DoubleToString() as shown below

		displayString+="weight is"+conversions::DoubleToString(ControllableObjectUtilities::getVolume(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the volume of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getVolume(ControllableObject& co).
		*/
		static double getVolume(const ControllableObject& co);

		/*!
		@description

		Used to check whether an object is activated in the VBS2 Environment. If the object is in the mission and linked correctly with VBS2Fusion, the function will return true.

		@locality 

		Global

		@version [VBS2Fusion v3.03]

		@param co� The object which is checked

		@return bool

		@example

		@code

		displayString ="Activated " conversions::BoolToString(ControllableObjectUtilities::activate(co,co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function also updates the properties of the Controllable Object.	

		@remarks To check the activated property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::activate(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static bool activate(ControllableObject& co);

		/*!
		@description

		Used to set an object�s position relative to the terrain, within the VBS2 environment. 

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param  co � The object for which the position is set. The object should be created before passing it as a parameter. 

		@param pos - The position of the controllable Object. 
		
		@return Nothing

		@example

		@code

		ControllableObjectUtilities::setPositionATL(co, player.getPosition()+position3D (0,-10,0)) 

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities:: getPositionATL(ControllableObject& co).

		@remarks If the positional information is invalid or not set, the object would be moved to map location [0,0,0].

		@remarks To set the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: setPositionATL(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static void setPositionATL(ControllableObject& co, const position3D& pos);
		
		/*!
		@description

		Used to obtain the position of an object relative to the terrain within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co - The object for which the position is obtained. The object should be created before passing it as a parameter. 

		@return position3D

		@example

		@code

		displayString+= "Position of co above terrain level is"+ControllableObjectUtilities::getPositionATL(co).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Positions of objects created with no positional information or invalid positions can also be obtained using this function, although their created at map location [0, 0, 0]

		@remarks To obtain the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getPositionATL(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static position3D getPositionATL(const ControllableObject& co);

		/*!
		@description

		Used to return the object's rendered position in format <ar>Position</ar> within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co - The object for which the position is obtained. The object should be created before passing it as a parameter. 

		@return Nothing

		@example

		@code

		displayString+= "Visible position of co is"+ControllableObjectUtilities::getVisblePosition(co).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Positions of objects created with no positional information or invalid positions can also be obtained using this function, although their created at map location [0, 0, 0]

		@remarks To obtain the visible position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getVisiblePosition(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static position3D getVisblePosition(const ControllableObject& co);

		/*!
		@description

		Used to get the rank of a given unit within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co � The object for which the rank is obtained. The object should be created before passing it as a parameter.

		@return string

		@example

		@code

		displayString+="Rank is"+ControllableObjectUtilities::getRanksystem(co);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the rank of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getRanksystem(ControllableObject& co).
		*/
		static std::string getRanksystem(const ControllableObject& co);

		/*!
		@description

		Used to obtain the center of an object in model coordinates within the VBS2 environment

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co � The object for which the position is obtained. The object should be created before passing it as a parameter. 

		@return position3D

		@example

		@code

		displayString+= "Bounding center of co is"+ControllableObjectUtilities::getBoundingCenter(co).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the center of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getBoundingCenter(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static position3D getBoundingCenter(const ControllableObject& co);

		/*!
		@description

		Used to obtain the current weapon of the object within the VBS2 environment

		@locality 

		Globally Applied

		@version [VBS2Fusion v3.05]

		@param co � The object for which the weapon is obtained. The object should be created before passing it as a parameter. 

		@return string

		@example

		@code

		//It is highly recommended to create the object using their respective classes for effective results (for Eg. The object u shown below is created via UnitUtilities::CreateUnit(Unit u)

		displayString+= "Current Weapon of unit is"+ControllableObjectUtilities::getCurrentWeapon(u);

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::getCurrentMagazine(ControllableObject& co)

		@remarks It is highly recommended to create the object using their respective classes for effective results.If the object is a vehicle, ensure that a gunner is present inside the vehicle before calling this function.
		
		@remarks It is returning the current weapon of player object or vehicle object (on the primary turret for vehicles).
		
		@remarks To obtain the current weapon of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getCurrentWeapon (ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static std::string getCurrentWeapon(const ControllableObject& co);

		/*!
		@description

		Used to obtain the current magazine of the object within the VBS2 environment

		@locality 

		Globally

		@version [VBS2Fusion v3.05]

		@param co � The object for which the magazine is obtained. The object should be created before passing it as a parameter. 

		@return string

		@example

		@code

		//It is highly recommended to create the object using their respective classes for effective results (for Eg. The object u shown below is created via UnitUtilities::CreateUnit(Unit u)

		displayString+= "Current Magazine of unit is"+ControllableObjectUtilities:: getCurrentMagazine(u)

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::getCurrentWeapon(ControllableObject& co)

		@remarks If the object is a vehicle, ensure that a gunner is present inside the vehicle before calling this function

		@remarks t is returning the current weapon of player object or vehicle object (on the primary turret for vehicles)

		@remarks To obtain the current magazine of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getCurrentMagazine(ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static std::string getCurrentMagazine(const ControllableObject& co);
		
		/*!
		@description

		 Used to hide an object within the VBS2 environment

		@locality 

		Globally

		@version [VBS2Fusion v3.05]

		@param  co � The object which is hidden. The object should be created before passing it as a parameter

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::hideObject(co);

		@endcode

		@overloaded

		ControllableObjectUtilities::hideObject(ControllableObject& co, bool hidden),

		@related

		None

		@remarks Will not be able to hide static objects

		*/
		static void hideObject(ControllableObject& co);

		/*!
		@description

		Used to check if an object is touching water (fully immersed or partially). If the object is on a shore, it is only temporarily covered in water(whenever there is a wave) thus the returned value may not be 100% accurate.

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co - The object which is checked. The object should be created before passing it as a parameter.

		@return boolean

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString +="Touching Water: "+ conversions::BoolToString(ControllableObjectUtilities::isTouchingWater(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To check whether an object created in a client machine within a network is touching water, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isTouchingWater(ControllableObject& co).	
		*/
		static bool isTouchingWater(const ControllableObject& co); 

		/*!
		@description

		Gets the path of the object�s model within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co - The object for which the path is obtained. The object should be created before passing it as a parameter.

		@return string

		@example

		@code

		displayString =" The path is : "(ControllableObjectUtilities::getModelPath(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the path of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getModelPath(ControllableObject& co).

		@remarks This will return full path & file name of object's model.
		*/
		static std::string getModelPath(const ControllableObject& co); 
		
		/*!
		@description

		Used to control a unit or vehicle by the player. i.e. It applies the player's control input to another unit or vehicle. This function allows the control of multiple slaves simultaneously

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co � The object for which the magazine is obtained. The object should be created before passing it as a parameter. 
		
		@param enable �If true the object will be remotely controlled or otherwise. 
		
		@return Nothing

		@example

		@code

		ControllableObjectUtilities::setRemoteControl(co,true);

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::isRemotelyControlled(ControllableObject& co)

		@remarks The slave's movements will not necessarily be an exact mirror of the player's; it only interprets the inputs, so if the slave runs into obstacles, the paths of the player and the slave could diverge.
		@remarks Vehicles do not have to have a driver to be remotely controlled, but if they do have one, the slave object should be the vehicle, and not the driver. 
		@remarks To control  an object created in a client machine within a network remotely , use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: setRemoteControl (ControllableObject& co, bool enable).
		*/
		static void setRemoteControl(ControllableObject& co, bool enable);

		/*!
		@description

		Used to check if an object is remotely controlled within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param  co � The object which isRemotelyControlled is checked. The object should be created before passing it as a parameter.  

		@return bool

		@example

		@code

		displayString+="co is remotely controlled"+conversions::BoolToString(ControllableObjectUtilities::isRemotelyControlled(co));

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::setRemoteControl (ControllableObject& co, bool enable)

		@remarks To check if an object created in a client machine within a network is remotely controlled, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isRemotelyControlled(ControllableObject& co)
		*/
		static bool isRemotelyControlled(const ControllableObject& co);

		/*!
		@description

		Used to set the object�s AI sensitivity to certain stimuli, within the VBS2 environment. Possible sensitivities that can be manipulated are sight and hearing.

		@locality 

		local

		@version [VBS2Fusion v3.05]

		@param co � The object for which the sensitivity is set. The object should be created before passing it as a parameter. 

		@param type - The type of sense to be manipulated

		@param value - If value = 0 the sense is disabled. Refer Additional Notes to set the sense to its maximum potential. 

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::setSensitivity(co,string("sensitivityEar"),1);

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities:: getSensitivity(ControllableObject& co, string& type)

		@remarks As remarked above, the level of sensitivity depends on the value passed. For example the default values of types �sensitivityEar� and � sensitivity � are 0.4 and 2.5. The detection range depends on multiple factors: the surface type, the movement speed, the engine noise (for vehicles). Setting sensitivityEar to higher values makes AI able to detect sounds from a greater distance, e.g. with a value of 0.4 a unit is heard at a distance of 20m, with a value of 1 it's heard at 30m, etc.
		There will also be random outliers, where sounds are occasionally detected at a much higher range than normally, sometimes by a factor of 2.(Similar to �sensitivityEar� increasing and reducing the values will improve/reduce AI�s visual range.)
		*/
		static void setSensitivity(ControllableObject& co, std::string& type, double value); 

		/*!
		@description

		Used to obtain the AI�s sensitivity to certain stimuli, within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co � The object for which the sensitivity value is obtained. The object should be created before passing it as a parameter. 

		@param type � The sensitivity type, for which the value is obtained.

		@return double

		@example

		@code

		displayString+="The sensitivity value co is is"+conversions::DoubleToString(ControllableObjectUtilities::getSensitivity(co,string("sensitivityEar")));

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::setSensitivity(ControllableObject& co, string& type, double value)

		@remarks To obtain the  sensitivity of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getSensitivity (ControllableObject& co, string& type).
		@remarks Invalid type will return 0.
		*/
		static double getSensitivity(const ControllableObject& co, const std::string& type);

		/*!
		@description

		Used to obtain the position of an object within the VBS2 environment as map coordinates.

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co � The object for which the position is obtained. The object should be created before passing it as a parameter. 

		@return string

		@example

		@code

		displayString+="The map grid position value co is"+(ControllableObjectUtilities::getMapGridPosition(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getMapGridPosition (ControllableObject& co).
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		*/
		static std::string getMapGridPosition(const ControllableObject& co);

		/*!
		@description

		Used to obtain the current morale level of an object within the VBS2 environment. For a vehicle, commander�s morale level is returned

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co � The object for which the morale level is obtained. The object should be created before passing it as a parameter. 

		@return double

		@example

		@code

		displayString+="The Morale level of co is is"+conversions::DoubleToString(ControllableObjectUtilities::getMorale(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the current morale of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getMorale(ControllableObject& co).
		*/
		static double getMorale(const ControllableObject& co);

		/*!
		@description

		Used to hide an object within the VBS2 environment.

		@locality 

		Local

		@version [VBS2Fusion v3.05]

		@param co � The object which is hidden. The object should be created before passing it as a parameter. 

		@param hidden - If the value is true, the object is hidden or otherwise.

		@return Nothing 

		@example

		@code

		ControllableObjectUtilities::hideObject(co, true);

		@endcode

		@overloaded

		ControllableObjectUtilities::hideObject(ControllableObject& co)

		@related

		None

		@remarks Static objects couldn't be able to hide.
		*/
		static void hideObject(ControllableObject& co, bool hidden);

		/*!
		@description

		Used to obtain the unlocalized text value of an object�s side (East, West, Civilian, Resistance, and Unknown) within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co � The object for which the side is obtained. The object should be created before passing it as a parameter. 

		@return string

		@example

		@code

		ControllableObjectUtilities::hideObject(co, true);

		@endcode

		@overloaded

		displayString+="The side text string of co is"+(ControllableObjectUtilities::getSideTextString(co));

		@related

		None

		@remarks To obtain the side of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getSideTextString(ControllableObject& co).
		*/
		static std::string getSideTextString(const ControllableObject& co);

		/*!
		@description

		Used to check if the object�s type is a subtype of the given type.

		@locality 

		Global

		@version [VBS2Fusion v3.02]

		@param co - The object which is checked. The object should be created before passing it as a parameter. 

		@param type � The type with which the object�s type is compared with.
		
		@return bool
		
		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString ="is Kind Of Tank: " conversions::BoolToString(ControllableObjectUtilities::isKindOf(co, �Tank�));

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To check the type an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: isKindOf(ControllableObject& co, string type).
		*/	
		static bool isKindOf(const ControllableObject& co, const std::string& type);

		/*!
		@description

		Used to check if an object is simulated using External Physical Engine and return true or otherwise.

		@locality 

		Global

		@version [VBS2Fusion v3.03]

		@param co - The object which is checked. The object should be created before passing it as a parameter.
		
		@return bool
		
		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString+="EPE is"+conversions::BoolToString(ControllableObjectUtilities::isExternalPhyEngine(co1));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To check the External Physical Engine property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: isExternalPhyEngine(ControllableObject& co).
		*/
		static bool isExternalPhyEngine(const ControllableObject& co);

		/*
		@description

		Used to get the texture of an object within the VBS2 Environment.
		
		@locality 

		Global

		@version [VBS2Fusion v3.03]

		@param co - The object for which the texture is obtained. The object should be created before passing it as a parameter. 

		@param index -Index of hidden selection
		
		@return string
		
		@example

		@code

		displayString+="Object Texture co"+(ControllableObjectUtilities::getObjectTexture(co,0));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the texture of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getObjectTexture(ControllableObject& co, int index).
		*/
		static std::string getObjectTexture(const ControllableObject& co, int index);

		/*!
		@description

		Used to check the identity of an object in the VBS2 Environment. (Refer additional notes 1st point). If either of the objects are null, false is returned.
		
		@locality 

		Global

		@version [VBS2Fusion v3.04]

		@param co1 - The object compared, the object should be created before passing it as a parameter. 

		@param co2- The object compared, the object should be created before passing it as a parameter.
		
		@return boolean
		
		@example

		@code

		displayString ="is Same " conversions::BoolToString(ControllableObjectUtilities::isSame(co,co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Only the object that is controlled by the player during the function call is considered as the player object i.e. once the player switches to another unit, the original player unit during the mission creation would return false
		@remarks This function compares an object with other objects to identify its match. (E.g. if a particular unit named as U1 needs to be identified among 10 units in a group, the object U1 is compared with all the other units, and if the unit matches the function will return true) 
		@remarks To check the isSame property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: isSame(ControllableObject& co1, ControllableObject& co2).
		*/
		static bool isSame(const ControllableObject& co1, const ControllableObject& co2);

		/*!
		@description 

		Used to check whether 2 objects are different in the VBS2 Environment. If either of the objects is null, true is returned.
		
		@locality 

		Global

		@version [VBS2Fusion v3.04]

		@param co1 - The object compared, the object should be created before passing it as a parameter. 

		@param co2 - The object compared, the object should be created before passing it as a parameter.
		
		@return boolean
		
		@example

		@code

		displayString ="is Different " conversions::BoolToString(ControllableObjectUtilities::isDifferent(co,co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Only the object that is controlled by the player during the function call is considered as the player object i.e. once the player switches to another unit, the original player unit during the mission creation would return false

		@remarks To check the isDifferent property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: isDifferent (ControllableObject& co1, ControllableObject& co2).
		*/
		static bool isDifferent(const ControllableObject& co1, const ControllableObject& co2);

		/*!
		Used to apply a position above sea level to an object within the VBS2 Environment. This function has been deprecated, please
		use the function void applyPositionASL(ControllableObject& co, position3D aslPos) instead.

		@param co - The object for which the position is applied. The object should be created before passing it as a parameter.
		@param aslpos - The position where the object would be moved.
		@param considerElev - If considerElev is true the elevation will be added to the entered Z-value (e.g. If there is an elevated surface (e.g. a roof ) at the specified position, its elevation will be added to the entered Z-value) and otherwise.

		@return Nothing

		@remarks If the positional information is invalid or not set the object would be moved to map location [0,0,0].
		@remarks To apply the position for an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyPositionASL(ControllableObject& co, position3D aslpos, bool considerElev).
		*/
		VBSFUSION_DPR(UCOB046) static void applyPositionASL(ControllableObject& co, position3D aslPos, bool considerElev);

		/*!
		Adjusts the slope of the object to follow the underlying terrain contour. 
		(Will not actually place the object on top of the surface, but only adjust its angle.) 

		Deprecated function.
		Use void applyPlaceOnSurface(ControllableObject& co) instead.
		*/
		VBSFUSION_DPR(UCOB031) static void placeOnSurface(ControllableObject& object);

		/*!
		@description

		Used to check if an object is a fence, wall, etc, where the object�s vertical components stay vertical even on a slope.

		@locality 

		Global

		@version [VBS2Fusion v3.04]

		@param object - The object which is checked. The object should be created before passing it as a parameter.

		@return bool

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString ="isClipLandKeep: " conversions::BoolToString(ControllableObjectUtilities::isClipLandKeep(object));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To check the ClipLandKeep property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isClipLandKeep(ControllableObject& object).
		*/
		static bool isClipLandKeep(const ControllableObject& object);

		/*!
		@description

		Used to check if an object is placed on a surface(ground, road, building, etc) and return true or otherwise.

		@locality 

		Global

		@version [VBS2Fusion v3.04]

		@param object - The object which is checked. The object should be created before passing it as a parameter.

		@return boolean

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString +="Touching the Ground: "+ conversions::BoolToString(ControllableObjectUtilities::isTouchingGround(object));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To check whether an object created in a client machine within a network is touching the ground, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isTouchingGround(ControllableObject& object).
		*/
		static bool isTouchingGround(const ControllableObject& object);

		/*!
		Moves the object to a random position.
		Uses either the position that's defined as the first parameter, or one of the marker positions
		from the markers vector. The object is placed inside a circle with this position as its center 
		and placement as its radius. 

		Deprecated function.
		Use applyRandomPosition(ControllableObject& co, position3D pos, vector<Marker> markers, double placement) instead.
		*/
		VBSFUSION_DPR(UCOB010) static void setRandomPosition(ControllableObject& co, position3D pos, std::vector<Marker> markers, double placement);

		/*!
		Preload all textures, materials and proxies needed to render given object. 
		Returns true once all data is loaded and ready.

		Deprecated function.
		Use bool applyPreloadObject(ControllableObject& co, double distance) instead.
		*/
		VBSFUSION_DPR(UCOB028) static bool preloadObject(ControllableObject& co, double distance);

		/*!
		Removes a message that is attached to object. 

		Deprecated function.
		Use void applyDisplayTextRemoval(ControllableObject& co) instead.
		*/
		VBSFUSION_DPR(UCOB027) static void removeDisplayText(ControllableObject& co);

		/*!
		Set the texture of the given selection.
		The selections must be registered in the "hiddenSelections" entry of the config.cpp, 
		and in the "sections" entry of the model.cfg for the object.

		selection : A string describing the selection name in the model.

		texture :  The texture can be in PAA or JPG format (be aware though, that JPGs are 
		displayed very bright), and must have a side length of 2^n (e.g. 64, 256, etc.). 
		They can reside in either the mission folder, or in an addon.
		
		If the texture is set to "#reset", it will be set to its starting/default texture.
		If the texture is set to "" (empty string), the selection becomes invisible.  	

		Deprecated Function.
		Use applyObjectTexture(ControllableObject& co,string& selection, string& texture) instead.
		*/
		VBSFUSION_DPR(UCOB007) static void setObjectTexture(ControllableObject& object, std::string& selection, std::string& texture);
	
		/*!
		Set the texture of the given selection.
		The selections must be registered in the "hiddenSelections" entry of the config.cpp, 
		and in the "sections" entry of the model.cfg for the object.

		selectionIndex : A number, with the index of the selection in the "hiddenSelections" config entry

		texture :  The texture can be in PAA or JPG format (be aware though, that JPGs are 
		displayed very bright), and must have a side length of 2^n (e.g. 64, 256, etc.). 
		They can reside in either the mission folder, or in an addon.
		
		If the texture is set to "#reset", it will be set to its starting/default texture.
		If the texture is set to "" (empty string), the selection becomes invisible.  	

		Deprecated function.
		Use applyObjectTexture(ControllableObject& object, int selectionIndex, string& texture) instead.
		*/
		VBSFUSION_DPR(UCOB006) static void setObjectTexture(ControllableObject& object, int selectionIndex, std::string& texture);

		/*!
		Set the texture of the given selection on all computers in a network session.
		The selections must be registered in the "hiddenSelections" entry of the config.cpp, 
		and in the "sections" entry of the model.cfg for the object.

		selectionName : A string describing the selection name in the model.

		texture :  Path to texture bitmap or procedural texture
		The texture can be in PAA or JPG format (be aware though, that JPGs are 
		displayed very bright), and must have a side length of 2^n (e.g. 64, 256, etc.). 
		They can reside in either the mission folder, or in an addon. 
		
		If the texture is set to "#reset", it will be set to its starting/default texture.
		If the texture is set to "" (empty string), the selection becomes invisible.  	

		Deprecated Function.
		Use  void applyObjectTextureGlobal(ControllableObject& co, string& selectionName, string& texture) instead.
		*/
		VBSFUSION_DPR(UCOB009) static void setObjectTextureGlobal(ControllableObject& object, std::string& selectionName, std::string& texture);

		/*!
		Set the texture of the given selection on all computers in a network session.
		The selections must be registered in the "hiddenSelections" entry of the config.cpp, 
		and in the "sections" entry of the model.cfg for the object.

		selectionIndex : A number, with the index of the selection in the "hiddenSelections" config entry

		texture :  Path to texture bitmap or procedural texture
		The texture can be in PAA or JPG format (be aware though, that JPGs are 
		displayed very bright), and must have a side length of 2^n (e.g. 64, 256, etc.). 
		They can reside in either the mission folder, or in an addon. 
		
		If the texture is set to "#reset", it will be set to its starting/default texture.
		If the texture is set to "" (empty string), the selection becomes invisible.  	

		Deprecated Function.
		Use  void applyObjectTextureGlobal(ControllableObject& co, int selectionIndex, string& texture) instead.
		*/
		VBSFUSION_DPR(UCOB008) static void setObjectTextureGlobal(ControllableObject& object, int selectionIndex, std::string& texture);

		/*!
		@description

		Gets the minimum extreme points of a bounding box of an object created within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v3.04]

		@param co - The object for which the minimum extreme points is obtained. The object should be created before passing it as a parameter

		@return vector3D

		@example

		@code

		displayString+=" The object�s bounding box�s minimum extreme points is �+ControllableObjectUtilities::getBoundingBoxMinExtremePoints(co).getVBSPosition();

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To obtain the minimum extreme points of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getBoundingBoxMinExtremePoints (ControllableObject& co).
		*/
		static vector3D getBoundingBoxMinExtremePoints(const ControllableObject& co);

		/*!
		@description

		Gets the maximum extreme points of a bounding box of an object created within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v3.04]

		@param co - The object for which the maximum extreme points is obtained. The object should be created before passing it as a parameter

		@return vector3D

		@example

		@code

		displayString+=" The object�s bounding box�s maximum extreme points is �+ControllableObjectUtilities::getBoundingBoxMaxExtremePoints(co).getVBSPosition();

		@endcode

		@overloaded

		None 

		@related

		None 

		@remarks To obtain the maximum extreme points of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getBoundingBoxMaxExtremePoints (ControllableObject& co).
		*/
		static vector3D getBoundingBoxMaxExtremePoints(const ControllableObject& co);

		/*!
		@description
		Used to apply a mass to the object created within the VBS2 environment.

		@locality 

		Local

		@version [VBS2Fusion v3.05]

		@param co - The object for which the mass is applied to. The object should be created before passing it as a parameter. 

		@param mass � The amount of mass(in KG) applied to the object. 

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyMass(co,1000 );	

		@endcode

		@overloaded

		ControllableObjectUtilities::applyMass(ControllableObject& co, double mass, double timeDelay );

		@related

		ControllableObjectUtilities::getMass(ControllableObject& co )

		*/
		static void applyMass(ControllableObject& co, double mass );

		/*!
		@description

		Used to apply a mass to the object created within the VBS2 environment.

		@locality 

		Local

		@version [VBS2Fusion v3.05]

		@param co - The object for which the mass(in KG) is applied to. The object should be created before passing it as a parameter. 

		@param mass � The amount of mass applied to the object. 

		@param timeDelay - Seconds taken to change the mass to the new value

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyMass(co,1000,10 );

		@endcode

		@overloaded

		ControllableObjectUtilities::applyMass(ControllableObject& co, double mass)

		@related

		ControllableObjectUtilities::getMass(ControllableObject& co )

		*/
		static void applyMass(ControllableObject& co, double mass, double timeDelay );

		/*!
		@description

		Gets the total mass of a physX object created within the VBS2 environment.

		@locality 

		Global

		@version [VBS2Fusion v2.66]

		@param co - The object for which the mass(weight in KG) is obtained. The object should be created before passing it as a parameter.   

		@return double	

		@example

		@code

		displayString+=" The mass of is "+conversions::DoubleToString(ControllableObjectUtilities::getMass(co));

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::applyMass(ControllableObject& co, double mass)
		ControllableObjectUtilities::applyMass(ControllableObject& co, double mass, double timeDelay );

		@remarks To obtain the mass of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getMass(ControllableObject& co).
		*/
		static double getMass(const ControllableObject& co );

		/*!
		@description

		Used to set the center of mass for a physX object created within the VBS2 environment.

		@locality 

		Local

		@version [VBS2Fusion v3.05]

		@param co - The object for which the centre of mass is applied to. The object should be created before passing it as a parameter.

		@param center � The center of  mass in model space which is relative to origin of object.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyCenterOfMass(co,position3D(10,10,0));	

		@endcode

		@overloaded

		ControllableObjectUtilities::applyCenterOfMass(ControllableObject& co, position3D center, double timeDelay); 

		@related

		ControllableObjectUtilities::getCenterOfMass(ControllableObject& co)

		*/
		static void applyCenterOfMass(ControllableObject& co, const position3D& center);

		/*!
		@description

		Used to set the center of mass for a physX  object created within the VBS2 environment.

		@locality 

		Local

		@version [VBS2Fusion v3.05]

		@param co - The object for which the path is obtained. The object should be created before passing it as a parameter.   

		@param center � The center of  mass in model space

		@param timeDelay - Seconds taken to change the center to the new value

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyCenterOfMass(co,position3D(10,10,0),10);	

		@endcode

		@overloaded

		ControllableObjectUtilities:: applyCenterOfMass(ControllableObject& co, position3D center)

		@related

		ControllableObjectUtilities::getCenterOfMass(ControllableObject& co)

		*/
		static void applyCenterOfMass(ControllableObject& co, const position3D& center, double timeDelay);

		/*!
		@description

		Gets the center of mass of a physX object created within the VBS2 environment 

		@locality 

		Global

		@version [VBS2Fusion v2.66]

		@param co - The object for which the center of mass is obtained. The object should be created before passing it as a parameter

		@return position3D

		@example

		@code

		displayString+=" The mass of co is "+(ControllableObjectUtilities::getCenterOfMass(co).getVBSPosition()); 

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::applyCenterOfMass(ControllableObject& co, position3D center)
		ControllableObjectUtilities::applyCenterOfMass(ControllableObject& co, position3D center, double timeDelay); 

		@remarks To obtain the center of mass of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: getCenterOfMass(ControllableObject& co).	
		*/
		static position3D getCenterOfMass(ControllableObject& co);

		/*!
		@description

		Used to check if a particular animation exists for the given object within the VBS2 environment. The function only checks for custom animations and not engine generated animation sources.

		@locality 

		Global

		@version [VBS2Fusion v2.66]

		@param co � The object which is checked. The object should be created before passing it as a parameter.   

		@param animation � The animation applied to the object. 

		@return bool

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString+="Animation exists"+conversions::BoolToString(ControllableObjectUtilities::isAnimationExists(co,"mainGun"));

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::getAnimationBone(ControllableObject& co, string animation);
		ControllableObjectUtilities::getAnimationPhase(ControllableObject &co, string animationName);

		@remarks To check whether an object created in a client machine within a network has a particular animation, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isAnimationExists(ControllableObject& co, string animation).
		*/
		static bool isAnimationExists(ControllableObject &co, const std::string& animation);

		/*!
		@description

		Gets the animation phase of an object created within the VBS2 environment. The return value would be between 0 (start point of animation) and 1 (endpoint of animation).

		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co � The object for which the animation phase is obtained. The object should be created before passing it as a parameter.

		@param animationName - The animation applied to the object. It is the class name of the animation defined in config.cpp

		@return Double

		@example

		@code

		displayString+="Animation phase"+conversions::DoubleToString(ControllableObjectUtilities::getAnimationPhase(co,"mainGun"));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the animation phase  of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getAnimationPhase(ControllableObject& co, string animationName).
		*/
		static double getAnimationPhase(ControllableObject &co, const std::string& animationName);

		/*!
		@description

		Gets the animation bone of an object created within the VBS2 environment.

		@locality 

		Local

		@version [VBS2Fusion v2.66]

		@param co � The object for which the animation bone is obtained. The object should be created before passing it as a parameter 

		@param animation - The animation applied to the object.
		
		@return string

		@example

		@code

		displayString+="Animation bone"+ControllableObjectUtilities::getAnimationBone(co,"mainGun");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the animation bone of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getAnimationBone(ControllableObject& co, string animation).
		*/
		static std::string getAnimationBone(ControllableObject& co, const std::string& animation);

		/*!
		 Set object's orientation (given as direction and up vector).
		 Since version +1.30 accepts additional parameter which, when object is attached, 
		 specifies if vectors applied are relative to parent object's orientation.  

		 Deprecated Function.
		 Use function  void applyVectorDirAndUp(ControllableObject& co,vector3D vectorDir, vector3D vectorUp, bool relative) instead.
		*/
		VBSFUSION_DPR(UCOB011) static void setVectorDirAndUp(ControllableObject& co,vector3D vectorDir, vector3D vectorUp, bool relative);

		/*!
		@description
		
		Used to check if an object exists within the VBS2 Environment dead or alive and return true.( If the object is not created or is deleted  the function would return false)
		
		@locality 

		Global

		@version [VBS2Fusion v3.06]

		@param co - The object which is checked. The object should be created before passing it as a parameter.   	
		
		@return boolean

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString ="is Available " conversions::BoolToString(ControllableObjectUtilities::isAvailable(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To check the isAvailable property of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isAvailable(ControllableObject& co).

		@remarks This is a replication of bool ControllableObjectExists(ControllableObject& co) function.
		*/
		static bool isAvailable(const ControllableObject& co);

		/*!
		@description 

		Used to control the object created within the VBS2 environment externally. 
		- If the object is the player or a vehicle controlled by the player, user will loose the control (Ex: keyboard and mouse) over the object.
		- If not, the AI behavior of the object will be disabled.
		By setting to false, user and AI control will be revoked.
		
		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co - The object for which the mode is applied to. The object should be created before passing it as a parameter.

		@param control � The type of control applied to the object, if control is set to true, object can be controlled externally (i.e. If the object is the player or a vehicle controlled by the player, user will loose the control (Ex: keyboard and mouse) over the object, or if the object is not controlled by the player I, the AI behavior of the object will be disabled.

		@return nothing

		@example

		@code

		ControllableObjectUtilities::applyExternalControl(co, true);	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void setExternalControl(ControllableObject& co, bool control) function.
		*/
		static void applyExternalControl(const ControllableObject& co, bool control);

		/*!
		@description

		Used to simulate a user input via Fusion to the object created within the VBS2 environment. (Refer remarks for further information)

		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param keyStroke - The type of key action you want to simulate.  Refer appendix in the VBS2Fusion Product Manual.

		@param amount � The value to simulate the user input, values should be between 0 and 1 for digital actions and between -1 and 1 for analog actions.

		@return nothing

		@example

		@code

		string action = "moveforward";
		ControllableObjectUtilities::applyKeyAction(action,1);

		@endcode

		@overloaded

		ControllableObjectUtilities:: applyKeyAction(ControllableObject& co,string keyStroke, double amount)

		@related

		None

		@remarks By default this function applies to the player object, unless the other objects created within the vbs2 environment are externally controlled. Refer Refer ControllableObjectUtilities::applyExternalControl(ControllableObject& co, bool control)

		@remarks This is a replication of void setKeyAction(ControllableObject& co, string keyStroke, float amount) and void setKeyAction(ControllableObject& co, string keyStroke, double amount)functions.
		*/
		static void applyKeyAction(const std::string& keyStroke, double amount);

		/*!
		@description

		Used to apply an action to the specified object created within the VBS2 environment. (Refer remarks for further information)

		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co � The object for which the action is applied to. The object should be created before passing it as a parameter.

		@param keyStroke - The type of key action you want to simulate.  Refer appendix in the VBS2Fusion Product Manual.

		@param amount � The value to simulate the user input, values should be between 0 and 1 for digital actions and between -1 and 1 for analog actions.

		@return nothing

		@example

		@code

		ControllableObjectUtilities::applyExternalControl(co, true);
		string action = "moveforward";
		ControllableObjectUtilities::applyKeyAction(action,1);	

		@endcode

		@overloaded

		ControllableObjectUtilities::applyKeyActionl(string keyStroke , double amount)

		@related

		ControllableObjectUtilities::applyExternalControl (string keyStroke, double amount)

		@remarks To apply the action to a specific object, ensure that the external control property is set true to the object.Refer ControllableObjectUtilities:: applyExternalControl(ControllableObject& co, bool control)
		@remarks This is a replication of void setUnitAction(ControllableObject& co, string keyStroke, float amount) and void setUnitAction(ControllableObject& co, string keyStroke, double amount)functions.
		*/
		static void applyKeyAction(ControllableObject& co, const std::string& keyStroke, double amount);

		/*!
		@description

		Used to limit the speed of an object within the VBS2 Environment.

		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co � The object for which the speed limit is applied 

		@param speedLimit � The speed limit applied to the object in km/h.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applySpeedLimit(co,5);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks The function has only a temporary effect (for e.g. if the speed is limited to 5 as shown above, the speed limit applied will not sustain for a long period and  would gradually gain speed over time,thus to prevent this or to see a noticeable effect, its recommended  to apply the function continuously in the onsimulation loop. 
		*/
		static void applySpeedLimit(ControllableObject& co , double speedLimit);

		/*!
		@description
		
		Used to apply a texture of the given selection to an object within the VBS2 Environment. 

		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co - The object for which the texture is applied. The object should be created before passing it as a parameter. 

		@param selectionIndex � A number, with the index of the selection in the "hiddenSelections" config entry.

		@param texture � The texture can be in PAA or JPG format (be aware though, that JPGs are displayed very bright), and must have a side length of 2^n (e.g. 64, 256, etc.). They can reside in either the mission folder, or in an addon. 

		@return Nothing

		@example

		@code

		string wtexturevehicle ="\\vbs2\\vehicles\\Land\\Tracked\\generaldynamics_m1\\us_m1a1\\data
		usmc_m1a1_ext_des_co";
		ControllableObjectUtilities::applyObjectTexture(co,0,wtexturevehicle);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyObjectTexture(ControllableObject& object, string& selection, string& texture)

		@related

		ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, string& selectionName, string& texture);
		ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, int selectionIndex, string& texture);

		@remarks To apply the object texture for an object created in a client machine within a network, use the function ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, string& selectionName, string& texture) or ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, int selectionIndex, string& texture);

		@remarks This is a replication of void ControllableObjectUtilities::setObjectTexture(ControllableObject& object, int selectionIndex, string& texture) function

		@remarks If the texture is set to "#reset", it will be set to its starting/default texture. If the texture is set to "" (empty string), the selection becomes invisible. To see the effect applied texture should be compatible with relevant object.

		*/
		static void applyObjectTexture(ControllableObject& co, int selectionIndex, const std::string& texture);

		/*!
		@description

		Used to apply a texture of the given selection to an object within the VBS2 Environment. 

		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param object - The object for which the texture is applied. The object should be created before passing it as a parameter. 

		@param selection � A string describing the selection name in the model.

		@param texture � The texture can be in PAA or JPG format (be aware though, that JPGs are displayed very bright), and must have a side length of 2^n (e.g. 64, 256, etc.). They can reside in either the mission folder, or in an addon. 

		@return Nothing

		@example

		@code

		string selectionvehicle = "swap_ext";
		string wtexturevehicle ="\\vbs2\\vehicles\\Land\\Tracked\\generaldynamics_m1\\us_m1a1\\data
		usmc_m1a1_ext_des_co";
		ControllableObjectUtilities::applyObjectTexture(co,selectionvehicle,wtexturevehicle);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyObjectTexture(ControllableObject& object,int& selection, string& texture)

		@related

		ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, string& selectionName, string& texture);
		ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, int selectionIndex, string& texture);

		@remarks To apply the object texture for an object created in a client machine within a network, use the function ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, string& selectionName, string& texture) or ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, int selectionIndex, string& texture);

		@remarks This is a replication of void ControllableObjectUtilities::setObjectTexture(ControllableObject& co, string& selection, string& texture) function

		@remarks The selections must be registered in the "hiddenSelections" entry of the config.cpp, and in the "sections" entry of the model.cfg for the object.

		@remarks If the texture is set to "#reset", it will be set to its starting/default texture. If the texture is set to "" (empty string), the selection becomes invisible. To see the effect applied texture should be compatible with relevant object.
		*/
		static void applyObjectTexture(ControllableObject& object, const std::string& selection, const std::string& texture);

		/*!
		@description

		Used to apply the texture of the given selection on all computers in a network session within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.06]

		@param co - The object for which the texture is applied. The object should be created before passing it as a parameter. 

		@param selectionIndex � A number, with the index of the selection in the "hiddenSelections" config entry.

		@param texture � The texture can be in PAA or JPG format (be aware though, that JPGs are displayed very bright), and must have a side length of 2^n (e.g. 64, 256, etc.). They can reside in either the mission folder, or in an addon. 

		@return Nothing

		@example

		@code

		string wtexturevehicle ="\\vbs2\\vehicles\\Land\\Tracked\\generaldynamics_m1\\us_m1a1\\data
		usmc_m1a1_ext_des_co";
		ControllableObjectUtilities::applyObjectTextureGlobal(co,0,wtexturevehicle);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, string& selection, string& texture)

		@related

		ControllableObjectUtilities::applyObjectTexture (ControllableObject& object, string& selection, string& texture);
		ControllableObjectUtilities::applyObjectTexture (ControllableObject& object, int selectionIndex, string& texture);

		@remarks To apply the object texture for an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, int selectionIndex, string& texture);

		@remarks This is a replication of void setObjectTexture(ControllableObject& object, int selectionIndex, string& texture) function

		@remarks The selections must be registered in the "hiddenSelections" entry of the config.cpp, and in the "sections" entry of the model.cfg for the object.

		@remarks If the texture is set to "#reset", it will be set to its starting/default texture. If the texture is set to "" (empty string), the selection becomes invisible. To see the effect applied texture should be compatible with relevant object.
		*/
		static void applyObjectTextureGlobal(ControllableObject& co, int selectionIndex, const std::string& texture);

		
		/*!
		@description

		Used to apply the texture of the given selection on all computers in a network session within the VBS2 Environment.

		@locality 

		Global

		@version [VBS2Fusion v3.06]

		@param object - The object for which the texture is applied. The object should be created before passing it as a parameter. 

		@param selectionName � A string describing the selection name in the model

		@param texture � The texture can be in PAA or JPG format (be aware though, that JPGs are displayed very bright), and must have a side length of 2^n (e.g. 64, 256, etc.). They can reside in either the mission folder, or in an addon. 

		@return Nothing

		@example

		@code

		string selectionvehicle = "swap_ext";
		string wtexturevehicle ="\\vbs2\\vehicles\\Land\\Tracked\\generaldynamics_m1\\us_m1a1\\data
		usmc_m1a1_ext_des_co";
		ControllableObjectUtilities::applyObjectTextureGlobal(co,selectionvehicle,wtexturevehicle);

		@endcode

		@overloaded

		ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object,int selectionIndex, string& texture)

		@related

		ControllableObjectUtilities::applyObjectTexture(ControllableObject& object, string& selection, string& texture);
		ControllableObjectUtilities::applyObjectTexture(ControllableObject& object, int selectionIndex, string& texture);

		@remarks To apply the object texture for an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::applyObjectTextureGlobal(ControllableObject& object, string selectionName, string& texture);

		@remarks This is a replication of void ControllableObjectUtilities::setObjectTexture(ControllableObject& object, string& selectionName, string& texture) function

		@remarks The selections must be registered in the "hiddenSelections" entry of the config.cpp, and in the "sections" entry of the model.cfg for the object.

		@remarks If the texture is set to "#reset", it will be set to its starting/default texture. If the texture is set to "" (empty string), the selection becomes invisible. To see the effect applied texture should be compatible with relevant object.
		*/
		static void applyObjectTextureGlobal(ControllableObject& co, const std::string& selectionName, const std::string& texture);

		/*!
		@description

		Used to move the object to a random position within the VBS2 Environment. The position applied can either be the position that is defined in the first parameter or the positions from the markers vector. The position is placed inside a circle with this position as its center and placement as its radius.
		
		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co - The object for which the position is applied. The object should be created before passing it as a parameter.

		@param pos - Position3D object at which you may want to place the object

		@param markers � List of markers to select a random position.

		@param placement � The radius of the circle in which the object is placed.

		@return Nothing

		@example

		@code

		//declare the markers to be assigned 
		Marker m1, m2, m3;
		vector<Marker> vectorMarker;
		//asigning positions to the markers
		position3D pos = player.getPosition()+position3D(40,40,0);
		m1.setPosition(player.getPosition()+position3D(50,50,0));
		MarkerUtilities::createMarker(m1);
		MarkerUtilities::updateMarker(m1);
		m2.setPosition(player.getPosition()+position3D(60,60,0));
		MarkerUtilities::createMarker(m2);
		MarkerUtilities::updateMarker(m2);
		m3.setPosition(player.getPosition()+position3D(70,70,0));
		MarkerUtilities::createMarker(m3);
		MarkerUtilities::updateMarker(m3);
		vectorMarker.push_back(m1);
		vectorMarker.push_back(m2);
		vectorMarker.push_back(m3);
		//applying the position to the object
		ControllableObjectUtilities::applyRandomPosition(co,player.getPosition(),vectorMarker,0);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void ControllableObjectUtilities::setRandomPosition(ControllableObject& co, position3D pos, vector<Marker> markers, double placement) function.

		@remarks This will place the object at either position which is specified by pos variable, or one of the markers positions from the vector.
		 */
		static void applyRandomPosition(ControllableObject& co, const position3D& pos, const std::vector<Marker>& markers, double placement);

		/*!
		@description

		Used to orient the object within the VBS2 Environment.

		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co � The object for which the orientation is applied. The object should be created before passing it as a parameter.

		@param vectorDir � The direction vector applied to the object.

		@param vectorUp � The up vector applied to the object.

		@param relative � The parameter specifying whether vectors are relative to parent object�s orientation (only occurs when the object is attached to another object).

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyVectorDirAndUp(co,vector3D(0,0,1),vector3D(0,1,0),true); 

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void ControllableObjectUtilities::setVectorDirAndUp(ControllableObject& co,vector3D vectorDir, vector3D vectorUp, bool relative) function
		*/
		static void applyVectorDirAndUp(ControllableObject& co, const vector3D& vectorDir, const vector3D& vectorUp, bool relative);

		/*!
		@description

		Used to move a person to a given position within the VBS2 environment. The speed of the movement can be changed using void ControllableObjectUtilities::applySpeed(ControllableObject& co, double speed) function.

		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co � The object for which is moved.

		@param pos - position3D object to specify the destination position.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyCommandMove(co, player.getPosition()+position3D(15,15,0));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void ControllableObjectUtilities::moveTo(ControllableObject& co, position3D pos) function 
		*/
		static void applyCommandMove(ControllableObject& co, const position3D& pos);

		/*!
		@description

		Returns a world position(above ground level), relative to an object's origin, taking into account its direction.
		
		@locality 

		Global

		@version [VBS2Fusion v3.05]

		@param co � The object used as a reference. The object should be created before passing it as a parameter.   

		@param pos � The position for which the world position is obtained. If this position is [0,0,0], the object passed as a parameter (i.e. co) will not be considered as a reference  and its  absolute position is returned ( including any elevations e.g. when the object is  positioned on top of another object) 

		@return position3D

		@example

		@code

		//This code would return the absolute position of co
		displayString+="Model to world position is"+ControllableObjectUtilities::getModelToWorld(co,position3D(0,0,0)).getVBSPosition();
		//would return the position of a point 1m to the right from co�s position and the direction it is facing
		displayString+="Model to world position is"+ControllableObjectUtilities::getModelToWorld(co,position3D(1,0,0)).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the world position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getModelToWorld(ControllableObject& co, position3D pos).

		@remarks This is a replication of position3D ControllableObjectUtilities::modelToWorld(ControllableObject& co, position3D& pos) function 

		@remarks An offset of position3D(0,0,0) will return the current, absolute position, including any elevations (e.g. when positioned on top of another object).
		*/
		static position3D getModelToWorld(const ControllableObject& co, const position3D& pos);

		/*!
		@description

		Returns an object�s offset from the specified world position, taking into account the object�s origin and direction.
		
		@locality 

		Global

		@version [VBS2Fusion v3.06]

		@param co � The object used as a reference. The object should be created before passing it as a parameter.   

		@param pos - The position for which the offset is obtained. (Refer remarks for further information on Offset)

		@return position3D

		@example

		@code

		//This code would return [-1234,-5678,0] if the object co is positioned at [1234,5678,0]
		displayString+="World to model offset is"+ControllableObjectUtilities::getWorldlToModel (co,position3D(0,0,0)).getVBSPosition();
		//This code would return the displacement between the objects co and co1   
		displayString+="World to model offset is"+ControllableObjectUtilities::getWorldToModel(co,ControllableObjectUtilities::getPosition(co1)).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of position3D worldToModel(ControllableObject& co, position3D& pos) function 

		@remarks Offset is the displacement between 2 entities and it is returned by the type position3D (i.e. X, Z, Y coordinates)

		@remarks As the function specifies the terrain at a location, the z value is likely to change if the terrain is covered in water. 

		@remarks To obtain the model position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getWorldToModel(ControllableObject& co, position3D pos).
		*/
		static position3D getWorldToModel(const ControllableObject& co, const position3D& pos);

		/*!
		@description

		Returns a world position (above sea level), relative to an object's origin, and taking into account its direction.

		@locality 

		Global

		@version [VBS2Fusion v3.06]

		@param co � The object used as a reference. The object should be created before passing it as a parameter.   

		@param pos � The position for which the world position is obtained. If this position is [0,0,0], the object passed as a parameter (i.e. co) will not be considered as a reference  and its  absolute position above sea level  is returned ( including any elevations e.g. when the object is  positioned on top of another object) 

		@return position 3D

		@example

		@code

		//This code would return the absolute position of co above sea level
		displayString+="Model to world position is"+ControllableObjectUtilities::getModelToWorldASL(co,position3D(0,0,0)).getVBSPosition();
		//would return the ASL position of a point 1m to the right from co�s position and the direction it is facing
		displayString+="Model to world position ASL is"+ControllableObjectUtilities::getModelToWorldASL(co,position3D(1,0,0)).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To obtain the world position ASL  of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getModelToWorldASL(ControllableObject& co, position3D pos).

		@remarks This is a replication of position3D ControllableObjectUtilities::modelToWorldASL(ControllableObject& co, position3D& pos) function
		*/
		static position3D getModelToWorldASL(const ControllableObject& co, const position3D& pos);

		/*!
		@description

		Returns an object�s offset from the specified world position, taking into account the object�s origin and direction.

		@locality 

		Global

		@version [VBS2Fusion v3.06]

		@param co � The object used as a reference. The object should be created before passing it as a parameter.   

		@param pos - The position for which the offset is obtained. (Refer Additional Notes for further information on Offset)

		@return position3D

		@example

		@code

		//This code would return [-1234,-5678,0] if the object co�s position ASL is [1234,5678,0]
		displayString+="World to modelASL offset is"+ControllableObjectUtilities::getWorldlToModel (co,position3D(0,0,0)).getVBSPosition();
		//This code would return the dispalcement between the objects co and co1 
		displayString+="World to modelASL offset is"+ControllableObjectUtilities::getWorldToModel(co,ControllableObjectUtilities::getPosition(co1)).getVBSPosition();

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Offset is the displacement between 2 entities and it is returned by the type position3D (i.e. X, Z, Y coordinates)

		@remarks This is a replication of position3D worldToModelASL(ControllableObject& co, position3D& pos) function 

		@remarks As the function specifies the terrain at a location, the z value is likely to change if the terrain is covered in water.

		@remarks To obtain the model ASL position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getWorldToModelASL(ControllableObject& co, position3D pos).
		*/
		static position3D getWorldToModelASL(const ControllableObject& co, const position3D& pos);

		/*!
		@description

		Used to attach an object to another object within the VBS2 Environment.Already attached objects can be re-attached to a new object, without having to be detached first. And this can be re-applied to an already attached item (with new offsets), without having to detach it first. Also note that you may want to adjust the attached objects orientation after issuing the attachTo command, using void applyVectorDirAndUp(ControllableObject& object,vector3D vectorDir, vector3D vectorUp, bool relative) function.

		@locality 

		Global

		@version [VBS2Fusion v3.06]

		@param co - The object which is attached to the target object. The object should be created before passing it as a parameter.

		@param tObject � The object for which the other object is attached to.

		@param offSet � The position where the object should be attached to the target object  

		@param memoryPoint � It specifies the location where the attaching should happen (name of the memory point in the model  -Refer appendix in the VBS2Fusion Product Manual).This is optional (can be a null string).

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyObjectAttach(co,co1,position3D(0,2,0),string(""))

		@endcode

		@overloaded

		ControllableObjectUtilities::applyObjectAttach(ControllableObject& co, ControllableObject& targetCo, position3D offSet, string memoryPoint, int rope, string bone, bool collisions = false);
		ControllableObjectUtilities::applyObjectAttach(ControllableObject& co, ControllableObject& tObject, position3D offSet);

		@related

		None

		@remarks This is a replication of void ControllableObjectUtilities::attachTo(ControllableObject& object, ControllableObject& tObject, position3D offSet, string memoryPoint) function 

		@remarks To attach an object created in a client machine within a network to another object, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyObjectAttach(ControllableObject& object, ControllableObject& tObject, position3D offSet, string memoryPoint)
		*/
		static void applyObjectAttach(ControllableObject& co, ControllableObject& tObject, const position3D& offSet, const std::string& memoryPoint);

		/*!
		@description

		Used to attach an object to another object within the VBS2 Environment. 
		Already attached objects can be re-attached to a new object, without having to be detached first. And this can be re-applied to an already attached item (with new offsets), without having to detach it first. Also note that you may want to adjust the attached objects orientation after issuing the attachTo command, using void applyVectorDirAndUp(ControllableObject& co,vector3D vectorDir, vector3D vectorUp, bool relative) function.

		@locality 

		Global

		@version [VBS2Fusion v3.06]

		@param co - The object which is attached to the target object. The object should be created before passing it as a parameter.

		@param targetCo � The object for which the other object is attached to.

		@param offSet � The position where the object should be attached to the target object  

		@param memoryPoint � It specifies the location where the attaching should happen (name of the memory point in the model  -Refer appendix in the VBS2Fusion Product Manual).

		@param rope � If the value is 0 or false, the attachment will be rigid (i.e. �object� will be fixed in position relative to �tobject� .If the value is 
		1 or true, the attachment will be done by a flexible rope, allowing �object� to swing about.If the value is 2, the object will be attached by a stiff link (like a tow bar, with pivots on each end).If the value is 3, the object will be attached by a horizontal rope (to simulate towing). 
					  If the model has a memory point "attachRope" then the rope will be attached to this point. 

		@param bone � Name of bone to be attached to on target object. The referenced bone must be of LOD "Memory", "Geometry" or "Fire", etc.

		@param collisions � Whether collisions for attached object should be enabled (default value is false)  

		@return nothing

		@example

		@code

		ControllableObjectUtilities::applyObjectAttach(co1,co,position3D(0,2,3),string("memorypoint"),1,"Fire",true) 

		@endcode

		@overloaded

		ControllableObjectUtilities::applyObjectAttach(ControllableObject& object, ControllableObject& tObject, position3D offSet, string memoryPoint);
		ControllableObjectUtilities::applyObjectAttach(ControllableObject& object, ControllableObject& tObject, position3D offSet);

		@related

		None

		@remarks To attach an object created in a client machine within a network to another object, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyObjectAttach(ControllableObject& object, ControllableObject& targetCo, position3D offSet, string memoryPoint, int rope, string bone, bool collisions = false);

		@remarks This is a replication of void ControllableObjectUtilities::attachTo(ControllableObject& object, ControllableObject& tObject, position3D offSet, string memoryPoint, int rope, string bone, bool collisions = false) function
		*/
		static void applyObjectAttach(ControllableObject& co, ControllableObject& targetCo, const position3D& offSet, const std::string& memoryPoint, int rope, const std::string& bone, bool collisions = false);

		/*!
		@description

		Used to attach an object to another object within the VBS2 Environment.Already attached objects can be re-attached to a new object, without having to be detached first. And this can be re-applied to an already attached item (with new offsets), without having to detach it first. Also note that you may want to adjust the attached objects orientation after issuing the attachTo command,using void ControllableObjectUtilities::applyVectorDirAndUp(ControllableObject& object,vector3D vectorDir, vector3D vectorUp, bool relative) function.

		@locality 

		Global

		@version [VBS2Fusion v3.06]

		@param co - The object which is attached to the target object. The object should be created before passing it as a parameter.

		@param tObject � The object for which the other object is attached to.

		@param offSet � The position where the object should be attached to the target object  

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyObjectAttach(co,co1,position3D(3,1,3));

		@endcode

		@overloaded

		ControllableObjectUtilities::applyObjectAttach(ControllableObject& object, ControllableObject& targetCo, position3D offSet, string memoryPoint, int rope, string bone, bool collisions = false);
		ControllableObjectUtilities::applyObjectAttach(ControllableObject& object, ControllableObject& tObject, position3D offSet, string memoryPoint);

		@related

		None

		@remarks To attach an object created in a client machine within a network to another object, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::applyObjectAttach(ControllableObject& object, ControllableObject& tObject, position3D offSet);

		@remarks This is a replication of void ControllableObjectUtilities::attachTo(ControllableObject& object, ControllableObject& tObject, position3D offSet) function
		*/
		static void applyObjectAttach(ControllableObject& co, ControllableObject& tObject, const position3D& offSet);

		/*!
		@description
		
		Used to detach the given object if it is attached to some object within the VBS2 Environment.	
		
		@locality 

		Global

		@version [VBS2Fusion v3.06]

		@param co � The object to be detached. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyObjectDetach(co);	

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To detach an object created in a client machine within a network from  another object, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::applyObjectDetach(ControllableObject& co).

		@remarks This is a replication of void ControllableObjectUtilities::detach(ControllableObject &object) function
		*/
		static void applyObjectDetach(ControllableObject &co);

		/*!
		@description

		Used to disable an AI feature of an object within the VBS2 Environment

		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co � The object for which the AI is disabled. The object should be created before passing it as a parameter.

		@param behaviour � The type of behavior disabled from the AI features. There are 8 types, TARGET - stop the unit to watch the assigned target, AUTOTARGET - prevent the unit from assigning a target independently and watching unknown objects, MOVE - disable the AI's movement, ANIM - disable ability of AI to change animation, COLLISIONAVOID - disables collision avoidance for vehicles. Default is on, GROUNDAVOID - disables ground avoidance for aircraft. Default is on, PATHPLAN - disable the AI's path planning, THREAT_PATH - disable threat pathplanning (avoiding threats)

		@return Nothing

		@example

		@code

		//create the object using its respective classes for effective results for example the object(u) below is created via UnitUtilities::createunit(Unit u) function
		ControllableObjectUtilities::applyAIBehaviourDisable(u,AIBEHAVIOUR::AIMOVE);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void ControllableObjectUtilities::disableAIbehaviour(ControllableObject& co, AIBEHAVIOUR behaviour) function

		@remarks Disabling path planning on persons will also disable collision-detection (i.e. persons will walk through walls to reach a waypoint). This does not apply to vehicles.

		@remarks All effects of this function is canceled after mission save or load.

		@remarks In multi-player mode, this need to be executed on the server or client where target object 'co' is local. 			
		*/
		static void applyAIBehaviourDisable(ControllableObject& co, AIBEHAVIOUR behaviour);

		/*!
		@description

		Used to enable an AI feature of an object within the VBS2 Environment

		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co � The object for which the AI is enabled. The object should be created before passing it as a parameter.

		@param behaviour � The type of behavior enabled from the AI features. There are 8 types, TARGET - allows the unit to watch the assigned target, AUTOTARGET - allows the unit to assign a target independently and watch unknown objects, MOVE - enables the AI's movement, ANIM - enables ability of AI to change animation, COLLISIONAVOID - enables collision avoidance for vehicles. Default is on, GROUNDAVOID - enables ground avoidance for aircraft. Default is on, PATHPLAN - enables the AI's path planning, THREAT_PATH - enable threat pathplanning (avoiding threats)

		@return Nothing

		@example

		@code

		//create the object using its respective classes for effective results for example the object(u) below is created via UnitUtilities::createunit(Unit u) 
		ControllableObjectUtilities::applyAIBehaviourEnable(u,AIBEHAVIOUR::AIMOVE);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void ControllableObjectUtilities::enableAIbehaviour(ControllableObject& co, AIBEHAVIOUR behaviour) function

		@remarks In multi-player mode, this need to be executed on the server or client where target object 'co' is local. 			
		*/
		static void applyAIBehaviourEnable(ControllableObject& co, AIBEHAVIOUR behaviour);

		/*!
		@description

		Used to allow or prevent an object being damaged (injured or killed) within the VBS2 environment. 
					
		@locality 

		Local

		@version [VBS2Fusion  v3.06]

		@param co � The object that is prevented from being damaged.

		@param allow � If the value is true the object is vulnerable and is prone to damage or otherwise.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyDamageEnable(co,false);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To prevent an object created in a client machine within a network being damaged, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyDamageEnable(ControllableObject& co , bool allow)

		@remarks This is a replication of void ControllableObjectUtilities::allowDamage(ControllableObject& co , bool allow) function
		*/
		static void applyDamageEnable(ControllableObject& co , bool allow);

		/*!
		@description

		Used to move the object at a particular speed within the VBS2 environment.
		
		@locality 

		Local

		@version [VBS2Fusion  v3.06]

		@param co � The object for which the speed limit is applied 

		@param speed � The speed limit applied to the object

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applySpeed(co,5);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Due to design and environmental factors the object would not achieve the exact speed passed by the parameter, however the object will not move faster than the speed passed as a parameter nor exceed the speed limit exerted by the function ControllableObjectUtilities::applySpeedLimit(ControllableObject& co , double speedLimit)

		@remarks This is a replication of void ControllableObjectUtilities::forceSpeed(ControllableObject& co, double forceSpeedVal) function 
		*/
		static void applySpeed(ControllableObject& co, double speed);

		/*!
		@description

		Used to check whether an object's movement is completed within the VBS2 Environment(related with low level movement- If the movement is completed the function will return true or otherwise)
		
		@locality 

		Global

		@version [VBS2Fusion  v3.06]

		@param co - The object for which the movement is checked. The object should be created before passing it as a parameter.

		@return bool

		@example

		@code

		displayString ="is Move Completed " conversions::BoolToString(ControllableObjectUtilities::isCommandMoveCompleted(co));

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::applyCommandMove(ControllableObject& co, position3D pos)

		@remarks This is a replication of bool isMoveToCompleted(ControllableObject& co) function

		@remarks Related with void ControllableObjectUtilities::applyCommandMove(ControllableObject& co, position3D pos)
		*/
		static bool isCommandMoveCompleted(ControllableObject& co);

		/*!
		@description

		Used to remove the message attached to the object within the VBS2 environment.
		
		@locality 

		Local

		@version [VBS2Fusion  v3.06]

		@param co � The object for which the message is removed .

		@return Nothing

		@example

		@code

		//attach a message to the object before removing the message (Refer ControllableObjectUtilities::applyDisplayText(ControllableObject& co))
		ControllableObjectUtilities::applyDisplayTextRemoval(co);

		@endcode

		@overloaded

		None

		@related

		ControllableObjectUtilities::applyDisplayText(ControllableObject& co)

		@remarks This is a replication of void ControllableObjectUtilities::removeDisplayText(ControllableObject& co) function 
		*/
		static void applyDisplayTextRemoval(ControllableObject& co);

		/*!
		@description

		Used to preload all textures, materials and proxies to render an object within the VBS2 environment. It returns true once all data is loaded and ready.
		
		@locality 

		Global

		@version [VBS2Fusion  v3.06]

		@param co � The object for which the preloading is done.

		@param distance - Whatever the distance to specify the area to be rendered with the object

		@return bool

		@example

		@code

		displayString+="Preload co "+conversions::BoolToString(ControllableObjectUtilities::applyPreloadObject(co,10));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To preload all textures, materials , proxies etc for an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyPreloadObject(ControllableObject& co, double distance); 

		@remarks This is a replication of bool ControllableObjectUtilities::preloadObject(ControllableObject& co, double distance) function
		*/
		static bool applyPreloadObject(ControllableObject& co, double distance);

		/*!
		@description

		Used to check if an object is able to move and return true or otherwise. This function checks only the damage value and not the fuel.
		
		@locality 

		Global

		@version [VBS2Fusion  v3.06]

		@param co � The object which is checked. The object should be created before passing it as a parameter.   

		@return bool

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString+="co Is Able to Move : "+conversions::BoolToString(ControllableObjectUtilities::isAbleToMove(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To check whether an object created in a client machine within a network is able to move, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: isAbleToMove(ControllableObject& co).

		@remarks This is a replication of bool ControllableObjectUtilities::canMove(ControllableObject& co) function  
		*/
		static bool isAbleToMove(const ControllableObject& co);

		/*!
		@description

		Used to check if an object is able to fire and return true or otherwise. This function checks only the damage value and not the ammo, thus the return value is true even if the unit is out of ammo.The function would return false only if the gun is damaged and if there is no gunner in the vehicle regardless of damage level
		
		@locality 

		Global

		@version [VBS2Fusion  v3.06]

		@param co � The object which is checked. The object should be created before passing it as a parameter.   

		@return bool

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString+="co Is Able to fire : "+conversions::BoolToString(ControllableObjectUtilities::isAbleToFire(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To check whether an object created in a client machine within a network  is able to fire, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: isAbleToFire(ControllableObject& co).

		@remarks This is a replication of bool ControllableObjectUtilities::canFire(ControllableObject& co) function
		*/
		static bool isAbleToFire(const ControllableObject& co);

		/*!
		@description
		
		Used to adjust the slope of the object to follow the underlying terrain contour. (The function will not place the object on top of the surface, but only adjusts the angle of the object) 
		
		@locality 

		Local

		@version [VBS2Fusion  v3.06]

		@param co � The object that you want to adjust. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyPlaceOnSurface(co);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void ControllableObjectUtilities::placeOnSurface(ControllableObject& object) function
		*/
		static void applyPlaceOnSurface(ControllableObject& co);

		/*!
		@description
		
		Used to suspend the simulation of an object within the VBS2 Environment. 
		
		@locality 

		Local

		@version [VBS2Fusion v3.05]

		@param co � The object for which the simulation is suspended. The object should be created before passing it as a parameter.

		@param collidable � A boolean value to determine the object appearance. If collidable is true, then object can collide and if otherwise  object will disappear from VBS2.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applySuspend(co,false);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void Suspend(ControllableObject& co , bool collidable = true ) function 
		*/
		static void applySuspend(ControllableObject& co , bool collidable = true );

		/*!
		@description
		
		Used to reactivate a suspended object to its normal status within the VBS2 Environment. 
		
		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co � The object for which the simulation is suspended. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyReactivate(co);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void Reactivate(ControllableObject& co) function 
		*/
		static void applyReactivate(ControllableObject& co);

		/*!
		@description
	
		Used to turn an object within the VBS2 Environment. This is different from using the void ControllableObjectUtilities::applyDirection(ControllableObject& co, double dir) function which immediately sets the direction of the unit to the direction specified. This function simulates the object actually turning in a more realistic manner.

		@locality 

		Local

		@version [VBS2Fusion v3.06]

		@param co � The object which is turned. The object should be created before passing it as a parameter.

		@param direction � Value should be specified in degrees. The direction should be specified in relation to world coordinates(i.e. 0 along positive x axis, 90 along positive z axis etc. )
		
		@return Nothing

		@example

		@code

		//create the object using its respective classes for effective results for example the object(u) below is created via UnitUtilities::createunit(Unit u) 
		ControllableObjectUtilities::applyTurn(u,190);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void ControllableObjectUtilities::Turn(ControllableObject& co, double direction) function	
		*/
		static void applyTurn(ControllableObject& co, double direction);

		/*!
		@description

		Used to turn object with respect to its current direction within the VBS2 Environment. This slightly differs from void ControllableObjectUtilities::applyTurn(ControllableObject& co, double direction) function which consider the absolute direction.

		@locality 

		Local

		@version [VBS2Fusion v3.05]

		@param co � The object which is turned. The object should be created before passing it as a parameter.

		@param relativeDir � The direction in degrees which the object should turn.And the relativeDir is specified in relation to the objects current direction.

		@return Nothing

		@example

		@code

		//create the object using its respective classes for effective results for example the object(u) below is created via UnitUtilities::createunit(Unit u)
		ControllableObjectUtilities::applyTurnRelative(u1,190);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void ControllableObjectUtilities::TurnRelative(ControllableObject& co, double direction) function 
		*/
		static void applyTurnRelative(ControllableObject& co, double relativeDir);

		/*!
		@description

		Used to select a weapon of an object within the VBS2 Environment using the weapon name.For weapons that have more than one muzzle, you have to input the muzzle name and not the weapon name.The only weapons that have muzzleNames seem to be rifles with grenade launchers, handgrenades,smokeshells and satchels. In all other cases the weaponName must be used.
		
		@locality 

		Local

		@version [VBS2Fusion v3.05]

		@param co � The object for which the weapon is changed. The object should be created before passing it as a parameter.

		@param muzzleName � The weapon name of the weapon to be changed.

		@return Nothing

		@example

		@code

		//It is highly recommended to create the object using its respecitive class, before apply the function.For e.g. the object below is created via UnitUtilities::CreateUnit(Unit u)
		ControllableObjectUtilities::applyWeaponSelection(co,"vbs2_CarlGustavM2");

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void ControllableObjectUtilities::selectWeapon(ControllableObject& co, string muzzleName) function
		*/
		static void applyWeaponSelection(ControllableObject& co, const std::string& muzzleName);

		/*!
		@description

		Used to check if an object is floatable and return true or otherwise.
		
		@locality 

		Global

		@version [VBS2Fusion  v3.05]

		@param co � The object which is checked. The object should be created before passing it as a parameter.   

		@return bool

		@example

		@code

		//To display the bool as a string use the function BoolToString() as shown below
		displayString ="Is Floatable: " conversions::BoolToString(ControllableObjectUtilities::isFloatble(co));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To check whether an object created in a client machine within a network is floatable, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::isFloatble(ControllableObject& co).

		@remarks This is a replication of  bool canFloat(ControllableObject& co) function 
		*/
		static bool isFloatable(const ControllableObject& co);

		/*!
		@description

		Used to disables or enables user actions for a specific object within the VBS2 Environment.
		
		@locality 

		Local

		@version [VBS2Fusion  v3.05]

		@param co � The object for which the actions are disabled. (I.e. Other objects wouldn�t be able to interact with this object).

		@param disable � The parameter passed to the object (If true, actions will be disabled and otherwise)

		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyActionsDisable(co,true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To apply action disable for an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyActionsDisable(ControllableObject& co, bool disable).

		@remarks As mentioned above, this function prevents user interactions with other objects. (For e.g. if the function is  applied to a vehicle the user will not be able to interact with the vehicle)

		@remarks This is a replication of void ControllableObjectUtilities::disableActions(ControllableObject& co, bool action=true) function
		*/
		static void applyActionsDisable(ControllableObject& co, bool disable=true);

		/*!
		@description

		Used to change the geometry settings (turn on or off) of the object within the VBS2 environment. A true argument turns the specific setting 

		@locality 

		Global

		@version [VBS2Fusion  v3.05]

		@param co � The object for which the geometry settings is disabled.

		@param visible - Toggles object�s visibility

		@param fire- Toggles object�s fire geometry

		@param view- Toggles object�s view geometry

		@param collision- Toggles object�s collision geometry
		
		@param roadways - Toggles object's roadways 
		
		@param shadows - Toggles object's shadows 

		
		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyGeometrySettingsDisable(co,true, true, true, true);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks To change the geometry settings for an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities:: applyGeometrySettingsDisable(ControllableObject& co, bool visible, bool fire, bool view, bool collision); 

		@remarks This is a replication of void ControllableObjectUtilities::disableGeo(ControllableObject& co, bool visible, bool fire, bool view, bool collision) function. But two additional parameter is added.
		*/
		static void applyGeometrySettingsDisable(ControllableObject& co, bool visible, bool fire, bool view, bool collision, bool roadways, bool shadows);

		/*!
		@description

		Used to perform a specified action by the object, within the VBS2 Environment.If the action is done by the object itself,  "co" and "targetCo" should be the same. If the action is performed by or on a different object, objects should be passed accordingly.

		@locality 

		Local

		@version [VBS2Fusion v3.05]

		@param co � The object which performs the action. The object should be created before passing it as a parameter.

		@param type � The action performed by the object. Refer appendix in the VBS2Fusion Product Manual.

		@param targetCo - object which is performing the action
		
		@return Nothing

		@example

		@code

		ControllableObjectUtilities::applyAction(co,"SitDown",co);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This is a replication of void performAction(ControllableObject& co, string type, ControllableObject& targetCo) function 

		@remarks If you want to perform an action with parameters, use overloaded versions of this function.
		*/
		static void applyAction(ControllableObject& co, const std::string& type, ControllableObject& targetCo);

		/*!
		
		@description
		
		Used to control the movement of a unit within the VBS2 environment. This command should be used in conjunction with the co.setMoveDirection  and co.setspeed.
		A single call to this method will order the unit to move in a straight line towards the direction pointed by co.setMoveDirection  at the speed specified by co.setspeed.
		Use co.setMoveDirection to control the direction of movement and co.setspeed to control the speed of the movement. 
		This command can be called continuously within the OnSimulationStep loop to control and manipulate the movement of the unit. Set the speed to 0 to stop the unit from moving.
		The unit will keep walking forward until an obstacle is reached. When an obstacle is reached, the unit will stop. 
		This is a crude method of controlling the direction and speed (i.e. steering) of  an object. 

		@locality 

		Locally applied

		@version [VBS2Fusion v3.05]

		@param co - The object for which the function is applied to. The object should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//setting the speed and direction before calling the function
		Unit u1;
		
		u1.setSpeed(5.00);
		
		double newangle = 90.00;
		
		ControllableObjectUtilities::Turn(u1,newangle);
		
		u1.setMoveDirection(newangle);
		
		ControllableObjectUtilities::applySpeed(u1,5);
		
		ControllableObjectUtilities::applyMoveForward(u1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks This function will not work if applied to the player unit. 

		@remarks This function should be called periodically instead  at everyOnSimulationStep as the unit walks 20 meters ahead at each call of this function. A call every 2-3 seconds will provide the same functionality as a call at every OnSimulation step and will also improve performance.

		@remarks This function should be called to this function soon after making any changes to the moveDirection
		or speed properties of an object for them to register immediately. 

		@remarks Disable 'PATHPLAN' AI for the unit if you do not want the unit to perform path planning and stop at obstacles. 

		@remarks This is a replication of void moveForward(ControllableObject& co) function.
		
		@remarks It is highly recommended to create objects using its respective classes for results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc).

		@remarks To make the object created to move forward in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all  clients in the network before calling the function ControllableObjectUtilities::moveForward(ControllableObject &co ).

		*/
		static void applyMoveForward(ControllableObject& co);

		/*!
		@description

		Used to perform a specified action by the object, within the VBS2 Environment

		@locality 

		Local

		@version [VBS2Fusion  v3.05]

		@param co � The object which performs the action. The object should be created before passing it as a parameter.

		@param actionName - The action performed by the object. Refer appendix in the VBS2Fusion product manual.

		@param targetCo - if the action is done by the object itself,  "co" and "targetCo" should be the same or it should be passed accordingly.

		@param secondTargetCo - Additional target Controllable object for the action performed

		@return Nothing	

		@example

		@code

		//It is highly recommended to create the objects using their respective classes for effective results. For e.g. the object u shown below is created via UnitUtilities::CreateUnit (Unit u)
		UnitUtilities::updateDynamicProperties(u);
		ControllableObject obj = WorldUtilities::getNearestObject(u.getPosition());
		ControllableObjectUtilities::applyAction(u,"SetTimer",u,obj);
		displayString = "Timer is set";

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Some of the actions that can be invoked by using this function are Deactivate, SetTimer and StartTimer. 
		*/
		static void applyAction(ControllableObject& co, const std::string& actionName ,ControllableObject& targetCo, ControllableObject& secondTargetCo);

		/*!
		@description

		Used to perform a specified action by the object, within the VBS2 Environment

		@locality 

		Local

		@version [VBS2Fusion  v3.05]

		@param co  � The object which performs the action. The object should be created before passing it as a parameter.

		@param actionName - The action performed by the object. Refer appendix in the VBS2Fusion Product Manual 

		@param targetCo - if the action is done by the object itself,  "co" and "targetCo" should be the same or it should be passed accordingly.

		@param intFirst - The integer parameter of the action.

		@return Nothing	

		@example

		@code

		//It is highly recommended to create the objects using their respective classes for effective results. For e.g. the object u and veh shown below are created via UnitUtilities::CreateUnit (Unit& unit), and Vehicle Utilities::CreateVehicle (Vehicle& vehicle)
		ControllableObjectUtilities::applyAction(u,"GetInCargo",veh,10000);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Some of the actions that can be invoked by using this function are : User, GetInCargo and MoveToCargo.
		*/
		static void applyAction(ControllableObject& co, const std::string& actionName ,ControllableObject& targetCo, int intFirst);

		/*!
		@description

		Used to perform a specified action by the object, within the VBS2 Environment

		@locality 

		Local

		@version [VBS2Fusion  v3.05]

		@param co � The object which performs the action. The object should be created before passing it as a parameter.

		@param actionName - The action performed by the object. Refer appendix in the VBS2Fusion Product Manual 

		@param targetCo - if the action is done by the object itself, "co" and "targetCo" should be the same or it should be passed accordingly.

		@param turretPath - The turret path of the turret used in the action

		@return Nothing	

		@example

		@code

		//It is highly recommended to create the objects using their respective classes for effective results. For e.g. the object u and veh shown below are created via UnitUtilities::CreateUnit (Unit& unit), and Vehicle Utilities::CreateVehicle (Vehicle& vehicle)
		Turret::TurretPath turretPath_0;
		turretPath_0.push_back(0);
		//update the vehicle properties of the vehicle before passing it as a parameter
		VehicleUtilities::updateDynamicProperties(veh);
		VehicleUtilities::updateDynamicProperties(veh);
		ControllableObjectUtilities::applyAction(u,"GetInTurret",veh,turretPath_0);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks Some of the actions that can be invoked by using this function are, GetInTurret, MoveToTurret
		*/
		
		static void applyAction(ControllableObject& co, const std::string& actionName, ControllableObject& targetCo, const Turret::TurretPath& turretPath);

		/*!
		@description 
		Apply position and orientation changes to an object over a specified time period.

		@locality

		locally applied locally effected.

		@version  [VBS2 2.02/VBS2Fusion v3.10.1]

		@param co - Object to interpolate.
		@param position - Position vector that the object to move
		@param vectorDir - Target direction vector 
		@param vectorUp - Target up vector to specify the alignment
		@param speed  Number of sections interpolation should take

		@return Nothing.

		@example

		@code

		//Controllable object to be created
		ControllableObjectUtilities::createObject(co1,"vbs2_se_army_cv9040c_d_x",position3D(2400,1900,0));

		//Apply Interpolation to given controllable object.
		ControllableObjectUtilities::applyInterpolation(co1,vector3D(1000,10,1000),vector3D(0,0,1),vector3D(0,1,0),60);

		@endcode

		@overloaded  None.

		@related

		@remarks 

		Some objects may be moved vertically (up or down) by the interpolation.
		The movement of some static objects may be jerky
		Objects without geometry or collision LODs defined will not stop at their destination, and will have to be stopped via a applyVelocity command.

		*/
		static void applyInterpolation(ControllableObject& co, const vector3D& position, const vector3D& vectorDir, const vector3D& vectorUp, double speed);

		/*!
		@description 

		Used to obtain the 'visible position' of an object(AGL) within the VBS2 environment.

		@locality

		Globally Applied

		@version  [VBS2Fusion v3.11]

		@param co - The object to be created.

		@return position3D - AGL(above ground level) position.

		@example

		@code

		//The object to be created
		ControllableObject co;

		//To obtain the position as a string use the function getVBSPosition as shown below
		displayString+=" The visible position of an object is "+ControllableObjectUtilities::getVisiblePosition(co).getVBSPosition();

		@endcode

		@overloaded  

		Nothing.

		@related

		@remarks It is highly recommended to create objects using its respective classes for effective results. (For e.g. create units via the function UnitUtilities::CreateUnit(Unit& u), create vehicles via the function VehicleUtilities::CreateVehicle(Vehicle& vehicle),create generic objects via the function ControllableObjectUtilities::createObject(ControllableObject& co) and etc)  
		@remarks To obtain the visible position of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function ControllableObjectUtilities::getVisiblePosition(ControllableObject& co).(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
 	
		*/
		static position3D getVisiblePosition(const ControllableObject& co);

		/*!
		@description 
		
		Gets the texture name for a hidden selection.

		@locality

		locally applied locally effected.

		@version  [VBSFusion v3.3.1]

		@param co - The object for which the texture is obtained. The object should be created before passing it as a parameter. 

		@param selection - Name of the hidden selection.

		@return string - Path to texture used or procedural texture.

		@example

		@code
		
		std::string bomberTexture = ControllableObjectUtilities::getObjectTexture(bomber,std::string("hide_scarf"));
		displayString += "\\n Bomber Texture: "+ bomberTexture;

		@endcode

		@overloaded 
		
		ControllableObjectUtilities::getObjectTexture(const ControllableObject& co, int index);

		@related

		@remarks
		
		*/
		static std::string getObjectTexture(const ControllableObject& co, const std::string& selection);

		/*!
		@description 
		
		Enables specific capabilities for a unit.
		The available capabilities are:
		FREEFALL: (default: true) If true, then a unit falling from an elevation more than 10m above the ground, will switch to a parachuting animation. If false, unit will fall in regular standing animation. An already falling unit can be switched from standing to parachuting animation (by enabling "FREEFALL"), but not the other way round.
		REBREATHER: (default: false) If true, the unit will swim much faster, and is able dive underwater indefinitely. In V3.0+ diving goggles are shown from the player's perspective.
		SCUBA: (default: false) Same as "REBREATHER". 
		A unit without "SCUBA" or "REBREATHER" capability will die after about 45 seconds underwater. (Not available in V3.x!).
		STEERABLEPARACHUTE: (default: false) Adds action to unit to deploy a steerable parachute. When ejecting from an aircraft, unit will not automatically open their regular (non-steerable) parachute, but enter the free-fall animation, until the chute is opened manually. Capability "FREEFALL" capability needs to be enabled, in order for the parachute to be available.
		SWIM: (default: true) If disabled, the unit will walk underwater (or sink to the ground), until it's out of breath, and die. If the "SWIM" capability is removed while a unit is swimming, this will not have any effect until leaving and re-entering the water.

		The following capabilities are available for player units in V2.08+, and require the simulation option "Vehicle and weapon qualification" to be enabled. Vehicles whose control has been disabled, can still be entered, but FreeLook is disabled when occupying the driver's seat. All of the capabilities listed below are true by default.
		
		DRIVEWHEELED: Control operation of vehicles of simulation type car(x). Motorcycles or bicycles (simulation type "motorcycle"), as well as wheeled UGVs are not affected.
		DRIVETRACKED: Control operation of vehicles of simulation type tank(x). Tracked UGVs are not affected.
		DRIVEROTORWING: Control operation of vehicles of simulation type helicopter(x). Helicopter-type UAVs are not affected.
		DRIVEFIXEDWING: Control operation of vehicles of simulation type plane(x). Only affects the manual control option for fixed wing UAVs.
		DRIVESHIP: Control operation of vehicles of simulation type ship(x).
		FIREVEHICLESHOTSHELL: Control operation of armored vehicles' main guns.
		FIREVEHICLESHOTBULLET: Control operation of vehicles' machineguns.
		FIREVEHICLESHOTMISSILE: Control operation of vehicles' missile launchers.
		FIREVEHICLESHOTROCKET: Control operation of vehicles' rocket launchers.

		@locality

		Globally applied.

		@version  [VBSFusion v3.3.1]

		@param co - The object for which the capability is applied. 

		@param CAPABILITYMODE - The capability which applies to the controllable unit.

		@return None.

		@example

		@code
	
		//The object to be created
		ControllableObject co;

		// Applied the capability for Controllable object.
		ControllableObjectUtilities::applyCapability(co, CAPABILITYMODE::SWIM);

		@endcode

		@overloaded 
				
		@related

		@remarks
		
		*/
		static void applyCapability(ControllableObject& co, CAPABILITYMODE mode);

		/*!
		@description 
		Disables a specific capability for a unit. The available capabilities are described in ControllableObjectUtilities::applyCapability(ControllableObject& co, CAPABILITYMODE mode) function.	

		@locality		

		Globally applied.

		@version  [VBSFusion v3.3.1]

		@param co - The object for which the capability is applied. 

		@param CAPABILITYMODE - The capability which applies to the controllable unit.

		@return None.

		@example

		@code

		//The object to be created
		ControllableObject co;

		//Removed the capability for Controllable object.
		ControllableObjectUtilities::applyCapabilityRemoval(co, CAPABILITYMODE::SWIM);
		
		@endcode

		@overloaded 
				
		@related

		@remarks
		
		*/
		static void applyCapabilityRemoval(ControllableObject& co, CAPABILITYMODE mode);

		/*!
		@description 
		Checks whether a unit has a specific capability enabled.
		The available capabilities are described in
		ControllableObjectUtilities::applyCapability(ControllableObject& co, CAPABILITYMODE mode) function.

		@locality

		Globally applied.

		@version  [VBSFusion v3.3.1]

		@param co - The object for which the capability is applied. 

		@param CAPABILITYMODE - The capability which applies to the controllable unit.

		@return true - if the capability is applied
				false - if the capability is not applied.

		@example

		@code

		//The object to be created
		ControllableObject co;

		ControllableObjectUtilities::isCapabilityApplied(co, CAPABILITYMODE::SWIM);

		@endcode

		@overloaded 
				
		@related

		@remarks
		
		*/
		static bool isCapabilityApplied(const ControllableObject& co, CAPABILITYMODE mode);	
	};
};

#endif //VBS2FUSION_CONTROLLABLE_OBJECT_UTILITIES_H