
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/
/*************************************************************************

Name:

WeaponUtilities.h

Purpose:

This file contains the declaration of the WeaponUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			29-01/2010	YFP: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_WEAPONUTILITIES_H
#define VBS2FUSION_WEAPONUTILITIES_H

/**************************************************************************
To disable the warnings arise because of using deprecated ArmedGroundVehicle 
inside the WeaponUtilities class.
Warning identifier [C:4996]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4996)

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "data/ArmedGroundVehicle.h"
#include "data/Projectile.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBSFusion
{

	class VBSFUSION_API WeaponUtilities
	{
	public:
	
		/*! Get the elevation of the weapon in specified turret of the Armed Vehicle from VBS2 Environment. */
		static double getElevation(const Vehicle& vehicle, const Turret& turret, const Weapon& weapon);

		/*! Get azimuth of the weapon in specified turret of the armed vehicle from VBS2 Environment.  */
		static double getAzimuth(const Vehicle& vehicle, const Turret& turret, const Weapon& weapon);

		/*!
		Gets ammo count of the weapon in the vehicle. If weapon is given with invalid weaponClassName or vehicle
		doesn't have armed features then function will return 0. If function is not executed, return value would 
		be -1. Before call the function, Correct weaponClassName needs to be set to the weapon object.
		*/
		static int getAmmoCount(const Vehicle& vehicle, const Weapon& weapon);

		/*! Get the aiming direction of the specified Weapon of the turret in armed vehicle.
		This method returns a unit vector of the direction. 
		*/
		static position3D getWeaponDirectionVector(const Vehicle& vehicle, const Turret& turret, const Weapon& weapon);

		/*! set reload state of the weapon.
		Deprecated. Use applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime)
		*/
		VBSFUSION_DPR(UWPN006)static void setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime);

		/*! Get the elevation of the weapon in specified turret of the Armed Vehicle from VBS2 Environment. */
		VBSFUSION_DPR(UWPN003) static double getElevation(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! Get azimuth of the weapon in specified turret of the armed vehicle from VBS2 Environment.  */
		VBSFUSION_DPR(UWPN002) static double getAzimuth(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! 
		Gets ammo count of the weapon in the armed vehicle from VBS2 Environment. If weapon is given with invalid 
		weaponClassName then function will return 0. If function is not executed, return value would be -1. Before 
		call the function, Correct weaponClassName needs to be set to the weapon object.
		*/
		VBSFUSION_DPR(UWPN001) static int getAmmoCount(ArmedGroundVehicle& vehicle, Weapon& weapon);

		/*! Get the aiming direction of the specified Weapon of the turret in armed vehicle.
		This method returns a unit vector of the direction. 
		*/
		VBSFUSION_DPR(UWPN004) static position3D getWeaponDirectionVector(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! set reload state of the weapon.
		Deprecated. Use applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime)
		*/
		VBSFUSION_DPR(UWPN005) static void setWeaponState(ArmedGroundVehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime);

		/*! 
		Creates a shot of the specified type, position and velocity
		*/
		static Projectile createShot(const std::string& ammoType, const position3D& pos, const vector3D& velocity);

		/*! 
		Enables shot creation via mouse clicks. The shot will be created at the player's location, and move
		towards	the position defined by the mouse. The mouse click can either be generated via an in-game 
		dialog, or via a windows message
		Deprecated. Use applyShotCreationEnable(bool enable)
		*/
		VBSFUSION_DPR(UWPN007) static void allowCreateShot(bool enable);

		/*! 
		Returns the unit or vehicle that fired the given shot or laser target.
		*/
		static ControllableObject getShotOwner(const Projectile& proj);

		/*! 
		Makes explosive shell inert. Shell wont create any visual effects or damage and will disappear.
		Deprecated. Use applyDummyRound(Projectile& proj, bool enable = true)
		*/
		VBSFUSION_DPR(UWPN008) static void setDummyRound(Projectile& proj, bool enable = true);


		/*! 
		Creates a shot of the specified type, position and velocity
		sound - Class name of weapon whose sound to play when shot is created.
		tracer - If true, then a tracer shot will be created.
		*/
		static Projectile createShot(const std::string& ammoType, const position3D& pos, const vector3D& velocity, const std::string& sound, bool tracer);

		/*!
		@description 
		Selects the weapon to be used via allowCreateShot created shots.

		@locality
		Locally Applied, Locally Effected

		@version  [VBS2Fusion v3.11]

		@param slot � Slot that can be passed through the GetMessageExtraInfo value in the WM_LBUTTONDOWN windows message (Default: 0).
		@param weapon - Weapon class name to be used by WeaponUtilities::allowCreateShot(bool enable) function.

		@return Nothing.

		@example

		@code

		WeaponUtilities::applyLasershotWeapon(0,"M136")

		@endcode

		@overloaded None.

		@related
		void WeaponUtilities::allowCreateShot(bool enable)

		@remarks 	
		*/
		static void applyLasershotWeapon(int slot, const std::string& weapon);

		/*!
		@description 

		Used to set the reload state of a weapon within the VBS2 environment. 

		@locality

		Locally applied Globally effected

		@version [VBS2Fusion v3.12] 

		@param vehicle - Type of vehicle.
		@param weapon - Type of weapon.
		@param reloadStatus - Reload status true or false.
		@param reloadTime - Time to reload.

		@return nothing.

		@example

		@code

		//The vehicle for which the weapon state is changed
		Vehicle veh;
		Weapon w;
		Turret::TurretPath turretPath;
		turretPath.push_back(0);

		w = VehicleUtilities::getWeaponState(veh,turretPath);

		WeaponUtilities::applyWeaponState(veh,w,true,0);

		@endcode

		@overloaded 

		None.

		@related  

		@remarks This is a replication of void setWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime);
		@remarks This function is mainly applicable to vehicles.
		*/
		static void WeaponUtilities::applyWeaponState(const Vehicle& vehicle, const Weapon& weapon, bool reloadStatus, double reloadTime);

		/*!
		@description 

		Enables shot creation via mouse clicks. The shot will be created at the player's location, and move
		towards	the position defined by the mouse. The mouse click can either be generated via an in-game 
		dialog, or via a windows message

		@locality

		Locally Applied Globally Effected

		@version  [VBS2Fusion v3.12]

		@param enable � Set shot creation via mouse clicks true or false.

		@return Nothing.

		@example

		@code

		//disable the shot creation via mouse click
		WeaponUtilities::applyShotCreationEnable(false);

		@endcode

		@overloaded 

		None.

		@related

		None

		@remarks This is a replication of void allowCreateShot(bool enable)
		*/
		static void applyShotCreationEnable(bool enable);

		/*!
		@description 
		Makes explosive shell inert. Shell wont create any visual effects or damage and will disappear.

		@locality

		Globally effected

		@version  [VBS2Fusion v3.12]
		
		@param proj - The projectile.

		@param enable � Slot that can be passed through the GetMessageExtraInfo value in the WM_LBUTTONDOWN windows message (Default: 0).

		@return Nothing.

		@example

		@code

		WeaponUtilities::applyDummyRound(grenade1,true);

		@endcode

		@overloaded None.

		@related

		@remarks This is a replication of void setDummyRound(Projectile& proj, bool enable = true);
		*/
		static void applyDummyRound(const Projectile& proj, bool enable = true);


		/*! 
		@description 
		Returns bounding box of specified weapon type for given unit.

		@locality

		Globally Applied, Globally Effected

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@param type - "PRIMARY", "SECONDARY", "HANDGUN" or "BINOCULAR".

		@param bbMin - Bounding Box minimum (out)

		@param bbMax - Bounding Box maximum (out)

		@return Nothing.

		@example

		@code

		Unit player;
		Vector3f bbMin,bbMax;

		WeaponUtilities::getWeaponBoundingBox(player,"PRIMARY",bbMin,bbMax);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static void getWeaponBoundingBox(const Unit& unit, const std::string& type, Vector3f &bbMin, Vector3f &bbMax);

		/*! 
		@description 
		Returns name of specified weapon type for given unit.

		@locality

		Globally effected

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@param type - "PRIMARY", "SECONDARY", "HANDGUN" or "BINOCULAR".

		@return string - display name of weapon

		@example

		@code

		Unit unit2;

		WeaponUtilities::getWeaponName(unit2, string("PRIMARY"));

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static std::string getWeaponName(const Unit& unit, const std::string& type);

		/*! 
		@description 
		Returns muzzle camera position of specified weapon type for given unit.

		@locality

		Globally effected

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@param type - "PRIMARY", "SECONDARY", "HANDGUN" or "BINOCULAR".

		@return Vector3f - muzzle camera position

		@example

		@code

		Unit unit2;
		Vector3f vectorMuzzle;

		vectorMuzzle=WeaponUtilities::getMuzzleCameraPos(unit2, string("PRIMARY"));

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static Vector3f getMuzzleCameraPos(const Unit& unit, const std::string& type);

		/*! 
		@description 
		Returns muzzle position of specified weapon type for given unit.

		@locality

		Globally effected

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@param type - "PRIMARY", "SECONDARY", "HANDGUN" or "BINOCULAR".

		@return Vector3f - muzzle position

		@example

		@code

		Unit unit2;
		Vector3f vectorMuzzle;

		vectorMuzzle=WeaponUtilities::getMuzzlePos(unit2, string("HANDGUN"));

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static Vector3f getMuzzlePos(const Unit& unit, const std::string& type);

		/*! 
		@description 
		Returns weight of specified weapon type for given unit.

		@locality

		Globally effected

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@param type - "PRIMARY", "SECONDARY", "HANDGUN" or "BINOCULAR".

		@return float - weight of weapon type

		@example

		@code

		Unit unit2;

		float weight=WeaponUtilities::getWeaponWeight(unit2, string("HANDGUN"));

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static float getWeaponWeight(const Unit& unit, const std::string& type);

		/*! 
		@description 
		Returns reload time of specified weapon type for given unit.

		@locality

		Globally effected

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@param type - "PRIMARY", "SECONDARY", "HANDGUN" or "BINOCULAR".

		@return float - reload time

		@example

		@code

		Unit unit2;

		float time=WeaponUtilities::getWeaponReloadTime(unit2, string("PRIMARY"));

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static float getWeaponReloadTime(const Unit& unit,const std::string& type);

		/*! 
		@description 
		Returns zoom of specified weapon type for given unit.

		@locality

		Globally effected

		@version  [VBSFusion v3.15]

		@param unit - passed unit.

		@param type - "PRIMARY", "SECONDARY", "HANDGUN" or "BINOCULAR".

		@param opticsZoomMin - minimum zoom (out)

		@param opticsZoomMax - maximum zoom (out)

		@return Nothing.

		@example

		@code

		Unit unit2;
		float optZoomMax, optZoomMin;

		WeaponUtilities::getMuzzleOpticsZoom(unit2, string("BINOCULAR"), optZoomMin, optZoomMax);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static void getMuzzleOpticsZoom(const Unit& unit,const std::string& type, float& opticsZoomMin, float& opticsZoomMax);

		/*! 
		@description 
		Returns information about burst count and reload time for given muzzle in given mode.

		@locality

		Globally effected

		@version  [VBSFusion v3.15]

		@param u - passed unit.

		@param type - "PRIMARY", "SECONDARY", "HANDGUN" or "BINOCULAR".

		@param muzzle - muzzle index

		@param mode -

		@param burst - burst count (out)

		@param reloadTime - reload time (out)

		@return bool - true if success.

		@example

		@code

		Unit unit2;
		int muzzle,mode,burst;
		float reloadTime;

		bool b=WeaponUtilities::getMuzzleInfo(unit2,string("PRIMARY"),muzzle,mode,burst,reloadTime);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static bool getMuzzleInfo(const Unit& u, const std::string &type, int muzzle, int mode, int& burst, float& reloadTime);

		/*! 
		@description 
		Returns maximum burst (i.e. bullets per burst) for given weapon type. 
		Also returns information if weapon is fully automatic.

		@locality

		Globally effected

		@version  [VBSFusion v3.15]

		@param u - passed unit.

		@param type - "PRIMARY", "SECONDARY", "HANDGUN" or "BINOCULAR".

		@param maxBurst - number of bullets per burst (out)

		@param fullAuto - automatic or not (out)

		@return Nothing.

		@example

		@code

		Unit unit2;
		int maxBru;
		bool fullAuto;

		WeaponUtilities::getWeaponMaxBurst(unit2,string("BINOCULAR"), maxBru, fullAuto);

		@endcode

		@overloaded

		@related

		@remarks
		*/
		static void getWeaponMaxBurst(const Unit& u, const std::string& type, int& maxBurst, bool& fullAuto);

	};

#pragma warning (pop) // Enable warnings [C:4996]

};

#endif

