/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

FSMUtilities.h

Purpose:

This file contains the declaration of the FSMUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			28-02/2013	BISim(Otakar Nieder): Original Implementation
/************************************************************************/

#include "VBSFusion.h"

#ifndef VBS2FUSION_FSMUTILITIES_H
#define VBS2FUSION_FSMUTILITIES_H

#include "data/FSM.h"
namespace VBSFusion
{
  namespace FSM
  {
    /* Class providing access to FSM related functionality.
        Example of usage:
          VBSFusion::FSM::TypeValueContainer object(VBSFusion::FSM::typeObject, (void *)&unit);
          std::string oString(output.getConstCharPtr());
          std::vector<string> oVector;
          oVector.push_back(oString);
          VBSFusion::FSM::TypeValueContainer string(VBSFusion::FSM::typeStringArray, (void *)&oVector);
          VBSFusion::FSM::TypeValueContainer result(VBSFusion::FSM::typeVoid, (void *)NULL);
          VBSFusion::FSM::FSMUtilities::Execute("DisplayObjectText", result, object, string);
    */
    class VBSFUSION_API FSMUtilities
    {
    public:
  		/*!
      Executes function implementing VBS2 command that does not take any parameters.
      @param functionName -  Name of the function that implements a command in VBS2. This is not name of the command, it is name of the function that implements it in VBS2.
      @param result Reference to variable that contains definition of type for result of the command. It is filled with the resulting value of the command.
      */
      static bool Execute(const std::string& functionName, TypeValueContainer &result);
  		/*!
      Executes function implementing VBS2 command that does not take any parameters.
      @param functionName -  Name of the function that implements a command in VBS2. This is not name of the command, it is name of the function that implements it in VBS2.
      @param result Reference to variable that contains definition of type for result of the command. It is filled with the resulting value of the command.
      @param oper1 Reference to first parameter of the command.
      */
      static bool Execute(const std::string& functionName, TypeValueContainer &result, const TypeValueContainer &oper1);
  		/*!
      Executes function implementing VBS2 command that takes 2 parameters.
      @param functionName -  Name of the function that implements a command in VBS2. This is not name of the command, it is name of the function that implements it in VBS2.
      @param result Reference to variable that contains definition of type for result of the command. It is filled with the resulting value of the command.
	  @param oper1 Reference to first parameter of the command.
      @param oper2 Reference to second parameter of the command.
      */
      static bool Execute(const std::string& functionName, TypeValueContainer &result, const TypeValueContainer &oper1, const TypeValueContainer &oper2);
      /*!
      Returns error code set by last execution.
      */
      static ExecutionError GetLastError();
      /*!
      Returns text context of the last error.
      */
      static std::string GetLastErrorContext();
    };
  }
};
#endif //VBS2FUSION_FSMUTILITIES_H