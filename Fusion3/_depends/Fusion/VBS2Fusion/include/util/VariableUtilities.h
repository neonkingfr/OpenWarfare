/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	VariableUtilities.h

Purpose:

	This file contains the declaration of the VariableUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			09-01/2010	RMR: Original Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		18-11-2011	DMB: Added Functions
									void bcastPublicVariable(string name)	

/************************************************************************/

#ifndef VBS2FUSION_VARIABLE_UTILITIES_H
#define VBS2FUSION_VARIABLE_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <math.h>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "conversions.h"
#include "data/ControllableObject.h"
#include "data/Group.h"
#include "util/ExecutionUtilities.h"
//#include "data/VBS2Variable.h"
#include "data/VBSVariable.h"
#include "position3D.h"

namespace VBSFusion
{	
	

	class VBSFUSION_API VariableUtilities
	{
	public:

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as a double. 
		Set public to false to limit the variable to the local machine. 
		*/
		VBSFUSION_DPR(UVRB001) static void setVariable(ControllableObject& co, std::string& name, double value, bool bPublic = true);

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as an integer. 
		Set public to false to limit the variable to the local machine. 
		*/
		VBSFUSION_DPR(UVRB001) static void setVariable(ControllableObject& co, std::string& name, int value, bool bPublic = true);

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as a float. 
		Set public to false to limit the variable to the local machine. 
		*/
		VBSFUSION_DPR(UVRB001) static void setVariable(ControllableObject& co, std::string& name, float value, bool bPublic = true);

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as a string. 
		Set public to false to limit the variable to the local machine. 
		*/
		VBSFUSION_DPR(UVRB001) static void setVariable(ControllableObject& co, std::string& name, std::string value, bool bPublic = true);

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as a boolean. 
		Set public to false to limit the variable to the local machine. 
		*/
		VBSFUSION_DPR(UVRB001) static void setVariable(ControllableObject& co, std::string& name, bool value, bool bPublic = true);

		/*!
		Sets a variable of name with value to the ControllableObject defined by co. Variable is set as a position3D. 
		Set public to false to limit the variable to the local machine. 
		*/
		VBSFUSION_DPR(UVRB001) static void setVariable(ControllableObject& co, std::string& name, position3D value, bool bPublic = true);


		/*!
		Used to toggle civilians between standard formation (line, wedge, column, etc) and crowd formation. 
		This is a tight, random "formation", keeps them off roads, and prevents them from running to catch up with their group.
		Applies parameter globally. 

		Applies to: civilian units on foot 

		Ref: http://community.bistudio.com/wiki/VBS2:_ObjectVariables
		*/
		static void setCrowdFormation(ControllableObject& co, bool value);

		/*!
		Returns the value of the crowd formation variable. 
		*/
		static bool getCrowdFormation(const ControllableObject& co);

		/*!
		Allows individual movement control of group members, without having them fall back into formation.
		Applies to: group members on foot. 

		Applies parameter globally. 

		Parameters: Position3D - destination to move to

		Ref: http://community.bistudio.com/wiki/VBS2:_ObjectVariables
		Deprecated. Use void applyForceMove(ControllableObject& co, position3D pos)
		*/
		VBSFUSION_DPR(UVRB002) static void setForceMove(ControllableObject& co, position3D pos);

		/*!
		Returns the value of the ForceMove variable. 
		*/
		static position3D getForceMove(const ControllableObject& co);



	
		/*static void setVariable(VBSFusion::Group& g, string& name, double value, bool bPublic = true);

		static void setVariable(VBSFusion::Group& g, string& name, int value, bool bPublic = true);

		static void setVariable(VBSFusion::Group& g, string& name, float value, bool bPublic = true);

		static void setVariable(VBSFusion::Group& g, string& name, string value, bool bPublic = true);

		static void setVariable(VBSFusion::Group& g, string& name, bool value, bool bPublic = true);



		static void setVariable(VBSFusion::Waypoint& w, string& name, double value, bool bPublic = true);

		static void setVariable(VBSFusion::Waypoint& w, string& name, int value, bool bPublic = true);

		static void setVariable(VBSFusion::Waypoint& w, string& name, float value, bool bPublic = true);

		static void setVariable(VBSFusion::Waypoint& w, string& name, string value, bool bPublic = true);

		static void setVariable(VBSFusion::Waypoint& w, string& name, bool value, bool bPublic = true);*/

		/*!
		Returns a variable of the name in the form of a VBS2Variable. The type of the returned value can be 
		obtained using the TypeOf method in the VBS2Variable. Type will be VBS2Variable::NONE if the returned value
		is invalid or the variable hasn't been assigned to the object, 
		*/
		static VBSVariable getVariable(const ControllableObject& co, const std::string& name) ;

		/*!
		Broadcast variable value to all computers.Types are supported;
			Number
			Boolean
			Object
			Group
		*/
		static void bcastPublicVariable(const std::string& name);

		/*!
		Saves the variable value into the global campaign space.
		The variable is then available to all following missions in the campaign.
		*/
		static void saveVariable(const std::string& variableName);

		/*!
		@description

		Applies variable to given value in the variable space of given object.	

		@locality

		Locally applied, Globally effected

		@version [VBSFusion v3.15]

		@param co - ControllableObject that variable is assigned to.
		@param name - Name of variable. Variable name is case-sensitive.
		@param val - Value to assign to variable.
		@param isPublic - If true, then the value is broadcast to all computers.

		@return Nothing.

		@example

		@code

		Vehicle tank;

		VariableUtilities::applyVariable(tank, string("owner"), VBS2Variable(string("player")), true);

		@endcode

		@overloaded 

		Nothing.

		@related

		@remarks This is a replication of 
			void setVariable(ControllableObject& co, string& name, double value, bool bPublic = true),
			void setVariable(ControllableObject& co, string& name, int value, bool bPublic = true),
			void setVariable(ControllableObject& co, string& name, float value, bool bPublic = true),
			void setVariable(ControllableObject& co, string& name, string value, bool bPublic = true),
			void setVariable(ControllableObject& co, string& name, bool value, bool bPublic = true) and
			void setVariable(ControllableObject& co, string& name, position3D value, bool bPublic = true) functions.

		@remarks If isPublic variable is false, this function does not globally effected.

		*/
		static void applyVariable(ControllableObject& co, const std::string& name, const VBSVariable& val, bool isPublic);
	
		/*!
		@description

		Allows individual movement control of group members, without having them fall back into formation.
		Applies to: group members on foot. 	

		@locality

		Locally applied, Locally effected

		@version [VBSFusion v3.20]

		@param co - ControllableObject that variable is assigned to.
		@param pos - destination to move to

		@return Nothing.

		@example

		@code

		Unit unit1;
		position3D newPos;

		VariableUtilities::applyForceMove(unit1,newPos);

		@endcode

		@overloaded 

		Nothing.

		@related

		@remarks This is a replication of 
			 void setForceMove(ControllableObject& co, position3D pos)
		*/
		static void applyForceMove(ControllableObject& co, const position3D& pos);
	};
};

#endif
