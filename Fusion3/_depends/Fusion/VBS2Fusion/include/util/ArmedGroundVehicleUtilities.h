/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

ArmedGroundVehicleUtilities.h

Purpose:

This file contains the declaration of the ArmedGroundVehicleUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			29-01/2010	YFP: Original Implementation
2.01		12-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_ARMEDGROUNDVEHICLEUTILITIES_H
#define VBS2FUSION_ARMEDGROUNDVEHICLEUTILITIES_H

/**************************************************************************
To disable the warnings arise because of using deprecated ArmedGroundVehicle 
inside the ArmedGroundVehicleUtilities class.
Warning identifier [C:4996]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4996)

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "data/ControllableObject.h"
#include "data/ArmedGroundVehicle.h"
#include "util/VehicleUtilities.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBSFusion
{

	class VBSFUSION_API VBSFUSION_DPR(UAGV) ArmedGroundVehicleUtilities : public VehicleUtilities
	{
	public:

		/*!
			Creates a new vehicle as defined by the parameters present in vehicle. 
		*/
		static void createVehicle(ArmedGroundVehicle& vehicle);

		/*!
			Updates the armed ground vehicle. 			
		*/
		static void updateVehicle(ArmedGroundVehicle& vehicle);

		/*!
			Disable the turret of the armed ground vehicle specified by the turret path.
			if the status is true gunner input will be disabled. 
			Deprecated.Use void applyGunnerInputDisable(ArmedGroundVehicle& vehicle, string strTurretpath , bool status) 
		*/
		VBSFUSION_DPR(UAGV001) static void disableGunnerInput(ArmedGroundVehicle& vehicle, std::string strTurretpath , bool status);

		/*!
			Armed ground vehicle fires from the specifically named weapon. 
			Deprecated.Use void applyFireFrom(ArmedGroundVehicle& vehicle, string weaponName)
		*/
		VBSFUSION_DPR(UAGV002) static void fire(ArmedGroundVehicle& vehicle, std::string weaponName);

		/*!
			Fire at the specified object. 
			Deprecated.Use void applyFire(ArmedGroundVehicle& vehicle,ControllableObject& object)
		*/
		VBSFUSION_DPR(UAGV003) static void doFire(ArmedGroundVehicle& vehicle,ControllableObject& object);

		/*!
			Order armed ground vehicle to fire at the specified object.
			Deprecated.Use void applyFireCommand(ArmedGroundVehicle& vehicle,ControllableObject& object)
		*/
		VBSFUSION_DPR(UAGV004) static void commandFire(ArmedGroundVehicle& vehicle,ControllableObject& object);


		/*!
			Change the direction of turret specified by the turret path of the Armed ground vehicle to target the 
			Controllable object specified.
		*/
		static void applyWeaponDirection(ArmedGroundVehicle& vehicle, const std::string& strTurretpath,const ControllableObject& object);

		/*!
			Apply Azimuth and elevation of the weapon of given turret in the armed ground vehicle.
			If the transition is TRUE then the weapon moves with transition else Direction changes without a transition.
		*/
		static void applyWeaponDirection(ArmedGroundVehicle& vehicle, const std::string& strTurretpath , double azimuth, double elevation, bool transition);

		/*!
			Returns turret elevation of the armed vehicle for a given target at specified target position.
		*/
		static double getElevation(ArmedGroundVehicle& vehicle, const std::string& strTurretpath, const position3D& targetPos);

		/*!
			Returns turret elevation of the armed vehicle for a given target specified as Controllable object.
		*/
		static double getElevation(ArmedGroundVehicle& vehicle, const std::string& strTurretpath, const ControllableObject& object);

		/*!
			Returns turret azimuth of the armed vehicle for a given target at specified target position.
		*/
		static double getAzimuth(ArmedGroundVehicle& vehicle, const std::string& strTurretpath, const position3D& targetPos);

		/*!
			Returns turret azimuth of the armed vehicle for a given target specified as Controllable object.
		*/
		static double getAzimuth(ArmedGroundVehicle& vehicle, const std::string& strTurretpath, const ControllableObject& object);

		/*!
			Apply the network shell mode. Normally bullets are locally simulated following a fired event.
			When network shell mode is set, the simulation is broadcast to the network. 
			Deprecated.Use void applyNetworkShellMode(ArmedGroundVehicle& vehicle,bool mode)
		*/
		VBSFUSION_DPR(UAGV005) static void setNetworkShellMode(ArmedGroundVehicle& vehicle,bool mode);
		/*!
		@description  

		Disable the turret of the armed ground vehicle specified by the turret path.
		if the status is true gunner input will be disabled.

		@locality
		
		Locally applied, Globally affected 

		@version [VBSFusion v3.10] 

		@param vehicle - ArmedGroundVehicle with turret.
		
		@param strTurretpath - String representation of Turretpath to disabled the gunner input.

		@param mode - If 'true' sets then AI/Player will not control turret anymore. If
		it sets to 'false' then AI/Player will control turret.

		@return nothing

		@example

		//Create a vehicle
		
		ArmedGroundVehicle vehicle1;

		//Create Turret
		
		Turret turret;

		//Create Turret path

		Turret::TurretPath turretPath

		//Define Turret location
		
		turretPath.push_back(0)

		turret.setTurretPath(turretPath)

		//Call applyGunnerInputDisable function. if status is true, turret will disable.  
		
		ArmedGroundVehicleUtilities::applyGunnerInputDisable(vehicle1, turretPath.getTurretpathString, true); 

		//Call applyGunnerInputDisable function. If it's false, turret will enable.

		ArmedGroundVehicleUtilities::applyGunnerInputDisable(vehicle1, turretPath.getTurretpathString, false);

		@code

		@overloaded 

		@related 

		@remarks  This is a replication of void disableGunnerInput(ArmedGroundVehicle& vehicle, string strTurretpath , bool status)
		*/
		static void applyGunnerInputDisable(ArmedGroundVehicle& vehicle, const std::string& strTurretpath , bool status);
	
		/*!
		@description  

		Armed ground vehicle fires from the specifically named weapon only if weapon is activated.

		@locality
		
		Locally applied, Globally affected 

		@version [VBSFusion v3.10] 

		@param vehicle - ArmedGroundVehicle which consists the weapon that will be used to fire.

		@param weaponName - Name of the weapon which is fired from.

		@return nothing

		@example
		
		//Create a vehicle
		
		ArmedGroundVehicle vehicle1;
		
		// Call applyFireFrom Function to fire from vehicle1 using specified weapon
		
		ArmedGroundVehicleUtilities::applyFireFrom(vehicle1, "VBS2_us_m240_veh_coax");

		@code

		@overloaded 

		@related 

		@remarks  This is a replication of void fire(ArmedGroundVehicle& vehicle, string weaponName)
		*/
		static void applyFireFrom(ArmedGroundVehicle& vehicle, const std::string& weaponName);

		/*!
		@description  

		Fire at the specified Controllable Object from the ArmerdGround Vehicle.

		@locality
		
		Locally applied, Globally affected 

		@version [VBSFusion v3.10] 

		@param vehicle - ArmedGroundVehicle which is going to fire to given target object.

		@param object - Target controllable object such as Vehicle,Unit .etc.

		@return nothing

		@example
		
		//Create two vehicles
		
		ArmedGroundVehicle vehicle1;
		ArmedGroundVehicle vehicle2;
		
		// Call applyFire Function to fire from vehicle1 to vehicle2
		
		ArmedGroundVehicleUtilities::applyFire(vehicle1, vehicle2);
		
		@code

		@overloaded 

		@related 

		@remarks  This is a replication of void doFire(ArmedGroundVehicle& vehicle,ControllableObject& object)
		*/
		static void applyFire(ArmedGroundVehicle& vehicle,const ControllableObject& object);

		/*!
		@description  

		Order/Command armed ground vehicle to fire at the specified object.

		@locality
		
		Locally applied, Globally affected

		@version [VBSFusion v3.10] 

		@param vehicle - ArmedGroundVehicle which is going to fire to given target object.

		@param object - Target controllable object such as Vehicle,Unit .etc.

		@return nothing

		@example

		@code
		
		//Firing Vehicle and Gunner
		ArmedGroundVehicle vehicle1;
		Unit gunner;

		//Set Gunner
		VehicleUtilities::applyMoveInAsGunner(vehicle1, gunner);

		//Create Target, may be Unit or Vehicle
		Unit target;

		//call applyFireCommand on vehicle1 instructing it to fire on Unit Target
		ArmedGroundVehicleUtilities::applyFireCommand(vehicle1,target);

		@endcode
		
		@overloaded 

		@related 

		@remarks  This is a replication of void commandFire(ArmedGroundVehicle& vehicle,ControllableObject& object)
		*/
		static void applyFireCommand(ArmedGroundVehicle& vehicle,const ControllableObject& object);

		/*!
		@description  

		Apply the network shell mode. Normally bullets are locally simulated following a fired event.

		@locality

		Locally applied, Locally affected

		@version [VBSFusion v3.15] 

		@param vehicle - ArmedGroundVehicle which is going to fire to given target object.

		@param mode - When network shell mode is set(true), the simulation is broadcast to the network.

		@return nothing

		@example

		@code
		
		//Create ArmedGroundVehicle
		ArmedGroundVehicle vehicle1;

		//Call applyNetworkShellMode on vehicle to set it
		ArmedGroundVehicleUtilities::applyNetworkShellMode(vehicle1,true);
		
		@endcode

		@overloaded 

		@related 

		@remarks  This is a replication of void setNetworkShellMode(ArmedGroundVehicle& vehicle,bool mode)
		*/
		static void applyNetworkShellMode(ArmedGroundVehicle& vehicle,bool mode);

	};
		
	

};
#pragma warning (pop) // Enable warnings [C:4996]

#endif //ARMEDGROUNDVEHICLEUTILITIES_H