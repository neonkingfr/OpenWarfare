/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	GroupListUtilities.h

Purpose:

	This file contains the declaration of the GroupListUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			01-04/2009	RMR: Original Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.012		13-09/2010  YFP: Modified loadGroups();

/************************************************************************/

#ifndef VBS2FUSION_GROUP_LIST_UTILITIES_H
#define VBS2FUSION_GROUP_LIST_UTILITIES_H

#include <sstream>
#include <string>


#include "position3D.h"
#include "VBSFusion.h"
#include "data/GroupList.h"
#include "util/GroupUtilities.h"
#include "util/GroupListUtilities.h"
#include "util/WaypointUtilities.h"


namespace VBSFusion
{
	class VBSFUSION_API GroupListUtilities
	{
	public:

		/*!
		Loads all groups in the VBS2 mission environment on to groupList. groupList is cleared before
		adding any members. 

		- Random aliases are assigned to each recognized group. 
		- Random aliases are assigned to each loaded unit. 
		- Random aliases are assigned to each loaded waypoint. 

		- Each group is updated once. 
		- Units are updated once
		- Waypoints are updated once. 
		*/

		static void loadGroups(GroupList& groupList);		

		/*!
		Updates the following for each group belonging to the group:

		- Group dynamic properties
		- Group membership
		- Member dynamic properties
		- Member static properties
		*/

		static void updateGroups(GroupList& groupList);

		/*!
		Updates the following for each group belonging to the group:

		- Group dynamic properties
		- Group membership
		- Member dynamic properties
		- Member static properties

		*/
		static void updateGroupList(GroupList& groupList);

		/*!
		Returns a unit object within playerUnit containing information and a handle
		for the VBS2 unit currently controlled by the player. 
		*/
		static void getPlayer(const GroupList& groupList, Unit& playerUnit);		

		

	};
};

#endif //GroupListUtilities
