/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

TriggerUtilities.h

Purpose:

This file contains the declaration of the TriggerUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			30-03/2009	RMR: Original Implementation
1.01		27-08/2009   SDS: Added updateIsLocal(Trigger& trig);	
							 Updated updateTrigger(Trigger& trig)		
1.02		02-08/2009		 Added getIDUsingAlias(Trigger& trig)
							 Updated getPosition(Trigger& trig)
2.0			10-02/2010	MHA: Comments Checked
/************************************************************************/

#ifndef VBS2FUSION_TRIGGER_UTILITIES_H
#define VBS2FUSION_TRIGGER_UTILITIES_H

#include <sstream>
#include <string>
#include <list>

#include "position3D.h"
#include "VBSFusion.h"
#include "data/Trigger.h"
#include "util/ExecutionUtilities.h"

namespace VBSFusion
{

	class VBSFUSION_API TriggerUtilities
	{
	public:

		//-------------------------------------------------------------------

		/*!
		@description

		This function returns the position of the trigger in position3D format within the VBS3 environment.

		@locality

		Globally Applied

		@version [VBSFusion v3.2.0]

		@param trig - The trigger for which its position is obtained. The trigger should be created before passing it as a parameter.

		@return position3D

		@example

		@code

		//Trigger to be created
		Trigger trigger1;

		displayString="The position of trigger1 is:  "+ (TriggerUtilities::getPosition(trigger1).getVBSPosition());

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::applyPosition(Trigger& trig, position3D position)

		@remarks - It is highly recommended to create triggers using TriggerUtilities: :createTrigger(Trigger& trig).

		@remarks - To obtain the position of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function getPosition(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		@remarks - This function uses the network ID of the object to access it within the game environment, so it is required to  assign registered alias or a string accessible name to trigger. 
		
		@remarks - Error checking utility validates the following:
		- NetworkID (outputs an error if the network ID is invalid)
		- Position (outputs an error if the position returned is invalid)
		*/

		static position3D getPosition(const Trigger& trig);		

		/*!
		@description

		This function applies the position to the trigger created within the VBS3 environment.

		@locality

		Globally Applied Globally Effected

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the position is applied. The trigger should be created before passing it as a parameter.
		
		@param position  The position to be applied to the trigger created.

		@return Nothing.

		@example

		@code

		//Trigger to be created

		Trigger trigger1;

		position3D position1 (2205,2096,0);
		trigger1.setPosition(position1);
		TriggerUtilities::applyPosition(trigger1,position1);

		@endcode

		@overloaded

		None.

		@related
		
		TriggerUtilities::updatePosition(Trigger& trig)

		@remarks - To apply the position of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function applyPosition(Trigger& trig, position3D position) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		@remarks - This function uses the network ID of the object to access it within  the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		@remarks - Error checking utility validates the following:
				 - NetworkID (outputs an error if the network ID is invalid)
				 - This method gets the parameter from the Fusion and applies it in VBS2.
		*/

		static void applyPosition(Trigger& trig, const position3D& position);

		/*!
		@description

		This method gets the details of the trigger from the VBS2  and updates the values in the Fusion end.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger which is updated.

		@return Nothing

		@example

		@code

		//Trigger to be created

		Trigger trigger1;
		TriggerUtilities::updatePosition(trigger1);

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::getPosition(Trigger& trig)

		@remarks To update the position of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function getPosition(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/

		static void updatePosition(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		@description

		This function returns the trigger statements applied on the trigger created within the VBS3 environment.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the statement is obtained. The trigger should be created before passing it as a parameter.

		@return string

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		displayString = "\n trigger statement = "+TriggerUtilities::getStatements(trigger1); 

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::UpdateStatements(Trigger& trig);

		@remarks - To obtain the statement of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function getStatements(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		@remarks - This function uses the network ID of the object to access it within the game environment, so it is required to assign registered alias or a string accessible name to trigger. 
		@remarks - Error checking utility validates the following:
				 - NetworkID (outputs an error if the network ID is invalid)

		*/
		static std::string getStatements(const Trigger& trig);		

		/*!
		@description

		This function defines a trigger's condition, and executable code for its activation and deactivation events.

		@locality

		Globally Applied Globally Effected

		@version [VBSFusion v3.2.0]

		@param trig -  The trigger for which the statement is applied.The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		Trigger1.setStatementsCond("this");
		Trigger1.setStatementsActiv("hint 'trigger on'");
		Trigger1.setStatementsDesactiv("hint 'trigger off'");
		TriggerUtilities::applyStatements(trigger1);

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::getStatements(Trigger& trig)

		@remarks - To apply a statement  to the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function applyStatements(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		@remarks - Error checking utility validates the following:
				   -NetworkID (outputs an error if the network ID is invalid)
		@remarks - The following functions need to be used before using this function as shown in the above sample code
					-setStatementsCond(string condition);
					-setStatementsActiv(string activation);
					-setStatementsDesactiv(string deactivation);	
					where the parameters are: 
					-condition: String - Code containing the trigger's condition, which has to return a boolean value. If this is used, the result of the trigger's [activation condition] is interpreted.
					-activation: String - Code that is executed when the trigger is activated (The variable thislist contains an array with the units that activated the trigger.)
					-deactivation: String - Code that is executed when the trigger is deactivated.
		*/

		static void applyStatements(const Trigger& trig);	

		/*!
		@description

		Updates all trigger statements for the trigger created within the VBS3 environment in the Fusion end. 

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig- The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return Nothing.

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::updateStatements(trigger1);

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::getupdateStatements(Trigger& trig)

		@remarks - To update the statement  of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function updateStatements(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger.
		@remarks - Error checking utility validates the following:
				   -NetworkID (outputs an error if the network ID is invalid).
		*/
		static void updateStatements(Trigger& trig);	

		//-------------------------------------------------------------------

		/*!
		@description

		Returns the activation statements for the trigger created within the VBS3 environment.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the activation is obtained.The trigger should be created before passing it as a parameter.

		@return string

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		displayString+= "The activation type is: "+ (TriggerUtilities::getActivation(trigger1));

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::applyActivation(Trigger& trig)

		@remarks - To obtain the activation  of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function getActivation(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		@remarks - Error checking utility validates the following:
					-NetworkID (outputs an error if the network ID is invalid)

		*/
		static std::string getActivation(const Trigger& trig);

		/*!
		@description

		Applies the activation statements to the trigger created within the VBS3 environment. This method gets the  parameter from Fusion and then apply it to VBS3.

		@locality

		Globally Applied Locally Effected

		@version [VBSFusion v3.2.0]

		@param trig- The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		trigger1.setTriggerActivation("ALPHA");
		trigger1.setTriggerPresence(string("PRESENT"));
		trigger1.setRepeating(true);
		TriggerUtilities::applyActivation(trigger1);

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::getActivation(Trigger& trig)

		@remarks - The following functions are used to edit trigger parameters before function applyActivation(Trigger& trig)is used.
		- setTriggerActivation(string by );
		- setTriggerPresence(string Type);
		- setRepeating(bool repeating);
		@remarks -This apply changes to the following parameters in VBS3 from Fusion 
		by: String - Who activates trigger. Can be "NONE" or

		Side': "EAST", "WEST", "GUER", "CIV", "LOGIC", "ANY"
		Or
		Radio: "ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", "HOTEL", "INDIA", "JULIET"
		Or
		Object: "STATIC", "VEHICLE", "GROUP", "LEADER", "MEMBER"
		Or
		Status: "WEST SEIZED", "EAST SEIZED" or "GUER SEIZED"

		type: String - How trigger is it activated. Can be:

		Presence: "PRESENT", "NOT PRESENT"
		Or
		Detection: "WEST D", "EAST D", "GUER D" or "CIV D"	

		repeating: Boolean - Activation can happen repeatedly	if the Boolean value is true.	

		@remarks - To apply activation of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyActivation(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		@remarks - Error checking utility validates the following:
		-NetworkID (outputs an error if the network ID is invalid)
		*/		
		static void applyActivation(const Trigger& trig);

		/*!
		@description

		Updates the trigger activation for the trigger created within the VBS3 environment in the VBSFusion end.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig- the trigger for which the above function is applied.  The trigger should be created before passing it as a parameter

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::updateActivation(trigger1);

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::updateActivation(Trigger& trig);

		@remarks - To update the activation of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::updateActivation(Trigger& trig)(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 

		@remarks - Error checking utility validates the following:
				 -NetworkID (outputs an error if the network ID is invalid)

		*/
		static void updateActivation(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		@description

		This function returns the area of the trigger created within the VBS3 environment.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return string 

		@example

		@code

		//Trigger to be created
		Trigger trigger1
		displayString+="The area is:  "+ TriggerUtilities::getArea(trigger1);

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::applyArea(Trigger& trig);

		@remarks To obtain the area of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities:: getArea(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		@remarks This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		@remarks  - Error checking utility validates the following:
					NetworkID (outputs an error if the network ID is invalid)

		*/
		static std::string getArea(const Trigger& trig);

		/*!
		@description

		This function applies the area for the trigger created within the VBS3 environment. Defines the area controlled by the trigger. The area is either rectangular or elliptical, the width is 2 * xrad, the height is 2 * yrad. It is rotated by angle degrees.

		@locality

		Globally Applied Locally Effected

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;

		trigger1.setAngle(25.2);
		trigger1.setRectangular(true);
		trigger1.setWidth(23.5);
		trigger1.setHeight(20.6)
		TriggerUtilities::applyArea(trigger1);

		@endcode

		@overloaded
		
		None.

		@related

		TriggerUtilities::getArea(Trigger& trig);
		TriggerUtilities::updateArea(Trigger& trig);

		@remarks - The functions below should be used before the applyArea(Trigger& trig)
					setAngle(double angle);
					setWidth(double width); 
					setHeight(double height);
					setRectangular(boolean rectangular);
				Where the parameters: 
					angle- Rotation angle of trigger
					width - half the width of the trigger area
					height- half the height of the trigger area
					rectangular - a boolean value true will create a rectangular area.
		@remarks - To apply the area for the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyArea(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		@remarks - Error checking utility validates the following:
					NetworkID (outputs an error if the network ID is invalid)
		*/		
		static void applyArea(const Trigger& trig);

		/*!
		@description

		This function updates the area of triggers created within the VBS3 environment in the Fusion end.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig -   The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::updateArea(trigger1);

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::getArea(Trigger& trig)

		@remarks - To update the area applied for  the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::updateArea(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		@remarks - Error checking utility validates the following:
					NetworkID (outputs an error if the network ID is invalid)	

		*/
		static void updateArea(Trigger& trig);
		//-------------------------------------------------------------------
		
		/*!
		@description

		This function returns the TimeOut of the trigger created within the VBS3 environment.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return string 
 
		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		displayString="The Timeout is: "+(TriggerUtilities::getTimeout(trigger1));

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::updateTimeout(Trigger& trig);

		@remarks - To obtain the Timeout of  the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::getTimeout(Trigger& trig).(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 

		@remarks - Error checking utility validates the following:
				NetworkID (outputs an error if the network ID is invalid)
		
		*/		
		static std::string getTimeout(const Trigger& trig);

		/*!
		@description

		Applies the Timeout for the trigger created within the VBS3 environment.

		@locality

		Globally Applied Locally Effected

		@version [VBSFusion v3.2.0]
 
		@param trig -   The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;

		trigger1.setTimeoutMax(21.2);
		trigger1.setTimeoutMid(15.2);
		trigger1.setTimeoutMin(10.2);
		TriggerUtilities::applyTimeout(trigger1);

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::getTimeout(Trigger& trig);

		@remarks - The functions below should be used to set the Timeout before the applyTimeout(Trigger& trig) function is used.
		setTimeoutMax(double val);
		setTimeoutMid(double val);
		setTimeoutMin(double val);

		@remarks - To apply the Timeout for  the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyTimeout(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		
		@remarks - Error checking utility validates the following:
				   NetworkID (outputs an error if the network ID is invalid)
		*/
		static void applyTimeout(const Trigger& trig);

		/*!
		@description

		Updates the Timeout of the trigger created within the VBS3 environment in the Fusion end.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied.  The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::updateTimeout(trigger1);

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::applyTimeout(Trigger& trig);

		@remarks - To update the Timeout  of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::updateTimeout(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		
		@remarks - Error checking utility validates the following:
				   NetworkID (outputs an error if the network ID is invalid)
		*/
		static void updateTimeout(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		@description

		This function obtains the text of the trigger created within the VBS3 environment.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig -    The trigger for which the above function is applied.  The trigger should be created before passing it as a parameter.

		@return string   

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		displayString+="The text of trig is:"TriggerUtilities::getText(trigger1);

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::applyText(Trigger& trig);

		@remarks - To obtain the text  of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::getText(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		
		@remarks - Error checking utility validates the following:
				   NetworkID (outputs an error if the network ID is invalid)
					
		*/
		static std::string getText(const Trigger& trig);

		/*!
		@description

		This function applies the text to the trigger created within the VBS3 environment.

		@locality

		Globally Applied Locally Effected

		@version [VBSFusion v3.2.0]

		@param trig - The trigger for which the above function is applied.  The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;

		//set the text to be applied on the trigger//

		trigger1.setText(string("Hello"));
		TriggerUtilities::applyText(trigger1);

		@endcode

		@overloaded

		TriggerUtilities::applyText(Trigger& trig, string text);

		@related

		@remarks - The function below is applied to set the Text before the  function applyText(Trigger& trig)is used.
		setText(string text);
		
		@remarks - To apply a text for the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyText(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		
		@remarks - Error checking utility validates the following:
				 -NetworkID (outputs an error if the network ID is invalid)
		*/		
		static void applyText(const Trigger& trig);

		/*!
		@description

		This function applies the text to the trigger created within the VBS3 environment.

		@locality

		Globally Applied Locally Effected

		@version [VBSFusion v3.2.0]

		@param trig - The trigger for which the above function is applied.  The trigger should be created before passing it as a parameter.

		@param text- The text to be applied on the trigger.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::applyText(trigger1,string("Hello Trigy"));

		@endcode

		@overloaded

		TriggerUtilities::applyText(Trigger& trig);

		@related

		TriggerUtilities::getText(Trigger& trig);

		@remarks - To apply a text  for the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyText(Trigger& trig, string text); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		
		@remarks - Error checking utility validates the following:
				   -NetworkID (outputs an error if the network ID is invalid)
		*/
		static void applyText(Trigger& trig, const std::string& text);

		/*!
		@description

		Updates the text of the trigger created within the VBS3 environment in the Fusion end.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig- The trigger for which the above function is created.   The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::updateText(trigger1);

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::applyText(Trigger& trig)

		@remarks - To update the text  of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::updateText(Trigger& trig)(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks - This method gets the parameters from VBS3 and updates the Fusion parameters. 
		*/
		static void updateText(Trigger& trig);

		/*!
		@description

		Applies the alias of the trigger object if the applied alias is not  been assigned to another object in the mission.

		@locality

		Globally Applied Locally Effected

		@version [VBS3Fusion 2.54]

		@param trig- The trigger for which the above function is applied.  The trigger should be created before passing it as a parameter.

		@return

		Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		trigger1.setAlias(string("Trigger1"));
		TriggerUtilities::applyAlias(trigger1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - The function setalias(string text)is applied to set the alias  before the function applyAlias(Trigger& trig)is used.
		
		@remarks - To apply an alias for the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyAlias(Trigger& trig)(Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void applyAlias(const Trigger& trig);

		/*!
		@description

		Returns the name of the variable which contains a primary editor reference to this trigger object. 

		@locality

		Globally applied

		@version [VBS3Fusion v2.54]

		@param trig -  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return string  

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		displayString+="var name:"+TriggerUtilities::getVarName(trigger1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - To obtain the var name of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::getVarName(Trigger& trig)(Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		*/
		static std::string getVarName(const Trigger& trig);

		/*!
		@description

		Returns the type of the trigger created within the VBS3 environment.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return string

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		displayString="The trigger type is:  "+(TriggerUtilities::getType(trigger1));

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::applyType(Trigger& trig);

		@remarks - To obtain the type of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::getType(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		
		@remarks - Error checking utility validates the following:
				 -NetworkID (outputs an error if the network ID is invalid)
		*/
		static std::string getType(const Trigger& trig);

		/*!
		@description

		This function applies the type to the trigger created within the VBS3 environment.

		@locality

		Globally Applied Globally Effected

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@param type  The type to be applied on the trigger.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::applyType(trigger1,"SWITCH");

		@endcode

		@overloaded

		TriggerUtilities::applyType(Trigger& trig);

		@related

		TriggerUtilities::getType(Trigger& trig);

		@remarks - To apply the type for the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyType(Trigger& trig, string type); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 

		@remarks  - Error checking utility validates the following:
					-NetworkID (outputs an error if the network ID is invalid)

		*/
		static void applyType(const Trigger& trig, const std::string& type);

		/*!
		@description

		This function applies the type to the trigger created within the VBS3 environment. This function sets the type of action processed by the trigger after activation (no action, a waypoints switch or an end of mission): 
		The following is a list of types available:
		-	"NONE"
		-	"EAST G" - Guarded by OPFOR
		-	"WEST G" - Guarded by BLUFOR
		-	"GUER G" - Guarded by Independent
		-	"SWITCH" - Switch waypoints/break loop
		-	"END1"	 - End #1
		-	"END2"	 - End #2
		-	"END3"	 - End #3
		-	"END4"	 - End #4
		-	"END5"	 - End #5
		-	"END6"	 - End #6
		-	"LOOSE"	 - Lose

		@locality

		Globally Applied Locally Effected

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		trigger1.setTriggerType(END6);
		TriggerUtilities::applyType(trigger1);

		@endcode

		@overloaded

		TriggerUtilities::applyType(Trigger& trig, string type);

		@related

		TriggerUtilities::applyType(Trigger& trig);

		@remarks -The function setTriggerType(string text) is applied to set the Type before the function applyType(Trigger& trig) is used.
		
		@remarks  - To apply the type for the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyType(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		@remarks  - This method gets parameters from Fusion and apply it to VBS3.

		*/		
		static void applyType(const Trigger& trig);

		/*!
		@description

		Updates the type of the trigger created within the VBS3 environment in the Fusion end.

		@locality

		Globally applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::updateType(trigger1);

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::getType(Trigger& trig);

		@remarks - To update the type for the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::updateType(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		@remarks - This method gets the parameters from VBS3 and updates the Fusion parameters.	

		*/
		static void updateType(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		@description

		Updates the activation status Boolean of the trigger created within the VBS3 environment in the Fusion end.

		@locality

		Globally Applied

		@version [VBSFusion v3.2.0]

		@param trig- The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return  Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::updateActivated(trigger1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - To update the activation of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::updateActivated(Trigger& trig)(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		@remarks - This function uses the network ID of the object to access it within the game environment, so please	ensure that either a registered alias or a string accessible name is assigned to trigger. 

		@remarks -Error checking utility validates the following:
				  NetworkID (outputs an error if the network ID is invalid)

		@remarks -This method gets the parameters from VBS3 and updates the Fusion parameters.

		*/
		static void updateActivated(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		@description

		Updates the following trigger properties  in the Fusion end.

		- Activation Statements
		- Area
		- Position
		- Statements
		- Text
		- Timeout values
		- Activation

		@locality

		Globally Applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.@return  Nothing

		@return Nothing.

		@example

		@code

		Trigger trigger1;
		TriggerUtilities::updateTrigger(trigger1);

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::createTrigger(Trigger& trig);

		@remarks  - To update the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::updateTrigger(Trigger& trig),(Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		
		@remarks  - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		
		@remarks  -  Error checking utility validates the following:
					-NetworkID (outputs an error if the network ID is invalid)
		*/
		static void updateTrigger(Trigger& trig);

		/*!
		@description

		Applies changes to all the values within the following to the trigger created within the VBS3 environment. 
		- Activation Statements
		- Area
		- Position
		- Statements3
		- Text	
		- Timeout values
		- Activation

		@locality

		Locally Effected

		@version [VBSFusion v3.2.0]

		@param trig -   The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return

		Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		trigger1.setTimeoutMax(12.2);
		trigger1.setTimeoutMid(5.2);
		trigger1.setTimeoutMin(2.2);
		trigger1.setText(string("trigger1"));
		position3D position1 (3000,3096,0);
		trigger1.setPosition(position1);
		TriggerUtilities::applyChanges(trigger1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - To apply changes for the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyChanges(Trigger& trig),Refer (MissionUtilities::loadMission(Mission& mission)for further information)
	
		*/

		static void applyChanges(Trigger& trig);		

		/*!
		Returns the network ID of an object using its alias. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias returned is invalid)
		*/
		//static string getIDUsingAlias(Trigger& trig);

		//-------------------------------------------------------
		// NetworkId utility
		//-------------------------------------------------------

		/*!
		@description

		Updates the "IsLocal" property of the trigger created within the VBS3 environment to know whether it has a valid network id in the Fusion end.

		@locality

		Globally Applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::updateIsLocal(trigger1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - To apply changes for the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::updateIsLocal(Trigger& trig),Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static void updateIsLocal(Trigger& trig);	
		
		/*!
		@description

		Updates the network ID of an object using its alias in the Fusion end.

		@locality

		Globally Applied

		@version [VBSFusion v3.2.0]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::updateTriggerID(trigger1);

		@endcode

		@overloaded

		None 

		@related

		None

		@remarks - To update the ID  of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::updateTriggerID(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 
		
		@remarks - Error checking utility validates the following:
				   -NetworkID (outputs an error if the network ID is invalid)
		*/

		static void updateTriggerID(Trigger& trig);

		//-------------------------------------------------------------------	

		/*!
		@description

		This function deletes the trigger created within the VBS3 environment.

		@locality

		Globally applied Globally Effected

		@version [VBSFusion v3.2.0]

		@param trig  The trigger to be deleted. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::deleteTrigger(trigger1);

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::createTrigger(Trigger& trig);

		@remarks - To delete  the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::deleteTrigger(Trigger& trig) (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		@remarks - This function uses the network ID of the object to access it within the game environment, so please ensure that either a registered alias or a string accessible name is assigned to trigger. 

		@remarks  Error checking utility validates the following:
				 -NetworkID (outputs an error if the network ID is invalid)
		*/
		static void deleteTrigger(Trigger& trig);

		//-------------------------------------------------------------------	

		/*!
		@description

		Creates a trigger within the VBS3 environment specified by the input trigger objects.

		@locality

		Globally Applied Globally effected

		@version [VBSFusion v3.2.0]

		@param trig- The trigger to be created. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;

		trigger1.setName(string("trig"));
		TriggerUtilities::createTrigger(trigger1);	
		TriggerUtilities::updateTrigger(trigger1);

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::deleteTrigger(Trigger& trig);

		@remarks
		*/
		static void createTrigger(Trigger& trig);
	
		/*!
		Sets optional script that is executed when the trigger activates. 
		Deprecated. Use double applyOnActivationStatement(Trigger& trigger, string command)
		*/
		VBSFUSION_DPR(UTGR001) static double addTriggerOnActivated(const Trigger& trigger, const std::string& command);

		/*!
		@description

		Creates a trigger locally within the VBS3 environment.

		@locality

		Locally Effected

		@version [VBSFusion v2.50]

		@param type- The type of the trigger.

		@param position- The position of the trigger.

		@return trigger

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::createTriggerLocal("EmptyDetector",position3D(2700,2800,10));

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - To create a trigger  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::createTriggerLocal(string type, position3D position),Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		
		*/
		static Trigger createTriggerLocal(const std::string& type, const position3D& position);

		//static void removeTriggerOnActivated(Trigger trig, double index);

		/*!
		Defines the trigger activation type. See Mission Editor - Triggers for a thorough overview 
		of triggers and its fields for activation, effects, etc.

		by: String - Who activates trigger. Can be "NONE" or

		Side': "EAST", "WEST", "GUER", "CIV", "LOGIC", "ANY"
		Radio: "ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", "HOTEL", "INDIA", "JULIET"
		Object: "STATIC", "VEHICLE", "GROUP", "LEADER", "MEMBER"
		Status: "WEST SEIZED", "EAST SEIZED" or "GUER SEIZED"

		type: String - How trigger is it activated. Can be:

		Presence: "PRESENT", "NOT PRESENT"
		Detection: "WEST D", "EAST D", "GUER D" or "CIV D"

		repeating: Boolean - Activation can happen repeatedly

		Deprecated. Use void applyActivationType(Trigger& trig, string& by, string& type, bool repeat) 
		*/
		VBSFUSION_DPR(UTGR003) static void setTriggerActivation(Trigger& trig, const std::string& by, const std::string& type, bool repeat);

		/*!
		Returns all objects attached to the trigger
		Deprecated. Use list<ControllableObject> getAttachedObjects(Trigger& trig)
		*/
		VBSFUSION_DPR(UTGR004) static std::list<ControllableObject> triggerAttachedObj(const Trigger& trig);

		/*!
		Detaches a game object from a trigger.
		Deprecated. Use applyObjectDetach(Trigger& trig, ControllableObject& co)
		*/
		VBSFUSION_DPR(UTGR006) static void triggerDetachObj(const Trigger& trig, const ControllableObject& obj);

		/*!
		Assigns any game object to a trigger. 
		Deprecated. Use void applyObjectAttach(Trigger& trig, ControllableObject& co)
		*/
		VBSFUSION_DPR(UTGR005) static void triggerAttachObj(const Trigger& trig, const ControllableObject& co);

		/*!
		Synchronizes a trigger with other list of waypoints.		
		Deprecated. Use void applySynchronization(Trigger& trig, list<Waypoint> wplist)
		*/
		VBSFUSION_DPR(UTGR007) static void synchronizeTrigger(const Trigger& trig, const std::list<Waypoint>& wplist);

		/*!
		@description

		Returns the statement that is executed when a trigger or way point is reached.

		@locality

		Globally Applied

		@version [VBS3Fusion v3.02]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return string 

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		displayString="The EffectCondition is:  " +TriggerUtilities::getEffectCondition(trigger1);

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::applyEffectCondition(Trigger& trig, string& statement)

		@remarks - To obtain the EffectCondition of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::getEffectCondition(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information).
		*/
		static std::string getEffectCondition(const Trigger& trig);
		/*!
		The statement is executed when the trigger is activated and the effects are launched
		depending on the result.
			- If the result is a boolean and true, the effect is launched.
			- If the result is an object, the effect is launched if the result is the player or
			  the player vehicle.
			- If the result is an array, the effect is launched if the result contains the player
			  or the player vehicle. 

		Deprecated. Use void applyEffectCondition(Trigger& trig, string& statement)
		*/
		VBSFUSION_DPR(UTGR011)static void setEffectCondition(const Trigger& trig, const std::string& statement);

		/*!
		Assign the trigger that is defined by the passed network id.
			trig - Trigger reference
			id - Network ID

		Note : Can assign the network id to trigger data object by setID(NetworkID nid) then
			use the update function in TriggerUtilities.
		*/
		VBSFUSION_DPR(UTGR012) static void networkIdToTrigger(Trigger& trig, const NetworkID& id);

		/*!
		Assign the trigger that is defined by the passed network id.
			trig - Trigger reference
			id - Network ID in string format ("2:1:3")

		Note : Can assign the network id to trigger data object by setID(string triggerID) then
			use the update function in TriggerUtilities.
		*/
		VBSFUSION_DPR(UTGR012) static void stringIdToTrigger(Trigger& trig, const std::string& id);

		/*!
		Defines the music track played on activation. Track is a subclass name of CfgMusic. 
		In addition, "$NONE$" (no change) or "$STOP$" (stops the current music track). 		
		Deprecated. Use void applyMusicEffect(Trigger& trig, string& trackName)
		*/
		VBSFUSION_DPR(UTGR008) static void setMusicEffect(const Trigger& trig, const std::string& trackName);

		/*!
		@description

		Returns the defined music track played on activation on a trigger. Track is a subclass name of CfgMusic or "$NONE$" or "$STOP$".  

		@locality

		Globally Applied

		@version [VBS3Fusion v3.03]

		@param trig- The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return string

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::updateTrigger(trigger1);
		string st;
		vector<string> vec;
		vec = TriggerUtilities::getMusicEffect(trigger1);
		for (vector<string>::iterator itr=vec.begin();itr!=vec.end();itr++)
		{
		st=*itr;
		displayString+=st;
		}
		string s = "";

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::applyMusicEffect(Trigger& trig, string& trackName);

		@remarks - To obtain the musical effect of the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::getMusicEffect(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
				
		*/
		static std::string getMusicEffect(const Trigger& trig);

		/*!
		Defines the different sound effects.
		Sound / voice plays a 2D / 3D sound from CfgSounds.
		SoundEnv plays an enviromental sound from CfgEnvSounds.
		SoundDet (only for triggers) creates a dynamic sound object attached to a trigger defined in CfgSFX. 
		Deprecated. Use void applySoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet)
		*/
		VBSFUSION_DPR(UTGR009) static void setSoundEffect(const Trigger& trig, const std::string& sound, const std::string& voice, const std::string& soundEnv, const std::string& soundDet);
	
		/*!
		@description

		Returns the sound, voice, soundEnv, and soundDet for a trigger.

		@locality

		Globally Applied

		@version [VBS3Fusion v3.03]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return vector<string>

		@example

		@code

		//Trigger to be created
		Trigger trigger1;

		string st;
		vector<string> vec;
		vec = TriggerUtilities::getSoundEffect(trigger1);
		for (vector<string>::iterator itr=vec.begin();itr!=vec.end();itr++)
		{
		st=*itr;
		displayString+=st;
		}
		string s = "";


		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::applySoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet);

		@remarks - To obtain the soundEffect of the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities:: getSoundEffect(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information;
		*/
		static std::vector<std::string> getSoundEffect(const Trigger& trig);

		/*!
		Defines the title effect via [Type, Effect, Text] where

		'Type' can be,
			- "NONE",
			- "OBJECT",
				'Text' defines the shown object , a subclass of CfgTitles. 
			- "RES"
				'Text' defines a resource class, a subclass of RscTitles. 
			- "TEXT"
				The 'Text' is shown as text itself.
				
		'Effect' defines a subtype: 
			"PLAIN", "PLAIN DOWN", "BLACK", "BLACK FADED", "BLACK OUT", 
			"BLACK IN", "WHITE OUT" or "WHITE IN". 

		Deprecated. Use void applyTitleEffect(Trigger& trig, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text)
		*/
		VBSFUSION_DPR(UTGR010) static void setTitleEffect(const Trigger& trig, const std::string& type, const std::string& effect, const std::string& text);

		/*!
		@description

		Returns the defined title effect of the  trigger created within the VBS3 environment. 
		It returns	'type' -which can be "NONE", "OBJECT", "RES" or "TEXT".
					'effect' -returns the selected Title Effect Type for type "TEXT".
					'text' returns the shown object (a subclass of CfgTitles) for type "OBJECT", the resource class (a subclass of RscTitles) for type "RES", and the plain text string for type "TEXT"

		@locality

		Globally Applied

		@version [VBS3Fusion v3.03]

		@param trig- The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return vector<string>

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		string st;
		vector<string> vec;
		vec = TriggerUtilities::getTitleEffect(trigger1);
		for (vector<string>::iterator itr=vec.begin();itr!=vec.end();itr++)
		{
		st=*itr;
		displayString+=st;
		}
		string s = "";

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::applyTitleEffect(Trigger& trig, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text);

		@remarks  - To obtain the TitleEffect of the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::getTitleEffect(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information;
		*/
		static std::vector<std::string> getTitleEffect(const Trigger& trig);

		/*!
		@description

		Returns the list of units that would activate a given Trigger.For trigger of type "Not present" the list is the same as that returned for "present". 

		@locality

		Globally Applied

		@version [VBS3Fusion v3.03]

		@param trig  The trigger for which the above function is applied. The trigger should be created before passing it as a parameter.

		@return vector<Unit>

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		vector<Unit> unitTrigList;
		unitTrigList = TriggerUtilities::getListOfUnits(trigger1);
		for (std::vector<Unit>::iterator uitr=unitTrigList.begin();uitr != unitTrigList.end();uitr++)
		{
		displayString += "s+u" + (*uitr).getName();
		}

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - To obtain the list of units that activate the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function getListOfUnits(Trigger& trig); (Refer (MissionUtilities::loadMission(Mission& mission)for further information.		
		*/
		static std::vector<Unit> getListOfUnits(const Trigger& trig);

		/*!
		Clears optional script that is executed when the trigger activates.
		Deprecated. Use void applyOnActivationStatementRemoval(Trigger& trigger, double index)
		*/
		VBSFUSION_DPR(UTGR002) static void removeTriggerOnActivated(const Trigger& trigger, double index);

		/*!
		Specifies the entity which will activate the selected trigger.
		Deprecated. Use void applyVehicleAttach(Trigger& trig, ControllableObject& co)
		*/
		VBSFUSION_DPR(UTGR014) static void triggerAttachVehicle(const Trigger& trig, const ControllableObject& co);

		/*!
		@description

		Applies VBS3 script command that is executed when the trigger activates.

		@locality

		Globally Applied Locally Effected

		@version [VBSFusion v3.2.0]

		@param trigger - Trigger for which the VBS3 script command is applied to. The trigger should be created before passing it as a parameter.

		@param command - VBS3 script that is executed when the trigger activates. 

		@return double 

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		//Trigger Name//
		string name = "12789: <no shape>";
		trigger.setName(name);

		ID = TriggerUtilities::applyOnActivationStatement(trigger1, "player sideChat \"Activated.\"");

		@endcode

		@overloaded

		None

		@related

		@remarks - Network ID of the trigger should be set as the trigger name.

		@remarks - Trigger activation should be set asAnyBody.

		@remarks  - To apply onActivationStatement for  the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyOnActivationStatement(Trigger& trigger, string command); (Refer (MissionUtilities::loadMission(Mission& mission)for further information;

		@remarks - This is a replication of double addTriggerOnActivated(Trigger& trigger, string command) function.
		*/

		static double applyOnActivationStatement(Trigger& trigger, const std::string& command);

		/*!
		@description

		Clears optional script that is executed when the trigger activates .

		@locality

		Globally Applied Locally Effected

		@version [VBSFusion v3.2.0]

		@param trigger - Trigger that VBS3 script is removed from. The trigger should be created before passing it as a parameter.

		@param index - Index (EventHandler ID) of the VBS3 script.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::applyOnActivationStatementRemoval(trigger1, ID);

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::applyOnActivationStatement(Trigger& trigger, string command);

		@remarks - To remove the OnActivationStatement of the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyOnActivationStatementRemoval(Trigger& trigger, double index) (Refer (MissionUtilities::loadMission(Mission& mission)for further information;

		@remarks This is a replication of void removeTriggerOnActivated(Trigger& trigger, double index) function.	

		*/
		static void applyOnActivationStatementRemoval(Trigger& trigger, double index);

		/*!
		@description

		Defines the trigger activation type for the trigger created within the VBS3 environment.

		@locality

		Globally Applied Locally Effected

		@version [VBSFusion v3.09]

		@param trig - Trigger for which the activation type is applied to.

		@param by - Who activates the  trigger.
				    It can be "NONE" or
					Side': "EAST", "WEST", "GUER", "CIV", "LOGIC", "ANY"
					Radio: "ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", "HOTEL", "INDIA", "JULIET"
					Object: "STATIC", "VEHICLE", "GROUP", "LEADER", "MEMBER"
					Status: "WEST SEIZED", "EAST SEIZED" or "GUER SEIZED"

		@param type  type indicates the trigger activation. 
					It can be:
					Presence: "PRESENT", "NOT PRESENT"
					Detection: "WEST D", "EAST D", "GUER D" or "CIV D"

		@param repeat - Activation happens  repeatedly if the boolean value is true.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::applyActivationType(trigger1,string("EAST"),string ("PRESENT"), true);

		@endcode

		@overloaded

		None.

		@related

		None.

		@remarks - To apply the activation type for  the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function  TriggerUtilities::applyActivationType(Trigger& trig, string& by, string& type, bool repeat); (Refer (MissionUtilities::loadMission(Mission& mission)for further information;

		@remarks -  This is a replication of void setTriggerActivation(Trigger& trig, string by, string type, bool repeat) function.
		
		*/
		static void applyActivationType(Trigger& trig, const std::string& by, const std::string& type, bool repeat);

		/*!
		@description

		Returns all the objects attached to the trigger within the VBS3 environment.

		@locality

		Globally Applied

		@version [VBS3Fusion v3.09]

		@param trig - Trigger object for which the above function is applied. The trigger should be created before passing it as a parameter

		@return list<ControllableObject>

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		controllableObject u1;
		list<ControllableObject> objList;
		objList = TriggerUtilities::getAttachedObjects(trigger1);
		displayString = " Number of attached objects " + conversions::IntToString(objList.size());

		ControllableObjectUtilities::updateStaticProperties(u1);
		ControllableObjectUtilities::updateDynamicProperties(u1);

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::applyObjectAttach(Trigger& trig, ControllableObject& co);

		@remarks  - To obtain the attached objects  of the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::getAttachedObjects(Trigger& trig);; (Refer (MissionUtilities::loadMission(Mission& mission)for further information;

		@remarks  - This is a replication of list<ControllableObject>triggerAttachedObj(Trigger& trig) function.
		
		*/
		static std::list<ControllableObject> getAttachedObjects(const Trigger& trig);

		/*!
		@description

		Assigns any game  object to a trigger.

		@locality 

		Globally Applied Locally Effected

		@version [VBS3Fusion v3.09]

		@param trig - Trigger for which the object is assigned.  The trigger should be created before passing it as a parameter.

		@param co - ControllableObject to be attached.

		@return  Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		//unit to be created//
		ControllableObject u1;
		TriggerUtilities::applyObjectAttach(trigger1,u1); 

		@endcode

		@overloaded

		None.

		@related

		None.

		@remarks - To attach objects  to the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyObjectAttach(Trigger& trig, ControllableObject& co); (Refer (MissionUtilities::loadMission(Mission& mission)for further information;
		
		@remarks - This is a replication of void triggerAttachObj(Trigger& trig, ControllableObject& co) function.

		*/
		static void applyObjectAttach(Trigger& trig, const ControllableObject& co);

		/*!
		@description

		Detaches a game object from a trigger.

		@locality

		Globally Applied Locally Effected

		@version [VBS3Fusion v3.09]

		@param trig - Trigger for which the object is detached from.

		@param co - ControllableObject to be detached from the trigger. The trigger should be created before passing it as a parameter.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::applyObjectDetach(trigger1,u1);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - To detach objects  from the  trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applyObjectDetach(Trigger& trig, ControllableObject& co); (Refer (MissionUtilities::loadMission(Mission& mission)for further information;

		@remarks - This is a replication of void triggerDetachObj(Trigger& trig, ControllableObject& co) function.

		*/
		static void applyObjectDetach(Trigger& trig, const ControllableObject& co);

		/*!
		@description

		Synchronizes a trigger with other list of waypoints. To remove the synchronization empty waypoints list can be passed into the function. 

		@locality

		Globally Applied Locally Effected

		@version [VBS3Fusion v3.09]

		@param trig - Trigger object to be synchronized with waypoins. The trigger should be created before passing it as a parameter.

		@param wplist - List of waypoints.

		@return  Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		//waypoints to be created
		Waypoint wp1,wp2;
		//List of waypoints
		list<Waypoint> inLst;
		inLst.push_back(wp1);
		inLst.push_back(wp2);
		TriggerUtilities::applySynchronization(trigger1,inLst);

		@endcode

		@overloaded

		None.

		@related

		TriggerUtilities::getSynchronizedWaypoints(Trigger& trig)

		@remarks -  Groups created should be assigned to waypoints before using this function.

		@remarks - To synchronize waypoints with the trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function TriggerUtilities::applySynchronization(Trigger& trig, list<Waypoint> wplist); (Refer (MissionUtilities::loadMission(Mission& mission)for further information;
		
		@remarks  - This is a replication of void synchronizeTrigger(Trigger& trig, list<Waypoint> wplist) function.

		*/
		static void applySynchronization(Trigger& trig,  const std::list<Waypoint> wplist);

		/*!
		@description

		Defines the music track played on activation.

		@locality

		Globally Applied Locally Effected

		@version

		[VBS3Fusion v3.09]

		@param trig - Trigger to which the  music track  is defined. The trigger should be created before passing it as a parameter.

		@param trackName - Name of the music track played on activation. Track is a subclass name of CfgMusic.In addition, "$NONE$" (no change) or "$STOP$" (stops the current music track). 

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::applyMusicEffect(trigger1, string("ATrack27"));

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::getMusicEffect(Trigger& trig);

		@remarks - To apply MusicEffects to the  triggers created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling TriggerUtilities::applyMusicEffect(Trigger& trig, string& trackName)
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information);

		@remarks This is a replication of void setMusicEffect(Trigger& trig, string& trackName) function.
		*/
		static void applyMusicEffect(Trigger& trig, const std::string& trackName);

		/*!
		@description

		This function defines the different sound effects to the triggers created within the VBS3 environment.

		@locality

		Globally Applied Locally Effected

		@version [VBS3Fusion v3.09]

		@param trig - Trigger to which the sound effects are defined to. The trigger should be created before passing it as a parameter.

		@param sound - Name of the 2D / 3D sound from CfgSounds.

		@param voice - Name of the 2D / 3D sound from CfgSounds.

		@param soundEnv-  Name of the environmental sound from CfgEnvSounds.

		@param soundDet - Creates a dynamic sound object attached to a trigger defined in CfgSFX. 

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::applySoundEffect(trigger1, string("Alarm"), string(""), string(""), string(""));

		@endcode

		@overloaded

		None 

		@related

		TriggerUtilities::getSoundEffect(Trigger& trig);

		@remarks  - To apply sounds to the  triggers created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the TriggerUtilities::applySoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet); (Refer (MissionUtilities::loadMission(Mission& mission)for further information;
		
		@remarks  - This is a replication of void setMusicEffect(Trigger& trig, string& trackName) function.
		*/
		static void applySoundEffect(Trigger& trig, const std::string& sound, const std::string& voice, const std::string& soundEnv, const std::string& soundDet);

		/*!
		@description

		Defines the title effect.

		@locality

		Globally Applied Locally Effected

		@version [VBS3Fusion v3.09]

		@param trig - Trigger to which the  title effect is defined. The trigger should be created before passing it as a parameter.

		@param classType - Type defines in TITLE_EFFECT_CLASS enum and it can be,
							- TEC_NONE - "NONE",
							- TEC_OBJECT - "OBJECT" : 'Text' defines the shown object , a subclass of CfgTitles. 
							- TEC_RES - "RES" : 'Text' defines a resource class, a subclass of RscTitles. 
							- TEC_TEXT - "TEXT" : The 'Text' is shown as text itself.		
		
		@param effect - Effect defines a subtype: "PLAIN", "PLAIN DOWN", "BLACK", "BLACK FADED", "BLACK OUT", "BLACK IN", "WHITE OUT" or "WHITE IN".

		@param text - Text to be shown. 

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::applyTitleEffect(trigger1, TITLE_EFFECT_CLASS::TEC_TEXT,TITLE_EFFECT_TYPE::BLACK , string(" Fire"));

		@endcode

		@overloaded
		
		None

		@related

		TriggerUtilities::getTitleEffect(Trigger& trig);

		@remarks - To apply TitleEffects to the  triggers created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling TriggerUtilities::applyTitleEffect(Trigger& trig, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text);
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information;
		
		@remarks - This is a replication of void setTitleEffect(Trigger& trig, string& type, string& effect, string& text) function.

		*/
		static void applyTitleEffect(Trigger& trig, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, const std::string& text);

		/*!
		@description
		
		The VBS3 script statement is executed when the trigger is activated and the effects are launched depending on the result of the statement.
		- If the result is a boolean and true, the effect is launched.
		- If the result is an object, the effect is launched if the result is the player or
		the player vehicle.
		- If the result is an array, the effect is launched if the result contains the player
		or the player vehicle.

		@locality

		Globally Applied Locally Effected

		@version [VBS3Fusion v3.09]

		@param trig - Trigger object. The trigger should be created before passing it as a parameter.

		@param statement - VBS3 script statement that is executed when the trigger is activated.

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		TriggerUtilities::applyEffectCondition(trigger1, string("isPlayer Unit1"));

		@endcode

		@overloaded

		None

		@related

		TriggerUtilities::getEffectCondition(Trigger& trig);

		@remarks  - To apply EffectCondition to the  triggers created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling TriggerUtilities::applyEffectCondition(Trigger& trig, string& statement);
		(Refer (MissionUtilities::loadMission(Mission& mission)for further information;
		
		@remarks  - This is a replication of void setEffectCondition(Trigger& trig, string& statement) function.

		*/
		static void applyEffectCondition(Trigger& trig, const std::string& statement);

		/*!
		@description

			Specifies the entity which will activate the selected trigger.
		-	If [] is given, the trigger is decoupled from the assigned vehicle. 
		-	If the activation source is "VEHICLE", "GROUP", "LEADER" or "MEMBER", it's changed to "NONE".
		-	If [vehicle] is given, the trigger is coupled to the vehicle or its group.
		-	When the source is "GROUP", "LEADER" or "MEMBER", it's coupled to the group, otherwise it's coupled to the vehicle and the source is changed to "VEHICLE".

		@locality

		Globally Applied Locally Effected

		@version [VBS3Fusion v3.09]

		@param trig - Trigger which is  is attached to a  given object. The trigger should be created before passing it as a parameter

		@param co - The ControllableObject which will activate the trigger. 

		@return Nothing

		@example

		@code

		//Trigger to be created
		Trigger trigger1;
		//object to be created
		ControllableObject co;
		TriggerUtilities::applyVehicleAttach(trigger1,co);

		@endcode

		@overloaded

		None

		@related

		None

		@remarks - To specify the entity which will activate the selected trigger created  in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling TriggerUtilities::applyVehicleAttach(Trigger& trig, ControllableObject& co); (Refer (MissionUtilities::loadMission(Mission& mission)for further information;
		
		@remarks - This is a replication of void triggerAttachVehicle(Trigger& trig, ControllableObject& co) function. Trigger activation type can be retrieved by using string getActivation(Trigger& trig) function.

		*/
		static void applyVehicleAttach(Trigger& trig, const ControllableObject& co);

		/*!
		@description 
		Gets the list of waypoints or triggers the specified trigger is synchronized with.
		
		@locality

		Locally applied

		@version  [VBS2Fusion v3.11]

		@param trig - Trigger object.
		
		@return list<Waypoint> - list of synchronized waypoints with passed trigger.

		@example

		@code

		//Apply synchronization
		TriggerUtilities::applySynchronization(trig1,listWp);

		//Update the trigger
		TriggerUtilities::updateTrigger(trig1);

		//Returns the Waypoint typed list which the particular trigger has synchronized with
		list<Waypoint> synWpList = TriggerUtilities::getSynchronizedWaypoints(trig1);

		//Prints the element names of the returned list
		for (list<Waypoint>::iterator it = synWpList.begin(); it != synWpList.end(); it++){
		displayString += it->getName()+"\\n";
		}

		@endcode

		@overloaded None.

		@related
		void TriggerUtilities::applySynchronization(Trigger& trig, list<Waypoint> wplist)

		@remarks 	
		*/
		static std::list<Waypoint> getSynchronizedWaypoints(const Trigger& trig);
	};

};

#endif //TRIGGER_UTILITIES_H