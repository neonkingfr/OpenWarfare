/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	EffectsUtilities.h

Purpose:

	This file contains the declaration of the EffectsUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			02-11/2011	NDB: Original Implementation
										
/************************************************************************/

#ifndef VBS2FUSION_EFFECT_UTILITIES_H
#define VBS2FUSION_EFFECT_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "position3D.h"
#include "data/ControllableObject.h"
//#include "Vector3f.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBSFusion
{


	class VBSFUSION_API EffectsUtilities
	{

	public:

		/*!
		Creates an Effect of given EFFECTS_TYPE , which is attached to the given ControllableObject.
		Returns an integer which is the reference number for the created effect.
		The returned reference number can be passed to the following functions to manipulate the effect
			- void DeleteEffect(int referenceNo)
			- void applyParticleCircle(int referenceNo, float radius, vector3D& velocity)
			- void applyParticleRandom(int referenceNo, PARTICLE_RANDOM_ARRAY& randomArray)
			- void applyDropInterval(int referenceNo, float interval)
			- void applyParticleParams(int referenceNo, ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray)
			- void applyParticleParams(int referenceNo, PARTICLE_PARAMS_ARRAY& paramsArray)
		*/
		static int CreateEffectWithReference(const ControllableObject& object, EFFECTS_TYPE effectsType);

		/*!
		Creates an Effect of given EFFECTS_TYPE , which is attached to the given ControllableObject.
		Returns an integer which is the reference number for the created effect.
		The returned reference number can be passed to the following functions to manipulate the effect
		- void DeleteEffect(int referenceNo)
		- void applyParticleCircle(int referenceNo, float radius, vector3D& velocity)
		- void applyParticleRandom(int referenceNo, PARTICLE_RANDOM_ARRAY& randomArray)
		- void applyDropInterval(int referenceNo, float interval)
		- void applyParticleParams(int referenceNo, ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray)
		- void applyParticleParams(int referenceNo, PARTICLE_PARAMS_ARRAY& paramsArray)
		*/
		static int CreateEffectWithReference(const ControllableObject& object, const PARTICLE_PARAMS_ARRAY& paramsArray, const PARTICLE_RANDOM_ARRAY& randomArray, float dropInterval);

		/*!
		Creates Effects in VBS environment according to given EFFECTS_TYPE (void return type)
		*/
		static void CreateEffects(const ControllableObject& object, EFFECTS_TYPE effectsType);

		/*!
		Creates Effects in VBS environment according to given parameters (void return type)
		*/
		static void CreateEffects(const ControllableObject& object, const PARTICLE_PARAMS_ARRAY& paramsArray, const PARTICLE_RANDOM_ARRAY& randomArray, float dropInterval);

		/*!
		Create Light in VBS environment according to given parameters.
		The light can see the in night time.
		*/
		static void CreateLight(const ControllableObject& object, const Color_RGB& lightColor, const Color_RGB& ambientColor, float brightness, const position3D& relatedPosition);

		/*!
		Creates Effects in VBS environment according to given parameters
		*/
		static void CreateEffects(const ControllableObject& object, const PARTICLE_PARAMS_ARRAY& paramsArray, const PARTICLE_RANDOM_ARRAY& randomArray, double dropInterval);

		/*!
		Create Light in VBS environment according to given parameters.
		The light can see the in night time.
		*/
		static void CreateLight(const ControllableObject& object, const Color_RGB& lightColor, const Color_RGB& ambientColor, double brightness, const position3D& relatedPosition);

		/*!
		Creates a particle effect.

		This command is used to create smoke, fire and similar effects.

		The particles are single polygons with single textures that always face the player.
		They can be set to dynamically change their position, size, direction, can be set to different weights and more or less dependant on the wind. 
		*/
		static void CreateDynamicEffects(const PARTICLE_PARAMS_ARRAY& particleArray);

		/*!
		Invokes flashbang and sets its duration.
		*/
		static void applyFlashBang(float duration);

		/*!
		Invokes flashbang and sets its duration.
		*/
		static void applyFlashBang(double duration);

		/*!
		Plays sound from CfgSounds, either already part of VBS2.
		Deprecated. Use void applySoundPlaying(string soundName)
		*/
		VBSFUSION_DPR(UEFT001) static void playSound(std::string soundName);

		/*!
		Plays music defined in description.ext or CfgMusic.
		musicName - Class name of sound to play. If empty ("") the currently played music will stop.
		Deprecated. Use void applyMusicPlaying(string musicName)		
		*/
		VBSFUSION_DPR(UEFT002) static void playMusic(std::string musicName);

		/*!
		Plays music defined in description.ext or CfgMusic.
		musicName - Class name of sound to play. If empty ("") the currently played music will stop.
		time - in seconds. Time where the music will start.
		*/
		static void playMusic(const std::string& musicName, double time);

		/*!
		Creates a sound source of the given type.

		Type is the name of the subclass of CfgVehicles. If the markers array contains several marker names, the position of a random one is used, otherwise, the given position is used. The sound source is placed inside a circle with this position as its center and placement as its radius.
		To stop the sound, delete the created sound object via ControllableObjectUtilities::deleteObject(). 

		type - String as per CfgSFX
		position - placement position
		markers - vector of markers to specify the possible placement position
		radius - repeated sounds will be generated within this radius around the specified position
		*/
		static ControllableObject CreateSoundSource(const std::string& type, const position3D& position, std::vector<Marker>& markers, double radius);


		/*!
		Update particle source to create particles on circle with given radius.
		Velocity is transformed and added to total velocity. 
		referenceNo - The reference number returned from a CreateEffect function.
		*/
		static void applyParticleCircle(int referenceNo, float radius, const vector3D& velocity);

		/*!
		Set randomization of particle source parameters. 
		referenceNo - The reference number returned from a CreateEffect function.
		*/
		static void applyParticleRandom(int referenceNo, const PARTICLE_RANDOM_ARRAY& randomArray);

		/*!
		Set interval of emitting particles from particle source. 
		referenceNo - The reference number returned from a CreateEffect function.
		*/
		static void applyDropInterval(int referenceNo, float interval);

		/*!
		Applies the position specified by pos to particle
		within the game environment.
		The reference number of the particle which is returned from a CreateEffectParticle function.
		*/
		static void applyPosition(int referenceNo, const position3D& pos);

		/*!
		Creates an empty particle effect at the given position and
		Returns an integer which is the reference number for the created effect.
		The returned reference number can be passed to the following functions to manipulate the effect
		- void DeleteEffect(int referenceNo)
		- void applyParticleCircle(int referenceNo, float radius, vector3D& velocity)
		- void applyParticleRandom(int referenceNo, PARTICLE_RANDOM_ARRAY& randomArray)
		- void applyDropInterval(int referenceNo, float interval)
		- void applyParticleParams(int referenceNo, ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray)
		- void applyParticleParams(int referenceNo, PARTICLE_PARAMS_ARRAY& paramsArray)
		- void applyPosition(int referenceNo, position3D pos)
		*/
		static int CreateEffectParticle(const position3D& pos);

		/*!
		Creates an empty particle effect locally at the given position and
		Returns an integer which is the reference number for the created effect.
		The returned reference number can be passed to the following functions to manipulate the effect
		- void DeleteEffect(int referenceNo)
		- void applyParticleCircle(int referenceNo, float radius, vector3D& velocity)
		- void applyParticleRandom(int referenceNo, PARTICLE_RANDOM_ARRAY& randomArray)
		- void applyDropInterval(int referenceNo, float interval)
		- void applyParticleParams(int referenceNo, ControllableObject& object, PARTICLE_PARAMS_ARRAY& paramsArray)
		- void applyParticleParams(int referenceNo, PARTICLE_PARAMS_ARRAY& paramsArray)
		- void applyPosition(int referenceNo, position3D pos)
		*/
		static int CreateEffectParticleLocal(const position3D& pos);

		/*!
		Set parameters to particle source.
		referenceNo - The reference number returned from a CreateEffect function.
		object	   - Controllable Object
		paramsArray -	SHAPE_NAME shapeName;
						string animationName;
						string type;
						double timePeriod;
						double lifeTime;
						position3D position;
						vector3D moveVelocity;
						double rotationVelocity;
						double weight;
						double volume;
						double rubbing;
						vector<double> size;
						vector<Color_RGBA> color;
						vector<double> animationPhase;
						double randamDirectionPeriod;
						double randomDirectionIntensity;
						string onTimer;
						string beforeDestroy;
		*/
		static void applyParticleParams(int referenceNo, const ControllableObject& object, const PARTICLE_PARAMS_ARRAY& paramsArray);

		/*!
		Set parameters to particle source.
		referenceNo - The reference number returned from a CreateEffect function.
		paramsArray -	SHAPE_NAME shapeName;
						string animationName;
						string type;
						double timePeriod;
						double lifeTime;
						position3D position;
						vector3D moveVelocity;
						double rotationVelocity;
						double weight;
						double volume;
						double rubbing;
						vector<double> size;
						vector<Color_RGBA> color;
						vector<double> animationPhase;
						double randamDirectionPeriod;
						double randomDirectionIntensity;
						string onTimer;
						string beforeDestroy;
		*/
		static void applyParticleParams(int referenceNo, const PARTICLE_PARAMS_ARRAY& paramsArray);

		/*!
		Deletes the Effect particles and the created Effect 
		referenceNo - The reference number of the particle which is returned from a CreateEffect function or a CreateLight function.
		*/
		static void DeleteEffect(int referenceNo);

		/*!
		Create a Light in VBS environment according to given parameters
		which is attached to the given ControllableObject and 
		Returns an integer which is the reference number for the created light.
		The lights are clearly visible during the night time of the simulation.
		The returned reference number can can be passed to the following function to delete the light object
			- void DeleteEffect(int referenceNo)
		*/
		static int createLightObjWithRef(const ControllableObject& object, const Color_RGB& lightColor, const Color_RGB& ambientColor, float brightness, const position3D& relatedPosition);

		/*!
		Detach light from object.
		referenceNo - The reference number of the particle which is returned from createLightObjWithRef function
		*/
		static void applyDetachLight(int referenceNo) ;
 
		/*!
		@description  
		Plays sound from CfgSounds, either already part of VBS2 or declared in description.ext.  

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v3.12] 

		@param  soundName - Name of the sound

		@return nothing

		@example  

		@code

		EffectsUtilities::applySoundPlaying("VBS2_ext_AV_door_close_v1");

		@endcode

		@overloaded 

		@related  

		@remarks This is a replication of void playSound(string soundName) 
		*/
		static void applySoundPlaying(const std::string& soundName);

		/*!
		@description  

		Plays music defined in description.ext or CfgMusic. 

		@locality

		Locally effected

		@version [VBS2Fusion v3.12] 

		@param  musicName -  Class name of sound to play. If empty ("") the currently played music will stop.

		@return nothing

		@example

		@code

		EffectsUtilities::applyMusicPlaying("ATrack27");

		@endcode

		@overloaded 

		@related  

		@remarks 
		*/

		static void applyMusicPlaying(const std::string& musicName);


			/*!
		@description  

		Plays music defined in description.ext or CfgMusic.

		@locality

		Locally Applied Locally Effected

		@version [VBS2Fusion v3.12] 

		@param  musicName - Class name of sound to play. If empty ("") the currently played music will stop.
		
		@param time - in seconds. Time where the music will start.

		@return nothing

		@example

		@code

		EffectsUtilities::applyMusicPlaying("ATrack10",3);

		@endcode

		@overloaded 

		@related  

		@remarks  This is a replication of void playMusic(string musicName, double time);
		*/

		static void applyMusicPlaying(const std::string& musicName, double time);
	};

	typedef EffectsUtilities EffectsUtilties;
};


#endif //EFFECT_UTILITIES_H