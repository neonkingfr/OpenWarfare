
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

VBSFusionErrorContext.h

Purpose:

This file contains the declaration of the VBS2Fusion Global Application Context.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			15-07-2013	MNG: Created in order to deprecate VBS2Fusion_ErrorContext.h

/************************************************************************/

#ifndef VBSFUSION_ERRORCONTEXT_H
#define VBSFUSION_ERRORCONTEXT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>
#include <set>
#include <fstream>
#include <ctime>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "util/ErrorHandleUtilities.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

//using namespace std;

namespace VBSFusion
{

		/*
		enum to define error types. 
		*/
		enum ERRORCODE
		{
			VBS2FUSION_ERRORS(VBS2FUSION_ERRORCODE)
			VBS2FUSION_E_C_TEMPLATE
		};

		/*!
		enum to define warning types. 
		*/
		enum WARNINGCODE
		{
			VBS2FUSION_WARNINGS(VBS2FUSION_WARNCODE)
			VBS2FUSION_W_C_TEMPLATE		
		};

		/*!
		enum to define error description. 
		*/
		enum ERRORDESC
		{
			VBS2FUSION_ERRORS(VBS2FUSION_ERRORDESC)
			VBS2FUSION_E_D_TEMPLATE
		};


		/*!
		enum to define warning description. 
		*/
		enum WARNINGDESC
		{
			VBS2FUSION_WARNINGS(VBS2FUSION_WARNDESC)
			VBS2FUSION_W_D_TEMPLATE	
		};

	class VBSFusionErrorContext
	{

	public:

		
		/*!
		Typedef for the file list in the form of a File Reference set.
		*/
		typedef std::set<std::string> fileReferenceSet;

		/*!primary constructor for class*/
		VBSFusionErrorContext(void);

		/*!primary destructor for class*/
		virtual ~VBSFusionErrorContext(void);		

		/*!
		Sets the type of the error using a ERRORCODE variable. 
		"callingClass" specifies the class which cause the error and "callingFunction" specifies
		the function that raise the error. These parameters and the related error descriptions 
		will be logged in the Error log file.
		*/
		virtual void processError(ERRORCODE code, std::string callingClass, std::string callingFunction, std::string fileName);

		/*!
		Sets the type of warning using a ERRORTYPES variable. Should be one of:
		"callingClass" specifies the class which cause the warning and "callingFunction" specifies
		the function raise the warning. These parameters and the related warning description 
		will be logged in the Error log file.
		*/
		virtual void processWarning(WARNINGCODE type, std::string callingClass, std::string callingFunction, std::string fileName);


		/*!
		Sets the type of operation perform inside plugin. Should be one of:
		"callingClass" specifies the class which cause the operation and "callingFunction" specifies
		the function that raise the operation. These parameters and the related operation description 
		will be logged in the Error log file.		
		*/
		virtual void processData(std::string operationDesc, std::string calledClass, std::string callingFunction, std::string fileName);

		/*!
		Clear Error Log	
		*/
		virtual void clearErrorLog(std::string fileName);

		/*!
		Enable Error Logging	
		*/
		virtual void enableLogging(bool logStatus);

		/*!
		Returns true if Error Logging is enabled, false if not.	
		*/
	    virtual bool isLoggingEnabled();

	protected:

		/*! Write the header text to the error file*/
		virtual void writeHeaderText(std::string fileName);

		/*!
		Returns the error code as a string variable. 
		*/
		virtual std::string getErrorCodeString(ERRORCODE types);

		/*!
		Returns the warning code as a string variable. 
		*/
		virtual std::string getWarningCodeString(WARNINGCODE types);

		/*!
		Returns the error description as a string variable. 
		*/
		virtual std::string getErrorDescString(ERRORDESC types);

		/*!
		Returns the warning description as a string variable. 
		*/
		virtual std::string getWarningDescString(WARNINGDESC types);	

		fileReferenceSet _fileset;

		int erroCount;
		int warnningCount;
		int dataCount;

		bool _bLogEnabled; 
	};
};

VBSFusion::VBSFusionErrorContext& getVBSFusionErrorContext(); //global error context

#endif