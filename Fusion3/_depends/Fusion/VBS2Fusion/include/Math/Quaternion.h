/**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:  Matrix4.h

Purpose:


Version Information:

Version		Date		Author and Comments
===========================================
1.0			23-12/2010	KDG and MNG: Original Implementation

/*****************************************************************************/

#ifndef  VBS2FUSION_QUATERNION_H
#define VBS2FUSION_QUATERNION_H

#include <Math/Vector3f.h>
#include <math.h>

namespace VBSFusion
{

class VBSFUSION_API Quaternion
{
public:
	
	/*!
		
	@description
	
	This constructor is used to create identity quaternion.
	
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return Nothing
	
	@example
	
	@code

	Quaternion quat;
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	Quaternion();

	/*!
		
	@description
	
	This constructor is used to create a quaternion by using w,x,y,z components.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param w - W component of the quaternion 
	@param x - X component of the quaternion 
	@param y - Y component of the quaternion 
	@param z - Z component of the quaternion 
	
	@return Nothing
	
	@example
	
	@code

	Quaternion quat1(30.0f,20.0f,16.4f,-12.0f);
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	Quaternion(float w, float x, float y, float z);
	
	/*!
		
	@description

	This constructor is used to create quaternion by angle-axis specification(radian).
	
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param angle - Rotation angle.
	@param axis - Relevant axis to rotate.
	
	@return Nothing
	
	@example
	
	@code

	float angle1=3.14f;

	float x1=1.0f;
	float y1=1.0;
	float z1=1.0f;

	Vector3f vector1(x1,z1, y1);

	Quaternion quat(angle1,vector1);
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	Quaternion(float angle, const Vector3f& axis);

	/*!
		
	@description

	This constructor is used to create a quaternion from three Euler angles in ZYX order. All the angles should be in radians.
	
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param radZ - angle to Z axis.
	@param radY - angle to Y axis.
	@param radX - angle to X axis.
	
	@return Nothing
	
	@example
	
	@code
	
	//define PI value
	#define FLOAT_PI 3.141592654f

	float radZ = FLOAT_PI;
	float radY = FLOAT_PI/2;
	float radX = FLOAT_PI/3;

	Quaternion qt(radZ, radY, radX);
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	Quaternion(float radZ, float radY, float radX);

<<<<<<< .mine
	/*!
		
	@description
	
	This function is used to set quaternion by angle-axis (radians).

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param angle - First parameter get as a  float angle. This value should be radian. 
	@param axis - Axis provide as a const vector reference. 
	
	@return Nothing
	
	@example
	
	@code

	//define PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=1.0f;
	float y1=1.0;
	float z1=1.0f;

	Vector3f vector1(x1,z1, y1);
	Quaternion quat;

	quat.setAngleAxisRad(angle1, vector1);
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	void setAngleAxisRad(float angle, const Vector3f& axis);
	
	/*!
		
	@description
	
	"Equal to" operator returns true only if each component of first quaternion equals in value to respective components of second quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Get a reference to a quaternion as right hand side parameter.
	
	@return bool
	
	@example
	
	@code

	//create Vector3f
	Vector3f vector1(10.0f,30.0f,50.0f); 

	//Create Quaternion
	Quaternion quat1(3.1430f,vector1); 

	//create Vector3f
	Vector3f vector2(10.0f,30.0f,50.0f); 

	//Create Quaternion
	Quaternion quat2(3.1430f,vector2); 

	bool b=quat1==quat2;
	
	@endcode
	
	@overloaded

	Overload the == operator.
	
	@related
	
	@remarks 
	
	*/
	bool operator==(const Quaternion& rhs) const;

	/*!
		
	@description

	"Not equal to" operator returns the negation of "equal to" operator.
	
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Get a reference to a quaternion as right hand side parameter.
	
	@return bool
	
	@example
	
	@code

	//create vector3f
	Vector3f vector1(10.0f,30.0f,50.0f);

	//Create Quaternion
	Quaternion quat1(3.1430f,vector1);

	//create vector3f
	Vector3f vector2(10.0f,30.0f,50.0000345f);

	//Create Quaternion
	Quaternion quat2(3.1430f,vector2);

	bool b=quat1!=quat2;
	
	@endcode
	
	@overloaded

	Overload the != operator.
	
	@related
	
	@remarks 
	
	*/
	bool operator!=(const Quaternion& rhs) const;

	/*!
		
	@description
	
	Component-wise addition and assignment from one quaternion to another

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Get a reference to a quaternion as right hand side parameter.
	
	@return Quaternion
	
	@example
	
	@code

	float angle1=22/7.0f;
	float x1=80.0;
	float y1=-40.0f;
	float z1=2.0f;

	//Create vector3f
	Vector3f vector1(x1,z1, y1);
	
	//create Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=11/7.0f;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//Create vector3f
	Vector3f vector2(x2,z2, y2);

	//create Quaternion
	Quaternion quat2(angle2,vector2);

	quat1+=quat2;
	
	@endcode
	
	@overloaded

	Overload the += operator.
	
	@related
	
	@remarks 
	
	*/
	Quaternion& operator+=(const Quaternion& rhs);

	/*!
		
	@description

	Component-wise addition of two quaternions.
	
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Get a reference to a quaternion as right hand side parameter.
	
	@return Nothing
	
	@example
	
	@code

	float angle1=22/7.0f;
	float x1=30.0;
	float y1=-40.0f;
	float z1=2.0f;

	//create a vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=11/7.0f;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//create a vector3f
	Vector3f vector2(x2,z2, y2);

	//create a Quaternion
	Quaternion quat2(angle2,vector2);

	Quaternion quat3=quat1+quat2;
	
	@endcode
	
	@overloaded

	Overload the + operator.
	
	@related
	
	@remarks 
	
	*/
	const Quaternion operator+(const Quaternion& rhs) const;

	/*!
		
	@description
	
	Component-wise substraction and assignment from one quaternion to another.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Get a reference to a quaternion as right hand side parameter.                              
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=-3.0f;
	float y1=10.10f;
	float z1=2.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=FLOAT_PI/2;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//create a Vector3f
	Vector3f vector2(x2,z2, y2);

	//create a Quaternion
	Quaternion quat2(angle2,vector2);

	quat1-=quat2;
	
	@endcode
	
	@overloaded

	Overload the -= operator.
	
	@related
	
	@remarks 
	
	*/
	Quaternion& operator-=(const Quaternion& rhs);

	/*!
		
	@description

	Component-wise substraction of two quaternions.
	
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Get a reference to a quaternion as right hand side parameter.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=30.0;
	float y1=-40.0f;
	float z1=2.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=FLOAT_PI/2;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//create a Vector3f
	Vector3f vector2(x2,z2, y2);

	//create a Quaternion
	Quaternion quat2(angle2,vector2);

	Quaternion quat3=quat1-quat2;
	
	@endcode
	
	@overloaded
	
	Overload the - operator.

	@related
	
	@remarks 
	
	*/
	const Quaternion operator-(const Quaternion& rhs) const;

	/*!
		
	@description
	
	Calculates cross product of two quaternions and assign new values to the caller.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Get a reference to a quaternion as right hand side parameter.
	
	@return Nothing
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=30.0;
	float y1=-40.0f;
	float z1=2.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=FLOAT_PI/2;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//create a Vector3f
	Vector3f vector2(x2,z2, y2);
=======
	/*!
		
	@description
	
	This function is used to set quaternion by angle-axis (radians).

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param First parameter get as a  float angle. This value should be radian. 
	@param Axis provide as a const vector reference. 
	
	@return Nothing
	
	@example
	
	@code

	//define PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=1.0f;
	float y1=1.0;
	float z1=1.0f;

	Vector3f vector1(x1,z1, y1);
	Quaternion quat;

	quat.setAngleAxisRad(angle1, vector1);
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	void setAngleAxisRad(float, const Vector3f&);
	
	/*!
		
	@description
	
	"Equal to" operator returns true only if each component of first quaternion equals in value to respective components of second quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Get a reference to a quaternion as right hand side parameter.
	
	@return bool
	
	@example
	
	@code

	//create Vector3f
	Vector3f vector1(10.0f,30.0f,50.0f); 

	//Create Quaternion
	Quaternion quat1(3.1430f,vector1); 

	//create Vector3f
	Vector3f vector2(10.0f,30.0f,50.0f); 

	//Create Quaternion
	Quaternion quat2(3.1430f,vector2); 

	bool b=quat1==quat2;
	
	@endcode
	
	@overloaded

	Overload the == operator.
	
	@related
	
	@remarks 
	
	*/
	bool operator==(const Quaternion&) const;

	/*!
		
	@description

	"Not equal to" operator returns the negation of "equal to" operator.
	
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Get a reference to a quaternion as right hand side parameter.
	
	@return bool
	
	@example
	
	@code

	//create vector3f
	Vector3f vector1(10.0f,30.0f,50.0f);

	//Create Quaternion
	Quaternion quat1(3.1430f,vector1);

	//create vector3f
	Vector3f vector2(10.0f,30.0f,50.0000345f);

	//Create Quaternion
	Quaternion quat2(3.1430f,vector2);

	bool b=quat1!=quat2;
	
	@endcode
	
	@overloaded

	Overload the != operator.
	
	@related
	
	@remarks 
	
	*/
	bool operator!=(const Quaternion&) const;
>>>>>>> .r859

<<<<<<< .mine
	//create a Quaternion
	Quaternion quat2(angle2,vector2);
=======
	/*!
		
	@description
	
	Component-wise addition and assignment from one quaternion to another

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Get a reference to a quaternion as right hand side parameter.
	
	@return Quaternion
	
	@example
	
	@code

	float angle1=22/7.0f;
	float x1=80.0;
	float y1=-40.0f;
	float z1=2.0f;

	//Create vector3f
	Vector3f vector1(x1,z1, y1);
	
	//create Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=11/7.0f;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//Create vector3f
	Vector3f vector2(x2,z2, y2);

	//create Quaternion
	Quaternion quat2(angle2,vector2);

	quat1+=quat2;
	
	@endcode
	
	@overloaded

	Overload the += operator.
	
	@related
	
	@remarks 
	
	*/
	Quaternion& operator+=(const Quaternion&);

	/*!
		
	@description

	Component-wise addition of two quaternions.
	
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Get a reference to a quaternion as right hand side parameter.
	
	@return Nothing
	
	@example
	
	@code

	float angle1=22/7.0f;
	float x1=30.0;
	float y1=-40.0f;
	float z1=2.0f;

	//create a vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=11/7.0f;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//create a vector3f
	Vector3f vector2(x2,z2, y2);

	//create a Quaternion
	Quaternion quat2(angle2,vector2);

	Quaternion quat3=quat1+quat2;
	
	@endcode
	
	@overloaded

	Overload the + operator.
	
	@related
	
	@remarks 
	
	*/
	const Quaternion operator+(const Quaternion&) const;

	/*!
		
	@description
	
	Component-wise substraction and assignment from one quaternion to another.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Get a reference to a quaternion as right hand side parameter.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=-3.0f;
	float y1=10.10f;
	float z1=2.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=FLOAT_PI/2;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//create a Vector3f
	Vector3f vector2(x2,z2, y2);

	//create a Quaternion
	Quaternion quat2(angle2,vector2);

	quat1-=quat2;
	
	@endcode
	
	@overloaded

	Overload the -= operator.
	
	@related
	
	@remarks 
	
	*/
	Quaternion& operator-=(const Quaternion&);

	/*!
		
	@description

	Component-wise substraction of two quaternions.
	
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Get a reference to a quaternion as right hand side parameter.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=30.0;
	float y1=-40.0f;
	float z1=2.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=FLOAT_PI/2;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//create a Vector3f
	Vector3f vector2(x2,z2, y2);

	//create a Quaternion
	Quaternion quat2(angle2,vector2);

	Quaternion quat3=quat1-quat2;
	
	@endcode
	
	@overloaded
	
	Overload the - operator.

	@related
	
	@remarks 
	
	*/
	const Quaternion operator-(const Quaternion&) const;
>>>>>>> .r859

<<<<<<< .mine
	quat1*=quat2;
	
	@endcode
	
	@overloaded
=======
	/*!
		
	@description
	
	Calculates cross product of two quaternions and assign new values to the caller.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Get a reference to a quaternion as right hand side parameter.
	
	@return Nothing
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=30.0;
	float y1=-40.0f;
	float z1=2.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=FLOAT_PI/2;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//create a Vector3f
	Vector3f vector2(x2,z2, y2);

	//create a Quaternion
	Quaternion quat2(angle2,vector2);

	quat1*=quat2;
	
	@endcode
	
	@overloaded

	Overload the *= operator.
	
	@related
	
	@remarks 
	
	*/
	Quaternion& operator*=(const Quaternion&);

	/*!
		
	@description
	
	Calculates cross product of two quaternions and returns a new quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Get a reference to a quaternion as right hand side parameter.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=30.0;
	float y1=-40.0f;
	float z1=2.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=FLOAT_PI/2;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//create a Vector3f
	Vector3f vector2(x2,z2, y2);

	//create a Quaternion
	Quaternion quat2(angle2,vector2);

	Quaternion quat3=quat1*quat2;
	
	@endcode
	
	@overloaded

	overload the * operator.
	
	@related
	
	@remarks 
	
	*/
	const Quaternion operator*(const Quaternion&) const;
>>>>>>> .r859

<<<<<<< .mine
	Overload the *= operator.
	
	@related
	
	@remarks 
	
	*/
	Quaternion& operator*=(const Quaternion& rhs);
=======
	/*!
		
	@description
	
	Multiplication and assignment of a quaternion with a scalar.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Float value to multiply the quaternion.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=1.0f;
	float y1=1.0;
	float z1=1.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat(angle1,vector1);

	//multiply float value
	float multiplyValue=3.1234f;

	quat*=multiplyValue;
	
	@endcode
	
	@overloaded

	overload the *= operator.
	
	@related
	
	@remarks 
	
	*/
	Quaternion& operator*=(float);

	/*!
		
	@description
	
	Multiply a quaternion with a scalar and return the new quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Float value to multiply the quaternion.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=1.0f;
	float y1=1.0;
	float z1=1.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	//multiply float value
	float multiplyValue=3.1234f;

	Quaternion quat2=quat1*multiplyValue;
	
	@endcode
	
	@overloaded

	Overload the * operator.
	
	@related
	
	@remarks 
	
	*/
	const Quaternion operator*(float) const;

	/*!
		
	@description
	
	Multiply a quaternion from a vector.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param Reference vector as right hand side parameter. 
	
	@return vector3f
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=1.0f;
	float y1=1.0;
	float z1=1.0f;

	//create Vector3f
	Vector3f vector1(x1,z1, y1);

	//create Quaternion
	Quaternion quat(angle1,vector1);

	float vectX=10.5f;
	float vectY=20.556f;
	float vectZ=453.5645f;

	//create Vector3f for multiplication
	Vector3f vectorMul(vectX,vectZ,vectY);

	Vector3f vectorResult=quat*vectorMul;
	
	@endcode
	
	@overloaded
	
	Overload the * operator.

	@related
	
	@remarks 
	
	*/
	const Vector3f operator*(const Vector3f&) const;
>>>>>>> .r859

<<<<<<< .mine
	/*!
		
	@description
	
	Calculates cross product of two quaternions and returns a new quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Get a reference to a quaternion as right hand side parameter.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=30.0;
	float y1=-40.0f;
	float z1=2.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	float angle2=FLOAT_PI/2;
	float x2=-74.22200002f;
	float y2=5.33000033f;
	float z2=6.44400004f;

	//create a Vector3f
	Vector3f vector2(x2,z2, y2);

	//create a Quaternion
	Quaternion quat2(angle2,vector2);

	Quaternion quat3=quat1*quat2;
	
	@endcode
	
	@overloaded

	overload the * operator.
	
	@related
	
	@remarks 
	
	*/
	const Quaternion operator*(const Quaternion& rhs) const;

	/*!
		
	@description
	
	Multiplication and assignment of a quaternion with a scalar.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Float value to multiply the quaternion.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=1.0f;
	float y1=1.0;
	float z1=1.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat(angle1,vector1);

	//multiply float value
	float multiplyValue=3.1234f;

	quat*=multiplyValue;
	
	@endcode
	
	@overloaded

	overload the *= operator.
	
	@related
	
	@remarks 
	
	*/
	Quaternion& operator*=(float rhs);

	/*!
		
	@description
	
	Multiply a quaternion with a scalar and return the new quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Float value to multiply the quaternion.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=1.0f;
	float y1=1.0;
	float z1=1.0f;

	//create a Vector3f
	Vector3f vector1(x1,z1, y1);

	//create a Quaternion
	Quaternion quat1(angle1,vector1);

	//multiply float value
	float multiplyValue=3.1234f;

	Quaternion quat2=quat1*multiplyValue;
	
	@endcode
	
	@overloaded

	Overload the * operator.
	
	@related
	
	@remarks 
	
	*/
	const Quaternion operator*(float rhs) const;

	/*!
		
	@description
	
	Multiply a quaternion from a vector.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - Reference vector as right hand side parameter. 
	
	@return vector3f
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=1.0f;
	float y1=1.0;
	float z1=1.0f;

	//create Vector3f
	Vector3f vector1(x1,z1, y1);

	//create Quaternion
	Quaternion quat(angle1,vector1);

	float vectX=10.5f;
	float vectY=20.556f;
	float vectZ=453.5645f;

	//create Vector3f for multiplication
	Vector3f vectorMul(vectX,vectZ,vectY);

	Vector3f vectorResult=quat*vectorMul;
	
	@endcode
	
	@overloaded
	
	Overload the * operator.

	@related
	
	@remarks 
	
	*/
	const Vector3f operator*(const Vector3f& rhs) const;

	/*!
		
	@description
	
	Returns a new quaternion with negative component values of the caller.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=-3.0f;
	float y1=10.10f;
	float z1=2.0f;

	//Create Vector3f
	Vector3f vector1(x1,z1, y1);

	//Create Quaternion
	Quaternion quat1(angle1,vector1);

	Quaternion quat2=-quat1;
	
	@endcode
	
	@overloaded

	overload the - operator.
	
	@related
	
	@remarks 
	
	*/
=======
	/*!
		
	@description
	
	Returns a new quaternion with negative component values of the caller.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=-3.0f;
	float y1=10.10f;
	float z1=2.0f;

	//Create Vector3f
	Vector3f vector1(x1,z1, y1);

	//Create Quaternion
	Quaternion quat1(angle1,vector1);

	Quaternion quat2=-quat1;
	
	@endcode
	
	@overloaded

	overload the - operator.
	
	@related
	
	@remarks 
	
	*/
>>>>>>> .r859
	const Quaternion operator-() const;

	/*!
		
	@description
	
	Calculates cross product of two quaternions and returns a new quaternion. Same as operator*.

<<<<<<< .mine
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param rhs - const quaternion reference.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=1.0f;
	float y1=1.0f;
	float z1=1.0f;

	//create Vector3f
	Vector3f vector1(x1,z1, y1);

	//create Quaternion
	Quaternion quat1(angle1, vector1);

	float angle2=FLOAT_PI;
	float x2=1.0f;
	float y2=1.0f;
	float z2=1.0f;

	//create Vector3f
	Vector3f vector2(x2,z2, y2);

	//create Quaternion
	Quaternion quat2(angle2, vector2);

	Quaternion quat3=quat2.cross(quat1);
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	const Quaternion cross(const Quaternion& rhs) const;

	/*!
		
	@description
	
	This function is used to get the W component of the quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return float value of W.
	
	@example
	
	@code

	Quaternion quat;

	float W=quat.getW();
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	float getW() const;

	/*!
		
	@description
	
	This function is used to get the X component of the quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return float value of X.
	
	@example
	
	@code

	Quaternion quat;

	float X=quat.getX();
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	float getX() const;

	/*!
		
	@description
	
	This function is used to get the Y component of the quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return float value of Y.
	
	@example
	
	@code

	Quaternion quat;

	float Y=quat.getY();
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	float getY() const;

	/*!
		
	@description
	
	This function is used to get the Z component of the quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return float value of Z.
	
	@example
	
	@code

	Quaternion quat;

	float Z=quat.getZ();
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	float getZ() const;

	/*!
		
	@description
	
	Set the quaternion into identity quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return Nothing
	
	@example
	
	@code

	Quaternion q;
	q.setIdentity();
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
=======
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param const quaternion reference.
	
	@return Quaternion
	
	@example
	
	@code

	//define the PI value
	#define FLOAT_PI 3.141592654f

	float angle1=FLOAT_PI;
	float x1=1.0f;
	float y1=1.0f;
	float z1=1.0f;

	//create Vector3f
	Vector3f vector1(x1,z1, y1);

	//create Quaternion
	Quaternion quat1(angle1, vector1);

	float angle2=FLOAT_PI;
	float x2=1.0f;
	float y2=1.0f;
	float z2=1.0f;

	//create Vector3f
	Vector3f vector2(x2,z2, y2);

	//create Quaternion
	Quaternion quat2(angle2, vector2);

	Quaternion quat3=quat2.cross(quat1);
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	const Quaternion cross(const Quaternion&) const;

	/*!
		
	@description
	
	This function is used to get the W component of the quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return float value of W.
	
	@example
	
	@code

	Quaternion quat;

	float W=quat.getW();
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	float getW() const;

	/*!
		
	@description
	
	This function is used to get the X component of the quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return float value of X.
	
	@example
	
	@code

	Quaternion quat;

	float X=quat.getX();
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	float getX() const;

	/*!
		
	@description
	
	This function is used to get the Y component of the quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return float value of Y.
	
	@example
	
	@code

	Quaternion quat;

	float Y=quat.getY();
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	float getY() const;

	/*!
		
	@description
	
	This function is used to get the Z component of the quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return float value of Z.
	
	@example
	
	@code

	Quaternion quat;

	float Z=quat.getZ();
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	float getZ() const;

	/*!
		
	@description
	
	Set the quaternion into identity quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return Nothing
	
	@example
	
	@code

	Quaternion q;
	q.setIdentity();
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
>>>>>>> .r859
	void setIdentity();

	/*!
		
	@description
	
	Inverse of a given quaternion.

	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return Quaternion
	
	@example
	
	@code
	
	Quaternion q(5.1232131231231237f,5.1232131231231237f,5.1232131231231237f,5.1232131231231237f);
	Quaternion inv_q = q.invert();

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	const Quaternion invert() const;

	/*!
		
	@description

	check weather a quaternion is identity or not.
	
	@locality

	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return bool
	
	@example
	
	@code

	Quaternion q;
	q.setIdentity();
	bool chk_Identity= q.isIdentity();

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	const bool isIdentity() const;

	/*!
		
	@description
	
	Get the dot product of the quaternion with another.

	@locality
	
	Locally applied, Locally effected

	@version [VBSFusion v3.3.2]
	
	@param rhs - Quaternion const reference.
	
	@return float of dot product.
	
	@example
	
	@code

	Quaternion q1(0.50f,1.00f,0.01f,1.00f);
	Quaternion q2(0.10f,0.00f,0.00f,1.00f);

	// dot product of Quaternion q1 and q2 will be assigned to variable 'f'
	float f=q1.dot(q2);

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	const float dot(const Quaternion& rhs) const;

	/*!
		
	@description
	
	Normalize the quaternion. 

	@locality

	Locally applied, Locally effected

	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return -1 return if there is any error. return 1 if normalization succeed 
	
	@example
	
	@code

	float angle1=FLOAT_PI;

	float x1=-0.0f;
	float y1=0.0f;
	float z1=0.0f;
	Vector3f vector1(x1,z1, y1);

	Quaternion quat(angle1,vector1);
	int result=quat.normalize();

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	int normalize();
<<<<<<< .mine

	/*!
		
	@description
	
	Get rotation in angle-axis representation. Angle value is in radians.

	@locality

	Locally applied, Locally effected

	@version [VBSFusion v3.3.2]
	
	@param angle - First parameter is for return the angle as reference.
	@param axis - return the axis as a vector3f reference.
	
	@return Nothing
	
	@example
	
	@code

	float ret_Angle;
	Vector3f ret_Vector3f;
	Quaternion quat(1.0f,1.0f,1.0f,1.0f);

	//Construct Unit Quaternion 
	quat.normalize();

	quat.getAngleAxisRad(ret_Angle,ret_Vector3f);

	@endcode
	
	@overloaded
	
	@related 

	Quaternion::setAngleAxisRad(float angle, const Vector3f& axis)
	
	@remarks Any Quaternion constructed(except unit Quaternion) should be normalized before the function call.
	*/
	void getAngleAxisRad(float& angle, Vector3f& axis) const;

	/*!
		
	@description
	
	returns the logarithm of a quaternion = v*a where q = [cos(a),v*sin(a)]

	@locality

    Locally applied, Locally effected	
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return Quaternion
	
	@example
	
	@code

	const Vector3f v(1.f,1.f,1.f);
	Quaternion q(0,1.f,0.f,0.f);

	Quaternion new_q =q.log();

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
=======

	/*!
		
	@description
	
	Get rotation in angle-axis representation. Angle value is in radians.

	@locality

	Locally applied, Locally effected

	@version [VBSFusion v3.3.2]
	
	@param First parameter is for return the angle as reference.
	@param return the axis as a vector3f reference.
	
	@return Nothing
	
	@example
	
	@code

	float ret_Angle;
	Vector3f ret_Vector3f;
	Quaternion quat(1.0f,1.0f,1.0f,1.0f);

	//Construct Unit Quaternion 
	quat.normalize();

	quat.getAngleAxisRad(ret_Angle,ret_Vector3f);

	@endcode
	
	@overloaded
	
	@related 

	Quaternion::setAngleAxisRad(float angle, const Vector3f& axis)
	
	@remarks Any Quaternion constructed(except unit Quaternion) should be normalized before the function call.
	*/
	void getAngleAxisRad(float&, Vector3f&) const;

	/*!
		
	@description
	
	returns the logarithm of a quaternion = v*a where q = [cos(a),v*sin(a)]

	@locality

    Locally applied, Locally effected	
	
	@version [VBSFusion v3.3.1]
	
	@param 
	
	@return Quaternion
	
	@example
	
	@code

	const Vector3f v(1.f,1.f,1.f);
	Quaternion q(0,1.f,0.f,0.f);

	Quaternion new_q =q.log();

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
>>>>>>> .r859
	const Quaternion log() const;
<<<<<<< .mine

	/*!
		
	@description
	
	Returns quaternion exponent of base 'e' where; e^quaternion = exp(v*a) = [cos(a),vsin(a)]

	@locality
	
	Locally applied, Locally effected 

	@version [VBSFusion v3.3.2]
	
	@param 
	
	@return Quaternion
	
	@example
	
	@code

	Quaternion q(2.1f,1.f,0.0f,1.f);
	Quaternion q_temp;
	if(q.normalize()){
	   q_temp=q;
	   q=q.log();
	   }	
	q=q.exp();
	 // (w,x,y,z)components of both Quaternion- q & q_temp approximately similar.
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
=======

	/*!
		
	@description
	
	returns e^quaternion = exp(v*a) = [cos(a),vsin(a)]

	@locality
	
	@version [VBSFusion v3.3.5]
	
	@param 
	
	@return Quaternion
	
	@example
	
	@code
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
>>>>>>> .r859
	const Quaternion exp() const;

<<<<<<< .mine
	/*!
		
	@description
	
	Linear interpolation of quaternions, where first and second parameters are start and end quaternions and the third is delta t.

	@locality
	
	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.2]
	
	@param q1 - First quaternion
	@param q2 - Second quaternion
	@param per - float percent
	
	@return Quaternion
	
	@example
	
	@code

	Quaternion q1(1.f,1.f,1.f,1.f);
	Quaternion q2(1.f,0.f,1.f,0.f);
	Quaternion q=Quaternion::lerp(q1,q2,0.5f);

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	static Quaternion lerp(const Quaternion& q1, const Quaternion& q2, float per);

	/*!

	@description

	Spherical linear interpolation of quaternions, where first and second parameters are start and end quaternions and the third is delta t.
	
	@locality
	
	Locally applied, Locally effected

	@version [VBSFusion v3.3.2]
	
	@param q1 - First quaternion
	@param q2 - Second quaternion
	@param per - float percent
	
	@return Quaternion
	
	@example
	
	@code

	Quaternion q1(0.1f,.1f,.1f,0.2f);
	Quaternion q2(0.03f,0.3f,0.3f,0.13f);
	Quaternion q=Quaternion::slerp(q1,q2,0.5f);

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	static Quaternion slerp(const Quaternion& q1, const Quaternion& q2, float per);

	/*!
		
	@description
	
	Same as slerp, but does not check if angle is greater than 90 degrees

	@locality
	
	Locally applied, Locally effected
 
	@version [VBSFusion v3.3.2]
	
	@param q1 - First quaternion
	@param q2 - Second quaternion
	@param t - float percent
	
	@return Nothing
	
	@example
	
	@code

	Quaternion q1(1.0f,0.1f,0.1f,0.2f);
	Quaternion q2(0.0f,0.3f,0.3f,0.13f);
	Quaternion q=Quaternion::slerpNoInvert(q1,q2,0.5f);
=======
	/*!
		
	@description
	
	Linear interpolation of quaternions, where first and second parameters are start and end quaternions and the third is delta t.

	@locality
	
	Locally applied, Locally effected
	
	@version [VBSFusion v3.3.2]
	
	@param q1 - First quaternion
	@param q2 - Second quaternion
	@param per - float percent
	
	@return Quaternion
	
	@example
	
	@code

	Quaternion q1(1.f,1.f,1.f,1.f);
	Quaternion q2(1.f,0.f,1.f,0.f);
	Quaternion q=Quaternion::lerp(q1,q2,0.5f);

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	static Quaternion lerp(const Quaternion& q1, const Quaternion& q2, float per);

	/*!

	@description

	Spherical linear interpolation of quaternions, where first and second parameters are start and end quaternions and the third is delta t.
	
	@locality
	
	Locally applied, Locally effected

	@version [VBSFusion v3.3.2]
	
	@param q1 - First quaternion
	@param q2 - Second quaternion
	@param per - float percent
	
	@return Quaternion
	
	@example
	
	@code

	Quaternion q1(0.1f,.1f,.1f,0.2f);
	Quaternion q2(0.03f,0.3f,0.3f,0.13f);
	Quaternion q=Quaternion::slerp(q1,q2,0.5f);

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	static Quaternion slerp(const Quaternion& q1, const Quaternion& q2, float per);

	/*!
		
	@description
	
	Same as slerp, but does not check if angle is greater than 90 degrees

	@locality
	
	Locally applied, Locally effected
 
	@version [VBSFusion v3.3.2]
	
	@param First quaternion
	@param Second quaternion
	@param float percent
	
	@return Nothing
	
	@example
	
	@code

	Quaternion q1(1.0f,0.1f,0.1f,0.2f);
	Quaternion q2(0.0f,0.3f,0.3f,0.13f);
	Quaternion q=Quaternion::slerpNoInvert(q1,q2,0.5f);

	@endcode
	
	@overloaded
	
	@related

	Quaternion::slerp(const Quaternion& q1, const Quaternion& q2, float per)
	
	@remarks 
	
	*/
	static Quaternion slerpNoInvert(const Quaternion&, const Quaternion&, float);

	/*!
		
	@description
	
	Returns spherical cubic interpolation.

	@locality
	
	Locally applied, Locally effected 

	@version [VBSFusion v3.3.2]
	
	@param First Quaternion
	@param Second Quaternion
	@param Third Quaternion
	@param Fourth Quaternion
	@param float percent

	@return Quaternion
	
	@example
	
	@code

	Quaternion q1(1.0f,1.0f,1.0f,0.2f);
	Quaternion q2(1.0f,.1f,1.f,0.0f);
	Quaternion q3(0.0f,0.3f,0.3f,0.13f);
	Quaternion q4(0.0f,0.3f,0.0f,0.13f);
	Quaternion q=Quaternion::squad(q1,q2,q3,q4,0.5f);

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	static Quaternion squad(const Quaternion&, const Quaternion&, const Quaternion&, const Quaternion&, float);

	/*!
		
	@description

	Shoemake-Bezier interpolation using De Castlejau algorithm
	
	@locality
	
	Locally applied, Locally effected 
>>>>>>> .r859

<<<<<<< .mine
	@endcode
	
	@overloaded
	
	@related
=======
	@version [VBSFusion v3.3.2]
	
	@param First Quaternion
	@param Second Quaternion
	@param Third Quaternion
	@param Fourth Quaternion
	@param float percent
	
	@return Quaternion
	
	@example
	
	@code
>>>>>>> .r859

<<<<<<< .mine
	Quaternion::slerp(const Quaternion& q1, const Quaternion& q2, float per)
	
	@remarks 
	
	*/
	static Quaternion slerpNoInvert(const Quaternion& q1, const Quaternion& q2, float t);
=======
	Quaternion q1(1.0f,.0f,.0f,0.0f);
	Quaternion q2(-1.0f,.0f,0.f,0.0f);
	Quaternion q3(0.5f,0.5f,0.5f,0.5f);
	Quaternion q4(0.5f,0.5f,0.5f,0.5f);
	Quaternion q=Quaternion::bezier(q1,q2,q3,q4,0.5f);
>>>>>>> .r859

<<<<<<< .mine
	/*!
		
	@description
	
	Returns spherical cubic interpolation.

	@locality
	
	Locally applied, Locally effected 

	@version [VBSFusion v3.3.2]
	
	@param q1 - First Quaternion
	@param q2 - Second Quaternion
	@param a - Third Quaternion
	@param b - Fourth Quaternion
	@param t - float percent

	@return Quaternion
	
	@example
	
	@code

	Quaternion q1(1.0f,1.0f,1.0f,0.2f);
	Quaternion q2(1.0f,.1f,1.f,0.0f);
	Quaternion q3(0.0f,0.3f,0.3f,0.13f);
	Quaternion q4(0.0f,0.3f,0.0f,0.13f);
	Quaternion q=Quaternion::squad(q1,q2,q3,q4,0.5f);

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	static Quaternion squad(const Quaternion& q1, const Quaternion& q2, const Quaternion& a, const Quaternion& b, float t);

	/*!
		
	@description

	Shoemake-Bezier interpolation using De Castlejau algorithm
	
	@locality
	
	Locally applied, Locally effected 

	@version [VBSFusion v3.3.2]
	
	@param q1 - First Quaternion
	@param q2 - Second Quaternion
	@param a - Third Quaternion
	@param b - Fourth Quaternion
	@param t - float percent
	
	@return Quaternion
	
	@example
	
	@code

	Quaternion q1(1.0f,.0f,.0f,0.0f);
	Quaternion q2(-1.0f,.0f,0.f,0.0f);
	Quaternion q3(0.5f,0.5f,0.5f,0.5f);
	Quaternion q4(0.5f,0.5f,0.5f,0.5f);
	Quaternion q=Quaternion::bezier(q1,q2,q3,q4,0.5f);

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	static Quaternion bezier(const Quaternion& q1, const Quaternion& q2, const Quaternion& a, const Quaternion& b, float t);

	/*!
		
	@description

	Given 3 quaternions, qn-1,qn and qn+1, calculate a control point to be used in spline interpolation
	
	@locality
	
	Locally applied, Locally effected 

	@version [VBSFusion v3.3.2]
	
	@param First Quaternion
	@param Second Quaternion
	@param Third Quaternion
	
	@return Quaternion
	
	@example
	
	@code

	Quaternion q1(-1.0f,-1.0f,-1.0f,-1.0f);
	Quaternion q2(0.1f,0.0f,0.0f,0.0f);
	Quaternion q3(.0f,0.5f,0.5f,0.5f);
	Quaternion q=Quaternion::spline(q1,q2,q3);

	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	static Quaternion spline(const Quaternion& qnm1, const Quaternion& qn, const Quaternion& qnp1);
=======
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	static Quaternion bezier(const Quaternion&, const Quaternion&, const Quaternion&, const Quaternion&, float);

	/*!
		
	@description

	Given 3 quaternions, qn-1,qn and qn+1, calculate a control point to be used in spline interpolation
	
	@locality
	
	@version [VBSFusion v3.3.5]
	
	@param First Quaternion
	@param Second Quaternion
	@param Third Quaternion
	
	@return Quaternion
	
	@example
	
	@code
	
	@endcode
	
	@overloaded
	
	@related
	
	@remarks 
	
	*/
	static Quaternion spline(const Quaternion& qnm1, const Quaternion& qn, const Quaternion& qnp1);
>>>>>>> .r859
private:
	float m_x, m_y, m_z, m_w;

};

};// VBSFusion

#endif