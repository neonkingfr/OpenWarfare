
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:  Matrix4.h

Purpose:

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			23-12/2010	YFP: Original Implementation
	1.01		20-07/2011	NDB: Edited void setRotationX(float angle)
								 void setRotationY(float angle)
								 void setRotationZ(float angle)
	1.02		26-08/2011	NDB: added void Normalize()
								 Comments Added

/*****************************************************************************/

#ifndef VBS2FUSION_MATRIX4D_H
#define VBS2FUSION_MATRIX4D_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Vector3f.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{
	class VBSFUSION_API Matrix4f
	{

	public:
		/*!
		Main constructor for this class.
		It initialize as identity matrix
		*/
		Matrix4f();

		/*!
		Main destructor for this class. 
		*/
		~Matrix4f();

		/*!
		The assignment operator for Matrix4f.
		*/
		Matrix4f & operator = (const Matrix4f& rhs);

		/*!
		Multiplication operator for Matrix4f 
		current matrix by given matrix
		*/
		Matrix4f operator * (const Matrix4f& mat) const;

		/*!
		Multiplication operator for Matrix4f 
		current matrix by given matrix
		*/
		Matrix4f operator * (float sclar) const;

		/*!
		Multiplication operator for Matrix4f 
		current matrix by given matrix
		*/
		Matrix4f operator * (double sclar) const;

		/*!
		Multiplication operator for Matrix4f 
		current matrix by given matrix
		*/
		Matrix4f operator + (const Matrix4f& mat) const;
		
		/*!
		Multiplication operator for Matrix4f 
		current matrix by given matrix
		*/
		Matrix4f operator - (const Matrix4f& mat) const;

		/*!
		transpose current Matrix 
		*/
		void transpose();

		/*!
		return Inverse matrix
		*/
		Matrix4f inverse() const;

		/*!
		returns the determinant of the matrix
		*/
		double determinant() const;

		/*!
		set the matrix as identity matrix
		*/
		void setIdentity();

		/*!
		set the matrix value as zero
		*/
		void setZero();

		/*!
		set translation or position of the matrix
		*/
		void setTranslation(float x, float y , float z);

		/*!
		set translation or position of the matrix
		*/
		void setTranslation(double x, double y , double z);

		/*!
		set the position of the matrix
		*/
		void setPosition(float x, float y , float z);

		/*!
		set the position of the matrix
		*/
		void setPosition(double x, double y , double z);

		/*!
		Rotation is Axis Z to Y direction
		angle - in degree
		*/
		void setRotationX(float angle);

		/*!
		Rotation is Axis Z to Y direction
		angle - in degree
		*/
		void setRotationX(double angle);

		/*!
		Rotation is Axis Y to X direction
		angle - in degree
		*/
		void setRotationZ(float angle);

		/*!
		Rotation is Axis Z to X direction
		angle - in degree
		*/
		void setRotationY(float angle);

		/*!
		Rotation is Axis Y to X direction
		angle - in degree
		*/
		void setRotationZ(double angle);

		/*!
		Rotation is Axis Z to X direction
		angle - in degree
		*/
		void setRotationY(double angle);


		/*!
		Returns Direction vector
		*/
		Vector3f getDirection() const;

		/*!
		Returns Up vector
		*/
		Vector3f getDirectionUp() const;

		/*!
		Returns Side vector
		*/
		Vector3f getDirectionSide() const;

		/*!
		Returns Position vector
		*/
		Vector3f getPosition() const;

		/*!
		returns the matrix element
		row,col- values range [0 -> 3]
		*/
		float getElement(int row, int col) const;

		/*!
		returns the matrix element
		row,col- values range [0 -> 3]
		*/
		double getElementEx(int row, int col) const;

		/*!
		sets the matrix element
		row,col- values range [0 -> 3]
		val - value need to be set as element
		*/
		void setElement(int row, int col, float val);

		/*!
		sets the matrix element
		row,col- values range [0 -> 3]
		val - value need to be set as element
		*/
		void setElement(int row, int col, double val);

		/*!
		sets elements of matrix from the input matrix
		input matrix should be float[4][3]
		*/
		void setMatrix(float mat[4][3]);

		/*!
		sets elements of matrix from the input matrix
		input matrix should be float[4][3]
		*/
		void setMatrix(double mat[4][3]);

		/*!
		returns the matrix array
		give the reference array as a input parameter
		refMat - reference array(float[4][3]
		*/
		void getMatrix(float refMat[4][3]);

		/*!
		returns the matrix array
		give the reference array as a input parameter
		refMat - reference array(float[4][3]
		*/
		void getMatrix(double refMat[4][3]);

		/*
		@description 

		Multiply a matrix and a vector, set the w = 0 to prevent translation on vectors.
		Return a vector3f.

		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.15]

		@param mat - matrix4f matrix that need to multiply with the vector.
		@param vec - vector3f vector that going to multiply with the matrix4f. 

		@return Vector3f - Return a vector3f.   

		@example

		@code

		Vector3f retV=Matrix4f::multiplyVector(mat,vec);

		@endcode

		@overloaded  

		@related

		@remarks 
		*/
		static Vector3f multiplyVector(const Matrix4f& mat, const Vector3f& vec);

		/*
		@description 

		Set the w = 1 to correctly apply the point translation.
		Return the corresponding vector3f.

		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.15]

		@param mat - matrix4f matrix that need to multiply with the vector.
		@param vec - vector3f vector that going to multiply with the matrix4f. 

		@return Vector3f - Return a vector3f.   

		@example

		@code

		Vector3f resV=Matrix4f::multiplyPoint(mat,vec);

		@endcode

		@overloaded  

		@related

		@remarks 
		*/
		static Vector3f multiplyPoint(const Matrix4f& mat, const Vector3f& vec);

		/*
		@description 

		This will give the corresponding rotation matrix for a given angle and axis.

		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.2.5]

		@param angleRad - rotation angle
		@param axis -  axis that rotate around.Assumes axis vector is normalized for performance reasons.
		@param matOut - corresponding rotation matrix 

		@return Nothing

		@example

		@code

		Matrix4f m1;
		const float PI = 3.141592653f;
		const float teeta = PI / 2;

		Vector3f vecx(0.0f,1.0f,0.0f);

		Matrix4f::getRotationMatrixForArbitraryAxis(theeta, vecx, m1);

		@endcode

		@overloaded  

		None.

		@related

		@remarks 
		*/
		static void getRotationMatrixForArbitraryAxis(float angleRad, const Vector3f& axis, Matrix4f& matOut);

		/*
		@description 

		Operator for multiply a matrix from a position3D vector.

		@locality

		Locally applied, Locally effected

		@version  [VBSFusion v3.2.5]

		@param pos - Left hand side of the operator. it will be a position3D value.		

		@return position3D - After multiply the matrix it will give 4x1 matrix. Then put it into a position3D vector by ignoring the w component of the answer.     

		@example

		@code

		Matrix4f mat;

		mat.setElement(0,0,4.0);
		mat.setElement(0,1,7.0);
		mat.setElement(0,2,1.0);


		mat.setElement(1,0,4.0);
		mat.setElement(1,1,3.0);
		mat.setElement(1,2,7.0);


		mat.setElement(2,0,2.0);
		mat.setElement(2,1,6.0);
		mat.setElement(2,2,8.0);

		position3D pos1 = position3D(15,20,2);

		position3D pos2 = mat*pos1;

		@endcode

		@overloaded 

		Matrix4f operator * (const Matrix4f& mat) const;
		Matrix4f operator * (float sclar) const;
		Matrix4f operator * (double sclar) const;

		@related

		@remarks 

		*/
		position3D operator * (const position3D& pos) const;

	public:
		/*float _m00, _m01 , _m02 , _m03;
		float _m10, _m11 , _m12 , _m13;
		float _m20, _m21 , _m22 , _m23;
		float _m30, _m31 , _m32 , _m33;*/

		union {
			struct {
				double _m00, _m01 , _m02 , _m03;
				double _m10, _m11 , _m12 , _m13;
				double _m20, _m21 , _m22 , _m23;
				double _m30, _m31 , _m32 , _m33;

			};
			double _m[4][4];
		};

	private:
		void setMatrixToM();
		double determinant(double arr[2][2]) const;
		double determinant(double arr[3][3]) const;		

		//float _m[4][4];

	};

}



#endif