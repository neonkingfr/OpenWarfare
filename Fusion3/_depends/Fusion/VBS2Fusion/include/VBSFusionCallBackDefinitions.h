 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************
Name:		VBS2FusionCallBackDefinitions
Purpose:	This file contains VBS2Fusion Global ENUM definitions which are used in callbacks


Version Information:

Version		Date		Author and Comments
===========================================
1.00		24-04-2012	RDJ: Original Implementation

/************************************************************************/
#ifndef VBS2FUSIONCALLBACKDEFINITIONS_H
#define VBS2FUSIONCALLBACKDEFINITIONS_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>


/**********************************************************************/
/* END INCLUDES
/**********************************************************************/
//using namespace std;

namespace VBSFusion
{	
	struct VBSParametersEx
	{
		const char *_dirInstall;  // VBS installation directory
		const char *_dirUser;     // user's VBS directory
	};

	// resize event types
	enum windowResizeEventType {WINSIZE_RESTORED, WINSIZE_MINIMIZED, WINSIZE_MAXIMIZED, WINSIZE_MAXSHOW, WINSIZE_MAXHIDE};

	// passed when the window resize event has happened
	struct windowResizeEventParams 
	{
		windowResizeEventType resizeEventType;
		unsigned int newWidth;
		unsigned int newHeight;
	};

	enum mouseEventType{ MOUSEMOVE, LEFTBUTTONDOWN, LEFTBUTTONUP, RIGHTBUTTONDOWN, RIGHTBUTTONUP, MOUSEWHEEL };
	struct mouseParameters
	{
		mouseEventType _mouseEvent;
		int _xPos;
		int _yPos;
	};


	struct FrustumSpecNG
	{
		// camera position
		double pointPosX;
		double pointPosY;
		double pointPosZ;

		// camera speed, used for OpenAL sounds
		float pointSpeedX;
		float pointSpeedY;
		float pointSpeedZ;

		// camera view direction
		float viewDirX;
		float viewDirY;
		float viewDirZ;

		// camera up-view
		float viewUpX;
		float viewUpY;
		float viewUpZ;

		// camera projection angle tangents
		// Beware: for now, top/bottom and left/right pairs are used as single (symmetric) values
		float projTanTop;
		float projTanBottom;
		float projTanLeft;
		float projTanRight;

		// near, far distance clipping
		float clipDistNear;
		float clipDistFar;
		float clipDistFarShadow;
		float clipDistFarSecShadow;
		float clipDistFarFog;

		// standard constructor
		FrustumSpecNG():
		pointPosX(0), pointPosY(0), pointPosZ(0), pointSpeedX(0), pointSpeedY(0), pointSpeedZ(0),
			viewDirX(0), viewDirY(0), viewDirZ(0), viewUpX(0), viewUpY(0), viewUpZ(0),
			projTanTop(0), projTanBottom(0), projTanLeft(0), projTanRight(0),
			clipDistNear(0),clipDistFar(0), clipDistFarShadow(0), clipDistFarSecShadow(0), clipDistFarFog(0)
		{}

		// auxiliary setting functions
		void SetPos(float posX, float posY, float posZ) {pointPosX = posX; pointPosY = posY; pointPosZ = posZ;}
		void SetSpeed(float speedX, float speedY, float speedZ) {pointSpeedX = speedX; pointSpeedY = speedY; pointSpeedZ = speedZ;}
		void SetDir(float dirX, float dirY, float dirZ) {viewDirX = dirX; viewDirY = dirY; viewDirZ = dirZ;}
		void SetUp(float upX, float upY, float upZ) {viewUpX = upX; viewUpY = upY; viewUpZ = upZ;}
		void SetProj(float top, float bottom, float left, float right) {projTanTop = top; projTanBottom = bottom; projTanLeft = left; projTanRight = right;}
		void SetClip(float nearStd, float farStd, float farShadow, float farSecShadow, float farFog)
		{
			clipDistNear = nearStd; clipDistFar = farStd; clipDistFarShadow = farShadow; clipDistFarSecShadow = farSecShadow; clipDistFarFog = farFog;
		}
	};


	
	struct GeometryFace
	{
		int nIndices;     // count of indices in 'indices' array (3 or 4)
		int indices[4];   // array of indices to vertex array

		GeometryFace(){nIndices = 0;}
		GeometryFace(const GeometryFace& face){*this = face;}
	};

	struct GeometryVertex
	{
		float x,y,z;
	};

	struct GeometryObjectVerticesList
	{
		// variables:
		int nVertices;
		GeometryVertex* vertices;	
		
	};
		
	struct GeometryObject
	{
		enum Type
		{
			House = 1,
			Tree = 2,
			Terrain = 4,
			Road = 8
		};
		Type type;
		long long realObjectId;
		float transform[4][3];
		GeometryVertex* vertices;
		int nVertices;
		GeometryFace* faces;
		int nFaces;

		GeometryObjectVerticesList spawnPoints;

		GeometryObject(){nVertices = 0; nFaces = 0; vertices = NULL; faces = NULL;}
		~GeometryObject(){if (vertices) delete[] vertices; if (faces) delete[] faces;}
		GeometryObject(const GeometryObject& obj);
		const GeometryObject& operator=(const GeometryObject& obj);
		bool IsValid()const{return nFaces > 0 && nVertices > 0 && vertices && faces;}

		GeometryObject& operator+=(const GeometryObject &source); // appends vertices and faces from another object
		
	};

	

	// structure holds additional vertex arrays for GeometryObject
	
	
	}





#endif
