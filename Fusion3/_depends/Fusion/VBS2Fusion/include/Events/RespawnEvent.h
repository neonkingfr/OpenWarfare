/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	RespawnEvent.h

Purpose:

	This file contains the declaration of the RespawnEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			21-04/2009	RMR: Original Implementation
	2.0			11-01/2009	UDW: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_RESPAWN_EVENT_H
#define VBS2FUSION_RESPAWN_EVENT_H

#include <sstream>
#include <string>

#include "VBSFusion.h"
#include "Events/Event.h"
#include "data/Vehicle.h"

namespace VBSFusion
{
	/*!
	Defines a Respawn event which is passed onto the object 
	when an 'RESPAWN' event is called. Usually passed into the processRespawnEvent
	method of a ControllableObject. 

	Triggered when an object Respawns. 

	VBS2 automatically assigns a new name for un-named VBS2 units when units when respawned. Therefore, often a new
	alias needs to be assigned within the game to obtain a handle to the new unit. 
	*/
	class VBSFUSION_API RespawnEvent: public Event
	{
	public:
		/*!
		Primary constructor for the class. Calls setType(RESPAWN) to initialize. 
		*/
		RespawnEvent();

		/*!
		Primary destructor for the class. 
		*/
		~RespawnEvent();	

		/*!
		Set the VBS2 name of the newly created (i.e. respawned ) unit. 
		*/
		void setNewObjectName(const std::string& name);

		/*!
		Return the VBS2 name of the newly created (i.e. respawned ) unit. 
		*/
		std::string getNewObjectName() const;
	
		/*!
		Set the VBS2Fusion alias of the newly created (i.e. respawned ) unit. This new alias should be used
		to replace the alias of the already existing Unit object to be able to continue update and manipulation
		of its representative object within the game. 
		*/		
		void setNewObjectAlias(const std::string& alias);

		/*!
		Return the VBS2Fusion alias of the newly created (i.e. respawned ) unit. This new alias should be used
		to replace the alias of the already existing Unit object to be able to continue update and manipulation
		of its representative object within the game. 
		*/		
		std::string getNewObjectAlias() const;

		/*!
		Set object network ID 
		*/
		void setNewObjectId(const std::string& name);

		/*!
		Returns object network ID 
		*/
		std::string getNewObjectId() const;

		/*!
		Set object NetworkID by using NetworkID type. 
		*/
		void setNewObjectID(const NetworkID& id);

		/*!
		Returns object NetworkID in NetworkID type.
		*/
		NetworkID getNewObjectID() const;

	private:
		std::string newObjectName;
		std::string newObjectAlias;
		std::string newObjectId;
		
	};
};

#endif //SUPPRESSED_EVENT_H