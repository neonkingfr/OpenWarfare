/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	InitEvent.h

Purpose:

	This file contains the declaration of the CoverReachedEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			04-03/2013	[BISim] Otakar Nieder: Original Implementation


/************************************************************************/

#ifndef VBS2FUSION_DANGER_EVENT_H
#define VBS2FUSION_DANGER_EVENT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Event.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{
	/*!
	Defines a Danger event which is passed onto the object when an 'DANGER' 
	event is called. 
	*/
	class VBSFUSION_API DangerEvent: public Event
	{
	public:

		/*!
		Primary constructor for the class. Calls setType(DANGER) to initialize. 
		*/
		DangerEvent();

		/*!
		Primary destructor for the class. 
		*/
		~DangerEvent();

    /*!
		Sets the VBS2 name of the Object that caused the danger.
		*/
		void setCausedByObjectName(const std::string &name);

		/*!
		Gets the VBS2 name of the Object that caused the danger.
		*/
		const std::string &getCausedByObjectName() const;

 		/*!
		Sets the VBS2 ID of the Object that caused the danger.
		*/
    void setCausedByObjectID(const NetworkID &id);

    /*!
		Gets the VBS2 ID of the Object that caused the danger.
		*/
		const NetworkID &getCausedByObjectID() const;

    /*!
    Sets std::string identifier of the reason behind the danger.
    */
    void setCauseString(const std::string &causeString);

    /*!
    Gets string identifier of the reason behind the danger.
    */
    const std::string &getCauseString() const;

    /*!
    Sets 3D position of the danger.
    */
    void setDangerPosition(const position3D &pos);

    /*!
    Gets 3D position of the danger.
    */
    const position3D &getDangerPosition() const;

  private:
    std::string _causedBy;
    NetworkID _causedByID;
    std::string _cause;
    position3D _dangerPosition;
	};
};

#endif //DANGER_EVENT_H
