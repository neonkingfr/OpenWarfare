/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	HitPartEvent.h

Purpose:

	This file contains the declaration of the HitPartEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			17-04/2009	RMR: Original Implementation
	2.0			11-01/2009	UDW: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_HIT_PART_EVENT_H
#define VBS2FUSION_HIT_PART_EVENT_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
  Added in stage 2 warning removal process.
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4251)


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Events/Event.h"
#include "position3D.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{

	/*!
	Defines a HitPart event which is passed onto the object 
	when an 'HITPART' event is called. Usually passed into the processHitPartManEvent
	method of a ControllableObject. 
	*/
	class VBSFUSION_API HitPartEvent: public Event
	{
	public:
		/*!
		Primary constructor for the class. Calls setType(HITPART) to initialize. 
		*/
		HitPartEvent();

		/*!
		Primary destructor for the class.
		*/
		~HitPartEvent();

		/*!
		Sets the VBS2 name of the unit which fired the object. 
		*/
		void setShooterName(const std::string& name);

		/*!
		Returns the VBS2 name of the unit which fired the object. 
		*/
		std::string getShooterName() const;

		/*!
		Sets the VBS2Fusion alias of the unit which fired the object. This alias
		should be usable to initialize a new unit by calling setAlias and updating its
		properties. 
		*/
		void setShooterAlias(const std::string& alias);

		/*!
		Sets the VBS2Fusion alias of the unit which fired the object. This alias
		should be usable to initialize a new unit by calling setAlias and updating its
		properties. 
		*/
		std::string getShooterAlias() const;

		/*!
		Sets the VBS2 name of the object which was fired. 
		*/
		void setBulletName(const std::string& name);

		/*!
		Returns the VBS2 name of the object which was fired. 
		*/
		std::string getBulletName() const;

		/*!
		Sets the VBS2Fusion alias of the object which was fired. This alias
		should be usable to initialize a new Projectile object by calling
		its setAlias method. 
		*/
		void setBulletAlias(const std::string& alias);

		/*!
		Returns the VBS2Fusion alias of the object which was fired. This alias
		should be usable to initialize a new Projectile object by calling
		its setAlias method. 
		*/
		std::string getBulletAlias() const;

		/*!
		Sets the position where the bullet impacted. 
		*/
		void setImpactPosition(const position3D& position);

		/*!
		Returns the position where the bullet impacted. 
		*/
		position3D getImpactPosition() const;

		/*!
		Sets the impact velocity of the bullet. 
		*/
		void setImpactVelocity(const position3D& velocity);

		/*!
		Returns the impact velocity of the bullet. 
		*/
		position3D getImpactVelocity() const;

		/*!
		Sets the ammo hit value. 
		*/
		void setAmmoHitValue(double value);

		/*!
		Returns the ammo hit value. 
		*/
		double getAmmoHitValue() const;

		/*!
		Sets the ammo indirect hit value. 
		*/
		void setAmmoIndirectHitValue(double value);

		/*!
		Returns the ammo indirect hit value. 
		*/
		double getAmmoIndirectHitValue() const;

		/*!
		Sets the ammo indirect hit range. 
		*/
		void setAmmoIndirectHitRange(double value);

		/*!
		Returns the ammo indirect hit range. 
		*/
		double getAmmoIndirectHitRange() const;

		/*!
		Sets the ammo explosive damage value. 
		*/
		void setAmmoExplosiveDamage(double value);

		/*!
		Returns the ammo explosive damage value. 
		*/
		double getAmmoExplosiveDamage() const;

		/*!
		Sets the vector which is orthogonal (perpendicular) to the surface struck. 
		*/
		void setDirection(const position3D& dir);

		/*!
		Returns the vector which is orthogonal (perpendicular) to the surface struck. 
		*/
		position3D getDirection() const;

		/*!
		Sets the radius (side) of the component hit. 
		*/
		void setRadius(double value);

		/*!
		Returns the radius (side) of the component hit. 
		*/
		double getRadius() const;

		/*!
		Sets the name of the surface type struck. 
		*/
		void setSurface(const std::string& surf);

		/*!
		Returns the name of the surface type struck. 
		*/
		std::string getSurface() const;

		/*!
		True if object was hit directly, false if it was hit by indirect/splash damage. 
		*/
		void setDirect(bool direct);

		/*!
		True if object was hit directly, false if it was hit by indirect/splash damage. 
		*/
		bool getDirect() const;

		/*!
		Sets the list of strings with named selection parts of the object that were hit. 
		*/
		void setHitPartsNames(const std::list<std::string>& parts);

		/*!
		Returns the list of strings with named selection parts of the object that were hit.
		Simply we can get to know the parts where it gets hit through this function.
		*/
		std::list<std::string> getHitPartsNames() const;

	private:

		std::string _strShooterName;
		std::string _strShooterAlias;
		std::string _strBulletName;
		std::string _strBulletAlias;
		position3D _impactPosition;
		position3D _velocity;
		double _dAmmoHitValue;
		double _dAmmoIndirectHitValue;
		double _dAmmoIndirectHitRange;
		double _dAmmoExplosiveDamage;
		position3D _direction;
		double _dRadius;
		std::string _strSurface;
		bool _bDirect;
		std::list<std::string> _listHitPartsNames;
	};
};

#pragma warning(pop) // Enable warnings [C:4251]

#endif //HIT_PART_EVENT_H