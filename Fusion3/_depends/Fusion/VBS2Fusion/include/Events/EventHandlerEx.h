
/**************************************************************************
* Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
* Reserved.
*
* COPYRIGHT/OWNERSHIP
* This Software and its source code are proprietary products of SimCentric and
* are protected by copyright and other intellectual property laws. The Software
* is licensed and not sold.
*
* USE OF SOURCE CODE
* This source code may not be used, modified or copied without the expressed,
* written permission of SimCentric.
*
* RESTRICTIONS
* You MAY NOT: (a) copy and distribute the Software or any portion of it;
* (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
* or permit reverse engineering, disassembly, decompilation or alteration of
* this Software; (d) remove any product identification, copyright notices, or
* other notices or proprietary restrictions from this Software; (e) copy the
* documentation accompanying the software.
*
* DISCLAIMER OF WARRANTIES
* The Software is supplied "AS IS". SimCentric disclaims all warranties,
* expressed or implied, including, without limitation, the warranties of
* merchantability and of fitness for any purpose. The user must assume the
* entire risk of using the Software.
*
* DISCLAIMER OF DAMAGES
* SimCentric assumes no liability for damages, direct or consequential, which
* may result from the use of the Software, even if SimCentric has been advised
* of the possibility of such damages. Any liability of the seller will be
* limited to refund the purchase price.
*
**************************************************************************/

/*****************************************************************************

Name:

Purpose:


Version Information:

Version		Date		Author and Comments
===========================================
1.0			00-00/2010	---: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_EVENT_HANDLEREX_H
#define VBS2FUSION_EVENT_HANDLEREX_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4251)


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <list>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Events/Event.h"
#include "data/ControllableObject.h"
#include "data/Unit.h"
#include "data/Vehicle.h"
#include "Events/AmmoDeleteEvent.h"
#include "Events/AmmoExplodeEvent.h"
#include "Events/AmmoHitEvent.h"
#include "Events/AfterGetInEvent.h"
#include "Events/AfterGetOutEvent.h"
#include "Events/AfterGetInManEvent.h"
#include "Events/AfterGetOutManEvent.h"
#include "Events/BeforeGetInEvent.h"
#include "Events/BeforeGetInManEvent.h"
#include "Events/BeforeGetOutEvent.h"
#include "Events/BeforeGetOutManEvent.h"
#include "Events/ObjCreateEvent.h"
#include "Events/DeletedEvent.h"
#include "Events/GroupChangedEvent.h"
#include "Events/GroupChangedRTEEvent.h"
#include "Events/PasteEvent.h"
#include "Events/ChangedWeaponEvent.h"
#include "Events/AnimChangedEvent.h"
#include "Events/AnimDoneEvent.h"
#include "Events/AttachToEvent.h"
#include "Events/BeforeKilledEvent.h"
#include "Events/CargoChangedEvent.h"
#include "Events/DamagedEvent.h"
#include "Events/DeleteEvent.h"
#include "Events/EngineEvent.h"
#include "Events/FiredEvent.h"
#include "Events/FuelEvent.h"
#include "Events/GearEvent.h"
#include "Events/GetInEvent.h"
#include "Events/GetOutEvent.h"
#include "Events/GetInManEvent.h"
#include "Events/GetOutManEvent.h"
#include "Events/HitEvent.h"
#include "Events/HitPartEvent.h"
#include "Events/IncomingMissileEvent.h"
#include "Events/InitEvent.h"
#include "Events/KilledEvent.h"
#include "Events/LandedStoppedEvent.h"
#include "Events/LandedTouchDownEvent.h"
#include "Events/LoadOutChangedEvent.h"
#include "Events/MuzzleFiredEvent.h"
#include "Events/PositionChangedEvent.h"
#include "Events/RespawnEvent.h"
#include "Events/SuppressedEvent.h"
#include "Events/TurnInEvent.h"
#include "Events/TurnOutEvent.h"
#include "Events/WaypointCompleteEvent.h"
#include "Events/DangerEvent.h"
#include "Events/CoverReachedEvent.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/
namespace VBSFusion
{
	struct event_info
	{
		std::string eventName;
		VBSEVENTTYPE type;
	};

	class VBSFUSION_API EventHandlerEx
	{
	public:

		/*!
		Main constructor for EventHandlerEx class. 
		*/
		EventHandlerEx();

		/*!
		destructor for EventhandlerEx class. 
		*/
		virtual ~EventHandlerEx();


		/*!
		Adds new Delete Event handler to object. 
		  The function will be called automatically after Delete event is called by VBS2 to specified object. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const DeleteEvent * ),const std::string& eventRegID, VBSEVENTTYPE type);

		/*!
		Adds new Fired Event handler to object. 
		  The function will be called automatically after Fired event is called by VBS2 to specified object. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const FiredEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Attachto Event handler to object. 
		  The function will be called automatically after attachto event is called by VBS2 to specified object. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const AttachToEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new GetInMan Event handler to object. 
		  The function will be called automatically after GetInMan event is called by VBS2 to specified object. 
		Note: Function accepts a valid vehicle object and unique eventRegID.
		*/
		void addEventHandler(Unit* object, void (*eventFunc)(const GetInManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new GetOutMan Event handler to object. 
		  The function will be called automatically after GetOutMan event is called by VBS2 to specified object. 
		Note: Function accepts a valid vehicle object and unique eventRegID.
		*/
		void addEventHandler(Unit* object, void (*eventFunc)(const GetOutManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new HitPart Event handler to object. 
		  The function will be called automatically after HitPart event is called by VBS2 to specified object. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const HitPartEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Respawn Event handler to object. 
		  The function will be called automatically after Respawn event is called by VBS2 to specified object. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const RespawnEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Suppressed Event handler to object. 
		  The function will be called automatically after Suppressed event is called by VBS2 to specified object. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const SuppressedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Waypoint Complete Event handler to group object. 
		  The function will be called automatically after the WaypointComplete event is called by VBS2 to specified object. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(Group* object, void (*eventFunc)(const WaypointCompleteEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Ammo Explode Event handler to group object. 
		  The function event func will be called automatically after the ammunition hits something. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const AmmoExplodeEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
			
		/*!
		Adds new Animation Changed Event handler to  object. 
		  The function event func will be called automatically after the object's animation changed. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const AnimChangedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AmmoDelete global Event handler to  object. 
			The function event func will be called automatically after the object's animation changed. 
		Note: Function accepts a valid controllable object and unique eventRegID
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const AmmoDeleteEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new MuzzleFired global Event handler to  object. 
			The function event func will be called automatically after the object's animation changed. 
		Note: Function accepts a valid controllable object and unique eventRegID
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const MuzzleFiredEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Animation done Event handler to  object. 
		  The function event func will be called automatically after the object's current animation finished. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const AnimDoneEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Ammo Hit Event handler to object. 
		  The function event func will be called automatically after the ammunition hits something. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const AmmoHitEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Deleted Event handler to ControllableObject object. 
		  The function will be called automatically after Delete event is called by VBS2 to specified object. 
		Note: Function accepts a valid controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const DeletedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Group Changed Event handler to group object. 
		  The function event func will be called automatically when a change occurs to a specific group's or subgroup status. 
		Note: Function accepts a valid group object and unique eventRegID.
		*/
		void addEventHandler(Group* object, void (*eventFunc)(const GroupChangedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Group ChangedRTE Event handler to group object. 
		  The function event func will be called automatically only if RTE is open, when a change occurs to a specific 
		  group's or subgroup status. 
		Note: Function accepts a valid group object and unique eventRegID.
		*/
		void addEventHandler(Group* object, void (*eventFunc)(const GroupChangedRTEEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AfterGetIn Event handler to Vehicle object. 
		  The function will be called automatically after AfterGetIn event is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const AfterGetInEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AfterGetOut Event handler to Vehicle object. 
		  The function will be called automatically after AfterGetOut event is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const AfterGetOutEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new BeforeGetInEvent Event handler to Vehicle object. 
		  The function will be called automatically after BeforeGetInEvent event is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const BeforeGetInEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new BeforeGetOutEvent Event handler to Vehicle object. 
		  The function will be called automatically after BeforeGetOutEvent event is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const BeforeGetOutEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AfterGetInMan Event handler to Unit object. 
		  The function will be called automatically after AfterGetInMan event is called by VBS2 to specified object. 
		Note: Function accepts a valid Unit object and unique eventRegID.
		*/
		void addEventHandler(Unit* object, void (*eventFunc)(const AfterGetInManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AfterGetOutMan Event handler to Unit object. 
		  The function will be called automatically after AfterGetOutMan event is called by VBS2 to specified object. 
		Note: Function accepts a valid Unit object and unique eventRegID.
		*/
		void addEventHandler(Unit* object, void (*eventFunc)(const AfterGetOutManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new BeforeGetInMan Event handler to Unit object. 
		  The function will be called automatically after BeforeGetInMan event is called by VBS2 to specified object. 
		Note: Function accepts a valid Unit object and unique eventRegID.
		*/
		void addEventHandler(Unit* object, void (*eventFunc)(const BeforeGetInManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new BeforeGetOutMan Event handler to Unit object. 
		  The function will be called automatically after BeforeGetOutMan event is called by VBS2 to specified object. 
		Note: Function accepts a valid Unit object and unique eventRegID.
		*/
		void addEventHandler(Unit* object, void (*eventFunc)(const BeforeGetOutManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Paste Event handler to Controllable object. 
		  The function will be called automatically after Paste event is called by VBS2 to specified object. 
		Note: Function accepts a valid Controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const PasteEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new ChangedWeapon Event handler to Controllable object. 
		  The function will be called automatically after ChangedWeaponEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Controllable object and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const ChangedWeaponEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new BeforeKilled Event handler to Unit object. 
		  The function will be called automatically after BeforeKilledEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Unit object and unique eventRegID.
		*/
		void addEventHandler(Unit* object, void (*eventFunc)(const BeforeKilledEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new CargoChanged Event handler to ControllableObject. 
		  The function will be called automatically after CargoChangedEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid ControllableObject and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const CargoChangedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new Damaged Event handler to ControllableObject. 
		  The function will be called automatically after Damaged is called by VBS2 to specified object. 
		Note: Function accepts a valid ControllableObject and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const DamagedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new Engine Event handler to Vehicle Object. 
		  The function will be called automatically after EngineEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle Object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const EngineEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new Fuel Event handler to Vehicle Object. 
		  The function will be called automatically after FuelEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle Object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const FuelEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new Gear Event handler to Vehicle Object. 
		  The function will be called automatically after GearEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle Object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const GearEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new GetIn Event handler to Vehicle Object. 
		  The function will be called automatically after GetInEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle Object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const GetInEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new GetOut Event handler to Vehicle Object. 
		  The function will be called automatically after GetOutEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle Object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const GetOutEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Hit Event handler to ControllableObject. 
		  The function will be called automatically after HitEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid ControllableObject and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const HitEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new IncomingMissile Event handler to Unit Object. 
		  The function will be called automatically after IncomingMissileEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Unit Object and unique eventRegID.
		*/
		void addEventHandler(Unit* object, void (*eventFunc)(const IncomingMissileEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Killed Event handler to Unit Object. 
		  The function will be called automatically after KilledEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Unit Object and unique eventRegID.
		*/
		void addEventHandler(Unit* object, void (*eventFunc)(const KilledEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new LandedStopped Event handler to Vehicle Object. 
		  The function will be called automatically after LandedStoppedEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle Object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const LandedStoppedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new LandedTouchDown Event handler to Vehicle Object. 
		  The function will be called automatically after LandedTouchDownEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle Object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const LandedTouchDownEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new LoadOutChanged Event handler to ControllableObject. 
		  The function will be called automatically after LoadOutChangedEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid ControllableObject and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const LoadOutChangedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new PositionChanged Event handler to ControllableObject. 
		  The function will be called automatically after PositionChangedEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid ControllableObject and unique eventRegID.
		*/
		void addEventHandler(ControllableObject* object, void (*eventFunc)(const PositionChangedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new TurnIn Event handler to Vehicle Object. 
		  The function will be called automatically after TurnInEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle Object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const TurnInEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new TurnOut Event handler to Vehicle Object. 
		  The function will be called automatically after TurnOutEvent is called by VBS2 to specified object. 
		Note: Function accepts a valid Vehicle Object and unique eventRegID.
		*/
		void addEventHandler(Vehicle* object, void (*eventFunc)(const TurnOutEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );


		/************************************************************************/
		/* GOBAL EVENT HANDLERS                                                */
		/************************************************************************/

		/*!
		Adds new Delete global Event handler. 
		  The function will be called automatically after any Delete event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const DeleteEvent * ),const std::string& eventRegID, VBSEVENTTYPE type);

		/*!
		Adds new Fired global Event handler. 
		  The function will be called automatically after any Fired event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const FiredEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AttachTo global Event handler. 
		  The function will be called automatically after any AttachTo event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const AttachToEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new GetInMan global Event handler. 
		  The function will be called automatically after any GetInMan event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const GetInManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new GetOutMan global Event handler. 
		  The function will be called automatically after any GetOutMan event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const GetOutManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );


		/*!
		Adds new HitPart global Event handler. 
		  The function will be called automatically after any HitPart event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const HitPartEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Respawn global Event handler. 
		  The function will be called automatically after any Respawn event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const RespawnEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Suppressed global Event handler. 
		  The function will be called automatically after any Suppressed event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const SuppressedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new WaypointComplete global Event handler. 
		  The function will be called automatically after any WaypointComplete event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const WaypointCompleteEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AmmoHit global Event handler. 
		  The function will be called automatically after any AmmoHit event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const AmmoHitEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Deleted global Event handler. 
		  The function will be called automatically after any Deleted event is called by VBS2. 
		Note: Function accepts a unique eventRegID.		*/
		void addEventHandler(void (*eventFunc)(const DeletedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );


		/*!
		Adds new GroupChanged global Event handler. 
		  The function will be called automatically after any GroupChanged event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const GroupChangedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new GroupChangedRTE global Event handler. 
		  The function will be called automatically after any GroupChangedRTE event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const GroupChangedRTEEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Create global Event handler. 
		  The function will be called automatically after any Create event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const ObjCreateEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AfterGetIn global Event handler. 
		  The function will be called automatically after any AfterGetIn event is called by VBS2.
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const AfterGetInEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AfterGetOut global Event handler. 
		  The function will be called automatically after any AfterGetOut event is called by VBS2.
		Note: Function accepts a unique eventRegID.		
		*/
		void addEventHandler(void (*eventFunc)(const AfterGetOutEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new BeforeGetIn global Event handler. 
		  The function will be called automatically after any BeforeGetIn event is called by VBS2.
		Note: Function accepts a unique eventRegID.		
		*/
		void addEventHandler(void (*eventFunc)(const BeforeGetInEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new BeforeGetOut global Event handler. 
		  The function will be called automatically after any BeforeGetOut event is called by VBS2.
		Note: Function accepts a unique eventRegID.		
		*/
		void addEventHandler(void (*eventFunc)(const BeforeGetOutEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AfterGetInMan global Event handler. 
		  The function will be called automatically after any AfterGetInMan event is called by VBS2.
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const AfterGetInManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new  AfterGetOutMan global Event handler. 
		  The function will be called automatically after any  AfterGetOutMan event is called by VBS2.
		Note: Function accepts a unique eventRegID.		
		*/
		void addEventHandler(void (*eventFunc)(const AfterGetOutManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new BeforeGetInMan global Event handler. 
		  The function will be called automatically after any BeforeGetInMan event is called by VBS2.
		Note: Function accepts a unique eventRegID.		*/
		void addEventHandler(void (*eventFunc)(const BeforeGetInManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new BeforeGetOutMan global Event handler.
		  The function will be called automatically after any BeforeGetOutMan event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const BeforeGetOutManEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Paste global Event handler.
		  The function will be called automatically after any Paste event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const PasteEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new ChangedWeapon global Event handler.
		  The function will be called automatically after any ChangedWeapon event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const ChangedWeaponEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AmmoExplode global Event handler.
		  The function will be called automatically after any AmmoExplode event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const AmmoExplodeEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new AnimChanged global Event handler.
		  The function will be called automatically after any AnimChanged event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const AnimChangedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new AnimDone global Event handler.
		  The function will be called automatically after any AnimDone event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const AnimDoneEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new BeforeKilled global Event handler.
		  The function will be called automatically after any BeforeKilled event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const BeforeKilledEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new CargoChanged global Event handler.
		  The function will be called automatically after any CargoChanged event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const CargoChangedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new Damaged global Event handler.
		  The function will be called automatically after any Damaged event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const DamagedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new Engine global Event handler.
		  The function will be called automatically after any Engine event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const EngineEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new Fuel global Event handler.
		  The function will be called automatically after any Fuel event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const FuelEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new Gear global Event handler.
		  The function will be called automatically after any Gear event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const GearEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new GetIn global Event handler.
		  The function will be called automatically after any GetIn event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const GetInEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new GetOut global Event handler.
		  The function will be called automatically after any GetOut event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const GetOutEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Hit global Event handler.
		  The function will be called automatically after any Hit event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const HitEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new IncomingMissile global Event handler.
		  The function will be called automatically after any IncomingMissile event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const IncomingMissileEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Init global Event handler.
		  The function will be called automatically after any Init event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const InitEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new Killed global Event handler.
		  The function will be called automatically after any Killed event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const KilledEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new LandedStopped global Event handler.
		  The function will be called automatically after any LandedStopped event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const LandedStoppedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new LandedTouchDown global Event handler.
		  The function will be called automatically after any LandedTouchDown event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const LandedTouchDownEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new LoadOutChanged global Event handler.
		  The function will be called automatically after any LoadOutChanged event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const LoadOutChangedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new PositionChanged global Event handler.
		  The function will be called automatically after any PositionChanged event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const PositionChangedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new TurnIn global Event handler.
		  The function will be called automatically after any TurnIn event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const TurnInEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );
		
		/*!
		Adds new TurnOut global Event handler.
		  The function will be called automatically after any TurnOut event is called by VBS2.  
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const TurnOutEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new AmmoDelete global Event handler. 
		  The function will be called automatically after any AmmoDelete event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const AmmoDeleteEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new MuzzleFired global Event handler. 
		  The function will be called automatically after any MuzzleFired event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const MuzzleFiredEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new CoverReached global Event handler. 
		  The function will be called automatically after any CoverReached event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const CoverReachedEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Adds new Danger global Event handler. 
		  The function will be called automatically after any Danger event is called by VBS2. 
		Note: Function accepts a unique eventRegID.
		*/
		void addEventHandler(void (*eventFunc)(const DangerEvent * ),const std::string& eventRegID, VBSEVENTTYPE type );

		/*!
		Remove the event Handler specified by eventRegID and type.
		*/
		void removeEventHandler(const std::string& eventRegID , VBSEVENTTYPE type);

		/*!
		Remove all the event Handler associated with particular EventHandlerEx object.
		*/
		void removeAllEventHandlers();

		private:

			std::list<event_info> _eventInfoList;

	};

}

#pragma warning(pop) // Enable warnings [C:4251]

#endif // VBS2FUSION_EVENT_HANDLEREX_H
