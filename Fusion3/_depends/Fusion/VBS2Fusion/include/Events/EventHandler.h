
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/
/*************************************************************************

Name:

	EventHandler.h

Purpose:

	This file contains the declaration of the EventHandler class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			17-04/2009	RMR: Original Implementation
	1.01		20-08/2009	SDS: Added isEvent(const char* returnString)	
								 remove generateRandomAlias(string alias)
								 updated parseFiredEvent()
								 updated addFiredEventHandler(ControllableObject &co)
	2.0			10-02/2010	UDW: Version 2 Implementation
	2.01		11-02/2010	MHA: Comments Checked
************************************************************************/

#ifndef VBS2FUSION_EVENT_HANDLER_H
#define VBS2FUSION_EVENT_HANDLER_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4251)


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <list>
#include <time.h>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "data/ControllableObject.h"
#include "data/Group.h"
#include "data/Unit.h"
#include "conversions.h"
#include "util/ExecutionUtilities.h"
#include "Events/Event.h"
#include "Events/FiredEvent.h"
#include "Events/DeleteEvent.h"
#include "Events/HitPartEvent.h"
#include "Events/WaypointCompleteEvent.h"
#include "Events/GetInManEvent.h"
#include "Events/GetOutManEvent.h"
#include "Events/SuppressedEvent.h"
#include "Events/RespawnEvent.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/


namespace VBSFusion
{	

	/*!
	
	*/
	class VBSFUSION_API EventHandler
	{
	public:
		//********************************************************
		/*!
		Typedef for a list containing pointers to ControllableObject objects. 
		*/
		typedef std::list<ControllableObject*> CobjectList;

		/*!
		Typedef for an iterator to a list containing pointers to ControllableObjects. 
		*/
		typedef std::list<ControllableObject*>::iterator co_iterator;

		/*!
		Typedef for a const iterator to a list containing pointers to ControllableObjects. 
		*/
		typedef std::list<ControllableObject*>::const_iterator co_const_iterator;

		//********************************************************
		
		/*!
		Typedef for a list containing pointers to Group objects. 
		*/
		typedef std::list<Group*> GList;

		/*!
		Typedef for iterator to a list containing pointers to Group objects. 
		*/
		typedef std::list<Group*>::iterator g_iterator;

		/*!
		Typedef for a const iterator to a list containing pointers to Group objects. 
		*/
		typedef std::list<Group*>::const_iterator g_const_iterator;

		//********************************************************

		/*!
		Typedef for a list containing pointers to Unit objects. 
		*/
		typedef std::list<Unit*> UList;

		/*!
		Typedef for an iterator to a list containing pointers to Unit objects. 
		*/
		typedef std::list<Unit*>::iterator u_iterator;

		/*!
		Typedef for a const iterator to a list containing pointers to Unit objects. 
		*/
		typedef std::list<Unit*>::const_iterator u_const_iterator;

		//********************************************************

		/*!
		The primary constructor for the class. Is initialized using the name of the 
		plug in DLL to which it is being added. This name is used to assign call backs to the 
		VBS2 event handler engine to run a pluginFunction[strDLLNAME, "<custom code>"] when
		an event is called. This class will not function properly f a name which is different
		to the name of the DLL is assigned.
		*/
		EventHandler(const std::string& strDLLName);

		/*!
		Primary destructor for the class. 
		*/

		~EventHandler();

		//********************************************************

		/*!
		Sets the name of the DLL. Rarely used function, and is provided only for completeness as 
		the DllName is assigned during construction of the class. 
		*/
		void setDllName(const std::string& strDLLName);

		/*!
		Returns the DllName. This name is used to assign call backs to the 
		VBS2 event handler engine to run a pluginFunction[strDLLNAME, "<custom code>"] when
		an event is called. This class will not function properly f a name which is different
		to the name of the DLL is assigned.
		*/
		std::string getDllName() const;		

		////********************************************************

		/*!
		Begin iterator to browse through the list of ControllableObject pointers which 
		are added automatically to the object every time a FIRED, DEL, RESPAWNED or 
		HITPART event handler is added to the object. 
		*/
		co_iterator co_begin();

		/*!
		Begin const iterator to browse through the list of ControllableObject pointers which 
		are added automatically to the object every time a FIRED, DEL, RESPAWNED or 
		HITPART event handler is added to the object. 
		*/
		co_const_iterator co_begin() const;

		/*!
		End iterator to browse through the list of ControllableObject pointers which 
		are added automatically to the object every time a FIRED, DEL, RESPAWNED or 
		HITPART event handler is added to the object. 
		*/
		co_iterator co_end();

		/*!
		End const iterator to browse through the list of ControllableObject pointers which 
		are added automatically to the object every time a FIRED, DEL, RESPAWNED or 
		HITPART event handler is added to the object. 
		*/
		co_const_iterator co_end() const;		

		////********************************************************

		/*!
		Begin iterator to browse through the list of Group pointers which 
		are added automatically to the object every time a WAYPOINTCOMPLETE
		event handler is added to the object. 
		*/
		g_iterator g_begin();

		/*!
		Begin const iterator to browse through the list of Group pointers which 
		are added automatically to the object every time a WAYPOINTCOMPLETE
		event handler is added to the object. 
		*/
		g_const_iterator g_begin() const;

		/*!
		End iterator to browse through the list of Group pointers which 
		are added automatically to the object every time a WAYPOINTCOMPLETE
		event handler is added to the object. 
		*/
		g_iterator g_end();

		/*!
		End const iterator to browse through the list of Group pointers which 
		are added automatically to the object every time a WAYPOINTCOMPLETE
		event handler is added to the object. 
		*/
		g_const_iterator g_end() const;

		////********************************************************

		/*!
		Begin iterator to browse through the list of Unit pointers which 
		are added automatically to the object every time a GETINMAN, GETOUTMAN or SUPPRESSED
		event handler is added to the object. 
		*/
		u_iterator u_begin();

		/*!
		Begin const iterator to browse through the list of Unit pointers which 
		are added automatically to the object every time a GETINMAN, GETOUTMAN or SUPPRESSED
		event handler is added to the object. 
		*/
		u_const_iterator u_begin() const;

		/*!
		End iterator to browse through the list of Unit pointers which 
		are added automatically to the object every time a GETINMAN, GETOUTMAN or SUPPRESSED
		event handler is added to the object. 
		*/
		u_iterator u_end();

		/*!
		End const iterator to browse through the list of Unit pointers which 
		are added automatically to the object every time a GETINMAN, GETOUTMAN or SUPPRESSED
		event handler is added to the object. 
		*/
		u_const_iterator u_end() const;


		////*************************************************
		//// Fired Event functions
		////*************************************************	

		/*!
		Adds a new Fired event handler to the object. Execution of this function using a 
		valid Controllable object (i.e. a ControllableObject, Unit, Vehicle or Projectile)
		will automatically issue a command to register an event handler with the VBS2
		engine to call back the plugin dll through the pluginFunction interface whenever 
		a fired event is called for the object defined by co. 

		*/
		void addFiredEventHandler(ControllableObject &co);		

		/*!
		This function is automatically called by the function HandleEvent whenever a valid
		'Fired' event is identified. It returns a complete FiredEvent object when a valid
		pluginFunction call for a VBS2Fusion 'Fired' event is parsed through it. This function 
		also makes a call to VBS2 to assign random alias to the fired object. This new alias can be accessed
		using the getFiredObjectAlias method of the FiredEvent. 
		*/

		/*Error checking utility validates the following:

		- outputs an error if an empty string passed
		*/
		FiredEvent parseFiredEvent(const std::string& returnString);

		/*!
		Searches through the list of ControllableObjects on the objects list to 
		find one with a matching name to _event.getObjectName and then calls
		the processFiredEvent method of that object. Returns false if 
		an object with the correct name is not found on the list. 
		*/
		bool processFiredEvent(FiredEvent& _event);

		/*!
		Generates a random alias for a new fired projectile and returns in string format. 
		*/

		//string generateRandomFiredMissileAlias(string uintName);

		/*!
		Issues a command to VBS2 to stop listening for Fired events for this object. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		void removeFiredEventHandler(const ControllableObject& co);

		////*************************************************
		//// Delete Event functions
		////*************************************************		

		/*!
		Adds a new Delete event handler to the object. Execution of this function using a 
		valid Controllable object (i.e. a ControllableObject, Unit, Vehicle or Projectile)
		will automatically issue a command to register an event handler with the VBS2
		engine to call back the plugin dll through the pluginFunction interface whenever 
		a Delete event is called for the object defined by co. 		
		*/
		void addDeleteEventHandler(ControllableObject &co);		

		/*!
		This function is automatically called by the function HandleEvent whenever a valid
		'Delete' event is identified. It returns a complete DeleteEvent object when a valid
		pluginFunction call for a VBS2Fusion 'Delete' event is parsed through it. 

		Error checking utility validates the following:

		- outputs an error if an empty string passed
		*/
		DeleteEvent parseDeleteEvent(const std::string& returnString);

		/*!
		Searches through the list of ControllableObjects on the objects list to 
		find one with a matching name to _event.getObjectName and then calls
		the processDeleteEvent method of that object. Returns false if 
		an object with the correct name is not found on the list. 
		*/
		bool processDeleteEvent(DeleteEvent& _event);	

		/*!
		Issues a command to VBS2 to stop listening for Delete events for this object. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		void removeDeleteEventHandler(const ControllableObject& co);

		////*************************************************
		//// HitPart Event functions
		////*************************************************		

		/*!
		Adds a new HitPart event handler to the object. Execution of this function using a 
		valid Controllable object (i.e. a ControllableObject, Unit, Vehicle or Projectile)
		will automatically issue a command to register an event handler with the VBS2
		engine to call back the plugin dll through the pluginFunction interface whenever 
		a HitPart event is called for the object defined by co. 		
		*/
		void addHitPartEventHandler(ControllableObject &co);

		
		/*!
		This function is automatically called by the function HandleEvent whenever a valid
		HitPart' event is identified. It returns a complete HitPartEvent object when a valid
		pluginFunction call for a VBS2Fusion 'HitPart' event is parsed through it. This function 
		also makes a call to VBS2 to assign random alias to the shooter object and the bullet object. 

		Error checking utility validates the following:

		- outputs an error if an empty string passed
		*/
		HitPartEvent parseHitPartEvent(const std::string& returnString);

		/*!
		Searches through the list of ControllableObjects on the objects list to 
		find one with a matching name to _event.getObjectName and then calls
		the processHitPartEvent method of that object. Returns false if 
		an object with the correct name is not found on the list. 
		*/
		bool processHitPartEvent(HitPartEvent& _event);	

		/*!
		Issues a command to VBS2 to stop listening for HitPart events for this object. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		void removeHitPartEventHandler(const ControllableObject &co);		

		/*!
		Generates and returns a random shooter alias. 
		*/
		std::string generateRandomShooterAlias();

		/*!
		Generates and returns a random bullet alias. 
		*/
		std::string generateRandomBulletAlias();

		/*!
		Generates and returns a random bullet alias. 
		*/
		std::string generateRandomAlias(const std::string& aliasName);

		//*************************************************
		// AttachTo Event functions
		//*************************************************	
		/*!
		Adds a new AttachTo event handler to the object. Execution of this function using a 
		valid Controllable object (i.e. a ControllableObject, Unit, Vehicle or Projectile)
		will automatically issue a command to register an event handler with the VBS2
		engine to call back the plugin dll through the pluginFunction interface whenever 
		a AttachTo event is called for the object defined by co. 		
		*/
		void addAttachToEventHandler(ControllableObject &co);		

		/*!
		This function is automatically called by the function HandleEvent whenever a valid
		'AttachedTo' event is identified. It returns a complete AttachToEvent object when a valid
		pluginFunction call for a VBS2Fusion 'AttachedTo' event is parsed through it.

		Error checking utility validates the following:

		- outputs an error if an empty string passed
		*/
		AttachToEvent parseAttachToEvent(const std::string& returnString);
		
		/*!
		Searches through the list of ControllableObjects on the objects list to 
		find one with a matching name to _event.getObjectName and then calls
		the processAttachedToEvent method of that object. Returns false if 
		an object with the correct name is not found on the list. 
		*/
		bool processAttachToEvent(AttachToEvent& _event);	

		/*!
		Issues a command to VBS2 to stop listening for AttachTo events for this object. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/		
		void removeAttachToEventHandler(const ControllableObject& co);

		//*End of AttachTo Event functions*/

		//************************************************* 
		//End of AttachToEvent Functions
		//**************************************************


		////*************************************************
		//// waypoint complete Event functions
		////*************************************************		

		/*!
		Adds a new WaypointComplete event handler to the object. Execution of this function using a 
		valid Group object. Will automatically issue a command to register an event handler with the VBS2
		engine to call back the plugin dll through the pluginFunction interface whenever 
		a WaypointComplete event is called for the Group defined by g.  		
		*/
		void addWaypointCompleteEventHandler(Group& g);		

		/*!
		This function is automatically called by the function HandleEvent whenever a valid
		'WaypointComplete' event is identified. It returns a complete WaypointCompleteEvent object when a valid
		pluginFunction call for a VBS2Fusion 'WaypointComplete' event is parsed through it.  		
		*/
		WaypointCompleteEvent parseWaypointCompleteEvent(const std::string& returnString);

		/*!
		Searches through the list of Groups list to 
		find one with a matching name to _event.getObjectName and then calls
		the processWaypointCompleteEvent method of that object. Returns false if 
		a group with the correct name is not found on the list. 

		Error checking utility validates the following:

		- outputs an error if an empty string passed
		*/
		bool processWaypointCompleteEvent(WaypointCompleteEvent& _event);	

		/*!
		Issues a command to VBS2 to stop listening for WaypointComplete events for this object. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		void removeWaypointCompleteEventHandler(const Group& g);

		////*************************************************
		//// GetInMan complete Event functions
		////*************************************************		

		///*!
		//Adds a new GetInMan event handler to the object. Execution of this function using a 
		//valid Unit object will automatically issue a command to register an event handler with the VBS2
		//engine to call back the plugin dll through the pluginFunction interface whenever 
		//a fired event is called for the object defined by u. 
		//*/
		void addGetInManEventHandler(Unit& u);		

		///*!
		//This function is automatically called by the function HandleEvent whenever a valid
		//GetInMan' event is identified. It returns a complete GetInManEvent object when a valid
		//pluginFunction call for a VBS2Fusion 'GetInMan' event is parsed through it. This function 
		//also makes a call to VBS2 to assign random alias to the vehicle object. 		
		//*/
		GetInManEvent parseGetInManEvent(const std::string& returnString);

		///*!
		//Searches through the list of Units on the objects list to 
		//find one with a matching name to _event.getObjectName and then calls
		//the processGetInManEvent method of that object. Returns false if 
		//an object with the correct name is not found on the list. 

		//Error checking utility validates the following:

		//- outputs an error if an empty string passed
		//*/
		bool processGetInManEvent(GetInManEvent& _event);	

		///*!
		//Issues a command to VBS2 to stop listening for GetInMan events for this object. 

		//Error checking utility validates the following:

		//- Alias (outputs an error if the alias is invalid)	
		//*/
		void removeGetInManEventHandler(const Unit& u);

		///*!
		//Generates and returns a random vehicle alias. 
		//*/
		//string generateRandomVehicleAlias();

		////*************************************************
		//// GetOutMan Event functions
		////*************************************************		

		///*!
		//Adds a new GetOutMan event handler to the object. Execution of this function using a 
		//valid Unit object will automatically issue a command to register an event handler with the VBS2
		//engine to call back the plugin dll through the pluginFunction interface whenever 
		//a fired event is called for the object defined by u. 
		//*/
		void addGetOutManEventHandler(Unit& u);		

		///*!
		//This function is automatically called by the function HandleEvent whenever a valid
		//GetOutMan' event is identified. It returns a complete GetOutManEvent object when a valid
		//pluginFunction call for a VBS2Fusion 'GetOutMan' event is parsed through it. This function 
		//also makes a call to VBS2 to assign random alias to the vehicle object. 

		//Error checking utility validates the following:

		//- outputs an error if an empty string passed
		//*/
		GetOutManEvent parseGetOutManEvent(const std::string& returnString);

		///*!
		//Searches through the list of Units on the objects list to 
		//find one with a matching name to _event.getObjectName and then calls
		//the processGetOutManEvent method of that object. Returns false if 
		//an object with the correct name is not found on the list. 
		//*/
		bool processGetOutManEvent(GetOutManEvent& _event);	

		///*!
		//Issues a command to VBS2 to stop listening for GetOutMan events for this object. 

		//Error checking utility validates the following:

		//- Alias (outputs an error if the alias is invalid)	
		//*/
		void removeGetOutManEventHandler(const Unit& u);

		////*************************************************
		//// Suppressed Event functions
		////*************************************************		

		///*!
		//Adds a new Suppressed event handler to the object. Execution of this function using a 
		//valid Unit object will automatically issue a command to register an event handler with the VBS2
		//engine to call back the plugin dll through the pluginFunction interface whenever 
		//a fired event is called for the object defined by u. 
		//*/
		void addSuppressedEventHandler(Unit& u);		

		///*!
		//This function is automatically called by the function HandleEvent whenever a valid
		//'Suppressed' event is identified. It returns a complete SuppressedEvent object when a valid
		//pluginFunction call for a VBS2Fusion 'Suppressed' event is parsed through it. This function 
		//also makes a call to VBS2 to assign random alias to the firing unit. 

		//Error checking utility validates the following:

		//- outputs an error if an empty string passed
		//*/
		SuppressedEvent parseSuppressedEvent(const std::string& returnString);

		///*!
		//Searches through the list of Units on the objects list to 
		//find one with a matching name to _event.getObjectName and then calls
		//the processSuppressedEvent method of that object. Returns false if 
		//an object with the correct name is not found on the list. 
		//*/
		bool processSuppressedEvent(SuppressedEvent& _event);	

		///*!
		//Issues a command to VBS2 to stop listening for Suppressed events for this object. 

		//Error checking utility validates the following:

		//- Alias (outputs an error if the alias is invalid)	
		//*/
		void removeSuppressedEventHandler(const Unit& u);

		////*************************************************
		//// Respawn Event functions
		////*************************************************		

		/*!
		Adds a new Respawn event handler to the object. Execution of this function using a 
		valid Controllable object (i.e. a ControllableObject, Unit, Vehicle or Projectile)
		will automatically issue a command to register an event handler with the VBS2
		engine to call back the plugin dll through the pluginFunction interface whenever 
		a Respawn event is called for the object defined by co. 		
		*/
		void addRespawnEventHandler(ControllableObject &co);		

		///*!
		//This function is automatically called by the function HandleEvent whenever a valid
		//'Respawn' event is identified. It returns a complete RespawnEvent object when a valid
		//pluginFunction call for a VBS2Fusion 'respawn' event is parsed through it. This function 
		//also makes a call to VBS2 to assign random alias to the newly created (i.e. respawned).

		//Error checking utility validates the following:

		//- outputs an error if an empty string passed
		//*/
		RespawnEvent parseRespawnEvent(const std::string& returnString);

		///*!
		//Searches through the list of ControllableObjects on the objects list to 
		//find one with a matching name to _event.getObjectName and then calls
		//the processRespawnEvent method of that object. Returns false if 
		//an object with the correct name is not found on the list. 
		//*/
		bool processRespawnEvent(RespawnEvent& _event);	

		///*!
		//Issues a command to VBS2 to stop listening for Respawn events for this object. 

		//Error checking utility validates the following:

		//- Alias (outputs an error if the alias is invalid)	
		//*/		
		void removeRespawnEventHandler(const ControllableObject& co);


		////*************************************************
		//// AmmoHit Event functions
		////*************************************************		

		/*!
		Adds a new AmmoHit event handler to the Ammo object. Execution of this function using a 
		valid ********* object will automatically issue a command to register an event handler with the VBS2
		engine to call back the plugin dll through the pluginFunction interface when AmmoHit event
		is called for the object defined by co. 		
		*/
		void addAmmoHitEventHandler(const ControllableObject &co);	


		//*************************************************
		// Primary event Handler
		//*************************************************		

		/*!
		Primary event handler of the HandleEvent class. Should be added to 
		the pluginFunction section of your plugin DLL and pass in the return string to 
		identify and parse a new event. The current version of VBS2Fusion can handle the following types of events:
		- Fired
		- HitPart
		- GetInMan
		- GetOutMan
		- Delete
		- Suppressed
		- WaypointComplete
		- Respawn

		/*Events can be identified within the pluginFunction conditional structure by checking for 
		a opening square bracket (i.e. "[") as the first character on the return string. 

		e.g. using a structure similar to the following:

		if (input[0] == '[')

		  {

			  string strInput = input;

			  int returnVal = eh.HandleEvent(strInput);	

			  // place the rest of your event handing code here. 

		  }

		/*Returns
		- '1' if a correct event was identified, the object was present on the list and the relevant
		process event function was called. 
		- '0' if a correct event was identified, but a relevant object was not found on the ControllableObject, Unit or Group
		list.
		- '-1' if not a valid event and nothing was done. 

		Error checking utility validates the following:

		- outputs an error if the FIRE event called on invalid object
		- outputs an error if the DELETE event called on invalid object
		- outputs an error if the HITPART event called on invalid object
		- outputs an error if the WAYPOINTCOMPLETE event called on invalid object
		- outputs an error if the GETINMAN event called on invalid object
		- outputs an error if the GETOUTMAN event called on invalid object
		- outputs an error if the SUPPRESSED event called on invalid object
		- outputs an error if the RESPAWN event called on invalid object
		*/
		int HandleEvent(const std::string& returnString);

		/*!
		clear all event handlers 
		*/
		void clearAll();



	private:

		std::string DLLNAME;
		CobjectList _objectList;
		GList _groupList;
		UList _unitList;

	private:

		void addControllableObject(ControllableObject& co);

		void addGroup(Group& g);

		void addUnit(Unit& u);

	};
};

#pragma warning (pop) // Enable warnings [C:4251]

#endif //EVENT_HANDLER_H
