/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	AnimChangedEvent.h

Purpose:

	This file contains the declaration of the AnimChangedEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			08-07/2011	CGS: Original Implementation


/************************************************************************/

#ifndef VBS2FUSION_ANIM_CHANGED_EVENT_H
#define VBS2FUSION_ANIM_CHANGED_EVENT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Event.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{

	/*!
	Defines a AnimChanged event which is passed onto the object when an 'ANIMCHANGED' 
	event is called. 
	*/
	class VBSFUSION_API AnimChangedEvent: public Event
	{
	public:

		/*!
		Primary constructor for the class. Calls setType(ANIMCHANGED) to initialize. 
		*/
		AnimChangedEvent();

		/*!
		Primary destructor for the class. 
		*/
		~AnimChangedEvent();

		/*!
		Sets the name of the animation that started.
		*/
		void setAnimation(const std::string& animation);

		/*!
		Gets the name of the animation that started.
		*/
		std::string getAnimation() const;

	private:

		std::string _anim;
	};
};

#endif //ANIM_CHANGED_EVENT_H