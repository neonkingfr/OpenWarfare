/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	IncomingMissile.h

Purpose:

	This file contains the declaration of the IncomingMissileEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			08-07/2011	CGS: Original Implementation
/************************************************************************/

#ifndef VBS2FUSION_INCOMINGMISSILE_EVENT_H
#define VBS2FUSION_INCOMINGMISSILE_EVENT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Events/Event.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{
	
	/*!
	Defines a fired event which is passed onto the object 
	when an 'FIRED' event is called. Usually passed into the processFiredEvent
	method of a ControllbleObject. 
	*/

	class VBSFUSION_API IncomingMissileEvent: public Event
	{
	public:

		/*!
		Primary constructor for the class. Calls setType(INCOMINGMISSILE) to initialize. 
		*/
		IncomingMissileEvent();

		/*!
		Primary destructor for the class. 
		*/
		~IncomingMissileEvent();
		
		/*!
		Sets the class name of the ammo type used. 
		*/
		void setAmmoType(const std::string& ammo);

		/*!
		Returns the class name of the ammo type used. 
		*/
		std::string getAmmoType() const;

		/*!
		Sets the VBS2 name of the Object that fired the weapon.
		*/
		void setFiredObjectName(const std::string& name);

		/*!
		Gets the VBS2 name of the Object that fired the weapon.
		*/
		std::string getFiredObjectName() const;

		/*!
		Sets the VBS2Fusion alias of the object that that fired the weapon. This value should be a valid
		alias which can be used to initialize a new object using setAlias() to create, update
		and manipulate it.
		*/
		void setFiredObjectAlias(const std::string& alias);

		/*!
		Gets the VBS2Fusion alias of the object that that fired the weapon. This value should be a valid
		alias which can be used to initialize a new object using setAlias() to create, update
		and manipulate it.
		*/
		std::string getFiredObjectAlias() const;

		/*!
		Sets the VBS2 ID of the Object that fired the weapon.
		*/
		void setFiredObjectID(const NetworkID& id);

		/*!
		Gets the VBS2 ID of the Object that fired the weapon.
		*/
		NetworkID getFiredObjectID() const;

	private:

		std::string _strAmmo;
		std::string _strFiredObj;
		NetworkID _strFiredObjId;
		std::string _strFiredObjAlias;
	};
};

#endif //FIRED_EVENT_H