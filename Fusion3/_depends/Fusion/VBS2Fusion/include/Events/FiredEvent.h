/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	FiredEvent.h

Purpose:

	This file contains the declaration of the FiredEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			17-4/2009	RMR: Original Implementation
	1.01		27-8/2009	SDS: Added setProjectileAlias(string alias)
									   getProjectileAlias()
    2.01		10-02/2010	MHA: Comments Checked
/************************************************************************/

#ifndef VBS2FUSION_FIRED_EVENT_H
#define VBS2FUSION_FIRED_EVENT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Events/Event.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{
	
	/*!
	Defines a fired event which is passed onto the object 
	when an 'FIRED' event is called. Usually passed into the processFiredEvent
	method of a ControllbleObject. 
	*/

	class VBSFUSION_API FiredEvent: public Event
	{
	public:
		/*!
		Primary constructor for the class. Calls setType(FIRED) to initialize. 
		*/
		FiredEvent();

		/*!
		Primary destructor for the class. 
		*/
		~FiredEvent();

		/*!
		Sets the class name of the weapon which was fired. 
		*/
		void setWeapon(const std::string& weapon);

		/*!
		Returns the class name of the weapon which was fired. 
		*/
		std::string getWeapon() const;

		/*!
		Sets the name of the muzzle which was used during firing. 
		*/
		void setMuzzle(const std::string& muzzle);

		/*!
		Returns the name of the muzzle which was used during firing. 
		*/
		std::string getMuzzle() const;

		/*!
		Sets the current mode of the fired weapon. 
		*/
		void setMode(const std::string& mode);

		/*!
		Returns the current mode of the fired weapon. 
		*/
		std::string getMode() const;

		/*!
		Sets the class name of the ammo type used. 
		*/
		void setAmmo(const std::string& ammo);

		/*!
		Returns the class name of the ammo type used. 
		*/
		std::string getAmmo() const;

		/*!
		Sets the class name of the magazine type used. 
		*/
		void setMagazine(const std::string& magazine);

		/*!
		Returns the class name of the magazine type used. 
		*/
		std::string getMagazine() const;

		/*!
		Sets the VBS2 name of the projectile which was fired. 
		*/
		void setProjectile(const std::string& projectile);

		/*!
		Returns the VBS2 name of the projectile which was fired. 
		*/
		std::string getProjectile() const;

		/*!
		Sets the VBS2Fusion network id of the projectile which was fired. This 
		id can be used to create a new Projectile object by calling
		its setAlias method to update and manipulate the newly created projectile. 
		*/
		void setProjectileId(const std::string& id);

		/*!
		Returns the VBS2Fusion network id of the projectile which was fired. This 
		id can be used to create a new Projectile object by calling
		its setAlias method to update and manipulate the newly created projectile. 
		*/
		std::string getProjectileId() const;

		/*!
		Sets the VBS2Fusion NetworkID id of the projectile which was fired.
		*/
		void setProjectileID(const NetworkID& id);

		/*!
		Returns the VBS2Fusion NetworkID id of the projectile which was fired. 
		*/
		NetworkID getProjectileID() const;

		/*!
		Sets the VBS2Fusion alias of the projectile which was fired. This 
		alias can be used to create a new Projectile object by calling
		its setAlias method to update and manipulate the newly created projectile. 
		*/
		void setProjectileAlias(const std::string& alias);

		/*!
		Returns the VBS2Fusion alias of the projectile which was fired. This 
		alias can be used to create a new Projectile object by calling
		its setAlias method to update and manipulate the newly created projectile. 
		*/
		std::string getProjectileAlias() const;


	private:

		std::string _strWeapon;
		std::string _strMuzzle;
		std::string _strMode;
		std::string _strAmmo;
		std::string _strMagazine;
		std::string _strProjectile;
		std::string _strProjectileid;
		std::string _strProjectileAlias;
	};
};

#endif //FIRED_EVENT_H