/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	AmmoDeleteEvent.h

Purpose:

	This file contains the declaration of the AmmoDeleteEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			04-08/2011	NDB: Original Implementation


/************************************************************************/

#ifndef VBS2FUSION_AMMO_DELETE_EVENT_H
#define VBS2FUSION_AMMO_DELETE_EVENT_H


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Event.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{

	/*!
	Defines a AmmoDelete event which is passed onto the object when an 'AMMODELETE' 
	event is called. 
	*/
	class VBSFUSION_API AmmoDeleteEvent: public Event
	{
	public:
		/*!
		Primary constructor for the class. Calls setType(AMMODELETE) to initialize. 
		*/
		AmmoDeleteEvent();

		/*!
		Primary destructor for the class. 
		*/
		~AmmoDeleteEvent();	

		/*!
		Sets the VBS2 name of the Object that fired shot.
		*/
		void setShooterName(const std::string& name);

		/*!
		Gets the VBS2 name of the Object that fired shot.
		*/
		std::string getShooterName() const;

		/*!
		Sets the VBS2 ID of the Object that fired shot.
		*/
		void setShooterID(const NetworkID& id);

		/*!
		Gets the VBS2 ID of the Object that fired shot.
		*/
		NetworkID getShooterID() const;

		/*!
		Sets the VBS2 name of the Object that was hit, or objnull if the ground was hit. 
		*/
		void setTargetName(const std::string& name);

		/*!
		Gets the VBS2 name of the Object that was hit, or objnull if the ground was hit. 
		*/
		std::string getTargetName() const;

		/*!
		Sets the position the bullet impacted in PositionASL coordinates.
		*/
		void setImpactPosition(const position3D& pos);

		/*!
		Gets the position the bullet impacted in PositionASL coordinates.
		*/
		position3D getImpactPosition() const;

		/*!
		Sets the 3D speed at which bullet impacted.
		*/
		void setImpactBulletVelocity(const vector3D& velocity);

		/*!
		Gets the 3D speed at which bullet impacted.
		*/
		vector3D getImpactBulletVelocity() const;

		/*!
		Set the ammo hit value.
		*/
		void setAmmoHitValue(double val);

		/*!
		Returns the ammo hit value.
		*/
		double getAmmoHitValue() const;

		/*!
		Set the ammo Indirect hit value.
		*/
		void setAmmoIndirectHitValue(double val);

		/*!
		Returns the ammo Indirect hit value.
		*/
		double getAmmoIndirectHitValue() const;

		/*!
		Set the ammo Indirect hit Range.
		*/
		void setAmmoIndirectHitRange(double range);

		/*!
		Get the ammo Indirect hit Range.
		*/
		double getAmmoIndirectHitRange() const;

		/*!
		Set the direction vector that is perpendicular to the surface struck.
		*/
		void setDirection(const vector3D& dir);

		/*!
		Get the direction vector that is perpendicular to the surface struck.
		*/
		vector3D getDirection() const;

		/*!
		Set if the ammo was exploded or was destroyed.
		*/
		void setExploded(bool exploded);

		/*!
		Tells if the ammo was exploded or was destroyed.
		*/
		bool isExploded() const;

		/*!
		Set the position relative to the object to be attached
		*/
		void setAmmoExplosiveDamage(double damage);

		/*!
		Returns the position relative to attached object
		*/
		double getAmmoExplosiveDamage() const;


	private:
		std::string _shooterName;
		NetworkID _shooterID;
		std::string _targetName;
		position3D _impactedPosition;
		vector3D _velocity;
		double _ammoHitValue;
		double _ammoIndirectHitValue;
		double _ammoIndirectHitRange;
		double _ammoExplosiveDamage;
		vector3D _direction;
		bool _exploded;

	};
};

#endif //AMMO_DELETE_EVENT_H