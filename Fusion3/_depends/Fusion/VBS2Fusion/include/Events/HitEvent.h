/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	HitEvent.h

Purpose:

	This file contains the declaration of the HitEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			08-07/2011	CGS: Original Implementation


/************************************************************************/

#ifndef VBS2FUSION_HIT_EVENT_H
#define VBS2FUSION_HIT_EVENT_H


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Event.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{

	/*!
	Defines a Hit event which is passed onto the vehicle object when a 'HIT' 
	event is called. 
	*/
	class VBSFUSION_API HitEvent: public Event
	{
	public:

		/*!
		Primary constructor for the class. Calls setType(HIT) to initialize. 
		*/
		HitEvent();

		/*!
		Primary destructor for the class. 
		*/
		~HitEvent();

		/*!
		Sets the VBS2 name of the Object that hit the unit.
		*/
		void setHitObjectName(const std::string& name);

		/*!
		Gets the VBS2 name of the Object that hit the unit.
		*/
		std::string getHitObjectName() const;

		/*!
		Sets the VBS2Fusion alias of the object that that hit the unit. This value should be a valid
		alias which can be used to initialize a new object using setAlias() to create, update
		and manipulate it.
		*/
		void setHitObjectAlias(const std::string& alias);

		/*!
		Gets the VBS2Fusion alias of the object that that hit the unit. This value should be a valid
		alias which can be used to initialize a new object using setAlias() to create, update
		and manipulate it.
		*/
		std::string getHitObjectAlias() const;

		/*!
		Sets the VBS2 ID of the Object that hit the unit.
		*/
		void setHitObjectID(const NetworkID& id);

		/*!
		Gets the VBS2 ID of the Object that hit the unit.
		*/
		NetworkID getHitObjectID() const;

		/*!
		Sets the resulting level of damage caused by the hit.
		*/
		void setDamage(double damage);

		/*!
		Gets the resulting level of damage caused by the hit.
		*/
		double getDamage() const;

	private:

		std::string _hitObjName;
		std::string _hitObjAlias;
		NetworkID _hitObjID;
		double _damage;
	};

};

#endif //HIT_EVENT_H