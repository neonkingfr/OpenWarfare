/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	PositionChangedEvent.h

Purpose:

	This file contains the declaration of the PositionChangedEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			08-07/2011	CGS: Original Implementation


/************************************************************************/

#ifndef VBS2FUSION_POSITION_CHANGED_EVENT_H
#define VBS2FUSION_POSITION_CHANGED_EVENT_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push) 
#pragma warning (disable: 4251)


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Event.h"
#include "data\Turret.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{

	/*!
	Defines a PositionChanged event which is passed onto the vehicle object when an 'POSITIONCHANGED' 
	event is called. 
	*/
	class VBSFUSION_API PositionChangedEvent: public Event
	{
	public:

		/*!
		Primary constructor for the class. Calls setType(POSITIONCHANGED) to initialize. 
		*/
		PositionChangedEvent();

		/*!
		Primary destructor for the class. 
		*/
		~PositionChangedEvent();

		/*!
		Sets the VBS2 name of the unit that entered/exited/moved in the vehicle.
		*/
		void setUnitName(const std::string& name);

		/*!
		Gets the VBS2 name of the unit that entered/exited/moved in the vehicle.
		*/
		std::string getUnitName() const;

		/*!
		Sets the VBS2Fusion alias of the unit that entered/exited/moved in the vehicle. This value 
		should be a valid alias which can be used to initialize a new object using setAlias() to 
		create, update	and manipulate it.
		*/
		void setUnitAlias(const std::string& alias);

		/*!
		Gets the VBS2Fusion alias of the unit that entered/exited/moved in the vehicle. This value 
		should be a valid alias which can be used to initialize a new object using setAlias() to 
		create, update	and manipulate it.
		*/
		std::string getUnitAlias() const;

		/*!
		Sets the VBS2 ID of the unit that entered/exited/moved in the vehicle.
		*/
		void setUnitID(const NetworkID& id);

		/*!
		Gets the VBS2 ID of the unit that entered/exited/moved in the vehicle.
		*/
		NetworkID getUnitID() const;

		/*!
		Sets the position or the role the person was in the vehicle. 
		*/
		void setStartedAssignment(const std::string& assignment);

		/*!
		Returns the position or the role the person was in the vehicle. 
		*/
		std::string getStartedAssignment() const;

		/*!
		Sets the turret path the person was in the vehicle. 
		*/
		void setStartedTurretPath(const Turret::TurretPath& turretpath);

		/*!
		Gets the turret path the person was in the vehicle. 
		*/
		Turret::TurretPath getStartedTurretPath() const;

		/*!
		Sets the cargo position person was in the vehicle.  
		*/
		void setStartedCargoIndex( int index);

		/*!
		Gets the cargo position person was in the vehicle. 
		*/
		int getStartedCargoIndex() const;

		/*!
		Sets the position or the role the person moved into the vehicle. 
		*/
		void setChangedAssignment(const std::string& assignment);

		/*!
		Returns the position or the role the person moved into the vehicle. 
		*/
		std::string getChangedAssignment() const;

		/*!
		Sets the turret path the person moved into the vehicle. 
		*/
		void setChangedTurretPath(const Turret::TurretPath& turretpath);

		/*!
		Gets the turret path the person moved into the vehicle. 
		*/
		Turret::TurretPath getChangedTurretPath() const;

		/*!
		Sets the cargo position person moved into the vehicle.  
		*/
		void setChangedCargoIndex(int index);

		/*!
		Gets the cargo position person moved into the vehicle. 
		*/
		int getChangedCargoIndex() const;

	private:

		std::string _unitName;
		std::string _unitAlias;
		NetworkID _unitID;
		std::string _startedAssign;
		Turret::TurretPath _startedTuPath;
		int _startedCargoIndex; 
		std::string _changedAssign;
		Turret::TurretPath _changedTupath;
		int _changedCargoIndex; 

	};

};

#pragma warning(pop) // Enable warnings [C:4251]

#endif //ENGINE_EVENT_H