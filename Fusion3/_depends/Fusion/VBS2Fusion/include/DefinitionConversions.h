
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:		DefinitionConversions.h

Purpose:	Contains the conversion functions for the ENUMs defined in "VBSFusionDefinitions.h"
	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			29-09-2011	SSD: Original Implementation

/*****************************************************************************/
#ifndef VBS2FUSION_DEFINITIONCONVERSIONS_H
#define VBS2FUSION_DEFINITIONCONVERSIONS_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusionDefinitions.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{
	class VBSFUSION_API DefinitionConversions
	{
	public:
		/*!
		returns the name string for the application state
		*/
		static std::string getApplicationStateString(APPLICATIONSTATE state);

		/*!
		Returns the application state enum for the name string
		*/
		static APPLICATIONSTATE getApplicationState(const std::string& state);

		/*!
		return string of file type
		*/
		static std::string getPicFileTypeString(PICTURE_FILETYPE fileType);
		
		/*!
		returns View parameter string
		*/
		static std::string getViewParemeterString(VIEW_TYPE type);

		/*!
		returns the name string for UnitMove
		*/
		static std::string getUnitMoveString(UNITMOVE move);

		/*!
		returns the name string for path post process mode
		*/
		static std::string getPathPostProcessModeString(PATHPOSTPROCESSMODE mode);

		/*!
		returns the name string for waypoint speed mode
		*/
		static std::string getWayPointSpeedModeString(WAYPOINTSPEEDMODE speedMode);

		/*!
		returns the name string for the Formation
		*/
		static std::string getFormationString(FORMATION formation);

		/*!
		returns the name string for the LockState
		*/
		static std::string getLockStateString(LOCK_STATE state);

		/*!
		returns the name string for the light mode
		*/
		static std::string getLightModeString(LIGHT_MODE mode);

		/*!
		returns the name string for the unit's face Mimic
		*/
		static std::string getWayPointCombatModeString(WAYPOINTCOMBATMODE mode);

		/*!
		returns the name string for the unit's face Mimic
		*/
		static std::string getMiminString(MIMIC mimic);

		/*!
		returns the name string for the unit's face type 
		*/
		static std::string getFaceTypeString(FACETYPE face);

		/*!
		returns the name string for land mode of planes
		*/
		static std::string getLandModeString(LANDMODE mode);

		/*!
		returns the name string for the unit's body parts
		*/
		static std::string getBodypartString(BODYPART bodyPart);

		/*!
		returns the name string for the unit's body parts
		*/
		static std::string getMainBodypartString(MAIN_BODYPART bodyPart);

		/*!
		returns the name string for the unit's body parts
		*/
		static std::string getBodySegmentString(BODY_SEGMENT segment);

		/*!
		returns the name string for the vehicle Assignment Type
		*/
		static std::string getVehicleAssignmentTypeString(VEHICLEASSIGNMENTTYPE type);

		/*!
		returns the name string for the unit's AI behavior.
		*/
		static std::string getAIBehaviourString(AIBEHAVIOUR behaviour); 

		/*!
		returns the name string for the unit behavior.
		*/
		static std::string getBehaviourString(BEHAVIOUR behaviour);

		/*!
		returns the name string for the unit rank.
		*/
		static std::string getRankString(VBSRANK rank);

		/*!
		returns the name string for the collision test type.
		*/
		static std::string getCollisionTestTypeString(COLLISIONTESTTYPE testType);

		/*!
		returns the name string for the unit position.
		*/
		static std::string getUnitPosString(UNITPOS pos);

		/*!
		returns the name string for the camera mode.
		*/
		static std::string getCamModeString(CAMMODETYPE camMode);

		/*!
		returns the name string for camera effect mode.
		*/
		static std::string getCamEffectModeString(CAMEFFECTMODE cameffectMode);

		/*!
		returns the command type string of Camera command type. 
		*/
		static std::string getCamCommandTypeString(CAMCOMMANDTYPE camcomtype);

		/*!
		returns the name string for the Side.
		*/
		static std::string getSideString(SIDE side);

		/*!
		returns the enum SIDE
		*/
		static SIDE getSide(const std::string& strSide);

		/*!
		returns the name string for the Mine type.
		*/
		static std::string getMineTypeString(MINETYPE type);

		/*!
		returns simulation value. 
		NORMAL  - 0
		FROZEN  - 1
		HIDDEN  - 2
		*/
		static int getSimulationModeValue(SIMULATION_MODE mode);

		/*!
		returns the road side.
		ROAD_LEFT - left
		ROAD_MIDDLE - middle 
		ROAD_RIGHT - right
		*/
		static std::string getRoadSideString(ROADSIDE side);

		/*!
		return the COST_TYPE Enum.
		*/
		static COST_TYPE getCostType(const std::string& strCost);

		/*!
		return the name string of the cost type.
		*/
		static std::string getCostTypeString(COST_TYPE type);

		/*!
		return the name string of the draw mode.
		*/
		static std::string getDrawModeString(DRAWMODE mode);

		/*!
		return the string of text effect type
		*/

		static std::string getTextEffectType(TITLE_EFFECT_TYPE type);

		/*!
		return the string of text for vehicle sound types
		*/
		static std::string getVehicleSoundType(VEHICLE_SOUND_TYPES type);

		/*!
		return the string of vehicle related actions
		*/
		static std::string getVehicleActionType(VEHICLE_ACTION action);

		/*
		Return the string of CombatMode enum parameter.
		*/
		static std::string getCombatModeString( COMBATMODE mode );

		/*
		Return the relavant COMBATMODE enum parameter.
		*/
		static COMBATMODE getCombatMode( const std::string& mode );

		/*!
		Return the relevant GEOMETRY_COORDINATE_TYPE enum parameter
		*/
		static GEOMETRY_COORDINATE_TYPE getGeometryCoordinateType(const std::string& coordinateType);

		/*!
		return the string of text effect class
		*/
		static std::string getTextEffectClass(TITLE_EFFECT_CLASS className);

		/*!
		return the name string of the capability type.
		*/
		static std::string getCapabilityModeString(CAPABILITYMODE mode);
	};

};

#endif