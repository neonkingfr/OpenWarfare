
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	VBS2Fusion.h

Purpose:

	This file contains the declaration of the VBS2Fusion class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			23-03/2009	RMR: Original Implementation
	1.001		18-09/2009	RMR: Added the VisibilityUtilities class to the definition list
	2.01		10-02/2010	MHA: Comments checked
	2.02        14-06/2010  YFP: added new typedef vector3D

/************************************************************************/

#ifndef VBS2Fusion_H
#define VBS2Fusion_H

#define VBS2FUSION_ERROR -1

#include <iostream>
#include <sstream>
#include <string>
#include <vector>


#include "VBS2FusionDeprecationDef.h"

#ifdef BISIM_DEV
#include "VBS2FusionBISIM.h"
#endif

#ifdef VBS2FUSION_EXPORTS
#define VBSFUSION_API __declspec(dllexport)
#else
#define VBSFUSION_API __declspec(dllimport)
#endif

#define WINAPI _stdcall

typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);
VBSFUSION_API void WINAPI setExecuteCommandFunction(void *executeCommandFnc);
VBSFUSION_API void WINAPI reloadObjects();

using namespace std;


typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

VBSFUSION_API void WINAPI setExecuteCommandFunction(void *executeCommandFnc);

VBSFUSION_API void WINAPI reloadObjects();


namespace VBSFusion
{
	
class conversions;
class DisplayFunctions;
class position2D;
class position3D;
class VBS2Fusion_ErrorContext;
class VBS2FusionAppContext;
class VBS2FusionDefinitions;

class ArmedGroundVehicle;
class ArmedModule;
class Camera;
class ControllableObject;
class EnvironmentState;
class Explosive;
class GeomObject;
class GroundVehicle;
class Group;
class GroupList;
class Magazine;
	class MapObject;
class Marker;
class Mine;
class Mission;
class NetworkID;
class Projectile;
class Target;
class Trigger;
class TriggerList;
class Turret;
class Unit;
class UnitArmedModule;
class vbs2Types;
class VBS2Variable;
class Vehicle;
class VehicleArmedModule;
class VehicleList;
class Waypoint;
class Weapon;

class AfterGetInEvent;
class AfterGetInManEvent;
class AfterGetOutEvent;
class AfterGetOutManEvent;
class AmmoDeleteEvent;
class AmmoExplodeEvent;
class AmmoHitEvent;
class AnimChangedEvent;
class AnimDoneEvent;
class AttachToEvent;
class BeforeGetInEvent;
class BeforeGetInManEvent;
class BeforeGetOutEvent;
class BeforeGetOutManEvent;
class BeforeKilledEvent;
class CargoChangedEvent;
class ChangedWeaponEvent;
class DamagedEvent;
class DeletedEvent;
class DeleteEvent;
class EngineEvent;
class Event;
class EventHandler;
class EventHandlerEx;
class FiredEvent;
class FuelEvent;
class GearEvent;
class GetInEvent;
class GetInManEvent;
class GetOutEvent;
class GetOutManEvent;
class GroupChangedEvent;
class GroupChangedRTEEvent;
class HitEvent;
class HitPartEvent;
class IncomingMissileEvent;
class InitEvent;
class KilledEvent;
class LandedStoppedEvent;
class LandedTouchDownEvent;
class LoadOutChangedEvent;
class MuzzleFiredEvent;
class ObcjCreateEvent;
class PasteEvent;
class PositionChangedEvent;
class RespawnEvent;
class SuppressedEvent;
class TurnInEvent;
class TurnOutEvent;
class VBS2EventTypes;
class WaypointCompleteEvent;

class Matrix4f;
class Vector3f;

class AARUtilities;
class ArmedGroundVehicleUtilities;
class ArmedModuleUtilities;
class CameraUtilities;
class CollisionUtilities;
class ControllableObjectUtilities;
class EffectsUtilities;
class EnvironmentStateUtilities;
class ErrorHandleUtilities;
class ExecutionUtilities;
class ExplosiveUtilities;
class GeneralUtilities;
class GroupListUtilities;
class GroupUtilities;
class InputUtilities;
	class JointUtilities;
class MarkerUtilities;
	class MapUtilities;
class MineUtilities;
class MissionConfigUtilities;
class MissionUtilities;
class ObjectVectorUtilities;
class OrderedCommandQueue;
class PlayerUtilities;
class PoseControlUtilities;
class RTEUtilities;
class TerrainUtilities;
class TIUtilities;
class TriggerListUtilities;
class TriggerUtilities;
class TurretUtilities;
class UnitUtilities;
class VariableUtilities;
class vbs2TypesUtilities;
class VehicleListUtilities;
class VehicleUtilities;
class VisibilityUtilities;
class WaypointUtilities;
class WeaponUtilities;
class WorldUtilities;
	
	struct VBS2Var;


	/*! struct for store position in the UTM system. */
	struct UTM_POSITION
	{
		double EASTING;
		double NORTHING;
		double ZONE;
		string HEMISPHERE;
	};

	struct Color_RGBA 
	{
		double r;
		double g;
		double b;
		double a;
	};

	struct Color_RGB 
	{
		double r;
		double g;
		double b;
	};

	typedef position3D vector3D;

};

namespace VBS2Fusion = VBSFusion;

#endif //VBS2Fusion_H

