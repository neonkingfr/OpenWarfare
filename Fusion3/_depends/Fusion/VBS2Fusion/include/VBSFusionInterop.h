/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************
Name:

VBSFusionInterop.h

Purpose:

/************************************************************************/
#ifndef VBSFUSION_INTEROP_H
#define VBSFUSION_INTEROP_H
#include "VBSFusion.h"
// Appearance Bit Masks used for entities
static const unsigned int INTEROP_STATUS_FIREPOWER_DISABLED    = 0x0001;
static const unsigned int INTEROP_STATUS_MOBILITY_DISABLED     = 0x0002; 
static const unsigned int INTEROP_STATUS_DESTROYED             = 0x0004;
static const unsigned int INTEROP_STATUS_SMOKE_PLUME           = 0x0008; 
static const unsigned int INTEROP_STATUS_ENGINE_SMOKE          = 0x0010; 
static const unsigned int INTEROP_STATUS_FLAMES_PRESENT        = 0x0020; 
static const unsigned int INTEROP_STATUS_POWER_PLANT_ON        = 0x0040; 
static const unsigned int INTEROP_STATUS_CONCEALED             = 0x0080; 
static const unsigned int INTEROP_STATUS_TENT_DEPLOYED         = 0x0100; 
static const unsigned int INTEROP_STATUS_RAMP_DEPLOYED         = 0x0200; 
static const unsigned int INTEROP_STATUS_LAUNCHER_FLASH        = 0x0400; 
static const unsigned int INTEROP_STATUS_LAUNCHER_RAISED       = 0x0800; 
static const unsigned int INTEROP_STATUS_AFTERBURNER_ON        = 0x1000;

// Light state Bit Masks used for game entities
static const unsigned int INTEROP_LIGHT_STATE_HEAD            = 0x0001;
static const unsigned int INTEROP_LIGHT_STATE_TAIL            = 0x0002;
static const unsigned int INTEROP_LIGHT_STATE_BRAKE           = 0x0004;
static const unsigned int INTEROP_LIGHT_STATE_BLACKOUT        = 0x0008;
static const unsigned int INTEROP_LIGHT_STATE_BLACKOUT_BRAKE  = 0x0010;
static const unsigned int INTEROP_LIGHT_STATE_RUNNING         = 0x0020;
static const unsigned int INTEROP_LIGHT_STATE_SPOT            = 0x0040;
static const unsigned int INTEROP_LIGHT_STATE_FLASHLIGHT      = 0x0080;
static const unsigned int INTEROP_LIGHT_STATE_INTERIOR        = 0x0100;
static const unsigned int INTEROP_LIGHT_STATE_LANDING         = 0x0200;
static const unsigned int INTEROP_LIGHT_STATE_NAV             = 0x0400;
static const unsigned int INTEROP_LIGHT_STATE_ACL             = 0x0800;

/**
 * Used to designate the stance of an entity
 */
enum Interop_StanceType
{
    Stance_None = 0,
    Stance_StandingStill,
    Stance_StandingWalk,
    Stance_StandingRun,
    Stance_Kneeling,
    Stance_Prone,
    Stance_Crawling,
    Stance_Swimming,
    Stance_Parachuting,
    Stance_Jumping,
    Stance_Sitting,
    Stance_Squatting,
    Stance_Crouching,
    Stance_Wading,
    Stance_Surrendering,
    Stance_Detained
};

/**
 * Used to designate the weapon posture
 */
enum Interop_WeaponPostureType
{
    WeaponPosture_None = 0,
    WeaponPosture_Stowed,
    WeaponPosture_Deployed,
    WeaponPosture_Raised
};

enum Interop_ComplianceType
{
    Compliance_None = 0,
    Compliance_Detained,
    Compliance_Surrender,
    Compliance_UsingFists,
    Compliance_VerbalAbuse1,
    Compliance_VerbalAbuse2,
    Compliance_VerbalAbuse3,
    Compliance_PassiveResistance1,
    Compliance_PassiveResistance2,
    Compliance_PassiveResistance3,
    Compliance_NonLethalWeapon1,
    Compliance_NonLethalWeapon2,
    Compliance_NonLethalWeapon3,
    Compliance_NonLethalWeapon4,
    Compliance_NonLethalWeapon5,
    Compliance_NonLethalWeapon6
};

enum Interop_HatchStateType
{
    HatchState_None = 0,
    HatchState_Closed,
    HatchState_Popped,
    HatchState_PoppedPersonVisible,
    HatchState_Open,
    HatchState_OpenPersonVisible
};

enum Interop_CamouflageType
{
    CamouflageType_None = 0,
    CamouflageType_Desert,
    CamouflageType_Winter,
    CamouflageType_Woodland,
    CamouflageType_Generic
};

enum Interop_TrailingEffectsType
{
    TrailingEffects_None = 0,
    TrailingEffects_Small,
    TrailingEffects_Medium,
    TrailingEffects_Large
};

/**
 * Used to say what encoding the marking is
 * on a game entity.
 */
enum Interop_MarkingEncodingType
{
    MarkingEncoding_Other = 0,
    MarkingEncoding_ASCII = 1,
    MarkingEncoding_ArmyMarkingCCTT = 2,
    MarkingEncoding_DigitChevron = 3
};

/**
 * An enumeration of detonation types
 */
enum Interop_DetonationType
{
    Detonation_Other = 0,	
    Detonation_Entity_Impact = 1,	
    Detonation_Entity_Proximate_Detonation = 2,	
    Detonation_Ground_Impact = 3,	
    Detonation_Ground_Proximate_Detonation = 4,	
    Detonation_Detonation = 5,	
    Detonation_None_or_No_Detonation = 6,
    Detonation_HE_hit_small = 7,	
    Detonation_HE_hit_medium = 8,	
    Detonation_HE_hit_large = 9,	
    Detonation_Armor_piercing_hit = 10,
    Detonation_Dirt_blast_small = 11,
    Detonation_Dirt_blast_medium = 12,
    Detonation_Dirt_blast_large = 13,
    Detonation_Water_blast_small = 14,
    Detonation_Water_blast_medium = 15,
    Detonation_Water_blast_large = 16,
    Detonation_Air_hit = 17,	
    Detonation_Building_hit_small = 18,	
    Detonation_Building_hit_medium = 19,	
    Detonation_Building_hit_large = 20,
    Detonation_Mine_clearing_line_charge = 21,	
    Detonation_Environment_object_impact = 22,	
    Detonation_Environment_object_proximate_detonation = 23,	
    Detonation_Water_Impact = 24,	
    Detonation_Air_Burst = 25,
    Detonation_Kill_with_fragment_type1 = 26,	
    Detonation_Kill_with_fragment_type2 = 27,
    Detonation_Kill_with_fragment_type3 = 28,
    Detonation_Kill_with_fragment_type1_after_fly_out_failure = 29,
    Detonation_Kill_with_fragment_type2_after_fly_out_failure = 30,
    Detonation_Miss_due_to_fly_out_failure = 31,	
    Detonation_Miss_due_to_end_game_failure = 32,	
    Detonation_Miss_due_to_fly_out_and_end_game_failure = 33,
};
   

/**
 * Used for the warhead type (not yet implemented)
 */
enum Interop_WarHeadType
{
    WareHeadType_Other = 0,
    WareHeadType_CargoVariableSubmunitions = 10,
    WareHeadType_FuelAirExplosive = 20,
    WareHeadType_GlassBeads = 30,
    WareHeadType_Warhead_1um = 31,
    WareHeadType_Warhead_5um = 32,
    WareHeadType_Warhead_10um = 33,
    WareHeadType_HighExplosive = 1000,
    WareHeadType_HE_Plastic = 1100,
    WareHeadType_HE_Incendiary = 1200,
    WareHeadType_HE_Fragmentation = 1300,
    WareHeadType_HE_Antitank = 1400,
    WareHeadType_HE_Bomblets = 1500,
    WareHeadType_HE_ShapedCharge = 1600,
    WareHeadType_HE_ContinuousRod = 1610,
    WareHeadType_HE_TungstenBall = 1615,
    WareHeadType_HE_BlastFragmentation = 1620,
    WareHeadType_HE_SteerableDartswithHE = 1625,
    WareHeadType_HE_Darts = 1630,
    WareHeadType_HE_Flechettes = 1635,
    WareHeadType_HE_DirectedFragmentation = 1640,
    WareHeadType_HE_SemiArmorPiercing = 1645,
    WareHeadType_HE_ShapedChargeFragmentation = 1650,
    WareHeadType_HE_SemiArmorPiercingFragmentation = 1655,
    WareHeadType_HE_HollowCharge = 1660,
    WareHeadType_HE_DoubleHollowCharge = 1665,
    WareHeadType_HE_GeneralPurpose = 1670,
    WareHeadType_HE_BlastPenetrator = 1675,
    WareHeadType_HE_RodPenetrator = 1680,
    WareHeadType_HE_Antipersonnel = 1685,
    WareHeadType_Smoke = 2000,
    WareHeadType_Illumination = 3000,
    WareHeadType_Practice = 4000,
    WareHeadType_Kinetic = 5000,
    WareHeadType_Mines = 6000,
    WareHeadType_Nuclear = 7000,
    WareHeadType_NuclearIMT = 7010,
    WareHeadType_ChemicalGeneral = 8000,
    WareHeadType_ChemicalBlisterAgent = 8100,
    WareHeadType_HD_Mustard = 8110,
    WareHeadType_ThickenedHD_Mustard = 8115,
    WareHeadType_DustyHD_Mustard = 8120,
    WareHeadType_ChemicalBloodAgent = 8200,
    WareHeadType_AC_HCN = 8210,
    WareHeadType_CK_CNCI = 8215,
    WareHeadType_CG_Phosgene = 8220,
    WareHeadType_ChemicalNerveAgent = 8300,
    WareHeadType_VX = 8310,
    WareHeadType_ThickenedVX = 8315,
    WareHeadType_DustyVX = 8320,
    WareHeadType_GA_Tabun = 8325,
    WareHeadType_ThickenedGA_Tabun = 8330,
    WareHeadType_DustyGA_Tabun = 8335,
    WareHeadType_GB_Sarin = 8340,
    WareHeadType_ThickenedGB_Sarin = 8345,
    WareHeadType_DustyGB_Sarin = 8350,
    WareHeadType_GD_Soman = 8355,
    WareHeadType_ThickenedGD_Soman = 8360,
    WareHeadType_DustyGD_Soman = 8365,
    WareHeadType_GF = 8370,
    WareHeadType_ThickenedGF = 8375,
    WareHeadType_DustyGF = 8380,
    WareHeadType_Biological = 9000,
    WareHeadType_BiologicalVirus = 9100,
    WareHeadType_BiologicalBacteria = 9200,
    WareHeadType_BiologicalRickettsia = 9300,
    WareHeadType_BiologicalGeneticallyModifiedMicroOrganisms = 9400,
    WareHeadType_BiologicalToxin = 9500
};


/**
 * Used for the fuse (not yet implemented)
 */
enum Interop_FuseType
{
    FuseType_Other = 0,
    FuseType_IntelligentInfluence = 10,
    FuseType_Sensor = 20,
    FuseType_SelfDestruct_Interop_= 30,
    FuseType_UltraQuick = 40,
    FuseType_Body = 50,
    FuseType_DeepIntrusion = 60,
    FuseType_Multifunction = 100,
    FuseType_PointDetonation_PD = 200,
    FuseType_BaseDetonation_BD = 300,
    FuseType_Contact = 1000,
    FuseType_ContactInstantImpact = 1100,
    FuseType_ContactDelayed = 1200,
    FuseType_Contact10msDelay = 1201,
    FuseType_Contact20msDelay = 1202,
    FuseType_Contact50msDelay = 1205,
    FuseType_Contact60msDelay = 1206,
    FuseType_Contact100msDelay = 1210,
    FuseType_Contact125msDelay = 1212,
    FuseType_Contact250msDelay = 1225,
    FuseType_ContactElectronicObliqueContact = 1300,
    FuseType_ContactGraze = 1400,
    FuseType_ContactCrush = 1500,
    FuseType_ContactHydrostatic = 1600,
    FuseType_ContactMechanical = 1700,
    FuseType_ContactChemical = 1800,
    FuseType_ContactPiezoelectric = 1900,
    FuseType_ContactPointInitiating = 1910,
    FuseType_ContactPointInitiatingBaseDetonating = 1920,
    FuseType_ContactBaseDetonating = 1930,
    FuseType_ContactBallisticCapAndBase = 1940,
    FuseType_ContactBase = 1950,
    FuseType_ContactNose = 1960,
    FuseType_ContactFittedInStandoffProbe = 1970,
    FuseType_ContactNonAligned = 1980,
    FuseType_Timed = 2000,
    FuseType_TimedProgrammable = 2100,
    FuseType_TimedBurnout = 2200,
    FuseType_TimedPyrotechnic = 2300,
    FuseType_TimedElectronic = 2400,
    FuseType_TimedBaseDelay = 2500,
    FuseType_TimedReinforcedNoseImpactDelay = 2600,
    FuseType_TimedShortDelayImpact = 2700,
    FuseType_Timed10msDelay = 2701,
    FuseType_Timed20msDelay = 2702,
    FuseType_Timed50msDelay = 2705,
    FuseType_Timed60msDelay = 2706,
    FuseType_Timed100msDelay = 2710,
    FuseType_Timed125msDelay = 2712,
    FuseType_Timed250msDelay = 2725,
    FuseType_TimedNoseMountedVariableDelay = 2800,
    FuseType_TimedLongDelaySide = 2900,
    FuseType_TimedSelectableDelay = 2910,
    FuseType_TimedImpact = 2920,
    FuseType_TimedSequence = 2930,
    FuseType_Proximity = 3000,
    FuseType_ProximityActiveLaser = 3100,
    FuseType_ProximityMagneticMagpolarity = 3200,
    FuseType_ProximityActiveDopplerRadar = 3300,
    FuseType_ProximityRadioFrequencyRF = 3400,
    FuseType_ProximityProgrammable = 3500,
    FuseType_ProximityProgrammablePrefragmented = 3600,
    FuseType_ProximityInfrared = 3700,
    FuseType_Command = 4000,
    FuseType_CommandElectronicRemotelySet = 4100,
    FuseType_Altitude = 5000,
    FuseType_AltitudeRadioAltimeter = 5100,
    FuseType_AltitudeAirBurst = 5200,
    FuseType_Depth = 6000,
    FuseType_Acoustic = 7000,
    FuseType_Pressure = 8000,
    FuseType_PressureDelay = 8010,
    FuseType_Inert = 8100,
    FuseType_Dummy = 8110,
    FuseType_Practice = 8120,
    FuseType_PlugRepresenting = 8130,
    FuseType_Training = 8150,
    FuseType_Pyrotechnic = 9000,
    FuseType_PyrotechnicDelay = 9010,
    FuseType_ElectroOptical = 9100,
    FuseType_ElectroMechanical = 9110,
    FuseType_ElectroMechanicalNose = 9120,
    FuseType_Strikerless = 9200,
    FuseType_StrikerlessNoseImpact = 9210,
    FuseType_StrikerlessCompressionIgnition = 9220,
    FuseType_CompressionIgnition = 9300,
    FuseType_CompressionIgnitionStrikerlessNoseImpact = 9310,
    FuseType_Percussion = 9400,
    FuseType_PercussionInstantaneous = 9410,
    FuseType_Electronic = 9500,
    FuseType_ElectronicInternallyMounted = 9510,
    FuseType_ElectronicRangeSetting = 9520,
    FuseType_ElectronicProgrammed = 9530,
    FuseType_Mechanical = 9600,
    FuseType_MechanicalNose = 9610,
    FuseType_MechanicalTail = 9620
};

/**
 * Used for articulated parts
 */
enum Interop_ParameterTypeEnum
{
    ParameterType_ArticulatedPart = 0,
    ParameterType_AttachedPart = 1
};

/**
 * Used for articulated parts
 */
enum Interop_StationType 
{
    StationType_Nothing_Empty = 0,
    StationType_Fuselage_Station1 = 512,
    StationType_Fuselage_Station2 = 513,
    StationType_Fuselage_Station3 = 514,
    StationType_Fuselage_Station4 = 515,
    StationType_Fuselage_Station5 = 516,
    StationType_Fuselage_Station6 = 517,
    StationType_Fuselage_Station7 = 518,
    StationType_Fuselage_Station8 = 519,
    StationType_Fuselage_Station9 = 520,
    StationType_LeftWingStation1 = 640,
    StationType_LeftWingStation2 = 641,
    StationType_LeftWingStation3 = 642,
    StationType_LeftWingStation4 = 643,
    StationType_LeftWingStation5 = 644,
    StationType_LeftWingStation6 = 645,
    StationType_LeftWingStation7 = 646,
    StationType_LeftWingStation8 = 647,
    StationType_LeftWingStation9 = 648,
    StationType_RightWingStation1 = 768,
    StationType_RightWingStation2 = 769,
    StationType_RightWingStation3 = 770,
    StationType_RightWingStation4 = 771,
    StationType_RightWingStation5 = 772,
    StationType_RightWingStation6 = 773,
    StationType_RightWingStation7 = 774,
    StationType_RightWingStation8 = 775,
    StationType_RightWingStation9 = 776,
    StationType_M16A42_rifle = 896,
    StationType_M249_SAW = 897,
    StationType_M60_Machine_gun = 898,
    StationType_M203_Grenade_Launcher = 899,
    StationType_M136_AT4 = 900,
    StationType_M47_Dragon = 901,
    StationType_AAWS_M_Javelin = 902,
    StationType_M18A1_Claymore_Mine = 903,
    StationType_MK19_Grenade_Launcher = 904,
    StationType_M2_Machine_Gun = 905,
    StationType_Other_attached_parts = 906
};


/**
 * Used for articulated parts
 */
enum Interop_ArticulatedPartsType 
{
    ArticulatedPartsType_Other = 0,
    ArticulatedPartsType_Rudder = 1024,
    ArticulatedPartsType_LeftFlap = 1056,
    ArticulatedPartsType_RightFlap = 1088,
    ArticulatedPartsType_LeftAileron = 1120,
    ArticulatedPartsType_RightAileron = 1152,
    ArticulatedPartsType_HelicopterMainRotor = 1184,
    ArticulatedPartsType_HelicopterTailRotor = 1216,
    ArticulatedPartsType_OtherAircraftControlSurfaces = 1248,
    ArticulatedPartsType_Periscope = 2048,
    ArticulatedPartsType_GenericAntenna = 2080,
    ArticulatedPartsType_Snorkel = 2112,
    ArticulatedPartsType_OtherExtendableParts = 2144,
    ArticulatedPartsType_LandingGear = 3072,
    ArticulatedPartsType_TailHook = 3104,
    ArticulatedPartsType_SpeedBrake = 3136,
    ArticulatedPartsType_LeftWeaponBayDoors = 3168,
    ArticulatedPartsType_RightWeaponBayDoors = 3200,
    ArticulatedPartsType_TankOrAPChatch = 3232,
    ArticulatedPartsType_Wingsweep = 3264,
    ArticulatedPartsType_BridgeLauncher = 3296,
    ArticulatedPartsType_BridgeSection1 = 3328,
    ArticulatedPartsType_BridgeSection2 = 3360,
    ArticulatedPartsType_BridgeSection3 = 3392,
    ArticulatedPartsType_PrimaryBlade1 = 3424,
    ArticulatedPartsType_PrimaryBlade2 = 3456,
    ArticulatedPartsType_PrimaryBoom = 3488,
    ArticulatedPartsType_PrimaryLauncherArm = 3520,
    ArticulatedPartsType_OtherFixedPositionParts = 3552,
    ArticulatedPartsType_PrimaryTurretNumber1 = 4096,
    ArticulatedPartsType_PrimaryTurretNumber2 = 4128,
    ArticulatedPartsType_PrimaryTurretNumber3 = 4160,
    ArticulatedPartsType_PrimaryTurretNumber4 = 4192,
    ArticulatedPartsType_PrimaryTurretNumber5 = 4224,
    ArticulatedPartsType_PrimaryTurretNumber6 = 4256,
    ArticulatedPartsType_PrimaryTurretNumber7 = 4288,
    ArticulatedPartsType_PrimaryTurretNumber8 = 4320,
    ArticulatedPartsType_PrimaryTurretNumber9 = 4352,
    ArticulatedPartsType_PrimaryTurretNumber10 = 4384,
    ArticulatedPartsType_PrimaryGunNumber1 = 4416,
    ArticulatedPartsType_PrimaryGunNumber2 = 4448,
    ArticulatedPartsType_PrimaryGunNumber3 = 4480,
    ArticulatedPartsType_PrimaryGunNumber4 = 4512,
    ArticulatedPartsType_PrimaryGunNumber5 = 4544,
    ArticulatedPartsType_PrimaryGunNumber6 = 4576,
    ArticulatedPartsType_PrimaryGunNumber7 = 4608,
    ArticulatedPartsType_PrimaryGunNumber8 = 4640,
    ArticulatedPartsType_PrimaryGunNumber9 = 4672,
    ArticulatedPartsType_PrimaryGunNumber10 = 4704,
    ArticulatedPartsType_PrimaryLauncher1 = 4736,
    ArticulatedPartsType_PrimaryLauncher2 = 4768,
    ArticulatedPartsType_PrimaryLauncher3 = 4800,
    ArticulatedPartsType_PrimaryLauncher4 = 4832,
    ArticulatedPartsType_PrimaryLauncher5 = 4864,
    ArticulatedPartsType_PrimaryLauncher6 = 4896,
    ArticulatedPartsType_PrimaryLauncher7 = 4928,
    ArticulatedPartsType_PrimaryLauncher8 = 4960,
    ArticulatedPartsType_PrimaryLauncher9 = 4992,
    ArticulatedPartsType_PrimaryLauncher10 = 5024,
    ArticulatedPartsType_PrimaryDefenseSystems1 = 5056,
    ArticulatedPartsType_PrimaryDefenseSystems2 = 5088,
    ArticulatedPartsType_PrimaryDefenseSystems3 = 5120,
    ArticulatedPartsType_PrimaryDefenseSystems4 = 5152,
    ArticulatedPartsType_PrimaryDefenseSystems5 = 5184,
    ArticulatedPartsType_PrimaryDefenseSystems6 = 5216,
    ArticulatedPartsType_PrimaryDefenseSystems7 = 5248,
    ArticulatedPartsType_PrimaryDefenseSystems8 = 5280,
    ArticulatedPartsType_PrimaryDefenseSystems9 = 5312,
    ArticulatedPartsType_PrimaryDefenseSystems10 = 5344,
    ArticulatedPartsType_PrimaryRadar1 = 5376,
    ArticulatedPartsType_PrimaryRadar2 = 5408,
    ArticulatedPartsType_PrimaryRadar3 = 5440,
    ArticulatedPartsType_PrimaryRadar4 = 5472,
    ArticulatedPartsType_PrimaryRadar5 = 5504,
    ArticulatedPartsType_PrimaryRadar6 = 5536,
    ArticulatedPartsType_PrimaryRadar7 = 5568,
    ArticulatedPartsType_PrimaryRadar8 = 5600,
    ArticulatedPartsType_PrimaryRadar9 = 5632,
    ArticulatedPartsType_PrimaryRadar10 = 5664,
    ArticulatedPartsType_SecondaryTurretNumber1 = 5696,
    ArticulatedPartsType_SecondaryTurretNumber2 = 5728,
    ArticulatedPartsType_SecondaryTurretNumber3 = 5760,
    ArticulatedPartsType_SecondaryTurretNumber4 = 5792,
    ArticulatedPartsType_SecondaryTurretNumber5 = 5824,
    ArticulatedPartsType_SecondaryTurretNumber6 = 5856,
    ArticulatedPartsType_SecondaryTurretNumber7 = 5888,
    ArticulatedPartsType_SecondaryTurretNumber8 = 5920,
    ArticulatedPartsType_SecondaryTurretNumber9 = 5952,
    ArticulatedPartsType_SecondaryTurretNumber10 = 5984,
    ArticulatedPartsType_SecondaryGunNumber1 = 6016,
    ArticulatedPartsType_SecondaryGunNumber2 = 6048,
    ArticulatedPartsType_SecondaryGunNumber3 = 6080,
    ArticulatedPartsType_SecondaryGunNumber4 = 6112,
    ArticulatedPartsType_SecondaryGunNumber5 = 6144,
    ArticulatedPartsType_SecondaryGunNumber6 = 6176,
    ArticulatedPartsType_SecondaryGunNumber7 = 6208,
    ArticulatedPartsType_SecondaryGunNumber8 = 6240,
    ArticulatedPartsType_SecondaryGunNumber9 = 6272,
    ArticulatedPartsType_SecondaryGunNumber10 = 6304,
    ArticulatedPartsType_SecondaryLauncher1 = 6336,
    ArticulatedPartsType_SecondaryLauncher2 = 6368,
    ArticulatedPartsType_SecondaryLauncher3 = 6400,
    ArticulatedPartsType_SecondaryLauncher4 = 6432,
    ArticulatedPartsType_SecondaryLauncher5 = 6464,
    ArticulatedPartsType_SecondaryLauncher6 = 6496,
    ArticulatedPartsType_SecondaryLauncher7 = 6528,
    ArticulatedPartsType_SecondaryLauncher8 = 6560,
    ArticulatedPartsType_SecondaryLauncher9 = 6592,
    ArticulatedPartsType_SecondaryLauncher10 = 6624,
    ArticulatedPartsType_SecondaryDefenseSystems1 = 6656,
    ArticulatedPartsType_SecondaryDefenseSystems2 = 6688,
    ArticulatedPartsType_SecondaryDefenseSystems3 = 6720,
    ArticulatedPartsType_SecondaryDefenseSystems4 = 6752,
    ArticulatedPartsType_SecondaryDefenseSystems5 = 6784,
    ArticulatedPartsType_SecondaryDefenseSystems6 = 6816,
    ArticulatedPartsType_SecondaryDefenseSystems7 = 6848,
    ArticulatedPartsType_SecondaryDefenseSystems8 = 6880,
    ArticulatedPartsType_SecondaryDefenseSystems9 = 6912,
    ArticulatedPartsType_SecondaryDefenseSystems10 = 6944,
    ArticulatedPartsType_SecondaryRadar1 = 6976,
    ArticulatedPartsType_SecondaryRadar2 = 7008,
    ArticulatedPartsType_SecondaryRadar3 = 7040,
    ArticulatedPartsType_SecondaryRadar4 = 7072,
    ArticulatedPartsType_SecondaryRadar5 = 7104,
    ArticulatedPartsType_SecondaryRadar6 = 7136,
    ArticulatedPartsType_SecondaryRadar7 = 7168,
    ArticulatedPartsType_SecondaryRadar8 = 7200,
    ArticulatedPartsType_SecondaryRadar9 = 7232,
    ArticulatedPartsType_SecondaryRadar10 = 7264,
    // The following are RPR2 elements
    ArticulatedPartsType_DeckElevator1 = 7296,
    ArticulatedPartsType_DeckElevator2 = 7328,
    ArticulatedPartsType_Catapult1 = 7360,
    ArticulatedPartsType_Catapult2 = 7392,
    ArticulatedPartsType_JetBlastDeflector1 = 7424,
    ArticulatedPartsType_JetBlastDeflector2 = 7456,
    ArticulatedPartsType_ArrestorWires1 = 7488,
    ArticulatedPartsType_ArrestorWires2 = 7520,
    ArticulatedPartsType_ArrestorWires3 = 7552,
    ArticulatedPartsType_WingOrRotorFold = 7584,
    ArticulatedPartsType_FuselageFold = 7616,
};

/**
 * Used for articulated parts
 */
enum Interop_ArticulatedTypeMetricType
{
    ArticulatedTypeMetric_Position = 1,
    ArticulatedTypeMetric_PositionRate = 2,
    ArticulatedTypeMetric_Extension = 3,
    ArticulatedTypeMetric_ExtensionRate = 4,
    ArticulatedTypeMetric_X = 5,
    ArticulatedTypeMetric_XRate = 6,
    ArticulatedTypeMetric_Y = 7,
    ArticulatedTypeMetric_YRate = 8,
    ArticulatedTypeMetric_Z = 9,
    ArticulatedTypeMetric_ZRate = 10,
    ArticulatedTypeMetric_Azimuth = 11,
    ArticulatedTypeMetric_AzimuthRate = 12,
    ArticulatedTypeMetric_Elevation = 13,
    ArticulatedTypeMetric_ElevationRate = 14,
    ArticulatedTypeMetric_Rotation = 15,
    ArticulatedTypeMetric_RotationRate = 16
};

enum Interop_UnitForceType
{
	UnitForce_Other = 0,
	UnitForce_Friendly = 1,
	UnitForce_Opposing = 2,
	UnitForce_Neutral = 3, 
	UnitForce_Friendly_2 = 4,
	UnitForce_Opposing_2 = 5,
	UnitForce_Neutral_2 = 6,
	UnitForce_Friendly_3 = 7,
	UnitForce_Opposing_3 = 8,
	UnitForce_Neutral_3 = 9,
	UnitForce_Friendly_4 = 10,
	UnitForce_Opposing_4 = 11,
	UnitForce_Neutral_4 = 12,
	UnitForce_Friendly_5 = 13,
	UnitForce_Opposing_5 = 14,
	UnitForce_Neutral_5 = 15,
	UnitForce_Friendly_6 = 16,
	UnitForce_Opposing_6 = 17,
	UnitForce_Neutral_6 = 18,
	UnitForce_Friendly_7 = 19,
	UnitForce_Opposing_7 = 20,
	UnitForce_Neutral_7 = 21,
	UnitForce_Friendly_8 = 22,
	UnitForce_Opposing_8 = 23,
	UnitForce_Neutral_8 = 24,
	UnitForce_Friendly_9 = 25,
	UnitForce_Opposing_9 = 26,
	UnitForce_Neutral_9 = 27,
	UnitForce_Friendly_10 = 28,
	UnitForce_Opposing_10 = 29,
	UnitForce_Neutral_10 = 30
};

/**
 * Converts type to a human readable string
 */
inline const char* toString(const Interop_UnitForceType& type)
{
    if(type == 1)
    {
        return "Friendly";
    }
    else if(type == 2)
    {
        return "Opposing";
    }
    else if(type == 3)
    {
        return "Neutral";
    }
    else if(type == 4)
    {
        return "Friendly_2";
    }
    else if(type == 5)
    {
        return "Opposing_2";
    }
    else if(type == 6)
    {
        return "Neutral_2";
    }
    else if(type == 7)
    {
        return "Friendly_3";
    }
    else if(type == 8)
    {
        return "Opposing_3";
    }
    else if(type == 9)
    {
        return "Neutral_3";
    }
    else if(type == 10)
    {
        return "Friendly_4";
    }
    else if(type == 11)
    {
        return "Opposing_4";
    }
    else if(type == 12)
    {
        return "Neutral_4";
    }
    else if(type == 13)
    {
        return "Friendly_5";
    }
    else if(type == 14)
    {
        return "Opposing_5";
    }
    else if(type == 15)
    {
        return "Neutral_5";
    }
    else if(type == 16)
    {
        return "Friendly_6";
    }
    else if(type == 17)
    {
        return "Opposing_6";
    }
    else if(type == 18)
    {
        return "Neutral_6";
    }
    else if(type == 19)
    {
        return "Friendly_7";
    }
    else if(type == 20)
    {
        return "Opposing_7";
    }
    else if(type == 21)
    {
        return "Neutral_7";
    }
    else if(type == 22)
    {
        return "Friendly_8";
    }
    else if(type == 23)
    {
        return "Opposing_8";
    }
    else if(type == 24)
    {
        return "Neutral_8";
    }
    else if(type == 25)
    {
        return "Friendly_9";
    }
    else if(type == 26)
    {
        return "Opposing_9";
    }
    else if(type == 27)
    {
        return "Neutral_9";
    }
    else if(type == 28)
    {
        return "Friendly_10";
    }
    else if(type == 29)
    {
        return "Opposing_10";
    }
    else if(type == 30)
    {
        return "Neutral_10";
    }
	else
	{
        return "Other";
	}
}

/**
 * Used to determine the domain of entities.
 */
enum Interop_DomainType
{
	Domain_Other = 0,
	Domain_Land = 1,
	Domain_Air = 2,
	Domain_Surface = 3,
	Domain_Subsurface = 4,
	Domain_Space = 5
};

/**
 * Converts type to a human readable string
 */
inline const char* toString(const Interop_DomainType& type)
{
	if(type == 1)
	{
        return "Land";
	}
	else if(type == 2)
	{
        return "Air";
	}
	else if(type == 3)
	{
        return "Surface";
	}
	else if(type == 4)
	{
        return "Subsurface";
	}
	else if(type == 5)
	{
        return "Space";
	}
	else
	{
        return "Other";
	}
}

/**
 * Used as part of the transmitter (radio)
 */
enum Interop_TransmitStateType
{
    TransmitState_Off = 0,
    TransmitState_OnNotTransmitting = 1,
    TransmitState_OnAndTransmitting = 2

};

typedef Interop_TransmitStateType TransmitStateType;

/**
 * Used as part of the transmitter (radio)
 */
enum Interop_InputSourceType
{
    Input_Other = 0,
    Input_Pilot = 1,
    Input_Copilot = 2,
    Input_FirstOfficer = 3,
    Input_Driver = 4,
    Input_Loader = 5,
    Input_Gunner = 6,
    Input_Commander = 7,
    Input_DigitalDataDevice = 8,
    Input_Intercom = 9
};

/**
 * Used as part of the transmitter (radio)
 */
enum Interop_AntennaPatternType
{
    Pattern_OmniDirectional = 0,
    Pattern_Beam = 1,
    Pattern_SphericalHarmonic = 2
};

/**
 * Used as part of the transmitter (radio)
 */
enum Interop_MajorModulationType
{
    MajorModulation_Other = 0,
    MajorModulation_Amplitude = 1,
    MajorModulation_AmplitudeAndAngle = 2,
    MajorModulation_Angle = 3,
    MajorModulation_Combination = 4,
    MajorModulation_Pulse = 5,
    MajorModulation_Unmodulated = 6
};

/**
 * Used as part of the transmitter (radio)
 */
enum Interop_ModulationSystemType
{
    ModulationSystem_Other = 0,
    ModulationSystem_Generic = 1,
    ModulationSystem_HQ = 2,
    ModulationSystem_HQII = 3,
    ModulationSystem_HQIIA = 4,
    ModulationSystem_SINCGARS = 5,
    ModulationSystem_CCTT_SINCGARS = 6
};

/**
 * Used as part of the transmitter (radio)
 */
enum Interop_CryptoSystemType
{
    CryptoSystem_Other = 0,
    CryptoSystem_KY28 = 1,
    CryptoSystem_KY58 = 2,
    CryptoSystem_NSVE = 3,
    CryptoSystem_WSVE = 4,
    CryptoSystem_SINCGARS_ICOM = 5
};


enum Interop_StartMessageType
{
    StartMessage_NotStartOfMessage = 0,
    StartMessage_StartOfMessage = 1
};


/**
 * Used as part of Signal (radio)
 */
enum Interop_EncodingClassType
{
    EncodingClass_EncodedVoice = 0,
    EncodingClass_RawBinaryData = 1,
    EncodingClass_AppSpecificData = 2,
    EncodingClass_DatabaseIndex = 3
};

/**
 * Used as part of Signal (radio)
 */
enum Interop_EncodingType
{
    EncodingType_8BitMuLaw = 1,
    EncodingType_CVSD = 2,
    EncodingType_ADPCM = 3,
    EncodingType_16bitPCM = 4,
    EncodingType_8bitPCM = 5,
    EncodingType_VQ = 6
};

/**
 * Used as part of Signal (radio)
 */
enum Interop_TDL_Type
{
    TDL_Other = 0,
    TLD_AbbreviatedCommandAndContrl = 15
};

/**
 * Used as part of the transmitter (radio)
 */
enum Interop_SpreadSpectrumType
{
    SpreadSpectrumType_None = 0,
    SpreadSpectrumType_SINCGARSFrequencyHop = 1
};

/**
 * Used as part of the Designator
 */
enum Interop_DesignatorNameType
{
	DesignatorName_Other = 0,
	DesignatorName_TBD = 1
};

/**
 * Used as part of the Designator
 */
enum Interop_DesignatorType
{
	DesignatorType_Other = 0,
	DesignatorType_TBD = 1
};

// ===================================
// Unit Side 
// ===================================
#define UNIT_SIDE_TYPE(XX)\
	XX(OpFor) \
	XX(BlueFor) \
	XX(Resistance) \
	XX(NonCombatant)
#define US_DEFINE_ENUM(name) US_##name,
enum Interop_UnitSide{
	UNIT_SIDE_TYPE(US_DEFINE_ENUM)
	US_NONE	
};
#define US_ENUM_Name(name) #name,
static const char *Interop_UnitSideNames[] = {
	UNIT_SIDE_TYPE(US_ENUM_Name)
};

enum Interop_SimStateData
{
	/*!  no SIM*/
	SNone,
	/*!  VBS is creating (message formats are not registered yet)*/
	SCreating,
	/*!  VBS is created, messages can be sent*/
	SCreate,
	/*!  unit is logged into sim (his identity is created)*/
	SLogin,
	/*!  server is editing mission*/
	SEdit,
	/*!  unit voted mission*/
	SMissionVoted,
	/*!  unit are assigned to sides*/
	SPrepareSide,
	/*!  unit are assigned to roles*/
	SPrepareRole,
	/*!  unit is already assigned*/
	SPrepareOK,
	/*!  mission debriefing*/
	SDebriefing,
	/*!  mission debriefing is already read*/
	SDebriefingOK,
	/*!  transfering mission file*/
	STransferMission,
	/*!  loading island*/
	SLoadIsland,
	/*!  briefing screen*/
	SBriefing,
	/*!  Sim is running*/
	SSimulation
};


enum Interop_EPosInfo
{
    Pos_None = 0,
    Pos_Driver,
    Pos_Turret,
    Pos_Cargo
};


struct Interop_EntityId
{
    int ApplicationNumber;
    int EntityNumber;
};

static const Interop_EntityId NullEntityId = {0,0};

struct Interop_Generic3DType
{
    double x;
    double y;
    double z;
};

typedef Interop_Generic3DType Interop_Coordinate3DType;
typedef Interop_Generic3DType Interop_Vector3DType;
typedef Interop_Generic3DType Interop_BoundingBoxType;

struct Interop_EntityCreateData
{
    Interop_EntityId Id;
    Interop_Coordinate3DType Pos;
    char EntityType[255];        /*! Entity type*/
    Interop_EntityId VehicleId;          /*! Id of vehicle, that this entity is in*/
    Interop_EPosInfo EPos;               /*! Entity position in vehicle*/
    Interop_BoundingBoxType Dimensions;  /*! Physical dimensions of the entity*/
    Interop_UnitSide Side;               /*! Entity side*/
    char Name[255];              /*! Entity name*/
    char Profile[255];           /*! Profile name*/
    int CrewIndex;               /*! Index inside the vehicle*/
};

struct Interop_CenterCreateData
{
  Interop_EntityId Id;
  Interop_UnitSide Side;               
};

struct Interop_GroupCreateData
{
  Interop_EntityId Id;
  Interop_EntityId CenterId;
  int Number;
  Interop_EntityId LeaderId;          /*!Unit Id*/
};

struct Interop_UnitCreateData
{
  Interop_EntityId Id;
  Interop_EntityId PersonId;           /*! Person representing the unit*/
  Interop_EntityId GroupId;            /*! Person representing the unit*/
  int Number;
};

struct Interop_EntityUpdateData
{
    Interop_EntityId Id;	/*! return corresponded Interop_EntityId from createEntity callback, should be initialize here*/
    Interop_Coordinate3DType Pos;
    Interop_Vector3DType Dir;
    Interop_Vector3DType Up;
    Interop_Vector3DType BodyVelocity;          /*! velocity m/s in body reference frame */
    Interop_Vector3DType Velocity;              /*! velocity m/s */
    Interop_Vector3DType BodyAcceleration;      /*! acceleration m/s^2 in body reference frame */
    Interop_Vector3DType Acceleration;          /*! linear acceleration m/s^2 */
	Interop_Vector3DType BodyAngularVelocity;    /*! Angular Velocity in body reference frame (radians/sec) */
    float Damage;                       /*! % of entity that is damaged */
    unsigned int Appearance;            /*! appearance bit fields */
    unsigned int Lights;                /*! light state bit fields */
    bool HasTurret;                     // Entity is articulated */
    float Azimuth;	    	            /*! Relative Azimuth for turret radians (VBS Y rot) */
    float Elevation;    	            /*! Relative Elevation for turret radians (VBS X rot) */
    float AzimuthWanted;	    	    /*! Wanted Relative rotation from Azimuth */
    float ElevationWanted;    	        /*! Wanted Relative Elevation from Elevation */
    Interop_EntityId VehicleId;                 /*! Id of vehicle, that this entity is in */
    Interop_EPosInfo EPos;                      /*! Entity position in vehicle */
    Interop_StanceType Stance;                  /*! Stance for lifeforms as per enums */
    Interop_WeaponPostureType PrWeaponPosture;  /*! Posture for primary weapon as per enums */
    Interop_WeaponPostureType SndWeaponPosture; /*! Posture for primary weapon as per enums */
    int CrewIndex;                      /*! Index inside the vehicle */
    int SelectedWeapon;                 /*! Currently selected weapon */
    char URN[12];                       /*! Unit Reference number */
    float StationaryTimeAccumulator;    /*! _VBS3_DRAW_STATIONARY Time in seconds the unit is stationary */
};

typedef Interop_EntityId Interop_EntityDeleteData;
typedef Interop_EntityId Interop_DesignatorDeleteData;
typedef Interop_EntityId Interop_GroupDeleteData;
typedef Interop_EntityId Interop_TransmitterDeleteData;

struct Interop_DesignatorCreateData
{
    Interop_EntityId Id;                        /*! Designator ID */
    Interop_EntityId DesignatingId;             /*! ID of the entity controlling the desig */
    Interop_EntityId DesignatedId;              /*! ID of the designated entity (or 0,0) */
    int DesignatorNameCode;             
    int DesignatorFunctionCode;         
    float DesignatorPower;              /*! power in watts */
    float DesignatorWavelength;         /*! wavelength in microns */
    Interop_Coordinate3DType RelativePosition;  /*! desig pos relative to entity centre */
    Interop_Coordinate3DType WorldPosition;     /*! desig pos in the world */
    Interop_Vector3DType LinearAcceleration;    /*! linear accel in m/s^2 */
};

typedef Interop_DesignatorCreateData Interop_DesignatorUpdateData;

struct Interop_FireWeaponData
{
    Interop_EntityId FiringId;          /*! Must be known as this is direct fire! */
    Interop_EntityId TargetId;          /*! if known -> otherwise 0,0 */
    Interop_EntityId MunitionId;        /*! if known -> otherwise 0,0 */
    int WeaponId;               /*! if known -> otherwise 0 */
    Interop_Coordinate3DType Pos;       /*! Initial position */
    Interop_Vector3DType Velocity;      /*! Initial Velocity */
    char MunitionType[255];     /*! MunitionType */
    float TimeToLive;           /*! Time to live (in seconds) */
};

struct Interop_DetonationData
{
    Interop_EntityId FiringId;	        /*! if known -> otherwise 0,0 */
    Interop_EntityId TargetId;          /*! if known -> otherwise 0,0 */
    Interop_EntityId MunitionId;        /*! if known -> otherwise 0,0 */
    Interop_Vector3DType Velocity;      /*! Impact velocity */
    Interop_Coordinate3DType Pos;	    /*! Impact position */
    Interop_Coordinate3DType RelPos;    /*! Impact position in entity, relative to its center */
    char MunitionType[255];     /*! Ammunition type  */
};

struct Interop_CNRLogEvent
{
	__int64 time;           /*! Event start time in milliseconds measured in milliseconds, between the time */
	                        /*! this recording started and midnight, January 1, 1970 UTC */
	                        
	int id;                 /*! Event index number */
	__int64 freq;           /*! Radio frequency the event was recorded in Hertz */
	__int64 duration;       /*! length of event (in milliseconds) */
	char source[128];
};



struct Interop_DataScriptData
{
	Interop_EntityId OriginatingId;		/*! DIS Entity of the incoming setDataPDU */
	Interop_EntityId ReceivingId;		/*! DIS Entity of the destination setDataPDU */	
	int RequestId;				/*! Id of the Datum x in the setDataPDU */
	int DatumID;
	char DatumValue[512];		/*! Datum script */

};


struct Interop_TransmitterCreateData
{
	Interop_EntityId TransmitterId;						/*! Unique identifier for this transmitter */
    Interop_EntityId OriginatingId;                     /*! Parent Radio Entity to which radio is attached */
    unsigned RadioId;                           /*! Radio index under Parent */
    char RadioType[255];                        /*! Radio Type in the game, like vbs2_singars_radio (for mapping) */
    Interop_TransmitStateType TxState;   /*! Transmit State */
    Interop_Coordinate3DType Location;                  /*! Radio Location */
    Interop_Vector3DType Dir;                           
    Interop_Vector3DType Up;
	Interop_Coordinate3DType RelativePosition;          /*! Relative pos from location */
	unsigned Frequency;                         /*! Frequency on which we are transmitting */
};

typedef Interop_TransmitterCreateData Interop_TransmitterUpdateData;

struct Interop_SignalData
{
    /*! Transmitter from which this signal is coming from */
    Interop_EntityId TransmitterId;
    
	unsigned int length;
    char* data;
};


/*! Structure specific to JPEG transmission for FCS */
struct Interop_SignalDataFCS_JPEG
{    
    Interop_Vector3DType CameraLocation;            /*! Camera location */
    Interop_Vector3DType CameraDirection, CameraUp; /*! Camera orientation */
    Interop_Vector3DType TargetLocation;            /*! Target location */
    Interop_Vector3DType TargetDirection, TargetUp; /*! Target orientation */
    
	unsigned int JpegLength;                    /*! How long the image is */
    char* JpegData;                      /*! Array of 32-bits */
};

struct Interop_GameEntityTypeInfo
{
    /*!
    Nice Human Readable string, like 'M1A1 Desert' etc...
    */
    char name[128];

    /*!
    Internal type used by the game, cryptic, which is what's used
    everywhere for mapping and such.
    */
    char gameType[128];
    
    /*!
    Domain this entity belongs to in the game
    */
    Interop_DomainType domain;
    
    /*!
    Unit force (Blue, Neutral, Red,...)
    */
    Interop_UnitForceType force;

};

namespace VBSFusion
{
	namespace Interop
	{
		typedef unsigned int PluginId;
		static const PluginId invalidId = 0;
		struct PluginCallbacks
		{
			Interop_EntityId (*fPtrCreateEntity)(Interop_EntityCreateData* data);
			Interop_EntityId (*fPtrCreateTransmitter)(Interop_TransmitterCreateData* data);
			int  (*fPtrUpdateEntity)(Interop_EntityUpdateData* data);
			void (*fPtrDeleteEntity)(Interop_EntityDeleteData* data);
			void (*fPtrCreateDesignator)(Interop_DesignatorCreateData* data);
			void (*fPtrUpdateDesignator)(Interop_DesignatorUpdateData* data);
			void (*fPtrFireWeapon)(Interop_FireWeaponData* data);
			void (*fPtrDetonateMunition)(Interop_DetonationData* data);
			int  (*fPtrExecuteCommand)(const char* command, char* result, int resultLength);
			void (*fPtrRequestGameTypeInfoRefresh)();
			int  (*fPtrUpdateTransmitter)(Interop_TransmitterUpdateData* data);
			void (*fPtrDeleteTransmitter)(Interop_TransmitterDeleteData* data);
			void (*fPtrSendSignalUnit)(Interop_SignalData* data);
		};

		struct PluginHandle
		{
			PluginId m_id;
			Interop_EntityId (*fPtrCreateEntity)(PluginId id, Interop_EntityCreateData* data);
			Interop_EntityId (*fPtrCreateTransmitter)(PluginId id, Interop_TransmitterCreateData* data);
			int  (*fPtrUpdateEntity)(PluginId id, Interop_EntityUpdateData* data);
			void (*fPtrDeleteEntity)(PluginId id, Interop_EntityDeleteData* data);
			void (*fPtrCreateDesignator)(PluginId id, Interop_DesignatorCreateData* data);
			void (*fPtrUpdateDesignator)(PluginId id, Interop_DesignatorUpdateData* data);
			void (*fPtrFireWeapon)(PluginId id, Interop_FireWeaponData* data);
			void (*fPtrDetonateMunition)(PluginId id, Interop_DetonationData* data);
			int  (*fPtrExecuteCommand)(PluginId id, const char* command, char* result, int resultLength);
			void (*fPtrRequestGameTypeInfoRefresh)(PluginId id);
			int  (*fPtrUpdateTransmitter)(PluginId id, Interop_TransmitterUpdateData* data);
			void (*fPtrDeleteTransmitter)(PluginId id, Interop_TransmitterDeleteData* data);
			void (*fPtrSendSignalUnit)(PluginId id, Interop_SignalData* data);

			PluginHandle():m_id(invalidId)
			{}
		};

		class VBSFUSION_API VBSFusionInterop
		{
		public:
			typedef Interop::PluginHandle* (*RegisterPluginCallbacksFunc)(const Interop::PluginCallbacks& callbacks);

			static PluginHandle registerPluginCallbacks(const PluginCallbacks& callbacks);
			static void setPluginCallbacks(RegisterPluginCallbacksFunc callbacks);
		private:
			static RegisterPluginCallbacksFunc registerCallbacks;
		};

		class Plugin
		{
			static PluginHandle& getHandle()
			{
				static PluginHandle handle;
				return handle;
			}

			static PluginId getId()
			{
				return getHandle().m_id;
			}

			static void setHandle(const PluginHandle& value)
			{
				getHandle() = value;
			}
		public:
			Plugin()
			{
				PluginCallbacks callbacks;
				callbacks.fPtrCreateEntity = &Plugin::createEntity;
				callbacks.fPtrCreateTransmitter = &Plugin::createTransmitter;
				callbacks.fPtrUpdateEntity = &Plugin::updateEntity;
				callbacks.fPtrDeleteEntity = &Plugin::deleteEntity;
				callbacks.fPtrCreateDesignator = &Plugin::createDesignator;
				callbacks.fPtrUpdateDesignator = &Plugin::updateDesignator;
				callbacks.fPtrFireWeapon = &Plugin::fireWeapon;
				callbacks.fPtrDetonateMunition = &Plugin::detonateMunition;
				callbacks.fPtrExecuteCommand = &Plugin::executeCommand;
				callbacks.fPtrRequestGameTypeInfoRefresh = &Plugin::requestGameTypeInfoRefresh;
				callbacks.fPtrUpdateTransmitter = &Plugin::updateTransmitter;
				callbacks.fPtrDeleteTransmitter = &Plugin::deleteTransmitter;
				callbacks.fPtrSendSignalUnit = &Plugin::sendSignalUnit;

				setHandle(VBSFusionInterop::registerPluginCallbacks(callbacks));
			}

			static Interop_EntityId createEntity(Interop_EntityCreateData* data)
			{
				return getHandle().fPtrCreateEntity(getId(), data);
			}

			static Interop_EntityId createTransmitter(Interop_TransmitterCreateData* data)
			{
				return getHandle().fPtrCreateTransmitter(getId(), data);
			}

			static int  updateEntity(Interop_EntityUpdateData* data)
			{
				return getHandle().fPtrUpdateEntity(getId(), data);
			}

			static void deleteEntity(Interop_EntityDeleteData* data)
			{
				return getHandle().fPtrDeleteEntity(getId(), data);
			}

			static void createDesignator(Interop_DesignatorCreateData* data)
			{
				return getHandle().fPtrCreateDesignator(getId(), data);
			}

			static void updateDesignator(Interop_DesignatorUpdateData* data)
			{
				return getHandle().fPtrUpdateDesignator(getId(), data);
			}

			static void fireWeapon(Interop_FireWeaponData* data)
			{
				return getHandle().fPtrFireWeapon(getId(), data);
			}

			static void detonateMunition(Interop_DetonationData* data)
			{
				return getHandle().fPtrDetonateMunition(getId(), data);
			}

			static int  executeCommand(const char* command, char* result, int resultLength)
			{
				return getHandle().fPtrExecuteCommand(getId(), command, result, resultLength);
			}

			static void requestGameTypeInfoRefresh()
			{
				return getHandle().fPtrRequestGameTypeInfoRefresh(getId());
			}

			static int  updateTransmitter(Interop_TransmitterUpdateData* data)
			{
				return getHandle().fPtrUpdateTransmitter(getId(), data);
			}

			static void deleteTransmitter(Interop_TransmitterDeleteData* data)
			{
				return getHandle().fPtrDeleteTransmitter(getId(), data);
			}

			static void sendSignalUnit(Interop_SignalData* data)
			{
				return getHandle().fPtrSendSignalUnit(getId(), data);
			}

		};
		static Plugin plugin;
	};
};

#endif