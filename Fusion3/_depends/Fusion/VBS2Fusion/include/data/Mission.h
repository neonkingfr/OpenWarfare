/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	Mission.h

Purpose:

	This file contains the declaration of the Mission class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			01-04/2009	RMR: Original Implementation

	2.01		11-02/2010	MHA: Comments Checked

	2.02		21-06/2011	SSD: Added	void setMissionName(string name);
										string getMissionName();


/************************************************************************/

#ifndef VBS2FUSION_MISSION_H
#define VBS2FUSION_MISSION_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard Includes
#include <sstream>
#include <string>


// Simcentric Includes
#include "position3D.h"
#include "VBSFusion.h"
#include "data/TriggerList.h"
#include "data/GroupList.h"
#include "data/VehicleList.h"
#include "data/EnvironmentState.h"

/***********************************************************************/
/* END OF INCLUDES
/***********************************************************************/


namespace VBSFusion
{	
	class VBSFUSION_API Mission
	{
	public:

		/*!
		The main constructor the Mission class. 
		*/
		Mission();

		/*!
		The main destructor the Mission class. 
		*/
		~Mission();

		/*!
		Sets the group list. 
		*/
		void setGroupList(const GroupList& groupList);

		/*!
		Returns the group list. 
		*/
		GroupList& getGroupList();

		/*!
		Sets the trigger list. 
		*/
		void setTriggerList(const TriggerList& triggerList);

		/*!
		Returns the trigger list. 
		*/
		TriggerList& getTriggerList();

		/*!
		Sets the vehicle list. 
		*/
		void setVehicleList(const VehicleList& vehicleList);

		/*!
		Returns the vehicle list. 
		*/
		VehicleList& getVehicleList();

		/*!
		Sets the EnvironmentState variable. 
		*/
		void setEnvironmentState(const EnvironmentState& environmentState);

		/*!
		Returns the EnvironmentState variable. 
		*/
		EnvironmentState& getEnvironmentState();


		/*!
		Sets the mission name variable. 
		*/
		void setMissionName(const std::string& name);

		/*!
		Returns the mission name variable. 
		*/
		std::string getMissionName();

	private:

		GroupList _groupList;
		VehicleList _vehicleList;
		TriggerList _triggerList;	
		EnvironmentState _environmentState;

		std::string _missionName;
	};
};

#endif //MISSION_H