/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	Vehicle.h

Purpose:

	This file contains the declaration of the VBS2 Type names

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			01-04/2009	SDS: Original Implementation
	2.0			10-01/2010	UDW: Ported into VBS2Fusion
	2.01		11-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2UNITLIST_TYPES_H
#define VBS2UNITLIST_TYPES_H

using namespace std;

namespace VBSFusion {
	namespace vbs2Type {
		class Airplane {
		public:

			enum AIRPLANE{
				VBS2_invis_camera_uav,
				VBS2_US_AF_F16C_GBU12,
				VBS2_US_AF_F16C_AGM65,
				AV8B,
				AV8B2,
				Camel,
				Camel2,
				DC3,
				A10,
				Su34,
				Su34B,
				VBS2_US_RQ7,
				VBS2_US_RQ11,
				VBS2_AU_RAAF_KC30A_MRTT,
				VBS2_GB_RAF_Harrier_GR9,
				VBS2_US_MC_AV8B_B,
				VBS2_AU_RAAF_C17_Globemaster,
				VBS2_AU_RAAF_C17_Globemaster_cargo,
				VBS2_AU_RAAF_F18_G,
				vbs2_au_scaneagle,
				vbs2_au_scaneagle2,
				vbs2_us_scaneagle,
				vbs2_us_scaneagle2,
				VBS2_US_AF_A10A_AGM65_HYDRA,
				VBS2_US_AF_A10A_GBU12_HYDRA,
				VBS2_US_AF_A10A_Mk82_HYDRA,
				vbs2_AU_RAAF_predator,
				vbs2_AU_RAAF_predator2,
				vbs2_GB_RAF_predator,
				vbs2_GB_RAF_predator2,
				vbs2_US_USAF_predator,
				vbs2_US_USAF_predator2,
				vbs2_GB_RAF_Reaper,
				vbs2_GB_RAF_Reaper2,
				vbs2_US_USAF_reaper,
				vbs2_US_USAF_reaper2,
				VBS2_AU_RAAF_C130J_G,
				VBS2_AU_RAAF_C130J_30_G,
				VBS2_GB_C130_C4,
				VBS2_GB_C130_C5,
				VBS2_NZ_RNZAF_C130H_B,
				VBS2_US_MC_KC130J_B,
				VBS2_US_AF_AC130U_B_L60_GAU12_M102,
				vbs2_GB_ARMY_DesertHawk,
				vbs2_GB_ARMY_DesertHawk2,
				VBS2_AU_RAAF_AP3C,
				vbs2_US_USAF_globalhawk,
				vbs2_US_USAF_globalhawk2,
				VBS2_GB_RAF_GR4,
				VBS2_ZZ_Air_Su22_w,
				vbs2_GB_ARMY_Watchkeeper,
				vbs2_GB_ARMY_Watchkeeper2,
				vbs2_us_navy_tomahawk_flyable_bluefor,
			};

			string airplane[53];
			Airplane(){
				airplane[0] ="VBS2_invis_camera_uav";
				airplane[1] ="VBS2_US_AF_F16C_GBU12";
				airplane[2] ="VBS2_US_AF_F16C_AGM65";
				airplane[3] ="AV8B";
				airplane[4] ="AV8B2";
				airplane[5] ="Camel";
				airplane[6] ="Camel2";
				airplane[7] ="DC3";
				airplane[8] ="A10";
				airplane[9] ="Su34";
				airplane[10] ="Su34B";
				airplane[11] ="VBS2_US_RQ7";
				airplane[12] ="VBS2_US_RQ11";
				airplane[13] ="VBS2_AU_RAAF_KC30A_MRTT";
				airplane[14] ="VBS2_GB_RAF_Harrier_GR9";
				airplane[15] ="VBS2_US_MC_AV8B_B";
				airplane[16] ="VBS2_AU_RAAF_C17_Globemaster";
				airplane[17] ="VBS2_AU_RAAF_C17_Globemaster_cargo";
				airplane[18] ="VBS2_AU_RAAF_F18_G";
				airplane[19] ="vbs2_au_scaneagle";
				airplane[20] ="vbs2_au_scaneagle2";
				airplane[21] ="vbs2_us_scaneagle";
				airplane[22] ="vbs2_us_scaneagle2";
				airplane[23] ="VBS2_US_AF_A10A_AGM65_HYDRA";
				airplane[24] ="VBS2_US_AF_A10A_GBU12_HYDRA";
				airplane[25] ="VBS2_US_AF_A10A_Mk82_HYDRA";
				airplane[26] ="vbs2_AU_RAAF_predator";
				airplane[27] ="vbs2_AU_RAAF_predator2";
				airplane[28] ="vbs2_GB_RAF_predator";
				airplane[29] ="vbs2_GB_RAF_predator2";
				airplane[30] ="vbs2_US_USAF_predator";
				airplane[31] ="vbs2_US_USAF_predator2";
				airplane[32] ="vbs2_GB_RAF_Reaper";
				airplane[33] ="vbs2_GB_RAF_Reaper2";
				airplane[34] ="vbs2_US_USAF_reaper";
				airplane[35] ="vbs2_US_USAF_reaper2";
				airplane[36] ="VBS2_AU_RAAF_C130J_G";
				airplane[37] ="VBS2_AU_RAAF_C130J_30_G";
				airplane[38] ="VBS2_GB_C130_C4";
				airplane[39] ="VBS2_GB_C130_C5";
				airplane[40] ="VBS2_NZ_RNZAF_C130H_B";
				airplane[41] ="VBS2_US_MC_KC130J_B";
				airplane[42] ="VBS2_US_AF_AC130U_B_L60_GAU12_M102";
				airplane[43] ="vbs2_GB_ARMY_DesertHawk";
				airplane[44] ="vbs2_GB_ARMY_DesertHawk2";
				airplane[45] ="VBS2_AU_RAAF_AP3C";
				airplane[46] ="vbs2_US_USAF_globalhawk";
				airplane[47] ="vbs2_US_USAF_globalhawk2";
				airplane[48] ="VBS2_GB_RAF_GR4";
				airplane[49] ="VBS2_ZZ_Air_Su22_w";
				airplane[50] ="vbs2_GB_ARMY_Watchkeeper";
				airplane[51] ="vbs2_GB_ARMY_Watchkeeper2";
				airplane[52] ="vbs2_us_navy_tomahawk_flyable_bluefor";
			};
		};

		class Car {
		public:

			enum CAR{
				VBS2_ZZ_BRDM_W,
				VBS2_ZZ_BRDM_D,
				VBS2_ZZ_BRDM_ATGM_W,
				VBS2_ZZ_BRDM_ATGM_D,
				VBS2_ZZ_army_BTR80_W,
				VBS2_ZZ_army_BTR80_D,
				VBS2_IQ_Civ_bus_city_01,
				VBS2_IQ_Civ_car_hatchback_01,
				VBS2_IQ_Civ_car_hatchback_01_ied,
				VBS2_IQ_Civ_car_hatchback_02,
				VBS2_IQ_Civ_car_hatchback_02_ied,
				VBS2_zz_sewagetruck,
				VBS2_IQ_Civ_datsuntruck_pickup_01,
				VBS2_IQ_Civ_datsuntruck_pickup_02,
				VBS2_IQ_Insurg_datsuntruck_pickup_01,
				VBS2_IQ_Insurg_datsuntruck_pickup_02,
				VBS2_IQ_Insurg_datsuntruck_pickup_pkm,
				VBS2_IQ_Insurg_datsuntruck_pickup_02_pkm,
				VBS2_IQ_Insurg_datsuntruck_pickup_dshkm,
				VBS2_IQ_Insurg_datsuntruck_pickup_02_dshkm,
				VBS2_IQ_Insurg_datsuntruck_pickup_spg9,
				VBS2_IQ_Insurg_datsuntruck_pickup_02_spg9,
				VBS2_IQ_Militia_datsuntruck_pickup_01,
				VBS2_IQ_Militia_datsuntruck_pickup_02,
				VBS2_IQ_Militia_datsuntruck_pickup_pkm,
				VBS2_IQ_Militia_datsuntruck_pickup_02_pkm,
				VBS2_IQ_Militia_datsuntruck_pickup_dshkm,
				VBS2_IQ_Militia_datsuntruck_pickup_02_dshkm,
				VBS2_IQ__Militia_datsuntruck_pickup_spg9,
				VBS2_IQ__Militia_datsuntruck_pickup_02_spg9,
				VBS2_zz_roller,
				VBS2_zz_tractor,
				VBS2_ZZ_Farm_Tractor,
				VBS2_IQ_Civil_jingle_truck,
				VBS2_IQ_Civil_jingle_truck_cargo,
				VBS2_IQ_Civil_2624_Watertruck,
				VBS2_IQ_Civil_2624_Dumptruck,
				VBS2_NZ_Army_Unimog_D,
				VBS2_NZ_Army_Unimog_W,
				VBS2_NZ_Army_Unimog_D_Open,
				VBS2_NZ_Army_Unimog_W_Open,
				VBS2_NZ_Army_UNIMOG_D_MAG58,
				VBS2_NZ_Army_UNIMOG_W_MAG58,
				VBS2_NZ_Army_UNIMOG_D_MAG58_Covered,
				VBS2_NZ_Army_UNIMOG_W_MAG58_Covered,
				VBS2_NZ_Army_UNIMOG_D_MEDIC,
				VBS2_NZ_Army_UNIMOG_W_MEDIC,
				VBS2_NZ_Army_UNIMOG_D_Fuel,
				VBS2_NZ_Army_UNIMOG_W_Fuel,
				VBS2_NZ_Army_UNIMOG_D_Ammo,
				VBS2_NZ_Army_UNIMOG_W_Ammo,
				VBS2_NZ_Army_UNIMOG_D_Repair,
				VBS2_NZ_Army_UNIMOG_W_Repair,
				VBS2_IQ_Civil_nissan_pickup,
				VBS2_IQ_Police_nissan_pickup,
				VBS2_NZ_Army_LOV_W,
				VBS2_NZ_Army_LOV_D,
				VBS2_NZ_Army_LOV_MV_GS_W,
				VBS2_NZ_Army_LOV_MV_GS_D,
				VBS2_NZ_Army_LOV_MV_C2_W,
				VBS2_NZ_Army_LOV_MV_C2_D,
				VBS2_NZ_Army_LOV_FRT_W,
				VBS2_NZ_Army_LOV_FRT_D,
				VBS2_NZ_Army_LOV_W_M2,
				VBS2_NZ_Army_LOV_D_M2,
				VBS2_NZ_Army_SOV_W_weapon,
				VBS2_NZ_Army_SOV_D_weapon,
				VBS2_ZZ_Army_toyota_hilux_01,
				VBS2_AU_ARMY_toyota_landcruiser_01,
				VBS2_IQ_Civ_toyota_landcruiser_01,
				VBS2_zz_concrete_mixer,
				VBS2_IQ_Civ_truck_light_transport,
				VBS2_ZZ_Uaz_469_W_Open,
				VBS2_ZZ_Uaz_469_D_Open,
				VBS2_ZZ_Uaz_469_W_Covered,
				VBS2_ZZ_Uaz_469_D_Covered,
				VBS2_ZZ_Ural_4320_W_Open,
				VBS2_ZZ_Ural_4320_D_Open,
				VBS2_ZZ_Ural_4320_W_Covered,
				VBS2_ZZ_Ural_4320_D_Covered,
				VBS2_ZZ_Ural_4320_W_Fuel,
				VBS2_ZZ_Ural_4320_D_Fuel,
				VBS2_ZZ_Ural_4320_W_Ammo,
				VBS2_ZZ_Ural_4320_D_Ammo,
				VBS2_ZZ_Ural_4320_W_Reapir,
				VBS2_ZZ_Ural_4320_D_Reapir,
				VBS2_IQ_Civ_van_utility_01,
				VBS2_IQ_Civ_van_utility_02,
				HMMWV50,
				HMMWVTOW,
				HMMWVMK,
				HMMWV,
				Truck5tMG,
				Truck5t,
				Truck5tOpen,
				Truck5tRepair,
				Truck5tReammo,
				Truck5tRefuel,
				Stryker_ICV_M2,
				Stryker_ICV_MK19,
				Stryker_TOW,
				UAZMG,
				UAZ_AGS30,
				UAZ,
				Ural,
				UralOpen,
				UralRepair,
				UralReammo,
				UralRefuel,
				UralCivil,
				UralCivil2,
				BRDM2,
				BRDM2_ATGM,
				Skoda,
				SkodaBlue,
				SkodaRed,
				SkodaGreen,
				datsun1_civil_1_open,
				datsun1_civil_2_covered,
				datsun1_civil_3_open,
				LandroverMG,
				Landrover_Closed,
				Landrover,
				Landrover_Police,
				Bus_city,
				tractor,
				car_hatchback,
				car_sedan,
				hilux1_civil_1_open,
				hilux1_civil_2_covered,
				hilux1_civil_3_open,
				DATSUN_PK1,
				MAA_DATSUN_PK2,
				DATSUN_DSHKM1,
				DATSUN_DSHKM2,
				HILUX_PK1,
				HILUX_PK2,
				HILUX_DSHKM1,
				HILUX_DSHKM2,
				VBS2_IQ_POLICE_M1151_PKM,
				VBS2_IQ_ARMY_M1151_PKM,
				VBS2_IQ_Civ_car_sedan_01,
				VBS2_IQ_Civ_car_sedan_01_ied,
				VBS2_IQ_Civ_car_sedan_02,
				VBS2_IQ_Civ_car_sedan_02_ied,
				VBS2_IQ_Civ_car_sedan_03,
				VBS2_IQ_Civ_car_sedan_03_ied,
				VBS2_AU_Army_Bushmaster_W_Mag58,
				VBS2_AU_Army_Bushmaster_D_Mag58,
				VBS2_US_GMV_W_M2,
				VBS2_US_GMV_D_M2,
				VBS2_US_GMV_W_Mk19,
				VBS2_US_GMV_D_Mk19,
				VBS2_US_GMV_W_m2_cargo,
				VBS2_US_GMV_D_m2_cargo,
				VBS2_US_GMV_W_M134,
				VBS2_US_GMV_D_M134,
				VBS2_US_ARMY_M1114_W,
				VBS2_US_ARMY_M1114_D,
				VBS2_US_ARMY_M1114_W_M2,
				VBS2_US_ARMY_M1114_D_M2,
				VBS2_US_ARMY_M1114_W_M240,
				VBS2_US_ARMY_M1114_D_M240,
				VBS2_US_ARMY_M1114_W_Mk19,
				VBS2_US_ARMY_M1114_D_Mk19,
				VBS2_US_MC_M1114_IWP_W,
				VBS2_US_MC_M1114_IWP_D,
				VBS2_US_MC_M1114_IWP_W_ATammo,
				VBS2_US_MC_M1114_IWP_D_ATammo,
				VBS2_US_MC_M1114_IWP_W_M2,
				VBS2_US_MC_M1114_IWP_W_M2_Fording_Kit,
				VBS2_US_MC_M1114_IWP_D_M2,
				VBS2_US_MC_M1114_IWP_D_M2_Fording_Kit,
				VBS2_US_MC_M1114_IWP_W_M2_NLOD,
				VBS2_US_MC_M1114_IWP_D_M2_NLOD,
				VBS2_US_MC_M1114_IWP_W_M240,
				VBS2_US_MC_M1114_IWP_D_M240,
				VBS2_US_MC_M1114_IWP_W_Mk19,
				VBS2_US_MC_M1114_IWP_D_Mk19,
				VBS2_US_MC_M1114_W_ADS,
				VBS2_US_MC_M1114_D_ADS,
				VBS2_US_MC_M1121_W_TOW,
				VBS2_US_MC_M1121_D_TOW,
				VBS2_US_ARMY_M1151UAH_W_M2,
				VBS2_US_ARMY_M1151UAH_D_M2,
				VBS2_US_ARMY_M1151UAH_W_M240,
				VBS2_US_ARMY_M1151UAH_D_M240,
				VBS2_US_ARMY_M1151UAH_W_Mk19,
				VBS2_US_ARMY_M1151UAH_D_Mk19,
				VBS2_US_ARMY_M997A2_W,
				VBS2_US_ARMY_M997A2_D,
				VBS2_US_MC_M997A2_W,
				VBS2_US_MC_M997A2_D,
				VBS2_US_ARMY_M1078_W,
				VBS2_US_ARMY_M1078_D,
				VBS2_US_ARMY_M1078_LSAC_W,
				VBS2_US_ARMY_M1078_LSAC_D,
				VBS2_US_ARMY_M1089_W,
				VBS2_US_ARMY_M1089_D,
				VBS2_US_ARMY_M923_W_COVERED,
				VBS2_US_ARMY_M923_D_COVERED,
				VBS2_US_ARMY_M923_W_OPEN,
				VBS2_US_ARMY_M923_D_OPEN,
				VBS2_US_ARMY_M923_W_AMMO,
				VBS2_US_ARMY_M923_D_AMMO,
				VBS2_US_ARMY_M923_M2_W_COVERED,
				VBS2_US_ARMY_M923_M2_D_COVERED,
				VBS2_US_ARMY_M923_M2_W_OPEN,
				VBS2_US_ARMY_M923_M2_D_OPEN,
				VBS2_US_ARMY_M923_M2_W_AMMO,
				VBS2_US_ARMY_M923_M2_D_AMMO,
				VBS2_US_ARMY_RG31_D,
				VBS2_US_ARMY_RG31_M2_D,
				VBS2_US_ARMY_RG31_Mk19_D,
				VBS2_US_ARMY_RG31_M240_D,
				VBS2_US_MC_RG31_D,
				VBS2_US_MC_RG31_M2_D,
				VBS2_US_MC_RG31_Mk19_D,
				VBS2_US_MC_RG31_M240_D,
				VBS2_US_Army_RG33L_D_M2,
				VBS2_US_Army_RG33_D_M2,
				VBS2_US_Army_M1117_D,
				VBS2_us_usmc_buffalo_D,
				VBS2_us_Army_buffalo_D,
				VBS2_GB_Mastiff_W,
				VBS2_GB_Mastiff_D,
				VBS2_GB_Mastiff_W_HMG,
				VBS2_GB_Mastiff_D_HMG,
				VBS2_GB_Mastiff_W_GPMG,
				VBS2_GB_Mastiff_D_GPMG,
				VBS2_GB_Mastiff_W_GPMG_Pintle,
				VBS2_GB_Mastiff_D_GPMG_Pintle,
				VBS2_USMC_Cougar_W,
				VBS2_USMC_Cougar_W_M2,
				VBS2_USMC_Cougar_W_M240G,
				VBS2_USMC_Cougar_W_Mk19,
				VBS2_GB_Army_Support_9t_W_Covered,
				VBS2_GB_Army_Support_9t_D_Covered,
				VBS2_GB_Army_Support_9t_W_Open,
				VBS2_GB_Army_Support_9t_D_Open,
				VBS2_GB_Army_Support_9t_W_Covered_UpArmour,
				VBS2_GB_Army_Support_9t_D_Covered_UpArmour,
				VBS2_GB_Army_Support_9t_W_Open_UpArmour,
				VBS2_GB_Army_Support_9t_D_Open_UpArmour,
				VBS2_GB_Army_Support_9t_W_Covered_UpArmour_GPMG,
				VBS2_GB_Army_Support_9t_D_Covered_UpArmour_GPMG,
				VBS2_GB_Army_Support_9t_W_Open_UpArmour_GPMG,
				VBS2_GB_Army_Support_9t_D_Open_UpArmour_GPMG,
				VBS2_GB_Army_Support_6t_W_Covered,
				VBS2_GB_Army_Support_6t_D_Covered,
				VBS2_GB_Army_Support_6t_W_Open,
				VBS2_GB_Army_Support_6t_D_Open,
				VBS2_GB_Army_Support_6t_W_Covered_UpArmour,
				VBS2_GB_Army_Support_6t_D_Covered_UpArmour,
				VBS2_GB_Army_Support_6t_W_Open_UpArmour,
				VBS2_GB_Army_Support_6t_D_Open_UpArmour,
				VBS2_GB_Army_Support_6t_W_Covered_UpArmour_GPMG,
				VBS2_GB_Army_Support_6t_D_Covered_UpArmour_GPMG,
				VBS2_GB_Army_Support_6t_W_Open_UpArmour_GPMG,
				VBS2_GB_Army_Support_6t_D_Open_UpArmour_GPMG,
				VBS2_GB_Army_Support_15t_W_Covered,
				VBS2_GB_Army_Support_15t_D_Covered,
				VBS2_GB_Army_Support_15t_W_Open,
				VBS2_GB_Army_Support_15t_D_Open,
				VBS2_GB_Army_Support_15t_W_Covered_UpArmour,
				VBS2_GB_Army_Support_15t_D_Covered_UpArmour,
				VBS2_GB_Army_Support_15t_W_Open_UpArmour,
				VBS2_GB_Army_Support_15t_D_Open_UpArmour,
				VBS2_GB_Army_Support_15t_W_Covered_UpArmour_GPMG,
				VBS2_GB_Army_Support_15t_D_Covered_UpArmour_GPMG,
				VBS2_GB_Army_Support_15t_W_Open_UpArmour_GPMG,
				VBS2_GB_Army_Support_15t_D_Open_UpArmour_GPMG,
				VBS2_GB_Army_Recovery_15t_W,
				VBS2_GB_Army_Recovery_15t_D,
				VBS2_GB_Army_Recovery_15t_W_UpArmour,
				VBS2_GB_Army_Recovery_15t_D_UpArmour,
				VBS2_GB_Army_Recovery_15t_W_UpArmour_gpmg,
				VBS2_GB_Army_Recovery_15t_D_UpArmour_gpmg,
				VBS2_AU_Army_Unimog_W_Mag58,
				VBS2_AU_Army_Unimog_D_Mag58,
				VBS2_AU_Army_Unimog_W_ambulance,
				VBS2_AU_Army_Unimog_D_ambulance,
				VBS2_AU_Army_Unimog_W_covered,
				VBS2_AU_Army_Unimog_D_covered,
				VBS2_AU_Army_Unimog_W_repair,
				VBS2_AU_Army_Unimog_D_repair,
				VBS2_AU_Army_Unimog_W_ammo,
				VBS2_AU_Army_Unimog_D_ammo,
				VBS2_AU_Army_AslavPC_W_M2,
				VBS2_AU_Army_AslavPC_D_M2,
				VBS2_AU_Army_AslavPCRWS_W_M2,
				VBS2_AU_Army_AslavPCRWS_D_M2,
				VBS2_AU_Army_AslavPCRWS_W_Mk19,
				VBS2_AU_Army_AslavPCRWS_D_Mk19,
				VBS2_AU_Army_Aslav_W_25,
				VBS2_AU_Army_Aslav_D_25,
				VBS2_CA_Army_Lav3_W_25,
				VBS2_CA_Army_Lav3_W_RWS,
				VBS2_NZ_Army_Lav3_W_25,
				VBS2_NZ_Army_Lav3_D_25,
				VBS2_US_MC_LAV25_W_UPARMOR,
				VBS2_US_MC_LAV25_D_UPARMOR,
				vbs2_US_Army_StrykerICV_A_M2,
				vbs2_US_Army_StrykerICV_A_Mk19,
				vbs2_US_Army_StrykerRecon_A_M2,
				vbs2_US_Army_StrykerRecon_A_Mk19,
				vbs2_US_Army_StrykerMGS_A_M2,
				vbs2_US_Army_StrykerMC_A_M2,
				vbs2_US_Army_StrykerMC_A_MK19,
				vbs2_US_Army_StrykerCommand_A_M2,
				vbs2_US_Army_StrykerCommand_A_Mk19,
				vbs2_US_Army_StrykerFSV_A_M2,
				vbs2_US_Army_StrykerFSV_A_Mk19,
				vbs2_US_Army_StrykerESV_A_M2,
				vbs2_US_Army_StrykerESV_A_Mk19,
				vbs2_US_Army_StrykerESV_A_M2_Plow,
				vbs2_US_Army_StrykerESV_A_M2_Roller,
				vbs2_US_Army_StrykerMEV_A_M2,
				vbs2_US_Army_StrykerMEV_A_Mk19,
				VBS2_AU_Army_M978Hemtt_W,
				VBS2_AU_Army_M978Hemtt_D,
				VBS2_US_Army_M977_W,
				VBS2_US_Army_M977_D,
				VBS2_US_Army_M977_UpArmor_W,
				VBS2_US_Army_M977_UpArmor_D,
				VBS2_US_Army_M978_W,
				VBS2_US_Army_M978_D,
				VBS2_US_Army_M978_UpArmor_W,
				VBS2_US_Army_M978_UpArmor_D,
				VBS2_US_MC_M978_W,
				VBS2_US_MC_M978_D,
				VBS2_US_Army_M985_W,
				VBS2_US_Army_M985_D,
				VBS2_US_Army_M985_UpArmor_W,
				VBS2_US_Army_M985_UpArmor_D,
				VBS2_US_MC_MTVR_W,
				VBS2_US_MC_MTVR_D,
				VBS2_US_MC_MTVR_W_repair,
				VBS2_US_MC_MTVR_D_repair,
				VBS2_US_MC_MTVR_W_ammo,
				VBS2_US_MC_MTVR_D_ammo,
				VBS2_US_MC_MTVR_W_M2,
				VBS2_US_MC_MTVR_D_M2,
				VBS2_US_MC_MTVR_W_M2_repair,
				VBS2_US_MC_MTVR_D_M2_repair,
				VBS2_US_MC_MTVR_W_M2_ammo,
				VBS2_US_MC_MTVR_D_M2_ammo,
				VBS2_US_MC_MTVR_W_M240,
				VBS2_US_MC_MTVR_D_M240,
				VBS2_US_MC_MTVR_W_M240_repair,
				VBS2_US_MC_MTVR_D_M240_repair,
				VBS2_US_MC_MTVR_W_M240_ammo,
				VBS2_US_MC_MTVR_D_M240_ammo,
				VBS2_US_MC_MTVR_W_Mk19,
				VBS2_US_MC_MTVR_D_Mk19,
				VBS2_US_MC_MTVR_W_Mk19_repair,
				VBS2_US_MC_MTVR_D_Mk19_repair,
				VBS2_US_MC_MTVR_W_Mk19_ammo,
				VBS2_US_MC_MTVR_D_Mk19_ammo,
				VBS2_GB_Army_Pinzgauer_4x4_W,
				VBS2_GB_Army_Pinzgauer_4x4_D,
				VBS2_GB_Army_Pinzgauer_6x6_W,
				VBS2_GB_Army_Pinzgauer_6x6_D,
				VBS2_GB_Army_Pinzgauer_Vector_W,
				VBS2_GB_Army_Pinzgauer_Vector_D,
				VBS2_AU_Army_Landrover_D,
				VBS2_AU_Army_Landrover_W,
				VBS2_AU_Army_Landrover_D_ATammo,
				VBS2_AU_Army_Landrover_W_ATammo,
				VBS2_AU_Army_LandroverSRV_D_M2,
				VBS2_AU_Army_LandroverSRV_W_M2,
				VBS2_AU_Army_LandroverLRPV_D_M2,
				VBS2_AU_Army_LandroverLRPV_W_M2,
				VBS2_AU_Army_LandroverLRPV_D_Mk19,
				VBS2_AU_Army_LandroverLRPV_W_Mk19,
				VBS2_GB_Army_LandRover_Pulse_W_Litters,
				VBS2_GB_Army_LandRover_Pulse_D_Litters,
				VBS2_GB_Army_LandRover_Pulse_W_Seats,
				VBS2_GB_Army_LandRover_Pulse_D_Seats,
				VBS2_GB_Army_LandRover_Snatch_W,
				VBS2_GB_Army_LandRover_Snatch_D,
				VBS2_GB_Army_LandRover_WMIK_W_GPMG,
				VBS2_GB_Army_LandRover_WMIK_D_GPMG,
				VBS2_GB_Army_LandRover_WMIK_W_HMG,
				VBS2_GB_Army_LandRover_WMIK_D_HMG,
				VBS2_GB_Army_LandRover_WMIK_W_GMG,
				VBS2_GB_Army_LandRover_WMIK_D_GMG,
				VBS2_GB_Army_LandRover_Wolf_110_W_SoftTop_Bowman,
				VBS2_GB_Army_LandRover_Wolf_110_D_SoftTop_Bowman,
				VBS2_GB_Army_LandRover_Wolf_110_W_HardTop_Bowman,
				VBS2_GB_Army_LandRover_Wolf_110_D_HardTop_Bowman,
				VBS2_GB_Army_LandRover_Wolf_110_FFR_W,
				VBS2_GB_Army_LandRover_Wolf_110_FFR_D,
				VBS2_GB_Army_Jackal_MWMIK_W_HMG,
				VBS2_GB_Army_Jackal_MWMIK_D_HMG,
				VBS2_GB_Army_Jackal_MWMIK_W_GMG,
				VBS2_GB_Army_Jackal_MWMIK_D_GMG,
			};

			string car[397];
			Car(){
				car[0] ="VBS2_ZZ_BRDM_W";
				car[1] ="VBS2_ZZ_BRDM_D";
				car[2] ="VBS2_ZZ_BRDM_ATGM_W";
				car[3] ="VBS2_ZZ_BRDM_ATGM_D";
				car[4] ="VBS2_ZZ_army_BTR80_W";
				car[5] ="VBS2_ZZ_army_BTR80_D";
				car[6] ="VBS2_IQ_Civ_bus_city_01";
				car[7] ="VBS2_IQ_Civ_car_hatchback_01";
				car[8] ="VBS2_IQ_Civ_car_hatchback_01_ied";
				car[9] ="VBS2_IQ_Civ_car_hatchback_02";
				car[10] ="VBS2_IQ_Civ_car_hatchback_02_ied";
				car[11] ="VBS2_zz_sewagetruck";
				car[12] ="VBS2_IQ_Civ_datsuntruck_pickup_01";
				car[13] ="VBS2_IQ_Civ_datsuntruck_pickup_02";
				car[14] ="VBS2_IQ_Insurg_datsuntruck_pickup_01";
				car[15] ="VBS2_IQ_Insurg_datsuntruck_pickup_02";
				car[16] ="VBS2_IQ_Insurg_datsuntruck_pickup_pkm";
				car[17] ="VBS2_IQ_Insurg_datsuntruck_pickup_02_pkm";
				car[18] ="VBS2_IQ_Insurg_datsuntruck_pickup_dshkm";
				car[19] ="VBS2_IQ_Insurg_datsuntruck_pickup_02_dshkm";
				car[20] ="VBS2_IQ_Insurg_datsuntruck_pickup_spg9";
				car[21] ="VBS2_IQ_Insurg_datsuntruck_pickup_02_spg9";
				car[22] ="VBS2_IQ_Militia_datsuntruck_pickup_01";
				car[23] ="VBS2_IQ_Militia_datsuntruck_pickup_02";
				car[24] ="VBS2_IQ_Militia_datsuntruck_pickup_pkm";
				car[25] ="VBS2_IQ_Militia_datsuntruck_pickup_02_pkm";
				car[26] ="VBS2_IQ_Militia_datsuntruck_pickup_dshkm";
				car[27] ="VBS2_IQ_Militia_datsuntruck_pickup_02_dshkm";
				car[28] ="VBS2_IQ__Militia_datsuntruck_pickup_spg9";
				car[29] ="VBS2_IQ__Militia_datsuntruck_pickup_02_spg9";
				car[30] ="VBS2_zz_roller";
				car[31] ="VBS2_zz_tractor";
				car[32] ="VBS2_ZZ_Farm_Tractor";
				car[33] ="VBS2_IQ_Civil_jingle_truck";
				car[34] ="VBS2_IQ_Civil_jingle_truck_cargo";
				car[35] ="VBS2_IQ_Civil_2624_Watertruck";
				car[36] ="VBS2_IQ_Civil_2624_Dumptruck";
				car[37] ="VBS2_NZ_Army_Unimog_D";
				car[38] ="VBS2_NZ_Army_Unimog_W";
				car[39] ="VBS2_NZ_Army_Unimog_D_Open";
				car[40] ="VBS2_NZ_Army_Unimog_W_Open";
				car[41] ="VBS2_NZ_Army_UNIMOG_D_MAG58";
				car[42] ="VBS2_NZ_Army_UNIMOG_W_MAG58";
				car[43] ="VBS2_NZ_Army_UNIMOG_D_MAG58_Covered";
				car[44] ="VBS2_NZ_Army_UNIMOG_W_MAG58_Covered";
				car[45] ="VBS2_NZ_Army_UNIMOG_D_MEDIC";
				car[46] ="VBS2_NZ_Army_UNIMOG_W_MEDIC";
				car[47] ="VBS2_NZ_Army_UNIMOG_D_Fuel";
				car[48] ="VBS2_NZ_Army_UNIMOG_W_Fuel";
				car[49] ="VBS2_NZ_Army_UNIMOG_D_Ammo";
				car[50] ="VBS2_NZ_Army_UNIMOG_W_Ammo";
				car[51] ="VBS2_NZ_Army_UNIMOG_D_Repair";
				car[52] ="VBS2_NZ_Army_UNIMOG_W_Repair";
				car[53] ="VBS2_IQ_Civil_nissan_pickup";
				car[54] ="VBS2_IQ_Police_nissan_pickup";
				car[55] ="VBS2_NZ_Army_LOV_W";
				car[56] ="VBS2_NZ_Army_LOV_D";
				car[57] ="VBS2_NZ_Army_LOV_MV_GS_W";
				car[58] ="VBS2_NZ_Army_LOV_MV_GS_D";
				car[59] ="VBS2_NZ_Army_LOV_MV_C2_W";
				car[60] ="VBS2_NZ_Army_LOV_MV_C2_D";
				car[61] ="VBS2_NZ_Army_LOV_FRT_W";
				car[62] ="VBS2_NZ_Army_LOV_FRT_D";
				car[63] ="VBS2_NZ_Army_LOV_W_M2";
				car[64] ="VBS2_NZ_Army_LOV_D_M2";
				car[65] ="VBS2_NZ_Army_SOV_W_weapon";
				car[66] ="VBS2_NZ_Army_SOV_D_weapon";
				car[67] ="VBS2_ZZ_Army_toyota_hilux_01";
				car[68] ="VBS2_AU_ARMY_toyota_landcruiser_01";
				car[69] ="VBS2_IQ_Civ_toyota_landcruiser_01";
				car[70] ="VBS2_zz_concrete_mixer";
				car[71] ="VBS2_IQ_Civ_truck_light_transport";
				car[72] ="VBS2_ZZ_Uaz_469_W_Open";
				car[73] ="VBS2_ZZ_Uaz_469_D_Open";
				car[74] ="VBS2_ZZ_Uaz_469_W_Covered";
				car[75] ="VBS2_ZZ_Uaz_469_D_Covered";
				car[76] ="VBS2_ZZ_Ural_4320_W_Open";
				car[77] ="VBS2_ZZ_Ural_4320_D_Open";
				car[78] ="VBS2_ZZ_Ural_4320_W_Covered";
				car[79] ="VBS2_ZZ_Ural_4320_D_Covered";
				car[80] ="VBS2_ZZ_Ural_4320_W_Fuel";
				car[81] ="VBS2_ZZ_Ural_4320_D_Fuel";
				car[82] ="VBS2_ZZ_Ural_4320_W_Ammo";
				car[83] ="VBS2_ZZ_Ural_4320_D_Ammo";
				car[84] ="VBS2_ZZ_Ural_4320_W_Reapir";
				car[85] ="VBS2_ZZ_Ural_4320_D_Reapir";
				car[86] ="VBS2_IQ_Civ_van_utility_01";
				car[87] ="VBS2_IQ_Civ_van_utility_02";
				car[88] ="HMMWV50";
				car[89] ="HMMWVTOW";
				car[90] ="HMMWVMK";
				car[91] ="HMMWV";
				car[92] ="Truck5tMG";
				car[93] ="Truck5t";
				car[94] ="Truck5tOpen";
				car[95] ="Truck5tRepair";
				car[96] ="Truck5tReammo";
				car[97] ="Truck5tRefuel";
				car[98] ="Stryker_ICV_M2";
				car[99] ="Stryker_ICV_MK19";
				car[100] ="Stryker_TOW";
				car[101] ="UAZMG";
				car[102] ="UAZ_AGS30";
				car[103] ="UAZ";
				car[104] ="Ural";
				car[105] ="UralOpen";
				car[106] ="UralRepair";
				car[107] ="UralReammo";
				car[108] ="UralRefuel";
				car[109] ="UralCivil";
				car[110] ="UralCivil2";
				car[111] ="BRDM2";
				car[112] ="BRDM2_ATGM";
				car[113] ="Skoda";
				car[114] ="SkodaBlue";
				car[115] ="SkodaRed";
				car[116] ="SkodaGreen";
				car[117] ="datsun1_civil_1_open";
				car[118] ="datsun1_civil_2_covered";
				car[119] ="datsun1_civil_3_open";
				car[120] ="LandroverMG";
				car[121] ="Landrover_Closed";
				car[122] ="Landrover";
				car[123] ="Landrover_Police";
				car[124] ="Bus_city";
				car[125] ="tractor";
				car[126] ="car_hatchback";
				car[127] ="car_sedan";
				car[128] ="hilux1_civil_1_open";
				car[129] ="hilux1_civil_2_covered";
				car[130] ="hilux1_civil_3_open";
				car[131] ="DATSUN_PK1";
				car[132] ="MAA_DATSUN_PK2";
				car[133] ="DATSUN_DSHKM1";
				car[134] ="DATSUN_DSHKM2";
				car[135] ="HILUX_PK1";
				car[136] ="HILUX_PK2";
				car[137] ="HILUX_DSHKM1";
				car[138] ="HILUX_DSHKM2";
				car[139] ="VBS2_IQ_POLICE_M1151_PKM";
				car[140] ="VBS2_IQ_ARMY_M1151_PKM";
				car[141] ="VBS2_IQ_Civ_car_sedan_01";
				car[142] ="VBS2_IQ_Civ_car_sedan_01_ied";
				car[143] ="VBS2_IQ_Civ_car_sedan_02";
				car[144] ="VBS2_IQ_Civ_car_sedan_02_ied";
				car[145] ="VBS2_IQ_Civ_car_sedan_03";
				car[146] ="VBS2_IQ_Civ_car_sedan_03_ied";
				car[147] ="VBS2_AU_Army_Bushmaster_W_Mag58";
				car[148] ="VBS2_AU_Army_Bushmaster_D_Mag58";
				car[149] ="VBS2_US_GMV_W_M2";
				car[150] ="VBS2_US_GMV_D_M2";
				car[151] ="VBS2_US_GMV_W_Mk19";
				car[152] ="VBS2_US_GMV_D_Mk19";
				car[153] ="VBS2_US_GMV_W_m2_cargo";
				car[154] ="VBS2_US_GMV_D_m2_cargo";
				car[155] ="VBS2_US_GMV_W_M134";
				car[156] ="VBS2_US_GMV_D_M134";
				car[157] ="VBS2_US_ARMY_M1114_W";
				car[158] ="VBS2_US_ARMY_M1114_D";
				car[159] ="VBS2_US_ARMY_M1114_W_M2";
				car[160] ="VBS2_US_ARMY_M1114_D_M2";
				car[161] ="VBS2_US_ARMY_M1114_W_M240";
				car[162] ="VBS2_US_ARMY_M1114_D_M240";
				car[163] ="VBS2_US_ARMY_M1114_W_Mk19";
				car[164] ="VBS2_US_ARMY_M1114_D_Mk19";
				car[165] ="VBS2_US_MC_M1114_IWP_W";
				car[166] ="VBS2_US_MC_M1114_IWP_D";
				car[167] ="VBS2_US_MC_M1114_IWP_W_ATammo";
				car[168] ="VBS2_US_MC_M1114_IWP_D_ATammo";
				car[169] ="VBS2_US_MC_M1114_IWP_W_M2";
				car[170] ="VBS2_US_MC_M1114_IWP_W_M2_Fording_Kit";
				car[171] ="VBS2_US_MC_M1114_IWP_D_M2";
				car[172] ="VBS2_US_MC_M1114_IWP_D_M2_Fording_Kit";
				car[173] ="VBS2_US_MC_M1114_IWP_W_M2_NLOD";
				car[174] ="VBS2_US_MC_M1114_IWP_D_M2_NLOD";
				car[175] ="VBS2_US_MC_M1114_IWP_W_M240";
				car[176] ="VBS2_US_MC_M1114_IWP_D_M240";
				car[177] ="VBS2_US_MC_M1114_IWP_W_Mk19";
				car[178] ="VBS2_US_MC_M1114_IWP_D_Mk19";
				car[179] ="VBS2_US_MC_M1114_W_ADS";
				car[180] ="VBS2_US_MC_M1114_D_ADS";
				car[181] ="VBS2_US_MC_M1121_W_TOW";
				car[182] ="VBS2_US_MC_M1121_D_TOW";
				car[183] ="VBS2_US_ARMY_M1151UAH_W_M2";
				car[184] ="VBS2_US_ARMY_M1151UAH_D_M2";
				car[185] ="VBS2_US_ARMY_M1151UAH_W_M240";
				car[186] ="VBS2_US_ARMY_M1151UAH_D_M240";
				car[187] ="VBS2_US_ARMY_M1151UAH_W_Mk19";
				car[188] ="VBS2_US_ARMY_M1151UAH_D_Mk19";
				car[189] ="VBS2_US_ARMY_M997A2_W";
				car[190] ="VBS2_US_ARMY_M997A2_D";
				car[191] ="VBS2_US_MC_M997A2_W";
				car[192] ="VBS2_US_MC_M997A2_D";
				car[193] ="VBS2_US_ARMY_M1078_W";
				car[194] ="VBS2_US_ARMY_M1078_D";
				car[195] ="VBS2_US_ARMY_M1078_LSAC_W";
				car[196] ="VBS2_US_ARMY_M1078_LSAC_D";
				car[197] ="VBS2_US_ARMY_M1089_W";
				car[198] ="VBS2_US_ARMY_M1089_D";
				car[199] ="VBS2_US_ARMY_M923_W_COVERED";
				car[200] ="VBS2_US_ARMY_M923_D_COVERED";
				car[201] ="VBS2_US_ARMY_M923_W_OPEN";
				car[202] ="VBS2_US_ARMY_M923_D_OPEN";
				car[203] ="VBS2_US_ARMY_M923_W_AMMO";
				car[204] ="VBS2_US_ARMY_M923_D_AMMO";
				car[205] ="VBS2_US_ARMY_M923_M2_W_COVERED";
				car[206] ="VBS2_US_ARMY_M923_M2_D_COVERED";
				car[207] ="VBS2_US_ARMY_M923_M2_W_OPEN";
				car[208] ="VBS2_US_ARMY_M923_M2_D_OPEN";
				car[209] ="VBS2_US_ARMY_M923_M2_W_AMMO";
				car[210] ="VBS2_US_ARMY_M923_M2_D_AMMO";
				car[211] ="VBS2_US_ARMY_RG31_D";
				car[212] ="VBS2_US_ARMY_RG31_M2_D";
				car[213] ="VBS2_US_ARMY_RG31_Mk19_D";
				car[214] ="VBS2_US_ARMY_RG31_M240_D";
				car[215] ="VBS2_US_MC_RG31_D";
				car[216] ="VBS2_US_MC_RG31_M2_D";
				car[217] ="VBS2_US_MC_RG31_Mk19_D";
				car[218] ="VBS2_US_MC_RG31_M240_D";
				car[219] ="VBS2_US_Army_RG33L_D_M2";
				car[220] ="VBS2_US_Army_RG33_D_M2";
				car[221] ="VBS2_US_Army_M1117_D";
				car[222] ="VBS2_us_usmc_buffalo_D";
				car[223] ="VBS2_us_Army_buffalo_D";
				car[224] ="VBS2_GB_Mastiff_W";
				car[225] ="VBS2_GB_Mastiff_D";
				car[226] ="VBS2_GB_Mastiff_W_HMG";
				car[227] ="VBS2_GB_Mastiff_D_HMG";
				car[228] ="VBS2_GB_Mastiff_W_GPMG";
				car[229] ="VBS2_GB_Mastiff_D_GPMG";
				car[230] ="VBS2_GB_Mastiff_W_GPMG_Pintle";
				car[231] ="VBS2_GB_Mastiff_D_GPMG_Pintle";
				car[232] ="VBS2_USMC_Cougar_W";
				car[233] ="VBS2_USMC_Cougar_W_M2";
				car[234] ="VBS2_USMC_Cougar_W_M240G";
				car[235] ="VBS2_USMC_Cougar_W_Mk19";
				car[236] ="VBS2_GB_Army_Support_9t_W_Covered";
				car[237] ="VBS2_GB_Army_Support_9t_D_Covered";
				car[238] ="VBS2_GB_Army_Support_9t_W_Open";
				car[239] ="VBS2_GB_Army_Support_9t_D_Open";
				car[240] ="VBS2_GB_Army_Support_9t_W_Covered_UpArmour";
				car[241] ="VBS2_GB_Army_Support_9t_D_Covered_UpArmour";
				car[242] ="VBS2_GB_Army_Support_9t_W_Open_UpArmour";
				car[243] ="VBS2_GB_Army_Support_9t_D_Open_UpArmour";
				car[244] ="VBS2_GB_Army_Support_9t_W_Covered_UpArmour_GPMG";
				car[245] ="VBS2_GB_Army_Support_9t_D_Covered_UpArmour_GPMG";
				car[246] ="VBS2_GB_Army_Support_9t_W_Open_UpArmour_GPMG";
				car[247] ="VBS2_GB_Army_Support_9t_D_Open_UpArmour_GPMG";
				car[248] ="VBS2_GB_Army_Support_6t_W_Covered";
				car[249] ="VBS2_GB_Army_Support_6t_D_Covered";
				car[250] ="VBS2_GB_Army_Support_6t_W_Open";
				car[251] ="VBS2_GB_Army_Support_6t_D_Open";
				car[252] ="VBS2_GB_Army_Support_6t_W_Covered_UpArmour";
				car[253] ="VBS2_GB_Army_Support_6t_D_Covered_UpArmour";
				car[254] ="VBS2_GB_Army_Support_6t_W_Open_UpArmour";
				car[255] ="VBS2_GB_Army_Support_6t_D_Open_UpArmour";
				car[256] ="VBS2_GB_Army_Support_6t_W_Covered_UpArmour_GPMG";
				car[257] ="VBS2_GB_Army_Support_6t_D_Covered_UpArmour_GPMG";
				car[258] ="VBS2_GB_Army_Support_6t_W_Open_UpArmour_GPMG";
				car[259] ="VBS2_GB_Army_Support_6t_D_Open_UpArmour_GPMG";
				car[260] ="VBS2_GB_Army_Support_15t_W_Covered";
				car[261] ="VBS2_GB_Army_Support_15t_D_Covered";
				car[262] ="VBS2_GB_Army_Support_15t_W_Open";
				car[263] ="VBS2_GB_Army_Support_15t_D_Open";
				car[264] ="VBS2_GB_Army_Support_15t_W_Covered_UpArmour";
				car[265] ="VBS2_GB_Army_Support_15t_D_Covered_UpArmour";
				car[266] ="VBS2_GB_Army_Support_15t_W_Open_UpArmour";
				car[267] ="VBS2_GB_Army_Support_15t_D_Open_UpArmour";
				car[268] ="VBS2_GB_Army_Support_15t_W_Covered_UpArmour_GPMG";
				car[269] ="VBS2_GB_Army_Support_15t_D_Covered_UpArmour_GPMG";
				car[270] ="VBS2_GB_Army_Support_15t_W_Open_UpArmour_GPMG";
				car[271] ="VBS2_GB_Army_Support_15t_D_Open_UpArmour_GPMG";
				car[272] ="VBS2_GB_Army_Recovery_15t_W";
				car[273] ="VBS2_GB_Army_Recovery_15t_D";
				car[274] ="VBS2_GB_Army_Recovery_15t_W_UpArmour";
				car[275] ="VBS2_GB_Army_Recovery_15t_D_UpArmour";
				car[276] ="VBS2_GB_Army_Recovery_15t_W_UpArmour_gpmg";
				car[277] ="VBS2_GB_Army_Recovery_15t_D_UpArmour_gpmg";
				car[278] ="VBS2_AU_Army_Unimog_W_Mag58";
				car[279] ="VBS2_AU_Army_Unimog_D_Mag58";
				car[280] ="VBS2_AU_Army_Unimog_W_ambulance";
				car[281] ="VBS2_AU_Army_Unimog_D_ambulance";
				car[282] ="VBS2_AU_Army_Unimog_W_covered";
				car[283] ="VBS2_AU_Army_Unimog_D_covered";
				car[284] ="VBS2_AU_Army_Unimog_W_repair";
				car[285] ="VBS2_AU_Army_Unimog_D_repair";
				car[286] ="VBS2_AU_Army_Unimog_W_ammo";
				car[287] ="VBS2_AU_Army_Unimog_D_ammo";
				car[288] ="VBS2_AU_Army_AslavPC_W_M2";
				car[289] ="VBS2_AU_Army_AslavPC_D_M2";
				car[290] ="VBS2_AU_Army_AslavPCRWS_W_M2";
				car[291] ="VBS2_AU_Army_AslavPCRWS_D_M2";
				car[292] ="VBS2_AU_Army_AslavPCRWS_W_Mk19";
				car[293] ="VBS2_AU_Army_AslavPCRWS_D_Mk19";
				car[294] ="VBS2_AU_Army_Aslav_W_25";
				car[295] ="VBS2_AU_Army_Aslav_D_25";
				car[296] ="VBS2_CA_Army_Lav3_W_25";
				car[297] ="VBS2_CA_Army_Lav3_W_RWS";
				car[298] ="VBS2_NZ_Army_Lav3_W_25";
				car[299] ="VBS2_NZ_Army_Lav3_D_25";
				car[300] ="VBS2_US_MC_LAV25_W_UPARMOR";
				car[301] ="VBS2_US_MC_LAV25_D_UPARMOR";
				car[302] ="vbs2_US_Army_StrykerICV_A_M2";
				car[303] ="vbs2_US_Army_StrykerICV_A_Mk19";
				car[304] ="vbs2_US_Army_StrykerRecon_A_M2";
				car[305] ="vbs2_US_Army_StrykerRecon_A_Mk19";
				car[306] ="vbs2_US_Army_StrykerMGS_A_M2";
				car[307] ="vbs2_US_Army_StrykerMC_A_M2";
				car[308] ="vbs2_US_Army_StrykerMC_A_MK19";
				car[309] ="vbs2_US_Army_StrykerCommand_A_M2";
				car[310] ="vbs2_US_Army_StrykerCommand_A_Mk19";
				car[311] ="vbs2_US_Army_StrykerFSV_A_M2";
				car[312] ="vbs2_US_Army_StrykerFSV_A_Mk19";
				car[313] ="vbs2_US_Army_StrykerESV_A_M2";
				car[314] ="vbs2_US_Army_StrykerESV_A_Mk19";
				car[315] ="vbs2_US_Army_StrykerESV_A_M2_Plow";
				car[316] ="vbs2_US_Army_StrykerESV_A_M2_Roller";
				car[317] ="vbs2_US_Army_StrykerMEV_A_M2";
				car[318] ="vbs2_US_Army_StrykerMEV_A_Mk19";
				car[319] ="VBS2_AU_Army_M978Hemtt_W";
				car[320] ="VBS2_AU_Army_M978Hemtt_D";
				car[321] ="VBS2_US_Army_M977_W";
				car[322] ="VBS2_US_Army_M977_D";
				car[323] ="VBS2_US_Army_M977_UpArmor_W";
				car[324] ="VBS2_US_Army_M977_UpArmor_D";
				car[325] ="VBS2_US_Army_M978_W";
				car[326] ="VBS2_US_Army_M978_D";
				car[327] ="VBS2_US_Army_M978_UpArmor_W";
				car[328] ="VBS2_US_Army_M978_UpArmor_D";
				car[329] ="VBS2_US_MC_M978_W";
				car[330] ="VBS2_US_MC_M978_D";
				car[331] ="VBS2_US_Army_M985_W";
				car[332] ="VBS2_US_Army_M985_D";
				car[333] ="VBS2_US_Army_M985_UpArmor_W";
				car[334] ="VBS2_US_Army_M985_UpArmor_D";
				car[335] ="VBS2_US_MC_MTVR_W";
				car[336] ="VBS2_US_MC_MTVR_D";
				car[337] ="VBS2_US_MC_MTVR_W_repair";
				car[338] ="VBS2_US_MC_MTVR_D_repair";
				car[339] ="VBS2_US_MC_MTVR_W_ammo";
				car[340] ="VBS2_US_MC_MTVR_D_ammo";
				car[341] ="VBS2_US_MC_MTVR_W_M2";
				car[342] ="VBS2_US_MC_MTVR_D_M2";
				car[343] ="VBS2_US_MC_MTVR_W_M2_repair";
				car[344] ="VBS2_US_MC_MTVR_D_M2_repair";
				car[345] ="VBS2_US_MC_MTVR_W_M2_ammo";
				car[346] ="VBS2_US_MC_MTVR_D_M2_ammo";
				car[347] ="VBS2_US_MC_MTVR_W_M240";
				car[348] ="VBS2_US_MC_MTVR_D_M240";
				car[349] ="VBS2_US_MC_MTVR_W_M240_repair";
				car[350] ="VBS2_US_MC_MTVR_D_M240_repair";
				car[351] ="VBS2_US_MC_MTVR_W_M240_ammo";
				car[352] ="VBS2_US_MC_MTVR_D_M240_ammo";
				car[353] ="VBS2_US_MC_MTVR_W_Mk19";
				car[354] ="VBS2_US_MC_MTVR_D_Mk19";
				car[355] ="VBS2_US_MC_MTVR_W_Mk19_repair";
				car[356] ="VBS2_US_MC_MTVR_D_Mk19_repair";
				car[357] ="VBS2_US_MC_MTVR_W_Mk19_ammo";
				car[358] ="VBS2_US_MC_MTVR_D_Mk19_ammo";
				car[359] ="VBS2_GB_Army_Pinzgauer_4x4_W";
				car[360] ="VBS2_GB_Army_Pinzgauer_4x4_D";
				car[361] ="VBS2_GB_Army_Pinzgauer_6x6_W";
				car[362] ="VBS2_GB_Army_Pinzgauer_6x6_D";
				car[363] ="VBS2_GB_Army_Pinzgauer_Vector_W";
				car[364] ="VBS2_GB_Army_Pinzgauer_Vector_D";
				car[365] ="VBS2_AU_Army_Landrover_D";
				car[366] ="VBS2_AU_Army_Landrover_W";
				car[367] ="VBS2_AU_Army_Landrover_D_ATammo";
				car[368] ="VBS2_AU_Army_Landrover_W_ATammo";
				car[369] ="VBS2_AU_Army_LandroverSRV_D_M2";
				car[370] ="VBS2_AU_Army_LandroverSRV_W_M2";
				car[371] ="VBS2_AU_Army_LandroverLRPV_D_M2";
				car[372] ="VBS2_AU_Army_LandroverLRPV_W_M2";
				car[373] ="VBS2_AU_Army_LandroverLRPV_D_Mk19";
				car[374] ="VBS2_AU_Army_LandroverLRPV_W_Mk19";
				car[375] ="VBS2_GB_Army_LandRover_Pulse_W_Litters";
				car[376] ="VBS2_GB_Army_LandRover_Pulse_D_Litters";
				car[377] ="VBS2_GB_Army_LandRover_Pulse_W_Seats";
				car[378] ="VBS2_GB_Army_LandRover_Pulse_D_Seats";
				car[379] ="VBS2_GB_Army_LandRover_Snatch_W";
				car[380] ="VBS2_GB_Army_LandRover_Snatch_D";
				car[381] ="VBS2_GB_Army_LandRover_WMIK_W_GPMG";
				car[382] ="VBS2_GB_Army_LandRover_WMIK_D_GPMG";
				car[383] ="VBS2_GB_Army_LandRover_WMIK_W_HMG";
				car[384] ="VBS2_GB_Army_LandRover_WMIK_D_HMG";
				car[385] ="VBS2_GB_Army_LandRover_WMIK_W_GMG";
				car[386] ="VBS2_GB_Army_LandRover_WMIK_D_GMG";
				car[387] ="VBS2_GB_Army_LandRover_Wolf_110_W_SoftTop_Bowman";
				car[388] ="VBS2_GB_Army_LandRover_Wolf_110_D_SoftTop_Bowman";
				car[389] ="VBS2_GB_Army_LandRover_Wolf_110_W_HardTop_Bowman";
				car[390] ="VBS2_GB_Army_LandRover_Wolf_110_D_HardTop_Bowman";
				car[391] ="VBS2_GB_Army_LandRover_Wolf_110_FFR_W";
				car[392] ="VBS2_GB_Army_LandRover_Wolf_110_FFR_D";
				car[393] ="VBS2_GB_Army_Jackal_MWMIK_W_HMG";
				car[394] ="VBS2_GB_Army_Jackal_MWMIK_D_HMG";
				car[395] ="VBS2_GB_Army_Jackal_MWMIK_W_GMG";
				car[396] ="VBS2_GB_Army_Jackal_MWMIK_D_GMG";
			};
		};

		class Helicopter{
		public:

			enum HELICOPTER{
				AH1W,
				UH60MG,
				UH60,
				AH6,
				AH6_RACS,
				MH6,
				MH6_RACS,
				KA50,
				Mi17_MG,
				Mi17,
				VBS2_GB_RAF_Merlin_HC3,
				VBS2_GB_RAF_Merlin_HC3_GPMG,
				VBS2_GB_RN_Merlin_HM1,
				VBS2_GB_ARMY_Lynx_AH7_GPMG,
				VBS2_GB_ARMY_Lynx_AH7_TOW,
				VBS2_GB_ARMY_Lynx_AH9,
				VBS2_GB_RN_Lynx_HMA8,
				VBS2_GB_RAF_SeaKing_HAR3,
				VBS2_GB_RN_SeaKing_HAS6CR,
				VBS2_GB_RN_SeaKing_HC4,
				VBS2_US_MC_AH1Z_B,
				VBS2_US_Army_OH58D_W_M296_M260,
				VBS2_US_Army_OH58D_W_M260_M299,
				VBS2_CA_Army_CH146_W,
				VBS2_CA_Army_CH146_W_C6,
				VBS2_CA_Army_CH146_W_M134,
				VBS2_NZ_RNZAF_UH1H_W,
				VBS2_NZ_RNZAF_UH1H_W_MAG58,
				VBS2_US_MC_UH1Y_B,
				VBS2_US_MC_UH1Y_B_FFAR_GAU21,
				VBS2_US_MC_UH1Y_B_FFAR_GAU17,
				VBS2_US_ARMY_UH1H_W,
				VBS2_GB_WAH64D_LONGBOW,
				VBS2_US_AH64D_LONGBOW,
				VBS2_US_AH64D,
				VBS2_US_AH64D_LONGBOW_16HELLFIRE,
				VBS2_GB_RAF_Puma_HC1,
				VBS2_AU_Army_ARH_W,
				VBS2_NZ_Navy_SH2G_G,
				VBS2_NZ_Navy_SH2G_G_AGM65,
				vbs2_zz_army_mil_mi24V_W,
				vbs2_zz_army_mil_mi8_w,
				VBS2_AU_Army_MRH90_W,
				VBS2_NZ_RNZAF_MRH90_B,
				VBS2_NZ_RNZAF_MRH90_B_mag58,
				VBS2_AU_Army_S70_W,
				vbs2_us_mc_ch46_b,
				vbs2_us_mc_ch46_b_gau21,
				VBS2_AU_Army_CH47D_G_M134,
				VBS2_GB_RAF_CH47_G_M134,
				VBS2_US_Army_CH47D_G_M134,
				VBS2_US_Army_CH47F_G_M134,
				VBS2_US_Army_UH60L_W,
				VBS2_US_Army_UH60Q_W,
				vbs2_us_mc_ch53e_b_gau21,
				vbs2_us_mc_ch53e_b_gau21_gau21,
				vbs2_us_mc_ch53e_b_gau17_gau21,
			};

			string helicopter[57];
			Helicopter(){
				helicopter[0] ="AH1W";
				helicopter[1] ="UH60MG";
				helicopter[2] ="UH60";
				helicopter[3] ="AH6";
				helicopter[4] ="AH6_RACS";
				helicopter[5] ="MH6";
				helicopter[6] ="MH6_RACS";
				helicopter[7] ="KA50";
				helicopter[8] ="Mi17_MG";
				helicopter[9] ="Mi17";
				helicopter[10] ="VBS2_GB_RAF_Merlin_HC3";
				helicopter[11] ="VBS2_GB_RAF_Merlin_HC3_GPMG";
				helicopter[12] ="VBS2_GB_RN_Merlin_HM1";
				helicopter[13] ="VBS2_GB_ARMY_Lynx_AH7_GPMG";
				helicopter[14] ="VBS2_GB_ARMY_Lynx_AH7_TOW";
				helicopter[15] ="VBS2_GB_ARMY_Lynx_AH9";
				helicopter[16] ="VBS2_GB_RN_Lynx_HMA8";
				helicopter[17] ="VBS2_GB_RAF_SeaKing_HAR3";
				helicopter[18] ="VBS2_GB_RN_SeaKing_HAS6CR";
				helicopter[19] ="VBS2_GB_RN_SeaKing_HC4";
				helicopter[20] ="VBS2_US_MC_AH1Z_B";
				helicopter[21] ="VBS2_US_Army_OH58D_W_M296_M260";
				helicopter[22] ="VBS2_US_Army_OH58D_W_M260_M299";
				helicopter[23] ="VBS2_CA_Army_CH146_W";
				helicopter[24] ="VBS2_CA_Army_CH146_W_C6";
				helicopter[25] ="VBS2_CA_Army_CH146_W_M134";
				helicopter[26] ="VBS2_NZ_RNZAF_UH1H_W";
				helicopter[27] ="VBS2_NZ_RNZAF_UH1H_W_MAG58";
				helicopter[28] ="VBS2_US_MC_UH1Y_B";
				helicopter[29] ="VBS2_US_MC_UH1Y_B_FFAR_GAU21";
				helicopter[30] ="VBS2_US_MC_UH1Y_B_FFAR_GAU17";
				helicopter[31] ="VBS2_US_ARMY_UH1H_W";
				helicopter[32] ="VBS2_GB_WAH64D_LONGBOW";
				helicopter[33] ="VBS2_US_AH64D_LONGBOW";
				helicopter[34] ="VBS2_US_AH64D";
				helicopter[35] ="VBS2_US_AH64D_LONGBOW_16HELLFIRE";
				helicopter[36] ="VBS2_GB_RAF_Puma_HC1";
				helicopter[37] ="VBS2_AU_Army_ARH_W";
				helicopter[38] ="VBS2_NZ_Navy_SH2G_G";
				helicopter[39] ="VBS2_NZ_Navy_SH2G_G_AGM65";
				helicopter[40] ="vbs2_zz_army_mil_mi24V_W";
				helicopter[41] ="vbs2_zz_army_mil_mi8_w";
				helicopter[42] ="VBS2_AU_Army_MRH90_W";
				helicopter[43] ="VBS2_NZ_RNZAF_MRH90_B";
				helicopter[44] ="VBS2_NZ_RNZAF_MRH90_B_mag58";
				helicopter[45] ="VBS2_AU_Army_S70_W";
				helicopter[46] ="vbs2_us_mc_ch46_b";
				helicopter[47] ="vbs2_us_mc_ch46_b_gau21";
				helicopter[48] ="VBS2_AU_Army_CH47D_G_M134";
				helicopter[49] ="VBS2_GB_RAF_CH47_G_M134";
				helicopter[50] ="VBS2_US_Army_CH47D_G_M134";
				helicopter[51] ="VBS2_US_Army_CH47F_G_M134";
				helicopter[52] ="VBS2_US_Army_UH60L_W";
				helicopter[53] ="VBS2_US_Army_UH60Q_W";
				helicopter[54] ="vbs2_us_mc_ch53e_b_gau21";
				helicopter[55] ="vbs2_us_mc_ch53e_b_gau21_gau21";
				helicopter[56] ="vbs2_us_mc_ch53e_b_gau17_gau21";
			};
		};

		class Soldier{
		public:

			enum SOLDIER{
				SoldierWB,
				SoldierWG,
				SoldierWMedic,
				SoldierWNOG,
				SoldierW,
				SoldierWAR,
				SoldierWMG,
				SoldierWAT,
				SoldierWAA,
				SoldierWSniper,
				SoldierWSaboteur,
				SoldierWSaboteurPipe,
				SoldierWSaboteurPipe2,
				SoldierWSaboteurRecon,
				SoldierWSaboteurAssault,
				SoldierWSaboteurMarksman,
				SoldierWMiner,
				SquadLeaderW,
				TeamLeaderW,
				OfficerW,
				SoldierWPilot,
				SoldierWCrew,
				BISCamelPilot,
				SoldierEB,
				SoldierEG,
				SoldierEMedic,
				SoldierENOG,
				SoldierE,
				SoldierEMG,
				SoldierEAT,
				SoldierEAA,
				SoldierEMiner,
				SquadLeaderE,
				TeamLeaderE,
				OfficerE,
				SoldierESniper,
				SoldierESaboteur,
				SoldierESaboteurPipe,
				SoldierESaboteurBizon,
				SoldierESaboteurMarksman,
				SoldierEPilot,
				BISCamelPilot2,
				SoldierECrew,
				SoldierGB,
				SoldierGMedic,
				SoldierGG,
				SoldierGNOG,
				SoldierG,
				SoldierGMG,
				SoldierGAT,
				SoldierGAA,
				OfficerG,
				SquadLeaderG,
				TeamLeaderG,
				SoldierGCrew,
				SoldierGSniper,
				SoldierGCommando,
				SoldierGMarksman,
				SoldierGPilot,
				SoldierGMiner,
				SoldierGGuard,
				Civilian,
				Civilian2,
				Civilian3,
				Civilian4,
				Civilian5,
				Civilian6,
				Civilian7,
				Civilian8,
				Civilian9,
				Civilian10,
				Civilian11,
				Civilian12,
				Civilian13,
				Civilian14,
				Civilian15,
				Civilian16,
				Civilian17,
				Civilian18,
				Civilian19,
				Civilian20,
				Civilian21,
				SoldierWCaptive,
				SoldierECaptive,
				SoldierGCaptive,
				Civil_Undead_1,
				Civil_Undead_2,
				Civil_Undead_3,
				Civil_Undead_4,
				King,
				MarianQuandt,
				MarianQuandt02,
				MarianQuandt03,
				MarianQuandt04,
				FieldReporter,
				Anchorman,
				NorthPrimeMinister,
				Riboli,
				civil_nprem2,
				D2_RCM03_Civilian1,
				D2_RCM03_Civilian2,
				vbs2_af_civ_woman_1,
				vbs2_iq_civ_woman_1,
				vbs2_af_civ_woman_2,
				vbs2_iq_civ_woman_2,
				vbs2_af_civ_woman_1_angry,
				vbs2_iq_civ_woman_1_angry,
				vbs2_af_civ_woman_2_angry,
				vbs2_iq_civ_woman_2_angry,
				VBS2_AU_Army_IFVCrew_W_F88c,
				VBS2_AU_Army_IFVCrew_D_F88c,
				VBS2_AU_RAAF_FighterPilot_G_Glock17,
				VBS2_AU_Army_HeliPilot_W_BrowningHP,
				VBS2_AU_Army_HeliPilot_D_BrowningHP,
				VBS2_AU_SOC_SASR_W_M4A5_Aim,
				VBS2_AU_SOC_SASR_D_M4A5_Aim,
				VBS2_AU_SOC_SASR_W_M4A5_Elc,
				VBS2_AU_SOC_SASR_D_M4A5_Elc,
				VBS2_AU_SOC_SASR_W_M4A5_Aim_fb,
				VBS2_AU_SOC_SASR_D_M4A5_Aim_fb,
				VBS2_AU_SOC_SASR_W_M4A5SD_Aim,
				VBS2_AU_SOC_SASR_D_M4A5SD_Aim,
				VBS2_AU_SOC_SASR_W_M4A5SD_Elc,
				VBS2_AU_SOC_SASR_D_M4A5SD_Elc,
				VBS2_AU_SOC_SASRGrenadier_W_M4A5_Aim,
				VBS2_AU_SOC_SASRGrenadier_D_M4A5_Aim,
				VBS2_AU_SOC_SASRGrenadier_W_M4A5_Elc,
				VBS2_AU_SOC_SASRGrenadier_D_M4A5_Elc,
				VBS2_AU_SOC_SASRGrenadier_W_M4A5SD_Aim,
				VBS2_AU_SOC_SASRGrenadier_D_M4A5SD_Aim,
				VBS2_AU_SOC_SASRGrenadier_W_M4A5SD_Elc,
				VBS2_AU_SOC_SASRGrenadier_D_M4A5SD_Elc,
				VBS2_AU_SOC_SASRAT_W_M4A5_LAW,
				VBS2_AU_SOC_SASRAT_D_M4A5_LAW,
				VBS2_AU_SOC_SASRAT_W_M4A5SD_LAW,
				VBS2_AU_SOC_SASRAT_D_M4A5SD_LAW,
				VBS2_AU_SOC_SASRsniper_W_aw50,
				VBS2_AU_SOC_SASRsniper_D_aw50,
				VBS2_AU_SOC_SASRsniper_W_sr98,
				VBS2_AU_SOC_SASRsniper_D_sr98,
				VBS2_AU_SOC_SASRmgunner_W_f89,
				VBS2_AU_SOC_SASRmgunner_D_f89,
				VBS2_AU_SOC_SASRmgunner_W_f89_dim,
				VBS2_AU_SOC_SASRmgunner_D_f89_dim,
				VBS2_AU_SOC_SASRmgunner_W_mag58,
				VBS2_AU_SOC_SASRmgunner_D_mag58,
				VBS2_AU_SOC_SASRmgunner_W_mag58_dim,
				VBS2_AU_SOC_SASRmgunner_D_mag58_dim,
				VBS2_AU_SOC_TAG_K_Mp5a5_Gasmask,
				VBS2_AU_SOC_TAG_K_Mp5a5,
				VBS2_AU_SOC_TAG_K_Mp5a5_Aim_Gasmask,
				VBS2_AU_SOC_TAG_K_Mp5a5_Aim,
				VBS2_AU_SOC_TAGMedic_K_Mp5a5_Aim,
				VBS2_AU_SOC_TAG_K_Mp5sd6_Gasmask,
				VBS2_AU_SOC_TAG_K_Mp5sd6,
				VBS2_AU_SOC_TAG_K_M4A5elc,
				vbs2_vrt_au_policeman,
				vbs2_vrt_au_policeman_traffic,
				vbs2_afg_child_01,
				vbs2_afg_child_02,
				vbs2_afg_child_downs_01,
				vbs2_iq_child_01,
				vbs2_iq_child_02,
				vbs2_iq_child_03,
				vbs2_iq_child_04,
				vbs2_iq_child_downs_01,
				vbs2_civ_businessman_black,
				vbs2_civ_businessman_blue,
				vbs2_civ_businessman_brown,
				vbs2_civ_businessman_grey,
				vbs2_civ_businessman_rndm,
				vbs2_civ_reporter,
				vbs2_civ_cameramann,
				vbs2_civ_woman_light,
				vbs2_civ_woman_brown,
				vbs2_civ_euro_1,
				vbs2_civ_euro_2,
				vbs2_civ_euro_3,
				vbs2_civ_euro_4,
				VBS2_GB_RAF_HeliPilot_W_L9,
				VBS2_GB_RAF_HeliPilot_D_L9,
				VBS2_GB_ARMY_HeliPilot_W_L9,
				VBS2_GB_ARMY_HeliPilot_D_L9,
				VBS2_in_soldier_ak47,
				VBS2_in_ballcapSoldier_ak47,
				vbs2_invisible_man_admin,
				vbs2_iq_civ_man_01,
				vbs2_iq_civ_man_02,
				vbs2_iq_civ_man_03,
				vbs2_iq_civ_man_04,
				vbs2_iq_civ_man_05,
				vbs2_iq_civ_man_06,
				vbs2_iq_civ_manelder_01,
				vbs2_iq_civ_manelder_02,
				vbs2_iq_civ_man_01_angry,
				vbs2_iq_civ_man_02_angry,
				vbs2_iq_civ_man_03_angry,
				vbs2_iq_civ_man_04_angry,
				vbs2_iq_civ_man_05_angry,
				vbs2_iq_civ_man_06_angry,
				vbs2_iq_civ_manelder_01_angry,
				vbs2_iq_civ_manelder_02_angry,
				vbs2_iq_civilian_bombvest_01,
				vbs2_iq_civilian_bombvest_02,
				vbs2_iq_civilian_bombvest_03,
				vbs2_iq_civilian_bombvest_04,
				vbs2_iq_civilian_bombvest_05,
				vbs2_iq_civilian_bombvest_06,
				vbs2_iq_civilian_bombvest_elder_01,
				vbs2_iq_civilian_bombvest_elder_02,
				vbs2_iq_civilian_bombvest_concealed_01,
				vbs2_iq_civ_dockworker_01,
				vbs2_iq_civ_dockworker_02,
				vbs2_iq_civ_dockworker_03,
				vbs2_iq_civ_dockworker_04,
				vbs2_iq_civ_dockworker_01_angry,
				vbs2_iq_civ_dockworker_02_angry,
				vbs2_iq_civ_dockworker_03_angry,
				vbs2_iq_civ_dockworker_04_angry,
				VBS2_IQ_Police_constable_akm,
				VBS2_IQ_Police_constable_ak74n,
				VBS2_IQ_Police_constable_aks74u,
				VBS2_IQ_Police_constable_rpk74,
				VBS2_IQ_Police_constable_m590,
				VBS2_IQ_Police_constable_m590_riot,
				VBS2_IQ_Police_constable_glock17,
				VBS2_IQ_Police_constable_makarovPM,
				VBS2_IQ_Police_constable_akm_bullhorn,
				VBS2_IQ_Police_constable_akm_mirror,
				VBS2_IQ_Police_constable_akm_paddle,
				VBS2_IQ_National_Policeman_ak47,
				VBS2_IQ_Rifleman_ak47,
				VBS2_NG_WOMAN_01,
				VBS2_NG_WOMAN_02,
				VBS2_NG_WOMAN_03,
				VBS2_NG_WOMAN_04,
				VBS2_NG_WOMAN_05,
				VBS2_NG_MAN_01,
				VBS2_NG_MAN_02,
				VBS2_NG_MAN_03,
				VBS2_NG_MAN_04,
				VBS2_NG_MAN_05,
				VBS2_NG_MAN_06,
				VBS2_NG_MAN_07,
				VBS2_NG_Militia,
				vbs2_ng_soldier_ak47,
				vbs2_ng_soldier_rpg,
				vbs2_ng_leader_ak47,
				vbs2_ng_militia_ak47,
				vbs2_ng_militia_rpg,
				vbs2_ng_militia_leader,
				VBS2_NZ_Army_IFVCrew_W_SteyrIWc,
				VBS2_NZ_Army_IFVCrew_D_SteyrIWc,
				VBS2_NZ_Army_HeliPilot_W_SigP226,
				VBS2_NZ_Army_HeliPilot_D_SigP226,
				VBS2_US_ARMY_HeliPilot_W_BerettaM9,
				VBS2_US_MC_EOD_W,
				VBS2_US_MC_EOD_D,
				VBS2_US_MC_FighterPilot_G_berettaM9,
				VBS2_US_MC_FighterPilot_D_berettaM9,
				VBS2_US_MC_HeliPilot_W_BerettaM9,
				VBS2_US_MC_HeliPilot_D_BerettaM9,
				vbs2_animal_camel_lightbrown_none,
				vbs2_animal_cow_01_none,
				vbs2_animal_dog_mongrel_01,
				vbs2_animal_germanshepherd_bomb_detection,
				vbs2_animal_donkey_01_none,
				VBS2_animal_goat_01_none,
				VBS2_animal_goat_02_none,
				VBS2_animal_goat_03_none,
				VBS2_animal_goat_04_none,
				VBS2_animal_goat_05_none,
				VBS2_animal_goat_06_none,
				vbs2_af_taliban_akm,
				vbs2_af_taliban_ak74,
				vbs2_af_taliban_aks74u,
				vbs2_af_taliban_ak74gla,
				vbs2_af_taliban_ak74pso,
				vbs2_af_taliban_svd,
				vbs2_af_taliban_rpk74,
				vbs2_af_taliban_pkm,
				vbs2_af_taliban_rpgAT,
				vbs2_af_taliban_rpgAP,
				vbs2_af_taliban_igla,
				vbs2_af_talibanLeader_akm,
				vbs2_af_talibanLeader_aks74pso,
				vbs2_af_talibanLeader_ak74gla,
				vbs2_af_talibanLeader_aks74uSD,
				vbs2_af_civ_man_1,
				vbs2_af_civ_man_2,
				vbs2_af_civ_man_3,
				vbs2_af_civ_man_4,
				vbs2_af_civ_man_5,
				vbs2_af_civ_man_1_bombvest,
				vbs2_af_civ_man_2_bombvest,
				vbs2_af_civ_man_3_bombvest,
				vbs2_af_civ_man_4_bombvest,
				vbs2_af_civilian_bombvest_facewrap,
				vbs2_af_civ_man_1_angry,
				vbs2_af_civ_man_2_angry,
				vbs2_af_civ_man_3_angry,
				vbs2_af_civ_man_4_angry,
				vbs2_af_civ_man_5_angry,
				VBS2_AU_Army_Rifleman_W_F88A1,
				VBS2_AU_Army_Rifleman_D_F88A1,
				VBS2_AU_Army_Rifleman_W_F88A2_Aim,
				VBS2_AU_Army_Rifleman_D_F88A2_Aim,
				VBS2_AU_Army_Rifleman_W_F88A2_Aim_fb,
				VBS2_AU_Army_Rifleman_D_F88A2_Aim_fb,
				VBS2_AU_Army_Rifleman_W_F88A2_Elc,
				VBS2_AU_Army_Rifleman_D_F88A2_Elc,
				VBS2_AU_Army_Grenadier_W_F88A1,
				VBS2_AU_Army_Grenadier_D_F88A1,
				VBS2_AU_Army_Grenadier_W_F88A2_Aim,
				VBS2_AU_Army_Grenadier_D_F88A2_Aim,
				VBS2_AU_Army_Grenadier_W_F88A2_Elc,
				VBS2_AU_Army_Grenadier_D_F88A2_Elc,
				VBS2_AU_Army_Mgunner_W_F89,
				VBS2_AU_Army_Mgunner_D_F89,
				VBS2_AU_Army_Mgunner_W_F89_dim,
				VBS2_AU_Army_Mgunner_D_F89_dim,
				VBS2_AU_Army_Mgunner_W_mag58,
				VBS2_AU_Army_Mgunner_D_mag58,
				VBS2_AU_Army_Mgunner_W_mag58_dim,
				VBS2_AU_Army_Mgunner_D_mag58_dim,
				VBS2_AU_Army_ATsoldier_W_LAW,
				VBS2_AU_Army_ATsoldier_D_LAW,
				VBS2_AU_Army_ATsoldier_W_CarlGustav,
				VBS2_AU_Army_ATsoldier_D_CarlGustav,
				VBS2_AU_Army_ATsoldier_W_CarlGustav2,
				VBS2_AU_Army_ATsoldier_D_CarlGustav2,
				VBS2_AU_Army_ATsoldier_W_Javelin,
				VBS2_AU_Army_ATsoldier_D_Javelin,
				VBS2_AU_Army_sniper_W_sr98,
				VBS2_AU_Army_sniper_D_sr98,
				VBS2_AU_Army_sniper_W_aw50,
				VBS2_AU_Army_sniper_D_aw50,
				VBS2_AU_Army_Medic_W_F88a1,
				VBS2_AU_Army_Medic_D_F88a1,
				VBS2_AU_Army_Engineer_W_F88A1,
				VBS2_AU_Army_Engineer_D_F88A1,
				VBS2_AU_Army_Engineer_W_F88A1_mines,
				VBS2_AU_Army_Engineer_D_F88A1_mines,
				VBS2_AU_Army_Officer_W_F88A2_Elc,
				VBS2_AU_Army_Officer_D_F88A2_Elc,
				VBS2_AU_Army_Officer_W_F88A2_Elc_night,
				VBS2_AU_Army_Officer_D_F88A2_Elc_night,
				VBS2_AU_Army_Officer_W_F88A2_Elc_mark,
				VBS2_AU_Army_Officer_D_F88A2_Elc_mark,
				vbs2_au_army_rifleman_w_F88A1_bullhorn,
				vbs2_au_army_rifleman_D_F88A1_bullhorn,
				vbs2_au_army_rifleman_w_F88A1_mirror,
				vbs2_au_army_rifleman_D_F88A1_mirror,
				vbs2_au_army_rifleman_w_F88A1_paddle,
				vbs2_au_army_rifleman_D_F88A1_paddle,
				VBS2_GB_Army_Soldier_W_L85A2_Kestrel_Helmet_None,
				VBS2_GB_Army_Soldier_W_L85A2_Kestrel_Helmet_Radio,
				VBS2_GB_Army_Soldier_W_L85A2_Osprey_Helmet_None,
				VBS2_GB_Army_Soldier_W_L85A2_Osprey_Helmet_Radio,
				VBS2_GB_Army_Soldier_W_L85A2_Osprey_Helmet_CBRN,
				VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Helmet_None,
				VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Helmet_Radio,
				VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Helmet_CBRN,
				VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Hat_None,
				VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Hat_Radio,
				VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Beret_None,
				VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Beret_Radio,
				VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Helmet_None,
				VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Helmet_Radio,
				VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Helmet_CBRN,
				VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Hat_None,
				VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Hat_Radio,
				VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Beret_None,
				VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Beret_Radio,
				VBS2_GB_Army_Medic_W_L85A2_Kestrel_Helmet_None,
				VBS2_GB_Army_Medic_W_L85A2_Osprey_Helmet_None,
				VBS2_GB_Army_Medic_W_L85A2_ChestRig_Hat_None,
				VBS2_GB_Army_Medic_W_L85A2_BeltOrder_Helmet_None,
				VBS2_GB_Army_Soldier_D_L85A2_Kestrel_Helmet_None,
				VBS2_GB_Army_Soldier_D_L85A2_Kestrel_Helmet_Radio,
				VBS2_GB_Army_Soldier_D_L85A2_Osprey_Helmet_None,
				VBS2_GB_Army_Soldier_D_L85A2_Osprey_Helmet_Radio,
				VBS2_GB_Army_Soldier_D_L85A2_Osprey_Helmet_CBRN,
				VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Helmet_None,
				VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Helmet_Radio,
				VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Helmet_CBRN,
				VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Hat_None,
				VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Hat_Radio,
				VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Beret_None,
				VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Beret_Radio,
				VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Helmet_None,
				VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Helmet_Radio,
				VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Helmet_CBRN,
				VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Hat_None,
				VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Hat_Radio,
				VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Beret_None,
				VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Beret_Radio,
				VBS2_GB_Army_Medic_D_L85A2_Kestrel_Helmet_None,
				VBS2_GB_Army_Medic_D_L85A2_Osprey_Helmet_None,
				VBS2_GB_Army_Medic_D_L85A2_ChestRig_Helmet_None,
				VBS2_GB_Army_Medic_D_L85A2_BeltOrder_Helmet_None,
				vbs2_iq_insurg_01_akm,
				vbs2_iq_insurg_02_akm,
				vbs2_iq_insurg_03_akm,
				vbs2_iq_insurg_04_akm,
				vbs2_iq_insurg_05_akm,
				vbs2_iq_insurg_01_ak74,
				vbs2_iq_insurg_02_ak74,
				vbs2_iq_insurg_03_ak74,
				vbs2_iq_insurg_04_ak74,
				vbs2_iq_insurg_05_ak74,
				vbs2_iq_insurg_01_ak74gla,
				vbs2_iq_insurg_02_ak74gla,
				vbs2_iq_insurg_03_ak74gla,
				vbs2_iq_insurg_04_ak74gla,
				vbs2_iq_insurg_05_ak74gla,
				vbs2_iq_insurg_01_aks74u,
				vbs2_iq_insurg_02_aks74u,
				vbs2_iq_insurg_03_aks74u,
				vbs2_iq_insurg_04_aks74u,
				vbs2_iq_insurg_05_aks74u,
				vbs2_iq_insurg_01_pkm,
				vbs2_iq_insurg_02_pkm,
				vbs2_iq_insurg_03_pkm,
				vbs2_iq_insurg_04_pkm,
				vbs2_iq_insurg_05_pkm,
				vbs2_iq_insurg_01_rpk74,
				vbs2_iq_insurg_02_rpk74,
				vbs2_iq_insurg_03_rpk74,
				vbs2_iq_insurg_04_rpk74,
				vbs2_iq_insurg_05_rpk74,
				vbs2_iq_insurg_01_svd,
				vbs2_iq_insurg_02_svd,
				vbs2_iq_insurg_03_svd,
				vbs2_iq_insurg_04_svd,
				vbs2_iq_insurg_05_svd,
				vbs2_iq_insurg_01_rpg,
				vbs2_iq_insurg_02_rpg,
				vbs2_iq_insurg_03_rpg,
				vbs2_iq_insurg_04_rpg,
				vbs2_iq_insurg_05_rpg,
				vbs2_iq_insurg_01_rpgmodern,
				vbs2_iq_insurg_02_rpgmodern,
				vbs2_iq_insurg_03_rpgmodern,
				vbs2_iq_insurg_04_rpgmodern,
				vbs2_iq_insurg_05_rpgmodern,
				vbs2_iq_insurg_01_strela,
				vbs2_iq_insurg_02_strela,
				vbs2_iq_insurg_03_strela,
				vbs2_iq_insurg_04_strela,
				vbs2_iq_insurg_05_strela,
				vbs2_iq_insurgLeader_01_akm,
				vbs2_iq_insurgLeader_02_akm,
				vbs2_iq_insurgLeader_03_akm,
				vbs2_iq_insurgLeader_04_akm,
				vbs2_iq_insurgLeader_05_akm,
				vbs2_iq_insurgLeader_01_ak74n,
				vbs2_iq_insurgLeader_02_ak74n,
				vbs2_iq_insurgLeader_03_ak74n,
				vbs2_iq_insurgLeader_04_ak74n,
				vbs2_iq_insurgLeader_05_ak74n,
				vbs2_iq_insurgLeader_01_aks74PSO,
				vbs2_iq_insurgLeader_02_aks74PSO,
				vbs2_iq_insurgLeader_03_aks74PSO,
				vbs2_iq_insurgLeader_04_aks74PSO,
				vbs2_iq_insurgLeader_05_aks74PSO,
				vbs2_iq_insurgLeader_01_aks74uSD,
				vbs2_iq_insurgLeader_02_aks74uSD,
				vbs2_iq_insurgLeader_03_aks74uSD,
				vbs2_iq_insurgLeader_04_aks74uSD,
				vbs2_iq_insurgLeader_05_aks74uSD,
				vbs2_iq_militia_01_akm,
				vbs2_iq_militia_02_akm,
				vbs2_iq_militia_03_akm,
				vbs2_iq_militia_04_akm,
				vbs2_iq_militia_05_akm,
				vbs2_iq_militia_06_akm,
				vbs2_iq_militiaelder_01_akm,
				vbs2_iq_militiaelder_02_akm,
				vbs2_iq_militia_01_pkm,
				vbs2_iq_militia_02_pkm,
				vbs2_iq_militia_03_pkm,
				vbs2_iq_militia_04_pkm,
				vbs2_iq_militia_05_pkm,
				vbs2_iq_militia_06_pkm,
				vbs2_iq_militiaelder_01_pkm,
				vbs2_iq_militiaelder_02_pkm,
				vbs2_iq_militia_01_rpg,
				vbs2_iq_militia_02_rpg,
				vbs2_iq_militia_03_rpg,
				vbs2_iq_militia_04_rpg,
				vbs2_iq_militia_05_rpg,
				vbs2_iq_militia_06_rpg,
				vbs2_iq_militiaelder_01_rpg,
				vbs2_iq_militiaelder_02_rpg,
				vbs2_iq_militia_01_makarov,
				vbs2_iq_militia_02_makarov,
				vbs2_iq_militia_03_makarov,
				vbs2_iq_militia_04_makarov,
				vbs2_iq_militia_05_makarov,
				vbs2_iq_militia_06_makarov,
				vbs2_iq_militiaelder_01_makarov,
				vbs2_iq_militiaelder_02_makarov,
				vbs2_iq_militia_01_aks74u,
				vbs2_iq_militia_02_aks74u,
				vbs2_iq_militia_03_aks74u,
				vbs2_iq_militia_04_aks74u,
				vbs2_iq_militia_05_aks74u,
				vbs2_iq_militia_06_aks74u,
				vbs2_iq_militiaelder_01_aks74u,
				vbs2_iq_militiaelder_02_aks74u,
				VBS2_NZ_Army_Rifleman_W_SteyrIW,
				VBS2_NZ_Army_Rifleman_D_SteyrIW,
				VBS2_NZ_Army_Rifleman_W_SteyrIW_ballcap,
				VBS2_NZ_Army_Rifleman_D_SteyrIW_ballcap,
				VBS2_NZ_Army_Rifleman_W_SteyrIWc_fb_ballcap,
				VBS2_NZ_Army_Rifleman_D_SteyrIWc_fb_ballcap,
				VBS2_NZ_Army_Grenadier_W_SteyrIW,
				VBS2_NZ_Army_Grenadier_D_SteyrIW,
				VBS2_NZ_Army_Mgunner_W_C9,
				VBS2_NZ_Army_Mgunner_W_C9_ballcap,
				VBS2_NZ_Army_Mgunner_D_C9_ballcap,
				VBS2_NZ_Army_Mgunner_D_C9,
				VBS2_NZ_Army_Mgunner_W_mag58,
				VBS2_NZ_Army_Mgunner_D_mag58,
				VBS2_NZ_Army_ATsoldier_W_LAW,
				VBS2_NZ_Army_ATsoldier_D_LAW,
				VBS2_NZ_Army_ATsoldier_W_CarlGustav,
				VBS2_NZ_Army_ATsoldier_D_CarlGustav,
				VBS2_NZ_Army_ATsoldier_W_CarlGustav2,
				VBS2_NZ_Army_ATsoldier_D_CarlGustav2,
				VBS2_NZ_Army_ATsoldier_W_Javelin,
				VBS2_NZ_Army_ATsoldier_D_Javelin,
				VBS2_NZ_Army_sniper_W_L96A1,
				VBS2_NZ_Army_sniper_D_L96A1,
				VBS2_NZ_Army_sniper_W_m82a3,
				VBS2_NZ_Army_sniper_D_m82a3,
				VBS2_NZ_Army_Medic_W_SteyrIW,
				VBS2_NZ_Army_Medic_D_SteyrIW,
				VBS2_NZ_Army_Medic_W_SteyrIW_ballcap,
				VBS2_NZ_Army_Medic_D_SteyrIW_ballcap,
				VBS2_NZ_Army_Officer_W_SteyrIW,
				VBS2_NZ_Army_Officer_W_SteyrIW_ballcap,
				VBS2_NZ_Army_Officer_D_SteyrIW_ballcap,
				VBS2_NZ_Army_Officer_D_SteyrIW,
				VBS2_US_ARMY_IFVCrew_W_M4,
				vbs2_us_army_rifleman_w_m4,
				VBS2_US_ARMY_Rifleman_W_M4A1,
				VBS2_US_ARMY_RiflemanF_W_M4,
				VBS2_US_ARMY_Grenadier_W_M4_acog,
				VBS2_US_ARMY_MGunner_W_M249,
				VBS2_US_ARMY_MGunner_W_M240,
				VBS2_US_ARMY_Sniper_W_M107,
				VBS2_us_army_ATsoldier_W_Javelin,
				VBS2_us_army_ATsoldier_W_AT4heat,
				VBS2_us_Army_Engineer_W_M4,
				VBS2_us_Army_Engineer_W_M4_Mines,
				VBS2_US_ARMY_Medic_W_M4,
				VBS2_US_ARMY_Officer_W_M4,
				VBS2_US_PMC_Rifleman_W_M4A1,
				VBS2_US_ARMY_Interpreter_W,
				VBS2_US_MC_IFVCrew_W_M4A1,
				VBS2_US_MC_IFVCrew_D_M4A1,
				vbs2_us_mc_rifleman_w_m16a4,
				vbs2_us_mc_rifleman_D_m16a4,
				vbs2_us_mc_rifleman_w_m16a4_nbc,
				vbs2_us_mc_rifleman_w_m16a4_nbc_mopp1,
				vbs2_us_mc_rifleman_w_m16a4_nbc_mopp2,
				vbs2_us_mc_rifleman_w_m16a4_nbc_mopp3,
				vbs2_us_mc_rifleman_w_m16a4_nbc_mopp4,
				vbs2_us_mc_rifleman_w_m16a4_nbc_moppAlpha,
				vbs2_us_mc_rifleman_d_m16a4_nbc,
				vbs2_us_mc_rifleman_d_m16a4_nbc_mopp1,
				vbs2_us_mc_rifleman_d_m16a4_nbc_mopp2,
				vbs2_us_mc_rifleman_d_m16a4_nbc_mopp3,
				vbs2_us_mc_rifleman_d_m16a4_nbc_mopp4,
				vbs2_us_mc_rifleman_d_m16a4_nbc_moppAlpha,
				vbs2_us_mc_rifleman_w_m16a4_acog,
				vbs2_us_mc_rifleman_D_m16a4_acog,
				vbs2_us_mc_rifleman_w_m16a2,
				vbs2_us_mc_rifleman_D_m16a2,
				vbs2_us_mc_rifleman_w_m4a1,
				vbs2_us_mc_rifleman_D_m4a1,
				vbs2_us_mc_rifleman_w_m4a1_aim,
				vbs2_us_mc_rifleman_D_m4a1_aim,
				vbs2_us_mc_rifleman_w_m4a1_aim_fb,
				vbs2_us_mc_rifleman_D_m4a1_aim_fb,
				vbs2_us_mc_grenadier_w_m16a4,
				vbs2_us_mc_grenadier_D_m16a4,
				vbs2_us_mc_grenadier_w_m16a4_acog,
				vbs2_us_mc_grenadier_D_m16a4_acog,
				vbs2_us_mc_grenadier_w_m16a2,
				vbs2_us_mc_grenadier_D_m16a2,
				VBS2_us_mc_ATsoldier_W_SMAW,
				VBS2_us_mc_ATsoldier_D_SMAW,
				VBS2_us_mc_ATsoldier_W_Javelin,
				VBS2_us_mc_ATsoldier_D_Javelin,
				VBS2_us_mc_ATsoldier_W_AT4heat,
				VBS2_us_mc_ATsoldier_D_AT4heat,
				VBS2_us_mc_ATsoldier_W_AT4hedp,
				VBS2_us_mc_ATsoldier_D_AT4hedp,
				VBS2_us_mc_ATsoldier_W_AT4hp,
				VBS2_us_mc_ATsoldier_D_AT4hp,
				VBS2_us_mc_AAsoldier_W_Stinger,
				VBS2_us_mc_AAsoldier_D_Stinger,
				vbs2_us_mc_mgunner_w_m249,
				vbs2_us_mc_mgunner_D_m249,
				vbs2_us_mc_mgunner_w_m249_dim,
				vbs2_us_mc_mgunner_D_m249_dim,
				vbs2_us_mc_mgunner_w_m240g,
				vbs2_us_mc_mgunner_D_m240g,
				vbs2_us_mc_mgunner_w_m240g_dim,
				vbs2_us_mc_mgunner_D_m240g_dim,
				VBS2_us_mc_sniper_W_m40a3,
				VBS2_us_mc_sniper_D_m40a3,
				VBS2_us_mc_sniper_W_m82a3,
				VBS2_us_mc_sniper_D_m82a3,
				VBS2_us_mc_dmarksman_W_m14dmr,
				VBS2_us_mc_dmarksman_D_m14dmr,
				VBS2_us_mc_rifleman_W_m590,
				VBS2_us_mc_rifleman_D_m590,
				VBS2_us_mc_rifleman_W_m590_riot,
				VBS2_us_mc_rifleman_D_m590_riot,
				VBS2_us_mc_rifleman_W_m1014,
				VBS2_us_mc_rifleman_D_m1014,
				vbs2_us_mc_medic_w_m16a2,
				vbs2_us_mc_medic_D_m16a2,
				VBS2_us_mc_Engineer_W_m16a4,
				VBS2_us_mc_Engineer_D_m16a4,
				VBS2_us_mc_Engineer_W_m16a4_mines,
				VBS2_us_mc_Engineer_D_m16a4_mines,
				vbs2_us_mc_officer_w_m16a4_acog,
				vbs2_us_mc_officer_D_m16a4_acog,
				vbs2_us_mc_breachSpecialist_w_m1014,
				vbs2_us_mc_breachSpecialist_D_m1014,
				vbs2_us_mc_officer_w_m16a4_acog_night,
				vbs2_us_mc_officer_D_m16a4_acog_night,
				vbs2_us_mc_officer_w_m16a4_acog_mark,
				vbs2_us_mc_officer_D_m16a4_acog_mark,
				vbs2_us_mc_rifleman_w_m16a4_bullhorn,
				vbs2_us_mc_rifleman_D_m16a4_bullhorn,
				vbs2_us_mc_rifleman_w_m16a4_mirror,
				vbs2_us_mc_rifleman_D_m16a4_mirror,
				vbs2_us_mc_rifleman_w_m16a4_paddle,
				vbs2_us_mc_rifleman_D_m16a4_paddle,
				VBS2_us_soc_rifleman_A_m4,
				VBS2_us_soc_rifleman_A_soflam,
				VBS2_us_soc_rifleman2_A_m4,
				VBS2_us_soc_rifleman2_A_soflam,
				vbs2_wp_soldier_ak74,
				vbs2_wp_soldier_rpg,
				vbs2_wp_machinegunner_pk,
				vbs2_wp_grenadier_ak74,
				vbs2_wp_leader_ak47,
				vbs2_wp_AFVcrew,
				vbs2_wp_pilot_heli,
				VBS2_GB_Army_AFVCrew_W_L22A2_None_CrewHelmet_None,
				VBS2_GB_Army_AFVCrew_W_L22A2_ChestRig_CrewHelmet_None,
				VBS2_GB_Army_AFVCrew_W_L22A2_None_Beret_Radio,
				VBS2_GB_Army_AFVCrew_W_L22A2_ChestRig_Beret_Radio,
				VBS2_GB_Army_AFVCrew_D_L22A2_None_CrewHelmet_None,
				VBS2_GB_Army_AFVCrew_D_L22A2_ChestRig_CrewHelmet_None,
				VBS2_GB_Army_AFVCrew_D_L22A2_None_Beret_Radio,
				VBS2_GB_Army_AFVCrew_D_L22A2_ChestRig_Beret_Radio,
			};

			string soldier[664];
			Soldier(){
				soldier[0] ="SoldierWB";
				soldier[1] ="SoldierWG";
				soldier[2] ="SoldierWMedic";
				soldier[3] ="SoldierWNOG";
				soldier[4] ="SoldierW";
				soldier[5] ="SoldierWAR";
				soldier[6] ="SoldierWMG";
				soldier[7] ="SoldierWAT";
				soldier[8] ="SoldierWAA";
				soldier[9] ="SoldierWSniper";
				soldier[10] ="SoldierWSaboteur";
				soldier[11] ="SoldierWSaboteurPipe";
				soldier[12] ="SoldierWSaboteurPipe2";
				soldier[13] ="SoldierWSaboteurRecon";
				soldier[14] ="SoldierWSaboteurAssault";
				soldier[15] ="SoldierWSaboteurMarksman";
				soldier[16] ="SoldierWMiner";
				soldier[17] ="SquadLeaderW";
				soldier[18] ="TeamLeaderW";
				soldier[19] ="OfficerW";
				soldier[20] ="SoldierWPilot";
				soldier[21] ="SoldierWCrew";
				soldier[22] ="BISCamelPilot";
				soldier[23] ="SoldierEB";
				soldier[24] ="SoldierEG";
				soldier[25] ="SoldierEMedic";
				soldier[26] ="SoldierENOG";
				soldier[27] ="SoldierE";
				soldier[28] ="SoldierEMG";
				soldier[29] ="SoldierEAT";
				soldier[30] ="SoldierEAA";
				soldier[31] ="SoldierEMiner";
				soldier[32] ="SquadLeaderE";
				soldier[33] ="TeamLeaderE";
				soldier[34] ="OfficerE";
				soldier[35] ="SoldierESniper";
				soldier[36] ="SoldierESaboteur";
				soldier[37] ="SoldierESaboteurPipe";
				soldier[38] ="SoldierESaboteurBizon";
				soldier[39] ="SoldierESaboteurMarksman";
				soldier[40] ="SoldierEPilot";
				soldier[41] ="BISCamelPilot2";
				soldier[42] ="SoldierECrew";
				soldier[43] ="SoldierGB";
				soldier[44] ="SoldierGMedic";
				soldier[45] ="SoldierGG";
				soldier[46] ="SoldierGNOG";
				soldier[47] ="SoldierG";
				soldier[48] ="SoldierGMG";
				soldier[49] ="SoldierGAT";
				soldier[50] ="SoldierGAA";
				soldier[51] ="OfficerG";
				soldier[52] ="SquadLeaderG";
				soldier[53] ="TeamLeaderG";
				soldier[54] ="SoldierGCrew";
				soldier[55] ="SoldierGSniper";
				soldier[56] ="SoldierGCommando";
				soldier[57] ="SoldierGMarksman";
				soldier[58] ="SoldierGPilot";
				soldier[59] ="SoldierGMiner";
				soldier[60] ="SoldierGGuard";
				soldier[61] ="Civilian";
				soldier[62] ="Civilian2";
				soldier[63] ="Civilian3";
				soldier[64] ="Civilian4";
				soldier[65] ="Civilian5";
				soldier[66] ="Civilian6";
				soldier[67] ="Civilian7";
				soldier[68] ="Civilian8";
				soldier[69] ="Civilian9";
				soldier[70] ="Civilian10";
				soldier[71] ="Civilian11";
				soldier[72] ="Civilian12";
				soldier[73] ="Civilian13";
				soldier[74] ="Civilian14";
				soldier[75] ="Civilian15";
				soldier[76] ="Civilian16";
				soldier[77] ="Civilian17";
				soldier[78] ="Civilian18";
				soldier[79] ="Civilian19";
				soldier[80] ="Civilian20";
				soldier[81] ="Civilian21";
				soldier[82] ="SoldierWCaptive";
				soldier[83] ="SoldierECaptive";
				soldier[84] ="SoldierGCaptive";
				soldier[85] ="Civil_Undead_1";
				soldier[86] ="Civil_Undead_2";
				soldier[87] ="Civil_Undead_3";
				soldier[88] ="Civil_Undead_4";
				soldier[89] ="King";
				soldier[90] ="MarianQuandt";
				soldier[91] ="MarianQuandt02";
				soldier[92] ="MarianQuandt03";
				soldier[93] ="MarianQuandt04";
				soldier[94] ="FieldReporter";
				soldier[95] ="Anchorman";
				soldier[96] ="NorthPrimeMinister";
				soldier[97] ="Riboli";
				soldier[98] ="civil_nprem2";
				soldier[99] ="D2_RCM03_Civilian1";
				soldier[100] ="D2_RCM03_Civilian2";
				soldier[101] ="vbs2_af_civ_woman_1";
				soldier[102] ="vbs2_iq_civ_woman_1";
				soldier[103] ="vbs2_af_civ_woman_2";
				soldier[104] ="vbs2_iq_civ_woman_2";
				soldier[105] ="vbs2_af_civ_woman_1_angry";
				soldier[106] ="vbs2_iq_civ_woman_1_angry";
				soldier[107] ="vbs2_af_civ_woman_2_angry";
				soldier[108] ="vbs2_iq_civ_woman_2_angry";
				soldier[109] ="VBS2_AU_Army_IFVCrew_W_F88c";
				soldier[110] ="VBS2_AU_Army_IFVCrew_D_F88c";
				soldier[111] ="VBS2_AU_RAAF_FighterPilot_G_Glock17";
				soldier[112] ="VBS2_AU_Army_HeliPilot_W_BrowningHP";
				soldier[113] ="VBS2_AU_Army_HeliPilot_D_BrowningHP";
				soldier[114] ="VBS2_AU_SOC_SASR_W_M4A5_Aim";
				soldier[115] ="VBS2_AU_SOC_SASR_D_M4A5_Aim";
				soldier[116] ="VBS2_AU_SOC_SASR_W_M4A5_Elc";
				soldier[117] ="VBS2_AU_SOC_SASR_D_M4A5_Elc";
				soldier[118] ="VBS2_AU_SOC_SASR_W_M4A5_Aim_fb";
				soldier[119] ="VBS2_AU_SOC_SASR_D_M4A5_Aim_fb";
				soldier[120] ="VBS2_AU_SOC_SASR_W_M4A5SD_Aim";
				soldier[121] ="VBS2_AU_SOC_SASR_D_M4A5SD_Aim";
				soldier[122] ="VBS2_AU_SOC_SASR_W_M4A5SD_Elc";
				soldier[123] ="VBS2_AU_SOC_SASR_D_M4A5SD_Elc";
				soldier[124] ="VBS2_AU_SOC_SASRGrenadier_W_M4A5_Aim";
				soldier[125] ="VBS2_AU_SOC_SASRGrenadier_D_M4A5_Aim";
				soldier[126] ="VBS2_AU_SOC_SASRGrenadier_W_M4A5_Elc";
				soldier[127] ="VBS2_AU_SOC_SASRGrenadier_D_M4A5_Elc";
				soldier[128] ="VBS2_AU_SOC_SASRGrenadier_W_M4A5SD_Aim";
				soldier[129] ="VBS2_AU_SOC_SASRGrenadier_D_M4A5SD_Aim";
				soldier[130] ="VBS2_AU_SOC_SASRGrenadier_W_M4A5SD_Elc";
				soldier[131] ="VBS2_AU_SOC_SASRGrenadier_D_M4A5SD_Elc";
				soldier[132] ="VBS2_AU_SOC_SASRAT_W_M4A5_LAW";
				soldier[133] ="VBS2_AU_SOC_SASRAT_D_M4A5_LAW";
				soldier[134] ="VBS2_AU_SOC_SASRAT_W_M4A5SD_LAW";
				soldier[135] ="VBS2_AU_SOC_SASRAT_D_M4A5SD_LAW";
				soldier[136] ="VBS2_AU_SOC_SASRsniper_W_aw50";
				soldier[137] ="VBS2_AU_SOC_SASRsniper_D_aw50";
				soldier[138] ="VBS2_AU_SOC_SASRsniper_W_sr98";
				soldier[139] ="VBS2_AU_SOC_SASRsniper_D_sr98";
				soldier[140] ="VBS2_AU_SOC_SASRmgunner_W_f89";
				soldier[141] ="VBS2_AU_SOC_SASRmgunner_D_f89";
				soldier[142] ="VBS2_AU_SOC_SASRmgunner_W_f89_dim";
				soldier[143] ="VBS2_AU_SOC_SASRmgunner_D_f89_dim";
				soldier[144] ="VBS2_AU_SOC_SASRmgunner_W_mag58";
				soldier[145] ="VBS2_AU_SOC_SASRmgunner_D_mag58";
				soldier[146] ="VBS2_AU_SOC_SASRmgunner_W_mag58_dim";
				soldier[147] ="VBS2_AU_SOC_SASRmgunner_D_mag58_dim";
				soldier[148] ="VBS2_AU_SOC_TAG_K_Mp5a5_Gasmask";
				soldier[149] ="VBS2_AU_SOC_TAG_K_Mp5a5";
				soldier[150] ="VBS2_AU_SOC_TAG_K_Mp5a5_Aim_Gasmask";
				soldier[151] ="VBS2_AU_SOC_TAG_K_Mp5a5_Aim";
				soldier[152] ="VBS2_AU_SOC_TAGMedic_K_Mp5a5_Aim";
				soldier[153] ="VBS2_AU_SOC_TAG_K_Mp5sd6_Gasmask";
				soldier[154] ="VBS2_AU_SOC_TAG_K_Mp5sd6";
				soldier[155] ="VBS2_AU_SOC_TAG_K_M4A5elc";
				soldier[156] ="vbs2_vrt_au_policeman";
				soldier[157] ="vbs2_vrt_au_policeman_traffic";
				soldier[158] ="vbs2_afg_child_01";
				soldier[159] ="vbs2_afg_child_02";
				soldier[160] ="vbs2_afg_child_downs_01";
				soldier[161] ="vbs2_iq_child_01";
				soldier[162] ="vbs2_iq_child_02";
				soldier[163] ="vbs2_iq_child_03";
				soldier[164] ="vbs2_iq_child_04";
				soldier[165] ="vbs2_iq_child_downs_01";
				soldier[166] ="vbs2_civ_businessman_black";
				soldier[167] ="vbs2_civ_businessman_blue";
				soldier[168] ="vbs2_civ_businessman_brown";
				soldier[169] ="vbs2_civ_businessman_grey";
				soldier[170] ="vbs2_civ_businessman_rndm";
				soldier[171] ="vbs2_civ_reporter";
				soldier[172] ="vbs2_civ_cameramann";
				soldier[173] ="vbs2_civ_woman_light";
				soldier[174] ="vbs2_civ_woman_brown";
				soldier[175] ="vbs2_civ_euro_1";
				soldier[176] ="vbs2_civ_euro_2";
				soldier[177] ="vbs2_civ_euro_3";
				soldier[178] ="vbs2_civ_euro_4";
				soldier[179] ="VBS2_GB_RAF_HeliPilot_W_L9";
				soldier[180] ="VBS2_GB_RAF_HeliPilot_D_L9";
				soldier[181] ="VBS2_GB_ARMY_HeliPilot_W_L9";
				soldier[182] ="VBS2_GB_ARMY_HeliPilot_D_L9";
				soldier[183] ="VBS2_in_soldier_ak47";
				soldier[184] ="VBS2_in_ballcapSoldier_ak47";
				soldier[185] ="vbs2_invisible_man_admin";
				soldier[186] ="vbs2_iq_civ_man_01";
				soldier[187] ="vbs2_iq_civ_man_02";
				soldier[188] ="vbs2_iq_civ_man_03";
				soldier[189] ="vbs2_iq_civ_man_04";
				soldier[190] ="vbs2_iq_civ_man_05";
				soldier[191] ="vbs2_iq_civ_man_06";
				soldier[192] ="vbs2_iq_civ_manelder_01";
				soldier[193] ="vbs2_iq_civ_manelder_02";
				soldier[194] ="vbs2_iq_civ_man_01_angry";
				soldier[195] ="vbs2_iq_civ_man_02_angry";
				soldier[196] ="vbs2_iq_civ_man_03_angry";
				soldier[197] ="vbs2_iq_civ_man_04_angry";
				soldier[198] ="vbs2_iq_civ_man_05_angry";
				soldier[199] ="vbs2_iq_civ_man_06_angry";
				soldier[200] ="vbs2_iq_civ_manelder_01_angry";
				soldier[201] ="vbs2_iq_civ_manelder_02_angry";
				soldier[202] ="vbs2_iq_civilian_bombvest_01";
				soldier[203] ="vbs2_iq_civilian_bombvest_02";
				soldier[204] ="vbs2_iq_civilian_bombvest_03";
				soldier[205] ="vbs2_iq_civilian_bombvest_04";
				soldier[206] ="vbs2_iq_civilian_bombvest_05";
				soldier[207] ="vbs2_iq_civilian_bombvest_06";
				soldier[208] ="vbs2_iq_civilian_bombvest_elder_01";
				soldier[209] ="vbs2_iq_civilian_bombvest_elder_02";
				soldier[210] ="vbs2_iq_civilian_bombvest_concealed_01";
				soldier[211] ="vbs2_iq_civ_dockworker_01";
				soldier[212] ="vbs2_iq_civ_dockworker_02";
				soldier[213] ="vbs2_iq_civ_dockworker_03";
				soldier[214] ="vbs2_iq_civ_dockworker_04";
				soldier[215] ="vbs2_iq_civ_dockworker_01_angry";
				soldier[216] ="vbs2_iq_civ_dockworker_02_angry";
				soldier[217] ="vbs2_iq_civ_dockworker_03_angry";
				soldier[218] ="vbs2_iq_civ_dockworker_04_angry";
				soldier[219] ="VBS2_IQ_Police_constable_akm";
				soldier[220] ="VBS2_IQ_Police_constable_ak74n";
				soldier[221] ="VBS2_IQ_Police_constable_aks74u";
				soldier[222] ="VBS2_IQ_Police_constable_rpk74";
				soldier[223] ="VBS2_IQ_Police_constable_m590";
				soldier[224] ="VBS2_IQ_Police_constable_m590_riot";
				soldier[225] ="VBS2_IQ_Police_constable_glock17";
				soldier[226] ="VBS2_IQ_Police_constable_makarovPM";
				soldier[227] ="VBS2_IQ_Police_constable_akm_bullhorn";
				soldier[228] ="VBS2_IQ_Police_constable_akm_mirror";
				soldier[229] ="VBS2_IQ_Police_constable_akm_paddle";
				soldier[230] ="VBS2_IQ_National_Policeman_ak47";
				soldier[231] ="VBS2_IQ_Rifleman_ak47";
				soldier[232] ="VBS2_NG_WOMAN_01";
				soldier[233] ="VBS2_NG_WOMAN_02";
				soldier[234] ="VBS2_NG_WOMAN_03";
				soldier[235] ="VBS2_NG_WOMAN_04";
				soldier[236] ="VBS2_NG_WOMAN_05";
				soldier[237] ="VBS2_NG_MAN_01";
				soldier[238] ="VBS2_NG_MAN_02";
				soldier[239] ="VBS2_NG_MAN_03";
				soldier[240] ="VBS2_NG_MAN_04";
				soldier[241] ="VBS2_NG_MAN_05";
				soldier[242] ="VBS2_NG_MAN_06";
				soldier[243] ="VBS2_NG_MAN_07";
				soldier[244] ="VBS2_NG_Militia";
				soldier[245] ="vbs2_ng_soldier_ak47";
				soldier[246] ="vbs2_ng_soldier_rpg";
				soldier[247] ="vbs2_ng_leader_ak47";
				soldier[248] ="vbs2_ng_militia_ak47";
				soldier[249] ="vbs2_ng_militia_rpg";
				soldier[250] ="vbs2_ng_militia_leader";
				soldier[251] ="VBS2_NZ_Army_IFVCrew_W_SteyrIWc";
				soldier[252] ="VBS2_NZ_Army_IFVCrew_D_SteyrIWc";
				soldier[253] ="VBS2_NZ_Army_HeliPilot_W_SigP226";
				soldier[254] ="VBS2_NZ_Army_HeliPilot_D_SigP226";
				soldier[255] ="VBS2_US_ARMY_HeliPilot_W_BerettaM9";
				soldier[256] ="VBS2_US_MC_EOD_W";
				soldier[257] ="VBS2_US_MC_EOD_D";
				soldier[258] ="VBS2_US_MC_FighterPilot_G_berettaM9";
				soldier[259] ="VBS2_US_MC_FighterPilot_D_berettaM9";
				soldier[260] ="VBS2_US_MC_HeliPilot_W_BerettaM9";
				soldier[261] ="VBS2_US_MC_HeliPilot_D_BerettaM9";
				soldier[262] ="vbs2_animal_camel_lightbrown_none";
				soldier[263] ="vbs2_animal_cow_01_none";
				soldier[264] ="vbs2_animal_dog_mongrel_01";
				soldier[265] ="vbs2_animal_germanshepherd_bomb_detection";
				soldier[266] ="vbs2_animal_donkey_01_none";
				soldier[267] ="VBS2_animal_goat_01_none";
				soldier[268] ="VBS2_animal_goat_02_none";
				soldier[269] ="VBS2_animal_goat_03_none";
				soldier[270] ="VBS2_animal_goat_04_none";
				soldier[271] ="VBS2_animal_goat_05_none";
				soldier[272] ="VBS2_animal_goat_06_none";
				soldier[273] ="vbs2_af_taliban_akm";
				soldier[274] ="vbs2_af_taliban_ak74";
				soldier[275] ="vbs2_af_taliban_aks74u";
				soldier[276] ="vbs2_af_taliban_ak74gla";
				soldier[277] ="vbs2_af_taliban_ak74pso";
				soldier[278] ="vbs2_af_taliban_svd";
				soldier[279] ="vbs2_af_taliban_rpk74";
				soldier[280] ="vbs2_af_taliban_pkm";
				soldier[281] ="vbs2_af_taliban_rpgAT";
				soldier[282] ="vbs2_af_taliban_rpgAP";
				soldier[283] ="vbs2_af_taliban_igla";
				soldier[284] ="vbs2_af_talibanLeader_akm";
				soldier[285] ="vbs2_af_talibanLeader_aks74pso";
				soldier[286] ="vbs2_af_talibanLeader_ak74gla";
				soldier[287] ="vbs2_af_talibanLeader_aks74uSD";
				soldier[288] ="vbs2_af_civ_man_1";
				soldier[289] ="vbs2_af_civ_man_2";
				soldier[290] ="vbs2_af_civ_man_3";
				soldier[291] ="vbs2_af_civ_man_4";
				soldier[292] ="vbs2_af_civ_man_5";
				soldier[293] ="vbs2_af_civ_man_1_bombvest";
				soldier[294] ="vbs2_af_civ_man_2_bombvest";
				soldier[295] ="vbs2_af_civ_man_3_bombvest";
				soldier[296] ="vbs2_af_civ_man_4_bombvest";
				soldier[297] ="vbs2_af_civilian_bombvest_facewrap";
				soldier[298] ="vbs2_af_civ_man_1_angry";
				soldier[299] ="vbs2_af_civ_man_2_angry";
				soldier[300] ="vbs2_af_civ_man_3_angry";
				soldier[301] ="vbs2_af_civ_man_4_angry";
				soldier[302] ="vbs2_af_civ_man_5_angry";
				soldier[303] ="VBS2_AU_Army_Rifleman_W_F88A1";
				soldier[304] ="VBS2_AU_Army_Rifleman_D_F88A1";
				soldier[305] ="VBS2_AU_Army_Rifleman_W_F88A2_Aim";
				soldier[306] ="VBS2_AU_Army_Rifleman_D_F88A2_Aim";
				soldier[307] ="VBS2_AU_Army_Rifleman_W_F88A2_Aim_fb";
				soldier[308] ="VBS2_AU_Army_Rifleman_D_F88A2_Aim_fb";
				soldier[309] ="VBS2_AU_Army_Rifleman_W_F88A2_Elc";
				soldier[310] ="VBS2_AU_Army_Rifleman_D_F88A2_Elc";
				soldier[311] ="VBS2_AU_Army_Grenadier_W_F88A1";
				soldier[312] ="VBS2_AU_Army_Grenadier_D_F88A1";
				soldier[313] ="VBS2_AU_Army_Grenadier_W_F88A2_Aim";
				soldier[314] ="VBS2_AU_Army_Grenadier_D_F88A2_Aim";
				soldier[315] ="VBS2_AU_Army_Grenadier_W_F88A2_Elc";
				soldier[316] ="VBS2_AU_Army_Grenadier_D_F88A2_Elc";
				soldier[317] ="VBS2_AU_Army_Mgunner_W_F89";
				soldier[318] ="VBS2_AU_Army_Mgunner_D_F89";
				soldier[319] ="VBS2_AU_Army_Mgunner_W_F89_dim";
				soldier[320] ="VBS2_AU_Army_Mgunner_D_F89_dim";
				soldier[321] ="VBS2_AU_Army_Mgunner_W_mag58";
				soldier[322] ="VBS2_AU_Army_Mgunner_D_mag58";
				soldier[323] ="VBS2_AU_Army_Mgunner_W_mag58_dim";
				soldier[324] ="VBS2_AU_Army_Mgunner_D_mag58_dim";
				soldier[325] ="VBS2_AU_Army_ATsoldier_W_LAW";
				soldier[326] ="VBS2_AU_Army_ATsoldier_D_LAW";
				soldier[327] ="VBS2_AU_Army_ATsoldier_W_CarlGustav";
				soldier[328] ="VBS2_AU_Army_ATsoldier_D_CarlGustav";
				soldier[329] ="VBS2_AU_Army_ATsoldier_W_CarlGustav2";
				soldier[330] ="VBS2_AU_Army_ATsoldier_D_CarlGustav2";
				soldier[331] ="VBS2_AU_Army_ATsoldier_W_Javelin";
				soldier[332] ="VBS2_AU_Army_ATsoldier_D_Javelin";
				soldier[333] ="VBS2_AU_Army_sniper_W_sr98";
				soldier[334] ="VBS2_AU_Army_sniper_D_sr98";
				soldier[335] ="VBS2_AU_Army_sniper_W_aw50";
				soldier[336] ="VBS2_AU_Army_sniper_D_aw50";
				soldier[337] ="VBS2_AU_Army_Medic_W_F88a1";
				soldier[338] ="VBS2_AU_Army_Medic_D_F88a1";
				soldier[339] ="VBS2_AU_Army_Engineer_W_F88A1";
				soldier[340] ="VBS2_AU_Army_Engineer_D_F88A1";
				soldier[341] ="VBS2_AU_Army_Engineer_W_F88A1_mines";
				soldier[342] ="VBS2_AU_Army_Engineer_D_F88A1_mines";
				soldier[343] ="VBS2_AU_Army_Officer_W_F88A2_Elc";
				soldier[344] ="VBS2_AU_Army_Officer_D_F88A2_Elc";
				soldier[345] ="VBS2_AU_Army_Officer_W_F88A2_Elc_night";
				soldier[346] ="VBS2_AU_Army_Officer_D_F88A2_Elc_night";
				soldier[347] ="VBS2_AU_Army_Officer_W_F88A2_Elc_mark";
				soldier[348] ="VBS2_AU_Army_Officer_D_F88A2_Elc_mark";
				soldier[349] ="vbs2_au_army_rifleman_w_F88A1_bullhorn";
				soldier[350] ="vbs2_au_army_rifleman_D_F88A1_bullhorn";
				soldier[351] ="vbs2_au_army_rifleman_w_F88A1_mirror";
				soldier[352] ="vbs2_au_army_rifleman_D_F88A1_mirror";
				soldier[353] ="vbs2_au_army_rifleman_w_F88A1_paddle";
				soldier[354] ="vbs2_au_army_rifleman_D_F88A1_paddle";
				soldier[355] ="VBS2_GB_Army_Soldier_W_L85A2_Kestrel_Helmet_None";
				soldier[356] ="VBS2_GB_Army_Soldier_W_L85A2_Kestrel_Helmet_Radio";
				soldier[357] ="VBS2_GB_Army_Soldier_W_L85A2_Osprey_Helmet_None";
				soldier[358] ="VBS2_GB_Army_Soldier_W_L85A2_Osprey_Helmet_Radio";
				soldier[359] ="VBS2_GB_Army_Soldier_W_L85A2_Osprey_Helmet_CBRN";
				soldier[360] ="VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Helmet_None";
				soldier[361] ="VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Helmet_Radio";
				soldier[362] ="VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Helmet_CBRN";
				soldier[363] ="VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Hat_None";
				soldier[364] ="VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Hat_Radio";
				soldier[365] ="VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Beret_None";
				soldier[366] ="VBS2_GB_Army_Soldier_W_L85A2_ChestRig_Beret_Radio";
				soldier[367] ="VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Helmet_None";
				soldier[368] ="VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Helmet_Radio";
				soldier[369] ="VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Helmet_CBRN";
				soldier[370] ="VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Hat_None";
				soldier[371] ="VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Hat_Radio";
				soldier[372] ="VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Beret_None";
				soldier[373] ="VBS2_GB_Army_Soldier_W_L85A2_BeltOrder_Beret_Radio";
				soldier[374] ="VBS2_GB_Army_Medic_W_L85A2_Kestrel_Helmet_None";
				soldier[375] ="VBS2_GB_Army_Medic_W_L85A2_Osprey_Helmet_None";
				soldier[376] ="VBS2_GB_Army_Medic_W_L85A2_ChestRig_Hat_None";
				soldier[377] ="VBS2_GB_Army_Medic_W_L85A2_BeltOrder_Helmet_None";
				soldier[378] ="VBS2_GB_Army_Soldier_D_L85A2_Kestrel_Helmet_None";
				soldier[379] ="VBS2_GB_Army_Soldier_D_L85A2_Kestrel_Helmet_Radio";
				soldier[380] ="VBS2_GB_Army_Soldier_D_L85A2_Osprey_Helmet_None";
				soldier[381] ="VBS2_GB_Army_Soldier_D_L85A2_Osprey_Helmet_Radio";
				soldier[382] ="VBS2_GB_Army_Soldier_D_L85A2_Osprey_Helmet_CBRN";
				soldier[383] ="VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Helmet_None";
				soldier[384] ="VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Helmet_Radio";
				soldier[385] ="VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Helmet_CBRN";
				soldier[386] ="VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Hat_None";
				soldier[387] ="VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Hat_Radio";
				soldier[388] ="VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Beret_None";
				soldier[389] ="VBS2_GB_Army_Soldier_D_L85A2_ChestRig_Beret_Radio";
				soldier[390] ="VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Helmet_None";
				soldier[391] ="VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Helmet_Radio";
				soldier[392] ="VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Helmet_CBRN";
				soldier[393] ="VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Hat_None";
				soldier[394] ="VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Hat_Radio";
				soldier[395] ="VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Beret_None";
				soldier[396] ="VBS2_GB_Army_Soldier_D_L85A2_BeltOrder_Beret_Radio";
				soldier[397] ="VBS2_GB_Army_Medic_D_L85A2_Kestrel_Helmet_None";
				soldier[398] ="VBS2_GB_Army_Medic_D_L85A2_Osprey_Helmet_None";
				soldier[399] ="VBS2_GB_Army_Medic_D_L85A2_ChestRig_Helmet_None";
				soldier[400] ="VBS2_GB_Army_Medic_D_L85A2_BeltOrder_Helmet_None";
				soldier[401] ="vbs2_iq_insurg_01_akm";
				soldier[402] ="vbs2_iq_insurg_02_akm";
				soldier[403] ="vbs2_iq_insurg_03_akm";
				soldier[404] ="vbs2_iq_insurg_04_akm";
				soldier[405] ="vbs2_iq_insurg_05_akm";
				soldier[406] ="vbs2_iq_insurg_01_ak74";
				soldier[407] ="vbs2_iq_insurg_02_ak74";
				soldier[408] ="vbs2_iq_insurg_03_ak74";
				soldier[409] ="vbs2_iq_insurg_04_ak74";
				soldier[410] ="vbs2_iq_insurg_05_ak74";
				soldier[411] ="vbs2_iq_insurg_01_ak74gla";
				soldier[412] ="vbs2_iq_insurg_02_ak74gla";
				soldier[413] ="vbs2_iq_insurg_03_ak74gla";
				soldier[414] ="vbs2_iq_insurg_04_ak74gla";
				soldier[415] ="vbs2_iq_insurg_05_ak74gla";
				soldier[416] ="vbs2_iq_insurg_01_aks74u";
				soldier[417] ="vbs2_iq_insurg_02_aks74u";
				soldier[418] ="vbs2_iq_insurg_03_aks74u";
				soldier[419] ="vbs2_iq_insurg_04_aks74u";
				soldier[420] ="vbs2_iq_insurg_05_aks74u";
				soldier[421] ="vbs2_iq_insurg_01_pkm";
				soldier[422] ="vbs2_iq_insurg_02_pkm";
				soldier[423] ="vbs2_iq_insurg_03_pkm";
				soldier[424] ="vbs2_iq_insurg_04_pkm";
				soldier[425] ="vbs2_iq_insurg_05_pkm";
				soldier[426] ="vbs2_iq_insurg_01_rpk74";
				soldier[427] ="vbs2_iq_insurg_02_rpk74";
				soldier[428] ="vbs2_iq_insurg_03_rpk74";
				soldier[429] ="vbs2_iq_insurg_04_rpk74";
				soldier[430] ="vbs2_iq_insurg_05_rpk74";
				soldier[431] ="vbs2_iq_insurg_01_svd";
				soldier[432] ="vbs2_iq_insurg_02_svd";
				soldier[433] ="vbs2_iq_insurg_03_svd";
				soldier[434] ="vbs2_iq_insurg_04_svd";
				soldier[435] ="vbs2_iq_insurg_05_svd";
				soldier[436] ="vbs2_iq_insurg_01_rpg";
				soldier[437] ="vbs2_iq_insurg_02_rpg";
				soldier[438] ="vbs2_iq_insurg_03_rpg";
				soldier[439] ="vbs2_iq_insurg_04_rpg";
				soldier[440] ="vbs2_iq_insurg_05_rpg";
				soldier[441] ="vbs2_iq_insurg_01_rpgmodern";
				soldier[442] ="vbs2_iq_insurg_02_rpgmodern";
				soldier[443] ="vbs2_iq_insurg_03_rpgmodern";
				soldier[444] ="vbs2_iq_insurg_04_rpgmodern";
				soldier[445] ="vbs2_iq_insurg_05_rpgmodern";
				soldier[446] ="vbs2_iq_insurg_01_strela";
				soldier[447] ="vbs2_iq_insurg_02_strela";
				soldier[448] ="vbs2_iq_insurg_03_strela";
				soldier[449] ="vbs2_iq_insurg_04_strela";
				soldier[450] ="vbs2_iq_insurg_05_strela";
				soldier[451] ="vbs2_iq_insurgLeader_01_akm";
				soldier[452] ="vbs2_iq_insurgLeader_02_akm";
				soldier[453] ="vbs2_iq_insurgLeader_03_akm";
				soldier[454] ="vbs2_iq_insurgLeader_04_akm";
				soldier[455] ="vbs2_iq_insurgLeader_05_akm";
				soldier[456] ="vbs2_iq_insurgLeader_01_ak74n";
				soldier[457] ="vbs2_iq_insurgLeader_02_ak74n";
				soldier[458] ="vbs2_iq_insurgLeader_03_ak74n";
				soldier[459] ="vbs2_iq_insurgLeader_04_ak74n";
				soldier[460] ="vbs2_iq_insurgLeader_05_ak74n";
				soldier[461] ="vbs2_iq_insurgLeader_01_aks74PSO";
				soldier[462] ="vbs2_iq_insurgLeader_02_aks74PSO";
				soldier[463] ="vbs2_iq_insurgLeader_03_aks74PSO";
				soldier[464] ="vbs2_iq_insurgLeader_04_aks74PSO";
				soldier[465] ="vbs2_iq_insurgLeader_05_aks74PSO";
				soldier[466] ="vbs2_iq_insurgLeader_01_aks74uSD";
				soldier[467] ="vbs2_iq_insurgLeader_02_aks74uSD";
				soldier[468] ="vbs2_iq_insurgLeader_03_aks74uSD";
				soldier[469] ="vbs2_iq_insurgLeader_04_aks74uSD";
				soldier[470] ="vbs2_iq_insurgLeader_05_aks74uSD";
				soldier[471] ="vbs2_iq_militia_01_akm";
				soldier[472] ="vbs2_iq_militia_02_akm";
				soldier[473] ="vbs2_iq_militia_03_akm";
				soldier[474] ="vbs2_iq_militia_04_akm";
				soldier[475] ="vbs2_iq_militia_05_akm";
				soldier[476] ="vbs2_iq_militia_06_akm";
				soldier[477] ="vbs2_iq_militiaelder_01_akm";
				soldier[478] ="vbs2_iq_militiaelder_02_akm";
				soldier[479] ="vbs2_iq_militia_01_pkm";
				soldier[480] ="vbs2_iq_militia_02_pkm";
				soldier[481] ="vbs2_iq_militia_03_pkm";
				soldier[482] ="vbs2_iq_militia_04_pkm";
				soldier[483] ="vbs2_iq_militia_05_pkm";
				soldier[484] ="vbs2_iq_militia_06_pkm";
				soldier[485] ="vbs2_iq_militiaelder_01_pkm";
				soldier[486] ="vbs2_iq_militiaelder_02_pkm";
				soldier[487] ="vbs2_iq_militia_01_rpg";
				soldier[488] ="vbs2_iq_militia_02_rpg";
				soldier[489] ="vbs2_iq_militia_03_rpg";
				soldier[490] ="vbs2_iq_militia_04_rpg";
				soldier[491] ="vbs2_iq_militia_05_rpg";
				soldier[492] ="vbs2_iq_militia_06_rpg";
				soldier[493] ="vbs2_iq_militiaelder_01_rpg";
				soldier[494] ="vbs2_iq_militiaelder_02_rpg";
				soldier[495] ="vbs2_iq_militia_01_makarov";
				soldier[496] ="vbs2_iq_militia_02_makarov";
				soldier[497] ="vbs2_iq_militia_03_makarov";
				soldier[498] ="vbs2_iq_militia_04_makarov";
				soldier[499] ="vbs2_iq_militia_05_makarov";
				soldier[500] ="vbs2_iq_militia_06_makarov";
				soldier[501] ="vbs2_iq_militiaelder_01_makarov";
				soldier[502] ="vbs2_iq_militiaelder_02_makarov";
				soldier[503] ="vbs2_iq_militia_01_aks74u";
				soldier[504] ="vbs2_iq_militia_02_aks74u";
				soldier[505] ="vbs2_iq_militia_03_aks74u";
				soldier[506] ="vbs2_iq_militia_04_aks74u";
				soldier[507] ="vbs2_iq_militia_05_aks74u";
				soldier[508] ="vbs2_iq_militia_06_aks74u";
				soldier[509] ="vbs2_iq_militiaelder_01_aks74u";
				soldier[510] ="vbs2_iq_militiaelder_02_aks74u";
				soldier[511] ="VBS2_NZ_Army_Rifleman_W_SteyrIW";
				soldier[512] ="VBS2_NZ_Army_Rifleman_D_SteyrIW";
				soldier[513] ="VBS2_NZ_Army_Rifleman_W_SteyrIW_ballcap";
				soldier[514] ="VBS2_NZ_Army_Rifleman_D_SteyrIW_ballcap";
				soldier[515] ="VBS2_NZ_Army_Rifleman_W_SteyrIWc_fb_ballcap";
				soldier[516] ="VBS2_NZ_Army_Rifleman_D_SteyrIWc_fb_ballcap";
				soldier[517] ="VBS2_NZ_Army_Grenadier_W_SteyrIW";
				soldier[518] ="VBS2_NZ_Army_Grenadier_D_SteyrIW";
				soldier[519] ="VBS2_NZ_Army_Mgunner_W_C9";
				soldier[520] ="VBS2_NZ_Army_Mgunner_W_C9_ballcap";
				soldier[521] ="VBS2_NZ_Army_Mgunner_D_C9_ballcap";
				soldier[522] ="VBS2_NZ_Army_Mgunner_D_C9";
				soldier[523] ="VBS2_NZ_Army_Mgunner_W_mag58";
				soldier[524] ="VBS2_NZ_Army_Mgunner_D_mag58";
				soldier[525] ="VBS2_NZ_Army_ATsoldier_W_LAW";
				soldier[526] ="VBS2_NZ_Army_ATsoldier_D_LAW";
				soldier[527] ="VBS2_NZ_Army_ATsoldier_W_CarlGustav";
				soldier[528] ="VBS2_NZ_Army_ATsoldier_D_CarlGustav";
				soldier[529] ="VBS2_NZ_Army_ATsoldier_W_CarlGustav2";
				soldier[530] ="VBS2_NZ_Army_ATsoldier_D_CarlGustav2";
				soldier[531] ="VBS2_NZ_Army_ATsoldier_W_Javelin";
				soldier[532] ="VBS2_NZ_Army_ATsoldier_D_Javelin";
				soldier[533] ="VBS2_NZ_Army_sniper_W_L96A1";
				soldier[534] ="VBS2_NZ_Army_sniper_D_L96A1";
				soldier[535] ="VBS2_NZ_Army_sniper_W_m82a3";
				soldier[536] ="VBS2_NZ_Army_sniper_D_m82a3";
				soldier[537] ="VBS2_NZ_Army_Medic_W_SteyrIW";
				soldier[538] ="VBS2_NZ_Army_Medic_D_SteyrIW";
				soldier[539] ="VBS2_NZ_Army_Medic_W_SteyrIW_ballcap";
				soldier[540] ="VBS2_NZ_Army_Medic_D_SteyrIW_ballcap";
				soldier[541] ="VBS2_NZ_Army_Officer_W_SteyrIW";
				soldier[542] ="VBS2_NZ_Army_Officer_W_SteyrIW_ballcap";
				soldier[543] ="VBS2_NZ_Army_Officer_D_SteyrIW_ballcap";
				soldier[544] ="VBS2_NZ_Army_Officer_D_SteyrIW";
				soldier[545] ="VBS2_US_ARMY_IFVCrew_W_M4";
				soldier[546] ="vbs2_us_army_rifleman_w_m4";
				soldier[547] ="VBS2_US_ARMY_Rifleman_W_M4A1";
				soldier[548] ="VBS2_US_ARMY_RiflemanF_W_M4";
				soldier[549] ="VBS2_US_ARMY_Grenadier_W_M4_acog";
				soldier[550] ="VBS2_US_ARMY_MGunner_W_M249";
				soldier[551] ="VBS2_US_ARMY_MGunner_W_M240";
				soldier[552] ="VBS2_US_ARMY_Sniper_W_M107";
				soldier[553] ="VBS2_us_army_ATsoldier_W_Javelin";
				soldier[554] ="VBS2_us_army_ATsoldier_W_AT4heat";
				soldier[555] ="VBS2_us_Army_Engineer_W_M4";
				soldier[556] ="VBS2_us_Army_Engineer_W_M4_Mines";
				soldier[557] ="VBS2_US_ARMY_Medic_W_M4";
				soldier[558] ="VBS2_US_ARMY_Officer_W_M4";
				soldier[559] ="VBS2_US_PMC_Rifleman_W_M4A1";
				soldier[560] ="VBS2_US_ARMY_Interpreter_W";
				soldier[561] ="VBS2_US_MC_IFVCrew_W_M4A1";
				soldier[562] ="VBS2_US_MC_IFVCrew_D_M4A1";
				soldier[563] ="vbs2_us_mc_rifleman_w_m16a4";
				soldier[564] ="vbs2_us_mc_rifleman_D_m16a4";
				soldier[565] ="vbs2_us_mc_rifleman_w_m16a4_nbc";
				soldier[566] ="vbs2_us_mc_rifleman_w_m16a4_nbc_mopp1";
				soldier[567] ="vbs2_us_mc_rifleman_w_m16a4_nbc_mopp2";
				soldier[568] ="vbs2_us_mc_rifleman_w_m16a4_nbc_mopp3";
				soldier[569] ="vbs2_us_mc_rifleman_w_m16a4_nbc_mopp4";
				soldier[570] ="vbs2_us_mc_rifleman_w_m16a4_nbc_moppAlpha";
				soldier[571] ="vbs2_us_mc_rifleman_d_m16a4_nbc";
				soldier[572] ="vbs2_us_mc_rifleman_d_m16a4_nbc_mopp1";
				soldier[573] ="vbs2_us_mc_rifleman_d_m16a4_nbc_mopp2";
				soldier[574] ="vbs2_us_mc_rifleman_d_m16a4_nbc_mopp3";
				soldier[575] ="vbs2_us_mc_rifleman_d_m16a4_nbc_mopp4";
				soldier[576] ="vbs2_us_mc_rifleman_d_m16a4_nbc_moppAlpha";
				soldier[577] ="vbs2_us_mc_rifleman_w_m16a4_acog";
				soldier[578] ="vbs2_us_mc_rifleman_D_m16a4_acog";
				soldier[579] ="vbs2_us_mc_rifleman_w_m16a2";
				soldier[580] ="vbs2_us_mc_rifleman_D_m16a2";
				soldier[581] ="vbs2_us_mc_rifleman_w_m4a1";
				soldier[582] ="vbs2_us_mc_rifleman_D_m4a1";
				soldier[583] ="vbs2_us_mc_rifleman_w_m4a1_aim";
				soldier[584] ="vbs2_us_mc_rifleman_D_m4a1_aim";
				soldier[585] ="vbs2_us_mc_rifleman_w_m4a1_aim_fb";
				soldier[586] ="vbs2_us_mc_rifleman_D_m4a1_aim_fb";
				soldier[587] ="vbs2_us_mc_grenadier_w_m16a4";
				soldier[588] ="vbs2_us_mc_grenadier_D_m16a4";
				soldier[589] ="vbs2_us_mc_grenadier_w_m16a4_acog";
				soldier[590] ="vbs2_us_mc_grenadier_D_m16a4_acog";
				soldier[591] ="vbs2_us_mc_grenadier_w_m16a2";
				soldier[592] ="vbs2_us_mc_grenadier_D_m16a2";
				soldier[593] ="VBS2_us_mc_ATsoldier_W_SMAW";
				soldier[594] ="VBS2_us_mc_ATsoldier_D_SMAW";
				soldier[595] ="VBS2_us_mc_ATsoldier_W_Javelin";
				soldier[596] ="VBS2_us_mc_ATsoldier_D_Javelin";
				soldier[597] ="VBS2_us_mc_ATsoldier_W_AT4heat";
				soldier[598] ="VBS2_us_mc_ATsoldier_D_AT4heat";
				soldier[599] ="VBS2_us_mc_ATsoldier_W_AT4hedp";
				soldier[600] ="VBS2_us_mc_ATsoldier_D_AT4hedp";
				soldier[601] ="VBS2_us_mc_ATsoldier_W_AT4hp";
				soldier[602] ="VBS2_us_mc_ATsoldier_D_AT4hp";
				soldier[603] ="VBS2_us_mc_AAsoldier_W_Stinger";
				soldier[604] ="VBS2_us_mc_AAsoldier_D_Stinger";
				soldier[605] ="vbs2_us_mc_mgunner_w_m249";
				soldier[606] ="vbs2_us_mc_mgunner_D_m249";
				soldier[607] ="vbs2_us_mc_mgunner_w_m249_dim";
				soldier[608] ="vbs2_us_mc_mgunner_D_m249_dim";
				soldier[609] ="vbs2_us_mc_mgunner_w_m240g";
				soldier[610] ="vbs2_us_mc_mgunner_D_m240g";
				soldier[611] ="vbs2_us_mc_mgunner_w_m240g_dim";
				soldier[612] ="vbs2_us_mc_mgunner_D_m240g_dim";
				soldier[613] ="VBS2_us_mc_sniper_W_m40a3";
				soldier[614] ="VBS2_us_mc_sniper_D_m40a3";
				soldier[615] ="VBS2_us_mc_sniper_W_m82a3";
				soldier[616] ="VBS2_us_mc_sniper_D_m82a3";
				soldier[617] ="VBS2_us_mc_dmarksman_W_m14dmr";
				soldier[618] ="VBS2_us_mc_dmarksman_D_m14dmr";
				soldier[619] ="VBS2_us_mc_rifleman_W_m590";
				soldier[620] ="VBS2_us_mc_rifleman_D_m590";
				soldier[621] ="VBS2_us_mc_rifleman_W_m590_riot";
				soldier[622] ="VBS2_us_mc_rifleman_D_m590_riot";
				soldier[623] ="VBS2_us_mc_rifleman_W_m1014";
				soldier[624] ="VBS2_us_mc_rifleman_D_m1014";
				soldier[625] ="vbs2_us_mc_medic_w_m16a2";
				soldier[626] ="vbs2_us_mc_medic_D_m16a2";
				soldier[627] ="VBS2_us_mc_Engineer_W_m16a4";
				soldier[628] ="VBS2_us_mc_Engineer_D_m16a4";
				soldier[629] ="VBS2_us_mc_Engineer_W_m16a4_mines";
				soldier[630] ="VBS2_us_mc_Engineer_D_m16a4_mines";
				soldier[631] ="vbs2_us_mc_officer_w_m16a4_acog";
				soldier[632] ="vbs2_us_mc_officer_D_m16a4_acog";
				soldier[633] ="vbs2_us_mc_breachSpecialist_w_m1014";
				soldier[634] ="vbs2_us_mc_breachSpecialist_D_m1014";
				soldier[635] ="vbs2_us_mc_officer_w_m16a4_acog_night";
				soldier[636] ="vbs2_us_mc_officer_D_m16a4_acog_night";
				soldier[637] ="vbs2_us_mc_officer_w_m16a4_acog_mark";
				soldier[638] ="vbs2_us_mc_officer_D_m16a4_acog_mark";
				soldier[639] ="vbs2_us_mc_rifleman_w_m16a4_bullhorn";
				soldier[640] ="vbs2_us_mc_rifleman_D_m16a4_bullhorn";
				soldier[641] ="vbs2_us_mc_rifleman_w_m16a4_mirror";
				soldier[642] ="vbs2_us_mc_rifleman_D_m16a4_mirror";
				soldier[643] ="vbs2_us_mc_rifleman_w_m16a4_paddle";
				soldier[644] ="vbs2_us_mc_rifleman_D_m16a4_paddle";
				soldier[645] ="VBS2_us_soc_rifleman_A_m4";
				soldier[646] ="VBS2_us_soc_rifleman_A_soflam";
				soldier[647] ="VBS2_us_soc_rifleman2_A_m4";
				soldier[648] ="VBS2_us_soc_rifleman2_A_soflam";
				soldier[649] ="vbs2_wp_soldier_ak74";
				soldier[650] ="vbs2_wp_soldier_rpg";
				soldier[651] ="vbs2_wp_machinegunner_pk";
				soldier[652] ="vbs2_wp_grenadier_ak74";
				soldier[653] ="vbs2_wp_leader_ak47";
				soldier[654] ="vbs2_wp_AFVcrew";
				soldier[655] ="vbs2_wp_pilot_heli";
				soldier[656] ="VBS2_GB_Army_AFVCrew_W_L22A2_None_CrewHelmet_None";
				soldier[657] ="VBS2_GB_Army_AFVCrew_W_L22A2_ChestRig_CrewHelmet_None";
				soldier[658] ="VBS2_GB_Army_AFVCrew_W_L22A2_None_Beret_Radio";
				soldier[659] ="VBS2_GB_Army_AFVCrew_W_L22A2_ChestRig_Beret_Radio";
				soldier[660] ="VBS2_GB_Army_AFVCrew_D_L22A2_None_CrewHelmet_None";
				soldier[661] ="VBS2_GB_Army_AFVCrew_D_L22A2_ChestRig_CrewHelmet_None";
				soldier[662] ="VBS2_GB_Army_AFVCrew_D_L22A2_None_Beret_Radio";
				soldier[663] ="VBS2_GB_Army_AFVCrew_D_L22A2_ChestRig_Beret_Radio";
			};
		};

		class Tank{
		public:

			enum TANK{
				M2StaticMG,
				M119,
				DSHKM,
				AGS,
				D30,
				SearchLight,
				vbs2_au_l119_w,
				vbs2_au_l119_d,
				vbs2_gb_l118_w,
				vbs2_gb_l118_d,
				VBS2_GB_Army_L16A2_Static_W,
				VBS2_GB_Army_L16A2_Static_D,
				VBS2_US_Army_M252_Static_W,
				VBS2_US_Army_M252_Static_D,
				VBS2_ZZ_Army_D30_W,
				VBS2_ZZ_Army_D30_D,
				VBS2_gboss_camera_static,
				VBS2_invis_camera_static,
				VBS2_IRAM_Type1,
				vbs2_UAV_RoverIII,
				vbs2_GB_ARMY_RoverIII_W,
				vbs2_GB_ARMY_RoverIII_D,
				vbs2_AU_Army_tps77,
				vbs2_AU_Army_PSTAR,
				vbs2_US_MC_PSTAR,
				vbs2_UAV_GCS_noScript,
				VBS2_au_army_anmpqr58,
				vbs2_AU_M198_W,
				vbs2_AU_M198_D,
				vbs2_US_MC_M198_W,
				vbs2_US_MC_M198_D,
				VBS2_ZZ_Army_2B14_Static_W,
				VBS2_AU_Army_RBS70_W_SAM,
				VBS2_m49static_W,
				VBS2_m49static_D,
				VBS2_US_Army_M120_Static_W,
				VBS2_gb_ssarf_tripod_W,
				VBS2_gb_ssarf_tripod_D,
				VBS2_US_Army_M224_Static_W,
				vbs2_gb_l118_d_vbs2fires,
				vbs2_gb_l118_d_vbs2fires_dummy,
				VBS2_au_army_nasams_asraam,
				VBS2_au_army_saab_asradr,
				VBS2_GB_FV103_W,
				VBS2_GB_FV103_D,
				VBS2_GB_FV103_W_EPBA,
				VBS2_GB_FV103_D_EPBA,
				VBS2_GB_FV104_W,
				VBS2_GB_FV104_D,
				VBS2_GB_FV104_W_EPBA,
				VBS2_GB_FV104_D_EPBA,
				VBS2_GB_FV106_W,
				VBS2_GB_FV106_D,
				VBS2_GB_FV107_W,
				VBS2_GB_FV107_D,
				VBS2_GB_FV107_W_EPBA_TS09,
				VBS2_GB_FV107_D_EPBA_TS09,
				VBS2_US_ARMY_M109A6_W,
				VBS2_US_ARMY_M3A3_D,
				VBS2_US_ARMY_M3A3_D_ERA,
				VBS2_ZZ_Army_bmp2_W,
				VBS2_ZZ_Army_bmp2_D,
				VBS2_ZZ_Army_bmp2_W_zu23,
				VBS2_ZZ_Army_bmp2_D_zu23,
				VBS2_ZZ_Army_bmp1_W,
				VBS2_ZZ_Army_bmp1_D,
				vbs2_AU_Army_Talon_Control_Unit,
				VBS2_AU_Army_M1A1_W,
				VBS2_AU_Army_M1A1_D,
				VBS2_AU_Army_M1A1_W_at,
				VBS2_AU_Army_M1A1_D_at,
				VBS2_AU_Army_M1A1_W_ap,
				VBS2_AU_Army_M1A1_D_ap,
				VBS2_US_MC_M1A1_W,
				VBS2_US_MC_M1A1_D,
				VBS2_US_MC_M1A1_W_at,
				VBS2_US_MC_M1A1_D_at,
				VBS2_US_MC_M1A1_W_ap,
				VBS2_US_MC_M1A1_D_ap,
				VBS2_US_ARMY_M1A1_W,
				VBS2_US_ARMY_M1A1_D,
				VBS2_US_ARMY_M1A1_W_at,
				VBS2_US_ARMY_M1A1_D_at,
				VBS2_US_ARMY_M1A1_W_ap,
				VBS2_US_ARMY_M1A1_D_ap,
				VBS2_US_ARMY_M1A2_W,
				VBS2_US_ARMY_M1A2_D,
				VBS2_US_ARMY_M1A2_W_at,
				VBS2_US_ARMY_M1A2_D_at,
				VBS2_US_ARMY_M1A2_W_ap,
				VBS2_US_ARMY_M1A2_D_ap,
				VBS2_GB_Army_FV432_W_L37,
				VBS2_GB_Army_FV432_D_L37,
				VBS2_GB_Army_FV432_Bulldog_W_L37,
				VBS2_GB_Army_FV432_Bulldog_D_L37,
				VBS2_GB_Army_FV432_Bulldog_W_L2,
				VBS2_GB_Army_FV432_Bulldog_D_L2,
				VBS2_GB_Army_FV434_W_L37,
				VBS2_GB_Army_FV434_D_L37,
				VBS2_GB_Army_FV436_Radar_W_L37,
				VBS2_GB_Army_FV436_Radar_D_L37,
				VBS2_GB_Army_FV436_Command_W_L37,
				VBS2_GB_Army_FV436_Command_D_L37,
				VBS2_zz_850j,
				VBS2_GB_ARMY_MLRS_W,
				VBS2_GB_ARMY_MLRS_D,
				VBS2_US_ARMY_M270_W,
				VBS2_US_ARMY_M270_D,
				VBS2_ZZ_Army_T55A_W,
				VBS2_ZZ_Army_T62A_W,
				VBS2_ZZ_Army_T72_W,
				VBS2_ZZ_Army_T72_D,
				VBS2_ZZ_Army_T72_W_modern,
				VBS2_ZZ_Army_T72_D_modern,
				VBS2_ZZ_Army_T72_W_dated,
				VBS2_ZZ_Army_T72_D_dated,
				VBS2_ZZ_Army_T72_W_obsolete,
				VBS2_ZZ_Army_T72_D_obsolete,
				VBS2_GB_ARMY_AS90_W,
				VBS2_GB_ARMY_AS90_D,
				VBS2_GB_Rhino_W,
				VBS2_GB_Rhino_D,
				VBS2_GB_Challenger2_W_Chobham,
				VBS2_GB_Challenger2_D_Chobham,
				VBS2_GB_Challenger2_W_DorchesterL2,
				VBS2_GB_Challenger2_D_DorchesterL2,
				VBS2_GB_Challenger2_W_DorchesterL2A,
				VBS2_GB_Challenger2_D_DorchesterL2A,
				VBS2_GB_Titan_W,
				VBS2_GB_Titan_D,
				VBS2_GB_Trojan_W,
				VBS2_GB_Trojan_D,
				VBS2_ZZ_Army_ZSU_23_4_W,
				VBS2_zz_250,
				M1Abrams,
				M113,
				M113_RACS,
				M113Ambul,
				Vulcan,
				Vulcan_RACS,
				BMP2,
				BMP2Ambul,
				T72,
				ZSU,
				vbs2_aerostat_17m_static,
				vbs2_scaneagle_launcher,
				vbs2_scaneagle_skyhook,
				VBS2_ZZ_DShKM,
				VBS2_ZZ_DShKM_API,
				VBS2_ZZ_spg9_pg9v,
				VBS2_ZZ_spg9_pg9n,
				VBS2_ZZ_spg9_pg9nt,
				VBS2_ZZ_spg9_og9v,
				VBS2_ZZ_zu23,
				VBS2_GB_Army_GMG_Static_W,
				VBS2_GB_Army_GMG_Static_D,
				VBS2_GB_Army_L7A2_Static_W,
				VBS2_GB_Army_L7A2_Static_D,
				VBS2_AU_Army_M2static_W,
				VBS2_AU_Army_M2static_D,
				VBS2_AU_Army_M2static_W_API,
				VBS2_AU_Army_M2static_D_API,
				VBS2_AU_Army_MK19static_W,
				VBS2_AU_Army_MK19static_D,
				VBS2_AU_Army_MK19static_W_HEDP,
				VBS2_AU_Army_MK19static_D_HEDP,
				VBS2_GB_Army_L2A1_Static_W,
				VBS2_GB_Army_L2A1_Static_D,
				VBS2_GB_Army_L2A1_Static_W_API,
				VBS2_GB_Army_L2A1_Static_D_API,
				VBS2_GB_Army_L2A1_Static_W_SLAP,
				VBS2_GB_Army_L2A1_Static_D_SLAP,
				VBS2_NZ_Army_M2static_W,
				VBS2_NZ_Army_M2static_W_API,
				VBS2_NZ_Army_MK19static_W,
				VBS2_NZ_Army_MK19static_W_HEDP,
				VBS2_us_mc_M2static_W,
				VBS2_us_mc_M2static_D,
				VBS2_us_mc_M2static_antvs5_W,
				VBS2_us_mc_M2static_antvs5_D,
				VBS2_us_mc_M2static_anpas13_W,
				VBS2_us_mc_M2static_anpas13_D,
				VBS2_us_mc_M2static_W_API,
				VBS2_us_mc_M2static_D_API,
				VBS2_us_mc_M2static_W_SLAP,
				VBS2_us_mc_M2static_D_SLAP,
				VBS2_us_mc_MK19static_W,
				VBS2_us_mc_MK19static_D,
				VBS2_us_mc_MK19static_W_HEDP,
				VBS2_us_mc_MK19static_D_HEDP,
				VBS2_usarmy_M2static_W,
				VBS2_usarmy_M2static_D,
				VBS2_usarmy_M2static_W_API,
				VBS2_usarmy_M2static_D_API,
				VBS2_usarmy_M2static_W_SLAP,
				VBS2_usarmy_M2static_D_SLAP,
				VBS2_usarmy_MK19static_W,
				VBS2_usarmy_MK19static_D,
				VBS2_usarmy_MK19static_W_HEDP,
				VBS2_usarmy_MK19static_D_HEDP,
				vbs2_AU_Army_GSR,
				vbs2_US_MC_GSR,
				VBS2_US_MC_AAVP7A1_W,
				VBS2_US_MC_AAVP7A1_D,
				vbs2_au_army_m113as4_w,
				vbs2_au_army_m113as4_D,
				vbs2_us_army_m1064a3_d,
				vbs2_us_army_m113a3_d,
				vbs2_usarmy_d7,
				vbs2_usarmy_d9_m2,
				vbs2_usarmy_d9_mk19,
				VBS2_au_850j,
				VBS2_au_250,
				VBS2_GB_FV510_W,
				VBS2_GB_FV510_D,
				VBS2_GB_FV510_W_Chobham,
				VBS2_GB_FV510_D_Chobham,
				VBS2_GB_FV510_W_EPBA,
				VBS2_GB_FV510_D_EPBA,
				VBS2_GB_FV510_W_WRAP2,
				VBS2_GB_FV510_D_WRAP2,
				VBS2_GB_FV511_W,
				VBS2_GB_FV511_D,
				VBS2_GB_FV511_W_Chobham,
				VBS2_GB_FV511_D_Chobham,
				VBS2_GB_FV511_W_EPBA,
				VBS2_GB_FV511_D_EPBA,
				VBS2_GB_FV511_W_WRAP2,
				VBS2_GB_FV511_D_WRAP2,
				VBS2_GB_FV512_W,
				VBS2_GB_FV512_D,
				VBS2_GB_FV512_W_EPBA,
				VBS2_GB_FV512_D_EPBA,
				VBS2_GB_FV513_W,
				VBS2_GB_FV513_D,
				VBS2_GB_FV513_W_EPBA,
				VBS2_GB_FV513_D_EPBA,
				VBS2_GB_FV514_W,
				VBS2_GB_FV514_D,
				VBS2_GB_FV514_W_Chobham,
				VBS2_GB_FV514_D_Chobham,
				VBS2_GB_FV514_W_EPBA,
				VBS2_GB_FV514_D_EPBA,
				VBS2_GB_FV514_W_WRAP2,
				VBS2_GB_FV514_D_WRAP2,
			};

			string tank[245];
			Tank(){
				tank[0] ="M2StaticMG";
				tank[1] ="M119";
				tank[2] ="DSHKM";
				tank[3] ="AGS";
				tank[4] ="D30";
				tank[5] ="SearchLight";
				tank[6] ="vbs2_au_l119_w";
				tank[7] ="vbs2_au_l119_d";
				tank[8] ="vbs2_gb_l118_w";
				tank[9] ="vbs2_gb_l118_d";
				tank[10] ="VBS2_GB_Army_L16A2_Static_W";
				tank[11] ="VBS2_GB_Army_L16A2_Static_D";
				tank[12] ="VBS2_US_Army_M252_Static_W";
				tank[13] ="VBS2_US_Army_M252_Static_D";
				tank[14] ="VBS2_ZZ_Army_D30_W";
				tank[15] ="VBS2_ZZ_Army_D30_D";
				tank[16] ="VBS2_gboss_camera_static";
				tank[17] ="VBS2_invis_camera_static";
				tank[18] ="VBS2_IRAM_Type1";
				tank[19] ="vbs2_UAV_RoverIII";
				tank[20] ="vbs2_GB_ARMY_RoverIII_W";
				tank[21] ="vbs2_GB_ARMY_RoverIII_D";
				tank[22] ="vbs2_AU_Army_tps77";
				tank[23] ="vbs2_AU_Army_PSTAR";
				tank[24] ="vbs2_US_MC_PSTAR";
				tank[25] ="vbs2_UAV_GCS_noScript";
				tank[26] ="VBS2_au_army_anmpqr58";
				tank[27] ="vbs2_AU_M198_W";
				tank[28] ="vbs2_AU_M198_D";
				tank[29] ="vbs2_US_MC_M198_W";
				tank[30] ="vbs2_US_MC_M198_D";
				tank[31] ="VBS2_ZZ_Army_2B14_Static_W";
				tank[32] ="VBS2_AU_Army_RBS70_W_SAM";
				tank[33] ="VBS2_m49static_W";
				tank[34] ="VBS2_m49static_D";
				tank[35] ="VBS2_US_Army_M120_Static_W";
				tank[36] ="VBS2_gb_ssarf_tripod_W";
				tank[37] ="VBS2_gb_ssarf_tripod_D";
				tank[38] ="VBS2_US_Army_M224_Static_W";
				tank[39] ="vbs2_gb_l118_d_vbs2fires";
				tank[40] ="vbs2_gb_l118_d_vbs2fires_dummy";
				tank[41] ="VBS2_au_army_nasams_asraam";
				tank[42] ="VBS2_au_army_saab_asradr";
				tank[43] ="VBS2_GB_FV103_W";
				tank[44] ="VBS2_GB_FV103_D";
				tank[45] ="VBS2_GB_FV103_W_EPBA";
				tank[46] ="VBS2_GB_FV103_D_EPBA";
				tank[47] ="VBS2_GB_FV104_W";
				tank[48] ="VBS2_GB_FV104_D";
				tank[49] ="VBS2_GB_FV104_W_EPBA";
				tank[50] ="VBS2_GB_FV104_D_EPBA";
				tank[51] ="VBS2_GB_FV106_W";
				tank[52] ="VBS2_GB_FV106_D";
				tank[53] ="VBS2_GB_FV107_W";
				tank[54] ="VBS2_GB_FV107_D";
				tank[55] ="VBS2_GB_FV107_W_EPBA_TS09";
				tank[56] ="VBS2_GB_FV107_D_EPBA_TS09";
				tank[57] ="VBS2_US_ARMY_M109A6_W";
				tank[58] ="VBS2_US_ARMY_M3A3_D";
				tank[59] ="VBS2_US_ARMY_M3A3_D_ERA";
				tank[60] ="VBS2_ZZ_Army_bmp2_W";
				tank[61] ="VBS2_ZZ_Army_bmp2_D";
				tank[62] ="VBS2_ZZ_Army_bmp2_W_zu23";
				tank[63] ="VBS2_ZZ_Army_bmp2_D_zu23";
				tank[64] ="VBS2_ZZ_Army_bmp1_W";
				tank[65] ="VBS2_ZZ_Army_bmp1_D";
				tank[66] ="vbs2_AU_Army_Talon_Control_Unit";
				tank[67] ="VBS2_AU_Army_M1A1_W";
				tank[68] ="VBS2_AU_Army_M1A1_D";
				tank[69] ="VBS2_AU_Army_M1A1_W_at";
				tank[70] ="VBS2_AU_Army_M1A1_D_at";
				tank[71] ="VBS2_AU_Army_M1A1_W_ap";
				tank[72] ="VBS2_AU_Army_M1A1_D_ap";
				tank[73] ="VBS2_US_MC_M1A1_W";
				tank[74] ="VBS2_US_MC_M1A1_D";
				tank[75] ="VBS2_US_MC_M1A1_W_at";
				tank[76] ="VBS2_US_MC_M1A1_D_at";
				tank[77] ="VBS2_US_MC_M1A1_W_ap";
				tank[78] ="VBS2_US_MC_M1A1_D_ap";
				tank[79] ="VBS2_US_ARMY_M1A1_W";
				tank[80] ="VBS2_US_ARMY_M1A1_D";
				tank[81] ="VBS2_US_ARMY_M1A1_W_at";
				tank[82] ="VBS2_US_ARMY_M1A1_D_at";
				tank[83] ="VBS2_US_ARMY_M1A1_W_ap";
				tank[84] ="VBS2_US_ARMY_M1A1_D_ap";
				tank[85] ="VBS2_US_ARMY_M1A2_W";
				tank[86] ="VBS2_US_ARMY_M1A2_D";
				tank[87] ="VBS2_US_ARMY_M1A2_W_at";
				tank[88] ="VBS2_US_ARMY_M1A2_D_at";
				tank[89] ="VBS2_US_ARMY_M1A2_W_ap";
				tank[90] ="VBS2_US_ARMY_M1A2_D_ap";
				tank[91] ="VBS2_GB_Army_FV432_W_L37";
				tank[92] ="VBS2_GB_Army_FV432_D_L37";
				tank[93] ="VBS2_GB_Army_FV432_Bulldog_W_L37";
				tank[94] ="VBS2_GB_Army_FV432_Bulldog_D_L37";
				tank[95] ="VBS2_GB_Army_FV432_Bulldog_W_L2";
				tank[96] ="VBS2_GB_Army_FV432_Bulldog_D_L2";
				tank[97] ="VBS2_GB_Army_FV434_W_L37";
				tank[98] ="VBS2_GB_Army_FV434_D_L37";
				tank[99] ="VBS2_GB_Army_FV436_Radar_W_L37";
				tank[100] ="VBS2_GB_Army_FV436_Radar_D_L37";
				tank[101] ="VBS2_GB_Army_FV436_Command_W_L37";
				tank[102] ="VBS2_GB_Army_FV436_Command_D_L37";
				tank[103] ="VBS2_zz_850j";
				tank[104] ="VBS2_GB_ARMY_MLRS_W";
				tank[105] ="VBS2_GB_ARMY_MLRS_D";
				tank[106] ="VBS2_US_ARMY_M270_W";
				tank[107] ="VBS2_US_ARMY_M270_D";
				tank[108] ="VBS2_ZZ_Army_T55A_W";
				tank[109] ="VBS2_ZZ_Army_T62A_W";
				tank[110] ="VBS2_ZZ_Army_T72_W";
				tank[111] ="VBS2_ZZ_Army_T72_D";
				tank[112] ="VBS2_ZZ_Army_T72_W_modern";
				tank[113] ="VBS2_ZZ_Army_T72_D_modern";
				tank[114] ="VBS2_ZZ_Army_T72_W_dated";
				tank[115] ="VBS2_ZZ_Army_T72_D_dated";
				tank[116] ="VBS2_ZZ_Army_T72_W_obsolete";
				tank[117] ="VBS2_ZZ_Army_T72_D_obsolete";
				tank[118] ="VBS2_GB_ARMY_AS90_W";
				tank[119] ="VBS2_GB_ARMY_AS90_D";
				tank[120] ="VBS2_GB_Rhino_W";
				tank[121] ="VBS2_GB_Rhino_D";
				tank[122] ="VBS2_GB_Challenger2_W_Chobham";
				tank[123] ="VBS2_GB_Challenger2_D_Chobham";
				tank[124] ="VBS2_GB_Challenger2_W_DorchesterL2";
				tank[125] ="VBS2_GB_Challenger2_D_DorchesterL2";
				tank[126] ="VBS2_GB_Challenger2_W_DorchesterL2A";
				tank[127] ="VBS2_GB_Challenger2_D_DorchesterL2A";
				tank[128] ="VBS2_GB_Titan_W";
				tank[129] ="VBS2_GB_Titan_D";
				tank[130] ="VBS2_GB_Trojan_W";
				tank[131] ="VBS2_GB_Trojan_D";
				tank[132] ="VBS2_ZZ_Army_ZSU_23_4_W";
				tank[133] ="VBS2_zz_250";
				tank[134] ="M1Abrams";
				tank[135] ="M113";
				tank[136] ="M113_RACS";
				tank[137] ="M113Ambul";
				tank[138] ="Vulcan";
				tank[139] ="Vulcan_RACS";
				tank[140] ="BMP2";
				tank[141] ="BMP2Ambul";
				tank[142] ="T72";
				tank[143] ="ZSU";
				tank[144] ="vbs2_aerostat_17m_static";
				tank[145] ="vbs2_scaneagle_launcher";
				tank[146] ="vbs2_scaneagle_skyhook";
				tank[147] ="VBS2_ZZ_DShKM";
				tank[148] ="VBS2_ZZ_DShKM_API";
				tank[149] ="VBS2_ZZ_spg9_pg9v";
				tank[150] ="VBS2_ZZ_spg9_pg9n";
				tank[151] ="VBS2_ZZ_spg9_pg9nt";
				tank[152] ="VBS2_ZZ_spg9_og9v";
				tank[153] ="VBS2_ZZ_zu23";
				tank[154] ="VBS2_GB_Army_GMG_Static_W";
				tank[155] ="VBS2_GB_Army_GMG_Static_D";
				tank[156] ="VBS2_GB_Army_L7A2_Static_W";
				tank[157] ="VBS2_GB_Army_L7A2_Static_D";
				tank[158] ="VBS2_AU_Army_M2static_W";
				tank[159] ="VBS2_AU_Army_M2static_D";
				tank[160] ="VBS2_AU_Army_M2static_W_API";
				tank[161] ="VBS2_AU_Army_M2static_D_API";
				tank[162] ="VBS2_AU_Army_MK19static_W";
				tank[163] ="VBS2_AU_Army_MK19static_D";
				tank[164] ="VBS2_AU_Army_MK19static_W_HEDP";
				tank[165] ="VBS2_AU_Army_MK19static_D_HEDP";
				tank[166] ="VBS2_GB_Army_L2A1_Static_W";
				tank[167] ="VBS2_GB_Army_L2A1_Static_D";
				tank[168] ="VBS2_GB_Army_L2A1_Static_W_API";
				tank[169] ="VBS2_GB_Army_L2A1_Static_D_API";
				tank[170] ="VBS2_GB_Army_L2A1_Static_W_SLAP";
				tank[171] ="VBS2_GB_Army_L2A1_Static_D_SLAP";
				tank[172] ="VBS2_NZ_Army_M2static_W";
				tank[173] ="VBS2_NZ_Army_M2static_W_API";
				tank[174] ="VBS2_NZ_Army_MK19static_W";
				tank[175] ="VBS2_NZ_Army_MK19static_W_HEDP";
				tank[176] ="VBS2_us_mc_M2static_W";
				tank[177] ="VBS2_us_mc_M2static_D";
				tank[178] ="VBS2_us_mc_M2static_antvs5_W";
				tank[179] ="VBS2_us_mc_M2static_antvs5_D";
				tank[180] ="VBS2_us_mc_M2static_anpas13_W";
				tank[181] ="VBS2_us_mc_M2static_anpas13_D";
				tank[182] ="VBS2_us_mc_M2static_W_API";
				tank[183] ="VBS2_us_mc_M2static_D_API";
				tank[184] ="VBS2_us_mc_M2static_W_SLAP";
				tank[185] ="VBS2_us_mc_M2static_D_SLAP";
				tank[186] ="VBS2_us_mc_MK19static_W";
				tank[187] ="VBS2_us_mc_MK19static_D";
				tank[188] ="VBS2_us_mc_MK19static_W_HEDP";
				tank[189] ="VBS2_us_mc_MK19static_D_HEDP";
				tank[190] ="VBS2_usarmy_M2static_W";
				tank[191] ="VBS2_usarmy_M2static_D";
				tank[192] ="VBS2_usarmy_M2static_W_API";
				tank[193] ="VBS2_usarmy_M2static_D_API";
				tank[194] ="VBS2_usarmy_M2static_W_SLAP";
				tank[195] ="VBS2_usarmy_M2static_D_SLAP";
				tank[196] ="VBS2_usarmy_MK19static_W";
				tank[197] ="VBS2_usarmy_MK19static_D";
				tank[198] ="VBS2_usarmy_MK19static_W_HEDP";
				tank[199] ="VBS2_usarmy_MK19static_D_HEDP";
				tank[200] ="vbs2_AU_Army_GSR";
				tank[201] ="vbs2_US_MC_GSR";
				tank[202] ="VBS2_US_MC_AAVP7A1_W";
				tank[203] ="VBS2_US_MC_AAVP7A1_D";
				tank[204] ="vbs2_au_army_m113as4_w";
				tank[205] ="vbs2_au_army_m113as4_D";
				tank[206] ="vbs2_us_army_m1064a3_d";
				tank[207] ="vbs2_us_army_m113a3_d";
				tank[208] ="vbs2_usarmy_d7";
				tank[209] ="vbs2_usarmy_d9_m2";
				tank[210] ="vbs2_usarmy_d9_mk19";
				tank[211] ="VBS2_au_850j";
				tank[212] ="VBS2_au_250";
				tank[213] ="VBS2_GB_FV510_W";
				tank[214] ="VBS2_GB_FV510_D";
				tank[215] ="VBS2_GB_FV510_W_Chobham";
				tank[216] ="VBS2_GB_FV510_D_Chobham";
				tank[217] ="VBS2_GB_FV510_W_EPBA";
				tank[218] ="VBS2_GB_FV510_D_EPBA";
				tank[219] ="VBS2_GB_FV510_W_WRAP2";
				tank[220] ="VBS2_GB_FV510_D_WRAP2";
				tank[221] ="VBS2_GB_FV511_W";
				tank[222] ="VBS2_GB_FV511_D";
				tank[223] ="VBS2_GB_FV511_W_Chobham";
				tank[224] ="VBS2_GB_FV511_D_Chobham";
				tank[225] ="VBS2_GB_FV511_W_EPBA";
				tank[226] ="VBS2_GB_FV511_D_EPBA";
				tank[227] ="VBS2_GB_FV511_W_WRAP2";
				tank[228] ="VBS2_GB_FV511_D_WRAP2";
				tank[229] ="VBS2_GB_FV512_W";
				tank[230] ="VBS2_GB_FV512_D";
				tank[231] ="VBS2_GB_FV512_W_EPBA";
				tank[232] ="VBS2_GB_FV512_D_EPBA";
				tank[233] ="VBS2_GB_FV513_W";
				tank[234] ="VBS2_GB_FV513_D";
				tank[235] ="VBS2_GB_FV513_W_EPBA";
				tank[236] ="VBS2_GB_FV513_D_EPBA";
				tank[237] ="VBS2_GB_FV514_W";
				tank[238] ="VBS2_GB_FV514_D";
				tank[239] ="VBS2_GB_FV514_W_Chobham";
				tank[240] ="VBS2_GB_FV514_D_Chobham";
				tank[241] ="VBS2_GB_FV514_W_EPBA";
				tank[242] ="VBS2_GB_FV514_D_EPBA";
				tank[243] ="VBS2_GB_FV514_W_WRAP2";
				tank[244] ="VBS2_GB_FV514_D_WRAP2";
			};
		};

		class Vasi{
		public:

			enum VASI{
				Land_Runway_PAPI,
				Land_Runway_PAPI_2,
				Land_Runway_PAPI_3,
				Land_Runway_PAPI_4,
			};

			string vasi[4];
			Vasi(){
				vasi[0] ="Land_Runway_PAPI";
				vasi[1] ="Land_Runway_PAPI_2";
				vasi[2] ="Land_Runway_PAPI_3";
				vasi[3] ="Land_Runway_PAPI_4";
			};
		};

	};
};
#endif