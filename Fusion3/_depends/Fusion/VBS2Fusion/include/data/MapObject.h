
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	MapObject.h

Purpose:

	This file contains the declaration of the GeometryObject class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			12-03/2012	NDB: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_MAPOBJ_H
#define VBS2FUSION_MAPOBJ_H
/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "VBSFusionDefinitions.h"


/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{
	class VBSFUSION_API MapObject
	{
	public:
		/*!
		Main constructor for this class. The following parameters are initialized as follows:
		*/
		MapObject();

		/*!
		Main destructor for this class. 
		*/
		~MapObject();

		/*!
		Assignment operator for MapObject class.
		Assign all the attributes of MapObject class.
		*/
		MapObject& operator =(const MapObject& object);

		/*!
		sets the Id of the object
		*/
		void setID(const std::string& id);

		/*!
		sets the position for the object
		*/
		void setPosition(const position3D& pos);

		/*!
		returns the Id of the object
		*/
		std::string getID() const;

		/*!
		returns the position for the object
		*/
		position3D getPosition() const;
		
	private:
		//void Initialize();
		std::string _id;
		position3D _pos;
	};

}; 
#endif //VBS2FUSION_GEOMOBJ_H
