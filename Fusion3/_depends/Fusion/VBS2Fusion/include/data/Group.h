
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	Group.h

Purpose:

	This file contains the declaration of the Group class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			12-09/2009	RMR: Original Implementation
	2.0			12-12/2009  UDW: added getLogFileName(), isActivated(),
								 getObjMapIndex(), setObjMapIndex(), Initialize()
	2.01		11-02/2010	MHA: Comments Checked
	2.02		17-08/2010  YFP: Modified non implemented  numberOfVehicles() to getNumberOfVehicles()  
	2.03		03-11/2010  YFP: Added begin & end methods for vehicle list inside group.
	2.04		10-01/2011  CGS: Added setActivated(bool status).
								 Modified setAlias & setName.
									operator=(const Group& group)
									

/************************************************************************/

#ifndef VBS2FUSION_GROUP_H
#define VBS2FUSION_GROUP_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push) 
#pragma warning (disable: 4251)


/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// STANDARD INCLUDES
#include <string>
#include <set>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBSFusion.h"
#include "data/Unit.h"
#include "data/Vehicle.h"
#include "data/Waypoint.h"
#include "data/NetworkID.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{
	
	class VBSFUSION_API Group
	{
	public:
		/*!
		Typedef for vector containing all units belonging to the group. 
		*/
		typedef std::set<Unit> unitList;

		/*!
		Typedef for vector containing all units belonging to the group. 
		*/
		typedef NetworkID GroupID;


		/*!
		Iterator for unit list. 
		*/
		typedef std::set<Unit>::iterator iterator;

		/*!
		Const iterator for unit list. 
		*/
		typedef std::set<Unit>::const_iterator const_iterator;

		/*!
		Typedef for vector containing all Waypoints belonging to the group. 
		*/
		typedef std::set<Waypoint> waypointList;

		/*!
		Iterator for waypoint list. 
		*/
		typedef std::set<Waypoint>::iterator wp_iterator;

		/*!
		Const iterator for waypoint list. 
		*/
		typedef std::set<Waypoint>::const_iterator const_wp_iterator;

		/*!
		Typedef for vector containing all Waypoints belonging to the group. 
		*/
		typedef std::set<Vehicle> vehicleList;

		/*!
		Iterator for waypoint list. 
		*/
		typedef std::set<Vehicle>::iterator vehicle_iterator;

		/*!
		Const iterator for waypoint list. 
		*/
		typedef std::set<Vehicle>::const_iterator const_vehicle_iterator;

		/*!
		Main constructor for the Group class. Initializes the following:
		_boolHasLeader = false; 
		_Formation = NONE;
		_strAlias = "";
		_strLeaderAlias = "";
		_strLeaderID = "";
		_strLeader = "";
		_Side = WEST;
		*/
		Group();

		/*!
		Main constructor for the Group class. Initializes the following:
		_boolHasLeader = false; 
		_Formation = NONE;
		_strAlias = "";
		_strLeaderAlias = "";
		_strLeaderID = "";
		_strLeader = "";
		_Side = WEST;
		_strName = strName;
		*/
		Group(const std::string& strName);

		/*!Copy constructor for group*/
		Group(const Group& _group);

		/*!
		Main destructor for the class. 
		*/
		virtual ~Group();

		/*!
		Sets the VBS2 name of the group. 
		*/
		void setName(const std::string& strName);

		/*!
		Returns the VBS2 name of the group. 
		*/
		std::string getName() const;

		/*!
		Sets the VBS2Fusion alias of the group. 
		*/
		void setAlias(const std::string& strAlias);

		/*!
		Returns the VBS2Fusion alias of the group. If
		no alias is set, it returns the name using getName().  
		*/
		std::string getAlias() const;

		/*!
		Retrieves the list of units. 
		*/
		unitList getUnits() const;
		
		/*!
		Adds a new unit to the group. 
		*/
		void addUnit(const Unit& unit);

		/*!
		Removes the unit from the group. Returns false if the unit
		does not belong to the group and the removal was unsuccessful. 
		*/
		bool removeUnit(const std::string& unitName);

		/*!
		Removes the unit from the group. Returns false if the unit
		does not belong to the group and the removal was unsuccessful. 
		*/
		bool removeUnit(const Unit& unit);

		/*!
		Returns true if a leader is assigned to the group. 
		*/
		bool hasLeader() const;

		/*!
		Sets the unit with the specified name as the leader. Returns false if the unit
		is not a member of the group and the leadership allocation was unsuccessful. 
		*/
		bool setLeader(const std::string& unitName);

		/*!
		Sets the unit with the specified name as the leader. Returns false if the unit
		is not a member of the group and the leadership allocation was unsuccessful. 
		*/
		bool setLeader(const Unit& unit);

		/*!
		Returns the leader of the group. 
		*/
		Unit getLeader() const;

		/*!
		Returns the name of the leader. 
		*/
		std::string getLeaderName() const;

		/*!
		Returns the VBS2Fusion alias of the leader. 
		*/
		std::string getLeaderAlias() const;

		/*!
		Returns the network ID of the leader. 
		*/
		std::string getLeaderID() const;		

		/*!
		Begin iterator for unit list. 
		*/
		iterator begin();

		/*!
		Begin iterator for unit list. 
		*/
		const_iterator begin() const;

		/*!
		End iterator for unit list. 
		*/
		iterator end();

		/*!
		End iterator for unit list. 
		*/
		const_iterator end() const;

		/*!
		Returns the number of units belonging to the group. 
		*/
		int getNumberOfUnits() const;

		/*!
		Clears the unit list. 
		*/
		void clearAllUnits();

		/*!
		Returns the waypoint list. 
		*/
		waypointList getWaypoints() const;

		/*!
		Adds a new waypoint to the end of the list. 
		*/
		void addWaypoint(Waypoint& wp);

		/*!
		Inserts a waypoint into the list. 
		*/
		void insertWaypoint(const Waypoint& wp, int position);

		/*!
		Removes the specified waypoint. Returns false if the specified waypoint
		was not a member of the list. 
		*/
		bool removeWaypoint(const std::string& waypointName);

		/*!
		Removes the specified waypoint. Returns false if the specified waypoint
		was not a member of the list. 
		*/
		bool removeWaypoint(const Waypoint& wp);

		/*!
		Removes the specified waypoint. Returns false if the specified waypoint
		was not a member of the list. 
		*/
		bool removeWaypoint(int position);

		/*!
		Returns the number of Waypoints. 
		*/
		int getNumberOfWaypoints() const;

		/*!
		Clears the waypoint list.
		*/
		void clearAllWaypoints();

		/*!
		Begin iterator for waypoint list. 
		*/
		wp_iterator wp_begin();

		/*!
		Begin iterator for waypoint list. 
		*/
		const_wp_iterator wp_begin() const;

		/*!
		End iterator for waypoint list. 
		*/
		wp_iterator wp_end();

		/*!
		End iterator for waypoint list. 
		*/
		const_wp_iterator wp_end() const;	

		/*! Begin iterator for vehicle list. */
		vehicle_iterator vehicle_begin();

		/*! Begin const iterator for vehicle list. */
		const_vehicle_iterator vehicle_begin() const;

		/*! End iterator for vehicle list. */
		vehicle_iterator vehicle_end();

		/*! End const iterator for vehicle list. */
		const_vehicle_iterator vehicle_end() const;
		
		/*!
		Sets the formation of the group in string format. Should be one
		of ("COLUMN", "STAG_COLUMN", "WEDGE", "ECH_LEFT", "ECH_RIGHT", "VEE", "LINE", "NONE")
		*/
		void setFormation(const std::string& strFormation);

		/*!
		Sets the formation of the group using a FORMATION variable. Should be one
		of (COLUMN, STAG_COLUMN, WEDGE, ECH_LEFT, ECH_RIGHT, VEE, LINE, NONE)
		*/
		void setFormation(FORMATION Formation);

		/*!
		Returns the formation of the group using a FORMATION variable. Should be one
		of (COLUMN, STAG_COLUMN, WEDGE, ECH_LEFT, ECH_RIGHT, VEE, LINE, NONE)
		*/
		FORMATION getFormation() const;

		/*!
		Returns the formation of the group in string format. Should be one
		of ("COLUMN", "STAG_COLUMN", "WEDGE", "ECH_LEFT", "ECH_RIGHT", "VEE", "LINE", "NONE")
		*/
		std::string getFormationString() const;

		/*!
		Sets the side of the group in string format. Should be one
		of ("WEST", "EAST", "CIVILIAN", "RESISTANCE")
		*/
		void setSide(const std::string& strSide);

		/*!
		Sets the side of the group using a SIDE variable. Should be one
		of (WEST, EAST, CIVILIAN, RESISTANCE)
		*/
		void setSide(SIDE Side);

		/*!
		Returns the side of the group using a SIDE variable. Should be one
		of (WEST, EAST, CIVILIAN, RESISTANCE)
		*/
		SIDE getSide() const;

		/*!
		Returns the side of the group in string format. Should be one
		of ("WEST", "EAST", "CIVILIAN", "RESISTANCE")
		*/
		std::string getSideString() const;			

		/*!
		Returns true if a unit with the alias unit.getAlias() is a member of the group.
		*/
		bool isMemberByAlias(const Unit& unit) const;

		/*!
		Returns true if a unit with the name unit.getName() is a member of the group.
		*/
		bool isMemberByName(const Unit& unit) const;

		/*!
		Returns true if a unit with the ID unit.getID() is a member of the group.
		*/
		bool isMemberByID(const Unit& unit) const;

		/*!
		Returns true if a unit with the alias strUnitAlias is a member of the group.
		*/
		bool isMemberByAlias(const std::string& strUnitAlias) const;

		/*!
		Returns true if a unit with the name strUnitName is a member of the group.
		*/
		bool isMemberByName(const std::string& strUnitName) const;

		/*!
		Returns true if a unit with the ID strUnitID is a member of the group.
		*/
		bool isMemberByID(const std::string& strUnitID) const;


		/*!
		Returns the member with the specified alias. Returns an empty unit if the unit
		is not a member of the group. 
		*/
		Unit getMemberByAlias(const std::string& strUnitAlias) const;

		/*!
		Returns the member with the specified name. Returns an empty unit if the unit
		is not a member of the group. 
		*/
		Unit getMemberByName(const std::string& strUnitName) const;

		/*!
		Returns the member with the specified ID. Returns an empty unit if the unit
		is not a member of the group. 
		*/
		Unit getMemberByID(const std::string& strUnitID) const;

		/*!
		Update the member with the alias given by unit.getAlias(). 
		*/
		void updateMemberByAlias(const Unit& unit);

		/*!
		Update the member with the name given by unit.getName(). 
		*/
		void updateMemberByName(const Unit& unit);

		/*!
		Update the member with the ID given by unit.getID(). 
		*/
		void updateMemberByID(const Unit& unit);


		/*!
		Set the current waypoint number of the group. 
		*/
		void setCurrentWaypoint(int number);

		/*!
		Return the current waypoint number of the group. 
		*/
		int getCurrentWaypoint() const;

		/*!
		Returns the Vehicle list of the Group
		*/
		vehicleList getVehicles() const;

		/*!
		Add a vehicle into the current Group
		*/
		void addVehicle(const Vehicle& vehicle);

		/*!
		Removes a vehicle from the vehicle list
		*/
		bool removeVehicle(const Vehicle& vehicle);

		/*!
		!clear the vehicle list of this group
		*/
		bool clearVehicleList();

		/*!
		Get the number of vehicles in the vehiclelist
		*/
		int getNumberOfVehicles() const;

		/*!
		Virtual function to handle WaypointComplete events from the group. This method is called
		by an EventHandler object in the case of an 'WaypointComplete' event being called. 

		A 'WaypointComplete' event handler for the relevant object should first be added
		using addWaypointCompleteEvent on a global EventHandler object. 

		Inherit from this class and rewrite the processRespawnEvent function 
		to provide customized functionality. 

		Current functionality does not do anything. Left empty for creating custom 
		behavior. 
		*/
		virtual void processWaypointCompleteEvent(WaypointCompleteEvent& _event);

		/*!
		Sets object is local or network object which has valid network id
		*/
		void setLocal(bool _boolLocal);

		/*!
		Returns true if object is local or false if object has valid network id
		*/
		bool isLocal() const;

		/*!
		Sets the VBS2 network ID of the object. 
		*/
		void setID(const GroupID& groupID);

		/*!
		Returns the VBS2 network ID of the object. 
		*/
		std::string getID() const;

		/*!
		Returns a string in the form of (idtoobj this->getID())
		which can be used to access the object within the 
		VBS2 environment. Similar to using the alias, but is more
		network and multi-player friendly. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network Id is invalid)	
		*/
		std::string getIdToObjString() const;


		/*!
		Assignment operator for Group class.
		Assign all the attributes of Group class.
		*/
		Group& operator=(const Group& group);

		/*!
		Equal operator for Group class. Compared by the name of the Group and an internal object 
		mapping between VBS2 and Fusion.
		*/
		bool operator==(const Group& group);

		/*!
		Less than operator for Group class.
		Compared by name.
		 */
		bool operator<(const Group& group) const;

		/*!
		 set the object map index (a unique id given to this group when created)
		 Users should not set this explicitly.
		 */
		void setObjMapIndex(int i);

		/*!
		Get the unique id set to the group
		*/
		int getObjMapIndex() const;

		/*!
		 returns true if the Group exists actually in VBS2
		 */
		bool isActivated() const;

		/*!
		 Set the activated status in Fusion side
		 */

		void setActivated(bool status);


		/*!
		 returns the name of the log file relevant to plug in
		 */
		std::string getLogFileName() const;


	private:		

		bool setLeader_A(const Unit& unit);

		void Initialize();

		//Members

		std::string _strName;
		std::string _strAlias;
		GroupID _ID;
		unitList _unitList;
		waypointList _waypointList;
		vehicleList _vehicle_List;
		FORMATION _Formation;
		SIDE _Side;
		std::string _strLeader;
		std::string _strLeaderAlias;
		std::string _strLeaderID;
		bool _boolHasLeader;
		bool _boolLocal;
		int _currentWaypoint;
		bool _bActivated;
		int _objMapIndex;
		std::string _logFileName;

	};
};

#pragma warning(pop) // Enable warnings [C:4251]

#endif //VBS2FUSION_GROUP_H