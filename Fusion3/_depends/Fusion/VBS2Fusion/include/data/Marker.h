/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	Marker.h

Purpose:

	This file contains the declaration of the Marker class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0		11-04/2011  CGS: Orginal Implementation

/************************************************************************/

#ifndef VBS2FUSION_MARKER_H
#define VBS2FUSION_MARKER_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusionDefinitions.h"
#include "position3D.h"
#include "VBSFusion.h"
#include "data/NetworkID.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{

	class VBSFUSION_API Marker
	{
	public:

		/*!
		The main constructor for the Marker class. Initializes the following:
		- _strName = ""; 
		- _strAlias="";
		*/
		Marker();

		/*!
		 Copy constructor
		 */
		Marker(const Marker& marker);

		/*!
		The main destructor for the class. 
		*/
		~Marker();

		/*!
		Set the VBS2 name of the Marker.
		*/
		void setName(const std::string& strName);

		/*!
		Return the name of the Marker.
		*/
		std::string getName() const;

		/*!
		Set the Position of the Marker. 
		*/
		void setPosition(const position3D& position);

		/*!
		Retrieve the Position of the Marker. 
		*/
		position3D getPosition() const;
		
		/*!
		Set the Marker type using string variable. It should be 
		one of the types specified in VBS. 
		*/
		void setMarkerType(const std::string& _strType);
		
		/*!
		Retrieve the Marker type as a string variable. 
		*/
		std::string getMarkerTypeString() const;
	
		/*!
		Set Marker width.
		*/
		void setWidth(double width);

		/*!
		Return Marker width.
		*/
		double getWidth() const;

		/*!
		Set Marker height.
		*/
		void setHeight(double height);

		/*!
		Return Marker height.
		*/
		double getHeight() const;

		/*!
		Set Marker text.
		*/
		void setText(const std::string& text);

		/*!
		Return Marker text.
		*/
		std::string getText() const;

		/*!
		Set Marker Color.
		*/
		void setColor(const std::string& color);

		/*!
		Return Marker Color.
		*/
		std::string getColor() const;

		/*!
		Set Marker Shape.
		*/
		void setShape(const std::string& shape);

		/*!
		Return Marker Shape.
		*/
		std::string getShape() const;

		/*!
		Set Marker Direction.
		*/
		void setDirection(double angle);

		/*!
		Return Marker Direction.
		*/
		double getDirection() const;

		/*!
		Set Marker Brush.
		*/
		void setBrush(const std::string& brush);

		/*!
		Return Marker Brush.
		*/
		std::string getBrush() const;

		/*!
		Set boolean determining whether the Marker is activated or not
		*/
		void setActivated(bool activated);

		/*!
		Returns the activation condition. This will return true if the Marker 
		has linked with VBS2 Marker object. 
		*/
		bool isActivated() const;

		/*!
		Sets object is local or network object which has valid network id
		*/
		void setLocal(bool _boolLocal);

		/*!
		Returns true if object is local or false if object has valid network id
		*/
		bool isLocal() const;

		/*
		 Assignment operator for Marker class.
		 Assign all the attributes of Marker class.
		*/
		Marker& operator=(const Marker& marker);
		
		/*
		Equal operator for Marker class. Compared by the name of the Marker and an internal object 
		mapping between VBS2 and Fusion.
		*/
		bool operator==(const Marker& marker);
 
		/*!
		 returns a unique it for a Marker
		 */
		int getObjMapIndex() const;

		/*!
		 set the unique Marker index
		*/
		void setObjMapIndex(int index);

		/*!
		 return the current log file name
		*/
		std::string getLogFileName() const;

	private:
		void Initialize();

		std::string _strName;
		std::string _logFileName;
		position3D _position;
		std::string _markertype;
		std::string _color;
		std::string _shape;
		double _angle;
		std::string _brush;
		bool _rectangular;
		double _width;
		double _height;
		std::string _text;
		bool _bActivated;	
		bool _boolLocal;
		int _objMapIndex;

	};

};

#endif

