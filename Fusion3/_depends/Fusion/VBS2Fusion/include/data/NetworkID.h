
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/


/*************************************************************************

Name:

	NetworkID.h

Purpose:

	This file contains the declaration of the NetworkID class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			08-12/2009	YFP: Original Implementation
	2.01		05-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_NETWORK_ID_H
#define VBS2FUSION_NETWORK_ID_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>
#include <BaseTsd.h>
// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBSFusion
{
	class VBSFUSION_API NetworkID
	{	
		/*!
			The Network ID contains three ID values.eg. [val1, val2 , val3].
				First value (_val1) : machine ID. 
				second value(_val2) : shared index of object. 
				Thrid value (_val3) : local  index of object.  
		*/
	public:
		/*!
			default constructor for NetworkID. 			
			variables are initialized as follows:
				  _val1 = -1 , _val2= -1, _val3 = -1. 
		*/
		NetworkID();

		/*!
			 constructor for three integer values.  
		*/
		NetworkID(int val1, int val2, int val3);

		/*!
			constructor for three float values.
		*/
#if 0
		NetworkID(double val1, double val2, double val3);
#elseif	
		NetworkID(float val1, float val2, float val3);
#endif
		/*!
			constructor for three string values. 
		*/
		NetworkID(const std::string& val1, const std::string& val2, const std::string& val3); 
		
		/*!
			constructor of network ID given by the string ID value. 
			Here the string ID will be tokenized into network id values.
		*/
		NetworkID(const std::string& IDString);
		
		/*!
			destructor for network ID.
		*/
		~NetworkID();

		/*!
			get the first string value of network ID. 
		*/
		std::string getVal1String() const ;

		/*!
			get the second string value of network ID. 
		*/
		std::string getVal2String() const ;

		/*!
			get the third string value of network ID. 
		*/
		std::string getVal3String() const ;

		/*!
			get NetworkID in string format. 
		*/
		std::string getNetworkIDString() const;

		/*!
			copy constructor 
		*/
		NetworkID(const NetworkID& value);
		/*!
			Assignment operator for NetworkID class.
			Assign the given NetworkID.
		*/
		NetworkID& operator= (const NetworkID& value);

		/*!
			Equal operator for NetworkID class.
		*/
		bool operator==(const NetworkID& value) const;

		/*!
			Less than operator for NetworkID class.

			If first valve is less it will return 'true'
			If first values are equal and second valve is less it will return 'true'
			If first and second valves are equal and third valve is less it will return 'true'
			Otherwise it will return 'false'
		*/
		bool operator < (const NetworkID& value) const;

		/*!
			set the integer first value. 
		*/
		void setVal1(int val);

		/*!
			set the integer second value. 
		*/
		void setVal2(int val);

		/*!
			set the integer third value. 
		*/
		void setVal3(int val);

		/*!
		  set the integer(INT64)
		*/
		void setVal3(INT64 val);

		/*!
			set the string first value. 
		*/
		void setVal1(const std::string& val); 
		
		/*!
			set the string second value.
		*/
		void setVal2(const std::string& val); 

		/*!
			set the string third value.
		*/
		void setVal3(const std::string& val); 
	
		/*!
			get first value of the Network ID.
		*/
		int getVal1() const ;

		/*!
			get second value of the Network value.
		*/
		int getVal2() const ;

		/*!
			get third value of the Network value.
		*/
		int getVal3() const ;	

		/*!
			Not equal operator for NetworkID class.
		*/
		bool operator != (const NetworkID& value) const;
	
	protected:
		int _val1;
		int _val2;
		INT64 _val3;
	};
};

#endif //VBS2FUSION_NETWORK_ID_H