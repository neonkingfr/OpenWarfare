/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

ArmedModule.h

Purpose:

This file contains the declaration of the NetworkID class.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			11-12/2009	YFP: Original Implementation
2.01		11-02/2010	MHA: Comments Checked

/************************************************************************/


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push) 
#pragma warning (disable: 4251)


#ifndef VBS2FUSION_ARMEDMODULE_H
#define VBS2FUSION_ARMEDMODULE_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "data/Turret.h"
#include "data/Magazine.h"
#include "data/Weapon.h"
#include "DataContainers/ObjectList.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBSFusion
{

	class VBSFUSION_API VBSFUSION_DPR(DARM) ArmedModule
	{
	public:

		/*!
			Type definition of the list that contains turrets.
		*/
		typedef std::list<Turret> TurretList;

		/*!
			Iterator for list containing turrets. 
		*/
		typedef std::list<Turret>::iterator t_iterator;

		/*!
			Constant iterator for turret list. 
		*/
		typedef std::list<Turret>::const_iterator t_const_iterator;

		/*!
			Type definition of the list that contains weapons.
		*/
		typedef std::list<Weapon> WeaponList;

		/*!
			Iterator for list that contains weapons.
		*/
		typedef std::list<Weapon>::iterator w_iterator;

		/*!
			Constant iterator for weapon list. 
		*/
		typedef std::list<Weapon>::const_iterator w_const_iterator;

		/*!
			Type definition of the list that contains magazines.
		*/
		typedef std::list<Magazine> MagazineList;

		/*!
			Iterator for list that contains magazines.
		*/
		typedef std::list<Magazine>::iterator m_iterator;	

		/*!
			Constant iterator for magazine list.
		*/
		typedef std::list<Magazine>::const_iterator m_const_iterator;
		
		/*!
			Armed Module constructor. 
		*/
		ArmedModule();

		/*!
			Armed Module Destructor. 
		*/
		~ArmedModule();

		/*!
			Copy constructor for Armed Module. 
		*/	
		ArmedModule(const ArmedModule& aModule);

		/*!
			Assignment operator for ArmedModule class.
			Assign all the attributes of ArmedModule class.
		*/		
		ArmedModule& operator = (const ArmedModule& aModule);

		/*!
			Begin iterator for unit list. 
		*/
		t_iterator turrets_begin();

		/*!
			Const Begin iterator for unit list. 
		*/
		t_const_iterator turrets_begin() const;

		/*!
			End iterator for unit list. 
		*/
		t_iterator turrets_end();

		/*!
			Const End iterator for unit list. 
		*/
		t_const_iterator turrets_end() const;	

		/*!
			begin iterator for weapon list.
		*/
		w_iterator weapons_begin();

		/*!
			begin const iterator for weapon list.
		*/
		w_const_iterator weapons_begin() const;

		/*!
			end iterator for weapon list.
		*/
		w_iterator weapons_end();

		/*!
			end iterator for weapon list.
		*/
		w_const_iterator weapons_end() const;

		/*!
			Begin iterator for magazine list.
		*/
		m_iterator magazines_begin();

		/*!
			Begin const iterator for magazine list.
		*/
		m_const_iterator magazines_begin() const;

		/*!
			End iterator for magazine list.
		*/
		m_iterator magazines_end();

		/*!
			End const iterator for magazine list.
		*/
		m_const_iterator magazines_end() const;

		/*!
			Add a turret to the armed module.
		*/
		void setTurret(const Turret& turret);

		/*!
			Get the main turret of the armed module. 
		*/
		Turret getMainTurret() const;

		/*!
			Get the turret by the specified path string.
		*/
		Turret getTurretByPathString(const std::string& turretPath);

		/*!
			Get the turret by the specified name. 
		*/
		Turret getTurretByName(const std::string& turretName);
		
		/*!
			Clears all turrets in the turret list.
		*/
		void clearTurretList();

		/*!
			Check weather the turret is available. 
		*/
		bool isTurretExists(const std::string& turretPath);

		/*!
			Add magazine to the armed module.
		*/
		void addMagazine(const Magazine& magazine);

		/*!
			Clear magazine list.
		*/
		void clearMagazineList();


	private:

		TurretList _turretList;

		WeaponList _weaponList;

		MagazineList _magazineList;

	};

};

#pragma warning (pop) // Enable warnings [C:4251]

#endif //ARMEDMODULE_H