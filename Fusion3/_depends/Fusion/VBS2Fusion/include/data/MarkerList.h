/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	MarkerList.h

Purpose:

	This file contains the declaration of the MarkerList class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			30-3/2009	RDJ: Original Implementation
	

/************************************************************************/

#ifndef VBS2FUSION_MARKER_LIST_H
#define VBS2FUSION_MARKER_LIST_H
/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4251)
#include "data/Marker.h"
namespace VBSFusion
{
	class VBSFUSION_API MarkerList
	{
	public:	
		/*!
		Typedef for vector list of Markers. 
		*/
		typedef std::vector<Marker> markerList;

		/*!
		Iterator for Markers.
		*/
		typedef std::vector<Marker>::iterator iterator;

		/*!
		Const Iterator for Markers.
		*/
		typedef std::vector<Marker>::const_iterator const_iterator;	

		/*!
		Main constructor for MarkerList class. 
		*/
		MarkerList();

		/*!
		Main destructor for MarkerList class. 
		*/
		~MarkerList();

		/*!		
		Adds a Marker to the list.	
		@param marker - Marker Object
		@return Nothing.		
		*/		
		void addMarker(const Marker& marker);	

		/*!
		Adds a new Markers to the list and assigns name strMarkerName. 
		@param strMarkerName - Marker name
		@return Nothing.
		*/
		void addMarker(const std::string& strMarkerName);

		/*!
		Returns true if a marker with the name marker.getName()
		exists on the marker list. 
		@param marker - Marker 
		@return bool
		*/
		bool isMember(const Marker& marker)const;

		/*!
		Returns true if a marker with the name strMarkerName
		exists on the marker list.
		@param strMarkerName - string 
		@return bool
		*/
		bool isMember(const std::string& strMarkerName)const;
		
		/*!
		Removes marker defined by marker. Returns false if marker
		is not a member of the marker list. 
		@param marker - marker 
		@return bool marker is deleted or not
		*/
		bool removeMarker(const Marker& marker);
		
		/*!
		Removes marker defined by marker. Returns false if marker
		is not a member of the marker list. 
		@param strMarkerName - string marker name
		@return bool marker is deleted or not
		*/
		bool removeMarker(const std::string& strMarkerName);

		/*!
		Clears the marker list. 
		@param nothing
		@return nothing
		*/
		void clearMarkers();

		/*!
		Returns the number of Markers. 
		@param nothing
		@return int - Number of markers in the list
		*/
		int getNumberOfMarkers() const;

		/*!
		Start of marker list. i.e. begin iterator. 
		*/
		iterator begin();

		/*!
		Start of marker list. i.e. begin const iterator. 
		*/
		const_iterator begin() const;

		/*!
		End of marker list. i.e. end iterator. 
		*/
		iterator end();

		/*!
		end of marker list. i.e. const end iterator. 
		*/
		const_iterator end() const;


	private:

		markerList _markers;
		
	};

};
#pragma warning (pop)
#endif