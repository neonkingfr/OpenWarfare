/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

UnitArmedModule.h

Purpose:

This file contains the declaration of the UnitArmedModule class.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			07-02/2011	CGS: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_UNITARMEDMODULE_H
#define VBS2FUSION_UNITARMEDMODULE_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "data/Weapon.h"
#include "DataContainers/ObjectList.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBSFusion
{

	class VBSFUSION_API UnitArmedModule
	{
	public:

		/*!
			Type definition of the list that contains weapons.
		*/
		typedef std::list<Weapon> WeaponList;

		/*!
			Iterator for list that contains weapons.
		*/
		typedef std::list<Weapon>::iterator w_iterator;

		/*!
			Constant iterator for weapon list. 
		*/
		typedef std::list<Weapon>::const_iterator w_const_iterator;

		/*!
			Type definition of the list that contains magazines.
		*/
		typedef std::list<Magazine> MagazineList;

		/*!
			Iterator for list that contains magazines.
		*/
		typedef std::list<Magazine>::iterator m_iterator;	

		/*!
			Constant iterator for magazine list.
		*/
		typedef std::list<Magazine>::const_iterator m_const_iterator;

		//**********************************************************************

		/*!
			Main constructor for UnitArmedModule class.
		*/
		UnitArmedModule();

		/*!
			Main Destructor for UnitArmedModule class. 
		*/
		~UnitArmedModule();

		/*!
		Copy constructor for the UnitArmedModule class. 
		*/
		UnitArmedModule(const UnitArmedModule& uaModule);

		/*!
			Assignment operator for UnitArmedModule class.
			Assign all the attributes of UnitArmedModule class.
		*/
		UnitArmedModule& operator = (const UnitArmedModule& uaModule);
		
		/*!
			begin iterator for weapon list.
		*/
		w_iterator weapons_begin();

		/*!
			begin const iterator for weapon list.
		*/
		w_const_iterator weapons_begin() const;

		/*!
			end iterator for weapon list.
		*/
		w_iterator weapons_end();

		/*!
			end iterator for weapon list.
		*/
		w_const_iterator weapons_end() const;

		/*!
			Begin iterator for magazine list.
		*/
		m_iterator magazines_begin();

		/*!
			Begin const iterator for magazine list.
		*/
		m_const_iterator magazines_begin() const;

		/*!
			End iterator for magazine list.
		*/
		m_iterator magazines_end();

		/*!
			End const iterator for magazine list.
		*/
		m_const_iterator magazines_end() const;

		/*!
			Add magazine to the armed module.
		*/
		void addMagazine(const Magazine& magazine);

		/*!
			Add magazine to the armed module using magazine name.
		*/
		void addMagazine(const std::string& magazineName);

		/*!
			Clear magazine list.
		*/
		void clearMagazineList();

		/*!
		Returns the number of magazine types currently assigned. 
		*/
		int getNumberOfMagazines() const;

		/*!
		Removes magazine with class name magazineName from list. Returns false if
		operation was unsuccessful due to the magazine not being on the list. 
		*/
		bool removeMagazine(const std::string& magazineName);

		/*!
		Returns true of a magazine with the class name magazineName
		is on the list. 
		*/
		bool isMagazineAdded(const std::string& magazineName);

		/*!
			Set primary weapon to the armed module.
		*/
		void setPrimaryWeapon(const std::string& weaponName);

		/*!
			get primary weapon to the armed module
		*/
		std::string getPrimaryWeapon() const; 

		/*!
		Adds a new weapon using weapon object.
		*/
		void addWeapon(const Weapon& weapon);

		/*!
		Adds a new weapon using a class name. Sets the ammo value to 0 for the 
		newly created weapon. 
		*/
		void addWeapon(const std::string& weaponsName);
		
		/*!
		Clears the weapons list. 
		*/
		void clearWeaponsList();

		/*!
		Returns the number of weapons currently assigned. 
		*/
		int getNumberOfWeapons() const;

		/*!
		Removes weapon with class name weaponName from list. Returns false if
		operation was unsuccessful due to the weapon not being on the list. 
		*/
		bool removeWeapon(const std::string& weaponName);


	private:

		std::string primaryWeapon;
		WeaponList _weaponList;
		MagazineList _magazineList;

	};

};

#endif //ARMEDMODULE_H