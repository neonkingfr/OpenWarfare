/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	Target.h

Purpose:

	This file contains the declaration of the Target class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			22-08/2011	NDB: Original Implementation
	

/************************************************************************/

#ifndef VBS2FUSION_TARGET_H
#define VBS2FUSION_TARGET_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusionDefinitions.h"
#include "data/ControllableObject.h"


/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{

	class VBSFUSION_API Target
	{

	public:

		/*!
		The main constructor for the Target class. Initializes the following:
		- 
		*/
		Target();

		/*!
		Copy constructor of Target data class
		 */
		Target(const Target& target);

		/*!
		The destructor for the class. 
		*/
		~Target();

		/*!
		sets the position of target
		*/
		void setPosition(const position3D& pos);

		/*!
		returns the position of the target
		*/
		position3D getPosition() const;

		/*!
		sets the type of the target
		*/
		void setType(const std::string& type);
		
		/*!
		returns the type of the target
		*/
		std::string getType() const;

		/*!
		Sets side of the target by enum SIDE
		It can be: WEST, EAST, CIVILIAN, RESISTANCE
		*/
		void setSide(SIDE side);

		/*!
		Sets side of the target by string
		It can be: "WEST", "EAST", "CIVILIAN", "RESISTANCE"
		*/
		void setSide(const std::string& side);

		/*!
		returns the side of the target as enum
		*/
		SIDE getTargetSide() const;

		/*!
		returns the side of the target as string
		*/
		std::string getTargetSideStr() const;
		
		/*!
		sets the subjective cost of the target
		*/
		void setSubjectiveCost(double cost);

		/*!
		returns the subjectiveCost
		*/
		double getSubjectiveCost() const;

		/*!
		sets the object
		*/
		void setObject(const ControllableObject& object);

		/*!
		returns the object
		*/
		ControllableObject getObject() const;

		/*!
		sets the position accuracy
		*/
		void setPositionAccuracy(double accuracy);

		/*!
		returns the position accuracy
		*/
		double getPositionAccuracy() const;

		/*!
		Assignment operator for Target class.
		Assign all the attributes of Target class.
		*/
		Target& operator=(const Target& u);

	private:
		//void Initialize();

		position3D _position;
		std::string _type;
		SIDE _side;
		double _subjectiveCost;
		ControllableObject _object;
		double _positionAccuracy;

	};

};

#endif //TARGET_H

