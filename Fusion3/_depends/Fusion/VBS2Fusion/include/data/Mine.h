
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name: Mine.h

Purpose:
	This file contains the declaration of the Mine class.
	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			05-04/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_MINE_H
#define VBS2FUSION_MINE_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "Explosive.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBSFusion
{
	class VBSFUSION_API Mine : public Explosive
	{
	public:
		
		/*!
		Main constructor for this class. 
		The following parameters are initialized as follows:
		MINETYPE = VBS2_MINEAT_M15
		placementradius = 0	
		*/
		Mine();
		
		/*!
		Constructor for the Mine class. 
		Variables are initialized as follows:
		 MINETYPE = VBS2_MINEAT_M15
		 placementradius = 0		
		*/
		Mine(const position3D& pos);

		/*!
		constructor for the Mine class. 
		Variables are initialized as follows:
		- placementradius = 0		
		*/
		Mine(MINETYPE type, const position3D& pos);

		/*!
		constructor for the Mine class. 
		*/
		Mine(MINETYPE type, const position3D& pos, double placementRadius);

		/*!
		Main destructor for the the Mine class. 
		*/
		~Mine();
		
		/*!
		Sets the type of the mine using a MINETYPE variable. Should be one
		of (VBS2_MINEAT_M15, VBS2_MINEAT_M19, VBS2_MINEAT_TM46, VBS2_MINEAT_TM62M)
		*/
		void setMineType(MINETYPE mineType);

		/*!
		Sets the type of the group in string format. Should be one
		of (VBS2_MINEAT_M15, VBS2_MINEAT_M19, VBS2_MINEAT_TM46, VBS2_MINEAT_TM62M)
		*/
		void setMineType(const std::string& mineType);

		/*!
		Returns the type of the mine using a MINETYPE variable. Should be one
		of (VBS2_MINEAT_M15, VBS2_MINEAT_M19, VBS2_MINEAT_TM46, VBS2_MINEAT_TM62M)
		*/
		MINETYPE getMineType() const;

		/*!
		Returns the type of the group in string format. Should be one
		of ("vbs2_mineAT_M15", "vbs2_mineAT_M19", "vbs2_mineAT_TM46", "vbs2_mineAT_TM62M")
		*/
		std::string getMineTypeString() const;

		/*!
		Sets the placement radius. 
		*/
		void setPlacementRadius(double placementRadius);

		/*!
		Returns the placement radius. 
		*/
		double getPlacementRadius() const;
	
	private: 
		double _dPlacementRadius;
		MINETYPE _mineType;

		/*!
		Init function that is used for initialize the Mine object. This function adds:
			 _dPlacementRadius	= 0.0
			 _mineType = VBS2_MINEAT_M15 			
		*/
		void initMine();
	};
}

#endif //VBS2FUSION_MINE_H