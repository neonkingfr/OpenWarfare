/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	Trigger.h

Purpose:

	This file contains the declaration of the Trigger class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			30-3/2009	RMR: Original Implementation
	1.01		27-8/2009	SDS: Added setLocal(bool _isLocal)
								 Added isLocal()
								 updated to NetworkId base referencing
	2.0			23-12/2009	UDW: operator=, operator==, getObjMapIndex(), getObjMapIndex(),
								 setObjMapIndex()
	2.01		05-2/2010	MHA: Comments Checked
	2.02		10-01/2011  CGS: Added setID(NetworkID nid)
								 Modified setID(string strID)
									setAlias(string strAlias)
									setName(string strName)


/************************************************************************/

#ifndef VBS2FUSION_TRIGGER_H
#define VBS2FUSION_TRIGGER_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusionDefinitions.h"
#include "position3D.h"
#include "VBSFusion.h"
#include "data/NetworkID.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBSFusion
{

	class VBSFUSION_API Trigger
	{
	public:

		/*!
		 A type defining TriggerID
		 */
		typedef NetworkID TriggerID;
		
		/*!
		The main constructor for the Trigger class. Initializes the following:
		- _strName = ""; 
		- _strAlias="";
		*/
		Trigger();

		/*!
		 Copy constructor
		 */
		Trigger(const Trigger& trigger);

		/*!
		The main destructor for the class. 
		*/
		~Trigger();

		/*!
		Set the VBS2 name of the trigger.
		*/
		void setName(const std::string& strName);

		/*!
		Return the name of the trigger.
		*/
		std::string getName() const;

		/*!
		Set the VBS2Fusion alias of the trigger. 
		*/
		void setAlias(const std::string& strAlias);

		/*!
		Retrieve the VBS2Fusion alias of the trigger. 
		*/
		std::string getAlias() const;

		/*!
		Set the Position of the trigger. 
		*/
		void setPosition(const position3D& position);

		/*!
		Retrieve the Position of the trigger. 
		*/
		position3D getPosition() const;


		/*!
		 Set the trigger activation type using a TRIGGERACTACTIVATION variable. 
		 It should be one of the following: (TNONE, TEAST, TWEST, TGUER, TCIV, 
		 TLOGIC, TANY, TALPHA, TBRAVO, TCHARLIE, TDELTA, TECHO, TFOXTROT, TGOLF, 
		 THOTEL, TINDIA, TJULIET, TSTATIC, TVEHICLE, TGROUP, TLEADER, TMEMBER,
		 TWEST_SEIZED, TEAST_SEIZED, TGUER_SEIZED)
		*/
		void setTriggerActivation(TRIGGERACTIVATION _type);

		/*!
		Set the trigger activation type using a string. 
		It should be one of the following: ("NONE", "EAST", "WEST", "GUER", "CIV", 
		"LOGIC", "ANY", "ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", 
		"HOTEL", "INDIA", "JULIET", "STATIC", "VEHICLE", "GROUP", "LEADER", "MEMBER",
		"WEST SEIZED", "EAST SEIZED", "GUER SEIZED")
		*/
		void setTriggerActivation(const std::string& _strType);

		/*!
		Get the trigger activation type as a TRIGGERACTACTIVATION variable. 
		It should be one of the following: (TNONE, TEAST, TWEST, TGUER, TCIV, 
		TLOGIC, TANY, TALPHA, TBRAVO, TCHARLIE, TDELTA, TECHO, TFOXTROT, TGOLF, 
		THOTEL, TINDIA, TJULIET, TSTATIC, TVEHICLE, TGROUP, TLEADER, TMEMBER,
		TWEST_SEIZED, TEAST_SEIZED, TGUER_SEIZED)
		*/
		TRIGGERACTIVATION getTriggerActivation() const;

		/*!
		Get the trigger activation type using a string. 
		It should be one of the following: ("NONE", "EAST", "WEST", "GUER", "CIV", 
		"LOGIC", "ANY", "ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", 
		"HOTEL", "INDIA", "JULIET", "STATIC", "VEHICLE", "GROUP", "LEADER", "MEMBER",
		"WEST SEIZED", "EAST SEIZED", "GUER SEIZED")
		*/
		std::string getTriggerActivationString() const;

		/*!
		Set the trigger re-sence type using a TRIGGERACTPRESENCE variable. 
		It should be one of the following: (PRESENT, NOT_PRESENT, WEST_D, EAST_D, GUER_D, CIV_D)
		*/
		void setTriggerPresence(TRIGGERPRESENCE _type);

		/*!
		Set the trigger re-sence type using a string. 
		It should be one of the following: ("PRESENT", "NOT PRESENT", 
		"WEST D", "EAST D", "GUER D", "CIV D")
		*/
		void setTriggerPresence(const std::string& _strType);

		/*!
		Retrieve the trigger re-sence type as TRIGGERACTPRESENCE variable. 
		It should be one of the following: (PRESENT, NOT_PRESENT, WEST_D, EAST_D, GUER_D, CIV_D)
		*/
		TRIGGERPRESENCE getTriggerPresence() const;

		/*!
		Retrieve the trigger re-sence type using a string. 
		It should be one of the following: ("PRESENT", "NOT PRESENT", 
		"WEST D", "EAST D", "GUER D", "CIV D")
		*/
		std::string getTriggerPresenceString() const;
		
		/*!
		Set the trigger re-sence type using a TRIGGERTYPE variable. 
		It should be one of the following: (TTNONE, SWITCH, END1, END2, 
		END3, END4, END5, END6, LOOSE, WIN)
		*/

		void setTriggerType(TRIGGERTYPE _type);

		/*!
		Set the trigger re-sence type using a string variable. 
		It should be one of the following: ("NONE", "SWITCH", "END1", "END2", 
		"END3", "END4", "END5", "END6", "LOOSE", "WIN")
		*/
		void setTriggerType(const std::string& _strType);

		/*!
		Retrieve the trigger re-sence type as a TRIGGERTYPE variable. 
		It should be one of the following: (TTNONE, SWITCH, END1, END2, 
		END3, END4, END5, END6, LOOSE, WIN)
		*/
		TRIGGERTYPE getTriggerType() const;

		/*!
		Retrieve the trigger re-sence type as a string variable. 
		It should be one of the following: ("NONE", "SWITCH", "END1", "END2", 
		"END3", "END4", "END5", "END6", "LOOSE", "WIN")
		*/
		std::string getTriggerTypeString() const;

		/*!
		Set boolean determining whether the trigger is repeating or not. 
		*/
		void setRepeating(bool repeating);

		/*!
		Return boolean determining whether the trigger is repeating or not. 
		*/
		bool getRepeating() const;

		/*!
		Set boolean determining whether the trigger is rectangular or not. 
		*/
		void setRectangular(bool rectangular);

		/*!
		Return boolean determining whether the trigger is rectangular or not. 
		*/
		bool getRectangular() const;

		/*!
		Set trigger area width.
		*/
		void setWidth(double width);

		/*!
		Return trigger area width.
		*/
		double getWidth() const;

		/*!
		Set trigger area height.
		*/
		void setHeight(double height);

		/*!
		Return trigger area height.
		*/
		double getHeight() const;

		/*!
		Set trigger angle.
		*/
		void setAngle(double angle);

		/*!
		Return trigger angle.
		*/
		double getAngle() const;

		/*!
		Set trigger statements.
		*/
		void setStatements(const std::string& statements);

		/*!
		Return trigger statements.
		*/
		std::string getStatements() const;

		/*!
		Set trigger statements condition.
		condition: String - Code containing the trigger's condition, 
		which has to return a boolean value. If this is used, 
		the result of the trigger's [activation condition] is interpreted. 
		*/
		void setStatementsCond(const std::string& statements);

		/*!
		Return trigger statements condition.
		condition: String - Code containing the trigger's condition, 
		which has to return a boolean value. If this is used, 
		the result of the trigger's [activation condition] is interpreted. 
		*/
		std::string getStatementsCond() const;

		/*!
		Set trigger statements activation condition.
		activation: String - Code that is executed when the trigger is activated 
		(The variable thislist contains an array with the units that activated the trigger.)
		*/
		void setStatementsActiv(const std::string& statements);

		/*!
		Return trigger statements activation condition.
		activation: String - Code that is executed when the trigger is activated 
		(The variable thislist contains an array with the units that activated the trigger.)
		*/
		std::string getStatementsActiv() const;

		/*!
		Set trigger statements deactivation condition.
		deactivation: String - Code that is executed when the trigger is deactivated.
		*/
		void setStatementsDesactiv(const std::string& statements);

		/*!
		Return trigger statements deactivation condition.
		deactivation: String - Code that is executed when the trigger is deactivated.
		*/
		std::string getStatementsDesactive() const;

		/*!
		Returns statements in VBS2 format. i.e. "[cond, activ, desactiv]"
		*/
		std::string getStatementsInVBSFormat() const;

		/*!
		Set trigger text.
		*/
		void setText(const std::string& text);

		/*!
		Return trigger text.
		*/
		std::string getText() const;

		/*!
		Set trigger timeout max.
		*/
		void setTimeoutMax(double val);

		/*!
		Return trigger timeout max.
		*/
		double getTimeoutMax() const;

		/*!
		Set trigger timeout mid.
		*/
		void setTimeoutMid(double val);

		/*!
		Return trigger timeout mid.
		*/
		double getTimeoutMid() const;

		/*!
		Set trigger timeout min.
		*/
		void setTimeoutMin(double val);

		/*!
		Return trigger timeout min.
		*/
		double getTimeoutMin() const;

		/*!
		Set boolean determining whether the trigger is activated or not
		*/
		void setActivated(bool activated);

		/*!
		Return boolean determining whether the trigger is activated or not
		*/
		bool getActivated() const;

		/*!
		Set boolean determining whether the trigger is interruptible or not
		*/
		void setInterruptable(bool interruptable);

		/*!
		Return boolean determining whether the trigger is interruptible or not
		*/
		bool getInterruptable() const;

		/*!
		Returns true if the trigger has linked with VBS2 trigger object. 
		*/
		bool isActivated() const;

		/*!
		Sets link status between the trigger and the VBS2 trigger object. 
		Note : Does not has an effect on the actual link.
		*/
		void setbActivated(bool status);
	
		/*!
		Returns trigger activation in VBS2 format. i.e. [by, type, repeating]
		*/
		std::string getTriggerActivationInVBSFormat() const;

		/*!
		Returns trigger area in VBS2 format. i.e. [a, b, angle, rectangle]
		*/
		std::string getTriggerAreaInVBSFormat() const;

		/*!
		Returns trigger timeout in VBS2 format. i.e. [min, mid, max, interruptable]
		*/
		std::string getTriggerTimeoutInVBSFormat() const;

		/*!
		Sets object is local or network object which has valid network id
		*/
		void setLocal(bool _boolLocal);

		/*!
		Returns true if object is local or false if object has valid network id
		*/
		bool isLocal() const;

		/*!
		Sets the VBS2 network ID of the object. 
		*/
		void setID(const std::string& triggerID);

        /*!
		Sets the VBS2 network ID of the object.
		*/
		void setID(const NetworkID& nid);

		/*!
		Returns the string VBS2 network ID of the object. 
		*/
		std::string getID() const;

		/*!
		Returns the VBS2 network ID of the object. 
		*/
		NetworkID getNetworkID() const;

		/*!
		Returns a string in the form of (idtoobj this->getID())
		which can be used to access the object within the 
		VBS2 environment. Similar to using the alias, but is more
		network and multi-player friendly. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network Id is invalid)	
		*/

		std::string getIdToObjString() const;

		/*
		 Assignment operator for Trigger class.
		 Assign all the attributes of Trigger class.
		 */
		Trigger& operator=(const Trigger& _trigger);
		
		/*
		Equal operator for Trigger class. Compared by the name of the Trigger and an internal object 
		mapping between VBS2 and Fusion.
		*/
		bool operator==(const Trigger& _trigger);
 
		/*!
		 returns a unique it for a Trigger
		 */
		int getObjMapIndex() const;

		/*!
		 set the unique Trigger id
		*/
		void setObjMapIndex(int index);

		/*!
		 set the unique Trigger id. It uses setObjMapIndex(int index) function inside.
		*/
		void setObjtMapIndex(int index);

		/*!
		 return the current log file name
		*/
		std::string getLogFileName() const;

	private:
		void Initialize();

		std::string _strName;
		std::string _strAlias;
		std::string _strID;
		std::string _logFileName;
		position3D _position;
		TRIGGERACTIVATION _triggeractivation;
		TRIGGERPRESENCE _triggerpresence;
		TRIGGERTYPE _triggertype;
		bool _repeating;
		bool _rectangular;
		double _width;
		double _height;
		double _angle;
		std::string _statements;
		std::string _statementsCond;
		std::string _statementsActiv;
		std::string _statementsDesactiv;
		std::string _text;
		double _timeoutMax;
		double _timeoutMid;
		double _timeoutMin;
		bool _activated;	
		bool _interruptable;
		bool _boolLocal;
		TriggerID _ID;

		int _objMapIndex;
		bool _bActivated;
	};
};

#endif //TRIGGER_H

