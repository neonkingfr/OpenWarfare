/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	GroundVehicle.h

Purpose:

	This file contains the declaration of the NetworkID class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			11-12/2009	YFP: Original Implementation
	2.01		11-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_GROUNDVEHICLE_H
#define VBS2FUSION_GROUNDVEHICLE_H

#pragma warning (push)
#pragma warning (disable: 4996)

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "data/Vehicle.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBSFusion
{
	class VBSFUSION_API VBSFUSION_DPR(DGRV) GroundVehicle: public Vehicle
	{
	public:
		GroundVehicle();

		virtual ~GroundVehicle();

		void initialize();

		GroundVehicle(const GroundVehicle& vehicle);

		/*!
			Assignment operator for GroundVehicle class. Assign all the attributes of GroundVehicle class.
			Also call the assignment operator of Vehicle class.
		*/
		GroundVehicle& operator =(const GroundVehicle& vehicle);

		/*!
			Less than operator for GroundVehicle class.
			Compared by NetworkID.
		*/
		bool operator < (const GroundVehicle& vehicle) const;

		/*!
			Equal operator for GroundVehicle class.
			Compared by NetworkID.
		*/
		bool operator ==(const GroundVehicle& vehicle);
	};

};

#pragma warning (pop) // Enable warnings [C:4996]

#endif //GROUNDVEHICLE_H