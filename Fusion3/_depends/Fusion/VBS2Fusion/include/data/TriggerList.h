/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	TriggerList.h

Purpose:

	This file contains the declaration of the TriggerList class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			30-3/2009	RMR: Original Implementation
	2.0			12-09/2009	UDW: enums move out
	2.01		11-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_TRIGGER_LIST_H
#define VBS2FUSION_TRIGGER_LIST_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push) 
#pragma warning (disable: 4251)


#include <sstream>
#include <string>

#include "position3D.h"
#include "VBSFusion.h"
#include "data/Trigger.h"

namespace VBSFusion
{
	class VBSFUSION_API TriggerList
	{
	public:	
		/*!
		Typedef for vector list of Triggers. 
		*/
		typedef std::vector<Trigger> triggerList;

		/*!
		Iterator for triggers.
		*/
		typedef std::vector<Trigger>::iterator iterator;

		/*!
		Const Iterator for triggers.
		*/
		typedef std::vector<Trigger>::const_iterator const_iterator;	

		/*!
		Vector list of strings. 
		*/
		typedef std::vector<std::string> strTriggerList;

		/*!
		Main constructor for TriggerList class. 
		*/
		TriggerList();

		/*!
		Main destructor for TriggerList class. 
		*/
		~TriggerList();

		/*!
		Adds a Triggers to the list. 
		*/
		void addTrigger(const Trigger& trigger);	

		/*!
		Adds a new Triggers to the list and assigns name strTriggerName. 
		*/
		void addTrigger(const std::string& strTriggerName);

		/*!
		Removes trigger defined by trigger. Returns false if trigger
		is not a member of the trigger list. 
		*/
		bool removeTrigger(const Trigger& trigger);

		/*!
		Removes trigger with name strTriggerName. Returns false if trigger
		is not a member of the trigger list. 
		*/
		bool removeTrigger(const std::string& strTriggerName);

		/*!
		Returns true if a trigger with the name trigger.getName()
		exists on the trigger list. 
		*/
		bool isMember(const Trigger& trigger);

		/*!
		Returns true if a trigger with the name strTriggerName
		exists on the trigger list. 
		*/
		bool isMember(const std::string& strTriggerName);

		/*!
		Start of trigger list. i.e. begin iterator. 
		*/
		iterator begin();

		/*!
		Start of trigger list. i.e. begin const iterator. 
		*/
		const_iterator begin() const;

		/*!
		End of trigger list. i.e. end iterator. 
		*/
		iterator end();

		/*!
		end of trigger list. i.e. const end iterator. 
		*/
		const_iterator end() const;

		/*!
		Clears the trigger list. 
		*/
		void clearTriggers();

		/*!
		Returns the number of triggers. 
		*/
		int getNumberOfTriggers() const;

	private:

		triggerList _triggers;
		strTriggerList _strTriggers;
	};
};

#pragma warning(pop) // Enable warnings [C:4251]

#endif //TRIGGER_LIST_H