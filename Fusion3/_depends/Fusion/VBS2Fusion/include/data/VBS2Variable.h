/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	VBS2Variable.h

Purpose:

	This file contains the declaration of the VBS2Variable class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			11-01/2010	RMR: Original Implementation
	2.01		11-02/2010	MHA: Comments Checked
	
/************************************************************************/

#ifndef VBS2_VARIABLE_H
#define VBS2_VARIABLE_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "position3D.h"

#include "VBSVariable.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBSFusion
{

	struct VBSFUSION_API VBSFUSION_DPR(DVAR) VBS2Var : public VBSVar
	{
		//union VBS2NumericVar
		//	{
		//		int i;
		//		float f;
		//		double d;				
		//		bool b;		
		//	} numeric;
		//string s;
		//position3D pos;
	};

	class VBSFUSION_API VBSFUSION_DPR(DVRBL) VBS2Variable : public VBSVariable
	{

	public:

		/*!
		Defines the types of variables which can be stored within a VBS2Variable class. Currently
		supported types are:

		- string (STRING)
		- int (INT)
		- float (FLOAT)
		- double (DOUBLE)
		- bool (BOOL)
		- position3D (POSITION)
		- No value stored in variable (NONE)

		*/
		//enum VBS2VariableType{STRING, INT, FLOAT, DOUBLE, BOOL, POSITION, NONE};
		typedef VBSVariableType VBS2VariableType;

		/*!
		The default constructor for the VBS2Variable class. 
		*/
		VBS2Variable();

		/*!
		Copy constructor for the VBS2Variable class. 
		*/
		VBS2Variable (const VBS2Variable &value);
		
		/*!
		Default destructor for the VBS2Variable class. 
		*/
		~VBS2Variable();

		/*!
		Returns the value stored in the VBS2Variable in the form of a VBs2Var structure variable. 
		*/
		VBS2Var getData() const ;


		/*!
		Constructor for string variables. 
		*/
		VBS2Variable(const std::string& strValue);		

		/*!
		Constructor for int variables. 
		*/
		VBS2Variable(int intValue);		

		/*!
		Constructor for float variables. 
		*/
		VBS2Variable(float floatValue);		

		/*!
		Constructor for double variables. 
		*/
		VBS2Variable(double doubleValue);

		/*!
		Constructor for bool variables. 
		*/
		VBS2Variable(bool boolValue);

		/*!
		Constructor for position3D variables. 
		*/
		VBS2Variable(const position3D& pos);

		/*!
		Sets the type of the variable. Recommend not using this function unless absolutely necessary as the setVariable and relevant constructors
		set this value automatically. 
		*/
		void setType(const VBS2VariableType& type);
		
		/*!
		Returns the type of the variable stored in the form of a VBS2VariableType. 
		*/
		VBS2VariableType TypeOf() const;


		/*!
		Operator to set the value of the VBS2Variable to a boolean. Type is automatically set. 
		*/
		VBS2Variable& operator = (bool value);

		/*!
		Operator to set the value of the VBS2Variable to an int. Type is automatically set. 
		*/
		VBS2Variable& operator = (int value);

		/*!
		Operator to set the value of the VBS2Variable to a double. Type is automatically set. 
		*/
		VBS2Variable& operator = (double value);

		/*!
		Operator to set the value of the VBS2Variable to a float. Type is automatically set. 
		*/
		VBS2Variable& operator = (float value);

		/*!
		Operator to set the value of the VBS2Variable to a string. Type is automatically set. 
		*/
		VBS2Variable& operator = (const std::string& value);

		/*!
		Operator to set the value of the VBS2Variable to a position3D. Type is automatically set. 
		*/
		VBS2Variable& operator = (const position3D& pos);

		/*!
		Operator to retrieve the value stored within the VBSVariable as an int. 
		*/
		operator int() const;

		/*!
		Operator to retrieve the value stored within the VBSVariable as a bool. 
		*/
		operator bool() const;

		/*!
		Operator to retrieve the value stored within the VBSVariable as a float. 
		*/
		operator float() const;

		/*!
		Operator to retrieve the value stored within the VBSVariable as double. 
		*/
		operator double() const;

		/*!
		Operator to retrieve the value stored within the VBSVariable as a string. 
		*/
		operator std::string() const;

		/*!
		Operator to retrieve the value stored within the VBSVariable as a position3D. 
		*/
		operator position3D() const;
		/*!
		Sets the value of the variable using an int. Type is automatically set. 
		*/
		void setValue(int intValue);		

		/*!
		Sets the value of the variable using a string. Type is automatically set. 
		*/
		void setValue(const std::string& stringValue);

		/*!
		Sets the value of the variable using a double. Type is automatically set. 
		*/
		void setValue(double doubleValue);

		/*!
		Sets the value of the variable using a float. Type is automatically set. 
		*/
		void setValue(float floatValue);

		/*!
		Sets the value of the variable using a bool. Type is automatically set. 
		*/
		void setValue(bool boolValue);

		/*!
		Sets the value of the variable using a position3D. Type is automatically set. 
		*/
		void setValue(const position3D& pos);


	//private:
	//	VBS2Var _value;
	//	VBS2VariableType _type;
	};
}
#endif // VBS2_VARIABLE_H