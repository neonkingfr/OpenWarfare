/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

Weapon.h

Purpose:

This file contains the declaration of the Weapon class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			17-12/2009	YFP: Original Implementation

2.0			10-02/2010  YFP: Added	list for muzzles
									operators = , == , < 
									typedef list<string> MuzzleList
									typedef list<string>::iterator iterator
									typedef list<string>::const_iterator const_iterator
									setTypeName()
									getTypeName()
									iterator muzzle_begin() , const_iterator  muzzle_begin()
									iterator muzzle_end() , const_iterator  muzzle_end()
									addMuzzle()
									clearMuzzleList()
									removeMuzzle()
2.01		11-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_WEAPON_H
#define VBS2FUSION_WEAPON_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push) 
#pragma warning (disable: 4251)


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>
#include <list>

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "Magazine.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBSFusion
{
	class VBSFUSION_API Weapon
	{

	public:
		/*!
			type definition for muzzle list.
		*/
		typedef std::list<std::string> MuzzleList;

		/*!
			type definitions for muzzle list iterator.
		*/
		typedef std::list<std::string>::iterator iterator;

		/*!
			type definition for muzzle list constant iterator.
		*/
		typedef std::list<std::string>::const_iterator const_iterator;

 		/*!
		Type definition for list contains magazines.
		*/
		typedef std::list<Magazine> MagazineList;

		/*!
		Iterator for Magazine list.
		*/
		typedef std::list<Magazine>::iterator mag_iterator;

		/*!
		Constant iterator for magazine list.
		*/
		typedef std::list<Magazine>::const_iterator mag_const_iterator;
			
		/*!
			Constructor  for weapon class.
		*/
		Weapon();

		/*!
			Copy constructor. 
		*/
		Weapon(const Weapon& weapon);

		/*!
			Destructor  for weapon class. 
		*/
		~Weapon();

		/*!
			set the name of the Weapon.
		*/
		void setWeaponName(const std::string& name);

		/*!
			get name form the weapon.
		*/
		std::string getWeaponName() const;
		
		/*!
			set type name of the weapon. 
		*/
		void setWeaponClassName(const std::string& className);

		/*!
			get type name of the weapon.
		*/
		std::string getWeaponClassName() const;

		/*!
		set the ammo amount of the weapon.
		*/
		void setAmmoCount(int count);

		/*!
		get Ammo count of the weapon. 
		*/
		int getAmmoCount() const;

		/*!
		set the elevation angle  of the weapon.  
		*/
		void setElevation(double elevation);

		/*!
		get weapon elevation angle. 
		*/
		double getElevation() const;

		/*!
		set azimuth angle of the weapon. 
		*/
		void setAzimuth(double azimuth);

		/*!
		get azimuth angle.
		*/
		double getAzimuth() const;

		/*!
			Begin muzzle iterator.
		*/
		iterator muzzle_begin();

		/*!
			Begin muzzle constant iterator.
		*/
		const_iterator muzzle_begin() const;

		/*!
			end muzzle iterator.
		*/
		iterator muzzle_end();

		/*!
			end muzzle const iterator.
		*/
		const_iterator muzzle_end() const;

		/*!
			add muzzle name to the list.
		*/
		void addMuzzle(const std::string& muzzleName);

		/*!
			clear muzzle list.
		*/
		void clearMuzzleList();

		/*!
			remove a specified muzzle from the list.
		*/
		bool removeMuzzle(const std::string& muzzleName);

		/*!
			Assignment operator for Weapon class.
			Assign all the attributes of Weapon class.
		*/
		Weapon& operator =(const Weapon& weapon);
		/*!
			Equal operator for Weapon class. Compared by name.
		*/
		bool operator == (const Weapon& weapon);

		/*!
			Less than operator for Weapon class.
			Compared by  name.
		*/
		bool operator < (const Weapon& weapon);

		/*!
			Not equal operator for Weapon class.
			Compared by  name.
		*/
		bool operator != (const Weapon& weapon);

		/*!
			set magazine name of the weapon.
		*/
		void setMagName(const std::string& name);

		/*!
			get magazine name of the weapon.
		*/
		std::string getMagName() const;

		/*!
			set mode name of the weapon.
		*/
		void setModeName(const std::string& name);

		/*!
			get mode name of the weapon.
		*/
		std::string getModeName() const;
		
		/*!
			set Muzzle name of the weapon.
		*/
		void setMuzzleName(const std::string& name);
		
		/*!
			get Muzzle name of the weapon.
		*/
		std::string getMuzzleName() const;

		/*!
			set next round name.
		*/
		void setNextRoundName(const std::string& name);

		/*!
			get next round name.
		*/
		std::string getNextRoundName() const;

		/*!
			set weapon type.
		*/
		void setWeaponType(int weaponType);

		/*!
			get weapon type.
		*/
		int getWeaponType() const;

		/*!
			set weapon weight.
		*/
		void setWeaponWeight(float weight);

		/*!
			get weapon weight.
		*/
		float getWeaponWeight() const;

		/*!
			set magazine type.
		*/
		void setMagazineType(int magazineType);

		/*!
			get magazine type.
		*/
		int getMagazineType() const;

		/*!
			set magazine weight.
		*/
		void setMagazineWeight(float weight);

		/*!
			get magazine weight.
		*/
		float getMagazineWeight() const;

		/*!
			set magazine ammo simulation type.
		*/
		void setMagazineAmmoSimulation(int magazineAmmoSimulation);

		/*!
			get magazine ammo simulation type.
		*/
		int getMagazineAmmoSimulation() const;

	private:

		std::string _weaponName;
		std::string _weaponClassName;
		MuzzleList muzzleList;		
		double _elevation;
		double _azimuth;
		int _ammoCount;

		std::string _magazineName;
		std::string _modeName;
		std::string _muzzleName;

		std::string _nextRoundName;
		int _weaponType;
		float _weaponWeight;
		int _magazineType;
		float _magazineWeight;
		int _magazineAmmoSimulation;

	};

};

#pragma warning(pop) // Enable warnings [C:4251]

#endif //WEAPON_H