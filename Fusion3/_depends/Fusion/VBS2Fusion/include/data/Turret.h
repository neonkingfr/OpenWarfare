/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	Turret.h

Purpose:

	This file contains the declaration of the NetworkID class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			11-12/2009	YFP: Original Implementation

	2.0			10-02/2010  YFP: Added	operators = , == , < 
										Turret(const Turret& turret)
										typedef list<Magazine> MagazineList
										typedef list<Magazine>::iterator mag_iterator
										typedef list<Magazine>::const_iterator mag_const_iterator
										mag_iterator begin(), mag_const_iterator begin()
										mag_iterator end(), mag_const_iterator end()
										setMagazine()
										getCurrentMagazine()
										clearMagazinelist()
										getMagazineCount()
	2.01		10-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_TURRET_H
#define VBS2FUSION_TURRET_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push) 
#pragma warning (disable: 4251)


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBSFusion.h"
#include "data/Magazine.h"
#include "DataContainers/ObjectList.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBSFusion
{
	class VBSFUSION_API Turret
	{
	public:
 		/*!
			Type definition for list contains weapons available in the turret.
		*/
		typedef std::list<Weapon> WeaponList;
		
		/*!
			Iterator for weapon list.
		*/
		typedef std::list<Weapon>::iterator wep_iterator;

		/*!
			Constant iterator for weapon list. 
		*/
		typedef std::list<Weapon>::const_iterator wep_const_iterator;


 		/*!
			Type definition for list contains magazines available in the turret.
		*/
		typedef std::list<Magazine> MagazineList;

		/*!
			Iterator for Magazine list.
		*/
		typedef std::list<Magazine>::iterator mag_iterator;

		/*!
			Constant iterator for magazine list.
		*/
		typedef std::list<Magazine>::const_iterator mag_const_iterator;

		/*!
			Constructor class of the turret.
		*/
		Turret();
		
		/*!
			Destructor class for the turret.
		*/
		~Turret();

		/*!
			Copy constructor for Turret class.
		*/
		Turret(const Turret& turret);

		/*!
			Assignment operator for Turret class.
			Assign all the attributes of Turret class.
		*/
		Turret& operator = (const Turret& turret);

		/*!
			Equal operator for Turret class.
			Compared by name.
		*/
		bool operator == (const Turret& turret);

		/*!
			Less than operator for Turret class.
			Compared by  name.
		*/
		bool operator < (const Turret& turret);

		/*!
			Iterator begin function  for magazine list.
		*/
		mag_iterator mag_begin();

		/*!
			Constant iterator begin function for magazine list.
		*/
		mag_const_iterator mag_begin() const;

		/*!
			Iterator end function for magazine list.
		*/
		mag_iterator mag_end();

		/*!
			Constant iterator end function for magazine list.
		*/
		mag_const_iterator mag_end() const;

		/*!
			Iterator begin function  for weapon list.
		*/
		wep_iterator wep_begin();

		/*!
			Constant iterator begin function for weapon list.
		*/
		wep_const_iterator wep_begin() const;

		/*!
			Iterator end function for weapon list.
		*/
		wep_iterator wep_end();

		/*!
			Constant iterator end function for weapon list.
		*/
		wep_const_iterator wep_end() const;

		/*!
			Typedef for vector containing turret path for Turret. 
		*/
		typedef std::vector<int> TurretPath;

		/*!
			Iterator for turret path. 
		*/
		typedef std::vector<int>::iterator turretPath_iterator;

		/*!
			Const iterator for turret path. 
		*/
		typedef std::vector<int>::const_iterator turretPath_const_iterator;

		//*********************************************************************

		/*!
			Begin iterator for turret path. 
		*/
		turretPath_iterator begin();

		/*!
			Begin const iterator for turret path. 
		*/
		turretPath_const_iterator begin() const;

		/*!
			End iterator for turret path.
		*/
		turretPath_iterator end();

		/*!
			End const iterator for turret path.
		*/
		turretPath_const_iterator end() const;

		//*********************************************************************

		/*!
			Set given turret path  to the turret. 			
		*/
		void setTurretPath(const TurretPath& turretPath);

		/*!
			Get the turret path of the turret. 
			Returns the turret path of the Turret object.
		*/
		TurretPath getTurretPath() const;
    
		/*!
			Set given turret name to the turret.
		*/
		void setTurretName(const std::string& turretName);
        
		/*!
			Returns the name of the turret.
		*/
		std::string getTurretName() const;

		/*!
		returns the path of the turret in string format. Ex [0],[0,1] etc.
		*/
		std::string getTurretpathString() const;

		//*********************************************************************

		/*!
			Set a weapon in the weapon list.
		*/
		void setWeapon(const Weapon& weapon);
		
		/*!
			get the weapon list. 
		*/
		WeaponList& getWeaponList();

		/*!
			clear the weapon list.
		*/
		void clearWeaponList();

		//*********************************************************************

		/*!
			Set a magazine in the magazine list.
		*/
		VBSFUSION_DPR(DTUR004) void setMagazine(Magazine magazine);

		/*!
			get The current magazine that is currently using.
		*/
		VBSFUSION_DPR(DTUR003) Magazine getCurrentMagazine();

		/*!
			clears the magazine list.
		
		*/
		VBSFUSION_DPR(DTUR001) void clearMagazinelist();

		/*!
			get number of magazines available in the turret.
		*/
		VBSFUSION_DPR(DTUR002) int getMagazineCount();

					
	private:
		TurretPath _turretPath;
		std::string _turretName;
		WeaponList _weaponList;

		MagazineList _magazineList;



	};

};

#pragma warning(pop) // Enable warnings [C:4251]

#endif //TURRET_H