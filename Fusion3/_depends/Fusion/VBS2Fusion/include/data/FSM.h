/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	FSM.h

Purpose:

	This file contains the declaration of data structures needed for operation with FSMUitilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0		11-04/2011  [BISim] Otakar Nieder: Orginal Implementation

/************************************************************************/

#include "VBSFusion.h"

#ifndef VBS2FUSION_FSM_H
#define VBS2FUSION_FSM_H

namespace VBSFusion
{
  namespace FSM
  {
    // Enum for types of values that can be passed to or returned by VBS2 script commands.
    enum TypeEnum 
    { 
      typeVoid, typeFloat, typeString, typeBool, typeObject, typeGroup, typeSide,
      typeFloatArray, typeStringArray, typeBoolArray, typeObjectArray, typeContainerArray
    };

    // Enum of error codes for errors during execution
    enum ExecutionError
    {
      eeOK,                 //< No problem encountered.
      eeFunctionNotFound,   //< Function given to Execute as name parameter was not found.
      eeSideNotFound,       //< Side identifier is invalid
      eeGroupNotFound,      //< Group identifier not found
    };

    // Structure used to pass generic type to VBS2 script commands execution class.
    struct TypeValueContainer
    {
      TypeEnum mType; // Type of the value.
      void* mValue;   // Pointer to memory where the value is stored. 

      TypeValueContainer(TypeEnum type, void* value)
        : mType(type)
        , mValue(value)
      {}

      TypeValueContainer(TypeEnum type)
        : mType(type)
        , mValue(NULL)
      {}
    }; 
  }
}

#endif //VBS2FUSION_FSM_H