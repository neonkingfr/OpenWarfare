 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:  Circle.h

Purpose: This file contains the declaration of the Circle class.
		
	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			15-04/2010	YFP: Original Implementation
	1.01		21-04/2010  YFP: Added  Default constructor
										void setRadius()
										void setOrigin()

/*****************************************************************************/

#ifndef VBS2FUSIONHUD_CIRCLE_H
#define VBS2FUSIONHUD_CIRCLE_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "ShapeObject.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	namespace HUD
	{
		class VBS2FUSION_HUD_API Circle : public ShapeObject
		{
		public:

			/*!
			Default constructor for this class. The following parameters are initialized as follows:
			centre = (0,0)
			radius = 0
			thickness = 1
			arcPointCount = 100
			color = WHITE 

			and centre and the radius should be provided.
			*/
			Circle();
			/*!
			Main constructor for this class. The following parameters are initialized as follows:
			thickness = 1
			arcPointCount = 100
			color = WHITE 

			and centre and the radius should be provided.
			*/
			Circle(Point2D centre,float radius);

			/*!
			Main destructor for the class.
			*/
			~Circle();
			
			/*!
			creates the circle resources. Calculate the arc points locations to draw circle.
			*/
			void create();

			/*!
			Render circle on the screen. DirectX draw circle on the screen.
			*/
			void render();

			/*!
			release drawing resource of the circle class by DirectX.
			*/
			void release();

			/*!
			set radius of the circle.
			*/
			void setRadius(float radius);

			/*!
			set centre of the circle.
			*/
			void setCentre(Point2D centre);

			/*!
			set color of the circle.
			*/
			void setColor(ColorRGBA color);

			/*!
			set Thickness of the circle.
			*/
			void setThickness(float thickness);

			/*!
			set Arc point count. If arc point count is high, circle will be smooth.
			*/
			void setArcPointCount(int count);

		private:
			Point2D _centre;
			float _radius;
			ColorRGBA _color;
			ID3DXLine *_line;
			float _thickness;
			int _arcpointCount;

		};
	}
}


#endif //VBS2FUSIONHUD_CIRCLE_H