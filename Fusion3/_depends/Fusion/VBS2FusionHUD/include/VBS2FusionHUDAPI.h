
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name: VBS2FusionHUDAPI.h

Purpose:	This file contains the declaration of the VBS2FusionHUDAPI class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			15-04/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSIONHUD_VBS2FUSIONHUDAPI_H
#define VBS2FUSIONHUD_VBS2FUSIONHUDAPI_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// WINDOWS INCLUDES 
#include <windows.h>

// DIRECTX INCLUDES
#include <d3d9.h>
#include <d3dx9.h>
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

#ifndef VBS2FUSION_HUD_EXPORTS
#define VBS2FUSION_HUD_API __declspec(dllimport)
#else
#define VBS2FUSION_HUD_API __declspec(dllexport)
#endif

struct ColorRGBA 
{
	int r;
	int g;
	int b;
	int a;
};

struct Point2D
{
	float x;
	float y;
};

struct VERTEX
{
	float x, y;
	DWORD color;
};

namespace VBS2Fusion
{
	namespace HUD
	{
		class Circle;
		class Rectangle;
		class TextObject;
		class StatusBar;

	
	}
};



#endif //VBS2FUSIONHUD_VBS2FUSIONHUDAPI_H