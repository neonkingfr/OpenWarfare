
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	DrawFunctions.h

Purpose:	This file contains the declaration of the StatusBar class.

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			15-04/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSIONHUD_DRAWFUNCTIONS_H
#define VBS2FUSIONHUD_DRAWFUNCTIONS_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <windows.h>
// DIRECTX INCLUDES
#include <d3d9.h>
#include <d3dx9.h>
// SIMCENTRIC INCLUDES

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	namespace HUD
	{
		class VBS2FUSION_HUD_API DrawFunctions
		{
		public:
			
			/*!
			Draw Rectangle on the Screen.

			x -  Screen position of the rectangle topmost left vertex( x co ordinate) 
			y -  Screen position of the rectangle topmost left vertex( y co ordinate) 


			height  -   height of the rectangle
			width   -   width of the rectangle 
			alphaVal  - alpha color value	(value should be  between 0 and 255)
			redVal	  - red color value		(value should be  between 0 and 255)
			greenVal  - green color value	(value should be  between 0 and 255)
			blueVal	  - blue color value	(value should be  between 0 and 255)
			thickness - Thickness of the rectangle outline.
			
			
			*/
			static void DrawRectangle(int x,int y,int height, int width, int alphaVal, int redVal, int greenVal,int blueVal, float thickness);

			/*!
			Draw Circle on the Screen.

			x -  Screen position of the center
			y -  Screen position of the rectangle topmost left vertex

			radius - radius of the circle

			alphaVal  - alpha color value	(value should be  between 0 and 255)
			redVal	  - red color value		(value should be  between 0 and 255)
			greenVal  - green color value	(value should be  between 0 and 255)
			blueVal	  - blue color value	(value should be  between 0 and 255)

			nPoints   - number of arc points . Smoothness increases when nPoints increases. 

			thickness - Thickness of the circle outline.


			*/
			static void DrawCircle(int x,int y, float radius, int aVal,int rVal,int gVal,int bVal, int nPoints,float thickness);

			/*!
			Draw Text on the screen.
			*/
			static void DrawHUDText(string message , float left , float top , int fontSize);

			/*!
			Draw Line on the screen.
			startX  - x co ordinate of start point on the display screen. 
			startY  - y co ordinate of start point on the display screen.
			endX	- x co ordinate of end point on the display screen.	
			endY	- y co ordinate of end point on the display screen.

			alphaVal  - alpha color value	(value should be  between 0 and 255)
			redVal	  - red color value		(value should be  between 0 and 255)
			greenVal  - green color value	(value should be  between 0 and 255)
			blueVal	  - blue color value	(value should be  between 0 and 255) 

			thickness - Thickness of the  outline.
			*/
			static void DrawLine(int startX, int startY, int endX, int endY,int alphaVal, int redVal, int greenVal,int blueVal, float thickness);

			/*!
			Draw image on the screen.
			fileName - full path name of the image file.
			xpos     - x co-ordinate of topmost left screen position.  
			ypos     - y co-ordinate of topmost left screen position.
			scale    - scale value.

			*/
			static void DrawImage(const char *filePath,int xpos, int ypos , float scale);

			/*!
			Draw image on the screen.
			fileName - full path name of the image file.
			xpos     - x co-ordinate of topmost left screen position.  
			ypos     - y co-ordinate of topmost left screen position.
			x_center - x co-ordinate of image center.
			y_center - y co-ordinate of image center.
			scale    - scale value.
			angle    - rotation value in degrees. 
			*/
			static void DrawImage(const char *filePath,int xpos, int ypos , int x_centre, int y_centre, float scale , float angle);

		};

	}
}


#endif //VBS2FUSIONHUD_DRAWFUNCTIONS_H