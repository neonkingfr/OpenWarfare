/*
* Copyright 2010 SimCentric Technologies, Pty. Ltd.  All Rights Reserved.
*
* Permission to use, copy, modify, and distribute this software in object
* code form for any purpose and without fee is hereby granted, provided
* that the above copyright notice appears in all copies and that both
* that copyright notice and the limited warranty and restricted rights
* notice below appear in all supporting documentation.
*
* SIMCENTRIC PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
* SIMCENTRIC SPECIFICALLY DISCLAIMS ANY AND ALL WARRANTIES, WHETHER EXPRESS
* OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTY
* OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE OR NON-INFRINGEMENT
* OF THIRD PARTY RIGHTS.  SIMCENTRIC DOES NOT WARRANT THAT THE OPERATION
* OF THE PROGRAM WILL BE UNINTERRUPTED OR ERROR FREE.
*
* In no event shall SimCentric Technologies, Pty. Ltd. be liable for any direct, indirect,
* incidental, special, exemplary, or consequential damages (including,
* but not limited to, procurement of substitute goods or services;
* loss of use, data, or profits; or business interruption) however caused
* and on any theory of liability, whether in contract, strict liability,
* or tort (including negligence or otherwise) arising in any way out
* of such code.
*/

/*****************************************************************************

Name:	HUDManager.h

Purpose:	This file contains the declaration of the HUDManager class.

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			15-04/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSIONHUD_HUDMANAGER_H
#define VBS2FUSIONHUD_HUDMANAGER_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <list>
// DIRECTX INCLUDES
#include <d3d9.h>
#include <d3dx9.h>

// SIMCENTRIC INCLUDES
#include "hudUT/DrawObject.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

using namespace std;

namespace VBS2Fusion
{
	namespace HUD
	{
		class VBS2FUSION_HUD_API HUDManager
		{
		public:
			/*!
			Main constructor for this class.
			*/
			HUDManager();

			/*
			Destructor for this class.
			*/
			~HUDManager();

			/*!
			set Direct3D device. 
			*/
			void setDevice(IDirect3DDevice9* device);

			/*!
			returns the Direct3D device.
			*/
			IDirect3DDevice9* getDevice();

			/*!
			returns screen Height of the user display device.
			*/
			int getDisplayScreenHeight();

			/*!
			returns screen width of the user display device.
			*/
			int getDisplayScreenWidth();


		private:
			IDirect3DDevice9* _device;
		};
	}	
};





#endif //VBS2FUSIONHUD_HUDMANAGER_H