// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/callback/FileIOCallback.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/String.h>
#include <xait/common/geom/IndexedTriangle.h>
#include <xait/common/container/Vector.h>
#include <xait/common/math/Vec3.h>
#include <xait/map/CompileDefines.h>
#include <xait/map/build/NavMeshBuilder.h>
#include <xait/map/build/NavMeshOptimizer.h>



namespace XAIT
{
	namespace Map
	{

		//! \brief The NavMesh Interface.
		//!
		//! This class provides you with methods to initialize and close the NavMesh library, and it offers you access to the NavMeshBuilder, the NavMeshOptimizer, and to load VRML2 files.
		class Interface
		{
		public:
			//! \brief Initializes the NavMesh library.
			//!
			//! It must be called before any further calls to the NavMesh interface class
			//! are issued. The callback objects must stay valid until the library is closed.
			//! \param cbFileIO		callbacks for file handling
			//! \remarks The cbfileIO is only needed if you intent to use the filename based serialize functions of the NavMeshSerializer class.
			//! \see closeLibrary
			XAIT_MAP_DLLAPI static void XAIT_CDECL initLibrary(const Common::Callback::FileIOCallback::Ptr& cbFileIO= Common::Callback::FileIOCallback::Ptr(NULL));

			//! \brief Closes the library.
			//!
			//! After this call you cannot use the library any more. This method also releases the used memory.
			//! \see initLibrary
			XAIT_MAP_DLLAPI static void XAIT_CDECL closeLibrary();

			//! \brief Creates a new NavMesh builder object.
			//! \return a new NavMesh builder object
			//! \remark You have to take care of the builder object deletion.
			XAIT_MAP_DLLAPI static Build::NavMeshBuilder* XAIT_CDECL createNavMeshBuilder();

			//! \brief Creates a new NavMesh optimizer object.
			//! \return a new NavMeshOptimizer object
			XAIT_MAP_DLLAPI static Build::NavMeshOptimizer* XAIT_CDECL createNavMeshOptimizer();

			//! \brief Imports a VRML2 file.
			//! \param vertices				vertex array to hold vertices
			//! \param triangles			triangle array to hold triangles
			//! \param fileName				VRML2 file to import
			//! \param reportUnknownNodes	report unknown nodes to logfile
			//! \return true, if importing the file was successful, and false otherwise
			//! \remark This method is currently only supported on WIN32/64.
			XAIT_MAP_DLLAPI static bool XAIT_CDECL importVRML2Geometry(Common::Container::Vector<Common::Math::Vec3f>& vertices, Common::Container::Vector<Common::Geom::IndexedTriangle>& triangles, const Common::String& fileName, const bool reportUnknownNodes= true);

			//! \brief Gets global allocator.
			//! \return the global allocator
			XAIT_MAP_DLLAPI static const Common::Memory::Allocator::Ptr& getGlobalAllocator();

		};
	}
}
