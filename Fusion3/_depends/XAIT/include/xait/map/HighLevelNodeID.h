// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/GlobalNodeID.h>

namespace XAIT
{
	namespace Map
	{
		typedef GlobalNodeID	HighLevelNodeID;
	}
}

