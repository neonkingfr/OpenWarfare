// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/BuildOnlyPathObject.h>
#include <xait/common/math/Matrix4.h>

namespace XAIT
{
	namespace Map
	{
		//! This is a path object in the shape of box. It will introduce geometry into the build process which
		//! is defined a non-walkable. You can use this to restrict navmesh in certain areas when combined with
		//! spawn points. 
		//! \brief A path object which represents a box and can block an area during navmesh build
		//! \remark This is a build only path object and does only influence the build process. It does not have
		//!			any functionality during runtime. It will not be stored inside the final navigation mesh except
		//!			you turned on storage of build only path objects (\see NavMeshBuildConfig::setStoreBuildOnlyPathObjects)
		class BlockedAreaPathObject : public BuildOnlyPathObject
		{
		public:
			//! \brief Constructs a new blocked area object
			//! \param name		A unique name of the path object
			//! \remark The name must be unique within the navmesh its being used.
			XAIT_MAP_DLLAPI BlockedAreaPathObject(const Common::String& name);

			//! \brief Gets the transformation matrix of the path object (right hand coordinate system)
			inline const Common::Math::Matrix4f& getTransform() const { return mTransform; }

			//! \brief Gets the transformation matrix of the path object (right hand coordinate system)
			inline Common::Math::Matrix4f& getTransform() { return mTransform; }

			//! \brief Gets the world bounding box of this path object
			XAIT_MAP_DLLAPI Common::Geom::AABB3Df getBoundingBox() const;

			//! Get the corner vertex of the blocking object with transformation applied. The indices reference to the
			//! vertices as follow
			//!
			//!          7-------6
			//!         /|      /|
			//!        4-+-----5 |
			//!        | 3-----|-2
			//!        |/      |/
			//!        0-------1
			//! 
			//! \brief Get corner vertex
			//! \param cornerID		id of the corner in the range of 0-7
			XAIT_MAP_DLLAPI Common::Math::Vec3f getCorner(const uint32 cornerID) const;

			//! \brief Get the corners of the blocking object without transformation
			XAIT_MAP_DLLAPI const Common::Math::Vec3f& getLocalCorner(const uint32 cornerID) const;

			//! \brief Gets a set of indices that form a cube
			//! \returns A set of 6 * 2 triangles for all faces of the cube. Each triangle consists of 3 indices.
			//!          So the number of indices is 36.
			//! \remark	This is intend for debug purposes
			//! \remark Indices are CCW.
			XAIT_MAP_DLLAPI const uint32* getIndices() const;

			//! \brief Get Number of indices
			inline uint32 getNumIndices() const { return 36; }

			virtual PathObject* clone() const { return new BlockedAreaPathObject(*this); }
			virtual uint32 getMemoryUsage() const { return sizeof(BlockedAreaPathObject); }
		protected:
			XAIT_MAP_DLLAPI virtual bool initFromStream(Common::Serialize::SerializeStorage* serializer);
			XAIT_MAP_DLLAPI virtual void writeToStream(Common::Serialize::SerializeStorage* serializer) const;

		private:
			Common::Math::Matrix4f	mTransform;
		};
	}
}
