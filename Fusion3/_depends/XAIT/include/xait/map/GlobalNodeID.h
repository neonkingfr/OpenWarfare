// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/String.h>
#include <xait/common/utility/BitMask.h>
#include <xait/map/CompileDefines.h>

// change only the following two lines
#define XAIT_MAP_BITS_NODEID						18
#define XAIT_MAP_BITS_GNODEID						32							

// computed macros
#define XAIT_MAP_BITS_SECTORID						(XAIT_MAP_BITS_GNODEID - XAIT_MAP_BITS_NODEID)
#define XAIT_MAP_MASK_NODEID						(XAIT::Common::Utility::BitMask<XAIT_MAP_BITS_NODEID>::MASK)   //0x000FFFFF	// 20-bit

#define XAIT_MAP_SECTORID(nodeid)					((nodeid) >> XAIT_MAP_BITS_NODEID)
#define XAIT_MAP_NODEID(nodeid)						((nodeid) & XAIT_MAP_MASK_NODEID)
#define XAIT_MAP_CREATE_NODEID(sectorID,nodeID)		( ((sectorID) << XAIT_MAP_BITS_NODEID) | (nodeID) )
#define XAIT_MAP_MAX_NODEID							XAIT_MAP_MASK_NODEID
#define XAIT_MAP_MAX_SECTORID						(0xFFFFFFFF >> XAIT_MAP_BITS_NODEID)

namespace XAIT
{
	namespace Map
	{
		//! \brief Wrapper class for a Node ID in the navigation mesh.
		//! \remarks This class wraps only an integer
		struct GlobalNodeID
		{
			//! \brief Creates a new global node from a sector ID and an internal node ID in the sector.
			GlobalNodeID(uint32 sectorID, uint32 sectorNodeID)
			{
				X_ASSERT_MSG_DBG2(sectorID <= XAIT_MAP_MAX_SECTORID,"SectorID out of range",sectorID,XAIT_MAP_MAX_SECTORID);
				X_ASSERT_MSG_DBG2(sectorNodeID < XAIT_MAP_MAX_NODEID,"NodeID out of range",sectorNodeID,XAIT_MAP_MAX_NODEID);
				mGlobalNodeID = XAIT_MAP_CREATE_NODEID(sectorID,sectorNodeID);				
			}

			//! \brief Creates an invalid global node ID.
			GlobalNodeID()
				: mGlobalNodeID(0xFFFFFFFF)
			{}

			//! \brief Initializes a global node ID from an unsigned integer.
			//! \param globalID the value with which the global node ID will be initialized
			explicit GlobalNodeID(const uint32 globalID)
				: mGlobalNodeID(globalID)
			{}

			//! \brief Gets the sector ID as part of the node ID.
			//! \returns the sector ID
			inline uint32 getSectorID() const
			{
				return XAIT_MAP_SECTORID(mGlobalNodeID);
			}

			//! \brief Gets the sector node ID.
			//! \returns the sector node ID
			inline uint32 getSectorNodeID() const
			{
				return XAIT_MAP_NODEID(mGlobalNodeID);
			}

			//! \brief Tests if this is a valid node ID.
			//! \returns true, if it is valid, and false otherwise
			inline bool isValid() const
			{
				return mGlobalNodeID != 0xFFFFFFFF;
			}

			//! \brief Gets the global node ID as an integer value.
			//! \returns the node ID as an integer value
			inline uint32 getGlobalID() const
			{
				return mGlobalNodeID;
			}

			//! \brief Assignment operator.
			//! \param b the global node ID which is assigned to this global node ID
			//! \returns the resulting global node ID
			inline GlobalNodeID& operator= (const GlobalNodeID& b) 
			{
				mGlobalNodeID = b.mGlobalNodeID;
				return *this;
			}

			//! \brief Writes this global node ID to a readable string.
			//! \returns this global node ID as a string.
			Common::String toString() const
			{
				return "(" + Common::String(getSectorID()) + "/" + Common::String(getSectorNodeID()) + ")";
			}

			//! \brief Equality comparison operator.
			//! \param nodeID the nodeID against which this nodeID is compared
			//! \returns true, if the node IDs are the same, and false otherwise
			inline bool operator==(const GlobalNodeID& nodeID) const
			{
				return mGlobalNodeID == nodeID.mGlobalNodeID;
			}

			//! \brief Inequality comparison operator.
			//! \param nodeID the nodeID against which this nodeID is compared
			//! \returns false, if the node IDs are the same, and true otherwise
			inline bool operator!=(const GlobalNodeID& nodeID) const
			{
				return mGlobalNodeID != nodeID.mGlobalNodeID;
			}

			//! \brief Less than comparison operator.
			//! \param nodeID the nodeID against which this nodeID is compared
			//! \returns true, if this node ID is strictly smaller than the comparison node ID, and false otherwise
			inline bool operator<(const GlobalNodeID& nodeID) const
			{
				return mGlobalNodeID < nodeID.mGlobalNodeID;
			}

			//! \brief An invalid node ID.
			XAIT_MAP_DLLAPI static const GlobalNodeID INVALID_NODEID;

			static const uint32 MAX_NUM_SECTORS		= Common::Utility::BitMask<XAIT_MAP_BITS_SECTORID>::MASK;
			static const uint32 MAX_NUM_NODES		= Common::Utility::BitMask<XAIT_MAP_BITS_NODEID>::MASK;

		private:
			uint32	mGlobalNodeID; //!< holds the global node ID, composed of the sector ID and the node ID
		};
	}

	inline std::ostream& operator<<(std::ostream& os,const XAIT::Map::GlobalNodeID node)
	{
		os << node.toString();
		return os;
	}
}
