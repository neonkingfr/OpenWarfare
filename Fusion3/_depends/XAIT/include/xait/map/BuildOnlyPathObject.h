// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/PathObject.h>

namespace XAIT
{
	namespace Map
	{
		class BuildOnlyPathObject : public PathObject
		{
		public:
			BuildOnlyPathObject(const PathObjectType typeID, const Common::String& name)
				: PathObject((uint32)typeID | CLASS_BUILDONLY,name)
			{}
		};
	}
}
