// (C) xaitment GmbH 2006-2012

#pragma once

// -------------------------------------------------------------------------------------------------------------
//										Nav Mesh Generator Class
//
// Author: Markus Wilhelm
//
// The nav mesh generator creates from a bunch of triangles a navigation mesh. The output is not connected to 
// the mediagraph, so that we can use this library in different ways.
// -------------------------------------------------------------------------------------------------------------

#include <xait/common/FundamentalTypes.h>
#include <xait/common/container/Vector.h>
#include <xait/common/io/Stream.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/threading/SpinLock.h>
#include <xait/common/threading/Thread.h>
#include <xait/map/CompileDefines.h>
#include <xait/map/NavMeshContainer.h>
#include <xait/map/build/BuildProgressHandler.h>
#include <xait/map/build/NavMeshBuildConfig.h>
#include <xait/map/build/GeometryInfo.h>

namespace XAIT
{
	namespace Map
	{
		namespace Build
		{
			class NavMeshBuild;

			//! \brief Core class for building a navigation mesh from a triangle collection.
			class NavMeshBuilder : public Common::Memory::MemoryManaged
			{
			public:
				//! \brief Default constructor. Use Map::Interface::createNavMeshBuilder to create a new instance.
				XAIT_MAP_DLLAPI NavMeshBuilder();

				//! \brief Destructor.
				XAIT_MAP_DLLAPI ~NavMeshBuilder();

				//! \brief Sets the build progress handler.
				//! 
				//! The build progress handler will notify every progress change during the build to you. If the build runs asynchronous you will
				//! have to take care that your progress handler is thread safe.
				XAIT_MAP_DLLAPI void setBuildProgressHandler(BuildProgressHandler* handler);

				//! \brief Adds an input geometry to the build.
				//! 
				//! Note that the same geometry info can be added twice or more.
				//! \param geometryInfo		the geometry info to add
				XAIT_MAP_DLLAPI void addInputGeometry(const GeometryInfo& geometryInfo);

				//! \brief Removes all geometry infos from the build.
				XAIT_MAP_DLLAPI void clearInputGeometry();


				XAIT_MAP_DLLAPI void addPathObject(PathObject* pathObject);
				XAIT_MAP_DLLAPI void clearPathObjects();

				//! \brief Gets all input geometries of the build.
				//! 
				//! This method can only be called if the build has not started yet. Use the const method instead if you want to query data during the build process.
				//! \return a vector containing all geometry infos of the build
				XAIT_MAP_DLLAPI Common::Container::Vector<GeometryInfo>& getInputGeometry();

				//! \brief Gets all input geometries of the build.
				//! \return a vector containing all geometry infos of the build
				XAIT_MAP_DLLAPI const Common::Container::Vector<GeometryInfo>& getInputGeometry() const;

				//! \brief Gets the current build configuration.
				//! \return the current build configuration
				XAIT_MAP_DLLAPI const NavMeshBuildConfig* getBuildConfig() const;

				//! \brief Gets the current build configuration.
				//! 
				//! This method can only be called if the build has not started yet. Use the const method instead if you want to query data during the build process.
				//! \return the current build configuration
				XAIT_MAP_DLLAPI NavMeshBuildConfig* getBuildConfig();

				//XAIT_MAP_DLLAPI void setViewName(const Common::String& viewName);

				//XAIT_MAP_DLLAPI const Common::String& getViewName() const;

				//! \brief Sets a NavMesh which should be used as base for an update.
				//!
				//! \param toUpdate the NavMesh used as base
				//! \remarks This supplied navmesh instance will be modified, and will NOT be the final build result. After the build this instance will be inconsistent, so just delete it.
				//!          This also holds true if the build will be canceled.
				XAIT_MAP_DLLAPI void setUpdateNavMesh(NavMeshContainer* toUpdate);

				//! \brief Gets the update NavMesh.
				//! \return the NavMesh instance used as update base
				XAIT_MAP_DLLAPI const NavMeshContainer* getUpdateNavMesh() const;

				//! \brief Sets a stream which should be used as the sector cache.
				//!
				//! If the stream is empty it will be initialized as a sector cache and it will be filled with cache data from the build.
				//! If it is already a sector cache stream it will be extended by new cache data. Note that the cache will always grow!
				//! If the stream is read only, the stream will only be used as cache and not extended by new cache data.
				//! \param sectorCacheStream	the stream to be used as sector cache
				//! \param bufferSize			buffer size for a buffer that should be used for reading / writing (<= 0 disables buffer)
				//! \return true if the stream can be used as sector cache, or false if stream cannot be used (this can be due to No Read, Wrong Version, etc. Look into the log for more information.)
				XAIT_MAP_DLLAPI bool setSectorCache(const Common::SharedPtr<Common::IO::Stream>& sectorCacheStream, int32 bufferSize= 32768);


				//! \brief Starts the generation of a NavMesh on the given geometries with the given build parameters.
				//!
				//! If blocking is set to false the computation of the NavMesh
				//! will be started in an additional thread with its worker threads. You can use the method NavMeshBuilder::isRunning to check
				//! whether the process is still running. In addition you could use NavMeshBuilder::waitForFinished which blocks the calling thread
				//! until the work has been done. It is essential that you do not change the geometry until the build has been finished.
				//! The NavMesh result can be read with getBuildResult after the generation has finished, either synchronous or asynchronous.
				//! \param blocking			if true, this call returns after the NavMesh has been generated, if false it returns immediately
				//! \param numberOfThreads	number of worker threads, -1 indicates auto (uses optimum for system), 0 runs the NavMesh generator 
				//!							non-threaded (this can still be combined with the blocking parameter)
				XAIT_MAP_DLLAPI void startNavMeshGeneration(bool blocking= true, int32 numberOfThreads= -1);

				//! \brief Checks whether the NavMesh algorithm is still running.
				//! \return true, if it is still running, and false otherwise
				XAIT_MAP_DLLAPI bool isRunning();

				//! \brief Waits until the NavMesh algorithm has finished its computation.
				//! 
				//! This method will block the calling thread until the computation finished.
				XAIT_MAP_DLLAPI void waitForFinished();

				//! \brief Gets the build result after the NavMesh generation has been finished.
				//!
				//! Note that this functions returns the build result only once. After this method has been called, the NavMesh will not longer be maintained inside.
				//! \return an instance to the newly created NavMesh, or NULL if the NavMesh is empty
				XAIT_MAP_DLLAPI NavMeshContainer* returnBuildResult();

				//! \brief Cancels the currently running build.
				//!
				//! This method might not cancel the build directly. The algorithm has some exit points at which the execution will be halted.
				//! If you have set an update mesh, it will unusable.
				//! \remark This method is non blocking. If you want to wait until the build is fully canceled call NavMeshBuilder::waitForFinished afterwards.
				XAIT_MAP_DLLAPI void cancelBuild();


				//! \brief Collects all the build data as debug info and writes it to a stream.
				//!
				//! Build data includes the build config, geometry (optional) and the resulting 
				//! navmesh if one is available. If mBuild does not provide a navmesh and altNavMesh is
				//! not set, the Navmesh export step is skipped.
				//! \param str destination where the debug data is to be written to
				//! \param includeGeometry		if true, geometry data will be exported as well. if false, no geometry is added
				//! \param includeNavmesh		if true, navmesh data will be exported if available
				//! \param altNavMesh			if set, the instance given here will be exported instead of the navmesh previously created by this build
				XAIT_MAP_DLLAPI bool collectDebugData(Common::IO::Stream* str, bool includeGeometry=true, bool includeNavmesh=true, XAIT::Map::NavMeshContainer* altNavMesh=NULL);

				//! \brief Collects all the build data as debug info and writes it to a stream.
				//!
				//! \param fileName				A filesystem destination where the debuginfo file should be created.
				//! \param includeGeometry		if true, geometry data will be exported as well. if false, no geometry is added
				//! \param includeNavmesh		if true, navmesh data will be exported if available
				//! \param altNavMesh			if set, the instance given here will be exported instead of the navmesh previously created by this build
				XAIT_MAP_DLLAPI bool collectDebugData(const Common::String& fileName, bool includeGeometry=true, bool includeNavmesh=true, XAIT::Map::NavMeshContainer* altNavMesh=NULL);


			private:
				NavMeshBuild*								mBuild; //!< contains the NavMesh that this builder builds
				volatile bool								mIsRunning; //!< flag indicating whether the build algorithm is running

				Common::Threading::SpinLock					mBuildLock; //!< lock to synchronize the worker threads
				Common::Threading::Thread*					mAsyncWorker; //!< the worker thread

				//! \brief Sets the mIsRunning flag.
				//! \param state the new state
				void setIsRunning(const bool state);

				//! \brief Deletes the worker thread.
				void deleteAsyncWorker();

				//! \brief Starts the NavMesh generation algorithm.
				//! \param numWorkerThreads the number of threads
				void runNavMeshGeneration(int32 numWorkerThreads);

				//! \brief Starts the NavMesh generation algorithm.
				//! \param param a pair consisting of a NavMeshBuilder and the number of worker threads
				static int32 runNavMeshGenerationAsync(void* param);
			};

		}
	}
}
