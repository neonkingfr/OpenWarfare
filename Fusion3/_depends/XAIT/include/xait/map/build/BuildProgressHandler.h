// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/String.h>

namespace XAIT
{
	namespace Map
	{
		namespace Build
		{
			//! \brief Base class for progress feedback.
			struct BuildProgressHandler
			{
				enum MessageType
				{
					MT_MESSAGE	= 0,
					MT_WARNING	= 1,
					MT_ERROR	= 2,
				};

				virtual ~BuildProgressHandler() {};

				//! \brief Will be called if the progress of the build has changed.
				//! \param progress		progress value in the range of 0 - 100
				//! \param message		describes the progress change
				//! \param messageType	the type of the message. one of the types from BuildProgressHandler::MessageType
				virtual void progressChanged(const float32 progress, const Common::String& message, const MessageType messageType)
				{
					progressChanged(progress,message);
				}

				//! \brief Obsoleted function, do not override or use any more.
				virtual void progressChanged(const float32 progress, const Common::String& message) {};
			};
		}
	}
}


