// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/LookUp.h>
#include <xait/map/MapMemoryPool.h>

namespace XAIT
{
	namespace Map
	{
		struct CharacterInfo;
		class NavMeshContainer;

		namespace Path
		{
			//! \brief	This class is used to be specified character parameters that are relevant to the path search
			//!			surfaces cost factors that are not defined are set to default surface factor (default 1.0).
			//!			default value of height cost factor is also 1.0
			//!			negative cost factors are not allowed
			//!			the character config does NOT depend on a certain nav mesh container
			class CharacterConfig : public Common::Memory::StaticMemoryManaged<MapMemoryPool::getLowAllocThreadSafeAllocator>
			{
			public:
				//! \brief constructor
				//! \param characterName	the character info the config is related to
				CharacterConfig(const Common::String& characterName)
					: mSurfaceCostFactors(getAllocator())
					, mCharacterName(characterName)
					, mDefaultSurfaceFactor(1.0f)
				{}

				//! \brief Sets the surface factor for all surfaces which have not been explicitly set by \see setSurfaceFactor
				//! \param factor	cost factor, use X_MAX_FLOAT32 to exclude all surfaces that have not been specified otherwise, or 1.0f to include all surfaces that have not been specified otherwise. The cost factor, should be in the range of [1,X_MAX_FLOAT32] for optimal performance.
				inline void setDefaultSurfaceFactor(const float32 factor)
				{
					X_ASSERT_MSG1(factor >= 0.f, "Parameter 'factor' is not allowed to be negativ.",factor);
					mDefaultSurfaceFactor= factor;
				}

				//! \brief Sets the default surface factor in such a way, that all non specified surfaces are excluded
				inline void setDefaultExcludeAllSurfaces()
				{
					setDefaultSurfaceFactor(X_MAX_FLOAT32);
				}

				//! \brief Sets the default surface factor in such a way, that all non specified surfaces are included
				inline void setDefaultIncludeAllSurfaces()
				{
					setDefaultSurfaceFactor(1.0f);
				}

				//! \brief Gets the default surface factor which will be used if the surface factor is not explicitly set
				inline float32 getDefaultSurfaceFactor() const
				{
					return mDefaultSurfaceFactor;
				}

				//! \brief sets the surface cost factor
				//! \param surfaceName		the name of the surface
				//! \param factor			the cost factor, should be in the range of [1,X_MAX_FLOAT32] for optimal performance 
				//! \remark negative cost factors are not allowed
				inline void setSurfaceFactor(const Common::String& surfaceName, const float32 factor)
				{

					X_ASSERT_MSG1(factor >= 0.f, "Parameter 'factor' is not allowed to be negativ.",factor);
					Common::Container::LookUp<Common::String,float32>::Iterator iter = mSurfaceCostFactors.find(surfaceName);

					if (iter != mSurfaceCostFactors.end())
						iter->second = factor;

					mSurfaceCostFactors[surfaceName] = factor;
				}

				//! \brief gets a surface cost factor
				//! \param surfaceName		the id of the surface
				//! \returns the cost factor of the surface. If the surface is not explicitly specified by setSurfaceFactor, the default surface factor will be returned.
				inline float32 getSurfaceFactor(const Common::String& surfaceName) const
				{
					Common::Container::LookUp<Common::String,float32>::ConstIterator iter = mSurfaceCostFactors.find(surfaceName);

					// return default costs on not specified surface types
					if (iter == mSurfaceCostFactors.end())
						return mDefaultSurfaceFactor;

					return iter->second;
				}

				//! \brief Reset surface factors to default
				inline void resetSurfaceFactors()
				{
					mSurfaceCostFactors.clear();
				}

				// \brief gets the character name
				inline const Common::String& getCharacterName() const
				{
					return mCharacterName;
				}


			protected:
				Common::Container::LookUp<Common::String,float32>	mSurfaceCostFactors;
				Common::String										mCharacterName;
				float32												mDefaultSurfaceFactor;
			};
		}
	}
}
