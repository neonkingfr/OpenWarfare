// (C) xaitment GmbH 2006-2012

#pragma once


#include <xait/map/path/SubdivisionPathSearch.h>
#include <xait/map/path/PathSearchInstance.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			class SearchRequest;

			//! \brief this class implements the PathSearchInstance interface with a Subdivision search (astar nodes are polygon Subdivisions)
			class SubdivisionPathSearchInstance : public PathSearchInstance
			{
				friend class SubdivisionPathSearch;
				struct Storage;
			public:
				//! \brief destructor
				XAIT_MAP_DLLAPI virtual ~SubdivisionPathSearchInstance();


			private:
				SubdivisionPathSearchInstance(SubdivisionPathSearch* owner, const SubdivisionAStarMap* map);

				virtual PathResult* getPath();

				virtual int runTypedSearch(const uint32 numSteps);

				virtual bool initTypedSearch(SearchRequest* searchRequest);

				virtual bool existsPossiblePath(const SearchRequest* searchRequest) const;

				Storage*	mStorage;
			};
		}
	}
}
