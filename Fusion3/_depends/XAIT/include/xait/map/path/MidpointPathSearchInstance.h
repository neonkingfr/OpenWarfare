// (C) xaitment GmbH 2006-2012

#pragma once


#include <xait/map/path/MidpointPathSearch.h>
#include <xait/map/path/PathSearchInstance.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			class SearchRequest;

			//! \brief this class implements the PathSearchInstance interface with a midpoint search (astar nodes are polygon midpoints)
			class MidpointPathSearchInstance : public PathSearchInstance
			{
				friend class MidpointPathSearch;
				struct Storage;
			public:
				//! \brief destructor
				XAIT_MAP_DLLAPI virtual ~MidpointPathSearchInstance();

				
			private:
				MidpointPathSearchInstance(MidpointPathSearch* owner, const MidpointAStarMap* map);

				virtual PathResult* getPath();

				virtual int runTypedSearch(const uint32 numSteps);

				virtual bool initTypedSearch(SearchRequest* searchRequest);

				virtual bool existsPossiblePath(const SearchRequest* searchRequest) const;

				Storage*	mStorage;
			};
		}
	}
}
