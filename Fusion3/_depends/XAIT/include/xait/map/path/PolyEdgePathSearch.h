// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/path/PathSearch.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			class PolyEdgeAStarMap;

			//! \brief	this class implements the pathsearch with an polyedge search space
			//!			this algorithm results in more precise paths but is slower
			class PolyEdgePathSearch : public PathSearch
			{
			public:
				//! \brief constructor
				//! \param navMesh		the navmesh the search is running on
				XAIT_MAP_DLLAPI PolyEdgePathSearch(const NavMeshContainer* navMesh);

				//! \brief destructor
				XAIT_MAP_DLLAPI ~PolyEdgePathSearch();

				//! \brief creates an instance of the pathsearch
				//! \returns an instance of the pathsearch
				XAIT_MAP_DLLAPI virtual PathSearchInstance* createInstance(const Common::Memory::Allocator::Ptr& allocator= Common::Memory::Allocator::Ptr());

				virtual Common::String getTypeName() const
				{
					return "PolyEdge";
				}

			private:
				PolyEdgeAStarMap*			mMap;
			};
		}
	}
}
