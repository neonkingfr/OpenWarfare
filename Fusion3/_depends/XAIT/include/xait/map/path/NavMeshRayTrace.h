// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/GlobalNodeID.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/container/List.h>
#include <xait/map/path/CompiledSearchConfig.h>



namespace XAIT
{
	namespace Map
	{
		namespace Path
		{		
			//! \brief A class that can be used for "ground ray tracing" on a NavMesh (rays will always follow the ground-height).
			class NavMeshRayTrace
			{
			public:
				struct CrossedNode
				{
					CrossedNode()
						: mEdgeID(X_MAX_UINT32)
					{}

					CrossedNode(GlobalNodeID nodeID, uint32 edgeID,const Common::Math::Vec3f& position)
						: mPosition(position), mNodeID(nodeID), mEdgeID(edgeID)
					{}

					Common::Math::Vec3f		mPosition;
					GlobalNodeID			mNodeID;
					uint32					mEdgeID;
				};

				//! \brief	Constructor.
				//! \param  navMesh		the NavMesh that is used for ray tracing
				XAIT_MAP_DLLAPI NavMeshRayTrace(const NavMeshContainer* navMesh);

				//! \brief Destructor.
				XAIT_MAP_DLLAPI ~NavMeshRayTrace();

				//! \brief Calculates the distance to either the farthest reachable polygon edge, or an polygon edge where the jump/fall height exceeds the given limits is found.
				//! \param startPos		start-position of the ray
				//! \param dir			direction of the ray (since it is traced ON NavMesh polygons, the up-component is ignored!)
				//! \param maxT			if nothing is hit, trace ends at distance maxT
				//! \param minHeight	the minimum height of the space above the NavMesh node polygon
				//! \return				the traced distance (maxT if nothing was hit, -1.0f if the initialization failed (when the direction or the startPos are invalid)
				XAIT_MAP_DLLAPI float32 traceRay(const Common::Math::Vec3f& startPos,const Common::Math::Vec3f& dir, const float32 maxT,const float32 minHeight);


				//! \brief Calculates the distance to either the farthest reachable polygon edge, or an polygon edge where the jump/fall height exceeds the given limits is found.
				//! \param startPos	start-position of the ray
				//! \param startNodeID	the ID of the NavMesh node where the start position is
				//! \param dir			direction of the ray (since it is traced ON NavMesh polygons, the up-component is ignored!)
				//! \param maxT			if nothing is hit, trace ends at distance maxT
				//! \param minHeight	the minimum height of the space above the NavMesh node polygon
				//! \return				the traced distance (maxT if nothing was hit, -1.0f if the initialization failed (when the direction or the startPos are invalid)
				XAIT_MAP_DLLAPI float32 traceRay(const Common::Math::Vec3f& startPos,const GlobalNodeID startNodeID,const Common::Math::Vec3f& dir, const float32 maxT,const float32 minHeight);



				//! \brief Checks whether the endPos can be reached from startPos on a straight line.
				//! \param startPos	start-position of the ray
				//! \param endPos	end-position of the ray
				//! \param minHeight	the minimum height of the space above the NavMesh node polygon
				//! \return		    true, if the endPos could be reached, and false otherwise  (including dir/startPos/endPos invalid)
				XAIT_MAP_DLLAPI bool traceRay(const Common::Math::Vec3f& startPos,const Common::Math::Vec3f& endPos,const float32 minHeight);

				//! \brief Checks whether the endPos can be reached from startPos on a straight line.
				//! \param startPos		start-position of the ray
				//! \param startNodeID	the NavMesh node ID containing the startPos
				//! \param endPos		end-position of the ray
				//! \param minHeight	the minimum height of the space above the NavMesh node polygon
				//! \return				true, if the endPos could be reached, and false otherwise  (including dir/startPos/endPos invalid)
				XAIT_MAP_DLLAPI bool traceRay(const Common::Math::Vec3f& startPos,const GlobalNodeID startNodeID,const Common::Math::Vec3f& endPos,const float32 minHeight);

				//! \brief Enable/disable storing of NavNodeIDs that have been crossed during trace [default: disabled]
				//! \param flag flag indicating whether the NavNodeIDs should be stored (true) or not (false)
				XAIT_MAP_DLLAPI void storeCrossedNodes(const bool flag);

				//! \brief Gets a list of nodes that have been crossed.
				//! \returns a list of the ids of the nodes that have been crossed
				XAIT_MAP_DLLAPI const Common::Container::Vector<CrossedNode>& getCrossedNodes() const;

				//! \brief Adds a walkable surface to the tracer.
				//! 
				//! Use this function if you want to restrict the trace to certain surfaceID-types.
				//! If no surface is given, the trace won't have surfaceID restrictions
				//! \param surfaceID the surfaceID that should be added
				XAIT_MAP_DLLAPI void addWalkableSurface(uint32 surfaceID);

				//! \brief Sets the cost function.
				//!
				//! If no costFunction is set cost will always be zero.
				//! \param costFunc the cost function
				XAIT_MAP_DLLAPI void setCostFunc(const CompiledSearchConfig::Ptr& costFunc);

				//! \brief Gets the sum of the costs of the last trace.
				//! \return the cost of the last trace, or 0 if no trace was done or the costFunc was not set
				XAIT_MAP_DLLAPI float32 getCost() const;

				inline bool getHitOccurred() const
				{
					return !mNoHitOccured;
				}

				inline const NavMeshContainer* getNavMesh() const
				{
					return mNavMesh;
				}

				inline void setNavMesh(const NavMeshContainer* navMesh)
				{
					mNavMesh = navMesh;
				}

				//! /brief enable checking if the end point is inside the given endpolygon
				//! /param flag		true if enabled, false otherwise
				//! /remark enabled by default;
				inline void setCheckLastPolygon(bool flag)
				{
					mCheckLastPolygon = flag;
				}

				inline bool getCheckLastPolygon() const
				{
					return mCheckLastPolygon;
				}

				//! \brief gets the endpoint of the ray.
				const Common::Math::Vec3f& getEndPoint() const
				{
					return mEndPos;
				}

				GlobalNodeID getEndNodeID() const
				{
					return mLastNodeID;
				}

			private:
				

				//! \brief Initializes the tracing algorithm.
				//! \param startPos the start position of the ray
				//! \param startNodeID the ID of the NavMesh node containing the start position
				//! \param dir the direction of the ray
				//! \param maxT the maximum distance to the hit point
				//! \param minHeight the minimum height of the polygon
				bool    init(const Common::Math::Vec3f startPos,const GlobalNodeID startNodeID,const Common::Math::Vec3f& dir, const float32 maxT, const float32 minHeight, const bool overRidePositionCheck = false);

				//! \brief moves the startpoint inside the polygon 
				void fixStartPoint();

				//! \brief Traces the NavMesh.
				float32 traceRay(const GlobalNodeID& nodeID);

				float32 handleEndReached( GlobalNodeID currentNodeID, float32 shortestEdgeDistance );
				
				//! \brief Gets the height of the space above the polygon corresponding to the given node ID.
				//! \param nodeID the node ID
				//! \return the height
				XAIT::float32 getNavNodeHeight(const GlobalNodeID& nodeID) const;

				//! \brief Checks whether the surface of the given node is walkable or not.
				//!
				//!	If there are no walkable surfaces explicitly specified, then all surface are considered to be walkable.
				//! \param nodeID the ID of the node
				//! \return true, if the surface is walkable, and false otherwise
				bool checkSurfaceType(const GlobalNodeID& nodeID);
			
				//! \brief gets the next node if the ray crosses a vertex
				GlobalNodeID getNextNodeOverVertex(GlobalNodeID nodeID, uint32 vertexContainingEdge,const Common::Math::Vec2f& dir2D, const NavMeshSector* sector);

				GlobalNodeID searchNextNodeOverVertex( GlobalNodeID nodeID, const Common::Math::Vec3f* vertices, const Common::Math::Vec2f& vertex, const uint32 vertexID, const Common::Math::Vec2f& directionNormal, bool ccw );
				
				//! \brief searches the next edge of a vertex in a certain direction
				//! \param node			the node that contains the edge
				//! \param vertexID		the vertexID
				//! \param ccw			the direction (true = counter clock wise)
				//! \returns the edge id or X_MAX_UINT32 if the node does not use the given vertex
				uint32 getNextEdgeByDirection(const NavMeshNode& node, uint32 vertexID, const bool ccw);
				uint32 getNextEdgeByDirection(const NavMeshNode& node, GlobalNodeID lastNodeID, uint32 lastEdgeID, const bool ccw);

				//! \brief gets the 2d start point of a polygon edge
				//! \param startPoint2D		the return value
				//! \param node				the navmesh node
				//! \param edgeID			the edge id containing the startPoint (usedEdgeID = edge % polySize)
				//! \param vertices			the vertex list
				void get2DEdgeStartPoint(Common::Math::Vec2f& startPoint2D, const NavMeshNode& node, uint32 edgeID, const Common::Math::Vec3f* vertices) const;

				bool intersectNodeEdgeRay( float32& t,  float32& edgeParameter, const Common::Math::Vec2f& start, const Common::Math::Vec2f& dir, const Common::Math::Vec2f& lineA, const Common::Math::Vec2f& lineB );

				const NavMeshContainer* mNavMesh;							//!< the NavMesh

				Common::Math::Vec3f mStartPos;								//!< the starting position
				Common::Math::Vec3f mEndPos;								//!< the end position
				Common::Math::Vec2f mStartPos2d;							//!< the starting position in 2D
				Common::Math::Vec3f mDir;									//!< the starting position in 3D
				Common::Math::Vec2f mDir2d;									//!< the direction in 2D
				
				GlobalNodeID mStartNodeID;									//!< the start node
				GlobalNodeID mLastNodeID;									//!< stores last visited node
				
				float32 mT;													//!< the distance to the hit point
				float32 mMaxT;												//!< the maximum distance to the hit point
				float32 mMinHeight;											//!< the minimum height of the space above the NavMesh polygon
				float32	mCosts;												//!< the cost of a completed trace

				XAIT::Common::Container::Vector<uint32> mWalkableSurfaces;	//!< the walkable surface IDs
				Common::Container::Vector<CrossedNode>	mCrossedNodes;		//!< the NavMesh

				CompiledSearchConfig::Ptr				mCostFunc;			//!< the cost function

				bool	mStoreCrossedNodes;									//!< flag indicating whether or not crossed nodes should be stored
				bool	mTraceFailed;										//!< used for internal error recovery
				bool    mNoHitOccured;
				bool	mCheckLastPolygon;

			};
		}
	}
}

