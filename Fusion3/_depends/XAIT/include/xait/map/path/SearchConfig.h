// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/GlobalNodeID.h>
#include <xait/map/MapMemoryPool.h>
#include <xait/map/path/MultiStateCharacter.h>
#include <xait/map/path/CompiledSearchConfig.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			//! \brief	this class is used to configure search request.
			//!			it contains all character information that is needed to perform an path search.
			//!			however it has no dependencies to a navmesh and has to be compiled before usage
			class SearchConfig : public Common::Memory::StaticMemoryManaged<MapMemoryPool::getLowAllocThreadSafeAllocator>
			{
			public:
				//! \brief constructor
				XAIT_MAP_DLLAPI SearchConfig();

				//! \brief destructor
				XAIT_MAP_DLLAPI ~SearchConfig();
				
				//! \brief adds a character to the search config
				//! \param characterName	the name of the character
				XAIT_MAP_DLLAPI void addCharacter(const Common::String& characterName);

				//! \brief adds a character to the search config 
				//! \param character		an existing character config
				XAIT_MAP_DLLAPI void addCharacter(const CharacterConfig& character);

				//! \brief adds a multistatecharacter to the search config
				//! \param character		an existing multistate character
				XAIT_MAP_DLLAPI void addCharacter(const MultiStateCharacter& character);

				//! \brief gets the contained characters
				//! \returns all characters that are used by the search config
				inline const Common::Container::Vector<MultiStateCharacter>& getCharacters() const
				{
					return mMultiStateCharacters;
				}

				//! \brief creates a search config that depends on a certain navigation mesh
				//! \param	navMesh		the navigation mesh
				//! \returns a compiled search config
				XAIT_MAP_DLLAPI virtual CompiledSearchConfig::Ptr compile(const NavMeshContainer* navMesh) const;

			protected:
				Common::Container::Vector<MultiStateCharacter>		mMultiStateCharacters;
			};
		}
	}
}
