// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/String.h>
#include <xait/common/memory/StaticMemoryManaged.h>
#include <xait/map/CompileDefines.h>
#include <xait/map/MapMemoryPool.h>
#include <xait/map/NavMeshContainer.h>
#include <xait/map/path/PathSearchInstance.h>

namespace XAIT
{
	namespace Map
	{
		namespace Path
		{
			//! \brief	This class contains the constant data (e.g. search space) that is needed to proceed a path search
			//!			different kinds of searches needs their own implemention (e.g. midpoint path search,...)
			//!			This base class defines the interface of an path search
			//!			To start a concrete path search you have to instantiate the path search 
			class PathSearch : public Common::Memory::StaticMemoryManaged<MapMemoryPool::getLowAllocThreadSafeAllocator>
			{
			public:
				//! \brief constructor
				//! \param	navMeshContainer		the navmesh the path searches works on
				XAIT_MAP_DLLAPI PathSearch(const NavMeshContainer* navMeshContainer);

				//! \brief destructor
				XAIT_MAP_DLLAPI virtual ~PathSearch();

				//! \brief create an instance of the path search
				//! \returns a pathsearch instance
				virtual PathSearchInstance* createInstance(const Common::Memory::Allocator::Ptr& allocator= Common::Memory::Allocator::Ptr()) = 0;


				//! \brief get the used navmesh container
				//! \returns the used navigation mesh
				XAIT_MAP_DLLAPI const NavMeshContainer* getNavMeshContainer() const;

				//! \brief Get the type name of the path search
				virtual Common::String getTypeName() const= 0;
			
			protected:

				const NavMeshContainer*				mNavMesh;
	
				//! \brief gets an allocator for a new instance, either takes the passed allocator or uses its own allocator
				inline Common::Memory::Allocator::Ptr getInstanceAllocator(const Common::Memory::Allocator::Ptr& alloc)
				{
					if (alloc == NULL)
						return getAllocator();
					else
						return alloc;
				}
			};

		}
	}
}
