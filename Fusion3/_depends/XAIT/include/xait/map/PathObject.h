// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/map/MapMemManagedObject.h>
#include <xait/common/String.h>
#include <xait/common/serialize/SerializeStorage.h>
#include <xait/map/CompileDefines.h>

namespace XAIT
{
	namespace Map
	{
		class PathObjectSectorData;
		class NavMeshContainer;

		//! \brief Base class for all path objects
		class PathObject : public MapMemManagedObject
		{
		public:
			//! Classes of path objects
			enum PathObjectClass
			{
				CLASS_SOLID		= 0x0100,		//!< path objects which have a solid (geomtry) representation and are not build only
				CLASS_GRAPH		= 0x0200,		//!< graph objects
				CLASS_BUILDONLY = 0x0300,		//!< path object which are only relevant for the navmesh build process
				CLASS_GROUP		= 0x1000,		//!< path object group
				CLASS_MASK		= 0xFF00,		
			};

			//! Types of path objects
			enum PathObjectType
			{
				TYPE_SPAWNPOINT		= CLASS_BUILDONLY | 0x01,
				TYPE_BLOCKEDAREA	= CLASS_BUILDONLY | 0x02,
				TYPE_BLOCKINGBOX	= CLASS_SOLID | 0x03,
				TYPE_BLOCKINGPLANE	= CLASS_SOLID | 0x04,
				TYPE_GROUP			= CLASS_GROUP | 0x00,
			};

			XAIT_MAP_DLLAPI PathObject(const uint16 typeID, const Common::String& name);

			virtual ~PathObject()
			{}

			//! \brief Gets the type of this path object
			inline uint16 getTypeID() const
			{
				return mTypeID;
			}

			//! \brief Gets the class of this path object
			inline uint16 getClassID() const
			{
				return mTypeID & CLASS_MASK;
			}

			//! \brief Gets the name of this path object
			inline const Common::String& getName() const
			{
				return mName;
			}

			//! \brief Gets the NavmeshContainer this path object is from
			//! \returns The point to the owning navmesh container or NULL if standalone object
			inline NavMeshContainer* getOwner() const
			{
				return mOwner;
			}

			//! \brief Gets a copy of the path object
			//! \remarks Owner is not copied
			virtual PathObject* clone() const= 0;

			//! \brief Factory method to create a certain path object by its type id
			//! \returns The new path object or NULL if type id is unknown.
			//! \remarks Internal function
			XAIT_MAP_DLLAPI static PathObject* create(const uint16 typeID, const Common::String& name);

			//! \brief Deserialize a path object from a serialize storage.
			//! \returns The path object or NULL if something went wrong
			//! \remarks Internal function
			XAIT_MAP_DLLAPI static PathObject* deserialize(Common::Serialize::SerializeStorage* serializer);

			//! \brief Serialize a path object to a serialize storage
			//! \remarks Internal function
			XAIT_MAP_DLLAPI void serialize(Common::Serialize::SerializeStorage* serializer) const;

			//! \brief The pathobject should refresh the usage of that sector data
			//! \remarks Internal function
			virtual void refreshSectorData(PathObjectSectorData* data) const {};

			//! \remarks Internal function
			virtual void detachSectorData(PathObjectSectorData* data) const {};

			//! \remarks Internal function
			virtual void appendSectorData(PathObjectSectorData* data) {};

			//! \remarks Internal function
			virtual void removeSectorData(PathObjectSectorData* data) {};

			//! \remarks Internal function
			//! \remarks Called after a set of path objects has been given a new parent, no need to set mOwner
			virtual void handleNewOwner(NavMeshContainer* newOwner) {};

			//! \remarks Internal function
			virtual uint32 getMemoryUsage() const= 0;

		protected:

			virtual bool initFromStream(Common::Serialize::SerializeStorage* serializer)= 0;
			virtual void writeToStream(Common::Serialize::SerializeStorage* serializer) const= 0;

			virtual void handleSerializeFinished() {};

		private:
			friend class NavMeshContainer;

			Common::String		mName;
			NavMeshContainer*	mOwner;
			uint16				mTypeID;

			void setOwner(NavMeshContainer* owner)
			{
				mOwner= owner;
			}

		};
	}
}
