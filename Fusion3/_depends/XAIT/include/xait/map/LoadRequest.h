// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/String.h>

namespace XAIT
{
	namespace Map
	{
		//! \brief Resource load request.
		struct LoadRequest
		{
			Common::String	mResourceName;		//!< full name of the resource which should be loaded
		};
	}
}


