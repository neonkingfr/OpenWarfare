// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/String.h>
#include <xait/common/serialize/SerializeStorage.h>
#include <xait/common/crypto/MD5Builder.h>
#include <xait/map/MapMemManagedObject.h>
#include <xait/map/CompileDefines.h>

namespace XAIT {
namespace Map {

	//! Description of the custom data layout attached to each navmesh node.
	//! The description will be specified during navmesh build and creates a special memory
	//! array inside the navmesh which is maintained alongside the navmesh nodes. You 
	//! can have access to this custom data through NavMeshSector and NavMeshContainer.
	//! \brief Description of the custom data layout attached to each navmesh node
	class CustomDataDesc
	{
	public:
		struct DataMember
		{
			Common::String	mName;
			uint16			mOffset;
			uint16			mSize;
		};

		enum
		{
			INVALID_ID	= X_MAX_UINT32,
		};

		XAIT_MAP_DLLAPI CustomDataDesc();


		//! \brief Validates the data size, not every data size is supported
		//! \remark Valid data sizes are 1,2,4,8
		XAIT_MAP_DLLAPI bool isValidDataSize(const uint32 byteSize);

		//! \brief Adds a new custom data member
		//! \param name		name of the custom data member
		//! \param byteSize	size of the member in bytes
		//! \returns ID of the new data member
		//! \remark Not all byte sizes are allowed, see isValidDataSize
		XAIT_MAP_DLLAPI uint32 addDataMember(const Common::String& name, const uint32 byteSize);

		//! \brief Adds a new custom data member
		//! \param name		name of the custom data member
		//! \returns ID of the new data member
		//! \remark The size is derived from the T_DATA template argument
		template<typename T_DATA>
		inline uint32 addDataMember(const Common::String& name) { return addDataMember(name,sizeof(T_DATA)); }

		//! \brief Gets a certain custom data member
		inline const DataMember& getDataMember(const uint32 id) const 
		{ 
			X_ASSERT_MSG2(id < mDataMembers.size(),"Custom data id is out of range",id,mDataMembers.size());
			return mDataMembers[id]; 
		}

		//! \brief Searches for a specific custom data member by its name
		//! \param name		custom data name
		//! \returns ID of the found data member or CustomDataMember::INVALID_ID if not found
		XAIT_MAP_DLLAPI uint32 findDataMember(const Common::String& name) const;

		//! \brief Removes all custom data members
		inline void clear() 
		{ 
			mDataMembers.clear();
			mDataSize= 0;
		}

		//! \brief Tests if the custom data has no members
		inline bool empty() const { return mDataMembers.empty(); }

		//! \brief Gets the byte size of the custom data
		inline uint32 getByteSize() const { return mDataSize; }

		//! \brief Gets the offset of the custom data
		inline uint32 getOffset() const { return mDataOffset; } 

		//! \brief Gets the padding at the start of the custom data
		//! \remark There is padding if the offset is not a multiple of the first data member size.
		inline uint32 getPadding() const { return mDataPadding; }

		//! \brief Get all data members
		inline const StaticContainer<DataMember>::Vector& getDataMembers() const { return mDataMembers; }

		//! \brief initializes the internal data padding
		//! \remarks This method must be called AFTER the custom data is defined
		//! \remarks This is an internal function and should not be called directly
		void finalize(const uint32 prefixSize);

		bool initFromStream(Common::Serialize::SerializeStorage* serializer);
		void writeToStream(Common::Serialize::SerializeStorage* serializer) const;
		void extendBuildChecksum(Common::Crypto::MD5Builder* checksum) const;

	private:
		uint32									mDataPadding;
		uint32									mDataOffset;
		uint32									mDataSize;
		StaticContainer<DataMember>::Vector		mDataMembers;

		inline uint32 computeUpperAlignment(const uint32 pos, const uint32 alignment) const 
		{ 
			if (alignment > 0)
			{
				const uint32 modu= pos % alignment;
				if (modu == 0)
					return pos;

				return pos + alignment - modu; 
			}
			else
				return pos;
		}

		uint32 computeDataPadding(const uint32 prefixSize) const;
		void computeDataSize();
	};

}}