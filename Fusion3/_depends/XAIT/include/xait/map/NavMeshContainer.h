// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/container/Vector.h>
#include <xait/common/container/Pair.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/math/Matrix4.h>
#include <xait/common/geom/Ray2D.h>
#include <xait/common/geom/Ray3D.h>
#include <xait/common/geom/AABB2D.h>
#include <xait/common/geom/AABB3D.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/io/Stream.h>
#include <xait/common/io/StreamDictionary.h>
#include <xait/common/event/Event.h>
#include <xait/common/serialize/SerializeStorage.h>
#include <xait/map/CompileDefines.h>
#include <xait/map/CharacterInfo.h>
#include <xait/map/SurfaceType.h>
#include <xait/map/LoadRequest.h>
#include <xait/map/NavMeshSector.h>
#include <xait/map/GlobalNodeID.h>
#include <xait/map/PathObject.h>
#include <xait/map/MapMemManagedObject.h>
#include <xait/map/CustomDataDesc.h>

#define XAIT_MAP_DEFAULT_VIEW_NAME		"default"
#define XAIT_MAP_CONTAINER_FILEVERSION	35

namespace XAIT
{
	namespace Map
	{
		// forward declarations
		struct NavMeshView;
		class NavMeshSectorManager;
		class NavMeshSectorLayout;
		class NavMeshSerializer;
		class NavMeshContainer;
		class HighLevelGraph;

		namespace Build
		{
			class NavMeshBuild;
			class NavMeshBuildConfig;
			class NavMeshOptimizer;
		}

		//! Sector delegate that provides the NavMesh container and the sector id which have changed
		typedef Common::Event::Delegate<void (NavMeshContainer*,const uint32)>	SectorDelegate;

		//! NavMes delegate that is provided by the navmesh container if the navmesh changes
		typedef Common::Event::Delegate<void (NavMeshContainer*)> NavMeshDelegate;
	
		//! \brief Navigation Mesh Container. Contains the result of a navigation mesh build.
		class NavMeshContainer : public NonVirtualMapMemManagedObject
		{
		public:
			enum 
			{
				INVALID_HEIGHTID	= 0xFFFFFFFF,	//!< invalid height id
				INVALID_VIEWID		= 0xFFFFFFFF,	//!< invalid view id
				INVALID_SECTORID	= 0xFFFFFFFF,	//!< invalid sector id
			};

			//! \brief Iterator which can iterate over all NavMesh nodes in all sectors.
			class NodeIterator
			{
				NavMeshContainer*		mNavMesh;
				uint32					mCurrSector;
				uint32					mCurrNode;

			public:
				//! \brief Constructor.
				//! \param navMesh the NavMeshContainer on which the iterator operates
				NodeIterator(NavMeshContainer* navMesh)
					: mNavMesh(navMesh)
				{
					setBegin();
				}

				//! \brief Increment operator. Iterates to the next NavMesh node.
				NodeIterator& operator++()
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot increment an invalid iterator");
					++mCurrNode;
					if (mCurrNode >= mNavMesh->mSectors[mCurrSector]->getNumNodes())
					{
						mCurrNode= 0;
						// get next sector
						++mCurrSector;
						for(; mCurrSector < mNavMesh->mNumSectors; ++mCurrSector)
						{
							NavMeshSector* sector= mNavMesh->mSectors[mCurrSector];
							if (sector && sector->getNumNodes() > 0)
								break;
						}
					}
					return *this;
				}

				//! \brief Dereference operator.
				//! \return the current NavMesh node the iterator points to
				inline const NavMeshNode& operator*() const
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot dereference an invalid iterator");
					return mNavMesh->mSectors[mCurrSector]->getNode(mCurrNode);
				}

				//! \brief Dereference operator.
				//! \return the current NavMesh node the iterator points to
				inline NavMeshNode& operator*()
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot dereference an invalid iterator");
					return mNavMesh->mSectors[mCurrSector]->getNode(mCurrNode);
				}

				//! \brief Memberdereference operator
				//! \return the current NavMesh node the iterator points to
				inline NavMeshNode* operator->()
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot dereference an invalid iterator");
					return &mNavMesh->mSectors[mCurrSector]->getNode(mCurrNode);
				}

				//! \brief Gets the node ID of the current NavMesh node.
				//! \return the node ID of the current NavMesh node
				inline GlobalNodeID getNodeID() const 
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot get the node id from an invalid iterator");
					return GlobalNodeID(mCurrSector,mCurrNode);
				}

				//! \brief Gets the sector of the current node
				inline const NavMeshSector* getSector() const
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot get sector from an invalid iterator");
					return mNavMesh->mSectors[mCurrSector];
				}

				//! \brief Gets the sector of the current node
				inline NavMeshSector* getSector()
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot get sector from an invalid iterator");
					return mNavMesh->mSectors[mCurrSector];
				}

				//! \brief Checks whether the iterator is valid.
				//! \return true, when the iterator is valid, and false otherwise
				inline bool isValid() const
				{
					return mCurrSector != mNavMesh->mNumSectors && mCurrNode < mNavMesh->mSectors[mCurrSector]->getNumNodes();
				}

				//! \brief Resets the iterator to the first NavMesh node in the container.
				void setBegin()
				{
					// search first valid sector
					mCurrNode= 0;
					for(mCurrSector= 0; mCurrSector < mNavMesh->mNumSectors && !mNavMesh->mSectors[mCurrSector]; ++mCurrSector)
					{}
				}

				//! \brief Invalidates the iterator.
				void setEnd()
				{
					mCurrNode= 0;
					mCurrSector= mNavMesh->mNumSectors;
				}
			};


			
			//! \brief ConstIterator which can iterate over all NavMesh nodes in all sectors.
			class ConstNodeIterator
			{
				const NavMeshContainer*	mNavMesh;
				uint32					mCurrSector;
				uint32					mCurrNode;

			public:
				//! \brief Constructor.
				//! \param navMesh the NavMeshContainer on which the iterator operates
				ConstNodeIterator(const NavMeshContainer* navMesh)
					: mNavMesh(navMesh)
				{
					setBegin();
				}

				//! \brief Increment operator. Iterates to the next NavMesh node.
				ConstNodeIterator& operator++()
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot increment an invalid iterator");
					++mCurrNode;
					if (mCurrNode >= mNavMesh->mSectors[mCurrSector]->getNumNodes())
					{
						mCurrNode= 0;
						// get next sector
						++mCurrSector;
						for(; mCurrSector < mNavMesh->mNumSectors; ++mCurrSector)
						{
							NavMeshSector* sector= mNavMesh->mSectors[mCurrSector];
							if (sector && sector->getNumNodes() > 0)
								break;
						}
					}
					return *this;
				}

				//! \brief Dereference operator.
				//! \return the current NavMesh node the iterator points to
				const NavMeshNode& operator*() const
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot dereference an invalid iterator");
					return mNavMesh->mSectors[mCurrSector]->getNode(mCurrNode);
				}

				//! \brief Memberdereference operator
				//! \return the current NavMesh node the iterator points to
				const NavMeshNode* operator->() const
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot dereference an invalid iterator");
					return &mNavMesh->mSectors[mCurrSector]->getNode(mCurrNode);
				}

				//! \brief Gets the node ID of the current NavMesh node.
				//! \return the node ID of the current NavMesh node
				inline GlobalNodeID getNodeID() const 
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot get the node id from an invalid iterator");
					return GlobalNodeID(mCurrSector,mCurrNode);
				}

				//! \brief Gets the sector of the current node
				inline const NavMeshSector* getSector() const
				{
					X_ASSERT_MSG_DBG(isValid(),"Cannot get sector from an invalid iterator");
					return mNavMesh->mSectors[mCurrSector];
				}

				//! \brief Checks whether the iterator is valid.
				//! \return true, when the iterator is valid, and false otherwise
				inline bool isValid() const
				{
					return mCurrSector != mNavMesh->mNumSectors && mCurrNode < mNavMesh->mSectors[mCurrSector]->getNumNodes();
				}

				//! \brief Resets the iterator to the first NavMesh node in the container.
				void setBegin()
				{
					// search first valid sector
					mCurrNode= 0;
					for(mCurrSector= 0; mCurrSector < mNavMesh->mNumSectors && !mNavMesh->mSectors[mCurrSector]; ++mCurrSector)
					{}
				}

				//! \brief Invalidates the iterator.
				void setEnd()
				{
					mCurrNode= 0;
					mCurrSector= mNavMesh->mNumSectors;
				}
			};

	

			//! \brief Constructor. Creates the NavMeshContainer object
			XAIT_MAP_DLLAPI NavMeshContainer();

			//! \brief Constructor. Creates the NavMeshContainer object with a particular build configuration and with a particular sector layout.
			//! \param buildConfig the build configuration
			//! \param navMeshLayout the sector layout
			XAIT_MAP_DLLAPI NavMeshContainer(const Build::NavMeshBuildConfig& buildConfig, NavMeshSectorLayout* navMeshLayout);

			//! \brief Destructor. Destroys the NavMeshContainer object.
			XAIT_MAP_DLLAPI ~NavMeshContainer();

			//! \brief Gets a reference to a NavMesh node by its global node ID.
			//! \param nodeID	global NavMesh id, must not be invalid
			//! \return a reference to the NavMeshNode
			inline const NavMeshNode& getNode(const GlobalNodeID nodeID) const
			{
				const uint32 sectorID= nodeID.getSectorID();
				X_ASSERT_MSG_DBG2(sectorID < mNumSectors,"SectorID out of range",sectorID,mNumSectors);
				X_ASSERT_MSG_DBG1(mSectors[sectorID],"SectorID invalid - no sector loaded",sectorID);

				return mSectors[sectorID]->getNode(nodeID.getSectorNodeID());
			}

			//! \brief Gets a reference to a NavMesh node by its global node ID.
			//! \param nodeID	global NavMesh id, must not be invalid
			//! \return a reference to the NavMeshNode
			inline NavMeshNode& getNode(const GlobalNodeID nodeID)
			{
				const uint32 sectorID= nodeID.getSectorID();
				X_ASSERT_MSG_DBG2(sectorID < mNumSectors,"SectorID out of range",sectorID,mNumSectors);
				X_ASSERT_MSG_DBG1(mSectors[sectorID],"SectorID invalid - no sector loaded",sectorID);

				return mSectors[sectorID]->getNode(nodeID.getSectorNodeID());
			}

			//! \brief Checks whether a NavMesh node ID refers to a loaded node.
			//! \param nodeID the NavMesh node ID that you want to test
			//! \return true, if the NavMesh node ID belongs to a valid NavMeshNode, and false otherwise
			//! \remark When the check fails, detailed information will be printed to the log.
			XAIT_MAP_DLLAPI bool isValidNodeID(const GlobalNodeID nodeID) const;

			//! \brief Gets a const iterator to iterate over all loaded NavMesh nodes.
			//! \return a const iterator
			inline ConstNodeIterator getNodeIterator() const
			{
				return ConstNodeIterator(this);
			}

			//! \brief Gets an iterator to iterate over all loaded NavMesh nodes.
			//! \return an iterator
			inline NodeIterator getNodeIterator()
			{
				return NodeIterator(this);
			}

			//! \brief Gets the midpoint of the polygon corresponding to a NavMesh node.
			//! \param globalNodeID		ID of the NavMesh node for which the midpoint should be computed
			//! \return the midpoint
			inline Common::Math::Vec3f getNodeMiddlePoint(const GlobalNodeID globalNodeID) const
			{
				const NavMeshSector* sector= getSector(globalNodeID.getSectorID());
				X_ASSERT_MSG1(sector != NULL,"Invalid nodeIndex",globalNodeID.getSectorID());

				return sector->getNodeMiddlePoint(globalNodeID.getSectorNodeID());
			}

			//! \brief Get the normal of the plane through a navmesh node
			//! \param globalNodeID		ID of the NavMesh node for which the node normal should be computed
			//! \remark The normal has to be computed and is not stored.
			inline Common::Math::Vec3f getNodeNormal(const GlobalNodeID globalNodeID) const
			{
				const NavMeshSector* sector= getSector(globalNodeID.getSectorID());
				X_ASSERT_MSG1(sector != NULL,"Invalid nodeIndex",globalNodeID.getSectorID());

				return sector->getNodeNormal(globalNodeID.getSectorNodeID());
			}

			//! \brief Gets the start and the end point of a polygon edge.
			//! \param startPos			returned start position of the edge
			//! \param endPos			returned end position of the edge
			//! \param globalNodeID		index of the NavMesh containing the edge
			//! \param edgeID			local edge index
			void getEdgeEndpoints(Common::Math::Vec3f& startPos, Common::Math::Vec3f& endPos, const GlobalNodeID globalNodeID, uint32 edgeID) const
			{
				const NavMeshSector* sector= getSector(globalNodeID.getSectorID());
				X_ASSERT_MSG1(sector != NULL,"Invalid nodeIndex",globalNodeID.getSectorID());

				sector->getEdgeEndpoints(startPos,endPos,globalNodeID.getSectorNodeID(),edgeID);
			}


			//! \brief Checks whether a point is inside a certain node.
			//! \param point			point which should be tested
			//! \param globalNodeID		node which should be tested
			//! \return true, if the point is inside the volume of the specified node, and false otherwise
			bool isPointInsideNode(const Common::Math::Vec3f& point, const GlobalNodeID globalNodeID) const
			{
				const NavMeshSector* sector= getSector(globalNodeID.getSectorID());
				X_ASSERT_MSG1(sector != NULL,"Invalid nodeIndex",globalNodeID.getSectorID());

				return sector->isPointInsideNode(point,globalNodeID.getSectorNodeID());
			}

			//! \brief Compute the minimal distance between the polygon edges of a certain node and a given point.
			//! \param point			the point to check
			//! \param globalNodeID		the ID of the NavMesh node whose edges will be checked
			//! \return	the distance to the nearest edge
			float32 getDistanceToNearestEdgeOfNode(const Common::Math::Vec3f& point, const GlobalNodeID globalNodeID) const
			{
				const NavMeshSector* sector= getSector(globalNodeID.getSectorID());
				X_ASSERT_MSG1(sector != NULL,"Invalid nodeIndex",globalNodeID.getSectorID());

				return sector->getDistanceToNearestEdgeOfNode(point,globalNodeID.getSectorNodeID());
			}


			//! \brief Gets a certain sector of a node by its sector ID.
			//! \param sectorID the ID of the sector
			//! \return the NavMeshSector, or NULL when it has not been loaded or build yet.
			inline NavMeshSector* getSector(const uint32 sectorID)
			{
				X_ASSERT_MSG2(sectorID < mNumSectors,"SectorID out of range",sectorID,mNumSectors);
				return mSectors[sectorID];
			}

			//! \brief Gets a certain sector of a node by its sector ID.
			//! \param sectorID the ID of the sector
			//! \return the NavMeshSector, or NULL when it has not been loaded or build yet.
			inline const NavMeshSector* getSector(const uint32 sectorID) const
			{
				X_ASSERT_MSG2(sectorID < mNumSectors,"SectorID out of range",sectorID,mNumSectors);
				return mSectors[sectorID];
			}

			//! \brief Gets the number of sectors that can be loaded.
			//! 
			//! Note that this number is not necessarily the same as the number of sectors that have actually been loaded.
			//! \return the number of sectors
			inline uint32 getNumSectors() const
			{
				return mNumSectors;
			}

			//! \brief Gets the maximal sector ID.
			//! 
			//! This depends on the number of loaded sectors, but it is not the number of loaded sectors.
			//! \return the maximal sector ID
			inline uint32 getMaxSectorID() const
			{
				return mNumSectors - 1;
			}

			//! \brief Returns the maximum sector size a sector can have.
			//! 
			//! There could be sectors of smaller size, use the getSectorSize method on an particular sector to get its actual sector size.
			//! \return the maximum sector size
			//! \see getSectorSize
			XAIT_MAP_DLLAPI float32 getMaxSectorSize() const;

			//! \brief Get number of nodes from the current state of the nav mesh
			//! \remark This considers only loaded sectors
			XAIT_MAP_DLLAPI uint32 getNumNodes() const;

			//! \brief Gets the node that matches a specified point
			//!
			//! The point must be inside the navigation mesh. If it is outside of the navigation mesh, this method
			//! will not search for the nearest alternative. You must have initialized the node search system in order to use this function.
			//! \param point					the position to where the node is searched
			//! \param applyHeightConstraints	checks if the given point lies inside the bounding box of the found node (true = pre xaitMap 2.8 behaviour)
			//! \returns The id of the found node, or INVALID_NODEID if no matching node has been found
			//! \remark if applyHeightContraints is disabled, the function finds the nearest node in gravity directions (negative heights have double costs). 
			XAIT_MAP_DLLAPI GlobalNodeID searchNode(const Common::Math::Vec3f& point,bool applyHeightConstraints = false) const;

			//! \brief Gets the nearest position on the navmesh to the given position (using euclidean distance)
			//! \param nearestNodeID	the nodeID of the nearest found node (invalid if no node in given radius found)
			//! \param point			the given position
			//! \param radius			the search radius around the given position
			//! \returns the nearest point on the navigation mesh. Vec3f(X_MAX_FLOAT32,X_MAX_FLOAT32,X_MAX_FLOAT32) if no position was found
			XAIT_MAP_DLLAPI Common::Math::Vec3f searchNearestPoint(GlobalNodeID& nearestNodeID, const Common::Math::Vec3f& point, float32 radius) const;

			//! Intersects a 3D ray with the navigation mesh. The mesh will be handled as if it would be normal geometry that can be intersected
			//! by a ray. The ray travels through world space. It is not traced on the surface of the navigation mesh. If you want to trace rays in
			//! in the navigation space use \see NavMeshRayTrace.
			//! \brief Intersect a 3D ray with the navigation mesh
			//! \param ray		Ray which should intersect the navmesh
			//! \return True if there was an intersection, false otherwise.
			//! \remark Only the surface of the navmesh will be considered, not the volumes they represent.
			//! \remark The ray will contain the intersecting node in mObjectID and the exact hit point.
			XAIT_MAP_DLLAPI bool intersect(Common::Geom::Ray3D<GlobalNodeID>& ray);

			//! \brief Gets the character description of a character that has been defined during the NavMesh build.
			//! \param name		name of the character
			//! \returns the character information, or NULL if no character was found.
			XAIT_MAP_DLLAPI const CharacterInfo* findCharacter(const Common::String& name) const;

			//! \brief Gets the character description of a character that has been defined during the NavMesh build.
			//! \param userID	user id of the character
			//! \returns the character information, or NULL if no character was found.
			XAIT_MAP_DLLAPI const CharacterInfo* findCharacter(const int64 userID) const;

			//! \brief Gets a vector containing all character infos.
			//! \return a pointer to the start of an array containing all character infos
			//! \see getNumCharacters()
			const CharacterInfo* getCharacters() const
			{
				return mCharacters;
			}

			//! \brief Get the number of characters
			//! \return the number of characters in the character array
			//! \see getCharacters()
			uint32 getNumCharacters() const
			{
				return mNumCharacters;
			}

			//! \brief Gets the surface type description of the surface type given by the name.
			//! \param name		name of the surface type
			//! \returns the surface type, or NULL if no matching type was found
			XAIT_MAP_DLLAPI const SurfaceType* findSurfaceType(const Common::String& name) const;

			//! \brief Gets the surface type description of the surface type given by the name.
			//! \param typeID		the type ID of the surface type
			//! \returns the surface type, or NULL if no matching type was found
			XAIT_MAP_DLLAPI const SurfaceType* findSurfaceType(const uint16 typeID) const;

			//! \brief Get the pointer to the surface type array
			//! \see getNumSurfaceTypes()
			const SurfaceType* getSurfaceTypes() const
			{
				return mSurfaceTypes;
			}

			//! \brief Get the number of surface types in the surface type array
			//! \see getSurfaceTypes()
			uint32 getNumSurfaceTypes() const
			{
				return mNumSurfaceTypes;
			}

			//! \brief Find a specified pathobject
			//! \returns The path object or NULL if path object not found
			XAIT_MAP_DLLAPI PathObject* findPathObject(const Common::String& name) const;

			//! \brief Find a specified pathobject with hint of type
			//! \param typeID	typeid of the pathobject to search for
			//! \param name		name of the pathobject
			//! \returns The path object or NULL if path object with given name and type not found.
			XAIT_MAP_DLLAPI PathObject* findPathObject(const uint16 typeID, const Common::String& name) const;

			//! \brief Finds the index in the path objects array
			//! \returns The index of the path object in the path objects array returned by NavMeshContainer::getPathObjects or X_MAX_UINT32 if not found
			XAIT_MAP_DLLAPI uint32 findPathObjectID(const Common::String& name) const;

			//! \brief Get the pointer to the path object group array
			PathObject* const* getPathObjects() const { return mPathObjects; }

			//! \brief Get the number of path objects
			uint32 getNumPathObjects() const { return mNumPathObjects; }


			//! \brief Gets the memory consumption of the current NavMesh instance.
			//!
			//! Note that this only includes the loaded sectors.
			//! \return the amount of memory used
			XAIT_MAP_DLLAPI uint32 getMemoryUsage() const;

			//! \brief Gets the memory consumption for a particular sector.
			//! \param sectorID the ID of the sector
			//! \returns the memory size of the sector, or 0 if the sector is empty or not loaded.
			XAIT_MAP_DLLAPI uint32 getMemoryUsage(uint32 sectorID) const;

			//! \brief Gets the bounding box containing only the loaded sectors.
			//! \return the bounding box
			XAIT_MAP_DLLAPI Common::Geom::AABB3Df getBoundingBox() const;

			//! \brief Gets the bounding box of the NavMesh, including sectors that have not been loaded.
			//! \return A 2D bounding box with the maximum dimension of the navmesh if all sectors loaded.
			XAIT_MAP_DLLAPI Common::Geom::AABB2Df getFullBoundingBox() const;

			//! \brief Returns an unique id of the global connected component associated with the given NavMesh node ID.
			//! \param id the NavMesh node ID
			//! \return the unique ID of the corresponding connected component
			XAIT_MAP_DLLAPI uint32 getConnectedComponentID( const GlobalNodeID id ) const;

			//! \brief Test if there is no connection at all between a and b
			//! \param a		one node to test (not a start of a path)
			//! \param b		other node to test (not the end of path)
			//! \returns True if there is no connection at all between a and b, false if there is either a connection from a to b,b to a or both.
			//! \remark This method can only detect if there is no connection between a and b. If the function returns
			//! false, this does only mean that there is either only a connection from a to b or only from b to a or from a to b and b to a.
			XAIT_MAP_DLLAPI bool isNotConnected(const GlobalNodeID a, const GlobalNodeID b) const;

			//! \brief Gets the height value for a certain height class ID.
			//! \param heightClassID the height class ID
			//! \return the actual height value
			inline float32 getHeightValue(const uint32 heightClassID) const
			{
				X_ASSERT_MSG2(heightClassID < mNumHeightMapping,"Height class id is out of range",heightClassID,mNumHeightMapping);
				return mHeightMapping[heightClassID];
			}

			//! \brief Get the maximum available height class ID.
			//! \return the maximum height class ID
			inline uint32 getMaxHeightID() const
			{
				X_ASSERT_MSG(mNumHeightMapping > 0,"Container uninitialized");
				return mNumHeightMapping - 1;
			}

			//! \brief Get the up direction which has been used to build this NavMesh.
			//! \return the up direction
			inline const Common::Math::Vec3f& getUpDirection() const
			{
				return mUpDirection;
			}

			//! \brief Get the up component of the up direction (most significant value)
			//!
			//! The value 0 corresponds to the x-component, 1 corresponds to the y-component, and 2 corresponds to the z-component.
			//! \return the up component
			inline uint32 getUpComp() const
			{
				return (uint32)mUpDirection.getAbsMaxComp();
			}

			//! \brief Gets the x-axis of the plane where each NavMesh polygon is counterclockwise.
			//! \return the x-axis
			inline const Common::Math::Vec3f& getXAxis() const
			{
				return mXAxis;
			}

			//! \brief Gets the y-axis of the plane where each NavMesh polygon is counterclockwise.
			//! \return the y-axis
			inline const Common::Math::Vec3f& getYAxis() const
			{
				return mYAxis;
			}

			//! \brief Gets the cube size which has been used in the build configuration.
			//! \return the cube size
			inline float32 getCubeSize() const
			{
				return mCubeSize;
			}

			//! \brief Gets the unit radius with which the navmesh has been build
			//! \returns the unit radius
			inline float32 getUnitRadius() const
			{
				return mUnitRadius;
			}

			//! \brief Gets the step height used during navmesh builds
			//! \returns the step height
			inline float32 getStepHeight() const
			{
				return mStepHeight;
			}

			//! \brief Gets the maximum polygon edge len for all polygons
			inline float32 getMaxPolyonEdgeLen() const
			{
				return mMaxPolygonEdgeLen;
			}

			//! \brief Gets the custom data description used in the sectors
			inline const CustomDataDesc& getCustomDataDesc() const { return mCustomDataDesc; }

			//! \brief Gets the pointer to the custom data of a specific node
			inline void* getCustomDataPtr(const GlobalNodeID globalNodeID) 
			{
				NavMeshSector* sector= getSector(globalNodeID.getSectorID());
				X_ASSERT_MSG1(sector != NULL,"Invalid globalNodeID",globalNodeID.getSectorID());
				return sector->getCustomDataPtr(globalNodeID.getSectorNodeID());
			}

			//! \brief Gets the pointer to the custom data of a specific node
			const void* getCustomDataPtr(const GlobalNodeID globalNodeID) const
			{
				const NavMeshSector* sector= getSector(globalNodeID.getSectorID());
				X_ASSERT_MSG1(sector != NULL,"Invalid globalNodeID",globalNodeID.getSectorID());
				return sector->getCustomDataPtr(globalNodeID.getSectorNodeID());
			}

			//! \brief Gets a single member from custom data of a node
			template<typename T>
			inline T& getCustomDataMember(const GlobalNodeID globalNodeID, const uint32 memberID)
			{
				NavMeshSector* sector= getSector(globalNodeID.getSectorID());
				X_ASSERT_MSG1(sector != NULL,"Invalid globalNodeID",globalNodeID.getSectorID());
				return sector->getCustomDataMember<T>(globalNodeID.getSectorNodeID(),memberID);
			}

			//! \brief Gets a single member from custom data of a node
			template<typename T>
			inline const T& getCustomDataMember(const GlobalNodeID globalNodeID, const uint32 memberID) const
			{
				const NavMeshSector* sector= getSector(globalNodeID.getSectorID());
				X_ASSERT_MSG1(sector != NULL,"Invalid globalNodeID",globalNodeID.getSectorID());
				return sector->getCustomDataMember<T>(globalNodeID.getSectorNodeID(),memberID);
			}


			//! \brief Sets the up-component of a position to ground level.
			//! \param position the position
			//! \return true, if the position could be moved to the ground level, and false if the ground level could not be determined
			XAIT_MAP_DLLAPI bool setToGroundHeight(XAIT::Common::Math::Vec3f& position) const;

			//! \brief Gets all characters that can use a certain height class ID.
			//! \param heightClassID the height class ID
			//! \return a vector containing all characters with the given height class ID
			XAIT_MAP_DLLAPI Common::Container::Vector<const CharacterInfo*> getCharactersByHeightClassID(const uint32 heightClassID) const;

			//////////////////////////////////////////////////////////////////////////
			//						Memory Streaming

			//! \brief Loads a complete NavMesh from a container and switches to a certain default view.
			//!
			//! The function writes a detailed log when an error occurs.
			//! \param containerStream		the stream used to initialize the NavMesh. It must be a container stream
			//! \param initialView			the view which should be initially set
			//! \param loadAllSectors		if true, all sectors of the current view will be loaded, if false no sectors will be loaded
			//! \return true if the navigation mesh could be initialized, false otherwise
			XAIT_MAP_DLLAPI bool initFromContainer(const Common::SharedPtr<Common::IO::Stream>& containerStream, const Common::String& initialView= XAIT_MAP_DEFAULT_VIEW_NAME, const bool loadAllSectors= true);

			//! \brief Switches to another view, and keeps all sectors loaded which where loaded before. Can be forced to load all sectors of the new view.
			//!
			//! If forceLoadAllSectors is set to false, the method will just replace those sectors which differ from the old view to
			//! the new view. This will unload the old sector data and load the new sector, so you will receive the sector loaded / unloaded events.
			//! \param viewName					name of the view
			//! \param forceLoadAllSectors		if true, all sector will be loaded, if false, no additional sectors will be loaded
			//! \return true, if the view could be switched, or false, if the new view could not be found
			XAIT_MAP_DLLAPI bool switchView(const Common::String& viewName, const bool forceLoadAllSectors= false);

			//! \brief Loads a particular sector.
			//!
			//! The sector data will be taken from the containerStream initially passed to NavMeshContainer::initFromContainer.
			//! This method is thread safe, so loading can be done asynchronous. 
			//! \param sectorID the ID of the sector
			XAIT_MAP_DLLAPI void loadSector(const uint32 sectorID);

			//! \brief Loads all sectors specified
			//! 
			//! The sectors in the array will be loaded. Sectors can be specified twice or more.
			//! In additions all sectors not specified in the array can be efficiently unloaded.
			//! \param sectorIDs			array with sectors to load
			//! \param unloadUnspecified	if true, unloads all sectors that are not specified in the array
			//! \remark The sectorIDs array may be sorted by the function. To save memory it does not make a copy.
			XAIT_MAP_DLLAPI void loadSectors(Common::Container::Vector<uint32>& sectorIDs, const bool unloadUnspecified= true);

			//! \brief Loads all sectors around a position within the radius.
			//! \param center						center of circle
			//! \param radius						radius of the circle
			//! \param unloadOutOfRangeSectors		if true, sectors that are outside the circle are automatically unloaded
			inline void loadSectors(const Common::Math::Vec3f& center, const float32 radius, const bool unloadOutOfRangeSectors= true)
			{
				loadSectors(center.reduceDimensions(getUpComp()),radius,unloadOutOfRangeSectors);
			}

			//! \brief Loads all sectors around a position within the radius.
			//! \param center						center of circle
			//! \param radius						radius of the circle
			//! \param unloadOutOfRangeSectors		if true, sectors that are outside the circle are automatically unloaded
			XAIT_MAP_DLLAPI void loadSectors(const Common::Math::Vec2f& center, const float32 radius, const bool unloadOutOfRangeSectors= true);

			//! \brief Unload a particular sector.
			//! \param sectorID		the id of the sector to unload
			XAIT_MAP_DLLAPI void unloadSector(uint32 sectorID);

			//! \brief Checks whether if a sector has been loaded.
			//! \returns true, if the sector has been loaded, and false otherwise
			//! \remark This method returns also true if there is no sector for the current view in the navigation mesh.
			inline bool isSectorLoaded(const uint32 sectorID) const
			{
				X_ASSERT_MSG2(sectorID < mNumSectors,"sectorID out of range",sectorID,mNumSectors);
				return mSectors[sectorID] != NULL;
			}

			//! \brief Checks if a sector is available in the current navmesh view
			//! \returns True, if the sector is available in the current navmesh view
			XAIT_MAP_DLLAPI bool isSectorAvailable(const uint32 sectorID) const;

			//! \brief Event signalizing that a sector has been loaded.
			//!
			//! This event triggers if a sector has been fully loaded.
			Common::Event::EventSource<SectorDelegate>	eventSectorLoaded;

			//! \brief Event signalizing that a sector will be unloaded.
			//!
			//! This event triggers before a sector will be unloaded.
			Common::Event::EventSource<SectorDelegate>	eventSectorUnLoading;

			//! \brief Event signalizing that a sector has been unloaded.
			//!
			//! This event triggers if a sector has been unloaded.
			Common::Event::EventSource<SectorDelegate>	eventSectorUnLoaded;

			//! \brief Event signalizing that the navmesh content has changed
			//!
			//! This event triggers if the content of the navmesh has changed, 
			//! i.e. blocking changed or sector loaded/unloaded
			Common::Event::EventSource<NavMeshDelegate> eventNavMeshChanged;

			//! \brief Event signalizing that the navmesh is subject to change
			//!
			//! This event triggers if the navmesh is subject to change. This event
			//! gives the user the possibility to suspend asynchronous operations until
			//! the navmesh change is finished.
			Common::Event::EventSource<NavMeshDelegate> eventNavMeshChanging;

			//! \brief Triggers the navmesh changing event
			//! 
			//! This method triggers a navmesh changing event if the internal call stack
			//! count for that event is zero. Normally when you call a method like loadSectors
			//! which will load several sectors, you get a changing event at the beginning
			//! of the load of all sectors and one navmesh changed event at the end, after all
			//! specified sectors have been loaded. If you now call several times the loadSectors
			//! method with different sets of sectors ids and want the navmesh to only trigger
			//! the navmesh changed / changing event once, you have to call raiseNavMeshChanging
			//! upfront and raiseNavMeshChanged afterwards. This will increase the internal call
			//! stack count by one and will trigger the navmesh changed event only once.
			//! \remark Take care that you call raiseNavMeshChanged as often as you call raiseNavMeshChanging,
			//!			otherwise the reference count will get mixed up.
			void raiseNavMeshChanging();

			//! \brief Triggers the navmesh changed event
			//! 
			//! Triggers the navmesh changed event. For more informations see the raiseNavMeshChanging
			//! method (\see NavMeshContainer::raiseNavMeshChanging).
			void raiseNavMeshChanged();


			//////////////////////////////////////////////////////////////////////////
			//						Extended stream interface

			//! \brief Loads only the navigation mesh settings.
			//!
			//! It does not set initial view. You have to set the view explicitly with switchView.
			XAIT_MAP_DLLAPI bool initFromStream(Common::IO::Stream* navMeshStream);

			//! \brief Switches the current view to another one (or to no view).
			//!
			//! This method unloads sectors that are not part of the new view.
			//! The sectors which should be loaded will be returned in a load request.
			//! So you can take control of providing the necessary sector streams. 
			//! \param viewName				name of the new view
			//! \param loadRequest			[out] list of sector ids which should be new loaded after switchView finished
			//! \param forceLoadAllSectors	forces to loaded all sectors not only those who where loaded before the switch.
			XAIT_MAP_DLLAPI bool switchView(const Common::String& viewName, Common::Container::Vector<uint32>& loadRequest, const bool forceLoadAllSectors);

			//! \brief Loads a sector from a stream.
			//! \param sectorStream		the stream which contains the sector data
			//! \returns sectorID of the newly loaded sector, or INVALID_SECTORID if the sector could not be loaded
			//! \see setUpdateEnable
			XAIT_MAP_DLLAPI uint32 loadSector(Common::IO::Stream* sectorStream);

			//! \brief Gets the resource name of a certain sector in the currently loaded view.
			//! \param sectorID		id of the sector
			//! \returns a generated resource name by which the sector can be uniquely identified
			XAIT_MAP_DLLAPI Common::String getSectorResourceName(const uint32 sectorID) const;

			//! \brief Gets all sector IDs of the sectors which should be loaded within a specified circle.
			//! \param loadRequest	[out] id of sectors to load
			//! \param center		center of the circle
			//! \param radius		radius of the circle
			inline void getSectorsToLoad(Common::Container::Vector<uint32>& loadRequest, const Common::Math::Vec3f& center, const float32 radius)
			{
				getSectorsToLoad(loadRequest,center.reduceDimensions(getUpComp()),radius);
			}
			
			//! \brief Gets all sector IDs of the sectors which should be loaded within a specified circle.
			//! \param loadRequest	[out] id of sectors to load
			//! \param center		center of the circle in 2D, without the up component
			//! \param radius		radius of the circle
			XAIT_MAP_DLLAPI void getSectorsToLoad(Common::Container::Vector<uint32>& loadRequest, const Common::Math::Vec2f& center, const float32 radius) const;

			//! \brief Gets all sector IDs of the sectors which should be loaded within a specified bounding box
			//! \param loadRequest	[out] id of sectors to load
			//! \param bbox			bounding box to test
			inline void getSectorsToLoad(Common::Container::Vector<uint32>& loadRequest, const Common::Geom::AABB3Df& bbox) const
			{
				Common::Geom::AABB2Df bbox2d;
				const uint32 upComp= getUpComp();
				bbox2d.mMin= bbox.mMin.reduceDimensions(upComp);
				bbox2d.mMax= bbox.mMax.reduceDimensions(upComp);

				getSectorsToLoad(loadRequest,bbox2d);
			}
		
			//! \brief Gets all sector IDs of the sectors which should be loaded within a specified bounding box
			//! \param loadRequest	[out] id of sectors to load
			//! \param bbox			bounding box to test
			XAIT_MAP_DLLAPI void getSectorsToLoad(Common::Container::Vector<uint32>& loadRequest, const Common::Geom::AABB2Df& bbox) const;

			//! \brief Gets all sector neighbour ids for one sector
			//! \param neighbours	[out] List of sector neighbour ids
			//! \param sectorID		sector for which to find sector neighbours
			//! \returns See neighbours parameter
			XAIT_MAP_DLLAPI void getSectorNeighbours(Common::Container::Vector<uint32>& neighbours, const uint32 sectorID);

			//! \brief Get a sector a certain position
			//! \param position		position to get the sector id for
			//! \returns the sectorid of the sector at the specified position. Returns INVALID_SECTORID if position is out of the navmesh sector layout.
			inline uint32 getSectorID(const Common::Math::Vec3f& position) const
			{
				return getSectorID(position.reduceDimensions(getUpComp()));
			}
			
			//! \brief Get a sector a certain position
			//! \param position		position to get the sector id for
			//! \returns the sectorid of the sector at the specified position. Returns INVALID_SECTORID if position is out of the navmesh sector layout.
			XAIT_MAP_DLLAPI uint32 getSectorID(const Common::Math::Vec2f& position) const;


			//////////////////////////////////////////////////////////////////////////
			//					Storage 

			//! \brief Writes the NavMesh container to a stream.
			//! \param navMeshStream	read/write stream into which the NavMesh settings will be written
			//! \param endiness			endiness which should be used
			XAIT_MAP_DLLAPI void writeToStream(Common::IO::Stream* navMeshStream, const Common::Platform::Endian::Endiness endiness) const;

			//! \brief Writes the NavMesh container including all sectors into a container stream (including all views).
			//!
			//! Non loaded sectors will be only included if the sector container has been set (see NavMeshContainer::initFromContainer).
			//! \param containerStream	read/write stream into which the complete NavMesh including all sectors will be written
			//! \param endiness			endiness which should be used
			//! \returns true if all sectors could be saved, and false otherwise. Missing sectors referenced in a view will be treated as fail.
			XAIT_MAP_DLLAPI bool writeToContainer(Common::IO::Stream* containerStream, const Common::Platform::Endian::Endiness endiness) const;


			//////////////////////////////////////////////////////////////////////////
			//					Debug


			XAIT_MAP_DLLAPI bool validateNavMesh() const;

		private:
			friend class Build::NavMeshBuild;
			friend class Build::NavMeshOptimizer;
			friend class ConstNodeIterator;

			HighLevelGraph*					mHighLevelGraph;
			NavMeshSectorLayout*			mSectorLayout;
			NavMeshSector**					mSectors;
			uint32							mNumSectors;
			

			StaticContainer<NavMeshView>::Vector	mViews;
			uint32									mCurrViewID;

			float32*						mHeightMapping;		//!< height values for the height class ids in the NavMesh nodes
			uint32							mNumHeightMapping;

			Common::Math::Vec3f				mUpDirection;		//!< up direction of the world
			Common::Math::Vec3f				mXAxis;				//!< x axis for projection of points in the navmesh 2D room
			Common::Math::Vec3f				mYAxis;				//!< y axis for projection of points in the navmesh 2D room

			float32							mCubeSize;			//!< cube size during build
			float32							mUnitRadius;		//!< unit radius used during build
			float32							mStepHeight;
			float32							mMaxPolygonEdgeLen;

			CharacterInfo*					mCharacters;	//!< character description used to build this navigation mesh
			uint32							mNumCharacters;

			SurfaceType*					mSurfaceTypes;	//!< surface description used to build this navigation mesh
			uint32							mNumSurfaceTypes;

			PathObject**					mPathObjects;
			uint32							mNumPathObjects;

			Common::SharedPtr<Common::IO::Stream>	mContainerStream;
			Common::IO::StreamDictionary*			mNavMeshDict;

			CustomDataDesc					mCustomDataDesc;

			uint32							mChangedEventCount;
			

			void writeToStream(Common::Serialize::SerializeStorage* outStream) const;
			bool initFromStream(Common::Serialize::SerializeStorage* inStream);

			//! \brief Associates an existing sector ID to another sector instance.
			//! \param sectorID the ID of the sector you want to set
			//! \param sector the new sector
			void setSector(const uint32 sectorID, NavMeshSector* sector);

			//! \remark Note that this method does not switch automatically to the new view.
			bool addView(const Common::String& viewName);
			bool removeView(const Common::String& viewName);
			uint32 findView(const Common::String& viewName);

			Common::String getSectorResourceName(const uint32 sectorID, const Common::Crypto::MD5Sum& uniqueID) const;

			void onSectorLoaded(const uint32 sectorID);
			void onSectorUnloading(const uint32 sectorID);
			void onSectorUnLoaded(const uint32 sectorID);
			void onNavMeshChanged();
			void onNavMeshChanging();

			void createSectorArray(const uint32 numSectors);
			void deleteSectorArray();

			void createPathObjectsArray(const uint32 numPathObjects);
			void deletePathObjectArray();
			void setPathObject(const uint32 id, PathObject* pathObject);

			void extendMissingSurfaceTypes();

			GlobalNodeID searchNodeInSector(uint32 sectorID, const Common::Math::Vec3f& point,bool applyHeightConstraints) const;

			void initHightLevelGraph(const bool reinitNodes= false);
		};
	}
}

