// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/MemoryManaged.h>
#include <xait/map/CompileDefines.h>
#include <xait/map/move/Frametime.h>
#include <xait/map/MapMemoryPool.h>

namespace XAIT
{
	namespace Map
	{
		namespace Move
		{
			class CollisionShape;
			class SimulationWorld;

			//! \brief the base entity class that is managed by the simulation world
			class MoveEntity
			{
				friend class SimulationWorld;
			public:
				enum UpdateState 
				{
					US_DONE,
					US_NOT_FINISHED
				};

				//! \brief constructor
				XAIT_MAP_DLLAPI MoveEntity();

				//! \brief destructor
				XAIT_MAP_DLLAPI virtual ~MoveEntity();

				//! \brief get the MoveEntityID (managed by the simulation world)
				//! \returns the MoveEntityID
				inline uint32 getMoveEntityID() const
				{
					return mMoveEntityID;
				}

				//! \brief get the ColliderID (managed by the collision interface)
				//! \returns the ColliderID
				inline uint32 getColliderID() const
				{
					return mColliderID;
				}

				//! \brief get the simulation world
				//! \returns the owning simulation world 
				inline SimulationWorld* getSimulationWorld() const
				{
					return mSimWorld;
				}


				//! \brief get the current position of move entity
				//! \returns the current move position
				virtual const Common::Math::Vec3f& getMovePosition() const = 0;

				//! \brief get the direction of the move entity 
				//! \returns the direction of the move entity
				virtual const Common::Math::Vec3f& getMoveDirection() const = 0;

				//! \brief get the current speed of the move entity
				//! \returns the current speed of the move entity
				inline float32 getCurrentSpeed() const
				{
					return mCurrentSpeed;
				}

				//! \brief get the move velocity
				//! \returns the move velocity
				inline const Common::Math::Vec3f getVelocity() const
				{
					return getMoveDirection() * getCurrentSpeed();
				}

				

			
			protected:
				
				float32							mCurrentSpeed;

				//! \brief returns a collision shape that is used by the collision interface to generate collision data
				virtual CollisionShape* getCollisionShape() const = 0;

				//! \brief the main update function of the entity this function is automatically called by the simulation world
				//! \param time			the frametime
				//! \param newFrame		true if the function is called the first time in a frame
				virtual UpdateState update(const Frametime& time, bool newFrame) = 0;
				

				
				//! \brief called each frame before the update function is called
				virtual void initFrame() = 0;
				//! \brief finish the frame and calls the appropriate functions to write the values back to the game
				virtual void finishFrame() = 0;






				// callback functions that has to be implemented by the entity class

				/*! \brief the main callback function. the given vector should be used to set the new position of the entity.
				 *			if you are using a character controller (e.g. physics character controller), you can use this vector
				 *			to generate the appropriate input. 
				 *			if you use a physic engine the entity position will differ in up direction from the move position.
				 *			in that case ignore the up component. in some cases (e.g. physx character controller) you have to
				 *			set the up component in gravity direction to simulate the gravity (see tutorial)
				 */
				virtual void onMove(const Common::Math::Vec3f& moveVector) = 0;


				//! \brief get the position of the entity from the game
				//! \param position		the position of the entity in the game
				virtual void onQueryPosition(Common::Math::Vec3f& position) const = 0;



				//! \brief set the position of the entity in the game/physic engine/animation system
				//! \param position			the new position of the entity
				//! \remark this function is only called if error handling occurred, otherwise the move function is called
				virtual void onSetPosition(const Common::Math::Vec3f& position) = 0;

				//! \brief set the direction of the entity in the game/physic engine/animation system
				//! \param direction			the new direction of the entity
				//! \remark this function is only called if error handling occurred, otherwise the move function is called
				virtual void onSetDirection(const Common::Math::Vec3f& direction) = 0;

				

				//! \brief get a the memory allocator
				static const Common::Memory::Allocator::Ptr& getAllocator()
				{
					return MapMemoryPool::getLowAllocThreadSafeAllocator();
				}
			private:
				SimulationWorld*				mSimWorld;
				uint32							mMoveEntityID;
				uint32							mColliderID;

				inline void addToSimulationWorld(SimulationWorld* simWorld, uint32 entityID, uint32 colliderID)
				{
					X_ASSERT_MSG(mSimWorld == NULL, "move entity is already in a simulation world");
					mSimWorld = simWorld;
					mMoveEntityID = entityID;
					mColliderID = colliderID;
				}

				inline void removeFromSimulationWorld()
				{
					X_ASSERT_MSG(mSimWorld != NULL, "move entity is not in a simulation world");

					mSimWorld = NULL;
					mMoveEntityID = X_MAX_UINT32;
					mColliderID = X_MAX_UINT32;
				}

				inline void setColliderID(uint32 colliderID)
				{
					mColliderID = colliderID;
				}
				inline void setMoveEntityID(uint32 entityID)
				{
					mMoveEntityID = entityID;
				}
			};
		}
	}
}
