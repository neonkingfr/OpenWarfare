// (C) xaitment GmbH 2006-2012

#pragma once


#include <xait/map/CompileDefines.h>
#include <xait/map/Interface.h>
#include <xait/map/move/MoveEntity.h>
#include <xait/map/move/QuadtreeCollisionInterface.h>
#include <xait/map/MapMemManagedObject.h>
#include <xait/map/NavMeshContainer.h>


namespace XAIT
{
	namespace Map
	{
		namespace Move
		{
			class CollisionInterface;

			/*! \brief The simulation world represents a collection of move entities that interact with each other
			 *	and with objects that are registered at the collision interface.
			 *	All move entities that are registered are automatically updated at the collision interface.
			 *	Additional obstacles that are not managed by this class, have to be registered and updated at 
			 *	the collision interface manually.
			 */
			class SimulationWorld : public MapMemManagedObject
			{
			public:
				//! \brief creates a simulation world the default quadtree collision interface
				//! \param navmesh	a navigation mesh that is used by the entities of the simulationworld
				static SimulationWorld* createWithQuadtreeCollisionInterface(const NavMeshContainer* navmesh)
				{
					return new SimulationWorld(new QuadtreeCollisionInterface(2.0f * navmesh->getUnitRadius(),navmesh->getUpComp()));
				}

				//! \brief constructor
				//! \param collisionInterface		the used collision interface
				XAIT_MAP_DLLAPI SimulationWorld(CollisionInterface* collisionInterface);

				//! \brief destructor
				//! \remark all not deregistered entities will be deleted
				XAIT_MAP_DLLAPI virtual ~SimulationWorld();

				//! \brief register a move entity
				//! \param moveEntity		the move entity
				//! \returns the move entity id or X_MAX_UINT32 if register failed
				//! \remark	The entity will be registered at the collision interface automatically. 
				//! \remark The register can fail if the registering of the move entity at the collision interfaces fails. This
				//!         can happen if the position or collision shape of the entity is wrong (i.e. position undefined). Make
				//!         sure that you use setPosition on a PathFollowMoveEntity once before you register the entity.
				XAIT_MAP_DLLAPI uint32 registerMoveEntity(MoveEntity* moveEntity);

				//! \brief deregister a move entity
				//! \param entityID		the id of the move entity
				//! \returns the deregistered move entity
				//! \remark	the entity will be deregistered at the collision interface automatically. 
				XAIT_MAP_DLLAPI MoveEntity* deregisterMoveEntity(uint32 entityID);

				//! \brief starts an update cycle of the complete simulation world (collision interface, move entities)
				//! \param	timeBudget		the time budget in milliseconds
				//! \param	gameTime		the current game time in milliseconds
				XAIT_MAP_DLLAPI void startUpdate(uint32 timeBudget, uint32 gameTime);

				//! \brief checks if the frame is finished
				//! \returns true if the frame computations are finished, false otherwise
				bool isFrameFinished()
				{
					return true;
				}

				//! \brief fetches the results of all move updates
				XAIT_MAP_DLLAPI void fetchResults();

				//! \brief get the used collision interface
				//! \returns the used collision interface
				CollisionInterface* getCollisionInterface()
				{
					return mCollisionInterface;
				}

				//! \brief get an entity by the movement id
				//! \param entityID		the movement id of the entity
				//! \returns the entity
				inline MoveEntity* getEntity(uint32 entityID)
				{
					X_ASSERT_MSG(mEntities.isValidID(entityID),"access with invalid entityID");
					return mEntities[entityID];
				}

				//! \brief get an entity by the collider id
				//! \param colliderID		the collider id of the entity
				//! \returns the entity if the collider id is known in the simulation world, NULL otherwise
				inline MoveEntity* getEntityByColliderID(uint32 colliderID)
				{
					if (colliderID >= mColliderIDMapping.size() || mColliderIDMapping[colliderID] == X_MAX_UINT32)
						return NULL;

					return mEntities[mColliderIDMapping[colliderID]];
				}

				//! \brief get all movement entities
				//! \returns an idvector with all MoveEntities
				inline const Common::Container::IDVector<MoveEntity*>& getEntities() const
				{
					return mEntities;
				}


			protected:
				CollisionInterface*							mCollisionInterface;
				Common::Container::IDVector<MoveEntity*>	mEntities;
				Common::Container::IDVector<bool>			mUpdateState;
				Common::Container::Vector<uint32>			mColliderIDMapping;
				uint32										mLastUpdatedEntity;
				uint32										mStartTime;
				uint32										mLastFrametime;


				bool										mIsRunning;


				void updateLoop(uint32 timeBudget, Frametime frametime);

			};
		}
	}
}
