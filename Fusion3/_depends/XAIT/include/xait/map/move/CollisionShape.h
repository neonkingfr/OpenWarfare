// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/map/MapMemManagedObject.h>

namespace XAIT
{
	namespace Map
	{
		namespace Move
		{
			//! \brief simple class that describes the shape of an obstacle
			class CollisionShape : public MapMemManagedObject
			{
			public:
				//! \brief get the shape id
				//! \returns the shape id
				virtual uint32 getShapeID() const = 0;			
			};
		}
	}
}
