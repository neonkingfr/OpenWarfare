// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>

namespace XAIT
{
	namespace Control
	{
		//! \brief This class encapsulates variable IDs. 
		class FSMVarID
		{
		public:
			//! \brief Constructor. Generates an invalid variable ID.
			FSMVarID();

			//! \brief Equality operator.
			//! \param other	the FSMVarID to compare against 
			//! \returns true if both ids are equal, and false otherwise
			bool operator==(const FSMVarID& other) const;

			//! \brief Constructor.
			//! \param fsmID	id of the FSM owning the variable
			//! \param varID	id of the variable in the FSM
			FSMVarID(FSMID fsmID,uint16 varID);

			//! \brief Gets the FSM id stored in this FSMVarID.
			//! \returns the fsm id
			FSMID getFSMID() const;

			//! \brief Gets the variable ID stored in this FSMVarID
			//! \returns the variable id
			uint32 getVarID() const;


			//! \brief Checks whether the FSMVarID is valid.
			//! \returns true if it is valid, false if it is invalid
			bool isValid() const;

		private:
			uint32	mID;
		};
	}
}

