// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/Singleton.h>
#include <xait/common/container/Vector.h>
#include <xait/common/container/IDVector.h>
#include <xait/common/IDWrapper.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/String.h>
#include <xait/common/reflection/function/FunctionRegistrar.h>
#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/function/Signature.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/io/Stream.h>
#include <xait/common/platform/Time.h>
#include <xait/common/platform/Endian.h>
#include <xait/common/serialize/SerializeStorage.h>
#include <xait/common/event/Event.h>

#include <xait/control/FSMTypedefs.h>
#include <xait/control/FSMVarID.h>
#include <xait/control/Iterators.h>
#include <xait/control/DebuggerAddress.h>
#include <xait/control/FSMSet.h>
#include <xait/control/FSM.h>


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				class XMLNodeElements;
			}
		}

#ifdef XAIT_CONTROL_DEBUGGER
		namespace Debugger
		{
			class Module;
			class ObjectGroup;
		}
#endif
	}
	
	namespace Control
	{
		class FSMInstance;
		class FSM;
		class FSMMemory;
		class FSMExternalDataInterface;
		class Interface;
		class FSMInstanceWrapper;
		class ExternalVariableCallback;

		typedef Common::Event::Delegate<void (FSMInstance*, FSMFunction, void*, const Common::Reflection::Function::FunctionArgumentList&)>  UnboundFSMFunctionCallHandler;

		typedef Common::Event::Delegate<void (FSMInstance*, FSMInstance*)> FSMInstanceCreatedHandler;
		typedef Common::Event::Delegate<void (FSMInstance*, FSMInstance*)> FSMInstanceDestroyingHandler;
		
		//! \brief This class manages all FSM sets, all FSMs and their instances.
		//!
		//! With the FSMManager you can load FSM sets, start the execution of FSMs, and control the execution of FSM instances. You can also register default callback functions.
		//! Note that the FSMManager is a singleton class. You have to use the interface (interface.h) to get access to it.
		class FSMManager : Common::Singleton<FSMManager>
		{
			friend class Common::Singleton<FSMManager>;
			friend class FSMSet;
			friend class FSM;
			friend class FSMInstance;
			friend class Interface;
			friend class FSMMemory;
			friend class FSMVariableResolver;
			friend class ControlDatatypesInitHelper;
			friend class FSMInstanceWrapper;
			friend class FSMEvent;
			friend class FSMIterator;
			friend class FSMInstanceIterator;
			friend class FSMInstanceSerializer;
			friend class FSMVariable;
			friend class FSMInstanceVariable;

		public:

			//! An invalid FSMID. Can be used to check the validity of returned FSMIDs.
			XAIT_CONTROL_DLLAPI static const FSMID			InvalidFSMID;

			//! An invalid FSMInstanceID. Can be used to check the validity of returned FSMInstanceIDs.
			XAIT_CONTROL_DLLAPI static const FSMInstanceID	InvalidFSMInstanceID;

			//! An invalid FSMEventID. Can be used to check the validity of returned FSMEventIDs.
			XAIT_CONTROL_DLLAPI static const FSMEventID		InvalidFSMEventID;

			//! An invalid FSMVarID. Can be used to check the validity of returned FSMVarIDs.
			XAIT_CONTROL_DLLAPI static const FSMVarID		InvalidFSMVarID;

			//! An invalid FSMSetID. Can be used to check the validity of returned FSMSetIDs.
			XAIT_CONTROL_DLLAPI static const FSMSetID		InvalidFSMSetID;
			
			//! \brief Event signalizing that a not bound function is called
			//! \remark if no fallback handler is registered, the fsm asserts in the startFSM method if not all functions are bound
			Common::Event::EventSource<UnboundFSMFunctionCallHandler>	onUnboundFunctionCall;

			//! \brief Event signalizing that a FSMInstance has been created
			//! \remark the first parameter specifies the created instance, the second one the instance that created it (if present, else null)
			Common::Event::EventSource<FSMInstanceCreatedHandler> onFSMInstanceCreated;

			//! \brief Event signalizing that a FSMInstance will be destroyed
			//! \remark the first parameter specifies the destroyed instance, the second one the instance that destroyed it (if present, else null)
			Common::Event::EventSource<FSMInstanceDestroyingHandler> onFSMInstanceDestroying;

			//! \brief Sends an update to every FSM instance in the FSMManager.
			//! \param gametime			the current game time
			//! \remark This function needs to be called with the actual game time when you use time base transitions.
			XAIT_CONTROL_DLLAPI void update(Common::Platform::MilliSeconds gametime);

			//! \brief Sets the current game time.
			//! \param gametime			the current game time
			XAIT_CONTROL_DLLAPI void setGametime(Common::Platform::MilliSeconds gametime);

			//! \brief Gets the current game time
			//! \returns the current game time
			XAIT_CONTROL_DLLAPI Common::Platform::MilliSeconds getGametime() const;

			//! \brief Loads a FSM set from the file given by the filename.
			//! \param filename		filename of the the FSM set that is to be loaded
			//! \returns the FSMSet when loading it was successful, or NULL otherwise
			//! \exception ArgumentException			fAccess or fMode has wrong values
			//! \exception UnAuthorizedAccessException	Given path is a directory, or file is read-only, but an open-for-writing operation was attempted
			//! \exception IOException					No more file descriptors available
			//! \exception FileNotFoundException		File or path not found
			//! \exception FormulaParserException		An Error occur while parsing a formula
			//! \exception SyntaxException				An syntax error occur
			//! \remark if a loaded set already exists NULL be returned, this can be checked in before by using getFSMSet(string name)			
			XAIT_CONTROL_DLLAPI FSMSet* loadFSMSet(const Common::String& filename);

			//! \brief Load a FSM set from a stream.
			//! \param inStream		the stream containing the FSM set
			//! \returns the FSMSet when loading it was successful, or NULL otherwise
			//! \remark if a loaded set already exists NULL will be returned, this can be checked in before by using getFSMSet(string name)
			XAIT_CONTROL_DLLAPI FSMSet* loadFSMSet(Common::IO::Stream* inStream);

			//! \brief Reads an FSM metafile and adds the contained list of FSMs to the FSMSet that is indicated by the metafile
			//! \param sourcePath	path where metafile (.FSMSet.xml) and FSMs (.FSM.xml) are stored
			//! \param filename		filename of the metafile
			//! \returns true if loading succeeded, false otherwise
			XAIT_CONTROL_DLLAPI bool loadFSMSetMetafile(const Common::String& sourcePath, const Common::String& filename);

			//! \brief Load a common config file for an FSM
			//! \param filename filename of the the CommonConfig.xml file that is to be loaded
			//! \returns the FSMSet when loading it was successful, or NULL otherwise
			//! \remark This will initialize a stripped down version of an FSMSet, containing common elements like functions and events but no FSMs.
			//! \remark FSMs can be added to a set using the FSMSet.loadFSM function.
			XAIT_CONTROL_DLLAPI FSMSet* loadFSMSetConfig(const Common::String& filename);
			
			//! \brief gets a FSM set by its name.
			//! \param name	the the name of the FSM set
			//! \returns the FSMSet if one with this name was found, or NULL otherwise
			XAIT_CONTROL_DLLAPI FSMSet* getFSMSet(const Common::String& name) const;

			//! \brief Gets an iterator to the first FSM set.
			//! \returns the iterator
			XAIT_CONTROL_DLLAPI FSMSetIterator getFSMSets() const;

			//! \brief Unload a fsm set
			//! \param fsmset	the FSMSet to unload
			XAIT_CONTROL_DLLAPI void unloadFSMSet(FSMSet* fsmset); 

			//! \brief Gets the corresponding FSMInstance to the FSMInstanceID given by parameter.
			//! \param id		the FSMInstanceID of the FSM instance you want to retrieve
			//! \returns a pointer to the FSMInstance if it exists, and NULL otherwise
			XAIT_CONTROL_DLLAPI FSMInstance* getFSMInstance(FSMInstanceID id);

			//! \brief Gets the corresponding FSMInstance to the FSMInstanceID given by parameter.
			//! \param id		the id of the FSM instance you want to retrieve
			//! \returns a pointer to the FSMInstance if it exists, and NULL otherwise
			XAIT_CONTROL_DLLAPI FSMInstance* getFSMInstance(int32 id);


			//! \brief Gets an iterator to the first FSM Instance
			//! \returns the iterator
			//! \remark this iterator points to all fsminstances, independent of their FSM type
			XAIT_CONTROL_DLLAPI GlobalFSMInstanceIterator getAllFSMInstances();

			//! \brief Loads a saved state from a file.
			//! \param filename		name of the save state file
			//! \param bufferSize	size of the input buffer when reading the file
			//! \exception FileNotFoundException		File or path not found
			//! \see saveState
			XAIT_CONTROL_DLLAPI void restoreState(const XAIT::Common::String& filename, const int32 bufferSize = 4096);

			//! \brief Loads a saved state from a stream.
			//! \param stream name of the stream from which the state is read
			//! \param bufferSize	size of the input buffer when reading the stream
			//! \exception FileNotFoundException		File or path not found
			//! \see saveState
			XAIT_CONTROL_DLLAPI void restoreState(Common::IO::Stream* stream, const int32 bufferSize = 4096);
			
			//! \brief Saves the state of the library to a file.
			//! 
			//! Note that only FSM instances are stored, not the description of the FSM sets or of the FSMs.
			//! \param filename		name of the file
			//! \see restoreState
			XAIT_CONTROL_DLLAPI void saveState(const XAIT::Common::String& filename);

			//! \brief Saves the state of the library to a file with the given endiness and buffer size.
			//! 
			//! Note that only FSM instances are stored, not the description of the FSM sets or of the FSMs.
			//! \param filename		name of the file
			//! \param endiness		which endiness should be used to write the fsminstance
			//! \param bufferSize	size of local write buffer (-1 == buffer disabled, useful if you have already a buffered stream)
			//! \see restoreState
			XAIT_CONTROL_DLLAPI void saveState(const XAIT::Common::String& filename, const Common::Platform::Endian::Endiness endiness, const int32 bufferSize= 4096);

			//! \brief Saves the state of the library to a stream with the given endiness and buffer size.
			//! 
			//! Note that only FSM instances are stored, not the description of the FSM sets or of the FSMs.
			//! \param stream	stream that contains the state
			//! \param endiness		which endiness should be used to write the fsminstance
			//! \param bufferSize	size of local write buffer (-1 == buffer disabled, useful if you have already a buffered stream)
			//! \see restoreState
			XAIT_CONTROL_DLLAPI void saveState( Common::IO::Stream* stream, const Common::Platform::Endian::Endiness endiness, const int32 bufferSize = 4096);
			
			//! \brief Tells the xaitControl library to hold at the next reachable breakpoint.
			//! \remark this effect occurs only if the debugger library is used and if a xaitControl Creator is connected
			XAIT_CONTROL_DLLAPI void holdOnNextBreakpoint();

			//! \brief sets the maximal length of the debugger history
			//! \param maximalHistoryLength the new maximal length of the history
			XAIT_CONTROL_DLLAPI void setMaximalHistoryLength(uint32 maxLength);

			//! \brief Defines whether the library tracks the variables during run time for debugger
			//! \param trackVariables	flag to define whether the variables should be tracked
			XAIT_CONTROL_DLLAPI void setVariableTracking(bool trackVariables);

			//! \brief sets whether events can be triggered with a faulty (w.r.t the parameter types) or incomplete signature  
			//! \param allowIncompleteEvents a bool flag that states whether events can be triggered with an incorrect signature
			//! \remark triggering an event with invalid signature results in filling the parameters with default values
			//! \remark this functionality is for debugging purposes only, events are needed to be triggered correctly in shipping mode 
			XAIT_CONTROL_DLLAPI void setAllowIncompleteEvents(bool allowIncompleteEvents);

			//! \brief gets whether events can be triggered with a faulty (w.r.t the parameter types) or incomplete signature  
			//! \return a bool flag that states whether events can be triggered with an incorrect signature
			XAIT_CONTROL_DLLAPI bool getAllowIncompleteEvents();

			//! \brief sets whether global transitions should be evaluated bottom up w.r.t. subfsms 
			//! \param bottomUpEvaluation a bool flag that states whether global transitions should be evaluated bottom up w.r.t. subfsms
			XAIT_CONTROL_DLLAPI void useGlobalTransitionsBottomUp(bool bottomUpEvaluation);				

			//! \brief The Destructor.
			XAIT_CONTROL_DLLAPI virtual ~FSMManager();

		private:
			
			Common::Container::IDVector<FSMSet*>					mFSMSets;			
			Common::Container::IDVector<FSM*>						mFSMs;			
			Common::Container::IDVector<FSMInstance*>				mFSMInstances;

			Common::Container::LookUp<Common::String,FSMSetID>		mFSMSetNameIDMapping;
			
			Common::Threading::SpinLock								mMutex;

			bool													mAllowIncompleteEvents;

			bool													mGlobalTransitionEvalutationBottomUp;

			FSMSet*													mCurrentlyLoadedSet;

			//! \brief constructor
			FSMManager(const DebuggerAddress& localAddress= DebuggerAddress());	

			static void createInstance(const Common::Memory::Allocator::Ptr& alloc, const DebuggerAddress& address);

			FSMSet* getFSMSet(FSMSetID setID) const;
			FSM*	getFSM(FSMID id) const;
			
			FSMInstance* instantiateFSMInternal(FSMID id);
			FSMInstance* instantiateFSMInternal(FSMID id, const Common::String& name, bool addIDToName = false);
			void removeFSMInstanceInternal(FSMInstanceID id);
						
			void handleFSMInstanceCreated(FSMInstance* instance, FSMInstance* initiator);
			void handleFSMInstanceRemoved(FSMInstance* instance, FSMInstance* initiator);
			
			FSMID addFSM(FSM* fsm);
			FSMID createFSMID(const uint32 internalID);
			FSMSet*	doLoadFSMSet(Common::IO::Stream* inStream, const Common::String& path = "");
			
			void saveState( Common::Serialize::SerializeStorage* stream);
			void restoreState( Common::Serialize::SerializeStorage* stream);
			//! \brief Creates and initializes an instance from a stream.
			//! \param stream	the stream from which you want to initialize the FSM instance
			//! \returns		the FSMInstanceID of the new FSM instance when it could be created, and an invalid FSMInstanceID otherwise
			//! \see InvalidFSMInstanceID
			//! \see removeFSMInstance
			FSMInstanceID instantiateFSMFromStream(Common::Serialize::SerializeStorage* stream);

			FSMInstanceWrapper createFSMInstance(Common::String setName, Common::String fsmName, int32 initiatorInstanceID);
			FSMInstanceWrapper createFSMInstanceWithName(Common::String setName, Common::String fsmName, Common::String instanceName, int32 initiatorInstanceID);
			void destroyFSMInstance(FSMInstanceWrapper& instance, int32 initiatorInstanceID);

			
			////////////////////////////////
			////	Debugger Part		////
			////////////////////////////////

#ifdef XAIT_CONTROL_DEBUGGER
		public:

		private:

			Common::Debugger::Module*			mDebuggerModule;

			void startDebuggerServer(const DebuggerAddress& localAddress);
			void shutDownDebuggerServer();
			void setupDebuggerModule();
			void attachDebuggerModule();
			void createBreakpointTypes();
			void registerRPCs();
			void unregisterRPCs();

			void ConvertStreamEventParameters(const Common::Reflection::Function::FunctionArgumentList& streamArguments, Common::Reflection::Function::FunctionArgumentList& eventParameters);

			//RPCs
			Common::Container::Vector<Common::WString> getEventQueue(Common::WString setName, Common::WString fsmName, Common::WString instanceName);
			bool clearEventQueue(Common::WString setName, Common::WString fsmName, Common::WString instanceName);
			void addEventToQueue(void* returnvalue, Common::Reflection::Datatype::DatatypeID returnTypeID, const Common::Reflection::Function::FunctionArgumentList& arguments);
			bool restartFSMInstance(Common::WString setName, Common::WString fsmName, Common::WString instanceName);
			bool supressTransition(Common::WString setName, Common::WString fsmName, Common::WString instanceName, Common::WString sourceState, Common::WString transitionName, uint32 transitionIndex);
			bool unsupressTransition(Common::WString setName, Common::WString fsmName, Common::WString instanceName, Common::WString sourceState, Common::WString transitionName, uint32 transitionIndex);
			bool clearSupressedTransitions();
			Common::Container::Vector<Common::WString> getInstanceNames();
			Common::Container::Vector<uint32> getInstanceIDs();
#endif	

		};
	}
}

