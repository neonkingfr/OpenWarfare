// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/control/FSMInstance.h>
#include <xait/common/String.h>
#include <xait/common/io/Stream.h>
#include <xait/common/platform/Endian.h>
#include <xait/common/memory/ReadOnlyMemoryStream.h>
#include <xait/common/Interface.h>

namespace XAIT
{
	namespace Control
	{
		//! \brief Serializes an FSM instance.
		//!
		//! This class uses the memory allocator of the FSM instance for additional memory allocation.
		class FSMInstanceSerializer
		{
		public:
			//! \brief Reads a previously stored FSM instance from a file.
			//! \param fileName		the file from which the instance is read
			//! \param bufferSize	the size of the input buffer when the file is read
			//! \returns an instance of the newly created FSM instance, or NULL if something went wrong
			//! \remark throws exceptions if something went wrong
			//! \remark This method needs a valid FileIOCallback.
			XAIT_CONTROL_DLLAPI static FSMInstance* readFSMInstance(const Common::String& fileName, const int32 bufferSize= 4096);

			//! \brief Reads a previously stored FSM instance from a stream.
			//! \param inStream		stream from which to read (needs to be readable)
			//! \param bufferSize	size of local read ahead buffer (-1 == buffer disabled, useful if you have already a buffered stream)
			//! \returns an instance of the newly created FSM instance, or NULL if something went wrong
			//! \remark throws exceptions if something went wrong
			//! \remark No FileIOCallback needed
			XAIT_CONTROL_DLLAPI static FSMInstance* readFSMInstance(Common::IO::Stream* inStream, const int32 bufferSize= 4096);

			//! \brief Reads a previously stored FSM instance from a memory buffer.
			//! \param buffer		buffer from which to read the FSM instance
			//! \param bufferSize	size of the buffer
			//! \returns an instance of the newly created FSM instance, or NULL if something went wrong
			//! \remark throws exceptions if something went wrong
			//! \remark No FileIOCallback needed
			static FSMInstance* readFSMInstance(const void* buffer, const uint32 bufferSize)
			{
				Common::Memory::ReadOnlyMemoryStream* memStream= new(Common::Interface::getGlobalAllocator()) Common::Memory::ReadOnlyMemoryStream(buffer,bufferSize,"FSMInstance");
				FSMInstance* res= readFSMInstance(memStream,-1);
				delete memStream;

				return res;
			}

			//! \brief Writes a FSM instance to a file.
			//! \param fileName		the name of the file to which the instance should be written.
			//! \param fsmInstance		pointer to the FSM instance which should be written
			//! \param bufferSize	size of local write buffer (-1 == buffer disabled, useful if you have already a buffered stream)
			//! \remark throws exceptions if something went wrong
			//! \remark This needs a working FileIOCallback
			XAIT_CONTROL_DLLAPI static void writeFSMInstance(const Common::String& fileName, FSMInstance* fsmInstance, const int32 bufferSize= 4096);

			//! \brief Writes a FSM instance to a file.
			//! \param fileName		the name of the file to which the instance should be written.
			//! \param fsmInstance		pointer to the FSM instance which should be written
			//! \param endiness		which endiness should be used to write the FSM instance
			//! \param bufferSize	size of local write buffer (-1 == buffer disabled, useful if you have already a buffered stream)
			//! \remark throws exceptions if something went wrong
			//! \remark This needs a working FileIOCallback
			XAIT_CONTROL_DLLAPI static void writeFSMInstance(const Common::String& fileName, FSMInstance* fsmInstance, const Common::Platform::Endian::Endiness endiness, const int32 bufferSize= 4096);

			//! \brief Write a FSM instance to a stream.
			//! \param outStream	stream which should receive the FSM instance (must be writable)
			//! \param fsmInstance		reference to the FSM instance which should be written
			//! \param endiness		which endiness should be used to write the FSM instance
			//! \param bufferSize	size of local write buffer (-1 == buffer disabled, useful if you have already a buffered stream)
			//! \remark throws exceptions if something went wrong
			//! \remark No FileIOCallback needed
			XAIT_CONTROL_DLLAPI static void writeFSMInstance(Common::IO::Stream* outStream, FSMInstance* fsmInstance, const Common::Platform::Endian::Endiness endiness, const int32 bufferSize= 4096);
		};
	}
}
