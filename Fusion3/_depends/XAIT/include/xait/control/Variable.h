// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/reflection/datatype/DatatypeBase.h>

namespace XAIT
{
	namespace Control
	{
		//! \brief Base class of FSM memory variables.
		class Variable : public ControlMemManagedObject
		{
			friend class FSMMemory;

		protected:			
			Common::Reflection::Datatype::DatatypeID	mDatatypeID;



		public:
			enum VARIABLE_TYPE
			{
				VT_INTERNAL,
				VT_EXTERNAL,
				VT_REFERENCE,
			};

			//! \brief Returns the concrete type of a variable
			virtual VARIABLE_TYPE getVariableType() const = 0;
			//! \brief Default constructor.
			Variable();

			//! \brief Constructor.
			//! \param datatypeID the ID of the data type of the variable
			Variable(Common::Reflection::Datatype::DatatypeID datatypeID);

			//! \brief Destructor.
			virtual ~Variable();

			//!	\brief Get the data type ID of the variable.
			//! \returns the type
			virtual Common::Reflection::Datatype::DatatypeID getDatatypeID() const;

			//! \brief Sets the value of the variable.
			//! \param value	the new value
			//! \see getValue
			template<typename T_ENTRY_TYPE>
			void setValue(const T_ENTRY_TYPE& value)
			{				
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to set variable value with wrong datatype");
				setValueByPtr((void*) &value);
			}

			//! \brief Gets the variable value
			//! \returns	the value of the variable
			//! \see setValue
			template<typename T_ENTRY_TYPE>
			const T_ENTRY_TYPE& getValue()
			{
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to get variable value with wrong datatype");
				void* value = getValuePtr();
				X_ASSERT_MSG_DBG(value != NULL, "variable is not initalized or already destroyed");
				return *( (T_ENTRY_TYPE*)value );
			}

			//! \brief Gets a pointer to the value of the variable.
			//! \returns a pointer to the value of the variable
			//! \see setValueByPtr
			virtual void* getValuePtr() const = 0;

			//! \brief Sets the value of the variable from a pointer.
			//! \param value	a pointer to the new value
			//! \see getValuePtr
			virtual void setValueByPtr(void* value) = 0;

			//! \brief Writes the variable to a stream.
			//! \param stream		the stream to which the variable should be written
			virtual void writeToStream(Common::Serialize::SerializeStorage* stream) = 0;

			//! \brief Reads the variable from a stream.
			//! \param	stream		the stream from which the variable will be read
			virtual void readFromStream(Common::Serialize::SerializeStorage* stream) = 0;
		};
	}
}
