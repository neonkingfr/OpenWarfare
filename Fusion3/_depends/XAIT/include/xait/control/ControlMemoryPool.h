// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/Allocator.h>
#include <xait/common/memory/StaticMemoryManaged.h>
#include <xait/control/FSMTypedefs.h>

namespace XAIT
{
	namespace Control
	{
		class ControlMemoryPool
		{
		public:
			//! \brief Get the free list memory allocator
			XAIT_CONTROL_DLLAPI static const Common::Memory::Allocator::Ptr& getLowAllocThreadSafeAllocator();
		};
	}
}
