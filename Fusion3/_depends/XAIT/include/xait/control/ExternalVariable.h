// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/control/Variable.h>
#include <xait/control/ExternalVariableCallback.h>

namespace XAIT
{
	namespace Control
	{
		//! \brief Provides a callback object rather than a value pointer. 
		//! \brief The callback interface defines getter/setter methods to manipulate a value stored in client code and 
		//! \brief additionally serves as a notification mechanism to inform when values need to be accessed.
		//! \brief Memory is allocated by client code.
		class ExternalVariable : public Variable
		{
		public:

			//! \brief default constructor
			ExternalVariable();

			//! \brief constructor
			ExternalVariable(Common::Reflection::Datatype::DatatypeID datatypeID, ExternalVariableCallback* callback);

			//! \brief copy constructor			
			ExternalVariable(const ExternalVariable& variable);
			
			//! \brief destructor
			~ExternalVariable();
			
			//! \brief Assignment operator.
			ExternalVariable& operator=(const ExternalVariable& variable);

			//! \brief retrieve the value wrapped with callback object
			void* getValuePtr() const;

			//! \brief set value via callback object
			void setValueByPtr(void* value);

			void writeToStream(Common::Serialize::SerializeStorage* stream);

			void readFromStream(Common::Serialize::SerializeStorage* stream);

		protected:

			VARIABLE_TYPE getVariableType() const
			{
				return VT_EXTERNAL;
			}

		private:
			ExternalVariableCallback*	mCallback;

			//! \brief Deinitialises the variable.
			void deinit(const Common::Memory::AllocatorPtr& allocator);
		};
	}
}