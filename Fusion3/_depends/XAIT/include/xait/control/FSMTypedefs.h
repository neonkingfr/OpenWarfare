// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/IDWrapper.h>

#define X_DEFAULT_NAMESPACE "Loc"


#ifdef XAIT_LIBRARY_DYNAMIC

#ifdef XAIT_CONTROL_DLL_EXPORTS
	#define XAIT_CONTROL_DLLAPI	XAIT_DLLEXPORT
#else
	#define XAIT_CONTROL_DLLAPI	XAIT_DLLIMPORT
#endif

#else

#define XAIT_CONTROL_DLLAPI

#endif


namespace XAIT
{
	namespace Control
	{
		class FSMManager;
		class FSMSet;
		class FSM;
	}

	// some compilers need some help to understand templates...
	namespace Common
	{
		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint32,Control::FSMSet,0);
		

		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint32,Control::FSMManager,0);
		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint32,Control::FSMManager,1);
		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint32,Control::FSMManager,2);

		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint16,Control::FSM,0);
		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint16,Control::FSM,1);
		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint16,Control::FSM,2);
		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint16,Control::FSM,3);
		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint16,Control::FSM,4);

	}
	
	namespace Control
	{
			
		//! \brief Data type for Event IDs.
		typedef		Common::IDWrapper<uint32,FSMSet,0>			FSMEventID;


		//! \brief Data type for FSM Instance IDs.
		typedef		Common::IDWrapper<uint32,FSMManager,0>		FSMInstanceID;		

		//! \brief Data type for FSM set IDs.
		typedef		Common::IDWrapper<uint32,FSMManager,1>		FSMSetID;

		//! \brief Data type for FSM IDs.
		typedef		Common::IDWrapper<uint32,FSMManager,2>		FSMID;


		//! \brief Data type for state IDs.
		typedef		Common::IDWrapper<uint16,FSM,0>				FSMStateID;

		//! \brief Data type for end state IDs.
		typedef		Common::IDWrapper<uint16,FSM,1>				FSMEndStateID;

		//! \brief Data type for constant IDs.
		typedef		Common::IDWrapper<uint16,FSM,2>				FSMConstantID;

		//! \brief Data type for transition collection IDs.
		typedef		Common::IDWrapper<uint16,FSM,3>				FSMTransitionCollectionID;

		//! \brief Data type for transition IDs.
		typedef		Common::IDWrapper<uint16,FSM,4>				FSMTransitionID;
	}
}
