// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/control/FSMManager.h>
#include <xait/control/FSMMemory.h>
#include <xait/control/FSMTransition.h>
#include <xait/control/FSMTriggeredEvent.h>
#include <xait/control/EventSignatureBuilder.h>
#include <xait/control/FSMFunction.h>

#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/function/FunctionRegistrar.h>
#include <xait/common/reflection/function/CallBackFunctionRegistration.h>

#include <xait/common/memory/MemoryManaged.h>

#include <xait/common/String.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/container/List.h>
#include <xait/common/container/Pair.h>
#include <xait/common/threading/Mutex.h>
#include <xait/common/threading/Thread.h>
#include <xait/common/platform/Time.h>


#include <xait/common/utility/TemplatesUtility.h>

#define X_MAX_TRANSITION_COUNT 32
#define XAIT_FUNCTIONSTUB_WII_COMPILE XAIT::Common::Event::_Delegate3<void, XAIT::Common::IDWrapper<unsigned int, XAIT::Common::Reflection::Function::FunctionDeclarationManager, 0>, void *, const XAIT::Common::Reflection::Function::FunctionArgumentList &, XAIT::Common::Event::Delegate<void (XAIT::Common::IDWrapper<unsigned int, XAIT::Common::Reflection::Function::FunctionDeclarationManager, 0>, void *, const XAIT::Common::Reflection::Function::FunctionArgumentList &)> >

namespace XAIT
{

#ifdef XAIT_CONTROL_DEBUGGER
	namespace Common
	{
		namespace Debugger
		{
			class ObjectType;
			class ObjectInstance;
			class BreakpointInstance;
			class HistoryEntry;
		}
	}
#endif

	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{
				template<typename T_VALUE_TYPE>class ReferencedFSMMemberVariableMathValue;
				class UntypedReferencedFSMMemberVariableMathValue;
			};
		}
	}

	namespace Control
	{
		class Command;
		class FSMVariableAccessor;
		class FSMInstanceSerializer;
		struct FSMState;
		struct FSMTimeBasedTransition;

		//! \brief This class represents instances of FSMs.
		//!
		//! A FSM instance contains the current state, the memory and callbacks. Shared data is not stored in this class.
		class FSMInstance :  public ControlMemManagedObject
		{
			friend class FSM;
			friend class FSMVariable;
			friend class FSMInstanceVariable;
			friend class FSMManager;
			friend class FSMSet;
			friend class FSMFunction;
			friend  class FSMFunctionIterator;
			friend class FSMInstanceSerializer;
#ifndef XAIT_COMPILER_MSVC8
			template<typename T_VALUE_TYPE>friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue;
#else
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<bool>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<int32>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<int64>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<uint32>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<float32>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<Common::Math::Vec2f>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<Common::Math::Vec3f>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<Common::String>;
#endif
			friend class Common::Parser::Formula::UntypedReferencedFSMMemberVariableMathValue;
			friend class XAIT_FUNCTIONSTUB_WII_COMPILE;
			typedef uint16						SubFSMID;
		
		public:
			//! \brief Used to store the actual state in the FSM hierarchy.
			struct FSMStackEntry
			{
				//! \brief Default constructor.
				FSMStackEntry()
					: mSubFSMID(SubFSMID(-1))
				{}

				//! \brief Constructor.
				//! \param fsmType		type of the fsm 
				//! \param subFSMID		id of the subfsm (-1) in the last entry
				FSMStackEntry(FSMID fsmType, SubFSMID subFSMID)
					: mFSMID(fsmType), mSubFSMID(subFSMID)
				{}

				FSMID		mFSMID;
				SubFSMID	mSubFSMID;

				bool mHasStaticHistoryEntry;
				void* mStaticHistoryEntry;

			};

			//! \brief Destructor.
			XAIT_CONTROL_DLLAPI virtual ~FSMInstance();

			//! \brief Gets the name of the FSM instance.
			//! \returns the name of this FSM instance
			XAIT_CONTROL_DLLAPI const Common::String& getName() const;

			//! \brief Finishes the initialization and starts the FSM instance.
			XAIT_CONTROL_DLLAPI void start();

			//! \brief Checks if a FSM instance is started.
			//! \returns true if the FSM instance is running, and false otherwise
			//! \see startFSM
			XAIT_CONTROL_DLLAPI bool isStarted() const;

			//! \brief gets the fsm type that this instance started with
			//! \returns the type of fsm that defines the start point of this instance
			//! \remark the fsm type of an instance changes when a subfsm is entered or left
			//! \see getCurrentFSM
			XAIT_CONTROL_DLLAPI FSM* getRootFSM() const;

			//! \brief gets the fsm type that this instance is currently in
			//! \returns the type of subfsm that this instance is currently in
			//! \remark the fsm type of an instance changes when a subfsm is entered or left
			//! \see getRootFSM
			XAIT_CONTROL_DLLAPI FSM* getCurrentFSM() const;

			//! \brief gets the fsmset the type of this instance is defined in
			//! \returns the parent fsmset
			XAIT_CONTROL_DLLAPI FSMSet* getFSMSet() const;
			
			//! \brief Gets the FSMInstanceID of the FSM instance.
			//! \returns the FSMInstanceID of the FSM instance
			//! \remark use this id for serialization
			XAIT_CONTROL_DLLAPI FSMInstanceID getFSMInstanceID() const;

			//! \brief gets the event with the given name from given namespace
			//! \param name       the name of the event
			//! \param nameSpace  the namespace of the event
			//! \returns event with the given name from the given namespace
			XAIT_CONTROL_DLLAPI FSMEvent* getEvent(const Common::String& name, const Common::String& nameSpace) const;
			
			//! \brief gets the event with the given name from default namespace (Loc)
			//! \param name  the Name of the event
			//! \returns event with the name in the default namespace
			XAIT_CONTROL_DLLAPI FSMEvent* getEvent(const Common::String& name) const;
			
			XAIT_CONTROL_DLLAPI FSMEventIterator getEvents() const;

			//! \brief Prompts the instance to handle the event given by the parameter.
			//! \param eventID the ID of the event that should be handled
			//! \returns false if the event could not be handled because of wrong parameters (this behaviour can be changed for debugging, see FSMManager::setAllowIncompleteEvents(bool allowIncompleteEvents))
			//! \remark If the instance has not been started, events will be ignored.
			XAIT_CONTROL_DLLAPI bool handleEvent(const FSMEvent* event);

			//! \brief Prompts the instance to handle the event given by the parameter.
			//! \param eventID the ID of the event that should be handled
			//! \param parameters a FunctionArgumentList containing all the parameters for the given event
			//! \returns false if the event could not be handled because of wrong parameters (this behaviour can be changed for debugging, see FSMManager::setAllowIncompleteEvents(bool allowIncompleteEvents))
			//! \remark If the instance has not been started, events will be ignored.
			XAIT_CONTROL_DLLAPI bool handleEvent(const FSMEvent* event, Common::Reflection::Function::FunctionArgumentList& parameters);

			//! \brief Prompts the instance to handle the event given by the parameter.
			//! \param eventID the ID of the event that should be handled
			//! \param param1 the parameter of the event
			//! \returns false if the event could not be handled because of wrong parameters (this behaviour can be changed for debugging, see FSMManager::setAllowIncompleteEvents(bool allowIncompleteEvents))
			//! \remark If the instance has not been started, events will be ignored.
			template<typename PAR_TYPE_1>
			bool handleEvent(const FSMEvent* event, PAR_TYPE_1 param1);

			//! \overload
			template<typename PAR_TYPE_1, typename PAR_TYPE_2>
			bool handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2);

			//! \overload
			template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3>
			bool handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3);

			//! \overload
			template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4>
			bool handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4);

			//! \overload
			template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4, typename PAR_TYPE_5>
			bool handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4, PAR_TYPE_5 param5);

			//! \overload
			template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4, typename PAR_TYPE_5, typename PAR_TYPE_6>
			bool handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4, PAR_TYPE_5 param5, PAR_TYPE_6 param6);

			//! \overload
			template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4, typename PAR_TYPE_5, typename PAR_TYPE_6, typename PAR_TYPE_7>
			bool handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4, PAR_TYPE_5 param5, PAR_TYPE_6 param6, PAR_TYPE_7 param7);

			//! \overload
			template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4, typename PAR_TYPE_5, typename PAR_TYPE_6, typename PAR_TYPE_7, typename PAR_TYPE_8>
			bool handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4, PAR_TYPE_5 param5, PAR_TYPE_6 param6, PAR_TYPE_7 param7, PAR_TYPE_8 param8);

			//! \overload
			template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4, typename PAR_TYPE_5, typename PAR_TYPE_6, typename PAR_TYPE_7, typename PAR_TYPE_8, typename PAR_TYPE_9>
			bool handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4, PAR_TYPE_5 param5, PAR_TYPE_6 param6, PAR_TYPE_7 param7, PAR_TYPE_8 param8, PAR_TYPE_9 param9);
						
			//! \brief Updates the current gametime in the instance, and executes time-based transitions when they are due.
			//!
			//! This function is automatically executed when you call the update method of the FSMManager.
			//! \param gametime			the current gametime
			//! \see FSMManager::update
			XAIT_CONTROL_DLLAPI void update(Common::Platform::MilliSeconds gametime);

			//! \brief Suspends the FSM.
			//! \param skipSuspendedCommand bool flag the allows to skip the execution of a callback function that caused the suspension
			//!
			//! If the suspend happens during the execution of a callback function, the function will be called again after resuming; this can be changed by the parameter.
			//! The incoming events will be queued.
			//! \see resume
			//! \see isSuspended
			XAIT_CONTROL_DLLAPI void suspend(bool skipSuspendedCommand = false);

			//! \brief Resumes the execution of the FSM instance.
			//! 
			//! The FSM instance will continue from where it stopped. If it was stopped during the execution of a callback function, the function is called again.
			//! \see suspend
			//! \see isSuspended
			XAIT_CONTROL_DLLAPI void resume();

			//! \brief Checks whether the FSM instance is suspended.
			//! \returns true if the FSM instance is suspended, and false otherwise.
			//! \see suspend
			//! \see resume
			XAIT_CONTROL_DLLAPI bool isSuspended() const;

			//! \brief Gets the name of the current state.
			//! \returns the name of the current state
			//! \see getStateID
			//! \see getStateName
			XAIT_CONTROL_DLLAPI const Common::String& getCurrentStateName() const;

			//! \brief Gets the current FSM stack.
			//!
			//! The stack contains a list of stack entries which can be used to determine the position inside the FSM hierarchy.
			//! The entries are tuples of the FSMID (the type of the FSM) and the SubFSM ID
			//! \returns the FSM stack
			XAIT_CONTROL_DLLAPI const StaticManagedContainer<FSMStackEntry>::Vector& getFSMStack() const;

			//! \brief gets the variable with the given name from given namespace
			//! \param name       the Name of the variable
			//! \param nameSpace  the namespace of the variable
			//! \returns variable with the given name from the given namespace
			XAIT_CONTROL_DLLAPI FSMInstanceVariable getVariable(const Common::String& name, const Common::String& nameSpace) const;

			//! \brief gets the variable with the given name from default namespace (Loc)
			//! \param name  the name of the variable
			//! \returns variable with the name in the default namespace
			XAIT_CONTROL_DLLAPI FSMInstanceVariable getVariable(const Common::String& name) const;
			
			XAIT_CONTROL_DLLAPI FSMInstanceVariableIterator getVariables() const;

			//! \brief Restarts the FSM instance.
			//!
			//! When an instance is restarted, all instance data is reset.
			XAIT_CONTROL_DLLAPI void restartInstance();

			//! \brief Tells the instance to hold when reaching the next breakpoint.
			//!
			//! This effect occurs only if the debugger library is used and if a xaitControl Creator is connected.
			XAIT_CONTROL_DLLAPI void holdOnNextBreakpoint();

			//! \brief gives the possibility to store some custom data into the fsminstance
			//! \param customData the data to store
			XAIT_CONTROL_DLLAPI void setCustomData(void* customData);

			//! \brief method to retrieve the stored custom data
			//! \returns the stored custom data
			XAIT_CONTROL_DLLAPI void* getCustomData();

			//! \brief closes all transitions with identifier on this instance
			//! \param identifier to specify which transitions should be closed (identifier is defined in control creator)
			XAIT_CONTROL_DLLAPI void closeTransition(const Common::String& identifier);
			
			//! \brief closes all transitions of same type with identifier on this instance
			//! \param identifier to specify which transitions should be closed (identifier is defined in control creator)
			XAIT_CONTROL_DLLAPI void closeTransitionGroup(const Common::String& identifier);
			
			//! \brief opens all transitions with identifier on this instance
			//! \param identifier to specify which transitions should be opened (identifier is defined in control creator)
			XAIT_CONTROL_DLLAPI void openTransition(const Common::String& identifier);
			
			//! \brief opens all transitions of same type with identifier on this instance
			//! \param identifier to specify which transitions should be opened (identifier is defined in control creator)
			XAIT_CONTROL_DLLAPI void openTransitionGroup(const Common::String& identifier);

			//! \brief gets a wrapper for a function
			//! \param functionName			the name of the function
			//! \param functionNameSpace	the nameSpace of the function
			//! \param signature			the signature of the function
			//! \returns wrapper that can be used to access function data and interface
			FSMFunction getFunction(const Common::String& functionName,const Common::String& nameSpace, const Common::Reflection::Function::Signature& signature) const
			{
				Common::Reflection::Function::FunctionID funcID =  mFunctionRegistrar.getFunctionID(functionName, nameSpace,signature);
				return FSMFunction(funcID, this, &mFunctionRegistrar);
			}


			//! \brief gets a wrapper for a function in local namespace
			//! \param functionName			the name of the function
			//! \param signature			the signature of the function
			//! \returns wrapper that can be used to access function data and interface
			FSMFunction getFunction(const Common::String& functionName, const Common::Reflection::Function::Signature& signature) const
			{
				Common::Reflection::Function::FunctionID funcID =  mFunctionRegistrar.getFunctionID(functionName, X_DEFAULT_NAMESPACE,signature);
				return FSMFunction(funcID, this, &mFunctionRegistrar);
			}

			//! \brief register an external static function wrapper for default xaitControl-namespace (Loc) that can be used to create dynamic bindings
			//!
			//! supports functions with the signature void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems.  .
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param func		a pointer to the function
			//! \param fullSignature	the signature of the function
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			void registerStaticWrapperFunction(const Common::String& name,
				void (*func)(void* returnValue, Common::Reflection::Datatype::DatatypeID returnValueType,const Common::Reflection::Function::FunctionArgumentList&),
				const Common::Reflection::Function::Signature& fullSignature)
			{
				mFunctionRegistrar.registerWrapperFunction(name,X_DEFAULT_NAMESPACE,func,fullSignature);
			}

			//! \brief register an external static function wrapper that can be used to create dynamic bindings
			//!
			//! supports functions with the signature void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems.  If you don't need that, use an overload
			//! without signature parameter instead, it will detect the signature of your function automatically.
			//!
			//! \param name		   access name of the function in the xaitControl creator
			//! \param nameSpace   nameSpace of the function
			//! \param func		   a pointer to the function
			//! \param fullSignature	the signature of the function
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			void registerStaticWrapperFunction(const Common::String& name,
								  const Common::String& nameSpace,
								  void (*func)(void* returnValue, Common::Reflection::Datatype::DatatypeID returnValueType,const Common::Reflection::Function::FunctionArgumentList&),
								  const Common::Reflection::Function::Signature& fullSignature)
			{
				mFunctionRegistrar.registerWrapperFunction(name,nameSpace,func,fullSignature);
			}

			//! \brief register an external function wrapper for default xaitControl-namespace (Loc) that can be used to create dynamic bindings
			//!
			//! supports functions with the signature void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems.  If you don't need that, use an overload
			//! without signature parameter instead, it will detect the signature of your function automatically.
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param owner	owner object of the method
			//! \param func		a pointer to the function 
			//! \param fullSignature	the signature of the function
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			template<typename T_FUNCTIONOWNER_TYPE>
			void registerMemberWrapperFunction(const Common::String& name,
				const T_FUNCTIONOWNER_TYPE& owner,
				void (T_FUNCTIONOWNER_TYPE::*func)(void* returnValue, Common::Reflection::Datatype::DatatypeID returnValueType,const Common::Reflection::Function::FunctionArgumentList&),
				const Common::Reflection::Function::Signature& fullSignature)
			{
				mFunctionRegistrar.registerWrapperFunction(name,X_DEFAULT_NAMESPACE,owner,func,fullSignature);
			}

			//! \brief register an external function wrapper that can be used to create dynamic bindings
			//!
			//! supports functions with the signature void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems. If you don't need that, use an overload
			//! without signature parameter instead, it will detect the signature of your function automatically.
			//!
			//! \param name		 access name of the function in the xaitControl creator
			//! \param nameSpace nameSpace of the function
			//! \param owner	 owner object of the method
			//! \param func		 a pointer to the function 
			//! \param fullSignature	the signature of the function
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			template<typename T_FUNCTIONOWNER_TYPE>
			void registerMemberWrapperFunction(const Common::String& name,
								  const Common::String& nameSpace,
								  const T_FUNCTIONOWNER_TYPE& owner,
								  void (T_FUNCTIONOWNER_TYPE::*func)(void* returnValue, Common::Reflection::Datatype::DatatypeID returnValueType,const Common::Reflection::Function::FunctionArgumentList&),
								  const Common::Reflection::Function::Signature& fullSignature)
			{
				mFunctionRegistrar.registerWrapperFunction(name,nameSpace,owner,func,fullSignature);
			}


			//! \brief register an external static function for default xaitControl-namespace (Loc) with up to 10 arguments with automatic signature detection
			//!
			//! the signature of the function will be detected automatically, and the correct overload of the function will be registered
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param func		a pointer to the function 
			template <typename T_FUNC>
			void registerStaticFunction(const Common::String& name, T_FUNC func)
			{
				mFunctionRegistrar.registerFunction(name,X_DEFAULT_NAMESPACE,func);
			}

			//! \brief register an external static function with up to 10 arguments with automatic signature detection
			//! 
			//! the signature of the function will be detected automatically, and the correct overload of the function will be registered
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param nameSpace nameSpace of the function
			//! \param func		a pointer to the function
			template <typename T_FUNC>
			void registerStaticFunction(const Common::String& name,const Common::String& nameSpace, T_FUNC func)
			{
				mFunctionRegistrar.registerFunction(name,nameSpace,func);
			}

			//! \brief register an external member function for default xaitControl-namespace (Loc) with up to 10 arguments with automatic signature detection
			//!
			//! the signature of the function will be detected automatically, and the correct overload of the function will be registered
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param owner	owner object of the method
			//! \param func		a pointer to the method
			template<typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
			void registerMemberFunction(const Common::String& name, const T_FUNCTIONOWNER_TYPE& owner, T_FUNC func)
			{
				mFunctionRegistrar.registerFunction(name,X_DEFAULT_NAMESPACE,owner,func);
			}
			
			//! \brief register an external member function with up to 10 arguments with automatic signature detection
			//! 
			//! the signature of the function will be detected automatically, and the correct overload of the function will be registered
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param nameSpace nameSpace of the function
			//! \param owner	owner object of the method
			//! \param func		a pointer to the method
			template<typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
			void registerMemberFunction(const Common::String& name,const Common::String& nameSpace, const T_FUNCTIONOWNER_TYPE& owner, T_FUNC func)
			{
				mFunctionRegistrar.registerFunction(name,nameSpace,owner,func);
			}


			//! \brief register external static function for default xaitControl-namespace (Loc) with a dynamic argument list 
			//! 
			//! if you register a callback this way, all overloading of the given function that are not registered separately call this function
			//! tha callbackfunction has to have the signature: void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
			//!
			//! \param name							access name of the function
			//! \param func							a pointer to the function 
			//! \param overrideExistingBindings		flag if existing bindings should be overridden
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			void registerStaticVariadicFunction(const Common::String& name, void (*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&), Common::Reflection::Datatype::DatatypeID returnType, bool overrideExistingBindings = false)
			{
				mFunctionRegistrar.registerVariadicFunction(name,X_DEFAULT_NAMESPACE,func,returnType,overrideExistingBindings);
			}

			//! \brief register external static function with a dynamic argument list 
			//! 
			//! if you register a callback this way, all overloading of the given function that are not registered separately call this function
			//! tha callbackfunction has to have the signature: void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
			//!
			//! \param name							access name of the function
			//! \param nameSpace                    nameSpace of the function
			//! \param func							a pointer to the function 
			//! \param overrideExistingBindings		flag if existing bindings should be overridden
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			void registerStaticVariadicFunction(const Common::String& name,const Common::String& nameSpace, void (*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&), Common::Reflection::Datatype::DatatypeID returnType, bool overrideExistingBindings = false)
			{
				mFunctionRegistrar.registerVariadicFunction(name,nameSpace,func,returnType,overrideExistingBindings);
			}

			//! \brief register external member function for default xaitControl-namespace (Loc) with a dynamic argument list 
			//! 
			//! if you register a callback this way, all overloading of the given function that are not registered separately call this function
			//! tha callbackfunction has to have the signature: void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
			//!
			//! \param name							access name of the function
			//! \param owner						ownerclass of the method
			//! \param func							a pointer to the function 
			//! \param overrideExistingBindings		flag if existing bindings should be overridden
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			template<typename T_FUNCTIONOWNER_TYPE>
			void registerMemberVariadicFunction(const Common::String& name, const T_FUNCTIONOWNER_TYPE& owner,void (T_FUNCTIONOWNER_TYPE::*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&), Common::Reflection::Datatype::DatatypeID returnType, bool overrideExistingBindings = false)
			{
				mFunctionRegistrar.registerVariadicFunction(name,X_DEFAULT_NAMESPACE,owner,func,returnType,overrideExistingBindings);
			}


			//! \brief register external member function with a dynamic argument list 
			//! 
			//! if you register a callback this way, all overloading of the given function that are not registered separately call this function
			//! tha callbackfunction has to have the signature: void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
			//!
			//! \param name							access name of the function
			//! \param nameSpace                    nameSpace of the function
			//! \param owner						ownerclass of the method
			//! \param func							a pointer to the function 
			//! \param overrideExistingBindings		flag if existing bindings should be overridden
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			template<typename T_FUNCTIONOWNER_TYPE>
			void registerMemberVariadicFunction(const Common::String& name,const Common::String& nameSpace, const T_FUNCTIONOWNER_TYPE& owner,void (T_FUNCTIONOWNER_TYPE::*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&), Common::Reflection::Datatype::DatatypeID returnType, bool overrideExistingBindings = false)
			{
				mFunctionRegistrar.registerVariadicFunction(name,nameSpace,owner,func,returnType,overrideExistingBindings);
			}
 
			//! \brief gets the required functions of the instance
			//! \returns the required functions of the fsm instance
			const FSMFunctionIterator getRequiredFunctions() const
			{
				return FSMFunctionIterator(this);
			}

			

		private:

			enum TransitionProcessResult
			{
				TRANSITION_TAKEN,
				PROCESS_CANCELLED,
				NONE_FOUND
			};

			bool												mStarted;
			FSMInstanceID										mOwnID;

			Common::String										mName;

			StaticManagedContainer<FSMStackEntry>::Vector		mFSMStack;

			bool												mSuspended;
			bool												mSkipSuspendedCommand;
			bool												mHandlingEvent;
			FSMTransitionCollectionID							mCurrentFSMTransitionCollectionID;
			uint16												mCurrentTransitionIndex;
			bool												mCurrentElseCase;

			uint16												mNextCommand;
			uint08												mCurrentUpCount;
			
			uint32												mNextLocalTimebasedTransition		: 15;
			uint32												mNextExitTimebasedTransition		: 15;
			uint32												mLocalActive						: 1;
			uint32												mSuspendedUpdate					: 1;

			Common::Platform::MilliSeconds						mTimer;
			
			Common::Container::List<FSMTriggeredEvent>			mCommandEventQueue;
			Common::Container::List<FSMTriggeredEvent>			mEventQueue;

			FSMID												mRootFSMID;
			FSMID												mCurrentFSM;
			FSMStateID											mCurrentState;
			FSMMemory											mFSMMemory;

			Common::Reflection::Function::FunctionRegistrar		mFunctionRegistrar;
			FSMVariableAccessor*								mVariableAccessor;

			Common::Threading::Mutex							mMutex;

			bool												mShouldRestart;

			void*												mCustomData;

			Common::Container::HashMap<uint32, bool> mClosedTransitions;

			//! \brief Constructor.
			//! \param fsmID	id of the root FSM for this instance
			FSMInstance(FSMID fsmID, const Common::String& name, FSMInstanceID instanceID);
			
			FSMInstance();

			void init();

			void checkTransitions(FSMTriggeredEvent& eventID);			

			void finishHandling(FSMEvent* event);

			//! \brief Sets the ID of the instance.
			//! \param instanceID			new id of the instance
			void setFSMInstanceID(const FSMInstanceID instanceID);

			//! \brief Executes a list of commands.
			//! \param commands		the list of commands
			void executeCommands(const StaticManagedContainer<Command*>::Vector& commands, const uint32 startIndex);

			//! \brief Changes to a state.
			//! \param upCount
			//! \param destinationID	the subFSM destination
			//! \param stateID	the destination state
			void changeStateTo(uint32 upCount,int32 destinationID, FSMStateID stateID);

			
			//! \brief Finds a transition that's eligible to execute, and executes it.
			//! \param eventTransitionID	the FSMTransitionCollectionID referring to the transition collection containing the transitions
			//! \param upCount				>0 if transitions are from a subfsm to a parentfsm
			//! \returns false if no transitions have been choosen, true otherwise
			bool proceedTransitions(const FSMTransitionCollectionID& transitionCollectionID, uint32 upCount);
			bool proceedTransitionPrioritySpan(const StaticManagedContainer<FSMTransition*>::Vector& transitions, uint08 startIndex, uint08 endIndex, float32 randomValue, float32 noOpProb, uint32& returnValue);

			//! \brief checks whether the given transition is currently closed
			//! \param transition the transition that should be checked
			//! \returns whether the transition is closed
			bool isTransitionClosed(FSMTransition* transition);

			//! \brief Handles the direct transitions.
			//! \returns true, if a transition has been executed, false otherwise
			bool handleDirectTransitions();

			void handleCommandEventQueue();

			void timerUpdate(Common::Platform::MilliSeconds gametime);

			void reset();

			void handleRestart();

			Common::Platform::MilliSeconds getCurrentTimeSpan() const;

			const FSMState* getCurrentState() const;

			void chooseNextTimer();

			const StaticManagedContainer<FSMTimeBasedTransition>::Vector* getExitTimebasedTransitions() const;

			void initFromStream(Common::Serialize::SerializeStorage* stream);

			void writeToStream(Common::Serialize::SerializeStorage* stream);
			
			static int32 startInstanceHandler(FSMInstance* instance);

			void registerDefaultFunctions();

			int32 getWrappedInstanceID();

			void buildDefaultEventParameters(Common::Reflection::Function::FunctionArgumentList& eventParameters, uint32 startIndex);

			void onUnboundFunctionCall(Common::Reflection::Function::FunctionID functionID, void* returnValue, const Common::Reflection::Function::FunctionArgumentList& argList);

		    FSMEvent* getEvent(FSMEventID eventID) const;

			FSMInstanceVariable getVariable(FSMVarID varID) const;
		
			void closeTransition(uint32 identifier);
			void closeTransitionGroup(uint32 identifier);
			void openTransition(uint32 identifier);
			void openTransitionGroup(uint32 identifier);

			////////////////////////////////
			////	Debugger Part		////
			////////////////////////////////

#ifdef XAIT_CONTROL_DEBUGGER
		public:			


		private:

			Common::Container::List<FSMTriggeredEvent> getCommandEvents() const;
			Common::Container::List<FSMTriggeredEvent> getEventQueue() const;
			void clearEventQueues();
			void addEvent(FSMTriggeredEvent event);
			void supressTransition(const Common::String& sourceState, const Common::String& transitionName, XAIT::uint32 transitionIndex);
			void unsupressTransition(const Common::String& sourceState, const Common::String& transitionName, XAIT::uint32 transitionIndex);

			void attachToObjectInstance(Common::Debugger::ObjectType* type);
			Common::Debugger::ObjectInstance*	getObjectInstance() const;

			bool isTransitionSupressed(const Common::String& state, const Common::String& transitionName, uint32 transitionID) const;

			void handleBreakpoint(Common::Debugger::BreakpointInstance* breakPoint, bool staticBreakpoint = false);

			Common::Debugger::ObjectInstance*			mObjectInstance;
			Common::Container::LookUp<Common::String, Common::Container::List<Common::Container::Pair<uint32, Common::String> > > mSupressedTransitions;
			Common::Debugger::HistoryEntry* mCurrentStaticHistory;
#endif	

		};		
	}
}

template<typename PAR_TYPE_1>
bool XAIT::Control::FSMInstance::handleEvent(const FSMEvent* event, PAR_TYPE_1 param1)
{
#ifdef XAIT_SHIPPING 
	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = event->getSignature();
#else
	EventSignatureBuilder sigBuilder(1, getAllocator());
	Common::Utility::ForeachParam::apply(sigBuilder, param1);
	if(sigBuilder.containsError())
		return false;

	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = sigBuilder.getSignature();
#endif	

	EventArgumentBuilder argBuilder(signature, getAllocator());
	Common::Utility::ForeachParam::apply(argBuilder, param1);

	return handleEvent( event, argBuilder.getArgumentList());
}

template<typename PAR_TYPE_1, typename PAR_TYPE_2>
bool XAIT::Control::FSMInstance::handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2)
{
#ifdef XAIT_SHIPPING 
	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = event->getSignature();
#else
	EventSignatureBuilder sigBuilder(2, getAllocator());
	Common::Utility::ForeachParam::apply(sigBuilder, param1, param2);
	if(sigBuilder.containsError())
		return false;

	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = sigBuilder.getSignature();
#endif	

	EventArgumentBuilder argBuilder(signature, getAllocator());
	Common::Utility::ForeachParam::apply(argBuilder, param1, param2);

	return handleEvent( event, argBuilder.getArgumentList());
}

template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3>
bool XAIT::Control::FSMInstance::handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3)
{
#ifdef XAIT_SHIPPING 
	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = event->getSignature();
#else
	EventSignatureBuilder sigBuilder(3, getAllocator());
	Common::Utility::ForeachParam::apply(sigBuilder, param1, param2, param3);
	if(sigBuilder.containsError())
		return false;

	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = sigBuilder.getSignature();
#endif	

	EventArgumentBuilder argBuilder(signature, getAllocator());
	Common::Utility::ForeachParam::apply(argBuilder, param1, param2, param3);

	return handleEvent( event, argBuilder.getArgumentList());
}

template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4>
bool XAIT::Control::FSMInstance::handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4)
{
#ifdef XAIT_SHIPPING 
	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = event->getSignature();
#else
	EventSignatureBuilder sigBuilder(4, getAllocator());
	Common::Utility::ForeachParam::apply(sigBuilder, param1, param2, param3, param4);
	if(sigBuilder.containsError())
		return false;

	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = sigBuilder.getSignature();
#endif	

	EventArgumentBuilder argBuilder(signature, getAllocator());
	Common::Utility::ForeachParam::apply(argBuilder, param1, param2, param3, param4);

	return handleEvent( event, argBuilder.getArgumentList());
}

template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4, typename PAR_TYPE_5>
bool XAIT::Control::FSMInstance::handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4, PAR_TYPE_5 param5)
{
#ifdef XAIT_SHIPPING 
	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = event->getSignature();
#else
	EventSignatureBuilder sigBuilder(5, getAllocator());
	Common::Utility::ForeachParam::apply(sigBuilder, param1, param2, param3, param4, param5);
	if(sigBuilder.containsError())
		return false;

	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = sigBuilder.getSignature();
#endif	

	EventArgumentBuilder argBuilder(signature, getAllocator());
	Common::Utility::ForeachParam::apply(argBuilder, param1, param2, param3, param4, param5);

	return handleEvent( event, argBuilder.getArgumentList());
}

template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4, typename PAR_TYPE_5, typename PAR_TYPE_6>
bool XAIT::Control::FSMInstance::handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4, PAR_TYPE_5 param5, PAR_TYPE_6 param6)
{
#ifdef XAIT_SHIPPING 
	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = event->getSignature();
#else
	EventSignatureBuilder sigBuilder(6, getAllocator());
	Common::Utility::ForeachParam::apply(sigBuilder, param1, param2, param3, param4, param5, param6);
	if(sigBuilder.containsError())
		return false;

	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = sigBuilder.getSignature();
#endif	

	EventArgumentBuilder argBuilder(signature, getAllocator());
	Common::Utility::ForeachParam::apply(argBuilder, param1, param2, param3, param4, param5, param6);

	return handleEvent( event, argBuilder.getArgumentList());
}

template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4, typename PAR_TYPE_5, typename PAR_TYPE_6, typename PAR_TYPE_7>
bool XAIT::Control::FSMInstance::handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4, PAR_TYPE_5 param5, PAR_TYPE_6 param6, PAR_TYPE_7 param7)
{
#ifdef XAIT_SHIPPING 
	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = event->getSignature();
#else
	EventSignatureBuilder sigBuilder(7, getAllocator());
	Common::Utility::ForeachParam::apply(sigBuilder, param1, param2, param3, param4, param5, param6, param7);
	if(sigBuilder.containsError())
		return false;

	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = sigBuilder.getSignature();
#endif	

	EventArgumentBuilder argBuilder(signature, getAllocator());
	Common::Utility::ForeachParam::apply(argBuilder, param1, param2, param3, param4, param5, param6, param7);

	return handleEvent( event, argBuilder.getArgumentList());
}

template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4, typename PAR_TYPE_5, typename PAR_TYPE_6, typename PAR_TYPE_7, typename PAR_TYPE_8>
bool XAIT::Control::FSMInstance::handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4, PAR_TYPE_5 param5, PAR_TYPE_6 param6, PAR_TYPE_7 param7, PAR_TYPE_8 param8)
{
#ifdef XAIT_SHIPPING 
	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = event->getSignature();
#else
	EventSignatureBuilder sigBuilder(8, getAllocator());
	Common::Utility::ForeachParam::apply(sigBuilder, param1, param2, param3, param4, param5, param6, param7, param8);
	if(sigBuilder.containsError())
		return false;

	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = sigBuilder.getSignature();
#endif	

	EventArgumentBuilder argBuilder(signature, getAllocator());
	Common::Utility::ForeachParam::apply(argBuilder, param1, param2, param3, param4, param5, param6, param7, param8);

	return handleEvent( event, argBuilder.getArgumentList());
}

template<typename PAR_TYPE_1, typename PAR_TYPE_2, typename PAR_TYPE_3, typename PAR_TYPE_4, typename PAR_TYPE_5, typename PAR_TYPE_6, typename PAR_TYPE_7, typename PAR_TYPE_8, typename PAR_TYPE_9>
bool XAIT::Control::FSMInstance::handleEvent(const FSMEvent* event, PAR_TYPE_1 param1, PAR_TYPE_2 param2, PAR_TYPE_3 param3, PAR_TYPE_4 param4, PAR_TYPE_5 param5, PAR_TYPE_6 param6, PAR_TYPE_7 param7, PAR_TYPE_8 param8, PAR_TYPE_9 param9)
{
#ifdef XAIT_SHIPPING 
	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = event->getSignature();
#else
	EventSignatureBuilder sigBuilder(9, getAllocator());
	Common::Utility::ForeachParam::apply(sigBuilder, param1, param2, param3, param4, param5, param6, param7, param8, param9);
	if(sigBuilder.containsError())
		return false;

	Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> signature = sigBuilder.getSignature();
#endif	

	EventArgumentBuilder argBuilder(signature, getAllocator());
	Common::Utility::ForeachParam::apply(argBuilder, param1, param2, param3, param4, param5, param6, param7, param8, param9);

	return handleEvent( event, argBuilder.getArgumentList());
}