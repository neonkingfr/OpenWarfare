// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/reflection/function/FunctionRegistrar.h>

namespace XAIT
{
	namespace Control
	{
		class FSM;
		class FSMInstance;
		class FSMFunctionIterator;

		class FSMFunction
		{

			friend class FSM;
			friend class FSMInstance;
			friend class FSMFunctionIterator;

		public:

			//! \brief checks if the function is registered
			//! \returns true if it is registered
			XAIT_CONTROL_DLLAPI bool isRegistered() const;

			//! \brief checks if this function wrapper is valid
			//! \returns true if valid
			XAIT_CONTROL_DLLAPI bool isValid() const;

			//! \brief gets the full name of the function (namespace included)
			//! \returns the full name of the event
			XAIT_CONTROL_DLLAPI const Common::String getFullName() const;

			//! \brief gets the name of a function
			//! \returns the name of the function
			XAIT_CONTROL_DLLAPI const Common::String getName() const;
			
			//! \brief gets the namespace of a function
			//! \returns the namespace of the function
			XAIT_CONTROL_DLLAPI const Common::String getNameSpace() const;
			
			//! \brief gets the signature of a function
			//! \returns the signature of the function
			XAIT_CONTROL_DLLAPI const Common::Reflection::Function::Signature getSignature() const;

			//! \brief gets the fsm this function belongs to (NULL if no FSM is the owner)
			//! \returns pointer to fsm
			XAIT_CONTROL_DLLAPI const FSM* getFSM() const;

			//! \brief gets the fsmInstance this function belongs to
			//! \returns pointer to fsmInstance (NULL if no FSMInstance is the owner)
			XAIT_CONTROL_DLLAPI const FSMInstance* getFSMInstance() const;


		private:

			XAIT_CONTROL_DLLAPI FSMFunction(Common::Reflection::Function::FunctionID funcID, const FSM* ownerFSM, const Common::Reflection::Function::FunctionRegistrar* functionRegistrar);
			XAIT_CONTROL_DLLAPI FSMFunction(Common::Reflection::Function::FunctionID funcID, const FSMInstance* ownerInstance, const Common::Reflection::Function::FunctionRegistrar* functionRegistrar);


			Common::Reflection::Function::FunctionID mFuncID;
			const FSM* mOwnerFSM;
			const FSMInstance* mOwnerInstance;
			const Common::Reflection::Function::FunctionRegistrar* mFunctionRegistrar;

		};
	}
}

