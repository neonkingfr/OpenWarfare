// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/debugger/ConnectionLayer.h>
#include <xait/control/FSMTypedefs.h>

namespace XAIT
{
	namespace Control
	{
		//! The DebuggerAddress specifies the local endpoint of the tcp connection for the control debugger.
		//! This can be either only a port or a local device address and a port.
		//! There is currently no address selection on Wii, default will be used.
		class DebuggerAddress
		{
		public:
			//! \brief Default contructor, use default settings.
			XAIT_CONTROL_DLLAPI DebuggerAddress();

#if XAIT_OS != XAIT_OS_WII
			//! \brief Define local device and port
			//! \param localDeviceAddress	Address of local device. Use IPV4::ANY if device not specified.
			//! \param port					Local port for the debugger
			XAIT_CONTROL_DLLAPI DebuggerAddress(const Common::Network::Sockets::IPV4& localDeviceAddress, const uint16 port);

			//! \brief Define port and use local default device
			//! \param port					Local port for the debugger
			XAIT_CONTROL_DLLAPI DebuggerAddress(const uint16 port);

			//! \brief Define local device and use default port
			//! \param localDeviceAddress	Address of local device. Use IPV4::ANY if device not specified.
			XAIT_CONTROL_DLLAPI DebuggerAddress(const Common::Network::Sockets::IPV4& localDeviceAddress);
#endif

			CONNECTION_LAYER_ADDRESS getAddress() const
			{
				return mAddress;
			}

		private:
			CONNECTION_LAYER_ADDRESS	mAddress;
		};
	}
}

