// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/DLLDefines.h>
#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/function/FunctionTypedefs.h>
#include <xait/common/container/Vector.h>
#include <xait/common/container/FixedVector.h>

#define X_FUNC_ARG_INPLACE_SIZE 32
#define X_MAX_SIG_SIZE 10

namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			class SerializeStorage;
		}

		namespace Reflection
		{
			namespace Function
			{

				class FunctionArgumentList
				{
					template<typename T_TYPE>
					class IntegerConverter
					{
					public:
						typedef T_TYPE	Type;
					};



				public:
					//! \brief constructor
					//! \param size	size of the argList
					XAIT_COMMON_DLLAPI FunctionArgumentList(const Container::FixedVector<Datatype::DatatypeID, X_MAX_SIG_SIZE>& argumentsignature, const Memory::AllocatorPtr& allocator);

					//! \brief serialisation constructor
					XAIT_COMMON_DLLAPI FunctionArgumentList(XAIT::Common::Serialize::SerializeStorage* stream, const Memory::AllocatorPtr& allocator);

					//! \brief copy constructor
					//! \param functionArgumentList		the other functionargumentlist
					XAIT_COMMON_DLLAPI FunctionArgumentList(const FunctionArgumentList& functionArgumentList);

					//! \brief destructor
					XAIT_COMMON_DLLAPI ~FunctionArgumentList();

					XAIT_COMMON_DLLAPI FunctionArgumentList& operator=(const FunctionArgumentList& other);

					//! \brief gets the value of an argument
					//! \param index		the index of the argument
					//! \returns the value
					template<typename T_ARG_TYPE>
					T_ARG_TYPE& getValue(uint32 index) const
					{
						return *((T_ARG_TYPE*) getValuePtr(index));
					}

					//! \brief gets a pointer of the value of an argument
					//! \param index		the index of the argument
					//! \returns a pointer to the value
					template<typename T_ARG_TYPE>
					T_ARG_TYPE* getValuePtr(uint32 index) const
					{
						return (T_ARG_TYPE*) getValuePtr(index);
					}

					//! \brief gets the value of an entry
					//! \param index	index of the value
					//! \return the value
					XAIT_COMMON_DLLAPI void* getValuePtr(uint32 index) const;

					//! \brief sets the value of an entry
					//! \param value		the new value
					//! \param datatypeID	the datatype of the argument
					//! \param index		index of the value
					XAIT_COMMON_DLLAPI void setValue(void* value, const Datatype::DatatypeID datatypeID, uint32 index);

					//! \brief sets the value of an entry
					//! \param value	the new value
					//! \param index	index of the value
					XAIT_COMMON_DLLAPI void setValue(void* value, uint32 index);

					//! \brief	gets the type of an entry
					//! \param	index	index of the entry
					//! \returns the type of the entry
					XAIT_COMMON_DLLAPI Datatype::DatatypeID getDatatypeID(uint32 index) const;

					//! \brief gets the size of the argumentlist
					XAIT_COMMON_DLLAPI uint32 size() const;
					
					//! \brief stores the functionargumentlist to the stream
					//! \param stream	the stream to store the argumentlist to 
					XAIT_COMMON_DLLAPI void storeToStream(XAIT::Common::Serialize::SerializeStorage* stream );

				protected:
					uint32						mSize;

					uint08						mStaticData[X_FUNC_ARG_INPLACE_SIZE];
					void*						mDynamicData;
					Datatype::DatatypeID		mSignature[X_MAX_SIG_SIZE];
					sysptr						mAttributeOffsets[10];

					Memory::Allocator::Ptr		mAllocator;

					void initializeArguments();
					void initializeArguments(const FunctionArgumentList& other);
					void initializeArguments(Common::Serialize::SerializeStorage* stream);
				};

				template<>
				class FunctionArgumentList::IntegerConverter<uint32>
				{
				public:
					typedef int32	Type;
				};
			}
		}
	}
}
