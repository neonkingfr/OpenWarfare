// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/threading/SpinLock.h>

#include <xait/common/reflection/function/CFunction.h>
#include <xait/common/reflection/function/FunctionRegistration.h>
#include <xait/common/reflection/function/SignatureCreator.h>
#include <xait/common/reflection/function/FunctionDeclarationManager.h>
#include <xait/common/reflection/function/FunctionTypedefs.h>
#include <xait/common/event/Delegate.h>

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{
				typedef Common::Event::Delegate<void (Common::Reflection::Function::FunctionID, void*, const Common::Reflection::Function::FunctionArgumentList&)>  UnboundFunctionCallDelegate;

				class FunctionRegistrar
				{
				public:
					//! \brief constructor
					XAIT_COMMON_DLLAPI FunctionRegistrar(const Memory::AllocatorPtr& allocator);

					//! \brief constructor
					XAIT_COMMON_DLLAPI FunctionRegistrar(const FunctionDeclarationManager::Ptr& functionDeclarations, const Memory::AllocatorPtr& allocator);

					//! \brief copy constructor
					XAIT_COMMON_DLLAPI FunctionRegistrar(const FunctionRegistrar& other);

					//! \brief destructor
					XAIT_COMMON_DLLAPI ~FunctionRegistrar();

					//! \brief assignment operator
					XAIT_COMMON_DLLAPI const FunctionRegistrar& operator=(const FunctionRegistrar& other);


					//! \brief register external static function wrapper that can be used to create dynamic bindings
					//! 
					//! supports functions with the signature void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
					//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
					//! 
					//! \param name				access name of the function
					//! \param namesSpace       nameSpace of the function
					//! \param func				a pointer to the function (with signature: functionName(void* returnvalue, DataTypeID returnValueType, FunctionArgumentList argList)
					//! \param fullSignature	the full signature of the function.
					//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
					void registerWrapperFunction(const String& name, const String& nameSpace, void (*func)(void*,Datatype::DatatypeID,const FunctionArgumentList&), const Signature& fullSignature)
					{
						internalRegister(name,nameSpace,FunctionCaller<void>::callFunc,FunctionCaller<void>::callFuncWR,0,0,&func,sizeof(func),fullSignature);
					}

					

					//! \brief register external function wrapper that can be used to create dynamic bindings
					//! 
					//! supports functions with the signature void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
					//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
					//! 
					//! \param name		 access name of the function
					//! \param nameSpace nameSpace of the function
					//! \param owner	 ownerclass of the method
					//! \param func		 a pointer to the function (with signature: functionName(void* returnvalue, DataTypeID returnValueType, FunctionArgumentList argList)
					//! \param fullSignature	the full signature of the function. 
					//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
					template<typename T_FUNCTIONOWNER_TYPE>
					void registerWrapperFunction(const String& name, const String& nameSpace, const T_FUNCTIONOWNER_TYPE& owner,void (T_FUNCTIONOWNER_TYPE::*func)(void*,Datatype::DatatypeID,const FunctionArgumentList&), const Signature& fullSignature)
					{
						internalRegister(name,nameSpace,MethodCaller<void,T_FUNCTIONOWNER_TYPE>::callFunc,MethodCaller<void,T_FUNCTIONOWNER_TYPE>::callFuncWR,(void*)&owner,sizeof(&owner),&func,sizeof(func),fullSignature);
					}

					//! \brief register external static function with a dynamic argument list
					//! 
					//! if you register a callback this way, all overloading of the given function that are not registered separately call this function
					//! tha callbackfunction has to have the signature: void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
					//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
					//! 
					//! \param name							access name of the function
					//! \param func							a pointer to the function 
					//! \param overrideExistingBindings		flag if existing bindings should be overridden
					//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
					void registerVariadicFunction(const String& name, const String& nameSpace, void (*func)(void*,Datatype::DatatypeID,const FunctionArgumentList&), Datatype::DatatypeID returnType, bool overrideExistingBindings)
					{
						Common::Reflection::Function::Signature signature(mAllocator);
						signature.mReturnType = returnType;
						signature.mVariadic = true;
						internalRegister(name,nameSpace,FunctionCaller<void>::callFunc,FunctionCaller<void>::callFuncWR,0,0,&func,sizeof(func),signature,overrideExistingBindings);
					}
					//! \brief register external function with a dynamic argument list
					//! 
					//! if you register a callback this way, all overloading of the given function that are not registered separately call this function
					//! tha callbackfunction has to have the signature: void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
					//! such a callback function can be used to create dynamic bindings of existing data driven systems
					//! 
					//! \param name							access name of the function
					//! \param owner						ownerclass of the method
					//! \param func							a pointer to the function 
					//! \param overrideExistingBindings		flag if existing bindings should be overridden
					//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
					template<typename T_FUNCTIONOWNER_TYPE>
					void registerVariadicFunction(const String& name, const String& nameSpace, const T_FUNCTIONOWNER_TYPE& owner,void (T_FUNCTIONOWNER_TYPE::*func)(void*,Datatype::DatatypeID,const FunctionArgumentList&), Datatype::DatatypeID returnType, bool overrideExistingBindings)
					{
						Common::Reflection::Function::Signature signature(mAllocator);
						signature.mReturnType = returnType;
						signature.mVariadic = true;
						internalRegister(name,nameSpace,MethodCaller<void,T_FUNCTIONOWNER_TYPE>::callFunc,MethodCaller<void,T_FUNCTIONOWNER_TYPE>::callFuncWR,(void*)&owner,sizeof(&owner),&func,sizeof(func),signature,overrideExistingBindings);
					}

					//! \brief register external static function with up to 10 arguments with compiletime/runtime check
					//!
					//! the signature of the function will be detected automatically, and the correct overload of the function will be registered
					//! 
					//! \param fullName  fullname of the function including namespace
					//! \param name		 access name of the function
					//! \param nameSpace nameSpace of the function
					//! \param func		a pointer to the function 
					//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
					template <typename T_FUNC>
					FunctionID registerFunctionByFullName(const String& fullName, T_FUNC func)
					{
						Signature signature(mAllocator);
						SignatureCreatorHelper::createSignature(func,signature);
						return internalRegister(fullName,ArgFunctionCaller<T_FUNC>::callFunc,ArgFunctionCaller<T_FUNC>::callFuncWR,0,0,&func,sizeof(func),signature);
					}

					//! \brief register external static function with  up to 10 arguments with compiletime/runtime check
					//! 
					//! the signature of the function will be detected automatically, and the correct overload of the function will be registered
					//! 
					//! \param name		access name of the function
					//! \param func		a pointer to the function 
					//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
					template <typename T_FUNC>
					FunctionID registerFunction(const String& name, const String& nameSpace, T_FUNC func)
					{
						Signature signature(mAllocator);
						SignatureCreatorHelper::createSignature(func,signature);
						return internalRegister(name,nameSpace,ArgFunctionCaller<T_FUNC>::callFunc,ArgFunctionCaller<T_FUNC>::callFuncWR,0,0,&func,sizeof(func),signature);
					}

					//! \brief register external member function with up to 10 arguments with compiletime/runtime check
					//! 
					//! the signature of the function will be detected automatically, and the correct overload of the function will be registered
					//! 
					//! \param name		access name of the function
					//! \param func		a pointer to the function 
					//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
					template <typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
					FunctionID registerFunctionByFullName(const String& fullName, T_FUNC func)
					{
						Signature signature(mAllocator);
						SignatureCreatorHelper::createMemberSignature<T_FUNCTIONOWNER_TYPE>(func,signature);
						return internalRegister(fullName,ArgOwnMethodCaller<T_FUNCTIONOWNER_TYPE,T_FUNC>::callFunc,ArgOwnMethodCaller<T_FUNCTIONOWNER_TYPE,T_FUNC>::callFuncWR,0,0,&func,sizeof(func),signature);
					}

			
					//! \brief register external member function with up to 10 arguments with compiletime/runtime check
					//! 
					//! the signature of the function will be detected automatically, and the correct overload of the function will be registered
					//! 
					//! \param name		access name of the function
					//! \param func		a pointer to the function 
					//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
					template <typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
					FunctionID registerFunction(const String& name, const String& nameSpace, T_FUNC func)
					{
						Signature signature(mAllocator);
						SignatureCreatorHelper::createMemberSignature<T_FUNCTIONOWNER_TYPE>(func,signature);
						return internalRegister(name,nameSpace,ArgOwnMethodCaller<T_FUNCTIONOWNER_TYPE,T_FUNC>::callFunc,ArgOwnMethodCaller<T_FUNCTIONOWNER_TYPE,T_FUNC>::callFuncWR,0,0,&func,sizeof(func),signature);
					}

					//! \brief register external member function with up to 10 arguments with compiletime/runtime check
					//! 
					//! the signature of the function will be detected automatically, and the correct overload of the function will be registered
					//! 
					//! \param name		 access name of the function
					//! \param nameSpace nameSpace of the function
					//! \param owner	ownerclass of the method
					//! \param func		a pointer to the function 
					//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
					template<typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
					FunctionID registerFunctionByFullName(const String& fullName, const T_FUNCTIONOWNER_TYPE& owner, T_FUNC func)
					{
						Signature signature(mAllocator);
						SignatureCreatorHelper::createMemberSignature<T_FUNCTIONOWNER_TYPE>(func,signature);
						return internalRegister(fullName, ArgMethodCaller<T_FUNCTIONOWNER_TYPE,T_FUNC>::callFunc,ArgMethodCaller<T_FUNCTIONOWNER_TYPE,T_FUNC>::callFuncWR,(void*)&owner,sizeof(&owner),&func,sizeof(func),signature);
					}

					//! \brief register external member function with up to 10 arguments with compiletime/runtime check
					//! 
					//! the signature of the function will be detected automatically, and the correct overload of the function will be registered
					//! 
					//! \param name		 access name of the function
					//! \param nameSpace nameSpace of the function
					//! \param owner	ownerclass of the method
					//! \param func		a pointer to the function 
					//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
					template<typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
					FunctionID registerFunction(const String& name, const String& nameSpace, const T_FUNCTIONOWNER_TYPE& owner, T_FUNC func)
					{
						Signature signature(mAllocator);
						SignatureCreatorHelper::createMemberSignature<T_FUNCTIONOWNER_TYPE>(func,signature);
						return internalRegister(name,nameSpace, ArgMethodCaller<T_FUNCTIONOWNER_TYPE,T_FUNC>::callFunc,ArgMethodCaller<T_FUNCTIONOWNER_TYPE,T_FUNC>::callFuncWR,(void*)&owner,sizeof(&owner),&func,sizeof(func),signature);
					}


					//! \brief get the name of a function
					//! \param functionID		the id of the function
					//! \returns the name of the function; if the function ID is not valid an empty string is returned
					XAIT_COMMON_DLLAPI String getFunctionName(FunctionID functionID) const;

					//! \brief get the name of a function
					//! \param functionID		the id of the function
					//! \returns the nameSpace of the function; if the function ID is not valid an empty string is returned
					XAIT_COMMON_DLLAPI String getFunctionNameSpace(FunctionID functionID) const;

					//! \brief get the full name of a function (including namespace)
					//! \param functionID		the id of the function
					//! \returns the name of the function; if the function ID is not valid an empty string is returned
					XAIT_COMMON_DLLAPI String getFunctionFullName(FunctionID functionID) const;

					//! \brief tests the existance of a function
					//! \param name		the name of the function
					//! \param dummy	is a dummy function is sufficiant (e.g. during loading)
					//! \returns true if function exists, false otherwise
					XAIT_COMMON_DLLAPI bool existsFunction(const String& name, const Signature& signature,const bool dummy);

					//! \brief tests if a function is registered
					//! \param functionID		the id of the function
					//! \returns true if function is registered, false otherwise
					XAIT_COMMON_DLLAPI bool isRegistered(const FunctionID functionID) const;


					//! \brief creates a dummy function
					//! \param name			the name of the function
					//! \param nameSpace    the namespace of the function
					//! \param signature	the signature of the function
					//! \returns the functionID of the dummy function
					//! \remark it is required to call the checkDummyFunctions at the end of the inialization step
					//! \remark a corresponding function or functiondummy exists, the ID of the existing function will be returned
					XAIT_COMMON_DLLAPI FunctionID addRequiredFunction(const String& name, const String& nameSpace, const Signature& signature);

					//! \brief gets the required functions
					//! \returns the required functions
					XAIT_COMMON_DLLAPI const Container::Vector<FunctionID>& getRequiredFunctions() const;

					//! \brief adds a required function to the registrar
					//! \param functionID		the id of the required function
					//! \remark call checkRegisteredFunctions to check if all required functiosn are registered 					
					XAIT_COMMON_DLLAPI void addRequiredFunction(FunctionID functionID);

					//! \brief check if all dummy functions are bound
					//! \return true if all dummies are bound, false otherwise
					//! \remarks an asseration is created if dummy is not bound
					XAIT_COMMON_DLLAPI bool checkRegisteredFunctions();

					//! \brief call a function with id and argumentlist
					//! \param Variable* pointer to a value who should store the returnvalue
					//! \param id		id of the function
					//! \param argList  argumentlist of the function
					XAIT_COMMON_DLLAPI void callFunction(void* returnValue,FunctionID id,void* owner,const FunctionArgumentList& argList);

					//! \brief call a function with id and argumentlist
					//! \param id		id of the function
					//! \param argList  argumentlist of the function
					XAIT_COMMON_DLLAPI void callFunction(FunctionID id,void* owner,const FunctionArgumentList& argList);

					//! \brief get an id of a function 
					//! \param name		name of the function
					//! \returns the id of the function, MAX_UINT32 if function not registered
					XAIT_COMMON_DLLAPI FunctionID getFunctionIDByArgumentSignature(const String& name, const Container::Vector<Datatype::DatatypeID>& signature);

					//! \brief get an id of a function 
					//! \param name		name of the function
					//! \returns pointer to vector of functionIDs registered with the given name, NULL pointer if name unknown
					XAIT_COMMON_DLLAPI const Container::Vector<FunctionID>* getFunctionIDs( const String& name ) const;

					//! \brief get an id of a function 
					//! \param name		fullname of the function (for performance reasons)
					//! \returns the id of the function, MAX_UINT32 if function not registered
					XAIT_COMMON_DLLAPI FunctionID getFunctionID(const String& name, const String& nameSpace, const Signature& signature) const;

					//! \brief get an id of a function 
					//! \param name		fullname of the function (for performance reasons)
					//! \returns the id of the function, MAX_UINT32 if function not registered
					XAIT_COMMON_DLLAPI FunctionID getFunctionID(const String& fullname, const Signature& signature) const;

					//! \brief gets the type of the return value of a function
					//! \param functionID		the id of the function
					//! \return the datatypeID of the returnvalue
					XAIT_COMMON_DLLAPI Datatype::DatatypeID getReturnValueType(const FunctionID functionID) const;

					//! \brief gets the argument signature of a function
					//! \param signature		the signature of the function (returnvalue)
					//! \param functionID		the id of the function
					//! \remark an invalid functionid will cause an assertion and an empty signature
					XAIT_COMMON_DLLAPI void getArgumentSignature(Container::Vector<Datatype::DatatypeID>& signature, FunctionID functionID) const;

					//! \brief gets the full signature of a function
					//! \param signature		the signature of the function (returnvalue)
					//! \param functionID		the id of the function
					//! \remark an invalid functionid will cause an assertion and an empty signature
					XAIT_COMMON_DLLAPI void getFullSignature(Signature& signature, FunctionID functionID) const;

					//! \brief gets the used function declarations
					//! \returns the used function declarations
					XAIT_COMMON_DLLAPI const FunctionDeclarationManager::Ptr& getFunctionDeclarations() const;

					//! \brief adds unbound function call handler
					void addUnboundFunctionCallHandler(const UnboundFunctionCallDelegate& unboundFunctionCallHandler)
					{
						mUnboundFunctionCallHandler = unboundFunctionCallHandler;
					}

				protected:

					XAIT_COMMON_DLLAPI FunctionID internalRegister(const String& name, const String& nameSpace, CallerFunction callerFunction, CallerFunction callerFunctionWR, void* owner, uint32 ownerSize, 
																   void* function, uint32 functionSize, const Signature& signature, bool variadicOverride = false);

					XAIT_COMMON_DLLAPI FunctionID internalRegister(const String& fullName, CallerFunction callerFunction, CallerFunction callerFunctionWR, void* owner, uint32 ownerSize, 
						void* function, uint32 functionSize, const Signature& signature, bool variadicOverride = false);

					void bindFunctionToDeclaration( void* function,uint32 functionSize, FunctionID functionID, void* owner, CallerFunction callerFunction, CallerFunction callerFunctionWR, uint32 ownerSize );

					FunctionDeclarationManager::Ptr											mFunctionDeclarations;
					Container::Vector<CFunction*>											mCFunctions;
					Container::Vector<FunctionID>											mRequiredFunctions;
					const Memory::AllocatorPtr&												mAllocator;

					UnboundFunctionCallDelegate												mUnboundFunctionCallHandler;

					mutable Threading::SpinLock														mMutex;

				};
			}
		}
	}
}
