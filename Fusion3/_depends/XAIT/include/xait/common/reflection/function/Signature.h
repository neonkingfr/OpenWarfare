// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/Vector.h>
#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/String.h>
#include <xait/common/memory/Allocator.h>

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{
				//! \brief this class represents the signature of a function used by the functionregistration
				struct Signature
				{
					//! \brief constructor
					//! \param allocator	the used memory allocator
					Signature(const Memory::AllocatorPtr& allocator)
						: mArgumentSignature(allocator)
						, mVariadic(false)
					{}
					
					//! \brief copy constructor
					//! \param other	the original signature
					Signature(const Signature& other)
						: mArgumentSignature(other.mArgumentSignature)
						, mReturnType(other.mReturnType)
						, mVariadic(other.mVariadic)
					{}

					//! \brief assignment operator
					//! \param other	the original signature
					const Signature& operator=(const Signature& other)
					{
						mArgumentSignature = other.mArgumentSignature;
						mReturnType = other.mReturnType;
						mVariadic = other.mVariadic;

						return *this;
					}


					//! \brief equal operator
					//! \param other	the compared signature
					//! \return true if equal, false otherwise
					XAIT_COMMON_DLLAPI bool operator==(const Signature& other) const;

					//! \brief builds a string that contains the functionname 
					//! \param functionName		the used function name
					//! \returns the generated string
					XAIT_COMMON_DLLAPI String buildFunctionString(const String& functionName) const;

					Container::Vector<Datatype::DatatypeID>		mArgumentSignature;
					Datatype::DatatypeID						mReturnType;
					bool										mVariadic;
				};
			}
		}
	}
}
