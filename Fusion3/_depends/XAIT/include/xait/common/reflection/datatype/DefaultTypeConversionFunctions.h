// (C) xaitment GmbH 2006-2012

#pragma once



#define XAIT_DATATYPE_CONVERTER_START(sourceType,destinationType) \
template<> \
class TypeConverter<sourceType,destinationType> \
	{ \
	public: \
		static bool exists() \
		{ \
			return true; \
		} \
		static destinationType convert(const sourceType& value) \
		{ 

#define XAIT_DATATYPE_CONVERTER_END() \
		} \
	};


namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Datatype
			{
				// base class of typeconverter
				template<typename T_SOURCE_TYPE,typename T_DESTINATION_TYPE>
				class TypeConverter
				{
				public:
					static bool exists() 
					{
						return false;
					}
				};
	
				// integer32
				XAIT_DATATYPE_CONVERTER_START(int32,float32)
					return (float32) value;
				XAIT_DATATYPE_CONVERTER_END();

				XAIT_DATATYPE_CONVERTER_START(int32,String)
					return String(value);
				XAIT_DATATYPE_CONVERTER_END();

				XAIT_DATATYPE_CONVERTER_START(int32,int64)
					return int64(value);
				XAIT_DATATYPE_CONVERTER_END();

				// integer64
				XAIT_DATATYPE_CONVERTER_START(int64,float32)
					return (float32) value;
				XAIT_DATATYPE_CONVERTER_END();

				XAIT_DATATYPE_CONVERTER_START(int64,String)
					return String(value);
				XAIT_DATATYPE_CONVERTER_END();

				XAIT_DATATYPE_CONVERTER_START(int64,int32)
					return int32(value);
				XAIT_DATATYPE_CONVERTER_END();


				// float
				XAIT_DATATYPE_CONVERTER_START(float32,int32)
					return (int32) value;
				XAIT_DATATYPE_CONVERTER_END();

				XAIT_DATATYPE_CONVERTER_START(float32,int64)
					return (int64) value;
				XAIT_DATATYPE_CONVERTER_END();

				XAIT_DATATYPE_CONVERTER_START(float32,String)
					return String(value);
				XAIT_DATATYPE_CONVERTER_END();

				// string
				XAIT_DATATYPE_CONVERTER_START(String,int32)
					return value.convertToInt32();
				XAIT_DATATYPE_CONVERTER_END();

				XAIT_DATATYPE_CONVERTER_START(String,int64)
					return value.convertToInt64();
				XAIT_DATATYPE_CONVERTER_END();

				XAIT_DATATYPE_CONVERTER_START(String,float32)
					return value.convertToFloat32();
				XAIT_DATATYPE_CONVERTER_END();

 				XAIT_DATATYPE_CONVERTER_START(String,bool)
 					return value.convertToBool();
 				XAIT_DATATYPE_CONVERTER_END();
				
	

				// bool
				XAIT_DATATYPE_CONVERTER_START(bool,String)
					return String(value);
				XAIT_DATATYPE_CONVERTER_END();

				// Vec2f
				XAIT_DATATYPE_CONVERTER_START(Math::Vec2f,String)
					return String("(") + value.mX + String(",") + value.mY + String(")");
				XAIT_DATATYPE_CONVERTER_END();

				// Vec3f
				XAIT_DATATYPE_CONVERTER_START(Math::Vec3f,String)
					return String("(") + value.mX + String(",") + value.mY + String(",") + value.mZ + String(")");
				XAIT_DATATYPE_CONVERTER_END();

			}
		}
	}
}
