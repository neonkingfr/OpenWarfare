// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/exceptions/Exception.h>
#include <xait/common/container/Vector.h>
#include <xait/common/String.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/reflection/function/FunctionArgumentList.h>


namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Datatype
			{
				XAIT_COMMON_DLLAPI bool* stringToBool(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI int08* stringToInt08(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI int16* stringToInt16(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI int32* stringToInt32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI int64* stringToInt64(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI uint08* stringToUInt08(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI uint16* stringToUInt16(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI uint32* stringToUInt32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI uint64* stringToUInt64(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI float32* stringToFloat32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI float64* stringToFloat64(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Math::Vec2f* stringToVec2f(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Math::Vec2d* stringToVec2d(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Math::Vec2i32* stringToVec2i32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Math::Vec2u32* stringToVec2u32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Math::Vec3f* stringToVec3f(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Math::Vec3d* stringToVec3d(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Math::Vec3i32* stringToVec3i32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Math::Vec3u32* stringToVec3u32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI String* stringToString(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI WString* stringToWString(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<bool>* stringToVectorBool(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<int08>* stringToVectorInt08(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<int16>* stringToVectorInt16(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<int32>* stringToVectorInt32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<int64>* stringToVectorInt64(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<uint08>* stringToVectorUInt08(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<uint16>* stringToVectorUInt16(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<uint32>* stringToVectorUInt32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<uint64>* stringToVectorUInt64(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<float32>* stringToVectorFloat32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<float64>* stringToVectorFloat64(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<Math::Vec2f>* stringToVectorVec2f(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<Math::Vec2d>* stringToVectorVec2d(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<Math::Vec2i32>* stringToVectorVec2i32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<Math::Vec2u32>* stringToVectorVec2u32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<Math::Vec3f>* stringToVectorVec3f(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<Math::Vec3d>* stringToVectorVec3d(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<Math::Vec3i32>* stringToVectorVec3i32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<Math::Vec3u32>* stringToVectorVec3u32(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Container::Vector<String>* stringToVectorString(const String& string, const Memory::AllocatorPtr& allocator);
				XAIT_COMMON_DLLAPI Reflection::Function::FunctionArgumentList* stringToFuncArgList(const String& string, const Memory::AllocatorPtr& allocator);
			}
		}
	}
}
