// (C) xaitment GmbH 2006-2012

#pragma once


#include <xait/common/reflection/datatype/DatatypeBase.h>
#include <xait/common/reflection/function/FunctionRegistrar.h>
#include <xait/common/reflection/function/FunctionTypedefs.h>
#include <xait/common/reflection/datatype/DefaultDatatypeSerializer.h>

#define XAIT_DATATYPE_ALIGNMENT 4

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Datatype
			{

				//! \brief this class allows to representent every datatype with 
				template<typename T_DATATYPE>
				class Datatype : public TypedDatatype<T_DATATYPE>
				{
				public:
					typedef T_DATATYPE* (*ParseFunction)(const String&, const Memory::AllocatorPtr&);
					typedef void (*StreamReadValueFunction) (Serialize::SerializeStorage*, T_DATATYPE&);
					typedef void (*StreamWriteValueFunction) (Serialize::SerializeStorage*, const T_DATATYPE&);

					//! \brief creates the datatype
					//! \param name				name of the datatype
					//! \param id				the datatypeID
					//! \param parseFunction	the parse function (converts string to an object of the type)
					//! \param allocator		memory allocator		
					static Datatype<T_DATATYPE>* createFundamentalDatatype(const String& name,
						DatatypeID id, 
						const T_DATATYPE& defaultValue, 
						ParseFunction parseFunction, 
						const Memory::AllocatorPtr& allocator)
					{
						if (TypedDatatype<T_DATATYPE>::mInstance)
						{
							X_ASSERT_MSG_DBG(!TypedDatatype<T_DATATYPE>::exists(), "DataType already exists with name: " << ((Datatype<T_DATATYPE>*)TypedDatatype<T_DATATYPE>::mInstance)->mName.getConstCharPtr());
						}


						Datatype<T_DATATYPE>* instance = new(allocator) Datatype<T_DATATYPE>(defaultValue);

						TypedDatatype<T_DATATYPE>::mInstance = instance;

						instance->mName = name;
						instance->mDatatypeID = id;
						instance->mParseFunction = parseFunction;

						return instance;
					}

					//! \brief creates a reference to an existing the datatype (used with dlls)
					//! \param parseFunction	the parse function (converts string to an object of the type)
					static Datatype<T_DATATYPE>* createReference(Datatype<T_DATATYPE>* instance,
																ParseFunction parseFunction)
					{
						if (TypedDatatype<T_DATATYPE>::mInstance)
						{
							X_ASSERT_MSG_DBG(!TypedDatatype<T_DATATYPE>::exists(), "DataType already exists with name: " << ((Datatype<T_DATATYPE>*)TypedDatatype<T_DATATYPE>::mInstance)->mName.getConstCharPtr());
						}


						TypedDatatype<T_DATATYPE>::mInstance = instance;
						instance->mReferences.pushBack(&(TypedDatatype<T_DATATYPE>::mInstance));

						if (parseFunction)
							instance->mParseFunction = parseFunction;

						return instance;
					}

					//! \brief getter DatatypeID
					//! \returns the datatypeID
					virtual inline DatatypeID getDatatypeID()
					{
						return mDatatypeID;
					}

					//! \brief getter DatatypeID
					//! \returns the datatypeID
					virtual DatatypeID getDatatypeID() const
					{
						return mDatatypeID;
					}

					//! \brief getter Name
					//! \returns the name of the datatype
					virtual inline String getName()
					{
						return mName;
					}

					//! \brief getter size
					//! \returns the size of the datatype
					virtual inline uint32 getSize()
					{
						return mSize;
					}

					//! \see DatatypeBase
					virtual inline void setDefaultValue(void* location)
					{
						new(location) T_DATATYPE(mDefaultValue);
					}

					//! \see DatatypeBase
					virtual inline void initializeValue(void* memory)
					{
						initializeValue(&mDefaultValue,memory);
					}

					//! \see DatatypeBase
					virtual inline void initializeValue(void* source, void* destination)
					{
						new(destination) T_DATATYPE(*((T_DATATYPE*)source));
					}

					//! \see DatatypeBase
					virtual inline void setValue(void* source, void* destination)
					{
						*((T_DATATYPE*)destination) = *((T_DATATYPE*)source);
					}

					//! \see DatatypeBase
					virtual inline void callDestructor(void* location)
					{
						((T_DATATYPE*)location)->~T_DATATYPE();
					}

					//! \see DatatypeBase
					virtual void deleteObject(void* location)
					{
						callDestructor(location);
						Datatype<T_DATATYPE>::mAllocator.get()->free(location);
					}

					//! \see DatatypeBase
					virtual inline uint08* parseValue(const String& valueString, const Memory::AllocatorPtr& allocator)
					{
						X_ASSERT_MSG(mParseFunction, "no parse function defined for type: " << mName.getConstCharPtr());
						return (uint08*)(*mParseFunction)(valueString,allocator);
					}

					//! \see DatatypeBase
					virtual bool isFundamentalDatatype()
					{
						return true;
					}

					//! \see DatatypeBase
					virtual void* createHeapObject()
					{
						return createHeapObject(Datatype<T_DATATYPE>::mAllocator);
					}

					//! \see DatatypeBase
					virtual void* createHeapObject(const void* value)
					{
						return createHeapObject(value,Datatype<T_DATATYPE>::mAllocator);
					}

					//! \brief destroys a heap object
					//! \param object	the object which should be destroyed
					//! \remark	should always be used because later versions eventually provide object pooling
					virtual void destroyHeapObject(void* object)
					{
						destroyHeapObject(object,Datatype<T_DATATYPE>::mAllocator);
					}

					//! \brief creates an instance of the datatype on the heap
					//! \param allocator	the used allocator
					//! \returns a pointer to the heap object
					virtual void* createHeapObject(const Common::Memory::AllocatorPtr& allocator)
					{
						return XAIT_NEW(allocator,T_DATATYPE(mDefaultValue));
					}

					//! \brief creates an instance of the datatype on the heap like a copy constructor
					//! \param value		pointer to another instance of this type
					//! \param allocator	the used allocator
					//! \returns a pointer to the heap object
					virtual void* createHeapObject(const void* value, const Common::Memory::AllocatorPtr& allocator)
					{
						return XAIT_NEW(allocator,T_DATATYPE(*((T_DATATYPE*)value)));
					}

					//! \brief destroys an heap instance of the datatype
					//! \param object		pointer to the object
					//! \param allocator	the used allocator
					virtual void destroyHeapObject(void* object, const Common::Memory::AllocatorPtr& allocator)
					{
						callDestructor(object);
						XAIT_DELETE(object,allocator);
					}

					//! \brief register external function with up to 10 arguments 
					//! \param name		access name of the function
					//! \param func		a pointer to the function 
					template <typename OwnerType,typename T_FUNC>
					Function::FunctionID registerMemberFunction(const String& name, T_FUNC func)
					{
						return mFunctionRegistrar.registerFunctionByFullName<OwnerType,T_FUNC>(name,func);
					}

					//! \brief register external function with up to 10 arguments
					//! \param name		access name of the function
					//! \param func		a pointer to the function 
					//template <typename T_FUNC>
					//Function::FunctionID registerMemberFunction(const String& fullName, T_FUNC func)
					//{
					//	if (fullName == mName)
					//	{
					//		XAIT_FAIL("only member functions can be used as init functions");
					//		return Function::FunctionID();
					//	}
					//	mFunctionRegistrar.registerFunctionByFullName(name,name, "",func);
					//}

					//! \see DatatypeBase
					virtual Function::FunctionID getFunctionIDByArgumentSignature(const String& name,const Container::Vector<DatatypeID>& signature) 
					{
						return mFunctionRegistrar.getFunctionIDByArgumentSignature(name,signature);
					}

					//! \brief returns the signature of the argumentlist (without returnValue type)
					//! \param signature	the signature of the arugmentlist 
					//! \param functionID	the id of the function
					virtual void getArgumentSignature(Container::Vector<Reflection::Datatype::DatatypeID>& signature,const Function::FunctionID functionID)
					{
						mFunctionRegistrar.getArgumentSignature(signature,functionID);
					}



					virtual DatatypeID getReturnValueType(const Function::FunctionID functionID)
					{
						return mFunctionRegistrar.getReturnValueType(functionID);
					}

					//! \brief call a function with id and argumentlist
					//! \param Variable* pointer to a value who should store the returnvalue
					//! \param id		id of the function
					//! \param owner	the owner of the function
					//! \param argList  argumentlist of the function
					virtual void callFunction(void* returnValue,Function::FunctionID id,void* owner,const Function::FunctionArgumentList& argList)
					{
						mFunctionRegistrar.callFunction(returnValue,id,owner,argList);
					}

					//! \brief call a function with id and argumentlist
					//! \param id		id of the function
					//! \param owner	the owner of the function
					//! \param argList  argumentlist of the function
					virtual void callFunction(Function::FunctionID id,void* owner,const Function::FunctionArgumentList& argList)
					{
						mFunctionRegistrar.callFunction(id,owner,argList);
					}

					//! \brief serialize the data to a stream
					//! \param value		pointer to the value
					//! \param stream			the storage stream
					virtual void serializeWrite(void* value,Serialize::SerializeStorage* stream)
					{
						(*mStreamWriteFunction)(stream,*((T_DATATYPE*)value));
					}

					//! \brief inits the object from a serialized stream
					//! \param value			pointer to the value (has to be a valid object)
					//! \param stream			the storage stream
					virtual void serializeRead(void* value,Serialize::SerializeStorage* stream)
					{
						return (*mStreamReadFunction)(stream,*((T_DATATYPE*)value));
					}

				protected:
					//! \brief constructor
					//! \param defaultValue
					Datatype(const T_DATATYPE& defaultValue)
						: mDefaultValue(defaultValue)
#if (XAIT_ARCH == XAIT_ARCH_INTEL_X86 || XAIT_ARCH == XAIT_ARCH_INTEL_X64)
						, mSize(sizeof(T_DATATYPE))
#else
						, mSize(sizeof(T_DATATYPE) + ((XAIT_DATATYPE_ALIGNMENT - (sizeof(T_DATATYPE) % XAIT_DATATYPE_ALIGNMENT) % XAIT_DATATYPE_ALIGNMENT)))
#endif 
						, mParseFunction(NULL)
						, mFunctionRegistrar(Datatype<T_DATATYPE>::mAllocator)					  
						, mStreamReadFunction(&DatatypeSerializer<T_DATATYPE>::readValue)
						, mStreamWriteFunction(&DatatypeSerializer<T_DATATYPE>::writeValue) 
					{}


					String							mName;
					T_DATATYPE						mDefaultValue;

					uint32							mSize;
					DatatypeID						mDatatypeID;
					ParseFunction					mParseFunction;
					Function::FunctionRegistrar		mFunctionRegistrar;
					StreamReadValueFunction			mStreamReadFunction;
					StreamWriteValueFunction		mStreamWriteFunction;
				};

				//! \brief	gets an datatype by template argument
				//! \returns a pointer to the datatype; NULL if datatype does not exist
				template<typename T_DATATYPE>
				Datatype<T_DATATYPE>* getDatatype()
				{
					X_ASSERT_MSG_DBG(TypedDatatype<T_DATATYPE>::exists(),"Datatype is not created");
					Datatype<T_DATATYPE>* instance = (Datatype<T_DATATYPE>*)TypedDatatype<T_DATATYPE>::getDatatype();

					return instance;
				}


				//! \brief class that allows to represent all datatypes 
				template<typename T_DATATYPE>
				class NFDatatype : public Datatype<T_DATATYPE>
				{
				public:
					typedef T_DATATYPE (T_DATATYPE::*BasicOperation) (const T_DATATYPE&) const;
					typedef bool	   (T_DATATYPE::*ComparisionOperation) (const T_DATATYPE&) const;

					//! \brief test on integral datatype
					//! \returns true if the dataype is integral, false otherwise
					virtual bool isFundamentalDatatype()
					{
						return false;
					}

					//! \brief constructor
					//! \param name					name of the datatype
					//! \param id					datatypeID
					//! \param defaultValue			the default value of the datatype (used to initialize value)
					//! \param parseFunction		a function that convert a string to an instance of the datatype on the heap
					//! \param additionMethod		pointer to addition method
					//! \param substractionMethod	pointer to substraction method
					//! \param multiplicationMethod	pointer to multiplication method
					//! \param divisionMethod		pointer to division method
					//! \param equalMethod			pointer to equalMethod method
					//! \param unequalMethod		pointer to unequalMethod method
					//! \param greaterMethod		pointer to greaterMethod method
					//! \param greaterEqualMethod	pointer to greaterEqualMethod method
					//! \param smallerMethod		pointer to smallerMethod method
					//! \param smallerEqualMethod	pointer to smallerEqualMethod method
					//! \param allocator			an allocatorPtr
					static NFDatatype<T_DATATYPE>* createDatatype(const String& name, DatatypeID id, const T_DATATYPE& defaultValue, typename Datatype<T_DATATYPE>::ParseFunction parseFunction, 
						BasicOperation additionMethod, BasicOperation substractionMethod, 
						BasicOperation multiplicationMethod, BasicOperation divisionMethod,
						ComparisionOperation equalMethod,ComparisionOperation unequalMethod,
						ComparisionOperation greaterMethod, ComparisionOperation greaterEqualMethod,
						ComparisionOperation smallerMethod, ComparisionOperation smallerEqualMethod,
						const Memory::AllocatorPtr& allocator)
					{
						if (NFDatatype<T_DATATYPE>::mInstance)
						{
							X_ASSERT_MSG_DBG(!NFDatatype<T_DATATYPE>::exists(), "DataType already exists with name: " << ((NFDatatype<T_DATATYPE>*)NFDatatype<T_DATATYPE>::mInstance)->mName.getConstCharPtr());
						}
						else
						{
							NFDatatype* instance = new(allocator) NFDatatype<T_DATATYPE>(defaultValue);
							NFDatatype<T_DATATYPE>::mInstance = instance;
							instance->mName = name;
							instance->mDatatypeID = id;
							instance->mParseFunction = parseFunction;
							instance->mAddition = additionMethod;
							instance->mSubstraction = substractionMethod;
							instance->mMultiplication = multiplicationMethod;
							instance->mDivision = divisionMethod;
							instance->mEqual = equalMethod;
							instance->mUnequal = unequalMethod;
							instance->mGreater = greaterMethod;
							instance->mGreaterEqual = greaterEqualMethod;
							instance->mSmaller = smallerMethod;
							instance->mSmallerEqual = smallerEqualMethod;

							return instance;
						}
						return NULL;
					}

					//! \brief constructor
					//! \param parseFunction		a function that convert a string to an instance of the datatype on the heap
					//! \param additionMethod		pointer to addition method
					//! \param substractionMethod	pointer to substraction method
					//! \param multiplicationMethod	pointer to multiplication method
					//! \param divisionMethod		pointer to division method
					//! \param equalMethod			pointer to equalMethod method
					//! \param unequalMethod		pointer to unequalMethod method
					//! \param greaterMethod		pointer to greaterMethod method
					//! \param greaterEqualMethod	pointer to greaterEqualMethod method
					//! \param smallerMethod		pointer to smallerMethod method
					//! \param smallerEqualMethod	pointer to smallerEqualMethod method
					//! \param allocator			an allocatorPtr
					static NFDatatype<T_DATATYPE>* createReference(NFDatatype<T_DATATYPE>* instance, typename Datatype<T_DATATYPE>::ParseFunction parseFunction, 
						BasicOperation additionMethod, BasicOperation substractionMethod, 
						BasicOperation multiplicationMethod, BasicOperation divisionMethod,
						ComparisionOperation equalMethod,ComparisionOperation unequalMethod,
						ComparisionOperation greaterMethod, ComparisionOperation greaterEqualMethod,
						ComparisionOperation smallerMethod, ComparisionOperation smallerEqualMethod)
					{
						if (NFDatatype<T_DATATYPE>::mInstance)
						{
							X_ASSERT_MSG_DBG(!NFDatatype<T_DATATYPE>::exists(), "DataType already exists with name: " << ((NFDatatype<T_DATATYPE>*)NFDatatype<T_DATATYPE>::mInstance)->mName.getConstCharPtr());
						}
						else
						{
							NFDatatype<T_DATATYPE>::mInstance = instance;
							instance->mReferences.pushBack(&(TypedDatatype<T_DATATYPE>::mInstance));

							if (parseFunction)
								instance->mParseFunction = parseFunction;
							if (additionMethod)
								instance->mAddition = additionMethod;
							if (substractionMethod)
								instance->mSubstraction = substractionMethod;
							if (multiplicationMethod)
								instance->mMultiplication = multiplicationMethod;
							if (divisionMethod)
								instance->mDivision = divisionMethod;
							if (equalMethod)
								instance->mEqual = equalMethod;
							if (unequalMethod)
								instance->mUnequal = unequalMethod;
							if (greaterMethod)
								instance->mGreater = greaterMethod;
							if (greaterEqualMethod)
								instance->mGreaterEqual = greaterEqualMethod;
							if (smallerMethod)
								instance->mSmaller = smallerMethod;
							if (smallerEqualMethod)
								instance->mSmallerEqual = smallerEqualMethod;

							return instance;
						}
						return NULL;
					}

					//! \see DatatypeBase
					virtual bool hasAddition()
					{
						return mAddition != NULL;
					}
					//! \see DatatypeBase
					virtual bool hasSubstraction()
					{
						return mSubstraction != NULL;
					}
					//! \see DatatypeBase
					virtual bool hasMultiplication()
					{
						return mMultiplication != NULL;
					}
					//! \see DatatypeBase
					virtual bool hasDivision()
					{
						return mDivision != NULL;
					}
					//! \see DatatypeBase
					virtual bool hasEqual()
					{
						return mEqual != NULL;
					}
					//! \see DatatypeBase
					virtual bool hasUnequal()
					{
						return mUnequal != NULL;
					}

					//! \see DatatypeBase
					virtual bool hasGreater()
					{
						return mGreater != NULL;
					}
					//! \see DatatypeBase
					virtual bool hasGreaterEqual()
					{
						return mGreaterEqual != NULL;
					}
					//! \see DatatypeBase
					virtual bool hasSmaller()
					{
						return mSmaller != NULL;
					}
					//! \see DatatypeBase
					virtual bool hasSmallerEqual()
					{
						return mSmallerEqual != NULL;
					}


					//! \see DatatypeBase
					virtual void callAddition(void* returnValue, void* owner, void* rightHandSideValue)
					{
						*((T_DATATYPE*)returnValue) = ((*(T_DATATYPE*)owner).*mAddition) (*((T_DATATYPE*)rightHandSideValue));
					}
					//! \see DatatypeBase
					virtual void callSubstraction(void* returnValue, void* owner, void* rightHandSideValue)
					{
						*((T_DATATYPE*)returnValue) = ((*(T_DATATYPE*)owner).*mSubstraction) (*((T_DATATYPE*)rightHandSideValue));
					}
					//! \see DatatypeBase
					virtual void callMultiplication(void* returnValue, void* owner, void* rightHandSideValue)
					{
						*((T_DATATYPE*)returnValue) = ((*(T_DATATYPE*)owner).*mMultiplication) (*((T_DATATYPE*)rightHandSideValue));
					}
					//! \see DatatypeBase
					virtual void callDivision(void* returnValue, void* owner, void* rightHandSideValue)
					{
						*((T_DATATYPE*)returnValue) = ((*(T_DATATYPE*)owner).*mDivision) (*((T_DATATYPE*)rightHandSideValue));
					}
					//! \see DatatypeBase
					virtual void callEqual(void* returnValue, void* owner, void* rightHandSideValue)
					{
						*((bool*)returnValue) = ((*(T_DATATYPE*)owner).*mEqual) (*((T_DATATYPE*)rightHandSideValue));
					}
					//! \see DatatypeBase
					virtual void callUnequal(void* returnValue, void* owner, void* rightHandSideValue)
					{
						*((bool*)returnValue) = ((*(T_DATATYPE*)owner).*mUnequal) (*((T_DATATYPE*)rightHandSideValue));
					}
					//! \see DatatypeBase
					virtual void callGreater(void* returnValue, void* owner, void* rightHandSideValue)
					{
						*((bool*)returnValue) = ((*(T_DATATYPE*)owner).*mGreater) (*((T_DATATYPE*)rightHandSideValue));
					}
					//! \see DatatypeBase
					virtual void callGreaterEqual(void* returnValue, void* owner, void* rightHandSideValue)
					{
						*((bool*)returnValue) = ((*(T_DATATYPE*)owner).*mGreaterEqual) (*((T_DATATYPE*)rightHandSideValue));
					}
					//! \see DatatypeBase
					virtual void callSmaller(void* returnValue, void* owner, void* rightHandSideValue)
					{
						*((bool*)returnValue) = ((*(T_DATATYPE*)owner).*mSmaller) (*((T_DATATYPE*)rightHandSideValue));
					}
					//! \see DatatypeBase
					virtual void callSmallerEqual(void* returnValue, void* owner, void* rightHandSideValue)
					{
						*((bool*)returnValue) = ((*(T_DATATYPE*)owner).*mSmallerEqual) (*((T_DATATYPE*)rightHandSideValue));
					}







				private:
					//! \brief copy constructor`
					NFDatatype(const T_DATATYPE& defaultValue)
						: Datatype<T_DATATYPE>(defaultValue)
						, mAddition(NULL)
						 ,mSubstraction(NULL)
						, mMultiplication(NULL)
						, mDivision(NULL)
					{}
				
					BasicOperation					mAddition;
					BasicOperation					mSubstraction;
					BasicOperation					mMultiplication;
					BasicOperation					mDivision;

					ComparisionOperation			mEqual;
					ComparisionOperation			mUnequal;
					ComparisionOperation			mGreater;
					ComparisionOperation			mGreaterEqual;
					ComparisionOperation			mSmaller;
					ComparisionOperation			mSmallerEqual;
				};

			}
		}		
	}
}
