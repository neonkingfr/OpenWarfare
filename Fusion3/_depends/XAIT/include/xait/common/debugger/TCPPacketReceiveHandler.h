// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/sockets/TCPSocket.h>
#include <xait/common/network/InputHandler.h>
#include <xait/common/network/Reactor.h>
#include <xait/common/debugger/Server.h>



namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class TCPConnectionLayer;
		}
	}
}


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Handles incoming TCP packets.
			//!
			//! Checks periodically whether there are incoming TCP packets. If there are incoming packets,
			//! it tries to put them together to a message.
			//! Has to be registered with the reactor.
			//! \see TCPConnectionLayer
			//! \see Reactor
			class TCPPacketReceiveHandler : public Network::InputHandler
			{
			public:
				//! \brief Constructor for receiving data via a TCP socket.
				//! \param socket	the socket to receive from
				//! \param connectionLayer	the connection layer is needed to know whether a connection was closed by the client, and to get the destination to send the message to
				//! \param connectionID		the identifier of the client
				TCPPacketReceiveHandler(Network::Sockets::TCPSocket* socket, TCPConnectionLayer* connectionLayer, const uint32 connectionID);

				//! \brief Destructor.
				virtual ~TCPPacketReceiveHandler();

				//! \brief	Initializes the the packet receive handler.
				//! \returns true, if the initialization was successful, and false otherwise
				//! \see XAIT::Common::Network::Sockets::SocketError::getErrorText()
				bool init();

				//! \brief Update function to assign computation time to the handler.
				bool onTick();

			private:
				//! \brief	Reads the length of the message from the stream.
				//! \returns true, when no error occurred, and false otherwise
				bool readLength();

				//! \brief Reads in the data until the predetermined number of bytes of the message were received completely.
				//! \returns true, when no error occurred, and false otherwise
				bool readData();

				//! \brief Checks whether the message was received completely.
				//! \returns true, if the message was received completely, and false otherwise
				bool isDone();

				//! \brief Gets the TCP socket that belongs to this message handler.
				//! \returns the TCPSocket
				Network::Sockets::TCPSocket* getTCPSocket() const;

				//! \brief Resets the variables and renews the stream.
				void resetHandler();

				//! \brief Removes the connection from the server and the reactor.
				void removeConnection();


				uint32		mBytesCurr;			//!< number of bytes already sent of the current message
				uint32		mByteMax;			//!< the total number of bytes of the current message
				uint32		mByteLengthMax;		//!< the number of bytes of the message length variable

				TCPConnectionLayer*		mConnectionLayer;	//!< the connection layer
				uint32		mConnectionID;					//!< the ID of the connection
			};

		}
	}
}
