// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/network/message/MessageHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_IDENTITY_CLIENT.
			//!
			//! Used when the client informs the server about his parameters including
			//! the version of the network protocol he uses.
			//! The message handler has to be registered with the server.
			//! \see Server
			class IdentityReplyMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_IDENTITY_PARAMETER	"clientInfo: { string:ComputerName; string:UserName, uint32:protocolVersion }"


			public:
				//! \brief Constructor.
				//! \param server	the server allows setting the client information
				IdentityReplyMsgHandler( Server* server );

				//! \brief Handles the message by reading the data through the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client from which the message is
				//! \return true, if the message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				//! \brief Sends the reply to the given connection ID.
				//! \param connectionID the connection ID
				//! \return true, if sending the reply was successful, and false otherwise
				bool reportReply( const uint32 connectionID );


				Server*		mServer; //!< the server
				String		mClientComputerName; //!< the computer name of the client
				String		mClientUserName; //!< the user name of the client
				uint32		mClientProtocolVersion; //!< the protocol version of the client
			};

		}
	}
}
