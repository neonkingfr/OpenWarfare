// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/network/message/MessageHandler.h>
#include <xait/common/debugger/ObjectType.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_CONTINUE.
			//!
			//! Releases the thread that has entered the breakpoint and activated the holding function.
			//! No breakflags are changed.
			//! The message handler has to be registered with the server.
			//! \see Server
			class ContinueMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_CONTINUE_PARAMETER	"continueMsgHandler: { uint32:moduleID; uint32:breakpointTypeID; }"

			public:
				//! \brief Constructor.
				//! \param server	the server allowing access to the registered modules
				ContinueMsgHandler( Server* server );

				//! \brief Handles the message by reading the data through the MessageReader
				//! \param msgReader		the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Gets the message parameters from the message reader on the network stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);


				Server*		mServer;		//!< the server

				uint32	mModuleID;			//!< the module ID of the owning module
				uint32	mBreakpointTypeID;	//!< the corresponding breakpoint type ID
			};

		}
	}
}
