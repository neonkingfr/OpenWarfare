// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/network/message/MessageHandler.h>
#include <xait/common/debugger/ObjectType.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_SET_BREAKPOINT_CONDITION.
			//!
			//! Called when the client requests to set a break condition for a BreakpointInstance.
			//! Has to be registered with the server.
			//! \see Server
			class SetBreakpointConditionMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_SETBREAKPOINT_CONDITION_PARAMETER	"setBreakpointConditionMessage: { uint32:moduleID; uint32:breakpointInstanceID; uint08[]:conditionStream }"

			public:
				//! \brief Constructor
				//! \param server	the server allows access to the registered modules
				SetBreakpointConditionMsgHandler( Server* server );

				//! \brief Destructor
				virtual ~SetBreakpointConditionMsgHandler();

				//! \brief handles the message by reading the data through the MessageReader
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if message was handled correctly, else false
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				void reinitVariables();

				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				bool reportReply( const uint32 connectionID );


				Server*		mServer;

				uint32 mModuleID;
				uint32 mBreakpointInstanceID;
				IO::Stream* mConditionStream;
			};

		}
	}
}
