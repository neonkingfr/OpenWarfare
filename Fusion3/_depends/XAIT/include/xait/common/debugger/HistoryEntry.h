// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/message/MessageBuilder.h>
#include <xait/common/container/List.h>
#include <xait/common/container/FixedVector.h>
#include <xait/common/io/Stream.h>
#include <xait/common/platform/Time.h>
#include <xait/common/memory/MemoryStream.h>


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class ObjectType;
			class ObjectInstance;
			class BreakpointInstance;
			class VariableInstance;
		}
	}
}


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			enum HistoryEntryType
			{
				HISTORYENTRYTYPE_STANDARD,
				HISTORYENTRYTYPE_RELEASESTATIC
			};

			//! \brief Describes a single entry of the history.
			//!
			//! Contains the content of all stored and watched variables within the object module hierarchy.
			class HistoryEntry : public Memory::MemoryManaged
			{
			public:
				//! \brief Constructor.
				//! \param gameTime the game time stamp of the entry
				//! \param systemTime the system time stamp of the entry
				//! \param breakpointInstance the breakpoint instance in which we are in
				//! \param objectInstance the objectInstance to which this history entry belongs to
				//! \param staticEntry states whether this is a static entry
				//HistoryEntry(const Platform::MilliSeconds& gameTime, const Platform::MilliSeconds& systemTime, const BreakpointInstance* breakpointInstance, const ObjectInstance* objectInstance, bool staticEntry = false);

				//! \brief constructor
				//! \param trackVariables	flag to define if variables should be tracked
				HistoryEntry(const bool trackVariables);

				//! \brief Destructor.
				~HistoryEntry();

				//! \brief initializes the HistoryEntry.
				//! \param gameTime the game time stamp of the entry
				//! \param systemTime the system time stamp of the entry
				//! \param breakpointInstance the breakpoint instance in which we are in
				//! \param objectInstance the objectInstance to which this history entry belongs to
				//! \param staticEntry states whether this is a static entry
				void init(const Platform::MilliSeconds& gameTime, const Platform::MilliSeconds& systemTime, const BreakpointInstance* breakpointInstance, const ObjectInstance* objectInstance, bool staticEntry = false);

				//! \brief Writes the history entry to a message stream.
				//! \param msgBuilder the message builder that is used
				//! \return true, if writing was successful, and false otherwise
				bool getHistoryData(Network::Message::MessageBuilder& msgBuilder);

				//! \brief gets whether this entry is a static one
				//! \remark static entries are not deleted when the length of the history reaches the maximum length
				//! \returns a bool flag that states whether this is a static entry
				bool isStaticEntry() const;

				//! \brief removes the static flag of this history entry
				void removeStaticFlag();

				//! \brief gets the type of this history entry
				//! \returns the type of the history entry
				const HistoryEntryType& getEntryType() const;

				//! \brief returns whether the type of this history entry is equal another type
				//! \returns true if the type of this history entry is equal another type
				bool isEntryType(const HistoryEntryType& other) const;

				void setVariableTracking(const bool newVal);


			private:
				//! \brief Copy constructor.
				//! \param other the HistoryEntry to copy
				HistoryEntry(const HistoryEntry* other);

				Memory::MemoryStream*	mHistoryStream;

				bool mIsStreamWritten;
				uint32 mGameTime;
				uint32 mSystemTime;
				uint32 mBreakpointInstID;

				bool	mTrackVariables;

			protected:
				bool  mStaticEntry;		//!< states whether this is an static history entry
				HistoryEntryType mType; //!< this is the type of the HistoryEntry
			};

			//! \brief represents a special history entry
			//! 
			//! \this kind of history entry is used to release another static history entry
			class ReleaseStaticHistoryEntry : public HistoryEntry
			{

			public:

				//! \brief Constructor.
				//! \param releaseEntry		states the link to the corresponding static history entry
				//! \param trackVariables	flag to define whether the variables should be tracked
				ReleaseStaticHistoryEntry(HistoryEntry* releaseEntry, const bool trackVariables);

				//! \brief gets the corresponding release entry
				//! \returns the corresponding release entry
				HistoryEntry* getReleaseEntry() const;

			protected :

				HistoryEntry* mReleaseHistoryEntry; //!< a link to the static release entry whose static flag should be removed
			};

			

			//! \brief Describes the debugger history.
			class HistoryList : public Memory::MemoryManaged
			{
			public:
				//! \brief Constructor.
				//! \param maxLength	the maximal Length each ObjectInstance may have for its HistoryEntryList
				//! \param multipleLength	the maximal multiple of maxLength of HistoryEntryList that is reserved for unused HistoryEntries
				HistoryList(const uint32 maxLength, const uint32 multipleLength = 10);

				//! \brief Destructor.
				~HistoryList();

				//! \brief Adds a new HistoryEntry of the given ObjectInstanceID.
				//! \param objectInstanceID		the ID of the ObjectInstance
				//! \returns the HistoryEntry that needs to be filled with the BreakpointInformation
				//! \remark If the amount of history entries is larger than maxLength then the last history entries will be deleted.
				HistoryEntry* add(const uint32 objectInstanceID);

				//! \brief Adds a new HistoryEntry of the given ObjectInstanceID.
				//! \param objectInstanceID		the ID of the ObjectInstance
				//! \param historyEntry			the HistoryEntry that needs to be filled with the BreakpointInformation
				//! \remark If the amount of history entries is larger than maxLength then the last history entries will be deleted.
				void add(const uint32 objectInstanceID, HistoryEntry* historyEntry);

				//! \brief removes the list of history entries for given ObjectInstanceID
				//! \param	objectInstanceID		the ID of the ObjectInstance
				void removeList(const uint32 objectInstanceID);

				//! \brief Gets the last HistoryEntry of the given ObjectInstance.
				//! \param historyEntry		the pointer to set to the HistoryEntry
				//! \param objectInstanceID	the ID of the ObjectInstance
				bool get(HistoryEntry*& historyEntry, const uint32 objectInstanceID);

				//! \brief Gets all HistoryEntries of the given ObjectInstance.
				//! \param historyList		the list of HistoryEntries
				//! \param objectInstanceID	the ID of the ObjectInstance
				bool getAll(Container::Vector<HistoryEntry*>*& historyList, const uint32 objectInstanceID);

				//! \brief Sets the maximal Length of an history
				//! \param maxHistoryLength	the new maximal length of the history
				void setMaximalHistoryLength(uint32 maxHistoryLength);

				//! \brief Defines whether the library tracks the variables during run time for debugger
				//! \param trackVariables	flag to define whether the variables should be tracked
				void setVariableTracking(bool trackVariables);

				//! \brief Returns whether the library tracks the variables during run time for debugger
				//! \returns flag that defines whether the variables should be tracked
				bool getVariableTracking() const { return mTrackVariables; }
				
			private:
				//! \brief Reduces the amount of stored unused HistoryEntries
				void reduceUnusedHistoryEntries();

				//! \brief remembers the HistoryEntry instead of deleting it
				//! \remark if it isn't a HISTORYENTRYTYPE_STANDARD it is deleted
				void rememberUnusedHistoryEntry(HistoryEntry* entry);

				uint32		mMaxLength; //!< the maximum length of the history
				uint32		mMaxMultipleLength;	// the amount of entities that can be created and store their max length of historyEntries without allocating for new Entries
				bool		mTrackVariables;
				Container::LookUp<uint32, Container::Vector<HistoryEntry*> >		mHistoryEntryLists; //!< the list of history entries
				Container::Vector<HistoryEntry*>		mUnusedHistoryEntryList;
			};
		}
	}
}

