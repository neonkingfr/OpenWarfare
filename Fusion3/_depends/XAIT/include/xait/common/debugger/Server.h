// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/platform/Time.h>
#include <xait/common/network/message/Dispatcher.h>
#include <xait/common/network/message/MessageHandler.h>
#include <xait/common/network/message/MessageReader.h>
#include <xait/common/network/message/MessageBuilder.h>
#include <xait/common/threading/Mutex.h>
#include <xait/common/threading/Thread.h>
#include <xait/common/threading/Threaded.h>
#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/function/FunctionRegistrar.h>
#include <xait/common/reflection/function/CallBackFunctionRegistration.h>
#include <xait/common/container/IDVector.h>
#include <xait/common/debugger/MutexGate.h>
#include <xait/common/debugger/ClientInfo.h>
#include <xait/common/debugger/RPCHelper.h>
#include <xait/common/container/Pair.h>
#include <xait/common/debugger/ConnectionLayer.h>

#if XAIT_OS == XAIT_OS_WII

#define DEBUGGER_SERVER_ADDRESS		NULL

#elif XAIT_OS == XAIT_OS_WIN	// same as editors, allowing only local connections per default so that there are no firewall mockings

#include <xait/common/network/sockets/HostAddress.h>
#define DEBUGGER_SERVER_ADDRESS		XAIT::Common::Network::Sockets::HostAddress(Network::Sockets::IPV4(127,0,0,1),6300)

#else // other platforms possibly remote connection needed

#include <xait/common/network/sockets/HostAddress.h>
#define DEBUGGER_SERVER_ADDRESS		XAIT::Common::Network::Sockets::HostAddress(Network::Sockets::IPV4::ANY,6300)

#endif

#define DEBUGGER_SERVER_PROTOCOL_VERSION	(uint32)4

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class Module;
			class ObjectInstance;
			class BreakpointInstance;

			class NetworkStream;

			class GetHistoryMsgHandler;
			class IdentityReplyMsgHandler;
			class ModuleConnectMsgHandler;
			class ModuleDisconnectMsgHandler;
			class NetworkStreamAnswerMsgHandler;
			class NetworkStreamDeregisterMsgHandler;
			class NetworkStreamRegisterMsgHandler;
			class RPCMsgHandler;
			class SetBreakpointClientMsgHandler;
			class SetBreakpointConditionMsgHandler;
			class SetBreakpointGlobalMsgHandler;
			class SetBreakpointMsgHandler;
			class SetBreakpointObjectInstanceMsgHandler;
			class TCPPacketReceiveHandler;
			class SetBreakpointMsgHandler;
			class SetVariableMsgHandler;
			class SetWatchesMsgHandler;
			class MakeStepMsgHandler;
			class ContinueMsgHandler;
			class RPCHelper;
			class ObjectTypeAckMsgHandler;
			class ObjectInstanceRequestMsgHandler;
			class ObjectInstanceReleaseMsgHandler;
		}
	}
}

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			enum MessageTypeID// : uint32
			{
				MESSAGE_BREAKPOINT_HOLDING						= 0x01000000,	// 16777216
				//MESSAGE_DISCONNECT_CLIENT						= 0x01000001,
				MESSAGE_DISCONNECT_FORCED_OTHER_CLIENT			= 0x01000002,
				MESSAGE_MODULE_CONNECT							= 0x01000003,
				MESSAGE_MODULE_CONNECT_REPLY					= 0x01000004,
				MESSAGE_MODULE_DISCONNECT						= 0x01000005,
				MESSAGE_MODULE_DISCONNECT_REPLY					= 0x01000006,

				MESSAGE_OBJECTGROUP_REGISTER					= 0x01000007,	// 16777223
				MESSAGE_OBJECTTYPE_REGISTER						= 0x01000008,
				MESSAGE_OBJECTTYPE_ACK							= 0x01000030,	// add for "late bound fix"
				MESSAGE_OBJECTTYPE_UNREGISTER					= 0x01000009,
				MESSAGE_OBJECTINSTANCE_REGISTER					= 0x0100000A,
				MESSAGE_OBJECTINSTANCE_UNREGISTER				= 0x0100000B,
				MESSAGE_OBJECTINSTANCE_REQUEST					= 0x01000031,
				MESSAGE_OBJECTINSTANCE_RELEASE					= 0x01000032,
				MESSAGE_OBJECTINSTANCE_UPDATE					= 0x01000033,	// 16777267

				MESSAGE_HISTORY_GET								= 0x0100000C,
				MESSAGE_HISTORY_REPLY							= 0x0100000D,
				MESSAGE_IDENTITY_CLIENT							= 0x0100000E,
				MESSAGE_IDENTITY_CLIENT_REPLY					= 0x0100000F,
				MESSAGE_VARIABLE_DATA_GET						= 0x01000010,
				MESSAGE_VARIABLE_DATA_GET_REPLY					= 0x01000011,
				MESSAGE_VARIABLE_DATA_SET						= 0x01000012,
				MESSAGE_VARIABLE_DATA_SET_REPLY					= 0x01000013,

				MESSAGE_BREAK									= 0x01000014,
				//MESSAGE_SET_BREAKFLAG_CLIENT_REPLY				= 0x01000015,
				MESSAGE_SET_BREAKFLAG_OBJECT_INSTANCE			= 0x01000016,
				MESSAGE_SET_BREAKFLAG_OBJECT_INSTANCE_REPLY		= 0x01000017,
				//MESSAGE_SET_BREAKFLAG_BREAKPOINT_TYPE			= 0x01000018,
				//MESSAGE_SET_BREAKFLAG_BREAKPOINT_TYPE_REPLY		= 0x01000019,
				MESSAGE_SET_BREAKFLAG_BREAKPOINT_INSTANCE		= 0x0100001A,
				MESSAGE_SET_BREAKFLAG_BREAKPOINT_INSTANCE_REPLY	= 0x0100001B,
				MESSAGE_SET_BREAKPOINT_CONDITION				= 0x0100001C,
				MESSAGE_SET_BREAKPOINT_CONDITION_REPLY			= 0x0100001D,
				MESSAGE_MAKE_STEP								= 0x0100001E,
				MESSAGE_CONTINUE								= 0x0100001F,

				MESSAGE_SET_WATCH								= 0x01000020,	// 16777248
				MESSAGE_SET_WATCH_REPLY							= 0x01000021,

				MESSAGE_SHUTDOWN_WAS_MODULE						= 0x01000022,
				//MESSAGE_SHUTDOWN_WAS_SERVER						= 0x01000023,

				MESSAGE_RPC_CALL								= 0x01000024,
				MESSAGE_RPC_REPLY								= 0x01000025,

				MESSAGE_NETWORKSTREAM_REGISTRATION				= 0x01000026,
				MESSAGE_NETWORKSTREAM_REGISTRATION_REPLY		= 0x01000027,
				MESSAGE_NETWORKSTREAM_DATA_REQUEST				= 0x01000028,
				MESSAGE_NETWORKSTREAM_DATA_REPLY				= 0x01000029,
				MESSAGE_NETWORKSTREAM_DEREGISTRATION			= 0x0100002A,
				MESSAGE_NETWORKSTREAM_DEREGISTRATION_REPLY		= 0x0100002B,
			};

			//! \brief Describes a filter that performs a step.
			struct MakeStepFilter
			{
				//! \brief Default constructor.
				MakeStepFilter();

				//! \brief Constructor.
				//! \param triggeredClient	the client who triggered this filter
				//! \param module			the module
				//! \param objectInstanceID	the ID of the object instance
				//! \param breakpointTypeID	the ID of the breakpoint type
				MakeStepFilter( ClientInfo* triggeredClient, Module* module, const uint32 objectInstanceID, const uint32 breakpointTypeID)
					: mTriggeredClient(triggeredClient), mModule(module), mObjectInstanceID(objectInstanceID), mBreakpointTypeID(breakpointTypeID)
				{};

				//! \brief This function allows to set all parameters of the filter.
				//! \param triggeredClient	the client who triggered this filter
				//! \param module			the module
				//! \param objectInstanceID	the ID of the object instance
				//! \param breakpointTypeID	the ID of the breakpoint type
				void set( ClientInfo* triggeredClient, Module* module, const uint32 objectInstanceID, const uint32 breakpointTypeID)
				{
					mTriggeredClient = triggeredClient;
					mModule = module;
					mObjectInstanceID = objectInstanceID;
					mBreakpointTypeID = breakpointTypeID;
				}

				//! \brief Clears all parameters of the filter.
				void clear();

				ClientInfo*		mTriggeredClient;	//!< the client who triggered this filter
				Module*	mModule;					//!< the module
				uint32	mObjectInstanceID;			//!< the ID of the object instance
				uint32	mBreakpointTypeID;			//!< the ID of the breakpoint type
			};

			//! \brief The server handles all network traffic and relays it to the appropriate handlers.
			class Server : public Threading::Threaded
			{
			public:
				//! \brief Creates an instance of the server
				//! \param allocator a memory allocator
				//! \param waitTimeMilliSeconds the interval at which the socket is checked for new messages
				XAIT_COMMON_DLLAPI static void createInstance( const Memory::Allocator::Ptr& allocator, Platform::MilliSeconds waitTimeMilliSeconds = Platform::MilliSeconds(100) );

				//! \brief Destroys the server instance.
				XAIT_COMMON_DLLAPI static void destroyInstance();

				//! \brief Gets the server instance.
				//! \return the server instance
				XAIT_COMMON_DLLAPI static Server* getInstance();

				//! \brief Starts the server in an additional thread.
				//!
				//! Use exitAsync to clean up.
				//! \param address the address used
				//! \return true, when starting the server was successfull, and false otherwise
				XAIT_COMMON_DLLAPI static bool runAsync( CONNECTION_LAYER_ADDRESS address = DEBUGGER_SERVER_ADDRESS );

				//! \brief Stops the server.
				//!
				//! Use this method only when the server was started with runAsync
				//! \return true, when stopping the server was successfull, and false otherwise
				XAIT_COMMON_DLLAPI static bool exitAsync();

				//! \brief Returns the waiting time between two main loops.
				//! \returns the waiting time
				inline Platform::MilliSeconds getWaitTime() const { return mWaitTimeMilliSeconds; }

				//! \brief Registers a new MessageHandler to the dispatcher within the server.
				//! \param messageHandler	the MessageHandler that should be registered
				//! \returns true, if the MessageHandler could be registered correctly, and false otherwise
				XAIT_COMMON_DLLAPI bool registerMessageHandler(Network::Message::MessageHandler* messageHandler);

				//! \brief Finds the specified NetworkStream.
				//! \param networkStreamID	the ID of the NetworkStream
				//! \param networkStream	pointer to store the NetworkStream
				//! \returns true if the NetworkStream was found, and false otherwise
				XAIT_COMMON_DLLAPI bool getNetworkStream(NetworkStream*& networkStream, const uint32 networkStreamID);

				//! \brief Registers a new module to the server.
				//! \param name		the name of the module
				//! \param version	the version of the module
				//! \param maxHistoryLength	the maximum length of the history
				//! \returns the created module if name and version wasn't registered, and NULL otherwise
				XAIT_COMMON_DLLAPI Module* createModule(const String& name, const uint32& version, const uint32 maxHistoryLength=50);

				//! \brief Unregisters a module from the server
				//!
				//! The pointer is set to NULL if unregistration was successful.
				//! \param module	the module to unregister
				//! \returns true if the module was registered, and false otherwise
				XAIT_COMMON_DLLAPI bool destroyModule(Module*& module);

				//! \brief Finds the module that matches the given ID.
				//! \param module			stores the found module
				//! \param moduleID			the ID of the searched module
				//! \returns true, if the module was found, and false otherwise
				XAIT_COMMON_DLLAPI bool getModule(Module*& module, const uint32& moduleID);

				//! \brief Finds the module that matches the given name and version.
				//! \param module			stores the found module
				//! \param moduleName		the name of the searched module
				//! \param moduleVersion	the version of the searched module
				//! \returns true, if the module was found, and false otherwise
				XAIT_COMMON_DLLAPI bool getModule(Module*& module, const String& moduleName, const uint32& moduleVersion);

				//! \brief Checks whether a breakpoint is holding in some registered module.
				//! \returns true, if a breakpoint is holding in some registered module, and false otherwise
				XAIT_COMMON_DLLAPI bool isBreakpointHolding() const;

				//! \brief Registers an external function with up to 10 arguments with compiletime/runtime check.
				//! \param name		access name of the function
				//! \param owner	owning object of the method
				//! \param func		a pointer to the function 
				//! \return the function ID
				template<typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
				Reflection::Function::FunctionID registerRPCServer(const String& name, const T_FUNCTIONOWNER_TYPE& owner, T_FUNC func)
				{
					mMutex.lock();
					Reflection::Function::FunctionID fID = mFunctionRegistrar.registerFunction(name,owner,func);
					mMutex.unlock();
					return fID;
				}

				//! \brief Sets the game time.
				//!
				//! The calling game thread is holding if a breakpoint was holding: see enterGlobalBreak
				//! \param	newTime		the new game time
				//! \see enterGlobalBreak
				XAIT_COMMON_DLLAPI void setGameTime(const Platform::MilliSeconds& newTime);

				//! \brief Gets the game time.
				//! \return the game time
				XAIT_COMMON_DLLAPI const Platform::MilliSeconds& getGameTime() const;

				//! \brief Gets the system time.
				//! \returns the system time
				XAIT_COMMON_DLLAPI static const Platform::MilliSeconds getSystemTime();


			private:
				friend class Module;
				friend class ObjectInstance;
				friend class BreakpointInstance;

				friend class NetworkStream;

				friend class GetHistoryMsgHandler;
				friend class GetVariableMsgHandler;
				friend class IdentityReplyMsgHandler;
				friend class ModuleConnectMsgHandler;
				friend class ModuleDisconnectMsgHandler;
				friend class NetworkStreamAnswerMsgHandler;
				friend class NetworkStreamDeregisterMsgHandler;
				friend class NetworkStreamRegisterMsgHandler;
				friend class RPCMsgHandler;
				friend class SetBreakpointClientMsgHandler;
				friend class SetBreakpointConditionMsgHandler;
				friend class SetBreakpointGlobalMsgHandler;
				friend class SetBreakpointMsgHandler;
				friend class SetBreakpointObjectInstanceMsgHandler;
				friend class SetVariableMsgHandler;
				friend class SetWatchesMsgHandler;
				friend class TCPPacketReceiveHandler;
				friend class MakeStepMsgHandler;
				friend class ContinueMsgHandler;
				friend class RPCHelper;
				friend class ConnectionLayer;
				friend class ObjectTypeAckMsgHandler;
				friend class ObjectInstanceRequestMsgHandler;
				friend class ObjectInstanceReleaseMsgHandler;


				//! \brief Constructor.
				//! \param waitingTime	the time to wait between two main loop cycles
				Server( Platform::MilliSeconds waitTimeMilliSeconds );

				//! \brief Destructor.
				//!
				//! If Server is used by other Thread call exit first
				~Server();

				//! \brief Will be executed when the thread is started.
				//!
				//! This method runs inside the thread
				//! \return The error code of the execution
				int32 main();

				//! \brief Closes the server after the current loop.
				void exit();

				//! \brief Cleans up the garbage.
				void clean();

				//! \brief Adds a new client.
				//! \param connectionID		the identifier for the client
				//! \returns true, if setting up the client information was successfull, and false otherwise
				bool connectClient( const uint32 connectionID );

				//! \brief Disconnects the client from the server and informs the reactor about the disconnection.
				//!
				//! Note that all modules are disconnect from this client as well.
				//! \param connectionID		the identifier for the client
				//! \returns true, if the client was known and he could be disconnected correctly, and false otherwise
				bool disconnectClient( const uint32 connectionID );

				//! \brief Deletes the clientInfo from the server.
				//! \param connectionID		the identifier representing the client
				//! \remark assertion will be caused if ClientInfo isn't known
				void removeClient( const uint32 connectionID );

				//! \brief Sets the client information.
				//! \param connectionID		the connection identifier of the client
				//! \param clName			the name of the client PC
				//! \param clUserName		the name of the user
				//! \param protocolVersion	the version of the network protocol used by the client
				//! \returns true, if the client info could be set, and false otherwise
				bool setClientInfo( const uint32 connectionID, const String& clName, const String& clUserName, const uint32 protocolVersion );

				//! \brief Sets the break flag of a client to the given value.
				//! \param clInfo the client information
				//! \param newValue the new value
				void setBreakflagClient( ClientInfo* clInfo, const bool newValue );

				//! \brief Connects a module to a client.
				//! \param connectionID		the connection ID
				//! \param moduleName		the Name of the module
				//! \param moduleVersion	the version of the module
				//! \returns true, if the module and the client were known and could be connected successfully, and false otherwise
				bool connectModule(const uint32 connectionID, const String& moduleName, const uint32& moduleVersion, const bool forcedConnect);

				//! \brief Disconnects a module from a client
				//! \param connectionID		the connection identifier
				//! \param moduleID			the ID of the module
				//! \returns true, if the module was connected to the client and could be disconnected successfully, and false otherwise
				bool disconnectModule( const uint32 connectionID, const uint32& moduleID );

				//! \brief Gets the client info to a given connection ID.
				//! \param clInfo stores a pointer to the client info
				//! \param connectionID the connection ID
				//! \return true, if the client info was found, and false otherwise
				bool getClientInfo( ClientInfo*& clInfo, const uint32 connectionID );

				//! \brief Checks whether the given connection is known to the server.
				//! \param connectionID the connection ID
				//! \return true, if the connection is known, and false otherwise
				bool isConnectionKnown( const uint32 connectionID ) const;

				//! \brief Called when a breakpoint holds.
				//! \param triggerSource	the Module containing the breakpoint
				//! \param breakInstance	the breakpoint that inflicted this call
				//! \param triggerObjInst	the ObjectInstance that entered the Breakpoint
				//! \return true, when the entering the breakpoint was successful, and false otherwise
				bool enterBreakpoint( Module* triggerSource, BreakpointInstance* breakInstance, ObjectInstance* triggerObjInst);

				//! \brief Stops the calling thread if a breakpoint is holding.
				//!
				//! The external threads aren't released within BreakAreas.
				void enterGlobalBreak();

				//! \brief Registers the default handler for messages and streams needed within the debugger.
				//! \return true, when registering the default handlers was successfull, and false otherwise
				bool registerDefaultHandler();

				//! \brief Registers the default remote procedure calls that are supported by the server itself.
				//! \return true, when registering the default RPCs was successfull, and false otherwise
				bool registerDefaultRPC();

				//! \brief Gets the function ID of a given RPC name, and gets the own FunctionRegistrar.
				//! \param rpcID			the functionID of the RPC
				//! \param funcRegistrar	the own FunctionRegistrar
				//! \param nameRPC			the name of the searched function
				//! \returns true if the function is known, and false otherwise
				bool getRPCServer(Reflection::Function::FunctionID& rpcID, Reflection::Function::FunctionRegistrar*& funcRegistrar, const String& nameRPC);

				//! \brief Returns the version of the network protocol used by the server.
				//! \return the network protocol version
				uint32 getProtocolVersion() const { return DEBUGGER_SERVER_PROTOCOL_VERSION; };

				//! \brief Registers a new NetworkStreamID to the NetworkStreamMsgHandler in the dispatcher.
				//! \param networkStreamID	the ID of the NetworkStream that should be registered
				//! \param name				the name of the NetworkStream that should be registered
				//! \param length			the length of the NetworkStream that should be registered
				//! \param connectionID		the identifier for the client trying to register
				//! \return true, if the NetworkStream could be registered correctly, and false otherwise
				bool registerNetworkStream(const uint32 networkStreamID, const String& name, const uint32 length, const uint32 connectionID);

				//! \brief Deregisters a NetworkStreamID from the NetworkStreamMsgHandler in the dispatcher.
				//! \param networkStreamID	the ID of the NetworkStream that should be deregistered
				//! \return true, if the NetworkStream could be deregistered correctly, and false otherwise
				bool deregisterNetworkStream(const uint32 networkStreamID);

				//! \brief Updates the data of a NetworkStream.
				//! \param networkStreamID	the ID of the NetworkStream that should be registered
				//! \param dataStream		the data to update within the NetworkStream
				//! \return true, if the NetworkStream data could be updated correctly, and false otherwise
				bool updateNetworkStreamData(const uint32 networkStreamID, IO::Stream* dataStream);

				//! \brief Sends a message stream to the given connection.
				//! \param connectionID	the identifier of the client the message should be send to
				//! \param message		the data to send
				//! \returns true, if the connection exists and a OutputHandler was found, and false otherwise
				bool sendMessage( const uint32 connectionID, IO::Stream* message);

				//! \brief Sends a shutdown message to the given connection.
				//! \param connectionID		the identifier of the client the message should be send to
				//! \param module	the module that is going to be shut down
				//! \returns true, if the message could be given correctly to the OutputHandler, and false otherwise
				bool sendMessageModuleShutDown( const uint32 connectionID , Module* module);

				//! \brief Sends a message to the given connection saying that the module is going to be forced to connect to another client.
				//! \param connectionID		the identifier of the client the message should be send to
				//! \param module	the module that is going to be disconnected and reconnected to other
				//! \param forcingClient client forcing an own connect
				//! \returns true, if the message could be given correctly to the OutputHandler, and false otherwise
				bool sendMessageModuleDisconnectForcedOther( const uint32 connectionID , Module* module, const ClientInfo* forcingClient);

				//! \brief Sends a message to the given connection saying that the module was connected successfully.
				//! \param connectionID		the identifier of the client the message should be send to
				//! \param moduleName		the name of the module
				//! \param moduleVersion	the version of the module
				//! \param module			the pointer to the module
				//! \param resultCode		the result
				//! \param errorText		containing error information if some error has occurred
				//! \returns true, if the message could be given correctly to the OutputHandler, and false otherwise
				bool sendMessageModuleConnectReply( const uint32 connectionID , const String& moduleName, const uint32& moduleVersion, Module* module, const uint08 resultCode, const String& errorText);

				//! \brief Further dispatches the received message.
				//! \param messageStream	the received data to dispatch
				//! \param connectionID		identifier enabling responses
				void dispatchMessage( IO::Stream* messageStream, const uint32 connectionID );

				//! \brief Releases the holding breakpoint of this client, OR deletes its makeStepRequest if it is set.
				//! \param clientInfo	the client sending the make continue
				//! \returns true, if the breakpoint was holding within a module of this client or the client had an active makeStepRequest, and false otherwise
				bool makeContinue( ClientInfo* clientInfo );

				//! \brief Allows the current holding thread to run again with restrictions.
				//! \param connectionID				the identifier of the client
				//! \param objInstFilter			flag indicating whether a breakpoint need the same objectInstance to pass
				//! \param breakpointTypeIDFilter	ID of the BreakType to use as a filter, -1 means no filter
				bool makeStep( const uint32 connectionID, const bool objInstFilter, const uint32 breakpointTypeIDFilter );

				//! \brief Enters the break area. BreakPoints should be handle without releasing the GlobalBreakpoint.
				//!
				//! Only one Thread (ObjectInstance) can enter a (nested) breakArea at a time.
				void enterBreakArea();

				//! \brief Leaves the break area. Releases the GlobalBreakpoint if a Breakpoint was holding within this BreakArea.
				//!
				//! Only one Thread (ObjectInstance) can enter a (nested) breakArea at a time.
				void leaveBreakArea();

				//! \brief Enters the BreakpointSync where only one thread can be at a time.
				void enterBreakpointSync();

				//! \brief Leaves the BreakpointSync where only one thread can be at a time.
				void leaveBreakpointSync();

				//! \brief Stops the calling thread if a breakpoint is holding.
				//! 
				//! Internal threads are released before external threads and are not tested for BreakAreas.
				void enterGlobalBreakInternal();

				//! \brief Checks whether the make step filter is active.
				//! \return true, if the make step filter is active, and false otherwise
				bool isMakeStepFilterActive() const;

				//! \brief Checks whether a makeStepFilter is set and whether the current parameters pass the filter.
				//! \param module				the module containing the BreakpointInstance
				//! \param breakpointTypeID		ID of the BreakpointType to which the current entered BreakpointInstance belongs to
				//! \param objInstID			ID of the ObjectInstance that entered the Breakpoint
				//! \returns true, if the filter was passed or no filter was set, and false otherwise
				bool canPassMakeStepFilter( Module* module, const uint32 breakpointTypeID, const uint32 objInstID ) const;

				//! \brief Adds a request from a client to call a remote procedure.
				//! \param msgReader	the MessageReader containing the data to read from
				//! \param connectionID	the identifier of the client
				void addRPCRequest(Network::Message::MessageReader* msgReader, const uint32 connectionID );


			//member variables
				CONNECTION_LAYER_ADDRESS	mAddress;				//!< the address to contact the server

				ConnectionLayer*				mConnectionLayer;	//!< responsible for all communication
				Network::Message::Dispatcher*	mDispatcher;		//!< dispatches streams to the registered MessageHandlers

				Platform::MilliSeconds	mWaitTimeMilliSeconds;		//!< time interval at which the main loop checks incoming network traffig
				Platform::MilliSeconds	mGameTime;					//!< the game time

				bool				mIsNeeded;						//!< flag indicating whether the server should shutdown
				Threading::Mutex	mMutex;							//!< mutex used to synchronize server functions


				Reflection::Function::FunctionRegistrar		mFunctionRegistrar;		//!< helper class for the RPC system

				Container::List<ClientInfo*>	mConnectedClients;					//!< list of client information
				Container::IDVector<Module*>	mModules;							//!< list of modules

				Container::LookUp< uint32, Container::Pair< uint32, NetworkStream*> >	mNetworkStreams;	//!< mapping from networkStreamID to pairs of client connectionIDs and NetworkStreams

			//singleton variables
				static Threading::Thread*		mServerThread;	//!< instance of the debugger server thread
				static Server*					mInstance;		//!< instance of the debugger server

			// variables for Breakpoints, BreakAreas and GlobalBreak
				MutexGate				mMutexBreakpointGate;		//!< locks the calling thread if a breakpoint holds
				Threading::SpinLock		mMutexEnterBreakpoint;		//!< used to allow only one external thread entering Breakpoints
				Threading::Mutex		mMutexBreakArea;			//!<
				Threading::Mutex		mMutexGlobal;				//!<
				bool	mIsGlobalBreakActive;						//!< flag indicating whether a global breakpoint is active
				bool	mIsBreakpointHolding;						//!< flag indicating whether a breakpoint is holding
				Module*		mModuleHolding;							//!< flag indicating whether a module is holding
				BreakpointInstance*		mBreakpointInstanceHolding;	//!< flag indicating whether a breakpoint instance is holding
				ObjectInstance*			mObjectInstanceHolding;		//!< flag indicating whether an object instance is holding

				MakeStepFilter	mMakeStepFilter;					//!< the make step filter

				RPCHelper*	mRPCHelper;								//!< an instance of the RPS helper class
				bool		mIsRPCRequestHandling;					//!< flag indicating whether the server currently handles an RPC request
			};
		}
	}
}
