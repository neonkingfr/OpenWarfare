// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/CommonLog.h>

#ifdef XAIT_DEBUGGER_LOG
#define XLOG_DEBUGGER(msg) XLOG_MSG(msg)
#else
#define XLOG_DEBUGGER(msg)
#endif
