// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/debugger/ObjectType.h>
#include <xait/common/network/message/MessageHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_SET_WATCH.
			//!
			//! When the client requests to either set or release a watch, this message handler is called.
			//! The NewFlag determines whether the watch should be set or released for the specified Variable within
			//! the ObjectInstance.
			//! Has to be registered with the server.
			//! \see Server
			class SetWatchesMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_SETWATCH_PARAMETER		"setWatchMessage: { uint32:moduleID; uint32:objInstanceID; uint32:variableID, bool:newFlag }"

			public:
				//! \brief Constructor.
				//! \param server	the server allows access to the modules
				SetWatchesMsgHandler( Server* server );

				//! \brief Handles the message by reading the data with the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				//! \brief Sends the reply to the given connection ID.
				//! \param connectionID the connection ID
				//! \return true, if sending the reply was successful, and false otherwise
				bool reportReply( const uint32 connectionID );


				Server*		mServer;			//!< the server

				uint32	mModuleID;				//!< the module ID
				uint32	mObjectInstanceID;		//!< the ID of the object instance
				uint32	mVariableID;			//!< the ID of the variable
				bool	mNewFlag;				//!< the flag indicating whether the watch should be set or released
			};

		}
	}
}
