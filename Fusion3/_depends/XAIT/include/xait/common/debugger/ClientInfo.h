// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>



namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Class holding information about connected clients.
			class ClientInfo
			{
			public:
				//! \brief Constructor.
				//! \param id the ID of the client
				ClientInfo( const uint32 id )
					: mBreakflagClient(false), mID(id)
				{}

				//! \brief Sets the client data.
				//! \param computerName the name of the client computer
				//! \param userName the name of the user
				void setNames(const String& computerName, const String& userName)
				{
					mComputerName = computerName;
					mUserName = userName;
				}

				//! \brief Destructor.
				virtual ~ClientInfo() {}

				//! \brief Gets the computer name.
				//! \return the name of the client computer
				const String& getComputerName() const { return mComputerName; }

				//! \brief Gets the user name.
				//! \return the name of the user
				const String& getUserName() const { return mUserName; }

				//! \brief Gets the ID.
				//! \return the ID
				const uint32& getID() const { return mID; }

				//! \brief Sets the break flag to the given value.
				//! \param val the new value
				void setBreakflag( const bool val ) { mBreakflagClient = val; }

				//! \brief Gets the break flag.
				bool getBreakflag() const { return mBreakflagClient; }

			private:
				String	mComputerName;		//!< the name of the clients' computer
				String	mUserName;			//!< the user name
				bool	mBreakflagClient;	//!< the break flag of the client
				uint32	mID;				//!< the ID
			};
		}
	}
}
