// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/message/MessageBuilder.h>
#include <xait/common/network/message/MessageReader.h>
#include <xait/common/container/List.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/debugger/BreakpointInstance.h>
#include <xait/common/debugger/HistoryEntry.h>
#include <xait/common/debugger/VariableInstance.h>
#include <xait/common/debugger/VariableType.h>
#include <xait/common/debugger/ObjectInstance.h>
#include <xait/common/debugger/ClientInfo.h>
#include <xait/common/threading/Semaphore.h>


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class ObjectGroup;
		}
	}
}

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
#define INVALID_OBJECT_TYPE_ID		(uint32)-1


			//! \brief An ObjectType defines the type of an object.
			//!
			//! The module class has the only access to the "changing" functions.
			//! In order to create a new ObjectType, you have to call the ObjectGroup::createObjectType
			//! method. You can use the ObjectType to create ObjectInstances by calling the createObjectInstance-method.
			//! \see ObjectGroup
			class ObjectType : public Memory::MemoryManaged
			{
			public:
				//! \brief Gets the name of this ObjectType.
				//! \return the name
				inline const String& getName() const {return mName;}

				//! \brief Gets the ID of this ObjectType
				//! \returns the ID of this ObjectType
				inline uint32 getID() const {return mID;}

				//! \brief Attaches the ObjectType to the debugger.
				XAIT_COMMON_DLLAPI void attach();

				//! \brief Checks whether this ObjectInstance is attached to the debugger.
				//! \return true, if this ObjectInstance is attached to the debugger, and false otherwise
				inline bool isAttached() const {return mIsAttached;}

				//! \brief Registers a new ObjectInstance to this ObjectType.
				//!
				//! The ObjectInstance need to be attached after all variables are registered
				//! \param name		the name of the ObjectInstance to add to this ObjectType
				//! \return the pointer to the created ObjectInstance
				XAIT_COMMON_DLLAPI ObjectInstance* createObjectInstance(const String& name);

				//! \brief Unregisters an ObjectInstance from this ObjectType.
				//! \param objID	the ID of the ObjectInstance to remove from this ObjectType
				//! \returns true, if the ObjectInstance was known and could be removed correctly, and false otherwise
				//! \remakr the ObjectInstance will stay as long as a client is connected
				XAIT_COMMON_DLLAPI bool destroyObjectInstance(const uint32 objID);

				//! \brief Registers a new breakpoint.
				//! \param typeName			the name of the corresponding BreakpointType
				//! \param instanceName		the name of the breakpoint instance
				//! \param storeVariables	the list of variables that are stored to history each time this BreakpointInstance is entered
				//! \returns pointer to the breakpoint if the type was known and name of breakpoint wasn't already used, and NULL otherwise
				XAIT_COMMON_DLLAPI BreakpointInstance* registerBreakpointInstance(const String& typeName, const String& instanceName, const Container::List<String>* storeVariables = NULL);

				//! \brief Gets the breakpoint instance with given name.
				//! \param breakpoint		the found breakpoint instance
				//! \param breakpointName	the name of the breakpoint instance
				//! \returns pointer to the breakpoint if type was known and name of breakpoint wasn't already used, NULL else
				XAIT_COMMON_DLLAPI bool getBreakpoint(BreakpointInstance*& breakpoint, const String& breakpointName) const;

				//! \brief Registers a new VariableType.
				//! \param varType		the real type of the variable type
				//! \param varName		the name of the variable type
				//! \param readOnly		the access right to this variable type (true: readOnly, false: readWrite)
				//! \return true, if registering was successful, and false otherwise
				XAIT_COMMON_DLLAPI bool registerVariableType(const Network::Message::StreamIDs varType, const String& varName, const bool readOnly);

				//! \brief Checks whether the breakpoint is activated.
				//! \return true, if it is activated, and false otherwise
				XAIT_COMMON_DLLAPI bool isBreakpointActivated() const;

				//! \brief Checks whether the breakpoint is holding.
				//! \return true, if it is holding, and false otherwise
				XAIT_COMMON_DLLAPI bool isBreakpointHolding() const;

				//! \brief Checks whether the variable with the given ID exists.
				//! \return true, if it exists, and false otherwise
				XAIT_COMMON_DLLAPI bool existsVariable(const uint32 varID) const;

				//! \brief Gets the matching variable ID to a given variable name
				//! \param id		the variable id
				//! \param name		the variable name
				//! \returns true if the variable name was known and the variable ID was found, and false otherwise
				XAIT_COMMON_DLLAPI bool getVarID(uint32& id, const String& name) const;

				//! \brief Sets the unique ID of the ObjectType.
				//! \param uniqueID0 part 0
				//! \param uniqueID1 part 1
				//! \param uniqueID2 part 2
				//! \param uniqueID3 part 3
				XAIT_COMMON_DLLAPI void setUniqueID(const uint32 uniqueID0, const uint32 uniqueID1, const uint32 uniqueID2, const uint32 uniqueID3);

				//! \brief Gets the unique ID of the ObjectType.
				//! \param uniqueID0 stores part 0
				//! \param uniqueID1 stores part 1
				//! \param uniqueID2 stores part 2
				//! \param uniqueID3 stores part 3
				XAIT_COMMON_DLLAPI void getUniqueID(uint32& uniqueID0, uint32& uniqueID1, uint32& uniqueID2, uint32& uniqueID3);
				
				//! \brief Finds the VariableType corresponding to the given variable name.
				//! \param varType		pointer to store the VariableType
				//! \param varTypeName	the name of the VariableType
				//! \return true, if the variable type was found, and false otherwise
				XAIT_COMMON_DLLAPI bool getVariableType(VariableType*& varType, const String& varTypeName) const;
				
				//! \brief Gets the ObjectTypes ObjectGroup.
				//! \return the ObjectGroup
				ObjectGroup* getGroup() {return mObjectGroup;}

			private:
				friend class Module;
				friend class ObjectGroup;
				friend class ObjectInstance;
				friend class BreakpointInstance;
				friend class HistoryEntry;

				friend class GetVariableMsgHandler;
				friend class DebuggerVariableResolver;
				friend class SetBreakpointMsgHandler;
				friend class SetBreakpointConditionMsgHandler;
				friend class SetVariableMsgHandler;
				friend class SetWatchesMsgHandler;
				friend class ObjectInstanceRequestMsgHandler;
				friend class ObjectTypeAckMsgHandler;

				//! \brief Constructor.
				//! \param name			the name of this object type
				//! \param objGroup		the ObjectGroup to which this ObjectType belongs to
				ObjectType(const String& name, ObjectGroup* objGroup);

				//! \brief Copy constructor.
				//! \param other the ObjectType to be copied
				ObjectType(const ObjectType& other);

				//! \brief Destructor.
				virtual ~ObjectType();

				//! \brief Registers a new ObjectInstance to this ObjectType.
				//! \param name		the name of the ObjectInstance to add to this ObjectType
				//! \param isClientOnly		flag to separate dummy Instances from client and real Instances from the game
				//! \return the pointer to the created ObjectInstance
				//! \remark The ObjectInstance need to be attached after all variables are registered
				ObjectInstance* createObjectInstanceInternal(const String& name, const bool isClientOnly);

				//! \brief Unregisters an ObjectInstance from this ObjectType.
				//! \param objID	the ID of the ObjectInstance to remove from this ObjectType
				//! \returns true, if the ObjectInstance was known and could be removed correctly, and false otherwise
				bool destroyClientOnlyObjectInstance(const uint32 objID);

				//! \brief Initializes the ID of the ObjectType.
				//! 
				//! This function should only be called once. The id must not be INVALID_OBJECT_TYPE_ID.
				//! \param id	the id to set
				void initID( const uint32 id );

				//! \brief Unregisters an existing watchpoint.
				//! \param objInstanceID	the ID of the object instance
				//! \param watchpointID		the ID of the watchpoint
				//! \returns true, if the watch point was registered before and could be unregistered, and false otherwise
				bool unregisterWatchpoint(const uint32 objInstanceID, const uint32 watchpointID);

				

				//! \brief Attaches an ObjectInstance to the debugger.
				//! \param obj		the objectInstance to attach
				void attachObjectInstance(ObjectInstance* obj);

				//! \brief Sends an unregister request for the given ObjectInstance.
				//! \param obj the ObjectInstance to be unregistered
				void reportUnregistrationObjectInstance(ObjectInstance* obj);

				//! \brief Collects the IDs of all breakpoints within the instance of this ObjectType.
				//! \param ids		list to fill with the registered breakpointIDs
				void getBreakpointIDs(Container::List<uint32>& ids) const;

				//! \brief Sets the flag that indicates whether or not the breakpoint is active.
				//! \param val the new value
				void setBreakpointActiveValue( const bool val );

				//! \brief Called when a breakpoint holds.
				//! \param triggerSource	the breakpoint that inflicted this call
				//! \param triggerObjInst	the ObjectInstance that entered the Breakpoint
				//! \returns true, when everything was correct, and false otherwise
				bool enterBreakpoint(BreakpointInstance* triggerSource, ObjectInstance* triggerObjInst);

				//! \brief Informs the module that the connected client closed the connection.
				//!
				//! No breakpoints and watchpoints points should report anymore.
				void onClientDisconnect();

				//! \brief Sends all the information about this ObjectGroup to the Client.
				//! \param newClientInfo	the client info of the new client
				void onClientConnect(ClientInfo* newClientInfo);

				//! \brief Sends a request to register this ObjectType.
				//! \return true, if sending the request was successful, and false otherwise
				bool sendRegister();

				//! \brief Sends a request to unregister this ObjectType.
				//! \return true, if sending the request was successful, and false otherwise
				bool sendUnregister();

				//! \brief Writes the value of the wanted variable into the stream via given MessageBuilder.
				//! \param msgBuilder			the MessageHandler to write the variable to
				//! \param objInstanceID		the ID of the object instance
				//! \param variableName			the name of the variable
				//! \return true, if the variable could be written correctly, and false otherwise
				bool getVariableData(Network::Message::MessageBuilder& msgBuilder, const uint32 objInstanceID, const String& variableName) const;

				//! \brief Registers a new watch point.
				//! \param watchTypeInstanceID	the ID of the watch point type within the object type
				//! \param objInstanceID		the ID of the object instance
				//! \param variableName			the name of the variable
				//! \return true, if the watch point wasn't registered before and could be registered, and false otherwise
				bool registerWatchpoint(const uint32 objInstanceID, const String& variableName);

				//! \brief Creates a new VariableType ID to a variable type.
				//! \param v the variable type
				//! \return the new VariableType ID
				uint32 createVariableTypeID( VariableType* v );

				//! \brief Deregisters the given VariableType ID.
				//! \param id the ID
				void deregisterVariableTypeID( const uint32 id );

				//! \brief Activates or deactivates the breakpoint instance with the given ID.
				//! \param breakPointInstanceID the breakpoint instance ID
				//! \param newValue set it to true, to activate the breakpoint, and to false to deactivate the breakpoint
				bool setBreakpointValue(const uint32 breakPointInstanceID, const bool newValue);

				//! \brief Activates or deactivates the breakpoint instance with the given name.
				//! \param breakPointTypeName the breakpoint instance name
				//! \param newValue set it to true, to activate the breakpoint, and to false to deactivate the breakpoint
				bool setBreakpointValue(const String& breakPointTypeName, const bool newValue);

				//! \brief Gets the DebuggerVariableAccessor of the given ObjectInstance.
				//! \param objInst the ObjectInstance
				//! \return a pointer to the DebuggerVariableAccessor
				const DebuggerVariableAccessor* getVariableAccessor(ObjectInstance* objInst) const;

				//! \brief Checks whether a client is connected.
				//! \return true, if a client is connected, and false otherwise
				bool isConnected() const;

				//! \brief Gets the matching variable name to a given variable ID.
				//! \param name stores the variable name
				//! \param id the variable ID
				//! \return true, if the variable was found, and false otherwise
				bool getVarName(String& name, const uint32 id) const;

				//! \brief creates an ObjectInstance that isn't created within game
				//! \param name		the name of the ObjectInstance to add to this ObjectType
				//! \return the pointer to the created ObjectInstance
				//! \remark The ObjectInstance need to be attached after all variables are registered
				ObjectInstance* createClientOnlyObjectInstance(const String& objectInstanceName);

				//! \brief returns whether this ObjectType is acknowledged or is waiting that the connected client acknowledges it
				//! \return false if connected client doesn't acknowledged this ObjectType, else true
				bool getAcknowledgeFlag();

				//! \brief sets the acknowledge flag
				//! \param newValue		new value of the acknopwledge flag
				//! \remark not acknowledged ObjectTypes have a mutex preventing ObjectInstance registrations
				void setAcknowledgeFlag(const bool newValue);

				//! \brief sets this type to be server only
				//! \remark blocks all communication from this type
				void setTypeServerOnly();

				//! \brief gets whether this type is server only
				bool getTypeServerOnly();

			//member variables
				String			mName;							//!< the name of the ObjectType
				uint32			mID;							//!< the ID of the ObjectType
				ObjectGroup*	mObjectGroup;					//!< the ObjectGroup which contains this ObjectType
				bool			mIsBreakpointActivated;			//!< flag indicating whether or not the breakpoint is activated
				uint32			mUniqueID0;						//!< unique ID part 0
				uint32			mUniqueID1;						//!< unique ID part 1
				uint32			mUniqueID2;						//!< unique ID part 2
				uint32			mUniqueID3;						//!< unique ID part 3

				bool	mIsAcknowledged;
				Threading::Semaphore	mSemaphore;

				Container::LookUp<uint32, BreakpointInstance*>	mBreakpointList;	//!< list of breakpoints
				Container::LookUp<uint32, VariableType*>		mVariableList;		//!< list of variables
				Container::LookUp<uint32, ObjectInstance*>		mInstanceList;		//!< list of instances

				Container::LookUp<String, uint32>				mVarNamesToVarIDs;	//!< mapping of variable names to variable IDs

				uint32			mHoldingBreakpointID;			//!< the ID of the holding breakpoint
				bool		mIsAttached;						//!< flag indicating whether or not the ObjectType is attached
				ClientInfo*	mClientConnected;					//!< holds the client info of the connected client
			
				bool mServerOnly;	//!< flag indicating whether this type was not found on client side
			};
		}
	}
}

