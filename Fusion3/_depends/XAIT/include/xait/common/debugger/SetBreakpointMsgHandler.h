// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/network/message/MessageHandler.h>
#include <xait/common/debugger/ObjectType.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_SET_BREAKFLAG_BREAKPOINT_INSTANCE.
			//!
			//! The client asks the server to set a breakflag for a breakpoint instance.
			class SetBreakpointMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_SETBREAKPOINT_PARAMETER	"setBreakpointMessage: { uint32:moduleID; uint32:breakpointInstanceID; bool:newValue }"

			public:
				//! \brief Constructor
				//! \param server	the server allows access to the registered modules
				SetBreakpointMsgHandler( Server* server );

				//! \brief handles the message by reading the data through the MessageReader
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if message was handled correctly, else false
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				bool getObjectType(ObjectType*& objType);

				bool setBreakpoint(ObjectType* objType);

				//! \brief Sends the reply to the given connection ID.
				//! \param connectionID the connection ID
				//! \return true, if sending the reply was successful, and false otherwise
				bool reportReply( const uint32 connectionID );


				Server*		mServer;			//!< the server

				uint32 mModuleID;				//!< the respective module
				uint32 mBreakpointInstanceID;	//!< the instance ID of the breakpoint
				bool mNewValue;
			};

		}
	}
}
