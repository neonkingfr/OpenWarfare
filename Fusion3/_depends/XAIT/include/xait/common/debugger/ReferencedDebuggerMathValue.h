// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/container/List.h>
#include <xait/common/parser/formula/MathValue.h>
#include <xait/common/parser/formula/VariableResolver.h>
#include <xait/common/debugger/DebuggerVariableAccessor.h>


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief This class describes a referenced math value that can be used by the debugger.
			template<typename T_VALUE_TYPE>
			class ReferencedDebuggerMathValue : public Parser::Formula::VariableMathValue<T_VALUE_TYPE>
			{
			public:
				//! \brief Constructor.
				//! \param varName	name of the variable
				ReferencedDebuggerMathValue(const String& varName)
					: mVarName(varName)
				{}

				//! \brief Destructor.
				virtual ~ReferencedDebuggerMathValue()
				{}

				//! \brief Evaluates the math value.
				//! \param value stores the result
				//! \param varAccessor the variable accessor
				//! \see TypedMathValue
				void evaluate(T_VALUE_TYPE& value,Parser::Formula::VariableAccessor* varAccessor)
				{
					((DebuggerVariableAccessor*)varAccessor)->getValue(value,mVarName);
				}

				//! \brief Evaluates the math value.
				//! \param value stores the result
				//! \param varAccessor the variable accessor
				//! \see MathValue
				void evaluateToPtr(void* value,Parser::Formula::VariableAccessor* varAccessor)
				{
					*((T_VALUE_TYPE*)value) = *((T_VALUE_TYPE*)((DebuggerVariableAccessor*)varAccessor)->getValuePtr(mVarName));
				}

				//! \brief Evaluates the math value.
				//! \param varAccessor the variable accessor
				//! \return a pointer to the result if it could be evaluated, or NULL otherwise
				//! \see MathValue
				virtual void* getValueReference(Parser::Formula::VariableAccessor* varAccessor)
				{
					XAIT_FAIL("not needed");
					return NULL;
				}

				//! \brief Checks whether the value is a referenced value.
				//! \return true, if it is referenced, and false otherwise
				//! \see MathValue
				virtual bool isReferenceValue() const
				{
					return true;
				}

				//! \brief Gets the ID of the variable.
				//! \return the ID
				//! \see MathValue
				virtual uint32 getVariableID()
				{
					XAIT_FAIL("not implemented - not needed");
					return X_MAX_UINT32;
				}

				//! \see MathValue
				virtual void getContainingVariables(Container::Vector<uint32>& variables) const
				{
					XAIT_FAIL("not needed");
				}

				//! \brief Replaces the variable access ID
				//! \param oldAccessID the old access ID
				//! \param newAccessID the new access ID
				//! \see MathValue
				virtual void replaceVariableAccessID(uint32 oldAccessID, uint32 newAccessID)
				{
					XAIT_FAIL("not needed");
				}

				//! \brief Clones the MathValue.
				//! \returns the cloned object
				virtual Parser::Formula::MathValue* clone() const
				{
					return new(Parser::Formula::TypedMathValue<T_VALUE_TYPE>::mAllocator) ReferencedDebuggerMathValue(mVarName);
				}

				//! \brief Compares two math values.
				//! \return true, when they are equal, and false otherwise
				virtual bool compare(Parser::Formula::MathValue* mathValue) const
				{
					XAIT_FAIL("not needed");
					return false;
				}

				virtual Common::String convertToString() const 
				{ 
					XAIT_FAIL("not needed");

					return "";
				} 

			protected:
				String		mVarName; //!< the variable name

				XAIT_MATHVALUE_TYPE_FUNCTIONS(ReferencedDebuggerMathValue<T_VALUE_TYPE>); 
			};


			//! \brief This class describes a referenced math value vector that can be used by the debugger.
			template<typename T_VALUE_TYPE>
			class ReferencedDebuggerMathValueVector : public Parser::Formula::VariableMathValue<Container::Vector<T_VALUE_TYPE> >
			{
			public:
				//! \brief Constructor.
				//! \param varName	name of the variable
				ReferencedDebuggerMathValueVector(const String& varName)
					: mVarName(varName)
				{}

				//! \brief Destructor.
				virtual ~ReferencedDebuggerMathValueVector()
				{}

				//! \brief Evaluates the math value.
				//! \param value stores the result
				//! \param varAccessor the variable accessor
				//! \see TypedMathValue
				void evaluate(Container::Vector<T_VALUE_TYPE>& value,Parser::Formula::VariableAccessor* varAccessor)
				{
					((DebuggerVariableAccessor*)varAccessor)->getValue(value,mVarName);
				}

				//! \brief Evaluates the math value.
				//! \param value stores the result
				//! \param varAccessor the variable accessor
				//! \see MathValue
				void evaluateToPtr(void* value,Parser::Formula::VariableAccessor* varAccessor)
				{
					*((Container::Vector<T_VALUE_TYPE>*)value) = *((Container::Vector<T_VALUE_TYPE>*)((DebuggerVariableAccessor*)varAccessor)->getValuePtr(mVarName));
				}

				//! \brief Evaluates the math value.
				//! \param varAccessor the variable accessor
				//! \return pointer to the value that stores the result
				//! \see MathValue
				virtual void* getValueReference(Parser::Formula::VariableAccessor* varAccessor)
				{
					XAIT_FAIL("not needed");
					return NULL;
				}

				//! \brief Checks whether the value is a referenced value.
				//! \return true, if it is referenced, and false otherwise
				//! \see MathValue
				virtual bool isReferenceValue() const
				{
					return true;
				}

				//! \brief Gets the ID of the variable.
				//! \return the ID
				//! \see MathValue
				virtual uint32 getVariableID()
				{
					XAIT_FAIL("not implemented - not needed");
					return X_MAX_UINT32;
				}

				//! \see MathValue
				virtual void getContainingVariables(Container::Vector<uint32>& variables) const
				{
					XAIT_FAIL("not needed");
				}

				//! \brief Replaces the variable access ID
				//! \param oldAccessID the old access ID
				//! \param newAccessID the new access ID
				//! \see MathValue
				virtual void replaceVariableAccessID(uint32 oldAccessID, uint32 newAccessID)
				{
					XAIT_FAIL("not needed");
				}

				//! \brief Clones the MathValue.
				//! \returns the cloned object
				virtual Parser::Formula::MathValue* clone() const
				{
					return new(Parser::Formula::TypedMathValue<Container::Vector<T_VALUE_TYPE> >::mAllocator) ReferencedDebuggerMathValueVector(mVarName);
				}

				//! \brief Compares two math values.
				//! \return true, when they are equal, and false otherwise
				virtual bool compare(Parser::Formula::MathValue* mathValue) const
				{
					XAIT_FAIL("not needed");
					return false;
				}

				virtual Common::String convertToString() const 
				{ 
					XAIT_FAIL("not needed");

					return "";
				} 

			protected:
				String		mVarName;	//!< the variable name

				XAIT_MATHVALUE_TYPE_FUNCTIONS(ReferencedDebuggerMathValue<Container::Vector<T_VALUE_TYPE> >); 
			};


			// this mathvalue is threadsafe because it does not evaluate anything
			class UntypedReferencedDebuggerMathValue : public Parser::Formula::MathValue
			{
			public:
				//! \brief Constructor.
				//! \param varName	name of the variable
				//! \param datatypeID the data type ID
				UntypedReferencedDebuggerMathValue(const String& varName, const Reflection::Datatype::DatatypeID& datatypeID );

				//! \brief Destructor.
				virtual ~UntypedReferencedDebuggerMathValue();

				//! \brief Evaluates the math value.
				//! \param value stores the result
				//! \param varAccessor the variable accessor
				//! \see MathValue
				void evaluateToPtr(void* value, Parser::Formula::VariableAccessor* varAccessor);

				//! \brief Gets the variable ID.
				//! \return the variable ID
				//! \see MathValue
				virtual uint32 getVariableID();

				//! \see MathValue
				virtual void getContainingVariables(Common::Container::Vector<uint32>& variables) const;

				//! \brief Replaces the variable access ID
				//! \param oldAccessID the old access ID
				//! \param newAccessID the new access ID
				//! \see MathValue
				virtual void replaceVariableAccessID(uint32 oldAccessID, uint32 newAccessID);

				//! \brief Gets the data type ID.
				//! \return the data type ID
				virtual Common::Reflection::Datatype::DatatypeID getDatatypeID() const;

				//! \brief Checks whether the math value is constant.
				//! \return true, if it is constant, and false otherwise
				virtual bool isConstant();

				//! \brief Clones the MathValue.
				//! \returns the cloned object
				virtual MathValue* clone() const;

				//! \brief Compares two math values.
				//! \return true, when they are equal, and false otherwise
				virtual bool compare(MathValue* mathValue) const;

				virtual Common::String convertToString() const; 

			protected:
				//Common::Reflection::Datatype::DatatypeBase*	mDatatype;
				//XFSM::FSMVarID								mVarID;
				String		mVarName;								//!< the name of the variable
				Reflection::Datatype::DatatypeID	mDatatypeID;	//!< the data type ID of the variable

				XAIT_MATHVALUE_TYPE_FUNCTIONS(UntypedReferencedDebuggerMathValue); 

			};
		}
	}
}
