// (C) xaitment GmbH 2006-2012

#pragma once
#ifndef _XAIT_COMMON_IDWRAPPER_H_
#define _XAIT_COMMON_IDWRAPPER_H_

#include <ostream>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/NumericLimits.h>

namespace XAIT 
{
	namespace Common
	{
		template<typename ID_T, typename NAMESPACE_T, int UNIQUE_ID>
		class IDWrapper;

		template<typename ID_T, typename NAMESPACE_T, int UNIQUE_ID>
		ID_T& GetIDWrapperValue(IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID>& val);

		template<typename ID_T, typename NAMESPACE_T, int UNIQUE_ID>
		inline std::ostream& operator<<(std::ostream& os,const IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID> &val);


		//! A wrapper class for standard c-style number types like int,float ... 
		//! Each wrapper will be defined with an extra unique identifier. Number types
		//! with different unique identifiers cannot be combined without an explicit
		//! cast. Thus we can define new different c number types which are incompatible.
		//! This has the advantage, that you can prevent assignments and computations between
		//! c numbers that have different types of usage, i.e. two different types of ids cannot
		//! be mixed.
		//! \brief A wrapper class for standard c-style number types
		//! \param ID_T			type of a c number type (int,float ...) that should be wrapped
		//! \param NAMESPACE_T	type of the class which has access for assigning c numbers to the wrapper
		//! \param UNIQUE_ID	number for differentiation between different types in one namespace

#if XAIT_OS != XAIT_OS_WII
#define XAIT_IDWRAPPER_TEMPLATE_GENERATION_FIX()\
		private:\
		template<typename OTHER_ID_T, typename OTHER_NAMESPACE_T, int OTHER_UNIQUE_ID>\
		friend class IDWrapper;
#else
#define XAIT_IDWRAPPER_TEMPLATE_GENERATION_FIX()
#endif

		
#define XAIT_IDWRAPPER_TEMPLATE_GENERATION(ID_T,NAMESPACE_T,UNIQUE_ID) \
	template<>\
		class IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID>\
		{\
		public:\
		typedef IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID>		CNumType;\
		IDWrapper()\
		{\
		mVal= XAIT::Common::Math::NumericLimits<ID_T>::maxPosValue();\
		}\
		template<typename OTHER_ID_T, typename OTHER_NAMESPACE_T, int OTHER_UNIQUE_ID>\
		explicit IDWrapper(const IDWrapper<OTHER_ID_T,OTHER_NAMESPACE_T,OTHER_UNIQUE_ID>& other)\
		: mVal((ID_T)other.mVal)\
		{}\
		template<typename OTHER_ID_T>\
		IDWrapper(const IDWrapper<OTHER_ID_T,NAMESPACE_T,UNIQUE_ID>& other)\
		: mVal((ID_T)other.mVal)\
		{}\
		inline IDWrapper& operator= (const IDWrapper& other)\
		{\
		mVal= other.mVal;\
		return *this;\
		}\
		inline bool operator< (const IDWrapper& other) const\
		{\
		return mVal < other.mVal;\
		}\
		inline bool operator<= (const IDWrapper& other) const\
		{\
		return mVal <= other.mVal;\
		}\
		inline bool operator> (const IDWrapper& other) const\
		{\
		return mVal > other.mVal;\
		}\
		inline bool operator>= (const IDWrapper& other) const\
		{\
		return mVal >= other.mVal;\
		}\
		inline bool operator== (const IDWrapper& other) const\
		{\
		return mVal == other.mVal;\
		}\
		inline bool operator!= (const IDWrapper& other) const\
		{\
		return mVal != other.mVal;\
		}\
		inline bool operator== (const ID_T other) const\
		{\
		return mVal == other;\
		}\
		inline bool operator!= (const ID_T other) const\
		{\
		return mVal != other;\
		}\
        XAIT_IDWRAPPER_TEMPLATE_GENERATION_FIX()\
		ID_T	mVal;\
		friend class NAMESPACE_T;\
		friend std::ostream& operator<< <ID_T,NAMESPACE_T,UNIQUE_ID>(std::ostream& os,const IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID> &val);\
		friend ID_T& GetIDWrapperValue<ID_T,NAMESPACE_T,UNIQUE_ID>(IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID>& val);\
		friend const ID_T& GetIDWrapperValue<ID_T,NAMESPACE_T,UNIQUE_ID>(const IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID>& val);\
		IDWrapper(const ID_T val)\
		: mVal(val)\
		{}\
		inline IDWrapper operator+ (const IDWrapper& other) const\
		{\
		IDWrapper tmp;\
		tmp.mVal= mVal + other.mVal;\
		return tmp;\
		}\
		inline IDWrapper operator- (const IDWrapper& other) const\
		{\
		IDWrapper tmp;\
		tmp.mVal= mVal - other.mVal;\
		return tmp;\
		}\
		inline IDWrapper operator* (const IDWrapper& other) const\
		{\
		IDWrapper tmp;\
		tmp.mVal= mVal * other.mVal;\
		return tmp;\
		}\
		inline IDWrapper operator/ (const IDWrapper& other) const\
		{\
		IDWrapper tmp;\
		tmp.mVal= mVal / other.mVal;\
		return tmp;\
		}\
		inline IDWrapper& operator+= (const IDWrapper& other)\
		{\
		mVal+= other.mVal;\
		return *this;\
		}\
		inline IDWrapper& operator-= (const IDWrapper& other)\
		{\
		mVal-= other.mVal;\
		return *this;\
		}\
		inline IDWrapper& operator*= (const IDWrapper& other)\
		{\
		mVal*= other.mVal;\
		return *this;\
		}\
		inline IDWrapper& operator/= (const IDWrapper& other)\
		{\
		mVal/= other.mVal;\
		return *this;\
		}\
		inline IDWrapper& operator++ ()\
		{\
		++mVal;\
		return *this;\
		}\
		inline IDWrapper operator++ (int)\
		{\
		ID_T tmp= mVal;\
		++mVal;\
		return tmp;\
		}\
		inline IDWrapper& operator-- ()\
		{\
		--mVal;\
		return *this;\
		}\
		inline IDWrapper operator-- (int)\
		{\
		ID_T tmp= mVal;\
		--mVal;\
		return tmp;\
		}\
		inline IDWrapper operator& (const IDWrapper& other) const\
		{\
		IDWrapper tmp;\
		tmp.mVal= mVal & other.mVal;\
		return tmp;\
		}\
		inline IDWrapper operator| (const IDWrapper& other) const\
		{\
		IDWrapper tmp;\
		tmp.mVal= mVal | other.mVal;\
		return tmp;\
		}\
		inline IDWrapper operator>> (const uint32 bitShift) const\
		{\
		IDWrapper tmp;\
		tmp.mVal= mVal >> bitShift;\
		return tmp;\
		}\
		inline IDWrapper operator<< (const uint32 bitShift) const\
		{\
		IDWrapper tmp;\
		tmp.mVal= mVal << bitShift;\
		return tmp;\
		}\
		};


		template<typename ID_T, typename NAMESPACE_T, int UNIQUE_ID>
		std::ostream& operator<<(std::ostream& os,const IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID> &val)
		{
			os << val.mVal;
			return os;
		}

		template<typename ID_T, typename NAMESPACE_T, int UNIQUE_ID>
		ID_T& GetIDWrapperValue(IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID>& val)
		{
			return val.mVal;
		}

		template<typename VAL_T>
		const VAL_T& GetIDWrapperValue(const VAL_T& val)
		{
			return val;
		}

		template<typename ID_T, typename NAMESPACE_T, int UNIQUE_ID>
		const ID_T& GetIDWrapperValue(const IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID>& val)
		{
			return val.mVal;
		}
	}
}

#endif
