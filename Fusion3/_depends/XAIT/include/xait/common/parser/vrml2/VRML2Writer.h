// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/Stream.h>
#include <xait/common/DLLDefines.h>
#include <xait/common/container/Vector.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{
				class VRML2Writer
				{
				public:
					XAIT_COMMON_DLLAPI VRML2Writer();
					XAIT_COMMON_DLLAPI ~VRML2Writer();

					XAIT_COMMON_DLLAPI bool open(IO::Stream* outputStream);

					XAIT_COMMON_DLLAPI void close(const bool closeStream= true);

					template<typename T_VERTEX, typename T_TRIANGLE>
					void append(const Container::Vector<T_VERTEX>& vertices, const Container::Vector<T_TRIANGLE>& triangles, const bool ccw= true)
					{
						if (vertices.size() > 0 && triangles.size() > 0)
							append(&vertices[0],vertices.size(),&triangles[0],triangles.size(),ccw);
					}

					template<typename T_VERTEX_LIST, typename T_TRIANGLE_LIST>
					void append(const T_VERTEX_LIST& vertices, const uint32 numVertices, const T_TRIANGLE_LIST& triangles, const uint32 numTriangles, const bool ccw= true)
					{
						writeLine("Shape {");
						writeLine("\tgeometry IndexedFaceSet {");

						if (ccw)
							writeLine("\t\tccw TRUE");
						else
							writeLine("\t\tccw FALSE");

						writeLine("\t\tcoord Coordinate { point [");

						char tmp[256];

						for(uint32 i= 0; i < numVertices; ++i)
						{
#if XAIT_OS == XAIT_OS_WIN || XAIT_OS == XAIT_OS_XBOX360
							sprintf_s(tmp,256,"\t\t\t%f %f %f",vertices[i].mX,vertices[i].mY,vertices[i].mZ);
#else
							sprintf(tmp,"\t\t\t%f %f %f",vertices[i].mX,vertices[i].mY,vertices[i].mZ);
#endif

							writeLine(tmp);
						}

						writeLine("\t\t] }");
						writeLine("\t\tcoordIndex [");

						for(uint32 i= 0; i < numTriangles; ++i)
						{
#if XAIT_OS == XAIT_OS_WIN || XAIT_OS == XAIT_OS_XBOX360
							sprintf_s(tmp,256,"\t\t\t%d %d %d -1", triangles[i].mVert[0],triangles[i].mVert[1],triangles[i].mVert[2]);
#else
							sprintf(tmp,"\t\t\t%d %d %d -1", triangles[i].mVert[0],triangles[i].mVert[1],triangles[i].mVert[2]);
#endif
							writeLine(tmp);
						}

						writeLine("\t\t]");
						writeLine("\t}");
						writeLine("}");
					}

				private:
					IO::Stream*		mStream;

					void writeVRML2Header();
					XAIT_COMMON_DLLAPI void writeStr(const char* str);
					XAIT_COMMON_DLLAPI void writeLine(const char* str);

				};
			}
		}
	}
}
