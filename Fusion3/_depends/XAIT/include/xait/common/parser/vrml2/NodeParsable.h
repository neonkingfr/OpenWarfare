// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/MemoryManaged.h>


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{
				class ParseInterface;

				template<typename DERIVED_NODE_T>
				inline static DERIVED_NODE_T* NodeCopy(DERIVED_NODE_T* srcPtr, const Memory::Allocator::Ptr& allocator)
				{
					return srcPtr ? (DERIVED_NODE_T*)srcPtr->getCopy(allocator) : NULL;
				}
		

				template<class NODE_T>
				class NodeParsable : public Memory::MemoryManaged
				{
				public:
					virtual ~NodeParsable() {};

					virtual void parseNode(ParseInterface* iParser)= 0;
					virtual NODE_T* getCopy(const Memory::Allocator::Ptr& allocator)= 0;
					virtual const char* getNodeName() const= 0;
				};
			}

		}
	}
}
