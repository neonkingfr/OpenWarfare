// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/vrml2/SceneNode.h>
#include <xait/common/parser/vrml2/FieldTypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{
				class AppearanceNode : public SceneNode
				{
				public:
					SFSceneNode			mMaterial;
					SFSceneNode			mTexture;
					SFSceneNode			mTextureTransform;


					AppearanceNode(SceneNode* parent);
					AppearanceNode(const AppearanceNode& other);
					~AppearanceNode();

					void parseNode(ParseInterface* iParser);
					AppearanceNode* getCopy(const Memory::Allocator::Ptr& allocator);
					const char* getNodeName() const;
				};

			}
		}
	}
}
