// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/vrml2/SceneNode.h>
#include <xait/common/parser/vrml2/FieldTypes.h>
#include <xait/common/parser/vrml2/Rotation.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{

			namespace VRML2
			{

				class TransformNode : public SceneNode
				{
				public:
					SFVec3f					mCenter;
					SFRotation				mRotation;
					SFQuaternion			mQuaternion;		//!< extension to the vrml2 standard, default 1,0,0,0 (w,x,y,z)
					MFSceneNode				mChildren;
					SFVec3f					mScale;
					SFRotation				mScaleOrientation;
					SFVec3f					mTranslation;
					SFVec3f					mBBoxCenter;
					SFVec3f					mBBoxSize;

					TransformNode(SceneNode* parent);
					TransformNode(const TransformNode& other);

					~TransformNode();

					void parseNode(ParseInterface* iParser);
					SceneNode* getCopy(const Memory::Allocator::Ptr& allocator);
					const char* getNodeName() const;

				};

			}
		}
	}
}
