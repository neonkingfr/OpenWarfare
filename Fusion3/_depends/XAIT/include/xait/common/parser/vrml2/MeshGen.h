// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/String.h>
#include <xait/common/container/Vector.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/math/Matrix4.h>
#include <xait/common/geom/IndexedTriangle.h>


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{
				class SceneNode;
				class TransformNode;
				class InlineNode;
				class ShapeNode;
				class GeometryNode;
				class BoxNode;
				class IndexedFaceSetNode;
				class ElevationGridNode;


				class MeshGen : public Memory::MemoryManaged
				{
				public:
					//! \brief opens a vrml2 file and imports the triangles from it
					//! \param vertices		vertex array to add vertices
					//! \param triangles	triangle array to add triangles
					//! \param rootNode		root of the vrml2 tree which should be converted into a mesh
					XAIT_COMMON_DLLAPI void createMesh(Container::Vector<Math::Vec3f>& vertices, Container::Vector<Geom::IndexedTriangle>& triangles, const SceneNode* rootNode);

				private:
					Container::Vector<Math::Vec3f>*				mVertices;
					Container::Vector<Geom::IndexedTriangle>*	mTriangles;

					void extractGeometry(const SceneNode* node, const Math::Matrix4f& frame);
					void extractGeomTransform(const TransformNode* node, const Math::Matrix4f& frame);
					void extractGeomInline(const InlineNode* node, const Math::Matrix4f& frame);
					void extractGeomShape(const ShapeNode* node, const Math::Matrix4f& frame);

					void convertGeometry(const GeometryNode* geomNode, const Math::Matrix4f& frame);
					void convertGeomBox(const BoxNode* boxNode, const Math::Matrix4f& frame);
					void convertGeomIndexedFaceSet(const IndexedFaceSetNode* indexedFaceSetNode, const Math::Matrix4f& frame);
					void convertGeomElevationGrid(const ElevationGridNode* elevationGridNode, const Math::Matrix4f& frame);
				};
			}
		}
	}
}
