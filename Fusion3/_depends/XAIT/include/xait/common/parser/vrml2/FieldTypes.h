// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/container/Vector.h>
#include <xait/common/String.h>
#include <xait/common/math/Quaternion.h>

namespace XAIT
{	
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{

				class SceneNode;
				class GeometryNode;
				class GeometricProperty;
				struct Rotation;

				typedef bool					SFBool;
				typedef int32					SFInt32;
				typedef float32					SFFloat;
				typedef Math::Vec2f				SFVec2f;
				typedef Math::Vec3f				SFVec3f;
				typedef Math::Vec3f				SFColor;
				typedef Rotation				SFRotation;
				typedef Math::Quaternionf32		SFQuaternion;
				typedef SceneNode*				SFSceneNode;
				typedef GeometryNode*			SFGeometryNode;
				typedef GeometricProperty*		SFGeometricProperty;
				typedef Common::String			SFString;

				typedef Container::Vector<SFInt32>			MFInt32;
				typedef Container::Vector<SFFloat>			MFFloat;
				typedef Container::Vector<SFVec3f>			MFVec3f;
				typedef Container::Vector<SFColor>			MFColor;
				typedef Container::Vector<SFRotation>		MFRotation;
				typedef Container::Vector<SFQuaternion>		MFQuaternion;
				typedef Container::Vector<SFSceneNode>		MFSceneNode;
				typedef Container::Vector<SFGeometryNode>	MFGeometryNode;
				typedef Container::Vector<SFString>			MFString;
			}
		}
	}
}
