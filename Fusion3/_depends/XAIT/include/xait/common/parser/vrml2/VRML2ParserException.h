// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/parser/ParserException.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{
				class VRML2ParserException : public ParserException
				{
				public:
					VRML2ParserException(const uint32 line, const uint32 row, const String& fileName, const String& errorMsg)
						: ParserException(errorMsg,line,row),mFileName(fileName)
					{}

					//! \brief get the name of the exception
					virtual Common::String getExceptionName() const
					{
						return "VRML2ParserException";
					}

					//! \brief name of the file currently handled 
					inline const String& getFileName() const
					{
						return mFileName;
					}

				private:
					String		mFileName;
				};
			}
		}
	}
}
