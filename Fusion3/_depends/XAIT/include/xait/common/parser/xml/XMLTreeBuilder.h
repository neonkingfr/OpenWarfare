// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/xml/XMLNode.h>
#include <xait/common/memory/MemoryManaged.h>

#define XMLTREEBUILDER_TMPSTRINGSIZE	128

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				//! \brief the xml tree builder is used to build up a xml tree and ,later on, to destroy it
				class XMLTreeBuilder : public Memory::MemoryManaged
				{
				public:
					//! \brief constructor of xml tree builder
					XMLTreeBuilder()
						: copyStrings(false),root(NULL),currNode(NULL)
					{};


					//! \brief destructor
					~XMLTreeBuilder()
					{
						deleteXMLTree();
					}


					//! \brief start a new xml tree
					//! \param rootname			name of the xml root
					//! \param copyStrings		indicates whether to copy passed strings for names,attributes, values
					//! \remark This function does delete an existing tree (calls XMLTreeBuilder::deleteXMLTree )
					XAIT_COMMON_DLLAPI void startXMLTree(const char* rootname, const bool copyStrings);

					//! \brief starts a new xml tree without initial rootnode
					//! \param copyStrings		indicates whether to copy passed strings for names,attributes, values
					//! \remark This function does delete an existing tree (calls XMLTreeBuilder::deleteXMLTree ). The
					//!	only allowed call so far is XMLTreeBuilder::enterSubTree as next functioncall. All others will
					//! result in an segfault, since there is no root object. This function has been add for the parser, so 
					//! in normal use, you dont need it.
					XAIT_COMMON_DLLAPI void startXMLTree(const bool copyStrings);

					//! \brief delete current xml tree
					XAIT_COMMON_DLLAPI void deleteXMLTree();

					//! \brief add a text attribute to the latest added node
					//! \param name		name of the attribute
					//! \param string	text for the attribute
					XAIT_COMMON_DLLAPI void addAttribText(const char* name, const char* string);

					//! \brief add a int32 attribute to the latest added node
					//! \param name		name of the attribute
					//! \param value	int32 value for the attribute
					//! \remark This function will fail, if copyStrings is false
					XAIT_COMMON_DLLAPI void addAttribInt32(const char* name,const int32 value);

					//! \brief add a uint32 attribute to the latest added node
					//! \param name		name of the attribute
					//! \param value	uint32 value for the attribute
					//! \remark This function will fail, if copyStrings is false
					XAIT_COMMON_DLLAPI void addAttribUInt32(const char* name,const uint32 value);

					//! \brief add a bool attribute to the latest added node
					//! \param name		name of the attribute
					//! \param value	bool value for the attribute
					//! \remark This function will fail, if copyStrings is false
					XAIT_COMMON_DLLAPI void addAttribBool(const char* name,const bool value);

					//! \brief add a float32 attribute to the latest added node
					//! \param name		name of the attribute
					//! \param value	float32 value for the attribute
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addAttribFloat32(const char* name,const float32 value);

					//!	\brief add text as element
					//! \param name		name of the element
					//! \param string	text in the elementdata
					XAIT_COMMON_DLLAPI void addElemText(const char* name, const char* string);

					//! \brief add a bool as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemBool(const char* name, const bool value);

					//! \brief add an int32 as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemInt32(const char* name, const int32 value);

					//! \brief add an uint32 as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemUInt32(const char* name, const uint32 value);

					//! \brief add an int64 as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemInt64(const char* name, const int64 value);

					//! \brief add an uint64 as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemUInt64(const char* name, const uint64 value);

					//! \brief add a float32 as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemFloat32(const char* name, const float32 value);

					//! \brief add a float32 as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemFloat64(const char* name, const float64 value);

					//! \brief add a vec3f as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemVec3f(const char* name, const Math::Vec3f& value);

					//! \brief add a vec3u32 as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemVec3u32(const char* name, const Math::Vec3u32& value);

					//! \brief add a vec3i32 as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemVec3i32(const char* name, const Math::Vec3i32& value);

					//! \brief add a vec2f as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemVec2f(const char* name, const Math::Vec2f& value);

					//! \brief add a vec2u32 as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemVec2u32(const char* name, const Math::Vec2u32& value);

					//! \brief add a vec2i32 as element
					//! \param name		name of the element
					//! \param value	value in the elementdata
					//! \remark This function will fail, if copyString is false
					XAIT_COMMON_DLLAPI void addElemVec2i32(const char* name, const Math::Vec2i32& value);

					//! \brief add an element with empty elementdata
					//! \param name		name of the element
					XAIT_COMMON_DLLAPI void addElemEmpty(const char* name);

					//! \brief add a new element containing other elements (subtree) and go into this subtree
					//! \param name		name of the subtree
					XAIT_COMMON_DLLAPI void enterSubTree(const char* name);

					//! \brief exit a subtree and go back to last tree
					//! \param name		name of the subtree which to exit, or NULL if no check required
					//! \returns true if we could exit the subtree, or false if the name does not mathc the current handled subtree
					XAIT_COMMON_DLLAPI bool exitSubTree(const char* name= NULL);

					//! \brief get the tree from the builder
					inline XMLNode* getXMLTree() const
					{
						return root;
					}


					// ----------------------------------- help functions for parser ---------------------------------

					void _addNode(const XMLNode::ElementData type);

					//! \brief add a complete tree vom another treebuilder
					//! \param tree		other treebuilder
					//! \remark This removes the complete tree from the other treebuilder and
					//!			adds it to the current tree. This assumes that the other tree
					//!			uses the same memory allocator. They must also have the same
					//!			copyStrings flag. If they does not match, an assertion will rise.
					void _addTree(XMLTreeBuilder* tree);

					void _setName(const char* name);

					void _setText(const char* text);

					//! \brief removes the last added node
					void _removeLastNode();

					//! \brief get the last added node
					const XMLNode* _getLastNode() const;



				private:
					bool						copyStrings;			//!< should passed strings be copied
					XMLNodeElements*			root;					//!< root node of the current generate tree
					XMLNode*					currNode;				//!< latest added node
					std::list<XMLNodeElements*>	lastTreeNode;			//!< last node forming a tree (XMLNodeElements)

					char				tmpString[XMLTREEBUILDER_TMPSTRINGSIZE];	//!< string for format conversion

					//! \brief copies a string if stringcopy is enabled
					//! \param string	string to copy
					//! \returns copied string if stringcopy is enabled, or the old pointer if not, or NULL if no memory available
					const char* copyString(const char* string);

					//! \brief delete a xml node
					//! \param node		node to delete
					void deleteXMLNode(XMLNode* node);

				};

			}
		}
	}

}
