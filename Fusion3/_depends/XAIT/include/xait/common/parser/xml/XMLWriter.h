// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/xml/XMLNode.h>
#include <xait/common/io/Stream.h>

#define XMLWRITER_MAXDEPTH		1024
#define XMLWRITER_NEWLINE		"\n"

//! \brief XML Writer writes a xml tree to a file
namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				class XMLWriter
				{
				public:
					//! constructor using default file function
					XAIT_COMMON_DLLAPI XMLWriter();

					XAIT_COMMON_DLLAPI virtual ~XMLWriter() {};

					//! \brief write a xml header to a stream 
					//! \param stream		stream in which to write the header
					//! \remark stream must be writable
					XAIT_COMMON_DLLAPI virtual void writeHeader(IO::Stream* stream);

					//! \brief write a complete xmltree to a stream (without header)
					//! \param stream		stream in which to write
					//! \param rootNode		pointer to the rootnode of the xmltree
					//! \remark stream must be writable
					//! \exception ArgumentNullException	Throws if XMLTree contains a NULL pointer element
					//! \exception IOException				Throws if we cannot write to the stream
					XAIT_COMMON_DLLAPI virtual void writeTree(IO::Stream* stream, XMLNode* rootNode);

				protected:
					char		tabs[XMLWRITER_MAXDEPTH + 1];		//!< variable holding tab symbols


					//! wrapper methode for write function
					inline void writeString(const char* string, IO::Stream* stream)
					{
						stream->write(string,(uint32)strlen(string));
					}


					//! write a node to the stream, includes recursive calls
					//! \param stream		stream in which to write
					//! \param node			node to write
					//! \param tabs			tabs in front of the element
					void writeNode(IO::Stream* stream, XMLNode* node, const uint32 depth);
				};


			}
		}
	}

}
