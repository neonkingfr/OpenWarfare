// (C) xaitment GmbH 2006-2012

#pragma once

#include <sstream>
#include <xait/common/parser/xml/XMLNode.h>


// \brief read a required node elements entry
// \param subNode	the subnode which contains the node elements (return value)
// \param currNode	current handled xml node
// \param name		name of the node elements tag
#define XMLREAD_ELEMENTS_REQ(subNode,currNode,name)		{if (!XAIT::Common::Parser::XML::XMLTreeReader::ReadNodeElementsReq(subNode,currNode,name)) return false;}

// \brief read a optional node elements entry
// \param subNode	the subnode which should contain the node elements (return value), or NULL if not found
// \param currNode	current handled xml node
// \param name		name of the node elements tag
// \returns true if the element could be read, or false if not
#define XMLREAD_ELEMENTS_OPT(subNode,currNode,name)		(XAIT::Common::Parser::XML::XMLTreeReader::ReadNodeElementsOpt(subNode,currNode,name))


// some helper macros
#define XMLREAD_REQ_READ(currNode,getMethode,value,name)	\
	{ if (!currNode->getMethode(value,name) ) \
	{ XAIT::Common::Parser::XML::XMLTreeReader::PrintTagNameError(currNode,name); return false;} }

#define XMLREAD_OPTDEF_READ(currNode,getMethode,value,name,defvalue) \
	{ value= defvalue; currNode->getMethode(value,name); }

// -------------------------------------------------- Text ----------------------------------------------------

// \brief read a required text node entry as text
// \param text		text to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \remark asserts if the tag has not been found, and issues a return false
#define XMLREAD_TEXT_REQ(text,currNode,name)			XMLREAD_REQ_READ(currNode,getNextText,text,name)

// \brief read an optional text node entry as text
// \param text		text to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns true if the text could be read or false tag has not been found
#define XMLREAD_TEXT_OPT(text,currNode,name)			(currNode->getNextText(text,name))

// \brief read an optional text node entry as text and assign an default value if nothing found
// \param text		text to read from node (return value)
// \param defvalue	default value which will be assigned if nothing found in xmltree
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns nothing
// \remark the null terminated string which is set, is only a local reference, and will not survive a return statement
#define XMLREAD_TEXT_OPT_DEF(text,defvalue,currNode,name)	XMLREAD_OPTDEF_READ(currNode,getNextText,text,name,defvalue)

// -------------------------------------------------- Int32 ---------------------------------------------------

// \brief read a required text node entry as int32
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \remark asserts if the tag has not been found, and issues a return false
#define XMLREAD_INT32_REQ(value,currNode,name)			XMLREAD_REQ_READ(currNode,getNextInt32,value,name)

// \brief read an optional text node entry as int32
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns true if the tag has been found or false if not 
#define XMLREAD_INT32_OPT(value,currNode,name)			(currNode->getNextInt32(value,name))

// \brief read an optional text node entry as int32 and assign an default value if nothing found
// \param value		value to read from node (return value)
// \param defvalue	default value which will be assigned if nothing found in xmltree
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns nothing
#define XMLREAD_INT32_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_OPTDEF_READ(currNode,getNextInt32,value,name,defvalue)

// -------------------------------------------------- UInt32 ---------------------------------------------------

// \brief read a required text node entry as UInt32
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \remark asserts if the tag has not been found, and issues a return false
#define XMLREAD_UINT32_REQ(value,currNode,name)			XMLREAD_REQ_READ(currNode,getNextUInt32,value,name)

// \brief read an optional text node entry as UInt32
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns true if the tag has been found or false if not 
#define XMLREAD_UINT32_OPT(value,currNode,name)			(currNode->getNextUInt32(value,name))

// \brief read an optional text node entry as UInt32 and assign an default value if nothing found
// \param value		value to read from node (return value)
// \param defvalue	default value which will be assigned if nothing found in xmltree
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns nothing
#define XMLREAD_UINT32_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_OPTDEF_READ(currNode,getNextUInt32,value,name,defvalue)

// -------------------------------------------------- Int64 ---------------------------------------------------

// \brief read a required text node entry as int64
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \remark asserts if the tag has not been found, and issues a return false
#define XMLREAD_INT64_REQ(value,currNode,name)			XMLREAD_REQ_READ(currNode,getNextInt64,value,name)

// \brief read an optional text node entry as int64
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns true if the tag has been found or false if not 
#define XMLREAD_INT64_OPT(value,currNode,name)			(currNode->getNextInt64(value,name))

// \brief read an optional text node entry as int64 and assign an default value if nothing found
// \param value		value to read from node (return value)
// \param defvalue	default value which will be assigned if nothing found in xmltree
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns nothing
#define XMLREAD_INT64_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_OPTDEF_READ(currNode,getNextInt64,value,name,defvalue)

// -------------------------------------------------- UInt64 ---------------------------------------------------

// \brief read a required text node entry as UInt64
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \remark asserts if the tag has not been found, and issues a return false
#define XMLREAD_UINT64_REQ(value,currNode,name)			XMLREAD_REQ_READ(currNode,getNextUInt64,value,name)

// \brief read an optional text node entry as UInt64
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns true if the tag has been found or false if not 
#define XMLREAD_UINT64_OPT(value,currNode,name)			(currNode->getNextUInt64(value,name))

// \brief read an optional text node entry as UInt64 and assign an default value if nothing found
// \param value		value to read from node (return value)
// \param defvalue	default value which will be assigned if nothing found in xmltree
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns nothing
#define XMLREAD_UINT64_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_OPTDEF_READ(currNode,getNextUInt64,value,name,defvalue)

// -------------------------------------------------- Float32 ---------------------------------------------------

// \brief read a required text node entry as Float32
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \remark asserts if the tag has not been found, and issues a return false
#define XMLREAD_FLOAT32_REQ(value,currNode,name)			XMLREAD_REQ_READ(currNode,getNextFloat32,value,name)

// \brief read an optional text node entry as Float32
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns true if the tag has been found or false if not 
#define XMLREAD_FLOAT32_OPT(value,currNode,name)			(currNode->getNextFloat32(value,name))

// \brief read an optional text node entry as Float32 and assign an default value if nothing found
// \param value		value to read from node (return value)
// \param defvalue	default value which will be assigned if nothing found in xmltree
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns nothing
#define XMLREAD_FLOAT32_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_OPTDEF_READ(currNode,getNextFloat32,value,name,defvalue)

// -------------------------------------------------- Vec3f ---------------------------------------------------

// \brief read a required text node entry as Vec3f
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \remark asserts if the tag has not been found, and issues a return false
#define XMLREAD_VEC3F_REQ(value,currNode,name)			XMLREAD_REQ_READ(currNode,getNextVec3f,value,name)

// \brief read an optional text node entry as Vec3f
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns true if the tag has been found or false if not 
#define XMLREAD_VEC3F_OPT(value,currNode,name)			(currNode->getNextVec3f(value,name))

// \brief read an optional text node entry as Vec3f and assign an default value if nothing found
// \param value		value to read from node (return value)
// \param defvalue	default value which will be assigned if nothing found in xmltree
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns nothing
#define XMLREAD_VEC3F_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_OPTDEF_READ(currNode,getNextVec3f,value,name,defvalue)

// -------------------------------------------------- Vec2f ---------------------------------------------------

// \brief read a required text node entry as Vec2f
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \remark asserts if the tag has not been found, and issues a return false
#define XMLREAD_VEC2F_REQ(value,currNode,name)			XMLREAD_REQ_READ(currNode,getNextVec2f,value,name)

// \brief read an optional text node entry as Vec2f
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns true if the tag has been found or false if not 
#define XMLREAD_VEC2F_OPT(value,currNode,name)			(currNode->getNextVec2f(value,name))

// \brief read an optional text node entry as Vec2f and assign an default value if nothing found
// \param value		value to read from node (return value)
// \param defvalue	default value which will be assigned if nothing found in xmltree
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns nothing
#define XMLREAD_VEC2F_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_OPTDEF_READ(currNode,getNextVec2f,value,name,defvalue)

// -------------------------------------------------- Vec2i32 ---------------------------------------------------

// \brief read a required text node entry as Vec2i32
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \remark asserts if the tag has not been found, and issues a return false
#define XMLREAD_VEC2I32_REQ(value,currNode,name)			XMLREAD_REQ_READ(currNode,getNextVec2i32,value,name)

// \brief read an optional text node entry as Vec2i32
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns true if the tag has been found or false if not 
#define XMLREAD_VEC2I32_OPT(value,currNode,name)			(currNode->getNextVec2i32(value,name))

// \brief read an optional text node entry as Vec2i32 and assign an default value if nothing found
// \param value		value to read from node (return value)
// \param defvalue	default value which will be assigned if nothing found in xmltree
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns nothing
#define XMLREAD_VEC2I32_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_OPTDEF_READ(currNode,getNextVec2i32,value,name,defvalue)

// -------------------------------------------------- Bool ---------------------------------------------------

// \brief read a required text node entry as Bool
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \remark asserts if the tag has not been found, and issues a return false
#define XMLREAD_BOOL_REQ(value,currNode,name)			XMLREAD_REQ_READ(currNode,getNextBool,value,name)

// \brief read an optional text node entry as Bool
// \param value		value to read from node (return value)
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns true if the tag has been found or false if not 
#define XMLREAD_BOOL_OPT(value,currNode,name)			(currNode->getNextBool(value,name))

// \brief read an optional text node entry as Bool and assign a default value if nothing found
// \param value		value to read from node (return value)
// \param defvalue	default value which will be assigned if nothing found in xmltree
// \param currNode	current handled xml node
// \param name		name of the text node
// \returns nothing
#define XMLREAD_BOOL_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_OPTDEF_READ(currNode,getNextBool,value,name,defvalue)


// -------------------------------------------------- Attributes -----------------------------------------------

// -------------------------------------------------- String ---------------------------------------------------

// \brief read a required string attribute from a node
// \param value		attribute value (return value)
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns true if attribute found or false if not (and throws assertion)
#define XMLREAD_ATTRIB_STRING_REQ(value,currNode,name)	\
	{ value= currNode->getAttributeString(name); \
	if (!value) { XAIT::Common::Parser::XML::XMLTreeReader::PrintTagNameError(currNode,name); return false;} }

// \brief read an optional string attribute from a node
// \param value		attribute value (return value)
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns true if attribute found or false if not
#define XMLREAD_ATTRIB_STRING_OPT(value,currNode,name)		( (value= currNode->getAttributeString(name)) != NULL)

// \brief read an optional string attribute from a node and assign a default value if nothing found
// \param value		attribute value (return value)
// \param defvalue	default value which will be assigned if nothing found
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns nothing
// \remark the null terminated string which is set, is only a local reference, and will not survive a return statement
#define XMLREAD_ATTRIB_STRING_OPT_DEF(value,defvalue,currNode,name)		\
	{ value= currNode->getAttributeString(name); \
	if (!value) value= defvalue; }


// --------------------------------------------------- Int32 -------------------------------------------------

// some helper macros
#define XMLREAD_ATTRIB_REQ_READ(currNode,getMethode,value,name)	\
	{ if (!currNode->getMethode(value,name) ) \
	{ XAIT::Common::Parser::XML::XMLTreeReader::PrintAttribNameError(currNode,name); return false;} }

#define XMLREAD_ATTRIB_OPTDEF_READ(currNode,getMethode,value,name,defvalue) \
	{ value= defvalue; currNode->getMethode(value,name); }

// \brief read a required int32 attribute from a node
// \param value		attribute value (return value)
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns true if attribute found or false if not (and throws assertion)
#define XMLREAD_ATTRIB_INT32_REQ(value,currNode,name)		XMLREAD_ATTRIB_REQ_READ(currNode,getAttributeInt32,value,name)

// \brief read an optional int32 attribute from a node
// \param value		attribute value (return value)
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns true if attribute found or false if not
#define XMLREAD_ATTRIB_INT32_OPT(value,currNode,name)		(currNode->getAttributeInt32(value,name))

// \brief read an optional int32 attribute from a node and assign a default value if nothing found
// \param value		attribute value (return value)
// \param defvalue	default value which will be assigned if nothing found
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns nothing
#define XMLREAD_ATTRIB_INT32_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_ATTRIB_OPTDEF_READ(currNode,getAttributeInt32,value,name,defvalue)

// --------------------------------------------------- UInt32 -------------------------------------------------

// \brief read a required uint32 attribute from a node
// \param value		attribute value (return value)
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns true if attribute found or false if not (and throws assertion)
#define XMLREAD_ATTRIB_UINT32_REQ(value,currNode,name)		XMLREAD_ATTRIB_REQ_READ(currNode,getAttributeUInt32,value,name)

// \brief read an optional uint32 attribute from a node
// \param value		attribute value (return value)
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns true if attribute found or false if not
#define XMLREAD_ATTRIB_UINT32_OPT(value,currNode,name)		(currNode->getAttributeUInt32(value,name))

// \brief read an optional uint32 attribute from a node and assign a default value if nothing found
// \param value		attribute value (return value)
// \param defvalue	default value which will be assigned if nothing found
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns nothing
#define XMLREAD_ATTRIB_UINT32_OPT_DEF(value,defvalue,currNode,name)		XMLREAD_ATTRIB_OPTDEF_READ(currNode,getAttributeUInt32,value,name,defvalue)

// --------------------------------------------------- Bool -------------------------------------------------

// \brief read a required Bool attribute from a node
// \param value		attribute value (return value)
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns true if attribute found or false if not (and throws assertion)
#define XMLREAD_ATTRIB_BOOL_REQ(value,currNode,name)		XMLREAD_ATTRIB_REQ_READ(currNode,getAttributeBool,value,name)

// \brief read an optional Bool attribute from a node
// \param value		attribute value (return value)
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns true if attribute found or false if not
#define XMLREAD_ATTRIB_BOOL_OPT(value,currNode,name)		(currNode->getAttributeBool(value,name))

// \brief read an optional Bool attribute from a node and assign a default value if nothing found
// \param value		attribute value (return value)
// \param defvalue	default value which will be assigned if nothing found
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns nothing
#define XMLREAD_ATTRIB_BOOL_OPT_DEF(value,defvalue,currNode,name)		XMLREAD_ATTRIB_OPTDEF_READ(currNode,getAttributeBool,value,name,defvalue)


// ----------------------------------------------------- Float32 ------------------------------------------------

// \brief read a required Float32 attribute from a node
// \param value		attribute value (return value)
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns true if attribute found or false if not (and throws assertion)
#define XMLREAD_ATTRIB_FLOAT32_REQ(value,currNode,name)		XMLREAD_ATTRIB_REQ_READ(currNode,getAttributeFloat32,value,name)

// \brief read an optional Float32 attribute from a node
// \param value		attribute value (return value)
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns true if attribute found or false if not
#define XMLREAD_ATTRIB_FLOAT32_OPT(value,currNode,name)		(currNode->getAttributeFloat32(value,name))

// \brief read an optional Float32 attribute from a node and assign a default value if nothing found
// \param value		attribute value (return value)
// \param defvalue	default value which will be assigned if nothing found
// \param currNode	node which has the attribute
// \param name		name of the attribute
// \returns nothing
#define XMLREAD_ATTRIB_FLOAT32_OPT_DEF(value,defvalue,currNode,name)	XMLREAD_ATTRIB_OPTDEF_READ(currNode,getAttributeFloat32,value,name,defvalue)


// ------------------------------------------------- Init Parents ----------------------------------------------


// \brief initialise a parent class and break if no xml definition found for the parentclass
// \param className		class name of the parent class, just the name, without cite tags ""
// \param xmlTag		name of the xmltag for the parentclass with ""
// \param currNode		node in which we should get the xml description of the parentclass
// \remark The macro has no return value. This macro issues return false, if something failed.
#define XMLREAD_PARENTCLASS_REQ(className,xmlTag,currNode)   \
	{ XAIT::Common::Parser::XML::XMLTreeReader subNode; XMLREAD_ELEMENTS_REQ(subNode,currNode,xmlTag); \
	if (!subNode) return false; if (!className::initFromXML(subNode)) return false; }


// \brief initialise a parent class where the initialisation is optional
// \param className		class name of the parent class, just the name, without cite tags ""
// \param xmlTag		name of the xmltag for the parentclass with ""
// \param currNode		node in which we should get the xml description of the parentclass
// \remark The macro has no return value. If xmlnode for the parent was available and the parentclass
//		   failed to init from this node, the macro issues a return false statement.
#define XMLREAD_PARENTCLASS_OPT(className,xmlTag,currNode)	\
	{ XAIT::Common::Parser::XML::XMLTreeReader subNode; if (XMLREAD_ELEMENTS_OPT(subNode,currNode,xmlTag)) \
	if (!className::initFromXML(subNode)) return false; }

// ------------------------------------------------ Init XMLParsable -------------------------------------------

#define XMLREAD_XMLPARSABLE_REQ(value,currNode,name) \
	{ XAIT::Common::Parser::XML::XMLTreeReader subNode= NULL; XMLREAD_ELEMENTS_REQ(subNode,currNode,name); \
	if (!subNode) return false; if (!value.initFromXML(subNode)) return false; }

#define XMLREAD_PTR_XMLPARSABLE_OPT(value,currNode,name) \
	{ XAIT::Common::Parser::XML::XMLTreeReader subNode; if (XMLREAD_ELEMENTS_OPT(subNode,currNode,name)) \
	if (!value->initFromXML(subNode)) return false; }

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				namespace XMLTreeReader
				{
					//! \brief read a required node elements entry
					//! \param subNode		the subnode which contains the node elements (return value)
					//! \param currNode		current handled xmlnode
					//! \param name			name of the node elements tag
					//! \returns returns true if node has been found or false if not (an assertion will be thrown)
					XAIT_COMMON_DLLAPI bool ReadNodeElementsReq(XMLNodeElements*& subNode, XMLNodeElements* currNode, const char* name);

					//! \brief read a optional node elements entry
					//! \param subNode		the subnode which contains the node elements (return value)
					//! \param currNode		current handled xmlnode
					//! \param name			name of the node elements tag
					//! \returns returns true if node has been found or false if not (NO assertion will be thrown)
					XAIT_COMMON_DLLAPI bool ReadNodeElementsOpt(XMLNodeElements*& subNode, XMLNodeElements* currNode, const char* name);

					//! \brief create the hierarchy from a node as string
					//! \param msg	stream where to append the hierarchy
					//! \param node	node from which to compute the hierarchy
					XAIT_COMMON_DLLAPI void CreateNamedNodeHierarchy(std::stringstream& msg, const XMLNode* node);

					//! \brief print an assertion error for a not found tag
					//! \param node			node in which the error occured
					//! \param reqTagName	name of the tag, which has not been found
					XAIT_COMMON_DLLAPI void PrintTagNameError(const XMLNode* node, const char* reqTagName);

					//! \brief print an assertion error for a not found attribute in a tag
					//! \param node				node in which the error occured
					//! \param reqAttribName	name of the attribute, which has not been found
					XAIT_COMMON_DLLAPI void PrintAttribNameError(const XMLNode* node, const char* reqAttribName);

				}

			}
		}
	}

}
