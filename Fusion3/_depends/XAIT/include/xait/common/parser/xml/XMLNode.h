// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/container/List.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/math/Vec2.h>


//! \brief basic xml node
namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				class XMLNode
				{
				public:
					enum ElementData 
					{
						ED_NOTHING,				//!< no elementdata, used for empty elements
						ED_TEXT,				//!< data in the element is text
						ED_ELEMENTS,			//!< data in the element are other elements
						ED_INCLUDE,				//!< include data from another file
					};

					typedef Container::LookUp<const char*,const char*,Container::StringComp>		AttribLU;

					ElementData					type;			//!< type of the node, specified by the containing elementdata
					XMLNode*					parent;			//!< pointer to parent object or NULL if root object
					const char*					name;			//!< name of this node
					AttribLU					attributes;		//!< attributes of the node

					//! \brief constructor for basic xml node
					//! \param type		type of this node
					//! \param name		name of the node, the name pointer will not be copied
					XMLNode(const ElementData type, XMLNode* parent, const char* name, const Memory::Allocator::Ptr& allocator);


					//! add a new attribute (overwriting existing ones)
					//! \param attribute	attribute name, string will not be copied
					//! \param value		value of the attribute, string will not be copied
					XAIT_COMMON_DLLAPI void addAttribute(const char* attribute, const char* value);

					//! get value for a specific attribute as string
					//! \param attribute		attribute name, which should be queried
					//! \returns value of the attribute or NULL if attribute not found
					XAIT_COMMON_DLLAPI const char* getAttributeString(const char* attribute) const;

					//! get value for a specific attribute as signed 32-bit integer
					//! \param attribute	attribute name, which should be queried
					//! \param value		value of the attribute to retrieve (if an error occured, the submitted variable will not be modified)
					//! \returns true if the attribute has been found or false otherwise
					XAIT_COMMON_DLLAPI bool getAttributeInt32(int32& value, const char* attribute) const;

					//! get value for a specific attribute as unsigned 32-bit integer
					//! \param attribute	attribute name, which should be queried
					//! \param value		value of the attribute to retrieve (if an error occured, the submitted variable will not be modified)
					//! \returns true if the attribute has been found or false otherwise
					XAIT_COMMON_DLLAPI bool getAttributeUInt32(uint32& value, const char* attribute) const;

					//! get value for a specific attribute as bool
					//! \param attribute	attribute name, which should be queried
					//! \param value		value of the attribute to retrieve (if an error occured, the submitted variable will not be modified)
					//! \returns true if the attribute has been found or false otherwise
					XAIT_COMMON_DLLAPI bool getAttributeBool(bool& value, const char* attribute) const;

					//! get value for a specific attribute as 32-bit float
					//! \param attribte		attribute name, which should be queried
					//! \param value		value of the attribute to retrieve (if an error occured, the submitted variable will not be modified)
					//! \returns true if the attribute has been found or false otherwise
					XAIT_COMMON_DLLAPI bool getAttributeFloat32(float32& value, const char* attribute) const;

					//! return true if the element is empty
					XAIT_COMMON_DLLAPI bool isEmpty();

				};


				//! \brief xml node for text only elements
				class XMLNodeText : public XMLNode
				{
				public:
					const char*		text;		//!< text in this node/element , nullterminated string


					//! \brief constructor for text node
					//! \param name		name of the node, the name pointer will not be copied
					//! \param text		nullterminated string to the text in this node, the content of the pointer will not be copied
					XAIT_COMMON_DLLAPI XMLNodeText(XMLNode* parent, const char* name, const char* text, const Memory::Allocator::Ptr& allocator);

					//! \brief get text as 32-bit integer
					//! \returns converted 32-bit integer value from text
					XAIT_COMMON_DLLAPI int32 getInt32() const;

					//! \brief get text as 32-bit unsigned integer
					//! \returns converted 32-bit unsigned integer value from text
					XAIT_COMMON_DLLAPI uint32 getUInt32() const;

					//! \brief get text as 64-bit integer
					//! \returns converted 64-bit integer value from text
					XAIT_COMMON_DLLAPI int64 getInt64() const;

					//! \brief get text as 64-bit unsigned integer
					//! \returns converted 64-bit unsigned integer value from text
					XAIT_COMMON_DLLAPI uint64 getUInt64() const;

					//! \brief get text as 32-bit float
					//! \returns converted 32-bit float value from text
					XAIT_COMMON_DLLAPI float32 getFloat32() const;

					//! \brief get text as 64-bit float
					//! \returns converted 64-bit float value from text
					XAIT_COMMON_DLLAPI float64 getFloat64() const;

					//! \brief get text as vector
					//! \returns converted vec3f from text
					XAIT_COMMON_DLLAPI Math::Vec3f getVec3f() const;

					//! \brief get text as vector
					//! \returns converted vec3f from text
					XAIT_COMMON_DLLAPI Math::Vec3i32 getVec3i32() const;

					//! \brief get text as vector
					//! \returns converted vec3f from text
					XAIT_COMMON_DLLAPI Math::Vec3u32 getVec3u32() const;

					//! \brief get text as vector
					//! \returns converted vec2f from text
					XAIT_COMMON_DLLAPI Math::Vec2f getVec2f() const;

					//! \brief get text as vector
					//! \returns converted vec2f from text
					XAIT_COMMON_DLLAPI Math::Vec2i32 getVec2i32() const;

					//! \brief get text as vector
					//! \returns converted vec2f from text
					XAIT_COMMON_DLLAPI Math::Vec2u32 getVec2u32() const;

					//! \brief get text as bool value
					//! \returns converted bool from text
					//! \remark accepted textstrings are true and false (non-case sensitiv) and 0 and 1
					XAIT_COMMON_DLLAPI bool getBool() const;

				};


				//! \brief xml node for elements which contain only elements
				class XMLNodeElements : public XMLNodeText
				{
				public:
					typedef Container::List<XMLNode*>	ElementList;	
					ElementList						elements;		//!< other elements in this node

				private:
					ElementList::Iterator			pos;			//!< position of current iteration

				public:

					//! \brief constructor for elements node
					//! \param name		name of the node, the name pointer will not be copied
					XAIT_COMMON_DLLAPI XMLNodeElements(XMLNode* parent, const char* name, const Memory::Allocator::Ptr& allocator);

					//! \brief add new element to this node
					//! \param element	element to add to this node
					//! \remark addition of new elements, will reset the search
					XAIT_COMMON_DLLAPI void addElement(XMLNode* element);

					//! \brief remove the last added element
					//! \returns the removed element
					XAIT_COMMON_DLLAPI XMLNode* removeLastElement();

					//! \brief get the last added element
					//! \returns the last added element
					XAIT_COMMON_DLLAPI XMLNode* getLastElement() const;

					//! init element search resets the interal iteration position to the first element of the node
					//! this function must be called before one of the getNext.. functions can be used
					//! \brief init element search
					XAIT_COMMON_DLLAPI void initSearch();

					//! Get the next element with a specific name. If the next element in the list is not the requested one
					//! the iteration position will NOT be enhanced.
					//! \brief get the next element with a specific name
					//! \param name		name of the element to look for
					//! \returns pointer to the element or NULL if nothing found
					XAIT_COMMON_DLLAPI XMLNode* getNextNode(const char* name);

					//! Get the next element.
					//! \brief get the next element
					//! \returns pointer to the element or NULL if nothing found
					XAIT_COMMON_DLLAPI XMLNode* getNextNode();

					//! \brief Get the next element with a specific name as node element
					//! \param name		name of the element to look for
					//! \returns pointer to the nodeelement or NULL if an error occured (no nodeelement or no further nodes)
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI XMLNodeElements* getNextElements(const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with text
					//! \param text		the searched content of the node (the returned pointer is no copy of the nodecontent)
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextText(const char*& text, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with int32
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextInt32(int32& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with uint32
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextUInt32(uint32& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with int64
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextInt64(int64& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with uint64
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextUInt64(uint64& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with float32
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextFloat32(float32& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with float64
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextFloat64(float64& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with vec3f
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextVec3f(Math::Vec3f& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with vec3i32
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextVec3i32(Math::Vec3i32& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with vec3u32
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextVec3u32(Math::Vec3u32& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with vec2f
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextVec2f(Math::Vec2f& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with vec2i32
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextVec2i32(Math::Vec2i32& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with vec2u32
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextVec2u32(Math::Vec2u32& value, const char* name);

					//! \brief Get the next element with a specific name and evaluate to a textnode with bool (true,false,0,1)
					//! \param value	the searched content of the node
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function iterates over the elements, like XMLNode::getNextNode
					XAIT_COMMON_DLLAPI bool getNextBool(bool& value, const char* name);

					//! \brief Check if the next node is empty
					//! \param name		name of the element to look for
					//! \returns true if correct element has been found, or false if not
					//! \remark This function does NOT iterate over the elements
					XAIT_COMMON_DLLAPI bool checkNextEmpty(const char* name);

					//! \brief Check if the actual node is the last node
					//! \remark This function does NOT iterate over the elements
					XAIT_COMMON_DLLAPI bool atLastNode();

					//! \brief get a pointer to the current handled node
					//! \returns the pointer to the current node, or NULL if NodeElements empty
					//! \remark this is only valid, if you made a call to getNextNode or getNext...
					XAIT_COMMON_DLLAPI XMLNode* getCurrentNode() const;
				};


				//! \brief xml node for elements with empty data
				class XMLNodeEmpty : public XMLNodeElements
				{
				public:
					//! \brief constructor for node with no element data
					//! \param name		name of the node, the name pointer will not be copied
					XMLNodeEmpty(XMLNode* parent, const char* name, const Memory::Allocator::Ptr& allocator)
						: XMLNodeElements(parent,name,allocator)
					{
						type= XMLNode::ED_NOTHING;
					}
				};
			}
		}
	}
}
