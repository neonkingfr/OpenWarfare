// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/String.h>
#include <xait/common/parser/xml/XMLNode.h>


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				template<typename DATA_T> 
				class XMLNodeDataReader
				{
				};


#define XMLNODEREAD_HELPER_FUNDDATA(dataType,getMethod,getMethodType)	\
				template<>\
				class XMLNodeDataReader<dataType>\
				{ public:\
					static inline bool readNextNode(dataType& readValue, const Common::String& nodeName, XMLNodeElements* parentNode)\
					{\
						getMethodType tmp;\
						const bool retVal= parentNode->getMethod(tmp,nodeName.getConstCharPtr());\
						readValue= (dataType)tmp;\
						return retVal;\
					}\
				};

				XMLNODEREAD_HELPER_FUNDDATA(bool,getNextBool,bool);

				XMLNODEREAD_HELPER_FUNDDATA(uint08,getNextUInt32,uint32);
				XMLNODEREAD_HELPER_FUNDDATA(int08,getNextInt32,int32);
				XMLNODEREAD_HELPER_FUNDDATA(uint16,getNextUInt32,uint32);
				XMLNODEREAD_HELPER_FUNDDATA(int16,getNextInt32,int32);
				XMLNODEREAD_HELPER_FUNDDATA(uint32,getNextUInt32,uint32);
				XMLNODEREAD_HELPER_FUNDDATA(int32,getNextInt32,int32);
				XMLNODEREAD_HELPER_FUNDDATA(uint64,getNextUInt64,uint64);
				XMLNODEREAD_HELPER_FUNDDATA(int64,getNextInt64,int64);

				XMLNODEREAD_HELPER_FUNDDATA(float32,getNextFloat32,float32);
				XMLNODEREAD_HELPER_FUNDDATA(float64,getNextFloat64,float64);

				XMLNODEREAD_HELPER_FUNDDATA(Common::String,getNextText,const char*);
				XMLNODEREAD_HELPER_FUNDDATA(Common::Math::Vec2f,getNextVec2f,Common::Math::Vec2f);
				XMLNODEREAD_HELPER_FUNDDATA(Common::Math::Vec3f,getNextVec3f,Common::Math::Vec3f);

			}
		}
	}
}