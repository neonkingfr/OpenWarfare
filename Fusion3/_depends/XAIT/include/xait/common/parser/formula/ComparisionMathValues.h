// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/formula/OperatorMathValue.h>


#define COMPARISON_MATH_VALUE(name,operation,enumName) \
	template<typename T_ARG_TYPE1,typename T_ARG_TYPE2> \
class name##MathValue : public OperatorMathValue<bool,T_ARG_TYPE1,T_ARG_TYPE2> \
	{ \
	typedef name##MathValue<T_ARG_TYPE1,T_ARG_TYPE2>	MyType; \
	public: \
	name##MathValue(TypedMathValue<T_ARG_TYPE1>* leftHandSide,TypedMathValue<T_ARG_TYPE2>* rightHandSide) \
	: OperatorMathValue<bool,T_ARG_TYPE1,T_ARG_TYPE2>(leftHandSide,rightHandSide) \
		{} \
		\
		virtual void evaluate(bool& value,VariableAccessor* varAccessor) \
		{ \
		T_ARG_TYPE1 value1; \
		T_ARG_TYPE2 value2; \
		\
		(MyType::mLeftHandSide)->evaluate(value1,varAccessor); \
		(MyType::mRightHandSide)->evaluate(value2,varAccessor); \
		\
		value = (value1 operation (T_ARG_TYPE1)value2); \
		} \
		\
		virtual Expression::Operation getOperationType() \
		{ \
		return Expression::enumName; \
		} \
		\
		virtual MathValue* clone() const \
		{ \
		return new(OperatorMathValue<bool,T_ARG_TYPE1,T_ARG_TYPE2>::mAllocator) name##MathValue((TypedMathValue<T_ARG_TYPE1>*)(OperatorMathValue<bool,T_ARG_TYPE1,T_ARG_TYPE2>::mLeftHandSide)->clone(),(TypedMathValue<T_ARG_TYPE2>*)(OperatorMathValue<bool,T_ARG_TYPE1,T_ARG_TYPE2>::mRightHandSide)->clone()); \
		} \
		\
		virtual bool compare(MathValue* mathValue) const \
		{ \
		if (getMathValueTypeID() != mathValue->getMathValueTypeID()) \
		return false; \
		MyType* other = (MyType*)(mathValue); \
		\
		return (((MyType::mLeftHandSide)->compare(((name##MathValue<T_ARG_TYPE1,T_ARG_TYPE2>*)other)->mLeftHandSide) \
		&& (MyType::mRightHandSide)->compare(((name##MathValue<T_ARG_TYPE1,T_ARG_TYPE2>*)other)->mRightHandSide))); \
		} \
		virtual Common::String convertToString() const \
		{ \
		return (MyType::mLeftHandSide)->convertToString() + Common::String(#operation) + (MyType::mRightHandSide)->convertToString(); \
		} \
	protected: \
		XAIT_MATHVALUE_TYPE_FUNCTIONS(MyType); \
	};  \
	\
class Untyped##name##MathValue : public UntypedComparisionMathValue\
	{ \
	typedef Untyped##name##MathValue	MyType; \
	public: \
	Untyped##name##MathValue(const Reflection::Datatype::DatatypeID datatypeID,MathValue* leftHandSide,MathValue* rightHandSide) \
	: UntypedComparisionMathValue(datatypeID,leftHandSide,rightHandSide) \
		{} \
		\
		virtual void evaluate(bool& value,VariableAccessor* varAccessor) \
		{ \
		evaluateToPtr(&value,varAccessor); \
		} \
		\
		virtual void evaluateToPtr(void* value,VariableAccessor* varAccessor) \
		{ \
		mLeftHandSide->evaluateToPtr(mOwnerValue,varAccessor); \
		mRightHandSide->evaluateToPtr(mArgumentValue,varAccessor); \
		mDatatype->call##name(value,mOwnerValue,mArgumentValue); \
		} \
		const Common::String getOperationName() const \
		{ \
		return #name; \
		} \
		virtual Expression::Operation getOperationType() \
		{ \
		return Expression::enumName; \
		} \
		\
		virtual MathValue* clone() const \
		{ \
		return new(mAllocator) Untyped##name##MathValue(mDatatype->getDatatypeID(),mLeftHandSide->clone(), mRightHandSide->clone()); \
		} \
		\
		virtual bool compare(MathValue* mathValue) const \
		{ \
		if (getMathValueTypeID() != mathValue->getMathValueTypeID()) \
		return false; \
		MyType* other = (MyType*)(mathValue); \
		if (mDatatype != other->mDatatype) \
		return false; \
		\
		return ((mLeftHandSide->compare(other->mLeftHandSide) && mRightHandSide->compare(other->mRightHandSide))); \
		} \
		virtual Common::String convertToString() const \
		{ \
		return mLeftHandSide->convertToString() + Common::String(#operation) + mRightHandSide->convertToString(); \
		} \
	protected: \
		XAIT_MATHVALUE_TYPE_FUNCTIONS(MyType); \
	};  \

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{

				class UntypedComparisionMathValue : public TypedMathValue<bool>
				{
				public:
					UntypedComparisionMathValue(const Reflection::Datatype::DatatypeID datatypeID,MathValue* leftHandSide,MathValue* rightHandSide);

					virtual ~UntypedComparisionMathValue();

					virtual bool isConstant();

					virtual void getContainingVariables(Common::Container::Vector<uint32>& variables) const
					{
						mLeftHandSide->getContainingVariables(variables);
						mRightHandSide->getContainingVariables(variables);
					}

					virtual Reflection::Datatype::DatatypeID getDatatypeID() const;

					//! \see MathValue
					virtual MathValue* merge();

					virtual Reflection::Datatype::DatatypeID getLeftDatatypeID()
					{
						return mLeftHandSide->getDatatypeID(); 
					}

					virtual Reflection::Datatype::DatatypeID getRightDatatypeID()
					{
						return mRightHandSide->getDatatypeID();
					}

					virtual void replaceVariableAccessID(uint32 oldID, uint32 newID)
					{
						mLeftHandSide->replaceVariableAccessID(oldID,newID);
						mRightHandSide->replaceVariableAccessID(oldID,newID);
					}

					Reflection::Datatype::DatatypeBase*		mDatatype;
					MathValue*								mLeftHandSide;
					MathValue*								mRightHandSide;
					void*									mOwnerValue;
					void*									mArgumentValue;

				protected: 
					XAIT_MATHVALUE_TYPE_FUNCTIONS(UntypedComparisionMathValue); 
				};
#if XAIT_OS == XAIT_OS_WIN
#pragma warning(push)
#pragma warning(disable: 4244)
#pragma warning(disable: 4018)
#endif
				COMPARISON_MATH_VALUE(Smaller,<,OP_SMALLER);
				COMPARISON_MATH_VALUE(SmallerEqual,<=,OP_SMALLEREQUAL);
				COMPARISON_MATH_VALUE(Equal,==,OP_EQUAL);
				COMPARISON_MATH_VALUE(GreaterEqual,>=,OP_GREATEREQUAL);
				COMPARISON_MATH_VALUE(Greater,>,OP_GREATER);
				COMPARISON_MATH_VALUE(Unequal,!=,OP_UNEQUAL);
#if XAIT_OS == XAIT_OS_WIN
#pragma warning(pop)
#endif
			}
		}
	}
}
