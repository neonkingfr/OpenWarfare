// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec2.h>
#include <xait/common/clr/Marshalling.h>

// ---------------------------------------------------------------------------------------------------------
//							Vector class for two int32
//
//	Vector class must stay identical to XAIT::Common::Math::Vec2 in means of attributes and virtual methods
//	such that the cast works properly.
//
//	Author:		Daniel G�rgen
//
// ---------------------------------------------------------------------------------------------------------


namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			//! \brief template class for vectors with 2 components
			//! \param T_COMP	type of the components (must be a number type)
			public value class Vec2i32
			{
			private:
				int32	mX;		//!< component in x
				int32	mY;		//!< component in y

			public:
				property int32 X
				{
					int32 get() { return mX; }
					void set(const int32 val) { mX= val; }
				}

				property int32 Y
				{
					int32 get() { return mY; }
					void set(const int32 val) { mY= val; }
				}

				//! \brief get a component by number (0-x,1-y)
				//! \remark Not so fast as direct member access
				property int32 default[unsigned int]
				{
					inline int32 get(unsigned int i)
					{
						X_ASSERT_MSG_DBG(i < 2,"component index out of range");

						return *(&mX + i);
					}

					inline void set(unsigned int i, int32 value)
					{
						X_ASSERT_MSG_DBG(i < 2,"component index out of range");

						*(&mX + i) = value;
					}
				}


				//! \brief Element constructor
				//! \param x	x component
				//! \param y	y component
				Vec2i32(const int32 x, const int32 y)
					:mX(x),mY(y)
				{}


				//! \brief Test if two vectors are equal
				//! \remark Vectors are equal if components are equal
				inline bool operator== (const Vec2i32 a)
				{
					return (a.mX == mX && a.mY == mY);
				}

				//! \brief Test if two vectors are unequal
				//! \remark Vectors are unequal if components are unequal
				inline bool operator!= (const Vec2i32 a)
				{
					return (a.mX != mX || a.mY != mY);
				}


			};	// class Vec2i32
		}	// namespace Math
	}	// namespace CommonCLR
}	// namespace XAIT

XAIT_NETCONVERT_DEFINE_VALUETYPE_CAST(XAIT::Common::Math::Vec2i32,XAIT::Common::CLR::Vec2i32);
