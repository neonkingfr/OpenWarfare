// (C) xaitment GmbH 2006-2012

#pragma once 

#include <assert.h>
#include <xait/common/math/MathDefines.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/clr/Marshalling.h>
#include <xait/common/clr/Vec2f.h>

// ---------------------------------------------------------------------------------------------------------
//							Vector class for three doubles
//
//	Vector class must stay identical to XAIT::Common::Math::Vec3 in means of attributes and virtual methods
//	such that the cast works properly.
//
//	Author:		Markus Wilhelm
//
// ---------------------------------------------------------------------------------------------------------


namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			public value class Vec3d
			{
			private:
				float64	mX;
				float64	mY;
				float64	mZ;
			public:

				property float64 X
				{
					float64 get() { return mX; }
					void set(const float64 val) { mX= val; }
				}

				property float64 Y
				{
					float64 get() { return mY; }
					void set(const float64 val) { mY= val; }
				}

				property float64 Z
				{
					float64 get() { return mZ; }
					void set(const float64 val) { mZ= val; }
				}


				//! \brief get a component by number (0-x,1-y,2-z)
				//! \remark Not so fast as direct member access
				property float64 default[unsigned int]
				{
					inline float64 get(unsigned int i)
					{
						assert( i < 3 );

						return *(&mX + i);
					}

					inline void set(unsigned int i, float64 value)
					{
						assert( i < 3 );

						*(&mX + i) = value;
					}
				}

				//! \brief get vector as float array
				inline array<float64>^ GetArrayCopy()
				{
					array<float64>^ tmp= gcnew array<float64>(3);
					tmp[0]= mX;
					tmp[1]= mY;
					tmp[2]= mZ;

					return tmp;
				}


				//! \brief Init vector from single components
				//! \param x	x component
				//! \param y	y component
				//! \param z	z component
				inline Vec3d(float64 x, float64 y, float64 z)
					: mX(x),mY(y),mZ(z)
				{}

				//! \brief Init vector from array
				inline Vec3d(array<float64>^ vec)
					: mX(vec[0]),mY(vec[1]),mZ(vec[2])
				{}


				//! \brief Test if two vectors are equal
				//! \remark Vectors are equal if components are equal
				inline bool operator==(const Vec3d p) 
				{ 
					return (mX == p.mX && mY == p.mY && mZ == p.mZ); 
				}

				//! \brief Test if two vectors are unequal
				//! \remark Vectors are unequal if components are unequal
				inline bool operator!=(const Vec3d p) 
				{ 
					return (mX != p.mX || mY != p.mY || mZ != p.mZ); 
				}
			};
		}
	}
}

XAIT_NETCONVERT_DEFINE_VALUETYPE_CAST(XAIT::Common::Math::Vec3d,XAIT::Common::CLR::Vec3d);
