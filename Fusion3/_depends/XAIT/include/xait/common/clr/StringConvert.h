// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/String.h>
#include <xait/common/clr/Marshalling.h>
#include < vcclr.h >

namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			template<>
			class WrapperType<XAIT::Common::WString >
			{
			public:
				typedef XAIT::Common::WString	Native;
				typedef System::String^			Managed;
			};

			template<>
			class WrapperType<System::String^ >
			{
			public:
				typedef XAIT::Common::WString	Native;
				typedef System::String^			Managed;
			};

			XAIT_FORCEINLINE XAIT::Common::WString NETConvert(const System::String^ str)
			{
				pin_ptr<const wchar_t> wch = PtrToStringChars(str);
				return XAIT::Common::WString(wch);
			}

			XAIT_FORCEINLINE System::String^ NETConvert(const XAIT::Common::WString& str)
			{
				return gcnew System::String(str.getConstCharPtr());
			}
		}
	}
}
