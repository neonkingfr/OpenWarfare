// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/Vector.h>
#include <xait/common/clr/Marshalling.h>
#include <xait/common/clr/FundamentalTypesConvert.h>

namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			template<class T_MANAGED, class T_NATIVE= WrapperType<T_MANAGED>::Native>
			public ref class VectorCLR : System::Collections::Generic::IList<T_MANAGED>
			{
				Container::Vector<T_NATIVE>* mNative;

			public:

				ref class Enumerator : System::Collections::Generic::IEnumerator<T_MANAGED>
				{
				private:
					typename Container::Vector<T_NATIVE>::Iterator* mIter;
					Container::Vector<T_NATIVE>* mVec;

				public:
					virtual property System::Object^ CurrentUntyped
					{
						System::Object^ get () = System::Collections::IEnumerator::Current::get
						{
							return NETConvert(*(*mIter));
						}
					}

					virtual property T_MANAGED Current
					{
						T_MANAGED get () = System::Collections::Generic::IEnumerator<T_MANAGED>::Current::get
						{
							return NETConvert(*(*mIter));
						}
					}


					virtual bool MoveNext ()
					{
						if (mIter == NULL)
						{
							if (mVec->empty())
								return false;

							mIter= new Container::Vector<T_NATIVE>::Iterator(mVec->begin());
							return true;
						}
						else
						{
							++(*mIter);
							if ((*mIter) == mVec->end())
								return false;
							else
								return true;
						}
					}

					virtual void Reset ()
					{
						if (mIter)
							delete mIter;
					}

					Enumerator(Container::Vector<T_NATIVE>* vec)
						: mVec(vec),mIter(NULL)
					{}

					~Enumerator()
					{
						if (mIter)
							delete mIter;
					}
				};


				VectorCLR(Container::Vector<T_NATIVE>& native)
					: mNative(&native)
				{
				}


#pragma region IList<T> Members

				virtual int IndexOf(T_MANAGED item)
				{
					throw gcnew System::NotImplementedException("Index of not implemented");
					//T_NATIVE native= NETConvert(item);

					//for(uint32 i= 0; i < mNative->size(); ++i)
					//{
					//	if ((*mNative)[i] == native)
					//		return i;
					//}
					//return -1;
				}

				virtual void Insert(int index, T_MANAGED item)
				{
					mNative->insert(index,NETConvert(item));
				}

				virtual void RemoveAt(int index)
				{
					mNative->erase(index);
				}

				virtual property T_MANAGED default[int]
				{
					T_MANAGED get(int index)
					{
						return NETConvert((*mNative)[index]);
					}
					void set(int index, T_MANAGED val)
					{
						(*mNative)[index] = NETConvert(val);
					}
				}

#pragma endregion

#pragma region ICollection<T> Members

				virtual void Add(T_MANAGED item)
				{
					mNative->pushBack(NETConvert(item));
				}

				virtual void Clear()
				{
					mNative->clear();
				}

				virtual bool Contains(T_MANAGED item)
				{
					return IndexOf(item) != -1;
				}

				virtual void CopyTo(array<T_MANAGED>^ dst, int arrayIndex)
				{
					throw gcnew System::NotImplementedException("CopyTo");
				}

				virtual property int Count
				{
					int get()
					{ 
						return mNative->size(); 
					}
				}

				virtual property bool IsReadOnly
				{
					bool get()
					{ 
						return false; 
					}
				}

				virtual bool Remove(T_MANAGED item)
				{
					int index= IndexOf(item);
					if (index < 0)
						return false;

					RemoveAt(index);
					return true;
				}

#pragma endregion


#pragma region IEnumerable<T> Members

				virtual System::Collections::Generic::IEnumerator<T_MANAGED>^ GetEnumerator() = System::Collections::Generic::IEnumerable<T_MANAGED>::GetEnumerator
				{
					return gcnew Enumerator(mNative);
				}

#pragma endregion

#pragma region IEnumerable Members

				virtual System::Collections::IEnumerator^ GetEnumeratorOld() = System::Collections::IEnumerable::GetEnumerator
				{
					return gcnew Enumerator(mNative);
				}

#pragma endregion


			};

		}
	}
}