// (C) xaitment GmbH 2006-2012

#pragma once 

#include <assert.h>
#include <xait/common/math/MathDefines.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/clr/Marshalling.h>
#include <xait/common/clr/Vec2f.h>

// ---------------------------------------------------------------------------------------------------------
//							Vector class for three doubles
//
//	Vector class must stay identical to XAIT::Common::Math::Vec3 in means of attributes and virtual methods
//	such that the cast works properly.
//
//	Author:		Markus Wilhelm
//
// ---------------------------------------------------------------------------------------------------------


namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			public value class Vec3i32
			{
			private:
				int32	mX;
				int32	mY;
				int32	mZ;
			public:

				property int32 X
				{
					int32 get() { return mX; }
					void set(const int32 val) { mX= val; }
				}

				property int32 Y
				{
					int32 get() { return mY; }
					void set(const int32 val) { mY= val; }
				}

				property int32 Z
				{
					int32 get() { return mZ; }
					void set(const int32 val) { mZ= val; }
				}


				//! \brief get a component by number (0-x,1-y,2-z)
				//! \remark Not so fast as direct member access
				property int32 default[unsigned int]
				{
					inline int32 get(unsigned int i)
					{
						assert( i < 3 );

						return *(&mX + i);
					}

					inline void set(unsigned int i, int32 value)
					{
						assert( i < 3 );

						*(&mX + i) = value;
					}
				}

				//! \brief get vector as float array
				inline array<int32>^ GetArrayCopy()
				{
					array<int32>^ tmp= gcnew array<int32>(3);
					tmp[0]= mX;
					tmp[1]= mY;
					tmp[2]= mZ;

					return tmp;
				}


				//! \brief Init vector from single components
				//! \param x	x component
				//! \param y	y component
				//! \param z	z component
				inline Vec3i32(int32 x, int32 y, int32 z)
					: mX(x),mY(y),mZ(z)
				{}

				//! \brief Init vector from array
				inline Vec3i32(array<int32>^ vec)
					: mX(vec[0]),mY(vec[1]),mZ(vec[2])
				{}


				//! \brief Test if two vectors are equal
				//! \remark Vectors are equal if components are equal
				inline bool operator==(const Vec3i32 p) 
				{ 
					return (mX == p.mX && mY == p.mY && mZ == p.mZ); 
				}

				//! \brief Test if two vectors are unequal
				//! \remark Vectors are unequal if components are unequal
				inline bool operator!=(const Vec3i32 p) 
				{ 
					return (mX != p.mX || mY != p.mY || mZ != p.mZ); 
				}
			};
		}
	}
}

XAIT_NETCONVERT_DEFINE_VALUETYPE_CAST(XAIT::Common::Math::Vec3i32,XAIT::Common::CLR::Vec3i32);
