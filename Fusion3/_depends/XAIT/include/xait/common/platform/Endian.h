// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/platform/PlatformDefines.h>
#include <xait/common/DLLDefines.h>
#include <xait/common/FundamentalTypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Platform
		{
			template<int NUM_BYTES>
			class EndianSwapBytes;

			//! \brief class contains methods for endian coding changing/query
			//! \remark All methods support up to 8 byte long datatypes
			class Endian
			{
			public:
				enum Endiness
				{
					ENDIAN_LITTLE = XAIT_ARCH_ENDIAN_LITTLE,	//!< little endian encoding
					ENDIAN_BIG = XAIT_ARCH_ENDIAN_BIG,			//!< big endian encoding
				};

				//! \brief get the encoding of the current system
				XAIT_COMMON_DLLAPI static Endiness getSystemEncoding();

				//! \brief convert system to little endian
				//! \param src	src value to by converted
				//! \param dst	dst variable where to write converted bytes
				//! \remark The function is its own inverse function.
				template<typename T>
				inline static void convertSystemToLittle(T& dst, const T& src)
				{
#if XAIT_ARCH_ENDIAN == XAIT_ARCH_ENDIAN_LITTLE
					copyBytes(dst,src);
#else
					swapBytes(dst,src);
#endif
				}

				//! \brief convert system to little endian
				//! \param src	src value to by converted
				//! \param dst	dst variable where to write converted bytes
				//! \remark The function is its own inverse function.
				template<typename T>
				inline static void convertLittleToSystem(T& dst, const T& src)
				{
#if XAIT_ARCH_ENDIAN == XAIT_ARCH_ENDIAN_LITTLE
					copyBytes(dst,src);
#else
					swapBytes(dst,src);
#endif
				}


				//! \brief convert system to big endian
				//! \param src	src value to by converted
				//! \param dst	dst variable where to write converted bytes
				//! \remark The function is its own inverse function.
				template<typename T>
				inline static void convertSystemToBig(T& dst, const T& src)
				{
#if XAIT_ARCH_ENDIAN == XAIT_ARCH_ENDIAN_BIG
					copyBytes(dst,src);
#else
					swapBytes(dst,src);
#endif
				}

				//! \brief convert system to big endian
				//! \param src	src value to by converted
				//! \param dst	dst variable where to write converted bytes
				//! \remark The function is its own inverse function.
				template<typename T>
				inline static void convertBigToSystem(T& dst, const T& src)
				{
#if XAIT_ARCH_ENDIAN == XAIT_ARCH_ENDIAN_BIG
					copyBytes(dst,src);
#else
					swapBytes(dst,src);
#endif
				}

				//! \brief convert system to parameter endian 
				//! \param src	src value to by converted
				//! \param dst	dst variable where to write converted bytes
				//! \remark The function is its own inverse function.
				//! \remark Slower as hard coded converts since each convert needs a compare
				template<typename T>
				inline static void convertToSystem(T& dst, const T& src, const Endiness encoding)
				{
					if (encoding != (Endiness)XAIT_ARCH_ENDIAN)
					{
						swapBytes(dst,src);
					}
					else
					{
						copyBytes(dst,src);
					}
				}

				//! \brief convert system to parameter endian 
				//! \param src	src value to by converted
				//! \param dst	dst variable where to write converted bytes
				//! \remark The function is its own inverse function.
				//! \remark Slower as hard coded converts since each convert needs a compare
				template<typename T>
				inline static void convertSystemTo(T& dst, const T& src, const Endiness encoding)
				{
					if (encoding != (Endiness)XAIT_ARCH_ENDIAN)
					{
						swapBytes(dst,src);
					}
					else
					{
						copyBytes(dst,src);
					}
				}

				//! \brief convert from one representation to the other
				template<typename T>
				inline static void swapBytes(T& dst, const T& src)
				{
					const typename EndianSwapBytes<sizeof(T)>::Type* srcInt= ((const typename EndianSwapBytes<sizeof(T)>::Type*)&src);
					typename EndianSwapBytes<sizeof(T)>::Type* const dstInt= ((typename EndianSwapBytes<sizeof(T)>::Type*)&dst);
					*dstInt= EndianSwapBytes<sizeof(T)>::swap(*srcInt);
				}

			private:

				template<typename T>
				inline static void copyBytes(T& dst, const T& src)
				{
					memcpy(&dst,&src,sizeof(T));
				}
			};


			template<>
			class EndianSwapBytes<1>
			{
			public:
				typedef int08	Type;
				static inline int08 swap(const int08 i)
				{
					return i;
				}
			};

			template<>
			class EndianSwapBytes<2>
			{
			public:
				typedef int16	Type;
				static inline int16 swap(const int16 i)
				{
					return ((i>>8)&0xff)+((i << 8)&0xff00);
				}
			};

			template<>
			class EndianSwapBytes<4>
			{
			public:
				typedef int32	Type;
				static inline int32 swap(const int32 i)
				{
					return((i&0xff)<<24)+((i&0xff00)<<8)+((i&0xff0000)>>8)+((i>>24)&0xff);
				}
			};

			template<>
			class EndianSwapBytes<8>
			{
			public:
				typedef int64	Type;
				static inline int64 swap(const int64 i)
				{
					return((i&0xff)<<56)+((i&0xff00)<<40)+((i&0xff0000)<<24)+((i&0xFF000000)<<8)+((i>>8)&0xFF000000)+((i>>24)&0xFF0000)+((i>>40)&0xFF00)+((i>>56)&0xFF);
				}
			};
		}
	}
}
