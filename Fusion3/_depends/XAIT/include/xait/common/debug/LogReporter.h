// (C) xaitment GmbH 2006-2012

#pragma  once

#include <xait/common/String.h>
#include <xait/common/debug/LogType.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			class LogManager;

			class LogReporter
			{
			public:
				//! \brief report a log 
				void XAIT_COMMON_DLLAPI reportLog(const LogType logType, const Common::String& logMessage, const Common::String& functionName, const Common::String& fileName, const uint32 lineNumber) const;

				//! \brief enable/disable log types
				//! \param logTypes		ored list of log types which should be enabled
				inline void setLogTypes(const LogType logTypes)
				{
					mEnabledTypes= (uint32)logTypes;
				}

				//! \brief get the log types
				//! \returns an ored list of logtypes which are enabled
				inline LogType getLogTypes() const
				{
					return (LogType)mEnabledTypes;
				}

				//! \brief test if a certain log type is enabled
				//! \returns true if the log type is enabled, false otherwise
				inline bool hasLogTypeEnabled(const LogType logType) const
				{
					return (mEnabledTypes & (uint32)logType) != 0;	// added != 0 to disable performance warning C4800
				}

				//! \brief enable/disable one log type
				//! \param logType	log type which should be enabled/disabled
				//! \param enable	if true log type will be enabled, if false, it will be disabled
				void XAIT_COMMON_DLLAPI enableLogType(const LogType logType, bool enable);

				//! \brief enable/disable log types on which an assertion should be thrown
				//! \param logTypes		ored list of log types which should issue an assert
				inline void setAssertTypes(const LogType logType)
				{
					mAssertTypes= (uint32)logType;
				}

				//! \brief get the log types on which assertions occur
				//! \returns an ored list of log types which trigger an assertion
				inline LogType getAssertTypes() const
				{
					return (LogType)mAssertTypes;
				}

				//! \brief enable/disable one log type to raise assertions
				//! \param logType	log type which should raise an assertion
				//! \param enable	if true log type will raise assertions, if false it will not.
				void XAIT_COMMON_DLLAPI enableAssertType(const LogType logType, bool enable);

				//! \brief get the name of the log reporter instance
				inline const Common::String& getName() const
				{
					return mInstanceName;
				}

			private:
				friend class LogManager;

				uint32				mEnabledTypes;
				uint32				mAssertTypes;
				Common::String		mInstanceName;
				LogManager*			mLogManager;

				//! \brief constructor for a log reporter
				//! \param instanceName		name of the logreporter instance
				//! \param logManager		pointer to the log manager maintaining this log reporter
				LogReporter(const Common::String& instanceName, LogManager* logManager)
					: mEnabledTypes(LT_LOG_ALL),mAssertTypes(LT_LOG_NONE),mInstanceName(instanceName),mLogManager(logManager)
				{}

				//! \brief destructor made private so only log manager can delete the object
				~LogReporter() {};

			};

		}
	}
}
