// (C) xaitment GmbH 2006-2012

#pragma once

//////////////////////////////////////////////////////////////////////////
// Defines for easy usage of the log facility
//////////////////////////////////////////////////////////////////////////

#include <xait/common/Platform.h>
#include <xait/common/debug/LogReporter.h>
#include <xait/common/Library.h>
#include <xait/common/memory/StaticSTLAllocator.h>
#include <sstream>

typedef std::basic_stringstream<char,std::char_traits<char>,XAIT::Common::Memory::StaticSTLAllocator<char,&XAIT::Common::Library::getDebugAlloc> >	StringStream;

#define XAIT_REPORT_LOG(reporter,logType,msg)	\
	{if (reporter->hasLogTypeEnabled(logType))\
	{\
		StringStream _xlog_string;\
		_xlog_string << msg;\
		reporter->reportLog(logType,_xlog_string.str().c_str(),XAIT_CURRENT_FUNC,XAIT_CURRENT_FILE,XAIT_CURRENT_LINE);\
	};}

#define XAIT_MESSAGE_DBG(reporter,msg)	XAIT_REPORT_LOG(reporter,XAIT::Common::Debug::LT_DEBUG_MESSAGE,msg)
#define XAIT_ERROR_DBG(reporter,msg)	XAIT_REPORT_LOG(reporter,XAIT::Common::Debug::LT_DEBUG_ERROR,msg)
#define XAIT_MESSAGE(reporter,msg)		XAIT_REPORT_LOG(reporter,XAIT::Common::Debug::LT_MESSAGE,msg)
#define XAIT_WARNING(reporter,msg)		XAIT_REPORT_LOG(reporter,XAIT::Common::Debug::LT_WARNING,msg)
#define XAIT_ERROR(reporter,msg)		XAIT_REPORT_LOG(reporter,XAIT::Common::Debug::LT_ERROR,msg)


