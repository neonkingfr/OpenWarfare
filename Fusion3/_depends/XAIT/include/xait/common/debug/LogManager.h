// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/Singleton.h>
#include <xait/common/debug/LogType.h>
#include <xait/common/debug/LogReporter.h>
#include <xait/common/debug/LogDispatcher.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/container/List.h>
#include <xait/common/container/Array.h>
#include <xait/common/threading/SpinLock.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			//! The LogManager class maintains the LogReporter. This is a system to distribute log messages.
			//! \brief LogManager class that maintains a set of LogReporter
			//! \remark You should first register a dispatcher before you use any other method so that you receive
			//!			already log messages.
			class LogManager : public Singleton<LogManager>
			{
				friend class Singleton<LogManager>;
				friend class LogReporter;
			public:
				//! \brief default constructor
				XAIT_COMMON_DLLAPI LogManager();

				//! \brief create a new log reporter
				//! \returns a pointer to the new log reporter or NULL if an log reporter with that name already exists
				XAIT_COMMON_DLLAPI LogReporter* createLogReporter(const Common::String& instanceName);

				//! \brief get the log reporter of a specific instance
				XAIT_COMMON_DLLAPI XAIT::Common::Debug::LogReporter* getLogReporter(const Common::String& instanceName);

				//! \brief delete a log reporter
				XAIT_COMMON_DLLAPI void deleteLogReporter(LogReporter* reporter);

				//! \brief enable/disable global log types (overrides local ones permanent)
				//! \param logTypes		ored list of log types which should be enabled
				XAIT_COMMON_DLLAPI void setGlobalLogTypes(const LogType logTypes);

				//! \brief enable/disable one global log type (overrides local ones permanent)
				//! \param logType	log type which should be enabled/disabled
				//! \param enable	if true log type will be enabled, if false, it will be disabled
				XAIT_COMMON_DLLAPI void enableGlobalLogType(const LogType logType, bool enable);

				//! \brief enable/disable global log types on which an assertion should be thrown
				//! \param logTypes		ored list of log types which should issue an assert
				XAIT_COMMON_DLLAPI void setGlobalAssertTypes(const LogType logTypes);

				//! \brief enable/disable one global log type to raise assertions
				//! \param logType	log type which should raise an assertion
				//! \param enable	if true log type will raise assertions, if false it will not.
				XAIT_COMMON_DLLAPI void enableGlobalAssertType(const LogType logType, bool enable);

				//! \brief register log dispatcher for certain log events
				//! \param logType			type on which the dispatcher should react
				//! \param logDispatcher	instance of the dispatcher to use
				//! \param singleHandler	if registered as single handler only this dispatcher will be used all other dispatcher will be removed
				//! \remark There can be multiple log dispatcher for a single log event
				XAIT_COMMON_DLLAPI void registerLogDispatcher(const LogType logType, LogDispatcher* logDispatcher, const bool singleHandler= false);

				//! \brief remove a log dispatcher for certain log events
				//! \param logType			type from which to deregister the dispatcher
				//! \param logDispatcher	dispatcher to remove
				//! \remark If you want to remove the dispatcher from all log types, use LT_LOG_ALL flag.
				XAIT_COMMON_DLLAPI void removeLogDispatcher(const LogType logType, LogDispatcher* logDispatcher);
			
			private:
				//! \brief destructor
				//! \remark Made private, so singleton instance must be used to destroy the instance
				~LogManager();

				//! \brief report a log 
				void reportLog(const LogReporter* reporter, LogReport& report);

				typedef Container::List<LogDispatcher*>				DispatchQueue;

				LogReporter*										mInternalReporter;		//!< reporter for internal messages
				Container::LookUp<Common::String,LogReporter*>		mReporter;				//!< all created log reporters
				Container::Array<DispatchQueue,LT_MAX_LOG_BIT>		mDispatchQueues;		//!< assignment for logType -> dispatchers
				Threading::SpinLock									mLogReportLock;			//!< used to support logging from different threads
			};
		}
	}
}

