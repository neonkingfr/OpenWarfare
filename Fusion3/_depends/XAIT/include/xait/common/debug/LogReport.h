// (C) xaitment GmbH 2006-2012

#pragma once 
#include <xait/common/debug/LogType.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			struct LogReport
			{
				LogType				mType;
				Common::String		mMessage;
				Common::String		mInstanceName;
				Common::String		mFunctionName;
				Common::String		mFileName;
				uint32				mLineNumber;
			};
		}
	}
}

