// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/MinimalPrerequisites.h>

#include <xait/common/CommonLog.h>

#include <xait/common/debug/Debug.h>

#include <xait/common/memory/Memory.h>
#include <xait/common/memory/StaticMemoryManaged.h>
#include <xait/common/memory/StaticSTLAllocator.h>

#include <xait/common/container/Array.h>
#include <xait/common/container/List.h>
#include <xait/common/container/Vector.h>
#include <xait/common/container/IDVector.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/container/Pair.h>
#include <xait/common/container/Queue.h>
#include <xait/common/container/Set.h>
#include <xait/common/container/Triple.h>
#include <xait/common/container/KDTree.h>

#include <xait/common/io/Stream.h>
#include <xait/common/io/FileStream.h>

#include <xait/common/math/NumericLimits.h>
#include <xait/common/math/ValueFunc.h>
#include <xait/common/math/Matrix3.h>
#include <xait/common/math/Matrix4.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>

#include <xait/common/geom/AABB2D.h>
#include <xait/common/geom/AABB3D.h>
#include <xait/common/geom/Ray2D.h>
#include <xait/common/geom/Ray3D.h>
#include <xait/common/geom/Triangle3D.h>
#include <xait/common/geom/GeomUtility.h>

#include <xait/common/reflection/datatype/Datatypes.h>

#include <xait/common/threading/Mutex.h>
#include <xait/common/threading/Thread.h>
#include <xait/common/threading/ThreadPool.h>
#include <xait/common/threading/Threaded.h>

#include <xait/common/parser/xml/XMLTreeReader.h>
#include <xait/common/parser/xml/XMLParser.h>
#include <xait/common/parser/xml/XMLWriter.h>
