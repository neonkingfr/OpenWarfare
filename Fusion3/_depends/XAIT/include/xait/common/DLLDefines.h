// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/Platform.h>


#ifdef XAIT_LIBRARY_DYNAMIC

#ifdef XAIT_COMMON_DLL_EXPORTS
	#define XAIT_COMMON_DLLAPI	XAIT_DLLEXPORT
#else
	#define XAIT_COMMON_DLLAPI	XAIT_DLLIMPORT
#endif

#else

#define XAIT_COMMON_DLLAPI

#endif

