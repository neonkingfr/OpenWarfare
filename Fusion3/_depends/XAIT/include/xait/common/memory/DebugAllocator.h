// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/Allocator.h>
#include <xait/common/String.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/container/Pair.h>


namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			//! \brief an allocator which stores the callstack during an allocate call
			class DebugAllocator : public Allocator
			{
			public:
				typedef Ptr::Rebind<DebugAllocator>::Other	Ptr;

				DebugAllocator(const Allocator::Ptr& allocator)
					: Allocator("Debug"),mInnerAlloc(allocator),mAllocateStack(allocator)
				{}

				~DebugAllocator();

				bool isClean() const
				{
					return mInnerAlloc->isClean();
				}

				//! \brief print the currently allocated memory blocks to the trace log
				void printAllocatedBlocks() const;

				//! \brief write allocated blocks to a file
				void writeAllocatedBlocksToFile(const Common::String& fileName);

			protected:
				void* allocInternal(const uint32 numBytes);

				void freeInternal(void* ptr);

			private:
				Memory::Allocator::Ptr			mInnerAlloc;
				Container::LookUp<void*,		// memory block address
					Container::Pair<uint32,		// number of bytes for the block
					Common::String				// callstack
					> >							mAllocateStack;
			};
		}
	}
}
